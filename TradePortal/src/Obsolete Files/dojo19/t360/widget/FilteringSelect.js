define(["dojo/_base/declare", "dijit/form/FilteringSelect", "dojo/domReady!"],
  function(declare, FilteringSelect){
    return declare("t360.widget.FilteringSelect", FilteringSelect, {
      _setCaretPos: function(element, location) {
        // This function is only ever called in order to move to the end of the 
        // input.  We want to prevent that, so call teh inherited code with
        // location equal to 0 (beginning of the field) every time.
        this.inherited(arguments, [element, 0]);
      }
    });
  }
);

