define([
	'dojo/_base/declare',
	'dojo/_base/array',
	'dojo/_base/lang',
	'dojo/dom-construct',
	'dojo/dom-style',
	'dojo/dom-geometry',
	'dojo/dom-class',
	'dojo/on',
	'dojo/string',
	'dijit/_WidgetBase',
	'dijit/_TemplatedMixin',
	'dijit/TitlePane',
	'dojo/query',
	'dojo/text!./template/FormSidebar.html'
], function(declare, arrayUtil, lang, domConstruct, domStyle, domGeometry, domClass,
            on, string, _WidgetBase, _TemplatedMixin, TitlePane,query, template){
	// module:
	//		app/widgets/FormSidebar
	// summary:
	//		This is an example of a basic templated widget. Its superclass is _WidgetBase
	//		and it mixes in _TemplatedMixin. Because declarative widgets are used and
	//		Dojo 1.7’s parser does not support loading through AMD yet, this class is
	//		declared as a global object at app.widgets.FormSidebar.

  return declare('t360.widget.FormSidebar', [ _WidgetBase, _TemplatedMixin ], {
    // baseClass:
    //		The base CSS class for this widget.
    baseClass: 'appFormSidebar',

    // templateString:
    //		The HTML template to use for this widget.
    templateString: template,

    // linkTemplateString:
    //		An HTML template to use when generating navigation links from the list of form sections.
    linkTemplateString: '<li><a href="${href}">${title}</a></li>',

    // form: String|dijit.form.Form
    //		The related form containing sections that should be used to build the list of sections.
    form: null,

    // sections: TitlePane[]
    //		The sections that are handled by this sidebar. Note that this array object is shared by
    //		all instances of FormSidebar due to the way prototypal inheritance works. We create a
    //		fresh array in postCreate that replaces this shared object for every instance of
    //		FormSidebar since the data is supposed to be unique to each FormSidebar instance.
    sections: [],

    //cquinton 10/10/2012 ir#6053 - remove title reference

    postCreate: function(){

      // Create a new array object for holding the list of sections for this widget
      var sections = [];

      // Make sure that the form property is actually a dijit.form.Form,
      // even if an ID string was passed instead.
      this.form = dijit.byId(this.form);

      // For each child widget of the associated form…
      arrayUtil.forEach(this.form.getChildren(), lang.hitch(this, function(child){

        // If the widget is a TitlePane widget…
        if(child instanceof TitlePane){
 
          //cquinton 11/3/2012 the special class doNoShowOnSidebar if applied to a
          // titlepane dom node will cause it not to be displayed in the toolbar 
          if ( !domClass.contains( child.domNode, "doNotShowOnSidebar" ) ) {
            // Add it to the list of sections for this sidebar
            sections.push(child);

            //cquinton 10/10/2012 ir#6053 - add sections to navContainer domElement
            // Create a new HTMLAnchorElement pointing to the section and
            // append it to navContainer (which is defined in the widget template)
            domConstruct.place(string.substitute(this.linkTemplateString, {
                title: child.get('title'),
                href: '#' + child.get('id')
              }), "navContainer");
          }
        }
      }));

      // Assign the list of sections to this instance so they can be used later
      this.sections = sections;

      //cquinton 10/10/2012 ir#6053 - make the section shortcuts visible
      //if there are some
      //cquinton 9/13/2012 Rel portal refresh add titlepane to navContainer
      if ( sections && sections.length > 0 ) {
        query('.sectionLinksSection').forEach(function(entry) {
          domClass.add(entry, 'contentExists');
        });
      }
      else {
        //no section links, check for quick links
        //if nothing there, display the header
        var noSidebarSections = true;
        query('.sidebarLinksSection').forEach(function(entry) {
          noSidebarSections = false;
        });
        if ( noSidebarSections ) {
          query('.formSidebarHeader').forEach(function(entry) {
            domClass.add(entry, 'noSidebarSections');
          });
        }
      }

      // Always remember to call the parent method!
      this.inherited(arguments);
    },

		startup: function(){
			// summary:
			//		startup is part of the Dijit _WidgetBase lifecycle. Note that if
			//		you ever programmatically create a widget, you must *manually call
			//		this method after adding the widget to the DOM* or some widgets
			//		will not work properly.

			// Get the static position of the element in the normal flow of the document
			var position = domGeometry.position(this.domNode);
			  console.log("startup");
			// Make it fixed-position
			domStyle.set(this.domNode, {
				position: 'fixed',
				left: position.x + 'px',
				top: position.y + 'px'
			});
			//added by dillip for IR T36000005372 and T36000005460
			var ret = domGeometry.position(this.domNode);
			console.log("ret::"+ret);
			var pos = parseInt(ret.h + 20 ) ;
			console.log("pos: " + pos);
			dojo.query('div.formArea').style('minHeight',pos+'px');

			//Ended Here T36000005372 and T36000005460
			// When the window size changes, the element needs to be moved since it
			// is resizes on the x-axis will change where it should appear
			this._repositionHandler = on(window, 'resize', lang.hitch(this, 'reposition'));

			// Always remember to call the parent method!
			this.inherited(arguments);
		},

		reposition: function(){
			// summary:
			//		Adjusts the positioning of this widget.

			// Reset the element back to its original position in the DOM so we can
			// figure out where it is supposed to be now. The browser will not actually
			// redraw until we exit this function (or in IE, until the top of the
			// call stack is reached) so the user won’t actually notice that we have
			// done this

			 console.log("reposition");
			domStyle.set(this.domNode, 'position', 'static');

			// Make it fixed-position
			domStyle.set(this.domNode, {
				position: 'fixed',
				left: domGeometry.position(this.domNode).x + 'px'
			});
			//added by dillip for IR T36000005372 and T36000005460
			var ret = domGeometry.position(this.domNode);
			var pos = parseInt(ret.y + ret.h + 20 ) ;
			console.log("pos: " + pos);
			//domStyle.set(this.form, {min-height: pos});
			domStyle.set(this.form, 'min-height', pos + 'px');
			this.form.style['min-height'] = pos+'px';
			//domStyle.set(this.form, 'min-height', '600px');
			var n = dojo.query('div.formArea');
			console.log("n: " + n);
			n.style['minHeight']=pos+'px';
			n.style.minHeight=pos+'px';
			//Ended here T36000005372 and T36000005460
		},

		expandAllSections: function(){
			// summary:
			//		Expands all form sections.

			// For each section…
			arrayUtil.forEach(this.sections, function(section){
				// Set its open property to true. Note the use of .set instead of
				// setting the property directly; this is done for two reasons:
				// 1. So the correct setter function is called automatically
				// which is actually responsible for changing the visual display of
				// the TitlePane
				// 2. So other components can observe this state change if they want
				section.set('open', true);
			});
		},

		hideTips: function(){
			// summary:
			//		Hides prompt tooltips.

			// For each form widget in the form…
			arrayUtil.forEach(this.form._getDescendantFormWidgets(), function(child){
				// If it has a prompt message for a user…
				if(child.get('promptMessage')){
					// Get rid of it.
					child.set('promptMessage', '');
				}
			});
		},

		destroy: function(){
			// summary:
			//		destroy is part of the Dijit _WidgetBase lifecycle.

			// Unset the event handler on the window so that this object can be
			// properly garbage collected.
			this._repositionHandler.remove();

			// Always remember to call the parent method!
			this.inherited(arguments);
		}
	});
});
