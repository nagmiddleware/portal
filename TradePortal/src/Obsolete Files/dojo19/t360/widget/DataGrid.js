define(["dojo/_base/declare", "dojo/_base/array", "dojo/_base/html", "dojox/grid/DataGrid", "dojo/dom", "dojo/dom-construct", "dojo/_base/sniff"], 
  function(declare, array, html, DataGrid, dom, domConstruct, sniff){
    return declare("t360.widget.DataGrid", DataGrid, {

      _adjustedHeight: 0, //keep track of height after adjustment
      _definedHorizScrollbarSize: false,
      _horizScrollbarSize: undefined,
      _allowSBHeightAdjust: true, //turn scrollbar height adjustment off if necessary

      // when sizing the grid, ensure we take into account a horizontal scrollbar
      _resize: function(changeSize, resultSize){
        //following commented code allows some debugging of how many times
        //the grid resize event fires. to use, add <div id="resizeCounter"></div> somewhere on the page
        //var resizeCounter = dom.byId("resizeCounter");
        //if ( resizeCounter ) {
        //  var resizeCount = parseInt(resizeCounter.innerHTML);
        //  resizeCount += 1;
        //  resizeCounter.innerHTML = resizeCount;
        //}
        this.inherited(arguments);

        //cquinton 1/9/2013 ie7 on some dialogs recursively calls resize if we modify stuff,
        // so just turn this off for ie7. please be careful with this
        // it causes javascript to run forever...
        if ( this._allowSBHeightAdjust ) {
          if ( sniff("ie")<=8 ) {
            this._allowSBHeightAdjust = false;
          }
        }

        if( this._allowSBHeightAdjust && !this._autoHeight && typeof this.autoHeight === "number" ) {
          // In this case, autoHeight was set to a number, and that number is
          // smaller than the total number of rows in the result set.
          // The inherited logic will have already attempted to adjust the
          // grid's height, but it will have failed to consider if a
          // horizontal scrollbar was present.

          //only adjust if size has changed to avoid repeating
          var currentHeight = parseInt(this.domNode.style.height, 10);
          if (currentHeight != this._adjustedHeight ) {
            //alert('size change:'+currentHeight+':'+this._adjustedHeight);
            var node = this.views.views[this.views.views.length - 1].scrollboxNode;
            if(node.scrollWidth > node.offsetWidth){

              //get a scrollbar height - this is done only once
              if ( !this._definedHorizScrollbarSize ) {
                var tempnode = domConstruct.create("div", {
                  style: {
                    visibility: "hidden",
                    position: "absolute",
                    height: "100px",
                    width: "100px",
                    overflow: "scroll"
                  }
                }, document.body);
                this._horizScrollbarSize = tempnode.offsetHeight - tempnode.clientHeight;
                if ( this._horizScrollbarSize <= 0 ) {
                  //this is for just in case
                  //ie7 sometimes can't figure it out
                  this._horizScrollbarSize = 16;
                }
                domConstruct.destroy(tempnode);
                tempnode = null;
                this._definedHorizScrollbarSize = true;
              }

              this._adjustedHeight = currentHeight + this._horizScrollbarSize;
              this.domNode.style.height = this._adjustedHeight + "px";
              this.adaptHeight();
              this.postresize();
            }
          }
          // Set a flag to avoid accidentally repeating this logic
          //this._adjustedForScrollbar = true;
        }

      }
 
    });
  }
);

