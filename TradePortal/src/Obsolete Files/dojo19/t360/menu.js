define([
  "t360/dialog","dijit/registry"
], function(dialog, registry) {

  var menu = {

		  createCopyPaymentsDialog: function() {
		      console.log('in createCopyPaymentsDialog');
		    //Closing all the registered dialog Id's before opening another one, so that widget Id conflicts wont occur
		      
		      if(registry.byId('bankBranchSelectorDialog'))
		           	dialog.close('bankBranchSelectorDialog');
		        if(registry.byId('transferExportLCDialog'))
		        	dialog.close('transferExportLCDialog');
		        if(registry.byId('instrumentSearchDialog'))
		        	dialog.close('instrumentSearchDialog');
		        if(registry.byId('templateSearchDialog'))
		        	dialog.close('templateSearchDialog');
		        if(registry.byId('copyPaymentsDialog'))
		        	dialog.close('copyPaymentsDialog');
		        if(registry.byId('copyDirectDebitDialog'))
		        	dialog.close('copyDirectDebitDialog');
		        
		        dialog.open('copyPaymentsDialog', copyPaymentsDialogTitle,
		                    'ExistingPaymentSearch.jsp',
		                    null, null, //parameters
		                    'select', null);
		    },
		    
		  createCopyDirectDebitDialog: function() {
		      console.log('in createCopyDirectDebitDialog');
		    
		      if(registry.byId('bankBranchSelectorDialog'))
		           	dialog.close('bankBranchSelectorDialog');
		        if(registry.byId('transferExportLCDialog'))
		        	dialog.close('transferExportLCDialog');
		        if(registry.byId('instrumentSearchDialog'))
		        	dialog.close('instrumentSearchDialog');
		        if(registry.byId('templateSearchDialog'))
		        	dialog.close('templateSearchDialog');
		        if(registry.byId('copyPaymentsDialog'))
		        	dialog.close('copyPaymentsDialog');
		        if(registry.byId('copyDirectDebitDialog'))
		        	dialog.close('copyDirectDebitDialog');
		        
		        dialog.open('copyDirectDebitDialog', copyDirectDebitDialogTitle,
		                    'ExistingDirectDebitSearch.jsp',
		                    null, null, //parameters
		                    'select', null);
		    },
		    
    //Create new instrument from a template
    createNewInstrumentFromTemplate: function(bankBranchArray) {
      //alert('in createNewInstrumentFromTemplate');
    
      if ( document.getElementsByName('NewInstrumentForm').length > 0 ) {
        var theForm = document.getElementsByName('NewInstrumentForm')[0];
        theForm.mode.value='CREATE_NEW_INSTRUMENT';
        theForm.copyType.value='Templ';
        
        if(registry.byId('bankBranchSelectorDialog'))
           	dialog.close('bankBranchSelectorDialog');
        if(registry.byId('transferExportLCDialog'))
        	dialog.close('transferExportLCDialog');
        if(registry.byId('instrumentSearchDialog'))
        	dialog.close('instrumentSearchDialog');
        if(registry.byId('templateSearchDialog'))
        	dialog.close('templateSearchDialog');
        if(registry.byId('copyPaymentsDialog'))
        	dialog.close('copyPaymentsDialog');
        if(registry.byId('copyDirectDebitDialog'))
        	dialog.close('copyDirectDebitDialog');
        
        dialog.open('templateSearchDialog', templateSearchDialogTitle,
                'templateSearchDialog.jsp',
                'bankBranchArray', bankBranchArray, //parameters
                'select', this.createNewInstrumentTemplateSelected);            
      } else {
        alert('Problem in createNewInstrumentFromTemplate: cannot find NewInstrumentForm');
      }
    
    },
    //the createNewInstrumentFromTemplate callbacks
    createNewInstrumentTemplateSelected: function(templateOid) {
      if ( document.getElementsByName('NewInstrumentForm').length > 0 ) {
        var theForm = document.getElementsByName('NewInstrumentForm')[0];
        //theForm.bankBranch.value=bankBranchOid;
        theForm.copyInstrumentOid.value=templateOid;
        theForm.submit();
      }
    },
    
    
    //Create new instrument from existing
    createNewInstrumentFromExisting: function(bankBranchArray) {
    
      if ( document.getElementsByName('NewInstrumentForm').length > 0 ) {
        var theForm = document.getElementsByName('NewInstrumentForm')[0];
        theForm.mode.value='CREATE_NEW_INSTRUMENT';
        theForm.copyType.value='Instr';
        theForm.bankBranch.value=bankBranchArray[0];
        
        if(registry.byId('bankBranchSelectorDialog'))
           	dialog.close('bankBranchSelectorDialog');
        if(registry.byId('transferExportLCDialog'))
        	dialog.close('transferExportLCDialog');
        if(registry.byId('instrumentSearchDialog'))
        	dialog.close('instrumentSearchDialog');
        if(registry.byId('templateSearchDialog'))
        	dialog.close('templateSearchDialog');
        if(registry.byId('copyPaymentsDialog'))
        	dialog.close('copyPaymentsDialog');
        if(registry.byId('copyDirectDebitDialog'))
        	dialog.close('copyDirectDebitDialog');
        
              dialog.open('instrumentSearchDialog', instrumentSearchDialogTitle,
                      'instrumentSearchDialog.jsp',
                      null, null, //parameters
                      'select', this.createNewInstrumentInstrumentSelected);
      } else {
        alert('Problem in createNewInstrumentFromExisting: cannot find NewInstrumentForm');
      }
    
    },
    
  //the createNewInstrument callbacks
    createNewInstrumentBankBranchSelected: function(bankBranchOid) {
      if ( document.getElementsByName('NewInstrumentForm').length > 0 ) {
        var theForm = document.getElementsByName('NewInstrumentForm')[0];
        theForm.bankBranch.value=bankBranchOid;
        theForm.submit();
      }
    },
    
    createNewInstrumentBankBranchSelectedValue: function(bankBranchOid) {

        if ( document.getElementsByName('NewInstrumentForm').length > 0 ) {
          var theForm = document.getElementsByName('NewInstrumentForm')[0];
          theForm.mode.value='CREATE_NEW_INSTRUMENT';
          theForm.copyType.value='Instr';
          theForm.bankBranch.value=bankBranchOid;
          //theForm.submit();        
        }
        
        if(registry.byId('bankBranchSelectorDialog'))
           	dialog.close('bankBranchSelectorDialog');
        if(registry.byId('transferExportLCDialog'))
        	dialog.close('transferExportLCDialog');
        if(registry.byId('instrumentSearchDialog'))
        	dialog.close('instrumentSearchDialog');
        if(registry.byId('templateSearchDialog'))
        	dialog.close('templateSearchDialog');
        if(registry.byId('copyPaymentsDialog'))
        	dialog.close('copyPaymentsDialog');
        if(registry.byId('copyDirectDebitDialog'))
        	dialog.close('copyDirectDebitDialog');
        
        dialog.open('instrumentSearchDialog', instrumentSearchDialogTitle,
                'instrumentSearchDialog.jsp',
                null, null, //parameters
                'select', this.createNewInstrumentInstrumentSelected);
      },
  //the createNewInstrumentFromExisting callbacks
    createNewInstrumentInstrumentSelected: function(instrumentOid) {
      if ( document.getElementsByName('NewInstrumentForm').length > 0 ) {
        var theForm = document.getElementsByName('NewInstrumentForm')[0];
        //theForm.bankBranch.value=bankBranchOid;
        theForm.copyInstrumentOid.value=instrumentOid;
        theForm.submit();
      }
    },
    
    //transfer export LC
    transferExportLC: function(bankBranchArray) {
      //alert('in createNewInstrumentFromTemplate');
    
      if ( document.getElementsByName('NewInstrumentForm').length > 0 ) {
        var theForm = document.getElementsByName('NewInstrumentForm')[0];
        theForm.mode.value='CREATE_NEW_INSTRUMENT';
        theForm.copyType.value='TRANSFER';
    
        if(registry.byId('bankBranchSelectorDialog'))
           	dialog.close('bankBranchSelectorDialog');
        if(registry.byId('transferExportLCDialog'))
        	dialog.close('transferExportLCDialog');
        if(registry.byId('instrumentSearchDialog'))
        	dialog.close('instrumentSearchDialog');
        if(registry.byId('templateSearchDialog'))
        	dialog.close('templateSearchDialog');
        if(registry.byId('copyPaymentsDialog'))
        	dialog.close('copyPaymentsDialog');
        if(registry.byId('copyDirectDebitDialog'))
        	dialog.close('copyDirectDebitDialog');
        
        //just reuse the createNewInstrumentFromExisting callback
        dialog.open('transferExportLCDialog', transferExportLCDialogTitle,
                    'transferExportLCDialog.jsp',
                    null, null, //parameters
                    'select', this.createNewInstrumentInstrumentSelected);
    
      } else {
        alert('Problem in transferExportLC: cannot find NewInstrumentForm');
      }
    
    },
    //the transferExportLC callbacks
    //for now just reuse create from existing callbacks above
    
    
    //Create new instrument by instrument type and bank/branch
    //it does this by locating the NewInstrumentForm and submitting it with necessary parameters
    //if a single bank/branch is passed in, immediately submit the form.
    //if more than one, open the BankBranchSelector dialog
    createNewInstrument: function(bankBranchArray, newInstrumentType) {
    
      if ( document.getElementsByName('NewInstrumentForm').length > 0 ) {
    	  
        var theForm = document.getElementsByName('NewInstrumentForm')[0];
        theForm.mode.value='CREATE_NEW_INSTRUMENT';
        theForm.copyType.value='Blank';
        
        if ( newInstrumentType ) {
          theForm.instrumentType.value=newInstrumentType;
        } else {
          alert('Problem in createNewInstrument: instrumentType not defined');
        }
    
        if ( bankBranchArray ) {
          if ( bankBranchArray.length == 1 ) {
            theForm.bankBranch.value=bankBranchArray[0];
            theForm.submit();
            //defect 2738 by dillip chnages revrerted back
          } else if (bankBranchArray.length > 1 ) {
        	  
        	  if(registry.byId('bankBranchSelectorDialog'))
		           	dialog.close('bankBranchSelectorDialog');
		        if(registry.byId('transferExportLCDialog'))
		        	dialog.close('transferExportLCDialog');
		        if(registry.byId('instrumentSearchDialog'))
		        	dialog.close('instrumentSearchDialog');
		        if(registry.byId('templateSearchDialog'))
		        	dialog.close('templateSearchDialog');
		        if(registry.byId('copyPaymentsDialog'))
		        	dialog.close('copyPaymentsDialog');
		        if(registry.byId('copyDirectDebitDialog'))
		        	dialog.close('copyDirectDebitDialog');
		        
            dialog.open('bankBranchSelectorDialog', bankBranchSelectorDialogTitle,
                  'bankBranchSelectorDialog.jsp',
                  null, null, //parameters
                  'select', this.createNewInstrumentBankBranchSelected);
          } else {
            alert('Problem in createNewInstrument: bankBranchArray is empty');
          }
        } else {
          alert('Problem in createNewInstrument: bankBranchArray not defined');
        }
    
      } else {
        alert('Problem in createNewInstrument: cannot find NewInstrumentForm');
      }
    
    },
    
    createAssignment: function() {
        if ( document.getElementsByName('NewInstrumentForm').length > 0 ) {
          var theForm = document.getElementsByName('NewInstrumentForm')[0];
          theForm.mode.value='USE_EXISTING_INSTR';
          theForm.copyType.value='Instr';
          //theForm.bankBranch.value=bankBranchArray[0];
          if(registry.byId('bankBranchSelectorDialog'))
	           	dialog.close('bankBranchSelectorDialog');
	        if(registry.byId('transferExportLCDialog'))
	        	dialog.close('transferExportLCDialog');
	        if(registry.byId('instrumentSearchDialog'))
	        	dialog.close('instrumentSearchDialog');
	        if(registry.byId('templateSearchDialog'))
	        	dialog.close('templateSearchDialog');
	        if(registry.byId('copyPaymentsDialog'))
	        	dialog.close('copyPaymentsDialog');
	        if(registry.byId('copyDirectDebitDialog'))
	        	dialog.close('copyDirectDebitDialog');
	        
                dialog.open('instrumentSearchDialog', instrumentSearchDialogTitle,
                        'instrumentSearchDialog.jsp',
                        ['createAssignmentFlag'], ['Y'], //parameters
                        'select', this.createAssignmentSelected);
        } else {
          alert('Problem in createAssignment: cannot find NewInstrumentForm');
        }
      
      },
      
      createAssignmentSelected: function(instrumentOid) {
    	  if ( document.getElementsByName('NewInstrumentForm').length > 0 ) {
            var theForm = document.getElementsByName('NewInstrumentForm')[0];
            theForm.copyInstrumentOid.value=instrumentOid;
            //IR 20032 -set transaction type
            theForm.transactionType.value='ASN'; 
            theForm.submit();
          }
        },
        
    createAmendment : function(){
    	if(registry.byId('bankBranchSelectorDialog'))
           	dialog.close('bankBranchSelectorDialog');
        if(registry.byId('transferExportLCDialog'))
        	dialog.close('transferExportLCDialog');
        if(registry.byId('instrumentSearchDialog'))
        	dialog.close('instrumentSearchDialog');
        if(registry.byId('templateSearchDialog'))
        	dialog.close('templateSearchDialog');
        if(registry.byId('copyPaymentsDialog'))
        	dialog.close('copyPaymentsDialog');
        if(registry.byId('copyDirectDebitDialog'))
        	dialog.close('copyDirectDebitDialog');
        
    	dialog.open('instrumentSearchDialog', instrumentSearchDialogTitle,
                'instrumentSearchDialog.jsp',                
                ['createAmendmentFlag'], ['Y'], //parameters
                'select', this.createAmendmentSelected);
    },
    createAmendmentSelected: function(instrumentOid) {
    	if ( document.getElementsByName('NewInstrumentForm').length > 0 ) {
    		var theForm = document.getElementsByName('NewInstrumentForm')[0];
            theForm.mode.value='USE_EXISTING_INSTR';
            theForm.copyInstrumentOid.value=instrumentOid;
            theForm.transactionType.value='AMD';
            theForm.submit();
    	}
      },
      
   /*  Jyoti modified on 15-10-2012 for IR6117  */
    createTracer : function(){
    	if(registry.byId('bankBranchSelectorDialog'))
           	dialog.close('bankBranchSelectorDialog');
        if(registry.byId('transferExportLCDialog'))
        	dialog.close('transferExportLCDialog');
        if(registry.byId('instrumentSearchDialog'))
        	dialog.close('instrumentSearchDialog');
        if(registry.byId('templateSearchDialog'))
        	dialog.close('templateSearchDialog');
        if(registry.byId('copyPaymentsDialog'))
        	dialog.close('copyPaymentsDialog');
        if(registry.byId('copyDirectDebitDialog'))
        	dialog.close('copyDirectDebitDialog');
        
    	dialog.open('instrumentSearchDialog', instrumentSearchDialogTitle,
                'instrumentSearchDialog.jsp',
                ['createTracerFlag'], ['Y'], //parameters
                'select', this.createTracerSelected);
    }, 
      createTracerSelected: function(instrumentOid) {
    	if ( document.getElementsByName('NewInstrumentForm').length > 0 ) {
      		var theForm = document.getElementsByName('NewInstrumentForm')[0];
              theForm.mode.value='USE_EXISTING_INSTR';
              theForm.copyInstrumentOid.value=instrumentOid;
              theForm.transactionType.value='TRC';
              theForm.submit();
      	}
        }
    
    
  };

  return menu;
}); 


