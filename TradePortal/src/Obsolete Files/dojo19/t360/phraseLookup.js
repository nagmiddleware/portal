define(["dojo/_base/xhr", "t360/common", "dijit/registry"],
    function(xhr, common, registry) {

  var phraseLookup = {
    appendPhrase: function(phraseSelect, textArea, textAreaMaxLength, errorSectionId) {

      if ( phraseSelect && textArea ) { //defensive test
        var phraseOid = phraseSelect.value;
        if ( phraseOid && phraseOid != null && phraseOid.length>0 ) {
          //for the given text area max length, figure out how long the additional
          // data can be
          var maxLength = textAreaMaxLength;
          if ( textArea.value && textArea.value.length > 0 ) {
            var textAreaCurrentLength = textArea.value.length;
            if ( textAreaMaxLength > textAreaCurrentLength ) {
              maxLength = textAreaMaxLength - textAreaCurrentLength;
              if ( maxLength > 0 ) {
                maxLength = maxLength - 1; //account for space concatenator
              }
            }
            else {
              //we already know we can't append, but go ahead and
              //do the phrase lookup so we can get an error
              maxLength = 0;
            }
          }
          //now do the lookup
          xhr.get({
            url: "/portal/phraseLookup?SelectedPhrase="+phraseOid+
                   "&Maxlength="+maxLength+"&TextAreaMaxlength="+textAreaMaxLength,
            load: function(data) {
              if ( data.indexOf("<errorXml>")>=0 ) {
                var errXml = data.substring( data.indexOf('<errorXml>')+10 );
                errXml = errXml.substring( 0, errXml.indexOf('</errorXml>') );
                if ( errXml && errXml.length > 0 ) {
                  var errorCount = common.addXmlErrorsToErrorSection( errorSectionId, errXml, true);
                  //move user to top of page if there actually was an error
                  if ( errorCount > 0 ) {
                    window.location.href = "#top";
                  }
                }
              }
              //for now avoid whether success or not as we want the too long error to display
              // in addition to truncating.  but really this should be a warning and
              // if an error don't do the behavior!
              //if ( data.indexOf("SUCCESS")>=0 ) {
                if ( data.indexOf("<phrase>")>=0 ) {
                  var phrase = data.substring( data.indexOf('<phrase>')+8 );
                  phrase = phrase.substring( 0, phrase.indexOf('</phrase>') );
                  if ( phrase && phrase.length > 0 ) {
                    if ( textArea.value && textArea.value.length > 0 ) {
                      //Add space in between words except first word.
                      textArea.value += " ";
                    }
                    textArea.value += phrase;
                  }
                }
              //}
              //now clear the phraseSelect - set to first item
              var firstItem = phraseSelect.store.data[0];
              if ( firstItem ) {
                phraseSelect.set('item',firstItem);
              }
            },
            error: function(err) {
              //todo: make a custom dialog for errors
              //would be nice to display this in a nice fashion, 
              // probably also allow the user to avoid the error in future
              // an alternative would be to have some kind of error display at bottom of the page???
              alert('Error on phrase lookup:'+err);
            }
          });

        }
      }

    },

    replacePhrase: function(phraseSelect, textArea, textAreaMaxLength, errorSectionId) {

      if ( phraseSelect && textArea ) { //defensive test
        var phraseOid = phraseSelect.value;
        if ( phraseOid && phraseOid != null && phraseOid.length>0 ) {
          //clear out the current values
          if ( textArea.value && textArea.value.length>0 ) {
            textArea.value = "";
          }
          this.appendPhrase(phraseSelect, textArea, textAreaMaxLength, errorSectionId);
        }
      }

    }

  };

  return phraseLookup;

}); 

