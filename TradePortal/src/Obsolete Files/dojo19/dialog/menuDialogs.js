/*menuDialogs.js - javascript functions associated with menu dialogs*/

function openTemplateSearchDialog() {
  //create the embedded data grid
  //todo:

  //open the BankBranchSelectorDialog
  showDialog('templateSearchDialog');
}

function openInstrumentSearchDialog() {
  //create the embedded data grid
  //todo:

  //open the BankBranchSelectorDialog
  showDialog('instrumentSearchDialog');
}

function openBankBranchSelectorDialog() {
  //create the embedded data grid
  //todo:

  //open the BankBranchSelectorDialog
  showDialog('bankBranchSelectorDialog');
}

function openTransferExportLCDialog() {
  //create the embedded data grid
  //todo:

  //open the BankBranchSelectorDialog
  showDialog('instrumentSearchDialog');
}




function selectTemplate(templateOid) {
  //alert('bankBranch='+bankBranch);
  if ( document.getElementsByName('NewInstrumentForm').length > 0 ) {
    var theForm = document.getElementsByName('NewInstrumentForm')[0];
    theForm.copyInstrumentOid.value=templateOid;
  }
}

function selectInstrument(instrumentOid) {
  //alert('bankBranch='+bankBranch);
  if ( document.getElementsByName('NewInstrumentForm').length > 0 ) {
    var theForm = document.getElementsByName('NewInstrumentForm')[0];
    theForm.copyInstrumentOid.value=instrumentOid;
  }
}

function selectBankBranch(bankBranch) {
  //alert('bankBranch='+bankBranch);
  if ( document.getElementsByName('NewInstrumentForm').length > 0 ) {
    var theForm = document.getElementsByName('NewInstrumentForm')[0];
    theForm.bankBranch.value=bankBranch;
  }
}

function submitNewInstrumentForm() {
  if ( document.getElementsByName('NewInstrumentForm').length > 0 ) {
    var theForm = document.getElementsByName('NewInstrumentForm')[0];
    theForm.submit();
  }
}


function createbillsgrid() {
	alert('before billsGrid');
require(["dojox/grid/DataGrid", "dojo/data/ItemFileReadStore", "dojo/domReady!"],
  function(DataGrid) {
    var theGreatestTeamOfAllTime = {         
      items: [ 
        { "number":"12", "name":"Jim Kelly",      "position":"QB",                   "victories":"0"                   },                  
        { "number":"34", "name":"Thurman Thomas", "position":"RB",                   "victories":"0"                   },                  
        { "number":"89", "name":"Steve Tasker",   "position":"WR",                   "victories":"0"                   },             
        { "number":"78", "name":"Bruce Smith",    "position":"DE",                 "victories":"0"             }       
             ],         
      identifier: "number"     
    };       
    var dataStore = new dojo.data.ItemFileReadStore( { data:theGreatestTeamOfAllTime } );     
    var grid = new DataGrid({             
      store: dataStore,             
      query: { id: "*" }
    }, "billsGrid");           
  }); 
}

function createmygrid() {
    alert (' before mygrid');  
var grid2, dataStore2;
require(["dojox/grid/DataGrid", "dojo/data/ItemFileReadStore", "dojo/domReady!"],
  function(DataGrid,ItemFileReadStore){
    var mydata = {         
      items: [ 
        { "number":"12", "name":"Jim Kelly",      "position":"QB",                   "victories":"0"                   },                  
        { "number":"34", "name":"Thurman Thomas", "position":"RB",                   "victories":"0"                   },                  
        { "number":"89", "name":"Steve Tasker",   "position":"WR",                   "victories":"0"                   },             
        { "number":"78", "name":"Bruce Smith",    "position":"DE",                 "victories":"0"             }       
             ],         
      identifier: "number"     
      };     
    alert ('mydata:'+mydata);  
    dataStore2 = new dojo.data.ItemFileReadStore( { data:mydata } );     
    alert ('dataStore22:'+dataStore2);  
    grid2 = new DataGrid({             
      store: dataStore2,             
      query: { id: "*" },             
      structure: [                 
        { name: "Number", field: "number", width: "84px" },                 
        { name: "Name", field: "name", width: "84px" },                 
        { name: "Position", field: "position", width: "84px" },                 
        { name: "Victories", field: "victories", width: "60px" }   
      ] 
    }, "mygrid");           
    alert ('grid2:'+grid2);  
  }); 
}


//createbillsgrid();
//createmygrid();

