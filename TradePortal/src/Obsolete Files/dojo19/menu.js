/*menu.js - common menu functions*/

//Create new instrument from a template
function createNewInstrumentFromTemplate(bankBranchArray) {
  //alert('in createNewInstrumentFromTemplate');

  if ( document.getElementsByName('NewInstrumentForm').length > 0 ) {
    var theForm = document.getElementsByName('NewInstrumentForm')[0];
    theForm.mode.value='CREATE_NEW_INSTRUMENT';
    theForm.copyType.value='Templ';

    openTemplateSearchDialogHelper(createNewInstrumentTemplateSelected);

  } else {
    alert('Problem in createNewInstrumentFromTemplate: cannot find NewInstrumentForm');
  }

}
//the createNewInstrumentFromTemplate callbacks
function createNewInstrumentTemplateSelected(bankBranchOid,templateOid) {
  if ( document.getElementsByName('NewInstrumentForm').length > 0 ) {
    var theForm = document.getElementsByName('NewInstrumentForm')[0];
    theForm.bankBranch.value=bankBranchOid;
    theForm.copyInstrumentOid.value=templateOid;
    theForm.submit();
  }
}


//Create new instrument from existing
function createNewInstrumentFromExisting(bankBranchArray) {
  //alert('in createNewInstrumentFromExisting');

  if ( document.getElementsByName('NewInstrumentForm').length > 0 ) {
    var theForm = document.getElementsByName('NewInstrumentForm')[0];
    theForm.mode.value='CREATE_NEW_INSTRUMENT';
    theForm.copyType.value='Instr';

    openInstrumentSearchDialogHelper(createNewInstrumentInstrumentSelected);

  } else {
    alert('Problem in createNewInstrumentFromExisting: cannot find NewInstrumentForm');
  }

}
//the createNewInstrumentFromExisting callbacks
function createNewInstrumentInstrumentSelected(bankBranchOid,instrumentOid) {
  if ( document.getElementsByName('NewInstrumentForm').length > 0 ) {
    var theForm = document.getElementsByName('NewInstrumentForm')[0];
    theForm.bankBranch.value=bankBranchOid;
    theForm.copyInstrumentOid.value=instrumentOid;
    theForm.submit();
  }
}


//transfer export LC
function transferExportLC(bankBranchArray) {
  //alert('in createNewInstrumentFromTemplate');

  if ( document.getElementsByName('NewInstrumentForm').length > 0 ) {
    var theForm = document.getElementsByName('NewInstrumentForm')[0];
    theForm.mode.value='CREATE_NEW_INSTRUMENT';
    theForm.copyType.value='TRANSFER_ELC';

    //just reuse the createNewInstrumentFromExisting callback
    openTransferExportLCDialogHelper(createNewInstrumentInstrumentSelected);

  } else {
    alert('Problem in transferExportLC: cannot find NewInstrumentForm');
  }

}
//the transferExportLC callbacks
//for now just reuse create from existing callbacks above


//Create new instrument by instrument type and bank/branch
//it does this by locating the NewInstrumentForm and submitting it with necessary parameters
//if a single bank/branch is passed in, immediately submit the form.
//if more than one, open the BankBranchSelector dialog
function createNewInstrument(bankBranchArray, newInstrumentType) {
  //alert('in create new instrument');

  if ( document.getElementsByName('NewInstrumentForm').length > 0 ) {
    var theForm = document.getElementsByName('NewInstrumentForm')[0];
    theForm.mode.value='CREATE_NEW_INSTRUMENT';
    theForm.copyType.value='Blank';

    if ( newInstrumentType ) {
      theForm.instrumentType.value=newInstrumentType;
    } else {
      alert('Problem in createNewInstrument: instrumentType not defined');
    }

    if ( bankBranchArray ) {
      if ( bankBranchArray.length == 1 ) {
        theForm.bankBranch.value=bankBranchArray[0];
        theForm.submit();
      } else if (bankBranchArray.length > 1) {
        openBankBranchSelectorDialogHelper(createNewInstrumentBankBranchSelected);
      } else {
        alert('Problem in createNewInstrument: bankBranchArray is empty');
      }
    } else {
      alert('Problem in createNewInstrument: bankBranchArray not defined');
    }

  } else {
    alert('Problem in createNewInstrument: cannot find NewInstrumentForm');
  }

}
//the createNewInstrument callbacks
function createNewInstrumentBankBranchSelected(bankBranchOid) {
  if ( document.getElementsByName('NewInstrumentForm').length > 0 ) {
    var theForm = document.getElementsByName('NewInstrumentForm')[0];
    theForm.bankBranch.value=bankBranchOid;
    theForm.submit();
  }
}


