//!!!!!!!!do not use anything here -- use t360/common.js instead
var currencybox;//Added by Prateep for setCurrencyForAmount
      
//define showDialog and hideDialog for dialog action
require(["dijit/registry", "dijit/Dialog"], function(registry) {
  showDialog = function(dialogId) {
    registry.byId(dialogId).show();
  }
  hideDialog = function(dialogId) {
    registry.byId(dialogId).hide();
  }
});

//This function will reset's the set of date fields sent as a parameter(array)
//We can reset single or multiple fields using this function by passing the date field ID(s)
//For multiple fields while calling the function pass all the required field id's in comma (,) separated format  
//For Example: resetDateField('fieldId1','fieldId2','fieldId3',...);
function resetDateField(dateFieldId){
	require(["t360/common"], function(common) {
	    common.resetDateField(dateFieldId);
	});
}

//Called by the hyperlink which opens the Other Conditions Dailog
function openOtherConditionsDialog(fieldId){
  require(["t360/common"], function(common) {
    common.openOtherConditionsDialog(fieldId,"SaveTrans");
  });
}

function openOtherConditionsDialog(fieldId,buttonName){
  require(["t360/common"], function(common) {
    common.openOtherConditionsDialog(fieldId,buttonName,"Other Conditions");
  });
}
function openOtherConditionsDialog(fieldId,buttonName,otherConditionsDialogTitle){
  require(["t360/common"], function(common) {
    common.openOtherConditionsDialog(fieldId,buttonName,otherConditionsDialogTitle);
  });
}

function otherConditionsCallback(otherConditionsData){
      require(["t360/common"], function(common) {
          common.otherConditionsCallback(otherConditionsData);
      });
}

//Called by the Delete button Click to conform the Delete Action
function conformDeleteAction(buttonName, itemName){
  require(["t360/common"], function(common) {
    common.conformDeleteAction(buttonName, itemName);
  });
}

//Called by the hyperlink which shuffle the Collapse All & Expand All
function expandCollapse(){
  require(["t360/common"], function(common) {
    common.expandCollapse();
  });
}

// Called by the hyperlink and also the onLoad method of the body
function collapseAll(){
  require(["t360/common"], function(common) {
    common.collapseAll();
  });
}

function expandAll(){
  require(["t360/common"], function(common) {
    common.expandAll();
  });
}

function expand(id){
  require(["t360/common"], function(common) {
    common.expand(id);
  });
} 

function collapse(id){
  require(["t360/common"], function(common) {
    common.collapse(id);
  });
}

//Called by the hyperlink which shuffle the Hide Tips & Show Tips -- Added by Sandeep
function hideShowTips(){
  require(["t360/common"], function(common) {
    common.hideShowTips();
  });
}

//-- Added by Sandeep
function hideTips(){
  require(["t360/common"], function(common) {
    common.hideTips();
  });
}

//-- Added by Sandeep
function showTips(){
  require(["t360/common"], function(common) {
    common.showTips();
  });
}

//Added by Prateep
function setCurrencyForAmount(value,field){
  require(["t360/common"], function(common) {
    common.setCurrencyForAmount(value,field);
  });
}

//make an ajax call to the given url
//and replace the node with the given id
//with the ajax result.
//this also parses the result to ensure
//dojo widgets are instantiated
function attachAjaxResult(nodeId,remoteUrl) {
  require(["t360/common"], function(common) {
    common.attachAjaxResult(nodeId,remoteUrl);
  });
}

//for a given form name, figure out the form index #
function getFormNumber(formName) {
  var formNumber = 0; //default
  require(["t360/common"], function(common) {
    formNumber = common.getFormNumber(formName);
  });
  return formNumber;
}

//add a parameter to submit action for a form
// by creating a hidden field on the form
function addParmToForm(myForm, parmName, parmValue) {
  require(["t360/common"], function(common) {
    common.addParmToForm(myForm,parmName,parmValue);
  });
}


//submit the form and add repeating parms to it
//this currently supports a single parm name with multiple parm values
//it iterates over the parm values and create a unique numbered
//parm name
//TODO: as necessary this function could be updated
//to also take an array of parm name and values
//then each pair could be evaluated to see if its a single
//parm or a set of multiples.  will leave this for future exercise as necessary
function submitFormWithParms(formName, buttonName, parmName, parmValue) {
  require(["t360/common"], function(common) {
    common.submitFormWithParms(formName, buttonName, parmName, parmValue);
  });
}


function setButtonPressed(button, formNum) {
  var returnVal = false;
  require(["t360/common"], function(common) {
    returnVal = common.setButtonPressed(button, formNum);
  });
  return returnVal;
}

function setShipmentButtonPressed(button, formNum, shipmentNumber) {
  var returnVal = false;
  require(["t360/common"], function(common) {
    returnVal = common.setShipmentButtonPressed(button, formNum, shipmentNumber);
  });
  return returnVal;
}

function autoSaveForTimeOut(button, formNum) {
  require(["t360/common"], function(common) {
    common.autoSaveForTimeOut(button, formNum);
  });
}
  


// this will place help information into a popup window
function openHelp (url) {
  require(["t360/common"], function(common) {
    common.openHelp(url);
  });
}
function changeImage() {
//does nothing
}

function showReturn(arr) {
      
      document.getElementById('DataBuyerID').value=arr[1];
      console.log('data set');
}
function partyCallback(itemsArr,arr) {
      console.log("itemsArr[5]"+itemsArr[5]);
      console.log("itemsArr[44445]"+itemsArr[4]);
//    dijit.byId(itemsArr[0]).set('value',arr[1]);
//    dijit.byId(itemsArr[1]).set('value',arr[2]);
//    dijit.byId(itemsArr[2]).set('value',arr[3]);
//    dijit.byId(itemsArr[3]).set('value',arr[4]);
//    dijit.byId(itemsArr[4]).set('value',arr[5]);
//    dijit.byId(itemsArr[5]).set('value',arr[6]);
      this.matchPartyPatterns (itemsArr, arr);
      console.log('data set');
      
}
//"rowKey":"V8u6LIa7J94rCkZaQO7Bkg==","contact_name":"","contact_title":"","contact_phone":"","contact_fax":"","contact_email":"",
//"fax_1":"","fax_2":"","phone_number":"","PartyName":"Prateep","AddressLine1":"26/1","AddressLine2":"","AddressLine3":"","AddressLine4":"",
//"BranchCode":"","DesignatedBank":"","City":"Bangalore","Country":"India","VendorId":""}

// 
//
// 

function callAjaxToUpdateSection(){
	require(["dijit/registry","dojo/dom","t360/common"],
	function(registry,dom,common){
		common.callAjaxToUpdate("sectionLinksSection1", 
			    "/portal/common/SaveQuickLinkSession.jsp?sectionLinkVal="+dijit.byId("sectionLinksSection").open);
	});
}

function callAjaxToUpdateQuickLnk(){
	require(["dijit/registry","dojo/dom","t360/common"],
	function(registry,dom,common){
		common.callAjaxToUpdate("sidebarLinksSection1", 
			    "/portal/common/SaveQuickLinkSession.jsp?quickLinkVal="+dijit.byId("sidebarLinksSection").open);
	});
}

function matchPartyPatterns (arr1, arr2) {
      // arr1 - array of id from page
      // arr2 - array of values from grid
      // pattersns 
      var cols = ["rowKey","contact_name","contact_title","contact_phone","contact_fax","contact_email","fax_1","fax_2","phone_number","PartyName","AddressLine1","AddressLine2","AddressLine3","AddressLine4","BranchCode","DesignatedBank","City","Country","VendorId"];
      // extra patterns - 
      var patterns = ["r*key","*name*","*email*","*fax*1*","*fax*2*","*phone*","*add*1*","*add*2*","*add*3*","*add*4*","*p*code*","b*code*","*des*b*","*city*","*count*","*vend*"];
      var values = new Array();
      for (i =0; i < arr1.length; i++){
            var str = arr1[i];
            console.log('str='+str);
            for (j=0; j< patterns.length; j++) {
                  console.log('ptn='+patterns[j]);
                  var regEx = new RegExp(patterns[j],'i') ;
                  console.log('result for id='+result);
                  var result = regEx.test(str);
                  if (result) {
                        values[i]= '';
                        // find the best match in the columns
                        for (k=0; k< cols.length; k++){
                              console.log('column='+cols[k]);
                              var colName = cols[k];
                              var result1 = regEx.test(colName);
                              if (result1) {
                                    console.log('result for column='+result1);
                                    values[i] = arr2[k];
                              }
                        }
                  }
            }
      }
            
            for (i =0; i < arr1.length; i++){
                  console.log('setting '+arr1[i] + ' to '+ values[i]);
                  dijit.byId(arr1[i]).set('value',values[i]);
            }
            
      }

  function matchPartyPatternsByPageName (arr1, arr2, type) {
    // arr1 - array of id from page
    // arr2 - array of values from grid
    // type - pagename 

    //cquinton 2/8/2013 add variables for testing
    // and use dom, registry rather than dijit, dojo
    var myNode; //use to test dom.byId()
    var myWidget; //use to test registry.byId()
      
    require(["dojo/dom", "dijit/registry", "t360/textFormatter"], 
          function(dom, registry, txtFmt) {

       console.log('arr1-'+arr1);
       console.log('arr2-'+arr2);
       console.log('type-'+type);

       
       
       /*Prateep Gedupudi T36000014411 5/1/2013 conditions for specific sections of party search in instrument page Start*/
       if (type == 'seller' || type == 'payben'||type == 'drawee' || type == 'freightSection' || type == 'exportLc_assigne' || type == 'ben_detaild' || type == 'beneficiary' || type == 'importben' || type == 'borrower' || type == 'ben_rta' || type == 'ben_iss' ){
    	   
    	   if(document.getElementsByName("VendorId")[0]){
	    	  document.getElementsByName("VendorId")[0].value=arr2[18];
	       }
    	   
       } 
	   if(type=='benexp'){
		   if(document.getElementsByName("VendorIdExp")[0]){
		    	  document.getElementsByName("VendorIdExp")[0].value=arr2[18];
		       }
	   }
       if(type=='benimp'){
    	   if(document.getElementsByName("VendorIdImp")[0]){
 	    	  document.getElementsByName("VendorIdImp")[0].value=arr2[18];
 	       }
       }
       if(type=='sellerdetails'){
    	   if(document.getElementsByName("VendorId")[0]){
 	    	  document.getElementsByName("VendorId")[0].value=arr2[18];
 	       }
       }
       if(type=='bentrd'){
    	   if(document.getElementsByName("VendorId")[0]){
 	    	  document.getElementsByName("VendorId")[0].value=arr2[18];
 	       }
       }
       /*Prateep Gedupudi T36000014411 5/1/2013 conditions for specific sections of party search in instrument page End*/
       
       if (type == 'orderPar'){
             
             registry.byId(arr1[0]).set('value', arr2[9]);
             registry.byId(arr1[1]).set('value', arr2[10]);
             registry.byId(arr1[2]).set('value', arr2[11]);
             registry.byId(arr1[3]).set('value', arr2[12]);
             registry.byId(arr1[4]).set('value', arr2[16]);
             registry.byId(arr1[5]).set('value',arr2[17]);
             
       }
       if (type == 'orderPartyBank'){     
             registry.byId(arr1[5]).set('value', arr2[14]);
             registry.byId(arr1[6]).set('value', arr2[9]+"\n"+arr2[10]+","+arr2[11]+"\n"+arr2[16]+"\n"+arr2[17]);
             
             document.getElementsByName(arr1[0])[0].value=arr2[9];
             document.getElementsByName(arr1[1])[0].value=arr2[10];
             document.getElementsByName(arr1[2])[0].value=arr2[11];
             document.getElementsByName(arr1[3])[0].value=arr2[16];
             document.getElementsByName(arr1[4])[0].value=arr2[17];
             
       }
       if (type == 'orderPayer'){   
             registry.byId(arr1[0]).set('value', arr2[14]);
             registry.byId(arr1[1]).set('value', arr2[9]);
             registry.byId(arr1[2]).set('value', arr2[10]);
             registry.byId(arr1[3]).set('value', arr2[11]);
             registry.byId(arr1[4]).set('value', arr2[12]);
             registry.byId(arr1[5]).set('value', arr2[16]);
             registry.byId(arr1[6]).set('value', arr2[17]);
             registry.byId(arr1[7]).set('value', arr2[6]);
             registry.byId(arr1[8]).set('value', arr2[13]);
             
       }
      
       if (type == 'freightSection'){     
             registry.byId(arr1[0]).set('value', arr2[9]);
             registry.byId(arr1[1]).set('value', arr2[10]);
             registry.byId(arr1[2]).set('value', arr2[11]);
             registry.byId(arr1[3]).set('value', arr2[16]);
             registry.byId(arr1[4]).set('value', arr2[12]);
             registry.byId(arr1[5]).set('value', arr2[13]);
             registry.byId(arr1[6]).set('value', arr2[17]);
             registry.byId(arr1[7]).set('value', arr2[4]);           
             
       }
       
       if(type =='partydesibank'){
                  var bOid = arr2[0];
                  var partyTemp = arr2[9]+"\n"+arr2[10];
                  if(""!=arr2[11])
                	  partyTemp = partyTemp+"\n"+arr2[11];
                  
                  	  partyTemp = partyTemp+"\n"+arr2[16];
                  if(""!=arr2[12])
                	  partyTemp = partyTemp+","+arr2[12];
                  if(""!=arr2[13])
                	  partyTemp = partyTemp+" "+arr2[13];
                  if(""!=arr2[17])
                	  partyTemp = partyTemp+","+arr2[17];
                  
                  // registry.byId(arr1[0]).set('value', arr2[9]+"\n"+arr2[10]+"\n"+arr2[11]+"\n"+arr2[16]+","+arr2[12]+" "+arr2[13]+","+arr2[17]);
                  registry.byId(arr1[0]).set('value',partyTemp);
                  document.getElementsByName(arr1[1])[0].value = arr2[15];
                   require(["dojo/_base/xhr"],
                             function(xhr) {
                                  
                                 // Execute a HTTP GET request
                                 xhr.get({
                                     // The URL to request
                                     url: "/portal/common/decryptElement.jsp?encryptedStr="+bOid,
                                     // The method that handles the request's successful result
                                     // Handle the response any way you'd like!
                                     load: function(result) {
                                         //var n = result.replace("/","").trim();
                                    	 var index = result.indexOf("/");
                                         var oidVal = result.substr(0,index) ;
                                         dom.byId(arr1[1]).value = oidVal.trim();
                                     }
                                 });
                                  
                         });
       }
       
       if(type=='seller'|| type=='Instructions_guar'){
             
            
             registry.byId(arr1[0]).set('value', arr2[9]);
             registry.byId(arr1[1]).set('value', arr2[10]);
             registry.byId(arr1[2]).set('value', arr2[11]);
             registry.byId(arr1[3]).set('value', arr2[16]);
             registry.byId(arr1[4]).set('value', arr2[12]);
             registry.byId(arr1[5]).set('value', arr2[13]);
             registry.byId(arr1[6]).set('value', arr2[17]);
             registry.byId(arr1[7]).set('value', arr2[8]);
	     if (type=='seller')
	        registry.byId(arr1[8]).set('value', arr2[20]);
             
             
       }
      
       if(type=='ben_iss'){
             registry.byId(arr1[0]).set('value', arr2[9]);
             registry.byId(arr1[1]).set('value', arr2[10]);
             registry.byId(arr1[2]).set('value', arr2[11]);
             registry.byId(arr1[3]).set('value', arr2[16]);
             registry.byId(arr1[4]).set('value', arr2[12]);
             registry.byId(arr1[5]).set('value', arr2[13]);
             registry.byId(arr1[6]).set('value', arr2[17]);
             registry.byId(arr1[7]).set('value', arr2[8]);
             if (arr1.length >= 9)
            	 registry.byId(arr1[8]).set('value', arr2[20]);
            
       }
     //02-feb-2013. IR-T36000010953. Modified for extra params to populate designated bank info into collecting bank
       if(type =='drawee'){
           registry.byId(arr1[0]).set('value', arr2[9]);
           registry.byId(arr1[1]).set('value', arr2[10]);
           registry.byId(arr1[2]).set('value', arr2[11]);
           registry.byId(arr1[3]).set('value', arr2[16]);
           registry.byId(arr1[4]).set('value', arr2[12]);
           registry.byId(arr1[5]).set('value', arr2[13]);
           registry.byId(arr1[6]).set('value', arr2[17]);
           registry.byId(arr1[7]).set('value', arr2[8]);
           if (arr1.length >= 9){
          	 document.getElementsByName(arr1[8])[0].value=arr2[20];
           }
          	 //registry.byId(arr1[8]).set('value', arr2[20]);
	       if(arr2[21] != ''){
       	           require(["dojo/_base/xhr"],
       	                   function(xhr) {
       	                        
       	                       // Execute a HTTP GET request
       	          	 		xhr.get({
       	                           // The URL to request
       	                           url: "/portal/transactions/Search-PartyDetails.jsp?partyoid="+arr2[21],	                            
       	                           preventCache: true,
       	                           handleAs: "json",
       	                           load: function(result) {
       	                        	   console.log(result);
       	                        	    document.getElementsByName(arr1[9])[0].value=arr2[21];
       		                            document.getElementsByName(arr1[10])[0].value=result.name;
       		                            document.getElementsByName(arr1[11])[0].value=result.address1;
       		                            document.getElementsByName(arr1[12])[0].value=result.address2;
       		                            document.getElementsByName(arr1[13])[0].value=result.address3;
       		                            document.getElementsByName(arr1[14])[0].value=result.city;
       		                            document.getElementsByName(arr1[16])[0].value=result.countrycode;
       		                            document.getElementsByName(arr1[17])[0].value=result.postalcode;
       		                            
       		                            registry.byId(arr1[19]).set('value', result.name+"\n"+result.address1+","+result.address2+"\n"+result.city+","+result.address3+"\n"+result.country);
       	                             },
       	              			 error: function(error){
       	              			    //todo
       	              			}
       	                       }); 
       	               }); 
	          }
     }


       
       if(type=='importben'|| type=='ben_rta'){
    	   
    	   registry.byId(arr1[0]).set('value', arr2[9]);
           registry.byId(arr1[1]).set('value', arr2[10]);
           registry.byId(arr1[2]).set('value', arr2[11]);
           registry.byId(arr1[3]).set('value', arr2[16]);
           registry.byId(arr1[4]).set('value', arr2[12]);
           registry.byId(arr1[5]).set('value', arr2[13]);
           registry.byId(arr1[6]).set('value', arr2[17]);
           registry.byId(arr1[7]).set('value', arr2[8]);
           
           
	       console.log('DesignatedBank : '+arr2[21]);	       
	      
	       if(arr2[21] != ''){
	           require(["dojo/_base/xhr"],
	                   function(xhr) {
	                        
	                       // Execute a HTTP GET request
	          	 		xhr.get({
	                           // The URL to request
	                           url: "/portal/transactions/Search-PartyDetails.jsp?partyoid="+arr2[21],	                            
	                           preventCache: true,
	                           handleAs: "json",
	                           load: function(result) {
	                        	   console.log(result);
	                        	 
	                        	   document.getElementsByName(arr1[8])[0].value=arr2[21];
		                            document.getElementsByName(arr1[9])[0].value=result.name;
		                            document.getElementsByName(arr1[10])[0].value=result.address1;
		                            document.getElementsByName(arr1[11])[0].value=result.address2;
		                            document.getElementsByName(arr1[12])[0].value=result.city;
		                            document.getElementsByName(arr1[13])[0].value=result.address3;
		                            document.getElementsByName(arr1[14])[0].value=result.countrycode;
		                            document.getElementsByName(arr1[15])[0].value=result.postalcode;
		                            
		                            registry.byId(arr1[16]).set('value', result.name+"\n"+result.address1+","+result.address2+"\n"+result.city+","+result.address3+"\n"+result.country);
	                             },
	              			 error: function(error){
	              			    //todo
	              			}
	                       }); 
	                       
	               }); 
	       }
       }

       if( type=='applicant'||type=='advisingbank'|| type=='applicant_rta'|| type=='app_bank_rta'|| 
    		   type=='advisethroughbank'|| type=='reimbursing'|| type=='borrower' || type=='insured'|| type=='applicantoutgoing'||type=='corres_slc'){
            
             //cquinton 2/8/2013 format address correctly
             var partyAddr = txtFmt.formatPartyAddress( 
               arr2[9], //party name
               arr2[10], //addr1
               arr2[11], //addr2
               arr2[16], //city
               arr2[12], //stateProvince
               arr2[13], //zip
               arr2[17] ); //country
             registry.byId(arr1[8]).set('value', partyAddr);
             
             document.getElementsByName(arr1[0])[0].value=arr2[0];
             document.getElementsByName(arr1[1])[0].value=arr2[9];
             document.getElementsByName(arr1[2])[0].value=arr2[10];
             document.getElementsByName(arr1[3])[0].value=arr2[11];
             document.getElementsByName(arr1[4])[0].value=arr2[16];
             document.getElementsByName(arr1[5])[0].value=arr2[12];
             document.getElementsByName(arr1[6])[0].value=arr2[17];
             document.getElementsByName(arr1[7])[0].value=arr2[13];
        	 if (type =='advisingbank'){
                 document.getElementsByName(arr1[9])[0].value=arr2[20];  
            	 console.log ("advising bank setting OTLID to" + arr2[20] );
        	 }
        }
       if(type=='drawer'){
             registry.byId(arr1[0]).set('value', arr2[9]+"\n"+arr2[10]+","+arr2[11]+"\n"+arr2[16]+","+arr2[12]+"\n"+arr2[17]);
       }
       if(type=='beneficiary'|| type=='beneficiary_bank'){
             
             registry.byId(arr1[0]).set('value', arr2[9]);
             registry.byId(arr1[1]).set('value', arr2[10]);
             registry.byId(arr1[2]).set('value', arr2[11]);
             registry.byId(arr1[3]).set('value', arr2[16]);
             registry.byId(arr1[4]).set('value', arr2[12]);
             registry.byId(arr1[5]).set('value', arr2[17]);
             registry.byId(arr1[6]).set('value', arr2[13]);
             
       }
       if(type=='bbkexp' || type=='bbkimp'|| type=='sellerbank'||type=='releasetopartySection'){
             
             registry.byId(arr1[0]).set('value', arr2[9]);
             registry.byId(arr1[1]).set('value', arr2[10]);
             registry.byId(arr1[2]).set('value', arr2[11]);
             registry.byId(arr1[3]).set('value', arr2[16]);
             registry.byId(arr1[4]).set('value', arr2[12]);
             registry.byId(arr1[5]).set('value', arr2[13]);
             registry.byId(arr1[6]).set('value', arr2[17]);
             
       }
       if( type=='benexp' || type=='benimp'|| type=='sellerdetails' ){
    	   
	       dijit.byId(arr1[0]).set('value', arr2[9]);
	       dijit.byId(arr1[1]).set('value', arr2[10]);
	       dijit.byId(arr1[2]).set('value', arr2[11]);
	       dijit.byId(arr1[3]).set('value', arr2[16]);
	       dijit.byId(arr1[4]).set('value', arr2[12]);
	       dijit.byId(arr1[5]).set('value', arr2[13]);
	       dijit.byId(arr1[6]).set('value', arr2[17]);
	       dijit.byId(arr1[7]).set('value', arr2[20]);
	       
	       console.log('DesignatedBank : '+arr2[21]);	       
		      
	       if(arr2[21] != ''){
	           require(["dojo/_base/xhr"],
	                   function(xhr) {
	                        
	                       // Execute a HTTP GET request
	          	 		dojo.xhrGet({
	                           // The URL to request
	                           url: "/portal/transactions/Search-PartyDetails.jsp?partyoid="+arr2[21],	                            
	                           preventCache: true,
	                           handleAs: "json",
	                           load: function(result) {
	                        	   console.log(result);
	                        	  
		                           dijit.byId(arr1[8]).set('value', result.name);
		                 	       dijit.byId(arr1[9]).set('value', result.address1);
		                 	       dijit.byId(arr1[10]).set('value', result.address2);
		                 	       dijit.byId(arr1[11]).set('value', result.city);
		                 	       dijit.byId(arr1[12]).set('value', result.address3);
		                 	       dijit.byId(arr1[13]).set('value', result.postalcode);
		                 	       dijit.byId(arr1[14]).set('value', result.countrycode);
		                 	       dijit.byId(arr1[15]).set('value', result.otlcustomerid);
	                             },
	              			 error: function(error){
	              			    //todo
	              			}
	                       }); 
	                       
	               }); 
	       }
       
 		}
       
  //RKAZI CR709 Rel 8.2 01/25/2013 -Start
     if(type=='bentrd'){

         dijit.byId(arr1[0]).set('value', arr2[9]);
         dijit.byId(arr1[1]).set('value', arr2[10]);
         dijit.byId(arr1[2]).set('value', arr2[11]);
         dijit.byId(arr1[3]).set('value', arr2[12]);
         dijit.byId(arr1[4]).set('value', arr2[16]);
         dijit.byId(arr1[5]).set('value', arr2[17]);
         dijit.byId(arr1[6]).set('value', arr2[20]);
	       console.log('DesignatedBank : '+arr2[21]);	       
		      
	       if(arr2[21] != ''){
	           require(["dojo/_base/xhr"],
	                   function(xhr) {
	                        
	                       // Execute a HTTP GET request
	          	 		dojo.xhrGet({
	                           // The URL to request
	                           url: "/portal/transactions/Search-PartyDetails.jsp?partyoid="+arr2[21],	                            
	                           preventCache: true,
	                           handleAs: "json",
	                           load: function(result) {
	                        	   console.log(result);
	                        	   dijit.byId(arr1[7]).set('value', result.branchcode);
		                           dijit.byId(arr1[8]).set('value', result.name);
		                 	       dijit.byId(arr1[9]).set('value', result.address1);
		                 	       dijit.byId(arr1[10]).set('value', result.address2);
		                 	       dijit.byId(arr1[11]).set('value', result.city);
		                 	       dijit.byId(arr1[12]).set('value', result.address3);
		                 	       dijit.byId(arr1[13]).set('value', result.countrycode);
		                 	       dijit.byId(arr1[14]).set('value', result.otlcustomerid);
	                             },
	              			 error: function(error){
	              			    //todo
	              			}
	                       }); 
	                       
	               }); 
	       }

     }
     if(type=='bbktrd' ){

    	 /*
         dijit.byId(arr1[0]).set('value', arr2[9]);
         dijit.byId(arr1[1]).set('value', arr2[10]);
         dijit.byId(arr1[2]).set('value', arr2[11]);
         dijit.byId(arr1[3]).set('value', arr2[16]);
         dijit.byId(arr1[4]).set('value', arr2[12]);
         dijit.byId(arr1[5]).set('value', arr2[17]);
         */
    	 
    	 dijit.byId(arr1[0]).set('value', arr2[3]);
    	 dijit.byId(arr1[1]).set('value', arr2[4]);
         dijit.byId(arr1[2]).set('value', arr2[6]);
         dijit.byId(arr1[3]).set('value', arr2[7]);
         dijit.byId(arr1[4]).set('value', arr2[8]);
         dijit.byId(arr1[5]).set('value', arr2[9]);
         dijit.byId(arr1[6]).set('value', arr2[1]);
        // dijit.byId(arr1[7]).set('value', arr2[20]);

     }
  //RKAZI CR709 Rel 8.2 01/25/2013 -End
if(type=='payben'){
             
             registry.byId(arr1[0]).set('value', arr2[9]);
             registry.byId(arr1[1]).set('value', arr2[10]);
             registry.byId(arr1[2]).set('value', arr2[11]);
             registry.byId(arr1[3]).set('value', arr2[16]);
             registry.byId(arr1[4]).set('value', arr2[13]);
             registry.byId(arr1[5]).set('value', arr2[17]);
             registry.byId(arr1[6]).set('value', arr2[4]);
             registry.byId(arr1[6]).set('value', arr2[5]);
			 
             dom.byId(arr1[8]).value= arr2[0];
             
       }
       if(type=='notify'){
             registry.byId(arr1[0]).set('value', arr2[9]);
             registry.byId(arr1[1]).set('value', arr2[10]);
             registry.byId(arr1[2]).set('value', arr2[16]);
             registry.byId(arr1[3]).set('value', arr2[13]);
             
       }
       if(type=='ben_detaild'){
             
             registry.byId(arr1[0]).set('value', arr2[9]);
             registry.byId(arr1[1]).set('value', arr2[10]);
             registry.byId(arr1[2]).set('value', arr2[11]);
             registry.byId(arr1[3]).set('value', arr2[16]);
             registry.byId(arr1[4]).set('value', arr2[12]);
             registry.byId(arr1[5]).set('value', arr2[13]);
             registry.byId(arr1[6]).set('value', arr2[17]);
             registry.byId(arr1[7]).set('value', arr2[3]);
             
       }
       if( type=='applicant_guar' || type=='applicant_slc'|| type=='exp_col_drawer'){
                  
             //cquinton 2/8/2013 format address correctly
             var partyAddr = txtFmt.formatPartyAddress( 
               arr2[9], //party name
               arr2[10], //addr1
               arr2[11], //addr2
               arr2[16], //city
               arr2[12], //stateProvince
               arr2[13], //zip
               arr2[17] ); //country
             registry.byId(arr1[9]).set('value', partyAddr);
             
             document.getElementsByName(arr1[0])[0].value=arr2[0];
             document.getElementsByName(arr1[1])[0].value=arr2[9];
             document.getElementsByName(arr1[2])[0].value=arr2[10];
             document.getElementsByName(arr1[3])[0].value=arr2[11];
             document.getElementsByName(arr1[4])[0].value=arr2[16];
             document.getElementsByName(arr1[5])[0].value=arr2[17];
             document.getElementsByName(arr1[6])[0].value=arr2[12];
             document.getElementsByName(arr1[7])[0].value=arr2[13];
             document.getElementsByName(arr1[8])[0].value=arr2[8];
       }
       if(type=='agent_guar'){
             registry.byId(arr1[0]).set('value', arr2[9]);
             registry.byId(arr1[1]).set('value', arr2[1]);
             registry.byId(arr1[2]).set('value', arr2[10]);
             registry.byId(arr1[3]).set('value', arr2[11]);
             registry.byId(arr1[4]).set('value', arr2[16]);
             registry.byId(arr1[5]).set('value', arr2[12]);
             registry.byId(arr1[6]).set('value', arr2[13]);
             registry.byId(arr1[7]).set('value', arr2[17]);
             registry.byId(arr1[8]).set('value', arr2[8]);
            
       }
       if(type=='notifyparty_imp'|| type=='consigneeparty_imp'||type=='notifyparty_atp'|| type=='consigneeparty_atp'){
             registry.byId(arr1[0]).set('value', arr2[9]);
             registry.byId(arr1[1]).set('value', arr2[10]);
             registry.byId(arr1[2]).set('value', arr2[13]);
             registry.byId(arr1[3]).set('value', arr2[16]+","+arr2[17]);
           
       
       }
       if(type=='need_of_contact'){
    	   
    	   registry.byId(arr1[0]).set('value', arr2[9]);
           registry.byId(arr1[1]).set('value', arr2[10]);
           registry.byId(arr1[2]).set('value', arr2[11]);
           registry.byId(arr1[3]).set('value', arr2[16]+","+arr2[12]+" "+arr2[13]+" "+arr2[17]);  
       }
       if(type=='direct_send_collecting_bank' || type=='direct_send_collecting_bank_two'){
             
             //cquinton 2/8/2013 format address correctly
             var partyAddr = txtFmt.formatPartyAddressWithPhone( 
               arr2[9], //party name
               arr2[10], //addr1
               arr2[11], //addr2
               arr2[16], //city
               arr2[12], //stateProvince
               arr2[13], //zip
               arr2[19], //country
               arr2[8]); //phone
            
             registry.byId(arr1[10]).set('value', partyAddr);
            
             document.getElementsByName(arr1[0])[0].value=arr2[0];
             document.getElementsByName(arr1[1])[0].value=arr2[9];
             document.getElementsByName(arr1[2])[0].value=arr2[10];
             document.getElementsByName(arr1[3])[0].value=arr2[11];
             document.getElementsByName(arr1[4])[0].value=arr2[14];
             document.getElementsByName(arr1[5])[0].value=arr2[16];
             document.getElementsByName(arr1[6])[0].value=arr2[12];
			 document.getElementsByName(arr1[7])[0].value=arr2[17];
             document.getElementsByName(arr1[8])[0].value=arr2[13];
             document.getElementsByName(arr1[9])[0].value=arr2[8];
             document.getElementsByName(arr1[11])[0].value=arr2[20];
             
       }
       /*IR #14533 start
        * Populate Assignee's Bank details along with Assignee details*/
       if(type=='exportLc_assigne'){
    	   
    	   registry.byId(arr1[0]).set('value', arr2[9]);
           registry.byId(arr1[1]).set('value', arr2[10]);
           registry.byId(arr1[2]).set('value', arr2[11]);
           registry.byId(arr1[3]).set('value', arr2[16]);
           registry.byId(arr1[4]).set('value', arr2[12]);
           registry.byId(arr1[5]).set('value', arr2[13]);
           registry.byId(arr1[6]).set('value', arr2[17]);
           registry.byId(arr1[7]).set('value', arr2[8]);
           registry.byId(arr1[8]).set('value', arr2[1]);
           
           console.log('DesignatedBank : '+arr2[21]);	       
 	      
	       if(arr2[21] != ''){
	           require(["dojo/_base/xhr"],
	                   function(xhr) {
	                        
	                       // Execute a HTTP GET request
	          	 		xhr.get({
	                           // The URL to request
	                           url: "/portal/transactions/Search-PartyDetails.jsp?partyoid="+arr2[21],	                            
	                           preventCache: true,
	                           handleAs: "json",
	                           load: function(result) {
	                        	   console.log(result);
	                        	 		                            
		                            registry.byId(arr1[9]).set('value', result.name);
		                            registry.byId(arr1[10]).set('value', result.address1);
		                            registry.byId(arr1[11]).set('value', result.address2);
		                            registry.byId(arr1[12]).set('value', result.city);
		                            registry.byId(arr1[13]).set('value', result.address3);
		                            registry.byId(arr1[14]).set('value', result.postalcode);
		                            registry.byId(arr1[15]).set('value', result.countrycode);
		                            
		                            //ASSIGNEE_BANK
		                            document.getElementsByName(arr1[16])[0].value='ASB';
		                            
	                             },
	              			 error: function(error){
	              			    //todo
	              			}
	                       }); 
	                       
	               }); 
	       }
    	   
       }/*IR #14533 END*/
       
       /*IR #14535 Start
        * Populate Transferee Bank details along with Transferee details*/
       if(type=='exportLc_transferee'){
    	   
    	   registry.byId(arr1[0]).set('value', arr2[9]);
             registry.byId(arr1[1]).set('value', arr2[10]);
             registry.byId(arr1[2]).set('value', arr2[11]);
             registry.byId(arr1[3]).set('value', arr2[16]);
             registry.byId(arr1[4]).set('value', arr2[12]);
             registry.byId(arr1[5]).set('value', arr2[13]);
             registry.byId(arr1[6]).set('value', arr2[17]);
             registry.byId(arr1[7]).set('value', arr2[8]);
             registry.byId(arr1[8]).set('value', arr2[1]);
             
             /*Rakesh IR #T36000018053 Start*/ 
        	   if(document.getElementsByName("VendorId")[0]){
    	    	  document.getElementsByName("VendorId")[0].value=arr2[18];
    	       }
        	  /*Rakesh IR #T36000018053 End*/ 
             console.log('DesignatedBank : '+arr2[21]);	       
   	      
  	       if(arr2[21] != ''){
  	           require(["dojo/_base/xhr"],
  	                   function(xhr) {
  	                        
  	                       // Execute a HTTP GET request
  	          	 		xhr.get({
  	                           // The URL to request
  	                           url: "/portal/transactions/Search-PartyDetails.jsp?partyoid="+arr2[21],	                            
  	                           preventCache: true,
  	                           handleAs: "json",
  	                           load: function(result) {
  	                        	   console.log(result);
  	                        	   
  	                        	   document.getElementsByName(arr1[9])[0].value=arr2[21];
  		                            document.getElementsByName(arr1[10])[0].value=result.name;
  		                            document.getElementsByName(arr1[11])[0].value=result.address1;
  		                            document.getElementsByName(arr1[12])[0].value=result.address2;
  		                            document.getElementsByName(arr1[13])[0].value=result.city;
  		                            document.getElementsByName(arr1[14])[0].value=result.address3;
  		                            document.getElementsByName(arr1[15])[0].value=result.countrycode;
  		                            document.getElementsByName(arr1[16])[0].value=result.postalcode;
  		                            
  		                            registry.byId(arr1[17]).set('value', result.name+"\n"+result.address1+","+result.address2+"\n"+result.city+","+result.address3+"\n"+result.country);
  	                             
  		                            //TRANSFEREE_NEGOT
		                            document.getElementsByName(arr1[18])[0].value='TNB';
  	                           
  	                           },
  	              			 error: function(error){
  	              			    //todo
  	              			}
  	                       }); 
  	                       
  	               }); 
  	       }
             
       }/*IR #14535 End*/
       if(type=='exportLc_assigneeBank'){
             
             registry.byId(arr1[0]).set('value', arr2[9]);
             registry.byId(arr1[1]).set('value', arr2[10]);
             registry.byId(arr1[2]).set('value', arr2[11]);
             registry.byId(arr1[3]).set('value', arr2[16]);
             registry.byId(arr1[4]).set('value', arr2[12]);
             registry.byId(arr1[5]).set('value', arr2[13]);
             registry.byId(arr1[6]).set('value', arr2[17]);
             
       }
       
             /*if(type=='drawer'){
             registry.byId(arr1[9]).set('value', arr2[9]+"\n"+arr2[10]+","+arr2[11]+"\n"+arr2[16]+","+arr2[12]+"\n"+arr2[17]+","+arr2[13]","+arr1[8]);
             console.log(registry.byId(arr1[8]));
             document.getElementsByName(arr1[0])[0].value=arr2[0];
             document.getElementsByName(arr1[1])[0].value=arr2[9];
             document.getElementsByName(arr1[2])[0].value=arr2[10];
             document.getElementsByName(arr1[3])[0].value=arr2[11];
             document.getElementsByName(arr1[4])[0].value=arr2[16];
             document.getElementsByName(arr1[5])[0].value=arr2[12];
             document.getElementsByName(arr1[6])[0].value=arr2[17];
             document.getElementsByName(arr1[7])[0].value=arr2[13];
             document.getElementsByName(arr1[8])[0].value=arr2[8]
              }*/
       
       
       /******************Bank Search**********************/
       
       if(type=='banksection_ddi'|| type=='payments_banksearch'|| type=='payments_firstintermediarybank'){
             
             //cquinton 2/18/2013 ir#11444 truncate bank name to 35 chars to
             // fit in domestic payment field
             if ( arr2[4] && arr2[4].length>35 ) {
               arr2[4] = arr2[4].substring(0,35);
             }

             registry.byId(arr1[6]).set('value', arr2[3]);
             //cquinton 2/8/2013 ir#11444 only set BankCode if available
             //ddi uses, payments do not
             myNode = dom.byId(arr1[8]);
             if ( myNode ) {
               myNode.value=arr2[3];
             }
            
             var bankAddr = txtFmt.formatBankAddress(
               arr2[4], //bank name
               arr2[5], //bank branch name
               arr2[6], //address 1
               arr2[7], //address 2
               arr2[8], //city
               arr2[9], //state
               null,    //zip
               arr2[1]); //country

             var addr3 = txtFmt.formatAddressLine3(
               arr2[8], //city
               arr2[9], //state
               null,    //zip
               arr2[1]); //country

             myWidget = registry.byId(arr1[7]);
             if ( myWidget ) {
               myWidget.set('value', bankAddr);
             }

             document.getElementsByName(arr1[0])[0].value=arr2[4]; //bank name
             document.getElementsByName(arr1[1])[0].value=arr2[6]; //address 1
             document.getElementsByName(arr1[2])[0].value=arr2[7]; //address 2
             document.getElementsByName(arr1[3])[0].value=addr3;
             document.getElementsByName(arr1[4])[0].value=arr2[1]; //country
             document.getElementsByName(arr1[5])[0].value=arr2[5]; //bank branch name
			 
			 if(type=='payments_firstintermediarybank'){
			  document.getElementsByName(arr1[9])[0].value=arr2[0];
			 }
            
       }
       if(type=='InstrumentId' || type=='InstrumentId_1' || type=='InstrumentId_2'){
             registry.byId(arr1[0]).set('value', arr2[1]);
             
       }

    });
  }
