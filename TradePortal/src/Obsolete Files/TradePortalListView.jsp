<%--
**********************************************************************************
                            Trade Portal List View

  Description:
      This jsp provides common logic for creating the Trade Portal list view.  To
      use this file, call the jsp using the following code.

        <jsp:include page="TradePortalListView.jsp">
          <jsp:param name="listView" value="listview.xml"/>
          <jsp:param name="whereClause2" value="additional where conditions"/>
        </jsp:include>

      The "listview.xml" parameter is the name of a list view xml parameter file
      that is stored in the mapfiles directory (the configFileDir parm in 
      AmsServletConfig).  The xml file is described in the ListViewManager class.
      "additional where conditions" is extra where clause conditions that are
      appended to the SQL created from the SQL parameters in the list view xml 
      parameter file.  It allows dynamic criteria to be used in addition to the
      static criteria in the listview xml file.

  Request Parameters:
      (when linking from same listview page)
      listView      - Name of the parameter file that drives the list view.
                      This is required.
      whereClause2  - Additional 'where' criteria.
                      This is optional and defaults to blank
      startRow      - Zero based starting row for first row to display.
                      This is optional and defaults to 0.
      sortColumn    - Zero based column number indicates which column to sort on.
                      This is optional and defaults to 0.
      sortOrder     - Indicates sort order: A for ascending, D for descending.
                      This is optional and defaults to A
      (when linking from other than same listview page)
      listView      - Name of the parameter file that drives the list view.
                      This is required.
      whereClause2  - Additional 'where' criteria.
                      This is optional and defaults to blank
	  searchCriteria- Search criteria that is saved in List View State.
                      This is optional and defaults to blank
	  searchType	- Advanced or Basic search - this is saved in
	  				  ListViewState.
                      This is optional and defaults to blank	  				
      [the startRow, sortColumn, and sortOrder are retrieved from the session]


  Response Parameters:
     (when linking back to same listview page)
      listView      - Name of the parameter file that drives the list view.
      whereClause2  - Additional 'where' criteria.
      startRow      - Zero based starting row for first row to display.
      sortColumn    - Zero based column number indicates which column to sort on.
      sortOrder     - Indicates sort order: A for ascending, D for descending.

     (when linking to detail page)
      oid           - Indicates the oid value for the selected row
**********************************************************************************
--%>

<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
com.amsinc.ecsg.web.*,com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,com.ams.tradeportal.html.*,java.text.*, java.util.*,com.businessobjects.rebean.wi.*,com.crystaldecisions.sdk.framework.*,
com.crystaldecisions.sdk.exception.*,com.ams.tradeportal.busobj.util.*" %>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
</jsp:useBean>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session">
  <%
    resMgr.init();
  %>
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" 
   scope="session">
</jsp:useBean>

<%
  String ua = request.getHeader( "User-Agent" );
  boolean isFirefox = ( ua != null && ua.indexOf( "Firefox/" ) != -1 );
  boolean isMSIE = ( ua != null && ua.indexOf( "MSIE" ) != -1 );
%>

<%
//TLE- 09/19/06 - CR379 - Add Begin
   String               relatedInstrumentId  = null;
   String               columnName           = null;
   String               value                = null;
//TLE- 09/19/06 - CR379 - Add End
%>


<script language="JavaScript">

  <%--
  //TLE - 09/13/06 - CR362 - Add Begin
  // This function is used to check and uncheck checkboxes when
  // the Select All checkbox is selected or deselected by the user
  --%>
  function selectAll(formNum){
	
     var numElements = document.forms[formNum].elements.length;
	
     var i = 0;
     for (i = 0; i < numElements; i++) {
	
       if (document.forms[formNum].elements[i].type == 'checkbox') {
			if (document.forms[formNum].sAll.checked == true){  
             if (document.forms[formNum].elements[i].name != 'sAll') {   
                document.forms[formNum].elements[i].checked = true;
             }
       } 
       else
       {
             if (document.forms[formNum].elements[i].name != 'sAll') {   
                document.forms[formNum].elements[i].checked = false;
             }
       }
       }
     }   
  }
  <%-- TLE - 09/13/06 - CR362 - Add End --%>

</script>


<%
//XI R2 Migration WISession webiSession = null;
IEnterpriseSession boSessionLocal = (IEnterpriseSession) request.getSession().getAttribute("CE_ENTERPRISESESSION");

String repFlag = request.getParameter("reportFlag"); 

//Commented the check to resolve TP listview issues
//if (repFlag != null && repFlag.equals("Y"))
//{
%>

<%-- PPX-051 - rbhaduri - 19 May 09 <%@ include file="/reports/fragments/wistartpage.frag" %> --%>


<%
//}

  //******************************************************************************
  // Step 1: Load the parameters for the list view.  This includes all retrieving
  // The list view parameters from the list view manager and extracting several
  // parameters from the request.  We use the ListViewHandler to manage all this
  // data for us.
  //******************************************************************************

  String listView = request.getParameter("listView");
//Validate XSS	
if(listView != null){
		listView = StringFunction.xssCharsToHtml(listView);
	}

  if (listView == null) System.out.println("No listView parameter given!!!");
  
  //reportFlag is set if TradePortalListView is called
  //from reporting infrastructure.
  String reportFlag = request.getParameter("reportFlag"); 
  
  // Set reports flag to N by default.
  if(reportFlag==null) reportFlag = "N";

   

  ListViewHandler lvHandler =null;/* new ListViewHandler(listView,
                                                  request,
                                                  response,
                                                  session,
                                                  userSession,
                                                  resMgr, 
                                                  formMgr);*/

  // Having the columns local to the page makes access easier.
  ListViewManager.ListViewColumn columns[];
  columns = lvHandler.getColumns();

  //******************************************************************************
  // Step 2: Display the header.  Each column prints within a <td> tag.  The
  // column data consists of the translated column name.  Each name is a link back
  // to this same page and includes several parameters to redisplay the data
  // correctly.  The current sort column also displays with an up or down arrow
  // indicating the current sort order.
  //******************************************************************************
 
  // Both the header and the data rows display within a table.  Start the
  // table here.
  int columnCount = lvHandler.getColumnCount();
  if (lvHandler.showCheckBox()) columnCount++;
  if (lvHandler.showRadioButton()) columnCount++;

  String maxScrollHeight = request.getParameter("maxScrollHeight");
  String absScrollHeight = request.getParameter("absoluteScrollHeight");
  String divStyle = "";
  if(absScrollHeight != null){
	  absScrollHeight = StringFunction.xssCharsToHtml(absScrollHeight);
	  }
  
  if(absScrollHeight != null && !"".equals(absScrollHeight)){
    divStyle = "style='height: " + StringFunction.escapeQuotesforJS(absScrollHeight) + "px; overflow: auto;'";
  } 
  
%>
  <div id="<%=StringFunction.escapeQuotesforJS(listView)%>ScrollableDiv" <%=divStyle%>>
  <table cellspacing=2 cellpadding=2 border=0 width=100% name="listViewDataTable">
    <thead>
<% if (lvHandler.getSortColumn() != -1) { %>
    <tr>
      <td width=5 nowrap>&nbsp;</td>
      <td class=ColorBeige nowrap colspan=<%=columnCount%>>
        <span class=ListText>
          <%=resMgr.getText("listview.ToSort", TradePortalConstants.TEXT_BUNDLE)%>
        </span>
      </td>
      <td width=100%>&nbsp;</td>
    </tr>
<% } %>
    <tr>
      <td width=5 nowrap>&nbsp;</td>
<%
      //TLE - 09/13/06 - CR362 - Add Select All checkbox
      if (lvHandler.showCheckBox()) {
         if (lvHandler.showSelectAllCheckBox()){
         	out.println("<td class=ColorBeige width=24> <input type=\"checkbox\" name=\"sAll\" value=\"select\" onClick=\"selectAll(0)\"> </td>");
         }
         else {
                out.println("<td class=ColorBeige width=24></td>");
         }

      }

      if (lvHandler.showRadioButton()) {
         out.println("<td class=ColorBeige style=\"width: " + (isMSIE ? "1%" : "24") + ";\">&nbsp;</td>");
      }
      for (int i=0; i < lvHandler.getColumnCount(); i++) {
		if (columns[i].displayColumn(userSession.getDisplayExtendedListview()) == true  && columns[i].isColumnDefined() ) {  		//IR# PRUL110773050  Rel7.1.0  - 

	         String style = null;
	         if (columns[i].isColumnHidden()) {
	            style="style=visibility:hidden;display:none";
	         }

				out.println("<td name=HeaderColumn class=ColorBeige aligh=left nowrap " + style + " ><span class=ColumnHeads " + style + " >");
				out.println(lvHandler.getColumnHeaderWithSort(columns[i].columnKey,
															  "ListHeaderText"));
				out.println("</span></td>");
		}
	}  // end for 
%>
      <td width=100%>&nbsp;</td>
    </tr>
    </thead>
    
    <tbody >
<%
  //******************************************************************************
  // Step 3: Retrieve the data.  
  //******************************************************************************

  //getData(WISession) is called if reportFlag is set to Y.
  //By default getData() is called when the flag is not set.
  if( reportFlag.equals("Y") )
  {
     if(boSessionLocal != null )
      {
  	  lvHandler.getData(boSessionLocal);     
      }  

  } 
  else {
      if (request.getParameter("documentContent") == null || request.getParameter("documentContent").equals("")) {
  	  lvHandler.getData();
      } else {
  	  String documentContent = request.getParameter("documentContent");
  	  DocumentHandler docHandler = new DocumentHandler(documentContent,false);
  	  lvHandler.setIsResultFromQuery(false);
  	  lvHandler.getData(docHandler);
      }
  }  
  

  //******************************************************************************
  // Step 4: Print the list view rows.  First print the checkbox column (if
  // the list view parms indicate to do so).  Then print the data for each column,
  // formatting the data as necessary.  Each column's data prints within its own 
  // table data cell.  Each row prints in alternating background colors.
  //******************************************************************************

  int numItems = lvHandler.getRetrievedRecordCount();
  String colorTag = "";

  if (numItems == 0) {
%>
    <tr name="NoRowsFoundRow">
      <td></td>
      <td colspan=<%=lvHandler.getColumnCount()%>>
        <span class=ListText>
          <%=resMgr.getText("listview.norows", TradePortalConstants.TEXT_BUNDLE)%>
        </span>
      </td>
    </tr>
<%
  }

  for (int i=0; i < numItems; i++) {
      // The background color of the rows alternates colors.
      if ( (i/2) != (i/2.0) ) {
          colorTag = "class=ColorGrey";
      } else {
          colorTag = "";
      }
%>
      <tr name="row" id="row<%=i%>">
        <td>&nbsp;</td>
<%
      // Display the checkbox if necessary
      if (lvHandler.showCheckBox()) {
%>
       <td width=24 <%=colorTag%>>
          <%=lvHandler.getCheckBoxField("checkbox"+i, i)%>
       </td>
<%
      }

      // Display the radiobutton if necessary
      if (lvHandler.showRadioButton()) {
            //use radioButtonName from passed in parameter if provided
    	  String radioButtonName = request.getParameter("radioButtonName"); 
    	  if (InstrumentServices.isBlank(radioButtonName)) {
    		  radioButtonName = "selection";
    	  }
    	  
    	  String radioButtonSelectedValue = request.getParameter("radioButtonSelectedValue");
    	  String radionButtonExtraTag = request.getParameter("radionButtonExtraTag");
%>
       <td <%=colorTag%>>
          <%=lvHandler.getRadioButtonField(radioButtonName, i, radioButtonSelectedValue,radionButtonExtraTag)%>
       </td>
<%
      }

      for (int col=0; col < lvHandler.getColumnCount(); col++) {
	if (columns[col].displayColumn(userSession.getDisplayExtendedListview()) == true && columns[col].isColumnDefined()){   		//IR# PRUL110773050  Rel7.1.0  - 
			

         String style = null;
         if (columns[col].isColumnHidden()) {
            style="style=visibility:hidden;display:none";
         }
    %>
       <td nowrap align=<%=columns[col].justifyType%> <%=colorTag%> <%=style%>>
         <span class=ListText name=<%=columns[col].columnKey%> id=<%=columns[col].columnKey+i%> <%=style%>>
<%
              //TLE- 09/19/06 - CR379 - Add Begin
              //out.println(lvHandler.getColumnData(columns[col].columnKey, i));
              if (lvHandler.specialGetRelatedInstrID()) {
                 columnName = lvHandler.getRelatedInstrColName();               
                                
                 if (columns[col].columnKey.equals(columnName)) {

                    value = lvHandler.getColumnData(columns[col].columnKey, i);

                    if (value == null || value.equals("") || value.equals("&nbsp;")) {

                       out.println(value);
                    }
                    else {

                        InstrumentWebBean    relatedInstrument    = null;
                        StringBuffer         additionalParms      = new StringBuffer();

                        relatedInstrument = (InstrumentWebBean) beanMgr.createBean("com.ams.tradeportal.busobj.webbean.InstrumentWebBean", "Instrument"); 
                        relatedInstrument.getById(value);
                        additionalParms.append("&instrument_oid=");
                        additionalParms.append(EncryptDecrypt.encryptStringUsingTripleDes(value, userSession.getSecretKey()));

     			relatedInstrumentId = formMgr.getLinkAsHref("goToInstrumentSummary", 
                                                  relatedInstrument.getAttribute("complete_instrument_id"), 
                                                  additionalParms.toString(), response);

                        out.println(relatedInstrumentId);
                    }
                 }
                 else {
                    out.println(lvHandler.getColumnData(columns[col].columnKey, i));
                 }
              }
              else {
                  out.println(lvHandler.getColumnData(columns[col].columnKey, i));
              }
              //TLE- 09/19/06 - CR379 - Add End
              if (style == null) {
%>
             <img src="/portal/images/Blank_15flat.gif" width="15" height="1">
             <%
              }
             %>
          </span>
        </td>
<%
			}
      }
%>
      <td width=100%>&nbsp;</td>
    </tr>
   
<%
  }
%>
  </tbody>
  
  </table>
  </div>

<script language="JavaScript">
<%
  if(maxScrollHeight != null && !"".equals(maxScrollHeight)){
	  //XSS Rel 9.2 CID 11471
	  maxScrollHeight = StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(maxScrollHeight));
%>  
    var lvDiv = document.getElementById("<%=StringFunction.escapeQuotesforJS(listView)%>ScrollableDiv");
    if(lvDiv.offsetHeight==0 || lvDiv.offsetHeight > <%=maxScrollHeight%>){
      lvDiv.style.height="<%=maxScrollHeight%>px";
      lvDiv.style.overflow="auto";
    }
    
<%}%>    
</script>
  
  
<%
  //******************************************************************************
  // Step 5: Print the (optional) footer.  The footer is placed in its own table 
  // and consists of several fields that look something like this:
  // "              Items 1-10 (20 Total)   Page 1 2 3  <- Previous Next -> "
  // Previous and Next only display if there are prior or more rows to scroll to.
  // The page numbers display as links to that page except for the current page
  // (which just display as text).
  //******************************************************************************

  if (lvHandler.showFooter())
  {
     // Define the table and print the first few blank data cells.
%>
     <table cellspacing=0 cellpadding=0 border=0 width=100%>
       <tr>
         <td width=70% class=ColorBeige>&nbsp;</td>
         <% if (lvHandler.getPageCount() == 1) { %>
           <td class=ColorBeige width=170 align=center valign=middle nowrap>
             <span class=pageactivities>
               <%=lvHandler.getItemCountLabel()%>
             </span>
           </td>
           <td class=ColorBeige width=90 align=center valign=middle>
             <span class=pageactivities>
               <%=lvHandler.getPageLinks("pageactivities")%>
             </span>
           </td>
         <% } else { %>
           <td class=ColorBeige width=400 align=center valign=middle nowrap>
             <span class=pageactivities>
               <%=lvHandler.getPagesDropdown("pageactivities")%>
             </span>
           </td>
         <% } %>
      
<%
         if (lvHandler.hasPriorRows())
         { 
           out.println(lvHandler.getPreviousLink("pageactivities",
                                  "width=11 align=center valign=middle class=ColorBeige",
                                  "width=52 align=center valign=middle class=ColorBeige"));
         }
         else
         {
           out.println("<td class=ColorBeige width=11>&nbsp;</td>");
           out.println("<td class=ColorBeige width=52>&nbsp;</td>");
         }
%>
         <td width=6 class=ColorBeige>&nbsp;</td>
<%
         if (lvHandler.hasMoreRows())
         {
             // There are more rows, so display the Next and RightArrow cells
             out.println(lvHandler.getNextLink("pageactivities", 
                                     "width=27 align=center valign=middle class=ColorBeige",
                                     "width=11 align=center valign=middle class=ColorBeige"));
         }
         else
         {
             out.println("<td width=27 class=ColorBeige>&nbsp;</td>");
             out.println("<td width=11 class=ColorBeige>&nbsp;</td>");
         }
%>
         <td width=5 class=ColorBeige>&nbsp;</td>
       </tr>
     </table>

<%
  }

  // Finally, store the current state in the session.  This gets used if we return to
  // the page by any means other than the link on this page.
  lvHandler.storeStateInSession(session);
%>

<%
if (repFlag != null && repFlag.equals("Y"))
{
%>

<%@ include file="/reports/fragments/wiendpage.frag" %>

<%
}

%>