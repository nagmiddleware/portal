<%--
*******************************************************************************
                         Rollover Button Link

  Description:
    This includable JSP creates HTML for an href with image that provides
  rollover logic.  It simulates a button but is actually a link.  The link may
  be to another page and may include paramters.  It may also be a link back to
  the same page.

  The width and height of this button is 96x17 (but width can be overridden)

  This JSP relies on the changeImage javascript method defined in header.jsp

  The image displayed on the button is based on the image parameter passed
  in.  This is a resource bundle key.  The rollover image is based on that
  save key value with RO atteched.  E.g. common.SaveImg and common.SaveImgRO

  Parameters:
    showButton - true/false: indicates if the href tag is returned or a
                 &nbsp;.  The default is true.
    name       - name to assign to the button.  Should be unique but this 
                 jsp appends a random number to the name to ensure
                 uniqueness
    image      - resource bundle key for the image in normal state
    text       - resource bundle key for the alternate text for the image
    extraTags  - any Javascript that should be included in the onClick
                 attribute.  Optional. (The data sent for this tag must 
		 not include any '&' characters.  Use %26 instead.)
    width      - width of the button.  Default value is 96.  If value is 
                 "imagewidth", no width is specified in the resulting HTML
    height      - height of the button.  Default value is 17.  If value is 
                 "imageheight", no width is specified in the resulting HTML
    link       - the link to use when the button is pressed.   This should be
                 the result of a call to formMgr.getLinkAsUrl.  This value
                 should then be encoded using java.net.URLEncoder.encode().
                 (Otherwise, parameters that are passed in the link may be
                 lost.)
    brandingDirectory - if provided, indicates the directory to retrieve
                 images from.  The image directory and the branding directory
                 are prepended to the image value retrieved from the resource
                 bundle.
    userAction - this parameter is used for performance testing only.

*******************************************************************************
--%>
<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.ams.tradeportal.common.*, com.ams.tradeportal.busobj.util.*,
         com.amsinc.ecsg.util.*" %>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session"></jsp:useBean>
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>

<%
  boolean showButton = true;
  String name;
  String image;
  String text;
  String extraTags;
  String width;
  String height;
  String link;
  String userAction;

  // Optional - only should be used if image is branded
  String brandingDirectory;

  String value = request.getParameter("showButton");
  if ((value != null) && value.equals("false")) showButton = false;

  name      = request.getParameter("name");
  image     = request.getParameter("image");
  text      = request.getParameter("text");
  extraTags = request.getParameter("extraTags");  
  width     = request.getParameter("width");
  height     = request.getParameter("height");
  link      = request.getParameter("link");
  
//Validation to check Cross Site Scripting
  if(link != null){
 	 link = StringFunction.xssCharsToHtml(link);
  }
  if(name != null){
	 	 name = StringFunction.xssCharsToHtml(name);
	}
  
  
  // If a brandingDirectory paramter is passed in, use the parameter passed
  // in for branding directory.  Such as logout button.
  // If brandingDirectory parameter is not passed in.  The button is branded if the 
  // client bank set "Use button images from branding directory" to true.
  // If brandingDirectory parameter is passed in and equals "" then no branding directory is used.
  brandingDirectory = request.getParameter("brandingDirectory"); 
   if(brandingDirectory == null)
    {
     if (userSession.getBrandButtonInd()) 
      {
         brandingDirectory = userSession.getBrandingDirectory();
      }
	}
	
  // userAction has been added to ensure an additional userAction parameter
  // can be passed in the URL.  User for performance testing.
  userAction = request.getParameter("userAction");

  // Because parameters to this page cannot contain embedded & characters,
  // we have encoded the incoming link (e.g., the '&' comes in as '%26').
  // Decode the link to restore these characters.
  link = java.net.URLDecoder.decode(link);

  // Some versions of Netscape do not behave properly when there is a 
  // space in the URL.  Spaces are not allowed in URLs according to the
  // HTTP spec.   Here, we convert any spaces to %20, which is the correct way
  // to escape out a space
  if ((link != null) && (!link.equals("")))
  {    
    link = StringService.change(link, " ", "%20");  
  } 

  // However, extraTags are not encoded but use %26 in place of &.  These
  // need to be restored.  
  if ((extraTags != null) && (!extraTags.equals("")))
  {    
    extraTags = StringService.change(extraTags, "%26", "&");  
  }
  

  if ((userAction != null) && (!userAction.equals("")))
  {
    link = link+"&userAction="+userAction;
  }

  String source; 
  String sourceRO; 

  if(!InstrumentServices.isBlank(brandingDirectory))
   {
	// For branded images, the text resource bundle only contains the path
	// under the branding directory
	source = TradePortalConstants.IMAGES_PATH + brandingDirectory + "/" +
			resMgr.getText(image, TradePortalConstants.TEXT_BUNDLE);

	sourceRO = TradePortalConstants.IMAGES_PATH + brandingDirectory + "/" +
			resMgr.getText(image + "RO", TradePortalConstants.TEXT_BUNDLE);
   }
  else
   {
   // For unbranded images, the text reousrce is the complete path
	source = TradePortalConstants.IMAGES_PATH + resMgr.getText(image, 
                                TradePortalConstants.TEXT_BUNDLE);
	sourceRO = TradePortalConstants.IMAGES_PATH + resMgr.getText(image + "RO",
                                   TradePortalConstants.TEXT_BUNDLE);
   }
 
  if ((width == null) || width.equals("")) width = "96";
  if ((height == null) || height.equals("")) height = "imageheight";

  // If a page has multiple buttons with the same name (e.g., Save), we have a problem
  // because rolling over one would cause all of the buttons with that name to
  // change image.  To get around this, we create a unique name by appending two
  // random numbers to the image name with a dot separator.  Thus a sample
  // name might be PartySearch5.21  Although it is possible that this logic could
  // generate the exact same set of numbers for two buttons, the odds are very low.

  long aNumber = (long)(Math.random()*100);
  String imageName = name + aNumber + ".";
  aNumber = (long)(Math.random()*100);
  imageName += aNumber;

  if (showButton) {
     if ("".equals(link)) {
	 %>
	    <span 
	 <%
	 } else {
	 %>
       <a href="<%=link%>" 
	<%}%>
	
	
	name="<%=name%>" onClick="<%=extraTags%>"
       onMouseOut="changeImage('<%=imageName%>', '<%=source%>')"
       onMouseOver="changeImage('<%=imageName%>', '<%=sourceRO%>')">
       <img border="0" name="<%=imageName%>" 
    <% if(!height.equalsIgnoreCase("imageheight")) out.print("height="+height); %> 
            src="<%=source%>"
            alt="<%=resMgr.getText(text,
                                   TradePortalConstants.TEXT_BUNDLE)%>">
	 <%
     if ("".equals(link)) {
	 %>
	 </span>
	 <%
	 } else {
	 %>
    </a>
	<%}%>	
		
<% } else {
       out.println("&nbsp;");
   }
%>
