<%--
*******************************************************************************
                            Instrument Search/Selection

  Description:
    This page is a multi-function page that is driven off the parameters that
  are passed into it.  Its primary purpose is to display a list of instruments.
  The page actually consists of two forms.  The upper portion of the page is
  a form the supports filtering of the intruments based on entered criteria.
  The bottom portion of the page is the list of instruments meeting the
  criteria.

  Filter Form
    The filter form operates in two modes: basic and advanced.  When the basic
  filter fields are displayed the fields operate in an 'or' mode.  Thus if ANY
  of the entered fields match an instrument, the instrument is displayed.  In
  advanced mode, the filter fields operate in an 'and' mode.  ALL the criteria
  must be met for an instrument to display.  The filter button accepts the
  values and redirects back to this same page (with all the other parameters).
  As the page is being rebuilt, validation occurs on date and number fields.
  Error messages display and those fields are not included in the search.

  Instrument List Form
    The instrument list form also operates in two modes: as a selection page
  or as a step in creating a new template/transaction.
    In selection mode, an appropriate message displays on the bottom and a
  selection button displays.  The selection button returns the selected
  instrument to the caller.
    In step mode, a different message displays on the bottom left and the
  prev and next step buttons display.  Prev step returns the user to the
  previous page (along with the copy type and bank branch parameters).  Next
  step submits a form based on the next step parameter.

  Parameters:
    SelectMode     - Y to indicate the page is being used to select an
                     instrument (Choose Instrument button displays), not as
                     part of new template/instrument process.  Required,
                     defaults to N.
    CancelAction   - A navigation action that drives where the page forwards
                     if Close button is pressed
    PrevStepAction - A navigation action that drives where the page goes to
                     when Prev Step button is pressed.  Only valid in non
                     select mode.  The CopyType parameter is passed out
    NextStepForm   - A FormManager form that drives what mediator is called
                     when next step or choose instrument is selected.  All
                     these parameters as well as the selected instrument are
                     passed in the request.
    SearchType     - ADVANCED or SIMPLE, drives what search fields to display
                     on the screen.  Default is SIMPLE
    CopyType       - FROM_INSTR, FROM_TEMPL, or FROM_BLANK, indicates how the
                     user want to create the new template/instrument.  The
                     creation mediator needs to know this.
    TitleKey       - The title of this page has two parts.  The first is
                     driven by this resource bundle key (if provided).  The
                     second part is driven by the search type.
    BankBranch     - A parameter passed from the New Transaction page that
                     drives the instrument number.  Nothing is done with this
                     parm except to pass it through.
    ShowNavBar     - A parameter used to indicate whether or not the navigation
                     bar should be displayed at the top of the page. The default 
                     is Y.
*******************************************************************************
--%>

<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.html.*, com.ams.tradeportal.busobj.*, com.ams.tradeportal.busobj.util.*" %>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager"
    scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean"
   scope="session">
</jsp:useBean>

<%-- ************** Data retrieval page setup begins here ****************  --%> 
<%
	WidgetFactory widgetFactory = new WidgetFactory(resMgr);
  String linkHref="";
  String linkName="";

  Debug.debug("***START********************INSTRUMENT SEARCH**************************START***");

  boolean newTemplate = true;     // indicates what page we came from (i.e.,
                                  // doing new template or new transaction)
  String loginLocale = userSession.getUserLocale();
  String loginRights = userSession.getSecurityRights();

  String options;
  String defaultText;

  // Parameters that drive logic for this page.
  String selectMode;
  String cancelAction;
  String prevStepAction;
  String nextStepForm;
  String searchCondition = "";
  String copyType;
  String titleKey;
  String bankBranch;
  String showNavBar;
  String showChildInstruments;
  String datePattern = ""; 
  String datePatternDisplay = ""; 

  String searchListViewName = "InstrumentSearchListView.xml";
  StringBuffer dynamicWhere = new StringBuffer();
  StringBuffer newSearchCriteria = new StringBuffer();

  String       NewDropdownOptions = "";
  StringBuffer statusOptions   = new StringBuffer();
  StringBuffer statusExtraTags = new StringBuffer();
  String       selectedStatus  = "";
  String            userOrgOid                   = null;

  final String STATUS_ALL      = resMgr.getText("common.StatusAll",
                                                TradePortalConstants.TEXT_BUNDLE);
  final String STATUS_ACTIVE   = resMgr.getText("common.StatusActive",
                                                TradePortalConstants.TEXT_BUNDLE);
  final String STATUS_INACTIVE = resMgr.getText("common.StatusInactive",
                                                TradePortalConstants.TEXT_BUNDLE);
  // W Zhu 6/8/07 POUH060841116 Add STATUS_STRICTLY_ACTIVE
  // This status is passed in from invoking page directly and will not be part of
  // the visible Option field so we do not need to get from language-specific
  // text resource file.
  final String STATUS_STRICTLY_ACTIVE = "Strictly Active";
  userOrgOid = userSession.getOwnerOrgOid();

// PPRAKASH IR NGUJ013070808 Add Begin
  CorporateOrganizationWebBean corpOrg  = (CorporateOrganizationWebBean) beanMgr.createBean("com.ams.tradeportal.busobj.webbean.CorporateOrganizationWebBean", "CorporateOrganization");
  corpOrg.getById(userSession.getOwnerOrgOid());
  //PPRAKASH IR NGUJ013070808 Add End
  
  // Based on the search type, read in the search parameters the user may have
  // previously used.  We use these to build the dynamic search criteria and
  // also to redisplay to the user

  String instrumentId = "";
  String refNum = "";
  String bankRefNum = "";
  String vendorId = "";
  String instrumentType = "";
  String currency = "";
  String amountFrom = "";
  String amountTo = "";
  String otherParty = "";
  String dayFrom = "";
  String monthFrom = "";
  String yearFrom = "";
  String dayTo = "";
  String monthTo = "";
  String yearTo = "";
  String helpSensitiveLink  = null;

  String searchType = "";
  Vector instrumentTypes = null;
  boolean showInstrumentDropDown = false;
  String mode = "";
	    
  DocumentHandler doc = null;
  // First read in the parameters that drive how the page operates.

  cancelAction = request.getParameter("CancelAction");
  if (cancelAction == null)
     Debug.debug("***Warning: CancelAction parameter not specified");

  prevStepAction = request.getParameter("PrevStepAction");

  nextStepForm = request.getParameter("NextStepForm");
  if (nextStepForm == null)
     Debug.debug("***Warning: NextStepForm parameter not specified");

  selectMode = request.getParameter("SelectMode");
  if (selectMode == null) selectMode = TradePortalConstants.INDICATOR_NO;

  showNavBar = request.getParameter("ShowNavBar");
  if (showNavBar == null)
  {
     showNavBar = TradePortalConstants.INDICATOR_YES;
  }

  // Indicates whether or not instruments belonging to the child organization 
  // of the user's organization should be displayed in the list
  showChildInstruments = request.getParameter("ShowChildInstruments");
  if(showChildInstruments == null)
   {
      showChildInstruments = TradePortalConstants.INDICATOR_NO;
   }


  // Check if this is a new Search (based on passed in NewSearch parameter)
  // If so, clear the listview information so it does not
  // retain any page/sort order/ sort column/ search type information 
  // resulting from previous searches
	
  // A new search can also result from the corp org or status type dropdown 
  // being reselected. In this case, NewDropdownSearch parameter indicates a 
  // pseudo new search -- the search type is retained but the rest of the 
  // search clause is not

  String newSearch = request.getParameter("NewSearch");
  String newDropdownSearch = request.getParameter("NewDropdownSearch");
  Debug.debug("New Search is " + newSearch);
  Debug.debug("New Dropdown Search is " + newDropdownSearch);

  if (newSearch != null && newSearch.equals(TradePortalConstants.INDICATOR_YES))
  {
      session.removeAttribute(searchListViewName);

      // Default search type for instrument status on the Instrument Search page is ALL.
      session.setAttribute("instrSearchStatusType", STATUS_ALL);
  }

  searchType = request.getParameter("SearchType");

  if (searchType == null)
  {
	   searchType = ListViewHandler.getSearchTypeFromState(searchListViewName, session);
	   if (searchType == null) searchType = TradePortalConstants.SIMPLE;

              if ((newDropdownSearch != null) 
                 && (newDropdownSearch.equals(TradePortalConstants.INDICATOR_YES)))
              {
                 session.removeAttribute(searchListViewName);
              }
  }
  else
  {
	   // this is probably a new search type clicked from the link, so remove
	   // the old search type
	   session.removeAttribute(searchListViewName);
  }

  copyType = request.getParameter("CopyType");
  if (copyType == null) copyType = "";


  bankBranch = request.getParameter("BankBranch");


  
  titleKey = request.getParameter("TitleKey");
  if (titleKey == null)
  {
     titleKey = "";
  }

  /************************
   * ERSKINE'S CODE START *
   ************************/

  //If we are in the transaction area the parameters will be passed in via this document
  HttpSession theSession = request.getSession(false);
  String inTransactionArea = (String) theSession.getAttribute("inNewTransactionArea");

  if (inTransactionArea != null && inTransactionArea.equals(TradePortalConstants.INDICATOR_YES))
  {
    doc = formMgr.getFromDocCache();
    Debug.debug("Getting data from doc cache ->\n" + doc.toString(true));

	mode    = doc.getAttribute("/In/mode");
    cancelAction   = TradePortalConstants.TRANSACTION_CANCEL_ACTION;
    selectMode     = TradePortalConstants.INDICATOR_NO;
    copyType       = doc.getAttribute("/In/copyType");
    bankBranch     = doc.getAttribute("/In/bankBranch");

    // Change the title of the page based on what we're in the process of creating
    if (mode != null && mode.equals(TradePortalConstants.NEW_TEMPLATE))
       titleKey = "InstSearch.HeadingNewTemplateStep2";
    else
       titleKey = "InstSearch.HeadingNewTransStep2"; 

    Debug.debug("Getting data from doc cache2 ->\n" + doc.toString(true));

    if (mode != null && mode.equals(TradePortalConstants.EXISTING_INSTR))
    {
        nextStepForm = "NewTransInstrSearchForm2";
        prevStepAction = "newTransactionStep1";
    }
    else
    {
        nextStepForm = "NewTransInstrSearchForm";
        if (mode != null && mode.equals(TradePortalConstants.NEW_TEMPLATE))
            prevStepAction = "newTemplate";
        else
            prevStepAction = "newTransactionStep1";
       
       //IAZ CR-586 08/16/10 Begin
       String isFixed = doc.getAttribute("/In/isFixed");
       String isExpress = doc.getAttribute("/In/isExpress");
       if(TradePortalConstants.INDICATOR_ON.equalsIgnoreCase(isFixed))
  	 	dynamicWhere.append(" and i.instrument_type_code = '" + TradePortalConstants.DOM_PMT + "'");
  	   if(TradePortalConstants.INDICATOR_ON.equalsIgnoreCase(isExpress ))
  	 	dynamicWhere.append(" and i.instrument_type_code in ('" + TradePortalConstants.SIMPLE_SLC + "'"
  	 	 + ", '" + TradePortalConstants.APPROVAL_TO_PAY + "'"
  	 	 + ", '" + TradePortalConstants.REQUEST_ADVISE + "'"
  	 	 + ", '" + TradePortalConstants.IMPORT_DLC + "')");  	   
       //IAZ CR-586 08/16/10 End

    }

    theSession.setAttribute("prevStepAction", "goToInstrumentSearch");
    Debug.debug("Getting data from doc cache 3->\n" + doc.toString());

  }
  /************************
   *  ERSKINE'S CODE END  *
   ************************/
  else
  {
     doc = formMgr.getFromDocCache();

     if (doc.getDocumentNode("/In/InstrumentSearchInfo/SelectMode") != null)
     {
        selectMode   = doc.getAttribute("/In/InstrumentSearchInfo/SelectMode");
        cancelAction = doc.getAttribute("../CancelAction");
        nextStepForm = doc.getAttribute("../NextStepForm");
        showNavBar   = doc.getAttribute("../ShowNavBar");
        showChildInstruments = doc.getAttribute("../ShowChildInstruments");
        if(showChildInstruments == null)
          {
             showChildInstruments = TradePortalConstants.INDICATOR_NO;
          }
     }
  }


  String searchOnInstrumentType = doc.getAttribute("/In/InstrumentSearchInfo/SearchInstrumentType");

  if (searchOnInstrumentType == null)
  	searchOnInstrumentType = "";
	
  // determine what type of search is required - this is used by 
  // SearchParms and SearchType for sqlWhereClause and Instrument Type
  // dropdowns
  if (searchOnInstrumentType.equals(TradePortalConstants.EXPORT_DLC))
  	searchCondition = TradePortalConstants.SEARCH_EXP_DLC_ACTIVE;


  // TLE - 10/26/06 - IR#AYUG101742823 - Add Begin
  else if (searchOnInstrumentType.equals(TradePortalConstants.ALL_EXPORT_DLC)) {
  	searchCondition = TradePortalConstants.ALL_EXPORT_DLC;
  } 
  else if (searchOnInstrumentType.equals(TradePortalConstants.EXPORT_COL)) {
  	searchCondition = TradePortalConstants.EXPORT_COL;
  }
  //Vasavi CR 524 03/31/2010 Begin
  else if (searchOnInstrumentType.equals(TradePortalConstants.NEW_EXPORT_COL)) {
  	searchCondition = TradePortalConstants.NEW_EXPORT_COL;
  }
  //Vasavi CR 524 03/31/2010 End
  else if (searchOnInstrumentType.equals(TradePortalConstants.IMPORT)) {
  	searchCondition = TradePortalConstants.IMPORT;
  }
  // TLE - 10/26/06 - IR#AYUG101742823 - Add End



  else if(searchOnInstrumentType.equals(TradePortalConstants.LOAN_RQST)) {
  	searchCondition = TradePortalConstants.LOAN_RQST;
        // Need to remove any old search critiera.
        session.removeAttribute(searchListViewName);
  }
  else if(searchOnInstrumentType.equals(TradePortalConstants.IMPORTS_ONLY))
	searchCondition = TradePortalConstants.IMPORTS_ONLY;	
  else if(mode != null && mode.equals(TradePortalConstants.EXISTING_INSTR))
  	searchCondition = TradePortalConstants.SEARCH_FOR_NEW_TRANSACTION;
  else if(mode != null && mode.equals(TradePortalConstants.NEW_INSTR))
  	searchCondition = TradePortalConstants.SEARCH_FOR_COPY_INSTRUMENT;
  else if(mode != null && mode.equals(TradePortalConstants.NEW_TEMPLATE))
  	searchCondition = TradePortalConstants.SEARCH_FOR_COPY_INSTRUMENT;
  else
      searchCondition = TradePortalConstants.SEARCH_ALL_INSTRUMENTS;

  Debug.debug("CancelAction is " + cancelAction);
  Debug.debug("PrevStepAction is " + prevStepAction);
  Debug.debug("NextStepForm is " + nextStepForm);
  Debug.debug("SelectMode is " + selectMode);
  Debug.debug("Mode is " + mode);
  Debug.debug("SearchType is " + searchType);
  Debug.debug("CopyType is " + copyType);
  Debug.debug("BankBranch is " + bankBranch);
  Debug.debug("ShowNavBar is " + showNavBar);
  Debug.debug("SearchCondition is " + searchCondition);
 
  // Create a link to the basic or advanced instrument search
  String linkParms;
  String linkText;
  String link;
    Debug.debug("Getting data from doc cache 5->\n");

  linkParms = "&SelectMode=" + selectMode
              + "&CancelAction=" + cancelAction
              + "&PrevStepAction=" + prevStepAction
              + "&NextStepForm=" + nextStepForm
              + "&CopyType=" + copyType
              + "&TitleKey=" + titleKey
              + "&BankBranch=" + bankBranch
              + "&ShowNavBar=" + showNavBar;
  linkText = "";

  if (searchType.equals(TradePortalConstants.ADVANCED)) {
    linkParms += "&SearchType=" + TradePortalConstants.SIMPLE;
    link = formMgr.getLinkAsUrl("goToInstrumentSearch",
                                linkParms, response);
    linkText = resMgr.getText("InstSearch.BasicSearch",
                              TradePortalConstants.TEXT_BUNDLE);
    helpSensitiveLink = OnlineHelp.createContextSensitiveLink("customer/instr_search.htm",
	   						"advanced",resMgr, userSession);
   } else {
    linkParms += "&SearchType=" + TradePortalConstants.ADVANCED;
    link = formMgr.getLinkAsUrl("goToInstrumentSearch",
                                linkParms, response);
    linkText = resMgr.getText("InstSearch.AdvancedSearch",
                              TradePortalConstants.TEXT_BUNDLE);
    helpSensitiveLink = OnlineHelp.createContextSensitiveLink("customer/instr_search.htm",
	   				"basic", resMgr, userSession);
   }


   if(showChildInstruments.equals(TradePortalConstants.INDICATOR_YES))
    {
      // Append the dynamic where clause to view the instruments of the user's 
      // organizations and all of its children.
      dynamicWhere.append(" and i.a_corp_org_oid in ( ");      
      dynamicWhere.append(" select organization_oid ");
      dynamicWhere.append(" from corporate_org ");
      dynamicWhere.append(" start with organization_oid =  ");
      dynamicWhere.append(userSession.getOwnerOrgOid());
      dynamicWhere.append(" connect by prior organization_oid = p_parent_corp_org_oid ");
      dynamicWhere.append(" ) ");
    }
   else
    {
      // Append the dynamic where clause to view only the user
      // organization's instruments
      dynamicWhere.append(" and i.a_corp_org_oid = "+  userSession.getOwnerOrgOid());
    } 

    // Based on the statusType dropdown, build the where clause to include one or more
    // instrument statuses.
    Vector statuses = new Vector();
    int numStatuses = 0;

    selectedStatus = request.getParameter("instrSearchStatusType");

    if (selectedStatus == null) 
    {
       selectedStatus = (String) session.getAttribute("instrSearchStatusType");
    }

    // W Zhu 6/8/07 POUH060841116 Add case Strictly_active
    if (STATUS_STRICTLY_ACTIVE.equals(selectedStatus) )
    {
       statuses.addElement(TradePortalConstants.INSTR_STATUS_ACTIVE);
    }
    else if (STATUS_ACTIVE.equals(selectedStatus) )
    {
       statuses.addElement(TradePortalConstants.INSTR_STATUS_ACTIVE);
       statuses.addElement(TradePortalConstants.INSTR_STATUS_PENDING);
       statuses.addElement(TradePortalConstants.INSTR_STATUS_EXPIRED);
    }

    else if (STATUS_INACTIVE.equals(selectedStatus) )
    {
       statuses.addElement(TradePortalConstants.INSTR_STATUS_CANCELLED);
       statuses.addElement(TradePortalConstants.INSTR_STATUS_CLOSED);
       statuses.addElement(TradePortalConstants.INSTR_STATUS_DEACTIVATED);
       statuses.addElement(TradePortalConstants.INSTR_STATUS_LIQUIDATE_ALL);
       statuses.addElement(TradePortalConstants.INSTR_STATUS_LIQUIDATE_BANK_SIDE);
       statuses.addElement(TradePortalConstants.INSTR_STATUS_LIQUIDATE_CUST_SIDE);
    }

    else // default is ALL (actually not all since DELeted instruments
         // never show up (handled by the SQL in the listview XML)
    {
       selectedStatus = STATUS_ALL;
    }

    session.setAttribute("instrSearchStatusType", selectedStatus);

    // If the desired status is ALL, there's no point in building the where 
    // clause for instrument status.  Otherwise, build it using the vector
    // of statuses we just set up.

    if (!STATUS_ALL.equals(selectedStatus))
    {
       //TLE - 07/10/07 - IR-POUH060841116 - Add Begin
       //dynamicWhere.append(" and i.instrument_status in (");
       if (STATUS_STRICTLY_ACTIVE.equals(selectedStatus) ) {
           dynamicWhere.append(" and (o.transaction_status = '");
           dynamicWhere.append(TradePortalConstants.TRANS_STATUS_AUTHORIZED);
           dynamicWhere.append("' or i.instrument_status in (");
       } else {
           dynamicWhere.append(" and i.instrument_status in (");
       }
       //TLE - 07/10/07 - IR-POUH060841116 - Add End

       numStatuses = statuses.size();

       for (int i=0; i<numStatuses; i++) {
         dynamicWhere.append("'");
         dynamicWhere.append( (String) statuses.elementAt(i) );
         dynamicWhere.append("'");

         if (i < numStatuses - 1) {
            dynamicWhere.append(", ");
         }
       }

       //TLE - 07/10/07 - IR-POUH060841116 - Add Begin
       //dynamicWhere.append(") ");
       if (STATUS_STRICTLY_ACTIVE.equals(selectedStatus) ) {
          dynamicWhere.append(")) ");
       } else {
          dynamicWhere.append(") ");
       }
       //TLE - 07/10/07 - IR-POUH060841116 - Add End
    }

         //IAZ CR-586 IR- PRUK092452162 09/29/10 Begin
         UserWebBean thisUser = (UserWebBean) beanMgr.createBean("com.ams.tradeportal.busobj.webbean.UserWebBean", "User");
         thisUser.setAttribute("user_oid", userSession.getUserOid());
         thisUser.getDataFromAppServer();
         String confInd = "";
         if (TradePortalConstants.ADMIN.equals(userSession.getSecurityType()))
         	confInd = TradePortalConstants.INDICATOR_YES;
         else if (userSession.getSavedUserSession() == null)
            confInd = thisUser.getAttribute("confidential_indicator");
         else   
         {
            if (TradePortalConstants.ADMIN.equals(userSession.getSavedUserSession().getSecurityType()))
                confInd = TradePortalConstants.INDICATOR_YES;
            else	
                confInd = thisUser.getAttribute("subsid_confidential_indicator");  
         }       
          
         if (!TradePortalConstants.INDICATOR_YES.equals(confInd))
        	dynamicWhere.append(" and i.confidential_indicator = 'N'");
         //IAZ CR-586 IR-PRUK092452162 09/29/10 End
         

   // Now include a file which reads the search criteria and build the
   // dynamic where clause.
%>
   <%@ include file="/transactions/fragments/InstSearch-InstType.frag" %>
   <%@ include file="/transactions/fragments/InstSearch-SearchParms.frag" %>

<%
   // Build the status dropdown options (hardcoded in ALL (default), ACTIVE, INACTIVE order), 
   // re-selecting the most recently selected option.

   statusOptions.append("<option value='");
   statusOptions.append(STATUS_ALL);
   statusOptions.append("' ");
   if (STATUS_ALL.equals(selectedStatus)) {
      statusOptions.append(" selected ");
   }
   statusOptions.append(">");
   statusOptions.append(STATUS_ALL);
   statusOptions.append("</option>");

   statusOptions.append("<option value='");
   statusOptions.append(STATUS_ACTIVE);
   statusOptions.append("' ");
   if (STATUS_ACTIVE.equals(selectedStatus)) {
      statusOptions.append(" selected ");
   }
   statusOptions.append(">");
   statusOptions.append(STATUS_ACTIVE);
   statusOptions.append("</option>");

   statusOptions.append("<option value='");
   statusOptions.append(STATUS_INACTIVE);
   statusOptions.append("' ");
   if (STATUS_INACTIVE.equals(selectedStatus)) {
      statusOptions.append(" selected ");
   }
   statusOptions.append(">");
   statusOptions.append(STATUS_INACTIVE);
   statusOptions.append("</option>");

   // Upon changing the status selection, automatically go back to the Instrument Search
   // page with the selected status type.  Force a new search to occur (causes cached
   // "where" clause to be reset.)
   statusExtraTags.append("onchange=\"location='");
   statusExtraTags.append(formMgr.getLinkAsUrl("goToInstrumentSearch", response));
   statusExtraTags.append("&NewDropdownSearch=Y");
   statusExtraTags.append("&amp;instrSearchStatusType='+this.options[this.selectedIndex].value\"");

%>
<%-- ********************* HTML for page begins here *********************  --%>

<script LANGUAGE="JavaScript">

  function checkForSelection() {
    <%--
    // Before going to the next step, the user must have selected one of the
    // radio buttons.  If not, give an error.  Since the selection radio button
    // is dynamic, it is possible the field doesn't even exist.  First check
    // for the existence of the field and then check if a selection was made.
    --%>

    numElements = document.InstrumentSearchResults.elements.length;
    i = 0;
    foundField = false;

    <%-- First determine if the field even exists. --%>
    for (i=0; i < numElements; i++) {
       if (document.InstrumentSearchResults.elements[i].name == "selection") {
          foundField = true;
       }
    }

    if (foundField) {
        var size=document.InstrumentSearchResults.selection.length;
        <%-- Handle the case where there are two or more radio buttons --%>
        for (i=0; i<size; i++)
        {
            if (document.InstrumentSearchResults.selection[i].checked)
            {
                return true;
            }
        }
        <%-- Handle the case where there is only one radio button --%>
        if (document.InstrumentSearchResults.selection.checked)
        {
            return true;
        }
    }
    <%-- Handle the case where no button is checked --%>
    var msg = "<%=resMgr.getText("InstSearch.SelectOne",
                                  TradePortalConstants.TEXT_BUNDLE)%>"
    alert(msg);


   <%-- narayan IR# POUJ092282592 --- Add below one line to make variable formSubmitted to FALSE
   when user hit the button Next Step, system actually submits the page and making the value of Boolean variable formSubmitted to TRUE.
   Hence after clicking to Next Step whenever user clicks on search system won't do anything (means system won't search / submit the page again)
   because it thinks that this page is already submitted and Trade Portal don't allow to form submission twice.
   --%>
    formSubmitted = false;
    return false;
  }

   <%--Enable form submission by enter.  event.which works for Netscape and event.keyCode works for IE --%>  
         function enterSubmit(event, myform) {
            if (event && event.which == 13) {
               setButtonPressed('null', 0);
               myform.submit();
            }
            else if (event && event.keyCode == 13) {
               setButtonPressed('null', 0);
               myform.submit();
            }
            else {
               return true;
            }
         }
  

</script>

<%
  // onLoad is set to default the cursor to the Instrument Type field.
  //String onLoad = "document.InstrumentFilter.TemplateName.focus();";
  String onLoad = "";
%>

<%-- Body tag included as part of common header --%>
<jsp:include page="/common/Header.jsp">
   <jsp:param name="includeNavigationBarFlag"
              value="<%=showNavBar%>" />
   <jsp:param name="includeErrorSectionFlag"
              value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="additionalOnLoad" value="<%=onLoad%>" />
</jsp:include>

<%
  // Store values such as the userid, his security rights, and his org in a
  // secure hashtable for the form.  The mediator needs this information.
  Hashtable secureParms = new Hashtable();
  secureParms.put("login_oid", userSession.getUserOid());
  secureParms.put("owner_org_oid", userSession.getOwnerOrgOid());
  secureParms.put("login_rights", loginRights);
%>

<%-- ********************** First form begins here ***********************  --%>
<%-- Supports processing of Filter button.                                  --%>

<form name="InstrumentFilter" method="post"
      action="<%=formMgr.getSubmitAction(response)%>">

  <table width="100%" border="0" cellspacing="0" cellpadding="0" height="21">
    <tr>
      <td width="20" class="BankColor" nowrap>&nbsp;</td>
      <td class="BankColor" valign="middle" align="left" width="100%" nowrap
          height="25">
        <p class="ControlLabelWhite">
<%
          if ((titleKey != null) && (!titleKey.equals("")))
          {
            // if we have a title, display it
%>
            <%=resMgr.getText(StringFunction.xssCharsToHtml(titleKey),
                              TradePortalConstants.TEXT_BUNDLE)%>
            <img src="/portal/images/Blank_15.gif" width="15"
                 height="15">
<%
          }
          String searchKey;
          if (searchType.equals(TradePortalConstants.ADVANCED)) {
             searchKey = "InstSearch.AdvancedInstrumentSearch";
          }
          else
          {
             searchKey = "InstSearch.SimpleInstrumentSearch";
          }
%>
          <%=resMgr.getText(searchKey,
                            TradePortalConstants.TEXT_BUNDLE)%>
        </p>
      </td>
      <td width="25" class="BankColor" height="25">&nbsp; </td>
      <td class=BankColor align=right valign=middle width=15 height=25 nowrap>
		<%=helpSensitiveLink%>
      </td>
      <td width="2" class="BankColor" nowrap>&nbsp;</td>
    </tr>
  </table>

  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="BankColor">
    <tr>
      <td width="15" nowrap>&nbsp;</td>
        <td nowrap align="left" valign="middle">
<%     //W Zhu 6/8/07 POUH060841116 BEGIN
       // If the invoking page force searching for strictly active instrument,
       // do not display the Instrument Status option.
       if (!STATUS_STRICTLY_ACTIVE.equals(selectedStatus) )
       {
       //W Zhu 6/8/07 POUH060841116 END
%>

          <span class="ListHeaderTextWhite">
            <%= resMgr.getText("InstrumentHistory.Show", 
                               TradePortalConstants.TEXT_BUNDLE) %>
          </span>
          &nbsp;
<%
          out.print(InputField.createSelectField("instrSearchStatusType", "", "",
                                                 statusOptions.toString(), "ListText", 
                                                 false, statusExtraTags.toString()));
%>
          &nbsp;
          <span class="ListHeaderTextWhite">
            <%= resMgr.getText("InstrumentHistory.Instruments", 
                               TradePortalConstants.TEXT_BUNDLE) %>&nbsp;
          </span>
<%
        } //if (!TradePortalConstants.INSTR_SEARCH_STATUS_ACTIVE.equals(selectedStatus))
%>        
        </td>
      <td width="100%" height="30">&nbsp;</td>
    </tr>
  </table>

  <%=formMgr.getFormInstanceAsInputField("InstrumentFilterForm", secureParms) %>
  <input type=hidden name=SelectMode value=<%=StringFunction.xssCharsToHtml(selectMode)%>>
  <input type=hidden name=CancelAction value=<%=StringFunction.xssCharsToHtml(cancelAction)%>>
  <input type=hidden name=PrevStepAction value=<%=StringFunction.xssCharsToHtml(prevStepAction)%>>
  <input type=hidden name=NextStepForm value=<%=StringFunction.xssCharsToHtml(nextStepForm)%>>
  <input type=hidden name=SearchType value=<%=StringFunction.xssCharsToHtml(searchType)%>>
  <input type=hidden name=CopyType value=<%=StringFunction.xssCharsToHtml(copyType)%>>
  <input type=hidden name=TitleKey value=<%=StringFunction.xssCharsToHtml(titleKey)%>>
  <input type=hidden name=BankBranch value=<%=StringFunction.xssCharsToHtml(bankBranch)%>>
  <input type=hidden name=ShowNavBar value=<%=StringFunction.xssCharsToHtml(showNavBar)%>>
  <input type=hidden name=instrumentType value=<%=StringFunction.xssCharsToHtml(searchOnInstrumentType)%>>


<%

  if (searchType.equals(TradePortalConstants.ADVANCED)) {
%>
    <%@ include file="/transactions/fragments/InstSearch-AdvancedFilter.frag" %>
<%
  } else {
%>
    <%@ include file="/transactions/fragments/InstSearch-BasicFilter.frag" %>
<%
  }
%>
</form>
<%-- ********************* Second form begins here ***********************  --%>
<%-- Supports processing of Prev, Next (or Chose), and Close buttons.       --%>
<form name="InstrumentSearchResults" method="post"
      action="<%=formMgr.getSubmitAction(response)%>">
  <input type=hidden value="" name=buttonName>

  <input type=hidden name=SelectMode value=<%=StringFunction.xssCharsToHtml(selectMode)%>>
  <input type=hidden name=CancelAction value=<%=StringFunction.xssCharsToHtml(cancelAction)%>>
  <input type=hidden name=PrevStepAction value=<%=StringFunction.xssCharsToHtml(prevStepAction)%>>
  <input type=hidden name=NextStepForm value=<%=StringFunction.xssCharsToHtml(nextStepForm)%>>
  <input type=hidden name=SearchType value=<%=StringFunction.xssCharsToHtml(searchType)%>>
  <input type=hidden name=CopyType value=<%=StringFunction.xssCharsToHtml(copyType)%>>
  <input type=hidden name=TitleKey value=<%=StringFunction.xssCharsToHtml(titleKey)%>>
  <input type=hidden name=BankBranch value=<%=StringFunction.xssCharsToHtml(bankBranch)%>>
  <input type=hidden name=ShowNavBar value=<%=StringFunction.xssCharsToHtml(showNavBar)%>>

  <%=formMgr.getFormInstanceAsInputField(nextStepForm, secureParms) %>

  <jsp:include page="/common/TradePortalListView.jsp">
    <jsp:param name="listView" value='<%=searchListViewName%>' />
    <jsp:param name="whereClause2" value='<%=dynamicWhere.toString()%>' />
	<jsp:param name="searchCriteria"   value='<%= newSearchCriteria.toString() %>' />
	<jsp:param name="searchType"   value='<%= searchType %>' />
  </jsp:include>
     
<%
  // Determine the number of rows in the listview.
  ListViewHandler.ListViewState lvState = (ListViewHandler.ListViewState)
                          session.getAttribute("InstrumentSearchListView.xml");
  int numRows = lvState.totalRows;
%>

  <table width="100%" border="0" cellspacing="0" cellpadding="0"
         class="BankColor">
    <tr>
      <td width="20" nowrap>&nbsp;</td>
      <td width="37" nowrap align="left" valign="top">
        <img src="/portal/images/UpIndicatorArrow.gif" width="37" height="27">
      </td>
      <td width="5" nowrap>
        <img src="/portal/images/Blank_4x4.gif" width="4" height="4">
      </td>
      <td nowrap valign="middle">
        <p class="ListTextwhite">
<%
          if (selectMode.equals(TradePortalConstants.INDICATOR_YES)) {
             out.println(resMgr.getText("InstSearch.SelectItem",
                                        TradePortalConstants.TEXT_BUNDLE));
          } else {
             out.println(resMgr.getText("InstSearch.ToContinue",
                                        TradePortalConstants.TEXT_BUNDLE));
          }
%>
        </p>
      </td>
      <td class="BankColor" width="100%">&nbsp;</td>
      <td>
  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="BankColor">  
    <tr>
       <td><img src="/portal/images/Blank_4x4.gif" width="4" height="4"></td>
    </tr>
  </table>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
<%
            if (selectMode.equals(TradePortalConstants.INDICATOR_YES)) {
              // In selection mode display a choose instrument button.
              // Otherwise show Prev Step and Next Step buttons
%>
              <td>&nbsp;</td>
              <td width="15" nowrap>&nbsp;</td>
              <td>
                <jsp:include page="/common/RolloverButtonSubmit.jsp">
                  <jsp:param name="showButton" value="<%=(numRows>0)%>" />
                  <jsp:param name="name" value="SelectInstButton" />
                  <jsp:param name="image" value='InstSearch.SelectInstImg' />
                  <jsp:param name="text" value='InstSearch.SelectInstText' />
			<jsp:param name="width" value='220' />
                  <jsp:param name="formNum" value='1' />
                  <jsp:param name="extraTags"
                             value="onClick='return checkForSelection();'" />
                </jsp:include>
              </td>
<%
            } else {
               String prevParm = "&CopyType=" + TradePortalConstants.FROM_INSTR
                               + "&BankBranch=" + bankBranch;
%>
              <td>
<%
                link=formMgr.getLinkAsUrl(prevStepAction, prevParm, response);
%>
                <jsp:include page="/common/RolloverButtonLink.jsp">
                  <jsp:param name="name" value="PrevStepButton" />
                  <jsp:param name="image" value='common.PrevStepImg' />
                  <jsp:param name="text" value='common.PrevStepText' />
                  <jsp:param name="link" value="<%=java.net.URLEncoder.encode(link)%>" />
                </jsp:include>
              </td>
              <td width="15" nowrap>&nbsp;</td>
              <td>
                <jsp:include page="/common/RolloverButtonSubmit.jsp">
                  <jsp:param name="showButton" value="<%=(numRows>0)%>" />
                  <jsp:param name="name" value="NextStepButton" />
                  <jsp:param name="image" value='common.NextStepImg' />
                  <jsp:param name="text" value='common.NextStepText' />
                  <jsp:param name="formNum" value='1' />
                  <jsp:param name="extraTags"
                             value="return checkForSelection();" />
                </jsp:include>
              </td>
<%
            }
%>
            <td width="15" nowrap>&nbsp;</td>
            <td>
<%
              link=formMgr.getLinkAsUrl(cancelAction, response);
%>
              <jsp:include page="/common/RolloverButtonLink.jsp">
                <jsp:param name="name"  value="CloseButton" />
                <jsp:param name="image" value='common.CloseImg' />
                <jsp:param name="text"  value='common.CloseText' />
                <jsp:param name="link"  value="<%=java.net.URLEncoder.encode(link)%>" />
              </jsp:include>
            </td>
            <td width="20" nowrap>&nbsp;</td>
          </tr>
        </table>
      </td>
    </tr>
  </table>

</form>
</body>

</html>

<%
  // Finally, reset the cached document to eliminate carryover of
  // information to the next visit of this page.

  if (doc!=null)
    doc.removeAllChildren("/Error");
  formMgr.storeInDocCache("default.doc", doc);
  Debug.debug(formMgr.getFromDocCache().toString());
%>




