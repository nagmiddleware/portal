<%--
*******************************************************************************
                                    User Detail

  Description:
     This page is used to maintain corporate users.  It supports insert,
  update and delete.  Errors found during a save are redisplayed on the same 
  page.  Successful updates return the user to the ref data home page.

*******************************************************************************
--%>

<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.html.*" %>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" 
   scope="session">
</jsp:useBean>


<%-- ************** Data retrieval page setup begins here ****************  --%>

<%
  String  bogOid = userSession.getBogOid();

  BankOrganizationGroupWebBean bog = (BankOrganizationGroupWebBean) beanMgr.createBean(
                 		"com.ams.tradeportal.busobj.webbean.BankOrganizationGroupWebBean",
                             	"BankOrganizationGroup");

  // Attempt to retrieve the BOG.  It should always work.  

  bog.setAttribute("organization_oid", bogOid);
  bog.getDataFromAppServer();

%>

<%-- ********************* HTML for page begins here *********************  --%>

<jsp:include page="/common/ButtonPrep.jsp" />


<%-- Body tag included as part of common header --%>
<jsp:include page="/common/Header.jsp">
   <jsp:param name="includeNavigationBarFlag" 
              value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeErrorSectionFlag"  
              value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>

  <br>

  <table width="100%" border="0" cellspacing="0" cellpadding="0" height="34">
    <tr> 
      <td width="15" class="BankColor" nowrap height="35">&nbsp;</td>
      <td class="BankColor" nowrap height="35"> 
        <p class="ControlLabelWhite">
          <%= resMgr.getText("SupportInformation.SupportInformation", 
                             TradePortalConstants.TEXT_BUNDLE) %>
        </p>
      </td>
      <td width="90%" class="BankColor" height="35">&nbsp;</td>
      <td class=BankColor width=15 align=right valign=middle height=35 nowrap>
	<%=OnlineHelp.createContextSensitiveLink("customer/support_info.htm", 
                                                 resMgr, userSession)%>
      </td>
      <td width="20" class="BankColor" nowrap height="35">&nbsp;</td>
    </tr>
  </table>

  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td width="20" nowrap>&nbsp;</td>
      <td width="100%">&nbsp;</td>
      <td width="20" nowrap>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td align=left>
        <pre class=ListText>
<%-- VERY VERY IMPORTANT!!!  The jsp code that gets the support information 
     MUST start in the first column for the  information to display correctly.     --%>
<%=bog.getAttribute("support_information")%>
        </pre>
      </td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
  </table>


  <table width="100%" border="0" cellspacing="0" cellpadding="0" height="30" 
         class="BankColor">
    <tr> 
      <td width="100%">&nbsp;</td>
      <td>
<%
        String link=formMgr.getLinkAsUrl("goToTradePortalHome", response);
        link = java.net.URLEncoder.encode(link);
%>
        <jsp:include page="/common/RolloverButtonLink.jsp">
           <jsp:param name="name"  value="CloseButton" />
           <jsp:param name="image" value='common.CloseImg' />
           <jsp:param name="text"  value='common.CloseText' />
           <jsp:param name="link"  value="<%=link%>" />
        </jsp:include>
      </td>
      <td width="20" nowrap>&nbsp;</td>
    </tr>
  </table>

  <p>&nbsp;</p>

</body>
</html>
