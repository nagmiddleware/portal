<%--
*******************************************************************************
                              Authorization Failed Page

  Description:
    This is the main driver for the Authorization Failed page.  It handles data
  retrieval of Instrument and Transaction Beans. No From Manager definition was 
  needed since this page only displays data from the Db.  This page is similar
  in nature to the ErrorSection.jsp in that we are display a list of errors.
  Because the needed html to display the list varies from that jsp, the 
  ErrorSection.jsp was broken up to call Authorization_Errors_Html.jsp based
  on a parameter passed into that Error.jsp that indicates it came from this
  jsp.  The Authorization_Errors_Html.jsp contains the specific html needed for
  the ErrorList section in this jsp.
*******************************************************************************
--%>

<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.ams.tradeportal.busobj.webbean.*,com.ams.tradeportal.common.*,
                 com.ams.tradeportal.html.*,java.util.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.frame.*" %>
  
<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session"></jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session"></jsp:useBean>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session"></jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>

<%
   Debug.debug("***START******************* Authorization Errors Detail ***********************START***");
    /*********\
    * GLOBALS *
    \*********/
    boolean readOnly       = true;   //is the page read only
    boolean getDataFromDoc = false;  //do we need to refresh from the doc cache
    boolean showSave       = false;  //do we show the save button
    boolean showDelete     = false;  //do we show the delete button

    String transactionOid    = null; //the oid of the Transaction
    String instrumentOid     = null; //the oid of the Instrument
    String breadCrumbPrefix  = " ";  //Link to Instrument Summary
    String breadCrumbPostfix = " ";  //Link to Transaction Page(s)
    String type		     = null; //Instrument type based on the Instrument Type Code (refdata)
    String errorList	     = null; //the xml string stored in the Transaction WebBean
    String encryptVal1;
    String encryptVal2;
    String urlParm;

    // Now determine where to return to.
    String returnAction;

    returnAction = (String) session.getAttribute( TradePortalConstants.AUTHORIZATION_ERRORS_CLOSE_ACTION );

    DocumentHandler doc = null;			//the doc we will use to get data from cache

  // These are the beans used on the page.
  transactionOid = EncryptDecrypt.decryptStringUsingTripleDes(request.getParameter("transaction_oid"), userSession.getSecretKey());

  TransactionWebBean transaction  = (TransactionWebBean) beanMgr.createBean(
					"com.ams.tradeportal.busobj.webbean.TransactionWebBean", 
					"Transaction");
  transaction.setAttribute("transaction_oid", transactionOid);
  transaction.getDataFromAppServer();

  instrumentOid = transaction.getAttribute("instrument_oid");
  
  InstrumentWebBean instrument    = (InstrumentWebBean) beanMgr.createBean(
					"com.ams.tradeportal.busobj.webbean.InstrumentWebBean",
					"Instrument");
  instrument.setAttribute("instrument_oid", instrumentOid );
  instrument.getDataFromAppServer();

  encryptVal1 = EncryptDecrypt.encryptStringUsingTripleDes( transactionOid, userSession.getSecretKey() ); 	//Transaction_Oid
  encryptVal2 = EncryptDecrypt.encryptStringUsingTripleDes( instrumentOid, userSession.getSecretKey() );	//Instrument_Oid
  urlParm = "&oid=" + encryptVal1 + "&instrument_oid=" + encryptVal2;

  Debug.debug("*** Getting the Errors from the DB ***");
  
  errorList = transaction.getAttribute( "authorization_errors" );

  doc = new DocumentHandler( errorList, false ); 

  // The second Document Handler is only needed because the originally stored
  // errors does not include the "Error" tag around the Errors which the
  // ErrorSection.jsp specifically looks for.  Therefor we needed to merge
  // the data from the original Document into a new Document that has the 
  // Error section in it.

  DocumentHandler doc2 = new DocumentHandler();
  doc2.setAttribute("/Error/", "" );
  doc2.mergeDocument("/Error/", doc.getFragment("/errorlist/"));

  // Store the document into the Cache so that the ErrorSection.jsp can get the 
  // xml data and do it's job (from 1 central location).

  formMgr.storeInDocCache("default.doc", doc2 );        
  Debug.debug("*** The ErrorList is: " + doc2.toString() + "***");


  type = ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.INSTRUMENT_TYPE,
						      instrument.getAttribute("instrument_type_code"));
  breadCrumbPrefix = instrument.getAttribute("complete_instrument_id") + "  " + type;

  breadCrumbPostfix = ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.TRANSACTION_TYPE,
						      transaction.getAttribute("transaction_type_code"));
  

%>

<%-- Body tag included as part of common header --%>

<jsp:include page="/common/Header.jsp">
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeErrorSectionFlag"  value="<%=TradePortalConstants.INDICATOR_NO%>" />
</jsp:include>

<jsp:include page="/common/ButtonPrep.jsp" />
<form name="AuthorizationErrorsDetail" method="post" action="<%=formMgr.getSubmitAction(response)%>">

  <table width="100%" border="0" cellspacing="0" cellpadding="0" height="34" class="BankColor">
    <tr> 
    <%
     /****************************
      * Start Bread Crumb Bar
      ***************************/                 Debug.debug("*** START Bread Crumb Bar ***");
    %>
      <td width="15" class="BankColor" nowrap height="35">&nbsp;</td>
      <td class="BankColor" nowrap height="35"> 
        <p class="ControlLabelWhite"> 
          <%= resMgr.getText("AuthErrors.AuthErrorsFor", TradePortalConstants.TEXT_BUNDLE) %>
          
	  <a href="<%=formMgr.getLinkAsUrl("goToInstrumentSummary", urlParm, response) %>" class="Links">

		<%= breadCrumbPrefix %>
          </a>
	  <span class="ControlLabelWhite">&gt; 
	  <a href="<%=formMgr.getLinkAsUrl("updateTransaction", urlParm, response) %>" class="Links">

		<%= breadCrumbPostfix %>
          </a>
          </span>
	</p>
      </td>
      <td width="90%" class="BankColor" height="35">&nbsp;</td>
      <td width="1" class="BankColor" height="35">&nbsp;</td>
      <td width="1" class="BankColor" height="35">&nbsp;</td>
      <td width="1" class="BankColor" nowrap height="35">&nbsp;</td>
      <td class="BankColor" align="right" valign="middle" width="15" height="35" nowrap>

<%    // -- Online Help button goes here -- (fileName, anchor, resMgr)
	out.print( OnlineHelp.createContextSensitiveLink( "customer/authorisation_errors.htm", "", resMgr, userSession) );
%>
      </td>
      <td width="20" class="BankColor" nowrap height="35">&nbsp;</td>
    </tr>
    <%
     /****************************
      * END Bread Crumb Bar
      ***************************/                 Debug.debug("*** END Bread Crumb Bar ***");
    %>
  </table>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <%
     /****************************
      * Start Error List display
      ***************************/                 Debug.debug("*** START Error List display ***");
    %>
    <tr> 
      <td width="20" nowrap>&nbsp;</td>
      <td>&nbsp;</td>
      <td width="10" nowrap>&nbsp;</td>
      <td>&nbsp;</td>
      <td width="20" nowrap>&nbsp;</td>
    </tr>

<%-- Display the list of Errors by calling a common ErrorSection.jsp. 
     It should be noted that since this will require a differnt set of html,
     that the ErrorSection.jsp will call Authorization_Errors_Html.jsp
     to display the html that is unique to this page.  The ErrorSection.jsp
     centrally locates the common logic needed here.				--%>

    <jsp:include page="/common/ErrorSection.jsp" >
	<jsp:param name="htmlPage" value="<%=TradePortalConstants.AUTHORIZATION_ERRORS_HTML%>" />
    </jsp:include>



    <%
     /****************************
      * END Error List display
      ***************************/                 Debug.debug("*** END Error List display ***");
    %>
  </table>


  <table width="100%" border="0" cellspacing="0" cellpadding="0" height="45" class="BankColor">
    <%
     /****************************
      * Start Table Footer
      ***************************/                 Debug.debug("*** START Table Footer ***");
    %>
    <tr> 
      <td width="20" nowrap>&nbsp;</td>
      <td width="100%" class="BankColor">&nbsp;</td>
      <td width="14">&nbsp;</td>
      <td width="96" nowrap> 
        <table width="96" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td width="8" height="17" align="right">

		<%-- This lays in the common close button logic --%>
          	<jsp:include page="/common/RolloverButtonLink.jsp">
            	  <jsp:param name="showButton" value="true" />
            	  <jsp:param name="name" value="CloseButton" />
            	  <jsp:param name="image" value='common.CloseImg' />
            	  <jsp:param name="text" value='common.CloseText' />
            	  <jsp:param name="link" value="<%=java.net.URLEncoder.encode(formMgr.getLinkAsUrl( returnAction, urlParm, response )) %>" />
            	  <jsp:param name="width" value="" />
          	</jsp:include>
	    </td>
          </tr>
        </table>
      </td>
      <td width="20" nowrap>&nbsp;</td>
    </tr>
    <%
     /********************************
      * END of Table Footer
      *******************************/             Debug.debug("*** END of Table Footer ***");
    %>
  </table>
  <p>&nbsp;</p>

  </form>
</body>
</html>
<%
  /**********
   * CLEAN UP
   **********/

   // Finally, reset the cached document to eliminate carryover of
   // information to the next visit of this page.
   formMgr.storeInDocCache("default.doc", new DocumentHandler());
   Debug.debug("***END******************** Authorization Errors Detail **************************END***");
%>

