<%--
//Maheswar CR 610 7/03/2011 Added this new page
//This page is called from CorporateCustomerDetails.jsp
**********************************************************************************
                             Other Corporate Customer Detail

  Description:
     
This page holds the Subsidary Accounts of the parent Corporate Customer.
Basically for the Corporate Customer detail page when we have to display all these
subsidary accounts we need to have display in read only mode except the checkbox field.

This page is called by one Ajax Call from the CorporateCustomerDetail page.
**********************************************************************************
--%>

<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page
	import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,com.ams.tradeportal.common.*,com.ams.tradeportal.html.*,com.ams.tradeportal.busobj.webbean.*,com.ams.tradeportal.busobj.util.*,java.util.*"%>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager"
	scope="session"></jsp:useBean>
<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager"
	scope="session"></jsp:useBean>
<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager"
	scope="session"></jsp:useBean>
<jsp:useBean id="userSession"
	class="com.ams.tradeportal.busobj.webbean.SessionWebBean"
	scope="session"></jsp:useBean>
<% 



	String orgOID = request.getParameter("org_oid") == null ? ""
			: request.getParameter("org_oid");
			
			
	String originalAccountSize = request.getParameter("originalAccountSize") == null ? ""
			: request.getParameter("originalAccountSize");		
	QueryListView queryListView = null;
	StringBuffer sql = new StringBuffer();
	
	int DEFAULT_ACCOUNT_COUNT = 8;
	DocumentHandler acctList = new DocumentHandler();
	Vector vVector;
	int rowCount = 0;
	boolean defaultCheckBoxValue = false;
	boolean isReadOnly = false;
	String loginLocale = userSession.getUserLocale();
	boolean isOthCorpCustReadOnly = false;
	String options;
	boolean errorFlag = false;
	boolean insertModeFlag = true;
	int originalAcSize=0;
%>
<%-- Maheswar CR-610 17/2/2011 Begin --%>
<div id="OtherCust">
<table width="95%" border="0" cellspacing="0" cellpadding="2" id="customer">
<tr border=1>

	<td width="5" height="46" align="left" valign="bottom" nowrap>&nbsp;</td>

	<td class="ControlLabel" valign="bottom" width="13%" height="46"
		align="left"><%=resMgr.getText("CorpCust.CustomerAccountNumber",
					TradePortalConstants.TEXT_BUNDLE)%><br>
	</td>
	<td class="ControlLabel" valign="bottom" width="13%" height="46"
		align="left" nowrap><%=resMgr.getText("CorpCust.CustomerAccountName",
					TradePortalConstants.TEXT_BUNDLE)%><br>
	</td>
	<td class="ControlLabel" valign="bottom" width="8%" height="46"
		align="left"><%=resMgr.getText("CorpCust.CustomerAccountCurrency",
					TradePortalConstants.TEXT_BUNDLE)%><br>
	</td>
	<td class="ControlLabel" valign="bottom" width="8%" height="46"
		align="left"><%=resMgr.getText("CorpCust.CustomerAccountType",
					TradePortalConstants.TEXT_BUNDLE)%><br>
	</td>
	<td class="ControlLabel" valign="bottom" width="8%" height="46"
		align="left"><%=resMgr.getText("CorpCust.BankCountryCode",
					TradePortalConstants.TEXT_BUNDLE)%><br>
	</td>
	<td class="ControlLabel" valign="bottom" width="8%" height="46"
		align="left"><%=resMgr.getText("CorpCust.SourceSystemOrBranch",
					TradePortalConstants.TEXT_BUNDLE)%><br></td>
					
	
	<td class="ControlLabel" valign="bottom" width="8%" height="46"
		align="left"><%=resMgr.getText("CorpCust.otlCustId",
					TradePortalConstants.TEXT_BUNDLE)%><br>
	</td>
	<td class="ControlLabel" valign="bottom" width="8%" height="46"
		align="left"><%=resMgr.getText("CorpCust.opBankOrgId",
					TradePortalConstants.TEXT_BUNDLE)%><br>
	</td>
	<td class="ControlLabel" valign="bottom" width="8%" height="46"
		align="left"><%=resMgr.getText("CorpCust.fxRateGroup",
					TradePortalConstants.TEXT_BUNDLE)%><br>
	</td>
	
	<%-- td class="ControlLabel" valign="bottom"  width="7%"height="46" align="left">
		  <%=resMgr.getText("CorpCust.AvailableForDebit",
					TradePortalConstants.TEXT_BUNDLE)%><br>
	  </td></!--%>
	
	<td class="ControlLabel" valign="bottom" width="7%" height="46"
		align="left"><%=resMgr.getText("CorpCust.AvailableForDirectDebit",
					TradePortalConstants.TEXT_BUNDLE)%><br>
	</td>
	

	<td class="ControlLabel" valign="bottom" width="7%" height="46"
		align="left"><%=resMgr.getText("CorpCust.AvailableForLoanRequest",
					TradePortalConstants.TEXT_BUNDLE)%><br>
	</td>
	
	<td class="ControlLabel" valign="bottom" width="7%" height="46"
		align="left"><%=resMgr.getText("CorpCust.AvailableForIntPmt",
					TradePortalConstants.TEXT_BUNDLE)%><br>
	</td>
	<td class="ControlLabel" valign="bottom" width="7%" height="46"
		align="left"><%=resMgr.getText("CorpCust.AvailableForTBA",
					TradePortalConstants.TEXT_BUNDLE)%><br>
	</td>
	
	<td class="ControlLabel" valign="bottom" width="7%" height="46"
		align="left"><%=resMgr.getText("CorpCust.AvailableForDomesticPymts",
					TradePortalConstants.TEXT_BUNDLE)%><br>
	</td>
	
	<td class="ControlLabel" valign="bottom" width="7%" height="46"
		align="left"><%=resMgr.getText("CorpCust.accountDeactIndex",
					TradePortalConstants.TEXT_BUNDLE)%><br>
	</td>
	
	<td class="ControlLabel" valign="bottom" width="7%" height="46"
		align="left"><%=resMgr.getText("CorpCust.PanelGroupId",
					TradePortalConstants.TEXT_BUNDLE)%><br>
	</td>
	
	
</tr>
<%



if(originalAccountSize !=null)
{
 originalAcSize= Integer.parseInt(originalAccountSize);
}
//Selects all the subsidary Accounts from the parent CorporateCustomer jsp page.		
queryListView = (QueryListView) EJBObjectFactory.createClientEJB(formMgr.getServerLocation(), "QueryListView"); 
 	 	sql.append("select account_oid, account_number, currency,bank_country_code,account_type,account_name,source_system_branch,available_for_internatnl_pay,available_for_xfer_btwn_accts,a_panel_auth_group_oid_1,available_for_direct_debit,available_for_loan_request,available_for_domestic_pymts,pending_transaction_balance, deactivate_indicator, pending_transac_credit_balance,a_op_bank_org_oid,fx_rate_group,proponix_customer_id,othercorp_customer_indicator");
 	 	sql.append(" from account");
 	sql.append(" where p_owner_oid = " + orgOID);
	//BSL IR HHUL070671455 07/07/11 Begin
	// Account list should not contain accounts from sub-subsidiaries of this subsidiary.
 	sql.append(" and othercorp_customer_indicator = '").append(TradePortalConstants.INDICATOR_NO).append("'");
	//BSL IR HHUL070671455 07/07/11 End
	sql.append(" order by ");
 	sql.append(resMgr.localizeOrderBy("account_number"));
 	Debug.debug(sql.toString());

 	queryListView.setSQL(sql.toString());
 	queryListView.getRecords();

 	acctList = queryListView.getXmlResultSet();
 	vVector = acctList.getFragments("/ResultSetRecord");
 	int numItems = vVector.size()+originalAcSize;

 	
 	if (numItems > 17) {
 		rowCount = numItems;

 	}
 	
 	for (int iLoop = originalAcSize; iLoop < numItems; iLoop++) {
 		
 		DocumentHandler account = (DocumentHandler) vVector
 				.elementAt(iLoop-originalAcSize);
 %>
<tr>


	<td class="ControlLabel" valign="bottom" height="25">
	
	<%	defaultCheckBoxValue = false;
	
	if(!InstrumentServices
	.isBlank(account.getAttribute("/OTHERCORP_CUSTOMER_INDICATOR")) && account
			.getAttribute("/OTHERCORP_CUSTOMER_INDICATOR").equals(TradePortalConstants.INDICATOR_YES)){
	 defaultCheckBoxValue = true;
	}
	else{
		 defaultCheckBoxValue = false;
	}
	
			out.print(InputField.createCheckboxField(TradePortalConstants.OTHER_ACCOUNT_CHECKBOX
					+ iLoop, TradePortalConstants.INDICATOR_YES, "",
					defaultCheckBoxValue, "ListText", "ListText",
					isOthCorpCustReadOnly, ""));
			//This is a separate hidden field which is passed to the premediator CorporateOrgPreEdit where we are looking
			//this field to loop and find out the values of otheraccountcheckbox 
			//we are doing this step because Ajax call doesnot pass the this jsp content as part of inputDoc
			
			 out.print("<INPUT TYPE=HIDDEN NAME='"+TradePortalConstants.OTHER_ACCOUNT_OID + iLoop + "' VALUE='" + account.getAttribute("/ACCOUNT_OID") + "'>");
	%>
	</td>
	
	<td class="ControlLabel" valign="bottom" height="25">
	<%
		
			String disabledField = "DISABLED";
			String disableText = "";
			String zeroCheck = "0"; 

			if ((!InstrumentServices.isBlank(account
					.getAttribute("/PENDING_TRANSACTION_BALANCE")))
					|| ((!InstrumentServices
							.isBlank(account
									.getAttribute("/PENDING_TRANSAC_CREDIT_BALANCE"))) && 
					(!zeroCheck
							.equals(account
									.getAttribute("/PENDING_TRANSAC_CREDIT_BALANCE"))))) 
			{

				disabledField = "DISABLED";
				disableText = "_D";
				
			}
			
	%> <%=InputField.createTextField("acctNum" + iLoop,
						account.getAttribute("/ACCOUNT_NUMBER"), "30", "30",
						"ListText", isReadOnly, disabledField)%></td>
	<td class="ControlLabel" valign="bottom" height="25">
	<%=InputField.createTextField("acctName" + iLoop,
						account.getAttribute("/ACCOUNT_NAME"), "35", "35",
						"ListText", isReadOnly, disabledField)%></td>
	<td class="ControlLabel" valign="bottom" height="25" align="left">	
	  <% options = Dropdown.createSortedCurrencyCodeOptions(account.getAttribute("/CURRENCY"),loginLocale); 
	  out.println(InputField.createSelectField("acctCurr" + iLoop + disableText, "", " ", options, "ListText", isReadOnly,disabledField)); %>
	</td>
	
	<td class="ControlLabel" valign="bottom" height="25">
	 
	<%=InputField.createTextField("acctType" + iLoop,
						account.getAttribute("/ACCOUNT_TYPE"), "3", "3",
						"ListText", isReadOnly, disabledField)%>
						</td>
	<td class="ControlLabel" valign="bottom" height="25" align="left">
	<%
		options = Dropdown.createSortedBankCountryCodeOptions(
					account.getAttribute("/BANK_COUNTRY_CODE"), loginLocale);
	
 	out.println(InputField.createSelectField("bankCountryCode"
 				+ iLoop + disableText, "", " ", options, "ListText",
 				isReadOnly, disabledField));
 %>
	</td>
	<td class="ControlLabel" valign="bottom" height="25">
	<%=InputField.createTextField("sourceSystemBranch"
						+ iLoop, account.getAttribute("/SOURCE_SYSTEM_BRANCH"),
						"3", "3", "ListText", isReadOnly, disabledField)%></td>

	<td class="ControlLabel" valign="bottom" height="25">
	<%=InputField.createTextField("PropCustomerId"
						+ iLoop, account.getAttribute("/PROPONIX_CUSTOMER_ID"),
						"20", "20", "ListText", isReadOnly, disabledField)%></td>
	<td class="ControlLabel" valign="bottom" height="25" align="left">
	<%
		StringBuffer sqlQuery = new StringBuffer();
			  sqlQuery.append("select organization_oid, name from operational_bank_org where organization_oid in (select a_first_op_bank_org from corporate_org where organization_oid = ");
			  sqlQuery.append(orgOID);
			  sqlQuery.append(" and a_first_op_bank_org is not NULL union select a_second_op_bank_org from corporate_org where organization_oid=");
			  sqlQuery.append(orgOID);		  
			  sqlQuery.append(" union select a_third_op_bank_org from corporate_org where organization_oid=");
			  sqlQuery.append(orgOID);  
			  sqlQuery.append(" union select a_fourth_op_bank_org from corporate_org where organization_oid=");
			  sqlQuery.append(orgOID);
			  sqlQuery.append(")");
			  
		     
		      queryListView.setSQL(sqlQuery.toString());
		      queryListView.getRecords();		      
		
		      DocumentHandler operationalBankOrgsDoc = queryListView.getXmlResultSet();	
		      String dropdownOptions = ListBox.createOptionList(operationalBankOrgsDoc, "ORGANIZATION_OID", "NAME", 
					  account.getAttribute("/A_OP_BANK_ORG_OID"), userSession.getSecretKey());					 
		
		             out.print(InputField.createSelectField("opBankOrgId"+ iLoop, "", " ", dropdownOptions, "ListText", 
                                                    isReadOnly,disabledField));
	%>
	</td>
	<td class="ControlLabel" valign="bottom" height="25">
	<%=InputField.createTextField("fxRate" + iLoop,
						account.getAttribute("/FX_RATE_GROUP"), "3", "3",
						"ListText", isReadOnly, disabledField)%>
</td>
	<td class="ControlLabel" valign="bottom" height="25">
	<%
		if (orgOID == null) {
				insertModeFlag = true;
			}
			else {
				insertModeFlag = false;
				errorFlag = true;
			}
			if ((!insertModeFlag || errorFlag)
					&& TradePortalConstants.INDICATOR_YES.equals(account
							.getAttribute("/AVAILABLE_FOR_DIRECT_DEBIT"))) {
				defaultCheckBoxValue = true;
			} else {
				defaultCheckBoxValue = false;

			}
			out.print(InputField.createCheckboxField(
					"availableForDirectDebit" + iLoop,
					TradePortalConstants.INDICATOR_YES, "",
					defaultCheckBoxValue, "ListText", "ListText",
					isReadOnly, disabledField));
	%>
	</td>
	
	<td class="ControlLabel" valign="bottom" height="25">
	<%
		if ((!insertModeFlag || errorFlag)
					&& TradePortalConstants.INDICATOR_YES.equals(account
							.getAttribute("/AVAILABLE_FOR_LOAN_REQUEST"))) {
				defaultCheckBoxValue = true;
			} else {
				defaultCheckBoxValue = false;
			}
			out.print(InputField.createCheckboxField("availableForLoanReq"
					+ iLoop, TradePortalConstants.INDICATOR_YES, "",
					defaultCheckBoxValue, "ListText", "ListText",
					isReadOnly, disabledField));
	%>
	</td>
	
	<td class="ControlLabel" valign="bottom" height="25">
	<%
		if ((!insertModeFlag || errorFlag)
					&& TradePortalConstants.INDICATOR_YES.equals(account
							.getAttribute("/AVAILABLE_FOR_INTERNATNL_PAY"))) {
				defaultCheckBoxValue = true;
			} else {
				defaultCheckBoxValue = false;
			}
			out.print(InputField.createCheckboxField("availableForIntPmt"
					+ iLoop, TradePortalConstants.INDICATOR_YES, "",
					defaultCheckBoxValue, "ListText", "ListText",
					isReadOnly, disabledField));
	%>
	</td>
	<td class="ControlLabel" valign="bottom" height="25">
	<%
		if ((!insertModeFlag || errorFlag)
					&& TradePortalConstants.INDICATOR_YES.equals(account
							.getAttribute("/AVAILABLE_FOR_XFER_BTWN_ACCTS"))) {
				defaultCheckBoxValue = true;
			} else {
				defaultCheckBoxValue = false;
			}
			out.print(InputField.createCheckboxField("availableForTBA"
					+ iLoop, TradePortalConstants.INDICATOR_YES, "",
					defaultCheckBoxValue, "ListText", "ListText",
					isReadOnly, disabledField));
	%>
	</td>
	
	<td class="ControlLabel" valign="bottom" height="25">
	<%
		if ((!insertModeFlag || errorFlag)
					&& TradePortalConstants.INDICATOR_YES.equals(account
							.getAttribute("/AVAILABLE_FOR_DOMESTIC_PYMTS"))) {
				defaultCheckBoxValue = true;
			} else {
				defaultCheckBoxValue = false;
			}
			out.print(InputField.createCheckboxField(
					"availableForDomesticPymt" + iLoop,
					TradePortalConstants.INDICATOR_YES, "",
					defaultCheckBoxValue, "ListText", "ListText",
					isReadOnly, disabledField));
	%>
	</td>

	<td class="ControlLabel" valign="bottom" height="25">
	<%
		if ((!insertModeFlag || errorFlag)
					&& TradePortalConstants.INDICATOR_YES.equals(account
							.getAttribute("/DEACTIVATE_INDICATOR"))) {
				defaultCheckBoxValue = true;
			} else {
				defaultCheckBoxValue = false;
			}
			out.print(InputField.createCheckboxField("deactivateIndicator"
					+ iLoop, TradePortalConstants.INDICATOR_YES, "",
					defaultCheckBoxValue, "ListText", "ListText",
					isReadOnly, disabledField));
	%>
	</td>

	<td class="ControlLabel" valign="bottom" height="25">
	<%
		String panelAuthGroupSql = "select panel_auth_group_oid, pag.panel_auth_group_id as PAGID from panel_auth_group pag where p_corp_org_oid = "
					+ orgOID;		
			DocumentHandler panelAuthgroupList = DatabaseQueryBean
					.getXmlResultSet(panelAuthGroupSql.toString(), false);
			String curPAGId = account.getAttribute("/A_PANEL_AUTH_GROUP_OID");
			if (curPAGId == null)
				curPAGId = "";
			String pagOptions = ListBox.createOptionList(
					panelAuthgroupList, "A_PANEL_AUTH_GROUP_OID", "PAGID",
					curPAGId);
			out.println(InputField.createSelectField("pagIdSelection"
					+ iLoop, "", "----", pagOptions, "ListText",
					isReadOnly, disabledField));
	%>
	</td>
</tr>
<%
	}
%>
</table>
<%-- Suresh IR-SHUL042254328 04/28/2011 Begin --%>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td width="37" nowrap align="top">
        <img src="/portal/images/UpArrow.gif" width="37" >
      </td>
      <td nowrap>
      <table>
	   <tr> 
       	 <td nowrap> 
             <jsp:include page="/common/RolloverButtonSubmit.jsp">
                         <jsp:param name="name"  value="AddAccounts" />
                         <jsp:param name="image" value="common.AddAccountsImg" />
                         <jsp:param name="text"  value="OtherCorpCust.AddAccounts" />
                         <jsp:param name="submitButton"  value="AddAccounts" />
              </jsp:include>
        </td>
      </tr>
     </table>
     </td>
     <td width="100%" align="right">&nbsp;</td>
   </tr>
</table>
<%-- Suresh IR-SHUL042254328 04/28/2011 End --%>
</div>