<%--
 *
 *     Copyright  � 2008                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
**********************************************************************************
                        Direct Debit Transaction History Tab

  Description:
    Contains HTML to create the History tab for the Direct Debit Transaction History page

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transactions-HistoryListView.jsp" %>
*******************************************************************************
--%>

<%

   //StringBuffer statusOptions = new StringBuffer();         // NSX - PRUK080636392 - 08/16/10 -
   StringBuffer statusExtraTags = new StringBuffer();

     // NSX - PRUK080636392 - 08/16/10 - Begin
   StringBuffer statusOptions = Dropdown.createActiveInActiveStatusOptions(resMgr,selectedStatus);
/*
   // Build the status dropdown options (hardcoded in ACTIVE (default), INACTIVE, ALL order), 
   // re-selecting the most recently selected option.
   statusOptions.append("<option value='");
   statusOptions.append(STATUS_ACTIVE);
   statusOptions.append("' ");
   if (STATUS_ACTIVE.equals(selectedStatus)) {
      statusOptions.append(" selected ");
   }
   statusOptions.append(">");
   statusOptions.append(STATUS_ACTIVE);
   statusOptions.append("</option>");

   statusOptions.append("<option value='");
   statusOptions.append(STATUS_INACTIVE);
   statusOptions.append("' ");
   if (STATUS_INACTIVE.equals(selectedStatus)) {
      statusOptions.append(" selected ");
   }
   statusOptions.append(">");
   statusOptions.append(STATUS_INACTIVE);
   statusOptions.append("</option>");

   statusOptions.append("<option value='");
   statusOptions.append(STATUS_ALL);
   statusOptions.append("' ");
   if (STATUS_ALL.equals(selectedStatus)) {
      statusOptions.append(" selected ");
   }
   statusOptions.append(">");
   statusOptions.append(STATUS_ALL);
   statusOptions.append("</option>");
*/        // NSX - PRUK080636392 - 08/16/10 - End
   // Upon changing the status selection, automatically go back to the Transactions Home
   // page with the selected status type.  Force a new search to occur (causes cached
   // "where" clause to be reset.)

   statusExtraTags.append("onchange=\"location='");
   statusExtraTags.append(formMgr.getLinkAsUrl("goToDirectDebitsSearch", response));
   statusExtraTags.append("&amp;current2ndNav=");
   statusExtraTags.append(current2ndNav);
   statusExtraTags.append("&NewDropdownSearch=Y");
   statusExtraTags.append("&amp;instrStatusType='+this.options[this.selectedIndex].value\"");
%>

		                                       
<div>
		<%
		
      // When the Instrument History tab is selected, display 
      // Organizations dropdown or the single organization.
      if ((SecurityAccess.hasRights(userSecurityRights, SecurityAccess.VIEW_CHILD_ORG_WORK)) 
           && (totalOrganizations > 1))
      {
      	 orgListDropDown = true;
          dropdownOptions.append(Dropdown.getUnsortedOptions(hierarchyDoc, 
                                                             "ORGANIZATION_OID", "NAME", 
                                                             selectedOrg, true));
          dropdownOptions.append("<option value=\"");
          dropdownOptions.append(EncryptDecrypt.encryptAlphaNumericString(ALL_ORGANIZATIONS));
          dropdownOptions.append("\"");

          if (selectedOrg.equals(ALL_ORGANIZATIONS)) {
             dropdownOptions.append(" selected");
          }

          dropdownOptions.append(">");
          dropdownOptions.append(ALL_ORGANIZATIONS);
          dropdownOptions.append("</option>");

          extraTags = new StringBuffer("");
          extraTags.append("onchange=\"location='");
          extraTags.append(formMgr.getLinkAsUrl("goToDirectDebitsSearch", response));
          extraTags.append("&amp;current2ndNav=");
          extraTags.append(current2ndNav);
          extraTags.append("&NewDropdownSearch=Y");
          extraTags.append("&historyOrg='+this.options[this.selectedIndex].value\"");
		 /**
          out.print(InputField.createSelectField("Organization", "", "", 
                                                 dropdownOptions.toString(), 
                                                 "ListText", false, 
                                                 extraTags.toString()));
		**/
		%>&nbsp;&nbsp;<%
		out.println(widgetFactory.createLabel("","Transactions For",false, false, false, "inline"));
		 out.println(widgetFactory.createSelectField("org", "",
			"", dropdownOptions.toString(), false,
			false, false, 
			"onChange='searchDirectDebitHistory();'", "", "inline"));
      }%>
<%= widgetFactory.createCheckboxField("Active", "Active", false, false, false,"onClick='searchDirectDebitHistory();'","","inline") %>
<%= widgetFactory.createCheckboxField("InActive", "Inactive", false, false,false,"onClick='searchDirectDebitHistory();'","","inline") %>
<div style="clear:both"></div>
</div>

		<%
			String linkName = "Basic";
			String paramStr = "&SearchType=";
			
			if (searchType == null) {
				searchType = TradePortalConstants.SIMPLE;
				paramStr=paramStr+TradePortalConstants.ADVANCED;
			} else if (searchType.equals(TradePortalConstants.ADVANCED)) {
				linkName = "Basic";
				paramStr=paramStr+TradePortalConstants.SIMPLE;
			} else if (searchType.equals(TradePortalConstants.SIMPLE)) {
				linkName = "Advanced";
				paramStr=paramStr+TradePortalConstants.ADVANCED;
			}
		
			String linkHref = formMgr.getLinkAsUrl("goToDirectDebitsSearch", paramStr, response);
		%>      
 
		<%
		 if (searchType.equals(TradePortalConstants.ADVANCED)) {
		%> <%@ include file="/transactions/fragments/ExistingDirectDebitSearch-AdvanceFilter.frag"%>
				<%
		  } else {
		%> <%@ include file="/transactions/fragments/ExistingDirectDebitSearch-BasicFilter.frag"%>
				<%
		  }
		  
			  gridHtml = dgFac.createDataGrid("existingDirectDebitSearchGridId","ExistingDirectDebitSearchDataGrid",null);
		%> 

		<%= gridHtml%>


<input type=hidden name=SearchType value=<%=searchType%>> 
		

