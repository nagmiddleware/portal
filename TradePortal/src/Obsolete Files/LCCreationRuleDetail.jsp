<%--
*******************************************************************************
                               LC Creation Rule Detail

  Description:
     This page is used to maintain LC creation rules.  It supports insert,
  update and delete.  Errors found during a save are redisplayed on the same 
  page.  Successful updates return the user to the ref data home page.

*******************************************************************************
--%>

<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.html.*, 
                 com.ams.tradeportal.busobj.*" %>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" 
   scope="session">
</jsp:useBean>


<%-- ************** Data retrieval page setup begins here ****************  --%>

<%
  boolean isReadOnly = false;

  boolean isViewInReadOnly = false;

  String defnName = "";

  String options;
  String fieldOptions = "";
  String defaultText;
  String link;

  String oid = "";
  String definitionOid = "";

  // These booleans help determine the mode of the JSP.  
  boolean getDataFromDoc = false;
  boolean retrieveFromDB = false;
  boolean insertMode = true;

  boolean showSave = true;
  boolean showDelete = true;

  DocumentHandler doc;

  // Must initialize these 2 doc, otherwise get a compile error.
  DocumentHandler templatesDoc = null;
  DocumentHandler opBankOrgsDoc = null;

  LCCreationRuleWebBean rule = (LCCreationRuleWebBean)beanMgr.createBean(
               			  "com.ams.tradeportal.busobj.webbean.LCCreationRuleWebBean",
                       		  "LCCreationRule");
  POUploadDefinitionWebBean uploadDefn = (POUploadDefinitionWebBean)beanMgr.createBean(
               			  "com.ams.tradeportal.busobj.webbean.POUploadDefinitionWebBean",
                       		  "POUploadDefinition");

  String loginLocale;
  String loginRights;
  String viewInReadOnly;

  viewInReadOnly = request.getParameter("fromHomePage");

  //If we got to this page from the Home Page, then we need to explicitly be in Read only mode.
  //Also, the close button action needs to take us back to the home page.  This flag will help
  //us keep track of where to go.

  if( (InstrumentServices.isNotBlank(viewInReadOnly)) &&
      (viewInReadOnly.equals("true")) )
  {	
	isViewInReadOnly = true;
  }

  // Get the webbeans for the login in user and his security profile.  Set the
  // readonly access appropriately.
  loginLocale = userSession.getUserLocale();
  loginRights = userSession.getSecurityRights();

  if (SecurityAccess.hasRights(loginRights, SecurityAccess.MAINTAIN_LC_CREATE_RULES)) {
     isReadOnly = false;
  } else if (SecurityAccess.hasRights(loginRights,
                                         SecurityAccess.VIEW_LC_CREATE_RULES)) {
     isReadOnly = true;
  } else {
     // Don't have maintain or view access, send the user back.
        formMgr.setCurrPage("RefDataHome");
        String physicalPage = NavigationManager.getNavMan().getPhysicalPage("RefDataHome", request);
%>
        <jsp:forward page='<%= physicalPage %>' />
<%
        return;
  }

  // We should only be in this page for corporate users.  If the logged in
  // user's level is not corporate, send back to ref data page.  This
  // condition should never occur but is caught just in case.
  String ownerLevel = userSession.getOwnershipLevel();
  if (!ownerLevel.equals(TradePortalConstants.OWNER_CORPORATE)) {
     formMgr.setCurrPage("RefDataHome");
     String physicalPage = NavigationManager.getNavMan().getPhysicalPage("RefDataHome", request);
%>
     <jsp:forward page='<%= physicalPage %>' />
<%
     return;
  }

  if (isViewInReadOnly) isReadOnly = true;

  Debug.debug("login_rights is " + loginRights);

  // Get the document from the cache.  We'll use it to repopulate the screen 
  // if the user was entering data with errors.
  doc = formMgr.getFromDocCache();

  Debug.debug("doc from cache is " + doc.toString());

  if (doc.getDocumentNode("/In/LCCreationRule") == null ) {
     // The document does not exist in the cache.  We are entering the page from
     // the listview page (meaning we're opening an existing item or creating 
     // a new one.)

     if (request.getParameter("oid") != null) {
        oid = EncryptDecrypt.decryptStringUsingTripleDes(request.getParameter("oid"), userSession.getSecretKey());
     } else {
        oid = null;
     }

     if ((oid == null) || (oid.equals(""))) {
       getDataFromDoc = false;
       retrieveFromDB = false;
       insertMode = true;
       oid = "0";
     } else if (oid.equals("0")) {
       getDataFromDoc = false;
       retrieveFromDB = false;
       insertMode = true;
     } else {
       // We have a good oid so we can retrieve.
       getDataFromDoc = false;
       retrieveFromDB = true;
       insertMode = false;
     }
  } else {
     // The doc does exist so check for errors.  If highest severity < WARNING,
     // its a good update.

     String maxError = doc.getAttribute("/Error/maxerrorseverity");

     if (maxError.compareTo(
                  String.valueOf(ErrorManager.WARNING_ERROR_SEVERITY)) < 0) {
        // We've returned from a save/update/delete that was successful
        // We shouldn't be here.  Forward to the RefDataHome page.
        formMgr.setCurrPage("RefDataHome");
        String physicalPage = NavigationManager.getNavMan().getPhysicalPage("RefDataHome", request);
%>
        <jsp:forward page='<%= physicalPage %>' />
<%
        return;
     } else {
        // We've returned from a save/update/delete but have errors.
        // We will display data from the cache.  Determine if we're
        // in insertMode by looking for a '0' oid in the input section
        // of the doc.

        retrieveFromDB = false;
        getDataFromDoc = true;

        String newOid = doc.getAttribute("/In/LCCreationRule/lc_creation_rule_oid");

        if (newOid.equals("0")) {
           insertMode = true;
           oid = "0";
        } else {
           // Not in insert mode, use oid from input doc.
           insertMode = false;
           oid = doc.getAttribute("/In/LCCreationRule/lc_creation_rule_oid");
        }
     }
  }

  if (retrieveFromDB) {
     // Attempt to retrieve the item.  It should always work.  Still, we'll
     // check for a blank oid -- this indicates record not found.  Set to 
     // insert mode and display error.

     getDataFromDoc = false;

     rule.setAttribute("lc_creation_rule_oid", oid);
     rule.getDataFromAppServer();

     if (rule.getAttribute("lc_creation_rule_oid").equals("")) {
       insertMode = true;
       oid = "0";
       getDataFromDoc = false;
     } else {
       insertMode = false;
     }
  } 

  if (getDataFromDoc) {
     // Populate the user bean with the input doc.
     try {
        rule.populateFromXmlDoc(doc.getComponent("/In"));
     } catch (Exception e) {
        out.println("Contact Administrator: Unable to get LC Creation Rule attributes "
             + " for oid: " + oid + " " + e.toString());
     }
  }



  // TLE - 01/23/07 - IR#FRUH012138122 - Add Begin 
  Hashtable secureParms = new Hashtable();
  String CurrentOrgOid = userSession.getOwnerOrgOid();
  String existingOrgOid = rule.getAttribute("owner_org_oid");

  if( InstrumentServices.isNotBlank( existingOrgOid )) {
      secureParms.put("owner_org_oid",existingOrgOid);

      if (!CurrentOrgOid.equals(existingOrgOid)) {
         isReadOnly = true;
      }

  } else {
      secureParms.put("owner_org_oid",userSession.getOwnerOrgOid()); 
  }
  // TLE - 01/23/07 - IR#FRUH012138122 - Add End 



  // This println is useful for debugging the state of the jsp.
  Debug.debug("retrieveFromDB: " + retrieveFromDB + " getDataFromDoc: "
              + getDataFromDoc + " insertMode: " + insertMode + " oid: "
              + oid);

  if (isReadOnly || insertMode) showDelete = false;
  if (isReadOnly) showSave = false;
%>

<%
  // Retrieve the data used to populate some of the dropdowns.

  StringBuffer sql = new StringBuffer();

  QueryListView queryListView = null;

  try {
      queryListView = (QueryListView) EJBObjectFactory.createClientEJB(
				formMgr.getServerLocation(), "QueryListView"); 

      sql.append("select template_oid, name ");
      sql.append(" from template t, instrument i");
      sql.append(" where t.p_owner_org_oid in (");
      sql.append(userSession.getOwnerOrgOid());

      // Also include templates from the user's actual organization if using subsidiary access
      if(userSession.showOrgDataUnderSubAccess())
       {
          sql.append(",").append(userSession.getSavedUserSession().getOwnerOrgOid());
       }

      sql.append(") and i.instrument_oid = t.c_template_instr_oid");
      sql.append(" and i.instrument_type_code = '");
      sql.append(TradePortalConstants.IMPORT_DLC);
      sql.append("'");
      sql.append(" order by ");
      sql.append(resMgr.localizeOrderBy("name"));
      Debug.debug(sql.toString());

      queryListView.setSQL(sql.toString());

      queryListView.getRecords();

      templatesDoc = queryListView.getXmlResultSet();
      Debug.debug(templatesDoc.toString());

      sql = new StringBuffer();
      sql.append("select b.organization_oid, b.name ");
      sql.append(" from corporate_org c, operational_bank_org b");
      sql.append(" where c.organization_oid = " + userSession.getOwnerOrgOid());
      sql.append(" and b.organization_oid in (c.a_first_op_bank_org, c.a_second_op_bank_org, ");
      sql.append(" c.a_third_op_bank_org, c.a_fourth_op_bank_org)");
      sql.append(" order by ");
      sql.append(resMgr.localizeOrderBy("b.name"));
      Debug.debug(sql.toString());
      queryListView.setSQL(sql.toString());

      queryListView.getRecords();

      opBankOrgsDoc = queryListView.getXmlResultSet();
      Debug.debug(opBankOrgsDoc.toString());
 
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      try {
          if (queryListView != null) {
              queryListView.remove();
          }
      } catch (Exception e) {
          System.out.println("error removing querylistview in LCCreationRuleDetail.jsp");
      }
  }  // try/catch/finally block

  // Now attempt to retrieve the PO Upload Definition object.  This is used to
  // display the Upload Defn name and fill in the dropdown fields with the data
  // items.

  try {
    // In insert mode we are passed a po upload definition oid.  Otherwise we
    // use the oid from the LC Creation Rule webbean.
    if (insertMode) {
      definitionOid = request.getParameter("UploadDefinition");
      String defOidFromPoUploadPage = doc.getAttribute("/Out/oid");
      if (InstrumentServices.isBlank(definitionOid))
       {
        if(InstrumentServices.isBlank(doc.getAttribute("/Out/oid")))
          {   
            // In the case where we're in insert mode but have errors, the upload defn
            // oid needs to be retrieved from the document rather than the request.
            // We'll assume that a blank definition means we had errors.
            definitionOid = doc.getAttribute("/In/LCCreationRule/po_upload_def_oid");
          }
         else
          {
             // We're coming from the PO Upload Definition page and the user has
             // selected to create an LC creation rule for a PO Upload Definition
             // Get the PO upload def oid from the results of the mediator
             definitionOid = EncryptDecrypt.decryptStringUsingTripleDes(doc.getAttribute("/Out/oid"), userSession.getSecretKey());
          }
      }  else {
        // Otherwise, we found an encrypted oid from the request.
        definitionOid = EncryptDecrypt.decryptStringUsingTripleDes(definitionOid, userSession.getSecretKey());
      }
    } else {
      // Not in insert mode, get the oid from the webbean.
      definitionOid = rule.getAttribute("po_upload_def_oid");
    }
    uploadDefn.setAttribute("po_upload_definition_oid", definitionOid);
    uploadDefn.getDataFromAppServer();

    fieldOptions = uploadDefn.buildFieldOptionList();
    defnName = uploadDefn.getAttribute("name");

  } catch (Exception e) {
    e.printStackTrace();
  }
%>

<%-- ********************* HTML for page begins here *********************  --%>

<jsp:include page="/common/ButtonPrep.jsp" />

<%
  // onLoad is set to default the cursor to the UserId field but only if the 
  // page is not in readonly mode.
  // Auto save the form when time-out if not readonly.  
  // (Header.jsp will check for auto-save setting of the corporate org).
  String onLoad = "";
  if (!isReadOnly) {
    onLoad = "document.LCCreationRuleDetail.RuleName.focus();";
  }
  String autoSaveFlag = isReadOnly ? TradePortalConstants.INDICATOR_NO : TradePortalConstants.INDICATOR_YES;

%>


<%-- Body tag included as part of common header --%>
<jsp:include page="/common/Header.jsp">
   <jsp:param name="includeNavigationBarFlag" 
              value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeErrorSectionFlag"  
              value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="additionalOnLoad" value="<%=onLoad%>" />
   <jsp:param name="autoSaveFlag"     value="<%=autoSaveFlag%>" />
</jsp:include>


<form name="LCCreationRuleDetail" method="post" 
      action="<%=formMgr.getSubmitAction(response)%>">
  <input type=hidden value="" name=buttonName>

<%
  // Store values such as the userid, his security rights, and his org in a secure
  // hashtable for the form.  The mediator needs this information.


  // TLE - 01/23/07 - IR#FRUH012138122 - Add Begin - Moved this up 
  //Hashtable secureParms = new Hashtable();
  // TLE - 01/23/07 - IR#FRUH012138122 - Add End 
  secureParms.put("login_oid", userSession.getUserOid());
  secureParms.put("owner_org_oid", userSession.getOwnerOrgOid());
  secureParms.put("login_rights", loginRights);
  secureParms.put("opt_lock",  rule.getAttribute("opt_lock"));
  secureParms.put("lc_creation_rule_oid", oid);
  secureParms.put("po_upload_def_oid", definitionOid);
%>

<%= formMgr.getFormInstanceAsInputField("LCCreationRuleDetailForm", secureParms) %>

  <input type="hidden" name="InstrumentType" value="IMP_DLC">

  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td width="15" class="BankColor" nowrap>&nbsp;</td>
      <td class="BankColor"  nowrap >
        <span class="Tabtext">
          <span class="Links">
            <a href=<%=formMgr.getLinkAsUrl("goToRefDataHome", response) %>
               class="Links">
               <%=resMgr.getText("LCCreationRuleDetail.LCCreationRules", 
                                 TradePortalConstants.TEXT_BUNDLE)%>
            </a>
          </span> 
          <span class="ControlLabelWhite">
            &gt; <%=(insertMode)
                       ?resMgr.getText("LCCreationRuleDetail.NewRule", 
                                     TradePortalConstants.TEXT_BUNDLE)
                       :rule.getAttribute("name")%>
          </span>
        </span>
      </td>
      <td width="100%" class="BankColor">&nbsp;</td>
      <td colspan=5 class="BankColor" nowrap width="96"> 

<%
        if (isViewInReadOnly) {
           link = "goToTradePortalHome";
        } else {
           link = "goToRefDataHome";
        }
%>
        <jsp:include page="/common/SaveDeleteClose.jsp">
           <jsp:param name="showSaveButton" value="<%=showSave%>" />
           <jsp:param name="showDeleteButton" value="<%=showDelete%>" />
           <jsp:param name="showCloseButton" value="true" />
           <jsp:param name="showHelpButton" value="true" />
           <jsp:param name="cancelAction" value="<%=link %>" />
           <jsp:param name="extraSaveTags" value="" />
	     <jsp:param name="helpLink" value="customer/lc_creation_rule.htm" />
        </jsp:include>

      </td>
    </tr>
  </table>

  <table width=100% border=0 cellspacing=0 cellpadding=0 class="ColorBeige">
    <tr>
      <td width=20 nowrap>&nbsp;</td>
      <td width="100%"> 
        <p class="AsterixText">
          <span class="Asterix">*</span>
            <%=resMgr.getText("common.Required",
                              TradePortalConstants.TEXT_BUNDLE)%>
        </p>
      </td>
    </tr>
  </table>

  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td width="20" nowrap>&nbsp;</td>
      <td nowrap>
        <p class="ControlLabel">
          <%=resMgr.getText("LCCreationRuleDetail.RuleName", 
                            TradePortalConstants.TEXT_BUNDLE)%>
          <span class="Asterix">*</span>
        </p>
      </td>
      <td width="40" nowrap>&nbsp;</td>
      <td nowrap>
        <p class="ControlLabel">
          <%=resMgr.getText("LCCreationRuleDetail.POUploadDefinition", 
                            TradePortalConstants.TEXT_BUNDLE)%>
        </p>
      </td>
      <td width="100%">&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td class="ListText"> 
        <%=InputField.createTextField("RuleName",
                                      rule.getAttribute("name"),
                                      "35", "35", "ListText", isReadOnly)%>
      </td>
      <td>&nbsp;</td>
      <td>
        <p class="ListText">
          <%=defnName%>
        </p>
      </td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td nowrap>
        <p class="ControlLabel">
          <%=resMgr.getText("LCCreationRuleDetail.Description", 
                            TradePortalConstants.TEXT_BUNDLE)%>
          <span class="Asterix">*</span>
        </p>
      </td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td class="ListText"> 
        <%=InputField.createTextField("Description",
                                      rule.getAttribute("description"),
                                      "35", "35", "ListText", isReadOnly)%>
      </td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
  </table>

  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td width="20" nowrap>&nbsp;</td>
      <td nowrap>
        <p class="ControlLabel">
          <%=resMgr.getText("LCCreationRuleDetail.WhenCritieriaMet", 
                            TradePortalConstants.TEXT_BUNDLE)%>
        </p>
      </td>
      <td width="100%">&nbsp;</td>
    </tr>
  </table>

  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td width="30" nowrap>&nbsp;</td>
      <td nowrap> 
        <p class="ControlLabel">
          <%=resMgr.getText("LCCreationRuleDetail.CreateUsingTemplate", 
                            TradePortalConstants.TEXT_BUNDLE)%>
          <span class="Asterix">*</span>
        </p>
      </td>
      <td width="15" nowrap>&nbsp;</td>
      <td nowrap> 
        <p class="ControlLabel">
          <%=resMgr.getText("LCCreationRuleDetail.ForBankBranch", 
                            TradePortalConstants.TEXT_BUNDLE)%>
          <span class="Asterix">*</span>
        </p>
      </td>
      <td width="15" nowrap>&nbsp;</td>
      <td width="15" nowrap>&nbsp;</td>
      <td nowrap> 
        <p class="ControlLabel">
          <%=resMgr.getText("LCCreationRuleDetail.UpToMaxAmt", 
                            TradePortalConstants.TEXT_BUNDLE)%>
        </p>
      </td>
      <td width="100%">&nbsp;</td>
    </tr>
    <tr> 
      <td>&nbsp;</td>
      <td class=ListText> 
<%
        options = ListBox.createOptionList(templatesDoc, 
                                      "TEMPLATE_OID", "NAME", 
                                      rule.getAttribute("template_oid"),
                                      userSession.getSecretKey());
        if (insertMode) {
          defaultText = resMgr.getText("LCCreationRuleDetail.selectTemplate",
                                       TradePortalConstants.TEXT_BUNDLE);
        } else {
          defaultText = "";
        }
        out.println(InputField.createSelectField("Template", "", 
                              defaultText, options, "ListText", isReadOnly));
%>
      </td>
      <td>&nbsp;</td>
      <td class=ListText> 
<%
        options = ListBox.createOptionList(opBankOrgsDoc, 
                                      "ORGANIZATION_OID", "NAME", 
                                      rule.getAttribute("op_bank_org_oid"),
                                      userSession.getSecretKey());
        if (insertMode) {
          defaultText = resMgr.getText("LCCreationRuleDetail.selectBranch",
                                       TradePortalConstants.TEXT_BUNDLE);
        } else {
          defaultText = "";
        }
        out.println(InputField.createSelectField("BankBranch", "", 
                              defaultText, options, "ListText", isReadOnly));
%>
      </td>
      <td>&nbsp;</td>
      <td nowrap> 
        <p class="ListText">
          <%=StringFunction.xssCharsToHtml(userSession.getBaseCurrencyCode())%>&nbsp;
        </p>
      </td>
      <td class="ListText"> 
<%
  String displayMaximumAmount;

  // Don't format the amount if we are reading data from the doc
  if(doc.getDocumentNode("/In/LCCreationRule") == null)
     displayMaximumAmount = 
       TPCurrencyUtility.getDisplayAmount(rule.getAttribute("maximum_amount"), userSession.getBaseCurrencyCode(), loginLocale);
  else
     displayMaximumAmount = rule.getAttribute("maximum_amount");

%>

        <%=InputField.createTextField("MaxAmount",
                                      displayMaximumAmount,
                                      "19", "19", "ListTextRight", isReadOnly)%>
      </td>
      <td>&nbsp;</td>
    </tr>
  </table>

  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td width="20" nowrap>&nbsp;</td>
      <td nowrap> 
        <p class="ListText">
          <%=resMgr.getText("LCCreationRuleDetail.POsAreGrouped", 
                            TradePortalConstants.TEXT_BUNDLE)%>
        </p>
      </td>
      <td width="100%">&nbsp;</td>
    </tr>
  </table>

  <br>

  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td width="20" nowrap>&nbsp;</td>
      <td nowrap> 
        <p class="ControlLabel">
          <%=resMgr.getText("LCCreationRuleDetail.GroupingCriteria", 
                            TradePortalConstants.TEXT_BUNDLE)%>
        </p>
      </td>
      <td width="100%">&nbsp;</td>
    </tr>
  </table>

  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td width="30" nowrap>&nbsp;</td>
      <td class=ListText nowrap>
		<%=resMgr.getText("LCCreationRuleDetail.IncludePOs", 
                            TradePortalConstants.TEXT_BUNDLE)%>
		<%=InputField.createTextField("MinDays",
                                        rule.getAttribute("min_last_shipment_date"),
                                        "3", "3", "ListText", isReadOnly)%>
		<%=resMgr.getText("LCCreationRuleDetail.DaysLessThan", 
                            TradePortalConstants.TEXT_BUNDLE)%><%=InputField.createTextField("MaxDays",
                                        rule.getAttribute("max_last_shipment_date"),
                                        "3", "3", "ListText", isReadOnly)%>
		<%=resMgr.getText("LCCreationRuleDetail.DaysFrom", 
                            TradePortalConstants.TEXT_BUNDLE)%> 
	</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td height="20">&nbsp;</td>
      <td valign="bottom"> 
        <p class="ListText">
          <%=resMgr.getText("LCCreationRuleDetail.UsingFields", 
                            TradePortalConstants.TEXT_BUNDLE)%>
        </p>
      </td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td height="20">&nbsp;</td>
      <td valign="bottom"> 
        <p class="ListText">
          <%=resMgr.getText("LCCreationRuleDetail.InValue", 
                            TradePortalConstants.TEXT_BUNDLE)%>
        </p>
      </td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td height="20">&nbsp;</td>
      <td valign="bottom"> 
        <p class="AsterixText">
          <%=resMgr.getText("LCCreationRuleDetail.AtLeastOne", 
                            TradePortalConstants.TEXT_BUNDLE)%>
        </p>
      </td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
  </table>


  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td width="40" nowrap>&nbsp;</td>
      <td nowrap>
        <p class="ControlLabel">
          <%=resMgr.getText("LCCreationRuleDetail.PODataItem", 
                            TradePortalConstants.TEXT_BUNDLE)%>
          <span class="Asterix">*</span>
        </p>
      </td>
      <td width="10" nowrap>&nbsp;</td>
      <td nowrap>&nbsp;</td>
      <td width="10" nowrap>&nbsp;</td>
      <td>
        <p class="ControlLabel">
          <%=resMgr.getText("LCCreationRuleDetail.Value", 
                            TradePortalConstants.TEXT_BUNDLE)%>
          <span class="Asterix">*</span>
        </p>
      </td>
      <td width="10" nowrap>&nbsp;</td>
      <td nowrap>&nbsp;</td>
      <td width="100%">&nbsp;</td>
    </tr>
<%
    // There are 6 rows of criteria.  Display each of them (except if we're in
    // readonly mode and neither the data nor value fields have values.
    String dataAttribute  = "";
    String valueAttribute ="";

    for (int x=1; x<=6; x++) {
      dataAttribute = rule.getAttribute("criterion"+x+"_data");
      valueAttribute = rule.getAttribute("criterion"+x+"_value");
      if (isReadOnly && 
         InstrumentServices.isBlank(dataAttribute) && 
         InstrumentServices.isBlank(valueAttribute)) {
         // Do nothing
      } else {
%>
        <tr>
          <td>&nbsp;</td>
          <td class="ListText"> 
<%
            // For the data item dropdowns, we'll add a blank entry.  This allows
            // the user to deselect a value.  We'll use the fieldOptions list
            // and attempt to 'select' one of the entries before building the
            // select html.
            defaultText = " ";
            options = uploadDefn.selectFieldInList(fieldOptions, dataAttribute);
            out.println(InputField.createSelectField("Data"+x, "", 
                        defaultText, options, "ListText", isReadOnly));
%>
          </td>
          <td>&nbsp;</td>
          <td nowrap> 
            <p class="ListText">=
              <%=resMgr.getText("LCCreationRuleDetail.Equal", 
                                TradePortalConstants.TEXT_BUNDLE)%>
            </p>
          </td>
          <td>&nbsp;</td>
          <td class="ListText"> 
            <%-- A width of 40 allows for about 35 characters to be typed --%>
            <%=InputField.createTextArea("Value"+x, valueAttribute,
                                         "40", "2", "ListText", isReadOnly)%>
          </td>
          <td>&nbsp;</td>
<%
          // Display the "and" text for rows 1-5.
          if (x != 6) {
%>
            <td nowrap>
              <p class="ListText">
                <%=resMgr.getText("LCCreationRuleDetail.And", 
                                  TradePortalConstants.TEXT_BUNDLE)%>
              </p>
            </td>
<%
          } // end if x!=6
        } // end if isReadOnly and both data and value are blank
%>
      </tr>
<%
    } // end for
%>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>

  </table>

  <table width=100% border=0 cellspacing=0 cellpadding=0 
         class="BankColor">
    <tr> 
      <td width="20" nowrap>&nbsp;</td>
      <td width="80%" class="BankColor">&nbsp;</td>
      <td colspan=5 class="BankColor" nowrap width="96"> 

<%
        if (isViewInReadOnly) {
           link = "goToTradePortalHome";
        } else {
           link = "goToRefDataHome";
        }
%>
        <jsp:include page="/common/SaveDeleteClose.jsp">
           <jsp:param name="showSaveButton" value="<%=showSave%>" />
           <jsp:param name="showDeleteButton" value="<%=showDelete%>" />
           <jsp:param name="showCloseButton" value="true" />
           <jsp:param name="showHelpButton" value="false" />
           <jsp:param name="cancelAction" value="<%=link %>" />
           <jsp:param name="extraSaveTags" value="" />
        </jsp:include>

      </td>
    </tr>
  </table>
</form>
</body>


<%
  // Finally, reset the cached document to eliminate carryover of 
  // information to the next visit of this page.
  formMgr.storeInDocCache("default.doc", new DocumentHandler());


%>

</html>


