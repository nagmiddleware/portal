<%--
*******************************************************************************
  DirectDebit History List View Footer

  Description: Everything that should be loaded at the end of the page
               should be included here.  This includes non-visible html 
               and javascript that should execute on page load.

*******************************************************************************
--%>
<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>


<%
   gridLayout = dgFac.createGridLayout("ExistingDirectDebitSearchDataGrid");
   String orgId = EncryptDecrypt.encryptAlphaNumericString(selectedOrg);
  // out.println(EncryptDecrypt.decryptAlphaNumericString("AXLajwBwqqBHOUUoetLJZPcW+IDwoszM"));
%>

<script type="text/javascript">

  <%--get grid layout from the data grid factory--%>
  var gridLayout = <%= gridLayout %>;
  <%--set the initial search parms--%>
  var initSearchParms = "active=false&inactive=false&organisation=<%=orgId%>"+"&orgList=<%=orgList.toString()%>";
  console.log ('initParams='+initSearchParms);
  <%--create the grid, attaching it to the div id specified in dataGrid.jsp--%>
  var existingDirectDebitSearchGridId = 
    createDataGrid("existingDirectDebitSearchGridId", "ExistingDirectDebitSearchDataView",
                    gridLayout, initSearchParms);
 
 
  function searchDirectDebitHistory(){
  require(["dojo/dom","dojo/domReady!"],
        function(dom){
    console.log('search invoked');
	var inact = dom.byId("InActive").checked;
	var act = dom.byId("Active").checked;
	if (<%=orgListDropDown%> == true)
		var org = dijit.byId('org').attr('value');
	else
		var org = "<%=orgId%>";
	var instId = dijit.byId('InstrumentId').value;
	var caId = dijit.byId('CreditAcct').value;
	var refId = dijit.byId('Reference').value;
	
	
	var searchParams= "inactive="+inact+"&active="+act+"&organisation="+org+"&orgList=<%=orgList.toString()%>"+
	"&searchType=<%=searchType%>&InstrumentId="+instId+"&CreditAcct="+caId+"&Reference="+refId;
	if ("<%=searchType%>" == "A") {
	searchParams=searchParams+"&fromDate="+encodeURIComponent(dojo.attr(dom.byId('FromDate'), 'value'))+
	"&toDate="+encodeURIComponent(dojo.attr(dom.byId('ToDate'), 'value'));
	console.log('AmountTo='+dijit.byId('AmountTo').value);
	if (dijit.byId('AmountFrom').value.toString() != "NaN")
		searchParams=searchParams+"&fromAmount="+dijit.byId('AmountFrom').value;
	if (dijit.byId('AmountTo').value.toString() != "NaN")
		searchParams=searchParams+"&toAmount="+dijit.byId('AmountTo').value;
	searchParams=searchParams+"&locale=<%=loginLocale%>"
	}
	console.log('searchParams='+searchParams);
	  searchDataGrid("existingDirectDebitSearchGridId", "ExistingDirectDebitSearchDataView",
                         searchParams);
    });
	
  }
  
  function resetDates() {
  dijit.byId('FromDate').reset();
  dijit.byId('ToDate').reset();
  }
  
  function selectDirectDebit(){
	  console.log("*** inside selectDirectDebit() function");
  }
</script>


