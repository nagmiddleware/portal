
<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.ams.tradeportal.busobj.webbean.*,com.ams.tradeportal.common.*,
                 com.ams.tradeportal.html.*,java.util.*,com.amsinc.ecsg.util.*,
		 com.ams.tradeportal.busobj.util.*,com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
		 com.amsinc.ecsg.frame.*" %>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session"></jsp:useBean>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session"></jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
</jsp:useBean>

<%
Debug.debug("***START********************PARTY*SEARCH**************************START***");
/******************************************************************************
 * PartySearch.jsp
 *
 * Filtering:
 * This page originally displays a listview containing all possible parties,
 * but the parties can be filtered using a text box and a link that redisplays
 * the page with the filter text passed as a url parameter.  JavaScript is used
 * to dynamically build that link.
 *
 * Selecting:
 * JavaScript is used to detect which radio button is selected, and the value
 * of that button is passed as a url parameter to page from which the search
 * page was called.
 ******************************************************************************/
 WidgetFactory widgetFactory = new WidgetFactory(resMgr);
String returnAction     = null;
String filterText       = null;
String partyType        = null;
String partyTypeCode    = null;
String onLoad           = "document.FilterForm.filterText.focus();";
String parentOrgID      = userSession.getOwnerOrgOid();
String bogID            = userSession.getBogOid();
String clientBankID     = userSession.getClientBankOid();
String globalID         = userSession.getGlobalOrgOid();
String ownershipLevel   = userSession.getOwnershipLevel();
String unicodeIndicator = null;  // CR-486 05/24/10
boolean userFlag   =false;
String tempOwnerOrgOid = null;
//rbhaduri - 21st Feb 06 - CR 239 - added newLink variable
String userSecurityRights = null;
StringBuffer newLink    = new StringBuffer();
boolean showCreateButton = true;

boolean filterMode = false;

String BankGroupDataView = "";
String initSearchParms="";
DataGridFactory dgFactory = new DataGridFactory(resMgr, formMgr, response);
String gridHtml = "";
String gridLayout = ""; 

Hashtable secureParams = new Hashtable();

DocumentHandler doc;

// Get the document from the cache.  We'll use it to repopulate the screen if the
// user was entering data with errors.
doc = formMgr.getFromDocCache();

Debug.debug("doc from cache is " + doc.toString());

returnAction    = doc.getAttribute("/In/PartySearchInfo/ReturnAction");
filterText      = doc.getAttribute("/In/PartySearchInfo/FilterText");
partyType       = doc.getAttribute("/In/PartySearchInfo/Type");
unicodeIndicator = doc.getAttribute("/In/PartySearchInfo/unicodeIndicator");   // CR-486 05/24/10

if (filterText != null)
{
    filterText = filterText.toUpperCase();
    filterMode = true;
}
else
    filterText = "";

//Resolve the party type code to search for
if (partyType.equals(TradePortalConstants.ADVISING_BANK) ||
    partyType.equals(TradePortalConstants.TRANSFEREE_NEGOT) ||
    partyType.equals(TradePortalConstants.DESIG_BANK) ||
    partyType.equals(TradePortalConstants.COLLECTING_BANK) ||
    partyType.equals(TradePortalConstants.OVERSEAS_BANK) ||
    partyType.equals(TradePortalConstants.OPENING_BANK) ||
    partyType.equals(TradePortalConstants.ASSIGNEE_BANK) ||
    partyType.equals(TradePortalConstants.ADVISE_THROUGH_BANK) ||
    partyType.equals(TradePortalConstants.REIMBURSING_BANK) ||
    partyType.equals(TradePortalConstants.APPLICANT_BANK) ||
    partyType.equals(TradePortalConstants.BENEFICIARY_BANK) ||
	partyType.equals(TradePortalConstants.ORDERING_PARTY_BANK) || //IAZ IR-PMUK012737723 02/03/10 Add   
	partyType.equals(TradePortalConstants.PAYEE_BANK))
{
    partyTypeCode = TradePortalConstants.PARTY_TYPE_BANK;
}
else if (partyType.equals(TradePortalConstants.BENEFICIARY) ||
         partyType.equals(TradePortalConstants.APPLICANT) ||
         partyType.equals(TradePortalConstants.FREIGHT_FORWARD) ||
         partyType.equals(TradePortalConstants.TRANSFEREE) ||
         partyType.equals(TradePortalConstants.ASSIGNEE_01) ||
         partyType.equals(TradePortalConstants.DRAWEE_BUYER) ||
         partyType.equals(TradePortalConstants.DRAWER_SELLER) ||
         partyType.equals(TradePortalConstants.RELEASE_TO_PARTY) ||
         partyType.equals(TradePortalConstants.AGENT) ||
	   partyType.equals(TradePortalConstants.NOTIFY_PARTY) ||
	   partyType.equals(TradePortalConstants.OTHER_CONSIGNEE) ||
	   partyType.equals(TradePortalConstants.CASE_OF_NEED) ||
	   partyType.equals(TradePortalConstants.PAYEE) ||
	   partyType.equals(TradePortalConstants.PAYER) ||
	    partyType.equals(TradePortalConstants.ORDERING_PARTY) || //Vshah CR507
	   partyType.equals(TradePortalConstants.ATP_SELLER) )//rkrishnap CR 375-D ATP-ISS 07/26/2007
{
    partyTypeCode = TradePortalConstants.PARTY_TYPE_CORP;
}
//rkrishnap CR 375-D ATP-ISS 07/26/2007 Begin
else if (partyType.equals(TradePortalConstants.INSURING_PARTY))
{
 partyTypeCode = "'"+TradePortalConstants.PARTY_TYPE_CORP+"','"+TradePortalConstants.PARTY_TYPE_BANK+"'";
}
//rkrishnap CR 375-D ATP-ISS 07/26/2007 End
else
{
    Debug.debug("INVALID PARTY TYPE -> " + partyType);
    partyTypeCode = "";
}



%>

<%-- Body tag included as part of common header --%>
<jsp:include page="/common/Header.jsp">
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeErrorSectionFlag"  value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="additionalOnLoad" value="<%=onLoad%>" />
</jsp:include>

<script LANGUAGE="JavaScript">

  function getCheckedRadioButton()
  {
    <%--
    // Before going to the next step, the user must have selected one of the
    // radio buttons.  If not, give an error.  Since the selection radio button
    // is dynamic, it is possible the field doesn't even exist.  First check
    // for the existence of the field and then check if a selection was made.
    --%>
    numElements = document.ChoosePartyForm.elements.length;
    i = 0;
    foundField = false;

    <%-- First determine if the field even exists. --%>
    for (i=0; i < numElements; i++) {
       if (document.ChoosePartyForm.elements[i].name == "selection") {
          foundField = true;
       }
    }

    if (foundField) {
        var size=document.ChoosePartyForm.selection.length;
        <%--
        //Handle the case where there is only one radio button
        //Handle the case where there are two or more radio buttons
        --%>
        for (i=0; i<size; i++)
        {
            if (document.ChoosePartyForm.selection[i].checked)
            {
                return document.ChoosePartyForm.selection[i].value;
            }
        }
        if (document.ChoosePartyForm.selection.checked)
        {
            return document.ChoosePartyForm.selection.value;
        }

    }
    return 0;
  }

  function confirmSelection()
  {
    if (getCheckedRadioButton()==0)
    {
        alert('<%=resMgr.getText("PartySearch.SelectBankWarning",TradePortalConstants.TEXT_BUNDLE)%>');
        return false;
    }
    return true;
  }

</script>

<%
/*************************************************************
 * FilterForm
 * This form contains the filter text field and submit button.
 *************************************************************/
%>

<div class="pageMainNoSidebar">
      <div class="pageContent">
            <div class="secondaryNav">
                 <jsp:include page="/common/PageHeader.jsp">
                    <jsp:param name="titleKey" value="PartySearch.TabHeading"/>
                    <jsp:param name="helpUrl"  value="customer/party_search.htm"/><!-- .htm help file is missing -->
                  </jsp:include>

                  <div class="subHeaderDivider"></div>
            </div>

<form method="post" name="FilterForm" action="<%=formMgr.getSubmitAction(response)%>">



  <input type=hidden value="" name=buttonName>
  <div class="formContentNoSidebar">
  
  <div class="gridFilter">
  			<table>
			<tr>&nbsp;</tr>
			</table>
  
              <%=widgetFactory.createSearchTextField("filterText", "PartySearch.PartyName", "30")%>
      
      
        <%					
        /********************************
         * START FILTER INPUT FIELD
         ********************************/
        Debug.debug("Filter Field");
        %>
       <%--  <%=resMgr.getText("PartySearch.NameStartsWithExample",TradePortalConstants.TEXT_BUNDLE)%><br>
       
        <%= InputField.createTextField("filterText", filterText, "30", "30", "ListText", false)%> --%>
        
        
        
    
			<div  style="float:right;padding-right:5px;margin-top: -2px;">	
					
				<button data-dojo-type="dijit.form.Button" type="button" class="gridFooterAction">
					Search
		    	<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
				searchParty();return false;
    			</script>
		  		</button>
        <%
        /********************************
         * END FILTER INPUT FIELD
         ********************************/%>
     
            <%
            /********************
            * START FILTER BUTTON
            *********************/
            Debug.debug("Filter Button");
            %>

           
              <%-- <jsp:include page="/common/RolloverButtonSubmit.jsp">
                <jsp:param name="showButton" value="true" />
                <jsp:param name="name" value="FilterButton" />
                <jsp:param name="image" value='common.SearchImg' />
                <jsp:param name="text" value='PartySearch.Filter' />
                <jsp:param name="submitButton" value="<%=TradePortalConstants.BUTTON_FILTER%>" />
              </jsp:include> --%>
           
            <%
            /********************
            * END FILTER BUTTON
            *********************/
            %>
            <%
            /*******************************
            * START CREATE NEW PARTY BUTTON
            ********************************/
            %>

<%-- rbhaduri - 3rd March 06 - CR239 - Add Begin - Add Create New Party button --%>
	    
              <%
		
		//rbhaduri - 31st March 06 - NNUG031554019 - Add Begin - If corporate customer is not alllowed to add bank
		//parties, the create party button should not be visible

		
		if (partyTypeCode.equals(TradePortalConstants.PARTY_TYPE_BANK))
		{
		    CorporateOrganizationWebBean corporateOrg = null;

		    corporateOrg = (CorporateOrganizationWebBean) beanMgr.createBean("com.ams.tradeportal.busobj.webbean.CorporateOrganizationWebBean","CorporateOrganization");
     		    corporateOrg.setAttribute("organization_oid", userSession.getOwnerOrgOid());
     		    corporateOrg.getDataFromAppServer();
		    showCreateButton = corporateOrg.getAttribute("allow_create_bank_parties").equals(TradePortalConstants.INDICATOR_YES);
		
		}

		//rbhaduri - 31st March 06 - NNUG031554019 - Add End

		
		 if (showCreateButton)
		 {
		    // Make sure the user has the correct security right
			userSecurityRights = userSession.getSecurityRights();
            if (SecurityAccess.hasRights(userSecurityRights, SecurityAccess.CREATE_NEW_TRANSACTION_PARTY) 
			     &&(SecurityAccess.hasRights(userSecurityRights, SecurityAccess.MAINTAIN_PARTIES)
			         ||SecurityAccess.hasRights(userSecurityRights, SecurityAccess.VIEW_PARTIES))) 
			{
			   if (!returnAction.equals("selectTransactionParty")) 
			   {
			      // If the user was on a PartyDetailNew page or PartyDetail page,
			      // and clicks on "Select the Designated Bank"
			      // to navigate to this page again, do not display "Create a Party",
			      // i.e. no nested "Create a Party".  That is too much trouble to achieve.
			      showCreateButton = false;
			   }
			}
			else
			{
			   showCreateButton = false;
			}
		}
			if (showCreateButton)
            {
                    newLink.append(formMgr.getLinkAsUrl("selectPartyNew", response));
	       %>
                   <a href="formMgr.getLinkAsUrl("selectPartyNew", response)">
                   <button data-dojo-type="dijit.form.Button" type="button" class="gridFooterAction">
					New
		  		</button>
		  		</a>
		   			<%-- <jsp:include page="/common/RolloverButtonLink.jsp">
            		<jsp:param name="name"  value="CreateNewButton" />
            		<jsp:param name="image" value="common.CreateNewPartyImg" />
            		<jsp:param name="text"  value="RefDataParties.CreateNewParty" />
            		<jsp:param name="link"  value="<%=java.net.URLEncoder.encode(newLink.toString())%>" />
            		<jsp:param name="width" value="imaewidth" />
         	   		</jsp:include>
                --%>
		<%
                  }
                  else //if (showCreateButton)
                  {
                    out.print("&nbsp;");
                  }
               %>
          
<%-- rbhaduri - 3rd March 06 - CR239 - Add End --%>
            <%
            /*******************************
            * END CREATE NEW PARTY BUTTON
            ********************************/
            %>

      </div>   
  <%= formMgr.getFormInstanceAsInputField("PartySearchFilterForm", secureParams) %>
 </div>
</form>

<%
/*********************************************************************
 * ChoosePartyForm
 * This form contains the list view and the choose and cancel buttons.
 *********************************************************************/
%>

<form method="post" name="ChoosePartyForm" action="<%=formMgr.getSubmitAction(response)%>">
  <input type=hidden value="" name=buttonName>
  <%
				gridHtml = dgFactory.createDataGrid("PartySearchNewDataGridId","PartySearchNewDataGrid",null);
				gridLayout = dgFactory.createGridLayout("PartySearchNewDataGrid");
 %>
  
<%
    /***********************************************************************
     * Begin list view for Parties List View Page
     ***********************************************************************/

     //Build the where clause

      //rkrishnap CR 375-D ATP-ISS 07/26/2007 Begin 
      
     //where = new StringBuffer("where party_type_code='").append(partyTypeCode).append("'");
     //where.append("and p_owner_org_oid in (");
     //Requirement not yet freezed.Both Corp and Bank parties are to be displayed for Insuring Party
     //existing where reference had to be put in else condition.May be changed later once requirement is freezed.
	 StringBuffer where=null;
     if(partyType.equals(TradePortalConstants.INSURING_PARTY))
      {
       where=new StringBuffer("where party_type_code in (").append(partyTypeCode).append(")");
       where.append("and p_owner_org_oid in (");
      }
	  else
	 {
      where = new StringBuffer("where party_type_code='").append(partyTypeCode).append("'");
      where.append("and p_owner_org_oid in (");
     }
     //rkrishnap CR 375-D ATP-ISS 07/26/2007 End
     // Also include parties from the user's actual organization if using subsidiary access
     if(userSession.showOrgDataUnderSubAccess())
        where.append(userSession.getSavedUserSession().getOwnerOrgOid()).append(",");

     where.append(parentOrgID).append(",").append(globalID);
     if (ownershipLevel.equals(TradePortalConstants.OWNER_BOG) || ownershipLevel.equals(TradePortalConstants.OWNER_CORPORATE))
     {
         where.append(",").append(clientBankID);
         if (ownershipLevel.equals(TradePortalConstants.OWNER_CORPORATE))
             where.append(",").append(bogID);
     }
     where.append(")");
     if (!filterText.equals(""))
         where.append("and upper(name) like unistr('").append(StringFunction.toUnistr(SQLParamFilter.filter(filterText))).append("%')");
     // CR-486 05/24/10 - Begin
    // Exclude records with unicode if indicator is not 'Y'
     if (TradePortalConstants.INDICATOR_NO.equals(unicodeIndicator)) {
        where.append(" and unicode_indicator = 'N'");
     }
    // CR-486 05/24/10 - End
        
     Debug.debug("*** where-> " + where);
%>

			<%=gridHtml %>	
     <%-- <jsp:include page="/common/TradePortalListView.jsp">
         <jsp:param name="listView" value="PartySearchListView.xml"/>
         <jsp:param name="whereClause2" value='<%=where%>' />
         <jsp:param name="userType" value='<%=userSession.getSecurityType()%>' />
         </jsp:include> --%>
<%
    Debug.debug("Listview complete");
    /***********************************************************************
     * End list view for Parties List View Page
     ***********************************************************************/
%>
 
<%
        String link= formMgr.getLinkAsUrl(returnAction, response) + "&useCache=true";
%>
         
           <%
             /*****************************************
             * END <CHOOSE PARTY> AND <CANCEL> BUTTONS
             *****************************************/
           %>
          
    <%= formMgr.getFormInstanceAsInputField("PartySearchChooseForm", secureParams) %>
    </div>
    <jsp:include page="/common/Footer.jsp">
<jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_NO%>" />
<jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>
<!-- cquinton Portal Refresh - end -->
<script type="text/javascript">
var gridLayout = <%=gridLayout%>;
var initSearchParms="parentOrgID=<%=parentOrgID%>&partyType=<%=partyType%>&partyTypeCode=<%=partyTypeCode%>&userFlag=<%=userFlag%>&tempOwnerOrgOid=<%=tempOwnerOrgOid%>&globalID=<%=globalID%>&ownershipLevel=<%=ownershipLevel%>&clientBankID=<%=clientBankID%>&bogID=<%=bogID%>&unicodeIndicator=<%=unicodeIndicator%>";

console.log("initSearchParms: "+initSearchParms);

var PartySearchNewDataGridId = createDataGrid("PartySearchNewDataGridId","PartySearchNewDataView",gridLayout,initSearchParms);
function searchParty() {
	 	   require(["dojo/dom"],
			      function(dom){
			    	
			    	
			    	var filterText=(dom.byId("filterText").value).toUpperCase();
			    	
			    	var searchParams = initSearchParms+"&filterText="+filterText;
			    	
			        console.log("SearchParmas: " + searchParams);
			        searchDataGrid("PartySearchNewDataGridId", "PartySearchNewDataView",
			        		searchParams);
			      });
	
}
function chooseParty() {
    require(["dojo/dom"],
      function(dom){
    	alert("hhhh");
    	var rowKeys = getSelectedGridRowKeys("PartySearchNewDataGridId");
    	var formName = 'ChoosePartyForm';
        var buttonName = 'Search';
        submitFormWithParms(formName, buttonName, "selection", rowKeys);
        
      });
  }

</script>
  </form>
  
 <%--  <%--cquinton Portal Refresh - start--%>


  </div>
  </div> 
</body>

</html>


