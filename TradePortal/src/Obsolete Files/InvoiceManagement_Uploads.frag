<%--
 *
 *     Copyright  � 2008                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
**********************************************************************************
                        Invoice Management Tab

  Description:
    Contains HTML to create the Invoice Uploads tab for the Invoice Management Home page

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="InvoiceManagement_Uploads.frag" %>
*******************************************************************************
--%>

<%
	String invoiceDefnSql = "select INV_UPLOAD_DEFINITION_OID, ind.NAME as NAME from INVOICE_DEFINITIONS ind order by NAME";
	DocumentHandler invoiceDefinitionList = DatabaseQueryBean
			.getXmlResultSet(invoiceDefnSql.toString(), false);
	String path = corpOrg.getAttribute("restricted_inv_upload_dir");
	path =path.replaceAll("\\\\","\\\\\\\\");
	String newPath = null;
	String clicktag = "return restrictAttach(document.forms[0],'" + path + "');";
	// Nar IR - NEUM032046408  Begin
	// taking sortOrder parameter from request if user is manually pressing link on column header on list view to 
	// sort data in either Ascending  or descending. bydefault file upload list view needed to be in descending order.
	String sortOrder =  request.getParameter("sortOrder");
	if(InstrumentServices.isBlank(sortOrder)){
	     sortOrder = "D";
	 }
    // Nar IR - NEUM032046408  End
%>
<SCRIPT LANGUAGE="JavaScript">

var blink_speed=200;
var i=0;

function restrictAttach(form,path) {

var allowUpload = true; 

var file = form.PaymentDataFilePath.value;
if (!file) {
	<%-- BSL IR NRUL080338698 03/28/2012 - Convert numeric character references to Unicode --%>
	alert('<%=StringFunction.asciiToUnicode(resMgr.getText(
		"InvoiceUploadLogDetail.NoInvoiceFileError",
		TradePortalConstants.TEXT_BUNDLE))%>');
	return false;
}
if (path){
var delimiter = "\\";
	delimiter = (file.lastIndexOf("//") != -1)?"//":"\\";
	delimiter = (file.lastIndexOf("/") != -1)?"/":"\\";
			
if (file.lastIndexOf(delimiter) != -1){
 var slashIndex = file.lastIndexOf(delimiter) ;
 file = file.substring(0,slashIndex);
 newPath = file;

//if path is not same as restricted path the show up the error
if(newPath != path){
	<%-- BSL IR NRUL080338698 03/28/2012 - Convert numeric character references to Unicode --%>
	alert('<%=StringFunction.asciiToUnicode(resMgr.getText(
			"InvoiceMgmtUploads.RestrictFilePathErrorText",
			TradePortalConstants.TEXT_BUNDLE))%>');
return false;
}

}}
   if (allowUpload) {
	   form.action ="<%=response.encodeURL(request.getContextPath()
					+ "/transactions/InvoiceFileUploadServlet.jsp")%>";
	   form.enctype="multipart/form-data";
	   form.encoding="multipart/form-data";
	   document.getElementById('UploadButtonDiv').style.visibility = 'hidden';
	   document.getElementById('UploadButtonDiv').style.display = 'none';	
	   document.getElementById('UploadMessage').style.visibility = 'visible';
			document.getElementById('UploadMessage').style.display = 'block';
	   Blink('UploadMessage');	
	   return true;
   }
   else {
       <%-- BSL IR NRUL080338698 03/28/2012 - Convert numeric character references to Unicode --%>
       alert('<%=StringFunction.asciiToUnicode(resMgr.getText(
					"InvoiceUploadLogDetail.ValidFileExtionsionText",
					TradePortalConstants.TEXT_BUNDLE))%>');
       return false;
   }
}

function Blink(layerName){
		
	if(i%2==0) {
		eval("document.all"+'["'+layerName+'"]'+
				'.style.visibility="visible"');
	} else {
 			eval("document.all"+'["'+layerName+'"]'+
			  '.style.visibility="hidden"');
		}
		 
 		if(i<1) {
			i++;
		} else {
			i--
		}
 		setTimeout("Blink('"+layerName+"')",blink_speed);
	}
	
function confirmDelete()
{
  <%-- BSL IR NRUL080338698 03/28/2012 - Convert numeric character references to Unicode --%>
  var   confirmMessage = "<%=StringFunction.asciiToUnicode(resMgr.getText("CashMgmtFutureValue.PopupMessage", 
                                            TradePortalConstants.TEXT_BUNDLE)) %>";

   if (!confirm(confirmMessage)) 
    {
       formSubmitted = false;
       return false;
    }
  else
  {
     return true;
  }
}

</script>

<table width="100%" border="0" cellspacing="0" cellpadding="0"
	class="BankColor">
	<tr>
		<td width="100%" height="40">&nbsp;</td>
		<td>
		<%
			userDefaultWipView = userSession.getDefaultWipView();
			selectedWorkflow = request.getParameter("workflow");

			if (selectedWorkflow == null) {
				selectedWorkflow = (String) session.getAttribute("workflow");

				if (selectedWorkflow == null) {
					if (userDefaultWipView
							.equals(TradePortalConstants.WIP_VIEW_MY_ORG)) {
						selectedWorkflow = userOrgOid;
					} else if (userDefaultWipView
							.equals(TradePortalConstants.WIP_VIEW_MY_ORG_CHILDREN)) {
						selectedWorkflow = ALL_WORK;
					} else {
						selectedWorkflow = MY_WORK;
					}
					selectedWorkflow = EncryptDecrypt
							.encryptStringUsingTripleDes(selectedWorkflow, userSession.getSecretKey());
				}
			}

			session.setAttribute("workflow", selectedWorkflow);

			selectedWorkflow = EncryptDecrypt
					.decryptStringUsingTripleDes(selectedWorkflow, userSession.getSecretKey());
			//dynamicWhereClause.append("0=0");
			if (selectedWorkflow.equals(MY_WORK)) {
				dynamicWhereClause.append("and INVOICE_FILE_UPLOADS.a_user_oid = "); 				
				dynamicWhereClause.append(userOid);
			} else if (selectedWorkflow.equals(ALL_WORK)) {
				dynamicWhereClause.append("and INVOICE_FILE_UPLOADS.a_owner_org_oid in (");
				dynamicWhereClause.append(" select organization_oid");
				dynamicWhereClause.append(" from corporate_org");
				dynamicWhereClause.append(" where activation_status = '");
				dynamicWhereClause.append(TradePortalConstants.ACTIVE);
				dynamicWhereClause.append("' start with organization_oid = ");
				dynamicWhereClause.append(userOrgOid);
				dynamicWhereClause
						.append(" connect by prior organization_oid = p_parent_corp_org_oid)");
			} else {
				dynamicWhereClause.append("and INVOICE_FILE_UPLOADS.a_owner_org_oid  = ");
				dynamicWhereClause.append(selectedWorkflow);
			}
			//
			newLink.append(formMgr.getLinkAsUrl("goToInvoiceManagement",
					response));
			newLink.append("&currentTab=");
			newLink.append(TradePortalConstants.INVOICE_UPLOADS_TAB);
			newLink.append("&workflow=");
			newLink.append(EncryptDecrypt
					.encryptStringUsingTripleDes(selectedWorkflow, userSession.getSecretKey());
		%> <jsp:include page="/common/RolloverButtonLink.jsp">
			<jsp:param name="name" value="RefreshThisPageButton" />
			<jsp:param name="image" value="common.RefreshThisPageImg" />
			<jsp:param name="text" value="common.RefreshThisPageText" />
			<jsp:param name="link"
				value="<%=java.net.URLEncoder.encode(newLink.toString())%>" />
			<jsp:param name="width" value="140" />
		</jsp:include></td>
		<td width="20" nowrap>&nbsp;</td>
	</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0"
	class="ColorGrey">
	<tr>
		<td width="5" nowrap>&nbsp;</td>
		<td nowrap align="left" valign="middle" nowrap>
		<p class="ListText"><%=resMgr.getText("InvoiceFileUpload.InstructionsText",
					TradePortalConstants.TEXT_BUNDLE)%>&nbsp;&nbsp;</p>
		</td>
	</tr>
</table>

<div id="UploadButtonDiv">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>

	<tr>
		<td width="15">&nbsp;</td>
		<td nowrap align="left" valign="bottom">
		<p class="ControlLabel"><%=resMgr.getText(
					"LocateUploadInvoiceFile.InvoiceDataFileLocation",
					TradePortalConstants.TEXT_BUNDLE)%>
		<span class="Asterix">*</span>
		</p>
		</td>
		<td  nowrap align="left" valign="bottom">
		 <p class="ControlLabel"><%=resMgr.getText(
					"LocateUploadInvoiceFile.InvoiceFileDefinition",
					TradePortalConstants.TEXT_BUNDLE)%>
		 <span class="Asterix">*</span>
	      </p>
		</td>
	</tr>

	<tr>
		<td width="15">&nbsp;</td>
		<td align="left" valign="bottom">
		<p class="ListText"><%=resMgr.getText("LocateUploadInvoiceFile.BrowseLocation",
					TradePortalConstants.TEXT_BUNDLE)%></p>
		</td>
		<td  nowrap align="left" valign="bottom">
		<p class="ListText"><%=resMgr.getText("LocateUploadInvoiceFile.SelectFileDef",
					TradePortalConstants.TEXT_BUNDLE)%></p>
		</td>
	</tr>

	<tr>
		<td width="15">&nbsp;</td>
		<td align="left" valign="bottom"><%=InputField.createFileField("PaymentDataFilePath", "50",
					"ListText", false)%></td>
		<td align="left" valign="bottom">
		<%
		   //ShilpaR IR MAUM021040190 start
		   // This is the query used for populating the Invoice Upload Definition dropdown list;
		   // it retrieves all Invoice Upload Definitions that belong to the corporate customer 
		   // user's org that are used for uploading 
		   StringBuffer sqlQuery1 = new StringBuffer();
		   sqlQuery1.append("select INV_UPLOAD_DEFINITION_OID, NAME");
		   sqlQuery1.append(" from invoice_definitions"); 
		   sqlQuery1.append(" where a_owner_org_oid in (");
		   sqlQuery1.append(userSession.getOwnerOrgOid());

		   // Also include invoice definitions from the user's actual organization if using subsidiary access
		   if(userSession.showOrgDataUnderSubAccess())
		    {
		      sqlQuery1.append(",").append(userSession.getSavedUserSession().getOwnerOrgOid());
		    }

		   sqlQuery1.append(") order by ");
		   sqlQuery1.append(resMgr.localizeOrderBy("name"));

		   DocumentHandler invUploadDefinitionDoc  = DatabaseQueryBean.getXmlResultSet(sqlQuery1.toString(), false);

		   if(invUploadDefinitionDoc == null)
			   invUploadDefinitionDoc = new DocumentHandler();
		   String invDefinitionOid = invUploadDefinitionDoc.getAttribute("/ResultSetRecord(0)/INV_UPLOAD_DEFINITION_OID");
		   StringBuffer invDefOptions = new StringBuffer();
		   invDefOptions.append(Dropdown.createSortedOptions(invUploadDefinitionDoc, "INV_UPLOAD_DEFINITION_OID", 
                                                              "NAME", invDefinitionOid, userSession.getSecretKey(), resMgr.getResourceLocale()));
           out.print(InputField.createSelectField("invoiceDefinitionName", "", 
                                                  "", 
                                                  invDefOptions.toString(), "ListText", false));
           
           
           /*String currMsgCat0 = "";
			String pagOptions1 = ListBox.createOptionList(
					invoiceDefinitionList, "INV_UPLOAD_DEFINITION_OID", "NAME",
					currMsgCat0, false);
			out.println(InputField.createSelectField("invoiceDefinitionName",
					"", "", pagOptions1, "ListText", false, ""));*/
			//ShilpaR IR MAUM021040190 end
           
		%>
		</td>
		<td width="30%" align="left" valign="bottom">
		<%if(hasUploadInvoicesRights){  // Narayan IR-NNUM030242625 Rel 8.0 03/22/2012
		%>
		<jsp:include
			page="/common/RolloverButtonSubmit.jsp">
			<jsp:param name="showButton" value="true" />
			<jsp:param name="name"
				value="<%=TradePortalConstants.BUTTON_UPLOAD_FILE%>" />
			<jsp:param name="image" value='common.UploadFileImg' />
			<jsp:param name="text" value='common.UploadFileText' />
			<jsp:param name="width" value="96" />
			<jsp:param name="submitButton"
				value="<%=TradePortalConstants.BUTTON_UPLOAD_FILE%>" />
			<jsp:param name="imageName" value="UploadFileButton" />
			  <jsp:param name="extraTags"  value='<%=clicktag%>' />
		</jsp:include></td>
		<%}%>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
</table>
</div>

<div id="UploadMessage" style="display: none;">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>

		<p class="ControlLabel"><font color="blue"> <%=resMgr.getText("PaymentUploadLogDetail.Uploading",
					TradePortalConstants.TEXT_BUNDLE)%>..... </font></p>
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
</table>
</div>
<table width="100%" border="0" cellspacing="0" cellpadding="0"
	class="BankColor">
	<tr>
		<td width="15" nowrap>&nbsp;</td>
		<td nowrap align="left" valign="middle" class="ListHeaderTextWhite">
		<%=resMgr.getText("PaymentFileUpload.Show",
					TradePortalConstants.TEXT_BUNDLE)%></td>
		<td align="left" valign="middle" class="ListText">
		<%
			StringBuffer prependText = new StringBuffer();
			prependText.append(resMgr.getText("PaymentFileUpload.WorkFor",
					TradePortalConstants.TEXT_BUNDLE));
			prependText.append(" ");

			// If the user has rights to view child organization work, retrieve the 
			// list of dropdown options from the query listview results doc (containing
			// an option for each child org) otherwise, simply use the user's org 
			// option to the dropdown list (in addition to the default 'My Work' option).

			 if (SecurityAccess.hasRights(userSecurityRights, 
			                            SecurityAccess.VIEW_CHILD_ORG_WORK)){
				dropdownOptions.append(Dropdown.getUnsortedOptions(
						hierarchyDoc, "ORGANIZATION_OID", "NAME",
						prependText.toString(), selectedWorkflow, userSession.getSecretKey()));

				// Only display the 'All Work' option if the user's org has child organizations
				if (totalOrganizations > 1) {
					dropdownOptions.append("<option value=\"");
					dropdownOptions.append(EncryptDecrypt
							.encryptStringUsingTripleDes(ALL_WORK, userSession.getSecretKey());
					dropdownOptions.append("\"");

					if (selectedWorkflow.equals(ALL_WORK)) {
						dropdownOptions.append(" selected");
					}

					dropdownOptions.append(">");
					dropdownOptions.append(ALL_WORK);
					dropdownOptions.append("</option>");
				}
			} else {
				dropdownOptions.append("<option value=\"");
				dropdownOptions.append(EncryptDecrypt
						.encryptStringUsingTripleDes(userOrgOid, userSession.getSecretKey());
				dropdownOptions.append("\"");

				if (selectedWorkflow.equals(userOrgOid)) {
					dropdownOptions.append(" selected");
				}

				dropdownOptions.append(">");
				dropdownOptions.append(prependText.toString());
				dropdownOptions.append(userSession.getOrganizationName());
				dropdownOptions.append("</option>");
			}

			extraTags.append("onchange=\"location='");
			extraTags.append(formMgr.getLinkAsUrl("goToInvoiceManagement",
					response));
			extraTags.append("&amp;currentTab=");
			extraTags.append(TradePortalConstants.INVOICE_UPLOADS_TAB);
			extraTags
					.append("&amp;workflow='+this.options[this.selectedIndex].value\"");

			String defaultValue = "";
			String defaultText = "";

			if (!userSession.hasSavedUserSession()) {
				defaultValue = EncryptDecrypt
				.encryptStringUsingTripleDes(MY_WORK, userSession.getSecretKey());
				defaultText = MY_WORK;
			}

			out.print(InputField.createSelectField("Workflow", defaultValue,
					defaultText, dropdownOptions.toString(), "ListText", false,
					extraTags.toString()));
		%>
		</td>
		<td width="100%" height="40">&nbsp;</td>
	</tr>
</table>

<jsp:include page="/common/TradePortalListView.jsp">
	<jsp:param name="listView"
		value="InvoiceMgmtFileUploadListView.xml" />
	<jsp:param name="whereClause2"
		value='<%= dynamicWhereClause.toString() %>' />		
	<jsp:param name="userType" value='<%= userSecurityType %>' />
	<%-- NarIR - NEUM032046408  Begin passing sortOrder to default as desending order and also redisplayList as it is required else sort order won't work--%>
	<jsp:param name="sortOrder" value='<%= sortOrder %>' />
	<jsp:param name="redisplayList" value="N" />
	<%-- Nar IR - NEUM032046408  End --%>
	<jsp:param name="userTimezone" value='<%=userSession.getTimeZone()%>' />
	<jsp:param name="maxScrollHeight" value="316" />
</jsp:include>

<%--  The table below displays the Delete buttons --%>
<table width="100%" border="0" cellspacing="0" cellpadding="0"
	class="BankColor">
	<tr>
		<td width="20" nowrap>&nbsp;</td>
		<td width="37" nowrap align="left" valign="top"><img
			src="/portal/images/UpIndicatorArrow.gif" width="37" height="27">
		</td>
		<td width="5" nowrap><img src="/portal/images/Blank_4x4.gif"
			width="4" height="4"></td>
		<td>
		<table width="100%" border="0" cellspacing="0" cellpadding="0"
			class="BankColor">
			<tr>
				<td><img src="/portal/images/Blank_4x4.gif" width="4"
					height="4"></td>
			</tr>
		</table>
		<table>
			<tr>
				<td width="15" nowrap>&nbsp;</td>
				<td width="264" valign=bottom>
				<%
				if (hasFailedInvoiceRemoveRights) {   // Narayan IR-NNUM030242625 Rel 8.0 03/22/2012
				%> <jsp:include page="/common/RolloverButtonSubmit.jsp">
					<jsp:param name="name" value="Delete" />
					<jsp:param name="image" value='InvoiceMgmtUploads.RemoveFailedItemsImg' />
					<jsp:param name="text" value='InvoiceMgmtUploads.RemoveFailedItemsText' />
					<jsp:param name="extraTags" value="return confirmDelete()" />
					<jsp:param name="width" value="152" />
					<jsp:param name="submitButton"
						value="<%=TradePortalConstants.BUTTON_DELETE_INVOICE_UPLOAD%>" />
				</jsp:include> <%
 	} else {
 		out.print("&nbsp;");
 	}
 %>
				</td>

			</tr>
		</table>
		</td>
		<td width="100%">&nbsp;</td>
	</tr>
</table>
</td>
</tr>
</table>


