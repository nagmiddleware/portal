package com.ams.tradeportal.servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.ams.tradeportal.busobj.webbean.SessionWebBean;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.frame.NavigationManager;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.EncryptDecrypt;
import com.amsinc.ecsg.util.StringFunction;

public class CorpCustAddressServlet extends HttpServlet {

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException{
		doTheWork(request, response);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException{
		doTheWork(request, response);
	}

	/**
	 * This method implements the standard servlet API for GET and POST
	 * requests. It handles uploading of files to the Trade Portal.
	 *
	 * @param javax
	 *            .servlet.http.HttpServletRequest request - the Http servlet
	 *            request object
	 * @param javax
	 *            .servlet.http.HttpServletResponse response - the Http servlet
	 *            response object
	 * @exception javax.servlet.ServletException
	 * @exception java.io.IOException
	 * @return void
	 * @throws AmsException
	 */
	private int rowsaffected=0;
	private String rowKey="";
	private String corp_org_oid="";
	private String AddressLine1="";
	private String AddressLine2="";
	private String City="";
	
	private String Country="";
	private String PostalCode="";
	private String SeqNo="";
	private String Name="";
	
	private String operation="";
	

	public void doTheWork(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException{
    	//T36000005819 W Zhu 9/24/12 Do not allow direct access to the servlet.
    	if (NavigationManager.bookmarkedAccessDenied(request, response, this)) {
    		return;
    	}

        HttpSession session = request.getSession(false); //don't create a new one, return null if none
	// W Zhu 9/11/2012 Rel 8.1 T36000004579 add key parameter.
        javax.crypto.SecretKey secretKey = null;
        if (session!=null) {
       	   SessionWebBean userSession = (SessionWebBean)session.getAttribute("userSession");
      	   secretKey = userSession.getSecretKey();
        }

		 rowKey=EncryptDecrypt.decryptStringUsingTripleDes(request.getParameter("rowKey"), secretKey);
		 //Rel 9.3 XSS CID-10032
		
		 corp_org_oid=request.getParameter("corp_org_oid");
		 AddressLine1=request.getParameter("AddressLine1");
		 AddressLine2=request.getParameter("AddressLine2");
		 City=request.getParameter("City");
		
		 Country=request.getParameter("Country");
		 PostalCode=request.getParameter("PostalCode");
		 SeqNo=request.getParameter("SeqNo");
		 Name=request.getParameter("Name");
		 
		 operation=request.getParameter("operation");

		if(operation!=null && !operation.equals("") && operation.equals("I")){
			addAddress("ECSG");
			response.getWriter().println();
		}
		if(operation!=null && !operation.equals("") && operation.equals("U")){
			updateAddress();
			response.getWriter().println();
		}
		if(operation!=null && !operation.equals("") && operation.equals("D")){
			response.getWriter().println(deleteAddress(corp_org_oid, rowKey));
		}
		else{
			response.getWriter().println(StringFunction.xssCharsToHtml(rowKey));
		}

	}

	public void addAddress(String instancename){
		List seqNo=new ArrayList();

		long nextValue=getSequenceNextval(instancename);
		if(nextValue!=1){
			
			String sqlcheckSeqNo="select ADDRESS_SEQ_NUM from address where P_CORP_ORG_OID= ?";
			
			DocumentHandler seqnohandler = null;
			try {
				seqnohandler = DatabaseQueryBean.getXmlResultSet(sqlcheckSeqNo, false, new Object[]{corp_org_oid});
				List<String> seqNos = seqnohandler.getAttributes("/ResultSetRecord");
			} catch (AmsException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}			
			
			


		}
	}
	public void updateAddress(){
		StringBuffer sqlupdateStatement=new StringBuffer("update address set ");
		if(Name!=null && !Name.equals("")){
			sqlupdateStatement.append("ADDRESS_LINE_1='"+AddressLine1);
			sqlupdateStatement.append("',");
		}
		if(AddressLine1!=null && !AddressLine1.equals("")){
			sqlupdateStatement.append(",");
			sqlupdateStatement.append("ADDRESS_LINE_1='"+AddressLine1+"'");

		}
		if(City!=null && !City.equals("")){
			sqlupdateStatement.append(",");
			sqlupdateStatement.append("ADDRESS_LINE_1='"+AddressLine1+"'");

		}
		if(Country!=null && !Country.equals("")){
			sqlupdateStatement.append(",");
			sqlupdateStatement.append("ADDRESS_LINE_1='"+AddressLine1+"'");

		}
		if(SeqNo!=null && !SeqNo.equals("")){
			sqlupdateStatement.append(",");
			sqlupdateStatement.append("ADDRESS_LINE_1='"+AddressLine1+"'");

		}
		if(AddressLine2!=null && !AddressLine2.equals("")){
			sqlupdateStatement.append(",");
			sqlupdateStatement.append("ADDRESS_LINE_1='"+AddressLine1+"'");

		}
		if(PostalCode!=null && !PostalCode.equals("")){
			sqlupdateStatement.append(",");
			sqlupdateStatement.append("ADDRESS_LINE_1='"+AddressLine1+"'");

		}
		if(corp_org_oid!=null && !corp_org_oid.equals("")){

			sqlupdateStatement.append("where P_CORP_ORG_OID='"+corp_org_oid+"'");

		}
		if(rowKey!=null && !rowKey.equals("")){
			sqlupdateStatement.append("and");
			sqlupdateStatement.append("address_oid='"+rowKey+"'");

		}


	}
	public int deleteAddress(String corp_org_oid,String rowKey){
		//jgadela R91 IR T36000026319 - SQL INJECTION FIX
		String sqlStatement="delete from address where P_CORP_ORG_OID = ? and address_oid= ?";
		try {
			//jgadela R91 IR T36000026319 - SQL INJECTION FIX
			rowsaffected=DatabaseQueryBean.executeUpdate(sqlStatement, false, corp_org_oid, rowKey);
		} catch (AmsException e) {
			// TODO: handle exception
		}
		catch (SQLException e) {
			// TODO: handle exception
		}

		return rowsaffected;
	}

	public long getSequenceNextval(String instancename){
		
		long currentObjectIDCount=0;
		long tempIncrementID=0;

		String sqlsqlStatement="SELECT SEQUENCE_VALUE,SEQUENCE_INCREMENT FROM SEQUENCE WHERE SEQUENCE_NAME = ?";
		
		try {
			DocumentHandler dhandler=DatabaseQueryBean.getXmlResultSet(sqlsqlStatement, false, new Object[]{instancename});
			currentObjectIDCount = dhandler.getAttributeLong("/ResultSetRecord/SEQUENCE_VALUE");//results.getLong ("SEQUENCE_INCREMENT"); = dhandler.getAttributeLong("/ResultSetRecord/SEQUENCE_VALUE");//results.getLong ("SEQUENCE_VALUE");
			tempIncrementID = dhandler.getAttributeLong("/ResultSetRecord/SEQUENCE_INCREMENT");//results.getLong ("SEQUENCE_INCREMENT");
		} catch (AmsException e) {
			// TODO: handle exception
		}

		return currentObjectIDCount+tempIncrementID+1;

	}

}



