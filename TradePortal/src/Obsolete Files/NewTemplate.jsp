<%--
*******************************************************************************
                                New Template

  Description: Allows the user to initiate a new template.  The template can
  be derived from an existing instrument (not specified on this page), an
  existing template (not specified on this page), or from a blank instrument
  (user specifies the instrument type).

  Clicking the Next Step button calls the TemplateStep1 JSP that determines
  what the next page (and parameters to the page) is.

*******************************************************************************
--%>

<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.html.*" %>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager"
   scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager"
   scope="session">
</jsp:useBean>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager"
   scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean"
   scope="session">
</jsp:useBean>

<%-- ************** Data retrieval/page setup begins here ****************  --%>

<%
Debug.debug("***START***************NEW TEMPLATE**********************START***");

  HttpSession theSession = request.getSession(false);

  String options;
  String defaultText;
  String loginLocale     = userSession.getUserLocale();
  String loginRights     = userSession.getSecurityRights();
  String name            = "";
  String instrType       = "";
  String copyType        = "";
  String isExpress       = "";
  String ownerLevel      = userSession.getOwnershipLevel();

  boolean createNewTemplate = false;
  boolean getDataFromDoc = false;
  boolean showExpress    = true;
  //IAZ CR-586 08/16/10 Begin
  String isFixed       = "";  	
  boolean showFixed    = SecurityAccess.hasRights(userSession.getSecurityRights(),
                                          SecurityAccess.CREATE_FIXED_PAYMENT_TEMPLATE);
  //IAZ CR-586 08/16/10 End  

  DocumentHandler doc   = formMgr.getFromDocCache();

  // This is needed so that the DocCleanUp page will know where to go when 
  // the user presses 'Cancel' during the create template process
  String startPage          = request.getParameter("startPage");

  if(startPage != null)
    {
       startPage =  EncryptDecrypt.decryptStringUsingTripleDes(startPage, userSession.getSecretKey());
       theSession.setAttribute("startPage", startPage);
    }

  // If the mode in the doc is create_new_template (or null) we assume we are
  // creating a new template.  Set a flag indicating this.
  String mode = doc.getAttribute("/In/mode");
  if (mode == null || mode.equals(TradePortalConstants.NEW_TEMPLATE)) {
     createNewTemplate = true;
  }

  // If the mode from the doc is create_new_template, we'll assume the data in the
  // doc is ours and use it to redisplay the page (either with errors or as the
  // result of pressing prev step.
  if (mode != null && mode.equals(TradePortalConstants.NEW_TEMPLATE)) {
     getDataFromDoc = true;
  }

  // Corporate customers cannot create express templates
  // However, admin users using the customer access feature to act on behalf of a corporate 
  // customer are able to create express templates
  if ( userSession.getOwnershipLevel().equals(TradePortalConstants.OWNER_CORPORATE) )
   {
     if(!userSession.hasSavedUserSession() ||
        !userSession.getSavedUserSessionSecurityType().equals(TradePortalConstants.ADMIN) )
     {
       showExpress = false;
     }
   }

  Debug.debug ("getDataFromDoc -> " + getDataFromDoc);


  if (getDataFromDoc)
  {
    Debug.debug ("doc exists.  Getting data from doc. ->\n" + doc.toString());
    name        = doc.getAttribute("/In/name");
    instrType   = doc.getAttribute("/In/instrumentType");
    copyType    = doc.getAttribute("/In/copyType");
    isExpress   = doc.getAttribute("/In/isExpress");
    if (isExpress == null) isExpress = "";
    //IAZ CR-586 08/16/10 Begin
    isFixed   	= doc.getAttribute("/In/isFixed");    
    if (isFixed == null) isFixed = "";
    //IAZ CR-586 08/16/10 End
  }

  if (InstrumentServices.isBlank(copyType)) {
    copyType = TradePortalConstants.FROM_BLANK;
  }

  //SECURE PARAMETERS
  Hashtable secParms = new Hashtable();
  secParms.put("userOid", userSession.getUserOid());
  secParms.put("securityRights", loginRights);
  secParms.put("clientBankOid", userSession.getClientBankOid());
  secParms.put("ownerOrg", userSession.getOwnerOrgOid());
  secParms.put("mode", TradePortalConstants.NEW_TEMPLATE);
  secParms.put("ownerLevel", ownerLevel);

  theSession.setAttribute("inNewTransactionArea", TradePortalConstants.INDICATOR_YES);

%>

<%-- ********************* HTML for page begins here *********************  --%>

<%
  // onLoad is set to default the cursor to the Instrument Type field.
  String onLoad = "";
  if (copyType.equals(TradePortalConstants.FROM_BLANK)) {
     onLoad = "document.NewTemplate1.name.focus();";
  }
%>

<%-- Body tag included as part of common header --%>
<jsp:include page="/common/Header.jsp">
   <jsp:param name="includeNavigationBarFlag"
              value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeErrorSectionFlag"
              value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="additionalOnLoad" value="<%=onLoad%>" />
</jsp:include>


<form name="NewTemplate1" method="POST" action="<%=formMgr.getSubmitAction(response)%>">

  <input type="hidden" name="NewSearch" value="Y">
  <table width="100%" border="0" cellspacing="0" cellpadding="0" height="34">
    <tr>
      <td width="20" class="BankColor" nowrap>&nbsp;</td>
      <td class="BankColor" width="220" nowrap height="34">
        <p class="ControlLabelWhite">
          <%=resMgr.getText("NewInstTemplate1.HeadingStep1",
                            TradePortalConstants.TEXT_BUNDLE)%>
        </p>
      </td>
      <td width="100%" class="BankColor" height="34">&nbsp; </td>
      <td class=BankColor align=right valign=middle width=15 height=34 nowrap>
	    <%= OnlineHelp.createContextSensitiveLink("customer/template_step1.htm",resMgr, userSession) %>
      </td>
      <td width="2" class="BankColor" nowrap>&nbsp;</td>
    </tr>
  </table>

  <br>

  <table width="100%" border="0" cellspacing="0" cellpadding="0" height="12">
    <tr align="left" valign="middle">
      <td width="25" nowrap>&nbsp;</td>
      <td nowrap>
        <p class="ControlLabel">
          <%=resMgr.getText("NewInstTemplate1.EnterNamePrompt",
                            TradePortalConstants.TEXT_BUNDLE)%>
        </p>
      </td>
      <td width="100%">&nbsp;</td>
    </tr>
  </table>

  <table width="100%" border="0" cellspacing="2" cellpadding="2">
    <tr align="left" valign="middle">
      <td>&nbsp;</td>
      <td width="40" nowrap>&nbsp; </td>
      <td colspan="2" nowrap>
        <span class="ControlLabel">
          <%=resMgr.getText("NewInstTemplate1.TemplateName", 
                            TradePortalConstants.TEXT_BUNDLE)%>
          <span class="AsterixText">*</span>
        </span>
        <br>
          <input type="text" name="name" size="25" maxlength="25" class="ListText" 
                 value="<%= name %>">
      </td>
      <td width="140" nowrap>&nbsp; </td>
      <%
      //VS CR-586 08/16/10 Begin
      DocumentHandler queryDoc = null; //this doc will be use to get data from a query
                                   
      String clientBankOid = userSession.getClientBankOid();
      ClientBankWebBean clientBank = (
      		ClientBankWebBean) beanMgr.createBean("com.ams.tradeportal.busobj.webbean.ClientBankWebBean","ClientBank");
      clientBank.getById(clientBankOid);      

      if(TradePortalConstants.INDICATOR_YES.equals(clientBank.getAttribute("template_groups_ind"))){
      %>
        <td colspan="2" nowrap>
        <span class="ControlLabel">
          <%=resMgr.getText("NewInstTemplate1.TemplateGroup", 
                            TradePortalConstants.TEXT_BUNDLE)%>
        </span>
        <br>       
         <%
         
        // Retrieve the data used to populate some of the dropdowns.
        // This data will be used in the default users dropdown
        StringBuffer sqlSet = new StringBuffer();
        QueryListView queryListView = null;

        try {
            queryListView = (QueryListView) EJBObjectFactory.createClientEJB(
				formMgr.getServerLocation(), "QueryListView");

            // Get a list of the users for the client bank
            sqlSet.append("select PAYMENT_TEMPLATE_GROUP_OID, NAME");
            sqlSet.append(" from PAYMENT_TEMPLATE_GROUP");
            sqlSet.append(" where P_OWNER_ORG_OID in (");
            sqlSet.append(userSession.getUserOid()).append(",").append(userSession.getOwnerOrgOid()).append(")");
            Debug.debug(sqlSet.toString());
            
            queryListView.setSQL(sqlSet.toString());
            queryListView.getRecords();
            queryDoc = queryListView.getXmlResultSet();
            Debug.debug("queryDoc --> " + queryDoc.toString());

        } catch (Exception e) {
            e.printStackTrace();
        } 
        finally {
          try {
                if (queryListView != null) {
                    queryListView.remove();
                }
              } 
              catch (Exception e) {
                   System.out.println("error removing querylistview in NewTemplate.jsp");
              }
        }  // try/catch/finally block
    
    
        /********************************
         * START Payment Template Group TYPE DROPDOWN
         ********************************/
         Debug.debug("Building Payment Template Group field");
         String selectedGroup = doc.getAttribute("/In/payment_templ_grp_oid");
         if (InstrumentServices.isBlank(selectedGroup)) 
         	selectedGroup = resMgr.getText("NewInstTemplate1.selectPayTempGrp", TradePortalConstants.TEXT_BUNDLE);
         	
         String groupTmplateOptions = Dropdown.createSortedOptions(queryDoc,"PAYMENT_TEMPLATE_GROUP_OID", "NAME", 
         										selectedGroup, 
         										resMgr.getResourceLocale());
         Debug.debug(groupTmplateOptions);
         out.print(InputField.createSelectField("PaymentTemplGrp", "", 
         								resMgr.getText("NewInstTemplate1.selectPayTempGrp", TradePortalConstants.TEXT_BUNDLE), 
         								groupTmplateOptions, "ListText", false));
            /********************************
             * END Payment Template Group TYPE DROPDOWN
             ********************************/
             
            %>         
      </td>
      <%}
      //VS CR-586 08/16/10 End
      %>
      <td width=100%>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
  </table>

  <table width="100%" border="0" cellspacing="0" cellpadding="0" height="12">
    <tr align="left" valign="middle">
      <td width="25" nowrap>&nbsp;</td>
      <td nowrap>
        <p class="ControlLabel">
          <%=resMgr.getText("NewInstTemplate1.Intro",
                            TradePortalConstants.TEXT_BUNDLE)%>
        </p>
      </td>
      <td width="100%">&nbsp;</td>
    </tr>
  </table>

  <table width="100%" border="0" cellspacing="2" cellpadding="2">

<%
    if (!userSession.getSecurityType().equals(TradePortalConstants.ADMIN))
    {
%>
    <tr align="left" valign="middle">
      <td width="40" nowrap>&nbsp;</td>
      <td width="5" nowrap>
        <%=InputField.createRadioButtonField("CopyType",
                       TradePortalConstants.FROM_INSTR, "",
                       copyType.equals(TradePortalConstants.FROM_INSTR),
                       "ListText", "", false)%>
      </td>
      <td width="6" nowrap>&nbsp;</td>
      <td width=550 nowrap>
        <p class="ListText">
          <%=resMgr.getText("NewInstTemplate1.CopyFromInstrument",
                            TradePortalConstants.TEXT_BUNDLE)%>
        </p>
      </td>
      <td width=100%>&nbsp;</td>
    </tr>
<%
    }
%>
    <tr align="left" valign="middle">
      <td width="40" nowrap>&nbsp;</td>
      <td width="5" nowrap>
        <%=InputField.createRadioButtonField("CopyType",
                       TradePortalConstants.FROM_TEMPL, "",
                       copyType.equals(TradePortalConstants.FROM_TEMPL),
                       "ListText", "", false)%>
      </td>
      <td width="6" nowrap>&nbsp;</td>
      <td width=550 nowrap>
        <p class="ListText">
          <%=resMgr.getText("NewInstTemplate1.CopyFromTemplate",
                            TradePortalConstants.TEXT_BUNDLE)%>
        </p>
      </td>
      <td width=100%>&nbsp;</td>
    </tr>
    <tr align="left" valign="middle">
      <td>&nbsp;</td>
      <td valign="bottom">
        <%=InputField.createRadioButtonField("CopyType",
                       TradePortalConstants.FROM_BLANK, "",
                       copyType.equals(TradePortalConstants.FROM_BLANK),
                       "ListText", "", false)%>
      </td>
      <td>&nbsp;</td>
      <td nowrap class="ListText">
        <p class="ListText">
          <%=resMgr.getText("NewInstTemplate1.UseBlankForm",
                            TradePortalConstants.TEXT_BUNDLE)%>
          &nbsp;
<%
	  //Get the Organization Oid from the userSession WebBean first
	  Long organization_oid = new Long( userSession.getOwnerOrgOid() );

	  //Get the instrument type
	  Vector instrumentTypes = Dropdown.getEditableInstrumentTypes(formMgr,
		                                       organization_oid.longValue(),
		                                       loginRights,
                                                       userSession.getSecurityType(),
                                                       ownerLevel);

          // Export LC should not appear in this dropdown since templates can
          // never be created for them
            if (instrumentTypes.contains(TradePortalConstants.EXPORT_DLC)) {
 	      instrumentTypes.removeElement(TradePortalConstants.EXPORT_DLC);
            }

	  //Build the option tags for the dropdown box.
	  options = Dropdown.getInstrumentList( instrType, loginLocale, instrumentTypes);
          defaultText = resMgr.getText("NewInstTemplate1.selectInstType",
                                       TradePortalConstants.TEXT_BUNDLE);

	  //Display the dropdown box
          out.println(InputField.createSelectField("InstrumentType", "",
                      defaultText, options, "ListText", false));
%>
          <%=resMgr.getText("NewInstTemplate1.Template",
                            TradePortalConstants.TEXT_BUNDLE)%>
        </p>
      </td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
<%
    if (showExpress) {
%>
      <tr align="left" valign="middle">
        <td>&nbsp;</td>
        <td valign="bottom">
          <input type="checkbox" name="expressFlag">
        </td>
        <td>&nbsp;</td>
        <td nowrap class="ListText">
          <%=resMgr.getText("NewInstTemplate1.IsExpress", 
                            TradePortalConstants.TEXT_BUNDLE)%>
        </td>
        <td>&nbsp;</td>
      </tr>
<%
    }
%>

<%
	//IAZ CR-586 08/16/10 Begin 
    if (showFixed) {
%>
      <tr align="left" valign="middle">
        <td>&nbsp;</td>
        <td valign="bottom">
          <input type="checkbox" name="fixedFlag">
        </td>
        <td>&nbsp;</td>
        <td nowrap class="ListText">
          <%=resMgr.getText("NewInstTemplate1.IsFixed", 
                            TradePortalConstants.TEXT_BUNDLE)%>
        </td>
        <td>&nbsp;</td>
      </tr>
<%
    }
    //IAZ CR-586 08/16/10 End
%>
  </table>

  <br>

  <table width="100%" border="0" cellspacing="0" cellpadding="0" height="45"
         class="BankColor">
    <tr>
      <td width="20" nowrap height="31">&nbsp;</td>
      <td width="80%" class="BankColor" height="31">&nbsp;</td>
      <td width="9" height="31">&nbsp;</td>
      <td width="96" nowrap height="31">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td>
              <jsp:include page="/common/RolloverButtonSubmit.jsp">
                <jsp:param name="showButton" value="true" />
                <jsp:param name="name" value="NextStepButton" />
                <jsp:param name="image" value='common.NextStepImg' />
                <jsp:param name="text" value='common.NextStepText' />
                <jsp:param name="submitButton"
                           value="<%=TradePortalConstants.BUTTON_SAVE%>" />
              </jsp:include>
            </td>
            <td width="15" nowrap>&nbsp;</td>
            <td>
<%
              // The Cancel button link sends us to different pages based
              // on whether we're creating a new template or a new
              // instrument (from a template).  Set it accordingly.
              String link = null;
              if (createNewTemplate) 
                link=formMgr.getLinkAsUrl("goToRefDataHome", response);
              else
                link=formMgr.getLinkAsUrl("cleanUpDoc", response);
%>
              <jsp:include page="/common/RolloverButtonLink.jsp">
                <jsp:param name="name" value="CancelButton" />
                <jsp:param name="image" value='common.CancelImg' />
                <jsp:param name="text" value='common.CancelText' />
                <jsp:param name="link" value="<%=java.net.URLEncoder.encode(link)%>" />
              </jsp:include>
            <td width="20" nowrap>&nbsp;</td>
          </tr>
        </table>
      </td>
    </tr>
  </table>

  <% //if (isExpress.equals("true")) 
  if (TradePortalConstants.INDICATOR_ON.equalsIgnoreCase(isExpress)){ %>
         <script LANGUAGE="JavaScript">
             document.NewTemplate1.expressFlag.checked='true';
         </script>
  <% } /*end if */ %>

  <%
  //IAZ CR-586 08/16/10 Begin
  if (TradePortalConstants.INDICATOR_ON.equalsIgnoreCase(isFixed)) { %>
         <script LANGUAGE="JavaScript">
             document.NewTemplate1.fixedFlag.checked='true';
         </script>
  <% }
  //IAZ CR-586 08/16/10 End 
  %>
  
  <%= formMgr.getFormInstanceAsInputField("NewTemplateForm", secParms) %>

</form>
</body>
</html>

<%
  formMgr.storeInDocCache("default.doc", new DocumentHandler());
  Debug.debug("***END************NEW TEMPLATE*******************END***");
%>


