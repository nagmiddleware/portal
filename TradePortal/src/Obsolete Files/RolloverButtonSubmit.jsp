<%--
*******************************************************************************
                            Rollover Button Submit

  Description:
    This includable JSP create HTML for a href with image that acts like
  a submit button.  The resulting HTML supports rollover logic so that the 
  image is replace with another image when the mouse moves over it.  The 
  showButton parameter can be used to supress the display of the button and 
  replace it with a non-breaking space.  showButton defaults to true.

  The width and height of this button is 96x17.
  
  This JSP relies on the changeImage javascript method defined in header.jsp

  The image displayed on the button is based on the image parameter passed
  in.  This is a resource bundle key.  The rollover image is based on that
  save key value with RO atteched.  E.g. common.SaveImg and common.SaveImgRO

  Each image within the link is given a unique name based on the passed in
  name plus a random 2 digit number.  This allows multiple buttons with the
  same name to support rollover correctly (i.e., only the rolled over button
  changes images, not all of them).

  NOTE: If a page has more than one form and there are submit buttons on 
  the second form, the formNum parameter MUST be passed in order for the 
  button to work correctly.

  Parameters:
    showButton - true/false: indicates if <input> tag or nbsp; is returned
    name       - name to assign to the button, also see above
    image      - resource bundle key for the image in normal state
    text       - resource bundle key for the alternate text for the image
    extraTags  - any extra attributes that should be included in the onClick
                 attribute.  Optional.
    width      - width of the button.  Default value is 96
    submitButton - a constant that is placed in a hidden field when the image
                 is clicked.  This can be used by the mediator to determine
                 which button was pressed.  Constants are of the form BUTTON_x
    form       - a number representing a form on a page.  The default form is
                 0.  This is passed in the setButtonPressed method which then
                 sets the buttonName field on the given form (e.g., 0 or 1).

*******************************************************************************
--%>
<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.ams.tradeportal.common.*, com.ams.tradeportal.busobj.util.*,
         com.amsinc.ecsg.util.*" %>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session"></jsp:useBean>
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>

<%
  boolean showButton = true;
  String name;
  String image;
  String text;
  String extraTags;
  String width;
  String submitButton;
  String formNum;
  String brandingDirectory;
  String source;
  String sourceRO;
  
  String value = request.getParameter("showButton");
  if ((value != null) && value.equals("false")) showButton = false;

  name  = request.getParameter("name");
  image = request.getParameter("image");
  text  = request.getParameter("text");
  width = request.getParameter("width");
  extraTags = request.getParameter("extraTags");  
  submitButton = request.getParameter("submitButton");
  formNum = request.getParameter("formNum");
  
//Validation to check Cross Site Scripting
  if(formNum != null){
	  formNum = StringFunction.xssCharsToHtml(formNum);
  }
  if(name != null){
	 	 name = StringFunction.xssCharsToHtml(name);
  }
  
  // If a brandingDirectory paramter is passed in, use the parameter passed
  // in for branding directory.
  // If brandingDirectory parameter is not passed in.  The button is branded if the 
  // client bank set "Use button images from branding directory" to true.
  // If brandingDirectory parameter is passed in and equals "" then no branding directory is used.
  brandingDirectory = request.getParameter("brandingDirectory"); 
   if(brandingDirectory == null)
    {
      if (userSession.getBrandButtonInd())
      {
         brandingDirectory = userSession.getBrandingDirectory();
      }
	}

  if(!InstrumentServices.isBlank(brandingDirectory))
   {
	// For branded images, the text resource bundle only contains the path
	// under the branding directory
	source = TradePortalConstants.IMAGES_PATH + brandingDirectory + "/" +
			resMgr.getText(image, TradePortalConstants.TEXT_BUNDLE);

	sourceRO = TradePortalConstants.IMAGES_PATH + brandingDirectory + "/" +
			resMgr.getText(image + "RO", TradePortalConstants.TEXT_BUNDLE);
   }
  else
   {
    // For unbranded images, the text reousrce is the path under the image path

    source = TradePortalConstants.IMAGES_PATH + resMgr.getText(image, 
                                TradePortalConstants.TEXT_BUNDLE);
    sourceRO = TradePortalConstants.IMAGES_PATH + resMgr.getText(image + "RO",
                                   TradePortalConstants.TEXT_BUNDLE);
                                   
    }
 
  if ((width == null) || width.equals("")) width = "96";

  // formNum is a parameter passed to setButtonPressed.  The default value
  // is for form 0.  Most pages only have one form so this is an optional
  // parameter.  Pages with more than one form must send the formNum parameter.

  if ((formNum == null) || formNum.equals("")) formNum = "0";

  // Including return in the onClickTag determines whether or not the 
  // form subission continues or not based on whatever is returned
  String onClickTag = "setButtonPressed('" + submitButton + "', "
                      + formNum + "); ";
  if((extraTags == null) || (extraTags.equals("")) || (extraTags.equals("null")))
    onClickTag = "return "+onClickTag;
  else
    onClickTag += extraTags;

  String submit = "javascript:document.forms[" + formNum + "].submit()";

  // If a page has multiple buttons with the same name (e.g., Save), we have a problem
  // because rolling over one would cause all of the buttons with that name to
  // change image.  To get around this, we create a unique name by appending two
  // random numbers to the image name with a dot separator.  Thus a sample
  // name might be PartySearch5.21  Although it is possible that this logic could
  // generate the exact same set of numbers for two buttons, the odds are very low.

  long aNumber = (long)(Math.random()*100);
  String imageName = name + aNumber + ".";
  aNumber = (long)(Math.random()*100);
  imageName += aNumber;


  if (showButton) {
%>
  
    <a href="<%=submit%>" name="<%=name%>" onClick="<%=onClickTag%>"
       onMouseOut="changeImage('<%=imageName%>', '<%=source%>')"
       onMouseOver="changeImage('<%=imageName%>', '<%=sourceRO%>')">
       <img border="0" name="<%=imageName%>" 
            src="<%=source%>"
            alt="<%=resMgr.getText(text,
                                   TradePortalConstants.TEXT_BUNDLE)%>"></a>
<% } else {
       out.println("&nbsp;");
   }
%>
