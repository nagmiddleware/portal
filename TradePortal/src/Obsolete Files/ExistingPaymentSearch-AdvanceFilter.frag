<%--
*****************************************************************************************************
                  Cash Management Transaction Search Advanced Filter include file

  Description:
    This is a JSP meant to be included using the <%@ include file="x.jsp" %> 
  tag.  It creates the HTML for displaying the Advanced Filter fields for the 
  Cash Management Instrument Search.  As it is included with <%@ include %> rather than with
  the <jsp:include> directive, it is not a standalone servlet.  
 
  NOTE: Since this JSP is copied into the including page at compile time of
  the including page, changes to this page do not automatically force the
  including pages to be recompiled.  If you make changes to this page, you must
  delete the including page's servlet in order to see the changes.

********************************************************************************************************
--%>

	<input type="hidden" name="NewSearch" value="Y">

<!--          <a href=<%//=link%>><%//=linkText%></a>-->

<div id="advancdePaymentFilter" style="padding-top: 5px;">
 	<div>
  		<% 
  			options = Dropdown.getInstrumentList(instrumentType, loginLocale, instrumentTypes );
        %>
    	<%=widgetFactory.createSearchSelectField("InstrumentType","CashMgmtTransactionSearch.TransactionType"," ", options, "onChange='filterPayments(\"Advanced\");'")%>
    	
    	<%
    		options = Dropdown.createSortedRefDataOptions(TradePortalConstants.CURRENCY_CODE, currency, loginLocale);
    	%>
    	<%=widgetFactory.createSearchSelectField("Currency","CashMgmtTransactionSearch.Currency"," ", options, "onChange='filterPayments(\"Advanced\");'")%>

		<button data-dojo-type="dijit.form.Button" type="button" class="title-right">
               <%=resMgr.getText("common.SearchGreyText", TradePortalConstants.TEXT_BUNDLE)%>
            <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
	        	filterPayments("Advanced");return false;
	      	</script>
        </button><br>
        <span class="title-right"><a href="<%=linkHref%>"><%=linkName%></a></span>
	</div>
	<div>	
		<%
		 	String amtStr = resMgr.getText("CashMgmtTransactionSearch.AmountRange",TradePortalConstants.TEXT_BUNDLE) + " " +
		 					resMgr.getText("CashMgmtTransactionSearch.From", TradePortalConstants.TEXT_BUNDLE);
			System.out.println("*** Existing Payment Search - AmtStr>>: "+amtStr);
		%>
		<%=widgetFactory.createSearchAmountField("AmountFrom", amtStr, "", "")%><!-- "27"-->
		<%=widgetFactory.createSearchAmountField("AmountTo", "CashMgmtTransactionSearch.To", "", "")%><!-- "27"-->
		
		<%=widgetFactory.createSearchTextField("OtherParty","CashMgmtTransactionSearch.OtherParty","30", "")%>
	</div>
</div>