<%--
 *
 *     Copyright  � 2008                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
**********************************************************************************
                        Cash Management Transaction History Tab

  Description:
    Contains HTML to create the History tab for the Cash Management Transaction page

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transactions-HistoryListView.jsp" %>
*******************************************************************************
--%> 

<%
   //StringBuffer statusOptions = new StringBuffer();           // NSX - PRUK080636392 - 08/16/10 
   StringBuffer statusExtraTags = new StringBuffer();

    // NSX - PRUK080636392 - 08/16/10 - Begin
   StringBuffer statusOptions = Dropdown.createActiveInActiveStatusOptions(resMgr,selectedStatus);
   /*
   // Build the status dropdown options (hardcoded in ACTIVE (default), INACTIVE, ALL order), 
   // re-selecting the most recently selected option.

   statusOptions.append("<option value='");
   statusOptions.append(STATUS_ACTIVE);
   statusOptions.append("' ");
   if (STATUS_ACTIVE.equals(selectedStatus)) {
      statusOptions.append(" selected ");
   }
   statusOptions.append(">");
   statusOptions.append(STATUS_ACTIVE);
   statusOptions.append("</option>");

   statusOptions.append("<option value='");
   statusOptions.append(STATUS_INACTIVE);
   statusOptions.append("' ");
   if (STATUS_INACTIVE.equals(selectedStatus)) {
      statusOptions.append(" selected ");
   }
   statusOptions.append(">");
   statusOptions.append(STATUS_INACTIVE);
   statusOptions.append("</option>");

   statusOptions.append("<option value='");
   statusOptions.append(STATUS_ALL);
   statusOptions.append("' ");
   if (STATUS_ALL.equals(selectedStatus)) {
      statusOptions.append(" selected ");
   }
   statusOptions.append(">");
   statusOptions.append(STATUS_ALL);
   statusOptions.append("</option>");
*/          // NSX - PRUK080636392 - 08/16/10 - End

   // Upon changing the status selection, automatically go back to the Transactions Home
   // page with the selected status type.  Force a new search to occur (causes cached
   // "where" clause to be reset.)

   statusExtraTags.append("onchange=\"location='");
   statusExtraTags.append(formMgr.getLinkAsUrl("goToPaymentTransactionsHome", response));
   statusExtraTags.append("&amp;current2ndNav=");
   statusExtraTags.append(current2ndNav);
   statusExtraTags.append("&NewDropdownSearch=Y");
   statusExtraTags.append("&amp;instrStatusType='+this.options[this.selectedIndex].value\"");

%>
	
		<div border-bottom: 1px solid grey; padding-top: 10px;">
			<%=widgetFactory.createSearchSelectField("instrStatusType", "CashMgmtTransactionHistory.Status", "", statusOptions.toString(), "onChange='filterPayments()'")%>
			
			<% Debug.debug("*** Existing Payment Search - statusOptions>>: "+statusOptions.toString()); %>
			<% Debug.debug("*** Existing Payment Search - statusExtraTags>>: "+statusExtraTags.toString()); %>
        	
			<%
			      // When the Instrument History tab is selected, display 
			      // Organizations dropdown or the single organization.
			    if ((SecurityAccess.hasRights(userSecurityRights, SecurityAccess.VIEW_CHILD_ORG_WORK)) 
			           && (totalOrganizations > 1))
				{
					dropdownOptions.append(Dropdown.getUnsortedOptions(hierarchyDoc, "ORGANIZATION_OID", "NAME", selectedOrg, true));
					dropdownOptions.append("<option value=\"");
					dropdownOptions.append(EncryptDecrypt.encryptAlphaNumericString(ALL_ORGANIZATIONS));
					dropdownOptions.append("\"");

					if (selectedOrg.equals(ALL_ORGANIZATIONS)) {
   						dropdownOptions.append(" selected");
					}

					dropdownOptions.append(">");
					dropdownOptions.append(ALL_ORGANIZATIONS);
					dropdownOptions.append("</option>");
					
					extraTags = new StringBuffer("");
					extraTags.append("onchange=\"location='");
					extraTags.append(formMgr.getLinkAsUrl("goToPaymentTransactionsHome", response));
					extraTags.append("&amp;current2ndNav=");
					extraTags.append(current2ndNav);
					extraTags.append("&NewDropdownSearch=Y");
					extraTags.append("&historyOrg='+this.options[this.selectedIndex].value\"");
			%>
				<%=widgetFactory.createSearchSelectField("Organization", "InstrumentHistory.Show", "", dropdownOptions.toString(), "onChange='filterPayments()'")%>
				
				<% Debug.debug("*** Existing Payment Search - dropdownOptions>>: "+dropdownOptions.toString()); %>
				<% Debug.debug("*** Existing Payment Search - extraTags>>: "+extraTags.toString()); %>
			<%
			      }
			%> 
		</div>     

		<%
			String linkName = "Basic";
			String paramStr = "&SearchType=";
			
			if (searchType == null) {
				searchType = TradePortalConstants.SIMPLE;
				paramStr=paramStr+TradePortalConstants.ADVANCED;
			} else if (searchType.equals(TradePortalConstants.ADVANCED)) {
				linkName = "Basic";
				paramStr=paramStr+TradePortalConstants.SIMPLE;
			} else if (searchType.equals(TradePortalConstants.SIMPLE)) {
				linkName = "Advanced";
				paramStr=paramStr+TradePortalConstants.ADVANCED;
			}
		
		
			String linkHref = formMgr.getLinkAsUrl("goToPaymentsSearch", paramStr, response);
		%>
		
		<%
			if (searchType.equals(TradePortalConstants.ADVANCED)) {
		%> 
				<%@ include file="/transactions/fragments/ExistingPaymentSearch-AdvanceFilter.frag" %>
		<%
			} else {
		%> 
				<%@ include file="/transactions/fragments/ExistingPaymentSearch-BasicFilter.frag" %>
		<% 	} %>

		<input type=hidden name=SearchType value=<%=searchType%>>

		<%
			DataGridFactory dgFactory = new DataGridFactory(resMgr, formMgr, response);
			String gridHtml = dgFactory.createDataGrid("existingPaymentSearchDataGridId","ExistingPaymentSearchDataGrid",null);
			//String gridLayout = dgFactory.createGridLayout("ExistingPaymentSearchDataGrid");
		%>
		<%=gridHtml%>
			<br/>		  



