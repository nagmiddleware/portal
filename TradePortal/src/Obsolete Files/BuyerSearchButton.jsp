<%--
*******************************************************************************
                         Rollover Button Submit

  Description:
    This includable JSP create HTML for a buyer search button.  It is a 
  basically a pass through to the RolloverButtonSubmit.jsp.  However, the
  button name is always BuyerSearch (while the image name takes on the name
  that was passed in).  The resulting HTML supports rollover logic
  and submits the page for doing a buyer search.

  This is some sample HTML produced:
  
     <a href="javascript:document.forms[0].submit()" name="BuyerSearch"
        onMouseOut="changeImage('SearchAppButton', 
                                '/portal/images/Button_SearchforApplicant_off.gif')"
        onMouseOver="changeImage('SearchAppButton', 
                                 '/portal/images/Button_SearchforApplicant_ro.gif')"
        onClick="setButtonPressed('BuyerSearch'); return setBuyerSearchFields('APP');">
       <img border="0" name="SearchAppButton" width=140 height=17
            src="/portal/images/Button_SearchforApplicant_off.gif"
            alt="Search"></a>

  The button is ALWAYS called BuyerSearch.  (Thus all buyer search buttons submit
  to the same location.)

  To use this jsp, include an entry similar to the following in your jsp.

        <jsp:include page="/common/BuyerSearchButton.jsp">
           <jsp:param name="showButton" value="<%=!isReadOnly%>" />
           <jsp:param name="name" value="SearchBenButton" />
           <jsp:param name="image" value='common.SearchBeneficiaryImg' />
           <jsp:param name="text" value='common.SearchText' />
           <jsp:param name="width" value="140" />
        </jsp:include>

  See below for a definition of the parameters.

  Parameters:
    showButton - true/false: indicates if <input> tag or nbsp; is returned
    name       - logical 'name' of the button, used for determing the mouse
                 over variable names -- see above.  This is NOT the name of 
                 the button, the button is always called BuyerSearchButton.
    image      - resource bundle key for the image in normal state
    text       - resource bundle key for the alternate text for the image
    width      - width of the button.  Default value is 96
    extraTage  - extra javascript to execute on the onclick event of the button.

*******************************************************************************
--%>
<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.ams.tradeportal.common.*" %>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session"></jsp:useBean>

<%
  boolean showButton = true;
  String name;

  String value = request.getParameter("showButton");
  if ((value != null) && value.equals("false")) showButton = false;

  name  = request.getParameter("name");
  String clicktag = "";

  if (showButton) {
%>
      <jsp:include page="/common/RolloverButtonSubmit.jsp">
        <jsp:param name="showButton" value='true' />
        <jsp:param name="name" value="<%=TradePortalConstants.BUTTON_BUYERSEARCH%>" />
        <jsp:param name="image" value='<%=request.getParameter("image")%>' />
        <jsp:param name="text" value='<%=request.getParameter("text")%>' />
        <jsp:param name="width" value='<%=request.getParameter("width")%>' />
        <jsp:param name="extraTags" value='<%=request.getParameter("extraTags")%>' />
        <jsp:param name="submitButton" value="<%=TradePortalConstants.BUTTON_BUYERSEARCH%>" />
        <jsp:param name="imageName" value='<%=request.getParameter("name")%>' />
      </jsp:include>
<% } else {
       out.println("&nbsp;");
   }
%>
