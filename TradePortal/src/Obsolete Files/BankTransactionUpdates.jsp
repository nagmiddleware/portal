<%--
**********************************************************************************
                              Bank Transaction Updates List page
  Description:
     This page is used to allow admin users to view all authorized tranasctions, 
     which are not integreted with TPS
**********************************************************************************
--%>

<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*, com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*, com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*, com.ams.tradeportal.html.*,
                 com.ams.tradeportal.busobj.webbean.*,java.util.*" %>

<jsp:useBean id="beanMgr"     class="com.amsinc.ecsg.web.BeanManager"            scope="session"></jsp:useBean>
<jsp:useBean id="formMgr"     class="com.amsinc.ecsg.web.FormManager"            scope="session"></jsp:useBean>
<jsp:useBean id="resMgr"      class="com.amsinc.ecsg.util.ResourceManager"       scope="session"></jsp:useBean>
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>

<%-- ************** Data retrieval page setup begins here ****************  --%>

<%

  Debug.debug("***START***********Bank TRANSACTION Update LIST***************START***");

DocumentHandler   xmlDoc              = null;
DocumentHandler   corpCustomersDoc	= null;
String            ownershipLevel      = null;
String			instrTypeOptions	= null;
StringBuffer 		sqlQuery 			= null;
StringBuffer		customerOptions		= new StringBuffer();
Vector<String> 	instrumentTypes 	= new Vector<String>();
StringBuffer 	bankUpdateStatusOptions = new StringBuffer();
StringBuffer 	transactionTypes = new StringBuffer();
DocumentHandler bankUpdateStatusDoc = null;

  
WidgetFactory widgetFactory = new WidgetFactory(resMgr);
DataGridFactory dgFactory = new DataGridFactory(resMgr, userSession, formMgr, response);
ReferenceDataManager refData = ReferenceDataManager.getRefDataMgr();

  // Set the global navigation tab indicator to the Update Centre tab
  userSession.setCurrentPrimaryNavigation("AdminNavigationBar.UpdateCentre");
  //set the current navigation page in session 
   if ( "true".equals( request.getParameter("returning") ) ) {
     userSession.pageBack(); //to keep it correct
   }
   else {
     userSession.addPage("goToBankTransactionUpdates");
   }
  
  Map searchQueryMap =  userSession.getSavedSearchQueryData();
  Map savedSearchQueryData =null;
  // Determine whether any errors are being returned to this page. If there are, set the 
  // original search criteria selected/entered by the user for the listview.
  xmlDoc = formMgr.getFromDocCache();

  ownershipLevel = userSession.getOwnershipLevel();
  String loginLocale = userSession.getUserLocale();
		  
  //Building options for bank update status dropdown
  sqlQuery = new StringBuffer();
  sqlQuery.append("select CODE, DESCR from refdata where TABLE_TYPE = ? ");
 // sqlQuery.append(" and LOCALE_NAME ? ");
 
  bankUpdateStatusDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery.toString(), false, new Object[]{TradePortalConstants.BANK_UPDATE_STATUS_TYPE});
  //Add option All
  bankUpdateStatusOptions.append("<option value=\"").append(TradePortalConstants.STATUS_ALL).append("\" >");
  bankUpdateStatusOptions.append(resMgr.getText( "common.all", TradePortalConstants.TEXT_BUNDLE));
  bankUpdateStatusOptions.append("</option>");
  
  bankUpdateStatusOptions.append(Dropdown.createSortedOptions(bankUpdateStatusDoc, "CODE", 
          "DESCR", "", resMgr.getResourceLocale()));
  
  //Building options for TransactionType
  transactionTypes.append("<option value=\"").append(TradePortalConstants.ISSUE).append("\" >");
  transactionTypes.append(resMgr.getText( "common.issue", TradePortalConstants.TEXT_BUNDLE));
  transactionTypes.append("</option>");
  transactionTypes.append("<option value=\"").append(TradePortalConstants.AMEND).append("\" >");
  transactionTypes.append(resMgr.getText( "common.amend", TradePortalConstants.TEXT_BUNDLE));
  transactionTypes.append("</option>");
  transactionTypes.append("<option value=\"").append(TradePortalConstants.RELEASE).append("\" >");
  transactionTypes.append(resMgr.getText( "common.release", TradePortalConstants.TEXT_BUNDLE));
  transactionTypes.append("</option>");
  
  
  //Building options for CustomerName
   sqlQuery = new StringBuffer();
   sqlQuery.append("select organization_oid, name");
   sqlQuery.append(" from corporate_org"); 
   sqlQuery.append(" where activation_status = ? ");
   sqlQuery.append(" order by ");
   sqlQuery.append(resMgr.localizeOrderBy("name"));

   corpCustomersDoc  = DatabaseQueryBean.getXmlResultSet(sqlQuery.toString(), false, new Object[]{TradePortalConstants.ACTIVE});
   customerOptions.append(Dropdown.createSortedOptions(corpCustomersDoc, "ORGANIZATION_OID", 
           "NAME", "", resMgr.getResourceLocale()));
   
   //Building options for instrumentTypes
   instrumentTypes.addElement(TradePortalConstants.AIR_WAYBILL);
   instrumentTypes.addElement(TradePortalConstants.IMPORT_DLC);
   instrumentTypes.addElement(TradePortalConstants.LOAN_RQST);
   instrumentTypes.addElement(TradePortalConstants.GUARANTEE);
   instrumentTypes.addElement(TradePortalConstants.STANDBY_LC);
   instrumentTypes.addElement(TradePortalConstants.SHIP_GUAR);
   instrTypeOptions = Dropdown.getInstrumentList( "", loginLocale, instrumentTypes );
  
  String searchNav = "BankTransUpdate";  
  String currency = "";
  String options = Dropdown.createSortedRefDataOptions(TradePortalConstants.CURRENCY_CODE, currency, loginLocale);

%>

<jsp:include page="/common/Header.jsp">
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>
<jsp:include page="/common/SearchCriteriaCleanup.jsp">
<jsp:param name="searchCriteriaSection" value="<%=searchNav%>"/>
</jsp:include>
<div class="pageMainNoSidebar">
  <div class="pageContent">

    <jsp:include page="/common/PageHeader.jsp">
      <jsp:param name="titleKey" value="BankTransactionUpdate.Title" />
      <jsp:param name="helpUrl"  value="admin/corp_cust_search.htm" />
    </jsp:include>

    <form name="BankTransactionUpdateForm" method="POST" action="<%= formMgr.getSubmitAction(response) %>">      
      <input type=hidden value="" name=buttonName>
      <div class="formContentNoSidebar">

        <div class="searchHeader">
	        <%=widgetFactory.createSearchSelectField("bankUpdateStatus","BankTransactionUpdate.show","", bankUpdateStatusOptions.toString(), "onChange='filterBankTranUpdateGridView();'" )%>
			<%=widgetFactory.createCheckboxField("xmlDownloadInd", "BankTransactionUpdate.XMLDownload", false, false, false,"onClick='filterBankTranUpdateGridView();'","","none")%>
			<div class="searchHeaderActions">
				<jsp:include page="/common/gridShowCount.jsp">
					<jsp:param name="gridId" value="bankTransactionUpdateGridId" />
				</jsp:include>
	
				<span id="bankTransactionUpdateRefresh" class="searchHeaderRefresh"></span>
				<%=widgetFactory.createHoverHelp("bankTransactionUpdateRefresh","RefreshImageLinkHoverText")%>
				<span id="bankTransactionUpdateGridEdit" class="searchHeaderEdit"></span>
				<%=widgetFactory.createHoverHelp("bankTransactionUpdateGridEdit","CustomizeListImageLinkHoverText")%>
				</span>
				<div style="clear: both"></div>
			</div>
			<div class="searchDivider"></div>

			<div class="searchDetail">
				<span class="searchCriteria"> 
				<%=widgetFactory.createSearchTextField("InstrumentId","BankTransactionUpdate.InstrumentID","16", " class='char15' onKeydown='filterGridOnEnter(window.event, \"InstrumentId\");'")%>
				<%=widgetFactory.createSearchSelectField("CustomerName","BankTransactionUpdate.Customer"," ", customerOptions.toString(), "onChange='filterBankTranUpdateGridView();'" )%>
					<%=widgetFactory.createSearchTextField("BankInstrumentId","BankTransactionUpdate.BankInstrumentID","16", " class='char15' onKeydown='filterGridOnEnter(window.event, \"BankInstrumentId\");'")%>
				</span>
				<div style="clear: both;"></div>
				<%=widgetFactory.createSearchSelectField("InstrumentType","BankTransactionUpdate.InstrumentType"," ", instrTypeOptions, "onChange='filterBankTranUpdateGridView();'" )%>
				<%=widgetFactory.createSearchSelectField("TransactionType","BankTransactionUpdate.TransactionType"," ", transactionTypes.toString(), "onChange='filterBankTranUpdateGridView();'" )%>
				<div style="clear: both;"></div>
				<%=widgetFactory.createSearchSelectField("Currency", "BankTransactionUpdate.Currency", " ", options,"onChange='filterBankTranUpdateGridView();'")%>
				<%=widgetFactory.createSubLabel("BankTransactionUpdate.Amount")%>
				<%=widgetFactory.createSearchAmountField("AmountFrom", "BankTransactionUpdate.From","","style='width:8em' class='char5'", "")%>
				<%=widgetFactory.createSearchAmountField("AmountTo", "BankTransactionUpdate.To","","style='width:8em' class='char5'", "")%>
				<span class="searchActions">
            <button id="searchButton" data-dojo-type="dijit.form.Button" type="button" >
              <%=resMgr.getText("common.search",TradePortalConstants.TEXT_BUNDLE)%>
            </button>
            <%=widgetFactory.createHoverHelp("searchButton","SearchHoverText") %>
          </span>
				<div style="clear: both;"></div>
			</div>

        
          
<% 
if(searchQueryMap!=null && !searchQueryMap.isEmpty()){
	 savedSearchQueryData = (Map)searchQueryMap.get("BankTransactionUpdateDataView");
}

  String gridHtml = dgFactory.createDataGrid("bankTransactionUpdateGridId","BankTransactionUpdateDataGrid",null);
%>

<%= gridHtml %>
<% String gridLayout = dgFactory.createGridLayout("bankTransactionUpdateGridId","BankTransactionUpdateDataGrid"); %>        

      </div>

    </form>

  </div>
</div>

<jsp:include page="/common/Footer.jsp">
  <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
  <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>
<%@ include file="/common/GetSavedSearchQueryData.frag"%>
<script>

var bankUpdateStatusType = "";
var bankXMLDownloadInd = "";
var customer = "";
var instrumentID = "";
var bankInstrumentID = "";
var transactionID = "";
var instrumentType = "";
var transactionType = "";
var currency = "";
var amountFrom = "";
var amountTo = "";
var initSearchParms = "";
var bankTransactionUpdateGridId ="";
var gridLayout = "";
var viewName = "";
var tempDatePattern = "";
var savedSort = savedSearchQueryData["sort"];


  require(["dijit/registry", "dojo/on", "t360/common", "t360/datagrid", "t360/popup", "dojo/dom","dojo/query", "dojo/ready"],
      function(registry, on, common, t360grid, t360popup, dom,query, ready) {
	  ready(function() {
		  
		gridLayout = <%= gridLayout %>;
		viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("BankTransactionUpdateDataView",userSession.getSecretKey())%>"; 
		
	 	bankUpdateStatusType = savedSearchQueryData["bankUpdateStatusType"];
	 	bankXMLDownloadInd = savedSearchQueryData["bankXMLDownloadInd"];
	 	customer = savedSearchQueryData["customer"];
	 	instrumentID = savedSearchQueryData["instrumentID"];
	 	bankInstrumentID = savedSearchQueryData["bankInstrumentID"];
	 	instrumentType = savedSearchQueryData["instrumentType"];
	 	transactionType = savedSearchQueryData["transactionType"];
	 	currency = savedSearchQueryData["currency"];
	 	amountFrom = savedSearchQueryData["amountFrom"];
	 	amountTo = savedSearchQueryData["amountTo"];
	 	
	 	if("" == bankUpdateStatusType || null == bankUpdateStatusType || "undefined" == bankUpdateStatusType)
	 		bankUpdateStatusType = registry.byId('bankUpdateStatus').attr('value');	
    	else
    		  registry.byId("bankUpdateStatus").set('value',bankUpdateStatusType);
	 	
	 	if("" == bankXMLDownloadInd || null == bankXMLDownloadInd || "undefined" == bankXMLDownloadInd){
	 		bankXMLDownloadInd="";
	 	}
    	else{
    		  dom.byId("xmlDownloadInd").checked = bankXMLDownloadInd;
    	}
	 	
	 	if("" == customer || null == customer || "undefined" == customer)
	 		customer = registry.byId('CustomerName').attr('value');	
    	else
    		  registry.byId("CustomerName").set('value',customer);
	 	
	 	if("" == instrumentID || null == instrumentID || "undefined" == instrumentID)
    		instrumentID = checkString( dijit.byId("InstrumentId") );
   		else
   			dom.byId("InstrumentId").value = instrumentID;
	 	
	 	if("" == bankInstrumentID || null == bankInstrumentID || "undefined" == bankInstrumentID)
	 		bankInstrumentID = checkString( dijit.byId("BankInstrumentId") );
   		else
   			dom.byId("BankInstrumentId").value = bankInstrumentID;
	 	
	 	if("" == instrumentType || null == instrumentType || "undefined" == instrumentType)
    		instrumentType = registry.byId('InstrumentType').attr('value');	
    	else
    		registry.byId("InstrumentType").set('value',instrumentType);
	 	
	 	if("" == transactionType || null == transactionType || "undefined" == transactionType)
	 		transactionType = registry.byId('TransactionType').attr('value');	
    	else
    		registry.byId("TransactionType").set('value',transactionType);
	 	
	 	if("" == currency || null == currency || "undefined" == currency)
	 		currency = registry.byId('Currency').attr('value');	
    	else
    		registry.byId("Currency").set('value',currency);
	 	
	 	if("" == amountFrom || null == amountFrom || "undefined" == amountFrom)
	 		amountFrom = checkString( dijit.byId("AmountFrom") );
   		else
   			dom.byId("AmountFrom").value = amountFrom;
	 	
	 	if("" == amountTo || null == amountTo || "undefined" == amountTo)
	 		amountTo = checkString( dijit.byId("AmountTo") );
   		else
   			dom.byId("AmountTo").value = amountTo;

	 	initSearchParms = initSearchParms+"&bankUpdateStatusType="+bankUpdateStatusType+"&bankXMLDownloadInd="+bankXMLDownloadInd+
				        "&customer="+customer+"&instrumentID="+instrumentID+"&bankInstrumentID="+bankInstrumentID+
						"&transactionType="+transactionType+"&instrumentType="+instrumentType+
						"&currency="+currency+"&amountFrom="+amountFrom+"&amountTo="+amountTo;
		console.log("initSearchParms-"+initSearchParms);
		
		bankTransactionUpdateGridId =t360grid.createDataGrid("bankTransactionUpdateGridId", viewName, gridLayout, initSearchParms);
		    
		 query('#bankTransactionUpdateRefresh').on("click", function() {
			 filterBankTranUpdateGridView();
	     });
		 
		 query('#bankTransactionUpdateGridEdit').on("click", function() {
	          var columns = t360grid.getColumnsForCustomization('bankTransactionUpdateGridId');
	          var parmNames = [ "gridId", "gridName", "columns" ];
	          var parmVals = [ "bankTransactionUpdateGridId", "BankTransactionUpdateDataGrid", columns ];
	          t360popup.open(
	            'bankTransactionUpdateGridEdit', 'gridCustomizationPopup.jsp',
	            parmNames, parmVals,
	            null, null);  //callbacks
	     });
		 
		 registry.byId("searchButton").on("click", function() {
			 filterBankTranUpdateGridView();
		      });
		
	    });
    
  });
  
  function filterBankTranUpdateGridView() {
	  require(["dojo/dom","t360/datagrid","dijit/registry"],
	      function(dom,t360grid,registry){
		 
		    var searchParms = "";
		    
	        bankUpdateStatusType = registry.byId("bankUpdateStatus").attr('value');
	     	bankXMLDownloadInd = dom.byId("xmlDownloadInd").checked;
	     	if(false === bankXMLDownloadInd){
	     		bankXMLDownloadInd = "";
	     	}
	        customer = registry.byId("CustomerName").attr('value');
	        instrumentID = dom.byId("InstrumentId").value;
	        bankInstrumentID = dom.byId("BankInstrumentId").value;
	        //transactionID = dom.byId("TransactionId").value;
	        instrumentType = registry.byId("InstrumentType").attr('value');
	        transactionType = registry.byId("TransactionType").attr('value');
	        currency = registry.byId("Currency").attr('value');
	        amountFrom = dom.byId("AmountFrom").value;
	        amountTo = dom.byId("AmountTo").value;
	        
	        searchParms = searchParms+"&bankUpdateStatusType="+bankUpdateStatusType+"&bankXMLDownloadInd="+bankXMLDownloadInd+
	        "&customer="+customer+"&instrumentID="+instrumentID+"&bankInstrumentID="+bankInstrumentID+
			"&transactionType="+transactionType+"&instrumentType="+instrumentType+
			"&currency="+currency+"&amountFrom="+amountFrom+"&amountTo="+amountTo;
	        
	        //searchParms = "";
	        console.log("searchParms-"+searchParms);
	               
	        t360grid.searchDataGrid("bankTransactionUpdateGridId", "BankTransactionUpdateDataView",searchParms);
	            
	    });
  }
  
  function filterGridOnEnter(event, fieldID){
		require(["dojo/on","dijit/registry"],function(on, registry) {
		    on(registry.byId(fieldID), "keypress", function(event) {
		        if (event && event.keyCode == 13) {
		        	dojo.stopEvent(event);
		        	filterBankTranUpdateGridView();
		        }
		    });
		});
	  }

  function checkString(inquiryStr){
		if(null == inquiryStr)
			return "";
		else
			return inquiryStr.attr('value');
	}  
 

</script>

<jsp:include page="/common/gridShowCountFooter.jsp">
  <jsp:param name="gridId" value="bankTransactionUpdateGridId" />
</jsp:include>

</body>
</html>

<%
   // Finally, reset the cached document to eliminate carryover of
   // information to the next visit of this page.
   formMgr.storeInDocCache("default.doc", new DocumentHandler());
%>
