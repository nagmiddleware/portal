

<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.ams.tradeportal.busobj.webbean.*,com.ams.tradeportal.common.*,
                 com.ams.tradeportal.mediator.*,com.ams.tradeportal.html.*,
                 com.ams.tradeportal.busobj.util.*,java.util.*,com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*" %>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session"></jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session"></jsp:useBean>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session"></jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>

<%
    Debug.debug("***START********************NEW TRANSACTION STEP 3**************************START***");
    userSession.setCurrentPrimaryNavigation("NavigationBar.Transactions");

    /**************************************************************************
     * THIS SCRIPTLET SETS THE GLOBALS FOR THE PAGE AND REFRESHES THE PAGE FROM
     * THE DOC CACHE IF WE'RE RETURNING TO THE PAGE AFTER AN ERROR
     **************************************************************************/

    //GET DOC
    DocumentHandler queryDoc = null;
    DocumentHandler doc      = formMgr.getFromDocCache();
    Debug.debug("doc -> \n" + doc.toString());

    String instrOid         = null;
    String instrType        = null;
    StringBuffer options    = new StringBuffer();

    Vector possibleCodes    = null;

    instrOid = InstrumentServices.parseForOid(doc.getAttribute("/In/instrumentOid"));

    InstrumentWebBean instr = (InstrumentWebBean)beanMgr.createBean ("com.ams.tradeportal.busobj.webbean.InstrumentWebBean", "Instrument");
    instr.getById(instrOid);

    instrType = instr.getAttribute("instrument_type_code");
    possibleCodes = InstrumentServices.getPossibleTransactionTypes(instrType, instr.getAttribute("original_transaction_oid"));

    for (int i=0; i<possibleCodes.size(); i++)
    {
        String code = (String)possibleCodes.elementAt(i);
        try
        {
            options.append("<option value='").append(code).append("'>");
            options.append(ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.TRANSACTION_TYPE, code, resMgr.getResourceLocale()));
            options.append("</option>");
        }
        catch (AmsException e)
        {
            Debug.debug("Error getting description for transaction type code -> " + code);
        }
    }
%>
<%-- Body tag included as part of common header --%>
<jsp:include page="/common/Header.jsp">
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeErrorSectionFlag"  value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="">
<form method="post" name="NewTransStep3Form" action="<%=formMgr.getSubmitAction(response)%>">
  <table width="100%" border="0" cellspacing="0" cellpadding="0" height="21">
    <tr>
      <td width="20" class="BankColor" nowrap height="25"><img src="/portal/images/Blank_15.gif" width="15" height="15"></td>
      <td class="BankColor" valign="middle" align="left" nowrap height="25">
        <p class="ControlLabelWhite">
          <%= resMgr.getText("NewTransExisIns1.HeadingStep3", TradePortalConstants.TEXT_BUNDLE) %>
        </p>
        </td>
      <td class="BankColor" width="100%" height="25" align="center" valign="middle">
        <p class="ControlLabelWhite">&nbsp;</p>
      </td>
      <td class="BankColor" width="15" height="25">
	    <%= OnlineHelp.createContextSensitiveLink("customer/new_transaction_context.htm",resMgr, userSession) %>
	</td>
      <td class="BankColor" width="20" height="25" nowrap>&nbsp;</td>
      <td class="BankColor" width="1" height="25" nowrap>&nbsp; </td>
      <td width="1" class="BankColor" nowrap height="25">&nbsp;</td>
    </tr>
  </table>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td>&nbsp;</td>
    </tr>
  </table>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td width="15" nowrap>&nbsp;</td>
      <td nowrap>
        <p class="ControlLabel">
          <%= resMgr.getText("NewTransExisIns1.Create", TradePortalConstants.TEXT_BUNDLE) %>
        </p>
      </td>
      <td width="100%">&nbsp;</td>
    </tr>
  </table>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
  </table>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td width="30" nowrap>&nbsp;</td>
      <td width="15" nowrap>&nbsp;</td>
      <td nowrap>
        <p class="ListText">
          <%= resMgr.getText("NewTransExisIns1.Select", TradePortalConstants.TEXT_BUNDLE) %>
        </p>
      </td>
      <td width="100%">&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td nowrap class="ListText" height="30" valign="bottom">
        <p class="ListText">
        <%
        /***********************************
         * Start Bank Branch Dropdown Box
         ***********************************/
        Debug.debug("Bank Branch Dropdown");
        Debug.debug(options.toString());
        %>
          <select name="transactionType" class="ListText">
          <%= options.toString() %>
        </select>
        <%
        /***********************************
         * End Bank Branch Dropdown Box
         ***********************************/
        %>
        </p>
      </td>
      <td>&nbsp;</td>
    </tr>
  </table>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td>&nbsp;</td>
    </tr>
  </table>
  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="BankColor" height="48">
    <tr>
      <td width="100%">&nbsp;</td>
      <td>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td>&nbsp;</td>
            <td width="15" nowrap>&nbsp;</td>
            <td>
                <jsp:include page="/common/RolloverButtonSubmit.jsp">
                    <jsp:param name="name" value="nextButton" />
                    <jsp:param name="image" value='common.NextStepImg' />
                    <jsp:param name="text" value='common.NextStepText' />
                    <jsp:param name="submitButton" value="<%=TradePortalConstants.BUTTON_SELECT%>" />
                </jsp:include>
            </td>
            <td width="15" nowrap>&nbsp;</td>
            <td>
                <jsp:include page="/common/RolloverButtonLink.jsp">
                    <jsp:param name="showButton" value="true" />
                    <jsp:param name="name" value="CloseButton" />
                    <jsp:param name="image" value='common.CloseImg' />
                    <jsp:param name="text" value='common.CloseText' />
                    <jsp:param name="link" value='<%= java.net.URLEncoder.encode(formMgr.getLinkAsUrl("cleanUpDoc", response)) %>' />
                </jsp:include>
            </td>
            <td width="20" nowrap>&nbsp;</td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <%= formMgr.getFormInstanceAsInputField("NewTransStep3Form") %>
  </form>
</body>

</html>

<%
  if (doc!=null)
    doc.removeAllChildren("/Error");
  formMgr.storeInDocCache("default.doc", doc);
%>

