<%--
*******************************************************************************
                       PO Upload Definition Selection for ATP

  Description: Allows the user to select a PO Upload Definition.  When creating
  a new ATP Creation Rule, the user must first select an upload definition.
  This page allows the user to select one and then continue to a validation
  page.

  Clicking the Next Step button calls the ValidateDefnSelection JSP to 
  determine if the user made a selection.  If not, the user is returned here.

*******************************************************************************
--%>

<%--
 *
 *     Copyright   2007                         
 *     CGI
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.html.*" %>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager"
   scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager"
   scope="session">
</jsp:useBean>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager"
   scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean"
   scope="session">
</jsp:useBean>

<%-- ************** Data retrieval/page setup begins here ****************  --%>

<%

  String options;
  String defaultText;
  String defaultDefnOid = "";
  String loginLocale    = userSession.getUserLocale();
  String loginRights    = userSession.getSecurityRights();

  DocumentHandler doc = formMgr.getFromDocCache();

  DocumentHandler definitionsDoc = new DocumentHandler();

  StringBuffer sql = new StringBuffer();

  QueryListView queryListView = null;

  try {
      queryListView = (QueryListView) EJBObjectFactory.createClientEJB(
				formMgr.getServerLocation(), "QueryListView"); 

      // Build a list of PO Upload Definitions for the user's org.
      sql.append("select po_upload_definition_oid, name ");
      sql.append(" from po_upload_definition");
      sql.append(" where a_owner_org_oid in (" + userSession.getOwnerOrgOid());

      // Also include PO defs from the user's actual organization if using subsidiary access
      if(userSession.showOrgDataUnderSubAccess())
       {
          sql.append(",").append(userSession.getSavedUserSession().getOwnerOrgOid());
       }

      sql.append(") and definition_type in (");
      sql.append("'" + TradePortalConstants.PO_DEFINITION_TYPE_UPLOAD + "',");
      sql.append("'" + TradePortalConstants.PO_DEFINITION_TYPE_BOTH + "')"); 
      sql.append(" order by ");
      sql.append(resMgr.localizeOrderBy("name"));
      Debug.debug(sql.toString());

      queryListView.setSQL(sql.toString());

      queryListView.getRecords();

      definitionsDoc = queryListView.getXmlResultSet();
      Debug.debug(definitionsDoc.toString());

      // Special navigation logic: if there is only one definition, there's no
      // point in making the user select it.  Simply pass this definition oid to
      // the ATPCreationRule page.
      if (queryListView.getRecordCount() ==1) {
        String definition = definitionsDoc.getAttribute("/ResultSetRecord(0)/PO_UPLOAD_DEFINITION_OID");
			 definition = EncryptDecrypt.encryptStringUsingTripleDes(definition, userSession.getSecretKey());
        formMgr.setCurrPage("ATPCreationRuleDetail");
        String physicalPage = NavigationManager.getNavMan().getPhysicalPage("ATPCreationRuleDetail", request);
%>
        <jsp:forward page='<%= physicalPage %>'>
          <jsp:param name="UploadDefinition" value="<%=definition%>" />
        </jsp:forward>
<%
      }

      // Determine if any PO Upload Definition is the default.  If so, we'll use
      // that as the definition to select in the dropdown list.
      sql = new StringBuffer("select po_upload_definition_oid");
      sql.append(" from po_upload_definition ");
      sql.append(" where a_owner_org_oid = " + userSession.getOwnerOrgOid());
      sql.append(" and default_flag='Y'");
      Debug.debug(sql.toString());

      DocumentHandler resultDoc = DatabaseQueryBean.getXmlResultSet(sql.toString(), false);
      if (resultDoc != null) {
        defaultDefnOid = resultDoc.getAttribute("/ResultSetRecord(0)/PO_UPLOAD_DEFINITION_OID");
      }

    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      try {
          if (queryListView != null) {
              queryListView.remove();
          }
      } catch (Exception e) {
          System.out.println("error removing querylistview in POUploadATPDefnSelection.jsp");
      }
  }  // try/catch/finally block


  //SECURE PARAMETERS
  Hashtable secParms = new Hashtable();
  secParms.put("userOid", userSession.getUserOid());
  secParms.put("securityRights", loginRights);

%>

<%-- ********************* HTML for page begins here *********************  --%>

<%
  // onLoad is set to default the cursor to the dropdown field
  String onLoad = "document.POUploadDefnSelection.UploadDefinition.focus();";
%>

<%-- Body tag included as part of common header --%>
<jsp:include page="/common/Header.jsp">
   <jsp:param name="includeNavigationBarFlag"
              value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeErrorSectionFlag"
              value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="additionalOnLoad" value="<%=onLoad%>" />
</jsp:include>

<%-- Note the non-standard ACTION on this form.  Rather than going through the
     AMS Servlet continue to a validation page (which validates the selection) --%>

<form name="POATPUploadDefnSelection" method="POST" 
      action="<%= request.getContextPath()+NavigationManager.getNavMan().getPhysicalPage("ValidateATPDefinitionSelection", request) %>">

  <table width="100%" border="0" cellspacing="0" cellpadding="0" height="34">
    <tr>
      <td width="20" class="BankColor" nowrap>&nbsp;</td>
      <td class="BankColor" width="300" nowrap height="34">
        <p class="ControlLabelWhite">
          <%=resMgr.getText("POUploadDefnSelection.NewATPCreationRuleStep1",
                            TradePortalConstants.TEXT_BUNDLE)%>
        </p>
      </td>
      <td width="100%" class="BankColor" height="34">&nbsp; </td>
      <td class=BankColor align=right valign=middle width=15 height=34 nowrap>
      
      </td>
      <td width="2" class="BankColor" nowrap>&nbsp;</td>
    </tr>
  </table>

  <table width=100% border=0 cellspacing=0 cellpadding=0 class="ColorBeige">
    <tr>
      <td width=20 nowrap>&nbsp;</td>
      <td width="100%"> 
        <p class="AsterixText">
          <span class="Asterix">*</span>
            <%=resMgr.getText("common.Required",
                              TradePortalConstants.TEXT_BUNDLE)%>
        </p>
      </td>
    </tr>
  </table>

  <br>

  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td width="20" nowrap>&nbsp;</td>
      <td class="ControlLabel">
        <p class="ControlLabel">
          <%=resMgr.getText("POUploadDefnSelection.ATPSelectDefn", 
                            TradePortalConstants.TEXT_BUNDLE)%>
        </p>
      </td>
      <td>&nbsp;</td>
    </tr>
  </table>

  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td>&nbsp;</td>
      <td width="40" nowrap>&nbsp;</td>
      <td nowrap height="30" valign="bottom"> 
        <p class="ControlLabel">
          <%=resMgr.getText("POUploadDefnSelection.POUploadDefinition",
                            TradePortalConstants.TEXT_BUNDLE)%>
          <span class="AsterixText">*</span>
        </p>
      </td>
      <td width="100%">&nbsp;</td>
    </tr>
    <tr> 
      <td height="25">&nbsp;</td>
      <td height="25">&nbsp;</td>
      <td nowrap class="ListText" height="25" valign="bottom"> 
        <p class="ListText">
<%
          // Build the list of definitions (with encrypted oids)
          options = ListBox.createOptionList(definitionsDoc, 
                                      "PO_UPLOAD_DEFINITION_OID", "NAME", 
                                      defaultDefnOid, userSession.getSecretKey());
          defaultText = resMgr.getText("POUploadDefnSelection.selectDefinition",
                                       TradePortalConstants.TEXT_BUNDLE);
          out.println(InputField.createSelectField("UploadDefinition", "", 
                              defaultText, options, "ListText", false));
%>
        </p>
      </td>
      <td height="25">&nbsp;</td>
    </tr>
  </table>

  <br>

  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="BankColor" height="48">
    <tr> 
      <td width="100%">&nbsp;</td>
      <td> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td>&nbsp;</td>
            <td width="15" nowrap>&nbsp;</td>

            <td width="8" height="17" align="right" valign="middle">
              <jsp:include page="/common/RolloverButtonSubmit.jsp">
                <jsp:param name="showButton" value="true" />
                <jsp:param name="name" value="NextStepButton" />
                <jsp:param name="image" value='common.NextStepImg' />
                <jsp:param name="text" value='common.NextStepText' />
                <jsp:param name="submitButton"
                           value="<%=TradePortalConstants.BUTTON_SAVE%>" />
              </jsp:include>
            </td>
            <td width="15" nowrap>&nbsp;</td>

            <td width="8" height="17" align="right">
<%
              String link = formMgr.getLinkAsUrl("goToRefDataHome", response);
%>
              <jsp:include page="/common/RolloverButtonLink.jsp">
                <jsp:param name="name" value="CloseButton" />
                <jsp:param name="image" value='common.CloseImg' />
                <jsp:param name="text" value='common.CloseText' />
                <jsp:param name="link" value="<%=java.net.URLEncoder.encode(link)%>" />
              </jsp:include>
            </td>

            <td width="20" nowrap>&nbsp;</td>
          </tr>
        </table>
      </td>
    </tr>
  </table>

</form>
</body>
</html>

<%
  formMgr.storeInDocCache("default.doc", doc);
%>

