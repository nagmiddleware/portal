<%--
*******************************************************************************
                                  Admin User Detail

  Description:
     This page is used to maintain corporate users.  It supports insert,
  update and delete.  Errors found during a save are redisplayed on the same 
  page.  Successful updates return the user to the ref data home page.

  Assumption: The organization and level of the org are always known.  
  Therefore, for a new user that information is passed in to this page.

*******************************************************************************
--%>

<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.html.*" %>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" 
   scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" 
   scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean"
   scope="session">
</jsp:useBean>

<%-- ************** Data retrieval page setup begins here ****************  --%>

<%
  String options;
  String loginRights    = userSession.getSecurityRights();

  DocumentHandler orgDoc = new DocumentHandler();
  String ownershipLevel = userSession.getOwnershipLevel();

  StringBuffer sql = new StringBuffer();

  QueryListView queryListView = null;

  try {

      queryListView = (QueryListView) EJBObjectFactory.createClientEJB(
				formMgr.getServerLocation(), "QueryListView"); 

      // Now get a list of organizations.  This is a little tricky.  By selecting
      // an org, the user is implicitly selecting an ownership level as well.  To
      // accomplish this, we'll get a list of organizations (which the user can 
      // view) and rather than selecting just the org's oid, we'll select the org's
      // oid and the ownership level.  This will be one value appended together
      // with a '/' separator.  (Thus, it might be 100/GLOBAL).

      sql = new StringBuffer();

     if (ownershipLevel.equals(TradePortalConstants.OWNER_GLOBAL)) {
         // For global org users, the list of orgs is the global org plus all
         // client banks
         sql.append("select organization_oid || '/");
         sql.append(TradePortalConstants.OWNER_GLOBAL);
         sql.append("' as OwnerOrgAndLevel, name from global_organization");
         sql.append(" where activation_status='ACTIVE'");
         sql.append(" union ");
         sql.append("select organization_oid || '/");
         sql.append(TradePortalConstants.OWNER_BANK);
         sql.append("' as OwnerOrgAndLevel, name from client_bank");
         sql.append(" where activation_status='ACTIVE'");
      } else if (ownershipLevel.equals(TradePortalConstants.OWNER_BANK)) {
         // For Client Bank users, the list of orgs is the user's client bank
         // plus all BOGs for that client bank
         sql.append("select organization_oid || '/");
         sql.append(TradePortalConstants.OWNER_BANK);
         sql.append("' as OwnerOrgAndLevel, name from client_bank");
         sql.append(" where organization_oid = " + userSession.getClientBankOid());
         sql.append(" and activation_status='ACTIVE'");
         sql.append(" union ");
         sql.append("select organization_oid || '/");
         sql.append(TradePortalConstants.OWNER_BOG);
         sql.append("' as OwnerOrgAndLevel, name from bank_organization_group");
         sql.append(" where p_client_bank_oid = " + userSession.getClientBankOid());
         sql.append(" and activation_status='ACTIVE'");
      } else if (ownershipLevel.equals(TradePortalConstants.OWNER_BOG)) {
         // For BOG users, the list of orgs is only the user's bog.  (Note
         // this is provided only for consistency.  The org field is not a
         // visible field for BOG users.)
         sql.append("select organization_oid || '/");
         sql.append(TradePortalConstants.OWNER_BOG);
         sql.append("' as OwnerOrgAndLevel, name from bank_organization_group");
         sql.append(" where organization_oid = " + userSession.getBogOid());
      }
      sql.append(" order by name");

      Debug.debug(sql.toString());
      queryListView.setSQL(sql.toString());

      queryListView.getRecords();

      orgDoc = queryListView.getXmlResultSet();
      Debug.debug(orgDoc.toString());


      // Special navigation logic: if there is only one org, there's no
      // point in making the user select it.  (This is always true for a BOG user
      // -- he can only add users for his BOG.)  Simply pass this org/level to
      // the AdminUserDetail page.
      if (queryListView.getRecordCount() ==1) {
        String orgAndLevel = orgDoc.getAttribute("/ResultSetRecord(0)/OWNERORGANDLEVEL");
        orgAndLevel = EncryptDecrypt.encryptStringUsingTripleDes(orgAndLevel, userSession.getSecretKey());
        formMgr.setCurrPage("AdminUserDetail");
        String physicalPage = 
                    NavigationManager.getNavMan().getPhysicalPage("AdminUserDetail", request);
%>
        <jsp:forward page='<%= physicalPage %>'>
          <jsp:param name="OrgAndLevel" value="<%=orgAndLevel%>" />
        </jsp:forward>
<%
      }

    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      try {
          if (queryListView != null) {
              queryListView.remove();
          }
      } catch (Exception e) {
          System.out.println("error removing querylistview in AdminUserOrgSelection.jsp");
      }
  }  // try/catch/finally block



  //SECURE PARAMETERS
  Hashtable secParms = new Hashtable();
  secParms.put("userOid", userSession.getUserOid());
  secParms.put("securityRights", loginRights);

%>

<%-- ********************* HTML for page begins here *********************  --%>

<jsp:include page="/common/ButtonPrep.jsp" />

<%
  String onLoad = "document.AdminUserOrgSelection.OrgAndLevel.focus();";
%>

<%-- Body tag included as part of common header --%>
<jsp:include page="/common/Header.jsp">
   <jsp:param name="includeNavigationBarFlag" 
              value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeErrorSectionFlag"  
              value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="additionalOnLoad" value="<%=onLoad%>" />
</jsp:include>


<form name="AdminUserOrgSelection" method="post" 
      action="<%= request.getContextPath()+NavigationManager.getNavMan().getPhysicalPage("ValidateAdminUserOrgSelection", request) %>">
  <input type=hidden value="" name=buttonName>

  <table width="100%" border="0" cellspacing="0" cellpadding="0" height="34">
    <tr>
      <td width="20" class="BankColor" nowrap>&nbsp;</td>
      <td class="BankColor" width="260" nowrap height="34">
        <p class="ControlLabelWhite">
          <%=resMgr.getText("NewAdminUser.Heading",
                            TradePortalConstants.TEXT_BUNDLE)%>
        </p>
      </td>
      <td width="100%" class="BankColor" height="34">&nbsp; </td>
      <td class=BankColor align=right valign=middle width=15 height=34 nowrap>
        <%= OnlineHelp.createContextSensitiveLink("admin/new_admin_user_step_one.htm",resMgr, userSession) %>
      </td>
      <td width="2" class="BankColor" nowrap>&nbsp;</td>
    </tr>
  </table>

  <br>

  <table width=100% border=0 cellspacing=0 cellpadding=0 class="ColorBeige">
    <tr>
      <td width=20 nowrap>&nbsp;</td>
      <td width="100%"> 
        <p class="AsterixText">
          <span class="Asterix">*</span>
            <%=resMgr.getText("common.Required",
                              TradePortalConstants.TEXT_BUNDLE)%>
        </p>
      </td>
    </tr>
  </table>

  <br>

  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td width="20" nowrap>&nbsp;</td>
      <td nowrap> 
        <p class="ControlLabel">
          <%=resMgr.getText("NewAdminUser.SelectTheOrg", 
                            TradePortalConstants.TEXT_BUNDLE)%>
          </p>
      </td>
      <td width="100%">&nbsp;</td>
    </tr>
  </table>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td>&nbsp;</td>
      <td width="40" nowrap>&nbsp;</td>
      <td nowrap height="30" valign="bottom"> 
        <p class="ControlLabel">
          <%=resMgr.getText("NewAdminUser.Org", 
                            TradePortalConstants.TEXT_BUNDLE)%>
          <span class="AsterixText">*</span>
        </p>
      </td>
      <td width="100%">&nbsp;</td>
    </tr>
    <tr> 
      <td height="25">&nbsp;</td>
      <td height="25">&nbsp;</td>
      <td nowrap class="ListText" height="25" valign="bottom"> 
        <p class="ListText"> 
<%
          // Build the list of definitions (with encrypted oids)
          options = ListBox.createOptionList(orgDoc, 
                                      "OWNERORGANDLEVEL", "NAME", 
                                      "", userSession.getSecretKey());
          out.println(InputField.createSelectField("OrgAndLevel", "", 
                              "", options, "ListText", false));
%>
        </p>
      </td>
      <td height="25">&nbsp;</td>
    </tr>
  </table>

  <br>

  <table width="100%" border="0" cellspacing="0" cellpadding="0" height="45"
         class="BankColor">
    <tr>
      <td width="20" nowrap height="31">&nbsp;</td>
      <td width="80%" class="BankColor" height="31">&nbsp;</td>
      <td width="9" height="31">&nbsp;</td>
      <td width="96" nowrap height="31">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td>
              <jsp:include page="/common/RolloverButtonSubmit.jsp">
                <jsp:param name="showButton" value="true" />
                <jsp:param name="name" value="NextStepButton" />
                <jsp:param name="image" value='common.NextStepImg' />
                <jsp:param name="text" value='common.NextStepText' />
                <jsp:param name="submitButton"
                           value="<%=TradePortalConstants.BUTTON_SAVE%>" />
              </jsp:include>
            </td>
            <td width="15" nowrap>&nbsp;</td>
            <td>
<%
              // The Cancel button link sends us to different pages based
              // on whether we're creating a new template or a new
              // instrument (from a template).  Set it accordingly.
              String link = formMgr.getLinkAsUrl("goToAdminRefDataHome", response);
%>
              <jsp:include page="/common/RolloverButtonLink.jsp">
                <jsp:param name="name" value="CancelButton" />
                <jsp:param name="image" value='common.CancelImg' />
                <jsp:param name="text" value='common.CancelText' />
                <jsp:param name="link" value="<%=java.net.URLEncoder.encode(link)%>" />
              </jsp:include>
            <td width="20" nowrap>&nbsp;</td>
          </tr>
        </table>
      </td>
    </tr>
  </table>

</form>
</body>
</html>

<%
  formMgr.storeInDocCache("default.doc", new DocumentHandler());
  Debug.debug("***END************NEW ADMIN USER****************END***");
%>







