<%--
**********************************************************************************
                              Authorised Transaction XML List page

  Description:
     This page is used to allow admin users to download xml for authorised transactions

**********************************************************************************
--%>

<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*, com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*, com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*, com.ams.tradeportal.html.*,
                 com.ams.tradeportal.busobj.webbean.*,java.util.*" %>

<jsp:useBean id="beanMgr"     class="com.amsinc.ecsg.web.BeanManager"            scope="session"></jsp:useBean>
<jsp:useBean id="formMgr"     class="com.amsinc.ecsg.web.FormManager"            scope="session"></jsp:useBean>
<jsp:useBean id="resMgr"      class="com.amsinc.ecsg.util.ResourceManager"       scope="session"></jsp:useBean>
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>

<%-- ************** Data retrieval page setup begins here ****************  --%>

<%

  Debug.debug("***START***********AUTHORISED TRANSACTION XML LIST***************START***");

  DocumentHandler   xmlDoc              = null;
  DocumentHandler   corpCustomersDoc	= null;
  String            ownershipLevel      = null;
  String			instrTypeOptions	= null;
  StringBuffer 		xmlDownloadedOptions = new StringBuffer();
  StringBuffer 		sqlQuery 			= new StringBuffer();
  StringBuffer		customerOptions		= new StringBuffer();
  StringBuffer 		downloadLink 		= new StringBuffer();
  Vector<String> 	instrumentTypes 	= new Vector<String>();

  WidgetFactory widgetFactory = new WidgetFactory(resMgr);
  DataGridFactory dgFactory = new DataGridFactory(resMgr, userSession, formMgr, response);
  ReferenceDataManager refData = ReferenceDataManager.getRefDataMgr();

  // Set the global navigation tab indicator to the Update Centre tab
  userSession.setCurrentPrimaryNavigation("AdminNavigationBar.UpdateCentre");
  
  Map searchQueryMap =  userSession.getSavedSearchQueryData();
  Map savedSearchQueryData =null;
  // Determine whether any errors are being returned to this page. If there are, set the 
  // original search criteria selected/entered by the user for the listview.
  xmlDoc = formMgr.getFromDocCache();

  if ( "true".equals( request.getParameter("returning") ) ) {
	     userSession.pageBack(); //to keep it correct
  }else {
	     userSession.addPage("goToXMLDownloads");
  }
  
  downloadLink = new StringBuffer();
  downloadLink.append(request.getContextPath());
  downloadLink.append("/common/TradePortalDownloadServlet?");
  downloadLink.append(TradePortalConstants.DOWNLOAD_REQUEST_TYPE);
  downloadLink.append("=");
  downloadLink.append(TradePortalConstants.DOWNLOAD_AUTHORISED_TRANSACTION_XML);
  
  ownershipLevel = userSession.getOwnershipLevel();
  String loginLocale = userSession.getUserLocale();
		  
  String datePattern = userSession.getDatePattern();
  String datePatternDisplay = refData.getDescr("DATEPATTERN", datePattern, loginLocale);
  String dateWidgetOptions = "placeHolder:'" + datePatternDisplay + "'";

  //Building options for ShowDownloadedXML dropdown
  xmlDownloadedOptions.append("<option value=\"").
  append(TradePortalConstants.INDICATOR_YES).
  append("\" >");
  xmlDownloadedOptions.append(resMgr.getText( "common.Yes", TradePortalConstants.TEXT_BUNDLE));
  xmlDownloadedOptions.append("</option>");
  xmlDownloadedOptions.append("<option value=\"").
  append(TradePortalConstants.INDICATOR_NO).
  append("\" selected>");
  xmlDownloadedOptions.append(resMgr.getText( "common.No", TradePortalConstants.TEXT_BUNDLE));
  xmlDownloadedOptions.append("</option>");
  
  //Building options for CustomerName
   sqlQuery.append("select organization_oid, name");
   sqlQuery.append(" from corporate_org"); 
   sqlQuery.append(" where activation_status = '");
   sqlQuery.append(TradePortalConstants.ACTIVE);
   sqlQuery.append("'");
   sqlQuery.append(" and cust_is_not_intg_tps ='");
   sqlQuery.append(TradePortalConstants.INDICATOR_YES);
   sqlQuery.append("'");
   sqlQuery.append(" order by ");
   sqlQuery.append(resMgr.localizeOrderBy("name"));

   corpCustomersDoc  = DatabaseQueryBean.getXmlResultSet(sqlQuery.toString(), false);
   customerOptions.append(Dropdown.createSortedOptions(corpCustomersDoc, "ORGANIZATION_OID", 
           "NAME", "", resMgr.getResourceLocale()));
   
   //Building options for instrumentTypes
   instrumentTypes.addElement(TradePortalConstants.AIR_WAYBILL);
   instrumentTypes.addElement(TradePortalConstants.IMPORT_DLC);
   instrumentTypes.addElement(TradePortalConstants.LOAN_RQST);
   instrumentTypes.addElement(TradePortalConstants.GUARANTEE);
   instrumentTypes.addElement(TradePortalConstants.STANDBY_LC);
   instrumentTypes.addElement(TradePortalConstants.SHIP_GUAR);
   instrTypeOptions = Dropdown.getInstrumentList( "", loginLocale, instrumentTypes );
  
  String searchNav = "custAccess";  
%>

<jsp:include page="/common/Header.jsp">
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>
<jsp:include page="/common/SearchCriteriaCleanup.jsp">
<jsp:param name="searchCriteriaSection" value="<%=searchNav%>"/>
</jsp:include>
<div class="pageMainNoSidebar">
  <div class="pageContent">

    <jsp:include page="/common/PageHeader.jsp">
      <jsp:param name="titleKey" value="AuthorizedXMLTransactions.Title" />
      <jsp:param name="helpUrl"  value="/admin/corp_cust_search.htm" />
    </jsp:include>

    <form name="XMLDownloadForm" method="POST" action="<%=downloadLink.toString()%>">      
      <input type=hidden name="buttonName"     value="">
      <div id="transactionOIDList"></div>
      <div class="formContentNoSidebar">

        <div class="searchHeader">
        <%= widgetFactory.createSearchSelectField("ShowDownloadedXML","AuthorizedXMLTransactions.ShowXML","", xmlDownloadedOptions.toString(), "onChange='filterAuthListView();'" )%>
          <div class="searchHeaderActions">
            <%--cquinton 10/8/2012 include gridShowCounts --%>
            <jsp:include page="/common/gridShowCount.jsp">
              <jsp:param name="gridId" value="xmlDownloadGrid" />
            </jsp:include>
            
            <span id="xmlDownloadRefresh" class="searchHeaderRefresh"></span>
		      <%=widgetFactory.createHoverHelp("xmlDownloadRefresh","RefreshImageLinkHoverText") %>
		      <span id="xmlDownloadGridEdit" class="searchHeaderEdit"></span>
		      <%=widgetFactory.createHoverHelp("xmlDownloadGridEdit","CustomizeListImageLinkHoverText") %>
		    </span>
          <div style="clear:both"></div>
        </div>

        <div class="searchDivider"></div>

        <div class="searchDetail" >
          <span class="searchCriteria">
			<%=widgetFactory.createSearchSelectField("CustomerName","AuthorizedXMLTransactions.CustomerName"," ", customerOptions.toString(), "onChange='filterAuthListView();' " )%>
            <%=widgetFactory.createSearchTextField("InstrumentId","AuthorizedXMLTransactions.InstrumentId","16", " class='char15' onKeydown=' filterAuthListViewOnEnter(window.event, \"InstrumentId\");'")%>
			<%=widgetFactory.createSearchTextField("BankInstrumentId","AuthorizedXMLTransactions.BankInstrumentId","16", " class='char15' onKeydown=' filterAuthListViewOnEnter(window.event, \"BankInstrumentId\");'")%>
			
		  </span>
          <span class="searchActions">
            <button id="searchButton" data-dojo-type="dijit.form.Button" type="button" >
              <%=resMgr.getText("common.search",TradePortalConstants.TEXT_BUNDLE)%>
            </button>
            <%=widgetFactory.createHoverHelp("searchButton","SearchHoverText") %>
          </span>
          <div style="clear:both;"></div>
			<%=widgetFactory.createSearchDateField("AuthorisedDateFrom","AuthorizedXMLTransactions.DateFrom", " class='char8'", "constraints:{datePattern:'"+datePattern+"'},placeHolder:'"+datePatternDisplay+"'")%>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<%=widgetFactory.createSearchTextField("TransactionId","AuthorizedXMLTransactions.TransactionId","16", " class='char15' onKeydown=' filterAuthListViewOnEnter(window.event, \"TransactionId\");'")%>
			<%= widgetFactory.createSearchSelectField("InstrumentType","AuthorizedXMLTransactions.InstrumentType"," ", instrTypeOptions, "onChange='filterTransTypesOptions(); filterAuthListView();' " )%>
			
			<div style="clear:both;"></div>
			
			<%=widgetFactory.createSearchDateField("AuthorisedDateTo","AuthorizedXMLTransactions.DateTo", " class='char8' labelClass=\"formItemWithIndent11\" ", "constraints:{datePattern:'"+datePattern+"'},placeHolder:'"+datePatternDisplay+"'")%>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<%= widgetFactory.createSearchSelectField("Transaction","AuthorizedXMLTransactions.Transaction","", "", "onChange='filterAuthListView();' " )%>
			<div style="clear:both;"></div>
			
		</div>
          
<% 
if(searchQueryMap!=null && !searchQueryMap.isEmpty()){
	 savedSearchQueryData = (Map)searchQueryMap.get("AuthorisedTransactionXMLDataView");
}
  String gridHtml = dgFactory.createDataGrid("xmlDownloadGrid","AuthorisedTransactionXMLDataGrid",null);
%>

        <%= gridHtml %>
        <%= formMgr.getFormInstanceAsInputField("XMLDownloadForm", "") %>
<% String gridLayout = dgFactory.createGridLayout("AuthorisedTransactionXMLDataGrid"); %>        

      </div>
    </form>

  </div>
</div>

<jsp:include page="/common/Footer.jsp">
  <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
  <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>
<%@ include file="/common/GetSavedSearchQueryData.frag" %>
<script>

var showDownloadedXMLInd="";
var customer = "";
var instrumentID = "";
var bankInstrumentID = "";
var transactionID = "";
var instrumentType = "";
var transaction = "";
var dateFrom = "";
var dateTo = "";
var initSearchParms = "";
var xmlDownloadGrid ="";
var gridLayout = "";
var viewName = "";
var tempDatePattern = encodeURI('<%=datePattern%>');
var savedSort = savedSearchQueryData["sort"];


  function filterAuthListViewOnEnter(event, fieldID){
	require(["dojo/on","dijit/registry"],function(on, registry) {
	    on(registry.byId(fieldID), "keypress", function(event) {
	        if (event && event.keyCode == 13) {
	        	dojo.stopEvent(event);
	        	filterAuthListView();
	        }
	    });
	});	//end require
  }//end of filterCorpCustomerOnEnter()

  function filterAuthListView() {
	  require(["dojo/dom","t360/datagrid","dijit/registry"],
	      function(dom,t360grid,registry){
		    var searchParms = "";
	        showDownloadedXMLInd = registry.byId("ShowDownloadedXML").attr('value');
	        customer = registry.byId("CustomerName").attr('value');
	        instrumentID = dom.byId("InstrumentId").value;
	        bankInstrumentID = dom.byId("BankInstrumentId").value;
	        transactionID = dom.byId("TransactionId").value;
	        instrumentType = registry.byId("InstrumentType").attr('value');
	        transaction = registry.byId("Transaction").attr('value');
	        dateFrom = dom.byId("AuthorisedDateFrom").value;
	        dateTo = dom.byId("AuthorisedDateTo").value;
	        
	        searchParms = searchParms+"&showDownloadedXMLInd="+showDownloadedXMLInd+"&customer="+customer+"&instrumentID="+instrumentID+"&bankInstrumentID="+bankInstrumentID+
				"&transactionID="+transactionID+"&transaction="+transaction+"&instrumentType="+instrumentType+"&dateFrom="+dateFrom+"&dateTo="+dateTo+"&datePattern="+tempDatePattern;
			console.log("searchParms-"+searchParms);
	               
	        t360grid.searchDataGrid("xmlDownloadGrid", "AuthorisedTransactionXMLDataView",searchParms);
	            
	    });
  }

  function filterTransTypesOptions() {
	  require(["dojo/dom","dojo/store/Memory", "dijit/registry"],
	      function(dom,Memory,registry){
		    var instrumentType = registry.byId("InstrumentType").attr('value');
		    var transactionType = registry.byId("Transaction");
		    if("" == instrumentType || null == instrumentType || "undefined" == instrumentType)
		    {
		    		transactionType.store = new Memory({data: [
	    		                                           {id: "", name: ""},
	    		                                           {id: "<%=TradePortalConstants.ISSUE%>", name:"<%=resMgr.getText( "AuthorizedXMLTransactions.TransactionISS", TradePortalConstants.TEXT_BUNDLE)%>"},
	    		                                           {id: "<%=TradePortalConstants.AMEND%>", name:"<%=resMgr.getText( "AuthorizedXMLTransactions.TransactionAMD", TradePortalConstants.TEXT_BUNDLE)%>"},
	    		                                           {id: "<%=TradePortalConstants.RELEASE%>", name:"<%=resMgr.getText( "AuthorizedXMLTransactions.TransactionREL", TradePortalConstants.TEXT_BUNDLE)%>"},
		    		                                       ]});
		    }else if("<%=TradePortalConstants.AIR_WAYBILL%>" == instrumentType){
		    		transactionType.store = new Memory({data: [
	    		                                           {id: "", name: ""},
	    		                                           {id: "<%=TradePortalConstants.RELEASE%>", name:"<%=resMgr.getText( "AuthorizedXMLTransactions.TransactionREL", TradePortalConstants.TEXT_BUNDLE)%>"},
	    		                                       ]});
		    }else if("<%=TradePortalConstants.IMPORT_DLC%>" == instrumentType || "<%=TradePortalConstants.GUARANTEE%>" == instrumentType || "<%=TradePortalConstants.STANDBY_LC%>" == instrumentType){
		    		transactionType.store = new Memory({data: [
	    		                                           {id: "", name: ""},
	    		                                           {id: "<%=TradePortalConstants.ISSUE%>", name:"<%=resMgr.getText( "AuthorizedXMLTransactions.TransactionISS", TradePortalConstants.TEXT_BUNDLE)%>"},
	    		                                           {id: "<%=TradePortalConstants.AMEND%>", name:"<%=resMgr.getText( "AuthorizedXMLTransactions.TransactionAMD", TradePortalConstants.TEXT_BUNDLE)%>"},
	    		                                       ]});
		    }else if("<%=TradePortalConstants.LOAN_RQST%>" == instrumentType || "<%=TradePortalConstants.SHIP_GUAR%>" == instrumentType){
		    		transactionType.store = new Memory({data: [
	    		                                           {id: "", name: ""},
	    		                                           {id: "<%=TradePortalConstants.ISSUE%>", name:"<%=resMgr.getText( "AuthorizedXMLTransactions.TransactionISS", TradePortalConstants.TEXT_BUNDLE)%>"},
	    		                                       ]});
		    }
	            
	    });
  }
  
  require(["dijit/registry", "dojo/on", "t360/common", "t360/datagrid", "t360/popup", "dojo/dom","dojo/query","dojo/store/Memory", "dojo/ready"],
      function(registry, on, common, t360grid, t360popup, dom,query, Memory, ready) {
	  ready(function() {
		filterTransTypesOptions();
		gridLayout = <%= gridLayout %>;
		viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("AuthorisedTransactionXMLDataView",userSession.getSecretKey())%>"; 
		
    	showDownloadedXMLInd = savedSearchQueryData["showDownloadedXMLInd"];
    	customer = savedSearchQueryData["customer"];
    	instrumentID = savedSearchQueryData["instrumentID"];
    	bankInstrumentID = savedSearchQueryData["bankInstrumentID"];
    	transactionID = savedSearchQueryData["transactionID"];
    	instrumentType = savedSearchQueryData["instrumentType"];
    	transaction = savedSearchQueryData["transaction"];
    	dateFrom = savedSearchQueryData["dateFrom"];
    	dateTo = savedSearchQueryData["dateTo"];
    	
    	if("" == savedSort || null == savedSort || "undefined" == savedSort)
  		  savedSort = '0';
    	
    	if("" == showDownloadedXMLInd || null == showDownloadedXMLInd || "undefined" == showDownloadedXMLInd)
    		showDownloadedXMLInd = registry.byId('ShowDownloadedXML').attr('value');	
    	else
    		  registry.byId("ShowDownloadedXML").set('value',showDownloadedXMLInd);
        	
    	if("" == customer || null == customer || "undefined" == customer)
    		customer = registry.byId('CustomerName').attr('value');	
    	else
    		  registry.byId("CustomerName").set('value',customer);
    	
    	if("" == instrumentID || null == instrumentID || "undefined" == instrumentID)
    		instrumentID = checkString( dijit.byId("InstrumentId") );
   		else
   			dom.byId("InstrumentId").value = instrumentID;
    	
    	if("" == bankInstrumentID || null == bankInstrumentID || "undefined" == bankInstrumentID)
    		bankInstrumentID = checkString( dijit.byId("BankInstrumentId") );
   		else
   			dom.byId("BankInstrumentId").value = bankInstrumentID;
    	
    	if("" == transactionID || null == transactionID || "undefined" == transactionID)
    		transactionID = checkString( dijit.byId("TransactionId") );
   		else
   			dom.byId("TransactionId").value = transactionID;
    	
    	if("" == instrumentType || null == instrumentType || "undefined" == instrumentType)
    		instrumentType = registry.byId('InstrumentType').attr('value');	
    	else
    		  registry.byId("InstrumentType").set('value',instrumentType);
    	
    	if("" == transaction || null == transaction || "undefined" == transaction)
    		transaction = registry.byId("Transaction").attr('value');	
    	else
    		  registry.byId("Transaction").set('value',transaction);
    	
    	if("" == dateFrom || null == dateFrom || "undefined" == dateFrom)
   		 	dateFrom = dom.byId("AuthorisedDateFrom").value;
	   	else
	   		registry.byId("AuthorisedDateFrom").set('displayedValue',dateFrom);
	   	
	   	if("" == dateTo || null == dateTo || "undefined" == dateTo)
	   		 dateTo = dom.byId("AuthorisedDateTo").value;
	   	else
	   		registry.byId("AuthorisedDateTo").set('displayedValue',dateTo);
	   	
    	initSearchParms = initSearchParms+"&showDownloadedXMLInd="+showDownloadedXMLInd+"&customer="+customer+"&instrumentID="+instrumentID+"&bankInstrumentID="+bankInstrumentID+
    						"&transactionID="+transactionID+"&transaction="+transaction+"&instrumentType="+instrumentType+"&dateFrom="+dateFrom+"&dateTo="+dateTo+"&datePattern="+tempDatePattern;
    	console.log("initSearchParms-"+initSearchParms);
		xmlDownloadGrid =t360grid.createDataGrid("xmlDownloadGrid", viewName, gridLayout, initSearchParms);
		    
		var f =  registry.byId('AuthorizedXMLTransactionsList_XMLDownload');
		if (f) f.set('disabled',true);
		
		var xmlDownloadGrid = registry.byId('xmlDownloadGrid');
		 on(xmlDownloadGrid,"selectionChanged", function() {
		   if (f){
	        t360grid.enableFooterItemOnSelectionGreaterThan(xmlDownloadGrid,"AuthorizedXMLTransactionsList_XMLDownload",0);
		   }
		  });


	      
	      registry.byId("searchButton").on("click", function() {
	        var searchParms = "";
	        showDownloadedXMLInd = registry.byId("ShowDownloadedXML").attr('value');
	        customer = registry.byId("CustomerName").attr('value');
	        instrumentID = dom.byId("InstrumentId").value;
	        bankInstrumentID = dom.byId("BankInstrumentId").value;
	        transactionID = dom.byId("TransactionId").value;
	        instrumentType = registry.byId("InstrumentType").attr('value');
	        transaction = registry.byId("Transaction").attr('value');
	        dateFrom = dom.byId("AuthorisedDateFrom").value;
	        dateTo = dom.byId("AuthorisedDateTo").value;
	        
	        searchParms = searchParms+"&showDownloadedXMLInd="+showDownloadedXMLInd+"&customer="+customer+"&instrumentID="+instrumentID+"&bankInstrumentID="+bankInstrumentID+
				"&transactionID="+transactionID+"&transaction="+transaction+"&instrumentType="+instrumentType+"&dateFrom="+dateFrom+"&dateTo="+dateTo+"&datePattern="+tempDatePattern;
			console.log("searchParms-"+searchParms);
	               
	        t360grid.searchDataGrid("xmlDownloadGrid", "AuthorisedTransactionXMLDataView",searchParms);
	       
	      });
	      
	      
	      query('#xmlDownloadRefresh').on("click", function() {
	          filterAuthListView();
	        });
	        query('#xmlDownloadGridEdit').on("click", function() {
	          var columns = t360grid.getColumnsForCustomization('xmlDownloadGrid');
	          var parmNames = [ "gridId", "gridName", "columns" ];
	          var parmVals = [ "xmlDownloadGrid", "AuthorisedTransactionXMLDataGrid", columns ];
	          t360popup.open(
	            'xmlDownloadGridEdit', 'gridCustomizationPopup.jsp',
	            parmNames, parmVals,
	            null, null);  //callbacks
	        });
	    	  
	    });
    
  });

  function xmlDownload(){
	    var rowKeys = getSelectedGridRowKeys("xmlDownloadGrid");
		
		if(rowKeys == ""){
			alert("Please select record(s).");
			return;
		}
		submitFormWithParms("XMLDownloadForm", "transactionOid", rowKeys);
  }
  
  function submitFormWithParms(formName,parmName, parmValue) {
      if ( document.getElementsByName(formName).length > 0 ) {
    	deleteParms();
    	var myForm = document.getElementsByName(formName)[0];
        if (parmValue instanceof Array) {
          for (var myParmIdx in parmValue) {
            var myParmValue = parmValue[myParmIdx];
            var myParmName = parmName + myParmIdx;
            addParmToForm(myForm, myParmName, myParmValue);
          }
        }
        else { //assume a single
          addParmToForm(myForm, parmName, parmValue);
        }
        var formNumber = getFormNumber(formName);
        myForm.submit(); 
      } else {
        alert('Problem submitting form: form ' + formName + ' not found');
      }
    }

  function addParmToForm(myForm, parmName, parmValue) {
	  var parent = document.getElementById("transactionOIDList");
      var hiddenField = document.createElement("input");
      hiddenField.setAttribute("type", "hidden");
      hiddenField.setAttribute("name", parmName);
      hiddenField.setAttribute("id", parmName);
      hiddenField.setAttribute("value", parmValue);
      parent.appendChild(hiddenField);
    }
  
  function deleteParms() {
	  var parent = document.getElementById("transactionOIDList");
	  var childNodes = parent.childNodes;
	  var len = childNodes.length;
	  for(i=0; i<len; i++) {
		  parent.removeChild(document.getElementById('transactionOid'+i));
		}
    }
  
  function checkString(inquiryStr){
		if(null == inquiryStr)
			return "";
		else
			return inquiryStr.attr('value');
	}

  
</script>

<%--cquinton 10/8/2012 include gridShowCounts --%>
<jsp:include page="/common/gridShowCountFooter.jsp">
  <jsp:param name="gridId" value="xmlDownloadGrid" />
</jsp:include>

</body>
</html>

<%
   // Finally, reset the cached document to eliminate carryover of
   // information to the next visit of this page.
   formMgr.storeInDocCache("default.doc", new DocumentHandler());
%>
