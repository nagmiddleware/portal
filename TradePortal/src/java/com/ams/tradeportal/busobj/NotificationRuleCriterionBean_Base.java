
  

package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;




/*
 * Allows Notification Rules to be defined differently for particular transaction
 * types and separates the decision criteria to send an email or a notification
 * message for each transaction type.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class NotificationRuleCriterionBean_Base extends TradePortalBusinessObjectBean
{

  
  /* 
   * Register the attributes and associations of the business object
   */
   protected void registerAttributes() throws AmsException
   {  

      /* Register attributes defined in the Ancestor class */
      super.registerAttributes();

      /* criterion_oid - Unique Identifier */
      attributeMgr.registerAttribute("criterion_oid", "criterion_oid", "ObjectIDAttribute");
      
	  attributeMgr.registerAttribute("transaction_type_or_action", "transaction_type_or_action");

	  attributeMgr.registerAttribute("instrument_type_or_category", "instrument_type_or_category");	   

      /* setting - Determines if an email or notification message will be sent to the Corporate
         Customer. */
      attributeMgr.registerReferenceAttribute("send_notif_setting", "send_notif_setting", "NOTIF_RULE_SETTING");

     attributeMgr.registerReferenceAttribute("send_email_setting", "send_email_setting", "NOTIF_RULE_SETTING");

	  attributeMgr.registerAttribute("additional_email_addr", "additional_email_addr");	         

	  attributeMgr.registerAttribute("notify_email_freq", "notify_email_freq","NumberAttribute");	    	   
        /* Pointer to the parent NotificationRule */
      attributeMgr.registerAttribute("notify_rule_oid", "p_notify_rule_oid", "ParentIDAttribute");

    	attributeMgr.registerAttribute("notification_user_ids", "notification_user_ids");		
		
		attributeMgr.registerAttribute("clear_tran_ind", "clear_tran_ind", "IndicatorAttribute");	
   
   } 
   
     /*
   * Register the components of the business object
   */
   protected void registerComponents() throws RemoteException, AmsException
   {
      /* Register the components defined in the Ancestor class */
      super.registerComponents();

       registerOneToManyComponent("NotifyRuleUserListList","NotifyRuleUserListList");
   }
}
