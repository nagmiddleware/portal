
  

package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;




/*
 * The holiday informatino for any given year of a TPCalendar.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class TPCalendarYearBean_Base extends TradePortalBusinessObjectBean
{
private static final Logger LOG = LoggerFactory.getLogger(TPCalendarYearBean_Base.class);

  
  /* 
   * Register the attributes and associations of the business object
   */
   protected void registerAttributes() throws AmsException
   {  

      /* Register attributes defined in the Ancestor class */
      super.registerAttributes();
      
      /* tp_calendar_year_oid - Unique Identifier. */
      attributeMgr.registerAttribute("tp_calendar_year_oid", "tp_calendar_year_oid", "ObjectIDAttribute");
      
      /* year - The year e.g. 2010 */
      attributeMgr.registerAttribute("year", "year", "NumberAttribute");
      attributeMgr.requiredAttribute("year");
      attributeMgr.registerAlias("year", getResourceManager().getText("TPCalendarYearBeanAlias.year", TradePortalConstants.TEXT_BUNDLE));
      
      /* days_in_year - A 366-character string that represents the days in a year.  Each character
         represents one day.  
         
         0 - workday
         1 - holiday */
      attributeMgr.registerAttribute("days_in_year", "days_in_year");
      
        /* Pointer to the parent TPCalendar */
      attributeMgr.registerAttribute("tp_calendar_oid", "p_tp_calendar_oid", "ParentIDAttribute");
   
   }
   
 
   
 
 
   
}
