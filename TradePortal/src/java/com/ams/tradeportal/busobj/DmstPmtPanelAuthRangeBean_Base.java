


package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;




/*
 * A DmstPmtPanelAuthRange consists of a set of panel range for beneficiary
 * of payment transaction.
 * 
 *     Copyright  © 2003
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class DmstPmtPanelAuthRangeBean_Base extends TradePortalBusinessObjectBean
{
private static final Logger LOG = LoggerFactory.getLogger(DmstPmtPanelAuthRangeBean_Base.class);


  /*
   * Register the attributes and associations of the business object
   */
   protected void registerAttributes() throws AmsException
   {

      /* Register attributes defined in the Ancestor class */
      super.registerAttributes();

      /* dmst_pmt_panel_auth_range_oid - Unique identifier */
      attributeMgr.registerAttribute("dmst_pmt_panel_auth_range_oid", "dmst_pmt_panel_auth_range_oid", "ObjectIDAttribute");
      
      /* panel_auth_range_oid - panel range associated to beneficiary. */
      attributeMgr.registerAssociation("panel_auth_range_oid", "a_panel_auth_range_oid", "PanelAuthorizationRange");
            
      /* auth_status - Beneficiary status */
      attributeMgr.registerReferenceAttribute("auth_status", "auth_status", "BENE_AUTH_STATUS");
   
     /* Pointer to the parent ReferenceDataOwner */
     attributeMgr.registerAttribute("transaction_oid", "p_transaction_oid", "ParentIDAttribute");
            
   }

   /* 
    * Register the components of the business object
    */
    protected void registerComponents() throws RemoteException, AmsException
    {               
       // Nar release 8.3.0.0 CR 857 15 July 2013
       /* PanelAuthorizerList - The DomesticPaymentPanelRange has many panel Authorizer User.  */
       registerOneToManyComponent("PanelAuthorizerList","PanelAuthorizerList");
                 
    }


}
