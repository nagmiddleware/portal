

/*
 * This file is generated from the model.  Normally it should not be modified manually.
 * Contact the modeler first.  (In a pinch, it can be modified first and then retrofitted
 * to the model.)
 *
 */
package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;


/*
 * Parties and corporate customers can be set up to be related to accounts.
 * This account data is used on the loan request and funds transfer instruments.
 *
 *     Copyright  � 2003
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class AccountBean_Base extends TradePortalBusinessObjectBean
{
private static final Logger LOG = LoggerFactory.getLogger(AccountBean_Base.class);


  /*
   * Register the attributes and associations of the business object
   */
   protected void registerAttributes() throws AmsException
   {

      /* Register attributes defined in the Ancestor class */
      super.registerAttributes();

      /* account_oid - Unique identifier */
      attributeMgr.registerAttribute("account_oid", "account_oid", "ObjectIDAttribute");

      /* account_number - Number identifying the account */
      attributeMgr.registerAttribute("account_number", "account_number");
      attributeMgr.registerAlias("account_number", getResourceManager().getText("AccountBeanAlias.account_number", TradePortalConstants.TEXT_BUNDLE));

      /* currency - The currency of funds in the account */
      attributeMgr.registerReferenceAttribute("currency", "currency", "CURRENCY_CODE");
      attributeMgr.registerAlias("currency", getResourceManager().getText("AccountBeanAlias.currency", TradePortalConstants.TEXT_BUNDLE));

      /* owner_object_type - the type of object that is the owner of this account */
      attributeMgr.registerReferenceAttribute("owner_object_type", "owner_object_type", "ACCT_OWNER_TYPE");

      /* account_name - This is the legal account name that the bank has on file. */
      attributeMgr.registerAttribute("account_name", "account_name");

      /* bank_country_code - This represents the Bank's Country code for the account. The CorporateCustomer
         will provide valid values to the Trade Portal Reference data for validation. */
      attributeMgr.registerReferenceAttribute("bank_country_code", "bank_country_code", "COUNTRY_ISO3116_3");

      /* source_system_branch - Source System/Branch Identifier Codevalue that bank will enter. */
      attributeMgr.registerAttribute("source_system_branch", "source_system_branch");

      /* account_type - A text field entered by the Corporate Customer to describe the account.
         The values for account type are defined by the Corporate Customer, and will
         not be validated in the Trade Portal. */
      attributeMgr.registerAttribute("account_type", "account_type");

      /* available_for_loan_request - If selected this account will display as available when creating a loan
         request in the drop-down list of account numbers. (Note: currently all accounts
         in this list appear in the drop-down list). */
      attributeMgr.registerAttribute("available_for_loan_request", "available_for_loan_request", "IndicatorAttribute");

      /* available_for_domestic_pymts - If selected this account will be available in the debit from account number
         dropdown list for selection for domestic payments. */
      attributeMgr.registerAttribute("available_for_domestic_pymts", "available_for_domestic_pymts", "IndicatorAttribute");

      /* pending_transaction_balance - This field captures running pending movements for each account (to be subtracted/or
         added to from retrieved balances). This field will be display only; It is
         not editable. */
      attributeMgr.registerAttribute("pending_transaction_balance", "pending_transaction_balance", "TradePortalSignedDecimalAttribute", "com.ams.tradeportal.common");

      /* current_balance - This field represents the current account balance returned from the bank
         request.   */
      attributeMgr.registerAttribute("current_balance", "current_balance", "TradePortalSignedDecimalAttribute", "com.ams.tradeportal.common");

      /* last_retrieved_from_bank_date - This represents the date and time the account balance was updated in the
         portal. */
      attributeMgr.registerAttribute("last_retrieved_from_bank_date", "last_retrieved_from_bank_date", "DateTimeAttribute");

      /* interest_rate - This presents the user with the current interest rate on the account, if
         applicable. */
      attributeMgr.registerAttribute("interest_rate", "interest_rate", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");

      /* available_funds - Available funds in this account. */
      attributeMgr.registerAttribute("available_funds", "available_funds", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");

      /* status_code - Status code from the bank. */
      attributeMgr.registerAttribute("status_code", "status_code");

      /* deactivate_indicator - Whether the account is deactivated.  Defaults to not deactivated.

         Y=the account is deactivated
 */
      attributeMgr.registerAttribute("deactivate_indicator", "deactivate_indicator", "IndicatorAttribute");

      /* available_for_internatnl_pay - If selected this account will be available in the debit from account number
         dropdown list for selection for International Payment (which used to be
         called Funds Transfer). */
      attributeMgr.registerAttribute("available_for_internatnl_pay", "available_for_internatnl_pay", "IndicatorAttribute");

      /* available_for_xfer_btwn_accts - If selected this account will be available in the debit from account number
         dropdown list for selection for Transfer Between Accounts. */
      attributeMgr.registerAttribute("available_for_xfer_btwn_accts", "available_for_xfer_btwn_accts", "IndicatorAttribute");

      /* available_for_direct_debit - If selected this account will display as available in the drop-down list
         of account numbers when creating a Direct Deposit. */
      attributeMgr.registerAttribute("available_for_direct_debit", "available_for_direct_debit", "IndicatorAttribute");

      /* proponix_customer_id - The unique identifier of the corporate organization associated with the
         account.   This is used by TPS to match up corporate organizations on the
         portal with those stored in TPS. */
      attributeMgr.registerAttribute("proponix_customer_id", "proponix_customer_id");

      /* fx_rate_group - The FX Rate Group (as defined in TPS) for the corporate customer associated
         with the account.  It is a simple TextAttribute and is not validated against
         REFDATA. */
      attributeMgr.registerAttribute("fx_rate_group", "fx_rate_group");

        /* Pointer to the parent CorporateOrganization */
      attributeMgr.registerAttribute("owner_oid", "p_owner_oid", "ParentIDAttribute");

        /* Pointer to the parent Party */
      attributeMgr.registerAttribute("owner_oid", "p_owner_oid", "ParentIDAttribute");

      /* panel_auth_group_oid - An account could be associated with a Panel Authorization Group if this
         account is available for either International Payment or Domestic Payment
         or Transfer Between Accounts. */
      //Commenting below line for CR 821 - Rel 8.3
      //attributeMgr.registerAssociation("panel_auth_group_oid", "a_panel_auth_group_oid", "PanelAuthorizationGroup");

      /* op_bank_org_oid - The operational bank org that this account's is assigned to.  This should
         be the one of the corporation's associated Operational Bank Org. */
      attributeMgr.registerAssociation("op_bank_org_oid", "a_op_bank_org_oid", "OperationalBankOrganization");

		//Ravindra - Added as part of fixing cash management authorization issue - 16th Mar 2011 - Start
      /* pending_transaction_credit_balance - This field captures running pending movements for each account (to be subtracted/or
	           added to from retrieved balances). This field will be display only; It is
	           not editable. */
      attributeMgr.registerAttribute("pending_transac_credit_balance", "pending_transac_credit_balance", "TradePortalSignedDecimalAttribute", "com.ams.tradeportal.common");
      //Ravindra - Added as part of fixing cash management authorization issue - 16th Mar 2011 - End

      /* othercorp_customer_indicator - This is the indicator which tells the corporate customer whether
       * it belongs to its own Account or the subsidary Account. */
      attributeMgr.registerAttribute("othercorp_customer_indicator", "othercorp_customer_indicator");
     
      attributeMgr.registerAttribute("other_account_owner_oid", "other_account_owner_oid");
      //Maheswar CR-610 Begin 28th Feb 2011 End
      
      //CR 821 - Rel 8.3 START
      /* panel_auth_group_oid_1 to panel_auth_group_oid_16 - An account could be associated with multiple Panel Authorization Groups, up to 16, if this
      account is available for either International Payment or Domestic Payment
      or Transfer Between Accounts. */
      attributeMgr.registerAssociation("panel_auth_group_oid_1", "a_panel_auth_group_oid_1", "PanelAuthorizationGroup");
      attributeMgr.registerAssociation("panel_auth_group_oid_2", "a_panel_auth_group_oid_2", "PanelAuthorizationGroup");
      attributeMgr.registerAssociation("panel_auth_group_oid_3", "a_panel_auth_group_oid_3", "PanelAuthorizationGroup");
      attributeMgr.registerAssociation("panel_auth_group_oid_4", "a_panel_auth_group_oid_4", "PanelAuthorizationGroup");
      attributeMgr.registerAssociation("panel_auth_group_oid_5", "a_panel_auth_group_oid_5", "PanelAuthorizationGroup");
      attributeMgr.registerAssociation("panel_auth_group_oid_6", "a_panel_auth_group_oid_6", "PanelAuthorizationGroup");
      attributeMgr.registerAssociation("panel_auth_group_oid_7", "a_panel_auth_group_oid_7", "PanelAuthorizationGroup");
      attributeMgr.registerAssociation("panel_auth_group_oid_8", "a_panel_auth_group_oid_8", "PanelAuthorizationGroup");
      attributeMgr.registerAssociation("panel_auth_group_oid_9", "a_panel_auth_group_oid_9", "PanelAuthorizationGroup");
      attributeMgr.registerAssociation("panel_auth_group_oid_10", "a_panel_auth_group_oid_10", "PanelAuthorizationGroup");
      attributeMgr.registerAssociation("panel_auth_group_oid_11", "a_panel_auth_group_oid_11", "PanelAuthorizationGroup");
      attributeMgr.registerAssociation("panel_auth_group_oid_12", "a_panel_auth_group_oid_12", "PanelAuthorizationGroup");
      attributeMgr.registerAssociation("panel_auth_group_oid_13", "a_panel_auth_group_oid_13", "PanelAuthorizationGroup");
      attributeMgr.registerAssociation("panel_auth_group_oid_14", "a_panel_auth_group_oid_14", "PanelAuthorizationGroup");
      attributeMgr.registerAssociation("panel_auth_group_oid_15", "a_panel_auth_group_oid_15", "PanelAuthorizationGroup");
      attributeMgr.registerAssociation("panel_auth_group_oid_16", "a_panel_auth_group_oid_16", "PanelAuthorizationGroup");
      //CR 821 - Rel 8.3 END
      
    //SSikhakolli - Rel-9.4 CR-818 - adding new attribute
      attributeMgr.registerAttribute("available_for_settle_instr", "available_for_settle_instr", "IndicatorAttribute");
    //dpatra - Rel-9.5 CR1051  -  adding new attribute
      attributeMgr.registerReferenceAttribute("regulatory_account_type", "a_regulatory_account_type", "REGULATORY_ACCOUNT_TYPE");
   }

  /*
   * Register the components of the business object
   */
   protected void registerComponents() throws RemoteException, AmsException
   {
      /* Register the components defined in the Ancestor class */
      super.registerComponents();

      /* EODDailyDataList -  */
      registerOneToManyComponent("EODDailyDataList","EODDailyDataList");
   }
}