
  
/*
 * This file is generated from the model.  Normally it should not be modified manually.  
 * Contact the modeler first.  (In a pinch, it can be modified first and then retrofitted 
 * to the model.)
 *
 */
package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;


/*
 * Business Object to store aliases (alternate names/identifiers) to be used
 * as indexes on the Customer File depending on the specific Message Category
 * for the Incoming Generic Message
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class CustomerAliasBean_Base extends TradePortalBusinessObjectBean
{
private static final Logger LOG = LoggerFactory.getLogger(CustomerAliasBean_Base.class);

  
  /* 
   * Register the attributes and associations of the business object
   */
   protected void registerAttributes() throws AmsException
   {  

      /* Register attributes defined in the Ancestor class */
      super.registerAttributes();
      
      /* alias - Alias(alternate names/identifiers) to be used as indexes on the Customer
         File depending on the specific Message Category for the Incoming Generic
         Message */
      attributeMgr.registerAttribute("alias", "alias");
      
      /* customer_alias_oid - Unique Identifier */
      attributeMgr.registerAttribute("customer_alias_oid", "customer_alias_oid", "ObjectIDAttribute");
      
        /* Pointer to the parent CorporateOrganization */
      attributeMgr.registerAttribute("owner_oid", "p_owner_oid", "ParentIDAttribute");
   
      /* gen_message_category_oid -  */
      attributeMgr.registerAssociation("gen_message_category_oid", "a_gen_message_category_oid", "GenericMessageCategory");
      
   }
   
 
   
 
 
   
}
