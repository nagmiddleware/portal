 
package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import com.ams.tradeportal.busobj.util.*;

/*
 * 
 *
 *     Copyright  � 2006                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class AddressBean extends AddressBean_Base
{
private static final Logger LOG = LoggerFactory.getLogger(AddressBean.class);


   /**
   * Performs any special "validate" processing that is specific to the
   * descendant of BusinessObject.
   *
   * This method verifies that WorkGroup name is unique for the Client Bank and
   * all amounts have the correct precision
   *
   * @see #validate()
   */

   protected void userValidate() throws AmsException, RemoteException
   {
	/** 
	* rbhaduri - 1st August 06 - IR LEUG070641695 - Address should have unique sequence number
	* verify that the address sequence number is unique
	*/
	
	/*
	 * Rel 951 - IR T36000049101
	 * Commenting - As this is not allowing to modify and swap address sequence number 
	 * 
	 String addressSeqNum = this.getAttribute("address_seq_num");

	if (!isUnique("address_seq_num", addressSeqNum, 
		" and p_corp_org_oid = " + this.getAttribute("corp_org_oid")))
	{
		this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
			TradePortalConstants.ALREADY_EXISTS, addressSeqNum, attributeMgr.getAlias("address_seq_num"));
	}*/

   }

}










