
  
/*
 * This file is generated from the model.  Normally it should not be modified manually.  
 * Contact the modeler first.  (In a pinch, it can be modified first and then retrofitted 
 * to the model.)
 *
 */
package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;


/*
 * Supplier Portal Margin Rule for a Corporate Customer.  These are the margins
 * used to calculate the invoice offer's net amount offer.  See InvoicesSummaryDate.calculateSupplierPortalDiscount().
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class SPMarginRuleBean_Base extends TradePortalBusinessObjectBean
{
private static final Logger LOG = LoggerFactory.getLogger(SPMarginRuleBean_Base.class);

  
  /* 
   * Register the attributes and associations of the business object
   */
   protected void registerAttributes() throws AmsException
   {  

      /* Register attributes defined in the Ancestor class */
      super.registerAttributes();
      
      /* sp_margin_rule_oid - Unique identifier */
      attributeMgr.registerAttribute("sp_margin_rule_oid", "sp_margin_rule_oid", "ObjectIDAttribute");
      
      /* currency - A matching crierion.  Matches the currency of the invoice. */
      attributeMgr.registerReferenceAttribute("currency", "currency", "CURRENCY_CODE");
      
      /* threshold_amt - A matching criterian.  Matches the invoice whose amount is less than this
         threshold amount. */
      attributeMgr.registerAttribute("threshold_amt", "threshold_amt", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      
      /* rate_type - Matching result.  Used to get interest_discount_rate by matching its rate_type. */
      attributeMgr.registerReferenceAttribute("rate_type", "rate_type", "REFINANCE_RATE_TYPE");
      
      /* margin - Matching result.  The margin over the interest discount rate. */
      attributeMgr.registerAttribute("margin", "margin", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      
      /* Pointer to the parent CorporateOrganization */
       attributeMgr.registerAttribute("owner_oid", "p_owner_oid", "ParentIDAttribute");
      
   }
   
 
   
 
 
   
}
