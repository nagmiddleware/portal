/**
 * 
 */
package com.ams.tradeportal.busobj;

/**
 * Supplier Portal Email Notifications for a Corporate Customer.  The time to send next email for the supplier is
 * calculated based on his input interval and stored in this table 
 * 
 * 
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 *
 */
public interface SPEmailNotifications extends TradePortalBusinessObject
{

}
