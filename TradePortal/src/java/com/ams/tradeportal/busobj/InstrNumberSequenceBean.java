 
package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;




/*
 * 
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class InstrNumberSequenceBean extends InstrNumberSequenceBean_Base
{
private static final Logger LOG = LoggerFactory.getLogger(InstrNumberSequenceBean.class);
  /**
   * 
   * 
   * @return long  
   */
   public long getNextInstrumentNumber() throws RemoteException, AmsException
   {
        // This method always creates its own transaction
        // That way, the instrument number will be gotten as quickly as possible,
        // reducing the chance of locking issues
    
        // Get the current last number used
        long instrNumber = this.getAttributeLong("last_instrument_id_used");
        
        // Increment id
        instrNumber++;
        
        // Set the attribute and save
        this.setAttribute("last_instrument_id_used", String.valueOf(instrNumber));
                
        this.save();

        return instrNumber;
   } 

}
