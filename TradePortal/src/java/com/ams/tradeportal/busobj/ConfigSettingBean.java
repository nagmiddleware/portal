package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.RemoteException;

import com.ams.tradeportal.common.TradePortalConstants;
import com.ams.tradeportal.common.cache.Cache;
import com.ams.tradeportal.common.cache.TPCacheManager;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.util.DocumentHandler;

public class ConfigSettingBean extends ConfigSettingBean_Base {
private static final Logger LOG = LoggerFactory.getLogger(ConfigSettingBean.class);

	
	  /**
	   * populate the business object from cache
	   *
	   * @param applicationName 
	   * @param className
	   * @param settingName
	   * 
	   */
	public void getDataFromCache(String applicationName, String className, String settingName) throws AmsException
	  {
		StringBuffer sqlQuery = new StringBuffer();
        sqlQuery.append("select * from config_setting where application_name = '")
                 .append(applicationName)
                 .append("' and class_name = '")
                 .append(className)
                 .append("' and setting_name = '")
                 .append(settingName)
                 .append("'");		
        String sql = sqlQuery.toString();

		  Cache cache = (Cache)TPCacheManager.getInstance().getCache(TradePortalConstants.CONFIG_SETTING_CACHE);
		  DocumentHandler result = (DocumentHandler)cache.get(sql);
	      if (result != null) {
	    	  populateDataFromResultSet(result);
	      }

	  }
	
}
