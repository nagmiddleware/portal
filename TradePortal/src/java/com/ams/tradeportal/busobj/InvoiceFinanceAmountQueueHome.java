
  

package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;
/**
 * The Invoice Files that are to be uploaded.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface InvoiceFinanceAmountQueueHome extends EJBHome
{
   public InvoiceFinanceAmountQueue create()
      throws RemoteException, CreateException, AmsException;

   public InvoiceFinanceAmountQueue create(ClientServerDataBridge csdb)
      throws RemoteException, CreateException, AmsException;
}
