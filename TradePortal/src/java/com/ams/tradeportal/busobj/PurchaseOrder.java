

  

package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;


/**
 * Represents a purchase order that has been uploaded into the Trade Portal.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface PurchaseOrder extends TradePortalBusinessObject
{   
public int deletePO(String  userOid) throws java.rmi.RemoteException, com.amsinc.ecsg.frame.AmsException;
}
