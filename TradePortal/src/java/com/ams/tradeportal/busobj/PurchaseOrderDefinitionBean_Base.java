
  
/*
 * This file is generated from the model.  Normally it should not be modified manually.  
 * Contact the modeler first.  (In a pinch, it can be modified first and then retrofitted 
 * to the model.)
 *
 */
package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;


/*
 * Describes the mapping of an new structured uploaded file to purchase order
 * line items stored in the database.   The fields contained in the file are
 * described, the file format is specified, and the goods description format
 * is provided.


 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class PurchaseOrderDefinitionBean_Base extends TradePortalBusinessObjectBean
{
private static final Logger LOG = LoggerFactory.getLogger(PurchaseOrderDefinitionBean_Base.class);

  
  /* 
   * Register the attributes and associations of the business object
   */
   protected void registerAttributes() throws AmsException
   {  

      /* Register attributes defined in the Ancestor class */
      super.registerAttributes();
      
      /* purchase_order_definition_oid - Unique identifier */
      attributeMgr.registerAttribute("purchase_order_definition_oid", "purchase_order_definition_oid", "ObjectIDAttribute");
      
      /* name - Name of the Purchase Order Definition.  This will be used in dropdown lists
         when choosing a definition. */
      attributeMgr.registerAttribute("name", "name");
      attributeMgr.requiredAttribute("name");
      attributeMgr.registerAlias("name", getResourceManager().getText("POStructureDefinition.Name", TradePortalConstants.TEXT_BUNDLE));
      
      /* description - Description of the definition for convenience. */
      attributeMgr.registerAttribute("description", "description");
      attributeMgr.requiredAttribute("description");
      attributeMgr.registerAlias("description", getResourceManager().getText("POStructureDefinition.Description", TradePortalConstants.TEXT_BUNDLE));
      
      /* delimiter_char - Only relevant if using a delimited file format.   This attribute indicates
         what character to use as the delimiter between fields in the uploaded file. */
      attributeMgr.registerReferenceAttribute("delimiter_char", "delimiter_char", "PO_DELIMITER_CHAR");
      attributeMgr.requiredAttribute("delimiter_char");
      attributeMgr.registerAlias("delimiter_char", getResourceManager().getText("POStructureDefinition.DelimiterChar", TradePortalConstants.TEXT_BUNDLE));

      /* date_format - Indicates which date format to use. */
      attributeMgr.registerReferenceAttribute("date_format", "date_format", "SPO_DATE_FORMAT");
      attributeMgr.requiredAttribute("date_format");
      attributeMgr.registerAlias("date_format", getResourceManager().getText("POStructureDefinition.DateFormat", TradePortalConstants.TEXT_BUNDLE));

      /* default_flag - Set to Yes if this is the default Purchase Order definition for the corporate
         organization. */
      attributeMgr.registerAttribute("default_flag", "default_flag", "IndicatorAttribute");
      
      /* opt_lock - Optimistic lock attribute
         See jPylon documentation for details on how this works */
      attributeMgr.registerAttribute("opt_lock", "opt_lock", "OptimisticLockAttribute");
      
      /* incoterm_req - Field representing the Incoterm associated with an underlying shipment.
         Set to Yes if this is required field. */
      attributeMgr.registerAttribute("incoterm_req", "incoterm_req", "IndicatorAttribute");
      
      /* incoterm_loc_req - Field representing the Incoterm location associated with an underlying shipment.
         Set to Yes if this is required field. */
      attributeMgr.registerAttribute("incoterm_loc_req", "incoterm_loc_req", "IndicatorAttribute");
      
      /* partial_ship_allowed_req - Set to Yes if this is required field. */
      attributeMgr.registerAttribute("partial_ship_allowed_req", "partial_ship_allowed_req", "IndicatorAttribute");
      
      /* goods_desc_req - Set to Yes if this is required field. */
      attributeMgr.registerAttribute("goods_desc_req", "goods_desc_req", "IndicatorAttribute");
      
      /* buyer_users_def1_label - The user-defined label for the buyer user defined field. */
      attributeMgr.registerAttribute("buyer_users_def1_label", "buyer_users_def1_label");
      
      /* buyer_users_def2_label - The user-defined label for the buyer user defined field. */
      attributeMgr.registerAttribute("buyer_users_def2_label", "buyer_users_def2_label");
      
      /* buyer_users_def3_label - The user-defined label for the buyer user defined field. */
      attributeMgr.registerAttribute("buyer_users_def3_label", "buyer_users_def3_label");
      
      /* buyer_users_def4_label - The user-defined label for the buyer user defined field. */
      attributeMgr.registerAttribute("buyer_users_def4_label", "buyer_users_def4_label");
      
      /* buyer_users_def5_label - The user-defined label for the buyer user defined field. */
      attributeMgr.registerAttribute("buyer_users_def5_label", "buyer_users_def5_label");
      
      /* buyer_users_def6_label - The user-defined label for the buyer user defined field. */
      attributeMgr.registerAttribute("buyer_users_def6_label", "buyer_users_def6_label");
      
      /* buyer_users_def7_label - The user-defined label for the buyer user defined field. */
      attributeMgr.registerAttribute("buyer_users_def7_label", "buyer_users_def7_label");
      
      /* buyer_users_def8_label - The user-defined label for the buyer user defined field. */
      attributeMgr.registerAttribute("buyer_users_def8_label", "buyer_users_def8_label");
      
      /* buyer_users_def9_label - The user-defined label for the buyer user defined field. */
      attributeMgr.registerAttribute("buyer_users_def9_label", "buyer_users_def9_label");
      
      /* buyer_users_def10_label - The user-defined label for the buyer user defined field. */
      attributeMgr.registerAttribute("buyer_users_def10_label", "buyer_users_def10_label");
      
      /* seller_users_def1_label - The user-defined label for the seller user defined field. */
      attributeMgr.registerAttribute("seller_users_def1_label", "seller_users_def1_label");
      
      /* seller_users_def2_label - The user-defined label for the seller user defined field. */
      attributeMgr.registerAttribute("seller_users_def2_label", "seller_users_def2_label");
      
      /* seller_users_def3_label - The user-defined label for the seller user defined field. */
      attributeMgr.registerAttribute("seller_users_def3_label", "seller_users_def3_label");
      
      /* seller_users_def4_label - The user-defined label for the seller user defined field. */
      attributeMgr.registerAttribute("seller_users_def4_label", "seller_users_def4_label");
      
      /* seller_users_def5_label - The user-defined label for the seller user defined field. */
      attributeMgr.registerAttribute("seller_users_def5_label", "seller_users_def5_label");
      
      /* seller_users_def6_label - The user-defined label for the seller user defined field. */
      attributeMgr.registerAttribute("seller_users_def6_label", "seller_users_def6_label");
      
      /* seller_users_def7_label - The user-defined label for the seller user defined field. */
      attributeMgr.registerAttribute("seller_users_def7_label", "seller_users_def7_label");
      
      /* seller_users_def8_label - The user-defined label for the seller user defined field. */
      attributeMgr.registerAttribute("seller_users_def8_label", "seller_users_def8_label");
      
      /* seller_users_def9_label - The user-defined label for the seller user defined field. */
      attributeMgr.registerAttribute("seller_users_def9_label", "seller_users_def9_label");
      
      /* seller_users_def10_label - The user-defined label for the seller user defined field. */
      attributeMgr.registerAttribute("seller_users_def10_label", "seller_users_def10_label");
      
      /* buyer_users_def1_req - Set to Yes if this is required field. */
      attributeMgr.registerAttribute("buyer_users_def1_req", "buyer_users_def1_req", "IndicatorAttribute");
      
      /* buyer_users_def2_req - Set to Yes if this is required field. */
      attributeMgr.registerAttribute("buyer_users_def2_req", "buyer_users_def2_req", "IndicatorAttribute");
      
      /* buyer_users_def3_req - Set to Yes if this is required field. */
      attributeMgr.registerAttribute("buyer_users_def3_req", "buyer_users_def3_req", "IndicatorAttribute");
      
      /* buyer_users_def4_req - Set to Yes if this is required field. */
      attributeMgr.registerAttribute("buyer_users_def4_req", "buyer_users_def4_req", "IndicatorAttribute");
      
      /* buyer_users_def5_req - Set to Yes if this is required field. */
      attributeMgr.registerAttribute("buyer_users_def5_req", "buyer_users_def5_req", "IndicatorAttribute");
      
      /* buyer_users_def6_req - Set to Yes if this is required field. */
      attributeMgr.registerAttribute("buyer_users_def6_req", "buyer_users_def6_req", "IndicatorAttribute");
      
      /* buyer_users_def7_req - Set to Yes if this is required field. */
      attributeMgr.registerAttribute("buyer_users_def7_req", "buyer_users_def7_req", "IndicatorAttribute");
      
      /* buyer_users_def8_req - Set to Yes if this is required field. */
      attributeMgr.registerAttribute("buyer_users_def8_req", "buyer_users_def8_req", "IndicatorAttribute");
      
      /* buyer_users_def9_req - Set to Yes if this is required field. */
      attributeMgr.registerAttribute("buyer_users_def9_req", "buyer_users_def9_req", "IndicatorAttribute");
      
      /* buyer_users_def10_req - Set to Yes if this is required field. */
      attributeMgr.registerAttribute("buyer_users_def10_req", "buyer_users_def10_req", "IndicatorAttribute");
      
      /* seller_users_def1_req - Set to Yes if this is required field. */
      attributeMgr.registerAttribute("seller_users_def1_req", "seller_users_def1_req", "IndicatorAttribute");
      
      /* seller_users_def2_req - Set to Yes if this is required field. */
      attributeMgr.registerAttribute("seller_users_def2_req", "seller_users_def2_req", "IndicatorAttribute");
      
      /* seller_users_def3_req - Set to Yes if this is required field. */
      attributeMgr.registerAttribute("seller_users_def3_req", "seller_users_def3_req", "IndicatorAttribute");
      
      /* seller_users_def4_req - Set to Yes if this is required field. */
      attributeMgr.registerAttribute("seller_users_def4_req", "seller_users_def4_req", "IndicatorAttribute");
      
      /* seller_users_def5_req - Set to Yes if this is required field. */
      attributeMgr.registerAttribute("seller_users_def5_req", "seller_users_def5_req", "IndicatorAttribute");
      
      /* seller_users_def6_req - Set to Yes if this is required field. */
      attributeMgr.registerAttribute("seller_users_def6_req", "seller_users_def6_req", "IndicatorAttribute");
      
      /* seller_users_def7_req - Set to Yes if this is required field. */
      attributeMgr.registerAttribute("seller_users_def7_req", "seller_users_def7_req", "IndicatorAttribute");
      
      /* seller_users_def8_req - Set to Yes if this is required field. */
      attributeMgr.registerAttribute("seller_users_def8_req", "seller_users_def8_req", "IndicatorAttribute");
      
      /* seller_users_def9_req - Set to Yes if this is required field. */
      attributeMgr.registerAttribute("seller_users_def9_req", "seller_users_def9_req", "IndicatorAttribute");
      
      /* seller_users_def10_req - Set to Yes if this is required field. */
      attributeMgr.registerAttribute("seller_users_def10_req", "seller_users_def10_req", "IndicatorAttribute");
      
      /* line_item_detail_provided - Indicates whether the definition will have line items details. If this indicator
         is not selected, then none of the  Line Item Detail field names can be selected. */
      attributeMgr.registerAttribute("line_item_detail_provided", "line_item_detail_provided", "IndicatorAttribute");
      
      /* line_item_num_req - Set to Yes if this is required field. */
      attributeMgr.registerAttribute("line_item_num_req", "line_item_num_req", "IndicatorAttribute");
      
      /* unit_price_req - Set to Yes if this is required field. */
      attributeMgr.registerAttribute("unit_price_req", "unit_price_req", "IndicatorAttribute");
      
      /* unit_of_measure_req - Set to Yes if this is required field. */
      attributeMgr.registerAttribute("unit_of_measure_req", "unit_of_measure_req", "IndicatorAttribute");
      
      /* quantity_req - Set to Yes if this is required field. */
      attributeMgr.registerAttribute("quantity_req", "quantity_req", "IndicatorAttribute");
      
      /* quantity_var_plus_req - Set to Yes if this is required field. */
      attributeMgr.registerAttribute("quantity_var_plus_req", "quantity_var_plus_req", "IndicatorAttribute");
      
      /* prod_chars_ud1_req - Set to Yes if this is required field. */
      attributeMgr.registerAttribute("prod_chars_ud1_req", "prod_chars_ud1_req", "IndicatorAttribute");
      
      /* prod_chars_ud2_req - Set to Yes if this is required field. */
      attributeMgr.registerAttribute("prod_chars_ud2_req", "prod_chars_ud2_req", "IndicatorAttribute");
      
      /* prod_chars_ud3_req - Set to Yes if this is required field. */
      attributeMgr.registerAttribute("prod_chars_ud3_req", "prod_chars_ud3_req", "IndicatorAttribute");
      
      /* prod_chars_ud4_req - Set to Yes if this is required field. */
      attributeMgr.registerAttribute("prod_chars_ud4_req", "prod_chars_ud4_req", "IndicatorAttribute");
      
      /* prod_chars_ud5_req - Set to Yes if this is required field. */
      attributeMgr.registerAttribute("prod_chars_ud5_req", "prod_chars_ud5_req", "IndicatorAttribute");
      
      /* prod_chars_ud6_req - Set to Yes if this is required field. */
      attributeMgr.registerAttribute("prod_chars_ud6_req", "prod_chars_ud6_req", "IndicatorAttribute");
      
      /* prod_chars_ud7_req - Set to Yes if this is required field. */
      attributeMgr.registerAttribute("prod_chars_ud7_req", "prod_chars_ud7_req", "IndicatorAttribute");
      
      /* prod_chars_ud1_label - The user-defined label for the product characteristic field. */
      attributeMgr.registerAttribute("prod_chars_ud1_label", "prod_chars_ud1_label");
      
      /* prod_chars_ud2_label - The user-defined label for the product characteristic field. */
      attributeMgr.registerAttribute("prod_chars_ud2_label", "prod_chars_ud2_label");
      
      /* prod_chars_ud3_label - The user-defined label for the product characteristic field. */
      attributeMgr.registerAttribute("prod_chars_ud3_label", "prod_chars_ud3_label");
      
      /* prod_chars_ud4_label - The user-defined label for the product characteristic field. */
      attributeMgr.registerAttribute("prod_chars_ud4_label", "prod_chars_ud4_label");
      
      /* prod_chars_ud5_label - The user-defined label for the product characteristic field. */
      attributeMgr.registerAttribute("prod_chars_ud5_label", "prod_chars_ud5_label");
      
      /* prod_chars_ud6_label - The user-defined label for the product characteristic field. */
      attributeMgr.registerAttribute("prod_chars_ud6_label", "prod_chars_ud6_label");
      
      /* prod_chars_ud7_label - The user-defined label for the product characteristic field. */
      attributeMgr.registerAttribute("prod_chars_ud7_label", "prod_chars_ud7_label");
      
      /* po_data_field1 - The Purchase Order data fields. */
      attributeMgr.registerAttribute("po_data_field1", "po_data_field1");
      
      /* po_data_field2 - The Purchase Order data fields. */
      attributeMgr.registerAttribute("po_data_field2", "po_data_field2");
      
      /* po_data_field3 - The Purchase Order data fields. */
      attributeMgr.registerAttribute("po_data_field3", "po_data_field3");
      
      /* po_data_field4 - The Purchase Order data fields. */
      attributeMgr.registerAttribute("po_data_field4", "po_data_field4");
      
      /* po_data_field5 - The Purchase Order data fields. */
      attributeMgr.registerAttribute("po_data_field5", "po_data_field5");
      
      /* po_data_field6 - The Purchase Order data fields. */
      attributeMgr.registerAttribute("po_data_field6", "po_data_field6");
      
      /* po_data_field7 - The Purchase Order data fields. */
      attributeMgr.registerAttribute("po_data_field7", "po_data_field7");
      
      /* po_line_item_field1 - The Purchase Order line item data fields. */
      attributeMgr.registerAttribute("po_line_item_field1", "po_line_item_field1");
      
      /* po_line_item_field2 - The Purchase Order line item data fields. */
      attributeMgr.registerAttribute("po_line_item_field2", "po_line_item_field2");
      
      /* po_line_item_field3 - The Purchase Order line item data fields. */
      attributeMgr.registerAttribute("po_line_item_field3", "po_line_item_field3");
      
      /* po_line_item_field4 - The Purchase Order line item data fields. */
      attributeMgr.registerAttribute("po_line_item_field4", "po_line_item_field4");
      
      /* po_line_item_field5 - The Purchase Order line item data fields. */
      attributeMgr.registerAttribute("po_line_item_field5", "po_line_item_field5");
      
      /* po_line_item_field6 - The Purchase Order line item data fields. */
      attributeMgr.registerAttribute("po_line_item_field6", "po_line_item_field6");
      
      /* po_line_item_field7 - The Purchase Order line item data fields. */
      attributeMgr.registerAttribute("po_line_item_field7", "po_line_item_field7");
      
      /* po_line_item_field8 - The Purchase Order line item data fields. */
      attributeMgr.registerAttribute("po_line_item_field8", "po_line_item_field8");
      
      /* po_line_item_field9 - The Purchase Order line item data fields. */
      attributeMgr.registerAttribute("po_line_item_field9", "po_line_item_field9");
      
      /* po_line_item_field10 - The Purchase Order line item data fields. */
      attributeMgr.registerAttribute("po_line_item_field10", "po_line_item_field10");
      
      /* po_line_item_field11 - The Purchase Order line item data fields. */
      attributeMgr.registerAttribute("po_line_item_field11", "po_line_item_field11");
      
      /* po_line_item_field12 - The Purchase Order line item data fields. */
      attributeMgr.registerAttribute("po_line_item_field12", "po_line_item_field12");
      
      /* po_summary_field1 - The Purchase Order line item data fields. */
      attributeMgr.registerAttribute("po_summary_field1", "po_summary_field1");
      
      /* po_summary_field2 - The Purchase Order line item data fields. */
      attributeMgr.registerAttribute("po_summary_field2", "po_summary_field2");
      
      /* po_summary_field3 - The Purchase Order line item data fields. */
      attributeMgr.registerAttribute("po_summary_field3", "po_summary_field3");
      
      /* po_summary_field4 - The Purchase Order line item data fields. */
      attributeMgr.registerAttribute("po_summary_field4", "po_summary_field4");
      
      /* po_summary_field5 - The Purchase Order line item data fields. */
      attributeMgr.registerAttribute("po_summary_field5", "po_summary_field5");
      
      /* po_summary_field6 - The Purchase Order line item data fields. */
      attributeMgr.registerAttribute("po_summary_field6", "po_summary_field6");
      
      /* po_summary_field7 - The Purchase Order line item data fields. */
      attributeMgr.registerAttribute("po_summary_field7", "po_summary_field7");
      
      /* po_summary_field8 - The Purchase Order line item data fields. */
      attributeMgr.registerAttribute("po_summary_field8", "po_summary_field8");
      
      /* po_summary_field9 - The Purchase Order line item data fields. */
      attributeMgr.registerAttribute("po_summary_field9", "po_summary_field9");
      
      /* po_summary_field10 - The Purchase Order line item data fields. */
      attributeMgr.registerAttribute("po_summary_field10", "po_summary_field10");
      
      /* po_summary_field11 - The Purchase Order line item data fields. */
      attributeMgr.registerAttribute("po_summary_field11", "po_summary_field11");
      
      /* po_summary_field12 - The Purchase Order line item data fields. */
      attributeMgr.registerAttribute("po_summary_field12", "po_summary_field12");
      
      /* po_summary_field13 - The Purchase Order line item data fields. */
      attributeMgr.registerAttribute("po_summary_field13", "po_summary_field13");
      
      /* po_summary_field14 - The Purchase Order line item data fields. */
      attributeMgr.registerAttribute("po_summary_field14", "po_summary_field14");
      
      /* po_summary_field15 - The Purchase Order line item data fields. */
      attributeMgr.registerAttribute("po_summary_field15", "po_summary_field15");
      
      /* po_summary_field16 - The Purchase Order line item data fields. */
      attributeMgr.registerAttribute("po_summary_field16", "po_summary_field16");
      
      /* po_summary_field17 - The Purchase Order line item data fields. */
      attributeMgr.registerAttribute("po_summary_field17", "po_summary_field17");
      
      /* po_summary_field18 - The Purchase Order line item data fields. */
      attributeMgr.registerAttribute("po_summary_field18", "po_summary_field18");
      
      /* po_summary_field19 - The Purchase Order line item data fields. */
      attributeMgr.registerAttribute("po_summary_field19", "po_summary_field19");
      
      /* po_summary_field20 - The Purchase Order line item data fields. */
      attributeMgr.registerAttribute("po_summary_field20", "po_summary_field20");
      
      /* po_summary_field21 - The Purchase Order line item data fields. */
      attributeMgr.registerAttribute("po_summary_field21", "po_summary_field21");
      
      /* po_summary_field22 - The Purchase Order line item data fields. */
      attributeMgr.registerAttribute("po_summary_field22", "po_summary_field22");
      
      /* po_summary_field23 - The Purchase Order line item data fields. */
      attributeMgr.registerAttribute("po_summary_field23", "po_summary_field23");
      
      /* po_summary_field24 - The Purchase Order line item data fields. */
      attributeMgr.registerAttribute("po_summary_field24", "po_summary_field24");
		//Srinivasu_D Rel8.0 IR# BTUM032048858 04/02/2012 Start
	  /* po_summary_field25 - The Purchase Order line item data fields. */
      attributeMgr.registerAttribute("po_summary_field25", "po_summary_field25");
      //Srinivasu_D Rel8.0 IR# BTUM032048858 04/02/2012 End
      /* owner_org_oid - A Purchase Order definition is "owned" by an organization.  This component
         relationship represents the ownership. */
      attributeMgr.registerAssociation("owner_org_oid", "a_owner_org_oid", "CorporateOrganization");

	  /* part_to_validate - This is a  local attribute, meaning that it is not stored in the database.
	         It stores the tab that the user is editing on the PO Upload Definition detail
	         page.   Based on what tab is being edited, different validations occur. */
	      attributeMgr.registerAttribute("part_to_validate", "part_to_validate", "LocalAttribute");
      
	  //Leelavathi CR-707 Rel-800 07/03/2012 Begin
      /* include_column_headers - Indicates whether Purchase Order file will have column headings. */
      attributeMgr.registerAttribute("include_column_headers", "include_column_headers", "IndicatorAttribute");
      /* latest_shipment_date_req - Set to Yes if this is required field. */
      attributeMgr.registerAttribute("latest_shipment_date_req", "latest_shipment_date_req", "IndicatorAttribute");
      //Leelavathi CR-707 Rel-800 07/03/2012 End
	   //Srinivasu_D Rel8.0 IR# RBUM032655407 03/29/2012  Start
	  //count of summary fields
		attributeMgr.registerAttribute("summary_count", "summary_count", "LocalAttribute");
		//count of pol ine item fields
		attributeMgr.registerAttribute("po_line_item_count", "po_line_item_count", "LocalAttribute");
		 //Srinivasu_D Rel8.0 IR# RBUM032655407 03/29/2012  End
   }
   
 
   
 
 
   
}
