

/*
 * This file is generated from the model.  Normally it should not be modified manually.
 * Contact the modeler first.  (In a pinch, it can be modified first and then retrofitted
 * to the model.)
 *
 */
package com.ams.tradeportal.busobj;
import java.io.IOException;
import java.rmi.RemoteException;

import javax.ejb.RemoveException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsException;


/*
 * The Invoice Files that are to be uploaded.
 *
 *     Copyright  � 2003
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class InvoiceFileUploadBean extends InvoiceFileUploadBean_Base
{
private static final Logger LOG = LoggerFactory.getLogger(InvoiceFileUploadBean.class);

 	public void deleteInvoiceFileUpload(java.lang.String invoiceFileUploadOid) throws RemoteException, AmsException
 	{
		InvoiceFileUpload upload = (InvoiceFileUpload) this.createServerEJB("InvoiceFileUpload");
		upload.getData(Long.parseLong(invoiceFileUploadOid));
		upload.setAttribute("deleted_ind", TradePortalConstants.INDICATOR_YES);
		
		upload.save();
	}
 	
 	/**
	 * Sets Validation status to Rejected Status. Also, set status of Instrument, Transaction accordingly.
	 *
	 *
	 * @param  String Error message
	 * @throws AmsException
	 * @throws RemoveException 
	 * @throws IOException 
	 */
	public void rejectFileUplaod(boolean setDefaultErrorMsg) throws RemoteException, AmsException {
	
		//setAttribute("validation_status", "Failed");//BSL IR NNUM040229447 04/02/2012 - should be reference attribute
		setAttribute("validation_status", TradePortalConstants.PYMT_UPLOAD_VALIDATION_FAILED);
		if (save() < 0) {
			throw new AmsException("error saving InvoiceFileUpload ....: InvoiceFileUpload OID = " + getAttribute("invoice_file_upload_oid"));
		}
	}
	
	
	
}
