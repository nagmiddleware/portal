
  

package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;

/**
 * Bank organization groups to which an announcement applies.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface AnnouncementBankGroupHome extends EJBHome
{
   public AnnouncementBankGroup create()
      throws RemoteException, CreateException, AmsException;

   public AnnouncementBankGroup create(ClientServerDataBridge csdb)
      throws RemoteException, CreateException, AmsException;
}
