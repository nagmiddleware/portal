
  

package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;
/**
 * 
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface CustomerMarginRuleHome extends EJBHome
{
   public CustomerMarginRule create()
      throws RemoteException, CreateException, AmsException;

   public CustomerMarginRule create(ClientServerDataBridge csdb)
      throws RemoteException, CreateException, AmsException;
}
