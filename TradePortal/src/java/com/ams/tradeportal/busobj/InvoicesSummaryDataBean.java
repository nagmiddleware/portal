package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.math.MathContext;
import java.rmi.RemoteException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.ams.tradeportal.busobj.util.InstrumentServices;
import com.ams.tradeportal.busobj.util.InvoiceUtility;
import com.ams.tradeportal.busobj.webbean.CorporateOrganizationWebBean;
import com.ams.tradeportal.common.InstrumentType;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.frame.IssuedError;
import com.amsinc.ecsg.frame.ReferenceDataManager;
import com.amsinc.ecsg.util.DateTimeUtility;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.GMTUtility;
import com.amsinc.ecsg.util.StringFunction;
/*
 *
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
import com.amsinc.ecsg.web.BeanManager;

public class InvoicesSummaryDataBean extends InvoicesSummaryDataBean_Base {
private static final Logger LOG = LoggerFactory.getLogger(InvoicesSummaryDataBean.class);

	/** The attribute paths in InvoicesSummaryData used for grouping, except
	 *  for payment_date and due_date, which have special grouping rules */
	protected static final String[] ATTR_PATHS = {"corp_org_oid",
		"tp_rule_name",           "currency", "linked_to_instrument_type",
		"invoice_status","loan_type","invoice_classification"};
	
	/** The corresponding attribute paths in InvoiceGroup used for grouping */
	protected static final String[] GROUP_ATTR_PATHS = {"corp_org_oid",
		"trading_partner_name", "currency", "linked_to_instrument_type",
		"invoice_status","loan_type","invoice_classification"};

	/** The corresponding DB columns in INVOICE_GROUP used for grouping */
	protected static final String[] GROUP_ATTR_COLUMNS = {"a_corp_org_oid",
		"trading_partner_name", "currency", "linked_to_instrument_type",
		"invoice_status","loan_type","invoice_classification"};
	
	//CR 913 start - For Payables Prgm we need to group by send to supplier date along with other fields
	protected static final String[] ATTR_PAY_PGRM_PATHS = {"corp_org_oid",
		"tp_rule_name","currency", "linked_to_instrument_type",
		"invoice_status","invoice_classification", "end_to_end_id", "send_to_supplier_date"};
	
	protected static final String[] ATTR_PAY_E2EID_PGRM_PATHS = {"corp_org_oid",
		"tp_rule_name", "currency", "linked_to_instrument_type",
		"invoice_status","invoice_classification", "end_to_end_id"}; 
	
	/** The corresponding attribute paths in InvoiceGroup used for grouping */
	protected static final String[] GROUP_ATTR_PAY_PGRM_PATHS = {"corp_org_oid",
		"trading_partner_name", "currency", "linked_to_instrument_type",
		"invoice_status","invoice_classification",  "end_to_end_id", "send_to_supplier_date"};

	/** The corresponding DB columns in INVOICE_GROUP used for grouping- Do not use supplier date here*/
	protected static final String[] GROUP_ATTR_PAY_PGRM_COLUMNS = {"a_corp_org_oid",
		"trading_partner_name", "currency", "linked_to_instrument_type",
		"invoice_status","invoice_classification", "end_to_end_id"}; 
		
	//CR 913 end
	
	//BSL CR710 02/01/2012 Rel8.0 BEGIN
	/**
	 * Determines if this invoice is actually a credit note.
	 * 
	 * @return true if amount is negative; otherwise, false
	 * @throws AmsException
	 * @throws RemoteException
	 */
	public boolean isCreditNote() throws AmsException, RemoteException {
		String creditNote = getAttribute("credit_note");
		BigDecimal amount = getAttributeDecimal("amount");
		if (StringFunction.isNotBlank(creditNote) && amount != null && amount.signum() != -1){
			amount = amount.negate();
		}
		
		return amount != null && amount.signum() == -1;
	}

	/**
	 * Determines if this invoice has an instrument type assigned.
	 * 
	 * @return true if instrument type is populated
	 * @throws AmsException
	 * @throws RemoteException
	 */
	public boolean hasInstrumentType() throws AmsException, RemoteException {
		return StringFunction.isNotBlank(getAttribute("linked_to_instrument_type"));
	}

	/**
	 * Validates the instrument type.  An AmsException is thrown with the
	 * appropriate error code if the instrument type is missing or invalid.
	 *
	 * @throws AmsException
	 * @throws RemoteException
	 */
	public void validateInstrumentType() throws AmsException, RemoteException {
		if (!hasInstrumentType()) {
			IssuedError is = new IssuedError();
			is.setErrorCode(TradePortalConstants.ERR_UPLOAD_INV_INSTR_TYPE_MISSING);
			throw new AmsException(is);
		}
	}

	/**
	 * Add/update the InvoiceGroup as necessary
	 */
	protected void preSave() throws AmsException {
		super.preSave();
		
		
		try {
			// NAR IR-NLUM050246930 rel 8.0 Begin
			// apply payment date or other user action can make invoice status as Unable to finance. if invoice status is unable to
			// finance, then all calculated amount(invoice finance amount, net invoice finance amount, and estimated interest amount) of 
			// invoice should be set to null same as when perform 'Remove for Financing' action. 
			verifyInvoiceStatus();
           // NAR IR-NLUM050246930 rel 8.0 End
			if(StringFunction.isBlank(getAttribute("tp_rule_name"))){
				setAttribute("tp_rule_name", getTpRuleName());
			}
			//BSL CR710 03/19/2012 Rel8.0 BEGIN
			// Always verify invoice group, even if the corp org has disabled
			// grouping.  If grouping is enabled later on, previously uploaded
			// invoices can still be accessed via the Invoice Groups page.
			if(!"Y".equals(getAttribute("bypass_grouping"))){
			verifyInvoiceGroup();
			}
			//BSL CR710 03/19/2012 Rel8.0 END
		}
		catch (RemoteException re) {
			throw new AmsException(re);
		}
	}
	
	public void verifyInvoiceGroup() throws AmsException, RemoteException {
		if (!hasMatchingInvoiceGroup()) {
			String invoiceGroupOid = findMatchingInvoiceGroup();
			if (StringFunction.isBlank(invoiceGroupOid)) {
				invoiceGroupOid = createInvoiceGroup();
			}
			// IR 18421 Rel8.3 09/17/2013 starts
			else{
				updateInvoiceGroup(invoiceGroupOid,false);
			}
			// IR 18421 Rel8.3 09/17/2013 ends
			setAttribute("invoice_group_oid", invoiceGroupOid);
			
		}
	}
	// IR 18421 Rel8.3 09/17/2013 starts
	/**
	 * update the viewPDF(attachment_ind) indicator of InvoiceGroup as necessary
	 */
	public void updateInvoiceGroup(String invoiceGroupOid,boolean isIgnoreCurrentInvoice) throws AmsException, RemoteException {
		if(StringFunction.isNotBlank(invoiceGroupOid)){

            StringBuilder invpPdfSql = new StringBuilder();
            List<Object> temp = new ArrayList<Object>();
			invpPdfSql.append("select doc_image_oid from document_image where p_transaction_oid in (");
			invpPdfSql.append( "SELECT upload_invoice_oid from INVOICES_SUMMARY_DATA where A_INVOICE_GROUP_OID = ?");
			temp.add(invoiceGroupOid);
			if(isIgnoreCurrentInvoice && StringFunction.isNotBlank(getAttribute("upload_invoice_oid"))){
				invpPdfSql.append( " and upload_invoice_oid != ?");
				temp.add(getAttribute("upload_invoice_oid"));
			}
			invpPdfSql.append(")");
		
			DocumentHandler	imagesListDoc = DatabaseQueryBean.getXmlResultSet(invpPdfSql.toString(), false, temp);
			String attachInd ="N";
			if (imagesListDoc != null){
				attachInd="Y";
			}
			String updateValueDateSQL = "UPDATE INVOICE_GROUP SET attachment_ind = ? where INVOICE_GROUP_OID = ?" ;
			try {
				DatabaseQueryBean.executeUpdate(updateValueDateSQL, false, attachInd, invoiceGroupOid);
			} catch (SQLException e) {
				throw new AmsException(e.getMessage());
			}
		}
	}
	// IR 18421 Rel8.3 09/17/2013 end

	/**
	 * Searches for an existing invoice group that matches the current object.
	 * @return OID of the Invoice Group if a match is found; otherwise null.
	 * @throws AmsException
	 * @throws RemoteException
	 */
	protected String findMatchingInvoiceGroup() throws AmsException, RemoteException {
		if(isCreditNote()){
		// Credit Notes should never be grouped with each other
			return null;
		}
		
		Date paymentDate = DateTimeUtility.convertStringDateToDate(getDateForGrouping());
		String dateStr = DateTimeUtility.formatDate(paymentDate, "dd-MMM-yyyy");
		String suppDateStr = null;
		if(StringFunction.isNotBlank(getAttribute("send_to_supplier_date"))){
		 Date suppDate = DateTimeUtility.convertStringDateToDate(getAttribute("send_to_supplier_date"));
		 suppDateStr = DateTimeUtility.formatDate(suppDate, "dd-MMM-yyyy");
		}
		
		StringBuilder oidSql = new StringBuilder("select INVOICE_GROUP_OID");
		List<Object> temp = new ArrayList<Object>();
		oidSql.append(" from invoice_group where payment_date = ?");
		temp.add(dateStr);
		if(TradePortalConstants.INV_LINKED_INSTR_PYB.equals(getAttribute("linked_to_instrument_type")) && StringFunction.isBlank(getAttribute("end_to_end_id"))){ 
			if( StringFunction.isNotBlank(suppDateStr)){
				oidSql.append(" and send_to_supplier_date = ?");
				temp.add(suppDateStr);
			}
			else{
				oidSql.append(" and send_to_supplier_date is null");
			}
			
		}
		 if(TradePortalConstants.INV_LINKED_INSTR_PYB.equals(getAttribute("linked_to_instrument_type")) ||
				 TradePortalConstants.INV_LINKED_INSTR_REC.equals(getAttribute("linked_to_instrument_type")) && 
				 StringFunction.isBlank(getAttribute("end_to_end_id"))){ 
			 	oidSql.append(" and invoice_status not in ('");
			 	oidSql.append(TradePortalConstants.UPLOAD_INV_STATUS_PAR_AUTH);
			 	oidSql.append("','");
			 	oidSql.append(TradePortalConstants.UPLOAD_INV_STATUS_AUTH_FAILED);
				oidSql.append("')");
		 }
		//SHR CR708 Rel8.1.1 Change Begin
		String[] ATTR = ATTR_PATHS;
		//CR 913 start
		String[] ATTR_COLUMNS = GROUP_ATTR_COLUMNS;
		if(TradePortalConstants.INV_LINKED_INSTR_PYB.equals(getAttribute("linked_to_instrument_type"))){
			ATTR = ATTR_PAY_PGRM_PATHS;
			ATTR_COLUMNS = GROUP_ATTR_PAY_PGRM_COLUMNS;
		}
		//CR 913 end
		for (int i=0; i<ATTR_COLUMNS.length; i++) {
			String currentValue = getAttribute(ATTR[i]);			
			//SHR CR708 Rel8.1.1 Change End
			
			oidSql.append(" and ");
			if(ATTR[i].equals("tp_rule_name")){
				oidSql.append("upper(");
				oidSql.append(ATTR_COLUMNS[i]);
				oidSql.append(")");
				currentValue= StringFunction.isNotBlank(currentValue)?currentValue.toUpperCase():"";
			}
			else{
				oidSql.append(ATTR_COLUMNS[i]);
				}

			if (StringFunction.isBlank(currentValue)) {
				oidSql.append(" is null");
			}
			else {
				oidSql.append(" = ?");
				temp.add(currentValue);
			}
		}

		DocumentHandler resultsDoc = DatabaseQueryBean.getXmlResultSet(oidSql.toString(), false, temp.toArray());
		if (resultsDoc != null) {
			return resultsDoc.getAttribute("/ResultSetRecord/INVOICE_GROUP_OID");
		}
		return null;
	}

	/**
	 * Searches for an existing invoice group that matches the current object.
	 * @return OID of the newly created Invoice Group
	 * @throws AmsException
	 * @throws RemoteException
	 */
	public String createInvoiceGroup() throws AmsException, RemoteException {
		InvoiceGroup invoiceGroup = (InvoiceGroup) createServerEJB("InvoiceGroup");
		Long invoiceGroupOid = invoiceGroup.newObject();
		//SHR CR708 Rel8.1.1 Change Begin
		String[] ATTR = ATTR_PATHS;
		String[] GROUP_ATTR_PATHS_NEW = GROUP_ATTR_PATHS;
		//CR 913 start
		if(TradePortalConstants.INV_LINKED_INSTR_PYB.equals(getAttribute("linked_to_instrument_type"))){
			ATTR = ATTR_PAY_PGRM_PATHS;
			GROUP_ATTR_PATHS_NEW = GROUP_ATTR_PAY_PGRM_PATHS;
		}
		//CR 913 end
		for (int i=0; i<GROUP_ATTR_PATHS_NEW.length; i++) {
			String currentValue = getAttribute(ATTR[i]);
			
			if (StringFunction.isNotBlank(currentValue)) {
				invoiceGroup.setAttribute(GROUP_ATTR_PATHS_NEW[i], currentValue);
			}
		}
		invoiceGroup.setAttribute("payment_date", getDateForGrouping());
				
		invoiceGroup.save();

		return invoiceGroupOid.toString();
	}

	/**
	 * Tests if this object has a matching invoice group associated with it
	 * @return true if invoice group is associated and all corresponding fields match
	 * @throws AmsException
	 * @throws RemoteException
	 */
	protected boolean hasMatchingInvoiceGroup() throws AmsException, RemoteException {
		String invoiceGroupOid = getAttribute("invoice_group_oid");
		if (StringFunction.isBlank(invoiceGroupOid) ){
		return false;
		}

		InvoiceGroup invoiceGroup = (InvoiceGroup) createServerEJB("InvoiceGroup",
				Long.valueOf(invoiceGroupOid));
		//SHR CR708 Rel8.1.1 Change Begin
		String[] ATTR = ATTR_PATHS;
		String[] GROUP_ATTR_PATHS_NEW = GROUP_ATTR_PATHS;
		//CR 913 start
		if(TradePortalConstants.INV_LINKED_INSTR_PYB.equals(getAttribute("linked_to_instrument_type"))){
			  ATTR = ATTR_PAY_PGRM_PATHS;
			GROUP_ATTR_PATHS_NEW = GROUP_ATTR_PAY_PGRM_PATHS;
		}
		//CR 913 end
		for (int i=0; i<ATTR.length; i++) {
			String myValue = getAttribute(ATTR[i]);
			//SHR CR708 Rel8.1.1 Change End
			String groupValue = invoiceGroup.getAttribute(GROUP_ATTR_PATHS_NEW[i]);

			if ((StringFunction.isBlank(myValue) && StringFunction.isNotBlank(groupValue)) ||
					(StringFunction.isNotBlank(myValue) && !myValue.equals(groupValue))) {
				// this invoice no longer matches the current group
				// IR 18421 Rel8.3 starts - update the current group
				updateInvoiceGroup(invoiceGroupOid,true);
				return false;
			}
		}
		
		// check if the date matches the current group
		String paymentDate = getDateForGrouping();
		if (StringFunction.isNotBlank(paymentDate) &&
				!paymentDate.equals(invoiceGroup.getAttribute("payment_date"))) {
			// IR 18421 starts - update the current group
			updateInvoiceGroup(invoiceGroupOid,true);
			return false;
		}

		// IR 18421 Rel8.3 starts
			updateInvoiceGroup(invoiceGroupOid,false);
	
		// IR 18421 Rel8.3 ends
		// everything was a match
		return true;
	}


	/**
	 * Gets the date to use for grouping.  It is assumed that Due Date always
	 * has a value.
	 * 
	 * @return Payment Date if it is set; otherwise, Due Date
	 * @throws AmsException
	 * @throws RemoteException
	 */
	protected String getDateForGrouping() throws AmsException, RemoteException {
		String dateForGrouping = getAttribute("payment_date");

		if (StringFunction.isBlank(dateForGrouping )) {
			dateForGrouping  = getAttribute("due_date");
		}

		return dateForGrouping;
	}

	/**
	 * Performs any special "issueOptimisticLockError" processing that is 
	 * specific to the descendant of BusinessObject.
	 * 
	 * @throws AmsException
	 * @throws RemoteException
	 */
	protected void issueOptimisticLockError() throws AmsException, RemoteException {
		if (!getRetryOptLock()) {
			getErrorManager().issueError(getClass().getName(),
					TradePortalConstants.UPLOAD_INV_OPT_LOCK_EXCEPTION);
		}
	}

	/**
	 * Calculates the discount or interest and updates this invoice with the
	 * appropriate values.  If any validations fail, an AmsException is thrown
	 * with the appropriate error code.
	 * 
	 * @param matchingRule
	 * @throws AmsException
	 * @throws RemoteException
	 */
	public void calculateInterestDiscount(BeanManager beanMgr) throws AmsException, RemoteException {
		ArMatchingRule matchingRule = getMatchingRule();
		calculateInterestDiscount(matchingRule,beanMgr);
		}

	/**
	 * Calculates the discount or interest and updates this invoice with the
	 * appropriate values.  If any validations fail, an AmsException is thrown
	 * with the appropriate error code.
	 * 
	 * @throws AmsException
	 * @throws RemoteException
	 */
	public void calculateInterestDiscount(ArMatchingRule matchingRule,BeanManager beanMgr)
			throws AmsException, RemoteException {
		
        CorporateOrganizationWebBean  corpOrg = beanMgr.createBean(CorporateOrganizationWebBean.class, "CorporateOrganization",csdb);
        corpOrg.getById(getAttribute("corp_org_oid"));
		calculateInterestDiscount(matchingRule, corpOrg);
	}
	public void calculateInterestDiscount(ArMatchingRule matchingRule,CorporateOrganizationWebBean corpOrg)
	throws AmsException, RemoteException {
		validateInstrumentType();
		if (matchingRule == null) {
			String errMsg = "Trading Partner Rule is null for Invoice ID "
				+ getAttribute("invoice_id");
			LOG.debug(errMsg);
			IssuedError is = new IssuedError();
			is.setErrorCode(TradePortalConstants.ERR_UPLOAD_INV_PCT_VAL_TO_FIN_ZERO);
			throw new AmsException(is);
		}
		String dueDate = getAttribute("due_date");
		String paymentDate = getAttribute("payment_date");
		int numTenorDays = InvoiceUtility.calculateNumTenorDays(matchingRule,null,null,dueDate,paymentDate);

		String value = matchingRule.getAttribute("invoice_value_finance");
		BigDecimal percentFinanceAllowed = StringFunction.isBlank(value) ? BigDecimal.ZERO :
			new BigDecimal(value).divide(new BigDecimal("100.0"),
					MathContext.DECIMAL64);

		value = getAttribute("amount");
		BigDecimal invoiceAmount = StringFunction.isBlank(value) ? BigDecimal.ZERO :
			new BigDecimal(value, MathContext.DECIMAL64);

		BigDecimal financeAmount = invoiceAmount.multiply(percentFinanceAllowed);
    
   // Surrewsh IR-T36000026428 11/26/2015 Start 
       
		int numberOfCurrencyPlaces = 2;
        String CurrencyCode =getAttribute("currency");
        
        
    
        if (CurrencyCode != null && !CurrencyCode.equals("")) {
               numberOfCurrencyPlaces = Integer.parseInt(ReferenceDataManager
                            .getRefDataMgr().getAdditionalValue(
                                          TradePortalConstants.CURRENCY_CODE,
                                          CurrencyCode));
        }

        financeAmount = financeAmount.setScale(numberOfCurrencyPlaces,
                     BigDecimal.ROUND_HALF_UP);
   // Surrewsh IR-T36000026428 11/26/2015 End 

		setAttribute("invoice_finance_amount", financeAmount.toString());

		if(corpOrg==null){
			BeanManager beanMgr = new BeanManager();
			corpOrg = beanMgr.createBean(CorporateOrganizationWebBean.class, "CorporateOrganization",csdb);
	        corpOrg.getById(getAttribute("corp_org_oid"));
		}

		String intDiscRateGroup = corpOrg.getAttribute("interest_disc_rate_group");
		if (intDiscRateGroup == null) {
			intDiscRateGroup = "";
		}

		//BSL IR LIUM050240824 05/07/2012 Rel 8.0 BEGIN
		DocumentHandler rateTypeAndMargin = InstrumentServices.getMarginAndRateType(getAttribute("linked_to_instrument_type"),
				getAttribute("currency"),
				getAttribute("invoice_finance_amount"),
				matchingRule.getAttribute("ar_matching_rule_oid"),
				getAttribute("corp_org_oid"));
		//BSL IR LIUM050240824 05/07/2012 Rel 8.0 END

		BigDecimal margin = BigDecimal.ZERO;
		String rateType = "";

		if (rateTypeAndMargin != null) {
			rateType = rateTypeAndMargin.getAttribute("/RATE_TYPE");
			String marginStr = rateTypeAndMargin.getAttribute("/MARGIN");
			if (StringFunction.isNotBlank(marginStr)) {
				margin = new BigDecimal(marginStr, MathContext.DECIMAL64);
			}
		}
		String sqlQuery = getInterestRateSQL(numTenorDays);

		String rateStr = null;
		String numInterestDaysStr = null;
		List<Object> tempList = new ArrayList<Object>();
		tempList.add(getAttribute("currency"));
		tempList.add(rateType);
		tempList.add(intDiscRateGroup);
		tempList.add(getAttribute("currency"));
		tempList.add(rateType);
		tempList.add(intDiscRateGroup);

		DocumentHandler resultsDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery.toString(), false, tempList);
		if (resultsDoc != null) {
			rateStr = resultsDoc.getAttribute("/ResultSetRecord/RATE");
			numInterestDaysStr = resultsDoc.getAttribute("/ResultSetRecord/NUM_INTEREST_DAYS");
		}

		if (StringFunction.isBlank(rateStr)) {
			rateStr = "0.0"; // Default if value not found in DB
		}

		if (StringFunction.isBlank(numInterestDaysStr)) {
			numInterestDaysStr = "360"; // Default if value not found in DB
		}
		
		BigDecimal rate = new BigDecimal(rateStr, MathContext.DECIMAL64);
		rate = rate.add(margin).divide(new BigDecimal("100.0"));

		BigDecimal numTenorDaysDecimal = new BigDecimal(numTenorDays);
		BigDecimal netFinanceAmount = financeAmount;

		String interestDiscountInd = corpOrg.getAttribute("interest_discount_ind");
		String calculateDiscountInd = corpOrg.getAttribute("calculate_discount_ind");

		BigDecimal numInterestDays = new BigDecimal(numInterestDaysStr);
		BigDecimal rateByDays = rate.multiply(numTenorDaysDecimal).divide(
				numInterestDays, MathContext.DECIMAL64);

        //cquinton 4/3/2013 Rel 8.1.0.4 ir#15381 use correct indicators		
		if (TradePortalConstants.INTEREST_COLLECTED_FINANCE_AMOUNT.equals(interestDiscountInd)) {
			BigDecimal interestAmount = financeAmount.multiply(rateByDays);

			setAttribute("estimated_interest_amount",
					interestAmount.toString());
		}
		else if (TradePortalConstants.DISCOUNT_NETTED_FINANCE_AMOUNT.equals(interestDiscountInd)) {
			BigDecimal discountAmount = BigDecimal.ZERO;

			if (TradePortalConstants.STRAIGHT_DISCOUNT.equals(calculateDiscountInd)) {
				discountAmount = financeAmount.multiply(rateByDays);
			}
			else if (TradePortalConstants.DISCOUNT_YIELD.equals(calculateDiscountInd)) {
				discountAmount =
					financeAmount.multiply(BigDecimal.ONE.subtract(BigDecimal.ONE.divide(
							BigDecimal.ONE.add(rateByDays), MathContext.DECIMAL64)));
			}

			netFinanceAmount = financeAmount.subtract(discountAmount);
		}

		setAttribute("net_invoice_finance_amount",
				netFinanceAmount.toString());
	}

	/**
	 * Identifies and retrieves the Matching Rule for this invoice.
	 * First it looks for a rule with a matching buyer_name.  If no match is
	 * found for the buyer_name, it looks for a rule with a matching buyer_id.
	 * 
	 * @throws AmsException
	 * @throws RemoteException
	 */
	public ArMatchingRule getMatchingRule() throws AmsException, RemoteException {
		final String outerSelect = "SELECT DISTINCT ar_matching_rule_oid"
			+ " FROM ar_matching_rule WHERE ";
		final String innerSelect = " OR ar_matching_rule_oid IN"
			+ " (SELECT p_ar_matching_rule_oid FROM";

		StringBuilder sqlBuilder = null;
		DocumentHandler resultSet = null;
		String tradePartnerOid = null;
        String corpOrgOid = getAttribute("corp_org_oid");
        List<Object> sqlParams = null;
		
        String buyerName = TradePortalConstants.INVOICE_TYPE_REC_MGMT.equals(getAttribute("invoice_classification"))?getAttribute("buyer_name"):getAttribute("seller_name");//SHR CR708 Rel8.1.1 
		
		if (StringFunction.isNotBlank(buyerName)) {
			buyerName = buyerName.toUpperCase().trim();
			sqlParams = new ArrayList<Object>();
			sqlBuilder = new StringBuilder(outerSelect);
			sqlBuilder.append("p_corp_org_oid = ?");
			sqlParams.add(corpOrgOid);
			sqlBuilder.append(" and (buyer_name = unistr(?)");	
			sqlParams.add(StringFunction.toUnistr(buyerName));
			sqlBuilder.append(innerSelect);
			sqlBuilder.append(" ar_buyer_name_alias WHERE buyer_name_alias = unistr(?)))"); // Nar -MKUM061235356
			sqlParams.add(StringFunction.toUnistr(buyerName));
			resultSet = DatabaseQueryBean.getXmlResultSet(sqlBuilder.toString(), false, sqlParams);
			if (resultSet != null) {
				tradePartnerOid = resultSet.getAttribute("/ResultSetRecord/AR_MATCHING_RULE_OID");
			}
		}

		if (tradePartnerOid == null) {
			String buyerId = TradePortalConstants.INVOICE_TYPE_REC_MGMT.equals(getAttribute("invoice_classification"))?getAttribute("buyer_id"):getAttribute("seller_id");//SHR CR708 Rel8.1.1
			if (StringFunction.isNotBlank(buyerId)) {
				buyerId = buyerId.toUpperCase().trim();
				sqlParams = new ArrayList<Object>();
				sqlBuilder = new StringBuilder(outerSelect);
				sqlBuilder.append(" p_corp_org_oid = ?");
				sqlParams.add(corpOrgOid);
				sqlBuilder.append(" and (buyer_id = unistr(?)");
				sqlParams.add(StringFunction.toUnistr(buyerId));
				sqlBuilder.append(innerSelect);
				sqlBuilder.append(" ar_buyer_id_alias WHERE buyer_id_alias = unistr(?)))"); // Nar -MKUM061235356
				sqlParams.add(StringFunction.toUnistr(buyerId));
				resultSet = DatabaseQueryBean.getXmlResultSet(sqlBuilder.toString(), false, sqlParams);
				if (resultSet != null) {
					tradePartnerOid = resultSet.getAttribute("/ResultSetRecord/AR_MATCHING_RULE_OID");
				}
			}
		}

		if (StringFunction.isNotBlank(tradePartnerOid)) {
			ArMatchingRule matchingRule = (ArMatchingRule)createServerEJB("ArMatchingRule",
					Long.parseLong(tradePartnerOid));
			setAttribute("tp_rule_name", matchingRule.getAttribute("buyer_name"));
			// In case buyer_name or buyer_id are blank or an alias value,
			// update with the actual values from the rule.
			if(TradePortalConstants.INVOICE_TYPE_REC_MGMT.equals(getAttribute("invoice_classification")) && StringFunction.isBlank(getAttribute("buyer_name"))){
				setAttribute("buyer_name", matchingRule.getAttribute("buyer_name"));
			}
			if(TradePortalConstants.INVOICE_TYPE_PAY_MGMT.equals(getAttribute("invoice_classification")) && StringFunction.isBlank(getAttribute("seller_name"))){
				setAttribute("seller_name", matchingRule.getAttribute("buyer_name"));
			}

			return matchingRule;
		}

		return null;
	}

	/**
	 * Validates compliance of Days Variance of Payment Date to Invoice Due
	 * Date and the Days Allowed for Financing Before Invoice Due Date or
	 * Payment Date and calculates the number of tenor days.  If the days
	 * values are non-compliant, an AmsException is thrown with the
	 * appropriate error code.
	 * 
	 * @return number of tenor days
	 * @throws AmsException
	 * @throws RemoteException
	 */

	private int calculateDaysDiff(String dateStr1) throws AmsException {
		Date date1 = DateTimeUtility.convertStringDateToDate(dateStr1);
		Date date2 = GMTUtility.getGMTDateTime();

		return calculateDaysDiff(date1, date2);
	}

	private int calculateDaysDiff(Date date1, Date date2)
	throws AmsException {
		Calendar cal = Calendar.getInstance();

		cal.setTime(date1);
		long dateInMillis1 = cal.getTimeInMillis();

		cal.setTime(date2);
		long dateInMillis2 = cal.getTimeInMillis();

		double daysDiff = (double)(dateInMillis1 - dateInMillis2) / (24 * 60 * 60 * 1000);
		// use ceil to make  full day if it is coming 20.24, then it should be 21 days.
		return ((int)Math.ceil(daysDiff));
	}

	/*
	 * if invoice status is unable to finance, then set all calculate amount to null.
	 * because Inv packager decide AFF or NFN invoice on basis of net invoice finance amount.
	 */
	private void verifyInvoiceStatus()
	throws AmsException, RemoteException {
		
		if(TradePortalConstants.UPLOAD_INV_STATUS_PEND_ACT_UNABLE_TO_FIN.equals(getAttribute("invoice_status"))){
			setAttribute("invoice_finance_amount", null);
			setAttribute("net_invoice_finance_amount", null);
			setAttribute("estimated_interest_amount", null);
		}
		
	}
	
	//SHR CR 708 Rel8.1.1 Start
	/* Perform processing for a New instance of the business object */

    protected void userNewObject() throws AmsException, RemoteException
    {
        /* Perform any New Object processing defined in the Ancestor class */
        super.userNewObject();
        this.setAttribute("creation_date_time", DateTimeUtility.getGMTDateTime(true, false));//do not use override date
    }
		
	public void calculateInterestDiscount(String corpOrgOid) throws AmsException, RemoteException{
    	LOG.debug("calculateInterestDiscount corpOrgOid: {}",corpOrgOid);
    	CorporateOrganization corp_org = null;
		String amount   = this.getAttribute("amount");
		String currency = this.getAttribute("currency");
		String percentToFinLoan = "";
		String intDiscRateGroup ="";		
		int numTenorDays =0;
		String interestDiscountInd  = null;
		String calculateDiscountInd = null;
			
		//create User object		
		if(StringFunction.isNotBlank(corpOrgOid)){
				corp_org = (CorporateOrganization) createServerEJB("CorporateOrganization",Long.parseLong(corpOrgOid));
				percentToFinLoan = corp_org.getAttribute("inv_percent_to_fin_trade_loan");
				
		        intDiscRateGroup = corp_org.getAttribute("interest_disc_rate_group");
				if (intDiscRateGroup == null) {
						intDiscRateGroup = "";
				}
			
			numTenorDays = calculateNumTenorDays(corp_org);
			 LOG.debug("numTenorDays->: {}",numTenorDays);
			 interestDiscountInd  = corp_org.getAttribute("interest_discount_ind");
		    calculateDiscountInd = corp_org.getAttribute("calculate_discount_ind");
		}
	
		BigDecimal percentFinanceAllowed = StringFunction.isBlank(percentToFinLoan) ? BigDecimal.ZERO :
			new BigDecimal(percentToFinLoan).divide(new BigDecimal("100.0"),
					MathContext.DECIMAL64);

		BigDecimal invoiceAmount = StringFunction.isBlank(amount) ? BigDecimal.ZERO :
			new BigDecimal(amount, MathContext.DECIMAL64);

		BigDecimal financeAmount = invoiceAmount.multiply(percentFinanceAllowed);
		   // Surrewsh IR-T36000026428 11/26/2015 Start 
       
		int numberOfCurrencyPlaces = 2;
        String CurrencyCode = getAttribute("currency");
      
        

        if (CurrencyCode != null && !CurrencyCode.equals("")) {
               numberOfCurrencyPlaces = Integer.parseInt(ReferenceDataManager
                            .getRefDataMgr().getAdditionalValue(
                                          TradePortalConstants.CURRENCY_CODE,
                                          CurrencyCode));
        }

        financeAmount = financeAmount.setScale(numberOfCurrencyPlaces,
                     BigDecimal.ROUND_HALF_UP);
   // Surrewsh IR-T36000026428 11/26/2015 End 
		

		DocumentHandler rateTypeAndMargin = InvoiceUtility.getMarginAndRateType(InstrumentType.LOAN_RQST, currency, amount, corpOrgOid);

		BigDecimal margin = BigDecimal.ZERO;
		String rateType = "";

		if (rateTypeAndMargin != null) {
			rateType = rateTypeAndMargin.getAttribute("/RATE_TYPE");
			String marginStr = rateTypeAndMargin.getAttribute("/MARGIN");
			if (StringFunction.isNotBlank(marginStr)) {
				margin = new BigDecimal(marginStr, MathContext.DECIMAL64);
			}
		}

		    String sqlQuery = getInterestRateSQL(numTenorDays);

	   		String rateStr = null;
	   		String numInterestDaysStr = null;
	   		List<Object> tempList = new ArrayList<Object>();
			tempList.add(currency);
			tempList.add(rateType);
			tempList.add(intDiscRateGroup);
			tempList.add(currency);
			tempList.add(rateType);
			tempList.add(intDiscRateGroup);

	   		DocumentHandler resultsDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery, false, tempList);
	   		if (resultsDoc != null) {
	   			rateStr = resultsDoc.getAttribute("/ResultSetRecord/RATE");
	   			numInterestDaysStr = resultsDoc.getAttribute("/ResultSetRecord/NUM_INTEREST_DAYS");
	   		}

	   		if (StringFunction.isBlank(rateStr)) {
	   			rateStr = "0.0"; // Default if value not found in DB
	   		}

	   		if (StringFunction.isBlank(numInterestDaysStr)) {
	   			numInterestDaysStr = "360"; // Default if value not found in DB
	   		}

	   		BigDecimal rate = new BigDecimal(rateStr, MathContext.DECIMAL64);
	   		rate = rate.add(margin).divide(new BigDecimal("100.0"));

	   		BigDecimal numTenorDaysDecimal = new BigDecimal(numTenorDays);
		    BigDecimal netFinanceAmount = financeAmount;


		   BigDecimal numInterestDays = new BigDecimal(numInterestDaysStr);
		   BigDecimal rateByDays = rate.multiply(numTenorDaysDecimal).divide(
		   				numInterestDays, MathContext.DECIMAL64);

		        //cquinton 4/3/2013 Rel 8.1.0.4 ir#15381 use correct indicators		
				if (TradePortalConstants.INTEREST_COLLECTED_FINANCE_AMOUNT.equals(interestDiscountInd)) {
					BigDecimal interestAmount = financeAmount.multiply(rateByDays);

					this.setAttribute("estimated_interest_amount", interestAmount.toString());
				}
				else if (TradePortalConstants.DISCOUNT_NETTED_FINANCE_AMOUNT.equals(interestDiscountInd)) {
					BigDecimal discountAmount = BigDecimal.ZERO;

					if (TradePortalConstants.STRAIGHT_DISCOUNT.equals(calculateDiscountInd)) {
						discountAmount = financeAmount.multiply(rateByDays);
					}
					else if (TradePortalConstants.DISCOUNT_YIELD.equals(calculateDiscountInd)) {
						discountAmount =
							financeAmount.multiply(BigDecimal.ONE.subtract(BigDecimal.ONE.divide(
									BigDecimal.ONE.add(rateByDays), MathContext.DECIMAL64)));
					}

					netFinanceAmount = financeAmount.subtract(discountAmount);
				}
				
		   		this.setAttribute("net_invoice_finance_amount", netFinanceAmount.toString());
		
	}

	
    private int calculateNumTenorDays (CorporateOrganization corpOrg)
    		throws AmsException, RemoteException {
    		LOG.debug("calculateNumTenorDays");
    			// check % inv val to finance
    			String pctFinAllowedStr = corpOrg.getAttribute("inv_percent_to_fin_trade_loan");
    			BigDecimal pctFinAllowed = StringFunction.isBlank(pctFinAllowedStr) ? BigDecimal.ZERO :
    				new BigDecimal(pctFinAllowedStr).divide(new BigDecimal("100.0"),
    						MathContext.DECIMAL64);

    			if (BigDecimal.ZERO.compareTo(pctFinAllowed) == 0) {
    				// Percent Invoice Value to Finance is zero
    				IssuedError is = new IssuedError();
    				is.setErrorCode(TradePortalConstants.ERR_UPLOAD_INV_PCT_VAL_TO_FIN_ZERO);
    				throw new AmsException(is);
    			}
    			

    			String paymentDateAllowed = corpOrg.getAttribute("use_payment_date_allowed_ind");
    			LOG.debug("paymentdate allow: {}",paymentDateAllowed);
    			String dueDate = getAttribute("due_date");
    			String paymentDate = getAttribute("payment_date");
    			LOG.debug("duedate: {} ;payment date: {}",dueDate,paymentDate);
    			boolean usePaymentDate = (StringFunction.isNotBlank(paymentDate)
    					&& TradePortalConstants.INDICATOR_YES.equals(paymentDateAllowed));

    			String effectiveDate;
    			if (usePaymentDate) {
    				effectiveDate = paymentDate;
    			}
    			else {
    				effectiveDate = dueDate;
    			}

    			// calculate based on current business date to due date/payment date
    			int numTenorDays = calculateDaysDiff(effectiveDate);

    			LOG.debug("numTenorDays: {}",numTenorDays);
    			return numTenorDays;
    		}
    
  //IR T36000017596 start- get the date used in calculation
    public String getActualDate() throws RemoteException, AmsException{
    	// Get settings from Corp Org instead of TP rule
    	CorporateOrganization corpOrg = (CorporateOrganization) createServerEJB("CorporateOrganization",getAttributeLong("corp_org_oid"));
		
		if (StringFunction.isNotBlank(getAttribute("payment_date"))
				&& TradePortalConstants.INDICATOR_YES.equals(corpOrg.getAttribute("payment_day_allow"))) {
			return getAttribute("payment_date");
		}
		else {
			return getAttribute("due_date");
		}
    }
  //IR T36000017596 end
  
    //Get Trade partner name based on invoice type.
    
    public String getTpRuleName() throws AmsException, RemoteException {
    	String tpName ="";
    	if(TradePortalConstants.INVOICE_TYPE_REC_MGMT.equals(getAttribute("invoice_classification"))){
    		tpName =getTpRuleName(getMatchingRule());
    	}
    	else{
    		tpName =getTpRuleName(null);
    	}
    	return tpName;
    }
    
    public String getTpRuleName(ArMatchingRule matchingRule) throws AmsException, RemoteException {
    	String tpName ="";
    	if(TradePortalConstants.INVOICE_TYPE_REC_MGMT.equals(getAttribute("invoice_classification"))){
			 if(matchingRule!=null)
				tpName =matchingRule.getAttribute("buyer_name"); 
			 
			 else
				 tpName= StringFunction.isNotBlank(getAttribute("buyer_name"))?getAttribute("buyer_name"):getAttribute("buyer_id");
		 }
		 else if(TradePortalConstants.INVOICE_TYPE_PAY_MGMT.equals(getAttribute("invoice_classification"))) {
			tpName = StringFunction.isNotBlank(getAttribute("seller_name"))?getAttribute("seller_name"):getAttribute("seller_id");
		 }
    	return tpName;
		}

    public void close(String uploadUser) throws AmsException, RemoteException{
  	  
  	  	this.setAttribute("invoice_status", TradePortalConstants.INVOICE_STATUS_CLOSED);
       
  	  	this.setAttribute("action", TradePortalConstants.INVOICE_STATUS_CLOSED);
        this.setAttribute("user_oid",uploadUser);
        this.save();
       

    }
    
    public int save() throws NumberFormatException, AmsException, RemoteException {
    	int ret = super.save();
    	if(ret==1){
    	String userOid = this.getAttribute("user_oid");
    	String corpOrgOid = this.getAttribute("corp_org_oid");
    	String action = this.getAttribute("action");
    	if(StringFunction.isNotBlank(action)){
		 if(StringFunction.isBlank(userOid) && StringFunction.isNotBlank(corpOrgOid)){
			 CorporateOrganization corpOrgEJB = (CorporateOrganization) this.createServerEJB("CorporateOrganization", Long.parseLong(corpOrgOid));
			 userOid  = corpOrgEJB.getAttribute("upload_system_user_oid");
		 }
		 InvoiceHistory invHistory = (InvoiceHistory) this.createServerEJB("InvoiceHistory");
		 invHistory.newObject();
		 invHistory.setAttribute("action_datetime", DateTimeUtility.getGMTDateTime(true, true));
		 invHistory.setAttribute("action", action);
		 invHistory.setAttribute("invoice_status", this.getAttribute("invoice_status"));
		 invHistory.setAttribute("upload_invoice_oid", this.getAttribute("upload_invoice_oid"));
		 invHistory.setAttribute("user_oid", userOid);
		 if(TradePortalConstants.INDICATOR_YES.equals(this.getAttribute("reqPanleAuth"))){
			 User user = (User) this.createServerEJB("User", Long.parseLong(userOid));
			 String userPanelAuthLevel = user.getAttribute("panel_authority_code");
			 invHistory.setAttribute("panel_authority_code", userPanelAuthLevel);
		 }
		 
		 if(invHistory.save(false)!=1)
		 {
			 LOG.info("Error occured while creating Invoice History Log from INV for action..."+action);
			 return 0;
		 }
		
    	}
      }
    return 1;
	}
    
    private String getInterestRateSQL(int numTenorDays){
    	StringBuilder sqlQuery = new StringBuilder("SELECT CASE WHEN ");
   		sqlQuery.append(numTenorDays).append(" <= 30 THEN rate_30_days WHEN ");
   		sqlQuery.append(numTenorDays).append(" > 30 AND ");
   		sqlQuery.append(numTenorDays).append(" <= 60 THEN rate_60_days WHEN ");
   		sqlQuery.append(numTenorDays).append(" > 60 AND ");
   		sqlQuery.append(numTenorDays).append(" <= 90 THEN rate_90_days WHEN ");
   		sqlQuery.append(numTenorDays).append(" > 90 AND ");
   		sqlQuery.append(numTenorDays).append(" <= 120 THEN rate_120_days WHEN ");
   		sqlQuery.append(numTenorDays).append(" > 120 AND ");
   		sqlQuery.append(numTenorDays).append(" <= 180 THEN rate_180_days WHEN ");
   		sqlQuery.append(numTenorDays).append(" > 180 THEN rate_360_days END rate,");
   		sqlQuery.append(" num_interest_days");
   		sqlQuery.append(" FROM interest_discount_rate a, interest_discount_rate_group b");
   		sqlQuery.append(" WHERE a.p_interest_disc_rate_group_oid = b.interest_disc_rate_group_oid");
   		sqlQuery.append(" AND currency_code = ?");
   		sqlQuery.append(" AND rate_type = ?");
   		sqlQuery.append(" AND group_id = ?");
   		sqlQuery.append(" AND effective_date  = (");
   		sqlQuery.append(" SELECT max(effective_date)");
   		sqlQuery.append(" FROM interest_discount_rate a2, interest_discount_rate_group b2");
   		sqlQuery.append(" WHERE a2.p_interest_disc_rate_group_oid = b2.interest_disc_rate_group_oid");
   		sqlQuery.append(" AND currency_code = ?");
   		sqlQuery.append(" AND rate_type = ?");
   		sqlQuery.append(" AND group_id = ?)");
   		return sqlQuery.toString();
    
    }
}
