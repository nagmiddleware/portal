


package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;
/**
 * An organization that has been created to process letters of credit.   When
 * each instrument is created, is will be assigned to an operational bank organization.
 * This organization will handle the processing of the LC once it is authorized
 * and sent to the back end.
 *
 *     Copyright  � 2003
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public interface OperationalBankOrganizationHome extends EJBHome
{
   public OperationalBankOrganization create()
      throws RemoteException, CreateException, AmsException;

   public OperationalBankOrganization create(ClientServerDataBridge csdb)
      throws RemoteException, CreateException, AmsException;
}
