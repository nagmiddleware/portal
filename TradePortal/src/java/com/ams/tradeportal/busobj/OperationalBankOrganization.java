



package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;


/**
 * An organization that has been created to process letters of credit.   When
 * each instrument is created, is will be assigned to an operational bank organization.
 * This organization will handle the processing of the LC once it is authorized
 * and sent to the back end.
 *
 *     Copyright  � 2003
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public interface OperationalBankOrganization extends Organization
{
	public java.lang.String getPrefix(String instrumentType) throws RemoteException;

	public java.lang.String getSuffix(String instrumentType) throws RemoteException;

}
