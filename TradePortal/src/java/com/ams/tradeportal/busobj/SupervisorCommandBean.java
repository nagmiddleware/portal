 
package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import java.text.*;
import com.ams.tradeportal.common.*;




/*
 * 
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class SupervisorCommandBean extends SupervisorCommandBean_Base
{
private static final Logger LOG = LoggerFactory.getLogger(SupervisorCommandBean.class);

protected void userNewObject() throws RemoteException, AmsException
 {

   /* Perform any New Object processing defined in the Ancestor class */
   super.userNewObject();
        
   SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");

   this.setAttribute("creation_timestamp", formatter.format(new Date()));   
 } 
}
