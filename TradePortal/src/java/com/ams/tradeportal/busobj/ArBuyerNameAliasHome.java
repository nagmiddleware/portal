
  

package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;
/**
 * The aliases for the AR Buyer Name in an ARMatchingRule.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface ArBuyerNameAliasHome extends EJBHome
{
   public ArBuyerNameAlias create()
      throws RemoteException, CreateException, AmsException;

   public ArBuyerNameAlias create(ClientServerDataBridge csdb)
      throws RemoteException, CreateException, AmsException;
}
