
  

package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;
/**
 * A UserPreferencesPayDef consists of a set of UserPreferencesPayDef
 * to specifiy the preferences of user.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface UserPreferencesPayDefHome extends EJBHome
{
   public UserPreferencesPayDef create()
      throws RemoteException, CreateException, AmsException;

   public UserPreferencesPayDef create(ClientServerDataBridge csdb)
      throws RemoteException, CreateException, AmsException;
}
