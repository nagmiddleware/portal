
  

package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;
/**
 * Business object to store Cross Rate Calculation rules.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface CrossRateCalcRuleHome extends EJBHome
{
   public CrossRateCalcRule create()
      throws RemoteException, CreateException, AmsException;

   public CrossRateCalcRule create(ClientServerDataBridge csdb)
      throws RemoteException, CreateException, AmsException;
}
