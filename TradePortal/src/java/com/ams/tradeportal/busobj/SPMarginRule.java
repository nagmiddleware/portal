

  

package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;


/**
 * Supplier Portal Margin Rule for a Corporate Customer.  These are the margins
 * used to calculate the invoice offer's net amount offer.  See InvoicesSummaryDate.calculateSupplierPortalDiscount().
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface SPMarginRule extends TradePortalBusinessObject
{   
}
