
package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.RemoteException;

import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.util.DateTimeUtility;

/*
 * Placing a row into this table will cause an e-mail to be sent.  The contents
 * of the e-mail are based on the email_data column.
 *
 *     Copyright  � 2003
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class PaymentBenEmailQueueBean extends PaymentBenEmailQueueBean_Base
{
private static final Logger LOG = LoggerFactory.getLogger(PaymentBenEmailQueueBean.class);
	
	
//	public void createNewPaymentBeneficiaryEmailQueueRecord(String domestic_payment_oid) throws RemoteException, AmsException{
//		this.newObject();
//    	
//    	this.setAttribute("creation_timestamp", DateTimeUtility.getCurrentDateTime());
//    	this.setAttribute("status", TradePortalConstants.PYMT_BEN_EMAIL_PENDING);
//    	this.setAttribute("domestic_payment_oid", domestic_payment_oid);
//	}
	
	public void createNewPaymentBeneficiaryEmailQueueRecord(String transactionOID, String requiredStatus)throws RemoteException, AmsException{
		this.newObject();
		this.setAttribute("creation_timestamp", DateTimeUtility.getCurrentDateTime());
    	this.setAttribute("status", TradePortalConstants.PYMT_BEN_EMAIL_PENDING);
    	this.setAttribute("transaction_oid", transactionOID);
    	this.setAttribute("required_status", requiredStatus);    	
	}
	
}
