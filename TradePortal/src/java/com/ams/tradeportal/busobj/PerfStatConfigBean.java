package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.common.TradePortalConstants;
import com.ams.tradeportal.common.cache.Cache;
import com.ams.tradeportal.common.cache.TPCacheManager;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.StringFunction;

public class PerfStatConfigBean extends PerfStatConfigBean_Base {
private static final Logger LOG = LoggerFactory.getLogger(PerfStatConfigBean.class);

	
	  /**
	   * populate the business object from cache
	   *
	   * @param applicationName 
	   * @param className
	   * @param settingName
	   * 
	   */
	public void getDataFromCache(String pageName, String clientBank) throws AmsException
	  {
		StringBuffer sqlQuery = new StringBuffer();
        sqlQuery.append("select * from perf_stat_config where page_name = '")
                 .append(pageName);
                 if (StringFunction.isNotBlank(clientBank)){
                	 sqlQuery.append("' and client_bank = '")
                	 .append(clientBank);
                 }
                 sqlQuery.append("'");		
        String sql = sqlQuery.toString();

		  Cache cache = (Cache)TPCacheManager.getInstance().getCache(TradePortalConstants.PERF_STAT_CONFIG_CACHE);
		  DocumentHandler result = (DocumentHandler)cache.get(sql);
	      if (result != null) {
	    	  populateDataFromResultSet(result);
	      }

	  }
	
}
