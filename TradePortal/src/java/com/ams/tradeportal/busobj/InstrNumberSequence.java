package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;


/**
 * Each client bank has a range of instrument numbers that they are allowed
 * to use.   The beginning and end of the range is stored on the ClientBank
 * business object.  The current point in the sequence is stored in this business
 * object, a one-to-one component of ClientBank.
 * 
 * This data is stored on a separate object instead of ClientBank because it
 * will be frequently updated.   If the sequence were stored on the ClientBank
 * table, it would be difficult for anyone to edit the object because the constant
 * changes of the sequence pointer would cause optimistic lock errors to happen.
 * 
 * Data in this table should always be updated in its own transaction.  Because
 * this data is frequently updated, locking it by placing it into a long transaction
 * could cause contention problems.
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface InstrNumberSequence extends TradePortalBusinessObject
{   
	public long getNextInstrumentNumber() throws RemoteException, AmsException;

}
