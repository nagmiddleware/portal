


package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;




/*
 * The contractual terms that comprise a transaction.  Data entered by the
 * user on the transaction forms pages will be stored in the Terms business
 * object.
 *
 * Attributes for this business object are dynamically registered.  That is,
 * all of the attributes defined here are not registered for all instances.
 * Terms objects have a terms manager object associated with them.  The terms
 * manager object determines which attributes should be registered and handles
 * any instance-specific method calls.
 *
 * For more information, see the "Terms Manager Delegation" section of the
 * Trade Portal Infrastructure Document.
 *
 *     Copyright  � 2003
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class TermsBean_Base extends TradePortalBusinessObjectBean
{
private static final Logger LOG = LoggerFactory.getLogger(TermsBean_Base.class);


  /*
   * Register the attributes and associations of the business object
   */
   protected void registerAttributes() throws AmsException
   {

      /* Register attributes defined in the Ancestor class */
      super.registerAttributes();

      /* Pointer to the component TermsParty */
      /* FirstTermsParty - A Terms business object can be associated to one to seven terms parties.
         Instead of creating a one-to-many association, seven one-to-one assocaitoins
         were created to make coding simpler.  Within these seven terms parties for
         any Terms object, the terms party type of each terms party will be unique.


         On the JSPs, a certain order of terms parties is assumed.  For example,
         the first terms party on an Issue Import LC is the Applicant.   On a standby,
         it might be the advising bank.   See each JSP for specifics.   The proper
         terms party can always be retrieved by examining the terms party type.

         For bank released terms, the applicant is the first terms party and the
         beneficiary is the second terms party. */
      attributeMgr.registerAttribute("c_FirstTermsParty", "c_FIRST_TERMS_PARTY_OID", "NumberAttribute");

      /* Pointer to the component TermsParty */
      /* SecondTermsParty - A Terms business object can be associated to one to seven terms parties.
         Instead of creating a one-to-many association, seven one-to-one assocaitoins
         were created to make coding simpler.  Within these seven terms parties for
         any Terms object, the terms party type of each terms party will be unique.


         On the JSPs, a certain order of terms parties is assumed.  For example,
         the first terms party on an Issue Import LC is the Applicant.   On a standby,
         it might be the advising bank.   See each JSP for specifics.   The proper
         terms party can always be retrieved by examining the terms party type.

         For bank released terms, the applicant is the first terms party and the
         beneficiary is the second terms party. */
      attributeMgr.registerAttribute("c_SecondTermsParty", "c_SECOND_TERMS_PARTY_OID", "NumberAttribute");

      /* Pointer to the component TermsParty */
      /* ThirdTermsParty - A Terms business object can be associated to one to seven terms parties.
         Instead of creating a one-to-many association, seven one-to-one assocaitoins
         were created to make coding simpler.  Within these seven terms parties for
         any Terms object, the terms party type of each terms party will be unique.


         On the JSPs, a certain order of terms parties is assumed.  For example,
         the first terms party on an Issue Import LC is the Applicant.   On a standby,
         it might be the advising bank.   See each JSP for specifics.   The proper
         terms party can always be retrieved by examining the terms party type.

         For bank released terms, the applicant is the first terms party and the
         beneficiary is the second terms party. */
      attributeMgr.registerAttribute("c_ThirdTermsParty", "c_THIRD_TERMS_PARTY_OID", "NumberAttribute");

      /* Pointer to the component TermsParty */
      /* FourthTermsParty - A Terms business object can be associated to one to seven terms parties.
         Instead of creating a one-to-many association, seven one-to-one assocaitoins
         were created to make coding simpler.  Within these seven terms parties for
         any Terms object, the terms party type of each terms party will be unique.


         On the JSPs, a certain order of terms parties is assumed.  For example,
         the first terms party on an Issue Import LC is the Applicant.   On a standby,
         it might be the advising bank.   See each JSP for specifics.   The proper
         terms party can always be retrieved by examining the terms party type.

         For bank released terms, the applicant is the first terms party and the
         beneficiary is the second terms party. */
      attributeMgr.registerAttribute("c_FourthTermsParty", "c_FOURTH_TERMS_PARTY_OID", "NumberAttribute");

      /* Pointer to the component TermsParty */
      /* FifthTermsParty - A Terms business object can be associated to one to six terms parties.
         Instead of creating a one-to-many association, six one-to-one assocaitoins
         were created to make coding simpler.  Within these six terms parties for
         any Terms object, the terms party type of each terms party will be unique.


         On the JSPs, a certain order of terms parties is assumed.  For example,
         the first terms party on an Issue Import LC is the Applicant.   On a standby,
         it might be the advising bank.   See each JSP for specifics.   The proper
         terms party can always be retrieved by examining the terms party type.

         For bank released terms, the applicant is the first terms party and the
         beneficiary is the second terms party. */
      attributeMgr.registerAttribute("c_FifthTermsParty", "c_FIFTH_TERMS_PARTY_OID", "NumberAttribute");

      /* Pointer to the component TermsParty */
      /* SixthTermsParty - A Terms business object can be associated to one to seven terms parties.
         Instead of creating a one-to-many association, seven one-to-one assocaitoins
         were created to make coding simpler.  Within these seven terms parties for
         any Terms object, the terms party type of each terms party will be unique.


         On the JSPs, a certain order of terms parties is assumed.  For example,
         the first terms party on an Issue Import LC is the Applicant.   On a standby,
         it might be the advising bank.   See each JSP for specifics.   The proper
         terms party can always be retrieved by examining the terms party type.

         For bank released terms, the applicant is the first terms party and the
         beneficiary is the second terms party. */
      attributeMgr.registerAttribute("c_SixthTermsParty", "c_SIXTH_TERMS_PARTY_OID", "NumberAttribute");

      /* Pointer to the component TermsParty */
      /* SeventhTermsParty - A Terms business object can be associated to one to seven terms parties.
         Instead of creating a one-to-many association, seven one-to-one assocaitoins
         were created to make coding simpler.  Within these seven terms parties for
         any Terms object, the terms party type of each terms party will be unique.


         On the JSPs, a certain order of terms parties is assumed.  For example,
         the first terms party on an Issue Import LC is the Applicant.   On a standby,
         it might be the advising bank.   See each JSP for specifics.   The proper
         terms party can always be retrieved by examining the terms party type.

         For bank released terms, the applicant is the first terms party and the
         beneficiary is the second terms party. */
      attributeMgr.registerAttribute("c_SeventhTermsParty", "c_SEVENTH_TERMS_PARTY_OID", "NumberAttribute");

      /* Pointer to the component PaymentParty */
      /* OrderingParty - Ordering Party for a CM instrument.

         Note this is using the physical column c_first_terms_party_oid to store
         the component association.  A Terms has either Terms Party component or
         Payment Party component, depending on the instrument type. */
      attributeMgr.registerAttribute("c_OrderingParty", "c_ORDERING_PARTY_OID", "NumberAttribute");

      /* Pointer to the component PaymentParty */
      /* OrderingPartyBank - Ordering Party Bank for a CM instrument.

         Note this is using the physical column c_second_terms_party_oid to store
         the component association.  A Terms has either Terms Party component or
         Payment Party component, depending on the instrument type. */
      attributeMgr.registerAttribute("c_OrderingPartyBank", "c_ORDERING_PARTY_BANK_OID", "NumberAttribute");

      attributeMgr.registerAttribute("individual_debit_ind", "individual_debit_ind", "IndicatorAttribute");

	registerDynamicAttributes();

   }

     /**
      * This method is overriden in the actual Bean class, where it
      * handles registration of dynamic attributes
      */
     protected void registerDynamicAttributes() throws AmsException
      {

      }

   /**
    *  Static inner class to define factory methods for the various attributes.
    *
    */
   public static class Attributes
    {

      /**
        * Attribute factory method for accounts_receivable_type
        *
        * This indicates the  type of A/R activity created by the bank.
        */
	public static Attribute create_accounts_receivable_type()
	 {
	  return (Attribute) new ReferenceAttribute("accounts_receivable_type", "accounts_receivable_type", "ACCOUNTS_RECEIVABLE_TYPE", false, "TermsBeanAlias.accounts_receivable_type");
	 }


      /**
        * Attribute factory method for additional_conditions
        *
        * The additional conditions text for the transaction.
        */
	public static Attribute create_additional_conditions()
	 {
	  return (Attribute) new Attribute("additional_conditions", "additional_conditions", false, "TermsBeanAlias.additional_conditions");
	 }


      /**
        * Attribute factory method for addl_amounts_covered
        *
        * Additional Amounts Covered (General section) - this will allow the user
        * to enter up to 4 lines of 35 characters (140 characters) to describe any
        * additional amounts covered in the LC. Any data in this field will overwrite
        * what was entered and sent to the bank  during the Request to Advise. It
        * will not supplement the previous data. This will be an optional field.
        */
	public static Attribute create_addl_amounts_covered()
	 {
	  return (Attribute) new Attribute("addl_amounts_covered", "addl_amounts_covered", false, "TermsBeanAlias.addl_amounts_covered");
	 }


      /**
        * Attribute factory method for addl_doc_indicator
        *
        * Indicates that an additional document has been specified in the Required
        * Documents section.
        */
	public static Attribute create_addl_doc_indicator()
	 {
	  return (Attribute) new IndicatorAttribute("addl_doc_indicator", "addl_doc_indicator", false, "TermsBeanAlias.addl_doc_indicator");
	 }


      /**
        * Attribute factory method for addl_doc_text
        *
        * Text for the additional document has been specified in the Required Documents
        * section.
        */
	public static Attribute create_addl_doc_text()
	 {
	  return (Attribute) new Attribute("addl_doc_text", "addl_doc_text", false, "TermsBeanAlias.addl_doc_text");
	 }


      /**
        * Attribute factory method for adjustment_type
        *
        * A classification of financial adjustments that are established by the bank
        */
	public static Attribute create_adjustment_type()
	 {
	  return (Attribute) new ReferenceAttribute("adjustment_type", "adjustment_type", "ADJUSTMENT_TYPE", false, "TermsBeanAlias.adjustment_type");
	 }


      /**
        * Attribute factory method for amendment_date
        *
        * The date that the amendent was processed on OTL
        */
	public static Attribute create_amendment_date()
	 {
	  return (Attribute) new DateTimeAttribute("amendment_date", "amendment_date", false, "TermsBeanAlias.amendment_date");
	 }


      /**
        * Attribute factory method for amendment_details
        *
        * The additional Amendment Details text for the transaction.
        */
	public static Attribute create_amendment_details()
	 {
	  return (Attribute) new Attribute("amendment_details", "amendment_details", false, "TermsBeanAlias.amendment_details");
	 }


      /**
        * Attribute factory method for amount
        *
        * The amount of the transaction. This is validated to have the correct number
        * of decimal places based on the currency selected.   For amendments, this
        * attribute is allowed to have a negative value.  For all other transactions,
        * this attribute must be validated that it is a positive number.
        */
	public static Attribute create_amount()
	 {
	  return (Attribute) new TradePortalSignedDecimalAttribute("amount", "amount", false, "TermsBeanAlias.amount");
	 }


	public static Attribute create_net_finance_amount()
	 {
	  return (Attribute) new TradePortalDecimalAttribute("net_finance_amount", "net_finance_amount", false, "TermsBeanAlias.amount");
	 }
		
	public static Attribute create_estimated_interest_amount()
	 {
	  return (Attribute) new TradePortalDecimalAttribute("estimated_interest_amount", "estimated_interest_amount", false, "TermsBeanAlias.amount");
	 }
	public static Attribute create_payment_method()
	 {
		return (Attribute) new ReferenceAttribute("payment_method", "payment_method", "PAYMENT_METHOD", false, "TermsBeanAlias.PAYMENT_METHOD");
	 }
	
      /**
        * Attribute factory method for amount_currency_code
        *
        * The currency for the instrument.
        */
	public static Attribute create_amount_currency_code()
	 {
	  return (Attribute) new ReferenceAttribute("amount_currency_code", "amount_currency_code", "CURRENCY_CODE", false, "TermsBeanAlias.amount_currency_code");
	 }


      /**
        * Attribute factory method for amt_tolerance_neg
        *
        * The amount tolerance of the transaction. This is the percentage amount that
        * the instrument can be underdrawn.
        */
	public static Attribute create_amt_tolerance_neg()
	 {
	  return (Attribute) new NumberAttribute("amt_tolerance_neg", "amt_tolerance_neg", false, "TermsBeanAlias.amt_tolerance_neg");
	 }


      /**
        * Attribute factory method for amt_tolerance_pos
        *
        * The amount tolerance of the transaction. This is the percentage amount that
        * the instrument can be overdrawn.
        */
	public static Attribute create_amt_tolerance_pos()
	 {
	  return (Attribute) new NumberAttribute("amt_tolerance_pos", "amt_tolerance_pos", false, "TermsBeanAlias.amt_tolerance_pos");
	 }


      /**
        * Attribute factory method for app_debit_acct_charges
        *
        * the Applicant's debit account that will be used to pay any charges related
        * to the instrument
        * @deprecated attribute to be removed after 5.0.0.0 goes to production
        */
	public static Attribute create_app_debit_acct_charges()
	 {
	  return (Attribute) new Attribute("app_debit_acct_charges", "app_debit_acct_charges", false, "TermsBeanAlias.app_debit_acct_charges");
	 }


      /**
        * Attribute factory method for app_debit_acct_principal
        *
        * the Applicant's debit account that will be used for the principal
        * @deprecated attribute to be removed after 5.0.0.0 goes to production
        */
	public static Attribute create_app_debit_acct_principal()
	 {
	  return (Attribute) new Attribute("app_debit_acct_principal", "app_debit_acct_principal", false, "TermsBeanAlias.app_debit_acct_principal");
	 }


      /**
        * Attribute factory method for assignees_account_number
        *
        * The bank account number for the assignee.
        */
	public static Attribute create_assignees_account_number()
	 {
	  return (Attribute) new Attribute("assignees_account_number", "assignees_account_number", false, "TermsBeanAlias.assignees_account_number");
	 }


      /**
        * Attribute factory method for attention_of
        *
        * The person or organisation at the Freight Forwarder / Carrier to whom correspondence
        * should be sent.
        */
	public static Attribute create_attention_of()
	 {
	  return (Attribute) new Attribute("attention_of", "attention_of", false, "TermsBeanAlias.attention_of");
	 }


      /**
        * Attribute factory method for auto_extend
        *
        * If the user wishes that the credit be auto extended, then the check box
        * needs to be selected. The user can then specify the terms of the auto extension
        * for the instrument.
        */
	public static Attribute create_auto_extend()
	 {
	  return (Attribute) new IndicatorAttribute("auto_extend", "auto_extend", false, "TermsBeanAlias.auto_extend");
	 }


      /**
        * Attribute factory method for available_by
        *
        * For letter of credit instruments, this information specifies the availability
        * of the instrument (as defined in Available By, according to SWIFT)
        */
	public static Attribute create_available_by()
	 {
	  return (Attribute) new ReferenceAttribute("available_by", "available_by", "AVAILABLE_BY", false, "TermsBeanAlias.available_by");
	 }
      /**
        * Attribute factory method for available_with_party
        *
        * Identifies the party type with whom the credit is available.
        */
	public static Attribute create_available_with_party()
	 {
	  return (Attribute) new ReferenceAttribute("available_with_party", "available_with_party", "AVAILABLE_WITH_PARTY", false, "TermsBeanAlias.available_with_party");
	 }


      /**
        * Attribute factory method for bank_action_required
        *
        * Indicates whether or not bank action is required for this instrument amendment.
        */
	public static Attribute create_bank_action_required()
	 {
	  return (Attribute) new ReferenceAttribute("bank_action_required", "bank_action_required", "BANK_ACTION_REQ", false, "TermsBeanAlias.bank_action_required");
	 }


      /**
        * Attribute factory method for bank_charges_type
        *
        * Indicates the distribution of bank charges for the instrument.
        */
	public static Attribute create_bank_charges_type()
	 {
	  return (Attribute) new ReferenceAttribute("bank_charges_type", "bank_charges_type", "BANK_CHARGES_TYPE", false, "TermsBeanAlias.bank_charges_type");
	 }


      /**
        * Attribute factory method for branch_code
        *
        * The bank branch code for settlement.
        */
	public static Attribute create_branch_code()
	 {
	  return (Attribute) new Attribute("branch_code", "branch_code", false, "TermsBeanAlias.branch_code");
	 }


      /**
        * Attribute factory method for cert_origin_copies
        *
        * Number of copies of the certificate of origin for this transaction.
        */
	public static Attribute create_cert_origin_copies()
	 {
	  return (Attribute) new NumberAttribute("cert_origin_copies", "cert_origin_copies", false, "TermsBeanAlias.cert_origin_copies");
	 }


      /**
        * Attribute factory method for cert_origin_indicator
        *
        * Indicates if data is included in this transaction for the certificate of
        * origin.
        */
	public static Attribute create_cert_origin_indicator()
	 {
	  return (Attribute) new IndicatorAttribute("cert_origin_indicator", "cert_origin_indicator", false, "TermsBeanAlias.cert_origin_indicator");
	 }


      /**
        * Attribute factory method for cert_origin_originals
        *
        * Number of originals  of the certificate of origin for this transaction.
        */
	public static Attribute create_cert_origin_originals()
	 {
	  return (Attribute) new NumberAttribute("cert_origin_originals", "cert_origin_originals", false, "TermsBeanAlias.cert_origin_originals");
	 }


      /**
        * Attribute factory method for cert_origin_text
        *
        * Text for the certificate of origin for this transaction.
        */
	public static Attribute create_cert_origin_text()
	 {
	  return (Attribute) new Attribute("cert_origin_text", "cert_origin_text", false, "TermsBeanAlias.cert_origin_text");
	 }


      /**
        * Attribute factory method for characteristic_type
        *
        * The type of Guarantee between the Applicant and Beneficiary. For example,
        * Tender/Bid Bond, Performance Guarantee, or Advance Payment
        */
	public static Attribute create_characteristic_type()
	 {
	  return (Attribute) new ReferenceAttribute("characteristic_type", "characteristic_type", "CHARACTERISTIC_TYPE", false, "TermsBeanAlias.characteristic_type");
	 }


      /**
        * Attribute factory method for collect_interest_at
        *
        * If this checkbox is selected, interest is to be collected on outstanding
        * charges according to the interest terms specified in the extended text area.
        */
	public static Attribute create_collect_interest_at()
	 {
	  return (Attribute) new Attribute("collect_interest_at", "collect_interest_at", false, "TermsBeanAlias.collect_interest_at");
	 }


      /**
        * Attribute factory method for collect_interest_at_indicator
        *
        * Indicates that text is being entered in the collect_interest_at attribute
        */
	public static Attribute create_collect_interest_at_indicator()
	 {
	  return (Attribute) new IndicatorAttribute("collect_interest_at_indicator", "collect_interest_at_indicator", false, "TermsBeanAlias.collect_interest_at_indicator");
	 }


      /**
        * Attribute factory method for collection_charges_includ_txt
        *
        * ...
        */
	public static Attribute create_collection_charges_includ_txt()
	 {
	  return (Attribute) new Attribute("collection_charges_includ_txt", "collection_charges_includ_txt", false, "TermsBeanAlias.collection_charges_includ_txt");
	 }


      /**
        * Attribute factory method for collection_charges_type
        *
        * Indicates who pays bank charges.
        */
	public static Attribute create_collection_charges_type()
	 {
	  return (Attribute) new ReferenceAttribute("collection_charges_type", "collection_charges_type", "COLLECTION_CHARGES_TYPE", false, "TermsBeanAlias.collection_charges_type");
	 }


      /**
        * Attribute factory method for collection_date
        *
        * Collection date of the export collection.
        */
	public static Attribute create_collection_date()
	 {
	  return (Attribute) new DateTimeAttribute("collection_date", "collection_date", false, "TermsBeanAlias.collection_date");
	 }


      /**
        * Attribute factory method for comm_invoice_copies
        *
        * Number of copies of the commercial invoice for this transaction.
        */
	public static Attribute create_comm_invoice_copies()
	 {
	  return (Attribute) new NumberAttribute("comm_invoice_copies", "comm_invoice_copies", false, "TermsBeanAlias.comm_invoice_copies");
	 }


      /**
        * Attribute factory method for comm_invoice_indicator
        *
        * Indicates if data is included in this transaction for the commercial invoice.
        */
	public static Attribute create_comm_invoice_indicator()
	 {
	  return (Attribute) new IndicatorAttribute("comm_invoice_indicator", "comm_invoice_indicator", false, "TermsBeanAlias.comm_invoice_indicator");
	 }


      /**
        * Attribute factory method for comm_invoice_originals
        *
        * Number of originals  of the commercial invoice for this transaction.
        */
	public static Attribute create_comm_invoice_originals()
	 {
	  return (Attribute) new NumberAttribute("comm_invoice_originals", "comm_invoice_originals", false, "TermsBeanAlias.comm_invoice_originals");
	 }


      /**
        * Attribute factory method for comm_invoice_text
        *
        * Text for the commercial invoice for this transaction.
        */
	public static Attribute create_comm_invoice_text()
	 {
	  return (Attribute) new Attribute("comm_invoice_text", "comm_invoice_text", false, "TermsBeanAlias.comm_invoice_text");
	 }


      /**
        * Attribute factory method for coms_chrgs_foreign_acct_curr
        *
        * The currency of the commissions and charges foreign currency account.
        */
	public static Attribute create_coms_chrgs_foreign_acct_curr()
	 {
	  return (Attribute) new ReferenceAttribute("coms_chrgs_foreign_acct_curr", "coms_chrgs_foreign_acct_curr", "CURRENCY_CODE", false, "TermsBeanAlias.coms_chrgs_foreign_acct_curr");
	 }


      /**
        * Attribute factory method for coms_chrgs_foreign_acct_num
        *
        * The foreign currency debit account number for commissions and charges.
        */
	public static Attribute create_coms_chrgs_foreign_acct_num()
	 {
	  return (Attribute) new Attribute("coms_chrgs_foreign_acct_num", "coms_chrgs_foreign_acct_num", false, "TermsBeanAlias.coms_chrgs_foreign_acct_num");
	 }


      /**
        * Attribute factory method for coms_chrgs_other_text
        *
        * The additional text describing settlement or commissions and charges for
        * the instrument.
        */
	public static Attribute create_coms_chrgs_other_text()
	 {
	  return (Attribute) new Attribute("coms_chrgs_other_text", "coms_chrgs_other_text", false, "TermsBeanAlias.coms_chrgs_other_text");
	 }


      /**
        * Attribute factory method for coms_chrgs_our_account_num
        *
        * The debit account number for commissions and charges.
        */
	public static Attribute create_coms_chrgs_our_account_num()
	 {
	  return (Attribute) new Attribute("coms_chrgs_our_account_num", "coms_chrgs_our_account_num", false, "TermsBeanAlias.coms_chrgs_our_account_num");
	 }


      /**
        * Attribute factory method for confirmation_indicator
        *
        * Determines whether the Instrument has been confirmed
        *
        * Yes=Instrument has been confirmed
        */
	public static Attribute create_confirmation_indicator()
	 {
	  return (Attribute) new IndicatorAttribute("confirmation_indicator", "confirmation_indicator");
	 }


      /**
        * Attribute factory method for confirmation_type
        *
        * Indicates the rules for Advising Bank confirmation.
        */
	public static Attribute create_confirmation_type()
	 {
	  return (Attribute) new ReferenceAttribute("confirmation_type", "confirmation_type", "CONFIRMATION_TYPE", false, "TermsBeanAlias.confirmation_type");
	 }


      /**
        * Attribute factory method for covered_by_fec_number
        *
        * The forward exchange contract (FEC) covering the settlement, if the instrument
        * is in a currency other than the Applicant's base currency.
        */
	public static Attribute create_covered_by_fec_number()
	 {
	  return (Attribute) new Attribute("covered_by_fec_number", "covered_by_fec_number", false, "TermsBeanAlias.covered_by_fec_number");
	 }


      /**
        * Attribute factory method for discount_rate
        *
        * The discount rate applied to the LC
        */
	public static Attribute create_discount_rate()
	 {
	  return (Attribute) new TradePortalDecimalAttribute("discount_rate", "discount_rate");
	 }


      /**
        * Attribute factory method for do_not_waive_refused_charges
        *
        * ...
        */
	public static Attribute create_do_not_waive_refused_charges()
	 {
	  return (Attribute) new IndicatorAttribute("do_not_waive_refused_charges", "do_not_waive_refused_charges", false, "TermsBeanAlias.do_not_waive_refused_charges");
	 }


      /**
        * Attribute factory method for do_not_waive_refused_interest
        *
        * If this checkbox is selected, interest should not be waived if the party
        * refuses to pay it. If this checkbox is left cleared, interest charges will
        * be waived if a party refuses to pay them.
        */
	public static Attribute create_do_not_waive_refused_interest()
	 {
	  return (Attribute) new IndicatorAttribute("do_not_waive_refused_interest", "do_not_waive_refused_interest", false, "TermsBeanAlias.do_not_waive_refused_interest");
	 }


      /**
        * Attribute factory method for drafts_required
        *
        * An indicator if drafts are required at presentation of documents under the
        * terms of the instrument.
        */
	public static Attribute create_drafts_required()
	 {
	  return (Attribute) new IndicatorAttribute("drafts_required", "drafts_required", false, "TermsBeanAlias.drafts_required");
	 }


      /**
        * Attribute factory method for drawing_number
        *
        * The drawing number represented by this transaction
        */
	public static Attribute create_drawing_number()
	 {
	  return (Attribute) new Attribute("drawing_number", "drawing_number");
	 }


      /**
        * Attribute factory method for drawn_on_party
        *
        * The party on whom the drafts are drawn.  The financial terms of the instrument
        * can have a drawn on party relationship with one of the parties to the LC
        * (or the drawn on party can be the bank itself or a negotiating bank).
        */
	public static Attribute create_drawn_on_party()
	 {
	  return (Attribute) new ReferenceAttribute("drawn_on_party", "drawn_on_party", "DRAWN_ON_PARTY", false, "TermsBeanAlias.drawn_on_party");
	 }


      /**
        * Attribute factory method for ex_rt_variation_adjust
        *
        * Indicates how variations in the exchange rate will be accommodated
        */
	public static Attribute create_ex_rt_variation_adjust()
	 {
	  return (Attribute) new ReferenceAttribute("ex_rt_variation_adjust", "ex_rt_variation_adjust", "EXCH_RT_VARIATION", false, "TermsBeanAlias.ex_rt_variation_adjust");
	 }


      /**
        * Attribute factory method for expiry_date
        *
        * The date that the instrument expires. To enter the expiry date, select a
        * day, month, and year from the appropriate drop-down lists.
        */
	public static Attribute create_expiry_date()
	 {
	  return (Attribute) new DateTimeAttribute("expiry_date", "expiry_date", false, "TermsBeanAlias.expiry_date");
	 }


      /**
        * Attribute factory method for export_discrepancy_instr
        *
        * Indicates how to resolve the discrepancy described in the discrepancy notice
        */
	public static Attribute create_export_discrepancy_instr()
	 {
	  return (Attribute) new ReferenceAttribute("export_discrepancy_instr", "export_discrepancy_instr", "EXPORT_DISCR_INSTR_TYPE");
	 }


      /**
        * Attribute factory method for fax_number
        *
        * The fax number of the Freight Forwarder / Carrier for the instrument.
        */
	public static Attribute create_fax_number()
	 {
	  return (Attribute) new Attribute("fax_number", "fax_number", false, "TermsBeanAlias.fax_number");
	 }


      /**
        * Attribute factory method for fec_amount
        *
        * The amount of the forward exchange contract FEC.
        */
	public static Attribute create_fec_amount()
	 {
	  return (Attribute) new TradePortalDecimalAttribute("fec_amount", "fec_amount", false, "TermsBeanAlias.fec_amount");
	 }


      /**
        * Attribute factory method for fec_maturity_date
        *
        * The maturity date for the forward exchange contract.
        */
	public static Attribute create_fec_maturity_date()
	 {
	  return (Attribute) new DateTimeAttribute("fec_maturity_date", "fec_maturity_date", false, "TermsBeanAlias.fec_maturity_date");
	 }


      /**
        * Attribute factory method for fec_rate
        *
        * The rate of the FEC (forward exchange contract)
        */
	public static Attribute create_fec_rate()
	 {
	  return (Attribute) new TradePortalDecimalAttribute("fec_rate", "fec_rate", false, "TermsBeanAlias.fec_rate");
	 }


    //MDB LAUL121661529 Rel7.1 12/21/11 - Begin
	public static Attribute create_fx_rate()
	 {
	  return (Attribute) new TradePortalDecimalAttribute("fec_rate", "fec_rate", false, "TransferBetweenAccounts.FECRate");
	 }
	//MDB LAUL121661529 Rel7.1 12/21/11 - End


      /**
        * Attribute factory method for finance_drawing
        *
        * Indicates if financing has been arranged for the Applicant.
        */
	public static Attribute create_finance_drawing()
	 {
	  return (Attribute) new IndicatorAttribute("finance_drawing", "finance_drawing", false, "TermsBeanAlias.finance_drawing");
	 }


      /**
        * Attribute factory method for finance_drawing_num_days
        *
        * The finance drawing period for the instrument
        */
	public static Attribute create_finance_drawing_num_days()
	 {
	  return (Attribute) new NumberAttribute("finance_drawing_num_days", "finance_drawing_num_days", false, "TermsBeanAlias.finance_drawing_num_days");
	 }


      /**
        * Attribute factory method for finance_drawing_type
        *
        * The finance drawing type for the instrument
        */
	public static Attribute create_finance_drawing_type()
	 {
	  return (Attribute) new ReferenceAttribute("finance_drawing_type", "finance_drawing_type", "FINANCE_DRAWING_TYPE", false, "TermsBeanAlias.finance_drawing_type");
	 }


      /**
        * Attribute factory method for finance_type
        *
        * Code for the finance type, i.e., type of transaction that a LRQ is financing.
        */
	public static Attribute create_finance_type()
	 {
	  return (Attribute) new ReferenceAttribute("finance_type", "finance_type", "FINANCE_TYPE", false, "TermsBeanAlias.finance_type");
	 }


      /**
        * Attribute factory method for finance_type_text
        *
        * Finance Type text - the text entered when the Financed transaction type
        * is OTHER.
        */
	public static Attribute create_finance_type_text()
	 {
	  return (Attribute) new Attribute("finance_type_text", "finance_type_text", false, "TermsBeanAlias.finance_type_text");
	 }


      /**
        * Attribute factory method for first_shipment_oid
        *
        * Pointer to the first shipment of a Terms object.  Used for reporting.
        */
	public static Attribute create_first_shipment_oid()
	 {
	  return (Attribute) new NumberAttribute("first_shipment_oid", "first_shipment_oid", false, "TermsBeanAlias.first_shipment_oid");
	 }


      /**
        * Attribute factory method for funds_xfer_settle_type
        *
        * Indicates the source of the funds to be paid as part of the funds transfer
        */
	public static Attribute create_funds_xfer_settle_type()
	 {
	  return (Attribute) new ReferenceAttribute("funds_xfer_settle_type", "funds_xfer_settle_type", "FUNDS_XFER_SETTLE", false, "TermsBeanAlias.funds_xfer_settle_type");
	 }


      /**
        * Attribute factory method for guar_accept_instr
        *
        * If this checkbox is selected, the Agent can modify details of the original
        * instructions for this Guarantee.
        */
	public static Attribute create_guar_accept_instr()
	 {
	  return (Attribute) new IndicatorAttribute("guar_accept_instr", "guar_accept_instr", false, "TermsBeanAlias.guar_accept_instr");
	 }


      /**
        * Attribute factory method for guar_advise_issuance
        *
        * If this checkbox is selected, notify the Agent that the Guarantee has been
        * issued, but do not deliver the Guarantee to him or her.
        */
	public static Attribute create_guar_advise_issuance()
	 {
	  return (Attribute) new IndicatorAttribute("guar_advise_issuance", "guar_advise_issuance", false, "TermsBeanAlias.guar_advise_issuance");
	 }


      /**
        * Attribute factory method for guar_bank_standard_text
        *
        * The actual wording of the Guarantee that will be issued. Data must be entered
        * for either  the Guarantee text here or in the Customer Textarea (but not
        * in both). To enter text in the Bank Standard Wording area, the user must
        * add a phrase from the drop-down list to the text area. If the user adds
        * a phrase that requires you to fill in gaps, you will be prompted to enter
        * all variable text items and press Submit to add the entered text items to
        * the text area.
        */
	public static Attribute create_guar_bank_standard_text()
	 {
	  return (Attribute) new Attribute("guar_bank_standard_text", "guar_bank_standard_text", false, "TermsBeanAlias.guar_bank_standard_text");
	 }


      /**
        * Attribute factory method for guar_bank_std_phrase_type
        *
        * Indicates which type of phrase was added in the bank standard wording area.
        * This is set by the system, not the user.
        */
	public static Attribute create_guar_bank_std_phrase_type()
	 {
	  return (Attribute) new ReferenceAttribute("guar_bank_std_phrase_type", "guar_bank_std_phrase_type", "PHRASE_TYPE");
	 }


      /**
        * Attribute factory method for guar_customer_text
        *
        * The actual wording of the Guarantee that will be issued. Data must be entered
        * for either enter the Guarantee text here or in the Bank Standard Wording
        * text area (but not in both).
        */
	public static Attribute create_guar_customer_text()
	 {
	  return (Attribute) new Attribute("guar_customer_text", "guar_customer_text", false, "TermsBeanAlias.guar_customer_text");
	 }


      /**
        * Attribute factory method for guar_deliver_by
        *
        * Indicates the method by which the Guarantee is to be delivered
        */
	public static Attribute create_guar_deliver_by()
	 {
	  return (Attribute) new ReferenceAttribute("guar_deliver_by", "guar_deliver_by", "DELIVER_BY", false, "TermsBeanAlias.guar_deliver_by");
	 }


      /**
        * Attribute factory method for guar_deliver_to
        *
        * Indicates who will take delivery of and acknowledge the physical Guarantee
        */
	public static Attribute create_guar_deliver_to()
	 {
	  return (Attribute) new ReferenceAttribute("guar_deliver_to", "guar_deliver_to", "DELIVER_TO", false, "TermsBeanAlias.guar_deliver_to");
	 }


      /**
        * Attribute factory method for guar_expiry_date_type
        *
        * Indicates the date until which the Guarantee is valid.
        */
	public static Attribute create_guar_expiry_date_type()
	 {
	  return (Attribute) new ReferenceAttribute("guar_expiry_date_type", "guar_expiry_date_type", "GUAR_EXPIRY_DATE_TYPE", false, "TermsBeanAlias.guar_expiry_date_type");
	 }


      /**
        * Attribute factory method for guar_valid_from_date
        *
        * The date from which the guarantee is valid if 'Other' is selected for the
        * guar_valid_from_date_tye
        */
	public static Attribute create_guar_valid_from_date()
	 {
	  return (Attribute) new DateTimeAttribute("guar_valid_from_date", "guar_valid_from_date", false, "TermsBeanAlias.guar_valid_from_date");
	 }


      /**
        * Attribute factory method for guar_valid_from_date_type
        *
        * Indicates the date from which the Guarantee is valid.
        */
	public static Attribute create_guar_valid_from_date_type()
	 {
	  return (Attribute) new ReferenceAttribute("guar_valid_from_date_type", "guar_valid_from_date_type", "GUAR_VALID_FROM_DATE_TYPE", false, "TermsBeanAlias.guar_valid_from_date_type");
	 }


      /**
        * Attribute factory method for guarantee_issue_type
        *
        * Indicates the bank issuing the Guarantee. To specify, select either Applicant's
        * Bank or Overseas Bank.
        */
	public static Attribute create_guarantee_issue_type()
	 {
	  return (Attribute) new ReferenceAttribute("guarantee_issue_type", "guarantee_issue_type", "GUARANTEE_ISSUE_TYPE", false, "TermsBeanAlias.guarantee_issue_type");
	 }


      /**
        * Attribute factory method for import_discrepancy_instr
        *
        * Indicates how to resolve the discrepancy described in the discrepancy notice
        */
	public static Attribute create_import_discrepancy_instr()
	 {
	  return (Attribute) new ReferenceAttribute("import_discrepancy_instr", "import_discrepancy_instr", "IMPORT_DISCR_INSTR_TYPE");
	 }


      /**
        * Attribute factory method for in_advance_disposition
        *
        *
        */
	public static Attribute create_in_advance_disposition()
	 {
	  return (Attribute) new ReferenceAttribute("in_advance_disposition", "in_advance_disposition", "IN_ADVANCE_DISP_TYPE", false, "TermsBeanAlias.in_advance_disposition");
	 }


      /**
        * Attribute factory method for in_arrears_disposition
        *
        *
        */
	public static Attribute create_in_arrears_disposition()
	 {
	  return (Attribute) new ReferenceAttribute("in_arrears_disposition", "in_arrears_disposition", "IN_ARREARS_DISP_TYPE", false, "TermsBeanAlias.in_arrears_disposition");
	 }


      /**
        * Attribute factory method for in_case_of_need_contact
        *
        * Indicates whether or not a case of need contact for the instrument has been
        * selected from a list of non-bank parties
        */
	public static Attribute create_in_case_of_need_contact()
	 {
	  return (Attribute) new IndicatorAttribute("in_case_of_need_contact", "in_case_of_need_contact");
	 }


      /**
        * Attribute factory method for in_case_of_need_contact_text
        *
        * Any Case of Need instructions for the instrument.
        */
	public static Attribute create_in_case_of_need_contact_text()
	 {
	  return (Attribute) new Attribute("in_case_of_need_contact_text", "in_case_of_need_contact_text", false, "TermsBeanAlias.in_case_of_need_contact_text");
	 }


      /**
        * Attribute factory method for in_case_of_need_use_instruct
        *
        * Indicates whether instructions should be accepted from this case of need
        * party or if they are just for guidance
        */
	public static Attribute create_in_case_of_need_use_instruct()
	 {
	  return (Attribute) new ReferenceAttribute("in_case_of_need_use_instruct", "in_case_of_need_use_instruct", "IN_CASE_OF_NEED", false, "TermsBeanAlias.in_case_of_need_use_instruct");
	 }


      /**
        * Attribute factory method for ins_policy_copies
        *
        * Number of copies of the insurance policy for this transaction.
        */
	public static Attribute create_ins_policy_copies()
	 {
	  return (Attribute) new NumberAttribute("ins_policy_copies", "ins_policy_copies", false, "TermsBeanAlias.ins_policy_copies");
	 }


      /**
        * Attribute factory method for ins_policy_indicator
        *
        * Indicates if data is included in this transaction for the insurance policy.
        */
	public static Attribute create_ins_policy_indicator()
	 {
	  return (Attribute) new IndicatorAttribute("ins_policy_indicator", "ins_policy_indicator", false, "TermsBeanAlias.ins_policy_indicator");
	 }


      /**
        * Attribute factory method for ins_policy_originals
        *
        * Number of originals of the insurance policy for this transaction.
        */
	public static Attribute create_ins_policy_originals()
	 {
	  return (Attribute) new NumberAttribute("ins_policy_originals", "ins_policy_originals", false, "TermsBeanAlias.ins_policy_originals");
	 }


      /**
        * Attribute factory method for ins_policy_text
        *
        * Text for the insurance policy for this transaction.
        */
	public static Attribute create_ins_policy_text()
	 {
	  return (Attribute) new Attribute("ins_policy_text", "ins_policy_text", false, "TermsBeanAlias.ins_policy_text");
	 }


      /**
        * Attribute factory method for instr_advise_accept_by_telco
        *
        * The collection instructions for the instrument with regards to advising
        * acceptance by telecommunications
        */
	public static Attribute create_instr_advise_accept_by_telco()
	 {
	  return (Attribute) new IndicatorAttribute("instr_advise_accept_by_telco", "instr_advise_accept_by_telco", false, "TermsBeanAlias.instr_advise_accept_by_telco");
	 }


      /**
        * Attribute factory method for instr_advise_non_accept_by_tel
        *
        * The collection instructions for the instrument with regards to advising
        * non acceptance by telecommunications
        */
	public static Attribute create_instr_advise_non_accept_by_tel()
	 {
	  return (Attribute) new IndicatorAttribute("instr_advise_non_accept_by_tel", "instr_advise_non_accept_by_tel", false, "TermsBeanAlias.instr_advise_non_accept_by_tel");
	 }


      /**
        * Attribute factory method for instr_advise_non_pmt_by_telco
        *
        * The collection instructions for the instrument with regards to advising
        * non payment by telecommunications
        */
	public static Attribute create_instr_advise_non_pmt_by_telco()
	 {
	  return (Attribute) new IndicatorAttribute("instr_advise_non_pmt_by_telco", "instr_advise_non_pmt_by_telco", false, "TermsBeanAlias.instr_advise_non_pmt_by_telco");
	 }


      /**
        * Attribute factory method for instr_advise_pmt_by_telco
        *
        * The collection instructions for the instrument with regards to advising
        * payment by telecommunications
        */
	public static Attribute create_instr_advise_pmt_by_telco()
	 {
	  return (Attribute) new IndicatorAttribute("instr_advise_pmt_by_telco", "instr_advise_pmt_by_telco", false, "TermsBeanAlias.instr_advise_pmt_by_telco");
	 }


      /**
        * Attribute factory method for instr_note_for_non_accept
        *
        * The collection instructions for the instrument with regards to note for
        * non acceptance
        */
	public static Attribute create_instr_note_for_non_accept()
	 {
	  return (Attribute) new IndicatorAttribute("instr_note_for_non_accept", "instr_note_for_non_accept", false, "TermsBeanAlias.instr_note_for_non_accept");
	 }


      /**
        * Attribute factory method for instr_note_for_non_pmt
        *
        * The collection instructions for the instrument with regards to note for
        * non payment
        */
	public static Attribute create_instr_note_for_non_pmt()
	 {
	  return (Attribute) new IndicatorAttribute("instr_note_for_non_pmt", "instr_note_for_non_pmt", false, "TermsBeanAlias.instr_note_for_non_pmt");
	 }


      /**
        * Attribute factory method for instr_other
        *
        * Any additional collection instructions for the instrument.
        */
	public static Attribute create_instr_other()
	 {
	  return (Attribute) new Attribute("instr_other", "instr_other", false, "TermsBeanAlias.instr_other");
	 }


      /**
        * Attribute factory method for instr_protest_for_non_accept
        *
        * The collection instructions for the instrument with regards to protest for
        * non acceptance
        */
	public static Attribute create_instr_protest_for_non_accept()
	 {
	  return (Attribute) new IndicatorAttribute("instr_protest_for_non_accept", "instr_protest_for_non_accept", false, "TermsBeanAlias.instr_protest_for_non_accept");
	 }


      /**
        * Attribute factory method for instr_protest_for_non_pmt
        *
        * The collection instructions for the instrument with regards to protest for
        * non payment
        */
	public static Attribute create_instr_protest_for_non_pmt()
	 {
	  return (Attribute) new IndicatorAttribute("instr_protest_for_non_pmt", "instr_protest_for_non_pmt", false, "TermsBeanAlias.instr_protest_for_non_pmt");
	 }


      /**
        * Attribute factory method for instr_release_doc_on_accept
        *
        * The collection instructions for the instrument with regards to release document
        * on acceptance
        */
	public static Attribute create_instr_release_doc_on_accept()
	 {
	  return (Attribute) new IndicatorAttribute("instr_release_doc_on_accept", "instr_release_doc_on_accept", false, "TermsBeanAlias.instr_release_doc_on_accept");
	 }


      /**
        * Attribute factory method for instr_release_doc_on_pmt
        *
        * The collection instructions for the instrument with regards to release document
        * on payment
        */
	public static Attribute create_instr_release_doc_on_pmt()
	 {
	  return (Attribute) new IndicatorAttribute("instr_release_doc_on_pmt", "instr_release_doc_on_pmt", false, "TermsBeanAlias.instr_release_doc_on_pmt");
	 }


      /**
        * Attribute factory method for insurance_endorse_value_plus
        *
        * For the Insurance Policy / Certificate document, the endorsement value must
        * be entered
        */
	public static Attribute create_insurance_endorse_value_plus()
	 {
	  return (Attribute) new TradePortalDecimalAttribute("insurance_endorse_value_plus", "insurance_endorse_value_plus", false, "TermsBeanAlias.insurance_endorse_value_plus");
	 }


      /**
        * Attribute factory method for insurance_risk_type
        *
        * For the Insurance Policy / Certificate document, the insurance coverage
        * type must be entered
        */
	public static Attribute create_insurance_risk_type()
	 {
	  return (Attribute) new ReferenceAttribute("insurance_risk_type", "insurance_risk_type", "INSURANCE_RISK_TYPE", false, "TermsBeanAlias.insurance_risk_type");
	 }


      /**
        * Attribute factory method for interest_debit_acct_num
        *
        *
        */
	public static Attribute create_interest_debit_acct_num()
	 {
	  return (Attribute) new Attribute("interest_debit_acct_num", "interest_debit_acct_num", false, "TermsBeanAlias.interest_debit_acct_num");
	 }


      /**
        * Attribute factory method for interest_to_be_paid
        *
        * Indicates when interest accrued during the loan period will be paid
        */
	public static Attribute create_interest_to_be_paid()
	 {
	  return (Attribute) new ReferenceAttribute("interest_to_be_paid", "interest_to_be_paid", "INTEREST_PAID_TYPE", false, "TermsBeanAlias.interest_to_be_paid");
	 }


      /**
        * Attribute factory method for internal_instructions
        *
        * Instructions that are internal to the corporate customer creating the instrument.
        * These are not sent to the bank
        */
	public static Attribute create_internal_instructions()
	 {
	  return (Attribute) new Attribute("internal_instructions", "internal_instructions", false, "TermsBeanAlias.internal_instructions");
	 }


      /**
        * Attribute factory method for irrevocable
        *
        * An indicator of whether the Letter of Credit is revocable or irrevocable.
        */
	public static Attribute create_irrevocable()
	 {
	  return (Attribute) new IndicatorAttribute("irrevocable", "irrevocable", false, "TermsBeanAlias.irrevocable");
	 }


      /**
        * Attribute factory method for issuer_ref_num
        *
        * Issuer's Reference Number (General Section) - this will display the Issuer's
        * Reference Number.
        */
	public static Attribute create_issuer_ref_num()
	 {
	  return (Attribute) new Attribute("issuer_ref_num", "issuer_ref_num", false, "TermsBeanAlias.issuer_ref_num");
	 }


      /**
        * Attribute factory method for linked_instrument_id
        *
        * The instrument id that an LRQ is financing.
        */
	public static Attribute create_linked_instrument_id()
	 {
	  return (Attribute) new Attribute("linked_instrument_id", "linked_instrument_id", false, "TermsBeanAlias.linked_instrument_id");
	 }


      /**
        * Attribute factory method for linked_instrument_type
        *
        * Type of the instrument that an LRQ is financing
        */
	public static Attribute create_linked_instrument_type()
	 {
	  return (Attribute) new ReferenceAttribute("linked_instrument_type", "linked_instrument_type", "INSTRUMENT_TYPE");
	 }


      /**
        * Attribute factory method for linked_tran_seq_num
        *
        * Sequence number of the transaction that an LRQ is financing.
        */
	public static Attribute create_linked_tran_seq_num()
	 {
	  return (Attribute) new NumberAttribute("linked_tran_seq_num", "linked_tran_seq_num");
	 }


      /**
        * Attribute factory method for linked_transaction_type
        *
        * Transaction type that an LRQ is financing
        */
	public static Attribute create_linked_transaction_type()
	 {
	  return (Attribute) new ReferenceAttribute("linked_transaction_type", "linked_transaction_type", "TRANSACTION_TYPE");
	 }


      /**
        * Attribute factory method for loan_maturity_debit_acct
        *
        * the account that will be used to repay the loan.
        */
	public static Attribute create_loan_maturity_debit_acct()
	 {
	  return (Attribute) new Attribute("loan_maturity_debit_acct", "loan_maturity_debit_acct", false, "TermsBeanAlias.loan_maturity_debit_acct");
	 }


      /**
        * Attribute factory method for loan_maturity_debit_other_txt
        *
        * other arrangements that have been made to repay the loan
        */
	public static Attribute create_loan_maturity_debit_other_txt()
	 {
	  return (Attribute) new Attribute("loan_maturity_debit_other_txt", "loan_maturity_debit_other_txt", false, "TermsBeanAlias.loan_maturity_debit_other_txt");
	 }


      /**
        * Attribute factory method for loan_maturity_debit_type
        *
        * Indicates how the loan will be repaid at loan maturity.
        */
	public static Attribute create_loan_maturity_debit_type()
	 {
	  return (Attribute) new ReferenceAttribute("loan_maturity_debit_type", "loan_maturity_debit_type", "LOAN_MATURITY_TYPE", false, "TermsBeanAlias.loan_maturity_debit_type");
	 }


      /**
        * Attribute factory method for loan_proceeds_credit_acct
        *
        * the account that the loan proceeds are to be credited to
        */
	public static Attribute create_loan_proceeds_credit_acct()
	 {
	  return (Attribute) new Attribute("loan_proceeds_credit_acct", "loan_proceeds_credit_acct", false, "TermsBeanAlias.loan_proceeds_credit_acct");
	 }


      /**
        * Attribute factory method for loan_proceeds_credit_other_txt
        *
        * other arrangements made for the loan proceeds
        */
	public static Attribute create_loan_proceeds_credit_other_txt()
	 {
	  return (Attribute) new Attribute("loan_proceeds_credit_other_txt", "loan_proceeds_credit_other_txt", false, "TermsBeanAlias.loan_proceeds_credit_other_txt");
	 }


      /**
        * Attribute factory method for loan_proceeds_credit_type
        *
        * Indicates how the loan proceeds will be paid/disbursed
        */
	public static Attribute create_loan_proceeds_credit_type()
	 {
	  return (Attribute) new ReferenceAttribute("loan_proceeds_credit_type", "loan_proceeds_credit_type", "SETTLE_INSTR_TYPE", false, "TermsBeanAlias.loan_proceeds_credit_type");
	 }


      /**
        * Attribute factory method for loan_proceeds_pmt_amount
        *
        * The amount of the loan.
        */
	public static Attribute create_loan_proceeds_pmt_amount()
	 {
	  return (Attribute) new TradePortalDecimalAttribute("loan_proceeds_pmt_amount", "loan_proceeds_pmt_amount", false, "TermsBeanAlias.loan_proceeds_pmt_amount");
	 }


      /**
        * Attribute factory method for loan_proceeds_pmt_curr
        *
        * the currency of the loan
        */
	public static Attribute create_loan_proceeds_pmt_curr()
	 {
	  return (Attribute) new ReferenceAttribute("loan_proceeds_pmt_curr", "loan_proceeds_pmt_curr", "CURRENCY_CODE", false, "TermsBeanAlias.loan_proceeds_pmt_curr");
	 }


      /**
        * Attribute factory method for loan_rate
        *
        * rate of the loan
        */
	public static Attribute create_loan_rate()
	 {
	  return (Attribute) new TradePortalDecimalAttribute("loan_rate", "loan_rate", false, "TermsBeanAlias.loan_rate");
	 }


      /**
        * Attribute factory method for loan_start_date
        *
        * The date when the loan will begin.
        */
	public static Attribute create_loan_start_date()
	 {
	  return (Attribute) new DateTimeAttribute("loan_start_date", "loan_start_date", false, "TermsBeanAlias.loan_start_date");
	 }


      /**
        * Attribute factory method for loan_terms_fixed_maturity_dt
        *
        * The loan matures at a fixed date
        */
	public static Attribute create_loan_terms_fixed_maturity_dt()
	 {
	  return (Attribute) new DateTimeAttribute("loan_terms_fixed_maturity_dt", "loan_terms_fixed_maturity_dt", false, "TermsBeanAlias.loan_terms_fixed_maturity_dt");
	 }


      /**
        * Attribute factory method for loan_terms_number_of_days
        *
        * a certain number of days after the Loan Start Date
        */
	public static Attribute create_loan_terms_number_of_days()
	 {
	  return (Attribute) new NumberAttribute("loan_terms_number_of_days", "loan_terms_number_of_days", false, "TermsBeanAlias.loan_terms_number_of_days");
	 }


      /**
        * Attribute factory method for loan_terms_type
        *
        * Indicates when presentations will be payable under the terms of the instrument.
        */
	public static Attribute create_loan_terms_type()
	 {
	  return (Attribute) new ReferenceAttribute("loan_terms_type", "loan_terms_type", "LOAN_TERMS_TYPE", false, "TermsBeanAlias.loan_terms_type");
	 }


      /**
        * Attribute factory method for maturity_covered_by_fec_number
        *
        * The Forward Exchange Contract (FEC) covering the settlement, if the instrument
        * is in a currency other than the Applicant's base currency.
        */
	public static Attribute create_maturity_covered_by_fec_number()
	 {
	  return (Attribute) new Attribute("maturity_covered_by_fec_number", "maturity_covered_by_fec_number", false, "TermsBeanAlias.maturity_covered_by_fec_number");
	 }


      /**
        * Attribute factory method for maturity_fec_amount
        *
        * The amount of the FEC
        */
	public static Attribute create_maturity_fec_amount()
	 {
	  return (Attribute) new TradePortalDecimalAttribute("maturity_fec_amount", "maturity_fec_amount", false, "TermsBeanAlias.maturity_fec_amount");
	 }


      /**
        * Attribute factory method for maturity_fec_maturity_date
        *
        * The maturity date for the forward exchange contract.
        */
	public static Attribute create_maturity_fec_maturity_date()
	 {
	  return (Attribute) new DateTimeAttribute("maturity_fec_maturity_date", "maturity_fec_maturity_date", false, "TermsBeanAlias.maturity_fec_maturity_date");
	 }


      /**
        * Attribute factory method for maturity_fec_rate
        *
        * The rate of the FEC
        */
	public static Attribute create_maturity_fec_rate()
	 {
	  return (Attribute) new TradePortalDecimalAttribute("maturity_fec_rate", "maturity_fec_rate", false, "TermsBeanAlias.maturity_fec_rate");
	 }


      /**
        * Attribute factory method for maturity_use_fec
        *
        * Selecting this check box indicates that a Forward Exchange Contract (FEC)
        * will be referenced for the instrument
        */
	public static Attribute create_maturity_use_fec()
	 {
	  return (Attribute) new IndicatorAttribute("maturity_use_fec", "maturity_use_fec");
	 }


      /**
        * Attribute factory method for maturity_use_mkt_rate
        *
        * Selecting this check box indicates that a spot/market rate will be used
        * for payment of the loan at loan maturity
        */
	public static Attribute create_maturity_use_mkt_rate()
	 {
	  return (Attribute) new IndicatorAttribute("maturity_use_mkt_rate", "maturity_use_mkt_rate");
	 }


      /**
        * Attribute factory method for maturity_use_other
        *
        * Select this check box and enter any additional exchange rate details
        */
	public static Attribute create_maturity_use_other()
	 {
	  return (Attribute) new IndicatorAttribute("maturity_use_other", "maturity_use_other");
	 }


      /**
        * Attribute factory method for maturity_use_other_text
        *
        * Text related to maturity_use_other
        */
	public static Attribute create_maturity_use_other_text()
	 {
	  return (Attribute) new Attribute("maturity_use_other_text", "maturity_use_other_text", false, "TermsBeanAlias.maturity_use_other_text");
	 }


      /**
        * Attribute factory method for maximum_credit_amount
        *
        * Maximum Credit Amount (General section) - this will be a radio button selection
        * with one valid preloaded value - "Not Exceeding". In accordance with SWIFT
        * standards, either this "Not Exceeding" value or a Percentage Amount Tolerance
        * can be defined for an LC during an amendment, if necessary. Therefore, the
        * system will impose that either this field is selected via its associated
        * radio button or a numeric value is entered in the Amount Tolerance fields,
        * in which case the associated radio button will automatically become selected
        * by the system.  It is possible for an amendment that neither of these are
        * fields are selected, since they may not be being amended.  They will not
        * be mandatory fields for an amendment transaction.
        */
	public static Attribute create_maximum_credit_amount()
	 {
	  return (Attribute) new Attribute("maximum_credit_amount", "maximum_credit_amount", false, "TermsBeanAlias.maximum_credit_amount");
	 }


      /**
        * Attribute factory method for message_to_beneficiary
        *
        * a message for the Beneficiary
        */
	public static Attribute create_message_to_beneficiary()
	 {
	  return (Attribute) new Attribute("message_to_beneficiary", "message_to_beneficiary", false, "TermsBeanAlias.message_to_beneficiary");
	 }


      /**
        * Attribute factory method for multiple_related_instruments
        *
        * a list of several related instruments
        */
	public static Attribute create_multiple_related_instruments()
	 {
	  return (Attribute) new Attribute("multiple_related_instruments", "multiple_related_instruments", false, "TermsBeanAlias.multiple_related_instruments");
	 }


      /**
        * Attribute factory method for opening_bank_instr_num
        *
        * The instrument reference number of the opening bank.
        */
	public static Attribute create_opening_bank_instr_num()
	 {
	  return (Attribute) new Attribute("opening_bank_instr_num", "opening_bank_instr_num", false, "TermsBeanAlias.opening_bank_instr_num");
	 }


      /**
        * Attribute factory method for operative
        *
        * An indicator of whether the instrument is operative or inoperative.
        */
	public static Attribute create_operative()
	 {
	  return (Attribute) new IndicatorAttribute("operative", "operative", false, "TermsBeanAlias.operative");
	 }


      /**
        * Attribute factory method for other_req_doc_1_copies
        *
        * Number of copies of the other required document #4 for this transaction.
        */
	public static Attribute create_other_req_doc_1_copies()
	 {
	  return (Attribute) new NumberAttribute("other_req_doc_1_copies", "other_req_doc_1_copies", false, "TermsBeanAlias.other_req_doc_1_copies");
	 }


      /**
        * Attribute factory method for other_req_doc_1_indicator
        *
        * Indicates if data is included in this transaction for the other required
        * document #4.
        */
	public static Attribute create_other_req_doc_1_indicator()
	 {
	  return (Attribute) new IndicatorAttribute("other_req_doc_1_indicator", "other_req_doc_1_indicator", false, "TermsBeanAlias.other_req_doc_1_indicator");
	 }


      /**
        * Attribute factory method for other_req_doc_1_name
        *
        * The name of the other required document #4.
        */
	public static Attribute create_other_req_doc_1_name()
	 {
	  return (Attribute) new Attribute("other_req_doc_1_name", "other_req_doc_1_name", false, "TermsBeanAlias.other_req_doc_1_name");
	 }


      /**
        * Attribute factory method for other_req_doc_1_originals
        *
        * Number of originals  of the other required document #4 for this transaction.
        */
	public static Attribute create_other_req_doc_1_originals()
	 {
	  return (Attribute) new NumberAttribute("other_req_doc_1_originals", "other_req_doc_1_originals", false, "TermsBeanAlias.other_req_doc_1_originals");
	 }


      /**
        * Attribute factory method for other_req_doc_1_text
        *
        * Text for the other required document #4 for this transaction.
        */
	public static Attribute create_other_req_doc_1_text()
	 {
	  return (Attribute) new Attribute("other_req_doc_1_text", "other_req_doc_1_text", false, "TermsBeanAlias.other_req_doc_1_text");
	 }


      /**
        * Attribute factory method for other_req_doc_2_copies
        *
        * Number of copies of the other required document #3 for this transaction.
        */
	public static Attribute create_other_req_doc_2_copies()
	 {
	  return (Attribute) new NumberAttribute("other_req_doc_2_copies", "other_req_doc_2_copies", false, "TermsBeanAlias.other_req_doc_2_copies");
	 }


      /**
        * Attribute factory method for other_req_doc_2_indicator
        *
        * Indicates if data is included in this transaction for the other required
        * document #3.
        */
	public static Attribute create_other_req_doc_2_indicator()
	 {
	  return (Attribute) new IndicatorAttribute("other_req_doc_2_indicator", "other_req_doc_2_indicator", false, "TermsBeanAlias.other_req_doc_2_indicator");
	 }


      /**
        * Attribute factory method for other_req_doc_2_name
        *
        * The name of the other required document #3.
        */
	public static Attribute create_other_req_doc_2_name()
	 {
	  return (Attribute) new Attribute("other_req_doc_2_name", "other_req_doc_2_name", false, "TermsBeanAlias.other_req_doc_2_name");
	 }


      /**
        * Attribute factory method for other_req_doc_2_originals
        *
        * Number of originals  of the other required document #3 for this transaction.
        */
	public static Attribute create_other_req_doc_2_originals()
	 {
	  return (Attribute) new NumberAttribute("other_req_doc_2_originals", "other_req_doc_2_originals", false, "TermsBeanAlias.other_req_doc_2_originals");
	 }


      /**
        * Attribute factory method for other_req_doc_2_text
        *
        * Text for the other required document #3 for this transaction.
        */
	public static Attribute create_other_req_doc_2_text()
	 {
	  return (Attribute) new Attribute("other_req_doc_2_text", "other_req_doc_2_text", false, "TermsBeanAlias.other_req_doc_2_text");
	 }


      /**
        * Attribute factory method for other_req_doc_3_copies
        *
        * Number of copies of the other required document #2 for this transaction.
        */
	public static Attribute create_other_req_doc_3_copies()
	 {
	  return (Attribute) new NumberAttribute("other_req_doc_3_copies", "other_req_doc_3_copies", false, "TermsBeanAlias.other_req_doc_3_copies");
	 }


      /**
        * Attribute factory method for other_req_doc_3_indicator
        *
        * Indicates if data is included in this transaction for the other required
        * document #2.
        */
	public static Attribute create_other_req_doc_3_indicator()
	 {
	  return (Attribute) new IndicatorAttribute("other_req_doc_3_indicator", "other_req_doc_3_indicator", false, "TermsBeanAlias.other_req_doc_3_indicator");
	 }


      /**
        * Attribute factory method for other_req_doc_3_name
        *
        * The name of the other required document #2.
        */
	public static Attribute create_other_req_doc_3_name()
	 {
	  return (Attribute) new Attribute("other_req_doc_3_name", "other_req_doc_3_name", false, "TermsBeanAlias.other_req_doc_3_name");
	 }


      /**
        * Attribute factory method for other_req_doc_3_originals
        *
        * Number of originals  of the other required document #2 for this transaction.
        */
	public static Attribute create_other_req_doc_3_originals()
	 {
	  return (Attribute) new NumberAttribute("other_req_doc_3_originals", "other_req_doc_3_originals", false, "TermsBeanAlias.other_req_doc_3_originals");
	 }


      /**
        * Attribute factory method for other_req_doc_3_text
        *
        * Text for the other required document #2 for this transaction.
        */
	public static Attribute create_other_req_doc_3_text()
	 {
	  return (Attribute) new Attribute("other_req_doc_3_text", "other_req_doc_3_text", false, "TermsBeanAlias.other_req_doc_3_text");
	 }


      /**
        * Attribute factory method for other_req_doc_4_copies
        *
        * Number of copies of the other required document #1 for this transaction.
        */
	public static Attribute create_other_req_doc_4_copies()
	 {
	  return (Attribute) new NumberAttribute("other_req_doc_4_copies", "other_req_doc_4_copies", false, "TermsBeanAlias.other_req_doc_4_copies");
	 }


      /**
        * Attribute factory method for other_req_doc_4_indicator
        *
        * Indicates if data is included in this transaction for the other required
        * document #1.
        */
	public static Attribute create_other_req_doc_4_indicator()
	 {
	  return (Attribute) new IndicatorAttribute("other_req_doc_4_indicator", "other_req_doc_4_indicator", false, "TermsBeanAlias.other_req_doc_4_indicator");
	 }


      /**
        * Attribute factory method for other_req_doc_4_name
        *
        * The name of the other required document #1.
        */
	public static Attribute create_other_req_doc_4_name()
	 {
	  return (Attribute) new Attribute("other_req_doc_4_name", "other_req_doc_4_name", false, "TermsBeanAlias.other_req_doc_4_name");
	 }


      /**
        * Attribute factory method for other_req_doc_4_originals
        *
        * Number of originals  of the other required document #1 for this transaction.
        */
	public static Attribute create_other_req_doc_4_originals()
	 {
	  return (Attribute) new NumberAttribute("other_req_doc_4_originals", "other_req_doc_4_originals", false, "TermsBeanAlias.other_req_doc_4_originals");
	 }


      /**
        * Attribute factory method for other_req_doc_4_text
        *
        * Text for the other required document #1 for this transaction.
        */
	public static Attribute create_other_req_doc_4_text()
	 {
	  return (Attribute) new Attribute("other_req_doc_4_text", "other_req_doc_4_text", false, "TermsBeanAlias.other_req_doc_4_text");
	 }


      /**
        * Attribute factory method for other_settlement_instructions
        *
        * The additional text describing settlement or commissions and charges for
        * the instrument.
        */
	public static Attribute create_other_settlement_instructions()
	 {
	  return (Attribute) new Attribute("other_settlement_instructions", "other_settlement_instructions", false, "TermsBeanAlias.other_settlement_instructions");
	 }


      /**
        * Attribute factory method for overseas_validity_date
        *
        * The date on which the Guarantee obligation expires at the bank issuing the
        * Guarantee
        */
	public static Attribute create_overseas_validity_date()
	 {
	  return (Attribute) new DateTimeAttribute("overseas_validity_date", "overseas_validity_date", false, "TermsBeanAlias.overseas_validity_date");
	 }


      /**
        * Attribute factory method for packing_list_copies
        *
        * Number of copies of the packing list for this transaction.
        */
	public static Attribute create_packing_list_copies()
	 {
	  return (Attribute) new NumberAttribute("packing_list_copies", "packing_list_copies", false, "TermsBeanAlias.packing_list_copies");
	 }


      /**
        * Attribute factory method for packing_list_indicator
        *
        * Indicates if data is included in this transaction for the packing list.
        */
	public static Attribute create_packing_list_indicator()
	 {
	  return (Attribute) new IndicatorAttribute("packing_list_indicator", "packing_list_indicator", false, "TermsBeanAlias.packing_list_indicator");
	 }


      /**
        * Attribute factory method for packing_list_originals
        *
        * Number of originals  of the packing list for this transaction.
        */
	public static Attribute create_packing_list_originals()
	 {
	  return (Attribute) new NumberAttribute("packing_list_originals", "packing_list_originals", false, "TermsBeanAlias.packing_list_originals");
	 }


      /**
        * Attribute factory method for packing_list_text
        *
        * Text for the packing list for this transaction.
        */
	public static Attribute create_packing_list_text()
	 {
	  return (Attribute) new Attribute("packing_list_text", "packing_list_text", false, "TermsBeanAlias.packing_list_text");
	 }


      /**
        * Attribute factory method for part_payments_percent
        *
        * ...
        */
	public static Attribute create_part_payments_percent()
	 {
	  return (Attribute) new NumberAttribute("part_payments_percent", "part_payments_percent", false, "TermsBeanAlias.part_payments_percent");
	 }


      /**
        * Attribute factory method for part_payments_type
        *
        * The '% of any drawing' field.  Corresponds to the part_payments_type attribute
        */
	public static Attribute create_part_payments_type()
	 {
	  return (Attribute) new ReferenceAttribute("part_payments_type", "part_payments_type", "PART_PAYMENTS_TYPE", false, "TermsBeanAlias.part_payments_type");
	 }


      /**
        * Attribute factory method for partial_shipment_allowed
        *
        * If this checkbox is selected, partial shipments are allowed.
        */
	public static Attribute create_partial_shipment_allowed()
	 {
	  return (Attribute) new IndicatorAttribute("partial_shipment_allowed", "partial_shipment_allowed", false, "TermsBeanAlias.partial_shipment_allowed");
	 }


      /**
        * Attribute factory method for payment_instructions
        *
        * Payment instructions for the export collection.  These are either added
        * automatically to the transaction or it is chosen the user.  See also pmt_instr_multiple_phrase.
        */
	public static Attribute create_payment_instructions()
	 {
	  return (Attribute) new Attribute("payment_instructions", "payment_instructions", false, "TermsBeanAlias.payment_instructions");
	 }


      /**
        * Attribute factory method for place_of_expiry
        *
        * The location of the expiration of the instrument.
        */
	public static Attribute create_place_of_expiry()
	 {
	  return (Attribute) new Attribute("place_of_expiry", "place_of_expiry", false, "TermsBeanAlias.place_of_expiry");
	 }


      /**
        * Attribute factory method for pmt_instr_multiple_phrase
        *
        * Indicates if payment instructions have been created for this transaction.
        * This is used on the Export Collection page.    Payment instructions are
        * either added automatically (if there is only one for the currency code)
        * or selected by the user.   The system must know the status of entering the
        * payment instructions in order to format the page properly.  This is a field
        * that is set by the system.
        */
	public static Attribute create_pmt_instr_multiple_phrase()
	 {
	  return (Attribute) new ReferenceAttribute("pmt_instr_multiple_phrase", "pmt_instr_multiple_phrase", "PMT_INSTR_TYPE");
	 }


      /**
        * Attribute factory method for pmt_terms_days_after_type
        *
        * The terms at which the purchaser will pay the seller for the goods. Tenor
        * examples include Sight (immediate payment), and 90 days from bill of lading
        * date
        */
	public static Attribute create_pmt_terms_days_after_type()
	 {
	  return (Attribute) new ReferenceAttribute("pmt_terms_days_after_type", "pmt_terms_days_after_type", "TENOR_TYPE", false, "TermsBeanAlias.pmt_terms_days_after_type");
	 }


      /**
        * Attribute factory method for pmt_terms_fixed_maturity_date
        *
        * Indicates that payment is due on a fixed maturity date
        */
	public static Attribute create_pmt_terms_fixed_maturity_date()
	 {
	  return (Attribute) new DateTimeAttribute("pmt_terms_fixed_maturity_date", "pmt_terms_fixed_maturity_date", false, "TermsBeanAlias.pmt_terms_fixed_maturity_date");
	 }


      /**
        * Attribute factory method for pmt_terms_num_days_after
        *
        * A specific number of days after a specific tenor type (Shipment Date, Bill
        * of Lading Date, Invoice Date, and so on) at which point a payment is due.
        */
	public static Attribute create_pmt_terms_num_days_after()
	 {
	  return (Attribute) new NumberAttribute("pmt_terms_num_days_after", "pmt_terms_num_days_after", false, "TermsBeanAlias.pmt_terms_num_days_after");
	 }


      /**
        * Attribute factory method for pmt_terms_percent_invoice
        *
        * The difference between the amount of the invoice and the amount of the actual
        * presentation.
        */
	public static Attribute create_pmt_terms_percent_invoice()
	 {
	  return (Attribute) new NumberAttribute("pmt_terms_percent_invoice", "pmt_terms_percent_invoice", false, "TermsBeanAlias.pmt_terms_percent_invoice");
	 }


      /**
        * Attribute factory method for pmt_terms_type
        *
        * Indicates when presentations will be payable under the terms of the instrument.
        * Examples are: Sight - Indicates that payment is due immediately upon receipt,
        * Other - Indicates that the Other Conditions section of the transaction contains
        * tenor option information, ____ days after ____ - Indicates that payment
        * is due a specific number of days after a specific tenor type (Shipment Date,
        * Bill of Lading Date, Invoice Date, and so on).
        */
	public static Attribute create_pmt_terms_type()
	 {
	  return (Attribute) new ReferenceAttribute("pmt_terms_type", "pmt_terms_type", "PAYMENT_TERMS_TYPE", false, "TermsBeanAlias.pmt_terms_type");
	 }


      /**
        * Attribute factory method for present_air_waybill
        *
        * Indicates whether or not an air waybill must be presented for the instrument.
        */
	public static Attribute create_present_air_waybill()
	 {
	  return (Attribute) new IndicatorAttribute("present_air_waybill", "present_air_waybill", false, "TermsBeanAlias.present_air_waybill");
	 }


      /**
        * Attribute factory method for present_bill_of_lading
        *
        * Indicates whether or not a bill of lading must be presented for the instrument.
        */
	public static Attribute create_present_bill_of_lading()
	 {
	  return (Attribute) new IndicatorAttribute("present_bill_of_lading", "present_bill_of_lading", false, "TermsBeanAlias.present_bill_of_lading");
	 }


      /**
        * Attribute factory method for present_bills_of_exchange
        *
        * Indicates whether or not a bill of exchange must be presented for the instrument.
        */
	public static Attribute create_present_bills_of_exchange()
	 {
	  return (Attribute) new IndicatorAttribute("present_bills_of_exchange", "present_bills_of_exchange", false, "TermsBeanAlias.present_bills_of_exchange");
	 }


      /**
        * Attribute factory method for present_cert_of_origin
        *
        * Indicates whether or not a certificate of origin must be presented for the
        * instrument.
        */
	public static Attribute create_present_cert_of_origin()
	 {
	  return (Attribute) new IndicatorAttribute("present_cert_of_origin", "present_cert_of_origin", false, "TermsBeanAlias.present_cert_of_origin");
	 }


      /**
        * Attribute factory method for present_commercial_invoice
        *
        * Indicates whether or not a commercial invoice must be presented for the
        * instrument.
        */
	public static Attribute create_present_commercial_invoice()
	 {
	  return (Attribute) new IndicatorAttribute("present_commercial_invoice", "present_commercial_invoice", false, "TermsBeanAlias.present_commercial_invoice");
	 }


      /**
        * Attribute factory method for present_docs_days_user_ind
        *
        * Indicates whether the field (present_docs_within_days) has been entered
        * by the user.
        * Y=Entered By User
        */
	public static Attribute create_present_docs_days_user_ind()
	 {
	  return (Attribute) new IndicatorAttribute("present_docs_days_user_ind", "present_docs_days_user_ind", false, "TermsBeanAlias.present_docs_days_user_ind");
	 }


      /**
        * Attribute factory method for present_docs_within_days
        *
        * The number of days after shipment that documents must be presented.
        */
	public static Attribute create_present_docs_within_days()
	 {
	  return (Attribute) new NumberAttribute("present_docs_within_days", "present_docs_within_days", false, "TermsBeanAlias.present_docs_within_days");
	 }


      /**
        * Attribute factory method for present_insurance
        *
        * Indicates whether or not an insurance certificate  must be presented for
        * the instrument.
        */
	public static Attribute create_present_insurance()
	 {
	  return (Attribute) new IndicatorAttribute("present_insurance", "present_insurance", false, "TermsBeanAlias.present_insurance");
	 }


      /**
        * Attribute factory method for present_non_neg_bill_of_lading
        *
        * Indicates whether or not a non negotiable bill of lading must be presented
        * for the instrument.
        */
	public static Attribute create_present_non_neg_bill_of_lading()
	 {
	  return (Attribute) new IndicatorAttribute("present_non_neg_bill_of_lading", "present_non_neg_bill_of_lading", false, "TermsBeanAlias.present_non_neg_bill_of_lading");
	 }


      /**
        * Attribute factory method for present_num_air_waybill
        *
        * The number of originals and copies of air waybills that must be presented.
        * The originals and copies are entered as one text field (for example, 3/3)
        */
	public static Attribute create_present_num_air_waybill()
	 {
	  return (Attribute) new Attribute("present_num_air_waybill", "present_num_air_waybill", false, "TermsBeanAlias.present_num_air_waybill");
	 }


      /**
        * Attribute factory method for present_num_bill_of_lading
        *
        * The number of originals and copies of bill of ladings that must be presented.
        * The originals and copies are entered as one text field (for example, 3/3)
        */
	public static Attribute create_present_num_bill_of_lading()
	 {
	  return (Attribute) new Attribute("present_num_bill_of_lading", "present_num_bill_of_lading", false, "TermsBeanAlias.present_num_bill_of_lading");
	 }


      /**
        * Attribute factory method for present_num_bills_of_exchange
        *
        * The number of originals and copies of bills of exchanges that must be presented.
        * The originals and copies are entered as one text field (for example, 3/3)
        */
	public static Attribute create_present_num_bills_of_exchange()
	 {
	  return (Attribute) new Attribute("present_num_bills_of_exchange", "present_num_bills_of_exchange", false, "TermsBeanAlias.present_num_bills_of_exchange");
	 }


      /**
        * Attribute factory method for present_num_cert_of_origin
        *
        * The number of originals and copies of certificates of origin that must be
        * presented.  The originals and copies are entered as one text field (for
        * example, 3/3)
        */
	public static Attribute create_present_num_cert_of_origin()
	 {
	  return (Attribute) new Attribute("present_num_cert_of_origin", "present_num_cert_of_origin", false, "TermsBeanAlias.present_num_cert_of_origin");
	 }


      /**
        * Attribute factory method for present_num_commercial_invoice
        *
        * The number of originals and copies of commercial invoices that must be presented.
        * The originals and copies are entered as one text field (for example, 3/3)
        */
	public static Attribute create_present_num_commercial_invoice()
	 {
	  return (Attribute) new Attribute("present_num_commercial_invoice", "present_num_commercial_invoice", false, "TermsBeanAlias.present_num_commercial_invoice");
	 }


      /**
        * Attribute factory method for present_num_insurance
        *
        * The number of originals and copies of insurance certificates that must be
        * presented.  The originals and copies are entered as one text field (for
        * example, 3/3)
        */
	public static Attribute create_present_num_insurance()
	 {
	  return (Attribute) new Attribute("present_num_insurance", "present_num_insurance", false, "TermsBeanAlias.present_num_insurance");
	 }


      /**
        * Attribute factory method for present_num_non_neg_bill_ladng
        *
        * ...
        */
	public static Attribute create_present_num_non_neg_bill_ladng()
	 {
	  return (Attribute) new Attribute("present_num_non_neg_bill_ladng", "present_num_non_neg_bill_ladng", false, "TermsBeanAlias.present_num_non_neg_bill_ladng");
	 }


      /**
        * Attribute factory method for present_num_packing_list
        *
        * The number of originals and copies of packing lists that must be presented.
        * The originals and copies are entered as one text field (for example, 3/3)
        */
	public static Attribute create_present_num_packing_list()
	 {
	  return (Attribute) new Attribute("present_num_packing_list", "present_num_packing_list", false, "TermsBeanAlias.present_num_packing_list");
	 }


      /**
        * Attribute factory method for present_other
        *
        * Indicates whether or not any other documents must be presented for the instruments.
        * The other documents are specified in the present other text attribute.
        */
	public static Attribute create_present_other()
	 {
	  return (Attribute) new IndicatorAttribute("present_other", "present_other", false, "TermsBeanAlias.present_other");
	 }


      /**
        * Attribute factory method for present_other_text
        *
        * Text describing any other documents that must be presented.  Corresponds
        * to the present_other attribute
        */
	public static Attribute create_present_other_text()
	 {
	  return (Attribute) new Attribute("present_other_text", "present_other_text", false, "TermsBeanAlias.present_other_text");
	 }


      /**
        * Attribute factory method for present_packing_list
        *
        * Indicates whether or not a packing list must be presented for the instrument.
        */
	public static Attribute create_present_packing_list()
	 {
	  return (Attribute) new IndicatorAttribute("present_packing_list", "present_packing_list", false, "TermsBeanAlias.present_packing_list");
	 }


      /**
        * Attribute factory method for purpose_type
        *
        * An internal classification designating the type of instrument you are creating
        * or editing.  Values vary depending on  the type of instrument you're   creating.
        */
	public static Attribute create_purpose_type()
	 {
	  return (Attribute) new Attribute("purpose_type", "purpose_type", false, "TermsBeanAlias.purpose_type");
	 }


      /**
        * Attribute factory method for reference_number
        *
        * Your reference number for the instrument.
        */
	public static Attribute create_reference_number()
	 {
	  return (Attribute) new Attribute("reference_number", "reference_number", false, "TermsBeanAlias.reference_number");
	 }


      /**
        * Attribute factory method for related_instrument_id
        *
        * The ID of the related instrument for the Air Waybill or Shipping Guarantee.
        */
	public static Attribute create_related_instrument_id()
	 {
	  return (Attribute) new Attribute("related_instrument_id", "related_instrument_id", false, "TermsBeanAlias.related_instrument_id");
	 }


      /**
        * Attribute factory method for release_by_bank_on
        *
        * The date when the bank will pay out the funds.  To enter the payment date,
        * select a day, month, and year from the appropriate drop-down lists.  This
        * date is required.
        */
	public static Attribute create_release_by_bank_on()
	 {
	  return (Attribute) new DateTimeAttribute("release_by_bank_on", "release_by_bank_on", false, "TermsBeanAlias.release_by_bank_on");
	 }


      /**
        * Attribute factory method for revolve
        *
        * If this checkbox is selected, the amounts under this instrument are available
        * on a revolving basis. If this checkbox is selected, make sure that revolving
        * terms are entered in the Additional Conditions field.
        */
	public static Attribute create_revolve()
	 {
	  return (Attribute) new IndicatorAttribute("revolve", "revolve", false, "TermsBeanAlias.revolve");
	 }


      /**
        * Attribute factory method for settlement_foreign_acct_curr
        *
        * The currency of the settlement foreign currency account.
        */
	public static Attribute create_settlement_foreign_acct_curr()
	 {
	  return (Attribute) new ReferenceAttribute("settlement_foreign_acct_curr", "settlement_foreign_acct_curr", "CURRENCY_CODE", false, "TermsBeanAlias.settlement_foreign_acct_curr");
	 }


      /**
        * Attribute factory method for settlement_foreign_acct_num
        *
        * The foreign currency debit account number for settlemen
        */
	public static Attribute create_settlement_foreign_acct_num()
	 {
	  return (Attribute) new Attribute("settlement_foreign_acct_num", "settlement_foreign_acct_num", false, "TermsBeanAlias.settlement_foreign_acct_num");
	 }


      /**
        * Attribute factory method for settlement_our_account_num
        *
        * The debit account number for settlement.
        */
	public static Attribute create_settlement_our_account_num()
	 {
	  return (Attribute) new Attribute("settlement_our_account_num", "settlement_our_account_num", false, "TermsBeanAlias.settlement_our_account_num");
	 }


      /**
        * Attribute factory method for special_bank_instructions
        *
        * The special instructions text for the transaction.
        */
	public static Attribute create_special_bank_instructions()
	 {
	  return (Attribute) new Attribute("special_bank_instructions", "special_bank_instructions", false, "TermsBeanAlias.special_bank_instructions");
	 }


      /**
        * Attribute factory method for tender_order_contract_details
        *
        * The details of the Guarantee tender, order, or contract; for example, a
        * description of the work to be completed, or the goods to be provided, in
        * order to meet the terms of the Guarantee.
        */
	public static Attribute create_tender_order_contract_details()
	 {
	  return (Attribute) new Attribute("tender_order_contract_details", "tender_order_contract_details", false, "TermsBeanAlias.tender_order_contract_details");
	 }


      /**
        * Attribute factory method for tenor_1
        *
        * The amount of the first tenor that applies to the export collection.
        */
	public static Attribute create_tenor_1()
	 {
	  return (Attribute) new Attribute("tenor_1", "tenor_1", false, "TermsBeanAlias.tenor_1");
	 }


      /**
        * Attribute factory method for tenor_1_amount
        *
        * The textual description of the first tenor that applies to the export collection.
        */
	public static Attribute create_tenor_1_amount()
	 {
	  return (Attribute) new TradePortalDecimalAttribute("tenor_1_amount", "tenor_1_amount", false, "TermsBeanAlias.tenor_1_amount");
	 }


      /**
        * Attribute factory method for tenor_1_draft_number
        *
        * The draft number of the first tenor that applies to the export collection.
        */
	public static Attribute create_tenor_1_draft_number()
	 {
	  return (Attribute) new Attribute("tenor_1_draft_number", "tenor_1_draft_number", false, "TermsBeanAlias.tenor_1_draft_number");
	 }


      /**
        * Attribute factory method for tenor_2
        *
        * The amount of the second tenor that applies to the export collection.
        */
	public static Attribute create_tenor_2()
	 {
	  return (Attribute) new Attribute("tenor_2", "tenor_2", false, "TermsBeanAlias.tenor_2");
	 }


      /**
        * Attribute factory method for tenor_2_amount
        *
        * The textual description of the second tenor that applies to the export collection.
        */
	public static Attribute create_tenor_2_amount()
	 {
	  return (Attribute) new TradePortalDecimalAttribute("tenor_2_amount", "tenor_2_amount", false, "TermsBeanAlias.tenor_2_amount");
	 }


      /**
        * Attribute factory method for tenor_2_draft_number
        *
        * The draft number of the second tenor that applies to the export collection.
        */
	public static Attribute create_tenor_2_draft_number()
	 {
	  return (Attribute) new Attribute("tenor_2_draft_number", "tenor_2_draft_number", false, "TermsBeanAlias.tenor_2_draft_number");
	 }


      /**
        * Attribute factory method for tenor_3
        *
        * The amount of the third tenor that applies to the export collection.
        */
	public static Attribute create_tenor_3()
	 {
	  return (Attribute) new Attribute("tenor_3", "tenor_3", false, "TermsBeanAlias.tenor_3");
	 }


      /**
        * Attribute factory method for tenor_3_amount
        *
        * The textual description of the third tenor that applies to the export collection.
        */
	public static Attribute create_tenor_3_amount()
	 {
	  return (Attribute) new TradePortalDecimalAttribute("tenor_3_amount", "tenor_3_amount", false, "TermsBeanAlias.tenor_3_amount");
	 }


      /**
        * Attribute factory method for tenor_3_draft_number
        *
        * The draft number of the third tenor that applies to the export collection.
        */
	public static Attribute create_tenor_3_draft_number()
	 {
	  return (Attribute) new Attribute("tenor_3_draft_number", "tenor_3_draft_number", false, "TermsBeanAlias.tenor_3_draft_number");
	 }


      /**
        * Attribute factory method for tenor_4
        *
        * The amount of the fourth tenor that applies to the export collection.
        */
	public static Attribute create_tenor_4()
	 {
	  return (Attribute) new Attribute("tenor_4", "tenor_4", false, "TermsBeanAlias.tenor_4");
	 }


      /**
        * Attribute factory method for tenor_4_amount
        *
        * The textual description of the fourth tenor that applies to the export collection.
        */
	public static Attribute create_tenor_4_amount()
	 {
	  return (Attribute) new TradePortalDecimalAttribute("tenor_4_amount", "tenor_4_amount", false, "TermsBeanAlias.tenor_4_amount");
	 }


      /**
        * Attribute factory method for tenor_4_draft_number
        *
        * The draft number of the fourth tenor that applies to the export collection.
        */
	public static Attribute create_tenor_4_draft_number()
	 {
	  return (Attribute) new Attribute("tenor_4_draft_number", "tenor_4_draft_number", false, "TermsBeanAlias.tenor_4_draft_number");
	 }


      /**
        * Attribute factory method for tenor_5
        *
        * The amount of the fifth tenor that applies to the export collection.
        */
	public static Attribute create_tenor_5()
	 {
	  return (Attribute) new Attribute("tenor_5", "tenor_5", false, "TermsBeanAlias.tenor_5");
	 }


      /**
        * Attribute factory method for tenor_5_amount
        *
        * The textual description of the fifth tenor that applies to the export collection.
        */
	public static Attribute create_tenor_5_amount()
	 {
	  return (Attribute) new TradePortalDecimalAttribute("tenor_5_amount", "tenor_5_amount", false, "TermsBeanAlias.tenor_5_amount");
	 }


      /**
        * Attribute factory method for tenor_5_draft_number
        *
        * The draft number of the fifth tenor that applies to the export collection.
        */
	public static Attribute create_tenor_5_draft_number()
	 {
	  return (Attribute) new Attribute("tenor_5_draft_number", "tenor_5_draft_number", false, "TermsBeanAlias.tenor_5_draft_number");
	 }


      /**
        * Attribute factory method for tenor_6
        *
        * The amount of the sixth tenor that applies to the export collection.
        */
	public static Attribute create_tenor_6()
	 {
	  return (Attribute) new Attribute("tenor_6", "tenor_6", false, "TermsBeanAlias.tenor_6");
	 }


      /**
        * Attribute factory method for tenor_6_amount
        *
        * The textual description of the sixth tenor that applies to the export collection.
        */
	public static Attribute create_tenor_6_amount()
	 {
	  return (Attribute) new TradePortalDecimalAttribute("tenor_6_amount", "tenor_6_amount", false, "TermsBeanAlias.tenor_6_amount");
	 }


      /**
        * Attribute factory method for tenor_6_draft_number
        *
        * The draft number of the sixth tenor that applies to the export collection.
        */
	public static Attribute create_tenor_6_draft_number()
	 {
	  return (Attribute) new Attribute("tenor_6_draft_number", "tenor_6_draft_number", false, "TermsBeanAlias.tenor_6_draft_number");
	 }


      /**
        * Attribute factory method for terms_oid
        *
        * Unique identifier
        */
	public static Attribute create_terms_oid()
	 {
	  return (Attribute) new ObjectIDAttribute("terms_oid", "terms_oid");
	 }


      /**
        * Attribute factory method for terms_source_type
        *
        * Indicates whether these terms are the bank released terms of a transaction
        * or whether they are the customer entered terms
        */
	public static Attribute create_terms_source_type()
	 {
	  return (Attribute) new ReferenceAttribute("terms_source_type", "terms_source_type", "TERMS_SOURCE");
	 }


      /**
        * Attribute factory method for total_rate
        *
        * Total Interest rate applied to the LC
        */
	public static Attribute create_total_rate()
	 {
	  return (Attribute) new TradePortalDecimalAttribute("total_rate", "total_rate");
	 }


      /**
        * Attribute factory method for tracer_details
        *
        * The additional tracer details for the instrumen
        */
	public static Attribute create_tracer_details()
	 {
	  return (Attribute) new Attribute("tracer_details", "tracer_details", false, "TermsBeanAlias.tracer_details");
	 }


      /**
        * Attribute factory method for tracer_send_type
        *
        * Indicates whether the tracer copy is being sent for information purposes
        * only, with no action required from the bank or whether the tracer is a request
        * for the bank to send a tracer on the customer's behalf and send confirmation
        * of the tracer
        */
	public static Attribute create_tracer_send_type()
	 {
	  return (Attribute) new ReferenceAttribute("tracer_send_type", "tracer_send_type", "TRACER_SEND_TYPE", false, "TermsBeanAlias.tracer_send_type");
	 }


      /**
        * Attribute factory method for transferrable
        *
        * If this checkbox is selected, part or all of the instrument may be transferred
        * to another Beneficiary. For example, a portion of the instrument can be
        * re-issued in favour of the goods manufacturer.
        */
	public static Attribute create_transferrable()
	 {
	  return (Attribute) new IndicatorAttribute("transferrable", "transferrable", false, "TermsBeanAlias.transferrable");
	 }


      /**
        * Attribute factory method for ucp_details
        *
        * Information about the version of UCP for Documentary Credits that covers
        * the documentary credit.
        */
	public static Attribute create_ucp_details()
	 {
	  return (Attribute) new Attribute("ucp_details", "ucp_details", false, "TermsBeanAlias.ucp_details");
	 }


      /**
        * Attribute factory method for ucp_version
        *
        * An indication of the ICC guidelines to which the documentary credit is issued.
        */
	public static Attribute create_ucp_version()
	 {
	  return (Attribute) new ReferenceAttribute("ucp_version", "ucp_version", "ICC_GUIDELINES", false, "TermsBeanAlias.ucp_version");
	 }


      /**
        * Attribute factory method for ucp_version_details_ind
        *
        * An indicator whether the documentary credit is issued subject to the UCP
        * for Documentary Credits which are effective on the date of issue.
        */
	public static Attribute create_ucp_version_details_ind()
	 {
	  return (Attribute) new IndicatorAttribute("ucp_version_details_ind", "ucp_version_details_ind", false, "TermsBeanAlias.ucp_version_details_ind");
	 }


      /**
        * Attribute factory method for urc_pub_num
        *
        * The Uniform Rules for Collections publication number which is effective
        * on the date of issue of the collection is issued
        */
	public static Attribute create_urc_pub_num()
	 {
	  return (Attribute) new Attribute("urc_pub_num", "urc_pub_num", false, "TermsBeanAlias.urc_pub_num");
	 }


      /**
        * Attribute factory method for urc_year
        *
        * The year related to Uniform Rules for Collections publication number.
        */
	public static Attribute create_urc_year()
	 {
	  return (Attribute) new Attribute("urc_year", "urc_year", false, "TermsBeanAlias.urc_year");
	 }


      /**
        * Attribute factory method for usance_maturity_date
        *
        * Indicates the maturity date of the instrument.
        */
	public static Attribute create_usance_maturity_date()
	 {
	  return (Attribute) new DateTimeAttribute("usance_maturity_date", "usance_maturity_date");
	 }


      /**
        * Attribute factory method for use_fec
        *
        * Selecting this check box indicates that a Forward Exchange Contract (FEC)
        * will be referenced for the instrument
        */
	public static Attribute create_use_fec()
	 {
	  return (Attribute) new IndicatorAttribute("use_fec", "use_fec");
	 }


      /**
        * Attribute factory method for use_mkt_rate
        *
        * Selecting this check box indicates that a spot/market rate will be used
        * for payment of the loan at loan maturity
        */
	public static Attribute create_use_mkt_rate()
	 {
	  return (Attribute) new IndicatorAttribute("use_mkt_rate", "use_mkt_rate");
	 }


      /**
        * Attribute factory method for use_other
        *
        * Select this check box and enter any additional exchange rate details
        */
	public static Attribute create_use_other()
	 {
	  return (Attribute) new IndicatorAttribute("use_other", "use_other");
	 }


      /**
        * Attribute factory method for use_other_text
        *
        * Text related to use_other
        */
	public static Attribute create_use_other_text()
	 {
	  return (Attribute) new Attribute("use_other_text", "use_other_text", false, "TermsBeanAlias.use_other_text");
	 }


      /**
        * Attribute factory method for value_date
        *
        * The value date of the transaction processed by the bank.
        */
	public static Attribute create_value_date()
	 {
	  return (Attribute) new DateTimeAttribute("value_date", "value_date");
	 }


      /**
        * Attribute factory method for work_item_number
        *
        * A unique reference number associated with each work item generated by the
        * bank.
        */
	public static Attribute create_work_item_number()
	 {
	  return (Attribute) new Attribute("work_item_number", "work_item_number");
	 }


      /**
        * Attribute factory method for work_item_priority
        *
        * The priority of the work item that was created in OTL for this transaction
        */
	public static Attribute create_work_item_priority()
	 {
	  return (Attribute) new Attribute("work_item_priority", "work_item_priority", false, "TermsBeanAlias.work_item_priority");
	 }


      /**
        * Attribute factory method for financed_invoices_details_text
        *
        * For Receivables Financing Loan Requests, a text field with the Label �Enter
        * details of the invoices being financed� will be available for the Corporate
        * Customer to enter details of the invoices being financed for the loan request.
        * This will be a required field.
        */
	public static Attribute create_financed_invoices_details_text()
	 {
	  return (Attribute) new Attribute("financed_invoices_details_text", "financed_invoices_details_text", false, "TermsBeanAlias.financed_invoices_details_text");
	 }


      /**
        * Attribute factory method for financing_backed_by_buyer_ind
        *
        * If the Type of Transaction being Financed is either Seller-Requested Financing
        * or Buyer-Requested Financing for Seller, a checkbox with the Label �Financing
        * Backed by Buyer� will be available for the Corporate Customer to specify
        * whether the Financing is backed by the Buyer and thus covered under the
        * Buyer�s credit line or not. Set to Y if financing backed by buyer
        */
	public static Attribute create_financing_backed_by_buyer_ind()
	 {
	  return (Attribute) new IndicatorAttribute("financing_backed_by_buyer_ind", "financing_backed_by_buyer_ind", false, "TermsBeanAlias.financing_backed_by_buyer_ind");
	 }


      /**
        * Attribute factory method for funding_amount
        *
        * Funding Payment Amount for a Funds Transfer Request Issue Transaction.
        * This value will usually be the same as the Transaction Amount (amount).
        */
	public static Attribute create_funding_amount()
	 {
	  return (Attribute) new TradePortalSignedDecimalAttribute("funding_amount", "funding_amount", false, "TermsBeanAlias.funding_amount");
	 }


      /**
        * Attribute factory method for financed_pos_details_text
        *
        * When the Loan Type is EXPORT LOAN, in section 3, Export Loan Instructions,
        * a new radio button option will be inserted in the Type of Transaction being
        * Financed section (inserted between Goods Under Export Collection and Other)
        * titled:"Standalone Pre-Shipment Financing (not under Export LC/Collection)
        * - enter details of the Purchase Orders being financed" followed by a text
        * box where the text details of the POs can be entered.
        */
	public static Attribute create_financed_pos_details_text()
	 {
	  return (Attribute) new Attribute("financed_pos_details_text", "financed_pos_details_text", false, "TermsBeanAlias.financed_pos_details_text");
	 }


      /**
        * Attribute factory method for invoice_only_ind
        *
        * For ATP transactions.  Set to Y if Invoice Only.
        */
	public static Attribute create_invoice_only_ind()
	 {
	  return (Attribute) new IndicatorAttribute("invoice_only_ind", "invoice_only_ind", false, "TermsBeanAlias.invoice_only_ind");
	 }


      /**
        * Attribute factory method for invoice_due_date
        *
        * Indicate the due date of the approved invoices.
        */
	public static Attribute create_invoice_due_date()
	 {
	  return (Attribute) new DateTimeAttribute("invoice_due_date", "invoice_due_date");
	 }
	//SHR CR708 Rel8.1.1 Begin
	/**
     * Attribute factory method for invoice_due_date
     *
     * Indicate the due date of the approved invoices.
     */
	public static Attribute create_invoice_details()
	 {
	  return (Attribute) new Attribute("invoice_details", "invoice_details");
	 }
	//SHR CR708 Rel8.1.1 End

      /**
        * Attribute factory method for unapplied_amount
        *
        * The total amount of invoices that have been unapplied in the portal.  This
        * amount could be populated on the message when it goes from OTL to the Portal
        * with an Apply Pay activity.

        */
	public static Attribute create_unapplied_amount()
	 {
	  return (Attribute) new TradePortalDecimalAttribute("unapplied_amount", "unapplied_amount");
	 }


      /**
        * Attribute factory method for financing_allowed
        *
        * Indicates (Y/N) whether financing is allowed for this seller/currency instrument.
        * It will also only be populated from OTL to Portal  for Receivables � ARM
        * and CHM activities.
        */
	public static Attribute create_financing_allowed()
	 {
	  return (Attribute) new IndicatorAttribute("financing_allowed", "financing_allowed");
	 }


      /**
        * Attribute factory method for finance_percentage
        *
        * If FinancingAllowed is Y, this is the default percentage of invoice that
        * will be financed.  It will also only be populated from OTL to Portal  for
        * Receivables � ARM and CHM activities
        */
	public static Attribute create_finance_percentage()
	 {
	  return (Attribute) new TradePortalDecimalAttribute("finance_percentage", "finance_percentage");
	 }


      /**
        * Attribute factory method for equivalent_exch_amount
        *
        * If the transfer amount specified is denominated in the same currency as
        * the credit account currency, after verification this will display the equivalent
        * amount in the currency of the debit account. If the transfer amount specified
        * is denominated in the same currency as the debit account currency, after
        * verification this will display the equivalent amount in the currency of
        * the credit account.
        */
	public static Attribute create_equivalent_exch_amount()
	 {
	  return (Attribute) new TradePortalDecimalAttribute("equivalent_exch_amount", "equivalent_exch_amount");
	 }


      /**
        * Attribute factory method for payment_settled_amount
        *
        * For the Apply Payment (PAR) transaction of Receivable (REC) instrument,
        * the amount settled.
        */
	public static Attribute create_payment_settled_amount()
	 {
	  return (Attribute) new TradePortalDecimalAttribute("payment_settled_amount", "payment_settled_amount");
	 }


      /**
        * Attribute factory method for equivalent_exch_amt_ccy
        *
        * The currency associated with the equivalent exchange amount for the Transfer
        * Between Accounts.
        */
	public static Attribute create_equivalent_exch_amt_ccy()
	 {
	  return (Attribute) new ReferenceAttribute("equivalent_exch_amt_ccy", "equivalent_exch_amt_ccy", "CURRENCY_CODE", false, "TermsBeanAlias.equivalent_exch_amt_ccy");
	 }


      /**
        * Attribute factory method for transfer_fx_rate
        *
        * The foreign exchange rate used at the time of a transfer transaction.
        */
	public static Attribute create_transfer_fx_rate()
	 {
	  return (Attribute) new TradePortalDecimalAttribute("transfer_fx_rate", "transfer_fx_rate");
	 }


      /**
        * Attribute factory method for credit_account_oid
        *
        * the credit account involved in the transfer
        */
	public static Attribute create_credit_account_oid()
	 {
	  return (Attribute) new NumberAttribute("credit_account_oid", "credit_account_oid", false, "TermsBeanAlias.credit_account_oid");
	 }


      /**
        * Attribute factory method for debit_account_oid
        *
        * the debit account involed in the transfer
        */
	public static Attribute create_debit_account_oid()
	 {
	  return (Attribute) new NumberAttribute("debit_account_oid", "debit_account_oid", false, "TermsBeanAlias.debit_account_oid");
	 }


      /**
        * Attribute factory method for transfer_description
        *
        * A text description to be entered as an explanation of the payment for the
        * recipient.  It appears on the debit account's statement.
        */
	public static Attribute create_transfer_description()
	 {
	  return (Attribute) new Attribute("transfer_description", "transfer_description");
	 }


      /**
        * Attribute factory method for payment_charges_account_oid
        *
        * the Applicant's debit account that will be used to pay any charges related
        * to the instrument
        */
	public static Attribute create_payment_charges_account_oid()
	 {
	  return (Attribute) new NumberAttribute("payment_charges_account_oid", "payment_charges_account_oid", false, "TermsBeanAlias.payment_charges_account_oid");
	 }


      /**
        * Attribute factory method for debit_account_pending_amount
        *
        * Used to store the pending balance for the debit account.   The debit account
        * amount is stored in the currency of the account, so we can essentially reverse
        * out the amount upon rejection, cancellation and moreover upon release of
        * every transaction in OTL.
        */
	public static Attribute create_debit_account_pending_amount()
	 {
	  return (Attribute) new TradePortalDecimalAttribute("debit_account_pending_amount", "debit_account_pending_amount", false, "TermsBeanAlias.debit_account_pending_amount");
	 }


      /**
        * Attribute factory method for credit_account_pending_amount
        *
        * Used to store the pending balance for the credit account.   The crebit account
        * amount is stored in the currency of the account, so we can essentially reverse
        * out the amount upon rejection, cancellation and moreover upon release of
        * every transfer transaction in OTL.
        */
	public static Attribute create_credit_account_pending_amount()
	 {
	  return (Attribute) new TradePortalDecimalAttribute("credit_account_pending_amount", "credit_account_pending_amount", false, "TermsBeanAlias.credit_account_pending_amount");
	 }


      /**
        * Attribute factory method for debit_pending_amount_in_base
        *
        * Used to store the base amount applied to the Corporate Org Daily Limit Update
        * amount.  The debit account amount is stored in the base currency of the
        * Corporate Org, so we can back out the amount from the Daily Limit Update
        * upon rejection or cancellation.
        */
	public static Attribute create_debit_pending_amount_in_base()
	 {
	  return (Attribute) new TradePortalDecimalAttribute("debit_pending_amount_in_base", "debit_pending_amount_in_base", false, "TermsBeanAlias.debit_pending_amount_in_base");
	 }

      /**
        * Attribute factory method for display_fx_rate_method
        *
        * Used to store the fx rate method used with currency conversion as shown to end user
        */
	public static Attribute create_display_fx_rate_method()
	 {
	  //MDB Rel 8.1 5/10/12 LMUM011342545 - MULTIPLY_DIVIDE_IND refdata represents text description not M/D which is defined for this field in TradePortalConstants
	  return (Attribute) new Attribute("display_fx_rate_method", "display_fx_rate_method", false, "TermsBeanAlias.display_fx_rate_method");
//	  return (Attribute) new ReferenceAttribute("display_fx_rate_method", "display_fx_rate_method", "MULTIPLY_DIVIDE_IND", false, "TermsBeanAlias.display_fx_rate_method");
	 }

	//CR-586
      /**
        * Attribute factory method for confidential_indicator
        *
        * Used to store the confidential indicator used with CM transactions
        */
	public static Attribute create_confidential_indicator()
	 {
	  return (Attribute) new ReferenceAttribute("confidential_indicator", "confidential_indicator", "CONFIDENTIAL_IND", false, "TermsBeanAlias.confidential_indicator");
	 }
      /**
        * Attribute factory method for source_template_terms_oid
        *
        * Used to store the oid of source template's terms to be used with Fixed Payment instrument/transactions
        */

	public static Attribute create_source_template_trans_oid()
	 {
	  return (Attribute) new NumberAttribute("source_template_trans_oid", "source_template_trans_oid", false, "TermsBeanAlias.source_template_trans_oid");
	 }
    //CR-586

      /**
        * Attribute factory method for processed_amount
        *
        * Total Payment Amount that have been processed by the bank.  (Payment Amount
        * minus the Amount Rejected By Bank)
        */
	public static Attribute create_processed_amount()
	 {
	  return (Attribute) new NumberAttribute("processed_amount", "processed_amount");
	 }


      /**
        * Attribute factory method for rejected_amount
        *
        * The total of all the payment amounts rejected prior to TPS release.
        */
	public static Attribute create_rejected_amount()
	 {
	  return (Attribute) new NumberAttribute("rejected_amount", "rejected_amount");
	 }
//Leelavathi CR-657 Rel 7.1.0 6/27/2011 Begin

	public static Attribute create_individual_debit_ind()
	 {
	  return (Attribute) new IndicatorAttribute("individual_debit_ind", "individual_debit_ind");
	 }
//Leelavathi CR-657 Rel 7.1.0 6/27/2011 End
//CR-640
	/**
	 * Attribute factory method for request_market_rate_ind
	 *
	 * Indicates whether Foreign Exchange Online market rate can be requested
	 */
	public static Attribute create_request_market_rate_ind()
	{
		return (Attribute) new IndicatorAttribute("request_market_rate_ind", "request_market_rate_ind");
	}

	/*
	// TP Refresh Change
		public static Attribute create_request_market_rate_ind()
		 {
		  return (Attribute) new ReferenceAttribute("request_market_rate_ind", "request_market_rate_ind", "REQUEST_MARKET_RATE_IND", false, "TermsBeanAlias.request_market_rate_ind");
		 }
		// END TP Refresh Change
	*/
	
	
	

      /**
        * Attribute factory method for fx_online_deal_confirm_date
        *
        * Date/time when Foreign Exchange Online market rate was booked
        */
	public static Attribute create_fx_online_deal_confirm_date()
	 {
	  return (Attribute) new DateTimeAttribute("fx_online_deal_confirm_date", "fx_online_deal_confirm_date");
	 }
//Leelavathi - 10thDec2012 - Rel8200 CR-737 - Start
	

    /**
      * Attribute factory method for auto_extend
      *
      * If the user wishes that the credit be auto extended, then the check box
      * needs to be selected. The user can then specify the terms of the auto extension
      * for the instrument.
      */
	public static Attribute create_auto_extension_ind()
	 {
	  return (Attribute) new IndicatorAttribute("auto_extension_ind", "auto_extension_ind", false, "TermsBeanAlias.auto_extension_ind");
	 }
	
	/**
     * Attribute factory method for transferrable
     *
     * If this checkbox is selected, part or all of the instrument may be transferred
     * to another Beneficiary. For example, a portion of the instrument can be
     * re-issued in favour of the goods manufacturer.
     */
	public static Attribute create_max_auto_extension_allowed()
	 {
	  return (Attribute) new NumberAttribute("max_auto_extension_allowed", "max_auto_extension_allowed", false, "TermsBeanAlias.max_auto_extension_allowed");
	 }
	
	/**
     * Attribute factory method for display_fx_rate_method
     *
     * Used to store the fx rate method used with currency conversion as shown to end user
     */
	public static Attribute create_auto_extension_period()
	 {
	  return (Attribute) new ReferenceAttribute("auto_extension_period", "auto_extension_period", "AUTO_EXTEND_PER_TYPE", false, "TermsBeanAlias.auto_extension_period");
	 }
	/**
     * Attribute factory method for transferrable
     *
     * If this checkbox is selected, part or all of the instrument may be transferred
     * to another Beneficiary. For example, a portion of the instrument can be
     * re-issued in favour of the goods manufacturer.
     */
	public static Attribute create_auto_extension_days()
	 {
	  return (Attribute) new NumberAttribute("auto_extension_days", "auto_extension_days", false, "TermsBeanAlias.auto_extension_days");
	 }
	/**
     * Attribute factory method for display_fx_rate_method
     *
     * Used to store the fx rate method used with currency conversion as shown to end user
     */
	public static Attribute create_payment_type()
	 {
	  return (Attribute) new ReferenceAttribute("payment_type", "payment_type", "AVAIL_BY_TYPE", false, "TermsBeanAlias.payment_type");
	 }
	/**
     * Attribute factory method for display_fx_rate_method
     *
     * Used to store the fx rate method used with currency conversion as shown to end user
     */
	public static Attribute create_percent_amount_dis_ind()
	 {
	  return (Attribute) new ValueListAttribute("percent_amount_dis_ind", "percent_amount_dis_ind", new String[]{"P","A"}) ;
	 }
	
	/**
     * Attribute factory method for display_fx_rate_method
     *
     * Used to store the fx rate method used with currency conversion as shown to end user
     */
	public static Attribute create_special_tenor_text()
	 {
	 	  return (Attribute) new Attribute("special_tenor_text", "special_tenor_text", false, "TermsBeanAlias.special_tenor_text");
	 }
	 
//	Leelavathi - 10thDec2012 - Rel8200 CR-737 - End
        //RKAZI - 01/30/2013 - REL8.2 CR-709 - START
        /**
         * Attribute factory method for finance_type
         *
         * Code for the finance type, i.e., type of transaction that a LRQ is financing.
         */
        public static Attribute create_coms_chrgs_type()
        {
            return (Attribute) new ReferenceAttribute("coms_chrgs_type", "coms_chrgs_type", "COMS_CHARGES_TYPE", false, "TermsBeanAlias.coms_chrgs_type");
        }
        //RKAZI - 01/30/2013 - REL8.2 CR-709 - END
        /**
         * Attribute factory method for close_adn_deactivate
         *
         */
        public static Attribute create_close_and_deactivate()
        {
           return (Attribute) new IndicatorAttribute("close_and_deactivate", "close_and_deactivate", false, "TermsBeanAlias.close_and_deactivate");
        }
        
        //NARAYAN - 01/07/2014 - REL9.0 CR-831 - START        
        /**
         * Attribute factory method for Final expiry date of auto extension
         *
         * Final expiry date for auto extension of instrument
         */
 	    public static Attribute create_final_expiry_date()
 	    {
 	        return (Attribute) new DateTimeAttribute("final_expiry_date", "final_expiry_date", false, "TermsBeanAlias.final_expiry_date");
 	    }
 	    
        /**
         * Attribute factory method for notify beneficiary days of auto extension
         *
         * notify beneficiary days for auto extension of instrument
         */
 	    public static Attribute create_notify_bene_days()
 	    {
 	    	return (Attribute) new NumberAttribute("notify_bene_days", "notify_bene_days", false, "TermsBeanAlias.notify_bene_days");
 	    } 
 	    
        //NARAYAN - 01/07/2014 - REL9.0 CR-831 - END
 	    
 	    
 	   /** MEer Rel 9.3.5 CR-1026
         * Attribute factory method for tt reimbursement allowed indicator
         * This indicator is used to allow the customer to indicate whether TT Reimbursement is allowed or not allowed (prohibited).  
         *If not checked (default), TT Reimbursement is not allowed.  If checked, TT Reimbursement is allowed.
         * 
         */
 	   public static Attribute create_tt_reimbursement_allow_indicator()
 		 {
 		  return (Attribute) new IndicatorAttribute("tt_reimbursement_allow_ind", "tt_reimbursement_allow_ind", false, "TermsBeanAlias.tt_reimbursement_allow_ind");
 		 }
 	   
 	   // Nar CR-818 Rel9400 07/27/2015 Begin
 	   
 	  /**
 	     * Attribute factory method for our_reference
 	     *
 	     * Used to store our_reference for Settlement instruction
 	     */
 		public static Attribute create_our_reference()
 		 {
 		  return (Attribute) new Attribute("our_reference", "our_reference") ;
 		 }
        
        /**
         * Attribute factory method for apply_payment_on_date
         */
 	    public static Attribute create_apply_payment_on_date()
 	    {
 	        return (Attribute) new DateTimeAttribute("apply_payment_on_date", "apply_payment_on_date", false, "TermsBeanAlias.apply_payment_on_date");
 	    }
 	    
 	   /**
         * Attribute factory method for pay_in_full_fin_roll_ind
         */
 	    public static Attribute create_pay_in_full_fin_roll_ind()
		{
		  return (Attribute) new ReferenceAttribute("pay_in_full_fin_roll_ind", "pay_in_full_fin_roll_ind", "PAY_FULL_FIN_ROLL_TYPE");
		}
 	    
 	   /**
         * Attribute factory method for pay_in_full_fin_roll_ind
         */
 	    public static Attribute create_account_remit_ind()
		{
		  return (Attribute) new ReferenceAttribute("account_remit_ind", "account_remit_ind", "ACCOUNT_REMIT_TYPE");
		}
 	    
 	   /**
         * Attribute factory method for pay_in_full_fin_roll_ind
         */
 	    public static Attribute create_fin_roll_full_partial_ind()
		{
		  return (Attribute) new ReferenceAttribute("fin_roll_full_partial_ind", "fin_roll_full_partial_ind", "FIN_ROLL_FULL_PARTIAL_TYPE");
		}
 	    
 	   /**
         * Attribute factory method for principal_account_oid
         */
 	    public static Attribute create_principal_account_oid()
 	    {
 	    	return (Attribute) new NumberAttribute("principal_account_oid", "principal_account_oid");
 	    }
 	    
 	   /**
         * Attribute factory method for charges_account_oid
         */
 	    public static Attribute create_charges_account_oid()
 	    {
 	    	return (Attribute) new NumberAttribute("charges_account_oid", "charges_account_oid");
 	    }
 	    
 	   /**
         * Attribute factory method for principal_account_oid
         */
 	    public static Attribute create_interest_account_oid()
 	    {
 	    	return (Attribute) new NumberAttribute("interest_account_oid", "interest_account_oid");
 	    }
 	    
 	   /**
 	     * Attribute factory method for our_reference
 	     */
 		public static Attribute create_rollover_terms_ind()
 		 {
 		   return (Attribute) new ReferenceAttribute("rollover_terms_ind", "rollover_terms_ind", "ROLLOVER_FIN_TERMS_TYPE");
 		 }
 	    
 	   /**
         * Attribute factory method for fin_roll_number_of_days
         */
 	    public static Attribute create_fin_roll_number_of_days()
 	    {
 	    	return (Attribute) new NumberAttribute("fin_roll_number_of_days", "fin_roll_number_of_days", false, "TermsBeanAlias.fin_roll_number_of_days");
 	    }
 	    
 	   /**
         * Attribute factory method for pay_in_full_fin_roll_ind
         */
 	    public static Attribute create_fin_roll_curr()
		{
		  return (Attribute) new ReferenceAttribute("fin_roll_curr", "fin_roll_curr", "CURRENCY_CODE", false, "TermsBeanAlias.currency");
		}
 	    
 	   /**
         * Attribute factory method for fin_roll_maturity_date
         */
 	    public static Attribute create_fin_roll_maturity_date()
 	    {
 	        return (Attribute) new DateTimeAttribute("fin_roll_maturity_date", "fin_roll_maturity_date", false, "TermsBeanAlias.fin_roll_maturity_date");
 	    }
 	    
 	   /**
         * Attribute factory method for fin_roll_partial_pay_amt
         */
 	    public static Attribute create_fin_roll_partial_pay_amt()
 		{
 		  return (Attribute) new TradePortalDecimalAttribute("fin_roll_partial_pay_amt", "fin_roll_partial_pay_amt", false, "TermsBeanAlias.fin_roll_partial_pay_amt");
 		}
 	    
 	   /**
 	     * Attribute factory method for our_reference
 	     */
 		public static Attribute create_other_addl_instructions_ind()
 		 {
 		   return (Attribute) new IndicatorAttribute("other_addl_instructions_ind", "other_addl_instructions_ind");
 		 }
 	    
 	   /**
 	     * Attribute factory method for our_reference
 	     */
 		public static Attribute create_other_addl_instructions()
 		 {
 		  return (Attribute) new Attribute("other_addl_instructions", "other_addl_instructions") ;
 		 }
 	    
 	   /**
         * Attribute factory method for daily_rate_fec_oth_ind
         */
 	    public static Attribute create_daily_rate_fec_oth_ind()
		{
		  return (Attribute) new ReferenceAttribute("daily_rate_fec_oth_ind", "daily_rate_fec_oth_ind", "DAILY_RATE_FEC_TYPE");
		}
 	    	
 	   /**
 	     * Attribute factory method for settle_covered_by_fec_num
 	     */
 		public static Attribute create_settle_covered_by_fec_num()
 		 {
 		  return (Attribute) new Attribute("settle_covered_by_fec_num", "settle_covered_by_fec_num") ;
 		 }
 		
 		/**
         * Attribute factory method for settle_fec_rate
         */
 	    public static Attribute create_settle_fec_rate()
 	    {
 	    	return (Attribute) new TradePortalDecimalAttribute("settle_fec_rate", "settle_fec_rate", false, "TermsBeanAlias.settle_fec_rate");
 	    }
 	    
 	   /**
 	     * Attribute factory method for settle_fec_currency
 	     */
 		public static Attribute create_settle_fec_currency()
 		 {
 			return (Attribute) new ReferenceAttribute("settle_fec_currency", "settle_fec_currency", "CURRENCY_CODE", false, "TermsBeanAlias.currency");
 		 }
 		
 		/**
 	     * Attribute factory method for settle_other_fec_text
 	     */
 		public static Attribute create_settle_other_fec_text()
 		 {
 		  return (Attribute) new Attribute("settle_other_fec_text", "settle_other_fec_text") ;
 		 }
 	   
 	   // Nar CR-818 Rel9400 07/27/2015 End


    }

  /*
   * Register the components of the business object
   */
   protected void registerComponents() throws RemoteException, AmsException
   {
      /* Register the components defined in the Ancestor class */
      super.registerComponents();

      /* FirstTermsParty - A Terms business object can be associated to one to seven terms parties.
         Instead of creating a one-to-many association, seven one-to-one assocaitoins
         were created to make coding simpler.  Within these seven terms parties for
         any Terms object, the terms party type of each terms party will be unique.


         On the JSPs, a certain order of terms parties is assumed.  For example,
         the first terms party on an Issue Import LC is the Applicant.   On a standby,
         it might be the advising bank.   See each JSP for specifics.   The proper
         terms party can always be retrieved by examining the terms party type.

         For bank released terms, the applicant is the first terms party and the
         beneficiary is the second terms party. */
      registerOneToOneComponent("FirstTermsParty", "TermsParty", "c_FirstTermsParty");

      /* SecondTermsParty - A Terms business object can be associated to one to seven terms parties.
         Instead of creating a one-to-many association, seven one-to-one assocaitoins
         were created to make coding simpler.  Within these seven terms parties for
         any Terms object, the terms party type of each terms party will be unique.


         On the JSPs, a certain order of terms parties is assumed.  For example,
         the first terms party on an Issue Import LC is the Applicant.   On a standby,
         it might be the advising bank.   See each JSP for specifics.   The proper
         terms party can always be retrieved by examining the terms party type.

         For bank released terms, the applicant is the first terms party and the
         beneficiary is the second terms party. */
      registerOneToOneComponent("SecondTermsParty", "TermsParty", "c_SecondTermsParty");

      /* ThirdTermsParty - A Terms business object can be associated to one to seven terms parties.
         Instead of creating a one-to-many association, seven one-to-one assocaitoins
         were created to make coding simpler.  Within these seven terms parties for
         any Terms object, the terms party type of each terms party will be unique.


         On the JSPs, a certain order of terms parties is assumed.  For example,
         the first terms party on an Issue Import LC is the Applicant.   On a standby,
         it might be the advising bank.   See each JSP for specifics.   The proper
         terms party can always be retrieved by examining the terms party type.

         For bank released terms, the applicant is the first terms party and the
         beneficiary is the second terms party. */
      registerOneToOneComponent("ThirdTermsParty", "TermsParty", "c_ThirdTermsParty");

      /* FourthTermsParty - A Terms business object can be associated to one to seven terms parties.
         Instead of creating a one-to-many association, seven one-to-one assocaitoins
         were created to make coding simpler.  Within these seven terms parties for
         any Terms object, the terms party type of each terms party will be unique.


         On the JSPs, a certain order of terms parties is assumed.  For example,
         the first terms party on an Issue Import LC is the Applicant.   On a standby,
         it might be the advising bank.   See each JSP for specifics.   The proper
         terms party can always be retrieved by examining the terms party type.

         For bank released terms, the applicant is the first terms party and the
         beneficiary is the second terms party. */
      registerOneToOneComponent("FourthTermsParty", "TermsParty", "c_FourthTermsParty");

      /* FifthTermsParty - A Terms business object can be associated to one to six terms parties.
         Instead of creating a one-to-many association, six one-to-one assocaitoins
         were created to make coding simpler.  Within these six terms parties for
         any Terms object, the terms party type of each terms party will be unique.


         On the JSPs, a certain order of terms parties is assumed.  For example,
         the first terms party on an Issue Import LC is the Applicant.   On a standby,
         it might be the advising bank.   See each JSP for specifics.   The proper
         terms party can always be retrieved by examining the terms party type.

         For bank released terms, the applicant is the first terms party and the
         beneficiary is the second terms party. */
      registerOneToOneComponent("FifthTermsParty", "TermsParty", "c_FifthTermsParty");

      /* ShipmentTermsList -  */
      registerOneToManyComponent("ShipmentTermsList","ShipmentTermsList");

      /* SixthTermsParty - A Terms business object can be associated to one to seven terms parties.
         Instead of creating a one-to-many association, seven one-to-one assocaitoins
         were created to make coding simpler.  Within these seven terms parties for
         any Terms object, the terms party type of each terms party will be unique.


         On the JSPs, a certain order of terms parties is assumed.  For example,
         the first terms party on an Issue Import LC is the Applicant.   On a standby,
         it might be the advising bank.   See each JSP for specifics.   The proper
         terms party can always be retrieved by examining the terms party type.

         For bank released terms, the applicant is the first terms party and the
         beneficiary is the second terms party. */
      registerOneToOneComponent("SixthTermsParty", "TermsParty", "c_SixthTermsParty");

      /* SeventhTermsParty - A Terms business object can be associated to one to seven terms parties.
         Instead of creating a one-to-many association, seven one-to-one assocaitoins
         were created to make coding simpler.  Within these seven terms parties for
         any Terms object, the terms party type of each terms party will be unique.


         On the JSPs, a certain order of terms parties is assumed.  For example,
         the first terms party on an Issue Import LC is the Applicant.   On a standby,
         it might be the advising bank.   See each JSP for specifics.   The proper
         terms party can always be retrieved by examining the terms party type.

         For bank released terms, the applicant is the first terms party and the
         beneficiary is the second terms party. */
      registerOneToOneComponent("SeventhTermsParty", "TermsParty", "c_SeventhTermsParty");

      /* OrderingParty - Ordering Party for a CM instrument.

         Note this is using the physical column c_first_terms_party_oid to store
         the component association.  A Terms has either Terms Party component or
         Payment Party component, depending on the instrument type. */
      registerOneToOneComponent("OrderingParty", "PaymentParty", "c_OrderingParty");

      /* OrderingPartyBank - Ordering Party Bank for a CM instrument.

         Note this is using the physical column c_second_terms_party_oid to store
         the component association.  A Terms has either Terms Party component or
         Payment Party component, depending on the instrument type. */
      registerOneToOneComponent("OrderingPartyBank", "PaymentParty", "c_OrderingPartyBank");
    //Leelavathi - 10thDec2012 - Rel8200 CR-737 - Start
      registerOneToManyComponent("AdditionalReqDocList","AdditionalReqDocList");
      registerOneToManyComponent("PmtTermsTenorDtlList","PmtTermsTenorDtlList");
      //Leelavathi - 10thDec2012 - Rel8200 CR-737 - End
      }






}
