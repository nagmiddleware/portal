
package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import com.ams.tradeportal.busobj.util.InstrumentAuthentication;
import com.ams.tradeportal.common.InstrumentType;
import com.ams.tradeportal.common.SecurityRules;
import com.ams.tradeportal.common.TradePortalConstants;
import com.ams.tradeportal.common.cache.Cache;
import com.ams.tradeportal.common.cache.TPCacheManager;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.DataSourceFactory;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.frame.EJBObjectFactory;
import com.amsinc.ecsg.frame.QueryListView;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.JPylonProperties;
import com.amsinc.ecsg.util.StringFunction;




/*
 *
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class ClientBankBean extends ClientBankBean_Base
{
private static final Logger LOG = LoggerFactory.getLogger(ClientBankBean.class);

	private static String dbmsParm;
	static
	{
		try {
			JPylonProperties jPylonProperties = JPylonProperties.getInstance();
			dbmsParm = jPylonProperties.getString ("dataSourceName");
		} catch (Exception e) {
			dbmsParm = "";
		}
	}


   /**
    * This method takes the instrument type and returns the oid of the default template
    * for that instrument type
    *
    * @param instrumentType java.lang.String
    * @return long
    */
    public String getDefaultTemplateOid(String instrumentType) throws RemoteException, AmsException
    {
        if (instrumentType.equals(InstrumentType.IMPORT_DLC))
            return this.getAttribute("import_lc_def_tmplt_oid");
        else if (instrumentType.equals(InstrumentType.SHIP_GUAR))
            return this.getAttribute("shipping_guar_def_tmplt_oid");
        else if (instrumentType.equals(InstrumentType.AIR_WAYBILL))
            return this.getAttribute("air_waybill_def_tmplt_oid");
        else if (instrumentType.equals(InstrumentType.GUARANTEE))
            return this.getAttribute("guar_def_tmplt_oid");
        else if (instrumentType.equals(InstrumentType.EXPORT_COL))
            return this.getAttribute("export_coll_def_tmplt_oid");
        //Vasavi CR 524 03/31/2010 Begin
        else if (instrumentType.equals(InstrumentType.NEW_EXPORT_COL))
            return this.getAttribute("new_export_coll_def_tmplt_oid");
        //Vasavi CR 524 03/31/2010 End
        else if (instrumentType.equals(InstrumentType.STANDBY_LC))
            return this.getAttribute("standby_lc_def_tmplt_oid");
        else if (instrumentType.equals(InstrumentType.LOAN_RQST))
            return this.getAttribute("loan_req_def_tmplt_oid");
        else if (instrumentType.equals(InstrumentType.FUNDS_XFER))
            return this.getAttribute("funds_transfer_def_tmplt_oid");
        //IAZ CM-451 10/29/08 Begin
        else if (instrumentType.equals(InstrumentType.DOM_PMT))
            return getAttribute("domestic_payment_def_tmplt_oid");
        //IAZ CM-451 End
        else if (instrumentType.equals(InstrumentType.REQUEST_ADVISE))
           return this.getAttribute("request_advise_def_tmplt_oid");
        // Krishna CR 375-D ATP-ISS 07/21/2007 Begin
        else if (instrumentType.equals(InstrumentType.APPROVAL_TO_PAY))
            return this.getAttribute("approval_to_pay_def_tmplt_oid");
        // Krishna CR 375-D ATP-ISS 07/21/2007 End
        // CR-451 NShrestha 10/27/2008 Begin
        else if (instrumentType.equals(InstrumentType.XFER_BET_ACCTS))
            return this.getAttribute("xfer_bet_accts_def_tmplt_oid");
        // CR-451 NShrestha 10/27/2008 End
        // NSX CR-509 12/15/2009 Begin
        else if (instrumentType.equals(InstrumentType.DIRECT_DEBIT_INSTRUCTION))
            return this.getAttribute("DDI_def_tmplt_oid");
        // NSX CR-509 12/15/2009 End
        return null;
    }


   /**
    * This method takes the instrument type and returns the oid of the default template
    * for that instrument type
    *
    * @param instrumentType java.lang.String
    * @param templateOid java.lang.String
    * @return long
    */
    public void setDefaultTemplateOid(String instrumentType, String templateOid) throws RemoteException, AmsException
    {
        if (instrumentType.equals(InstrumentType.IMPORT_DLC))
            this.setAttribute("import_lc_def_tmplt_oid", templateOid);
        else if (instrumentType.equals(InstrumentType.SHIP_GUAR))
            this.setAttribute("shipping_guar_def_tmplt_oid", templateOid);
        else if (instrumentType.equals(InstrumentType.AIR_WAYBILL))
            this.setAttribute("air_waybill_def_tmplt_oid", templateOid);
        else if (instrumentType.equals(InstrumentType.GUARANTEE))
            this.setAttribute("guar_def_tmplt_oid", templateOid);
        else if (instrumentType.equals(InstrumentType.EXPORT_COL))
            this.setAttribute("export_coll_def_tmplt_oid", templateOid);
        //Vasavi CR 524 03/31/2010 Begin
        else if (instrumentType.equals(InstrumentType.NEW_EXPORT_COL))
            this.setAttribute("new_export_coll_def_tmplt_oid", templateOid);
        //Vasavi CR 524 03/31/2010 End
        else if (instrumentType.equals(InstrumentType.STANDBY_LC))
            this.setAttribute("standby_lc_def_tmplt_oid", templateOid);
        else if (instrumentType.equals(InstrumentType.LOAN_RQST))
            this.setAttribute("loan_req_def_tmplt_oid", templateOid);
        else if (instrumentType.equals(InstrumentType.FUNDS_XFER))
            this.setAttribute("funds_transfer_def_tmplt_oid", templateOid);
        //IAZ CM-451 10/29/08 Begin
        else if (instrumentType.equals(InstrumentType.DOM_PMT))
            this.setAttribute("domestic_payment_def_tmplt_oid", templateOid);
        //IAZ CM-451 Ends
        else if (instrumentType.equals(InstrumentType.REQUEST_ADVISE))
            this.setAttribute("request_advise_def_tmplt_oid", templateOid);
        // Krishna CR 375-D ATP-ISS 07/21/2007 Begin
        else if (instrumentType.equals(InstrumentType.APPROVAL_TO_PAY))
            this.setAttribute("approval_to_pay_def_tmplt_oid", templateOid);
        // Krishna CR 375-D ATP-ISS 07/21/2007 End
        // CR-451 NShrestha 10/27/2008 Begin
        else if (instrumentType.equals(InstrumentType.XFER_BET_ACCTS))
            this.setAttribute("xfer_bet_accts_def_tmplt_oid", templateOid);
        // CR-451 NShrestha 10/27/2008 End
        // NSX CR-509 12/15/2009 Begin
        else if (instrumentType.equals(InstrumentType.DIRECT_DEBIT_INSTRUCTION))
            this.setAttribute("DDI_def_tmplt_oid", templateOid);
        // NSX CR-509 12/15/2009 End
        else
            throw new AmsException("Cannot set default template oid on ClientBank for instrumentType -> " + instrumentType);
    }



	/**
	 * Retrieves a database connection from the data source factory. The
	 * connection is used to access the instrument number sequence table.
	 * <p>
	 * @return Connection - the database connection
	 */
	private Connection connect () throws AmsException
	{
		try
		{
			DataSource ds = DataSourceFactory.getDataSource (dbmsParm, true);
			return ds.getConnection ();
		}
		catch (SQLException e)
		{
			throw new AmsException (
				"Error on connecting to Datasource in InstrumentIDCache using DBMS " +
				"parms: " + dbmsParm + " Nested Exception: " + e.getMessage ());
		}
	}


  /**
   * This function returns the enxt available instrument number for this client
   * bank.  If the next available number cannot be determined or is outside the
   * allowable range, -1 is returned.  The method should be transaction to
   * ensure that a hold is placed on the table for the shortest amount of time
   * to prevent deadlocks.  This method uses straight JDBC to get and update the
   * instrument number table.
   * <p>
   * @return long - The next available instrument number
   */
   public long useNextInstrumentNumber() throws RemoteException
   {
      LOG.debug("Entering useNextInstrumentNumber()");
      long nextInstrumentNumber = -1;

      try {
        long start   = this.getAttributeLong("instr_id_range_start");
        long end     = this.getAttributeLong("instr_id_range_end");

	long instrNumSeqOid = this.getAttributeLong("c_InstrumentNumberSequence");


	

	try(Connection dbmsConnection = connect();
			Statement	statement = dbmsConnection.createStatement ()) {
		ResultSet results = null;
		
		dbmsConnection.setAutoCommit (false);
		// Get the last used instrument number.
		results = statement.executeQuery (
			"SELECT LAST_INSTRUMENT_ID_USED FROM INSTR_NUM_SEQ WHERE INSTR_NUM_SEQ_OID = "
				+ instrNumSeqOid
				+ " FOR UPDATE");
		results.next ();

		nextInstrumentNumber = results.getLong ("LAST_INSTRUMENT_ID_USED") + 1;

		// If the next available number is out of our range, reset it to -1
		// (indicating error condition).  Otherwise, update the database with
		// the number we just used.
		if (nextInstrumentNumber < start || nextInstrumentNumber > end) {
			nextInstrumentNumber = -1;
		} else {
			statement.executeUpdate (
				"UPDATE INSTR_NUM_SEQ SET LAST_INSTRUMENT_ID_USED = "
				+ nextInstrumentNumber
				+ " WHERE INSTR_NUM_SEQ_OID = " + instrNumSeqOid);
		}

	} catch (Exception e) {
		nextInstrumentNumber = -1;
	} 

       } catch (AmsException e) {
		nextInstrumentNumber = -1;
       }

	return nextInstrumentNumber;

   }


   /**
    * User validate checks to see that the client bank's name is unique
    */
   public void userValidate() throws java.rmi.RemoteException, com.amsinc.ecsg.frame.AmsException {

        if (this.hasChangesPending()) {
            String name = getAttribute("name");

            if (!isUnique("name", name, "")) {
                this.getErrorManager().issueError(
                                        TradePortalConstants.ERR_CAT_1,
                                        TradePortalConstants.ALREADY_EXISTS,
                                        name,
                                        attributeMgr.getAlias("name"));
            }

            String otlId = getAttribute("OTL_id");

            if (!isUnique("OTL_id", otlId, "")) {
                this.getErrorManager().issueError(
                                        TradePortalConstants.ERR_CAT_1,
                                        TradePortalConstants.ALREADY_EXISTS,
                                        otlId,
                                        attributeMgr.getAlias("OTL_id"));
            }

            // If admin users need password, make sure the password fields are set
            // and then check that all client bank/bog users have login/password.
            // Otherwise, make sure all client bank/bog users have certificates.
            String authMethod = this.getAttribute("authentication_method");
            // Nar CR-1071 Rel9350 08/2802015 - Add validation for PerUser also.
            if ( TradePortalConstants.AUTH_PASSWORD.equals(authMethod)|| TradePortalConstants.AUTH_PERUSER.equals(authMethod) ) {
                attributeMgr.requiredAttribute("bank_min_password_len");
                attributeMgr.requiredAttribute("bank_change_password_days");
                attributeMgr.requiredAttribute("bank_max_failed_attempts");
                // Nar CR-1071 Validate only for Password. Individual authentication can be set at bank label without user data check validation.
               if( TradePortalConstants.AUTH_PASSWORD.equals(authMethod) ) {            	   
                  if (!validateBankUserSecurityData(TradePortalConstants.AUTH_PASSWORD)) {
                    this.getErrorManager().issueError(
                                        TradePortalConstants.ERR_CAT_1,
                                        TradePortalConstants.SECURITY_MISSING_PASSWORDS);
                 }
               }
            } else if (authMethod.equals(TradePortalConstants.AUTH_CERTIFICATE)){
                if (!validateBankUserSecurityData(TradePortalConstants.AUTH_CERTIFICATE)) {
                    this.getErrorManager().issueError(
                                        TradePortalConstants.ERR_CAT_1,
                                        TradePortalConstants.SECURITY_MISSING_CERTIFICATES);
                }
            }
            else if (authMethod.equals(TradePortalConstants.AUTH_SSO)) {
                if (!validateBankUserSecurityData(TradePortalConstants.AUTH_SSO)) {
                    this.getErrorManager().issueError(
                                        TradePortalConstants.ERR_CAT_1,
                                        TradePortalConstants.SECURITY_MISSING_SSO);
                }
            }
            else if (authMethod.equals(TradePortalConstants.AUTH_2FA)) { // W Zhu 12/11/09 CR-482 Add 2FA
                if (!validateBankUserSecurityData(TradePortalConstants.AUTH_2FA)) {
                     this.getErrorManager().issueError(
                                         TradePortalConstants.ERR_CAT_1,
                                         TradePortalConstants.SECURITY_MISSING_2FA);
                }
            }

            // If we force corporations to have certificates, make sure each corp
            // is set for certificates.
            String corpsAuthMethod = this.getAttribute("corp_auth_method");
            if (corpsAuthMethod.equals(TradePortalConstants.AUTH_CERTIFICATE)) {
                if (!validateCorpUserSecurityData(TradePortalConstants.AUTH_CERTIFICATE)) {
                    this.getErrorManager().issueError(
                                        TradePortalConstants.ERR_CAT_1,
                                        TradePortalConstants.SECURITY_CORP_CUST_NO_CERT);
                }
            } else if (corpsAuthMethod.equals(TradePortalConstants.AUTH_SSO)) {
                if (!validateCorpUserSecurityData(TradePortalConstants.AUTH_SSO)) {
                    this.getErrorManager().issueError(
                                        TradePortalConstants.ERR_CAT_1,
                                        TradePortalConstants.SECURITY_CORP_CUST_NO_SSO);
                }
            } else if (corpsAuthMethod.equals(TradePortalConstants.AUTH_2FA)) {
                if (!validateCorpUserSecurityData(TradePortalConstants.AUTH_2FA)) {
                    this.getErrorManager().issueError(
                                        TradePortalConstants.ERR_CAT_1,
                                        TradePortalConstants.SECURITY_CORP_CUST_NO_2FA);
                }
            }
            //BSL 09/07/11 CR663 Rel 7.1 Begin
            else if (TradePortalConstants.AUTH_REGISTERED_SSO.equals(corpsAuthMethod)) {
                attributeMgr.requiredAttribute("corp_min_password_length");
                attributeMgr.requiredAttribute("corp_max_failed_attempts");

                if (!validateCorpUserSecurityData(TradePortalConstants.AUTH_REGISTERED_SSO)) {
                    this.getErrorManager().issueError(
                                        TradePortalConstants.ERR_CAT_1,
                                        TradePortalConstants.SECURITY_CORP_CUST_NO_REGISTERED_SSO);
                }
            }
            //BSL 09/07/11 CR663 Rel 7.1 End
            else {
                // Otherwise, the password fields are required.
                attributeMgr.requiredAttribute("corp_min_password_length");
                attributeMgr.requiredAttribute("corp_change_password_days");
                attributeMgr.requiredAttribute("corp_max_failed_attempts");
            }

            // cr498 11/22/2010 use instrument authentications
            // If we force corporations that can create Loan Request to have certificates, make sure
            // such corporations are set for certificates.
            //if (isAttributeModified("require_certs_loan_and_funds")) {
            if (isAttributeModified("require_tran_auth")) {
				validateRequireCertsLoanAndFunds();
			}

            // Validations of security details section (performed even if
            // using certificates).  If data is blank, we do not edit it.
            // (A required validation may be performed, as set above.)
            boolean errorWithCorporateCustomerArea = false;
            boolean errorWithAdminUserArea = false;

            if (!getAttribute("bank_min_password_len").equals("")
                && (SecurityRules.getBankPasswordMinimumLengthLimit()
                    > getAttributeInteger("bank_min_password_len"))) {
                errorWithAdminUserArea = true;
            }

            if (!getAttribute("bank_change_password_days").equals("")
                && (SecurityRules.getBankChangePasswordDaysLimit()
                    < getAttributeInteger("bank_change_password_days"))) {
                errorWithAdminUserArea = true;
            }

            if (!getAttribute("bank_max_failed_attempts").equals("")
                && (SecurityRules.getBankFailedLoginAttemptsLimit()
                    < getAttributeInteger("bank_max_failed_attempts"))) {
                errorWithAdminUserArea = true;
            }

			if (!getAttribute("bank_password_history_count").equals("")
				&& (SecurityRules.getBankPasswordHistoryCountLimit()
					< getAttributeInteger("bank_password_history_count"))) {
				errorWithAdminUserArea = true;
			}

            if (!getAttribute("corp_min_password_length").equals("")
                && (SecurityRules.getCorporatePasswordMinimumLengthLimit()
                    > getAttributeInteger("corp_min_password_length"))) {
                errorWithCorporateCustomerArea = true;
            }

            if (!getAttribute("corp_change_password_days").equals("")
                && (SecurityRules.getCorporateChangePasswordDaysLimit()
                    < getAttributeInteger("corp_change_password_days"))) {
                errorWithCorporateCustomerArea = true;
            }

            if (!getAttribute("corp_max_failed_attempts").equals("")
                && (SecurityRules.getCorporateFailedLoginAttemptsLimit()
                    < getAttributeInteger("corp_max_failed_attempts"))) {
                errorWithCorporateCustomerArea = true;
            }

			if (!getAttribute("corp_password_history_count").equals("")
				&& (SecurityRules.getCorporatePasswordHistoryCountLimit()
					< getAttributeInteger("corp_password_history_count"))) {
						errorWithCorporateCustomerArea = true;
			}

            // Create error messages
            if (errorWithCorporateCustomerArea) {
                this.getErrorManager().issueError(
                                TradePortalConstants.ERR_CAT_1,
                                TradePortalConstants.SECURITY_DETAILS_LIMITS,
                                getResourceManager().getText("ClientBankDetail.CorporateCustomers",
                                TradePortalConstants.TEXT_BUNDLE));
            }

            if (errorWithAdminUserArea) {
                this.getErrorManager().issueError(
                                TradePortalConstants.ERR_CAT_1,
                                TradePortalConstants.SECURITY_DETAILS_LIMITS,
                                getResourceManager().getText("ClientBankDetail.AdminUsers",
                                    TradePortalConstants.TEXT_BUNDLE));
            }


			if (getAttribute("corp_password_history").equals(TradePortalConstants.INDICATOR_YES) &&
			    getAttribute("corp_password_history_count").equals(""))
		    {
				this.getErrorManager().issueError(
								TradePortalConstants.ERR_CAT_1,
								TradePortalConstants.PASSWORD_HISTORY_COUNT,
								getResourceManager().getText("ClientBankDetail.CorporateCustomers",
									TradePortalConstants.TEXT_BUNDLE));
			}

			if (getAttribute("bank_password_history").equals(TradePortalConstants.INDICATOR_YES) &&
				getAttribute("bank_password_history_count").equals(""))
			{
				this.getErrorManager().issueError(
								TradePortalConstants.ERR_CAT_1,
								TradePortalConstants.PASSWORD_HISTORY_COUNT,
								getResourceManager().getText("ClientBankDetail.AdminUsers",
									TradePortalConstants.TEXT_BUNDLE));
			}

            //IAZ CM-451 02/25/09 Begin
            //Do not allow to check Authorization Certificate Req for Final Approval if the authentication URL is not provided
			//cr498 11/22/2010 throw the error if any instrument transactions require authentication
            //if ((TradePortalConstants.INDICATOR_YES.equals(getAttribute("require_certs_fund_final_auth"))) &&
			// DK IR T36000028485 Rel8.4 BTMU 05/27/2014
			if ( (corpsAuthMethod.equals(TradePortalConstants.AUTH_CERTIFICATE) || corpsAuthMethod.equals(TradePortalConstants.AUTH_SSO)) 
					&& InstrumentAuthentication.requireTransactionAuthentication(
                     getAttribute("require_tran_auth"),
                     InstrumentAuthentication.ANY_TRAN_AUTH ) &&
                (!StringFunction.isNotBlank(getAttribute("authorization_cert_auth_URL"))))
            {
				this.getErrorManager().issueError(
								TradePortalConstants.ERR_CAT_1,
								TradePortalConstants.AUTH_URL_NOT_SPECIFIED);
			}
			//IAZ CM-451 02/25/09 End

            // Call this method to possibly update the instrument ID range
            updateLastUsed();
        }

	validateCityStateZip();
	validateRegistrationMessages();//BSL 08/31/11 CR663 Rel 7.1 Add
	// DK CR-640 Rel7.1 BEGINS
	JPylonProperties jPylonProperties = JPylonProperties.getInstance();
	String serverLocation             = jPylonProperties.getString("serverLocation");

	QueryListView bankQueryListView = (QueryListView) EJBObjectFactory.createClientEJB(serverLocation,"QueryListView");
	String bankSql = "select fx_online_avail_ind  from bank_organization_group where p_client_bank_oid = ?";
	bankQueryListView.setSQL(bankSql, new Object[]{this.getAttribute("organization_oid")});
	bankQueryListView.getRecords();

	DocumentHandler bankList = bankQueryListView.getXmlResultSet();

	List<DocumentHandler>  bankOrgList = bankList.getFragmentsList("/ResultSetRecord");

	for (DocumentHandler bankDoc :bankOrgList) {
		
		String sValue = bankDoc.getAttribute("/FX_ONLINE_AVAIL_IND");
		if(sValue.equals("Y") && (this.getAttribute("allow_live_market_rates_req")).equals("N")){
			this.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.FX_AVL_ON);

			break; //IR#LUUL120259744 - Rel7.1

		}
	}
	// DK CR-640 Rel7.1 ENDS
	 // jgadela  REL 8.3 IR T36000021109  [START] - If the new settings are changed from Yes to No, the system will now check if there are any Pending record
	String replStr = null;
	if (checkPendingRecordsInTheQue("corp_org_dual_ctrl_reqd_ind")) {
		replStr = this.getResourceManager().getText(
				"AdminCorpCust.CorpCust",
				TradePortalConstants.TEXT_BUNDLE);
		
		this.getErrorManager().issueError(
				TradePortalConstants.ERR_CAT_1,
				TradePortalConstants.CAN_NOT_TURN_OFF_DUAL_CONTROL,
				replStr);
    }
	
	if (checkPendingRecordsInTheQue("admin_user_dual_ctrl_reqd_ind")) {
		replStr = this.getResourceManager().getText(
				"UsersAndSecurityMenu.AdminUsers",
				TradePortalConstants.TEXT_BUNDLE);
		
		this.getErrorManager().issueError(
				TradePortalConstants.ERR_CAT_1,
				TradePortalConstants.CAN_NOT_TURN_OFF_DUAL_CONTROL,
				replStr);
   }
	
	if (checkPendingRecordsInTheQue("corp_user_dual_ctrl_reqd_ind")) {
		replStr = this.getResourceManager().getText(
				"ClientBankDetail.CorporateUsers",
				TradePortalConstants.TEXT_BUNDLE);
		
		this.getErrorManager().issueError(
				TradePortalConstants.ERR_CAT_1,
				TradePortalConstants.CAN_NOT_TURN_OFF_DUAL_CONTROL,
				replStr);
   }
	// jgadela  REL 8.3 IR T36000021109  [END]
	
	
	//MEer CR-934a Modifications to Mobile Banking access Indicator -- STARTS HERE
	if (isAttributeModified("mobile_banking_access_ind")){
		if(TradePortalConstants.INDICATOR_NO.equals(this.getAttribute("mobile_banking_access_ind"))){
			setCorpCustMobileBankingInd(getAttribute("organization_oid"));
		}
	}
	//MEer CR-934a Modifications to Mobile Banking access  Indicator --  ENDS HERE
	
	//SSikhakolli - Rel 9.3.5 CR 1029 - Begin
	//If "Enable Update center or Enable Session Sync HTML is enabled at any Bank Group's under this Client Bank, we should not allow to off the Enable Admin Update Center setting
	String newAdminUpdateCenterandSyncVal  = getAttribute("enable_admin_update_centre");
	String oldAdminUpdateCenterandSyncVal = DatabaseQueryBean.getObjectID("client_bank", "ENABLE_ADMIN_UPDATE_CENTRE" ,"organization_oid = ? ", new Object[]{this.getObjectID()});
	int count;
	
	if(TradePortalConstants.INDICATOR_NO.equalsIgnoreCase(newAdminUpdateCenterandSyncVal) && 
			TradePortalConstants.INDICATOR_YES.equalsIgnoreCase(oldAdminUpdateCenterandSyncVal)) { 
		count = DatabaseQueryBean.getCount( "ORGANIZATION_OID", "BANK_ORGANIZATION_GROUP", 
				"P_CLIENT_BANK_OID = ? and ENABLE_ADMIN_UPDATE_CENTRE  = ?", true, new Object[]{this.getObjectID(), TradePortalConstants.INDICATOR_YES});
		if(count > 0) {
			this.getErrorManager().issueError(
				TradePortalConstants.ERR_CAT_1,
				TradePortalConstants.CANNOT_UNCHECK_BANK_ADMIN_UPDATE_SYNC,
				this.getResourceManager().getText("ClientBankDetail.EnableAdministrativeUpdateCentre", TradePortalConstants.TEXT_BUNDLE),
				this.getResourceManager().getText("BankGroups.EnableAdministrativeUpdateCentre", TradePortalConstants.TEXT_BUNDLE));
	    }
		
		count = DatabaseQueryBean.getCount( "ORGANIZATION_OID", "BANK_ORGANIZATION_GROUP", 
				"P_CLIENT_BANK_OID = ? and ENABLE_SESSION_SYNC_HTML  = ?", true, new Object[]{this.getObjectID(), TradePortalConstants.INDICATOR_YES});
		if(count > 0) {
			this.getErrorManager().issueError(
				TradePortalConstants.ERR_CAT_1,
				TradePortalConstants.CANNOT_UNCHECK_BANK_ADMIN_UPDATE_SYNC,
				this.getResourceManager().getText("ClientBankDetail.EnableAdministrativeUpdateCentre", TradePortalConstants.TEXT_BUNDLE),
				this.getResourceManager().getText("BankGroups.EnableSessionSyncHTMLTag", TradePortalConstants.TEXT_BUNDLE));
	    }
	}
	//SSikhakolli - Rel 9.3.5 CR 1029 - End

   }


   /**
    * When saving a Client Bank, validate combination of City , State, Postal
    * Code
    */
	public void validateCityStateZip()
		  throws java.rmi.RemoteException, com.amsinc.ecsg.frame.AmsException
	{
		   int countTotalNoOfChars = 0;
		   String city = this.getAttribute("address_city");
		   if (city != null)
			   countTotalNoOfChars += city.length();
		   String state = this.getAttribute("address_state_province");
		   if (state != null)
			   countTotalNoOfChars += state.length();
		   String postalcode = this.getAttribute("address_postal_code");
		   if (postalcode != null)
			   countTotalNoOfChars += postalcode.length();
		    //Surrewsh IR-T36000043283 12/3/2015 Start 
		   //Validate total no. of characters
		   if (countTotalNoOfChars > TradePortalConstants.CITY_STATE_ZIP_PARTYBEAN_LENGTH)
		   {
			   this.errMgr.issueError(TradePortalConstants.ERR_CAT_1,
					   TradePortalConstants.TOO_MANY_CHARS_IN_COMB_PARTYBEAN);
		   }
	}
	       //Surrewsh IR-T36000043283 12/3/2015 End 
	//BSL 08/31/11 CR663 Rel 7.1 Begin
	/**
	 * Validate the Registration Header and Announcement
	 */
	public void validateRegistrationMessages() throws RemoteException, AmsException {
		// The registration header is limited to 500 characters
		String registrationHeader = getAttribute("registration_header");

		if (registrationHeader.length() > 500) {
			String endText = registrationHeader.substring(480, 500);
			getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.TEXT_TOO_LONG_SHOULD_END,
					resMgr.getText("ClientBankBeanAlias.registration_header", TradePortalConstants.TEXT_BUNDLE),
					endText, "");
		}

		// The registration announcement is limited to 2000 characters
		String registrationAnnouncement = getAttribute("registration_text");

		if (registrationAnnouncement.length() > 2000) {
			String endText = registrationAnnouncement.substring(1980, 2000);
			getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.TEXT_TOO_LONG_SHOULD_END,
					resMgr.getText("ClientBankBeanAlias.registration_text", TradePortalConstants.TEXT_BUNDLE),
					endText, "");
		}
	}
	//BSL 08/31/11 CR663 Rel 7.1 End

    /**
     * Performs a security validation test for the bank user authentication method.
     * If method is certificate, all users of the client bank and its bogs must have
     * a non-blank certificate.  If method is password, all users of the client bank
     * and its bogs must have a non-blank login id AND password.  In either case,
     * we only test ACTIVE users.  This test is only done if the method changes.
     *
     * @return boolean - true if all users pass the test, false otherwise
     * @param authMethod String - AUTH_PASSWORD or AUTH_CERTIFICATE constant.
     */
    public boolean validateBankUserSecurityData(String authMethod)
                throws RemoteException, AmsException
    {
        boolean passesTest = true;

        if (isAttributeModified("authentication_method")) {
        	//jgadela R91 IR T36000026319 - SQL INJECTION FIX
            StringBuilder whereClause = new StringBuilder();
            whereClause.append("(OWNERSHIP_LEVEL = '");
            whereClause.append(TradePortalConstants.OWNER_BANK);
            whereClause.append("' or OWNERSHIP_LEVEL = '");
            whereClause.append(TradePortalConstants.OWNER_BOG);
            whereClause.append("') and a_client_bank_oid= ?");
            whereClause.append(" and ACTIVATION_STATUS='");
            whereClause.append(TradePortalConstants.ACTIVE);
            whereClause.append("'");
            
            // Nar CR-1071 Rel9400 08/28/2015 Add- Begin
            // Comment below code which check respective value for authentication method. bow now with this CR enchancement, 
            //every user's authentication method will be check with 'AUTHENTICATION_METHOD' column.
            
           /* if (authMethod.equals(TradePortalConstants.AUTH_CERTIFICATE)) {
                whereClause.append(" and CERTIFICATE_ID IS NULL");
            } else if (authMethod.equals(TradePortalConstants.AUTH_PASSWORD)){
                whereClause.append(" and (USER_IDENTIFIER IS NULL or PASSWORD IS NULL)");
            } else if (authMethod.equals(TradePortalConstants.AUTH_SSO)) {
            	whereClause.append(" and SSO_ID IS NULL");
            } else if (authMethod.equals(TradePortalConstants.AUTH_2FA)) {
                whereClause.append(" and TOKEN_ID_2FA IS NULL");
            }*/
            
            whereClause.append(" and authentication_method != ? ");

           // Nar CR-1071 Rel9400 08/28/2015 Add- End
            int count = DatabaseQueryBean.getCount( "user_oid", "users", whereClause.toString(), false, new Object[]{this.getObjectID(), authMethod });
            passesTest = (count < 1);
        } else {
            debug(" Authentication method not modified, skipping security validation test");
        }

        return passesTest;
   }

    /**
     * Performs a security validation test for the corporations for this bank.
     * If banks requires corporates to have a certificate, all active corporate
     * customers of the client bank must be set to use certificates.  (Works
     * generically for passwords as well, though that test is not required.)
     * This test is only done if the method changes.
     *
     * @return boolean - true if unique, false otherwise
     * @param authMethod String - AUTH_PASSWORD or AUTH_CERTIFICATE constant.
     */
    private boolean validateCorpUserSecurityData(String authMethod)
                throws RemoteException, AmsException
    {
        boolean passesTest = true;

        if (isAttributeModified("corp_auth_method")) {
        	//jgadela R91 IR T36000026319 - SQL INJECTION FIX
            String whereClause = "AUTHENTICATION_METHOD != ?  and ACTIVATION_STATUS='"+TradePortalConstants.ACTIVE+
            "' and a_client_bank_oid = ? ";

            int count =
                DatabaseQueryBean.getCount( "organization_oid", "corporate_org",whereClause, false, authMethod, this.getObjectID());

            passesTest = (count < 1);
        } else {
            debug(" Corporate User Authentication method not modified, skipping security validation test");
        }

        return passesTest;
   }


   /**
	* Performs a security validation test for the corporations for this bank.
	* If a bank requires its corporate customers that can create loan requests
	* or funds transfer to have a certificate, all active corporate customers of
	* such sort must be set to use certificates. This test only needs to be done
	* when this requirement changes.
	*
	* @return boolean - true if validation passes, false otherwise
	*/
    // Narayan
   private void validateRequireCertsLoanAndFunds()
			   throws RemoteException, AmsException
   {
	   //boolean passesTest = true;

	   //cr498 change to use requireTranAuth instrument specific authentications
	   String requireTranAuth = this.getAttribute("require_tran_auth");
	   // Narayan IR-KRUK120152767 Begin
	   if ( InstrumentAuthentication.requireTransactionAuthentication(
			   requireTranAuth, InstrumentAuthentication.TRAN_AUTH__LRQ_ISS ) ) {
		   validateUserAuthRights("allow_loan_request", getResourceManager().getText("ClientBankDetail.InstrumentAuthLRQ",TradePortalConstants.TEXT_BUNDLE));
	   }

	   if ( InstrumentAuthentication.requireTransactionAuthentication(
			   requireTranAuth, InstrumentAuthentication.TRAN_AUTH__RECEIVABLES_MATCH_RESPONSE ) ||InstrumentAuthentication.requireTransactionAuthentication(
			   requireTranAuth, InstrumentAuthentication.TRAN_AUTH__RECEIVABLES_APPROVE_DISCOUNT )||InstrumentAuthentication.requireTransactionAuthentication(
			   requireTranAuth, InstrumentAuthentication.TRAN_AUTH__RECEIVABLES_CLOSE_INVOICE )||InstrumentAuthentication.requireTransactionAuthentication(
			   requireTranAuth, InstrumentAuthentication.TRAN_AUTH__RECEIVABLES_FIN_INVOICE )||InstrumentAuthentication.requireTransactionAuthentication(
			   requireTranAuth, InstrumentAuthentication.TRAN_AUTH__RECEIVABLES_DISPUTE_INVOICE )) {
		   validateUserAuthRights("allow_arm", getResourceManager().getText("ClientBankDetail.InstrumentAuthReceivables",TradePortalConstants.TEXT_BUNDLE));
	   }

	   if ( InstrumentAuthentication.requireTransactionAuthentication(
			   requireTranAuth, InstrumentAuthentication.TRAN_AUTH__FTBA_ISS ) ) {
		   validateUserAuthRights("allow_xfer_btwn_accts", getResourceManager().getText("ClientBankDetail.InstrumentAuthFTBA",TradePortalConstants.TEXT_BUNDLE));
	   }
	   if ( InstrumentAuthentication.requireTransactionAuthentication(
			   requireTranAuth, InstrumentAuthentication.TRAN_AUTH__SHP_ISS ) ) {
		   validateUserAuthRights("allow_shipping_guar", getResourceManager().getText("ClientBankDetail.InstrumentAuthSHP",TradePortalConstants.TEXT_BUNDLE));
	   }

	   if ( InstrumentAuthentication.requireTransactionAuthentication(
			   requireTranAuth, InstrumentAuthentication.TRAN_AUTH__FTDP_ISS ) ) {
		   validateUserAuthRights("allow_domestic_payments", getResourceManager().getText("ClientBankDetail.InstrumentAuthFTDP",TradePortalConstants.TEXT_BUNDLE));
	   }

	   if ( InstrumentAuthentication.requireTransactionAuthentication(
			   requireTranAuth, InstrumentAuthentication.TRAN_AUTH__IMP_DLC_ISS )||InstrumentAuthentication.requireTransactionAuthentication(
			   requireTranAuth, InstrumentAuthentication.TRAN_AUTH__IMP_DLC_AMD )||InstrumentAuthentication.requireTransactionAuthentication(
			   requireTranAuth, InstrumentAuthentication.TRAN_AUTH__IMP_DLC_DCR )) {
		   validateUserAuthRights("allow_import_DLC", getResourceManager().getText("ClientBankDetail.InstrumentAuthIMP_DLC",TradePortalConstants.TEXT_BUNDLE));
	   }
	   if ( InstrumentAuthentication.requireTransactionAuthentication(
			   requireTranAuth, InstrumentAuthentication.TRAN_AUTH__EXP_COL_ISS ) ||InstrumentAuthentication.requireTransactionAuthentication(
					   requireTranAuth, InstrumentAuthentication.TRAN_AUTH__EXP_COL_AMD )) {
		   validateUserAuthRights("allow_export_collection", getResourceManager().getText("ClientBankDetail.InstrumentAuthEXP_COL",TradePortalConstants.TEXT_BUNDLE));
	   }

	   if ( InstrumentAuthentication.requireTransactionAuthentication(
			   requireTranAuth, InstrumentAuthentication.TRAN_AUTH__AIR_REL ) ) {
		   validateUserAuthRights("allow_airway_bill", getResourceManager().getText("ClientBankDetail.InstrumentAuthAIR",TradePortalConstants.TEXT_BUNDLE));
	   }

	   if ( InstrumentAuthentication.requireTransactionAuthentication(
			   requireTranAuth, InstrumentAuthentication.TRAN_AUTH__ATP_ISS )|| InstrumentAuthentication.requireTransactionAuthentication(
			   requireTranAuth, InstrumentAuthentication.TRAN_AUTH__ATP_AMD ) || InstrumentAuthentication.requireTransactionAuthentication(
			   requireTranAuth, InstrumentAuthentication.TRAN_AUTH__ATP_APR )) {
		   validateUserAuthRights("allow_approval_to_pay", getResourceManager().getText("ClientBankDetail.InstrumentAuthATP",TradePortalConstants.TEXT_BUNDLE));
	   }
	   if ( InstrumentAuthentication.requireTransactionAuthentication(
			   requireTranAuth, InstrumentAuthentication.TRAN_AUTH__EXP_OCO_ISS ) || InstrumentAuthentication.requireTransactionAuthentication(
			   requireTranAuth, InstrumentAuthentication.TRAN_AUTH__EXP_OCO_AMD )) {
		   validateUserAuthRights("allow_new_export_collection", getResourceManager().getText("ClientBankDetail.InstrumentAuthEXP_OCO",TradePortalConstants.TEXT_BUNDLE));
	   }

	   if ( InstrumentAuthentication.requireTransactionAuthentication(
			   requireTranAuth, InstrumentAuthentication.TRAN_AUTH__EXP_DLC_TRN ) ||InstrumentAuthentication.requireTransactionAuthentication(
			   requireTranAuth, InstrumentAuthentication.TRAN_AUTH__EXP_DLC_ATR )|| InstrumentAuthentication.requireTransactionAuthentication(
			   requireTranAuth, InstrumentAuthentication.TRAN_AUTH__EXP_DLC_ASN ) ||InstrumentAuthentication.requireTransactionAuthentication(
			   requireTranAuth, InstrumentAuthentication.TRAN_AUTH__EXP_DLC_DCR )) {
		   validateUserAuthRights("allow_export_LC", getResourceManager().getText("ClientBankDetail.InstrumentAuthEXP_DLC",TradePortalConstants.TEXT_BUNDLE));
	   }

	   if ( InstrumentAuthentication.requireTransactionAuthentication(
			   requireTranAuth, InstrumentAuthentication.TRAN_AUTH__GUAR_ISS)|| InstrumentAuthentication.requireTransactionAuthentication(
					   requireTranAuth, InstrumentAuthentication.TRAN_AUTH__GUAR_AMD)) {
		   validateUserAuthRights("allow_guarantee", getResourceManager().getText("ClientBankDetail.InstrumentAuthGUAR",TradePortalConstants.TEXT_BUNDLE));
	   }
	   if ( InstrumentAuthentication.requireTransactionAuthentication(
			   requireTranAuth, InstrumentAuthentication.TRAN_AUTH__DDI_ISS ) ) {
		   validateUserAuthRights("allow_direct_debit", getResourceManager().getText("ClientBankDetail.InstrumentAuthDDI",TradePortalConstants.TEXT_BUNDLE));
	   }

	   if ( InstrumentAuthentication.requireTransactionAuthentication(
			   requireTranAuth, InstrumentAuthentication.TRAN_AUTH__INC_SLC_DCR ) ) {
		   validateUserAuthRights("allow_non_prtl_org_instr_ind", getResourceManager().getText("ClientBankDetail.InstrumentAuthINC_SLC",TradePortalConstants.TEXT_BUNDLE));
	   }

	   if ( InstrumentAuthentication.requireTransactionAuthentication(
			   requireTranAuth, InstrumentAuthentication.TRAN_AUTH__FTRQ_ISS ) ) {
		   validateUserAuthRights("allow_funds_transfer", getResourceManager().getText("ClientBankDetail.InstrumentAuthFTRQ",TradePortalConstants.TEXT_BUNDLE));
	   }
	   if ( InstrumentAuthentication.requireTransactionAuthentication(
			   requireTranAuth, InstrumentAuthentication.TRAN_AUTH__SLC_ISS) ||InstrumentAuthentication.requireTransactionAuthentication(
					   requireTranAuth, InstrumentAuthentication.TRAN_AUTH__SLC_AMD) ||InstrumentAuthentication.requireTransactionAuthentication(
							   requireTranAuth, InstrumentAuthentication.TRAN_AUTH__SLC_DCR)  ) {
		   validateUserAuthRights("allow_SLC", getResourceManager().getText("ClientBankDetail.InstrumentAuthSLC",TradePortalConstants.TEXT_BUNDLE));
	   }

	   if ( InstrumentAuthentication.requireTransactionAuthentication(
			   requireTranAuth, InstrumentAuthentication.TRAN_AUTH__RQA_ISS ) ||InstrumentAuthentication.requireTransactionAuthentication(
			   requireTranAuth, InstrumentAuthentication.TRAN_AUTH__RQA_ISS )||InstrumentAuthentication.requireTransactionAuthentication(
			   requireTranAuth, InstrumentAuthentication.TRAN_AUTH__RQA_ISS )) {
		   validateUserAuthRights("allow_request_advise", getResourceManager().getText("ClientBankDetail.InstrumentAuthRQA",TradePortalConstants.TEXT_BUNDLE));
	   }
	   // Narayan IR-KRUK120152767 End
	   // Narayan --Delete (move logic into validateUserAuthRights function) Begin

	 
	   //Narayan ---Delete End

  }


   // Narayan IR-KRUK120152767 Begin
   private void validateUserAuthRights(String instrumentRights, String instrumentType )throws RemoteException, AmsException{

	  //jgadela R91 IR T36000026319 - SQL INJECTION FIX
       StringBuilder whereClause = new StringBuilder()
       	    .append("AUTHENTICATION_METHOD not in ('")
       	    .append(TradePortalConstants.AUTH_CERTIFICATE)
       	    .append("', '")
            .append(TradePortalConstants.AUTH_2FA) //W Zhu 12/11/09 CR-482 add 2FA
   			//.append("') and (allow_loan_request = '")
            .append("', '")
            .append(TradePortalConstants.AUTH_PERUSER)
            .append("') and (").append(instrumentRights).append("='")
   					.append(TradePortalConstants.INDICATOR_YES)
   			.append("') and ACTIVATION_STATUS='")
   			.append(TradePortalConstants.ACTIVE)
   			.append("' and a_client_bank_oid = ?");
	       int count =
	       DatabaseQueryBean.getCount( "organization_oid",
								   "corporate_org",
								   whereClause.toString(), false, new Object[]{this.getObjectID()});
	       if(!(count<1)){

	    	   this.getErrorManager().issueError(
	    			   TradePortalConstants.ERR_CAT_1,
	    			   TradePortalConstants.SECURITY_LRFT_CORP_NO_CERT,instrumentType);
	       }
   }



   // Narayan IR-KRUK120152767 End

   /**
    * Set the initial default values when a new object is created
    *
    */
   protected void userNewObject() throws RemoteException, AmsException
    {
	super.userNewObject();

        // Always default it so that users of this organization when logging in with
        // certificates must present a digital signature
        this.setAttribute("verify_logon_digital_sig", TradePortalConstants.INDICATOR_YES);
    }



    /**
     * This function checks to see if the value of this client bank's instr_id_range_start
     * attribute has changed.  If so, the instr_id_range_last_used attribute is set to the
     * value of the start attribute minus one
     */
    private void updateLastUsed () throws AmsException, RemoteException
    {
        InstrNumberSequence seq;

        String newSequenceNumber;

        // Only change it if the start of the range changed
        if (this.isAttributeModified("instr_id_range_start"))
        {
           try
            {
               // The new sequence number is the start of the range minus one
               newSequenceNumber = String.valueOf(this.getAttributeLong("instr_id_range_start") - 1);
            }
           catch (AmsException e)
            {
               // If there was any sort of problem (start of range set to blank, invalid number
               // format), then it will be caught through jPylon's validation.  Just set it to zero here
               newSequenceNumber = "0";
            }

           if(this.isNew)
            {
               // If the sequence doesn't exist yet, it must be created
               this.newComponent("InstrumentNumberSequence");
               seq = (InstrNumberSequence) this.getComponentHandle("InstrumentNumberSequence");
            }
           else
            {
                // Get a handle to the component
                seq = (InstrNumberSequence) this.getComponentHandle("InstrumentNumberSequence");
            }

            // Set the last instrument ID used
            seq.setAttribute("last_instrument_id_used", newSequenceNumber);
        }
    }

  /**
   * Hook that is called when a client bank is deactivated.
   *
   */
  protected void userDeactivate() throws RemoteException, AmsException
   {
     if (isAssociated("BankOrganizationGroup", "byActiveClientBank",
		new String[] {this.getAttribute("organization_oid"), TradePortalConstants.ACTIVE }))
		    this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
		         TradePortalConstants.ACTIVE_BOGS, getAttribute("name") );

     // check if there are any active users

     if (isAssociated("User", "activeByClientBank",
		new String[] {getAttribute("organization_oid"), TradePortalConstants.ACTIVE}))
		this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
		    TradePortalConstants.ACTIVE_USERS, getAttribute("name"));



   }
  // CR-451 TRudden 01/12/2009 Begin
  /**
   *  Use the preSave hook to clear cache prior to updating the database.
   *  This will ensure stale cache data is reloaded to reflect any maintenance change.
   */
  protected void preSave ()
            throws AmsException
  {
	  super.preSave();
	  try {
	      Cache cacheToFlush = (Cache)TPCacheManager.getInstance().getCache(TradePortalConstants.CLIENT_BANK_CACHE);
          if (cacheToFlush != null) {
        	  String cacheKey = getAttribute("organization_oid");
              cacheToFlush.remove(cacheKey);
           }
	  }
	  catch (RemoteException e)
	  {
			throw new AmsException (
				"Remote Exception on getAttribute.  Nested Exception: " + e.getMessage ());
		}
  }

  /**
   * populate the business object from reference cache
   *
   * @param OID The objectID of the object being instantiated.
   */
  public void getDataFromReferenceCache (long OID)
            throws AmsException
  {
	  String clientBankOID = Long.toString(OID);

	  Cache cache = (Cache)TPCacheManager.getInstance().getCache(TradePortalConstants.CLIENT_BANK_CACHE);
	  DocumentHandler result = (DocumentHandler)cache.get(clientBankOID);
      if (result != null) {
    	  populateDataFromResultSet(result);
      }

  }

  // CR-451 TRudden 01/12/2009 End
//jgadela  REL 8.3 IR T36000021109  [START] - If the new settings are changed from Yes to No, the system will now check if there are any Pending record
  
  /**
	 * this method find out whether where there are any pending reference data records in the que.
	 * @param attributeName
	 * @return true if there are any records
	 * @throws RemoteException
	 * @throws AmsException
	 */
  private boolean checkPendingRecordsInTheQue(String attributeName) throws AmsException, RemoteException{
        
        String newDualCtrlVal  = this.getAttribute(attributeName);
        // get the new/modified dual control setting value
        String oldDualCtrlVal = DatabaseQueryBean.getObjectID("client_bank", attributeName ,"organization_oid = ? ", new Object[]{this.getObjectID()});
        
        if(TradePortalConstants.INDICATOR_NO.equalsIgnoreCase(newDualCtrlVal) && !newDualCtrlVal.equalsIgnoreCase(oldDualCtrlVal)) { 
      	  //jgadela R91 IR T36000026319 - SQL INJECTION FIX
        	  List<Object> sqlParams = new ArrayList();
        	  StringBuilder dynamicWhere = new StringBuilder("");
              dynamicWhere.append(" select count(PENDING_DATA_OID) as COUNT from ref_data_pending a where ");
              String corpCustObjectsSql = getCorpCustObjectsSql();
              String adminUserIDsSql = getAdminUserObjectsSql();
              
              //Check for corporate organization
              if("corp_org_dual_ctrl_reqd_ind".equalsIgnoreCase(attributeName)){
                    dynamicWhere.append("  ( ( (a.CHANGED_OBJECT_OID in  ("+corpCustObjectsSql+") and CLASS_NAME in ('CorporateOrganization')) " +
                      " or (CHANGE_USER_OID in ("+adminUserIDsSql+") and CLASS_NAME in ('CorporateOrganization') and CHANGE_TYPE= 'C')     )");
                    sqlParams.add(this.getObjectID());
                    sqlParams.add(this.getObjectID());
              }//Check for Admin Users
              else if("admin_user_dual_ctrl_reqd_ind".equalsIgnoreCase(attributeName)){
              dynamicWhere.append("  (    ((a.CHANGED_OBJECT_OID in  ("+adminUserIDsSql+") and CLASS_NAME in 'User')" +
                          " or (CHANGE_USER_OID in ("+adminUserIDsSql+") and CLASS_NAME in ('User') and CHANGE_TYPE= 'C') )");
              		sqlParams.add(this.getObjectID());
              		sqlParams.add(this.getObjectID());
              }//Check for sers
              else if("corp_user_dual_ctrl_reqd_ind".equalsIgnoreCase(attributeName)){
                    dynamicWhere.append("  (    ((a.CHANGED_OBJECT_OID in  ("+getNonAdminUserObjectsSql()+") and CLASS_NAME in 'User')" +
                          " or (CHANGE_USER_OID in ("+getNonAdminUserObjectsSql()+") and CLASS_NAME in ('User') and CHANGE_TYPE= 'C') )");
                    sqlParams.add(this.getObjectID());
                    sqlParams.add(this.getObjectID());
              } 
              dynamicWhere.append(" )");;
              DocumentHandler resultSet = null;
              resultSet = DatabaseQueryBean.getXmlResultSet( dynamicWhere.toString(), false, sqlParams);
              int count = resultSet.getAttributeInt("/ResultSetRecord(0)/COUNT");
              if(count > 0) {
                    return true;
              }
        }
         return false;
    }
  
    /**
	 * this method build the query for admin users to check pending refdata record.
	 * @param 
	 * @return String - sql string
	 */
    private String getAdminUserObjectsSql() {
        
          StringBuilder sql = new StringBuilder("");
          sql.append("select a.user_oid from users a where activation_status = 'ACTIVE' ");
          sql.append(" and a.a_client_bank_oid = ? ");
          sql.append(" and (a.ownership_level = '");
          sql.append(TradePortalConstants.OWNER_BANK);
          sql.append("' or a.ownership_level = '");
          sql.append(TradePortalConstants.OWNER_BOG);
          sql.append("')");
        return sql.toString();
    }
    
    /**
 	 * this method build the query for corp cust to check pending refdata record.
 	 * @param 
 	 * @return String - sql string
 	 */
     private String getCorpCustObjectsSql() {
        

        String sql = "select   a.organization_oid from corporate_org a, bank_organization_group b where  a.a_bank_org_group_oid = b.organization_oid"
                   + " and b.p_client_bank_oid = ?  ";
        
        return sql;
    }
    
     /**
 	 * this method build the query for non adminusers check pending refdata record.
 	 * @param 
 	 * @return String - sql string
 	 */
     private String getNonAdminUserObjectsSql() {
              
                StringBuilder sql = new StringBuilder("");
                
                sql.append("select a.user_oid from users a where activation_status = 'ACTIVE' ");
                sql.append(" and a.a_client_bank_oid = ? ");
                sql.append(" and (a.ownership_level = '");
                sql.append(TradePortalConstants.OWNER_CORPORATE);
                sql.append("')");
              return sql.toString();
          }
  	// jgadela  REL 8.3 IR T36000021109  [END]
     
     
   //MEer CR-934a Modifications to Mobile Banking access Indicator -- STARTS HERE
     private void setCorpCustMobileBankingInd(String clientBankOid) throws AmsException{
    	 List<Object> sqlParams = new ArrayList();
    	 DocumentHandler resultSet = null; 
    	 String corpCustObjectsSql = getCorpCustObjectsSql();
    	 sqlParams.add(clientBankOid); 	
    	 resultSet = DatabaseQueryBean.getXmlResultSet( corpCustObjectsSql, false, sqlParams);
    	 if (  resultSet != null ) {
    		 LOG.info(" Inside setCorpCustMobileBankingInd()..  resultSet is: {}", resultSet.toString());
    		 List corgOrgList =  resultSet.getFragmentsList("/ResultSetRecord");
    		 for (int i = 0; i<corgOrgList.size(); i++ ) {
    			 DocumentHandler doc = (DocumentHandler) corgOrgList.get(i);
    			 String corpCustomerOrgId = doc.getAttribute("/ORGANIZATION_OID");
    			 if(StringFunction.isNotBlank(corpCustomerOrgId)){
    				 List<Object> updateSqlParams = new ArrayList();
    				 StringBuilder updateCustMobileBankingSql = new StringBuilder();
    				 
    				 updateCustMobileBankingSql.append("update corporate_org set mobile_banking_access_ind = '")
    				 .append(TradePortalConstants.INDICATOR_NO)    	                   	                       
    				 .append("'  where mobile_banking_access_ind = '")
    				 .append(TradePortalConstants.INDICATOR_YES)    	
    				 .append("' and	organization_oid = ? ");
    				 
    				 updateSqlParams.add(corpCustomerOrgId);
    				 
    				 LOG.info(" The updateCustMobileBankingSql  is: {}",updateCustMobileBankingSql.toString());
    				 try {
    					DatabaseQueryBean.executeUpdate(updateCustMobileBankingSql.toString(), false,  updateSqlParams);
    				 } catch (SQLException e) {
    					 LOG.error("Exception while updating Corporate Customer Mobile_Banking_Access_Indicator ",e);
    				 }
    			 }
    		 }
    	 }

     }
   //MEer CR-934a Modifications to Mobile Banking access Indicator -- ENDS HERE

    	 
    
}
