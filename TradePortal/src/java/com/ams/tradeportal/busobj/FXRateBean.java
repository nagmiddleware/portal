 
package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.rmi.RemoteException;

import com.ams.tradeportal.busobj.util.InstrumentServices;
import com.ams.tradeportal.common.TradePortalConstants;
import com.ams.tradeportal.common.cache.Cache;
import com.ams.tradeportal.common.cache.TPCacheManager;
import com.ams.tradeportal.mediator.PayRemitMediator;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.frame.ErrorManager;
import com.amsinc.ecsg.frame.ReferenceDataManager;
import com.amsinc.ecsg.util.DateTimeUtility;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.StringFunction;




/*
 * 
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class FXRateBean extends FXRateBean_Base
{
private static final Logger LOG = LoggerFactory.getLogger(FXRateBean.class);
 
   /**
    * Set the last updated date for the FX rate to the GMT time on
    * the database server.
    *
    * @exception   com.amsinc.ecsg.frame.AmsException
    */
   public void preSave() throws AmsException {
		super.preSave();
		try {
			// Set the last modified date to the current date
			setAttribute("last_updated_date", DateTimeUtility.getGMTDateTime());
		} catch (RemoteException e) {
			LOG.error("RemoteException occurred in FXRate preSave method: ",e);
		}

		Cache cacheToFlush = (Cache) TPCacheManager.getInstance().getCache(
				TradePortalConstants.FX_RATE_CACHE);
		if (cacheToFlush != null) {
			// Flush the entire cache since the cache key is a composite key and we
			// cannot simply pick the proper items to be removed.
			cacheToFlush.flush();
		}

	}

   public String getListCriteria(String listTypeName)
   {
      if (listTypeName.equals("byCorpOrgAndCurrency"))
	 return "p_owner_org_oid = {0} and currency_code = {1}"; //AAlubala Rel7.0.0.0 CR 610 - 03/2011 - Modified to use p_owner_org_oid
      return "";
   }


  /**
   * Performs any special "validate" processing that is specific to the
   * descendant of BusinessObject.
   *
   * This method verifies that rate is not zero.
   *
   * @see #validate()
   */

   protected void userValidate() throws AmsException, RemoteException
   {
	/**
	 Verify rate is not zero
	*/
	String currencyDescription = null;
	if (attributeMgr.isAttributeModified("rate"))
	{
	    if ((getAttributeDecimal("rate")).floatValue()==0f)
	    {
		currencyDescription = ReferenceDataManager.getRefDataMgr().getDescr("CURRENCY_CODE", 
		    getAttribute("currency_code"), csdb.getLocaleName());
		LOG.debug("FXRate cur desc {}" , currencyDescription);
 	        this.getErrorManager().issueError(getClass().getName(),
		    TradePortalConstants.INVALID_FX_RATE, getAttributeDecimal("rate").toString(), currencyDescription);
	    }
	}

        // Validate that the rate field will fit in the database
        InstrumentServices.validateDecimalNumber(getAttribute("rate"), 
                                         5,
                                         8,
                                         "FXRateBeanAlias.rate",
                                         this.getErrorManager(),
                                         this.getResourceManager());

	//IR - PHUJ012766360 - Begin
	//Validate that the buy rate field will fit in the database
	InstrumentServices.validateDecimalNumber(getAttribute("buy_rate"), 
					 5,
					 8,
					 "FXRateBeanAlias.buy_rate",
					 this.getErrorManager(),
					 this.getResourceManager());
	    
	//Validate that the sell rate field will fit in the database
	InstrumentServices.validateDecimalNumber(getAttribute("sell_rate"), 
        				 5,
        				 8,
        				 "FXRateBeanAlias.sell_rate",
        				 this.getErrorManager(),
        				 this.getResourceManager());
	//IR - PHUJ012766360 - End
	
	//IR - ACUJ030978538 - Begin
	//cquinton 3/22/2011 Rel 7.0.0 only do this validation on corporate org rates
    String ownershipLevel = getAttribute("ownership_level");
    if ( TradePortalConstants.OWNER_CORPORATE.equals(ownershipLevel) ) {
	    String ownerOrgOid= getAttribute("owner_org_oid");
	    CorporateOrganization corpOrg = (CorporateOrganization)createServerEJB("CorporateOrganization", Long.parseLong(ownerOrgOid));
	    if (TradePortalConstants.INDICATOR_YES.equals(corpOrg.getAttribute("allow_pymt_instrument_auth"))){
    		if (StringFunction.isBlank(getAttribute("buy_rate")) || StringFunction.isBlank(getAttribute("sell_rate"))) {
	    		getErrorManager().issueError(getClass().getName(), TradePortalConstants.BUY_SELL_RATE_REQD);
		    }
	    }
	}
	//IR - ACUJ030978538 - End
   }

  /**
   * Performs any special "issueOptimisticLockError" processing that is 
   * specific to the descendant of BusinessObject. (Note: this method was
   * originally coded for the purpose of specifying exactly which fx rate
   * object was updated (by someone else) in the case of multiple fx rates 
   * being updated at the same time. The default error (in 
   * BusinessObjectBean) uses the generic word 'item' which works fine for
   * objects being updated one at a time.)
   *
   * This method verifies that rate is not zero.
   *
   * @see #validate()
   */
   protected void issueOptimisticLockError() throws AmsException, RemoteException
   {
      this.getErrorManager().issueError(getClass().getName(), TradePortalConstants.FX_RATE_OPT_LOCK_EXCEPTION, 
                                        getAttribute("currency_code"));
   }
   
   // W Zhu 1/22/09 PNUI121964617 Add method getAmountInBaseCurrency.
   /**
    * This method returns an amount from a foreign currency to the
    * instrument's base currency. If the foreign exchange rate is not found
    * for the instrument's org, it will return a -1.0 which indicates an
    * error. Note: only the absolute value is returned. If the amount is
    * provided is negative, the amount returned is positive.
    * 
    * @param currency
    *            java.lang.String - the foreign currency
    * @param amount
    *            java.lang.String - the amount to convert to base currency
    * @param baseCurrency
    * @param userOrgOid
    * @param clientBankOid
    * @param errorManager
    * @return BigDecimal - the amount converted to base currency
    */
   //Ravindra - Rel8000 - 14thMar2012 - IR# VSUL080550054 - Begin
   static public BigDecimal getAmountInBaseCurrency(String currency,
            String amount, String baseCurrency, String userOrgOid, String clientBankOid,
            ErrorManager errorManager) throws AmsException,
            RemoteException {
	//Ravindra - Rel8000 - 14thMar2012 - IR# VSUL080550054 - End

        BigDecimal amountInForeign = (new BigDecimal(amount)).abs();
        BigDecimal amountInBase = new BigDecimal(-1.0f);
        DocumentHandler resultSet = null;
        DocumentHandler fxRate = null;
        String multiplyIndicator = null;
        BigDecimal rate = null;
      //LSuresh R91 IR T36000026319 - SQLINJECTION FIX
        
        String sql = "select rate, multiply_indicator from fx_rate "
                + "where p_owner_org_oid =  ?" //AAlubala Rel7.0.0.0 CR 610 - 03/2011 - Modify to use p_owner_org_oid
                + " and currency_code = ?"; 

        // Get the foreign exchange rate for the given userOrgOid
        // and foreign currency code

        if (baseCurrency.equals(currency)) {
            amountInBase = amountInForeign;
        } else {

            // get the foreign exchange rate. If it doesn't exist,
            // issue error. Otherwise, get the multiply indicator
            // and rate and convert the amount
            resultSet = DatabaseQueryBean.getXmlResultSet(sql, true,userOrgOid ,currency);
            //Ravindra - Rel8000 - 14thMar2012 - IR# VSUL080550054 - Begin
            if (resultSet == null && clientBankOid != null) {
            	sql = "select rate, multiply_indicator from fx_rate "
                    + "where p_owner_org_oid = ?"  
                    + " and currency_code =  ? ";
            	resultSet = DatabaseQueryBean.getXmlResultSet(sql, true,clientBankOid,currency);
            }
            //Ravindra - Rel8000 - 14thMar2012 - IR# VSUL080550054 - End
            
            if (resultSet == null) {
                errorManager.issueError(
                        PayRemitMediator.class.getName(),
                        TradePortalConstants.NO_FOREX_DEFINED);
            } else {
                fxRate = resultSet.getFragment("/ResultSetRecord");
                if (fxRate == null) {
                    errorManager.issueError(
                            PayRemitMediator.class.getName(),
                            TradePortalConstants.NO_FOREX_DEFINED);
                } else {
                    multiplyIndicator = fxRate
                            .getAttribute("/MULTIPLY_INDICATOR");
                    rate = fxRate.getAttributeDecimal("/RATE");
                }
            }

            if (multiplyIndicator != null && rate != null) {
                if (multiplyIndicator.equals(TradePortalConstants.DIVIDE))
                    amountInBase = amountInForeign.divide(rate,
                            BigDecimal.ROUND_HALF_UP);
                else
                    amountInBase = amountInForeign.multiply(rate);
            }
        }
        return amountInBase;

    }   
}
