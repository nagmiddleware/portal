
  

package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;
/**
 * 
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface PayRemitHome extends EJBHome
{
   public PayRemit create()
      throws RemoteException, CreateException, AmsException;

   public PayRemit create(ClientServerDataBridge csdb)
      throws RemoteException, CreateException, AmsException;
}
