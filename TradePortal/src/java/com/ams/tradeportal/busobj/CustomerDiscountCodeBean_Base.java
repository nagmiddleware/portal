package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;

import com.ams.tradeportal.busobj.TradePortalBusinessObjectBean;
import com.ams.tradeportal.common.*;
import javax.servlet.http.*;

/**
  Invoice data information uploaded will be stored here
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class CustomerDiscountCodeBean_Base extends TradePortalBusinessObjectBean  {
private static final Logger LOG = LoggerFactory.getLogger(CustomerDiscountCodeBean_Base.class);

	/*
	 * Register the attributes and associations of the business object
	 */
	   protected void registerAttributes() throws AmsException
	   {    

	      /* Register attributes defined in the Ancestor class */
	      super.registerAttributes();
	      /*CUSTOMER_DISC_CODE_OID*/
	      attributeMgr.registerAttribute("customer_disc_code_oid", "customer_disc_code_oid", "ObjectIDAttribute");	      
	      /* the discount - code */
	      attributeMgr.registerAttribute("discount_code", "discount_code");
	      /* The description */
	      attributeMgr.registerAttribute("discount_description", "discount_description");
	      /*  customer owner oid  */
	      attributeMgr.registerAttribute("p_owner_oid", "p_owner_oid");
		  /* Srinivasu_D CR#997 Rel9.3 03/10/2015 - Added default_disc_rate_ind
		   /*  default discount rate ind*/
		  attributeMgr.registerAttribute("default_disc_rate_ind", "default_disc_rate_ind", "IndicatorAttribute");
	      /*  deactivate indicator  */
	      attributeMgr.registerAttribute("deactivate_ind", "deactivate_ind", "IndicatorAttribute");	      
	       /* opt_lock - Optimistic lock attribute
	         See jPylon documentation for details on how this works */
	      attributeMgr.registerAttribute("opt_lock", "opt_lock", "OptimisticLockAttribute");	      
		      
	   }
		   		   
	}

