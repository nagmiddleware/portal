package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsException;

/**
 * Kyriba
 *
 */
public class ExternalBankBean_Base extends TradePortalBusinessObjectBean {
private static final Logger LOG = LoggerFactory.getLogger(ExternalBankBean_Base.class);
	
	/*
	   * Register the attributes and associations of the business object
	   */
	   protected void registerAttributes() throws AmsException
	   {
	      /* External Bank OID - Unique Identifier */
	      attributeMgr.registerAttribute("external_bank_oid", "EXTERNAL_BANK_OID", "ObjectIDAttribute");

	      /* Pointer to the parent CorporateOrganization */
	       attributeMgr.registerAttribute("corp_org_oid", "P_ORGANIZATION_OID", "ParentIDAttribute");
	       
	       /* Operational Bank Organization OID , This has an association with operationalbankorganization */	      
	       attributeMgr.registerAssociation("op_bank_org_oid", "A_OP_ORGANIZATION_OID", "OperationalBankOrganization");

	   }

}
