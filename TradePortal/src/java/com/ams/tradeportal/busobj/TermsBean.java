
package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.RemoteException;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import com.ams.tradeportal.busobj.termsmgr.TermsMgr;
import com.ams.tradeportal.busobj.util.InstrumentServices;
import com.ams.tradeportal.common.InstrumentType;
import com.ams.tradeportal.common.TradePortalConstants;
import com.ams.tradeportal.common.TransactionStatus;
import com.ams.tradeportal.common.TransactionType;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.Attribute;
import com.amsinc.ecsg.frame.AttributeManager;
import com.amsinc.ecsg.frame.ComponentEntry;
import com.amsinc.ecsg.frame.ComponentList;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.frame.ErrorManager;
import com.amsinc.ecsg.util.DateTimeUtility;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.StringFunction;





/*
 *
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class TermsBean extends TermsBean_Base
{
private static final Logger LOG = LoggerFactory.getLogger(TermsBean.class);


	// TLE - 11/09/06 - IR#ANUG110757179 - Add Begin
	private String importIndicator = "";
	// TLE - 11/09/06 - IR#ANUG110757179 - Add End


  /**
   *
   *
   * @param  doc
   * @param  componentIdentifer
   * @return TermsParty
   */
   public TermsParty populateTermsParty(DocumentHandler doc, String componentIdentifer) throws RemoteException
   {
	  TermsParty returnValue = null;
	  return returnValue;
   }



  /**
   * copy(...)
   *
   * this method takes a terms and copies all the information to this terms
   *
   * @param source Terms
   */
   public void copy(Terms source) throws RemoteException, AmsException
   {
       LOG.debug("Entering Terms.copy");

        //get the attributes from the source terms party and use them to set
        //the attributes on the new terms party
        if (source.getAttributeHash() == null)
            throw new AmsException("Null attribute hash returned");

        Vector nameValueArrays = InstrumentServices.attributeHashProcessor(source.getAttributeHash());

        if (nameValueArrays == null)
            throw new AmsException("nameValueArrays == null");

       LOG.debug ("nameValueArrays.size() -> {}" , nameValueArrays.size());
        String [] name = (String []) nameValueArrays.elementAt(0);
        String [] value = (String []) nameValueArrays.elementAt(1);

       LOG.debug ("name.length -> {}" , name.length);
       LOG.debug ("value.length -> {}" , value.length);

        for (int i=0; i<name.length; i++)
            LOG.debug ("{} - {}", name[i],value[i]);

        this.setAttributes(name, value);

        this.copyTermsPartyInfo(source);

		this.copyShipmentTerms(source);
		
		//SHR Rel 8.1 IR VIUL061757186 - Looks like there is no copy of PaymentParty.. Do it now
		//this.copyPaymentPartyInfo(source);  moved this call to copyTermsPartyInfo
		if (this.isAttributeRegistered("payment_instructions")){
			this.setAttribute("payment_instructions", null);
		}
	}

 //SHR Rel 8.1 IR VIUL061757186 Start
   /**
    * copyTermsPartyInfo(Terms source)
    *
    * This method makes a copy of all non-null terms parties for a Terms object
    *
    * @param source Terms
    */
   public void copyPaymentPartyInfo(Terms source) throws RemoteException, AmsException
   {
        LOG.debug("Creating terms party list");
        copyIndividualPaymentPartyInfo("c_OrderingParty", source);
        copyIndividualPaymentPartyInfo("c_OrderingPartyBank", source);
       
   }
   
   /**
    *
    * This method makes a copy of all non-null PaymentPartyIs for a Terms object
    *
    * @param source Terms
    */
    protected void copyIndividualPaymentPartyInfo(String partyId, Terms source) throws AmsException, RemoteException
    {
    	    //IR T36000016333 start - instead of using newComponent use createServerEJB
                      
            DocumentHandler tmpXML = new DocumentHandler();
     		String orgPartyOid = source.getAttribute(partyId);
     		if (StringFunction.isNotBlank(orgPartyOid))
     		{
     			PaymentParty originalParty = (PaymentParty) createServerEJB("PaymentParty");
     			originalParty.getData((source.getAttributeLong(partyId)));
     			LOG.debug("copyIndividualPaymentPartyInfo::copy from terms bean::orgPartyOid: {} " , orgPartyOid);
     			PaymentParty newParty = (PaymentParty) createServerEJB("PaymentParty");
     			long newPartyOid = newParty.newObject();   // RKAZI T36000017299 01/25/2014 - Added - store the new party oid.
     			originalParty.populateXmlDoc(tmpXML);
     			newParty.populateFromXmlDoc(tmpXML);
                newParty.setAttribute("payment_party_oid", String.valueOf(newPartyOid));   //RKAZI T36000017299 01/25/2014 - Added - need this other wise the new payment_party_oid is replaced with old one
     			newParty.save();
     			this.setAttribute(partyId, newParty.getAttribute("payment_party_oid"));   // RKAZI T36000017299 01/25/2014 - Added - set the new payment party oid on new terms c_ordering_party_oid
     		}
     		//IR T36000016333 end 
             
    }
 //SHR IR VIUL061757186 End

  /**
   * copyTermsPartyInfo(Terms source)
   *
   * This method makes a copy of all non-null terms parties for a Terms object
   *
   * @param source Terms
   */
   public void copyTermsPartyInfo(Terms source) throws RemoteException, AmsException
   {
        LOG.debug("Creating terms party list");
        copyIndividualTermsPartyInfo("FirstTermsParty", source);
        copyIndividualTermsPartyInfo("SecondTermsParty", source);
        copyIndividualTermsPartyInfo("ThirdTermsParty", source);
        copyIndividualTermsPartyInfo("FourthTermsParty", source);
        copyIndividualTermsPartyInfo("FifthTermsParty", source);
        copyIndividualTermsPartyInfo("OrderingParty", source);
        copyIndividualTermsPartyInfo("OrderingPartyBank", source);

   }


  /**
   * copyTermsPartyInfo(Terms source)
   *
   * This method makes a copy of all non-null terms parties for a Terms object
   *
   * @param source Terms
   */
   protected void copyIndividualTermsPartyInfo(String partyId, Terms source) throws AmsException, RemoteException
   {
            // Check to see if the terms party exists
                if(source.getAttribute("c_"+partyId).equals(""))
                 {
                    // Nothing to copy
                    return;
                 }

                TradePortalBusinessObject sourceParty = (TradePortalBusinessObject) source.getComponentHandle(partyId);
                //create a new terms party component and copy
                long oid = this.newComponent(partyId);
                TradePortalBusinessObject newTermsParty = (TradePortalBusinessObject) this.getComponentHandle(partyId);
//                newTermsParty.copy(sourceParty);
//              brought copy logic here to make this method independent of Party Type.
                Vector nameValueArrays = InstrumentServices.attributeHashProcessor(sourceParty.getAttributeHash());
                String [] name = (String []) nameValueArrays.elementAt(0);
                String [] value = (String []) nameValueArrays.elementAt(1);

                newTermsParty.setAttributes(name, value);
                LOG.debug("{} data copied to new terms party",partyId);

   }


   /**
	* copyShipmentTerms()
	*
	* This method makes a copy of all ShipmentTerms for a Terms object
	*
	* @param sourceTerms Terms
	*/
	protected void copyShipmentTerms(Terms sourceTerms) throws AmsException, RemoteException
	{
			//Copy ShipmentTerms
			ComponentList shipmentTermsList = (ComponentList) sourceTerms.getComponentHandle("ShipmentTermsList");
			if (shipmentTermsList != null)
			{
				int numberShipmentTerms = shipmentTermsList.getObjectCount();
				for (int i = 0; i < numberShipmentTerms; i++)
				{
					shipmentTermsList.scrollToObjectByIndex(i);
					ShipmentTerms sourceShipmentTerms = (ShipmentTerms) shipmentTermsList.getBusinessObject();
					long newShipmentTermsOid = this.newComponent("ShipmentTermsList");
					ShipmentTerms newShipmentTerms = (ShipmentTerms) this.getComponentHandle("ShipmentTermsList", newShipmentTermsOid);
					newShipmentTerms.copy(sourceShipmentTerms);

					if (i==0) {
						this.setAttribute("first_shipment_oid",String.valueOf(newShipmentTermsOid));
					}

					LOG.debug("ShipmentTerms # {} copied to new terms.",i);
				}
			}
		return;
	}



  /*
   * Perform processing for a New instance of the business object
   */
   protected void userNewObject() throws AmsException, RemoteException
   {
	  /* Perform any New Object processing defined in the Ancestor class */
	  super.userNewObject();

	  /* Default value for terms source type - set to component id */
		this.setAttribute("terms_source_type", (String)(this.getClientServerDataBridge().getCSDBValue("component_id")));
   }


  // Manager for this object.  Responsibility is delegated to this object
  // for dynamic attribute registration based on instrument type and transaction
  // type.  Manager will be a pointer to an instance of a subclass of TermsMgr.
  TermsMgr manager;

  /*
   * Register the attributes and associations of the business object
   */
   protected void registerDynamicAttributes() throws AmsException
   {
	  // Non-dynamic attributes - these always exist, no matter what type of terms
	  attributeMgr.registerAttribute( TermsBean.Attributes.create_terms_oid() );
	  attributeMgr.registerAttribute( TermsBean.Attributes.create_terms_source_type() );
	  attributeMgr.registerAttribute( TermsBean.Attributes.create_first_shipment_oid() );

	  // Local attribute - this is not saved in the data
	  // Set in TransactionMediator to contain the currency code of the original transaction
	  attributeMgr.registerAttribute( "original_trans_curr_code", "dummyfield", "LocalAttribute");


	  // Determine subclass of TermsMgr to use as the manager
	  String termsMgrSubClass = TermsMgr.getSubClassName(
									(String)getClientServerDataBridge().getCSDBValue("instrument_type"),
									(String)getClientServerDataBridge().getCSDBValue("transaction_type"),
									(String)getClientServerDataBridge().getCSDBValue("component_id"),
									(String)getClientServerDataBridge().getCSDBValue("standby_using_guarantee_form"));

	  // Use the Java reflection API to create the TermsMgr subclass as the manager, passing in
	  // a reference to AttributeMgr
	  try
	   {
		// Array of classes describes the signature of the constructor to call
		Class parameterClassArray[] = {AttributeManager.class};
		// Array of objects is the objects to be passed as parameters to the constructor
		Object parameterArray[] = {attributeMgr};

		// Create an instance
	      manager = (TermsMgr) Class.forName(termsMgrSubClass).getConstructor(parameterClassArray).newInstance(parameterArray);
	   }
	  catch (ClassNotFoundException | InstantiationException | java.lang.reflect.InvocationTargetException | NoSuchMethodException | IllegalAccessException e)
	   {
		    manager = null;
			LOG.error("TermsBean:: Exception at registerDynamicAttributes(): ",e);
	   }

	   // Now the locale must be set for all attributes
	   Enumeration attrs = this.attributeMgr.getAttributeHashtable().elements();

	   while(attrs.hasMoreElements())
	    {
	        Attribute attr = (Attribute) attrs.nextElement();

	        attr.setLocaleName(this.getClientServerDataBridge().getLocaleName());

	        // Convert the alias from a text resource key
	        // into the internationalized version of the alias
	        if(attr.getAlias() != null)
	            attr.setAlias(this.getResourceManager().getText(attr.getAlias(), TradePortalConstants.TEXT_BUNDLE));
	    }

   }


    /**
     * Calls validate on the terms manager associated with this Terms object.
     *
     */
  public void userValidate() throws java.rmi.RemoteException, com.amsinc.ecsg.frame.AmsException
   {
	manager.validate(this);

   }

    /**
     * First sends a preSave to the terms manager class to perform any special
     * presave processing.
     *
     * Then, examines the partiesToDelete vector in the terms
     * manager class.  If there any elements, these are terms parties that need to
     * be deleted.  We have to do this here in order to make the delete part of
     * the database transaction so that it can be rolled back properly if there is
     * an error.
     *
     * Second set first_shipment_oid when the first component in
     * ShipmentTermsList is added.
     */
    protected void preSave () throws AmsException
    {
        manager.preSave(this);

	try {
		// Delete any terms parties that need to be deleted.
		if (this.getErrorManager ().getMaxErrorSeverity () < ErrorManager.ERROR_SEVERITY)
		{
			Vector partiesToDelete = manager.getPartiesToDelete();
			TermsParty termsParty = null;

			for (int i=0; i< partiesToDelete.size(); i++) {
				String termsPartyName = (String) partiesToDelete.elementAt(i);

                                deleteComponent(termsPartyName);
			}
		}

		// Set first_shipment_oid when the first component in ShipmentTermsList is added.
		if (getAttribute("first_shipment_oid").equals(""))
		{
			// If one component has just been created in ShipmentTermsList then the
			// ComponentEntry for ShipmentTermsList must have been instantiated.
			// Otherwise, no need to get the component handle, which would cause the retrieving of data.
		    ComponentEntry component =	this.componentGetComponentEntry ("ShipmentTermsList");
		    if (component != null && component.isInstantiated ())
		    {
		    	ComponentList shipmentTermsList = (ComponentList) this.getComponentHandle("ShipmentTermsList");
				if (shipmentTermsList != null && shipmentTermsList.getObjectCount() > 0)
				{
					shipmentTermsList.scrollToObjectByIndex(0);
					this.setAttribute("first_shipment_oid", shipmentTermsList.getAttribute(shipmentTermsList.getIDAttributeName()));
				}
		    }
		}

	} catch (RemoteException e) {
		throw new AmsException("Remote exception found: " + e.toString());
	}

    }


   /**
     * Returns a DocumentHandler Doc populated with the values of the corresponding
     * Terms object. Uses TermsMgr to get the values depending on the type of Terms
     *
     * @return com.amsinc.ecsg.util.DocumentHandler
     * @param termsDoc com.amsinc.ecsg.util.DocumentHandler
     */
      public DocumentHandler getTermsAttributesDoc(DocumentHandler termsDoc) throws java.rmi.RemoteException, com.amsinc.ecsg.frame.AmsException
  	    {
  	 	 return(manager.getManagerTerms(this,termsDoc));
  	    }


  /**
   * Takes the values from an XML doc and sets the corresponding values
   * Terms object. Uses TermsMgr to set the values depending on the type of Terms
   *
   * @return boolean
   * @param inputDoc com.amsinc.ecsg.util.DocumentHandler
   */
       public boolean setTermsAttributesFromDoc(DocumentHandler inputDoc) throws java.rmi.RemoteException, com.amsinc.ecsg.frame.AmsException
       {
  		 boolean success = false;
  		 try
  		 {
  			success = manager.setManagerTerms(this,inputDoc);
  		 }
  		 catch(AmsException e)
  		 {
  			 LOG.error("Error saving the terms using Terms Manager : AMSException  " , e);
  			 return false;
  		 }
  		 catch(RemoteException e)
  		 {
  			 LOG.error("Error saving the terms using Terms Manager : RemoteException  " , e);
  			 return false;
  		 }

  		 catch(Exception e)
  		 {
  		 			 LOG.error("Error saving the terms using Terms Manager : Exception  " , e);
  		 			 return false;
  		 }

  		 return success;
  	 }

       public boolean unpackageWorkItem(DocumentHandler inputDoc) throws java.rmi.RemoteException, com.amsinc.ecsg.frame.AmsException
       {
  		 boolean success = false;
  		 try
  		 {
  			success = manager.setManagerTermsForWorkItem(this,inputDoc);
  		 }
  		 catch(Exception e)
  		 {
  		 			 LOG.error("Error saving the terms using Terms Manager : Exception  " , e);
  		 			 return false;
  		 }

  		 return success;
  	 }


/**
 * Returns the alias for an attribute on the terms object.  This
 * allows the TermMgr beans to get the alias for error messages.
 *
 * @return java.lang.String
 * @param attributeName java.lang.String
 */
 public String getAlias(String attributeName) {
	String alias = attributeMgr.getAlias(attributeName);
	if (alias == null || alias.equals("")) alias = attributeName;
	return alias;
 }

/**
 * Called from one of the Terms Manager classes, this method allows the
 * Terms Manager to register an attribute as required.  Since the
 * attributes are dynamic, only the TermsManager knows which attributes
 * should be required (and he doesn't have access to the attribute
 * manager).
 *
 * @param attributeName java.lang.String
 */
public void registerRequiredAttribute(String attributeName) throws AmsException {
	attributeMgr.requiredAttribute(attributeName);
 }

 /**
  *
  * This method set the expiry date of the Transaction given the latest
  * shipment date.
  *
  * @param lastShipmentDate
  * @throws RemoteException
  * @throws AmsException
  */
 public void setPODates(java.util.Date lastShipmentDate) throws RemoteException, AmsException
  {
		updateExpiryDateFromPO(null, lastShipmentDate);
		return;
  }

   /**
    *
    * This method update the expiry date of the Terms given the latest shipment
    * date if the derived expiry date is after the active expiry date.
    *
    * @param instrument - instrument
    * @param lastShipmentDate - lastShipmentDate
    * @throws RemoteException
    * @throws AmsException
    */
   public void updateExpiryDateFromPO(Instrument instrument, java.util.Date lastShipmentDate) throws RemoteException, AmsException
    {

        //set the expiry date
        long numMillis;    //the number of days to add measured in milliseconds
        String numDays = null;

        // Check to determine whether the "present_docs_within_days" attribute is registered; for
        // Import LC - Amend transactions, it won't be, so this is necessary to prevent exceptions
        // from occurring.
        if (this.isAttributeRegistered("present_docs_within_days"))
        {
           numDays = this.getAttribute("present_docs_within_days");
        }

		if (StringFunction.isBlank(numDays) && instrument!=null)
		{
		    numDays = instrument.getAttribute("present_docs_within_days");
		}

        if (StringFunction.isBlank(numDays))
            numMillis = TradePortalConstants.DEFAULT_PRESENTATION_DAYS*TradePortalConstants.MILLISECONDS_IN_DAY;
        else
            numMillis = (Long.parseLong(numDays))*TradePortalConstants.MILLISECONDS_IN_DAY;

        //the date measured in milliseconds from 00:00:00 01/01/1970
        long originalDate = Date.UTC(lastShipmentDate.getYear(),    lastShipmentDate.getMonth(),
                                     lastShipmentDate.getDate(),    lastShipmentDate.getHours(),
                                     lastShipmentDate.getMinutes(), lastShipmentDate.getSeconds());

        Date expiryDate = new Date (originalDate + numMillis);

        Date originalExpiryDate = null;
        if (instrument!=null  && StringFunction.isNotBlank(instrument.getAttribute("copy_of_expiry_date"))) 
		{
        	 originalExpiryDate = instrument.getAttributeDate("copy_of_expiry_date");
        }

        if (originalExpiryDate==null || expiryDate.after(originalExpiryDate))
        {
           this.setAttribute("expiry_date",
                           DateTimeUtility.convertDateToDateTimeString(expiryDate));
        }
        else
        {
        	this.setAttribute("expiry_date", "");
        }


    }

  /**
   * Called from TransactionMediator.  This method deletes a shipment (set of shipment terms)
   * and deletes any manually created PO's assigned to this shipment.  It will then unassign
   * any uploaded PO's assigned to this shipment.
   *
   * @param  instrumentOid
   * @param  transactionOid
   * @param  shipmentOid
   * @return void
   */
   public void deleteShipmentTerms(String instrumentOid, String transactionOid, String shipmentOid, String userOid)
         throws RemoteException, AmsException
   {
     try {

       ShipmentTerms shipmentTerms = (ShipmentTerms)getComponentHandle("ShipmentTermsList", Long.parseLong(shipmentOid));
       shipmentTerms.removeAssignedPOs(instrumentOid, transactionOid);
       //ShilpaR CR-707 Rel 8.0 Start
       String instrumentType = (String)getClientServerDataBridge().getCSDBValue("instrument_type");
       if ((InstrumentType.IMPORT_DLC).equals(instrumentType) || InstrumentType.APPROVAL_TO_PAY.equals(instrumentType)) {
       Map map = new HashMap();	
       map.put("user", userOid);
       map.put("status", TradePortalConstants.PO_STATUS_UNASSIGNED);
       map.put("action", TradePortalConstants.PO_ACTION_UNLINK);
       shipmentTerms.updateAssignedStrucPOStatus(instrumentOid, transactionOid, map);
       }
       //ShilpaR CR-707 Rel 8.0 End

       shipmentTerms.delete();

      } catch (RemoteException e) {
        throw new AmsException("Remote exception found: " + e.toString());
      }
   }

  /**
   * Convenience method to get the first shipment of a set of terms
   *
   * @return ShipmentTerms - the first shipment or null if one does not exist
   */
  public ShipmentTerms getFirstShipment() throws AmsException, RemoteException
    {
        ComponentList shipmentTermsList = (ComponentList) this.getComponentHandle("ShipmentTermsList");
        if(shipmentTermsList.getObjectCount() > 0)
        {
        	//T36000013934 - Rel8.1.0.6 - 04/29/2013 - <BEGIN> 
        	String firstShipmentOid = this.getAttribute("first_shipment_oid");
        	if (StringFunction.isNotBlank(firstShipmentOid)) 
        		return (ShipmentTerms) this.getComponentHandle("ShipmentTermsList", Long.parseLong(firstShipmentOid));
        	//T36000013934 - Rel8.1.0.6 - 04/29/2013 - <END>
        	shipmentTermsList.scrollToObjectByIndex(0);
        	return (ShipmentTerms) shipmentTermsList.getBusinessObject();
        }
        else
        	return null;
   }

  /**
   * Convenience method to create the first shipment for a set of terms
   *
   * @return ShipmentTerms - the first shipment that was created
   */
  public ShipmentTerms createFirstShipment() throws AmsException, RemoteException
   {
     long shipmentOid = this.newComponent("ShipmentTermsList");
     this.setAttribute("first_shipment_oid", String.valueOf(shipmentOid));
     return (ShipmentTerms) this.getComponentHandle("ShipmentTermsList", shipmentOid);
   }
  
  //Leelavathi - IR#T36000015642 10/05/2013 CR-737- Rel8200 - Start
  /**
   * Convenience method to createPmtTermsTenorDtl for a set of terms
   *
   * @return PmtTermsTenorDtl - createPmtTermsTenorDtl that was created
   */
  public PmtTermsTenorDtl createPmtTermsTenorDtl() throws AmsException, RemoteException
   {
     long pmtTermsOid = this.newComponent("PmtTermsTenorDtlList");
     return (PmtTermsTenorDtl) this.getComponentHandle("PmtTermsTenorDtlList", pmtTermsOid);
   }
//Leelavathi - IR#T36000015642 10/05/2013 CR-737- Rel8200 - End
  
//Leelavathi - IR#T36000018142 13/06/2013 CR-737- Rel8200 - Start
  /**
   * Convenience method to createAdditionalReqDocs for a set of terms
   *
   * @return AdditionalReqDocs - createAdditionalReqDocs that was created
   */
  public AdditionalReqDoc createAdditionalReqDoc() throws AmsException, RemoteException
   {
     long addReqDocOid = this.newComponent("AdditionalReqDocList");
     return (AdditionalReqDoc) this.getComponentHandle("AdditionalReqDocList", addReqDocOid);
   }
//Leelavathi - IR#T36000018142 13/06/2013 CR-737- Rel8200 - End


	// TLE - 11/09/06 - IR#ANUG110757179 - Add Begin
	public void setImportIndicator(String value) throws RemoteException, AmsException {
		importIndicator = value;
	}

	public String getImportIndicator() throws RemoteException, AmsException {
		return importIndicator;
	}
	// TLE - 11/09/06 - IR#ANUG110757179 - Add End

    //cquinton 4/8/2011 Rel 7.0.0 ir#bkul040547648 start
	/**
	 * Get the Terms Party by party type.
	 * Note this avoids an exception, which is typically thrown when getting
	 * a component that does not exist.
	 * @return the TermsParty if found, else null
	 */
	public TermsParty getTermsPartyByPartyType(String partyType) throws AmsException, RemoteException {
        if (partyType==null) {
            return null;
        }
        TermsParty termsParty = null;
        String componentName = "FirstTermsParty";
        if ( doesTermsPartyComponentExist(componentName)) {
            termsParty = (TermsParty) this.getComponentHandle(componentName);
            if ( termsParty!=null && partyType.equals(termsParty.getAttribute("terms_party_type"))) {
                return termsParty;
            }
        }
        componentName = "SecondTermsParty";
        if ( doesTermsPartyComponentExist(componentName)) {
            termsParty = (TermsParty) this.getComponentHandle(componentName);
            if ( termsParty!=null && partyType.equals(termsParty.getAttribute("terms_party_type"))) {
                return termsParty;
            }
        }
        componentName = "ThirdTermsParty";
        if ( doesTermsPartyComponentExist(componentName)) {
            termsParty = (TermsParty) this.getComponentHandle(componentName);
            if ( termsParty!=null && partyType.equals(termsParty.getAttribute("terms_party_type"))) {
                return termsParty;
            }
        }
        componentName = "FourthTermsParty";
        if ( doesTermsPartyComponentExist(componentName)) {
            termsParty = (TermsParty) this.getComponentHandle(componentName);
            if ( termsParty!=null && partyType.equals(termsParty.getAttribute("terms_party_type"))) {
                return termsParty;
            }
        }
        componentName = "FifthTermsParty";
        if ( doesTermsPartyComponentExist(componentName)) {
            termsParty = (TermsParty) this.getComponentHandle(componentName);
            if ( termsParty!=null && partyType.equals(termsParty.getAttribute("terms_party_type"))) {
                return termsParty;
            }
        }
        componentName = "SixthTermsParty";
        if ( doesTermsPartyComponentExist(componentName)) {
            termsParty = (TermsParty) this.getComponentHandle(componentName);
            if ( termsParty!=null && partyType.equals(termsParty.getAttribute("terms_party_type"))) {
                return termsParty;
            }
        }
        componentName = "SeventhTermsParty";
        if ( doesTermsPartyComponentExist(componentName)) {
            termsParty = (TermsParty) this.getComponentHandle(componentName);
            if ( termsParty!=null && partyType.equals(termsParty.getAttribute("terms_party_type"))) {
                return termsParty;
            }
        }
        return null;        
    }
    
	//cquinton 7/6/2011 Rel 7.0 - add version of getNextEmptyTermsParty()
	// to allow getting the next empty terms party starting at
	// an initial specified component
    /**
     * Get the first empty TermsParty -- this dynamically
     * allows a TermsParty to be the next available one.
     * @return
     * @throws AmsException
     * @throws RemoteException
     */
    public String getNextEmptyTermsParty() throws AmsException, RemoteException {
        //by default start by looking at the first terms party
        return getNextEmptyTermsParty( "FirstTermsParty" );
    }
    
	/**
	 * Get the first empty TermsParty -- this dynamically
	 * allows a TermsParty to be the next available one.
	 * @param componentId - the first terms party component to consider
	 *   should be one of 'FirstTermsParty', 'Second...', etc
	 * @return
	 * @throws AmsException
	 * @throws RemoteException
	 */
    public String getNextEmptyTermsParty(String componentId) throws AmsException, RemoteException {
        boolean componentFound = false;
        String componentName = "FirstTermsParty";
        if ( componentName.equals(componentId)) {
            componentFound = true;
        }
        if ( componentFound && !doesTermsPartyComponentExist(componentName ) ) {
            return componentName;
        }
        componentName = "SecondTermsParty";
        if ( componentName.equals(componentId)) {
            componentFound = true;
        }
        if ( componentFound && !doesTermsPartyComponentExist(componentName ) ) {
            return componentName;
        }
        componentName = "ThirdTermsParty";
        if ( componentName.equals(componentId)) {
            componentFound = true;
        }
        if ( componentFound && !doesTermsPartyComponentExist(componentName ) ) {
            return componentName;
        }
        componentName = "FourthTermsParty";
        if ( componentName.equals(componentId)) {
            componentFound = true;
        }
        if ( componentFound && !doesTermsPartyComponentExist(componentName ) ) {
            return componentName;
        }
        componentName = "FifthTermsParty";
        if ( componentName.equals(componentId)) {
            componentFound = true;
        }
        if ( componentFound && !doesTermsPartyComponentExist(componentName ) ) {
            return componentName;
        }
        componentName = "SixthTermsParty";
        if ( componentName.equals(componentId)) {
            componentFound = true;
        }
        if ( componentFound && !doesTermsPartyComponentExist(componentName ) ) {
            return componentName;
        }
        componentName = "SeventhTermsParty";
        if ( componentName.equals(componentId)) {
            componentFound = true;
        }
        if ( componentFound && !doesTermsPartyComponentExist(componentName ) ) {
            return componentName;
        }
        return null;                
    }

    /**
     * Does the given terms party (by component id) exist?
     * 
     * @param componentId
     * @return true if it exists, false otherwise.
     * @throws AmsException
     * @throws RemoteException
     */
    public boolean doesTermsPartyComponentExist(String componentId) throws AmsException, RemoteException {
        String attributeVal = this.getAttribute("c_"+componentId);
        if ( attributeVal != null && attributeVal.length()>0 ) {
            //component exists, get it
            return true;
        }
        return false;        
    }
    //cquinton 4/8/2011 Rel 7.0.0 ir#bkul040547648 end
    
    /** SHR CR708 Rel8.1.1 Update the Invoice status when some actions are performed
	 * @param instrumentOid
	 * @param transactionOid
	 * @throws AmsException
	 * @throws RemoteException
	 */
	public void updateAssignedInvoiceStatus(String instrumentOid, String transactionOid,Map map) throws AmsException, RemoteException
	  {
		String sql = "";
		if(map.get("isAmended") != null && (Boolean) map.get("isAmended")){
		sql = "UPDATE INVOICES_SUMMARY_DATA  SET INVOICE_STATUS= (case when invoice_type ='"+TradePortalConstants.REPLACEMENT+"' then '"+
		(String)map.get("amendStatus")+"' else '"+(String)map.get("status")+"' end) , deleted_ind = (case when invoice_type ='"+TradePortalConstants.REPLACEMENT+"' then '"
		+ TradePortalConstants.INDICATOR_YES + "' end),amend_seq_no =(case when invoice_type ='"+TradePortalConstants.REPLACEMENT+"' then '0' end),a_invoice_group_oid=? where upload_invoice_oid=?";
		}
		else if(TransactionStatus.CANCELLED_BY_BANK.equals((String) map.get("tranStatus")) 
				&& TransactionType.ISSUE.equals((String) map.get("tranType"))){
			sql = "UPDATE INVOICES_SUMMARY_DATA  SET INVOICE_STATUS= '"+(String)map.get("status")+
			"' , a_transaction_oid = null, a_instrument_oid = null, a_terms_oid = null,a_invoice_group_oid=? where upload_invoice_oid=?";

		}
		else{
			sql = "UPDATE INVOICES_SUMMARY_DATA  SET INVOICE_STATUS= '"+(String)map.get("status")+"',a_invoice_group_oid=? where upload_invoice_oid=?";
		}
		
		  String invSql = "select upload_invoice_oid,invoice_type from invoices_summary_data "
				 +"where a_terms_oid=?"; 
			DocumentHandler result = DatabaseQueryBean.getXmlResultSet(invSql, false, new Object[]{this.getAttribute("terms_oid")});
			if(result!=null){
			Vector rows = result.getFragments("/ResultSetRecord");
			String logAction ="";
			String invoiceStatus = null;			
		
	try {	 for (int j=0; j<rows.size(); j++) {
				 DocumentHandler row = (DocumentHandler) rows.elementAt(j);
				 String invOid = row.getAttribute("/UPLOAD_INVOICE_OID");
				 String invType = row.getAttribute("/INVOICE_TYPE");
				 InvoicesSummaryData inv = (InvoicesSummaryData) createServerEJB(
							"InvoicesSummaryData", Long.parseLong(invOid));				 
				//Srinivasu_D IR#T36000017280 Rel8.2 start - added below logic and commented out batch update
				 if(map.get("isAmended") != null && (Boolean) map.get("isAmended") && TradePortalConstants.REPLACEMENT.equals(invType)){
					 logAction=(String)map.get("amendAction");
					// logStatus=(String)map.get("amendStatus");
				 }
				 else{
					 logAction=(String)map.get("action");
					 //logStatus=(String)map.get("status");
				 }
				 //invoice.setAttribute("tp_rule_name",rule.getAttribute("buyer_name"));
				 if(map.get("isAmended") != null && (Boolean) map.get("isAmended") ){
					 if(TradePortalConstants.REPLACEMENT.equals(invType)){
						
						 invoiceStatus=(String)map.get("amendStatus");						 
						 inv.setAttribute("deleted_ind",TradePortalConstants.INDICATOR_YES);
						 inv.setAttribute("amend_seq_no","0");
					 }else
					 {  
						 invoiceStatus=(String)map.get("status");
					 }				 
				 }
				 else if(TransactionStatus.CANCELLED_BY_BANK.equals((String) map.get("tranStatus")) 
							&& TransactionType.ISSUE.equals((String) map.get("tranType"))){
					 
					 invoiceStatus=(String)map.get("status");
					 inv.setAttribute("terms_oid",null);
					 inv.setAttribute("instrument_oid",null);
					 inv.setAttribute("transaction_oid",null);					 
				 }
				 else{
					 
					 invoiceStatus = (String)map.get("status");
				 }
				 	inv.setAttribute("tp_rule_name",inv.getTpRuleName());
					inv.setAttribute("invoice_status", invoiceStatus);
					inv.setAttribute("status", invoiceStatus);
					inv.setAttribute("action", logAction);
					inv.setAttribute("user_oid",(String)map.get("user"));
					 inv.save();
					
			 }
			} catch (AmsException e) {
				LOG.error("Error occured during saving PO Upload Error log ...",	e);
			}
		  }
	  }

}
