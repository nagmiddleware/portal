
  

package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;
/**
 * Invoice Goods.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface InvoiceGoodsHome extends EJBHome
{
   public InvoiceGoods create()
      throws RemoteException, CreateException, AmsException;

   public InvoiceGoods create(ClientServerDataBridge csdb)
      throws RemoteException, CreateException, AmsException;
}
