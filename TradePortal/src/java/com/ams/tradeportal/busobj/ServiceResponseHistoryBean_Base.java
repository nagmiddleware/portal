
  
/*
 * This file is generated from the model.  Normally it should not be modified manually.  
 * Contact the modeler first.  (In a pinch, it can be modified first and then retrofitted 
 * to the model.)
 *
 */
package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;




/*
 * ServiceResponseHistory is used for tracking the Service Request sent from
 * Tradeportal to an external system.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class ServiceResponseHistoryBean_Base extends BusinessObjectBean
{
private static final Logger LOG = LoggerFactory.getLogger(ServiceResponseHistoryBean_Base.class);

  
  /* 
   * Register the attributes and associations of the business object
   */
   protected void registerAttributes() throws AmsException
   {  
      /* service_response_hist_oid - Unique identifier */
      attributeMgr.registerAttribute("service_response_hist_oid", "service_response_hist_oid", "ObjectIDAttribute");
      
      /* correlation_id - correlation_id sent in the request */
      attributeMgr.registerAttribute("correlation_id", "correlation_id");
      
      /* time_sent - Timestamp in GMT of when the request was sent */
      attributeMgr.registerAttribute("time_sent", "time_sent", "DateTimeAttribute");
      
      /* time_received - Timestamp in GMT of when the response was received */
      attributeMgr.registerAttribute("time_received", "time_received", "DateTimeAttribute");
      
      /* status - Lifecycle status of the message. */
      attributeMgr.registerReferenceAttribute("status", "status", "SERVICE_RESPONSE_STATUS");
      
      /* message_id - message_id sent in the request xml */
      attributeMgr.registerAttribute("message_id", "message_id");
      
      /* request_msg - Stores a copy of the data sent in a bank request. */
      attributeMgr.registerAttribute("request_msg", "request_msg");
      
      /* response_msg - Stores a copy of the data received from  a bank request. */
      attributeMgr.registerAttribute("response_msg", "response_msg");
      
      /* msg_type - The type of message to be processed.  This indicates what data is contained
         in the XML */
      attributeMgr.registerAttribute("msg_type", "msg_type");
      
      /* transaction_oid - The transaction which initiated the request */
      attributeMgr.registerAssociation("transaction_oid", "a_transaction_oid", "Transaction");
      
      /* user_oid - The user initiated the request */
      attributeMgr.registerAssociation("user_oid", "a_user_oid", "User");
      
   }
   
 
   
 
 
   
}
