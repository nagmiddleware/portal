package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.RemoteException;

import com.amsinc.ecsg.frame.AmsException;

/*
 *
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */

public class InvoiceSummaryGoodsBean extends InvoiceSummaryGoodsBean_Base {
private static final Logger LOG = LoggerFactory.getLogger(InvoiceSummaryGoodsBean.class);

	public void setAttribute(String attributeName, String value)
			throws AmsException, RemoteException {
		super.setAttribute(attributeName, value);

		// TODO future use
		if ("amount".equals(attributeName)) {
			boolean isCreditNote = value != null && value.startsWith("-");
		}
	}
}
