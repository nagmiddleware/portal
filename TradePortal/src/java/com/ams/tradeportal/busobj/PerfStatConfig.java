

  

package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;

import java.rmi.*;
import java.util.*;
import java.math.*;

import javax.ejb.*;

import com.ams.tradeportal.common.*;


/**
 * The configuration of the application.  These configuration entries used
 * to reside on the properties files.  They are moved to database for easier
 * management.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface PerfStatConfig extends TradePortalBusinessObject
{   
	public void getDataFromCache(String pageName, String clientBank) throws RemoteException, AmsException;

}
