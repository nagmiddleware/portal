 
package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;




/*
 * 
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class NotifyRuleCriterionBean extends NotifyRuleCriterionBean_Base
{
private static final Logger LOG = LoggerFactory.getLogger(NotifyRuleCriterionBean.class);

  /**
   * Descendent implemented method that identifies listing object
   * criteria.  
   * 
   * Used to identify one or more listTypes and list processing criteria.
   *
   * @param listTypeName The name of the listtype (as specified by the
   * developer)
   * @return The qsuedo SQL used for specifying the list criteria.
   *
   *			      
   */
  public String getListCriteria(String type)
  {
      if (type.equals("byEmailSetting"))
      {
	 return "p_notify_rule_oid = {0} and criterion_type = ''"+TradePortalConstants.NOTIF_RULE_EMAIL+ "''" +
			"and (setting = ''"+TradePortalConstants.NOTIF_RULE_ALWAYS+ "''" +
			"or setting = ''"+TradePortalConstants.NOTIF_RULE_CHRGS_DOCS_ONLY+"'')";

      }
      else if (type.equals("forCorpOrgTransaction"))
      {
	 return "p_notify_rule_oid = {0} and instr_notify_category = {1} and transaction_type = {2}" + 
		"and criterion_type = {3}";

      }
      return "";
  }
  
  @Override
	protected void userValidate() throws AmsException, RemoteException {
	  /*Overrride method added by Prateep  
	  TODO This is not correct way of doing clear required. But findings are going on to do with this on correct place. 
	  Remove this after finding correct solution.*/
	  attributeMgr.clearRequired();
	}

  
}
