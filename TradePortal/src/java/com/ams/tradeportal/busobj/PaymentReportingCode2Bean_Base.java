
  
/*
 * This file is generated from the model.  Normally it should not be modified manually.  
 * Contact the modeler first.  (In a pinch, it can be modified first and then retrofitted 
 * to the model.)
 *
 */
package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;


/*
 * Payment Reporting Code 2
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class PaymentReportingCode2Bean_Base extends TradePortalBusinessObjectBean
{
private static final Logger LOG = LoggerFactory.getLogger(PaymentReportingCode2Bean_Base.class);

  
  /* 
   * Register the attributes and associations of the business object
   */
   protected void registerAttributes() throws AmsException
   {  
      /* payment_reporting_code_2_oid - Unique Identifier */
      attributeMgr.registerAttribute("payment_reporting_code_2_oid", "payment_reporting_code_2_oid", "ObjectIDAttribute");
      
      /* code - Reporting Code */
      attributeMgr.registerAttribute("code", "code");
      attributeMgr.requiredAttribute("code");
      attributeMgr.registerAlias(
				"code",
				getResourceManager().getText(
						"PaymentReportingCode2BeanAlias.code",
						TradePortalConstants.TEXT_BUNDLE));
      
      /* short_description - Short Description */
      attributeMgr.registerAttribute("short_description", "short_description");
		//BSL IR #VIUL032443438 CR-655 03/28/11 - description and short_description should also be required
		attributeMgr.requiredAttribute("short_description");
		attributeMgr.registerAlias("short_description",
				getResourceManager().getText(
						"PaymentReportingCode2BeanAlias.short_description",
						TradePortalConstants.TEXT_BUNDLE));
      
      /* description - Description */
      attributeMgr.registerAttribute("description", "description");
		attributeMgr.requiredAttribute("description");
		attributeMgr.registerAlias("description",
				getResourceManager().getText(
						"PaymentReportingCode1BeanAlias.description",
						TradePortalConstants.TEXT_BUNDLE));
      
      /* locale_name - Locale of the language for description */
      attributeMgr.registerReferenceAttribute("locale_name", "locale_name", "LOCALE");
      
        /* Pointer to the parent BankOrganizationGroup */
      attributeMgr.registerAttribute("bank_group_oid", "p_bank_group_oid", "ParentIDAttribute");
   
   }
   
 
   
 
 
   
}
