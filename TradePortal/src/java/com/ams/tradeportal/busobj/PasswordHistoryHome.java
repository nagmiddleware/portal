
  

package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;
/**
 * Used to keep track of the passwords that have been used by a user.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface PasswordHistoryHome extends EJBHome
{
   public PasswordHistory create()
      throws RemoteException, CreateException, AmsException;

   public PasswordHistory create(ClientServerDataBridge csdb)
      throws RemoteException, CreateException, AmsException;
}
