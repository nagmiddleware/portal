package com.ams.tradeportal.busobj;
import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;


/**
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */

public interface InvoiceGroup extends TradePortalBusinessObject {

	/**
	 * Builds a string description of this group from the attributes
	 * used for grouping.
	 */
	public String getDescription() throws RemoteException, AmsException;

	/**
	 * Ensures this object will be saved, which increments the opt lock
	 */
	public void touch() throws RemoteException, AmsException;
}
