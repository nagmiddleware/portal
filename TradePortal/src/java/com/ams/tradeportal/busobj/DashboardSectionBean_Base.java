
  

package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;




/*
 * When a user logs out, a history record is created that represents their
 * session.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class DashboardSectionBean_Base extends TradePortalBusinessObjectBean
{
private static final Logger LOG = LoggerFactory.getLogger(DashboardSectionBean_Base.class);

  
  /* 
   * Register the attributes and associations of the business object
   */
   protected void registerAttributes() throws AmsException
   {  

      /* Register attributes defined in the Ancestor class */
      super.registerAttributes();
      
      /* dashboard_section_oid - Object identifier of this record */
      attributeMgr.registerAttribute("dashboard_section_oid", "dashboard_section_oid", "ObjectIDAttribute");
      
      /* user_oid -  */
      attributeMgr.registerAssociation("user_oid", "a_user_oid", "User");
      attributeMgr.requiredAttribute("user_oid");
      
      /* server_instance_name - The name of the server instance to which the user logged in. */
      attributeMgr.registerAttribute("display_order", "display_order", "NumberAttribute");
      attributeMgr.requiredAttribute("display_order");

      /* server_instance_name - The name of the server instance to which the user logged in. */
      attributeMgr.registerAttribute("section_id", "section_id");
      attributeMgr.requiredAttribute("section_id");
      
      /* opt_lock - Optimistic lock attribute
         See jPylon documentation for details on how this works */
      attributeMgr.registerAttribute("opt_lock", "opt_lock", "OptimisticLockAttribute");
   }
   
 
   
 
 
   
}
