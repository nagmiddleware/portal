
  

package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;
/**
 * The alias for the AR Buyer ID in an ARMatchingRule
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface TradePartnerMarginRulesHome extends EJBHome
{
   public TradePartnerMarginRules create()
      throws RemoteException, CreateException, AmsException;

   public TradePartnerMarginRules create(ClientServerDataBridge csdb)
      throws RemoteException, CreateException, AmsException;
}
