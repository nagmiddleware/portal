


package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;




/*
 * Templates and instruments are very similar in data, and are stored in the
 * same database tables.
 *
 * This is an abstract business object that is the superclass for both types
 * of instruments: templates and real instruments.   This business object contains
 * associations and attributes that are common to both templates and instruments.
 *
 *     Copyright  � 2003
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class InstrumentDataBean_Base extends TradePortalBusinessObjectBean
{
private static final Logger LOG = LoggerFactory.getLogger(InstrumentDataBean_Base.class);


  /*
   * Register the attributes and associations of the business object
   */
   protected void registerAttributes() throws AmsException
   {

      /* Register attributes defined in the Ancestor class */
      super.registerAttributes();

      /* instrument_type_code - Code for the instrument type */
      attributeMgr.registerReferenceAttribute("instrument_type_code", "instrument_type_code", "INSTRUMENT_TYPE");

      /* template_flag - Indicates whether or not the row in the INSTRUMENT table represents a template
         or a real instrument.   Yes=Template */
      attributeMgr.registerAttribute("template_flag", "template_flag", "IndicatorAttribute");

      /* instrument_oid - Unique identifier */
      attributeMgr.registerAttribute("instrument_oid", "instrument_oid", "ObjectIDAttribute");

      /* a_counter_party_oid - A pointer to the counterparty of this instrument or template.  Contains
         an OID from the TermsParty business object.

         This is an attribute and not a real jPylon association because the possibility
         exists that a terms party can be created at the same time as an InstrumentData
         entry.  In that situation, the association check would always fail. */
      attributeMgr.registerAttribute("a_counter_party_oid", "a_counter_party_oid", "NumberAttribute");

      /* original_transaction_oid - A pointer to the first transaction created for this instrument or template.
         For templates, this will be the only transaction.

         This is an attribute and not a real jPylon association because the possibility
         exists that a transaction can be created at the same time as an InstrumentData
         entry.  In that situation, the association check would always fail. */
      attributeMgr.registerAttribute("original_transaction_oid", "original_transaction_oid", "NumberAttribute");

      /* language - The language in which the instrument (or the instrument resulting from this
         template) will be issued. */
      attributeMgr.registerReferenceAttribute("language", "language", "INSTRUMENT_LANGUAGE");
      attributeMgr.requiredAttribute("language");
      attributeMgr.registerAlias("language", getResourceManager().getText("InstrumentDataBeanAlias.language", TradePortalConstants.TEXT_BUNDLE));

      /* import_indicator - Whether a Loan Request (LRQ) is for import.  'Y' for import and 'N' for
         export. */
      attributeMgr.registerAttribute("import_indicator", "import_indicator", "IndicatorAttribute");

      /* fixed_payment_flag - Indicates whether or not the instrument is a fixed payment instrument.

         Yes=template is a fixed payment instrument */
      attributeMgr.registerAttribute("fixed_payment_flag", "fixed_payment_flag", "IndicatorAttribute");


   }

  /*
   * Register the components of the business object
   */
   protected void registerComponents() throws RemoteException, AmsException
   {
      /* Register the components defined in the Ancestor class */
      super.registerComponents();

      /* TransactionList - Each instrument is comprised of one to many transactions.   There must always
         be one.  This component relationship represents that the transaction is
         a part of the instrument. */
      registerOneToManyComponent("TransactionList","TransactionList");
   }






}
