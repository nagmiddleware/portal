
  
/*
 * This file is generated from the model.  Normally it should not be modified manually.  
 * Contact the modeler first.  (In a pinch, it can be modified first and then retrofitted 
 * to the model.)
 *
 */
package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;


/*
 * Represents a purchase order that has been uploaded into the Trade Portal.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class PurchaseOrderBean_Base extends TradePortalBusinessObjectBean
{
private static final Logger LOG = LoggerFactory.getLogger(PurchaseOrderBean_Base.class);

  
  /* 
   * Register the attributes and associations of the business object
   */
   protected void registerAttributes() throws AmsException
   {  

      /* Register attributes defined in the Ancestor class */
      super.registerAttributes();
      
      /* purchase_order_oid - Unique identfier */
      attributeMgr.registerAttribute("purchase_order_oid", "purchase_order_oid", "ObjectIDAttribute");
      
      /* purchase_order_num - Field to store the purchase order number.   This field will be unique within
         all Purchase Orders for a particular corporate organization. */
      attributeMgr.registerAttribute("purchase_order_num", "purchase_order_num");
      
      /* purchase_order_type - This details the type of purchase order designation. */
      attributeMgr.registerAttribute("purchase_order_type", "purchase_order_type");
      
      /* opt_lock - Optimistic lock attribute */
      attributeMgr.registerAttribute("opt_lock", "opt_lock", "OptimisticLockAttribute");
      
      /* issue_date - Purchase Order Issue Date. */
      attributeMgr.registerAttribute("issue_date", "issue_date", "DateAttribute");
      
      /* latest_shipment_date - The latest shipment date of this PO line item */
      attributeMgr.registerAttribute("latest_shipment_date", "latest_shipment_date", "DateTimeAttribute");
      
      /* currency - The currency in which the amount is expressed. */
      attributeMgr.registerAttribute("currency", "currency");
      
      /* amount - The amount of the Purchase Order */
      attributeMgr.registerAttribute("amount", "amount", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      
      /* seller_name - The name of the seller for this Purchase Order. */
      attributeMgr.registerAttribute("seller_name", "seller_name");
      
      /* incoterm - The shipping term, or Incoterm, that describes what is included in the total
         cost of the transaction goods. For example, Cost and Freight or Cost Insurance
         Freight. */
      attributeMgr.registerReferenceAttribute("incoterm", "incoterm", "INCOTERM");
      attributeMgr.registerAlias("incoterm", getResourceManager().getText("PurchaseOrderBeanAlias.incoterm", TradePortalConstants.TEXT_BUNDLE));
      
      /* incoterm_location - field representing the Incoterm location associated with the incoterm type. */
      attributeMgr.registerAttribute("incoterm_location", "incoterm_location");
      attributeMgr.registerAlias("incoterm_location", getResourceManager().getText("PurchaseOrderBeanAlias.incoterm_location", TradePortalConstants.TEXT_BUNDLE));
      
      /* partial_shipment_allowed - If this checkbox is selected, partial shipments are allowed. */
      attributeMgr.registerAttribute("partial_shipment_allowed", "partial_shipment_allowed", "IndicatorAttribute");
      attributeMgr.registerAlias("partial_shipment_allowed", getResourceManager().getText("PurchaseOrderBeanAlias.partial_shipment_allowed", TradePortalConstants.TEXT_BUNDLE));
      
      /* buyer_user_def1_label - A buyer user-defined field label for Purchase Order */
      attributeMgr.registerAttribute("buyer_user_def1_label", "buyer_user_def1_label");
      
      /* buyer_user_def2_label - A buyer user-defined field label for Purchase Order */
      attributeMgr.registerAttribute("buyer_user_def2_label", "buyer_user_def2_label");
      
      /* buyer_user_def3_label - A buyer user-defined field label for Purchase Order */
      attributeMgr.registerAttribute("buyer_user_def3_label", "buyer_user_def3_label");
      
      /* buyer_user_def4_label - A buyer user-defined field label for Purchase Order */
      attributeMgr.registerAttribute("buyer_user_def4_label", "buyer_user_def4_label");
      
      /* buyer_user_def5_label - A buyer user-defined field label for Purchase Order */
      attributeMgr.registerAttribute("buyer_user_def5_label", "buyer_user_def5_label");
      
      /* buyer_user_def6_label - A buyer user-defined field label for Purchase Order */
      attributeMgr.registerAttribute("buyer_user_def6_label", "buyer_user_def6_label");
      
      /* buyer_user_def7_label - A buyer user-defined field label for Purchase Order */
      attributeMgr.registerAttribute("buyer_user_def7_label", "buyer_user_def7_label");
      
      /* buyer_user_def8_label - A buyer user-defined field label for Purchase Order */
      attributeMgr.registerAttribute("buyer_user_def8_label", "buyer_user_def8_label");
      
      /* buyer_user_def9_label - A buyer user-defined field label for Purchase Order */
      attributeMgr.registerAttribute("buyer_user_def9_label", "buyer_user_def9_label");
      
      /* buyer_user_def10_label - A buyer user-defined field label for Purchase Order */
      attributeMgr.registerAttribute("buyer_user_def10_label", "buyer_user_def10_label");
      
      /* buyer_user_def1_value - A buyer user-defined field value for Purchase Order */
      attributeMgr.registerAttribute("buyer_user_def1_value", "buyer_user_def1_value");
      
      /* buyer_user_def2_value - A buyer user-defined field value for Purchase Order */
      attributeMgr.registerAttribute("buyer_user_def2_value", "buyer_user_def2_value");
      
      /* buyer_user_def3_value - A buyer user-defined field value for Purchase Order */
      attributeMgr.registerAttribute("buyer_user_def3_value", "buyer_user_def3_value");
      
      /* buyer_user_def4_value - A buyer user-defined field value for Purchase Order */
      attributeMgr.registerAttribute("buyer_user_def4_value", "buyer_user_def4_value");
      
      /* buyer_user_def5_value - A buyer user-defined field value for Purchase Order */
      attributeMgr.registerAttribute("buyer_user_def5_value", "buyer_user_def5_value");
      
      /* buyer_user_def6_value - A buyer user-defined field value for Purchase Order */
      attributeMgr.registerAttribute("buyer_user_def6_value", "buyer_user_def6_value");
      
      /* buyer_user_def7_value - A buyer user-defined field value for Purchase Order */
      attributeMgr.registerAttribute("buyer_user_def7_value", "buyer_user_def7_value");
      
      /* buyer_user_def8_value - A buyer user-defined field value for Purchase Order */
      attributeMgr.registerAttribute("buyer_user_def8_value", "buyer_user_def8_value");
      
      /* buyer_user_def9_value - A buyer user-defined field value for Purchase Order */
      attributeMgr.registerAttribute("buyer_user_def9_value", "buyer_user_def9_value");
      
      /* buyer_user_def10_value - A buyer user-defined field value for Purchase Order */
      attributeMgr.registerAttribute("buyer_user_def10_value", "buyer_user_def10_value");
      
      /* seller_user_def1_label - A seller user-defined field label for Purchase Order */
      attributeMgr.registerAttribute("seller_user_def1_label", "seller_user_def1_label");
      
      /* seller_user_def2_label - A seller user-defined field label for Purchase Order */
      attributeMgr.registerAttribute("seller_user_def2_label", "seller_user_def2_label");
      
      /* seller_user_def3_label - A seller user-defined field label for Purchase Order */
      attributeMgr.registerAttribute("seller_user_def3_label", "seller_user_def3_label");
      
      /* seller_user_def4_label - A seller user-defined field label for Purchase Order */
      attributeMgr.registerAttribute("seller_user_def4_label", "seller_user_def4_label");
      
      /* seller_user_def5_label - A seller user-defined field label for Purchase Order */
      attributeMgr.registerAttribute("seller_user_def5_label", "seller_user_def5_label");
      
      /* seller_user_def6_label - A seller user-defined field label for Purchase Order */
      attributeMgr.registerAttribute("seller_user_def6_label", "seller_user_def6_label");
      
      /* seller_user_def7_label - A seller user-defined field label for Purchase Order */
      attributeMgr.registerAttribute("seller_user_def7_label", "seller_user_def7_label");
      
      /* seller_user_def8_label - A seller user-defined field label for Purchase Order */
      attributeMgr.registerAttribute("seller_user_def8_label", "seller_user_def8_label");
      
      /* seller_user_def9_label - A seller user-defined field label for Purchase Order */
      attributeMgr.registerAttribute("seller_user_def9_label", "seller_user_def9_label");
      
      /* seller_user_def10_label - A seller user-defined field label for Purchase Order */
      attributeMgr.registerAttribute("seller_user_def10_label", "seller_user_def10_label");
      
      /* seller_user_def1_value - A seller user-defined field label for Purchase Order */
      attributeMgr.registerAttribute("seller_user_def1_value", "seller_user_def1_value");
      
      /* seller_user_def2_value - A seller user-defined field label for Purchase Order */
      attributeMgr.registerAttribute("seller_user_def2_value", "seller_user_def2_value");
      
      /* seller_user_def3_value - A seller user-defined field label for Purchase Order */
      attributeMgr.registerAttribute("seller_user_def3_value", "seller_user_def3_value");
      
      /* seller_user_def4_value - A seller user-defined field label for Purchase Order */
      attributeMgr.registerAttribute("seller_user_def4_value", "seller_user_def4_value");
      
      /* seller_user_def5_value - A seller user-defined field label for Purchase Order */
      attributeMgr.registerAttribute("seller_user_def5_value", "seller_user_def5_value");
      
      /* seller_user_def6_value - A seller user-defined field label for Purchase Order */
      attributeMgr.registerAttribute("seller_user_def6_value", "seller_user_def6_value");
      
      /* seller_user_def7_value - A seller user-defined field label for Purchase Order */
      attributeMgr.registerAttribute("seller_user_def7_value", "seller_user_def7_value");
      
      /* seller_user_def8_value - A seller user-defined field label for Purchase Order */
      attributeMgr.registerAttribute("seller_user_def8_value", "seller_user_def8_value");
      
      /* seller_user_def9_value - A seller user-defined field label for Purchase Order */
      attributeMgr.registerAttribute("seller_user_def9_value", "seller_user_def9_value");
      
      /* seller_user_def10_value - A seller user-defined field label for Purchase Order */
      attributeMgr.registerAttribute("seller_user_def10_value", "seller_user_def10_value");
      
      /* goods_description -  */
      attributeMgr.registerAttribute("goods_description", "goods_description");
      
      /* status - Purchase Order status */
      attributeMgr.registerReferenceAttribute("status", "status", "PURCHASE_ORDER_STATUS");
      
      /* creation_date_time - Timestamp when Purchase Order was created. */
      attributeMgr.registerAttribute("creation_date_time", "creation_date_time", "DateTimeAttribute");
      
      /* deleted_ind - Indicates whether the Purchase Order is deleted. */
      attributeMgr.registerAttribute("deleted_ind", "deleted_ind", "IndicatorAttribute");
      
      /* amend_seq_no - Tracks number of amend done. */
      attributeMgr.registerAttribute("amend_seq_no", "amend_seq_no", "NumberAttribute");

       /* user_oid - user oid . */
       attributeMgr.registerAttribute("user_oid", "a_user_oid", "LocalAttribute");

       /* action - action performed */
       attributeMgr.registerAttribute("action", "action", "LocalAttribute");

      /* transaction_oid -  */
      attributeMgr.registerAssociation("transaction_oid", "a_transaction_oid", "Transaction");
      
      /* owner_org_oid -  */
      attributeMgr.registerAssociation("owner_org_oid", "a_owner_org_oid", "CorporateOrganization");
      
      /* upload_definition_oid -  */
      attributeMgr.registerAssociation("upload_definition_oid", "a_upload_definition_oid", "PurchaseOrderDefinition");
      
      /* instrument_oid -  */
      attributeMgr.registerAssociation("instrument_oid", "a_instrument_oid", "Instrument");
      
	  /* shipment_oid - This association indicates to which association a PO is assoicated. */
      attributeMgr.registerAssociation("shipment_oid", "a_shipment_oid", "ShipmentTerms");

      /* po_file_upload_oid - file upload oid */
      attributeMgr.registerAttribute("po_file_upload_oid", "a_po_file_upload_oid", "NumberAttribute");

       /* previous_purchase_order_oid - previous purchase order oid */
       attributeMgr.registerAttribute("previous_purchase_order_oid", "previous_purchase_order_oid", "NumberAttribute");


   }
   
  /* 
   * Register the components of the business object
   */
   protected void registerComponents() throws RemoteException, AmsException
   {  
      /* Register the components defined in the Ancestor class */
      super.registerComponents();
      
      /* PurchaseOrderLineItemList -  */
      registerOneToManyComponent("PurchaseOrderLineItemList","PurchaseOrderLineItemList");

      /* PurchaseOrderHistoryList -  */
      registerOneToManyComponent("PurchaseOrderHistoryList","PurchaseOrderHistoryList");
   }

 
   
 
 
   
}
