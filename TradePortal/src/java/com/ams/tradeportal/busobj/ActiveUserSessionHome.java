package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;
/**
 * This class is used to represent the fact that a user is currently logged
 * on to the Trade Portal.   LogonMediator places an ActiveUserSession into
 * the database whenever a user logs in.  It is removed by LogoutMediator and
 * by the ServerCleanup process. 
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface ActiveUserSessionHome extends EJBHome
{
   public ActiveUserSession create()
      throws RemoteException, CreateException, AmsException;

   public ActiveUserSession create(ClientServerDataBridge csdb)
      throws RemoteException, CreateException, AmsException;
}
