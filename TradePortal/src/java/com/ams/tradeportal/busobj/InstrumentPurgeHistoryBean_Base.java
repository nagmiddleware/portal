
  

package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;




/*
 * When an instrument is purged from the Trade Portal database after a period
 * of time, a record is maintained in this table of the instruments that were
 * deleted.
 * 
 * This is populated by the middleware.
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class InstrumentPurgeHistoryBean_Base extends TradePortalBusinessObjectBean
{
private static final Logger LOG = LoggerFactory.getLogger(InstrumentPurgeHistoryBean_Base.class);

  
  /* 
   * Register the attributes and associations of the business object
   */
   protected void registerAttributes() throws AmsException
   {  

      /* Register attributes defined in the Ancestor class */
      super.registerAttributes();
      
      /* instr_purge_history_oid - Unique identifier */
      attributeMgr.registerAttribute("instr_purge_history_oid", "instr_purge_history_oid", "ObjectIDAttribute");
      
      /* instrument_id - The complete instrument id of the instrument that was deleted */
      attributeMgr.registerAttribute("instrument_id", "instrument_id");
      
      /* purge_timestamp - A timestamp (in GMT) of when the instrument was deleted. */
      attributeMgr.registerAttribute("purge_timestamp", "purge_timestamp", "DateTimeAttribute");
      
      /* instrument_type_code - The instrument type code of the instrument that was deleted. */
      attributeMgr.registerReferenceAttribute("instrument_type_code", "instrument_type_code", "INSTRUMENT_TYPE");
      
      /* amount - The instrument amount of the instrument that was deleted. */
      attributeMgr.registerAttribute("amount", "amount", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      
      /* currency - The currency of the instrument that was deleted */
      attributeMgr.registerReferenceAttribute("currency", "currency", "CURRENCY_CODE");
      
      /* counterparty_name - The name of the counterparty for the instrument that was deleted. */
      attributeMgr.registerAttribute("counterparty_name", "counterparty_name");
      
        /* Pointer to the parent CorporateOrganization */
      attributeMgr.registerAttribute("corp_org_oid", "p_corp_org_oid", "ParentIDAttribute");
   
   }
   
 
   
 
 
   
}
