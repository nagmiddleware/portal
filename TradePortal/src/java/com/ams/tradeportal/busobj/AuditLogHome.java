package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;
/**
 * An AuditLog record is created whenever a piece of reference data is inserted,
 * updated or deleted.   It is used for reporting purposes to track changes
 * to reference data.
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface AuditLogHome extends EJBHome
{
   public AuditLog create()
      throws RemoteException, CreateException, AmsException;

   public AuditLog create(ClientServerDataBridge csdb)
      throws RemoteException, CreateException, AmsException;
}
