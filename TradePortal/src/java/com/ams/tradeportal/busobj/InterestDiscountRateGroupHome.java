
  

package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;
/**
 * Interest Discount Rate Group
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface InterestDiscountRateGroupHome extends EJBHome
{
   public InterestDiscountRateGroup create()
      throws RemoteException, CreateException, AmsException;

   public InterestDiscountRateGroup create(ClientServerDataBridge csdb)
      throws RemoteException, CreateException, AmsException;
}
