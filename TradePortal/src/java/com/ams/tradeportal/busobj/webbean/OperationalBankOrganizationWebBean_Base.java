



package com.ams.tradeportal.busobj.webbean;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import javax.servlet.http.*;

/**
 * An organization that has been created to process letters of credit.   When
 * each instrument is created, is will be assigned to an operational bank organization.
 * This organization will handle the processing of the LC once it is authorized
 * and sent to the back end.
 *
 *     Copyright  � 2003
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class OperationalBankOrganizationWebBean_Base extends OrganizationWebBean
{

  public void registerAttributes()
   {
      super.registerAttributes();


       /* imp_DLC_prefix - Prefix for all Import LC  transactions to be processed by this operational
         bank organization. */
       registerAttribute("imp_DLC_prefix", "imp_DLC_prefix");


       /* imp_DLC_suffix - Suffix for all Import LC transactions to be processed by this operational
         bank organization. */
       registerAttribute("imp_DLC_suffix", "imp_DLC_suffix");

       // smanohar Kyriba PPX268
       registerAttribute("external_bank_ind", "external_bank_ind");
       
       /* incoming_SLC_prefix - Prefix for all standby  transactions to be processed by this operational
         bank organization. */
       registerAttribute("incoming_SLC_prefix", "incoming_SLC_prefix");


       /* incoming_SLC_suffix - Suffix for all Standby transactions to be processed by this operational
         bank organization. */
       registerAttribute("incoming_SLC_suffix", "incoming_SLC_suffix");


       /* airway_bill_prefix - Prefix for all Air Waybill  transactions to be processed by this operational
         bank organization. */
       registerAttribute("airway_bill_prefix", "airway_bill_prefix");


       /* airway_bill_suffix - Suffix for all Air Waybill transactions to be processed by this operational
         bank organization. */
       registerAttribute("airway_bill_suffix", "airway_bill_suffix");


       /* steamship_guar_prefix - Prefix for all Shipping Guarantee  transactions to be processed by this
         operational bank organization. */
       registerAttribute("steamship_guar_prefix", "steamship_guar_prefix");


       /* steamship_guar_suffix - Suffix for all Shipping Guarantee transactions to be processed by this operational
         bank organization. */
       registerAttribute("steamship_guar_suffix", "steamship_guar_suffix");


       /* dir_snd_coll_prefix - Prefix for all  Export Collection transactions to be processed by this operational
         bank organization. */
       registerAttribute("dir_snd_coll_prefix", "dir_snd_coll_prefix");


       /* dir_snd_coll_suffix - Suffix for all Direct Send Collection transactions to be processed by this
         operational bank organization. */
       registerAttribute("dir_snd_coll_suffix", "dir_snd_coll_suffix");


       /* guar_prefix - Prefix for all Guarantee  transactions to be processed by this operational
         bank organization. */
       registerAttribute("guar_prefix", "guar_prefix");


       /* guar_suffix - Suffix for all Guarantee transactions to be processed by this operational
         bank organization. */
       registerAttribute("guar_suffix", "guar_suffix");


       /* Proponix_id - The unique identifier of this  organization on OTL, the back end system.
         This is not used by the Trade Portal, but is used by OTL to match up organizations
         on the portal with those stored in OTL. */
       registerAttribute("Proponix_id", "Proponix_id");


       /* export_LC_prefix - Prefix for all  Export LC  transactions to be processed by this operational
         bank organization. */
       registerAttribute("export_LC_prefix", "export_LC_prefix");


       /* export_LC_suffix - Suffix for all Export LC transactions to be processed by this operational
         bank organization. */
       registerAttribute("export_LC_suffix", "export_LC_suffix");


       /* address_city - City where this organization is located. */
       registerAttribute("address_city", "address_city");


       /* address_country - Country  where this organization is located. */
       registerAttribute("address_country", "address_country");


       /* address_line_1 - First line of address */
       registerAttribute("address_line_1", "address_line_1");


       /* address_line_2 - Second line of address */
       registerAttribute("address_line_2", "address_line_2");


       /* address_postal_code - Postal code  where this organization is located. */
       registerAttribute("address_postal_code", "address_postal_code");


       /* address_state_province - State / province  where this organization is located. */
       registerAttribute("address_state_province", "address_state_province");


       /* fax_1 - Primary fax number for this organization */
       registerAttribute("fax_1", "fax_1");


       /* telex_1 - Telex for this organization */
       registerAttribute("telex_1", "telex_1");


       /* telex_answer_back_1 - Telex answer back  for this organization */
       registerAttribute("telex_answer_back_1", "telex_answer_back_1");


       /* fax_2 - Secondary fax  for this organization */
       registerAttribute("fax_2", "fax_2");


       /* swift_address_part1 - First part of the organization's SWIFT address */
       registerAttribute("swift_address_part1", "swift_address_part1");


       /* swift_address_part2 - Second  part of the organization's SWIFT address */
       registerAttribute("swift_address_part2", "swift_address_part2");


       /* phone_number - Phone number of this organization */
       registerAttribute("phone_number", "phone_number");


       /* loan_req_prefix - Prefix for all Loan Request  transactions to be processed by this operational
         bank organization. */
       registerAttribute("loan_req_prefix", "loan_req_prefix");


       /* loan_req_suffix - Suffix for all  Loan Request   transactions to be processed by this operational
         bank organization. */
       registerAttribute("loan_req_suffix", "loan_req_suffix");


       /* funds_transfer_prefix - Prefix for all Funds Transfer transactions to be processed by this operational
         bank organization. */
       registerAttribute("funds_transfer_prefix", "funds_transfer_prefix");


       /* funds_transfer_suffix - Suffix for all Funds Transfer  transactions to be processed by this operational
         bank organization. */
       registerAttribute("funds_transfer_suffix", "funds_transfer_suffix");


       /* request_advise_prefix - Prefix for all Request to Advise transactions to be processed by this operational
         bank organization. */
       registerAttribute("request_advise_prefix", "request_advise_prefix");


       /* request_advise_suffix - Suffix for all Request to Advise transactions to be processed by this operational
         bank organization. */
       registerAttribute("request_advise_suffix", "request_advise_suffix");

       //Krishna CR 375-D 07/19/2007 Begin

       /* approval_to_pay_prefix - Preffix for all Approval to Pay  transactions to be processed by this operational
       bank organization. */
       registerAttribute("approval_to_pay_prefix", "approval_to_pay_prefix");

       /* approval_to_pay_suffix - Suffix for all Approval to Pay  transactions to be processed by this operational
       bank organization. */
       registerAttribute("approval_to_pay_suffix", "approval_to_pay_suffix");

       //Krishna CR 375-D 07/19/2007 End
		/* transfer_btw_acct_prefix - Prefix for all Funds Transfer between accounts transactions to be processed
         by this operational bank organization. */
       registerAttribute("transfer_btw_acct_prefix", "transfer_btw_acct_prefix");


       /* transfer_btw_acct_suffix - Suffix for all Funds Transfer  between accounts transactions to be processed
         by this operational bank organization. */
       registerAttribute("transfer_btw_acct_suffix", "transfer_btw_acct_suffix");


       /* domestic_payment_prefix - Prefix for all Domestic payment transactions to be processed by this operational
         bank organization. */
       registerAttribute("domestic_payment_prefix", "domestic_payment_prefix");


       /* domestic_payment_suffix - Suffix for all Domestic Payment transactions to be processed by this operational
         bank organization. */
       registerAttribute("domestic_payment_suffix", "domestic_payment_suffix");
	
	//Pratiksha CR-509 DDI 12/09/2009 Begin

       /* direct_debit_instruction_prefix - Preffix for all Direct Debit Instruction transactions to be processed by this operational bank organization. */
       registerAttribute("direct_debit_prefix", "direct_debit_prefix");

       /* direct_debit_instruction_suffix - Suffix for all Direct Debit Instruction transactions to be processed by this operational bank organization. */
       registerAttribute("direct_debit_suffix", "direct_debit_suffix");

       //Pratiksha CR-509 DDI 12/09/2009 End		   

//Vasavi CR 524 04/30/10 Begin

/* new_exp_coll_prefix - Prefix for all  Export Collection transactions to be processed by this operational
         bank organization. */
       registerAttribute("new_exp_coll_prefix", "new_exp_coll_prefix");


       /* new_exp_coll_suffix - Suffix for all Export Collection transactions to be processed by this operational
         bank organization. */
       registerAttribute("new_exp_coll_suffix", "new_exp_coll_suffix");

//Vasavi CR 524 04/30/10 End

      /* Pointer to the parent ClientBank */
       registerAttribute("owner_bank_oid", "p_owner_bank_oid", "ParentIDAttribute");

       /* bank_org_group_oid - Each operational bank organization is assigned to particular bank organization
         group (BOG).  The operational bank org can do business with corporate customers
         under that BOG */
       registerAttribute("bank_org_group_oid", "a_bank_org_group_oid", "NumberAttribute");
   }






}
