

  

package com.ams.tradeportal.busobj.webbean;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import javax.servlet.http.*;

/**
 * A group of users.  The corporate customer can specify whether a transaction
 * must be authorized by two user that belong to different WorkGroups.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class WorkGroupWebBean_Base extends TradePortalBusinessObjectWebBean
{

  public void registerAttributes()
   {  
      super.registerAttributes();


       /* work_group_oid - Unique Identifier */
       registerAttribute("work_group_oid", "work_group_oid", "ObjectIDAttribute");


       /* work_group_name - Name of a WorkGroup.  The name must be unique among the WorkGroups of one
         corporate customer. */
       registerAttribute("work_group_name", "work_group_name");


       /* work_group_description - Description of the WorkGroup */
       registerAttribute("work_group_description", "work_group_description");


       /* opt_lock - Optimistic lock attribute
         See jPylon documentation for details on how this works */
       registerAttribute("opt_lock", "opt_lock", "OptimisticLockAttribute");


       /* activation_status - Indicates whether or not the WorkGroup has been deactivated. WorkGroups
         are not deleted from the database; they are deactivated instead so that
         the transactions authorized with these WorkGroups still has valid association. */
       registerAttribute("activation_status", "activation_status");


       /* date_deactivated - Timestamp (in GMT) of when the WorkGroup was deactivated. */
       registerAttribute("date_deactivated", "date_deactivated", "DateTimeAttribute");

      /* Pointer to the parent CorporateOrganization */
       registerAttribute("corp_org_oid", "p_corp_org_oid", "ParentIDAttribute");
   }
   
   




}
