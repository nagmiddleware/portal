

  

package com.ams.tradeportal.busobj.webbean;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import javax.servlet.http.*;

/**
 * Represents the global organization.  In the hierarchy of organizations,
 * this is the highest.   All client banks are owned by this organization.
 * 
 * There will only ever be one of these objects in the database, since there
 * is only one global organization.


 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class GlobalOrganizationWebBean_Base extends ReferenceDataOwnerWebBean
{

  public void registerAttributes()
   {  
      super.registerAttributes();


       /* authentication_method - Determines how users of the client bank and its bank groups will authenticate
         themselves. */
       registerAttribute("authentication_method", "authentication_method");


       /* branding_directory - The branding directory used when users owned by the global organization
         access the Trade Portal. */
       registerAttribute("branding_directory", "branding_directory");


       /* reporting_user_suffix - Suffix that is placed on all reporting users for all organizations under
         this global org */
       registerAttribute("reporting_user_suffix", "reporting_user_suffix");


       /* verify_logon_digital_sig -  */
       registerAttribute("verify_logon_digital_sig", "verify_logon_digital_sig", "IndicatorAttribute");
   }
   
   




}
