

  

package com.ams.tradeportal.busobj.webbean;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import javax.servlet.http.*;
import com.ams.tradeportal.html.*;

/**
 * This class is used to represent the fact that a user is currently logged
 * on to the Trade Portal.   LogonMediator places an ActiveUserSession into
 * the database whenever a user logs in.  It is removed by LogoutMediator and
 * by the ServerCleanup process. 
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class ActiveUserSessionWebBean_Base extends TradePortalBusinessObjectWebBean
{

  public void registerAttributes()
   {  
      super.registerAttributes();


       /* active_user_session_oid - Object identifier of the active user session */
       registerAttribute("active_user_session_oid", "active_user_session_oid", "ObjectIDAttribute");


       /* creation_timestamp - The date and time (stored in the database in the GMT timezone) when the
         ActiveUserSession record was created */
       registerAttribute("creation_timestamp", "creation_timestamp", "DateTimeAttribute");


       /* server_instance_name - The name of the server instance from a jPylon properties file.  This attribute
         is necessary so that when a server starts up after a crash, it will know
         which ActiveUserSession records to delete. */
       registerAttribute("server_instance_name", "server_instance_name");

       /* user_oid - The user who is currenlty logged into the Trade Portal. */
       registerAttribute("user_oid", "a_user_oid", "NumberAttribute");
   }
   
   




}
