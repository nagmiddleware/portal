

  

package com.ams.tradeportal.busobj.webbean;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import javax.servlet.http.*;

/**
 * 
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class PayRemitInvoiceWebBean_Base extends com.amsinc.ecsg.web.AmsEntityWebBean
{

  public void registerAttributes()
   {  


       /* pay_remit_invoice_oid - Unique Identifier */
       registerAttribute("pay_remit_invoice_oid", "pay_remit_invoice_oid", "ObjectIDAttribute");


       /* invoice_reference_id - This represents the invoice number from the remittance line item (invoice). */
       registerAttribute("invoice_reference_id", "invoice_reference_id");


       /* invoice_payment_amount - This represents the amount being paid on this invoice. It will be a negative
         amount if credit note ind = YES. */
       registerAttribute("invoice_payment_amount", "invoice_payment_amount", "TradePortalDecimalAttribute");


       /* invoice_original_amount - Original amount of the invoice. */
       registerAttribute("invoice_original_amount", "invoice_original_amount", "TradePortalDecimalAttribute");


       /* invoice_balanace_amount - Remaing amount tobe paid on this invoice.
 */
       registerAttribute("invoice_balanace_amount", "invoice_balanace_amount", "TradePortalDecimalAttribute");


       /* invoice_date - Date of the invoice */
       registerAttribute("invoice_date", "invoice_date", "DateAttribute");


       /* invoice_discount_amount - Amount of discount taken by the buyer. */
       registerAttribute("invoice_discount_amount", "invoice_discount_amount", "TradePortalDecimalAttribute");


       /* credit_note_indicator - Indicates if this invoice is a credit note.  (YES/NO). NOTE: any payment_remittance_invoice
         lines with the credit note indicator = Yes are ignored by the matching engine.
         The credit note-related fields are available for reporting only. */
       registerAttribute("credit_note_indicator", "credit_note_indicator", "IndicatorAttribute");


       /* otl_pay_remit_invoice_uoid - OTL uoid. */
       registerAttribute("otl_pay_remit_invoice_uoid", "otl_pay_remit_invoice_uoid");

      /* Pointer to the parent PayRemit */
       registerAttribute("pay_remit_oid", "p_pay_remit_oid", "ParentIDAttribute");
   }
   
   




}
