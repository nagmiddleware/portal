

  

package com.ams.tradeportal.busobj.webbean;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import javax.servlet.http.*;

/**
 * A foreign exchange rate between one currency and the corporate customer's
 * base currency.  This is populated by the corporate customer users.
 * 
 * FX Rates are used for threshold checking and for grouping by amount during
 * Auto LC Creation.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class FXRateWebBean_Base extends TradePortalBusinessObjectWebBean
{

  public void registerAttributes()
   {  
      super.registerAttributes();


       /* fx_rate_oid - Unique identifer */
       registerAttribute("fx_rate_oid", "fx_rate_oid", "ObjectIDAttribute");


       /* currency_code - The currency code for which the FX Rate is being established. */
       registerAttribute("currency_code", "currency_code");


       /* rate - The foreign exchange rate */
       registerAttribute("rate", "rate", "TradePortalDecimalAttribute");


       /* multiply_indicator - Indicates whether someone should multiple or divide by the rate in order
         to convert between currencies */
       registerAttribute("multiply_indicator", "multiply_indicator");


       /* last_updated_date - Timestamp (in GMT) of when this FX rate was last updated. */
       registerAttribute("last_updated_date", "last_updated_date", "DateTimeAttribute");


       /* opt_lock - Optimistic lock attribute
         See jPylon documentation for details on how this works */
       registerAttribute("opt_lock", "opt_lock", "OptimisticLockAttribute");


       /* buy_rate - Currency exchange rate specific to purchases. */
       registerAttribute("buy_rate", "buy_rate", "TradePortalDecimalAttribute");


       /* sell_rate - Currency exchange rate specific to sales. */
       registerAttribute("sell_rate", "sell_rate", "TradePortalDecimalAttribute");


       /* ownership_level - The level at which the reference data is owned (global, client bank, bank
         organization group, or corporate customer) */
       registerAttribute("ownership_level", "ownership_level");


       /* ownership_type - If the ownership level is set to corporate customer, the ownership type
         is set to non-admin.  If ownership level is set to anything else, the ownership
         type is admin. */
       registerAttribute("ownership_type", "ownership_type");


       /* fx_rate_group - The FX Rate Group (as defined in TPS) for the account/base currency.  It
         is a simple TextAttribute and is not validated against REFDATA. */
       registerAttribute("fx_rate_group", "fx_rate_group");

      // DK CR-640 Rel7.1 BEGINS
      /* max_spot_rate_amount - Maximum settlement amount in base currency that can be converted using a
         Spot rate */
      registerAttribute("max_spot_rate_amount", "max_spot_rate_amount", "TradePortalDecimalAttribute");
      
      /* max_deal_amount - Maximum Deal Amount allowed for the currency */
      registerAttribute("max_deal_amount", "max_deal_amount", "TradePortalDecimalAttribute");      
      // DK CR-640 Rel7.1 ENDS
      
       /* base_currency_code - The base currency of the FX Rate. */
       registerAttribute("base_currency_code", "base_currency_code");

      /* Pointer to the parent ReferenceDataOwner */
       registerAttribute("owner_org_oid", "p_owner_org_oid", "ParentIDAttribute");
   }
   
   




}
