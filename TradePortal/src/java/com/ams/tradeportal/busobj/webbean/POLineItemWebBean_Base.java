

  

package com.ams.tradeportal.busobj.webbean;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import javax.servlet.http.*;

/**
 * Represents a purchase order line item that has been uploaded or manually
 * entered into the Trade Portal. 
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class POLineItemWebBean_Base extends TradePortalBusinessObjectWebBean
{

  public void registerAttributes()
   {  
      super.registerAttributes();


       /* po_line_item_oid - Unique identfier */
       registerAttribute("po_line_item_oid", "po_line_item_oid", "ObjectIDAttribute");


       /* auto_added_to_amend_ind - Indicates whether or not this PO line item was automatically added to an
         amendment transaction, as opposed to being added manually by a user.   This
         is used to filter the Remove PO line items page; items that were automatically
         added cannot be removed.
         
         Yes=this PO line item was automatically added to an amendment */
       registerAttribute("auto_added_to_amend_ind", "auto_added_to_amend_ind", "IndicatorAttribute");


       /* po_num - Field to store the purchase order number.   This field, in combination with
         the item_num field, will be unique within all PO line items for a particular
         corporate organization. */
       registerAttribute("po_num", "po_num");


       /* item_num - Field to store the line item  number.   This field, in combination with
         the po_num field, will be unique within all PO line items for a particular
         corporate organization. */
       registerAttribute("item_num", "item_num");


       /* ben_name - The name of the beneficiary for this PO line item. */
       registerAttribute("ben_name", "ben_name");


       /* currency - The currency in which the amount is expressed. */
       registerAttribute("currency", "currency");


       /* amount - The amount of the PO line item */
       registerAttribute("amount", "amount", "TradePortalDecimalAttribute");


       /* last_ship_dt - The latest shipment date of this PO line item */
       registerAttribute("last_ship_dt", "last_ship_dt", "DateTimeAttribute");


       /* po_text - Stores a block of text for a purchase order. */
       registerAttribute("po_text", "po_text");


       /* other1 - A user-defined data field for the PO line item. */
       registerAttribute("other1", "other1");


       /* other2 - A user-defined data field for the PO line item. */
       registerAttribute("other2", "other2");


       /* other3 - A user-defined data field for the PO line item. */
       registerAttribute("other3", "other3");


       /* other4 - A user-defined data field for the PO line item. */
       registerAttribute("other4", "other4");


       /* other5 - A user-defined data field for the PO line item. */
       registerAttribute("other5", "other5");


       /* other6 - A user-defined data field for the PO line item. */
       registerAttribute("other6", "other6");


       /* other7 - A user-defined data field for the PO line item. */
       registerAttribute("other7", "other7");


       /* other8 - A user-defined data field for the PO line item. */
       registerAttribute("other8", "other8");


       /* other9 - A user-defined data field for the PO line item. */
       registerAttribute("other9", "other9");


       /* other10 - A user-defined data field for the PO line item. */
       registerAttribute("other10", "other10");


       /* other11 - A user-defined data field for the PO line item. */
       registerAttribute("other11", "other11");


       /* other12 - A user-defined data field for the PO line item. */
       registerAttribute("other12", "other12");


       /* other13 - A user-defined data field for the PO line item. */
       registerAttribute("other13", "other13");


       /* other14 - A user-defined data field for the PO line item. */
       registerAttribute("other14", "other14");


       /* other15 - A user-defined data field for the PO line item. */
       registerAttribute("other15", "other15");


       /* other16 - A user-defined data field for the PO line item. */
       registerAttribute("other16", "other16");


       /* other17 - A user-defined data field for the PO line item. */
       registerAttribute("other17", "other17");


       /* other18 - A user-defined data field for the PO line item. */
       registerAttribute("other18", "other18");


       /* other19 - A user-defined data field for the PO line item. */
       registerAttribute("other19", "other19");


       /* other20 - A user-defined data field for the PO line item. */
       registerAttribute("other20", "other20");


       /* other21 - A user-defined data field for the PO line item. */
       registerAttribute("other21", "other21");


       /* other22 - A user-defined data field for the PO line item. */
       registerAttribute("other22", "other22");


       /* other23 - A user-defined data field for the PO line item. */
       registerAttribute("other23", "other23");


       /* other24 - A user-defined data field for the PO line item. */
       registerAttribute("other24", "other24");


       /* upload_sequence_num - As POs are being uploaded, a sequence number is assigned to each one indicating
         the order in which it was uploaded. */
       registerAttribute("upload_sequence_num", "upload_sequence_num", "NumberAttribute");


       /* previous_po_line_item_oid - When the active PO is moved from an issue transaction to an amendment, a
         copy is made of the PO data so that it can be retained at the transaction
         level.   This association points to the copy that was made when the active
         PO was moved from one transaction to another.
         
         This association is used to determine where the active PO line item should
         be assigned when the transaction is deleted. 
         
         This is an attribute and not a real jPylon association.
 */
       registerAttribute("previous_po_line_item_oid", "previous_po_line_item_oid", "NumberAttribute");


       /* source_type - Indicates whether or not this PO was uploaded or manually entered. */
       registerAttribute("source_type", "source_type");


       /* creation_date_time - Timestamp (in GMT) of when the PO line item was created. */
       registerAttribute("creation_date_time", "creation_date_time", "DateTimeAttribute");

       /* source_upload_definition_oid - The PO upload definition that was used to upload this purchase order data.
         Since the definition was user to upload this data, it will be used to determine
         the meaning of the user defined fields for this PO line item data. */
       registerAttribute("source_upload_definition_oid", "a_source_upload_definition_oid", "NumberAttribute");

       /* owner_org_oid - The corporate organization that uploaded this PO line item. */
       registerAttribute("owner_org_oid", "a_owner_org_oid", "NumberAttribute");

       /* assigned_to_trans_oid - The transaction to which a PO line item is assigned.   By being assigned,
         the PO line item data should also appear in the po_line_items field of the
         terms for the transaction. */
       registerAttribute("assigned_to_trans_oid", "a_assigned_to_trans_oid", "NumberAttribute");

       /* active_for_instrument - Indicates that the PO is active for this instrument.  If POs are not assigned
         to a transaction, they are not active for any instrument.   If POs are assigned
         to a transaction, they are active for the instrument if the PO is up to
         date with the latest data uploaded.   When a PO is reuploaded and an amendment
         is created, the PO on the old transaction is no longer active for the instrument. */
       registerAttribute("active_for_instrument", "a_active_for_instrument", "NumberAttribute");

      /* Pointer to the parent ShipmentTerms */
       registerAttribute("shipment_oid", "p_shipment_oid", "ParentIDAttribute");

     /* selected_indicator - This is a  local attribute, meaning that it is not stored in the database.
         It stores the value of the checkbox which resides next to the PO Line Item and indicates
         if this PO Line Item is selected */
       registerAttribute("selected_indicator", "selected_indicator", "LocalAttribute");
   }
   
   




}
