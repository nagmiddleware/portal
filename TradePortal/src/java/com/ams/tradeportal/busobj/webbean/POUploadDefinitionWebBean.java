package com.ams.tradeportal.busobj.webbean;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.busobj.util.POLineItemField;
import com.ams.tradeportal.common.Sort;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.util.ResourceManager;
import com.amsinc.ecsg.util.StringFunction;

/*
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class POUploadDefinitionWebBean extends POUploadDefinitionWebBean_Base
{
private static final Logger LOG = LoggerFactory.getLogger(POUploadDefinitionWebBean.class);

/**
 * Builds a set of sorted html option tags based on the 6 predefined data
 * attributes (amount, ben_name, currency, ...) as well as the generic
 * "otherx" fields.  The option id is the internal identifier (e.g., amount 
 * or other1) and the displayed value is whatever name the user has 
 * assigned to that attribute.  Only non-blank attributes are included.
 * 
 * @return java.lang.String
 */
public String buildFieldOptionList() {
    
    return buildFieldOptionList( false, null );
}

/**
 * Builds a set of sorted html option tags based on the 6 predefined data
 * attributes (amount, ben_name, currency, ...) as well as the generic
 * "otherx" fields.  The option id is the internal identifier (e.g., amount 
 * or other1) and the displayed value is whatever name the user has 
 * assigned to that attribute.  Only non-blank attributes are included.  po_text
 * is included.
 *
 * @param boolean displaySize   - this is a flag to whether we should display 
 *                                the field size in the option list (that's returned)
 * @return java.lang.String
 */
public String buildFieldOptionList(boolean displaySize, ResourceManager resMgr) {
	return buildFieldOptionList(displaySize, true, resMgr);
}
/**
 * Builds a set of sorted html option tags based on the 6 predefined data
 * attributes (amount, ben_name, currency, ...) as well as the generic
 * "otherx" fields.  The option id is the internal identifier (e.g., amount 
 * or other1) and the displayed value is whatever name the user has 
 * assigned to that attribute.  Only non-blank attributes are included.
 *
 * @param boolean displaySize   - this is a flag to whether we should display 
 *                                the field size in the option list (that's returned)
 * @param boolean displayPOText - whether to include po_text in the list
 * @param ResourceManager resMgr - handle to a ResourceMgr from which to get
 * TextResource
 * @return java.lang.String
 */
public String buildFieldOptionList(boolean displaySize, boolean displayPOText, ResourceManager resMgr) {

	String fieldName;
	String fieldSize;
	String[] options = new String[30];
	StringBuffer optionList = new StringBuffer();
	int count = 0;
	int x = 1;
	int tabPosition;

	// Build an array consisting of each non-blank attribute.  The array
	// contents consist of the user's provided attribute name, a tab 
	// separator, and the internal name.      
	fieldName = getAttribute("amount_field_name");
	fieldSize = getAttributeSize( "amount_size", displaySize, resMgr );
	if (StringFunction.isNotBlank(fieldName)) {
		options[count++] = StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(fieldName)) + fieldSize + "\t" + "amount";
	}

	fieldName = getAttribute("ben_name_field_name");
	fieldSize = getAttributeSize( "ben_name_size", displaySize, resMgr );
	if (StringFunction.isNotBlank(fieldName)) {
		options[count++] = StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(fieldName)) + fieldSize + "\t" + "ben_name";
	}

	fieldName = getAttribute("currency_field_name");
	fieldSize = getAttributeSize( "currency_size", displaySize, resMgr );
	if (StringFunction.isNotBlank(fieldName)) {
		options[count++] = StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(fieldName)) + fieldSize + "\t" + "currency";
	}

	fieldName = getAttribute("item_num_field_name");
	fieldSize = getAttributeSize( "item_num_size", displaySize, resMgr );
	if (StringFunction.isNotBlank(fieldName)) {
		options[count++] = StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(fieldName))+ fieldSize + "\t" + "item_num";
	}

	fieldName = getAttribute("last_ship_dt_field_name");
	fieldSize = getAttributeSize( "last_ship_dt_size", displaySize, resMgr );
	if (StringFunction.isNotBlank(fieldName)) {
		options[count++] = StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(fieldName)) + fieldSize + "\t" + "last_ship_dt";
	}

	fieldName = getAttribute("po_num_field_name");
	fieldSize = getAttributeSize( "po_num_size", displaySize, resMgr );
	if (StringFunction.isNotBlank(fieldName)) {
		options[count++] = StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(fieldName)) + fieldSize + "\t" + "po_num";
	}
	
	if (displayPOText)  {
		fieldName = getAttribute("po_text_field_name");
		fieldSize = getAttributeSize( "po_text_size", displaySize, resMgr );
		if (StringFunction.isNotBlank(fieldName)) {
			options[count++] = StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(fieldName)) + fieldSize + "\t" + "po_text";
		}
	}

	for (x = 1; x <= 24; x++) {
		fieldName = getAttribute("other" + x + "_field_name");
	    fieldSize = getAttributeSize( "other" + x + "_size", displaySize, resMgr );
		if (StringFunction.isNotBlank(fieldName)) {
			options[count++] = StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(fieldName)) + fieldSize + "\t" + "other" + x;
		}
	}

	// Now sort the options array.  Since the user's defined name is the first
	// part of each entry, that's how they are sorted.
	try {
    		if (resMgr!=null) Sort.sort(options, resMgr.getResourceLocale());
	} catch (Exception e) {
		// if the sort fails, we do nothing and the list remains unsorted
	}

	// Now build the HTML option list from the sorted array
	for (x = 0; x < count; x++) {
		tabPosition = options[x].indexOf("\t");

		optionList.append("<option value=");
		optionList.append(options[x].substring(tabPosition + 1, options[x].length()));
		optionList.append(">");
		optionList.append(options[x].substring(0, tabPosition));
		optionList.append("</option>");
	}

	return optionList.toString();
}

/**
 * @param fieldNameOptionList
 * @return
 * 
 * This method will parse the dropdown options data and loads it to the map.
 */
public Map<Integer, List> parseFieldOptionsList(String fieldNameOptionList, String pageName){
	
	
	String[] optionStr = null;
	String[] temp1, temp2 = null;
	Map<Integer, List> hashMap = new HashMap<Integer, List>();
	List<String> list = null;
	int mIndex = 1;	
	boolean flag = false;
	
	final String PO_UPLOAD_DEFN_DETAIL_FILE = "POUploadDefinitionDetail-File";
	final String PO_UPLOAD_DEFN_DETAIL_GOODS = "POUploadDefinitionDetail-Goods";
	final String GOODS_DESC_ORDER = "goods_descr_order_";
	final String UPLOAD_ORDER = "upload_order_";
	
	String attributeName = null;	
	
	//Check for the fieldNameOptionList null
	if(fieldNameOptionList != null){
		optionStr = fieldNameOptionList.split("</option>");
	}

	//iterate through optionStr, parse options name, value
	if(optionStr!=null){
	 for(String s : optionStr){
		
		temp1 = s.split(" value=");
		
		for(String s1 : temp1){
			
			temp2 = s1.split(" \\(");			
			list = new ArrayList<String>();
			
			for(String s2 : temp2){				
				
				if(s2.contains(">")){
					list.add(s2.split(">")[0]);
					list.add(s2.split(">")[1]);
				}
				//Check if we have Size in the string, if exists parse size, value
				if(s2.contains("=")){
					list.add(s2.split("=")[1].trim().substring(0, s2.split("=")[1].trim().length()-1));
					
				}					
				
			}
			//After parsing , load it to hashmap as key:value
			if(list != null && list.size() > 0){
				hashMap.put(mIndex++, list);
			}
			list = null;
		}			
	 }
	}
	
	LOG.info("=== hashMap before Value ===");
	LOG.info(hashMap.toString());
	LOG.info(" ===== POUploadDefinitionWebBean.java ==== ");
	
	list = new ArrayList<String>();
	/*Check for the pagename poupload....file or pouload...goods, 
	 * get correct data's which need to display in their respective screns */
	if( null != pageName && !"".equalsIgnoreCase(pageName) ){
		if( PO_UPLOAD_DEFN_DETAIL_FILE.equalsIgnoreCase(pageName) ){
			
			attributeName = UPLOAD_ORDER;
			
		}else if( PO_UPLOAD_DEFN_DETAIL_GOODS.equalsIgnoreCase(pageName) ){
			
			attributeName = GOODS_DESC_ORDER;
		}
		
		if( null != attributeName ){
			//hashmap contains option data.
			for(Integer key : hashMap.keySet()){
				LOG.info("{} : {} ",attributeName+key,getAttribute(attributeName+key));
				list.add(getAttribute(attributeName+key));
				
				if( null == getAttribute(attributeName+key) || "".equalsIgnoreCase(getAttribute(attributeName+key)) ){
					flag = true;
					break;
				}
			}
			List tempList = null;
			if(!flag){
				Map<Integer, List> newMap = new HashMap<Integer, List>();
				for(Integer key : hashMap.keySet()){
					list = hashMap.get(key);
					for(Integer key1 : hashMap.keySet()){
						if( StringFunction.isNotBlank(getAttribute(attributeName+key1))) {
							if(getAttribute(attributeName+key1).equalsIgnoreCase(list.get(0))){
								newMap.put(key1, list);
								break;
							}
						}				
					}			
				}
				hashMap = newMap;
			}
			
		}
	}

	
	LOG.info("=== hashMap after change Value ===");
	LOG.info(hashMap.toString());
	//return map which consists key = position of the option, value = list (contains fieldname, value, size)
	return hashMap;
	
}


/**
 * Taking an HTML set of <option> values, searches for the one with an id
 * equal to the given selection.  If found, adds the 'selected' tag to
 * the HTML.  The updated set of options is returned.
 *
 * @return java.lang.String
 * @param htmlOptions java.lang.String - a set of html option tags
 * @param selection java.lang.String - the selection to search for 
 */
public String selectFieldInList(String htmlOptions, String selection) {
	if (StringFunction.isNotBlank(selection)) {
		int start = htmlOptions.indexOf(selection);
		int insertPosition = htmlOptions.indexOf(">", start);
		htmlOptions = 
			htmlOptions.substring(0, insertPosition)
				+ " selected"
				+ htmlOptions.substring(insertPosition); 
	}
	return htmlOptions;

}


/**
 * This method is used to localize certain logic to help determine whether or
 * not to return an attributes Size.  In the PO Upload Definition Object, there
 * are several attributes that have defined sizes.  This method will return a 
 * string that includes the attribute's size if the boolean valueis turned on.
 *
 * @param String Attribute  - This is the attribute size to be appended to the
 *                            outgoing string for display purposes.
 * @param boolean display   - This indicates whether to return the size or an 
 *                            empty string.
 * @return java.lang.String
 */
private String getAttributeSize( String attribute, boolean display, ResourceManager resMgr ) {

    String attributeSize = this.getAttribute( attribute );
    
    if( display ){
        if ( StringFunction.isBlank(attributeSize) )
                attributeSize = "0";
                
        return " " + resMgr.getText("common.ParenSize", TradePortalConstants.TEXT_BUNDLE) 
                   + " = " + attributeSize 
                   + resMgr.getText("ClientBankDetail.EndParenthesis", TradePortalConstants.TEXT_BUNDLE);

    }

    return "";
}

   
   /**
    * This method loops through all the attributes looking for the associated size
    * and adds up the returned integer values.  The total is returned for display.
    *
    * @return String AttributeLength
    */
   public String getLineLengthString() {
    
        String lineLength = "";
        int length = 0;
        int attributeCnt = 0;
                
        for( int x = 1; x <= 30; x++ ) {
               //because we need to add a space between each selection we 
               //need to keep track of the number of attributes that have a value.
               if( StringFunction.isNotBlank(this.getAttribute("goods_descr_order_" + x)) )
                    attributeCnt += 1;
               
               length += getFieldSize( "goods_descr_order_" + x );
        }
        //Now that we have the count of attributes that have a populated value,
        //we now know how mant spaces to account for between attributes -
        //The number of spaces = # of attributes populated - 1
        if( attributeCnt != 0 )
            attributeCnt -= 1;

      
        return lineLength.valueOf( length + attributeCnt );
        
        }
   
   
   /**
    * This method does the work of getting the actual field size for a passed in
    * attribute name.  Basically we are passing in a "goods_desc_order_##" which
    * has a value like: 'po_num'  : ie some attribute that was defined on the
    * General Data Definition tab.  Once we have the actual attribute name that
    * was selected by the user, we then can look up what the associated field size
    * is...If at any time the value is null, we return a 0, otherwise we return the
    * stored size for the associated attribute.
    *
    * @param String Goods_Desc_Order_value_##   - This is the initial attribute name.
    * @return int Attribute Size
    */
   private int getFieldSize (String value) {
                    
        String  attribute = this.getAttribute( value );
        String  attributeSize = "";
        String  attributeFieldName = "";

        Integer iSize = new Integer(0);
        
        try{
            if( StringFunction.isNotBlank( attribute ) ) {
                attributeSize = getAttribute( attribute + "_size" );
                attributeFieldName = getAttribute( attribute + "_field_name" );
            }
            if( StringFunction.isNotBlank( attributeSize ) ) {
                iSize = new Integer( attributeSize );
            }

            // If the user has chosen to include a column header, the
            // space taken up by the data will be the greater of the field size
            // and the size of the field name (which will be in the header)
            //Commenting below condition as it is not Required
           
        }catch(NumberFormatException e) {   //should never get here...
            e.printStackTrace();   
        }

        return iSize.intValue();
   }

  /**
   * Retrieves the fields that will be used to construct the PO Details Entry Page
   * which allows users to manually enter PO Line Items.  This method also retrieves 
   * information about the fields such as their size and their data type.
   *
   */
  public Vector loadPOLineItemFields() 
  {
    boolean hasMoreFields       = true;
    String  fieldType           = null;
    String  fieldName           = null;
    String  fieldDataType       = null;
    int     fieldSize           = 0;
    int     index               = 1;
    Vector  poLineItemFieldList = new Vector();

    while ((hasMoreFields) && (index <= TradePortalConstants.PO_NUMBER_OF_FIELDS))
    {
      // Get the type, name, size, and datatype of each field specified in the PO Definition
      fieldType = getAttribute("upload_order_" + index);
      if (!StringFunction.isBlank(fieldType))
      {
        fieldName     = getAttribute(fieldType + "_field_name");
        fieldSize     = Integer.parseInt(getAttribute(fieldType + "_size"));
        fieldDataType = getAttribute(fieldType + "_datatype");
      
        // Create a PO Line Item class and save information for each field
        POLineItemField poLineItemField = new POLineItemField();
        poLineItemField.setFieldType(fieldType);
        poLineItemField.setFieldName(fieldName);
        poLineItemField.setFieldSize(fieldSize);
        poLineItemField.setFieldDataType(fieldDataType);

        // Add the PO Line Item field to the list
        poLineItemFieldList.addElement(poLineItemField);
        index++;
      }
      else
      {
        hasMoreFields = false;
      }
    }
    return poLineItemFieldList;
  }

}