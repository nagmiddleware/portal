

  

package com.ams.tradeportal.busobj.webbean;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import javax.servlet.http.*;

/**
 * Details for the various actions performed on the Cash Management transactions
 * by a user
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class TransactionHistoryWebBean_Base extends TradePortalBusinessObjectWebBean
{

  public void registerAttributes()
   {  
      super.registerAttributes();


       /* transaction_history_oid - Unique Identifier */
       registerAttribute("transaction_history_oid", "transaction_history_oid", "ObjectIDAttribute");


       /* action_datetime - Timestamp (in GMT) when the action is taken. */
       registerAttribute("action_datetime", "action_datetime", "DateTimeAttribute");


       /* action - Action. */
       registerAttribute("action", "action");


       /* panel_authority_code - The Panel Level at which the authorization action is taken. */
       registerAttribute("panel_authority_code", "panel_authority_code");


       /* transaction_status - Lifecycle status of a transactioin */
       registerAttribute("transaction_status", "transaction_status");

      /* Pointer to the parent Transaction */
       registerAttribute("transaction_oid", "p_transaction_oid", "ParentIDAttribute");

       /* user_oid - The user who the updates the transaction. */
       registerAttribute("user_oid", "a_user_oid", "NumberAttribute");
       
       /* repair_reason - repair reason to sedn transaction for repair. */
       registerAttribute("repair_reason", "repair_reason");
   }
   
   




}
