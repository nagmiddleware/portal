package com.ams.tradeportal.busobj.webbean;

import com.ams.tradeportal.common.TradePortalConstants;


/**
  Invoice data information uploaded will be stored here
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class InvoicePaymentInstructionsWebBean_Base extends TradePortalBusinessObjectWebBean {

	/*
	 * Register the attributes and associations of the business object
	 */
	  public void registerAttributes()
	   {  
	      super.registerAttributes();

		      /* Register attributes defined in the Ancestor class */
		      super.registerAttributes();
		      
		      /* inv_upload_definition_oid - Unique identifier */
		      registerAttribute("inv_pay_inst_oid", "inv_pay_inst_oid", "ObjectIDAttribute");
		      
		      /* inv_upload_definition_oid - Unique identifier */
		      registerAttribute("invoice_summary_data_oid", "p_invoice_summary_data_oid");
		      /* Invoice ID - Unique Id */
		      registerAttribute("pay_method", "pay_method");
		      registerAttribute("ben_acct_num", "ben_acct_num");
		      registerAttribute("ben_address_one", "ben_address_one");
		      registerAttribute("ben_address_two", "ben_address_two");
		      registerAttribute("ben_address_three", "ben_address_three");
		      registerAttribute ("ben_city", "ben_city");
		      registerAttribute ("ben_country", "ben_country");
		      registerAttribute("ben_bank_name", "ben_bank_name");
		      registerAttribute("ben_branch_code", "ben_branch_code");
		      registerAttribute("ben_branch_address1", "ben_branch_address1");
		      registerAttribute("ben_branch_address2", "ben_branch_address2");
		      registerAttribute("ben_bank_city", "ben_bank_city");
		      registerAttribute("ben_bank_prvnce", "ben_bank_prvnce");
		      registerAttribute("ben_branch_country", "ben_branch_country");
		      registerAttribute ("payment_charges", "payment_charges");
		      registerAttribute("central_bank_rep1", "central_bank_rep1");
		      registerAttribute("central_bank_rep2", "central_bank_rep2");
		      registerAttribute("central_bank_rep3", "central_bank_rep3");
		      registerAttribute("customer_reference", "customer_reference");
		      registerAttribute("sequence_number", "sequence_number", "NumberAttribute");

	             /* Pointer to the parent Transaction */
	           registerAttribute("transaction_oid", "p_transaction_oid", "ParentIDAttribute");
	           /* amount - The amount of the domestic payment. This is validated to have the correct
	           number of decimal places based on the currency selected.   For amendments,
	           this attribute is allowed to have a negative value.  For all other transactions,
	           this attribute must be validated that it is a positive number. */
	           registerAttribute("amount", "amount", "TradePortalSignedDecimalAttribute");
	        
	        /* amount_currency_code - The currency for the instrument. */
	        registerAttribute("amount_currency_code", "amount_currency_code");
	        registerAttribute("ben_bank_sort_code", "ben_bank_sort_code");//CR 709A
	        /*Rpasupulati CR1001 Start*/
	        registerAttribute("reporting_code_1", "reporting_code_1");
	        registerAttribute("reporting_code_2", "reporting_code_2");
	        /*Rpasupulati CR1001 End*/
		   }
		   		   
	}

