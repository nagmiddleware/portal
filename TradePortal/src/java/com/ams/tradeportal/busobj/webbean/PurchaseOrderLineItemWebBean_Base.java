

  

package com.ams.tradeportal.busobj.webbean;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import javax.servlet.http.*;

/**
 * Represents a purchase order line item that has been uploaded into the Trade
 * Portal.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class PurchaseOrderLineItemWebBean_Base extends TradePortalBusinessObjectWebBean
{

  public void registerAttributes()
   {  
      super.registerAttributes();


       /* po_line_item_oid - Unique identfier */
       registerAttribute("po_line_item_oid", "po_line_item_oid", "ObjectIDAttribute");


       /* line_item_num - Field to store the line item  number.   This field, in combination with
         the purchase_order_num field, will be unique within all PO line items for
         a particular corporate organization. */
       registerAttribute("line_item_num", "line_item_num");


       /* unit_price - The individual unit price of the item. */
       registerAttribute("unit_price", "unit_price", "NumberAttribute");


       /* unit_of_measure - This is the unit of measure (for example, Cases, Cartons, Boxes etc) */
       registerAttribute("unit_of_measure", "unit_of_measure");


       /* quantity - the quantity of the line item. */
       registerAttribute("quantity", "quantity", "NumberAttribute");


       /* quantity_variance_plus - The positive variance in percentage allowed above the agreed quantity */
       registerAttribute("quantity_variance_plus", "quantity_variance_plus", "NumberAttribute");


       /* prod_chars_ud1_label - User-defined product characteristic field label for Purchase Order */
       registerAttribute("prod_chars_ud1_label", "prod_chars_ud1_label");


       /* prod_chars_ud2_label - User-defined product characteristic field label for Purchase Order */
       registerAttribute("prod_chars_ud2_label", "prod_chars_ud2_label");


       /* prod_chars_ud3_label - User-defined product characteristic field label for Purchase Order */
       registerAttribute("prod_chars_ud3_label", "prod_chars_ud3_label");


       /* prod_chars_ud4_label - User-defined product characteristic field label for Purchase Order */
       registerAttribute("prod_chars_ud4_label", "prod_chars_ud4_label");


       /* prod_chars_ud5_label - User-defined product characteristic field label for Purchase Order */
       registerAttribute("prod_chars_ud5_label", "prod_chars_ud5_label");


       /* prod_chars_ud6_label - User-defined product characteristic field label for Purchase Order */
       registerAttribute("prod_chars_ud6_label", "prod_chars_ud6_label");


       /* prod_chars_ud7_label - User-defined product characteristic field label for Purchase Order */
       registerAttribute("prod_chars_ud7_label", "prod_chars_ud7_label");


       /* prod_chars_ud1_value - User-defined product characteristic field value for Purchase Order */
       registerAttribute("prod_chars_ud1_value", "prod_chars_ud1_value");


       /* prod_chars_ud2_value - User-defined product characteristic field value for Purchase Order */
       registerAttribute("prod_chars_ud2_value", "prod_chars_ud2_value");


       /* prod_chars_ud3_value - User-defined product characteristic field value for Purchase Order */
       registerAttribute("prod_chars_ud3_value", "prod_chars_ud3_value");


       /* prod_chars_ud4_value - User-defined product characteristic field value for Purchase Order */
       registerAttribute("prod_chars_ud4_value", "prod_chars_ud4_value");


       /* prod_chars_ud5_value - User-defined product characteristic field value for Purchase Order */
       registerAttribute("prod_chars_ud5_value", "prod_chars_ud5_value");


       /* prod_chars_ud6_value - User-defined product characteristic field value for Purchase Order */
       registerAttribute("prod_chars_ud6_value", "prod_chars_ud6_value");


       /* prod_chars_ud7_value - User-defined product characteristic field value for Purchase Order */
       registerAttribute("prod_chars_ud7_value", "prod_chars_ud7_value");

       /* derived_line_item_ind - Indicates whether or not this Purchase Order Line Item was automatically
         added by system as opposed to being added by a user.

         Y=this PO line item was derived
         N=this PO line item was added by user */
       registerAttribute("derived_line_item_ind", "derived_line_item_ind", "IndicatorAttribute");

      /* Pointer to the parent PurchaseOrder */
       registerAttribute("purchase_order_oid", "p_purchase_order_oid", "ParentIDAttribute");
   }
   
   




}
