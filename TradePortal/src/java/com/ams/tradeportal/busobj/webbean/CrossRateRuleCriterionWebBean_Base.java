

  

package com.ams.tradeportal.busobj.webbean;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import javax.servlet.http.*;

/**
 * Allows Cross Rate Calculatoin Rules to be defined.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class CrossRateRuleCriterionWebBean_Base extends com.amsinc.ecsg.web.AmsEntityWebBean
{

  public void registerAttributes()
   {  


       /* criterion_oid - Unique Identifier */
       registerAttribute("criterion_oid", "criterion_oid", "ObjectIDAttribute");


       /* from_ccy - From currency */
       registerAttribute("from_ccy", "from_ccy");


       /* to_ccy - To currency. */
       registerAttribute("to_ccy", "to_ccy");


       /* calc_method - Calculation method */
       registerAttribute("calc_method", "calc_method");


       /* lower_variance - Lower variance */
       registerAttribute("lower_variance", "lower_variance", "TradePortalSignedDecimalAttribute");


       /* upper_variance - Upper variance */
       registerAttribute("upper_variance", "upper_variance", "TradePortalSignedDecimalAttribute");

      /* Pointer to the parent CrossRateCalcRule */
       registerAttribute("cross_rate_calc_rule_oid", "p_cross_rate_calc_rule_oid", "ParentIDAttribute");
   }
   
   




}
