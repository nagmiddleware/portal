

  

package com.ams.tradeportal.busobj.webbean;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import javax.servlet.http.*;

/**
 * Defines a rule which determines if a Corporate Customer will receive notification
 * messages and emails after a transaction has been authorized.
 * 
 * A Notification Rule can be established by Client Banks and Bank Group level
 * users.  The Notification Rule can then be assigned to a specific Corporate
 * Customer.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class NotificationRuleWebBean_Base extends ReferenceDataWebBean
{

  public void registerAttributes()
   {  
      super.registerAttributes();


       /* notification_rule_oid - Unique identifier */
       registerAttribute("notification_rule_oid", "notification_rule_oid", "ObjectIDAttribute");


       /* description - Description of the rule */
       registerAttribute("description", "description");


       /* email_frequency - Determines whether notification emails will be received for each transaction
         or daily. */
       registerAttribute("email_frequency", "email_frequency");


       /* email_for_message - Indicates if an email will be sent for mail messages. */
  //     registerAttribute("email_for_message", "email_for_message");


       /* email_for_discrepancy - Indicates if an email will be sent for discrepancy messages. */
 //      registerAttribute("email_for_discrepancy", "email_for_discrepancy");


       /* part_to_validate - This is a  local attribute, meaning that it is not stored in the database.
         It stores the tab that the user is editing on the PO Upload Definition detail
         page.   Based on what tab is being edited, different validations occur. */
       registerAttribute("part_to_validate", "part_to_validate", "LocalAttribute");


       /* email_for_ar_match_notice - Whether an email will be sent when AR Match Notice is received. */
//       registerAttribute("email_for_ar_match_notice", "email_for_ar_match_notice");

      /* Pointer to the parent ReferenceDataOwner */
       registerAttribute("owner_org_oid", "p_owner_org_oid", "ParentIDAttribute");
       
     //SSikhakolli - Rel-9.4 CR-818 - adding new attribute
//       registerAttribute("email_for_settle_instr", "email_for_settle_instr", "NOTIF_RULE_SETTING");

	    registerAttribute("template_ind", "template_ind", "IndicatorAttribute");
       
       registerAttribute("source_template_notif_oid", "source_template_notif_oid","NumberAttribute");

	    registerAttribute("save_as_flag", "save_as_flag", "LocalAttribute");
    
		 registerAttribute("default_apply_to_all_grp", "default_apply_to_all_grp", "LocalAttribute");

	  	registerAttribute("default_clear_to_all_grp", "default_clear_to_all_grp", "LocalAttribute");		
 
	  	registerAttribute("default_update_recipients_only", "default_update_recipients_only", "LocalAttribute");

	  	registerAttribute("default_update_emails_only", "default_update_emails_only", "LocalAttribute");
	  	
		registerAttribute("default_notify_setting", "default_notify_setting", "LocalAttribute");			
		
	  	registerAttribute("default_email_setting", "default_email_setting", "LocalAttribute");	

		registerAttribute("default_addl_email_addr", "default_addl_email_addr", "LocalAttribute");			

    	registerAttribute("default_notification_user_ids", "default_notification_user_ids", "LocalAttribute");	
		
	  	registerAttribute("change_count", "change_count", "LocalAttribute");			
	
		registerAttribute("col_type", "col_type", "LocalAttribute");		

		registerAttribute("instrument_type0", "instrument_type0", "LocalAttribute");		
		registerAttribute("instrument_type1", "instrument_type1", "LocalAttribute");		
		registerAttribute("instrument_type2", "instrument_type2", "LocalAttribute");		
		registerAttribute("instrument_type3", "instrument_type3", "LocalAttribute");		
		registerAttribute("instrument_type4", "instrument_type4", "LocalAttribute");		
		registerAttribute("instrument_type5", "instrument_type5", "LocalAttribute");		
		registerAttribute("instrument_type6", "instrument_type6", "LocalAttribute");		
		registerAttribute("instrument_type7", "instrument_type7", "LocalAttribute");		
		registerAttribute("instrument_type8", "instrument_type8", "LocalAttribute");		
		registerAttribute("instrument_type9", "instrument_type9", "LocalAttribute");		
		registerAttribute("instrument_type10", "instrument_type10", "LocalAttribute");	
		registerAttribute("instrument_type11", "instrument_type11", "LocalAttribute");		
		registerAttribute("instrument_type12", "instrument_type12", "LocalAttribute");		
		registerAttribute("instrument_type13", "instrument_type13", "LocalAttribute");		
		registerAttribute("instrument_type14", "instrument_type14", "LocalAttribute");		
		registerAttribute("instrument_type15", "instrument_type15", "LocalAttribute");		
		registerAttribute("instrument_type16", "instrument_type16", "LocalAttribute");		
		registerAttribute("instrument_type17", "instrument_type17", "LocalAttribute");		
		registerAttribute("instrument_type18", "instrument_type18", "LocalAttribute");		
		registerAttribute("instrument_type19", "instrument_type19", "LocalAttribute");		
		registerAttribute("instrument_type20", "instrument_type20", "LocalAttribute");	
		registerAttribute("instrument_type21", "instrument_type21", "LocalAttribute");		
		registerAttribute("instrument_type22", "instrument_type22", "LocalAttribute");		
		registerAttribute("instrument_type23", "instrument_type23", "LocalAttribute");		
		registerAttribute("instrument_type24", "instrument_type24", "LocalAttribute");		
		registerAttribute("instrument_type25", "instrument_type25", "LocalAttribute");		
		registerAttribute("instrument_type26", "instrument_type26", "LocalAttribute");		
		registerAttribute("instrument_type27", "instrument_type27", "LocalAttribute");		
		registerAttribute("instrument_type28", "instrument_type28", "LocalAttribute");		
		registerAttribute("instrument_type29", "instrument_type29", "LocalAttribute");		
		registerAttribute("instrument_type30", "instrument_type30", "LocalAttribute");	
		registerAttribute("instrument_type31", "instrument_type31", "LocalAttribute");		
		registerAttribute("instrument_type32", "instrument_type32", "LocalAttribute");		
		registerAttribute("instrument_type33", "instrument_type33", "LocalAttribute");	
		registerAttribute("instrument_type34", "instrument_type34", "LocalAttribute");

		registerAttribute("apply_to_all_tran0", "apply_to_all_tran0", "LocalAttribute");		
		registerAttribute("apply_to_all_tran1", "apply_to_all_tran1", "LocalAttribute");		
		registerAttribute("apply_to_all_tran2", "apply_to_all_tran2", "LocalAttribute");		
		registerAttribute("apply_to_all_tran3", "apply_to_all_tran3", "LocalAttribute");		
		registerAttribute("apply_to_all_tran4", "apply_to_all_tran4", "LocalAttribute");		
		registerAttribute("apply_to_all_tran5", "apply_to_all_tran5", "LocalAttribute");		
		registerAttribute("apply_to_all_tran6", "apply_to_all_tran6", "LocalAttribute");		
		registerAttribute("apply_to_all_tran7", "apply_to_all_tran7", "LocalAttribute");		
		registerAttribute("apply_to_all_tran8", "apply_to_all_tran8", "LocalAttribute");		
		registerAttribute("apply_to_all_tran9", "apply_to_all_tran9", "LocalAttribute");		
		registerAttribute("apply_to_all_tran10", "apply_to_all_tran10", "LocalAttribute");	
		registerAttribute("apply_to_all_tran11", "apply_to_all_tran11", "LocalAttribute");		
		registerAttribute("apply_to_all_tran12", "apply_to_all_tran12", "LocalAttribute");		
		registerAttribute("apply_to_all_tran13", "apply_to_all_tran13", "LocalAttribute");		
		registerAttribute("apply_to_all_tran14", "apply_to_all_tran14", "LocalAttribute");		
		registerAttribute("apply_to_all_tran15", "apply_to_all_tran15", "LocalAttribute");		
		registerAttribute("apply_to_all_tran16", "apply_to_all_tran16", "LocalAttribute");		
		registerAttribute("apply_to_all_tran17", "apply_to_all_tran17", "LocalAttribute");		
		registerAttribute("apply_to_all_tran18", "apply_to_all_tran18", "LocalAttribute");		
		registerAttribute("apply_to_all_tran19", "apply_to_all_tran19", "LocalAttribute");		
		registerAttribute("apply_to_all_tran20", "apply_to_all_tran20", "LocalAttribute");	
		registerAttribute("apply_to_all_tran21", "apply_to_all_tran21", "LocalAttribute");		
		registerAttribute("apply_to_all_tran22", "apply_to_all_tran22", "LocalAttribute");		
		registerAttribute("apply_to_all_tran23", "apply_to_all_tran23", "LocalAttribute");		
		registerAttribute("apply_to_all_tran24", "apply_to_all_tran24", "LocalAttribute");		
		registerAttribute("apply_to_all_tran25", "apply_to_all_tran25", "LocalAttribute");		
		registerAttribute("apply_to_all_tran26", "apply_to_all_tran26", "LocalAttribute");		
		registerAttribute("apply_to_all_tran27", "apply_to_all_tran27", "LocalAttribute");		
		registerAttribute("apply_to_all_tran28", "apply_to_all_tran28", "LocalAttribute");		
		registerAttribute("apply_to_all_tran29", "apply_to_all_tran29", "LocalAttribute");		
		registerAttribute("apply_to_all_tran30", "apply_to_all_tran30", "LocalAttribute");	
		registerAttribute("apply_to_all_tran31", "apply_to_all_tran31", "LocalAttribute");		
		registerAttribute("apply_to_all_tran32", "apply_to_all_tran32", "LocalAttribute");		
		registerAttribute("apply_to_all_tran33", "apply_to_all_tran33", "LocalAttribute");	
		registerAttribute("apply_to_all_tran34", "apply_to_all_tran34", "LocalAttribute");	

		registerAttribute("clear_to_all_tran0", "clear_to_all_tran0", "LocalAttribute");	
		registerAttribute("clear_to_all_tran1", "clear_to_all_tran1", "LocalAttribute");		
		registerAttribute("clear_to_all_tran2", "clear_to_all_tran2", "LocalAttribute");		
		registerAttribute("clear_to_all_tran3", "clear_to_all_tran3", "LocalAttribute");		
		registerAttribute("clear_to_all_tran4", "clear_to_all_tran4", "LocalAttribute");		
		registerAttribute("clear_to_all_tran5", "clear_to_all_tran5", "LocalAttribute");		
		registerAttribute("clear_to_all_tran6", "clear_to_all_tran6", "LocalAttribute");		
		registerAttribute("clear_to_all_tran7", "clear_to_all_tran7", "LocalAttribute");		
		registerAttribute("clear_to_all_tran8", "clear_to_all_tran8", "LocalAttribute");		
		registerAttribute("clear_to_all_tran9", "clear_to_all_tran9", "LocalAttribute");		
		registerAttribute("clear_to_all_tran10", "clear_to_all_tran10", "LocalAttribute");	
		registerAttribute("clear_to_all_tran11", "clear_to_all_tran11", "LocalAttribute");		
		registerAttribute("clear_to_all_tran12", "clear_to_all_tran12", "LocalAttribute");		
		registerAttribute("clear_to_all_tran13", "clear_to_all_tran13", "LocalAttribute");		
		registerAttribute("clear_to_all_tran14", "clear_to_all_tran14", "LocalAttribute");		
		registerAttribute("clear_to_all_tran15", "clear_to_all_tran15", "LocalAttribute");		
		registerAttribute("clear_to_all_tran16", "clear_to_all_tran16", "LocalAttribute");		
		registerAttribute("clear_to_all_tran17", "clear_to_all_tran17", "LocalAttribute");		
		registerAttribute("clear_to_all_tran18", "clear_to_all_tran18", "LocalAttribute");		
		registerAttribute("clear_to_all_tran19", "clear_to_all_tran19", "LocalAttribute");		
		registerAttribute("clear_to_all_tran20", "clear_to_all_tran20", "LocalAttribute");	
		registerAttribute("clear_to_all_tran21", "clear_to_all_tran21", "LocalAttribute");		
		registerAttribute("clear_to_all_tran22", "clear_to_all_tran22", "LocalAttribute");		
		registerAttribute("clear_to_all_tran23", "clear_to_all_tran23", "LocalAttribute");		
		registerAttribute("clear_to_all_tran24", "clear_to_all_tran24", "LocalAttribute");		
		registerAttribute("clear_to_all_tran25", "clear_to_all_tran25", "LocalAttribute");		
		registerAttribute("clear_to_all_tran26", "clear_to_all_tran26", "LocalAttribute");		
		registerAttribute("clear_to_all_tran27", "clear_to_all_tran27", "LocalAttribute");		
		registerAttribute("clear_to_all_tran28", "clear_to_all_tran28", "LocalAttribute");		
		registerAttribute("clear_to_all_tran29", "clear_to_all_tran29", "LocalAttribute");		
		registerAttribute("clear_to_all_tran30", "clear_to_all_tran30", "LocalAttribute");	
		registerAttribute("clear_to_all_tran31", "clear_to_all_tran31", "LocalAttribute");		
		registerAttribute("clear_to_all_tran32", "clear_to_all_tran32", "LocalAttribute");		
		registerAttribute("clear_to_all_tran33", "clear_to_all_tran33", "LocalAttribute");	
		registerAttribute("clear_to_all_tran34", "clear_to_all_tran34", "LocalAttribute");	

		registerAttribute("send_notif_setting0", "send_notif_setting0", "LocalAttribute");	
		registerAttribute("send_notif_setting1", "send_notif_setting1", "LocalAttribute");		
		registerAttribute("send_notif_setting2", "send_notif_setting2", "LocalAttribute");		
		registerAttribute("send_notif_setting3", "send_notif_setting3", "LocalAttribute");		
		registerAttribute("send_notif_setting4", "send_notif_setting4", "LocalAttribute");		
		registerAttribute("send_notif_setting5", "send_notif_setting5", "LocalAttribute");		
		registerAttribute("send_notif_setting6", "send_notif_setting6", "LocalAttribute");		
		registerAttribute("send_notif_setting7", "send_notif_setting7", "LocalAttribute");		
		registerAttribute("send_notif_setting8", "send_notif_setting8", "LocalAttribute");		
		registerAttribute("send_notif_setting9", "send_notif_setting9", "LocalAttribute");		
		registerAttribute("send_notif_setting10", "send_notif_setting10", "LocalAttribute");	
		registerAttribute("send_notif_setting11", "send_notif_setting11", "LocalAttribute");		
		registerAttribute("send_notif_setting12", "send_notif_setting12", "LocalAttribute");		
		registerAttribute("send_notif_setting13", "send_notif_setting13", "LocalAttribute");		
		registerAttribute("send_notif_setting14", "send_notif_setting14", "LocalAttribute");		
		registerAttribute("send_notif_setting15", "send_notif_setting15", "LocalAttribute");		
		registerAttribute("send_notif_setting16", "send_notif_setting16", "LocalAttribute");		
		registerAttribute("send_notif_setting17", "send_notif_setting17", "LocalAttribute");		
		registerAttribute("send_notif_setting18", "send_notif_setting18", "LocalAttribute");		
		registerAttribute("send_notif_setting19", "send_notif_setting19", "LocalAttribute");		
		registerAttribute("send_notif_setting20", "send_notif_setting20", "LocalAttribute");	
		registerAttribute("send_notif_setting21", "send_notif_setting21", "LocalAttribute");		
		registerAttribute("send_notif_setting22", "send_notif_setting22", "LocalAttribute");		
		registerAttribute("send_notif_setting23", "send_notif_setting23", "LocalAttribute");		
		registerAttribute("send_notif_setting24", "send_notif_setting24", "LocalAttribute");		
		registerAttribute("send_notif_setting25", "send_notif_setting25", "LocalAttribute");		
		registerAttribute("send_notif_setting26", "send_notif_setting26", "LocalAttribute");		
		registerAttribute("send_notif_setting27", "send_notif_setting27", "LocalAttribute");		
		registerAttribute("send_notif_setting28", "send_notif_setting28", "LocalAttribute");		
		registerAttribute("send_notif_setting29", "send_notif_setting29", "LocalAttribute");		
		registerAttribute("send_notif_setting30", "send_notif_setting30", "LocalAttribute");	
		registerAttribute("send_notif_setting31", "send_notif_setting31", "LocalAttribute");		
		registerAttribute("send_notif_setting32", "send_notif_setting32", "LocalAttribute");		
		registerAttribute("send_notif_setting33", "send_notif_setting33", "LocalAttribute");	
		registerAttribute("send_notif_setting34", "send_notif_setting34", "LocalAttribute");

		registerAttribute("send_email_setting0", "send_email_setting0", "LocalAttribute");	
		registerAttribute("send_email_setting1", "send_email_setting1", "LocalAttribute");		
		registerAttribute("send_email_setting2", "send_email_setting2", "LocalAttribute");		
		registerAttribute("send_email_setting3", "send_email_setting3", "LocalAttribute");		
		registerAttribute("send_email_setting4", "send_email_setting4", "LocalAttribute");		
		registerAttribute("send_email_setting5", "send_email_setting5", "LocalAttribute");		
		registerAttribute("send_email_setting6", "send_email_setting6", "LocalAttribute");		
		registerAttribute("send_email_setting7", "send_email_setting7", "LocalAttribute");		
		registerAttribute("send_email_setting8", "send_email_setting8", "LocalAttribute");		
		registerAttribute("send_email_setting9", "send_email_setting9", "LocalAttribute");		
		registerAttribute("send_email_setting10", "send_email_setting10", "LocalAttribute");			
		registerAttribute("send_email_setting11", "send_email_setting11", "LocalAttribute");		
		registerAttribute("send_email_setting12", "send_email_setting12", "LocalAttribute");		
		registerAttribute("send_email_setting13", "send_email_setting13", "LocalAttribute");		
		registerAttribute("send_email_setting14", "send_email_setting14", "LocalAttribute");		
		registerAttribute("send_email_setting15", "send_email_setting15", "LocalAttribute");		
		registerAttribute("send_email_setting16", "send_email_setting16", "LocalAttribute");		
		registerAttribute("send_email_setting17", "send_email_setting17", "LocalAttribute");		
		registerAttribute("send_email_setting18", "send_email_setting18", "LocalAttribute");		
		registerAttribute("send_email_setting19", "send_email_setting19", "LocalAttribute");		
		registerAttribute("send_email_setting20", "send_email_setting20", "LocalAttribute");	
		registerAttribute("send_email_setting21", "send_email_setting21", "LocalAttribute");		
		registerAttribute("send_email_setting22", "send_email_setting22", "LocalAttribute");		
		registerAttribute("send_email_setting23", "send_email_setting23", "LocalAttribute");		
		registerAttribute("send_email_setting24", "send_email_setting24", "LocalAttribute");		
		registerAttribute("send_email_setting25", "send_email_setting25", "LocalAttribute");		
		registerAttribute("send_email_setting26", "send_email_setting26", "LocalAttribute");		
		registerAttribute("send_email_setting27", "send_email_setting27", "LocalAttribute");		
		registerAttribute("send_email_setting28", "send_email_setting28", "LocalAttribute");		
		registerAttribute("send_email_setting29", "send_email_setting29", "LocalAttribute");		
		registerAttribute("send_email_setting30", "send_email_setting30", "LocalAttribute");	
		registerAttribute("send_email_setting31", "send_email_setting31", "LocalAttribute");		
		registerAttribute("send_email_setting32", "send_email_setting32", "LocalAttribute");		
		registerAttribute("send_email_setting33", "send_email_setting33", "LocalAttribute");	
		registerAttribute("send_email_setting34", "send_email_setting34", "LocalAttribute");

		registerAttribute("additional_email_addr0", "additional_email_addr0", "LocalAttribute");	
		registerAttribute("additional_email_addr1", "additional_email_addr1", "LocalAttribute");		
		registerAttribute("additional_email_addr2", "additional_email_addr2", "LocalAttribute");		
		registerAttribute("additional_email_addr3", "additional_email_addr3", "LocalAttribute");		
		registerAttribute("additional_email_addr4", "additional_email_addr4", "LocalAttribute");		
		registerAttribute("additional_email_addr5", "additional_email_addr5", "LocalAttribute");		
		registerAttribute("additional_email_addr6", "additional_email_addr6", "LocalAttribute");		
		registerAttribute("additional_email_addr7", "additional_email_addr7", "LocalAttribute");		
		registerAttribute("additional_email_addr8", "additional_email_addr8", "LocalAttribute");		
		registerAttribute("additional_email_addr9", "additional_email_addr9", "LocalAttribute");		
		registerAttribute("additional_email_addr10", "additional_email_addr10", "LocalAttribute");			
		registerAttribute("additional_email_addr11", "additional_email_addr11", "LocalAttribute");		
		registerAttribute("additional_email_addr12", "additional_email_addr12", "LocalAttribute");		
		registerAttribute("additional_email_addr13", "additional_email_addr13", "LocalAttribute");		
		registerAttribute("additional_email_addr14", "additional_email_addr14", "LocalAttribute");		
		registerAttribute("additional_email_addr15", "additional_email_addr15", "LocalAttribute");		
		registerAttribute("additional_email_addr16", "additional_email_addr16", "LocalAttribute");		
		registerAttribute("additional_email_addr17", "additional_email_addr17", "LocalAttribute");		
		registerAttribute("additional_email_addr18", "additional_email_addr18", "LocalAttribute");		
		registerAttribute("additional_email_addr19", "additional_email_addr19", "LocalAttribute");		
		registerAttribute("additional_email_addr20", "additional_email_addr20", "LocalAttribute");	
		registerAttribute("additional_email_addr21", "additional_email_addr21", "LocalAttribute");		
		registerAttribute("additional_email_addr22", "additional_email_addr22", "LocalAttribute");		
		registerAttribute("additional_email_addr23", "additional_email_addr23", "LocalAttribute");		
		registerAttribute("additional_email_addr24", "additional_email_addr24", "LocalAttribute");		
		registerAttribute("additional_email_addr25", "additional_email_addr25", "LocalAttribute");		
		registerAttribute("additional_email_addr26", "additional_email_addr26", "LocalAttribute");		
		registerAttribute("additional_email_addr27", "additional_email_addr27", "LocalAttribute");		
		registerAttribute("additional_email_addr28", "additional_email_addr28", "LocalAttribute");		
		registerAttribute("additional_email_addr29", "additional_email_addr29", "LocalAttribute");		
		registerAttribute("additional_email_addr30", "additional_email_addr30", "LocalAttribute");	
		registerAttribute("additional_email_addr31", "additional_email_addr31", "LocalAttribute");		
		registerAttribute("additional_email_addr32", "additional_email_addr32", "LocalAttribute");		
		registerAttribute("additional_email_addr33", "additional_email_addr33", "LocalAttribute");	
		registerAttribute("additional_email_addr34", "additional_email_addr34", "LocalAttribute");

		registerAttribute("notification_user_ids0", "notification_user_ids0", "LocalAttribute");	
		registerAttribute("notification_user_ids1", "notification_user_ids1", "LocalAttribute");		
		registerAttribute("notification_user_ids2", "notification_user_ids2", "LocalAttribute");		
		registerAttribute("notification_user_ids3", "notification_user_ids3", "LocalAttribute");		
		registerAttribute("notification_user_ids4", "notification_user_ids4", "LocalAttribute");		
		registerAttribute("notification_user_ids5", "notification_user_ids5", "LocalAttribute");		
		registerAttribute("notification_user_ids6", "notification_user_ids6", "LocalAttribute");		
		registerAttribute("notification_user_ids7", "notification_user_ids7", "LocalAttribute");		
		registerAttribute("notification_user_ids8", "notification_user_ids8", "LocalAttribute");		
		registerAttribute("notification_user_ids9", "notification_user_ids9", "LocalAttribute");		
		registerAttribute("notification_user_ids10", "notification_user_ids10", "LocalAttribute");			
		registerAttribute("notification_user_ids11", "notification_user_ids11", "LocalAttribute");		
		registerAttribute("notification_user_ids12", "notification_user_ids12", "LocalAttribute");		
		registerAttribute("notification_user_ids13", "notification_user_ids13", "LocalAttribute");		
		registerAttribute("notification_user_ids14", "notification_user_ids14", "LocalAttribute");		
		registerAttribute("notification_user_ids15", "notification_user_ids15", "LocalAttribute");		
		registerAttribute("notification_user_ids16", "notification_user_ids16", "LocalAttribute");		
		registerAttribute("notification_user_ids17", "notification_user_ids17", "LocalAttribute");		
		registerAttribute("notification_user_ids18", "notification_user_ids18", "LocalAttribute");		
		registerAttribute("notification_user_ids19", "notification_user_ids19", "LocalAttribute");		
		registerAttribute("notification_user_ids20", "notification_user_ids20", "LocalAttribute");	
		registerAttribute("notification_user_ids21", "notification_user_ids21", "LocalAttribute");		
		registerAttribute("notification_user_ids22", "notification_user_ids22", "LocalAttribute");		
		registerAttribute("notification_user_ids23", "notification_user_ids23", "LocalAttribute");		
		registerAttribute("notification_user_ids24", "notification_user_ids24", "LocalAttribute");		
		registerAttribute("notification_user_ids25", "notification_user_ids25", "LocalAttribute");		
		registerAttribute("notification_user_ids26", "notification_user_ids26", "LocalAttribute");		
		registerAttribute("notification_user_ids27", "notification_user_ids27", "LocalAttribute");		
		registerAttribute("notification_user_ids28", "notification_user_ids28", "LocalAttribute");		
		registerAttribute("notification_user_ids29", "notification_user_ids29", "LocalAttribute");		
		registerAttribute("notification_user_ids30", "notification_user_ids30", "LocalAttribute");	
		registerAttribute("notification_user_ids31", "notification_user_ids31", "LocalAttribute");		
		registerAttribute("notification_user_ids32", "notification_user_ids32", "LocalAttribute");		
		registerAttribute("notification_user_ids33", "notification_user_ids33", "LocalAttribute");	
		registerAttribute("notification_user_ids34", "notification_user_ids34", "LocalAttribute");	
		
		registerAttribute("update_recipients_only_tran0", "update_recipients_only_tran0", "LocalAttribute");		
		registerAttribute("update_recipients_only_tran1", "update_recipients_only_tran1", "LocalAttribute");		
		registerAttribute("update_recipients_only_tran2", "update_recipients_only_tran2", "LocalAttribute");		
		registerAttribute("update_recipients_only_tran3", "update_recipients_only_tran3", "LocalAttribute");		
		registerAttribute("update_recipients_only_tran4", "update_recipients_only_tran4", "LocalAttribute");		
		registerAttribute("update_recipients_only_tran5", "update_recipients_only_tran5", "LocalAttribute");		
		registerAttribute("update_recipients_only_tran6", "update_recipients_only_tran6", "LocalAttribute");		
		registerAttribute("update_recipients_only_tran7", "update_recipients_only_tran7", "LocalAttribute");		
		registerAttribute("update_recipients_only_tran8", "update_recipients_only_tran8", "LocalAttribute");		
		registerAttribute("update_recipients_only_tran9", "update_recipients_only_tran9", "LocalAttribute");		
		registerAttribute("update_recipients_only_tran10", "update_recipients_only_tran10", "LocalAttribute");	
		registerAttribute("update_recipients_only_tran11", "update_recipients_only_tran11", "LocalAttribute");		
		registerAttribute("update_recipients_only_tran12", "update_recipients_only_tran12", "LocalAttribute");		
		registerAttribute("update_recipients_only_tran13", "update_recipients_only_tran13", "LocalAttribute");		
		registerAttribute("update_recipients_only_tran14", "update_recipients_only_tran14", "LocalAttribute");		
		registerAttribute("update_recipients_only_tran15", "update_recipients_only_tran15", "LocalAttribute");		
		registerAttribute("update_recipients_only_tran16", "update_recipients_only_tran16", "LocalAttribute");		
		registerAttribute("update_recipients_only_tran17", "update_recipients_only_tran17", "LocalAttribute");		
		registerAttribute("update_recipients_only_tran18", "update_recipients_only_tran18", "LocalAttribute");		
		registerAttribute("update_recipients_only_tran19", "update_recipients_only_tran19", "LocalAttribute");		
		registerAttribute("update_recipients_only_tran20", "update_recipients_only_tran20", "LocalAttribute");	
		registerAttribute("update_recipients_only_tran21", "update_recipients_only_tran21", "LocalAttribute");		
		registerAttribute("update_recipients_only_tran22", "update_recipients_only_tran22", "LocalAttribute");		
		registerAttribute("update_recipients_only_tran23", "update_recipients_only_tran23", "LocalAttribute");		
		registerAttribute("update_recipients_only_tran24", "update_recipients_only_tran24", "LocalAttribute");		
		registerAttribute("update_recipients_only_tran25", "update_recipients_only_tran25", "LocalAttribute");		
		registerAttribute("update_recipients_only_tran26", "update_recipients_only_tran26", "LocalAttribute");		
		registerAttribute("update_recipients_only_tran27", "update_recipients_only_tran27", "LocalAttribute");		
		registerAttribute("update_recipients_only_tran28", "update_recipients_only_tran28", "LocalAttribute");		
		registerAttribute("update_recipients_only_tran29", "update_recipients_only_tran29", "LocalAttribute");		
		registerAttribute("update_recipients_only_tran30", "update_recipients_only_tran30", "LocalAttribute");	
		registerAttribute("update_recipients_only_tran31", "update_recipients_only_tran31", "LocalAttribute");		
		registerAttribute("update_recipients_only_tran32", "update_recipients_only_tran32", "LocalAttribute");		
		registerAttribute("update_recipients_only_tran33", "update_recipients_only_tran33", "LocalAttribute");	
		registerAttribute("update_recipients_only_tran34", "update_recipients_only_tran34", "LocalAttribute");	
		
		registerAttribute("update_emails_only_tran0", "update_emails_only_tran0", "LocalAttribute");		
		registerAttribute("update_emails_only_tran1", "update_emails_only_tran1", "LocalAttribute");		
		registerAttribute("update_emails_only_tran2", "update_emails_only_tran2", "LocalAttribute");		
		registerAttribute("update_emails_only_tran3", "update_emails_only_tran3", "LocalAttribute");		
		registerAttribute("update_emails_only_tran4", "update_emails_only_tran4", "LocalAttribute");		
		registerAttribute("update_emails_only_tran5", "update_emails_only_tran5", "LocalAttribute");		
		registerAttribute("update_emails_only_tran6", "update_emails_only_tran6", "LocalAttribute");		
		registerAttribute("update_emails_only_tran7", "update_emails_only_tran7", "LocalAttribute");		
		registerAttribute("update_emails_only_tran8", "update_emails_only_tran8", "LocalAttribute");		
		registerAttribute("update_emails_only_tran9", "update_emails_only_tran9", "LocalAttribute");		
		registerAttribute("update_emails_only_tran10", "update_emails_only_tran10", "LocalAttribute");	
		registerAttribute("update_emails_only_tran11", "update_emails_only_tran11", "LocalAttribute");		
		registerAttribute("update_emails_only_tran12", "update_emails_only_tran12", "LocalAttribute");		
		registerAttribute("update_emails_only_tran13", "update_emails_only_tran13", "LocalAttribute");		
		registerAttribute("update_emails_only_tran14", "update_emails_only_tran14", "LocalAttribute");		
		registerAttribute("update_emails_only_tran15", "update_emails_only_tran15", "LocalAttribute");		
		registerAttribute("update_emails_only_tran16", "update_emails_only_tran16", "LocalAttribute");		
		registerAttribute("update_emails_only_tran17", "update_emails_only_tran17", "LocalAttribute");		
		registerAttribute("update_emails_only_tran18", "update_emails_only_tran18", "LocalAttribute");		
		registerAttribute("update_emails_only_tran19", "update_emails_only_tran19", "LocalAttribute");		
		registerAttribute("update_emails_only_tran20", "update_emails_only_tran20", "LocalAttribute");	
		registerAttribute("update_emails_only_tran21", "update_emails_only_tran21", "LocalAttribute");		
		registerAttribute("update_emails_only_tran22", "update_emails_only_tran22", "LocalAttribute");		
		registerAttribute("update_emails_only_tran23", "update_emails_only_tran23", "LocalAttribute");		
		registerAttribute("update_emails_only_tran24", "update_emails_only_tran24", "LocalAttribute");		
		registerAttribute("update_emails_only_tran25", "update_emails_only_tran25", "LocalAttribute");		
		registerAttribute("update_emails_only_tran26", "update_emails_only_tran26", "LocalAttribute");		
		registerAttribute("update_emails_only_tran27", "update_emails_only_tran27", "LocalAttribute");		
		registerAttribute("update_emails_only_tran28", "update_emails_only_tran28", "LocalAttribute");		
		registerAttribute("update_emails_only_tran29", "update_emails_only_tran29", "LocalAttribute");		
		registerAttribute("update_emails_only_tran30", "update_emails_only_tran30", "LocalAttribute");	
		registerAttribute("update_emails_only_tran31", "update_emails_only_tran31", "LocalAttribute");		
		registerAttribute("update_emails_only_tran32", "update_emails_only_tran32", "LocalAttribute");		
		registerAttribute("update_emails_only_tran33", "update_emails_only_tran33", "LocalAttribute");	
		registerAttribute("update_emails_only_tran34", "update_emails_only_tran34", "LocalAttribute");	
		
		registerAttribute("c_InvoiceCRNoteEmailNotification", "c_notif_inv_crn_oid", "NumberAttribute");
		registerAttribute("c_SPEmailNotifications", "c_notif_spemail_oid", "NumberAttribute");
   
   }
}