

  

package com.ams.tradeportal.busobj.webbean;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import javax.servlet.http.*;

/**
 * 
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class PaymentUploadErrorLogWebBean_Base extends com.amsinc.ecsg.web.AmsEntityWebBean
{

  public void registerAttributes()
   {  


       /* payment_upload_error_log_oid - Unique identifier */
       registerAttribute("payment_upload_error_log_oid", "payment_upload_error_log_oid", "ObjectIDAttribute");


       /* beneficiary_info - Beneficiary Info (Name, customer ref, amount) */
       registerAttribute("beneficiary_info", "beneficiary_info");


       /* error_msg - Error message. */
       registerAttribute("error_msg", "error_msg");


       /* line_number - The line number in the upload file. */
       registerAttribute("line_number", "line_number", "NumberAttribute");

      /* Pointer to the parent PaymentFileUpload */
       registerAttribute("payment_file_upload_oid", "p_payment_file_upload_oid", "ParentIDAttribute");
       
       /* Pointer to domestic_payment_oid */
       registerAttribute("domestic_payment_oid", "p_domestic_payment_oid", "NumberAttribute");
       
       /* reporting_code_error_ind - Indicates if the error is related to Reporting code errors  */
       registerAttribute("reporting_code_error_ind", "reporting_code_error_ind", "IndicatorAttribute");
   }
   
   




}
