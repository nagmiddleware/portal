package com.ams.tradeportal.busobj.webbean;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;

import java.rmi.*;
import java.util.*;
import java.math.*;

import javax.ejb.*;

import com.ams.tradeportal.common.*;

import javax.servlet.http.*;

import com.ams.tradeportal.html.*;

/**
 * Business object to store commonly used BankGrpRestrictRules
 *
 * Copyright � 2015 CGI, Incorporated All rights reserved
 */
public class BankGrpRestrictRuleWebBean_Base extends ReferenceDataWebBean {

	public void registerAttributes() {
		super.registerAttributes();
		/* bank_grp_restrict_rules_oid - Unique identifier */
		registerAttribute("bank_grp_restrict_rules_oid",
				"BANK_GRP_RESTRICT_RULES_OID", "ObjectIDAttribute");
		/* description - required */
		registerAttribute("description", "DESCRIPTION");
		/* Pointer to the parent ReferenceDataOwner */
		registerAttribute("client_bank_oid", "P_CLIENT_BANK_OID",
				"ParentIDAttribute");
	}

}
