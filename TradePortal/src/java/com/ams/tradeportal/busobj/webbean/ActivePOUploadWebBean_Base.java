

  

package com.ams.tradeportal.busobj.webbean;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import javax.servlet.http.*;
import com.ams.tradeportal.html.*;

/**
 * When a user initiates an Auto LC Create operation by uploading a file, a
 * row is placed into this table.   This row contains data about the current
 * process.  Prior to starting an Auto LC Create operation, the presence of
 * a row in this table is checked for.  If there is a row, the user is not
 * allowed to move forward with the process.
 * 
 * The AutoLCAgent looks for rows in this table in order to kick off the actual
 * process of creating LCs from purchase orders.
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class ActivePOUploadWebBean_Base extends TradePortalBusinessObjectWebBean
{

  public void registerAttributes()
   {  
      super.registerAttributes();


       /* active_po_upload_oid - Unique identifier */
       registerAttribute("active_po_upload_oid", "active_po_upload_oid", "ObjectIDAttribute");


       /* creation_timestamp - Timestamp (in GMT) of when this business object instance was created */
       registerAttribute("creation_timestamp", "creation_timestamp", "DateTimeAttribute");


       /* status - The status of the upload process. */
       registerAttribute("status", "status");


       /* po_data_file - The contents of the file that was uploaded by the user for this Auto LC
         Create process. */
       registerAttribute("po_data_file", "po_data_file");


       /* po_upload_parameters - An XML document containing information about what the user wants to happen
         with the data they just uploaded. */
       registerAttribute("po_upload_parameters", "po_upload_parameters");


       /* agent_id - The ID of the agent that picked up this row for processing. */
       registerAttribute("agent_id", "agent_id");

       /* corp_org_oid - The corporate organiztaion that is currently performing an upload of purchase
         orders. */
       registerAttribute("corp_org_oid", "a_corp_org_oid", "NumberAttribute");
   }
   
   




}
