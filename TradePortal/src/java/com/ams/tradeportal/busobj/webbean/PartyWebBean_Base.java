

  

package com.ams.tradeportal.busobj.webbean;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import javax.servlet.http.*;

/**
 * A business (corporate or bank) that will be a party to an instrument in
 * the portal.    This business object is part of reference data.   The TermsParty
 * business object contains parties that are actually a part of instruments.
 * 
 * When a party is added to an instrument, the data for the party is copied
 * out of the Party business object and into the TermsParty business object.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class PartyWebBean_Base extends ReferenceDataWebBean
{

  public void registerAttributes()
   {  
      super.registerAttributes();


       /* party_oid - Unique identifier */
       registerAttribute("party_oid", "party_oid", "ObjectIDAttribute");


       /* contact_name - Name of primary contact for this party */
       registerAttribute("contact_name", "contact_name");


       /* contact_title - Title of the primary contact for this party */
       registerAttribute("contact_title", "contact_title");


       /* contact_phone - Phone Number for the primary contact for this party */
       registerAttribute("contact_phone", "contact_phone");


       /* contact_fax - Fax number for the primary contact of this party */
       registerAttribute("contact_fax", "contact_fax");


       /* contact_email - E-mail of the primary contact for this party */
       registerAttribute("contact_email", "contact_email");


       /* telex_1 - Telex id for this party */
       registerAttribute("telex_1", "telex_1");


       /* telex_answer_back_1 - Telex answer back ID for this party */
       registerAttribute("telex_answer_back_1", "telex_answer_back_1");


       /* fax_1 - The primary fax number of this party */
       registerAttribute("fax_1", "fax_1");


       /* fax_2 - The secondary fax number of this party */
       registerAttribute("fax_2", "fax_2");


       /* swift_address_part1 - Part 1 of the swift address of this party */
       registerAttribute("swift_address_part1", "swift_address_part1");


       /* swift_address_part2 - Part 1 of the swift address of this party */
       registerAttribute("swift_address_part2", "swift_address_part2");


       /* address_line_1 - First line of the party's address */
       registerAttribute("address_line_1", "address_line_1");


       /* address_line_2 - First line of the party's address */
       registerAttribute("address_line_2", "address_line_2");


       /* party_type_code - Indicates whether this party is a bank or a non-bank corporation. */
       registerAttribute("party_type_code", "party_type_code");


       /* address_city - The city in which the party is located */
       registerAttribute("address_city", "address_city");


       /* address_state_province - The state or province in which the party is located */
       registerAttribute("address_state_province", "address_state_province");


       /* address_country - The country in which the party is located */
       registerAttribute("address_country", "address_country");


       /* address_postal_code - The postal code in which the party is located */
       registerAttribute("address_postal_code", "address_postal_code");


       /* OTL_customer_id - The unique identifier of this party on OTL, the back end system.   This
         is not used by the Trade Portal, but is used by OTL to determine which party
         stored in OTL represents a party referenced on the Trade Portal. */
       registerAttribute("OTL_customer_id", "OTL_customer_id");


       /* phone_number - The party's phone number */
       registerAttribute("phone_number", "phone_number");


       /* branch_code - Branch code of the party (used only for bank parties) */
       registerAttribute("branch_code", "branch_code");


       /* vendor_id - A 15 character alphanumeric value representing the vendor ID associated
         with this party */
       registerAttribute("vendor_id", "vendor_id");


       /* unicode_indicator - Whether the party attributes have unicode characters that are not compatible
         with WE8WIN1252 encoding.  Such parties can only be used for Cash Management
         Payment transaction. */
       registerAttribute("unicode_indicator", "unicode_indicator", "IndicatorAttribute");

      /* Pointer to the parent ReferenceDataOwner */
       registerAttribute("owner_org_oid", "p_owner_org_oid", "ParentIDAttribute");

       /* designated_bank_oid - Each bank can be assigned a "designated bank".  This association points
         to another party as the designated bank.   When a party with a designated
         bank is added to a transaction, the designated bank is added as well in
         another party field. */
       registerAttribute("designated_bank_oid", "a_designated_bank_oid", "NumberAttribute");
       
       //Nar Rel9.5.0.0 CR-1132 01/27/2016 - Begin
	   //added attributes for Portal only Bank Additional address user defined fields
	    registerAttribute("user_defined_field_1", "user_defined_field_1");
	    registerAttribute("user_defined_field_2", "user_defined_field_2");
	    registerAttribute("user_defined_field_3", "user_defined_field_3");
	    registerAttribute("user_defined_field_4", "user_defined_field_4");
	   //Nar Rel9.5.0.0 CR-1132 01/27/2016 - End
   }
   
   




}
