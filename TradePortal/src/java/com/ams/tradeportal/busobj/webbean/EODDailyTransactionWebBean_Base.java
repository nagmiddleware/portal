

  

package com.ams.tradeportal.busobj.webbean;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import javax.servlet.http.*;

/**
 * An Incoming End of Day Transaction Request message will be sent to the portal
 * by the bank.  There will potentially be multiple of these transactions recordsper
 * account each day.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class EODDailyTransactionWebBean_Base extends TradePortalBusinessObjectWebBean
{

  public void registerAttributes()
   {  
      super.registerAttributes();


       /* eod_daily_trans_oid - Unique Identifier */
       registerAttribute("eod_daily_trans_oid", "eod_daily_trans_oid", "ObjectIDAttribute");


       /* transaction_date - The date of the transaction in YYYYMMDD format. */
       registerAttribute("transaction_date", "transaction_date", "DateTimeAttribute");


       /* transaction_type - Bank-specific transaction type. Not validated against any reference/code
         tables. */
       registerAttribute("transaction_type", "transaction_type");


       /* transaction_reference - Indicates the source/type of account. */
       registerAttribute("transaction_reference", "transaction_reference");


       /* transaction_amount - Transaction amount (signed amount). There will be an explicit decimal point.
         First character is always either a "+", which represents a Credit or a "-"
         which represents a Debit. Right justified. Zero filled */
       registerAttribute("transaction_amount", "transaction_amount", "TradePortalSignedDecimalAttribute");


       /* transaction_narrative - Transaction narrative. */
       registerAttribute("transaction_narrative", "transaction_narrative");


       /* effective_date - Value date of the transaction in YYYYMMDD format. */
       registerAttribute("effective_date", "effective_date", "DateTimeAttribute");


       /* trace_id - Trace Details. */
       registerAttribute("trace_id", "trace_id");


       /* transaction_code - Bank-specific transaction code. Not validated against any reference/code
         tables. */
       registerAttribute("transaction_code", "transaction_code");


       /* auxiliary_dom - Auxiliary Domestic - bank-specific structured information regarding the
         transaction. Not validate against the bank's AuxDom/Trancode, */
       registerAttribute("auxiliary_dom", "auxiliary_dom");


       /* ex_auxiliary_dom - ExAuxiliary Domestic - additional bank-specific structured information regarding
         the transaction. Not validate against the bank's AuxDom/Trancode, */
       registerAttribute("ex_auxiliary_dom", "ex_auxiliary_dom");


       /* bai_code - BAI code. Not validated against any reference/code tables. */
       registerAttribute("bai_code", "bai_code");


       /* remitter_name - Name of the remitter. */
       registerAttribute("remitter_name", "remitter_name");


       /* lodgment_reference - Lodgment Reference. */
       registerAttribute("lodgment_reference", "lodgment_reference");


       /* short_description - Short description */
       registerAttribute("short_description", "short_description");


       /* number_collection_items - Number of collection items */
       registerAttribute("number_collection_items", "number_collection_items", "NumberAttribute");


       /* sequence_number - A sequence number to be used in the portal to allow the transactions for
         one transaction date to be sorted and presented in the correct order on
         the report. */
       registerAttribute("sequence_number", "sequence_number", "NumberAttribute");
       /* date_time_received - Date and time on which EOD Data was received. */
       registerAttribute("date_time_received", "date_time_received", "DateTimeAttribute");

      /* Pointer to the parent EODDailyData */
       registerAttribute("eod_daily_data_oid", "p_eod_daily_data_oid", "ParentIDAttribute");
   }
   
   




}
