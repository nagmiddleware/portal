

  

package com.ams.tradeportal.busobj.webbean;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import javax.servlet.http.*;

/**
 * The Payment Files that are to be uploaded.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class InvoiceFinanceAmountQueueWebBean_Base extends com.amsinc.ecsg.web.AmsEntityWebBean
{

  public void registerAttributes()
   {  

	   /* invoice_fin_amount_queue_oid - Unique Identifier. */
	   registerAttribute("invoice_fin_amount_queue_oid", "invoice_fin_amount_queue_oid", "ObjectIDAttribute");
	         
	   /* creation_timestamp - The time the item was created. */
	   registerAttribute("creation_timestamp", "creation_timestamp", "DateTimeAttribute");
	         
	   /* status - The status of the item. */
	   registerAttribute("status", "status", "INVOICE_FIN_AMOUNT_STATUS");
	    
	   /* agent_id - The agent who is processing this item. */
	   registerAttribute("agent_id", "agent_id");
	    
	   /* Interest_discount_rate_group_oid - The id of the interest discount rate group to process with this request. */
	   registerAttribute("interest_disc_rate_grp_oid", "a_interest_disc_rate_grp_oid", "InterestDiscountRateGroup");

	   /* error_msg � Error message of recalculation. */
	   registerAttribute("error_msg", "error_msg");


   }
   
   




}
