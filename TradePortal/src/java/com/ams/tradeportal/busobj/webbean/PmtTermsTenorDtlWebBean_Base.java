//Leelavathi - 10thDec2012 - Rel8200 CR-737 - Initialversion

package com.ams.tradeportal.busobj.webbean;


/**
 * 
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class PmtTermsTenorDtlWebBean_Base extends TradePortalBusinessObjectWebBean
{

  public void registerAttributes()
   {  
      super.registerAttributes();
       registerAttribute("pmt_terms_tenor_dtl_oid", "pmt_terms_tenor_dtl_oid", "ObjectIDAttribute");
       registerAttribute("percent", "percent", "NumberAttribute");
       registerAttribute("amount", "amount", "DecimalAttribute");
       registerAttribute("tenor_type", "tenor_type");
       registerAttribute("num_days_after", "num_days_after", "NumberAttribute");
       registerAttribute("days_after_type", "days_after_type");
       registerAttribute("maturity_date", "maturity_date", "DateTimeAttribute");
       registerAttribute("terms_oid", "p_terms_oid", "ParentIDAttribute");

   }
   
   




}
