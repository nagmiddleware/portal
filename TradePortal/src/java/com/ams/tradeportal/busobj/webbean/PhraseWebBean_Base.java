

  

package com.ams.tradeportal.busobj.webbean;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import javax.servlet.http.*;
import com.ams.tradeportal.html.*;

/**
 * Business object to store commonly used phrases that comprise instruments
 * created in the Trade Portal.  Phrases then can be added into text fields
 * on  instruments.
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class PhraseWebBean_Base extends ReferenceDataWebBean
{

  public void registerAttributes()
   {  
      super.registerAttributes();


       /* phrase_oid - Unique identifier */
       registerAttribute("phrase_oid", "phrase_oid", "ObjectIDAttribute");


       /* phrase_category - The phrase category to which this phrase belongs.   Phrase category is used
         to determine which text boxes it can be added to.   Some example of phrase
         catgeories are goods description, payment instructions, etc.
 */
       registerAttribute("phrase_category", "phrase_category");


       /* phrase_text - The text that comprises the phrase */
       registerAttribute("phrase_text", "phrase_text");


       /* phrase_type - Indicates whether the phrase is modifiable.   The default for this setting
         is modifiable, but admin users may set it to static or partially modifiable
         (fill in the blank).  The phrase type is only looked at on the guarantee
         pages. */
       registerAttribute("phrase_type", "phrase_type");

      /* Pointer to the parent ReferenceDataOwner */
       registerAttribute("owner_org_oid", "p_owner_org_oid", "ParentIDAttribute");
   }
   
   




}
