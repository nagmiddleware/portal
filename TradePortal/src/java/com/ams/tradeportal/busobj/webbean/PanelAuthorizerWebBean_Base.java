package com.ams.tradeportal.busobj.webbean;

/**
 * A PanelAuthorizer consists of a set of authorize users.
 *
 *     Copyright  @ 2003
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class PanelAuthorizerWebBean_Base extends TradePortalBusinessObjectWebBean
{

  public void registerAttributes()
   {
      super.registerAttributes();


      /* panel_authorizer_oid - Unique identifier */
      registerAttribute("panel_authorizer_oid", "panel_authorizer_oid", "ObjectIDAttribute");

      /* panel_authority_code - The level at which the reference data is owned (Proponix, client bank, bank
      organization group, or corporate customer */
      registerAttribute("panel_code", "panel_code");

      /* user_oid - The user who authorize the transaction. */
      registerAttribute("user_oid", "a_user_oid", "NumberAttribute");

      /*authorize_status_date - Timestamp (in GMT) of when authorization took place on this transaction. */
      registerAttribute("auth_date_time", "auth_date_time", "DateTimeAttribute");
      
      /* owner_object_type - the type of object that is the owner of this user */
      registerAttribute("owner_object_type", "owner_object_type");

      /* Pointer to the parent Transaction */
      registerAttribute("owner_oid", "p_owner_oid", "ParentIDAttribute");

   }

}
