

  

package com.ams.tradeportal.busobj.webbean;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import javax.servlet.http.*;
import com.ams.tradeportal.html.*;

/**
 * When an instrument is purged from the Trade Portal database after a period
 * of time, a record is maintained in this table of the instruments that were
 * deleted.
 * 
 * This is populated by the middleware.
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class InstrumentPurgeHistoryWebBean_Base extends TradePortalBusinessObjectWebBean
{

  public void registerAttributes()
   {  
      super.registerAttributes();


       /* instr_purge_history_oid - Unique identifier */
       registerAttribute("instr_purge_history_oid", "instr_purge_history_oid", "ObjectIDAttribute");


       /* instrument_id - The complete instrument id of the instrument that was deleted */
       registerAttribute("instrument_id", "instrument_id");


       /* purge_timestamp - A timestamp (in GMT) of when the instrument was deleted. */
       registerAttribute("purge_timestamp", "purge_timestamp", "DateTimeAttribute");


       /* instrument_type_code - The instrument type code of the instrument that was deleted. */
       registerAttribute("instrument_type_code", "instrument_type_code");


       /* amount - The instrument amount of the instrument that was deleted. */
       registerAttribute("amount", "amount", "TradePortalDecimalAttribute");


       /* currency - The currency of the instrument that was deleted */
       registerAttribute("currency", "currency");


       /* counterparty_name - The name of the counterparty for the instrument that was deleted. */
       registerAttribute("counterparty_name", "counterparty_name");

      /* Pointer to the parent CorporateOrganization */
       registerAttribute("corp_org_oid", "p_corp_org_oid", "ParentIDAttribute");
   }
   
   




}
