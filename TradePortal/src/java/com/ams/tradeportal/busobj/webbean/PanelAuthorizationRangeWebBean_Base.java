



package com.ams.tradeportal.busobj.webbean;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import javax.servlet.http.*;

/**
 * "Panel authorization" allows thresholds of authorization to be set at the
 * corporation level for each payment instrument (not trade instruments).
 * When a payment instrument goes through the authorization process, if panel
 * authorization is selected for the corporation, the system will use the type
 * and amount of the payment instrument and look at the corresponding level
 * (threshold) to determine the total number of authorizers and the profile
 * / panel level that the user must have (based on the new Panel Authority
 * -  A, B or C - present on the authorizing user's profile).
 *
 *     Copyright  © 2003
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class PanelAuthorizationRangeWebBean_Base extends TradePortalBusinessObjectWebBean
{

  public void registerAttributes()
   {
      super.registerAttributes();


       /* panel_auth_range_oid - Unique identifier */
       registerAttribute("panel_auth_range_oid", "panel_auth_range_oid", "ObjectIDAttribute");


       /* panel_auth_range_min_amt - The minimum amount of a Panel Authorization range of amounts. */
       registerAttribute("panel_auth_range_min_amt", "panel_auth_range_min_amt", "TradePortalDecimalAttribute");


       /* panel_auth_range_max_amt - The maximum amount of a Panel Authorization range of amounts.  The last
         field in the last range can be left empty (or it may be determined in the
         development process that a large number such as 999,999,999,999.999 should
         be entered or an "*" or some other symbol representing 'infinity' should
         be entered if no upper limit is entered. */
       registerAttribute("panel_auth_range_max_amt", "panel_auth_range_max_amt", "TradePortalDecimalAttribute");
       //
      
   }






}
