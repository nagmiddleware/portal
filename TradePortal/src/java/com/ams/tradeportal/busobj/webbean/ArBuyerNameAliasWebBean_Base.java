

  

package com.ams.tradeportal.busobj.webbean;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import javax.servlet.http.*;

/**
 * The aliases for the AR Buyer Name in an ARMatchingRule.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class ArBuyerNameAliasWebBean_Base extends com.amsinc.ecsg.web.AmsEntityWebBean
{

  public void registerAttributes()
   {  


       /* ar_buyer_name_alias_oid - Unique Identifier */
       registerAttribute("ar_buyer_name_alias_oid", "ar_buyer_name_alias_oid", "ObjectIDAttribute");


       /* buyer_name_alias - Buyer Name Alias */
       registerAttribute("buyer_name_alias", "buyer_name_alias");

      /* Pointer to the parent ArMatchingRule */
       registerAttribute("ar_matching_rule_oid", "p_ar_matching_rule_oid", "ParentIDAttribute");
   }
   
   




}
