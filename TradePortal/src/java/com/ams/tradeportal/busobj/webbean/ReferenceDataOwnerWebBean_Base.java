

  

package com.ams.tradeportal.busobj.webbean;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import javax.servlet.http.*;
import com.ams.tradeportal.html.*;

/**
 * Any organization that can own reference data is a subclass of this business
 * object.  It defines one-to-many component relationships to reference data
 * such as phrases, templates, etc.
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class ReferenceDataOwnerWebBean_Base extends OrganizationWebBean
{

  public void registerAttributes()
   {  
      super.registerAttributes();

       /* default_user_oid - The default user for an organization.  The use of this association differs
         from one organization to another. */
       registerAttribute("default_user_oid", "a_default_user_oid", "NumberAttribute");
   }
   
   




}
