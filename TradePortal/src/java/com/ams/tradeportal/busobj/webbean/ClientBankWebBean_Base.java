

  

package com.ams.tradeportal.busobj.webbean;


/**
 * A client bank that has chosen to use the Trade Portal for their customers'
 * trade processing.   
 * 
 * Each bank has a separate instrument number (ID) range.  When instruments
 * are created, this range is used to determine the valid range of instrument
 * numbers.
 * 
 * Information about instrument ID ranges, security, branding, and default
 * templates are stored in a ClientBank record.
 *
 *     Copyright  © 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class ClientBankWebBean_Base extends ReferenceDataOwnerWebBean
{

  public void registerAttributes()
   {  
      super.registerAttributes();


       /* instr_id_range_start - The instrument number at the start of the Instrument ID range for this client
         bank. */
       registerAttribute("instr_id_range_start", "instr_id_range_start", "NumberAttribute");


       /* instr_id_range_end - The instrument number at the end of the Instrument ID range for this client
         bank. */
       registerAttribute("instr_id_range_end", "instr_id_range_end", "NumberAttribute");


       /* branding_directory - The branding directory used when users owned by this ClientBank access the
         Trade Portal.  When bank organization groups are created under the ClientBank,
         the BOG's branding directory defaults to the client bank's branding directory */
       registerAttribute("branding_directory", "branding_directory");


       /* bank_min_password_len - The minimum password length for users owned by this ClientBank and users
         owned by BankOrganizationGroups that are under this ClientBank. */
       registerAttribute("bank_min_password_len", "bank_min_password_len", "NumberAttribute");


       /* bank_change_password_days - The number of days after which users must change their password.   This
         applys to users owned by this ClientBank and users owned by BankOrganizationGroups
         under this ClientBank. */
       registerAttribute("bank_change_password_days", "bank_change_password_days", "NumberAttribute");


       /* bank_max_failed_attempts - The number of unsuccesful logon attempts that a user can have within their
         lifetime before they are locked out.   This applys to users owned by this
         ClientBank and users owned by BankOrganizationGroups under this ClientBank. */
       registerAttribute("bank_max_failed_attempts", "bank_max_failed_attempts", "NumberAttribute");


       /* corp_min_password_length - The minimum password length for users owned  by CorporateOrganizations that
         are under this ClientBank. */
       registerAttribute("corp_min_password_length", "corp_min_password_length", "NumberAttribute");


       /* corp_change_password_days - The number of days after which users must change their password.   This
         applys to users owned by CorporateOrganizations that are under this ClientBank.. */
       registerAttribute("corp_change_password_days", "corp_change_password_days", "NumberAttribute");


       /* corp_max_failed_attempts - The number of unsuccesful logon attempts that a user can have within their
         lifetime before they are locked out.    This applys to users owned by CorporateOrganizations
         that are under this ClientBank.. */
       registerAttribute("corp_max_failed_attempts", "corp_max_failed_attempts", "NumberAttribute");


       /* fax_1 - First fax number of this Client Bank. */
       registerAttribute("fax_1", "fax_1");


       /* telex_1 - Telex number of this ClientBank */
       registerAttribute("telex_1", "telex_1");


       /* telex_answer_back_1 - Telex Answer Back number of this Client Bank */
       registerAttribute("telex_answer_back_1", "telex_answer_back_1");


       /* fax_2 - Second fax number for this ClientBank */
       registerAttribute("fax_2", "fax_2");


       /* swift_address_part1 - Part 1 of the Swift Address of this ClientBank */
       registerAttribute("swift_address_part1", "swift_address_part1");


       /* swift_address_part2 - Part 2 of the SWIFT address of this ClientBank */
       registerAttribute("swift_address_part2", "swift_address_part2");


       /* phone_number - Phone number of the client bank */
       registerAttribute("phone_number", "phone_number");


       /* address_city - City part of the bank's address */
       registerAttribute("address_city", "address_city");


       /* address_country - Code of the country of the bank's address */
       registerAttribute("address_country", "address_country");


       /* address_line_1 - Line 1 of the bank's address */
       registerAttribute("address_line_1", "address_line_1");


       /* address_line_2 - Line 2 of the bank's address */
       registerAttribute("address_line_2", "address_line_2");


       /* address_postal_code - Postal code of the bank's address */
       registerAttribute("address_postal_code", "address_postal_code");


       /* address_state_province - State or province for the bank's address */
       registerAttribute("address_state_province", "address_state_province");


       /* OTL_id - A set of letters that is used to identify the bank.  These letters will
         be same as those used on the OTL back end.   This ID is used to match up
         the bank record on the portal with the bank record on OTL */
       registerAttribute("OTL_id", "OTL_id");


       /* guar_def_tmplt_oid - OID on the Template table for the Guarantee Default Template.
         
         Default templates are used frequently, and having an association on client
         bank to the default template provides quick access.   If this association
         weren't here, a more complex database query would be required to determine
         the default template for the bank.
         
         This is an attribute and not a real jPylon association because the default
         tempalte is created at the same time as the update to the ClientBank entry.
         In that situation, the association check would always fail. */
       registerAttribute("guar_def_tmplt_oid", "guar_def_tmplt_oid", "NumberAttribute");


       /* export_coll_def_tmplt_oid - OID on the Template table for the Direct Send Collection Default Template.
         (For the proper Export Collection in TPS perspective, see new_export_coll_def_tmplt_oid).
         
         Default templates are used frequently, and having an association on client
         bank to the default template provides quick access.   If this association
         weren't here, a more complex database query would be required to determine
         the default template for the bank.
         
         This is an attribute and not a real jPylon association because the default
         tempalte is created at the same time as the update to the ClientBank entry.
         In that situation, the association check would always fail. */
       registerAttribute("export_coll_def_tmplt_oid", "export_coll_def_tmplt_oid", "NumberAttribute");


       /* shipping_guar_def_tmplt_oid - OID on the Template table for the Shipping Guarantee Default Template.
         
         Default templates are used frequently, and having an association on client
         bank to the default template provides quick access.   If this association
         weren't here, a more complex database query would be required to determine
         the default template for the bank.
         
         This is an attribute and not a real jPylon association because the default
         tempalte is created at the same time as the update to the ClientBank entry.
         In that situation, the association check would always fail. */
       registerAttribute("shipping_guar_def_tmplt_oid", "shipping_guar_def_tmplt_oid", "NumberAttribute");


       /* air_waybill_def_tmplt_oid - OID on the Template table for the Air Waybill Default Template.
         
         Default templates are used frequently, and having an association on client
         bank to the default template provides quick access.   If this association
         weren't here, a more complex database query would be required to determine
         the default template for the bank.
         
         This is an attribute and not a real jPylon association because the default
         tempalte is created at the same time as the update to the ClientBank entry.
         In that situation, the association check would always fail. */
       registerAttribute("air_waybill_def_tmplt_oid", "air_waybill_def_tmplt_oid", "NumberAttribute");


       /* standby_lc_def_tmplt_oid - OID on the Template table for the Standby Default Template.
         
         Default templates are used frequently, and having an association on client
         bank to the default template provides quick access.   If this association
         weren't here, a more complex database query would be required to determine
         the default template for the bank.
         
         This is an attribute and not a real jPylon association because the default
         tempalte is created at the same time as the update to the ClientBank entry.
         In that situation, the association check would always fail. */
       registerAttribute("standby_lc_def_tmplt_oid", "standby_lc_def_tmplt_oid", "NumberAttribute");


       /* loan_req_def_tmplt_oid - OID on the Template table for the Loan Request Default Template.
         
         Default templates are used frequently, and having an association on client
         bank to the default template provides quick access.   If this association
         weren't here, a more complex database query would be required to determine
         the default template for the bank.
         
         This is an attribute and not a real jPylon association because the default
         tempalte is created at the same time as the update to the ClientBank entry.
         In that situation, the association check would always fail. */
       registerAttribute("loan_req_def_tmplt_oid", "loan_req_def_tmplt_oid", "NumberAttribute");


       /* funds_transfer_def_tmplt_oid - OID on the Template table for the Funds Transfer Default Template.
         
         Default templates are used frequently, and having an association on client
         bank to the default template provides quick access.   If this association
         weren't here, a more complex database query would be required to determine
         the default template for the bank.
         
         This is an attribute and not a real jPylon association because the default
         tempalte is created at the same time as the update to the ClientBank entry.
         In that situation, the association check would always fail. */
       registerAttribute("funds_transfer_def_tmplt_oid", "funds_transfer_def_tmplt_oid", "NumberAttribute");


       /* import_lc_def_tmplt_oid - OID on the Template table for the Import LC Default Template.
         
         Default templates are used frequently, and having an association on client
         bank to the default template provides quick access.   If this association
         weren't here, a more complex database query would be required to determine
         the default template for the bank.
         
         This is an attribute and not a real jPylon association because the default
         tempalte is created at the same time as the update to the ClientBank entry.
         In that situation, the association check would always fail. */
       registerAttribute("import_lc_def_tmplt_oid", "import_lc_def_tmplt_oid", "NumberAttribute");


       /* authentication_method - Determines how users of the client bank and its bank groups will authenticate
         themselves. */
       registerAttribute("authentication_method", "authentication_method");


       /* verify_logon_digital_sig -  */
       registerAttribute("verify_logon_digital_sig", "verify_logon_digital_sig", "IndicatorAttribute");


       /* bank_password_addl_criteria - Indicates that admin users of this client bank must use passwords that meet
         additional criteria. */
       registerAttribute("bank_password_addl_criteria", "bank_password_addl_criteria", "IndicatorAttribute");


       /* corp_password_addl_criteria - Indicates that corporate customer users of this client bank must use passwords
         that meet additional criteria. */
       registerAttribute("corp_password_addl_criteria", "corp_password_addl_criteria", "IndicatorAttribute");


       /* bank_password_history - Indicates that when admin users of this client bank change their password,
         the password will be validated against there last few passwords. */
       registerAttribute("bank_password_history", "bank_password_history", "IndicatorAttribute");


       /* corp_password_history - Indicates that when corporate customer users of this client bank change
         their password, the password will be validated against there last few passwords. */
       registerAttribute("corp_password_history", "corp_password_history", "IndicatorAttribute");


       /* bank_password_history_count - For admin users of this client bank, the number of previous passwords against
         which the user's new password will be validated. */
       registerAttribute("bank_password_history_count", "bank_password_history_count", "NumberAttribute");


       /* corp_password_history_count - For corporate users of this client bank, the number of previous passwords
         against which the user's new password will be validated. */
       registerAttribute("corp_password_history_count", "corp_password_history_count", "NumberAttribute");


       /* instrument_purge_type - Indicates whether or not instruments in the portal will be purged when instruments
         are purged in OTL */
       registerAttribute("instrument_purge_type", "instrument_purge_type");


       /* brand_button_ind - A setting indicating whether the client bank makes all the buttons branded.
         
         Yes - The client bank uses the branded buttons stored in the branding directory.
         No (default) - The client bank uses the standard buttons. */
       registerAttribute("brand_button_ind", "brand_button_ind", "IndicatorAttribute");


       /* override_swift_length_ind - A setting indicating whether the client bank allows more characters than
         the SWIFT recommended length.
         
         Y - Allow more characters than SWIFT recommended field size limitations.
         N(default) - Conform to SWIFT recommended field size limitations. */
       registerAttribute("override_swift_length_ind", "override_swift_length_ind", "IndicatorAttribute");


       /* request_advise_def_tmplt_oid - OID on the Template table for the Request Advise Default Template.
         
         Default templates are used frequently, and having an association on client
         bank to the default template provides quick access.   If this association
         weren't here, a more complex database query would be required to determine
         the default template for the bank.
         
         This is an attribute and not a real jPylon association because the default
         tempalte is created at the same time as the update to the ClientBank entry.
         In that situation, the association check would always fail. */
       registerAttribute("request_advise_def_tmplt_oid", "request_advise_def_tmplt_oid", "NumberAttribute");


       /* approval_to_pay_def_tmplt_oid - OID on the Template table for the Approval To Pay Default Template.
         
         Default templates are used frequently, and having an association on client
         bank to the default template provides quick access.   If this association
         weren't here, a more complex database query would be required to determine
         the default template for the bank.
         
         This is an attribute and not a real jPylon association because the default
         tempalte is created at the same time as the update to the ClientBank entry.
         In that situation, the association check would always fail. */
       registerAttribute("approval_to_pay_def_tmplt_oid", "approval_to_pay_def_tmplt_oid", "NumberAttribute");


       /* corp_auth_method - Determines how users of the corporate customers that belong to client bank
         will authenticate themselves. */
       registerAttribute("corp_auth_method", "corp_auth_method");


       /* doc_prep_url - URL of Corporate Service (used to be called doc prep service) for the client
         bank.  Could be overridden for each corporate customer. */
       registerAttribute("doc_prep_url", "doc_prep_url");


       /* xfer_bet_accts_def_tmplt_oid - OID on the Template table for the Transfer Between Accounts Default Template.
         
         Default templates are used frequently, and having an association on client
         bank to the default template provides quick access.   If this association
         weren't here, a more complex database query would be required to determine
         the default template for the bank.
         
         This is an attribute and not a real jPylon association because the default
         tempalte is created at the same time as the update to the ClientBank entry.
         In that situation, the association check would always fail. */
       registerAttribute("xfer_bet_accts_def_tmplt_oid", "xfer_bet_accts_def_tmplt_oid", "NumberAttribute");


       /* domestic_payment_def_tmplt_oid - OID on the Template table for the Domestic Payment Default Template.
         
         Default templates are used frequently, and having an association on client
         bank to the default template provides quick access.   If this association
         weren't here, a more complex database query would be required to determine
         the default template for the bank.
         
         This is an attribute and not a real jPylon association because the default
         tempalte is created at the same time as the update to the ClientBank entry.
         In that situation, the association check would always fail. */
       registerAttribute("domestic_payment_def_tmplt_oid", "domestic_payment_def_tmplt_oid", "NumberAttribute");


       /* authorization_cert_auth_URL - URL of the Smart Card/Certificate Authentication used to present a certificate
         authentication window to the user during Funds authorization. */
       registerAttribute("authorization_cert_auth_URL", "authorization_cert_auth_URL");


       /* can_edit_own_profile_ind - Whether the users can edit their own User Profile, Security Pofile and Threshold
         Group. */
       registerAttribute("can_edit_own_profile_ind", "can_edit_own_profile_ind", "IndicatorAttribute");


       /* DDI_def_tmplt_oid - OID on the Template table for the Direct Debit Default Template.
         
         Default templates are used frequently, and having an association on client
         bank to the default template provides quick access.   If this association
         weren't here, a more complex database query would be required to determine
         the default template for the bank.
         
         This is an attribute and not a real jPylon association because the default
         tempalte is created at the same time as the update to the ClientBank entry.
         In that situation, the association check would always fail. */
       registerAttribute("DDI_def_tmplt_oid", "DDI_def_tmplt_oid", "NumberAttribute");


       /* new_export_coll_def_tmplt_oid - OID on the Template table for the Export Collection Default Template.  (Note
         export_coll_def_tmplt_oid is only for Direct Send Collection).
         
         Default templates are used frequently, and having an association on client
         bank to the default template provides quick access.   If this association
         weren't here, a more complex database query would be required to determine
         the default template for the bank.
         
         This is an attribute and not a real jPylon association because the default
         tempalte is created at the same time as the update to the ClientBank entry.
         In that situation, the association check would always fail. */
       registerAttribute("new_export_coll_def_tmplt_oid", "new_export_coll_def_tmplt_oid", "NumberAttribute");


       /* can_edit_id_ind - Whether to allow Corporate Users to view/edit the Certificate ID and Security
         Device ID fields on Corporate User Profile. */
       registerAttribute("can_edit_id_ind", "can_edit_id_ind", "IndicatorAttribute");


       /* suppress_doc_link_ind - Suppress PDF Document link during transaction processing.  Only after the
         transaction is successfully released in the TPS after a successful Denied
         Party Compliance check has been executed, and the status of the transaction
         in the Portal is updated to "Processed by Bank", will the links for the
         Collection/Amendment Schedule and Bill of Exchange document be made available
         in the Portal as part of the update process of the transaction. */
       registerAttribute("suppress_doc_link_ind", "suppress_doc_link_ind", "IndicatorAttribute");


       /* template_groups_ind - Whether the client bank uses template groups */
       registerAttribute("template_groups_ind", "template_groups_ind", "IndicatorAttribute");

       // Suresh CR-603 03/09/2011 Begin
       /* report_categories_ind - Whether the client bank uses report categories */
       registerAttribute("report_categories_ind", "report_categories_ind", "IndicatorAttribute");
       // Suresh CR-603 03/09/2011 End 

       /* require_tran_auth - Contains the Requirement of Transaction Authorization for the client bank.
         The value stored in this field will be the string representation of a number.
         This number can be converted into a string of 1s and 0s.  Each of these
         bits (a 1 or 0) represents whether an instrument/transaction type requires
         certificate authentication or 2-Factor Token authentication.
 */
       registerAttribute("require_tran_auth", "require_tran_auth");


       /* bank_logon_tolerance_minutes - The tolerance minutes for the certificate of a Client Bank user who use
         the authentication method AES256 (i.e. Single Sign-on for CMA).  The certificate
         expires after this many minutes. */
       registerAttribute("bank_logon_tolerance_minutes", "bank_logon_tolerance_minutes", "NumberAttribute");


       /* corp_logon_tolerance_minutes - The tolerance minutes for the certificate of a Corporate user who use the
         authentication method AES256 (i.e. Single Sign-on for CMA).  The certificate
         expires after this many minutes. */
       registerAttribute("corp_logon_tolerance_minutes", "corp_logon_tolerance_minutes", "NumberAttribute");


       /* encryption_salt - Encryption Salt for the certificates used in AES256 log-in (i.e. single
         sign-on for CMA). */
       registerAttribute("encryption_salt", "encryption_salt");


       //BSL 09/07/11 CR663 Rel 7.1 Begin
       /* registration_header - Registration header */
       registerAttribute("registration_header", "registration_header");


       /* registration_text - Registration announcement text */
       registerAttribute("registration_text", "registration_text");
       //BSL 09/07/11 CR663 Rel 7.1 End


       /* allow_pay_by_another_accnt - This allows for more than one customer to be associated by one account.
         This option allows a corporate customer to process payment instruments using
         another corporate customerís account(s).  When this is set to Y, then the
         other Corporate Customers' Accounts will be available on the Corporate Customer
         Profile page. */
       registerAttribute("allow_pay_by_another_accnt", "allow_pay_by_another_accnt", "IndicatorAttribute");

       /* InstrumentNumberSequence - The instrument number sequence counter that is used to generate instrument
         numbers for the client bank. */
       registerAttribute("c_InstrumentNumberSequence", "c_INSTR_NUMBER_SEQ", "NumberAttribute");

      /* Pointer to the parent GlobalOrganization */
       registerAttribute("global_organization_oid", "p_global_organization_oid", "ParentIDAttribute");
       
       /* override_swift_validation_ind - A setting indicating whether the client bank allows characters other than
       the SWIFT valid characters
       
       Y - Allow characters other than SWIFT valid characters.
       N(default) - Conform to SWIFT valid characters. */
       registerAttribute("override_swift_validation_ind", "override_swift_validation_ind", "IndicatorAttribute");
       
       
       // CR ANZ 501 Rel 8.3 03-24-2013 jgadela  Begin
       /* corp_org_dual_ctrl_reqd_ind - Indicates whether Corporate Organization reference data requires dual control
       for maintenance. */
       registerAttribute("corp_org_dual_ctrl_reqd_ind", "corp_org_dual_ctrl_reqd_ind", "IndicatorAttribute");

       /* admin_user_dual_ctrl_reqd_ind - Indicates whether Admin user reference data requires dual control for maintenance. */
       registerAttribute("admin_user_dual_ctrl_reqd_ind", "admin_user_dual_ctrl_reqd_ind", "IndicatorAttribute");
       /* corp_user_dual_ctrl_reqd_ind - Indicates whether Corporate user reference data requires dual control for maintenance. */
       registerAttribute("corp_user_dual_ctrl_reqd_ind", "corp_user_dual_ctrl_reqd_ind", "IndicatorAttribute");

	    // CR ANZ 501 Rel 8.3 03-24-2013 jgadela  end

        // DK CR-640 Rel7.1 BEGINS
		/* allow_live_market_rates_req - This allows ClientBank to get live market rate from the bank's backend system.
         When this is set to Y, then the FXOnline setting on BankOrgGroup page and
         FXOnline AccountID on the Corporate Customer Profile page will be availbale. */
		registerAttribute("allow_live_market_rates_req", "allow_live_market_rates_req",
				"IndicatorAttribute");
		// DK CR-640 Rel7.1 ENDS
		
		 //IValavala CR 742. Add new attributes to client bank
	     registerAttribute("image_service_endpoint", "image_svc_endpoint");
	      
	     registerAttribute("image_service_userid", "image_svc_userid");
	      
	     registerAttribute("image_service_password", "image_svc_pword");
	     
	     //MEerupula Rel8.4 CR-934a Add mobile banking indicator to enable web services for Client Banks(via mobile platform)
	     registerAttribute("mobile_banking_access_ind", "mobile_banking_access_ind", "IndicatorAttribute");
	       
	    // jgadela - CR 854 Rel 8.4 T36000024797 03-11-2014 - Reporting language option indicator
	     /* reporting_lang_option_ind - Used to turn on/off the reporting languange option */
	     registerAttribute("reporting_lang_option_ind", "reporting_lang_option_ind", "IndicatorAttribute");
	     
	     registerAttribute("pre_debit_funding_option_ind", "pre_debit_funding_option_ind", "IndicatorAttribute");

	    //SSikhakolli Rel 9.3.5 CR-1029 Adding new attribute enable_admin_update_centre
	     registerAttribute("enable_admin_update_centre", "enable_admin_update_centre", "IndicatorAttribute");
	     
	     //Nar Rel 9.5.0.0 CR-1132 Adding new attribute utilize_additional_bank_fields
	     registerAttribute("utilize_additional_bank_fields", "utilize_additional_bank_fields", "IndicatorAttribute");
	     
	     //Rel 9.5.0.0 CR-927B Adding new attribute control_NotifRule_byAdmin
	     registerAttribute("control_NotifRule_byAdmin", "control_NotifRule_byAdmin", "IndicatorAttribute");
	     
	     //Rel 9.5.0.0 CR-927B Adding new attribute allow_filtering_of_instruments 
	     registerAttribute("allow_filtering_of_instruments", "allow_filtering_of_instruments", "IndicatorAttribute");
	      
		registerAttribute("allow_only_iso8859_ind", "allow_only_iso8859_ind", "IndicatorAttribute");
   }
}
