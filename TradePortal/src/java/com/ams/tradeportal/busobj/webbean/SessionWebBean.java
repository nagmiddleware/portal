package com.ams.tradeportal.busobj.webbean;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import javax.crypto.SecretKey;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.busobj.util.FavoriteTaskObject;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.util.JPylonProperties;
import com.amsinc.ecsg.util.StringFunction;

/*
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class SessionWebBean implements Cloneable, java.io.Serializable {
	private static final Logger LOG = LoggerFactory.getLogger(SessionWebBean.class);
	// cquinton 4/24/2012 change global navigation name
	private String currentPrimaryNavigation = null;
	private String baseCurrencyCode = null;
	private String organizationName = null;
	private String brandingDirectory = null;
	private String clientBankOid = null;
	private String globalOrganizationOid = null;
	private String bogOid = null;
	private String loginOrganizationId = null;
	private String reportingUserSuffix = null;
	private boolean passwordValidationFlag = false;
	// cquinton 12/20/2012 anz security issue#341
	// changePassword is set at login, if true, the Header.jsp checks to see if the user should be forwarded to the change password page
	private boolean forceChangePassword = false;
	private String changePasswordReason = null; // only set if forceChangePassword true
	private String clientBankCode = null;
	private String timeoutAutoSaveInd = TradePortalConstants.INDICATOR_NO;
	// CR-433 Krishna Begin 05/22/2008
	private String docPrepURL = null;
	// CR-433 Krishna End 05/22/2008

	// attributes from user
	private String customerAccessSecurityProfileOid = null;
	private String orgAutoLCCreateIndicator = null;
	private String orgManualPOIndicator = null;
	private String customerAccessIndicator = null;
	private String ownerOrgOid = null;
	private String ownershipLevel = null;
	private String timeZone = null;
	/* pgedupudi - Rel-9.3 CR-976A 03/26/2015 */
	private String bankGrpRestrictRuleOid = null;
	private String datePattern = null;
	private Boolean showToolTips = null; // user settings - to turn off or on the toolTips

	private String userLocale = null;
	private String userOid = null;
	private String userFullName = null;
	private String defaultWipView = null;
	private String userId = null;
	private boolean brandButtonInd = false;
	private boolean useMultiPartMode = false;
	private boolean useDisplayExtendedListview = false;
	private boolean reportLoginInd = false;
	private boolean corpCanAccessDocPrep = false;
	private boolean userCanAccessDocPrep = false;
	// BSL CR 749 05/16/2012 Rel 8.0 BEGIN
	private boolean usingSiteMinderWebAgentSSO = false;
	private boolean usingMaxSessionTimeout = false;
	private Date sessionStartDate = null;
	// BSL CR 749 05/16/2012 Rel 8.0 END
	private String canSeeOwnDataUsingSubAccess = TradePortalConstants.INDICATOR_NO;

	private String orgAutoATPCreateIndicator = null;
	private String orgATPManualPOIndicator = null;
	private String orgATPInvoiceIndicator = null;// SHR CR708 Rel8.1.1.0

	// Copy of session data
	private SessionWebBean savedUserSession = null;

	// attributes from security_profile
	private String securityRights = null;
	private String securityType = null;

	private String liveMarketRateInd = TradePortalConstants.INDICATOR_NO; // -- CR-640/581 Rel 7.1.0 09/22/11 -

	// cquinton 2/16/2012 Rel 8.0 ppx255 start
	private String activeUserSessionOid = null;
	// cquinton 2/16/2012 Rel 8.0 ppx255 end

	private boolean instrumentLock = false; 

	private List<FavoriteTaskObject> favoriteTasks = null;
	private List<FavoriteTaskObject> favoriteReports = null;

	// IR 16481
	private HashMap savedSearchQueryData = new HashMap();
	private String savedSearchCriteriaSection = "";
	private boolean savedQuickLinkValue = true;
	private boolean savedSectionValue = true;
	// jgadela Rel 8.4 CR-854 [START]- Adding reporting language option
	private String reportingLanguage = null;
	// vdesingu Rel8.4 CR-590
	boolean isAcctBalancesRefreshed = false;

	// MEerupula Rel 8.4 IR-23915- Adding confidential indicator
	private String confInd = null;
	private String subsidConfInd = null;

	// T36000013446- use custom http header to communicate CMA specific status codes - start
	private String authMethod = null;
	private String ssoId = null;
	// T36000013446- use custom http header to communicate CMA specific status codes - end

	// MEerupula Rel 9.4 CR-1026- Adding cust_not-intg_with_TPS value to session.
	private boolean custNotIntgTPS = false;
	private boolean enableAdminUpdateInd = false;

	// Tang Rel9.3.5 CR#1032 Session Synch -[Begin]
	private String sessionSynchImg = null;
	// Tang Rel9.3.5 CR#1032 Session Synch -[End]

	public String getReportingLanguage() {
		return (reportingLanguage == null || "".equals(reportingLanguage)) ? "en_CA" : reportingLanguage;
	}

	public void setReportingLanguage(String reportingLanguage) {
		this.reportingLanguage = reportingLanguage;
	}
	// jgadela Rel 8.4 CR-854 [START]- Adding reporting language option

	public boolean getSavedSectionValue() {
		return savedSectionValue;
	}

	public void setSavedSectionValue(boolean savedSectionValue) {
		this.savedSectionValue = savedSectionValue;
	}

	public String getSavedSearchCriteriaSection() {
		return savedSearchCriteriaSection;
	}

	public void setSavedSearchCriteriaSection(String savedSearchCriteriaSection) {
		this.savedSearchCriteriaSection = savedSearchCriteriaSection;
	}

	public boolean getSavedQuickLinkValue() {
		return savedQuickLinkValue;
	}

	public void setSavedQuickLinkValue(boolean savedQuickLinkValue) {
		this.savedQuickLinkValue = savedQuickLinkValue;
	}

	public Map getSavedSearchQueryData() {
		return savedSearchQueryData;
	}

	public void setSavedSearchQueryData(String viewName, Map savedSearchQueryData) {
		this.savedSearchQueryData.put(viewName, savedSearchQueryData);
	}

	public void clearSavedSearchQueryData() {
		savedSearchQueryData.clear();
	}
	// IR 16481 end

	// cquinton 12/12/12 use a Queue for recent instruments
	// as it frequently requires modifying both end of the queue.
	// note that ArrayDeque only exists in java 1.6 and would be ideal if possible
	public class RecentInstrumentRef {
		// this is actually transaction based :.)
		private String transactionOid;
		private String instrumentId;
		private String transactionType;
		private String transactionStatus;

		public RecentInstrumentRef(String oid, String id, String tType, String status) {
			transactionOid = oid;
			instrumentId = id;
			transactionType = tType;
			transactionStatus = status;
		}

		public String getTransactionOid() {
			return transactionOid;
		}

		public String getInstrumentId() {
			return instrumentId;
		}

		public String getTransactionType() {
			return transactionType;
		}

		public String getTransactionStatus() {
			return transactionStatus;
		}
	}

	private Queue<RecentInstrumentRef> recentInstrumentsQueue = new ConcurrentLinkedQueue<>();
	private static final int MAX_RECENT_INSTRUMENTS = 5;

	private String auth2FASubType = null;

	// W Zhu 8/17/2012 Rel 8.1 T36000004579
	// The secret key (Triple-DES) used to encrypt sensitive text (such as oid) on the web page.
	// This key is generated randomly for each user session.
	private SecretKey secretKey;

	// cquinton 1/18/2013 add page flow logic
	// todo: info about pages that return pages is ideally specified in navigation metadata
	// use a linked list to keep track of pages the user has visited
	// we use linked list because its a list and we need to continually add/remove items
	public class PageFlowParameter {
		private String name;
		private String value;

		public PageFlowParameter(String name, String value) {
			this.name = name;
			this.value = value;
		}

		public String getName() {
			return name;
		}

		public String getValue() {
			return value;
		}
	}

	public class PageFlowRef {
		private String linkAction;
		private PageFlowParameter[] linkParameters = null;

		public PageFlowRef(String linkAction) {
			this.linkAction = linkAction;
		}

		public PageFlowRef(String linkAction, PageFlowParameter linkParameter) {
			this.linkAction = linkAction;
			if (linkParameter != null) {
				linkParameters = new PageFlowParameter[1];
				linkParameters[0] = linkParameter;
			}
		}

		public PageFlowRef(String linkAction, PageFlowParameter[] linkParameter) {
			this.linkAction = linkAction;
			linkParameters = linkParameter; // ok if null
		}

		public String getLinkAction() {
			return linkAction;
		}

		public PageFlowParameter[] getLinkParameters() {
			return linkParameters;
		}

		public String getLinkParametersString() {
			StringBuilder str = new StringBuilder();
			if (linkParameters != null) {
				for (int i = 0; i < linkParameters.length; i++) {
					PageFlowParameter p = linkParameters[i];
					if (p != null) {
						str.append("&").append(p.name).append("=").append(p.value);
					}
				}
			}
			return str.toString();
		}

		public String getParameterValue(String name) {
			String value = null;
			if (linkParameters != null && name != null) {
				for (int i = 0; i < linkParameters.length; i++) {
					PageFlowParameter p = linkParameters[i];
					if (p != null) {
						if (name.equals(p.getName())) {
							value = p.getValue();
							break;
						}
					}
				}
			}
			return value;
		}
	}

	private LinkedList<PageFlowRef> pageFlow = new LinkedList<>();
	private static final int DEFAULT_MAX_PAGE_FLOW_SIZE = 5;
	private int maxPageFlowSize = DEFAULT_MAX_PAGE_FLOW_SIZE;
   
   private static final String DOJO_JS_PATH = "/portal/js";
   private String dojoJsPath = DOJO_JS_PATH;
   
	private PageFlowRef currentPage = null; // keeps track of current page

	// CR 821 Rel 8.3 START
	private String lastLoginTimestamp = null;

	public String getLastLoginTimestamp() {
		return lastLoginTimestamp;
	}

	public void setLastLoginTimestamp(String lastLoginTimestamp) {
		this.lastLoginTimestamp = lastLoginTimestamp;
	}

	// CR 821 Rel 8.3 END
	private String serverName = null;

	public String getServerName() {
		return serverName;
	}

	public void setServerName(String serverName) {
		this.serverName = serverName;
	}

	private String isSLAMonitoringOn = null;

	public String getIsSLAMonitoringOn() {
		return isSLAMonitoringOn;
	}

	public void setIsSLAMonitoringOn(String isSLAMonitoringOn) {
		this.isSLAMonitoringOn = isSLAMonitoringOn;
	}

	public SessionWebBean() {
		super();

		// cquinton 1/18/2013 adding maxPageFlowSize jPylon property
		try {
			String tmp = JPylonProperties.getInstance().getString("maxPageFlowSize");
			if (tmp != null && tmp.length() > 0) {
				tmp = tmp.trim();
				this.maxPageFlowSize = Integer.parseInt(tmp);
			} else {
				LOG.info("jPylon maxPageFlowSize property not specified. NavigationManager maxPageFlowSize defaulting to {}", DEFAULT_MAX_PAGE_FLOW_SIZE);
				this.maxPageFlowSize = DEFAULT_MAX_PAGE_FLOW_SIZE;
			}
		} catch (Exception ex) {
			LOG.info("Problem reading jPylon maxPageFlowSize property. NavigationManager maxPageFlowSize defaulting to {}", DEFAULT_MAX_PAGE_FLOW_SIZE);
			this.maxPageFlowSize = DEFAULT_MAX_PAGE_FLOW_SIZE;
		}


	}

	/**
	 * Getter for dojoJsPath
	 * @return String
	*/
	public String getdojoJsPath(){
		return dojoJsPath;
	}

   /**
	* Getter for the user organization's branding directory
	* @return String
	*/
	public String getBrandingDirectory() {
		return brandingDirectory;
	}

	/**
	 * Setter for the branding directory attribute.
	 * 
	 * @param newBrandingDirectory
	 *            String
	 */
	public void setBrandingDirectory(String newBrandingDirectory) {
		brandingDirectory = newBrandingDirectory;
	}

	/**
	 * Getter for the button branding flag
	 * 
	 * @return report flag
	 */
	public boolean getBrandButtonInd() {
		return brandButtonInd;
	}

	/**
	 * Setter for the button branding flag
	 * 
	 * @param value
	 *            String
	 */
	public void setBrandButtonInd(boolean value) {
		brandButtonInd = value;
	}

	/**
	 * Setter for the subsidiary access reference data
	 * 
	 * @param canSeeOwnDataUsingSubAccess
	 *            String
	 */
	public void setCanSeeOwnDataUsingSubAccess(String canSeeOwnDataUsingSubAccess) {
		this.canSeeOwnDataUsingSubAccess = canSeeOwnDataUsingSubAccess;
	}

	/**
	 * Getter for the flag indicating if when the user logged in they did it with a password
	 * 
	 * @return flag
	 */
	public boolean getPasswordValidationFlag() {
		return passwordValidationFlag;
	}

	/**
	 * Setter for the flag indicating if when the user logged in they did it with a password
	 * 
	 * @param value
	 *            String
	 */
	public void setPasswordValidationFlag(boolean value) {
		passwordValidationFlag = value;
	}

	/**
	 * Getter for the flag indicating if user must change password
	 * 
	 * @return flag
	 */
	public boolean getForceChangePassword() {
		return forceChangePassword;
	}

	/**
	 * Setter for the flag indicating if when the user logged in they did it with a password
	 * 
	 * @param value
	 *            String
	 */
	public void setForceChangePassword(boolean value) {
		forceChangePassword = value;
	}

	/**
	 * Getter change password reason
	 * 
	 * @return flag
	 */
	public String getChangePasswordReason() {
		return changePasswordReason;
	}

	/**
	 * Setter for change password reason
	 * 
	 * @param value
	 *            String
	 */
	public void setChangePasswordReason(String value) {
		changePasswordReason = value;
	}

	/**
	 * Getter for the client bank code
	 * 
	 * @return client bank code
	 */
	public String getClientBankCode() {
		return clientBankCode;
	}

	/**
	 * Setter for the client bank code
	 * 
	 * @param value
	 *            String
	 */
	public void setClientBankCode(String value) {
		clientBankCode = value;
	}

	/**
	 * Getter for the multi part mode flag
	 * 
	 * @return multi-part mode flag
	 */
	public boolean getMultiPartMode() {
		return useMultiPartMode;
	}

	/**
	 * Setter for the multi part mode flag
	 * 
	 * @param value
	 *            String
	 */
	public void setMultiPartMode(boolean value) {
		useMultiPartMode = value;
	}

	/**
	 * Getter for the display extended listview flag
	 * 
	 * @return display extended listview flag
	 */
	public boolean getDisplayExtendedListview() {
		return useDisplayExtendedListview;
	}

	/**
	 * Setter for the display extended listview flag
	 * 
	 * @param value
	 *            boolean
	 */
	public void setDisplayExtendedListview(boolean value) {
		useDisplayExtendedListview = value;
	}

	/**
	 * Getter for the report flag
	 * 
	 * @return report flag
	 */
	public boolean getReportLoginInd() {
		return reportLoginInd;
	}

	/**
	 * Setter for the report flag
	 * 
	 * @param value
	 *            String
	 */
	public void setReportLoginInd(boolean value) {
		reportLoginInd = value;
	}

	/**
	 * Getter for the reporting user suffix
	 * 
	 * @return reporting user suffix
	 */
	public String getReportingUserSuffix() {
		return reportingUserSuffix;
	}

	/**
	 * Setter for the reporting user suffix
	 * 
	 * @param value
	 *            String
	 */
	public void setReportingUserSuffix(String value) {
		reportingUserSuffix = value;
	}

	// cquinton 4/24/2012 portal refresh start
	// rename global navigation
	/**
	 * Setter for current primary navigation.
	 * 
	 * @param x
	 *            String
	 */
	public void setCurrentPrimaryNavigation(String x) {
		currentPrimaryNavigation = x;
	}

	/**
	 * Getter for current primary navigation.
	 * 
	 * @return String
	 */
	public String getCurrentPrimaryNavigation() {
		return currentPrimaryNavigation;
	}
	// cquinton 4/24/2012 portal refresh end

	/**
	 * Setter for the global org oid attribute.
	 * 
	 * @param newGlobalOrgOid
	 *            String
	 */
	public void setGlobalOrgOid(String newGlobalOrgOid) {
		globalOrganizationOid = newGlobalOrgOid;
	}

	/**
	 * Setter for the client bank oid attribute.
	 * 
	 * @param newClientBankOid
	 *            String
	 */
	public void setClientBankOid(String newClientBankOid) {
		clientBankOid = newClientBankOid;
	}

	/**
	 * Getter for the global organization oid of the client bank for the user
	 * 
	 * @return String
	 */
	public String getGlobalOrgOid() {
		return globalOrganizationOid;
	}

	/**
	 * Getter for the client bank oid of the user's organization (in the case of client bank organizations, BOG organizations, and
	 * corporate organizations).
	 * 
	 * @return String
	 */
	public String getClientBankOid() {
		return clientBankOid;
	}

	public String canSeeOwnDataUsingSubAccess() {
		return this.canSeeOwnDataUsingSubAccess;
	}

	/**
	 * Setter for the BOG oid attribute.
	 * 
	 * @param newBogOid
	 *            String
	 */
	public void setBogOid(String newBogOid) {
		bogOid = newBogOid;
	}

	/**
	 * Getter for the bank organization group oid of the user's organization (in the case of BOG organizations and corporate
	 * organizations).
	 * 
	 * @return String
	 */
	public String getBogOid() {
		return bogOid;
	}

	/**
	 * Setter for the organization name attribute.
	 * 
	 * @param newOrganizationName
	 *            String
	 */
	public void setOrganizationName(String newOrganizationName) {
		organizationName = StringFunction.xssCharsToHtml(newOrganizationName);
	}

	/**
	 * Getter for the name of the user's organization.
	 * 
	 * @return String
	 */
	public String getOrganizationName() {
		return organizationName;
	}

	/**
	 * Setter for the base currency description attribute.
	 * 
	 * @param newBaseCurrencyCode
	 *            String
	 */
	public void setBaseCurrencyCode(String newBaseCurrencyCode) {
		baseCurrencyCode = newBaseCurrencyCode;
	}

	/**
	 * Getter for the base currency code of the user's organization (in the case of corporate organizations). Ex: "CAN"
	 * 
	 * @return String
	 */
	public String getBaseCurrencyCode() {
		return baseCurrencyCode;
	}

	/**
	 * Getter for the customer access indicator attribute (from User). This attribute only applies to ADMIN users.
	 * 
	 * @return String
	 */
	public String getCustomerAccessIndicator() {
		return customerAccessIndicator;
	}

	/**
	 * Setter for the customer access indicator attribute (from User). This attribute only applies to ADMIN users.
	 * 
	 * @param newCustomerAccessIndicator
	 *            String
	 */
	public void setCustomerAccessIndicator(String newCustomerAccessIndicator) {
		customerAccessIndicator = newCustomerAccessIndicator;
	}

	/**
	 * Getter for the customer access security profile oid attribute (from User). This attribute only applies to ADMIN users.
	 * 
	 * @return String
	 */
	public String getCustomerAccessSecurityProfileOid() {
		return customerAccessSecurityProfileOid;
	}

	/**
	 * Setter for the customer access security profile oid attribute (from User). This attribute only applies to ADMIN users.
	 * 
	 * @param newCustomerAccessSecurityProfileOid
	 *            String
	 */
	public void setCustomerAccessSecurityProfileOid(String newCustomerAccessSecurityProfileOid) {
		customerAccessSecurityProfileOid = newCustomerAccessSecurityProfileOid;
	}

	/**
	 * Getter for the default WIP View attribute (from User).
	 * 
	 * @return String
	 */
	public String getDefaultWipView() {
		return defaultWipView;
	}

	/**
	 * Getter for the ownerOrgOid attribute (from User).
	 * 
	 * @return String
	 */
	public String getOwnerOrgOid() {
		return ownerOrgOid;
	}

	/**
	 * Getter for the ownership level attribute (from User).
	 * 
	 * @return String
	 */
	public String getOwnershipLevel() {
		return ownershipLevel;
	}

	/**
	 * Getter for the security rights attribute (from SecurityProfile).
	 * 
	 * @return String
	 */
	public String getSecurityRights() {
		return securityRights;
	}

	/**
	 * Getter for the security type attribute (from SecurityProfile).
	 * 
	 * @return String
	 */
	public String getSecurityType() {
		return securityType;
	}

	/**
	 * Getter for the timeZone attribute (from User).
	 * 
	 * @return String
	 */
	public String getTimeZone() {
		return timeZone;
	}

	/**
	 * Getter for the locale attribute (from User).
	 * 
	 * @return String
	 */
	public String getUserLocale() {
		return userLocale;
	}

	/**
	 * Getter for the datePattern attribute (from User).
	 * 
	 * @return String
	 */
	public String getDatePattern() {
		return datePattern;
	}

	/**
	 * Getter for the userOid attribute (from User).
	 * 
	 * @return String
	 */
	public String getUserOid() {
		return userOid;
	}

	/**
	 * Setter for the default WIP view attribute (from User).
	 * 
	 * @param newDefaultWipView
	 *            String
	 */
	public void setDefaultWipView(String newDefaultWipView) {
		defaultWipView = newDefaultWipView;
	}

	/**
	 * Setter for the ownerOrgOid attribute (from User).
	 * 
	 * @param newOwnerOrgOid
	 *            String
	 */
	public void setOwnerOrgOid(String newOwnerOrgOid) {
		ownerOrgOid = newOwnerOrgOid;
	}

	/**
	 * Setter for the ownership level attribute (from User).
	 * 
	 * @param newOwnershipLevel
	 *            String
	 */
	public void setOwnershipLevel(String newOwnershipLevel) {
		ownershipLevel = newOwnershipLevel;
	}

	/**
	 * Setter for the security rights attribute (from SecurityProfile).
	 * 
	 * @param newSecurityRights
	 *            String
	 */
	public void setSecurityRights(String newSecurityRights) {
		securityRights = newSecurityRights;
	}

	/**
	 * Setter for the security type attribute (from SecurityProfile).
	 * 
	 * @param newSecurityType
	 *            String
	 */
	public void setSecurityType(String newSecurityType) {
		securityType = newSecurityType;
	}

	/**
	 * Setter for the timeZone attribute (from User).
	 * 
	 * @param newTimeZone
	 *            String
	 */
	public void setTimeZone(String newTimeZone) {
		timeZone = newTimeZone;
	}

	/**
	 * Setter for the user locale attribute (from User).
	 * 
	 * @param newUserLocale
	 *            String
	 */
	public void setUserLocale(String newUserLocale) {
		userLocale = newUserLocale;
	}

	/**
	 * Setter for the user datepattern attribute (from User).
	 * 
	 * @param newUserLocale
	 *            String
	 */
	public void setDatePattern(String newDatePattern) {
		datePattern = newDatePattern;
	}

	/**
	 * Setter for the userOid attribute (from User).
	 * 
	 * @param newUserOid
	 *            String
	 */
	public void setUserOid(String newUserOid) {
		userOid = newUserOid;
	}

	/**
	 * Getter for the userId attribute (from User).
	 * 
	 * @return String
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * Setter for the userId attribute (from User).
	 * 
	 * @param newLoginId
	 *            String
	 */
	public void setUserId(String newUserId) {
		userId = newUserId;
	}

	/**
	 * @return
	 */
	public String getUserFullName() {
		return userFullName;
	}

	/**
	 * @param userFullName
	 */
	public void setUserFullName(String userFullName) {
		this.userFullName = userFullName;
	}

	/**
	 * Getter for the organization ID (usually the OTL_ID) that was passed to the logon page when this user logged in.
	 * 
	 * @return the id
	 */
	public String getLoginOrganizationId() {
		return loginOrganizationId;
	}

	/**
	 * Setter for the organization ID (usually the OTL_ID) that was passed to the logon page when this user logged in.
	 * 
	 * @param loginOrgId
	 *            - the id
	 */
	public void setLoginOrganizationId(String loginOrgId) {
		loginOrganizationId = loginOrgId;
	}

	/**
	 * Getter for the orgAutoLCCreateIndicator attribute (from CorporateOrganization).
	 * 
	 * @return String
	 */
	public String getOrgAutoLCCreateIndicator() {
		return orgAutoLCCreateIndicator;
	}

	/**
	 * Setter for the orgAutoLCCreateIndicator attribute (from CorporateOrganization).
	 * 
	 * @param newOrgAutoLCCreateIndicator
	 *            String
	 */
	public void setOrgAutoLCCreateIndicator(String newOrgAutoLCCreateIndicator) {
		orgAutoLCCreateIndicator = newOrgAutoLCCreateIndicator;
	}

	/**
	 * Getter for the orgManualPOIndicator attribute (from CorporateOrganization).
	 * 
	 * @return String
	 */
	public String getOrgManualPOIndicator() {
		return orgManualPOIndicator;
	}

	/**
	 * Setter for the orgManualPOIndicator attribute (from CorporateOrganization).
	 * 
	 * @param newOrgManualPOIndicator
	 *            String
	 */
	public void setOrgManualPOIndicator(String newOrgManualPOIndicator) {
		orgManualPOIndicator = newOrgManualPOIndicator;
	}

	// TLE - 08/-0/07 - CR-375 - Begin
	/**
	 * Getter for the orgAutoATPCreateIndicator attribute (from CorporateOrganization).
	 * 
	 * @return String
	 */
	public String getOrgAutoATPCreateIndicator() {
		return orgAutoATPCreateIndicator;
	}

	/**
	 * Setter for the orgAutoATPCreateIndicator attribute (from CorporateOrganization).
	 * 
	 * @param newOrgAutoATPCreateIndicator
	 *            String
	 */
	public void setOrgAutoATPCreateIndicator(String newOrgAutoATPCreateIndicator) {
		orgAutoATPCreateIndicator = newOrgAutoATPCreateIndicator;
	}

	/**
	 * Getter for the orgATPManualPOIndicator attribute (from CorporateOrganization).
	 * 
	 * @return String
	 */
	public String getOrgATPManualPOIndicator() {
		return orgATPManualPOIndicator;
	}

	/**
	 * Setter for the orgATPManualPOIndicator attribute (from CorporateOrganization).
	 * 
	 * @param newOrgATPManualPOIndicator
	 *            String
	 */
	public void setOrgATPManualPOIndicator(String newOrgATPManualPOIndicator) {
		orgATPManualPOIndicator = newOrgATPManualPOIndicator;
	}
	// TLE - 08/-0/07 - CR-375 - End

	/**
	 * Getter for the attribute TimeOutAutoSaveInd from the CorporateOrganization.
	 * 
	 * @return String
	 */
	public String getTimeoutAutoSaveInd() {
		return this.timeoutAutoSaveInd;
	}

	/**
	 * Setter for the attribute TimeOutAutoSaveInd from the CorporateOrganization.
	 * 
	 * @param timeOutAutoSaveInd
	 */
	public void setTimeoutAutoSaveInd(String timeoutAutoSaveInd) {
		this.timeoutAutoSaveInd = timeoutAutoSaveInd;
	}

	/**
	 * Setter for the attribute doc prep indicator from the CorporateOrganization.
	 * 
	 * @param indicator
	 */
	public void setCorpCanAccessDocPrep(String indicator) {
		if ((indicator != null) && indicator.equals(TradePortalConstants.INDICATOR_YES))
			this.corpCanAccessDocPrep = true;
		else
			this.corpCanAccessDocPrep = false;
	}

	/**
	 * Getter for the flag that determines if the corp org can use doc prep
	 * 
	 * @return String
	 */
	public boolean corpCanAccessDocPrep() {
		return this.corpCanAccessDocPrep;
	}

	/**
	 * Setter for the attribute doc prep indicator from the User.
	 * 
	 * @param indicator
	 */
	public void setUserCanAccessDocPrep(String indicator) {
		if ((indicator != null) && indicator.equals(TradePortalConstants.INDICATOR_YES))
			this.userCanAccessDocPrep = true;
		else
			this.userCanAccessDocPrep = false;
	}

	/**
	 * Getter for the flag that determines if the user can use doc prep
	 * 
	 * @return String
	 */
	public boolean userCanAccessDocPrep() {
		return this.userCanAccessDocPrep;
	}

	// BSL CR 749 05/16/2012 Rel 8.0 BEGIN
	/**
	 * Setter for the flag that determines if the user is logged in using SiteMinder Web Agent SSO.
	 * 
	 * @param indicator
	 */
	public void setUsingSiteMinderWebAgentSSO(boolean indicator) {
		usingSiteMinderWebAgentSSO = indicator;
	}

	/**
	 * Getter for the flag that determines if the max session timeout expires before the normal timeout
	 * 
	 * @return boolean
	 */
	public boolean isUsingSiteMinderWebAgentSSO() {
		return usingSiteMinderWebAgentSSO;
	}

	/**
	 * Setter for the flag that determines if the max session timeout expires before the normal timeout
	 * 
	 * @param indicator
	 */
	public void setUsingMaxSessionTimeout(boolean indicator) {
		usingMaxSessionTimeout = indicator;
	}

	/**
	 * Getter for the flag that determines if the max session timeout expires before the normal timeout
	 * 
	 * @return boolean
	 */
	public boolean isUsingMaxSessionTimeout() {
		return usingMaxSessionTimeout;
	}

	/**
	 * Setter for the session start date
	 * 
	 * @param startDate
	 */
	public void setSessionStartDate(Date startDate) {
		sessionStartDate = startDate;
	}

	/**
	 * Getter for the session start date
	 * 
	 * @return java.util.Date
	 */
	public Date getSessionStartDate() {
		return sessionStartDate;
	}
	// BSL CR 749 05/16/2012 Rel 8.0 END

	// CR-433 Krishna Begin 05/22/2008
	/**
	 * Getter for the attribute Doc Prep Url from the CorporateOrganization.
	 * 
	 * @return String
	 */
	public String getDocPrepURL() {
		return this.docPrepURL;
	}

	/**
	 * Setter for the attribute Doc Prep Url from the CorporateOrganization.
	 * 
	 * @param docPrepURL
	 */
	public void setDocPrepURL(String docPrepURL) {
		this.docPrepURL = docPrepURL;
	}
	// CR-433 Krishna End 05/22/2008

	/**
	 * This method is used to return a copy of the session web bean. This is necessary for the Customer Access functionality, where
	 * the logged in user acts on behalf of a corporate organization, and his/her original session must be first saved.
	 *
	 * @return SessionWebBean
	 * @exception java.lang.CloneNotSupportedException
	 */
	public SessionWebBean getSession() throws CloneNotSupportedException {
		return (SessionWebBean) clone();
	}

	/**
	 * Clones the session and stores it on this web bean
	 */
	public void storeSavedUserSession() throws CloneNotSupportedException {
		this.savedUserSession = this.getSession();
	}

	/**
	 * Retrieves the user session that has been saved.
	 *
	 * @return SessionWebBean - the session that was saved
	 */
	public SessionWebBean getSavedUserSession() {
		return this.savedUserSession;
	}

	/**
	 *
	 * @return the security type of the saved user session
	 */
	public String getSavedUserSessionSecurityType() {
		return this.savedUserSession.getSecurityType();
	}

	/**
	 * Returns true if there is a cloned user session saved.
	 *
	 */
	public boolean hasSavedUserSession() {
		return savedUserSession != null;
	}

	/**
	 * Returns true if the user is currently using subsidiary access
	 *
	 */
	public boolean isUsingSubsidiaryAccess() {
		return hasSavedUserSession() && getSavedUserSessionSecurityType().equals(TradePortalConstants.NON_ADMIN);
	}

	/**
	 * Returns true if the user is currently using subsidiary access and they are set up to have access to parent reference data
	 * while using subsidiary access
	 *
	 */
	public boolean showOrgDataUnderSubAccess() {
		return isUsingSubsidiaryAccess() && canSeeOwnDataUsingSubAccess().equals(TradePortalConstants.INDICATOR_YES);
	}

	// Vshah - Portal-refresh - <BEGIN>
	/*
	 * @return
	 */
	public boolean hasInstrumentLock() {
		return this.instrumentLock;
	}

	/**
	 * @param instrumentLock
	 */
	public void setInstrumentLock(boolean instrumentLock) {
		this.instrumentLock = instrumentLock;
	}
	// Vshah - Portal-refresh - <END>

	// -- CR-640/581 Rel 7.1.0 09/22/11 - Begin -
	/**
	 * @return
	 */
	public boolean hasAccessToLiveMarketRate() {
		return TradePortalConstants.INDICATOR_YES.equals(liveMarketRateInd);
	}

	/**
	 * @param liveMarketRateInd
	 */
	public void setLiveMarketRateInd(String liveMarketRateInd) {
		this.liveMarketRateInd = liveMarketRateInd;
	}
	// -- CR-640/581 Rel 7.1.0 09/22/11 - End -

	// cquinton 2/16/2012 Rel 8.0 ppx255 start
	public String getActiveUserSessionOid() {
		return this.activeUserSessionOid;
	}

	public void setActiveUserSessionOid(String x) {
		this.activeUserSessionOid = x;
	}
	// cquinton 2/16/2012 Rel 8.0 ppx255 end

	// SHR Cr708 Rel 8.1.1 start
	/**
	 * Getter for the orgATPInvoiceIndicator attribute (from CorporateOrganization).
	 * 
	 * @return String
	 */
	public String getOrgATPInvoiceIndicator() {
		return orgATPInvoiceIndicator;
	}

	/**
	 * Setter for the orgATPInvoiceIndicator attribute (from CorporateOrganization).
	 * 
	 * @param newOrgATPInvoiceIndicator
	 *            String
	 */
	public void setOrgATPInvoiceIndicator(String newOrgATPInvoiceIndicator) {
		orgATPInvoiceIndicator = newOrgATPInvoiceIndicator;
	}
	// SHR Cr708 Rel 8.1.1 end

	public List<FavoriteTaskObject> getFavoriteTasks() {
		return this.favoriteTasks;
	}

	/**
	 * Setter for the branding directory attribute.
	 * 
	 * @param newBrandingDirectory
	 *            String
	 */
	public void setFavoriteTasks(List<FavoriteTaskObject> favoriteTasksList) {
		favoriteTasks = favoriteTasksList;
	}

	public Queue<RecentInstrumentRef> getRecentInstrumentsQueue() {
		return this.recentInstrumentsQueue;
	}

	/**
	 * Setter for the branding directory attribute.
	 * 
	 * @param newBrandingDirectory
	 *            String
	 */
	public void addRecentInstrument(String transactionOid, String instrumentId, String transactionType, String transactionStatus) {
		// if the instrument is already in the list, remove it - this is becuase it will be added in next step as the latest
		Queue<SessionWebBean.RecentInstrumentRef> recentsQ = this.getRecentInstrumentsQueue();
		for (Iterator<SessionWebBean.RecentInstrumentRef> iter = recentsQ.iterator(); iter.hasNext();) {
			SessionWebBean.RecentInstrumentRef recentInstr = iter.next();

			if (recentInstr.getTransactionOid().equals(transactionOid)) {
				iter.remove();
			}
		}

		// add to the end of the queue
		RecentInstrumentRef ref = new RecentInstrumentRef(transactionOid, instrumentId, transactionType, transactionStatus);
		recentInstrumentsQueue.offer(ref);
		if (recentInstrumentsQueue.size() > MAX_RECENT_INSTRUMENTS) {
			recentInstrumentsQueue.poll();
		}
	}

	public List<FavoriteTaskObject> getFavoriteReports() {
		return this.favoriteReports;
	}

	/**
	 * Setter for the branding directory attribute.
	 * 
	 * @param newBrandingDirectory
	 *            String
	 */
	public void setFavoriteReports(List<FavoriteTaskObject> favoriteReportsList) {
		favoriteReports = favoriteReportsList;
	}

	// W Zhu 8/17/2012 Rel 8.1 T36000004579 add secrekey
	public SecretKey getSecretKey() {
		return this.secretKey;
	}

	public void setSecreKey(SecretKey secretKey) {
		this.secretKey = secretKey;
	}

	public void setAuth2FASubTyp(String auth2FASubType) {
		this.auth2FASubType = auth2FASubType;
	}

	public String getAuth2FASubType() {
		return this.auth2FASubType;
	}

	// cquinton 1/18/2013 adding page flow logic
	/**
	 * Add page to page flow stack
	 * 
	 * @param link
	 *            action
	 */
	public void addPage(String linkAction) {
		addPage(linkAction, (PageFlowParameter[]) null);
	}

	public void addPage(String linkAction, HttpServletRequest request) {
		Map parmMap = request.getParameterMap();
		List parmList = new ArrayList();
		for (Iterator iter = parmMap.keySet().iterator(); iter.hasNext();) {
			String key = (String) iter.next();
			if ("ni" != key) { // ni is generated, don't include it
				String[] values = request.getParameterValues(key);
				if (values != null) {
					for (int j = 0; j < values.length; j++) {
						parmList.add(new PageFlowParameter(key, values[j]));
					}
				}
			}
		}
		PageFlowParameter[] parms = (PageFlowParameter[]) parmList.toArray(new PageFlowParameter[0]);
		addPage(linkAction, parms);
	}

	public void addPage(String linkAction, PageFlowParameter parameter) {
		if (parameter != null) {
			PageFlowParameter[] parms = new PageFlowParameter[1];
			parms[0] = parameter;
			addPage(linkAction, parms);
		} else {
			addPage(linkAction, (PageFlowParameter[]) null);
		}
	}

	public void addPage(String linkAction, PageFlowParameter[] parameter) {
		// first check to see if new page is same as current page, if so skip this
		if (currentPage != null) {
			if (linkAction.equals(currentPage.getLinkAction())) {
				return; // drop out
			}
		}
		int flowSize = pageFlow.size();
		int idx = pageFlow.indexOf(currentPage);
		// remove any now non-valid forward pages
		if (idx >= 0) {
			while (flowSize > idx + 1) {
				pageFlow.remove(idx + 1);
				flowSize--;
			}
			// trim the list
			while (flowSize > maxPageFlowSize && idx > 0) {
				pageFlow.remove(0);
				flowSize--;
				idx--;
			}
		}
		// add to end
		PageFlowRef ref = new PageFlowRef(linkAction, parameter);
		pageFlow.add(ref);
		currentPage = ref;
	}

	/**
	 * Clear the page flow. This is done in event of errors or special reset conditions.
	 */
	public void clearPageFlow() {
		pageFlow.clear();
		currentPage = null;
	}

	/**
	 * Return previous page info, but don't move the current page index.
	 * 
	 * @returns PageRef - page info or null if none
	 */
	public PageFlowRef peekPageBack() {
		PageFlowRef backPage = null;
		int idx = pageFlow.indexOf(currentPage);
		if (idx > 0) {
			backPage = pageFlow.get(idx - 1);
		}
		return backPage;
	}

	/**
	 * Move the current page index back as well as returning the back page info.
	 * 
	 * @returns PageRef - page info or null if not successful
	 */
	public PageFlowRef pageBack() {
		int idx = pageFlow.indexOf(currentPage);
		if (idx > 0) {
			currentPage = pageFlow.get(idx - 1);
		}
		return currentPage;
	}

	/**
	 * Get current page info.
	 * 
	 * @returns PageRef - page info or null if not successful
	 */
	public PageFlowRef getCurrentPage() {
		return currentPage;
	}

	/**
	 * This method is used to restore a user's session that was saved
	 *
	 * @return void
	 */
	public void restoreSession() {
		if (savedUserSession != null) {
			this.setOrganizationName(savedUserSession.getOrganizationName());
			this.setBrandingDirectory(savedUserSession.getBrandingDirectory());
			this.setBrandButtonInd(savedUserSession.getBrandButtonInd());
			this.setClientBankOid(savedUserSession.getClientBankOid());
			this.setClientBankCode(savedUserSession.getClientBankCode());
			this.setBogOid(savedUserSession.getBogOid());
			this.setBaseCurrencyCode(savedUserSession.getBaseCurrencyCode());
			this.setOwnerOrgOid(savedUserSession.getOwnerOrgOid());
			this.setDefaultWipView(savedUserSession.getDefaultWipView());
			this.setOwnershipLevel(savedUserSession.getOwnershipLevel());
			this.setCustomerAccessIndicator(savedUserSession.getCustomerAccessIndicator());
			this.setSecurityType(savedUserSession.getSecurityType());
			this.setSecurityRights(savedUserSession.getSecurityRights());
			this.setOrgAutoLCCreateIndicator(savedUserSession.getOrgAutoLCCreateIndicator());
			this.setOrgManualPOIndicator(savedUserSession.getOrgManualPOIndicator());

			this.setOrgAutoATPCreateIndicator(savedUserSession.getOrgAutoATPCreateIndicator());
			this.setOrgATPManualPOIndicator(savedUserSession.getOrgATPManualPOIndicator());

			this.setLoginOrganizationId(savedUserSession.getLoginOrganizationId());
			this.setPasswordValidationFlag(savedUserSession.getPasswordValidationFlag());
			this.setReportingUserSuffix(savedUserSession.getReportingUserSuffix());
			this.setGlobalOrgOid(savedUserSession.getGlobalOrgOid());
			this.setTimeoutAutoSaveInd(savedUserSession.getTimeoutAutoSaveInd());
			this.setCorpCanAccessDocPrep(savedUserSession.corpCanAccessDocPrep() ? TradePortalConstants.INDICATOR_YES : TradePortalConstants.INDICATOR_NO);
			this.setUserCanAccessDocPrep(savedUserSession.userCanAccessDocPrep() ? TradePortalConstants.INDICATOR_YES : TradePortalConstants.INDICATOR_NO);
			// CR-433 Krishna Begin 05/22/2008
			this.setDocPrepURL(savedUserSession.getDocPrepURL());
			// CR-433 Krishna End 05/22/2008
			this.setReportLoginInd(false);
			// BSL CR 749 05/16/2012 Rel 8.0 BEGIN
			this.setUsingSiteMinderWebAgentSSO(savedUserSession.isUsingSiteMinderWebAgentSSO());
			this.setUsingMaxSessionTimeout(savedUserSession.isUsingMaxSessionTimeout());
			this.setSessionStartDate(savedUserSession.getSessionStartDate());
			// BSL CR 749 05/16/2012 Rel 8.0 END

			// Clear out the saved user session
			this.setShowToolTips(savedUserSession.getShowToolTips());

			// T36000013446- use custom http header to communicate CMA specific status codes
			this.setAuthMethod(savedUserSession.getAuthMethod());
			this.setSsoId(savedUserSession.getSsoId());

			// Tang Rel9.3.5 CR#1032 Session Synch -[Begin]
			this.setSessionSynchImg(savedUserSession.getSessionSynchImg());
			// Tang Rel9.3.5 CR#1032 Session Synch -[End]

			savedUserSession = null;
		}
	}

	public Boolean getShowToolTips() {
		return showToolTips;
	}

	public void setShowToolTips(Boolean showToolTips) {
		this.showToolTips = showToolTips;
	}

	// vdesingu Rel8.4 CR-590 Start
	/**
	 * @return the isAcctBalancesRefreshed
	 */
	public boolean isAcctBalancesRefreshed() {
		return isAcctBalancesRefreshed;
	}

	/**
	 * @param isAcctBalancesRefreshed
	 *            the isAcctBalancesRefreshed to set
	 */
	public void setAcctBalancesRefreshed(boolean isAcctBalancesRefreshed) {
		this.isAcctBalancesRefreshed = isAcctBalancesRefreshed;
	}
	// vdesingu Rel8.4 CR-590 End

	// MEerupula Rel 8.4 IR-23915
	public String getConfInd() {
		return confInd;
	}

	public void setConfInd(String confInd) {
		this.confInd = confInd;
	}

	public String getSubsidConfInd() {
		return subsidConfInd;
	}

	public void setSubsidConfInd(String subsidConfInd) {
		this.subsidConfInd = subsidConfInd;
	}

	// T36000013446- use custom http header to communicate CMA specific status codes - start
	public String getAuthMethod() {
		return authMethod;
	}

	public void setAuthMethod(String authMethod) {
		this.authMethod = authMethod;
	}

	public String getSsoId() {
		return ssoId;
	}

	public void setSsoId(String ssoId) {
		this.ssoId = ssoId;
	}

	/**
	 * @return the bankGrpRestrictRuleOid
	 */
	public String getBankGrpRestrictRuleOid() {
		return bankGrpRestrictRuleOid;
	}

	/**
	 * @param bankGrpRestrictRuleOid
	 *            the bankGrpRestrictRuleOid to set
	 */
	public void setBankGrpRestrictRuleOid(String bankGrpRestrictRuleOid) {
		this.bankGrpRestrictRuleOid = bankGrpRestrictRuleOid;
	}

	// T36000013446- use custom http header to communicate CMA specific status codes - end

	public boolean isCustNotIntgTPS() {
		return custNotIntgTPS;
	}

	public void setCustNotIntgTPS(boolean custNotIntgTPS) {
		this.custNotIntgTPS = custNotIntgTPS;
	}

	public boolean isEnableAdminUpdateInd() {
		return enableAdminUpdateInd;
	}

	public void setEnableAdminUpdateInd(boolean enableAdminUpdateInd) {
		this.enableAdminUpdateInd = enableAdminUpdateInd;
	}

	// Tang Rel9.3.5 CR#1032 Session Synch -[Begin]
	public String getSessionSynchImg() {
		return sessionSynchImg;
	}

	public void setSessionSynchImg(String sessionSynchImg) {
		this.sessionSynchImg = sessionSynchImg;
	}
	// Tang Rel9.3.5 CR#1032 Session Synch -[End]

}