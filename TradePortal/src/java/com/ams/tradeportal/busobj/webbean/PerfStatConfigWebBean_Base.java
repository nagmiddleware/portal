

  

package com.ams.tradeportal.busobj.webbean;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import javax.servlet.http.*;

/**
 * A client bank that has chosen to use the Trade Portal for their customers'
 * trade processing.   
 * 
 * Each bank has a separate instrument number (ID) range.  When instruments
 * are created, this range is used to determine the valid range of instrument
 * numbers.
 * 
 * Information about instrument ID ranges, security, branding, and default
 * templates are stored in a ClientBank record.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class PerfStatConfigWebBean_Base extends ReferenceDataOwnerWebBean
{

  public void registerAttributes()
   {  
      super.registerAttributes();

      /* perf_stat_config_oid - Unique identifier */
      registerAttribute("perf_stat_config_oid", "perf_stat_config_oid", "ObjectIDAttribute");
      
      /* name - The name of the page */
      registerAttribute("page_name", "page_name");
      
      
      
      /* activation_status - Indicates whether or not the SLA monitoring has been activated.  */
      registerAttribute("active", "active","IndicatorAttribute");
      
      registerAttribute("client_bank", "client_bank");
   }   




}
