package com.ams.tradeportal.busobj.webbean;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;

import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;

import com.ams.tradeportal.common.*;

import javax.servlet.http.*;
import com.ams.tradeportal.html.*;
import com.ams.tradeportal.common.TPDateTimeUtility;
import com.ams.tradeportal.common.cache.*;

/*
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class UserWebBean extends UserWebBean_Base
{
    //cquinton 12/20/2012 replicate here from UserBean for use on jsps
    /**
     * This method checks to see if the user's password has expired.
     *
     * @return true if password is expired, false if it is valid.
     */
    public boolean isPasswordExpired()
            throws AmsException, RemoteException {
        boolean isExpired = true;;

        String passwordChangeDateStr = this.getAttribute("password_change_date");
        if( passwordChangeDateStr == null || passwordChangeDateStr.length()<=0 ) {
            // If password change date is null, consider it expired
            isExpired = true;
        }
        else {
            // Determine password expiration period
            int passwordExpirationPeriodDays = this.getPasswordExpirationDays();

            // Converts the expiration period (passed in as days)
            // into milliseconds
            // (number of days) * 24 hours in a day * 60 minutes in an hour *
            //     60 seconds in a minute * 1000 milliseconds in a second
            long numMillisecondsInDay = 86400000;
            long expirationPeriod = passwordExpirationPeriodDays * numMillisecondsInDay;

            // Last password change date is stored as GMT (converted to milliseconds since 1/1/1970)
            Date lastPasswordChangeDate = null;
            //not sure why but this is in isodate format
            if ( passwordChangeDateStr.indexOf("-")>0 ) {
                lastPasswordChangeDate = TPDateTimeUtility.convertDateStringToDate(
                    passwordChangeDateStr,"yyyy-MM-dd HH:mm:ss.S");
            } 
            else { //just in case, but i think never used
                lastPasswordChangeDate = DateTimeUtility.convertStringDateTimeToDate (                            
                    passwordChangeDateStr);            
            }
            long lastPasswordChangeDateMillis = lastPasswordChangeDate.getTime();

            // Get current date time in GMT (converted to milliseconds since 1/1/1970)
            long todaysDateInGMT = GMTUtility.getGMTDateTime().getTime();


            // Determine if the password is expired
            if( (lastPasswordChangeDateMillis + expirationPeriod) < todaysDateInGMT ) {
                isExpired = true;
            }
            else {
                isExpired = false;
            }
        }

        return isExpired;
    }
    
    private int getPasswordExpirationDays() throws RemoteException, AmsException
    {
        String passwordExpirationDaysStr = "";

        if( TradePortalConstants.OWNER_GLOBAL.equals(this.getAttribute("ownership_level"))) {
            // For Proponix users, use the values stored in a config file.
            passwordExpirationDaysStr = 
                String.valueOf(SecurityRules.getProponixChangePasswordDaysLimit());
        }
        else
        {
            //get client bank info from cache
            String clientBankOid = this.getAttribute("client_bank_oid");
            Cache reCertCache = (Cache)TPCacheManager.getInstance().getCache(TradePortalConstants.CLIENT_BANK_CACHE);
            DocumentHandler CBCResult = (DocumentHandler)reCertCache.get(clientBankOid);   

            // Pull the appropriate values from the client bank
            if( TradePortalConstants.OWNER_CORPORATE.equals(this.getAttribute("ownership_level")) ) {
                passwordExpirationDaysStr =
                    CBCResult.getAttribute("/ResultSetRecord(0)/CORP_CHANGE_PASSWORD_DAYS");
            }
            else
            {
                passwordExpirationDaysStr = 
                    CBCResult.getAttribute("/ResultSetRecord(0)/BANK_CHANGE_PASSWORD_DAYS");
            }
        }
        
        int passwordExpirationDays =
            Integer.parseInt(passwordExpirationDaysStr);
        
        return passwordExpirationDays;
   }
    
}
