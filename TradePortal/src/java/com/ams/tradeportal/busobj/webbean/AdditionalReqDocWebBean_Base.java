//Leelavathi - 10thDec2012 - Rel8200 CR-737 - Initialversion  

package com.ams.tradeportal.busobj.webbean;


/**
 * 
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class AdditionalReqDocWebBean_Base extends TradePortalBusinessObjectWebBean
{

  public void registerAttributes()
   {  
      super.registerAttributes();
      registerAttribute("addl_req_doc_oid", "addl_req_doc_oid", "ObjectIDAttribute");
      registerAttribute("addl_req_doc_ind", "addl_req_doc_ind", "IndicatorAttribute");
      registerAttribute("addl_req_doc_name", "addl_req_doc_name");
      registerAttribute("addl_req_doc_originals", "addl_req_doc_originals", "NumberAttribute");
      registerAttribute("addl_req_doc_copies", "addl_req_doc_copies", "NumberAttribute");
      registerAttribute("addl_req_doc_text", "addl_req_doc_text");
      registerAttribute("terms_oid", "p_terms_oid", "ParentIDAttribute");
   }
  }
