package com.ams.tradeportal.busobj.webbean;

import com.ams.tradeportal.common.Sort;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.util.ResourceManager;
import com.amsinc.ecsg.util.StringFunction;


public class PurchaseOrderDefinitionWebBean extends PurchaseOrderDefinitionWebBean_Base {
	//Leelavathi CR-707 Rel-800 20/03/2012 Begin
	private int summaryRecCount;
	private int goodsRecCount;
	private int lineitemRecCount;
	//Srinivasu CR-707 Rel-800 04/08/2012 Begin
	private int userDefCount;
	private int poLineItemCount;
	private int getPoLineItemCount() {
		return poLineItemCount;
	}
	
	private void setPoLineItemCount(int poLineItemCount) {
		this.poLineItemCount = poLineItemCount;
	}

	public int getUserDefCount() {
		return userDefCount;
	}

	private void setUserDefCount(int userDefCount) {
		this.userDefCount = userDefCount;
	}
	//Srinivasu CR-707 Rel-800 04/08/2012 End
	public int getSummaryRecCount() {
		return summaryRecCount;
	}

	private void setSummaryRecCount(int summaryRecCount) {
		this.summaryRecCount = summaryRecCount;
	}

	public int getGoodsRecCount() {
		return goodsRecCount;
	}

	private void setGoodsRecCount(int goodsRecCount) {
		this.goodsRecCount = goodsRecCount;
	}

	public int getLineitemRecCount() {
		return lineitemRecCount;
	}

	private void setLineitemRecCount(int lineitemRecCount) {
		this.lineitemRecCount = lineitemRecCount;
	}
	//Leelavathi CR-707 Rel-800 20/03/2012 End
	/**
	 * Builds a set of sorted html Purchase summary option tags based on the 7 predefined data
	 * attributes (amount, ben_name, currency, ...) as well as the optional 
	 * "linked_instrument_ty" fields.  The option id is the internal identifier (e.g., amount 
	 * or other1) and the displayed value is whatever name the user has 
	 * assigned to that attribute.  Only non-blank attributes are included.
	 * 
	 * @return java.lang.String
	 */
		
	public String buildINVFieldOptionList(ResourceManager resMgr) {	    
		String[] options = new String[9];
		StringBuffer optionList = new StringBuffer();
		int count = 0;
		int x = 1;
		int tabPosition;
		
		// Build an array consisting of each non-blank attribute.  The array
		// contents consist of the user's provided attribute name, a tab 
		// separator, and the internal name.    
		//Srinivasu CR-707 Rel-800 04/08/2012 Begin
			options[count++] = resMgr.getText("POStructureDefinition.PurchaseOrderNum", TradePortalConstants.TEXT_BUNDLE) + "\t" + "purchase_order_num";								
			options[count++] = resMgr.getText("POStructureDefinition.OrderType", TradePortalConstants.TEXT_BUNDLE) + "\t" + "purchase_order_type";
			options[count++] = resMgr.getText("POStructureDefinition.IssueDate", TradePortalConstants.TEXT_BUNDLE) + "\t" + "issue_date";
			options[count++] = resMgr.getText("POStructureDefinition.Currency", TradePortalConstants.TEXT_BUNDLE) + "\t" + "currency";
			options[count++] = resMgr.getText("POStructureDefinition.Amount", TradePortalConstants.TEXT_BUNDLE) + "\t" + "amount";
			options[count++] = resMgr.getText("POStructureDefinition.SellerName", TradePortalConstants.TEXT_BUNDLE) + "\t" + "seller_name";
			//Srinivasu CR-707 Rel-800 04/08/2012 End
				
			
		// Now sort the options array.  Since the user's defined name is the first
		// part of each entry, that's how they are sorted.
		try {
	    		Sort.sort(options, resMgr.getResourceLocale());
		} catch (Exception e) {
			// if the sort fails, we do nothing and the list remains unsorted
		}

		// Now build the HTML option list from the sorted array
		for (x = 0; x < count; x++) {
			tabPosition = options[x].indexOf("\t");

			optionList.append("<option value=");
			optionList.append(options[x].substring(tabPosition + 1, options[x].length()));
			optionList.append(">");
			optionList.append(options[x].substring(0, tabPosition));
			optionList.append("</option>");
		}
		//Leelavathi CR-707 Rel-800 20/03/2012 Begin
		setSummaryRecCount(count);
		//Leelavathi CR-707 Rel-800 20/03/2012 End
	
		return optionList.toString();

	}

	/**
	 * Builds a set of sorted html Purchase Goods option tags based on the the optional 
	 * "28" attribute.  The option id is the internal identifier (e.g., other1, 
	 * or other2) and the displayed value is whatever name the user has 
	 * assigned to that attribute.  Only non-blank attributes are included.
	 * 
	 * @return java.lang.String
	 */
	
	
	public String buildINVGoodsFieldOptionList(ResourceManager resMgr) {	    
		String fieldName;
		String[] options = new String[28];
		StringBuffer optionList = new StringBuffer();
		int count = 0;
		int x = 1;
		int tabPosition;
		
		// Build an array consisting of each non-blank attribute.  The array
		// contents consist of the user's provided attribute name, a tab 
		// separator, and the internal name.      						
        
		if((TradePortalConstants.INDICATOR_YES).equals(getAttribute("incoterm_req")))
			options[count++] = resMgr.getText("POStructureDefinition.Incoterm", TradePortalConstants.TEXT_BUNDLE) + "\t" + "incoterm";

		if((TradePortalConstants.INDICATOR_YES).equals(getAttribute("incoterm_loc_req")))
			options[count++] = resMgr.getText("POStructureDefinition.IncotermLoc", TradePortalConstants.TEXT_BUNDLE) + "\t" + "incoterm_location";
		
		//Leelavathi CR-707 Rel-800 13/03/2012 Begin
		if((TradePortalConstants.INDICATOR_YES).equals(getAttribute("latest_shipment_date_req")))
			options[count++] = resMgr.getText("POStructureDefinition.ShipmentDate", TradePortalConstants.TEXT_BUNDLE) + "\t" + "latest_shipment_date";
		//Leelavathi CR-707 Rel-800 13/03/2012 End

		if((TradePortalConstants.INDICATOR_YES).equals(getAttribute("partial_ship_allowed_req")))
			options[count++] = resMgr.getText("POStructureDefinition.PartialShip", TradePortalConstants.TEXT_BUNDLE) + "\t" + "partial_shipment_allowed";

		if((TradePortalConstants.INDICATOR_YES).equals(getAttribute("goods_desc_req")))
			options[count++] = resMgr.getText("POStructureDefinition.GoodsDesc", TradePortalConstants.TEXT_BUNDLE) + "\t" + "goods_description";
   								
		for (x = 1; x <= TradePortalConstants.PO_BUYER_OR_SELLER_USER_DEF_FIELD; x++) {
			fieldName =  getAttribute("buyer_users_def" + x + "_label");
			if ((TradePortalConstants.INDICATOR_YES).equals(getAttribute("buyer_users_def" + x + "_req"))) {
				options[count++] = StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(fieldName))+  "\t" + "buyer_user_def" + x + "_label";
				
			}
		}
		for (x = 1; x <= TradePortalConstants.PO_BUYER_OR_SELLER_USER_DEF_FIELD; x++) {
			fieldName =  getAttribute("seller_users_def" + x + "_label");
		    if ((TradePortalConstants.INDICATOR_YES).equals(getAttribute("seller_users_def" + x + "_req"))) {
					options[count++] = StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(fieldName)) +  "\t" + "seller_user_def" + x + "_label";
					
				}		
		} 
		setUserDefCount(count);
		// Now sort the options array.  Since the user's defined name is the first
		// part of each entry, that's how they are sorted.
		try {
	    		Sort.sort(options, resMgr.getResourceLocale());
		} catch (Exception e) {
			// if the sort fails, we do nothing and the list remains unsorted
		}
		
		// Now build the HTML option list from the sorted array
		for (x = 0; x < count; x++) {
			tabPosition = options[x].indexOf("\t");

			optionList.append("<option value=");
			optionList.append(options[x].substring(tabPosition + 1, options[x].length()));
			optionList.append(">");
			optionList.append(options[x].substring(0, tabPosition));
			optionList.append("</option>");
		}  
		//Leelavathi CR-707 Rel-800 20/03/2012 Begin
		setGoodsRecCount(count);
		//Leelavathi CR-707 Rel-800 20/03/2012 End
		return optionList.toString();

	}
	/**
	 * Builds a set of sorted html Purchase line item detail option tags based optional 
	 * "12" attribute.  The option id is the internal identifier (e.g., other1, 
	 * or other2) and the displayed value is whatever name the user has 
	 * assigned to that attribute.  Only non-blank attributes are included.
	 * 
	 * @return java.lang.String
	 */	
	public String buildLineItemFieldOptionList(ResourceManager resMgr) {	    
		String fieldName;
		String[] options = new String[12];
		StringBuffer optionList = new StringBuffer();
		int count = 0;
		int x = 1;
		int tabPosition;
		
		// Build an array consisting of each non-blank attribute.  The array
		// contents consist of the user's provided attribute name, a tab 
		// separator, and the internal name.      						
			
		if((TradePortalConstants.INDICATOR_YES).equals(getAttribute("line_item_num_req")))
			options[count++] = resMgr.getText("POStructureDefinition.LineItemNumber", TradePortalConstants.TEXT_BUNDLE) + "\t" + "line_item_num";
			//Srinivasu_D Rel8.0 IR# BRUM032159913 04/02/2012 Start   
		if((TradePortalConstants.INDICATOR_YES).equals(getAttribute("unit_price_req")))
			options[count++] = resMgr.getText("POStructureDefinition.LineItemUnitPrice", TradePortalConstants.TEXT_BUNDLE) + "\t" + "unit_price";
		
		if((TradePortalConstants.INDICATOR_YES).equals(getAttribute("unit_of_measure_req")))
			options[count++] = resMgr.getText("POStructureDefinition.LineItemUnitMeasure", TradePortalConstants.TEXT_BUNDLE) + "\t" + "unit_of_measure";

		if((TradePortalConstants.INDICATOR_YES).equals(getAttribute("quantity_req")))
			options[count++] = resMgr.getText("POStructureDefinition.LineItemDetailQuantity", TradePortalConstants.TEXT_BUNDLE) + "\t" + "quantity";

		if((TradePortalConstants.INDICATOR_YES).equals(getAttribute("quantity_var_plus_req")))
			options[count++] = resMgr.getText("POStructureDefinition.LineItemDetailQntPlus", TradePortalConstants.TEXT_BUNDLE) + "\t" + "quantity_variance_plus";
			//Srinivasu_D Rel8.0 IR# BRUM032159913 04/02/2012 End
   								
		for (x = 1; x <= TradePortalConstants.PO_LINE_ITEM_TYPE_OR_VALUE_FIELD; x++) {
			fieldName =  getAttribute("prod_chars_ud" + x + "_label");
			if ((TradePortalConstants.INDICATOR_YES).equals(getAttribute("prod_chars_ud" + x + "_req"))) {
				options[count++] = StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(fieldName)) +  "\t" + "prod_chars_ud" + x + "_label";								
			}
		} 
		setPoLineItemCount(count);
		// Now sort the options array.  Since the user's defined name is the first
		// part of each entry, that's how they are sorted.
		try {
	    		Sort.sort(options, resMgr.getResourceLocale());
		} catch (Exception e) {
			// if the sort fails, we do nothing and the list remains unsorted
		}

		// Now build the HTML option list from the sorted array
		for (x = 0; x < count; x++) {
			tabPosition = options[x].indexOf("\t");

			optionList.append("<option value=");
			optionList.append(options[x].substring(tabPosition + 1, options[x].length()));
			optionList.append(">");
			optionList.append(options[x].substring(0, tabPosition));
			optionList.append("</option>");
		}

		//Leelavathi CR-707 Rel-800 20/03/2012 Begin
		setLineitemRecCount(count);
		//Leelavathi CR-707 Rel-800 20/03/2012 End
	
		return optionList.toString();

}
	
	
	
	
	
	/**
	 * Taking an HTML set of <option> values, searches for the one with an id
	 * equal to the given selection.  If found, adds the 'selected' tag to
	 * the HTML.  The updated set of options is returned.
	 *
	 * @return java.lang.String
	 * @param htmlOptions java.lang.String - a set of html option tags
	 * @param selection java.lang.String - the selection to search for 
	 */
	public String selectFieldInList(String htmlOptions, String selection) {
		if (StringFunction.isNotBlank(selection)) {
			int start = htmlOptions.indexOf(selection);
			int insertPosition = htmlOptions.indexOf(">", start);
			htmlOptions = 
				htmlOptions.substring(0, insertPosition)
					+ " selected"
					+ htmlOptions.substring(insertPosition); 
		}
		return htmlOptions;

	}
	
	/**
	 * This method retursn the options for LC Creation Rule's dynamic dropdown box
	 * @return String
	 */
	public String buildLCOptionList(ResourceManager resMgr) {	    
		String fieldName;
		String[] options = new String[32];
		StringBuffer optionList = new StringBuffer();
		int count = 0;
		int x = 1;
		int tabPosition;
		
		// Build an array consisting of each non-blank attribute.  The array
		// contents consist of the user's provided attribute name, a tab 
		// separator, and the internal name.  
			options[count++] = resMgr.getText("POStructureDefinition.PurchaseOrderNum", TradePortalConstants.TEXT_BUNDLE) + "\t" + "purchase_order_num";								
			options[count++] = resMgr.getText("POStructureDefinition.OrderType", TradePortalConstants.TEXT_BUNDLE) + "\t" + "purchase_order_type";
			options[count++] = resMgr.getText("POStructureDefinition.IssueDate", TradePortalConstants.TEXT_BUNDLE) + "\t" + "issue_date";
			options[count++] = resMgr.getText("POStructureDefinition.Currency", TradePortalConstants.TEXT_BUNDLE) + "\t" + "currency";
			options[count++] = resMgr.getText("POStructureDefinition.Amount", TradePortalConstants.TEXT_BUNDLE) + "\t" + "amount";
			options[count++] = resMgr.getText("POStructureDefinition.SellerName", TradePortalConstants.TEXT_BUNDLE) + "\t" + "seller_name";
			options[count++] = resMgr.getText("POStructureDefinition.ShipmentDate", TradePortalConstants.TEXT_BUNDLE) + "\t" + "latest_shipment_date";
			
			if((TradePortalConstants.INDICATOR_YES).equals(getAttribute("incoterm_req")))
				options[count++] = resMgr.getText("POStructureDefinition.Incoterm", TradePortalConstants.TEXT_BUNDLE) + "\t" + "incoterm";
			if((TradePortalConstants.INDICATOR_YES).equals(getAttribute("incoterm_loc_req")))
				options[count++] = resMgr.getText("POStructureDefinition.IncotermLoc", TradePortalConstants.TEXT_BUNDLE) + "\t" + "incoterm_location";
			if((TradePortalConstants.INDICATOR_YES).equals(getAttribute("partial_ship_allowed_req")))
				options[count++] = resMgr.getText("POStructureDefinition.PartialShip", TradePortalConstants.TEXT_BUNDLE) + "\t" + "partial_shipment_allowed";
			if((TradePortalConstants.INDICATOR_YES).equals(getAttribute("goods_desc_req")))
				options[count++] = resMgr.getText("POStructureDefinition.GoodsDesc", TradePortalConstants.TEXT_BUNDLE) + "\t" + "goods_description";						
			for (x = 1; x <= TradePortalConstants.PO_BUYER_OR_SELLER_USER_DEF_FIELD; x++) {
				fieldName =  getAttribute("buyer_users_def" + x + "_label");
				if ((TradePortalConstants.INDICATOR_YES).equals(getAttribute("buyer_users_def" + x + "_req"))) {
					options[count++] = StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(fieldName)) +  "\t" + "buyer_user_def" + x + "_value";  //RKAZI REL 8.1 IR-MMUM092141213 09/24/12 changed the
					                                                                         //option value
				}
			}
			for (x = 1; x <= TradePortalConstants.PO_BUYER_OR_SELLER_USER_DEF_FIELD; x++) {
				fieldName =  getAttribute("seller_users_def" + x + "_label");
			    if ((TradePortalConstants.INDICATOR_YES).equals(getAttribute("seller_users_def" + x + "_req"))) {
						options[count++] = StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(fieldName)) +  "\t" + "seller_user_def" + x + "_value"; //RKAZI REL 8.1 IR-MMUM092141213 09/24/12 changed the
                                                                                                 //option value
						
					}		
			}
					
		// Now sort the options array.  Since the user's defined name is the first
		// part of each entry, that's how they are sorted.
		try {
	    		Sort.sort(options, resMgr.getResourceLocale());
		} catch (Exception e) {
			// if the sort fails, we do nothing and the list remains unsorted
		}

		// Now build the HTML option list from the sorted array
		for (x = 0; x < count; x++) {
			tabPosition = options[x].indexOf("\t");

			optionList.append("<option value=");
			optionList.append(options[x].substring(tabPosition + 1, options[x].length()));
			optionList.append(">");
			optionList.append(options[x].substring(0, tabPosition));
			optionList.append("</option>");
		}

	
		return optionList.toString();

	}
	
	
}
