

  

package com.ams.tradeportal.busobj.webbean;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import javax.servlet.http.*;

/**
 * After processing, data stored as OutgoingInterfaceQueue business objects
 * is moved to be stored as OutgoingQueueHistory.  This is done to prevenet
 * the OutgoingInterfaceQueue table from becoming too large.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class OutgoingQueueHistoryWebBean_Base extends TradePortalBusinessObjectWebBean
{

  public void registerAttributes()
   {  
      super.registerAttributes();


       /* queue_history_oid - Unique identifier */
       registerAttribute("queue_history_oid", "queue_history_oid", "ObjectIDAttribute");


       /* date_created - Timestamp (in GMT)  when this data was originally created in the OutgoingInterfaceQueue
         table. */
       registerAttribute("date_created", "date_created", "DateTimeAttribute");

       /* override_date_created - Timestamp (in GMT)  when this data was originally created in the OutgoingInterfaceQueue
       table. */
       	registerAttribute("override_date_created", "override_date_created", "DateTimeAttribute");
       

       /* date_sent - Timestamp (in GMT) of when this data was placed onto the queue to be sent
         to OTL. */
       registerAttribute("date_sent", "date_sent", "DateTimeAttribute");


       /* status - Lifecycle status of the message. */
       registerAttribute("status", "status");


       /* confirm_status - Indicates whether or not a confirmation has been received from OTL for a
         message that was sent to OTL. */
       registerAttribute("confirm_status", "confirm_status");


       /* msg_text - The actual text of the XML message */
       registerAttribute("msg_text", "msg_text");


       /* packaging_error_text - Errors from packaging. */
       registerAttribute("packaging_error_text", "packaging_error_text");


       /* unpackaging_error_text - Errors from unpackaging */
       registerAttribute("unpackaging_error_text", "unpackaging_error_text");


       /* msg_type - The type of message to be processed.  This indicates what data is contained
         in the XML - a transaction or mail message. */
       registerAttribute("msg_type", "msg_type");


       /* message_id -  */
       registerAttribute("message_id", "message_id");


       /* reply_to_message_id - Used for confirmation messages only.  Refers to the message that this message
         is confirming */
       registerAttribute("reply_to_message_id", "reply_to_message_id");


       /* agent_id - The name of the agent that is currently dealing with this message.   This
         prevents multiple agents from accessing the same data. */
       registerAttribute("agent_id", "agent_id");


       /* process_parameters - Extra processing parameters that are specific to certain message types.
         The parameters are formatted as name value pairs delimited by |, for example,
         name1=value1|name2=value2|name3=value3. */
       registerAttribute("process_parameters", "process_parameters");


       /* transaction_oid - The OID of the transaction or main object on which an action was performed
         that required placing a row in the outgoing queue.  This is not an association
         so it can store OID of heterogeneous objects. */
       registerAttribute("transaction_oid", "a_transaction_oid", "NumberAttribute");


       /* datetime_pack_start - Date and time when the packaging of the message starts. */
       registerAttribute("datetime_pack_start", "datetime_pack_start", "DateTimeAttribute");


       /* pack_seconds - The amount of time (in seconds) it takes to package the message. */
       registerAttribute("pack_seconds", "pack_seconds", "NumberAttribute");


       /* process_seconds - The amount of time (in seconds) it takes to process the message, starting
         from the outgoing_queue object is created to to the message being sent out. */
       registerAttribute("process_seconds", "process_seconds", "NumberAttribute");

       /* instrument_oid - The instrument on which an action was performed that required placing a
         row in the outgoing queue.. */
       registerAttribute("instrument_oid", "a_instrument_oid", "NumberAttribute");

       /* mail_message_oid - The mail message on which an action was performed that required placing
         a row in the outgoing queue.. */
       registerAttribute("mail_message_oid", "a_mail_message_oid", "NumberAttribute");
       
       /* xml_sent_time - Date/Time when the XML was placed on outgoing destination queue ie. TPS incoming Queue by TIM */
       registerAttribute("xml_sent_time", "xml_sent_time", "DateTimeAttribute");
       /* system_name -  */
       registerAttribute("system_name", "system_name");
       
       /* server_name -  */
       registerAttribute("server_name", "server_name");
       
       //Rel9.3.5 CR-1028 - Start
       /* downloaded_xml_ind - Whether this transaction's xml has been downloaded. */
       registerAttribute("downloaded_xml_ind", "downloaded_xml_ind", "IndicatorAttribute");
       //Rel9.3.5 CR-1028 - End
   }
   
   




}
