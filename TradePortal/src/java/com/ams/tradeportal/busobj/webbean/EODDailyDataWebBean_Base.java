

  

package com.ams.tradeportal.busobj.webbean;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import javax.servlet.http.*;

/**
 * An Incoming End of Day Transaction Request message will be sent to the portal
 * by the bank.  There will be one of these records per account each day.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class EODDailyDataWebBean_Base extends TradePortalBusinessObjectWebBean
{

  public void registerAttributes()
   {  
      super.registerAttributes();


       /* eod_daily_data_oid - Unique Identifier */
       registerAttribute("eod_daily_data_oid", "eod_daily_data_oid", "ObjectIDAttribute");


       /* balance_date - The date for which balances are being reported in YYYYMMDD format. */
       registerAttribute("balance_date", "balance_date", "DateTimeAttribute");


       /* source - Indicates the source/type of account. */
       registerAttribute("source", "source");


       /* bank_routing_id - The bank routing id for the account */
       registerAttribute("bank_routing_id", "bank_routing_id");


       /* statement_number - First 4 bytes of the statement number. */
       registerAttribute("statement_number", "statement_number", "NumberAttribute");


       /* opening_balance - Opening Balance (signed amount). There will be an explicit decimal point.
         First character is always either a "+", which represents a Credit or a "-"
         which represents a Debit. Right justified. Zero filled */
       registerAttribute("opening_balance", "opening_balance", "TradePortalSignedDecimalAttribute");


       /* closing_balance - Closing Balance (signed amount). There will be an explicit decimal point.
         First character is always either a "+", which represents a Credit or a "-"
         which represents a Debit. Right justified. Zero filled */
       registerAttribute("closing_balance", "closing_balance", "TradePortalSignedDecimalAttribute");


       /* total_debit_movement - Total debit movement amount (unsigned). Right justified and zero filled.
         Explicit decimal. */
       registerAttribute("total_debit_movement", "total_debit_movement", "TradePortalSignedDecimalAttribute");


       /* number_of_debits - Total number of debits */
       registerAttribute("number_of_debits", "number_of_debits", "NumberAttribute");


       /* total_credit_movement - Total credit movement amount (unsigned). Right justified and zero filled.
         Explicit decimal. */
       registerAttribute("total_credit_movement", "total_credit_movement", "TradePortalSignedDecimalAttribute");


       /* number_of_credits - Total number of  credits */
       registerAttribute("number_of_credits", "number_of_credits", "NumberAttribute");


       /* debit_interest_rate - Interest rate applied if the account is in debit, otherwise zero. Assumed
         3 decimal places */
       registerAttribute("debit_interest_rate", "debit_interest_rate", "TradePortalDecimalAttribute");


       /* credit_interest_rate - Interest rate applied if the account is in credit, otherwise zero. Assumed
         3 decimal places */
       registerAttribute("credit_interest_rate", "credit_interest_rate", "TradePortalDecimalAttribute");


       /* overdraft_limit - Overdraft limit. Unsigned amount. */
       registerAttribute("overdraft_limit", "overdraft_limit", "TradePortalDecimalAttribute");


       /* debit_interest_accrued - Debit Interest Accrued (unsigned amount) Right justified and zero filled. */
       registerAttribute("debit_interest_accrued", "debit_interest_accrued", "TradePortalDecimalAttribute");


       /* credit_interest_accrued - Credit Interest Accrued (unsigned amount) Right justified and zero filled. */
       registerAttribute("credit_interest_accrued", "credit_interest_accrued", "TradePortalDecimalAttribute");


       /* fid_accrued - Financial Institutions Duty Accrued (unsigned amount) Right justified and
         zero filled. */
       registerAttribute("fid_accrued", "fid_accrued", "TradePortalDecimalAttribute");


       /* badt_accrued - Business Account Deposit Tax, (unsigned amount) Right justified and zero
         filled. */
       registerAttribute("badt_accrued", "badt_accrued", "TradePortalDecimalAttribute");


       /* next_posting_date - Next posting date in YYYYMMDD format */
       registerAttribute("next_posting_date", "next_posting_date", "DateTimeAttribute");


       /* date_time_received - Date and time on which EOD Data was received. */
       registerAttribute("date_time_received", "date_time_received", "DateTimeAttribute");

      /* Pointer to the parent Account */
       registerAttribute("account_oid", "p_account_oid", "ParentIDAttribute");
   }
   
   




}
