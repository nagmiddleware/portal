
package com.ams.tradeportal.busobj.webbean;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import javax.servlet.http.*;
/**
 * Details for the various actions performed on the Invoice
 * by a user
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class InvoiceHistoryWebBean_Base extends TradePortalBusinessObjectWebBean
{

  public void registerAttributes()
   {  
      super.registerAttributes();


       /* invoice_history_oid - Unique Identifier */
       registerAttribute("invoice_history_oid", "invoice_history_oid", "ObjectIDAttribute");

       /* action_datetime - Timestamp (in GMT) when the action is taken. */
       registerAttribute("action_datetime", "action_datetime", "DateTimeAttribute");

       /* action - Action. */
       registerAttribute("action", "action");
       
       /* invoice_status - Staus of invoices. */
       registerAttribute("invoice_status", "invoice_status");

      /* Pointer to the parent Invoice */
       registerAttribute("upload_invoice_oid", "p_upload_invoice_oid", "ParentIDAttribute");

       /* user_oid - The user who the updates the invoice. */
       registerAttribute("user_oid", "a_user_oid", "NumberAttribute");
       
       //Nar 04 Sep 2013 Release8.3.0.0 IR-T36000020395 ADD Begin
       /* panel_authority_code - The Panel Level at which the authorization action is taken. */
       registerAttribute("panel_authority_code", "panel_authority_code");
      //Nar 04 Sep 2013 Release8.3.0.0 IR-T36000020395 ADD End
   }
} 