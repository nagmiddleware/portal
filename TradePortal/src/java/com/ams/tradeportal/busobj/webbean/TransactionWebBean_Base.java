
  

package com.ams.tradeportal.busobj.webbean;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;

import javax.servlet.http.*;

/**
 * Represents a transaction between the corporate customer and their bank.
 * When a transaction is authorized, it is sent to the back end system, OTL,
 * and processed.   
 * 
 * A transaction has two sets of terms associated with it: Customer Entered
 * Terms and Bank Released Terms.   Customer Entered terms are created and
 * edited on the Trade Portal.  Bank Released Terms are updated only by OTL.
 *
 *     Copyright  © 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class TransactionWebBean_Base extends TradePortalBusinessObjectWebBean
{

  public void registerAttributes()
   {  
      super.registerAttributes();


       /* transaction_oid - Unique identifier */
       registerAttribute("transaction_oid", "transaction_oid", "ObjectIDAttribute");

       /*display_transaction_oid - Added for BTMU, for display purpose */
       registerAttribute("display_transaction_oid", "display_transaction_oid", "NumberAttribute");

       /* transaction_type_code - Code indicating the type of transaction.   Examples of types are Issue,
         Amend, Assignment of Proceeds, etc. */
       registerAttribute("transaction_type_code", "transaction_type_code");


       /* transaction_status - Lifecycle status of a transactioin */
       registerAttribute("transaction_status", "transaction_status");

       /* sequence_num - Indicates the sequence number of the transaction on the back end system.
         This is never updated in the portal.  It is updated by the middleware instead. */
       registerAttribute("sequence_num", "sequence_num", "NumberAttribute");


       /* show_on_notifications_tab - Indicates whether or not the transaction should appear on the Notifications
         Listview.   This is set to No initially, then set to Yes when the middleware
         updates the status of the transaction after it is authorized.    When a
         user "deletes" the notification, this flag is simpley set to No. */
       registerAttribute("show_on_notifications_tab", "show_on_notifications_tab", "IndicatorAttribute");
       registerAttribute("unread_flag", "unread_flag", "IndicatorAttribute");

       /* transaction_status_date - Timestamp (in GMT) of the last time the status of this transaction was updated */
       registerAttribute("transaction_status_date", "transaction_status_date", "DateTimeAttribute");


       /* copy_of_amount - This field contains a copy of the amount stored in the most recently updated
         set of terms. */
       registerAttribute("copy_of_amount", "copy_of_amount", "TradePortalSignedDecimalAttribute");


       /* copy_of_currency_code - This field contains a copy of the currency code stored in the most recently
         updated set of terms. */
       registerAttribute("copy_of_currency_code", "copy_of_currency_code");


       /* authorization_errors - If there were any errors from the most recent authorization attempt,  this
         attribute contains XML text that was returned from the mediator in the /Error
         section.   The Authorization Errors page reads these errors and displays
         them. */
       registerAttribute("authorization_errors", "authorization_errors");


       /* copy_of_instr_type_code - A copy of the instrument type code from the parent Instrument business object.
         The copy is stored here for convenience so that the parent object does not
         need to be examining as frequently in order to obtain the instrument type
         code. */
       registerAttribute("copy_of_instr_type_code", "copy_of_instr_type_code");


       /* first_authorize_status_date - Timestamp (in GMT) of when the first authorization took place on this transaction.
         Depending on the corporate customer's settings, this may be the only authorization
         that takes places on this transaction. */
       registerAttribute("first_authorize_status_date", "first_authorize_status_date", "DateTimeAttribute");


       /* second_authorize_status_date - Timestamp (in GMT) of when the second authorization took place on this transaction.
         Depending on the corporate customer's settings, only one authorization may
         be necessary. */
       registerAttribute("second_authorize_status_date", "second_authorize_status_date", "DateTimeAttribute");


       /* transaction_as_text - A representation of the transaction is SWIFT message format.  This is only
         used for a small number of instrument types and is always populated only
         by the middleware.   The Document Preparation functionality reads this attribute
         and uses it to create an XML document. */
       registerAttribute("transaction_as_text", "transaction_as_text");


       /* standby_using_guarantee_form - If this attribute is set to Yes, the Guarantee or "Long" form will be displayed
         to users to enter data for this LC.   If this attribute is set to No, the
         form that is displayed will correspond exactly to the instrument type. */
       registerAttribute("standby_using_guarantee_form", "standby_using_guarantee_form", "IndicatorAttribute");


       /* return_message_id - Populated by the middleware */
       registerAttribute("return_message_id", "return_message_id");


       /* instrument_amount - The instrument amount of the instrument as of the last update of this transaction.
         The amount is stored for each transaction to create a "history" of amounts
         for an instrument.   When the copy_of_amount attribute of Transaction is
         updated, this field is updated with a copy as well. */
       registerAttribute("instrument_amount", "instrument_amount", "TradePortalDecimalAttribute");


       /* available_amount - The available amount of the instrument as of the last update of this transaction.
         The amount is stored for each transaction to create a "history" of amounts
         for an instrument. This is not updated on the portal; it is updated by the
         middleware. */
       registerAttribute("available_amount", "available_amount", "TradePortalDecimalAttribute");


       /* liability_amt_in_limit_curr - The liability amount of the instrument in the limit currency as of the last
         update of this transaction.    The amount is stored for each transaction
         to create a "history" of amounts for an instrument.  This is not updated
         on the portal; it is updated by the middleware. */
       registerAttribute("liability_amt_in_limit_curr", "liability_amt_in_limit_curr", "TradePortalDecimalAttribute");


       /* equivalent_amount - The equivalent amount of the instrument as of the last update of this transaction.
         The amount is stored for each transaction to create a "history" of amounts
         for an instrument.  This is not updated on the portal; it is updated by
         the middleware. */
       registerAttribute("equivalent_amount", "equivalent_amount", "TradePortalDecimalAttribute");


       /* base_currency_code - The base currency code for this transaction */
       registerAttribute("base_currency_code", "base_currency_code");


       /* liability_amt_in_base_curr - The liability amount of the instrument in the base currency as of the last
         update of this transaction.    The amount is stored for each transaction
         to create a "history" of amounts for an instrument.  This is not updated
         on the portal; it is updated by the middleware. */
       registerAttribute("liability_amt_in_base_curr", "liability_amt_in_base_curr", "TradePortalDecimalAttribute");


       /* limit_currency_code - The limit currency code for this transaction */
       registerAttribute("limit_currency_code", "limit_currency_code");


       /* notification_date - Timestamp (in GMT) of when the notification message was created for this
         transaction. */
       registerAttribute("notification_date", "notification_date", "DateTimeAttribute");


       /* globaltrade_msg - Contains an XML message that will be sent to @GlobalTrade upon successful
         unpackaging of this transaction. */
       registerAttribute("globaltrade_msg", "globaltrade_msg");


       /* display_change_transaction - Indicates whether or not a Change transaction should appear on the Instrument
         Summary Page; in certain situations the banks don't want the corporate customer
         to to view Changes because its an internal transaction type. */
       registerAttribute("display_change_transaction", "display_change_transaction", "IndicatorAttribute");


       /* conversion_indicator - Whether this transaction has been converted. */
       registerAttribute("conversion_indicator", "conversion_indicator", "IndicatorAttribute");


       /* rejection_reason_text - Stores the current rejection reason(s) to be stored and displayed against
         the relevant rejected transaction. */
       registerAttribute("rejection_reason_text", "rejection_reason_text");


       /* rejection_indicator - so that the portal can identify a rejected transaction and so that when
         the transaction is re-authorized and sent to the bank, the portal XML will
         have a new "ResubmissionIndicator" set to Y.
         Can also have a value of 'X' to allow TPAgents to bypass presave Validation. */
       registerAttribute("rejection_indicator", "rejection_indicator");


       /* date_started - The date the transaction was created. */
       registerAttribute("date_started", "date_started", "DateTimeAttribute");


       /* resulting_port_act_uoid - The original OTL UOID of the resulting_port_act_oid association.  We need
         this since the transaction may come in TP before the otl_portfolio_activity
         does.  We need to store the original uoid until the portfolio activity comes
         in and translate it to the oid association. */
       registerAttribute("resulting_port_act_uoid", "resulting_port_act_uoid");


       /* prior_active_port_act_uoid - The original OTL UOID of the prior_active_port_act_oid association.  We
         need this since the transaction may come in TP before the otl_portfolio_activity
         does.  We need to store the original uoid until the portfolio activity comes
         in and translate it to the oid association. */
       registerAttribute("prior_active_port_act_uoid", "prior_active_port_act_uoid");


       /* otl_transaction_uoid - The UOID of the instrument in OTL DB.  This helps reporting that spans over
         the OTL &amp; TP db tables. */
       registerAttribute("otl_transaction_uoid", "otl_transaction_uoid");


       /* sequence_date - The sequence datetime that the transaction is released on OTL. */
       registerAttribute("sequence_date", "sequence_date", "DateTimeAttribute");


       /* payment_date - Timestamp (in GMT) of the Payment  transaction.   It could be a future payment
         to occur after Date_Started. */
       registerAttribute("payment_date", "payment_date", "DateTimeAttribute");

       // Narayan Rel 8.3 CR 821 06/06/2013 commented below code as now panel data will save in panel_authorizer table  Start
       /* authorizing_panel_code_1 - The first authorizing user's panel authority code. 
       registerAttribute("authorizing_panel_code_1", "authorizing_panel_code_1");


        authorizing_panel_code_2 - The second authorizing user's panel authority code. 
       registerAttribute("authorizing_panel_code_2", "authorizing_panel_code_2");


        authorizing_panel_code_3 - The third authorizing user's panel authority code. 
       registerAttribute("authorizing_panel_code_3", "authorizing_panel_code_3");


        authorizing_panel_code_4 - The fourth authorizing user's panel authority code. 
       registerAttribute("authorizing_panel_code_4", "authorizing_panel_code_4");


        authorizing_panel_code_5 - The fifth authorizing user's panel authority code. 
       registerAttribute("authorizing_panel_code_5", "authorizing_panel_code_5");


        authorizing_panel_code_6 - The sixth authorizing user's panel authority code. 
       registerAttribute("authorizing_panel_code_6", "authorizing_panel_code_6");


        third_authorize_status_date - Timestamp (in GMT) of when the third authorization took place on this transaction. 
       registerAttribute("third_authorize_status_date", "third_authorize_status_date", "DateTimeAttribute");


        fourth_authorize_status_date - Timestamp (in GMT) of when the fourth authorization took place on this transaction. 
       registerAttribute("fourth_authorize_status_date", "fourth_authorize_status_date", "DateTimeAttribute");


        fifth_authorize_status_date - Timestamp (in GMT) of when the fifth authorization took place on this transaction. 
       registerAttribute("fifth_authorize_status_date", "fifth_authorize_status_date", "DateTimeAttribute");


        sixth_authorize_status_date - Timestamp (in GMT) of when the sixth authorization took place on this transaction. 
       registerAttribute("sixth_authorize_status_date", "sixth_authorize_status_date", "DateTimeAttribute");
*/
       // Narayan Rel 8.3 CR 821 06/06/2013 End
       /* daily_limit_exceeded_indicator - This is used by Cash Management transactions when the daily limit is exceeded. */
       registerAttribute("daily_limit_exceeded_indicator", "daily_limit_exceeded_indicator");


       /* uploaded_ind - Whether the transaction is created from uploading file. */
       registerAttribute("uploaded_ind", "uploaded_ind", "IndicatorAttribute");

       /* assigned_to_user_oid - The user to which a transaction is currently assigned. */
       registerAttribute("assigned_to_user_oid", "a_assigned_to_user_oid", "NumberAttribute");

      /* Pointer to the parent InstrumentData */
       registerAttribute("instrument_oid", "p_instrument_oid", "ParentIDAttribute");

       /* first_authorizing_user_oid - The user that performed the first authorization on a transaction.  Depending
         on the corporate customer settings, this may be the only user to perform
         an authorize on this transaction. */
       registerAttribute("first_authorizing_user_oid", "a_first_authorizing_user_oid", "NumberAttribute");

       /* second_authorizing_user_oid - The user that performed the second authorization on a transaction.  Depending
         on the corporate customer settings, this may be no second user to authorize
         this transaction. */
       registerAttribute("second_authorizing_user_oid", "a_second_authorizing_user_oid", "NumberAttribute");

       /* CustomerEnteredTerms - Terms of the transaction that were entered by the user on the Trade Portal. */
       registerAttribute("c_CustomerEnteredTerms", "c_CUST_ENTER_TERMS_OID", "NumberAttribute");

       /* BankReleasedTerms - The terms of the transaction that were updated using the middleware by the
         back end system, OTL.  Bank Released terms are never created using the Trade
         Portal. */
       registerAttribute("c_BankReleasedTerms", "c_BANK_RELEASE_TERMS_OID", "NumberAttribute");

       /* last_entry_user_oid - The last user to enter data and save it for a transaction. */
       registerAttribute("last_entry_user_oid", "a_last_entry_user_oid", "NumberAttribute");

       /* first_authorizing_work_group_oid - The work group of the first user that performed the authorization on a transaction.
         Depending on the corporate customer settings, this user may be the only
         user to perform an authorize on this transaction. */
       registerAttribute("first_authorizing_work_group_oid", "a_first_auth_work_group_oid", "NumberAttribute");

       /* second_authorizing_work_group_oid - The work group of the second user that performed the authorization on a
         transaction.  Depending on the corporate customer settings, this may be
         no second user to authorize this transaction. */
       registerAttribute("second_authorizing_work_group_oid", "a_second_auth_work_group_oid", "NumberAttribute");
       
       // Narayan Rel 8.3 CR 821 06/06/2013 commented below code as now panel data will save in panel_authorizer table  Start
       /* third_authorizing_user_oid - The user that performed the third authorization on a transaction. 
       registerAttribute("third_authorizing_user_oid", "a_third_authorizing_user_oid", "NumberAttribute");

        fourth_authorizing_user_oid - The user that performed the fourth authorization on a transaction. 
       registerAttribute("fourth_authorizing_user_oid", "a_fourth_authorizing_user_oid", "NumberAttribute");

        fifth_authorizing_user_oid - The user that performed the second authorization on a transaction. 
       registerAttribute("fifth_authorizing_user_oid", "a_fifth_authorizing_user_oid", "NumberAttribute");

        sixth_authorizing_user_oid - The user that performed the sixth authorization on a transaction. 
       registerAttribute("sixth_authorizing_user_oid", "a_sixth_authorizing_user_oid", "NumberAttribute");

        third_authorizing_work_group_oid - The work group of the first user that performed the authorization on a transaction. 
       registerAttribute("third_authorizing_work_group_oid", "a_third_auth_work_group_oid", "NumberAttribute");

        fourth_authorizing_work_group_oid - The work group of the first user that performed the authorization on a transaction. 
       registerAttribute("fourth_authorizing_work_group_oid", "a_fourth_auth_work_group_oid", "NumberAttribute");

        fifth_authorizing_work_group_oid - The work group of the fifth user that performed the authorization on a transaction. 
       registerAttribute("fifth_authorizing_work_group_oid", "a_fifth_auth_work_group_oid", "NumberAttribute");

        sixth_authorizing_work_group_oid - The work group of the sixth user that performed the authorization on a transaction. 
       registerAttribute("sixth_authorizing_work_group_oid", "a_sixth_auth_work_group_oid", "NumberAttribute");*/
       // Narayan Rel 8.3 CR 821 06/06/2013 End
       /*SHILPAR CR-597 added copy_payment_file_amt*/
       registerAttribute("copy_payment_file_amt", "copy_payment_file_amt", "TradePortalSignedDecimalAttribute");
       
 	  /* CJR CR-593 gxs_ref */
 	  registerAttribute("gxs_ref", "gxs_ref");
 	  
 	  /* CJR CR-593 customer_file_ref */
 	  registerAttribute("customer_file_ref", "customer_file_ref");
 registerAttribute("converted_transaction_ind", "converted_transaction_ind", "IndicatorAttribute");
 	  
	  // Narayan Rel 8.3 CR 821 06/06/2013 Start
      /* panel_auth_group_oid - Panel group associated to a transaction. */
       registerAttribute("panel_auth_group_oid", "a_panel_auth_group_oid", "NumberAttribute");
       
       /* panel_auth_range_oid - Panel Range to a transaction. */
       registerAttribute("panel_auth_range_oid", "a_panel_auth_range_oid", "NumberAttribute");
      
       /* opt_lock - Optimistic lock attribute*/
       registerAttribute("panel_oplock_val", "panel_oplock_val", "NumberAttribute");   
       
       /* panel_auth_next_approvals - this will contain all required next panel level approvals to authorize transaction. */
       registerAttribute("panel_auth_next_approvals", "panel_auth_next_approvals");
	  // Narayan Rel 8.3 CR 821 06/06/2013 End
	  
	  // Narayan Rel 8.3 CR 857 07/15/2013 Start
      /* bene_panel_auth_ind - Whether panel authorization should be executed at the beneficiary level. */
      registerAttribute("bene_panel_auth_ind", "bene_panel_auth_ind", "IndicatorAttribute");
      // Narayan Rel 8.3 CR 857 07/15/2013 End
      
      /* NAR CR-694A Rel9.0.0.0 05-MAY-2014  */
      /* h2h_source - sender of payment file i.e GXS or FLA */
	  registerAttribute("h2h_source", "h2h_source");
	  
	  //MEer Rel 9.1 CR-934b- To store payment_method_type at transaction level
      registerAttribute("payment_method_type", "payment_method_type");
      
      //Rel9.3.5 CR-1028 - Start
      /* downloaded_xml_ind - Whether this transaction's xml has been downloaded. */
      registerAttribute("downloaded_xml_ind", "downloaded_xml_ind", "IndicatorAttribute");
      //Rel9.3.5 CR-1028 - End
      // Nar CR-818 Rel 9400 08/03/2015
      /* settle_instr_work_item_type - this field will contain work item type i.e WRO, WSP, WSL */
      registerAttribute("settle_instr_work_item_type", "settle_instr_work_item_type");
      
      //Rel9.5 CR-1132 
      /* client_bank_ind - Whether this transaction is verified or not. default will be 'N' */
      registerAttribute("client_bank_ind", "client_bank_ind", "IndicatorAttribute");
      
   }
   
   




}
