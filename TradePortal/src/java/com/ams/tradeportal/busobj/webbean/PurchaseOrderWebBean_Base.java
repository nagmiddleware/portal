

  

package com.ams.tradeportal.busobj.webbean;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import javax.servlet.http.*;

/**
 * Represents a purchase order that has been uploaded into the Trade Portal.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class PurchaseOrderWebBean_Base extends TradePortalBusinessObjectWebBean
{

  public void registerAttributes()
   {  
      super.registerAttributes();


       /* purchase_order_oid - Unique identfier */
       registerAttribute("purchase_order_oid", "purchase_order_oid", "ObjectIDAttribute");


       /* purchase_order_num - Field to store the purchase order number.   This field will be unique within
         all Purchase Orders for a particular corporate organization. */
       registerAttribute("purchase_order_num", "purchase_order_num");


       /* purchase_order_type - This details the type of purchase order designation. */
       registerAttribute("purchase_order_type", "purchase_order_type");


       /* opt_lock - Optimistic lock attribute */
       registerAttribute("opt_lock", "opt_lock", "OptimisticLockAttribute");


       /* issue_date - Purchase Order Issue Date. */
       registerAttribute("issue_date", "issue_date", "DateAttribute");


       /* latest_shipment_date - The latest shipment date of this PO line item */
       registerAttribute("latest_shipment_date", "latest_shipment_date", "DateTimeAttribute");


       /* currency - The currency in which the amount is expressed. */
       registerAttribute("currency", "currency");


       /* amount - The amount of the Purchase Order */
       registerAttribute("amount", "amount", "TradePortalDecimalAttribute");


       /* seller_name - The name of the seller for this Purchase Order. */
       registerAttribute("seller_name", "seller_name");


       /* incoterm - The shipping term, or Incoterm, that describes what is included in the total
         cost of the transaction goods. For example, Cost and Freight or Cost Insurance
         Freight. */
       registerAttribute("incoterm", "incoterm");


       /* incoterm_location - field representing the Incoterm location associated with the incoterm type. */
       registerAttribute("incoterm_location", "incoterm_location");


       /* partial_shipment_allowed - If this checkbox is selected, partial shipments are allowed. */
       registerAttribute("partial_shipment_allowed", "partial_shipment_allowed", "IndicatorAttribute");


       /* buyer_user_def1_label - A buyer user-defined field label for Purchase Order */
       registerAttribute("buyer_user_def1_label", "buyer_user_def1_label");


       /* buyer_user_def2_label - A buyer user-defined field label for Purchase Order */
       registerAttribute("buyer_user_def2_label", "buyer_user_def2_label");


       /* buyer_user_def3_label - A buyer user-defined field label for Purchase Order */
       registerAttribute("buyer_user_def3_label", "buyer_user_def3_label");


       /* buyer_user_def4_label - A buyer user-defined field label for Purchase Order */
       registerAttribute("buyer_user_def4_label", "buyer_user_def4_label");


       /* buyer_user_def5_label - A buyer user-defined field label for Purchase Order */
       registerAttribute("buyer_user_def5_label", "buyer_user_def5_label");


       /* buyer_user_def6_label - A buyer user-defined field label for Purchase Order */
       registerAttribute("buyer_user_def6_label", "buyer_user_def6_label");


       /* buyer_user_def7_label - A buyer user-defined field label for Purchase Order */
       registerAttribute("buyer_user_def7_label", "buyer_user_def7_label");


       /* buyer_user_def8_label - A buyer user-defined field label for Purchase Order */
       registerAttribute("buyer_user_def8_label", "buyer_user_def8_label");


       /* buyer_user_def9_label - A buyer user-defined field label for Purchase Order */
       registerAttribute("buyer_user_def9_label", "buyer_user_def9_label");


       /* buyer_user_def10_label - A buyer user-defined field label for Purchase Order */
       registerAttribute("buyer_user_def10_label", "buyer_user_def10_label");


       /* buyer_user_def1_value - A buyer user-defined field value for Purchase Order */
       registerAttribute("buyer_user_def1_value", "buyer_user_def1_value");


       /* buyer_user_def2_value - A buyer user-defined field value for Purchase Order */
       registerAttribute("buyer_user_def2_value", "buyer_user_def2_value");


       /* buyer_user_def3_value - A buyer user-defined field value for Purchase Order */
       registerAttribute("buyer_user_def3_value", "buyer_user_def3_value");


       /* buyer_user_def4_value - A buyer user-defined field value for Purchase Order */
       registerAttribute("buyer_user_def4_value", "buyer_user_def4_value");


       /* buyer_user_def5_value - A buyer user-defined field value for Purchase Order */
       registerAttribute("buyer_user_def5_value", "buyer_user_def5_value");


       /* buyer_user_def6_value - A buyer user-defined field value for Purchase Order */
       registerAttribute("buyer_user_def6_value", "buyer_user_def6_value");


       /* buyer_user_def7_value - A buyer user-defined field value for Purchase Order */
       registerAttribute("buyer_user_def7_value", "buyer_user_def7_value");


       /* buyer_user_def8_value - A buyer user-defined field value for Purchase Order */
       registerAttribute("buyer_user_def8_value", "buyer_user_def8_value");


       /* buyer_user_def9_value - A buyer user-defined field value for Purchase Order */
       registerAttribute("buyer_user_def9_value", "buyer_user_def9_value");


       /* buyer_user_def10_value - A buyer user-defined field value for Purchase Order */
       registerAttribute("buyer_user_def10_value", "buyer_user_def10_value");


       /* seller_user_def1_label - A seller user-defined field label for Purchase Order */
       registerAttribute("seller_user_def1_label", "seller_user_def1_label");


       /* seller_user_def2_label - A seller user-defined field label for Purchase Order */
       registerAttribute("seller_user_def2_label", "seller_user_def2_label");


       /* seller_user_def3_label - A seller user-defined field label for Purchase Order */
       registerAttribute("seller_user_def3_label", "seller_user_def3_label");


       /* seller_user_def4_label - A seller user-defined field label for Purchase Order */
       registerAttribute("seller_user_def4_label", "seller_user_def4_label");


       /* seller_user_def5_label - A seller user-defined field label for Purchase Order */
       registerAttribute("seller_user_def5_label", "seller_user_def5_label");


       /* seller_user_def6_label - A seller user-defined field label for Purchase Order */
       registerAttribute("seller_user_def6_label", "seller_user_def6_label");


       /* seller_user_def7_label - A seller user-defined field label for Purchase Order */
       registerAttribute("seller_user_def7_label", "seller_user_def7_label");


       /* seller_user_def8_label - A seller user-defined field label for Purchase Order */
       registerAttribute("seller_user_def8_label", "seller_user_def8_label");


       /* seller_user_def9_label - A seller user-defined field label for Purchase Order */
       registerAttribute("seller_user_def9_label", "seller_user_def9_label");


       /* seller_user_def10_label - A seller user-defined field label for Purchase Order */
       registerAttribute("seller_user_def10_label", "seller_user_def10_label");


       /* seller_user_def1_value - A seller user-defined field label for Purchase Order */
       registerAttribute("seller_user_def1_value", "seller_user_def1_value");


       /* seller_user_def2_value - A seller user-defined field label for Purchase Order */
       registerAttribute("seller_user_def2_value", "seller_user_def2_value");


       /* seller_user_def3_value - A seller user-defined field label for Purchase Order */
       registerAttribute("seller_user_def3_value", "seller_user_def3_value");


       /* seller_user_def4_value - A seller user-defined field label for Purchase Order */
       registerAttribute("seller_user_def4_value", "seller_user_def4_value");


       /* seller_user_def5_value - A seller user-defined field label for Purchase Order */
       registerAttribute("seller_user_def5_value", "seller_user_def5_value");


       /* seller_user_def6_value - A seller user-defined field label for Purchase Order */
       registerAttribute("seller_user_def6_value", "seller_user_def6_value");


       /* seller_user_def7_value - A seller user-defined field label for Purchase Order */
       registerAttribute("seller_user_def7_value", "seller_user_def7_value");


       /* seller_user_def8_value - A seller user-defined field label for Purchase Order */
       registerAttribute("seller_user_def8_value", "seller_user_def8_value");


       /* seller_user_def9_value - A seller user-defined field label for Purchase Order */
       registerAttribute("seller_user_def9_value", "seller_user_def9_value");


       /* seller_user_def10_value - A seller user-defined field label for Purchase Order */
       registerAttribute("seller_user_def10_value", "seller_user_def10_value");


       /* goods_description -  */
       registerAttribute("goods_description", "goods_description");


       /* status - Purchase Order status */
       registerAttribute("status", "status");


       /* creation_date_time - Timestamp when Purchase Order was created. */
       registerAttribute("creation_date_time", "creation_date_time", "DateTimeAttribute");


       /* deleted_ind - Indicates whether the Purchase Order is deleted. */
       registerAttribute("deleted_ind", "deleted_ind", "IndicatorAttribute");


       /* amend_seq_no - Tracks number of amend done. */
       registerAttribute("amend_seq_no", "amend_seq_no", "NumberAttribute");

       /* transaction_oid -  */
       registerAttribute("transaction_oid", "a_transaction_oid", "NumberAttribute");

       /* owner_org_oid -  */
       registerAttribute("owner_org_oid", "a_owner_org_oid", "NumberAttribute");

       /* upload_definition_oid -  */
       registerAttribute("upload_definition_oid", "a_upload_definition_oid", "NumberAttribute");

       /* instrument_oid -  */
       registerAttribute("instrument_oid", "a_instrument_oid", "NumberAttribute");
	
	/* shipment_oid - This association indicates to which association a PO is assoicated. */
       registerAttribute("shipment_oid", "a_shipment_oid", "NumberAttribute");
   
	 
   }
   
   




}
