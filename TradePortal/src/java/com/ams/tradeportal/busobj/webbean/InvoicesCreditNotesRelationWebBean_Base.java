package com.ams.tradeportal.busobj.webbean;


/**
 * Describes the InvoicesCreditNotesRelation.
 * 
 * Copyright � 2003 American Management Systems, Incorporated All rights
 * reserved
 */
public class InvoicesCreditNotesRelationWebBean_Base extends
		TradePortalBusinessObjectWebBean {
	public void registerAttributes() {
		super.registerAttributes();

		/* invoice_credit_note_rel_oid - Unique identifier */
	     registerAttribute("invoice_credit_note_rel_oid", "invoice_credit_note_rel_oid", "ObjectIDAttribute");
	      
	      /* credit_note_applied_amount - Credit Amount applied for an Invoice */
	     registerAttribute("credit_note_applied_amount", "credit_note_applied_amount","TradePortalDecimalAttribute");	
	     
	      /* a_upload_credit_note_oid - Points to Credit_Note */
	     registerAttribute("a_upload_credit_note_oid", "a_upload_credit_note_oid", "NumberAttribute");
	      
	      /* a_upload_credit_note_oid - Points to Invoices_summary_data */
	     registerAttribute("a_upload_invoice_oid", "a_upload_invoice_oid", "NumberAttribute");
	}

}
