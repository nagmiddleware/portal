

  

package com.ams.tradeportal.busobj.webbean;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import javax.servlet.http.*;
import com.ams.tradeportal.html.*;

/**
 * Abstract business object to represent an organization in the Trade Portal.
 * All organizations are subclasses of this business object.
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class OrganizationWebBean_Base extends TradePortalBusinessObjectWebBean
{

  public void registerAttributes()
   {  
      super.registerAttributes();


       /* organization_oid - Unique identifier */
       registerAttribute("organization_oid", "organization_oid", "ObjectIDAttribute");


       /* name - The name of the organization */
       registerAttribute("name", "name");


       /* activation_status - Indicates whether or not the organization has been deactivated.  In most
         cases, organizations are not deleted from the database; they are deactivated
         instead. */
       registerAttribute("activation_status", "activation_status");


       /* date_deactivated - Timestamp (in GMT) of when the organization was deactivated. */
       registerAttribute("date_deactivated", "date_deactivated", "DateTimeAttribute");


       /* opt_lock - Optimistic lock attribute
         See jPylon documentation for details on how this works */
       registerAttribute("opt_lock", "opt_lock", "OptimisticLockAttribute");
   }
   
   




}
