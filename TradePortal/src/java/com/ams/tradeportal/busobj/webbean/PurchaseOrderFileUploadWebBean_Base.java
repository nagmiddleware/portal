

  

package com.ams.tradeportal.busobj.webbean;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import javax.servlet.http.*;

/**
 * The PurchaseOrder files that are to be uploaded.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class PurchaseOrderFileUploadWebBean_Base extends com.amsinc.ecsg.web.AmsEntityWebBean
{

  public void registerAttributes()
   {  


       /* po_file_upload_oid - Unique Identifider */
       registerAttribute("po_file_upload_oid", "po_file_upload_oid", "ObjectIDAttribute");


       /* po_file_name - Purchase Order File Name. */
       registerAttribute("po_file_name", "po_file_name");


       /* validation_status - The status of Purchase Order File upload validation. */
       registerAttribute("validation_status", "validation_status");


       /* number_of_po_uploaded - The number of purchase orders successfully uploaded. */
       registerAttribute("number_of_po_uploaded", "number_of_po_uploaded", "NumberAttribute");


       /* number_of_po_failed - Number of purchase orders that failed for upload. */
       registerAttribute("number_of_po_failed", "number_of_po_failed", "NumberAttribute");


       /* creation_timestamp - Date/Time when the file is uploaded. */
       registerAttribute("creation_timestamp", "creation_timestamp", "DateTimeAttribute");


       /* completion_timestamp - Date/Time when the validation is completed. */
       registerAttribute("completion_timestamp", "completion_timestamp", "DateTimeAttribute");


       /* agent_id - The name of the upload agent that processes/validates the file.  This prevents
         multiple agents from accessing the same file. */
       registerAttribute("agent_id", "agent_id");


       /* deleted_ind - Whether the file is deleted. */
       registerAttribute("deleted_ind", "deleted_ind", "IndicatorAttribute");


       /* error_msg - Error message of uploading. */
       registerAttribute("error_msg", "error_msg");


       /* validation_seconds - The time it takes the agent to validate the file. */
       registerAttribute("validation_seconds", "validation_seconds", "NumberAttribute");


       /* currency - The currency of the Purchase Orders in the file. If the file contains multiple
         currencies, the system will display "Mixed" */
       registerAttribute("currency", "currency");


       /* amount - The total amount of all Purchase Orders. If the Purchase Orderss have mixed
         currencies, the field will be blank. */
       registerAttribute("amount", "amount", "TradePortalDecimalAttribute");


       /* po_upload_parameters - An XML document containing information about what the user wants to happen
         with the data they just uploaded. */
       registerAttribute("po_upload_parameters", "po_upload_parameters");


       /* a_upload_definition_oid - The PO upload definition that was used to upload this purchase order data. */
       registerAttribute("a_upload_definition_oid", "a_upload_definition_oid", "NumberAttribute");


       /* po_result_parameters -  */
       registerAttribute("po_result_parameters", "po_result_parameters");

       /* user_oid - The user who initiates the Purchase Order File Upload. */
       registerAttribute("user_oid", "a_user_oid", "NumberAttribute");

       /* owner_org_oid - The Corporate Customer who uploaded the file. */
       registerAttribute("owner_org_oid", "a_owner_org_oid", "NumberAttribute");
       
       /* datetime_process_start - Date/Time when the file processing started. */
       registerAttribute("datetime_process_start", "datetime_process_start", "DateTimeAttribute");
       /* total_process_seconds - The total time it takes the agent to process the invoice file. */
       registerAttribute("total_process_seconds", "total_process_seconds", "NumberAttribute");
       /* process_seconds - The time it takes the agent to validate the invoice file. */
       registerAttribute("process_seconds", "process_seconds", "NumberAttribute");
       /* system_name -  */
       registerAttribute("system_name", "system_name");
       /* server_name - */
       registerAttribute("server_name", "server_name");

   }
   
   




}
