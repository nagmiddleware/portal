package com.ams.tradeportal.busobj.webbean;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import javax.servlet.http.*;
import com.ams.tradeportal.html.*;

/*
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class TransactionWebBean extends TransactionWebBean_Base
{
    /**
     * Places the instrument type, transaction type, and component identifier into
     * the client-server data bridge.  
     *
     * @param componentIdentifier String - the component identifier
     */
    private void placeDataInCSDB(String componentIdentifier) 
     {
	   getClientServerDataBridge().setCSDBValue("instrument_type",this.getAttribute("copy_of_instr_type_code"));
	   getClientServerDataBridge().setCSDBValue("transaction_type",this.getAttribute("transaction_type_code"));
	   getClientServerDataBridge().setCSDBValue("standby_using_guarantee_form", this.getAttribute("standby_using_guarantee_form"));	   
	   getClientServerDataBridge().setCSDBValue("component_id",componentIdentifier);
     }
  
    /**
     * Registers the TermsWebBean that is the 'CustomerEnteredTerms' component
     * of this web bean.  The CSDB is passed to the component bean, allowing it
     * to dynamically register its attribute.
     *
     * @return TermsWebBean - the bean that was registered
     */  
    public TermsWebBean registerCustomerEnteredTerms()
     {
       if(!this.getAttribute("c_CustomerEnteredTerms").equals("")) 
        {        
            placeDataInCSDB("CustomerEnteredTerms"); 
      
            beanMgr.registerBean (TermsWebBean.class.getName(), "Terms", getClientServerDataBridge());
            TermsWebBean bean = (TermsWebBean)beanMgr.getBean("Terms");      
            bean.getById(this.getAttribute("c_CustomerEnteredTerms"));
            return bean;
        }
       else
        return null;       
     }
  
    /**
     * Registers the TermsWebBean that is the 'BankReleasedTerms' component
     * of this web bean.  The CSDB is passed to the component bean, allowing it
     * to dynamically register its attribute.
     *
     * @return TermsWebBean - the bean that was registered
     */       
    public TermsWebBean registerBankReleasedTerms()
     {
       if(!this.getAttribute("c_BankReleasedTerms").equals("")) 
        {
            placeDataInCSDB("BankReleasedTerms"); 

            beanMgr.registerBean (TermsWebBean.class.getName(), "Terms", getClientServerDataBridge());
            TermsWebBean bean = (TermsWebBean)beanMgr.getBean("Terms");      
            bean.getById(this.getAttribute("c_BankReleasedTerms"));
            return bean;
        }
       else
        return null;
     }

}
