

  

package com.ams.tradeportal.busobj.webbean;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import javax.servlet.http.*;

/**
 * Details for the various actions performed by admin user on non-integrated customer transaction
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class BankTransactionHistoryWebBean_Base extends TradePortalBusinessObjectWebBean
{

  public void registerAttributes()
   {  
      super.registerAttributes();


       /* bank_transaction_history_oid - Unique Identifier */
       registerAttribute("bank_transaction_history_oid", "bank_transaction_history_oid", "ObjectIDAttribute");

       /* action_datetime - Timestamp (in GMT) when the action is taken. */
       registerAttribute("action_datetime", "action_datetime", "DateTimeAttribute");

       /* action - Action. */
       registerAttribute("action", "action");

       /* transaction_status - Lifecycle status of a transaction */
       registerAttribute("bank_update_status", "bank_update_status");

      /* Pointer to the parent Transaction */
       registerAttribute("bank_update_transaction_oid", "p_bank_update_transaction_oid", "ParentIDAttribute");

       /* user_oid - The user who the updates the transaction. */
       registerAttribute("user_oid", "a_user_oid", "NumberAttribute");
       
       /* repair_reason - repair reason to sedn transaction for repair. */
       registerAttribute("repair_reason", "repair_reason");
       
       /*bank_transaction_status - REFDATA table Type - BANK_TRANSACTION_STATUS*/
	   registerAttribute("bank_transaction_status", "bank_transaction_status");
   }
   
   




}
