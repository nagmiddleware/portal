

  

package com.ams.tradeportal.busobj.webbean;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import javax.servlet.http.*;

/**
 * The rule that indicates which calendar is linked to each currency.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class CurrencyCalendarRuleWebBean_Base extends TradePortalBusinessObjectWebBean
{

  public void registerAttributes()
   {  
      super.registerAttributes();


       /* currency_calendar_rule_oid - Unique Identifier */
       registerAttribute("currency_calendar_rule_oid", "currency_calendar_rule_oid", "ObjectIDAttribute");


       /* opt_lock - Optimistic lock attribute
         See jPylon documentation for details on how this works */
       registerAttribute("opt_lock", "opt_lock", "OptimisticLockAttribute");


       /* rule_name - Rule Name */
       registerAttribute("rule_name", "rule_name");


       /* currency - Currency */
       registerAttribute("currency", "currency");

       /* tp_calendar_oid -  */
       registerAttribute("tp_calendar_oid", "a_tp_calendar_oid", "NumberAttribute");
   }
   
   




}
