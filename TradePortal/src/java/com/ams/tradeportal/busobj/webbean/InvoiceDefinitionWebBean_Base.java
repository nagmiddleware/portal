package com.ams.tradeportal.busobj.webbean;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import javax.servlet.http.*;

/**
 * Describes the mapping of an uploaded file to Invoice definition data stored
 * in the database.   The fields contained in the file are described, the file
 * format is specified, and the goods description format is provided.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class InvoiceDefinitionWebBean_Base extends TradePortalBusinessObjectWebBean {
		 
		/*
		 * Describes the mapping of an uploaded file to Invoice definiton.   
		 * The fields contained in the file are described, the file
		 * format is specified.
		 * 
		 * Can also be used to describe the process of manually entering purchase order
		 * data.  The fields are used to define the fields that the user will enter,
		 * the order in which they will appear and their format in goods description.
		 *
		 *     Copyright  � 2003                         
		 *     American Management Systems, Incorporated 
		 *     All rights reserved
		 */
		  
		  /* 
		   * Register the attributes and associations of the business object
		   */
	  public void registerAttributes()
	   {  
	      super.registerAttributes();

		      /* Register attributes defined in the Ancestor class */
		      super.registerAttributes();
		      
		      /* inv_upload_definition_oid - Unique identifier */
		      registerAttribute("inv_upload_definition_oid", "inv_upload_definition_oid", "ObjectIDAttribute");
		      
		      /* name - Name of the Invoice Definition Definition.  This will be used in dropdown lists when
		         choosing a definition. */
		      registerAttribute("name", "name");		      
		      /* description - Description of the definition for convenience. */
		      registerAttribute("description", "description");
		      
		      /* delimiter_char - Only relevant if using a delimited file format.   This attribute indicates
		         what character to use as the delimiter,comma, semi-colon,tab between fields in the uploaded file. */
		      registerAttribute("delimiter_char", "delimiter_char");
		      
		      /* date_format - describe the date format to be used for Invoice file upload. */
		      registerAttribute("date_format", "date_format");
		      
		      /* linked_instrument_ty_req - field representing the code value for the instrument to which the invoices should ultimately be associated:
		       *  REC (Receivables), PAY (Payables), ATP (Approval to Pay), LNR (Loan request) */
		      registerAttribute("linked_to_instrument_type_req", "linked_instrument_ty_req", "IndicatorAttribute");

		      /* incoterm_req - field representing the Incoterm associated with an underlying shipment */
		      registerAttribute("incoterm_req", "incoterm_req","IndicatorAttribute");
		      
		      /* country_of_loding_req - Country of Loading for the goods that the invoice is covering */
		      registerAttribute("country_of_loading_req", "country_of_loding_req","IndicatorAttribute");

		      /* country_of_discharge_req - Country of Discharge for the goods that the invoice is covering */
		      registerAttribute("country_of_discharge_req", "country_of_discharge_req","IndicatorAttribute");
		      
		      /* vessel_req - vessel shipping the goods that the invoice is covering */
		      registerAttribute("vessel_req", "vessel_req","IndicatorAttribute");
		      
		      
		      /* carrier_req - the carrier for the goods that the invoice is covering. */
		      registerAttribute("carrier_req", "carrier_req","IndicatorAttribute");
		      
		      /* actual_ship_date_req - The valid date format can be selected from the dropdown list*/
		      registerAttribute("actual_ship_date_req", "actual_ship_date_req", "IndicatorAttribute");
		      
		      /* payment_date_req - valid date format can be selected from the dropdown list. */
		      registerAttribute("payment_date_req", "payment_date_req", "IndicatorAttribute");
		      
		      /* purchase_ord_id_req - field representing the Purchase Order Id against which the invoice is shipping. */
		      registerAttribute("purchase_order_id_req", "purchase_ord_id_req", "IndicatorAttribute");

		      /* goods_description_req - field representing the goods description for the invoice goods. */
		      registerAttribute("goods_description_req", "goods_description_req", "IndicatorAttribute");
		      
		       /* part_to_validate - This is a  local attribute, meaning that it is not stored in the database.
		         It stores the tab that the user is editing on the PO Upload Definition detail
		         page.   Based on what tab is being edited, different validations occur. */
		       registerAttribute("part_to_validate", "part_to_validate", "LocalAttribute");
		      
		      
		      /* opt_lock - Optimistic lock attribute
		         See jPylon documentation for details on how this works */
		      registerAttribute("opt_lock", "opt_lock", "OptimisticLockAttribute");
		      
		      /* Buyer User defined 1 to 10- Buyer User Defined Fields for which customer specific labels 
		        are defined and for which 140 A/N characters of field content can be captured during Invocie upload */
		      registerAttribute("buyer_users_def1_label", "buyer_users_def1_label");
		      registerAttribute("buyer_users_def2_label", "buyer_users_def2_label");
		      registerAttribute("buyer_users_def3_label", "buyer_users_def3_label");
		      registerAttribute("buyer_users_def4_label", "buyer_users_def4_label");
		      registerAttribute("buyer_users_def5_label", "buyer_users_def5_label");
		      registerAttribute("buyer_users_def6_label", "buyer_users_def6_label");
		      registerAttribute("buyer_users_def7_label", "buyer_users_def7_label");
		      registerAttribute("buyer_users_def8_label", "buyer_users_def8_label");
		      registerAttribute("buyer_users_def9_label", "buyer_users_def9_label");
		      registerAttribute("buyer_users_def10_label", "buyer_users_def10_label");
		      
		      /* Buyer User defined required 1 to 10- if it is seledted, then user defined field must be present in
		        *Invoice file summary order */
		      registerAttribute("buyer_users_def1_req", "buyer_users_def1_req", "IndicatorAttribute");
		      registerAttribute("buyer_users_def2_req", "buyer_users_def2_req", "IndicatorAttribute");
		      registerAttribute("buyer_users_def3_req", "buyer_users_def3_req", "IndicatorAttribute");
		      registerAttribute("buyer_users_def4_req", "buyer_users_def4_req", "IndicatorAttribute");
		      registerAttribute("buyer_users_def5_req", "buyer_users_def5_req", "IndicatorAttribute");
		      registerAttribute("buyer_users_def6_req", "buyer_users_def6_req", "IndicatorAttribute");
		      registerAttribute("buyer_users_def7_req", "buyer_users_def7_req", "IndicatorAttribute");
		      registerAttribute("buyer_users_def8_req", "buyer_users_def8_req", "IndicatorAttribute");
		      registerAttribute("buyer_users_def9_req", "buyer_users_def9_req", "IndicatorAttribute");
		      registerAttribute("buyer_users_def10_req", "buyer_users_def10_req", "IndicatorAttribute");
		      
		      /* Seller User defined 1 to 10- Seller User Defined Fields for which customer specific labels 
		        are defined and for which 140 A/N characters of field content can be captured during Invocie upload */		      		      
		      registerAttribute("seller_users_def1_label", "seller_users_def1_label");
		      registerAttribute("seller_users_def2_label", "seller_users_def2_label");
		      registerAttribute("seller_users_def3_label", "seller_users_def3_label");
		      registerAttribute("seller_users_def4_label", "seller_users_def4_label");
		      registerAttribute("seller_users_def5_label", "seller_users_def5_label");
		      registerAttribute("seller_users_def6_label", "seller_users_def6_label");
		      registerAttribute("seller_users_def7_label", "seller_users_def7_label");
		      registerAttribute("seller_users_def8_label", "seller_users_def8_label");
		      registerAttribute("seller_users_def9_label", "seller_users_def9_label");
		      registerAttribute("seller_users_def10_label", "seller_users_def10_label");
		      
		      /* Seller User defined required 1 to 10- if it is seledted, then user defined field must be present in
		        *Line Item Detail order */
		      registerAttribute("seller_users_def1_req", "seller_users_def1_req", "IndicatorAttribute");
		      registerAttribute("seller_users_def2_req", "seller_users_def2_req", "IndicatorAttribute");
		      registerAttribute("seller_users_def3_req", "seller_users_def3_req", "IndicatorAttribute");
		      registerAttribute("seller_users_def4_req", "seller_users_def4_req", "IndicatorAttribute");
		      registerAttribute("seller_users_def5_req", "seller_users_def5_req", "IndicatorAttribute");
		      registerAttribute("seller_users_def6_req", "seller_users_def6_req", "IndicatorAttribute");
		      registerAttribute("seller_users_def7_req", "seller_users_def7_req", "IndicatorAttribute");
		      registerAttribute("seller_users_def8_req", "seller_users_def8_req", "IndicatorAttribute");
		      registerAttribute("seller_users_def9_req", "seller_users_def9_req", "IndicatorAttribute");
		      registerAttribute("seller_users_def10_req", "seller_users_def10_req", "IndicatorAttribute");
		      
		      /* line_item_id_req - captures the line item ID at the line item details level. */
		      registerAttribute("line_item_id_req", "line_item_id_req", "IndicatorAttribute");
		      
		      /* unit_price_req - captures the individual unit price of the item being shipped under the invoice. */
		      registerAttribute("unit_price_req", "unit_price_req", "IndicatorAttribute");
		      
		      /* unit_of_measure_price_req - This is the unit of measure applicable to the price. */
		      registerAttribute("unit_of_measure_price_req", "unit_of_measure_price_req", "IndicatorAttribute");
		      
		      /* inv_quantity_req - captures the invoiced quantity of the item being shipped under the invoice. */
		      registerAttribute("inv_quantity_req", "inv_quantity_req", "IndicatorAttribute");
		      
		      /* unit_of_measure_quantity_req - This is the unit of measure applicable to the quantity. */
		      registerAttribute("unit_of_measure_quantity_req", "unit_of_measure_quantity_req", "IndicatorAttribute");
		      
		      /* Product Char UD Type 1 to 7 label- There are 7 user defined product characteristics fields in the PPM (and the TSU Invoice format). 
		       * They can essentially be completely user-defined by the Customer/User.  
		       * The user has the ability to define the �Type� of the characteristic by entering a specific label for the typ */		      
		      registerAttribute("prod_chars_ud1_type", "prod_chars_ud1_type");
		      registerAttribute("prod_chars_ud2_type", "prod_chars_ud2_type");
		      registerAttribute("prod_chars_ud3_type", "prod_chars_ud3_type");
		      registerAttribute("prod_chars_ud4_type", "prod_chars_ud4_type");
		      registerAttribute("prod_chars_ud5_type", "prod_chars_ud5_type");
		      registerAttribute("prod_chars_ud6_type", "prod_chars_ud6_type");
		      registerAttribute("prod_chars_ud7_type", "prod_chars_ud7_type");
		      		      
		      /* Product Char UD Type 1 to 7 label required- if selected , the label name is required */ 
		      registerAttribute("prod_chars_ud1_type_req", "prod_chars_ud1_type_req", "IndicatorAttribute");
		      registerAttribute("prod_chars_ud2_type_req", "prod_chars_ud2_type_req", "IndicatorAttribute");
		      registerAttribute("prod_chars_ud3_type_req", "prod_chars_ud3_type_req", "IndicatorAttribute");
		      registerAttribute("prod_chars_ud4_type_req", "prod_chars_ud4_type_req", "IndicatorAttribute");
		      registerAttribute("prod_chars_ud5_type_req", "prod_chars_ud5_type_req", "IndicatorAttribute");
		      registerAttribute("prod_chars_ud6_type_req", "prod_chars_ud6_type_req", "IndicatorAttribute");
		      registerAttribute("prod_chars_ud7_type_req", "prod_chars_ud7_type_req", "IndicatorAttribute");
		      		      
		      /* Invoice Summary Order 1 to 34 define the order of invoice data in which format invoice file 
		       * will be uploaded */
		      registerAttribute("inv_summary_order1", "inv_summary_order1");
		      registerAttribute("inv_summary_order2", "inv_summary_order2");
		      registerAttribute("inv_summary_order3", "inv_summary_order3");
		      registerAttribute("inv_summary_order4", "inv_summary_order4");
		      registerAttribute("inv_summary_order5", "inv_summary_order5");
		      registerAttribute("inv_summary_order6", "inv_summary_order6");
		      registerAttribute("inv_summary_order7", "inv_summary_order7");
		      registerAttribute("inv_summary_order8", "inv_summary_order8");
		      registerAttribute("inv_summary_order9", "inv_summary_order9");
		      registerAttribute("inv_summary_order10", "inv_summary_order10");  // Srinivasu_D Rel-8.1.1 IR# T36000005217 09/17/2012 
		       // RKAZI Rel 8.2 IR# T36000012087 02/28/2013 START
		       registerAttribute("inv_summary_order11", "inv_summary_order11"); 
		       registerAttribute("inv_summary_order12", "inv_summary_order12"); 
		       registerAttribute("inv_summary_order13", "inv_summary_order13"); 
		       registerAttribute("inv_summary_order14", "inv_summary_order14"); 
		       registerAttribute("inv_summary_order15", "inv_summary_order15"); 
		       registerAttribute("inv_summary_order16", "inv_summary_order16"); 
		       registerAttribute("inv_summary_order17", "inv_summary_order17"); 
		       registerAttribute("inv_summary_order18", "inv_summary_order18"); 
		       registerAttribute("inv_summary_order19", "inv_summary_order19"); 
		       registerAttribute("inv_summary_order20", "inv_summary_order20"); 
		       registerAttribute("inv_summary_order21", "inv_summary_order21"); 
		       registerAttribute("inv_summary_order22", "inv_summary_order22"); 
		       registerAttribute("inv_summary_order23", "inv_summary_order23"); 
		       registerAttribute("inv_summary_order24", "inv_summary_order24"); 
		       registerAttribute("inv_summary_order25", "inv_summary_order25"); 
		       registerAttribute("inv_summary_order26", "inv_summary_order26"); 
		       registerAttribute("inv_summary_order27", "inv_summary_order27"); 
		       registerAttribute("inv_summary_order28", "inv_summary_order28"); 
		       // RKAZI Rel 8.2 IR# T36000012087 02/28/2013 END
		       // Narayan CR913 Rel9.0 30-Jan-2014 Begin
		       registerAttribute("inv_summary_order29", "inv_summary_order29"); 
		       registerAttribute("inv_summary_order30", "inv_summary_order30");
		       registerAttribute("inv_summary_order31", "inv_summary_order31");
		       // Narayan CR913 Rel9.0 30-Jan-2014 End
			   //Rel 9.0 - IR 29896 Start
			   registerAttribute("inv_summary_order32", "inv_summary_order32");
			   //Rel 9.0 IR 29896 End 			      
			   //Rel 9.0 - IR 30279 Portal changes Start
			   registerAttribute("inv_summary_order33", "inv_summary_order33");
			   //Rel 9.0 IR 30279 End 
			   // Narayan CR914 Rel9.2 03-Nov-2014 Add- Begin
			   registerAttribute("inv_summary_order34", "inv_summary_order34");
			   // Narayan CR914 Rel9.2 03-Nov-2014 Add- End
			   
		      /* Invoice Goods Order 1-28-  define the order of invoice order in which format invoice file 
		       * will be uploaded */
		      registerAttribute("inv_goods_order1", "inv_goods_order1");
		      registerAttribute("inv_goods_order2", "inv_goods_order2");
		      registerAttribute("inv_goods_order3", "inv_goods_order3");
		      registerAttribute("inv_goods_order4", "inv_goods_order4");
		      registerAttribute("inv_goods_order5", "inv_goods_order5");
		      registerAttribute("inv_goods_order6", "inv_goods_order6");
		      registerAttribute("inv_goods_order7", "inv_goods_order7");
		      registerAttribute("inv_goods_order8", "inv_goods_order8");
		      registerAttribute("inv_goods_order9", "inv_goods_order9");
		      registerAttribute("inv_goods_order10", "inv_goods_order10");
		      registerAttribute("inv_goods_order11", "inv_goods_order11");
		      registerAttribute("inv_goods_order12", "inv_goods_order12");
		      registerAttribute("inv_goods_order13", "inv_goods_order13");
		      registerAttribute("inv_goods_order14", "inv_goods_order14");
		      registerAttribute("inv_goods_order15", "inv_goods_order15");
		      registerAttribute("inv_goods_order16", "inv_goods_order16");
		      registerAttribute("inv_goods_order17", "inv_goods_order17");
		      registerAttribute("inv_goods_order18", "inv_goods_order18");
		      registerAttribute("inv_goods_order19", "inv_goods_order19");
		      registerAttribute("inv_goods_order20", "inv_goods_order20");
		      registerAttribute("inv_goods_order21", "inv_goods_order21");
		      registerAttribute("inv_goods_order22", "inv_goods_order22");
		      registerAttribute("inv_goods_order23", "inv_goods_order23");
		      registerAttribute("inv_goods_order24", "inv_goods_order24");
		      registerAttribute("inv_goods_order25", "inv_goods_order25");
		      registerAttribute("inv_goods_order26", "inv_goods_order26");
		      registerAttribute("inv_goods_order27", "inv_goods_order27");
		      registerAttribute("inv_goods_order28", "inv_goods_order28");		      
		     // registerAttribute("inv_goods_order29", "inv_goods_order29"); // Srinivasu_D Rel-8.1.1 IR# T36000005217 09/17/2012 
		      /* line_item_detail_provided - If this file definition does not need line item details to be captured, the user must consciously select this indicator. 
		       * If this indicator is selected, then none of the Invoice Line Item Detail field names can be selected. */		      
		      registerAttribute("line_item_detail_provided", "line_item_detail_provided", "IndicatorAttribute");
		      
		      /* Invoice Line Item Order 1- 12 - describe the order in which format invoice file will be
		       * having invoice data. */
		      registerAttribute("inv_line_item_order1", "inv_line_item_order1");
		      registerAttribute("inv_line_item_order2", "inv_line_item_order2");
		      registerAttribute("inv_line_item_order3", "inv_line_item_order3");
		      registerAttribute("inv_line_item_order4", "inv_line_item_order4");
		      registerAttribute("inv_line_item_order5", "inv_line_item_order5");
		      registerAttribute("inv_line_item_order6", "inv_line_item_order6");
		      registerAttribute("inv_line_item_order7", "inv_line_item_order7");
		      registerAttribute("inv_line_item_order8", "inv_line_item_order8");
		      registerAttribute("inv_line_item_order9", "inv_line_item_order9");
		      registerAttribute("inv_line_item_order10", "inv_line_item_order10");
		      registerAttribute("inv_line_item_order11", "inv_line_item_order11");
		      registerAttribute("inv_line_item_order12", "inv_line_item_order12");
		      
		      //
		      //AAlubala - Rel8.2 CR741 - Credit Note Order on Invoice Definition - BEGIN
		      registerAttribute("inv_credit_order1", "inv_credit_order1");
		      registerAttribute("inv_credit_order2", "inv_credit_order2");
		      registerAttribute("inv_credit_order3", "inv_credit_order3");	
		      /* Discount Code*/
		      registerAttribute("discount_code_req", "discount_code_req", "IndicatorAttribute");
		      /* Discount GL Code*/
		      registerAttribute("discount_gl_code_req", "discount_gl_code_req", "IndicatorAttribute");
		      /* Discount Comments*/
		      registerAttribute("discount_comments_req", "discount_comments_req", "IndicatorAttribute");	
		      /* Discount Code data required*/
		      registerAttribute("discount_code_data_req", "discount_code_data_req", "IndicatorAttribute");
		      /* Discount GL Code data required*/
		      registerAttribute("discount_gl_code_data_req", "discount_gl_code_data_req", "IndicatorAttribute");
		      /* Discount Comments data required*/
		      registerAttribute("discount_comments_data_req", "discount_comments_data_req", "IndicatorAttribute");		      
		      //
		      //CR741 - END
		      
              // Nar IR-NNUM022937252 03/24/2012 Begin
		       /* include_column_headers - Indicates whether Invoice file will have column headings. */
		       registerAttribute("include_column_headers", "include_column_headers", "IndicatorAttribute");
             // Nar IR-NNUM022937252 03/24/2012 End
			 
			  //count of invoice summary fields
			  registerAttribute("inv_summary_count", "inv_summary_count", "LocalAttribute");
			   //count of invoice goods fields
			  registerAttribute("inv_summary_goods_count", "inv_summary_goods_count", "LocalAttribute"); 
			  //count of invoice line item fields
			  registerAttribute("inv_line_item_count", "inv_line_item_count", "LocalAttribute"); 
			  
		      /* owner_org_oid - A PO upload definition is "owned" by an organization.  This component relationship
		         represents the ownership. */
		      registerAttribute("owner_org_oid", "a_owner_org_oid", "NumberAttribute");
		    //Srinivasu_D CR-708 Rel8.1.1 05/30/2012 Start    
			  registerAttribute("invoice_type_indicator",		 "invoice_type_indicator");
			  registerAttribute("linked_to_instrument_type_data_req", "linked_instrument_ty_data_req",		"IndicatorAttribute");
		      registerAttribute("invoice_type_req",					"invoice_type_req",				    "IndicatorAttribute");
		      registerAttribute("invoice_type_data_req",		 "invoice_type_data_req",			    "IndicatorAttribute");
		      registerAttribute("goods_description_data_req",	 "goods_description_data_req",		    "IndicatorAttribute");
		      registerAttribute("incoterm_data_req",			 "incoterm_data_req",				    "IndicatorAttribute");
		      registerAttribute("country_of_loading_data_req",	 "country_of_loding_data_req",		    "IndicatorAttribute");
		      registerAttribute("country_of_discharge_data_req", "country_of_discharge_data_req",		"IndicatorAttribute");
		      registerAttribute("vessel_data_req",				 "vessel_data_req",						"IndicatorAttribute");
		      registerAttribute("carrier_data_req",				 "carrier_data_req",					"IndicatorAttribute");
		      registerAttribute("actual_ship_date_data_req",	 "actual_ship_date_data_req",			"IndicatorAttribute");
		      registerAttribute("payment_date_data_req",		 "payment_date_data_req",				"IndicatorAttribute");
			  registerAttribute("purchase_order_id_data_req",		 "purchase_ord_id_data_req",			"IndicatorAttribute");
			  registerAttribute("seller_users_def1_label_data_req", "seller_users_def1_data_req", "IndicatorAttribute");
			  registerAttribute("seller_users_def2_label_data_req", "seller_users_def2_data_req", "IndicatorAttribute");
			  registerAttribute("seller_users_def3_label_data_req", "seller_users_def3_data_req", "IndicatorAttribute");
			  registerAttribute("seller_users_def4_label_data_req", "seller_users_def4_data_req", "IndicatorAttribute");
			  registerAttribute("seller_users_def5_label_data_req", "seller_users_def5_data_req", "IndicatorAttribute");
			  registerAttribute("seller_users_def6_label_data_req", "seller_users_def6_data_req", "IndicatorAttribute");
			  registerAttribute("seller_users_def7_label_data_req", "seller_users_def7_data_req", "IndicatorAttribute");
			  registerAttribute("seller_users_def8_label_data_req", "seller_users_def8_data_req", "IndicatorAttribute");
			  registerAttribute("seller_users_def9_label_data_req", "seller_users_def9_data_req", "IndicatorAttribute");
			  registerAttribute("seller_users_def10_label_data_req", "seller_users_def10_data_req", "IndicatorAttribute");

			  registerAttribute("buyer_users_def1_label_data_req", "buyer_users_def1_data_req", "IndicatorAttribute");
			  registerAttribute("buyer_users_def2_label_data_req", "buyer_users_def2_data_req", "IndicatorAttribute");
			  registerAttribute("buyer_users_def3_label_data_req", "buyer_users_def3_data_req", "IndicatorAttribute");
			  registerAttribute("buyer_users_def4_label_data_req", "buyer_users_def4_data_req", "IndicatorAttribute");
			  registerAttribute("buyer_users_def5_label_data_req", "buyer_users_def5_data_req", "IndicatorAttribute");
			  registerAttribute("buyer_users_def6_label_data_req", "buyer_users_def6_data_req", "IndicatorAttribute");
			  registerAttribute("buyer_users_def7_label_data_req", "buyer_users_def7_data_req", "IndicatorAttribute");
			  registerAttribute("buyer_users_def8_label_data_req", "buyer_users_def8_data_req", "IndicatorAttribute");
			  registerAttribute("buyer_users_def9_label_data_req", "buyer_users_def9_data_req", "IndicatorAttribute");
			  registerAttribute("buyer_users_def10_label_data_req", "buyer_users_def10_data_req", "IndicatorAttribute");

			  registerAttribute("prod_chars_ud1_type_data_req", "prod_chars_ud1_type_data_req", "IndicatorAttribute");
			  registerAttribute("prod_chars_ud2_type_data_req", "prod_chars_ud2_type_data_req", "IndicatorAttribute");
			  registerAttribute("prod_chars_ud3_type_data_req", "prod_chars_ud3_type_data_req", "IndicatorAttribute");
			  registerAttribute("prod_chars_ud4_type_data_req", "prod_chars_ud4_type_data_req", "IndicatorAttribute");
			  registerAttribute("prod_chars_ud5_type_data_req", "prod_chars_ud5_type_data_req", "IndicatorAttribute");
			  registerAttribute("prod_chars_ud6_type_data_req", "prod_chars_ud6_type_data_req", "IndicatorAttribute");
			  registerAttribute("prod_chars_ud7_type_data_req", "prod_chars_ud7_type_data_req", "IndicatorAttribute");

		      registerAttribute("line_item_id_data_req", "line_item_id_data_req", "IndicatorAttribute");
		      registerAttribute("unit_price_data_req", "unit_price_data_req", "IndicatorAttribute");
		      registerAttribute("unit_of_measure_price_data_req", "unit_measure_price_data_req", "IndicatorAttribute");
		      registerAttribute("inv_quantity_data_req", "inv_quantity_data_req", "IndicatorAttribute");
		      registerAttribute("unit_of_measure_quantity_data_req", "unit_measure_qnty_data_req", "IndicatorAttribute");

			  //Srinivasu_D CR-708 Rel8.1.1 05/30/2012 End
		      registerAttribute("buyer_name_req", "buyer_name_req","IndicatorAttribute");
		      registerAttribute("buyer_id_req", "buyer_id_req","IndicatorAttribute");
		      registerAttribute("seller_name_req", "seller_name_req","IndicatorAttribute");
		      registerAttribute("seller_id_req", "seller_id_req","IndicatorAttribute");
		      // 	DK CR709 Rel8.2 10/31/2012 BEGINS
		      registerAttribute("credit_note_req", "credit_note_req", "IndicatorAttribute");
		      registerAttribute("credit_note_data_req", "credit_note_data_req", "IndicatorAttribute");
		      registerAttribute("credit_note_ind_text", "credit_note_ind_text");		      
		      registerAttribute("pay_method_req", "pay_method_req", "IndicatorAttribute");
		      registerAttribute("ben_acct_num_req", "ben_acct_num_req", "IndicatorAttribute");
		      registerAttribute("ben_add1_req", "ben_add1_req", "IndicatorAttribute");
		      registerAttribute("ben_add2_req", "ben_add2_req", "IndicatorAttribute");
		      registerAttribute("ben_add3_req", "ben_add3_req", "IndicatorAttribute");
		      registerAttribute("ben_country_req", "ben_country_req", "IndicatorAttribute");
		      registerAttribute("ben_bank_name_req", "ben_bank_name_req", "IndicatorAttribute");
		      registerAttribute("ben_branch_code_req", "ben_branch_code_req", "IndicatorAttribute");
		      registerAttribute("ben_branch_add1_req", "ben_branch_add1_req", "IndicatorAttribute");
		      registerAttribute("ben_branch_add2_req", "ben_branch_add2_req", "IndicatorAttribute");
		      registerAttribute("ben_bank_city_req", "ben_bank_city_req", "IndicatorAttribute");
		      registerAttribute("ben_bank_province_req", "ben_bank_province_req", "IndicatorAttribute");
		      registerAttribute("ben_branch_country_req", "ben_branch_country_req", "IndicatorAttribute");
		      registerAttribute("charges_req", "charges_req", "IndicatorAttribute");
		      registerAttribute("central_bank_rep1_req", "central_bank_rep1_req", "IndicatorAttribute");
		      registerAttribute("central_bank_rep2_req", "central_bank_rep2_req", "IndicatorAttribute");
		      registerAttribute("central_bank_rep3_req", "central_bank_rep3_req", "IndicatorAttribute");		      
		      registerAttribute("pay_method_data_req", "pay_method_data_req", "IndicatorAttribute");
		      registerAttribute("ben_acct_num_data_req", "ben_acct_num_data_req", "IndicatorAttribute");
		      registerAttribute("ben_add1_data_req", "ben_add1_data_req", "IndicatorAttribute");
		      registerAttribute("ben_add2_data_req", "ben_add2_data_req", "IndicatorAttribute");
		      registerAttribute("ben_add3_data_req", "ben_add3_data_req", "IndicatorAttribute");
		      registerAttribute("ben_country_data_req", "ben_country_data_req", "IndicatorAttribute");
		      registerAttribute("ben_bank_name_data_req", "ben_bank_name_data_req", "IndicatorAttribute");
		      registerAttribute("ben_branch_code_data_req", "ben_branch_code_data_req", "IndicatorAttribute");
		      registerAttribute("ben_branch_add1_data_req", "ben_branch_add1_data_req", "IndicatorAttribute");
		      registerAttribute("ben_branch_add2_data_req", "ben_branch_add2_data_req", "IndicatorAttribute");
		      registerAttribute("ben_bank_city_data_req", "ben_bank_city_data_req", "IndicatorAttribute");
		      registerAttribute("ben_bank_province_data_req", "ben_bank_province_data_req", "IndicatorAttribute");
		      registerAttribute("ben_branch_country_data_req", "ben_branch_country_data_req", "IndicatorAttribute");
		      registerAttribute("charges_data_req", "charges_data_req", "IndicatorAttribute");
		      registerAttribute("central_bank_rep1_data_req", "central_bank_rep1_data_req", "IndicatorAttribute");
		      registerAttribute("central_bank_rep2_data_req", "central_bank_rep2_data_req", "IndicatorAttribute");
		      registerAttribute("central_bank_rep3_data_req", "central_bank_rep3_data_req", "IndicatorAttribute");	
			  // DK CR709 Rel8.2 10/31/2012 ENDS
		      // Narayan CR913 Rel9.0 30-Jan-2014 Begin
		      registerAttribute("send_to_supplier_date_req", "send_to_supplier_date_req", "IndicatorAttribute");
		      registerAttribute("send_to_supplier_date_data_req", "send_to_supplier_date_data_req", "IndicatorAttribute");
		      registerAttribute("buyer_acct_currency_req", "buyer_acct_currency_req", "IndicatorAttribute");
		      registerAttribute("buyer_acct_currency_data_req", "buyer_acct_currency_data_req", "IndicatorAttribute");
		      registerAttribute("buyer_acct_num_req", "buyer_acct_num_req", "IndicatorAttribute");
		      registerAttribute("buyer_acct_num_data_req", "buyer_acct_num_data_req", "IndicatorAttribute");
		      // Narayan CR913 Rel9.0 30-Jan-2014 End		      
		      //Rel 9.0 - IR 29896 Start
		      registerAttribute("ben_email_addr_req", "ben_email_addr_req", "IndicatorAttribute");
		      registerAttribute("ben_email_addr_data_req", "ben_email_addr_data_req", "IndicatorAttribute");
		      //Rel 9.0 IR 29896 End 		      
		      //Rel 9.0 - IR 30279 Portal changes Start
		      registerAttribute("ben_bank_sort_code_req", "ben_bank_sort_code_req", "IndicatorAttribute");
		      registerAttribute("ben_bank_sort_code_data_req", "ben_bank_sort_code_data_req", "IndicatorAttribute");
		      //Rel 9.0 IR 30279 End 
		      // Narayan CR914 Rel9.2 03-Nov-2014 Add- Begin
		      registerAttribute("end_to_end_id_req", "end_to_end_id_req", "IndicatorAttribute");
		      registerAttribute("end_to_end_id_data_req", "end_to_end_id_data_req", "IndicatorAttribute");
		      // Narayan CR914 Rel9.2 03-Nov-2014 Add- End
		      // CR1001 Rel9.4 Add- Begin
		      registerAttribute("reporting_code_1_req", "reporting_code_1_req", "IndicatorAttribute");
		      registerAttribute("reporting_code_1_data_req", "reporting_code_1_data_req", "IndicatorAttribute");
		      registerAttribute("reporting_code_2_req", "reporting_code_2_req", "IndicatorAttribute");
		      registerAttribute("reporting_code_2_data_req", "reporting_code_2_data_req", "IndicatorAttribute");
		      // CR1001 Rel9.4 Add- End
		      
			 }
		      
		   }
		   		   
	

