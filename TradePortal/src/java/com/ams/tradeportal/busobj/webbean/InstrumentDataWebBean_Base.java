

  

package com.ams.tradeportal.busobj.webbean;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import javax.servlet.http.*;

/**
 * Templates and instruments are very similar in data, and are stored in the
 * same database tables.
 * 
 * This is an abstract business object that is the superclass for both types
 * of instruments: templates and real instruments.   This business object contains
 * associations and attributes that are common to both templates and instruments.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class InstrumentDataWebBean_Base extends TradePortalBusinessObjectWebBean
{

  public void registerAttributes()
   {  
      super.registerAttributes();


       /* instrument_type_code - Code for the instrument type */
       registerAttribute("instrument_type_code", "instrument_type_code");


       /* template_flag - Indicates whether or not the row in the INSTRUMENT table represents a template
         or a real instrument.   Yes=Template */
       registerAttribute("template_flag", "template_flag", "IndicatorAttribute");


       /* instrument_oid - Unique identifier */
       registerAttribute("instrument_oid", "instrument_oid", "ObjectIDAttribute");


       /* a_counter_party_oid - A pointer to the counterparty of this instrument or template.  Contains
         an OID from the TermsParty business object.
         
         This is an attribute and not a real jPylon association because the possibility
         exists that a terms party can be created at the same time as an InstrumentData
         entry.  In that situation, the association check would always fail. */
       registerAttribute("a_counter_party_oid", "a_counter_party_oid", "NumberAttribute");


       /* original_transaction_oid - A pointer to the first transaction created for this instrument or template.
         For templates, this will be the only transaction.
         
         This is an attribute and not a real jPylon association because the possibility
         exists that a transaction can be created at the same time as an InstrumentData
         entry.  In that situation, the association check would always fail. */
       registerAttribute("original_transaction_oid", "original_transaction_oid", "NumberAttribute");


       /* language - The language in which the instrument (or the instrument resulting from this
         template) will be issued. */
       registerAttribute("language", "language");


       /* import_indicator - Whether a Loan Request (LRQ) is for import.  'Y' for import and 'N' for
         export. */
       registerAttribute("import_indicator", "import_indicator", "IndicatorAttribute");
	
	
   }
   
   




}
