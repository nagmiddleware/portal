



package com.ams.tradeportal.busobj.webbean;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import javax.servlet.http.*;

/**
 * Interest Discount Rate Class, Which will ultimately allow the Corporate
 * Customers to view Discount/Interest rates that will be applied on financed
 * invoices.
 *
 *     Copyright  � 2003
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class InterestDiscountRateWebBean_Base extends TradePortalBusinessObjectWebBean
{

  public void registerAttributes()
   {


       /* interest_discount_rate_oid - Interest_discount_rate_oid - Unique Identifier */
       registerAttribute("interest_discount_rate_oid", "interest_discount_rate_oid", "ObjectIDAttribute");


       /* currency_code - The currency code for which the Interest Discount Rate is being established. */
       registerAttribute("currency_code", "currency_code");


       /* rate_type - The rate type for which the Interest Discount Rate is being established. */
       registerAttribute("rate_type", "rate_type");


       /* call_rate - The Call Rate */
       registerAttribute("call_rate", "call_rate", "TradePortalDecimalAttribute");


       /* rate_30_days -  */
       registerAttribute("rate_30_days", "rate_30_days", "TradePortalDecimalAttribute");


       /* rate_60_days -  */
       registerAttribute("rate_60_days", "rate_60_days", "TradePortalDecimalAttribute");


       /* rate_90_days -  */
       registerAttribute("rate_90_days", "rate_90_days", "TradePortalDecimalAttribute");


       /* rate_120_days -  */
       registerAttribute("rate_120_days", "rate_120_days", "TradePortalDecimalAttribute");


       /* rate_180_days -  */
       registerAttribute("rate_180_days", "rate_180_days", "TradePortalDecimalAttribute");


       /* rate_360_days -  */
       registerAttribute("rate_360_days", "rate_360_days", "TradePortalDecimalAttribute");


       /* num_interest_days - Number of interest days of currency. */
       registerAttribute("num_interest_days", "num_interest_days", "NumberAttribute");


       /* effective_date - Effective of Interest Discount Rate in YYYYMMDD format. */
       registerAttribute("effective_date", "effective_date", "DateTimeAttribute");


       /* opt_lock - Optimistic lock attribute
         See jPylon documentation for details on how this works */
       registerAttribute("opt_lock", "opt_lock", "OptimisticLockAttribute");

      /* Pointer to the parent InterestDiscountRateGroup */
       registerAttribute("interest_disc_rate_group_oid", "p_interest_disc_rate_group_oid", "ParentIDAttribute");
   }






}
