package com.ams.tradeportal.busobj.webbean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.busobj.termsmgr.TermsMgr;
import com.ams.util.ConvenienceServices;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.StringFunction;

/*
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class TermsWebBean extends TermsWebBean_Base
{
	private static final Logger LOG = LoggerFactory.getLogger(TermsWebBean.class);
   // Manager for this object.  Responsibility is delegated to this object
   // for dynamic attribute registration based on instrument type and transaction
   // type.  Manager will be a pointer to an instance of a subclass of TermsMgr.
   TermsMgr manager; 

   ShipmentTermsWebBean firstShipment;
   
   public TermsWebBean sourceTermsWebBean = null;

   // Commented this out temporarily.  For now, attributes are not dynamically created.
   public void registerDynamicAttributes()
   {  
    
      // These attributes always exist
      registerAttribute("terms_oid", "terms_oid", "ObjectIDAttribute");
      registerAttribute("terms_source_type", "terms_source_type");
      registerAttribute("first_shipment_oid", "first_shipment_oid");
      registerAttribute("place_of_expiry", "place_of_expiry");
      registerAttribute("related_instrument_id", "related_instrument_id");
      registerAttribute("additional_conditions", "additional_conditions");
      registerAttribute("special_bank_instructions", "special_bank_instructions");
      registerAttribute("coms_chrgs_other_text", "coms_chrgs_other_text");
      registerAttribute("our_reference", "our_reference");
      registerAttribute("apply_payment_on_date", "apply_payment_on_date");
      registerAttribute("pay_in_full_fin_roll_ind", "pay_in_full_fin_roll_ind");
      registerAttribute("account_remit_ind", "account_remit_ind");
      registerAttribute("fin_roll_full_partial_ind", "fin_roll_full_partial_ind");
      registerAttribute("principal_account_oid", "principal_account_oid");
      registerAttribute("charges_account_oid", "charges_account_oid");
      registerAttribute("interest_account_oid", "interest_account_oid");
      registerAttribute("rollover_terms_ind", "rollover_terms_ind");
      registerAttribute("fin_roll_number_of_days", "fin_roll_number_of_days");
      registerAttribute("fin_roll_curr", "fin_roll_curr");
      registerAttribute("fin_roll_maturity_date", "fin_roll_maturity_date");
      registerAttribute("fin_roll_partial_pay_amt", "fin_roll_partial_pay_amt");
      registerAttribute("other_addl_instructions_ind", "other_addl_instructions_ind");
      registerAttribute("other_addl_instructions", "other_addl_instructions");
      registerAttribute("daily_rate_fec_oth_ind", "daily_rate_fec_oth_ind");
      registerAttribute("settle_covered_by_fec_num", "settle_covered_by_fec_num");
      registerAttribute("settle_fec_rate", "settle_fec_rate");
      registerAttribute("settle_fec_currency", "settle_fec_currency");
      registerAttribute("settle_other_fec_text", "settle_other_fec_text");

      registerAttribute("c_FirstTermsParty", "c_FIRST_TERMS_PARTY_OID", "NumberAttribute");
      registerAttribute("c_SecondTermsParty", "c_SECOND_TERMS_PARTY_OID", "NumberAttribute");
      registerAttribute("c_ThirdTermsParty", "c_THIRD_TERMS_PARTY_OID", "NumberAttribute");
      registerAttribute("c_FourthTermsParty", "c_FOURTH_TERMS_PARTY_OID", "NumberAttribute");
      registerAttribute("c_FifthTermsParty", "c_FIFTH_TERMS_PARTY_OID", "NumberAttribute");

	  // Get the fully qualified name of the TermsMgr subclass that will apply
      // for this terms instance

      String termsMgrSubClass = TermsMgr.getSubClassName(
                                    (String)getClientServerDataBridge().getCSDBValue("instrument_type"),
                                    (String)getClientServerDataBridge().getCSDBValue("transaction_type"),
                                    (String)getClientServerDataBridge().getCSDBValue("component_id"),
                                    (String)getClientServerDataBridge().getCSDBValue("standby_using_guarantee_form"));

	  // Use the Java reflection API to create the TermsMgr subclass as the manager, passing in
      // a reference to AttributeMgr
	  try
       {
		// Array of classes describes the signature of the constructor to call
		Class parameterClassArray[] = {TermsWebBean.class};
		// Array of objects is the objects to be passed as parameters to the constructor
		Object parameterArray[] = {this};

		// Create an instance
	    manager = (TermsMgr) Class.forName(termsMgrSubClass).getConstructor(parameterClassArray).newInstance(parameterArray);
       }
	  catch (ClassNotFoundException |InstantiationException | java.lang.reflect.InvocationTargetException | NoSuchMethodException 
			  | IllegalAccessException e)
       {
		    manager = null;
		    LOG.error("TermsWebBean: Exception registering attributes: ",e);
	   }   
   }

  /**
   * Retrieves data for the first shipment of this terms object
   */
  public void populateFirstShipmentFromDatabase()
   {
      firstShipment = beanMgr.createBean(ShipmentTermsWebBean.class, "ShipmentTerms");

      String firstShipmentOid = this.getAttribute("first_shipment_oid");

      if(!ConvenienceServices.isBlank(firstShipmentOid))
        firstShipment.getById(firstShipmentOid);
   }

  /**
   * Populates the first shipment based on XML
   */
  public void populateFirstShipmentFromXml(DocumentHandler xml)
   {
      firstShipment =  beanMgr.createBean(ShipmentTermsWebBean.class,"ShipmentTerms");

      if(xml != null)
        firstShipment.populateFromXmlDoc(xml, "/In/Terms/ShipmentTermsList", false); 
   }

  /**
   * Returns the first shipment.  One of the populate methods must have been called
   * for this method to work.
   */
  public ShipmentTermsWebBean getFirstShipment()
   {
      if(firstShipment == null)
       {
          return beanMgr.createBean(ShipmentTermsWebBean.class, "ShipmentTerms");
       }

      return firstShipment;
   }
  
	public boolean isFixedValue (String propertyToCheck, boolean isTemplate) {

        if (isTemplate)
        	return false;
        LOG.debug("PPWB::source Template in isFixedValue {} ][ {}" , sourceTermsWebBean , this.getAttribute(propertyToCheck));
		if (StringFunction.isBlank(this.getAttribute(propertyToCheck)))
			return false;
		else if (sourceTermsWebBean == null)
			return false;
		else if (StringFunction.isBlank(sourceTermsWebBean.getAttribute(propertyToCheck)))
			return false;
		else
			return true;

	}

}
