

  

package com.ams.tradeportal.busobj.webbean;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import javax.servlet.http.*;

/**
 * 
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class ShipmentTermsWebBean_Base extends TradePortalBusinessObjectWebBean
{

  public void registerAttributes()
   {  
      super.registerAttributes();


       /* shipment_oid - Unique identifier */
       registerAttribute("shipment_oid", "shipment_oid", "ObjectIDAttribute");


       /* description - Description */
       registerAttribute("description", "description");


       /* creation_date - Timestamp (in GMT) of when the shipment was created.  Used to order them
         on the page. */
       registerAttribute("creation_date", "creation_date", "DateTimeAttribute");


       /* trans_doc_consign_order_of_pty - The party to whom the goods are consigned. */
       registerAttribute("trans_doc_consign_order_of_pty", "trans_doc_consign_order_of_pty");


       /* trans_doc_consign_to_pty - The party to whom the goods are consigned. */
       registerAttribute("trans_doc_consign_to_pty", "trans_doc_consign_to_pty");


       /* trans_doc_consign_type - Indicates whether the goods are consigned directly to or are consigned to
         the order of */
       registerAttribute("trans_doc_consign_type", "trans_doc_consign_type");


       /* trans_doc_copies - The number of copies for the transport document */
       registerAttribute("trans_doc_copies", "trans_doc_copies", "NumberAttribute");


       /* trans_doc_included - Indicates whether or not transport documents data is entered. */
       registerAttribute("trans_doc_included", "trans_doc_included", "IndicatorAttribute");


       /* trans_doc_marked_freight - Indicates whether the freight is being paid by the Beneficiary (Prepaid)
         or by the Applicant (Collect). */
       registerAttribute("trans_doc_marked_freight", "trans_doc_marked_freight");


       /* trans_doc_originals - The number of originals for the transport document. */
       registerAttribute("trans_doc_originals", "trans_doc_originals");


       /* trans_doc_text - The transport document text for the instrument. */
       registerAttribute("trans_doc_text", "trans_doc_text");


       /* trans_doc_type - The type of transport document required by the instrument. For example,
         �Multi-Modal Transport Document or Port to Port B/L */
       registerAttribute("trans_doc_type", "trans_doc_type");


       /* goods_description - The description of the goods being purchased. */
       registerAttribute("goods_description", "goods_description");


       /* incoterm - The shipping term, or Incoterm, that describes what is included in the total
         cost of the transaction goods. For example, Cost and Freight or Cost Insurance
         Freight. */
       registerAttribute("incoterm", "incoterm");


       /* incoterm_location - The location where the purchaser of the transaction goods will take responsibility
         for them. For example, the port of discharge for the goods */
       registerAttribute("incoterm_location", "incoterm_location");


       /* po_line_items - The purchase order line items assigned to the transaction. This read-only
         text area appears only if the organisation uses the system's purchase orders
         functionality and there are one or more PO line items are assigned to this
         transaction. */
       registerAttribute("po_line_items", "po_line_items");


       /* shipment_from - Place of Taking Charge/Dispatch From.../Receipt */
       registerAttribute("shipment_from", "shipment_from");


       /* shipment_to - Port of Discharge/Airport of Destination */
       registerAttribute("shipment_to", "shipment_to");


       /* transshipment_allowed - If this checkbox is selected, transhipment is allowed for this instrument.
         For example, if the goods can be shipped part of the way by ship and the
         remaining distance by truck. */
       registerAttribute("transshipment_allowed", "transshipment_allowed", "IndicatorAttribute");


       /* vessel_name_voyage_num - The vessel name and/or voyage number for the goods represented by the instrument */
       registerAttribute("vessel_name_voyage_num", "vessel_name_voyage_num");


       /* air_waybill_num - The number of the air waybill being released by the instrument. */
       registerAttribute("air_waybill_num", "air_waybill_num");


       /* bill_of_lading_num - The number of the bill of lading for �the instrument. */
       registerAttribute("bill_of_lading_num", "bill_of_lading_num");


       /* carrier_name_flight_num - The carrier name and/or flight number for the goods represented by the instrument. */
       registerAttribute("carrier_name_flight_num", "carrier_name_flight_num");


       /* container_number - The container number for the goods. */
       registerAttribute("container_number", "container_number");


       /* trans_addl_doc_included - Indicates whether there are additional transport documents for the instrument. */
       registerAttribute("trans_addl_doc_included", "trans_addl_doc_included", "IndicatorAttribute");


       /* trans_addl_doc_text - Additional transport documents for the instrument. */
       registerAttribute("trans_addl_doc_text", "trans_addl_doc_text");


       /* latest_shipment_date - The latest date that the goods can be shipped for the instrument. */
       registerAttribute("latest_shipment_date", "latest_shipment_date", "DateTimeAttribute");


       /* shipment_from_loading - Port of Loading/Airport of Departure */
       registerAttribute("shipment_from_loading", "shipment_from_loading");


       /* shipment_to_discharge - Place of Final Destination/Deliver/For Transport To */
       registerAttribute("shipment_to_discharge", "shipment_to_discharge");

      /* Pointer to the parent Terms */
       registerAttribute("terms_oid", "p_terms_oid", "ParentIDAttribute");

       /* NotifyParty -  */
       registerAttribute("c_NotifyParty", "c_notify_party_oid", "NumberAttribute");

       /* OtherConsigneeParty -  */
       registerAttribute("c_OtherConsigneeParty", "c_other_consignee_party_oid", "NumberAttribute");
   }
   
   




}
