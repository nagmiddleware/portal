

  

package com.ams.tradeportal.busobj.webbean;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import javax.servlet.http.*;

/**
 * After processing, data stored as IncomingInterfaceQueue business objects
 * is moved to be stored as IncomingQueueHistory.  This is done to prevenet
 * the IncomingInterfaceQueue table from becoming too large.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class IncomingQueueHistoryWebBean_Base extends TradePortalBusinessObjectWebBean
{

  public void registerAttributes()
   {  
      super.registerAttributes();


       /* queue_history_oid - Unique identifier */
       registerAttribute("queue_history_oid", "queue_history_oid", "ObjectIDAttribute");


       /* date_received - Timestamp in GMT of when the message was received */
       registerAttribute("date_received", "date_received", "DateTimeAttribute");


       /* date_sent - Timestamp in GMT of when this message was sent in OTL */
       registerAttribute("date_sent", "date_sent", "DateTimeAttribute");


       /* msg_text - The XML data for the message */
       registerAttribute("msg_text", "msg_text");


       /* msg_type - The type of message to be processed.  This indicates what data is contained
         in the XML - a transaction or mail message */
       registerAttribute("msg_type", "msg_type");


       /* status - Lifecycle status of the message. */
       registerAttribute("status", "status");


       /* client_bank - Not currently used */
       registerAttribute("client_bank", "client_bank");


       /* message_id - An ID for the message.  This ID is generated on OTL */
       registerAttribute("message_id", "message_id");


       /* reply_to_message_id - Used for confirmation messages only.  Refers to the message that this message
         is confirming */
       registerAttribute("reply_to_message_id", "reply_to_message_id");


       /* agent_id - The name of the agent that is currently dealing with this message.   This
         prevents multiple agents from accessing the same dat */
       registerAttribute("agent_id", "agent_id");


       /* confirmation_error_text - Any errors resulting from attempting to confirm this message. */
       registerAttribute("confirmation_error_text", "confirmation_error_text");


       /* unpackaging_error_text - Errors from unpackaging */
       registerAttribute("unpackaging_error_text", "unpackaging_error_text");


       /* complete_instrument_id - The prefix, instrument number, and suffix concatenated together. */
       registerAttribute("complete_instrument_id", "complete_instrument_id");


       /* transaction_type_code - Code indicating the type of transaction.   Examples of types are Issue,
         Amend, Assignment of Proceeds, etc. */
       registerAttribute("transaction_type_code", "transaction_type_code");


       /* transaction_oid - The OID of the transaction or main object on which an action was performed
         that required placing a row in the incoming queue.  This is not an association
         so it can store OID of heterogeneous objects. */
       registerAttribute("transaction_oid", "a_transaction_oid", "NumberAttribute");


       /* datetime_msgbroker - Date and Time when the message is recirved on MQ message broker. */
       registerAttribute("datetime_msgbroker", "datetime_msgbroker", "DateTimeAttribute");


       /* datetime_unpack_start - Date and Time when the unpackaging starts for this message. */
       registerAttribute("datetime_unpack_start", "datetime_unpack_start", "DateTimeAttribute");


       /* datetime_unpack_end - Date and Time when the unpackaging ends for this message. */
       registerAttribute("datetime_unpack_end", "datetime_unpack_end", "DateTimeAttribute");


       /* unpack_seconds - The amount of time (in seconds) it takes to unpackage the message. */
       registerAttribute("unpack_seconds", "unpack_seconds", "NumberAttribute");


       /* process_seconds - The amount of time (in seconds) it takes to process the message, starting
         from the message being received on MQ message broker to the message being
         unpackaged. */
       registerAttribute("process_seconds", "process_seconds", "NumberAttribute");

       /* instrument_oid - The instrument on which an action was performed that required placing a
         row in the incoming queue. */
       registerAttribute("instrument_oid", "a_instrument_oid", "NumberAttribute");

       /* mail_message_oid - The mail message on which an action was performed that required placing
         a row in the incoming queue */
       registerAttribute("mail_message_oid", "a_mail_message_oid", "NumberAttribute");
   }
   
   




}
