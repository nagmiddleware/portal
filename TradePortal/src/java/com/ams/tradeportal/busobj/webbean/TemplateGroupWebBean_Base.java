

  

package com.ams.tradeportal.busobj.webbean;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import javax.servlet.http.*;

/**
 * Accounts associated to the User, where the User is authorized to transfer
 * funds.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class TemplateGroupWebBean_Base extends TradePortalBusinessObjectWebBean
{

  public void registerAttributes()
   {  
      super.registerAttributes();


       /* authorized_account_oid - Unique Identifier */
       registerAttribute("template_group_oid", "template_group_oid", "ObjectIDAttribute");


      /* Pointer to the parent User */
       registerAttribute("template_name", "template_name");

       /* account_oid -  */
       registerAttribute("corp_org_oid", "p_org_oid", "ParentIDAttribute");
   }
   
   




}
