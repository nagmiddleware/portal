package com.ams.tradeportal.busobj.webbean;

import com.ams.tradeportal.common.TradePortalConstants;

/*
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class CorporateOrganizationWebBean extends CorporateOrganizationWebBean_Base
{
    public boolean allowSomeTradeTransaction() {
        boolean allow = false;
        String[] allowArray = { this.getAttribute("allow_import_DLC"),
                                this.getAttribute("allow_SLC"),
                                this.getAttribute("allow_export_LC"),
                                this.getAttribute("allow_guarantee"),
                                this.getAttribute("allow_airway_bill"),
                                this.getAttribute("allow_shipping_guar"),
                                this.getAttribute("allow_loan_request"),
                                this.getAttribute("allow_export_collection"),
                                this.getAttribute("allow_new_export_collection"),
                                this.getAttribute("allow_request_advise"),
                                this.getAttribute("allow_non_prtl_org_instr_ind"),  
                                this.getAttribute("allow_approval_to_pay")};

        for(int i=0;i<allowArray.length;i++){
            if((TradePortalConstants.INDICATOR_YES).equals(allowArray[i])){
                allow=true;
                break;
            }

        }
        return allow;
    }

    public boolean allowSomePaymentTransaction() {
        boolean allowXferBtwnAccts = false;
        boolean allowDomesticPayments = false;
        boolean allowInternationalPayments = false;
    
        if(this.getAttribute("allow_panel_auth_for_pymts").equals(TradePortalConstants.INDICATOR_YES)){
            allowXferBtwnAccts = this.getAttribute("allow_xfer_btwn_accts_panel").equals(TradePortalConstants.INDICATOR_YES);
            allowDomesticPayments = this.getAttribute("allow_domestic_payments_panel").equals(TradePortalConstants.INDICATOR_YES);
   		   
        } else {
            allowXferBtwnAccts = this.getAttribute("allow_xfer_btwn_accts").equals(TradePortalConstants.INDICATOR_YES);
            allowDomesticPayments = this.getAttribute("allow_domestic_payments").equals(TradePortalConstants.INDICATOR_YES);

        }

        return allowXferBtwnAccts || allowDomesticPayments || allowInternationalPayments;        
    }
    

    public boolean allowCashManagement(){
    	
        boolean allowCashMgmt = false;	
    
        boolean allowInternationalPayments = false;

        if(this.getAttribute("allow_panel_auth_for_pymts").equals(TradePortalConstants.INDICATOR_YES)){
          allowInternationalPayments = this.getAttribute("allow_funds_transfer_panel").equals(TradePortalConstants.INDICATOR_YES);
        } else {
          allowInternationalPayments = this.getAttribute("allow_funds_transfer").equals(TradePortalConstants.INDICATOR_YES);
        }

    	boolean allowTrade = allowSomeTradeTransaction();
    	boolean allowPayment = allowSomePaymentTransaction();

       if(allowPayment)
    	   allowCashMgmt = true;
       else if (!allowTrade && (allowPayment || allowInternationalPayments)) 
    	   allowCashMgmt = true;

       return allowCashMgmt;
    }

       
    public boolean allowSomeReceivablesTransaction(){
        return TradePortalConstants.INDICATOR_YES.equals(this.getAttribute("allow_arm"));
    }

    public boolean allowSomeDirectDebitTransaction(){
        return TradePortalConstants.INDICATOR_YES.equals(this.getAttribute("allow_direct_debit"));
    } 

    public boolean allowSomeInvoiceOffersTransaction(){
        return TradePortalConstants.INDICATOR_YES.equals(this.getAttribute("allow_supplier_portal"));
    }

    

    public boolean allowPayablesTransaction(){
        return TradePortalConstants.INDICATOR_YES.equals(this.getAttribute("allow_payables"));
    }

 

}
