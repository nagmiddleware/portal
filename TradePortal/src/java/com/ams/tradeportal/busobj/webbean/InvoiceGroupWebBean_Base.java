package com.ams.tradeportal.busobj.webbean;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import javax.servlet.http.*;

/**
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class InvoiceGroupWebBean_Base extends TradePortalBusinessObjectWebBean {

	/*
	 * Register the attributes and associations of the business object
	 */
	  public void registerAttributes()
	   {  
	      super.registerAttributes();

		      /* Register attributes defined in the Ancestor class */
		      super.registerAttributes();
		      
		      /* invoice_group_oid - Unique identifier */
		      registerAttribute("invoice_group_oid", "invoice_group_oid", "ObjectIDAttribute");
		      /*  This is the Trading Partner Name as established in the Trading Partner rule.  */
		      registerAttribute("trading_partner_name", "trading_partner_name");
		      /* Invoice Status */
		      registerAttribute("invoice_status", "invoice_status");
		      /* Currency */
		      registerAttribute("currency", "currency");
		      /* linked_instrument_type field representing the code value for the instrument to which the invoices should ultimately be associated:
		       * REC (Receivables), PAY (Payables), ATP (Approval to Pay), LNR (Loan request) */
		      registerAttribute("linked_to_instrument_type", "linked_to_instrument_type");
		      /* payment_date - valid date format can be selected from the dropdown list. */
		      registerAttribute("payment_date", "payment_date","DateAttribute");
		    
		      /* opt_lock - Optimistic lock attribute
				 See jPylon documentation for details on how this works */
		      registerAttribute("opt_lock", "opt_lock", "OptimisticLockAttribute");
		      
		      registerAttribute("corp_org_oid", "a_corp_org_oid", "NumberAttribute");

  			  registerAttribute("attachment_ind", "attachment_ind", "IndicatorAttribute");
			  //Srinivasu_D CR-709 Rel8.2 02/02/2013
			  registerAttribute("loan_type", "loan_type");

			  registerAttribute("invoice_classification", "invoice_classification");
			  
			  // Pavani Rel 8.3 CR 821 Start
		       /* panel_auth_group_oid - Panel group associated to an Invoice. */
		        registerAttribute("panel_auth_group_oid", "a_panel_auth_group_oid", "NumberAttribute");
		        
		        /* panel_auth_range_oid - Panel Range to a InvoiceGroup. */
		        registerAttribute("panel_auth_range_oid", "a_panel_auth_range_oid", "NumberAttribute");
		       
		        /* opt_lock - Optimistic lock attribute*/
		        registerAttribute("panel_oplock_val", "panel_oplock_val", "NumberAttribute");
		       
		 	  // Pavani Rel 8.3 CR 821 End
		        registerAttribute("send_to_supplier_date", "send_to_supplier_date");
		        
		      // Nar Rel 9.2 CR 914A  grouping uses End To End ID for PYB invoices
			    registerAttribute("end_to_end_id", "end_to_end_id");
		   }
	}

