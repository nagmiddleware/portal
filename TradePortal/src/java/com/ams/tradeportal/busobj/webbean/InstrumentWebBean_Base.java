



package com.ams.tradeportal.busobj.webbean;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import javax.servlet.http.*;

/**
 * Subclass of the InstrumentData business object that represents instruments
 * that are created by the corporate customers of the Trade Portal.    Each
 * instrument has one to many transactions.
 *
 *     Copyright  � 2003
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class InstrumentWebBean_Base extends InstrumentDataWebBean
{

  public void registerAttributes()
   {
      super.registerAttributes();


       /* instrument_num - The instrument number assigned to this instrument.  This is generated from
         the client bank's instrument number sequence. */
       registerAttribute("instrument_num", "instrument_num");


       /* instrument_prefix - The instrument number prefix assigned to this instrument.  This is determined
         from the operational bank organization to which this instrument is associated. */
       registerAttribute("instrument_prefix", "instrument_prefix");


       /* instrument_suffix - The instrument number suffix assigned to this instrument.  This is determined
         from the operational bank organization to which this instrument is associated. */
       registerAttribute("instrument_suffix", "instrument_suffix");


       /* complete_instrument_id - The prefix, instrument number, and suffix concatenated together. */
       registerAttribute("complete_instrument_id", "complete_instrument_id");


       /* instrument_status - The lifecycle status of an instrument. */
       registerAttribute("instrument_status", "instrument_status");


       /* from_express_template - Set to Yes if this instrument was created from an express template.   If
         it is created from an express template, certain fields on the transaction
         form may be read-only. */
       registerAttribute("from_express_template", "from_express_template", "IndicatorAttribute");


       /* copy_of_ref_num - A copy of the most recently updated transactions' reference number.   This
         copy must exist so that instrument searching can occur based on the reference
         number. */
       registerAttribute("copy_of_ref_num", "copy_of_ref_num");


       /* active_transaction_oid - A pointer to the active transaction of the instrument.   This is not a jPylon
         association because the possibility exists that the Instrument business
         object could be created at the same time as the active transaction's Transaction
         business object.   If this were a true jPylon association, it would always
         fail because the Transaction business object would not exist in the database
         yet.  To avoid this situation, this is just a number attribute instead of
         an assocation */
       registerAttribute("active_transaction_oid", "active_transaction_oid", "NumberAttribute");


       /* issue_date - The issue date of the instrument.  This field is only updated by the middleware. */
       registerAttribute("issue_date", "issue_date", "DateTimeAttribute");


       /* copy_of_expiry_date - A copy of the most recently updated transaction's expiry date.   This copy
         must exist so that instrument searching can occur based on the expiry date. */
       registerAttribute("copy_of_expiry_date", "copy_of_expiry_date", "DateTimeAttribute");


       /* copy_of_instrument_amount - The current amount for this instrument.  When the original transaction of
         the instrument is saved, the transaction's instrument amount is copied to
         this field.   For all other transactions, the transaction's instrument amount
         field is only copied when the middleware updates a transaction. */
       registerAttribute("copy_of_instrument_amount", "copy_of_instrument_amount", "TradePortalDecimalAttribute");


       /* opening_bank_ref_num - The reference number used by the opening bank. */
       registerAttribute("opening_bank_ref_num", "opening_bank_ref_num");


       /* purge_date - The date/time at which this instrument was purged in OTL.  This is set to
         null if no purging has been requested yet for this instrument.  See the
         instrument_purge_type on client_bank. */
       registerAttribute("purge_date", "purge_date", "DateTimeAttribute");


       /* copy_of_confirmation_ind - Determines whether the Instrument has been confirmed

         Yes=Instrument has been confirmed */
       registerAttribute("copy_of_confirmation_ind", "copy_of_confirmation_ind", "IndicatorAttribute");


       /* present_docs_within_days - present_docs_within_days - The number of days after shipment that documents
         must be presented.  This         attribute is updated by incoming messages
         from bank. */
       registerAttribute("present_docs_within_days", "present_docs_within_days", "NumberAttribute");


       /* vendor_id - A 15 character alphanumeric value representing the vendor ID associated
         with this instrument */
       registerAttribute("vendor_id", "vendor_id");

       /* client_bank_oid - Each instrument is associated to a corporate organization, which is then
         "owned" in the Trade Portal by a client bank.   This association is a shortcut
         to the client bank from instrument to make reporting more efficient. */
       registerAttribute("client_bank_oid", "a_client_bank_oid", "NumberAttribute");

       /* corp_org_oid - Each instrument in related to a corporate organization.  This reflects the
         fact that the instrument was created by the corporate organization or deals
         with the business of that corporate organization. */
       registerAttribute("corp_org_oid", "a_corp_org_oid", "NumberAttribute");

       /* op_bank_org_oid - When creating an instrument, the user must select the operational bank organization
         to which the instrument should be sent for processing.  This association
         represents that relationship. */
       registerAttribute("op_bank_org_oid", "a_op_bank_org_oid", "NumberAttribute");

       /* related_instrument_oid - Sometimes, instruments are related to other instruments.  This association
         represents this relationship. */
       registerAttribute("related_instrument_oid", "a_related_instrument_oid", "NumberAttribute");

       /* OTL_POInvoicePortfolio - The one-to-one component pointer for OTL_POInvoice_portfolio.  */
       registerAttribute("OTL_POInvoicePortfolio", "c_OTL_PO_INVOICE_PORTFOLIO_OID", "NumberAttribute");

       /* suppress_doc_link_ind - Suppress PDF Document link during transaction processing.  Only after the
	          transaction is successfully released in the TPS after a successful Denied
	          Party Compliance check has been executed, and the status of the transaction
	          in the Portal is updated to "Processed by Bank", will the links for the
	          Collection/Amendment Schedule and Bill of Exchange document be made available
	          in the Portal as part of the update process of the transaction. */
       registerAttribute("suppress_doc_link_ind", "suppress_doc_link_ind", "IndicatorAttribute");//Vshah CR-452 - Register this new attribute

       /* confidential_indicator - Indicates whether or not the template is confidential.

         Yes=template is a confidential instrument */
       registerAttribute("confidential_indicator", "confidential_indicator");
		//Srinivasu_D CR#269 Rel8.4 09/02/2013 - added
	   registerAttribute("converted_transaction_ind", "converted_transaction_ind", "IndicatorAttribute");
	   
	   /* Rpasupulati IR #T36000019732 fixed_payment_flag - Set to Yes if this instrument was created from an fixed template.   If
       it is created from an fixed template, certain fields on the payment
       form may be read-only. */
	   registerAttribute("fixed_payment_flag", "fixed_payment_flag", "IndicatorAttribute");
	   
	   //MEer Rel 9.3.5 CR-1029
	   registerAttribute("bank_instrument_id", "bank_instrument_id");
	   
	   registerAttribute("template_oid", "template_oid", "NumberAttribute"); //RKAZI 07-07-2015 T36000038018 - ADD
   }






}
