package com.ams.tradeportal.busobj.webbean;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import javax.servlet.http.*;
import com.ams.tradeportal.html.*;

public class InvOnlyCreateRuleWebBean_Base extends TradePortalBusinessObjectWebBean{
	
	   public void registerAttributes() 
	   {

	      /* Register attributes defined in the Ancestor class */
	      super.registerAttributes();

	      /* phrase_oid - Unique identifier */
	      registerAttribute("inv_only_create_rule_oid", "inv_only_create_rule_oid", "ObjectIDAttribute");
	       /* name - Name of the LC Creation Rule */
	       registerAttribute("name", "name");


	       /* description - Description of the rule for convenience */

		  registerAttribute("opt_lock", "opt_lock", "OptimisticLockAttribute");
		  registerAttribute("invoice_def", "a_invoice_def");
		  registerAttribute("inv_only_create_rule_desc", "inv_only_create_rule_desc");
 		  registerAttribute("instrument_type_code", "instrument_type_code");
 		  registerAttribute("creation_date_time", "creation_date_time", "DateTimeAttribute");
 		  registerAttribute("inv_only_create_rule_template", "inv_only_create_rule_template");
 		  registerAttribute("op_bank_org_oid", "a_op_bank_org_oid");
 		  
		  registerAttribute("inv_only_create_rule_max_amt", "inv_only_create_rule_max_amt","NumberAttribute");
		  registerAttribute("due_or_pay_min_days", "due_or_pay_min_days"," NumberAttribute");
		  registerAttribute("due_or_pay_max_days", "due_or_pay_max_days"," NumberAttribute");
		  registerAttribute("inv_data_item1", "inv_data_item1");
		  registerAttribute("inv_data_item_val1", "inv_data_item_val1");
		  registerAttribute("inv_data_item2", "inv_data_item2");
		  registerAttribute("inv_data_item_val2", "inv_data_item_val2");
		  registerAttribute("inv_data_item3", "inv_data_item3");
		  registerAttribute("inv_data_item_val3", "inv_data_item_val3");
		  registerAttribute("inv_data_item4", "inv_data_item4");
		  registerAttribute("inv_data_item_val4", "inv_data_item_val4");
		  registerAttribute("inv_data_item5", "inv_data_item5");
		  registerAttribute("inv_data_item_val5", "inv_data_item_val5");
		  registerAttribute("inv_data_item6", "inv_data_item6");
		  registerAttribute("inv_data_item_val6", "inv_data_item_val6");
		  
	        /* Pointer to the parent ReferenceDataOwner */
	      registerAttribute("owner_org_oid", "p_owner_org_oid", "ParentIDAttribute");
	      
	   }

	      
}
