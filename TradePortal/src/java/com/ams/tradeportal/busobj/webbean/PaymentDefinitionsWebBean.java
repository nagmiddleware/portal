package com.ams.tradeportal.busobj.webbean;

import com.ams.tradeportal.common.Sort;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.util.ResourceManager;
import com.amsinc.ecsg.util.StringFunction;


public class PaymentDefinitionsWebBean extends PaymentDefinitionsWebBean_Base {

    // Nar IR-NEUM031959693 04/09/2012 Begin
    private int reqInvSummaryCount;
    private int reqInvGoodsCount;
    private int reqInvLineitemCount;

    public int getReqInvSummaryCount() {
        return reqInvSummaryCount;
    }

    private void setReqInvSummaryCount(int reqInvSummaryCount) {
        this.reqInvSummaryCount = reqInvSummaryCount;
    }

    public int getReqInvGoodsCount() {
        return reqInvGoodsCount;
    }

    private void setReqInvGoodsCount(int reqInvGoodsCount) {
        this.reqInvGoodsCount = reqInvGoodsCount;
    }

    public int getReqInvLineitemCount() {
        return reqInvLineitemCount;
    }

    private void setReqInvLineitemCount(int reqInvLineitemCount) {
        this.reqInvLineitemCount = reqInvLineitemCount;
    }
    // Nar IR-NEUM031959693 04/09/2012	End

    /**
     * Builds a set of sorted html invoice summary option tags based on the 7 predefined data
     * attributes (amount, ben_name, currency, ...) as well as the optional
     * "linked_instrument_ty" fields.  The option id is the internal identifier (e.g., amount
     * or other1) and the displayed value is whatever name the user has
     * assigned to that attribute.  Only non-blank attributes are included.
     *
     * @return java.lang.String
     */

    public String buildINVFieldOptionList(ResourceManager resMgr) {
        String[] options = new String[26];
        StringBuffer optionList = new StringBuffer();
        int count = 0;
        int x = 1;
        int tabPosition;

        // Build an array consisting of each non-blank attribute.  The array
        // contents consist of the user's provided attribute name, a tab
        // separator, and the internal name.
        options[count++] = resMgr.getText("InvoiceDefinition.InvoiceID", TradePortalConstants.TEXT_BUNDLE) + "\t" + "invoice_id";

        if ((TradePortalConstants.INVOICE_TYPE_REC_MGMT).equals(getAttribute("invoice_type_indicator"))) {
            options[count++] = resMgr.getText("InvoiceDefinition.BuyerID", TradePortalConstants.TEXT_BUNDLE) + "\t" + "buyer_id";
            options[count++] = resMgr.getText("InvoiceDefinition.BuyerName", TradePortalConstants.TEXT_BUNDLE) + "\t" + "buyer_name";
        } else if ((TradePortalConstants.INVOICE_TYPE_PAY_MGMT).equals(getAttribute("invoice_type_indicator"))) {
            options[count++] = resMgr.getText("InvoiceDefinition.SellerId", TradePortalConstants.TEXT_BUNDLE) + "\t" + "seller_id";
            options[count++] = resMgr.getText("InvoiceDefinition.SellerName", TradePortalConstants.TEXT_BUNDLE) + "\t" + "seller_name";
        }
        options[count++] = resMgr.getText("InvoiceDefinition.Cuurency", TradePortalConstants.TEXT_BUNDLE) + "\t" + "currency";
        options[count++] = resMgr.getText("InvoiceDefinition.Amount", TradePortalConstants.TEXT_BUNDLE) + "\t" + "amount";
        // RKAZI Rel 8.2 IR# T36000012087 02/28/2013 START
        if ((TradePortalConstants.INDICATOR_YES).equals(getAttribute("linked_to_instrument_type_req")))
            options[count++] = resMgr.getText("InvoiceDefinition.LinkedInsrtTy", TradePortalConstants.TEXT_BUNDLE) + "\t" + "linked_to_instrument_type";
        if ((TradePortalConstants.INDICATOR_YES).equals(getAttribute("pay_method_req")))
            options[count++] = resMgr.getText("InvoiceUploadRequest.pay_method", TradePortalConstants.TEXT_BUNDLE) + "\t" + "pay_method";
      if ((TradePortalConstants.INDICATOR_YES).equals(getAttribute("ben_country_req")))
            options[count++] = resMgr.getText("InvoiceUploadRequest.ben_country", TradePortalConstants.TEXT_BUNDLE) + "\t" + "ben_country";
      // Now sort the options array.  Since the user's defined name is the first
        // part of each entry, that's how they are sorted.
        try {
            Sort.sort(options, resMgr.getResourceLocale());
        } catch (Exception e) {
            // if the sort fails, we do nothing and the list remains unsorted
        }

        // Now build the HTML option list from the sorted array
        for (x = 0; x < count; x++) {
            tabPosition = options[x].indexOf("\t");

            optionList.append("<option value=");
            optionList.append(options[x].substring(tabPosition + 1, options[x].length()));
            optionList.append(">");
            optionList.append(options[x].substring(0, tabPosition));
            optionList.append("</option>");
        }
        // Nar IR-NEUM031959693 04/09/2012 Begin
        setReqInvSummaryCount(count);
        // Nar IR-NEUM031959693 04/09/2012 End

        return optionList.toString();

    }

    /**
     * Builds a set of sorted html invoice Goods option tags based on the the optional
     * "28" attribute.  The option id is the internal identifier (e.g., other1,
     * or other2) and the displayed value is whatever name the user has
     * assigned to that attribute.  Only non-blank attributes are included.
     *
     * @return java.lang.String
     */


    public String buildINVGoodsFieldOptionList(ResourceManager resMgr) {
        String fieldName;
        String[] options = new String[28];
        StringBuffer optionList = new StringBuffer();
        int count = 0;
        int x = 1;
        int tabPosition;

        // Build an array consisting of each non-blank attribute.  The array
        // contents consist of the user's provided attribute name, a tab
        // separator, and the internal name.

        if ((TradePortalConstants.INDICATOR_YES).equals(getAttribute("goods_description_req")))
            options[count++] = resMgr.getText("InvoiceDefinition.GoodDesc", TradePortalConstants.TEXT_BUNDLE) + "\t" + "goods_description";

        if ((TradePortalConstants.INDICATOR_YES).equals(getAttribute("incoterm_req")))
            options[count++] = resMgr.getText("InvoiceDefinition.Incoterm", TradePortalConstants.TEXT_BUNDLE) + "\t" + "incoterm";

        if ((TradePortalConstants.INDICATOR_YES).equals(getAttribute("country_of_loading_req"))) // RKAZI Rel 8.2 IR# T36000012087 02/28/2013 changed loding to loading
            options[count++] = resMgr.getText("InvoiceDefinition.CountryOfLoading", TradePortalConstants.TEXT_BUNDLE) + "\t" + "country_of_loading";

        if ((TradePortalConstants.INDICATOR_YES).equals(getAttribute("country_of_discharge_req")))
            options[count++] = resMgr.getText("InvoiceDefinition.CountryOfDischarge", TradePortalConstants.TEXT_BUNDLE) + "\t" + "country_of_discharge";

        if ((TradePortalConstants.INDICATOR_YES).equals(getAttribute("vessel_req")))
            options[count++] = resMgr.getText("InvoiceDefinition.Vessel", TradePortalConstants.TEXT_BUNDLE) + "\t" + "vessel";

        if ((TradePortalConstants.INDICATOR_YES).equals(getAttribute("carrier_req")))
            options[count++] = resMgr.getText("InvoiceDefinition.Carrier", TradePortalConstants.TEXT_BUNDLE) + "\t" + "carrier";

        if ((TradePortalConstants.INDICATOR_YES).equals(getAttribute("actual_ship_date_req")))
            options[count++] = resMgr.getText("InvoiceDefinition.ActualShipDate", TradePortalConstants.TEXT_BUNDLE) + "\t" + "actual_ship_date";

        if ((TradePortalConstants.INDICATOR_YES).equals(getAttribute("purchase_ord_id_req")))
            options[count++] = resMgr.getText("InvoiceDefinition.PurchageOrdDate", TradePortalConstants.TEXT_BUNDLE) + "\t" + "purchase_order_id";

        for (x = 1; x <= TradePortalConstants.INV_BUYER_OR_SELLER_USER_DEF_FIELD; x++) {
            fieldName = getAttribute("buyer_users_def" + x + "_label");
            if ((TradePortalConstants.INDICATOR_YES).equals(getAttribute("buyer_users_def" + x + "_req"))) {
                options[count++] = fieldName + "\t" + "buyer_users_def" + x + "_label";

            }
        }
        for (x = 1; x <= TradePortalConstants.INV_BUYER_OR_SELLER_USER_DEF_FIELD; x++) {
            fieldName = getAttribute("seller_users_def" + x + "_label");
            if ((TradePortalConstants.INDICATOR_YES).equals(getAttribute("seller_users_def" + x + "_req"))) {
                options[count++] = fieldName + "\t" + "seller_users_def" + x + "_label";

            }
        }

        // Now sort the options array.  Since the user's defined name is the first
        // part of each entry, that's how they are sorted.
        try {
            Sort.sort(options, resMgr.getResourceLocale());
        } catch (Exception e) {
            // if the sort fails, we do nothing and the list remains unsorted
        }

        // Now build the HTML option list from the sorted array
        for (x = 0; x < count; x++) {
            tabPosition = options[x].indexOf("\t");

            optionList.append("<option value=");
            optionList.append(options[x].substring(tabPosition + 1, options[x].length()));
            optionList.append(">");
            optionList.append(options[x].substring(0, tabPosition));
            optionList.append("</option>");
        }

        // Nar IR-NEUM031959693 04/09/2012 Begin
        setReqInvGoodsCount(count);
        // Nar IR-NEUM031959693 04/09/2012 End

        return optionList.toString();

    }

    /**
     * Builds a set of sorted html invoice line item detail option tags based optional
     * "12" attribute.  The option id is the internal identifier (e.g., other1,
     * or other2) and the displayed value is whatever name the user has
     * assigned to that attribute.  Only non-blank attributes are included.
     *
     * @return java.lang.String
     */
    public String buildLineItemFieldOptionList(ResourceManager resMgr) {
        String fieldName;
        String[] options = new String[12];
        StringBuffer optionList = new StringBuffer();
        int count = 0;
        int x = 1;
        int tabPosition;

        // Build an array consisting of each non-blank attribute.  The array
        // contents consist of the user's provided attribute name, a tab
        // separator, and the internal name.

        if ((TradePortalConstants.INDICATOR_YES).equals(getAttribute("line_item_id_req")))
            options[count++] = resMgr.getText("InvoiceDefinition.LineItemNumber", TradePortalConstants.TEXT_BUNDLE) + "\t" + "line_item_id";

        if ((TradePortalConstants.INDICATOR_YES).equals(getAttribute("unit_price_req")))
            options[count++] = resMgr.getText("InvoiceDefinition.LineItemUnitPrice", TradePortalConstants.TEXT_BUNDLE) + "\t" + "unit_price";

        if ((TradePortalConstants.INDICATOR_YES).equals(getAttribute("unit_of_measure_price_req")))
            options[count++] = resMgr.getText("InvoiceDefinition.LineItemUnitMesrPrice", TradePortalConstants.TEXT_BUNDLE) + "\t" + "unit_of_measure_price";

        if ((TradePortalConstants.INDICATOR_YES).equals(getAttribute("inv_quantity_req")))
            options[count++] = resMgr.getText("InvoiceDefinition.LineItemDetailInvQunt", TradePortalConstants.TEXT_BUNDLE) + "\t" + "inv_quantity";

        if ((TradePortalConstants.INDICATOR_YES).equals(getAttribute("unit_of_measure_quantity_req")))
            options[count++] = resMgr.getText("InvoiceDefinition.LineItemDetailUnitMsrQunty", TradePortalConstants.TEXT_BUNDLE) + "\t" + "unit_of_measure_quantity";


        for (x = 1; x <= TradePortalConstants.INV_LINE_ITEM_TYPE_OR_VALUE_FIELD; x++) {
            fieldName = getAttribute("prod_chars_ud" + x + "_type");
            if ((TradePortalConstants.INDICATOR_YES).equals(getAttribute("prod_chars_ud" + x + "_type_req"))) {
                options[count++] = fieldName + "\t" + "prod_chars_ud" + x + "_type";
            }
        }
        // Now sort the options array.  Since the user's defined name is the first
        // part of each entry, that's how they are sorted.
        try {
            Sort.sort(options, resMgr.getResourceLocale());
        } catch (Exception e) {
            // if the sort fails, we do nothing and the list remains unsorted
        }

        // Now build the HTML option list from the sorted array
        for (x = 0; x < count; x++) {
            tabPosition = options[x].indexOf("\t");

            optionList.append("<option value=");
            optionList.append(options[x].substring(tabPosition + 1, options[x].length()));
            optionList.append(">");
            optionList.append(options[x].substring(0, tabPosition));
            optionList.append("</option>");
        }

        // Nar IR-NEUM031959693 04/09/2012 Begin
        setReqInvLineitemCount(count);
        // Nar IR-NEUM031959693 04/09/2012 End

        return optionList.toString();

    }


    /**
     * Taking an HTML set of <option> values, searches for the one with an id
     * equal to the given selection.  If found, adds the 'selected' tag to
     * the HTML.  The updated set of options is returned.
     *
     * @param htmlOptions java.lang.String - a set of html option tags
     * @param selection   java.lang.String - the selection to search for
     * @return java.lang.String
     */
    public String selectFieldInList(String htmlOptions, String selection) {
        if (StringFunction.isNotBlank(selection)) {
            int start = htmlOptions.indexOf(selection);
            int insertPosition = htmlOptions.indexOf(">", start);
            htmlOptions =
                    htmlOptions.substring(0, insertPosition)
                            + " selected"
                            + htmlOptions.substring(insertPosition);
        }
        return htmlOptions;

    }


}
