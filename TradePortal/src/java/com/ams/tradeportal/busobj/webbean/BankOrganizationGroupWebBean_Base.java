

  

package com.ams.tradeportal.busobj.webbean;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;

import java.rmi.*;
import java.util.*;
import java.math.*;

import javax.ejb.*;

import com.ams.tradeportal.common.*;

import javax.servlet.http.*;

/**
 * A Bank Organization Group (BOG) represents a grouping of corporate customers
 * underneath a Client Bank.    Each BOG has its own branding directory and
 * can own reference data.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class BankOrganizationGroupWebBean_Base extends ReferenceDataOwnerWebBean
{

  public void registerAttributes()
   {  
      super.registerAttributes();


       /* description - A description or name of the Bank Organization Group. */
       registerAttribute("description", "description");


       /* branding_directory - The branding directory that is used when users that belong to this BOG are
         accessing the system.  Users who belong to corporate customers underneath
         a BOG will use the branding directory of their BOG. */
       registerAttribute("branding_directory", "branding_directory");


       /* support_information - Information on how the corporate customers under this BOG can gain support
         information.  Whatever is typed here will appear to the user on the support
         information page */
       registerAttribute("support_information", "support_information");


       /* sender_name - Determines who will be the sender of emails for the first language setting */
       registerAttribute("sender_name", "sender_name");


       /* sender_email_address - Email address of the sender for first language setting */
       registerAttribute("sender_email_address", "sender_email_address");


       /* system_name - Name used for this bank online trade system for first language setting */
       registerAttribute("system_name", "system_name");


       /* bank_url - The URL for this bank for first language setting. */
       registerAttribute("bank_url", "bank_url");


       /* email_language - Determines which language the sender name, sender email address, trade system
         name, and bank url should appear in */
       registerAttribute("email_language", "email_language");


       /* email_language_2 - Determines which language sender name 2, sender email address 2, trade system
         name 2, and bank url 2 should appear in */
       registerAttribute("email_language_2", "email_language_2");


       /* sender_name_2 - Determines who will be the sender of emails for the second language setting */
       registerAttribute("sender_name_2", "sender_name_2");


       /* do_not_reply_ind - A setting indicating whether or not emails sent will include the sentence
         "Do Not Reply to Email" */
       registerAttribute("do_not_reply_ind", "do_not_reply_ind", "IndicatorAttribute");


       /* sender_email_address_2 - Email address of the sender for second language setting */
       registerAttribute("sender_email_address_2", "sender_email_address_2");


       /* system_name_2 - Name used for this bank online trade system for second language setting */
       registerAttribute("system_name_2", "system_name_2");


       /* bank_url_2 - The URL for this bank for second language setting. */
       registerAttribute("bank_url_2", "bank_url_2");


       /* brand_button_ind - A setting indicating whether the client bank makes all the buttons branded.
         
         Yes - The client bank uses the branded buttons stored in the branding directory.
         No (default) - The client bank uses the standard buttons. */
       registerAttribute("brand_button_ind", "brand_button_ind", "IndicatorAttribute");

      /* Pointer to the parent ClientBank */
       registerAttribute("client_bank_oid", "p_client_bank_oid", "ParentIDAttribute");
       
       //SHILPAR- CR 597 start
       /* sender_name - Determines who will be the sender of emails for the first language setting */
       registerAttribute("bene_sender_name", "bene_sender_name");


       /* sender_email_address - Email address of the sender for first language setting */
       registerAttribute("bene_sender_email_address", "bene_sender_email_address");


       /* system_name - Name used for this bank online trade system for first language setting */
       registerAttribute("bene_system_name", "bene_system_name");


       /* bank_url - The URL for this bank for first language setting. */
       registerAttribute("bene_bank_url", "bene_bank_url");


       /* email_language - Determines which language the sender name, sender email address, trade system
         name, and bank url should appear in */
       registerAttribute("bene_email_language", "bene_email_language");
       
       /* do_not_reply_ind - A setting indicating whether or not emails sent will include the sentence
       "Do Not Reply to Email" */
       registerAttribute("bene_do_not_reply_ind", "bene_do_not_reply_ind", "IndicatorAttribute");
     //SHILPAR- CR 597 start
       
       // DK CR-640 Rel7.1 BEGINS      
       /* fx_online_avail_ind - A setting indicating whether online foreign exchange rate is availlable. */
       registerAttribute("fx_online_avail_ind", "fx_online_avail_ind", "IndicatorAttribute");
       
       /* deflt_fx_online_acct_id - Default Online Account Id for customer to access online foreign exchange */
       registerAttribute("deflt_fx_online_acct_id", "deflt_fx_online_acct_id");
       
       /* domain_org_url - Domain/Organisational URL for customer to access FX Online */
       registerAttribute("domain_org_url", "domain_org_url");      
       // DK CR-640 Rel7.1 ENDS
    
       /* cross_rate_calc_oid -  */
       registerAttribute("cross_rate_calc_oid", "a_cross_rate_calc_oid", "NumberAttribute");
       
     //PMitnala- CR 821 Rel 8.3 start
       /* sender_name - Determines who will be the sender of emails for the first language setting */
       registerAttribute("repair_sender_name", "repair_sender_name");


       /* sender_email_address - Email address of the sender for first language setting */
       registerAttribute("repair_sender_email_address", "repair_sender_email_address");


       /* system_name - Name used for this bank online trade system for first language setting */
       registerAttribute("repair_system_name", "repair_system_name");


       /* bank_url - The URL for this bank for first language setting. */
       registerAttribute("repair_bank_url", "repair_bank_url");


       /* email_language - Determines which language the sender name, sender email address, trade system
         name, and bank url should appear in */
       registerAttribute("repair_email_language", "repair_email_language");
       
       /* do_not_reply_ind - A setting indicating whether or not emails sent will include the sentence
       "Do Not Reply to Email" */
       registerAttribute("repair_do_not_reply_ind", "repair_do_not_reply_ind", "IndicatorAttribute");
     //PMitnala- CR 821 Rel 8.3 end
       
       /*KMehta 10/15/2013 Rel8400 CR-910 Start*/
       /* slc_gua_expiry_date_required - Add new slc_gua_expiry_date_required attribute to the BankOrganizationGroup bean. 
        * In the database this is a new char(1) column that defaults to �N�.*/
       registerAttribute("slc_gua_expiry_date_required", "slc_gua_expiry_date_required", "IndicatorAttribute");
       /*KMehta 10/15/2013 Rel8400 CR-910 End*/
       
     // Narayan- CR 913 Rel 9.0 29-Jan-2014 start
       /* sender_name - Determines who will be the sender of emails for the first language setting */
       registerAttribute("payable_sender_name", "payable_sender_name");


       /* sender_email_address - Email address of the sender for first language setting */
       registerAttribute("payable_sender_email_address", "payable_sender_email_address");


       /* system_name - Name used for this bank online trade system for first language setting */
       registerAttribute("payable_system_name", "payable_system_name");


       /* bank_url - The URL for this bank for first language setting. */
       registerAttribute("payable_bank_url", "payable_bank_url");


       /* email_language - Determines which language the sender name, sender email address, trade system
         name, and bank url should appear in */
       registerAttribute("payable_email_language", "payable_email_language");
       
       /* do_not_reply_ind - A setting indicating whether or not emails sent will include the sentence
       "Do Not Reply to Email" */
       registerAttribute("payable_do_not_reply_ind", "payable_do_not_reply_ind", "IndicatorAttribute");
     // Narayan- CR 913 Rel 9.0 29-Jan-2014 End

     //SSikhakolli Rel 9.3.5 CR-1029 Adding new attributes enable_admin_update_centre, include_tt_reim_allowed, enable_session_sync_html
       registerAttribute("enable_admin_update_centre", "enable_admin_update_centre", "IndicatorAttribute");
       registerAttribute("include_tt_reim_allowed", "include_tt_reim_allowed", "IndicatorAttribute");
       registerAttribute("enable_session_sync_html", "enable_session_sync_html", "IndicatorAttribute");
       
       //MEer CR-1027 Rel 9.3.5 START
       registerAttribute("airway_bill_pdf_type", "airway_bill_pdf_type");
       registerAttribute("atp_pdf_type", "atp_pdf_type");
       registerAttribute("exp_oco_pdf_type", "exp_oco_pdf_type");
       registerAttribute("exp_col_pdf_type", "exp_col_pdf_type");
       registerAttribute("imp_dlc_pdf_type", "imp_dlc_pdf_type");
       registerAttribute("lrq_pdf_type", "lrq_pdf_type");
       registerAttribute("gua_pdf_type", "gua_pdf_type");
       registerAttribute("slc_pdf_type", "slc_pdf_type");
       registerAttribute("rqa_pdf_type", "rqa_pdf_type");
       registerAttribute("shp_pdf_type", "shp_pdf_type");
       //MEer CR-1027 Rel 9.3.5 END
       
       //RPasupulati Rel 9.4 CR-1001 Start
       
       registerAttribute("reporting_code1_reqd_for_rtgs", "reporting_code1_reqd_for_rtgs", "IndicatorAttribute");
       registerAttribute("reporting_code1_reqd_for_ach", "reporting_code1_reqd_for_ach", "IndicatorAttribute");
       registerAttribute("reporting_code2_reqd_for_ach", "reporting_code2_reqd_for_ach", "IndicatorAttribute");
       registerAttribute("reporting_code2_reqd_for_rtgs", "reporting_code2_reqd_for_rtgs", "IndicatorAttribute");
      
       //RPasupulati Rel 9.4 CR-1001 Ends

       // W Zhu 9/28/2015 Rel 9.4 CR-1018 
      registerAttribute("no_sso_ind", "no_sso_ind", "IndicatorAttribute");
      
      /*CR1123 Start*/
      registerAttribute("incld_bnk_url_trade", "incld_bnk_url_trade", "IndicatorAttribute");
      registerAttribute("incld_bnk_url_pmnt_ben", "incld_bnk_url_pmnt_ben", "IndicatorAttribute");
      registerAttribute("incld_bnk_url_repr_panel", "incld_bnk_url_repr_panel", "IndicatorAttribute");
      registerAttribute("incld_bnk_url_payable_mgmt", "incld_bnk_url_payable_mgmt", "IndicatorAttribute");
	  /*CR1123 End*/
       
   }
}
