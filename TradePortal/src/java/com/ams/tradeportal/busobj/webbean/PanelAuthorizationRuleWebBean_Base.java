



package com.ams.tradeportal.busobj.webbean;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import javax.servlet.http.*;

/**
 * "Panel authorization" allows thresholds of authorization to be set at the
 * corporation level for each payment instrument (not trade instruments).
 * When a payment instrument goes through the authorization process, if panel
 * authorization is selected for the corporation, the system will use the type
 * and amount of the payment instrument and look at the corresponding level
 * (threshold) to determine the total number of authorizers and the profile
 * / panel level that the user must have (based on the new Panel Authority
 * -  A, B or C - present on the authorizing user's profile).
 *
 *     Copyright  © 2003
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class PanelAuthorizationRuleWebBean_Base extends TradePortalBusinessObjectWebBean
{

  public void registerAttributes()
   {
      super.registerAttributes();


registerAttribute("panel_auth_rule_oid", "panel_auth_rule_oid", "ObjectIDAttribute");
      

/*PanelAuthorizationRange ParentIDAttribute*/
/*registerAttribute("panel_auth_range_oid", "p_panel_auth_range_oid", "ParentIDAttribute");*/

/*Approver sequence Reference Attribute*/
registerAttribute("approver_sequence", "approver_sequence", "PANEL_APPROVE_SEQUENCE");

/* Approver Level Reference Attribute*/
registerAttribute("approver_1", "approver_1", "PANEL_AUTH_TYPE");
registerAttribute("approver_2", "approver_2", "PANEL_AUTH_TYPE");
registerAttribute("approver_3", "approver_3", "PANEL_AUTH_TYPE");
registerAttribute("approver_4", "approver_4", "PANEL_AUTH_TYPE");
registerAttribute("approver_5", "approver_5", "PANEL_AUTH_TYPE");
registerAttribute("approver_6", "approver_6", "PANEL_AUTH_TYPE");
registerAttribute("approver_7", "approver_7", "PANEL_AUTH_TYPE");
registerAttribute("approver_8", "approver_8", "PANEL_AUTH_TYPE");
registerAttribute("approver_9", "approver_9", "PANEL_AUTH_TYPE");
registerAttribute("approver_10", "approver_10", "PANEL_AUTH_TYPE");
registerAttribute("approver_11", "approver_11", "PANEL_AUTH_TYPE");
registerAttribute("approver_12", "approver_12", "PANEL_AUTH_TYPE");
registerAttribute("approver_13", "approver_13", "PANEL_AUTH_TYPE");
registerAttribute("approver_14", "approver_14", "PANEL_AUTH_TYPE");
registerAttribute("approver_15", "approver_15", "PANEL_AUTH_TYPE");
registerAttribute("approver_16", "approver_16", "PANEL_AUTH_TYPE");
registerAttribute("approver_17", "approver_17", "PANEL_AUTH_TYPE");
registerAttribute("approver_18", "approver_18", "PANEL_AUTH_TYPE");
registerAttribute("approver_19", "approver_19", "PANEL_AUTH_TYPE");
registerAttribute("approver_20", "approver_20", "PANEL_AUTH_TYPE");


registerAttribute("panel_auth_range_oid", "p_panel_auth_range_oid", "ParentIDAttribute");
   }






}
