package com.ams.tradeportal.busobj.webbean;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import javax.servlet.http.*;

/**
 * A PanelAuthorizationGroup consists of a set of PalenAuthorizationRanges
 * to specifiy the requirements of panel authorization.
 *
 *     Copyright  � 2003
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class PanelAuthorizationGroupWebBean_Base extends ReferenceDataWebBean
{

  public void registerAttributes()
   {
      super.registerAttributes();

      registerAttribute("panel_auth_group_oid", "panel_auth_group_oid", "ObjectIDAttribute");
      registerAttribute("instrument_group_type", "instrument_group_type", "PANEL_INST_GROUP");
      registerAttribute("payment_type", "payment_type");
      registerAttribute("ccy_basis", "ccy_basis", "CURRENCY_BASIS");
      registerAttribute("panelgroupconfpaymentinst", "panelgroupconfpaymentinst", "IndicatorAttribute");
      
      registerAttribute("ACH_pymt_method_ind", "ACH_pymt_method_ind", "IndicatorAttribute");
      registerAttribute("BCHK_pymt_method_ind", "BCHK_pymt_method_ind", "IndicatorAttribute");
      registerAttribute("BKT_pymt_method_ind", "BKT_pymt_method_ind", "IndicatorAttribute");
      registerAttribute("CBFT_pymt_method_ind", "CBFT_pymt_method_ind", "IndicatorAttribute");
      registerAttribute("CCHK_pymt_method_ind", "CCHK_pymt_method_ind", "IndicatorAttribute");
      registerAttribute("RTGS_pymt_method_ind", "RTGS_pymt_method_ind", "IndicatorAttribute");
      registerAttribute("FTBA_pymt_method_ind", "FTBA_pymt_method_ind", "IndicatorAttribute");
      registerAttribute("matchin_approval_ind", "matchin_approval_ind", "IndicatorAttribute");
      registerAttribute("inv_auth_ind", "inv_auth_ind", "IndicatorAttribute");
      registerAttribute("credit_auth_ind", "credit_auth_ind", "IndicatorAttribute");
      registerAttribute("AIR_ind", "AIR_ind", "IndicatorAttribute");
      registerAttribute("ATP_ind", "ATP_ind", "IndicatorAttribute");
      registerAttribute("EXP_COL_ind", "EXP_COL_ind", "IndicatorAttribute");
      registerAttribute("EXP_DLC_ind", "EXP_DLC_ind", "IndicatorAttribute");
      registerAttribute("EXP_OCO_ind", "EXP_OCO_ind", "IndicatorAttribute");
      registerAttribute("IMP_DLC_ind", "IMP_DLC_ind", "IndicatorAttribute");
      registerAttribute("LRQ_ind", "LRQ_ind", "IndicatorAttribute");
      registerAttribute("GUA_ind", "GUA_ind", "IndicatorAttribute");
      registerAttribute("SLC_ind", "SLC_ind", "IndicatorAttribute");
      registerAttribute("SHP_ind", "SHP_ind", "IndicatorAttribute");
      registerAttribute("RQA_ind", "RQA_ind", "IndicatorAttribute");
      registerAttribute("SP_ind", "SP_ind", "IndicatorAttribute");
      registerAttribute("DCR_ind", "DCR_ind", "IndicatorAttribute");
      //Rel9.0 CR 913 Start
      registerAttribute("pay_mgm_inv_auth_ind","pay_mgm_inv_auth_ind","IndicatorAttribute");
      registerAttribute("pay_upl_inv_auth_ind","pay_upl_inv_auth_ind","IndicatorAttribute");
      //Rel9.0 CR 913 End

      registerAttribute("panel_range_1_oid", "panel_range_1_oid", "NumberAttribute");
      registerAttribute("panel_range_2_oid", "panel_range_2_oid", "NumberAttribute");
      registerAttribute("panel_range_3_oid", "panel_range_3_oid", "NumberAttribute");
      registerAttribute("panel_range_4_oid", "panel_range_4_oid", "NumberAttribute");
      registerAttribute("panel_range_5_oid", "panel_range_5_oid", "NumberAttribute");
      registerAttribute("panel_range_6_oid", "panel_range_6_oid", "NumberAttribute");

       /* panel_auth_group_id - The ID to be displayed in dropdown. */
       registerAttribute("panel_auth_group_id", "panel_auth_group_id");

      /* Pointer to the parent CorporateOrganization */
       registerAttribute("corp_org_oid", "p_corp_org_oid", "ParentIDAttribute");
       
       //Rel9.2 CR 914A 
       registerAttribute("pay_credit_note_auth_ind","pay_credit_note_auth_ind","IndicatorAttribute");
       
     //SSikhakolli - Rel-9.4 CR-818 - adding new attribute
       registerAttribute("settlement_instr_ind" , "settlement_instr_ind", "IndicatorAttribute");
       registerAttribute("import_col_ind" , "import_col_ind", "IndicatorAttribute");
   }
}