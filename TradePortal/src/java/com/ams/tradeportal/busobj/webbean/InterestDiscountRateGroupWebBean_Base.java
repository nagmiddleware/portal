



package com.ams.tradeportal.busobj.webbean;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import javax.servlet.http.*;

/**
 * Interest Discount Rate Group
 *
 *     Copyright  � 2003
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class InterestDiscountRateGroupWebBean_Base extends TradePortalBusinessObjectWebBean
{

  public void registerAttributes()
   {


       /* interest_disc_rate_group_oid - interest_disc_rate_group_oid - Unique Identifier */
       registerAttribute("interest_disc_rate_group_oid", "interest_disc_rate_group_oid", "ObjectIDAttribute");


       /* group_id - A set of letters that is used to identify the different group. */
       registerAttribute("group_id", "group_id");


       /* description - Description of the Interest Discount Rate Group */
       registerAttribute("description", "description");


       /* short_description - Short Description of Interest Discount Rate Group */
       registerAttribute("short_description", "short_description");


       /* opt_lock - Optimistic lock attribute
         See jPylon documentation for details on how this works */
       registerAttribute("opt_lock", "opt_lock", "OptimisticLockAttribute");


       /* ownership_level - The level at which the reference data is owned (global, client bank, bank
         organization group, or corporate customer) */
       registerAttribute("ownership_level", "ownership_level");


       /* ownership_type - If the ownership level is set to corporate customer, the ownership type
         is set to non-admin.  If ownership level is set to anything else, the ownership
         type is admin. */
       registerAttribute("ownership_type", "ownership_type");

      /* Pointer to the parent ReferenceDataOwner */
       registerAttribute("owner_org_oid", "p_owner_org_oid", "ParentIDAttribute");
   }






}
