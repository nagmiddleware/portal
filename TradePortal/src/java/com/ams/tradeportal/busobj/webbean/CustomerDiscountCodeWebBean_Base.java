package com.ams.tradeportal.busobj.webbean;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import javax.servlet.http.*;

/**
  Invoice data information uploaded will be stored here
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class CustomerDiscountCodeWebBean_Base extends TradePortalBusinessObjectWebBean {

	/*
	 * Register the attributes and associations of the business object
	 */
	  public void registerAttributes()
	   {  
	      /* Register attributes defined in the Ancestor class */
	      super.registerAttributes();
	      /*CUSTOMER_DISC_CODE_OID*/
	      registerAttribute("customer_disc_code_oid", "customer_disc_code_oid", "ObjectIDAttribute");	      
	      /* the discount - code */
	      registerAttribute("discount_code", "discount_code");
	      /* The description */
	      registerAttribute("discount_description", "discount_description");
	      /*  customer owner oid  */
	      registerAttribute("p_owner_oid", "p_owner_oid");
		  /* Srinivasu_D CR#997 Rel9.3 03/10/2015 - Added default_disc_rate_ind
		  /*  default discount rate ind*/
		  registerAttribute("default_disc_rate_ind", "default_disc_rate_ind", "IndicatorAttribute");	
	      /*  deactivate indicator  */
	      registerAttribute("deactivate_ind", "deactivate_ind", "IndicatorAttribute");	
	       /* opt_lock - Optimistic lock attribute
	         See jPylon documentation for details on how this works */
	       registerAttribute("opt_lock", "opt_lock", "OptimisticLockAttribute");	      
	  }
		   		   
	}

