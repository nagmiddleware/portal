package com.ams.tradeportal.busobj.webbean;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import javax.servlet.http.*;

/**
 * Describes the Invoice Line Item Details.
 * 
 * Copyright � 2003 American Management Systems, Incorporated All rights
 * reserved
 */
public class InvoiceLineItemDetailWebBean_Base extends
		TradePortalBusinessObjectWebBean {
	public void registerAttributes() {
		super.registerAttributes();

		/* Register attributes defined in the Ancestor class */
		super.registerAttributes();

		/* inv_upload_definition_oid - Unique identifier */
		registerAttribute("upload_line_item_detail_oid",
				"upload_line_item_detail_oid", "ObjectIDAttribute");
		/*  line item ID at the line item details level */
		registerAttribute("line_item_id", "line_item_id");
		/*  unit price of the item being shipped under the invoice */
		//registerAttribute("unit_price", "unit_price", "NumberAttribute");
		registerAttribute("unit_price", "unit_price", "TradePortalDecimalAttribute");
		/* This is the unit of measure applicable to the price */
		registerAttribute("unit_of_measure_price", "unit_of_measure_price");
		/* invoiced quantity of the item being shipped under the invoice */
		registerAttribute("inv_quantity", "inv_quantity", "TradePortalDecimalAttribute");
		/* This is the unit of measure applicable to the quantity */
		registerAttribute("unit_of_measure_quantity",
				"unit_of_measure_quantity");
		/* user defined product characteristics fields in the PPM (and the TSU Invoice format) */
		registerAttribute("prod_chars_ud1_type", "prod_chars_ud1_type");
		registerAttribute("prod_chars_ud2_type", "prod_chars_ud2_type");
		registerAttribute("prod_chars_ud3_type", "prod_chars_ud3_type");
		registerAttribute("prod_chars_ud4_type", "prod_chars_ud4_type");
		registerAttribute("prod_chars_ud5_type", "prod_chars_ud5_type");
		registerAttribute("prod_chars_ud6_type", "prod_chars_ud6_type");
		registerAttribute("prod_chars_ud7_type", "prod_chars_ud7_type");
		/* user defined product characteristics field values in the PPM (and the TSU Invoice format) */
		registerAttribute("prod_chars_ud1_val", "prod_chars_ud1_val");
		registerAttribute("prod_chars_ud2_val", "prod_chars_ud2_val");
		registerAttribute("prod_chars_ud3_val", "prod_chars_ud3_val");
		registerAttribute("prod_chars_ud4_val", "prod_chars_ud4_val");
		registerAttribute("prod_chars_ud5_val", "prod_chars_ud5_val");
		registerAttribute("prod_chars_ud6_val", "prod_chars_ud6_val");
		registerAttribute("prod_chars_ud7_val", "prod_chars_ud7_val");
		/* invoice summary data oid */
		registerAttribute("invoice_summary_goods_oid", "p_invoice_summary_goods_oid", "ParentIDAttribute");
		registerAttribute("derived_line_item_ind", "derived_line_item_ind", "IndicatorAttribute");//SHR Cr708 Rel8.1.1
	}

}
