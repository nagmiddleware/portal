

  

package com.ams.tradeportal.busobj.webbean;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import javax.servlet.http.*;

/**
 * A business (coporate or bank) that is a party to an instrument.   The terms_party_type
 * attribute distinguishes between the different  types of parties that can
 * be associated to an instrument.
 * 
 * When a party is referenced in the Trade Portal by using the Party Search
 * page, data is copied from a 'Party' business object into a 'TermsParty'
 * business object.  Because of this, many of the attributes of this business
 * object are similar to the attributes of the Party business object.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class TermsPartyWebBean_Base extends TradePortalBusinessObjectWebBean
{

  public void registerAttributes()
   {  
      super.registerAttributes();


       /* terms_party_oid - Unique identifier */
       registerAttribute("terms_party_oid", "terms_party_oid", "ObjectIDAttribute");


       /* terms_party_type - The type of terms party.  This type describes the relationship of the party
         to the transaction.   Examples of terms party types are applicant, beneficiary,
         advising bank, etc. 
         
         Each of the terms parties that are related to a particular transaction will
         have different terms party types. */
       registerAttribute("terms_party_type", "terms_party_type");


       /* name - Name of the party. */
       registerAttribute("name", "name");


       /* address_line_1 - First line of the address for this party */
       registerAttribute("address_line_1", "address_line_1");


       /* address_line_2 - Second line of the address for this party */
       registerAttribute("address_line_2", "address_line_2");


       /* address_line_3 - Third line of the address for this party.
         
         This attribute will only be populated when the state, province, country
         and postal code attributes are not.   Some party entry fields allow for
         the entry of state/province, etc whereas some allow only for the entry of
         address_line_3.    When copying from a reference data Party object, state/province,
         country and postal code are combined together to form address line 3 if
         appropriate. */
       registerAttribute("address_line_3", "address_line_3");


       /* address_city - City of the party */
       registerAttribute("address_city", "address_city");


       /* address_state_province - State or province where the party is located. */
       registerAttribute("address_state_province", "address_state_province");


       /* address_country - Country  where the party is located. */
       registerAttribute("address_country", "address_country");


       /* address_postal_code - Postal code  where the party is located. */
       registerAttribute("address_postal_code", "address_postal_code");


       /* phone_number - Phone number of this party */
       registerAttribute("phone_number", "phone_number");


       /* OTL_customer_id - The unique identifier of this party on OTL, the back end system.   This
         value is only entered directly on the Party Detail page.  It would only
         get populated in a TermsParty object if the user searches for a party using
         the Party Search page.
         
         Care must be taken to clear out this field when appropriate.  For example,
         if a user references a party in a JSP, this copies all of the fields for
         the party (including otl_customer_id) to the terms party.  If the user then
         changes the name of the terms party, the otl_customer_id is no longer valid
         (since the user is now referring to a different party).  In this scenario,
         the otl_customer_id must be set to null. */
       registerAttribute("OTL_customer_id", "OTL_customer_id");


       /* branch_code - Branch code of this party */
       registerAttribute("branch_code", "branch_code");


       /* contact_name - Contact name for this party */
       registerAttribute("contact_name", "contact_name");


       /* acct_num - Account number */
       registerAttribute("acct_num", "acct_num");


       /* acct_currency - The currency of the account */
       registerAttribute("acct_currency", "acct_currency");


       /* acct_choices - HTML (or some other format) representing the choices available in the account
         list dropdown for this terms party */
       registerAttribute("acct_choices", "acct_choices");


       /* address_seq_num - Sequence Number of Address (1-99) */
       registerAttribute("address_seq_num", "address_seq_num", "NumberAttribute");


       /* address_search_indicator - Indicates whether Search for an Address button should be displayed.  Defaults
         to Yes. */
       registerAttribute("address_search_indicator", "address_search_indicator", "IndicatorAttribute");
       
       /* vendor_id - A 15 character alphanumeric value representing the vendor ID associated with this party.
        *             The vendor_id for the counter-party is saved to instrument.vendor_id when the transaction
        *             is saved.  This is a hidden value on the transaction screens.  The unpackager/packager
        *             will update/use instrument.vendor_id and not the values that exist on terms_party */
       registerAttribute("vendor_id", "vendor_id");
       
       registerAttribute("central_bank_rep1", "central_bank_rep1");
       registerAttribute("central_bank_rep2", "central_bank_rep2");
       registerAttribute("central_bank_rep3", "central_bank_rep3");
       registerAttribute("customer_reference", "customer_reference");
       registerAttribute("ben_bank_sort_code", "ben_bank_sort_code");//CR 709A
       /*Rpasupulati CR 1001 Start*/
       registerAttribute("reporting_code_1", "reporting_code_1");
       registerAttribute("reporting_code_2", "reporting_code_2");
       /*Rpasupulati CR 1001 End*/
		//Rel-8.4.0.1 IR# T36000028107. Backing out changes done under T36000021894 - Begin
     //Rel-8.4 IR# T36000021894 on 12/04/2013 - Begin
//       registerAttribute("guar_deliver_to", "guar_deliver_to");
//       registerAttribute("guar_deliver_by", "guar_deliver_by");
     //Rel-8.4 IR# T36000021894 on 12/04/2013 - End
		//Rel-8.4.0.1 IR# T36000028107. Backing out changes done under T36000021894 - end
       
       //Nar Rel9.5.0.0 CR-1132 01/27/2016 - Begin
	   //added attributes for Portal only Bank Additional address user defined fields
	    registerAttribute("user_defined_field_1", "user_defined_field_1");
	    registerAttribute("user_defined_field_2", "user_defined_field_2");
	    registerAttribute("user_defined_field_3", "user_defined_field_3");
	    registerAttribute("user_defined_field_4", "user_defined_field_4");
	   //Nar Rel9.5.0.0 CR-1132 01/27/2016 - End
       
   }
   
   




}
