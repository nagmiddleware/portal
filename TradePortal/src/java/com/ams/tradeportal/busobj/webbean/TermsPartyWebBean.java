package com.ams.tradeportal.busobj.webbean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.EJBObjectFactory;
import com.amsinc.ecsg.frame.QueryListView;
import com.amsinc.ecsg.frame.ReferenceDataManager;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.ResourceManager;

/*
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class TermsPartyWebBean extends TermsPartyWebBean_Base
{
	private static final Logger LOG = LoggerFactory.getLogger(TermsPartyWebBean.class);
    public void loadTermsPartyFromDoc(DocumentHandler doc) {

	 // The method takes a document containing party fields (either terms party
	 // or party) and overwrites the data in the bean with the data from the
	 // doc.  The terms_party_oid is not overwritten.

	 // The doc being sent in either came from a Party or a Terms Party.  Most
	 // of the fields are conveniently the same.  For Party doc, there is no
	 // terms_party_oid.  Therefore to ensure that the terms party oid is
	 // NOT overwritten in the passed in bean check if there is a value.  If
	 // not, don't set it.

	 if(doc == null)
          {
		// If the document is null, there is nothing to populate
		return;
 	  }

	 String value;

	 value = doc.getAttribute("/terms_party_oid");
	 if (value != null && !value.equals("")) {
		this.setAttribute("terms_party_oid", value);
	 }

	 value = doc.getAttribute("/name");
	 if (value == null) value = "";
	 this.setAttribute("name", value);

	 value = doc.getAttribute("/address_line_1");
	 if (value == null) value = "";
	 this.setAttribute("address_line_1", value);

	 value = doc.getAttribute("/address_line_2");
	 if (value == null) value = "";
	 this.setAttribute("address_line_2",value);

	 value = doc.getAttribute("/address_city");
	 if (value == null) value = "";
	 this.setAttribute("address_city", value);

	 value = doc.getAttribute("/address_state_province");
	 if (value == null) value = "";
	 this.setAttribute("address_state_province", value);

	 value = doc.getAttribute("/address_country");
	 if (value == null) value = "";
	 this.setAttribute("address_country", value);

	 value = doc.getAttribute("/address_postal_code");
	 if (value == null) value = "";
	 this.setAttribute("address_postal_code", value);

	 value = doc.getAttribute("/phone_number");
	 if (value == null) value = "";
	 this.setAttribute("phone_number", value);

	 value = doc.getAttribute("/OTL_customer_id");
	 if (value == null) value = "";
	 this.setAttribute("OTL_customer_id", value);

	 value = doc.getAttribute("/acct_num");
	 if (value == null) value = "";
	 this.setAttribute("acct_num", value);

	 value = doc.getAttribute("/acct_currency");
	 if (value == null) value = "";
	 this.setAttribute("acct_currency", value);

	 value = doc.getAttribute("/acct_choices");
	 if (value == null) value = "";

         // TLE - 11/03/06 - IR#AYUG110347318 Add Begin
         //this.setAttribute("acct_choices", value);
         if (!value.equals("")) {
	    this.setAttribute("acct_choices", value);
         }
         // TLE - 11/03/06 - IR#AYUG110347318 Add Begin

	 //rbhaduri - 31st July 06 - SIUG063066511
	 value = doc.getAttribute("/address_seq_num");
	 if (value == null) value = "";
	 this.setAttribute("address_seq_num", value);

	 // CR383
	 value = doc.getAttribute("/vendor_id");
	 if (value == null) value = "";
	 this.setAttribute("vendor_id", value);

	 // ELUG101059432
	 value = doc.getAttribute("/contact_name");
	 if (value == null) value = "";
	 this.setAttribute("contact_name", value);
   }

   public void loadTermsPartyFromDocTagsOnly(DocumentHandler doc)
   {
	 // The method takes a document containing party fields (either terms party
	 // or party) and overwrites the data in the bean with the data from the
	 // doc. If a tag is not found in the document, it will not be set at all in
	 // the web bean. The terms_party_oid is not overwritten.

	 if(doc == null)
          {
		// If the document is null, there is nothing to populate
		return;
 	  }

	 String value;

	 value = doc.getAttribute("/terms_party_oid");
	 if (value != null && !value.equals("")) {
		this.setAttribute("terms_party_oid", value);
	 }

	 value = doc.getAttribute("/name");
     if (value != null)
     {
	    this.setAttribute("name", value);
	 }

	 value = doc.getAttribute("/address_line_1");
	 if (value != null)
	 {
	    this.setAttribute("address_line_1", value);
	 }

	 value = doc.getAttribute("/address_line_2");
	 if (value != null)
	 {
	    this.setAttribute("address_line_2",value);
	 }

	 value = doc.getAttribute("/address_line_3");
	 if (value != null)
	 {
	    this.setAttribute("address_line_3",value);
	 }

	 value = doc.getAttribute("/address_city");
	 if (value != null)
	 {
	    this.setAttribute("address_city", value);
	 }

	 value = doc.getAttribute("/address_state_province");
	 if (value != null)
	 {
	    this.setAttribute("address_state_province", value);
	 }

	 value = doc.getAttribute("/address_country");
	 if (value != null)
	 {
	    this.setAttribute("address_country", value);
	 }

	 value = doc.getAttribute("/address_postal_code");
	 if (value != null)
	 {
	    this.setAttribute("address_postal_code", value);
	 }

	 value = doc.getAttribute("/phone_number");
	 if (value != null)
	 {
	    this.setAttribute("phone_number", value);
	 }

	 value = doc.getAttribute("/OTL_customer_id");
	 if (value != null)
	 {
	    this.setAttribute("OTL_customer_id", value);
	 }

	 value = doc.getAttribute("/acct_num");
	 if (value != null)
       {
	    this.setAttribute("acct_num", value);
	 }

	 value = doc.getAttribute("/acct_currency");
	 if (value != null)
       {
	    this.setAttribute("acct_currency", value);
	 }

	 value = doc.getAttribute("/acct_choices");

	 if (value != null && !value.equals(""))
       {
	    this.setAttribute("acct_choices", value);
	 }




	//rbhaduri - 31st July 06 - SIUG063066511
	value = doc.getAttribute("/address_seq_num");
	if (value == null) value = "";
	this.setAttribute("address_seq_num", value);

	// CR383
	value = doc.getAttribute("/vendor_id");
	if (value == null) value = "";
	this.setAttribute("vendor_id", value);

	// ELUG101059432
	value = doc.getAttribute("/contact_name");
	if (value == null) value = "";
	this.setAttribute("contact_name", value);
   }


   //IAZ CM-451 10/29/08 Begin
   public void loadAcctChoices(String partyOid, String serverLocation, ResourceManager resMgr)
   {
	   loadAcctChoices(partyOid, serverLocation, resMgr, null, null, false);
   }

   //IAZ CM-451 12/11/08 Begin
   public void loadAcctChoices(String partyOid, String serverLocation, ResourceManager resMgr, Vector availability)
   {
	   loadAcctChoices(partyOid, serverLocation, resMgr, availability, null, false);
   }
   
   public void loadAcctChoices(String partyOid, String serverLocation, ResourceManager resMgr, Vector availability, String userOid)
   {
	   loadAcctChoices(partyOid, serverLocation, resMgr, availability, userOid, false);
   }

   //public void loadAcctChoices(String partyOid, String serverLocation, ResourceManager resMgr, String availability)
   public void loadAcctChoices(String partyOid, String serverLocation, ResourceManager resMgr, Vector availability, String userOid, boolean isSettlementInstr)
   {
   //IAZ CM-451 10/29/08 and 12/11/08 End

	 // The method takes a party oid and queries the database for accounts belonging
	 // to the party.  The resulting ResultSetRecord XML is written to the acct_choices
	 // attribute.  The format of this XML is
	 // like a result set returned from a query.
	 //    <ResultSetRecord ID="1">
	 //      <ACCOUNT_NUMBER>xxxxx</ACCOUNT_NUMBER>
	 //      <CURRENCY>xxx</CURRENCY>
	 //    </ResultSetRecord>

     // The optional input parameter is userOid which is provided if the user is not logged as bank admin or does not perform
     // subsidiary access. In this case, teh result set also contains
     //   <ACCOUNT_DESCRIPTION>user sepcific account // description<ACCOUNT_DESCRIPTION>

     // The optional Vector availability contains a set of availability checks to perorm (available for credit, available
     // for dom payment, etc) and exclude account from the result set if it's condition is not met.
     // Note: if the availability parameter is started with '!' (e.g., !deactivate_account), method will perform reverse check
     // (e.g., will exclude account from the result set if condition is met)

	   List<Object> sqlPrmsLst = new ArrayList();
	StringBuilder    sql           = new StringBuilder();
	QueryListView   queryListView = null;
      DocumentHandler acctList      = new DocumentHandler();

	try {
	      queryListView = (QueryListView) EJBObjectFactory.createClientEJB(
					serverLocation, "QueryListView");
			//AAlubala CR 610 Rel 7.0.0.0 2.18 02/26/2011 - added  p_owner_oid and a_op_bank_org_oid
	      sql.append("select account_oid, account_number, currency, a_op_bank_org_oid, OTHER_ACCOUNT_OWNER_OID ");
      	  //sql.append(" from account");
      	  if (userOid != null)
      	  {
      			 sql.append(",ACCOUNT_DESCRIPTION from account, user_authorized_acct");
		  }
		  else
		  {
			if ( isSettlementInstr ) { //CR-818 settlement instruction will display description from account table
				 sql.append(",ACCOUNT_NAME as ACCOUNT_DESCRIPTION from account");
			} else {
			  sql.append(" from account");
			}
		  }
      	  // IR-45609
	      sql.append(" where p_owner_oid = ? ");
	      sqlPrmsLst.add(partyOid);
	      // CR -818 - load only settlement account for settlement instrcution
	      if ( isSettlementInstr && availability == null ) {
	    	  sql.append(" and available_for_settle_instr = ? ");
	    	  sqlPrmsLst.add("Y");
	      }
	      //IAZ CM-451 10/29/08 and 12/11/08 Begin
	      if (availability != null)
	      {
	        // IAZ CR-483 08/07/09 Begin
			String curAvailability;
			String curIndicator;
			// IAZ CR-483 08/07/09 Add

	        int acMax = availability.size();
        	//out.println(acMax);
        	for (int acIndex = 0; acIndex < acMax; acIndex++)
        	{
        	    // IAZ CR-483 08/07/09 Begin
				curIndicator = "=";
				curAvailability = ((String)availability.get(acIndex));
				if (curAvailability.charAt(0) == '!') {
					curAvailability = curAvailability.substring(1);
					curIndicator = "!=";
			    }
				//sql.append(" and " + availability.get(acIndex) + " = 'Y' ");
				sql.append(" and " + curAvailability + " " + curIndicator + " 'Y'");
				// IAZ CR-483 08/07/09 Begin
        	}

	      }
      	  if (userOid != null)
      	  {
      	  	sql.append(" and account_oid = a_account_oid and p_user_oid = ? ");
      	  	sqlPrmsLst.add(userOid);
		  }
	      //IAZ CM-451 End

      	sql.append(" order by ");
	      sql.append(resMgr.localizeOrderBy("account_number"));

      	queryListView.setSQL(sql.toString(), sqlPrmsLst);

	      queryListView.getRecords();

	      acctList = queryListView.getXmlResultSet();	      

		this.setAttribute("acct_choices", acctList.toString());

	} catch (Exception e) {
		LOG.error("TermsPartyWebBean: Exception while loading acct choices: ",e);
	} finally {
      	try {
	          if (queryListView != null) {
      	        queryListView.remove();
	          }
      	} catch (Exception e) {
	          LOG.error("error removing querylistview in loadAcctChoices",e);
	      }
	}  // try/catch/finally block

   }



   public String buildAddress(boolean readOnly, ResourceManager resMgr)
    {
       return buildAddress(readOnly, false, resMgr);
    }


   public String buildAddress(boolean readOnly, boolean useAddressLine3, ResourceManager resMgr) {
	 // Constructs a formatted address from the data in the bean.

	 StringBuffer address = new StringBuffer("");
	 String eol = "\n";
	 boolean addFourthLine = false;

	 if (readOnly) eol = "<BR>";

	 try {
	   address.append(this.getAttribute("name"));
	   address.append(eol);
	   address.append(this.getAttribute("address_line_1"));
	   address.append(eol);

	   if (!this.getAttribute("address_line_2").equals("")) {
		 address.append(this.getAttribute("address_line_2"));
		 address.append(eol);
	   } else {
		 addFourthLine = true;
	   }

  	   if(useAddressLine3)
        {
		    address.append(this.getAttribute("address_line_3"));
        }
       else
        {
		   String city = this.getAttribute("address_city");
		   address.append(city);
		   if (!city.equals("")) address.append(", ");
		   address.append(this.getAttribute("address_state_province"));
		   address.append(" ");
		   address.append(this.getAttribute("address_postal_code"));
        }
	   address.append(" ");

	   // Append the description for the country
	   try {
	         //IR - FDUH121457510 & GYUH060564111 12/19/2007 Krishna Begin
             /*
               Commented the following and rewritten bellow that with modification.
	           address.append(ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.COUNTRY,
	                                                                     this.getAttribute("address_country"),
	                                                                     resMgr.getCSDB().getLocaleName()));
              */
		   String addressCountry =      ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.COUNTRY,
                                                                        this.getAttribute("address_country"),
                                                                        resMgr.getCSDB().getLocaleName());

		   if(addressCountry == null)
		   address.append(this.getAttribute("address_country"));
		   else
	       address.append(addressCountry);
		   //IR - FDUH121457510 & GYUH060564111 12/19/2007 Krishna End
	   } catch (AmsException amse) {
		    /*
			IR - FDUH121457510 & GYUH060564111 Krishna Begin
		    When description is not found getDescr(String tableType, String code, String localeName)
		    function of ReferenceDataManager.java used to to throw the AMSException.So,whenever Exception is
			thrown  that finally ends up here.Here 'null' value is being overwritten through the code
			*address.append(this.getAttribute("address_country"))* - But,due to the IR-GYUH060564111 which states                     the requirement that links in the Page should work even if description is not found.
		    With this,now getDescr(String tableType, String code, String localeName) returns 'null'
		    instead of throwing Exception in contrast to the earlier behaviour.So,following line is commented                          below and added in the try block itself by checking description is equal to null.                         below and added in the try block itself by checking description is equal to null.
		    */
	        //address.append(this.getAttribute("address_country"));
		    //IR - FDUH121457510 & GYUH060564111 Krishna End
	   }


	   // Ensure a consistent size of the address (4 lines)
	   if (addFourthLine) {
		 address.append(eol);
		 address.append("&nbsp;");
	   }

	 } catch (NullPointerException e) {
	 }

	 return address.toString();
  }




/**
 * Convenience method for combining the city, state, and postal
 * code into one value and placing it in the address line 3 attribute.
 *
 */
public void createAddressLine3(ResourceManager resMgr) {
	StringBuffer line3 = new StringBuffer(this.getAttribute("address_city"));
	line3.append(", ");
	line3.append(this.getAttribute("address_state_province"));
	line3.append(" ");
	line3.append(this.getAttribute("address_postal_code"));
        line3.append(" ");
	line3.append(this.getAttribute("address_country"));
	this.setAttribute("address_line_3", line3.toString());
}

}