

  

package com.ams.tradeportal.busobj.webbean;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import javax.servlet.http.*;
import com.ams.tradeportal.html.*;

/**
 * Abstract business object to represent an organization in the Trade Portal.
 * All organizations are subclasses of this business object.
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class AnnouncementWebBean_Base extends TradePortalBusinessObjectWebBean
{

   public void registerAttributes()
   {  
      super.registerAttributes();

      /* announcement_oid - Unique identifier */
      registerAttribute("announcement_oid", "announcement_oid", "ObjectIDAttribute");
      
      /* title - a short blurb that provides the essence of the announcement. */
      registerAttribute("title", "title");
      
      /* subject - the text of the announcement. */
      registerAttribute("subject", "subject");
      
      /* status - announcement status. */
      registerAttribute("announcement_status", "announcement_status");
      
      /* start_date - the date the announcement should first be displayed to users */
      registerAttribute("start_date", "start_date", "DateAttribute");
      
      /* end_date - the last day the announcement should be displayed to users */
      registerAttribute("end_date", "end_date", "DateAttribute");

      /* critical - the last day the announcement should be displayed to users */
      registerAttribute("critical_ind", "critical_ind", "IndicatorAttribute");
            
      /* Pointer to the parent ReferenceDataOwner */
      registerAttribute("owner_org_oid", "p_owner_org_oid", "ParentIDAttribute");
   
      /* ownership_level - The level at which the reference data is owned (global, client bank, bank
         organization group, or corporate customer */
      registerAttribute("ownership_level", "ownership_level");

      /* does the announcement apply to all bank org groups?  this only 
       * applies when the ownership_level=client bank */
      registerAttribute("all_bogs_ind", "all_bogs_ind", "IndicatorAttribute");

      /* opt_lock - Optimistic lock attribute
         See jPylon documentation for details on how this works */
      registerAttribute("opt_lock", "opt_lock", "OptimisticLockAttribute");
   }
   
}
