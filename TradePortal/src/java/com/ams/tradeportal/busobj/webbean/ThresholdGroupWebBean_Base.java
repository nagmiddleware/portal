



package com.ams.tradeportal.busobj.webbean;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import javax.servlet.http.*;

/**
 * A threshold group is a set of threshold settings.  There are two types of
 * thresholds: a LC threshold and a daily limit threshold.  An LC threshold
 * a rule that indicates the maximum value of an LC that the user can authorize.
 * A daily limit threshold is the maximum total value that a user can authorize
 * in a single day.
 *
 * An LC threshold and a daily threshold exists for every type of instrument
 * type / transaction type combination that can be created in the Trade Portal.
 *
 *     Copyright  � 2003
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class ThresholdGroupWebBean_Base extends TradePortalBusinessObjectWebBean
{

  public void registerAttributes()
   {
      super.registerAttributes();


       /* threshold_group_oid - Unique identifier */
       registerAttribute("threshold_group_oid", "threshold_group_oid", "ObjectIDAttribute");


       /* threshold_group_name - Name of the threshold group */
       registerAttribute("threshold_group_name", "threshold_group_name");


       /* import_LC_issue_thold - LC threshold for an Issue Import LC transaction */
       registerAttribute("import_LC_issue_thold", "import_LC_issue_thold", "TradePortalDecimalAttribute");


       /* import_LC_amend_thold - LC threshold for an Amend Import LC */
       registerAttribute("import_LC_amend_thold", "import_LC_amend_thold", "TradePortalDecimalAttribute");


       /* import_LC_discr_thold - LC threshold for an Import LC Discrepancy Response */
       registerAttribute("import_LC_discr_thold", "import_LC_discr_thold", "TradePortalDecimalAttribute");


       /* standby_LC_issue_thold - LC threshold for an Issue Standby LC */
       registerAttribute("standby_LC_issue_thold", "standby_LC_issue_thold", "TradePortalDecimalAttribute");


       /* standby_LC_amend_thold - LC threshold for an Amend Standby LC */
       registerAttribute("standby_LC_amend_thold", "standby_LC_amend_thold", "TradePortalDecimalAttribute");


       /* standby_LC_discr_thold - LC threshold for a Standby LC Discrepancy Response */
       registerAttribute("standby_LC_discr_thold", "standby_LC_discr_thold", "TradePortalDecimalAttribute");


       /* export_LC_issue_thold - LC threshold for an Export LC Issue Transfer transaction */
       registerAttribute("export_LC_issue_thold", "export_LC_issue_thold", "TradePortalDecimalAttribute");


       /* export_LC_amend_transfer_thold - LC threshold for an Export LC Amend Transfer  transaction */
       registerAttribute("export_LC_amend_transfer_thold", "export_LC_amend_transfer_thold", "TradePortalDecimalAttribute");


       /* export_LC_issue_assign_thold - LC threshold for an Export LC Assignment of Proceeds transaction */
       registerAttribute("export_LC_issue_assign_thold", "export_LC_issue_assign_thold", "TradePortalDecimalAttribute");


       /* export_LC_discr_thold - LC threshold for an Export LC Discrepancy Response transaction */
       registerAttribute("export_LC_discr_thold", "export_LC_discr_thold", "TradePortalDecimalAttribute");


       /* guarantee_issue_thold - LC threshold for an Issue Guarantee transaction */
       registerAttribute("guarantee_issue_thold", "guarantee_issue_thold", "TradePortalDecimalAttribute");


       /* guarantee_amend_thold - LC threshold for an Amend Guarantee transaction */
       registerAttribute("guarantee_amend_thold", "guarantee_amend_thold", "TradePortalDecimalAttribute");


       /* airwaybill_issue_thold - LC threshold for an Air Waybill Release transaction */
       registerAttribute("airwaybill_issue_thold", "airwaybill_issue_thold", "TradePortalDecimalAttribute");


       /* ship_guarantee_issue_thold - LC threshold for an Issue Shipping Guarantee transaction */
       registerAttribute("ship_guarantee_issue_thold", "ship_guarantee_issue_thold", "TradePortalDecimalAttribute");


       /* export_collection_issue_thold - LC threshold for an Issue Direct Send Collection transaction.   (Note the
         proper Export Collection from TPS perspective is new_export_coll_issue_thold.) */
       registerAttribute("export_collection_issue_thold", "export_collection_issue_thold", "TradePortalDecimalAttribute");


       /* export_collection_amend_thold - LC threshold for an Amend Direct Send Collection transaction.  (Note the
         proper Export Collection from TPS perspective is new_export_coll_amend_thold.) */
       registerAttribute("export_collection_amend_thold", "export_collection_amend_thold", "TradePortalDecimalAttribute");


       /* inc_standby_LC_discr_thold - LC threshold for an Incoming Standby Discrepancy Reponse */
       registerAttribute("inc_standby_LC_discr_thold", "inc_standby_LC_discr_thold", "TradePortalDecimalAttribute");


       /* loan_request_issue_thold - LC threshold for a Loan Request Issue */
       registerAttribute("loan_request_issue_thold", "loan_request_issue_thold", "TradePortalDecimalAttribute");


       /* funds_transfer_issue_thold - Threshold for Funds Transfer Issue */
       registerAttribute("funds_transfer_issue_thold", "funds_transfer_issue_thold", "TradePortalDecimalAttribute");


       /* import_LC_discr_dlimit - Daily limit threshold for an Import LC Discrepancy Response transaction. */
       registerAttribute("import_LC_discr_dlimit", "import_LC_discr_dlimit", "TradePortalDecimalAttribute");


       /* import_LC_issue_dlimit - Daily limit threshold for an Issue Import LC  transaction. */
       registerAttribute("import_LC_issue_dlimit", "import_LC_issue_dlimit", "TradePortalDecimalAttribute");


       /* import_LC_amend_dlimit - Daily limit threshold for an Amend Import LC transaction. */
       registerAttribute("import_LC_amend_dlimit", "import_LC_amend_dlimit", "TradePortalDecimalAttribute");


       /* standby_LC_issue_dlimit - Daily limit threshold for an Issue Standby LC transaction. */
       registerAttribute("standby_LC_issue_dlimit", "standby_LC_issue_dlimit", "TradePortalDecimalAttribute");


       /* standby_LC_amend_dlimit - Daily limit threshold for an Amend Standby LC transaction. */
       registerAttribute("standby_LC_amend_dlimit", "standby_LC_amend_dlimit", "TradePortalDecimalAttribute");


       /* standby_LC_discr_dlimit - Daily limit threshold for a Standby LC Discrepancy Reponse transaction. */
       registerAttribute("standby_LC_discr_dlimit", "standby_LC_discr_dlimit", "TradePortalDecimalAttribute");


       /* export_LC_issue_dlimit - Daily limit threshold for an Issue Transfer Export  LC  transaction. */
       registerAttribute("export_LC_issue_dlimit", "export_LC_issue_dlimit", "TradePortalDecimalAttribute");


       /* export_LC_amend_transfer_dlim - Daily limit threshold for an Export LC Amend Transfer  transaction. */
       registerAttribute("export_LC_amend_transfer_dlim", "export_LC_amend_transfer_dlim", "TradePortalDecimalAttribute");


       /* export_LC_issue_assign_dlimit - Daily limit threshold for an Export LC Assignment of Proceeds transaction. */
       registerAttribute("export_LC_issue_assign_dlimit", "export_LC_issue_assign_dlimit", "TradePortalDecimalAttribute");


       /* export_LC_discr_dlimit - Daily limit threshold for an Export LC Discrepancy Response transaction. */
       registerAttribute("export_LC_discr_dlimit", "export_LC_discr_dlimit", "TradePortalDecimalAttribute");


       /* guarantee_issue_dlimit - Daily limit threshold for an Issue Guarantee transaction. */
       registerAttribute("guarantee_issue_dlimit", "guarantee_issue_dlimit", "TradePortalDecimalAttribute");


       /* guarantee_amend_dlimit - Daily limit threshold for an Amend Guarantee transaction. */
       registerAttribute("guarantee_amend_dlimit", "guarantee_amend_dlimit", "TradePortalDecimalAttribute");


       /* airwaybill_issue_dlimit - Daily limit threshold for an Air Waybill release transaction. */
       registerAttribute("airwaybill_issue_dlimit", "airwaybill_issue_dlimit", "TradePortalDecimalAttribute");


       /* ship_guarantee_issue_dlimit - Daily limit threshold for an Issue Shipping Guarantee transaction. */
       registerAttribute("ship_guarantee_issue_dlimit", "ship_guarantee_issue_dlimit", "TradePortalDecimalAttribute");


       /* export_collection_issue_dlimit - Daily limit threshold for an Issue Direct Send Collection transaction. 
         (Note new_export_collection_issue_dlimit is the proper Export Collection.) */
       registerAttribute("export_collection_issue_dlimit", "export_collection_issue_dlimit", "TradePortalDecimalAttribute");


       /* export_collection_amend_dlimit - Daily limit threshold for an Amend Export Collection  transaction.  (Note
         new_export_collection_amend_dlimit is the proper export collection.) */
       registerAttribute("export_collection_amend_dlimit", "export_collection_amend_dlimit", "TradePortalDecimalAttribute");


       /* inc_standby_LC_discr_dlimit - Daily limit threshold for an Incoming Standby Discrepancy Response transaction. */
       registerAttribute("inc_standby_LC_discr_dlimit", "inc_standby_LC_discr_dlimit", "TradePortalDecimalAttribute");


       /* opt_lock - Optimistic lock attribute
         See jPylon documentation for details on how this works */
       registerAttribute("opt_lock", "opt_lock", "OptimisticLockAttribute");


       /* loan_request_issue_dlimit - Daily limit threshold for an Loan Request issue transaction */
       registerAttribute("loan_request_issue_dlimit", "loan_request_issue_dlimit", "TradePortalDecimalAttribute");


       /* funds_transfer_issue_dlimit - Daily limit threshold for an Funds Transfer Issue transaction. */
       registerAttribute("funds_transfer_issue_dlimit", "funds_transfer_issue_dlimit", "TradePortalDecimalAttribute");


       /* request_advise_issue_thold - Threshold amount for Request to Advise Issue transaction */
       registerAttribute("request_advise_issue_thold", "request_advise_issue_thold", "TradePortalDecimalAttribute");


       /* request_advise_amend_thold - Threshold amount for Request to Advise Amend transaction */
       registerAttribute("request_advise_amend_thold", "request_advise_amend_thold", "TradePortalDecimalAttribute");


       /* request_advise_discr_thold - Threshold amount for Request to Advise Discrepancy Response transaction */
       registerAttribute("request_advise_discr_thold", "request_advise_discr_thold", "TradePortalDecimalAttribute");


       /* request_advise_issue_dlimit - Daily limit threshold for an Issue Request to Advise transaction */
       registerAttribute("request_advise_issue_dlimit", "request_advise_issue_dlimit", "TradePortalDecimalAttribute");


       /* request_advise_amend_dlimit - Daily limit threshold for an Amend Request to Advise transaction */
       registerAttribute("request_advise_amend_dlimit", "request_advise_amend_dlimit", "TradePortalDecimalAttribute");


       /* request_advise_discr_dlimit - Daily limit threshold for a Discrpancy Response Request to Advise transaction */
       registerAttribute("request_advise_discr_dlimit", "request_advise_discr_dlimit", "TradePortalDecimalAttribute");

       //Krishna CR 375-D ATP 07/19/2007 Begin
       /* approval_to_pay_issue_thold - Threshold amount for Approval to Pay Issue transaction */
       registerAttribute("approval_to_pay_issue_thold", "approval_to_pay_issue_thold", "TradePortalDecimalAttribute");


       /* approval_to_pay_amend_thold - Threshold amount for Approval to Pay Amend transaction */
       registerAttribute("approval_to_pay_amend_thold", "approval_to_pay_amend_thold", "TradePortalDecimalAttribute");


       /* approval_to_pay_discr_thold - Threshold amount for Approval to Pay Discrepancy Response transaction */
       registerAttribute("approval_to_pay_discr_thold", "approval_to_pay_discr_thold", "TradePortalDecimalAttribute");


       /* approval_to_pay_issue_dlimit - Daily limit threshold for an Issue Approval to Pay transaction */
       registerAttribute("approval_to_pay_issue_dlimit", "approval_to_pay_issue_dlimit", "TradePortalDecimalAttribute");


       /* approval_to_pay_amend_dlimit - Daily limit threshold for an Amend Approval to Pay transaction */
       registerAttribute("approval_to_pay_amend_dlimit", "approval_to_pay_amend_dlimit", "TradePortalDecimalAttribute");


       /* approval_to_pay_discr_dlimit - Daily limit threshold for a Discrpancy Response Approval to Pay transaction */
       registerAttribute("approval_to_pay_discr_dlimit", "approval_to_pay_discr_dlimit", "TradePortalDecimalAttribute");

       //Krishna CR 375-D ATP 07/19/2007 End
	//Pratiksha CR-434 ARM 09/29/2008 Begin
	/* ar_match_response_thold - Threshold amount for AR Match Response */
       registerAttribute("ar_match_response_thold", "ar_match_response_thold", "TradePortalDecimalAttribute");

       /* ar_match_response_dlimit - Daily limit threshold for AR Match Response. */
       registerAttribute("ar_match_response_dlimit", "ar_match_response_dlimit", "TradePortalDecimalAttribute");

       /* ar_approve_discount_thold - Threshold amount for AR approve discount */
       registerAttribute("ar_approve_discount_thold", "ar_approve_discount_thold", "TradePortalDecimalAttribute");

       /* ar_approve_discount_dlimit - Daily limit threshold for AR Approve Discount */
       registerAttribute("ar_approve_discount_dlimit", "ar_approve_discount_dlimit", "TradePortalDecimalAttribute");

	   /* ar_close_invoice_thold - Threshold amount for AR Close Invoice */
       registerAttribute("ar_close_invoice_thold", "ar_close_invoice_thold", "TradePortalDecimalAttribute");

       /* ar_close_invoice_dlimit - Daily limit threshold for AR Close Invoice */
       registerAttribute("ar_close_invoice_dlimit", "ar_close_invoice_dlimit", "TradePortalDecimalAttribute");

	   /* ar_finance_invoice_thold - Threshold amount for AR Finance Invoice */
       registerAttribute("ar_finance_invoice_thold", "ar_finance_invoice_thold", "TradePortalDecimalAttribute");

       /* ar_finance_invoice_dlimit - Daily limit threshold for AR Finance Invoice */
       registerAttribute("ar_finance_invoice_dlimit", "ar_finance_invoice_dlimit", "TradePortalDecimalAttribute");

	   //Pratiksha CR-434 ARM 09/29/2008 End
	   /* ar_dispute_invoice_thold - Threshold amount for AR dispute/undispute invoice. */
	   registerAttribute("ar_dispute_invoice_thold", "ar_dispute_invoice_thold", "TradePortalDecimalAttribute");


	   /* ar_dispute_invoice_dlimit - Daily limit threshold for AR Dispute/Undispute Invoice */
       registerAttribute("ar_dispute_invoice_dlimit", "ar_dispute_invoice_dlimit", "TradePortalDecimalAttribute");
	   /* transfer_btw_accts_issue_thold - Threshold amount for Transfer Between Account Issue transaction. */
	   registerAttribute("transfer_btw_accts_issue_thold", "transfer_btw_accts_issue_thold", "TradePortalDecimalAttribute");


	   /* transfer_btw_accts_issue_dlimit - Daily limit threshold for Transfer Between Accounts Issue transaction. */
	    registerAttribute("transfer_btw_accts_issue_dlimit", "transfer_btw_accts_iss_dlimit", "TradePortalDecimalAttribute");


	   /* domestic_payment_issue_thold - Threshold amount for Domestic Payment Issue transaction. */
	    registerAttribute("domestic_payment_issue_thold", "domestic_payment_issue_thold", "TradePortalDecimalAttribute");


	   /* domestic_payment_issue_dlimit - Daily limit threshold for Domestic Payment Issue transaction. */
       registerAttribute("domestic_payment_issue_dlimit", "domestic_payment_issue_dlimit", "TradePortalDecimalAttribute");



	 /* direct_debit_thold - Threshold amount for Direct Debit Issue. */
	   registerAttribute("direct_debit_thold", "direct_debit_thold", "TradePortalDecimalAttribute");


	   /* direct_debit_dlimit - Daily limit threshold for Direct Debit Issue */
       registerAttribute("direct_debit_dlimit", "direct_debit_dlimit", "TradePortalDecimalAttribute");
	  

       /* new_export_coll_issue_thold - Instrument threshold for an Issue Export Collection transaction.   (Note
         export_collection_issue_thold is for Direct Send Collection from TPS perspective.) */
       registerAttribute("new_export_coll_issue_thold", "new_export_coll_issue_thold", "TradePortalDecimalAttribute");


       /* new_export_coll_amend_thold - Instrument threshold for an Amend Export Collection transaction.   (Note
         export_collection_amend_thold is for Direct Send Collection from TPS perspective.) */
       registerAttribute("new_export_coll_amend_thold", "new_export_coll_amend_thold", "TradePortalDecimalAttribute");


       /* new_export_coll_issue_dlimit - Daily limit threshold for an Issue Export Collection transaction.  (Note
         this is the proper export colleciton.  export_collection_issue_dlimit is
         only Direct Send Collection.) */
       registerAttribute("new_export_coll_issue_dlimit", "new_export_coll_issue_dlimit", "TradePortalDecimalAttribute");

       /* new_export_coll_amend_dlimit - Daily limit threshold for an Amend Export Collection transaction.  (Note
         this is the proper export colleciton.  export_collection_amend_dlimit is
         only Direct Send Collection.) */
       registerAttribute("new_export_coll_amend_dlimit", "new_export_coll_amend_dlimit", "TradePortalDecimalAttribute");
       
       //Narayan CR-913 03-Feb-2014 Rel9.0 ADD- BEGIN      
       /* upload_rec_inv_thold - Threshold amount for Uploaded Receivable Invoices */
       registerAttribute("upload_rec_inv_thold", "upload_rec_inv_thold", "TradePortalDecimalAttribute");

       /* upload_rec_inv_dlimit - Daily limit threshold for Uploaded Receivable Invoices */
       registerAttribute("upload_rec_inv_dlimit", "upload_rec_inv_dlimit", "TradePortalDecimalAttribute");
       
       /* upload_pay_inv_thold - Threshold amount for Uploaded Payable Invoices */
       registerAttribute("upload_pay_inv_thold", "upload_pay_inv_thold", "TradePortalDecimalAttribute");

       /* upload_pay_inv_dlimit - Daily limit threshold for Uploaded Payable Invoices */
       registerAttribute("upload_pay_inv_dlimit", "upload_pay_inv_dlimit", "TradePortalDecimalAttribute");
       
       /* pyb_mgm_inv_thold - Threshold amount for Payable  Management Invoices */
       registerAttribute("pyb_mgm_inv_thold", "pyb_mgm_inv_thold", "TradePortalDecimalAttribute");

       /* pyb_mgm_inv_dlimit - Daily limit threshold for Payable  Management Invoices */
       registerAttribute("pyb_mgm_inv_dlimit", "pyb_mgm_inv_dlimit", "TradePortalDecimalAttribute");
      //Narayan CR-913 03-Feb-2014 Rel9.0 ADD- END
       
       //vdesingu - CR-914A - Rel 9.2 - register attributes - Start
       /* pay_credit_note_thold - Threshold amount for Payables Credit Note */
       registerAttribute("pay_credit_note_thold", "pay_credit_note_thold", "TradePortalDecimalAttribute");

       /* pay_credit_note_dlimit - Daily limit threshold for Payables Credit Note */
       registerAttribute("pay_credit_note_dlimit", "pay_credit_note_dlimit", "TradePortalDecimalAttribute");
       //vdesingu - CR-914A - Rel 9.2 - Added attributes - End

      /* Pointer to the parent CorporateOrganization */
       registerAttribute("corp_org_oid", "p_corp_org_oid", "ParentIDAttribute");
       
       //SSikhakolli - Rel-9.4 CR-818 - adding new attributes
       registerAttribute("imp_col_set_instr_thold", "imp_col_set_instr_thold", "TradePortalDecimalAttribute");
       registerAttribute("imp_col_set_instr_dlimit", "imp_col_set_instr_dlimit", "TradePortalDecimalAttribute");
       registerAttribute("loan_request_set_instr_dlimit", "loan_request_set_instr_dlimit", "TradePortalDecimalAttribute");
       registerAttribute("loan_request_set_instr_thold", "loan_request_set_instr_thold", "TradePortalDecimalAttribute");
   }
}