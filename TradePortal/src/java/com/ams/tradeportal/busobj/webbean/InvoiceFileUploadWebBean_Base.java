package com.ams.tradeportal.busobj.webbean;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import javax.servlet.http.*;

/**
 * The Invoice Files that are to be uploaded.
 * 
 * Copyright � 2003 American Management Systems, Incorporated All rights
 * reserved
 */
public class InvoiceFileUploadWebBean_Base extends
		com.amsinc.ecsg.web.AmsEntityWebBean {

	public void registerAttributes() {

		/* invoice_file_upload_oid - Unique Identifider */
		registerAttribute("invoice_file_upload_oid", "invoice_file_upload_oid",
				"ObjectIDAttribute");

		/* invoice_file_name - Payment File Name. */
		registerAttribute("invoice_file_name", "invoice_file_name");

		/* validation_status - The status of Invoice File upload validation. */
		registerAttribute("validation_status", "validation_status");

		/*
		 * number_of_invoices_uploaded - The number of invoices successfully
		 * uploaded.
		 */
		registerAttribute("number_of_invoices_uploaded",
				"number_of_invoices_uploaded", "NumberAttribute");

		/*
		 * number_of_invoices_failed - Number of invoices that failed for
		 * upload.
		 */
		registerAttribute("number_of_invoices_failed",
				"number_of_invoices_failed", "NumberAttribute");

		/* creation_timestamp - Date/Time when the file is uploaded. */
		registerAttribute("creation_timestamp", "creation_timestamp",
				"DateTimeAttribute");

		/* completion_timestamp - Date/Time when the validation is completed. */
		registerAttribute("completion_timestamp", "completion_timestamp",
				"DateTimeAttribute");

		/*
		 * agent_id - The name of the upload agent that processes/validates the
		 * file. This prevents multiple agents from accessing the same file.
		 */
		registerAttribute("agent_id", "agent_id");

		/* deleted_ind - Whether the file is deleted. */
		registerAttribute("deleted_ind", "deleted_ind", "IndicatorAttribute");

		/* error_msg - Error message of uploading. */
		registerAttribute("error_msg", "error_msg");

		/*
		 * validation_seconds - The time it takes the agent to validate the
		 * invoice file.
		 */
		registerAttribute("validation_seconds", "validation_seconds",
				"NumberAttribute");

		/* user_oid - The user who initiates the invoice File Upload. */
		registerAttribute("user_oid", "a_user_oid", "NumberAttribute");

		/* owner_org_oid - The Corporate Customer who uploaded the file. */
		registerAttribute("owner_org_oid", "a_owner_org_oid", "NumberAttribute");

		/* currency used */
		registerAttribute("currency", "currency");

		/* amount of all invoices */
		//registerAttribute("amount", "amount", "NumberAttribute");
		registerAttribute("amount", "amount", "TradePortalDecimalAttribute");
		/* invoice definition name */
		registerAttribute("invoice_file_definition_name",
				"invoice_file_definition_name");
		registerAttribute("trading_partner_name",
		"trading_partner_name");
		//SHR CR708 Rel8.1.1 Start
		  registerAttribute("invoice_upload_parameters","invoice_upload_parameters");

		//SHR CR708 Rel8.1.1 end
	    /* inv_result_parameters -  */
	    registerAttribute("inv_result_parameters", "inv_result_parameters");
	    
	    /* datetime_process_start - Date/Time when the file processing started. */
	      registerAttribute("datetime_process_start", "datetime_process_start", "DateTimeAttribute");
	      /* total_process_seconds - The total time it takes the agent to process the invoice file. */
	      registerAttribute("total_process_seconds", "total_process_seconds", "NumberAttribute");
	      /* process_seconds - The time it takes the agent to validate the invoice file. */
	      registerAttribute("process_seconds", "process_seconds", "NumberAttribute");
	      /* system_name -  */
	      registerAttribute("system_name", "system_name");
	      /* server_name - */
	      registerAttribute("server_name", "server_name");	    

	}

}
