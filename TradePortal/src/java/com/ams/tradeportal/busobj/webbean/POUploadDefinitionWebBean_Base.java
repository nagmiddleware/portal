

  

package com.ams.tradeportal.busobj.webbean;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import javax.servlet.http.*;

/**
 * Describes the mapping of an uploaded file to purchase order line items stored
 * in the database.   The fields contained in the file are described, the file
 * format is specified, and the goods description format is provided.
 * 
 * Can also be used to describe the process of manually entering purchase order
 * data.  The fields are used to define the fields that the user will enter,
 * the order in which they will appear and their format in goods description.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class POUploadDefinitionWebBean_Base extends TradePortalBusinessObjectWebBean
{

  public void registerAttributes()
   {  
      super.registerAttributes();


       /* po_upload_definition_oid - Unique identifier */
       registerAttribute("po_upload_definition_oid", "po_upload_definition_oid", "ObjectIDAttribute");


       /* name - Name of the PO Upload Definition.  This will be used in dropdown lists when
         choosing a definition. */
       registerAttribute("name", "name");


       /* description - Description of the definition for convenience. */
       registerAttribute("description", "description");


       /* file_format - Indicates which file format to use.  Examples of file formats are fixed
         length and delimited.  Not used if this definition is being used for manual
         PO entry */
       registerAttribute("file_format", "file_format");


       /* delimiter_char - Only relevant if using a delimited file format.   This attribute indicates
         what character to use as the delimiter between fields in the uploaded file. */
       registerAttribute("delimiter_char", "delimiter_char");


       /* include_column_header - Indicates whether or not column headers should be included when the goods
         description is formatted. */
       registerAttribute("include_column_header", "include_column_header", "IndicatorAttribute");


       /* include_footer - Indicates whether or not the footer should be included when the goods description
         is formatted. */
       registerAttribute("include_footer", "include_footer", "IndicatorAttribute");


       /* footer_line_1 - Line 1 of the footer */
       registerAttribute("footer_line_1", "footer_line_1");


       /* footer_line_2 - Line 2 of the footer */
       registerAttribute("footer_line_2", "footer_line_2");


       /* footer_line_3 - Line 3 of the footer */
       registerAttribute("footer_line_3", "footer_line_3");


       /* po_num_field_name - The user-defined description of the PO number field.   This description
         will be used whenever this field is refered to by the user. */
       registerAttribute("po_num_field_name", "po_num_field_name");


       /* po_num_size - Size for PO number field.   If the user is uploading a fixed length file,
         this will be the fixed length for the field.  If the user is uploading a
         delimited file, this is the maximum length of the data in the field. */
       registerAttribute("po_num_size", "po_num_size", "NumberAttribute");


       /* po_num_datatype - The datatype of the PO number field */
       registerAttribute("po_num_datatype", "po_num_datatype");


       /* item_num_field_name - The user-defined description of the Item number field.   This description
         will be used whenever this field is refered to by the user. */
       registerAttribute("item_num_field_name", "item_num_field_name");


       /* item_num_size - Size for  Item Number field.   If the user is uploading a fixed length file,
         this will be the fixed length for the field.  If the user is uploading a
         delimited file, this is the maximum length of the data in the field. */
       registerAttribute("item_num_size", "item_num_size", "NumberAttribute");


       /* item_num_datatype - The datatype of the item number field */
       registerAttribute("item_num_datatype", "item_num_datatype");


       /* ben_name_field_name - The user-defined description of the beneficiary name field.   This description
         will be used whenever this field is refered to by the user. */
       registerAttribute("ben_name_field_name", "ben_name_field_name");


       /* ben_name_size - Size for beneficiary name field.   If the user is uploading a fixed length
         file, this will be the fixed length for the field.  If the user is uploading
         a delimited file, this is the maximum length of the data in the field. */
       registerAttribute("ben_name_size", "ben_name_size", "NumberAttribute");


       /* ben_name_datatype - The datatype of the beneficiary name field */
       registerAttribute("ben_name_datatype", "ben_name_datatype");


       /* currency_field_name - The user-defined description of the currency field.   This description will
         be used whenever this field is refered to by the user. */
       registerAttribute("currency_field_name", "currency_field_name");


       /* currency_size - Size for currency field.   If the user is uploading a fixed length file,
         this will be the fixed length for the field.  If the user is uploading a
         delimited file, this is the maximum length of the data in the field. */
       registerAttribute("currency_size", "currency_size", "NumberAttribute");


       /* currency_datatype - The datatype of the currency field */
       registerAttribute("currency_datatype", "currency_datatype");


       /* amount_field_name - The user-defined description of the amount field.   This description will
         be used whenever this field is refered to by the user. */
       registerAttribute("amount_field_name", "amount_field_name");


       /* amount_size - Size for user amount field.   If the user is uploading a fixed length file,
         this will be the fixed length for the field.  If the user is uploading a
         delimited file, this is the maximum length of the data in the field. */
       registerAttribute("amount_size", "amount_size", "NumberAttribute");


       /* amount_datatype - The datatype of the amount field */
       registerAttribute("amount_datatype", "amount_datatype");


       /* last_ship_dt_field_name - The user-defined description of the latest shipment date field.   This description
         will be used whenever this field is refered to by the user. */
       registerAttribute("last_ship_dt_field_name", "last_ship_dt_field_name");


       /* last_ship_dt_size - Size for last shipment date field.   If the user is uploading a fixed length
         file, this will be the fixed length for the field.  If the user is uploading
         a delimited file, this is the maximum length of the data in the field. */
       registerAttribute("last_ship_dt_size", "last_ship_dt_size", "NumberAttribute");


       /* last_ship_dt_datatype - The datatype of the latest shipment date field */
       registerAttribute("last_ship_dt_datatype", "last_ship_dt_datatype");


       /* other1_field_name - The user-defined description of the user defined field.   This description
         will be used whenever this field is refered to by the user. */
       registerAttribute("other1_field_name", "other1_field_name");


       /* other1_size - Size for user defined field.   If the user is uploading a fixed length file,
         this will be the fixed length for the field.  If the user is uploading a
         delimited file, this is the maximum length of the data in the field. */
       registerAttribute("other1_size", "other1_size", "NumberAttribute");


       /* other1_datatype - The datatype of the user defined field */
       registerAttribute("other1_datatype", "other1_datatype");


       /* other2_field_name - The user-defined description of the user defined field.   This description
         will be used whenever this field is refered to by the user. */
       registerAttribute("other2_field_name", "other2_field_name");


       /* other2_size - Size for user defined field.   If the user is uploading a fixed length file,
         this will be the fixed length for the field.  If the user is uploading a
         delimited file, this is the maximum length of the data in the field. */
       registerAttribute("other2_size", "other2_size", "NumberAttribute");


       /* other2_datatype - The datatype of the user defined field */
       registerAttribute("other2_datatype", "other2_datatype");


       /* other3_field_name - The user-defined description of the user defined field.   This description
         will be used whenever this field is refered to by the user. */
       registerAttribute("other3_field_name", "other3_field_name");


       /* other3_size - Size for user defined field.   If the user is uploading a fixed length file,
         this will be the fixed length for the field.  If the user is uploading a
         delimited file, this is the maximum length of the data in the field. */
       registerAttribute("other3_size", "other3_size", "NumberAttribute");


       /* other3_datatype - The datatype of the user defined field */
       registerAttribute("other3_datatype", "other3_datatype");


       /* other4_field_name - The user-defined description of the user defined field.   This description
         will be used whenever this field is refered to by the user. */
       registerAttribute("other4_field_name", "other4_field_name");


       /* other4_size - Size for user defined field.   If the user is uploading a fixed length file,
         this will be the fixed length for the field.  If the user is uploading a
         delimited file, this is the maximum length of the data in the field. */
       registerAttribute("other4_size", "other4_size", "NumberAttribute");


       /* other4_datatype - The datatype of the user defined field */
       registerAttribute("other4_datatype", "other4_datatype");


       /* other5_field_name - The user-defined description of the user defined field.   This description
         will be used whenever this field is refered to by the user. */
       registerAttribute("other5_field_name", "other5_field_name");


       /* other5_size - Size for user defined field.   If the user is uploading a fixed length file,
         this will be the fixed length for the field.  If the user is uploading a
         delimited file, this is the maximum length of the data in the field. */
       registerAttribute("other5_size", "other5_size", "NumberAttribute");


       /* other5_datatype - The datatype of the user defined field */
       registerAttribute("other5_datatype", "other5_datatype");


       /* other6_field_name - The user-defined description of the user defined field.   This description
         will be used whenever this field is refered to by the user. */
       registerAttribute("other6_field_name", "other6_field_name");


       /* other6_size - Size for user defined field.   If the user is uploading a fixed length file,
         this will be the fixed length for the field.  If the user is uploading a
         delimited file, this is the maximum length of the data in the field. */
       registerAttribute("other6_size", "other6_size", "NumberAttribute");


       /* other6_datatype - The datatype of the user defined field */
       registerAttribute("other6_datatype", "other6_datatype");


       /* other7_field_name - The user-defined description of the user defined field.   This description
         will be used whenever this field is refered to by the user. */
       registerAttribute("other7_field_name", "other7_field_name");


       /* other7_size - Size for user defined field.   If the user is uploading a fixed length file,
         this will be the fixed length for the field.  If the user is uploading a
         delimited file, this is the maximum length of the data in the field. */
       registerAttribute("other7_size", "other7_size", "NumberAttribute");


       /* other7_datatype - The datatype of the user defined field */
       registerAttribute("other7_datatype", "other7_datatype");


       /* other8_field_name - The user-defined description of the user defined field.   This description
         will be used whenever this field is refered to by the user. */
       registerAttribute("other8_field_name", "other8_field_name");


       /* other8_size - Size for user defined field.   If the user is uploading a fixed length file,
         this will be the fixed length for the field.  If the user is uploading a
         delimited file, this is the maximum length of the data in the field. */
       registerAttribute("other8_size", "other8_size", "NumberAttribute");


       /* other8_datatype - The datatype of the user defined field */
       registerAttribute("other8_datatype", "other8_datatype");


       /* other9_field_name - The user-defined description of the user defined field.   This description
         will be used whenever this field is refered to by the user. */
       registerAttribute("other9_field_name", "other9_field_name");


       /* other9_size - Size for user defined field.   If the user is uploading a fixed length file,
         this will be the fixed length for the field.  If the user is uploading a
         delimited file, this is the maximum length of the data in the field. */
       registerAttribute("other9_size", "other9_size", "NumberAttribute");


       /* other9_datatype - The datatype of the user defined field */
       registerAttribute("other9_datatype", "other9_datatype");


       /* other10_field_name - The user-defined description of the user defined field.   This description
         will be used whenever this field is refered to by the user. */
       registerAttribute("other10_field_name", "other10_field_name");


       /* other10_size - Size for user defined field.   If the user is uploading a fixed length file,
         this will be the fixed length for the field.  If the user is uploading a
         delimited file, this is the maximum length of the data in the field. */
       registerAttribute("other10_size", "other10_size", "NumberAttribute");


       /* other10_datatype - The datatype of the user defined field */
       registerAttribute("other10_datatype", "other10_datatype");


       /* other11_field_name - The user-defined description of the user defined field.   This description
         will be used whenever this field is refered to by the user. */
       registerAttribute("other11_field_name", "other11_field_name");


       /* other11_size - Size for user defined field.   If the user is uploading a fixed length file,
         this will be the fixed length for the field.  If the user is uploading a
         delimited file, this is the maximum length of the data in the field. */
       registerAttribute("other11_size", "other11_size", "NumberAttribute");


       /* other11_datatype - The datatype of the user defined field */
       registerAttribute("other11_datatype", "other11_datatype");


       /* other12_field_name - The user-defined description of the user defined field.   This description
         will be used whenever this field is refered to by the user. */
       registerAttribute("other12_field_name", "other12_field_name");


       /* other12_size - Size for user defined field.   If the user is uploading a fixed length file,
         this will be the fixed length for the field.  If the user is uploading a
         delimited file, this is the maximum length of the data in the field. */
       registerAttribute("other12_size", "other12_size", "NumberAttribute");


       /* other12_datatype - The datatype of the user defined field */
       registerAttribute("other12_datatype", "other12_datatype");


       /* other13_field_name - The user-defined description of the user defined field.   This description
         will be used whenever this field is refered to by the user. */
       registerAttribute("other13_field_name", "other13_field_name");


       /* other13_size - Size for user defined field.   If the user is uploading a fixed length file,
         this will be the fixed length for the field.  If the user is uploading a
         delimited file, this is the maximum length of the data in the field. */
       registerAttribute("other13_size", "other13_size", "NumberAttribute");


       /* other13_datatype - The datatype of the user defined field */
       registerAttribute("other13_datatype", "other13_datatype");


       /* other14_field_name - The user-defined description of the user defined field.   This description
         will be used whenever this field is refered to by the user. */
       registerAttribute("other14_field_name", "other14_field_name");


       /* other14_size - Size for user defined field.   If the user is uploading a fixed length file,
         this will be the fixed length for the field.  If the user is uploading a
         delimited file, this is the maximum length of the data in the field. */
       registerAttribute("other14_size", "other14_size", "NumberAttribute");


       /* other14_datatype - The datatype of the user defined field */
       registerAttribute("other14_datatype", "other14_datatype");


       /* other15_field_name - The user-defined description of the user defined field.   This description
         will be used whenever this field is refered to by the user. */
       registerAttribute("other15_field_name", "other15_field_name");


       /* other15_size - Size for user defined field.   If the user is uploading a fixed length file,
         this will be the fixed length for the field.  If the user is uploading a
         delimited file, this is the maximum length of the data in the field. */
       registerAttribute("other15_size", "other15_size", "NumberAttribute");


       /* other15_datatype - The datatype of the user defined field */
       registerAttribute("other15_datatype", "other15_datatype");


       /* other16_field_name - The user-defined description of the user defined field.   This description
         will be used whenever this field is refered to by the user. */
       registerAttribute("other16_field_name", "other16_field_name");


       /* other16_size - Size for user defined field.   If the user is uploading a fixed length file,
         this will be the fixed length for the field.  If the user is uploading a
         delimited file, this is the maximum length of the data in the field. */
       registerAttribute("other16_size", "other16_size", "NumberAttribute");


       /* other16_datatype - The datatype of the user defined field */
       registerAttribute("other16_datatype", "other16_datatype");


       /* other17_field_name - The user-defined description of the user defined field.   This description
         will be used whenever this field is refered to by the user. */
       registerAttribute("other17_field_name", "other17_field_name");


       /* other17_size - Size for user defined field.   If the user is uploading a fixed length file,
         this will be the fixed length for the field.  If the user is uploading a
         delimited file, this is the maximum length of the data in the field. */
       registerAttribute("other17_size", "other17_size", "NumberAttribute");


       /* other17_datatype - The datatype of the user defined field */
       registerAttribute("other17_datatype", "other17_datatype");


       /* other18_field_name - The user-defined description of the user defined field.   This description
         will be used whenever this field is refered to by the user. */
       registerAttribute("other18_field_name", "other18_field_name");


       /* other18_size - Size for user defined field.   If the user is uploading a fixed length file,
         this will be the fixed length for the field.  If the user is uploading a
         delimited file, this is the maximum length of the data in the field. */
       registerAttribute("other18_size", "other18_size", "NumberAttribute");


       /* other18_datatype - The datatype of the user defined field */
       registerAttribute("other18_datatype", "other18_datatype");


       /* other19_field_name - The user-defined description of the user defined field.   This description
         will be used whenever this field is refered to by the user. */
       registerAttribute("other19_field_name", "other19_field_name");


       /* other19_size - Size for user defined field.   If the user is uploading a fixed length file,
         this will be the fixed length for the field.  If the user is uploading a
         delimited file, this is the maximum length of the data in the field. */
       registerAttribute("other19_size", "other19_size", "NumberAttribute");


       /* other19_datatype - The datatype of the user defined field */
       registerAttribute("other19_datatype", "other19_datatype");


       /* other20_field_name - The user-defined description of the user defined field.   This description
         will be used whenever this field is refered to by the user. */
       registerAttribute("other20_field_name", "other20_field_name");


       /* other20_size - Size for user defined field.   If the user is uploading a fixed length file,
         this will be the fixed length for the field.  If the user is uploading a
         delimited file, this is the maximum length of the data in the field. */
       registerAttribute("other20_size", "other20_size", "NumberAttribute");


       /* other20_datatype - The datatype of the user defined field */
       registerAttribute("other20_datatype", "other20_datatype");


       /* other21_field_name - The user-defined description of the user defined field.   This description
         will be used whenever this field is refered to by the user. */
       registerAttribute("other21_field_name", "other21_field_name");


       /* other21_size - Size for user defined field.   If the user is uploading a fixed length file,
         this will be the fixed length for the field.  If the user is uploading a
         delimited file, this is the maximum length of the data in the field. */
       registerAttribute("other21_size", "other21_size", "NumberAttribute");


       /* other21_datatype - The datatype of the user defined field */
       registerAttribute("other21_datatype", "other21_datatype");


       /* other22_field_name - The user-defined description of the user defined field.   This description
         will be used whenever this field is refered to by the user. */
       registerAttribute("other22_field_name", "other22_field_name");


       /* other22_size - Size for user defined field.   If the user is uploading a fixed length file,
         this will be the fixed length for the field.  If the user is uploading a
         delimited file, this is the maximum length of the data in the field. */
       registerAttribute("other22_size", "other22_size", "NumberAttribute");


       /* other22_datatype - The datatype of the user defined field */
       registerAttribute("other22_datatype", "other22_datatype");


       /* other23_field_name - The user-defined description of the user defined field.   This description
         will be used whenever this field is refered to by the user. */
       registerAttribute("other23_field_name", "other23_field_name");


       /* other23_size - Size for user defined field.   If the user is uploading a fixed length file,
         this will be the fixed length for the field.  If the user is uploading a
         delimited file, this is the maximum length of the data in the field. */
       registerAttribute("other23_size", "other23_size", "NumberAttribute");


       /* other23_datatype - The datatype of the user defined field */
       registerAttribute("other23_datatype", "other23_datatype");


       /* other24_field_name - The user-defined description of the user defined field.   This description
         will be used whenever this field is refered to by the user. */
       registerAttribute("other24_field_name", "other24_field_name");


       /* other24_size - Size for user defined field.   If the user is uploading a fixed length file,
         this will be the fixed length for the field.  If the user is uploading a
         delimited file, this is the maximum length of the data in the field. */
       registerAttribute("other24_size", "other24_size", "NumberAttribute");


       /* other24_datatype - The datatype of the user defined field */
       registerAttribute("other24_datatype", "other24_datatype");


       /* upload_order_1 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will be retrieved from the uploaded file at this position in the uploaded
         file. */
       registerAttribute("upload_order_1", "upload_order_1");


       /* upload_order_2 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will be retrieved from the uploaded file at this position in the uploaded
         file. */
       registerAttribute("upload_order_2", "upload_order_2");


       /* upload_order_3 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will be retrieved from the uploaded file at this position in the uploaded
         file. */
       registerAttribute("upload_order_3", "upload_order_3");


       /* upload_order_4 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will be retrieved from the uploaded file at this position in the uploaded
         file. */
       registerAttribute("upload_order_4", "upload_order_4");


       /* upload_order_5 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will be retrieved from the uploaded file at this position in the uploaded
         file. */
       registerAttribute("upload_order_5", "upload_order_5");


       /* upload_order_6 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will be retrieved from the uploaded file at this position in the uploaded
         file. */
       registerAttribute("upload_order_6", "upload_order_6");


       /* upload_order_8 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will be retrieved from the uploaded file at this position in the uploaded
         file. */
       registerAttribute("upload_order_8", "upload_order_8");


       /* upload_order_7 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will be retrieved from the uploaded file at this position in the uploaded
         file. */
       registerAttribute("upload_order_7", "upload_order_7");


       /* upload_order_9 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will be retrieved from the uploaded file at this position in the uploaded
         file. */
       registerAttribute("upload_order_9", "upload_order_9");


       /* upload_order_10 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will be retrieved from the uploaded file at this position in the uploaded
         file. */
       registerAttribute("upload_order_10", "upload_order_10");


       /* upload_order_11 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will be retrieved from the uploaded file at this position in the uploaded
         file. */
       registerAttribute("upload_order_11", "upload_order_11");


       /* upload_order_12 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will be retrieved from the uploaded file at this position in the uploaded
         file. */
       registerAttribute("upload_order_12", "upload_order_12");


       /* upload_order_13 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will be retrieved from the uploaded file at this position in the uploaded
         file. */
       registerAttribute("upload_order_13", "upload_order_13");


       /* upload_order_14 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will be retrieved from the uploaded file at this position in the uploaded
         file. */
       registerAttribute("upload_order_14", "upload_order_14");


       /* upload_order_15 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will be retrieved from the uploaded file at this position in the uploaded
         file. */
       registerAttribute("upload_order_15", "upload_order_15");


       /* upload_order_16 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will be retrieved from the uploaded file at this position in the uploaded
         file. */
       registerAttribute("upload_order_16", "upload_order_16");


       /* upload_order_17 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will be retrieved from the uploaded file at this position in the uploaded
         file. */
       registerAttribute("upload_order_17", "upload_order_17");


       /* upload_order_18 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will be retrieved from the uploaded file at this position in the uploaded
         file. */
       registerAttribute("upload_order_18", "upload_order_18");


       /* upload_order_19 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will be retrieved from the uploaded file at this position in the uploaded
         file. */
       registerAttribute("upload_order_19", "upload_order_19");


       /* upload_order_20 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will be retrieved from the uploaded file at this position in the uploaded
         file. */
       registerAttribute("upload_order_20", "upload_order_20");


       /* upload_order_21 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will be retrieved from the uploaded file at this position in the uploaded
         file. */
       registerAttribute("upload_order_21", "upload_order_21");


       /* upload_order_22 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will be retrieved from the uploaded file at this position in the uploaded
         file. */
       registerAttribute("upload_order_22", "upload_order_22");


       /* upload_order_24 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will be retrieved from the uploaded file at this position in the uploaded
         file. */
       registerAttribute("upload_order_24", "upload_order_24");


       /* upload_order_25 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will be retrieved from the uploaded file at this position in the uploaded
         file. */
       registerAttribute("upload_order_25", "upload_order_25");


       /* upload_order_26 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will be retrieved from the uploaded file at this position in the uploaded
         file. */
       registerAttribute("upload_order_26", "upload_order_26");


       /* upload_order_27 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will be retrieved from the uploaded file at this position in the uploaded
         file. */
       registerAttribute("upload_order_27", "upload_order_27");


       /* upload_order_28 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will be retrieved from the uploaded file at this position in the uploaded
         file. */
       registerAttribute("upload_order_28", "upload_order_28");


       /* upload_order_29 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will be retrieved from the uploaded file at this position in the uploaded
         file. */
       registerAttribute("upload_order_29", "upload_order_29");


       /* upload_order_30 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will be retrieved from the uploaded file at this position in the uploaded
         file. */
       registerAttribute("upload_order_30", "upload_order_30");


       /* goods_descr_order_1 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will appear in this position in the goods description text when it is formatted. */
       registerAttribute("goods_descr_order_1", "goods_descr_order_1");


       /* goods_descr_order_2 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will appear in this position in the goods description text when it is formatted. */
       registerAttribute("goods_descr_order_2", "goods_descr_order_2");


       /* goods_descr_order_3 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will appear in this position in the goods description text when it is formatted. */
       registerAttribute("goods_descr_order_3", "goods_descr_order_3");


       /* goods_descr_order_4 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will appear in this position in the goods description text when it is formatted. */
       registerAttribute("goods_descr_order_4", "goods_descr_order_4");


       /* goods_descr_order_5 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will appear in this position in the goods description text when it is formatted. */
       registerAttribute("goods_descr_order_5", "goods_descr_order_5");


       /* goods_descr_order_6 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will appear in this position in the goods description text when it is formatted. */
       registerAttribute("goods_descr_order_6", "goods_descr_order_6");


       /* goods_descr_order_7 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will appear in this position in the goods description text when it is formatted. */
       registerAttribute("goods_descr_order_7", "goods_descr_order_7");


       /* goods_descr_order_8 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will appear in this position in the goods description text when it is formatted. */
       registerAttribute("goods_descr_order_8", "goods_descr_order_8");


       /* goods_descr_order_9 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will appear in this position in the goods description text when it is formatted. */
       registerAttribute("goods_descr_order_9", "goods_descr_order_9");


       /* goods_descr_order_10 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will appear in this position in the goods description text when it is formatted. */
       registerAttribute("goods_descr_order_10", "goods_descr_order_10");


       /* default_flag - Set to Yes if this is the default PO upload definition for the corporate
         organization. */
       registerAttribute("default_flag", "default_flag", "IndicatorAttribute");


       /* goods_descr_order_11 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will appear in this position in the goods description text when it is formatted. */
       registerAttribute("goods_descr_order_11", "goods_descr_order_11");


       /* goods_descr_order_12 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will appear in this position in the goods description text when it is formatted. */
       registerAttribute("goods_descr_order_12", "goods_descr_order_12");


       /* goods_descr_order_13 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will appear in this position in the goods description text when it is formatted. */
       registerAttribute("goods_descr_order_13", "goods_descr_order_13");


       /* goods_descr_order_14 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will appear in this position in the goods description text when it is formatted. */
       registerAttribute("goods_descr_order_14", "goods_descr_order_14");


       /* goods_descr_order_15 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will appear in this position in the goods description text when it is formatted. */
       registerAttribute("goods_descr_order_15", "goods_descr_order_15");


       /* goods_descr_order_16 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will appear in this position in the goods description text when it is formatted. */
       registerAttribute("goods_descr_order_16", "goods_descr_order_16");


       /* goods_descr_order_17 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will appear in this position in the goods description text when it is formatted. */
       registerAttribute("goods_descr_order_17", "goods_descr_order_17");


       /* goods_descr_order_18 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will appear in this position in the goods description text when it is formatted. */
       registerAttribute("goods_descr_order_18", "goods_descr_order_18");


       /* goods_descr_order_19 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will appear in this position in the goods description text when it is formatted. */
       registerAttribute("goods_descr_order_19", "goods_descr_order_19");


       /* goods_descr_order_20 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will appear in this position in the goods description text when it is formatted. */
       registerAttribute("goods_descr_order_20", "goods_descr_order_20");


       /* goods_descr_order_21 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will appear in this position in the goods description text when it is formatted. */
       registerAttribute("goods_descr_order_21", "goods_descr_order_21");


       /* goods_descr_order_22 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will appear in this position in the goods description text when it is formatted. */
       registerAttribute("goods_descr_order_22", "goods_descr_order_22");


       /* goods_descr_order_23 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will appear in this position in the goods description text when it is formatted. */
       registerAttribute("goods_descr_order_23", "goods_descr_order_23");


       /* goods_descr_order_24 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will appear in this position in the goods description text when it is formatted. */
       registerAttribute("goods_descr_order_24", "goods_descr_order_24");


       /* goods_descr_order_25 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will appear in this position in the goods description text when it is formatted. */
       registerAttribute("goods_descr_order_25", "goods_descr_order_25");


       /* goods_descr_order_26 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will appear in this position in the goods description text when it is formatted. */
       registerAttribute("goods_descr_order_26", "goods_descr_order_26");


       /* goods_descr_order_27 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will appear in this position in the goods description text when it is formatted. */
       registerAttribute("goods_descr_order_27", "goods_descr_order_27");


       /* goods_descr_order_28 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will appear in this position in the goods description text when it is formatted. */
       registerAttribute("goods_descr_order_28", "goods_descr_order_28");


       /* goods_descr_order_29 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will appear in this position in the goods description text when it is formatted. */
       registerAttribute("goods_descr_order_29", "goods_descr_order_29");


       /* goods_descr_order_30 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will appear in this position in the goods description text when it is formatted. */
       registerAttribute("goods_descr_order_30", "goods_descr_order_30");


       /* part_to_validate - This is a  local attribute, meaning that it is not stored in the database.
         It stores the tab that the user is editing on the PO Upload Definition detail
         page.   Based on what tab is being edited, different validations occur. */
       registerAttribute("part_to_validate", "part_to_validate", "LocalAttribute");


       /* opt_lock - Optimistic lock attribute
         See jPylon documentation for details on how this works */
       registerAttribute("opt_lock", "opt_lock", "OptimisticLockAttribute");


       /* upload_order_23 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will be retrieved from the uploaded file at this position in the uploaded
         file. */
       registerAttribute("upload_order_23", "upload_order_23");


       /* po_text_datatype - The datatype of the po text field */
       registerAttribute("po_text_datatype", "po_text_datatype");


       /* po_text_field_name - The user-defined description of the PO Text field.   This description will
         be used whenever this field is refered to by the user. */
       registerAttribute("po_text_field_name", "po_text_field_name");


       /* po_text_size - Size for PO Text field.   If the user is uploading a fixed length file,
         this will be the fixed length for the field.  If the user is uploading a
         delimited file, this is the maximum length of the data in the field. */
       registerAttribute("po_text_size", "po_text_size", "NumberAttribute");


       /* date_format - Indicates the format that will be used for uploading and manually entering
         dates. */
       registerAttribute("date_format", "date_format");


       /* include_po_text - Indicates whether or not PO Text should be included in the goods description. */
       registerAttribute("include_po_text", "include_po_text", "IndicatorAttribute");


       /* definition_type - Indicates whether this PO definition is used for manual entry, upload or
         both. */
       registerAttribute("definition_type", "definition_type");


       /* upload_order_31 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will be retrieved from the uploaded file at this position in the uploaded
         file. */
       registerAttribute("upload_order_31", "upload_order_31");

       /* owner_org_oid - A PO upload definition is "owned" by an organization.  This component relationship
         represents the ownership. */
       registerAttribute("owner_org_oid", "a_owner_org_oid", "NumberAttribute");
   }
   
   




}
