

  

package com.ams.tradeportal.busobj.webbean;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import javax.servlet.http.*;
import com.ams.tradeportal.html.*;

/**
 * When the Auto LC Create functionality is used, a log is kept that contains
 * any information, warning, or error messages that are generated.   Since
 * most of the processing for the Auto LC Create functionality happens in the
 * background, the log is the only way that a user can view the results of
 * their operation.
 * 
 * The information that comprises the log is stored in this business object.
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class AutoLCCreateLogWebBean_Base extends com.amsinc.ecsg.web.AmsEntityWebBean
{

  public void registerAttributes()
   {  


       /* auto_lc_create_log_oid - Unique identifier */
       registerAttribute("auto_lc_create_log_oid", "auto_lc_create_log_oid", "ObjectIDAttribute");


       /* sequence_number - A number that is created from the jPylon sequence facility.  This is used
         to uniquely identify this set of log entries.  */
       registerAttribute("sequence_number", "sequence_number", "NumberAttribute");


       /* line_number - The line number of the message.   This ensures that the log text will be
         displayed in the proper order.   As the log is popualted, this  is incremented
         for each item that is placed into the log. */
       registerAttribute("line_number", "line_number", "NumberAttribute");


       /* text - The actual text of the message to be displayed to the user when they view
         the log.  This attribute will be populated with text in the language that
         is appropriate for the user that started the Auto LC Create process.  Any
         other user (regardless of their locale), will see the text in that language. */
       registerAttribute("text", "text");

       /* corp_org_oid - The corporate organization whose Auto LC Create actions are being logged. */
       registerAttribute("corp_org_oid", "a_corp_org_oid", "NumberAttribute");
   }
   
   




}
