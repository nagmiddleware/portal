package com.ams.tradeportal.busobj.webbean;


/**
  Credit Note data information uploaded will be stored here
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class CreditNotesGoodsWebBean_Base extends TradePortalBusinessObjectWebBean {

	/*
	 * Register the attributes and associations of the business object
	 */
	  public void registerAttributes()
	   {  
	           /* Register attributes defined in the Ancestor class */
		      super.registerAttributes();
		      
		      /* upload_credit_goods_oid - Unique identifier */
		      registerAttribute("upload_credit_goods_oid", "upload_credit_goods_oid", "ObjectIDAttribute");
		      
		      /* purchase_order_id - field representing the Purchase Order Id against which the invoice is shipping. */
		      registerAttribute("purchase_order_id", "purchase_order_id");
		      
		      /* description - Description of the definition for convenience. */
		      registerAttribute("goods_description", "goods_description");
		     
		      /* incoterm_req - field representing the Incoterm associated with an underlying shipment */
		      registerAttribute("incoterm", "incoterm");
		      
		      /* country_of_loding - Country of Loading for the goods that the invoice is covering */
		      registerAttribute("country_of_loading", "country_of_loading");
		      
		      /* country_of_discharge - Country of Discharge for the goods that the invoice is covering */
		      registerAttribute("country_of_discharge", "country_of_discharge");
		      
		      /* vessel - vessel shipping the goods that the invoice is covering */
		      registerAttribute("vessel", "vessel");
		      
		      
		      /* carrier - the carrier for the goods that the invoice is covering. */
		      registerAttribute("carrier", "carrier");
		      
		      /* actual_ship_date- The valid date format can be selected from the dropdown list*/
		      registerAttribute("actual_ship_date", "actual_ship_date","DateAttribute");
		      /* Buyer User defined label 1 to 10- Buyer User Defined Fields  */
		      registerAttribute("buyer_users_def1_label", "buyer_users_def1_label");
		      registerAttribute("buyer_users_def2_label", "buyer_users_def2_label");
		      registerAttribute("buyer_users_def3_label", "buyer_users_def3_label");
		      registerAttribute("buyer_users_def4_label", "buyer_users_def4_label");
		      registerAttribute("buyer_users_def5_label", "buyer_users_def5_label");
		      registerAttribute("buyer_users_def6_label", "buyer_users_def6_label");
		      registerAttribute("buyer_users_def7_label", "buyer_users_def7_label");
		      registerAttribute("buyer_users_def8_label", "buyer_users_def8_label");
		      registerAttribute("buyer_users_def9_label", "buyer_users_def9_label");
		      registerAttribute("buyer_users_def10_label", "buyer_users_def10_label");
		      
		      /* Buyer User defined value 1 to 10 */
		      registerAttribute("buyer_users_def1_value", "buyer_users_def1_value");
		      registerAttribute("buyer_users_def2_value", "buyer_users_def2_value");
		      registerAttribute("buyer_users_def3_value", "buyer_users_def3_value");
		      registerAttribute("buyer_users_def4_value", "buyer_users_def4_value");
		      registerAttribute("buyer_users_def5_value", "buyer_users_def5_value");
		      registerAttribute("buyer_users_def6_value", "buyer_users_def6_value");
		      registerAttribute("buyer_users_def7_value", "buyer_users_def7_value");
		      registerAttribute("buyer_users_def8_value", "buyer_users_def8_value");
		      registerAttribute("buyer_users_def9_value", "buyer_users_def9_value");
		      registerAttribute("buyer_users_def10_value", "buyer_users_def10_value");
		      
		      /* Seller User defined label1 to 10 */
		      registerAttribute("seller_users_def1_label", "seller_users_def1_label");
		      registerAttribute("seller_users_def2_label", "seller_users_def2_label");
		      registerAttribute("seller_users_def3_label", "seller_users_def3_label");
		      registerAttribute("seller_users_def4_label", "seller_users_def4_label");
		      registerAttribute("seller_users_def5_label", "seller_users_def5_label");
		      registerAttribute("seller_users_def6_label", "seller_users_def6_label");
		      registerAttribute("seller_users_def7_label", "seller_users_def7_label");
		      registerAttribute("seller_users_def8_label", "seller_users_def8_label");
		      registerAttribute("seller_users_def9_label", "seller_users_def9_label");
		      registerAttribute("seller_users_def10_label", "seller_users_def10_label");
		      
		      /* Seller User defined value 1 to 10 */
		      registerAttribute("seller_users_def1_value", "seller_users_def1_value");
		      registerAttribute("seller_users_def2_value", "seller_users_def2_value");
		      registerAttribute("seller_users_def3_value", "seller_users_def3_value");
		      registerAttribute("seller_users_def4_value", "seller_users_def4_value");
		      registerAttribute("seller_users_def5_value", "seller_users_def5_value");
		      registerAttribute("seller_users_def6_value", "seller_users_def6_value");
		      registerAttribute("seller_users_def7_value", "seller_users_def7_value");
		      registerAttribute("seller_users_def8_value", "seller_users_def8_value");
		      registerAttribute("seller_users_def9_value", "seller_users_def9_value");
		      registerAttribute("seller_users_def10_value", "seller_users_def10_value");
		      registerAttribute("credit_note_oid", "p_credit_note_oid","ParentIDAttribute");
		     
		   }
		   		   
	}

