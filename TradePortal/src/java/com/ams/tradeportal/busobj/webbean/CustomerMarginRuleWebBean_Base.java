



package com.ams.tradeportal.busobj.webbean;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import javax.servlet.http.*;

/**
 *
 *
 *     Copyright  � 2003
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class CustomerMarginRuleWebBean_Base extends TradePortalBusinessObjectWebBean
{

  public void registerAttributes()
   {


       /* customer_margin_rule_oid - Unique Identifier */
       registerAttribute("customer_margin_rule_oid", "customer_margin_rule_oid", "ObjectIDAttribute");


       /* currency - The currency of customer margin rule */
       registerAttribute("currency", "currency");


       /* instrument_type - Code for the instrument type */
       registerAttribute("instrument_type", "instrument_type");


       /* threshold_amt - Threshould Amount for margin rule */
       registerAttribute("threshold_amt", "threshold_amt", "TradePortalDecimalAttribute");


       /* rate_type - The rate type for customer margin rule. */
       registerAttribute("rate_type", "rate_type");


       /* margin - Margin for margin rule */
       registerAttribute("margin", "margin", "TradePortalDecimalAttribute");

      /* Pointer to the parent CorporateOrganization */
       registerAttribute("owner_oid", "p_owner_oid", "ParentIDAttribute");
   }






}
