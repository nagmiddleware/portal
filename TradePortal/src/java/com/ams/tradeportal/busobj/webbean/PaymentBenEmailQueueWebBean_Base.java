

  

package com.ams.tradeportal.busobj.webbean;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import javax.servlet.http.*;

/**
 * Placing a row into this table will cause an e-mail to be sent for each beneficiaries
 * of the associated FTDP transaction. 
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class PaymentBenEmailQueueWebBean_Base extends TradePortalBusinessObjectWebBean
{

  public void registerAttributes()
   {  
      super.registerAttributes();


       /* payment_ben_email_queue_oid - Unique Identifier. */
       registerAttribute("payment_ben_email_queue_oid", "payment_ben_email_queue_oid", "ObjectIDAttribute");


       /* creation_timestamp - The time the item was created. */
       registerAttribute("creation_timestamp", "creation_timestamp", "DateTimeAttribute");


       /* status - The status of the item. */
       registerAttribute("status", "status");


       /* agent_id - The agent who is processing this item. */
       registerAttribute("agent_id", "agent_id");


       /* required_status - The required payment_status of the domestic payment needed to send the email. */
       registerAttribute("required_status", "required_status");

       /* transaction_oid - The id of the transaction to process with this request. */
       registerAttribute("transaction_oid", "a_transaction_oid", "NumberAttribute");
   }
   
   




}
