

  

package com.ams.tradeportal.busobj.webbean;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import javax.servlet.http.*;

/**
 * Invoice Goods.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class InvoiceGoodsWebBean_Base extends com.amsinc.ecsg.web.AmsEntityWebBean
{

  public void registerAttributes()
   {  


       /* invoice_goods_oid - Unique Identifier. */
       registerAttribute("invoice_goods_oid", "invoice_goods_oid", "ObjectIDAttribute");


       /* po_reference_id -  */
       registerAttribute("po_reference_id", "po_reference_id");


       /* po_issue_datetime -  */
       registerAttribute("po_issue_datetime", "po_issue_datetime", "DateTimeAttribute");


       /* line_items_total_amount -  */
       registerAttribute("line_items_total_amount", "line_items_total_amount", "TradePortalDecimalAttribute");


       /* total_net_amount -  */
       registerAttribute("total_net_amount", "total_net_amount", "TradePortalDecimalAttribute");


       /* seller_information_label1 -  */
       registerAttribute("seller_information_label1", "seller_information_label1");


       /* seller_information_label2 -  */
       registerAttribute("seller_information_label2", "seller_information_label2");


       /* seller_information_label3 -  */
       registerAttribute("seller_information_label3", "seller_information_label3");


       /* seller_information_label4 -  */
       registerAttribute("seller_information_label4", "seller_information_label4");


       /* seller_information_label5 -  */
       registerAttribute("seller_information_label5", "seller_information_label5");


       /* seller_information_label6 -  */
       registerAttribute("seller_information_label6", "seller_information_label6");


       /* seller_information_label7 -  */
       registerAttribute("seller_information_label7", "seller_information_label7");


       /* seller_information_label8 -  */
       registerAttribute("seller_information_label8", "seller_information_label8");


       /* seller_information_label9 -  */
       registerAttribute("seller_information_label9", "seller_information_label9");


       /* seller_information_label10 -  */
       registerAttribute("seller_information_label10", "seller_information_label10");


       /* seller_information1 -  */
       registerAttribute("seller_information1", "seller_information1");


       /* seller_information2 -  */
       registerAttribute("seller_information2", "seller_information2");


       /* seller_information3 -  */
       registerAttribute("seller_information3", "seller_information3");


       /* seller_information4 -  */
       registerAttribute("seller_information4", "seller_information4");


       /* seller_information5 -  */
       registerAttribute("seller_information5", "seller_information5");


       /* seller_information6 -  */
       registerAttribute("seller_information6", "seller_information6");


       /* seller_information7 -  */
       registerAttribute("seller_information7", "seller_information7");


       /* seller_information8 -  */
       registerAttribute("seller_information8", "seller_information8");


       /* seller_information9 -  */
       registerAttribute("seller_information9", "seller_information9");


       /* seller_information10 -  */
       registerAttribute("seller_information10", "seller_information10");

      /* Pointer to the parent Invoice */
       registerAttribute("invoice_oid", "p_invoice_oid", "ParentIDAttribute");
       //CR1006 start
       registerAttribute("incoterm", "incoterm");
       
       /* country_of_loding - Country of Loading for the goods that the invoice is covering */
       registerAttribute("country_of_loading", "country_of_loading");
       
       /* country_of_discharge - Country of Discharge for the goods that the invoice is covering */
       registerAttribute("country_of_discharge", "country_of_discharge");
       
       /* vessel - vessel shipping the goods that the invoice is covering */
       registerAttribute("vessel", "vessel");
       
       
       /* carrier - the carrier for the goods that the invoice is covering. */
       registerAttribute("carrier", "carrier");
       
       /* actual_ship_date- */
       registerAttribute("actual_ship_date", "actual_ship_date");
       //CR1006 end
   }
   
   




}
