package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.amsinc.ecsg.frame.AmsException;


public class InvoicesCreditNotesRelationBean_Base extends TradePortalBusinessObjectBean {
private static final Logger LOG = LoggerFactory.getLogger(InvoicesCreditNotesRelationBean_Base.class);

	 
	/*
	 * This holds cross reference b/w INVOICES_SUMMARY_DATA and CREDIT_NOTES table
	 * 
	 */
	  
	  /* 
	   * Register the attributes and associations of the business object
	   */
	   protected void registerAttributes() throws AmsException
	   {  

	      /* Register attributes defined in the Ancestor class */
	      super.registerAttributes();
	      
	      /* invoice_credit_note_rel_oid - Unique identifier */
	      attributeMgr.registerAttribute("invoice_credit_note_rel_oid", "invoice_credit_note_rel_oid", "ObjectIDAttribute");
	      
	      /* credit_note_applied_amount - Credit Amount applied for an Invoice */
	      attributeMgr.registerAttribute("credit_note_applied_amount", "credit_note_applied_amount","TradePortalDecimalAttribute", "com.ams.tradeportal.common");	
	     
	      /* a_upload_credit_note_oid - Points to Credit_Note */
	      attributeMgr.registerAttribute("a_upload_credit_note_oid", "a_upload_credit_note_oid", "NumberAttribute");
	      
	      /* a_upload_credit_note_oid - Points to Invoices_summary_data */
	      attributeMgr.registerAttribute("a_upload_invoice_oid", "a_upload_invoice_oid", "NumberAttribute");
                 
	   }

	}
