package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;


/**
 * This class is used to represent the fact that a user is currently logged
 * on to the Trade Portal.   LogonMediator places an ActiveUserSession into
 * the database whenever a user logs in.  It is removed by LogoutMediator and
 * by the ServerCleanup process. 
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface ActiveUserSession extends TradePortalBusinessObject
{

}
