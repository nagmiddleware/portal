//Leelavathi - 10thDec2012 - Rel8200 CR-737 - Initialversion

/*
 * This file is generated from the model.  Normally it should not be modified manually.
 * Contact the modeler first.  (In a pinch, it can be modified first and then retrofitted
 * to the model.)
 *
 */
package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.RemoteException;

import com.amsinc.ecsg.frame.AmsException;


/*
 * 
 *     Copyright  � 2003
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class AdditionalReqDocBean_Base extends TradePortalBusinessObjectBean
{
private static final Logger LOG = LoggerFactory.getLogger(AdditionalReqDocBean_Base.class);


  /*
   * Register the attributes and associations of the business object
   */
   protected void registerAttributes() throws AmsException
   {

      /* Register attributes defined in the Ancestor class */
      super.registerAttributes();
      attributeMgr.registerAttribute("addl_req_doc_oid", "addl_req_doc_oid", "ObjectIDAttribute");
      attributeMgr.registerAttribute("addl_req_doc_ind", "addl_req_doc_ind", "IndicatorAttribute");
      attributeMgr.registerAttribute("addl_req_doc_name", "addl_req_doc_name");
      attributeMgr.registerAttribute("addl_req_doc_originals", "addl_req_doc_originals", "NumberAttribute");
      attributeMgr.registerAttribute("addl_req_doc_copies", "addl_req_doc_copies", "NumberAttribute");
      attributeMgr.registerAttribute("addl_req_doc_text", "addl_req_doc_text");
      attributeMgr.registerAttribute("terms_oid", "p_terms_oid", "ParentIDAttribute");
   }

  /*
   * Register the components of the business object
   */
   protected void registerComponents() throws RemoteException, AmsException
   {
      /* Register the components defined in the Ancestor class */
      super.registerComponents();

   }






}
