package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;

import com.ams.tradeportal.busobj.TradePortalBusinessObjectBean;
import com.ams.tradeportal.common.*;
import javax.servlet.http.*;

/**
  Invoice data information uploaded will be stored here
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class CustomerErpGlCodeBean_Base extends TradePortalBusinessObjectBean  {
private static final Logger LOG = LoggerFactory.getLogger(CustomerErpGlCodeBean_Base.class);

	/*
	 * Register the attributes and associations of the business object
	 */
	   protected void registerAttributes() throws AmsException
	   {    

	      /* Register attributes defined in the Ancestor class */
	      super.registerAttributes();
	      /*CUSTOMER_ERP_GL_CODE_OID*/
	      attributeMgr.registerAttribute("customer_erp_gl_code_oid", "customer_erp_gl_code_oid", "ObjectIDAttribute");	      
	      /* erp_gl_code - code */
	      attributeMgr.registerAttribute("erp_gl_code", "erp_gl_code");
	      /* The description */
	      attributeMgr.registerAttribute("erp_gl_description", "erp_gl_description");
	      /* GL Category  */
	      attributeMgr.registerAttribute("erp_gl_category", "erp_gl_category");
	      /*  default gl code  */
	      attributeMgr.registerAttribute("default_gl_code_ind", "default_gl_code_ind");
	      /*  customer owner oid  */
	      attributeMgr.registerAttribute("p_owner_oid", "p_owner_oid");	      
	      /*  deactivate indicator  */
	      attributeMgr.registerAttribute("deactivate_ind", "deactivate_ind", "IndicatorAttribute");	      
	       /* opt_lock - Optimistic lock attribute
	         See jPylon documentation for details on how this works */
	      attributeMgr.registerAttribute("opt_lock", "opt_lock", "OptimisticLockAttribute");	      
		      
	   }
		   		   
	}

