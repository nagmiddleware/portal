
  

package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;




/*
 * Represents the global organization.  In the hierarchy of organizations,
 * this is the highest.   All client banks are owned by this organization.
 * 
 * There will only ever be one of these objects in the database, since there
 * is only one global organization.


 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class GlobalOrganizationBean_Base extends ReferenceDataOwnerBean
{
private static final Logger LOG = LoggerFactory.getLogger(GlobalOrganizationBean_Base.class);

  
  /* 
   * Register the attributes and associations of the business object
   */
   protected void registerAttributes() throws AmsException
   {  

      /* Register attributes defined in the Ancestor class */
      super.registerAttributes();
      
      /* authentication_method - Determines how users of the client bank and its bank groups will authenticate
         themselves. */
      attributeMgr.registerReferenceAttribute("authentication_method", "authentication_method", "AUTHENTICATION_METHOD");
      attributeMgr.requiredAttribute("authentication_method");
      
      /* branding_directory - The branding directory used when users owned by the global organization
         access the Trade Portal. */
      attributeMgr.registerAttribute("branding_directory", "branding_directory");
      attributeMgr.requiredAttribute("branding_directory");
      attributeMgr.registerAlias("branding_directory", getResourceManager().getText("GlobalOrganizationBeanAlias.branding_directory", TradePortalConstants.TEXT_BUNDLE));
      
      /* reporting_user_suffix - Suffix that is placed on all reporting users for all organizations under
         this global org */
      attributeMgr.registerAttribute("reporting_user_suffix", "reporting_user_suffix");
      attributeMgr.requiredAttribute("reporting_user_suffix");
      
      /* verify_logon_digital_sig -  */
      attributeMgr.registerAttribute("verify_logon_digital_sig", "verify_logon_digital_sig", "IndicatorAttribute");
      attributeMgr.requiredAttribute("verify_logon_digital_sig");
      
   }
   
  /* 
   * Register the components of the business object
   */
   protected void registerComponents() throws RemoteException, AmsException
   {  
      /* Register the components defined in the Ancestor class */
      super.registerComponents();
      
      /* ClientBankList - Each client bank belongs to one global organization.l */
      registerOneToManyComponent("ClientBankList","ClientBankList");
   }

 
   
 
 
   
}
