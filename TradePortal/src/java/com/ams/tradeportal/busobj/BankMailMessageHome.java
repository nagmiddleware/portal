package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;
/**
 * Business object to store a message or discrepancy notice.   These messages
 * can be created in the back end system and placed into this table by the
 * middleware or they can be created by portal users and sent to the back end
 * system via the middleware.
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface BankMailMessageHome extends EJBHome
{
   public BankMailMessage create()
      throws RemoteException, CreateException, AmsException;

   public BankMailMessage create(ClientServerDataBridge csdb)
      throws RemoteException, CreateException, AmsException;
}
