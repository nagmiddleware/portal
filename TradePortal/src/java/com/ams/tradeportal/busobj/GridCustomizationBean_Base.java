
  

package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;




/*
 * When a user logs out, a history record is created that represents their
 * session.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class GridCustomizationBean_Base extends TradePortalBusinessObjectBean
{
private static final Logger LOG = LoggerFactory.getLogger(GridCustomizationBean_Base.class);

  
  /* 
   * Register the attributes and associations of the business object
   */
   protected void registerAttributes() throws AmsException
   {  

      /* Register attributes defined in the Ancestor class */
      super.registerAttributes();
      
      /* grid_cust_oid - Object identifier of this record */
      attributeMgr.registerAttribute("grid_cust_oid", "grid_cust_oid", "ObjectIDAttribute");
      
      /* user_oid -  */
      attributeMgr.registerAssociation("user_oid", "a_user_oid", "User");
      attributeMgr.requiredAttribute("user_oid");
      
      /* grid_id - this is the id of the dom node that uniquely identifes the
       * grid in the application. */
      attributeMgr.registerAttribute("grid_id", "grid_id");
      attributeMgr.requiredAttribute("grid_id");
      
      /* grid_name - the name of the data grid xml that stores metadata about the grid. */
      attributeMgr.registerAttribute("grid_name", "grid_name");
      attributeMgr.requiredAttribute("grid_name");
      
      /* opt_lock - Optimistic lock attribute
         See jPylon documentation for details on how this works */
      attributeMgr.registerAttribute("opt_lock", "opt_lock", "OptimisticLockAttribute");
   }
   
  /*
   * Register the components of the business object
   */
   protected void registerComponents() throws RemoteException, AmsException
   {
      /* Register the components defined in the Ancestor class */
      super.registerComponents();

      /* GridCustomizationColumnList - list of columns. */
      registerOneToManyComponent("GridColumnCustomizationList","GridColumnCustomizationList");
   }
   
}
