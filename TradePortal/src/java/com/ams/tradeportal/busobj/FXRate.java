package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;


/**
 * A foreign exchange rate between one currency and the corporate customer's
 * base currency.  This is populated by the corporate customer users.
 * 
 * FX Rates are used for threshold checking and for grouping by amount during
 * Auto LC Creation.

 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface FXRate extends TradePortalBusinessObject
{   
}
