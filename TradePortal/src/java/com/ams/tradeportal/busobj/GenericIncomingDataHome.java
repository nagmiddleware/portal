
  

package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;
/**
 * This Business object stores data from Generic Incoming Data Message sent
 * by bank. Bank uses Generic Incoming Data Message to send over data in no
 * specific format.  
 * 
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface GenericIncomingDataHome extends EJBHome
{
   public GenericIncomingData create()
      throws RemoteException, CreateException, AmsException;

   public GenericIncomingData create(ClientServerDataBridge csdb)
      throws RemoteException, CreateException, AmsException;
}
