
  

package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;
/**
 * A user of the Trade Portal system.   Each active user in this table is able
 * to log into the Trade Portal and perform actions for their organization.
 * Users can exist for corporate organizations, bank organization groups, client
 * banks, and the global organization.
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface UserHome extends EJBHome
{
   public User create()
      throws RemoteException, CreateException, AmsException;

   public User create(ClientServerDataBridge csdb)
      throws RemoteException, CreateException, AmsException;
}
