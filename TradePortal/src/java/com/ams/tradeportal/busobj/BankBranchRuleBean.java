
package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.RemoteException;

import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.ReferenceDataManager;
import com.amsinc.ecsg.util.StringFunction;

/*
 *
 *
 *     Copyright  � 2009
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class BankBranchRuleBean extends BankBranchRuleBean_Base
{
private static final Logger LOG = LoggerFactory.getLogger(BankBranchRuleBean.class);    
    protected void userValidate ()  throws AmsException, RemoteException {
        String checkCountry;
        if ("".equals(getAttribute("address_country"))) {
            checkCountry = " and address_country is null"; // LRUK060947155 check for null address_country correctly.
        }
        else {
            checkCountry = " and address_country = '" + getAttribute("address_country") + "'";
        }
        // CR-486 Add unicode_indicator
        if (!isUnique("payment_method", getAttribute("payment_method"), checkCountry + " and bank_branch_code = '"+getAttribute("bank_branch_code") + "' and unicode_indicator = '" + getAttribute("unicode_indicator") + "'",false)) {
            this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                      TradePortalConstants.BANK_BRANCH_RULE_ALREADY_EXISTS, 
                      ReferenceDataManager.getRefDataMgr().getDescr("COUNTRY", getAttribute("address_country"), csdb.getLocaleName()),
                      ReferenceDataManager.getRefDataMgr().getDescr("PAYMENT_METHOD", getAttribute("payment_method"), csdb.getLocaleName()),
                      getAttribute("bank_branch_code"));

        }
        
//      NSX CR-573 07/08/10  Begin   -  remove validation
//        if (getAttribute("address_city").length() + getAttribute("address_state_province").length() > 31) {
//            this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
//                    TradePortalConstants.CITY_PROVINCE_MORE_THAN_31);            
//        }
//      NSX CR-573 07/08/10  End
        
        // CR-486 LKUK061036185 Check bank_branch_code does not have unicode.
        if (!StringFunction.canEncodeIn_WIN1252(getAttribute("bank_branch_code"))) {
            this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                    TradePortalConstants.UNICODE_NOT_ALLOWED,
                    getResourceManager().getText("BankBranchRuleBeanAlias.bank_branch_code", 
                            TradePortalConstants.TEXT_BUNDLE));      
        }
    }
    
    protected void preSave () throws AmsException
    {
        super.preSave();
        
        // check if name and address fields have unicode character(s) and set the unicode_indicator accordingly 

        try  {
            if (StringFunction.canEncodeIn_WIN1252(getAttribute("bank_name")) && 
                StringFunction.canEncodeIn_WIN1252(getAttribute("branch_name")) && 
                StringFunction.canEncodeIn_WIN1252(getAttribute("address_line_1")) &&
                StringFunction.canEncodeIn_WIN1252(getAttribute("address_line_2")) && 
                StringFunction.canEncodeIn_WIN1252(getAttribute("address_city")) && 
                StringFunction.canEncodeIn_WIN1252(getAttribute("address_state_province"))) {
                
                   setAttribute("unicode_indicator",TradePortalConstants.INDICATOR_NO);
            }
            else {
                   setAttribute("unicode_indicator",TradePortalConstants.INDICATOR_YES);
            }

        }
        catch (RemoteException ex) {
            LOG.error("BankBranchRuleBean: RemoteException on preSave(): ",ex);
            throw new AmsException(ex.getMessage());
        }
    }

}










