
  

package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;



/*
 * An Announcement.
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class RecentInstrumentBean_Base extends TradePortalBusinessObjectBean
{
private static final Logger LOG = LoggerFactory.getLogger(RecentInstrumentBean_Base.class);
  
  /* 
   * Register the attributes and associations of the business object
   */
   protected void registerAttributes() throws AmsException
   {  

      /* Register attributes defined in the Ancestor class */
      super.registerAttributes();

      /* recent_instrument_oid - Unique Identifier. */
      attributeMgr.registerAttribute("recent_instrument_oid", "recent_instrument_oid", "ObjectIDAttribute");
      
      /* user_oid -  */
      attributeMgr.registerAssociation("user_oid", "a_user_oid", "User");
      attributeMgr.requiredAttribute("user_oid");

      /* display order */
      attributeMgr.registerAttribute("display_order", "display_order", "NumberAttribute");
      attributeMgr.requiredAttribute("display_order");

      attributeMgr.registerAssociation("transaction_oid", "transaction_oid", "Transaction");
      attributeMgr.requiredAttribute("display_order");
     
      attributeMgr.registerAttribute("complete_instrument_id", "complete_instrument_id");

      attributeMgr.registerAttribute("transaction_type_code", "transaction_type_code");

      attributeMgr.registerAttribute("transaction_status", "transaction_status");

      /* opt_lock - Optimistic lock attribute
         See jPylon documentation for details on hRow this works */
      attributeMgr.registerAttribute("opt_lock", "opt_lock", "OptimisticLockAttribute");      
   }
   
}
