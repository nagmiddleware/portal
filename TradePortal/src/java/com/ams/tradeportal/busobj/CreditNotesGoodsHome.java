package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;

import java.rmi.*;

import javax.ejb.*;
/**
 * Describes the mapping of an uploaded file to Invoice Definition
 * in the database.   The fields contained in the file are described, the file
 * format is specified, and the Line Item Detail order is provided.
 * 
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */

public interface CreditNotesGoodsHome extends EJBHome {
	
	public CreditNotesGoods create()
    throws RemoteException, CreateException, AmsException;

    public CreditNotesGoods create(ClientServerDataBridge csdb)
    throws RemoteException, CreateException, AmsException;

}
