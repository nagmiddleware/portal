package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;
/**
 * Abstract business object that is the superclass for reference data that
 * can be owned by organizations at multiple levels.
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface ReferenceDataHome extends EJBHome
{
   public ReferenceData create()
      throws RemoteException, CreateException, AmsException;

   public ReferenceData create(ClientServerDataBridge csdb)
      throws RemoteException, CreateException, AmsException;
}
