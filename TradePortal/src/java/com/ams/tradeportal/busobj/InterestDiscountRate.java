



package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;


/**
 * Interest Discount Rate Class, Which will ultimately allow the Corporate
 * Customers to view Discount/Interest rates that will be applied on financed
 * invoices.
 *
 *     Copyright  � 2003
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public interface InterestDiscountRate extends TradePortalBusinessObject
{
}
