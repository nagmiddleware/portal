//Leelavathi - 10thDec2012 - Rel8200 CR-737 - Initialversion

package com.ams.tradeportal.busobj;

import java.rmi.RemoteException;

import javax.ejb.CreateException;
import javax.ejb.EJBHome;

import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.ClientServerDataBridge;
/**
 * Parties and corporate customers can be set up to be related to accounts.
 * This account data is used on the loan request and funds transfer instruments.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface AdditionalReqDocHome extends EJBHome
{
   public AdditionalReqDoc create()
      throws RemoteException, CreateException, AmsException;

   public AdditionalReqDoc create(ClientServerDataBridge csdb)
      throws RemoteException, CreateException, AmsException;
}
