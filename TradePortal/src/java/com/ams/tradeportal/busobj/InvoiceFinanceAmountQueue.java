package com.ams.tradeportal.busobj;

    import com.amsinc.ecsg.frame.*;
    import com.amsinc.ecsg.util.*;
    import java.rmi.*;
    import java.util.*;
    import java.math.*;
    import javax.ejb.*;
import com.ams.tradeportal.common.*;


    /**
    * The Invoice Files that are to be uploaded.
    *
    *     Copyright  � 2003
    *     American Management Systems, Incorporated
    *     All rights reserved
    */
    public interface InvoiceFinanceAmountQueue extends BusinessObject
    {
				
    }
