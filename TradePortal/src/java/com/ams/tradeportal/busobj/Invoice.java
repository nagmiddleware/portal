

  

package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;

import java.rmi.*;
import java.util.*;
import java.math.*;

import javax.ejb.*;

import com.ams.tradeportal.common.*;


/**
 * Invoice.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface Invoice extends BusinessObject
{   
            	/**
	 * Calculates the discount or interest and updates this invoice with the
	 * appropriate values.  If any validations fail, an AmsException is thrown
	 * with the appropriate error code.
	 * 
	 * @param matchingRule
	 * @throws AmsException
	 * @throws RemoteException
	 */
		public void calculateInvoiceOfferNetAmountOffered()
		throws AmsException, RemoteException;
        
        public void updateAssociatedInvoiceOfferGroup() throws AmsException, RemoteException;
        
        public boolean isCreditNoteApplied() throws AmsException, RemoteException;
        
        public void updateCreidtNoteAppliedAmout(DocumentHandler invoiceDoc) throws AmsException, RemoteException;
        
}
