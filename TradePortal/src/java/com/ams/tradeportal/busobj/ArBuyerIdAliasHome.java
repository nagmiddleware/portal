
  

package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;
/**
 * The alias for the AR Buyer ID in an ARMatchingRule
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface ArBuyerIdAliasHome extends EJBHome
{
   public ArBuyerIdAlias create()
      throws RemoteException, CreateException, AmsException;

   public ArBuyerIdAlias create(ClientServerDataBridge csdb)
      throws RemoteException, CreateException, AmsException;
}
