
package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;

import java.rmi.*;
import java.util.*;
import com.ams.tradeportal.common.*;
import com.ams.tradeportal.busobj.util.*;

import java.nio.charset.CharacterCodingException;
import java.nio.charset.Charset;
import java.nio.CharBuffer;
import java.nio.ByteBuffer;
import java.nio.charset.CharsetEncoder;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CodingErrorAction;





/*
 *
 *
 *     Copyright  © 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class TermsPartyBean extends TermsPartyBean_Base
{
private static final Logger LOG = LoggerFactory.getLogger(TermsPartyBean.class);
  /**
   * Returns the alias for an attribute on the terms party object.
   * This allows the TermMgr beans to get the alias for error messages.
   *
   * @return java.lang.String
   * @param attributeName java.lang.String
   */
   public String getAlias(String attributeName) throws RemoteException, AmsException
   {
      String alias = attributeMgr.getAlias(attributeName);

      if (alias == null || alias.equals(""))
      {
         alias = attributeName;
      }

      return alias;
   }



  /**
   * This functions copies data from a corporate org to a terms party and
   * then sets the terms party type to the original terms party type for
   * the instrument type specified by the param instrType.
   *
   * THIS METHOD IS INTENDED TO BE USED IN THE CREATE TRANSACTION PROCESS ONLY
   *
   * Note: This method could be made much more generic by passing in the
   * desired terms party type, but at this point we may not need it to be
   * that generic.
   *
   * @return com.ams.tradeportal.busobj.TermsParty
   * @param corp com.ams.tradeportal.busobj.CorporateOrganization
   * @param instrType java.lang.String
   */
  public void populateFirstTermsParty(CorporateOrganization corp, String instrType) throws RemoteException, AmsException
  {
    LOG.debug("Entering populateFirstTermsParty()");

    this.setAttribute("name", corp.getAttribute("name"));
    this.setAttribute("address_line_1", corp.getAttribute("address_line_1"));
    this.setAttribute("address_line_2", corp.getAttribute("address_line_2"));
    this.setAttribute("address_city", corp.getAttribute("address_city"));
    this.setAttribute("address_state_province", corp.getAttribute("address_state_province"));
    this.setAttribute("address_country", corp.getAttribute("address_country"));
    this.setAttribute("address_postal_code", corp.getAttribute("address_postal_code"));
    this.setAttribute("OTL_customer_id", corp.getAttribute("Proponix_customer_id"));
    //Rel9.5 CR1132 populate userdefinedfields from corporate
    this.setAttribute("user_defined_field_1", corp.getAttribute("user_defined_field_1"));
    this.setAttribute("user_defined_field_2", corp.getAttribute("user_defined_field_2"));
    this.setAttribute("user_defined_field_3", corp.getAttribute("user_defined_field_3"));
    this.setAttribute("user_defined_field_4", corp.getAttribute("user_defined_field_4"));

    if(instrType.equals(InstrumentType.EXPORT_DLC) ||
       instrType.equals(InstrumentType.EXPORT_COL) ||
       instrType.equals(InstrumentType.NEW_EXPORT_COL)) //Vasavi CR 524 03/31/2010 Add
    {
        this.setAttribute("terms_party_type", TermsPartyType.DRAWER_SELLER);
    }
    // rkrishna CR 375-D ATP 08/17/2007 Begin
    else if(instrType.equals(InstrumentType.APPROVAL_TO_PAY))
    {
    	this.setAttribute("terms_party_type", TermsPartyType.ATP_BUYER);
    }
    //  rkrishna CR 375-D ATP 08/17/2007 End
    else
        this.setAttribute("terms_party_type", TermsPartyType.APPLICANT);

    // For FUNDS TRANSFER and LOAN REQUEST instruments, populate the acct_choices
    // field with the set of account for the corp. org.
    //IAZ CM-451 02/22/09 Begin
    //FUNDS XFER (INT PMT) does not req account choices to be stored with TermsPartyBean
    //INT PMT TRAN Page will build account choices on a fly
      if (instrType.equals(InstrumentType.LOAN_RQST))
    //IAZ CM-451 02/22/09 End
    {
        String choices = getAcctChoices(corp.getAttribute("organization_oid"));
        this.setAttribute("acct_choices", choices);
    }

    LOG.debug("returning from populateFirstTermsParty()");

    return;
  }

  public void populateSixthTermsParty(CorporateOrganization corp, String instrType) throws RemoteException, AmsException
  {
    LOG.debug("Entering populateSixthTermsParty()");

    this.setAttribute("name", corp.getAttribute("name"));
    this.setAttribute("address_line_1", corp.getAttribute("address_line_1"));
    this.setAttribute("address_line_2", corp.getAttribute("address_line_2"));
    this.setAttribute("address_city", corp.getAttribute("address_city"));
    this.setAttribute("address_state_province", corp.getAttribute("address_state_province"));
    this.setAttribute("address_country", corp.getAttribute("address_country"));
    this.setAttribute("address_postal_code", corp.getAttribute("address_postal_code"));
    this.setAttribute("OTL_customer_id", corp.getAttribute("Proponix_customer_id"));

    this.setAttribute("terms_party_type", TermsPartyType.ISSUING_BANK);

    LOG.debug("returning from populateSixthTermsParty()");

    return;
  }


  private String getAcctChoices(String orgOid)
  {
	// The method takes an organization oid and queries the database for accounts belonging
	// to that org.  The resulting ResultSetRecord XML is sent back.  The format of this XML
	// is the result set returned from a query.
	//    <ResultSetRecord ID="1">
	//      <ACCOUNT_NUMBER>xxxxx</ACCOUNT_NUMBER>
	//      <CURRENCY>xxx</CURRENCY>
	//    </ResultSetRecord>
	  
	// LSuresh R91 IR T36000026319 - SQLINJECTION FIX
	StringBuffer    sql           = new StringBuffer();
      DocumentHandler acctList      = new DocumentHandler();

      ResourceManager resMgr = this.getResourceManager();
	try {
	      sql.append("select account_oid, account_number, currency ");
      	  sql.append(" from account");
	      sql.append(" where p_owner_oid =  ?");
	      // IR PYUJ022346797 NShrestha 03/12/2009 Begin
	      sql.append(" and available_for_loan_request= ?");
	      // IR PYUJ022346797 NShrestha 03/12/2009 End
      	  sql.append(" order by ");
	      sql.append(resMgr.localizeOrderBy("account_number"));

		acctList = DatabaseQueryBean.getXmlResultSet( sql.toString(),false,orgOid, "Y" );

	} catch (Exception e) {
            LOG.error("Error in TermsPartyBean.getAcctChoices: ",e);
	}  // try/catch/finally block

      if(acctList == null)
         return "";
      else
         return acctList.toString();

  }



  /**
   * copy(...)
   *
   * this method takes a terms party and copies all the information to this terms party
   *
   * @param source TermsParty
   */
  public void copy(TermsParty source) throws RemoteException, AmsException
  {
    LOG.debug("Entering TermsParty.copy");

    //get the attributes from the source terms party and use them to set
    //the attributes on the new terms party
    Vector nameValueArrays = InstrumentServices.attributeHashProcessor(source.getAttributeHash());
    String [] name = (String []) nameValueArrays.elementAt(0);
    String [] value = (String []) nameValueArrays.elementAt(1);

    LOG.debug ("name.length -> {}" , name.length);
    LOG.debug ("value.length -> {}" , value.length);

    for (int i=0; i<name.length; i++)
        LOG.debug ("{} - {}" ,name[i], value[i]);

    this.setAttributes(name, value);
  }

  /**
   * Takes the termsPartyDoc to be populated with the required values of this
   * TermsParty at the location indicated with tpid
   *
   * @param tpid int
   * @param termsPartyDoc com.amsinc.util.DocumentHandler
   */
  public DocumentHandler packageTermsPartyAttributes( int tpid, DocumentHandler termsPartyDoc)
                                                       throws RemoteException, AmsException
  {


 	  	String [] termsPartyAttributes = {"terms_party_type","name","address_line_1", "address_line_2","address_line_3","address_city",
 	   	                                  "address_state_province", "address_country","address_postal_code","phone_number","contact_name",
 	   	                                  "OTL_customer_id","address_seq_num","user_defined_field_1","user_defined_field_2","user_defined_field_3","user_defined_field_4"};
 	   	Hashtable<String,String> termsPartyValues =  this.getAttributes(termsPartyAttributes);

 	   	termsPartyDoc.setAttribute("/TermsParty/FullTermsParty(" + tpid + ")/TermsPartyType",        termsPartyValues.get("terms_party_type"));
 	   	termsPartyDoc.setAttribute("/TermsParty/FullTermsParty(" + tpid + ")/Name",                  termsPartyValues.get("name"));
 	   	termsPartyDoc.setAttribute("/TermsParty/FullTermsParty(" + tpid + ")/AddressLine1",          termsPartyValues.get("address_line_1"));
 	   	termsPartyDoc.setAttribute("/TermsParty/FullTermsParty(" + tpid + ")/AddressLine2",          termsPartyValues.get("address_line_2"));
 	   	termsPartyDoc.setAttribute("/TermsParty/FullTermsParty(" + tpid + ")/AddressLine3",          termsPartyValues.get("address_line_3"));
 	   	termsPartyDoc.setAttribute("/TermsParty/FullTermsParty(" + tpid + ")/AddressCity",           termsPartyValues.get("address_city"));
 	   	termsPartyDoc.setAttribute("/TermsParty/FullTermsParty(" + tpid + ")/AddressStateProvince",  termsPartyValues.get("address_state_province"));
 	   	termsPartyDoc.setAttribute("/TermsParty/FullTermsParty(" + tpid + ")/AddressCountry",        termsPartyValues.get("address_country"));
 	   	termsPartyDoc.setAttribute("/TermsParty/FullTermsParty(" + tpid + ")/AddressPostalCode",     termsPartyValues.get("address_postal_code"));
 	   	termsPartyDoc.setAttribute("/TermsParty/FullTermsParty(" + tpid + ")/PhoneNumber",           termsPartyValues.get("phone_number"));
 	   	termsPartyDoc.setAttribute("/TermsParty/FullTermsParty(" + tpid + ")/FaxNumber1",            "");
 	   	termsPartyDoc.setAttribute("/TermsParty/FullTermsParty(" + tpid + ")/ContactName",           termsPartyValues.get("contact_name"));
 	   	termsPartyDoc.setAttribute("/TermsParty/FullTermsParty(" + tpid + ")/OTLCustomerID",         termsPartyValues.get("OTL_customer_id"));
 	   	termsPartyDoc.setAttribute("/TermsParty/FullTermsParty(" + tpid + ")/OTLAddressSequenceNum", termsPartyValues.get("address_seq_num"));
 	   	if ( StringFunction.isNotBlank(termsPartyValues.get("user_defined_field_1")) ) {
 	   	  termsPartyDoc.setAttribute("/TermsParty/FullTermsParty(" + tpid + ")/UserDefinedField1", termsPartyValues.get("user_defined_field_1"));
 	   	}
 	    if ( StringFunction.isNotBlank(termsPartyValues.get("user_defined_field_2")) ) {
  	   	  termsPartyDoc.setAttribute("/TermsParty/FullTermsParty(" + tpid + ")/UserDefinedField2", termsPartyValues.get("user_defined_field_2"));
  	   	}
 	    if ( StringFunction.isNotBlank(termsPartyValues.get("user_defined_field_3")) ) {
 	   	  termsPartyDoc.setAttribute("/TermsParty/FullTermsParty(" + tpid + ")/UserDefinedField3", termsPartyValues.get("user_defined_field_3"));
 	   	}
 	    if ( StringFunction.isNotBlank(termsPartyValues.get("user_defined_field_4")) ) {
	   	  termsPartyDoc.setAttribute("/TermsParty/FullTermsParty(" + tpid + ")/UserDefinedField4", termsPartyValues.get("user_defined_field_4"));
	   	}

 	   	return termsPartyDoc;



  }

  /**
   * Takes the termsDoc to be populated with the required values of this
   * TermsParty.  This method is not intended for parties which utilize
   * all 4 address lines; will only packages address lines 1,2,3
   *
   * @param shipmentIndex int
   * @param partyType String
   * @param termsPartyDoc com.amsinc.util.DocumentHandler
   */
  public DocumentHandler packageShipmentTermsPartyAttributes(int shipmentIndex, String partyType, DocumentHandler termsDoc)
                                                       throws RemoteException, AmsException
  {
    String [] termsPartyAttributes = {"name","address_line_1", "address_line_2","address_line_3"};
    Hashtable termsPartyValues     =  this.getAttributes(termsPartyAttributes);

    termsDoc.setAttribute("/Terms/ShipmentTerms/FullShipmentTerms(" + shipmentIndex + ")/"+partyType+"BusinessName", (String)termsPartyValues.get("name"));
    termsDoc.setAttribute("/Terms/ShipmentTerms/FullShipmentTerms(" + shipmentIndex + ")/"+partyType+"AddressLine1", (String)termsPartyValues.get("address_line_1"));
    termsDoc.setAttribute("/Terms/ShipmentTerms/FullShipmentTerms(" + shipmentIndex + ")/"+partyType+"AddressLine2", (String)termsPartyValues.get("address_line_2"));
    termsDoc.setAttribute("/Terms/ShipmentTerms/FullShipmentTerms(" + shipmentIndex + ")/"+partyType+"AddressLine3", (String)termsPartyValues.get("address_line_3"));
    return termsDoc;
  }

   public void copyFromParty(Party source) throws RemoteException, AmsException
    {
	    this.setAttribute("name", source.getAttribute("name"));
        this.setAttribute("address_line_1", source.getAttribute("address_line_1"));
        this.setAttribute("address_line_2", source.getAttribute("address_line_2"));
        this.setAttribute("address_city", source.getAttribute("address_city"));
        this.setAttribute("address_state_province", source.getAttribute("address_state_province"));
        this.setAttribute("address_country", source.getAttribute("address_country"));
        this.setAttribute("address_postal_code", source.getAttribute("address_postal_code"));
        this.setAttribute("OTL_customer_id", source.getAttribute("OTL_customer_id"));
    }

   /**
    * When saving a terms party, validate combination of City , State, Postal
    * Code
    */
    public void userValidate () throws AmsException, RemoteException
    {
	int countTotalNoOfChars = 0;
	String city = this.getAttribute("address_city");
	if (city != null)
		countTotalNoOfChars += city.length();
	String state = this.getAttribute("address_state_province");
	if (state != null)
		countTotalNoOfChars += state.length();
	String postalcode = this.getAttribute("address_postal_code");
	if (postalcode != null)
		countTotalNoOfChars += postalcode.length();
	/*KMehta IR-T36000034134 Rel 9300 on 27-Apr-2015 Change - Start */
	//Validate total no. of characters
	if (countTotalNoOfChars > TradePortalConstants.CITY_STATE_ZIP_PARTYBEAN_LENGTH)
	{
		this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
				 TradePortalConstants.TOO_MANY_CHARS_IN_COMB_PARTYBEAN);
	}
	/*KMehta IR-T36000034134 Rel 9300 on 27-Apr-2015 Change - End */
    }

    protected void preSave () throws AmsException
    {
        super.preSave();
        
        // CR-486 TERMS_PARTY table allows unicode in some columns (using NCHAR datatype).   
        // But we should allowed unicode only for (Domestic) Payment. 
        // For the other instruments, we replace non-win1252 characters with 0xbf (upside-down question mark).
        String instrumentType = getClientServerDataBridge().getCSDBValue("instrument_type");
        /*KMehta IR-T36000041329 Rel 9300 on 17-Jul-2015 Change - Start */
        if (!(InstrumentType.DOM_PMT.equals(instrumentType) || InstrumentType.LOAN_RQST.equals(instrumentType))) {
            CharsetEncoder encoder = Charset.forName("windows-1252").newEncoder();
            CharsetDecoder decoder = Charset.forName("windows-1252").newDecoder();
            // Default replacement is regular question mark.  Use upside-down question mark to be consistent 
            // with behavior in other non-unicode fields.
            byte[] replacement = {(byte)0xbf};
            encoder.onUnmappableCharacter(CodingErrorAction.REPLACE);
            encoder.replaceWith(replacement); 
            replaceNonWin1252Chars(encoder, decoder, "name");
            replaceNonWin1252Chars(encoder, decoder, "address_line_1");
            replaceNonWin1252Chars(encoder, decoder, "address_line_2");
            replaceNonWin1252Chars(encoder, decoder, "address_line_3");
            replaceNonWin1252Chars(encoder, decoder, "address_city");
            replaceNonWin1252Chars(encoder, decoder, "address_state_province");
        }
        /*KMehta IR-T36000041329 Rel 9300 on 17-Jul-2015 Change - End */
    }    
    
    private void replaceNonWin1252Chars(CharsetEncoder encoder, CharsetDecoder decoder, String attributeName) throws AmsException {
        try {
            String attributeValue = getAttribute(attributeName);
            if (attributeValue == null) return;
            ByteBuffer win1252Value =  encoder.encode(CharBuffer.wrap(attributeValue.toCharArray()));
            String convertedValue = decoder.decode(win1252Value).toString();
            if (!attributeValue.equals(convertedValue)) {
                setAttribute(attributeName, convertedValue);
            }
        }
        catch (CharacterCodingException e) {
        	LOG.error("TermsPartyBean:: CharacterCodingException at replaceNonWin1252Chars(): ",e);
        }
        catch (RemoteException ex) {
        	LOG.error("TermsPartyBean:: RemoteException at replaceNonWin1252Chars(): ",ex);
            throw new AmsException(ex.getMessage());
        }
        return;
    }


    //cquinton 4/8/2011 Rel 7.0.0 ir#bkul040547648 start
    /**
     * Populate the corporate org components of the TermsParty.
     * Also set the party type.
     */
    public void populateTermsParty(CorporateOrganization corp, String partyType) throws RemoteException, AmsException {
        this.setAttribute("name", corp.getAttribute("name"));
        this.setAttribute("address_line_1", corp.getAttribute("address_line_1"));
        this.setAttribute("address_line_2", corp.getAttribute("address_line_2"));
        this.setAttribute("address_city", corp.getAttribute("address_city"));
        this.setAttribute("address_state_province", corp.getAttribute("address_state_province"));
        this.setAttribute("address_country", corp.getAttribute("address_country"));
        this.setAttribute("address_postal_code", corp.getAttribute("address_postal_code"));
        this.setAttribute("OTL_customer_id", corp.getAttribute("Proponix_customer_id"));

        this.setAttribute("terms_party_type", partyType);

        return;
    }
    //cquinton 4/8/2011 Rel 7.0.0 ir#bkul040547648 end


//End of Class
}
