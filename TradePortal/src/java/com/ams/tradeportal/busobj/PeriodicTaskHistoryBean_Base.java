
  

package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;




/*
 * This object represents the running of a periodic task, such as purge or
 * sending daily email.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class PeriodicTaskHistoryBean_Base extends TradePortalBusinessObjectBean
{
private static final Logger LOG = LoggerFactory.getLogger(PeriodicTaskHistoryBean_Base.class);

  
  /* 
   * Register the attributes and associations of the business object
   */
   protected void registerAttributes() throws AmsException
   {  

      /* Register attributes defined in the Ancestor class */
      super.registerAttributes();
      
      /* periodic_task_history_oid - Unique identifier */
      attributeMgr.registerAttribute("periodic_task_history_oid", "periodic_task_history_oid", "ObjectIDAttribute");
      
      /* run_timestamp - A timestamp (in GMT) of when the task was run */
      attributeMgr.registerAttribute("run_timestamp", "run_timestamp", "DateTimeAttribute");
      
      /* task_type - The type of task that was run */
      attributeMgr.registerReferenceAttribute("task_type", "task_type", "PERIODIC_TASK_TYPE");
      
   }
   
 
   
 
 
   
}
