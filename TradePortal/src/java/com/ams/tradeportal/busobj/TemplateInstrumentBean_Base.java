
  

package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;




/*
 * Subclass of InstrumentData that represents a template's data in the database
 * (in the INSTRUMENT table).  There is another business object, Template,
 * that represents the reference data entry for a template.
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class TemplateInstrumentBean_Base extends InstrumentDataBean
{
private static final Logger LOG = LoggerFactory.getLogger(TemplateInstrumentBean_Base.class);

  
  /* 
   * Register the attributes and associations of the business object
   */
   protected void registerAttributes() throws AmsException
   {  

      /* Register attributes defined in the Ancestor class */
      super.registerAttributes();
      
   }
   
 
   
 
 
   
}
