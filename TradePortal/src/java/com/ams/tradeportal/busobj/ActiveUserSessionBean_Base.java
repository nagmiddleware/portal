
  

package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;




/*
 * This class is used to represent the fact that a user is currently logged
 * on to the Trade Portal.   LogonMediator places an ActiveUserSession into
 * the database whenever a user logs in.  It is removed by LogoutMediator and
 * by the ServerCleanup process. 
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class ActiveUserSessionBean_Base extends TradePortalBusinessObjectBean
{
private static final Logger LOG = LoggerFactory.getLogger(ActiveUserSessionBean_Base.class);

  
  /* 
   * Register the attributes and associations of the business object
   */
   protected void registerAttributes() throws AmsException
   {  

      /* Register attributes defined in the Ancestor class */
      super.registerAttributes();
      
      /* active_user_session_oid - Object identifier of the active user session */
      attributeMgr.registerAttribute("active_user_session_oid", "active_user_session_oid", "ObjectIDAttribute");
      
      /* creation_timestamp - The date and time (stored in the database in the GMT timezone) when the
         ActiveUserSession record was created */
      attributeMgr.registerAttribute("creation_timestamp", "creation_timestamp", "DateTimeAttribute");
      
      /* server_instance_name - The name of the server instance from a jPylon properties file.  This attribute
         is necessary so that when a server starts up after a crash, it will know
         which ActiveUserSession records to delete. */
      attributeMgr.registerAttribute("server_instance_name", "server_instance_name");
      
      /* user_oid - The user who is currenlty logged into the Trade Portal. */
      attributeMgr.registerAssociation("user_oid", "a_user_oid", "User");
      
      //initial or relogon attempt
      attributeMgr.registerAttribute("logon_type", "logon_type");
      
      //if a relogon attempt, did the user confirm relogon or not
      attributeMgr.registerAttribute("confirm_relogon", "confirm_relogon");
      
      //flag to determine if the session should be forced out
      attributeMgr.registerAttribute("force_logout", "force_logout");
   }
   
 
   
 
 
   
}
