package com.ams.tradeportal.busobj;

/**
 * The Payable Invoice Payment Instructions for Bank defined Trading Parter.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface PayableInvPayInst extends TradePortalBusinessObject
{   
}
