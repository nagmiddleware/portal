
  

package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;




/*
 * Placing a row into this table will cause an e-mail to be sent.  The contents
 * of the e-mail are based on the email_data column.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class EmailTriggerBean_Base extends TradePortalBusinessObjectBean
{
private static final Logger LOG = LoggerFactory.getLogger(EmailTriggerBean_Base.class);

  
  /* 
   * Register the attributes and associations of the business object
   */
   protected void registerAttributes() throws AmsException
   {  

      /* Register attributes defined in the Ancestor class */
      super.registerAttributes();
      
      /* email_trigger_oid - Unique identifier */
      attributeMgr.registerAttribute("email_trigger_oid", "email_trigger_oid", "ObjectIDAttribute");
      
      /* agent_id - The ID of the agent that has picked up this row to send as an e-mail. */
      attributeMgr.registerAttribute("agent_id", "agent_id");
      
      /* status - The status of the email trigger. */
      attributeMgr.registerReferenceAttribute("status", "status", "EMAIL_TRIGGER_STATUS");
      
      /* email_data - XML data that is used to describe the contents of the e-mail. */
      attributeMgr.registerAttribute("email_data", "email_data");

      /* creation_timestamp - Timestamp (in GMT) of when this business object instance was created */
      attributeMgr.registerAttribute("creation_timestamp", "creation_timestamp", "DateTimeAttribute");
      
      /* send_attempt_count - This is incremented each time the email is attempted to be sent.  If this
         is high and other messages are being sent succesfully, there is likely a
         problem with this message. */
      attributeMgr.registerAttribute("send_attempt_count", "send_attempt_count", "NumberAttribute");
      
      //BSL Cocoon Upgrade 10/11/11 Rel 7.0 BEGIN
      /* attachment_data - binary data to attach to the e-mail */
      attributeMgr.registerAttribute("attachment_data", "attachment_data");
      //BSL Cocoon Upgrade 10/11/11 Rel 7.0 END
   }
   
 
   
 
 
   
}
