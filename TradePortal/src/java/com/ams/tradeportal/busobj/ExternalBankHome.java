
  

package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;
/**
 *Kyriba
 */
public interface ExternalBankHome extends EJBHome
{
   public ExternalBank create()
      throws RemoteException, CreateException, AmsException;

   public ExternalBank create(ClientServerDataBridge csdb)
      throws RemoteException, CreateException, AmsException;
}
