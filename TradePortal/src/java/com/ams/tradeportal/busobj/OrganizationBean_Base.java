
  

package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;




/*
 * Abstract business object to represent an organization in the Trade Portal.
 * All organizations are subclasses of this business object.
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class OrganizationBean_Base extends TradePortalBusinessObjectBean
{
private static final Logger LOG = LoggerFactory.getLogger(OrganizationBean_Base.class);

  
  /* 
   * Register the attributes and associations of the business object
   */
   protected void registerAttributes() throws AmsException
   {  

      /* Register attributes defined in the Ancestor class */
      super.registerAttributes();
      
      /* organization_oid - Unique identifier */
      attributeMgr.registerAttribute("organization_oid", "organization_oid", "ObjectIDAttribute");
      
      /* name - The name of the organization */
      attributeMgr.registerAttribute("name", "name");
      attributeMgr.requiredAttribute("name");
      attributeMgr.registerAlias("name", getResourceManager().getText("OrganizationBeanAlias.name", TradePortalConstants.TEXT_BUNDLE));
      
      /* activation_status - Indicates whether or not the organization has been deactivated.  In most
         cases, organizations are not deleted from the database; they are deactivated
         instead. */
      attributeMgr.registerReferenceAttribute("activation_status", "activation_status", "ACTIVATION_STATUS");
      attributeMgr.requiredAttribute("activation_status");
      
      /* date_deactivated - Timestamp (in GMT) of when the organization was deactivated. */
      attributeMgr.registerAttribute("date_deactivated", "date_deactivated", "DateTimeAttribute");
      
      /* opt_lock - Optimistic lock attribute
         See jPylon documentation for details on how this works */
      attributeMgr.registerAttribute("opt_lock", "opt_lock", "OptimisticLockAttribute");
      
   }
   
 
   
 
 
   
}
