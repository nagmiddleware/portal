package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;
/**
 * Subclass of InstrumentData that represents a template's data in the database
 * (in the INSTRUMENT table).  There is another business object, Template,
 * that represents the reference data entry for a template.
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface TemplateInstrumentHome extends EJBHome
{
   public TemplateInstrument create()
      throws RemoteException, CreateException, AmsException;

   public TemplateInstrument create(ClientServerDataBridge csdb)
      throws RemoteException, CreateException, AmsException;
}
