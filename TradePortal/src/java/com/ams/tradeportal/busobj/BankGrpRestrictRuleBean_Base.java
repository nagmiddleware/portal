package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;

import java.rmi.*;
import java.util.*;
import java.math.*;

import javax.ejb.*;

import com.ams.tradeportal.common.*;

/**
 * Business object to store commonly used BankGrpRestrictRules
 *
 * Copyright � 2015 CGI, Incorporated All rights reserved
 */
public class BankGrpRestrictRuleBean_Base extends ReferenceDataBean {
private static final Logger LOG = LoggerFactory.getLogger(BankGrpRestrictRuleBean_Base.class);

	/*
	 * Register the attributes and associations of the business object
	 */
	protected void registerAttributes() throws AmsException {

		super.registerAttributes();

		/* bank_grp_restrict_rules_oid - Unique identifier */
		attributeMgr.registerAttribute("bank_grp_restrict_rules_oid",
				"bank_grp_restrict_rules_oid", "ObjectIDAttribute");
		
		attributeMgr.registerAlias(
				"name",
				getResourceManager().getText(
						"BankGrpRestrictRuleBeanAlias.RuleName",
						TradePortalConstants.TEXT_BUNDLE));

		/* description - required */
		attributeMgr.registerAttribute("description", "description");
		attributeMgr.requiredAttribute("description");
		attributeMgr.registerAlias(
				"description",
				getResourceManager().getText(
						"BankGrpRestrictRuleBeanAlias.Description",
						TradePortalConstants.TEXT_BUNDLE));

		/* Pointer to the parent ReferenceDataOwner */
		attributeMgr.registerAttribute("client_bank_oid", "P_CLIENT_BANK_OID",
				"ParentIDAttribute");

	}

	/*
	 * Register the components of the business object
	 */
	protected void registerComponents() throws RemoteException, AmsException {
		/* Register the components defined in the Ancestor class */
		super.registerComponents();

		/* UserAuthorizedTemplateGroupList - */
		registerOneToManyComponent("BankGrpForRestrictRuleList",
				"BankGrpForRestrictRuleList");
	}

}
