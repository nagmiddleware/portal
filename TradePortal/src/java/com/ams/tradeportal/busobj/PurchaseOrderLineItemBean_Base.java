
  
/*
 * This file is generated from the model.  Normally it should not be modified manually.  
 * Contact the modeler first.  (In a pinch, it can be modified first and then retrofitted 
 * to the model.)
 *
 */
package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;


/*
 * Represents a purchase order line item that has been uploaded into the Trade
 * Portal.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class PurchaseOrderLineItemBean_Base extends TradePortalBusinessObjectBean
{
private static final Logger LOG = LoggerFactory.getLogger(PurchaseOrderLineItemBean_Base.class);

  
  /* 
   * Register the attributes and associations of the business object
   */
   protected void registerAttributes() throws AmsException
   {  

      /* Register attributes defined in the Ancestor class */
      super.registerAttributes();
      
      /* po_line_item_oid - Unique identfier */
      attributeMgr.registerAttribute("po_line_item_oid", "po_line_item_oid", "ObjectIDAttribute");
      
      /* line_item_num - Field to store the line item  number.   This field, in combination with
         the purchase_order_num field, will be unique within all PO line items for
         a particular corporate organization. */
      attributeMgr.registerAttribute("line_item_num", "line_item_num");
      
      /* unit_price - The individual unit price of the item. */
      attributeMgr.registerAttribute("unit_price", "unit_price", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      
      /* unit_of_measure - This is the unit of measure (for example, Cases, Cartons, Boxes etc) */
      attributeMgr.registerAttribute("unit_of_measure", "unit_of_measure");
      
      /* quantity - the quantity of the line item. */
      attributeMgr.registerAttribute("quantity", "quantity", "DecimalAttribute");
      
      /* quantity_variance_plus - The positive variance in percentage allowed above the agreed quantity */
      attributeMgr.registerAttribute("quantity_variance_plus", "quantity_variance_plus", "DecimalAttribute");
      
      /* prod_chars_ud1_label - User-defined product characteristic field label for Purchase Order */
      attributeMgr.registerAttribute("prod_chars_ud1_label", "prod_chars_ud1_label");
      
      /* prod_chars_ud2_label - User-defined product characteristic field label for Purchase Order */
      attributeMgr.registerAttribute("prod_chars_ud2_label", "prod_chars_ud2_label");
      
      /* prod_chars_ud3_label - User-defined product characteristic field label for Purchase Order */
      attributeMgr.registerAttribute("prod_chars_ud3_label", "prod_chars_ud3_label");
      
      /* prod_chars_ud4_label - User-defined product characteristic field label for Purchase Order */
      attributeMgr.registerAttribute("prod_chars_ud4_label", "prod_chars_ud4_label");
      
      /* prod_chars_ud5_label - User-defined product characteristic field label for Purchase Order */
      attributeMgr.registerAttribute("prod_chars_ud5_label", "prod_chars_ud5_label");
      
      /* prod_chars_ud6_label - User-defined product characteristic field label for Purchase Order */
      attributeMgr.registerAttribute("prod_chars_ud6_label", "prod_chars_ud6_label");
      
      /* prod_chars_ud7_label - User-defined product characteristic field label for Purchase Order */
      attributeMgr.registerAttribute("prod_chars_ud7_label", "prod_chars_ud7_label");
      
      /* prod_chars_ud1_value - User-defined product characteristic field value for Purchase Order */
      attributeMgr.registerAttribute("prod_chars_ud1_value", "prod_chars_ud1_value");
      
      /* prod_chars_ud2_value - User-defined product characteristic field value for Purchase Order */
      attributeMgr.registerAttribute("prod_chars_ud2_value", "prod_chars_ud2_value");
      
      /* prod_chars_ud3_value - User-defined product characteristic field value for Purchase Order */
      attributeMgr.registerAttribute("prod_chars_ud3_value", "prod_chars_ud3_value");
      
      /* prod_chars_ud4_value - User-defined product characteristic field value for Purchase Order */
      attributeMgr.registerAttribute("prod_chars_ud4_value", "prod_chars_ud4_value");
      
      /* prod_chars_ud5_value - User-defined product characteristic field value for Purchase Order */
      attributeMgr.registerAttribute("prod_chars_ud5_value", "prod_chars_ud5_value");
      
      /* prod_chars_ud6_value - User-defined product characteristic field value for Purchase Order */
      attributeMgr.registerAttribute("prod_chars_ud6_value", "prod_chars_ud6_value");
      
      /* prod_chars_ud7_value - User-defined product characteristic field value for Purchase Order */
      attributeMgr.registerAttribute("prod_chars_ud7_value", "prod_chars_ud7_value");

       /* derived_line_item_Ind - Indicates whether or not this Purchase Order Line Item was automatically
            added by system as opposed to being added by a user.

            Y=this PO line item was derived
            N=this PO line item was added by user */
       attributeMgr.registerAttribute("derived_line_item_ind", "derived_line_item_ind", "IndicatorAttribute");

      /* Pointer to the parent PurchaseOrder */
       attributeMgr.registerAttribute("purchase_order_oid", "p_purchase_order_oid", "ParentIDAttribute");
      
   }
   
 
   
 
 
   
}
