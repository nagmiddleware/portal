
  

package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;




/*
 * Rule for Payment Method Validation.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class PaymentMethodValidationBean_Base extends TradePortalBusinessObjectBean
{
private static final Logger LOG = LoggerFactory.getLogger(PaymentMethodValidationBean_Base.class);

  
  /* 
   * Register the attributes and associations of the business object
   */
   protected void registerAttributes() throws AmsException
   {  

      /* Register attributes defined in the Ancestor class */
      super.registerAttributes();
      
      /* payment_method_validation_oid - Object ID. */
      attributeMgr.registerAttribute("payment_method_validation_oid", "payment_method_validation_oid", "ObjectIDAttribute");
      
      /* payment_method - Payment Method */
      attributeMgr.registerReferenceAttribute("payment_method", "payment_method", "PAYMENT_METHOD");
      attributeMgr.requiredAttribute("payment_method");
      attributeMgr.registerAlias("payment_method", getResourceManager().getText("PaymentMethodValidationBeanAlias.payment_method", TradePortalConstants.TEXT_BUNDLE));
      
      /* country - country */
      attributeMgr.registerReferenceAttribute("country", "country", "COUNTRY");
      attributeMgr.requiredAttribute("country");
      attributeMgr.registerAlias("country", getResourceManager().getText("PaymentMethodValidationBeanAlias.country", TradePortalConstants.TEXT_BUNDLE));
      
      /* offset_days - Number of Offset Days given the payment method and country. */
      attributeMgr.registerAttribute("offset_days", "offset_days", "NumberAttribute");
      attributeMgr.requiredAttribute("offset_days");
      attributeMgr.registerAlias("offset_days", getResourceManager().getText("PaymentMethodValidationBeanAlias.offset_days", TradePortalConstants.TEXT_BUNDLE));
      
      /* currency - Currency */
      attributeMgr.registerReferenceAttribute("currency", "currency", "CURRENCY_CODE");
      attributeMgr.requiredAttribute("currency");
      attributeMgr.registerAlias("currency", getResourceManager().getText("PaymentMethodValidationBeanAlias.currency", TradePortalConstants.TEXT_BUNDLE));
      
      /* opt_lock - Optimistic lock attribute.  We need this to take advantage the locking in
         the framework of RefDataMediator.  But we are not inheriting from ReferenceData
         since we do not need the other attributes.
         See jPylon documentation for details on how this works */
      attributeMgr.registerAttribute("opt_lock", "opt_lock", "OptimisticLockAttribute");
      
   }
   
 
   
 
 
   
}
