package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.RemoteException;
import com.amsinc.ecsg.frame.AmsException;

/**
 *  *
 */
public class BankTransactionUpdateBean_Base extends TradePortalBusinessObjectBean {
private static final Logger LOG = LoggerFactory.getLogger(BankTransactionUpdateBean_Base.class);
	
	/*
	   * Register the attributes and associations of the business object
	   */
	   protected void registerAttributes() throws AmsException
	   {
		   
		  /* Register attributes defined in the Ancestor class */
		      super.registerAttributes();
		      
	      /* bank_transaction_update_oid - Unique Identifier */
	      attributeMgr.registerAttribute("bank_transaction_update_oid", "bank_transaction_update_oid", "ObjectIDAttribute");
	     
	      /*bank_transaction_status - REFDATA table Type - BANK_TRANSACTION_STATUS*/
	      attributeMgr.registerReferenceAttribute("bank_transaction_status", "bank_transaction_status", "BANK_TRANSACTION_STATUS");
	      
	      /*bank_rejection_reason_text - Rejection reason*/
	      attributeMgr.registerAttribute("bank_rejection_reason_text", "bank_rejection_reason_text");
	      
	      attributeMgr.registerAttribute("bank_internal_info", "bank_internal_info");
	      
	      /*bank_transaction_status - REFDATA table Type - BANK_UPDATE_STATUS_TYPE*/
	      attributeMgr.registerReferenceAttribute("bank_update_status", "bank_update_status", "BANK_UPDATE_STATUS_TYPE");
	      
	      /* user_oid -  */
	      attributeMgr.registerAssociation("bank_user_oid", "a_bank_user_oid", "User");
	      
	      /* opt_lock - Optimistic lock attribute
	         See jPylon documentation for details on how this works */
	      attributeMgr.registerAttribute("opt_lock", "opt_lock", "OptimisticLockAttribute");
	      
	      /* transaction_oid - The transaction which initiated the request */
	      attributeMgr.registerAssociation("transaction_oid", "a_transaction_oid", "Transaction");

	   }
	   
	   /* 
	    * Register the components of the business object
	    */
	    protected void registerComponents() throws RemoteException, AmsException
	    { 
	    	/* BankTransactionHistoryList - The Transaction has many BankTransactionHistory.  */
	        registerOneToManyComponent("BankTransactionHistoryList","BankTransactionHistoryList");
	        
	        /* DocumentImageList - Each BankTransactionUpdate can have document images associated with it.   This relationship
	         represents that a document image belongs to a BankTransactionUpdate and is set by
	         the middleware. */
	         registerOneToManyComponent("DocumentImageList","DocumentImageList");
	    }

}
