package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;
/**
 * Business object to store commonly used phrases that comprise instruments
 * created in the Trade Portal.  Phrases then can be added into text fields
 * on  instruments.
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface PhraseHome extends EJBHome
{
   public Phrase create()
      throws RemoteException, CreateException, AmsException;

   public Phrase create(ClientServerDataBridge csdb)
      throws RemoteException, CreateException, AmsException;
}
