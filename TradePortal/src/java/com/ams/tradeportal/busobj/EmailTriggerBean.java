 
package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import java.text.*;
import com.ams.tradeportal.common.*;

/*
 * Placing a row into this table will cause an e-mail to be sent.  The contents
 * of the e-mail are based on the email_data column.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class EmailTriggerBean extends EmailTriggerBean_Base
{
private static final Logger LOG = LoggerFactory.getLogger(EmailTriggerBean.class);
 protected void userNewObject() throws RemoteException, AmsException
 {

   /* Perform any New Object processing defined in the Ancestor class */
   super.userNewObject();
        
   SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");

   this.setAttribute("creation_timestamp", formatter.format(new Date()));   
   this.setAttribute("send_attempt_count", "0");
 } 
}
