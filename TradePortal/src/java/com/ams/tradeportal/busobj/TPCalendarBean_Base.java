
  

package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;




/*
 * The business calendar that stores the holiday information.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class TPCalendarBean_Base extends TradePortalBusinessObjectBean
{
private static final Logger LOG = LoggerFactory.getLogger(TPCalendarBean_Base.class);

  
  /* 
   * Register the attributes and associations of the business object
   */
   protected void registerAttributes() throws AmsException
   {  

      /* Register attributes defined in the Ancestor class */
      super.registerAttributes();
      
      /* tp_calendar_oid - Unique Identifier. */
      attributeMgr.registerAttribute("tp_calendar_oid", "tp_calendar_oid", "ObjectIDAttribute");
      
      /* opt_lock - Optimistic lock attribute
         See jPylon documentation for details on how this works */
      attributeMgr.registerAttribute("opt_lock", "opt_lock", "OptimisticLockAttribute");
      
      /* calendar_name - Calendar Name */
      attributeMgr.registerAttribute("calendar_name", "calendar_name");
      attributeMgr.requiredAttribute("calendar_name");
      attributeMgr.registerAlias("calendar_name", getResourceManager().getText("TPCalendarBeanAlias.calendar_name", TradePortalConstants.TEXT_BUNDLE));
      
      /* weekend_day_1 - The first weekend day.  e.g. java.util.Calendar.SATURDAY.
 */
      attributeMgr.registerAttribute("weekend_day_1", "weekend_day_1", "NumberAttribute");
      
      /* weekend_day_2 - The second weekend day.  e.g. java.util.Calendar.SUNDAY. */
      attributeMgr.registerAttribute("weekend_day_2", "weekend_day_2", "NumberAttribute");
      
      /* creation_date - The date when the calendar is created. */
      attributeMgr.registerAttribute("creation_date", "creation_date", "DateAttribute");
      
   }
   
  /* 
   * Register the components of the business object
   */
   protected void registerComponents() throws RemoteException, AmsException
   {  
      /* Register the components defined in the Ancestor class */
      super.registerComponents();
      
      /* TPCalendarYearList - Each TPCalendar has one-to-many TPCalendarYear components.  Each TPCalendarYear
         has the holiday information for one year. */
      registerOneToManyComponent("TPCalendarYearList","TPCalendarYearList");
   }

 
   
 
 
   
}
