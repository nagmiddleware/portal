
package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.RemoteException;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Set;
import java.util.Vector;

import com.ams.tradeportal.common.EmailValidator;
import com.ams.tradeportal.common.HSMEncryptDecrypt;
import com.ams.tradeportal.common.Password;
import com.ams.tradeportal.common.SecurityRules;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.BusinessObject;
import com.amsinc.ecsg.frame.ComponentList;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.frame.ErrorManager;
import com.amsinc.ecsg.frame.InvalidAttributeValueException;
import com.amsinc.ecsg.frame.InvalidObjectAssociationException;
import com.amsinc.ecsg.util.DateTimeUtility;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.GMTUtility;
import com.amsinc.ecsg.util.StringFunction;



/*
 *
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class UserBean extends UserBean_Base
{
private static final Logger LOG = LoggerFactory.getLogger(UserBean.class);


  /**
   *
   *
   * @return Hashtable
   */
   public Hashtable getSecurityLimits() throws RemoteException, AmsException
   {
        String minimumPasswordLength = "";
        String passwordExpirationDays = "";
        String maxFailedLogons = "";
        String requireUpperLowerDigit = "";
        String passwordHistoryCount = "";

        if( this.getAttribute("ownership_level").equals(TradePortalConstants.OWNER_GLOBAL) )
        {
            // For Proponix users, use the values stored in a config file.
            minimumPasswordLength = String.valueOf(SecurityRules.getProponixPasswordMinimumLengthLimit());
            passwordExpirationDays = String.valueOf(SecurityRules.getProponixChangePasswordDaysLimit());
            maxFailedLogons = String.valueOf(SecurityRules.getProponixFailedLoginAttemptsLimit());
			requireUpperLowerDigit = SecurityRules.getProponixRequireUpperLowerDigit();
			passwordHistoryCount = SecurityRules.getPasswordHistoryCount();

        }
        else
        {

            // Create the client bank that this user directly or indirectly belongs to
 	        ClientBank bank = (ClientBank)this.createServerEJB("ClientBank", this.getAttributeLong("client_bank_oid"));

            // Pull the appropriate values from the client bank
		if( this.getAttribute("ownership_level").equals(TradePortalConstants.OWNER_CORPORATE) )
		{
		        minimumPasswordLength = bank.getAttribute("corp_min_password_length");
		        passwordExpirationDays = bank.getAttribute("corp_change_password_days");
		        maxFailedLogons = bank.getAttribute("corp_max_failed_attempts");
			    requireUpperLowerDigit = bank.getAttribute("corp_password_addl_criteria");
			    if(bank.getAttribute("corp_password_history").equals(TradePortalConstants.INDICATOR_YES))
			        passwordHistoryCount = bank.getAttribute("corp_password_history_count");
			    else
			        passwordHistoryCount = "0";
	        }
	        else
	        {
		        minimumPasswordLength = bank.getAttribute("bank_min_password_len");
		        passwordExpirationDays = bank.getAttribute("bank_change_password_days");
		        maxFailedLogons = bank.getAttribute("bank_max_failed_attempts");
				requireUpperLowerDigit = bank.getAttribute("bank_password_addl_criteria");
				if(bank.getAttribute("bank_password_history").equals(TradePortalConstants.INDICATOR_YES))
				    passwordHistoryCount = bank.getAttribute("bank_password_history_count");
				else
					passwordHistoryCount = "0";
    	    }
        }

        // Places values into hashtable
        Hashtable returnValue = new Hashtable(3);
        returnValue.put("minimumPasswordLength",minimumPasswordLength);
        returnValue.put("passwordExpirationDays",passwordExpirationDays);
        returnValue.put("maxFailedLogons",maxFailedLogons);
        returnValue.put("requireUpperLowerDigit",requireUpperLowerDigit);
        returnValue.put("passwordHistoryCount", passwordHistoryCount);
        return returnValue;
   }


/**
 * Validates the password according to the security rules.  Creates errors if the
 * password fails any of the validations.
 *
 * @return boolean - whether or not password was actually set
 * @exception java.rmi.RemoteException
 * @exception com.amsinc.ecsg.frame.AmsException
 */
  public boolean validateAndEncryptPassword()
    throws RemoteException, AmsException
  {
	// [START] CR-482
	boolean instanceHSMEnabled = HSMEncryptDecrypt.isInstanceHSMEnabled();
	String passwordValidationError = this.getAttribute("passwordValidationError");
	// [END] CR-482

    String password = this.getAttribute("newPassword");
    String salt = this.getAttribute("encryption_salt");

    // T36000030120-smanohar--check password complexity at server also for HSMEnabled instances-- START 
    String eMacKey = null;
    String eMac = null;
    // T36000030120-smanohar--check password complexity at server also for HSMEnabled instances-- END 
    // [START] CR-482
    String encryptedPassword = null;
    // [START] CR-543
    if(instanceHSMEnabled)
    {
    	// Make sure that the eMACKeyString passed in is a valid challenge/response and eMACKey pair
    	if(!Password.validateProtectedPassword(password))
    	{
    		LOG.error("[UserBean.validateAndEncryptPassword()] Decrypted eMACKeyString does not valid (challenge/response and eMACKey)");
    		throw new AmsException("Decrypted eMACKeyString does not valid (challenge/response and eMACKey)");
    	}

    	// T36000030120-smanohar--check password complexity at server also for HSMEnabled instances-- START 
    	// Get the eMACKey from the eMACKeyString
    	String[] eMACKeyStrings =  password.split("\\s");
       	String combinedPassword =eMACKeyStrings[1]; //publicKey encrypted SHA256 Hash(password) + '|' publicKey encrypted(password)) 
    	String[] passwordFields = combinedPassword.split("\\|"); 
    	eMacKey =passwordFields[0];
    	password =passwordFields[1];
    	eMac = eMACKeyStrings[0] + " " + eMacKey;

    	 // T36000030120-smanohar--check password complexity at server also for HSMEnabled instances-- END 
    	
    	LOG.debug("[UserBean.validateAndEncryptPassword()] Creating new challengeResponse to store as the encryptedPassword using a new challenge and eMACKey: {} ",eMacKey);
    	try {
    		encryptedPassword = HSMEncryptDecrypt.signMAC(HSMEncryptDecrypt.getNewIV(), eMacKey);
    	} catch (Exception e){
    		LOG.error("[UserBean.validateAndEncryptPassword()] Exception caught while trying to create new challengeResponse for encryptedPassword: "
    				,e);
    		throw new AmsException(e.getMessage());
    	}
    }
    // [END] CR-543
    else
    {
    	// For instances with HSM not enabled, encrypt the password the old way
    	// [END] CR-482
    	encryptedPassword = Password.encryptPassword(password, salt);
    }

    String passwordFieldName;
    String retypePasswordFieldName;

    // Change the error parameters based on whether we are in insert mode
    if(this.isNew)
      {
	 passwordFieldName = getResourceManager().getText("ChangePassword.Password", TradePortalConstants.TEXT_BUNDLE);
	 retypePasswordFieldName = getResourceManager().getText("ChangePassword.RetypePassword", TradePortalConstants.TEXT_BUNDLE);
      }
    else
      {
	 passwordFieldName = getResourceManager().getText("ChangePassword.NewPassword", TradePortalConstants.TEXT_BUNDLE);
	 retypePasswordFieldName = getResourceManager().getText("ChangePassword.RetypeNewPassword", TradePortalConstants.TEXT_BUNDLE);
      }

    // Check that the new password and retype password fields are the same
    if(( !password.equals(this.getAttribute("retypePassword")) || passwordValidationError.equals(TradePortalConstants.ENTRIES_DONT_MATCH))) // CR-482 unless it is passed from the browser
        {

             this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                                               TradePortalConstants.ENTRIES_DONT_MATCH,
					       passwordFieldName,
					       retypePasswordFieldName);


             // Can't proceed further if these don't match
             return false;
        }

    // T36000030120-smanohar--check password complexity at server also for HSMEnabled instances-- START
    // Password cannot be the same as the current password
    if (!instanceHSMEnabled){
	    if( encryptedPassword.equals(this.getAttribute("password"))){ 
			this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
				                          TradePortalConstants.PASSWORD_SAME_AS_CURRENT);
	    }
    }else{ 
    	
    	String existingPassword = this.getAttribute("password");
     	String[] challengeResponse = new String[]{existingPassword};
     	
     	try {

     		boolean[] isRepeating = HSMEncryptDecrypt.validateMACs(challengeResponse,eMacKey);
			if ( isRepeating [0]){
				this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                        TradePortalConstants.PASSWORD_SAME_AS_CURRENT);
			}
		} catch (Exception e) {
 			LOG.error("UserBean:: Exception at validateAndEncryptPassword(): ",e);
		}
     }
    // T36000030120-smanohar--check password complexity at server also for HSMEnabled instances-- END 
    // Password cannot be the same as the user identifier, first name, last name, or certificate key
    if(( (password.equalsIgnoreCase(this.getAttribute("user_identifier"))))
    	|| passwordValidationError.equals(TradePortalConstants.PASSWORD_SAME_AS+this.getAttribute("user_identifier"))) // CR-482 unless it is passed from the browser
		this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
			                          TradePortalConstants.PASSWORD_SAME_AS,
						  passwordFieldName,
						  attributeMgr.getAlias("user_identifier"));
    //rkazi 10/09/2013 Rel 8.3 T36000021563 - Add - check if new password contains user identifier
    // Commented below code of IR T36000021563 to test IR T36000022315 issue(unable to create admin and Corporate USer)
    //MEer Rel 8.3 IR-22533 Reverting back the changes made towards IR-22315
    else if(( ( password.toLowerCase().contains(this.getAttribute("user_identifier").toLowerCase())))
    		 
    	|| passwordValidationError.equals(TradePortalConstants.PASSWORD_CONTAINS_ERROR+this.getAttribute("user_identifier"))) // CR-482 unless it is passed from the browser
		this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
			                          TradePortalConstants.PASSWORD_CONTAINS_ERROR,
						  passwordFieldName,
						  attributeMgr.getAlias("user_identifier"));

    if(( (password.equalsIgnoreCase(this.getAttribute("first_name"))))
    		 
        || passwordValidationError.equals(TradePortalConstants.PASSWORD_SAME_AS+this.getAttribute("first_name"))) // CR-482 unless it is passed from the browser
		this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
			                          TradePortalConstants.PASSWORD_SAME_AS,
						  passwordFieldName,
						  attributeMgr.getAlias("first_name"));
    //rkazi 10/09/2013 Rel 8.3 T36000021563 - Add - check if new password contains first name
 // Commented below code of IR T36000021563 to test IR T36000022315 issue(unable to create Admin and Corporate USer)
    //MEer Rel 8.3 IR-22533 Reverting back the changes made towards IR-22315
    else if(( (password.toLowerCase().contains(this.getAttribute("first_name").toLowerCase())))
    		  
        || passwordValidationError.equals(TradePortalConstants.PASSWORD_CONTAINS_ERROR+this.getAttribute("first_name"))) // CR-482 unless it is passed from the browser
		this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
			                          TradePortalConstants.PASSWORD_CONTAINS_ERROR,
						  passwordFieldName,
						  attributeMgr.getAlias("first_name"));

    if(( (password.equalsIgnoreCase(this.getAttribute("last_name"))))
    		  
    	|| passwordValidationError.equals(TradePortalConstants.PASSWORD_SAME_AS+this.getAttribute("last_name"))) // CR-482 unless it is passed from the browser
		this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
			                          TradePortalConstants.PASSWORD_SAME_AS,
						  passwordFieldName,
						  attributeMgr.getAlias("last_name"));
    //rkazi 10/09/2013 Rel 8.3 T36000021563 - Add - check if new password contains last name 
 // Commented below code of IR T36000021563 to test IR T36000022315 issue(unable to create Admin and Corporate USer)
    //MEer Rel 8.3 IR-22533 Reverting back the changes made towards IR-22315
    else if(( (password.toLowerCase().contains(this.getAttribute("last_name").toLowerCase())))
    		  
    	|| passwordValidationError.equals(TradePortalConstants.PASSWORD_CONTAINS_ERROR+this.getAttribute("last_name"))) // CR-482 unless it is passed from the browser
		this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
			                          TradePortalConstants.PASSWORD_CONTAINS_ERROR,
						  passwordFieldName,
						  attributeMgr.getAlias("last_name"));

    if(( (password.equalsIgnoreCase(this.getAttribute("login_id"))))
    		  
        || passwordValidationError.equals(TradePortalConstants.PASSWORD_SAME_AS+this.getAttribute("login_id"))) // CR-482 unless it is passed from the browser
		this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
			                          TradePortalConstants.PASSWORD_SAME_AS,
						  passwordFieldName,
						  attributeMgr.getAlias("login_id"));
    //rkazi 10/09/2013 Rel 8.3 T36000021563 - Add - check if new password contains login id  
 // Commented below code of IR T36000021563 to test IR T36000022315 issue(unable to create Admin and Corporate USer)
    //MEer Rel 8.3 IR-22533 Reverting back the changes made towards IR-22315
    else if(( (password.toLowerCase().contains(this.getAttribute("login_id").toLowerCase())))
    		  
        || passwordValidationError.equals(TradePortalConstants.PASSWORD_CONTAINS_ERROR+this.getAttribute("login_id"))) // CR-482 unless it is passed from the browser
		this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
			                          TradePortalConstants.PASSWORD_CONTAINS_ERROR,
						  passwordFieldName,
						  attributeMgr.getAlias("login_id"));

    //rkazi 10/09/2013 Rel 8.3 T36000021563 - Add - check if new password is considered a weak passwrod asper black list maintained in password_blacklist table 
 // Commented below code of IR T36000021563 to test IR T36000022315 issue(unable to create Admin and Corporate USer)
    //MEer Rel 8.3 IR-22533 Reverting back the changes made towards IR-22315
    if(( (checkForWeakPassword(password)))
    		 
        || passwordValidationError.equals(TradePortalConstants.PASSWORD_WEAK_ERROR+this.getAttribute("password"))) { // CR-482 unless it is passed from the browser
		this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                TradePortalConstants.PASSWORD_WEAK_ERROR,
                passwordFieldName);
		    	
    }
    // Password cannot start or end with a space
    if((( password.startsWith(" ") ||
    		password.endsWith(" ") ))
    		 
    	|| passwordValidationError.equals(TradePortalConstants.PASSWORD_SPACE)) // CR-482 unless it is passed from the browser
		this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
			                              TradePortalConstants.PASSWORD_SPACE);


    // Determine the minimum password length by looking up the security limits
    int minimumPasswordLength =
        Integer.parseInt((String)this.getSecurityLimits().get("minimumPasswordLength"));

    // Check password length
    if((( password.length() < minimumPasswordLength))
    		 
    	|| passwordValidationError.equals(TradePortalConstants.PASSWORD_TOO_SHORT)) // CR-482 unless it is passed from the browser
		this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
			                                   TradePortalConstants.PASSWORD_TOO_SHORT, String.valueOf(minimumPasswordLength));

    // The password must not contain the same character three times in a row
    char[] passwordChars = password.toCharArray();

    int consecutiveCharacterCount = 0;

    // Keep track of whether or not password contains certains
    // kinds of characters
    boolean containsUpperCase = false;
    boolean containsLowerCase = false;
    boolean containsDigit = false;


    	for(int i=0;i<passwordChars.length;i++)
    	{
    		if( (i!=0) && (passwordChars[i] == passwordChars[i-1]) )
    			consecutiveCharacterCount++;
    		else
    			consecutiveCharacterCount = 1;

    		if(Character.isDigit(passwordChars[i]))
    			containsDigit = true;

    		if(Character.isUpperCase(passwordChars[i]))
    			containsUpperCase = true;

    		if(Character.isLowerCase(passwordChars[i]))
    			containsLowerCase = true;

    		if( consecutiveCharacterCount >= 3)
    		{
    			this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
    					TradePortalConstants.PASSWORD_CONSECUTIVE);
    			break;
    		}
    	}
 
    if (passwordValidationError.equals(TradePortalConstants.PASSWORD_CONSECUTIVE)) // CR-482 unless it is passed from the browser)
    {
		this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
				TradePortalConstants.PASSWORD_CONSECUTIVE);
    }
    // [END] CR-482

     // Determine whether or not this password should be checked
     // for the presence of an upper case, lower case, and numeric characters
     String checkUpperLowerNumeric = (String) this.getSecurityLimits().get("requireUpperLowerDigit");

     if(((checkUpperLowerNumeric != null) && (checkUpperLowerNumeric.equals(TradePortalConstants.INDICATOR_YES)))
    		  
    	|| passwordValidationError.equals(TradePortalConstants.PASSWORD_UPPER_LOWER_DIGIT)) // CR-482 unless it is passed from the browser
      {
      	 // If password doesn't contain all the proper kinds of characters, issue an error
      	 if(!containsDigit ||
      	    !containsUpperCase ||
      	    !containsLowerCase)
      	     {
		       this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.PASSWORD_UPPER_LOWER_DIGIT);
      	     }
      }

    // Determine whether to check against the user's password history
	String passwordHistoryCountStr = (String) this.getSecurityLimits().get("passwordHistoryCount");
	int passwordHistoryCount = Integer.parseInt(passwordHistoryCountStr);

 	// Check the password history to see if they are reusing passwords
 	// If no history checking is set up, we still need to call this method
 	// so that any leftover (if setting had been previously set) history items will be purged.
	
	// T36000030120-smanohar--check password complexity at server also for HSMEnabled instances-- START
	if (instanceHSMEnabled){
		this.checkPasswordHistory(eMac, encryptedPassword, passwordHistoryCount);
	}else{
		this.checkPasswordHistory(password, encryptedPassword, passwordHistoryCount);	
	}
	 // T36000030120-smanohar--check password complexity at server also for HSMEnabled instances-- END
    // Set the actual password attribute
    // SVUK040873934 Check error severity instead of error count.  Ignore Warnings.
    if (this.getErrorManager().getMaxErrorSeverity () < ErrorManager.ERROR_SEVERITY)

	{
		// [START] CR-482
		LOG.debug("[UserBean.valdateAndEncryptPassword()] Saving encrypted password with length = {}",encryptedPassword.length());
		this.setAttribute("password", encryptedPassword);
		// [START] CR-543
		// [START] PAUK020233284
		if(instanceHSMEnabled && Password.isUserPasswordLegacy(this))
		// [END] PAUK020233284
		{
			LOG.debug("[UserBean.valdateAndEncryptPassword()] Converting user from Legacy to MAC password.  Setting encryption salt to {} ",TradePortalConstants.ENCRYPTION_SALT_BLANK);
			this.setAttribute("encryption_salt", TradePortalConstants.ENCRYPTION_SALT_BLANK);
		}
		// [END] CR-543
		// [END] CR-482

		return true;
	}
	else
	{
		return false;
	}

  }

  /**
   * This method does regular expression check for the new password to be a strong password.
   * For this we test it against a list of black listed passwords stored in the passsword_blacklist
   * table.
   * 
   * @param password
   * @return
   */
private boolean checkForWeakPassword(String password) throws RemoteException, AmsException {
	if (StringFunction.isBlank(password)){
		return false;
	}
	  // Query to retrieve the password history
	  // In order with the latest ones first
	 // LSuresh R91 IR T36000026319 - SQLINJECTION FIX
	  StringBuffer sql = new StringBuffer();
	  sql.append(" select count(password) passwordCount ");
	  sql.append(" from password_blacklist");
	  sql.append(" where REGEXP_LIKE (?");
	 sql.append(",password||'*','i')");


		DocumentHandler result = DatabaseQueryBean.getXmlResultSet(sql.toString(), false, new Object[]{password});
		if (result != null){
			int passwordCount = result.getAttributeInt("/ResultSetRecord(0)/PASSWORDCOUNT");
			if (passwordCount > 0){
				return true;
			}
		}

	return false;
}


//BSL 09/01/11 CR663 Rel 7.1 Begin
  /**
   * Validates the registration password according to the security rules.
   * Creates errors if the password fails any of the validations.
   *
   * @return boolean - whether or not registration password was actually set
   * @exception java.rmi.RemoteException
   * @exception com.amsinc.ecsg.frame.AmsException
   */
   public boolean validateAndEncryptRegistrationPassword() throws RemoteException, AmsException {
      String password = this.getAttribute("newRegistrationPassword");
      String salt = this.getAttribute("encryption_salt");
      String encryptedPassword = Password.encryptPassword(password, salt);

      // Password cannot be the same as the current password
      if (encryptedPassword.equals(this.getAttribute("registration_password"))) {
    	  this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
    			  TradePortalConstants.PASSWORD_SAME_AS_CURRENT);
      }

      // Password cannot start or end with a space
      if (password.startsWith(" ") || password.endsWith(" ")) {
    	  this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
    			  TradePortalConstants.PASSWORD_SPACE);
      }

      // Determine the minimum password length by looking up the security limits
      int minimumPasswordLength =
          Integer.parseInt((String)this.getSecurityLimits().get("minimumPasswordLength"));

      // Check password length
      if (password.length() < minimumPasswordLength) {
    	  this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
    			  TradePortalConstants.PASSWORD_TOO_SHORT,
    			  String.valueOf(minimumPasswordLength));
      }

      // The password must not contain the same character three times in a row
      char[] passwordChars = password.toCharArray();
      int consecutiveCharacterCount = 0;

      // Keep track of whether or not password contains certains
      // kinds of characters
      boolean containsUpperCase = false;
      boolean containsLowerCase = false;
      boolean containsDigit = false;

      for (int i=0; i < passwordChars.length; i++) {
    	  if ((i!=0) && (passwordChars[i] == passwordChars[i-1]))
    		  consecutiveCharacterCount++;
    	  else
    		  consecutiveCharacterCount = 1;

    	  if (Character.isDigit(passwordChars[i]))
    		  containsDigit = true;

    	  if (Character.isUpperCase(passwordChars[i]))
    		  containsUpperCase = true;

    	  if (Character.isLowerCase(passwordChars[i]))
    		  containsLowerCase = true;

    	  if (consecutiveCharacterCount >= 3) {
    		  this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
    				  TradePortalConstants.PASSWORD_CONSECUTIVE);
    		  break;
    	  }
      }

      // Determine whether or not this password should be checked for the
      // presence of an upper case, lower case, and numeric characters
      String checkUpperLowerNumeric = (String) this.getSecurityLimits().get("requireUpperLowerDigit");

      if (TradePortalConstants.INDICATOR_YES.equals(checkUpperLowerNumeric)) {
    	  // If password doesn't contain all the proper kinds of characters, issue an error
    	  if(!containsDigit || !containsUpperCase || !containsLowerCase) {
    		  this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
    				  TradePortalConstants.PASSWORD_UPPER_LOWER_DIGIT);
    	  }
      }

      // Determine whether to check against the user's password history
      String passwordHistoryCountStr = (String) this.getSecurityLimits().get("passwordHistoryCount");
      int passwordHistoryCount = Integer.parseInt(passwordHistoryCountStr);

      // Check the password history to see if they are reusing passwords
      // If no history checking is set up, we still need to call this method
      // so that any leftover (if setting had been previously set) history
      // items will be purged.
      checkPasswordHistory(password, encryptedPassword, passwordHistoryCount);

      // Set the actual password attribute
      if (this.getErrorManager().getMaxErrorSeverity() < ErrorManager.ERROR_SEVERITY) {
    	  LOG.debug("[UserBean.valdateAndEncryptRegistrationPassword()] Saving encrypted registration password with length = {}" , encryptedPassword.length());
    	  this.setAttribute("registration_password", encryptedPassword);

    	  return true;
      }
      else {
    	  return false;
      }
   }
   //BSL 09/01/11 CR663 Rel 7.1 End

  /**
   * Checks the entered password against the password history for the user
   * Also purges the password history
   *
   * @param newPassword - the password entered by the user
   * @param encryptedPassword - encrypted form of the password entered
   * @param passwordHistoryCount - the size of the password history to check
   * @exception java.rmi.RemoteException
   * @exception com.amsinc.ecsg.frame.AmsException
   */
  private void checkPasswordHistory(String newPassword, String encryptedPassword, int passwordHistoryCount) throws AmsException, RemoteException
  {
	  // [START] CR-543
	  boolean isInstanceHSMEnabled = HSMEncryptDecrypt.isInstanceHSMEnabled();
	  boolean isUserPasswordLegacy = Password.isUserPasswordLegacy(this);
	  // [END] CR-543

	  // Query to retrieve the password history
	  // In order with the latest ones first
	  StringBuffer sql = new StringBuffer();
	  sql.append(" select password_history_oid, password");
	  sql.append(" from password_history");
	  sql.append(" where p_user_oid = ?");
	
	  sql.append(" order by creation_timestamp desc");

	  DocumentHandler result = DatabaseQueryBean.getXmlResultSet(sql.toString(),false,new Object[]{this.getAttribute("user_oid")});

	  if(result != null)
	  {
		  Vector passwords = result.getFragments("/ResultSetRecord");

		  // [START] CR-543
		  if(isInstanceHSMEnabled && isUserPasswordLegacy)
		  {
			  LOG.debug("[UserBean.checkPasswordHistory(String,String,int)] Instance is HSM enabled and user has a legacy password.  Purging legacy password history.");
			  LOG.debug("[UserBean.checkPasswordHistory(String,String,int)] Found {} legacy passwords to purge.",passwords.size());

			  for(int i=0;i<passwords.size();i++)
			  {
				  String oldPasswordOid = result.getAttribute("/ResultSetRecord("+i+")/PASSWORD_HISTORY_OID");

				  // Create EJB for the password history, then delete it
				  PasswordHistory oldPasswordToDelete = (PasswordHistory) this.createServerEJB("PasswordHistory", Long.parseLong(oldPasswordOid));
				  oldPasswordToDelete.delete();
			  }

			  LOG.debug("[UserBean.checkPasswordHistory(String,String,int)] Successfully purged {} legacy passwords",passwords.size());
		  } 
		  else if (isInstanceHSMEnabled && !isUserPasswordLegacy)
		  {
			  LOG.debug("[UserBean.checkPasswordHistory(String,String,int)] Checking password history for MAC password user");

			  // Get the eMacKey that will be used to compare previous challenge/responses against
			  String[] eMACKeyStrings =  newPassword.split("\\s");
			  String eMacKey = eMACKeyStrings[1];
			  LOG.debug("[UserBean.checkPasswordHistory(String,String,int)] eMacKey = {} ",eMacKey);

			  // Validate against up to passwordHistoryCount or passwords.size(), whichever is less
			  String[] challengeResponses = null;
			  if(passwordHistoryCount < passwords.size())
			  {
				  challengeResponses = new String[passwordHistoryCount];
			  }
			  else
			  {
				  challengeResponses = new String[passwords.size()];
			  }

			  // Copy the stored challenge/responses into a String[]
			  for(int i=0; i<challengeResponses.length; i++)
			  {
				  challengeResponses[i] = result.getAttribute("/ResultSetRecord("+i+")/PASSWORD");
				  LOG.debug("[UserBean.checkPasswordHistory(String,String,int)] .Copied results into challengeResponses");
			  } 

			  // Validate the list of previous challenge/responses against the new eMacKey
			  boolean[] validChallengeResponses = null;
			  try
			  {
				  validChallengeResponses = HSMEncryptDecrypt.validateMACs(challengeResponses, eMacKey);
				  LOG.debug("[UserBean.checkPasswordHistory(String,String,int)] .Validated challenge/responses");
			  } catch (Exception e) {
				  LOG.error("[UserBean.checkPasswordHistory(String,String,int)] Caught the following exception while trying to validate {} with {} : "
						  ,new Object[]{challengeResponses.toString(),eMacKey ,e});
				  validChallengeResponses = null;
			  }

			  // Issue an error if one of the challenge/responses validated
			  if(validChallengeResponses!=null){
				  for(int i=0; i<validChallengeResponses.length; i++)
				  {
					  if(validChallengeResponses[i])
					  {
						  LOG.debug("[UserBean.checkPasswordHistory(String,String,int)]. Found a match for a previous challenge/response: {}",challengeResponses[i]);
						  this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.PASSWORD_HISTORY, String.valueOf(passwordHistoryCount));
						  break;
					  }
				  }
			  }
		  } 
		  else
		  {
			  // Loop through old passwords that we're checking against
			  // The passwords are ordered from most recent to oldest
			  // Only loop through the number that we're checking against
			  // Also don't loop beyond the end of the list (if the history isn't full yet)
			  for(int i=0;(i<passwordHistoryCount) && (i<passwords.size());i++)
			  {
				  String oldPassword = result.getAttribute("/ResultSetRecord("+i+")/PASSWORD");

				  // See if the encrypted version of the user's new password
				  // matches the encryped password stored in the password history
				  if(oldPassword.equals(encryptedPassword))
				  {
					  this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.PASSWORD_HISTORY, String.valueOf(passwordHistoryCount));
					  break;
				  }
			  }
		  }

		  if(!(isInstanceHSMEnabled && isUserPasswordLegacy))
		  {

			  int startPurgeAtIndex;

			  // Purge the password history
			  // Any password history entries that are older than the
			  // number to check against - 1 (-1 because we are adding the
			  // user's new password) will be deleted.  If the history count
			  // is 0, purge all items in the list since history should not
			  // be tracked
			  if(passwordHistoryCount == 0)
				  startPurgeAtIndex = 0;
			  else
				  startPurgeAtIndex = passwordHistoryCount-1;


			  for(int i=startPurgeAtIndex;i<passwords.size();i++)
			  {
				  String oldPasswordOid = result.getAttribute("/ResultSetRecord("+i+")/PASSWORD_HISTORY_OID");

				  // Create EJB for the password history, then delete it
				  PasswordHistory oldPasswordToDelete = (PasswordHistory) this.createServerEJB("PasswordHistory", Long.parseLong(oldPasswordOid));
				  oldPasswordToDelete.delete();
			  }
		  }
		  // [END] CR-543
	  }

	  // Only create an entry in the history if we are keeping track of the history
	  if(passwordHistoryCount > 0)
	  {
		  // Create an entry for the user's new password in the password history list
		  long newOid = this.newComponent("PasswordHistoryList");
		  PasswordHistory newPasswordHistory = (PasswordHistory) this.getComponentHandle("PasswordHistoryList", newOid);
		  newPasswordHistory.setAttribute("password", encryptedPassword);
	  }
  }

  protected void preSave ()
              throws AmsException
    {
	  super.preSave();
	  try
	  {
		  if(isDuplicateReportCategs()){
			  this.getErrorManager().issueError(
					  TradePortalConstants.ERR_CAT_1,
					  TradePortalConstants.DUPLICATE_REPT_CATEGS);
		  }  //Narayan - 06/17/2011 IR-ALUL0616770572
		  //RavindraB - Rel800 IR#LKUM021755721 - 7th Mar 2012 - Begin 
		  /**	
		   * If the user enters a value in the Security Device ID field, but does not select either the RSA or VASCO radio button, 
		   * then upon save the system should display error message � �Type of Security Device not specified�.
		   */     
		  //IR 19969 - If the user selects 2FA additional authentication field, but does not select either the RSA or VASCO radio button throw error
		  if ((StringFunction.isNotBlank(this.getAttribute("token_id_2fa")) ||
				  TradePortalConstants.AUTH_2FA.equalsIgnoreCase(this.getAttribute("additional_auth_type")))
				  && (!TradePortalConstants.RSA_TOKEN.equals(getAttribute("token_type")) 
						  && !TradePortalConstants.VASCO_TOKEN.equals(getAttribute("token_type")) ) 
				  ){
			  this.getErrorManager().issueError(
					  TradePortalConstants.ERR_CAT_1,
					  TradePortalConstants.MISSING_SECURITY_TYPE_DEVICE);
		  }
		
		  
		  if(isNew && !this.getAttribute("owner_org_oid").equals(""))
		  {
			  // Sets the client bank association for a user.  This association is a shortcut to make it easier to
			  // get to the user's client bank.   Once set, it cannot change, so we only need to do it when the user is new.
			  // Don't need to do this if the owner_org_oid is not set, because that will cause an error so this will be rolled
			  // back anyway
			  if(this.getAttribute("ownership_level").equals(TradePortalConstants.OWNER_GLOBAL))
			  {
				  this.setAttribute("client_bank_oid", "");
			  }
			  else if(this.getAttribute("ownership_level").equals(TradePortalConstants.OWNER_BANK))
			  {
				  this.setAttribute("client_bank_oid", this.getAttribute("owner_org_oid"));
			  }
			  else if(this.getAttribute("ownership_level").equals(TradePortalConstants.OWNER_CORPORATE))
			  {
				  CorporateOrganization corp =
						  (CorporateOrganization) this.createServerEJB("CorporateOrganization", this.getAttributeLong("owner_org_oid"));

				  this.setAttribute("client_bank_oid", corp.getAttribute("client_bank_oid"));
			  }
			  else if(this.getAttribute("ownership_level").equals(TradePortalConstants.OWNER_BOG))
			  {
				  BankOrganizationGroup bog =
						  (BankOrganizationGroup)this.createServerEJB("BankOrganizationGroup", this.getAttributeLong("owner_org_oid"));

				  this.setAttribute("client_bank_oid", bog.getAttribute("client_bank_oid"));
			  }
		  }
		  if (this.isNew)
		  {
			  this.setAttribute("display_extended_listviews", TradePortalConstants.INDICATOR_NO);
		  }
		  else{
			  this.setAttribute("display_extended_listviews", TradePortalConstants.INDICATOR_YES);
		  }
	  } catch(RemoteException re)
	  {
		  LOG.error("Remote exception in User preSave(): ",re);
	  }
    }



   public String getListCriteria(String type)
   {
	  // Retrieve the user by login id
	  if (type.equals("byLoginId")) {
		 return "login_id = {0} and activation_status = {1}";
	  }

	  // Retrieve the user by organization OID
	  if (type.equals("byOrgOid")) {
		 return "p_owner_org_oid = {0} AND user_identifier = {1}";
	  }

	  // Retrieve the user by threshold group Oid
  	  if (type.equals("byThresholdGroupOid")) {
	 	return "a_threshold_group_oid = {0}";
  	  }

	  // Retrieve the user by threshold group Oid
  	  if (type.equals("activeByThresholdGroupOid")) {
	 	return "a_threshold_group_oid = {0} and activation_status = {1}";
  	  }

	  // Retrieve the user by activation status and bank organization group oid
 	  if (type.equals("activeByBankOrgOp")) {
		return "p_owner_org_oid = {0}  and activation_status = {1}";
	  }

	  // Retrieve the user by activation status and client bank oid
 	  if (type.equals("activeByClientBank")) {
		return "p_owner_org_oid = {0}  and activation_status = {1}";
	  }

	  // Retrieve the users by activation status and corporate org oid
 	  if (type.equals("activeByCorporateOrg")) {
		return "p_owner_org_oid = {0}  and activation_status = {1}";
	  }

  	  // Retrieve by security profile
  	  if (type.equals("bySecurityProfile")) {
	  	  return "(a_security_profile_oid = {0} or a_cust_access_sec_profile_oid = {0}) and activation_status = {1}";
  	  }

	  // Retrieve the user by work group Oid
	  if (type.equals("activeByWorkGroupOid")) {
	      return "a_work_group_oid = {0} and activation_status = {1}";
	  }

	  return "";
   }

/**
 * Default several User attributes for a new object:
 *   activation_status = ACTIVE
 *   logon_on_ind = N
 *   customer_access_ind = N
 *   ownership_level = CORPORATE
 *
 * @exception java.rmi.RemoteException
 * @exception com.amsinc.ecsg.frame.AmsException
 */
protected void userNewObject() throws java.rmi.RemoteException, com.amsinc.ecsg.frame.AmsException {
	/* Perform any New Object processing defined in the Ancestor class */
	super.userNewObject();

	//Ravindra - Rel7100 -IR VDUK100867774 - 11th Aug 2011 - Start
	this.setAttribute("creation_date", DateTimeUtility.getGMTDateTime());
	//Ravindra - Rel7100 -IR VDUK100867774 - 11th Aug 2011 - End
	/* Default value for activation_status */
	this.setAttribute("activation_status", TradePortalConstants.ACTIVE);

	/* Default value for new_user_ind */
	this.setAttribute("new_user_ind", TradePortalConstants.INDICATOR_YES);

	/* Default value for customer_access_ind */
	this.setAttribute("customer_access_ind", TradePortalConstants.INDICATOR_NO);

	/* Default value for ownership_level */
	this.setAttribute("ownership_level", TradePortalConstants.OWNER_CORPORATE);

        /* Default value for locked_out_ind */
        this.setAttribute("locked_out_ind", TradePortalConstants.INDICATOR_NO);

        /* Default value for invalid_login_attempt_count */
        this.setAttribute("invalid_login_attempt_count", "0");

        /* Compute a "salt" for this user.  The "salt" is used for password encryption */
        // [START] CR-543
        if(HSMEncryptDecrypt.isInstanceHSMEnabled())
        {
        	// All new users in an HSM Enabled environment will not have encryption salt
        	// because the new password encryption scheme does not require one
        	this.setAttribute("encryption_salt", TradePortalConstants.ENCRYPTION_SALT_BLANK);
        }
        else
        {
        	this.setAttribute("encryption_salt", Password.getEncryptedRandomBytes(Password.SALT_SIZE));
        }
        // [END] CR-543

 }

//BSL IR BIUM050341259 05/10/2012 Rel 8.0 BEGIN
/**
 * Overrides the BusinessObjectBean method to avoid invalid association
 * errors for Users that have been deactivated and subsequently had their
 * associated objects deleted.
 * 
 * This override ensures that activation_status is set before all the other
 * attributes 
 *
 * @param  attributePaths An array of attribute paths to be set.
 * @param  attributeValues An array of attribute values that correspond
 * to the specified attributePaths array.
 */
public void setAttributes(String[] attributePaths, String[] attributeValues)
throws RemoteException, AmsException {
	// First find and set the value of activation_status
	for (int i = 0; i < attributePaths.length; i++) {
		if ("activation_status".equals(attributePaths[i])) {
			setAttribute("activation_status", attributeValues[i]);
			break;
		}
	}

	// Now process the rest of the attributes
	super.setAttributes(attributePaths, attributeValues);
}

/**
 * Overrides the BusinessObjectBean method to avoid invalid association
 * errors for Users that have been deactivated and subsequently had their
 * associated objects deleted.
 * 
 * This override is identical to the original method except in the handling
 * of InvalidObjectAssociationException  
 *
 * @param   attributePath The path for the requested attribute (i.e.
 * first_name or /Account/account_id)
 * @param   attributeValue  The value that is to be set for the
 * specified attribute path.
 */
public void setAttribute (String attributePath, String attributeValue)
throws RemoteException, AmsException {
    try {
        // If no component string processing is required, set
        // the object's attribute
        if(!StringFunction.requiresComponentParsing(attributePath)) {
            attributeMgr.setAttributeValue(attributePath, attributeValue);
        }
        else {
            componentSetComponentAttribute(attributePath, attributeValue);
            newComponentCalled = true;
        }
    }
    catch (InvalidAttributeValueException attributeEx) {
        // Determine if an "Alias" has been registered on the attribute
        // manager. If an alias exists, issue the error using the alias
        // name, otherwise use the attribute name provided by the
        // attribute exception.
        String alias = attributeMgr.getAlias(attributePath);
        if(alias == null) {
            getErrorManager().issueError(getClass().getName(),
            		attributeEx.getMessage(), attributeValue, attributePath);
        }
        else {
            getErrorManager().issueError(getClass().getName(),
            		attributeEx.getMessage(), attributeValue, alias);
        }
    }
    catch (InvalidObjectAssociationException attributeEx)
    {
    	if (TradePortalConstants.INACTIVE.equals(getAttribute("activation_status"))) {
    		// Do nothing
    	}
    	else {
            getErrorManager().issueError(getClass().getName(),
            		attributeEx.getErrorID(), attributeValue,
            		attributeEx.getAssociationObjectName());
    	}
    }
}
//BSL IR BIUM050341259 05/10/2012 Rel 8.0 END

/**
 * Performs User validation:
 *    default_wip_view required for non_admin users
 *    unique user_identifier required.
 *
 * @exception java.rmi.RemoteException
 * @exception com.amsinc.ecsg.frame.AmsException
 */
public void userValidate() throws java.rmi.RemoteException, com.amsinc.ecsg.frame.AmsException {
	//BSL IR BIUM050341259 05/10/2012 Rel 8.0 BEGIN
	// No validations should be performed on inactive users
	if (TradePortalConstants.INACTIVE.equals(getAttribute("activation_status"))) {
		// Set required attributes without values to not required
		attributeMgr.clearRequired();

		return;
	}
	//BSL IR BIUM050341259 05/10/2012 Rel 8.0 END

	String alias = "";
	//Srinivasu_D IR#T36000048154 Rel9.5 04/22/2015 START - Email validation
		String emailAddr = getAttribute("email_addr");
		if (StringFunction.isNotBlank(emailAddr)) {
			if (performEmailValidation(emailAddr)) {
				this.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.INVALID_EMAIL, "");
			}
		}
		//Srinivasu_D IR#T36000048154 Rle9.5 04/22/2015 End
	// If the security profile for the user is a non_admin profile, we know
	// we're editing a non_admin user.  For these users the default_wip_view
	// is a required field.
	SecurityProfile secProfile;
	secProfile = (SecurityProfile) createServerEJB("SecurityProfile");
	long secProfileOid = 0l;
	boolean isNonAdminUser = false;

	try {
		secProfileOid = this.getAttributeLong("security_profile_oid");
		secProfile.getData(secProfileOid);
		isNonAdminUser = secProfile.getAttribute("security_profile_type").equals(TradePortalConstants.NON_ADMIN);
		if (isNonAdminUser) {
			attributeMgr.requiredAttribute("default_wip_view");
		}

	} catch (AmsException e) {
		LOG.error("AmsException trying to validate user: " , e);
	} catch (Exception e) {
		LOG.error("error trying to validate user " , e);
	}


	String userid = getAttribute("user_identifier");
	

	// now we need to check that user_identifier is unique
	long myOrg = this.getAttributeLong("owner_org_oid");
	if (!isUnique("user_identifier", userid, " and p_owner_org_oid = " + myOrg)) {
		this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
			                  TradePortalConstants.ALREADY_EXISTS,
                                          userid,
                                          attributeMgr.getAlias("user_identifier"));
	}

	// If the customer access indicator is N, we don't allow a customer
	// access security profile -- blank it out.
	String corpAccess = getAttribute("customer_access_ind");
	if ((corpAccess == null) || (corpAccess.equals(TradePortalConstants.INDICATOR_NO))) {
		setAttribute("cust_access_sec_profile_oid", "");
	} else {
		// The custoemr access indicator is Yes.  The customer access security
		// profile is required.  Any profile oid value except 0 is assumed to
		// be valid.  Otherwise, 0, or a non-number throws a required attribute
		//exception.

		if (isNonAdminUser) {
			alias = "Subsidiary Access Security Profile";
		} else {
			alias = "Customer Access Security Profile";
		}

		try {
			long profile = getAttributeLong("cust_access_sec_profile_oid");
			if (profile  == 0) {
	                	this.getErrorManager().issueError (
			           getClass().getName(), AmsConstants.REQUIRED_ATTRIBUTE,
					   alias);
			}
		} catch (AmsException e) {
			this.getErrorManager().issueError (
				   getClass().getName(), AmsConstants.REQUIRED_ATTRIBUTE,
				   alias);
		}
	}
	//-- CR-451 NShrestha 12/03/2008 Begin
	/*KMehta - 22 Sep 2014 - Rel9.1 IR-T36000032290 - Change  - Begin*/ 
	if (this.isNew){
		validateAccounts();
	}
	/*KMehta - 22 Sep 2014 - Rel9.1 IR-T36000032290 - Change  - End*/
	//-- CR-451 NShrestha 12/03/2008 End

	//VSUK090955243 VS
	validateTemplateGroups();
	// NSX 06/29/10 RIUK062552727 - Begin
	if (TradePortalConstants.INDICATOR_YES.equals(getAttribute("customer_access_ind")) ) {
		if (StringFunction.isBlank(getAttribute("subsidiary_threshold_group_oid")) && StringFunction.isNotBlank(getAttribute("threshold_group_oid"))) {
			this.getErrorManager().issueError (
					getClass().getName(), TradePortalConstants.NO_SUBSIDARY_THRESHOLD);
		}
		if (StringFunction.isBlank(getAttribute("subsidiary_work_group_oid")) && StringFunction.isNotBlank(getAttribute("work_group_oid"))) {
			this.getErrorManager().issueError (
					getClass().getName(), TradePortalConstants.NO_SUBSIDARY_WORKGRP);
		}
	}
	// NSX 06/29/10 RIUK062552727 - End

	// The owner org is required.  Any number value except 0 is assumed to be
	// a valid org.  Otherwise, 0, or a non-number throws a required attribute
	// exception.
	alias = attributeMgr.getAlias("owner_org_oid");
	if (alias == null) alias = "Organisation";
	try {
		long ownerOrg = getAttributeLong("owner_org_oid");
		if (ownerOrg == 0) {
	                this.getErrorManager().issueError (
			           getClass().getName(), AmsConstants.REQUIRED_ATTRIBUTE,
					   alias);
		}
	} catch (AmsException e) {
				this.getErrorManager().issueError (
				   getClass().getName(), AmsConstants.REQUIRED_ATTRIBUTE,
				   alias);
	}


        // Security validations are as follows:
        // --Certificate or login id/password are required.
        // --Authentication method (certificate or password) can be found on either
        //     global_organization, client_bank, or corporate_org.
        // --Login id must be unique across the client bank (or Proponix for Proponix
        //     users.  This is true even for certificate authentication (but only if
        //     login id is non-blank)
        // --Passwords are validated in another method.  Although not required for
        //     certificate authentication, if provided, they must follow the same
        //     validation rules.

	// Check that login is unique across either Proponix or within the
        // client bank.  (Only test non-blank values).
        String whereClause;
        if(this.getAttribute("ownership_level").equals(TradePortalConstants.OWNER_GLOBAL))
            whereClause = " and a_client_bank_oid is null";
        else
            whereClause = " and a_client_bank_oid = "+this.getAttribute("client_bank_oid");

        if (StringFunction.isNotBlank(this.getAttribute("login_id"))) {
          if (!isUnique("login_id", this.getAttribute("login_id"), whereClause))
	  {
		this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
			                              TradePortalConstants.ALREADY_EXISTS,
                                                      this.getAttribute("login_id"),
                                                      attributeMgr.getAlias("login_id"));
	  }
        }

        // Perform the same check for the certificate_id
        if (StringFunction.isNotBlank(this.getAttribute("certificate_id"))) {
          if (!isUnique("certificate_id", this.getAttribute("certificate_id"), whereClause))
	  {
		this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
			                              TradePortalConstants.ALREADY_EXISTS,
                                                      this.getAttribute("certificate_id"),
                                                      attributeMgr.getAlias("certificate_id"));
	  }
        }

        // CR-482 Check the 2FA ID is unique
        if (StringFunction.isNotBlank(this.getAttribute("token_id_2fa"))) {
          if (!isUnique("token_id_2fa", this.getAttribute("token_id_2fa"), whereClause))
      {
        this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                                          TradePortalConstants.ALREADY_EXISTS,
                                                      this.getAttribute("token_id_2fa"),
                                                      attributeMgr.getAlias("token_id_2fa"));
      }
        }

        //cquinton 4/15/2011 Rel 7.0.0 ir#clul041136190 start
        if (StringFunction.isNotBlank(this.getAttribute("sso_id"))) {
            if (!isUnique("sso_id", this.getAttribute("sso_id"), whereClause)) {
                this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                     TradePortalConstants.ALREADY_EXISTS,
                     this.getAttribute("sso_id"),
                     attributeMgr.getAlias("sso_id"));
            }
        }
        //cquinton 4/15/2011 Rel 7.0.0 ir#clul041136190 end

        //BSL 09/01/11 CR663 Rel 7.1 Begin
        // Check that SSO Registration Login ID is unique
        if (StringFunction.isNotBlank(this.getAttribute("registration_login_id"))) {
            if (!isUnique("registration_login_id", this.getAttribute("registration_login_id"), whereClause)) {
                this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                     TradePortalConstants.ALREADY_EXISTS,
                     this.getAttribute("registration_login_id"),
                     attributeMgr.getAlias("registration_login_id"));
            }
        }

        // Check that Registered SSO ID is unique
        if (StringFunction.isNotBlank(this.getAttribute("registered_sso_id"))) {
            if (!isUnique("registered_sso_id", this.getAttribute("registered_sso_id"), whereClause)) {
                this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                     TradePortalConstants.ALREADY_EXISTS,
                     this.getAttribute("registered_sso_id"),
                     attributeMgr.getAlias("registered_sso_id"));
            }
        }
        //BSL 09/01/11 CR663 Rel 7.1 End

        if(  StringFunction.isNotBlank(this.getAttribute("doc_prep_ind")) &&
            this.getAttribute("doc_prep_ind").equals(TradePortalConstants.INDICATOR_YES) )
        {
           // If a user is set up for doc prep, but there is no username, give a warning.
           if(!StringFunction.isNotBlank(this.getAttribute("doc_prep_username")))
            {
		this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.DOC_PREP_USERNAME);
            }
        }
       else
        {
           // If user is not set up for doc prep, but a username has been specified,
           // blank out the username
           if(StringFunction.isNotBlank(this.getAttribute("doc_prep_username")))
            {
               this.setAttribute("doc_prep_username", "");
            }
        }

        // When GEMALTO is selected for certificate type, SSO ID is required.  Rel 9.4 CR-923 W Zhu 9/15/2015
        if(  TradePortalConstants.GEMALTO.equals(this.getAttribute("certificate_type"))
                && StringFunction.isBlank(this.getAttribute("sso_id"))) {
                this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                     TradePortalConstants.SSO_REQUIRED_FOR_GEMALTO);
        }

                // Use the associated client bank or owning GlobalOrganization to determine if
       // password fields should be validated.  We look at the authentication_method
       // for the global organization (Proponix), or for the client bank (for client
       // bank and bog users), or the corporate org.   By definition, if a client bank
       // only allows certificates for its corporates, the corporates will have the
       // certificate setting also.
       // This is only validated when the user is opened as a stand-alone object.
       // Otherwise when a corporate organization's authentication method is changed,
       // it may automatically modify its user components' authentication method.  The user object
       // should not try to validate again since the organization object is not saved yet.
		if (!this.isComponent()) {
			Organization org;
			String ownershipLevel = this.getAttribute("ownership_level");

			// Create the appropirate org (each has an "authentication_method" column
			if (ownershipLevel.equals(TradePortalConstants.OWNER_GLOBAL))
			{
			org = (Organization)this.createServerEJB("GlobalOrganization",
                                                   this.getAttributeLong("owner_org_oid"));
       		}
       		else if (ownershipLevel.equals(TradePortalConstants.OWNER_CORPORATE))
       		{
          		org = (Organization)this.createServerEJB("CorporateOrganization",
                                                   this.getAttributeLong("owner_org_oid"));
       		}
       		else
       		{
          		org = (Organization)this.createServerEJB("ClientBank",
                                                  this.getAttributeLong("client_bank_oid"));
       		}


       		boolean checkForBlankPassword = true;

       		// At this point, the password is still unencrypted.
       		// Call this method (if anything was entered for the password)
       		// and then encrypt (errors get posted)
       		if( (!this.getAttribute("newPassword").equals(""))   ||
           		(!this.getAttribute("retypePassword").equals("")) )
       		{
               	if(!validateAndEncryptPassword())
                   checkForBlankPassword = false;
       		}

       		//BSL 09/01/11 CR663 Rel 7.1 Begin
       		boolean checkForBlankRegistrationPassword = true;

       		// At this point, the password is still unencrypted.
       		// Call this method (if anything was entered for the password)
       		// and then encrypt (errors get posted)
       		if (StringFunction.isNotBlank(this.getAttribute("newRegistrationPassword"))) {
       			checkForBlankRegistrationPassword = validateAndEncryptRegistrationPassword();
       		}
       		//BSL 09/01/11 CR663 Rel 7.1 End

       		// If the corporate organization specifies authentication method for
       		// each user individually, the user needs to have authentication method
       		// specified.
       		String orgAuthenticationMethod = org.getAttribute("authentication_method");
       		String userAuthenticationMethod = getAttribute("authentication_method");
       		boolean isAdmin = !TradePortalConstants.OWNER_CORPORATE.equals(ownershipLevel);
	   		if( (TradePortalConstants.AUTH_PERUSER.equals(orgAuthenticationMethod) || isAdmin )
	   				&& StringFunction.isBlank(userAuthenticationMethod)  )
 	   		{
	         	this.getErrorManager().issueError (
			    	getClass().getName(), AmsConstants.REQUIRED_ATTRIBUTE,
		         	attributeMgr.getAlias("authentication_method"));
	   		}


	   		// Based on the authentication method, perform validations.  For password, the
	   		// login id and password is required.  For certificate, the certificate id is
	   		// required.
       		if( (orgAuthenticationMethod.equals(TradePortalConstants.AUTH_PASSWORD) || orgAuthenticationMethod.equals(TradePortalConstants.AUTH_PERUSER) || isAdmin)
             	   && userAuthenticationMethod.equals(TradePortalConstants.AUTH_PASSWORD) )
       		{
         		// Login id is required.
         		if( StringFunction.isBlank( this.getAttribute("login_id") ) )
          		{
	     			this.getErrorManager().issueError (
	           			getClass().getName(), AmsConstants.REQUIRED_ATTRIBUTE,
		   				attributeMgr.getAlias("login_id"));
          		}

         		// Only check for a blank password if there were no errors during validation
         		if(checkForBlankPassword)
          		{
             		if( StringFunction.isBlank(this.getAttribute("password")) )
              		{
	        			this.getErrorManager().issueError (
		           			getClass().getName(), AmsConstants.REQUIRED_ATTRIBUTE,
			   				attributeMgr.getAlias("password"));
              		}
          		}
       		}
       		else if( (orgAuthenticationMethod.equals(TradePortalConstants.AUTH_CERTIFICATE)
              		|| orgAuthenticationMethod.equals(TradePortalConstants.AUTH_PERUSER) || isAdmin)
              	   && userAuthenticationMethod.equals(TradePortalConstants.AUTH_CERTIFICATE) )
       		{
       			if ( StringFunction.isBlank(getAttribute("certificate_type")) ) {
       			       this.getErrorManager().issueError( TradePortalConstants.ERR_CAT_1, TradePortalConstants.MISSING_CERTIFICATE_TYPE);
       			// Nar IR-T36000045386 certificate id is required only for certificate_ID certificate type
       			} else if ( TradePortalConstants.CERTIFICATE_ID.equals(this.getAttribute("certificate_type")) ) {
          		   // If the authentication method is certificate, then certificate ID is required.
          		   if(this.getAttribute("certificate_id").equals(""))
          		   {
	     			this.getErrorManager().issueError (
	           			getClass().getName(), AmsConstants.REQUIRED_ATTRIBUTE,
		   			attributeMgr.getAlias("certificate_id"));
          		   }
       	 	    }
          		// CR-1071 added below condition for Admin. Login ID and password is required when Auth is certificate for Admin
          		if ( isAdmin ) {
          			
          		    // Login id is required.
             		if( StringFunction.isBlank( this.getAttribute("login_id") ) )
              		{
    	     			this.getErrorManager().issueError (
    	           			getClass().getName(), AmsConstants.REQUIRED_ATTRIBUTE,
    		   				attributeMgr.getAlias("login_id"));
              		}

             		// Only check for a blank password if there were no errors during validation
             		if(checkForBlankPassword)
              		{
                 		if( StringFunction.isBlank(this.getAttribute("password")) )
                  		{
    	        			this.getErrorManager().issueError (
    		           			getClass().getName(), AmsConstants.REQUIRED_ATTRIBUTE,
    			   				attributeMgr.getAlias("password"));
                  		}
              		}
          			
          		}
       		}
            else if( (orgAuthenticationMethod.equals(TradePortalConstants.AUTH_2FA)
                    || orgAuthenticationMethod.equals(TradePortalConstants.AUTH_PERUSER) || isAdmin)
                       && userAuthenticationMethod.equals(TradePortalConstants.AUTH_2FA) )
                {
                    // Login id is required.
                    if(this.getAttribute("login_id").equals(""))
                    {
                        this.getErrorManager().issueError (
                            getClass().getName(), AmsConstants.REQUIRED_ATTRIBUTE,
                            attributeMgr.getAlias("login_id"));
                    }

                    // token_id_2fa is required.
                    if(this.getAttribute("token_id_2fa").equals(""))
                    {
                        this.getErrorManager().issueError (
                            getClass().getName(), AmsConstants.REQUIRED_ATTRIBUTE,
                            attributeMgr.getAlias("token_id_2fa"));
                    }

                    // Only check for a blank password if there were no errors during validation
                    if(checkForBlankPassword)
                    {
                        if(this.getAttribute("password").equals(""))
                        {
                            this.getErrorManager().issueError (
                                getClass().getName(), AmsConstants.REQUIRED_ATTRIBUTE,
                                attributeMgr.getAlias("password"));
                        }
                    }
                }
       		else if( (orgAuthenticationMethod.equals(TradePortalConstants.AUTH_SSO)
              		|| orgAuthenticationMethod.equals(TradePortalConstants.AUTH_PERUSER) || isAdmin)
              	   && userAuthenticationMethod.equals(TradePortalConstants.AUTH_SSO) )
       		{
          		// If the authentication method is certificate, then certificate ID is required.
          		if(this.getAttribute("sso_id").equals(""))
          		{
	     			this.getErrorManager().issueError (
	           			getClass().getName(), AmsConstants.REQUIRED_ATTRIBUTE,
		   			attributeMgr.getAlias("sso_id"));
          		}
       		}
       		//BSL 09/01/11 CR663 Rel 7.1 Begin
       		else if (TradePortalConstants.AUTH_REGISTERED_SSO.equals(orgAuthenticationMethod)
       				|| TradePortalConstants.AUTH_PERUSER.equals(orgAuthenticationMethod)
       				&& TradePortalConstants.AUTH_REGISTERED_SSO.equals(userAuthenticationMethod))
           	{
       			// Registration Login ID is required.
       			if (StringFunction.isBlank(this.getAttribute("registration_login_id"))) {
       				this.getErrorManager().issueError(getClass().getName(),
       						AmsConstants.REQUIRED_ATTRIBUTE,
       						attributeMgr.getAlias("registration_login_id"));
       			}

       			// Only check for a blank Registration Password if there
       			// were no errors during validation
       			if (checkForBlankRegistrationPassword) {
       				if (StringFunction.isBlank(this.getAttribute("registration_password"))) {
       					this.getErrorManager().issueError(getClass().getName(),
       							AmsConstants.REQUIRED_ATTRIBUTE,
       							attributeMgr.getAlias("registration_password"));
       				}
       			}
           	}
       		//BSL 09/01/11 CR663 Rel 7.1 End
       		// Nar CR-1071 Rel9400 08/28/2015 Begin
       		if ( isAdmin ) {
       			validateUserAuthWithBankAuthMethod( orgAuthenticationMethod,  userAuthenticationMethod );
       		}
       	    // Nar CR-1071 Rel9400 08/28/2015 Begin
			/**********************************************************************
			 * Give warning if the user is not assigned to a work group while
			 * the org requires some instrument be authorized by users from
			 * different work groups.
			 * Note this validation happens only for standalone user object too.
			 * ********************************************************************/
			if (ownershipLevel.equals(TradePortalConstants.OWNER_CORPORATE)
				&& this.getAttribute("work_group_oid").equals("")
				&& (org.getAttribute("dual_auth_airway_bill").equals(TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS)
					|| org.getAttribute("dual_auth_export_coll").equals(TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS)
					|| org.getAttribute("dual_auth_guarantee").equals(TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS)
					|| org.getAttribute("dual_auth_import_DLC").equals(TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS)
					|| org.getAttribute("dual_auth_SLC").equals(TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS)
					|| org.getAttribute("dual_auth_shipping_guar").equals(TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS)
					|| org.getAttribute("dual_auth_export_LC").equals(TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS)
					|| org.getAttribute("dual_auth_loan_request").equals(TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS)
					|| org.getAttribute("dual_auth_funds_transfer").equals(TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS)
					|| org.getAttribute("dual_auth_request_advise").equals(TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS)))
			{
				this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.NO_WORKGROUP_ASSIGNED);
			}

       }
		/*pgedupudi - Rel-9.3 CR-976A 03/26/2015 Start*/
		String BankGrpRestrictRuleOid;
		BankGrpRestrictRuleOid = getAttribute("bank_grp_restrict_rule_oid");
		if(!StringFunction.isBlank(BankGrpRestrictRuleOid)){
			this.validateBankGroupRestrictRule(BankGrpRestrictRuleOid);
		}
		/*pgedupudi - Rel-9.3 CR-976A 03/26/2015 End*/

       // W Zhu 10/6/2015 Rel 9.4 CR-1018 add edit for allow_sso_password_ind
       String ssoPasswordInd = getAttribute("allow_sso_password_ind");
       String tokenType = getAttribute("token_type");
       String certificateType = getAttribute("certificate_type");
       if (TradePortalConstants.INDICATOR_YES.equals(ssoPasswordInd) 
              && (StringFunction.isNotBlank(tokenType)
                  || StringFunction.isNotBlank(certificateType)) ) {
           this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.SSO_PASSWORD_SELECTED);
       }
      

  }

// CR-451 NShrestha 12/03/2008 Begin
/*
 * validates if duplicate accounts are selected
 */
private void validateAccounts() throws RemoteException, AmsException {
	String acctOid = null;
	String acctDescription = null;
	ComponentList acctList = (ComponentList)this.getComponentHandle("UserAuthorizedAccountList");
	int tot = acctList.getObjectCount();
	Set set = new HashSet();
	for (int i = 0; i < tot; i++) {
		acctList.scrollToObjectByIndex(i);
		acctOid = acctList.getBusinessObject().getAttribute("account_oid");
		if(!set.contains(acctOid)) {
			set.add(acctOid);
		}
		else {
			this.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.DUPLICATE_ACCT_SELECTED);
			//return;
		}
	}

	//IR - PRUJ012743139 - Begin
	set.clear();
	for (int i = 0; i < tot; i++) {
		acctList.scrollToObjectByIndex(i);
		acctDescription = acctList.getBusinessObject().getAttribute("account_description");
		if(!set.contains(acctDescription.trim())) {
			set.add(acctDescription.trim());
		}
		else {
			this.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.DUPLICATE_ACCT_DESCRIPTION_SELECTED,
					acctDescription);

		}
	}
	//IR - PRUJ012743139 - End
}
// CR-451 NShrestha 12/03/2008 End

//VSUK090955243 VS 09/22/2010 Begin
/*
 * validates if duplicate accounts are selected
 */
private void validateTemplateGroups() throws RemoteException, AmsException {
	String templateGrpOid = null;

	BusinessObject busobj = null;
	ComponentList templateGrpList = (ComponentList)this.getComponentHandle("UserAuthorizedTemplateGroupList");
	int tem = templateGrpList.getObjectCount();
	Set sets = new HashSet();

	LOG.debug("Template Group List: {} ",templateGrpList);

	for (int i = 0; i < tem; i++) {
		templateGrpList.scrollToObjectByIndex(i);
		busobj = templateGrpList.getBusinessObject();
		templateGrpOid = busobj.getAttribute("payment_template_group_oid");
		LOG.debug("Template Group OID: {} {} ", i,templateGrpOid);
		//Vshah - 10/13/2010 - IR#VIUK101259666 - Add Below Condition.
		if (TradePortalConstants.INDICATOR_NO.equals(this.getAttribute("confidential_indicator")))
			checkTemplateGroupLink(busobj);
		if(!sets.contains(templateGrpOid)) {
			sets.add(templateGrpOid);
		}
		else {
			this.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.DUPLICATE_TEMPLATEGROUP_SELECTED);
			//return;
		}
	}
	sets.clear();
}
//VSUK090955243 VS 09/22/2010 End

//Vshah - 10/13/2010 - IR#VIUK101259666 - [BEGIN]
//System should not allow to link a template group with confidential templates to a user who does not have the confidential indicator selected.
private void checkTemplateGroupLink(BusinessObject busobj) throws RemoteException, AmsException {
	try {
		String templateGrpOid = null;
		String templateSQL = null;
		String userConfInd = null;
		String templateConfInd = null;

		DocumentHandler resultSet =  null;

		templateGrpOid = busobj.getAttribute("payment_template_group_oid");
		templateSQL = "SELECT CONFIDENTIAL_INDICATOR FROM TEMPLATE WHERE PAYMENT_TEMPL_GRP_OID = ?";
		resultSet = DatabaseQueryBean.getXmlResultSet(templateSQL,false,new Object[]{templateGrpOid});
		if (resultSet != null)
		{
			templateConfInd = resultSet.getAttribute("/ResultSetRecord(0)/CONFIDENTIAL_INDICATOR");
		}
		userConfInd = this.getAttribute("confidential_indicator");
		if ((TradePortalConstants.CONF_IND_FRM_TMPLT.equals(templateConfInd) &&
				TradePortalConstants.INDICATOR_NO.equals(userConfInd)))
		{
			PaymentTemplateGroup paymentTempGrp = null;
			paymentTempGrp = (PaymentTemplateGroup) this.createServerEJB("PaymentTemplateGroup", Long.parseLong(templateGrpOid));

			this.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.TEMPLATE_GROUP_CANNOT_BE_LINK,
					paymentTempGrp.getAttribute("name"));

			paymentTempGrp.remove();
		}
	} catch (Exception e) {
		LOG.error("Error during UserBean.checkTemplateGroupLink(): " , e);
	}
}
//Vshah - 10/13/2010 - IR#VIUK101259666 - [END]

      // CJR - 04/06/2011 - CR-593 Begin
	protected void userDeactivate () throws AmsException, RemoteException {
		if (isUserERPSystemUser())
        {
            this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                            TradePortalConstants.USER_REFERECED_AS_SYSTEM_USER, this.getAttribute("login_id"));
        }
     //NAR CR710 rel8.0 12/12/2011 Begin
		if (isUserUploadystemUser())
        {
            this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                            TradePortalConstants.USER_REFERECED_AS_SYSTEM_UPLOAD_USER, this.getAttribute("login_id"));
        }
     // NAR CR710 rel8.0 12/12/2011 End
	}

	protected boolean isUserERPSystemUser() throws RemoteException, AmsException{
		 StringBuffer sql = new StringBuffer();
		 sql.append("SELECT COUNT(*) AS USERCOUNT FROM CORPORATE_ORG WHERE A_H2H_SYSTEM_USER_OID = ?");
		
	

		 DocumentHandler resultSet = DatabaseQueryBean.getXmlResultSet(sql.toString(),false,new Object[]{this.getAttribute("user_oid")});
         resultSet = resultSet.getFragment("/ResultSetRecord");

         // Retrieve the total for the SQL query that was just constructed
         int count = 0;
         try {
        	 count = resultSet.getAttributeInt("/USERCOUNT");
         }
         catch(Exception e) {
            /* Ignore null amount */
         }
         return (count != 0);
	}
      // CJR - 04/06/2011 - CR-593 End
      //NAR CR710 rel8.0 12/12/2011 Begin
	protected boolean isUserUploadystemUser() throws RemoteException, AmsException{
		 StringBuffer sql = new StringBuffer();
		 sql.append("SELECT COUNT(*) AS USERCOUNT FROM CORPORATE_ORG WHERE A_UPLOAD_SYSTEM_USER_OID = ? ");
		 
	

		 DocumentHandler resultSet = DatabaseQueryBean.getXmlResultSet(sql.toString(),false,new Object[]{this.getAttribute("user_oid")});
        resultSet = resultSet.getFragment("/ResultSetRecord");

        // Retrieve the total for the SQL query that was just constructed
        int count = 0;
        try {
       	 count = resultSet.getAttributeInt("/USERCOUNT");
        }
        catch(Exception e) {
           /* Ignore null amount */
        }
        return (count != 0);
	}
   //NAR CR710 rel8.0 12/12/2011 End
  //Narayan - 06/17/2011 IR-ALUL0616770572 Begin
	private boolean isDuplicateReportCategs() throws RemoteException, AmsException {
		String firstReptCategOid = this.getAttribute("first_rept_categ");
		String secondReptCategOid = this.getAttribute("second_rept_categ");
		String thirdReptCategOid = this.getAttribute("third_rept_categ");
		String fourthReptCategOid = this.getAttribute("fourth_rept_categ");
		String fifthReptCategOid = this.getAttribute("fifth_rept_categ");
		String sixthReptCategOid = this.getAttribute("sixth_rept_categ");
		String seventhReptCategOid = this.getAttribute("seventh_rept_categ");
		String eighthReptCategOid = this.getAttribute("eighth_rept_categ");
		String ninethReptCategOid = this.getAttribute("nineth_rept_categ");
		String tenthReptCategOid = this.getAttribute("tenth_rept_categ");

		       if (StringFunction.isNotBlank(firstReptCategOid)) {
			      if ((firstReptCategOid.equals(secondReptCategOid))
				       || (firstReptCategOid.equals(thirdReptCategOid))
				       || (firstReptCategOid.equals(fourthReptCategOid))
				       || (firstReptCategOid.equals(fifthReptCategOid))
				       || (firstReptCategOid.equals(sixthReptCategOid))
				       || (firstReptCategOid.equals(seventhReptCategOid))
				       || (firstReptCategOid.equals(eighthReptCategOid))
				       || (firstReptCategOid.equals(ninethReptCategOid))
				       || (firstReptCategOid.equals(tenthReptCategOid))) {
				       return  true;
			          }
		         }
				if (StringFunction.isNotBlank(secondReptCategOid)) {
					if ((secondReptCategOid.equals(thirdReptCategOid))
						|| (secondReptCategOid.equals(fourthReptCategOid))
						|| (secondReptCategOid.equals(fifthReptCategOid))
						|| (secondReptCategOid.equals(sixthReptCategOid))
						|| (secondReptCategOid.equals(seventhReptCategOid))
						|| (secondReptCategOid.equals(eighthReptCategOid))
						|| (secondReptCategOid.equals(ninethReptCategOid))
						|| (secondReptCategOid.equals(tenthReptCategOid))) {
						return  true;
					   }
				   }

				if (StringFunction.isNotBlank(thirdReptCategOid)) {
					if ((thirdReptCategOid.equals(fourthReptCategOid))
						|| 	(thirdReptCategOid.equals(fifthReptCategOid))
						|| 	(thirdReptCategOid.equals(sixthReptCategOid))
						|| 	(thirdReptCategOid.equals(seventhReptCategOid))
						|| 	(thirdReptCategOid.equals(eighthReptCategOid))
						|| 	(thirdReptCategOid.equals(ninethReptCategOid))
						|| 	(thirdReptCategOid.equals(tenthReptCategOid))) {
						return  true;
					}
				}
				if (StringFunction.isNotBlank(fourthReptCategOid)) {
					if ((fourthReptCategOid.equals(fifthReptCategOid))
							|| 	(fourthReptCategOid.equals(sixthReptCategOid))
							|| 	(fourthReptCategOid.equals(seventhReptCategOid))
							|| 	(fourthReptCategOid.equals(eighthReptCategOid))
							|| 	(fourthReptCategOid.equals(ninethReptCategOid))
							|| 	(fourthReptCategOid.equals(tenthReptCategOid))) {

						return  true;
						}
				}
				if (StringFunction.isNotBlank(fifthReptCategOid)) {
					if ((fifthReptCategOid.equals(sixthReptCategOid))
						|| 	(fifthReptCategOid.equals(seventhReptCategOid))
						|| 	(fifthReptCategOid.equals(eighthReptCategOid))
						|| 	(fifthReptCategOid.equals(ninethReptCategOid))
						|| 	(fifthReptCategOid.equals(tenthReptCategOid))) {
						return  true;
					}
				}
				if (StringFunction.isNotBlank(sixthReptCategOid)) {
					if ((sixthReptCategOid.equals(seventhReptCategOid))
						|| 	(sixthReptCategOid.equals(eighthReptCategOid))
						|| 	(sixthReptCategOid.equals(ninethReptCategOid))
						|| 	(sixthReptCategOid.equals(tenthReptCategOid))) {
						return  true;
					}
				}
				if (StringFunction.isNotBlank(seventhReptCategOid)) {
					if ((seventhReptCategOid.equals(eighthReptCategOid))
						|| 	(seventhReptCategOid.equals(ninethReptCategOid))
						|| 	(seventhReptCategOid.equals(tenthReptCategOid))) {
						return  true;
					}
				}
				if (StringFunction.isNotBlank(eighthReptCategOid)) {
					if ((eighthReptCategOid.equals(ninethReptCategOid))
						|| 	(eighthReptCategOid.equals(tenthReptCategOid))) {
						return  true;
					}
				}
				if (StringFunction.isNotBlank(ninethReptCategOid)) {
					if (ninethReptCategOid.equals(tenthReptCategOid)) {
						return  true;
						}
				}
			return  false;
    }//Narayan - 06/17/2011 IR-ALUL0616770572 End

	//cquinton 1/23/2012 Rel 7.1 ir#cnum012364273 start
    /**
     * Limited validation for changing Registered SSO ID.
     */
    public void validateChangingRegisteredSSOId()
            throws RemoteException, AmsException {
        String whereClause;
        if ( TradePortalConstants.OWNER_GLOBAL.equals( this.getAttribute( "ownership_level" ) ) ) {
            whereClause = " and a_client_bank_oid is null";
        }
        else {
            whereClause = " and a_client_bank_oid = "+this.getAttribute("client_bank_oid");
        }

        // Check that Registered SSO ID is unique
        if (StringFunction.isNotBlank(this.getAttribute("registered_sso_id"))) {
            if (!isUnique("registered_sso_id", this.getAttribute("registered_sso_id"), whereClause)) {
                this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                     TradePortalConstants.ALREADY_EXISTS,
                     this.getAttribute("registered_sso_id"),
                     attributeMgr.getAlias("registered_sso_id"));
            }
        }
    }
    //cquinton 1/23/2012 Rel 7.1 ir#cnum012364273 end
    
    //cquinton 2/15/2012 Rel 8.0 ppx255 start
    //refactor password expiration logic here
    /**
     * This method checks to see if the user's password has expired.
     *
     * @return true if password is expired, false if it is valid.
     */
    public boolean isPasswordExpired()
            throws AmsException, RemoteException {
        boolean isExpired;

        if(this.getAttribute("password_change_date").equals("")) {
            // If password change date is null, consider it expired
            isExpired = true;
        }
        else {
            // Determine password expiration period
            int passwordExpirationPeriodDays =
                Integer.parseInt((String)this.getSecurityLimits().get("passwordExpirationDays"));

            // Converts the expiration period (passed in as days)
            // into milliseconds
            // (number of days) * 24 hours in a day * 60 minutes in an hour *
            //     60 seconds in a minute * 1000 milliseconds in a second
            long numMillisecondsInDay = 86400000;
            long expirationPeriod = passwordExpirationPeriodDays * numMillisecondsInDay;

            // Last password change date is stored as GMT (converted to milliseconds since 1/1/1970)
            long lastPasswordChangeDate = this.getAttributeDateTime("password_change_date")!=null?this.getAttributeDateTime("password_change_date").getTime():0;

            // Get current date time in GMT (converted to milliseconds since 1/1/1970)
            long todaysDateInGMT = GMTUtility.getGMTDateTime().getTime();


            // Determine if the password is expired
            if( (lastPasswordChangeDate + expirationPeriod) < todaysDateInGMT ) {
                isExpired = true;
            }
            else {
                isExpired = false;
            }
        }

        return isExpired;
    }
    //cquinton 2/15/2012 Rel 8.0 ppx255 end
    
    /**
     * This method will check Organization under admin user detail is not one of the Resticted Bank Groups of refdata.
     * If so it raised validation error.   
     * @param bankGrpRestrictRuleOid 
     * @throws AmsException
     * @throws RemoteException
     */
    private void validateBankGroupRestrictRule(String bankGrpRestrictRuleOid) throws AmsException, RemoteException{
    	String bnkGrpOid=null;
    	BusinessObject busobj = null;
    	BankGrpRestrictRule bnkGrpRestrictRl;
    	Organization org;
		String ownershipLevel = this.getAttribute("ownership_level");

		if (ownershipLevel.equals(TradePortalConstants.OWNER_GLOBAL))
		{
			org = (Organization)this.createServerEJB("GlobalOrganization",
                                               this.getAttributeLong("owner_org_oid"));
   		}
   		else if (ownershipLevel.equals(TradePortalConstants.OWNER_CORPORATE))
   		{
      		org = (Organization)this.createServerEJB("CorporateOrganization",
                                               this.getAttributeLong("owner_org_oid"));
   		}
   		else if (ownershipLevel.equals(TradePortalConstants.OWNER_BOG)) {
   			org = (Organization)this.createServerEJB("BankOrganizationGroup",
                    this.getAttributeLong("owner_org_oid"));
   		}
   		else
   		{
      		org = (Organization)this.createServerEJB("ClientBank",
                                              this.getAttributeLong("client_bank_oid"));
   		}
    	
    	bnkGrpRestrictRl = (BankGrpRestrictRule)this.createServerEJB("BankGrpRestrictRule",Long.parseLong(bankGrpRestrictRuleOid));
    	ComponentList bnkGrpList = (ComponentList)bnkGrpRestrictRl.getComponentHandle("BankGrpForRestrictRuleList");
    	int count = bnkGrpList.getObjectCount();
    	Set bnkGrpset = new HashSet();

    	for (int i = 0; i < count; i++) {
    		bnkGrpList.scrollToObjectByIndex(i);
    		busobj = bnkGrpList.getBusinessObject();
    		bnkGrpOid = busobj.getAttribute("bank_organization_group_oid");
    		bnkGrpset.add(bnkGrpOid);
    	}
    	
    	if(bnkGrpset.contains(org.getAttribute("organization_oid"))){
    		this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                    TradePortalConstants.USER_ORG_IS_RESTRICTED,
                    org.getAttribute("name"));
    		
    	}
    	
    }
    // Nar CR-1071 Rel9400 08/28/2015 Add- Begin
    /**
     * This method is used to check the authentication method of user. if owner org is setup for PerUser(individual Configuration), then user can be setup
     * for any method (2FA, SSO, Certificate or PASSWORD). but if it is different, thow the warning messag to user.
     * @param orgAuthenticationMethod
     * @param userAuthenticationMethod
     * @throws AmsException 
     * @throws RemoteException 
     */
    private void validateUserAuthWithBankAuthMethod(String orgAuthenticationMethod, String userAuthenticationMethod) throws RemoteException, AmsException {
      // if Owner org is set for individual authnetication configuataion, then admin user can be set up for any auth method and don't throw error.
      if ( StringFunction.isNotBlank(orgAuthenticationMethod)  && !TradePortalConstants.AUTH_PERUSER.equals(orgAuthenticationMethod)) {
    	  // Admin user Auth method should be same as owner org auth method (2FA, SSO, PASSWORD, Certificate)
    	  if( StringFunction.isNotBlank(userAuthenticationMethod) && ! orgAuthenticationMethod.equals(userAuthenticationMethod) ) {
    		 this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.ADMIN_AUTH_METHOD_NOT_MATCH);
    	  }
      }
    	
    }
   // Nar CR-1071 Rel9400 08/28/2015 Add- End
  //Srinivasu_D IR#T36000048154 Rel9.5 04/22/2015 START - Email validation
    /**
     * THis method performs email validations for a given text.
     * @param emailId
     * @return boolean
     * @throws RemoteException
     * @throws AmsException
     */
    protected boolean performEmailValidation(String emailId) throws RemoteException, AmsException {
		if (StringFunction.isBlank(emailId)) return false;
    
	    if (!EmailValidator.validateEmailList(emailId))	{
	    	return true;
	    }
		return false;
	}
  //Srinivasu_D IR#T36000048154 Rel9.5 04/22/2015 START - END
}