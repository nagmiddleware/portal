
  
/*
 * This file is generated from the model.  Normally it should not be modified manually.  
 * Contact the modeler first.  (In a pinch, it can be modified first and then retrofitted 
 * to the model.)
 *
 */
package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;


/*
 * Details for the various actions performed on the Cash Management transactions
 * by a user
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class TransactionHistoryBean_Base extends TradePortalBusinessObjectBean
{
private static final Logger LOG = LoggerFactory.getLogger(TransactionHistoryBean_Base.class);

  
  /* 
   * Register the attributes and associations of the business object
   */
   protected void registerAttributes() throws AmsException
   {  

      /* Register attributes defined in the Ancestor class */
      super.registerAttributes();
      
      /* transaction_history_oid - Unique Identifier */
      attributeMgr.registerAttribute("transaction_history_oid", "transaction_history_oid", "ObjectIDAttribute");
      
      /* action_datetime - Timestamp (in GMT) when the action is taken. */
      attributeMgr.registerAttribute("action_datetime", "action_datetime", "DateTimeAttribute");
      
      /* action - Action. */
      attributeMgr.registerReferenceAttribute("action", "action", "TRANSACTION_ACTION_TYPE");
      
      /* panel_authority_code - The Panel Level at which the authorization action is taken. */
      attributeMgr.registerReferenceAttribute("panel_authority_code", "panel_authority_code", "PANEL_AUTH_TYPE");
      
      /* transaction_status - Lifecycle status of a transactioin */
      attributeMgr.registerReferenceAttribute("transaction_status", "transaction_status", "TRANSACTION_STATUS");
      
        /* Pointer to the parent Transaction */
      attributeMgr.registerAttribute("transaction_oid", "p_transaction_oid", "ParentIDAttribute");
   
      /* user_oid - The user who the updates the transaction. */
      attributeMgr.registerAssociation("user_oid", "a_user_oid", "User");
      
      /* repair_reason - repair reason to sedn transaction for repair. */
      attributeMgr.registerAttribute("repair_reason", "repair_reason");
      
   }
   
 
   
 
 
   
}
