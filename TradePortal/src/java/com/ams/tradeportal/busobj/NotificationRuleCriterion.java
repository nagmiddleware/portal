

  

package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;


/**
 * Allows Notification Rules to be defined differently for particular transaction
 * types and separates the decision criteria to send an email or a notification
 * message for each transaction type.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface NotificationRuleCriterion extends TradePortalBusinessObject
{   
}
