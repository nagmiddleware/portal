
package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;
/**
 * Details for the various actions performed on the Invoices
 * by a user.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface InvoiceHistoryHome extends EJBHome
{
   public InvoiceHistory create()
      throws RemoteException, CreateException, AmsException;

   public InvoiceHistory create(ClientServerDataBridge csdb)
      throws RemoteException, CreateException, AmsException;
}