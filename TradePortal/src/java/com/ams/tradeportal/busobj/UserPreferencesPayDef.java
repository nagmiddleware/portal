

  

package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;


/**
 * A UserPreferencesPayDef consists of a set of UserPreferencesPayDef
 * to specifiy the requirements of Payment Defintions by User preferences.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface UserPreferencesPayDef extends TradePortalBusinessObject
{   
}
