

  

package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;


/**
 * A Bank Organization Group (BOG) represents a grouping of corporate customers
 * underneath a Client Bank.    Each BOG has its own branding directory and
 * can own reference data.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface BankOrganizationGroup extends ReferenceDataOwner
{   
}
