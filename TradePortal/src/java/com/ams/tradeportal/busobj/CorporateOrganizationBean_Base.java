

/*
 * This file is generated from the model.  Normally it should not be modified manually.
 * Contact the modeler first.  (In a pinch, it can be modified first and then retrofitted
 * to the model.)
 *
 */
package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;




/*
 * This business object represents a business organization that is a customer
 * of the client banks.   Corporate organizations apply for instruments and,
 * by authorizing them, send them to the bank.
 *
 *     Copyright  � 2003
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class CorporateOrganizationBean_Base extends ReferenceDataOwnerBean
{
private static final Logger LOG = LoggerFactory.getLogger(CorporateOrganizationBean_Base.class);


  /*
   * Register the attributes and associations of the business object
   */
   protected void registerAttributes() throws AmsException
   {

      /* Register attributes defined in the Ancestor class */
      super.registerAttributes();

      //cquinton 11/5/2012 - add alias for name
      attributeMgr.registerAlias("name", getResourceManager().getText("CorporateOrganizationBeanAlias.name", TradePortalConstants.TEXT_BUNDLE));

      /* address_city - City where the corporate organization is located. */
      attributeMgr.registerAttribute("address_city", "address_city");
      attributeMgr.requiredAttribute("address_city");
      attributeMgr.registerAlias("address_city", getResourceManager().getText("CorporateOrganizationBeanAlias.address_city", TradePortalConstants.TEXT_BUNDLE));

      /* address_country - Country where the corporate organization is located. */
      attributeMgr.registerReferenceAttribute("address_country", "address_country", "COUNTRY");
      attributeMgr.requiredAttribute("address_country");
      attributeMgr.registerAlias("address_country", getResourceManager().getText("CorporateOrganizationBeanAlias.address_country", TradePortalConstants.TEXT_BUNDLE));

      /* address_line_1 - Address line 1 for this organization */
      attributeMgr.registerAttribute("address_line_1", "address_line_1");
      attributeMgr.requiredAttribute("address_line_1");
      attributeMgr.registerAlias("address_line_1", getResourceManager().getText("CorporateOrganizationBeanAlias.address_line_1", TradePortalConstants.TEXT_BUNDLE));

      /* address_line_2 - Address line 2 for this organization */
      attributeMgr.registerAttribute("address_line_2", "address_line_2");

      /* address_postal_code - Postal code  where the corporate organization is located. */
      attributeMgr.registerAttribute("address_postal_code", "address_postal_code");

      /* address_state_province - State or province   where the corporate organization is located. */
      attributeMgr.registerAttribute("address_state_province", "address_state_province");

      /* allow_airway_bill - Indicates whether or not this corporate organization can create Air Waybill
         transactions.

         Yes=Corporate organization can create */
      attributeMgr.registerAttribute("allow_airway_bill", "allow_airway_bill", "IndicatorAttribute");

      /* allow_auto_lc_create - Indicates whether or not this corporate organization has access to the Auto
         LC Create (by uploading Purchase Orders) functionality.

         yes=this organization has access */
      attributeMgr.registerAttribute("allow_auto_lc_create", "allow_auto_lc_create", "IndicatorAttribute");

      /* allow_create_bank_parties - Indicates whether or not this corporate organization can create reference
         data parties that have a type of Bank.   If this is set to No, they can
         only create reference data parties with a type of Corporate */
      attributeMgr.registerAttribute("allow_create_bank_parties", "allow_create_bank_parties", "IndicatorAttribute");

      /* allow_export_collection - Indicates whether or not this corporate organization can create Direct Send
         Collection transactions.  (For a proper Export Collection from TPS perspective,
         see allow_new_export_collection.)

         Yes=Corporate organization can create */
      attributeMgr.registerAttribute("allow_export_collection", "allow_export_collection", "IndicatorAttribute");

      /* allow_export_LC - Indicates whether or not this corporate organization can create Export LC
         transactions.

         Yes=Corporate organization can create */
      attributeMgr.registerAttribute("allow_export_LC", "allow_export_LC", "IndicatorAttribute");

      /* allow_funds_transfer - Indicates whether or not this corporate organization can create Funds Transfer
         transactions using user thresholds.

         Yes=Corporate organization can create */
      attributeMgr.registerAttribute("allow_funds_transfer", "allow_funds_transfer", "IndicatorAttribute");

      /* allow_guarantee - Indicates whether or not this corporate organization can create Guarantee
         transactions.

         Yes=Corporate organization can create */
      attributeMgr.registerAttribute("allow_guarantee", "allow_guarantee", "IndicatorAttribute");

      /* allow_import_DLC - Indicates whether or not this corporate organization can create Import LC
         transactions.

         Yes=Corporate organization can create */
      attributeMgr.registerAttribute("allow_import_DLC", "allow_import_DLC", "IndicatorAttribute");

      /* allow_loan_request - Indicates whether or not this corporate organization can create Loan Request
         transactions.

         Yes=Corporate organization can create */
      attributeMgr.registerAttribute("allow_loan_request", "allow_loan_request", "IndicatorAttribute");

      /* allow_manual_po_entry - Indicates whether or not this corporate organization has the ability to
         manually enter POs.

         yes=this organization has access */
      attributeMgr.registerAttribute("allow_manual_po_entry", "allow_manual_po_entry", "IndicatorAttribute");

      /* allow_request_advise - Indicates whether or not this corporate organization can create Request
         to Advise transactions.

         Yes=Corporate organization can create */
      attributeMgr.registerAttribute("allow_request_advise", "allow_request_advise", "IndicatorAttribute");
      
      //Ravindra - CR-708B - Start
      /* allow_supplier_portal - Indicates whether or not this corporate organization can create Supplier Portal transactions.

	      Yes=Corporate organization can create */
	   attributeMgr.registerAttribute("allow_supplier_portal", "allow_supplier_portal", "IndicatorAttribute");
	 //Ravindra - CR-708B - End

      /* allow_shipping_guar - Indicates whether or not this corporate organization can create Shipping
         Guarantee transactions.

         Yes=Corporate organization can create */
      attributeMgr.registerAttribute("allow_shipping_guar", "allow_shipping_guar", "IndicatorAttribute");

      /* allow_SLC - Indicates whether or not this corporate organization can create Standby
         LC transactions.

         Yes=Corporate organization can create */
      attributeMgr.registerAttribute("allow_SLC", "allow_SLC", "IndicatorAttribute");

      /* auth_user_different_ind - A setting indicating whether or not the authorizing user must be different
         than the last entry user.   This is enforced when all transactions are authorized
         by this corporate customer's users.

         Yes=Authorizing user must be different from last entry user. */
      attributeMgr.registerAttribute("auth_user_different_ind", "auth_user_different_ind", "IndicatorAttribute");

      /* authentication_method - Determines how users of the corporate organization will authenticate themselves. */
      attributeMgr.registerReferenceAttribute("authentication_method", "authentication_method", "AUTHENTICATION_METHOD");
      attributeMgr.requiredAttribute("authentication_method");
      attributeMgr.registerAlias("authentication_method", getResourceManager().getText("CorporateOrganizationBeanAlias.authentication_method", TradePortalConstants.TEXT_BUNDLE));

      /* base_currency_code - The currency is which this corporate organization typically conducts business */
      attributeMgr.registerReferenceAttribute("base_currency_code", "base_currency_code", "CURRENCY_CODE");
      attributeMgr.requiredAttribute("base_currency_code");
      attributeMgr.registerAlias("base_currency_code", getResourceManager().getText("CorporateOrganizationBeanAlias.base_currency_code", TradePortalConstants.TEXT_BUNDLE));

      /* corporate_org_type_code - Indicates whether this corporate organization is a bank or a non-bank corporation.

BANK/CORP=Bank/Corporate */
      attributeMgr.registerReferenceAttribute("corporate_org_type_code", "corporate_org_type_code", "CORP_ORG_TYPE");
      attributeMgr.requiredAttribute("corporate_org_type_code");
      attributeMgr.registerAlias("corporate_org_type_code", getResourceManager().getText("CorporateOrganizationBeanAlias.corporate_org_type_code", TradePortalConstants.TEXT_BUNDLE));

      /* creation_date - Timestamp (in GMT) when the corporate organization was created. */
      attributeMgr.registerAttribute("creation_date", "creation_date", "DateTimeAttribute");

      /* dnld_adv_terms_format - Determines the format in which the user can download the Advice terms. */
      attributeMgr.registerReferenceAttribute("dnld_adv_terms_format", "dnld_adv_terms_format", "DNLD_ADV_TERMS_FORMAT");
      attributeMgr.requiredAttribute("dnld_adv_terms_format");
      attributeMgr.registerAlias("dnld_adv_terms_format", getResourceManager().getText("CorporateOrganizationBeanAlias.dnld_adv_terms_format", TradePortalConstants.TEXT_BUNDLE));

      /* doc_prep_accountname - The company account name for this corporate organization in the document
         prep system.  This information is sent to the doc prep system during single
         sign-on. */
      attributeMgr.registerAttribute("doc_prep_accountname", "doc_prep_accountname");
      attributeMgr.registerAlias("doc_prep_accountname", getResourceManager().getText("CorporateOrganizationBeanAlias.doc_prep_accountname", TradePortalConstants.TEXT_BUNDLE));

      /* doc_prep_domainname - The domain of the document prep system that the user of this corporation
         uses by default.  This is unique to the TradeBeam system we are using now.
         This information is sent to TradeBeam during single sign-on. */
      attributeMgr.registerAttribute("doc_prep_domainname", "doc_prep_domainname");
      attributeMgr.registerAlias("doc_prep_domainname", getResourceManager().getText("CorporateOrganizationBeanAlias.doc_prep_domainname", TradePortalConstants.TEXT_BUNDLE));

      /* doc_prep_ind - Indicates whether or not users of the corporate customer will have access
         to the document preparation software (i.e. corporate service) that is integrated
         into the Trade Portal. */
      attributeMgr.registerAttribute("doc_prep_ind", "doc_prep_ind", "IndicatorAttribute");

      /* dual_auth_airway_bill - Indicates whether or not dual authorization is required for  Air Waybill
         transactions.

         N or null=Can be authorized by one user
         Y=Must be authorized by two users
         G=Must be authorized by two users that belong to different work groups */
      attributeMgr.registerReferenceAttribute("dual_auth_airway_bill", "dual_auth_airway_bill", "DUAL_AUTH_REQ");

      /* dual_auth_disc_response - Indicates whether or not dual authorization is required for  Discrepancy
         Response transactions.

         N or null=Can be authorized by one user
         Y=Must be authorized by two users
         G=Must be authorized by two users that belong to different work groups
         I=Use Instrument Type Authorization Defaults */
      attributeMgr.registerReferenceAttribute("dual_auth_disc_response", "dual_auth_disc_response", "DUAL_AUTH_REQ");

      /* dual_auth_export_coll - Indicates whether or not dual authorization is required for  Direct Send
         Collection transactions.  (For proper Export Collection from TPS perspective,
         see dual_auth_new_export_coll)

         N or null=Can be authorized by one user
         Y=Must be authorized by two users
         G=Must be authorized by two users that belong to different work groups */
      attributeMgr.registerReferenceAttribute("dual_auth_export_coll", "dual_auth_export_coll", "DUAL_AUTH_REQ");

      /* dual_auth_export_LC - Indicates whether or not dual authorization is required for  Export LC transactions.

         N or null=Can be authorized by one user
         Y=Must be authorized by two users
         G=Must be authorized by two users that belong to different work groups */
      attributeMgr.registerReferenceAttribute("dual_auth_export_LC", "dual_auth_export_LC", "DUAL_AUTH_REQ");

      /* dual_auth_funds_transfer - Indicates whether or not dual authorization is required for  Funds Transfers
         transactions.

         N or null=Can be authorized by one user
         Y=Must be authorized by two users
         G=Must be authorized by two users that belong to different work groups
         P=Must be authorized using Panel Authorization */
      attributeMgr.registerReferenceAttribute("dual_auth_funds_transfer", "dual_auth_funds_transfer", "DUAL_AUTH_REQ");

      /* dual_auth_guarantee - Indicates whether or not dual authorization is required for  Guarantee transactions.

         N or null=Can be authorized by one user
         Y=Must be authorized by two users
         G=Must be authorized by two users that belong to different work groups */
      attributeMgr.registerReferenceAttribute("dual_auth_guarantee", "dual_auth_guarantee", "DUAL_AUTH_REQ");

      /* dual_auth_import_DLC - Indicates whether or not dual authorization is required for  Import LC transactions.

         N or null=Can be authorized by one user
         Y=Must be authorized by two users
         G=Must be authorized by two users that belong to different work groups */
      attributeMgr.registerReferenceAttribute("dual_auth_import_DLC", "dual_auth_import_DLC", "DUAL_AUTH_REQ");

      /* dual_auth_loan_request - Indicates whether or not dual authorization is required for  Loan Request
         transactions.

         N or null=Can be authorized by one user
         Y=Must be authorized by two users
         G=Must be authorized by two users that belong to different work groups */
      attributeMgr.registerReferenceAttribute("dual_auth_loan_request", "dual_auth_loan_request", "DUAL_AUTH_REQ");

      /* dual_auth_request_advise - Indicates whether or not dual authorization is required for  Request to
         Advise transactions

         N or null=Can be authorized by one user
         Y=Must be authorized by two users
         G=Must be authorized by two users that belong to different work groups */
      attributeMgr.registerReferenceAttribute("dual_auth_request_advise", "dual_auth_request_advise", "DUAL_AUTH_REQ");
      //Ravindra - CR-708B - Start
      /* dual_auth_supplier_portal - Indicates whether or not dual authorization is required for Supplier Portal transactions

      N or null=Can be authorized by one user
      Y=Must be authorized by two users
      G=Must be authorized by two users that belong to different work groups */
	   attributeMgr.registerReferenceAttribute("dual_auth_supplier_portal", "dual_auth_supplier_portal", "DUAL_AUTH_REQ");
	 //Ravindra - CR-708B - End

      /* dual_auth_shipping_guar - Indicates whether or not dual authorization is required for  Shipping Guarantee
         transactions.

         N or null=Can be authorized by one user
         Y=Must be authorized by two users
         G=Must be authorized by two users that belong to different work groups */
      attributeMgr.registerReferenceAttribute("dual_auth_shipping_guar", "dual_auth_shipping_guar", "DUAL_AUTH_REQ");

      /* dual_auth_SLC - Indicates whether or not dual authorization is required for  Standby transactions.

         N or null=Can be authorized by one user
         Y=Must be authorized by two users
         G=Must be authorized by two users that belong to different work groups */
      attributeMgr.registerReferenceAttribute("dual_auth_SLC", "dual_auth_SLC", "DUAL_AUTH_REQ");

      /* email_language - Determines which language email notifications should be in. */
      attributeMgr.registerReferenceAttribute("email_language", "email_language", "INSTRUMENT_LANGUAGE");
      attributeMgr.requiredAttribute("email_language");
      attributeMgr.registerAlias("email_language", getResourceManager().getText("CorporateOrganizationBeanAlias.email_language", TradePortalConstants.TEXT_BUNDLE));

      /* email_receiver - Determines which user(s) will receive an email when a new notification come
         into the Portal */
      attributeMgr.registerAttribute("email_receiver", "email_receiver");

      /* force_certs_for_user_maint - Whether the users of this corporate customer who can maintain users must
         use certificate authentication.  This attribute is applicable only when
         the corporate customer's authentication method is configured for each user
         individually. */
      attributeMgr.registerAttribute("force_certs_for_user_maint", "force_certs_for_user_maint", "IndicatorAttribute");

      /* last_email_date - Date (in GMT) when the last email reminder was sent. */
      attributeMgr.registerAttribute("last_email_date", "last_email_date", "DateAttribute");

      /* Proponix_customer_id - The unique identifier of this corporate organization on OTL, the back end
         system.   This is not used by the Trade Portal, but is used by OTL to match
         up corporate organizations on the portal with those stored in OTL. */
      attributeMgr.registerAttribute("Proponix_customer_id", "Proponix_customer_id");
      attributeMgr.requiredAttribute("Proponix_customer_id");
      attributeMgr.registerAlias("Proponix_customer_id", getResourceManager().getText("CorporateOrganizationBeanAlias.Proponix_customer_id", TradePortalConstants.TEXT_BUNDLE));

      /* route_within_hierarchy - Determine how the user is able to route within their hierarchy of organizations. */
      attributeMgr.registerReferenceAttribute("route_within_hierarchy", "route_within_hierarchy", "ROUTE_WITHIN_HIERARCHY");
      attributeMgr.requiredAttribute("route_within_hierarchy");
      attributeMgr.registerAlias("route_within_hierarchy", getResourceManager().getText("CorporateOrganizationBeanAlias.route_within_hierarchy", TradePortalConstants.TEXT_BUNDLE));

      /* standbys_use_either_form - Indicates whether or not the users of this corporate organization will be
         given an option of which form to use when creating a Standby LC.   If this
         is set to Yes, the users will be given the option between the Standby Short
         Form (real Standby LC form) and the Standby Long Form (real Guarantee form). */
      attributeMgr.registerAttribute("standbys_use_either_form", "standbys_use_either_form", "IndicatorAttribute");

      /* standbys_use_guar_form_only - Indicates that when this corporate organization creates a "Standby LC",
         it will use the same form as a "Guarantee".   The users of this corporate
         org will not have the option to choose between the original standby form
         and the guarantee form.

         Yes=organizations must use the guarantee form for all standbys. */
      attributeMgr.registerAttribute("standbys_use_guar_form_only", "standbys_use_guar_form_only", "IndicatorAttribute");

      /* timeout_auto_save_ind - Whether the data that users have entered on web page would be automatically
         saved before time-outs. */
      attributeMgr.registerAttribute("timeout_auto_save_ind", "timeout_auto_save_ind", "IndicatorAttribute");

      /* allow_atp_manual_po_entry - Indicates whether or not this corporate organization has the ability to
         manually enter POs on Approval To Pay transactions.

         yes=this organization has access */
      attributeMgr.registerAttribute("allow_atp_manual_po_entry", "allow_atp_manual_po_entry", "IndicatorAttribute");

      /* allow_approval_to_pay - Indicates whether or not this corporate organization can create Approval
         To Pay transactions.

         Yes=Corporate organization can create */
      attributeMgr.registerAttribute("allow_approval_to_pay", "allow_approval_to_pay", "IndicatorAttribute");

      /* dual_auth_approval_to_pay - Indicates whether or not dual authorization is required for  Approval To
         Pay transactions.

         N or null=Can be authorized by one user
         Y=Must be authorized by two users
         G=Must be authorized by two users that belong to different work groups */
      attributeMgr.registerReferenceAttribute("dual_auth_approval_to_pay", "dual_auth_approval_to_pay", "DUAL_AUTH_REQ");

      /* allow_auto_atp_create - Indicates whether or not this corporate organization has access to the Auto
         "Approval to Pay" Create (by uploading Purchase Orders) functionality.

         yes=this organization has access */
      attributeMgr.registerAttribute("allow_auto_atp_create", "allow_auto_atp_create", "IndicatorAttribute");

      /* allow_arm - Indicates whether or not this corporate organization can create Account
         Receivable Management  transactions.

         Yes=Corporate organization can create */
      attributeMgr.registerAttribute("allow_arm", "allow_arm", "IndicatorAttribute");

      /* dual_auth_arm - Indicates whether or not dual authorization is required for  Approval To
         Pay transactions.

         N or null=Can be authorized by one user
         Y=Must be authorized by two users
         G=Must be authorized by two users that belong to different work groups */
      attributeMgr.registerReferenceAttribute("dual_auth_arm", "dual_auth_arm", "DUAL_AUTH_REQ");
      //Leelavathi CR-710 Rel 8.0.0 24/10/2011 Begin
      /* allow_arm_invoice - Indicates whether or not this corporate organization can create Receivable Management invoices.
      Yes=Corporate organization can create */
      attributeMgr.registerAttribute("allow_arm_invoice", "allow_arm_invoice", "IndicatorAttribute");

      /* dual_auth_arm_invoice - Indicates whether or not dual authorization is required for Receivable Management Invoices.
      	N or null=Can be authorized by one user
      	Y=Must be authorized by two users
      	G=Must be authorized by two users that belong to different work groups */
      attributeMgr.registerReferenceAttribute("dual_auth_arm_invoice", "dual_auth_arm_invoice", "DUAL_AUTH_REQ");
    //Leelavathi CR-710 Rel 8.0.0 24/10/2011 End

    //Leelavathi CR-710 Rel 8.0.0 23/11/2011 Begin
      /* allow_credit_note - Indicates whether or not this corporate organization can perform Credit Note Authorization.
      Yes=Corporate organization can create */
      attributeMgr.registerAttribute("allow_credit_note", "allow_credit_note", "IndicatorAttribute");

      /* dual_auth_credit_note - Indicates whether or not dual authorization is required for Credit Note.
      N or null=Can be authorized by one user
      Y=Must be authorized by two users
      G=Must be authorized by two users that belong to different work groups */
      attributeMgr.registerReferenceAttribute("dual_auth_credit_note", "dual_auth_credit_note", "DUAL_AUTH_REQ");

      /* allow_grouped_invoice_upload - Indicates whether or not this corporate organization can Upload grouped invoices. */
      attributeMgr.registerAttribute("allow_grouped_invoice_upload", "allow_grouped_invoice_upload", "IndicatorAttribute");

      /* allow_restricted_invoice_upload - Indicates whether or not this corporate organization can Restrict Invoice Upload Directory */
      attributeMgr.registerAttribute("allow_restricted_inv_upload", "allow_restricted_inv_upload", "IndicatorAttribute");

      /* restricted_inv_upload_directory - Indicates directory / drive to which invoice uploads should be restricted */
      attributeMgr.registerAttribute("restricted_inv_upload_dir", "restricted_inv_upload_dir");

    //Leelavathi CR-710 Rel 8.0.0 23/11/2011 End
      
      
    //jgadela CR ANZ 501 Rel 8.3 03/28/2013 START 
      /*corp_user_dual_ctrl_reqd_ind - Indicates whether Corporate Organization user reference data require dual   control for maintenance. */
      attributeMgr.registerAttribute("corp_user_dual_ctrl_reqd_ind", "corp_user_dual_ctrl_reqd_ind", "IndicatorAttribute");
      
      /*panel_authorisation_dual_ctrl_reqd_ind - Indicates whether Corporate Organization user reference data require dual   control for maintenance. */
      attributeMgr.registerAttribute("panel_auth_dual_ctrl_reqd_ind", "panel_auth_dual_ctrl_reqd_ind", "IndicatorAttribute");
    //jgadela CR ANZ 501 Rel 8.3 03/28/2013 END
      
      
      //Leelavathi CR-710 Rel 8.0.0 28/11/2011 Begin
      attributeMgr.registerReferenceAttribute("interest_discount_ind", "interest_discount_ind", "INTEREST_DISCOUNT_TYPE");
      attributeMgr.registerReferenceAttribute("calculate_discount_ind", "calculate_discount_ind", "CALCULATE_DISCOUNT_USING");
    //Leelavathi CR-710 Rel 8.0.0 28/11/2011 End
    //Leelavathi CR-707 Rel 8.0.0 02/02/2012 Begin
      /* allow_purchase_order_upload - Indicates whether purchase order upload is available to this corporate organization.

      Yes=Purchase order upload is available. */
      attributeMgr.registerAttribute("allow_purchase_order_upload", "allow_purchase_order_upload", "IndicatorAttribute");

   /* po_upload_format - Determine whether the Customer will process Purchase Order Uploads files
      in the new structured format or whether they will be processed in the existing
      format which will continue to be stored as text in the Import DLC or Approval
      to Payment application forms.

      S=Structured format
      T=Existing Text format
*/
   attributeMgr.registerReferenceAttribute("po_upload_format", "po_upload_format", "PO_UPLOAD_FORMAT");

   /* allow_restricted_po_upload - Indicates whether PO upload should be restricted to a specific folder */
   attributeMgr.registerAttribute("allow_restricted_po_upload", "allow_restricted_po_upload", "IndicatorAttribute");

   /* restricted_po_upload_dir - Folder/directory where corporate customer are allowd to upload PO file from. */
   attributeMgr.registerAttribute("restricted_po_upload_dir", "restricted_po_upload_dir");

   //Leelavathi CR-707 Rel 8.0.0 02/02/2012 End

      /* enforce_daily_limit_transfer - When the indicator is set, daily limits should be enforced on Transfers
         between Accounts. */
      attributeMgr.registerAttribute("enforce_daily_limit_transfer", "enforce_daily_limit_transfer", "IndicatorAttribute");

      /* transfer_daily_limit_amt - When enforce_daily_limit_transfer is set, this amount provides the daily
         limit for transferring between accounts. */
      attributeMgr.registerAttribute("transfer_daily_limit_amt", "transfer_daily_limit_amt", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      attributeMgr.registerAlias("transfer_daily_limit_amt", getResourceManager().getText("CorporateOrganizationBeanAlias.transfer_daily_limit_amt", TradePortalConstants.TEXT_BUNDLE));

      /* enforce_daily_limit_dmstc_pymt - When the indicator is set, daily limits should be enforced on Domestic Payments. */
      attributeMgr.registerAttribute("enforce_daily_limit_dmstc_pymt", "enforce_daily_limit_dmstc_pymt", "IndicatorAttribute");

      /* domestic_pymt_daily_limit_amt - When enforce_daily_limit_domestic_pymt is set, this amount provides the
         daily limit for domestic payments. */
      attributeMgr.registerAttribute("domestic_pymt_daily_limit_amt", "domestic_pymt_daily_limit_amt", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      attributeMgr.registerAlias("domestic_pymt_daily_limit_amt", getResourceManager().getText("CorporateOrganizationBeanAlias.domestic_pymt_daily_limit_amt", TradePortalConstants.TEXT_BUNDLE));

      /* enforce_daily_limit_intl_pymt - When the indicator is set, daily limits should be enforced on International
         Payments. */
      attributeMgr.registerAttribute("enforce_daily_limit_intl_pymt", "enforce_daily_limit_intl_pymt", "IndicatorAttribute");

      /* intl_pymt_daily_limit_amt - When enforce_daily_limit_international_pymt is set, this amount provides
         the daily limit for domestic payments. */
      attributeMgr.registerAttribute("intl_pymt_daily_limit_amt", "intl_pymt_daily_limit_amt", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      attributeMgr.registerAlias("intl_pymt_daily_limit_amt", getResourceManager().getText("CorporateOrganizationBeanAlias.intl_pymt_daily_limit_amt", TradePortalConstants.TEXT_BUNDLE));

      /* timezone_for_daily_limit - The timezone for daily limit  indicates what day it is for the Corporate
         Customer and will be used for zeroing out the Daily balance running totals. */
      attributeMgr.registerReferenceAttribute("timezone_for_daily_limit", "timezone_for_daily_limit", "TIMEZONE");
      attributeMgr.registerAlias("timezone_for_daily_limit", getResourceManager().getText("CorporateOrganizationBeanAlias.timezone_for_daily_limit", TradePortalConstants.TEXT_BUNDLE));

      /* allow_xfer_btwn_accts - Indicates whether or not this corporate organization can transfer between
         accounts using user thresholds.

         Yes=Corporate organization can create */
      attributeMgr.registerAttribute("allow_xfer_btwn_accts", "allow_xfer_btwn_accts", "IndicatorAttribute");

      /* dual_xfer_btwn_accts - Indicates whether or not dual authorization is required for  Approval To
         Pay transactions.

         N or null=Can be authorized by one user
         Y=Must be authorized by two users
         G=Must be authorized by two users that belong to different work groups
         P=Must be authorized using Panel Authorization */
      attributeMgr.registerReferenceAttribute("dual_xfer_btwn_accts", "dual_xfer_btwn_accts", "DUAL_AUTH_REQ");

      /* allow_domestic_payments - Indicates whether or not this corporate organization can make domestic payments
         using user thresholds.

         Yes=Corporate organization can create */
      attributeMgr.registerAttribute("allow_domestic_payments", "allow_domestic_payments", "IndicatorAttribute");

      /* dual_domestic_payments - Indicates whether or not dual authorization is required for  Domestic Payment
         transactions.

         N or null=Can be authorized by one user
         Y=Must be authorized by two users
         G=Must be authorized by two users that belong to different work groups
         P=Must be authorized using Panel Authorization
 */
      attributeMgr.registerReferenceAttribute("dual_domestic_payments", "dual_domestic_payments", "DUAL_AUTH_REQ");

      /* allow_pymt_instrument_auth - Indicates whether Payment Instrument Capabilities and Authorization Settings
         are enabled for this profile.

         Yes=Payment Instrument authorization is enabled */
      attributeMgr.registerAttribute("allow_pymt_instrument_auth", "allow_pymt_instrument_auth", "IndicatorAttribute");

      /* allow_panel_auth_for_pymts - Indicates whether to allow Authorization with Panel Levels for Payment Instruments.

         Yes=Authorization with Panel Levels is enabled
         No=Authorization with User Thresholds is enabled */
      attributeMgr.registerAttribute("allow_panel_auth_for_pymts", "allow_panel_auth_for_pymts", "IndicatorAttribute");

      /* allow_xfer_btwn_accts_panel - Indicates whether or not this corporate organization can transfer between
         accounts using panel levels.

         Yes=Corporate organization can create */
      attributeMgr.registerAttribute("allow_xfer_btwn_accts_panel", "allow_xfer_btwn_accts_panel", "IndicatorAttribute");

      /* allow_domestic_payments_panel - Indicates whether or not this corporate organization can make domestic payments
         using panel levels.

         Yes=Corporate organization can create */
      attributeMgr.registerAttribute("allow_domestic_payments_panel", "allow_domestic_payments_panel", "IndicatorAttribute");

      /* allow_funds_transfer_panel - Indicates whether or not this corporate organization can create Funds Transfer
         transactions using panel levels.

         Yes=Corporate organization can create */
      attributeMgr.registerAttribute("allow_funds_transfer_panel", "allow_funds_transfer_panel", "IndicatorAttribute");

      /* doc_prep_url - The document preparation (i.e. corporate service) URL. */
      attributeMgr.registerAttribute("doc_prep_url", "doc_prep_url");
      attributeMgr.registerAlias("doc_prep_url", getResourceManager().getText("CorporateOrganizationBeanAlias.doc_prep_url", TradePortalConstants.TEXT_BUNDLE));

      /* reporting_type - Reporting Type indicates whether the reporting type for the user of this
         corporate org is Cash Management or Trade Services. */
      attributeMgr.registerAttribute("reporting_type", "reporting_type");
      attributeMgr.requiredAttribute("reporting_type");
      //IR-SGUL050957854 05/11/2011 RKAZI Begin
      attributeMgr.registerAlias("reporting_type", getResourceManager().getText("CorporateOrganizationBeanAlias.reporting_type", TradePortalConstants.TEXT_BUNDLE));
      //IR-SGUL050957854 05/11/2011 RKAZI End

      /* cust_daily_balance_amt_ftrq - This amount tracks the daily running balance for funds transfer requests.
         The balance only applies to the date stored in cust_daily_balance_date,
         and the amount is reset to zero with the first transaction of each day. */
      attributeMgr.registerAttribute("cust_daily_balance_amt_ftrq", "cust_daily_balance_amt_ftrq", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      attributeMgr.registerAlias("cust_daily_balance_amt_ftrq", getResourceManager().getText("CorporateOrganizationBeanAlias.cust_daily_balance_amt_ftrq", TradePortalConstants.TEXT_BUNDLE));

      /* cust_daily_balance_amt_ftdp - This amount tracks the daily running balance for domestic payments.  The
         balance only applies to the date stored in cust_daily_balance_date, and
         the amount is reset to zero with the first transaction of each day. */
      attributeMgr.registerAttribute("cust_daily_balance_amt_ftdp", "cust_daily_balance_amt_ftdp", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      attributeMgr.registerAlias("cust_daily_balance_amt_ftdp", getResourceManager().getText("CorporateOrganizationBeanAlias.cust_daily_balance_amt_ftdp", TradePortalConstants.TEXT_BUNDLE));

      /* cust_daily_balance_amt_ftba - This amount tracks the daily running balance for funds transfer between
         accounts.  The balance only applies to the date stored in cust_daily_balance_date,
         and the amount is reset to zero with the first transaction of each day. */
      attributeMgr.registerAttribute("cust_daily_balance_amt_ftba", "cust_daily_balance_amt_ftba", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      attributeMgr.registerAlias("cust_daily_balance_amt_ftba", getResourceManager().getText("CorporateOrganizationBeanAlias.cust_daily_balance_amt_ftba", TradePortalConstants.TEXT_BUNDLE));

      /* cust_daily_balance_date - Timestamp of the current processing date associated with the cust_daily_balance_amt.
         When the balance  is reset to zero with the first transaction of each day,
         this field is set to the current processing date. */
      attributeMgr.registerAttribute("cust_daily_balance_date", "cust_daily_balance_date", "DateTimeAttribute");

      /* fx_rate_group - The FX Rate Group (as defined in OTL) for the customer/base currency.  It
         is a simple TextAttribute and is not validated against REFDATA. */
      attributeMgr.registerAttribute("fx_rate_group", "fx_rate_group");
      /* Suresh CR-713 Rel 8.0 - interest_disc_rate_group - Interest Discount Rate Type */
      attributeMgr.registerAttribute("interest_disc_rate_group", "interest_disc_rate_group");

      /* allow_non_prtl_org_instr_ind - Indicates whether this corporate organization can view instruments originated
         from OTL.  It is possible that a corporate organization cannot create any
         instruments but still can view those originated from OTL.

         Yes=Corporate organization can view */
      attributeMgr.registerAttribute("allow_non_prtl_org_instr_ind", "allow_non_prtl_org_instr_ind", "IndicatorAttribute");

      /* allow_direct_debit - Indicates whether or not this corporate organization can do Direct Debit.

Y=Yes */
      attributeMgr.registerAttribute("allow_direct_debit", "allow_direct_debit", "IndicatorAttribute");

      /* dual_auth_direct_debit - Indicates whether or not dual authorization is required for  Direct Debit
         transactions.

         N or null=Can be authorized by one user
         Y=Must be authorized by two users
         G=Must be authorized by two users that belong to different work groups */
      attributeMgr.registerReferenceAttribute("dual_auth_direct_debit", "dual_auth_direct_debit", "DUAL_AUTH_REQ");
      //Leelavathi CR-710 Rel 8.0.0 24/10/2011 Begin
      /* allow_invoice_file_upload - Indicates whether or not this corporate organization can Upload invoices. */
      attributeMgr.registerAttribute("allow_invoice_file_upload", "allow_invoice_file_upload", "IndicatorAttribute");
      //Leelavathi CR-710 Rel 8.0.0 24/10/2011 End

      /* allow_new_export_collection - Indicates whether or not this corporate organization can create Export Collection
         transactions.  (This is the proper Export Collection from TPS perspective.
         allow_export_collection is only a Direct Send collection.) */
      attributeMgr.registerAttribute("allow_new_export_collection", "allow_new_export_collection", "IndicatorAttribute");

      /* dual_auth_new_export_coll - Indicates whether or not dual authorization is required for  Export Collection
         transactions.  (dual_auth_export_coll is only for Direct Send Collection.)

         N or null=Can be authorized by one user
         Y=Must be authorized by two users
         G=Must be authorized by two users that belong to different work groups */
      attributeMgr.registerReferenceAttribute("dual_auth_new_export_coll", "dual_auth_new_export_coll", "DUAL_AUTH_REQ");

      /* client_bank_oid - Each corporate organization is associated to a bank organization group,
         which is then "owned" in the Trade Portal by a client bank.   This association
         is a shortcut to the client bank from corporate org to make reporting more
         efficient. */
      attributeMgr.registerAssociation("client_bank_oid", "a_client_bank_oid", "ClientBank");

        /* Pointer to the parent CorporateOrganization */
      attributeMgr.registerAttribute("parent_corp_org_oid", "p_parent_corp_org_oid", "ParentIDAttribute");

      /* bank_org_group_oid - Each corporate customer is assigned to a bank oranization group.   This
         essentially means that the corporate organization thinks that they are doing
         business with the particalar BOG.  This association is used to determine
         which branding directory to use for each corporate customer's users. */
      attributeMgr.registerAssociation("bank_org_group_oid", "a_bank_org_group_oid", "BankOrganizationGroup");
      attributeMgr.requiredAttribute("bank_org_group_oid");
      attributeMgr.registerAlias("bank_org_group_oid",getResourceManager().getText("CorporateOrganizationBeanAlias.bank_org_group_oid",TradePortalConstants.TEXT_BUNDLE));

      /* first_op_bank_org - Each corporate customer can do business with up to four operational bank
         organizations.  Instead of creating a one-to-many association, four one-to-one
         assocaitions were created to make coding simpler. */
      attributeMgr.registerAssociation("first_op_bank_org", "a_first_op_bank_org", "OperationalBankOrganization");
    //Kyriba CR 268 Start
      //Remove Mandatory check for first op bank
      //attributeMgr.requiredAttribute("first_op_bank_org"); 
      //attributeMgr.registerAlias("first_op_bank_org",getResourceManager().getText("CorporateOrganizationBeanAlias.first_op_bank_org",TradePortalConstants.TEXT_BUNDLE));
    //Kyriba CR 268 End
      /* second_op_bank_org - Each corporate customer can do business with up to four operational bank
         organizations.  Instead of creating a one-to-many association, four one-to-one
         assocaitions were created to make coding simpler. */
      attributeMgr.registerAssociation("second_op_bank_org", "a_second_op_bank_org", "OperationalBankOrganization");

      /* third_op_bank_org - Each corporate customer can do business with up to four operational bank
         organizations.  Instead of creating a one-to-many association, four one-to-one
         assocaitions were created to make coding simpler. */
      attributeMgr.registerAssociation("third_op_bank_org", "a_third_op_bank_org", "OperationalBankOrganization");

      /* fourth_op_bank_org - Each corporate customer can do business with up to four operational bank
         organizations.  Instead of creating a one-to-many association, four one-to-one
         assocaitions were created to make coding simpler. */
      attributeMgr.registerAssociation("fourth_op_bank_org", "a_fourth_op_bank_org", "OperationalBankOrganization");

      //Suresh CR-603 03/11/2011 Begin
      /* first_rept_categ - Each corporate customer can do business with up to ten report
      categories.  Instead of creating a one-to-many association, ten one-to-one
      assocaitions were created to make coding simpler. */
      attributeMgr.registerReferenceAttribute("first_rept_categ", "first_rept_categ", "REPORTING_CATEGORY");

      /* second_rept_categ - Each corporate customer can do business with up to ten report
      categories.  Instead of creating a one-to-many association, ten one-to-one
      assocaitions were created to make coding simpler. */
      attributeMgr.registerReferenceAttribute("second_rept_categ", "second_rept_categ", "REPORTING_CATEGORY");

      /* third_rept_categ - Each corporate customer can do business with up to ten report
      categories.  Instead of creating a one-to-many association, ten one-to-one
      assocaitions were created to make coding simpler. */
      attributeMgr.registerReferenceAttribute("third_rept_categ", "third_rept_categ", "REPORTING_CATEGORY");

      /* fourth_rept_categ - Each corporate customer can do business with up to ten report
      categories.  Instead of creating a one-to-many association, ten one-to-one
      assocaitions were created to make coding simpler. */
      attributeMgr.registerReferenceAttribute("fourth_rept_categ", "fourth_rept_categ", "REPORTING_CATEGORY");

      /* fifth_rept_categ - Each corporate customer can do business with up to ten report
      categories.  Instead of creating a one-to-many association, ten one-to-one
      assocaitions were created to make coding simpler. */
      attributeMgr.registerReferenceAttribute("fifth_rept_categ", "fifth_rept_categ", "REPORTING_CATEGORY");

      /* sixth_rept_categ - Each corporate customer can do business with up to ten report
      categories.  Instead of creating a one-to-many association, ten one-to-one
      assocaitions were created to make coding simpler. */
      attributeMgr.registerReferenceAttribute("sixth_rept_categ", "sixth_rept_categ", "REPORTING_CATEGORY");

      /* seventh_rept_categ - Each corporate customer can do business with up to ten report
      categories.  Instead of creating a one-to-many association, ten one-to-one
      assocaitions were created to make coding simpler. */
      attributeMgr.registerReferenceAttribute("seventh_rept_categ", "seventh_rept_categ", "REPORTING_CATEGORY");

      /* eighth_rept_categ - Each corporate customer can do business with up to ten report
      categories.  Instead of creating a one-to-many association, ten one-to-one
      assocaitions were created to make coding simpler. */
      attributeMgr.registerReferenceAttribute("eighth_rept_categ", "eighth_rept_categ", "REPORTING_CATEGORY");

      /* nineth_rept_categ - Each corporate customer can do business with up to ten report
      categories.  Instead of creating a one-to-many association, ten one-to-one
      assocaitions were created to make coding simpler. */
      attributeMgr.registerReferenceAttribute("nineth_rept_categ", "nineth_rept_categ", "REPORTING_CATEGORY");

      /* tenth_rept_categ - Each corporate customer can do business with up to ten report
      categories.  Instead of creating a one-to-many association, ten one-to-one
      assocaitions were created to make coding simpler. */
      attributeMgr.registerReferenceAttribute("tenth_rept_categ", "tenth_rept_categ", "REPORTING_CATEGORY");
      //Suresh CR-603 03/11/2011 End
      
            
      //Vshah - CR#564 - [BEGIN]
      /* allow_payment_file_upload - Indicates whether or not this corporate organization can Process Payment thru File Upload. */
      attributeMgr.registerAttribute("allow_payment_file_upload", "allow_payment_file_upload", "IndicatorAttribute");
      //Vshah - CR#564 - [END]

      //Narayan CR-644 Begin
      /* alternate_name - Alternate Corporate Customer Name*/
      attributeMgr.registerAttribute("alternate_name", "alternate_name");
      //Narayan CR-644 End

      // CJR - 04/06/2011 - CR-593 Begin


      /* auto_auth_incoming_payment - Indicates whether to automatically authorize incoming payments. */
      attributeMgr.registerAttribute("straight_through_authorize", "straight_through_authorize");

      /* h2h_system_user - Host to Host System User. */
      attributeMgr.registerAssociation("h2h_system_user_oid", "a_h2h_system_user_oid", "User");
      // CJR - 04/06/2011 - CR-593 End
   // Leelavathi CR-710 Rel-8.0 08/12/2011 Begin
      attributeMgr.registerAssociation("upload_system_user_oid", "a_upload_system_user_oid", "User");
      // Leelavathi CR-710 Rel-8.0 08/12/2011 End

	  // DK CR-640 Rel7.1 BEGINS
	  /* fx_online_acct_id - FX Online Account ID */
      attributeMgr.registerAttribute("fx_online_acct_id", "fx_online_acct_id");
      // DK CR-640 Rel7.1 ENDS
   // Srinivasu_D CR-708 Rel8.1.1 05/11/2012 Start

	  attributeMgr.registerAttribute("atp_invoice_indicator", "atp_invoice_ind", "IndicatorAttribute");

	  attributeMgr.registerAttribute("failed_invoice_indicator", "failed_invoice_ind", "IndicatorAttribute");

	  attributeMgr.registerAttribute("payment_day_allow", "payment_day_allow_ind");

	  attributeMgr.registerAttribute("days_before", "days_before","NumberAttribute");

	  attributeMgr.registerAttribute("days_after", "days_after","NumberAttribute");
	  // Srinivasu_D CR-708 Rel8.1.1 05/11/2012 End
	  
      //AAlubala - Rel8.2 CR741 - BEGIN
      attributeMgr.registerAttribute("erp_transaction_rpt_reqd", "erp_transaction_rpt_reqd", "IndicatorAttribute"); //ERP_TRANSACTION_RPT_REQD
      attributeMgr.registerAttribute("corp_discount_code_type", "corp_discount_code_type"); //CORP_DISCOUNT_CODE_TYPE
      //END	  
	  // DK CR709 Rel8.2 10/31/2012 BEGINS
      attributeMgr.registerAttribute("process_invoice_file_upload", "process_invoice_file_upload", "IndicatorAttribute");
      attributeMgr.registerAttribute("trade_loan_type", "trade_loan_type", "IndicatorAttribute");
      attributeMgr.registerAttribute("import_loan_type", "import_loan_type", "IndicatorAttribute");
      attributeMgr.registerAttribute("export_loan_type", "export_loan_type", "IndicatorAttribute");
      attributeMgr.registerAttribute("invoice_financing_type", "invoice_financing_type", "IndicatorAttribute");
      attributeMgr.registerAttribute("allow_auto_loan_req_inv_upld", "allow_auto_loan_req_inv_upld", "IndicatorAttribute");
      attributeMgr.registerAttribute("use_payment_date_allowed_ind", "use_payment_date_allowed_ind", "IndicatorAttribute");
      attributeMgr.registerAttribute("days_before_payment_date", "days_before_payment_date","NumberAttribute");
      attributeMgr.registerAlias("days_before_payment_date",getResourceManager().getText("CorpCust.PaymenttoInvoiceDaysBefore",TradePortalConstants.TEXT_BUNDLE));
      attributeMgr.registerAttribute("days_after_payment_date", "days_after_payment_date","NumberAttribute");
      attributeMgr.registerAlias("days_after_payment_date",getResourceManager().getText("CorpCust.PaymenttoInvoiceDaysAfter",TradePortalConstants.TEXT_BUNDLE));
      attributeMgr.registerAttribute("fin_days_before_loan_maturity", "fin_days_before_loan_maturity","NumberAttribute");
      attributeMgr.registerAlias("fin_days_before_loan_maturity",getResourceManager().getText("CorpCust.DaysBeforeLoanMaturityDate",TradePortalConstants.TEXT_BUNDLE));
      attributeMgr.registerAttribute("inv_percent_to_fin_trade_loan", "inv_percent_to_fin_trade_loan","TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      attributeMgr.registerAlias("inv_percent_to_fin_trade_loan",getResourceManager().getText("CorpCust.InvoiceForReceivablesTradeLoan",TradePortalConstants.TEXT_BUNDLE));
      attributeMgr.registerAttribute("allow_auto_atp_inv_only_upld", "allow_auto_atp_inv_only_upld", "IndicatorAttribute");
      // DK CR709 Rel8.2 10/31/2012 ENDS
	  
	  //Ravindra - Rel8200 CR-708B - Start
	  /* sp_calculate_discount_ind - S/D whether to calculate supplier portal invoice 
	   	with Straight discount or Discount to yield. */
	  attributeMgr.registerReferenceAttribute("sp_calculate_discount_ind", "sp_calculate_discount_ind", "CALCULATE_DISCOUNT_USING");
	  //Ravindra - Rel8200 CR-708B - End
	  
	//M Eerupula 04/16/2013 Rel8.3 CR776 
	  attributeMgr.registerAttribute("payables_prog_name" ,  "payables_prog_name");  
	  
	
    //M Eerupula 04/22/2013 Rel 8.3 CR 821
	  attributeMgr.registerAttribute("inherit_panel_auth_ind" , "inherit_panel_auth_ind" , "IndicatorAttribute");
	  attributeMgr.registerAttribute("panel_level_A_alias" , "panel_level_A_alias"); 
	  attributeMgr.registerAttribute("panel_level_B_alias" , "panel_level_B_alias");
	  attributeMgr.registerAttribute("panel_level_C_alias" , "panel_level_C_alias"); 
	  attributeMgr.registerAttribute("panel_level_D_alias" , "panel_level_D_alias"); 
	  attributeMgr.registerAttribute("panel_level_E_alias" , "panel_level_E_alias");
	  attributeMgr.registerAttribute("panel_level_F_alias" , "panel_level_F_alias"); 
	  attributeMgr.registerAttribute("panel_level_G_alias" , "panel_level_G_alias"); 
	  attributeMgr.registerAttribute("panel_level_H_alias" , "panel_level_H_alias");
	  attributeMgr.registerAttribute("panel_level_I_alias" , "panel_level_I_alias"); 
	  attributeMgr.registerAttribute("panel_level_J_alias" , "panel_level_J_alias"); 
	 
	 //CR 812 - Rel 8.3 START
	  /*IR T36000019042 */
	  attributeMgr.registerAssociation("air_panel_group_oid", "awb_panel_group_oid", "PanelAuthorizationGroup");
	  attributeMgr.registerAssociation("exp_dlc_panel_group_oid", "exp_dlc_panel_group_oid", "PanelAuthorizationGroup");
	  attributeMgr.registerAssociation("exp_col_panel_group_oid", "exp_col_panel_group_oid", "PanelAuthorizationGroup");
	  attributeMgr.registerAssociation("exp_oco_panel_group_oid", "exp_oco_panel_group_oid", "PanelAuthorizationGroup");
	  /*IR T36000018638 Prateep fix. guar_panel_group_oid to gua_panel_group_oid*/
	  attributeMgr.registerAssociation("gua_panel_group_oid", "guar_panel_group_oid", "PanelAuthorizationGroup");
	  attributeMgr.registerAssociation("imp_dlc_panel_group_oid", "imp_dlc_panel_group_oid", "PanelAuthorizationGroup");
	  attributeMgr.registerAssociation("slc_panel_group_oid", "slc_panel_group_oid", "PanelAuthorizationGroup");
	  attributeMgr.registerAssociation("shp_panel_group_oid", "shp_panel_group_oid", "PanelAuthorizationGroup");
	  attributeMgr.registerAssociation("lrq_panel_group_oid", "lrq_panel_group_oid", "PanelAuthorizationGroup");
	  attributeMgr.registerAssociation("rqa_panel_group_oid", "rqa_panel_group_oid", "PanelAuthorizationGroup");
	  attributeMgr.registerAssociation("supplier_panel_group_oid", "supplier_panel_group_oid", "PanelAuthorizationGroup");
	  attributeMgr.registerAssociation("atp_panel_group_oid", "atp_panel_group_oid", "PanelAuthorizationGroup");
	  attributeMgr.registerAssociation("dcr_panel_group_oid", "dcr_panel_group_oid", "PanelAuthorizationGroup");
	  attributeMgr.registerAssociation("mtch_apprv_panel_group_oid", "mtch_apprv_panel_group_oid", "PanelAuthorizationGroup");
	  attributeMgr.registerAssociation("inv_auth_panel_group_oid", "inv_auth_panel_group_oid", "PanelAuthorizationGroup");
	  attributeMgr.registerAssociation("credit_auth_panel_group_oid", "credit_auth_panel_group_oid", "PanelAuthorizationGroup");
	  /*IR T36000019042 */
	  attributeMgr.registerAttribute("air_checker_auth_ind", "awb_checker_auth_ind","IndicatorAttribute");
	  attributeMgr.registerAttribute("atp_checker_auth_ind", "atp_checker_auth_ind","IndicatorAttribute");
	  attributeMgr.registerAttribute("exp_col_checker_auth_ind", "exp_col_checker_auth_ind","IndicatorAttribute");
	  attributeMgr.registerAttribute("exp_dlc_checker_auth_ind", "exp_dlc_checker_auth_ind","IndicatorAttribute");
	  attributeMgr.registerAttribute("exp_oco_checker_auth_ind", "exp_oco_checker_auth_ind","IndicatorAttribute");
	  attributeMgr.registerAttribute("imp_dlc_checker_auth_ind", "imp_dlc_checker_auth_ind","IndicatorAttribute");
	  attributeMgr.registerAttribute("lrq_checker_auth_ind", "lrq_checker_auth_ind","IndicatorAttribute");
	  /*IR T36000018638 Prateep fix. guar_checker_auth_ind to gua_checker_auth_ind*/
	  attributeMgr.registerAttribute("gua_checker_auth_ind", "guar_checker_auth_ind","IndicatorAttribute");
	  attributeMgr.registerAttribute("slc_checker_auth_ind", "slc_checker_auth_ind","IndicatorAttribute");
	  attributeMgr.registerAttribute("shp_checker_auth_ind", "shp_checker_auth_ind","IndicatorAttribute");
	  attributeMgr.registerAttribute("rqa_checker_auth_ind", "rqa_checker_auth_ind","IndicatorAttribute");
	  attributeMgr.registerAttribute("dcr_checker_auth_ind", "dcr_checker_auth_ind","IndicatorAttribute");
	  attributeMgr.registerAttribute("ftrq_checker_auth_ind", "ftrq_checker_auth_ind","IndicatorAttribute");
	  attributeMgr.registerAttribute("ftdp_checker_auth_ind", "ftdp_checker_auth_ind","IndicatorAttribute");
	  attributeMgr.registerAttribute("ftba_checker_auth_ind", "ftba_checker_auth_ind","IndicatorAttribute");
	  
	  // CR 812 - Rel 8.3 END
	  
	  //Rel 8.3 CR 776
   	   /* Pointer to the component SPEmailNotifications */
   	   attributeMgr.registerAttribute("c_SPEmailNotifications", "c_ORG_SPEMAIL_OID", "NumberAttribute");
   	   attributeMgr.registerAttribute("myorg_profile_validation", "myorg_profile_validation", "LocalAttribute");
	   /*CR 857 */
	   attributeMgr.registerAttribute("pmt_bene_panel_auth_ind", "pmt_bene_panel_auth_ind","IndicatorAttribute");
	   
	   // DK CR-886 10/15/2013 Rel 8.4 starts
	      attributeMgr.registerReferenceAttribute("pmt_bene_sort_default", "pmt_bene_sort_default", "PMT_BENE_SORT");
	      //attributeMgr.requiredAttribute("pmt_bene_sort_default");// T36000024113 RKAZI 01/15/2014 - commented since this is reuiqred only for My Org Profile.
	      attributeMgr.registerAlias("pmt_bene_sort_default", getResourceManager().getText("OrgProfileDetail.PmtBeneView", TradePortalConstants.TEXT_BUNDLE));		  
      // DK CR-886 10/15/2013 Rel 8.4 ends
	   
	  // Srinivasu_D CR-599 Rel8.4 11/20/2013 start
   	  attributeMgr.registerReferenceAttribute("man_upld_pmt_file_frmt_ind", "man_upld_pmt_file_frmt_ind","PAYMENT_FILE_FORMAT");
	  attributeMgr.registerAttribute("man_upld_pmt_file_dir_ind", "man_upld_pmt_file_dir_ind","IndicatorAttribute");
	  attributeMgr.registerAttribute("restricted_pay_mgmt_upload_dir", "restricted_pay_mgmt_upload_dir");

	 // Srinivasu_D CR-599 Rel8.4 11/20/2013 end
	//MEerupula Rel 8.4 CR-934a Add mobile banking indicator to enable corporate organizations to use web services
	  //enabling corporate customers to access the Trade360 Portal via  mobile device(Trade360 Web Services)
      	attributeMgr.registerAttribute("mobile_banking_access_ind", "mobile_banking_access_ind", "IndicatorAttribute");
	  
	  //Rel9.0 CR 913 START
	  attributeMgr.registerAttribute("allow_payables", "allow_payables", "IndicatorAttribute");
	  attributeMgr.registerAttribute("allow_payables_upl", "allow_payables_upl", "IndicatorAttribute");
	  attributeMgr.registerReferenceAttribute("dual_auth_payables_inv", "dual_auth_payables_inv", "DUAL_AUTH_REQ");
	  attributeMgr.registerReferenceAttribute("dual_auth_upload_pay_inv", "dual_auth_upload_pay_inv", "DUAL_AUTH_REQ");
	  attributeMgr.registerAssociation("pay_inv_panel_group_oid", "pay_inv_panel_group_oid", "PanelAuthorizationGroup");
	  attributeMgr.registerAssociation("upl_pay_inv_panel_group_oid", "upl_pay_inv_panel_group_oid", "PanelAuthorizationGroup");
	  attributeMgr.registerAttribute("invoice_file_upload_pay_ind", "invoice_file_upload_pay_ind", "IndicatorAttribute");
	  attributeMgr.registerAttribute("invoice_file_upload_rec_ind", "invoice_file_upload_rec_ind", "IndicatorAttribute");
	  attributeMgr.registerAttribute("pay_invoice_grouping_ind", "pay_invoice_grouping_ind", "IndicatorAttribute");
	  attributeMgr.registerAttribute("pay_invoice_utilised_ind", "pay_invoice_utilised_ind", "IndicatorAttribute");
	  attributeMgr.registerAttribute("rec_invoice_utilised_ind", "rec_invoice_utilised_ind", "IndicatorAttribute");
	  //Rel9.0 CR 913 END
	  attributeMgr.registerAttribute("pre_debit_funding_option_ind", "pre_debit_funding_option_ind", "IndicatorAttribute");
	  
	  //R9.2 CR 914A 
	  attributeMgr.registerAttribute("allow_pay_credit_note_upload", "allow_pay_credit_note_upload","IndicatorAttribute");
	  attributeMgr.registerAttribute("allow_pay_credit_note", "allow_pay_credit_note","IndicatorAttribute");
 	  
	  attributeMgr.registerAttribute("allow_end_to_end_id_process", "allow_end_to_end_id_process","IndicatorAttribute");
	  attributeMgr.registerAttribute("allow_manual_cr_note_apply", "allow_manual_cr_note_apply","IndicatorAttribute");
 
	  attributeMgr.registerReferenceAttribute("apply_pay_credit_note", "apply_pay_credit_note","CREDIT_NOTE_APPLY_TYPE");
	  attributeMgr.registerReferenceAttribute("pay_credit_sequential_type", "pay_credit_sequential_type","CREDIT_NOTE_SEQUENTIAL_TYPE");
	  attributeMgr.registerReferenceAttribute("dual_auth_pay_credit_note", "dual_auth_pay_credit_note","DUAL_AUTH_REQ");
	  
	  attributeMgr.registerAttribute("pay_credit_note_available_days", "pay_credit_note_available_days");
	  attributeMgr.registerAssociation("pay_cr_note_auth_panel_grp_oid", "pay_cr_note_auth_panel_grp_oid","PanelAuthorizationGroup");
	//R9.2
	
	//Rel9.2 CR 988 Begin
	  /* allow_partial_pay_auth - Indicates whether to automatically authorize H2H partial payments. */
      	attributeMgr.registerAttribute("allow_partial_pay_auth", "allow_partial_pay_auth", "IndicatorAttribute");
     	 //Rel9.2 CR 988 End 
      	//Srinivasu_D CR#1006 Rel9.3 04/27/2015 - added attributes for InvoiceCR notification
        attributeMgr.registerAttribute("c_InvoiceCRNoteEmailNotification", "c_org_inv_crn_oid", "NumberAttribute");
		
      //Srinivasu_D CR#1006 Rel9.3 04/27/2015 - End     	
     
	 //Rel9.2 CR 988 End

			//#RKAZI CR1006 04-24-2015 - BEGIN
       attributeMgr.registerAttribute("allow_rec_dec_inv", "allow_rec_dec_inv", "IndicatorAttribute");
       attributeMgr.registerAttribute("allow_pay_dec_inv", "allow_pay_dec_inv", "IndicatorAttribute");
       attributeMgr.registerAttribute("allow_pay_dec_crn", "allow_pay_dec_crn", "IndicatorAttribute");
       attributeMgr.registerReferenceAttribute("dual_auth_rec_dec_inv", "dual_auth_rec_dec_inv", "DUAL_AUTH_REQ");
       attributeMgr.registerReferenceAttribute("dual_auth_pay_dec_inv", "dual_auth_pay_dec_inv", "DUAL_AUTH_REQ");
       attributeMgr.registerReferenceAttribute("dual_auth_pay_dec_crn", "dual_auth_pay_dec_crn", "DUAL_AUTH_REQ");
       attributeMgr.registerAttribute("allow_h2h_pay_inv_approval", "allow_h2h_pay_inv_approval", "IndicatorAttribute");
       attributeMgr.registerAttribute("allow_h2h_pay_crn_approval", "allow_h2h_pay_crn_approval", "IndicatorAttribute");
       attributeMgr.registerAttribute("allow_h2h_rec_inv_approval", "allow_h2h_rec_inv_approval", "IndicatorAttribute");
	   //#RKAZI CR1006 04-24-2015 END
       //SSikhakolli Rel 9.3.5 CR-1029 Adding new attribute cust_is_not_intg_tps
       attributeMgr.registerAttribute("cust_is_not_intg_tps", "cust_is_not_intg_tps", "IndicatorAttribute");
       
     //SSikhakolli - Rel-9.4 CR-818 - adding new attributes
       attributeMgr.registerAttribute("include_settle_instruction" , "include_settle_instruction", "IndicatorAttribute"); 
       attributeMgr.registerAttribute("allow_settle_instruction" , "allow_settle_instruction", "IndicatorAttribute");
       attributeMgr.registerAttribute("sit_checker_auth_ind" , "sit_checker_auth_ind", "IndicatorAttribute");
       attributeMgr.registerReferenceAttribute("dual_settle_instruction", "dual_settle_instruction", "DUAL_AUTH_REQ");
       attributeMgr.registerAttribute("sit_panel_group_oid" , "sit_panel_group_oid", "NumberAttribute");
       attributeMgr.registerAttribute("allow_imp_col" , "allow_imp_col", "IndicatorAttribute");
       attributeMgr.registerAttribute("imp_col_checker_auth_ind" , "imp_col_checker_auth_ind", "IndicatorAttribute");
       attributeMgr.registerReferenceAttribute("dual_imp_col", "dual_imp_col", "DUAL_AUTH_REQ");
       attributeMgr.registerAttribute("imp_col_panel_group_oid" , "imp_col_panel_group_oid", "NumberAttribute");
     //SURREWSH - Rel-9.4 CR-932 -adding new attributes 
      attributeMgr.registerAttribute("enable_billing_instruments" , "enable_billing_instruments", "IndicatorAttribute");
      
      //Nar Rel9.5.0.0 CR-1132 01/27/2016 - Begin
      //added attributes for Portal only Bank Additional address user defined fields
      attributeMgr.registerAttribute("user_defined_field_1", "user_defined_field_1");
      attributeMgr.registerAttribute("user_defined_field_2", "user_defined_field_2");
      attributeMgr.registerAttribute("user_defined_field_3", "user_defined_field_3");
      attributeMgr.registerAttribute("user_defined_field_4", "user_defined_field_4");
      //Nar Rel9.5.0.0 CR-1132 01/27/2016 - End
      
  }

  /*
   * Register the components of the business object
   */
   protected void registerComponents() throws RemoteException, AmsException
   {
      /* Register the components defined in the Ancestor class */
      super.registerComponents();

      /* ThresholdGroupList - A threshold group is "owned" by a corporate organization.  This component
         relationship represents the ownership. */
      registerOneToManyComponent("ThresholdGroupList","ThresholdGroupList");

      /* CorporateOrganizationList - Sometimes, a hierarchy of corporate customers will exist.  This hierarchy
         is established through this association.   This association points to the
         parent corporate org. */
      registerOneToManyComponent("CorporateOrganizationList","CorporateOrganizationList");

      /* FXRateList - An FX Rate is "owned" by a corporate organization.  This component relationship
         represents the ownership. */
      registerOneToManyComponent("FXRateList","FXRateList");

      /* InstrumentPurgeHistoryList - The corporate organization to which the purged instrument belongs. */
      registerOneToManyComponent("InstrumentPurgeHistoryList","InstrumentPurgeHistoryList");

      /* AccountList -  */
      registerOneToManyComponent("AccountList","AccountList");

      registerOneToManyComponent("AddressList","AddressList");
      /* WorkGroupList - A WorkGroup belongs to a corporate organizaiton. */
      registerOneToManyComponent("WorkGroupList","WorkGroupList");

      /* AddressList - The list of additional addresses.  (The main address is on the corporate
         org itself.) 
      registerOneToManyComponent("AddressList","AddressList");*/

      /* ArMatchingRuleList - An ARMatchingRule belongs to a corporate organization */
      registerOneToManyComponent("ArMatchingRuleList","ArMatchingRuleList");

       /* PanelAuthorizationGroupList - A corporate org can have multiple Panel Authorization Groups (Reference
          Data). */
       registerOneToManyComponent("PanelAuthorizationGroupList","PanelAuthorizationGroupList");
		// DK CR-587 Begins
		/* CustomerAliasList */
		registerOneToManyComponent("CustomerAliasList", "CustomerAliasList");
		// DK CR-587 Ends
		/* Suresh CR-713 Rel 8.0 - CustomerMarginRuleList -  */
	   registerOneToManyComponent("CustomerMarginRuleList","CustomerMarginRuleList");
	   
	   /* Ravindra Rel8200 CR-708B - SPMarginRuleList -  */
	   registerOneToManyComponent("SPMarginRuleList","SPMarginRuleList");
	   /* Kyriba CR 268 - ExternalBankList -  */
	   registerOneToManyComponent("ExternalBankList","ExternalBankList");
	   
	   // Srinivasu_D CR-708 Rel8.1.1 05/11/2012 End
	   registerOneToManyComponent("UserPreferencesPayDefList","UserPreferencesPayDefList");
	 
   }
}