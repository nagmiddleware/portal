
  

package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;
/**
 * The business calendar that stores the holiday information.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface TPCalendarHome extends EJBHome
{
   public TPCalendar create()
      throws RemoteException, CreateException, AmsException;

   public TPCalendar create(ClientServerDataBridge csdb)
      throws RemoteException, CreateException, AmsException;
}
