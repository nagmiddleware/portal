
  
/*
 * This file is generated from the model.  Normally it should not be modified manually.  
 * Contact the modeler first.  (In a pinch, it can be modified first and then retrofitted 
 * to the model.)
 *
 */
package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;


/*
 * 
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class InvoiceUploadErrorLogBean_Base extends BusinessObjectBean
{
private static final Logger LOG = LoggerFactory.getLogger(InvoiceUploadErrorLogBean_Base.class);

  
  /* 
   * Register the attributes and associations of the business object
   */
   protected void registerAttributes() throws AmsException
   {  
      /* invoice_upload_error_log_oid - Unique identifier */
      attributeMgr.registerAttribute("invoice_upload_error_log_oid", "invoice_upload_error_log_oid", "ObjectIDAttribute");
      
      /* invoice_id */
      attributeMgr.registerAttribute("invoice_id", "invoice_id");
      
      /* error_msg - Error message. */
      attributeMgr.registerAttribute("error_msg", "error_msg");
      
      /* trading_partner_name -trading_partner_name */
      attributeMgr.registerAttribute("trading_partner_name", "trading_partner_name");
      
        /* Pointer to the parent InvoiceFileUpload */
      attributeMgr.registerAttribute("invoice_file_upload_oid", "p_invoice_file_upload_uoid", "ParentIDAttribute");
   
   }
   
 
   
 
 
   
}
