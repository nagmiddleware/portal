 
package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;




/*
 * 
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class TradePartnerMarginRulesBean extends TradePartnerMarginRulesBean_Base
{
private static final Logger LOG = LoggerFactory.getLogger(TradePartnerMarginRulesBean.class);
	
	public String getListCriteria(String type) 
	   {
		  // Retrieve the BuyerIdAlias by AR Matching Rule Oid
		  if (type.equals("byArMatchingRuleOid"))
		  {
			 return "p_ar_matching_rule_oid = {0}";
		  }

		  return "";
	   }                     
  
}
