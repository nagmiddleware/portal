


package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;




/*
 * A fee that is associated with a transaction.   This table is populated by
 * the middleware when unpackaging data that has come from OTL.
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class FeeBean_Base extends TradePortalBusinessObjectBean
{
private static final Logger LOG = LoggerFactory.getLogger(FeeBean_Base.class);


  /*
   * Register the attributes and associations of the business object
   */
   protected void registerAttributes() throws AmsException
   {

      /* Register attributes defined in the Ancestor class */
      super.registerAttributes();

      /* fee_oid - Unique identifier */
      attributeMgr.registerAttribute("fee_oid", "fee_oid", "ObjectIDAttribute");

      /* settlement_curr_code - Settlement currency of the fee */
      attributeMgr.registerReferenceAttribute("settlement_curr_code", "settlement_curr_code", "CURRENCY_CODE");

      /* settlement_curr_amount - Settlement amount of the fee */
		// GGAYLE - IR EGUE031253734 - 05/10/2004
		// Make the settlement currency amount a signed decimal value.
      	//attributeMgr.registerAttribute("settlement_curr_amount", "settlement_curr_amount", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      attributeMgr.registerAttribute("settlement_curr_amount", "settlement_curr_amount", "TradePortalSignedDecimalAttribute", "com.ams.tradeportal.common");
		// GGAYLE - IR EGUE031253734 - 05/10/2004

      /* charge_type - Type of fee that is being charged to the user's corporate organization */
      attributeMgr.registerReferenceAttribute("charge_type", "charge_type", "CHARGE_TYPE");

      /* base_curr_amount - Base currency amount */
		// GGAYLE - IR EGUE031253734 - 05/10/2004
		// Make the base currency amount a signed decimal value.
      	//attributeMgr.registerAttribute("base_curr_amount", "base_curr_amount", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      attributeMgr.registerAttribute("base_curr_amount", "base_curr_amount", "TradePortalSignedDecimalAttribute", "com.ams.tradeportal.common");
		// GGAYLE - IR EGUE031253734 - 05/10/2004

      /* acct_number - The account number for this fee. */
      attributeMgr.registerAttribute("acct_number", "acct_number");

      /* settlement_how_type - A code that describes how the payment of the fee should be settled. */
      attributeMgr.registerReferenceAttribute("settlement_how_type", "settlement_how_type", "SETTLE_HOW");

        /* Pointer to the parent Transaction */
      attributeMgr.registerAttribute("transaction_oid", "p_transaction_oid", "ParentIDAttribute");

   }






}
