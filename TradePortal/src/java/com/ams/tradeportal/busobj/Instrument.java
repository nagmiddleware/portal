



package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;

import javax.ejb.*;
import com.ams.tradeportal.common.*;


/**
 * Subclass of the InstrumentData business object that represents instruments
 * that are created by the corporate customers of the Trade Portal.    Each
 * instrument has one to many transactions.
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public interface Instrument extends InstrumentData
{
	public void deleteTransaction(java.lang.String transactionOid, java.lang.String userOid, java.lang.String securityRights) throws RemoteException, AmsException;

	public boolean routeTransaction(java.lang.String transactionOid, java.lang.String userOid, java.lang.String securityRights, java.lang.String userOidRoute) throws RemoteException, AmsException;

	public boolean preAuthorizeTransaction(java.lang.String transactionOid, java.lang.String userOid, java.lang.String securityRights) throws RemoteException, AmsException;

	public boolean authorizeTransaction(java.lang.String transactionOid, java.lang.String userOid,java.lang.String authorizeAtParentViewOrSubs, java.lang.String securityRights, boolean checkTransactionStatus,HashMap params) throws RemoteException, AmsException;

	public boolean instrumentNumberIsUnique(java.lang.String instrNum) throws RemoteException, AmsException;

	public boolean isTransactionTypeValid(java.lang.String transactionType) throws RemoteException, AmsException;

	public boolean pendingTransactionsExist() throws RemoteException, AmsException;

	public String createNew(com.amsinc.ecsg.util.DocumentHandler doc) throws RemoteException, AmsException;

	public String createTransOnExisting(com.amsinc.ecsg.util.DocumentHandler doc) throws RemoteException, AmsException;

	public void deleteNotification(java.lang.String transactionOid, java.lang.String securityRights) throws RemoteException, AmsException;

	public void deleteAllNotification(java.lang.String transactionOid, java.lang.String securityRights,List<String> listInstrumentId,boolean isDeleteNotification) throws RemoteException, AmsException;

	public com.amsinc.ecsg.util.DocumentHandler createLCFromPOData(com.amsinc.ecsg.util.DocumentHandler inputDoc, java.util.Vector poOids, java.util.Vector poNums, java.lang.String lcRuleName) throws RemoteException, AmsException;

    public com.amsinc.ecsg.util.DocumentHandler createLCFromStructuredPOData(com.amsinc.ecsg.util.DocumentHandler inputDoc, java.util.Vector poOids, java.util.Vector poNums, java.lang.String lcRuleName) throws RemoteException, AmsException;

	public void updateParent(java.util.Hashtable attributes) throws RemoteException, AmsException;

	// CR-451 NShrestha 12/03/2008 Begin
	public BigDecimal getAmountInBaseCurrency(String currency, String amount, String baseCurrency, String userOrgOid, String instrumentType) 	throws AmsException, RemoteException;

	public BigDecimal getAmounInFXCurrency(String fxcurrency, String baseCurrency, String baseAmount, String userOrgOid, String useType) throws AmsException, RemoteException;
	// CR-451 NShrestha 12/03/2008 End

	// IAZ ER 5.2.1.3 06/29/10 Begin
	public BigDecimal getAmountInBaseCurrency(String currency, String amount, String baseCurrency, String userOrgOid, String instrumentType, BigDecimal userEnteredRate, Vector outMDInd) 	throws AmsException, RemoteException;
	public BigDecimal getAmounInFXCurrency(String fxcurrency, String baseCurrency, String baseAmount, String userOrgOid, String useType, BigDecimal userEnteredRate, Vector outMDInd) throws AmsException, RemoteException;
	// IAZ ER 5.2.1.3 06/29/10 End

	//Removed in 7.0.0 version. No longer used
	//public boolean updateAuthenticationStatus (String transactionOid, String userOid, String securityRights, String reCertStatus) throws RemoteException, AmsException;

	// MDB CR-609 11/29/2010 Begin
	public boolean authorizeFVDTransaction(com.ams.tradeportal.busobj.Transaction transaction) throws RemoteException, AmsException; //MDB Rel6.1 IR# RIUL010274654 1/15/11
	// MDB CR-609 11/29/2010 End

    //MDB CR-640 Rel7.1 9/21/11 Begin
    public BigDecimal getFXMidRate(String currency, String userOrgOid) throws AmsException, RemoteException;
	public void fxCrossRateProcessing(String transferCurrency, String fromCurrency, String baseCurrency, String toCurrency,
								      String amount, Terms terms) throws RemoteException, AmsException;
	public boolean validateMaxDealamount(DocumentHandler inputDoc, String currency, Terms terms, Transaction transaction) throws RemoteException, AmsException; //MDB PRUL112347061 Rel7.1 12/15/11
	public void validateMaxSpotRateAmtThreshold(DocumentHandler inputDoc, String currency, Terms terms, Transaction transaction) throws RemoteException, AmsException; //MDB PRUL112347061 Rel7.1 12/15/11
	public boolean checkFXOnlineIndicator(Terms terms) throws RemoteException, AmsException;
	public boolean requestMarketRateIndicator(Terms terms, Transaction transaction) throws RemoteException, AmsException;
    //MDB CR-640 Rel7.1 9/21/11 End

	public String crossRateCriterionProcessing(Terms terms, String fromCurrency, String toCurrency, String rate) throws RemoteException, AmsException; //MDB CR-640 Rel7.1 10/28/11
	//Srinivasu_D CR-709 Rel8.2 02/02/2013 
	public com.amsinc.ecsg.util.DocumentHandler createLoanReqForInvoiceData(com.amsinc.ecsg.util.DocumentHandler inputDoc, java.util.Vector poOids, java.lang.String lcRuleName) throws RemoteException, AmsException;
    //RKAZI PPX-269 09/18/2013 Rel 8.4 Kyriba Start
	public boolean convertTransaction(String transactionOid, String userOid, String convertAtParentViewOrSubs, String securityRights, boolean checkTransactionStatus, HashMap params) throws RemoteException, AmsException;
    //RKAZI PPX-269 09/18/2013 Rel 8.4 Kyriba End
}
