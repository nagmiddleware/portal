package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;
/**
 * A business (coporate or bank) that is a party to an instrument.   The terms_party_type
 * attribute distinguishes between the different  types of parties that can
 * be associated to an instrument.
 * 
 * When a party is referenced in the Trade Portal by using the Party Search
 * page, data is copied from a 'Party' business object into a 'TermsParty'
 * business object.  Because of this, many of the attributes of this business
 * object are similar to the attributes of the Party business object.
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface TermsPartyHome extends EJBHome
{
   public TermsParty create()
      throws RemoteException, CreateException, AmsException;

   public TermsParty create(ClientServerDataBridge csdb)
      throws RemoteException, CreateException, AmsException;
}
