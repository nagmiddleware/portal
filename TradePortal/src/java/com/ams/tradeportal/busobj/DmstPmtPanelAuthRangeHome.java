
  

package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;
/**
 * A DmstPmtPanelAuthRange consists of a set of panel range for beneficiary
 * of payment transaction.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface DmstPmtPanelAuthRangeHome extends EJBHome
{
   public DmstPmtPanelAuthRange create()
      throws RemoteException, CreateException, AmsException;

   public DmstPmtPanelAuthRange create(ClientServerDataBridge csdb)
      throws RemoteException, CreateException, AmsException;
}
