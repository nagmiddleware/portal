
package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.ejb.EJBObject;

import com.ams.tradeportal.busobj.util.InstrumentServices;
import com.ams.tradeportal.common.InstrumentType;
import com.ams.tradeportal.common.TPDateTimeUtility;
import com.ams.tradeportal.common.TermsPartyType;
import com.ams.tradeportal.common.TradePortalConstants;
import com.ams.tradeportal.common.TransactionStatus;
import com.ams.tradeportal.common.TransactionType;
import com.amsinc.ecsg.frame.AmsConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.ClientServerDataBridge;
import com.amsinc.ecsg.frame.ComponentList;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.frame.ObjectIDCache;
import com.amsinc.ecsg.util.DateTimeUtility;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.GMTUtility;
import com.amsinc.ecsg.util.StringFunction;

/*
 *
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class TransactionBean extends TransactionBean_Base
{
private static final Logger LOG = LoggerFactory.getLogger(TransactionBean.class);

  /**
   *
   *
   * @param  doc
   * @return Terms
   */
   public Terms populateCustomerTerms(DocumentHandler doc) throws RemoteException
   {
	long lOid = 0;
	Terms terms = null;

	try {
		// Get the oid from the doc.  '0' indicates a new item so
		// change the mode.
		String oid = doc.getAttribute("/Terms/terms_oid");
		if (oid == null) {
			lOid = 0;
		} else {
			lOid = doc.getAttributeLong("/Terms/terms_oid");
		}

		if (lOid == 0) {
			lOid = newComponent("CustomerEnteredTerms");
		}

		terms = (Terms)getComponentHandle("CustomerEnteredTerms");

		terms.populateFromXmlDoc(doc);

	} catch (Exception e) {
		LOG.error("Exception trying to populate terms: " ,e);
	}

	return terms;

   }

  /**
   *
   *
   * @return boolean
   */
   public boolean canDeleteState() throws RemoteException, AmsException
   {
	String state = getAttribute("transaction_status");
	String rejected = getAttribute("rejection_indicator");
	if ((canAuthorizeState() || state.equals(TransactionStatus.STARTED)) &&
	    !rejected.equals(TradePortalConstants.INDICATOR_YES))
		return true;
	else
		return false;
   }

  /**
   *
   *
   * @return boolean
   */
   public boolean canRouteState() throws RemoteException, AmsException
   {
	String state = getAttribute("transaction_status");
	if (canAuthorizeState() ||
	    state.equals(TransactionStatus.STARTED))
		return true;
	else
		return false;
   }

  /**
   *
   *
   * @return boolean
   */
   public boolean canAuthorizeState() throws RemoteException, AmsException
   {
	String state = getAttribute("transaction_status");
	if (state.equals(TransactionStatus.AUTHORIZE_FAILED) ||
	    state.equals(TransactionStatus.FVD_AUTHORIZED) ||        //VS CR 609 11/23/10
	    state.equals(TransactionStatus.FX_THRESH_EXCEEDED) ||    //NSX CR-640/581 Rel 7.1.0  09/26/11
	    state.equals(TransactionStatus.AUTH_PEND_MARKET_RATE) || //NSX CR-640/581 Rel 7.1.0  09/26/11
	    state.equals(TransactionStatus.PARTIALLY_AUTHORIZED) ||
	    state.equals(TransactionStatus.READY_TO_AUTHORIZE))
		return true;
	else
		return false;
   }



	/**
	 * This method populates all the fields of a transaction by copying from
	 * an existing transaction.  It also creates new Terms and TermsParty objects
	 * and copies the Terms and TermsParty information from the source transaction.
	 * This method assumes that this transaction is the first transaction for its
	 * instrument and set the transaction type accordingly.
	 *
	 * @param source - com.ams.tradeportal.busobj.Transaction - The source transaction
	 * @param userOid - java.lang.String - the oid of the user creating the transaction
	 * @param corpOrgOid - java.lang.String - the oid of the user's corporate org
	 * @param instrumentType - java.lang.String - the type of the transaction's instrument
	 * @param ownedByTemplate - boolean - true if the transaction is owned by a template instrument
	 * @return void
	 */

	public void createNewCopy(Transaction source, String userOid, String corpOrgOid,
							  String instrumentType, boolean ownedByTemplate)
							 throws RemoteException, AmsException
	{
		String transType = null;

		LOG.debug("Creating a new transaction copy");
		// Convert the instrumentType to its physical type.  Thus,
		// DetailedSLC is SLC (even though it uses the GUA form).
		String physicalInstType = InstrumentServices.getPhysicalInstrumentType(instrumentType);
		LOG.debug("The instrument type being created is a: {} which is actually a: {}" , instrumentType,physicalInstType);

		//Copy the transaction data
		this.setAttribute("copy_of_amount",
			    		  source.getAttribute("copy_of_amount"));
		this.setAttribute("instrument_amount",
			    		  source.getAttribute("instrument_amount"));

		this.setAttribute("copy_of_currency_code",
						  source.getAttribute("copy_of_currency_code"));
		this.setAttribute("copy_of_instr_type_code",
						  physicalInstType);
		this.setAttribute("standby_using_guarantee_form",
						  source.getAttribute("standby_using_guarantee_form"));

        //IAZ CM-451 10/29/08, 12/14/08, 04/27/08 Begin:
        //Payment Date needs to be copied as well for Payment Instruments
        //Only perform this copy (set the date) for Transactions. No need to do this for Templates
        if ((instrumentType.equals(InstrumentType.DOM_PMT)
        		|| instrumentType.equals(InstrumentType.XFER_BET_ACCTS)
        		||instrumentType.equals(InstrumentType.FUNDS_XFER)
        		||instrumentType.equals(InstrumentType.DIRECT_DEBIT_INSTRUCTION))
        	&& (!ownedByTemplate))
        {
					this.setAttribute("payment_date",
						  source.getPaymentDate(userOid));
		}
		//IAZ CM-451 ** End

		// If the instrument type passed in is either DetailedSLC or
		// SLC_IS_GUA, then the standby_using_guarantee_form must be
		// reset to Y.
		if (instrumentType.equals(TradePortalConstants.DETAILED_SLC)
			|| instrumentType.equals(TradePortalConstants.SLC_IS_GUA)) {
			this.setAttribute("standby_using_guarantee_form",
							  TradePortalConstants.INDICATOR_YES);
			LOG.debug("This is logically a GUA but being saved as an SLC.");
			LOG.debug("   Setting standby_using_guarantee_form to Yes");
		}

		if (StringFunction.isBlank(this.getAttribute("standby_using_guarantee_form"))) {
			this.setAttribute("standby_using_guarantee_form", TradePortalConstants.INDICATOR_NO);
		}


		//Set the transaction type
		transType = InstrumentServices.getOriginalTransType(physicalInstType);
		LOG.debug("Setting transaction_type_code to -> {}" , transType);
		this.setAttribute("transaction_type_code",transType);

		LOG.debug("Creating terms.");
		long termsOid = this.newComponent("CustomerEnteredTerms");
		LOG.debug ("Getting handle with termsOid -> {}" , this.getAttribute("c_CustomerEnteredTerms"));
		Terms newTerms = (Terms) this.getComponentHandle("CustomerEnteredTerms");

		//Copy the terms data
		LOG.debug("Copying Terms data from sourceTerms with this oid -> {}" , source.getAttribute("c_CustomerEnteredTerms"));
		
		if(StringFunction.isNotBlank(source.getAttribute("c_CustomerEnteredTerms"))){
			Terms sourceTerms = (Terms) source.getComponentHandle("CustomerEnteredTerms");
			newTerms.copy(sourceTerms);
			//Leelavathi - IR#T36000015642 10/05/2013 CR-737- Rel8200 - Start
			if(sourceTerms.isAttributeRegistered("payment_type") && StringFunction.isNotBlank(sourceTerms.getAttribute("payment_type")) ){
				ComponentList sourcePmtTermsTenorDtlList = (ComponentList) sourceTerms.getComponentHandle("PmtTermsTenorDtlList");
				int pmtTermsCount = sourcePmtTermsTenorDtlList.getObjectCount();
				for (int pmtTermsIndex = 0; pmtTermsIndex < pmtTermsCount; pmtTermsIndex++){
					sourcePmtTermsTenorDtlList.scrollToObjectByIndex(pmtTermsIndex);
					PmtTermsTenorDtl newPmtTermsTenorDtl = newTerms.createPmtTermsTenorDtl();
					newPmtTermsTenorDtl.setAttribute("amount",sourcePmtTermsTenorDtlList.getListAttribute("amount"));
					newPmtTermsTenorDtl.setAttribute("percent",sourcePmtTermsTenorDtlList.getListAttribute("percent"));
					newPmtTermsTenorDtl.setAttribute("num_days_after",sourcePmtTermsTenorDtlList.getListAttribute("num_days_after"));
					newPmtTermsTenorDtl.setAttribute("maturity_date",sourcePmtTermsTenorDtlList.getListAttribute("maturity_date"));
					newPmtTermsTenorDtl.setAttribute("tenor_type",sourcePmtTermsTenorDtlList.getListAttribute("tenor_type"));
					newPmtTermsTenorDtl.setAttribute("days_after_type",sourcePmtTermsTenorDtlList.getListAttribute("days_after_type"));
					newPmtTermsTenorDtl.setAttribute("terms_oid",newTerms.getAttribute("terms_oid"));	
					newPmtTermsTenorDtl.save();
				}
			}
			//Leelavathi - IR#T36000018887 28/10/2013 CR-737- Rel8400 - Begin
			/*commented below code and added sort method to get correct order of documents when we copy from existing instrument */
			//Leelavathi - IR#T36000015642 10/05/2013 CR-737- Rel8200 - End
			//Leelavathi - IR#T36000018142 13/06/2013 CR-737- Rel8200 � Begin
			ComponentList sourceAdditionalReqDocList = (ComponentList) sourceTerms.getComponentHandle("AdditionalReqDocList");			
			List sortedAdditionalReqDocList = new ArrayList();
			sortedAdditionalReqDocList = sortAddlReqDocs(sourceAdditionalReqDocList);
			int additionalReqDocCount = (sortedAdditionalReqDocList == null) ? 0: sortedAdditionalReqDocList.size();
           for (int additionalReqDocIndex = 0; additionalReqDocIndex < additionalReqDocCount; additionalReqDocIndex++){
        	   AdditionalReqDoc additionalReqDoc = (AdditionalReqDoc)sourceAdditionalReqDocList
        	   													.getComponentObject(
        	   																	Long.parseLong(sortedAdditionalReqDocList.
        	   																			get(additionalReqDocIndex).toString()));
               AdditionalReqDoc newadditionalReqDocs = newTerms.createAdditionalReqDoc();
               newadditionalReqDocs.setAttribute("addl_req_doc_ind",additionalReqDoc.getAttribute("addl_req_doc_ind"));
               newadditionalReqDocs.setAttribute("addl_req_doc_name",additionalReqDoc.getAttribute("addl_req_doc_name"));
               newadditionalReqDocs.setAttribute("addl_req_doc_originals", additionalReqDoc.getAttribute("addl_req_doc_originals"));
               newadditionalReqDocs.setAttribute("addl_req_doc_copies", additionalReqDoc.getAttribute("addl_req_doc_copies"));
               newadditionalReqDocs.setAttribute("addl_req_doc_text", additionalReqDoc.getAttribute("addl_req_doc_text"));
               newadditionalReqDocs.setAttribute("terms_oid",newTerms.getAttribute("terms_oid"));   
               newadditionalReqDocs.save();
	                        }
			         //Leelavathi - IR#T36000018142 13/06/2013 CR-737- Rel8200 � End  
           //Leelavathi - IR#T36000018887 28/10/2013 CR-737- Rel8400 - End
		}else{
			this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.FAILED_TO_COPY_INSTRUMENT);
		}

		if (!ownedByTemplate)
		{
			//These fields only make sense for transactions that belong to real instrument not templates
			this.setAttribute("assigned_to_user_oid", userOid);
			this.setAttribute("last_entry_user_oid", userOid);
			//rbhaduri - 8th August 06 - IR CGUG071768901 - commented out and put outside 'if' logic
		}
		else
		{
			//Vshah - 10/07/2010 - VIUK100640356 - [Add Below "if" condition]
			//Only set confidential_indicator for CM instrument types (viz Payment, TBA and inl Payment)
			if (instrumentType.equals(InstrumentType.DOM_PMT) ||
					instrumentType.equals(InstrumentType.XFER_BET_ACCTS)	||
					instrumentType.equals(InstrumentType.FUNDS_XFER))
			{
				//IAZ CR-586 IR-PRUK092452162 09/29/10 Begin
				if (!TradePortalConstants.CONF_IND_FRM_TMPLT.equals(newTerms.getAttribute("confidential_indicator")))
					newTerms.setAttribute("confidential_indicator", TradePortalConstants.INDICATOR_NO);
				LOG.debug ("corected conf ind in temrs to {}" , newTerms.getAttribute("confidential_indicator"));
				//IAZ CR-586 IR-PRUK092452162 09/29/10 End

			//MDB Rel 8.1 5/10/12 LMUM011342545 - Begin - remove values from CM templates copied from existing instrument
			newTerms.setAttribute("transfer_fx_rate", null); //fx rate
			newTerms.setAttribute("display_fx_rate_method", null); //calc method
			newTerms.setAttribute("equivalent_exch_amount", null); //equivalent amount
			//MDB Rel 8.1 5/10/12 LMUM011342545 - End
			}
			
		}
		//IR 23618 Start - while creating template using existing instrument do not copy invoice/po details
		if(instrumentType.equals(InstrumentType.APPROVAL_TO_PAY) || instrumentType.equals(InstrumentType.LOAN_RQST)){
			newTerms.setAttribute("invoice_only_ind", null); 
			newTerms.setAttribute("invoice_due_date", null); 
			newTerms.setAttribute("invoice_details", null); 
		}
		
		//IR 23618 End
		
		//rbhaduri - 8th August 06 - IR CGUG071768901 - Now this needs to be done even for template isntruments
		//for templates creted by corp users
		if (!corpOrgOid.equals(TradePortalConstants.OWNER_TYPE_ADMIN))
			copyCorpDataToTerms(corpOrgOid, physicalInstType, newTerms);

		return;
	}

	//Leelavathi - IR#T36000018887 - Rel8400 CR-737 28/10/2013- Start
	public List sortAddlReqDocs(ComponentList additionalReqDocList) throws AmsException, RemoteException
	{
		String           addlReqDocOid;
		List           addlReqDocSortedList = new ArrayList();
		int addRedDocCount = additionalReqDocList.getObjectCount();
		for (int i =0 ; i < addRedDocCount ; i++)
		{
			additionalReqDocList.scrollToObjectByIndex(i);
			addlReqDocOid   = additionalReqDocList.getListAttribute("addl_req_doc_oid");
			addlReqDocSortedList.add(addlReqDocOid);
		}
		Collections.sort(addlReqDocSortedList);
		return addlReqDocSortedList;
	}
	//Leelavathi - IR#T36000018887 - Rel8400 CR-737 28/10/2013- End
	/**
	 *
	 * @param transType - java.lang.String - The type of this transaction
	 * @param userOid - java.lang.String - the oid of the user creating the transaction
	 * @param corpOrgOid - java.lang.String - the oid of the user's corporate org
	 * @param instrumentType - java.lang.String - the type of the transaction's instrument
	 * @param ownedByTemplate - boolean - true if the transaction is owned by a template instrument
	 * @param messageOid - java.lang.String
	 * @return void
	 */

	public void createNewBlank(String transType, String userOid, String corpOrgOid, String instrumentType,
							   boolean ownedByTemplate, String messageOid) throws RemoteException, AmsException
	{
	    this.createNewBlank(transType, userOid, corpOrgOid, instrumentType, ownedByTemplate, messageOid, "");
    }

	/**
	 *
	 * @param transType - java.lang.String - The type of this transaction
	 * @param userOid - java.lang.String - the oid of the user creating the transaction
	 * @param corpOrgOid - java.lang.String - the oid of the user's corporate org
	 * @param instrumentType - java.lang.String - the type of the transaction's instrument
	 * @param ownedByTemplate - boolean - true if the transaction is owned by a template instrument
	 * @param messageOid - java.lang.String
	 * @param relatedInstrID - java.lang.String - the id of the related instrument for a tranferred export lc
	 * @return void
	 */

	public void createNewBlank(String transType, String userOid, String corpOrgOid, String instrumentType,
							   boolean ownedByTemplate, String messageOid, String relatedInstrID)
							   throws RemoteException, AmsException
	{
          createNewBlank(transType, userOid, corpOrgOid, instrumentType, ownedByTemplate, messageOid, relatedInstrID, null);
        }

	/**
	 *
	 * @param transType - java.lang.String - The type of this transaction
	 * @param userOid - java.lang.String - the oid of the user creating the transaction
	 * @param corpOrgOid - java.lang.String - the oid of the user's corporate org
	 * @param instrumentType - java.lang.String - the type of the transaction's instrument
	 * @param ownedByTemplate - boolean - true if the transaction is owned by a template instrument
	 * @param messageOid - java.lang.String
	 * @param relatedInstrID - java.lang.String - the id of the related instrument for a tranferred export lc
	 * @param poLineItem - com.ams.tradeportal.busobj.POLineItem - the PO to be used to generate the goods description
	 * @return void
	 */

	public void createNewBlank(String transType, String userOid, String corpOrgOid, String instrumentType,
							   boolean ownedByTemplate, String messageOid, String relatedInstrID, POLineItem poLineItem)
							   throws RemoteException, AmsException
	{
		// Convert the instrumentType to its physical type.  Thus,
		// DetailedSLC is SLC (even though it uses the GUA form).
		String physicalInstType = InstrumentServices.getPhysicalInstrumentType(instrumentType);
		LOG.debug("The instrument type being created is a: {} which is actually a: {}" , instrumentType,physicalInstType);

		// If the instrument type passed in is either DetailedSLC or
		// SLC_IS_GUA, then the standby_using_guarantee_form must be
		// reset to Y.
		if (instrumentType.equals(TradePortalConstants.DETAILED_SLC)
			|| instrumentType.equals(TradePortalConstants.SLC_IS_GUA)) {
			this.setAttribute("standby_using_guarantee_form",
							  TradePortalConstants.INDICATOR_YES);
			LOG.debug("This is logically a GUA but being saved as an SLC.");
			LOG.debug("   Setting standby_using_guarantee_form to Yes");
		}

		if (StringFunction.isBlank(this.getAttribute("standby_using_guarantee_form"))) {
			this.setAttribute("standby_using_guarantee_form", TradePortalConstants.INDICATOR_NO);
		}

		//Set the transaction type
		LOG.debug("Setting transaction_type_code to -> {}" , transType);
		this.setAttribute("transaction_type_code",transType);

		LOG.debug("Setting attributes on the new transaction");
		this.setAttribute("copy_of_instr_type_code", physicalInstType);

		LOG.debug("Creating terms.");
		long termsOid = this.newComponent("CustomerEnteredTerms");
		LOG.debug ("Getting handle with termsOid -> {}" , this.getAttribute("c_CustomerEnteredTerms"));
		Terms newTerms = (Terms) this.getComponentHandle("CustomerEnteredTerms");

		if (!ownedByTemplate)
		{
			//These fields only make sense for transactions that belong to real instrument not templates
			this.setAttribute("assigned_to_user_oid", userOid);
			this.setAttribute("last_entry_user_oid", userOid);
			//rbhaduri - 8th August 06 - IR CGUG071768901 - commented out and put outside 'if' logic
		}


		//rbhaduri - 8th August 06 - IR CGUG071768901 - Now this needs to be done even for template isntruments
		//for templates creted by corp users
		if (!corpOrgOid.equals(TradePortalConstants.OWNER_TYPE_ADMIN))
			copyCorpDataToTerms(corpOrgOid, physicalInstType, newTerms);


		//Added for discrepancies,ATP-Responses - START
                //krishna CR 375-D ATP-Response
                //Inserted ||transType.equals(TradePortalConstants.APPROVAL_TO_PAY_RESPONSE) for ATP-Response
		if ( transType.equals(TransactionType.DISCREPANCY)||transType.equals(TransactionType.APPROVAL_TO_PAY_RESPONSE) )
		{
			MailMessage msg = (MailMessage) createServerEJB ("MailMessage", Long.parseLong(messageOid));

			String disc_amount = msg.getAttribute("discrepancy_amount");
			String disc_currency = msg.getAttribute("discrepancy_currency_code");
			String seq_number = msg.getAttribute("sequence_number");
			String work_item_number = msg.getAttribute("work_item_number");

			// Each of these attributes must be created in the terms managers for the discrepancy
			// transactions
			newTerms.setAttribute("amount", disc_amount);
			newTerms.setAttribute("amount_currency_code", disc_currency);
			newTerms.setAttribute("drawing_number", seq_number);
			newTerms.setAttribute("work_item_number", work_item_number);
			this.setAttribute("copy_of_amount", disc_amount);
			this.setAttribute("instrument_amount", disc_amount);
			this.setAttribute("copy_of_currency_code", disc_currency);
			// Nar IR-T36000045155 rel9400 01/28/2015 - save default date as current business date if settlement include enable for corporate org.
			if( TransactionType.DISCREPANCY.equals(transType) && StringFunction.isNotBlank(corpOrgOid)) {
				String sql = "SELECT include_settle_instruction FROM corporate_org WHERE organization_oid = ?";
				DocumentHandler doc = DatabaseQueryBean.getXmlResultSet( sql, true, corpOrgOid );
				if ( doc != null ) {
					String settlementFlag   = doc.getAttribute("/ResultSetRecord(0)/INCLUDE_SETTLE_INSTRUCTION");
					if ( TradePortalConstants.INDICATOR_YES.equals(settlementFlag) ) {
						newTerms.setAttribute("apply_payment_on_date", DateTimeUtility.getGMTDateTime());
					}
				}
				
			}
		} else if ( TransactionType.SIR.equals(transType)) {
			MailMessage msg = (MailMessage) createServerEJB ("MailMessage", Long.parseLong(messageOid));
			String fundingAmount     = msg.getAttribute("funding_amount");
			String fundingCurrency   = msg.getAttribute("funding_currency");
			String seqNumber         = msg.getAttribute("sequence_number");
			String workItemNumber    = msg.getAttribute("work_item_number");
			// Each of these attributes must be created in the terms managers for the discrepancy
			// transactions
			newTerms.setAttribute("amount", fundingAmount);
			newTerms.setAttribute("amount_currency_code", fundingCurrency);
			newTerms.setAttribute("drawing_number", seqNumber);
			newTerms.setAttribute("work_item_number", workItemNumber);
			newTerms.setAttribute("apply_payment_on_date", DateTimeUtility.getGMTDateTime());
			this.setAttribute("copy_of_amount", fundingAmount);
			this.setAttribute("instrument_amount", fundingAmount);
			this.setAttribute("copy_of_currency_code", fundingCurrency);
		} else if ( TransactionType.SIM.equals(transType) ) {
			newTerms.setAttribute("apply_payment_on_date", DateTimeUtility.getGMTDateTime());
			String sql = "SELECT i.copy_of_instrument_amount amount, t.copy_of_currency_code currency FROM instrument i, transaction t" +
                               " WHERE t.transaction_oid = i.original_transaction_oid AND i.instrument_oid = ? ";
			DocumentHandler doc = DatabaseQueryBean.getXmlResultSet( sql, true, this.getAttribute("instrument_oid") );
			if ( doc != null ) {
				String amount   = doc.getAttribute("/ResultSetRecord(0)/AMOUNT");
				String currency = doc.getAttribute("/ResultSetRecord(0)/CURRENCY");
				
				newTerms.setAttribute("amount", amount);
				newTerms.setAttribute("amount_currency_code", currency);
				
				this.setAttribute("copy_of_amount", amount);
				this.setAttribute("instrument_amount", amount);
				this.setAttribute("copy_of_currency_code", currency);
			}
		}
		//Added for discrepancies,ATP-Responses - END

		if (transType.equals(TransactionType.TRANSFER))
			newTerms.setAttribute("related_instrument_id", relatedInstrID);
		//IR T36000019095 start - create shipment terms when AMD activity is started else while adding POs there will be invalid association error
		ShipmentTerms shipmentTerms = null;
		if ((TransactionType.AMEND.equals(transType) && (InstrumentType.APPROVAL_TO_PAY.equals(instrumentType) || InstrumentType.IMPORT_DLC.equals(instrumentType))) || (poLineItem != null)){
		 shipmentTerms = newTerms.getFirstShipment();
		

        if(shipmentTerms == null)
            shipmentTerms = newTerms.createFirstShipment();
		}
		//IR T36000019095 end
                // Set the po_line_items field of the terms if there are POs passed in
                if (poLineItem != null)
                 {
			 //set the goods description
			 LOG.debug("Setting Purchase Order Goods Description");
			//IR T36000019095 -removed newTerms.createFirstShipment()
			 shipmentTerms.setPOGoodsDescription(poLineItem);

                         // Append text onto the end of the goods description text
                         // (since this is called for an amendment)
		         String goodsDescription = shipmentTerms.getAttribute("goods_description");

        		 String textToAdd = getResourceManager().getText("AddPOLineItems.POItemsAdded",
                                                                   TradePortalConstants.TEXT_BUNDLE);
		         int index     = StringFunction.isNotBlank(goodsDescription)?goodsDescription.indexOf(textToAdd):-1;

		         if (index < 0)
		         {
		            if (StringFunction.isNotBlank(goodsDescription))
		            {
		               goodsDescription += "\n\n";
		            }

		            goodsDescription += textToAdd;

		            shipmentTerms.setAttribute("goods_description", goodsDescription);
	                 }


	         }

		return;
	}

    /**
     *
     * @param transType - java.lang.String - The type of this transaction
     * @param userOid - java.lang.String - the oid of the user creating the transaction
     * @param corpOrgOid - java.lang.String - the oid of the user's corporate org
     * @param instrumentType - java.lang.String - the type of the transaction's instrument
     * @param ownedByTemplate - boolean - true if the transaction is owned by a template instrument
     * @param messageOid - java.lang.String
     * @param relatedInstrID - java.lang.String - the id of the related instrument for a tranferred export lc
     * @param poLineItem - com.ams.tradeportal.busobj.POLineItem - the PO to be used to generate the goods description
     * @return void
     */

    public void createNewBlankStructPOTran(String transType, String userOid, String corpOrgOid, String instrumentType,
                               boolean ownedByTemplate, String messageOid, String relatedInstrID, PurchaseOrder poLineItem)
            throws RemoteException, AmsException
    {
        // Convert the instrumentType to its physical type.  Thus,
        // DetailedSLC is SLC (even though it uses the GUA form).
        String physicalInstType = InstrumentServices.getPhysicalInstrumentType(instrumentType);
        LOG.debug("The instrument type being created is a: {} which is actually a: {}" , instrumentType,physicalInstType);

        // If the instrument type passed in is either DetailedSLC or
        // SLC_IS_GUA, then the standby_using_guarantee_form must be
        // reset to Y.
        if (instrumentType.equals(TradePortalConstants.DETAILED_SLC)
                || instrumentType.equals(TradePortalConstants.SLC_IS_GUA)) {
            this.setAttribute("standby_using_guarantee_form",
                    TradePortalConstants.INDICATOR_YES);
            LOG.debug("This is logically a GUA but being saved as an SLC.");
            LOG.debug("   Setting standby_using_guarantee_form to Yes");
        }

        if (StringFunction.isBlank(this.getAttribute("standby_using_guarantee_form"))) {
            this.setAttribute("standby_using_guarantee_form", TradePortalConstants.INDICATOR_NO);
        }

        //Set the transaction type
        LOG.debug("Setting transaction_type_code to -> {}" , transType);
        this.setAttribute("transaction_type_code",transType);

        LOG.debug("Setting attributes on the new transaction");
        this.setAttribute("copy_of_instr_type_code", physicalInstType);

        LOG.debug("Creating terms.");
        long termsOid = this.newComponent("CustomerEnteredTerms");
        LOG.debug ("Getting handle with termsOid -> {}" , this.getAttribute("c_CustomerEnteredTerms"));
        Terms newTerms = (Terms) this.getComponentHandle("CustomerEnteredTerms");

        if (!ownedByTemplate)
        {
            //These fields only make sense for transactions that belong to real instrument not templates
            this.setAttribute("assigned_to_user_oid", userOid);
            this.setAttribute("last_entry_user_oid", userOid);
            //rbhaduri - 8th August 06 - IR CGUG071768901 - commented out and put outside 'if' logic
        }


        //rbhaduri - 8th August 06 - IR CGUG071768901 - Now this needs to be done even for template isntruments
        //for templates creted by corp users
        if (!corpOrgOid.equals(TradePortalConstants.OWNER_TYPE_ADMIN))
            copyCorpDataToTerms(corpOrgOid, physicalInstType, newTerms);


        //Added for discrepancies,ATP-Responses - START
        //krishna CR 375-D ATP-Response
        //Inserted ||transType.equals(TradePortalConstants.APPROVAL_TO_PAY_RESPONSE) for ATP-Response
        if (transType.equals(TransactionType.DISCREPANCY)||transType.equals(TransactionType.APPROVAL_TO_PAY_RESPONSE))
        {
            MailMessage msg = (MailMessage) createServerEJB ("MailMessage", Long.parseLong(messageOid));

            String disc_amount = msg.getAttribute("discrepancy_amount");
            String disc_currency = msg.getAttribute("discrepancy_currency_code");
            String seq_number = msg.getAttribute("sequence_number");
            String work_item_number = msg.getAttribute("work_item_number");

            // Each of these attributes must be created in the terms managers for the discrepancy
            // transactions
            newTerms.setAttribute("amount", disc_amount);
            newTerms.setAttribute("amount_currency_code", disc_currency);
            newTerms.setAttribute("drawing_number", seq_number);
            newTerms.setAttribute("work_item_number", work_item_number);
            this.setAttribute("copy_of_amount", disc_amount);
            this.setAttribute("instrument_amount", disc_amount);
            this.setAttribute("copy_of_currency_code", disc_currency);
        }
        //Added for discrepancies,ATP-Responses - END

        if (transType.equals(TransactionType.TRANSFER))
            newTerms.setAttribute("related_instrument_id", relatedInstrID);

        // Set the po_line_items field of the terms if there are POs passed in
        if (poLineItem != null)
        {
            //set the goods description
            LOG.debug("Setting Purchase Order Goods Description");

            ShipmentTerms shipmentTerms = newTerms.getFirstShipment();

            if(shipmentTerms == null)
                shipmentTerms = newTerms.createFirstShipment();
            
            // Append text onto the end of the goods description text
            // (since this is called for an amendment)
            String goodsDescription = shipmentTerms.getAttribute("goods_description");

            String textToAdd = getResourceManager().getText("AddPOLineItems.POItemsAdded",
                    TradePortalConstants.TEXT_BUNDLE);
            int index     = StringFunction.isNotBlank(goodsDescription)?goodsDescription.indexOf(textToAdd):-1;

            if (index < 0)
            {
                if (StringFunction.isNotBlank(goodsDescription))
                {
                    goodsDescription += "\n\n";
                }

                goodsDescription += textToAdd;

                shipmentTerms.setAttribute("goods_description", goodsDescription);
            }


        }

        return;
    }

    /**
     *
     * @param transType - java.lang.String - The type of this transaction
     * @param userOid - java.lang.String - the oid of the user creating the transaction
     * @param corpOrgOid - java.lang.String - the oid of the user's corporate org
     * @param instrumentType - java.lang.String - the type of the transaction's instrument
     * @param ownedByTemplate - boolean - true if the transaction is owned by a template instrument
     * @param messageOid - java.lang.String
     * @param relatedInstrID - java.lang.String - the id of the related instrument for a tranferred export lc
     * @param invoicesSummaryData - com.ams.tradeportal.busobj.InvoicesSummaryData - the Invoice to be used
     * @return void
     */

    public void createNewBlankInvTran(String transType, String userOid, String corpOrgOid, String instrumentType,
                                           boolean ownedByTemplate, String messageOid, String relatedInstrID, InvoicesSummaryData invoicesSummaryData)
            throws RemoteException, AmsException
    {
        // Convert the instrumentType to its physical type.  Thus,
        // DetailedSLC is SLC (even though it uses the GUA form).
        String physicalInstType = InstrumentServices.getPhysicalInstrumentType(instrumentType);
        LOG.debug("The instrument type being created is a: {} which is actually a: {}" , instrumentType,physicalInstType);

        // If the instrument type passed in is either DetailedSLC or
        // SLC_IS_GUA, then the standby_using_guarantee_form must be
        // reset to Y.
        if (instrumentType.equals(TradePortalConstants.DETAILED_SLC)
                || instrumentType.equals(TradePortalConstants.SLC_IS_GUA)) {
            this.setAttribute("standby_using_guarantee_form",
                    TradePortalConstants.INDICATOR_YES);
            LOG.debug("This is logically a GUA but being saved as an SLC.");
            LOG.debug("   Setting standby_using_guarantee_form to Yes");
        }

        if (StringFunction.isBlank(this.getAttribute("standby_using_guarantee_form"))) {
            this.setAttribute("standby_using_guarantee_form", TradePortalConstants.INDICATOR_NO);
        }

        //Set the transaction type
        LOG.debug("Setting transaction_type_code to -> {}" , transType);
        this.setAttribute("transaction_type_code",transType);

        LOG.debug("Setting attributes on the new transaction");
        this.setAttribute("copy_of_instr_type_code", physicalInstType);

        LOG.debug("Creating terms.");
        long termsOid = this.newComponent("CustomerEnteredTerms");
        LOG.debug ("Getting handle with termsOid -> {}" , this.getAttribute("c_CustomerEnteredTerms"));
        Terms newTerms = (Terms) this.getComponentHandle("CustomerEnteredTerms");

        if (!ownedByTemplate)
        {
            //These fields only make sense for transactions that belong to real instrument not templates
            this.setAttribute("assigned_to_user_oid", userOid);
            this.setAttribute("last_entry_user_oid", userOid);
        }


        if (!corpOrgOid.equals(TradePortalConstants.OWNER_TYPE_ADMIN))
            copyCorpDataToTerms(corpOrgOid, physicalInstType, newTerms);


        if (transType.equals(TransactionType.DISCREPANCY)||transType.equals(TransactionType.APPROVAL_TO_PAY_RESPONSE))
        {
            MailMessage msg = (MailMessage) createServerEJB ("MailMessage", Long.parseLong(messageOid));

            String disc_amount = msg.getAttribute("discrepancy_amount");
            String disc_currency = msg.getAttribute("discrepancy_currency_code");
            String seq_number = msg.getAttribute("sequence_number");
            String work_item_number = msg.getAttribute("work_item_number");

            // Each of these attributes must be created in the terms managers for the discrepancy
            // transactions
            newTerms.setAttribute("amount", disc_amount);
            newTerms.setAttribute("amount_currency_code", disc_currency);
            newTerms.setAttribute("drawing_number", seq_number);
            newTerms.setAttribute("work_item_number", work_item_number);
            this.setAttribute("copy_of_amount", disc_amount);
            this.setAttribute("instrument_amount", disc_amount);
            this.setAttribute("copy_of_currency_code", disc_currency);
        }

        if (transType.equals(TransactionType.TRANSFER))
            newTerms.setAttribute("related_instrument_id", relatedInstrID);


        return;
    }

    /*
   * Perform processing for a New instance of the business object
   */
   protected void userNewObject() throws AmsException, RemoteException
   {
	  /* Perform any New Object processing defined in the Ancestor class */
	  super.userNewObject();

	  //Default value for transaction_status_date is the current GMT date
	  String date = DateTimeUtility.getGMTDateTime();
	  this.setAttribute("transaction_status_date", date);

	  //Default value for transaction_status
	  this.setAttribute("transaction_status", TransactionStatus.STARTED);

	  /* Default value for show_on_notifications_tab */
	  this.setAttribute("show_on_notifications_tab", TradePortalConstants.INDICATOR_NO);

	  /* Default value for standby_using_guarantee_form */
	  this.setAttribute("standby_using_guarantee_form", TradePortalConstants.INDICATOR_NO);

          // TLE - 09/25/06 - CR382 - Add Begin
          // Set value for date_started to the current GMT date
	  this.setAttribute("date_started", date);
          // TLE - 09/25/06 - CR382 - Add Begin
   }




	/**
	 * Places the instrument type, transaction type, and component identifier into
	 * the client-server data bridge.
	 *
	 * @param componentIdentifier String - the component identifier
	 */
	private void placeDataInCSDB(String componentIdentifier) throws AmsException, RemoteException

	 {
	   if(getClientServerDataBridge() == null)
	      LOG.info("Client-Server Data Bridge is null.  CSDB must be populated in order to access Terms data from a Transaction.");

	   getClientServerDataBridge().setCSDBValue("instrument_type",this.getAttribute("copy_of_instr_type_code"));
	   getClientServerDataBridge().setCSDBValue("transaction_type",this.getAttribute("transaction_type_code"));
	   getClientServerDataBridge().setCSDBValue("standby_using_guarantee_form", this.getAttribute("standby_using_guarantee_form"));
	   getClientServerDataBridge().setCSDBValue("component_id",componentIdentifier);
	 }

	/**
	 *  Overrides method in BusinessObjectBean.  This makes sure that the needed values
	 *  are placed into the CSDB before the component is created.  The component may
	 *  then use the values to determine which attributes to register.
	 *
	 *  @param componentIdentifier String - the name of the component
	 *  @return long - the OID of the new component
	 */
	public long newComponent (String componentIdentifier)
		   throws AmsException, RemoteException
	 {
	  placeDataInCSDB(componentIdentifier);
	  return super.newComponent(componentIdentifier);
	 }

	/**
	 *  Overrides method in BusinessObjectBean.  This makes sure that the needed values
	 *  are placed into the CSDB before the component is deleted.  The component may
	 *  then use the values to determine which attributes to register.
	 *
	 *  @param componentIdentifier String - the name of the component
	 *  @return int - success flag
	 */
	public int deleteComponent (String componentIdentifier)
		   throws AmsException, RemoteException
	 {
	  placeDataInCSDB(componentIdentifier);
	  return super.deleteComponent(componentIdentifier);
	 }

	/**
	 *  Overrides method in BusinessObjectBean.  This makes sure that the needed values
	 *  are placed into the CSDB before the component is gotten.  The component may
	 *  then use the values to determine which attributes to register.
	 *
	 *  @param componentIdentifier String - the name of the component
	 *  @return EJBObject - the component
	 */
	public EJBObject getComponentHandle (String componentIdentifier)
		   throws RemoteException, AmsException
	{
	  placeDataInCSDB(componentIdentifier);
	  return super.getComponentHandle (componentIdentifier);
	}

	/**
	 *  Overrides method in BusinessObjectBean.  This makes sure that the needed values
	 *  are placed into the CSDB before the component is gotten.  The component may
	 *  then use the values to determine which attributes to register.
	 *
	 *  @param componentIdentifier String - the name of the component
	 *  @param objectId long - the OID of the component to get
	 *  @return EJBObject - the component
	 */
	public EJBObject getComponentHandle (String componentIdentifier, long objectId)
		   throws RemoteException, AmsException
 	{
	  placeDataInCSDB(componentIdentifier);
	  return super.getComponentHandle (componentIdentifier, objectId);
	  }


	/**
	 * copyCorpDataToTerms (String corpOrgOid, Terms newTerms)
	 *
	 * this method copies the data from the user's
	 */
	private void copyCorpDataToTerms (String corpOrgOid, String instrumentType, Terms newTerms) throws AmsException, RemoteException
	{
//KMehta Start Rel 9.0 IR 20002 		
		TermsParty  powParty = newTerms.getTermsPartyByPartyType(TermsPartyType.PAYMENT_OWNER); 
		if (powParty !=null) {
		    String payerPartyType = TermsPartyType.APPLICANT; //ftdp, ftba
		    if ( InstrumentType.FUNDS_XFER.equals(instrumentType)) { //ftrq
		       payerPartyType = TermsPartyType.PAYER;
		    }
		  powParty.setAttribute("terms_party_type", payerPartyType);
		}
//KMehta End Rel 9.0 IR 20002
    	if (instrumentType.equals(InstrumentType.EXPORT_DLC))
    	{
    		LOG.debug("Skipping create because this is an EXP_DLC instrument");
    	} 
    	else
    	{
    		//Get a handle to the corpOrg
    		CorporateOrganization corp = (CorporateOrganization)
    			createServerEJB("CorporateOrganization");
    		corp.getData(Long.parseLong(corpOrgOid));

    		if(!instrumentType.equals(InstrumentType.REQUEST_ADVISE))
    		{ // IMP_DLC, etc...

    			//T36000034504 -Start- If the source term from which the newTerms has been copied, had the secondTermParty,
				// overwrite it with latest data from corporate organization. This normally 'Copy from existing' scenario
				if (StringFunction.isNotBlank(newTerms.getAttribute("c_SecondTermsParty")))
				{ // and the second terms party (Applicant) is blank

					TermsParty newTermsParty = (TermsParty)newTerms.getComponentHandle("SecondTermsParty");
 					//Copy the data from the corpOrg to the terms party
					newTermsParty.populateFirstTermsParty(corp, instrumentType);
 				}//T36000034504-End

				if (StringFunction.isBlank(newTerms.getAttribute("c_SecondTermsParty")))
				{ // and the second terms party (Applicant) is blank
				  // This normally 'New instrument ' scenario

					//Create terms party from user's corpOrg
					LOG.debug("Creating second terms party from corpOrgOid -> {}" , corpOrgOid);

					//Create the new terms party
					long termsPartyOid = newTerms.newComponent("SecondTermsParty");
					TermsParty newTermsParty = (TermsParty)
							newTerms.getComponentHandle("SecondTermsParty");

					//Copy the data from the corpOrg to the terms party
					newTermsParty.populateFirstTermsParty(corp, instrumentType);
				} else if (checkAppParty(corp, (TermsParty)
						newTerms.getComponentHandle("SecondTermsParty")) && InstrumentType.DOM_PMT.equals(instrumentType)){
					TermsParty newTermsParty = (TermsParty)
							newTerms.getComponentHandle("SecondTermsParty");
					//Copy the data from the corpOrg to the terms party
					newTermsParty.populateFirstTermsParty(corp, instrumentType);

				}

				// CR-451 NShrestha 01/23/2009 IR#ALUJ012244013 Begin
				if (instrumentType.equals(InstrumentType.XFER_BET_ACCTS)) {
					if (StringFunction.isBlank(newTerms.getAttribute("c_FirstTermsParty"))) {
						LOG.debug("Creating second terms party from corpOrgOid -> {}" , corpOrgOid);

						//Create the new terms party
						long termsPartyOid = newTerms.newComponent("FirstTermsParty");
						TermsParty newTermsParty = (TermsParty)
								newTerms.getComponentHandle("FirstTermsParty");

						//Copy the data from the corpOrg to the terms party
						newTermsParty.populateFirstTermsParty(corp, instrumentType);
						newTermsParty.setAttribute("terms_party_type", TermsPartyType.PAYEE);
					}
				}
				// CR-451 NShrestha 01/23/2009 IR#ALUJ012244013 End
 

    		} // if(!instrumentType.equals(TradePortalConstants.REQUEST_ADVISE))
    		else // Request to Advise Sixth Terms Party
    		{
    			// If the sixth terms party is blank
    			if(StringFunction.isBlank(newTerms.getAttribute("c_SixthTermsParty")))
    			{
    				LOG.debug("Creating sixth terms party from corpOrgOid -> {}" , corpOrgOid);
    				// Create the sixth terms party
    				long sixthTermsPartyOid = newTerms.newComponent("SixthTermsParty");
    				TermsParty sixthTermsParty = (TermsParty)
    				newTerms.getComponentHandle("SixthTermsParty");
    				sixthTermsParty.populateSixthTermsParty(corp, instrumentType);
    			}
    		}
    	}
	}

	private boolean checkAppParty(CorporateOrganization corp, TermsParty termsParty) throws RemoteException, AmsException {
		String partyName = termsParty.getAttribute("name");
		if (StringFunction.isNotBlank(partyName)){
			partyName = partyName.trim();
		}
		String corpName = corp.getAttribute("name");
		if (StringFunction.isNotBlank(partyName)){
			corpName = corpName.trim();
		}else{
			corpName = ""; // to avoid null pointer exception
		}
		
 		return !corpName.equals(partyName);
	}

	/**
	 * Removes all POs from a transaction by looping through its shipments
	 */
	public void unassignPOAssignment() throws RemoteException, AmsException
         {
  		           Terms terms = (Terms)getComponentHandle("CustomerEnteredTerms");
			   ComponentList shipmentTermsList = (ComponentList) terms.getComponentHandle("ShipmentTermsList");
			   if (shipmentTermsList != null)
			    {
				int numberShipmentTerms = shipmentTermsList.getObjectCount();
				for (int i = 0; i < numberShipmentTerms; i++)
				{
					shipmentTermsList.scrollToObjectByIndex(i);
					ShipmentTerms shipmentTerms = (ShipmentTerms) shipmentTermsList.getBusinessObject();
                    shipmentTerms.removeAssignedPOs(this.getAttribute("instrument_oid"), this.getAttribute("transaction_oid"));

					}
			   }
         }

	 //ShilpaR CR-707 Rel 8.0 Start
	/**
	 * Removes all POs from a transaction by looping through its shipments
	 */
	public void unassignStructuredPOAssignment(String transactionType)
         {
  		    Terms terms=null;
			try {
			   terms = (Terms)getComponentHandle("CustomerEnteredTerms");

			   ComponentList shipmentTermsList = (ComponentList) terms.getComponentHandle("ShipmentTermsList");
			   if (shipmentTermsList != null)
			    {
				int numberShipmentTerms = shipmentTermsList.getObjectCount();
				for (int i = 0; i < numberShipmentTerms; i++)
				{
					shipmentTermsList.scrollToObjectByIndex(i);
					ShipmentTerms shipmentTerms = (ShipmentTerms) shipmentTermsList.getBusinessObject();

                        	Map map = new HashMap();
			            	String statusPO = "";
			            	String actionPO = "";
			            	String amendActionPO = "";
			            	String amendStatusPO = "";
			            	String userOID = "";
			            	boolean isAmended = false;
			            	String status = getAttribute("transaction_status");
			            	if(status.equals(TransactionStatus.DELETED) && TransactionType.ISSUE .equals(transactionType)){
			            		statusPO = TradePortalConstants.PO_STATUS_CANCELLED;
			            		actionPO = TradePortalConstants.PO_ACTION_UNLINK;
			            		userOID = getAttribute("last_entry_user_oid");
			            	}
			            	else if(status.equals(TransactionStatus.DELETED) && TransactionType.AMEND .equals(transactionType)){
			            		statusPO = TradePortalConstants.PO_STATUS_DELETE;
			            		actionPO = TradePortalConstants.PO_ACTION_DELETE;
			            		userOID = getAttribute("last_entry_user_oid");
			            	}
			            	else if(TransactionStatus.PROCESSED_BY_BANK .equals(status) && (TransactionType.ISSUE .equals(transactionType) || TransactionType.AMEND .equals(transactionType))){
			            		statusPO = TradePortalConstants.PO_STATUS_PROCESSED_BY_BANK;
			            		actionPO = TradePortalConstants.PO_ACTION_PROCESSED_BY_BANK;
			            		userOID = getSystemUserOID();
			            	}
			            	else if(status.equals(TransactionStatus.CANCELLED_BY_BANK) && TransactionType.ISSUE .equals(transactionType)){
			            		statusPO = TradePortalConstants.PO_STATUS_UNASSIGNED; //SHR
			            		actionPO = TradePortalConstants.PO_ACTION_CAN_BY_BANK;
			            		userOID = getSystemUserOID();
			            	}
			            	else if(status.equals(TransactionStatus.CANCELLED_BY_BANK) && TransactionType.AMEND .equals(transactionType)){
			            		statusPO = TradePortalConstants.PO_STATUS_UNASSIGNED;
			            		actionPO = TradePortalConstants.PO_ACTION_CAN_BY_BANK;
			            		amendStatusPO = TradePortalConstants.PO_STATUS_CANCELLED;
			            		amendActionPO = TradePortalConstants.PO_ACTION_CAN_BY_BANK;
			            		userOID = getSystemUserOID();
			            		isAmended = true;
			            	}
			            	else if(status.equals(TransactionStatus.REJECTED_BY_BANK)){
			            		statusPO = TradePortalConstants.PO_STATUS_ASSIGNED;
			            		actionPO = TradePortalConstants.STATUS_REJECTED_BY_BANK;
			            		userOID =getSystemUserOID();
			            	}
			            	map.put("status", statusPO);
			            	map.put("action", actionPO);
			            	map.put("amendStatus", amendStatusPO);
			            	map.put("amendAction", amendActionPO);
			            	map.put("isAmended", isAmended);
			            	map.put("user", userOID);
			            	if(StringFunction.isNotBlank(statusPO))
			            	shipmentTerms.updateAssignedStrucPOStatus(this.getAttribute("instrument_oid"), this.getAttribute("transaction_oid"), map);
			            	//ShilpaR CR-707 Rel 8.0 End
						}
			    	}
				} catch (RemoteException e) {
					LOG.error("TransactionBean:: RemoteException at unassignStructuredPOAssignment(): ",e);
				} catch (AmsException e) {
					LOG.error("TransactionBean:: AmsException at unassignStructuredPOAssignment(): ",e);
				}
				catch (Exception e) {
					LOG.error("TransactionBean:: Exception at unassignStructuredPOAssignment(): ",e);
				}
         }

	// SHR CR-708 Rel 8.1.1 Start
	/**
	 * Removes all Invoices from a transaction by Terms
	 */
	public void unassignInvoiceAssignment(String transactionType) {
		Terms terms = null;
		try {
			terms = (Terms) getComponentHandle("CustomerEnteredTerms");

				Map map = new HashMap();
				String statusInv = "";
				String actionInv = "";
				String amendActionInv = "";
				String amendstatusInv = "";
				String userOID = "";
				boolean isAmended = false;
				String status = getAttribute("transaction_status");
				if (status.equals(TransactionStatus.DELETED)
						&& TransactionType.ISSUE.equals(transactionType)) {
					statusInv = TradePortalConstants.PO_STATUS_CANCELLED;
					actionInv = TradePortalConstants.PO_ACTION_UNLINK;
					userOID = getAttribute("last_entry_user_oid");
				} else if (status
						.equals(TransactionStatus.DELETED)
						&& TransactionType.AMEND.equals(transactionType)) {
					statusInv = TradePortalConstants.PO_STATUS_DELETE;
					actionInv = TradePortalConstants.PO_ACTION_DELETE;
					userOID = getAttribute("last_entry_user_oid");
				} else if (TransactionStatus.PROCESSED_BY_BANK
						.equals(status)
						&& (TransactionType.ISSUE.equals(transactionType) || TransactionType.AMEND
								.equals(transactionType))) {
					statusInv = TradePortalConstants.PO_STATUS_PROCESSED_BY_BANK;
					actionInv = TradePortalConstants.PO_ACTION_PROCESSED_BY_BANK;
					userOID = getSystemUserOID();
				} else if (status
						.equals(TransactionStatus.CANCELLED_BY_BANK)
						&& TransactionType.ISSUE.equals(transactionType)) {
					statusInv = TradePortalConstants.PO_STATUS_UNASSIGNED; //IR 6754
					actionInv = TradePortalConstants.PO_ACTION_UNLINK;
					userOID = getAttribute("last_entry_user_oid"); // IR 15760
				} else if (status
						.equals(TransactionStatus.CANCELLED_BY_BANK)
						&& TransactionType.AMEND.equals(transactionType)) {
					statusInv = TradePortalConstants.PO_STATUS_UNASSIGNED;
					actionInv = TradePortalConstants.PO_ACTION_UNLINK;
					amendstatusInv = TradePortalConstants.PO_STATUS_CANCELLED;
					amendActionInv = TradePortalConstants.PO_ACTION_CAN_BY_BANK;
					userOID = getSystemUserOID();
					isAmended = true;
				} else if (status
						.equals(TransactionStatus.REJECTED_BY_BANK)) {
					statusInv = TradePortalConstants.PO_STATUS_ASSIGNED;
					actionInv = TradePortalConstants.STATUS_REJECTED_BY_BANK;
					userOID = getSystemUserOID();
				}
				map.put("status", statusInv);
				map.put("action", actionInv);
				map.put("amendStatus", amendstatusInv);
				map.put("amendAction", amendActionInv);
				map.put("isAmended", isAmended);
				map.put("user", userOID);
				map.put("tranStatus",getAttribute("transaction_status"));
				map.put("tranType",transactionType);
				if (StringFunction.isNotBlank(statusInv))
					terms.updateAssignedInvoiceStatus(
							this.getAttribute("instrument_oid"),
							this.getAttribute("transaction_oid"), map);

		//	}

		} catch (RemoteException e) {
			LOG.error("TransactionBean:: RemoteException at unassignInvoiceAssignment(): ",e);
		} catch (AmsException e) {
			LOG.error("TransactionBean:: AmsException at unassignInvoiceAssignment(): ",e);
		} catch (Exception e) {
			LOG.error("TransactionBean:: Exception at unassignInvoiceAssignment(): ",e);
		}
	}

	// SHR CR-708 Rel 8.1.1 End

	//IAZ CR-586 08/18/10 Begin - Add overload method that takes user object as input so extra bean does not need to be created
	public String getPaymentDate(String userOid) throws RemoteException, AmsException
	{
		User user = (User) createServerEJB("User", Long.parseLong(userOid));

		return getPaymentDate(user.getAttribute("timezone"), false);
	}

	public String getPaymentDate(String timeZone, boolean timeZoneFlag) throws RemoteException, AmsException
	{
			try
	 		{

				java.util.Date todaysDate = null;
				LOG.debug("tz is {}" , timeZone);
				if (StringFunction.isNotBlank(timeZone))
					todaysDate = TPDateTimeUtility.getLocalDateTime(DateTimeUtility.getGMTDateTime(), timeZone);
				else
					todaysDate = GMTUtility.getGMTDateTime();

		   		SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy 00:00:00");

				java.util.Date sourceDate = null;
				if (StringFunction.isNotBlank(getAttribute("payment_date")))
                	sourceDate = getAttributeDate("payment_date");
                else
                {
					LOG.debug("TransactionBean::getPaymentDate - Setting Payment Date to Todays Date: {}" , formatter.format(todaysDate));
					return formatter.format(todaysDate);
				}

	      		if (sourceDate!=null && todaysDate.after(sourceDate))
	      		{
		   			LOG.debug("TransactionBean::getPaymentDate {}" , formatter.format(todaysDate));
		   			return formatter.format(todaysDate);

	      		}
	      		else
	      		{
					return getAttribute("payment_date");
				}

	 		}
	 		catch (Exception exc)
	 		{
				LOG.error("TransactionBean::getPaymentDate - Error setting Payment Date: ",exc);
				LOG.info("Set Payment Date to Today's GMT Date");
		      	return DateTimeUtility.getGMTDateTime();
	 		}
    }


	/**
	 * When saving a transaction, if the status is DELETED or CANCELLED_BY_BANK
	 * we need to unassigne any assigned po's.
	 *
	 * @exception   com.amsinc.ecsg.frame.AmsException
	 */
	public void preSave() throws AmsException
	{
		try
		{
			//Leelavathi CR-657 Rel 7.1.0 7/26/2011 start
			String instrumentType = getAttribute("copy_of_instr_type_code");
			if(InstrumentType.DOMESTIC_PMT.equals(instrumentType)){
			    //cquinton 12/12/2011 Rel 7.1 ir deul120843426 start
			    //comment out defaulting individual debit ind on new here -
			    // this causes issues when copying templates.
			    //defaulted by db column default instead
			    //if(isNew){
				//Vshah - 09/21/2011 - IR#LAUL082662583 - Rel7.1.
				//Added below if condition to check whether Txn has CustomerEnteredTerms component or not.
				//Instrument/Txn coming from TPS for the 1st time will not have CustomerEnteredTerms component
				//So calling getComponentHandle("CustomerEnteredTerms") eventually throws ComponentNoRelationshipException.
				
			    if (!isNew ) {
			    //cquinton 12/12/2011 Rel 7.1 ir deul120843426 end
			        String tableName = "domestic_payment";
			        String columnName = "domestic_payment_oid";


			        int count = DatabaseQueryBean.getCount(columnName, tableName, " p_transaction_oid = ? ", false, getAttribute("transaction_oid"));

			        //Ravindra B - 10/13/2011 - IR#AWUL100763381 - Rel7.1.
			        //Upgraded below if condition to check whether Txn has CustomerEnteredTerms component or not.
			        //Without CustomerEnteredTerms calling getComponentHandle("CustomerEnteredTerms") eventually throws ComponentNoRelationshipException.
			        if(count<=1 && StringFunction.isNotBlank(getAttribute("c_CustomerEnteredTerms")))   {
			            Terms terms = (Terms)getComponentHandle("CustomerEnteredTerms");
			            terms.setAttribute("individual_debit_ind","N");
			        }
				}
			}
			//Leelavathi CR-657 Rel 7.1.0 7/26/2011 End

			String status = getAttribute("transaction_status");
			if (status.equals(TransactionStatus.DELETED) ||
			   status.equals(TransactionStatus.CANCELLED_BY_BANK))
			{
                           unassignPOAssignment();
			}
			//ShilpaR CR-707 Rel 8.0 Start
			String transactionType = (String)getClientServerDataBridge().getCSDBValue("transaction_type");
			//IR 16127 - invoke unassignInvoiceAssignment for LRQ as well
			if ((instrumentType.equals(InstrumentType.IMPORT_DLC) || instrumentType.equals(InstrumentType.APPROVAL_TO_PAY) || InstrumentType.LOAN_RQST.equals(instrumentType)) && (TransactionType.ISSUE .equals(transactionType) || TransactionType.AMEND .equals(transactionType))) {
				//Make sure we have Terms associated
				if(StringFunction.isNotBlank(getAttribute("c_CustomerEnteredTerms")))   {
			    unassignStructuredPOAssignment(transactionType);

			    //SHR CR708 Rel8.1.1 Begin
			    //IR T36000016127 -- call unassignInvoiceAssignment function for Loan Request also
				if(instrumentType.equals(InstrumentType.APPROVAL_TO_PAY) || InstrumentType.LOAN_RQST.equals(instrumentType))
				{
					unassignInvoiceAssignment(transactionType);
				}
				//SHR CR708 Rel8.1.1 End
				}
			}
			//ShilpaR CR-707 Rel 8.0 End

			String rejectionIndicator = getAttribute("rejection_indicator");
			// This should only be executed the first time the user saves the rejected transaction
			if((rejectionIndicator != null)
					&& (rejectionIndicator.equals(TradePortalConstants.INDICATOR_X))
					&& (status.equals(TransactionStatus.REJECTED_BY_BANK)))
			{
				// Setting the rejection indicator to "Y" means that the rejected transaction
				// has been saved at least once since being received as a rejection by bank
				this.setAttribute("rejection_indicator", TradePortalConstants.INDICATOR_YES);
				//MDB Rel6.1 IR# PDUL012402000 2/1/11 - Begin
			    if (TradePortalConstants.INDICATOR_YES.equals(getAttribute("uploaded_ind")))
					this.setAttribute("transaction_status", TransactionStatus.VERIFIED);
				else
					this.setAttribute("transaction_status", TransactionStatus.STARTED);
				//MDB Rel6.1 IR# PDUL012402000 2/1/11 - End
			}
			// This should only be executed by the unpackager
			else if(((rejectionIndicator == null) || (rejectionIndicator.trim().length() == 0)
					|| (rejectionIndicator.equals(TradePortalConstants.INDICATOR_A)))
					&& (status.equals(TransactionStatus.REJECTED_BY_BANK)))
			{
				// Setting the rejection indicator to "X" means that the rejection message
				// was uploaded into the TradePortal by the Inbound Agent
				this.setAttribute("rejection_indicator", TradePortalConstants.INDICATOR_X);
			}

			// TLE - 12/04/06 - IR#FZUF120341403 - Add Begin TAILE
			// For those transactions not from Portal - Blank out the date started
			String custEnteredTerms = getAttribute("c_CustomerEnteredTerms");

			if(StringFunction.isBlank(custEnteredTerms)) {
			   this.setAttribute("date_started", "");
			}
			// TLE - 12/04/06 - IR#FZUF120341403 - Add End TAILE

			// Rel9.3.5 - CR-1028. This column is used for BTMU to have a display_transaction_oid. This is only for display purpose.
			String displayTransactionOid = getAttribute("display_transaction_oid");
			if(StringFunction.isBlank(displayTransactionOid)){
				String transOid = Long.toString(ObjectIDCache.getInstance(AmsConstants.OID).
		              generateObjectID(false));
				this.setAttribute("display_transaction_oid", transOid);
			}
			
		}
		catch (RemoteException e)
		{
			LOG.error("RemoteException occurred in Transaction preSave method! ",e);
		}
	}


	/* ShilpaR CR-707 Rel 8.0 Start
	 * @return - returns the upload system user id defined in corporate customer profile
	 * @throws RemoteException
	 * @throws AmsException
	 */
	public String getSystemUserOID () throws RemoteException, AmsException{

		Instrument instr = (Instrument)createServerEJB("Instrument", Long.parseLong(getAttribute("instrument_oid")));
		String corpOrgOid = instr.getAttribute("corp_org_oid");
		CorporateOrganization corOrg = (CorporateOrganization) createServerEJB("CorporateOrganization",
				  Long.parseLong(corpOrgOid));
		String userOid = corOrg.getAttribute("upload_system_user_oid");
		return userOid;
	}
	//ShilpaR CR-707 Rel 8.0 End


	// NSX CR-574 09/21/10 - Begin -
	/**
	 *
	 */
	public int save (boolean shouldValidate) throws RemoteException, AmsException

    {
		int val = super.save(shouldValidate);

		if (val == 1) { // continue postSave if no error
			val = postSave();
		}
		return val;
    }

	/**
	 * Post save processing goes here
	 *
	 */
	protected int postSave() throws  RemoteException, AmsException {
		int val = 0;

    	boolean isInstrument = TradePortalConstants.INDICATOR_NO.equals(getClientServerDataBridge().getCSDBValue("template"));
    	String action = getClientServerDataBridge().getCSDBValue(TradePortalConstants.TRAN_ACTION);
   		 if (isInstrument || TradePortalConstants.TRAN_ACTION_REPAIR.equals(action) || TradePortalConstants.TRAN_ACTION_SEND_FOR_AUTH.equals(action) ||
   				TradePortalConstants.TRAN_ACTION_BANK.equals(action) || TradePortalConstants.TRAN_ACTION_UPDATE_RECEIVED.equals(action) ) { // Nar IR-T36000022468 log the entry if transaction action is Bank
    		 if (StringFunction.isNotBlank(action)) {
    			// only create Transaction History if action is specified,
    		    val = createTransactionHistoryLog(action);
    		 }
    	 }

		return val;
	}

	/**
	 *
	 * Creates TransactionHistory log.
	 *
	 */
	protected int createTransactionHistoryLog(String action) throws RemoteException, AmsException {

		TransactionHistory tranHistory = (TransactionHistory) createServerEJB("TransactionHistory");
		tranHistory.newObject();
		tranHistory.setAttribute("action_datetime", DateTimeUtility.getGMTDateTime());
		tranHistory.setAttribute("action", action);
		tranHistory.setAttribute("transaction_status", getAttribute("transaction_status"));
		tranHistory.setAttribute("transaction_oid", getAttribute("transaction_oid"));

		if (!"Bank".equals(action)) {   // do not set user for bank processed actions
			tranHistory.setAttribute("user_oid", getAttribute("last_entry_user_oid"));
		}

		// if authorize, capture user's panel auth level in Transaction History
		//cquinton 1/16/2011 Rel 7.1 ir#dwum011663623 start
		// also capture panel auth for deal confirmation
		if (TradePortalConstants.BUTTON_AUTHORIZE.equals(action) ||
		        TradePortalConstants.TRAN_ACTION_CONFIRM_FXDEAL.equals(action)) {
			String user_oid = getClientServerDataBridge().getCSDBValue("user_oid");
        //cquinton 1/16/2011 Rel 7.1 ir#dwum011663623 end			
			//Nar 04 Sep 2013 Release8.3.0.0 IR-T36000020369 ADD Begin
			// Set the panel Auth level only when panel is enabled for transaction
			if(StringFunction.isNotBlank(getAttribute("panel_auth_group_oid"))){
			 User user = (User) createServerEJB("User", Long.parseLong(user_oid));
			 // Nar IR-T36000021246 01-Oct-2013 ADD Begin
			 //modify code to set sub_panel_auth_code if parent is working on behalf of subsidiary through subsiadiary
			 // access tab and use subsidiary profile setting is selected.
			 String userPanelAuthLevel = null;
			 if(TradePortalConstants.INDICATOR_YES.equals( getClientServerDataBridge().getCSDBValue("subsidiaryAccessTab")) && 
					 TradePortalConstants.INDICATOR_YES.equals(user.getAttribute("use_subsidiary_profile"))){
				 userPanelAuthLevel =  user.getAttribute("sub_panel_authority_code");
			 }else{
			   userPanelAuthLevel = user.getAttribute("panel_authority_code");
			 }
			// Nar IR-T36000021246 01-Oct-2013 ADD END
			 tranHistory.setAttribute("panel_authority_code", userPanelAuthLevel);
			}
			//Nar 04 Sep 2013 Release8.3.0.0 IR-T36000020369 ADD End
			tranHistory.setAttribute("user_oid", user_oid);

		}
        // Nar rel 8.3 CR 821 05/31/2013 Begin
		// set repair reason only if Action is Repair.
		if(TradePortalConstants.TRAN_ACTION_REPAIR.equals(action)){
			String repairReason = getClientServerDataBridge().getCSDBValue("RepairReason");
			if(StringFunction.isNotBlank(repairReason)){
				tranHistory.setAttribute("repair_reason", repairReason);
			}
			//MEer For Rel 8.3 IR-18343  Commented below part and handled it differently -- by setting user_oid appropriately in MediatorBean
			/*//MEerupula IR-18343/18637 // get user_oid to log in transaction history table
			*/
			
		}
		// Nar rel 8.3 CR 821 05/31/2013 End
		//Ravindra - 15th Nov 2012- Rel8100 IR-DEUM070438986 - Start
		//set proxyUserOid to Transaction History if the activity has performed by offline user
		//String proxyUserOid=getClientServerDataBridge().getCSDBValue("proxyUserOid");
		//AAlubala 23th Nov 2012- Rel8100 IR-DEUM070438986 - Start
		//The reason for checking string "null" is because it is possible 
		//for this value to be set to string null as the value is set inside a hidden
		//form by javascript.
                // W Zhu 15 May 2013 T36000011771 comment out. AuthorizeTransactionMediator replaces userOid with proxy user's oid.  Should take care of this.
		//Ravindra - 15th Nov 2012- Rel8100 IR-DEUM070438986 - End

		int ret = tranHistory.save(false);
		if (ret != 1) {
			LOG.info("Error occured while creating Transaction History Log...");
		}
		return ret;

	}

	public ClientServerDataBridge getClientServerDataBridge()  {
		return csdb;
	}
	// NSX CR-574 09/21/10 - End -

	// CJR CR-593 04/06/2011 Begin
	/**
	 * Returns true if this is a payment sent from H@H.
	 *
	 * @return true if a H2H Sent Payment, false otherwise.
	 */
	public boolean isH2HSentPayment() throws RemoteException, AmsException{
		return StringFunction.isNotBlank(this.getAttribute("h2h_source")); // Nar CR694A Rel9.0
	}
	// CJR CR-593 04/06/2011 End


	/**ShilpaR CR-707 update the POS associated to this instrument
	 * @param map- pass the status and action to be set on each Purchase Order
	 * @throws RemoteException
	 * @throws AmsException
	 */
	public void updateAssociatedPOStatus(Map map) throws RemoteException, AmsException
	{
		String sql = "Select purchase_order_oid,purchase_order_type from PURCHASE_ORDER "
			 +"where A_INSTRUMENT_OID=? ";
		DocumentHandler result = DatabaseQueryBean.getXmlResultSet(sql, false, new Object[]{this.getAttribute("instrument_oid")});
		Vector rows = result.getFragments("/ResultSetRecord");
		 for (int j=0; j<rows.size(); j++) {
			 DocumentHandler row = (DocumentHandler) rows.elementAt(j);
			 String strucPOOid = row.getAttribute("/PURCHASE_ORDER_OID");
			 String strucPOType = row.getAttribute("/PURCHASE_ORDER_TYPE");
			 PurchaseOrder strucPO = (PurchaseOrder) createServerEJB(
						"PurchaseOrder", Long.parseLong(strucPOOid));
			 strucPO.setAttribute("transaction_oid", null);
			 strucPO.setAttribute("instrument_oid", null);
			 strucPO.setAttribute("shipment_oid", null);
			 //if amendment transaction is  cancelled by bank then clear out amend counter in PO
			 if(map.get("isAmended") != null && (Boolean) map.get("isAmended") && TransactionType.AMEND.equals(strucPOType)){
				 strucPO.setAttribute("action",(String)map.get("amendAction"));
				 strucPO.setAttribute("status",(String)map.get("amendStatus"));
				 strucPO.setAttribute("amend_seq_no","0");
			 }
			 else{
			 strucPO.setAttribute("action",(String)map.get("action"));
			 strucPO.setAttribute("status",(String)map.get("status"));
			 }
			 if(StringFunction.isNotBlank((String) map.get("user")))
				strucPO.setAttribute("user_oid",(String)map.get("user"));

			 if(strucPO.save()!=1){
				 LOG.debug(" Error Saving in removeAssociatedPOs from Instrument" );
			 }
		}
	}
}