
  

package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;




/*
 * Allows Notification Rules to be defined differently for particular transaction
 * types and separates the decision criteria to send an email or a notification
 * message for each transaction type.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class NotifyRuleUserListBean_Base extends TradePortalBusinessObjectBean
{

  
  /* 
   * Register the attributes and associations of the business object
   */
   protected void registerAttributes() throws AmsException
   {  

      /* Register attributes defined in the Ancestor class */
   

      /* criterion_oid - Unique Identifier */
      attributeMgr.registerAttribute("notify_rule_user_list_oid", "notify_rule_user_list_oid", "ObjectIDAttribute");
      
		attributeMgr.registerAttribute("user_oid", "p_user_oid", "ParentIDAttribute");

		attributeMgr.registerAttribute("criterion_oid", "p_criterion_oid", "ParentIDAttribute");
		
   }
   
}
