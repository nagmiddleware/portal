
  

package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;
/**
 * Represents a purchase order that has been uploaded into the Trade Portal.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface PurchaseOrderHome extends EJBHome
{
   public PurchaseOrder create()
      throws RemoteException, CreateException, AmsException;

   public PurchaseOrder create(ClientServerDataBridge csdb)
      throws RemoteException, CreateException, AmsException;
}
