 
package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import com.ams.tradeportal.common.*;




/*
 * 
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class TradePortalBusinessObjectBean extends TradePortalBusinessObjectBean_Base
{
private static final Logger LOG = LoggerFactory.getLogger(TradePortalBusinessObjectBean.class);
   

  /**
   * 
   * 
   * @return void  
   */
   public void deactivate() throws RemoteException, AmsException
   {
    this.userDeactivate();

	this.setAttribute("activation_status", com.ams.tradeportal.common.TradePortalConstants.INACTIVE);
	this.setAttribute("date_deactivated", DateTimeUtility.getGMTDateTime());
   }

  /*
   * Performs any special "deactivate" processing that is specific to the
   * descendant of TradePortalBusinessObject.
   *
   * This method is optionally coded in descendant objects as desired.
   * Any object specific deactivation should be coded within this method
   * on descendants of TradePortalBusinessObject.
   */
   protected void userDeactivate() throws RemoteException, AmsException
   {
   }

/**
 * This method determines if a business object is currently being
 * associated by another object.  This method uses a GenericList
 * to search for associated objects.
 * 
 * @param objectName String	- the name of the Object to check for an association
 * @param criteria   String     - the retrieval criteria as specified in objectName's
 *				  getListCriteria
 * @param values     String[]   - array of parameter values used in criteria
 * @return boolean - true = is being associated, false = not associated
 */
  protected boolean isAssociated(String objectName, String criteria, 
			String[] values) throws RemoteException, AmsException
  {
	boolean isAssociated = false;

        // Since the items in the value array become SQL parameters,
        // run them through the filter first
        for(int i=0; i<values.length; i++)
           values[i] = SQLParamFilter.filter(values[i]);

	GenericList list = (GenericList) 
		EJBObjectFactory.createServerEJB(getSessionContext(), "GenericList");
	list.prepareList(objectName, criteria, values);
	list.getData();
	if (list.getObjectCount() > 0)
	    isAssociated = true;

	try
	{
	    list.remove();
	}
	catch (javax.ejb.RemoveException e)
	{
	    LOG.error("TradePortalBusinessObjectBean:: RemoteException at isAssociated(): ",e);
	}	

        return isAssociated;

  }

/**
 * Calls the isUnique method after adding a parameter
 *
 * @return boolean - true if unique, false otherwise
 * @param uniqueAttribute String - logical attribute name to test for uniqueness
 * @param testValue String - value to test for uniqueness
 * @param extraCriteria String - additional criteria to include on the where clause
 */
public boolean isUnique(String uniqueAttribute, String testValue, String extraCriteria) 
				throws RemoteException, AmsException
 {

   return isUnique(uniqueAttribute, testValue, extraCriteria, true);
 }
			
/**
 * Performs a uniqueness test on a specific attribute (assumed to not be
 * the oid column) of a business object given the value to test.  The
 * SQL used to test this is of the form:
 *    SELECT COUNT(oid_column) FROM table
 *    WHERE unique_col != testValue
 *    AND   oid_column = thisOid
 *    extraCriteria
 *
 * where 
 *   oid_column = physical database column name for this object's oid column
 *   table      = physical database table name for this object
 *   unique_col = physical database column name for the logical attribute passed in
 *   testValue  = value passed in that is being tested for uniqueness
 *   thisOid    = this object's oid
 *   extraCriteria = and additional criteria to include on the where clause
 *                   MUST INCLUDE A PRECEDING 'AND'
 *
 * For example, isUnique("user_identifier", "Joe", "") for the User object 
 * with user_oid of 3 creates this SQL:
 *   SELECT COUNT(user_oid) FROM USERS 
 *   WHERE user_identifer = 'Joe' AND user_oid != 3
 *
 *
 * @return boolean - true if unique, false otherwise
 * @param uniqueAttribute String - logical attribute name to test for uniqueness
 * @param testValue String - value to test for uniqueness
 * @param extraCriteria String - additional criteria to include on the where clause
 * @param checkModifiedStatus boolean - determines if we check if the attribute has been modified
 */
public boolean isUnique(String uniqueAttribute, String testValue, String extraCriteria, boolean checkModifiedStatus,boolean checkIDPhysicalName) 
				throws RemoteException, AmsException
 {
    boolean isUnique = true;
    
	if ((checkModifiedStatus && isAttributeModified(uniqueAttribute)) || (!checkModifiedStatus))
	 {       //jgadela - 07/15/2014 - Rel 9.1 IR#T36000026319 - SQL Injection fix
            StringBuffer whereClause = new StringBuffer();
		    whereClause.append(attributeMgr.getPhysicalName(uniqueAttribute));
		    whereClause.append("=? and ");
		    whereClause.append(attributeMgr.getPhysicalName(this.getIDAttributeName(checkIDPhysicalName)));
		    whereClause.append(" != ? ");
		    
		    // If this type of business object can be deactivated,
		    // only check uniqueness within the active items
		    if(this.attributeMgr.getAttributeHashtable().get("activation_status") != null){
		           whereClause.append(" and activation_status = '"+TradePortalConstants.ACTIVE+"' ");   
		    }
	    	whereClause.append(extraCriteria);	  
	    	//jgadela - 07/15/2014 - Rel 9.1 IR#T36000026319 - SQL Injection fix
            int count = 
                DatabaseQueryBean.getCount( uniqueAttribute, this.getDataTableName(), whereClause.toString(), false, testValue, this.getObjectID());			    
            
            isUnique = (count < 1);
	 }
	else
			LOG.debug("{} not modified, skipping isUnique test",uniqueAttribute);
			
	return isUnique;
 }
public boolean isUnique(String uniqueAttribute, String testValue, String extraCriteria, boolean checkModifiedStatus) 
		throws RemoteException, AmsException
{
	return isUnique( uniqueAttribute,  testValue,  extraCriteria,  checkModifiedStatus,true) ;
}

/**
 * Loads the values from the XML formatted result set using the business object's 
 * Hashtable of registered attributes.  If the DBMS value is null,  the attribute
 * value is NOT set.   Must be a unique row in document type of <DocRoot><ResultSetRecord ID="0">
 *
 * @param Hashtable The Hash of registered attribute objects on the business object

 */
protected void populateDataFromResultSet(DocumentHandler doc) throws AmsException
{
	LOG.debug("Start populateDataFromResultSet()");
	Hashtable registeredAttributes = attributeMgr.getAttributeHashtable ();
	String value = null;
	String physicalName;
	// For each attribute, get the value from the result set and 
	// set the attribute value accordingly
	Enumeration attributes = registeredAttributes.elements();
	while(attributes.hasMoreElements()){
		Attribute attribute = (Attribute)attributes.nextElement();
		// If the attribute is a DoubleDispatchedAttribute, no processing is necessary
		if(attribute instanceof DoubleDispatchAttribute | 
		   attribute instanceof ComputedAttribute |
		   attribute instanceof LocalAttribute){
			// No processing necessary, process next attribute
		}else{
			// Process standard attributes
			physicalName = attribute.getPhysicalName().toUpperCase();
			try{
				value = doc.getAttribute("/ResultSetRecord(0)/" + physicalName);
			}catch(Exception e){
				LOG.error("Errors encountered while getting data from record: " , e);
				throw new AmsException("Error occured in TradePortalBusinessObject populateDataFromResultSet: Nested Exception: " + e.getMessage());
			}
			// Only set the attribute value is the DBMS value is NOT null
			if(value != null){
				attribute.setAttributeValue(value, false);
			}
		}
	}
	LOG.debug("End dataObjectLoadDatatoManager()");
}
}
