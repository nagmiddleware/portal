
  

package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;




/*
 * When a user logs out, a history record is created that represents their
 * session.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class UserSessionHistoryBean extends UserSessionHistoryBean_Base
{
private static final Logger LOG = LoggerFactory.getLogger(UserSessionHistoryBean.class); 
 
    /**
     * Copies a few values off of an active session to populate this history object
     *
     * @param activeSession - the active session being copied from 
     */
    public void populateFromActiveSession(ActiveUserSession activeSession)
       throws RemoteException, AmsException 
     {
        this.setAttribute("login_timestamp", activeSession.getAttribute("creation_timestamp"));
        this.setAttribute("server_instance_name", activeSession.getAttribute("server_instance_name"));
        this.setAttribute("user_oid", activeSession.getAttribute("user_oid"));
        //cquinton 2/16/2012 Rel 8.0 ppx255 start
        this.setAttribute("logon_type", activeSession.getAttribute("logon_type"));
        this.setAttribute("confirm_relogon", activeSession.getAttribute("confirm_relogon"));
        this.setAttribute("force_logout", activeSession.getAttribute("force_logout"));
        //cquinton 2/16/2012 Rel 8.0 ppx255 end
     }
}
