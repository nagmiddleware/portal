
  

package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;




/*
 * A Bank Organization Group (BOG) represents a grouping of corporate customers
 * underneath a Client Bank.    Each BOG has its own branding directory and
 * can own reference data.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class BankOrganizationGroupBean_Base extends ReferenceDataOwnerBean
{
private static final Logger LOG = LoggerFactory.getLogger(BankOrganizationGroupBean_Base.class);

  
  /* 
   * Register the attributes and associations of the business object
   */
   protected void registerAttributes() throws AmsException
   {  

      /* Register attributes defined in the Ancestor class */
      super.registerAttributes();
      
      /* description - A description or name of the Bank Organization Group. */
      attributeMgr.registerAttribute("description", "description");
      attributeMgr.requiredAttribute("description");
      attributeMgr.registerAlias("description", getResourceManager().getText("BankOrganizationGroupBeanAlias.description", TradePortalConstants.TEXT_BUNDLE));
      
      /* branding_directory - The branding directory that is used when users that belong to this BOG are
         accessing the system.  Users who belong to corporate customers underneath
         a BOG will use the branding directory of their BOG. */
      attributeMgr.registerAttribute("branding_directory", "branding_directory");
      attributeMgr.requiredAttribute("branding_directory");
      attributeMgr.registerAlias("branding_directory", getResourceManager().getText("BankOrganizationGroupBeanAlias.branding_directory", TradePortalConstants.TEXT_BUNDLE));
      
      /* support_information - Information on how the corporate customers under this BOG can gain support
         information.  Whatever is typed here will appear to the user on the support
         information page */
      attributeMgr.registerAttribute("support_information", "support_information");
      
      /* sender_name - Determines who will be the sender of emails for the first language setting */
      attributeMgr.registerAttribute("sender_name", "sender_name");
      
      /* sender_email_address - Email address of the sender for first language setting */
      attributeMgr.registerAttribute("sender_email_address", "sender_email_address");
      
      /* system_name - Name used for this bank online trade system for first language setting */
      attributeMgr.registerAttribute("system_name", "system_name");
      
      /* bank_url - The URL for this bank for first language setting. */
      attributeMgr.registerAttribute("bank_url", "bank_url");
      
      /* email_language - Determines which language the sender name, sender email address, trade system
         name, and bank url should appear in */
      attributeMgr.registerReferenceAttribute("email_language", "email_language", "INSTRUMENT_LANGUAGE");
      attributeMgr.registerAlias("email_language", getResourceManager().getText("BankOrganizationGroupBeanAlias.email_language", TradePortalConstants.TEXT_BUNDLE));
      
      /* email_language_2 - Determines which language sender name 2, sender email address 2, trade system
         name 2, and bank url 2 should appear in */
      attributeMgr.registerReferenceAttribute("email_language_2", "email_language_2", "INSTRUMENT_LANGUAGE");
      attributeMgr.registerAlias("email_language_2", getResourceManager().getText("BankOrganizationGroupBeanAlias.email_language_2", TradePortalConstants.TEXT_BUNDLE));
      
      /* sender_name_2 - Determines who will be the sender of emails for the second language setting */
      attributeMgr.registerAttribute("sender_name_2", "sender_name_2");
      
      /* do_not_reply_ind - A setting indicating whether or not emails sent will include the sentence
         "Do Not Reply to Email" */
      attributeMgr.registerAttribute("do_not_reply_ind", "do_not_reply_ind", "IndicatorAttribute");
      
      /* sender_email_address_2 - Email address of the sender for second language setting */
      attributeMgr.registerAttribute("sender_email_address_2", "sender_email_address_2");
      
      /* system_name_2 - Name used for this bank online trade system for second language setting */
      attributeMgr.registerAttribute("system_name_2", "system_name_2");
      
      /* bank_url_2 - The URL for this bank for second language setting. */
      attributeMgr.registerAttribute("bank_url_2", "bank_url_2");
      
      /* brand_button_ind - A setting indicating whether the client bank makes all the buttons branded.
         
         Yes - The client bank uses the branded buttons stored in the branding directory.
         No (default) - The client bank uses the standard buttons. */
      attributeMgr.registerAttribute("brand_button_ind", "brand_button_ind", "IndicatorAttribute");
      
        /* Pointer to the parent ClientBank */
      attributeMgr.registerAttribute("client_bank_oid", "p_client_bank_oid", "ParentIDAttribute");
      
      //SHILPAR- CR 597 start//
      /* sender_name - Determines who will be the sender of emails for the first language setting */
      attributeMgr.registerAttribute("bene_sender_name", "bene_sender_name");
      
      /* sender_email_address - Email address of the sender for first language setting */
      attributeMgr.registerAttribute("bene_sender_email_address", "bene_sender_email_address");
      
      /* system_name - Name used for this bank online trade system for first language setting */
      attributeMgr.registerAttribute("bene_system_name", "bene_system_name");
      
      /* bank_url - The URL for this bank for first language setting. */
      attributeMgr.registerAttribute("bene_bank_url", "bene_bank_url");
      
      /* email_language - Determines which language the sender name, sender email address, trade system
         name, and bank url should appear in */
      attributeMgr.registerReferenceAttribute("bene_email_language", "bene_email_language", "INSTRUMENT_LANGUAGE");
      attributeMgr.registerAlias("bene_email_language", getResourceManager().getText("BankOrganizationGroupBeanAlias.email_language", TradePortalConstants.TEXT_BUNDLE));
      
      /* do_not_reply_ind - A setting indicating whether or not emails sent will include the sentence
      "Do Not Reply to Email" */
      attributeMgr.registerAttribute("bene_do_not_reply_ind", "bene_do_not_reply_ind", "IndicatorAttribute");
      //SHILPAR- CR 597 end//

      // DK CR-640 Rel7.1 BEGINS
      /* fx_online_avail_ind - A setting indicating whether online foreign exchange rate is availlable. */
      attributeMgr.registerAttribute("fx_online_avail_ind", "fx_online_avail_ind", "IndicatorAttribute");

      /* deflt_fx_online_acct_id - Default Online Account Id for customer to access online foreign exchange */
      attributeMgr.registerAttribute("deflt_fx_online_acct_id", "deflt_fx_online_acct_id");

      /* domain_org_url - Domain/Organisational URL for customer to access FX Online */
      attributeMgr.registerAttribute("domain_org_url", "domain_org_url");
      // DK CR-640 Rel7.1 ENDS
      
      /* cross_rate_calc_oid -  */
      attributeMgr.registerAssociation("cross_rate_calc_oid", "a_cross_rate_calc_oid", "CrossRateCalcRule");
      
    //PMitnala- CR 821 Rel 8.3 start//
      /* sender_name - Determines who will be the sender of emails for the first language setting */
      attributeMgr.registerAttribute("repair_sender_name", "repair_sender_name");
      
      /* sender_email_address - Email address of the sender for first language setting */
      attributeMgr.registerAttribute("repair_sender_email_address", "repair_sender_email_address");
      
      /* system_name - Name used for this bank online trade system for first language setting */
      attributeMgr.registerAttribute("repair_system_name", "repair_system_name");
      
      /* bank_url - The URL for this bank for first language setting. */
      attributeMgr.registerAttribute("repair_bank_url", "repair_bank_url");
      
      /* email_language - Determines which language the sender name, sender email address, trade system
         name, and bank url should appear in */
      attributeMgr.registerReferenceAttribute("repair_email_language", "repair_email_language", "INSTRUMENT_LANGUAGE");
      attributeMgr.registerAlias("repair_email_language", getResourceManager().getText("BankOrganizationGroupBeanAlias.email_language", TradePortalConstants.TEXT_BUNDLE));
      
      /* do_not_reply_ind - A setting indicating whether or not emails sent will include the sentence
      "Do Not Reply to Email" */
      attributeMgr.registerAttribute("repair_do_not_reply_ind", "repair_do_not_reply_ind", "IndicatorAttribute");
      //PMitnala- CR 821 Rel 8.3 end//

      /*KMehta 10/15/2013 Rel8400 CR-910 Start*/
      /* slc_gua_expiry_date_required - Add new slc_gua_expiry_date_required attribute to the BankOrganizationGroup bean. 
       * In the database this is a new char(1) column that defaults to �N�.*/
      attributeMgr.registerAttribute("slc_gua_expiry_date_required", "slc_gua_expiry_date_required", "IndicatorAttribute");
      /*KMehta 10/15/2013 Rel8400 CR-910 End*/
      
      // Narayan- CR 913 Rel 9.0 29-Jan-2014 start
      /* sender_name - Determines who will be the sender of emails for the first language setting */
      attributeMgr.registerAttribute("payable_sender_name", "payable_sender_name");
      
      /* sender_email_address - Email address of the sender for first language setting */
      attributeMgr.registerAttribute("payable_sender_email_address", "payable_sender_email_address");
      
      /* system_name - Name used for this bank online trade system for first language setting */
      attributeMgr.registerAttribute("payable_system_name", "payable_system_name");
      
      /* bank_url - The URL for this bank for first language setting. */
      attributeMgr.registerAttribute("payable_bank_url", "payable_bank_url");
      
      /* email_language - Determines which language the sender name, sender email address, trade system
         name, and bank url should appear in */
      attributeMgr.registerReferenceAttribute("payable_email_language", "payable_email_language", "INSTRUMENT_LANGUAGE");
      attributeMgr.registerAlias("payable_email_language", getResourceManager().getText("BankOrganizationGroupBeanAlias.email_language", TradePortalConstants.TEXT_BUNDLE));
      
      /* do_not_reply_ind - A setting indicating whether or not emails sent will include the sentence
      "Do Not Reply to Email" */
      attributeMgr.registerAttribute("payable_do_not_reply_ind", "payable_do_not_reply_ind", "IndicatorAttribute");
      // Narayan- CR 913 Rel 9.0 29-Jan-2014 End
      
    //SSikhakolli Rel 9.3.5 CR-1029 Adding new attributes enable_admin_update_centre, include_tt_reim_allowed
      attributeMgr.registerAttribute("enable_admin_update_centre", "enable_admin_update_centre", "IndicatorAttribute");
      attributeMgr.registerAttribute("include_tt_reim_allowed", "include_tt_reim_allowed", "IndicatorAttribute");
      attributeMgr.registerAttribute("enable_session_sync_html", "enable_session_sync_html", "IndicatorAttribute");
      
      //MEer Rel 9.3.5 CR-1027 -START
      /* airway_bill_pdf_type - Indicates the generated pdf form is expanded version of PDF. 

      B=Generated Application form is Basic version
      E=Generated Application form is Expanded version
      N=Neither Basic nor Expanded is selected
      R=Generated Application form can be selected either Basic or Expanded versions
     */
      attributeMgr.registerReferenceAttribute("airway_bill_pdf_type", "airway_bill_pdf_type", "EXPANDED_TYPE_PDF_FORM");
      attributeMgr.registerReferenceAttribute("atp_pdf_type", "atp_pdf_type", "BASIC_TYPE_PDF_FORM");
      attributeMgr.registerReferenceAttribute("exp_col_pdf_type", "exp_col_pdf_type", "BASIC_TYPE_PDF_FORM"); //Direct Send Collection
      attributeMgr.registerReferenceAttribute("exp_oco_pdf_type", "exp_oco_pdf_type", "BASIC_TYPE_PDF_FORM"); //Export Collection   
      attributeMgr.registerReferenceAttribute("imp_dlc_pdf_type", "imp_dlc_pdf_type", "BASIC_OR_EXPANDED_PDF_FORM");
      attributeMgr.registerReferenceAttribute("lrq_pdf_type", "lrq_pdf_type", "EXPANDED_TYPE_PDF_FORM");
      attributeMgr.registerReferenceAttribute("gua_pdf_type", "gua_pdf_type", "BASIC_OR_EXPANDED_PDF_FORM");
      attributeMgr.registerReferenceAttribute("slc_pdf_type", "slc_pdf_type", "BASIC_OR_EXPANDED_PDF_FORM");
      attributeMgr.registerReferenceAttribute("rqa_pdf_type", "rqa_pdf_type", "BASIC_TYPE_PDF_FORM");
      attributeMgr.registerReferenceAttribute("shp_pdf_type", "shp_pdf_type", "EXPANDED_TYPE_PDF_FORM");
      //MEer Rel 9.3.5 CR-1027 -END



      //Rpasupulati Rel 9.4 CR -1001 Start
      attributeMgr.registerAttribute("reporting_code1_reqd_for_ach", "reporting_code1_reqd_for_ach", "IndicatorAttribute");
      attributeMgr.registerAttribute("reporting_code1_reqd_for_rtgs", "reporting_code1_reqd_for_rtgs", "IndicatorAttribute");
      attributeMgr.registerAttribute("reporting_code2_reqd_for_ach", "reporting_code2_reqd_for_ach", "IndicatorAttribute");
      attributeMgr.registerAttribute("reporting_code2_reqd_for_rtgs", "reporting_code2_reqd_for_rtgs", "IndicatorAttribute");
      //Rpasupulati Rel 9.4 CR -1001 End
      
      // W Zhu 9/28/2015 Rel 9.4 CR-1018 
      attributeMgr.registerAttribute("no_sso_ind", "no_sso_ind", "IndicatorAttribute");
      
      /*CR1123 Start*/
      attributeMgr.registerAttribute("incld_bnk_url_trade", "incld_bnk_url_trade", "IndicatorAttribute");
      attributeMgr.registerAttribute("incld_bnk_url_pmnt_ben", "incld_bnk_url_pmnt_ben", "IndicatorAttribute");
      attributeMgr.registerAttribute("incld_bnk_url_repr_panel", "incld_bnk_url_repr_panel", "IndicatorAttribute");
      attributeMgr.registerAttribute("incld_bnk_url_payable_mgmt", "incld_bnk_url_payable_mgmt", "IndicatorAttribute");
	  /*CR1123 End*/
     
   }
   
 /* 
   * CR-655
   * Register the components of the business object
   */
   protected void registerComponents() throws RemoteException, AmsException
   {  
      /* Register the components defined in the Ancestor class */
      super.registerComponents();
      
      /* PaymentReportingCode1List - Each Bank Group has multiple entries of Payment Reporting Code 1. */
      registerOneToManyComponent("PaymentReportingCode1List","PaymentReportingCode1List");

      /* PaymentReportingCode2List - Each Bank Group has multiple entries of Payment Reporting Code 2. */
      registerOneToManyComponent("PaymentReportingCode2List","PaymentReportingCode2List");
   }
   
 
 
   
}
