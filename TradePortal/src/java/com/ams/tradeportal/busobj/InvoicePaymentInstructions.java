package com.ams.tradeportal.busobj;
import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.DocumentHandler;

import java.rmi.*;


/**
 * Describes the mapping of an Invoice Payment Instructions uploaded for the Invoice. 
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */

public interface InvoicePaymentInstructions extends TradePortalBusinessObject {

	public boolean updateInvPaymentInstructions(DocumentHandler inputDoc,
			DocumentHandler outputDoc)throws RemoteException, AmsException;
	
	//PMitnala IR T36000020561 - CR709 Rel 8.3  -Start
	public void createNewInvPaymentInstructions(String originalTransOid, String newTransactionOid) throws RemoteException, AmsException;
	//PMitnala IR T36000020561 - CR709 Rel 8.3  -End
	
	public String deleteInvoicePaymentInstruction (DocumentHandler inputDoc)	throws RemoteException, AmsException;
	
	public DocumentHandler processInvPayTerms(DocumentHandler inputDoc, int numberOfPayees) throws RemoteException, AmsException; 

}
