package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;
/**
 * Any organization that can own reference data is a subclass of this business
 * object.  It defines one-to-many component relationships to reference data
 * such as phrases, templates, etc.
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface ReferenceDataOwnerHome extends EJBHome
{
   public ReferenceDataOwner create()
      throws RemoteException, CreateException, AmsException;

   public ReferenceDataOwner create(ClientServerDataBridge csdb)
      throws RemoteException, CreateException, AmsException;
}
