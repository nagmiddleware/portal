package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;
/**
 * When the Auto LC Create functionality is used, a log is kept that contains
 * any information, warning, or error messages that are generated.   Since
 * most of the processing for the Auto LC Create functionality happens in the
 * background, the log is the only way that a user can view the results of
 * their operation.
 * 
 * The information that comprises the log is stored in this business object.
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface AutoLCCreateLogHome extends EJBHome
{
   public AutoLCCreateLog create()
      throws RemoteException, CreateException, AmsException;

   public AutoLCCreateLog create(ClientServerDataBridge csdb)
      throws RemoteException, CreateException, AmsException;
}
