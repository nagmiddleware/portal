package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;

import com.ams.tradeportal.common.*;

/*
 * 
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class InvoiceDetailsBean extends InvoiceDetailsBean_Base {
private static final Logger LOG = LoggerFactory.getLogger(InvoiceDetailsBean.class);

	//BSL IR#PUL032965444 04/04/11 Begin
	//public void updateInvoiceDetail(DocumentHandler inputDoc,DocumentHandler outputDoc)throws RemoteException, AmsException {
	/**
	 * updates InvoiceDetail object.
	 */
	public void updateInvoiceDetail(DocumentHandler inputDoc,
			DocumentHandler outputDoc,
			DomesticPayment dp) throws RemoteException, AmsException {
		try {
			String domesticPaymentOid = dp.getAttribute("domestic_payment_oid");
			String invoiceDetailOid = inputDoc.getAttribute("/InvoiceDetails/invoice_detail_oid/");

			// create/update an InvoiceDetails only if we have a valid DomesticPayment 
			if (StringFunction.isNotBlank(domesticPaymentOid)) {
				if (StringFunction.isNotBlank(invoiceDetailOid)) {
					Long lInvoiceDetailOid = new Long(invoiceDetailOid);
					this.getData(lInvoiceDetailOid.longValue());
				} else {
					this.newObject();
					invoiceDetailOid = String.valueOf(this.getObjectID());
					inputDoc.setAttribute("/InvoiceDetails/invoice_detail_oid", invoiceDetailOid);
					inputDoc.setAttribute("/InvoiceDetails/domestic_payment_oid", domesticPaymentOid);
				}

				this.populateFromXmlDoc(inputDoc);
			}
		} catch (Exception any_e) {
			LOG.error("INVDETBean::updateNewInvoiceDetails::Exception: ",any_e);
			this.getErrorManager().issueError(getClass().getName(),
					TradePortalConstants.INV_DET_GEN_ERR_WRN);
		}
	}
	//BSL IR#PUL032965444 04/04/11 End

}
