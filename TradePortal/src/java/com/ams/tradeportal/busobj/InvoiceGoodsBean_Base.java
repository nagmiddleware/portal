
  

package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;




/*
 * Invoice Goods.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class InvoiceGoodsBean_Base extends BusinessObjectBean
{
private static final Logger LOG = LoggerFactory.getLogger(InvoiceGoodsBean_Base.class);

  
  /* 
   * Register the attributes and associations of the business object
   */
   protected void registerAttributes() throws AmsException
   {  
      /* invoice_goods_oid - Unique Identifier. */
      attributeMgr.registerAttribute("invoice_goods_oid", "invoice_goods_oid", "ObjectIDAttribute");
      
      /* po_reference_id -  */
      attributeMgr.registerAttribute("po_reference_id", "po_reference_id");
      
      /* po_issue_datetime -  */
      attributeMgr.registerAttribute("po_issue_datetime", "po_issue_datetime", "DateTimeAttribute");
      
      /* line_items_total_amount -  */
      attributeMgr.registerAttribute("line_items_total_amount", "line_items_total_amount", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      
      /* total_net_amount -  */
      attributeMgr.registerAttribute("total_net_amount", "total_net_amount", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      
      /* seller_information_label1 -  */
      attributeMgr.registerAttribute("seller_information_label1", "seller_information_label1");
      
      /* seller_information_label2 -  */
      attributeMgr.registerAttribute("seller_information_label2", "seller_information_label2");
      
      /* seller_information_label3 -  */
      attributeMgr.registerAttribute("seller_information_label3", "seller_information_label3");
      
      /* seller_information_label4 -  */
      attributeMgr.registerAttribute("seller_information_label4", "seller_information_label4");
      
      /* seller_information_label5 -  */
      attributeMgr.registerAttribute("seller_information_label5", "seller_information_label5");
      
      /* seller_information_label6 -  */
      attributeMgr.registerAttribute("seller_information_label6", "seller_information_label6");
      
      /* seller_information_label7 -  */
      attributeMgr.registerAttribute("seller_information_label7", "seller_information_label7");
      
      /* seller_information_label8 -  */
      attributeMgr.registerAttribute("seller_information_label8", "seller_information_label8");
      
      /* seller_information_label9 -  */
      attributeMgr.registerAttribute("seller_information_label9", "seller_information_label9");
      
      /* seller_information_label10 -  */
      attributeMgr.registerAttribute("seller_information_label10", "seller_information_label10");
      
      /* seller_information1 -  */
      attributeMgr.registerAttribute("seller_information1", "seller_information1");
      
      /* seller_information2 -  */
      attributeMgr.registerAttribute("seller_information2", "seller_information2");
      
      /* seller_information3 -  */
      attributeMgr.registerAttribute("seller_information3", "seller_information3");
      
      /* seller_information4 -  */
      attributeMgr.registerAttribute("seller_information4", "seller_information4");
      
      /* seller_information5 -  */
      attributeMgr.registerAttribute("seller_information5", "seller_information5");
      
      /* seller_information6 -  */
      attributeMgr.registerAttribute("seller_information6", "seller_information6");
      
      /* seller_information7 -  */
      attributeMgr.registerAttribute("seller_information7", "seller_information7");
      
      /* seller_information8 -  */
      attributeMgr.registerAttribute("seller_information8", "seller_information8");
      
      /* seller_information9 -  */
      attributeMgr.registerAttribute("seller_information9", "seller_information9");
      
      /* seller_information10 -  */
      attributeMgr.registerAttribute("seller_information10", "seller_information10");
      
      /* goods_description -  */
      attributeMgr.registerAttribute("goods_description", "goods_description");

      /* Pointer to the parent Invoice */
      attributeMgr.registerAttribute("invoice_oid", "p_invoice_oid", "ParentIDAttribute");
      //CR1006 start
      /*Specifies the applicable incoterm by means of a code*/
      attributeMgr.registerReferenceAttribute("incoterm", "incoterm","INCOTERM");
      
      /* country_of_loding - Country of Loading for the goods that the invoice is covering */
      attributeMgr.registerAttribute("country_of_loading", "country_of_loading");
      
      /* country_of_discharge - Country of Discharge for the goods that the invoice is covering */
      attributeMgr.registerAttribute("country_of_discharge", "country_of_discharge");
      
      /* vessel - vessel shipping the goods that the invoice is covering */
      attributeMgr.registerAttribute("vessel", "vessel");
      
      
      /* carrier - the carrier for the goods that the invoice is covering. */
      attributeMgr.registerAttribute("carrier", "carrier");
      
      /* actual_ship_date- */
      attributeMgr.registerAttribute("actual_ship_date", "actual_ship_date","DateAttribute");
      //CR1006 end
   }


    /*
      * Register the components of the business object
      */
    protected void registerComponents() throws RemoteException, AmsException {
        /* InvoiceSummaryGoodsList - */
        registerOneToManyComponent("InvoiceLineItemDetailList", "InvoiceLineItemDetailList");
    }




}
