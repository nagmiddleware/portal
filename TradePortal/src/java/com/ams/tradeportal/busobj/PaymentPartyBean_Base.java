


package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;




/*
 * A party of Payment instrument.  It should be a BankBranch.
 *
 *     Copyright  � 2003
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class PaymentPartyBean_Base extends TradePortalBusinessObjectBean
{
private static final Logger LOG = LoggerFactory.getLogger(PaymentPartyBean_Base.class);


  /*
   * Register the attributes and associations of the business object
   */
   protected void registerAttributes() throws AmsException
   {

      /* Register attributes defined in the Ancestor class */
      super.registerAttributes();

      /* payment_party_oid - Object ID. */
      attributeMgr.registerAttribute("payment_party_oid", "payment_party_oid", "ObjectIDAttribute");

      /* bank_name - Bank Name */
      attributeMgr.registerAttribute("bank_name", "bank_name");

      /* bank_branch_code - Bank/Branch Code */
      attributeMgr.registerAttribute("bank_branch_code", "bank_branch_code");

      /* branch_name - Branch Name */
      attributeMgr.registerAttribute("branch_name", "branch_name");

      /* address_line_1 - Address Line 1 */
      attributeMgr.registerAttribute("address_line_1", "address_line_1");

      /* address_line_2 - Address Line 2 */
      attributeMgr.registerAttribute("address_line_2", "address_line_2");

      /* address_line_3 - Address Line 3 */
      attributeMgr.registerAttribute("address_line_3", "address_line_3");

      /* address_line_4 - Address Line 4 */
      attributeMgr.registerAttribute("address_line_4", "address_line_4");

      attributeMgr.registerReferenceAttribute("party_type", "party_type", "TERMS_PARTY_TYPE");

	 attributeMgr.registerReferenceAttribute("country", "country", "COUNTRY"); //IR - SWUK012840187

   }






}
