
  

package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;




/*
 * Business object to store commonly used phrases that comprise instruments
 * created in the Trade Portal.  Phrases then can be added into text fields
 * on  instruments.
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class PhraseBean_Base extends ReferenceDataBean
{
private static final Logger LOG = LoggerFactory.getLogger(PhraseBean_Base.class);

  
  /* 
   * Register the attributes and associations of the business object
   */
   protected void registerAttributes() throws AmsException
   {  

      /* Register attributes defined in the Ancestor class */
      super.registerAttributes();
      
      /* phrase_oid - Unique identifier */
      attributeMgr.registerAttribute("phrase_oid", "phrase_oid", "ObjectIDAttribute");
      
      /* phrase_category - The phrase category to which this phrase belongs.   Phrase category is used
         to determine which text boxes it can be added to.   Some example of phrase
         catgeories are goods description, payment instructions, etc.
 */
      attributeMgr.registerReferenceAttribute("phrase_category", "phrase_category", "PHRASE_CATEGORY");
      attributeMgr.requiredAttribute("phrase_category");
      attributeMgr.registerAlias("phrase_category", getResourceManager().getText("PhraseBeanAlias.phrase_category", TradePortalConstants.TEXT_BUNDLE));
      
      /* phrase_text - The text that comprises the phrase */
      attributeMgr.registerAttribute("phrase_text", "phrase_text");
      attributeMgr.requiredAttribute("phrase_text");
      attributeMgr.registerAlias("phrase_text", getResourceManager().getText("PhraseBeanAlias.phrase_text", TradePortalConstants.TEXT_BUNDLE));
      
      /* phrase_type - Indicates whether the phrase is modifiable.   The default for this setting
         is modifiable, but admin users may set it to static or partially modifiable
         (fill in the blank).  The phrase type is only looked at on the guarantee
         pages. */
      attributeMgr.registerReferenceAttribute("phrase_type", "phrase_type", "PHRASE_TYPE");
      attributeMgr.requiredAttribute("phrase_type");
      attributeMgr.registerAlias("phrase_type", getResourceManager().getText("PhraseBeanAlias.phrase_type", TradePortalConstants.TEXT_BUNDLE));
      
        /* Pointer to the parent ReferenceDataOwner */
      attributeMgr.registerAttribute("owner_org_oid", "p_owner_org_oid", "ParentIDAttribute");
   
   }
   
 
   
 
 
   
}
