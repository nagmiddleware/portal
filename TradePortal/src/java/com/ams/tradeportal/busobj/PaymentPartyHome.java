
  

package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;
/**
 * A party of Payment instrument.  It should be a BankBranch.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface PaymentPartyHome extends EJBHome
{
   public PaymentParty create()
      throws RemoteException, CreateException, AmsException;

   public PaymentParty create(ClientServerDataBridge csdb)
      throws RemoteException, CreateException, AmsException;
}
