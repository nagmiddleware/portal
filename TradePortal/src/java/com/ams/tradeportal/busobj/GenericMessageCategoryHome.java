
  

package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;
/**
 * Business object to store generic message categories that comprise instruments
 * created in the Trade Portal.  
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface GenericMessageCategoryHome extends EJBHome
{
   public GenericMessageCategory create()
      throws RemoteException, CreateException, AmsException;

   public GenericMessageCategory create(ClientServerDataBridge csdb)
      throws RemoteException, CreateException, AmsException;
}
