

package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;




/*
 * The alias for the AR Buyer ID in an ARMatchingRule
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class ArBuyerIdAliasBean_Base extends TradePortalBusinessObjectBean
{
private static final Logger LOG = LoggerFactory.getLogger(ArBuyerIdAliasBean_Base.class);

  
  /* 
   * Register the attributes and associations of the business object
   */
   protected void registerAttributes() throws AmsException
   {  
      /* ar_buyer_id_alias_oid - Unique Identifier */
      attributeMgr.registerAttribute("ar_buyer_id_alias_oid", "ar_buyer_id_alias_oid", "ObjectIDAttribute");
      
      /* buyer_id_alias - Buyer ID Alias */
      attributeMgr.registerAttribute("buyer_id_alias", "buyer_id_alias");
      
      attributeMgr.registerAlias("buyer_id_alias", getResourceManager().getText("ArBuyerIdAliasBeanAlias.buyer_id_alias", TradePortalConstants.TEXT_BUNDLE));
      
        /* Pointer to the parent ArMatchingRule */
      attributeMgr.registerAttribute("ar_matching_rule_oid", "p_ar_matching_rule_oid", "ParentIDAttribute");
   
   }
   
 
   
 
 
   
}
