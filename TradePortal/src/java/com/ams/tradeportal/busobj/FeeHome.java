package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;
/**
 * A fee that is associated with a transaction.   This table is populated by
 * the middleware when unpackaging data that has come from OTL.
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface FeeHome extends EJBHome
{
   public Fee create()
      throws RemoteException, CreateException, AmsException;

   public Fee create(ClientServerDataBridge csdb)
      throws RemoteException, CreateException, AmsException;
}
