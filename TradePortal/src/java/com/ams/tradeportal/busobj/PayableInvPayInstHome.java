
  

package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;
/**
 * The Payable Invoice Payment Instructions for Bank defined Trading Parter.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface PayableInvPayInstHome extends EJBHome
{
   public PayableInvPayInst create()
      throws RemoteException, CreateException, AmsException;

   public PayableInvPayInst create(ClientServerDataBridge csdb)
      throws RemoteException, CreateException, AmsException;
}
