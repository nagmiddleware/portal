
  

package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;


/*
 * FavoriteTaskCustomization.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class FavoriteTaskCustomizationBean_Base extends TradePortalBusinessObjectBean
{
private static final Logger LOG = LoggerFactory.getLogger(FavoriteTaskCustomizationBean_Base.class);

  
  /* 
   * Register the attributes and associations of the business object
   */
   protected void registerAttributes() throws AmsException
   {  
	   
	  /* Register attributes defined in the Ancestor class */
	      super.registerAttributes();
	      
      /* favorite_task_cust_oid - Unique Identifier. */
      attributeMgr.registerAttribute("favorite_task_cust_oid", "favorite_task_cust_oid", "ObjectIDAttribute");
      
      /* user_oid -  */
      attributeMgr.registerAssociation("user_oid", "a_user_oid", "User");
      attributeMgr.requiredAttribute("user_oid");
      
      /* server_instance_name - The name of the server instance to which the user logged in. */
      attributeMgr.registerAttribute("display_order", "display_order", "NumberAttribute");
      attributeMgr.requiredAttribute("display_order");

      /* server_instance_name - The name of the server instance to which the user logged in. */
      attributeMgr.registerAttribute("favorite_id", "favorite_id");
     attributeMgr.requiredAttribute("favorite_id");
            
      /* opt_lock - Optimistic lock attribute
      See jPylon documentation for details on how this works */
      attributeMgr.registerAttribute("opt_lock", "opt_lock", "OptimisticLockAttribute");
      
      /* favorite_type - this will store favorite indicator. 
       *                 R- Report
       *                 T- Task. */
      attributeMgr.registerAttribute("favorite_type", "favorite_type");
      
      attributeMgr.registerAttribute("addl_params", "addl_params");
      
      
                 
   }
   
   
}
