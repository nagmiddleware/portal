package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;


/**
 * This is the rule that a corporate customer can set up to match AR payment
 * and invoices.  This rule is sent to OTL.  It is not used in TP otherwise.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface ArMatchingRule extends TradePortalBusinessObject
{   
}
