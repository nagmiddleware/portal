package com.ams.tradeportal.busobj;


/**
 * Describes the mapping of an uploaded file to Invoice Definition stored
 * in the database.   The fields contained in the file are described, the file
 * format is specified, and the goods description format is provided.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */

public interface CreditNotesGoods extends TradePortalBusinessObject {

}
