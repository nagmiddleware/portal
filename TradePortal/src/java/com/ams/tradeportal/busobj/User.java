

  

package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;


/**
 * A user of the Trade Portal system.   Each active user in this table is able
 * to log into the Trade Portal and perform actions for their organization.
 * Users can exist for corporate organizations, bank organization groups, client
 * banks, and the global organization.
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface User extends TradePortalBusinessObject
{   
	public Hashtable getSecurityLimits() throws RemoteException, AmsException;

	//cquinton 1/23/2012 Rel 7.1 ir#cnum012364273 start
	/**
	 * Validate on changing registered SSO ID separately from standard user validation.
	 */
	public void validateChangingRegisteredSSOId() throws RemoteException, AmsException;
    //cquinton 1/23/2012 Rel 7.1 ir#cnum012364273 end
	
    //cquinton 2/15/2012 Rel 8.0 ppx255 start
    /**
     * isPasswordExpired
     */
    public boolean isPasswordExpired() throws AmsException, RemoteException;
    //cquinton 2/15/2012 Rel 8.0 ppx255 end
}
