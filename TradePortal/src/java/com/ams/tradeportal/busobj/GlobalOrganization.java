

  

package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;


/**
 * Represents the organization that provides trade processing services (or
 * in some cases, just the software) to the client banks.  In the hierarchy
 * of organizations, this is the highest.   Client banks are owned by this
 * organization.
 * 
 * Under the current design, there will only ever be one of these objects in
 * the database.

 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface GlobalOrganization extends ReferenceDataOwner
{   
}
