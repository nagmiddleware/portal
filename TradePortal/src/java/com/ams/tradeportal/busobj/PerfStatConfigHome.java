package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;
/**
 * The configuration of the application.  These configuration entries used
 * to reside on the properties files.  They are moved to database for easier
 * management.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface PerfStatConfigHome extends EJBHome
{
   public PerfStatConfig create()
      throws RemoteException, CreateException, AmsException;

   public PerfStatConfig create(ClientServerDataBridge csdb)
      throws RemoteException, CreateException, AmsException;
}
