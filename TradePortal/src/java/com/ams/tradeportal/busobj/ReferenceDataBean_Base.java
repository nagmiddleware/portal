
  

package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;




/*
 * Abstract business object that is the superclass for reference data that
 * can be owned by organizations at multiple levels.
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class ReferenceDataBean_Base extends TradePortalBusinessObjectBean
{
private static final Logger LOG = LoggerFactory.getLogger(ReferenceDataBean_Base.class);

  
  /* 
   * Register the attributes and associations of the business object
   */
   protected void registerAttributes() throws AmsException
   {  

      /* Register attributes defined in the Ancestor class */
      super.registerAttributes();
      
      /* name - The name of the piece of reference data. */
      attributeMgr.registerAttribute("name", "name");
      attributeMgr.requiredAttribute("name");
      attributeMgr.registerAlias("name", getResourceManager().getText("ReferenceDataBeanAlias.name", TradePortalConstants.TEXT_BUNDLE));
      
      /* ownership_level - The level at which the reference data is owned (global, client bank, bank
         organization group, or corporate customer */
      attributeMgr.registerReferenceAttribute("ownership_level", "ownership_level", "OWNERSHIP_LEVEL");
      attributeMgr.requiredAttribute("ownership_level");
      
      /* opt_lock - Optimistic lock attribute
         See jPylon documentation for details on how this works */
      attributeMgr.registerAttribute("opt_lock", "opt_lock", "OptimisticLockAttribute");
      
      /* ownership_type - This attribute is used to populate the "Added By" column of listviews. 
         If the ownership level is set to corporate customer, the ownership type
         is set to non-admin.  If ownership level is set to anything else, the ownership
         type is admin. */
      attributeMgr.registerReferenceAttribute("ownership_type", "ownership_type", "OWNERSHIP_TYPE");
      
   }
   
 
   
 
 
   
}
