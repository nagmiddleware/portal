package com.ams.tradeportal.busobj;
import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.util.HashSet;
import java.util.Set;
import java.util.Vector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.busobj.util.InstrumentServices;
import com.ams.tradeportal.common.InstrumentType;
import com.ams.tradeportal.common.MessageType;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.ComponentList;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.frame.EJBObjectFactory;
import com.amsinc.ecsg.frame.ObjectIDCache;
import com.amsinc.ecsg.util.DateTimeUtility;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.StringFunction;


/*
 *
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class ArMatchingRuleBean extends ArMatchingRuleBean_Base
{
private static final Logger LOG = LoggerFactory.getLogger(ArMatchingRuleBean.class);
	 /**
	    * Since the Data Display for the business object is broken up into 5 different 'views'
	    * it is only appropriate to validate the data in 3 different methods ofr varying attributes.
	    * To determine which attribute is validated, the value in the 'part_to_validate' attribute
	    * is analyzed.  The rest of the code is based on the required business rules.

	    */
	public final static String MAX_DAYS_BEFORE_AFTER   = "999"; //DK IR DHUM050755025 Rel 8.0 05/10/2012
	   
	public void userValidate() throws java.rmi.RemoteException, com.amsinc.ecsg.frame.AmsException {			
			

			  	this.validateName();
				this.validateDupBuyerNameAlias();
				this.validateDupBuyerIDAlias();              		

	             
	          this.validateInvoiceDefinitionTab ();	    	         
	      
			 this.validateAmountandPercent();

	          this.validateAuthorisationsTab ();

	              this.validateBankDefRulesTab ();
	              this.duplicateMarginRules();

        if (!StringFunction.canEncodeIn_WIN1252(getAttribute("tps_customer_id"))) {
            this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                    TradePortalConstants.UNICODE_NOT_ALLOWED,
                    getResourceManager().getText("TradingPartnerDetails.TPSCustomerID", 
                            TradePortalConstants.TEXT_BUNDLE));      
        }

   }

	// SHR CR708 Rel8.1.1 Start
	/**
	 * This method performs the validation for Invoices Tab
	 * 
	 * @throws java.rmi.RemoteException
	 * @throws com.amsinc.ecsg.frame.AmsException
	 */
	protected void preSave() throws AmsException {
		super.preSave();

		try {
			
			// DK CR709 Rel8.2 Begins
			if ((InstrumentType.LOAN_RQST.equals(getAttribute("pay_instrument_type")))
					&& StringFunction
							.isBlank(getAttribute("pay_loan_type")) || (InstrumentType.LOAN_RQST.equals(getAttribute("rec_instrument_type")))
					&& StringFunction
							.isBlank(getAttribute("rec_loan_type"))) {
				this.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.WARNING_NO_LOAN_TYPE);
			}
			// DK CR709 Rel8.2 Ends
		} catch (RemoteException e) {
			LOG.error("Exception caught in ArMatchingRuleBean.validateInvoiceDefinitionTab()",e);
		}
	}

	// SHR CR708 Rel8.1.1 End

	public void validateInvoiceDefinitionTab() throws java.rmi.RemoteException,
			com.amsinc.ecsg.frame.AmsException {

		         LOG.trace("========= Validating the invoiceDefinitionTab1  data in the Trade PartnerRules Object ==========");			     
				 }
	   /**
	    * This method performs validation for Receivable Management tab
	    * @throws java.rmi.RemoteException
	    * @throws com.amsinc.ecsg.frame.AmsException
	    */
	   public void validateReceivableManagementTab ()
	          throws java.rmi.RemoteException, com.amsinc.ecsg.frame.AmsException {
		   		validateAmountandPercent();
	         LOG.trace("========= Validating the ReceivableManagement Tab data in the Trade PartnerRules Object ==========");

	    	 
	   }
	   
	   /**
	    * This method performs validations on Authorizations tab data.
	    * @throws java.rmi.RemoteException
	    * @throws com.amsinc.ecsg.frame.AmsException
	    */
	    public void validateAuthorisationsTab ()
		          throws java.rmi.RemoteException, com.amsinc.ecsg.frame.AmsException {
				//Srini_D IR-KRUM010338190 01/23/2012 Rel8.0 Start
				 boolean cond=true;
		         LOG.trace("========= Validating the Authorisations Tab data in the Trade PartnerRules Object ==========");		         
		         String receivableInvoice = this.getAttribute("receivable_invoice");
		         String receivableAuthorise = this.getAttribute("receivable_authorise");
		         String creditNote = this.getAttribute("credit_note");
		         String creditAuthorise = this.getAttribute("credit_authorise");
		    	// Narayan IR - ACUM031962698 Begin - modifed code to correct validation
		    	if(TradePortalConstants.INDICATOR_YES.equalsIgnoreCase(receivableInvoice) && TradePortalConstants.INDICATOR_YES.equalsIgnoreCase(creditNote)) { 
		    		if(StringFunction.isBlank(receivableAuthorise) && StringFunction.isBlank(creditAuthorise)){
		    			cond = false;
		    			this.getErrorManager().issueError(
		    					TradePortalConstants.ERR_CAT_1,
		       					TradePortalConstants.AUTHORIZATION_MANDATORY);	        	 
		    		}
				}
		    	if(cond && TradePortalConstants.INDICATOR_YES.equalsIgnoreCase(receivableInvoice)){
		    		if(StringFunction.isBlank(receivableAuthorise)){
		    			this.getErrorManager().issueError(
		    					TradePortalConstants.ERR_CAT_1,
		       					TradePortalConstants.INV_AUTHORIZATION_MANDATORY);	 
		    		}
		    		
		       }
               if(cond && TradePortalConstants.INDICATOR_YES.equalsIgnoreCase(creditNote)){
		    	 if(StringFunction.isBlank(creditAuthorise)){
		    			this.getErrorManager().issueError(
		    					TradePortalConstants.ERR_CAT_1,
		       					TradePortalConstants.CRED_AUTHORIZATION_MANDATORY);	        	 
		    		}
		      }
                
	   }
	    /**
	     * This method performs validation on Bank-Def tab in Trading Partner Rule.
	     * @throws java.rmi.RemoteException
	     * @throws com.amsinc.ecsg.frame.AmsException
	     */
	   public void validateBankDefRulesTab ()
		          throws java.rmi.RemoteException, com.amsinc.ecsg.frame.AmsException {

		         LOG.trace("========= Validating the BankDefinedRules Tab data in the Trade PartnerRules Object ==========>>");
		         String instrumentType="";
		         String margin ="";
		         String currencyCode="";
		         String thresholdAmt = "";
		         String rateType = "";
		         
		         String invoiceFianaceValue = this.getAttribute("invoice_value_finance");
		         String paymentDayAllowed = this.getAttribute("payment_day_allow");
		         String daysBefore = this.getAttribute("days_before");
		         String daysAfter = this.getAttribute("days_after");
                //RKAZi IR MMUM020953219 Rel 8.0 02/29/2012 added this field
                 String daysFinanceOfPayment = this.getAttribute("days_finance_of_payment");
		         
		         if(!StringFunction.isBlank(paymentDayAllowed) && paymentDayAllowed.equalsIgnoreCase("Y")){		        
		        	 
		        	 if(StringFunction.isBlank(daysBefore) || StringFunction.isBlank(daysAfter)){
		        		 this.getErrorManager().issueError(
		       					TradePortalConstants.DAYS_BEFORE_AFTER_REQUIRED,
		       					getResourceManager().getText("TradingPartnerDetails.BankDefinedRulesMarginErrorMsg1",TradePortalConstants.TEXT_BUNDLE));
		        	 }
		         }
                   //RKAZI IR MMUM020953219 Rel 8.0 02/29/2012 Added validations - Start
                   if (!StringFunction.isBlank(daysBefore)){
                       if (Integer.parseInt(daysBefore) <= 0){
                           this.getErrorManager().issueError(
                                   TradePortalConstants.DAYS_BEFORE_AFTER_REQUIRED,
                                   getResourceManager().getText("TradingPartnerDetails.BankDefinedRulesMarginErrorMsg9",TradePortalConstants.TEXT_BUNDLE));
                       }
                       //DK IR DHUM050755025 Rel 8.0 05/10/2012 Begin
                       if (Integer.parseInt(daysBefore) > 999){
                    	   this.getErrorManager().issueError(
                					TradePortalConstants.ERR_CAT_1,
                					TradePortalConstants.MUST_BE_LESS_THAN,
                					getResourceManager().getText("TradingPartnerDetails.BankDefinedRulesDaysBefore",
                				    	    TradePortalConstants.TEXT_BUNDLE),
                				    	    MAX_DAYS_BEFORE_AFTER);
                    	   
                       }
                       //DK IR DHUM050755025 Rel 8.0 05/10/2012 End                       
                   }
                   if (!StringFunction.isBlank(daysAfter)){
                       if (Integer.parseInt(daysAfter) <= 0){
                           this.getErrorManager().issueError(
                                   TradePortalConstants.DAYS_BEFORE_AFTER_REQUIRED,
                                   getResourceManager().getText("TradingPartnerDetails.BankDefinedRulesMarginErrorMsg10",TradePortalConstants.TEXT_BUNDLE));

                       }
                       //DK IR DHUM050755025 Rel 8.0 05/10/2012 Begin
                       if (Integer.parseInt(daysAfter) > 999){                    	                  	   
                    	   this.getErrorManager().issueError(
                 					TradePortalConstants.ERR_CAT_1,
                 					TradePortalConstants.MUST_BE_LESS_THAN,
                 					getResourceManager().getText("TradingPartnerDetails.BankDefinedRulesDaysAfter",
                 				    	    TradePortalConstants.TEXT_BUNDLE),
                 				    	    MAX_DAYS_BEFORE_AFTER);                    	   
                       }
                      //DK IR DHUM050755025 Rel 8.0 05/10/2012 End 
                   }                   
                   if (!StringFunction.isBlank(daysFinanceOfPayment)){
                       if (Integer.parseInt(daysFinanceOfPayment) <= 0){
                           this.getErrorManager().issueError(
                                   TradePortalConstants.DAYS_BEFORE_AFTER_REQUIRED,
                                   getResourceManager().getText("TradingPartnerDetails.BankDefinedRulesMarginErrorMsg11",TradePortalConstants.TEXT_BUNDLE));

                       }
                   }
                   //RKAZI IR MMUM020953219 Rel 8.0 02/29/2012 Added validations - End
				 if( validateFinanceValue(invoiceFianaceValue))
					 InstrumentServices.validateDecimalNumber(invoiceFianaceValue,3,2,"TradingPartnerDetails.BankDefinedRulesInvoiceValue",
			                 this.getErrorManager(),
			                 this.getResourceManager());
		    	
				 ComponentList marginList = (ComponentList)this.getComponentHandle("TradePartnerMarginRulesList");

					int marginsCount = marginList.getObjectCount();

					
					for(int i=0; i< marginsCount; i++)
				    {
						marginList.scrollToObjectByIndex(i);
				    	 currencyCode = marginList.getBusinessObject().getAttribute("currency_code");
				    	 instrumentType = marginList.getBusinessObject().getAttribute("margin_instrument_type");
				    	 thresholdAmt = marginList.getBusinessObject().getAttribute("threshold_amount");
				    	 rateType = marginList.getBusinessObject().getAttribute("rate_type");
				    	 margin = marginList.getBusinessObject().getAttribute("margin");
				    	
				    	if((StringFunction.isBlank(margin)) && ((!StringFunction.isBlank(currencyCode)) || (!StringFunction.isBlank(instrumentType))||
				    			(!StringFunction.isBlank(rateType)))){
				    
				    		String errorParameter2= getResourceManager().getText("TradingPartnerDetails.BankDefinedRulesMarginErrorLine",  TradePortalConstants.TEXT_BUNDLE) +" #: "+(i+1)+" " +
						    	    getResourceManager().getText("TradingPartnerDetails.BankDefinedRulesMarginErrorMsg2",
								    	    TradePortalConstants.TEXT_BUNDLE);
							this.getErrorManager().issueError(
		  	    					TradePortalConstants.TRADE_MARGIN_REQUIRED,		  	    					
		  	    					errorParameter2);
				    	}
				    	//IR 18534 - start
				    	if((StringFunction.isBlank(rateType)) && (StringFunction.isNotBlank(currencyCode) || StringFunction.isNotBlank(instrumentType)||
				    			StringFunction.isNotBlank(margin) || StringFunction.isNotBlank(thresholdAmt))){
				    		this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,TradePortalConstants.TP_RATE_TYPE_REQUIRED, String.valueOf(i+1), null);
							
				    	}
				    	//IR 18534 - end
				    	if((!StringFunction.isBlank(thresholdAmt)) && (StringFunction.isBlank(currencyCode))){
			
				    		String errorParameter2= getResourceManager().getText("TradingPartnerDetails.BankDefinedRulesMarginErrorLine",  TradePortalConstants.TEXT_BUNDLE) +" #: "+(i+1)+" " +
						    	    getResourceManager().getText("TradingPartnerDetails.BankDefinedRulesMarginErrorMsg3",
								    	    TradePortalConstants.TEXT_BUNDLE);
							this.getErrorManager().issueError(		  	    					
		  	    					TradePortalConstants.CURRENCY_CODE_REQUIRED,
		  	    					errorParameter2);
				    	}		    	

				    }
					// Narayan CR-913 Rel9.0 28-Jan-2014 Begin
					// check for duplicate currencies in Payable Invoice payment instruction.
					ComponentList payableInvPayInstrList = (ComponentList)this.getComponentHandle("PayableInvPayInstList");
					String payableInvCurrCode = null;
					Set<String> payableCurrencySet = new HashSet<String>();
					for(int i=0; i< payableInvPayInstrList.getObjectCount(); i++)
				    {
						payableInvPayInstrList.scrollToObjectByIndex(i);
						payableInvCurrCode = payableInvPayInstrList.getBusinessObject().getAttribute("currency_code");
						if(!payableCurrencySet.add(payableInvCurrCode)){
							this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.DUPLICATE_PAY_INV_CURR, payableInvCurrCode);
						}						
					}
					// Narayan CR-913 Rel9.0 28-Jan-2014 End
		   }  
	   
	   /**
	    * This method checks if the buyer name is already exist in the db and throws error if duplicated 
	    * @throws java.rmi.RemoteException
	    * @throws com.amsinc.ecsg.frame.AmsException
	    */
	public  void validateName() throws java.rmi.RemoteException, com.amsinc.ecsg.frame.AmsException {
		String buyerName = this.getAttribute("buyer_name");
				String buyerId = this.getAttribute("buyer_id");//IR MTUJ021664878 Added Variable
				String orgFilter = " AND P_CORP_ORG_OID=" + this.getAttribute("corp_org_oid") ;//IR MTUJ021664878 Added Variable to filter on Org condition
				if (!isUnique("buyer_name",buyerName,orgFilter)) {
					this.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.ALREADY_EXISTS,
						buyerName,
						attributeMgr.getAlias("buyer_name"));
				}
				//IR MTUJ021664878  Add Begin
				if (!isUnique("buyer_id",buyerId,orgFilter)) {
					this.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.ALREADY_EXISTS,
						buyerId,
						attributeMgr.getAlias("buyer_id"));
				}
		//IR MTUJ021664878  Add End
 	 }
	//Nazia - 12/18/08 - CR-434 - Add Begin
	// Validates Tolerance Plus field.

	private void validateAmountandPercent() throws java.rmi.RemoteException, com.amsinc.ecsg.frame.AmsException{
		String tolerancepercent = this.getAttribute("tolerance_percent_plus");
		String amountPlusMinus = this.getAttribute("tolerance_amount_plus");

		 if( validateIsNumericPercent100(tolerancepercent))
			 InstrumentServices.validateDecimalNumber(tolerancepercent,3,2,"ArMatchingRuleDetail.Percent",
	                 this.getErrorManager(),
	                 this.getResourceManager());


		  tolerancepercent = this.getAttribute("tolerance_percent_minus");

		  if( validateIsNumericPercent100(tolerancepercent))
				 InstrumentServices.validateDecimalNumber(tolerancepercent,3,2,"ArMatchingRuleDetail.Percent",
		                 this.getErrorManager(),
		                 this.getResourceManager());

		  InstrumentServices.validateDecimalNumber(amountPlusMinus,12,2,"ArMatchingRuleDetail.Amount",
	                 this.getErrorManager(),
	                 this.getResourceManager());

		  amountPlusMinus = this.getAttribute("tolerance_amount_minus");
		  InstrumentServices.validateDecimalNumber(amountPlusMinus,12,2,"ArMatchingRuleDetail.Amount",
	                 this.getErrorManager(),
	                 this.getResourceManager());



	}
	
	/**
	 * This method checks the entered value is <100
	 * @param value
	 * @return
	 * @throws java.rmi.RemoteException
	 * @throws com.amsinc.ecsg.frame.AmsException
	 */
	public boolean validateFinanceValue(String value)throws java.rmi.RemoteException, com.amsinc.ecsg.frame.AmsException
	{
		boolean returnVal = false;
		BigDecimal decimalValue;
		BigDecimal validateAgainst = new BigDecimal("100");
		try{
			decimalValue = new BigDecimal(value);
		}
		catch(Exception e) {
			return !returnVal;
		}
        //if negative throw error
        //RKAZI IR MMUM020953219 Rel 8.0 02/22/2012 Throw error if negative value entered - Start
        if(decimalValue.compareTo(BigDecimal.ZERO)==-1){
			this.getErrorManager().issueError(
  					TradePortalConstants.ERR_CAT_1,
  					getResourceManager().getText("TradingPartnerDetails.BankDefinedRulesMarginErrorMsg8", TradePortalConstants.TEXT_BUNDLE));

        }
        //RKAZI IR MMUM020953219 Rel 8.0 02/22/2012 Throw error if negative value entered - End
		//MUST_BE_LESS_THAN
		if(!(validateAgainst.compareTo(decimalValue)==-1))
			returnVal=true;
		else
			this.getErrorManager().issueError(
  					TradePortalConstants.ERR_CAT_1,  					
  					getResourceManager().getText("TradingPartnerDetails.BankDefinedRulesMarginErrorMsg5", TradePortalConstants.TEXT_BUNDLE));
  					


		return returnVal;
	}

	/**
	 * This method performs validation on 100 percent and throws error if value exceeds 100
	 * @param value
	 * @return
	 * @throws java.rmi.RemoteException
	 * @throws com.amsinc.ecsg.frame.AmsException
	 */
	public boolean validateIsNumericPercent100(String value)throws java.rmi.RemoteException, com.amsinc.ecsg.frame.AmsException
	{
		boolean returnVal = false;
		BigDecimal decimalValue;
		BigDecimal validateAgainst = new BigDecimal("100");
		try{
			decimalValue = new BigDecimal(value);
		}
		catch(Exception e) {
			return !returnVal;
		}
		//MUST_BE_LESS_THAN
		if(!(validateAgainst.compareTo(decimalValue)==-1))
			returnVal=true;
		else
			this.getErrorManager().issueError(
  					TradePortalConstants.ERR_CAT_1,
  					TradePortalConstants.MUST_BE_LESS_THAN,
  					getResourceManager().getText("ArMatchingRuleDetail.Percent",
  				    	    TradePortalConstants.TEXT_BUNDLE),
  					getResourceManager().getText("ArMatchingRuleDetail.Maximum",
  				    	    TradePortalConstants.TEXT_BUNDLE));
		return returnVal;
	}


	  // Validates Tolerance Minus field.

		//Nazia - 12/18/08 - CR-434 - Add End

	protected void userNewObject() throws java.rmi.RemoteException, com.amsinc.ecsg.frame.AmsException
  	 {
    	 /* Perform any New Object processing defined in the Ancestor class */
    	 super.userNewObject();
    	 this.setAttribute("creation_date_time", DateTimeUtility.getGMTDateTime());
   	}

	/**
     * This method will validate whether user has entered any deplicate Buyer Name(Alias).
     * Duplicate values are being considered based on case insensitivity. For example if uer has
     * enter values "TEST" and "test", then both values will be consider as same and error will be displayed.
     * @throws java.rmi.RemoteException
	 * @throws com.amsinc.ecsg.frame.AmsException
     */
	protected void validateDupBuyerNameAlias() throws java.rmi.RemoteException, com.amsinc.ecsg.frame.AmsException
	{
		//LSuresh R91 IR T36000026319 - SQLINJECTION FIX
		int i,j,k;
		boolean isDupFound = false;
		String primaryBuyerName = this.getAttribute("buyer_name");
		String matchingRuleOid = this.getAttribute("ar_matching_rule_oid") ;
		StringBuffer selectClause = new StringBuffer("select buyer_name_alias from ar_buyer_name_alias,ar_matching_rule where ");
		selectClause.append("ar_matching_rule_oid = p_ar_matching_rule_oid and p_corp_org_oid = ? ");
		
		//jgadela 09/22/2014 -  R91 IR T36000032455 - Editing a rule and save it, resulted in duplication error.
		selectClause.append(" and ar_matching_rule_oid <> ? ");

		Vector allBuyerNameAliasVector = new Vector();

		DocumentHandler allBuyerNameAliasHandler = DatabaseQueryBean.getXmlResultSet(selectClause.toString(), false,this.getAttribute("corp_org_oid"),matchingRuleOid);
		if(allBuyerNameAliasHandler != null){
			allBuyerNameAliasVector = allBuyerNameAliasHandler.getFragments("/ResultSetRecord");
		}

		ComponentList buyerNameList = (ComponentList)this.getComponentHandle("ArBuyerNameAliasList");

		int buyerNameAliasCount = buyerNameList.getObjectCount();

		for(i=0; i< buyerNameAliasCount; i++)
  	    {
  	    	buyerNameList.scrollToObjectByIndex(i);
  	    	String buyerName = buyerNameList.getBusinessObject().getAttribute("buyer_name_alias");

  	    	String errorParameter1;
  	    	String errorParameter2=getResourceManager().getText("ArMatchingRuleDetail.AliasNumber",
		    	    TradePortalConstants.TEXT_BUNDLE);


  	    	if (primaryBuyerName.equalsIgnoreCase(buyerName))
  	    	{
  	    		errorParameter1 = buyerName + "-" + getResourceManager().getText("ArMatchingRuleDetail.BuyerName",
				    	    TradePortalConstants.TEXT_BUNDLE);
  	    		errorParameter2 =  getResourceManager().getText("ArMatchingRuleDetail.AliasNumber",
			    	    TradePortalConstants.TEXT_BUNDLE);
  	    		this.getErrorManager().issueError(
  	  				TradePortalConstants.ERR_CAT_1,
  	  				TradePortalConstants.ALREADY_EXISTS,
  	  			errorParameter1  ,errorParameter2);
  	    	}

  	    	for (j=i+1; j<buyerNameAliasCount; j++)
  	    	{
  	    		buyerNameList.scrollToObjectByIndex(j);
  	    		if (buyerName.equalsIgnoreCase(buyerNameList.getBusinessObject().getAttribute("buyer_name_alias")))
  	    		{
  	    			isDupFound = true;
  	    			this.getErrorManager().issueError(
  	    					TradePortalConstants.ERR_CAT_1,
  	    					TradePortalConstants.ALREADY_EXISTS,
  	    					buyerName,
  	    					errorParameter2);
  	    		}

  	    	}

  	    	if(isDupFound) break;
  	    	for (k=0; k<allBuyerNameAliasVector.size(); k++)
			{
				DocumentHandler resultRow = (DocumentHandler)allBuyerNameAliasVector.elementAt(k);
				if (buyerName.equalsIgnoreCase(resultRow.getAttribute("/BUYER_NAME_ALIAS")))
				{
					errorParameter2= getResourceManager().getText("ArMatchingRuleDetail.AliasNumber",
				    	    TradePortalConstants.TEXT_BUNDLE) +
				    	    getResourceManager().getText("ArMatchingRuleDetail.errorMessage",
						    	    TradePortalConstants.TEXT_BUNDLE)+
						    	    getResourceManager().getText("ArMatchingRuleDetail.BuyerName",
								    	    TradePortalConstants.TEXT_BUNDLE)	    ;
					this.getErrorManager().issueError(
  	    					TradePortalConstants.ERR_CAT_1,
  	    					TradePortalConstants.ALREADY_EXISTS,
  	    					buyerName,
  	    					errorParameter2);
				}

			}
  	    }

	}

	/**
     * This method will validate whether user has entered any duplicate Buyer ID(Alias).
     * Duplicate values are being considered based on case insensitivity. For example if user has
     * enter values "TEST" and "test", then both values will be consider as same and error will be displayed.
     * @throws java.rmi.RemoteException
	 * @throws com.amsinc.ecsg.frame.AmsException
     */
	protected void validateDupBuyerIDAlias() throws java.rmi.RemoteException, com.amsinc.ecsg.frame.AmsException
	{
		// LSuresh R91 IR T36000026319 - SQLINJECTION FIX
		int i,j,k;
		boolean isDupFound = false;
		String primaryBuyerId = this.getAttribute("buyer_id");
		String matchingRuleOid = this.getAttribute("ar_matching_rule_oid") ;
		
		StringBuilder selectClause = new StringBuilder("SELECT AR_BUYER_ID_ALIAS.BUYER_ID_ALIAS ");
		selectClause.append(" FROM AR_BUYER_ID_ALIAS,AR_MATCHING_RULE WHERE ");
		selectClause.append(" AR_MATCHING_RULE.AR_MATCHING_RULE_OID = AR_BUYER_ID_ALIAS.P_AR_MATCHING_RULE_OID AND ");
		
		selectClause.append(" AR_MATCHING_RULE.P_CORP_ORG_OID = ? ");
	
		//jgadela 09/22/2014 -  R91 IR T36000032850 - Fixed duplication error.
		selectClause.append(" and ar_matching_rule_oid <> ? ");

		Vector allBuyerIDAliasVector = new Vector();

		DocumentHandler allBuyerIdAliasHandler = DatabaseQueryBean.getXmlResultSet(selectClause.toString(), false,this.getAttribute("corp_org_oid"),matchingRuleOid);
		if(allBuyerIdAliasHandler != null){
			allBuyerIDAliasVector = allBuyerIdAliasHandler.getFragments("/ResultSetRecord");
		}

		ComponentList buyerIdList = (ComponentList)this.getComponentHandle("ArBuyerIdAliasList");

		int buyerIdAliasCount = buyerIdList.getObjectCount();

		
		for(i=0; i< buyerIdAliasCount; i++)
	    {
  	    	buyerIdList.scrollToObjectByIndex(i);
	    	String buyerId = buyerIdList.getBusinessObject().getAttribute("buyer_id_alias");
	    	
	    	String errorParameter1;
	    	String errorParameter2=getResourceManager().getText("ArMatchingRuleDetail.AliasId",
	    	    TradePortalConstants.TEXT_BUNDLE);
	    	if (primaryBuyerId.equalsIgnoreCase(buyerId))
  	    	{
	    		errorParameter1 = buyerId + "-" + getResourceManager().getText("ArMatchingRuleDetail.BuyerId", TradePortalConstants.TEXT_BUNDLE);
	    		this.getErrorManager().issueError(
	    					TradePortalConstants.ERR_CAT_1,
	    					TradePortalConstants.ALREADY_EXISTS,
	    					errorParameter1,
	    					errorParameter2);
  	    	}
	    	for (j=i+1; j<buyerIdAliasCount; j++)
	    	{
	    		buyerIdList.scrollToObjectByIndex(j);
	    		if (buyerId.equalsIgnoreCase(buyerIdList.getBusinessObject().getAttribute("buyer_id_alias")))
	    		{
	    			isDupFound = true;
	    			this.getErrorManager().issueError(
  	    					TradePortalConstants.ERR_CAT_1,
  	    					TradePortalConstants.ALREADY_EXISTS,
  	    					buyerId,
  	    					errorParameter2);
	    		}

	    	}
	    	if(isDupFound) break;
	    	for (k=0; k<allBuyerIDAliasVector.size(); k++)
			{
				DocumentHandler resultRow = (DocumentHandler)allBuyerIDAliasVector.elementAt(k);
				if (buyerId.equalsIgnoreCase(resultRow.getAttribute("/BUYER_ID_ALIAS")))
				{
					errorParameter2= getResourceManager().getText("ArMatchingRuleDetail.AliasId",
				    	    TradePortalConstants.TEXT_BUNDLE) +
				    	    getResourceManager().getText("ArMatchingRuleDetail.errorMessage",
						    	    TradePortalConstants.TEXT_BUNDLE)+
						    	    getResourceManager().getText("ArMatchingRuleDetail.BuyerId",
								    	    TradePortalConstants.TEXT_BUNDLE)	    ;
					this.getErrorManager().issueError(
  	    					TradePortalConstants.ERR_CAT_1,
  	    					TradePortalConstants.ALREADY_EXISTS,
  	    					buyerId,
  	    					errorParameter2);
				}

			}


	    }



	}

/**
 * Delete functionality of the Records
 * @throws java.rmi.RemoteException
 * @throws com.amsinc.ecsg.frame.AmsException
 */
public void userDelete() throws java.rmi.RemoteException, com.amsinc.ecsg.frame.AmsException
	{
		//BSL IR NEUM041853493 04/23/2012 Rel 8.0 BEGIN
		// Check if this rule is being used anywhere
		if (isUsed()) {
			getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.ERROR_RULE_IS_USED);

			return;
		}
		//BSL IR NEUM041853493 04/23/2012 Rel 8.0 END

		try
		{
		CorporateOrganization corpOrg = null;
		OperationalBankOrganization operationalBankOrganization = null;
		
		String corpOrgID = this.getAttribute("corp_org_oid");
		String customerID = "";
		String bankOrgID = "";
		String operationOrganizationID = "";
		String buyerIDPrimary = "";
		String buyerNamePrimary = "";
		String processParm = "";
		
		corpOrg = (CorporateOrganization) EJBObjectFactory.createServerEJB(getSessionContext(),"CorporateOrganization",Long.parseLong(corpOrgID));
		customerID = corpOrg.getAttribute("Proponix_customer_id");
		
		bankOrgID = corpOrg.getAttribute("first_op_bank_org");
	    operationalBankOrganization = (OperationalBankOrganization) EJBObjectFactory.createServerEJB(getSessionContext(),"OperationalBankOrganization", Long.parseLong(bankOrgID));
	    operationOrganizationID = operationalBankOrganization.getAttribute("Proponix_id");
		
	    buyerIDPrimary = this.getAttribute("buyer_id");
		buyerNamePrimary = this.getAttribute("buyer_name");
		
		processParm = "CustomerID=" + customerID + "|" + "OperationOrganizationID=" + operationOrganizationID + "|" + "BuyerIDPrimary=" + buyerIDPrimary + "|" + "BuyerNamePrimary=" + buyerNamePrimary + "|" + "DeletedInd=Y";
				
		OutgoingInterfaceQueue oiQueue = null;
	   	oiQueue = (OutgoingInterfaceQueue) EJBObjectFactory.createServerEJB(getSessionContext(),"OutgoingInterfaceQueue");
		oiQueue.newObject();
		
		String attributeNames[] = new String[] {"date_created", "status","msg_type", "message_id", "transaction_oid", "process_parameters"};
		
		String messageId = Long.toString(ObjectIDCache.getInstance(AmsConstants.MESSAGE).generateObjectID(false));
		String attributeValues[] = new String[] {DateTimeUtility.getGMTDateTime(),
		        TradePortalConstants.OUTGOING_STATUS_STARTED,
		    	MessageType.RPMRULE,
		    	messageId,
		    	Long.toString(this.getObjectID()),
		    	processParm};
		
		oiQueue.setAttributes(attributeNames, attributeValues);
	    	oiQueue.save();
		}
		catch(Exception e)
		{
			LOG.error("Exception caught in ArMatchingRuleBean.userDelete()",e);
		}
		
	}

	//BSL IR NEUM041853493 04/23/2012 Rel 8.0 BEGIN
	/**
	 * This method determines if this rule is currently being
	 * used by an active invoice.
	 *
	 * @return boolean - true = is being used, false = not used
	 */
	public boolean isUsed() {
		boolean isUsed = true;
		
		try {
			String corpOrgOid = getAttribute("corp_org_oid");
			String buyerName = getAttribute("buyer_name");
			
			// since both buyer_name and buyer_id must be populated and unique
			// for each rule in the corp org, we need only check one of 
			// buyer_name and buyer_id, rather than both.
			
			//LSuresh R91 IR T36000026319 - SQLINJECTION FIX
			StringBuilder whereClause = new StringBuilder();
			//Srinivasu_D IR#T36000034058 Rel 9.1 10/30/2014 - initialized sqlParams properly
			Object sqlParams[] = new Object[11];
			 
			whereClause.append("a_corp_org_oid = ?");
			sqlParams[0] = corpOrgOid;
			whereClause.append(" and buyer_name = ?");
			sqlParams[1] = buyerName;
			whereClause.append(" and invoice_status in (?,?,?,?,?,?,?,?,?)");
			
			sqlParams[2] = TradePortalConstants.UPLOAD_INV_STATUS_AUTH_FAILED;
		    sqlParams[3] = TradePortalConstants.UPLOAD_INV_STATUS_AVL_FOR_AUTH;
		    sqlParams[4] = TradePortalConstants.UPLOAD_INV_STATUS_AVL_FOR_FIN;
			sqlParams[5] = TradePortalConstants.UPLOAD_INV_STATUS_FIN_AUTH_FAILED;
			sqlParams[6] = TradePortalConstants.UPLOAD_INV_STATUS_FIN_PAR_AUTH;
			sqlParams[7] = TradePortalConstants.UPLOAD_INV_STATUS_PAR_AUTH;
	        sqlParams[8] = TradePortalConstants.UPLOAD_INV_STATUS_PEND_ACT_CREDIT_NOTE;
		    sqlParams[9] = TradePortalConstants.UPLOAD_INV_STATUS_PEND_ACT_NO_INSTR_TYPE;
		    sqlParams[10]= TradePortalConstants.UPLOAD_INV_STATUS_PEND_ACT_UNABLE_TO_FIN;

			int count = DatabaseQueryBean.getCount("upload_invoice_oid",
					"invoices_summary_data", whereClause.toString(),false,sqlParams);

			// If any users were found, the profile is being used
			if (count == 0) {
				isUsed = false;
			}
		} catch (Exception e) {
			
			LOG.error("Exception caught in ArMatchingRuleBean.isUsed()",e);
			// error condition, so assume it is used
			isUsed = true;
		}
	
		return isUsed;
	}
	//BSL IR NEUM041853493 04/23/2012 Rel 8.0 END

	/**
	 * This method returns the duplicate records in Trading Partner Margin Rule table on ui.
	 * If any duplicates found throws error with a row number
	 * @throws java.rmi.RemoteException
	 * @throws com.amsinc.ecsg.frame.AmsException
	 */
	protected void duplicateMarginRules() throws java.rmi.RemoteException, com.amsinc.ecsg.frame.AmsException{
		
		String instrumentType = "";
        String margin = "";
        String currencyCode = "";
        String thresholdAmt = "";
        String errorParameter1;
		int pos=0;
		boolean errorFlag=false;
    	String errorParameter2=getResourceManager().getText("TradingPartnerDetails.BankDefinedRulesMarginErrorMsg4",TradePortalConstants.TEXT_BUNDLE);    	
		ComponentList marginList = (ComponentList)this.getComponentHandle("TradePartnerMarginRulesList");		
		int marginsCount = marginList.getObjectCount();
		Set<String> marginValues = new HashSet<String>();
		
			for(int i=0; i< marginsCount; i++)
		    {
				String marignUniqueValue;
					marginList.scrollToObjectByIndex(i);
					TradePartnerMarginRules tradePartnerMarginRules = (TradePartnerMarginRules)marginList.getBusinessObject();
			    	currencyCode = tradePartnerMarginRules.getAttribute("currency_code");
			    	instrumentType = tradePartnerMarginRules.getAttribute("margin_instrument_type");
			    	thresholdAmt = tradePartnerMarginRules.getAttribute("threshold_amount");
			    	margin = tradePartnerMarginRules.getAttribute("margin");
			    	marignUniqueValue = currencyCode + instrumentType + thresholdAmt;
			    	String digits="";
					String decimal="";
					if(margin!=null && margin.trim().length()>0){
						digits="";
						decimal="";
						pos=margin.indexOf(".");
						if(pos>0){
						   digits=margin.substring(0,pos);
						   decimal=margin.substring(pos+1,margin.length());						
						}
						if(pos<0 && margin.length()>3)
							errorFlag=true;
						if(errorFlag==true || ((digits!=null && digits.length()>3) || (decimal!=null && decimal.length()>6))){
							errorParameter1 = getResourceManager().getText("TradingPartnerDetails.BankDefinedRulesMarginErrorMsg6", TradePortalConstants.TEXT_BUNDLE);
				    		this.getErrorManager().issueError(
				    					TradePortalConstants.ERR_CAT_1,				    					
				    					errorParameter1);
						  }
					}

					pos=0;
					errorFlag=false;
					if(thresholdAmt!=null && thresholdAmt.trim().length()>0){
						digits="";
						decimal="";
						pos=thresholdAmt.indexOf(".");
						if(pos>0){
						digits=thresholdAmt.substring(0,pos);
						decimal=thresholdAmt.substring(pos+1,thresholdAmt.length());
						
						}
						if(pos<0 && thresholdAmt.length()>15)
							errorFlag=true;
						if(errorFlag==true || ((digits!=null && digits.length()>15) || (decimal!=null && decimal.length()>3))){
							errorParameter1 = getResourceManager().getText("TradingPartnerDetails.BankDefinedRulesMarginErrorMsg7", TradePortalConstants.TEXT_BUNDLE);
				    		this.getErrorManager().issueError(
				    					TradePortalConstants.ERR_CAT_1,				    					
				    					errorParameter1);
						  }
					}

			    	if(!marginValues.add(marignUniqueValue)){
			    		errorParameter1 = getResourceManager().getText("TradingPartnerDetails.BankDefinedRulesMarginErrorLine", TradePortalConstants.TEXT_BUNDLE) +" #: "+(i+1)+" " +getResourceManager().getText("TradingPartnerDetails.BankDefinedRulesMarginErrorMsg4",TradePortalConstants.TEXT_BUNDLE);
			    		this.getErrorManager().issueError(
			    					TradePortalConstants.ERR_CAT_1,				    					
			    					errorParameter1,
			    					errorParameter2);	
			    		
			    	}	
		    }
	    }			
}
