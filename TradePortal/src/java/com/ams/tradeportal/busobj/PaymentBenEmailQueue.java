



package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;


/**
 * Placing a row into this table will cause an e-mail to be sent for each beneficiaries
 * of the associated FTDP transaction. 
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface PaymentBenEmailQueue extends TradePortalBusinessObject
{
	   
	   public void createNewPaymentBeneficiaryEmailQueueRecord(String domestic_payment_oid, String requiredStatus) 
	   	  throws RemoteException, AmsException;
}
