package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.RemoteException;
import java.util.Calendar;

import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.util.DateTimeUtility;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.GMTUtility;
import com.amsinc.ecsg.util.StringFunction;


/*
 * 
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class TPCalendarBean extends TPCalendarBean_Base
{
private static final Logger LOG = LoggerFactory.getLogger(TPCalendarBean.class);

	protected void userValidate ()  throws AmsException, RemoteException {
		//validate calendar name is unique
        if (!isUnique("calendar_name", getAttribute("calendar_name"),"" )) {
            this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                      TradePortalConstants.CALENDAR_NAME_NOT_UNIQUE, 
                      getAttribute("calendar_name"));

        }
        
        //validate weekendday1 and weekendday2 are not same
        if (StringFunction.isNotBlank(getAttribute("weekend_day_1")) && 
       		getAttribute("weekend_day_1").equals(getAttribute("weekend_day_2"))) {
            this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                      TradePortalConstants.WEEKEND_DAY1_SAME_AS_WEEKEND_DAY1);

        }
    }
	
	protected void userNewObject() throws AmsException, RemoteException	   {  
	     super.userNewObject();

	     // set creation date
	     this.setAttribute("creation_date", DateTimeUtility.formatDate(GMTUtility.getGMTDateTime(true,true),"MM/dd/yyyy"));
	}  
	

	public void userDelete() throws RemoteException, AmsException
	  {
		// validate TPCalendar is not referenced by Currency Calendar Rule.
	        if (isAssociated("CurrencyCalendarRule", "byTPCalendarOID",
			new String[] {getAttribute("tp_calendar_oid")}))
			this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
			    TradePortalConstants.CALENDAR_IS_REFERECED );
	  }

         public boolean isWeekend(java.util.Calendar date) throws AmsException, RemoteException{
             int dayOfWeek = date.get(Calendar.DAY_OF_WEEK);
             try {
                int weekendDay1 = this.getAttributeInteger("weekend_day_1");
                if (dayOfWeek == weekendDay1) 
                    return true;
             }
             catch (AmsException e) {
                 //weekend_day_1 may be undefined.  Ignore.
             }
             try {
                int weekendDay2 = this.getAttributeInteger("weekend_day_2");
                if (dayOfWeek == weekendDay2) 
                    return true;
             }
             catch (AmsException e) {
                 //weekend_day_2 may be undefined.  Ignore.
             }
             
             return false;
         }
         
         public TPCalendarYear getTPCalendarYear(int year) throws AmsException, RemoteException{
        	//LSuresh R91 IR T36000026319 - SQLINJECTION FIX
             String sqlSelect = "select TP_CALENDAR_YEAR_OID FROM TP_CALENDAR_YEAR WHERE P_TP_CALENDAR_OID =?  and year = ?";
		DocumentHandler calendarYear = DatabaseQueryBean.getXmlResultSet(sqlSelect, false,this.getAttribute("tp_calendar_oid"),year );
		if (calendarYear == null)
		{
			return null;
		}
		String calendarYearOid = calendarYear.getAttribute("/ResultSetRecord(0)/TP_CALENDAR_YEAR_OID"); 
                if (StringFunction.isBlank(calendarYearOid)) {
                    return null;
                }
                return (TPCalendarYear)createServerEJB("TPCalendarYear", Long.parseLong(calendarYearOid));
         }

}
