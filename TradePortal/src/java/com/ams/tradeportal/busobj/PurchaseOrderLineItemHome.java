
  

package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;
/**
 * Represents a purchase order line item that has been uploaded into the Trade
 * Portal.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface PurchaseOrderLineItemHome extends EJBHome
{
   public PurchaseOrderLineItem create()
      throws RemoteException, CreateException, AmsException;

   public PurchaseOrderLineItem create(ClientServerDataBridge csdb)
      throws RemoteException, CreateException, AmsException;
}
