
  

package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;
/**
 *
 */
public interface BankTransactionUpdateHome extends EJBHome
{
   public BankTransactionUpdate create()
      throws RemoteException, CreateException, AmsException;

   public BankTransactionUpdate create(ClientServerDataBridge csdb)
      throws RemoteException, CreateException, AmsException;
}
