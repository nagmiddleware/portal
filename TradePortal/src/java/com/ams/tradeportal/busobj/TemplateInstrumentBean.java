package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import com.ams.tradeportal.common.*;
import com.ams.tradeportal.busobj.util.*;




/*
 *
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class TemplateInstrumentBean extends TemplateInstrumentBean_Base
{
private static final Logger LOG = LoggerFactory.getLogger(TemplateInstrumentBean.class);




  /**
   * This function will call other functions and execute logic that will create a new template.
   * A template instrument and transaction will then be created for the newly created instrument.
   *
   * INPUTDOC - The input doc must contain the following
   *    /instrumentOid ---------- The existing instruments oid or "0" for a new instr.
   *    /userOid ---------------- The oid of the user creating the doc
   *    /ownerOrg --------------- The oid of the user's corporate organization
   *    /copyType --------------- What we copy a new instrument from - possible values:
   *                              (TP CONSTANTS: FROM_INSTR, FROM_TEMPL, & FROM_BLANK)
   *
   * @param DocumentHandler doc - this is a DocumentHandler stored as a char array
   * @return String - the oid of the transaction created from that you can get the instrument and terms
   */
  public String createNew (DocumentHandler doc) throws RemoteException, AmsException
  {
	LOG.debug("entering createNew()");
	String sourceOid = "";
	String templateOid = "";
	String decryptedString = "";
	String instrumentOid = "";
	long longSourceOid = -1;

	try{
		//This is to identify if the instrumentOid is encrypted or not.
		longSourceOid = Long.valueOf(InstrumentServices.parseForOid(doc.getAttribute("/instrumentOid"))).longValue();
		instrumentOid = doc.getAttribute("/instrumentOid");
	}catch(NumberFormatException ne){
		// W Zhu 9/11/2012 Rel 8.1 T36000004579 add key parameter.
		String secretKeyString = csdb.getCSDBValue("SecretKey");
		byte[] keyBytes = com.amsinc.ecsg.util.EncryptDecrypt.base64StringToBytes(secretKeyString);
		javax.crypto.SecretKey secretKey = new javax.crypto.spec.SecretKeySpec(keyBytes, "DESede/CBC/PKCS5Padding");
		decryptedString = EncryptDecrypt.decryptStringUsingTripleDes(doc.getAttribute("/instrumentOid"), secretKey);
		if(decryptedString.contains("/")){
			instrumentOid = (decryptedString.split("/"))[1];
		}else{
			instrumentOid = EncryptDecrypt.decryptStringUsingTripleDes(doc.getAttribute("/instrumentOid"), secretKey);
		}
	}

	sourceOid        = InstrumentServices.parseForOid(instrumentOid);
	templateOid        = InstrumentServices.parseForOid(instrumentOid);

	String copyFrom         = doc.getAttribute("/copyType");
	String ownerOrg         = doc.getAttribute("/ownerOrg");
	String userOid          = doc.getAttribute("/userOid");
	String instrumentType   = doc.getAttribute("/instrumentType");
	//pcutrone - 04-MAR-07 - IR-ANUH021252527 - Template copy fields and errors
	String importIndicator  = doc.getAttribute("/ImportIndicator");

	//rbhaduri - 9th August 06 - IR CGUG071768901 - Get ownership level of the template itself
	String ownerLevel       = doc.getAttribute("/ownerLevel"); //the level of the user's owner org.

	long transactionOid     = -1; //will store the oid of the new transaction

	Instrument sourceInst   = null; //will point to the instrument from which we copy

	/*
	 * Get the Source Instrument's Oid
	 */
	//here we instatiate the template to get the oid of the template instument
	if (copyFrom.equals(TradePortalConstants.FROM_TEMPL) ||
		copyFrom.equals(TradePortalConstants.FROM_BLANK))
	{
		LOG.debug("Copying from a non-default template with oid -> {}" , templateOid);
		Template temp = (Template)createServerEJB("Template");
		temp.getData(Long.parseLong(templateOid));
		sourceOid = temp.getAttribute("c_TemplateInstrument");
		LOG.debug("Got instrument_oid from template -> {}" , sourceOid);

		//IAZ CR-586 IR-PRUK092452162 09/29/10 - Transfer Conf Ind to new template if copied from a template
		doc.setAttribute("/NewTemplate/confidential_indicator", temp.getAttribute("confidential_indicator"));
	}

	//Instantiate the source instrument
	long oid = Long.parseLong(sourceOid);
	LOG.debug("Getting source object with oid -> ' {} '" , oid );
	sourceInst = (Instrument) createServerEJB("Instrument");
	try
	{
		sourceInst.getData(oid);
	}
	catch (InvalidObjectIdentifierException e) //The source oid was invalid
	{
		//ERROR
		LOG.error("NTM_INVALID_INSTR_OID error");
		getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
	                                 TradePortalConstants.NTM_INVALID_INSTR_OID);
		return null;
	}

	// If we weren't passed an instrument type, use the type on the
	// source instrument.
	if (StringFunction.isBlank(instrumentType)) {
		instrumentType = sourceInst.getAttribute("instrument_type_code");
	}
	//pcutrone - 04-MAR-07 - IR-ANUH021252527 - Template copy fields and errors [begin]
	if (StringFunction.isBlank(importIndicator)) {
		importIndicator = sourceInst.getAttribute("import_indicator");
	}
	//pcutrone - 04-MAR-07 - IR-ANUH021252527 - Template copy fields and errors [end]

	String physicalType = InstrumentServices.getPhysicalInstrumentType(instrumentType);
	LOG.debug("The instrument type being created is a: {} which is actually a: {}" ,instrumentType,physicalType);

        String isExpress = doc.getAttribute("/isExpress");

        if( StringFunction.isNotBlank( physicalType ) &&
        		StringFunction.isNotBlank( isExpress )) {
           //IR RAUH092044410 Krishna added for ATP also
            if( isExpress.equals( TradePortalConstants.INDICATOR_ON )  &&
                !(instrumentType.equals( TradePortalConstants.SIMPLE_SLC )   ||
                  instrumentType.equals( InstrumentType.IMPORT_DLC )   ||
                  instrumentType.equals( InstrumentType.REQUEST_ADVISE)||
                  instrumentType.equals( InstrumentType.APPROVAL_TO_PAY)) ) {

                getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                                                              TradePortalConstants.CANT_CREATE_EXPRESS_TEMPLATE);
                return null;
            }
        }


	//IAZ CR-586 08/16/10
	String isFixed = doc.getAttribute("/isFixed");
	if((TradePortalConstants.INDICATOR_ON.equalsIgnoreCase(isFixed)) &&
	   (!InstrumentType.DOM_PMT.equals(instrumentType)))

	{
		getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
				TradePortalConstants.CANT_CREATE_NON_PMT_FIXD_TMLTE);
		return null;
	}
	//IAZ CR-586 08/16/10

	// Validate the instument type (using the physical instrument type --
	// thus Detailed SLC is an SLC (but logically a GUA)).  We validate
	// creation rights based on SLC (rather than GUA)
	LOG.debug("Attempting to validate an instrument type of: {}" , physicalType);
	if (instrumentType == null
		|| !InstrumentServices.canCreateInstrumentType(physicalType)
		|| physicalType.equals(InstrumentType.EXPORT_DLC))
	{
		//ERROR
		LOG.debug("NTM_INVALID_INSTR_TYPE error");
		String instrumentTypeDescr = ReferenceDataManager.getRefDataMgr().getDescr("INSTRUMENT_TYPE",
		                                                                        instrumentType, csdb.getLocaleName());

		getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
		                                              TradePortalConstants.NTM_INVALID_TEMPLATE_TYPE,
		                                              instrumentTypeDescr);
		return null;
	}

	//Create the new instrument
	this.setAttribute("instrument_type_code", physicalType);
	//pcutrone - 04-MAR-07 - IR-ANUH021252527 - Template copy fields and errors
	this.setAttribute("import_indicator", importIndicator);

	//Create the new transaction
	LOG.debug("Creating Transaction component");
	transactionOid = this.newComponent("TransactionList");
	Transaction newTrans = (Transaction) this.getComponentHandle("TransactionList", transactionOid);

	//Get a handle to the transaction from which we will copy
	Transaction sourceTrans = (Transaction) sourceInst.getComponentHandle("TransactionList",
																	 Long.valueOf(sourceInst.getAttribute("original_transaction_oid")).longValue());

	//rbhaduri - 9th August 06 - IR CGUG071768901 - Set ownerOrg to something other than oid
	if (!ownerLevel.equals(TradePortalConstants.OWNER_CORPORATE))
		ownerOrg = TradePortalConstants.OWNER_TYPE_ADMIN;

	newTrans.createNewCopy(sourceTrans, userOid, ownerOrg, instrumentType, true);
	this.setAttribute("original_transaction_oid", newTrans.getAttribute("transaction_oid"));

    //IAZ CM-451 10/29/08 Begin: If this is Domestic Payment template, create copies of all
    //                 domestic payments associated with the original template for the
    //                 new template
	if (instrumentType.equals(InstrumentType.DOM_PMT))
	{
		DomesticPayment domesticPayment = (DomesticPayment) this.createServerEJB("DomesticPayment");
    	domesticPayment.createNewDomesticPayments(sourceTrans.getAttribute("transaction_oid"),
    								  newTrans.getAttribute("transaction_oid"));
    	//IR ACUL011562908 rkazi 01/28/2011 Start
    	try
		{


			//obtain a list of all domestic payments from source DP Transaction/Template
			String sqlQuery = "SELECT DP.DOMESTIC_PAYMENT_OID FROM DOMESTIC_PAYMENT DP WHERE DP.p_transaction_oid = ?" ;

			DocumentHandler resultsDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery,true, new Object[]{newTrans.getAttribute("transaction_oid")});
			if (resultsDoc == null)
			{
				LOG.debug("TemplateInstrumentBean::createNew::The source template/transaction has no Payee informationn to copy");

			}
			else
			{
				int curIndex = 0;
				String originalDomesticPaymentOidStr =
					resultsDoc.getAttribute("/ResultSetRecord(" + curIndex + ")/DOMESTIC_PAYMENT_OID");

				//loop through all the source's domestic payments
				while (
						(originalDomesticPaymentOidStr != null) &&
						(!originalDomesticPaymentOidStr.equals(""))
				)
				{

					long originalDomesticPaymentOid = 0;
					originalDomesticPaymentOid =
						resultsDoc.getAttributeLong("/ResultSetRecord(" + curIndex + ")/DOMESTIC_PAYMENT_OID");

					//dom payment oid must be present for each entry
					if (originalDomesticPaymentOid == 0)
					{
						LOG.debug("TemplateInstrumentBean::createNew::Error: domestic_payment_oid not present");
					}
					else
					{
						//create an instance of the DomesticPaymentBean.
						DomesticPayment originalDomesticPayment = (DomesticPayment) this.createServerEJB("DomesticPayment");
						originalDomesticPayment.getData(originalDomesticPaymentOid);
						//Update the payment status field to blank as this is a template being created.
						originalDomesticPayment.setAttribute("payment_status",null);
						originalDomesticPayment.setAttribute("sequence_number",null);
						originalDomesticPayment.save();
						originalDomesticPayment.remove();
						curIndex++;
						originalDomesticPaymentOidStr =
							resultsDoc.getAttribute("/ResultSetRecord(" + curIndex + ")/DOMESTIC_PAYMENT_OID");
					}

				}
			}
		}
		catch (Exception any_e)
		{
			LOG.error("TemplateInstrumentBean::createNew::Exception: ",any_e);
			this.getErrorManager().issueError(getClass().getName(),
					TradePortalConstants.DOM_PMT_GEN_ERR_WRN);
		}
    	//IR ACUL011562908 Rkazi 01/28/2011 End
	}
    //IAZ CM-451 End

    //IAZ CR-586 IR-PRUK092452162 09/29/10 Iand R-VRUK091652765 10/05/10Begin
    //           Set Confidential Indicator as needed if copied from an instrument

	if (!copyFrom.equals(TradePortalConstants.FROM_TEMPL) &&
		!copyFrom.equals(TradePortalConstants.FROM_BLANK))
	{
    	if (TradePortalConstants.INDICATOR_YES.equals (sourceInst.getAttribute("confidential_indicator")))
    	   doc.setAttribute("/NewTemplate/confidential_indicator", TradePortalConstants.INDICATOR_NO);
    	if (TradePortalConstants.CONF_IND_FRM_TMPLT.equals (sourceInst.getAttribute("confidential_indicator")))
    	   doc.setAttribute("/NewTemplate/confidential_indicator", TradePortalConstants.CONF_IND_FRM_TMPLT);
    	LOG.debug("setting in createNew {} | {}" ,sourceInst.getAttribute("confidential_indicator") , doc.getAttribute("/NewTemplate/confidential_indicator"));
    }

    //           Set Fixed Payment Indicator if Fixed Template
    String fixedFlag = doc.getAttribute("/isFixed");
	if (fixedFlag != null && fixedFlag.equalsIgnoreCase("on"))
		this.setAttribute("fixed_payment_flag", TradePortalConstants.INDICATOR_YES);
	else
		this.setAttribute("fixed_payment_flag", TradePortalConstants.INDICATOR_NO);
    LOG.debug("templateInstrument::fixed payment flag {}" , this.getAttribute("fixed_payment_flag"));

    //IAZ CR-586 IR-PRUK092452162 09/29/10 and IR-VRUK091652765 10/05/10 End

	return newTrans.getAttribute("transaction_oid");
  }




  /**
   * String createNewDefault (char[] chars)
   *
   * This function will call other functions and execute logic that will create a new template.
   * A template instrument and transaction will then be created for the newly created instrument.
   *
   * INPUTDOC - The input doc must contain the following
   *    /instrumentOid ---------- The existing instruments oid or "0" for a new instr.
   *    /userOid ---------------- The oid of the user creating the doc
   *    /ownerOrg --------------- The oid of the user's corporate organization
   *    /copyType --------------- What we copy a new instrument from - possible values:
   *                              (TP CONSTANTS: FROM_INSTR, FROM_TEMPL, & FROM_BLANK)
   *
   * @param DocumentHandler doc - this is a DocumentHandler stored as a char array
   * @return String - the oid of the transaction created from that you can get the instrument and terms
   */
  public String createNewDefault (DocumentHandler doc) throws RemoteException, AmsException
  {
	LOG.debug("entering createNewDefault()");

	String ownerOrg         = doc.getAttribute("/ownerOrg"); //the oid of the user's owner org.
	String userOid          = doc.getAttribute("/userOid"); //the user's oid
	String instrumentType   = doc.getAttribute("/instrumentType"); //type of the template instrument

	//rbhaduri - 9th August 06 - IR CGUG071768901 - Get ownership level of the template itself
	String ownerLevel       = doc.getAttribute("/ownerLevel"); //the level of the user's owner org.


	long transactionOid     = -1; //will store the oid of the new transaction

	//Validate the instument Type
	String physicalInstType = InstrumentServices.getPhysicalInstrumentType(instrumentType);
	LOG.debug("The instrument type being created is a: {}  which is actually a: {}" , instrumentType,physicalInstType);

	// Validate the instument type (using the physical instrument type --
	// thus Detailed SLC is an SLC (but logically a GUA)).  We validate
	// creation rights based on SLC (rather than GUA)
	LOG.debug("Attempting to validate an instrument type of: {}" , physicalInstType);
	if (instrumentType == null
		|| !InstrumentServices.canCreateInstrumentType(physicalInstType)
		|| physicalInstType.equals(InstrumentType.EXPORT_DLC))
	{
		//ERROR
		debug("NTM_INVALID_INSTR_TYPE error");
		String instrumentTypeDescr = ReferenceDataManager.getRefDataMgr().getDescr("INSTRUMENT_TYPE",
		                                                                        instrumentType, csdb.getLocaleName());

		getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
		                                              TradePortalConstants.NTM_INVALID_TEMPLATE_TYPE,
		                                              instrumentTypeDescr);
		return null;
	}

	//Create the new instrument
	this.setAttribute("instrument_type_code", physicalInstType);

	//Create the new transaction
	LOG.debug("Creating Transaction component");
	transactionOid = this.newComponent("TransactionList");
	Transaction newTrans = (Transaction) this.getComponentHandle("TransactionList", transactionOid);

	//Set the transaction type
	String transType        = null;
	transType = InstrumentServices.getOriginalTransType(instrumentType);

	//rbhaduri - 9th August 06 - IR CGUG071768901 - Set ownerOrg to something other than oid
	if (!ownerLevel.equals(TradePortalConstants.OWNER_CORPORATE))
		ownerOrg = TradePortalConstants.OWNER_TYPE_ADMIN;

	newTrans.createNewBlank(transType, userOid, ownerOrg, instrumentType, true, "");
	this.setAttribute("original_transaction_oid", newTrans.getAttribute("transaction_oid"));

	return newTrans.getAttribute("transaction_oid");
  }


   /**
	* This method is used to initialize several attributes for a new template
	* instrument. Specifically, it sets the template_flag to 'N'.
	*/
   protected void userNewObject() throws RemoteException, AmsException
   {
	  super.userNewObject();

	  this.setAttribute("template_flag", TradePortalConstants.INDICATOR_YES);
   }


}