package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import com.ams.tradeportal.common.*;

/*
 *
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class IncomingInterfaceQueueBean extends IncomingInterfaceQueueBean_Base
{
private static final Logger LOG = LoggerFactory.getLogger(IncomingInterfaceQueueBean.class);
   //provide a upper limit for duration seconds to avoid failing the entire record if calculations are out of bounds	
   private static long MAX_DURATION_SECONDS = 9999999999L;
   
   /**
    *   This method copies current row from incoming_queue table into incoming_q_history,
    *   then deletes original from incoming_queue table.
    *
    *   @return int
    */
   public void moveToHistory() throws RemoteException, AmsException{

            IncomingQueueHistory incoming_q_history = (IncomingQueueHistory)createServerEJB("IncomingQueueHistory");

            incoming_q_history.newObject();
            incoming_q_history.setAttribute("date_received",            getAttribute("date_received"));
            incoming_q_history.setAttribute("date_sent",                getAttribute("date_sent"));
            incoming_q_history.setAttribute("msg_text",                 getAttribute("msg_text"));
            incoming_q_history.setAttribute("msg_type",                 getAttribute("msg_type"));
            incoming_q_history.setAttribute("status",                   getAttribute("status"));
            incoming_q_history.setAttribute("client_bank",              getAttribute("client_bank"));
            incoming_q_history.setAttribute("message_id",               getAttribute("message_id"));
            incoming_q_history.setAttribute("reply_to_message_id",      getAttribute("reply_to_message_id"));
            incoming_q_history.setAttribute("agent_id",                 getAttribute("agent_id"));
            incoming_q_history.setAttribute("confirmation_error_text",  getAttribute("confirmation_error_text"));
            incoming_q_history.setAttribute("unpackaging_error_text",   getAttribute("unpackaging_error_text"));
            incoming_q_history.setAttribute("mail_message_oid",         getAttribute("mail_message_oid"));
            incoming_q_history.setAttribute("instrument_oid",           getAttribute("instrument_oid"));
            incoming_q_history.setAttribute("transaction_oid",          getAttribute("transaction_oid"));
            incoming_q_history.setAttribute("complete_instrument_id",   getAttribute("complete_instrument_id"));
            incoming_q_history.setAttribute("transaction_type_code",    getAttribute("transaction_type_code"));
            
            //include values for processing slas
            String msgBroker = getAttribute("datetime_msgbroker");
            String unpackStart  = getAttribute("datetime_unpack_start");
            String unpackEnd    = getAttribute("datetime_unpack_end");
            incoming_q_history.setAttribute("datetime_msgbroker",    msgBroker);
            incoming_q_history.setAttribute("datetime_unpack_start", unpackStart);
            incoming_q_history.setAttribute("datetime_unpack_end",   unpackEnd);
            try {
                long unpackSeconds = TPDateTimeUtility.calculateDurationSeconds(unpackStart,unpackEnd);
                if ( unpackSeconds >= 0 ) {
                	if ( unpackSeconds <= MAX_DURATION_SECONDS ) {
	                	String secondsStr = "" + unpackSeconds;
	                	incoming_q_history.setAttribute("unpack_seconds", secondsStr);
                	} else {
                		//avoid failure if duration is out of bounds
    	        		LOG.debug("IncomingInterfaceQueueBean.moveToHistory::unpack duration out of bounds. Ignoring.");
                	}
	        	} else {
	            	//avoid failure if duration is negative.
	        		LOG.debug("IncomingInterfaceQueueBean.moveToHistory::negative unpack duration. Ignoring.");
	        	}
            } catch (Throwable t) { //i.e. illegal argument exception
        		//just write out to the log (i.e. debug)
        		LOG.error("IncomingInterfaceQueueBean.moveToHistory::unable to calculate unpack seconds duration. Ignoring. Exception: " , t);
            }
            try {
            	long processSeconds = TPDateTimeUtility.calculateDurationSeconds(msgBroker,unpackEnd);
            	if ( processSeconds>=0 ) {
            		if ( processSeconds <= MAX_DURATION_SECONDS ) {
	                	String secondsStr = "" + processSeconds;
	            	    incoming_q_history.setAttribute("process_seconds", secondsStr);
                	} else {
                		//avoid failure if duration is out of bounds
    	        		LOG.debug("IncomingInterfaceQueueBean.moveToHistory::processing duration out of bounds. Ignoring.");
                	}
            	} else {
                	//avoid failure if duration is negative
            		LOG.debug("IncomingInterfaceQueueBean.moveToHistory::negative processing duration. Ignoring.");
            	}
            } catch (Throwable t) { //i.e. illegal argument exception
				//just write out to the log (i.e. debug)
				LOG.error("IncomingInterfaceQueueBean.moveToHistory::unable to calculate process seconds duration." +
						"Ignoring. Exception:" , t);
			}
            
            if ( incoming_q_history.save()<0 ) {
            	LOG.debug("IncomingInterfaceQueueBean.moveToHistory::Problem saving incoming queue history");
            }

            this.delete();

    }// end of moveToHistory()

}
