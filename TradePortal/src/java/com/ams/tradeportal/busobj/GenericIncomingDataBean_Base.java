
  
/*
 * This file is generated from the model.  Normally it should not be modified manually.  
 * Contact the modeler first.  (In a pinch, it can be modified first and then retrofitted 
 * to the model.)
 *
 */
package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;


/*
 * This Business object stores data from Generic Incoming Data Message sent
 * by bank. Bank uses Generic Incoming Data Message to send over data in no
 * specific format.  
 * 
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class GenericIncomingDataBean_Base extends BusinessObjectBean
{
private static final Logger LOG = LoggerFactory.getLogger(GenericIncomingDataBean_Base.class);

  
	/* 
	   * Register the attributes and associations of the business object
	   */
	   protected void registerAttributes() throws AmsException
	   {  
	      //BSL IR#PUL032965444 04/04/11
	      /* domestic_payment_oid - Unique Identifier. */
			  attributeMgr.registerAttribute("gen_inc_data_oid", "gen_inc_data_oid", "ObjectIDAttribute");
			
			  /* invoice_details -  */
			attributeMgr.registerAttribute("corporate_org_oid", "corporate_org_oid");
//		    attributeMgr.registerAttribute("date_sent", "date_sent", "DateTimeAttribute");
//			attributeMgr.registerAttribute("time_sent", "time_sent", "DateTimeAttribute");
			attributeMgr.registerAttribute("date_time_sent", "date_time_sent", "DateTimeAttribute");
			  
			attributeMgr.registerAttribute("message_category", "message_category");
			attributeMgr.registerAttribute("acct_number", "acct_number");
			attributeMgr.registerAttribute("currency", "currency");
			attributeMgr.registerAttribute("source_system_branch", "source_system_branch");
			attributeMgr.registerAttribute("customer_alias", "customer_alias");
			  
			attributeMgr.registerAttribute("user_defined_1", "user_defined_1");
	  		attributeMgr.registerAttribute("user_defined_2", "user_defined_2");
	  		attributeMgr.registerAttribute("user_defined_3", "user_defined_3");
	  		attributeMgr.registerAttribute("user_defined_4", "user_defined_4");
	  		attributeMgr.registerAttribute("user_defined_5", "user_defined_5");
	  		attributeMgr.registerAttribute("user_defined_6", "user_defined_6");
	  		attributeMgr.registerAttribute("user_defined_7", "user_defined_7");
	  		attributeMgr.registerAttribute("user_defined_8", "user_defined_8");
	  		attributeMgr.registerAttribute("user_defined_9", "user_defined_9");
	  		attributeMgr.registerAttribute("user_defined_10", "user_defined_10");
	  		attributeMgr.registerAttribute("user_defined_11", "user_defined_11");
	  		attributeMgr.registerAttribute("user_defined_12", "user_defined_12");
	  		attributeMgr.registerAttribute("user_defined_13", "user_defined_13");
	  		attributeMgr.registerAttribute("user_defined_14", "user_defined_14");
	  		attributeMgr.registerAttribute("user_defined_15", "user_defined_15");
	  		attributeMgr.registerAttribute("user_defined_16", "user_defined_16");
	  		attributeMgr.registerAttribute("user_defined_17", "user_defined_17");
	  		attributeMgr.registerAttribute("user_defined_18", "user_defined_18");
	  		attributeMgr.registerAttribute("user_defined_19", "user_defined_19");
	  		attributeMgr.registerAttribute("user_defined_20", "user_defined_20");
	  		attributeMgr.registerAttribute("user_defined_21", "user_defined_21");
	  		attributeMgr.registerAttribute("user_defined_22", "user_defined_22");
	  		attributeMgr.registerAttribute("user_defined_23", "user_defined_23");
	  		attributeMgr.registerAttribute("user_defined_24", "user_defined_24");
	  		attributeMgr.registerAttribute("user_defined_25", "user_defined_25");
	  		attributeMgr.registerAttribute("user_defined_26", "user_defined_26");
	  		attributeMgr.registerAttribute("user_defined_27", "user_defined_27");
	  		
	  		attributeMgr.registerAttribute("user_defined_curr1", "user_defined_curr1");
	  		attributeMgr.registerAttribute("user_defined_amount1", "user_defined_amount1");
	  		attributeMgr.registerAttribute("user_defined_curr2", "user_defined_curr2");
	  		attributeMgr.registerAttribute("user_defined_amount2", "user_defined_amount2");
	  		attributeMgr.registerAttribute("user_defined_date", "user_defined_date", "DateTimeAttribute");
	  		attributeMgr.registerAttribute("receipt_date_time", "receipt_date_time", "DateTimeAttribute");
	          
	   }
   
  
   
}
