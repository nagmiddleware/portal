
  

package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;
/**
 * 
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface PayRemitInvoiceHome extends EJBHome
{
   public PayRemitInvoice create()
      throws RemoteException, CreateException, AmsException;

   public PayRemitInvoice create(ClientServerDataBridge csdb)
      throws RemoteException, CreateException, AmsException;
}
