
package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import com.ams.tradeportal.busobj.util.*;



/*
 *
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class PanelAuthorizationRangeBean extends PanelAuthorizationRangeBean_Base
{
private static final Logger LOG = LoggerFactory.getLogger(PanelAuthorizationRangeBean.class);


	protected void userValidate() throws AmsException, RemoteException {
		
		this.validatePanelAmountFields();
		this.validatePanelAuthRules();

	}

	
 private void validatePanelAmountFields() throws RemoteException, AmsException{
			if(StringFunction.isBlank(this.getAttribute("panel_auth_range_min_amt")) && !StringFunction.isBlank(this.getAttribute("panel_auth_range_max_amt")))
				//minAmount = this.getAttribute("panel_auth_range_min_amt");
				 this.getErrorManager().issueError(
						  TradePortalConstants.ERR_CAT_1,
						  TradePortalConstants.PANEL_AUTH_COMBINATION);
			
			
 
 }
 /*This method is to validate 1.If sequence is filled then at least one approver should filled and vice versa. 
  * 2. If Range is there then at least one rule should be available*/
 private void validatePanelAuthRules() throws RemoteException, AmsException{

	 ComponentList panelAuthRuleList = (ComponentList)this.getComponentHandle("PanelAuthorizationRuleList");
	 int numOfPanelAuthRuleBeanList = panelAuthRuleList.getObjectCount();
	 String minAmount = null;
		minAmount = this.getAttribute("panel_auth_range_min_amt");
		
	if(!StringFunction.isBlank(minAmount) && numOfPanelAuthRuleBeanList==0){
		this.getErrorManager().issueError(
				  TradePortalConstants.ERR_CAT_1,
				  TradePortalConstants.PANEL_RANGE_RULES_CHECK);
	}
	 
	 for (int i = 0; i < numOfPanelAuthRuleBeanList; i++) {
		 panelAuthRuleList.scrollToObjectByIndex(i);
			
		 	int aprroverscount=0;
			String approver_sequence=panelAuthRuleList.getBusinessObject().getAttribute("approver_sequence");
			for (int j= 1; j < 21; j++) {
				if(! StringFunction.isBlank(panelAuthRuleList.getBusinessObject().getAttribute("approver_"+j)))
					aprroverscount++;
			}
			if(StringFunction.isBlank(approver_sequence) && aprroverscount>0) {
				
				this.getErrorManager().issueError(
						  TradePortalConstants.ERR_CAT_1,
						  TradePortalConstants.PANEL_RULE_SEQUENCE_APPROVER_CHECK);
			}else if(!StringFunction.isBlank(approver_sequence) && aprroverscount==0){
				
				this.getErrorManager().issueError(
						  TradePortalConstants.ERR_CAT_1,
						  TradePortalConstants.PANEL_RULE_SEQUENCE_APPROVER_CHECK);
			}

			
		}
		
	}

	
}
