

  

package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;


/**
 * The Error Logs of uploading Purchase Order File for each purchase order.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class PurchaseOrderUploadErrorLogBean extends PurchaseOrderUploadErrorLogBean_Base
{
private static final Logger LOG = LoggerFactory.getLogger(PurchaseOrderUploadErrorLogBean.class);   
}
