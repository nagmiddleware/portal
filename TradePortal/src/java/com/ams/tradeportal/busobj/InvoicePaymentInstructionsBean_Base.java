package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;


public class InvoicePaymentInstructionsBean_Base extends TradePortalBusinessObjectBean {
private static final Logger LOG = LoggerFactory.getLogger(InvoicePaymentInstructionsBean_Base.class);

	 
	/*
	 * Describes the mapping of an uploaded file to Invoice definiton.   
	 * The fields contained in the file are described, the file
	 * format is specified.
	 * 
	 * Can also be used to describe order of Invoice Summary and line Item detail data
	 *     Copyright  � 2003                         
	 *     American Management Systems, Incorporated 
	 *     All rights reserved
	 */
	  
	  /* 
	   * Register the attributes and associations of the business object
	   */
	   protected void registerAttributes() throws AmsException
	   {  

	      /* Register attributes defined in the Ancestor class */
	      super.registerAttributes();
	      /* inv_upload_definition_oid - Unique identifier */
	      attributeMgr.registerAttribute("inv_pay_inst_oid", "inv_pay_inst_oid", "ObjectIDAttribute");
	      
	      /* inv_upload_definition_oid - Unique identifier */
	      attributeMgr.registerAttribute("invoice_summary_data_oid", "p_invoice_summary_data_oid");
	      /* Invoice ID - Unique Id */
	      attributeMgr.registerReferenceAttribute("pay_method", "pay_method","PAYMENT_METHOD");
	      attributeMgr.registerAttribute("pay_method", "pay_method");
          attributeMgr.registerAttribute("ben_name", "ben_name");
	      attributeMgr.registerAttribute("ben_acct_num", "ben_acct_num");
	      attributeMgr.registerAttribute("ben_address_one", "ben_address_one");
	      attributeMgr.registerAttribute("ben_address_two", "ben_address_two");
	      attributeMgr.registerAttribute("ben_address_three", "ben_address_three");
	      attributeMgr.registerAttribute("ben_city", "ben_city");
	      attributeMgr.registerReferenceAttribute ("ben_country", "ben_country","COUNTRY");
	      attributeMgr.registerAttribute("ben_bank_name", "ben_bank_name");
	      attributeMgr.registerAttribute("ben_branch_code", "ben_branch_code");
	      attributeMgr.registerAttribute("ben_branch_address1", "ben_branch_address1");
	      attributeMgr.registerAttribute("ben_branch_address2", "ben_branch_address2");
	      attributeMgr.registerAttribute("ben_bank_city", "ben_bank_city");
	      attributeMgr.registerAttribute("ben_bank_prvnce", "ben_bank_prvnce");
	      attributeMgr.registerAttribute("ben_branch_country", "ben_branch_country");
	      attributeMgr.registerReferenceAttribute ("payment_charges", "payment_charges", "BANK_CHARGES_TYPE");
	      attributeMgr.registerAttribute("central_bank_rep1", "central_bank_rep1");
	      attributeMgr.registerAttribute("central_bank_rep2", "central_bank_rep2");
	      attributeMgr.registerAttribute("central_bank_rep3", "central_bank_rep3");
	      attributeMgr.registerAttribute("customer_reference", "customer_reference");

	      /* sequence_number - Sequence Number of this InvoicePaymentInstructions within the transaction. */
	      attributeMgr.registerAttribute("sequence_number", "sequence_number", "NumberAttribute");

             /* Pointer to the parent Transaction */
           attributeMgr.registerAttribute("transaction_oid", "p_transaction_oid", "ParentIDAttribute");
           /* amount - The amount of the domestic payment. This is validated to have the correct
           number of decimal places based on the currency selected.   For amendments,
           this attribute is allowed to have a negative value.  For all other transactions,
           this attribute must be validated that it is a positive number. */
        attributeMgr.registerAttribute("amount", "amount", "TradePortalSignedDecimalAttribute", "com.ams.tradeportal.common");
        
        /* amount_currency_code - The currency for the instrument. */
        attributeMgr.registerReferenceAttribute("amount_currency_code", "amount_currency_code", "CURRENCY_CODE");
        attributeMgr.registerAlias("amount_currency_code", getResourceManager().getText("DomesticPaymentBeanAlias.amount_currency_code", TradePortalConstants.TEXT_BUNDLE));
        attributeMgr.registerAttribute("ben_bank_sort_code", "ben_bank_sort_code");//CR 709A
        /*Rpasupulati Cr1001 Start*/
        attributeMgr.registerAttribute("reporting_code_1", "reporting_code_1");
        attributeMgr.registerAttribute("reporting_code_2", "reporting_code_2");
        /*Rpasupulati Cr1001 End*/
	   }


	}
