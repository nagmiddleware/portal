package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;
/**
 * All Trade Portal business objects inherit from this business object directly
 * or indirectly.   Any logic or attributes common to all Trade Portal business
 * objects should be placed here.
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface TradePortalBusinessObjectHome extends EJBHome
{
   public TradePortalBusinessObject create()
      throws RemoteException, CreateException, AmsException;

   public TradePortalBusinessObject create(ClientServerDataBridge csdb)
      throws RemoteException, CreateException, AmsException;
}
