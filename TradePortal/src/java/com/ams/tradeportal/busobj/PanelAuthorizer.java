
package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;


/**
 * A PanelAuthorizer consists of a set of Authorize users
 * of transaction.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface PanelAuthorizer extends BusinessObject
{   
}
