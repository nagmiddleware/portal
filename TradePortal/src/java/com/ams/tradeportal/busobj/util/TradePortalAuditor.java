package com.ams.tradeportal.busobj.util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class to handle creation of audit_log records.
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */

import java.rmi.*;
import com.ams.tradeportal.busobj.*;
import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;

public class TradePortalAuditor {
private static final Logger LOG = LoggerFactory.getLogger(TradePortalAuditor.class);
/**
 * TradePortalAuditor constructor comment.
 */
public TradePortalAuditor() {
	super();
}


/**
 * Creates an audit log record given a set of information.  This
 * method is called from a mediator.
 *
 * @param medServices MediatorServices - the mediatorServices object from
 *                                       the mediator
 * @param busObj BusinessObject - the object being audited.
 * @param objOid String - oid of the business object
 * @param description String - description column of the business object
 * @param ownerOrgOid String - owner org oid of the business object
 * @param changeType String - change type (must match CHANGE_TYPE in refdata)
 * @param userOid String - oid of the user making the change
 */
public static void createRefDataAudit(MediatorServices medServices,
									  BusinessObject busObj,
								      String objOid,
								      String description,
								      String ownerOrgOid,
								      String changeType,
								      String userOid) {
	
	createRefDataAudit(medServices, busObj, objOid, description, ownerOrgOid, changeType, userOid, null, false);
	
}


/**
 * Creates an audit log record given a set of information.  This
 * method is called from a mediator.
 *
 * @param medServices MediatorServices - the mediatorServices object from
 *                                       the mediator
 * @param busObj BusinessObject - the object being audited.
 * @param objOid String - oid of the business object
 * @param description String - description column of the business object
 * @param ownerOrgOid String - owner org oid of the business object
 * @param changeType String - change type (must match CHANGE_TYPE in refdata)
 * @param userOid String - oid of the user making the change
 * @param approverUserOid String - oid of the user aproving the change
 * @param myOrgProfileInd boolean - true if the request comes from MyOrganizationProfile page
 */
public static void createRefDataAudit(MediatorServices medServices,
									  BusinessObject busObj,
								      String objOid,
								      String description,
								      String ownerOrgOid,
								      String changeType,
								      String userOid,
								      String approverUserOid,
								      boolean myOrgProfileInd) {
	try {
		AuditLog audit = (AuditLog) medServices.createServerEJB("AuditLog");
		audit.newObject();

		// Using the business object, get its class name.  This will be of
		// the form 'com.ams.tradeportal.busobj.User_pwli8g_EOImpl'.  We only want
		// the 'User' part so strip off the beginning and the end.
		String name = busObj.getClass().getName();

		int lastDot = name.lastIndexOf(".");
		int beanNameEnd;

                if(name.indexOf("Bean") >= 0)
                     beanNameEnd = name.indexOf("Bean_");
                else
                     beanNameEnd = name.indexOf("_"); // LOGANATHAN - 08/08/2005 - IR NNUF080856644

		name = name.substring(lastDot + 1, beanNameEnd);
		if(myOrgProfileInd){
			name = name.concat("-MyOrganization");		
		}

		audit.setAttribute("class_name", name);
		audit.setAttribute("changed_object_oid", objOid);
		audit.setAttribute("object_description", description);
		audit.setAttribute("owner_org_oid", ownerOrgOid);
		audit.setAttribute("change_type", changeType);
		audit.setAttribute("date_time_stamp", DateTimeUtility.getGMTDateTime());
		audit.setAttribute("change_user_oid", userOid);
		audit.setAttribute("approver_user_oid", approverUserOid);
		audit.save();
	} catch (RemoteException | AmsException e) {
		LOG.error("Error creating AuditLog record: " ,e);
	}
}
}
