package com.ams.tradeportal.busobj.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.RemoteException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.PropertyResourceBundle;
import java.util.Vector;

import javax.ejb.RemoveException;

import com.ams.tradeportal.busobj.POLineItem;
import com.ams.tradeportal.busobj.POUploadDefinition;
import com.ams.tradeportal.common.TPCurrencyUtility;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.BusinessObject;
import com.amsinc.ecsg.frame.ComponentList;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.frame.EJBObjectFactory;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.JPylonProperties;
import com.amsinc.ecsg.util.StringFunction;

/**
 * Utility to build the goods description for a transaction based on either a set of po line item oids or the given transaction oid.
 * 
 * Copyright � 2001 American Management Systems, Incorporated All rights reserved
 */
public class GoodsDescriptionUtility {
	private static final Logger LOG = LoggerFactory.getLogger(GoodsDescriptionUtility.class);

	// These sql statements are used to select the data for po line items
	private static final String PO_LINE_ITEM_SELECT = "select po_num, item_num, amount, ben_name, currency, last_ship_dt, po_text, "
			+ "other1, other2, other3, other4, other5, other6, other7, other8, "
			+ "other9, other10, other11, other12, other13, other14, other15, "
			+ "other16, other17, other18, other19, other20, other21, other22, "
			+ "other23, other24, a_source_upload_definition_oid as source_upload_definition_oid from po_line_item ";
	private static final String PO_LINE_ITEM_WHERE_SHIPMENT = " where a_shipment_oid = ";
	private static final String PO_LINE_ITEM_ORDERBY = "order by po_num, item_num";

	// We will use substrings of these strings to print a certain number of
	// dashes or spaces. We assume no column is larger than 40 characters.
	// (Otherwise we get an array out of bound exception.)
	private static final String DASHES = "----------------------------------------";
	private static final String SPACES = "                                        ";
	private static final String LINES = "________________________________________";
	private static final int MAX_CHARS = 65;
	private static final String TYPE_NUMBER = "NUMBER";
	public static String[] invField = { "invoice_id", "issue_date", "payment_date", "currency", "amount", "seller_name" };
	public static int[] invFieldSize = { 35, 12, 12, 3, 24, 35 };

	private static PropertyResourceBundle bundle = (PropertyResourceBundle) PropertyResourceBundle.getBundle(TradePortalConstants.TEXT_BUNDLE);

	// private SessionContext context;

	private static class POFieldDefn {
		String internalName;
		String externalName;
		String datatype;
		int size;
	}

	// Common interface to retrieve PO Line Item data from either DocumentHandler (as result of DatabaseQueryBean)
	// or BusinessObject
	private static interface POLineItemData {
		public String getAttribute(String attributeName) throws AmsException, RemoteException;

		public long getAttributeLong(String attributeName) throws AmsException, RemoteException;
	}

	// Implement the PO Line Item data interface by retrieving from DocumentHandler
	private static class POLineItemDataDoc implements POLineItemData {
		private DocumentHandler doc;

		public POLineItemDataDoc(DocumentHandler doc) {
			this.doc = doc;
		}

		public String getAttribute(String attributeName) {
			return doc.getAttribute("/" + attributeName.toUpperCase());
		}

		public long getAttributeLong(String attributeName) throws AmsException {
			return doc.getAttributeLong("/" + attributeName.toUpperCase());
		}
	}

	// Implement the PO Line Item data interface by retrieving from BusinessObject
	private static class POLineItemDataBusobj implements POLineItemData {
		private BusinessObject busobj;

		public POLineItemDataBusobj(BusinessObject busobj) {
			this.busobj = busobj;
		}

		public String getAttribute(String attributeName) throws AmsException, RemoteException {
			return busobj.getAttribute(attributeName);
		}

		public long getAttributeLong(String attributeName) throws AmsException, RemoteException {
			return busobj.getAttributeLong(attributeName);
		}
	}

	// Common interface of list of PO Line Item data, either a vector of DocumentHandler or
	// ComponentList.
	private static interface POLineItemDataList {
		public POLineItemData elementAt(int index) throws AmsException, RemoteException;

		public int size() throws AmsException, RemoteException;
	}

	// Implement the list as a single PO Line Item data as a ComponentList
	private static class POLineItemDataSingleObject implements POLineItemDataList {
		private POLineItem poLineItem;

		public POLineItemDataSingleObject(POLineItem poLineItemObject) {
			this.poLineItem = poLineItemObject;
		}

		public POLineItemData elementAt(int index) throws AmsException, RemoteException {
			if (index > 0)
				return null;
			else
				return new POLineItemDataBusobj(poLineItem);
		}

		public int size() throws AmsException, RemoteException {
			return 1;
		}
	}

	// Implement the list of PO Line Item data as a ComponentList
	private static class POLineItemDataBusobjList implements POLineItemDataList {
		private ComponentList componentList;
		private int size;
		private Vector poLineItemListSorted = new Vector();

		public POLineItemDataBusobjList(ComponentList componentList, int size) throws AmsException, RemoteException {
			this.componentList = componentList;
			this.size = size;
			this.poLineItemListSorted = sortPOLineItemComponentList();
		}

		public POLineItemData elementAt(int index) throws AmsException, RemoteException {
			long componentOid = Long.valueOf((String) poLineItemListSorted.elementAt(index)).longValue();
			return new POLineItemDataBusobj(componentList.getComponentObject(componentOid));
		}

		/**
		 * This method sorts a component list of po line items by creation date and po line item oid
		 * 
		 * @exception com.amsinc.ecsg.frame.AmsException
		 *                The exception description.
		 */
		public Vector sortPOLineItemComponentList() throws AmsException, RemoteException {
			boolean poSorted;
			Date formattedDate;
			Date compareDate;
			String creationDate;
			String poLineItemOid;
			int poOid;
			Vector poLineItemsSorted = new Vector();
			Vector poDates = new Vector();
			Vector poOids = new Vector();

			try {

				SimpleDateFormat dateFormatter = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");

				for (int index = 0; index < componentList.getObjectCount(); index++) {
					componentList.scrollToObjectByIndex(index);
					poLineItemOid = componentList.getListAttribute("po_line_item_oid");
					creationDate = componentList.getListAttribute("creation_date_time");
					formattedDate = dateFormatter.parse(creationDate);

					poSorted = false;

					for (int i = 0; i < poDates.size(); i++) {
						compareDate = (Date) poDates.elementAt(i);
						poOid = Integer.parseInt((String) poOids.elementAt(i));

						if (formattedDate.compareTo(compareDate) < 0) {
							poSorted = true;
						} else if (formattedDate.compareTo(compareDate) == 0) {
							if (Integer.parseInt(poLineItemOid) < poOid)
								poSorted = true;
						}

						if (poSorted) {
							poDates.insertElementAt(formattedDate, i);
							poOids.insertElementAt(poLineItemOid, i);
							poLineItemsSorted.insertElementAt(poLineItemOid, i);
							break;
						}
					}

					if (!poSorted) {
						poDates.addElement(formattedDate);
						poOids.addElement(poLineItemOid);
						poLineItemsSorted.addElement(poLineItemOid);
					}
				}

			} catch (RemoteException e) {
				LOG.error("Remote Exception caught in GoodsDescriptionUtility.sortPOLineItemComponentList(): ", e);
			} catch (ParseException e) {
				throw new AmsException("Date parse error caught in GoodsDescriptionUtility.sortPOLineItemComponentList()");
			}
			return poLineItemsSorted;
		}

		public int size() throws AmsException, RemoteException {
			return this.size;
		}
	}

	// Implement the list of PO Line Item Data as a vector of DocumentHandler.
	private static class POLineItemDataDocList implements POLineItemDataList {
		private Vector docList;

		public POLineItemDataDocList(Vector docList) {
			this.docList = docList;
		}

		public POLineItemData elementAt(int index) {
			return (POLineItemData) new POLineItemDataDoc((DocumentHandler) docList.elementAt(index));
		}

		public int size() {
			return docList.size();
		}
	}

	/**
	 * Append a line of data to an existing string buffer. A check is done to ensure no more than MAX_CHARS characters per line. A
	 * carriage return is added the end of the line (not counted in the MAX_CHARS characters)
	 * 
	 * @return java.lang.StringBuilder - the resulting appended StringBuilder
	 * @param addTo
	 *            java.lang.StringBuilder - what to add to
	 * @param newLine
	 *            java.lang.StringBuilder - the line to add
	 */
	public static StringBuilder addLine(StringBuilder addTo, StringBuilder newLine) {
		addTo.append(newLine);
		addTo.append("\n");
		return addTo;
	}

	/**
	 * Builds a data line of the desired po line items fields as specified by the goods description order (given by the goodsFields
	 * vector). Each column is as wide as the data or the column header (whichever is greater). Text and Date values are left
	 * justified; number values are right justified. (No formatting of the data is done)
	 * 
	 * @return java.lang.StringBuilder - the resulting data line
	 * @param poLineDoc
	 *            com.amsinc.ecsg.util.DocumentHandler - a doc handler representation of a po line item record
	 * @param fieldDefns
	 *            java.util.Hashtable - a hashtable of field definitions (an internal class containing information about a po line
	 *            item field (size, type, name)
	 * @param goodsFields
	 *            java.util.Vector - vector from the loadGoodsFields method
	 * @exception java.rmi.RemoteException
	 *                The exception description.
	 * @exception com.amsinc.ecsg.frame.AmsException
	 *                The exception description.
	 */
	private static StringBuilder buildDataLine(POLineItemData poLineDoc, Hashtable fieldDefns, Vector goodsFields) throws RemoteException, AmsException {

		POFieldDefn fieldDefn;
		StringBuilder line = new StringBuilder();
		int length;
		int columnSize = 0;

		for (int i = 0; i < goodsFields.size(); i++) {
			String fieldName = (String) goodsFields.elementAt(i);
			fieldDefn = (POFieldDefn) fieldDefns.get(fieldName);

			// First determine the size of the column;
			length = fieldDefn.externalName.length();
			if (length < fieldDefn.size) {
				// The external field name is shorter than the size of the
				// data. This means the column size is correct.
				columnSize = fieldDefn.size;
			} else {
				// The external field name is longer than the size of the
				// data. This means the column width was expanded to the
				// size of the field name
				columnSize = length;
			}

			String value = poLineDoc.getAttribute(fieldName);

			// We could potentially do formatting of data based on the type.
			// However, at present the rules are to not format the data.

			// Dates are retrieved from the database as yyyy-mm-dd 00:00:00.0.
			// We only want the date portion (the first 10 characters) so strip
			// off the time portion.
			if (fieldDefn.datatype.equals(TradePortalConstants.PO_FIELD_DATA_TYPE_DATE)) {
				if (value.length() > 10) {
					value = value.substring(0, 10);
				}
			}

			length = value.length();
			if (length < columnSize) {
				// The value is less than the column width so we need to pad.
				if (fieldDefn.datatype.equals(TYPE_NUMBER)) {
					// For numbers, pad on the left (to right justify).
					line.append(SPACES.substring(0, (columnSize - length)));
					line.append(value);
				} else {
					// For Text and Date values, pad on the right
					// (to left justify)
					line.append(value);
					line.append(SPACES.substring(0, (columnSize - length)));
				}
			} else {
				// The value width is the same as the column width so no padding
				// is required. (Edits elsewhere prevent the value from being
				// wider than the max width)
				line.append(value);
			}

			// Add a space between the columns.
			line.append(" ");

		} // end for loop

		// Append the po_text (entered by user) if the PO definition is set to include it.
		String includePoText = (String) fieldDefns.get("include_po_text");

		if (TradePortalConstants.INDICATOR_YES.equals(includePoText)) {
			String poText = poLineDoc.getAttribute("po_text");

			if (poText != null) {
				int poTextLength = poText.length();
				int poTextLineBeginIndex = 0;
				boolean previousLineAlreadyIncludesNewLine = false;
				while (poTextLineBeginIndex < poTextLength) {
					int endIndex;
					if ((poTextLineBeginIndex + 65) > poText.length())
						endIndex = poText.length();
					else
						endIndex = poTextLineBeginIndex + 65;

					String poTextLine = poText.substring(poTextLineBeginIndex, endIndex);

					// Add a newline to move this to the next line, but
					// only do it if there isn't already a newline character in the previous line
					if (!previousLineAlreadyIncludesNewLine)
						line.append("\n");

					int hardEnterIndex = poTextLine.indexOf("\n");
					if (hardEnterIndex >= 0) { // accomodate the hard carriage enter by user.
						poTextLine = poText.substring(poTextLineBeginIndex, poTextLineBeginIndex + hardEnterIndex);
						poTextLineBeginIndex += hardEnterIndex + 1;
						previousLineAlreadyIncludesNewLine = true;
					} else {
						poTextLineBeginIndex += 65;
						previousLineAlreadyIncludesNewLine = false;
					}
					line.append(poTextLine);
				}
			}
		}
		return line;
	}

	// <Amit - IR EEUE032659500 -05/09/2005 >
	// Over loaded method added
	/**
	 * Builds a data line of the desired po line items fields as specified by the goods description order (given by the goodsFields
	 * vector). Each column is as wide as the data or the column header (whichever is greater). Text and Date values are left
	 * justified; number values are right justified. (No formatting of the data is done)
	 * 
	 * @return java.lang.StringBuilder - the resulting data line
	 * @param poLineDoc
	 *            com.amsinc.ecsg.util.DocumentHandler - a doc handler representation of a po line item record
	 * @param fieldDefns
	 *            java.util.Hashtable - a hashtable of field definitions (an internal class containing information about a po line
	 *            item field (size, type, name)
	 * @param goodsFields
	 *            java.util.Vector - vector from the loadGoodsFields method
	 * @param userLocale
	 *            java.lang.String - string with the users locale
	 * @param numberOfDecimalPlaces
	 *            int - integer for storing the number of decimal places to be used for amount formatting
	 * @exception java.rmi.RemoteException
	 *                The exception description.
	 * @exception com.amsinc.ecsg.frame.AmsException
	 *                The exception description.
	 */
	private static StringBuilder buildDataLine(POLineItemData poLineDoc, Hashtable fieldDefns, Vector goodsFields, String userLocale, int numberOfDecimalPlaces) throws RemoteException, AmsException {

		POFieldDefn fieldDefn;
		StringBuilder line = new StringBuilder();
		int length;
		int columnSize = 0;

		for (int i = 0; i < goodsFields.size(); i++) {
			String fieldName = (String) goodsFields.elementAt(i);
			fieldDefn = (POFieldDefn) fieldDefns.get(fieldName);

			// First determine the size of the column;
			length = fieldDefn.externalName.length();
			if (length < fieldDefn.size) {
				// The external field name is shorter than the size of the
				// data. This means the column size is correct.
				columnSize = fieldDefn.size;
			} else {
				// The external field name is longer than the size of the
				// data. This means the column width was expanded to the
				// size of the field name
				columnSize = length;
			}

			String value = poLineDoc.getAttribute(fieldName);

			if (fieldName.equals("amount")) {
				value = TPCurrencyUtility.getDecimalAmountWithoutThousandSeparator(value, numberOfDecimalPlaces, userLocale);
			}

			// We could potentially do formatting of data based on the type.
			// However, at present the rules are to not format the data.

			// Dates are retrieved from the database as yyyy-mm-dd 00:00:00.0.
			// We only want the date portion (the first 10 characters) so strip
			// off the time portion.
			if (fieldDefn.datatype.equals(TradePortalConstants.PO_FIELD_DATA_TYPE_DATE)) {
				if (value.length() > 10) {
					value = value.substring(0, 10);
				}
			}

			length = value.length();
			if (length < columnSize) {
				// The value is less than the column width so we need to pad.
				if (fieldDefn.datatype.equals(TYPE_NUMBER)) {
					// For numbers, pad on the left (to right justify).
					line.append(SPACES.substring(0, (columnSize - length)));
					line.append(value);
				} else {
					// For Text and Date values, pad on the right
					// (to left justify)
					line.append(value);
					line.append(SPACES.substring(0, (columnSize - length)));
				}
			} else {
				// The value width is the same as the column width so no padding
				// is required. (Edits elsewhere prevent the value from being
				// wider than the max width)
				line.append(value);
			}

			// Add a space between the columns.
			line.append(" ");

		} // end for loop

		// Append the po_text (entered by user) if the PO definition is set to include it.
		String includePoText = (String) fieldDefns.get("include_po_text");

		if (TradePortalConstants.INDICATOR_YES.equals(includePoText)) {
			String poText = poLineDoc.getAttribute("po_text");

			if (poText != null) {
				int poTextLength = poText.length();
				int poTextLineBeginIndex = 0;
				boolean previousLineAlreadyIncludesNewLine = false;
				while (poTextLineBeginIndex < poTextLength) {
					int endIndex;
					if ((poTextLineBeginIndex + 65) > poText.length())
						endIndex = poText.length();
					else
						endIndex = poTextLineBeginIndex + 65;

					String poTextLine = poText.substring(poTextLineBeginIndex, endIndex);

					// Add a newline to move this to the next line, but
					// only do it if there isn't already a newline character in the previous line
					if (!previousLineAlreadyIncludesNewLine)
						line.append("\n");

					int hardEnterIndex = poTextLine.indexOf("\n");
					if (hardEnterIndex >= 0) { // accomodate the hard carriage enter by user.
						poTextLine = poText.substring(poTextLineBeginIndex, poTextLineBeginIndex + hardEnterIndex);
						poTextLineBeginIndex += hardEnterIndex + 1;
						previousLineAlreadyIncludesNewLine = true;
					} else {
						poTextLineBeginIndex += 65;
						previousLineAlreadyIncludesNewLine = false;
					}
					line.append(poTextLine);
				}
			}
		}
		return line;
	}

	/**
	 * Returns the 4 line footer consisting of a blank line and each of the 3 footer lines from the po upload definition.
	 * 
	 * @return java.lang.StringBuilder - the resulting footer
	 * @param uploadDefn
	 *            POUploadDefinition - the upload definition to use for the footer
	 * @exception RemoteException
	 * @exception AmsException
	 */
	private static StringBuilder buildFooter(POUploadDefinition uploadDefn) throws RemoteException, AmsException {

		StringBuilder footer = new StringBuilder();
		String footerText;

		footer = addLine(footer, new StringBuilder(""));

		for (int i = 1; i <= 3; i++) {
			footerText = uploadDefn.getAttribute("footer_line_" + i);
			footer = addLine(footer, new StringBuilder(footerText));
		}

		return footer;
	}

	/**
	 * The basic processing for building the goods description is as follows:<br>
	 * 1. Retrieve the POLineItemList of the given ShipmentTerms. <br>
	 * 2. Call the createGoodsLines process with the po line items.
	 * 
	 * @return String - the resulting goods description
	 * @param shipmentTerms
	 *            ShipmentTerms - Handle to the ShipmentTerms to build goods description for
	 * @param poLineItemList
	 *            ComponentList - Handle to the POLineItemList
	 * @param numberOfPOLineItems
	 *            int - Number of PO Line Items. Needed to avoid transaction conflict.
	 * @exception java.rmi.RemoteException
	 * @exception AmsException
	 */
	public static String buildGoodsDescription(ComponentList poLineItemList, int numberOfPOLineItems) throws RemoteException, AmsException {
		return createGoodsLines(new POLineItemDataBusobjList(poLineItemList, numberOfPOLineItems));
	}

	// New overloaded method added
	/**
	 * The basic processing for building the goods description is as follows:<br>
	 * 1. Retrieve the POLineItemList of the given ShipmentTerms. <br>
	 * 2. Call the createGoodsLines process with the po line items.
	 * 
	 * @return String - the resulting goods description
	 * @param poLineItemList
	 *            ComponentList - Handle to the POLineItemList
	 * @param numberOfPOLineItems
	 *            int - Number of PO Line Items. Needed to avoid transaction conflict.
	 * @param userLocale
	 *            java.lang.String - string with the users locale
	 * @param numberOfDecimalPlaces
	 *            int - integer for storing the number of decimal places to be used for amount formatting
	 * @exception java.rmi.RemoteException
	 * @exception AmsException
	 */
	public static String buildGoodsDescription(ComponentList poLineItemList, int numberOfPOLineItems, String userLocale, int numberOfDecimalPlaces) throws RemoteException, AmsException {
		return createGoodsLines(new POLineItemDataBusobjList(poLineItemList, numberOfPOLineItems), userLocale, numberOfDecimalPlaces);
	}

	// </Amit - IR EEUE032659500 -05/09/2005 >

	/**
	 * The basic processing for building the goods description is as follows:<br>
	 * 1. Select all the po line items assigned to the given ShipmentTerms.<br>
	 * 2. Call the createGoodsLines process with the po line items.
	 * 
	 * @return String - the resulting goods description
	 * @param shipmentTermsOid
	 *            String - ShipmentTerms to build goods description for
	 * @exception java.rmi.RemoteException
	 * @exception AmsException
	 */
	public static String buildGoodsDescription(String shipmentTermsOid) throws RemoteException, AmsException {
		Vector poLinesList = getPoLines(shipmentTermsOid);

		return createGoodsLines(new POLineItemDataDocList(poLinesList));
	}

	/**
	 * The basic processing for building the goods description is as follows:<br>
	 * 1. Select all the po line items given a set of po line item oids.<br>
	 * 2. Call the createGoodsLines process with the po line items.
	 * 
	 * @return String - the resulting goods description
	 * @param poOids
	 *            Vector - vector of po line item oids to use for building the goods description
	 * @exception java.rmi.RemoteException
	 * @exception AmsException
	 */
	public static String buildGoodsDescription(POLineItem updatedPoLineItem) throws RemoteException, AmsException {
		return createGoodsLines(new POLineItemDataSingleObject(updatedPoLineItem));
	}

	/**
	 * The basic processing for building the goods description is as follows:<br>
	 * 1. Select all the po line items given a set of po line item oids.<br>
	 * 2. Call the createGoodsLines process with the po line items.
	 * 
	 * @return String - the resulting goods description
	 * @param poOids
	 *            Vector - vector of po line item oids to use for building the goods description
	 * @param shipmentTermsOid
	 *            String - ShipmentTerms to build goods description for
	 * @exception java.rmi.RemoteException
	 * @exception AmsException
	 */
	public static String buildGoodsDescription(Vector poOids, String shipmentTermsOid) throws RemoteException, AmsException {
		Vector poLinesList = getPoLines(shipmentTermsOid);
		poLinesList.addAll(getPoLines(poOids));

		return createGoodsLines(new POLineItemDataDocList(poLinesList));
	}

	// <Amit - IR EEUE032659500 -05/09/2005 >
	/**
	 * The basic processing for building the goods description is as follows:<br>
	 * 1. Select all the po line items given a set of po line item oids.<br>
	 * 2. Call the createGoodsLines process with the po line items.
	 * 
	 * @return String - the resulting goods description
	 * @param poOids
	 *            Vector - vector of po line item oids to use for building the goods description
	 * @param shipmentTermsOid
	 *            String - ShipmentTerms to build goods description for
	 * @param userLocale
	 *            java.lang.String - string with the users locale
	 * @param numberOfDecimalPlaces
	 *            int - integer for storing the number of decimal places to be used for amount formatting
	 * @exception java.rmi.RemoteException
	 * @exception AmsException
	 */
	public static String buildGoodsDescription(Vector poOids, String shipmentTermsOid, String userLocale, int numberOfDecimalPlaces) throws RemoteException, AmsException {

		Vector poLinesList = getPoLines(shipmentTermsOid);
		poLinesList.addAll(getPoLines(poOids));

		return createGoodsLines(new POLineItemDataDocList(poLinesList), userLocale, numberOfDecimalPlaces);
	}

	// </Amit - IR EEUE032659500 -05/09/2005 >

	/**
	 * The basic processing for building the goods description is as follows:<br>
	 * 1. Retrieve the POLineItemList of the given ShipmentTerms. <br>
	 * 2. Call the createGoodsLines process with the po line items.
	 * 
	 * @return String - the resulting goods description
	 * @param poOids
	 *            Vector - vector of po line item oids to use for building the goods description
	 * @exception java.rmi.RemoteException
	 * @exception AmsException
	 */
	public static String buildGoodsDescription(Vector poOids) throws RemoteException, AmsException {
		Vector poLinesList = getPoLines(poOids);
		return createGoodsLines(new POLineItemDataDocList(poLinesList));
	}

	// <Amit - IR EEUE032659500 -05/09/2005 >
	/**
	 * The basic processing for building the goods description is as follows:<br>
	 * 1. Retrieve the POLineItemList of the given ShipmentTerms. <br>
	 * 2. Call the createGoodsLines process with the po line items.
	 * 
	 * @return String - the resulting goods description
	 * @param poOids
	 *            Vector - vector of po line item oids to use for building the goods description
	 * @param userLocale
	 *            java.lang.String - string with the users locale
	 * @param numberOfDecimalPlaces
	 *            int - integer for storing the number of decimal places to be used for amount formatting
	 * @exception java.rmi.RemoteException
	 * @exception AmsException
	 */
	public static String buildGoodsDescription(Vector poOids, String userLocale, int numberOfDecimalPlaces) throws RemoteException, AmsException {
		Vector poLinesList = getPoLines(poOids);
		return createGoodsLines(new POLineItemDataDocList(poLinesList), userLocale, numberOfDecimalPlaces);
	}

	// </Amit - IR EEUE032659500 -05/09/2005 >

	/**
	 * Builds the header lines for the goods description. This consists of a line showing the column names (external field names)
	 * and a second line with dashes. Both lines are maximum 65 characters. The result is one string buffer with carriage returns at
	 * the end of the line. It will look something like this: Column1 Column2 Column3 --------- ---------- ---------
	 * 
	 * @return java.lang.StringBuilder - the resulting header
	 * @param fieldDefns
	 *            java.util.Hashtable - set of field definitions
	 * @param goodsFields
	 *            java.util.Vector - vector of ordered goods desc fields
	 * @exception java.rmi.RemoteException
	 * @exception AmsException
	 */
	private static StringBuilder buildHeader(Hashtable fieldDefns, Vector goodsFields) throws RemoteException, AmsException {

		POFieldDefn fieldDefn;
		StringBuilder line1 = new StringBuilder();
		StringBuilder line2 = new StringBuilder();
		int length;

		for (int i = 0; i < goodsFields.size(); i++) {
			String fieldName = (String) goodsFields.elementAt(i);
			fieldDefn = (POFieldDefn) fieldDefns.get(fieldName);
			length = fieldDefn.externalName.length();
			if (length < fieldDefn.size) {
				// The external field name is shorter than the size of the
				// data. This means we need to pad the column header to make
				// it as long as the data (plus 1 for the space between
				// columns
				line1.append(fieldDefn.externalName);
				int pad = fieldDefn.size - length + 1;
				if (pad > 0) {
					line1.append(SPACES.substring(0, pad));
				}

				line2.append(DASHES.substring(0, fieldDefn.size));
				line2.append(" ");
			} else {
				// The external field name is longer than the size of the
				// data. This means the column expands to the size of the
				// field name.
				line1.append(fieldDefn.externalName);
				line1.append(" ");
				line2.append(DASHES.substring(0, length));
				line2.append(" ");
			}
		}
		StringBuilder result = new StringBuilder();
		result = addLine(result, line1);
		result = addLine(result, line2);

		return result;
	}

	/**
	 * The basic processing for building the goods description is as follows:<br>
	 * 1. Given a set of po line items (in DocumentHandler form)<br>
	 * 2. Select the po upload definition. (All po lines are supposed to have the same definition so we use the defn oid from the
	 * first po line.)<br>
	 * 3. From the defn, load the name, size, and datatype of the 6 standard fields and the 24 other fields to a hashtable.<br>
	 * 4. If the include header flag is on, print a header row and a line of -'s<br>
	 * 5. For each po line item record<br>
	 * 5.1 Start a new line<br>
	 * 5.2 Loop through the 30 goods descr fields (from 1 until we hit the first blank field).<br>
	 * 5.2.1 Get the field name for the nth goods descr field<br>
	 * 5.2.2 Use the field name to get the hashtable entry describing that field<br>
	 * 5.2.3 Get the value for that field from the po line item record<br>
	 * 5.2.4 Pad the value to the left or right based on type and size<br>
	 * 5.2.5 Print the result<br>
	 * 6. If the footer flag is on, print a blank line and the 3 footer rows.<br>
	 * 
	 * @return String - the resulting goods description (with header, data, and footer lines)
	 * @param poLinesList
	 *            java.util.Vector - set of po line item docs to use for creating the good description
	 * @exception java.rmi.RemoteException
	 * @exception AmsException
	 */
	private static String createGoodsLines(POLineItemDataList poLinesList) throws java.rmi.RemoteException, com.amsinc.ecsg.frame.AmsException {

		POLineItemData poLineDoc;
		POUploadDefinition uploadDefn = null;
		Hashtable fieldData = null;
		Vector goodsFields = null;
		StringBuilder goodsDesc = new StringBuilder();

		if (poLinesList.size() < 1)
			return null;

		for (int i = 0; i < poLinesList.size(); i++) {
			poLineDoc = poLinesList.elementAt(i);
			if (i == 0) {
				// Get the po upload defn record and then preload data from
				// the object into a hashtable and an array for more efficient
				// processing.
				long defnOid = poLineDoc.getAttributeLong("source_upload_definition_oid");
				uploadDefn = getPoUploadDefn(defnOid);
				fieldData = uploadDefn != null ? loadFieldDefinitions(uploadDefn) : null;
				goodsFields = loadGoodsFields(uploadDefn);

				String flag = uploadDefn != null ? uploadDefn.getAttribute("include_column_header") : null;
				if (TradePortalConstants.INDICATOR_YES.equals(flag)) {
					goodsDesc.append(buildHeader(fieldData, goodsFields));
				}
			}
			goodsDesc = addLine(goodsDesc, buildDataLine(poLineDoc, fieldData, goodsFields));
		}

		if (uploadDefn != null) {
			String flag = uploadDefn.getAttribute("include_footer");
			if (TradePortalConstants.INDICATOR_YES.equals(flag)) {
				goodsDesc.append(buildFooter(uploadDefn));
			}
			try {
				uploadDefn.remove();
			} catch (RemoveException re) {
				LOG.error("Exception removing EJB in createGoodsLines: ", re);
			}
		}
		return goodsDesc.toString();
	}

	// <Amit - IR EEUE032659500 -05/09/2005 >
	// New overloaded method added
	/**
	 * The basic processing for building the goods description is as follows:<br>
	 * 1. Given a set of po line items (in DocumentHandler form)<br>
	 * 2. Select the po upload definition. (All po lines are supposed to have the same definition so we use the defn oid from the
	 * first po line.)<br>
	 * 3. From the defn, load the name, size, and datatype of the 6 standard fields and the 24 other fields to a hashtable.<br>
	 * 4. If the include header flag is on, print a header row and a line of -'s<br>
	 * 5. For each po line item record<br>
	 * 5.1 Start a new line<br>
	 * 5.2 Loop through the 30 goods descr fields (from 1 until we hit the first blank field).<br>
	 * 5.2.1 Get the field name for the nth goods descr field<br>
	 * 5.2.2 Use the field name to get the hashtable entry describing that field<br>
	 * 5.2.3 Get the value for that field from the po line item record<br>
	 * 5.2.4 Pad the value to the left or right based on type and size<br>
	 * 5.2.5 Print the result<br>
	 * 6. If the footer flag is on, print a blank line and the 3 footer rows.<br>
	 * 
	 * @return String - the resulting goods description (with header, data, and footer lines)
	 * @param poLinesList
	 *            java.util.Vector - set of po line item docs to use for creating the good description
	 * @param userLocale
	 *            java.lang.String - string with the users locale
	 * @param numberOfDecimalPlaces
	 *            int - integer for storing the number of decimal places to be used for amount formatting
	 * @exception java.rmi.RemoteException
	 * @exception AmsException
	 */
	private static String createGoodsLines(POLineItemDataList poLinesList, String userLocale, int numberOfDecimalPlaces) throws java.rmi.RemoteException, com.amsinc.ecsg.frame.AmsException {

		POLineItemData poLineDoc;
		POUploadDefinition uploadDefn = null;
		Hashtable fieldData = null;
		Vector goodsFields = null;
		StringBuilder goodsDesc = new StringBuilder();

		if (poLinesList.size() < 1)
			return null;

		for (int i = 0; i < poLinesList.size(); i++) {
			poLineDoc = poLinesList.elementAt(i);
			if (i == 0) {
				// Get the po upload defn record and then preload data from
				// the object into a hashtable and an array for more efficient
				// processing.
				long defnOid = poLineDoc.getAttributeLong("source_upload_definition_oid");
				uploadDefn = getPoUploadDefn(defnOid);
				fieldData = uploadDefn != null ? loadFieldDefinitions(uploadDefn) : null;
				goodsFields = loadGoodsFields(uploadDefn);

				String flag = uploadDefn != null ? uploadDefn.getAttribute("include_column_header") : null;
				if (fieldData != null && TradePortalConstants.INDICATOR_YES.equals(flag)) {
					goodsDesc.append(buildHeader(fieldData, goodsFields));
				}

			}
			// <Amit - IR EEUE032659500 -05/09/2005 >

			if (fieldData != null)
				goodsDesc = addLine(goodsDesc, buildDataLine(poLineDoc, fieldData, goodsFields, userLocale, numberOfDecimalPlaces));

			// </Amit - IR EEUE032659500 -05/09/2005 >
		}

		if (uploadDefn != null) {
			String flag = uploadDefn.getAttribute("include_footer");
			if (TradePortalConstants.INDICATOR_YES.equals(flag)) {
				goodsDesc.append(buildFooter(uploadDefn));
			}
			try {
				uploadDefn.remove();
			} catch (RemoveException re) {
				LOG.error("Exception removing EJB in createGoodsLines(): ", re);
			}
		}
		return goodsDesc.toString();
	}

	// </Amit - IR EEUE032659500 -05/09/2005 >

	/**
	 * Uses the DatabaseQueryBean to execute sql to retrieve data from the po_line_item table which are assigned to the given
	 * ShipmentTerms. the result is a vector of documents, one for each po line item row.
	 * 
	 * @return java.util.Vector - a vector of po line item records (as doc handlers)
	 * @param transactionOid
	 *            java.lang.String - the transaction to use for getting the po line items.
	 */
	private static Vector getPoLines(String shipmentTermsOid) throws AmsException {
		String poLineItemSql = PO_LINE_ITEM_SELECT + PO_LINE_ITEM_WHERE_SHIPMENT + " ? " + PO_LINE_ITEM_ORDERBY;
		DocumentHandler resultSet = DatabaseQueryBean.getXmlResultSet(poLineItemSql, false, new Object[] { shipmentTermsOid });

		if (resultSet == null) {
			return new Vector();
		} else {
			return resultSet.getFragments("/ResultSetRecord");
		}

	}

	/**
	 * Uses the DatabaseQueryBean to execute sql to retrieve data from the po_line_item table which are in a given set of po line
	 * item oids. the result is a vector of documents, one for each po line item row.
	 * 
	 * @return java.util.Vector - set of po line item documents.
	 * @param Vector
	 *            - set of po line item oids
	 */
	private static Vector getPoLines(Vector poOids) throws AmsException {
		StringBuilder poLineItemSql = new StringBuilder(PO_LINE_ITEM_SELECT);

		// The where clause is based on a set of po oids and looks something like:
		// "where po_line_item_oid in (1,2,3)"
		poLineItemSql.append("where ");
		String requiredOid = "po_line_item_oid";// SHR CR708 Rel8.1.1
		poLineItemSql.append(POLineItemUtility.getPOLineItemOidsSql(poOids, requiredOid));
		poLineItemSql.append(PO_LINE_ITEM_ORDERBY);

		DocumentHandler resultSet = DatabaseQueryBean.getXmlResultSet(poLineItemSql.toString(), false, new ArrayList<Object>());
		return resultSet.getFragments("/ResultSetRecord");

	}

	/**
	 * Creates an EJB instance for the given upload definition oid.
	 * 
	 * @return com.ams.tradeportal.busobj.POUploadDefinition
	 * @param definitionOid
	 *            long - oid of the po upload definition to retrieve
	 */
	private static POUploadDefinition getPoUploadDefn(long definitionOid) throws RemoteException, AmsException {
		POUploadDefinition uploadDefn = null;
		try {
			String serverLocation = JPylonProperties.getInstance().getString("serverLocation");
			uploadDefn = (POUploadDefinition) EJBObjectFactory.createClientEJB(serverLocation, "POUploadDefinition", definitionOid);
		} catch (Exception e) {
			LOG.error("Excpeption while getting PoUploadDefn: " ,e);
		}
		return uploadDefn;
	}

	/**
	 * For each of the 6 standard and 24 other fields in the upload defn, create a hashtable containing POFieldDefn objects keyed by
	 * the field's internal name (e.g., ben_name or other5).
	 * 
	 * @return java.util.Hashtable - a set of POFieldDefn classes
	 * @param uploadDefn
	 *            POUploadDefinition - the upload definition to use
	 * @exception java.rmi.RemoteException
	 * @exception AmsException
	 */
	private static Hashtable loadFieldDefinitions(POUploadDefinition uploadDefn) throws RemoteException, AmsException {
		POFieldDefn fieldDefn;
		Hashtable fields = new Hashtable(TradePortalConstants.PO_NUMBER_OF_FIELDS);
		String fieldName;
		String fieldSize;
		fields.put("include_po_text", uploadDefn.getAttribute("include_po_text"));
		fieldDefn = new POFieldDefn();
		fieldDefn.datatype = uploadDefn.getAttribute("amount_datatype");
		fieldDefn.externalName = uploadDefn.getAttribute("amount_field_name");
		fieldSize = uploadDefn.getAttribute("amount_size");
		if ((fieldSize != null) && (fieldSize.length() > 0)) {
			fieldDefn.size = Integer.parseInt(fieldSize);
		} else {
			fieldDefn.size = 0;
		}
		fields.put("amount", fieldDefn);

		fieldDefn = new POFieldDefn();
		fieldDefn.datatype = uploadDefn.getAttribute("ben_name_datatype");
		fieldDefn.externalName = uploadDefn.getAttribute("ben_name_field_name");
		fieldSize = uploadDefn.getAttribute("ben_name_size");
		if ((fieldSize != null) && (fieldSize.length() > 0)) {
			fieldDefn.size = Integer.parseInt(fieldSize);
		} else {
			fieldDefn.size = 0;
		}
		fields.put("ben_name", fieldDefn);

		fieldDefn = new POFieldDefn();
		fieldDefn.datatype = uploadDefn.getAttribute("currency_datatype");
		fieldDefn.externalName = uploadDefn.getAttribute("currency_field_name");
		fieldSize = uploadDefn.getAttribute("currency_size");
		if ((fieldSize != null) && (fieldSize.length() > 0)) {
			fieldDefn.size = Integer.parseInt(fieldSize);
		} else {
			fieldDefn.size = 0;
		}
		fields.put("currency", fieldDefn);

		fieldDefn = new POFieldDefn();
		fieldDefn.datatype = uploadDefn.getAttribute("item_num_datatype");
		fieldSize = uploadDefn.getAttribute("item_num_size");
		if ((fieldSize != null) && (!fieldSize.equals(""))) {
			fieldDefn.size = (new Integer(fieldSize)).intValue();
		} else {
			fieldDefn.size = 0;
		}
		fieldDefn.externalName = uploadDefn.getAttribute("item_num_field_name");
		fields.put("item_num", fieldDefn);

		fieldDefn = new POFieldDefn();
		fieldDefn.datatype = uploadDefn.getAttribute("last_ship_dt_datatype");
		fieldSize = uploadDefn.getAttribute("last_ship_dt_size");
		if ((fieldSize != null) && (!fieldSize.equals(""))) {
			fieldDefn.size = (new Integer(fieldSize)).intValue();
		} else {
			fieldDefn.size = 0;
		}
		fieldDefn.externalName = uploadDefn.getAttribute("last_ship_dt_field_name");
		fields.put("last_ship_dt", fieldDefn);

		fieldDefn = new POFieldDefn();
		fieldDefn.datatype = uploadDefn.getAttribute("po_text_datatype");
		fieldSize = uploadDefn.getAttribute("po_text_size");
		if ((fieldSize != null) && (!fieldSize.equals(""))) {
			fieldDefn.size = (new Integer(fieldSize)).intValue();
		} else {
			fieldDefn.size = 0;
		}
		fieldDefn.externalName = uploadDefn.getAttribute("po_text_field_name");
		fields.put("po_text", fieldDefn);

		fieldDefn = new POFieldDefn();
		fieldDefn.datatype = uploadDefn.getAttribute("po_num_datatype");
		fieldDefn.externalName = uploadDefn.getAttribute("po_num_field_name");
		fieldSize = uploadDefn.getAttribute("po_num_size");
		if ((fieldSize != null) && (fieldSize.length() > 0)) {
			fieldDefn.size = Integer.parseInt(fieldSize);
		} else {
			fieldDefn.size = 0;
		}
		fields.put("po_num", fieldDefn);

		for (int x = 1; x <= 24; x++) {
			fieldName = "other" + x;
			fieldDefn = new POFieldDefn();
			fieldDefn.datatype = uploadDefn.getAttribute(fieldName + "_datatype");
			fieldSize = uploadDefn.getAttribute(fieldName + "_size");
			if ((fieldSize != null) && (!fieldSize.equals(""))) {
				fieldDefn.size = (new Integer(fieldSize)).intValue();
			} else {
				fieldDefn.size = 0;
			}
			fieldDefn.externalName = uploadDefn.getAttribute(fieldName + "_field_name");
			fields.put(fieldName, fieldDefn);
		}

		return fields;
	}

	/**
	 * Build a vector of the goods description fields (in order of display). This is done since these will be accessed multiple
	 * times. It is more efficient to re-read from a vector to to constantly get attributes.
	 * 
	 * @return java.util.Vector - ordered list of fields to use in the goods description.
	 * @param uploadDefn
	 *            POUploadDefinition - upload definition to use
	 * @exception java.rmi.RemoteException
	 * @exception AmsException
	 */
	private static Vector loadGoodsFields(POUploadDefinition uploadDefn) throws RemoteException, AmsException {
		int fieldCount = 1;
		Vector fields = new Vector();

		if (null == uploadDefn)
			return fields;

		String fieldName = uploadDefn.getAttribute("goods_descr_order_1");

		while (StringFunction.isNotBlank(fieldName)) {
			fields.add(fieldName);

			fieldCount++;
			if (fieldCount > 24)
				break;

			fieldName = uploadDefn.getAttribute("goods_descr_order_" + fieldCount);
		}

		return fields;
	}

	// SHR CR708 Rel8.1.1 start
	public static StringBuilder buildInvoiceHeader(Map<String, String> map, String loanType, int numberOfDecimalPlaces) throws RemoteException, AmsException {
		StringBuilder line1 = new StringBuilder();
		StringBuilder line2 = new StringBuilder();
		int maxSize;

		for (int i = 0; i < invField.length; i++) {
			if (!((TradePortalConstants.TRADE_LOAN.equals(loanType)) && (i == 1 || i == 2))) {
				String label = bundle.getString("AddInvoiceDetail." + invField[i]);
				maxSize = StringFunction.isNotBlank(map.get(invField[i])) ? Integer.valueOf(map.get(invField[i])) : 0;
				if ("amount".equals(invField[i])) {
					maxSize = maxSize + numberOfDecimalPlaces + 2;
				}
				if (label.length() < maxSize) {
					// The external field name is shorter than the size of the
					// data. This means we need to pad the column header to make
					// it as long as the data (plus 1 for the space between
					// columns
					line1.append(label);
					line2.append(DASHES.substring(0, label.length()));
					int pad = maxSize - label.length() + 1;
					if (pad > 0) {
						line1.append(SPACES.substring(0, pad));
						line2.append(SPACES.substring(0, pad));
					} else {
						line1.append("  ");
						line2.append("  ");
					}
				} else {
					// The external field name is longer than the size of the
					// data. This means the column expands to the size of the
					// field name.
					map.put(invField[i], String.valueOf(label.length()));
					line1.append(label);
					line1.append("  ");
					line2.append(DASHES.substring(0, label.length()));
					line2.append("  ");
				}
			}
		}
		StringBuilder result = new StringBuilder();
		result = addLine(result, line1);
		result = addLine(result, line2);

		return result;
	}

	public static StringBuilder buildInvoiceDataLine(Vector invList, String termsOid, Vector invOids, int numberOfDecimalPlaces, String userLocale) throws RemoteException, AmsException {
		// DK IR T36000016686 Rel8.2 05/12/2013 starts
		String loanType = "";
		String invOid = null;
		if (invOids != null && invOids.size() > 0) {
			invOid = invOids.get(0).toString();
		}
		String loanTypeSQL = "select loan_type from invoices_summary_data where upload_invoice_oid = ?";
		try {
			DocumentHandler loanTypeResultSet = DatabaseQueryBean.getXmlResultSet(loanTypeSQL, false, new Object[] { invOid });
			if (loanTypeResultSet != null) {
				Vector termsList = loanTypeResultSet.getFragments("/ResultSetRecord");
				if (termsList != null) {
					DocumentHandler doc = (DocumentHandler) termsList.get(0);
					loanType = doc.getAttribute("/LOAN_TYPE");
				}
			}
		} catch (AmsException e) {
			LOG.error("Exception while retreiving loan type: ", e);
		}
		// DK IR T36000016686 Rel8.2 05/12/2013 ends

		String alreadyAssociatedSql = "select max(length(invoice_id)) as MaxInvID, max(length(issue_date)) as MaxISSUEDATE,"
				+ "max(length(payment_date)) as MaxPAYDATE, max(length(currency)) as MaxCcy, max(length(amount)) as MaxAmt, max(length(seller_name)) as MaxSellerName, max(length(buyer_name)) as MaxBuyerName"
				+ " from invoices_summary_data where a_terms_oid = ? ";
		List<Object> sqlprmLst = new ArrayList<Object>();
		sqlprmLst.add(termsOid);
		if (invOids != null && invOids.size() > 0) {

			String invOidsSql = POLineItemUtility.getInvoiceItemOidsSqlWithPlaceHolder(invOids, "UPLOAD_INVOICE_OID", sqlprmLst);
			alreadyAssociatedSql = alreadyAssociatedSql.concat(" or " + invOidsSql);
		}
		DocumentHandler resultSet = DatabaseQueryBean.getXmlResultSet(alreadyAssociatedSql, false, sqlprmLst);

		if (resultSet == null)
			resultSet = new DocumentHandler();

		Vector alreadyAssociatedList = resultSet.getFragments("/ResultSetRecord");
		Map<String, String> maxLength = new HashMap<String, String>();
		maxLength.put("invoice_id", ((DocumentHandler) alreadyAssociatedList.elementAt(0)).getAttribute("/MAXINVID"));
		if (!(loanType.equals(TradePortalConstants.TRADE_LOAN))) {
			maxLength.put("issue_date", ((DocumentHandler) alreadyAssociatedList.elementAt(0)).getAttribute("/MAXISSUEDATE"));
			maxLength.put("payment_date", ((DocumentHandler) alreadyAssociatedList.elementAt(0)).getAttribute("/MAXPAYDATE"));
		}
		maxLength.put("currency", ((DocumentHandler) alreadyAssociatedList.elementAt(0)).getAttribute("/MAXCCY"));
		maxLength.put("amount", ((DocumentHandler) alreadyAssociatedList.elementAt(0)).getAttribute("/MAXAMT"));
		String keyName = "seller_name";
		String partnerName = ((DocumentHandler) alreadyAssociatedList.elementAt(0)).getAttribute("/MAXSELLERNAME");
		invField[5] = "seller_name";
		if (StringFunction.isBlank(partnerName)) {
			keyName = "buyer_name";
			partnerName = ((DocumentHandler) alreadyAssociatedList.elementAt(0)).getAttribute("/MAXBUYERNAME");
			invField[5] = "buyer_name";
		}
		maxLength.put(keyName, partnerName);
		StringBuilder result = new StringBuilder();
		addLine(result, buildInvoiceHeader(maxLength, loanType, numberOfDecimalPlaces)); // DK IR T36000016686 Rel8.2 05/12/2013

		int length;
		for (int i = 0; i < invList.size(); i++) {
			StringBuilder line = new StringBuilder();
			Integer maxSize = 0;
			for (int j = 0; j < invField.length; j++) {
				if (!((TradePortalConstants.TRADE_LOAN.equals(loanType)) && (j == 1 || j == 2))) {
					String name = invField[j];
					String label = bundle.getString("AddInvoiceList." + invField[j]);
					String attr = name.toUpperCase();
					String fieldVal = ((DocumentHandler) invList.elementAt(i)).getAttribute("/" + attr);

					maxSize = StringFunction.isNotBlank(maxLength.get(name)) ? Integer.valueOf(maxLength.get(name)) : 0;

					if ("amount".equals(name)) {
						fieldVal = TPCurrencyUtility.getDecimalAmountWithoutThousandSeparator(fieldVal, numberOfDecimalPlaces, userLocale);
						maxSize = maxSize + numberOfDecimalPlaces + 1;
					}
					maxSize = label.length() > maxSize ? label.length() + 1 : maxSize + 1;
					// First determine the size of the column;
					length = StringFunction.isNotBlank(fieldVal) ? fieldVal.length() : 0;
					if (length < maxSize) {
						// The external field name is shorter than the size of the
						// data. This means the column size is correct.

						if (length != 0)
							line.append(fieldVal);
						line.append(SPACES.substring(0, (maxSize - length)));
					} else {
						// The external field name is longer than the size of the
						// data. This means the column width was expanded to the
						// size of the field name
						line.append(fieldVal);
					}
					// Add a space between the columns.
					line.append(" ");
				}
			}
			addLine(result, line);
		} // end for loop
		return result;
	}
}