package com.ams.tradeportal.busobj.util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.Vector;

import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.ClientServerDataBridge;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.frame.ErrorManager;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.StringFunction;

public class Panel {
private static final Logger LOG = LoggerFactory.getLogger(Panel.class);

	static final String AUTH_RESULT_FAILED="FALSE";
	static final String AUTH_RESULT_PARTIAL="PARTIAL";
	static final String AUTH_RESULT_COMPLETE="TRUE";


	HashMap<String , PanelRange> panelRangeMap = new HashMap<String , PanelRange>();
	Map<Double , PanelRange> panelRangeSortedMap = null;
	String panelOid = null;
	String optLock = null;
	String instrumentType = null;
//	String instrumentTypeDesc = null;
	protected ClientServerDataBridge csdb;
	ErrorManager errorMgr;
	private String ccyBasis = null;
	private boolean errorFound = false;

   public Panel() {
   }
   
   public Panel(String panelOid, String optLock, String instrumentType, ErrorManager errorMgr) throws AmsException {
		PanelRule panelRule = null;
		PanelRange curPanelRange = null;
		String lastRangeOid = null;
		String tempRangeOid = null;
		DocumentHandler panelData = null;
		Vector panelDataList = null; 
		String sequence = null;
		DocumentHandler resultSet = null;
		
		this.optLock= optLock;
		this.errorMgr = errorMgr;
		this.instrumentType = instrumentType;

		setPanelOid(panelOid);

		StringBuffer sql = new StringBuffer(); 

		//LSuresh R91 IR T36000026319 - SQL INJECTION FIX
		//query database to get opt_lock and check whether its been changed. if changed the issue error
		String optLockSql = "select opt_lock, ccy_basis from panel_auth_group where panel_auth_group_oid = ?";
		resultSet = DatabaseQueryBean.getXmlResultSet( optLockSql, false,new Object[]{panelOid});

		if( resultSet != null){
			String optLockFromDB = resultSet.getAttribute("/ResultSetRecord(0)/OPT_LOCK");
			if(StringFunction.isBlank(optLockFromDB) || (StringFunction.isNotBlank(optLockFromDB) && !optLockFromDB.equals(this.optLock))){
			LOG.debug("Panel data has changed since its been last referenced.");
			//Rel 9.0 IR T36000027060 - Appropriate error message for Invoice added
			if(instrumentType.equals(TradePortalConstants.INVOICE)) { 
				errorMgr.issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.PANEL_AUTH_CAN_NOT_PROCESS_INVOICE);
			}else {
				//jgadela IR - Rel 8.3  T36000020977 - [START] - Corrected the error message.	
				errorMgr.issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.PANEL_AUTH_CAN_NOT_PROCESS);
				//jgadela IR - Rel 8.3  T36000020977 - [END].
			}
			errorFound = true;
			return ;	
			}
		  setCcyBasis(resultSet.getAttribute("/ResultSetRecord(0)/CCY_BASIS"));
		}
		//calculate the time to load panel with all ranges and panel rules
		long startLoadingPanelData = System.currentTimeMillis();
		//SureshL R91 IR T36000026319 - SQL INJECTION FIX
		sql.append("select panel_range_1_oid, panel_range_2_oid, panel_range_3_oid, panel_range_4_oid, panel_range_5_oid, panel_range_6_oid ");
		sql.append(" from panel_auth_group where panel_auth_group_oid= ?");
		
		//Get all ranges for given panelOid
		resultSet = DatabaseQueryBean.getXmlResultSet(sql.toString(), false,new Object[]{panelOid});

		if(resultSet == null){
			LOG.debug("No Panel Ranges found for this Panel Group: {}", panelOid);
			errorFound = true;
			return ;
		}

		if( resultSet != null){
			sql = new StringBuffer(); 
			//Get panel data by joining PanelRange and PanelRule tables on panel_range_oid
			sql.append("select r.panel_auth_range_oid, r.panel_auth_range_min_amt, r.panel_auth_range_max_amt, ru.panel_auth_rule_oid, ru.approver_sequence, ru.approver_1,");
			sql.append("ru.approver_2, ru.approver_3, ru.approver_4, ru.approver_5, ru.approver_6, ru.approver_7, ru.approver_8, ru.approver_9,");
			sql.append("ru.approver_10, ru.approver_11, ru.approver_12, ru.approver_13, ru.approver_14, ru.approver_15, ru.approver_16, ru.approver_17,");
			sql.append("ru.approver_18, ru.approver_19,  ru.approver_20 ");
			sql.append(" from panel_auth_range r, panel_auth_rule ru");
			sql.append(" where r.panel_auth_range_oid = ru.p_panel_auth_range_oid ");

			//LSuresh R91 IR T36000026319 - SQL INJECTION FIX	
			List<Object> sqlParams = new ArrayList();
		
			sql.append("and ( r.panel_auth_range_oid = ? ");
			sqlParams.add(resultSet.getAttribute("/ResultSetRecord/PANEL_RANGE_1_OID"));
			if(StringFunction.isNotBlank(resultSet.getAttribute("/ResultSetRecord/PANEL_RANGE_2_OID"))){
				sql.append(" or r.panel_auth_range_oid =  ? ");
				sqlParams.add(resultSet.getAttribute("/ResultSetRecord/PANEL_RANGE_2_OID"));	
			}
			if(StringFunction.isNotBlank(resultSet.getAttribute("/ResultSetRecord/PANEL_RANGE_3_OID"))){
				sql.append(" or r.panel_auth_range_oid =  ? ");
				sqlParams.add( resultSet.getAttribute("/ResultSetRecord/PANEL_RANGE_3_OID"));
			}
			if(StringFunction.isNotBlank(resultSet.getAttribute("/ResultSetRecord/PANEL_RANGE_4_OID"))){
				sql.append(" or r.panel_auth_range_oid = ? ");
				sqlParams.add( resultSet.getAttribute("/ResultSetRecord/PANEL_RANGE_4_OID"));
				}
			if(StringFunction.isNotBlank(resultSet.getAttribute("/ResultSetRecord/PANEL_RANGE_5_OID"))){
				sql.append(" or r.panel_auth_range_oid = ? ");
				sqlParams.add(resultSet.getAttribute("/ResultSetRecord/PANEL_RANGE_5_OID"));	
			}
			if(StringFunction.isNotBlank(resultSet.getAttribute("/ResultSetRecord/PANEL_RANGE_6_OID"))){
				sql.append(" or r.panel_auth_range_oid = ? ");
				sqlParams.add(resultSet.getAttribute("/ResultSetRecord/PANEL_RANGE_6_OID"));
			}
			sql.append(")" );
			sql.append(" order by r.panel_auth_range_oid");
			//MEerupula Rel8.3.0 IR-T36000020334 - The sql query was modified to add  panel rule oid in order by clause.
			sql.append(" , r.panel_auth_range_min_amt, ru.panel_auth_rule_oid ");			
			resultSet =null;

			resultSet = DatabaseQueryBean.getXmlResultSet(sql.toString(), false,sqlParams);
			if(resultSet == null){
				LOG.debug("No panel range and rule information found.");
				errorFound = true;
				return ;
			}

			if( resultSet != null){
				panelDataList  = resultSet.getFragments("/ResultSetRecord");
				LOG.debug("panel data size: {}" ,panelDataList.size());
				for (int i=0; i<  panelDataList .size(); i++){
					panelData = (DocumentHandler) panelDataList .elementAt(i);
					tempRangeOid = panelData.getAttribute("/PANEL_AUTH_RANGE_OID");
					if(StringFunction.isNotBlank(tempRangeOid) && !(tempRangeOid.equalsIgnoreCase(lastRangeOid))){
						curPanelRange = new PanelRange(panelData.getAttribute("/PANEL_AUTH_RANGE_MIN_AMT"), panelData.getAttribute("/PANEL_AUTH_RANGE_MAX_AMT"));
						curPanelRange.setPanelRangeOid(tempRangeOid);
						panelRangeMap.put(tempRangeOid,curPanelRange);
						LOG.debug("panelRangeMap size :  ",  panelRangeMap.size());
						lastRangeOid = tempRangeOid;
					}
					sequence = panelData.getAttribute("/APPROVER_SEQUENCE");
					if (TradePortalConstants.APPROVER_SEQ_FIX_FIRST.equals(sequence)){
						panelRule = new FixFirst(panelData);
					}else if(TradePortalConstants.APPROVER_SEQ_FIX_LAST.equals(sequence)){
						panelRule = new FixLast(panelData);
					}else if(TradePortalConstants.APPROVER_SEQ_FIX_ALL.equals(sequence)){
						panelRule = new FixSequence(panelData);
					}else if(TradePortalConstants.APPROVER_SEQ_NOT_FIXED.equals(sequence)){
						panelRule = new FixNoSequence(panelData);
					}
					if (curPanelRange!=null) curPanelRange.add(panelRule);
				}
			}
		}
		LOG.debug(" Time taken to load panel data is : {} \tmilliseconds", (System.currentTimeMillis()-startLoadingPanelData));
	}
	
	// CR 857
	
	public Panel(String panelOid, String panelRangeOid[], String optLock, String instrumentType, ErrorManager errorMgr) throws AmsException {
		PanelRule panelRule = null;
		PanelRange curPanelRange = null;
		String lastRangeOid = null;
		String tempRangeOid = null;
		DocumentHandler panelData = null;
		Vector panelDataList = null; 
		String sequence = null;
		DocumentHandler resultSet = null;
		HashMap<Double , PanelRange> panelRangeUnsortedMap = new HashMap<Double , PanelRange>();
		
		this.optLock= optLock;
		this.errorMgr = errorMgr;
		this.instrumentType = instrumentType;
		setPanelOid(panelOid);
		StringBuffer sql = new StringBuffer(); 
		
		//query database to get opt_lock and check whether its been changed. if changed the issue error
		String optLockSql = "select opt_lock, ccy_basis from panel_auth_group where panel_auth_group_oid = ?";
		resultSet = DatabaseQueryBean.getXmlResultSet( optLockSql, false,new Object[]{panelOid});

		if( resultSet != null){
			String optLockFromDB = resultSet.getAttribute("/ResultSetRecord(0)/OPT_LOCK");
			if(StringFunction.isBlank(optLockFromDB) || (StringFunction.isNotBlank(optLockFromDB) && !optLockFromDB.equals(this.optLock))){
			LOG.debug("Panel data has changed since its been last referenced.");
			//Rel 9.0 IR T36000027060 - Appropriate error message for Invoice added
			if(instrumentType.equals(TradePortalConstants.INVOICE)) { 
				errorMgr.issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.PANEL_AUTH_CAN_NOT_PROCESS_INVOICE);
			}else {
				//jgadela IR - Rel 8.3  T36000020977 - [START] - Corrected the error message.	
				errorMgr.issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.PANEL_AUTH_CAN_NOT_PROCESS);
				//jgadela IR - Rel 8.3  T36000020977 - [END].
			}
			errorFound = true;
			return ;	
			}
			setCcyBasis(resultSet.getAttribute("/ResultSetRecord(0)/CCY_BASIS"));
		}
		//calculate the time to load panel with all ranges and panel rules
		long startLoadingPanelData = System.currentTimeMillis();
		
			sql = new StringBuffer(); 
			//Get panel data by joining PanelRange and PanelRule tables on panel_range_oid
			sql.append("select r.panel_auth_range_oid, r.panel_auth_range_min_amt, r.panel_auth_range_max_amt, ru.panel_auth_rule_oid, ru.approver_sequence, ru.approver_1,");
			sql.append("ru.approver_2, ru.approver_3, ru.approver_4, ru.approver_5, ru.approver_6, ru.approver_7, ru.approver_8, ru.approver_9,");
			sql.append("ru.approver_10, ru.approver_11, ru.approver_12, ru.approver_13, ru.approver_14, ru.approver_15, ru.approver_16, ru.approver_17,");
			sql.append("ru.approver_18, ru.approver_19,  ru.approver_20 ");
			sql.append(" from panel_auth_range r, panel_auth_rule ru");
			sql.append(" where r.panel_auth_range_oid = ru.p_panel_auth_range_oid ");
			sql.append("and r.panel_auth_range_oid in (" );
			
			//LSuresh R91 IR T36000026319 - SQLINJECTION FIX
			List<Object> sqlParamsList = new ArrayList();
			
			for(int i=0; i < panelRangeOid.length ; i++){
			    if(i != 0){
			    	sql.append(", ");
			    }
				sql.append("?");
				sqlParamsList.add(panelRangeOid[i]);
			}
			sql.append(")");
			sql.append(" order by r.panel_auth_range_oid");
			//MEerupula - 04 Sept 2013 - Rel8.3.0 IR-T36000020334 ADD BEGIN
        	// The sql query was modified to add  panel rule oid in order by clause.
			sql.append(", r.panel_auth_range_min_amt, ru.panel_auth_rule_oid ");
			//MEerupula - 04 Sept 2013 - Rel8.3.0 IR-T36000020334 ADD END 
			resultSet =null;

			resultSet = DatabaseQueryBean.getXmlResultSet(sql.toString(), false, sqlParamsList);
			if(resultSet == null){
				LOG.debug("No panel range and rule information found.");
				errorFound = true;
				return ;
			}

			if( resultSet != null){
				panelDataList  = resultSet.getFragments("/ResultSetRecord");
				LOG.debug("panel data size: {}",panelDataList.size());
				for (int i=0; i<  panelDataList .size(); i++){
					panelData = (DocumentHandler) panelDataList .elementAt(i);
					tempRangeOid = panelData.getAttribute("/PANEL_AUTH_RANGE_OID");
					if(StringFunction.isNotBlank(tempRangeOid) && !(tempRangeOid.equalsIgnoreCase(lastRangeOid))){
						curPanelRange = new PanelRange(panelData.getAttribute("/PANEL_AUTH_RANGE_MIN_AMT"), panelData.getAttribute("/PANEL_AUTH_RANGE_MAX_AMT"));
						curPanelRange.setPanelRangeOid(tempRangeOid);
						panelRangeMap.put(tempRangeOid,curPanelRange);
						//Created another HahMap with Min Amount as the Key to enable sorting based on Min Amount
						panelRangeUnsortedMap.put(Double.parseDouble(panelData.getAttribute("/PANEL_AUTH_RANGE_MIN_AMT")),curPanelRange);
						LOG.debug("panelRangeMap size :  {}",  panelRangeMap.size());
						lastRangeOid = tempRangeOid;
					}
					sequence = panelData.getAttribute("/APPROVER_SEQUENCE");
					if (TradePortalConstants.APPROVER_SEQ_FIX_FIRST.equals(sequence)){
						panelRule = new FixFirst(panelData);
					}else if(TradePortalConstants.APPROVER_SEQ_FIX_LAST.equals(sequence)){
						panelRule = new FixLast(panelData);
					}else if(TradePortalConstants.APPROVER_SEQ_FIX_ALL.equals(sequence)){
						panelRule = new FixSequence(panelData);
					}else if(TradePortalConstants.APPROVER_SEQ_NOT_FIXED.equals(sequence)){
						panelRule = new FixNoSequence(panelData);
					}
					if (curPanelRange!=null) curPanelRange.add(panelRule);
				}
			}
        //To sort the Panel Ranges based on Min Amount. This will be used by the View Pending Panel Authorisations Dialog			
		panelRangeSortedMap =  new TreeMap<Double, PanelRange>(panelRangeUnsortedMap);
		LOG.debug(" Time taken to load panel data is : {} \tmilliseconds", (System.currentTimeMillis()-startLoadingPanelData));
	}


	/**
	* This method returns the String with the panelOid, the ranges belongs to panelOid with min and max amount and
	* all applicable rules with the approver sequence with total authorizers and all awaitingauthorizers 
   */
	public String toString(){
		StringBuilder panel = new StringBuilder("PanelOid: ").append(this.panelOid).append("\n");
		for (Iterator iter = panelRangeMap.keySet().iterator(); iter.hasNext(); ) {
			String rangeOid = (String) iter.next() ;		
			PanelRange range =  panelRangeMap.get(rangeOid);
			panel.append(range.toString()).append("\n");
		}
		return panel.toString();

	}


	/** Added by MEerupula Rel 8.3 CR 821
	* This method checks whether current user with given panel authority can authorize a transaction(rule) or not
	* depending on the history and panel rule. If he/she can authorize then the method returns partial and if he is the last authorizer in the rule 
	* then the method returns complete and if he can't authorize the transaction then method returns  fail.
	*
	* @param authHistory  java.lang.String - the panel history
	* @param amount java.lang.String - the amount-used to determine which range it falls in 
	* @param userPanelLevel java.lang.String -current user panel level
	* @return String - authorization result either fail or complete or partial
	* 
   */
	public String canAuthorize(String authHistory, String amount, String userPanelLevel) throws AmsException {
		String authStatus = AUTH_RESULT_FAILED;
		if(errorFound){
			return authStatus;
		}
		long startAuthorizePanelByAmount = System.currentTimeMillis();
		PanelRange panelRange = getRange(amount);
		
		if(panelRange == null){	
			//Rel 8.3 IR-T36000019257 Added error category and errorText if the amount is not within the Range
			errorMgr.issueError(TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.NO_PANEL_RANGE_FOR_AMOUNT_INST_TYPE, amount,	instrumentType );
			LOG.debug("Panel Auth: No Range found within this amount. ");	
			return authStatus;  
		}
		authStatus = panelRange.match(authHistory, userPanelLevel); 
		LOG.debug(" Time taken for panel authorization  is : {} \tmilliseconds", (System.currentTimeMillis()-startAuthorizePanelByAmount ));
		return  authStatus;

	}
	
	// CR 857
	
	/** Added by MEerupula Rel 8.3 CR 821
	* This method checks whether current user with given panel authority can authorize a transaction(rule) or not
	* depending on the history and panel rule. If he/she can authorize then the method returns partial and if he is the last authorizer in the rule 
	* then the method returns complete and if he can't authorize the transaction then method returns  fail.
	*
	* @param authHistory  java.lang.String - the panel history
	* @param panelAuthRangeOid java.lang.String - the amount-used to determine which range it falls in 
	* @param userPanelLevel java.lang.String -current user panel level
	* @return String - authorization result either fail or complete or partial
	* 
   */
	public String canBeneAuthorize(String authHistory, String panelAuthRangeOid, String userPanelLevel) throws AmsException {
		String authStatus = AUTH_RESULT_FAILED;
		long startAuthorizePanelByAmount = System.currentTimeMillis();
		PanelRange panelRange = getBeneRange(panelAuthRangeOid);
		authStatus = panelRange.match(authHistory, userPanelLevel); 
		LOG.debug(" Time taken for panel authorization  is : {} \tmilliseconds", (System.currentTimeMillis()-startAuthorizePanelByAmount ));
		return  authStatus;

	}
		
	/**
	 * Added by MEerupula Rel 8.3 CR 821
	* This method checks whether current user with given panel authority can authorize a transaction(rule) or not
	* depending on the history and panel rule. If he/she can authorize then the method returns partial and if he is the last authorizer in the rule 
	* then the method returns complete and if he can't authorize the transaction then method returns  fail.
	*
	* @param authHistory  java.lang.String - the panel history
	* @param range java.lang.String - the panel range 
	* @param userPanelLevel java.lang.String -current user panel level
	* @return String - authorization result either fail or complete or partial
	* 
   */

	public String canAuthorizeByRange(String authHistory, String panelRangeOid, String userPanelLevel) throws AmsException {	
		String authStatus = AUTH_RESULT_FAILED;
		if(errorFound){
			return authStatus;
		}
		PanelRange panelRange = null;
		long startAuthorizePanelByRange = System.currentTimeMillis();
	    panelRange =  panelRangeMap.get(panelRangeOid);

			
		if(panelRange!=null)
			authStatus = panelRange.match(authHistory, userPanelLevel); 
		else
			LOG.debug("Panel Auth: No Range found with this RangeOid: {}", panelRangeOid);	
			
		LOG.debug(" Time taken for panel authorization  is : {} \tmilliseconds", (System.currentTimeMillis()-startAuthorizePanelByRange));
		if(AUTH_RESULT_FAILED.equalsIgnoreCase(authStatus)){
			LOG.debug("The Current PanelApprover does not match with the Approver in the  PanelRule. ");
			errorMgr.issueError(TradePortalConstants.ERR_CAT_1,TradePortalConstants.AUTHORISER_NOT_SPECIFIED);
		}
		return authStatus;

	}

	
	public HashMap getNextPanelApproverList(String history, String amount) throws AmsException { //remove duplicate
		HashMap awaitingPanelLevels = new HashMap();
		String awaitingAuthorizers = null;
		PanelRange panelRange =	 null;
		List<PanelRule> panelRuleList = new ArrayList<PanelRule>();

		if (StringFunction.isNotBlank(amount)){
			panelRange = getRange(amount);
			if (panelRange != null) {
			   panelRuleList =  panelRange.panelRuleList;
			}
		}else{
			for (Iterator iter = panelRangeMap.keySet().iterator(); iter.hasNext(); ) {
				String rangeOid = (String) iter.next() ;		
				panelRange =  panelRangeMap.get(rangeOid);
				panelRuleList.addAll(panelRange.panelRuleList);
			}
		}

		for(int i=0; i< panelRuleList.size(); i++){
			//Rel 8.3 IR T36000021533 START
			PanelRule panelObj = panelRuleList.get(i);
			String panelStr = panelObj.toString();
			String[] panelStrArr = panelStr.split(":");
			String seq = panelStrArr[0];
			
			awaitingAuthorizers  =	panelRuleList.get(i).getNextAwaitingPanelLevels(history, seq);
			//Rel 8.3 IR T36000021533 END
			//RPasupulati IR T36000032872
			if(StringFunction.isNotBlank(awaitingAuthorizers)){
			populateAwaitingAuthMap(awaitingAuthorizers,awaitingPanelLevels);
			}
		}

		return awaitingPanelLevels;

		
	}
	
	protected void populateAwaitingAuthMap(String awaitingAuthorizers, HashMap awaitingMap){
		int approversCount = 0;
		int tot = awaitingAuthorizers.length();

		for(int i=0; i<tot; i++){
			String panelLevel =awaitingAuthorizers.substring(i, i+1);
			if (awaitingMap.containsKey(panelLevel)) {
				approversCount  = (Integer) awaitingMap.get(panelLevel);
				awaitingMap.put(panelLevel, ++approversCount);
			}else {
				awaitingMap.put(panelLevel, 1);
			}   
		}

	}




	/**
	* This method returns Panel Range for the given amount
	*
	* @param amount java.lang.String - the amount 
	* @return PanelRange - PanelRange object 
	* 
   */

	public PanelRange getRange(String amount) throws AmsException {
		PanelRange range = null;
		for (Iterator iter = panelRangeMap.keySet().iterator(); iter.hasNext(); ) {
			String rangeOid = (String) iter.next() ;		
			range =  panelRangeMap.get(rangeOid);
			if(range.withinRange(amount)){
				return range;
			}
		}
		return null;
	}
	
	// CR 857
	
	public PanelRange getBeneRange(String panelAuthRangeOid) throws AmsException {
		PanelRange range = null;		
		range =  panelRangeMap.get(panelAuthRangeOid);
		return range;
	}
	
	
	
	public String getPanelOid() {
		return panelOid;
	}



	public void setPanelOid(String panelOid) {
		this.panelOid = panelOid;
	}

	public HashMap<String, PanelRange> getPanelRangeMap() {
		return panelRangeMap;
	}

	public void setPanelRangeMap(HashMap<String, PanelRange> panelRangeMap) {
		this.panelRangeMap = panelRangeMap;
	}

	public String getCcyBasis() {
		return ccyBasis;
	}

	public void setCcyBasis(String ccyBasis) {
		this.ccyBasis = ccyBasis;
	}

	public Map<Double, PanelRange> getPanelRangeSortedMap() {
		return panelRangeSortedMap;
	}

	public void setPanelRangeSortedMap(Map<Double, PanelRange> panelRangeSortedMap) {
		this.panelRangeSortedMap = panelRangeSortedMap;
	}




}
