package com.ams.tradeportal.busobj.util;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.common.SQLParamFilter;
import com.amsinc.ecsg.frame.AmsConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.frame.ErrorManager;
import com.amsinc.ecsg.frame.IssuedError;
import com.amsinc.ecsg.frame.ObjectIDCache;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.StringFunction;

/**
 * The StructuredPOLogger is a specialized database access class.  It
 * performs inserts, deletes, and reads to the PO_UPLOAD_ERROR_LOG file.
 * This class was created (rather than using an EJB) for performance
 * reasons (to reduce the number of EJbs that are part of a transaction.
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 *

 * @version 1.0 
 */
public class StructuredPOLogger {
private static final Logger LOG = LoggerFactory.getLogger(StructuredPOLogger.class);

	private String INSERT_SQL =
		"INSERT INTO PO_UPLOAD_ERROR_LOG " +
		" (DISPLAYABLE, PO_UPLOAD_ERROR_LOG_OID, P_PO_FILE_UPLOAD_OID, TEXT, A_CORP_ORG_OID )" +
		" VALUES ('N', ";

    private String UPDATE_SQL =
            "UPDATE PO_UPLOAD_ERROR_LOG SET TEXT = TEXT || {0} WHERE PO_UPLOAD_ERROR_LOG_OID = {1}" ;

	private static Hashtable instanceCache = new Hashtable();

	private String corporateOrg;


	/**
	 * Create method is private because all references to class should be
	 * through one of the add log message methods.
	 */
	private StructuredPOLogger()
	{
	}

	/**
	 * Create method is private because all references to class should be
	 * through one of the add log message methods.
	 *
	 * @param corporateOrg java.lang.String
	 */
	private StructuredPOLogger(String corporateOrg) {
		this.corporateOrg = corporateOrg;
	}


	/**
	 * This method obtains the next valid object ID from the ObjectIDCache and
	 * returns it as a long.
	 *
	 * @return long The newly generated unique object identifier.

	 */
	private synchronized long generateObjectID() throws AmsException{
	    ObjectIDCache oid = ObjectIDCache.getInstance(AmsConstants.OID);
		return oid.generateObjectID();
	}

	/**
	 * Inserts a blank line to the po_upload_error_log table.  Each corporate org
	 * has their own messages and messages are grouped by sequence number.  The
	 * sequence number is used to refer to a particular upload occurence (i.e.,
	 * all the po line items uploaded in a file are given the same sequence).
	 *
	 * @param corporateOrg java.lang.String - the org this message is for
	 * @param sequence long - the upload sequence number for this message
	 */
	public void addBlankLine(String corporateOrg, String sequence) throws AmsException
	{
	       insertLogMessage(corporateOrg, sequence, "");
	}

	/**
	 * Inserts a log message to the po_upload_error_log table.  Each corporate org
	 * has their own messages and messages are grouped by sequence number.  The
	 * sequence number is used to refer to a particular upload occurence (i.e.,
	 * all the po line items uploaded in a file are given the same sequence).
	 *
	 * @param corporateOrg java.lang.String - the org this message is for
	 * @param sequence long - the upload sequence number for this message
	 * @param errorCode java.lang.String - errorCode of the message to write
	 * @param sub1 java.lang.String - The first substitution value for the error text message     
	 * @param sub2 java.lang.String - The second substitution value for the error text message     
	 * @param sub3 java.lang.String - The third substitution value for the error text message     
	 * @param sub4 java.lang.String - The fourth substitution value for the error text message
	 */
	public void addLogMessage(String corporateOrg, String sequence, String errorCode, String sub1, String sub2, 
	                          String sub3, String sub4) throws AmsException
	{
	   String[]   substitutionValues = {sub1, sub2, sub3, sub4};

	   addLogMessage(corporateOrg, sequence, errorCode, substitutionValues);
	}

	/**
	 * Inserts a log message to the po_upload_error_log table.  Each corporate org
	 * has their own messages and messages are grouped by sequence number.  The
	 * sequence number is used to refer to a particular upload occurence (i.e.,
	 * all the po line items uploaded in a file are given the same sequence).
	 *
	 * @param corporateOrg java.lang.String - the org this message is for
	 * @param sequence long - the upload sequence number for this message
	 * @param errorCode java.lang.String - errorCode of the message to write
	 * @param sub1 java.lang.String - The first substitution value for the error text message     
	 * @param sub2 java.lang.String - The second substitution value for the error text message     
	 * @param sub3 java.lang.String - The third substitution value for the error text message     
	 */
	public void addLogMessage(String corporateOrg, String sequence, String errorCode, String sub1, String sub2, 
	                          String sub3) throws AmsException
	{
	   String[]   substitutionValues = {sub1, sub2, sub3};

	   addLogMessage(corporateOrg, sequence, errorCode, substitutionValues);
	}

	/**
	 * Inserts a log message to the po_upload_error_log table.  Each corporate org
	 * has their own messages and messages are grouped by sequence number.  The
	 * sequence number is used to refer to a particular upload occurence (i.e.,
	 * all the po line items uploaded in a file are given the same sequence).
	 *
	 * @param corporateOrg java.lang.String - the org this message is for
	 * @param sequence long - the upload sequence number for this message
	 * @param errorCode java.lang.String - errorCode of the message to write
	 * @param sub1 java.lang.String - The first substitution value for the error text message     
	 * @param sub2 java.lang.String - The second substitution value for the error text message     
	 */
	public void addLogMessage(String corporateOrg, String sequence, String errorCode, String sub1, String sub2) 
	                          throws AmsException
	{
	   String[]   substitutionValues = {sub1, sub2};

	   addLogMessage(corporateOrg, sequence, errorCode, substitutionValues);
	}

	/**
	 * Inserts a log message to the po_upload_error_log table.  Each corporate org
	 * has their own messages and messages are grouped by sequence number.  The
	 * sequence number is used to refer to a particular upload occurence (i.e.,
	 * all the po line items uploaded in a file are given the same sequence).
	 *
	 * @param corporateOrg java.lang.String - the org this message is for
	 * @param sequence long - the upload sequence number for this message
	 * @param errorCode java.lang.String - errorCode of the message to write
	 * @param sub1 java.lang.String - The first substitution value for the error text message     
	 */
	public void addLogMessage(String corporateOrg, String sequence, String errorCode, String sub1) throws AmsException
	{
	   String[]   substitutionValues = {sub1};

	   addLogMessage(corporateOrg, sequence, errorCode, substitutionValues);
	}

	/**
	 * Inserts a log message to the po_upload_error_log table.  Each corporate org
	 * has their own messages and messages are grouped by sequence number.  The
	 * sequence number is used to refer to a particular upload occurence (i.e.,
	 * all the po line items uploaded in a file are given the same sequence).
	 *
	 * @param corporateOrg java.lang.String - the org this message is for
	 * @param sequence long - the upload sequence number for this message
	 * @param errorCode java.lang.String - errorCode of the message to write
	 */
	public void addLogMessage(String corporateOrg, String sequence, String errorCode) throws AmsException
	{
	   String[]   substitutionValues = null;
	   addLogMessage(corporateOrg, sequence, errorCode, substitutionValues);
	}

	/**
	 * Adds a log message to the po_upload_error_log table.  Each corporate org
	 * has their own messages and messages are grouped by sequence number.  The
	 * sequence number is used to refer to a particular upload occurence (i.e.,
	 * all the po line items uploaded in a file are given the same sequence).
	 *
	 * @param corporateOrg java.lang.String - the org this message is for
	 * @param sequence long - the upload sequence number for this message
	 * @param errorCode java.lang.String - errorCode of the message to write
	 * @param msgParms java.lang.String[] - array of parms to put in message text
	 */
	public void addLogMessage(String corporateOrg, String sequence, String errorCode, String[] msgParms) 
		throws AmsException {

		try {
                        boolean[] emptyArray = null;
			IssuedError error = ErrorManager.getIssuedError("", errorCode, msgParms, emptyArray);
			insertLogMessage(corporateOrg, sequence, error.getErrorText());
		} catch (Exception e) {
			throw new AmsException(
				"Error inserting Structured PO Log message. Nested Excpetion: " + e.getMessage());
		}

	}

	
	public void addLogMessageText(String corporateOrg, String sequence, String text) throws AmsException { 

			insertLogMessage(corporateOrg, sequence, text);

	}
	
	
	/**
	 * Inserts a log message to the po_upload_error_log table.  Each corporate org
	 * has their own messages and messages are grouped by sequence number.  The
	 * sequence number is used to refer to a particular upload occurence (i.e.,
	 * all the po line items uploaded in a file are given the same sequence).
	 *
	 * @param corporateOrg java.lang.String - the org this message is for
	 * @param sequence long - the upload sequence number for this message
	 * @param message java.lang.String - message to write
	 */
	private void insertLogMessage(String corporateOrg, String sequence, String message) 
		throws AmsException {
        StringBuilder sql = new StringBuilder("");
        try {
        DocumentHandler resultsDoc = fetchLogMessage(sequence, corporateOrg);
        if (resultsDoc != null){

            String poUploadErrorLogOid = resultsDoc.getAttribute("/ResultSetRecord(0)/PO_UPLOAD_ERROR_LOG_OID");
            message = "'" + SQLParamFilter.filter(message) +" \\n'"; 
            Object[] arguments = {message, poUploadErrorLogOid};

            sql.append(MessageFormat.format(UPDATE_SQL, arguments));
            int resultCount = DatabaseQueryBean.executeUpdate(sql.toString(),false,new ArrayList<Object>());

        }else{

		    sql = new StringBuilder(INSERT_SQL);
			
			// Construct the sql for the insert using the given parameters.
			// (Note that the message may contain single quotes which must
			// be converted to two single quotes in order for the sql to
			// work correctly.)
			sql.append("?, ?, ?, ?)");

			int resultCount = DatabaseQueryBean.executeUpdate(sql.toString(), false, new Object[]{generateObjectID(), sequence, message, corporateOrg});
        }
		} catch (SQLException e) {
			throw new AmsException("SQL Exception found executing sql statement \n"
				+ sql.toString()
				+ "\nException is " + e.getMessage());
		}
	

	}

	/**
	 * Used in lieu of a constructor.  Because this class operates as a 
	 * singleton (by corporate org), the getInstance returns the ONE instance
	 * of the class for that org.
	 *
	 * @param corporateOrg java.lang.String - determines which instance of the
	 *                 logger to return.
	 */
	public static StructuredPOLogger getInstance(String corporateOrg)
	{
		StructuredPOLogger myInstance = (StructuredPOLogger) instanceCache.get(corporateOrg);
		if (myInstance == null)
		{
		   myInstance = new StructuredPOLogger(corporateOrg);
		   instanceCache.put(corporateOrg, myInstance);
		}

		return myInstance;
	}
    
    
    @SuppressWarnings("deprecation")
	private DocumentHandler fetchLogMessage(String sequence, String corporateOrg) throws AmsException    {
        String selectSQL = "SELECT PO_UPLOAD_ERROR_LOG_OID FROM PO_UPLOAD_ERROR_LOG WHERE ";
        List<Object> sqlPrmsList = new ArrayList();
        if (!StringFunction.isBlank(sequence)){
            selectSQL += " P_PO_FILE_UPLOAD_OID = ? and ";
            sqlPrmsList.add(sequence);
        }
        if (!StringFunction.isBlank(corporateOrg)) {
            selectSQL += (" A_CORP_ORG_OID = ?");
            sqlPrmsList.add(corporateOrg);
        }
        try {
           return DatabaseQueryBean.getXmlResultSet(selectSQL, false, sqlPrmsList);

        } catch (AmsException e) {
            throw new AmsException("SQL Exception found executing count statement \n"
                    + "\nException is " + e.getMessage());
        }
    }
}