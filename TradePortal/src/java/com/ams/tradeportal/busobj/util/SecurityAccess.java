package com.ams.tradeportal.busobj.util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.busobj.SecurityProfile;
import com.ams.tradeportal.busobj.User;
import com.ams.tradeportal.common.*;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.BusinessObject;
import com.amsinc.ecsg.util.DocumentHandler;

import java.math.*;
import java.rmi.RemoteException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A set of constants (indicating security rights) and methods for determining
 * if a user has certain rights.
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */


public class SecurityAccess {
private static final Logger LOG = LoggerFactory.getLogger(SecurityAccess.class);
	// Security rights.  We will use bitwise comparision to test for
	// access.  Each right is assigned a unique BigInteger value of
	// a power of 2 (thus each right is a binary number consisting of
	// only one 1 and a bunch of 0s.  The various rights a user has
	// are ANDed together to come up with a single long value.

	private final static BigInteger ZERO = new BigInteger("0");
	private final static BigInteger TWO  = new BigInteger("2");

	// NEVER EVER change the values assigned to these constants!
	public final static BigInteger NO_ACCESS                  = ZERO;
	public final static BigInteger ACCESS_ADM_REFDATA_AREA    = TWO.pow(0);
	public final static BigInteger ACCESS_INSTRUMENTS_AREA    = TWO.pow(1);
	public final static BigInteger ACCESS_MESSAGES_AREA       = TWO.pow(2);
	public final static BigInteger ACCESS_ORGANIZATION_AREA   = TWO.pow(3);
	public final static BigInteger ACCESS_REFDATA_AREA        = TWO.pow(4);
	public final static BigInteger ACCESS_REPORTS_AREA        = TWO.pow(5);
	public final static BigInteger AIR_WAYBILL_AUTHORIZE      = TWO.pow(6);
	public final static BigInteger AIR_WAYBILL_CREATE_MODIFY  = TWO.pow(7);
	public final static BigInteger AIR_WAYBILL_DELETE         = TWO.pow(8);
	public final static BigInteger AIR_WAYBILL_ROUTE          = TWO.pow(9);
	public final static BigInteger EXPORT_COLL_AUTHORIZE      = TWO.pow(10);
	public final static BigInteger EXPORT_COLL_CREATE_MODIFY  = TWO.pow(11);
	public final static BigInteger EXPORT_COLL_DELETE         = TWO.pow(12);
	public final static BigInteger EXPORT_COLL_ROUTE          = TWO.pow(13);
	public final static BigInteger EXPORT_LC_AUTHORIZE        = TWO.pow(14);
	public final static BigInteger EXPORT_LC_CREATE_MODIFY    = TWO.pow(15);
	public final static BigInteger EXPORT_LC_DELETE           = TWO.pow(16);
	public final static BigInteger EXPORT_LC_ROUTE            = TWO.pow(17);
	public final static BigInteger GUAR_AUTHORIZE             = TWO.pow(18);
	public final static BigInteger GUAR_CREATE_MODIFY         = TWO.pow(19);
	public final static BigInteger GUAR_DELETE                = TWO.pow(20);
	public final static BigInteger GUAR_ROUTE                 = TWO.pow(21);
	public final static BigInteger DLC_AUTHORIZE              = TWO.pow(22);
	public final static BigInteger DLC_CREATE_MODIFY          = TWO.pow(23);
	public final static BigInteger DLC_DELETE                 = TWO.pow(24);
	public final static BigInteger DLC_ROUTE                  = TWO.pow(25);
	public final static BigInteger SLC_AUTHORIZE              = TWO.pow(26);
	public final static BigInteger SLC_CREATE_MODIFY          = TWO.pow(27);
	public final static BigInteger SLC_DELETE                 = TWO.pow(28);
	public final static BigInteger SLC_ROUTE                  = TWO.pow(29);
	// LC_ASSIGN and LC_TRANSFER are not currently used.
	public final static BigInteger LC_ASSIGN_AUTHORIZE        = TWO.pow(30);
	public final static BigInteger LC_ASSIGN_CREATE_MODIFY    = TWO.pow(31);
	public final static BigInteger LC_ASSIGN_DELETE           = TWO.pow(32);
	public final static BigInteger LC_ASSIGN_ROUTE            = TWO.pow(33);
	public final static BigInteger LC_TRANSFER_AUTHORIZE      = TWO.pow(34);
	public final static BigInteger LC_TRANSFER_CREATE_MODIFY  = TWO.pow(35);
	public final static BigInteger LC_TRANSFER_DELETE         = TWO.pow(36);
	public final static BigInteger LC_TRANSFER_ROUTE          = TWO.pow(37);
	public final static BigInteger SHIP_GUAR_AUTHORIZE        = TWO.pow(38);
	public final static BigInteger SHIP_GUAR_CREATE_MODIFY    = TWO.pow(39);
	public final static BigInteger SHIP_GUAR_DELETE           = TWO.pow(40);
	public final static BigInteger SHIP_GUAR_ROUTE            = TWO.pow(41);
	public final static BigInteger VIEW_CHILD_ORG_WORK        = TWO.pow(42);
	public final static BigInteger MAINTAIN_FX_RATE           = TWO.pow(43);
	public final static BigInteger MAINTAIN_PARTIES           = TWO.pow(44);
	public final static BigInteger MAINTAIN_PHRASE            = TWO.pow(45);
	public final static BigInteger MAINTAIN_SEC_PROF          = TWO.pow(46);
	public final static BigInteger MAINTAIN_TEMPLATE          = TWO.pow(47);
	public final static BigInteger MAINTAIN_THRESH_GRP        = TWO.pow(48);
	public final static BigInteger MAINTAIN_USERS             = TWO.pow(49);
	public final static BigInteger MAINTAIN_ADM_SEC_PROF      = TWO.pow(50);
	public final static BigInteger MAINTAIN_ADM_USERS         = TWO.pow(51);
	public final static BigInteger VIEW_FX_RATE               = TWO.pow(52);
	public final static BigInteger VIEW_PARTIES               = TWO.pow(53);
	public final static BigInteger VIEW_PHRASE                = TWO.pow(54);
	public final static BigInteger VIEW_SEC_PROF              = TWO.pow(55);
	public final static BigInteger VIEW_TEMPLATE              = TWO.pow(56);
	public final static BigInteger VIEW_THRESH_GRP            = TWO.pow(57);
	public final static BigInteger VIEW_USERS                 = TWO.pow(58);
	public final static BigInteger VIEW_ADM_SEC_PROF          = TWO.pow(59);
	public final static BigInteger VIEW_ADM_USERS             = TWO.pow(60);
	public final static BigInteger DELETE_MESSAGE             = TWO.pow(61);
	public final static BigInteger DELETE_DISCREPANCY_MSG     = TWO.pow(62);
	public final static BigInteger DELETE_NOTIFICATION        = TWO.pow(63);
	public final static BigInteger CREATE_MESSAGE             = TWO.pow(64);
	public final static BigInteger ROUTE_MESSAGE	          	= TWO.pow(65);
	public final static BigInteger SEND_MESSAGE               = TWO.pow(66);
	public final static BigInteger ROUTE_DISCREPANCY_MSG      = TWO.pow(67);
	public final static BigInteger VIEW_CHILD_ORG_MESSAGES    = TWO.pow(82);
	public final static BigInteger MAINTAIN_NOTIF_RULE_TEMPLATES	  	= TWO.pow(98);//Rel9.5 CR 927B
	public final static BigInteger VIEW_NOTIF_RULE_TEMPLATES	        = TWO.pow(99);//Rel9.5 CR 927B
	public final static BigInteger MAINTAIN_WORK_GROUPS	  	= TWO.pow(100);
	public final static BigInteger VIEW_WORK_GROUPS	        = TWO.pow(101);

	// CR-186 - pcutrone - Attach/Delete Document Security
	public final static BigInteger ATTACH_DOC_MESSAGE		= TWO.pow(107);
	public final static BigInteger DELETE_DOC_MESSAGE		= TWO.pow(108);
	public final static BigInteger ATTACH_DOC_TRANSACTION		= TWO.pow(109);
	public final static BigInteger DELETE_DOC_TRANSACTION		= TWO.pow(110);

		// The next few define access to organizations by types of admin users.
		// CB=Client Bank, BG=Bank Group, CORP=Corporate Customer,
		// OPBANK=Operational Bank, PX=Proponix
		// Thus VIEW_CB_BY_PX is "Proponix admin user can view client bank orgs"
	public final static BigInteger VIEW_CB_BY_PX              = TWO.pow(68);
	public final static BigInteger VIEW_BG_BY_CB              = TWO.pow(69);
	public final static BigInteger VIEW_CORP_BY_CB            = TWO.pow(70);
	public final static BigInteger VIEW_OPBANK_BY_CB          = TWO.pow(71);
	public final static BigInteger VIEW_CORP_BY_BG            = TWO.pow(72);
	public final static BigInteger MAINTAIN_CB_BY_PX          = TWO.pow(73);
	public final static BigInteger MAINTAIN_BG_BY_CB          = TWO.pow(74);
	public final static BigInteger MAINTAIN_CORP_BY_CB        = TWO.pow(75);
	public final static BigInteger MAINTAIN_OPBANK_BY_CB      = TWO.pow(76);
	public final static BigInteger MAINTAIN_CORP_BY_BG        = TWO.pow(77);
		// The next few constants define access to discrepancy transactions, regardless
		// of the instrument type
	public final static BigInteger DISCREPANCY_CREATE_MODIFY  = TWO.pow(78);
	public final static BigInteger DISCREPANCY_DELETE	  = TWO.pow(79);
	public final static BigInteger DISCREPANCY_ROUTE	  = TWO.pow(80);
	public final static BigInteger DISCREPANCY_AUTHORIZE	  = TWO.pow(81);

		// These are for PO processing.
	public final static BigInteger MAINTAIN_LC_CREATE_RULES   = TWO.pow(83);
	public final static BigInteger MAINTAIN_PO_UPLOAD_DEFN    = TWO.pow(84);
	public final static BigInteger VIEW_LC_CREATE_RULES       = TWO.pow(85);
	public final static BigInteger VIEW_PO_UPLOAD_DEFN        = TWO.pow(86);
	public final static BigInteger DLC_PROCESS_PO             = TWO.pow(87);

		// New for loan request and funds transfer instrument types.
	public final static BigInteger LOAN_RQST_AUTHORIZE        = TWO.pow(88);
	public final static BigInteger LOAN_RQST_CREATE_MODIFY    = TWO.pow(89);
	public final static BigInteger LOAN_RQST_DELETE           = TWO.pow(90);
	public final static BigInteger LOAN_RQST_ROUTE            = TWO.pow(91);
	public final static BigInteger FUNDS_XFER_AUTHORIZE       = TWO.pow(92);
	public final static BigInteger FUNDS_XFER_CREATE_MODIFY   = TWO.pow(93);
	public final static BigInteger FUNDS_XFER_DELETE          = TWO.pow(94);
	public final static BigInteger FUNDS_XFER_ROUTE           = TWO.pow(95);

	// Security rights for resetting a user account
	public final static BigInteger MAINTAIN_LOCKED_OUT_USERS  = TWO.pow(96);
	public final static BigInteger VIEW_LOCKED_OUT_USERS      = TWO.pow(97);

	// Security rights for Request to Advise instrument type
	public final static BigInteger REQUEST_ADVISE_AUTHORIZE        = TWO.pow(102);
	public final static BigInteger REQUEST_ADVISE_CREATE_MODIFY    = TWO.pow(103);
	public final static BigInteger REQUEST_ADVISE_DELETE           = TWO.pow(104);
	public final static BigInteger REQUEST_ADVISE_ROUTE            = TWO.pow(105);

	//Krishna CR 375-D 07/19/2007 Begin
	// Security rights for Approval to Pay instrument type
	public final static BigInteger APPROVAL_TO_PAY_AUTHORIZE        = TWO.pow(111);
	public final static BigInteger APPROVAL_TO_PAY_CREATE_MODIFY    = TWO.pow(112);
	public final static BigInteger APPROVAL_TO_PAY_DELETE           = TWO.pow(113);
	public final static BigInteger APPROVAL_TO_PAY_ROUTE            = TWO.pow(114);
	public final static BigInteger APPROVAL_TO_PAY_PROCESS_PO       = TWO.pow(115);
        //Krishna CR 375-D 07/19/2007 End

	// TLE - 07/19/07 - CR-375 - Add Begin
	public final static BigInteger MAINTAIN_ATP_CREATE_RULES   = TWO.pow(116);
	public final static BigInteger VIEW_ATP_CREATE_RULES       = TWO.pow(117);
	// TLE - 07/19/07 - CR-375 - Add End

	//rbhaduri - 18th Jan 06 - CR239 - Security rights to Create New Party during Transaction
	public final static BigInteger CREATE_NEW_TRANSACTION_PARTY    = TWO.pow(106);

	// crhodes 10/15/2008 CR-451 Begin
	// Security rights for transfer between accounts
	public final static BigInteger TRANSFER_AUTHORIZE        = TWO.pow(118);
	public final static BigInteger TRANSFER_CREATE_MODIFY    = TWO.pow(119);
	public final static BigInteger TRANSFER_DELETE           = TWO.pow(120);
	public final static BigInteger TRANSFER_ROUTE            = TWO.pow(121);

	// Security rights for domestic payments
	public final static BigInteger DOMESTIC_AUTHORIZE        = TWO.pow(122);
	public final static BigInteger DOMESTIC_CREATE_MODIFY    = TWO.pow(123);
	public final static BigInteger DOMESTIC_DELETE           = TWO.pow(124);
	public final static BigInteger DOMESTIC_ROUTE            = TWO.pow(125);
	public final static BigInteger DOMESTIC_UPLOAD_BULK_FILE = TWO.pow(126);
	// crhodes 10/15/2008 CR-451 End

	// crhodes 10/15/2008 CR-434 Begin
	public final static BigInteger ACCESS_RECEIVABLES_AREA					= TWO.pow(127);
	public final static BigInteger VIEW_CHILD_ORG_RECEIVABLES				= TWO.pow(128);
	public final static BigInteger RECEIVABLES_MATCH						= TWO.pow(129);
	public final static BigInteger RECEIVABLES_ROUTE						= TWO.pow(130);
	public final static BigInteger RECEIVABLES_AUTHORIZE					= TWO.pow(131);
	public final static BigInteger RECEIVABLES_APPROVE_DISCOUNT_AUTHORIZE	= TWO.pow(132);
	public final static BigInteger RECEIVABLES_ADD_BUYER_TO_PAYMENT			= TWO.pow(133);
	public final static BigInteger RECEIVABLES_DISPUTE_UNDISPUTE			= TWO.pow(134);
	public final static BigInteger RECEIVABLES_CLOSE						= TWO.pow(135);
	public final static BigInteger RECEIVABLES_FINANCE						= TWO.pow(136);

	public final static BigInteger ACCESS_REC_NOTICES_AREA					= TWO.pow(137);

	public final static BigInteger VIEW_RECEIVABLES_MATCH_RULES			= TWO.pow(138);
	public final static BigInteger MAINTAIN_RECEIVABLES_MATCH_RULES		= TWO.pow(139);
	public final static BigInteger REC_NOTICES_ROUTE						= TWO.pow(140);
	// crhodes 10/15/2008 CR-434 End
	public final static BigInteger MAINTAIN_PANEL_AUTH_GRP              = TWO.pow(141);		//IAZ CR-511 11/09/09 Add
	public final static BigInteger VIEW_PANEL_AUTH_GRP                  = TWO.pow(142);		//IAZ CR-511 11/09/09 Add


	// VSHAH 12/14/09 CR#509 Begin

	public final static BigInteger ACCESS_DIRECTDEBIT_AREA					= TWO.pow(143);
	public final static BigInteger VIEW_CHILD_ORG_DIRECTDEBIT				= TWO.pow(144);
	public final static BigInteger DDI_CREATE_MODIFY					= TWO.pow(145);
	public final static BigInteger DDI_DELETE						= TWO.pow(146);
	public final static BigInteger DDI_ROUTE						= TWO.pow(147);
	public final static BigInteger DDI_AUTHORIZE						= TWO.pow(148);
	public final static BigInteger DDI_UPLOAD_FILE						= TWO.pow(149);

	// VSHAH 12/14/09 CR#509 End

    // Tang 12/01/2009 CR-507 Begin
    public final static BigInteger MAINTAIN_BANK_BRANCH_RULES           = TWO.pow(150);
    public final static BigInteger VIEW_BANK_BRANCH_RULES               = TWO.pow(151);
    public final static BigInteger MAINTAIN_PAYMENT_METH_VAL            = TWO.pow(152);
    public final static BigInteger VIEW_PAYMENT_METH_VAL                = TWO.pow(153);
    // Tang 12/01/2009 CR-507 End

    // [START] 20100106 CR-507
    public final static BigInteger MAINTAIN_CALENDAR           			= TWO.pow(154);
    public final static BigInteger VIEW_CALENDAR               			= TWO.pow(155);
    public final static BigInteger MAINTAIN_CURRENCY_CALENDAR_RULES     = TWO.pow(156);
    public final static BigInteger VIEW_CURRENCY_CALENDAR_RULES         = TWO.pow(157);
    // [END] 20100106 CR-507

    // [START] Vasavi CR 524 03/31/2010
    public final static BigInteger NEW_EXPORT_COLL_AUTHORIZE      = TWO.pow(158);
	public final static BigInteger NEW_EXPORT_COLL_CREATE_MODIFY  = TWO.pow(159);
	public final static BigInteger NEW_EXPORT_COLL_DELETE         = TWO.pow(160);
	public final static BigInteger NEW_EXPORT_COLL_ROUTE          = TWO.pow(161);
    // [END] Vasavi CR 524 03/31/2010

    //Vshah  CR-586 [START]
	public final static BigInteger MAINTAIN_PAYMENT_TEMPLATE_GRP              = TWO.pow(162);
	public final static BigInteger VIEW_PAYMENT_TEMPLATE_GRP                  = TWO.pow(163);
	public final static BigInteger VIEW_OR_MAINTAIN_PAYMENT_TEMPLATE_GRP 	  = VIEW_PAYMENT_TEMPLATE_GRP.or(MAINTAIN_PAYMENT_TEMPLATE_GRP);

	public final static BigInteger CREATE_FIXED_PAYMENT_TEMPLATE    		  = TWO.pow(164);
	//Vshah  CR-586 [END]
	//Suresh CR-603 03/22/2011 Begin
	public final static BigInteger VIEW_STANDARD_REPORT               = TWO.pow(165);
	public final static BigInteger MAINTAIN_STANDARD_REPORT           = TWO.pow(166);
	public final static BigInteger VIEW_CUSTOM_REPORT               = TWO.pow(167);
	public final static BigInteger MAINTAIN_CUSTOM_REPORT           = TWO.pow(168);
	public final static BigInteger ACCESS_REPORT_AREA              = TWO.pow(169);
	public final static BigInteger VIEW_OR_MAINTAIN_STANDARD_REPORT	=
							 VIEW_STANDARD_REPORT.or(MAINTAIN_STANDARD_REPORT);
	public final static BigInteger VIEW_OR_MAINTAIN_CUSTOM_REPORT	=
		 					 VIEW_CUSTOM_REPORT.or(MAINTAIN_CUSTOM_REPORT);
	//Suresh CR-603 End

	//DK CR-587 Rel7.1 Begin
	public final static BigInteger MAINTAIN_GM_CAT_GRP             = TWO.pow(170);
	public final static BigInteger VIEW_GM_CAT_GRP                 = TWO.pow(171);
	public final static BigInteger VIEW_OR_MAINTAIN_GM_CAT_GRP 	   = VIEW_GM_CAT_GRP.or(MAINTAIN_GM_CAT_GRP);
	//DK CR-587 Rel7.1 End

	//DK CR-581/640 Rel7.1 Begin
		public final static BigInteger MAINTAIN_CROSS_RATE_RULE          = TWO.pow(172);
		public final static BigInteger VIEW_CROSS_RATE_RULE              = TWO.pow(173);
		public final static BigInteger VIEW_OR_MAINTAIN_CROSS_RATE_RULE  = VIEW_CROSS_RATE_RULE.or(MAINTAIN_CROSS_RATE_RULE);
	//DK CR-581/640 Rel7.1 End

	//Srini_D CR-713 10/13/2011 Rel8.0 Start
	public final static BigInteger VIEW_INTEREST_RATE               = TWO.pow(174);
	public final static BigInteger MAINTAIN_INTEREST_RATE             = TWO.pow(175);
	//Srini_D CR-713 10/13/2011 Rel8.0 End

	//AAlubala CR-711 Rel8.0 - New proxy Authorize fields - 10/06/2011 - start

	public final static BigInteger PROXY_DLC_AUTHORIZE              = TWO.pow(176);
	public final static BigInteger PROXY_SLC_AUTHORIZE              = TWO.pow(177);
	public final static BigInteger PROXY_EXP_DLC_AUTHORIZE              = TWO.pow(178);
	public final static BigInteger PROXY_EXP_COLL_AUTHORIZE              = TWO.pow(179);
	public final static BigInteger PROXY_GUAR_AUTHORIZE              = TWO.pow(180);
	public final static BigInteger PROXY_NEW_EXP_COLL_AUTHORIZE              = TWO.pow(181);
	public final static BigInteger PROXY_SG_AUTHORIZE              = TWO.pow(182);
	public final static BigInteger PROXY_AWR_AUTHORIZE              = TWO.pow(183);
	public final static BigInteger PROXY_LR_AUTHORIZE              = TWO.pow(184);
	public final static BigInteger PROXY_TF_AUTHORIZE              = TWO.pow(185);
	public final static BigInteger PROXY_RA_AUTHORIZE              = TWO.pow(186);
	public final static BigInteger PROXY_FT_AUTHORIZE              = TWO.pow(187);
	public final static BigInteger PROXY_ATP_AUTHORIZE              = TWO.pow(188);
	public final static BigInteger PROXY_DP_AUTHORIZE              = TWO.pow(189);
	public final static BigInteger PROXY_DR_AUTHORIZE              = TWO.pow(190);
	public final static BigInteger PROXY_RECEIVABLES_AUTHORIZE              = TWO.pow(191);
	public final static BigInteger PROXY_DDI_AUTHORIZE              = TWO.pow(192);
	//AAlubala CR-711 Rel8.0 - end

	//CR-711 Rel8.0 End

	//Srini_D CR-710 11/13/2011 Rel8.0 Start
	public final static BigInteger ACCESS_INVOICE_MGT_AREA             = TWO.pow(193);
	public final static BigInteger VIEW_UPLOADED_INVOICES              = TWO.pow(194);
	public final static BigInteger UPLOAD_INVOICES            		   = TWO.pow(195);
	public final static BigInteger REMOVE_UPLOADED_FILES         	   = TWO.pow(196);

	public final static BigInteger VIEW_RECEIVABLES_TRADING_RULES       = TWO.pow(197);
	public final static BigInteger MAINTAIN_RECEIVABLES_TRADING_RULES   = TWO.pow(198);

	public final static BigInteger VIEW_INVOICE_DEFINITION       = TWO.pow(199);
	public final static BigInteger MAINTAIN_INVOICE_DEFINITION   = TWO.pow(200);

	public final static BigInteger RECEIVABLES_APPROVE_FINANCING   = TWO.pow(201);
	public final static BigInteger RECEIVABLES_REMOVE_INVOICE   = TWO.pow(202);
	public final static BigInteger RECEIVABLES_AUTHORIZE_INVOICE   = TWO.pow(203);
	public final static BigInteger RECEIVABLES_DELETE_INVOICE   = TWO.pow(204);

	//Srini_D CR-710 11/13/2011 Rel8.0 End

	//Shilpa R CR-710 Rel8.0 Start
	public final static BigInteger MAINTAIN_UPLOADED_INVOICES   = TWO.pow(205);
	//Ravindra 5th Mar 2012 Rel800 IR#LKUM021742692 - Begin
	public final static BigInteger RECEIVABLES_PROXY_APPROVE_DISCOUNT_AUTHORIZE	= TWO.pow(206);
	//Ravindra 5th Mar 2012 Rel800 IR#LKUM021742692 - End
	//AAlubala - CR710 Rel8.0 - Proxy Authorize Invoice
	public final static BigInteger PROXY_RECEIVABLES_AUTHORIZE_INVOICE   = TWO.pow(207);
	//CR710 - END
	public final static BigInteger VIEW_OR_MAINTAIN_UPLOADED_INVOICES	=
		VIEW_UPLOADED_INVOICES.or(MAINTAIN_UPLOADED_INVOICES);

	/* IR T36000005615 Changes */
	// Security rights for resetting a user account
	public final static BigInteger MAINTAIN_ANNOUNCEMENT  = TWO.pow(208);
	public final static BigInteger VIEW_ANNOUNCEMENT      = TWO.pow(209);

	//Shilpa R CR-710 Rel8.0 end

	//SHR CR-708 Rel8.1.1 START
	public final static BigInteger ATP_PROCESS_INVOICES              = TWO.pow(210);
	public final static BigInteger PAYABLES_APPLY_PAYDATE   = TWO.pow(211);
	public final static BigInteger PAYABLES_DELETE_INVOICE   = TWO.pow(212);
	public final static BigInteger PAYABLES_CREATE_ATP = TWO.pow(213);
	//SHR CR-708 Rel8.1.1 END

	//AAlubala - Rel8.2 CR741 - 12/17/2012 - ERP and DISCOUNT CODES - BEGIN
	public final static BigInteger VIEW_ERP_GL_CODES = TWO.pow(214);
	public final static BigInteger MAINTAIN_ERP_GL_CODES = TWO.pow(215);
	public final static BigInteger VIEW_DISCOUNT_CODES = TWO.pow(216);
	public final static BigInteger MAINTAIN_DISCOUNT_CODES = TWO.pow(217);
	public final static BigInteger VIEW_OR_MAINTAIN_ERP_GL_CODES = VIEW_ERP_GL_CODES.or(MAINTAIN_ERP_GL_CODES);
	public final static BigInteger VIEW_OR_MAINTAIN_DISCOUNT_CODES = VIEW_DISCOUNT_CODES.or(MAINTAIN_DISCOUNT_CODES) ;
	//CR741 - END

	// DK CR709 Rel8.2 10/31/2012 BEGINS

	public final static BigInteger PAY_CLEAR_PAYMENT_DATE   = TWO.pow(218);
	public final static BigInteger PAY_ASSIGN_INSTR_TYPE   = TWO.pow(219);
	public final static BigInteger PAY_ASSIGN_LOAN_TYPE   = TWO.pow(220);
	public final static BigInteger PAY_CREATE_LOAN_REQ   = TWO.pow(221);
	public final static BigInteger MAINTAIN_ATP_INV_CREATE_RULES   = TWO.pow(222);
	public final static BigInteger VIEW_ATP_INV_CREATE_RULES       = TWO.pow(223);
	public final static BigInteger LOAN_RQST_PROCESS_INVOICES      = TWO.pow(224);
	public final static BigInteger MAINTAIN_LR_CREATE_RULES   = TWO.pow(225);
	public final static BigInteger VIEW_LR_CREATE_RULES       = TWO.pow(226);
	public final static BigInteger RECEIVABLES_APPLY_PAYMENT_DATE   = TWO.pow(227);

	public final static BigInteger RECEIVABLES_CLEAR_PAYMENT_DATE   = TWO.pow(228);
	public final static BigInteger RECEIVABLES_ASSIGN_INSTR_TYPE   = TWO.pow(229);
	public final static BigInteger RECEIVABLES_ASSIGN_LOAN_TYPE   = TWO.pow(230);
	public final static BigInteger RECEIVABLES_CREATE_LOAN_REQ   = TWO.pow(231);

	// DK CR709 Rel8.2 10/31/2012 ENDS


	//Ravindra - CR-708B - Start
	public final static BigInteger SP_INVOICE_OFFERED = TWO.pow(232);
	public final static BigInteger SP_HISTORY = TWO.pow(233);
	public final static BigInteger SP_ACCEPT_OFFER = TWO.pow(234);
	public final static BigInteger SP_AUTHORIZE_OFFER = TWO.pow(235);
	public final static BigInteger SP_DECLINE_OFFER = TWO.pow(236);
	public final static BigInteger SP_ASSIGN_FVD = TWO.pow(237);
	public final static BigInteger SP_REMOVE_FVD = TWO.pow(238);
	public final static BigInteger SP_REMOVE_INVOICE = TWO.pow(239);
	public final static BigInteger SP_RESET_TO_OFFERED = TWO.pow(240);
	//Ravindra - CR-708B - End


	// VIEW for ref data means view only.  MAINTAIN means maintain (with
	// implied view access.)  However, to test if a user can view (for either
	// view or maintain access), you must use these VIEW_OR_MAINTAIN_xxx constants.
	//CR 821 - Rel 8.3 START
	public final static BigInteger APPROVAL_TO_PAY_CHECKER = TWO.pow(241);
	public final static BigInteger APPROVAL_TO_PAY_REPAIR = TWO.pow(242);
	public final static BigInteger DOMESTIC_CHECKER = TWO.pow(243);
	public final static BigInteger DOMESTIC_REPAIR = TWO.pow(244);
	public final static BigInteger DLC_CHECKER = TWO.pow(245);
	public final static BigInteger DLC_REPAIR = TWO.pow(246);
	public final static BigInteger SLC_CHECKER = TWO.pow(247);
	public final static BigInteger SLC_REPAIR = TWO.pow(248);
	public final static BigInteger EXPORT_LC_CHECKER = TWO.pow(249);
	public final static BigInteger EXPORT_LC_REPAIR = TWO.pow(250);
	public final static BigInteger EXPORT_COLL_CHECKER = TWO.pow(251);
	public final static BigInteger EXPORT_COLL_REPAIR = TWO.pow(252);
	public final static BigInteger GUAR_CHECKER = TWO.pow(253);
	public final static BigInteger GUAR_REPAIR = TWO.pow(254);
	public final static BigInteger NEW_EXPORT_COLL_CHECKER = TWO.pow(255);
	public final static BigInteger NEW_EXPORT_COLL_REPAIR = TWO.pow(256);
	public final static BigInteger SHIP_GUAR_CHECKER = TWO.pow(257);
	public final static BigInteger SHIP_GUAR_REPAIR = TWO.pow(258);
	public final static BigInteger AIR_WAYBILL_CHECKER = TWO.pow(259);
	public final static BigInteger AIR_WAYBILL_REPAIR = TWO.pow(260);
	public final static BigInteger LOAN_RQST_CHECKER = TWO.pow(261);
	public final static BigInteger LOAN_RQST_REPAIR = TWO.pow(262);
	public final static BigInteger TRANSFER_CHECKER = TWO.pow(263);
	public final static BigInteger TRANSFER_REPAIR = TWO.pow(264);
	public final static BigInteger REQUEST_ADVISE_CHECKER = TWO.pow(265);
	public final static BigInteger REQUEST_ADVISE_REPAIR = TWO.pow(266);
	public final static BigInteger FUNDS_XFER_CHECKER = TWO.pow(267);
	public final static BigInteger FUNDS_XFER_REPAIR = TWO.pow(268);
	public final static BigInteger DISCREPANCY_CHECKER = TWO.pow(269);
	public final static BigInteger DISCREPANCY_REPAIR = TWO.pow(270);
	//CR 821 - Rel 8.3 END 
	
	//MEerupula CR776 Rel8.3 04/17/2013
	public final static BigInteger VIEW_ORG_PROFILE = TWO.pow(271);
	public final static BigInteger MAINTAIN_ORG_PROFILE = TWO.pow(272);
	
	//Srinivasu_D CR-599 Rel8.4 11/21/2013 - Start
	public final static BigInteger MAINTAIN_PAYMENT_FILE_DEF         = TWO.pow(280);
	public final static BigInteger VIEW_PAYMENT_FILE_DEF             = TWO.pow(281);
	//Srinivasu_D CR-599 Rel8.4 11/21/2013 - eND
	
	//Rel 9.0 CR 913 Start
	public final static BigInteger PAY_MGM_AUTH							= TWO.pow(282);
	public final static BigInteger PAY_MGM_APPLY_PAYMENT_DATE			= TWO.pow(283);
	public final static BigInteger PAY_MGM_CLEAR_PAYMENT_DATE			= TWO.pow(284);
	public final static BigInteger PAY_MGM_APPLY_EARLY_PAY_AMOUNT		= TWO.pow(285);
	public final static BigInteger PAY_MGM_RESET_EARLY_PAY_AMOUNT		= TWO.pow(286);
	public final static BigInteger PAY_MGM_APPLY_SUPPLIER_DATE			= TWO.pow(287);
	public final static BigInteger PAY_MGM_RESET_SUPPLIER_DATE			= TWO.pow(288);
	public final static BigInteger UPLOAD_PAY_MGM_AUTH					= TWO.pow(289);
	public final static BigInteger UPLOAD_PAY_MGM_OFFLINE_AUTH			= TWO.pow(290);
	public final static BigInteger UPLOAD_PAY_MGM_APPLY_EARLY_PAY_AMOUNT= TWO.pow(291);
	public final static BigInteger UPLOAD_PAY_MGM_RESET_EARLY_PAY_AMOUNT= TWO.pow(292);
	public final static BigInteger UPLOAD_PAY_MGM_APPLY_SUPPLIER_DATE	= TWO.pow(293);
	public final static BigInteger UPLOAD_PAY_MGM_RESET_SUPPLIER_DATE	= TWO.pow(294);
	public final static BigInteger DELETE_PREDEBIT_FUNDING_NOTIFICATION	= TWO.pow(295);
	public final static BigInteger ROUTE_PREDEBIT_FUNDING_NOTIFICATION	= TWO.pow(296);
	public final static BigInteger PAY_MGM_OFFLINE_AUTH					= TWO.pow(297);
	//Rel 9.0 CR 913 End
		// VIEW for ref data means view only.  MAINTAIN means maintain (with
		// implied view access.)  However, to test if a user can view (for either
		// view or maintain access), you must use these VIEW_OR_MAINTAIN_xxx constants.


	
	public final static BigInteger VIEW_EXTERNAL_BANK = TWO.pow(330);
	public final static BigInteger MAINTAIN_EXTERNAL_BANK = TWO.pow(331);
 	public final static BigInteger VIEW_OR_MAINTAIN_EXTERNAL_BANKS =
		VIEW_EXTERNAL_BANK.or(MAINTAIN_EXTERNAL_BANK);
	
	
	public final static BigInteger ACCESS_CONVERSION_CENTER_AREA		= TWO.pow(332);
	public final static BigInteger CC_GUA_CREATE_MODIFY						= TWO.pow(333);
	public final static BigInteger CC_GUA_DELETE							= TWO.pow(334);
	public final static BigInteger CC_GUA_ROUTE								= TWO.pow(335);
	public final static BigInteger CC_GUA_CONVERT							= TWO.pow(336);
	
	public final static BigInteger PAY_CREDIT_NOTE_AUTH =  TWO.pow(337);
	public final static BigInteger PAY_CREDIT_NOTE_OFFLINE_AUTH =  TWO.pow(338);
	public final static BigInteger PAY_CREDIT_NOTE_DELETE =  TWO.pow(339);
	public final static BigInteger PAY_CREDIT_NOTE_MANUAL_APPLY =  TWO.pow(340);
	
	public final static BigInteger PAY_CREDIT_NOTE_MANUAL_UNAPPLY =  TWO.pow(341);
	public final static BigInteger PAY_CREDIT_NOTE_CLOSE =  TWO.pow(342);
	
	/*pgedupudi - Rel9.3 CR976A 03/09/2015*/
	public final static BigInteger MAINTAIN_BANK_GROUP_RESTRICT_RULES	  	= TWO.pow(343);
	public final static BigInteger VIEW_BANK_GROUP_RESTRICT_RULES	        = TWO.pow(344);
	
	//#RKAZI CR1006 04-24-2015 - BEGIN
	public final static BigInteger REC_INVOICE_DECLINE_AUTH   = TWO.pow(345);	
	public final static BigInteger PAY_INVOICE_DECLINE_AUTH   = TWO.pow(346);
	public final static BigInteger PAY_CREDIT_NOTE_DECLINE_AUTH   = TWO.pow(347);
	//#RKAZI CR1006 04-24-2015 - END	
	//#PGedupudi CR1006 05-14-2015
	public final static BigInteger PAY_CREDIT_NOTE_APPROVE_AUTH   = TWO.pow(348);
	
	//SSikhakolli - Rel-9.4 CR-818
	public final static BigInteger SIT_CREATE_MODIFY   = TWO.pow(362);
	public final static BigInteger SIT_DELETE   = TWO.pow(363);
	public final static BigInteger SIT_ROUTE	   = TWO.pow(364);
	public final static BigInteger SIT_AUTHORIZE   = TWO.pow(365);
	public final static BigInteger PROXY_SIT_AUTHORIZE   = TWO.pow(366);
	public final static BigInteger SIT_CHECKER   = TWO.pow(367);
	public final static BigInteger SIT_REPAIR   = TWO.pow(368);
	//SSikhakolli Rel 9.3.5 CR-1029 Adding new securities - Begin
	public final static BigInteger BANK_TRANS_APPLY_UPDATE = TWO.pow(349);
	public final static BigInteger BANK_TRANS_APPROVE = TWO.pow(350);
	public final static BigInteger BANK_TRANS_SEND_FOR_REPAIR = TWO.pow(351);
	public final static BigInteger BANK_TRANS_ATTACH_DOC = TWO.pow(352);
	public final static BigInteger BANK_TRANS_DELETE_DOC = TWO.pow(353);
	//public final static BigInteger BANK_TRANS_PRINT = TWO.pow(354); //IR-43061 Rel 9.3.5
	public final static BigInteger BANK_XML_DOWNLOAD = TWO.pow(355);
	public final static BigInteger BANK_MAIL_DELETE = TWO.pow(356);
	public final static BigInteger BANK_MAIL_CREATE_REPLY = TWO.pow(357);
	public final static BigInteger BANK_MAIL_SEND_TO_CUST = TWO.pow(358);
	public final static BigInteger BANK_MAIL_ATTACH_DOC = TWO.pow(359);
	public final static BigInteger BANK_MAIL_DELETE_DOC = TWO.pow(360);
	public final static BigInteger ACCESS_ADMIN_UPDATE_CENTRE = TWO.pow(361);
	//SSikhakolli Rel 9.3.5 CR-1029 Adding new securities - End
	
	//SSikhakolli Rel 9.5 CR-927B Adding new securities - Begin
	public final static BigInteger VIEW_NOTIFICATION_RULE = TWO.pow(369);
	public final static BigInteger MAINTAIN_NOTIFICATION_RULE = TWO.pow(370);
	//SSikhakolli Rel 9.5 CR-927B Adding new securities - End
	
	public final static BigInteger VIEW_OR_MAINTAIN_FX_RATE =
							VIEW_FX_RATE.or(MAINTAIN_FX_RATE);
	//Rel9.5 CR 927B
	public final static BigInteger VIEW_OR_MAINTAIN_NOTIF_RULE_TEMPLATES =
			VIEW_NOTIF_RULE_TEMPLATES.or(MAINTAIN_NOTIF_RULE_TEMPLATES);
	public final static BigInteger VIEW_OR_MAINTAIN_NOTIFICATION_RULE =
			VIEW_NOTIFICATION_RULE.or(MAINTAIN_NOTIFICATION_RULE);

	/*pgedupudi - Rel9.3 CR976A 03/09/2015*/
	public final static BigInteger VIEW_OR_MAINTAIN_BANK_GROUP_RESTRICT_RULES =
			VIEW_BANK_GROUP_RESTRICT_RULES.or(MAINTAIN_BANK_GROUP_RESTRICT_RULES);
	public final static BigInteger VIEW_OR_MAINTAIN_PARTIES =
							VIEW_PARTIES.or(MAINTAIN_PARTIES);
	public final static BigInteger VIEW_OR_MAINTAIN_PHRASE =
							VIEW_PHRASE.or(MAINTAIN_PHRASE);
	public final static BigInteger VIEW_OR_MAINTAIN_SEC_PROF =
							VIEW_SEC_PROF.or(MAINTAIN_SEC_PROF);
	public final static BigInteger VIEW_OR_MAINTAIN_TEMPLATE =
							VIEW_TEMPLATE.or(MAINTAIN_TEMPLATE);
	public final static BigInteger VIEW_OR_MAINTAIN_THRESH_GRP =
							VIEW_THRESH_GRP.or(MAINTAIN_THRESH_GRP);
	public final static BigInteger VIEW_OR_MAINTAIN_USERS =
							VIEW_USERS.or(MAINTAIN_USERS);
	public final static BigInteger VIEW_OR_MAINTAIN_LOCKED_OUT_USERS =
	                        VIEW_LOCKED_OUT_USERS.or(MAINTAIN_LOCKED_OUT_USERS);
	public final static BigInteger VIEW_OR_MAINTAIN_ANNOUNCEMENT =
            VIEW_ANNOUNCEMENT.or(MAINTAIN_ANNOUNCEMENT);	
	public final static BigInteger VIEW_OR_MAINTAIN_LC_CREATE_RULES =
							VIEW_LC_CREATE_RULES.or(MAINTAIN_LC_CREATE_RULES);
	public final static BigInteger VIEW_OR_MAINTAIN_PO_UPLOAD_DEFN =
							VIEW_PO_UPLOAD_DEFN.or(MAINTAIN_PO_UPLOAD_DEFN);
	public final static BigInteger VIEW_OR_MAINTAIN_ADM_SEC_PROF =
							VIEW_ADM_SEC_PROF.or(MAINTAIN_ADM_SEC_PROF);
	public final static BigInteger VIEW_OR_MAINTAIN_ADM_USERS =
							VIEW_ADM_USERS.or(MAINTAIN_ADM_USERS);
	public final static BigInteger VIEW_OR_MAINTAIN_WORK_GROUPS =
							VIEW_WORK_GROUPS.or(MAINTAIN_WORK_GROUPS);
	public final static BigInteger VIEW_OR_MAINTAIN_PANEL_AUTH_GRP =			
							VIEW_PANEL_AUTH_GRP.or(MAINTAIN_PANEL_AUTH_GRP);	
    public final static BigInteger VIEW_OR_MAINTAIN_BANK_BRANCH_RULES =
                        VIEW_BANK_BRANCH_RULES.or(MAINTAIN_BANK_BRANCH_RULES);     
    public final static BigInteger VIEW_OR_MAINTAIN_PAYMENT_METH_VAL =
                        VIEW_PAYMENT_METH_VAL.or(MAINTAIN_PAYMENT_METH_VAL);    

    public final static BigInteger VIEW_OR_MAINTAIN_CALENDAR =
        VIEW_CALENDAR.or(MAINTAIN_CALENDAR);
    public final static BigInteger VIEW_OR_MAINTAIN_CURRENCY_CALENDAR_RULES =
        VIEW_CURRENCY_CALENDAR_RULES.or(MAINTAIN_CURRENCY_CALENDAR_RULES);

	public final static BigInteger VIEW_OR_MAINTAIN_ATP_CREATE_RULES =
							VIEW_ATP_CREATE_RULES.or(MAINTAIN_ATP_CREATE_RULES);


	public final static BigInteger VIEW_OR_MAINTAIN_RECEIVABLES_MATCH_RULES =
							VIEW_RECEIVABLES_MATCH_RULES.or(MAINTAIN_RECEIVABLES_MATCH_RULES);

	

	public final static BigInteger VIEW_OR_MAINTAIN_INTEREST_RATE =
			VIEW_INTEREST_RATE.or(MAINTAIN_INTEREST_RATE);     

	public final static BigInteger VIEW_OR_MAINTAIN_INVOICE_DEFINITION =
		VIEW_INVOICE_DEFINITION.or(MAINTAIN_INVOICE_DEFINITION);     

	public final static BigInteger	 VIEW_OR_MAINTAIN_RECEIVABLES_TRADING_RULES =
		VIEW_RECEIVABLES_TRADING_RULES.or(MAINTAIN_RECEIVABLES_TRADING_RULES);     

public final static BigInteger VIEW_OR_MAINTAIN_LR_CREATE_RULES =
		VIEW_LR_CREATE_RULES.or(MAINTAIN_LR_CREATE_RULES);  

public final static BigInteger VIEW_OR_MAINTAIN_ATP_INV_CREATE_RULES =
	VIEW_ATP_INV_CREATE_RULES.or(MAINTAIN_ATP_INV_CREATE_RULES);  


public final static BigInteger VIEW_OR_MAINTAIN_ORG_PROFILE =
	VIEW_ORG_PROFILE.or(MAINTAIN_ORG_PROFILE);


public final static BigInteger VIEW_OR_MAINTAIN_PAYMENT_FILE_DEF = VIEW_PAYMENT_FILE_DEF.or(MAINTAIN_PAYMENT_FILE_DEF);


	// This variable is used to test for any instrument maintenance access.
	// It is private on purpose to keep others away from this powerful value.
	private final static BigInteger INSTRUMENT_MAINT =
	                        new BigInteger("0").or(AIR_WAYBILL_AUTHORIZE)
	                        .or(AIR_WAYBILL_CREATE_MODIFY)
							.or(AIR_WAYBILL_DELETE)
							.or(AIR_WAYBILL_ROUTE)
							.or(EXPORT_COLL_AUTHORIZE)
							.or(EXPORT_COLL_CREATE_MODIFY)
							.or(EXPORT_COLL_DELETE)
							.or(EXPORT_COLL_ROUTE)
							.or(NEW_EXPORT_COLL_AUTHORIZE) 
							.or(NEW_EXPORT_COLL_CREATE_MODIFY)
							.or(NEW_EXPORT_COLL_DELETE)
							.or(NEW_EXPORT_COLL_ROUTE)
							.or(EXPORT_LC_AUTHORIZE)
							.or(EXPORT_LC_CREATE_MODIFY)
							.or(EXPORT_LC_DELETE)
							.or(EXPORT_LC_ROUTE)
							.or(GUAR_AUTHORIZE)
							.or(GUAR_CREATE_MODIFY)
							.or(GUAR_DELETE)
							.or(GUAR_ROUTE)
							.or(DLC_AUTHORIZE)
							.or(DLC_CREATE_MODIFY)
							.or(DLC_DELETE)
							.or(DLC_ROUTE)
							.or(SLC_AUTHORIZE)
							.or(SLC_CREATE_MODIFY)
							.or(SLC_DELETE)
							.or(SLC_ROUTE)
							.or(LC_ASSIGN_AUTHORIZE)
							.or(LC_ASSIGN_CREATE_MODIFY)
							.or(LC_ASSIGN_DELETE)
							.or(LC_ASSIGN_ROUTE)
							.or(LC_TRANSFER_AUTHORIZE)
							.or(LC_TRANSFER_CREATE_MODIFY)
							.or(LC_TRANSFER_DELETE)
							.or(LC_TRANSFER_ROUTE)
							.or(SHIP_GUAR_AUTHORIZE)
							.or(SHIP_GUAR_CREATE_MODIFY)
							.or(SHIP_GUAR_DELETE)
							.or(SHIP_GUAR_ROUTE)
							.or(VIEW_CHILD_ORG_WORK)
							.or(DISCREPANCY_CREATE_MODIFY)
							.or(DISCREPANCY_AUTHORIZE)
							.or(DISCREPANCY_DELETE)
							.or(DISCREPANCY_ROUTE)
							.or(LOAN_RQST_AUTHORIZE)
							.or(LOAN_RQST_CREATE_MODIFY)
							.or(LOAN_RQST_DELETE)
							.or(LOAN_RQST_ROUTE)
							.or(FUNDS_XFER_AUTHORIZE)
							.or(FUNDS_XFER_CREATE_MODIFY)
							.or(FUNDS_XFER_DELETE)
							.or(FUNDS_XFER_ROUTE)
							.or(REQUEST_ADVISE_AUTHORIZE)
							.or(REQUEST_ADVISE_CREATE_MODIFY)
							.or(REQUEST_ADVISE_DELETE)
							.or(REQUEST_ADVISE_ROUTE)
                                                        .or(APPROVAL_TO_PAY_AUTHORIZE)      
							.or(APPROVAL_TO_PAY_CREATE_MODIFY)
							.or(APPROVAL_TO_PAY_DELETE)
							.or(APPROVAL_TO_PAY_ROUTE)
							.or(APPROVAL_TO_PAY_PROCESS_PO)
							.or(TRANSFER_AUTHORIZE) 			
							.or(TRANSFER_CREATE_MODIFY)
							.or(TRANSFER_DELETE)
							.or(TRANSFER_ROUTE)
							.or(DOMESTIC_AUTHORIZE)			
							.or(DOMESTIC_CREATE_MODIFY)
							.or(DOMESTIC_DELETE)
							.or(DOMESTIC_ROUTE)
							.or(DOMESTIC_UPLOAD_BULK_FILE)
							.or(DDI_CREATE_MODIFY)				
							.or(DDI_DELETE)
							.or(DDI_ROUTE)
							.or(DDI_AUTHORIZE)
							.or(DDI_UPLOAD_FILE)
							.or(LOAN_RQST_PROCESS_INVOICES); 


	// This variable is used to test for any instrument create or modify access.
	// This is being used ONLY for presentation - Discrepancy Response transaction
	// is not included here because it is created indirectly by replying to bank.
	public final static BigInteger VIEW_INSTRUMENT_CREATE_OR_MODIFY =
	                        new BigInteger("0").or(AIR_WAYBILL_CREATE_MODIFY)
							.or(EXPORT_COLL_CREATE_MODIFY)
							.or(NEW_EXPORT_COLL_CREATE_MODIFY) 
							.or(EXPORT_LC_CREATE_MODIFY)
							.or(GUAR_CREATE_MODIFY)
							.or(DLC_CREATE_MODIFY)
							.or(SLC_CREATE_MODIFY)
							.or(SHIP_GUAR_CREATE_MODIFY)
							.or(LOAN_RQST_CREATE_MODIFY)
							.or(FUNDS_XFER_CREATE_MODIFY)
							.or(REQUEST_ADVISE_CREATE_MODIFY)
							.or(APPROVAL_TO_PAY_CREATE_MODIFY)  
                            .or(TRANSFER_CREATE_MODIFY)		
                            .or(DOMESTIC_CREATE_MODIFY)
			    .or(DDI_CREATE_MODIFY);				

	// This variable is used to test for any instrument authorize access.
	public final static BigInteger INSTRUMENT_AUTHORIZE =
	                        new BigInteger("0").or(AIR_WAYBILL_AUTHORIZE)
							.or(EXPORT_COLL_AUTHORIZE)
							.or(NEW_EXPORT_COLL_AUTHORIZE) 
							.or(EXPORT_LC_AUTHORIZE)
							.or(GUAR_AUTHORIZE)
							.or(DLC_AUTHORIZE)
							.or(SLC_AUTHORIZE)
							.or(SHIP_GUAR_AUTHORIZE)
							.or(DISCREPANCY_AUTHORIZE)
							.or(LOAN_RQST_AUTHORIZE)
							.or(FUNDS_XFER_AUTHORIZE)
							.or(REQUEST_ADVISE_AUTHORIZE)
                            .or(APPROVAL_TO_PAY_AUTHORIZE)		
							.or(TRANSFER_AUTHORIZE)			
							.or(DOMESTIC_AUTHORIZE)
							.or(DDI_AUTHORIZE);		


	// This variable is used to test for any instrument PROXY authorize access.
	public final static BigInteger INSTRUMENT_PROXY_AUTHORIZE =
	                        new BigInteger("0").or(PROXY_AWR_AUTHORIZE)
							.or(PROXY_EXP_COLL_AUTHORIZE)
							.or(PROXY_NEW_EXP_COLL_AUTHORIZE)
							.or(PROXY_EXP_DLC_AUTHORIZE)
							.or(PROXY_GUAR_AUTHORIZE)
							.or(PROXY_DLC_AUTHORIZE)
							.or(PROXY_SLC_AUTHORIZE)
							.or(PROXY_SG_AUTHORIZE)
							.or(PROXY_DR_AUTHORIZE)
							.or(PROXY_LR_AUTHORIZE)
							.or(PROXY_FT_AUTHORIZE)
							.or(PROXY_RA_AUTHORIZE)
                            .or(PROXY_ATP_AUTHORIZE)
							.or(PROXY_TF_AUTHORIZE)
							.or(PROXY_DP_AUTHORIZE)
							.or(PROXY_DDI_AUTHORIZE);



	// This variable is used to test for any instrument delete access.
	public final static BigInteger INSTRUMENT_DELETE =
	                        new BigInteger("0").or(AIR_WAYBILL_DELETE)
							.or(EXPORT_COLL_DELETE)
							.or(NEW_EXPORT_COLL_DELETE) 
							.or(EXPORT_LC_DELETE)
							.or(GUAR_DELETE)
							.or(DLC_DELETE)
							.or(SLC_DELETE)
							.or(SHIP_GUAR_DELETE)
							.or(DISCREPANCY_DELETE)
							.or(LOAN_RQST_DELETE)
							.or(FUNDS_XFER_DELETE)
							.or(REQUEST_ADVISE_DELETE)
                            .or(APPROVAL_TO_PAY_DELETE)		
                            .or(TRANSFER_DELETE)		
							.or(DOMESTIC_DELETE)		
							.or(DDI_DELETE);	

	// This variable is used to test for any instrument route access.
	public final static BigInteger INSTRUMENT_ROUTE =
	                        new BigInteger("0").or(AIR_WAYBILL_ROUTE)
							.or(EXPORT_COLL_ROUTE)
							.or(NEW_EXPORT_COLL_ROUTE) 
							.or(EXPORT_LC_ROUTE)
							.or(GUAR_ROUTE)
							.or(DLC_ROUTE)
							.or(SLC_ROUTE)
							.or(SHIP_GUAR_ROUTE)
							.or(DISCREPANCY_ROUTE)
							.or(LOAN_RQST_ROUTE)
							.or(FUNDS_XFER_ROUTE)
							.or(REQUEST_ADVISE_ROUTE)
                            .or(APPROVAL_TO_PAY_ROUTE)   
							.or(TRANSFER_ROUTE)			
							.or(DOMESTIC_ROUTE)		
							.or(DDI_ROUTE);	


	// This variable is used to test for any reports maintenance access.
	private final static BigInteger REPORT_MAINT =
        new BigInteger("0").or(MAINTAIN_STANDARD_REPORT)
		.or(MAINTAIN_CUSTOM_REPORT);

	// This variable is used to test for any ref data maintenance access.
	// It is private on purpose to keep others away from this powerful value.
	private final static BigInteger REFDATA_MAINT =
	                        new BigInteger("0").or(MAINTAIN_FX_RATE)
							.or(MAINTAIN_PARTIES)
							.or(MAINTAIN_PHRASE)
							.or(MAINTAIN_TEMPLATE)
							.or(MAINTAIN_THRESH_GRP)
							.or(MAINTAIN_USERS)
							.or(MAINTAIN_LC_CREATE_RULES)
							.or(MAINTAIN_PO_UPLOAD_DEFN)
							.or(MAINTAIN_NOTIFICATION_RULE)
							.or(MAINTAIN_NOTIF_RULE_TEMPLATES)
							.or(MAINTAIN_BANK_GROUP_RESTRICT_RULES) /*pgedupudi - Rel9.3 CR976A 03/09/2015*/
							.or(MAINTAIN_PANEL_AUTH_GRP)					
                            .or(MAINTAIN_ATP_CREATE_RULES)
                            .or(MAINTAIN_BANK_BRANCH_RULES)                 
                            .or(MAINTAIN_PAYMENT_METH_VAL)                 
                            .or(MAINTAIN_CALENDAR)						
                            .or(MAINTAIN_CURRENCY_CALENDAR_RULES)		
							.or(MAINTAIN_INTEREST_RATE)					
                            .or(MAINTAIN_INVOICE_DEFINITION)
                            .or(MAINTAIN_LR_CREATE_RULES)
                            .or(MAINTAIN_ATP_INV_CREATE_RULES)
	                        .or(MAINTAIN_ORG_PROFILE)  
							.or(MAINTAIN_PAYMENT_FILE_DEF); 






	// This variable is used to test for any messages maintenance access.
	// It is private on purpose to keep others away from this powerful value.
	private final static BigInteger MESSAGE_MAINT =
	                        new BigInteger("0").or(DELETE_MESSAGE)
							.or(DELETE_DISCREPANCY_MSG)
							.or(DELETE_NOTIFICATION)
							.or(CREATE_MESSAGE)
							.or(ROUTE_MESSAGE)
							.or(SEND_MESSAGE)
							.or(ROUTE_DISCREPANCY_MSG)
							.or(VIEW_CHILD_ORG_MESSAGES)
							.or(ATTACH_DOC_MESSAGE)
							.or(DELETE_DOC_MESSAGE);


	private final static BigInteger INVOICE_MGMT =
        new BigInteger("0").or(VIEW_UPLOADED_INVOICES)
        				   .or(UPLOAD_INVOICES)
						   .or(REMOVE_UPLOADED_FILES);

	private final static BigInteger CONVERSION_CENTER =
            new BigInteger("0").or(CC_GUA_CREATE_MODIFY)
            .or(CC_GUA_DELETE)
            .or(CC_GUA_ROUTE)
            .or(CC_GUA_CONVERT);

	
	

	// Defined security rights for respective action.
	private static Map<String, BigInteger> transSecurityAccess = new HashMap<String, BigInteger>();
	static {		
		transSecurityAccess.put("ATP_DELETE", APPROVAL_TO_PAY_DELETE); // Approval To Pay
		transSecurityAccess.put("ATP_ROUTE", APPROVAL_TO_PAY_ROUTE);
		transSecurityAccess.put("ATP_CREATE_MODIFY", APPROVAL_TO_PAY_CREATE_MODIFY);
		transSecurityAccess.put("ATP_AUTHORIZE", APPROVAL_TO_PAY_AUTHORIZE);
		transSecurityAccess.put("ATP_SENDFORAUTH", APPROVAL_TO_PAY_CHECKER);
		transSecurityAccess.put("ATP_SENDFORREPAIR", APPROVAL_TO_PAY_REPAIR);
		transSecurityAccess.put("AIR_DELETE", AIR_WAYBILL_DELETE); // Air Waybill
		transSecurityAccess.put("AIR_ROUTE", AIR_WAYBILL_ROUTE);
		transSecurityAccess.put("AIR_CREATE_MODIFY", AIR_WAYBILL_CREATE_MODIFY);
		transSecurityAccess.put("AIR_AUTHORIZE", AIR_WAYBILL_AUTHORIZE);
		transSecurityAccess.put("AIR_SENDFORAUTH", AIR_WAYBILL_CHECKER);
		transSecurityAccess.put("AIR_SENDFORREPAIR", AIR_WAYBILL_REPAIR);
		transSecurityAccess.put("LRQ_DELETE", LOAN_RQST_DELETE); // Loan Request
		transSecurityAccess.put("LRQ_ROUTE", LOAN_RQST_ROUTE);
		transSecurityAccess.put("LRQ_CREATE_MODIFY", LOAN_RQST_CREATE_MODIFY);
		transSecurityAccess.put("LRQ_AUTHORIZE", LOAN_RQST_AUTHORIZE);
		transSecurityAccess.put("LRQ_SENDFORAUTH", LOAN_RQST_CHECKER);
		transSecurityAccess.put("LRQ_SENDFORREPAIR", LOAN_RQST_REPAIR);
		transSecurityAccess.put("RQA_DELETE", REQUEST_ADVISE_DELETE); // Request to Advice
		transSecurityAccess.put("RQA_ROUTE", REQUEST_ADVISE_ROUTE);
		transSecurityAccess.put("RQA_CREATE_MODIFY", REQUEST_ADVISE_CREATE_MODIFY);
		transSecurityAccess.put("RQA_AUTHORIZE", REQUEST_ADVISE_AUTHORIZE);
		transSecurityAccess.put("RQA_SENDFORAUTH", REQUEST_ADVISE_CHECKER);
		transSecurityAccess.put("RQA_SENDFORREPAIR", REQUEST_ADVISE_REPAIR);
		transSecurityAccess.put("EXP_OCO_DELETE", NEW_EXPORT_COLL_DELETE); // Export Collection
		transSecurityAccess.put("EXP_OCO_ROUTE", NEW_EXPORT_COLL_ROUTE);
		transSecurityAccess.put("EXP_OCO_CREATE_MODIFY", NEW_EXPORT_COLL_CREATE_MODIFY);
		transSecurityAccess.put("EXP_OCO_AUTHORIZE", NEW_EXPORT_COLL_AUTHORIZE);
		transSecurityAccess.put("EXP_OCO_SENDFORAUTH", NEW_EXPORT_COLL_CHECKER);
		transSecurityAccess.put("EXP_OCO_SENDFORREPAIR", NEW_EXPORT_COLL_REPAIR);
		transSecurityAccess.put("EXP_COL_DELETE", EXPORT_COLL_DELETE); // Direct Send Collection
		transSecurityAccess.put("EXP_COL_ROUTE", EXPORT_COLL_ROUTE);
		transSecurityAccess.put("EXP_COL_CREATE_MODIFY", EXPORT_COLL_CREATE_MODIFY);
		transSecurityAccess.put("EXP_COL_AUTHORIZE", EXPORT_COLL_AUTHORIZE);
		transSecurityAccess.put("EXP_COL_SENDFORAUTH", EXPORT_COLL_CHECKER);
		transSecurityAccess.put("EXP_COL_SENDFORREPAIR", EXPORT_COLL_REPAIR);
		transSecurityAccess.put("EXP_DLC_DELETE", EXPORT_LC_DELETE); // Export LC
		transSecurityAccess.put("EXP_DLC_ROUTE", EXPORT_LC_ROUTE);
		transSecurityAccess.put("EXP_DLC_CREATE_MODIFY", EXPORT_LC_CREATE_MODIFY);
		transSecurityAccess.put("EXP_DLC_AUTHORIZE", EXPORT_LC_AUTHORIZE);
		transSecurityAccess.put("EXP_DLC_SENDFORAUTH", EXPORT_LC_CHECKER);
		transSecurityAccess.put("EXP_DLC_SENDFORREPAIR", EXPORT_LC_REPAIR);
		transSecurityAccess.put("SLC_DELETE", SLC_DELETE); // Outgoing Standby Letter of Credit
		transSecurityAccess.put("SLC_ROUTE", SLC_ROUTE);
		transSecurityAccess.put("SLC_CREATE_MODIFY", SLC_CREATE_MODIFY);
		transSecurityAccess.put("SLC_AUTHORIZE", SLC_AUTHORIZE);
		transSecurityAccess.put("SLC_SENDFORAUTH", SLC_CHECKER);
		transSecurityAccess.put("SLC_SENDFORREPAIR", SLC_REPAIR);
		transSecurityAccess.put("GUA_DELETE", GUAR_DELETE); // Outgoing Guarantee
		transSecurityAccess.put("GUA_ROUTE", GUAR_ROUTE);
		transSecurityAccess.put("GUA_CREATE_MODIFY", GUAR_CREATE_MODIFY);
		transSecurityAccess.put("GUA_AUTHORIZE", GUAR_AUTHORIZE);
		transSecurityAccess.put("GUAR_SENDFORAUTH", GUAR_CHECKER);
		transSecurityAccess.put("GUAR_SENDFORREPAIR", GUAR_REPAIR);
		transSecurityAccess.put("IMP_DLC_DELETE", DLC_DELETE); // Import Letter of Credit
		transSecurityAccess.put("IMP_DLC_ROUTE", DLC_ROUTE);
		transSecurityAccess.put("IMP_DLC_CREATE_MODIFY", DLC_CREATE_MODIFY);
		transSecurityAccess.put("IMP_DLC_AUTHORIZE", DLC_AUTHORIZE);
		transSecurityAccess.put("IMP_DLC_SENDFORAUTH", DLC_CHECKER);
		transSecurityAccess.put("IMP_DLC_SENDFORREPAIR", DLC_REPAIR);
		transSecurityAccess.put("SHP_DELETE", SHIP_GUAR_DELETE); // Shipping Guarantee
		transSecurityAccess.put("SHP_ROUTE", SHIP_GUAR_ROUTE);
		transSecurityAccess.put("SHP_CREATE_MODIFY", SHIP_GUAR_CREATE_MODIFY);
		transSecurityAccess.put("SHP_AUTHORIZE", SHIP_GUAR_AUTHORIZE);
		transSecurityAccess.put("SHP_SENDFORAUTH", SHIP_GUAR_CHECKER);
		transSecurityAccess.put("SHP_SENDFORREPAIR", SHIP_GUAR_REPAIR);
		transSecurityAccess.put("DDI_DELETE", DDI_DELETE); // Direct Debit Instructions
		transSecurityAccess.put("DDI_ROUTE", DDI_ROUTE);
		transSecurityAccess.put("DDI_CREATE_MODIFY", DDI_CREATE_MODIFY);
		transSecurityAccess.put("DDI_AUTHORIZE", DDI_AUTHORIZE);
		transSecurityAccess.put("FTRQ_DELETE", FUNDS_XFER_DELETE); // International Payment
		transSecurityAccess.put("FTRQ_ROUTE", FUNDS_XFER_ROUTE);
		transSecurityAccess.put("FTRQ_CREATE_MODIFY", FUNDS_XFER_CREATE_MODIFY);
		transSecurityAccess.put("FTRQ_AUTHORIZE", FUNDS_XFER_AUTHORIZE);
		transSecurityAccess.put("FTRQ_SENDFORAUTH", FUNDS_XFER_CHECKER);
		transSecurityAccess.put("FTRQ_SENDFORREPAIR", FUNDS_XFER_REPAIR);
		transSecurityAccess.put("FTDP_DELETE", DOMESTIC_DELETE); // Payments
		transSecurityAccess.put("FTDP_ROUTE", DOMESTIC_ROUTE);
		transSecurityAccess.put("FTDP_CREATE_MODIFY", DOMESTIC_CREATE_MODIFY);
		transSecurityAccess.put("FTDP_AUTHORIZE", DOMESTIC_AUTHORIZE);
		transSecurityAccess.put("FTDP_SENDFORAUTH", DOMESTIC_CHECKER);
		transSecurityAccess.put("FTDP_SENDFORREPAIR", DOMESTIC_REPAIR);
		transSecurityAccess.put("FTBA_DELETE", TRANSFER_DELETE); // Transfer Between Accounts
		transSecurityAccess.put("FTBA_ROUTE", TRANSFER_ROUTE);
		transSecurityAccess.put("FTBA_CREATE_MODIFY", TRANSFER_CREATE_MODIFY);
		transSecurityAccess.put("FTBA_AUTHORIZE", TRANSFER_AUTHORIZE);
		transSecurityAccess.put("FTBA_SENDFORAUTH", TRANSFER_CHECKER);
		transSecurityAccess.put("FTBA_SENDFORREPAIR", TRANSFER_REPAIR);
		transSecurityAccess.put("DCR_DELETE", DISCREPANCY_DELETE); // Discrepancy or ATP Response
		transSecurityAccess.put("DCR_ROUTE", DISCREPANCY_ROUTE);
		transSecurityAccess.put("DCR_CREATE_MODIFY", DISCREPANCY_CREATE_MODIFY);
		transSecurityAccess.put("DCR_AUTHORIZE", DISCREPANCY_AUTHORIZE);
		transSecurityAccess.put("DCR_SENDFORAUTH", DISCREPANCY_CHECKER);
		transSecurityAccess.put("DCR_SENDFORREPAIR", DISCREPANCY_REPAIR);
		transSecurityAccess.put("NOTIFICATION_DELETE", DELETE_NOTIFICATION);
		transSecurityAccess.put("MAIL_MESSAGE_DELETE", DELETE_MESSAGE);
		transSecurityAccess.put("MAIL_MESSAGE_CREATE", CREATE_MESSAGE);
		transSecurityAccess.put("MAIL_MESSAGE_ROUTE", ROUTE_MESSAGE);
		transSecurityAccess.put("MAIL_MESSAGE_SENDTOBANK", SEND_MESSAGE);
		transSecurityAccess.put("DISCREPANCY_DELETE", DELETE_DISCREPANCY_MSG);
		transSecurityAccess.put("DISCREPANCY_ROUTE", ROUTE_DISCREPANCY_MSG);
		transSecurityAccess.put("PRE_DEBIT_DELETE", DELETE_PREDEBIT_FUNDING_NOTIFICATION);
	}
	
	private static Map<String, BigInteger> refdataSecurityAccess = new HashMap<String, BigInteger>();
	static {
		refdataSecurityAccess.put("Party", SecurityAccess.MAINTAIN_PARTIES);
		refdataSecurityAccess.put("Phrase", SecurityAccess.MAINTAIN_PHRASE);
		refdataSecurityAccess.put("InvoiceDefinition", SecurityAccess.MAINTAIN_INVOICE_DEFINITION);
		refdataSecurityAccess.put("ThresholdGroup", SecurityAccess.MAINTAIN_THRESH_GRP);
		refdataSecurityAccess.put("PanelAuthorizationGroup", SecurityAccess.MAINTAIN_PANEL_AUTH_GRP);
		refdataSecurityAccess.put("ArMatchingRule", SecurityAccess.MAINTAIN_RECEIVABLES_MATCH_RULES);
		refdataSecurityAccess.put("WorkGroup", SecurityAccess.MAINTAIN_WORK_GROUPS);
		refdataSecurityAccess.put("PaymentDefinitions", SecurityAccess.MAINTAIN_PAYMENT_FILE_DEF);
		refdataSecurityAccess.put("NotificationRuleTemplate", SecurityAccess.MAINTAIN_NOTIF_RULE_TEMPLATES);//Rel9.5 CR 927B
		refdataSecurityAccess.put("BankGroupRestrictRule", SecurityAccess.MAINTAIN_BANK_GROUP_RESTRICT_RULES); /*pgedupudi - Rel9.3 CR976A 03/09/2015*/
		refdataSecurityAccess.put("GenericMessageCategory", SecurityAccess.MAINTAIN_GM_CAT_GRP);
		refdataSecurityAccess.put("PaymentMethodValidation", SecurityAccess.MAINTAIN_PAYMENT_METH_VAL);
		refdataSecurityAccess.put("PaymentTemplateGroup", SecurityAccess.MAINTAIN_PAYMENT_TEMPLATE_GRP);
		refdataSecurityAccess.put("CrossRateCalcRule", SecurityAccess.MAINTAIN_CROSS_RATE_RULE);
		refdataSecurityAccess.put("BankBranchRule", SecurityAccess.MAINTAIN_BANK_BRANCH_RULES);
		refdataSecurityAccess.put("IMP_DLCLCCreationRule", SecurityAccess.MAINTAIN_LC_CREATE_RULES);
		refdataSecurityAccess.put("ATPLCCreationRule", SecurityAccess.MAINTAIN_ATP_CREATE_RULES);
		refdataSecurityAccess.put("LRQInvOnlyCreateRule", SecurityAccess.MAINTAIN_LR_CREATE_RULES);
		refdataSecurityAccess.put("ATPInvOnlyCreateRule", SecurityAccess.MAINTAIN_ATP_INV_CREATE_RULES);
		refdataSecurityAccess.put("FXRate", SecurityAccess.MAINTAIN_FX_RATE);
	}
	
	private static final List<String> beanWithDiffCorpAttr = Arrays.asList("ThresholdGroup", "ArMatchingRule", "PanelAuthorizationGroup", "WorkGroup");
	private static final List<String> beanToSkipSameOrqCheck = Arrays.asList("BankBranchRule", "PaymentMethodValidation");

	
/**
 * SecurityAccess constructor.
 */
public SecurityAccess() {
	super();
}


/**
 * Determines whether user has a specific security access right.
 *
 * @return boolean - true if has access, false otherwise
 * @param securityRights String - the security rights of the user
 * @param requestedAccess long - one of the SecurityAccess constants
 */
public static boolean hasRights(String securityRights, BigInteger requestedAccess) {

	// For the VIEW_OR_MAINTAIN rights, check for MAINTAIN or VIEW individually.
	// (We can't do the normal logic)
	if (requestedAccess.equals(VIEW_OR_MAINTAIN_ADM_SEC_PROF)) {
		return hasEitherRight(securityRights, MAINTAIN_ADM_SEC_PROF, VIEW_ADM_SEC_PROF);
	}

	if (requestedAccess.equals(VIEW_OR_MAINTAIN_ADM_USERS)) {
		return hasEitherRight(securityRights, MAINTAIN_ADM_USERS, VIEW_ADM_USERS);
	}

	if (requestedAccess.equals(VIEW_OR_MAINTAIN_STANDARD_REPORT)) {
		return hasEitherRight(securityRights, MAINTAIN_STANDARD_REPORT, VIEW_STANDARD_REPORT);
	}

	if (requestedAccess.equals(VIEW_OR_MAINTAIN_CUSTOM_REPORT)) {
		return hasEitherRight(securityRights, MAINTAIN_CUSTOM_REPORT, VIEW_CUSTOM_REPORT);
	}


	if (requestedAccess.equals(VIEW_OR_MAINTAIN_FX_RATE)) {
		return hasEitherRight(securityRights, MAINTAIN_FX_RATE, VIEW_FX_RATE);
	}

	/*pgedupudi - Rel9.3 CR976A 03/09/2015*/
	if (requestedAccess.equals(VIEW_OR_MAINTAIN_BANK_GROUP_RESTRICT_RULES)) {
		return hasEitherRight(securityRights, MAINTAIN_BANK_GROUP_RESTRICT_RULES, VIEW_BANK_GROUP_RESTRICT_RULES);
	}
	

	if (requestedAccess.equals(VIEW_OR_MAINTAIN_PARTIES)) {
		return hasEitherRight(securityRights, MAINTAIN_PARTIES, VIEW_PARTIES);
	}

	if (requestedAccess.equals(VIEW_OR_MAINTAIN_PHRASE)) {
		return hasEitherRight(securityRights, MAINTAIN_PHRASE, VIEW_PHRASE);
	}

	if (requestedAccess.equals(VIEW_OR_MAINTAIN_SEC_PROF)) {
		return hasEitherRight(securityRights, MAINTAIN_SEC_PROF, VIEW_SEC_PROF);
	}

	if (requestedAccess.equals(VIEW_OR_MAINTAIN_TEMPLATE)) {
		return hasEitherRight(securityRights, MAINTAIN_TEMPLATE, VIEW_TEMPLATE);
	}
	//Rel9.5 CR 927B
	if (requestedAccess.equals(VIEW_OR_MAINTAIN_NOTIFICATION_RULE)) {
		return hasEitherRight(securityRights, VIEW_NOTIFICATION_RULE, MAINTAIN_NOTIFICATION_RULE);
	}
	
	if (requestedAccess.equals(VIEW_OR_MAINTAIN_NOTIF_RULE_TEMPLATES)) {
		return hasEitherRight(securityRights, MAINTAIN_NOTIF_RULE_TEMPLATES, VIEW_NOTIF_RULE_TEMPLATES);
	}

	if (requestedAccess.equals(VIEW_OR_MAINTAIN_THRESH_GRP)) {
		return hasEitherRight(securityRights, MAINTAIN_THRESH_GRP, VIEW_THRESH_GRP);
	}

	if (requestedAccess.equals(VIEW_OR_MAINTAIN_USERS)) {
		return hasEitherRight(securityRights, MAINTAIN_USERS, VIEW_USERS);
	}


	if (requestedAccess.equals(VIEW_OR_MAINTAIN_ANNOUNCEMENT)) {
		return hasEitherRight(securityRights, MAINTAIN_ANNOUNCEMENT, VIEW_ANNOUNCEMENT);
	}

	if (requestedAccess.equals(VIEW_OR_MAINTAIN_LOCKED_OUT_USERS)) {
		return hasEitherRight(securityRights, MAINTAIN_LOCKED_OUT_USERS, VIEW_LOCKED_OUT_USERS);
	}

	if (requestedAccess.equals(VIEW_OR_MAINTAIN_LC_CREATE_RULES)) {
		return hasEitherRight(securityRights, MAINTAIN_LC_CREATE_RULES, VIEW_LC_CREATE_RULES);
	}


	if (requestedAccess.equals(VIEW_OR_MAINTAIN_ATP_CREATE_RULES)) {
		return hasEitherRight(securityRights, MAINTAIN_ATP_CREATE_RULES, VIEW_ATP_CREATE_RULES);
	}
      
	if (requestedAccess.equals(VIEW_OR_MAINTAIN_RECEIVABLES_MATCH_RULES)) {
		return hasEitherRight(securityRights, MAINTAIN_RECEIVABLES_MATCH_RULES, VIEW_RECEIVABLES_MATCH_RULES);
	}
	

	if (requestedAccess.equals(VIEW_OR_MAINTAIN_PO_UPLOAD_DEFN)) {
		return hasEitherRight(securityRights, MAINTAIN_PO_UPLOAD_DEFN, VIEW_PO_UPLOAD_DEFN);
	}

	if (requestedAccess.equals(VIEW_OR_MAINTAIN_WORK_GROUPS)) {
		return hasEitherRight(securityRights, MAINTAIN_WORK_GROUPS, VIEW_WORK_GROUPS);
	}


	if (requestedAccess.equals(VIEW_OR_MAINTAIN_PANEL_AUTH_GRP)) {
		return hasEitherRight(securityRights, MAINTAIN_PANEL_AUTH_GRP, VIEW_PANEL_AUTH_GRP);
	}
    
    if (requestedAccess.equals(VIEW_OR_MAINTAIN_BANK_BRANCH_RULES)) {
        return hasEitherRight(securityRights, MAINTAIN_BANK_BRANCH_RULES, VIEW_BANK_BRANCH_RULES);
    }

    if (requestedAccess.equals(VIEW_OR_MAINTAIN_PAYMENT_METH_VAL)) {
        return hasEitherRight(securityRights, MAINTAIN_PAYMENT_METH_VAL, VIEW_PAYMENT_METH_VAL);
    }
    
    if (requestedAccess.equals(VIEW_OR_MAINTAIN_CALENDAR)) {
        return hasEitherRight(securityRights, MAINTAIN_CALENDAR, VIEW_CALENDAR);
    }

    if (requestedAccess.equals(VIEW_OR_MAINTAIN_CURRENCY_CALENDAR_RULES)) {
        return hasEitherRight(securityRights, MAINTAIN_CURRENCY_CALENDAR_RULES, VIEW_CURRENCY_CALENDAR_RULES);
    }
    
	if (requestedAccess.equals(VIEW_OR_MAINTAIN_PAYMENT_TEMPLATE_GRP)) {
		return hasEitherRight(securityRights, MAINTAIN_PAYMENT_TEMPLATE_GRP, VIEW_PAYMENT_TEMPLATE_GRP);
	}
   
    if (requestedAccess.equals(VIEW_OR_MAINTAIN_GM_CAT_GRP)) {
		return hasEitherRight(securityRights, MAINTAIN_GM_CAT_GRP, VIEW_GM_CAT_GRP);
	}
	
    if (requestedAccess.equals(VIEW_OR_MAINTAIN_CROSS_RATE_RULE)) {
		return hasEitherRight(securityRights, MAINTAIN_CROSS_RATE_RULE, VIEW_CROSS_RATE_RULE);
	}
	
    if (requestedAccess.equals(VIEW_OR_MAINTAIN_INTEREST_RATE)) {
		return hasEitherRight(securityRights, MAINTAIN_INTEREST_RATE, VIEW_INTEREST_RATE);
	}
    
    if (requestedAccess.equals(VIEW_OR_MAINTAIN_INVOICE_DEFINITION)) {
		return hasEitherRight(securityRights, MAINTAIN_INVOICE_DEFINITION, VIEW_INVOICE_DEFINITION);
	}

    if (requestedAccess.equals(VIEW_OR_MAINTAIN_RECEIVABLES_TRADING_RULES)) {
		return hasEitherRight(securityRights, MAINTAIN_RECEIVABLES_TRADING_RULES, VIEW_RECEIVABLES_TRADING_RULES);
	}

    
    if (requestedAccess.equals(VIEW_OR_MAINTAIN_UPLOADED_INVOICES)) {
		return hasEitherRight(securityRights, VIEW_UPLOADED_INVOICES, MAINTAIN_UPLOADED_INVOICES);
	}
    
    if (requestedAccess.equals(VIEW_OR_MAINTAIN_ERP_GL_CODES)) {
		return hasEitherRight(securityRights, VIEW_ERP_GL_CODES, MAINTAIN_ERP_GL_CODES);
	}
    if (requestedAccess.equals(VIEW_OR_MAINTAIN_DISCOUNT_CODES)) {
		return hasEitherRight(securityRights, VIEW_DISCOUNT_CODES, MAINTAIN_DISCOUNT_CODES);
	}
   

    if (requestedAccess.equals(VIEW_OR_MAINTAIN_LR_CREATE_RULES)) {
		return hasEitherRight(securityRights, MAINTAIN_LR_CREATE_RULES, VIEW_LR_CREATE_RULES);
	}

    if (requestedAccess.equals(VIEW_OR_MAINTAIN_ATP_INV_CREATE_RULES)) {
		return hasEitherRight(securityRights, MAINTAIN_ATP_INV_CREATE_RULES, VIEW_ATP_INV_CREATE_RULES);
	}
 
    

    
    if (requestedAccess.equals(VIEW_OR_MAINTAIN_EXTERNAL_BANKS)) {
		return hasEitherRight(securityRights, MAINTAIN_EXTERNAL_BANK, VIEW_EXTERNAL_BANK);
	}

    

    if (requestedAccess.equals(VIEW_OR_MAINTAIN_ORG_PROFILE)) {
		return hasEitherRight(securityRights, MAINTAIN_ORG_PROFILE, VIEW_ORG_PROFILE);
	}

	if (requestedAccess.equals(VIEW_OR_MAINTAIN_PAYMENT_FILE_DEF)) {
		return hasEitherRight(securityRights, MAINTAIN_PAYMENT_FILE_DEF, VIEW_PAYMENT_FILE_DEF);
	}

	BigInteger userRights;
	try {
		userRights = new BigInteger(securityRights);
	} catch (Exception e) {
		// if securityRights is not a good long number, userRights remains 0.
		userRights = ZERO;
	}


	// If the access is for any of a combination of security rights,
	// then the resulting AND ing of the security rights and requested access
	// should just be greater than 0, ie. that the user has at least one of the rights
	// requested.  This apples to VIEW_INSTRUMENT_CREATE_OR_MODIFY,
	// INSTRUMENT_AUTHORIZE, INSTRUMENT_DELETE and INSTRUMENT_ROUTE
	if (requestedAccess.equals(VIEW_INSTRUMENT_CREATE_OR_MODIFY) ||
	    requestedAccess.equals(INSTRUMENT_DELETE) ||
	    requestedAccess.equals(INSTRUMENT_ROUTE) ||
	    requestedAccess.equals(INSTRUMENT_AUTHORIZE) )
	return userRights.and(requestedAccess).compareTo(ZERO) > 0;



	// If user wants to access any instrument maint access, he must also
	// have ACCESS_INSTRUMENTS_AREA access.  ('OR'ing the two values produces
	// a value where both flags are turned on.)
	if ((requestedAccess.and(INSTRUMENT_MAINT)).compareTo(ZERO) > 0) {
		requestedAccess.or(ACCESS_INSTRUMENTS_AREA);
	}


	// If user wants to access any report maint access, he must also
	// have ACCESS_REPORT_AREA access.
	if ((requestedAccess.and(REPORT_MAINT)).compareTo(ZERO) > 0) {
		requestedAccess.or(ACCESS_REPORT_AREA);
	}


	// If user wants to access any ref data maint access, he must also
	// have ACCESS_REFDATA_AREA access.
	if ((requestedAccess.and(REFDATA_MAINT)).compareTo(ZERO) > 0) {
		requestedAccess.or(ACCESS_REFDATA_AREA);
	}

	// If user wants to access any message maint access, he must also
	// have ACCESS_MESSAGES_AREA access.
	if ((requestedAccess.and(MESSAGE_MAINT)).compareTo(ZERO) > 0) {
		requestedAccess.or(ACCESS_MESSAGES_AREA);
	}

	// ACCESS_ADM_REFDATA_AREA required if user wants to maintain adm. sec. profiles
	if ((requestedAccess.and(MAINTAIN_ADM_SEC_PROF)).compareTo(ZERO) > 0) {
		requestedAccess.or(ACCESS_ADM_REFDATA_AREA);
	}

	// ACCESS_ADM_REFDATA_AREA required if user wants to maintain adm. users
	if ((requestedAccess.and(MAINTAIN_ADM_USERS)).compareTo(ZERO) > 0) {
		requestedAccess.or(ACCESS_ADM_REFDATA_AREA);
	}
	if ((requestedAccess.and(INVOICE_MGMT)).compareTo(ZERO) > 0) {
		requestedAccess.or(ACCESS_INVOICE_MGT_AREA);
	}


	if ((requestedAccess.and(CONVERSION_CENTER)).compareTo(ZERO) > 0) {
		requestedAccess.or(ACCESS_CONVERSION_CENTER_AREA);
	}


	// Do a bitwise comparision of the user's rights with the requested
	// access to see if the user has it.  If so, true is returned.
	return (userRights.and(requestedAccess).equals(requestedAccess));

}






/**
 * Determines whether user has access to either (or both) of two
 * security rights.
 *
 * @return boolean - true if has access, false otherwise
 * @param securityRights String - the security rights of the user
 * @param requestedAccess1 long - one of the SecurityAccess constants
 * @param requestedAccess2 long - one of the SecurityAccess constants
 */
public static boolean hasEitherRight(String securityRights, BigInteger requestedAccess1, BigInteger requestedAccess2) {

	// developer note: caution has to be taken that a circular call does not
	// occur in calling this, since hasRights calls hasEitherRight.

	// Do a bitwise comparision of the user's rights with the requested
	// access to see if the user has it.  This is an 'either/or' check.
	// This calls hasRights since the requested access may have some
	// certain validation that is performed in hasRights.
	if (hasRights(securityRights, requestedAccess1)) return true;
	if (hasRights(securityRights, requestedAccess2)) return true;
	return false;

}

/**
 * Given the security rights and a instrument type, determines if the
 * user has authorise access for that instrument type.
 *
 * @return boolean - true if has access, false otherwise
 * @param securityRights java.lang.String
 * @param instrumentType java.lang.String
 * @param transactionType java.lang.String
 */
public static boolean canAuthorizeInstrument(String securityRights, String instrumentType,
					     String transactionType) {
	BigInteger userRights;
	try {
		userRights = new BigInteger(securityRights);
	} catch (Exception e) {
		// if securityRights is not a good number, userRights remains 0.
		userRights = ZERO;
	}

	// Do a bitwise comparision of the user's rights with the requested
	// access to see if the user has it.  If so, true is returned.
	if ((userRights.and(ACCESS_INSTRUMENTS_AREA).equals(ACCESS_INSTRUMENTS_AREA))) {

		// this check should always be the first because it overrides every instrument
		// type check.
               
		if (transactionType != null &&
			(transactionType.equals(TransactionType.DISCREPANCY)||transactionType.equals(TransactionType.APPROVAL_TO_PAY_RESPONSE))){
			return userRights.and(DISCREPANCY_AUTHORIZE).equals(DISCREPANCY_AUTHORIZE);
		}
		
		if ( TransactionType.SIM.equals(transactionType) || TransactionType.SIR.equals(transactionType) ) {
			return userRights.and(SIT_AUTHORIZE).equals(SIT_AUTHORIZE);
		}

		if (instrumentType.equals(InstrumentType.AIR_WAYBILL)) {
			return userRights.and(AIR_WAYBILL_AUTHORIZE).equals(AIR_WAYBILL_AUTHORIZE);
		}
		if (instrumentType.equals(InstrumentType.EXPORT_COL)) {
			return userRights.and(EXPORT_COLL_AUTHORIZE).equals(EXPORT_COLL_AUTHORIZE);
		}
		if (instrumentType.equals(InstrumentType.NEW_EXPORT_COL)) { 
			return userRights.and(NEW_EXPORT_COLL_AUTHORIZE).equals(NEW_EXPORT_COLL_AUTHORIZE);
		}
		if (instrumentType.equals(InstrumentType.GUARANTEE)) {
			return userRights.and(GUAR_AUTHORIZE).equals(GUAR_AUTHORIZE);
		}
		if (instrumentType.equals(InstrumentType.IMPORT_DLC)) {
			return userRights.and(DLC_AUTHORIZE).equals(DLC_AUTHORIZE);
		}
		if (instrumentType.equals(InstrumentType.STANDBY_LC)) {
			return userRights.and(SLC_AUTHORIZE).equals(SLC_AUTHORIZE);
		}
		if (instrumentType.equals(InstrumentType.EXPORT_DLC)) {
			return userRights.and(EXPORT_LC_AUTHORIZE).equals(EXPORT_LC_AUTHORIZE);
		}
		if (instrumentType.equals(InstrumentType.SHIP_GUAR)) {
			return userRights.and(SHIP_GUAR_AUTHORIZE).equals(SHIP_GUAR_AUTHORIZE);
		}
		if (instrumentType.equals(InstrumentType.LOAN_RQST)) {
			return userRights.and(LOAN_RQST_AUTHORIZE).equals(LOAN_RQST_AUTHORIZE);
		}
		if (instrumentType.equals(InstrumentType.FUNDS_XFER)) {
			return userRights.and(FUNDS_XFER_AUTHORIZE).equals(FUNDS_XFER_AUTHORIZE);
		}
		if (instrumentType.equals(InstrumentType.REQUEST_ADVISE)) {
			return userRights.and(REQUEST_ADVISE_AUTHORIZE).equals(REQUEST_ADVISE_AUTHORIZE);
		}

		if (instrumentType.equals(InstrumentType.APPROVAL_TO_PAY)) {
			return userRights.and(APPROVAL_TO_PAY_AUTHORIZE).equals(APPROVAL_TO_PAY_AUTHORIZE);
		}



		if (instrumentType.equals(InstrumentType.XFER_BET_ACCTS)) {
			return userRights.and(TRANSFER_AUTHORIZE).equals(TRANSFER_AUTHORIZE);
		}
		if (instrumentType.equals(InstrumentType.DOMESTIC_PMT)) {
			return userRights.and(DOMESTIC_AUTHORIZE).equals(DOMESTIC_AUTHORIZE);
		}


	}


	if ((userRights.and(ACCESS_DIRECTDEBIT_AREA).equals(ACCESS_DIRECTDEBIT_AREA))) {

		if (instrumentType.equals(InstrumentType.DIRECT_DEBIT_INSTRUCTION)) {
			return userRights.and(DDI_AUTHORIZE).equals(DDI_AUTHORIZE);
		}
	}


	return false;
}

/**
 * Given the security rights and a instrument type, determines if the
 * user has create/modify access for that instrument type.
 *
 * @return boolean - true if has access, false otherwise
 * @param securityRights java.lang.String
 * @param instrumentType java.lang.String
 * @param transactionType java.lang.String
 */
public static boolean canCreateModInstrument(String securityRights, String instrumentType,
					     String transactionType) {
	
	BigInteger userRights;
	try {
		userRights = new BigInteger(securityRights);
	} catch (Exception e) {
		// if securityRights is not a good number, userRights remains 0.
		userRights = ZERO;
	}

	// Do a bitwise comparision of the user's rights with the requested
	// access to see if the user has it.  If so, true is returned.
	if ((userRights.and(ACCESS_INSTRUMENTS_AREA).equals(ACCESS_INSTRUMENTS_AREA))) {
		
		// this check should always be the first because it overrides every instrument
		// type check.
               
		if (transactionType != null &&
			(transactionType.equals(TransactionType.DISCREPANCY)||transactionType.equals(TransactionType.APPROVAL_TO_PAY_RESPONSE))) {
			return userRights.and(DISCREPANCY_CREATE_MODIFY).equals(DISCREPANCY_CREATE_MODIFY);
		}
		if ( TransactionType.SIM.equals(transactionType) || TransactionType.SIR.equals(transactionType) ) {
			return userRights.and(SIT_CREATE_MODIFY).equals(SIT_CREATE_MODIFY);
		}
		if (instrumentType.equals(InstrumentType.AIR_WAYBILL)) {
			return userRights.and(AIR_WAYBILL_CREATE_MODIFY).equals(AIR_WAYBILL_CREATE_MODIFY);
		}
		if (instrumentType.equals(InstrumentType.EXPORT_COL)) {
			return userRights.and(EXPORT_COLL_CREATE_MODIFY).equals(EXPORT_COLL_CREATE_MODIFY);
		}
		if (instrumentType.equals(InstrumentType.NEW_EXPORT_COL)) { 
			return userRights.and(NEW_EXPORT_COLL_CREATE_MODIFY).equals(NEW_EXPORT_COLL_CREATE_MODIFY);
		}
		if (instrumentType.equals(InstrumentType.GUARANTEE)) {
			return userRights.and(GUAR_CREATE_MODIFY).equals(GUAR_CREATE_MODIFY);
		}
		if (instrumentType.equals(InstrumentType.IMPORT_DLC)) {
			return userRights.and(DLC_CREATE_MODIFY).equals(DLC_CREATE_MODIFY);
		}
		if (instrumentType.equals(InstrumentType.STANDBY_LC)) {
			return userRights.and(SLC_CREATE_MODIFY).equals(SLC_CREATE_MODIFY);
		}
		if (instrumentType.equals(InstrumentType.EXPORT_DLC)) {
			return userRights.and(EXPORT_LC_CREATE_MODIFY).equals(EXPORT_LC_CREATE_MODIFY);
		}
		if (instrumentType.equals(InstrumentType.SHIP_GUAR)) {
			return userRights.and(SHIP_GUAR_CREATE_MODIFY).equals(SHIP_GUAR_CREATE_MODIFY);
		}
		if (instrumentType.equals(InstrumentType.LOAN_RQST)) {
			return userRights.and(LOAN_RQST_CREATE_MODIFY).equals(LOAN_RQST_CREATE_MODIFY);
		}
		if (instrumentType.equals(InstrumentType.FUNDS_XFER)) {
			return userRights.and(FUNDS_XFER_CREATE_MODIFY).equals(FUNDS_XFER_CREATE_MODIFY);
		}
		if (instrumentType.equals(InstrumentType.REQUEST_ADVISE)) {
			return userRights.and(REQUEST_ADVISE_CREATE_MODIFY).equals(REQUEST_ADVISE_CREATE_MODIFY);
		}

		if (instrumentType.equals(InstrumentType.APPROVAL_TO_PAY)) {
			return userRights.and(APPROVAL_TO_PAY_CREATE_MODIFY).equals(APPROVAL_TO_PAY_CREATE_MODIFY);
		}

		if (instrumentType.equals(InstrumentType.XFER_BET_ACCTS)) {
			return userRights.and(TRANSFER_CREATE_MODIFY).equals(TRANSFER_CREATE_MODIFY);
		}
		if (instrumentType.equals(InstrumentType.DOMESTIC_PMT)) {
			return userRights.and(DOMESTIC_CREATE_MODIFY).equals(DOMESTIC_CREATE_MODIFY);
		}
		if ( instrumentType.equals(InstrumentType.IMPORT_COL) || instrumentType.equals(InstrumentType.DOCUMENTARY_BA) || 
				instrumentType.equals(InstrumentType.DEFERRED_PAY) ) {
			return userRights.and(SIT_CREATE_MODIFY).equals(SIT_CREATE_MODIFY);
		}

	}


	if ((userRights.and(ACCESS_DIRECTDEBIT_AREA).equals(ACCESS_DIRECTDEBIT_AREA))) {

		if (instrumentType.equals(InstrumentType.DIRECT_DEBIT_INSTRUCTION)) {
			return userRights.and(DDI_CREATE_MODIFY).equals(DDI_CREATE_MODIFY);
		}
	}
	

	
	return false;
}

/**
 * Given the security rights and a instrument type, determines if the
 * user has delete access for that instrument type.
 *
 * @return boolean - true if has access, false otherwise
 * @param securityRights java.lang.String
 * @param instrumentType java.lang.String
 * @param transactionType java.lang.String
 */
public static boolean canDeleteInstrument(String securityRights, String instrumentType,
					     String transactionType) {
	BigInteger userRights;
	try {
		userRights = new BigInteger(securityRights);
	} catch (Exception e) {
		// if securityRights is not a good number, userRights remains 0.
		userRights = ZERO;
	}

	// Do a bitwise comparision of the user's rights with the requested
	// access to see if the user has it.  If so, true is returned.
	if ((userRights.and(ACCESS_INSTRUMENTS_AREA).equals(ACCESS_INSTRUMENTS_AREA))) {

		// this check should always be the first because it overrides every instrument
		// type check.
              
		if (transactionType != null &&
			(transactionType.equals(TransactionType.DISCREPANCY)||transactionType.equals(TransactionType.APPROVAL_TO_PAY_RESPONSE))) {
			return userRights.and(DISCREPANCY_DELETE).equals(DISCREPANCY_DELETE);
		}
		if ( TransactionType.SIM.equals(transactionType) || TransactionType.SIR.equals(transactionType) ) {
			return userRights.and(SIT_DELETE).equals(SIT_DELETE);
		}
		if (instrumentType.equals(InstrumentType.AIR_WAYBILL)) {
			return userRights.and(AIR_WAYBILL_DELETE).equals(AIR_WAYBILL_DELETE);
		}
		if (instrumentType.equals(InstrumentType.EXPORT_COL)) {
			return userRights.and(EXPORT_COLL_DELETE).equals(EXPORT_COLL_DELETE);
		}
		if (instrumentType.equals(InstrumentType.NEW_EXPORT_COL)) { 
			return userRights.and(NEW_EXPORT_COLL_DELETE).equals(NEW_EXPORT_COLL_DELETE);
		}
		if (instrumentType.equals(InstrumentType.GUARANTEE)) {
			return userRights.and(GUAR_DELETE).equals(GUAR_DELETE);
		}
		if (instrumentType.equals(InstrumentType.IMPORT_DLC)) {
			return userRights.and(DLC_DELETE).equals(DLC_DELETE);
		}
		if (instrumentType.equals(InstrumentType.STANDBY_LC)) {
			return userRights.and(SLC_DELETE).equals(SLC_DELETE);
		}
		if (instrumentType.equals(InstrumentType.EXPORT_DLC)) {
			return userRights.and(EXPORT_LC_DELETE).equals(EXPORT_LC_DELETE);
		}
		if (instrumentType.equals(InstrumentType.SHIP_GUAR)) {
			return userRights.and(SHIP_GUAR_DELETE).equals(SHIP_GUAR_DELETE);
		}
		if (instrumentType.equals(InstrumentType.LOAN_RQST)) {
			return userRights.and(LOAN_RQST_DELETE).equals(LOAN_RQST_DELETE);
		}
		if (instrumentType.equals(InstrumentType.FUNDS_XFER)) {
			return userRights.and(FUNDS_XFER_DELETE).equals(FUNDS_XFER_DELETE);
		}
		if (instrumentType.equals(InstrumentType.REQUEST_ADVISE)) {
			return userRights.and(REQUEST_ADVISE_DELETE).equals(REQUEST_ADVISE_DELETE);
		}

		if (instrumentType.equals(InstrumentType.APPROVAL_TO_PAY)) {
			return userRights.and(APPROVAL_TO_PAY_DELETE).equals(APPROVAL_TO_PAY_DELETE);
		}

		if (instrumentType.equals(InstrumentType.XFER_BET_ACCTS)) {
			return userRights.and(TRANSFER_DELETE).equals(TRANSFER_DELETE);
		}
		if (instrumentType.equals(InstrumentType.DOMESTIC_PMT)) {
			return userRights.and(DOMESTIC_DELETE).equals(DOMESTIC_DELETE);
		}

	}


	if ((userRights.and(ACCESS_DIRECTDEBIT_AREA).equals(ACCESS_DIRECTDEBIT_AREA))) {

		if (instrumentType.equals(InstrumentType.DIRECT_DEBIT_INSTRUCTION)) {
			return userRights.and(DDI_DELETE).equals(DDI_DELETE);
		}
	}


	return false;
}

/**
 * Given the security rights and a instrument type, determines if the
 * user has route access for that instrument type.
 *
 * @return boolean - true if has access, false otherwise
 * @param securityRights java.lang.String
 * @param instrumentType java.lang.String
 * @param transactionType java.lang.String
 */
public static boolean canRouteInstrument(String securityRights, String instrumentType,
					     String transactionType) {
	BigInteger userRights;
	try {
		userRights = new BigInteger(securityRights);
	} catch (Exception e) {
		// if securityRights is not a good number, userRights remains 0.
		userRights = ZERO;
	}

	// Do a bitwise comparision of the user's rights with the requested
	// access to see if the user has it.  If so, true is returned.
	if ((userRights.and(ACCESS_INSTRUMENTS_AREA).equals(ACCESS_INSTRUMENTS_AREA))) {

		// this check should always be the first because it overrides every instrument
		// type check.
                
		if (transactionType != null &&
			(transactionType.equals(TransactionType.DISCREPANCY)||transactionType.equals(TransactionType.APPROVAL_TO_PAY_RESPONSE))) {
			return userRights.and(DISCREPANCY_ROUTE).equals(DISCREPANCY_ROUTE);
		}
		if ( TransactionType.SIM.equals(transactionType) || TransactionType.SIR.equals(transactionType) ) {
			return userRights.and(SIT_ROUTE).equals(SIT_ROUTE);
		}
		if (instrumentType.equals(InstrumentType.AIR_WAYBILL)) {
			return userRights.and(AIR_WAYBILL_ROUTE).equals(AIR_WAYBILL_ROUTE);
		}
		if (instrumentType.equals(InstrumentType.EXPORT_COL)) {
			return userRights.and(EXPORT_COLL_ROUTE).equals(EXPORT_COLL_ROUTE);
		}
		if (instrumentType.equals(InstrumentType.NEW_EXPORT_COL)) { 
			return userRights.and(NEW_EXPORT_COLL_ROUTE).equals(NEW_EXPORT_COLL_ROUTE);
		}
		if (instrumentType.equals(InstrumentType.GUARANTEE)) {
			return userRights.and(GUAR_ROUTE).equals(GUAR_ROUTE);
		}
		if (instrumentType.equals(InstrumentType.IMPORT_DLC)) {
			return userRights.and(DLC_ROUTE).equals(DLC_ROUTE);
		}
		if (instrumentType.equals(InstrumentType.STANDBY_LC)) {
			return userRights.and(SLC_ROUTE).equals(SLC_ROUTE);
		}
		if (instrumentType.equals(InstrumentType.EXPORT_DLC)) {
			return userRights.and(EXPORT_LC_ROUTE).equals(EXPORT_LC_ROUTE);
		}
		if (instrumentType.equals(InstrumentType.SHIP_GUAR)) {
			return userRights.and(SHIP_GUAR_ROUTE).equals(SHIP_GUAR_ROUTE);
		}
		if (instrumentType.equals(InstrumentType.LOAN_RQST)) {
			return userRights.and(LOAN_RQST_ROUTE).equals(LOAN_RQST_ROUTE);
		}
		if (instrumentType.equals(InstrumentType.FUNDS_XFER)) {
			return userRights.and(FUNDS_XFER_ROUTE).equals(FUNDS_XFER_ROUTE);
		}
		if (instrumentType.equals(InstrumentType.REQUEST_ADVISE)) {
			return userRights.and(REQUEST_ADVISE_ROUTE).equals(REQUEST_ADVISE_ROUTE);
		}
	        
		if (instrumentType.equals(InstrumentType.APPROVAL_TO_PAY)) {
			return userRights.and(APPROVAL_TO_PAY_ROUTE).equals(APPROVAL_TO_PAY_ROUTE);
		}

		if (instrumentType.equals(InstrumentType.XFER_BET_ACCTS)) {
			return userRights.and(TRANSFER_ROUTE).equals(TRANSFER_ROUTE);
		}
		if (instrumentType.equals(InstrumentType.DOMESTIC_PMT)) {
			return userRights.and(DOMESTIC_ROUTE).equals(DOMESTIC_ROUTE);
		}
	

	}


	if ((userRights.and(ACCESS_DIRECTDEBIT_AREA).equals(ACCESS_DIRECTDEBIT_AREA))) {

		if (instrumentType.equals(InstrumentType.DIRECT_DEBIT_INSTRUCTION)) {
			return userRights.and(DDI_ROUTE).equals(DDI_ROUTE);
		}
	}
	

	return false;
}




public static boolean canProcessPurchaseOrder(String securityRights, String instrumentType) {
	BigInteger userRights;
	try {
		userRights = new BigInteger(securityRights);
	} catch (Exception e) {
		// if securityRights is not a good number, userRights remains 0.
		userRights = ZERO;
	}

      if (instrumentType.equals(InstrumentType.APPROVAL_TO_PAY)) {
          return userRights.and(APPROVAL_TO_PAY_PROCESS_PO).equals(APPROVAL_TO_PAY_PROCESS_PO);
      }

      if (instrumentType.equals(InstrumentType.IMPORT_DLC)) {
          return userRights.and(DLC_PROCESS_PO).equals(DLC_PROCESS_PO);
      }

	return false;
}


public static boolean canProcessATPInvoice(String securityRights, String instrumentType) {
	BigInteger userRights;
	try {
		userRights = new BigInteger(securityRights);
	} catch (Exception e) {
		// if securityRights is not a good number, userRights remains 0.
		userRights = ZERO;
	}

      if (instrumentType.equals(InstrumentType.APPROVAL_TO_PAY)) {
          return userRights.and(ATP_PROCESS_INVOICES).equals(ATP_PROCESS_INVOICES);
      }

	return false;
}

public static boolean canProcessLRQInvoice(String securityRights, String instrumentType) {
	BigInteger userRights;
	try {
		userRights = new BigInteger(securityRights);
	} catch (Exception e) {
		// if securityRights is not a good number, userRights remains 0.
		userRights = ZERO;
	}

      if (instrumentType.equals(InstrumentType.LOAN_RQST)) {
          return userRights.and(LOAN_RQST_PROCESS_INVOICES).equals(LOAN_RQST_PROCESS_INVOICES);
      }

	return false;
}


/*
 * Whether the user can upload Payment file.  Only applicable for Domestic Payment
 */
public static boolean canProcessUploadBulkFile(String securityRights, String instrumentType,
					     String transactionType) {
	BigInteger userRights;
	try {
		userRights = new BigInteger(securityRights);
	} catch (Exception e) {
		// if securityRights is not a good number, userRights remains 0.
		userRights = ZERO;
	}


	if ((userRights.and(ACCESS_INSTRUMENTS_AREA).equals(ACCESS_INSTRUMENTS_AREA))) {
		if (instrumentType.equals(InstrumentType.DOMESTIC_PMT)) {
				return userRights.and(DOMESTIC_UPLOAD_BULK_FILE).equals(DOMESTIC_UPLOAD_BULK_FILE);
		}
	}
	return false;
}

/*
 * Whether the user can upload Direct Debit file.  Only applicable for Direct Debit.
 */
public static boolean canProcessUploadDirectDebitFile(String securityRights, String instrumentType) {
	if (!InstrumentType.DIRECT_DEBIT_INSTRUCTION.equals(instrumentType)) return false;

    BigInteger userRights;
    try {
        userRights = new BigInteger(securityRights);
    } catch (Exception e) {
        // if securityRights is not a good number, userRights remains 0.
        userRights = ZERO;
    }

    return userRights.and(DDI_UPLOAD_FILE).equals(DDI_UPLOAD_FILE);
}


/**
 * Check SecurityRights, and return true if the user was granted
 * any Cash Management Instruments.
 *
 * @param String securityRights
 * @return true if user has access to any TradeInstruments, including InternationalPayment
 */
public static boolean canViewCashManagement(String securityRights){
	
	return (canViewDomesticPaymentOrTransfer(securityRights) || canViewInternationalPayment(securityRights));
	
}
/**
 * Check SecurityRights, and return true if the user was granted
 * any Trade Instruments, excluding International Payments.  Because InternationalPayment
 * is treated separately, it will return false if the user only has access to InternationalPayment.
 *
 * @param String securityRights
 * @return true if user has access to TradeInstrument, excluding InternationalPayment
 */
public static boolean canViewTradeInstrument(String securityRights){
	BigInteger userRights;
	boolean tradeInstrument  = false;
	try {
		userRights = new BigInteger(securityRights);
	} catch (Exception e) {
		// if securityRights is not a good number, userRights remains 0.
		userRights = ZERO;
	}
	tradeInstrument=
		(userRights.and(AIR_WAYBILL_AUTHORIZE).equals(AIR_WAYBILL_AUTHORIZE) ||
	userRights.and(AIR_WAYBILL_CREATE_MODIFY).equals(AIR_WAYBILL_CREATE_MODIFY) ||
	userRights.and(AIR_WAYBILL_DELETE).equals(AIR_WAYBILL_DELETE) ||
	userRights.and(AIR_WAYBILL_ROUTE).equals(AIR_WAYBILL_ROUTE) ||

	userRights.and(EXPORT_COLL_AUTHORIZE).equals(EXPORT_COLL_AUTHORIZE) ||
	userRights.and(EXPORT_COLL_CREATE_MODIFY).equals(EXPORT_COLL_CREATE_MODIFY) ||
	userRights.and(EXPORT_COLL_DELETE).equals(EXPORT_COLL_DELETE) ||
	userRights.and(EXPORT_COLL_ROUTE).equals(EXPORT_COLL_ROUTE) ||
	
	userRights.and(NEW_EXPORT_COLL_AUTHORIZE).equals(NEW_EXPORT_COLL_AUTHORIZE) ||
	userRights.and(NEW_EXPORT_COLL_CREATE_MODIFY).equals(NEW_EXPORT_COLL_CREATE_MODIFY) ||
	userRights.and(NEW_EXPORT_COLL_DELETE).equals(NEW_EXPORT_COLL_DELETE) ||
	userRights.and(NEW_EXPORT_COLL_ROUTE).equals(NEW_EXPORT_COLL_ROUTE) ||

	userRights.and(EXPORT_LC_AUTHORIZE).equals(EXPORT_LC_AUTHORIZE) ||
	userRights.and(EXPORT_LC_CREATE_MODIFY).equals(EXPORT_LC_CREATE_MODIFY) ||
	userRights.and(EXPORT_LC_DELETE).equals(EXPORT_LC_DELETE) ||
	userRights.and(EXPORT_LC_ROUTE).equals(EXPORT_LC_ROUTE) ||

	userRights.and(GUAR_AUTHORIZE).equals(GUAR_AUTHORIZE) ||
	userRights.and(GUAR_CREATE_MODIFY).equals(GUAR_CREATE_MODIFY) ||
	userRights.and(GUAR_DELETE).equals(GUAR_DELETE) ||
	userRights.and(GUAR_ROUTE).equals(GUAR_ROUTE) ||

	userRights.and(DLC_AUTHORIZE).equals(DLC_AUTHORIZE) ||
	userRights.and(DLC_CREATE_MODIFY).equals(DLC_CREATE_MODIFY) ||
	userRights.and(DLC_DELETE).equals(DLC_DELETE) ||
	userRights.and(DLC_ROUTE).equals(DLC_ROUTE) ||

	userRights.and(SLC_AUTHORIZE).equals(SLC_AUTHORIZE) ||
	userRights.and(SLC_CREATE_MODIFY).equals(SLC_CREATE_MODIFY) ||
	userRights.and(SLC_DELETE).equals(SLC_DELETE) ||
	userRights.and(SLC_ROUTE).equals(SLC_ROUTE) ||

	userRights.and(LC_ASSIGN_AUTHORIZE).equals(LC_ASSIGN_AUTHORIZE) ||
	userRights.and(LC_ASSIGN_CREATE_MODIFY).equals(LC_ASSIGN_CREATE_MODIFY) ||
	userRights.and(LC_ASSIGN_DELETE).equals(LC_ASSIGN_DELETE) ||
	userRights.and(LC_ASSIGN_ROUTE).equals(LC_ASSIGN_ROUTE) ||

	userRights.and(LC_TRANSFER_AUTHORIZE).equals(LC_TRANSFER_AUTHORIZE) ||
	userRights.and(LC_TRANSFER_CREATE_MODIFY).equals(LC_TRANSFER_CREATE_MODIFY) ||
	userRights.and(LC_TRANSFER_DELETE).equals(LC_TRANSFER_DELETE) ||
	userRights.and(LC_TRANSFER_ROUTE).equals(LC_TRANSFER_ROUTE) ||

	userRights.and(SHIP_GUAR_AUTHORIZE).equals(SHIP_GUAR_AUTHORIZE) ||
	userRights.and(SHIP_GUAR_CREATE_MODIFY).equals(SHIP_GUAR_CREATE_MODIFY) ||
	userRights.and(SHIP_GUAR_DELETE).equals(SHIP_GUAR_DELETE) ||
	userRights.and(SHIP_GUAR_ROUTE).equals(SHIP_GUAR_ROUTE) ||

	userRights.and(VIEW_CHILD_ORG_WORK).equals(VIEW_CHILD_ORG_WORK) ||
	userRights.and(DISCREPANCY_CREATE_MODIFY).equals(DISCREPANCY_CREATE_MODIFY) ||
	userRights.and(DISCREPANCY_AUTHORIZE).equals(DISCREPANCY_AUTHORIZE) ||
	userRights.and(DISCREPANCY_DELETE).equals(DISCREPANCY_DELETE) ||
	userRights.and(DISCREPANCY_ROUTE).equals(DISCREPANCY_ROUTE) ||

	userRights.and(LOAN_RQST_AUTHORIZE).equals(SHIP_GUAR_ROUTE) ||
	userRights.and(LOAN_RQST_CREATE_MODIFY).equals(LOAN_RQST_CREATE_MODIFY) ||
	userRights.and(LOAN_RQST_DELETE).equals(LOAN_RQST_DELETE) ||
	userRights.and(LOAN_RQST_ROUTE).equals(LOAN_RQST_ROUTE) ||

	userRights.and(REQUEST_ADVISE_AUTHORIZE).equals(REQUEST_ADVISE_AUTHORIZE) ||
	userRights.and(REQUEST_ADVISE_CREATE_MODIFY).equals(REQUEST_ADVISE_CREATE_MODIFY) ||
	userRights.and(REQUEST_ADVISE_DELETE).equals(REQUEST_ADVISE_DELETE) ||
	userRights.and(REQUEST_ADVISE_ROUTE).equals(REQUEST_ADVISE_ROUTE) ||

	userRights.and(APPROVAL_TO_PAY_AUTHORIZE).equals(APPROVAL_TO_PAY_AUTHORIZE) ||
	userRights.and(APPROVAL_TO_PAY_CREATE_MODIFY).equals(APPROVAL_TO_PAY_CREATE_MODIFY) ||
	userRights.and(APPROVAL_TO_PAY_DELETE).equals(APPROVAL_TO_PAY_DELETE) ||
	userRights.and(APPROVAL_TO_PAY_ROUTE).equals(APPROVAL_TO_PAY_ROUTE) ||
	userRights.and(APPROVAL_TO_PAY_PROCESS_PO).equals(APPROVAL_TO_PAY_PROCESS_PO) );


	return tradeInstrument;

}

/**
 * Check SecurityRights, and return true if the user was granted
 * any TradeInstruments, other than International Payments.
 * @param String securityRights
 * @return true if user has access to TradeInstruments
 */
public static boolean canViewInternationalPayment(String securityRights){
	BigInteger userRights;

	try {
		userRights = new BigInteger(securityRights);
	} catch (Exception e) {
		// if securityRights is not a good number, userRights remains 0.
		userRights = ZERO;
	}

	boolean allowInternationalPayment = (userRights.and(FUNDS_XFER_AUTHORIZE).equals(FUNDS_XFER_AUTHORIZE)||
							userRights.and(FUNDS_XFER_CREATE_MODIFY).equals(FUNDS_XFER_CREATE_MODIFY)||
							userRights.and(FUNDS_XFER_DELETE).equals(FUNDS_XFER_DELETE)||
							userRights.and(FUNDS_XFER_ROUTE).equals(FUNDS_XFER_ROUTE));
	return allowInternationalPayment;
  }

/**
 * Check SecurityRights, and return true if the user was granted either
 * Domestic Payments or Transfer Between Accounts Instruments.
 *
 * @param String securityRights
 * @return true if user has access to either Domestic Payments or Transfer Between Accounts
 */
public static boolean canViewDomesticPaymentOrTransfer(String securityRights){
	BigInteger userRights;
	boolean allowCashManagement  = false;
	try {
		userRights = new BigInteger(securityRights);
	} catch (Exception e) {
		// if securityRights is not a good number, userRights remains 0.
		userRights = ZERO;
	}

	allowCashManagement=(userRights.and(TRANSFER_AUTHORIZE).equals(TRANSFER_AUTHORIZE)||
			userRights.and(TRANSFER_CREATE_MODIFY).equals(TRANSFER_CREATE_MODIFY)||
			userRights.and(TRANSFER_DELETE).equals(TRANSFER_DELETE)||
			userRights.and(TRANSFER_ROUTE).equals(TRANSFER_ROUTE)||
			userRights.and(DOMESTIC_AUTHORIZE).equals(DOMESTIC_AUTHORIZE)||
			userRights.and(DOMESTIC_CREATE_MODIFY).equals(DOMESTIC_CREATE_MODIFY)||
			userRights.and(DOMESTIC_DELETE).equals(DOMESTIC_DELETE)||
			userRights.and(DOMESTIC_ROUTE).equals(DOMESTIC_ROUTE)||
			userRights.and(DOMESTIC_UPLOAD_BULK_FILE).equals(DOMESTIC_UPLOAD_BULK_FILE));

	return allowCashManagement;
  }

/**
 * Check SecurityRights, and return true if the user was granted
 * any TradeInstruments, including InternationalPayments (although sometimes International
 * Payments are only a Cash instrument)
 *
 * @param String securityRights
 * @return true if user has access to any TradeInstruments, including InternationalPayment
 */
public static boolean canViewTradeInstrumentsOrInternationalPayments(String securityRights){

	return (canViewInternationalPayment(securityRights) || canViewTradeInstrument(securityRights));
}
//TRUDDEN IR PYUJ031764918 End


//AAlubala - CR-711 Rel8.0 - 10/14/2011 - Proxy Instrument Authorization - start
/**
 * Given the security rights and a instrument type, determines if the
 * user has PROXY authorize access for that instrument type.
 *
 * @return boolean - true if has access, false otherwise
 * @param securityRights java.lang.String
 * @param instrumentType java.lang.String
 * @param transactionType java.lang.String
 */

	public static boolean canProxyAuthorizeInstrument(String securityRights,
			String instrumentType, String transactionType) {
		BigInteger userRights;
		try {
			userRights = new BigInteger(securityRights);
		} catch (Exception e) {
			// if securityRights is not a good number, userRights remains 0.
			userRights = ZERO;
		}

		// Do a bitwise comparison of the user's rights with the requested
		// access to see if the user has it. If so, true is returned.
		if ((userRights.and(ACCESS_INSTRUMENTS_AREA)
				.equals(ACCESS_INSTRUMENTS_AREA))) {
			//The first check that overrides all other checks
			if (transactionType != null
					&& (transactionType
							.equals(TransactionType.DISCREPANCY) || transactionType
							.equals(TransactionType.APPROVAL_TO_PAY_RESPONSE))) {
				return userRights.and(PROXY_DR_AUTHORIZE).equals(
						PROXY_DR_AUTHORIZE);
			}
			if ( TransactionType.SIM.equals(transactionType) || TransactionType.SIR.equals(transactionType) ) {
				return userRights.and(PROXY_SIT_AUTHORIZE).equals(PROXY_SIT_AUTHORIZE);
			}

			if (instrumentType.equals(InstrumentType.AIR_WAYBILL)) {
				return userRights.and(PROXY_AWR_AUTHORIZE).equals(
						PROXY_AWR_AUTHORIZE);
			}
			if (instrumentType.equals(InstrumentType.EXPORT_COL)) {
				return userRights.and(PROXY_EXP_COLL_AUTHORIZE).equals(
						PROXY_EXP_COLL_AUTHORIZE);
			}
			if (instrumentType.equals(InstrumentType.NEW_EXPORT_COL)) {
				return userRights.and(PROXY_NEW_EXP_COLL_AUTHORIZE).equals(
						PROXY_NEW_EXP_COLL_AUTHORIZE);
			}
			if (instrumentType.equals(InstrumentType.GUARANTEE)) {
				return userRights.and(PROXY_GUAR_AUTHORIZE).equals(PROXY_GUAR_AUTHORIZE);
			}
			if (instrumentType.equals(InstrumentType.IMPORT_DLC)) {
				return userRights.and(PROXY_DLC_AUTHORIZE).equals(PROXY_DLC_AUTHORIZE);
			}
			if (instrumentType.equals(InstrumentType.STANDBY_LC)) {
				return userRights.and(PROXY_SLC_AUTHORIZE).equals(PROXY_SLC_AUTHORIZE);
			}
			if (instrumentType.equals(InstrumentType.EXPORT_DLC)) {
				return userRights.and(PROXY_EXP_DLC_AUTHORIZE).equals(
						PROXY_EXP_DLC_AUTHORIZE);
			}
			if (instrumentType.equals(InstrumentType.SHIP_GUAR)) {
				return userRights.and(PROXY_SG_AUTHORIZE).equals(
						PROXY_SG_AUTHORIZE);
			}
			if (instrumentType.equals(InstrumentType.LOAN_RQST)) {
				return userRights.and(PROXY_LR_AUTHORIZE).equals(
						PROXY_LR_AUTHORIZE);
			}
			if (instrumentType.equals(InstrumentType.FUNDS_XFER)) {
				return userRights.and(PROXY_FT_AUTHORIZE).equals(
						PROXY_FT_AUTHORIZE);
			}
			if (instrumentType.equals(InstrumentType.REQUEST_ADVISE)) {
				return userRights.and(PROXY_RA_AUTHORIZE).equals(
						PROXY_RA_AUTHORIZE);
			}
			if (instrumentType.equals(InstrumentType.APPROVAL_TO_PAY)) {
				return userRights.and(PROXY_ATP_AUTHORIZE).equals(
						PROXY_ATP_AUTHORIZE);
			}
			if (instrumentType.equals(InstrumentType.XFER_BET_ACCTS)) {
				return userRights.and(PROXY_TF_AUTHORIZE).equals(
						PROXY_TF_AUTHORIZE);
			}
			if (instrumentType.equals(InstrumentType.DOMESTIC_PMT)) {
				return userRights.and(PROXY_DP_AUTHORIZE).equals(
						PROXY_DP_AUTHORIZE);
			}
			//Receivables management
			if (instrumentType.equals(InstrumentType.RECEIVABLES_FINANCE_INSTRUMENT)) {
				return userRights.and(PROXY_RECEIVABLES_AUTHORIZE).equals(
						PROXY_RECEIVABLES_AUTHORIZE);
			}
			
			if (instrumentType.equals(InstrumentType.RECEIVABLES_PAYBLES_MANAGEMENT)) {
				boolean canSeeReceivablesManagement = false;
				if( userRights.and(PROXY_RECEIVABLES_AUTHORIZE).equals(
						PROXY_RECEIVABLES_AUTHORIZE) ||
						userRights.and(RECEIVABLES_PROXY_APPROVE_DISCOUNT_AUTHORIZE).equals(
								RECEIVABLES_PROXY_APPROVE_DISCOUNT_AUTHORIZE) ||
								userRights.and(PROXY_RECEIVABLES_AUTHORIZE_INVOICE).equals(
										PROXY_RECEIVABLES_AUTHORIZE_INVOICE)	//Invoice is also REC instrument type
						){
					canSeeReceivablesManagement= true;
				}

				return canSeeReceivablesManagement;


			}

			//Invoice Management
			if (instrumentType.equals("RECI")) {
				return userRights.and(PROXY_RECEIVABLES_AUTHORIZE_INVOICE).equals(
						PROXY_RECEIVABLES_AUTHORIZE_INVOICE);
			}

			
		}
		if ((userRights.and(ACCESS_DIRECTDEBIT_AREA)
				.equals(ACCESS_DIRECTDEBIT_AREA))) {

			if (instrumentType
					.equals(InstrumentType.DIRECT_DEBIT_INSTRUCTION)) {
				return userRights.and(PROXY_DDI_AUTHORIZE).equals(PROXY_DDI_AUTHORIZE);
			}
		}

		return false;
	}

	
	
	public static boolean canCreateModConvertInstrument(String securityRights, String instrumentType,
					     String transactionType) {
	
	BigInteger userRights;
	try {
		userRights = new BigInteger(securityRights);
	} catch (Exception e) {
		// if securityRights is not a good number, userRights remains 0.
		userRights = ZERO;
	}
	if (instrumentType.equals(InstrumentType.GUARANTEE)) {
			return userRights.and(CC_GUA_CREATE_MODIFY).equals(CC_GUA_CREATE_MODIFY);
		}
		return false;
	}

	public static boolean canAuthorizeConvertInstrument(String securityRights, String instrumentType,
					     String transactionType) {
	
	BigInteger userRights;
	try {
		userRights = new BigInteger(securityRights);
	} catch (Exception e) {
		// if securityRights is not a good number, userRights remains 0.
		userRights = ZERO;
	}
	if (instrumentType.equals(InstrumentType.GUARANTEE)) {
			return userRights.and(CC_GUA_CONVERT).equals(CC_GUA_CONVERT);
		}
		return false;
	}
	




	
	//Sandeep Rel-8.3 CR-821 Dev 06/05/2013 - Start
	/**
	 * Given the security rights and a instrument type, determines if the
	 * user has Checker access for that instrument type.
	 *
	 * @return boolean - true if has access, false otherwise
	 * @param securityRights java.lang.String
	 * @param instrumentType java.lang.String
	  * @param transactionType java.lang.String  - // jgadela - CR-821 IR T36000019687.
	 */
	public static boolean hasCheckerAccess(String securityRights, String instrumentType, String transactionType ) {
		BigInteger userRights;
	    try {
	    	userRights = new BigInteger(securityRights);
	    } catch (Exception e) {
	    	// if securityRights is not a good number, userRights remains 0.
	       	userRights = ZERO;
	              
	       	LOG.error("*** Exception occured inside hasCheckerAccess() of Security Access Java >> Exception: ",e);
	    }
	    

	    if (InstrumentType.INCOMING_SLC.equals(instrumentType) && TransactionType.DISCREPANCY.equals(transactionType))  {
	        return userRights.and(DISCREPANCY_CHECKER).equals(DISCREPANCY_CHECKER);
	    }
	   
	    if (InstrumentType.STANDBY_LC.equals(instrumentType) && TransactionType.DISCREPANCY.equals(transactionType))  {
	        return userRights.and(DISCREPANCY_CHECKER).equals(DISCREPANCY_CHECKER);
	    }
	    
	    if (InstrumentType.IMPORT_DLC.equals(instrumentType) && TransactionType.DISCREPANCY.equals(transactionType))  {
	        return userRights.and(DISCREPANCY_CHECKER).equals(DISCREPANCY_CHECKER);
	    }
	    
	    if (InstrumentType.EXPORT_DLC.equals(instrumentType) && TransactionType.DISCREPANCY.equals(transactionType))  {
	        return userRights.and(DISCREPANCY_CHECKER).equals(DISCREPANCY_CHECKER);
	    }
	    
	    if ( TransactionType.SIR.equals(transactionType) || TransactionType.SIM.equals(transactionType) ) {
	    	return userRights.and(SIT_CHECKER).equals(SIT_CHECKER);
	    }
	  

	    if (InstrumentType.APPROVAL_TO_PAY.equals(instrumentType))  {
	    	return userRights.and(APPROVAL_TO_PAY_CHECKER).equals(APPROVAL_TO_PAY_CHECKER);
	    }
	    if (InstrumentType.DOM_PMT.equals(instrumentType))  {
	    	return userRights.and(DOMESTIC_CHECKER).equals(DOMESTIC_CHECKER);
	    }
	    if (InstrumentType.IMPORT_DLC.equals(instrumentType))  {
	    	return userRights.and(DLC_CHECKER).equals(DLC_CHECKER);
	    }
	    if (InstrumentType.STANDBY_LC.equals(instrumentType))  {
	    	return userRights.and(SLC_CHECKER).equals(SLC_CHECKER);
	    }
	    if (InstrumentType.EXPORT_DLC.equals(instrumentType))  {
	    	return userRights.and(EXPORT_LC_CHECKER).equals(EXPORT_LC_CHECKER);
	    }
	    if (InstrumentType.EXPORT_COL.equals(instrumentType))  {
	    	return userRights.and(EXPORT_COLL_CHECKER).equals(EXPORT_COLL_CHECKER);
	    }
	    if (InstrumentType.GUARANTEE.equals(instrumentType))  {
	    	return userRights.and(GUAR_CHECKER).equals(GUAR_CHECKER);
	    }
	    if (InstrumentType.NEW_EXPORT_COL.equals(instrumentType))  {
	    	return userRights.and(NEW_EXPORT_COLL_CHECKER).equals(NEW_EXPORT_COLL_CHECKER);
	    }
	    if (InstrumentType.SHIP_GUAR.equals(instrumentType))  {
	    	return userRights.and(SHIP_GUAR_CHECKER).equals(SHIP_GUAR_CHECKER);
	    }
	    if (InstrumentType.AIR_WAYBILL.equals(instrumentType))  {
	    	return userRights.and(AIR_WAYBILL_CHECKER).equals(AIR_WAYBILL_CHECKER);
	    }
	    if (InstrumentType.LOAN_RQST.equals(instrumentType))  {
	    	return userRights.and(LOAN_RQST_CHECKER).equals(LOAN_RQST_CHECKER);
	    }
	    if (InstrumentType.XFER_BET_ACCTS.equals(instrumentType))  {
	    	return userRights.and(TRANSFER_CHECKER).equals(TRANSFER_CHECKER);
	    }
	    if (InstrumentType.REQUEST_ADVISE.equals(instrumentType))  {
	    	return userRights.and(REQUEST_ADVISE_CHECKER).equals(REQUEST_ADVISE_CHECKER);
	    }
	    if (InstrumentType.FUNDS_XFER.equals(instrumentType))  {
	    	return userRights.and(FUNDS_XFER_CHECKER).equals(FUNDS_XFER_CHECKER);
	    }
	    if (TransactionType.DISCREPANCY.equals(instrumentType))  {
	    	return userRights.and(DISCREPANCY_CHECKER).equals(DISCREPANCY_CHECKER);
	    }

	    return false;
	}
	
	/**
	 * Given the security rights and a instrument type, determines if the
	 * user has Repair access for that instrument type.
	 *
	 * @return boolean - true if has access, false otherwise
	 * @param securityRights java.lang.String
	 * @param instrumentType java.lang.String 
	 * @param transactionType java.lang.String  - // jgadela - CR-821 IR T36000019687.
	 */
	public static boolean hasRepairAccess(String securityRights, String instrumentType, String transactionType) {
		BigInteger userRights;
		try {
			userRights = new BigInteger(securityRights);
		} catch (Exception e) {
         	// if securityRights is not a good number, userRights remains 0.
        	userRights = ZERO;
              
         	LOG.error("*** Exception occured inside hasRepairAccess() of Security Access Java >> Exception: ",e);
		}
		
	
	    if (InstrumentType.INCOMING_SLC.equals(instrumentType) && TransactionType.DISCREPANCY.equals(transactionType))  {
	    	return userRights.and(DISCREPANCY_REPAIR).equals(DISCREPANCY_REPAIR);
	    }
	   
	    if (InstrumentType.STANDBY_LC.equals(instrumentType) && TransactionType.DISCREPANCY.equals(transactionType))  {
	    	return userRights.and(DISCREPANCY_REPAIR).equals(DISCREPANCY_REPAIR);
	    }
	    
	    if (InstrumentType.IMPORT_DLC.equals(instrumentType) && TransactionType.DISCREPANCY.equals(transactionType))  {
	    	return userRights.and(DISCREPANCY_REPAIR).equals(DISCREPANCY_REPAIR);
	    }
	    
	    if (InstrumentType.EXPORT_DLC.equals(instrumentType) && TransactionType.DISCREPANCY.equals(transactionType))  {
	    	return userRights.and(DISCREPANCY_REPAIR).equals(DISCREPANCY_REPAIR);
	    }
	    
	    if ( TransactionType.SIR.equals(transactionType) || TransactionType.SIM.equals(transactionType) ) {
	    	return userRights.and(SIT_REPAIR).equals(SIT_REPAIR);
	    }
	    
		if (InstrumentType.APPROVAL_TO_PAY.equals(instrumentType))  {
	    	return userRights.and(APPROVAL_TO_PAY_REPAIR).equals(APPROVAL_TO_PAY_REPAIR);
	    }
	    if (InstrumentType.DOM_PMT.equals(instrumentType))  {
	    	return userRights.and(DOMESTIC_REPAIR).equals(DOMESTIC_REPAIR);
	    }
	    if (InstrumentType.IMPORT_DLC.equals(instrumentType))  {
	    	return userRights.and(DLC_REPAIR).equals(DLC_REPAIR);
	    }
	    if (InstrumentType.STANDBY_LC.equals(instrumentType))  {
	    	return userRights.and(SLC_REPAIR).equals(SLC_REPAIR);
	    }
	    if (InstrumentType.EXPORT_DLC.equals(instrumentType))  {
	    	return userRights.and(EXPORT_LC_REPAIR).equals(EXPORT_LC_REPAIR);
	    }
	    if (InstrumentType.EXPORT_COL.equals(instrumentType))  {
	    	return userRights.and(EXPORT_COLL_REPAIR).equals(EXPORT_COLL_REPAIR);
	    }
	    if (InstrumentType.GUARANTEE.equals(instrumentType))  {
	    	return userRights.and(GUAR_REPAIR).equals(GUAR_REPAIR);
	    }
	    if (InstrumentType.NEW_EXPORT_COL.equals(instrumentType))  {
	    	return userRights.and(NEW_EXPORT_COLL_REPAIR).equals(NEW_EXPORT_COLL_REPAIR);
	    }
	    if (InstrumentType.SHIP_GUAR.equals(instrumentType))  {
	    	return userRights.and(SHIP_GUAR_REPAIR).equals(SHIP_GUAR_REPAIR);
	    }
	    if (InstrumentType.AIR_WAYBILL.equals(instrumentType))  {
	    	return userRights.and(AIR_WAYBILL_REPAIR).equals(AIR_WAYBILL_REPAIR);
	    }
	    if (InstrumentType.LOAN_RQST.equals(instrumentType))  {
	    	return userRights.and(LOAN_RQST_REPAIR).equals(LOAN_RQST_REPAIR);
	    }
	    if (InstrumentType.XFER_BET_ACCTS.equals(instrumentType))  {
	    	return userRights.and(TRANSFER_REPAIR).equals(TRANSFER_REPAIR);
	    }
	    if (InstrumentType.REQUEST_ADVISE.equals(instrumentType))  {
	    	return userRights.and(REQUEST_ADVISE_REPAIR).equals(REQUEST_ADVISE_REPAIR);
	    }
	    if (InstrumentType.FUNDS_XFER.equals(instrumentType))  {
	    	return userRights.and(FUNDS_XFER_REPAIR).equals(FUNDS_XFER_REPAIR);
	    }
	    if (TransactionType.DISCREPANCY.equals(instrumentType))  {
	    	return userRights.and(DISCREPANCY_REPAIR).equals(DISCREPANCY_REPAIR);
	    }
	    
		return false;
	}
	
	/**
	 * This method validates the user rights for performing action against transaction.
	 * 
	 * @param instrType -  Instrument type i.e ATP, LRQ, AIR etc
	 * @param transType -  Transaction type of instrument i.e. ISS, AMD, APR, DCR etc
	 * @param securityRights -  security rights of logged in user
	 * @param buttonPressed -   Button pressed by user i.e. Authorize, Verify, Edit, Delete etc
	 * @throws AmsException
	 */
	public static void validateUserRightsForTrans(String instrType, String transType, String securityRights, String buttonPressed)
	throws AmsException {
		
		if(TradePortalConstants.BUTTON_VERIFY.equals(buttonPressed) || TradePortalConstants.BUTTON_SAVETRANS.equals(buttonPressed) ||
				TradePortalConstants.BUTTON_SAVECLOSETRANS.equals(buttonPressed) || TradePortalConstants.BUTTON_EDIT.equals(buttonPressed)||
				TradePortalConstants.BUTTON_VERIFY_FX.equals(buttonPressed) )
		{
			buttonPressed="CREATE_MODIFY";
		}
		if( TransactionType.DISCREPANCY.equals(transType)||TransactionType.APPROVAL_TO_PAY_RESPONSE.equals(transType) )
		{
			instrType = TransactionType.DISCREPANCY;
		}
		
		if (TradePortalConstants.BUTTON_PROXY_AUTHORIZE.equals(buttonPressed)) 
		{
			if (!canProxyAuthorizeInstrument(securityRights, instrType, transType)) 
			{
				throw new AmsException("User is trying to perform action without proper access level");
			}
		} 
		else
		{
			BigInteger sucurityAccess = transSecurityAccess.get(instrType + "_" + buttonPressed.toUpperCase());
			if (sucurityAccess != null) 
			{
				if (!SecurityAccess.hasRights(securityRights, sucurityAccess)) 
				{
					throw new AmsException("User is trying to perform action without proper access level");
				}
			}
		}
	}
	
	/**
	 *  This method is used to validate logged-in user rights for refdata
	 *  
	 * @param objectName - name of refdata that is being modified
	 * @param loginUserRights - security rights of user
	 * @param updateMode -  update mode C- Insert, D- delete
	 * @param ownerOrgOid -  user owner org oid
	 * @param inputDoc - Input Document handler
	 * @param busObj -  Object of refdata
	 * @throws RemoteException
	 * @throws AmsException
	 */
	public static void validateUserRightsForRefdata(String objectName, String loginUserRights, String updateMode, String ownerOrgOid,
			DocumentHandler inputDoc, BusinessObject busObj)
			throws RemoteException, AmsException {
		
		String dataRecordOwnerOrgOIdAttr = "owner_org_oid";
		if(beanWithDiffCorpAttr.contains(objectName)){
			dataRecordOwnerOrgOIdAttr = "corp_org_oid";
		}
		if("User".equals(objectName) || "SecurityProfile".equals(objectName)){				
			if ( !(SecurityAccess.hasRights( loginUserRights, getMaintainAccess(busObj, isAdmin(inputDoc, updateMode, busObj)) ) &&
					 isSameOrg( objectName, updateMode, dataRecordOwnerOrgOIdAttr, ownerOrgOid, busObj )) ){
		        throw new AmsException("User is trying to maintain: "+objectName+" without proper access level");
			}
		}else if( "LCCreationRule".equals(objectName) || "InvOnlyCreateRule".equals(objectName) ) {
			String instrType = inputDoc.getAttribute("/" + objectName + "/instrument_type_code");;
			if ( !(SecurityAccess.hasRights(loginUserRights, refdataSecurityAccess.get( instrType + objectName )) && 
					                      isSameOrg( objectName, updateMode, dataRecordOwnerOrgOIdAttr, ownerOrgOid, busObj )) ) {
		        throw new AmsException("User is trying to maintain: "+objectName+" without proper access level");
			}
		}
		
		else if(refdataSecurityAccess.get(objectName) != null){				
			if ( !(SecurityAccess.hasRights(loginUserRights, refdataSecurityAccess.get(objectName))) && 
					 isSameOrg( objectName, updateMode, dataRecordOwnerOrgOIdAttr, ownerOrgOid, busObj ))  {
			  throw new AmsException("User is trying to maintain: "+objectName+" without proper access level");
			}
		}
	}
	
	/**
	 * This method is used to checke whether owner of refdata (in database) is same as Logined-in user onwer org
	 * 
	 * @param updateMode
	 * @param dataRecordOwner
	 * @param loginUserOwnerOrgOid
	 * @return
	 * @throws AmsException 
	 * @throws RemoteException 
	 */
	private static boolean isSameOrg(String beanName, String updateMode,
			String dataRecordOwnerOrgOIdAttr, String loginUserOwnerOrgOid,
			BusinessObject busObj) throws RemoteException, AmsException {
		boolean isSameOrg = true;
		boolean isSameOrgCheckReq = true;
		if (beanToSkipSameOrqCheck.contains(beanName)) {
			isSameOrgCheckReq = false;
		}
		final String INSERT_MODE = "C";
		if (!INSERT_MODE.equals(updateMode) && isSameOrgCheckReq) {
			String dataRecordOwnerOid = busObj.getAttribute(dataRecordOwnerOrgOIdAttr);
			if (!dataRecordOwnerOid.equals(loginUserOwnerOrgOid)) {
				isSameOrg = false;
			}
		}
		return isSameOrg;
	}
	
	/**
	 * This method return the secuirty rights according to owner level and refdata type
	 * 
	 * @param busObj
	 * @param isAdmin
	 * @return
	 * @throws AmsException
	 */
	private static BigInteger getMaintainAccess(Object busObj, boolean isAdmin) throws AmsException{
		
		BigInteger SecurityRights = SecurityAccess.NO_ACCESS;
		
		if(busObj instanceof User){			
			if(isAdmin){
				SecurityRights = SecurityAccess.MAINTAIN_ADM_USERS;	
			}else{				
				SecurityRights = SecurityAccess.MAINTAIN_USERS;
			}
		}else if(busObj instanceof SecurityProfile){
			if(isAdmin){
				SecurityRights = SecurityAccess.MAINTAIN_ADM_SEC_PROF;
			}else{
				SecurityRights = SecurityAccess.MAINTAIN_SEC_PROF;
			}
		}
		
		return SecurityRights;
	}
	
	/**
	 * This method is used to check whether modifing refdata is related to ADMIN(Admin Security profile or admin user) or 
	 * NON-ADMIN (security profile or corporate user).
	 * 
	 * @param inputDoc
	 * @param updateMode
	 * @param busObj
	 * @return
	 * @throws RemoteException
	 * @throws AmsException
	 */
	private static boolean isAdmin(DocumentHandler inputDoc, String updateMode, Object busObj) throws RemoteException, AmsException{

		String theType="";
		final String INSERT_MODE = "C";
		if(busObj instanceof User){			
			if(INSERT_MODE.equals(updateMode)){
				theType = inputDoc.getAttribute("/User/user_type");
			}else{
				User user = (User) busObj;
				theType = user.getAttribute("ownership_level");
			}
			if (!TradePortalConstants.OWNER_CORPORATE.equals(theType)) {
				return true;
			}			
		}else if(busObj instanceof SecurityProfile){
			if(INSERT_MODE.equals(updateMode)){
				theType = inputDoc.getAttribute("/SecurityProfile/security_profile_type");
			}else{
				SecurityProfile secProfile = (SecurityProfile) busObj;
				theType = secProfile.getAttribute("security_profile_type");
			}
			if(TradePortalConstants.ADMIN.equals(theType)){
				return true;
			}			
		}		
		
		return false;
	}
	
	/**
	 * This method is used to check whetehr user has rights to create settlement instruction messages and response
	 * @param userSecurityRights
	 * @return
	 */
	public static boolean canCreateModSettlementMsgAndResponse (String userSecurityRights) {
		return hasRights(userSecurityRights, SecurityAccess.SIT_CREATE_MODIFY );
	}
	
}