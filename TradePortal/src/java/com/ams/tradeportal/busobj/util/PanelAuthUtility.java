package com.ams.tradeportal.busobj.util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.Vector;

import javax.ejb.EJBObject;

import com.ams.tradeportal.busobj.Account;
import com.ams.tradeportal.busobj.CorporateOrganization;
import com.ams.tradeportal.busobj.CorporateOrganizationBean;
import com.ams.tradeportal.busobj.DmstPmtPanelAuthRange;
import com.ams.tradeportal.busobj.Instrument;
import com.ams.tradeportal.busobj.Invoice;
import com.ams.tradeportal.busobj.InvoiceGroup;
import com.ams.tradeportal.busobj.InvoiceOfferGroup;
import com.ams.tradeportal.busobj.InvoicesSummaryData;
import com.ams.tradeportal.busobj.PanelAuthorizationGroup;
import com.ams.tradeportal.busobj.PayRemit;
import com.ams.tradeportal.busobj.Terms;
import com.ams.tradeportal.busobj.Transaction;
import com.ams.tradeportal.busobj.User;
import com.ams.tradeportal.busobj.CreditNotes;
import com.ams.tradeportal.common.InstrumentType;
import com.ams.tradeportal.common.TPCurrencyUtility;
import com.ams.tradeportal.common.TradePortalConstants;
import com.ams.tradeportal.common.TransactionStatus;
import com.ams.tradeportal.common.TransactionType;
import com.ams.tradeportal.common.cache.Cache;
import com.ams.tradeportal.common.cache.TPCacheManager;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.frame.EJBObjectFactory;
import com.amsinc.ecsg.frame.ErrorManager;
import com.amsinc.ecsg.frame.ReferenceDataManager;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.JPylonProperties;
import com.amsinc.ecsg.util.StringFunction;

public class PanelAuthUtility {
private static final Logger LOG = LoggerFactory.getLogger(PanelAuthUtility.class);

	protected static JPylonProperties jPylonProperties;
	protected static String  ejbServerLocation;

	static
	   {
	      try
	       {
	          jPylonProperties = JPylonProperties.getInstance();
	  		  ejbServerLocation = jPylonProperties.getString("serverLocation");
	       }
	      catch(Exception e)
	       {
	          LOG.error("Can't determine JPylon property and Server location: ",e);
	       }
	   }

	private String panelGroupOid  = null;
    private String panelOplockVal = null;
    private String instrumentType = null;
    private Hashtable panelAliasesMap = new Hashtable();
    private Hashtable refData = new Hashtable();
    private String locale = null;
    private String currencyBasis = null;
    private String currency = null;
    private String authHistory = null;
    private Map panelAppSummaryMap = null;
    boolean isBulkPayment = false;

    public String getCurrencyBasis() {
		return currencyBasis;
	}

	public void setCurrencyBasis(String currencyBasis) {
		this.currencyBasis = currencyBasis;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getPanelGroupOid() {
		return panelGroupOid;
	}

	public String getPanelOplockVal() {
		return panelOplockVal;
	}

	public String getInstrumentType() {
		return instrumentType;
	}

	public boolean isBulkPayment() {
		return isBulkPayment;
	}

	public void setBulkPayment(boolean isBulkPayment) {
		this.isBulkPayment = isBulkPayment;
	}

	public String getAuthHistory() {
		return authHistory;
	}

	public void setAuthHistory(String authHistory) {
		this.authHistory = authHistory;
	}

	public Map getPanelAppSummaryMap() {
		return panelAppSummaryMap;
	}

	public void setPanelAppSummaryMap(Map<String, String> panelApproversSummarySortedMap) {
		this.panelAppSummaryMap = panelApproversSummarySortedMap;
	}

	/**
     * This method returns whether it is subsidiary access.
     *
     * @param user
     * @return
     * @throws RemoteException
     * @throws AmsException
     */
	public static boolean isSubsidiaryAccess(String transactionOrgOid, User user) throws RemoteException,
			AmsException {

		boolean isSubsidiaryAccess = false;

		if (!transactionOrgOid.equals(user.getAttribute("owner_org_oid"))) {
			isSubsidiaryAccess = true;
		}

		return isSubsidiaryAccess;
	}

	/**
     * This method is used to get base curreny of transaction to find panel group refdata
     *
     * @param coroOrg          owner org of processing transaction
     * @param panelAuthGroup   panel group associate with transaction type at corporate org level
     * @param account          debit account of transaction
     * @param instrumentType   insturment type of processing transaction
     * @return base currecy of processing transaction to find panel range from panel group refdata
     * @throws RemoteException
     * @throws AmsException
     */
	public static String getBaseCurrency(CorporateOrganization corpOrg, PanelAuthorizationGroup panelAuthGroup, Account account, String instrumentType)
	throws RemoteException, AmsException {

		String baseCurrency = "";

		if (InstrumentServices.isCashManagementTypeInstrument(instrumentType)) {
				if (account!=null && isSubsidiaryAccount(corpOrg, account)) {

				String accountCorpOrgOid = account.getAttribute("owner_oid");
				CorporateOrganization accountCorpOrg = getCorporateOrg( corpOrg, accountCorpOrgOid);
				baseCurrency = accountCorpOrg.getAttribute("base_currency_code");

				} else {

				baseCurrency = corpOrg.getAttribute("base_currency_code");
				}

		} else if (InstrumentServices.isTradeTypeInstrument(instrumentType) || TradePortalConstants.SUPPLIER_PORTAL.equals(instrumentType)) {

			baseCurrency = corpOrg.getAttribute("base_currency_code");
		}else{
			baseCurrency = corpOrg.getAttribute("base_currency_code");
		}

		return baseCurrency;
	}

   /**
    * this method returns the amount in base currency
    *
    * @param currency
    * @param amount
    * @param baseCurrency
    * @param userOrgOid
    * @param rateType
    * @param userEnteredRate
    * @param noScaleFlag
    * @param errorMgr
    * @return
    * @throws AmsException
    * @throws RemoteException
    */
	public static BigDecimal getAmountInBaseCurrency(String currency, String amount,
			String baseCurrency, String userOrgOid, String rateType,
			BigDecimal userEnteredRate, boolean noScaleFlag,
			ErrorManager errorMgr) throws AmsException, RemoteException {

		BigDecimal amountInForeign = (new BigDecimal(amount)).abs();
		BigDecimal amountInBase = new BigDecimal(-1.0f);
		DocumentHandler resultSet = null;
		DocumentHandler fxRate = null;
		String multiplyIndicator = null;
		BigDecimal rate = null;
		BigDecimal use_rate = null;

		// Get the foreign exchange rate for the given userOrgOid
		// and foreign currency code

		if (baseCurrency.equals(currency)) {
			amountInBase = amountInForeign;
		} else {
			// get the foreign exchange rate. If it doesn't exist,
			// issue error. Otherwise, get the multiply indicator
			// and rate and convert the amount

			Cache fxCache = TPCacheManager.getInstance().getCache(TradePortalConstants.FX_RATE_CACHE);
			resultSet = (DocumentHandler) fxCache.get(userOrgOid + "|" + currency);

			// if not found in corporate or bank level throw error
			if (resultSet == null) {
				errorMgr.issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.NO_FOREX_DEFINED, currency);
			} else {
				fxRate = resultSet.getFragment("/ResultSetRecord");
				if (fxRate == null) {
					errorMgr.issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.NO_FOREX_DEFINED, currency);

				} else {
					multiplyIndicator = fxRate.getAttribute("/MULTIPLY_INDICATOR");

					// Only get date from table if not entered by user

					rate = fxRate.getAttributeDecimal("/RATE");

					// Use SELL_RATE for Transfer between Accounts Transactions
					if (InstrumentType.XFER_BET_ACCTS.equals(rateType) || TradePortalConstants.USE_SELL_RATE.equals(rateType)) {
						if (StringFunction.isBlank(fxRate.getAttribute("/SELL_RATE"))) {
							errorMgr.issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.NO_SELL_FOREX_DEFINED);
						}
						try {
							use_rate = fxRate.getAttributeDecimal("/SELL_RATE");
						} catch (Exception any_e) {
							errorMgr.issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.NO_SELL_FOREX_DEFINED);
						}
						rate = use_rate;
					}
					// Use BUY_RATE for DP, IP and explicit requests
					else if (InstrumentType.FUNDS_XFER.equals(rateType) || InstrumentType.DOM_PMT.equals(rateType)
							|| TradePortalConstants.USE_BUY_RATE.equals(rateType)) {
						if (StringFunction.isBlank(fxRate.getAttribute("/BUY_RATE"))) {
							errorMgr.issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.NO_BUY_FOREX_DEFINED);
						} else {
							try {
								use_rate = fxRate.getAttributeDecimal("/BUY_RATE");
							} catch (Exception any_e) {
								errorMgr.issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.NO_BUY_FOREX_DEFINED);
							}
						}
						rate = use_rate;
					} else{

						try {
							use_rate = fxRate.getAttributeDecimal("/SELL_RATE");
						} catch (Exception any_e) {
							errorMgr.issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.NO_SELL_FOREX_DEFINED);
						}
						rate = use_rate;
					}
				}

				//Send back Calc Method if requested by the callign routine
				if (multiplyIndicator != null && rate != null) {
					   int dec = TPCurrencyUtility.getDecimalPrecision(baseCurrency);
				   	   if (multiplyIndicator.equals(TradePortalConstants.DIVIDE)){

						 if (noScaleFlag){
							 amountInBase = amountInForeign.divide(rate, java.math.MathContext.DECIMAL128);
						 }else{
				   			amountInBase = amountInForeign.divide(rate, dec, BigDecimal.ROUND_HALF_UP);
						 }
			   		 } else {
			   			amountInBase = amountInForeign.multiply(rate);
			   		 }
				   }
				LOG.debug("rate used was: {} for type: {}: ",rate ,rateType);
			}
		}
		LOG.debug("Original amount: {} and amount in base: {} " ,amount, amountInBase);
		return amountInBase;

	}





	/**
	 * this method is used to fine whether debit account is subsidiary account or loged in user account
	 *
	 * @param corpOrg Logged in user corporate org
	 * @param account debit account for transaction
	 * @return true if it is subsidiary account or parent account for transaction
	 * @throws RemoteException
	 * @throws AmsException
	 */
    public static boolean isSubsidiaryAccount(CorporateOrganization corpOrg, Account account)
    throws RemoteException, AmsException{
    	return !corpOrg.getAttribute("organization_oid").equals(account.getAttribute("owner_oid"));
    }

    /**
     * Creates an EJB instance for the given corporate org oid.
     *
     * @param tempCorpOrg BusinessObject to create CorporateCustomer object for account corporate org
     * @param CorpOrgOid - account corporate org
     * @return corpOrg   debit account corporate org
     * @throws RemoteException
     * @throws AmsException
     */
    private static CorporateOrganization getCorporateOrg(CorporateOrganization tempCorpOrg, String CorpOrgOid)
    	throws RemoteException, AmsException {

    	CorporateOrganization corpOrg = null;;
        corpOrg = (CorporateOrganization) tempCorpOrg.createServerEJB("CorporateOrganization", Long.parseLong(CorpOrgOid));
    	return corpOrg;
    }

	/**
	 * this method returns the panel code for processing transaction
	 *
	 * @param isSubsidiaryAccess
	 * @param user
	 * @return
	 * @throws RemoteException
	 * @throws AmsException
	 */
	public static String getPanelCode(boolean isSubsidiaryAccess, User user)
			throws RemoteException, AmsException {

		String userPanelLevel = "";

		if (isSubsidiaryAccess && TradePortalConstants.INDICATOR_YES.equals(user.getAttribute("use_subsidiary_profile"))) {	//IR -T36000019989
			userPanelLevel = user.getAttribute("sub_panel_authority_code");
		} else {
			userPanelLevel = user.getAttribute("panel_authority_code");
		}

		return userPanelLevel;
	}


	public static BigDecimal getTransactionAmount(Transaction transaction, CorporateOrganization transactionOwnerOrg,
			Account debitAccount, Instrument instrument,
			PanelAuthorizationGroup panelAuthGroup, ErrorManager errMgr) throws RemoteException, AmsException{

	return getTransactionAmount(transaction, transactionOwnerOrg,
			 debitAccount, instrument, panelAuthGroup,  null, null, errMgr);
	}

	/**
	 *
	 * @param transaction
	 * @param transactionOwnerOrg
	 * @param debitAccount
	 * @param instrument
	 * @param panelAuthGroup
	 * @param errMgr
	 * @return
	 * @throws RemoteException
	 * @throws AmsException
	 */
	public static BigDecimal getTransactionAmount(Transaction transaction, CorporateOrganization transactionOwnerOrg,
			Account debitAccount, Instrument instrument,
			PanelAuthorizationGroup panelAuthGroup, String termCurrency,String termAmount, ErrorManager errMgr) throws RemoteException, AmsException{

	    String instrumentType = instrument.getAttribute("instrument_type_code");
	    String currency = transaction.getAttribute("copy_of_currency_code");
		String amount = transaction.getAttribute("copy_of_amount");
		String transactionOwnerOrgOid = instrument.getAttribute("corp_org_oid");

		if (StringFunction.isBlank(amount) || StringFunction.isBlank(currency)) {
			Terms customerTerms = (Terms)(transaction.getComponentHandle("CustomerEnteredTerms"));
			if(instrumentType.equals(InstrumentType.LOAN_RQST)) {
				currency = customerTerms.getAttribute("loan_proceeds_pmt_curr");
				amount = customerTerms.getAttribute("loan_proceeds_pmt_amount");
		    }else {
		    	currency = termCurrency;
				amount = termAmount;
		    }

		}

		BigDecimal amountInBaseToPanelGroup;
		String baseCurrencyToPanelGroup = getBaseCurrency(transactionOwnerOrg, panelAuthGroup, debitAccount, instrumentType);
		if("ACCT_CCY".equals(panelAuthGroup.getAttribute("ccy_basis"))) {

	    	   amountInBaseToPanelGroup =  getDebitAmount(transaction, baseCurrencyToPanelGroup, debitAccount.getAttribute("currency"),transactionOwnerOrgOid, termCurrency, termAmount, instrumentType, errMgr);

	       }else{

	    	   amountInBaseToPanelGroup =  getAmountInBaseCurrency(currency, amount, baseCurrencyToPanelGroup, transactionOwnerOrgOid,
	    			   instrumentType, null, false, errMgr);
	       }
		return amountInBaseToPanelGroup;
	}

	protected  static BigDecimal getDebitAmount(Transaction transaction, String baseCurrency, String fromCurrency, String transactionOwnerOrgOid, String instrumentType, ErrorManager errMgr)
	throws RemoteException, AmsException {

		return getDebitAmount(transaction, baseCurrency, fromCurrency, transactionOwnerOrgOid, null, null, instrumentType, errMgr) ;
	}

	/**
	 *
	 * @param transaction
	 * @param baseCurrency
	 * @param fromCurrency
	 * @param transactionOwnerOrgOid
	 * @param errMgr
	 * @return
	 * @throws RemoteException
	 * @throws AmsException
	 */
	protected  static BigDecimal getDebitAmount(Transaction transaction, String baseCurrency, String fromCurrency, String transactionOwnerOrgOid,
			String termCurrency, String termAmount, String instrumentType, ErrorManager errMgr) throws RemoteException, AmsException {

		   BigDecimal debit_amt =null;
		   String corp_org_oid = transactionOwnerOrgOid;

		   Terms terms = (Terms)(transaction.getComponentHandle("CustomerEnteredTerms"));
		   String transferCurrency = terms.getAttribute("amount_currency_code");
		   String amount = terms.getAttribute("amount");

		   if(StringFunction.isBlank(amount) || StringFunction.isBlank(transferCurrency)){
			   transferCurrency = termCurrency;
			   amount = termAmount;
		   }

		   if (transferCurrency.equals(fromCurrency)) {
			   debit_amt = new BigDecimal(amount).abs();
		   }
		   else if (transferCurrency.equals(baseCurrency)) {
			   debit_amt = getAmounInFXCurrency(fromCurrency, baseCurrency, amount, corp_org_oid, TradePortalConstants.USE_BUY_RATE, null, null, false, errMgr);


		   } else if (fromCurrency.equals(baseCurrency)) {
			   debit_amt = getAmountInBaseCurrency(transferCurrency, amount, baseCurrency, corp_org_oid, TradePortalConstants.USE_SELL_RATE, null, false, errMgr);
		   }
		   else {
			   BigDecimal amountInBase = getAmountInBaseCurrency(transferCurrency, amount, baseCurrency, corp_org_oid, TradePortalConstants.USE_SELL_RATE, null, false, errMgr);
			   debit_amt = getAmounInFXCurrency(fromCurrency, baseCurrency, amountInBase.toString(), corp_org_oid, TradePortalConstants.USE_BUY_RATE, null, null, false, errMgr);
		   }
		   return debit_amt;
	   }


  /**
   *
   * @param fxcurrency
   * @param baseCurrency
   * @param baseAmount
   * @param userOrgOid
   * @param useType
   * @param userEnteredRate
   * @param outMDInd
   * @param setScaleFlag
   * @param errMgr
   * @return
   * @throws AmsException
   * @throws RemoteException
   */
  public static BigDecimal getAmounInFXCurrency(String fxcurrency, String baseCurrency,
			String baseAmount, String userOrgOid, String useType, BigDecimal userEnteredRate, Vector outMDInd, boolean setScaleFlag,
			ErrorManager errMgr) //MDB MRUL122933633 Rel7.1 1/3/12
			throws AmsException, RemoteException {

		BigDecimal amountInBase = (new BigDecimal(baseAmount)).abs();
		BigDecimal amountInForeign = new BigDecimal(-1.0f);
		DocumentHandler resultSet = null, fxRate = null;
		String multiplyIndicator = null;
		BigDecimal use_rate = null;
		String useRateParm = null;

		if (baseCurrency.equals(fxcurrency)) {
			return amountInBase;
		}

		//IAZ ER 5.2.1.3 06/29/10 - Update
		//useRateParm is used to obatin rate if it is not passed
		//by the calling routine (e.g., not entered by the user)
		if (userEnteredRate == null)
		{
			if (StringFunction.isBlank(useType))
			{
				useType = TradePortalConstants.USE_BUY_RATE;
			}
			if (TradePortalConstants.USE_SELL_RATE.equals(useType))
			{
				useRateParm = "/SELL_RATE";
			}
			else
			{
				useRateParm = "/BUY_RATE";
			}
		}
		else
			use_rate = userEnteredRate;

		Cache fxCache = TPCacheManager.getInstance().getCache(TradePortalConstants.FX_RATE_CACHE);
		resultSet = (DocumentHandler) fxCache.get(userOrgOid + "|" + fxcurrency);

		if (resultSet == null) {
			if(errMgr == null){
				LOG.debug("PanelAuthUtility::getAmountInFXCurrency::No Forex Defined");
			}else{
				errMgr.issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.NO_FOREX_DEFINED, fxcurrency); //MDB CR-640 Rel7.1 9/21/11
			}
		} else {
			fxRate = resultSet.getFragment("/ResultSetRecord");
			if (fxRate == null) {
				if(errMgr == null){
					LOG.debug("PanelAuthUtility::getAmountInFXCurrency::No Forex Defined");
				}else{
					errMgr.issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.NO_FOREX_DEFINED, fxcurrency); //MDB CR-640 Rel7.1 9/21/11
				}

			} else {
				multiplyIndicator = fxRate.getAttribute("/MULTIPLY_INDICATOR");

				//IAZ ER 5.2.1.3 06/29/10 - Update
				//Only use rate form table if not entered by user
				if (use_rate == null)
				{
					try {
						use_rate = fxRate.getAttributeDecimal(useRateParm);
					} catch (Exception any_e) {
						if(errMgr == null){
							LOG.debug("PanelAuthUtility::getAmountInFXCurrency::No Forex Defined");
						}else{
							errMgr.issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.NO_FOREX_DEFINED, fxcurrency); //MDB CR-640 Rel7.1 9/21/11
						}
					}
			    }
			    //IAZ ER 5.2.1.3 06/29/10 - End-Update
			}
		}

		//IAZ ER 5.2.1.3 06/29/10 - Update
		//Send back Calc Method if requested by the callign routine
		if (multiplyIndicator != null && use_rate != null) {
			 int dec = TPCurrencyUtility.getDecimalPrecision(fxcurrency);
			if (multiplyIndicator.equals(TradePortalConstants.DIVIDE)){
				//MDB MRUL122933633 Rel7.1 1/3/12 - Begin
				if (setScaleFlag)
					amountInForeign = amountInBase.multiply(use_rate);
				else
					amountInForeign = amountInBase.multiply(use_rate).setScale(dec, BigDecimal.ROUND_HALF_UP);
				//MDB MRUL122933633 Rel7.1 1/3/12 - End
				if (outMDInd != null)
					outMDInd.add(0, TradePortalConstants.MULTIPLY);
			}else{
				//MDB MRUL122933633 Rel7.1 1/3/12 - Begin
				if (setScaleFlag)
					amountInForeign = amountInBase.divide(use_rate, java.math.MathContext.DECIMAL128);
				else
					amountInForeign = amountInBase.divide(use_rate, dec, BigDecimal.ROUND_HALF_UP);
				//MDB MRUL122933633 Rel7.1 1/3/12 - End
				if (outMDInd != null)
					outMDInd.add(0, TradePortalConstants.DIVIDE);
			}
		}
		//IAZ ER 5.2.1.3 06/29/10 - End-Update

		LOG.debug("fx rate used is: {} for type: {} " ,use_rate ,useType);

		LOG.debug("Base amount: {} and amount in ForeignCurrency: {} ",baseAmount,amountInForeign);
		return amountInForeign;

	}

  /**
   *
   * @param user
   * @param transaction
   * @param instrument
   * @param userOrgOid
   * @param skipUserAuthorizedAccountCheck
   * @return
   * @throws AmsException
   * @throws RemoteException
   */
  //IR T36000020321 Rel 8.3. Commented Terms Object and passing account_oids
  private static String validateAuthorizedAccounts(User user, Transaction transaction, Instrument instrument,String userOrgOid, boolean skipUserAuthorizedAccountCheck,
		  			String debit_account_oid, String credit_account_oid, String payment_charges_account_oid)
 	throws AmsException, RemoteException
 {

	   	//IAZ CM-451 03/05/09 Begin
	   	//For subsidiary access, any and all child organization accounts are valid
	   	//Check whether corp org stored with the instrument is a subsiadiary of authorizing user:
	  
	   	String instrumentACorpOrgOid = instrument.getAttribute("corp_org_oid");
	   	LOG.debug("validateAuthorizedAccounts:: corpOrgOid: {} and userOrgOid: {} " ,instrumentACorpOrgOid, userOrgOid);

	   	if ((StringFunction.isNotBlank(instrumentACorpOrgOid))&&(!instrumentACorpOrgOid.equals(userOrgOid)))
	   	{
			CorporateOrganization instrumentCorpOrg = (CorporateOrganization) EJBObjectFactory.createClientEJB(ejbServerLocation, "CorporateOrganization",
													  Long.parseLong(instrumentACorpOrgOid));

			String corpParentOrgOid = instrumentCorpOrg.getAttribute("parent_corp_org_oid");

			//cleanup ejb
			removeEJBReference(instrumentCorpOrg);

	   		LOG.debug("validateAuthorizedAccounts:: for corpOrgParentOid: {} and userOrgOid: {}" ,corpParentOrgOid ,userOrgOid);

			if (userOrgOid.equals(corpParentOrgOid))
			{
				return instrumentACorpOrgOid;
				//return true;
			}
		}
	   	//IAZ CM-451 03/05/09 End


		DocumentHandler resultSet = null;

		 // IAZ ER 5.0.1.1 06/29/09 Begin
      if (StringFunction.isBlank(debit_account_oid))
      {
			if (InstrumentType.FUNDS_XFER.equals(instrument.getAttribute("instrument_type_code")))
				return userOrgOid;
			else
				return null;
		}
		//IAZ ER 5.0.1.1 06/29/09 End

      // CJR - 7/6/2011 - REUL070142455 - Added skipUserAuthorizedAccountCheck parameter to ignore checking the user_authorized_acct table based on this flag.
      // 									This parameter is used for straight through processing.
    
      if(skipUserAuthorizedAccountCheck == false){

      	String sql = "select authorized_account_oid from user_authorized_acct where p_user_oid = ?  and a_account_oid =  ? " ;
			resultSet = DatabaseQueryBean.getXmlResultSet(sql,false,user.getAttribute("user_oid"),debit_account_oid);

			if (resultSet == null)
			{
			    return null;
			}

	        if (InstrumentType.XFER_BET_ACCTS.equals(instrument.getAttribute("instrument_type_code")))
	        {
				sql = "select authorized_account_oid from user_authorized_acct where p_user_oid = ? and a_account_oid = ? " ;

				resultSet = DatabaseQueryBean.getXmlResultSet(sql,false,user.getAttribute("user_oid"),credit_account_oid);

				if (resultSet == null)
				{
				    return null;
				}
			}

	        //IAZ IR PSUJ082545588 08/28/09 Begin
	      //SureshL R91 IR T36000026319 - SQL INJECTION FIX
	        if (InstrumentType.FUNDS_XFER.equals(instrument.getAttribute("instrument_type_code")))
	        {
				sql = "select authorized_account_oid from user_authorized_acct where p_user_oid = ? and a_account_oid = ? " ;
				resultSet = DatabaseQueryBean.getXmlResultSet(sql,false,user.getAttribute("user_oid"),payment_charges_account_oid);


				if (resultSet == null)
				{
				    return null;
				}
			}
			//IAZ IR PSUJ082545588 08/28/09 End
      }

		return userOrgOid;
 }
  /**
   *
   * @param accountsCorpOrg
   * @param testAccountOid
   * @return
   * @throws AmsException
   * @throws RemoteException
   */
	private static Account validateAccount(CorporateOrganization accountsCorpOrg, String testAccountOid)
  throws AmsException, RemoteException
	  {

			Account aAccount = (Account) accountsCorpOrg.getComponentHandle("AccountList", Long.parseLong(testAccountOid));
			if (TradePortalConstants.INDICATOR_YES.equals(aAccount.getAttribute("deactivate_indicator")))
			{
				LOG.debug("PanelAuthUtility::validateAccount()::Account with accountNumber: {} is deactivated,",aAccount.getAttribute("account_number"));
			}
			return aAccount;
	  }

	/**
	 *
	 * @param instrumentType
	 * @return
	 * @throws RemoteException
	 */
	private static boolean isCashManagementTypeInstrument(String instrumentType)
   	throws RemoteException
   {
	   if (instrumentType.equals(InstrumentType.DOM_PMT) ||
       	 instrumentType.equals(InstrumentType.FUNDS_XFER) ||
       	 instrumentType.equals(InstrumentType.XFER_BET_ACCTS))
       {
       		return true;
	   }
	   else
	   {
		   return false;
	   }
   }

	/**
	 *
	 * @param userOid
	 * @param locale
	 * @throws RemoteException
	 * @throws AmsException
	 */
	public void getPanelLevelAliases(String userOid, String locale) throws RemoteException, AmsException{

		CorporateOrganizationBean corpOrgBean = new CorporateOrganizationBean();
		//MEer Rel 8.3 IR-21992- Updated method call with ignoreDefaultPanelValue
		boolean ignoreDefaultPanelValue = false;
		//MEer Rel 8.3 IR-19699- Updated method call with locale as parameter
		this.panelAliasesMap = corpOrgBean.getPanelLevelAliases(userOid, locale, ignoreDefaultPanelValue);
		this.locale = locale;
		//Fetch refData values for Approver Sequence
		try {
			this.refData = ReferenceDataManager.getRefDataMgr().getAllCodeAndDescr("PANEL_AUTH_TYPE", this.locale);
		} catch (AmsException e) {
			LOG.error("WidgetFactory createSortedOptions[] - Error in fetching RefData Values: ",e);
		}
	}

	/**
	 *
	 * @param panelCode
	 * @return
	 */
	public String getAliasName(String panelCode){

		String panelName = null;
		if(StringFunction.isNotBlank(panelCode)){
		    if(!("").equals(this.panelAliasesMap.get(panelCode).toString())){
		    	panelName = this.panelAliasesMap.get(panelCode).toString();
		    }else{
		    	panelName = this.refData.get(panelCode).toString();
		    }
		}else{
			panelName = "";
		}

		return panelName;
	}

	/**
	 *
	 * @param instrType
	 * @param corpOrg
	 * @param errorMgr
	 * @return
	 * @throws NumberFormatException
	 * @throws RemoteException
	 * @throws AmsException
	 */
	public static String getPanelAuthorizationGroupOid(String instrType, CorporateOrganization corpOrg, ErrorManager errorMgr)
	throws NumberFormatException, RemoteException, AmsException{
		return getPanelAuthorizationGroupOid(instrType, corpOrg, errorMgr, null, null, null, null);
	}

	/**
    *
    * @param instrType instrument type to search panel group
    * @param inputDoc form input data
    * @param corpOrg  Corporate Organization
    * @param mediatorServices
    * @return PanelAuthorizationGroup it is panel group for instrument type
    * @throws NumberFormatException
    * @throws RemoteException
    * @throws AmsException
    */
   public static String getPanelAuthorizationGroupOid(String instrType, CorporateOrganization corpOrg, ErrorManager errorMgr, String transType, String paymentMethodType, String confInd,  String debitAcctOid )
 throws NumberFormatException, RemoteException,
			AmsException {

		String panelGroupOid = null;

		if (InstrumentServices.isTradeTypeInstrument(instrType)) {

			if ((TransactionType.DISCREPANCY.equals(transType) || TransactionType.APPROVAL_TO_PAY_RESPONSE.equals(transType))
					&& !(TradePortalConstants.DUAL_AUTH_REQ_INSTRUMENT_DEFAULTS.equals(corpOrg.getAttribute("dual_auth_disc_response")))) {
				panelGroupOid = corpOrg.getAttribute("dcr_panel_group_oid");
			} else if ((TransactionType.SIM.equals(transType) || TransactionType.SIR.equals(transType))
					&& !(TradePortalConstants.DUAL_AUTH_REQ_INSTRUMENT_DEFAULTS.equals(corpOrg.getAttribute("dual_settle_instruction")))) {
				panelGroupOid = corpOrg.getAttribute("sit_panel_group_oid");
			} else {
				if ( InstrumentType.DOCUMENTARY_BA.equals(instrType) || InstrumentType.DEFERRED_PAY.equals(instrType) ) {
					instrType = InstrumentType.IMPORT_DLC;
				} else if ( InstrumentType.IMPORT_COL.equals(instrType) ) {
					instrType = TradePortalConstants.IMP_COL;
				}
				panelGroupOid = corpOrg.getAttribute(instrType.toLowerCase() + "_panel_group_oid");
			}

		} else if (TradePortalConstants.NEW_RECEIVABLES.equals(instrType)) {
			panelGroupOid = corpOrg.getAttribute("mtch_apprv_panel_group_oid");

		} else if (TradePortalConstants.INVOICE.equals(instrType)) {
			panelGroupOid = corpOrg.getAttribute("inv_auth_panel_group_oid");

		} else if (TradePortalConstants.RECEIVABLES_CREDIT_NOTE.equals(instrType)) {
			panelGroupOid = corpOrg.getAttribute("credit_auth_panel_group_oid");

		} else if (TradePortalConstants.SUPPLIER_PORTAL.equals(instrType)) {
			panelGroupOid = corpOrg.getAttribute("supplier_panel_group_oid");
		}
		//CR913 start
		 else if (TradePortalConstants.PAYABLES_PROGRAM_INVOICE.equals(instrType)) {
				panelGroupOid = corpOrg.getAttribute("upl_pay_inv_panel_group_oid");
		 }
		 else if (TradePortalConstants.PAYABLES_MANAGEMENT_INVOICE.equals(instrType)) {
				panelGroupOid = corpOrg.getAttribute("pay_inv_panel_group_oid");
		 }
		//CR913 end
		 else if (TradePortalConstants.PAYABLES_CREDIT_NOTE.equals(instrType)) {
				panelGroupOid = corpOrg.getAttribute("pay_cr_note_auth_panel_grp_oid");
		 }
		else if (InstrumentServices.isCashManagementTypeInstrument(instrType)) {
            // Nar IR-T36000020782 release 8.3.0.0 27 Sep 2013 Add  Begin
			boolean isPanelGruopAvlForAcct = false;
			if (StringFunction.isNotBlank(debitAcctOid)) {
				Account account = (Account) EJBObjectFactory.createClientEJB(ejbServerLocation, "Account", Long.parseLong(debitAcctOid));
				for (int groupId = 1; groupId <= TradePortalConstants.NUMBER_OF_PANEL_GROUP_FOR_ACCT; groupId++) {
					String attributeName = "panel_auth_group_oid_" + groupId;
					if (StringFunction.isNotBlank(account.getAttribute(attributeName))) {
						isPanelGruopAvlForAcct = true;
						break;
					}
				}
				removeEJBReference(account); // clear client EJB reference
			}
			// Nar IR-T36000020782 release 8.3.0.0 27 Sep 2013 Add  End
			if (isPanelGruopAvlForAcct) {
				//LSuresh R91 IR T36000026319 - SQLINJECTION FIX
				 List<Object> sqlParams = new ArrayList();
				StringBuilder sql = new StringBuilder("SELECT pnl.panel_auth_group_oid FROM panel_auth_group pnl, account act WHERE ");
				// NAR Release8.3.0.0 24-Sep-2013 IR-T36000020312 DELETE - Begin
				// once Panel group is assigned to account, then it is related
				// to account. we should not check panel group owner org.
				// NAR Release8.3.0.0 24-Sep-2013 IR-T36000020312 DELETE - End
				sql.append(" pnl.instrument_group_type = ? ");
				sqlParams.add( TradePortalConstants.PAYMENT_INSTR_GROUP_TYPE);

				//jgadela 10/09/2014 R91 IR T36000033212 [START] - Panel authorization not found in international payments
				if (InstrumentType.DOMESTIC_PMT.equals(instrType) && StringFunction.isNotBlank(paymentMethodType)) {
					sql.append(" AND pnl.").append(paymentMethodType).append("_Pymt_Method_Ind = ? ");
					sqlParams.add("Y");
				} else if (InstrumentType.XFER_BET_ACCTS.equals(instrType)) {
					sql.append(" AND pnl.").append(instrType).append("_Pymt_Method_Ind = ? ");
					sqlParams.add("Y");
				}

				if (InstrumentType.FUNDS_XFER.equals(instrType)) {
					sql.append(" AND pnl.payment_type = ? ");
					sqlParams.add(TradePortalConstants.INT_PANEL_GROUP_TYPE);
				} else {
					sql.append(" AND pnl.payment_type = ? ");
					sqlParams.add(TradePortalConstants.PAYMENT_PANEL_GROUP_TYPE);
				}

				if (TradePortalConstants.INDICATOR_YES.equals(confInd)) {
					sql.append(" AND pnl.Panelgroupconfpaymentinst = ? ");
					sqlParams.add("Y");
				} else {
					sql.append(" AND pnl.Panelgroupconfpaymentinst != ? ");
					sqlParams.add("Y");
				}
				//jgadela 10/09/2014 R91 IR T36000033212 [END]

				sql.append(" AND act.account_oid = ?");
				sqlParams.add(debitAcctOid);
				sql.append(" AND ( ");
				sql.append("pnl.panel_auth_group_oid = act.a_panel_auth_group_oid_1 OR ");
				sql.append("pnl.panel_auth_group_oid = act.a_panel_auth_group_oid_2 OR ");
				sql.append("pnl.panel_auth_group_oid = act.a_panel_auth_group_oid_3 OR ");
				sql.append("pnl.panel_auth_group_oid = act.a_panel_auth_group_oid_4 OR ");
				sql.append("pnl.panel_auth_group_oid = act.a_panel_auth_group_oid_5 OR ");
				sql.append("pnl.panel_auth_group_oid = act.a_panel_auth_group_oid_6 OR ");
				sql.append("pnl.panel_auth_group_oid = act.a_panel_auth_group_oid_7 OR ");
				sql.append("pnl.panel_auth_group_oid = act.a_panel_auth_group_oid_8 OR ");
				sql.append("pnl.panel_auth_group_oid = act.a_panel_auth_group_oid_9 OR ");
				sql.append("pnl.panel_auth_group_oid = act.a_panel_auth_group_oid_10 OR ");
				sql.append("pnl.panel_auth_group_oid = act.a_panel_auth_group_oid_11 OR ");
				sql.append("pnl.panel_auth_group_oid = act.a_panel_auth_group_oid_12 OR ");
				sql.append("pnl.panel_auth_group_oid = act.a_panel_auth_group_oid_13 OR ");
				sql.append("pnl.panel_auth_group_oid = act.a_panel_auth_group_oid_14 OR ");
				sql.append("pnl.panel_auth_group_oid = act.a_panel_auth_group_oid_15 OR ");
				sql.append("pnl.panel_auth_group_oid = act.a_panel_auth_group_oid_16 ");
				sql.append(")");
				DocumentHandler resultXML = DatabaseQueryBean.getXmlResultSet(sql.toString(),false,sqlParams);
				if (resultXML == null) {
					String instrumentTypeDesc = ReferenceDataManager.getRefDataMgr().getDescr(
							TradePortalConstants.INSTRUMENT_TYPE,
							instrType, errorMgr.getLocaleName());
					if (TradePortalConstants.INDICATOR_YES.equals(confInd)) {
						if (InstrumentType.DOM_PMT.equals(instrType)) {
							errorMgr.issueError(
									TradePortalConstants.ERR_CAT_1,
									TradePortalConstants.PANEL_AUTH_NOT_FOR_CONF_FTDP_OR_FTBA,
									paymentMethodType);
						} else if (InstrumentType.XFER_BET_ACCTS.equals(instrType)) {
							errorMgr.issueError(
									TradePortalConstants.ERR_CAT_1,
									TradePortalConstants.PANEL_AUTH_NOT_FOR_CONF_FTDP_OR_FTBA,
									instrumentTypeDesc); // Nar IR-T36000022081 display TBA instead of FTBA
						} else if (InstrumentType.FUNDS_XFER.equals(instrType)) {
							errorMgr.issueError(
									TradePortalConstants.ERR_CAT_1,
									TradePortalConstants.PANEL_AUTH_NOT_FOR_CONF_INTERNATIONAL);
						}

					} else {
						if (InstrumentType.DOM_PMT.equals(instrType)) {
							errorMgr.issueError(
									TradePortalConstants.ERR_CAT_1,
									TradePortalConstants.PANEL_AUTH_NOT_FOR_FTDP_OR_FTBA,
									paymentMethodType);
						} else if (InstrumentType.XFER_BET_ACCTS.equals(instrType)) {
							errorMgr.issueError(
									TradePortalConstants.ERR_CAT_1,
									TradePortalConstants.PANEL_AUTH_NOT_FOR_FTDP_OR_FTBA,
									instrumentTypeDesc); // Nar IR-T36000022081 display TBA instead of FTBA
						} else if (InstrumentType.FUNDS_XFER.equals(instrType)) {
							errorMgr.issueError(
									TradePortalConstants.ERR_CAT_1,
									TradePortalConstants.PANEL_AUTH_NOT_FOR_INTERNATIONAL);
						}

					}

				} else if ((resultXML.getFragments("/ResultSetRecord")).size() > 1) {
					String instrumentTypeDesc = ReferenceDataManager.getRefDataMgr().getDescr(
									TradePortalConstants.INSTRUMENT_TYPE,
									instrType, errorMgr.getLocaleName());
					errorMgr.issueError(TradePortalConstants.ERR_CAT_1,
							TradePortalConstants.MULTIPLE_PANEL_GROUP_FOUND,
							instrumentTypeDesc);

				} else {
					panelGroupOid = resultXML.getAttribute("/ResultSetRecord(0)/PANEL_AUTH_GROUP_OID");
				}
			} else {
				errorMgr.issueError(TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.NO_PANEL_GROUP_SET_FOR_DEB_ACCOUNT); // Nar IR-T36000020782
			}
		}

		return panelGroupOid;
	}
	/**
	 * determine the panel range for the given panel auth group and amount
	 * return null if none found
	 * @param panelGroupOid
	 * @param amountInBase
	 * @return
	 * @throws AmsException
	 */
   //SureshL R91 IR T36000026319 - SQL INJECTION FIX
	public static String getPanelRange(String panelGroupOid, BigDecimal amountInBase) throws AmsException {
          String panelAuthRangeOid = null;
          StringBuilder sqlStatement = new StringBuilder("SELECT par.panel_auth_range_oid FROM panel_auth_range par, panel_auth_group pag ");
          sqlStatement.append(" WHERE ( par.panel_auth_range_oid = pag.panel_range_1_oid OR ");
          sqlStatement.append(" par.panel_auth_range_oid = pag.panel_range_2_oid OR ");
          sqlStatement.append(" par.panel_auth_range_oid = pag.panel_range_3_oid OR ");
          sqlStatement.append(" par.panel_auth_range_oid = pag.panel_range_4_oid OR ");
          sqlStatement.append(" par.panel_auth_range_oid = pag.panel_range_5_oid OR ");
          sqlStatement.append(" par.panel_auth_range_oid = pag.panel_range_6_oid) AND (");
          sqlStatement.append(" par.panel_auth_range_min_amt <= ? ");
          sqlStatement.append(" AND par.panel_auth_range_max_amt >= ? )");
          sqlStatement.append(" AND pag.panel_auth_group_oid = ?");

          DocumentHandler resultXML = DatabaseQueryBean.getXmlResultSet(sqlStatement.toString(),false,amountInBase,amountInBase,panelGroupOid);
          if(resultXML != null){
        	  panelAuthRangeOid = resultXML.getAttribute("/ResultSetRecord(0)/PANEL_AUTH_RANGE_OID");
          }

		  return panelAuthRangeOid;
		}

	  /**
	 *
	 * @param transaction
	 * @param baseCurrency
	 * @param fromCurrency
	 * @param transactionOwnerOrgOid
	 * @param errMgr
	 * @return
	 * @throws RemoteException
	 * @throws AmsException
	 */
	public static BigDecimal getAmountToPanelGroup(String transferCurrency, String amount, String baseCurrency, String fromCurrency, String corp_org_oid,
			boolean isAccountCurrency, ErrorManager errMgr) throws RemoteException, AmsException {

		   BigDecimal debit_amt =null;

		   if(isAccountCurrency){
			   debit_amt = getdebitAmount(transferCurrency, amount, baseCurrency, fromCurrency, corp_org_oid, InstrumentType.DOM_PMT, errMgr);
		   }else{
			   debit_amt = getAmountInBaseCurrency(transferCurrency, amount, baseCurrency, corp_org_oid,
					   InstrumentType.DOM_PMT, null, false, errMgr);
		   }
		   return debit_amt;
	   }

	  /**
	 *
	 * @param transaction
	 * @param baseCurrency
	 * @param fromCurrency
	 * @param transactionOwnerOrgOid
	 * @param errMgr
	 * @return
	 * @throws RemoteException
	 * @throws AmsException
	 */
	public static BigDecimal getdebitAmount(String transferCurrency, String amount, String baseCurrency, String fromCurrency, String corp_org_oid, String instrumentType,
			ErrorManager errMgr) throws RemoteException, AmsException {

		   BigDecimal debit_amt =null;

		   if (transferCurrency.equals(fromCurrency)) {
			   debit_amt = new BigDecimal(amount).abs();
		   }
		   else if (transferCurrency.equals(baseCurrency)) {
			   debit_amt = getAmounInFXCurrency(fromCurrency, baseCurrency, amount, corp_org_oid, TradePortalConstants.USE_BUY_RATE, null, null, false, errMgr);


		   } else if (fromCurrency.equals(baseCurrency)) {
			   debit_amt = getAmountInBaseCurrency(transferCurrency, amount, baseCurrency, corp_org_oid, TradePortalConstants.USE_SELL_RATE, null, false, errMgr);
		   }
		   else {
			   BigDecimal amountInBase = getAmountInBaseCurrency(transferCurrency, amount, baseCurrency, corp_org_oid, TradePortalConstants.USE_SELL_RATE, null, false, errMgr);
			   debit_amt = getAmounInFXCurrency(fromCurrency, baseCurrency, amountInBase.toString(), corp_org_oid, TradePortalConstants.USE_BUY_RATE, null, null, false, errMgr);
		   }
		   return debit_amt;
	   }

	/**
	 *
	 * @param instrumentType
	 * @param paramOid - OID for TradeCash/RM/Supplier Portal/InvoiceGroup/InvoiceSummary
	 * @param userOid of Corporate customer
	 * @return Panel object
	 * @throws NumberFormatException
	 * @throws RemoteException
	 * @throws AmsException
	 */
	//MEer Rel 8.3 IR#23030 Changed method signature -added boolean for request from viewPanelAuthorisations dialog
	public Panel populatePanelDetails(String instrumentType, String paramOid, String userOid)
	throws NumberFormatException, RemoteException, AmsException {
		 return populatePanelDetails(instrumentType, paramOid, null, userOid, false);
	}


	/**
	 *
	 * @param instrumentType
	 * @param paramOid - OID for TradeCash/RM/Supplier Portal/InvoiceGroup/InvoiceSummary
	 * @param instrumentOid - for Trade and CM instruments
	 * @param userOid of Corporate Customer
	 * @return Panel Object
	 * @throws NumberFormatException
	 * @throws RemoteException
	 * @throws AmsException
	 */
	public Panel populatePanelDetails(String instrumentType, String paramOid, String instrumentOid, String userOid, boolean viewPanelAuthDialog)
	throws NumberFormatException, RemoteException, AmsException {

		boolean isBulkPayment = false;
		String instrumentTypeCode = "";
		String panelGroupOid = null;
		String panelRangeOid []= null;
		String panelOplockVal = null;
		String currency = null;
		String currencyBasis = null;
		String authHistory = "";
		HashMap<String , PanelRange> panelRangeMap = new HashMap<String , PanelRange>();
		PanelAuthorizationGroup panelAuthGroup = null;
		PanelAuthProcessor panelAuthProcessor = null;
		Account debitAccount = null;
		User user = (User) EJBObjectFactory.createClientEJB(ejbServerLocation, "User", Long.parseLong(userOid));
		String userOrgOid = null;
		boolean isGXSStraightThroughInd = false;
		CorporateOrganization corpOrg = null;


		if(instrumentType.equals("TradeCash")){
			Transaction transaction = (Transaction) EJBObjectFactory.createClientEJB(ejbServerLocation, "Transaction",
					   Long.parseLong(paramOid));
			Instrument instrument = null;
			if(StringFunction.isNotBlank(instrumentOid)){
				instrument = (Instrument) EJBObjectFactory.createClientEJB(ejbServerLocation, "Instrument",Long.parseLong(instrumentOid));
				instrumentTypeCode = instrument.getAttribute("instrument_type_code");
				corpOrg = (CorporateOrganization) EJBObjectFactory.createClientEJB(ejbServerLocation, "CorporateOrganization",
						Long.parseLong(instrument.getAttribute("corp_org_oid")));
			}
			

			//IR T36000020321 Rel 8.3 PMitnala
			if (isCashManagementTypeInstrument(instrumentTypeCode))
		    { 	//MEer Rel 8.3 IR#23030 for viewPanel authorisations dialog skip the account validation for the user
				if (viewPanelAuthDialog) isGXSStraightThroughInd=true;
				userOrgOid = instrument.getAttribute("corp_org_oid");//Rel8.3 IR#T36000023002 - Passing proper corp_org_oid even when logged in as Admin user
		    	debitAccount = getDebitAccount(transaction, instrument, user, userOrgOid, isGXSStraightThroughInd);
		    }
			if (InstrumentType.DOM_PMT.equals(instrumentTypeCode) && StringFunction.isNotBlank(transaction.getAttribute("bene_panel_auth_ind"))){
				if(TradePortalConstants.INDICATOR_YES.equals(transaction.getAttribute("bene_panel_auth_ind"))){
					isBulkPayment = true;
				}
			}
			panelGroupOid = transaction.getAttribute("panel_auth_group_oid");
			panelOplockVal = transaction.getAttribute("panel_oplock_val");
			// validating panel group is not deleted.
			panelAuthGroup = (PanelAuthorizationGroup) EJBObjectFactory.createClientEJB(ejbServerLocation, "PanelAuthorizationGroup");
		    try{
		    	 panelAuthGroup.find("panel_auth_group_oid", panelGroupOid);
		    }catch(AmsException exe){
		    	LOG.debug("Error while trying to find panelGroupOid: ",exe);
			     return null;
		    }
		    panelAuthProcessor = new PanelAuthProcessor(user, instrumentTypeCode, new ErrorManager());
			if(!isBulkPayment){
				panelRangeOid = new String[] { transaction.getAttribute("panel_auth_range_oid") };
				try {
					authHistory = panelAuthProcessor.retrieveAuthHistory(transaction);
				} catch (Exception e) {
					LOG.error("PanelAuthUtility::populatePanelDetails - Error retrieving authHistory: ",e);
				}
			}else{
				String dmstPmtPanelAuthRaneOid[] = null;
				DmstPmtPanelAuthRange dmstPmtPanelAuthRange = null;
				//LSuresh R91 IR T36000026319 - SQLINJECTION FIX
				StringBuilder sqlStatement = new StringBuilder(
						"SELECT a_panel_auth_range_oid, dmst_pmt_panel_auth_range_oid FROM dmst_pmt_panel_auth_range WHERE p_transaction_oid = ?");

				sqlStatement.append(" AND (auth_status IS NULL OR auth_status != 'AUTHORIZED')");
				DocumentHandler resultsDoc = DatabaseQueryBean.getXmlResultSet(sqlStatement.toString(),false,new Object[]{paramOid});

				if (resultsDoc != null) {
					Vector<DocumentHandler> rangeOidList = resultsDoc.getFragments("/ResultSetRecord");
					panelRangeOid = new String[rangeOidList.size()];
					dmstPmtPanelAuthRaneOid = new String[rangeOidList.size()];
					int i = 0;
					for (DocumentHandler doc : rangeOidList) {
						panelRangeOid[i] = doc.getAttribute("/A_PANEL_AUTH_RANGE_OID");
						dmstPmtPanelAuthRaneOid[i] = doc.getAttribute("/DMST_PMT_PANEL_AUTH_RANGE_OID");
						i++;
					}
					for (i = 0; i < panelRangeOid.length; i++) {
						dmstPmtPanelAuthRange = (DmstPmtPanelAuthRange) EJBObjectFactory.createClientEJB(ejbServerLocation, "DmstPmtPanelAuthRange",Long.parseLong(dmstPmtPanelAuthRaneOid[i]));
						try {
							authHistory = authHistory+panelAuthProcessor.retrieveAuthHistory(dmstPmtPanelAuthRange);
							//cleanup ejb
							removeEJBReference(dmstPmtPanelAuthRange);
						} catch (Exception e) {
							LOG.error("PanelAuthUtility::populatePanelDetails - Error retrieving authHistory: ",e);
						}
					}
				}
			}
			//cleanup ejb
			removeEJBReference(transaction);
			removeEJBReference(instrument);
		}else if(instrumentType.equals("RM")){
			PayRemit payRemit = (PayRemit) EJBObjectFactory.createClientEJB(ejbServerLocation, "PayRemit",
					   Long.parseLong(paramOid));
			corpOrg = (CorporateOrganization) EJBObjectFactory.createClientEJB(ejbServerLocation, "CorporateOrganization",
					Long.parseLong(payRemit.getAttribute("seller_corp_org_oid")));
			panelGroupOid = payRemit.getAttribute("panel_auth_group_oid");
			panelOplockVal = payRemit.getAttribute("panel_oplock_val");
			panelRangeOid = new String[] { payRemit.getAttribute("panel_auth_range_oid") };
			if (StringFunction.isBlank(panelGroupOid)) {
				panelGroupOid = corpOrg.getAttribute("mtch_apprv_panel_group_oid");
			}
			panelAuthGroup = (PanelAuthorizationGroup) EJBObjectFactory.createClientEJB(ejbServerLocation, "PanelAuthorizationGroup",
					Long.parseLong(panelGroupOid));
			if (StringFunction.isBlank(panelOplockVal)) {
				panelOplockVal = panelAuthGroup.getAttribute("opt_lock");
			}
			panelAuthProcessor = new PanelAuthProcessor(user, TradePortalConstants.NEW_RECEIVABLES, new ErrorManager());
			try {
				authHistory = panelAuthProcessor.retrieveAuthHistory(payRemit);
			} catch (Exception e) {
				LOG.error("PanelAuthUtility::populatePanelDetails - Error retrieving authHistory: ",e);
			}
			removeEJBReference(payRemit);
		}else if(instrumentType.equals(TradePortalConstants.SUPPLIER_PORTAL)){
			InvoiceOfferGroup invoiceOfferGroup = (InvoiceOfferGroup) EJBObjectFactory.createClientEJB(ejbServerLocation, "InvoiceOfferGroup",
					   Long.parseLong(paramOid));
			corpOrg = (CorporateOrganization) EJBObjectFactory.createClientEJB(ejbServerLocation, "CorporateOrganization",
					Long.parseLong(invoiceOfferGroup.getAttribute("corp_org_oid")));
			panelGroupOid = invoiceOfferGroup.getAttribute("panel_auth_group_oid");
			panelOplockVal = invoiceOfferGroup.getAttribute("panel_oplock_val");
			panelRangeOid = new String[] { invoiceOfferGroup.getAttribute("panel_auth_range_oid") };
			if (StringFunction.isBlank(panelGroupOid)) {
				panelGroupOid = corpOrg.getAttribute("supplier_panel_group_oid");
			}
			panelAuthGroup = (PanelAuthorizationGroup) EJBObjectFactory.createClientEJB(ejbServerLocation, "PanelAuthorizationGroup",
					Long.parseLong(panelGroupOid));
			if (StringFunction.isBlank(panelOplockVal)) {
				panelOplockVal = panelAuthGroup.getAttribute("opt_lock");
			}
			panelAuthProcessor = new PanelAuthProcessor(user, TradePortalConstants.SUPPLIER_PORTAL, new ErrorManager());
			try {
				authHistory = panelAuthProcessor.retrieveAuthHistory(invoiceOfferGroup);
			} catch (Exception e) {
				LOG.error("PanelAuthUtility::populatePanelDetails - Error retrieving authHistory-", e);
			}
			removeEJBReference(invoiceOfferGroup);
		}else if(instrumentType.equals("INV_SUMMARY")){
			InvoicesSummaryData invoicesSummaryData = (InvoicesSummaryData) EJBObjectFactory.createClientEJB(ejbServerLocation, "InvoicesSummaryData",
					   Long.parseLong(paramOid));
			corpOrg = (CorporateOrganization) EJBObjectFactory.createClientEJB(ejbServerLocation, "CorporateOrganization",
					Long.parseLong(invoicesSummaryData.getAttribute("corp_org_oid")));
			panelGroupOid = invoicesSummaryData.getAttribute("panel_auth_group_oid");
			panelOplockVal = invoicesSummaryData.getAttribute("panel_oplock_val");
			panelRangeOid = new String[] { invoicesSummaryData.getAttribute("panel_auth_range_oid") };
			if (StringFunction.isBlank(panelGroupOid)) {
				panelGroupOid = corpOrg.getAttribute("inv_auth_panel_group_oid");
			}
			panelAuthGroup = (PanelAuthorizationGroup) EJBObjectFactory.createClientEJB(ejbServerLocation, "PanelAuthorizationGroup",
					Long.parseLong(panelGroupOid));
			panelAuthProcessor = new PanelAuthProcessor(user, TradePortalConstants.INVOICE, new ErrorManager());
			try {
				authHistory = panelAuthProcessor.retrieveAuthHistory(invoicesSummaryData);
			} catch (Exception e) {
				LOG.error("PanelAuthUtility::populatePanelDetails - Error retrieving authHistory-",e);
			}
			removeEJBReference(invoicesSummaryData);
		}else if(instrumentType.equals("INV_GROUP")){
			InvoiceGroup invoiceGroup = (InvoiceGroup) EJBObjectFactory.createClientEJB(ejbServerLocation, "InvoiceGroup",
					   Long.parseLong(paramOid));
			corpOrg = (CorporateOrganization) EJBObjectFactory.createClientEJB(ejbServerLocation, "CorporateOrganization",
					Long.parseLong(invoiceGroup.getAttribute("corp_org_oid")));
			panelGroupOid = invoiceGroup.getAttribute("panel_auth_group_oid");
			panelOplockVal = invoiceGroup.getAttribute("panel_oplock_val");
			panelRangeOid = new String[] { invoiceGroup.getAttribute("panel_auth_range_oid") };
			if (StringFunction.isBlank(panelGroupOid)) {
				panelGroupOid = corpOrg.getAttribute("inv_auth_panel_group_oid");
			}
			panelAuthGroup = (PanelAuthorizationGroup) EJBObjectFactory.createClientEJB(ejbServerLocation, "PanelAuthorizationGroup",
					Long.parseLong(panelGroupOid));
			if (StringFunction.isBlank(panelOplockVal)) {
				panelOplockVal = panelAuthGroup.getAttribute("opt_lock");
			}
			panelAuthProcessor = new PanelAuthProcessor(user, TradePortalConstants.INVOICE, new ErrorManager());
			try {
				authHistory = panelAuthProcessor.retrieveAuthHistory(invoiceGroup);
			} catch (Exception e) {
				LOG.error("PanelAuthUtility::populatePanelDetails - Error retrieving authHistory-",e);
			}
			removeEJBReference(invoiceGroup);
		}
		//CR913 start
		else if(instrumentType.equals("PAY_INV_GROUP")){
			InvoiceGroup invoiceGroup = (InvoiceGroup) EJBObjectFactory.createClientEJB(ejbServerLocation, "InvoiceGroup",
					   Long.parseLong(paramOid));
			corpOrg = (CorporateOrganization) EJBObjectFactory.createClientEJB(ejbServerLocation, "CorporateOrganization",
					Long.parseLong(invoiceGroup.getAttribute("corp_org_oid")));
			panelGroupOid = invoiceGroup.getAttribute("panel_auth_group_oid");
			panelOplockVal = invoiceGroup.getAttribute("panel_oplock_val");
			panelRangeOid = new String[] { invoiceGroup.getAttribute("panel_auth_range_oid") };
			if (StringFunction.isBlank(panelGroupOid)) {
				panelGroupOid = corpOrg.getAttribute("upl_pay_inv_panel_group_oid");
			}
			panelAuthGroup = (PanelAuthorizationGroup) EJBObjectFactory.createClientEJB(ejbServerLocation, "PanelAuthorizationGroup",
					Long.parseLong(panelGroupOid));
			if (StringFunction.isBlank(panelOplockVal)) {
				panelOplockVal = panelAuthGroup.getAttribute("opt_lock");
			}
			panelAuthProcessor = new PanelAuthProcessor(user, TradePortalConstants.INVOICE, new ErrorManager());
			try {
				authHistory = panelAuthProcessor.retrieveAuthHistory(invoiceGroup);
			} catch (Exception e) {
				LOG.error("PanelAuthUtility::populatePanelDetails - Error retrieving authHistory- ",e);
			}
			removeEJBReference(invoiceGroup);
		}
		else if(instrumentType.equals("PAY_INV_SUMMARY")){
			InvoicesSummaryData invoicesSummaryData = (InvoicesSummaryData) EJBObjectFactory.createClientEJB(ejbServerLocation, "InvoicesSummaryData",
					   Long.parseLong(paramOid));
			corpOrg = (CorporateOrganization) EJBObjectFactory.createClientEJB(ejbServerLocation, "CorporateOrganization",
					Long.parseLong(invoicesSummaryData.getAttribute("corp_org_oid")));
			panelGroupOid = invoicesSummaryData.getAttribute("panel_auth_group_oid");
			panelOplockVal = invoicesSummaryData.getAttribute("panel_oplock_val");
			panelRangeOid = new String[] { invoicesSummaryData.getAttribute("panel_auth_range_oid") };
			if (StringFunction.isBlank(panelGroupOid)) {
				panelGroupOid = corpOrg.getAttribute("upl_pay_inv_panel_group_oid");
			}
			panelAuthGroup = (PanelAuthorizationGroup) EJBObjectFactory.createClientEJB(ejbServerLocation, "PanelAuthorizationGroup",
					Long.parseLong(panelGroupOid));
			panelAuthProcessor = new PanelAuthProcessor(user, TradePortalConstants.INVOICE, new ErrorManager());
			try {
				authHistory = panelAuthProcessor.retrieveAuthHistory(invoicesSummaryData);
			} catch (Exception e) {
				LOG.error("PanelAuthUtility::populatePanelDetails - Error retrieving authHistory-",e);
			}
			removeEJBReference(invoicesSummaryData);
		}
		else if(instrumentType.equals("PAY_INV")){
			Invoice invoice = (Invoice) EJBObjectFactory.createClientEJB(ejbServerLocation, "Invoice",
					   Long.parseLong(paramOid));
			corpOrg = (CorporateOrganization) EJBObjectFactory.createClientEJB(ejbServerLocation, "CorporateOrganization",
					Long.parseLong(invoice.getAttribute("corp_org_oid")));
			panelGroupOid = invoice.getAttribute("panel_auth_group_oid");
			panelOplockVal = invoice.getAttribute("panel_oplock_val");
			panelRangeOid = new String[] { invoice.getAttribute("panel_auth_range_oid") };
			if (StringFunction.isBlank(panelGroupOid)) {
				panelGroupOid = corpOrg.getAttribute("pay_inv_panel_group_oid");
			}
			panelAuthGroup = (PanelAuthorizationGroup) EJBObjectFactory.createClientEJB(ejbServerLocation, "PanelAuthorizationGroup",
					Long.parseLong(panelGroupOid));
			panelAuthProcessor = new PanelAuthProcessor(user, TradePortalConstants.INVOICE, new ErrorManager());
			try {
				authHistory = panelAuthProcessor.retrieveAuthHistory(invoice);
			} catch (Exception e) {
				LOG.error("PanelAuthUtility::populatePanelDetails - Error retrieving authHistory- ",e);
			}
			removeEJBReference(invoice);
		}
		else if(instrumentType.equals("CREDIT_NOTES")){
			CreditNotes creditNotes = (CreditNotes) EJBObjectFactory.createClientEJB(ejbServerLocation, "CreditNotes",
					   Long.parseLong(paramOid));
			corpOrg = (CorporateOrganization) EJBObjectFactory.createClientEJB(ejbServerLocation, "CorporateOrganization",
					Long.parseLong(creditNotes.getAttribute("corp_org_oid")));
			panelGroupOid = creditNotes.getAttribute("panel_auth_group_oid");
			panelOplockVal = creditNotes.getAttribute("panel_oplock_val");
			panelRangeOid = new String[] { creditNotes.getAttribute("panel_auth_range_oid") };
			if (StringFunction.isBlank(panelGroupOid)) {
				panelGroupOid = corpOrg.getAttribute("pay_inv_panel_group_oid");
			}
			panelAuthGroup = (PanelAuthorizationGroup) EJBObjectFactory.createClientEJB(ejbServerLocation, "PanelAuthorizationGroup",
					Long.parseLong(panelGroupOid));
			panelAuthProcessor = new PanelAuthProcessor(user, TradePortalConstants.INVOICE, new ErrorManager());
			try {
				authHistory = panelAuthProcessor.retrieveAuthHistory(creditNotes);
			} catch (Exception e) {
				LOG.error("PanelAuthUtility::populatePanelDetails - Error retrieving authHistory- ",e);
			}
			removeEJBReference(creditNotes);
		}
		//CR913 end
		Panel panelObj = new Panel(panelGroupOid, panelRangeOid, panelOplockVal, instrumentType, new ErrorManager());
		panelRangeMap = panelObj.getPanelRangeMap();

		HashMap panelApproversSummaryMap = panelObj.getNextPanelApproverList(authHistory, null);
		Map<String, String> panelApproversSummarySortedMap = new TreeMap<String, String>(panelApproversSummaryMap);

		if(panelAuthGroup!=null)
		currencyBasis = panelAuthGroup.getAttribute("ccy_basis");

		if(TradePortalConstants.PANEL_GROUP_ACCT_CURR_BASIS.equals(currencyBasis)){
			if(debitAccount != null){
				currency = debitAccount.getAttribute("currency");
			}
		}else{
			if(debitAccount != null){
				currency = getBaseCurrency(corpOrg, panelAuthGroup, debitAccount, instrumentTypeCode);
			}			
		}
		setCurrency(currency);
		setCurrencyBasis(currencyBasis);
		setBulkPayment(isBulkPayment);
		setAuthHistory(authHistory);
		setPanelAppSummaryMap(panelApproversSummarySortedMap);

		//cleanup ejb
		removeEJBReference(user);
		removeEJBReference(corpOrg);
		removeEJBReference(panelAuthGroup);
		return panelObj;
	}





	public static boolean isTransactionAuthInProcess(String transactionStatus){

		boolean isInProcess = true;

		isInProcess = TransactionStatus.READY_TO_AUTHORIZE.equals(transactionStatus) ||
		              TransactionStatus.PARTIALLY_AUTHORIZED.equals(transactionStatus) ||
		              TransactionStatus.AUTHORIZE_FAILED.equals(transactionStatus) ||
		              TransactionStatus.TRANS_STATUS_REQ_AUTHENTICTN.equals(transactionStatus) ||
		              TransactionStatus.FX_THRESH_EXCEEDED.equals(transactionStatus) ||
		              TransactionStatus.AUTH_PEND_MARKET_RATE.equals(transactionStatus);

		return isInProcess;
	}

	/**
	 * This method returns the awaiting approval list for given ranges of transaction.
	 *
	 * @param dmstPymtPanelRangeMap
	 * @param trans
	 * @param instrumentType
	 * @param errorMgr
	 * @return
	 * @throws AmsException
	 * @throws RemoteException
	 */
	public static String getPanelAuthAwaitingApprovalForBene(Map<String, String> dmstPymtPanelRangeMap, Transaction trans, String instrumentType, ErrorManager errorMgr)
	throws RemoteException, AmsException{

		String[] dmstPymtPanelRangeStr = dmstPymtPanelRangeMap.keySet().toArray(new String[0]);
		// getting panel object to store next awaiting approval to in attribute panel_Auth_next_approval of transaction object
		Panel panelObj = new Panel(trans.getAttribute("panel_auth_group_oid"), dmstPymtPanelRangeStr, trans.getAttribute("panel_oplock_val"), instrumentType, errorMgr);
		Map panelApproversSummaryMap = panelObj.getNextPanelApproverList("", null);
		String panelApprovalsList = StringFunction.getMapKeyToStringFormat(panelApproversSummaryMap);
		return panelApprovalsList;
	}

/**
	 * This method returns the account object
	 * @param transaction
	 * @param instrument
	 * @param user
	 * @param userOrgOid
	 * @param isGXSStraightThroughInd
	 * @return
	 * @throws RemoteException
	 * @throws AmsException
	 */
	public Account getDebitAccount(Transaction transaction,Instrument instrument,User user, String userOrgOid, boolean isGXSStraightThroughInd) throws RemoteException, AmsException{

		Account debitAccount = null;
		String accountsCorpOrgOid = null;
		StringBuilder sql = new StringBuilder();
		String debit_account_oid = null;
		String credit_account_oid  = null;
		String payment_charges_account_oid = null;

		sql.append("SELECT debit_account_oid, credit_account_oid, payment_charges_account_oid FROM Terms WHERE terms_oid= ?");
		//SureshL R91 IR T36000026319 - SQL INJECTION FIX

		DocumentHandler resultXML = DatabaseQueryBean.getXmlResultSet(sql.toString(),false,new Object[]{transaction.getAttribute("c_CustomerEnteredTerms")});
		if(resultXML!=null ){
			debit_account_oid = resultXML.getAttribute("/ResultSetRecord(0)/DEBIT_ACCOUNT_OID");
			credit_account_oid = resultXML.getAttribute("/ResultSetRecord(0)/CREDIT_ACCOUNT_OID");
			payment_charges_account_oid = resultXML.getAttribute("/ResultSetRecord(0)/PAYMENT_CHARGES_ACCOUNT_OID");
		}

		CorporateOrganization corpOrg = (CorporateOrganization) EJBObjectFactory.createClientEJB(ejbServerLocation, "CorporateOrganization",
				Long.parseLong(userOrgOid));
		if( corpOrg.isStraightThroughAuthorize() && transaction.isH2HSentPayment()){
			isGXSStraightThroughInd = true;
		}

			accountsCorpOrgOid = validateAuthorizedAccounts(user, transaction, instrument, userOrgOid, isGXSStraightThroughInd,
					debit_account_oid,credit_account_oid, payment_charges_account_oid);
			if(accountsCorpOrgOid != null)
	    	{
				CorporateOrganization accountsCorpOrg = (CorporateOrganization) EJBObjectFactory.createClientEJB(ejbServerLocation, "CorporateOrganization",
						Long.parseLong(accountsCorpOrgOid));
				if (StringFunction.isNotBlank(debit_account_oid))
				{
					debitAccount = validateAccount(accountsCorpOrg, debit_account_oid);
				}
	    	}
		removeEJBReference(corpOrg);
	    return debitAccount;
	}

	/**
	 * This method is used to clear the EJB object reference
	 * @param objRef
	 */
	protected static void removeEJBReference(EJBObject objRef){
		try{
			if (objRef != null)
			{
				objRef.remove();
				objRef = null;
			}
		}
		catch(Exception e){
			LOG.error("Exception While removing instance of EJB object", e);
		}
	}
}
