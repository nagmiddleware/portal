package com.ams.tradeportal.busobj.util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.PropertyResourceBundle;

   

/**
 * SupervisorCommandUtility reads data from supervisor_command table
 * and build text file to add users and groups in Supervisor.
 * Supervisor is invoked in command line mode with generated text file 
 * as input.
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved 
 * 
 */
public class SupervisorCommandUtility {
private static final Logger LOG = LoggerFactory.getLogger(SupervisorCommandUtility.class);
	
	
	
            //into SUPERVISOR_COMMAND table
            //Following code inserts new user in Supervisor
            //Command syntax - NG - new group, Group name , User ID
	public static String getBankGroupFirstCommand(String pclientbankOID, String orgOID)
	 {
            //rbhaduri - 13th May 09 - PPX-051 - added XI specific command
            return "BOG,NG,"+pclientbankOID+","+orgOID;
	 }


            //Command to add new group
            //SYNTAX NG new group, parent group, group id
	public static String getCorpOrgFirstCommand(String parentOID, String orgOID, String reportingType)
	 {
            
            //rbhaduri - 13th May 09 - PPX-051 - added XI specific command, also added reportingUserType argument
            return "CC,NG,"+parentOID+","+orgOID+","+reportingType;
	 }

            //Delete the corporate customer	
	public static String deleteCorpOrgUser(String corpOrgOID)
	 {
            return "DU,G"+corpOrgOID+","+corpOrgOID;
	 }


	public static String deleteCorpOrgGroup(String corpOrgOID)
	 {
       //rbhaduri - 26 May 10 - IR KSUK050849844 - put appropriate command to alter the reporting group for XI
	   return "CC,DG,"+corpOrgOID;
	 }
	
      //rbhaduri - 08 Jun 10 - IR KSUK050849844 - Added the following function to build the command for BO XI to change the parent
	//of one corporate customer to another
	public static String changeCorpOrgParent(String corpOrgOIDBeingChanged, String oldParentOrgOID, String newParentOrgOID)
	{
		return "CC,CP,"+corpOrgOIDBeingChanged+","+oldParentOrgOID+","+newParentOrgOID;
	} 

	 //rbhaduri - 08 Jun 10 - IR KSUK050849844 - Added the following function to build the command for BO XI to assign
	//parent of one corporate customer for the first time
	public static String assignCorpOrgParent(String corpOrgOIDBeingChanged, String newParentOrgOID)
	{
		return "CC,AP,"+corpOrgOIDBeingChanged+","+newParentOrgOID;
	} 

	
	
	//rbhaduri - 13th May 09 - PPX-051 - added the following function to alter the group of a user for XI
	public static String getAlterGroupCommand(String corpOrgOID, String clientBankOID,String reportingType, String oldreportingType)
	 {
              return "CC,AG,"+ clientBankOID + ","+ corpOrgOID + "," + reportingType + "," + oldreportingType;
	 }
	         
	

	
/**
 * This method retrieves universe paremeters from text file.
 * @return Hashtable - Universe paramaters list
 * @param void
 */	
public static Hashtable getUnivParameters()
{
    Hashtable univParams = new Hashtable(5);
    String key = null;
    
    
    // Information about the Universe is stored
    // in a properties file
    PropertyResourceBundle univParmFile = 
                  (PropertyResourceBundle)PropertyResourceBundle.getBundle ("Reports");
                    
    // Get the list of universe paramaters       
    Enumeration params = univParmFile.getKeys();
            
    // Loop through these universe paramaters, adding them to a hash table
    while(params.hasMoreElements())
    {
        key = (String) params.nextElement();
        univParams.put(key, univParmFile.getString(key));
    }   
     
    return univParams;
    
}


} //end
