package com.ams.tradeportal.busobj.util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import com.ams.tradeportal.busobj.webbean.SessionWebBean;
import com.ams.tradeportal.common.SQLParamFilter;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.EncryptDecrypt;
import com.amsinc.ecsg.util.StringFunction;

/**
 * This utlity class returns the pay remit invoice oid based on 
 * the invoice status.

 * @version 0.1
 */
public class RemittanceItemUtility {
private static final Logger LOG = LoggerFactory.getLogger(RemittanceItemUtility.class);
	
	 /**
	  * This method oid for the given set of payment remit invoice oids
	  * based on their status.	
	  * @param payRemitInvoiceOids
	  * @param remitOid
	  * @param userSession
	  * @return String
	  * @throws RemoteException
	  * @throws AmsException
	  */
	 public String getInvoiceRemittanceId(String payRemitInvoiceOids,String remitOid,SessionWebBean userSession) throws RemoteException,AmsException{
		  
		  String tobeReturned = null;
		  String payRemArray[] = null;
		  if(StringFunction.isNotBlank(payRemitInvoiceOids)){
			  payRemArray = payRemitInvoiceOids.split(",");
		  }
		  
		  if(StringFunction.isNotBlank(payRemitInvoiceOids)) {	 
			 List<Object> sqlParams = new ArrayList();
		   	 String sql = "select a_pay_remit_invoice_oid,invoice_matching_status from pay_match_result where "
		   	 		+ "a_pay_remit_invoice_oid in (" +SQLParamFilter.preparePlaceHolderStrForInClause(payRemitInvoiceOids, sqlParams)+") and p_pay_remit_oid = ?";
			
			 sqlParams.add(remitOid);
			 DocumentHandler result = DatabaseQueryBean.getXmlResultSet(sql,false,sqlParams);	
			 String matchAvailable = TradePortalConstants.INDICATOR_NO;
			 int matchCount = 0;
			 int unmatchCount = 0;
			 
			 if(result != null){
				   Vector remittances = result.getFragments("/ResultSetRecord");		  
				    for(int k=0;k<payRemArray.length;k++){
					matchAvailable = findInvoiceStatus(remittances,payRemArray[k]);
					if(TradePortalConstants.INDICATOR_YES.equals(matchAvailable)){
						matchCount++;
					}else{
						unmatchCount++;
					}
					if(TradePortalConstants.INDICATOR_NO.equals(matchAvailable)){
						tobeReturned = payRemArray[k];
					 break;
					 }
					}
					if(payRemArray.length == matchCount){
						tobeReturned = payRemArray[0];
					}
					if(payRemArray.length == unmatchCount){
						tobeReturned = payRemArray[0];
					}
			  }
			  if(StringFunction.isNotBlank(tobeReturned)){
					 tobeReturned = EncryptDecrypt.encryptStringUsingTripleDes(tobeReturned, userSession.getSecretKey());
				 }
		  }  
		 return tobeReturned;
	 }
	
	/**
	 * This method returns match found status based on invoice match status.
	 * @param docVec
	 * @param dbOid
	 * @return
	 */
	 private String findInvoiceStatus(Vector docVec, String dbOid) { 
			   DocumentHandler tempDoc = null;
			   String matchFound = TradePortalConstants.INDICATOR_NO;
			   String tempOid = null;
			   String status = null;
				  for(int i=0;i<docVec.size();i++){
				   tempDoc = (DocumentHandler) docVec.elementAt(i);		
				   tempOid = tempDoc.getAttribute("/A_PAY_REMIT_INVOICE_OID");
				   status = tempDoc.getAttribute("/INVOICE_MATCHING_STATUS");
				   if(dbOid.equals(tempOid)){					   
					if(TradePortalConstants.MATCHING_STATUS_MANUAL_MATCH.equals(status) || TradePortalConstants.MATCHING_STATUS_AUTO_MATCHED.equals(status) || 
										TradePortalConstants.MATCHING_STATUS_PARTIALLY_MATCHED.equals(status)){
						
						matchFound = TradePortalConstants.INDICATOR_YES;
					}
				  }
			    }
				return matchFound;
		   } 
}
