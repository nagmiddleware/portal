package com.ams.tradeportal.busobj.util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.util.*;
import java.rmi.*;
import com.ams.tradeportal.busobj.*;
import com.ams.tradeportal.common.*;
import com.amsinc.ecsg.frame.*;

/*
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class RouteServices {
private static final Logger LOG = LoggerFactory.getLogger(RouteServices.class);


    public RouteServices() {
	super();
    }

    /** 
      * This method is used by both RouteTransactionMediatorBean and 
      * RouteAndDeleteMessageBean to verify that the correct information 
      * is available and to determine the user to route to.
      * @param DocumentHandler inputDoc - the input document received by the mediator
      * @param MediatorServices mediatorServices
      * @param java.lang.StringBuffer routeUserOid - the user oid to be routed to.  This is '' data if 
      *                  the user does not exist and an error is found.  This is assumed to be a new empty StringBuffer.
      * @return java.lang.String - return TradePortalConstants.ROUTE_SELECTION_ERROR for Route Selection error, 
      * 			   return TradePortalConstants.ROUTE_GENERAL_ERROR for  for other errors and 
      * 			   return TradePortalConstants.ROUTE_SUCCESS for to continue processing with the return userOid
      * 	
      */
    public static String validateRouteData(DocumentHandler inputDoc, MediatorServices mediatorServices, StringBuffer routeToUserOid)
	throws AmsException, RemoteException
    {
      boolean	routeToUser	   = false;
      boolean	routeToOrg	   = false;
      String    routeCorporateOrgOid = null;
      CorporateOrganization   corporateOrg         = null;
      int                     rows                 = 0;
      String	routeUserOid = null;
      String    routeMultipleOrgs = null;

      routeToUser = inputDoc.getAttribute("/Route/routeToUser") != null &&
		    inputDoc.getAttribute("/Route/routeToUser").equals
		    (TradePortalConstants.INDICATOR_YES);

      routeToOrg  = inputDoc.getAttribute("/Route/routeToUser") != null &&
		    inputDoc.getAttribute("/Route/routeToUser").equals
		    (TradePortalConstants.INDICATOR_NO);
      routeUserOid = inputDoc.getAttribute("/Route/userOid");
      routeCorporateOrgOid = inputDoc.getAttribute("../corporateOrgOid");

      // check selection errors
      // for multiple related organizations, make sure that the radio button 
      // for the user or organization has been selected and if so, 
      // the corresponding dropdown has been selected.
      // for stand alone organizations, make sure that a user has been selected.

      routeMultipleOrgs = inputDoc.getAttribute("/Route/multipleOrgs");

      if (routeMultipleOrgs != null && routeMultipleOrgs.equals(TradePortalConstants.INDICATOR_YES))
      {
	if (routeToUser)
	{
	  if (routeUserOid == null || routeUserOid.equals(""))
	  {
	    mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, 
	    TradePortalConstants.SELECT_USER_TO_ROUTE);
	    return TradePortalConstants.ROUTE_SELECTION_ERROR;
          }
	}
	else if (routeToOrg)
	{
	  if (routeCorporateOrgOid == null || routeCorporateOrgOid.equals(""))
          {
	    mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, 
	    TradePortalConstants.SELECT_ORG_TO_ROUTE);
	    return TradePortalConstants.ROUTE_SELECTION_ERROR;
	  }
	}
	else
	{
	    mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, 
	    TradePortalConstants.SELECT_USER_OR_ORG_TO_ROUTE);
	    return TradePortalConstants.ROUTE_SELECTION_ERROR;
	}
      }
      else if (routeUserOid == null || routeUserOid.equals(""))
      {
	  mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, 
	  TradePortalConstants.SELECT_USER_TO_ROUTE);      
	  return TradePortalConstants.ROUTE_SELECTION_ERROR;
      }


      // determine if routing is based on the given org's default user oid
      // or a given user oid; if user oid is given, use it, otherwise, find 
      // the given org's default user oid
      if (routeToOrg)
      {

         corporateOrg = (CorporateOrganization) mediatorServices.createServerEJB("CorporateOrganization");
		
	 try
	 {
            corporateOrg.getData(Long.parseLong(routeCorporateOrgOid));
	 }
	 catch(com.amsinc.ecsg.frame.InvalidObjectIdentifierException e)
	 {
            mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, 
                                                          TradePortalConstants.ORG_NOT_EXIST);
	    LOG.info("Object not found exception called for {}",routeCorporateOrgOid);
	    return TradePortalConstants.ROUTE_GENERAL_ERROR;
         }

         routeUserOid = corporateOrg.getAttribute("default_user_oid");

         if (routeUserOid == null || routeUserOid.equals(""))
         {
            mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, 
                                                          TradePortalConstants.ORG_NO_DEFAULT_USER);

            return TradePortalConstants.ROUTE_GENERAL_ERROR;
         }
      }


      // routeUserOid is expected to be filled now; check the validity of the user oid
      rows = DatabaseQueryBean.getCount("user_oid", "USERS", "user_oid = ? and activation_status=? ", false, new Object[]{routeUserOid,TradePortalConstants.ACTIVE});

      if (rows <= 0)
      {
         mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, 
                                                       TradePortalConstants.USER_NOT_EXIST);

         return TradePortalConstants.ROUTE_GENERAL_ERROR;
      }

    routeToUserOid.append(routeUserOid);
    return TradePortalConstants.ROUTE_SUCCESS;


   }

}
