package com.ams.tradeportal.busobj.util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.util.DocumentHandler;

public class FixNoSequence extends PanelRule{
private static final Logger LOG = LoggerFactory.getLogger(FixNoSequence.class);


 

	FixNoSequence(String str) {
		super(str);
	}
	
	FixNoSequence(DocumentHandler doc) {
    	super(doc);
    }
	
	

	public String toString(){
		super.setSequence("NOT_FIXED");
		return super.toString("NOT_FIXED: ");
	}

	@Override
	protected String matchSub(String authHistory, String userPanelLevel) throws AmsException {
	

		authHistory += userPanelLevel;

		return  processAuth(authHistory,true);

	}

}
