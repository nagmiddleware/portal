
  

package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;
/**
 * Accounts associated to the User, where the User is authorized to transfer
 * funds.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface UserAuthorizedTemplateGroupHome extends EJBHome
{
   public UserAuthorizedTemplateGroup create()
      throws RemoteException, CreateException, AmsException;

   public UserAuthorizedTemplateGroup create(ClientServerDataBridge csdb)
      throws RemoteException, CreateException, AmsException;
}
