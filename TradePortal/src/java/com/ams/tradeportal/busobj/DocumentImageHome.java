package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;
/**
 * This business object represents an entry on the imaging server that corresponds
 * to a transaction or mail message in the portal.  The imaging server entry
 * is generated by using the back end system (OTL).   This table is populated
 * by the middleware only when the transaction is updated from OTL.
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface DocumentImageHome extends EJBHome
{
   public DocumentImage create()
      throws RemoteException, CreateException, AmsException;

   public DocumentImage create(ClientServerDataBridge csdb)
      throws RemoteException, CreateException, AmsException;
}
