 
package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;




/*
 * 
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class InvoiceGoodsBean extends InvoiceGoodsBean_Base
{
private static final Logger LOG = LoggerFactory.getLogger(InvoiceGoodsBean.class);

/**
* Performs any special "validate" processing that is specific to the
* descendant of BusinessObject. Specifically, it checks to see whether
* or not the the Account Number and Currency information has been entered correctly
*/

  
}
