package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;
/**
 * Represents an instrument template.  The data for an instrument template
 * is stored in the TemplateInstrument business object.  This business object
 * contains the characteristics of the template from a reference data point
 * of view (who owns it, its name, whether or not it is express), where as
 * the TemplateInstrument actually contains the data that comprises the template.
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface TemplateHome extends EJBHome
{
   public Template create()
      throws RemoteException, CreateException, AmsException;

   public Template create(ClientServerDataBridge csdb)
      throws RemoteException, CreateException, AmsException;
}
