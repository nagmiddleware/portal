package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;
/**
 * Announcement home.
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface AnnouncementHome extends EJBHome
{
   public Announcement create()
      throws RemoteException, CreateException, AmsException;

   public Announcement create(ClientServerDataBridge csdb)
      throws RemoteException, CreateException, AmsException;
}
