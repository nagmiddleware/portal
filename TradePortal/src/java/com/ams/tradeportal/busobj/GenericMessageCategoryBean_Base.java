

/*
 * This file is generated from the model.  Normally it should not be modified manually.
 * Contact the modeler first.  (In a pinch, it can be modified first and then retrofitted
 * to the model.)
 *
 */
package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;


/*
 * Business object to store generic message categories that comprise instruments
 * created in the Trade Portal.
 *
 *     Copyright  � 2003
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class GenericMessageCategoryBean_Base extends ReferenceDataBean
{
private static final Logger LOG = LoggerFactory.getLogger(GenericMessageCategoryBean_Base.class);


  /*
   * Register the attributes and associations of the business object
   */
   protected void registerAttributes() throws AmsException
   {

      /* Register attributes defined in the Ancestor class */
      super.registerAttributes();

      /* description - A description of the Generic Message Category. */
      attributeMgr.registerAttribute("description", "description");
      attributeMgr.requiredAttribute("description");
      attributeMgr.registerAlias("description", getResourceManager().getText("GenericMessageCategoryBeanAlias.description", TradePortalConstants.TEXT_BUNDLE));

      /* short_description - A short description of the Generic Message Category. */
      attributeMgr.registerAttribute("short_description", "short_description");
      attributeMgr.requiredAttribute("short_description");
      attributeMgr.registerAlias("short_description", getResourceManager().getText("GenericMessageCategoryBeanAlias.short_description", TradePortalConstants.TEXT_BUNDLE));

      //Vshah - IR#LUUL120240799 - Rel7.1 - 12/14/2011 - <BEGIN>
      //Commented out below attribute registration as its already been done in super class.
      /* opt_lock - Optimistic lock attribute
         See jPylon documentation for details on how this works */
      //attributeMgr.registerAttribute("opt_lock", "opt_lock", "OptimisticLockAttribute");

      /* ownership_level - Ownership level of the Generic Message Category. */
      //attributeMgr.registerAttribute("ownership_level", "ownership_level");

      /* ownership_type - Ownership type of the Generic Message Category. */
      //attributeMgr.registerAttribute("ownership_type", "ownership_type");
      //Vshah - IR#LUUL120240799 - Rel7.1 - 12/14/2011 - <END>

      /* gen_message_category_oid - Unique identifier */
      attributeMgr.registerAttribute("gen_message_category_oid", "gen_message_category_oid", "ObjectIDAttribute");

        /* Pointer to the parent ReferenceDataOwner */
      attributeMgr.registerAttribute("owner_org_oid", "p_owner_org_oid", "ParentIDAttribute");

   }






}
