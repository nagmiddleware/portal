package com.ams.tradeportal.busobj.termsmgr;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import com.ams.tradeportal.busobj.*;
import com.ams.tradeportal.busobj.webbean.*;


/**
 * This class manages the dynamic attribute registration and validation
 * for a Terms business object or TermsWebBean for bank released terms
 * of a transaction of type Issue
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class ISSBankRelTermsMgr extends TermsMgr
{
private static final Logger LOG = LoggerFactory.getLogger(ISSBankRelTermsMgr.class);
	/**
	 *  Constructor that is used by business objects to create their
	 *  manager.  This method also register the dynamic attributes of
	 *  the bean.
	 *
	 *  @param mgr AttributeManager attribute manager of the bean being managed.  Used to register attributes
	 */
	public ISSBankRelTermsMgr(AttributeManager mgr) throws AmsException
	{
		super(mgr);
	}

	/**
	 *  Constructor that is used by web beans to create their
	 *  manager.  This method also register the dynamic attributes of
	 *  the bean.
	 *
	 *  @param mgr TermsWebBean - the bean being managed.  Used to register attributes.
	 */
	public ISSBankRelTermsMgr(TermsWebBean terms) throws AmsException
	{
		super(terms);
	}

	/**
	 *  Returns a list of attributes.  These describe each of the attributes
	 *  that will be registered for the bean being managed.
	 *
	 *  This method controls which attributes of TermsBean or TermsWebBean are registered
	 *  for bank released terms with this transaction type
	 *
	 *  @return Vector - list of attributes
	 */

	public List<Attribute> getAttributesToRegister()
	{
		List<Attribute> attrList = super.getAttributesToRegister();
		attrList.add( TermsBean.Attributes.create_amount() );
		attrList.add( TermsBean.Attributes.create_amount_currency_code() );
		attrList.add( TermsBean.Attributes.create_amt_tolerance_neg() );
		attrList.add( TermsBean.Attributes.create_amt_tolerance_pos() );
		attrList.add( TermsBean.Attributes.create_expiry_date() );
		attrList.add( TermsBean.Attributes.create_interest_to_be_paid() );
		attrList.add( TermsBean.Attributes.create_loan_rate() );
		attrList.add( TermsBean.Attributes.create_loan_terms_fixed_maturity_dt() );
		attrList.add( TermsBean.Attributes.create_loan_terms_number_of_days() );
		attrList.add( TermsBean.Attributes.create_loan_terms_type() );
		attrList.add( TermsBean.Attributes.create_pmt_terms_days_after_type() );
		attrList.add( TermsBean.Attributes.create_pmt_terms_fixed_maturity_date() );
		attrList.add( TermsBean.Attributes.create_pmt_terms_num_days_after () );
		attrList.add( TermsBean.Attributes.create_pmt_terms_percent_invoice() );
		attrList.add( TermsBean.Attributes.create_pmt_terms_type() );
		attrList.add( TermsBean.Attributes.create_related_instrument_id() );
        attrList.add( TermsBean.Attributes.create_usance_maturity_date() );
		attrList.add( TermsBean.Attributes.create_work_item_number() );
		//Narayan IR-RAUJ041840519 3/26/2010 Begin
		attrList.add( TermsBean.Attributes.create_discount_rate() );
		//Narayan IR-RAUJ041840519 3/26/2010 End
		//Vshah - 06/15/2011 - IR#SRUL061466014 - R6.1CMA - <BEGIN>
		//Added below 2 attributes, as required for Export Collection producer.
		attrList.add( TermsBean.Attributes.create_urc_year() );
		attrList.add( TermsBean.Attributes.create_urc_pub_num() );
		//Vshah - 06/15/2011 - IR#SRUL061466014 - R6.1CMA - <END>
		attrList.add( TermsBean.Attributes.create_final_expiry_date() );
		attrList.add( TermsBean.Attributes.create_transferrable() );

		return attrList;
	}


	/**
	 * This method is supposed to be used to set attributes of Terms business object.
	 *
	 * @param inputDoc DocumentHandler - universal XML message from MQSeries
	 * @param terms TermsBean
	 * @return boolean
	 */
	public boolean setManagerTerms(TermsBean terms, DocumentHandler inputDoc)
	throws java.rmi.RemoteException, AmsException
	{

		terms.setAttribute("amount_currency_code", 	    inputDoc.getAttribute("/Terms/TermsDetails/AmountCurrencyCode"));
		terms.setAttribute("amount",               	    inputDoc.getAttribute("/Terms/TermsDetails/Amount"));
		terms.setAttribute("amt_tolerance_pos",    	    inputDoc.getAttribute("/Terms/TermsDetails/AmountTolerancePositive"));
		terms.setAttribute("amt_tolerance_neg",    	    inputDoc.getAttribute("/Terms/TermsDetails/AmountToleranceNegative"));
		terms.setAttribute("expiry_date",		        inputDoc.getAttribute("/Terms/TermsDetails/ExpiryDate"));
		terms.setAttribute("work_item_number",          inputDoc.getAttribute("/Terms/TermsDetails/WorkItemNumber"));
		terms.setAttribute("pmt_terms_type",                inputDoc.getAttribute("/Terms/PaymentTerms/PaymentTermsType"));
		terms.setAttribute("pmt_terms_fixed_maturity_date", inputDoc.getAttribute("/Terms/PaymentTerms/PaymentTermsFixedMaturityDate"));
		terms.setAttribute("pmt_terms_num_days_after",      inputDoc.getAttribute("/Terms/PaymentTerms/PaymentTermsNumberDaysAfter"));
		terms.setAttribute("pmt_terms_days_after_type",     inputDoc.getAttribute("/Terms/PaymentTerms/PaymentTermsDaysAfterType"));
		terms.setAttribute("pmt_terms_percent_invoice",     inputDoc.getAttribute("/Terms/PaymentTerms/PaymentTermsPercentInvoice"));
		// W Zhu 6/29/07 PUUH062751147 fix typo LoanTermstype to LoanTermsType
		terms.setAttribute("loan_terms_type",               inputDoc.getAttribute("/Terms/LoanTerms/LoanTermsType"));
		terms.setAttribute("loan_rate",                     inputDoc.getAttribute("/Terms/LoanTerms/LoanRate"));
                terms.setAttribute("discount_rate", inputDoc.getAttribute("/Terms/TermsDetails/DiscountRate"));
		terms.setAttribute("loan_terms_fixed_maturity_dt",  inputDoc.getAttribute("/Terms/LoanTerms/LoanTermsFixedMaturityDate"));
		terms.setAttribute("loan_terms_number_of_days",     inputDoc.getAttribute("/Terms/LoanTerms/LoanTermsNumberOfDays"));
		//W Zhu 1/26/07 Set related instrument id.
		// Set related_instrument_id.  Usually the related instrument should be found in TP and
		// instrument.related_instrument_oid is set.  But if, for example, a new DCO spawns a LRQ,
		// the LRQ may come in before the DCO.  We need to store the related_instrument_id here
		// and later update instrument.related_instrument_oid when the DCO comes in.
            //Krishna added - "TermsDetails" in the following as per the TP4.0 tplInbound.dtd
		terms.setAttribute("related_instrument_id",     inputDoc.getAttribute("/Terms/TermsDetails/RelatedInstrumentID"));

		// Now create the shipment terms since some of the data needs to go there
		ShipmentTerms shipmentTerms = terms.createFirstShipment();
		shipmentTerms.setAttribute("latest_shipment_date", 	    inputDoc.getAttribute("/Terms/ShipmentTerms/LatestShipmentDate"));
		//Narayan CR 831 15-Jan-2014 Rel9.0 Begin
		terms.setAttribute("final_expiry_date",          inputDoc.getAttribute("/Terms/TermsDetails/FinalExpiryDate"));
		//Narayan CR 831 15-Jan-2014 Rel9.0 End
		if ( StringFunction.isNotBlank(inputDoc.getAttribute("/Terms/TermsDetails/Transferrable")) ) {
			terms.setAttribute("transferrable",          inputDoc.getAttribute("/Terms/TermsDetails/Transferrable"));
		}


		return true;
	}

	public boolean setManagerTermsFromWorkItem(TermsBean terms, DocumentHandler inputDoc)
	throws java.rmi.RemoteException, AmsException
	{
		terms.setAttribute("amount_currency_code", 	    inputDoc.getAttribute("/Amount"));
		terms.setAttribute("amount",               	    inputDoc.getAttribute("/Currency"));
		terms.setAttribute("work_item_number",     	    inputDoc.getAttribute("/WorkItemNumber"));

		return true;
	}

}