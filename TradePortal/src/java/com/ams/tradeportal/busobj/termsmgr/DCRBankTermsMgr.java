package com.ams.tradeportal.busobj.termsmgr;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.StringFunction;
import com.ams.tradeportal.busobj.*;
import com.ams.tradeportal.busobj.webbean.*;
import com.ams.tradeportal.common.*;

import java.rmi.*;

import com.ams.tradeportal.busobj.util.*;

/**
 * This abstract class has the common validation code for EXP_DLC__DCRTermsMgr and
 * INC_SLC__DCRTermsMgr.
 *
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public abstract class DCRBankTermsMgr extends TermsMgr
 {
private static final Logger LOG = LoggerFactory.getLogger(DCRBankTermsMgr.class);
    /**
     *  Constructor that is used by business objects to create their 
     *  manager.  This method also register the dynamic attributes of
     *  the bean.  
     *  
     *  @param mgr AttributeManager attribute manager of the bean being managed.  Used to register attributes
     */
    public DCRBankTermsMgr(AttributeManager mgr) throws AmsException
     {
	  super(mgr);
     }

    /**
     *  Constructor that is used by web beans to create their 
     *  manager.  This method also register the dynamic attributes of
     *  the bean.  
     *  
     *  @param mgr TermsWebBean - the bean being managed.  Used to register attributes.
     */
    public DCRBankTermsMgr(TermsWebBean terms) throws AmsException
     {
	  super(terms);
     }


    /**
     * Performs the Terms Manager-specific validation of text fields in the Terms
     * and TermsParty objects.  
     *
     * @param terms - the managed object
     *
     */
    protected void validateForSwiftCharacters(TermsBean terms) throws AmsException, RemoteException
     {
        InstrumentServices.checkForInvalidSwiftCharacters(terms.getAttributeHash(), terms.getErrorManager());
     }


    /**
     *  Performs validation of the attributes of this type of instrument and transaction.   This method is
     *  called from the userValidate() hook in the managed object.
     *
     *  @param terms - the bean being validated
     */
    public void validate(TermsBean terms)
			throws java.rmi.RemoteException, AmsException
     {
        super.validate(terms);

        String discrInstr;


       // Note: these validations are the same as those in EXP_DLC__DCR - 
       // any changes in here should probably be replicated in the other

       // Give an error if the export  discrepancy instruction attribute is blank.
       discrInstr = terms.getAttribute("export_discrepancy_instr");

       if (StringFunction.isBlank(discrInstr))
       {
          terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, 
		TradePortalConstants.PLEASE_SPECIFY_INSTR_TO_BANK);
       }

       // If the discrepancy instruction selected is "Other", ensure that
       // the "Other Instructions" field is populated, else issue error

       if (discrInstr != null && discrInstr.equals(TradePortalConstants.EXP_OTHER))
       {
	   if (StringFunction.isBlank(terms.getAttribute("instr_other")))
	   {
	      terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
		TradePortalConstants.ENTER_INSTRUCTIONS, terms.getAlias("instr_other"));
	   }

       }


     }


 }