package com.ams.tradeportal.busobj.termsmgr;
import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.busobj.TermsBean;
import com.ams.tradeportal.busobj.TermsParty;
import com.ams.tradeportal.busobj.util.InstrumentServices;
import com.ams.tradeportal.busobj.webbean.TermsWebBean;
import com.ams.tradeportal.common.TPCurrencyUtility;
import com.ams.tradeportal.common.TermsPartyType;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.Attribute;
import com.amsinc.ecsg.frame.AttributeManager;
import com.amsinc.ecsg.frame.ComponentList;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.util.DateTimeUtility;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.StringFunction;

/**
 * This class manages the dynamic attribute registration and validation
 * for a Terms business object or TermsWebBean for an Issue Standby LC.
*
 * Note: for Standby LC, the 5 terms parties represent these types:
 *        first     beneficiary
 *        second    applicant
 *        third     correspondence bank
 *
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class SLC__ISSTermsMgr extends TermsMgr
 {
private static final Logger LOG = LoggerFactory.getLogger(SLC__ISSTermsMgr.class);
    /**
     *  Constructor that is used by business objects to create their
     *  manager.  This method also register the dynamic attributes of
     *  the bean.
     *
     *  @param mgr AttributeManager attribute manager of the bean being managed.  Used to register attributes
     */
    public SLC__ISSTermsMgr(AttributeManager mgr) throws AmsException
     {
	  super(mgr);
     }

    /**
     *  Constructor that is used by web beans to create their
     *  manager.  This method also register the dynamic attributes of
     *  the bean.
     *
     *  @param mgr TermsWebBean - the bean being managed.  Used to register attributes.
     */
    public SLC__ISSTermsMgr(TermsWebBean terms) throws AmsException
     {
	  super(terms);
     }

    /**
     *  Returns a list of attributes.  These describe each of the attributes
     *  that will be registered for the bean being managed.
     *
     *  This method controls which attributes of TermsBean or TermsWebBean are registered
     *  for an this instrument and transaction type
     *
     *  @return Vector - list of attributes
     */
          public List<Attribute> getAttributesToRegister()
     {
        List<Attribute> attrList = super.getAttributesToRegister();
        attrList.add( TermsBean.Attributes.create_additional_conditions() );
        attrList.add( TermsBean.Attributes.create_addl_doc_text() );
        attrList.add( TermsBean.Attributes.create_amount() );
        attrList.add( TermsBean.Attributes.create_amount_currency_code() );
        attrList.add( TermsBean.Attributes.create_amt_tolerance_neg() );
        attrList.add( TermsBean.Attributes.create_amt_tolerance_pos() );
        attrList.add( TermsBean.Attributes.create_auto_extend() );
        attrList.add( TermsBean.Attributes.create_available_by() );
        attrList.add( TermsBean.Attributes.create_available_with_party() );
        attrList.add( TermsBean.Attributes.create_bank_charges_type() );
        attrList.add( TermsBean.Attributes.create_branch_code() );
        attrList.add( TermsBean.Attributes.create_characteristic_type() );
        attrList.add( TermsBean.Attributes.create_coms_chrgs_foreign_acct_curr() );
        attrList.add( TermsBean.Attributes.create_coms_chrgs_foreign_acct_num() );
        attrList.add( TermsBean.Attributes.create_coms_chrgs_other_text() );
        attrList.add( TermsBean.Attributes.create_coms_chrgs_our_account_num() );
        attrList.add( TermsBean.Attributes.create_confirmation_type() );
        attrList.add( TermsBean.Attributes.create_drafts_required() );
        attrList.add( TermsBean.Attributes.create_drawn_on_party() );
        attrList.add( TermsBean.Attributes.create_expiry_date() );
        attrList.add( TermsBean.Attributes.create_internal_instructions() );
        attrList.add( TermsBean.Attributes.create_irrevocable() );
        attrList.add( TermsBean.Attributes.create_operative() );
        attrList.add( TermsBean.Attributes.create_place_of_expiry() );
        attrList.add( TermsBean.Attributes.create_pmt_terms_type() );
        attrList.add( TermsBean.Attributes.create_purpose_type() );
        attrList.add( TermsBean.Attributes.create_guar_deliver_by() );
		attrList.add( TermsBean.Attributes.create_guar_deliver_to() );
        attrList.add( TermsBean.Attributes.create_reference_number() );
        attrList.add( TermsBean.Attributes.create_settlement_foreign_acct_curr() );
        attrList.add( TermsBean.Attributes.create_settlement_foreign_acct_num() );
        attrList.add( TermsBean.Attributes.create_settlement_our_account_num() );
        attrList.add( TermsBean.Attributes.create_special_bank_instructions() );
        attrList.add( TermsBean.Attributes.create_ucp_details() );
        attrList.add( TermsBean.Attributes.create_ucp_version () );
        attrList.add( TermsBean.Attributes.create_ucp_version_details_ind() );
        attrList.add( TermsBean.Attributes.create_work_item_number() );
        attrList.add( TermsBean.Attributes.create_guarantee_issue_type() );//IR-PAUK040245609 SWIFT Upgrade
        attrList.add( TermsBean.Attributes.create_overseas_validity_date() );//IR-PAUK040245609 SWIFT Upgrade

      //Leelavathi - 10thDec2012 - Rel8200 CR-737 - Start
        attrList.add( TermsBean.Attributes.create_auto_extension_ind() );
        attrList.add( TermsBean.Attributes.create_max_auto_extension_allowed() );
		attrList.add( TermsBean.Attributes.create_auto_extension_period() );
		attrList.add( TermsBean.Attributes.create_auto_extension_days() );
		attrList.add( TermsBean.Attributes.create_payment_type() );
		attrList.add( TermsBean.Attributes.create_percent_amount_dis_ind() );
		attrList.add( TermsBean.Attributes.create_special_tenor_text() );
	 	attrList.add( TermsBean.Attributes.create_final_expiry_date() );
		attrList.add( TermsBean.Attributes.create_notify_bene_days() );
	   //Narayan - 07-Jan-2014 - Rel9.0 CR-831 - End
		
        return attrList;
     }




    /**
     * Performs presave processing (regardless of whether validation is done)
     *
     * @param terms com.ams.tradeportal.busobj.termsBean
     * @exception com.amsinc.ecsg.frame.AmsException The exception description.
     */
    public void preSave(TermsBean terms) throws AmsException
    {
      super.preSave(terms);

      try
      {
	// check to make sure that the amount is valid for the given currency
	validateAmount(terms, "amount", "amount_currency_code");
        validateTextLengths(terms);
      }
      catch (Exception e)
      {
	LOG.error("Exception at preSave() ",e);
	throw new AmsException(e.getMessage());
      }

    }

    /**
     *  Performs validation of the attributes of this type of instrument and transaction.   This method is
     *  called from the userValidate() hook in the managed object.
     *
     *  @param terms - the bean being validated
     */
    public void validate(TermsBean terms)
			throws java.rmi.RemoteException, AmsException
     {
        super.validate(terms);

	terms.registerRequiredAttribute("amount_currency_code");
	terms.registerRequiredAttribute("amount");
	terms.registerRequiredAttribute("bank_charges_type");
	terms.registerRequiredAttribute("addl_doc_text");
	// The second terms party is the applicant.
	terms.registerRequiredAttribute("c_SecondTermsParty");
	terms.registerRequiredAttribute("guar_deliver_to");
    terms.registerRequiredAttribute("guar_deliver_by");

	validateGeneral(terms);
	//Leelavathi - 10thDec2012 - Rel8200 CR-737 - Start
	validateOtherCondtions(terms);
	//Leelavathi - 10thDec2012 - Rel8200 CR-737 - End
        validateInstructions(terms);
     }

    /**
     * Performs the Terms Manager-specific validation of text fields in the Terms
     * and TermsParty objects.
     *
     * @param terms - the managed object
     *
     */
    protected void validateForSwiftCharacters(TermsBean terms) throws AmsException, RemoteException
     {
        final String[] attributesToExclude = {"special_bank_instructions", "branch_code", "settlement_our_account_num",
                                        "settlement_foreign_acct_num", "coms_chrgs_our_account_num", "coms_chrgs_foreign_acct_num",
                                        "coms_chrgs_other_text", "internal_instructions"};

        // CR-594 - manohar 08/08/2011 - Begin
        String overrideSwiftValidationInd = getOverrideSwiftValidationInd(terms);
        String deliverBy = terms.getAttribute("guar_deliver_by");
        if (!TradePortalConstants.INDICATOR_YES.equalsIgnoreCase(overrideSwiftValidationInd) 
                || TradePortalConstants.DELIVER_BY_TELEX.equalsIgnoreCase(deliverBy)) {
        	InstrumentServices.checkForInvalidSwiftCharacters(terms.getAttributeHash(), terms.getErrorManager(), attributesToExclude);

             validateTermsPartyForSwiftChars(terms, "FirstTermsParty", "SLCIssue.Beneficiary");
             validateTermsPartyForSwiftChars(terms, "SecondTermsParty", "SLCIssue.Applicant");
             validateTermsPartyForSwiftChars(terms, "ThirdTermsParty", "SLCIssue.CorrespondentBank");	

		}// CR-594 - manohar 08/08/2011 - End
		else {
			validateTermsPartyForISO8859Chars(terms, "FirstTermsParty");
			validateTermsPartyForISO8859Chars(terms, "SecondTermsParty");
			validateTermsPartyForISO8859Chars(terms, "ThirdTermsParty");
		}

     }

   /**
     * Performs validation for the general section.
     *
     * @param terms com.ams.tradeportal.busobj.termsBean
     */
    public void validateGeneral(TermsBean terms) throws java.rmi.RemoteException, AmsException
    {
		// The first terms party is the beneficiary, whose fields should be required
		this.editRequiredBeneficiaryValue(terms, "FirstTermsParty");

		// pcutrone - 10/17/2007 - REUH101146143 - Check to make sure the amount field is not set to zero.
	    String value;
	  //Jyoti IR#T36000012016 CR737 Rel-8.2 27th Feb,2013  Start
	  	StringBuffer  sqlQuery  = null;
	  	String 		  termsOid  = terms.getAttribute("terms_oid");
	  	DocumentHandler    instrumentAmountDoc       = null;
	  	//Jyoti IR#T36000012016 CR737 Rel-8.2 27th Feb,2013  end
		value = terms.getAttribute("amount");
		if (!StringFunction.isBlank(value)) {
			if (Double.parseDouble(value) == 0) {
				terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.AMOUNT_IS_ZERO);
			}
		}
				value = terms.getAttribute("guarantee_issue_type");
				if (value.equals(TradePortalConstants.GUAR_ISSUE_APPLICANTS_BANK)) {
					terms.setAttribute("overseas_validity_date", "");
					}
		//IR-PAUK040245609 SWIFT Upgrade
				
				
				//Leelavathi - 10thDec2012 - Rel8200 CR-737 - Start
				value = terms.getAttribute("payment_type");
				if(StringFunction.isBlank(value)){
					terms.getErrorManager ().issueError (
			                   getClass (). getName (), AmsConstants.REQUIRED_ATTRIBUTE, terms.getResourceManager().getText("ImportDLCIssue.PmtType",
										TradePortalConstants.TEXT_BUNDLE));
				
				}else{
					
					ComponentList pmtTermsTenorDtlList = (ComponentList) terms.getComponentHandle("PmtTermsTenorDtlList");

					int pmtTermsCount = pmtTermsTenorDtlList.getObjectCount();
					String amountPercentInd = terms.getAttribute("percent_amount_dis_ind");
					int totalPercent = 0;
					BigDecimal totalAmount = BigDecimal.ZERO;
					BigDecimal instrumentAmount = BigDecimal.ZERO;
					String specialTenorText= terms.getAttribute("special_tenor_text");
					
					 if(value.equals("SPEC") ){
							if(StringFunction.isBlank(specialTenorText) ){
							//�Tenor Terms details are require if Available By is Special�.
								terms.getErrorManager ().issueError (
						                   getClass (). getName (), TradePortalConstants.PMT_TENOR_DTL_REQ);
							}
						} else 
					
					// Validate each set of tenor payment terms
					for (int pmtTermsIndex = 0; pmtTermsIndex < pmtTermsCount; pmtTermsIndex++){
						pmtTermsTenorDtlList.scrollToObjectByIndex(pmtTermsIndex);
						//percent amount tenor_type num_days_after days_after_type maturity_date
						String percent= pmtTermsTenorDtlList.getListAttribute("percent");
						String amount= pmtTermsTenorDtlList.getListAttribute("amount");
						String tenorType= pmtTermsTenorDtlList.getListAttribute("tenor_type");
						String numDaysAfter= pmtTermsTenorDtlList.getListAttribute("num_days_after");
						String daysAfterType= pmtTermsTenorDtlList.getListAttribute("days_after_type");
						String maturityDate= pmtTermsTenorDtlList.getListAttribute("maturity_date");
						if(value.equals("MIXP") || value.equals("NEGO")){
							 if(StringFunction.isBlank(tenorType) ){
									//�If Tenor Details has been specified, then �Days after� must also be specified in Payment Summary line �x�
								 	//IR T36000048558 Rel9.5 05/09/2016
									terms.getErrorManager ().issueError (
							                   getClass (). getName (), TradePortalConstants.TENOR_TYPE_REQUIRED_CONDITION, 
											new String[]{terms.getResourceManager().getText("OutgoingSLCIssue.TenorType",
													TradePortalConstants.TEXT_BUNDLE),terms.getResourceManager().getText("OutgoingSLCIssue.PmtSummary",
															TradePortalConstants.TEXT_BUNDLE),""+(pmtTermsIndex+1)});
									}
						}
						
						if(value.equals("DEFP") || value.equals("ACCP") || (
								(value.equals("MIXP") || value.equals("NEGO")) 
									&& ("ACC".equals(tenorType) || "DFP".equals(tenorType))
						) ){
							
												if(StringFunction.isNotBlank(daysAfterType) ){
													if(StringFunction.isBlank(numDaysAfter) ){
														//�If Tenor Details has been specified, then �Days after� must also be specified in Payment Summary line �x�
														terms.getErrorManager ().issueError (
												                   getClass (). getName (), TradePortalConstants.ROW_ATTRIBUTE_REQUIRED_BASED_CONDITION, 
																new String[]{terms.getResourceManager().getText("OutgoingSLCIssue.TenorDetails",
																		TradePortalConstants.TEXT_BUNDLE),terms.getResourceManager().getText("CorpCust.BankDefinedRulesDaysAfter",
																				TradePortalConstants.TEXT_BUNDLE),terms.getResourceManager().getText("OutgoingSLCIssue.PmtSummary",
																		TradePortalConstants.TEXT_BUNDLE),""+(pmtTermsIndex+1)});
													}else if(Integer.parseInt(numDaysAfter)<=0){
														//�Value in days after field should be greater than 0 in Payment Summary line �x�
														terms.getErrorManager ().issueError (
												                   getClass (). getName (), TradePortalConstants.ROW_ATTRIBUTE_MIN_CONDITION, 
																new String[]{terms.getResourceManager().getText("ExportCollectionIssue.DaysAfter",
																		TradePortalConstants.TEXT_BUNDLE),"0", terms.getResourceManager().getText("OutgoingSLCIssue.PmtSummary",
																		TradePortalConstants.TEXT_BUNDLE),""+(pmtTermsIndex+1)});
													}
												}else if (StringFunction.isBlank(maturityDate)){
													if(StringFunction.isNotBlank(numDaysAfter) 
															&& Integer.parseInt(numDaysAfter)<=0){
																//�Value in days after field should be greater than 0 in Payment Summary line �x�
																terms.getErrorManager ().issueError (
														                   getClass (). getName (), TradePortalConstants.ROW_ATTRIBUTE_MIN_CONDITION, 
																		new String[]{terms.getResourceManager().getText("ExportCollectionIssue.DaysAfter",
																				TradePortalConstants.TEXT_BUNDLE),"0", terms.getResourceManager().getText("OutgoingSLCIssue.PmtSummary",
																				TradePortalConstants.TEXT_BUNDLE),""+(pmtTermsIndex+1)});

														}
													
															//�Either Tenor Details or Maturity Date is required for Payment Summary line x� 
															//IR T36000048558 Rel9.5 05/09/2016
															terms.getErrorManager ().issueError (
													                   getClass (). getName (), TradePortalConstants.ROW_ATTRIBUTE_REQUIRED_EITHER_OF_TWO, 
																	new String[]{terms.getResourceManager().getText("OutgoingSLCIssue.TenorDetails",
																			TradePortalConstants.TEXT_BUNDLE),terms.getResourceManager().getText("OutgoingSLCIssue.MaturityDate",
																			TradePortalConstants.TEXT_BUNDLE), terms.getResourceManager().getText("OutgoingSLCIssue.PmtSummary",
																					TradePortalConstants.TEXT_BUNDLE),""+(pmtTermsIndex+1)});
															//Leelavathi IR#T36000012144 03/06/2013 Rel-8.2 End

							}else {
								SimpleDateFormat dateFormatter   = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
								String creationDate = DateTimeUtility.getGMTDateTime();
								try {
									if(dateFormatter.parse(maturityDate).before(dateFormatter.parse(creationDate))){
									//Maturity Date in Payment Summary line �x� cannot be less than today�s date or the Issue date of this transaction
										terms.getErrorManager ().issueError (
								                   getClass (). getName (), TradePortalConstants.PMT_MATURITY_DATE_CHECK, 
												new String[]{""+(pmtTermsIndex+1)});
									}
								} catch (ParseException e) {									
									throw new AmsException("Date parse error caught in IMP_DLC__ISSTermsMgr.validateGeneral()");
								}
							}
						}
						 if(value.equals("MIXP") || value.equals("NEGO")){
							//Leelavathi IR#T36000015183,IR#T36000015039,IR# T36000011401  CR-737 Rel-8.2 28/03/2013 Begin
							 if( "P".equals(amountPercentInd)){
								    //PMitnala Rel 8.3 IR#T36000022676 - Check for Blank/Null values of percent 
								    if(StringFunction.isNotBlank(percent)){
								    	totalPercent += Integer.parseInt(percent);
								    }
								}else if ( "A".equals(amountPercentInd)){
									
									if (StringFunction.isNotBlank(amount))
						            {
										StringBuffer alias = new StringBuffer() ;
										alias.append(terms.getResourceManager().getText("ImportDLCIssue.PaymentInAmount", TradePortalConstants.TEXT_BUNDLE));
										alias.append(" 'in Payment Summary "+(pmtTermsIndex+1)+"' ");
										
										try{
										    // Validate that value will fit in database and that currency precision is correct
										TPCurrencyUtility.validateAmount(terms.getAttribute("amount_currency_code"), 
												amount, alias.toString(), terms.getErrorManager() );
										}catch(com.amsinc.ecsg.frame.AmsException amsExp){
											LOG.error("Ams Exception at validateGeneral() ",amsExp);
										}
										//Leelavathi IR#T36000017721 03/06/2013 CR-737 Rel-8.2 Begin
										totalAmount = totalAmount.add(new BigDecimal(amount));
										//Leelavathi IR#T36000017721 03/06/2013 CR-737 Rel-8.2 End
						            }
									
								}
						}
					}
					//Jyoti IR#T36000012016 CR737 Rel-8.2 27th Feb,2013  Start
					 try{
						 LOG.debug("termsOid is: {}", termsOid);
						 if(termsOid!=null){
						 	sqlQuery  = new StringBuffer();
							sqlQuery.append("select instrument_amount ");
							sqlQuery.append("from transaction ");
							sqlQuery.append("where C_CUST_ENTER_TERMS_OID= ?");

							instrumentAmountDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery.toString(), false, new Object[]{termsOid});
							
							if (instrumentAmountDoc != null)
							{
								//Leelavathi IR#T36000015183,IR#T36000015039,IR# T36000011401  CR-737 Rel-8.2 28/03/2013 Begin
								String instrumentAmt=instrumentAmountDoc .getAttribute("/ResultSetRecord(0)/INSTRUMENT_AMOUNT");
								//Leelavathi IR#T36000015041 CR-737 Rel-8.2 07/05/2013 Begin
								Double instrumentLongAmount=new Double(StringFunction.isNotBlank(instrumentAmt)?
														instrumentAmt:terms.getAttribute("amount"));
								//Leelavathi IR#T36000015041 CR-737 Rel-8.2 07/05/2013 End
								instrumentAmount = BigDecimal.valueOf(instrumentLongAmount);
								//Leelavathi IR#T36000015183,IR#T36000015039,IR# T36000011401  CR-737 Rel-8.2 28/03/2013 End
								LOG.debug("instrumentAmount is: {}", instrumentAmount);
							}
						 }
						
					 }catch(Exception e ){
						 LOG.error("Error occured getting instument amount", e);

					 }
					//Leelavathi IR#T36000015183,IR#T36000015039,IR# T36000011401  CR-737 Rel-8.2 28/03/2013 Begin
					 //Leelavathi IR#T36000015492 CR-737 Rel-8.2 04/04/2013 Begin
					 if(value.equals("MIXP") || value.equals("NEGO")){
						 //Leelavathi IR#T36000015492 CR-737 Rel-8.2 04/04/2013 Begin
					 if(  "P".equals(amountPercentInd) && totalPercent!= 100){
							//�Total of all Tenor percents must equal to 100�
							terms.getErrorManager ().issueError (
					                   getClass (). getName (), TradePortalConstants.ROW_COLUMN_TOT_CHECK,
					                   					new String[]{terms.getResourceManager().getText("OutgoingSLCIssue.TenorPct",
																TradePortalConstants.TEXT_BUNDLE), "100"});
							
						}
					
				}
				}
				
    }

 	 /* Performs validation for the bank instructions section.
	 *
	 * @param terms com.ams.tradeportal.busobj.TermsBean
	 */
	public void validateOtherCondtions(TermsBean terms) throws java.rmi.RemoteException, AmsException
	{
		String value;
		value = terms.getAttribute("auto_extension_ind");
		if (TradePortalConstants.INDICATOR_YES.equalsIgnoreCase(value))
		{
			int autoExtensionDays = StringFunction.isNotBlank(terms.getAttribute("auto_extension_days")) 
													? Integer.parseInt(terms.getAttribute("auto_extension_days")) :0;
			int maxAutoExtensionAllowed = StringFunction.isNotBlank(terms.getAttribute("max_auto_extension_allowed")) 
													? Integer.parseInt(terms.getAttribute("max_auto_extension_allowed")) :0;
			value = terms.getAttribute("auto_extension_period");
			
			if(StringFunction.isBlank(value)){
				terms.getErrorManager ().issueError (
		                   getClass (). getName (), AmsConstants.REQUIRED_ATTRIBUTE,terms.getResourceManager().getText("OutgoingSLCIssue.AutoExtPeriod",
									TradePortalConstants.TEXT_BUNDLE));
			}else if (value.equals("DAY") && autoExtensionDays ==0 ){
				terms.getErrorManager ().issueError (
		                   getClass (). getName (), TradePortalConstants.ATTRIBUTE_REQUIRED_BASED_CONDITION, 
						new String[]{terms.getResourceManager().getText("OutgoingSLCIssue.AutoExtDays",
								TradePortalConstants.TEXT_BUNDLE),terms.getResourceManager().getText("OutgoingSLCIssue.ExtensionPeriod",
										TradePortalConstants.TEXT_BUNDLE),"Daily"});
			}else if( value.equals("DAY") && (autoExtensionDays <1 || autoExtensionDays >999 )){
				terms.getErrorManager ().issueError (
		                   getClass (). getName (), TradePortalConstants.ATTRIBUTE_VLAUE_RANGE, 
						new String[]{terms.getResourceManager().getText("OutgoingSLCIssue.AutoExtNoOfDays",
								TradePortalConstants.TEXT_BUNDLE),"0","1000"});
			}
			
									
			//Narayan - 09-Jan-2014 - Rel9.0 CR-831 - Start			
			try {
				Date finalExpiryDate = terms.getAttributeDate("final_expiry_date");					
				//If Auto Extension is selected and the Final Expiry Date and Maximum Number are both blank then display a Warning
				if(finalExpiryDate == null ){
					if(StringFunction.isBlank(terms.getAttribute("max_auto_extension_allowed"))){
						terms.getErrorManager().issueError(getClass().getName(), TradePortalConstants.EXPIRY_DATE_OR_MAX_NUM_NOT_PRESENT_WARN);
				    }else if (maxAutoExtensionAllowed <1 || maxAutoExtensionAllowed >999 ){
							terms.getErrorManager ().issueError (
					                   getClass (). getName (), TradePortalConstants.ATTRIBUTE_VLAUE_RANGE, 
									new String[]{terms.getResourceManager().getText("OutgoingSLCIssue.AutoExtMaxNo",
											TradePortalConstants.TEXT_BUNDLE),"0","1000"});
				    }
				} else {
					Date expiryDate = terms.getAttributeDate("expiry_date");
				    if( expiryDate != null && expiryDate.getTime() >= finalExpiryDate.getTime() )
				    {
					  terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
												   TradePortalConstants.EXPIRY_DATE_MUST_BE_LESS_THAN);
			 	    }
				}			
			}catch (AmsException e) {			
				// Probable cause is an unparseable date (because user did not
				// fill it in.  In this case, skip over the validation since we
				// can't validate invalid dates.
				 LOG.error("Error occured getting expiry date", e);
			}
			//Narayan - 09-Jan-2014 - Rel9.0 CR-831 - End
		}
		
		//Sandeep - Rel-8.3 CR-752 Dev 06/12/2013 - Begin
		value = terms.getAttribute("ucp_version_details_ind");
		
		if (TradePortalConstants.INDICATOR_YES.equals(value)) {
			value = terms.getAttribute("ucp_version");
			
			if(StringFunction.isBlank(value)){
				terms.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						AmsConstants.REQUIRED_ATTRIBUTE,
						terms.getResourceManager().getText( "common.ICCVersionRequired", TradePortalConstants.TEXT_BUNDLE)
						);
			}
			
			if(TradePortalConstants.SLC_OTHER.equals(value)){
				value = terms.getAttribute("ucp_details");
				if(StringFunction.isBlank(value)){
					terms.getErrorManager().issueError(
							TradePortalConstants.ERR_CAT_1,
							TradePortalConstants.ICC_PUBLICATION_DETAILS_CHECK);
				}
			}
		}
		//Sandeep - Rel-8.3 CR-752 Dev 06/12/2013 - End
	}
	//Leelavathi - 10thDec2012 - Rel8200 CR-737 - End
    /**
     * Performs validation for the bank instructions section.
     *
     * @param terms com.ams.tradeportal.busobj.termsBean
     */
    public void validateInstructions(TermsBean terms) throws java.rmi.RemoteException, AmsException
    {
        String forAcct, forCurr;
       	String alias;

	// If debit foreign currency account number is filled, currency of account
	// should be filled in and vice versa.  Applies to Settlement Instructions and
	// Commissions and Charges

	forAcct = terms.getAttribute("settlement_foreign_acct_num");
	forCurr = terms.getAttribute("settlement_foreign_acct_curr");
	if (!StringFunction.isBlank(forAcct) &&
	    StringFunction.isBlank(forCurr))
	{
	    alias = terms.getAlias("settlement_foreign_acct_curr");
	    terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
		TradePortalConstants.SETTLE_INFO_MISSING, alias);

	}
	else if (StringFunction.isBlank(forAcct) &&
	    !StringFunction.isBlank(forCurr))
	{
	    alias = terms.getAlias("settlement_foreign_acct_num");
	    terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
		TradePortalConstants.SETTLE_INFO_MISSING, alias);

	}

	forAcct = terms.getAttribute("coms_chrgs_foreign_acct_num");
	forCurr = terms.getAttribute("coms_chrgs_foreign_acct_curr");
	if (!StringFunction.isBlank(forAcct) &&
	    StringFunction.isBlank(forCurr))
	{
	    alias = terms.getAlias("coms_chrgs_foreign_acct_curr");
	    terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
		TradePortalConstants.SETTLE_INFO_MISSING, alias);

	}
	else if (StringFunction.isBlank(forAcct) &&
	    !StringFunction.isBlank(forCurr))
	{
	    alias = terms.getAlias("coms_chrgs_foreign_acct_num");
	    terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
		TradePortalConstants.SETTLE_INFO_MISSING, alias);

	}
    }

   /**
    *  Performs validation of text fields for length.
    *
    *  @param terms TermsBean - the bean being validated
    */
    public void validateTextLengths(TermsBean terms) throws java.rmi.RemoteException, AmsException
    {
    	String overrideSwiftLengthInd = getOverrideSwiftLengthInd(terms);

    	if((overrideSwiftLengthInd != null)
    			&& (overrideSwiftLengthInd.equals(TradePortalConstants.INDICATOR_NO)))
    	{
    		checkTextLength(terms, "addl_doc_text",         6500);
    		checkTextLength(terms, "additional_conditions", 6500);
    	} 
    	checkTextLength(terms, "internal_instructions", 1000);
    	checkTextLength(terms, "special_bank_instructions", 1000);
    	checkTextLength(terms, "coms_chrgs_other_text", 1000);
    	checkTextLength(terms, "special_tenor_text", 1000);
    }



  //Beginning of code used by the Middleware team
    /**
	 *  gets the values of the attributes of the managed object. This method is called
	 *  from the getTermsAttributesDoc() hook in the managed object.
	 *
	 *  return com.amsinc.ecsg.util.DocumentHandler
	 *  @param terms TermsBean - the bean being validated
	 *  @param termsDoc com.amsinc.ecsg.util.DocumentHandler
	 */
   public DocumentHandler getManagerTerms(TermsBean terms, DocumentHandler termsDoc) throws java.rmi.RemoteException, AmsException

   {

   	         DocumentHandler termsValuesDoc = terms.populateXmlDoc(new DocumentHandler());
             LOG.debug("This is the termValuesDoc containing all the terms values ==> {}",  termsValuesDoc);


   	   	           //Terms_Details
			             termsDoc.setAttribute("/Terms/TermsDetails/Irrevocable",	            termsValuesDoc.getAttribute("/Terms/irrevocable"));
			             termsDoc.setAttribute("/Terms/TermsDetails/Operative",       	        termsValuesDoc.getAttribute("/Terms/operative"));
			             termsDoc.setAttribute("/Terms/TermsDetails/PlaceOfExpiry", 	        termsValuesDoc.getAttribute("/Terms/place_of_expiry"));
			             termsDoc.setAttribute("/Terms/TermsDetails/ExpiryDate",				termsValuesDoc.getAttribute("/Terms/expiry_date"));
			             termsDoc.setAttribute("/Terms/TermsDetails/ReferenceNumber",	        termsValuesDoc.getAttribute("/Terms/reference_number"));
			             termsDoc.setAttribute("/Terms/TermsDetails/AmountCurrencyCode",        termsValuesDoc.getAttribute("/Terms/amount_currency_code"));
			             termsDoc.setAttribute("/Terms/TermsDetails/Amount",                    termsValuesDoc.getAttribute("/Terms/amount"));
			             termsDoc.setAttribute("/Terms/TermsDetails/AmountTolerancePositive",	termsValuesDoc.getAttribute("/Terms/amt_tolerance_pos"));
			             termsDoc.setAttribute("/Terms/TermsDetails/AmountToleranceNegative",	termsValuesDoc.getAttribute("/Terms/amt_tolerance_neg"));
			             termsDoc.setAttribute("/Terms/TermsDetails/PurposeType",		        termsValuesDoc.getAttribute("/Terms/purpose_type"));
			             termsDoc.setAttribute("/Terms/TermsDetails/GuaTerms/GuaDeliverBy",			       termsValuesDoc.getAttribute("/Terms/guar_deliver_by"));
			 			termsDoc.setAttribute("/Terms/TermsDetails/GuaTerms/GuaDeliverTo",		           termsValuesDoc.getAttribute("/Terms/guar_deliver_to"));
			             termsDoc.setAttribute("/Terms/TermsDetails/DraftsRequired",			termsValuesDoc.getAttribute("/Terms/drafts_required"));
			           //Leelavathi - 10thDec2012 - Rel8200 CR-737 - Start
				     		/*termsDoc.setAttribute("/Terms/TermsDetails/AvailableBy",		            terms.getAttribute("available_by"));*/
				     		termsDoc.setAttribute("/Terms/TermsDetails/AvailableBy",		            terms.getAttribute("payment_type"));
				     		//Leelavathi - 10thDec2012 - Rel8200 CR-737 - End
			             termsDoc.setAttribute("/Terms/TermsDetails/AvailableWithParty",        termsValuesDoc.getAttribute("/Terms/available_with_party"));
			             if(termsValuesDoc.getAttribute("/Terms/drafts_required").equals(TradePortalConstants.DRAFTS_REQUIRED))
			               termsDoc.setAttribute("/Terms/TermsDetails/DrawnOnParty",		        termsValuesDoc.getAttribute("/Terms/drawn_on_party"));
			             termsDoc.setAttribute("/Terms/TermsDetails/UcpVersion",				termsValuesDoc.getAttribute("/Terms/ucp_version"));
			             termsDoc.setAttribute("/Terms/TermsDetails/UcpDetails",				termsValuesDoc.getAttribute("/Terms/ucp_details"));
			             termsDoc.setAttribute("/Terms/TermsDetails/UcpIndicator",              termsValuesDoc.getAttribute("/Terms/ucp_version_details_ind"));
			             termsDoc.setAttribute("/Terms/TermsDetails/CharacteristicType",        termsValuesDoc.getAttribute("/Terms/characteristic_type"));
			             termsDoc.setAttribute("/Terms/TermsDetails/GuaTerms/GuaIssueType",		termsValuesDoc.getAttribute("/Terms/guarantee_issue_type"));//IR-PAUK040245609 SWIFT Upgrade
			             termsDoc.setAttribute("/Terms/TermsDetails/GuaTerms/OverseasValidityDate",		   termsValuesDoc.getAttribute("/Terms/overseas_validity_date"));//IR-PAUK040245609 SWIFT Upgrade
			             //End of Terms_Details

			 	        //Leelavathi - 10thDec2012 - Rel8200 CR-737 - Start
			      	    termsDoc.setAttribute("/Terms/TermsDetails/AutoExtendTerms/AutoExtendIndicator", termsValuesDoc.getAttribute("/Terms/auto_extension_ind"));
			      	    termsDoc.setAttribute("/Terms/TermsDetails/AutoExtendTerms/AutoExtendPeriod",	termsValuesDoc.getAttribute("/Terms/auto_extension_period"));
			 		    if(TradePortalConstants.INDICATOR_YES.equals(termsValuesDoc.getAttribute("/Terms/auto_extension_ind")) && 
			 		    		StringFunction.isBlank(termsValuesDoc.getAttribute("/Terms/max_auto_extension_allowed")) && 
			 		    		StringFunction.isBlank(termsValuesDoc.getAttribute("/Terms/final_expiry_date"))){
			 		    	termsDoc.setAttribute("/Terms/TermsDetails/AutoExtendTerms/MaximumNo", TradePortalConstants.MAX_AUTO_EXTENSION_ALLOWED);
			 		    	
			 		    }else{
			 		    	termsDoc.setAttribute("/Terms/TermsDetails/AutoExtendTerms/MaximumNo", termsValuesDoc.getAttribute("/Terms/max_auto_extension_allowed"));
			 		    }
			      	    termsDoc.setAttribute("/Terms/TermsDetails/AutoExtendTerms/NumberOfDays", termsValuesDoc.getAttribute("/Terms/auto_extension_days"));
			      	    //Leelavathi - 10thDec2012 - Rel8200 CR-737 - End
						//Narayan - 14/Jan/2014 - Rel9.0 CR-831 - Start
						 termsDoc.setAttribute("/Terms/TermsDetails/AutoExtendTerms/BeneficiaryNotifyDays", termsValuesDoc.getAttribute("/Terms/notify_bene_days"));
						 termsDoc.setAttribute("/Terms/TermsDetails/AutoExtendTerms/FinalExpiryDate", termsValuesDoc.getAttribute("/Terms/final_expiry_date"));
						 //Narayan - 14/Jan/2014 - Rel9.0 CR-831 - End
				     	    
				     	 //Leelavathi - 10thDec2012 - Rel8200 CR-737 - Start
				     	   termsDoc.setAttribute("/Terms/PaymentTerms/TenorSpecialText", terms.getAttribute("special_tenor_text"));
				     		ComponentList pmtTermsTenorDtlList = (ComponentList) terms.getComponentHandle("PmtTermsTenorDtlList");
				     		int pmtTermsCount = pmtTermsTenorDtlList.getObjectCount();
				     		int pmtTermsIndex = 0;
				     		for ( pmtTermsIndex = 0; pmtTermsIndex < pmtTermsCount; pmtTermsIndex++){
				    			pmtTermsTenorDtlList.scrollToObjectByIndex(pmtTermsIndex);
				    			termsDoc.setAttribute("/Terms/PaymentTerms/FullPaymentTerms(" + pmtTermsIndex + ")/Amount", pmtTermsTenorDtlList.getListAttribute("amount"));
				    			termsDoc.setAttribute("/Terms/PaymentTerms/FullPaymentTerms(" + pmtTermsIndex + ")/Percent", pmtTermsTenorDtlList.getListAttribute("percent"));
				    			termsDoc.setAttribute("/Terms/PaymentTerms/FullPaymentTerms(" + pmtTermsIndex + ")/Days", pmtTermsTenorDtlList.getListAttribute("num_days_after"));
				    			termsDoc.setAttribute("/Terms/PaymentTerms/FullPaymentTerms(" + pmtTermsIndex + ")/DateMaturity", pmtTermsTenorDtlList.getListAttribute("maturity_date"));
				    			termsDoc.setAttribute("/Terms/PaymentTerms/FullPaymentTerms(" + pmtTermsIndex + ")/TenorType",	pmtTermsTenorDtlList.getListAttribute("tenor_type"));
				    			termsDoc.setAttribute("/Terms/PaymentTerms/FullPaymentTerms(" + pmtTermsIndex + ")/TenorDetailType",	pmtTermsTenorDtlList.getListAttribute("days_after_type"));
				    			}
				     		termsDoc.setAttribute("/Terms/PaymentTerms/TotalNumberOfEntries", String.valueOf(pmtTermsIndex));
				     		//Leelavathi - 10thDec2012 - Rel8200 CR-737 - End
			             //Docs_Required
			            termsDoc.setAttribute("/Terms/AdditionalDocumentText",				termsValuesDoc.getAttribute("/Terms/addl_doc_text"));
			             //End of DocsRequired

			             //Instructions
			             termsDoc.setAttribute("/Terms/Instructions/InstructionsToBank",			termsValuesDoc.getAttribute("/Terms/special_bank_instructions"));

			       		//SettlementInstructions
			 		termsDoc.setAttribute("/Terms/Instructions/SettlementInstructions/BranchCode",					        termsValuesDoc.getAttribute("/Terms/branch_code"));
			 		termsDoc.setAttribute("/Terms/Instructions/SettlementInstructions/SettlementOurAccountNumber",			termsValuesDoc.getAttribute("/Terms/settlement_our_account_num"));
			   		termsDoc.setAttribute("/Terms/Instructions/SettlementInstructions/SettlementForeignAccountNumber",		termsValuesDoc.getAttribute("/Terms/settlement_foreign_acct_num"));
			   		termsDoc.setAttribute("/Terms/Instructions/SettlementInstructions/SettlementForeignAccountCurrency",	termsValuesDoc.getAttribute("/Terms/settlement_foreign_acct_curr"));
			                //End of SettlementInstructions

			                //CommissionAndCharges
			 		termsDoc.setAttribute("/Terms/Instructions/CommissionAndCharges/CommissionChargesOurAccountNumber",			termsValuesDoc.getAttribute("/Terms/coms_chrgs_our_account_num"));
			 		termsDoc.setAttribute("/Terms/Instructions/CommissionAndCharges/CommissionChargesForeignAccountNumber",		termsValuesDoc.getAttribute("/Terms/coms_chrgs_foreign_acct_num"));
			   		termsDoc.setAttribute("/Terms/Instructions/CommissionAndCharges/CommissionChargesForeignAccountCurrency",	termsValuesDoc.getAttribute("/Terms/coms_chrgs_foreign_acct_curr"));
			   		termsDoc.setAttribute("/Terms/Instructions/CommissionAndCharges/CommissionChargesOtherText",			    termsValuesDoc.getAttribute("/Terms/coms_chrgs_other_text"));
			              //End of CommissionCharges

			          //End of Instructions

			         //PaymentTerms
			         termsDoc.setAttribute("/Terms/PaymentTerms/PaymentTermsType",			termsValuesDoc.getAttribute("/Terms/pmt_terms_type"));
			             //End of PaymentTerms

			          //BankCharges
			           termsDoc.setAttribute("/Terms/BankCharges/BankChargesType",			termsValuesDoc.getAttribute("/Terms/bank_charges_type"));
			          //End of BankCharges

			         //OtherConditions
			             termsDoc.setAttribute("/Terms/OtherConditions/ConfirmationType",		termsValuesDoc.getAttribute("/Terms/confirmation_type"));
			             termsDoc.setAttribute("/Terms/OtherConditions/AutoExtend",			    termsValuesDoc.getAttribute("/Terms/auto_extend"));
			             termsDoc.setAttribute("/Terms/OtherConditions/AdditionalConditions",	termsValuesDoc.getAttribute("/Terms/additional_conditions"));
                     //End of OtherConditions


	           //Terms Party
               DocumentHandler termsPartyDoc = new DocumentHandler();

                //Additional Terms Party
                String testTP = TermsPartyType.APPLICANT;


             int id = 0;
            termsPartyDoc.setAttribute("/TermsParty/TotalNumberOfEntries","0");

            for (int i = 0; i<TradePortalConstants.MAX_NUM_TERMS_PARTIES;i++)
   			 {

				String termsPartyOID = termsValuesDoc.getAttribute("/Terms/"+TradePortalConstants.TERMS_PARTY_ATTRIBUTE_NAMES[i]);
   		      if(!(termsPartyOID == null || termsPartyOID.equals("")))
   		      {
   				 TermsParty termsParty = (TermsParty)(terms.getComponentHandle(TradePortalConstants.TERMS_PARTY_COMPONENT_NAMES[i]));
   				 if(termsParty.getAttribute("terms_party_type").equals(testTP))
   				 {
   					 LOG.debug("party_Type is APP, hence I need to check for thirdParty");
   					 termsPartyDoc = packageAdditionalTermsParty(terms,id,termsPartyDoc,termsParty);
   					 if(termsPartyDoc.getAttribute("/AdditionalTermsParty").equals("Y"))
   					  id++;
   				 }
   				 termsPartyDoc.setAttribute("/AdditionalTermsParty","N");
   				 //If the termsValuesDoc contains termsParty also need to modify the line below
   			     termsPartyDoc = termsParty.packageTermsPartyAttributes(id,termsPartyDoc);
   			     id++;
   			   }
		      }

		 LOG.debug("This is TermsPartyDoc {}" ,  termsPartyDoc);

         termsPartyDoc.setAttribute("/TermsParty/TotalNumberOfEntries",String.valueOf(id));
         termsDoc.setComponent("/TermsParty",termsPartyDoc.getFragment("/TermsParty"));


   		return termsDoc;
   }

  //End of Code by Indu Valavala

  //End of Class
 }