package com.ams.tradeportal.busobj.termsmgr;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import com.amsinc.ecsg.frame.*;
import com.ams.tradeportal.busobj.*;
import com.ams.tradeportal.busobj.webbean.*;
import com.ams.tradeportal.common.*;
import java.rmi.*;
import com.amsinc.ecsg.util.*;
import com.ams.tradeportal.busobj.util.*;

/**
 * This class manages the dynamic attribute registration and validation
 * for a Terms business object or TermsWebBean for an Export Collection Amendment
 *
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class EXP_COL__TRCTermsMgr extends TermsMgr
 {
private static final Logger LOG = LoggerFactory.getLogger(EXP_COL__TRCTermsMgr.class);
    /**
     *  Constructor that is used by business objects to create their
     *  manager.  This method also register the dynamic attributes of
     *  the bean.
     *
     *  @param mgr AttributeManager attribute manager of the bean being managed.  Used to register attributes
     */
    public EXP_COL__TRCTermsMgr(AttributeManager mgr) throws AmsException
     {
	  super(mgr);
     }

    /**
     *  Constructor that is used by web beans to create their
     *  manager.  This method also register the dynamic attributes of
     *  the bean.
     *
     *  @param mgr TermsWebBean - the bean being managed.  Used to register attributes.
     */
    public EXP_COL__TRCTermsMgr(TermsWebBean terms) throws AmsException
     {
	  super(terms);
     }

    /**
     *  Returns a list of attributes.  These describe each of the attributes
     *  that will be registered for the bean being managed.
     *
     *  This method controls which attributes of TermsBean or TermsWebBean are registered
     *  for an this instrument and transaction type
     *
     *  @return Vector - list of attributes
     */
                      public List<Attribute> getAttributesToRegister()
     {
        List<Attribute> attrList = super.getAttributesToRegister();
        attrList.add( TermsBean.Attributes.create_amount() );
        attrList.add( TermsBean.Attributes.create_amount_currency_code() );
        attrList.add( TermsBean.Attributes.create_collection_date() );
        attrList.add( TermsBean.Attributes.create_reference_number() );
        attrList.add( TermsBean.Attributes.create_special_bank_instructions() );
        attrList.add( TermsBean.Attributes.create_tracer_details() );
        attrList.add( TermsBean.Attributes.create_tracer_send_type() );
        attrList.add( TermsBean.Attributes.create_work_item_number() );
        /*
         * Adding Tenor to Vector list-Start Rakesh Pasupulati
         * 
         * */
        attrList.add( TermsBean.Attributes.create_tenor_1() );
        attrList.add( TermsBean.Attributes.create_tenor_1_amount() );
        attrList.add( TermsBean.Attributes.create_tenor_1_draft_number());

        attrList.add( TermsBean.Attributes.create_tenor_2() );
        attrList.add( TermsBean.Attributes.create_tenor_2_amount() );
        attrList.add( TermsBean.Attributes.create_tenor_2_draft_number());

        attrList.add( TermsBean.Attributes.create_tenor_3() );
        attrList.add( TermsBean.Attributes.create_tenor_3_amount() );
        attrList.add( TermsBean.Attributes.create_tenor_3_draft_number());

        attrList.add( TermsBean.Attributes.create_tenor_4() );
        attrList.add( TermsBean.Attributes.create_tenor_4_amount() );
        attrList.add( TermsBean.Attributes.create_tenor_4_draft_number());

        attrList.add( TermsBean.Attributes.create_tenor_5() );
        attrList.add( TermsBean.Attributes.create_tenor_5_amount() );
        attrList.add( TermsBean.Attributes.create_tenor_5_draft_number());

        attrList.add( TermsBean.Attributes.create_tenor_6() );
        attrList.add( TermsBean.Attributes.create_tenor_6_amount() );
        attrList.add( TermsBean.Attributes.create_tenor_6_draft_number());
        /*
         * End -Rakesh Pasupulati
         */

        return attrList;
     }




	/**
     * Performs presave processing (regardless of whether validation is done)
     *
     * @param terms com.ams.tradeportal.busobj.termsBean
     * @exception com.amsinc.ecsg.frame.AmsException The exception description.
     */
    public void preSave(TermsBean terms) throws AmsException
    {
       super.preSave(terms);

	    try {
            //The text length check is done in the presave to guard against
            //a possible problem with saving the data into the DB if it is too long.
		    validateTextLengths(terms);

	    } catch (java.rmi.RemoteException e) {
		    LOG.info("Remote Exception caught in EXP_COL__AMDTermsMgr.preSave()");
	    }

    }



    /**
     *  Performs validation of text fields for length.
     *
     *  @param terms TermsBean - the bean being validated
     */
    public void validateTextLengths(TermsBean terms)
	    throws java.rmi.RemoteException, AmsException {

        checkTextLength(terms, "tracer_details", 1000);
	    checkTextLength(terms, "special_bank_instructions", 1000);
    }

     /**
      * Performs the Terms Manager-specific validation of text fields in the Terms
      * and TermsParty objects.  
      *
      * @param terms - the managed object
      *
      */
     protected void validateForSwiftCharacters(TermsBean terms) throws AmsException, RemoteException
      {
         final String[] attributesToExclude = {"special_bank_instructions"};

         InstrumentServices.checkForInvalidSwiftCharacters(terms.getAttributeHash(), terms.getErrorManager(), attributesToExclude);
      }




    /**
     *  Performs validation of the attributes of this type of instrument and transaction.   This method is
     *  called from the userValidate() hook in the managed object.
     *
     *  @param terms - the bean being validated
     */
    public void validate(TermsBean terms)
			throws java.rmi.RemoteException, AmsException
     {
        super.validate(terms);

        String value  = terms.getAttribute("tracer_send_type");

        if( StringFunction.isBlank( value ) )
        {
            terms.getErrorManager().issueError(
							TradePortalConstants.ERR_CAT_1,
			                TradePortalConstants.PLEASE_SPECIFY_INSTR_TO_BANK);
        }
     }



   //Beginning of code used by the Middleware team
     /**
 	 *  gets the values of the attributes of the managed object. This method is called
 	 *  from the getTermsAttributesDoc() hook in the managed object.
 	 *
 	 *  return com.amsinc.ecsg.util.DocumentHandler
 	 *  @param terms TermsBean - the bean being validated
 	 *  @param termsDoc com.amsinc.ecsg.util.DocumentHandler
 	 */
    public DocumentHandler getManagerTerms(TermsBean terms, DocumentHandler termsDoc) throws java.rmi.RemoteException, AmsException

    {

    	      DocumentHandler termsValuesDoc = terms.populateXmlDoc(new DocumentHandler());

             //Terms_Details
              termsDoc.setAttribute("/Terms/TermsDetails/ReferenceNumber",	        termsValuesDoc.getAttribute("/Terms/reference_number"));
              termsDoc.setAttribute("/Terms/TermsDetails/AmountCurrencyCode",       termsValuesDoc.getAttribute("/Terms/amount_currency_code"));
              termsDoc.setAttribute("/Terms/TermsDetails/Amount",                   termsValuesDoc.getAttribute("/Terms/amount"));
              //End of Terms_Details

             //Instructions
               termsDoc.setAttribute("/Terms/Instructions/InstructionsToBank",		termsValuesDoc.getAttribute("/Terms/special_bank_instructions"));
  	           //TracerDetails
               termsDoc.setAttribute("/Terms/Instructions/TracerDetails",			termsValuesDoc.getAttribute("/Terms/tracer_details"));
              	//End of TracerDetails
   	           //TracerSendType
               termsDoc.setAttribute("/Terms/Instructions/TracerSendType",			termsValuesDoc.getAttribute("/Terms/tracer_send_type"));
              	//End of TracerSendType
              //End of Instructions


    		return termsDoc;
    }

  //End of Code by Indu Valavala

 }