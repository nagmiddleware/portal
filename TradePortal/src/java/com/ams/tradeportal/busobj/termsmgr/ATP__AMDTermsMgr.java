package com.ams.tradeportal.busobj.termsmgr;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

import com.amsinc.ecsg.frame.*;
import com.ams.tradeportal.busobj.*;
import com.ams.tradeportal.busobj.webbean.*;
import com.ams.tradeportal.common.*;
import java.rmi.*;
import com.amsinc.ecsg.util.*;
import com.ams.tradeportal.busobj.util.*;

/**
 * This class manages the dynamic attribute registration and validation
 * for a Terms business object or TermsWebBean for an Approval to Pay Amendment
 *
 *  
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class ATP__AMDTermsMgr extends TermsMgr
{
private static final Logger LOG = LoggerFactory.getLogger(ATP__AMDTermsMgr.class);
	
	/**
	 *  Constructor that is used by business objects to create their
	 *  manager.  This method also register the dynamic attributes of
	 *  the bean.
	 *
	 *  @param mgr AttributeManager attribute manager of the bean being managed.  Used to register attributes
	 */
	public ATP__AMDTermsMgr(AttributeManager mgr) throws AmsException
	{
		super(mgr);
	}
	
	/**
	 *  Constructor that is used by web beans to create their
	 *  manager.  This method also register the dynamic attributes of
	 *  the bean.
	 *
	 *  @param mgr TermsWebBean - the bean being managed.  Used to register attributes.
	 */
	public ATP__AMDTermsMgr(TermsWebBean terms) throws AmsException
	{
		super(terms);
	}

	/**
	 *  Returns a list of attributes.  These describe each of the attributes
	 *  that will be registered for the bean being managed.
	 *
	 *  This method controls which attributes of TermsBean or TermsWebBean are registered
	 *  for an this instrument and transaction type
	 *
	 *  @return Vector - list of attributes
	 */
	public List<Attribute> getAttributesToRegister()
	{
		List<Attribute> attrList = super.getAttributesToRegister();
		attrList.add( TermsBean.Attributes.create_additional_conditions() );
		attrList.add( TermsBean.Attributes.create_amount() );
		attrList.add( TermsBean.Attributes.create_amount_currency_code() );
		attrList.add( TermsBean.Attributes.create_amt_tolerance_neg() );
		attrList.add( TermsBean.Attributes.create_amt_tolerance_pos() );
		attrList.add( TermsBean.Attributes.create_expiry_date() );
		attrList.add( TermsBean.Attributes.create_reference_number() );
		attrList.add( TermsBean.Attributes.create_special_bank_instructions() );
		attrList.add( TermsBean.Attributes.create_work_item_number() );
		attrList.add( TermsBean.Attributes.create_invoice_due_date() );
		attrList.add( TermsBean.Attributes.create_invoice_only_ind() );
		attrList.add( TermsBean.Attributes.create_invoice_details() );//SHR CR 708 Rel8.1.1
		//Srinivasu IR#T36000023986 Rel8.4 01/21/2014 - start
		attrList.add( TermsBean.Attributes.create_guar_deliver_by() );
		attrList.add( TermsBean.Attributes.create_guar_deliver_to() );
		//Srinivasu IR#T36000023986 Rel8.4 01/21/2014 - end
		return attrList;
	}





	/**
	 *  Performs validation of the attributes of this type of instrument and transaction.   This method is
	 *  called from the userValidate() hook in the managed object.
	 *
	 *  @param terms - the bean being validated
	 */
	public void validate(TermsBean terms)
	throws java.rmi.RemoteException, AmsException
	{
		// Variables used to retrieve the shipment terms
		ShipmentTerms shipmentTerms = null;

		super.validate(terms);

		// Retrieve the shipment terms component
		shipmentTerms = terms.getFirstShipment();

		// If all of these fields are blank, issue an error
		if( StringFunction.isBlank(terms.getAttribute("amt_tolerance_neg")) &&
				StringFunction.isBlank(terms.getAttribute("amt_tolerance_pos")) &&
				StringFunction.isBlank(terms.getAttribute("amount")) &&
				StringFunction.isBlank(terms.getAttribute("expiry_date")) &&
				StringFunction.isBlank(terms.getAttribute("special_bank_instructions")) &&
				StringFunction.isBlank(terms.getAttribute("additional_conditions")))
		{
			//Krishna DGUH060851916 shipment_from_loading(Airport of Departure)
			//and shipment_to_discharge(For Transport To) to the list.If all of the following
			// are left blank for which error would be issued.Atleast one of the above list or
			//below list is necessary to be filled in the Amend transaction page.
			if((shipmentTerms == null) ||
					(StringFunction.isBlank(shipmentTerms.getAttribute("latest_shipment_date")) &&
							StringFunction.isBlank(shipmentTerms.getAttribute("incoterm")) &&
							StringFunction.isBlank(shipmentTerms.getAttribute("goods_description")) &&
							StringFunction.isBlank(shipmentTerms.getAttribute("incoterm_location")) &&
							StringFunction.isBlank(shipmentTerms.getAttribute("shipment_from")) &&
							StringFunction.isBlank(shipmentTerms.getAttribute("shipment_to"))  &&
							StringFunction.isBlank(shipmentTerms.getAttribute("shipment_from_loading")) &&
							StringFunction.isBlank(shipmentTerms.getAttribute("shipment_to_discharge"))) )
			{
				terms.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.MUST_AMEND);
			}
		}

		String shipmentDate = null;
		String value;
		String description = "";
	

		// Expiry date must be after shipment date (if provided).
		// (Expiry date is a required field.  Shipment date is not.)
		if(shipmentTerms != null)
			shipmentDate = shipmentTerms.getAttribute("latest_shipment_date");
		value = terms.getAttribute("expiry_date");
		if (!StringFunction.isBlank(shipmentDate) && !StringFunction.isBlank(value)) {
			GregorianCalendar shipDate = new GregorianCalendar();
			GregorianCalendar expiryDate = new GregorianCalendar();

			shipDate.setTime(DateTimeUtility.convertStringDateTimeToDate(shipmentDate));
			expiryDate.setTime(DateTimeUtility.convertStringDateTimeToDate(value));
			LOG.debug("ship date {} - exp. date {} " , shipDate, expiryDate);
			// If the ship date is after the expiry date, give error
			if (shipDate.after(expiryDate)) {
				terms.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.SHIP_DATE_PAST_EXPIRY_DATE,
						description);
				LOG.debug("ship date after expiry");
			} else{
				LOG.debug("ship date before expiry");
			}

		}

		// Validate the Goods Description and PO Line Items fields
		if(shipmentTerms != null)
		validateGoodsDescriptionFields(shipmentTerms);

		// Validate the Amount currency code with the PO Line Items currency code.  They must match.
		validateAmountCurrencyAndPOCurrency(terms);

		validateOtherConditions(terms);
		//Krishna IR-RAUH101636054 11/23/2007 Begin
		validateForAmendTermsAmount(terms);
                //Krishna IR-RAUH101636054 11/23/2007 End
		//Srinivasu_D IR#T36000019036  Rel8.2 07/11/2013 - added invoice_ind/due_date cond check
		// in case of blank invoices need not to go thru below validatin.
		String invInd = terms.getAttribute("invoice_only_ind");
		String invDueDate = terms.getAttribute("invoice_due_date");		
		if((StringFunction.isNotBlank(invInd) && TradePortalConstants.INDICATOR_YES.equals(invInd)) && (StringFunction.isNotBlank(invDueDate))){
		// IR T36000017596 -validate expiry date
		validateInvDueAndExpiryDate(terms);
		}
	}
           //Krishna IR-RAUH101636054 11/23/2007 Begin
	/**
	 * Performs the Terms Manager-specific validity of the new ATP Amount in the Terms
	 *
	 * @param terms - the managed object
	 *
	 */
	protected void validateForAmendTermsAmount(TermsBean terms) throws AmsException, RemoteException
	{
	   String curAmt        = terms.getAttribute("amount");
	   String instrumentAmt = "";
	 //SSikhakolli - 05/06/2014 - Rel 9.0 IR#T36000026319 - SQL fix - Begin
		String instrAmntQuery = "select INSTRUMENT_AMOUNT from TRANSACTION where "
				+ "TRANSACTION_OID in(select ACTIVE_TRANSACTION_OID from INSTRUMENT, TRANSACTION where "
				+ "TRANSACTION.C_CUST_ENTER_TERMS_OID = ? and "
				+ "TRANSACTION.P_INSTRUMENT_OID = INSTRUMENT.INSTRUMENT_OID)";

       DocumentHandler instrumentDoc = DatabaseQueryBean.getXmlResultSet(instrAmntQuery, true,
    		   new Object[]{terms.getAttribute("terms_oid")});
     //SSikhakolli - 05/06/2014 - Rel 9.0 IR#T36000026319 - SQL fix - End
       
           if(instrumentDoc!=null)
	   instrumentAmt = instrumentDoc.getAttribute("/ResultSetRecord(0)/INSTRUMENT_AMOUNT");
	   //Krishna IR- WUUI011042560(Handling NumberFormatException) -Begin
	    double curAmtValue        = 0;
	    double instrumentAmtValue = 0;
	   try {
		   curAmtValue        = Double.parseDouble(curAmt);
		   instrumentAmtValue = Double.parseDouble(instrumentAmt);
	       } catch (NumberFormatException e) {

	      }
	    //Krishna IR-WUUI011042560 -End
	     //Newly claculated ATP Amend amount can not be negative
		if (curAmtValue+instrumentAmtValue<0 )
		{
			terms.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.NEGATIVE_AMOUNT);
		}
	}
        //Krishna IR-RAUH101636054 11/23/2007 End

	/**
	 * Performs the Terms Manager-specific validation of text fields in the Terms
	 * and TermsParty objects.
	 *
	 * @param terms - the managed object
	 *
	 */
	protected void validateForSwiftCharacters(TermsBean terms) throws AmsException, RemoteException
	{
		final String[] attributesToExclude = {"special_bank_instructions"};

		InstrumentServices.checkForInvalidSwiftCharacters(terms.getAttributeHash(), terms.getErrorManager(), attributesToExclude);

		ShipmentTerms shipmentTerms = terms.getFirstShipment();
		if(shipmentTerms != null)
		{
			InstrumentServices.checkForInvalidSwiftCharacters(shipmentTerms.getAttributeHash(), shipmentTerms.getErrorManager());
		}
	}

	/**
	 *  Performs validation of the other Conditions section of the page
	 *
	 *  @param terms TermsBean - the bean being validated
	 */

	public void validateOtherConditions(TermsBean terms) throws java.rmi.RemoteException, AmsException
	{

		// pcutrone - Rolled back IR-RIUH1016488248
//		pcutrone - Rolled back IR-RIUH1016488248 - END
	}



	/**
	 *  Performs validation of text fields for length.
	 *
	 *  @param terms TermsBean - the bean being validated
	 */
	public void validateTextLengths(TermsBean terms) throws java.rmi.RemoteException, AmsException
	{
		String overrideSwiftLengthInd = getOverrideSwiftLengthInd(terms);

		if((overrideSwiftLengthInd != null)
				&& (overrideSwiftLengthInd.equals(TradePortalConstants.INDICATOR_NO)))
		{
			// SRUG020846202 W Zhu 4/11/06 do not check goods_description here.
			// It's checked in validateGoodsDescriptionFields
			//checkTextLength(terms.getFirstShipment(), "goods_description", 6500);
			checkTextLength(terms, "additional_conditions",     1000);
		} 
		//Vshah - 6/16/08 - PPX-067 - Change the third argument in below finction call to 3000 from 1000.		
		checkTextLength(terms, "special_bank_instructions", 3000);
	}
	
	/**
	 * This method is used to validate the Goods Description and PO Line Items
	 * fields.  If data has been entered in the Goods Description text area box, the method
	 * will issue an error if its length exceeds 6500 characters. If the length
	 * does NOT exceed 6500 characters and data has been enetered in the PO Line
	 * Items text area box, it will then determine whether the combined lengths are
	 * greater than 6500 characters; an appropriate error message will be issued if
	 * this total is greater than 6500.
	 *

	 * @exception  com.amsinc.ecsg.frame.AmsException
	 * @exception  java.rmi.RemoteException
	 * @return     void
	 */
	public void validateGoodsDescriptionFields(ShipmentTerms shipmentTerms) throws RemoteException, AmsException
	{
		String   goodsDescription = null;
		String   poLineItems      = null;
		String   endText          = null;
		String   description      = "";
		
		goodsDescription = shipmentTerms.getAttribute("goods_description");
		poLineItems      = shipmentTerms.getAttribute("po_line_items");
		
		String overrideSwiftLengthInd = getOverrideSwiftLengthInd(shipmentTerms);
		
		// If nothing has been entered in the Goods Description text area box, determine whether
		// data was entered in the PO Line Items text area box
		if (StringFunction.isBlank(goodsDescription))
		{
			if ((poLineItems.length() > 6500)
					&& (overrideSwiftLengthInd != null)
					&& (overrideSwiftLengthInd.equals(TradePortalConstants.INDICATOR_NO)))
			{
				shipmentTerms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.PO_LINE_ITEMS_EXCEED_MAX_CHARS,
						description);
			}
		}
		else
		{
			// If the number of characters in the Goods Description text area box is greater
			// than 6500, issue an approrpiate error; otherwise, check the combined lengths of
			// the Goods Description and PO Line Items text area boxes and issue an error if
			// this total exceeds 6500 characters
			if ((goodsDescription.length() > 6500)
					&& (overrideSwiftLengthInd != null)
					&& (overrideSwiftLengthInd.equals(TradePortalConstants.INDICATOR_NO)))
			{
				endText = goodsDescription.substring(6480, 6500);

				shipmentTerms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.TEXT_TOO_LONG_SHOULD_END,
						shipmentTerms.getAlias("goods_description"), endText,
						description);

			}
			else
			{
				if ((!StringFunction.isBlank(poLineItems))
						&& ((goodsDescription.length() + poLineItems.length()) > 6500)
						&& (overrideSwiftLengthInd != null)
						&& (overrideSwiftLengthInd.equals(TradePortalConstants.INDICATOR_NO)))
				{
					shipmentTerms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
							TradePortalConstants.PO_LINE_ITEMS_EXCEED_MAX_CHARS,
							description);
				}
			}
		}
	}

	/**
	 * This method is used to validate the currency Code of the Transaction Amount
	 * with the Currency Code of the PO Line Items.  The two must match.  The PO Line Items
	 * will always have the same currency code as per the PO design validations, therefore, we
	 * only need to determine what that single currency code is and compare it to the currency
	 * code of the transaction.
	 *

	 * @exception  com.amsinc.ecsg.frame.AmsException
	 * @exception  java.rmi.RemoteException
	 * @return     void
	 */
	public void validateAmountCurrencyAndPOCurrency(TermsBean terms) throws RemoteException, AmsException
	{
		String   amountCurrency	  = null;

		// Get the currency code of the transaction.  If not blank, proceed with validating that it is
		// the same as the PO line items currency code, otherwise fall through and let the required
		// validation process flag it.
		amountCurrency = terms.getAttribute("amount_currency_code");

		if ((amountCurrency != null) && (!amountCurrency.equals("")))
		{
			// Query the database to get the currency code of the PO line items.
			LOG.debug("Inside validateAmountCurrencyAndPOCurrency() - Getting currency code from po_line_item using sql statement.");

			DocumentHandler    poLineItemCurrencyDoc       = null;
			String             poLineItemCurrencyCode 	   = null;
			String 			   termsOid 			   	   = terms.getAttribute("terms_oid");


			String sqlQuery = "select distinct p.currency "
			                 + "from po_line_item p, terms t, transaction r " 
					         +  " where t.terms_oid = r.c_cust_enter_terms_oid " 
			                 +  " and r.transaction_oid = p.a_assigned_to_trans_oid "
			                 + " and t.terms_oid = ?";


			// If value found, compare the PO line item value to the amountCurrency value.
			// If nothing found, then no PO line items have been added and we do not need to continue as there is
			// nothing for us to validate against.
			poLineItemCurrencyDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery, true, new Object[]{termsOid});

			if (poLineItemCurrencyDoc != null)
			{
				poLineItemCurrencyCode = poLineItemCurrencyDoc.getAttribute("/ResultSetRecord(0)/CURRENCY");

				if ((poLineItemCurrencyCode != null) && (!poLineItemCurrencyCode.equals("")))
				{
					if (!poLineItemCurrencyCode.equals(amountCurrency))
					{
						terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
								TradePortalConstants.PO_ITEM_CURR_NOT_SAME_AS_TRANS);
					}
				}
			}
		}
	}



	/**
	 * Performs presave processing (regardless of whether validation is done)
	 *
	 * @param terms com.ams.tradeportal.busobj.termsBean
	 * @exception com.amsinc.ecsg.frame.AmsException The exception description.
	 */
	public void preSave(TermsBean terms) throws AmsException
	{
		super.preSave(terms);

		try
		{
			validateTextLengths(terms);

			validateAmount(terms, "amount", "amount_currency_code");
			
		}
		catch (RemoteException e)
		{
			LOG.error("Remote Exception caught in ATP__AMDTermsMgr.preSave()", e);
		}
	}



	//Beginning of code used by the Middleware team
	/**
	 *  gets the values of the attributes of the managed object. This method is called
	 *  from the getTermsAttributesDoc() hook in the managed object.
	 *
	 *  return com.amsinc.ecsg.util.DocumentHandler
	 *  @param terms TermsBean - the bean being validated
	 *  @param termsDoc com.amsinc.ecsg.util.DocumentHandler
	 */
	public DocumentHandler getManagerTerms(TermsBean terms, DocumentHandler termsDoc) throws java.rmi.RemoteException, AmsException

	{

		DocumentHandler termsValuesDoc = terms.populateXmlDoc(new DocumentHandler());

		//Terms_Details
		termsDoc.setAttribute("/Terms/TermsDetails/ExpiryDate",				 termsValuesDoc.getAttribute("/Terms/expiry_date"));
		termsDoc.setAttribute("/Terms/TermsDetails/Amount",                     termsValuesDoc.getAttribute("/Terms/amount"));
		termsDoc.setAttribute("/Terms/TermsDetails/AmountTolerancePositive",	 termsValuesDoc.getAttribute("/Terms/amt_tolerance_pos"));
		termsDoc.setAttribute("/Terms/TermsDetails/AmountToleranceNegative",	 termsValuesDoc.getAttribute("/Terms/amt_tolerance_neg"));
		//End of Terms_Details

		// ShipmentTerms
		populateFirstShipmentTermsXml(terms, termsDoc);
		// End of ShipmentTerms

		//OtherConditions
		termsDoc.setAttribute("/Terms/OtherConditions/AdditionalConditions",			termsValuesDoc.getAttribute("/Terms/additional_conditions"));
		termsDoc.setAttribute("/Terms/OtherConditions/InvoiceDueDate",                 termsValuesDoc.getAttribute("/Terms/invoice_due_date"));
		//End of Other_Conditions

		//Instructions
		termsDoc.setAttribute("/Terms/Instructions/InstructionsToBank",			termsValuesDoc.getAttribute("/Terms/special_bank_instructions"));
		//End of Instructions

		return termsDoc;
	}

	//End of Code by Indu Valavala
	
	// IR T36000017596 - Validate the ISS expiry date is equal to AMD expiry date
	private void validateInvDueAndExpiryDate(TermsBean terms)
			throws AmsException, RemoteException {
		String expDate = "";
		String instrAmntQuery = "select COPY_OF_EXPIRY_DATE from TRANSACTION,INSTRUMENT where "
		                       + " TRANSACTION.C_CUST_ENTER_TERMS_OID =? and "
		                       + " TRANSACTION.P_INSTRUMENT_OID = INSTRUMENT.INSTRUMENT_OID";

		DocumentHandler instrumentDoc = DatabaseQueryBean.getXmlResultSet(instrAmntQuery, false, new Object[]{terms.getAttribute("terms_oid")});
		if (instrumentDoc != null) {
			expDate = instrumentDoc
					.getAttribute("/ResultSetRecord(0)/COPY_OF_EXPIRY_DATE");
			if (!StringFunction.isBlank(terms.getAttribute("expiry_date"))
					&& !StringFunction.isBlank(expDate)) {
				GregorianCalendar newExpDate = new GregorianCalendar();
				GregorianCalendar expiryDate = new GregorianCalendar();

				newExpDate.setTime(DateTimeUtility
						.convertStringDateTimeToDate(terms
								.getAttribute("expiry_date")));
				expiryDate.setTime(DateTimeUtility
						.convertStringDateTimeToDate(TPDateTimeUtility.convertDBISODateToJPylonDate(expDate)));
				if (!(newExpDate.equals(expiryDate))) {
					terms.getErrorManager().issueError(
							TradePortalConstants.ERR_CAT_1,
							TradePortalConstants.EXP_DUE_DATE_UNMATCH);
				}
				
			}

		}
	}
	// IR T36000017596 - end


}