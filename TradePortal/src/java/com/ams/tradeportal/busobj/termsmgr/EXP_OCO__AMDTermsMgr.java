package com.ams.tradeportal.busobj.termsmgr;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.rmi.*;
import java.math.*;
import com.amsinc.ecsg.frame.*;
import com.ams.tradeportal.busobj.*;
import com.ams.tradeportal.busobj.webbean.*;
import com.amsinc.ecsg.util.*;
import com.ams.tradeportal.common.*;
import com.ams.tradeportal.busobj.util.*;


/**
 * This class manages the dynamic attribute registration and validation
 * for a Terms business object or TermsWebBean for an Export Collection Amendment
 *
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 *     Vasavi CR 524 03/31/2010
 */
public class EXP_OCO__AMDTermsMgr extends TermsMgr
 {
private static final Logger LOG = LoggerFactory.getLogger(EXP_OCO__AMDTermsMgr.class);
    public final static int NUMBER_OF_TENORS = 6;

    /**
     *  Constructor that is used by business objects to create their
     *  manager.  This method also register the dynamic attributes of
     *  the bean.
     *
     *  @param mgr AttributeManager attribute manager of the bean being managed.  Used to register attributes
     */
    public EXP_OCO__AMDTermsMgr(AttributeManager mgr) throws AmsException
     {
	  super(mgr);
     }

    /**
     *  Constructor that is used by web beans to create their
     *  manager.  This method also register the dynamic attributes of
     *  the bean.
     *
     *  @param mgr TermsWebBean - the bean being managed.  Used to register attributes.
     */
    public EXP_OCO__AMDTermsMgr(TermsWebBean terms) throws AmsException
     {
	  super(terms);
     }

    /**
     *  Returns a list of attributes.  These describe each of the attributes
     *  that will be registered for the bean being managed.
     *
     *  This method controls which attributes of TermsBean or TermsWebBean are registered
     *  for an this instrument and transaction type
     *
     *  @return Vector - list of attributes
     */
    public List<Attribute> getAttributesToRegister()
     {
        List<Attribute> attrList = super.getAttributesToRegister();
        attrList.add( TermsBean.Attributes.create_amendment_details() );
        attrList.add( TermsBean.Attributes.create_amount() );
        attrList.add( TermsBean.Attributes.create_amount_currency_code() );
        attrList.add( TermsBean.Attributes.create_bank_action_required() );
        attrList.add( TermsBean.Attributes.create_collection_date() );
        attrList.add( TermsBean.Attributes.create_reference_number() );
        attrList.add( TermsBean.Attributes.create_special_bank_instructions() );
        attrList.add( TermsBean.Attributes.create_tenor_1() );
        attrList.add( TermsBean.Attributes.create_tenor_1_amount() );
        attrList.add( TermsBean.Attributes.create_tenor_1_draft_number() );
        attrList.add( TermsBean.Attributes.create_tenor_2() );
        attrList.add( TermsBean.Attributes.create_tenor_2_amount() );
        attrList.add( TermsBean.Attributes.create_tenor_2_draft_number() );
        attrList.add( TermsBean.Attributes.create_tenor_3() );
        attrList.add( TermsBean.Attributes.create_tenor_3_amount() );
        attrList.add( TermsBean.Attributes.create_tenor_3_draft_number() );
        attrList.add( TermsBean.Attributes.create_tenor_4() );
        attrList.add( TermsBean.Attributes.create_tenor_4_amount() );
        attrList.add( TermsBean.Attributes.create_tenor_4_draft_number() );
        attrList.add( TermsBean.Attributes.create_tenor_5() );
        attrList.add( TermsBean.Attributes.create_tenor_5_amount() );
        attrList.add( TermsBean.Attributes.create_tenor_5_draft_number() );
        attrList.add( TermsBean.Attributes.create_tenor_6() );
        attrList.add( TermsBean.Attributes.create_tenor_6_amount() );
        attrList.add( TermsBean.Attributes.create_tenor_6_draft_number() );
        attrList.add( TermsBean.Attributes.create_work_item_number() );
        

        return attrList;
     }




	/**
     * Performs presave processing (regardless of whether validation is done)
     *
     * @param terms com.ams.tradeportal.busobj.termsBean
     * @exception com.amsinc.ecsg.frame.AmsException The exception description.
     */
    public void preSave(TermsBean terms) throws AmsException
    {
       super.preSave(terms);

	    String value;

	    try {
            //The text length check is done in the presave to guard against
            //a possible problem with saving the data into the DB if it is too long.
		    validateTextLengths(terms);

		    //Check all amount entry fields
		    validateAmount(terms, "amount", "amount_currency_code");

            //including tenor amounts if they are non-null --Loop thru all 6
            for(int x=1; x <= NUMBER_OF_TENORS; x++)
            {
                value = terms.getAttribute("tenor_" + x + "_amount");
                if( !StringFunction.isBlank(value) )
                {
		            validateAmount(terms, "tenor_" + x + "_amount", "original_trans_curr_code");
                }
            }


	    } catch (java.rmi.RemoteException e) {
		    LOG.error("Remote Exception caught in EXP_OCO__AMDTermsMgr.preSave()",e);
	    }

    }

    /**
     * Performs the Terms Manager-specific validation of text fields in the Terms
     * and TermsParty objects.  
     *
     * @param terms - the managed object
     *
     */
    protected void validateForSwiftCharacters(TermsBean terms) throws AmsException, RemoteException
     {
        final String[] attributesToExclude = {"special_bank_instructions"};

        InstrumentServices.checkForInvalidSwiftCharacters(terms.getAttributeHash(), terms.getErrorManager(), attributesToExclude);
     }



    /**
     *  Performs validation of the attributes of this type of instrument and transaction.   This method is
     *  called from the userValidate() hook in the managed object.
     *
     *  @param terms - the bean being validated
     */
    public void validate(TermsBean terms)
			throws java.rmi.RemoteException, AmsException
     {
        super.validate(terms);

        validateGeneral(terms);
        validateInstructionsToBank(terms);
        validateNewPaymentTerms(terms);
     }



    /**
     *  Performs validation of text fields for length.
     *
     *  @param terms TermsBean - the bean being validated
     */
    public void validateTextLengths(TermsBean terms)
	    throws java.rmi.RemoteException, AmsException {

        checkTextLength(terms, "amendment_details", 1000);
	    checkTextLength(terms, "special_bank_instructions", 1000);
    }



    /**
     *  Performs validation of the general section of the page
     *
     *  @param terms TermsBean - the bean being validated
     */
    public void validateGeneral(TermsBean terms)
	    throws java.rmi.RemoteException, AmsException {

	    String value  = terms.getAttribute("amount");
	    String value2 = terms.getAttribute("amendment_details");

        //If Both the amount field and the amendment detail field are balnk
        //Then issue an error - Bus. Rule #1
        if((StringFunction.isBlank( value )) &&
           (StringFunction.isBlank( value2 )) )
        {
            terms.getErrorManager().issueError(
							TradePortalConstants.ERR_CAT_1,
			                TradePortalConstants.MUST_AMEND);
        }

    }



    /**
     * Performs validation for the Instructions to Bank section.
     *
     * @param terms com.ams.tradeportal.busobj.termsBean
     */
    public void validateInstructionsToBank(TermsBean terms)
	    throws java.rmi.RemoteException, AmsException
	{
        String value  = terms.getAttribute("bank_action_required");

        if( StringFunction.isBlank( value ) )
        {
            terms.getErrorManager().issueError(
							TradePortalConstants.ERR_CAT_1,
			                TradePortalConstants.PLEASE_SPECIFY_INSTR_TO_BANK);
        }
    }


    /**
     * Performs validation for the New Payment Terms section.
     *
     * @param terms com.ams.tradeportal.busobj.termsBean
     */
    public void validateNewPaymentTerms(TermsBean terms)
	    throws java.rmi.RemoteException, AmsException
	{
        String tenorNum;
        String tenorAmount;
        String tenorDraftNum;
        String value;
        String value2;
        BigDecimal totalAmount = BigDecimal.ZERO;

        for(int x=1; x <= NUMBER_OF_TENORS; x++)
        {
            tenorNum = "tenor_" + x;
            tenorAmount = "tenor_" + x + "_amount";
            tenorDraftNum = "tenor_" + x + "_draft_number";

            if( (!StringFunction.isBlank(terms.getAttribute(tenorNum))     ) ||
                (!StringFunction.isBlank(terms.getAttribute(tenorAmount))  ) ||
                (!StringFunction.isBlank(terms.getAttribute(tenorDraftNum))))
            {
                //make sure there's a Tenor# entered
                if(StringFunction.isBlank(terms.getAttribute(tenorNum)) )
                {
			        value = terms.getResourceManager().getText( "ExportCollectionAmend.Tenor" + x,
				                                                TradePortalConstants.TEXT_BUNDLE);
			        terms.getErrorManager().issueError( TradePortalConstants.ERR_CAT_1,
			                                            AmsConstants.REQUIRED_ATTRIBUTE, value);
			    }

                //Make sure there's a tenor amount entered
                if(StringFunction.isBlank(terms.getAttribute(tenorAmount)) )
                {
			        value = terms.getResourceManager().getText( "ExportCollectionAmend.Tenor" + x,
				                                                TradePortalConstants.TEXT_BUNDLE);
			        value2 = terms.getResourceManager().getText( "ExportCollectionAmend.Amount",
				                                                TradePortalConstants.TEXT_BUNDLE);
			        value = value + " " + value2;
			        terms.getErrorManager().issueError( TradePortalConstants.ERR_CAT_1,
			                                            AmsConstants.REQUIRED_ATTRIBUTE, value);
                }
            }//Done checking this Tenor Row

        }//End of For Loop

    }


  //Beginning of code used by the Middleware team
    /**
	 *  gets the values of the attributes of the managed object. This method is called
	 *  from the getTermsAttributesDoc() hook in the managed object.
	 *
	 *  return com.amsinc.ecsg.util.DocumentHandler
	 *  @param terms TermsBean - the bean being validated
	 *  @param termsDoc com.amsinc.ecsg.util.DocumentHandler
	 */
   public DocumentHandler getManagerTerms(TermsBean terms, DocumentHandler termsDoc) throws java.rmi.RemoteException, AmsException

   {

   	         DocumentHandler termsValuesDoc = terms.populateXmlDoc(new DocumentHandler());
             LOG.debug("This is the termValuesDoc containing all the terms values ==> {}" ,  termsValuesDoc);
   	         String termOid = terms.getAttribute("terms_oid");
             String orginalAmountSQL = "SELECT INSTRUMENT_AMOUNT FROM TRANSACTION WHERE " +
                                       " TRANSACTION_OID IN (SELECT ACTIVE_TRANSACTION_OID FROM INSTRUMENT WHERE " +
                                       " INSTRUMENT_OID IN (SELECT P_INSTRUMENT_OID FROM TRANSACTION WHERE " +
                                       " C_CUST_ENTER_TERMS_OID = ?))";
              DocumentHandler resultsDoc = DatabaseQueryBean.getXmlResultSet(orginalAmountSQL,false, new Object[]{termOid});
              double amountDifference = 0;
              double newAmount      = 0;
              String snewAmount     = termsValuesDoc.getAttribute("/Terms/amount");
              double actualAmount   = 0;
              String  sactualAmount = "0";
              int rid = 0;
              if(snewAmount == null) snewAmount = "0";
              if(snewAmount.equals("")) snewAmount = "0";
                newAmount = Double.parseDouble(snewAmount);

              if(resultsDoc != null)
                sactualAmount =resultsDoc.getAttribute("/ResultSetRecord(" + rid + ")/INSTRUMENT_AMOUNT");
              if(sactualAmount == null) sactualAmount = "0";
              if(sactualAmount.equals("")) sactualAmount = "0";
              actualAmount  =  Double.parseDouble(sactualAmount);
              //Update amount appropriately.  If the user doesn't enter an amount, do not default to zero
              if(newAmount != 0){
              amountDifference = newAmount - actualAmount ;
              }
              else
              {
                amountDifference = 0;
              }

              //Terms_Details
              termsDoc.setAttribute("/Terms/TermsDetails/Amount",                    String.valueOf(amountDifference));
			  termsDoc.setAttribute("/Terms/TermsDetails/AmendmentDetails",	         termsValuesDoc.getAttribute("/Terms/amendment_details"));
			  termsDoc.setAttribute("/Terms/TermsDetails/BankActionRequired",	     termsValuesDoc.getAttribute("/Terms/bank_action_required"));
	          //End of Terms_Details

		     //Instructions
			   termsDoc.setAttribute("/Terms/Instructions/InstructionsToBank",			termsValuesDoc.getAttribute("/Terms/special_bank_instructions"));
			 //End of Instructions


			 //TenorTerms
		      termsDoc.setAttribute("/Terms/TermsDetails/TenorTerms/Tenor1Amount",			termsValuesDoc.getAttribute("/Terms/tenor_1_amount"));
			  termsDoc.setAttribute("/Terms/TermsDetails/TenorTerms/Tenor2Amount",			termsValuesDoc.getAttribute("/Terms/tenor_2_amount"));
		      termsDoc.setAttribute("/Terms/TermsDetails/TenorTerms/Tenor3Amount",			termsValuesDoc.getAttribute("/Terms/tenor_3_amount"));
	          termsDoc.setAttribute("/Terms/TermsDetails/TenorTerms/Tenor4Amount",			termsValuesDoc.getAttribute("/Terms/tenor_4_amount"));
			  termsDoc.setAttribute("/Terms/TermsDetails/TenorTerms/Tenor5Amount",			termsValuesDoc.getAttribute("/Terms/tenor_5_amount"));
	          termsDoc.setAttribute("/Terms/TermsDetails/TenorTerms/Tenor6Amount",			termsValuesDoc.getAttribute("/Terms/tenor_6_amount"));
			  termsDoc.setAttribute("/Terms/TermsDetails/TenorTerms/Tenor1",			    termsValuesDoc.getAttribute("/Terms/tenor_1"));
		      termsDoc.setAttribute("/Terms/TermsDetails/TenorTerms/Tenor2",			    termsValuesDoc.getAttribute("/Terms/tenor_2"));
	          termsDoc.setAttribute("/Terms/TermsDetails/TenorTerms/Tenor3",			    termsValuesDoc.getAttribute("/Terms/tenor_3"));
		      termsDoc.setAttribute("/Terms/TermsDetails/TenorTerms/Tenor4",			    termsValuesDoc.getAttribute("/Terms/tenor_4"));
		      termsDoc.setAttribute("/Terms/TermsDetails/TenorTerms/Tenor5",			    termsValuesDoc.getAttribute("/Terms/tenor_5"));
		      termsDoc.setAttribute("/Terms/TermsDetails/TenorTerms/Tenor6",			    termsValuesDoc.getAttribute("/Terms/tenor_6"));
              termsDoc.setAttribute("/Terms/TermsDetails/TenorTerms/TenorDraftNumber1",	    termsValuesDoc.getAttribute("/Terms/tenor_1_draft_number"));
		      termsDoc.setAttribute("/Terms/TermsDetails/TenorTerms/TenorDraftNumber2",		termsValuesDoc.getAttribute("/Terms/tenor_2_draft_number"));
		      termsDoc.setAttribute("/Terms/TermsDetails/TenorTerms/TenorDraftNumber3",		termsValuesDoc.getAttribute("/Terms/tenor_3_draft_number"));
			  termsDoc.setAttribute("/Terms/TermsDetails/TenorTerms/TenorDraftNumber4",		termsValuesDoc.getAttribute("/Terms/tenor_4_draft_number"));
			  termsDoc.setAttribute("/Terms/TermsDetails/TenorTerms/TenorDraftNumber5",		termsValuesDoc.getAttribute("/Terms/tenor_5_draft_number"));
	          termsDoc.setAttribute("/Terms/TermsDetails/TenorTerms/TenorDraftNumber6",		termsValuesDoc.getAttribute("/Terms/tenor_6_draft_number"));


	           //Terms Party

               DocumentHandler termsPartyDoc = new DocumentHandler();

                //Additional Terms Party
                String testTP = TermsPartyType.APPLICANT;


             int id = 0;

            termsPartyDoc.setAttribute("/TermsParty/TotalNumberOfEntries","0");

            for (int i = 0; i<TradePortalConstants.MAX_NUM_TERMS_PARTIES;i++)
   			 {
				String termsPartyOID = termsValuesDoc.getAttribute("/Terms/"+TradePortalConstants.TERMS_PARTY_ATTRIBUTE_NAMES[i]);

   		      if(!(termsPartyOID == null || termsPartyOID.equals("")))
   		      {
   				 TermsParty termsParty = (TermsParty)(terms.getComponentHandle(TradePortalConstants.TERMS_PARTY_COMPONENT_NAMES[i]));
   				 if(termsParty.getAttribute("terms_party_type").equals(testTP))
   				 {
   					 LOG.debug("party_Type is APP, hence I need to check for thirdParty");
   					 termsPartyDoc = packageAdditionalTermsParty(terms,id,termsPartyDoc,termsParty);
   					 if(termsPartyDoc.getAttribute("/AdditionalTermsParty").equals("Y"))
   					  id++;
   				 }
   				 termsPartyDoc.setAttribute("/AdditionalTermsParty","N");
   				 //If the termsValuesDoc contains termsParty also need to modify the line below
   			     termsPartyDoc = termsParty.packageTermsPartyAttributes(id,termsPartyDoc);
   			     id++;
   			   }
		      }

		 LOG.debug("This is TermsPartyDoc {}" ,  termsPartyDoc);

         termsPartyDoc.setAttribute("/TermsParty/TotalNumberOfEntries",String.valueOf(id));
         termsDoc.setComponent("/TermsParty",termsPartyDoc.getFragment("/TermsParty"));


   		return termsDoc;
   }

     //End of Code by Indu Valavala

//End of Class

 }