package com.ams.tradeportal.busobj.termsmgr;

import java.rmi.RemoteException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.busobj.ShipmentTerms;
import com.ams.tradeportal.busobj.TermsBean;
import com.ams.tradeportal.busobj.TermsParty;
import com.ams.tradeportal.busobj.util.InstrumentServices;
import com.ams.tradeportal.busobj.webbean.TermsWebBean;
import com.ams.tradeportal.common.TermsPartyType;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.Attribute;
import com.amsinc.ecsg.frame.AttributeManager;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.StringFunction;

/**
 * This class manages the dynamic attribute registration and validation for a Terms business object or TermsWebBean for an Airway
 * Bill Issue
 *
 *  
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class AIR__RELTermsMgr extends TermsMgr {
	private static final Logger LOG = LoggerFactory.getLogger(AIR__RELTermsMgr.class);

	/**
	 * Constructor that is used by business objects to create their manager. This method also register the dynamic attributes of the
	 * bean.
	 *
	 * @param mgr
	 *            AttributeManager attribute manager of the bean being managed. Used to register attributes
	 */
	public AIR__RELTermsMgr(AttributeManager mgr) throws AmsException {
		super(mgr);
	}

	/**
	 * Constructor that is used by web beans to create their manager. This method also register the dynamic attributes of the bean.
	 *
	 * @param mgr
	 *            TermsWebBean - the bean being managed. Used to register attributes.
	 */
	public AIR__RELTermsMgr(TermsWebBean terms) throws AmsException {
		super(terms);
	}

	/**
	 * Returns a list of attributes. These describe each of the attributes that will be registered for the bean being managed.
	 *
	 * This method controls which attributes of TermsBean or TermsWebBean are registered for an this instrument and transaction type
	 *
	 * @return Vector - list of attributes
	 */
	public List<Attribute> getAttributesToRegister() {
		List<Attribute> attrList = super.getAttributesToRegister();
		attrList.add(TermsBean.Attributes.create_additional_conditions());
		attrList.add(TermsBean.Attributes.create_amount());
		attrList.add(TermsBean.Attributes.create_amount_currency_code());
		attrList.add(TermsBean.Attributes.create_attention_of());
		attrList.add(TermsBean.Attributes.create_coms_chrgs_foreign_acct_curr());
		attrList.add(TermsBean.Attributes.create_coms_chrgs_foreign_acct_num());
		attrList.add(TermsBean.Attributes.create_coms_chrgs_our_account_num());
		attrList.add(TermsBean.Attributes.create_fax_number());
		attrList.add(TermsBean.Attributes.create_reference_number());
		attrList.add(TermsBean.Attributes.create_related_instrument_id());
		attrList.add(TermsBean.Attributes.create_special_bank_instructions());
		attrList.add(TermsBean.Attributes.create_work_item_number());

		return attrList;
	}

	/**
	 * Performs validation of the attributes of this type of instrument and transaction. This method is called from the
	 * userValidate() hook in the managed object.
	 *
	 * @param terms
	 *            - the bean being validated
	 */
	public void validate(TermsBean terms) throws java.rmi.RemoteException, AmsException {
		super.validate(terms);

		terms.registerRequiredAttribute("amount_currency_code");
		terms.registerRequiredAttribute("amount");

		// The second terms party is the applicant.
		terms.registerRequiredAttribute("c_SecondTermsParty");

		validateGeneral(terms);
		validateInstructions(terms);
		validateShipment(terms);
		validateTextLengths(terms);
	}

	/**
	 * Performs the Terms Manager-specific validation of text fields in the Terms and TermsParty objects.
	 *
	 * @param terms
	 *            - the managed object
	 *
	 */
	protected void validateForSwiftCharacters(TermsBean terms) throws AmsException, RemoteException {
		final String[] attributesToExclude = { "special_bank_instructions", "coms_chrgs_our_account_num", "coms_chrgs_foreign_acct_num" };

		InstrumentServices.checkForInvalidSwiftCharacters(terms.getAttributeHash(), terms.getErrorManager(), attributesToExclude);

		ShipmentTerms shipmentTerms = terms.getFirstShipment();
		if (shipmentTerms != null) {
			InstrumentServices.checkForInvalidSwiftCharacters(shipmentTerms.getAttributeHash(), shipmentTerms.getErrorManager());
		}

		validateTermsPartyForSwiftChars(terms, "FirstTermsParty", "AWB_SGTEE.FreightForwarder");
		validateTermsPartyForSwiftChars(terms, "SecondTermsParty", "AWB_SGTEE.Applicant");
		validateTermsPartyForSwiftChars(terms, "ThirdTermsParty", "AWB_SGTEE.ReleaseToParty");
	}

	/**
	 * Performs validation for the general section.
	 *
	 * @param terms
	 *            com.ams.tradeportal.busobj.termsBean
	 */
	public void validateGeneral(TermsBean terms) throws java.rmi.RemoteException, AmsException {
		this.editFreightForwarderValue(terms, "FirstTermsParty");
		this.editReleaseToPartyValue(terms, "ThirdTermsParty");
	}

	/**
	 * Performs validation for the shipment section.
	 *
	 * @param terms
	 *            com.ams.tradeportal.busobj.termsBean
	 */
	public void validateShipment(TermsBean terms) throws java.rmi.RemoteException, AmsException {
		String carrier = "";
		String airWaybillNumber = "";
		String goodsDescr = "";

		ShipmentTerms shipmentTerms = terms.getFirstShipment();

		if (shipmentTerms != null) {
			carrier = shipmentTerms.getAttribute("carrier_name_flight_num");
			airWaybillNumber = shipmentTerms.getAttribute("air_waybill_num");
			goodsDescr = shipmentTerms.getAttribute("goods_description");

			// Give a warning if the carrier name / flight number attribute is blank
			if (StringFunction.isBlank(carrier)) {
				shipmentTerms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.CARRIER_NAME_MISSING);
			}

			// Give a warning if the air waybill number attribute is blank.
			if (StringFunction.isBlank(airWaybillNumber)) {
				shipmentTerms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.AIR_WAYBILL_NUMBER_MISSING);
			}

			// Give a warning if the goods description attribute is blank.
			if (StringFunction.isBlank(goodsDescr)) {
				shipmentTerms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.GOODS_DESCRIPTION_MISSING);
			}

		}
	}

	/**
	 * Performs validation for the bank instructions section.
	 *
	 * @param terms
	 *            com.ams.tradeportal.busobj.termsBean
	 */
	public void validateInstructions(TermsBean terms) throws java.rmi.RemoteException, AmsException {
		String value;
		String alias;

		// If the c&c foreign account number is given, the c&c foreign currency
		// must also be given; and vice-versa.
		value = terms.getAttribute("coms_chrgs_foreign_acct_num");

		if (!StringFunction.isBlank(value)) {
			value = terms.getAttribute("coms_chrgs_foreign_acct_curr");

			if (StringFunction.isBlank(value)) {
				alias = terms.getAlias("coms_chrgs_foreign_acct_curr");

				terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.COMM_CHRG_INFO_MISSING, alias);
			}
		} else {
			value = terms.getAttribute("coms_chrgs_foreign_acct_curr");

			if (!StringFunction.isBlank(value)) {
				alias = terms.getAlias("coms_chrgs_foreign_acct_num");

				terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.COMM_CHRG_INFO_MISSING,
						alias);
			}
		}
	}

	/**
	 * Performs validation of text fields for length.
	 *
	 * @param terms
	 *            TermsBean - the bean being validated
	 */
	public void validateTextLengths(TermsBean terms) throws java.rmi.RemoteException, AmsException {
		checkTextLength(terms, "special_bank_instructions", 1000);

		String overrideSwiftLengthInd = getOverrideSwiftLengthInd(terms);

		if ((overrideSwiftLengthInd != null) && (overrideSwiftLengthInd.equals(TradePortalConstants.INDICATOR_NO))) {
			checkTextLength(terms.getFirstShipment(), "goods_description", 6500);
		}
	}

	/**
	 * Performs presave processing (regardless of whether validation is done)
	 *
	 * @param terms
	 *            com.ams.tradeportal.busobj.termsBean
	 * @exception com.amsinc.ecsg.frame.AmsException
	 *                The exception description.
	 */
	public void preSave(TermsBean terms) throws AmsException {
		super.preSave(terms);

		try {
			validateTextLengths(terms);

			validateAmount(terms, "amount", "amount_currency_code");
		} catch (RemoteException e) {
			LOG.info("Remote Exception caught in AIR__RELTermsMgr.preSave()");
		}
	}

	// Beginning of code used by the Middleware team
	/**
	 * gets the values of the attributes of the managed object. This method is called from the getTermsAttributesDoc() hook in the
	 * managed object.
	 *
	 * return com.amsinc.ecsg.util.DocumentHandler
	 * 
	 * @param terms
	 *            TermsBean - the bean being validated
	 * @param termsDoc
	 *            com.amsinc.ecsg.util.DocumentHandler
	 */
	public DocumentHandler getManagerTerms(TermsBean terms, DocumentHandler termsDoc) throws java.rmi.RemoteException, AmsException

	{

		DocumentHandler termsValuesDoc = terms.populateXmlDoc(new DocumentHandler());
		LOG.debug("This is the termValuesDoc containing all the terms values" , termsValuesDoc.toString());

		// Terms_Details
		termsDoc.setAttribute("/Terms/TermsDetails/ReferenceNumber", termsValuesDoc.getAttribute("/Terms/reference_number"));
		termsDoc.setAttribute("/Terms/TermsDetails/AmountCurrencyCode", termsValuesDoc.getAttribute("/Terms/amount_currency_code"));
		termsDoc.setAttribute("/Terms/TermsDetails/Amount", termsValuesDoc.getAttribute("/Terms/amount"));
		termsDoc.setAttribute("/Terms/TermsDetails/RelatedInstrumentID", termsValuesDoc.getAttribute("/Terms/related_instrument_id"));
		// End of Terms_Details

		populateFirstShipmentTermsXml(terms, termsDoc);

		// Instructions
		termsDoc.setAttribute("/Terms/Instructions/InstructionsToBank", termsValuesDoc.getAttribute("/Terms/special_bank_instructions"));

		// Commission_And_Charges
		termsDoc.setAttribute("/Terms/Instructions/CommissionAndCharges/CommissionChargesOurAccountNumber", termsValuesDoc.getAttribute("/Terms/coms_chrgs_our_account_num"));
		termsDoc.setAttribute("/Terms/Instructions/CommissionAndCharges/CommissionChargesForeignAccountNumber", termsValuesDoc.getAttribute("/Terms/coms_chrgs_foreign_acct_num"));
		termsDoc.setAttribute("/Terms/Instructions/CommissionAndCharges/CommissionChargesForeignAccountCurrency", termsValuesDoc.getAttribute("/Terms/coms_chrgs_foreign_acct_curr"));
				// End of Commission_And_Charges

		// End of Instructions

		// Terms Party

		DocumentHandler termsPartyDoc = new DocumentHandler();

		// Additional Terms Party
		String testTP = TermsPartyType.APPLICANT;

		int id = 0;

		termsPartyDoc.setAttribute("/TermsParty/TotalNumberOfEntries", "0");

		for (int i = 0; i < TradePortalConstants.MAX_NUM_TERMS_PARTIES; i++) {
			String termsPartyOID = termsValuesDoc.getAttribute("/Terms/" + TradePortalConstants.TERMS_PARTY_ATTRIBUTE_NAMES[i]);

			if (!(termsPartyOID == null || termsPartyOID.equals(""))) {
				TermsParty termsParty = (TermsParty) (terms.getComponentHandle(TradePortalConstants.TERMS_PARTY_COMPONENT_NAMES[i]));
				String terms_party_type = termsParty.getAttribute("terms_party_type");
				if (terms_party_type.equals(testTP)) {
					LOG.debug("party_Type is APP, hence I need to check for thirdParty");
					termsPartyDoc = packageAdditionalTermsParty(terms, id, termsPartyDoc, termsParty);
					if (termsPartyDoc.getAttribute("/AdditionalTermsParty").equals("Y"))
						id++;
				}
				termsPartyDoc.setAttribute("/AdditionalTermsParty", "N");
				// If the termsValuesDoc contains termsParty also need to modify the line below
				termsPartyDoc = termsParty.packageTermsPartyAttributes(id, termsPartyDoc);
				if (terms_party_type.equals(TermsPartyType.FREIGHT_FORWARD)) {
					termsPartyDoc.setAttribute("/TermsParty/FullTermsParty(" + id + ")/FaxNumber1", termsValuesDoc.getAttribute("/Terms/fax_number"));
					termsPartyDoc.setAttribute("/TermsParty/FullTermsParty(" + id + ")/ContactName", termsValuesDoc.getAttribute("/Terms/attention_of"));
				}

				id++;
			}
		}

		LOG.debug("TermsPartyDoc in getManagerTerms(): {}" , termsPartyDoc.toString());

		termsPartyDoc.setAttribute("/TermsParty/TotalNumberOfEntries", String.valueOf(id));
		termsDoc.setComponent("/TermsParty", termsPartyDoc.getFragment("/TermsParty"));

		return termsDoc;
	}

}