package com.ams.tradeportal.busobj.termsmgr;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import com.ams.tradeportal.busobj.*;
import com.ams.tradeportal.busobj.webbean.*;
import com.ams.tradeportal.common.*;

import java.rmi.*;

/**
 * This class manages the dynamic attribute registration and validation
 * for a Terms business object or TermsWebBean for an Issue Funds Transfer Request
 *
 *
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class FTDP__ISSTermsMgr extends CMTermsMgr
{
private static final Logger LOG = LoggerFactory.getLogger(FTDP__ISSTermsMgr.class);
	/**
	 *  Constructor that is used by business objects to create their
	 *  manager.  This method also register the dynamic attributes of
	 *  the bean.
	 *
	 *  @param mgr AttributeManager attribute manager of the bean being managed.  Used to register attributes
	 */
	public FTDP__ISSTermsMgr(AttributeManager mgr) throws AmsException
	{
		super(mgr);
	}

	/**
	 *  Constructor that is used by web beans to create their
	 *  manager.  This method also register the dynamic attributes of
	 *  the bean.
	 *
	 *  @param mgr TermsWebBean - the bean being managed.  Used to register attributes.
	 */
	public FTDP__ISSTermsMgr(TermsWebBean terms) throws AmsException
	{
		super(terms);
	}

	/**
	 *  Returns a list of attributes.  These describe each of the attributes
	 *  that will be registered for the bean being managed.
	 *
	 *  This method controls which attributes of TermsBean or TermsWebBean are registered
	 *  for an this instrument and transaction type
	 *
	 *  @return Vector - list of attributes
	 */
	public List<Attribute> getAttributesToRegister()
	{
		List<Attribute> attrList = super.getAttributesToRegister();
		attrList.add( TermsBean.Attributes.create_amount() );
		attrList.add( TermsBean.Attributes.create_amount_currency_code() );
		attrList.add( TermsBean.Attributes.create_payment_charges_account_oid() );
		attrList.add( TermsBean.Attributes.create_debit_account_oid() );
		attrList.add( TermsBean.Attributes.create_bank_charges_type() );
		attrList.add( TermsBean.Attributes.create_funding_amount() );
		attrList.add( TermsBean.Attributes.create_message_to_beneficiary() );
		attrList.add( TermsBean.Attributes.create_reference_number() );
		attrList.add( TermsBean.Attributes.create_related_instrument_id() );
		attrList.add( TermsBean.Attributes.create_work_item_priority() );
		attrList.add( TermsBean.Attributes.create_work_item_number() );
        attrList.add( TermsBean.Attributes.create_purpose_type() );
        attrList.add( TermsBean.Attributes.create_use_other() );
        attrList.add( TermsBean.Attributes.create_use_other_text() );
        attrList.add( TermsBean.Attributes.create_finance_type_text() );
        attrList.add( TermsBean.Attributes.create_debit_account_pending_amount());

		//IAZ CR-483b 08/13/09 Begin
        attrList.add( TermsBean.Attributes.create_covered_by_fec_number() );
        attrList.add( TermsBean.Attributes.create_equivalent_exch_amount() );
        attrList.add( TermsBean.Attributes.create_equivalent_exch_amt_ccy());
        attrList.add( TermsBean.Attributes.create_transfer_fx_rate() );
        attrList.add( TermsBean.Attributes.create_ex_rt_variation_adjust() );
        attrList.add( TermsBean.Attributes.create_display_fx_rate_method());		//IAZ ER 5.2.1.3 06/29/10 Add
        attrList.add( TermsBean.Attributes.create_source_template_trans_oid());	//IAZ CR-586 IR-VRUK091652765 06/29/10 Add
        //IAZ CR-483b 08/13/09 End
        
        //rkazi CR-596 Begin
        attrList.add( TermsBean.Attributes.create_processed_amount() );
        attrList.add( TermsBean.Attributes.create_rejected_amount() );
        //rkazi CR-596 11/25/2010 End
      //Leelavathi CR-657 Rel 7.1.0 6/27/2011 Begin
        attrList.add( TermsBean.Attributes.create_individual_debit_ind() );
      //Leelavathi CR-657 Rel 7.1.0 6/27/2011 End

        attrList.add( TermsBean.Attributes.create_request_market_rate_ind());		//NSX CR-640/581 Rel 7.1.0 09/29/11
        attrList.add( TermsBean.Attributes.create_fx_online_deal_confirm_date());	//NSX CR-640/581 Rel 7.1.0 09/29/11

		return attrList;
	}

	/**
	 *  Performs validation of the attributes of the managed object.   This method is called
	 *  from the userValidate() hook in the managed object.
	 *
	 *  @param terms TermsBean - the bean being validated
	 */
	public void validate(TermsBean terms)
	throws java.rmi.RemoteException, AmsException {

		super.validate(terms);
		terms.registerRequiredAttribute("amount_currency_code");
		terms.registerRequiredAttribute("funding_amount");
		//terms.registerRequiredAttribute("bank_charges_type");

		// The second terms party is the Payer.
		terms.registerRequiredAttribute("c_SecondTermsParty");

		validateGeneral(terms);
		validateTextLengths(terms);
		validatePaymentTerms(terms);
		LOG.debug("FTDP__TermsMgr::validate amount: {}" , terms.getAttribute("amount_currency_code"));
		validateAmount(terms, "amount", "amount_currency_code");	//IAZ CR-483b 08/13/09 Add
		validateFXRate(terms);										//IAZ CR-483b 08/13/09 Add
	}


//BSL Rel 6.1 IR #VEUL031445617 03/21/11 Begin
// Override TermsMgr.validateAmount to check for amount values that end
// in ".0" (e.g., change "1234.0" to "1234") to facilitate proper
// processing of whole-number currencies such as Japanese Yen.
// This function may later be removed from FTDP__ISSTermsMgr.java by
// incorporating the same logic into TermsMgr.java.
/**
 * Validates the precision of an amount value given the currency for it.
 * If the precision is wrong, the invalid currency format error is issued.
 *
 * It also checks that the value will fit into the database
 *
 * @param terms com.ams.tradeportal.busobj.termsBean
 * @param amountAttribute java.lang.String
 * @param currencyAttribute java.lang.String
 * @exception java.rmi.RemoteException The exception description.
 * @exception com.amsinc.ecsg.frame.AmsException The exception description.
 */
public void validateAmount(TermsBean terms, String amountAttribute, String currencyAttribute) throws java.rmi.RemoteException, com.amsinc.ecsg.frame.AmsException {
	String amount = terms.getAttribute(amountAttribute);
	String currencyCode = terms.getAttribute(currencyAttribute);

	if (amount != null && amount.matches("^[^.]+\\.0+$"))
		amount = amount.substring(0,amount.indexOf('.'));

	// Validate that value will fit in database and that currency precision is correct
	if(amount!=null)
	TPCurrencyUtility.validateAmount(currencyCode, amount, terms.getAlias(amountAttribute), terms.getErrorManager() );
}
//BSL Rel 6.1 IR #VEUL031445617 03/21/11 End


	/**
	 *  Performs validation of the general section of the page
	 *
	 *  @param terms TermsBean - the bean being validated
	 */
	public void validateGeneral(TermsBean terms) throws java.rmi.RemoteException, AmsException
	{
		// No edits for general.  Method exists as placeholder for future edits.
	}


	/**
	 * Performs validation for the payment terms section.
	 *
	 * @param terms com.ams.tradeportal.busobj.TermsBean
	 */
	public void validatePaymentTerms(TermsBean terms) throws java.rmi.RemoteException, AmsException
	{
		String value;

		value = terms.getAttribute("funding_amount");
		if (!StringFunction.isBlank(value)) {
			if (Double.parseDouble(value) == 0) {
				terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.AMOUNT_IS_ZERO);
			}
		}

	}

	/**
	 * Performs the Terms Manager-specific validation of text fields in the Terms
	 * and TermsParty objects.
	 *
	 * @param terms - the managed object
	 *
	 */
	protected void validateForSwiftCharacters(TermsBean terms) throws AmsException, RemoteException
	{
        //IAZ CM CR-507 01/12/10 Begin
        //We do not need to check Terms or FirstParty because data is either MULTIPLE indicators (if tarnsaction ahs
        //multiple payments) or mimics data from DP record (if tarnsaction has a single payemnt)
        //Also, we do not use ThirdTermsParty with DP
        //
        //We will only check Second Party as this data is copied form user/customer profile and must be checked.

		validateTermsPartyForSwiftChars(terms, "SecondTermsParty", "FundsTransferRequest.Payer");
		
	}

	/**
	 *  Performs validation of text fields for length.
	 *
	 *  @param terms TermsBean - the bean being validated
	 */
	public void validateTextLengths(TermsBean terms)
	throws java.rmi.RemoteException, AmsException
	{
		/* KMehta IR-T36000013135 Rel 9300 on 11-May-2015 Add - Begin */
		checkTextLength(terms, "reference_number", 20);
		
	}




	//Beginning of code used by the Middleware team
	/**
	 *  gets the values of the attributes of the managed object. This method is called
	 *  from the getTermsAttributesDoc() hook in the managed object.
	 *
	 *  return com.amsinc.ecsg.util.DocumentHandler
	 *  @param terms TermsBean - the bean being validated
	 *  @param termsDoc com.amsinc.ecsg.util.DocumentHandler
	 */
	public DocumentHandler getManagerTerms(TermsBean terms, DocumentHandler termsDoc) throws java.rmi.RemoteException, AmsException

	{

		DocumentHandler termsValuesDoc = terms.populateXmlDoc(new DocumentHandler());
		LOG.debug("This is the termValuesDoc containing all the terms values" + "\n" +
				termsValuesDoc.toString());

		// MQ DTD FIELD                                                                       TRADEPORTAL FIELD
		// TERMS DETAILS TERMS

		termsDoc.setAttribute("/Terms/TermsDetails/ReferenceNumber",	                      termsValuesDoc.getAttribute("/Terms/reference_number"));
		termsDoc.setAttribute("/Terms/TermsDetails/AmountCurrencyCode",       	              termsValuesDoc.getAttribute("/Terms/amount_currency_code"));
		termsDoc.setAttribute("/Terms/TermsDetails/Amount",                    	              termsValuesDoc.getAttribute("/Terms/amount"));
		termsDoc.setAttribute("/Terms/TermsDetails/PurposeType",                              termsValuesDoc.getAttribute("/Terms/purpose_type"));
		termsDoc.setAttribute("/Terms/TermsDetails/FinanceTypeText",                          termsValuesDoc.getAttribute("/Terms/finance_type_text"));

		//If there is an fec rate, set UseFec to Y.
		if (!termsValuesDoc.getAttribute("/Terms/fec_rate").equals("")){
    	    termsDoc.setAttribute("/Terms/LoansAndFunds/UseFec",                                  "Y");
		}

        termsDoc.setAttribute("/Terms/LoansAndFunds/UseOther",                                termsValuesDoc.getAttribute("/Terms/use_other"));
        termsDoc.setAttribute("/Terms/LoansAndFunds/UseOtherText",                            termsValuesDoc.getAttribute("/Terms/use_other_text"));
		termsDoc.setAttribute("/Terms/LoansAndFunds/WorkItemPriority",                        termsValuesDoc.getAttribute("/Terms/work_item_priority"));
		termsDoc.setAttribute("/Terms/LoansAndFunds/BeneficiaryMessage",                      termsValuesDoc.getAttribute("/Terms/message_to_beneficiary"));
		termsDoc.setAttribute("/Terms/LoansAndFunds/BankChargesType", 		                  termsValuesDoc.getAttribute("/Terms/bank_charges_type"));
		termsDoc.setAttribute("/Terms/LoansAndFunds/AppDebitAccPrincipal", 		              "");
		// IValavala CR451 get the AppDebitAccPrincipal accountnum from account
		String accountUoid = termsValuesDoc.getAttribute("/Terms/debit_account_oid");
		// CR-610 - Adarsha K S- 2/21/2011 - Start
		//MDB CR-640 Rel7.1 10/13/11 - BEGIN added currency and bank_country_code
		String accountNumSQL = "SELECT ACCOUNT_NUMBER, SOURCE_SYSTEM_BRANCH, CURRENCY, ADDL_VALUE as BANK_COUNTRY_CODE FROM ACCOUNT, REFDATA WHERE ACCOUNT_OID = ?" +
								" AND ACCOUNT.BANK_COUNTRY_CODE=REFDATA.CODE AND REFDATA.TABLE_TYPE='" + TradePortalConstants.COUNTRY_ISO3116_3 +
								"' and REFDATA.LOCALE_NAME IS NULL"; //MDB PKUL102163801 Rel7.1 10/21/11
	    String accountNum = "";
	    String branch = "";
	    String debitCCY = "";
	    String debitCountry = "";
	    DocumentHandler resultXML = DatabaseQueryBean.getXmlResultSet(accountNumSQL, false, new Object[]{accountUoid});
	    if(resultXML != null)
	     {
		    accountNum = resultXML.getAttribute("/ResultSetRecord(" + 0 + ")/ACCOUNT_NUMBER");
		    branch = resultXML.getAttribute("/ResultSetRecord(" + 0 + ")/SOURCE_SYSTEM_BRANCH");
		    debitCCY = resultXML.getAttribute("/ResultSetRecord(" + 0 + ")/CURRENCY");
		    debitCountry = resultXML.getAttribute("/ResultSetRecord(" + 0 + ")/BANK_COUNTRY_CODE");
	 	  }
		//MDB CR-640 Rel7.1 10/13/11 - END added currency and bank_country_code
		termsDoc.setAttribute("/Terms/LoansAndFunds/AppDebitAccPrincipal", 		              accountNum);
	    //MDB PKUL102043568 Rel7.1 10/20/11 Begin - moved location in XML to match DTD
		termsDoc.setAttribute("/Terms/LoansAndFunds/DebitCCY",            	debitCCY);
		termsDoc.setAttribute("/Terms/LoansAndFunds/DebitCountry",        	debitCountry);
	   
		
		// IValavala CR451 Adding DailyLimitExceeded, PaymentDate Value will be added in the TPLPackager as it comes from Transaction
		//VS IR SLUK101932513 Begin
		termsDoc.setAttribute("/Terms/LoansAndFunds/PayeeAccountNumber", 		              "");
		//VS IR SLUK101932513 End
		termsDoc.setAttribute("/Terms/LoansAndFunds/DailyLimitExceeded", 		              "");
		termsDoc.setAttribute("/Terms/LoansAndFunds/PaymentDate", "");
        //cquinton 3/29/2011 Rel 7.0.0 ir#laul032634214 start
        termsDoc.setAttribute("/Terms/LoansAndFunds/DebitAcctSourceSystemBranch", branch);
        //cquinton 3/29/2011 Rel 7.0.0 ir#laul032634214 end
		//Leelavathi CR-657 Rel 7.1.0 6/27/2011 start
        termsDoc.setAttribute("/Terms/LoansAndFunds/IndividualDebitInd",
            termsValuesDoc.getAttribute("/Terms/individual_debit_ind"));
        

	    //Terms Party
	    DocumentHandler termsPartyDoc = new DocumentHandler();


	     int id = 0;

	    termsPartyDoc.setAttribute("/TermsParty/TotalNumberOfEntries","0");

	    for (int i = 0; i<TradePortalConstants.MAX_NUM_TERMS_PARTIES;i++)
	   	 {
			String termsPartyOID = termsValuesDoc.getAttribute("/Terms/"+TradePortalConstants.TERMS_PARTY_ATTRIBUTE_NAMES[i]);
	        if(!(termsPartyOID == null || termsPartyOID.equals("")))
	   	      {
	   			 TermsParty termsParty = (TermsParty)(terms.getComponentHandle(TradePortalConstants.TERMS_PARTY_COMPONENT_NAMES[i]));
	   			 termsPartyDoc.setAttribute("/AdditionalTermsParty","N");
	   			 termsPartyDoc = termsParty.packageTermsPartyAttributes(id,termsPartyDoc);
	   		     id++;
	   		   }
		      }

		 LOG.debug("This is TermsPartyDoc: {} " ,  termsPartyDoc.toString());

	     termsPartyDoc.setAttribute("/TermsParty/TotalNumberOfEntries",String.valueOf(id));
	     termsDoc.setComponent("/TermsParty",termsPartyDoc.getFragment("/TermsParty"));

		return termsDoc;
	}


	/**
	 * Performs presave processing (regardless of whether validation is done)
	 *
	 * @param terms com.ams.tradeportal.busobj.TermsBean
	 * @exception com.amsinc.ecsg.frame.AmsException The exception description.
	 */
	public void preSave(TermsBean terms) throws AmsException {
		super.preSave(terms);

		try {
			

			validateTextLengths(terms);
		
		} catch (RemoteException e) {
			LOG.error("Remote Exception caught in FTDP__ISSTermsMgr.preSave()",e);
		}

	}

    //IAZ CR-483b 08/13/09 Begin
	/**
	 *  Performs validation of the FXRate section
	 *
	 *  @param terms TermsBean - the bean being validated
	 */
	public void validateFXRate(TermsBean terms) throws java.rmi.RemoteException, AmsException
	{
		String errorVal1, errorVal2;

		if (StringFunction.isBlank(terms.getAttribute("covered_by_fec_number")) &&
		    StringFunction.isNotBlank(terms.getAttribute("fec_rate")))
        {
					//Vshah - IR#LMUL120861050 - Rel7.1 - 12/09/2011 - <BEGIN>
   		            errorVal1 = terms.getResourceManager().getText( "TransferBetweenAccounts.FECCovered",TradePortalConstants.TEXT_BUNDLE);
   		            //Vshah - IR#LMUL120861050 - Rel7.1 - 12/09/2011 - <END>
					errorVal2 = terms.getResourceManager().getText( "TransferBetweenAccounts.FEC",
																TradePortalConstants.TEXT_BUNDLE);
					terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
										TradePortalConstants.NOT_INDICATED_UNDER,
										errorVal1, errorVal2);
		}

		if (StringFunction.isNotBlank(terms.getAttribute("covered_by_fec_number")) &&
			    StringFunction.isBlank(terms.getAttribute("fec_rate")))
	    {

					//Vshah - IR#LMUL120861050 - Rel7.1 - 12/09/2011 - <BEGIN>
					errorVal1 = terms.getResourceManager().getText( "TransferBetweenAccounts.FECRate",TradePortalConstants.TEXT_BUNDLE);
					//Vshah - IR#LMUL120861050 - Rel7.1 - 12/09/2011 - <END>
					errorVal2 = terms.getResourceManager().getText( "TransferBetweenAccounts.FEC",
																TradePortalConstants.TEXT_BUNDLE);
					terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
										TradePortalConstants.NOT_INDICATED_UNDER,
										errorVal1, errorVal2);
		}
		// DK IR T36000013357 Rel9.1 starts
		if("0".equals(terms.getAttribute("covered_by_fec_number"))){
			errorVal1 = terms.getResourceManager().getText( "TransferBetweenAccounts.FECCovered",TradePortalConstants.TEXT_BUNDLE);
			terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
								TradePortalConstants.CANNOT_BE_ZERO,
								errorVal1);			
		}
		// DK IR T36000013357 Rel9.1 ends

	}
	//IAZ CR-483b 08/13/09 End

}