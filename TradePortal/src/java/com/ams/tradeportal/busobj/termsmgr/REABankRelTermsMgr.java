package com.ams.tradeportal.busobj.termsmgr;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import com.ams.tradeportal.busobj.*;
import com.ams.tradeportal.busobj.webbean.*;

/**
 * This class manages the dynamic attribute registration and validation
 * for a Terms business object or TermsWebBean for bank released terms 
 * of a transaction of type Reactivate
 *
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class REABankRelTermsMgr extends TermsMgr
 {
private static final Logger LOG = LoggerFactory.getLogger(REABankRelTermsMgr.class);
    /**
     *  Constructor that is used by business objects to create their 
     *  manager.  This method also register the dynamic attributes of
     *  the bean.  
     *  
     *  @param mgr AttributeManager attribute manager of the bean being managed.  Used to register attributes
     */
    public REABankRelTermsMgr(AttributeManager mgr) throws AmsException
     {
	  super(mgr);
     }

    /**
     *  Constructor that is used by web beans to create their 
     *  manager.  This method also register the dynamic attributes of
     *  the bean.  
     *  
     *  @param mgr TermsWebBean - the bean being managed.  Used to register attributes.
     */
    public REABankRelTermsMgr(TermsWebBean terms) throws AmsException
     {
	  super(terms);
     }

    /**
     *  Returns a list of attributes.  These describe each of the attributes
     *  that will be registered for the bean being managed.  
     *
     *  This method controls which attributes of TermsBean or TermsWebBean are registered
     *  for bank released terms with this transaction type
     *
     *  @return Vector - list of attributes
     */
     public List<Attribute> getAttributesToRegister()
     {
        List<Attribute> attrList = super.getAttributesToRegister();
        attrList.add( TermsBean.Attributes.create_amount() );
        attrList.add( TermsBean.Attributes.create_amount_currency_code() );

        return attrList;
     }




















     
   /**
    * This method is supposed to be used to set attributes of Terms business object.
    *
    * @param inputDoc DocumentHandler - universal XML message from MQSeries
    * @param terms TermsBean
    * @return boolean
    */
    public boolean setManagerTerms(TermsBean terms, DocumentHandler inputDoc) throws java.rmi.RemoteException, AmsException{

		    terms.setAttribute("amount",			    inputDoc.getAttribute("/Terms/TermsDetails/Amount"));
		    terms.setAttribute("amount_currency_code", 	inputDoc.getAttribute("/Terms/TermsDetails/AmountCurrencyCode"));
	    
	        return true; 	 	  
    }

 }