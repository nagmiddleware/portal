package com.ams.tradeportal.busobj.termsmgr;
import java.rmi.RemoteException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.busobj.TermsBean;
import com.ams.tradeportal.busobj.TermsParty;
import com.ams.tradeportal.busobj.util.InstrumentServices;
import com.ams.tradeportal.busobj.webbean.TermsWebBean;
import com.ams.tradeportal.common.TermsPartyType;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.Attribute;
import com.amsinc.ecsg.frame.AttributeManager;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.StringFunction;






/**
 * This class manages the dynamic attribute registration and validation
 * for a  Terms business object or TermsWebBean for an Issue Funds Transfer Request
 *
 *
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class FTRQ__ISSTermsMgr extends CMTermsMgr
{
private static final Logger LOG = LoggerFactory.getLogger(FTRQ__ISSTermsMgr.class);
	/**
	 *  Constructor that is used by business objects to create their
	 *  manager.  This method also register the dynamic attributes of
	 *  the bean.
	 *
	 *  @param mgr AttributeManager attribute manager of the bean being managed.  Used to register attributes
	 */
	public FTRQ__ISSTermsMgr(AttributeManager mgr) throws AmsException
	{
		super(mgr);
	}

	/**
	 *  Constructor that is used by web beans to create their
	 *  manager.  This method also register the dynamic attributes of
	 *  the bean.
	 *
	 *  @param mgr TermsWebBean - the bean being managed.  Used to register attributes.
	 */
	public FTRQ__ISSTermsMgr(TermsWebBean terms) throws AmsException
	{
		super(terms);
	}

	/**
	 *  Returns a list of attributes.  These describe each of the attributes
	 *  that will be registered for the bean being managed.
	 *
	 *  This method controls which attributes of TermsBean or TermsWebBean are registered
	 *  for an this instrument and transaction type
	 *
	 *  @return Vector - list of attributes
	 */
	public List<Attribute> getAttributesToRegister()
	{
		List<Attribute> attrList = super.getAttributesToRegister();
		attrList.add( TermsBean.Attributes.create_amount() );
		attrList.add( TermsBean.Attributes.create_amount_currency_code() );
		attrList.add( TermsBean.Attributes.create_payment_charges_account_oid() );
		attrList.add( TermsBean.Attributes.create_debit_account_oid());
		attrList.add( TermsBean.Attributes.create_bank_charges_type() );
		attrList.add( TermsBean.Attributes.create_ex_rt_variation_adjust() );
		attrList.add( TermsBean.Attributes.create_funding_amount() );
		attrList.add( TermsBean.Attributes.create_funds_xfer_settle_type() );
		attrList.add( TermsBean.Attributes.create_interest_to_be_paid() );
		attrList.add( TermsBean.Attributes.create_loan_proceeds_credit_other_txt() );
		attrList.add( TermsBean.Attributes.create_loan_terms_fixed_maturity_dt() );
		attrList.add( TermsBean.Attributes.create_loan_terms_number_of_days() );
		attrList.add( TermsBean.Attributes.create_loan_terms_type() );
		attrList.add( TermsBean.Attributes.create_message_to_beneficiary() );
		attrList.add( TermsBean.Attributes.create_reference_number() );
		attrList.add( TermsBean.Attributes.create_related_instrument_id() );
		attrList.add( TermsBean.Attributes.create_release_by_bank_on() );
		attrList.add( TermsBean.Attributes.create_work_item_priority() );
		attrList.add( TermsBean.Attributes.create_work_item_number() );
        attrList.add( TermsBean.Attributes.create_special_bank_instructions() );
        attrList.add( TermsBean.Attributes.create_purpose_type() );
        attrList.add( TermsBean.Attributes.create_in_advance_disposition() );
        attrList.add( TermsBean.Attributes.create_in_arrears_disposition() );
        attrList.add( TermsBean.Attributes.create_covered_by_fec_number() );
        attrList.add( TermsBean.Attributes.create_fec_amount() );
        attrList.add( TermsBean.Attributes.create_fec_maturity_date() );
        attrList.add( TermsBean.Attributes.create_maturity_covered_by_fec_number() );
        attrList.add( TermsBean.Attributes.create_maturity_fec_amount() );
        attrList.add( TermsBean.Attributes.create_maturity_fec_maturity_date() );
        attrList.add( TermsBean.Attributes.create_maturity_fec_rate() );
        attrList.add( TermsBean.Attributes.create_maturity_use_fec() );
        attrList.add( TermsBean.Attributes.create_use_fec() );
        attrList.add( TermsBean.Attributes.create_use_mkt_rate() );
        attrList.add( TermsBean.Attributes.create_use_other() );
        attrList.add( TermsBean.Attributes.create_use_other_text() );
        attrList.add( TermsBean.Attributes.create_finance_type_text() );
        attrList.add( TermsBean.Attributes.create_maturity_use_other() );
        attrList.add( TermsBean.Attributes.create_maturity_use_other_text() );
        attrList.add( TermsBean.Attributes.create_additional_conditions() );
        attrList.add( TermsBean.Attributes.create_addl_doc_indicator() );
        attrList.add( TermsBean.Attributes.create_addl_doc_text() );
        attrList.add( TermsBean.Attributes.create_debit_account_pending_amount());
        attrList.add( TermsBean.Attributes.create_request_market_rate_ind());		//NSX CR-640/581 Rel 7.1.0 09/29/11
		attrList.add( TermsBean.Attributes.create_display_fx_rate_method());		//DK CR-640 Rel7.1
		attrList.add( TermsBean.Attributes.create_equivalent_exch_amount() );	//DK CR-640 Rel7.1
		attrList.add( TermsBean.Attributes.create_equivalent_exch_amt_ccy());    //IR# PSUL110355116 Rel. 7.1.0 -
        attrList.add( TermsBean.Attributes.create_transfer_fx_rate() );          //DK CR-640 Rel7.1
		attrList.add( TermsBean.Attributes.create_fx_online_deal_confirm_date());	//NSX CR-640/581 Rel 7.1.0 09/29/11
		
		return attrList;
	}

	/**
	 *  Performs validation of the attributes of the managed object.   This method is called
	 *  from the userValidate() hook in the managed object.
	 *
	 *  @param terms TermsBean - the bean being validated
	 */
	public void validate(TermsBean terms)
	throws java.rmi.RemoteException, AmsException {

		super.validate(terms);
		terms.registerRequiredAttribute("amount_currency_code");
		terms.registerRequiredAttribute("amount");
		terms.registerRequiredAttribute("bank_charges_type");
		terms.registerRequiredAttribute("payment_charges_account_oid");
		terms.registerRequiredAttribute("funds_xfer_settle_type");
		// The second terms party is the Payer.
		terms.registerRequiredAttribute("c_SecondTermsParty");
		terms.registerRequiredAttribute("reference_number");

		validateGeneral(terms);
		validatePayee(terms);

		validatePaymentTerms(terms);
		validateFundingDetails(terms);
	}


	/**
	 * Get the Beneficiary terms party and then ask it to validate himself.
	 *
	 * @param terms com.ams.tradeportal.busobj.TermsBean
	 */
	public void validatePayee(TermsBean terms)
	throws RemoteException, AmsException
	{
		TermsParty termsParty;
		String value = "";

		this.editRequiredBeneficiaryValue(terms, "FirstTermsParty");

		// The beneficiary must also have a non-blank account number
		termsParty = (TermsParty) terms.getComponentHandle("FirstTermsParty");

		value = termsParty.getAttribute("acct_num");
		if (StringFunction.isBlank(value))
		{
			issueTermsPartyError(termsParty, terms, "FundsTransferRequest.Payee",
			"acct_num");
		}


	}

	/**
	 *  Performs validation of the general section of the page
	 *
	 *  @param terms TermsBean - the bean being validated
	 */
	public void validateGeneral(TermsBean terms) throws java.rmi.RemoteException, AmsException
	{
		
		// No edits for general.  Method exists as placeholder for future edits.
	}


	/**
	 * Performs validation for the payment terms section.
	 *
	 * @param terms com.ams.tradeportal.busobj.TermsBean
	 */
	/**
	 * @param terms
	 * @throws java.rmi.RemoteException
	 * @throws AmsException
	 */
	public void validatePaymentTerms(TermsBean terms) throws java.rmi.RemoteException, AmsException
	{
		String value;

		// pcutrone - 10/17/2007 - REUH101146143 - Check to make sure the amount field is not set to zero.
		value = terms.getAttribute("amount");
		if (!StringFunction.isBlank(value)) {
			if (Double.parseDouble(value) == 0) {
				terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.AMOUNT_IS_ZERO);
			}
		}
		// pcutrone - 10/17/2007 - REUH101146143 - END

		// Release By Bank On date must be a valid date.  The required edit
		// logic guarantees it has a value.  The setAttribute logic
		// guarantees it is a valid date.  No logic required here.

	}

	/**
	 *  Performs validation of the funding details section of the page
	 *
	 *  @param terms TermsBean - the bean being validated
	 */
	public void validateFundingDetails(TermsBean terms) throws java.rmi.RemoteException, AmsException
		{
			String val4;
			String val;
			String value;
			String errorVal1, errorVal2;
			
	// 		 Amount must be greater than funding amount.
			val = terms.getAttribute("funds_xfer_settle_type");
			
			// PPRAKASH IR ALUJ011377704 Comments End
			if (val.equals(TradePortalConstants.XFER_SETTLE_OTHER)){
				val4 = terms.getAttribute("finance_type_text");
				if (StringFunction.isBlank(val4)) {
				terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.OTH_TXT_REQD);
				}
			}
			else if (val.equals(TradePortalConstants.XFER_SETTLE_DEBIT_PAY_ACCT)){
				val4 = terms.getAttribute("debit_account_oid");//PPRAKASH IR AOUJ012785698
				if (StringFunction.isBlank(val4)) {
				terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.DEBIT_ACT_REQD);
				}
			}
			
	//	 Exhange Rate Details (PROCEEDS)
			value = terms.getAttribute("use_fec");
			if (value.equals(TradePortalConstants.INDICATOR_YES)) {
				// Check if covered by fec number is blank
				value = terms.getAttribute("covered_by_fec_number");
				if (StringFunction.isBlank(value)) {
					//Vshah - IR#LMUL120861050 - Rel7.1 - 12/09/2011 - <BEGIN>
					errorVal1 = terms.getResourceManager().getText( "TransferBetweenAccounts.FECCovered",TradePortalConstants.TEXT_BUNDLE);
					//Vshah - IR#LMUL120861050 - Rel7.1 - 12/09/2011 - <END>
					errorVal2 = terms.getResourceManager().getText( "LoanRequest.LoanProceeds",
																TradePortalConstants.TEXT_BUNDLE);
					terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
										TradePortalConstants.NOT_INDICATED_UNDER,
										errorVal1, errorVal2);
				}
				
				// check if rate of fec is blank
				value = terms.getAttribute("fec_rate");
				if (StringFunction.isBlank(value)) {
					//Vshah - IR#LMUL120861050 - Rel7.1 - 12/09/2011 - <BEGIN>
					errorVal1 = terms.getResourceManager().getText( "TransferBetweenAccounts.FECRate",TradePortalConstants.TEXT_BUNDLE);
					//Vshah - IR#LMUL120861050 - Rel7.1 - 12/09/2011 - <END>
					errorVal2 = terms.getResourceManager().getText( "LoanRequest.LoanProceeds",
																TradePortalConstants.TEXT_BUNDLE);
					terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
										TradePortalConstants.NOT_INDICATED_UNDER,
										errorVal1, errorVal2);
				}
				
			} else {
				 
				terms.setAttribute("fec_maturity_date", "");
			}

			value = terms.getAttribute("use_other");
			if (value.equals(TradePortalConstants.INDICATOR_YES)) {
				// Check if covered by use other text is blank
				value = terms.getAttribute("use_other_text");
				if (StringFunction.isBlank(value)) {
					errorVal1 = terms.getResourceManager().getText( "LoanRequest.Other2",
																TradePortalConstants.TEXT_BUNDLE);
					errorVal2 = terms.getResourceManager().getText( "LoanRequest.LoanProceeds",
																TradePortalConstants.TEXT_BUNDLE);
					terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
										TradePortalConstants.NOT_INDICATED_UNDER,
										errorVal1, errorVal2);
				}
			} else {
				// Blank out field
				terms.setAttribute("use_other_text", "");
			}


	}




	/**
	 * Performs the Terms Manager-specific validation of text fields in the Terms
	 * and TermsParty objects.
	 *
	 * @param terms - the managed object
	 *
	 */
	protected void validateForSwiftCharacters(TermsBean terms) throws AmsException, RemoteException
	{
		final String[] attributesToExclude = {"message_to_benficiary", "use_other_text"};

		InstrumentServices.checkForInvalidSwiftCharacters(terms.getAttributeHash(), terms.getErrorManager(), attributesToExclude);

		validateTermsPartyForSwiftChars(terms, "FirstTermsParty", "FundsTransferRequest.Payee");
		validateTermsPartyForSwiftChars(terms, "SecondTermsParty", "FundsTransferRequest.Payer");
		validateTermsPartyForSwiftChars(terms, "ThirdTermsParty", "FundsTransferRequest.PayeeBank");
	}

	/**
	 *  Performs validation of text fields for length.
	 *
	 *  @param terms TermsBean - the bean being validated
	 */
	public void validateTextLengths(TermsBean terms)
	throws java.rmi.RemoteException, AmsException
	{
		String overrideSwiftLengthInd = getOverrideSwiftLengthInd(terms);

		if((overrideSwiftLengthInd != null)
				&& (overrideSwiftLengthInd.equals(TradePortalConstants.INDICATOR_NO)))
		{
			checkTextLength(terms, "additional_conditions", 5000);
		} 
		checkTextLength(terms, "use_other_text", 210);
		checkTextLength(terms, "message_to_beneficiary", 210);
                checkTextLength(terms, "special_bank_instructions", 1000);
	}


	/**
	 * Validates the fields that can only have a certain number of decimal places
	 *
	 */
	public void validateDecimalFields(TermsBean terms) throws AmsException, RemoteException
	{
		
       //   	 Validate that the rate field will fit in the database
		InstrumentServices.validateDecimalNumber(terms.getAttribute("fec_rate"),
										 5,
										 8,
										 "TermsBeanAlias.fec_rate",
										 terms.getErrorManager(),
										 terms.getResourceManager());

		// Validate that the rate field will fit in the database
		InstrumentServices.validateDecimalNumber(terms.getAttribute("maturity_fec_rate"),
										 5,
										 8,
										 "TermsBeanAlias.maturity_fec_rate",
										 terms.getErrorManager(),
										 terms.getResourceManager());

   //		Edozie Code- 08/28/07- Funding Details Ends


	}

	//Beginning of code used by the Middleware team
	/**
	 *  gets the values of the attributes of the managed object. This method is called
	 *  from the getTermsAttributesDoc() hook in the managed object.
	 *
	 *  return com.amsinc.ecsg.util.DocumentHandler
	 *  @param terms TermsBean - the bean being validated
	 *  @param termsDoc com.amsinc.ecsg.util.DocumentHandler
	 */
	public DocumentHandler getManagerTerms(TermsBean terms, DocumentHandler termsDoc) throws java.rmi.RemoteException, AmsException

	{

		DocumentHandler termsValuesDoc = terms.populateXmlDoc(new DocumentHandler());
		LOG.debug("This is the termValuesDoc containing all the terms values: {}" ,termsValuesDoc.toString());

		// MQ DTD FIELD                                                                       TRADEPORTAL FIELD
		// TERMS DETAILS TERMS

        termsDoc.setAttribute("/Terms/Instructions/InstructionsToBank",                       termsValuesDoc.getAttribute("/Terms/special_bank_instructions"));
		//End of Instructions_to_Bank
		termsDoc.setAttribute("/Terms/TermsDetails/ReferenceNumber",	                      termsValuesDoc.getAttribute("/Terms/reference_number"));
		termsDoc.setAttribute("/Terms/TermsDetails/AmountCurrencyCode",       	              termsValuesDoc.getAttribute("/Terms/amount_currency_code"));
		termsDoc.setAttribute("/Terms/TermsDetails/Amount",                    	              termsValuesDoc.getAttribute("/Terms/amount"));
		termsDoc.setAttribute("/Terms/TermsDetails/PurposeType",                              termsValuesDoc.getAttribute("/Terms/purpose_type"));
		termsDoc.setAttribute("/Terms/TermsDetails/FinanceTypeText",                          termsValuesDoc.getAttribute("/Terms/finance_type_text"));

		// LOANS AND FUNDS TERMS
		termsDoc.setAttribute("/Terms/LoansAndFunds/LoanTermsFixedMaturityDate", 		      termsValuesDoc.getAttribute("/Terms/loan_terms_fixed_maturity_dt"));
		termsDoc.setAttribute("/Terms/LoansAndFunds/LoanTermsType",       	                  termsValuesDoc.getAttribute("/Terms/loan_terms_type"));
		termsDoc.setAttribute("/Terms/LoansAndFunds/InterestToBePaid",                        termsValuesDoc.getAttribute("/Terms/interest_to_be_paid"));
		termsDoc.setAttribute("/Terms/LoansAndFunds/LoanTermsNumberOfDays",                   termsValuesDoc.getAttribute("/Terms/loan_terms_number_of_days"));
		termsDoc.setAttribute("/Terms/LoansAndFunds/UseMarketRate",                           termsValuesDoc.getAttribute("/Terms/use_mkt_rate"));
        termsDoc.setAttribute("/Terms/LoansAndFunds/UseFec",                                  termsValuesDoc.getAttribute("/Terms/use_fec"));
        termsDoc.setAttribute("/Terms/LoansAndFunds/UseOther",                                termsValuesDoc.getAttribute("/Terms/use_other"));
        termsDoc.setAttribute("/Terms/LoansAndFunds/UseOtherText",                            termsValuesDoc.getAttribute("/Terms/use_other_text"));
		termsDoc.setAttribute("/Terms/LoansAndFunds/InAdvanceDisposition",                    termsValuesDoc.getAttribute("/Terms/in_advance_disposition"));
		termsDoc.setAttribute("/Terms/LoansAndFunds/InArrearsDisposition",                    termsValuesDoc.getAttribute("/Terms/in_arrears_disposition"));
		termsDoc.setAttribute("/Terms/LoansAndFunds/WorkItemPriority",                        termsValuesDoc.getAttribute("/Terms/work_item_priority"));
		termsDoc.setAttribute("/Terms/LoansAndFunds/BeneficiaryMessage",                      termsValuesDoc.getAttribute("/Terms/message_to_beneficiary"));
		termsDoc.setAttribute("/Terms/LoansAndFunds/ReleaseByBankOn", 		                  termsValuesDoc.getAttribute("/Terms/release_by_bank_on"));
		termsDoc.setAttribute("/Terms/LoansAndFunds/BankChargesType", 		                  termsValuesDoc.getAttribute("/Terms/bank_charges_type"));
		// DK CR-640 Rel7.1
		termsDoc.setAttribute("/Terms/LoansAndFunds/FundsXrEfSettleType", 		              termsValuesDoc.getAttribute("/Terms/funds_xfer_settle_type"));
		termsDoc.setAttribute("/Terms/LoansAndFunds/AppDebitAccPrincipal", 		              "");
		//MDB POUL102455599 Rel7.1 10/25/11 - Begin
		termsDoc.setAttribute("/Terms/LoansAndFunds/DebitCCY", 		              			  "");
		termsDoc.setAttribute("/Terms/LoansAndFunds/DebitCountry", 		          			  "");
		//MDB POUL102455599 Rel7.1 10/25/11 - End
		termsDoc.setAttribute("/Terms/LoansAndFunds/AppDebitAcctCharges", 		              "");
		termsDoc.setAttribute("/Terms/LoansAndFunds/CreditAccPrincipal", 		              "");
		 // TP Refresh Change
		


		// Get Payee Account Number if available.  Need to get this off of the first terms party
		String payeeTermsPartyOID  = termsValuesDoc.getAttribute("/Terms/c_FirstTermsParty");
		if (payeeTermsPartyOID != null && !payeeTermsPartyOID.equals("")) {
			TermsParty payeeTermsParty = (TermsParty)(terms.getComponentHandle("FirstTermsParty"));
			String payeeAcctInfo = payeeTermsParty.getAttribute("acct_num");
			if (payeeTermsParty.getAttribute("acct_currency").equals("")) {
				termsDoc.setAttribute("/Terms/LoansAndFunds/PayeeAccountNumber",				  payeeAcctInfo);
			} else {
				termsDoc.setAttribute("/Terms/LoansAndFunds/PayeeAccountNumber",				  payeeAcctInfo + " (" + payeeTermsParty.getAttribute("acct_currency") + ")");
			}
		}
		termsDoc.setAttribute("/Terms/LoansAndFunds/FundingAmount", 		                  termsValuesDoc.getAttribute("/Terms/funding_amount"));

       // IValavala CR451 get the AppDebitAccPrincipal accountnum from account
		String accountUoid = termsValuesDoc.getAttribute("/Terms/debit_account_oid");

		// IAZ ER 5.0.1.1 06/29/09 Begin
	    String accountNumSQL;
	    String accountNum;
	    String branch = ""; //cquinton 3/29/2011 ir#laul032634214
	    DocumentHandler resultXML;

		if (StringFunction.isNotBlank(accountUoid) ||
		   (!TradePortalConstants.OTHER.equals(termsValuesDoc.getAttribute("/Terms/funds_xfer_settle_type"))))
		{

	    // CR-610 - Adarsha K S- 2/21/2011 - Start
		//MDB CR-640 Rel7.1 10/13/11 - BEGIN added currency and bank_country_code
		accountNumSQL = "SELECT ACCOUNT_NUMBER, SOURCE_SYSTEM_BRANCH, CURRENCY, ADDL_VALUE as BANK_COUNTRY_CODE FROM ACCOUNT, REFDATA WHERE ACCOUNT_OID = ?" +
						" AND ACCOUNT.BANK_COUNTRY_CODE=REFDATA.CODE AND REFDATA.TABLE_TYPE='" + TradePortalConstants.COUNTRY_ISO3116_3 +
						"' and REFDATA.LOCALE_NAME IS NULL"; //MDB PKUL102163801 Rel7.1 10/21/11
	    accountNum = "";
	    String debitCCY = "";
	    String debitCountry = "";
	    resultXML = DatabaseQueryBean.getXmlResultSet(accountNumSQL, false, new Object[]{accountUoid});
		// IAZ ER 5.0.1.1 06/29/09 End

	    if(resultXML != null)
	     {
		    accountNum = resultXML.getAttribute("/ResultSetRecord(" + 0 + ")/ACCOUNT_NUMBER");
		    branch = resultXML.getAttribute("/ResultSetRecord(" + 0 + ")/SOURCE_SYSTEM_BRANCH");
		    debitCCY = resultXML.getAttribute("/ResultSetRecord(" + 0 + ")/CURRENCY");
		    debitCountry = resultXML.getAttribute("/ResultSetRecord(" + 0 + ")/BANK_COUNTRY_CODE");
	 	  }
		//MDB CR-640 Rel7.1 10/13/11 - END added currency and bank_country_code
		termsDoc.setAttribute("/Terms/LoansAndFunds/AppDebitAccPrincipal", 		              accountNum);
	    //MDB PKUL102043568 Rel7.1 10/20/11 Begin - moved location in XML to match DTD
		termsDoc.setAttribute("/Terms/LoansAndFunds/DebitCCY",            	debitCCY);
		termsDoc.setAttribute("/Terms/LoansAndFunds/DebitCountry",        	debitCountry);
	    
		// IValavala CR434 Adding CreditAccPrincipal. accountnum from account
		//accountUoid = termsValuesDoc.getAttribute("/Terms/payment_charges_account_oid");
		accountNum = "";
		resultXML = DatabaseQueryBean.getXmlResultSet(accountNumSQL, false, new Object[]{accountUoid});
		if(resultXML != null)
		 {
		    accountNum = resultXML.getAttribute("/ResultSetRecord(" + 0 + ")/ACCOUNT_NUMBER");
		  }
		termsDoc.setAttribute("/Terms/LoansAndFunds/CreditAccPrincipal", 		              accountNum);

        // IAZ ER 5.0.1.1 06/29/09 Begin
		}
	    // IAZ ER 5.0.1.1 06/29/09 End

		//Vshah - IR#ANUJ041553240 - BEGIN
		accountUoid = termsValuesDoc.getAttribute("/Terms/payment_charges_account_oid");
		accountNumSQL = "SELECT ACCOUNT_NUMBER FROM ACCOUNT WHERE ACCOUNT_OID = ?" ;
		accountNum = "";
		resultXML = DatabaseQueryBean.getXmlResultSet(accountNumSQL, false, new Object[]{accountUoid});
		if(resultXML != null)
		 {
		    accountNum = resultXML.getAttribute("/ResultSetRecord(" + 0 + ")/ACCOUNT_NUMBER");
		  }

		termsDoc.setAttribute("/Terms/LoansAndFunds/AppDebitAcctCharges", 					  accountNum);
		//Vshah - IR#ANUJ041553240 - END

// IValavala CR434 Adding DailyLimitExceeded, PaymentDate Value will be added in the TPLPackager as it comes from Transaction
		termsDoc.setAttribute("/Terms/LoansAndFunds/DailyLimitExceeded", 		              "");
		termsDoc.setAttribute("/Terms/LoansAndFunds/PaymentDate", "");
        //cquinton 3/29/2011 Rel 7.0.0 ir#laul032634214 start
        termsDoc.setAttribute("/Terms/LoansAndFunds/DebitAcctSourceSystemBranch", branch);
        //cquinton 3/29/2011 Rel 7.0.0 ir#laul032634214 end

// Edozie's Code - CR 375 - 8-39-07 Begins
        //	Other_Conditions
		termsDoc.setAttribute("/Terms/OtherConditions/ConfirmationType",         termsValuesDoc.getAttribute("/Terms/confirmation_type"));
		termsDoc.setAttribute("/Terms/OtherConditions/Revolve",                  termsValuesDoc.getAttribute("/Terms/revolve"));
		termsDoc.setAttribute("/Terms/OtherConditions/AdditionalConditions",     termsValuesDoc.getAttribute("/Terms/additional_conditions"));


		termsDoc.setAttribute("/Terms/AdditionalDocumentText",     termsValuesDoc.getAttribute("/Terms/addl_doc_text"));
// Edozie's Code - CR 375 - 8-39-07 Ends

		// TERMS PARTY

		DocumentHandler termsPartyDoc = new DocumentHandler();

		//Additional Terms Party
		String testTP = TermsPartyType.PAYER;


		int id = 0;

		termsPartyDoc.setAttribute("/TermsParty/TotalNumberOfEntries","0");
		String countryOfCOL = "";
		for (int i = 0; i<TradePortalConstants.MAX_NUM_TERMS_PARTIES;i++) {
			String termsPartyOID = termsValuesDoc.getAttribute("/Terms/"+TradePortalConstants.TERMS_PARTY_ATTRIBUTE_NAMES[i]);
			if(!(termsPartyOID == null || termsPartyOID.equals(""))) {
				TermsParty termsParty = (TermsParty)(terms.getComponentHandle(TradePortalConstants.TERMS_PARTY_COMPONENT_NAMES[i]));
				if(termsParty.getAttribute("terms_party_type").equals(testTP)) {
					LOG.debug("party_Type is APP, hence I need to check for thirdParty");
					termsPartyDoc = packageAdditionalTermsParty(terms,id,termsPartyDoc,termsParty);
					if(termsPartyDoc.getAttribute("/AdditionalTermsParty").equals("Y"))
						id++;
				}
				termsPartyDoc.setAttribute("/AdditionalTermsParty","N");
				//If the termsValuesDoc contains termsParty also need to modify the line below
				if(termsParty.getAttribute("terms_party_type").equals(TermsPartyType.COLLECTING_BANK))
					countryOfCOL = termsParty.getAttribute("address_country");
				if(termsParty.getAttribute("terms_party_type").equals(TermsPartyType.CASE_OF_NEED))
					termsParty.setAttribute("address_country",countryOfCOL);
				termsPartyDoc = termsParty.packageTermsPartyAttributes(id,termsPartyDoc);
				id++;
			}
		}

		LOG.debug("This is TermsPartyDoc: {} " ,  termsPartyDoc.toString());

		termsPartyDoc.setAttribute("/TermsParty/TotalNumberOfEntries",String.valueOf(id));
		termsDoc.setComponent("/TermsParty",termsPartyDoc.getFragment("/TermsParty"));

		return termsDoc;
	}


	/**
	 * Performs presave processing (regardless of whether validation is done)
	 *
	 * @param terms com.ams.tradeportal.busobj.TermsBean
	 * @exception com.amsinc.ecsg.frame.AmsException The exception description.
	 */
	public void preSave(TermsBean terms) throws AmsException {
		super.preSave(terms);

		try {
			validateAmount(terms, "amount", "amount_currency_code");
			validateAmount(terms, "funding_amount", "amount_currency_code");

			validateTextLengths(terms);

			validateDecimalFields(terms);

		} catch (RemoteException e) {
			LOG.error("Remote Exception caught in FTRQ__ISSTermsMgr.preSave()",e);
		}

	}

}