package com.ams.tradeportal.busobj.termsmgr;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

import com.amsinc.ecsg.frame.*;
import com.ams.tradeportal.busobj.*;
import com.ams.tradeportal.busobj.webbean.*;
import com.ams.tradeportal.common.*;

import java.rmi.*;

import com.amsinc.ecsg.util.*;
import com.ams.tradeportal.busobj.util.*;

/**
 * This class manages the dynamic attribute registration and validation
 * for a Terms business object or TermsWebBean for an Import DLC Amendment
 *
 *  
 *     Copyright  © 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class IMP_DLC__AMDTermsMgr extends TermsMgr
 {
private static final Logger LOG = LoggerFactory.getLogger(IMP_DLC__AMDTermsMgr.class);
	
 	/**
	 *  Constructor that is used by business objects to create their
	 *  manager.  This method also register the dynamic attributes of
	 *  the bean.
	 *
	 *  @param mgr AttributeManager attribute manager of the bean being managed.  Used to register attributes
	 */
	public IMP_DLC__AMDTermsMgr(AttributeManager mgr) throws AmsException
	 {
	  super(mgr);
	 }

	/**
	 *  Constructor that is used by web beans to create their
	 *  manager.  This method also register the dynamic attributes of
	 *  the bean.
	 *
	 *  @param mgr TermsWebBean - the bean being managed.  Used to register attributes.
	 */
	public IMP_DLC__AMDTermsMgr(TermsWebBean terms) throws AmsException
	 {
	  super(terms);
	 }

	/**
	 *  Returns a list of attributes.  These describe each of the attributes
	 *  that will be registered for the bean being managed.
	 *
	 *  This method controls which attributes of TermsBean or TermsWebBean are registered
	 *  for an this instrument and transaction type
	 *
	 *  @return Vector - list of attributes
	 */
			       public List<Attribute> getAttributesToRegister()
     {
        List<Attribute> attrList = super.getAttributesToRegister();
        attrList.add( TermsBean.Attributes.create_additional_conditions() );
        attrList.add( TermsBean.Attributes.create_amount() );
        attrList.add( TermsBean.Attributes.create_amount_currency_code() );
        attrList.add( TermsBean.Attributes.create_amt_tolerance_neg() );
        attrList.add( TermsBean.Attributes.create_amt_tolerance_pos() );
        attrList.add( TermsBean.Attributes.create_expiry_date() );
        attrList.add( TermsBean.Attributes.create_reference_number() );
        attrList.add( TermsBean.Attributes.create_special_bank_instructions() );
        attrList.add( TermsBean.Attributes.create_work_item_number() );
        attrList.add( TermsBean.Attributes.create_place_of_expiry() );

        return attrList;
     }





	/**
	 *  Performs validation of the attributes of this type of instrument and transaction.   This method is
	 *  called from the userValidate() hook in the managed object.
	 *
	 *  @param terms - the bean being validated
	 */
public void validate(TermsBean terms)
			throws java.rmi.RemoteException, AmsException
{
        // Variables used to retrieve the shipment terms
        ShipmentTerms shipmentTerms = null;
        StringBuffer  shipmentNumDesc = new StringBuffer();
        
        super.validate(terms);

        // Retrieve the shipment terms component
        shipmentTerms = terms.getFirstShipment();
        
	// If all of these fields are blank, issue an error
	if( StringFunction.isBlank(terms.getAttribute("amt_tolerance_neg")) &&
			StringFunction.isBlank(terms.getAttribute("amt_tolerance_pos")) &&
			StringFunction.isBlank(terms.getAttribute("amount")) &&
			StringFunction.isBlank(terms.getAttribute("expiry_date")) &&
			StringFunction.isBlank(terms.getAttribute("special_bank_instructions")) &&
			StringFunction.isBlank(terms.getAttribute("additional_conditions")))
         {
			//Krishna DGUH060851916 shipment_from_loading(Airport of Departure) 
			//and shipment_to_discharge(For Transport To) to the list.If all of the following
			// are left blank for which error would be issued.Atleast one of the above list or 
			//below list is necessary to be filled in the Amend transaction page.
            if((shipmentTerms == null) ||  
               (StringFunction.isBlank(shipmentTerms.getAttribute("latest_shipment_date")) &&
            		   StringFunction.isBlank(shipmentTerms.getAttribute("incoterm")) &&
            		   StringFunction.isBlank(shipmentTerms.getAttribute("goods_description")) &&
            		   StringFunction.isBlank(shipmentTerms.getAttribute("incoterm_location")) &&
            		   StringFunction.isBlank(shipmentTerms.getAttribute("shipment_from")) &&
            		   StringFunction.isBlank(shipmentTerms.getAttribute("shipment_to")) &&
            		   StringFunction.isBlank(shipmentTerms.getAttribute("shipment_from_loading")) &&
            		   StringFunction.isBlank(shipmentTerms.getAttribute("shipment_to_discharge"))) )
             {
		terms.getErrorManager().issueError(
							TradePortalConstants.ERR_CAT_1,
							TradePortalConstants.MUST_AMEND);
             }
         }

	String shipmentDate = null;
	String value;
        String description = "";
 

	// Expiry date must be after shipment date (if provided).
	// (Expiry date is a required field.  Shipment date is not.)
        if(shipmentTerms != null)
            shipmentDate = shipmentTerms.getAttribute("latest_shipment_date");
	value = terms.getAttribute("expiry_date");
	if (!StringFunction.isBlank(shipmentDate) && !StringFunction.isBlank(value)) {
		GregorianCalendar shipDate = new GregorianCalendar();
		GregorianCalendar expiryDate = new GregorianCalendar();

		shipDate.setTime(DateTimeUtility.convertStringDateTimeToDate(shipmentDate));
		expiryDate.setTime(DateTimeUtility.convertStringDateTimeToDate(value));
		LOG.debug("ship date: {} - exp. date: {}" , shipDate, expiryDate);
		// If the ship date is after the expiry date, give error
		if (shipDate.after(expiryDate)) {
			terms.getErrorManager().issueError(
							TradePortalConstants.ERR_CAT_1,
							TradePortalConstants.SHIP_DATE_PAST_EXPIRY_DATE,
                                                        description);
			LOG.debug("ship date after expiry");
		} else{
			LOG.debug("ship date before expiry");
		}

	}
	// Perform subclass-specific validation of text fields for SWIFT characters
	//validateForSwiftCharacters(shipmentTerms);
				
	shipmentNumDesc.append(terms.getResourceManager().getText("ImportDLCIssue.Shipment",
			TradePortalConstants.TEXT_BUNDLE));
	shipmentNumDesc.append(" -");
	
    // Validate the Goods Description and PO Line Items fields
	if(shipmentTerms!=null)
    validateGoodsDescriptionFields(shipmentTerms, terms);

    // Validate the Amount currency code with the PO Line Items currency code.  They must match.
    validateAmountCurrencyAndPOCurrency(terms);
    //Krishna IR-RAUH101636054 11/23/2007 Begin
    validateForAmendTermsAmount(terms);
    //Krishna IR-RAUH101636054 11/23/2007 End    
 }
            //Krishna IR-RAUH101636054 11/23/2007 Begin
	/**
	 * Performs the Terms Manager-specific validity of the new Import DLC Amount in the Terms
	 *
	 * @param terms - the managed object
	 *
	 */
	protected void validateForAmendTermsAmount(TermsBean terms) throws AmsException, RemoteException
	{
	   String curAmt        = terms.getAttribute("amount");
	   String instrumentAmt = "";
	   
		String instrAmntQuery = "select INSTRUMENT_AMOUNT from TRANSACTION where "
				+ "TRANSACTION_OID in(select ACTIVE_TRANSACTION_OID from INSTRUMENT, TRANSACTION where "
				+ "TRANSACTION.C_CUST_ENTER_TERMS_OID = ? and "
				+ "TRANSACTION.P_INSTRUMENT_OID = INSTRUMENT.INSTRUMENT_OID)";      		
	     
     DocumentHandler instrumentDoc = DatabaseQueryBean.getXmlResultSet(instrAmntQuery, true, 
    		 new Object[]{terms.getAttribute("terms_oid")});
                  
                  if(instrumentDoc != null)
                  instrumentAmt = instrumentDoc.getAttribute("/ResultSetRecord(0)/INSTRUMENT_AMOUNT");	      
	   //Krishna IR-WUUI011042560(Handling NumberFormatException) -Begin 
	    double curAmtValue        = 0;
	    double instrumentAmtValue = 0;
	   try {
		   curAmtValue        = Double.parseDouble(curAmt);
		   instrumentAmtValue = Double.parseDouble(instrumentAmt);
	       } catch (NumberFormatException e) {
   
	      }
	    //Krishna IR-WUUI011042560 -End
	     //Newly claculated Import LC Amend amount can not be negative
		if (curAmtValue+instrumentAmtValue<0 ) 
		{
			terms.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.NEGATIVE_AMOUNT);
		}	
	}
        //Krishna IR-RAUH101636054 11/23/2007 End	

/**
 * Performs validation for the shipment section.
 *
 * @param currentTerms com.ams.tradeportal.busobj.TermsBean
 * @param terms com.ams.tradeportal.busobj.ShipmentTerms
 * @param shipmentNumDesc java.lang.String
 */
public void validateShipment(TermsBean currentTerms, ShipmentTerms shipmentTerms, String shipmentNumDesc)
throws java.rmi.RemoteException, AmsException {

	String shipFrom;
	String shipLoad;
	String shipTo;
	String shipDischarge;

//	 Give warning if both shipment from fields are blank
	shipFrom = shipmentTerms.getAttribute("shipment_from");
	shipLoad = shipmentTerms.getAttribute("shipment_from_loading");
	if (StringFunction.isBlank(shipFrom) && StringFunction.isBlank(shipLoad)) {
		String[] errorParameters = {shipmentNumDesc};
		boolean[] wrapParameters = {false};
		shipmentTerms.getErrorManager().issueError(
				TradePortalConstants.ERR_CAT_1,
				TradePortalConstants.SHIP_FROM_MISSING,
				errorParameters, wrapParameters);
	}

	// Give warning if both shipment to fields are blank.
	shipTo = shipmentTerms.getAttribute("shipment_to");
	shipDischarge= shipmentTerms.getAttribute("shipment_to_discharge");
	if (StringFunction.isBlank(shipTo)&& StringFunction.isBlank(shipDischarge)) {
		String[] errorParameters = {shipmentNumDesc};
		boolean[] wrapParameters = {false};
		shipmentTerms.getErrorManager().issueError(
				TradePortalConstants.ERR_CAT_1,
				TradePortalConstants.SHIP_TO_MISSING,
				errorParameters, wrapParameters);
	}
}

    /**
     * Performs the Terms Manager-specific validation of text fields in the Terms
     * and TermsParty objects.
     *
     * @param terms - the managed object
     *
     */
    protected void validateForSwiftCharacters(TermsBean terms) throws AmsException, RemoteException
     {
        final String[] attributesToExclude = {"special_bank_instructions"};

        InstrumentServices.checkForInvalidSwiftCharacters(terms.getAttributeHash(), terms.getErrorManager(), attributesToExclude);

         ShipmentTerms shipmentTerms = terms.getFirstShipment();
         if(shipmentTerms != null)
          {
        	 InstrumentServices.checkForInvalidSwiftCharacters(shipmentTerms.getAttributeHash(), shipmentTerms.getErrorManager());        
          }
     }

    /**
     *  Performs validation of text fields for length.
     *
     *  @param terms TermsBean - the bean being validated
     */
    public void validateTextLengths(TermsBean terms) throws java.rmi.RemoteException, AmsException
    {
    	//String overrideSwiftLengthInd = getOverrideSwiftLengthInd(terms);
    	/*KMehta Rel 9500 IR T36000034805 on 16 Mar 2016 Commented Below Code as per updated email from Ann C
    	The setting - Allow more characters than SWIFT recommended field size limitations for Goods Description,
    	Documents Required and Additional Conditions fields applies to Issue transactions only. */
    	//Changing limit to 5000 for consistency with DLC Issue.
    	/*if((overrideSwiftLengthInd != null)
				&& (overrideSwiftLengthInd.equals(TradePortalConstants.INDICATOR_NO)))
		{*/
    		// IR T36000034805, updated max length
			//checkTextLength(terms, "additional_conditions", 3300);
		//}
    	//Vshah - 6/16/08 - PPX-067 - Change the third argument in below finction call to 3000 from 1000.
    	//Changing limit back to 1000 since TradeForm maximum length fails  for Goods Desc+Spcl BAnk Instr
    	// IR T36000048753 Rel9.5
    	checkTextLength(terms, "special_bank_instructions", 5000);
    }

/**
 * This method is used to validate the Goods Description and PO Line Items
 * fields.  If data has been entered in the Goods Description text area box, the method
 * will issue an error if its length exceeds 6500 characters. If the length
 * does NOT exceed 6500 characters and data has been enetered in the PO Line
 * Items text area box, it will then determine whether the combined lengths are
 * greater than 6500 characters; an appropriate error message will be issued if
 * this total is greater than 6500.
 * @param terms 
 *

 * @exception  com.amsinc.ecsg.frame.AmsException
 * @exception  java.rmi.RemoteException
 * @return     void
 */
 public void validateGoodsDescriptionFields(ShipmentTerms shipmentTerms, TermsBean terms) throws RemoteException, AmsException
 {
    String   goodsDescription = null;
    String   poLineItems      = null;
    String   endText          = null;
    String   description      = "";
    String	 additional_conditions = null;
    int 	 total = 0; //Rel 9.5 IR T36000049311
    String temp = null;

    goodsDescription = shipmentTerms.getAttribute("goods_description");
    poLineItems      = shipmentTerms.getAttribute("po_line_items");
    additional_conditions      = terms.getAttribute("additional_conditions");
    
    String overrideSwiftLengthInd = getOverrideSwiftLengthInd(shipmentTerms);
  //Rel 9.5 IR T36000034805 start
    if(StringFunction.isNotBlank(goodsDescription)){
    	temp = goodsDescription;
    	temp = temp.replaceAll("(\\r\\n)", "");
    	total = temp.length();
    }
    if(StringFunction.isNotBlank(poLineItems)){    	
    	temp = poLineItems;
    	temp = temp.replaceAll("(\\r\\n)", "");
    	total = total+temp.length();
    }
    if(StringFunction.isNotBlank(additional_conditions)){    	
    	temp = additional_conditions;
    	temp = temp.replaceAll("(\\r\\n)", "");
    	total = total+temp.length();
    }
    
    if(total > 3300){
    	terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                TradePortalConstants.LINE_ITEMS_EXCEED_MAX_CHARS,
                description);
    }
  //Rel 9.5 IR T36000034805 end
    
   }



 /**
  * This method is used to validate the currency Code of the Transaction Amount
  * with the Currency Code of the PO Line Items.  The two must match.  The PO Line Items
  * will always have the same currency code as per the PO design validations, therefore, we
  * only need to determine what that single currency code is and compare it to the currency
  * code of the transaction.
  *

  * @exception  com.amsinc.ecsg.frame.AmsException
  * @exception  java.rmi.RemoteException
  * @return     void
  */
  public void validateAmountCurrencyAndPOCurrency(TermsBean terms) throws RemoteException, AmsException
  {
     String   amountCurrency	  = null;

     // Get the currency code of the transaction.  If not blank, proceed with validating that it is
     // the same as the PO line items currency code, otherwise fall through and let the required
     // validation process flag it.
     amountCurrency = terms.getAttribute("amount_currency_code");

 	if ((amountCurrency != null) && (!amountCurrency.equals("")))
     {
         // Query the database to get the currency code of the PO line items.
 	    LOG.debug("Inside validateAmountCurrencyAndPOCurrency()");
 	    LOG.debug("Getting currency code from po_line_item using sql statement.");

        DocumentHandler    poLineItemCurrencyDoc       = null;
        StringBuilder       sqlQuery                    = null;
        String             poLineItemCurrencyCode 	   = null;
        String 			   termsOid 			   	   = terms.getAttribute("terms_oid");


 	    sqlQuery = new StringBuilder();
        sqlQuery.append("select distinct p.currency ");
        sqlQuery.append("from po_line_item p, terms t, transaction r ");
        sqlQuery.append("where t.terms_oid = r.c_cust_enter_terms_oid ");
        sqlQuery.append("and r.transaction_oid = p.a_assigned_to_trans_oid ");
        sqlQuery.append("and t.terms_oid = ?");

		LOG.debug(sqlQuery.toString());

		// If value found, compare the PO line item value to the amountCurrency value.
		// If nothing found, then no PO line items have been added and we do not need to continue as there is
		// nothing for us to validate against.
		//SSikhakolli - 05/06/2014 - Rel 9.0 IR#T36000026319 - SQL fix - Begin
        poLineItemCurrencyDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery.toString(), true, new Object[]{termsOid});
        //SSikhakolli - 05/06/2014 - Rel 9.0 IR#T36000026319 - SQL fix - End

		if (poLineItemCurrencyDoc != null)
		{
			poLineItemCurrencyCode = poLineItemCurrencyDoc.getAttribute("/ResultSetRecord(0)/CURRENCY");

			if ((poLineItemCurrencyCode != null) && (!poLineItemCurrencyCode.equals("")))
			{
				if (!poLineItemCurrencyCode.equals(amountCurrency))
				{
				   terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
													  TradePortalConstants.PO_ITEM_CURR_NOT_SAME_AS_TRANS);
				}
			}
		}
    }
 }



 /**
 * Performs presave processing (regardless of whether validation is done)
 *
 * @param terms com.ams.tradeportal.busobj.termsBean
 * @exception com.amsinc.ecsg.frame.AmsException The exception description.
 */
 public void preSave(TermsBean terms) throws AmsException
 {
    super.preSave(terms);

    try
    {
    	// IR T36000048753 Rel9.5
       validateTextLengths(terms);

       validateAmount(terms, "amount", "amount_currency_code");
    }
    catch (RemoteException e)
    {
       LOG.error("Remote Exception caught in IMP_DLC__AMDTermsMgr.preSave()",e);
    }
 }



   //Beginning of code used by the Middleware team
     /**
 	 *  gets the values of the attributes of the managed object. This method is called
 	 *  from the getTermsAttributesDoc() hook in the managed object.
 	 *
 	 *  return com.amsinc.ecsg.util.DocumentHandler
 	 *  @param terms TermsBean - the bean being validated
 	 *  @param termsDoc com.amsinc.ecsg.util.DocumentHandler
 	 */
    public DocumentHandler getManagerTerms(TermsBean terms, DocumentHandler termsDoc) throws java.rmi.RemoteException, AmsException

    {

    	      DocumentHandler termsValuesDoc = terms.populateXmlDoc(new DocumentHandler());

            //Terms_Details
             termsDoc.setAttribute("/Terms/TermsDetails/ExpiryDate",				 termsValuesDoc.getAttribute("/Terms/expiry_date"));
             termsDoc.setAttribute("/Terms/TermsDetails/Amount",                     termsValuesDoc.getAttribute("/Terms/amount"));
             termsDoc.setAttribute("/Terms/TermsDetails/AmountTolerancePositive",	 termsValuesDoc.getAttribute("/Terms/amt_tolerance_pos"));
             termsDoc.setAttribute("/Terms/TermsDetails/AmountToleranceNegative",	 termsValuesDoc.getAttribute("/Terms/amt_tolerance_neg"));
             termsDoc.setAttribute("/Terms/TermsDetails/PlaceOfExpiry", 	         terms.getAttribute("place_of_expiry"));
            

             // ShipmentTerms
              populateFirstShipmentTermsXml(terms, termsDoc); 
             // End of ShipmentTerms

             //OtherConditions
             termsDoc.setAttribute("/Terms/OtherConditions/AdditionalConditions",			termsValuesDoc.getAttribute("/Terms/additional_conditions"));
             //End of Other_Conditions

            //Instructions
             termsDoc.setAttribute("/Terms/Instructions/InstructionsToBank",			termsValuesDoc.getAttribute("/Terms/special_bank_instructions"));
             //End of Instructions

    		return termsDoc;
    }

  //End of Code by Indu Valavala


}