/**
 * 
 */
package com.ams.tradeportal.busobj.termsmgr;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.busobj.TermsBean;
import com.ams.tradeportal.busobj.webbean.TermsWebBean;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.Attribute;
import com.amsinc.ecsg.frame.AttributeManager;
import com.amsinc.ecsg.util.DocumentHandler;

/**

 *
 */
public class BNWBankRelTermsMgr extends TermsMgr{
private static final Logger LOG = LoggerFactory.getLogger(BNWBankRelTermsMgr.class);

	/**
	 *  Constructor that is used by business objects to create their
	 *  manager.  This method also register the dynamic attributes of
	 *  the bean.
	 *
	 *  @param mgr AttributeManager attribute manager of the bean being managed.  Used to register attributes
	 */
	public BNWBankRelTermsMgr(AttributeManager mgr) throws AmsException
	{
		super(mgr);
	}

	/**
	 *  Constructor that is used by web beans to create their
	 *  manager.  This method also register the dynamic attributes of
	 *  the bean.
	 *
	 *  @param mgr TermsWebBean - the bean being managed.  Used to register attributes.
	 */
	public BNWBankRelTermsMgr(TermsWebBean terms) throws AmsException
	{
		super(terms);
	}

	/**
	 *  Returns a list of attributes.  These describe each of the attributes
	 *  that will be registered for the bean being managed.
	 *
	 *  This method controls which attributes of TermsBean or TermsWebBean are registered
	 *  for bank released terms with this transaction type
	 *
	 *  @return Vector - list of attributes
	 */
	public List<Attribute> getAttributesToRegister()
	{
		List<Attribute> attrList = super.getAttributesToRegister();
		attrList.add( TermsBean.Attributes.create_amount() );
		attrList.add( TermsBean.Attributes.create_amount_currency_code() );
		attrList.add( TermsBean.Attributes.create_expiry_date() );
		attrList.add( TermsBean.Attributes.create_value_date() );
		attrList.add( TermsBean.Attributes.create_work_item_number() );
		attrList.add( TermsBean.Attributes.create_purpose_type() );
		
		return attrList;
	}


	/**
	 * This method is supposed to be used to set attributes of Terms business object.
	 *
	 * @param inputDoc DocumentHandler - universal XML message from MQSeries
	 * @param terms TermsBean
	 * @return boolean
	 */
	public boolean setManagerTerms(TermsBean terms, DocumentHandler inputDoc)
			throws java.rmi.RemoteException, AmsException{
		if(inputDoc!=null){
			LOG.info("Inside setManagerTerms().. inputDoc= {} ",inputDoc.toString());

		terms.setAttribute("amount",               	    inputDoc.getAttribute("/Terms/TermsDetails/Amount"));
		terms.setAttribute("amount_currency_code", 	    inputDoc.getAttribute("/Terms/TermsDetails/AmountCurrencyCode"));
		terms.setAttribute("expiry_date",           	inputDoc.getAttribute("/Terms/TermsDetails/ExpiryDate"));
		terms.setAttribute("value_date",           	    inputDoc.getAttribute("/Terms/TermsDetails/ValueDate"));
		terms.setAttribute("work_item_number",      inputDoc.getAttribute("/Terms/TermsDetails/WorkItemNumber"));
		terms.setAttribute("purpose_type",		       inputDoc.getAttribute("/Terms/TermsDetails/PurposeType"));
		}

		return true ;
	}



}
