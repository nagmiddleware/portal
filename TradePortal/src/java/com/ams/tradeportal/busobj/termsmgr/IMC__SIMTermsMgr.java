	package com.ams.tradeportal.busobj.termsmgr;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import com.ams.tradeportal.busobj.*;
import com.ams.tradeportal.busobj.webbean.*;
import com.ams.tradeportal.common.*;

/**
 * This class manages the dynamic attribute registration and validation
 * for a Terms business object or TermsWebBean for an Issue Loan Request
 *
 *
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class IMC__SIMTermsMgr extends TermsMgr
 {
private static final Logger LOG = LoggerFactory.getLogger(IMC__SIMTermsMgr.class);
    /**
     *  Constructor that is used by business objects to create their
     *  manager.  This method also register the dynamic attributes of
     *  the bean.
     *
     *  @param mgr AttributeManager attribute manager of the bean being managed.  Used to register attributes
     */
    public IMC__SIMTermsMgr(AttributeManager mgr) throws AmsException
     {
	  super(mgr);
     }

    /**
     *  Constructor that is used by web beans to create their
     *  manager.  This method also register the dynamic attributes of
     *  the bean.
     *
     *  @param mgr TermsWebBean - the bean being managed.  Used to register attributes.
     */
    public IMC__SIMTermsMgr(TermsWebBean terms) throws AmsException
     {
	  super(terms);
     }

    /**
     *  Returns a list of attributes.  These describe each of the attributes
     *  that will be registered for the bean being managed.
     *
     *  This method controls which attributes of TermsBean or TermsWebBean are registered
     *  for an this instrument and transaction type
     *
     *  @return Vector - list of attributes
     */
     public List<Attribute> getAttributesToRegister()
       {
        List<Attribute> attrList = super.getAttributesToRegister(); 
        attrList.add( TermsBean.Attributes.create_amount() );
        attrList.add( TermsBean.Attributes.create_amount_currency_code() );
        attrList.add( TermsBean.Attributes.create_work_item_number() );
        attrList.add( TermsBean.Attributes.create_our_reference() );
        attrList.add( TermsBean.Attributes.create_apply_payment_on_date() );
        attrList.add( TermsBean.Attributes.create_pay_in_full_fin_roll_ind() );
        attrList.add( TermsBean.Attributes.create_account_remit_ind() );
        attrList.add( TermsBean.Attributes.create_fin_roll_full_partial_ind() );
        attrList.add( TermsBean.Attributes.create_principal_account_oid() );
        attrList.add( TermsBean.Attributes.create_charges_account_oid() );
        attrList.add( TermsBean.Attributes.create_interest_account_oid() );
        attrList.add( TermsBean.Attributes.create_fin_roll_partial_pay_amt() );
        attrList.add( TermsBean.Attributes.create_rollover_terms_ind() );
        attrList.add( TermsBean.Attributes.create_fin_roll_number_of_days() );
        attrList.add( TermsBean.Attributes.create_fin_roll_curr() );
        attrList.add( TermsBean.Attributes.create_fin_roll_maturity_date() );
        attrList.add( TermsBean.Attributes.create_other_addl_instructions() );
        attrList.add( TermsBean.Attributes.create_other_addl_instructions_ind() );
        attrList.add( TermsBean.Attributes.create_daily_rate_fec_oth_ind() );
        attrList.add( TermsBean.Attributes.create_settle_covered_by_fec_num() );
        attrList.add( TermsBean.Attributes.create_settle_fec_rate() );
        attrList.add( TermsBean.Attributes.create_settle_fec_currency() );
        attrList.add( TermsBean.Attributes.create_settle_other_fec_text() );
        return attrList;
     }



	/**
	 *  Performs validation of the attributes of the managed object.   This method is called
	 *  from the userValidate() hook in the managed object.
	 *
	 *  @param terms TermsBean - the bean being validated
	 */
	public void validate(TermsBean terms)
		throws java.rmi.RemoteException, AmsException {

		super.validate(terms);		
	}




	//Beginning of code used by the Middleware team
	/**
	 *  gets the values of the attributes of the managed object. This method is called
	 *  from the getTermsAttributesDoc() hook in the managed object.
	 *
	 *  return com.amsinc.ecsg.util.DocumentHandler
	 *  @param terms TermsBean - the bean being validated
	 *  @param termsDoc com.amsinc.ecsg.util.DocumentHandler
	 */
	public DocumentHandler getManagerTerms(TermsBean terms, DocumentHandler termsDoc) throws java.rmi.RemoteException, AmsException

	{

			 DocumentHandler termsValuesDoc = terms.populateXmlDoc(new DocumentHandler());
			 LOG.debug("This is the termValuesDoc containing all the terms values: {}",termsValuesDoc.toString());

		// MQ DTD FIELD                                                                       TRADEPORTAL FIELD
		// TERMS DETAILS TERMS
		termsDoc.setAttribute("/Terms/TermsDetails/ReferenceNumber",	                      termsValuesDoc.getAttribute("/Terms/our_reference"));
		termsDoc.setAttribute("/Terms/TermsDetails/AmountCurrencyCode",       	              termsValuesDoc.getAttribute("/Terms/amount_currency_code"));
		termsDoc.setAttribute("/Terms/TermsDetails/Amount",                    	              termsValuesDoc.getAttribute("/Terms/amount"));

		// TERMS PARTY
	   DocumentHandler termsPartyDoc = new DocumentHandler();

		//Additional Terms Party
		String testTP = TermsPartyType.APPLICANT;


		int id = 0;
		termsPartyDoc.setAttribute("/TermsParty/TotalNumberOfEntries","0");
		String countryOfCOL = "";
		for (int i = 0; i<TradePortalConstants.MAX_NUM_TERMS_PARTIES;i++) {
			String termsPartyOID = termsValuesDoc.getAttribute("/Terms/"+TradePortalConstants.TERMS_PARTY_ATTRIBUTE_NAMES[i]);
			if(!(termsPartyOID == null || termsPartyOID.equals(""))) {
				TermsParty termsParty = (TermsParty)(terms.getComponentHandle(TradePortalConstants.TERMS_PARTY_COMPONENT_NAMES[i]));
				if(termsParty.getAttribute("terms_party_type").equals(testTP)) {
					LOG.debug("party_Type is APP, hence I need to check for thirdParty");
					termsPartyDoc = packageAdditionalTermsParty(terms,id,termsPartyDoc,termsParty);
					if(termsPartyDoc.getAttribute("/AdditionalTermsParty").equals(TradePortalConstants.INDICATOR_YES))
					id++;
				}
				termsPartyDoc.setAttribute("/AdditionalTermsParty",TradePortalConstants.INDICATOR_NO);
				//If the termsValuesDoc contains termsParty also need to modify the line below
				if(termsParty.getAttribute("terms_party_type").equals(TermsPartyType.COLLECTING_BANK))
					countryOfCOL = termsParty.getAttribute("address_country");
				if(termsParty.getAttribute("terms_party_type").equals(TermsPartyType.CASE_OF_NEED))
					termsParty.setAttribute("address_country",countryOfCOL);
					 termsPartyDoc = termsParty.packageTermsPartyAttributes(id,termsPartyDoc);
					 id++;
			}
		}

		LOG.debug("This is TermsPartyDoc: {} " , termsPartyDoc.toString());

		termsPartyDoc.setAttribute("/TermsParty/TotalNumberOfEntries",String.valueOf(id));
		termsDoc.setComponent("/TermsParty",termsPartyDoc.getFragment("/TermsParty"));

		return termsDoc;
	}

	/**
	 * Performs presave processing (regardless of whether validation is done)
	 *
	 * @param terms com.ams.tradeportal.busobj.TermsBean
	 * @exception com.amsinc.ecsg.frame.AmsException The exception description.
	 */
	public void preSave(TermsBean terms) throws AmsException {
		  super.preSave(terms);

		

	   }


 }