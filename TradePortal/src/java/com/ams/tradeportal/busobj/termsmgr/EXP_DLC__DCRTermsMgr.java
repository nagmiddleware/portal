package com.ams.tradeportal.busobj.termsmgr;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

import com.amsinc.ecsg.frame.*;
import com.ams.tradeportal.busobj.*;
import com.ams.tradeportal.busobj.webbean.*;
import com.ams.tradeportal.common.*;

import com.amsinc.ecsg.util.*;

/**
 * This class manages the dynamic attribute registration and validation
 * for a Terms business object or TermsWebBean for an Export LC Discrepancy Response
 *
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class EXP_DLC__DCRTermsMgr extends DCRBankTermsMgr
 {
private static final Logger LOG = LoggerFactory.getLogger(EXP_DLC__DCRTermsMgr.class);
    /**
     *  Constructor that is used by business objects to create their
     *  manager.  This method also register the dynamic attributes of
     *  the bean.
     *
     *  @param mgr AttributeManager attribute manager of the bean being managed.  Used to register attributes
     */
    public EXP_DLC__DCRTermsMgr(AttributeManager mgr) throws AmsException
     {
	  super(mgr);
     }

    /**
     *  Constructor that is used by web beans to create their
     *  manager.  This method also register the dynamic attributes of
     *  the bean.
     *
     *  @param mgr TermsWebBean - the bean being managed.  Used to register attributes.
     */
    public EXP_DLC__DCRTermsMgr(TermsWebBean terms) throws AmsException
     {
	  super(terms);
     }

    /**
     *  Returns a list of attributes.  These describe each of the attributes
     *  that will be registered for the bean being managed.
     *
     *  This method controls which attributes of TermsBean or TermsWebBean are registered
     *  for an this instrument and transaction type
     *
     *  @return Vector - list of attributes
     */
                               public List<Attribute> getAttributesToRegister()
     {
        List<Attribute> attrList = super.getAttributesToRegister();
        attrList.add( TermsBean.Attributes.create_amount() );
        attrList.add( TermsBean.Attributes.create_amount_currency_code() );
        attrList.add( TermsBean.Attributes.create_drawing_number() );
        attrList.add( TermsBean.Attributes.create_export_discrepancy_instr() );
        attrList.add( TermsBean.Attributes.create_instr_other() );
        attrList.add( TermsBean.Attributes.create_work_item_number() );
        attrList.add( TermsBean.Attributes.create_our_reference() );
        attrList.add( TermsBean.Attributes.create_apply_payment_on_date() );
        attrList.add( TermsBean.Attributes.create_pay_in_full_fin_roll_ind() );
        attrList.add( TermsBean.Attributes.create_account_remit_ind() );
        attrList.add( TermsBean.Attributes.create_fin_roll_full_partial_ind() );
        attrList.add( TermsBean.Attributes.create_principal_account_oid() );
        attrList.add( TermsBean.Attributes.create_charges_account_oid() );
        attrList.add( TermsBean.Attributes.create_fin_roll_partial_pay_amt() );
        attrList.add( TermsBean.Attributes.create_rollover_terms_ind() );
        attrList.add( TermsBean.Attributes.create_fin_roll_number_of_days() );
        attrList.add( TermsBean.Attributes.create_fin_roll_curr() );
        attrList.add( TermsBean.Attributes.create_fin_roll_maturity_date() );
        attrList.add( TermsBean.Attributes.create_other_addl_instructions() );
        attrList.add( TermsBean.Attributes.create_other_addl_instructions_ind() );
        attrList.add( TermsBean.Attributes.create_daily_rate_fec_oth_ind() );
        attrList.add( TermsBean.Attributes.create_settle_covered_by_fec_num() );
        attrList.add( TermsBean.Attributes.create_settle_fec_rate() );
        attrList.add( TermsBean.Attributes.create_settle_fec_currency() );
        attrList.add( TermsBean.Attributes.create_settle_other_fec_text() );
        attrList.add( TermsBean.Attributes.create_transferrable() );

        return attrList;
     }










    /**
     *  Performs validation of the attributes of this type of instrument and transaction.   This method is
     *  called from the userValidate() hook in the managed object.
     *
     *  @param terms - the bean being validated
     */
    public void validate(TermsBean terms)
			throws java.rmi.RemoteException, AmsException
     {
 	super.validate(terms);
     }

  //Beginning of code used by the Middleware team
     /**
 	 *  gets the values of the attributes of the managed object. This method is called
 	 *  from the getTermsAttributesDoc() hook in the managed object.
 	 *
 	 *  return com.amsinc.ecsg.util.DocumentHandler
 	 *  @param terms TermsBean - the bean being validated
 	 *  @param termsDoc com.amsinc.ecsg.util.DocumentHandler
 	 */
    public DocumentHandler getManagerTerms(TermsBean terms, DocumentHandler termsDoc) throws java.rmi.RemoteException, AmsException

    {

    	         DocumentHandler termsValuesDoc = terms.populateXmlDoc(new DocumentHandler());
              LOG.debug("This is the termValuesDoc containing all the terms values: {}" , termsValuesDoc.toString());


               //Terms_Details
              if ( StringFunction.isNotBlank(termsValuesDoc.getAttribute("/Terms/our_reference")) ) {
            	  termsDoc.setAttribute("/Terms/TermsDetails/ReferenceNumber",	     termsValuesDoc.getAttribute("/Terms/our_reference"));
              }
               termsDoc.setAttribute("/Terms/TermsDetails/Amount",                   termsValuesDoc.getAttribute("/Terms/amount"));
 			  termsDoc.setAttribute("/Terms/TermsDetails/AmountCurrencyCode",	     termsValuesDoc.getAttribute("/Terms/amount_currency_code"));
 			  termsDoc.setAttribute("/Terms/TermsDetails/WorkItemNumber",	         termsValuesDoc.getAttribute("/Terms/work_item_number"));
 			 termsDoc.setAttribute("/Terms/TermsDetails/Transferrable",		         termsValuesDoc.getAttribute("/Terms/transferrable"));
 			  //End of Terms_Details

 		     //Instructions
 		       termsDoc.setAttribute("/Terms/Instructions/ExportDiscrepancyInstructions",			termsValuesDoc.getAttribute("/Terms/export_discrepancy_instr"));
 			   termsDoc.setAttribute("/Terms/Instructions/InstructionsOther",			            termsValuesDoc.getAttribute("/Terms/instr_other"));
 			 //End of Instructions


 	           //Terms Party

                DocumentHandler termsPartyDoc = new DocumentHandler();

                 //Additional Terms Party
                 String testTP = TermsPartyType.APPLICANT;


              int id = 0;

             termsPartyDoc.setAttribute("/TermsParty/TotalNumberOfEntries","0");

             for (int i = 0; i<TradePortalConstants.MAX_NUM_TERMS_PARTIES;i++)
    			 {
					String termsPartyOID = termsValuesDoc.getAttribute("/Terms/"+TradePortalConstants.TERMS_PARTY_ATTRIBUTE_NAMES[i]);

    		      if(!(termsPartyOID == null || termsPartyOID.equals("")))
    		      {
    				 TermsParty termsParty = (TermsParty)(terms.getComponentHandle(TradePortalConstants.TERMS_PARTY_COMPONENT_NAMES[i]));
    				 if(termsParty.getAttribute("terms_party_type").equals(testTP))
    				 {
    					 LOG.debug("party_Type is APP, hence I need to check for thirdParty");
    					 termsPartyDoc = packageAdditionalTermsParty(terms,id,termsPartyDoc,termsParty);
    					 if(termsPartyDoc.getAttribute("/AdditionalTermsParty").equals("Y"))
    					  id++;
    				 }
    				 termsPartyDoc.setAttribute("/AdditionalTermsParty","N");
    				 //If the termsValuesDoc contains termsParty also need to modify the line below
    			     termsPartyDoc = termsParty.packageTermsPartyAttributes(id,termsPartyDoc);
    			     id++;
    			   }
 		      }

 		  LOG.debug("This is TermsPartyDoc: {} ", termsPartyDoc.toString());

          termsPartyDoc.setAttribute("/TermsParty/TotalNumberOfEntries",String.valueOf(id));
          termsDoc.setComponent("/TermsParty",termsPartyDoc.getFragment("/TermsParty"));


    		return termsDoc;
    }

     //End of getManagerTerms

 }