package com.ams.tradeportal.busobj.termsmgr;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.busobj.ShipmentTerms;
import com.ams.tradeportal.busobj.TermsBean;
import com.ams.tradeportal.busobj.TermsParty;
import com.ams.tradeportal.busobj.util.InstrumentServices;
import com.ams.tradeportal.busobj.util.InvoiceUtility;
import com.ams.tradeportal.busobj.webbean.TermsWebBean;
import com.ams.tradeportal.common.InstrumentType;
import com.ams.tradeportal.common.TermsPartyType;
import com.ams.tradeportal.common.TradePortalConstants;
import com.ams.tradeportal.common.TransactionStatus;
import com.ams.tradeportal.common.TransactionType;
import com.amsinc.ecsg.frame.AmsConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.Attribute;
import com.amsinc.ecsg.frame.AttributeManager;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.util.DateTimeUtility;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.StringFunction;

/**
 * This class manages the dynamic attribute registration and validation
 * for a Terms business object or TermsWebBean for an Issue Loan Request
 *
 *
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class LRQ__ISSTermsMgr extends TermsMgr
 {
private static final Logger LOG = LoggerFactory.getLogger(LRQ__ISSTermsMgr.class);
    /**
     *  Constructor that is used by business objects to create their
     *  manager.  This method also register the dynamic attributes of
     *  the bean.
     *
     *  @param mgr AttributeManager attribute manager of the bean being managed.  Used to register attributes
     */
    public LRQ__ISSTermsMgr(AttributeManager mgr) throws AmsException
     {
	  super(mgr);
     }

    /**
     *  Constructor that is used by web beans to create their
     *  manager.  This method also register the dynamic attributes of
     *  the bean.
     *
     *  @param mgr TermsWebBean - the bean being managed.  Used to register attributes.
     */
    public LRQ__ISSTermsMgr(TermsWebBean terms) throws AmsException
     {
	  super(terms);
     }

    /**
     *  Returns a list of attributes.  These describe each of the attributes
     *  that will be registered for the bean being managed.
     *
     *  This method controls which attributes of TermsBean or TermsWebBean are registered
     *  for an this instrument and transaction type
     *
     *  @return Vector - list of attributes
     */
     public List<Attribute> getAttributesToRegister()
     {
        List<Attribute> attrList = super.getAttributesToRegister();
        attrList.add( TermsBean.Attributes.create_amount() );
        attrList.add( TermsBean.Attributes.create_amount_currency_code() );
        attrList.add( TermsBean.Attributes.create_coms_chrgs_our_account_num() );
        attrList.add( TermsBean.Attributes.create_covered_by_fec_number() );
        attrList.add( TermsBean.Attributes.create_fec_amount() );
        attrList.add( TermsBean.Attributes.create_fec_maturity_date() );
        attrList.add( TermsBean.Attributes.create_fec_rate() );
        attrList.add( TermsBean.Attributes.create_finance_type() );
        attrList.add( TermsBean.Attributes.create_finance_type_text() );
        attrList.add( TermsBean.Attributes.create_in_advance_disposition() );
        attrList.add( TermsBean.Attributes.create_in_arrears_disposition() );
        attrList.add( TermsBean.Attributes.create_interest_debit_acct_num() );
        attrList.add( TermsBean.Attributes.create_interest_to_be_paid() );
        attrList.add( TermsBean.Attributes.create_linked_instrument_id() );
        attrList.add( TermsBean.Attributes.create_linked_instrument_type() );
        attrList.add( TermsBean.Attributes.create_linked_tran_seq_num() );
        attrList.add( TermsBean.Attributes.create_linked_transaction_type() );
        attrList.add( TermsBean.Attributes.create_loan_maturity_debit_acct() );
        attrList.add( TermsBean.Attributes.create_loan_maturity_debit_other_txt() );
        attrList.add( TermsBean.Attributes.create_loan_maturity_debit_type() );
        attrList.add( TermsBean.Attributes.create_loan_proceeds_credit_acct() );
        attrList.add( TermsBean.Attributes.create_loan_proceeds_credit_other_txt() );
        attrList.add( TermsBean.Attributes.create_loan_proceeds_credit_type() );
        attrList.add( TermsBean.Attributes.create_loan_proceeds_pmt_amount() );
        attrList.add( TermsBean.Attributes.create_loan_proceeds_pmt_curr() );
        attrList.add( TermsBean.Attributes.create_loan_start_date() );
        attrList.add( TermsBean.Attributes.create_loan_terms_fixed_maturity_dt() );
        attrList.add( TermsBean.Attributes.create_loan_terms_number_of_days() );
        attrList.add( TermsBean.Attributes.create_loan_terms_type() );
        attrList.add( TermsBean.Attributes.create_maturity_covered_by_fec_number() );
        attrList.add( TermsBean.Attributes.create_maturity_fec_amount() );
        attrList.add( TermsBean.Attributes.create_maturity_fec_maturity_date() );
        attrList.add( TermsBean.Attributes.create_maturity_fec_rate() );
        attrList.add( TermsBean.Attributes.create_maturity_use_fec() );
        attrList.add( TermsBean.Attributes.create_maturity_use_mkt_rate() );
        attrList.add( TermsBean.Attributes.create_maturity_use_other() );
        attrList.add( TermsBean.Attributes.create_maturity_use_other_text() );
        attrList.add( TermsBean.Attributes.create_multiple_related_instruments() );
        attrList.add( TermsBean.Attributes.create_reference_number() );
        attrList.add( TermsBean.Attributes.create_related_instrument_id() );
        attrList.add( TermsBean.Attributes.create_use_fec() );
        attrList.add( TermsBean.Attributes.create_use_mkt_rate() );
        attrList.add( TermsBean.Attributes.create_use_other() );
        attrList.add( TermsBean.Attributes.create_use_other_text() );
        attrList.add( TermsBean.Attributes.create_work_item_priority() );
        attrList.add( TermsBean.Attributes.create_work_item_number() );
        // TLE - 10/25/06 - MHUG101470192 - Add Begin
		attrList.add( TermsBean.Attributes.create_internal_instructions() );
		attrList.add( TermsBean.Attributes.create_special_bank_instructions() );
		// TLE - 10/25/06 - MHUG101470192 - Add End
		attrList.add( TermsBean.Attributes.create_financing_backed_by_buyer_ind() );
		attrList.add( TermsBean.Attributes.create_financed_invoices_details_text() );
		attrList.add( TermsBean.Attributes.create_financed_pos_details_text() );
         //RKAZI - 01/30/2013 - REL8.2 CR-709 - START
		attrList.add( TermsBean.Attributes.create_coms_chrgs_type() );
         //RKAZI - 01/30/2013 - REL8.2 CR-709 - END
		 //Srinivasu_D IR#11999 Rel8.2 start
		attrList.add( TermsBean.Attributes.create_invoice_only_ind() );
		attrList.add( TermsBean.Attributes.create_invoice_due_date() );
		attrList.add( TermsBean.Attributes.create_invoice_details() );
		//Srinivasu_D IR#11999 Rel8.2 End
        attrList.add( TermsBean.Attributes.create_invoice_details() );//RKazi CR 709 Rel8.2

        attrList.add( TermsBean.Attributes.create_estimated_interest_amount() );
        attrList.add( TermsBean.Attributes.create_net_finance_amount() );
        attrList.add( TermsBean.Attributes.create_bank_charges_type() );
        attrList.add( TermsBean.Attributes.create_payment_method() );
        return attrList;
     }



	/**
	 *  Performs validation of the attributes of the managed object.   This method is called
	 *  from the userValidate() hook in the managed object.
	 *
	 *  @param terms TermsBean - the bean being validated
	 */
	public void validate(TermsBean terms)
		throws java.rmi.RemoteException, AmsException {


		super.validate(terms);

		//TLE - 02/19/07 - IR-AYUH020879117 - Add Begin
		//Krishna IR-FRUH110934059 01/13/2007 Begin
		//Incase of authorization setImportIndicator(..) is never called.Incase of Verify it does.
		//So,While authorising ImportIndicator on terms object is an empty string. 
		//But it should be same as it is already stored in DB in the instrument table.
		//For that reason,DB fetch code is wrapped in If(..) condition.
		String importInd = terms.getImportIndicator();
		if(StringFunction.isBlank(importInd))
		{
			//SSikhakolli - 05/06/2014 - Rel 9.0 IR#T36000026319 - SQL fix - Begin
			DocumentHandler docResFinType=null;
			String sqlqryForFinType = "SELECT a.IMPORT_INDICATOR FROM INSTRUMENT a,TRANSACTION b "
			+ "WHERE b.C_CUST_ENTER_TERMS_OID=? "
			+ "AND b.P_INSTRUMENT_OID=a.INSTRUMENT_OID";
			docResFinType = DatabaseQueryBean.getXmlResultSet(sqlqryForFinType, true, 
					new Object[]{terms.getAttribute("terms_oid")});
			//SSikhakolli - 05/06/2014 - Rel 9.0 IR#T36000026319 - SQL fix - End
		
		//IValera - IR# MJUH120561469 - 03 Mar, 2008 - Change Begin
		//The issue was fixed in IR# AYUH020879117 itself, but after Grafton's code review,
		//I have added below two conditions to avoid throwing exception
		//importInd     = docResFinType.getAttribute("/ResultSetRecord(0)/IMPORT_INDICATOR");
		//
		//In case of no rows retrived, the getXmlResultSet will return NULL
		if(docResFinType != null)
		{
			//if getAttribute does not find IMPORT_INDICATOR, it returns NULL
			importInd = docResFinType.getAttribute("/ResultSetRecord(0)/IMPORT_INDICATOR");
		}
		
		//Sets importInd to Blank String if it is NULL
		if(importInd == null)
		{
			importInd = "";
		}
		//IValera - IR# MJUH120561469 - 03 Mar, 2008 - Change End
		
		terms.setImportIndicator(importInd);
		}
                //Krishna IR-FRUH110934059 01/13/2007 End
		String financeType = terms.getImportIndicator();

		if (financeType.equals(TradePortalConstants.INDICATOR_NO)) {
			financeType = "Export";
			terms.registerRequiredAttribute("loan_proceeds_credit_type");
		} else if (financeType.equals(TradePortalConstants.INDICATOR_YES)) {
			financeType = "Import";
			terms.registerRequiredAttribute("loan_proceeds_credit_type");
		} else if (financeType.equals(TradePortalConstants.INDICATOR_X)) {
			financeType = "Receivables";
		//RKAZI CR709 Rel 8.2 01/25/2013 Start
	    } else if (financeType.equals(TradePortalConstants.INDICATOR_T)) {
	    	terms.registerRequiredAttribute("loan_proceeds_credit_type");
    	//RKAZI CR709 Rel 8.2 01/25/2013 End
	    } else {
			// The user didn't select the finance type - Return from
			// here since the system doesn't know whether data entered
			// for export or import

	   		terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
       							TradePortalConstants.LOAN_TYPE_IS_REQUIRED);
			return;
		}
		//TLE - 02/19/07 - IR-AYUH020879117 - Add End

		// The second terms party is the applicant.
		terms.registerRequiredAttribute("c_SecondTermsParty");
		terms.registerRequiredAttribute("loan_start_date");
		terms.registerRequiredAttribute("amount_currency_code");
		terms.registerRequiredAttribute("loan_terms_type");
		terms.registerRequiredAttribute("interest_to_be_paid");
		if(!TradePortalConstants.CREDIT_MULTI_BEN_ACCT.equals(terms.getAttribute("loan_proceeds_credit_type")) && !"Receivables".equals(financeType)){ 
			terms.registerRequiredAttribute("loan_proceeds_pmt_curr");
		}
		terms.registerRequiredAttribute("loan_maturity_debit_type");
		// Nar IR-T36000015971 charges and interest account number is required only if debit charger from loan req or account receivable not selected.
		if((!(TradePortalConstants.DEBIT_LOAN_PROCEEDS.equals(terms.getAttribute("coms_chrgs_type")) || 
				TradePortalConstants.DEBIT_ACCT_REC.equals(terms.getAttribute("coms_chrgs_type"))) && 
				!TradePortalConstants.IN_ADVANCE.equals(terms.getAttribute("interest_to_be_paid")))){
		 terms.registerRequiredAttribute("coms_chrgs_our_account_num");
		 terms.registerRequiredAttribute("interest_debit_acct_num");
		}
		

		validateGeneral(terms);
		validateLoanRequestDetails(terms);

		validatePaymentInstructions(terms);
		validateBankDefined(terms);
                //Krishna IR PEUH112772586 12/06/2007 Begin
                validateParties(terms);
                //Krishna IR PEUH112772586 12/06/2007 End

	}

	/**
	 * Performs validation for the general section.
	 *
	 * @param terms com.ams.tradeportal.busobj.TermsBean
	 */
	 public void validateGeneral(TermsBean terms) throws java.rmi.RemoteException, AmsException
	{
		// pcutrone - 10/17/2007 - REUH101146143 - Check to make sure the amount field is not set to zero.
		String value;
		
		value = terms.getAttribute("amount");
		if (StringFunction.isNotBlank(value)) {
			if (Double.parseDouble(value) == 0) {
				terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.AMOUNT_IS_ZERO);
			}
		}
		// pcutrone - 10/17/2007 - REUH101146143 - END

	   // Release By Bank On date must be a valid date.  The required edit
	   // logic guarantees it has a value.  The setAttribute logic
	   // guarantees it is a valid date.  No logic required here.

	}

	/**
	 * Performs validation for the loan request details section.
	 *
	 * @param terms com.ams.tradeportal.busobj.TermsBean
	 */
	 public void validateLoanRequestDetails(TermsBean terms) throws java.rmi.RemoteException, AmsException
	{
	   String value;;
	   String startDate;
	   
	   // Loan Terms Type Radio Buttons
	   value = terms.getAttribute("loan_terms_type");
	   if (value.equals(TradePortalConstants.LOAN_FIXED_MATURITY)) {
			value = terms.getAttribute("loan_terms_fixed_maturity_dt");
			//Suresh Penke 08/08/2011 IR-VVUJ051144191 Added else if condition
			startDate = terms.getAttribute("loan_start_date");
			if (StringFunction.isBlank(value)) {
				terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
									TradePortalConstants.FIXED_MATURITY_DATE_REQUIRED);
			} else if (StringFunction.isNotBlank(value) && StringFunction.isNotBlank(startDate)) {
				GregorianCalendar gcStartDate = new GregorianCalendar();
				GregorianCalendar maturityDate = new GregorianCalendar();

				gcStartDate.setTime(DateTimeUtility.convertStringDateTimeToDate(startDate));
				maturityDate.setTime(DateTimeUtility.convertStringDateTimeToDate(value));
				// If the start date is after the maturity date, give error
				//Shilpa R-VVUJ051144191 - Also if the start date is equal to the maturity date, give error
				if ((gcStartDate.after(maturityDate)) || (gcStartDate.equals(maturityDate))) {
					terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
							TradePortalConstants.INVALID_DATE_MATURITY_DATE);
					}
			 }
			//Suresh Penke 08/08/2011 IR-VVUJ051144191 End
			// Blank out all fields associated with LOAN_TERMS_TYPE radio button.
			terms.setAttribute("loan_terms_number_of_days", "");

		} else if (value.equals(TradePortalConstants.LOAN_DAYS_AFTER)) {
			value = terms.getAttribute("loan_terms_number_of_days");
			if (StringFunction.isBlank(value)) {
				terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
									TradePortalConstants.LOAN_TERMS_NUM_DAYS_REQD);
			}
			// Blank out all fields associated with LOAN_TERMS_TYPE radio button.
			terms.setAttribute("loan_terms_fixed_maturity_dt", "");
		}

	   // TLE - 11/09/06 - IR#ACUG110353108 - Add Begin
	   //Make sure that the finance type is required for export loan
	   value = terms.getImportIndicator();
	
		// validate tradeloan requirements
	   if (value.equals(TradePortalConstants.INDICATOR_NO) && StringFunction.isBlank(terms.getAttribute("finance_type"))){
 		   		terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
	       							TradePortalConstants.FINANCE_TYPE_REQUIRED);
	   }

	   if (value.equals(TradePortalConstants.INDICATOR_X) && StringFunction.isBlank(terms.getAttribute("finance_type"))){
	   		terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
     							TradePortalConstants.INVF_FINANCE_TYPE_REQUIRED);
	   }

	}
	 

	/**
	 * Performs validation for the payment instructions section.
	 *
	 * @param terms com.ams.tradeportal.busobj.TermsBean
	 */
	 public void validatePaymentInstructions(TermsBean terms) throws java.rmi.RemoteException, AmsException
	{
	   String value, valueA, valueB;
	   String errorVal1, errorVal2;
	   boolean loanAmountBlank, loanPaymentBlank;
	   boolean relatedInstrumentIDBlank, multipleRelatedInstrumentsBlank;

	   // Make sure that amount of loan and amount of payment are not both blank.
	   valueA = terms.getAttribute("amount");
	   valueB = terms.getAttribute("loan_proceeds_pmt_amount");
	   loanAmountBlank = StringFunction.isBlank(valueA);
	   loanPaymentBlank = StringFunction.isBlank(valueB);
		if ((loanAmountBlank && loanPaymentBlank) || (isCustNotIntgWithTPS(terms) && loanAmountBlank && TradePortalConstants.INDICATOR_X.equalsIgnoreCase(terms.getImportIndicator()))) {
		   
		   if ( isCustNotIntgWithTPS(terms) ) { // Rel-935 display different error message for 'TPS not integrated customer'
			   terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.LOAN_AMOUNT_REQD);
		   } else {
			errorVal1 = terms.getResourceManager().getText( "LoanRequest.AmountIfKnown",
															TradePortalConstants.TEXT_BUNDLE);
			errorVal2 = terms.getResourceManager().getText( "LoanRequest.LoanProceedsAmount",
															TradePortalConstants.TEXT_BUNDLE);
			terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
									TradePortalConstants.LOAN_PAYMENT_AMOUNT_REQD,
									errorVal1, errorVal2);
		   }
	   }

		// pcutrone - 10/17/2007 - REUH101146143 - Check to make sure the amount field is not set to zero.
		value = terms.getAttribute("loan_proceeds_pmt_amount");
		if (StringFunction.isNotBlank(value)) {
			if (Double.parseDouble(value) == 0) {
				errorVal1 = terms.getResourceManager().getText( "LoanRequest.AmountIfKnown",
						TradePortalConstants.TEXT_BUNDLE);
				errorVal2 = terms.getResourceManager().getText( "LoanRequest.LoanProceedsAmount",
						TradePortalConstants.TEXT_BUNDLE);
				terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.LOAN_PAYMENT_AMOUNT_REQD,
						errorVal1, errorVal2);
			}
		}
		// pcutrone - 10/17/2007 - REUH101146143 - END
		
	   ///////////////////////////////////////////////////
	   // Check the loan proceeds radio buttons
	   ///////////////////////////////////////////////////
	   value = terms.getAttribute("loan_proceeds_credit_type");
	   // CREDIT_REL_INST
	   if (value.equals(TradePortalConstants.CREDIT_REL_INST)) {
			valueA = terms.getAttribute("related_instrument_id");
			valueB = terms.getAttribute("multiple_related_instruments");
			relatedInstrumentIDBlank = StringFunction.isBlank(valueA);
			multipleRelatedInstrumentsBlank = StringFunction.isBlank(valueB);
			if (relatedInstrumentIDBlank && multipleRelatedInstrumentsBlank) {
				errorVal1 = terms.getResourceManager().getText( "LoanRequest.RelatedInstrumentID",
																TradePortalConstants.TEXT_BUNDLE);
				errorVal2 = terms.getResourceManager().getText( "LoanRequest.LoanProceeds",
																TradePortalConstants.TEXT_BUNDLE);
				terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
										TradePortalConstants.NOT_INDICATED_UNDER,
										errorVal1, errorVal2);
			} else if (!relatedInstrumentIDBlank && !multipleRelatedInstrumentsBlank) {
				terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
									TradePortalConstants.NOT_MULTI_AND_RELATED_INST_ID);
			}


			//TLE - 04/03/07 - IR-AYUH031545048- Add Begin
			// Validate the Instrument ID for import Loan
			if (StringFunction.isNotBlank(valueA)) {
               validateRelatedInstrumentID(terms, valueA);
			}
			//TLE - 04/03/07 - IR-AYUH031545048- Add End
		}
		else {
			// Blank out all fields associated with CREDIT_REL_INST radio button.
			terms.setAttribute("related_instrument_id", "");
			terms.setAttribute("multiple_related_instruments", "");
		}

		// CREDIT_OUR_ACCT
		if (value.equals(TradePortalConstants.CREDIT_OUR_ACCT)) {
			value = terms.getAttribute("loan_proceeds_credit_acct");
			if (StringFunction.isBlank(value)) {
				errorVal1 = terms.getResourceManager().getText( "LoanRequest.AccountNumber",
																TradePortalConstants.TEXT_BUNDLE);
				errorVal2 = terms.getResourceManager().getText( "LoanRequest.LoanProceeds",
																TradePortalConstants.TEXT_BUNDLE);
				terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
										TradePortalConstants.NOT_INDICATED_UNDER,
										errorVal1, errorVal2);
			}
			String fintype = terms.getAttribute("finance_type");
			//IR 31463- Charges is not required for Receivables Trade Loan			
			boolean isTradeLoan = ( TradePortalConstants.TRADE_LOAN_PAY.equals(fintype) );
			value = terms.getAttribute("bank_charges_type");
			if (StringFunction.isBlank(value) && isTradeLoan){
				errorVal1 = terms.getResourceManager().getText( "TermsBeanAlias.collection_charges_type",
						TradePortalConstants.TEXT_BUNDLE);
				terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
						 AmsConstants.REQUIRED_ATTRIBUTE,
						 errorVal1);

			}
		}
		else {
			// Blank out all fields associated with CREDIT_OUR_ACCT radio button.
			terms.setAttribute("loan_proceeds_credit_acct", "");
		}
		// W Zhu 2/6/07 AYUH020554087 Make sure to finish checking credit type
		// before checking linked instrument so that the variable "value" is not reset.
		// CREDIT_BEN_ACCT
		if (value.equals(TradePortalConstants.CREDIT_BEN_ACCT)) {

			this.editRequiredBeneficiaryValue(terms, "FirstTermsParty");
           // TLE - 11/20/06 - IR#ANUG110757179 - Add Begin
           // Only Beneficiary info is required - Not account number
			//// The beneficiary must also have a non-blank account number
			/* KMEhta IR T36000022557 on 15/1/2014 Start */ 
			//SSikhakolli - Rel 9.2 IR#T36000034828 12/25/2014 - Null check for c_FirstTermsParty - Begin
			if(StringFunction.isNotBlank(terms.getAttribute("c_FirstTermsParty"))){
				TermsParty termsParty = (TermsParty) terms.getComponentHandle("FirstTermsParty");
	
				value = termsParty.getAttribute("acct_num");
				if (StringFunction.isBlank(value)) {
					issueTermsPartyError(termsParty, terms, "LoanRequest.Beneficiary",
								   "acct_num");
				}
			}
			//SSikhakolli - Rel 9.2 IR#T36000034828 12/25/2014 - Null check for c_FirstTermsParty - End
			/* KMEhta IR T36000022557 on 15/1/2014 End */ 
           // TLE - 11/20/06 - IR#ANUG110757179 - Add End
			String fintype = terms.getAttribute("finance_type");
			boolean isTradeLoan = (TradePortalConstants.TRADE_LOAN_REC.equals(fintype) || TradePortalConstants.TRADE_LOAN_PAY.equals(fintype) );
			value = terms.getAttribute("bank_charges_type");
			if (StringFunction.isBlank(value) && (isTradeLoan)){
				errorVal1 = terms.getResourceManager().getText( "TermsBeanAlias.collection_charges_type",
						TradePortalConstants.TEXT_BUNDLE);
				terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
						 AmsConstants.REQUIRED_ATTRIBUTE,
						 errorVal1);

			}
			
			value = terms.getAttribute("payment_method");
			if (StringFunction.isBlank(value) && isTradeLoan){
				errorVal1 = terms.getResourceManager().getText( "PaymentMethodValidationBeanAlias.payment_method",
						TradePortalConstants.TEXT_BUNDLE);
				terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
						 AmsConstants.REQUIRED_ATTRIBUTE,
						 errorVal1);

			}

		} else {
			value = terms.getAttribute("finance_type");
			if (value.equals(TradePortalConstants.BUYER_REQUESTED_FINANCING)) {
				//Krishna DAUI031878508 Begin
				this.editRequiredBeneficiaryValue(terms, "FirstTermsParty", "LoanRequest.Seller");
                //Krishna DAUI031878508 End
			} else {	
				// Blank out all fields associated with CREDIT_BEN_ACCT
				terms.setAttribute("c_FirstTermsParty", "");
				terms.setAttribute("c_ThirdTermsParty", "");				
			}
		}
		//CREDIT_OTHER_ACCT
		//Suresh Penke IR-NEUK121055005 08/08/2011 Begin
		value = terms.getAttribute("loan_proceeds_credit_type");
		//IR-NEUK121055005 08/08/2011 End
		if (value.equals(TradePortalConstants.CREDIT_OTHER_ACCT)) {
			value = terms.getAttribute("loan_proceeds_credit_other_txt");
			if (StringFunction.isBlank(value)) {
				errorVal1 = terms.getResourceManager().getText( "LoanRequest.Other1",
																TradePortalConstants.TEXT_BUNDLE);
				errorVal2 = terms.getResourceManager().getText( "LoanRequest.LoanProceeds",
																TradePortalConstants.TEXT_BUNDLE);
				terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
										TradePortalConstants.NOT_INDICATED_UNDER,
										errorVal1, errorVal2);
			}
		} else {
			// Blank out all fields associated with CREDIT_OTHER_ACCT
			terms.setAttribute("loan_proceeds_credit_other_txt", "");
		}


		///////////////////////////////////////////////////
		// Check linked instrument exists
		//////////////////////////////////////////////////
		//TLE - 01/19/07 - IR-KNUG122039470 - Add Begin
	    String relInstrID = terms.getAttribute("linked_instrument_id");

	    //Get Import indicator
	    value = terms.getImportIndicator();
	    if (StringFunction.isBlank(value) || value.equals(TradePortalConstants.INDICATOR_NO)) {
		  if (StringFunction.isNotBlank(relInstrID)) {

            // Moved code to validate related instrument id for export loan
            // into a function so that it can be called from different places
			validateRelatedInstrumentID(terms, relInstrID);
            //TLE - 04/03/07 - IR-AYUH031545048- Add End

		  }
	    }
	    //TLE - 01/19/07 - IR-KNUG122039470 - Add End




		// Exhange Rate Details (PROCEEDS)
		value = terms.getAttribute("use_fec");
		if (value.equals(TradePortalConstants.INDICATOR_YES)) {
			// Check if covered by fec number is blank
			value = terms.getAttribute("covered_by_fec_number");
			if (StringFunction.isBlank(value)) {
				errorVal1 = terms.getResourceManager().getText( "LoanRequest.FECCovered",
																TradePortalConstants.TEXT_BUNDLE);
				errorVal2 = terms.getResourceManager().getText( "LoanRequest.LoanProceeds",
																TradePortalConstants.TEXT_BUNDLE);
				terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
										TradePortalConstants.NOT_INDICATED_UNDER,
										errorVal1, errorVal2);
			}
			// check if amount of fec is blank
			value = terms.getAttribute("fec_amount");
			if (StringFunction.isBlank(value)) {
				errorVal1 = terms.getResourceManager().getText( "LoanRequest.FECAmount",
																TradePortalConstants.TEXT_BUNDLE);
				errorVal2 = terms.getResourceManager().getText( "LoanRequest.LoanProceeds",
																TradePortalConstants.TEXT_BUNDLE);
				terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
										TradePortalConstants.NOT_INDICATED_UNDER,
										errorVal1, errorVal2);
			}
			// check if rate of fec is blank
			value = terms.getAttribute("fec_rate");
			if (StringFunction.isBlank(value)) {
				errorVal1 = terms.getResourceManager().getText( "LoanRequest.FECRate",
																TradePortalConstants.TEXT_BUNDLE);
				errorVal2 = terms.getResourceManager().getText( "LoanRequest.LoanProceeds",
																TradePortalConstants.TEXT_BUNDLE);
				terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
										TradePortalConstants.NOT_INDICATED_UNDER,
										errorVal1, errorVal2);
			}
			// check if maturity date is blank
			value = terms.getAttribute("fec_maturity_date");
			if (StringFunction.isBlank(value)) {
				errorVal1 = terms.getResourceManager().getText( "LoanRequest.FECMaturity",
																TradePortalConstants.TEXT_BUNDLE);
				errorVal2 = terms.getResourceManager().getText( "LoanRequest.LoanProceeds",
																TradePortalConstants.TEXT_BUNDLE);
				terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
										TradePortalConstants.NOT_INDICATED_UNDER,
										errorVal1, errorVal2);
			}
		} else {
			// Blank out fields
			terms.setAttribute("covered_by_fec_number", "");
			terms.setAttribute("fec_amount", "");
			terms.setAttribute("fec_rate", "");
			terms.setAttribute("fec_maturity_date", "");
		}

		value = terms.getAttribute("use_other");
		if (value.equals(TradePortalConstants.INDICATOR_YES)) {
			// Check if covered by use other text is blank
			value = terms.getAttribute("use_other_text");
			if (StringFunction.isBlank(value)) {
				errorVal1 = terms.getResourceManager().getText( "LoanRequest.Other2",
																TradePortalConstants.TEXT_BUNDLE);
				errorVal2 = terms.getResourceManager().getText( "LoanRequest.LoanProceeds",
																TradePortalConstants.TEXT_BUNDLE);
				terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
										TradePortalConstants.NOT_INDICATED_UNDER,
										errorVal1, errorVal2);
			}
		} else {
			// Blank out field
			terms.setAttribute("use_other_text", "");
		}

		// Check the loan maturity debit type radio buttons
		value = terms.getAttribute("loan_maturity_debit_type");

		// DEBIT_APPt
		if (value.equals(TradePortalConstants.DEBIT_APP)) {
			// TLE - 11/09/06 - IR#ANUG110757179 - Add Begin
			//Since loan maturity debit account is only available for Import loan,
			//check to see if this is an import loan
			
			// fix IR T36000016236, loan maturity debit account mandatory for all loan types excluding export loan 
			// TLE - 11/09/06 - IR#ANUG110757179 - Add End
				value = terms.getAttribute("loan_maturity_debit_acct");
				if (StringFunction.isBlank(value)) {
					errorVal1 = terms.getResourceManager().getText( "LoanRequest.AccountNumber",
																	TradePortalConstants.TEXT_BUNDLE);
					errorVal2 = terms.getResourceManager().getText( "LoanRequest.LoanMaturity",
																	TradePortalConstants.TEXT_BUNDLE);
					terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
											TradePortalConstants.NOT_INDICATED_UNDER,
											errorVal1, errorVal2);
			    }
			//} end IR T36000016236
		} else {
			// Blank out field
			terms.setAttribute("loan_maturity_debit_acct", "");
		}
		
		
		// DEBIT_OTHER
		if (value.equals(TradePortalConstants.DEBIT_OTHER)) {
			value = terms.getAttribute("loan_maturity_debit_other_txt");
			if (StringFunction.isBlank(value)) {
				errorVal1 = terms.getResourceManager().getText( "LoanRequest.LoanMaturityOther",
																TradePortalConstants.TEXT_BUNDLE);
				errorVal2 = terms.getResourceManager().getText( "LoanRequest.LoanMaturity",
																TradePortalConstants.TEXT_BUNDLE);
				terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
										TradePortalConstants.NOT_INDICATED_UNDER,
										errorVal1, errorVal2);
			}
		} else {
			// Blank out field
			terms.setAttribute("loan_maturity_debit_other_txt", "");
		}
 		// Exhange Rate Details (MATURITY)
		value = terms.getAttribute("maturity_use_fec");
		if (value.equals(TradePortalConstants.INDICATOR_YES)) {
			// Check if maturity covered by fec number is blank
			value = terms.getAttribute("maturity_covered_by_fec_number");
			if (StringFunction.isBlank(value)) {
				errorVal1 = terms.getResourceManager().getText( "LoanRequest.FECCovered",
																TradePortalConstants.TEXT_BUNDLE);
				errorVal2 = terms.getResourceManager().getText( "LoanRequest.LoanMaturity",
																TradePortalConstants.TEXT_BUNDLE);
				terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
										TradePortalConstants.NOT_INDICATED_UNDER,
										errorVal1, errorVal2);
			}
			// check if maturity amount of fec is blank
			value = terms.getAttribute("maturity_fec_amount");
			if (StringFunction.isBlank(value)) {
				errorVal1 = terms.getResourceManager().getText( "LoanRequest.FECAmount",
																TradePortalConstants.TEXT_BUNDLE);
				errorVal2 = terms.getResourceManager().getText( "LoanRequest.LoanMaturity",
																TradePortalConstants.TEXT_BUNDLE);
				terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
										TradePortalConstants.NOT_INDICATED_UNDER,
										errorVal1, errorVal2);
			}
			// check if maturity rate of fec is blank
			value = terms.getAttribute("maturity_fec_rate");
			if (StringFunction.isBlank(value)) {
				errorVal1 = terms.getResourceManager().getText( "LoanRequest.FECRate",
																TradePortalConstants.TEXT_BUNDLE);
				errorVal2 = terms.getResourceManager().getText( "LoanRequest.LoanMaturity",
																TradePortalConstants.TEXT_BUNDLE);
				terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
										TradePortalConstants.NOT_INDICATED_UNDER,
										errorVal1, errorVal2);
			}
			// check if maturity fec maturity date is blank
			value = terms.getAttribute("maturity_fec_maturity_date");
			if (StringFunction.isBlank(value)) {
				errorVal1 = terms.getResourceManager().getText( "LoanRequest.FECMaturity",
																TradePortalConstants.TEXT_BUNDLE);
				errorVal2 = terms.getResourceManager().getText( "LoanRequest.LoanMaturity",
																TradePortalConstants.TEXT_BUNDLE);
				terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
										TradePortalConstants.NOT_INDICATED_UNDER,
										errorVal1, errorVal2);
			}
		} else {
			// Blank out fields
			terms.setAttribute("maturity_covered_by_fec_number", "");
			terms.setAttribute("maturity_fec_amount", "");
			terms.setAttribute("maturity_fec_rate", "");
			terms.setAttribute("maturity_fec_maturity_date", "");
		}

		value = terms.getAttribute("maturity_use_other");
		if (value.equals(TradePortalConstants.INDICATOR_YES)) {
			// Check if maturity use other text is blank
			value = terms.getAttribute("maturity_use_other_text");
			if (StringFunction.isBlank(value)) {
				errorVal1 = terms.getResourceManager().getText( "LoanRequest.Other2",
																TradePortalConstants.TEXT_BUNDLE);
				errorVal2 = terms.getResourceManager().getText( "LoanRequest.LoanMaturity",
																TradePortalConstants.TEXT_BUNDLE);
				terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
										TradePortalConstants.NOT_INDICATED_UNDER,
										errorVal1, errorVal2);
			}
		} else {
			// Blank out field
			terms.setAttribute("maturity_use_other_text", "");
		}

	}


	//TLE - 04/03/07 - IR-AYUH031545048- Add Begin
	/**
	 * Performs validation for related instrument ID.
	 *
	 * @param instrumentID com.ams.tradeportal.busobj.String
	 * @param terms TermsBean
	 */
	 public void validateRelatedInstrumentID(TermsBean terms, String instrumentID) throws java.rmi.RemoteException, AmsException
	{
		String transTermsOid;
		String importInd;
		String financeType;
		String sqlQuery  = null;
		DocumentHandler  relInstrDoc = null;
		String sqlQuery2   = null;
		DocumentHandler  returnedDoc = null;

		transTermsOid = terms.getAttribute("terms_oid");

        // TLE - 06/05/07 - IR-PNUH041332326 - Add Begin
        // Removed existing code and rewrote the logic to validate currency for related instrument

        // Get currency code of the Loan Request instrument
        String loanReqCurrencyCode = terms.getAttribute("amount_currency_code");

        //(1) Build an SQL to check to see if the related instrument id exists
        //    for the Corporate customer
		//SSikhakolli - 05/06/2014 - Rel 9.0 IR#T36000026319 - SQL fix - Begin
		sqlQuery = "select d.transaction_type_code, a.instrument_status, d.transaction_status, "
				+ " a.a_corp_org_oid as REL_INSTRUMENT_CORP_OID, d.copy_of_currency_code,b.a_corp_org_oid as NEW_INSTRUMENT_CORP_OID "
				+ " from instrument a, instrument b, transaction c, transaction d"
				+ " where a.a_op_bank_org_oid = b.a_op_bank_org_oid"
				+ " and a.original_transaction_oid = d.transaction_oid"
				+ " and b.original_transaction_oid = c.transaction_oid"
				+ " and c.c_cust_enter_terms_oid = ? "
				+ " and a.complete_instrument_id = ?";

		relInstrDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery, true, transTermsOid, instrumentID);
		//SSikhakolli - 05/06/2014 - Rel 9.0 IR#T36000026319 - SQL fix - End
		
		if (relInstrDoc == null)
		{
		  terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
		  								 TradePortalConstants.REL_INSTRUMENT_ID_NOT_FOUND_FOR_BANK_ORG);
		  return;
		}
		else {
		   String corpOrgOidNewInstr   = relInstrDoc.getAttribute("/ResultSetRecord(0)/REL_INSTRUMENT_CORP_OID");
		   String corpOrgOidRelInstr   = relInstrDoc.getAttribute("/ResultSetRecord(0)/NEW_INSTRUMENT_CORP_OID");
		   String relInstrCurrencyCode = relInstrDoc.getAttribute("/ResultSetRecord(0)/COPY_OF_CURRENCY_CODE");

		   //TLE - 07/10/07 - IR-POUH060841116 - Add Begin
		   String transactionStatus = relInstrDoc.getAttribute("/ResultSetRecord(0)/TRANSACTION_STATUS");
		   //TLE - 07/10/07 - IR-POUH060841116 - Add End

           if (!corpOrgOidNewInstr.equals(corpOrgOidRelInstr)) {
		  	  terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
											 TradePortalConstants.REL_INSTRUMENT_ID_NOT_FOUND_FOR_CORP_CUS);
              return;
           }

		   //TLE - 06/14/07 - IR-POUH060841116 - Add Begin
		   String relInstrStatus = relInstrDoc.getAttribute("/ResultSetRecord(0)/INSTRUMENT_STATUS");

           //TLE - 07/10/07 - IR-POUH060841116 - Add Begin
		   if (!relInstrStatus.equals(TradePortalConstants.INSTR_STATUS_ACTIVE) && !transactionStatus.equals(TransactionStatus.AUTHORIZED)){
		   //TLE - 07/10/07 - IR-POUH060841116 - Add End
		  	  terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
											 TradePortalConstants.REL_INSTRUMENT_ID_NOT_ACTIVE);
              return;
		   }
		   //TLE - 06/14/07 - IR-POUH060841116 - Add End

 	       String transactionTypeCode = relInstrDoc.getAttribute("/ResultSetRecord(0)/TRANSACTION_TYPE_CODE");

           if (transactionTypeCode.equals(TransactionType.TRANSFER)) {
		  	  terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
											 TradePortalConstants.LOAN_REQ_CANNOT_RELATE_TRANSFER);
              return;
           }

           // If the currency of the related instrument is not same as the currency of the
           // loan request then display an error message to inform the user
           if (!loanReqCurrencyCode.equals(relInstrCurrencyCode)){
		  	  terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
											 TradePortalConstants.LOAN_REQ_WITH_DIFF_CURRENCY_CODE);
              return;
		   }

		}
		
		//(2) Check to see if the related instrument id is the correct instrument type for
		//    for option that the user chooses
	    importInd = terms.getImportIndicator();
	    if (StringFunction.isBlank(importInd) || importInd.equals(TradePortalConstants.INDICATOR_NO)) {
            // Export Loan

            financeType = terms.getAttribute("finance_type");

            if (financeType.equals(InstrumentType.EXPORT_DLC)) {
            	//SSikhakolli - 05/06/2014 - Rel 9.0 IR#T36000026319 - SQL fix - Begin
				sqlQuery2 = "select instrument_oid from instrument"
						+ " where complete_instrument_id = ? "
						+ " and instrument_type_code = ?";

				returnedDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery2, true, instrumentID, InstrumentType.EXPORT_DLC);

				if (returnedDoc == null)
				{
				  terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
													 TradePortalConstants.INVALID_REL_INSTR_EXPORT_LC);
				}
		    } else {
				sqlQuery2 = "select instrument_oid from instrument"
						+ " where complete_instrument_id = ? "
						+ " and instrument_type_code = ?";

				returnedDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery2, true, instrumentID, InstrumentType.EXPORT_COL);
				//SSikhakolli - 05/06/2014 - Rel 9.0 IR#T36000026319 - SQL fix - End

				if (returnedDoc == null)
				{
				  terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
													 TradePortalConstants.INVALID_REL_INSTR_EXPORT_COL);
				}
	        }
        } else {
        	//SSikhakolli - 05/06/2014 - Rel 9.0 IR#T36000026319 - SQL fix - Begin
            // Import Loan
			sqlQuery2 = "select instrument_oid from instrument"
					+ " where complete_instrument_id = ? "
					+ " and instrument_type_code in ('"
					+ InstrumentType.IMPORT_DLC + "','"
					+ InstrumentType.SHIP_GUAR + "','"
					+ InstrumentType.IMPORT_COL + "','"
					+ InstrumentType.APPROVAL_TO_PAY + "','"
					+ InstrumentType.AIR_WAYBILL + "')";

		    returnedDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery2, true, new Object[]{instrumentID});
		    //SSikhakolli - 05/06/2014 - Rel 9.0 IR#T36000026319 - SQL fix - End

			if (returnedDoc == null)
			{
			  terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
												 TradePortalConstants.INVALID_REL_INSTR_IMPORT);
			}
        }
	}
	//TLE - 04/03/07 - IR-AYUH031545048- Add End


	/**
	 * Performs validation for the bank defined section.
	 *
	 * @param terms com.ams.tradeportal.busobj.TermsBean
	 */
	 public void validateBankDefined(TermsBean terms) throws java.rmi.RemoteException, AmsException
	{
	  
	   // Release By Bank On date must be a valid date.  The required edit
	   // logic guarantees it has a value.  The setAttribute logic
	   // guarantees it is a valid date.  No logic required here.

	}
	//Krishna IR PEUH112772586 12/06/2007 Begin
	/**
	 * Performs validation for the Terms parties.
	 *
	 * @param terms com.ams.tradeportal.busobj.TermsBean
	 */
	 public void validateParties(TermsBean terms) throws java.rmi.RemoteException, AmsException
	 {
			String firstTermsParty=null;
			String thirdTermsParty=null;
			String financeType=null;
			firstTermsParty = terms.getAttribute("c_FirstTermsParty");
			thirdTermsParty = terms.getAttribute("c_ThirdTermsParty");
			financeType     = terms.getAttribute("finance_type");
            //Krishna IR-DAUI031878508 Begin
			String importInd = terms.getImportIndicator();
            
			//Validations of parties incase of Import or Export Loan
			if(!importInd.equals(TradePortalConstants.INDICATOR_X))
			{
				//Incase of Import or Export Loan  if Beneficiary Bank- fields are filled without Beneficiary Bank-fields
				//being filled the Error should be thrown like "Beneficiary's Bank can only be entered if a 'Beneficiary' is entered".
				if(StringFunction.isBlank(firstTermsParty) && 
						(StringFunction.isNotBlank(thirdTermsParty)))
				{
					terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
							TradePortalConstants.BEN_BANK_REQD_IF_BEN);
				}
				//Incase of Import or Export Loan  if some fields from the "Beneficiary Bank" are not filled and some are filled
				//errors should be shown to the user about the required Beneficiary Bank-fields.
				if(StringFunction.isNotBlank(firstTermsParty) && 
						(StringFunction.isNotBlank(thirdTermsParty)))
				{
					this.editRequiredBeneficiaryValue(terms, "ThirdTermsParty", "LoanRequest.BankBeneficiary");
				}	
			}
			//Validations of parties incase of Invoice Financing Loan
			else if(importInd.equals(TradePortalConstants.INDICATOR_X))
			{
				//Incase of Invoice Financing Loan if Seller Bank- fields are filled without Seller Bank-fields
				//being filled the Error should be thrown like "Seller's Bank can only be entered if a 'Seller' is entered".
				if(StringFunction.isBlank(firstTermsParty) && 
						(StringFunction.isNotBlank(thirdTermsParty)))
				{
					terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
							TradePortalConstants.SEL_BANK_REQD_IF_SEL);
				}
				//Incase of Invoice Financing Loan  if some fields from the "Seller Bank" are not filled and some are filled
				//errors should be shown to the user about the required Seller Bank-fields.
				if(StringFunction.isNotBlank(firstTermsParty) && 
						(StringFunction.isNotBlank(thirdTermsParty)))
				{
					this.editRequiredBeneficiaryValue(terms, "ThirdTermsParty", "LoanRequest.SellerBank");
				}
			}
			//Krishna IR-DAUI031878508 End
			//Seller is required if 'Buyer-Requested Finacing for Seller' is selected
			if(StringFunction.isNotBlank(financeType)   
				&& financeType.equals(TradePortalConstants.BUYER_REQUESTED_FINANCING)
				&& StringFunction.isBlank(firstTermsParty)){
			    terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.SEL_REQD_IF_BUY_FIN_SEL);
	        }
			
			if (StringFunction.isNotBlank(financeType) && (TradePortalConstants.TRADE_LOAN_PAY.equals(financeType))
					&& TradePortalConstants.CREDIT_BEN_ACCT.equals(terms.getAttribute("loan_proceeds_credit_type"))){
				this.validateBankInformation(terms, "ThirdTermsParty");
				this.validatePaymentMethod(terms, "ThirdTermsParty", "LoanRequest.SellerBank");
				if (this.validateSamePaymentMethod(terms)){
					terms.getErrorManager().issueError(
							TradePortalConstants.ERR_CAT_1,
							TradePortalConstants.DIFFERENT_INVOICE_PAYMENTMETHOD);
				}
				this.validateReportingCodes(terms,"FirstTermsParty");//CR-1001 Rel9.4 Adding validations for Reporting codes

			}
					 
	 }
	 
	private boolean validateSamePaymentMethod(TermsBean terms) throws AmsException, RemoteException {
		//SSikhakolli - 05/06/2014 - Rel 9.0 IR#T36000026319 - SQL fix - Begin
		String samePayMethodSQL = "SELECT  CASE    WHEN NULLPAYMETHODCOUNT > 0  "
				+ "   AND payMethodCount      = 0    THEN 0    WHEN NULLPAYMETHODCOUNT > 0    "
				+ "AND payMethodCount      != 0    THEN 1    WHEN NULLPAYMETHODCOUNT = 0    AND "
				+ "payMethodCount      = 1    THEN 0    WHEN NULLPAYMETHODCOUNT = 0    AND "
				+ "payMethodCount      > 1    THEN 1    ELSE      0  END  PAYMETHODCOUNT "
				+ "FROM  (SELECT SUM(    CASE      WHEN NVL(pay_method,1) = '1'      THEN 1      ELSE 0    END) NULLPAYMETHODCOUNT,    "
				+ "COUNT(DISTINCT(pay_method)) payMethodCount from invoices_summary_data where"
				+ "  (a_transaction_oid in (select transaction_oid from transaction where c_cust_enter_terms_oid = ? ))) ";
		
		DocumentHandler resultDoc = DatabaseQueryBean.getXmlResultSet(samePayMethodSQL, true, 
				new Object[]{terms.getAttribute("terms_oid")});
		//SSikhakolli - 05/06/2014 - Rel 9.0 IR#T36000026319 - SQL fix - End
		if (resultDoc == null){
			return false;
		}
		int currCount = resultDoc.getAttributeInt("/ResultSetRecord(0)/PAYMETHODCOUNT");
		
		return (currCount > 0);
	}

        //Krishna IR PEUH112772586 12/06/2007 End
		private boolean validatePaymentMethod(TermsBean terms, String termsPartyName, String partyResource) throws RemoteException, AmsException {

			String paymentMethod = terms.getAttribute("payment_method");
			String currency = terms.getAttribute("amount_currency_code");
			boolean isValid = true;
			
			String country = InvoiceUtility.fetchOpOrgCountry(terms.getAttribute("terms_oid"));
			TermsParty benParty = null;
			if (terms.doesTermsPartyComponentExist("FirstTermsParty"))
				benParty = (TermsParty) terms.getComponentHandle("FirstTermsParty");
			String benCountry = null;
			if (benParty != null){
				benCountry = benParty.getAttribute("address_country");
			}
			
			isValid = InvoiceUtility.validatePaymentMethod(paymentMethod, currency, country, benCountry, terms.getErrorManager(), false, "");

			return isValid;
		}

		private boolean validateBankInformation(TermsBean terms, String termsPartyName) throws RemoteException, AmsException {
			
			String paymentMethod = terms.getAttribute("payment_method");

			TermsParty   termsParty = null;
			
			if (terms.doesTermsPartyComponentExist(termsPartyName))
				termsParty = (TermsParty) terms.getComponentHandle(termsPartyName);

			boolean returnFlag = true;
			if (termsParty != null){
				String value = termsParty.getAttribute("branch_code");
					if (StringFunction.isNotBlank(value)){
	
					String country = InvoiceUtility.fetchOpOrgCountry(terms.getAttribute("terms_oid")); 
	
					String bankName = termsParty.getAttribute("name");
					String addressLine1 = termsParty.getAttribute("address_line_1");
					returnFlag = InvoiceUtility.validateBankInformation(paymentMethod, value, country, bankName, addressLine1, terms.getErrorManager(), false, "");
				}
			}else{
				terms.getErrorManager().issueError(getClass().getName(),
						TradePortalConstants.BEN_BANK_NAME_BRANCH_REQD);
				returnFlag =  false;

			}
			return returnFlag;
		}

	/**
	 * Performs the Terms Manager-specific validation of text fields in the Terms
	 * and TermsParty objects.
	 *
	 * @param terms - the managed object
	 *
	 */
		protected void validateForSwiftCharacters(TermsBean terms) throws AmsException, RemoteException
		 {
			final String[] attributesToExclude = {"multiple_related_instruments", "use_other_text", "loan_proceeds_credit_other_txt", "loan_maturity_debit_other_txt", "maturity_use_other_text", "internal_instructions","special_bank_instructions"};			
			// // TLE - 10/25/06 - MHUG101470192 - Added internal_instructions, special_bank_instruction			
			// IR T36000042640 Rel9.5 04/06/2016 - Added invoice_details for ACH payment
			final String[] attributesToExcludeACH = {"multiple_related_instruments", "use_other_text", "loan_proceeds_credit_other_txt", "loan_maturity_debit_other_txt", "maturity_use_other_text", "internal_instructions","special_bank_instructions","invoice_details"};
			int payMthdACHCount=0;
			String hasACHpayMthdSql = "select count(*) as INVOICECOUNT from invoices_summary_data where pay_method=?"
					 +"and a_terms_oid=?"; 
			DocumentHandler result = DatabaseQueryBean.getXmlResultSet(hasACHpayMthdSql, true, new Object[]{TradePortalConstants.PAYMENT_METHOD_ACH_GIRO,terms.getAttribute("terms_oid")});
			if (result != null){
				payMthdACHCount = result.getAttributeInt("/ResultSetRecord(0)/INVOICECOUNT");
			}		
			if(payMthdACHCount>0){
				InstrumentServices.checkForInvalidSwiftCharacters(terms.getAttributeHash(), terms.getErrorManager(), attributesToExcludeACH);
			}
			else {
				InstrumentServices.checkForInvalidSwiftCharacters(terms.getAttributeHash(), terms.getErrorManager(), attributesToExclude);
			}			

            ShipmentTerms shipmentTerms = terms.getFirstShipment();
            if(shipmentTerms != null)
             {
                String [] shipmentAttributesToExclude = {"goods_description"};
                InstrumentServices.checkForInvalidSwiftCharacters(shipmentTerms.getAttributeHash(), shipmentTerms.getErrorManager(), shipmentAttributesToExclude);
             }

			validateTermsPartyForSwiftChars(terms, "FirstTermsParty", "LoanRequest.Beneficiary");
			validateTermsPartyForSwiftChars(terms, "SecondTermsParty", "LoanRequest.Applicant");
			validateTermsPartyForSwiftChars(terms, "ThirdTermsParty", "LoanRequest.BankBeneficiary");
		 }

		/**
		 *  Performs validation of text fields for length.
		 *
		 *  @param terms TermsBean - the bean being validated
		 */
		public void validateTextLengths(TermsBean terms)
		throws java.rmi.RemoteException, AmsException
		{
			String overrideSwiftLengthInd = getOverrideSwiftLengthInd(terms);

			if((overrideSwiftLengthInd != null)
					&& (overrideSwiftLengthInd.equals(TradePortalConstants.INDICATOR_NO)))
			{
				checkTextLength(terms.getFirstShipment(), "goods_description", 6500);
			} 
			checkTextLength(terms, "multiple_related_instruments", 210);
			checkTextLength(terms, "loan_proceeds_credit_other_txt", 210);
			checkTextLength(terms, "use_other_text", 210);
			checkTextLength(terms, "loan_maturity_debit_other_txt", 210);
			checkTextLength(terms, "maturity_use_other_text", 210);
			// TLE - 10/25/06 - MHUG101470192 - Add Begin
			checkTextLength(terms, "internal_instructions", 1000);
			checkTextLength(terms, "special_bank_instructions", 1000);
			// TLE - 10/25/06 - MHUG101470192 - Add End
			// pcutrone - 1-MAR-07 - IR-SAUG120659367 - Begin
			checkTextLength(terms, "finance_type_text", 500);
			// pcutrone - 1-MAR-07 - IR-SAUG120659367 - End
			checkTextLength(terms, "financed_invoices_details_text", 500);
			checkTextLength(terms, "financed_pos_details_text", 500);
		}


	/**
	 * Validates the fields that can only have a certain number of decimal places
	 *
	 */
	public void validateDecimalFields(TermsBean terms) throws AmsException, RemoteException
	 {
			// Validate that the rate field will fit in the database
		InstrumentServices.validateDecimalNumber(terms.getAttribute("fec_rate"),
											 5,
											 8,
											 "TermsBeanAlias.fec_rate",
											 terms.getErrorManager(),
											 terms.getResourceManager());

			// Validate that the rate field will fit in the database
		InstrumentServices.validateDecimalNumber(terms.getAttribute("maturity_fec_rate"),
											 5,
											 8,
											 "TermsBeanAlias.maturity_fec_rate",
											 terms.getErrorManager(),
											 terms.getResourceManager());

	 }

	//Beginning of code used by the Middleware team
	/**
	 *  gets the values of the attributes of the managed object. This method is called
	 *  from the getTermsAttributesDoc() hook in the managed object.
	 *
	 *  return com.amsinc.ecsg.util.DocumentHandler
	 *  @param terms TermsBean - the bean being validated
	 *  @param termsDoc com.amsinc.ecsg.util.DocumentHandler
	 */
	public DocumentHandler getManagerTerms(TermsBean terms, DocumentHandler termsDoc) throws java.rmi.RemoteException, AmsException

	{

			 DocumentHandler termsValuesDoc = terms.populateXmlDoc(new DocumentHandler());
			 LOG.debug("This is the termValuesDoc containing all the terms values" + "\n" +
							 termsValuesDoc.toString());

		// MQ DTD FIELD                                                                       TRADEPORTAL FIELD
		// TERMS DETAILS TERMS
		termsDoc.setAttribute("/Terms/TermsDetails/ReferenceNumber",	                      termsValuesDoc.getAttribute("/Terms/reference_number"));
		termsDoc.setAttribute("/Terms/TermsDetails/AmountCurrencyCode",       	              termsValuesDoc.getAttribute("/Terms/amount_currency_code"));
		termsDoc.setAttribute("/Terms/TermsDetails/Amount",                    	              termsValuesDoc.getAttribute("/Terms/amount"));

		// LOANS AND FUNDS TERMS
		termsDoc.setAttribute("/Terms/LoansAndFunds/LoanTermsFixedMaturityDate", 		      termsValuesDoc.getAttribute("/Terms/loan_terms_fixed_maturity_dt"));
		termsDoc.setAttribute("/Terms/LoansAndFunds/LoanTermsType",       	                  termsValuesDoc.getAttribute("/Terms/loan_terms_type"));
		termsDoc.setAttribute("/Terms/LoansAndFunds/LoanStartDate",			                  termsValuesDoc.getAttribute("/Terms/loan_start_date"));
		termsDoc.setAttribute("/Terms/LoansAndFunds/InterestToBePaid",                        termsValuesDoc.getAttribute("/Terms/interest_to_be_paid"));
		termsDoc.setAttribute("/Terms/LoansAndFunds/LoanTermsNumberOfDays",                   termsValuesDoc.getAttribute("/Terms/loan_terms_number_of_days"));
		termsDoc.setAttribute("/Terms/LoansAndFunds/LoanProceedPaymentCurrency",              termsValuesDoc.getAttribute("/Terms/loan_proceeds_pmt_curr"));
		termsDoc.setAttribute("/Terms/LoansAndFunds/LoanProceedPaymentAmount",                termsValuesDoc.getAttribute("/Terms/loan_proceeds_pmt_amount"));
		termsDoc.setAttribute("/Terms/LoansAndFunds/MultiplerelatedInstruments",              termsValuesDoc.getAttribute("/Terms/multiple_related_instruments"));
		termsDoc.setAttribute("/Terms/LoansAndFunds/LoanProceedCreditAccount",                termsValuesDoc.getAttribute("/Terms/loan_proceeds_credit_acct"));
		termsDoc.setAttribute("/Terms/LoansAndFunds/LoanProceedCreditOtherText",              termsValuesDoc.getAttribute("/Terms/loan_proceeds_credit_other_txt"));
		termsDoc.setAttribute("/Terms/LoansAndFunds/LoanProceedCreditType",                   termsValuesDoc.getAttribute("/Terms/loan_proceeds_credit_type"));
		termsDoc.setAttribute("/Terms/LoansAndFunds/UseMarketRate",                           termsValuesDoc.getAttribute("/Terms/use_mkt_rate"));
		termsDoc.setAttribute("/Terms/LoansAndFunds/UseFec",                                  termsValuesDoc.getAttribute("/Terms/use_fec"));
		termsDoc.setAttribute("/Terms/LoansAndFunds/UseOther",                                termsValuesDoc.getAttribute("/Terms/use_other"));
		termsDoc.setAttribute("/Terms/LoansAndFunds/UseOtherText",                            termsValuesDoc.getAttribute("/Terms/use_other_text"));
		termsDoc.setAttribute("/Terms/LoansAndFunds/LoanMaturityDebitType",                   termsValuesDoc.getAttribute("/Terms/loan_maturity_debit_type"));
		termsDoc.setAttribute("/Terms/LoansAndFunds/LoanMaturityDebitAccount",                termsValuesDoc.getAttribute("/Terms/loan_maturity_debit_acct"));
		termsDoc.setAttribute("/Terms/LoansAndFunds/LoanMaturityDebitOtherText",              termsValuesDoc.getAttribute("/Terms/loan_maturity_debit_other_txt"));
		termsDoc.setAttribute("/Terms/LoansAndFunds/MaturityUseMarketRate",                   termsValuesDoc.getAttribute("/Terms/maturity_use_mkt_rate"));
		termsDoc.setAttribute("/Terms/LoansAndFunds/MaturityUseFec",                          termsValuesDoc.getAttribute("/Terms/maturity_use_fec"));
		termsDoc.setAttribute("/Terms/LoansAndFunds/MaturityCoveredByFecNumber",              termsValuesDoc.getAttribute("/Terms/maturity_covered_by_fec_number"));
		termsDoc.setAttribute("/Terms/LoansAndFunds/MaturityFecAmount",                       termsValuesDoc.getAttribute("/Terms/maturity_fec_amount"));
		termsDoc.setAttribute("/Terms/LoansAndFunds/MaturityFecRate",                         termsValuesDoc.getAttribute("/Terms/maturity_fec_rate"));
		termsDoc.setAttribute("/Terms/LoansAndFunds/MaturityFecMaturityDate",                 termsValuesDoc.getAttribute("/Terms/maturity_fec_maturity_date"));
		termsDoc.setAttribute("/Terms/LoansAndFunds/MaturityUseOther",                        termsValuesDoc.getAttribute("/Terms/maturity_use_other"));
		termsDoc.setAttribute("/Terms/LoansAndFunds/MaturityUseOtherText",                    termsValuesDoc.getAttribute("/Terms/maturity_use_other_text"));
		termsDoc.setAttribute("/Terms/LoansAndFunds/FinancingBackedByBuyer"     ,             termsValuesDoc.getAttribute("/Terms/financing_backed_by_buyer_ind"));
		termsDoc.setAttribute("/Terms/LoansAndFunds/FinancedInvoicesDetailsText",             termsValuesDoc.getAttribute("/Terms/financed_invoices_details_text"));
		termsDoc.setAttribute("/Terms/LoansAndFunds/FinancedPOsDetailsText",                  termsValuesDoc.getAttribute("/Terms/financed_pos_details_text"));
		//RKAZI CR709 Rel 8.2 01/25/2013 Start
		String comChargesType = terms.getAttribute("coms_chrgs_type");
		if (TradePortalConstants.DEBIT_OUR_ACCOUNTS.equals(comChargesType)){
			termsDoc.setAttribute("/Terms/LoansAndFunds/CommChargesOurAccountNumber",             termsValuesDoc.getAttribute("/Terms/coms_chrgs_our_account_num"));
			termsDoc.setAttribute("/Terms/LoansAndFunds/InterestDebitAccountNumber",              termsValuesDoc.getAttribute("/Terms/interest_debit_acct_num"));
		}
		//RKAZI CR709 Rel 8.2 01/25/2013 End
		termsDoc.setAttribute("/Terms/LoansAndFunds/InAdvanceDisposition",                    termsValuesDoc.getAttribute("/Terms/in_advance_disposition"));
		termsDoc.setAttribute("/Terms/LoansAndFunds/InArrearsDisposition",                    termsValuesDoc.getAttribute("/Terms/in_arrears_disposition"));
		termsDoc.setAttribute("/Terms/LoansAndFunds/WorkItemPriority",                        termsValuesDoc.getAttribute("/Terms/work_item_priority"));
		// Get Beneficiary Account Number if available.  Need to get this off of the first terms party
		if (termsValuesDoc.getAttribute("/Terms/loan_proceeds_credit_type").equals(TradePortalConstants.CREDIT_BEN_ACCT)) {
			TermsParty benTermsParty = (TermsParty)(terms.getComponentHandle("FirstTermsParty"));
			String benAcctInfo = benTermsParty.getAttribute("acct_num");
			if (benTermsParty.getAttribute("acct_currency").equals("")) {
				termsDoc.setAttribute("/Terms/LoansAndFunds/BenAccountNumber",				  benAcctInfo);
			} else {
				termsDoc.setAttribute("/Terms/LoansAndFunds/BenAccountNumber",				  benAcctInfo + " (" + benTermsParty.getAttribute("acct_currency") + ")");
			}
		}
		//RKAZI CR709 Rel 8.2 01/25/2013 Start
		String finType = terms.getAttribute("finance_type");
		if (TradePortalConstants.TRADE_LOAN_REC.equals(finType) || TradePortalConstants.TRADE_LOAN_PAY.equals(finType)){
			termsDoc.setAttribute("/Terms/LoansAndFunds/NumberOfPayees",				  "0");
		}
		if (TradePortalConstants.DEBIT_LOAN_PROCEEDS.equals(comChargesType)){
			termsDoc.setAttribute("/Terms/LoansAndFunds/ChargesDeductLoanProceeds",				  TradePortalConstants.INDICATOR_YES);
		} else if (TradePortalConstants.DEBIT_ACCT_REC.equals(comChargesType)){
			termsDoc.setAttribute("/Terms/LoansAndFunds/ChargesDeductAccountsRec",				  TradePortalConstants.INDICATOR_YES);
		}
		//RKAZI CR709 Rel 8.2 01/25/2013 End		
                // ShipmentTerms
                populateFirstShipmentTermsXml(terms, termsDoc);

		// INSTRUMENT
		termsDoc.setAttribute("/Terms/TermsDetails/RelatedInstrumentID",	                          termsValuesDoc.getAttribute("/Terms/related_instrument_id"));

		//Instructions to Bank AOUG111981868
		termsDoc.setAttribute("/Terms/Instructions/InstructionsToBank",       termsValuesDoc.getAttribute("/Terms/special_bank_instructions"));

		//mseshadri BEGIN- CR-21 Add new fields for EXP_FIN
		termsDoc.setAttribute("/Terms/TermsDetails/FinanceType",	                          termsValuesDoc.getAttribute("/Terms/finance_type"));
		termsDoc.setAttribute("/Terms/TermsDetails/FinanceTypeText",	                          termsValuesDoc.getAttribute("/Terms/finance_type_text"));
		termsDoc.setAttribute("/Terms/TermsDetails/LinkedInstrumentID",	                          termsValuesDoc.getAttribute("/Terms/linked_instrument_id"));
		termsDoc.setAttribute("/Terms/TermsDetails/LinkedTransactionType",	                  termsValuesDoc.getAttribute("/Terms/linked_instrument_type"));
		termsDoc.setAttribute("/Terms/TermsDetails/LinkedTransactionSequenceNumber",	          termsValuesDoc.getAttribute("/Terms/linked_tran_seq_num"));
		termsDoc.setAttribute("/Terms/TermsDetails/LinkedInstrumentType",	                  termsValuesDoc.getAttribute("/Terms/linked_transaction_type"));
		//mseshadri END- CR-21 Add new fields for EXP_FIN

		// COMMISSION AND CHARGES INSTRUCTIONS TERMS
		termsDoc.setAttribute("/Terms/Instructions/CommissionAndCharges/CoveredByFECNumber",  termsValuesDoc.getAttribute("/Terms/covered_by_fec_number"));
		termsDoc.setAttribute("/Terms/Instructions/CommissionAndCharges/FECAmount",           termsValuesDoc.getAttribute("/Terms/fec_amount"));
		termsDoc.setAttribute("/Terms/Instructions/CommissionAndCharges/FECRate",             termsValuesDoc.getAttribute("/Terms/fec_rate"));
		termsDoc.setAttribute("/Terms/Instructions/CommissionAndCharges/FECMaturityDate",     termsValuesDoc.getAttribute("/Terms/fec_maturity_date"));



		// TERMS PARTY
	   DocumentHandler termsPartyDoc = new DocumentHandler();

		//Additional Terms Party
		String testTP = TermsPartyType.APPLICANT;


		int id = 0;
		termsPartyDoc.setAttribute("/TermsParty/TotalNumberOfEntries","0");
		String countryOfCOL = "";
		for (int i = 0; i<TradePortalConstants.MAX_NUM_TERMS_PARTIES;i++) {
			String termsPartyOID = termsValuesDoc.getAttribute("/Terms/"+TradePortalConstants.TERMS_PARTY_ATTRIBUTE_NAMES[i]);
			if(!(termsPartyOID == null || termsPartyOID.equals(""))) {
				TermsParty termsParty = (TermsParty)(terms.getComponentHandle(TradePortalConstants.TERMS_PARTY_COMPONENT_NAMES[i]));
				if(termsParty.getAttribute("terms_party_type").equals(testTP)) {
					LOG.debug("party_Type is APP, hence I need to check for thirdParty");
					termsPartyDoc = packageAdditionalTermsParty(terms,id,termsPartyDoc,termsParty);
					if(termsPartyDoc.getAttribute("/AdditionalTermsParty").equals(TradePortalConstants.INDICATOR_YES))
					id++;
				}
				termsPartyDoc.setAttribute("/AdditionalTermsParty",TradePortalConstants.INDICATOR_NO);
				//If the termsValuesDoc contains termsParty also need to modify the line below
				if(termsParty.getAttribute("terms_party_type").equals(TermsPartyType.COLLECTING_BANK))
					countryOfCOL = termsParty.getAttribute("address_country");
				if(termsParty.getAttribute("terms_party_type").equals(TermsPartyType.CASE_OF_NEED))
					termsParty.setAttribute("address_country",countryOfCOL);
					 termsPartyDoc = termsParty.packageTermsPartyAttributes(id,termsPartyDoc);
					 id++;
			}
		}

		LOG.debug("This is TermsPartyDoc: {} " ,  termsPartyDoc.toString());

		termsPartyDoc.setAttribute("/TermsParty/TotalNumberOfEntries",String.valueOf(id));
		termsDoc.setComponent("/TermsParty",termsPartyDoc.getFragment("/TermsParty"));

		return termsDoc;
	}

	/**
	 * Performs presave processing (regardless of whether validation is done)
	 *
	 * @param terms com.ams.tradeportal.busobj.TermsBean
	 * @exception com.amsinc.ecsg.frame.AmsException The exception description.
	 */
	public void preSave(TermsBean terms) throws AmsException {
		  super.preSave(terms);

		try {
			validateAmount(terms, "amount", "amount_currency_code");
			validateAmount(terms, "loan_proceeds_pmt_amount", "loan_proceeds_pmt_curr");
			validateAmount(terms, "fec_amount", "loan_proceeds_pmt_curr");
			validateAmount(terms, "maturity_fec_amount", "amount_currency_code");

			validateTextLengths(terms);
			validateDecimalFields(terms);
			String financeType     = terms.getAttribute("finance_type");
            
			String importInd = terms.getImportIndicator();

			if (StringFunction.isNotBlank(financeType) && (TradePortalConstants.TRADE_LOAN_PAY.equals(financeType))
					&& TradePortalConstants.INDICATOR_T.equals(importInd)
					&& TradePortalConstants.CREDIT_BEN_ACCT.equals(terms.getAttribute("loan_proceeds_credit_type"))){
				populateBankDetailsForBen(terms);
			}
		} catch (RemoteException e) {
			LOG.error("Remote Exception caught in LRQ__ISSTermsMgr.preSave()",e);
		}

	   }

	private void populateBankDetailsForBen(TermsBean terms) throws RemoteException, AmsException {

		String paymentMethod = terms.getAttribute("payment_method");

		TermsParty   termsParty = null;
		TermsParty benTermsParty = null;
		if (terms.doesTermsPartyComponentExist("ThirdTermsParty"))
			termsParty = (TermsParty) terms.getComponentHandle("ThirdTermsParty");

		if (terms.doesTermsPartyComponentExist("FirstTermsParty"))
			benTermsParty = (TermsParty) terms.getComponentHandle("FirstTermsParty");

		if (termsParty != null && benTermsParty != null){
			String value = termsParty.getAttribute("branch_code");
			String country = InvoiceUtility.fetchOpOrgCountry(terms.getAttribute("terms_oid")); 
			if (StringFunction.isNotBlank(value)){
				//SSikhakolli - 05/06/2014 - Rel 9.0 IR#T36000026319 - SQL fix - Begin
				ArrayList<Object> sqlVars = new ArrayList<Object>();
				StringBuilder sqlQuery = new StringBuilder();
			     sqlQuery.append("select BANK_BRANCH_CODE, case when bank_name is null then branch_name else bank_name end as bank_name");
			     sqlQuery.append(", address_line_1, address_line_2, address_city, address_state_province, address_country from BANK_BRANCH_RULE ");
			     sqlQuery.append(" where upper(BANK_BRANCH_CODE) = ? ");
			     
			     sqlVars.add(value.toUpperCase());
			     
			     if (StringFunction.isNotBlank(paymentMethod)){
			    	sqlQuery.append(" and upper(PAYMENT_METHOD) = ? ");
				    sqlVars.add(paymentMethod.toUpperCase());
			     }
			     
			     if (StringFunction.isNotBlank(country) && 
			    		 (StringFunction.isNotBlank(paymentMethod) && 
			    				 !TradePortalConstants.PAYMENT_METHOD_CBFT.equals(paymentMethod))){
			    	sqlQuery.append(" and upper(ADDRESS_COUNTRY) = ? ");
			    	sqlVars.add(country.toUpperCase());
			     }

				DocumentHandler result = (DocumentHandler)DatabaseQueryBean.getXmlResultSet(sqlQuery.toString(), true, sqlVars);
				//SSikhakolli - 05/06/2014 - Rel 9.0 IR#T36000026319 - SQL fix - End
				
				if (result!=null){
					String tempValue = "";
					tempValue = result.getAttribute("/ResultSetRecord/BANK_NAME");
					if (StringFunction.isBlank(termsParty.getAttribute("name")) && StringFunction.isNotBlank(tempValue)){
						if (tempValue.length() > 35){
							//get first 35 characters
							tempValue = tempValue.substring(0, 35);
						}
						termsParty.setAttribute("name", tempValue);
					}
					if (StringFunction.isBlank(termsParty.getAttribute("address_line_1")) && StringFunction.isNotBlank(result.getAttribute("/ResultSetRecord/ADDRESS_LINE_1"))){
						termsParty.setAttribute("address_line_1", result.getAttribute("/ResultSetRecord/ADDRESS_LINE_1"));
					}
					if (StringFunction.isBlank(termsParty.getAttribute("address_line_2")) && StringFunction.isNotBlank(result.getAttribute("/ResultSetRecord/ADDRESS_LINE_2"))){
						termsParty.setAttribute("address_line_2", result.getAttribute("/ResultSetRecord/ADDRESS_LINE_2"));
					}
					tempValue = result.getAttribute("/ResultSetRecord/ADDRESS_CITY");
					if (StringFunction.isBlank(termsParty.getAttribute("address_city")) && StringFunction.isNotBlank(tempValue)){
						if (tempValue.length() > 23){
							//get first 35 characters
							tempValue = tempValue.substring(0, 23);
						}
						termsParty.setAttribute("address_city", tempValue);
					}
					if (StringFunction.isBlank(termsParty.getAttribute("address_state_province")) && StringFunction.isNotBlank(result.getAttribute("/ResultSetRecord/ADDRESS_STATE_PROVINCE"))){
						termsParty.setAttribute("address_state_province", result.getAttribute("/ResultSetRecord/ADDRESS_STATE_PROVINCE"));
					}
					if (StringFunction.isBlank(termsParty.getAttribute("address_country")) && StringFunction.isNotBlank(result.getAttribute("/ResultSetRecord/ADDRESS_COUNTRY"))){
						termsParty.setAttribute("address_country", result.getAttribute("/ResultSetRecord/ADDRESS_COUNTRY"));
					}
					
				}
			}	
		}
		
	}
	
	/**
	 * This method is used to check whether owner org of instrument is integrated with TPS or not.
	 * @param terms
	 * @return
	 * @throws RemoteException
	 * @throws AmsException
	 */
	private boolean isCustNotIntgWithTPS( TermsBean terms ) throws RemoteException, AmsException {
		
		boolean custNotIntgWithTPS = false;
		String sql = " SELECT cust_is_not_intg_tps FROM instrument i, transaction t, corporate_org c "
				+ " WHERE c.organization_oid = i.a_corp_org_oid AND i.instrument_oid = t.p_instrument_oid "
				+ " AND t.c_cust_enter_terms_oid = ?";
		DocumentHandler resultDoc = DatabaseQueryBean.getXmlResultSet(sql, true, terms.getAttribute("terms_oid"));
		if( resultDoc!=null && TradePortalConstants.INDICATOR_YES.equals(resultDoc.getAttribute("/ResultSetRecord/CUST_IS_NOT_INTG_TPS")) ) {
			custNotIntgWithTPS = true;			
		}		
		return custNotIntgWithTPS;
		
	}
	/**
	 * Added for Rel9.4 CR-1001
	 * @param terms
	 * @param termsPartyName
	 * @return
	 * @throws RemoteException
	 * @throws AmsException
	 */
	private boolean validateReportingCodes(TermsBean terms, String termsPartyName) throws RemoteException, AmsException {
		
		int invCount = 0;
		String hasInvoicesSQL = "SELECT  count(*) as INVOICECOUNT from invoices_summary_data where"
				+ "  (a_transaction_oid in (select transaction_oid from transaction where c_cust_enter_terms_oid = ? )) ";
		
		DocumentHandler resultInvDoc = DatabaseQueryBean.getXmlResultSet(hasInvoicesSQL, true, 
				new Object[]{terms.getAttribute("terms_oid")});
		
		if (resultInvDoc != null){
			invCount = resultInvDoc.getAttributeInt("/ResultSetRecord(0)/INVOICECOUNT");
		}
		//In case of invoices uploaded, reporting code validations will NOT be done
		if(invCount > 0){
			return true;
		}
		String paymentMethod = terms.getAttribute("payment_method");

		TermsParty   termsParty = null;
		String opOrgOid = null;
		
		if (terms.doesTermsPartyComponentExist(termsPartyName))
			termsParty = (TermsParty) terms.getComponentHandle(termsPartyName);
		
		String[] reportingCodes = new String[2];    	

		boolean returnFlag = true;
		if (termsParty != null){
			reportingCodes[0] = termsParty.getAttribute("reporting_code_1");
	    	reportingCodes[1] = termsParty.getAttribute("reporting_code_2");
	    	
			 String sqlQuery = "select a_op_bank_org_oid from instrument, transaction "
					+ " where instrument_oid = p_instrument_oid "
					+ " and c_cust_enter_terms_oid = ?";

			DocumentHandler resultDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery, true, terms.getAttribute("terms_oid"));
			if(resultDoc!=null){
				opOrgOid = resultDoc.getAttribute("/ResultSetRecord/A_OP_BANK_ORG_OID");
				
			}
			if(opOrgOid!=null){
				String bankGroupOid = InvoiceUtility.fetchBankGroupForOpOrg(opOrgOid);
				returnFlag = InvoiceUtility.validateReportingCodes(bankGroupOid, paymentMethod, reportingCodes,"", "", terms.getErrorManager());
			}
		}
		return returnFlag;
	}


 }