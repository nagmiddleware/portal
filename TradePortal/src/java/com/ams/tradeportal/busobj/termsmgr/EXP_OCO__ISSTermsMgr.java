package com.ams.tradeportal.busobj.termsmgr;
import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.util.Calendar;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.busobj.Phrase;
import com.ams.tradeportal.busobj.TermsBean;
import com.ams.tradeportal.busobj.TermsParty;
import com.ams.tradeportal.busobj.util.InstrumentServices;
import com.ams.tradeportal.busobj.webbean.TermsWebBean;
import com.ams.tradeportal.common.TermsPartyType;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.Attribute;
import com.amsinc.ecsg.frame.AttributeManager;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.frame.EJBObjectFactory;
import com.amsinc.ecsg.frame.QueryListView;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.JPylonProperties;
import com.amsinc.ecsg.util.StringFunction;

/**
 * This class manages the dynamic attribute registration and validation
 * for a Terms business object or TermsWebBean for an Export Collection Issue
 *
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 *     Vasavi CR 524 03/31/2010
 */
public class EXP_OCO__ISSTermsMgr extends TermsMgr
{
private static final Logger LOG = LoggerFactory.getLogger(EXP_OCO__ISSTermsMgr.class);
	//This will be used to loop through all the tenors - there can only be 6 of them.
	public final static int NUMBER_OF_TENORS = 6;
	public final static int INSTR_TO_BANK_DECIMAL_PLACES = 7;

	/**
	 *  Constructor that is used by business objects to create their
	 *  manager.  This method also register the dynamic attributes of
	 *  the bean.
	 *
	 *  @param mgr AttributeManager attribute manager of the bean being managed.  Used to register attributes
	 */
	public EXP_OCO__ISSTermsMgr(AttributeManager mgr) throws AmsException
	{
		super(mgr);
	}

	/**
	 *  Constructor that is used by web beans to create their
	 *  manager.  This method also register the dynamic attributes of
	 *  the bean.
	 *
	 *  @param mgr TermsWebBean - the bean being managed.  Used to register attributes.
	 */
	public EXP_OCO__ISSTermsMgr(TermsWebBean terms) throws AmsException
	{
		super(terms);
	}

	/**
	 *  Returns a list of attributes.  These describe each of the attributes
	 *  that will be registered for the bean being managed.
	 *
	 *  This method controls which attributes of TermsBean or TermsWebBean are registered
	 *  for an this instrument and transaction type
	 *
	 *  @return Vector - list of attributes
	 */
	public List<Attribute> getAttributesToRegister()
	{
		List<Attribute> attrList = super.getAttributesToRegister();
		attrList.add( TermsBean.Attributes.create_amount() );
		attrList.add( TermsBean.Attributes.create_amount_currency_code() );
		attrList.add( TermsBean.Attributes.create_branch_code() );
		attrList.add( TermsBean.Attributes.create_bank_action_required() );
		attrList.add( TermsBean.Attributes.create_amendment_details() );
		attrList.add( TermsBean.Attributes.create_collect_interest_at() );
		attrList.add( TermsBean.Attributes.create_collect_interest_at_indicator() );
		attrList.add( TermsBean.Attributes.create_collection_charges_includ_txt() );
		attrList.add( TermsBean.Attributes.create_collection_charges_type() );
		attrList.add( TermsBean.Attributes.create_collection_date() );
		attrList.add( TermsBean.Attributes.create_covered_by_fec_number() );
		attrList.add( TermsBean.Attributes.create_do_not_waive_refused_charges() );
		attrList.add( TermsBean.Attributes.create_do_not_waive_refused_interest() );
		attrList.add( TermsBean.Attributes.create_fec_amount() );
		attrList.add( TermsBean.Attributes.create_fec_maturity_date() );
		attrList.add( TermsBean.Attributes.create_fec_rate() );
		attrList.add( TermsBean.Attributes.create_in_case_of_need_contact() );
		attrList.add( TermsBean.Attributes.create_in_case_of_need_contact_text() );
		attrList.add( TermsBean.Attributes.create_in_case_of_need_use_instruct() );
		attrList.add( TermsBean.Attributes.create_instr_advise_accept_by_telco() );
		attrList.add( TermsBean.Attributes.create_instr_advise_non_accept_by_tel() );
		attrList.add( TermsBean.Attributes.create_instr_advise_non_pmt_by_telco() );
		attrList.add( TermsBean.Attributes.create_instr_advise_pmt_by_telco() );
		attrList.add( TermsBean.Attributes.create_instr_note_for_non_accept() );
		attrList.add( TermsBean.Attributes.create_instr_note_for_non_pmt() );
		attrList.add( TermsBean.Attributes.create_instr_other() );
		attrList.add( TermsBean.Attributes.create_instr_protest_for_non_accept() );
		attrList.add( TermsBean.Attributes.create_instr_protest_for_non_pmt() );
		attrList.add( TermsBean.Attributes.create_instr_release_doc_on_accept() );
		attrList.add( TermsBean.Attributes.create_instr_release_doc_on_pmt() );
		attrList.add( TermsBean.Attributes.create_other_settlement_instructions() );
		attrList.add( TermsBean.Attributes.create_payment_instructions() );
		attrList.add( TermsBean.Attributes.create_pmt_instr_multiple_phrase() );
		attrList.add( TermsBean.Attributes.create_pmt_terms_days_after_type() );
		attrList.add( TermsBean.Attributes.create_pmt_terms_fixed_maturity_date() );
		attrList.add( TermsBean.Attributes.create_pmt_terms_num_days_after () );
		attrList.add( TermsBean.Attributes.create_pmt_terms_type() );
		attrList.add( TermsBean.Attributes.create_present_air_waybill() );
		attrList.add( TermsBean.Attributes.create_present_bill_of_lading() );
		attrList.add( TermsBean.Attributes.create_present_bills_of_exchange() );
		attrList.add( TermsBean.Attributes.create_present_cert_of_origin() );
		attrList.add( TermsBean.Attributes.create_present_commercial_invoice() );
		attrList.add( TermsBean.Attributes.create_present_insurance() );
		attrList.add( TermsBean.Attributes.create_present_non_neg_bill_of_lading() );
		attrList.add( TermsBean.Attributes.create_present_num_air_waybill() );
		attrList.add( TermsBean.Attributes.create_present_num_bill_of_lading() );
		attrList.add( TermsBean.Attributes.create_present_num_bills_of_exchange() );
		attrList.add( TermsBean.Attributes.create_present_num_cert_of_origin() );
		attrList.add( TermsBean.Attributes.create_present_num_commercial_invoice() );
		attrList.add( TermsBean.Attributes.create_present_num_insurance() );
		attrList.add( TermsBean.Attributes.create_present_num_non_neg_bill_ladng() );
		attrList.add( TermsBean.Attributes.create_present_num_packing_list() );
		attrList.add( TermsBean.Attributes.create_present_other() );
		attrList.add( TermsBean.Attributes.create_present_other_text() );
		attrList.add( TermsBean.Attributes.create_present_packing_list() );
		attrList.add( TermsBean.Attributes.create_purpose_type() );
		attrList.add( TermsBean.Attributes.create_reference_number() );
		attrList.add( TermsBean.Attributes.create_settlement_foreign_acct_curr() );
		attrList.add( TermsBean.Attributes.create_settlement_foreign_acct_num() );
		attrList.add( TermsBean.Attributes.create_settlement_our_account_num() );
		attrList.add( TermsBean.Attributes.create_special_bank_instructions() );
		attrList.add( TermsBean.Attributes.create_tenor_1() );
		attrList.add( TermsBean.Attributes.create_tenor_1_amount() );
		attrList.add( TermsBean.Attributes.create_tenor_1_draft_number() );
		attrList.add( TermsBean.Attributes.create_tenor_2() );
		attrList.add( TermsBean.Attributes.create_tenor_2_amount() );
		attrList.add( TermsBean.Attributes.create_tenor_2_draft_number() );
		attrList.add( TermsBean.Attributes.create_tenor_3() );
		attrList.add( TermsBean.Attributes.create_tenor_3_amount() );
		attrList.add( TermsBean.Attributes.create_tenor_3_draft_number() );
		attrList.add( TermsBean.Attributes.create_tenor_4() );
		attrList.add( TermsBean.Attributes.create_tenor_4_amount() );
		attrList.add( TermsBean.Attributes.create_tenor_4_draft_number() );
		attrList.add( TermsBean.Attributes.create_tenor_5() );
		attrList.add( TermsBean.Attributes.create_tenor_5_amount() );
		attrList.add( TermsBean.Attributes.create_tenor_5_draft_number() );
		attrList.add( TermsBean.Attributes.create_tenor_6() );
		attrList.add( TermsBean.Attributes.create_tenor_6_amount() );
		attrList.add( TermsBean.Attributes.create_tenor_6_draft_number() );
		attrList.add( TermsBean.Attributes.create_ucp_details() );
		attrList.add( TermsBean.Attributes.create_ucp_version () );
		attrList.add( TermsBean.Attributes.create_urc_pub_num() );
		attrList.add( TermsBean.Attributes.create_urc_year() );
        attrList.add( TermsBean.Attributes.create_work_item_number() );
        
		return attrList;
	}








	/**
	 * Performs presave processing (regardless of whether validation is done)
	 *
	 * @param terms com.ams.tradeportal.busobj.TermsBean
	 * @exception com.amsinc.ecsg.frame.AmsException The exception description.
	 */
	public void preSave(TermsBean terms) throws AmsException
	{
		super.preSave(terms);

		String value;

		try {
			//The text length check is done in the presave to guard against
			//a possible problem with saving the data into the DB if it is too long.
			validateTextLengths(terms);

			//If the currency is about to change - then we need to reset the
			//payment_instructions attribute to "" to force validate to occur on the
			//Payment Instructions section...Also, the 'CURR' denotes that the Currency
			//code has changed.  The pmt_instr_multiple_phrse is a status indicator to
			//let us know if we need to show the Payment Instructions section to the user
			//in the upcoming jsp.
			if( terms.getAttribute("pmt_instr_multiple_phrase").equals(TradePortalConstants.PMT_INSTR_CURR ) )
			{
				terms.setAttribute("payment_instructions", "");
				terms.setAttribute("pmt_instr_multiple_phrase", TradePortalConstants.PMT_INSTR_NO );
			}

			// Validate that the rate field will fit in the database
			InstrumentServices.validateDecimalNumber(terms.getAttribute("fec_rate"),
					5,
					8,
					"TermsBeanAlias.fec_rate",
					terms.getErrorManager(),
					terms.getResourceManager());

			//Check all amount entry fields
			validateAmount(terms, "amount", "amount_currency_code");
			validateAmount(terms, "fec_amount", "amount_currency_code");
			//including tenor amounts if they are non-null --Loop thru all 6
			for(int x=1; x <= NUMBER_OF_TENORS; x++)
			{
				value = terms.getAttribute("tenor_" + x + "_amount");
				if( !StringFunction.isBlank(value) )
				{
					validateAmount(terms, "tenor_" + x + "_amount", "amount_currency_code");
				}
			}
		} catch (java.rmi.RemoteException e) {
			LOG.error("Remote Exception caught in EXP_OCO__ISSTermsMgr.preSave()",e);
		}

	}



	/**
	 *  Performs validation of the attributes of this type of instrument and transaction.   This method is
	 *  called from the userValidate() hook in the managed object.
	 *
	 *  @param terms - the bean being validated
	 */
	public void validate(TermsBean terms)
	throws java.rmi.RemoteException, AmsException
	{
		super.validate(terms);

		terms.registerRequiredAttribute("amount_currency_code");
		terms.registerRequiredAttribute("amount");
		terms.registerRequiredAttribute("pmt_terms_type");
		terms.registerRequiredAttribute("collection_charges_type");
		// The second terms party is the Drawer.
		terms.registerRequiredAttribute("c_SecondTermsParty");
		// The third terms party is the collecting bank.
		// Instead of letting jPylon handle this for us - we'll explicitely
		// issue the error by calling : issueTermsPartyError from the super class TermsMgr.
		//terms.registerRequiredAttribute("c_ThirdTermsParty");
		if( StringFunction.isBlank(terms.getAttribute("c_ThirdTermsParty"))) {
			issueTermsPartyError(terms, "ExportCollectionIssue.CollectingBank");
		}
		//Start validating each section of the Jsp
		validateGeneral(terms);
		validateDocsPresented(terms);
		validateCollectionInstructions(terms);
		validateInstructionsToBank(terms);

		//If an error has NOT be issued to the error manager,
		//call this validation - otherwise there's no need to call this...
		if (terms.getErrorManager().getMaxErrorSeverity() <= terms.getErrorManager().WARNING_ERROR_SEVERITY )
			validatePaymentInstructions(terms);

		validateTextLengths(terms);

	}



	/**
	 *  Performs validation of the general section of the page
	 *  Calls ValidateMultiTenor here because the use case calls for certain
	 *  checks due to a Multi-tenor radio button in this section.
	 *
	 *  @param terms TermsBean - the bean being validated
	 */
	public void validateGeneral(TermsBean terms)
	throws java.rmi.RemoteException, AmsException {

		String value;

		//Validate the required Terms party info first (for the Drawee)
		this.editRequiredDraweeValue(terms, "FirstTermsParty");
		validateTermsPartyForISO8859Chars(terms, "FirstTermsParty");
		validateTermsPartyForISO8859Chars(terms, "SecondTermsParty");
		validateTermsPartyForISO8859Chars(terms, "ThirdTermsParty");
		// At Fixed Maturity Date must be a valid date.  The required edit
		// logic guarantees it has a value.  The setAttribute logic
		// guarantees it is a valid date.  No logic required here.
		//
		// A warning will be issued if the Fixed Maturity Date is before
		// the Transaction Status Date
		if(terms.getAttribute("pmt_terms_fixed_maturity_date") != null)
		{
			Calendar fixedMaturityDate = getFixedMaturityDateAsCalendar(terms);
			Calendar transactionStatusDate = getTransactionStatusDateAsCalendar(terms);
			if((fixedMaturityDate != null) &&(transactionStatusDate != null))
			{
				if(fixedMaturityDate.before(transactionStatusDate))
				{
					terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
							TradePortalConstants.FIXED_MATURITY_DATE_BEFORE_TRANS_DATE);
				} 
			} 
		} 

		// If pmt terms type is the 'days after' option, both
		// the num days and tenor type fields are required.  If
		// the type is not 'days after' reset those attributes to blank.
		value = terms.getAttribute("pmt_terms_type");
		if (value.equals(TradePortalConstants.PMT_DAYS_AFTER)) {
			value = terms.getAttribute("pmt_terms_num_days_after");
			if (StringFunction.isBlank( value )) {
				terms.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.NUM_DAYS_REQD);
			}
			value = terms.getAttribute("pmt_terms_days_after_type");
			if (StringFunction.isBlank( value )) {
				terms.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.TENOR_REQD);
			}
		} else {
			terms.setAttribute("pmt_terms_num_days_after", "");
			terms.setAttribute("pmt_terms_days_after_type", "");
		}
		//Reset the fixed Maturity date if the radial button is not selected
		if( !value.equals(TradePortalConstants.PMT_FIXED_MATURITY) )
		{
			terms.setAttribute("pmt_terms_fixed_maturity_date", "");
		}
		else
		{
			if (StringFunction.isBlank(terms.getAttribute("pmt_terms_fixed_maturity_date")))
			{
				terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.MATURITY_DATE_REQUIRED);
			}
		}

		validateMultiTenors(terms);

		// pcutrone - 10/17/2007 - REUH101146143 - Check to make sure the amount field is not set to zero.
		value = terms.getAttribute("amount");
		if (!StringFunction.isBlank(value)) {
			if (Double.parseDouble(value) == 0) {
				terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.AMOUNT_IS_ZERO);
			}
		}
		// pcutrone - 10/17/2007 - REUH101146143 - END


	}



	/**
	 *  Performs validation of text fields for length.
	 *
	 *  @param terms TermsBean - the bean being validated
	 */
	public void validateTextLengths(TermsBean terms)
	throws java.rmi.RemoteException, AmsException {
		/* KMehta IR-T36000040135 Rel 9500 on 07-Jan-2015 Change  - Begin*/
		/*
		String overrideSwiftLengthInd = getOverrideSwiftLengthInd(terms);

		if((overrideSwiftLengthInd != null)
				&& (overrideSwiftLengthInd.equals(TradePortalConstants.INDICATOR_NO)))
		{
			checkTextLength(terms.getFirstShipment(), "goods_description", 500);
		} */
		   
		// if((overrideSwiftLengthInd != null)
		// && (overrideSwiftLengthInd.equals(TradePortalConstants.INDICATOR_NO)))
		
		checkTextLength(terms.getFirstShipment(), "goods_description", 4000);
		/* KMehta IR-T36000040135 Rel 9500 on 07-Jan-2015 Change  - End*/
		checkTextLength(terms, "present_other_text", 2000);
		checkTextLength(terms, "instr_other", 500);
		checkTextLength(terms, "in_case_of_need_contact_text", 500);
		checkTextLength(terms, "collection_charges_includ_txt", 1000);
		checkTextLength(terms, "collect_interest_at", 200);
		checkTextLength(terms, "special_bank_instructions", 1000);
		checkTextLength(terms, "other_settlement_instructions", 4000); //Rel 9.5 IR T36000040135
	}



	/**
	 * Performs validation for the documents Presented section.
	 *
	 * @param terms com.ams.tradeportal.busobj.TermsBean
	 */
	public void validateDocsPresented(TermsBean terms)
	throws java.rmi.RemoteException, AmsException
	{
		boolean check = false;          //number of check boxes turned on...
		boolean result = false;
		String  errVal1;

		//IF the check box for the Bill(s) of Exchange is NOT checked -then-
		//erase the number of documents entered for the Bill of Exchange.
		//ELSE IF the number of documents is null then throw an error...

		result = checkAssocCheckBoxValues(terms, "present_bills_of_exchange",
				"present_num_bills_of_exchange",
				TradePortalConstants.NUMBER_OF_DOCS_MISSING,
		"ExportCollectionIssue.BillsOfExchangeDrafts");
		check = check || result;

		//Commercial Invoice
		result = checkAssocCheckBoxValues(terms, "present_commercial_invoice",
				"present_num_commercial_invoice",
				TradePortalConstants.NUMBER_OF_DOCS_MISSING,
		"ExportCollectionIssue.CommercialInvoice");
		check = check || result;

		//Bill of Lading
		result = checkAssocCheckBoxValues(terms, "present_bill_of_lading",
				"present_num_bill_of_lading",
				TradePortalConstants.NUMBER_OF_DOCS_MISSING,
		"ExportCollectionIssue.BillOfLading" );
		check = check || result;

		//Non-Negotiable Bill of Lading
		result = checkAssocCheckBoxValues(terms, "present_non_neg_bill_of_lading",
				"present_num_non_neg_bill_ladng",
				TradePortalConstants.NUMBER_OF_DOCS_MISSING,
		"ExportCollectionIssue.NonNegotiableBillOfLading" );
		check = check || result;

		//Air Waybill
		result = checkAssocCheckBoxValues(terms, "present_air_waybill",
				"present_num_air_waybill",
				TradePortalConstants.NUMBER_OF_DOCS_MISSING,
		"ExportCollectionIssue.AirWaybill" );
		check = check || result;

		//Insurance Policy / Certificate
		result = checkAssocCheckBoxValues(terms, "present_insurance",
				"present_num_insurance",
				TradePortalConstants.NUMBER_OF_DOCS_MISSING,
		"ExportCollectionIssue.InsurancePolicyCertificate" );
		check = check || result;

		//Certificate of origin
		result = checkAssocCheckBoxValues(terms, "present_cert_of_origin",
				"present_num_cert_of_origin",
				TradePortalConstants.NUMBER_OF_DOCS_MISSING,
		"ExportCollectionIssue.CertificateOfOrigin" );
		check = check || result;

		//Packing List
		result = checkAssocCheckBoxValues(terms, "present_packing_list",
				"present_num_packing_list",
				TradePortalConstants.NUMBER_OF_DOCS_MISSING,
		"ExportCollectionIssue.PackingList" );
		check = check || result;

		//Other documents
		result = checkAssocCheckBoxValues(terms, "present_other",
				"present_other_text",
				TradePortalConstants.OTHER_DOCS_MISSING,
				null);
		check = check || result;

		if ( check == false )
		{
			errVal1 = terms.getResourceManager().getText( "ExportCollectionIssue.DocsPresented",
					TradePortalConstants.TEXT_BUNDLE);

			terms.getErrorManager().issueError( TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.NO_DOCUMENTS_SELECTED,
					errVal1);
		}
	}



	/**
	 * Performs validation for the documents Presented section.
	 *
	 * @param terms com.ams.tradeportal.busobj.TermsBean
	 */
	public void validateCollectionInstructions(TermsBean terms)
	throws java.rmi.RemoteException, AmsException
	{
		String inCaseOfNeedCheck  = terms.getAttribute("in_case_of_need_contact");
		String inCaseOfNeedText   = terms.getAttribute("in_case_of_need_contact_text");
		String inCaseOfNeedRadio  = terms.getAttribute("in_case_of_need_use_instruct");
		String pmtTermsType = terms.getAttribute("pmt_terms_type");
		String yes = TradePortalConstants.INDICATOR_YES;
		String no  = TradePortalConstants.INDICATOR_NO;
		String value;

		//In Case Of Need, Contact:

		if (inCaseOfNeedCheck.equals(yes)) {
			// If radios are not selected and one of the party textboxes are defined w/ a value.
			if( this.hasAddressInfo(terms, "FourthTermsParty") &&
					(StringFunction.isBlank(inCaseOfNeedRadio)) )
				terms.getErrorManager().issueError( TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.IN_CASE_MISSING_AUTHORITY );

			//if a radio button is selected yet no data was entered for the party (in one of the 4 textboxes)
			if( (StringFunction.isNotBlank(inCaseOfNeedRadio)) &&
					(!this.hasAddressInfo(terms, "FourthTermsParty")) )
				terms.getErrorManager().issueError( TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.IN_CASE_MISSING_TEXT );

			// If the checkbox is checked but there is no data in any other the
			// associated fields, uncheck the checkbox.
			if (!this.hasAddressInfo(terms, "FourthTermsParty") &&
					(StringFunction.isBlank(inCaseOfNeedRadio)) &&
					(StringFunction.isBlank(inCaseOfNeedText))) {
				terms.setAttribute("in_case_of_need_contact", no);
			}

		} else {
			// The In Case Of Need, contact: checkbox is N.  Reset all the
			// associated fields to blank.
			terms.setAttribute("in_case_of_need_use_instruct", "");
			terms.setAttribute("in_case_of_need_contact_text", "");
			removeTermsParty(terms, "FourthTermsParty");
		}


		//if a Payment Term selection is By Sight or Cash Against Documents,
		//Then make sure one of the 5 types of collection Instructions were NOT
		//Selected - Otherwise
		if( (!StringFunction.isBlank(pmtTermsType)) &&
				((pmtTermsType.equals(TradePortalConstants.PMT_SIGHT)) ||
						(pmtTermsType.equals(TradePortalConstants.PMT_CASH_AGAINST)))  )
		{
			if(( terms.getAttribute("instr_release_doc_on_accept").equals( yes ))    ||
					( terms.getAttribute("instr_advise_accept_by_telco").equals( yes ))   ||
					( terms.getAttribute("instr_advise_non_accept_by_tel").equals( yes )) ||
					( terms.getAttribute("instr_note_for_non_accept").equals( yes ))      ||
					( terms.getAttribute("instr_protest_for_non_accept").equals( yes )) )
				terms.getErrorManager().issueError( TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.INVALID_COMB_PMT_COLL_INSTR );
		}

		//Collect Interest At:
		//If the first radio button is selected (All Banks) but the text boxis empty--
		//Then issue an error.

		value = terms.getAttribute("collection_charges_type");
		if (value.equals(TradePortalConstants.COLLECTION_CHARGES_TYPE_ALL)) {
			value = terms.getAttribute("collection_charges_includ_txt");
			// VS IR LIUK031741938 01/20/11 Rel 7.0: update from isBlank to isBlankOrWhiteSpace
			if (StringFunction.isBlank(value))
				terms.getErrorManager().issueError( TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.SPECIFY_ADDITIONAL_CHARGES );
		} else {
			// When checkbox is N, reset the text.
			terms.setAttribute("collection_charges_includ_txt", "");
		}

		//If the Collect interest at Check box is selected but no data is in the Text Area
		// Then reset the check box to 'N' - per use case
		value = terms.getAttribute("collect_interest_at");

		if( StringFunction.isBlank( value ) )
			terms.setAttribute("collect_interest_at_indicator", no);

		// If the Collect Interest At checkbox is N, set the text to blank
		value = terms.getAttribute("collect_interest_at_indicator");
		if (value.equals(no)) {
			terms.setAttribute("collect_interest_at", "");
		}
	}



	/**
	 * Performs validation for the Instructions to Bank section.
	 *
	 * @param terms com.ams.tradeportal.busobj.TermsBean
	 */
	public void validateInstructionsToBank(TermsBean terms)
	throws java.rmi.RemoteException, AmsException
	{
		String amount;
		String currencyCode;
		String errorVal1;
		String errorVal2;
		String rate;
		int length;
		int index;

		amount = terms.getAttribute("settlement_foreign_acct_num");
		currencyCode = terms.getAttribute("settlement_foreign_acct_curr");

		//If Foreign Account Number has a value and the Currency code does not - issue an error
		if( (!StringFunction.isBlank(amount)) &&
				(StringFunction.isBlank(currencyCode)) )
		{
			errorVal1 = terms.getResourceManager().getText( "ExportCollectionIssue.CreditForeignCurrencyAccountNumber",
					TradePortalConstants.TEXT_BUNDLE);
			errorVal2 = terms.getResourceManager().getText( "ExportCollectionIssue.SettlementInstructions",
					TradePortalConstants.TEXT_BUNDLE);

			terms.getErrorManager().issueError( TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.NOT_INDICATED_UNDER,
					errorVal1, errorVal2);
		}

		//If Currency code has a value and the Foreign Account Number does not - issue an error
		if( (!StringFunction.isBlank(currencyCode)) &&
				(StringFunction.isBlank(amount)) )
		{
			errorVal1 = terms.getResourceManager().getText( "ExportCollectionIssue.CcyofAccount",
					TradePortalConstants.TEXT_BUNDLE);
			errorVal2 = terms.getResourceManager().getText( "ExportCollectionIssue.SettlementInstructions",
					TradePortalConstants.TEXT_BUNDLE);

			terms.getErrorManager().issueError( TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.NOT_INDICATED_UNDER,
					errorVal1, errorVal2);
		}

	}


	/**
	 * Performs validation for the Multi-Tenor section.
	 * This is only called in the general section since the only way to enter Tenors is if
	 *
	 * @param terms com.ams.tradeportal.busobj.TermsBean
	 */
	public void validateMultiTenors(TermsBean terms)
	throws java.rmi.RemoteException, AmsException
	{
		String tenorNum;
		String tenorAmount;
		String tenorDraftNum;
		String value;
		String value2;
		String pmtTermsType = terms.getAttribute("pmt_terms_type");
		BigDecimal totalAmount = BigDecimal.ZERO;

		for(int x=1; x <= NUMBER_OF_TENORS; x++)
		{
			tenorNum = "tenor_" + x;
			tenorAmount = "tenor_" + x + "_amount";
			tenorDraftNum = "tenor_" + x + "_draft_number";

			if( (!StringFunction.isBlank(terms.getAttribute(tenorNum))     ) ||
					(!StringFunction.isBlank(terms.getAttribute(tenorAmount))  ) ||
					(!StringFunction.isBlank(terms.getAttribute(tenorDraftNum))))
			{
				//if Payment Terms typs is NOT Other ie mutli-Tenor then
				//if we have any data entered in the multi-tenor secton
				//- throw an error AND exit out of this method.
				if( !pmtTermsType.equals(TradePortalConstants.PMT_OTHER) )
				{
					terms.getErrorManager().issueError( TradePortalConstants.ERR_CAT_1,
							TradePortalConstants.PMT_TERM_NOT_VALID_FOR_TENOR);
					return;
				}

				//make sure there's a Tenor# entered
				if(StringFunction.isBlank(terms.getAttribute(tenorNum)) )
				{
					value = terms.getResourceManager().getText( "ExportCollectionIssue.Tenor" + x,
							TradePortalConstants.TEXT_BUNDLE);
					terms.getErrorManager().issueError( TradePortalConstants.ERR_CAT_1,
							AmsConstants.REQUIRED_ATTRIBUTE, value);
				}

				//Make sure there's a tenor amount entered
				if(StringFunction.isBlank(terms.getAttribute(tenorAmount)) )
				{
					value = terms.getResourceManager().getText( "ExportCollectionIssue.Tenor" + x,
							TradePortalConstants.TEXT_BUNDLE);
					value2 = terms.getResourceManager().getText( "ExportCollectionIssue.Amount",
							TradePortalConstants.TEXT_BUNDLE);
					value = value + " " + value2;
					terms.getErrorManager().issueError( TradePortalConstants.ERR_CAT_1,
							AmsConstants.REQUIRED_ATTRIBUTE, value);
				}else
				{
					totalAmount = totalAmount.add( terms.getAttributeDecimal( tenorAmount ));
				}
			}

		}

		//Verify that if Tenors were entered, that the total tenor amount matches the
		//saved Terms.amount attribute.
		if( pmtTermsType.equals(TradePortalConstants.PMT_OTHER) )
		{
			BigDecimal termsAmount = terms.getAttributeDecimal("amount");
			if( totalAmount.longValue() != termsAmount.longValue() )
			{
				terms.getErrorManager().issueError( TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.TENOR_AMOUNTS_NOT_EQUAL);
			}

			//Check to see that if Multi_Tenors are selected, verify that atleast 1
			//Tenor has been entered.
			if( totalAmount.longValue() == 0 )
			{
				terms.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.MULTI_TENOR_NOT_IND);
			}

		}

	}


	/**
	 * This method is written to be used by all sections that contain checkboxes.
	 * The idea is: if the check box is not checked, then blank out the associated attributes
	 * and return 0 because the check box was'nt checked. If the check box is checked, then
	 * look for a value in the associated attribute.  If that attribute is blank - throw an
	 * error and return 1 because the checkbox was turned on.
	 *
	 * @param terms com.ams.tradeportal.busobj.TermsBean
	 * @param attribute1 - the checkbox attribute name
	 * @param attribute2 - the value associated with the checkbox (attribute name).
	 * @errorType        - This is the specific error code that needs to be thrown( in the event )
	 * @param erorrParm  - This is the associated document that needs to be displayed
	 *                     as part of the error meesage.
	 * @return boolean   - indicates whether or not this check box was turned on.
	 */
	private boolean checkAssocCheckBoxValues(TermsBean terms, String attribute1,
			String attribute2,
			String errorType,
			String errorParm)
	throws java.rmi.RemoteException, AmsException
	{
		String value;

		value = terms.getAttribute( attribute1 );        //get the check box value

		if (StringFunction.isBlank(value) ||
				value.equals(TradePortalConstants.INDICATOR_NO))
		{
			terms.setAttribute( attribute2, "");
			return false;
		}else {                                         //if associated attribute is blank
			// VS IR LIUK031741938 01/20/11 Rel 7.0: update from isBlank to isBlankOrWhiteSpace
			if (StringFunction.isBlank(terms.getAttribute( attribute2 )))
			{
				if( errorParm != null)                  //determine which type of error to issue
				{
					value = terms.getResourceManager().getText( errorParm,
							TradePortalConstants.TEXT_BUNDLE);
					terms.getErrorManager().issueError( TradePortalConstants.ERR_CAT_1,
							errorType, value);
				}else
					terms.getErrorManager().issueError( TradePortalConstants.ERR_CAT_1,
							errorType );
			}
		}
		return true;               //if we get here then the check box must've been turned on.
	}




	/**
	 * The Payment Instruction section of the Export Collection document, requires only 1
	 * Validation really.  The problem is getting to the BankOrganizationGroup oid.
	 * The check requires looking for a phrase owned by the BOG with a phrase Category of
	 * Payment Instructions AND the phrase name is a partial match of the currency code in the
	 * current terms object (amount_currency_code) : if these conditions are met AND there's
	 * only one record in the DB found, then the Terms (payment_instructions) is set equal to
	 * the phrase text. <BR><BR> ** NOTE ** -1- If there are no matches found, we return and error.
	 * <BR> ** NOTE ** -2- If there are more than one match found then we return the user to the
	 * jsp with an error asking themn to make a selection in the Payment Instructions section.
	 * The funny thing is, this section only appears in the jsp if this error exists, Otherwise the
	 * assignment is made and the user never knows this all took place.
	 *
	 * @param terms com.ams.tradeportal.busobj.TermsBean
	 * @exception java.rmi.RemoteException
	 * @exception AmsException
	 */
	public void validatePaymentInstructions(TermsBean terms)
	throws java.rmi.RemoteException, AmsException

	{
		StringBuffer sql = new StringBuffer();
		QueryListView bogListView = null;
		QueryListView phraseListView = null;
		int numBOGs = 0;                             //number of returned BOG(s) records from ListView
		int numPhrasesFound = 0;                     //number of returned phrase records from ListView
		Long bogOid = null;                          //Oid of found BOG
		Long phraseOid = null;                       //Oid of found phrase		
		String yes = TradePortalConstants.INDICATOR_YES;
		String no  = TradePortalConstants.INDICATOR_NO;

		Phrase phraseFound = null;                   //Used only if 1 phrase is found

		JPylonProperties jPylonProperties = JPylonProperties.getInstance();
		String serverLocation             = jPylonProperties.getString("serverLocation");


		//Dont bother with this check if Payment Instruction is already defined
		if( StringFunction.isBlank( terms.getAttribute("payment_instructions") ) || TradePortalConstants.PMT_INSTR_CURR.equals(terms.getAttribute("pmt_instr_multiple_phrase")) )
		{
			try {
				bogListView = (QueryListView) EJBObjectFactory.createClientEJB(
						serverLocation, "QueryListView");

				//This sql statement is used to get the BOG OID from the terms...
				//Allthough this seems awkward, it was better than creating 5 seperate select
				//statements.  This has been tested, and does work. It's like doing several joins.
				//To do this we have to go through the Transaction, which takes us through
				//the Parent Instrument, which takes us to the Corporate Organization,
				//Which allows us to get to the Bank Organization Group.
				sql.append("select bank_organization_group.organization_oid ");
				sql.append(" from bank_organization_group, corporate_org, instrument, transaction, terms");
				sql.append(" where terms.terms_oid = ? AND");
				sql.append(" transaction.c_cust_enter_terms_oid = terms.terms_oid AND");
				sql.append(" transaction.p_instrument_oid = instrument.instrument_oid AND");
				sql.append(" instrument.a_corp_org_oid = corporate_org.organization_oid AND");
				sql.append(" corporate_org.a_bank_org_group_oid = bank_organization_group.organization_oid");
				bogListView.setSQL(sql.toString(), new Object[]{terms.getAttributeLong("terms_oid")});

				bogListView.getRecords();
				numBOGs = bogListView.getRecordCount();

				if( numBOGs > 0 )               //BOG's hava a 1-1 correspondance w/ the instrument
				{
					bogListView.scrollToRow(0);
					bogOid = new Long(bogListView.getRecordValue("organization_oid"));

					phraseListView = (QueryListView) EJBObjectFactory.createClientEJB(
							serverLocation, "QueryListView");
					//Now do the lookup in the DB based on the use case definition--
					//Get the Phrase-OID if: the BOG_OID matches what we found earlier AND
					// IF : the Phrase Category is Payment Instructions AND
					// IF : the Phrase Name partially matches the terms.currency code.
					sql = new StringBuffer();
					sql.append("select PHRASE_OID ");
					sql.append("from phrase ");
					sql.append("where P_OWNER_ORG_OID = ? AND ");
					sql.append("PHRASE_CATEGORY = ? AND ");
					sql.append("NAME LIKE ? ");
					phraseListView.setSQL(sql.toString(), bogOid.longValue(), TradePortalConstants.PHRASE_CAT_PMT_INST, terms.getAttribute("amount_currency_code") + "%");

					LOG.debug("Phrase Sql == {}" , sql);

					phraseListView.getRecords();
					numPhrasesFound = phraseListView.getRecordCount();  //Get number of records returned

					if( numPhrasesFound == 0 )                          //issueError
					{
						//Set indicator for re-showing the Paymt Instr Section in the Jsp...
						terms.setAttribute("pmt_instr_multiple_phrase", no);

						terms.getErrorManager().issueError(
								TradePortalConstants.ERR_CAT_1,
								TradePortalConstants.CONTACT_BANK_PMT_INSTR );

					}else if( numPhrasesFound == 1 )                    //Only 1 found - do assignment
					{
						//Set indicator for re-showing the Paymt Instr Section in the Jsp...
						terms.setAttribute("pmt_instr_multiple_phrase", no);

						phraseListView.scrollToRow(0);
						phraseOid = new Long(phraseListView.getRecordValue("PHRASE_OID"));
						phraseFound = (Phrase) EJBObjectFactory.createClientEJB( serverLocation,
								"Phrase", phraseOid.longValue());

						terms.setAttribute("payment_instructions", phraseFound.getAttribute("phrase_text"));

					}else if( numPhrasesFound > 1 )                     //issueError
					{
						//Set indicator for re-showing the Paymt Instr Section in the Jsp...
						terms.setAttribute("pmt_instr_multiple_phrase", yes);

						terms.getErrorManager().issueError(
								TradePortalConstants.ERR_CAT_1,
								TradePortalConstants.SELECT_PMT_INSTR );
						
					}

				}else //issueError - logically equivalant to having 0 Phrases defined for current currency
				{
					//Set indicator for re-showing the Paymt Instr Section in the Jsp...
					terms.setAttribute("pmt_instr_multiple_phrase", no);

					terms.getErrorManager().issueError(
							TradePortalConstants.ERR_CAT_1,
							TradePortalConstants.CONTACT_BANK_PMT_INSTR );
				}

				checkTextLength(terms, "payment_instructions", 500);

			} catch (Exception e) {
				e.printStackTrace();
			} finally {                         //Clean up objects
				try {
					if (bogListView != null) {
						bogListView.remove();
					}
					if (phraseListView != null) {
						phraseListView.remove();
					}
					if (phraseFound != null) {
						phraseFound.remove();
					}
				}catch (Exception e) {
					LOG.error("error removing querylistview in Export Collection - Issue.jsp",e);
				}
			}  
		}

	}


	//Beginning of code used by the Middleware team
	/**
	 *  gets the values of the attributes of the managed object. This method is called
	 *  from the getTermsAttributesDoc() hook in the managed object.
	 *
	 *  return com.amsinc.ecsg.util.DocumentHandler
	 *  @param terms TermsBean - the bean being validated
	 *  @param termsDoc com.amsinc.ecsg.util.DocumentHandler
	 */
	public DocumentHandler getManagerTerms(TermsBean terms, DocumentHandler termsDoc) throws java.rmi.RemoteException, AmsException

	{

		DocumentHandler termsValuesDoc = terms.populateXmlDoc(new DocumentHandler());
		LOG.debug("This is the termValuesDoc containing all the terms values ==> {}" , termsValuesDoc);


		//Terms_Details
		termsDoc.setAttribute("/Terms/TermsDetails/ReferenceNumber",	         termsValuesDoc.getAttribute("/Terms/reference_number"));
		termsDoc.setAttribute("/Terms/TermsDetails/AmountCurrencyCode",         termsValuesDoc.getAttribute("/Terms/amount_currency_code"));
		termsDoc.setAttribute("/Terms/TermsDetails/Amount",                     termsValuesDoc.getAttribute("/Terms/amount"));
		termsDoc.setAttribute("/Terms/TermsDetails/CaseOfNeedAuthority",        termsValuesDoc.getAttribute("/Terms/in_case_of_need_use_instruct"));
		termsDoc.setAttribute("/Terms/TermsDetails/CaseOfNeedText",             termsValuesDoc.getAttribute("/Terms/in_case_of_need_contact_text"));
		termsDoc.setAttribute("/Terms/TermsDetails/CaseOfNeedContact",          termsValuesDoc.getAttribute("/Terms/in_case_of_need_contact"));
		//Added PurposeType for CR 092 - AE 7/2/2002
		termsDoc.setAttribute("/Terms/TermsDetails/PurposeType",		        termsValuesDoc.getAttribute("/Terms/purpose_type"));


		//End of Terms_Details

		//Docs_Required
		int numberOfDocsReq    = 8;
		int drid               = 0;
		termsDoc.setAttribute("/Terms/DocumentsRequired/TotalNumberOfEntries", String.valueOf(drid));

		String [] indicator        = {"present_air_waybill","present_bill_of_lading", "present_cert_of_origin",
				"present_insurance", "present_non_neg_bill_of_lading","present_packing_list",
				"present_bills_of_exchange","present_commercial_invoice"};
		String [] DocumentName     = {"Air Waybill","Ocean Bill/Lading","Certificate of Origin",
				"Insurance Certificate","Non Negotiating Bill of Lading","Packaging List",
				"Bill of Exchange", "Commercial Invoice"};
		String [] NumberOfOriginalsCopies = {"present_num_air_waybill","present_num_bill_of_lading", "present_num_cert_of_origin",
				"present_num_insurance", "present_num_non_neg_bill_ladng","present_num_packing_list",
				"present_num_bills_of_exchange","present_num_commercial_invoice"};
		String originalsCopies = null;
		String originals = null;
		String copies = null;

		termsDoc.removeComponent("/Terms/DocumentsRequired/FullDocumentRequired");

		for (int docIndex=0; docIndex<numberOfDocsReq; docIndex++)
		{
			if(terms.getAttribute(indicator[docIndex]).equals("Y"))
			{
				originalsCopies = terms.getAttribute(NumberOfOriginalsCopies[docIndex]);
				DocumentName[docIndex] = originalsCopies + " " + DocumentName[docIndex];
				termsDoc.setAttribute("/Terms/DocumentsRequired/FullDocumentRequired(" + drid + ")/DocumentName",			DocumentName[docIndex]);
				termsDoc.setAttribute("/Terms/DocumentsRequired/FullDocumentRequired(" + drid + ")/DocumentText",			"");
				termsDoc.setAttribute("/Terms/DocumentsRequired/FullDocumentRequired(" + drid + ")/NumberOfCopies",		"");
				termsDoc.setAttribute("/Terms/DocumentsRequired/FullDocumentRequired(" + drid + ")/NumberOfOriginals",		"");

				drid ++;

			}
		}

		String presentOtherText = null;
		if(terms.getAttribute("present_other").equals("Y"))
		{
			presentOtherText = terms.getAttribute("present_other_text");
			if(presentOtherText.length()>30)
				termsDoc.setAttribute("/Terms/DocumentsRequired/FullDocumentRequired(" + drid + ")/DocumentName",			presentOtherText.substring(0,29));
			else
				termsDoc.setAttribute("/Terms/DocumentsRequired/FullDocumentRequired(" + drid + ")/DocumentName",			presentOtherText);
			termsDoc.setAttribute("/Terms/DocumentsRequired/FullDocumentRequired(" + drid + ")/DocumentText",			presentOtherText);
			termsDoc.setAttribute("/Terms/DocumentsRequired/FullDocumentRequired(" + drid + ")/NumberOfCopies",		"");
			termsDoc.setAttribute("/Terms/DocumentsRequired/FullDocumentRequired(" + drid + ")/NumberOfOriginals",		"");
			drid++;
		}

		termsDoc.setAttribute("/Terms/DocumentsRequired/TotalNumberOfEntries", String.valueOf(drid));

		//End of DocsRequired

		//TransportDocuments
		//End of Transport_Documents

		// ShipmentTerms
		populateFirstShipmentTermsXml(terms, termsDoc);

		//Instructions
		termsDoc.setAttribute("/Terms/Instructions/InstructionsToBank",			termsValuesDoc.getAttribute("/Terms/special_bank_instructions"));
		//CollectionInstructions
		termsDoc.setAttribute("/Terms/Instructions/CollectionInstructions/CollectionChargesType",			termsValuesDoc.getAttribute("/Terms/collection_charges_type"));
		termsDoc.setAttribute("/Terms/Instructions/CollectionInstructions/CollectionChargesIncludeText",			termsValuesDoc.getAttribute("/Terms/collection_charges_includ_txt"));
		termsDoc.setAttribute("/Terms/Instructions/CollectionInstructions/DoNotWaiveRefusedCharges",			termsValuesDoc.getAttribute("/Terms/do_not_waive_refused_charges"));
		termsDoc.setAttribute("/Terms/Instructions/CollectionInstructions/DoNotWaiveRefusedInterest",			termsValuesDoc.getAttribute("/Terms/do_not_waive_refused_interest"));
		termsDoc.setAttribute("/Terms/Instructions/CollectionInstructions/CollectInterestAtIndicator",			termsValuesDoc.getAttribute("/Terms/collect_interest_at_indicator"));
		termsDoc.setAttribute("/Terms/Instructions/CollectionInstructions/CollectInterestAt",			termsValuesDoc.getAttribute("/Terms/collect_interest_at"));
		termsDoc.setAttribute("/Terms/Instructions/CollectionInstructions/InstructAdviseAcceptByTel",			termsValuesDoc.getAttribute("/Terms/instr_advise_accept_by_telco"));
		termsDoc.setAttribute("/Terms/Instructions/CollectionInstructions/InstructAdviseNonAcceptByTel",			termsValuesDoc.getAttribute("/Terms/instr_advise_non_accept_by_tel"));
		termsDoc.setAttribute("/Terms/Instructions/CollectionInstructions/InstructAdvisePaymentByTel",			termsValuesDoc.getAttribute("/Terms/instr_advise_pmt_by_telco"));
		termsDoc.setAttribute("/Terms/Instructions/CollectionInstructions/InstructAdviseNonPaymentByTel",			termsValuesDoc.getAttribute("/Terms/instr_advise_non_pmt_by_telco"));
		termsDoc.setAttribute("/Terms/Instructions/CollectionInstructions/InstructNoteForNonAcceptance",			termsValuesDoc.getAttribute("/Terms/instr_note_for_non_accept"));
		termsDoc.setAttribute("/Terms/Instructions/CollectionInstructions/InstructNoteForNonPayment",			termsValuesDoc.getAttribute("/Terms/instr_note_for_non_pmt"));
		termsDoc.setAttribute("/Terms/Instructions/CollectionInstructions/InstructOther",			termsValuesDoc.getAttribute("/Terms/instr_other"));
		termsDoc.setAttribute("/Terms/Instructions/CollectionInstructions/InstructProtestforNonAcceptance",			termsValuesDoc.getAttribute("/Terms/instr_protest_for_non_accept"));
		termsDoc.setAttribute("/Terms/Instructions/CollectionInstructions/InstructProtestForNonPayment",			termsValuesDoc.getAttribute("/Terms/instr_protest_for_non_pmt"));
		termsDoc.setAttribute("/Terms/Instructions/CollectionInstructions/InstructReleaseDocumentOnAcceptance",			termsValuesDoc.getAttribute("/Terms/instr_release_doc_on_accept"));
		termsDoc.setAttribute("/Terms/Instructions/CollectionInstructions/InstructReleaseDocumentOnPayment",			termsValuesDoc.getAttribute("/Terms/instr_release_doc_on_pmt"));
		//End of CollectionInstructions

		//CommissionAndCharges
		termsDoc.setAttribute("/Terms/Instructions/CommissionAndCharges/CommissionChargesOurAccountNumber",			termsValuesDoc.getAttribute("/Terms/coms_chrgs_our_account_num"));
		termsDoc.setAttribute("/Terms/Instructions/CommissionAndCharges/CommissionChargesForeignAccountNumber",			termsValuesDoc.getAttribute("/Terms/coms_chrgs_foreign_acct_num"));
		termsDoc.setAttribute("/Terms/Instructions/CommissionAndCharges/CommissionChargesForeignAccountCurrency",	termsValuesDoc.getAttribute("/Terms/coms_chrgs_foreign_acct_curr"));
		termsDoc.setAttribute("/Terms/Instructions/CommissionAndCharges/CommissionChargesOtherText",			termsValuesDoc.getAttribute("/Terms/other_settlement_instructions"));
		termsDoc.setAttribute("/Terms/Instructions/CommissionAndCharges/FinanceDrawing",				termsValuesDoc.getAttribute("/Terms/finance_drawing"));
		termsDoc.setAttribute("/Terms/Instructions/CommissionAndCharges/FinanceDrawingNumberOfDays",			termsValuesDoc.getAttribute("/Terms/finance_drawing_num_of_days"));
		termsDoc.setAttribute("/Terms/Instructions/CommissionAndCharges/FinanceDrawingType",			termsValuesDoc.getAttribute("/Terms/finance_drawing_type"));
		termsDoc.setAttribute("/Terms/Instructions/CommissionAndCharges/CoveredByFECNumber",			termsValuesDoc.getAttribute("/Terms/covered_by_fec_number"));
		termsDoc.setAttribute("/Terms/Instructions/CommissionAndCharges/FECAmount",				termsValuesDoc.getAttribute("/Terms/fec_amount"));
		termsDoc.setAttribute("/Terms/Instructions/CommissionAndCharges/FECRate",				termsValuesDoc.getAttribute("/Terms/fec_rate"));
		termsDoc.setAttribute("/Terms/Instructions/CommissionAndCharges/FECMaturityDate",			termsValuesDoc.getAttribute("/Terms/fec_maturity_date"));
		//End of CommissionCharges

		//Settlement Instructions
		termsDoc.setAttribute("/Terms/Instructions/SettlementInstructions/BranchCode",			                termsValuesDoc.getAttribute("/Terms/branch_code"));
		termsDoc.setAttribute("/Terms/Instructions/SettlementInstructions/SettlementOurAccountNumber",			termsValuesDoc.getAttribute("/Terms/settlement_our_account_num"));

		//End of Instructions

		//PaymentTerms
		termsDoc.setAttribute("/Terms/PaymentTerms/PaymentTermsType",			termsValuesDoc.getAttribute("/Terms/pmt_terms_type"));
		termsDoc.setAttribute("/Terms/PaymentTerms/PaymentTermsNumberDaysAfter",			termsValuesDoc.getAttribute("/Terms/pmt_terms_num_days_after"));
		termsDoc.setAttribute("/Terms/PaymentTerms/PaymentTermsDaysAfterType",			termsValuesDoc.getAttribute("/Terms/pmt_terms_days_after_type"));
		termsDoc.setAttribute("/Terms/PaymentTerms/PaymentTermsPercentInvoice",			termsValuesDoc.getAttribute("/Terms/pmt_terms_percent_invoice"));
		termsDoc.setAttribute("/Terms/PaymentTerms/PaymentTermsFixedMaturityDate",			termsValuesDoc.getAttribute("/Terms/pmt_terms_fixed_maturity_date"));
		//End of PaymentTerms

		//TenorTerms
		termsDoc.setAttribute("/Terms/TermsDetails/TenorTerms/Tenor1Amount",			termsValuesDoc.getAttribute("/Terms/tenor_1_amount"));
		termsDoc.setAttribute("/Terms/TermsDetails/TenorTerms/Tenor2Amount",			termsValuesDoc.getAttribute("/Terms/tenor_2_amount"));
		termsDoc.setAttribute("/Terms/TermsDetails/TenorTerms/Tenor3Amount",			termsValuesDoc.getAttribute("/Terms/tenor_3_amount"));
		termsDoc.setAttribute("/Terms/TermsDetails/TenorTerms/Tenor4Amount",			termsValuesDoc.getAttribute("/Terms/tenor_4_amount"));
		termsDoc.setAttribute("/Terms/TermsDetails/TenorTerms/Tenor5Amount",			termsValuesDoc.getAttribute("/Terms/tenor_5_amount"));
		termsDoc.setAttribute("/Terms/TermsDetails/TenorTerms/Tenor6Amount",			termsValuesDoc.getAttribute("/Terms/tenor_6_amount"));
		termsDoc.setAttribute("/Terms/TermsDetails/TenorTerms/Tenor1",			termsValuesDoc.getAttribute("/Terms/tenor_1"));
		termsDoc.setAttribute("/Terms/TermsDetails/TenorTerms/Tenor2",			termsValuesDoc.getAttribute("/Terms/tenor_2"));
		termsDoc.setAttribute("/Terms/TermsDetails/TenorTerms/Tenor3",			termsValuesDoc.getAttribute("/Terms/tenor_3"));
		termsDoc.setAttribute("/Terms/TermsDetails/TenorTerms/Tenor4",			termsValuesDoc.getAttribute("/Terms/tenor_4"));
		termsDoc.setAttribute("/Terms/TermsDetails/TenorTerms/Tenor5",			termsValuesDoc.getAttribute("/Terms/tenor_5"));
		termsDoc.setAttribute("/Terms/TermsDetails/TenorTerms/Tenor6",			termsValuesDoc.getAttribute("/Terms/tenor_6"));
		termsDoc.setAttribute("/Terms/TermsDetails/TenorTerms/TenorDraftNumber1",			termsValuesDoc.getAttribute("/Terms/tenor_1_draft_number"));
		termsDoc.setAttribute("/Terms/TermsDetails/TenorTerms/TenorDraftNumber2",			termsValuesDoc.getAttribute("/Terms/tenor_2_draft_number"));
		termsDoc.setAttribute("/Terms/TermsDetails/TenorTerms/TenorDraftNumber3",			termsValuesDoc.getAttribute("/Terms/tenor_3_draft_number"));
		termsDoc.setAttribute("/Terms/TermsDetails/TenorTerms/TenorDraftNumber4",			termsValuesDoc.getAttribute("/Terms/tenor_4_draft_number"));
		termsDoc.setAttribute("/Terms/TermsDetails/TenorTerms/TenorDraftNumber5",			termsValuesDoc.getAttribute("/Terms/tenor_5_draft_number"));
		termsDoc.setAttribute("/Terms/TermsDetails/TenorTerms/TenorDraftNumber6",			termsValuesDoc.getAttribute("/Terms/tenor_6_draft_number"));
		//End of TenorTerms

		//BankCharges
		termsDoc.setAttribute("/Terms/BankCharges/BankChargesType",			termsValuesDoc.getAttribute("/Terms/bank_charges_type"));
		//End of BankCharges
		//Terms Party

		DocumentHandler termsPartyDoc = new DocumentHandler();

		//Additional Terms Party
		String testTP = TermsPartyType.DRAWER_SELLER;


		int id = 0;

		termsPartyDoc.setAttribute("/TermsParty/TotalNumberOfEntries","0");
		String countryOfCOL = "";
		for (int i = 0; i<TradePortalConstants.MAX_NUM_TERMS_PARTIES;i++)
		{
			String termsPartyOID = termsValuesDoc.getAttribute("/Terms/"+TradePortalConstants.TERMS_PARTY_ATTRIBUTE_NAMES[i]);

			if(!(termsPartyOID == null || termsPartyOID.equals("")))
			{
				TermsParty termsParty = (TermsParty)(terms.getComponentHandle(TradePortalConstants.TERMS_PARTY_COMPONENT_NAMES[i]));
				if(termsParty.getAttribute("terms_party_type").equals(testTP))
				{
					LOG.debug("party_Type is APP, hence I need to check for thirdParty");
					termsPartyDoc = packageAdditionalTermsParty(terms,id,termsPartyDoc,termsParty);
					if(termsPartyDoc.getAttribute("/AdditionalTermsParty").equals("Y"))
						id++;
				}
				termsPartyDoc.setAttribute("/AdditionalTermsParty","N");
				//If the termsValuesDoc contains termsParty also need to modify the line below
				if(termsParty.getAttribute("terms_party_type").equals(TermsPartyType.COLLECTING_BANK))
					countryOfCOL = termsParty.getAttribute("address_country");
				if(termsParty.getAttribute("terms_party_type").equals(TermsPartyType.CASE_OF_NEED))
					termsParty.setAttribute("address_country",countryOfCOL);
				termsPartyDoc = termsParty.packageTermsPartyAttributes(id,termsPartyDoc);
				id++;
			}
		}

		termsPartyDoc.setAttribute("/TermsParty/TotalNumberOfEntries",String.valueOf(id));
		termsDoc.setComponent("/TermsParty",termsPartyDoc.getFragment("/TermsParty"));


		return termsDoc;
	}

	//End of Code by Indu Valavala

	/**
	 * Returns the Transaction Status Date from the TermsBean as a Calendar object
	 *
	 * @param terms TermsBean
	 * @return The Transaction Status Date as a Calendar object or null if the Transaction Status Date is either less than 10 characters or null
	 * @throws AmsException
	 * @throws RemoteException
	 */
	private Calendar getTransactionStatusDateAsCalendar(TermsBean terms) throws AmsException, RemoteException
	{
		String sqlQuery = "select transaction_status_date from transaction where c_cust_enter_terms_oid = ?";
		DocumentHandler results = DatabaseQueryBean.getXmlResultSet(sqlQuery, true, new Object[]{terms.getAttributeLong("terms_oid")});

		if(results != null)
		{
			String transactionStatusDateAsString = results.getAttribute("/ResultSetRecord(0)/TRANSACTION_STATUS_DATE");
			if((transactionStatusDateAsString != null)
				&& (transactionStatusDateAsString.length() >= 10))
			{
				// The query returns the date in the following format: YYYY-MM-DD
				// 00000000001
				// 01234567890
				// YYYY-MM-DD
				Integer year = new Integer(transactionStatusDateAsString.substring(0,4));
				Integer month = new Integer(transactionStatusDateAsString.substring(5,7));
				Integer date = new Integer(transactionStatusDateAsString.substring(8,10));


				Calendar transactionStatusDateAsCalendar = Calendar.getInstance();
				transactionStatusDateAsCalendar.set(year.intValue(), month.intValue(), date.intValue());

				return transactionStatusDateAsCalendar;
			} 
			  
		} 
		return null;
	} 

	/**
	 * Returns the Fixed Maturity Date from the TermsBean as a Calendar object
	 *
	 * @param terms TermsBean
	 * @return The Fixed Maturity Date as a Calendar object or null if the pmt_terms_fixed_maturity_date field is less than 10 characters
	 * @throws AmsException
	 * @throws RemoteException
	 */
	private Calendar getFixedMaturityDateAsCalendar(TermsBean terms) throws AmsException, RemoteException
	{
		if(terms.getAttribute("pmt_terms_fixed_maturity_date").length() >= 10)
		{
			// terms.getAttribute(String) returns the Fixed Maturity Date in the following format: MM/DD/YYYY
			// 00000000001
			// 01234567890
			// MM/DD/YYYY
			String fixedMaturityDateAsString = terms.getAttribute("pmt_terms_fixed_maturity_date");

			Integer year = new Integer(fixedMaturityDateAsString.substring(6,10));
			Integer month = new Integer(fixedMaturityDateAsString.substring(0,2));
			Integer date = new Integer(fixedMaturityDateAsString.substring(3,5));

			Calendar fixedMaturityDateAsCalendar = Calendar.getInstance();
			fixedMaturityDateAsCalendar.set(year.intValue(), month.intValue(), date.intValue());

			return fixedMaturityDateAsCalendar;
		} 

		return null;
	} 
}