package com.ams.tradeportal.busobj.termsmgr;

import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.StringTokenizer;
import java.util.Vector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.busobj.CorporateOrganization;
import com.ams.tradeportal.busobj.ShipmentTerms;
import com.ams.tradeportal.busobj.TermsBean;
import com.ams.tradeportal.busobj.TermsParty;
import com.ams.tradeportal.busobj.util.InstrumentServices;
import com.ams.tradeportal.busobj.webbean.TermsWebBean;
import com.ams.tradeportal.common.InstrumentType;
import com.ams.tradeportal.common.TPCurrencyUtility;
import com.ams.tradeportal.common.TermsPartyType;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.Attribute;
import com.amsinc.ecsg.frame.AttributeManager;
import com.amsinc.ecsg.frame.ComponentProcessingException;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.frame.EJBObjectFactory;
import com.amsinc.ecsg.frame.ErrorManager;
import com.amsinc.ecsg.frame.ReferenceDataManager;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.JPylonProperties;
import com.amsinc.ecsg.util.ResourceManager;
import com.amsinc.ecsg.util.StringFunction;

/**
 * This class manages the dynamic attribute registration and validation
 * for a Terms business object or TermsWebBean.  Each terms bean has an instance
 * of a subclass of TermsMgr.   The subclass that the object has
 * has an instance of is dependent on the transaction type and / or the instrument type.
 * to which the terms belong.
 *
 * For terms that represent customer entered terms,
 *     the subclasses of TermsMgr must be named as follows:
 *          {instrument type code}__{transaction type code}TermsMgr
 *
 * For terms that represent bank released terms,
 *     the subclasses of TermsMgr must be named as follows:
 *          {transaction type code}BankRelTermsMgr
 *
 * There may be exceptions to this rule, which will be documented in the
 * getSubClassName() method below.
 *
 * For example, the subclass that will be used to manage the terms of
 * an Issue Import DLC transaction will be named:
 *
 *    IMP_DLC__ISSTermsMgr
 *
 * Since the bean being managed only has a reference to TermsMgr (even though
 * the manager is really a subclass of TermsMgr), any methods to be called
 * on the subclasses must appear on this class.
 *
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public abstract class TermsMgr implements java.io.Serializable
 {
	private static final Logger LOG = LoggerFactory.getLogger(TermsMgr.class);

	// When a checkbox is cleared and certain related values are blanked out
	// in the terms object (on verification), we sometimes have to delete a
	// terms party as well.  We will collect these components in a vector
	// and the Terms object itself will perform the delete (has to be this
	// way for correct transactional processing.)
	private Vector partiesToDelete = new Vector(5);

  	/**
	 *  Static method used to determine the appropriate TermsMgr subclass
	 *  to use for a particular situation.   This returns the fully qualifed
	 *  class name of the TermsMgr subclass
	 *
	 *  @param instumentType String - instrument type of the transaction
	 *  @param transactionType String  - transaction type of the transaction
	 *  @param termsSourceType String  - the 'terms source' - either CustomerEnteredTerms or BankReleasedTerms
	 *  @return String  - fully qualified class name
	 */
	public static String getSubClassName(String instrumentType, String transactionType,
	                                     String termsSourceType, String standbyUsingGuaranteeForm)
	 {


		// Holder for name of the manager object
	    StringBuffer termsMgrSubClass = new StringBuffer();

	    // Append TermsMgr Package Name
	    termsMgrSubClass.append(TermsMgr.class.getName().substring(0 , TermsMgr.class.getName().indexOf("TermsMgr")));

            // If the caller wants all attributes, give them the proper terms mgr for that
            if(instrumentType.equals(TradePortalConstants.ALL_ATTRIBUTES))
            {
              return termsMgrSubClass.append("AllAttributesTermsMgr").toString();
            }

            if (termsSourceType.equals("BankReleasedTerms"))
             {
              termsMgrSubClass.append(transactionType);
              termsMgrSubClass.append("BankRel");
             }
            else
	     {
	        // There are some instances where transactions that have an instrument type
	        // of Standby LC will use the Guarantee form, and thus also use the guarantee
	        // termsMgr class.  Whether or not to use the guarantee form is driven by an
	        // indicator attribute stored on transaction that is placed into the CSDB
			// then passed into this method.
	        if( (standbyUsingGuaranteeForm != null) &&
	            standbyUsingGuaranteeForm.equals(TradePortalConstants.INDICATOR_YES) &&
	            instrumentType.equals(InstrumentType.STANDBY_LC)  )
	         {
	            // This is a standby LC and the indicator is set, so ignore the
	            // instrument type and use guarantee instead
	            termsMgrSubClass.append(InstrumentType.GUARANTEE);
	         }
	        else
	         {
	            // No special rules apply - just use the instrument type
 		        termsMgrSubClass.append(instrumentType);
 		     }

		  termsMgrSubClass.append("__");

	 	  termsMgrSubClass.append(transactionType);
	     }

	     termsMgrSubClass.append("TermsMgr");

	     LOG.debug("*** Using the following terms manager class: {}",termsMgrSubClass.toString());

		 return termsMgrSubClass.toString();
	 }



	/**
	 *  Constructor that is used by business objects to create their
	 *  manager.  This method also register the dynamic attributes of
	 *  the bean.  TermsMgr is an abstract class, so this constructor is
	 *  never used directly.  Instead, it is called from the subclasses
	 *  of TermsMgr.
	 *
	 *  @param mgr AttributeManager attribute manager of the bean being managed.  Used to register attributes
	 */
	public TermsMgr(AttributeManager mgr) throws AmsException
	 {
		// Obtain a list of attributes to register
	    // Loop through, registering each attribute with the attribute manager
	    for (Attribute attr : getAttributesToRegister())
	        mgr.registerAttribute(attr);
	 }

	/**
	 *  Constructor that is used by web beans to create their
	 *  manager.  This method also register the dynamic attributes of
	 *  the bean.  TermsMgr is an abstract class, so this constructor is
	 *  never used directly.  Instead, it is called from the subclasses
	 *  of TermsMgr.
	 *
	 *  @param mgr TermsWebBean - the bean being managed.  Used to register attributes.
	 */
	public TermsMgr(TermsWebBean terms) throws AmsException
	 {
		// Obtain a list of attributes to register
 	    // Loop through, registering each attribute with the attribute manager
 	    for (Attribute attr: getAttributesToRegister())   {
 	        if(attr.getAttributeType() != null)
		        terms.registerAttribute(attr.getAttributeName(),
 	                                    attr.getPhysicalName(),
 	                                    attr.getAttributeType());
 	        else
		        terms.registerAttribute(attr.getAttributeName(),
 	                                    attr.getPhysicalName());
	    }
	 }

	/**
	 *  Returns a list of Attributes.  These describe each of the attributes
	 *  that will be registered for the bean being managed.  This method will be
	 *  overriden in the subclasses of DynamicAttributeBeanManager, allowing each specific type
	 *  to specify their own set of attributes. Each of the overriden methods must
	 *  call super.getAttributesToRegister to obtain the attributes specified in the superclasses.
	 *
	 *  @return List - list of attributes
	 */
	public List<Attribute> getAttributesToRegister()
	 {
		// Create a vector to store the attributes.  All of the overriden versions of this
		// method will use this vector to add their attributes to.
	    return new ArrayList<>(30);
	 }

	/**
	 *  Performs validation of the attributes of the managed object.   This method is called
	 *  from the userValidate() hook in the managed object.
	 *
	 *  @param terms TermsBean - the bean being validated
	 */
	public void validate(TermsBean terms)
			throws java.rmi.RemoteException, AmsException
         {
             // Perform subclass-specific validation of text fields
             // for SWIFT characters
             validateForSwiftCharacters(terms);
         }

         /**
          * Performs the Terms Manager-specific validation of text fields in the Terms
          * and TermsParty objects.  For some terms managers, this will never be overriden
          * and thus nothing will happen.   Others will override this method to indicate
          * which text fields to exclude from checking and whether or not Terms Parties
          * should be validated.
          *
          * @param terms - the managed object
          *
          */
         protected void validateForSwiftCharacters(TermsBean terms) throws AmsException, RemoteException
         {

         }

         /**
          * This method validates all of the text attributes on each of the TermsParties
          * that are components of the managed object.
          *
          * @param terms - the managed object
          * @param componentIdentifier - the component ID of the terms party to check
          * @param partyTypeResourceKey - text resource key that identifies the terms party (i.e. "Drawee's Bank")
          *
          */
       public void validateTermsPartyForSwiftChars(TermsBean terms, String componentIdentifier, String partyTypeResourceKey) throws AmsException, RemoteException
       {
             try
             {
		    // acct_choices is an internal attribute and consists of XML data -- this is definitely
		    // not valid SWIFT chars so it must be excluded.
            	 //T36000021753 - excluded OTL_customer_id
	          final String[] attributesToExclude = {"acct_choices","OTL_customer_id"};

                TermsParty termsParty = (TermsParty) terms.getComponentHandle(componentIdentifier);
                String termsPartyName = terms.getResourceManager().getText(partyTypeResourceKey, TradePortalConstants.TEXT_BUNDLE);
                InstrumentServices.checkForInvalidSwiftCharacters(termsParty.getAttributeHash(),
											terms.getErrorManager(),
											attributesToExclude,
											termsPartyName);
             }
             catch(ComponentProcessingException cpe)
             {
                 // Component terms party does not exist... nothing to validate
             }
       }

         /**
          * This method validates all of the text attributes on each of the TermsParties
          * that are components of the managed object.
          *
          * @param shipmentTerms - the managed object
          * @param componentIdentifier - the component ID of the terms party to check
          * @param partyTypeResourceKey - text resource key that identifies the terms party (i.e. "Drawee's Bank")
          *
          */
       public void validateTermsPartyForSwiftChars(ShipmentTerms shipmentTerms, String componentIdentifier, String partyTypeResourceKey) throws AmsException, RemoteException
       {
             try
             {
		    // acct_choices is an internal attribute and consists of XML data -- this is definitely
		    // not valid SWIFT chars so it must be excluded.
	          final String[] attributesToExclude = {"acct_choices"};

                TermsParty termsParty = (TermsParty) shipmentTerms.getComponentHandle(componentIdentifier);
                String termsPartyName = shipmentTerms.getResourceMgr().getText(partyTypeResourceKey, TradePortalConstants.TEXT_BUNDLE);
                InstrumentServices.checkForInvalidSwiftCharacters(termsParty.getAttributeHash(),
											shipmentTerms.getErrorManager(),
											attributesToExclude,
											termsPartyName);
             }
             catch(ComponentProcessingException cpe)
             {
                 // Component terms party does not exist... nothing to validate
             }
       }


	/**
	 *  gets the values of the attributes of the managed object. This method is called
	 *  from the getTermsAttributesDoc() hook in the managed object.
	 *
	 *  return com.amsinc.ecsg.util.DocumentHandler
	 *  @param terms TermsBean - the bean being validated
	 *  @param termsDoc com.amsinc.ecsg.util.DocumentHandler
	 */

	public DocumentHandler getManagerTerms(TermsBean terms, DocumentHandler termsDoc)
			throws java.rmi.RemoteException, AmsException
			{
				return(new DocumentHandler());
			}


	/**
	 *  sets the values of the attributes of the managed object. This method is called
	 *  from the setTermsAttributesFromDoc() hook in the managed object.
	 *
	 *  return com.amsinc.ecsg.util.DocumentHandler
	 *  @param terms TermsBean - the bean being validated
	 *  @param termsDoc com.amsinc.ecsg.util.DocumentHandler
	 */
	public boolean setManagerTerms(TermsBean terms, DocumentHandler inputDoc)
		    throws java.rmi.RemoteException, AmsException
			{
				return true;
			}

    public boolean setManagerTermsForWorkItem(TermsBean terms, DocumentHandler inputDoc)
                               throws java.rmi.RemoteException, AmsException
	{
			return true;
	}

  /**
   * Checks if the additionalTermsParty values have to be packaged, If yes
   * populates the termsPartyDoc with the additional TermsParty values
   * @param terms com.ams.tradeportal.busobj.termsBean
   * @param tpid int
   * @param termsPartyDoc com.amsinc.ecsg.util.DocumentHandler
   * @param termsParty com.ams.tradeportal.busobj.termsPartyBean
   */
  public DocumentHandler packageAdditionalTermsParty(TermsBean terms,int tpid, DocumentHandler termsPartyDoc, TermsParty termsParty)
																						  throws RemoteException, AmsException
 	   {
 	    	String terms_oid             = null;
 	    	String tp_otl_customer_id    = null;
 	    	//String ejbServerLocation     = null;

			JPylonProperties jPylonProperties = JPylonProperties.getInstance();
		    String serverLocation             = jPylonProperties.getString("serverLocation");
			tp_otl_customer_id = termsParty.getAttribute("OTL_customer_id");

 	    	terms_oid = terms.getAttribute("terms_oid");


 	   	    String corpOrgSQL ="SELECT ORGANIZATION_OID,PROPONIX_CUSTOMER_ID FROM CORPORATE_ORG WHERE ORGANIZATION_OID IN " +
								 " (SELECT A_CORP_ORG_OID FROM INSTRUMENT WHERE INSTRUMENT_OID IN "+
								  " (SELECT P_INSTRUMENT_OID FROM TRANSACTION WHERE TRANSACTION_OID IN "+
								   " (SELECT TRANSACTION_OID FROM TRANSACTION, TERMS "+
									" WHERE TERMS.TERMS_OID= TRANSACTION.C_CUST_ENTER_TERMS_OID "+
									" AND TERMS_OID=?)))";


 	       DocumentHandler resultsDoc = DatabaseQueryBean.getXmlResultSet(corpOrgSQL,false,new Object[]{terms_oid});
 	       int rid = 0;
 	       String proponix_customer_id = resultsDoc.getAttribute("/ResultSetRecord(" + rid + ")/PROPONIX_CUSTOMER_ID");
 	       String scorp_org_oid        = resultsDoc.getAttribute("/ResultSetRecord(" + rid + ")/ORGANIZATION_OID");
		   Long corp_org_oid = new Long(scorp_org_oid );



 	       LOG.debug("This is the tp_otl_customer_id: {}" , tp_otl_customer_id);

 	       if(!(proponix_customer_id.equals(tp_otl_customer_id)))
 	       {
			   CorporateOrganization corpOrg	= (CorporateOrganization) EJBObjectFactory.createClientEJB
			   	   	                                        (serverLocation,"CorporateOrganization",corp_org_oid.longValue());


 	   				LOG.debug("LCIPackager: termsParty.terms_party_type  : {}" , termsParty.getAttribute("terms_party_type"));

 	   				termsPartyDoc.setAttribute("/TermsParty/FullTermsParty(" + tpid + ")/TermsPartyType",    TermsPartyType.THIRD_PARTY);
 	   				termsPartyDoc.setAttribute("/TermsParty/FullTermsParty(" + tpid + ")/Name",                  corpOrg.getAttribute("name"));
 	   				termsPartyDoc.setAttribute("/TermsParty/FullTermsParty(" + tpid + ")/AddressLine1",          corpOrg.getAttribute("address_line_1"));
 	   				termsPartyDoc.setAttribute("/TermsParty/FullTermsParty(" + tpid + ")/AddressLine2",          corpOrg.getAttribute("address_line_2"));
 	   				termsPartyDoc.setAttribute("/TermsParty/FullTermsParty(" + tpid + ")/AddressLine3",                                              "");
 	   				termsPartyDoc.setAttribute("/TermsParty/FullTermsParty(" + tpid + ")/AddressCity",           corpOrg.getAttribute("address_city"));
 	   				termsPartyDoc.setAttribute("/TermsParty/FullTermsParty(" + tpid + ")/AddressStateProvince",  corpOrg.getAttribute("address_state_province"));
 	   				termsPartyDoc.setAttribute("/TermsParty/FullTermsParty(" + tpid + ")/AddressCountry",        corpOrg.getAttribute("address_country"));
 	   				termsPartyDoc.setAttribute("/TermsParty/FullTermsParty(" + tpid + ")/AddressPostalCode",     corpOrg.getAttribute("address_postal_code"));
 	   				termsPartyDoc.setAttribute("/TermsParty/FullTermsParty(" + tpid + ")/PhoneNumber",                                                "");
 	   				termsPartyDoc.setAttribute("/TermsParty/FullTermsParty(" + tpid + ")/FaxNumber1",           "");
 	   				termsPartyDoc.setAttribute("/TermsParty/FullTermsParty(" + tpid + ")/ContactName",                                                "");
 	   				termsPartyDoc.setAttribute("/TermsParty/FullTermsParty(" + tpid + ")/OTLCustomerID",         corpOrg.getAttribute("Proponix_customer_id"));
 	                termsPartyDoc.setAttribute("/TermsParty/FullTermsParty(" + tpid + ")/OTLAddressSequenceNum", "");
 	                termsPartyDoc.setAttribute("/AdditionalTermsParty", "Y");

 	           try
 	            {
 	               corpOrg.remove();
 	            }
 	           catch(javax.ejb.RemoveException re)
 	            {
 	               LOG.error("Remove exception in  packageAdditionalTermsParty",re);
 	            }

 	       	}
 	     	else
 	   	    {
 	   			//Nothing to be done
 	   			termsPartyDoc.setAttribute("/AdditionalTermsParty", "N");

 	   	     }

 	     return termsPartyDoc;

	   }



 /**
 * Validates the length of a text attribute.  If the text is more than the
 * allowed length, an error is issued indicating where the text should end
 *
 * @param terms com.ams.tradeportal.busobj.termsBean
 * @param attributeName String - The name of the attribute in terms
 * @param allowedLength int - the max allowed length for the text
 */
public void checkTextLength(TermsBean terms, String attributeName, int allowedLength)
	throws RemoteException, AmsException {

	String text;
	String endText;

	text = terms.getAttribute(attributeName);

	if (text.length() > allowedLength) {
		endText = text.substring(allowedLength-20, allowedLength);
		terms.getErrorManager().issueError (
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.TEXT_TOO_LONG_SHOULD_END,
					terms.getAlias(attributeName),
					endText, "");
	}

 }

 /**
 * Validates the length of a text attribute.  If the text is more than the
 * allowed length, an error is issued indicating where the text should end
 *
 * @param terms com.ams.tradeportal.busobj.ShipmentTerms
 * @param attributeName String - The name of the attribute in terms
 * @param allowedLength int - the max allowed length for the text
 */
public void checkTextLength(ShipmentTerms shipmentTerms, String attributeName, int allowedLength)
	throws RemoteException, AmsException
 {
   checkTextLength(shipmentTerms, "", attributeName, allowedLength);
 }



 /**
 * Validates the length of a text attribute.  If the text is more than the
 * allowed length, an error is issued indicating where the text should end
 *
 * @param terms com.ams.tradeportal.busobj.TermsBean - needed to issue an error on
 * @param shipmentTerms com.ams.tradeportal.busobj.ShipmentTerms
 * @param attributeName String - The name of the attribute in terms
 * @param allowedLength int - the max allowed length for the text
 */
public void checkTextLength(ShipmentTerms shipmentTerms, String shipmentNumDesc, String attributeName, int allowedLength)
	throws RemoteException, AmsException {

        if(shipmentTerms == null)
           return;

	String text;
	String endText;

	text = shipmentTerms.getAttribute(attributeName);

	if (text.length() > allowedLength) {
		endText = text.substring(allowedLength-20, allowedLength);

                String[] errorParameters = {shipmentTerms.getAlias(attributeName), endText, shipmentNumDesc};
                boolean[] wrapParameters = {true, true, false};
		shipmentTerms.getErrorManager().issueError (
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.TEXT_TOO_LONG_SHOULD_END,
					errorParameters,
					wrapParameters);

	}
 }

	/**
	 * Called from the preSave methods on the managed bean.
	 *
	 *
	 */
	public void preSave(TermsBean terms) throws AmsException
	 {
             try
             {
                // Perform subclass-specific validation of text fields
                // for SWIFT characters
                // A template validates its SWIFT characters when the template is saved,
                // unless the template is being auto-saved when the user session times out.
                String templateFlag = "";
				String timeoutFlag = null;
                if(terms.getClientServerDataBridge() != null) {
                     templateFlag = (String) terms.getClientServerDataBridge().getCSDBValue("template");
                     timeoutFlag = (String) terms.getClientServerDataBridge().getCSDBValue("timeout");
                } else
                {
                    LOG.info("]]]]]]]]] CSDB IS NULL ]]]]]]]]]]]]]]]]]");
                }

                if( (templateFlag != null) &&
                    (templateFlag.equals(TradePortalConstants.INDICATOR_YES)) &&
                    (timeoutFlag == null || !timeoutFlag.equals(TradePortalConstants.INDICATOR_YES)))
                 {
                     validateForSwiftCharacters(terms);
                 }
             }
             catch(RemoteException re)
             {

             }
	 }


/**
 * Validates the precision of an amount value given the currency for it.
 * If the precision is wrong, the invalid currency format error is issued.
 *
 * It also checks that the value will fit into the database
 *
 * @param terms com.ams.tradeportal.busobj.termsBean
 * @param amountAttribute java.lang.String
 * @param currencyAttribute java.lang.String
 * @exception java.rmi.RemoteException The exception description.
 * @exception com.amsinc.ecsg.frame.AmsException The exception description.
 */
public void validateAmount(TermsBean terms, String amountAttribute, String currencyAttribute) throws java.rmi.RemoteException, com.amsinc.ecsg.frame.AmsException {

	String amount = terms.getAttribute(amountAttribute);
	String currencyCode = terms.getAttribute(currencyAttribute);

        // Validate that value will fit in database and that currency precision is correct
        TPCurrencyUtility.validateAmount(currencyCode, amount, terms.getAlias(amountAttribute), terms.getErrorManager() );
}

   /**
	* Validates that at least one field of name, addresss line 1,2 or 3 has
	* a value.  This is used to edit for a valid consignee party.
	*
	* @return void
	*/
   public void editForAnyConsigneeValue(ShipmentTerms terms, String termsPartyName, String shipmentNumDesc)
               throws RemoteException, AmsException
   {
	  TermsParty   termsParty   = null;
	  String       name         = null;
	  String       line1        = null;
	  String       line2        = null;
	  String       line3        = null;

          String[] errorParameters = {shipmentNumDesc};
          boolean[] wrapParameters = {false};

	  try
	  {
		 termsParty = (TermsParty) terms.getComponentHandle(termsPartyName);

		 name  = termsParty.getAttribute("name");
		 line1 = termsParty.getAttribute("address_line_1");
		 line2 = termsParty.getAttribute("address_line_2");
		 line3 = termsParty.getAttribute("address_line_3");

		 if (StringFunction.isBlank(name) &&
			 StringFunction.isBlank(line1) &&
			 StringFunction.isBlank(line2) &&
			 StringFunction.isBlank(line3))
		 {
		    terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
			                               TradePortalConstants.OTHER_CONSIGNEE_MISSING,
                                                       errorParameters,
                                                       wrapParameters);
		 }
	  }
	  catch (ComponentProcessingException e)
	  {
	     terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
		                                TradePortalConstants.OTHER_CONSIGNEE_MISSING,
                                                errorParameters,
                                                wrapParameters);
	  }
   }

   /**
	* Validates beneficiary fields: must have name, line 1, city, and
	* country.
	*
	* @return void
	* @param TermsBean terms
	* @param String termsPartyName
	* @param String errorDisplayName
	* @throws RemoteException, AmsException
	*/
   public void editRequiredBeneficiaryValue(TermsBean terms, String termsPartyName, String errorDisplayName) throws RemoteException, AmsException
   {
	  TermsParty   termsParty = null;
	  String       value      = null;
	  String financeType = null;
	  String payMethod = null;
	  
	  if(terms.isAttributeRegistered("payment_method"))
      {
		  payMethod=terms.getAttribute("payment_method");
      }
	  
	  if(terms.isAttributeRegistered("finance_type"))
      {
		  financeType = terms.getAttribute("finance_type");
		  
      }
	  try
	  {
		 termsParty = (TermsParty) terms.getComponentHandle(termsPartyName);
		 String partyType = null;
		 if(termsParty.isAttributeRegistered("terms_party_type")){
			 partyType = termsParty.getAttribute("terms_party_type");
		 }

		 if (!((TradePortalConstants.TRADE_LOAN_REC.equals(financeType) || 
				 TradePortalConstants.TRADE_LOAN_PAY.equals(financeType) ) &&
				 TermsPartyType.BENEFICIARY_BANK.equals(partyType))){
			 
			 value = termsParty.getAttribute("name");
			 if (StringFunction.isBlank(value))
			 {
				issueTermsPartyError(termsParty, terms, errorDisplayName, "name");
			 }
			 
			 //T36000031469  -BENE COUNTRY is mandatory in Payables Trade Loan only for CBFT Payment Mehtod.
			 
			 if (TradePortalConstants.TRADE_LOAN_PAY.equals(financeType) 
					 && TradePortalConstants.CROSS_BORDER_FIN_TRANSFER.equals(payMethod)
					 && StringFunction.isBlank(termsParty.getAttribute("address_country"))){
					issueTermsPartyError(termsParty, terms, errorDisplayName, "address_country");
			 }
			//T36000031469 - End
 		 }
		 

		 
		 
		 if (!(TradePortalConstants.TRADE_LOAN_REC.equals(financeType) || TradePortalConstants.TRADE_LOAN_PAY.equals(financeType) )){

			 value = termsParty.getAttribute("address_line_1");
			 if (StringFunction.isBlank(value))
			 {
				issueTermsPartyError(termsParty, terms, errorDisplayName, "address_line_1");
			 }
			 //IR 30000 start
			 String importInd = terms.getImportIndicator();
			 if(!(TradePortalConstants.INDICATOR_NO.equals(importInd) || TradePortalConstants.INDICATOR_YES.equals(importInd))){
				 value = termsParty.getAttribute("address_city");
				 if (StringFunction.isBlank(value))
				 {
					 issueTermsPartyError(termsParty, terms, errorDisplayName, "address_city");
				 }
			 }//IR 30000 end
		 }
	  }
	  catch (ComponentProcessingException e)
	  {
		 issueTermsPartyError(terms, errorDisplayName, "TermsPartyBeanAlias.name");
		 issueTermsPartyError(terms, errorDisplayName, "TermsPartyBeanAlias.address_line_1");
		 issueTermsPartyError(terms, errorDisplayName, "TermsPartyBeanAlias.address_city");
		 issueTermsPartyError(terms, errorDisplayName, "TermsPartyBeanAlias.address_country");
	  }
   }

   /**
	* Validates beneficiary fields: must have name, line 1, city, and
	* country.
	*
	* @return void
	* @param TermsBean terms
	* @param String termsPartyName
	* @throws RemoteException, AmsException
	*/
  public void editRequiredBeneficiaryValue(TermsBean terms, String termsPartyName) throws RemoteException, AmsException
  {
	  this.editRequiredBeneficiaryValue(terms, termsPartyName, "ImportDLCIssue.Beneficiary");
  }
   
   /**
	* Validates that the name field has a value for a valid freight forwarder party.
	*
	* @return void
	*/
   public void editFreightForwarderValue(TermsBean terms, String termsPartyName) throws RemoteException, AmsException
   {
	  TermsParty   termsParty = null;
	  String       value      = null;

	  try
	  {
		 termsParty = (TermsParty) terms.getComponentHandle(termsPartyName);

		 value = termsParty.getAttribute("name");
		 if (StringFunction.isBlank(value))
		 {
			issueTermsPartyError(termsParty, terms, "AWB_SGTEE.FreightForwarder", "name");
		 }
	  }
	  catch (ComponentProcessingException e)
	  {
		 issueTermsPartyError(terms, "AWB_SGTEE.FreightForwarder", "TermsPartyBeanAlias.name");
	  }
   }

  /**
   * Validates that the name field has a value for a valid release to party.
   *
   * @return void
   */
   public void editReleaseToPartyValue(TermsBean terms, String termsPartyName) throws RemoteException, AmsException
   {
	  TermsParty   termsParty = null;
	  String       value      = null;

	  try
	  {
		 termsParty = (TermsParty) terms.getComponentHandle(termsPartyName);

		 value = termsParty.getAttribute("name");
		 if (StringFunction.isBlank(value))
		 {
			issueTermsPartyError(termsParty, terms, "AWB_SGTEE.ReleaseToParty", "name");
		 }
	  }
	  catch (ComponentProcessingException e)
	  {
		 issueTermsPartyError(terms, "AWB_SGTEE.ReleaseToParty", "TermsPartyBeanAlias.name");
	  }
   }


   /**
	* This checks that the current terms party has a defined :
	* Name, AddressLine 1, AddressLine 2, and AddressLine 3.
	*
	* @return boolean  True/False
	*
	*/
   public boolean hasAddressInfo(TermsBean terms, String termsPartyName)
		  throws RemoteException, AmsException
   {
	  TermsParty   termsParty   = null;
	  String       name         = null;
	  String       addressLine1 = null;
	  String       addressLine2 = null;
	  String       addressLine3 = null;

	  try
	  {
		 termsParty = (TermsParty) terms.getComponentHandle(termsPartyName);
		 name = termsParty.getAttribute("name");
		 addressLine1 = termsParty.getAttribute("address_line_1");
		 addressLine2 = termsParty.getAttribute("address_line_2");
		 addressLine3 = termsParty.getAttribute("address_line_3");

		 if( StringFunction.isNotBlank(name) ||
			 StringFunction.isNotBlank(addressLine1) ||
			 StringFunction.isNotBlank(addressLine2) ||
			 StringFunction.isNotBlank(addressLine3) )
				return true;
	  }
	  catch (ComponentProcessingException e)
	  {
	    return false;
	  }
	  return false;
   }

   /**
	* This checks that the current shipment terms party has a defined :
	* Name, AddressLine 1, AddressLine 2, and AddressLine 3.
	*
	* @return boolean  True/False
	*
	*/
   public boolean hasAddressInfo(ShipmentTerms shipmentTerms, String termsPartyName)
		  throws RemoteException, AmsException
   {
	  TermsParty   termsParty   = null;
	  String       name         = null;
	  String       addressLine1 = null;
	  String       addressLine2 = null;
	  String       addressLine3 = null;

	  try
	  {
		 termsParty = (TermsParty) shipmentTerms.getComponentHandle(termsPartyName);
		 name = termsParty.getAttribute("name");
		 addressLine1 = termsParty.getAttribute("address_line_1");
		 addressLine2 = termsParty.getAttribute("address_line_2");
		 addressLine3 = termsParty.getAttribute("address_line_3");

		 if( StringFunction.isNotBlank(name) ||
			 StringFunction.isNotBlank(addressLine1) ||
			 StringFunction.isNotBlank(addressLine2) ||
			 StringFunction.isNotBlank(addressLine3) )
				return true;
	  }
	  catch (ComponentProcessingException e)
	  {
	    return false;
	  }
	  return false;
   }

   /**
	* Validates Drawee fields: must have name, line 1, city, and
	* country.
	*
	* @return void
	*/
   public void editRequiredDraweeValue(TermsBean terms, String termsPartyName) throws RemoteException, AmsException
   {
	  TermsParty   termsParty = null;
	  String       value      = null;

	  try
	  {
		 termsParty = (TermsParty) terms.getComponentHandle(termsPartyName);

		 value = termsParty.getAttribute("name");
		 if (StringFunction.isBlank(value))
		 {
			issueTermsPartyError(termsParty, terms, "ExportCollectionIssue.Drawee", "name");
		 }

		 value = termsParty.getAttribute("address_line_1");
		 if (StringFunction.isBlank(value))
		 {
			issueTermsPartyError(termsParty, terms, "ExportCollectionIssue.Drawee", "address_line_1");
		 }

		 value = termsParty.getAttribute("address_city");
		 if (StringFunction.isBlank(value))
		 {
			issueTermsPartyError(termsParty, terms, "ExportCollectionIssue.Drawee", "address_city");
		 }

		 value = termsParty.getAttribute("address_country");
		 if (StringFunction.isBlank(value))
		 {
			issueTermsPartyError(termsParty, terms, "ExportCollectionIssue.Drawee", "address_country");
		 }
	  }
	  catch (ComponentProcessingException e)
	  {
		 issueTermsPartyError(terms, "ExportCollectionIssue.Drawee", "TermsPartyBeanAlias.name");
		 issueTermsPartyError(terms, "ExportCollectionIssue.Drawee", "TermsPartyBeanAlias.address_line_1");
		 issueTermsPartyError(terms, "ExportCollectionIssue.Drawee", "TermsPartyBeanAlias.address_city");
		 issueTermsPartyError(terms, "ExportCollectionIssue.Drawee", "TermsPartyBeanAlias.address_country");
	  }
   }

  /**
   * Validates that the name, address line 1, city, and country fields have values
   * for a valid transferee party.
   *
   * @return void
   */
   public void editTransfereeValue(TermsBean terms, String termsPartyName) throws RemoteException, AmsException
   {
	  TermsParty   termsParty = null;
	  String       value      = null;

	  try
	  {
		 termsParty = (TermsParty) terms.getComponentHandle(termsPartyName);

		 value = termsParty.getAttribute("name");
		 if (StringFunction.isBlank(value))
		 {
			issueTermsPartyError(termsParty, terms, "TransferELC.Transferee", "name");
		 }
		 value = termsParty.getAttribute("address_line_1");
		 if (StringFunction.isBlank(value))
		 {
			issueTermsPartyError(termsParty, terms, "TransferELC.Transferee", "address_line_1");
		 }

		 value = termsParty.getAttribute("address_city");
		 if (StringFunction.isBlank(value))
		 {
			issueTermsPartyError(termsParty, terms, "TransferELC.Transferee", "address_city");
		 }

		 value = termsParty.getAttribute("address_country");
		 if (StringFunction.isBlank(value))
		 {
			issueTermsPartyError(termsParty, terms, "TransferELC.Transferee", "address_country");
		 }
	  }
	  catch (ComponentProcessingException e)
	  {
		 issueTermsPartyError(terms, "TransferELC.Transferee", "TermsPartyBeanAlias.name");
		 issueTermsPartyError(terms, "TransferELC.Transferee", "TermsPartyBeanAlias.address_line_1");
		 issueTermsPartyError(terms, "TransferELC.Transferee", "TermsPartyBeanAlias.address_city");
		 issueTermsPartyError(terms, "TransferELC.Transferee", "TermsPartyBeanAlias.address_country");
	  }
   }

  /**
   * Validates that the name, address line 1, city, and country fields have values
   * for a valid assignee party.
   *
   * @return void
   */
   public void editAssigneeValue(TermsBean terms, String termsPartyName) throws RemoteException, AmsException
   {
	  TermsParty   termsParty = null;
	  String       value      = null;

	  try
	  {
		 termsParty = (TermsParty) terms.getComponentHandle(termsPartyName);

		 value = termsParty.getAttribute("name");
		 if (StringFunction.isBlank(value))
		 {
			issueTermsPartyError(termsParty, terms, "Assignment.Assignee", "name");
		 }
		 value = termsParty.getAttribute("address_line_1");
		 if (StringFunction.isBlank(value))
		 {
			issueTermsPartyError(termsParty, terms, "Assignment.Assignee", "address_line_1");
		 }

		 value = termsParty.getAttribute("address_city");
		 if (StringFunction.isBlank(value))
		 {
			issueTermsPartyError(termsParty, terms, "Assignment.Assignee", "address_city");
		 }

		 value = termsParty.getAttribute("address_country");
		 if (StringFunction.isBlank(value))
		 {
			issueTermsPartyError(termsParty, terms, "Assignment.Assignee", "address_country");
		 }
	  }
	  catch (ComponentProcessingException e)
	  {
		 issueTermsPartyError(terms, "Assignment.Assignee", "TermsPartyBeanAlias.name");
		 issueTermsPartyError(terms, "Assignment.Assignee", "TermsPartyBeanAlias.address_line_1");
		 issueTermsPartyError(terms, "Assignment.Assignee", "TermsPartyBeanAlias.address_city");
		 issueTermsPartyError(terms, "Assignment.Assignee", "TermsPartyBeanAlias.address_country");
	  }
   }

  /**
   * Validates that the name, address line 1, city, and country fields have values
   * for a valid assignee bank party.
   *
   * @return void
   */
   public void editAssigneeBankValue(TermsBean terms, String termsPartyName) throws RemoteException, AmsException
   {
	  TermsParty   termsParty = null;
	  String       value      = null;

	  try
	  {
		 termsParty = (TermsParty) terms.getComponentHandle(termsPartyName);

		 value = termsParty.getAttribute("name");
		 if (StringFunction.isBlank(value))
		 {
			issueTermsPartyError(termsParty, terms, "Assignment.AssigneesBank", "name");
		 }
		 value = termsParty.getAttribute("address_line_1");
		 if (StringFunction.isBlank(value))
		 {
			issueTermsPartyError(termsParty, terms, "Assignment.AssigneesBank", "address_line_1");
		 }

		 value = termsParty.getAttribute("address_city");
		 if (StringFunction.isBlank(value))
		 {
			issueTermsPartyError(termsParty, terms, "Assignment.AssigneesBank", "address_city");
		 }

		 value = termsParty.getAttribute("address_country");
		 if (StringFunction.isBlank(value))
		 {
			issueTermsPartyError(termsParty, terms, "Assignment.AssigneesBank", "address_country");
		 }
	  }
	  catch (ComponentProcessingException e)
	  {
		 issueTermsPartyError(terms, "Assignment.AssigneesBank", "TermsPartyBeanAlias.name");
		 issueTermsPartyError(terms, "Assignment.AssigneesBank", "TermsPartyBeanAlias.address_line_1");
		 issueTermsPartyError(terms, "Assignment.AssigneesBank", "TermsPartyBeanAlias.address_city");
		 issueTermsPartyError(terms, "Assignment.AssigneesBank", "TermsPartyBeanAlias.address_country");
	  }
   }

  /**
   * Issues a terms party error based on the attribute name's alias and text resource key passed in.
   *
   * @return void
   */
   public void issueTermsPartyError(TermsParty termsParty, TermsBean terms, String textResourceKey,
									String attributeName) throws RemoteException, AmsException
   {
	  StringBuffer   substitutionParm = new StringBuffer();

	  substitutionParm.append(termsParty.getResourceManager().getText(textResourceKey,
																	  TradePortalConstants.TEXT_BUNDLE));
	  substitutionParm.append(" ");
	  substitutionParm.append(termsParty.getAlias(attributeName));

	  terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
										 AmsConstants.REQUIRED_ATTRIBUTE,
										 substitutionParm.toString());
   }

  /**
   * Issues a terms party error based on the attribute name's alias and text resource key passed in.
   * This method should be called ONLY if you are dealing with an empty terms party (which should be
   * caught when getComponentHandle is called on a deleted terms party).
   *
   * @return void
   */
   public void issueTermsPartyError(TermsBean terms, String textResourceKey, String missingAttributeAlias)
									throws RemoteException, AmsException
   {
	  ResourceManager   resourceManager  = null;
	  StringBuffer      substitutionParm = new StringBuffer();

	  resourceManager = terms.getResourceManager();

	  substitutionParm.append(resourceManager.getText(textResourceKey, TradePortalConstants.TEXT_BUNDLE));
	  substitutionParm.append(" ");
	  substitutionParm.append(resourceManager.getText(missingAttributeAlias, TradePortalConstants.TEXT_BUNDLE));

	  terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, AmsConstants.REQUIRED_ATTRIBUTE,
										 substitutionParm.toString());
   }

 /**
 * Validates that the text attribute (assumed to be a fill-in phrase) has
 * had all the fields filled in.  (This means there are no sets of brackets
 * in the text.
 *
 * @param terms com.ams.tradeportal.busobj.termsBean
 * @param attributeName String - The name of the text attribute to check
 */
public void validateFillInPhrase(TermsBean terms, String attributeName)
	throws RemoteException, AmsException {

	StringTokenizer st = new StringTokenizer(terms.getAttribute(attributeName),
											"[]", true);
	String token;
	String fieldLabel = "";
	boolean fieldFound = false;
	int semicolon;
	LOG.debug("in validate fill in phrase");

	// Search for fields enclosed within brackets [].  If found, it
	// means we're trying to save a fill-in phrase which hasn't been
	// filled in.
	while (st.hasMoreTokens()) {
		// Get the token
		token = st.nextToken();

		// Determine if it is the start of a field, end of a field, or
		// just text
		if (token.equals("[")) {
			fieldFound = true;
			LOG.debug("found [");
		} else if (token.equals("]")) {
			LOG.debug("found close ]");
			fieldFound = false;
		} else {
			if (fieldFound) {
				// If we've found a field token, attempt to parse out
				// the label
				semicolon = token.indexOf(";");
				if (semicolon > -1) {
					fieldLabel = token.substring(0, semicolon);
				} else {
					fieldLabel = token;
				}

				LOG.debug("found field: {} " , fieldLabel);
				terms.getErrorManager().issueError (
					TradePortalConstants.ERR_CAT_1,
					AmsConstants.REQUIRED_ATTRIBUTE,
					fieldLabel);
			}
		}

	} // end while

 }


  /**
   * This method is meant to provide a way to specifically substitute an explicit string
   * for an attribute name.  Since the TermsPartys bean name can't have a specific alias
   * registered (Because there are many different types of objects that share the same
   * terms party bean names - this method helps substitute the string for the registered
   * bean name.
   * Issues a terms party error based on the text resource key passed in.
   *
   * @param TermsBean   - Terms             - the object we use to get the data from.
   * @param String      - TextResourceKey   - The value to be looked up and substituted in.
   * @return void
   */
   public void issueTermsPartyError(TermsBean terms, String textResourceKey) throws RemoteException, AmsException
   {
	  ResourceManager   resourceManager  = terms.getResourceManager();

	  terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, AmsConstants.REQUIRED_ATTRIBUTE,
										 resourceManager.getText(textResourceKey, TradePortalConstants.TEXT_BUNDLE));
   }

/**
  * The intention of this method is to delete an associated terms party from the
  * terms object.  (We'd like to just get a handle to the component and delete it
  * here.  Unfortunately transactional processing is not including this delete in
  * the transaction for the Instrument/Transaction/Terms.  Therefore, on a verify
  * error, Instrument/Transaction/Terms were correctly rolling back their changes
  * but not the TermsParty delete).  Instead, we'll break the component association
  * here and place the component name in a vector.  After verification, Terms can
  * query this vector and perform the component deletes itself (in preSave).
  * <p>
  * @param terms TermsBean - the terms object to remove the party from
  * @param termsPartyName String - the name of the party component to remove
  *                                (e.g., FourthTermsParty)
  */
public void removeTermsParty(TermsBean terms, String termsPartyName)
	throws RemoteException, AmsException {

	String termsPartyOidPath = "";
	String partyOid = "";

	termsPartyOidPath = "c_" + termsPartyName;
	partyOid = terms.getAttribute(termsPartyOidPath);

	if (StringFunction.isNotBlank(partyOid)) {
		partiesToDelete.addElement(termsPartyName);
		terms.setAttribute(termsPartyOidPath, "");
	}

}

/**
  * The intention of this method is to delete an associated terms party from the
  * shipment terms object. We break the component association
  * here and place the component name in a vector.  After verification, Shipment
  * Terms will delete the component
  * <p>
  * @param terms ShipmentTerms - the terms object to remove the party from
  * @param termsPartyName String - the name of the party component to remove
  *                                (e.g., FourthTermsParty)
  */
public void removeTermsParty(ShipmentTerms shipmentTerms, String termsPartyName)
	throws RemoteException, AmsException {

	String termsPartyOidPath = "";
	String partyOid = "";

	termsPartyOidPath = "c_" + termsPartyName;
	partyOid = shipmentTerms.getAttribute(termsPartyOidPath);

	if (StringFunction.isNotBlank(partyOid)) {
		shipmentTerms.setAttribute(termsPartyOidPath, "");
	}

}

/**
  * getter for the partiesToDelete attribute (which has entries added to by the
  * removeTermsParty method.)
  * <p>
  * @return Vector - list of component names to delete
  */
public Vector getPartiesToDelete() {

	return partiesToDelete;

}


/**
 * Populates the first shipment terms object of a Terms object
 * This is used when packaging transaction data
 *
 * @param terms - the terms of the transaction
 * @param termsDoc - the XML being updated
 *
 */
protected void populateFirstShipmentTermsXml(TermsBean terms, DocumentHandler termsDoc) throws AmsException, RemoteException
 {
	// GGAYLE - IR LSUE042751489 - 05/06/2004
    String vesselName = "";
    String carrierName = "";
	// GGAYLE - IR LSUE042751489 - 05/06/2004

    ShipmentTerms shipmentTerms = terms.getFirstShipment();
    if(shipmentTerms != null)
     {
        termsDoc.removeComponent("/Terms/ShipmentTerms/FullShipmentTerms");
        termsDoc.setAttribute("/Terms/ShipmentTerms/TotalNumberOfEntries", "1");

        termsDoc.setAttribute("/Terms/ShipmentTerms/FullShipmentTerms(0)/TransshipmentAllowed",	 shipmentTerms.getAttribute("transshipment_allowed"));
        termsDoc.setAttribute("/Terms/ShipmentTerms/FullShipmentTerms(0)/LatestShipmentDate",	 shipmentTerms.getAttribute("latest_shipment_date"));
        termsDoc.setAttribute("/Terms/ShipmentTerms/FullShipmentTerms(0)/Incoterm",		 shipmentTerms.getAttribute("incoterm"));
        termsDoc.setAttribute("/Terms/ShipmentTerms/FullShipmentTerms(0)/IncotermLocation",	 shipmentTerms.getAttribute("incoterm_location"));
        termsDoc.setAttribute("/Terms/ShipmentTerms/FullShipmentTerms(0)/ShipmentFrom",		 shipmentTerms.getAttribute("shipment_from"));
        termsDoc.setAttribute("/Terms/ShipmentTerms/FullShipmentTerms(0)/ShipmentTo",		 shipmentTerms.getAttribute("shipment_to"));
        termsDoc.setAttribute("/Terms/ShipmentTerms/FullShipmentTerms(0)/GoodsDescription",	 shipmentTerms.getAttribute("goods_description"));
        termsDoc.setAttribute("/Terms/ShipmentTerms/FullShipmentTerms(0)/POLineItems",		 shipmentTerms.getAttribute("po_line_items"));
        termsDoc.setAttribute("/Terms/ShipmentTerms/FullShipmentTerms(0)/AirWaybillNumber",	 shipmentTerms.getAttribute("air_waybill_num"));

	// GGAYLE - IR LSUE042751489 - 05/06/2004
	// Determine if we should get the vessel name voyage number or the carrier name flight number.  Vessel name is used by
	// Shipping Guarantee Issue and Carrier name is used by Air Waybill Release.  It's always one or the other, so if the
	// vessel name is blank, then use the carrier name, even if it's also blank.  They both map to the same tag in the
	// XML so either can be used.
        //termsDoc.setAttribute("/Terms/ShipmentTerms/FullShipmentTerms(0)/VesselName",		 shipmentTerms.getAttribute("vessel_name_voyage_num"));
		vesselName = shipmentTerms.getAttribute("vessel_name_voyage_num");
		carrierName = shipmentTerms.getAttribute("carrier_name_flight_num");

	    if (!StringFunction.isBlank(vesselName))
	    {
			termsDoc.setAttribute("/Terms/ShipmentTerms/FullShipmentTerms(0)/VesselName", vesselName);
	    }
	    else
	    {
			termsDoc.setAttribute("/Terms/ShipmentTerms/FullShipmentTerms(0)/VesselName", carrierName);
	    }
	// GGAYLE - IR LSUE042751489 - 05/06/2004


        // This is always blank.
        termsDoc.setAttribute("/Terms/ShipmentTerms/FullShipmentTerms(0)/TransportTermsText",    "");
        termsDoc.setAttribute("/Terms/ShipmentTerms/FullShipmentTerms(0)/ContainerNumber",	 shipmentTerms.getAttribute("container_number"));
        termsDoc.setAttribute("/Terms/ShipmentTerms/FullShipmentTerms(0)/BillOfLadingNumber",	 shipmentTerms.getAttribute("bill_of_lading_num"));
   		//iv Adding ShipmentFrom and ShipmentTo

		termsDoc.setAttribute("/Terms/ShipmentTerms/FullShipmentTerms(0)/ShipFromLoading", shipmentTerms.getAttribute("shipment_from_loading"));
		termsDoc.setAttribute("/Terms/ShipmentTerms/FullShipmentTerms(0)/ShipToDischarge", shipmentTerms.getAttribute("shipment_to_discharge"));

        // For now, all of these are blank.  In the future, this could get data from the parties that
        // are components of the shipment terms
        termsDoc.setAttribute("/Terms/ShipmentTerms/FullShipmentTerms(0)/NPBusinessName", "");
        termsDoc.setAttribute("/Terms/ShipmentTerms/FullShipmentTerms(0)/NPAddressLine1", "");
        termsDoc.setAttribute("/Terms/ShipmentTerms/FullShipmentTerms(0)/NPAddressLine2", "");
        termsDoc.setAttribute("/Terms/ShipmentTerms/FullShipmentTerms(0)/NPAddressLine3", "");
        termsDoc.setAttribute("/Terms/ShipmentTerms/FullShipmentTerms(0)/CONBusinessName", "");
        termsDoc.setAttribute("/Terms/ShipmentTerms/FullShipmentTerms(0)/CONAddressLine1", "");
        termsDoc.setAttribute("/Terms/ShipmentTerms/FullShipmentTerms(0)/CONAddressLine2", "");
        termsDoc.setAttribute("/Terms/ShipmentTerms/FullShipmentTerms(0)/CONAddressLine3", "");

    }
 }

	/**
	 * Get the OVERRIDE_SWIFT_LENGTH_IND value for a given terms_oid
	 *
	 * @param String termsOid - the terms_oid of a given transaction
	 * @return String - the value of CLIENT_BANK.OVERRIDE_SWIFT_LENGTH_IND for this terms_oid; either "Y" or "N" (If the database query results return null then "N" will be returned to the call as a default value).
	 * @throws RemoteException
	 * @throws AmsException
	 */
	private String getOverrideSwiftLengthInd(String termsOid) throws RemoteException, AmsException
	{
		StringBuffer sqlQuery = new StringBuffer();

		LOG.debug("TermsMgr.getOverrideSwiftLengthInd(String): Creating SQL query with terms_oid = {}",termsOid);

		sqlQuery.append("select CLIENT_BANK.override_swift_length_ind");
		sqlQuery.append(" from INSTRUMENT, TRANSACTION, CLIENT_BANK");
		sqlQuery.append(" where");
		sqlQuery.append(" TRANSACTION.C_CUST_ENTER_TERMS_OID = ?");
		sqlQuery.append(" AND INSTRUMENT.INSTRUMENT_OID = TRANSACTION.P_INSTRUMENT_OID");
		sqlQuery.append(" AND CLIENT_BANK.ORGANIZATION_OID = INSTRUMENT.A_CLIENT_BANK_OID");

		DocumentHandler results = DatabaseQueryBean.getXmlResultSet(sqlQuery.toString(), false, new Object[]{termsOid});
		if(results == null)
		{
			LOG.debug("TermsMgr.getOverrideSwiftLengthInd(String): DB query returned null results. Query was: {}",sqlQuery);
			return null;
		} // if(results == null)

		String overrideSwiftLengthInd = results.getAttribute("/ResultSetRecord(0)/OVERRIDE_SWIFT_LENGTH_IND");
		if(overrideSwiftLengthInd == null)
		{
			LOG.debug("TermsMgr.getOverrideSwiftLengthInd(String): Results returned null value for OVERRIDE_SWIFT_LENGTH_IND.  Result object was: {}",results.toString());
			return TradePortalConstants.INDICATOR_NO;
		} 

		LOG.debug("TermsMgr.getOverrideSwiftLengthInd(String): Returning the following value for overrideSwiftLengthInd= {}",overrideSwiftLengthInd);
		return overrideSwiftLengthInd;
	} // private String getOverrideSwiftLengthInd(String termsOid) throws RemoteException, AmsException

	/**
	 * Get the OVERRIDE_SWIFT_LENGTH_IND value for the given Term (Terms->Transaction->Instrument->Client_Bank)
	 *
	 * @param TermsBean terms - the Terms of the transaction
	 * @return String - the value of CLIENT_BANK.OVERRIDE_SWIFT_LENGTH_IND for this Term; either "Y" or "N" (If the database query results return null then "N" will be returned to the call as a default value).
	 * @throws RemoteException
	 * @throws AmsException
	 */
	protected String getOverrideSwiftLengthInd(TermsBean terms) throws RemoteException, AmsException
	{
		if(terms.getAttribute("terms_oid") == null)
		{
			LOG.debug("TermsMgr.getOverrideSwiftLengthInd(TermsBean): Unable to get overrideSwiftLengthInd because terms_oid of TermsBean is null.");
			return TradePortalConstants.INDICATOR_NO;
		}

		return getOverrideSwiftLengthInd(terms.getAttribute("terms_oid"));
	} 

	/**
	 * Get the OVERRIDE_SWIFT_LENGTH_IND value for the given Term (Shipment_Terms->Transaction->Instrument->Client_Bank)
	 *
	 * @param ShipmentTerms terms - the ShipmentTerms of the transaction
	 * @return String - the value of CLIENT_BANK.OVERRIDE_SWIFT_LENGTH_IND for this ShipmentTerms; either "Y" or "N" (If the database query results return null then "N" will be returned to the call as a default value).
	 * @throws RemoteException
	 * @throws AmsException
	 */
	protected String getOverrideSwiftLengthInd(ShipmentTerms terms) throws RemoteException, AmsException
	{
		if(terms.getAttribute("terms_oid") == null)
		{
			LOG.debug("TermsMgr.getOverrideSwiftLengthInd(ShipmentTerms): Unable to et overrideSwiftLengthInd because terms_oid of ShipmentTerms is null.");
			return TradePortalConstants.INDICATOR_NO;
		} // if(terms.getAttribute("terms_oid") == null)

		return getOverrideSwiftLengthInd(terms.getAttribute("terms_oid"));
	} // protected String getOverrideSwiftLengthInd(ShipmentTerms terms)

	/**
	 * Calculate the Presentation Days with by finding the difference between Latest Shipment Date and Expiry Date.
	 *
	 * @param shipDate GregorianCalendar
	 * @param expiryDate GregorianCalendar
	 * @return int the difference between the shipment date and the expiry date
	 */
	protected int calculatePresentationDays(GregorianCalendar shipDate, GregorianCalendar expiryDate)
	{
		// First we need to convert the GregorianCalendar objects to Date objects
		Date shipDateAsDate = shipDate.getTime();
		Date expiryDateAsDate = expiryDate.getTime();

		// Now convert the Date objects to long (number of milliseconds before/after January 1, 1970)
		long shipDateAsLong = shipDateAsDate.getTime();
		long expiryDateAsLong = expiryDateAsDate.getTime();
		LOG.debug("TermsMgr.calculatePresentationDays(GregorianCalendar,GregorianCalendar): shipDateAsLong is {}",shipDateAsLong);
		LOG.debug("TermsMgr.calculatePresentationDays(GregorianCalendar,GregorianCalendar): expiryDateAsLong is {}",expiryDateAsLong);

		long differenceInMillis = Math.abs(expiryDateAsLong - shipDateAsLong);
		LOG.debug("TermsMgr.calculatePresentationDays(GregorianCalendar,GregorianCalendar): differenceInMillis is {}",differenceInMillis);

		long differenceInDays = ((((differenceInMillis/1000)/60)/60)/24);
		LOG.debug("TermsMgr.calculatePresentationDays(GregorianCalendar,GregorianCalendar): differenceInDays is {}",differenceInDays);

		Long differenceInDaysAsLong = new Long(differenceInDays);
		LOG.debug("TermsMgr.calculatePresentationDays(GregorianCalendar,GregorianCalendar): differenceInDaysAsLong.intValue() is {}",differenceInDaysAsLong.intValue());

		return differenceInDaysAsLong.intValue();
	} 

	/**
	 * Get the CORPORATE_ORG_TYPE_CODE from the CORPORATE_ORG table for the sixth terms party of a given TermsBean
	 *
	 * @param terms TermsBean
	 * @return String the CORPORATE_ORG_TYPE_CODE from the CORPORATE_ORG table for the sixth terms party of a given TermsBean or null of none exists
	 * @throws AmsException
	 * @throws RemoteException
	 */
	protected String getCorporateOrgTypeCode(TermsBean terms) throws AmsException, RemoteException
	{
		StringBuffer sqlQuery = new StringBuffer();

		sqlQuery.append("select corporate_org.CORPORATE_ORG_TYPE_CODE");
		sqlQuery.append(" from corporate_org, terms_party");
		sqlQuery.append(" where terms_party.terms_party_oid=?");
		sqlQuery.append(" and terms_party.OTL_CUSTOMER_ID=corporate_org.proponix_customer_id");
		DocumentHandler results = DatabaseQueryBean.getXmlResultSet(sqlQuery.toString(), false, new Object[]{terms.getAttribute("c_SixthTermsParty")});

		if(results != null)
		{
			String corporateOrgTypeCode = results.getAttribute("/ResultSetRecord(0)/CORPORATE_ORG_TYPE_CODE");
			LOG.debug("TermsMgr.getCorporateOrgTypeCode(TermsBean): corporateOrgTypeCode = {}",corporateOrgTypeCode);
			return corporateOrgTypeCode;
		}
		else
		{
			return null;
		}
	} 
	
	// CR-594 - MANOHAR - 08/08/2011 - BEGIN
	/**
	 * Get the OVERRIDE_SWIFT_VALIDATION_IND value for a given terms_oid
	 *
	 * @param String termsOid - the terms_oid of a given transaction
	 * @return String - the value of CLIENT_BANK.OVERRIDE_SWIFT_VALIDATION_IND for this terms_oid; either "Y" or "N" (If the database query results return null then "N" will be returned to the call as a default value).
	 * @throws RemoteException
	 * @throws AmsException
	 */
	private String getOverrideSwiftValidationInd(String termsOid, String templateFlag) throws RemoteException, AmsException
	{	
		StringBuffer sqlQuery = new StringBuffer();
		DocumentHandler results = null; 
		
		if(templateFlag.equals(TradePortalConstants.INDICATOR_YES)){
			LOG.debug("TermsMgr.getOverrideSwiftValidationInd(String, String): Creating SQL query with terms_oid = {}",termsOid);

			sqlQuery.append("select override_swift_validation_ind from client_bank where organization_oid =");
			sqlQuery.append("(select a_client_bank_oid from corporate_org where organization_oid =");
			sqlQuery.append("(select p_owner_org_oid from template, Transaction where TRANSACTION.C_CUST_ENTER_TERMS_OID =?");
			sqlQuery.append(" and template.c_template_instr_oid=Transaction.p_instrument_oid))");
			
			results = DatabaseQueryBean.getXmlResultSet(sqlQuery.toString(), false, new Object[]{termsOid});
		} else{
			LOG.debug("TermsMgr.getOverrideSwiftValidationInd(String, String): Creating SQL query with terms_oid = {}",termsOid);

			sqlQuery.append("select CLIENT_BANK.override_swift_validation_ind");
			sqlQuery.append(" from INSTRUMENT, TRANSACTION, CLIENT_BANK");
			sqlQuery.append(" where");
			sqlQuery.append(" TRANSACTION.C_CUST_ENTER_TERMS_OID = ?");
			sqlQuery.append(" AND INSTRUMENT.INSTRUMENT_OID = TRANSACTION.P_INSTRUMENT_OID");
			sqlQuery.append(" AND CLIENT_BANK.ORGANIZATION_OID = INSTRUMENT.A_CLIENT_BANK_OID");

			results = DatabaseQueryBean.getXmlResultSet(sqlQuery.toString(), false, new Object[]{termsOid});
		}
		
		if(results == null)
		{
			LOG.debug("TermsMgr.getOverrideSwiftValidationInd(String, String): DB query returned null results. Query was :{}",sqlQuery);
			return null;
		} 

		String overrideSwiftValidatonInd = results.getAttribute("/ResultSetRecord(0)/OVERRIDE_SWIFT_VALIDATION_IND");
		if(overrideSwiftValidatonInd == null)
		{
			LOG.debug("TermsMgr.getOverrideSwiftValidationInd(String, String): Results returned null value for OVERRIDE_SWIFT_VALIDATION_IND.  Result object was : {}",results.toString());
			return TradePortalConstants.INDICATOR_NO;
		} 
		
		

		LOG.debug("TermsMgr.getOverrideSwiftValidationInd(String, String): Returning the following value for overrideSwiftValidationInd = {}",overrideSwiftValidatonInd);
		return overrideSwiftValidatonInd;
	}
	
	/**
	 * Get the OVERRIDE_SWIFT_VALIDATION_IND value for the given Term (Terms->Transaction->Instrument->Client_Bank)
	 *
	 * @param TermsBean terms - the Terms of the transaction
	 * @return String - the value of CLIENT_BANK.OVERRIDE_SWIFT_VALIDATION_IND for this Term; either "Y" or "N" (If the database query results return null then "N" will be returned to the call as a default value).
	 * @throws RemoteException
	 * @throws AmsException
	 */
	protected String getOverrideSwiftValidationInd(TermsBean terms) throws RemoteException, AmsException
	{	
		String templateFlag = (String) terms.getClientServerDataBridge().getCSDBValue("template");
		
		if(terms.getAttribute("terms_oid") == null)
		{
			LOG.debug("TermsMgr.getOverrideSwiftValidationInd(TermsBean): Unable to get overrideSwiftValidationInd because terms_oid of TermsBean is null.");
			return TradePortalConstants.INDICATOR_NO;
		} 
		
		return getOverrideSwiftValidationInd(terms.getAttribute("terms_oid"), templateFlag);
	}
	// CR-594 - MANOHAR - 08/08/2011 - END

	// REL9.5 IR 43891
	protected String getAllowISO8859OnlyInd(String termsOid) throws AmsException, RemoteException {
		StringBuilder sqlQuery = new StringBuilder("select allow_only_iso8859_ind from CLIENT_BANK c ,corporate_org o, instrument i, transaction t, terms m");
		sqlQuery.append(" where m.terms_oid = ? AND t.c_cust_enter_terms_oid = m.terms_oid AND t.p_instrument_oid = i.instrument_oid AND i.a_corp_org_oid = o.organization_oid AND o.a_client_bank_oid = c.organization_oid");

		DocumentHandler results = DatabaseQueryBean.getXmlResultSet(sqlQuery.toString(), false, new Object[] { termsOid });
		if (results == null) {
			LOG.debug("validateISO8859OnlyChars(): DB query returned null results. Query was : {}",sqlQuery);
			return "";
		}

		return results.getAttribute("/ResultSetRecord(0)/ALLOW_ONLY_ISO8859_IND");

	}

	public void validateTermsPartyForISO8859Chars(TermsBean terms, String componentIdentifier) throws AmsException, RemoteException {
		String allowISO8859OnlyInd = getAllowISO8859OnlyInd(terms.getAttribute("terms_oid"));
		if (TradePortalConstants.INDICATOR_YES.equalsIgnoreCase(allowISO8859OnlyInd)) {
		try {
				final String[] attributesToInclude = { "address_city", "name", "address_line_1", "address_line_2", "address_line_3", "address_state_province", "contact_name", "central_bank_rep1", "central_bank_rep2", "central_bank_rep3", "customer_reference" };

			TermsParty termsParty = (TermsParty) terms.getComponentHandle(componentIdentifier);
			isValidISO8859Chars(termsParty, terms.getErrorManager(), attributesToInclude);
		} catch (ComponentProcessingException cpe) {
			// Component terms party does not exist... nothing to validate
		}
		}
	}

	private void isValidISO8859Chars(TermsParty termsParty, ErrorManager errMgr, String[] attributesToInclude) throws AmsException, RemoteException {
		for (int i = 0; i < attributesToInclude.length; i++) {
			String attr = attributesToInclude[i];
			String value = termsParty.getAttribute(attr);
			if (StringFunction.isNotBlank(value)) {
			CharsetEncoder encoder = Charset.forName("ISO-8859-1").newEncoder();
			boolean isValidCharSeq = encoder.canEncode(CharBuffer.wrap(value.toCharArray()));
			if (!isValidCharSeq) {

				char[] attrCharSeq = value.toCharArray();
				StringBuilder invalidChars = new StringBuilder();
					for (int j = 0; j < attrCharSeq.length; j++) {
						boolean isValidChar = encoder.canEncode(attrCharSeq[j]);
					if (!isValidChar)
							invalidChars.append(attrCharSeq[j]);
				}
					String partyTypeCode = termsParty.getAttribute("terms_party_type");
					errMgr.issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.INVALID_ISO_ONLY_8859_CHARS_ERR, ReferenceDataManager.getRefDataMgr().getDescr("TERMS_PARTY_TYPE", partyTypeCode), termsParty.getAlias(attr), invalidChars.toString());
			}
		}
	}
	}
}
