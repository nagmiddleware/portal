package com.ams.tradeportal.busobj.termsmgr;
import java.rmi.RemoteException;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.busobj.TermsBean;
import com.ams.tradeportal.busobj.TermsParty;
import com.ams.tradeportal.busobj.util.InstrumentServices;
import com.ams.tradeportal.busobj.webbean.TermsWebBean;
import com.ams.tradeportal.common.TermsPartyType;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.Attribute;
import com.amsinc.ecsg.frame.AttributeManager;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.StringFunction;

/**
 * This class manages the dynamic attribute registration and validation
 * for a Terms business object or TermsWebBean for an Issue Guarantee
 *
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class GUA__ISSTermsMgr extends TermsMgr
 {
private static final Logger LOG = LoggerFactory.getLogger(GUA__ISSTermsMgr.class);
	     //End of Code by Indu Valavala

  	/**
	 *  Constructor that is used by business objects to create their
	 *  manager.  This method also register the dynamic attributes of
	 *  the bean.
	 *
	 *  @param mgr AttributeManager attribute manager of the bean being managed.  Used to register attributes
	 */
	public GUA__ISSTermsMgr(AttributeManager mgr) throws AmsException
	 {
	  super(mgr);
	 }

	/**
	 *  Constructor that is used by web beans to create their
	 *  manager.  This method also register the dynamic attributes of
	 *  the bean.
	 *
	 *  @param mgr TermsWebBean - the bean being managed.  Used to register attributes.
	 */
	public GUA__ISSTermsMgr(TermsWebBean terms) throws AmsException
	 {
	  super(terms);
	 }

	/**
	 *  Returns a list of attributes.  These describe each of the attributes
	 *  that will be registered for the bean being managed.
	 *
	 *  This method controls which attributes of TermsBean or TermsWebBean are registered
	 *  for an this instrument and transaction type
	 *
	 *  @return Vector - list of attributes
	 */
			       public List<Attribute> getAttributesToRegister()
	 {
		List<Attribute> attrList = super.getAttributesToRegister();
		attrList.add( TermsBean.Attributes.create_amount() );
		attrList.add( TermsBean.Attributes.create_amount_currency_code() );
		attrList.add( TermsBean.Attributes.create_branch_code() );
		attrList.add( TermsBean.Attributes.create_characteristic_type() );
		attrList.add( TermsBean.Attributes.create_coms_chrgs_foreign_acct_curr() );
		attrList.add( TermsBean.Attributes.create_coms_chrgs_foreign_acct_num() );
		attrList.add( TermsBean.Attributes.create_coms_chrgs_other_text() );
		attrList.add( TermsBean.Attributes.create_coms_chrgs_our_account_num() );
		attrList.add( TermsBean.Attributes.create_expiry_date() );
		attrList.add( TermsBean.Attributes.create_guar_bank_standard_text() );
		attrList.add( TermsBean.Attributes.create_guar_bank_std_phrase_type() );
		attrList.add( TermsBean.Attributes.create_guar_customer_text() );
		attrList.add( TermsBean.Attributes.create_guar_deliver_by() );
		attrList.add( TermsBean.Attributes.create_guar_deliver_to() );
		attrList.add( TermsBean.Attributes.create_guar_valid_from_date() );
		attrList.add( TermsBean.Attributes.create_internal_instructions() );
		attrList.add( TermsBean.Attributes.create_irrevocable() );
		attrList.add( TermsBean.Attributes.create_place_of_expiry() );
		attrList.add( TermsBean.Attributes.create_purpose_type() );
		attrList.add( TermsBean.Attributes.create_reference_number() );
		attrList.add( TermsBean.Attributes.create_settlement_foreign_acct_curr() );
		attrList.add( TermsBean.Attributes.create_settlement_foreign_acct_num() );
		attrList.add( TermsBean.Attributes.create_settlement_our_account_num() );
		attrList.add( TermsBean.Attributes.create_special_bank_instructions() );
		attrList.add( TermsBean.Attributes.create_ucp_details() );
		attrList.add( TermsBean.Attributes.create_ucp_version () );
		attrList.add( TermsBean.Attributes.create_ucp_version_details_ind() );
		attrList.add( TermsBean.Attributes.create_guar_valid_from_date_type() );
		attrList.add( TermsBean.Attributes.create_guar_expiry_date_type() );
		attrList.add( TermsBean.Attributes.create_tender_order_contract_details() );
		attrList.add( TermsBean.Attributes.create_guar_accept_instr() );
		attrList.add( TermsBean.Attributes.create_guar_advise_issuance() );
		attrList.add( TermsBean.Attributes.create_guarantee_issue_type() );
		attrList.add( TermsBean.Attributes.create_overseas_validity_date() );
        attrList.add( TermsBean.Attributes.create_work_item_number() );

      //Leelavathi - 10thDec2012 - Rel8200 CR-737 - Start
        attrList.add( TermsBean.Attributes.create_auto_extension_ind() );
        attrList.add( TermsBean.Attributes.create_max_auto_extension_allowed() );
		attrList.add( TermsBean.Attributes.create_auto_extension_period() );
		attrList.add( TermsBean.Attributes.create_auto_extension_days() );
	  //Leelavathi - 10thDec2012 - Rel8200 CR-737 - End	
		
	   //Narayan - 07-Jan-2014 - Rel9.0 CR-831 - Start
		attrList.add( TermsBean.Attributes.create_final_expiry_date() );
		attrList.add( TermsBean.Attributes.create_notify_bene_days() );
	   //Narayan - 07-Jan-2014 - Rel9.0 CR-831 - End
       
		return attrList;
	 }






	/**
	 * Performs presave processing (regardless of whether validation is done)
	 *
	 * @param terms com.ams.tradeportal.busobj.termsBean
	 * @exception com.amsinc.ecsg.frame.AmsException The exception description.
	 */
	public void preSave(TermsBean terms) throws AmsException
	{
           super.preSave(terms);

	    String value;

	    try {
			//The text length check is done in the presave to guard against
			//a possible problem with saving the data into the DB if it is too long.
		    validateTextLengths(terms);

		    //Check all amount entry fields
		    validateAmount(terms, "amount", "amount_currency_code");

	    } catch (java.rmi.RemoteException e) {
		    LOG.error("Remote Exception caught in EXP_COL__ISSTermsMgr.preSave()",e);
	    }

	}



	/**
	 *  Performs validation of the attributes of this type of instrument and transaction.   This method is
	 *  called from the userValidate() hook in the managed object.
	 *
	 *  @param terms - the bean being validated
	 */
	public void validate(TermsBean terms)
			throws java.rmi.RemoteException, AmsException
	 {
            super.validate(terms);

		 String value;
		 
	    terms.registerRequiredAttribute("amount_currency_code");
	    terms.registerRequiredAttribute("amount");
	    terms.registerRequiredAttribute("guar_valid_from_date_type");
	    /* KMehta commented as fix for CR-910 Irs on 1/7/2014 Start*/
	    //terms.registerRequiredAttribute("guar_expiry_date_type");
	    /* KMehta commented as fix for CR-910 Irs on 1/7/2014 End*/
	    terms.registerRequiredAttribute("guar_deliver_to");
	    terms.registerRequiredAttribute("guar_deliver_by");
	    // The second terms party is the Applicant.
	    terms.registerRequiredAttribute("c_SecondTermsParty");
		editRequiredBeneficiaryValue(terms, "FirstTermsParty");

		//Validate that there is text in "Customer Text" or "Bank Standard Wording" but not both.
		String customerText = terms.getAttribute("guar_customer_text");
		String bankWording = terms.getAttribute("guar_bank_standard_text");
		String foreignAccountNum = terms.getAttribute("settlement_foreign_acct_num");
		String currencyOfAccount = terms.getAttribute("settlement_foreign_acct_curr");

		LOG.debug("customerText -> {} " , customerText);
		LOG.debug("bankWording -> {}" , bankWording);
		boolean customerTextIsBlank = StringFunction.isBlank(customerText);
		boolean bankWordingIsBlank = StringFunction.isBlank(bankWording);
		//   both are blank                                 neither are blank
		if ((customerTextIsBlank && bankWordingIsBlank) || (!customerTextIsBlank && !bankWordingIsBlank))
			terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
											   TradePortalConstants.CUST_TEXT_OR_BANK_WORDING);

		//Validate that if a foreign currency account number is specified the currency of the account is also specified
		//and vice versa
		String errorVal1 = null;
		String errorVal2 = null;
		LOG.debug("foreignAccountNum -> {}" , foreignAccountNum);
		LOG.debug("currencyOfAccount -> {}" , currencyOfAccount);
		boolean foreignAccountNumIsBlank = StringFunction.isBlank(foreignAccountNum);
		boolean currencyOfAccountIsBlank = StringFunction.isBlank(currencyOfAccount);
		if (foreignAccountNumIsBlank && !currencyOfAccountIsBlank)
		{
			errorVal1 = terms.getResourceManager().getText( "GuaranteeIssue.DebitFCA",
				                                            TradePortalConstants.TEXT_BUNDLE);
			errorVal2 = terms.getResourceManager().getText( "GuaranteeIssue.SettlementInstructions",
				                                            TradePortalConstants.TEXT_BUNDLE);
			terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
											   TradePortalConstants.NOT_INDICATED_UNDER, errorVal1, errorVal2);
		}

		if (!foreignAccountNumIsBlank && currencyOfAccountIsBlank)
		{
			errorVal1 = terms.getResourceManager().getText( "GuaranteeIssue.CcyofAccount",
				                                            TradePortalConstants.TEXT_BUNDLE);
			errorVal2 = terms.getResourceManager().getText( "GuaranteeIssue.SettlementInstructions",
				                                            TradePortalConstants.TEXT_BUNDLE);
			terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
											   TradePortalConstants.NOT_INDICATED_UNDER, errorVal1, errorVal2);
		}

		//Validate that if a foreign currency account number is specified the currency of the account is also specified
		//and vice versa
		foreignAccountNum = terms.getAttribute("coms_chrgs_foreign_acct_num");
		currencyOfAccount = terms.getAttribute("coms_chrgs_foreign_acct_curr");
		foreignAccountNumIsBlank = StringFunction.isBlank(foreignAccountNum);
		currencyOfAccountIsBlank = StringFunction.isBlank(currencyOfAccount);
		LOG.debug("foreignAccountNum -> {}" , foreignAccountNum);
		LOG.debug("currencyOfAccount -> {}" ,currencyOfAccount);
		if (foreignAccountNumIsBlank && !currencyOfAccountIsBlank)
		{
			errorVal1 = terms.getResourceManager().getText( "GuaranteeIssue.DebitFCA",
				                                            TradePortalConstants.TEXT_BUNDLE);
			errorVal2 = terms.getResourceManager().getText( "GuaranteeIssue.CommissionsCharges",
				                                            TradePortalConstants.TEXT_BUNDLE);
			terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
											   TradePortalConstants.NOT_INDICATED_UNDER, errorVal1, errorVal2);
		}

		if (!foreignAccountNumIsBlank && currencyOfAccountIsBlank)
		{
			errorVal1 = terms.getResourceManager().getText( "GuaranteeIssue.CcyofAccount",
				                                            TradePortalConstants.TEXT_BUNDLE);
			errorVal2 = terms.getResourceManager().getText( "GuaranteeIssue.CommissionsCharges",
				                                            TradePortalConstants.TEXT_BUNDLE);
			terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
											   TradePortalConstants.NOT_INDICATED_UNDER, errorVal1, errorVal2);
		}

		// Validate that the value entered for field "Validity Date at Overseas Bank" should be less than
		// value entered for  "Validity Date" under "Valid To" field.
		try {
			Date validityDateAtOverseasBank = terms.getAttributeDate("overseas_validity_date");
			Date validityDateUnderValidTo = terms.getAttributeDate("expiry_date");
            // compare only if both date has value
			if(validityDateAtOverseasBank !=null && validityDateUnderValidTo !=null){
			 if( validityDateAtOverseasBank.getTime() > validityDateUnderValidTo.getTime() )
			  {
				terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
											   TradePortalConstants.MUST_BE_LESS_THAN,
											   terms.getAlias("overseas_validity_date"),
											   terms.getResourceManager().getText( "GuaranteeIssue.ValidToDate",
				                                            TradePortalConstants.TEXT_BUNDLE));
		 	 }
			}			
		}catch (AmsException e) {			
			// Probable cause is an unparseable date (because user did not
			// fill it in.  In this case, skip over the validation since we
			// can't validate invalid dates.
			 LOG.error("Exception while parsing date ",e);
		}

		validateFillInPhrase(terms, "guar_bank_standard_text");

		// Upon validation, if the issue type is applicant's bank, blank out
		// the overseas validity date and the overseas bank party
		value = terms.getAttribute("guarantee_issue_type");
		if (value.equals(TradePortalConstants.GUAR_ISSUE_APPLICANTS_BANK)) {
			terms.setAttribute("overseas_validity_date", "");
			removeTermsParty(terms, "ThirdTermsParty");

		}

		// Upon validation if the valid from date type is date of issue,
		// blank out the valid from date
		value = terms.getAttribute("guar_valid_from_date_type");
		if (value.equals(TradePortalConstants.DATE_OF_ISSUE)) {
			terms.setAttribute("guar_valid_from_date", "");
		}
		
		if(value.equals(TradePortalConstants.OTHER_VALID_FROM_DATE) && StringFunction.isBlank(terms.getAttribute("guar_valid_from_date"))){
			
			terms.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.FROM_OTHER_DATE_NOT_ENTERED);
		}

		// Upon validation if the valid to date is other, blank out the
		// validity date
		value = terms.getAttribute("guar_expiry_date_type");
		if (value.equals(TradePortalConstants.OTHER_EXPIRY_DATE)) {
			terms.setAttribute("expiry_date", "");
		}
		
		if(value.equals(TradePortalConstants.VALIDITY_DATE) && StringFunction.isBlank(terms.getAttribute("expiry_date"))){
			
			terms.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.TO_END_DATE_NOT_ENTERED);
		}
		
		// pcutrone - 10/17/2007 - REUH101146143 - Check to make sure the amount field is not set to zero.
		value = terms.getAttribute("amount");
		if (!StringFunction.isBlank(value)) {
			if (Double.parseDouble(value) == 0) {
				terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.AMOUNT_IS_ZERO);
			}
		}
		// pcutrone - 10/17/2007 - REUH101146143 - END
		
		//Leelavathi - 10thDec2012 - Rel8200 CR-737 - Start
		validateOtherCondtions(terms);
		//Leelavathi - 10thDec2012 - Rel8200 CR-737 - End

	 }


       /**
        * Performs the Terms Manager-specific validation of text fields in the Terms
        * and TermsParty objects.  
        *
        * @param terms - the managed object
        *
        */
        protected void validateForSwiftCharacters(TermsBean terms) throws AmsException, RemoteException
        {
           final String[] attributesToExclude = {"special_bank_instructions", "branch_code", "settlement_our_account_num",
                                           "settlement_foreign_acct_num", "coms_chrgs_our_account_num", "coms_chrgs_foreign_acct_num",
                                           "coms_chrgs_other_text", "internal_instructions"};
           
           // CR-594 - manohar 08/08/2011 - Begin
           String overrideSwiftValidationInd = getOverrideSwiftValidationInd(terms);
           String deliverBy = terms.getAttribute("guar_deliver_by");
           if (!TradePortalConstants.INDICATOR_YES.equalsIgnoreCase(overrideSwiftValidationInd) 
                   || TradePortalConstants.DELIVER_BY_TELEX.equalsIgnoreCase(deliverBy)) {
        	   InstrumentServices.checkForInvalidSwiftCharacters(terms.getAttributeHash(), terms.getErrorManager(), attributesToExclude);

              validateTermsPartyForSwiftChars(terms, "FirstTermsParty", "GuaranteeIssue.Beneficiary");
              validateTermsPartyForSwiftChars(terms, "SecondTermsParty", "GuaranteeIssue.ApplicantsBank");
              validateTermsPartyForSwiftChars(terms, "ThirdTermsParty", "GuaranteeIssue.OverseasBank1");
              validateTermsPartyForSwiftChars(terms, "FourthTermsParty", "GuaranteeIssue.Agent1");
           }// CR-594 - manohar 08/08/2011 - End
		else {
			validateTermsPartyForISO8859Chars(terms, "FirstTermsParty");
			validateTermsPartyForISO8859Chars(terms, "SecondTermsParty");
			validateTermsPartyForISO8859Chars(terms, "ThirdTermsParty");
			validateTermsPartyForISO8859Chars(terms, "FourthTermsParty");
		}

	}


	/**
	 *  Performs validation of text fields for length.
	 *
	 *  @param terms TermsBean - the bean being validated
	 */
	public void validateTextLengths(TermsBean terms)
	    throws java.rmi.RemoteException, AmsException {

	    checkTextLength(terms, "tender_order_contract_details", 500);
	    checkTextLength(terms, "guar_customer_text", 5000);
	    checkTextLength(terms, "guar_bank_standard_text", 5000);
	    checkTextLength(terms, "special_bank_instructions", 2000);
	    checkTextLength(terms, "coms_chrgs_other_text", 1000);
	    checkTextLength(terms, "internal_instructions", 1000);
	}

	//Leelavathi - 10thDec2012 - Rel8200 CR-737 - Start
	 /* Performs validation for the Other Conditions section.
	 *
	 * @param terms com.ams.tradeportal.busobj.TermsBean
	 */
	public void validateOtherCondtions(TermsBean terms) throws java.rmi.RemoteException, AmsException
	{
		String value;
		value = terms.getAttribute("auto_extension_ind");
		if (TradePortalConstants.INDICATOR_YES.equalsIgnoreCase(value))
		{
			int autoExtensionDays = StringFunction.isNotBlank(terms.getAttribute("auto_extension_days")) 
													? Integer.parseInt(terms.getAttribute("auto_extension_days")) :0;
			int maxAutoExtensionAllowed = StringFunction.isNotBlank(terms.getAttribute("max_auto_extension_allowed")) 
													? Integer.parseInt(terms.getAttribute("max_auto_extension_allowed")) :0;
			value = terms.getAttribute("auto_extension_period");
			
			if(StringFunction.isBlank(value)){
				terms.getErrorManager ().issueError (
		                   getClass (). getName (), AmsConstants.REQUIRED_ATTRIBUTE, "Auto Extension Period");
			}else if (value.equals("DAY") && autoExtensionDays ==0 ){
				terms.getErrorManager ().issueError (
		                   getClass (). getName (), TradePortalConstants.ATTRIBUTE_REQUIRED_BASED_CONDITION, 
						new String[]{"Auto Extension Days","Extension Period","Daily"});
			}else if( value.equals("DAY") && (autoExtensionDays <1 || autoExtensionDays >999 )){
				terms.getErrorManager ().issueError (
		                   getClass (). getName (), TradePortalConstants.ATTRIBUTE_VLAUE_RANGE, 
						new String[]{"Auto Extension Number of Days","0","1000"});
			}
			
					
			//Narayan - 09-Jan-2014 - Rel9.0 CR-831 - Start			
			try {
				Date finalExpiryDate = terms.getAttributeDate("final_expiry_date");					
				//If Auto Extension is selected and the Final Expiry Date and Maximum Number are both blank then display a Warning
				if(finalExpiryDate == null ){
					if(StringFunction.isBlank(terms.getAttribute("max_auto_extension_allowed"))){
						terms.getErrorManager().issueError(getClass().getName(), TradePortalConstants.EXPIRY_DATE_OR_MAX_NUM_NOT_PRESENT_WARN);
				    }else if (maxAutoExtensionAllowed <1 || maxAutoExtensionAllowed >999 ){
							terms.getErrorManager ().issueError (
					                   getClass (). getName (), TradePortalConstants.ATTRIBUTE_VLAUE_RANGE, 
									new String[]{"Auto Extension Maximum Number","0","1000"});
				    }
				} else {
					Date expiryDate = terms.getAttributeDate("expiry_date");
				    if( expiryDate != null && expiryDate.getTime() >= finalExpiryDate.getTime() )
				    {
					  terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
												   TradePortalConstants.EXPIRY_DATE_MUST_BE_LESS_THAN);
			 	    }
				}			
			}catch (AmsException e) {			
				// Probable cause is an unparseable date (because user did not
				// fill it in.  In this case, skip over the validation since we
				// can't validate invalid dates.
				 LOG.error("Exception while parsing date ",e);
			}
			//Narayan - 09-Jan-2014 - Rel9.0 CR-831 - End						
		}
		
		//Sandeep - Rel-8.3 CR-752 Dev 06/11/2013 - Begin
		value = terms.getAttribute("ucp_version_details_ind");
		
		if (TradePortalConstants.INDICATOR_YES.equals(value)) {
			value = terms.getAttribute("ucp_version");
			
			if(StringFunction.isBlank(value)){
				terms.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						AmsConstants.REQUIRED_ATTRIBUTE,
						terms.getResourceManager().getText( "common.ICCVersionRequired", TradePortalConstants.TEXT_BUNDLE)
						);
			}
			
			if(TradePortalConstants.IMP_OTHER.equals(value)){
				value = terms.getAttribute("ucp_details");
				if(StringFunction.isBlank(value)){
					terms.getErrorManager().issueError(
							TradePortalConstants.ERR_CAT_1,
							TradePortalConstants.ICC_PUBLICATION_DETAILS_CHECK);
				}
			}
		}
		//Sandeep - Rel-8.3 CR-752 Dev 06/11/2013 - End
	}
	//Leelavathi - 10thDec2012 - Rel8200 CR-737 - End
	 //Beginning of code used by the Middleware team
	    /**
		 *  gets the values of the attributes of the managed object. This method is called
		 *  from the getTermsAttributesDoc() hook in the managed object.
		 *
		 *  return com.amsinc.ecsg.util.DocumentHandler
		 *  @param terms TermsBean - the bean being validated
		 *  @param termsDoc com.amsinc.ecsg.util.DocumentHandler
		 */
	   public DocumentHandler getManagerTerms(TermsBean terms, DocumentHandler termsDoc) throws java.rmi.RemoteException, AmsException

	   {

	   	         DocumentHandler termsValuesDoc = terms.populateXmlDoc(new DocumentHandler());
	             LOG.debug("This is the termValuesDoc containing all the terms values: {}" ,termsValuesDoc.toString());


		   //Terms_Details
			termsDoc.setAttribute("/Terms/TermsDetails/PlaceOfExpiry", 	       				   termsValuesDoc.getAttribute("/Terms/place_of_expiry"));
		    termsDoc.setAttribute("/Terms/TermsDetails/Irrevocable",	                       termsValuesDoc.getAttribute("/Terms/irrevocable"));
			termsDoc.setAttribute("/Terms/TermsDetails/ExpiryDate",			                   termsValuesDoc.getAttribute("/Terms/expiry_date"));
			termsDoc.setAttribute("/Terms/TermsDetails/ReferenceNumber",	                   termsValuesDoc.getAttribute("/Terms/reference_number"));
			termsDoc.setAttribute("/Terms/TermsDetails/AmountCurrencyCode",       	           termsValuesDoc.getAttribute("/Terms/amount_currency_code"));
			termsDoc.setAttribute("/Terms/TermsDetails/Amount",                    	           termsValuesDoc.getAttribute("/Terms/amount"));
			termsDoc.setAttribute("/Terms/TermsDetails/CharacteristicType",                    termsValuesDoc.getAttribute("/Terms/characteristic_type"));
			termsDoc.setAttribute("/Terms/TermsDetails/UcpVersion",							   termsValuesDoc.getAttribute("/Terms/ucp_version"));
			termsDoc.setAttribute("/Terms/TermsDetails/UcpDetails",				               termsValuesDoc.getAttribute("/Terms/ucp_details"));
			termsDoc.setAttribute("/Terms/TermsDetails/UcpIndicator",                          termsValuesDoc.getAttribute("/Terms/ucp_version_details_ind"));
			termsDoc.setAttribute("/Terms/TermsDetails/GuaTerms/GuaAcceptanceInstruction",	   termsValuesDoc.getAttribute("/Terms/guar_accept_instr"));
			termsDoc.setAttribute("/Terms/TermsDetails/GuaTerms/GuaAdviseIssuance",	           termsValuesDoc.getAttribute("/Terms/guar_advise_issuance"));
			termsDoc.setAttribute("/Terms/TermsDetails/PurposeType",		        	       termsValuesDoc.getAttribute("/Terms/purpose_type"));
			termsDoc.setAttribute("/Terms/TermsDetails/GuaTerms/GuaDeliverBy",			       termsValuesDoc.getAttribute("/Terms/guar_deliver_by"));
			termsDoc.setAttribute("/Terms/TermsDetails/GuaTerms/GuaDeliverTo",		           termsValuesDoc.getAttribute("/Terms/guar_deliver_to"));
			termsDoc.setAttribute("/Terms/TermsDetails/GuaTerms/GuaExpiryDateType",            termsValuesDoc.getAttribute("/Terms/guar_expiry_date_type"));
			termsDoc.setAttribute("/Terms/TermsDetails/GuaTerms/GuaValidFromDate",		       termsValuesDoc.getAttribute("/Terms/guar_valid_from_date"));
			termsDoc.setAttribute("/Terms/TermsDetails/GuaTerms/GuaValidFromDateType",		   termsValuesDoc.getAttribute("/Terms/guar_valid_from_date_type"));
			termsDoc.setAttribute("/Terms/TermsDetails/GuaTerms/GuaIssueType",			       termsValuesDoc.getAttribute("/Terms/guarantee_issue_type"));
			termsDoc.setAttribute("/Terms/TermsDetails/GuaTerms/OverseasValidityDate",		   termsValuesDoc.getAttribute("/Terms/overseas_validity_date"));
			termsDoc.setAttribute("/Terms/TermsDetails/GuaTerms/TenderOrderContractDetail",    termsValuesDoc.getAttribute("/Terms/tender_order_contract_details"));
			termsDoc.setAttribute("/Terms/TermsDetails/GuaTerms/GuaCustomerText",		       termsValuesDoc.getAttribute("/Terms/guar_customer_text"));
			termsDoc.setAttribute("/Terms/TermsDetails/GuaTerms/GuaBankStandardText",		   termsValuesDoc.getAttribute("/Terms/guar_bank_standard_text"));
		   //End of Terms_Details
			
	        //Leelavathi - 10thDec2012 - Rel8200 CR-737 - Start
     	    termsDoc.setAttribute("/Terms/TermsDetails/AutoExtendTerms/AutoExtendIndicator", termsValuesDoc.getAttribute("/Terms/auto_extension_ind"));
     	    termsDoc.setAttribute("/Terms/TermsDetails/AutoExtendTerms/AutoExtendPeriod",	termsValuesDoc.getAttribute("/Terms/auto_extension_period"));     	    
		    if(TradePortalConstants.INDICATOR_YES.equals(termsValuesDoc.getAttribute("/Terms/auto_extension_ind")) && 
		    		StringFunction.isBlank(termsValuesDoc.getAttribute("/Terms/max_auto_extension_allowed")) && 
		    		StringFunction.isBlank(termsValuesDoc.getAttribute("/Terms/final_expiry_date"))){
		    	termsDoc.setAttribute("/Terms/TermsDetails/AutoExtendTerms/MaximumNo", TradePortalConstants.MAX_AUTO_EXTENSION_ALLOWED);
		    	
		    }else{
		    	termsDoc.setAttribute("/Terms/TermsDetails/AutoExtendTerms/MaximumNo", termsValuesDoc.getAttribute("/Terms/max_auto_extension_allowed"));
		    }
		    termsDoc.setAttribute("/Terms/TermsDetails/AutoExtendTerms/NumberOfDays", termsValuesDoc.getAttribute("/Terms/auto_extension_days"));
     	    //Leelavathi - 10thDec2012 - Rel8200 CR-737 - End
		    //Narayan - 14/Jan/2014 - Rel9.0 CR-831 - Start
		    termsDoc.setAttribute("/Terms/TermsDetails/AutoExtendTerms/BeneficiaryNotifyDays", termsValuesDoc.getAttribute("/Terms/notify_bene_days"));
		    termsDoc.setAttribute("/Terms/TermsDetails/AutoExtendTerms/FinalExpiryDate", termsValuesDoc.getAttribute("/Terms/final_expiry_date"));
		    //Narayan - 14/Jan/2014 - Rel9.0 CR-831 - End		    
			//Instructions
			termsDoc.setAttribute("/Terms/Instructions/InstructionsToBank",			termsValuesDoc.getAttribute("/Terms/special_bank_instructions"));

	  		//SettlementInstructions
		    termsDoc.setAttribute("/Terms/Instructions/SettlementInstructions/BranchCode",					        termsValuesDoc.getAttribute("/Terms/branch_code"));
		    termsDoc.setAttribute("/Terms/Instructions/SettlementInstructions/SettlementOurAccountNumber",			termsValuesDoc.getAttribute("/Terms/settlement_our_account_num"));
  		    termsDoc.setAttribute("/Terms/Instructions/SettlementInstructions/SettlementForeignAccountNumber",		termsValuesDoc.getAttribute("/Terms/settlement_foreign_acct_num"));
  		    termsDoc.setAttribute("/Terms/Instructions/SettlementInstructions/SettlementForeignAccountCurrency",	termsValuesDoc.getAttribute("/Terms/settlement_foreign_acct_curr"));
			//End of SettlementInstructions

			//CommissionAndCharges
			termsDoc.setAttribute("/Terms/Instructions/CommissionAndCharges/CommissionChargesOurAccountNumber",			termsValuesDoc.getAttribute("/Terms/coms_chrgs_our_account_num"));
		    termsDoc.setAttribute("/Terms/Instructions/CommissionAndCharges/CommissionChargesForeignAccountNumber",		termsValuesDoc.getAttribute("/Terms/coms_chrgs_foreign_acct_num"));
  		    termsDoc.setAttribute("/Terms/Instructions/CommissionAndCharges/CommissionChargesForeignAccountCurrency",	termsValuesDoc.getAttribute("/Terms/coms_chrgs_foreign_acct_curr"));
  		    termsDoc.setAttribute("/Terms/Instructions/CommissionAndCharges/CommissionChargesOtherText",			    termsValuesDoc.getAttribute("/Terms/coms_chrgs_other_text"));
			//End of CommissionCharges

		           //Terms Party
	               DocumentHandler termsPartyDoc = new DocumentHandler();

	                //Additional Terms Party
	                String testTP = TermsPartyType.APPLICANT;


	             int id = 0;

	            termsPartyDoc.setAttribute("/TermsParty/TotalNumberOfEntries","0");

	            for (int i = 0; i<TradePortalConstants.MAX_NUM_TERMS_PARTIES;i++)
	   			 {
					String termsPartyOID = termsValuesDoc.getAttribute("/Terms/"+TradePortalConstants.TERMS_PARTY_ATTRIBUTE_NAMES[i]);

	   		      if(!(termsPartyOID == null || termsPartyOID.equals("")))
	   		      {
	   				 TermsParty termsParty = (TermsParty)(terms.getComponentHandle(TradePortalConstants.TERMS_PARTY_COMPONENT_NAMES[i]));
	   				 if(termsParty.getAttribute("terms_party_type").equals(testTP))
	   				 {
	   					 LOG.debug("party_Type is APP, hence I need to check for thirdParty");
	   					 termsPartyDoc = packageAdditionalTermsParty(terms,id,termsPartyDoc,termsParty);
	   					 if(termsPartyDoc.getAttribute("/AdditionalTermsParty").equals("Y"))
	   					  id++;
	   				 }
	   				 termsPartyDoc.setAttribute("/AdditionalTermsParty","N");
	   				 //If the termsValuesDoc contains termsParty also need to modify the line below
	   			     termsPartyDoc = termsParty.packageTermsPartyAttributes(id,termsPartyDoc);
	   			     id++;
	   			   }
			      }

			 LOG.debug("This is TermsPartyDoc: {} " , termsPartyDoc.toString());

	         termsPartyDoc.setAttribute("/TermsParty/TotalNumberOfEntries",String.valueOf(id));
	         termsDoc.setComponent("/TermsParty",termsPartyDoc.getFragment("/TermsParty"));


	   		return termsDoc;
	   }

	     //End of Code by Indu Valavala

 }