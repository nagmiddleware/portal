
  
/*
 * This file is generated from the model.  Normally it should not be modified manually.  
 * Contact the modeler first.  (In a pinch, it can be modified first and then retrofitted 
 * to the model.)
 *
 */
package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;




/*
 *
 *
 *     Copyright  � 2003
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class DomesticPaymentBean_Base extends TradePortalBusinessObjectBean
{
private static final Logger LOG = LoggerFactory.getLogger(DomesticPaymentBean_Base.class);


  /*
   * Register the attributes and associations of the business object
   */
   protected void registerAttributes() throws AmsException
   {

      /* Register attributes defined in the Ancestor class */
      super.registerAttributes();

      /* domestic_payment_oid - Unique identifier */
      attributeMgr.registerAttribute("domestic_payment_oid", "domestic_payment_oid", "ObjectIDAttribute");

      /* amount - The amount of the domestic payment. This is validated to have the correct
         number of decimal places based on the currency selected.   For amendments,
         this attribute is allowed to have a negative value.  For all other transactions,
         this attribute must be validated that it is a positive number. */
      attributeMgr.registerAttribute("amount", "amount", "TradePortalSignedDecimalAttribute", "com.ams.tradeportal.common");
      attributeMgr.registerAlias("amount", getResourceManager().getText("DomesticPaymentBeanAlias.amount", TradePortalConstants.TEXT_BUNDLE));

      /* amount_currency_code - The currency for the instrument. */
      attributeMgr.registerReferenceAttribute("amount_currency_code", "amount_currency_code", "CURRENCY_CODE");
      attributeMgr.registerAlias("amount_currency_code", getResourceManager().getText("DomesticPaymentBeanAlias.amount_currency_code", TradePortalConstants.TEXT_BUNDLE));

      /* payee_name - Name of the payee involved in the Domestic Payment. */
      attributeMgr.registerAttribute("payee_name", "payee_name");
      attributeMgr.registerAlias("payee_name", getResourceManager().getText("DomesticPaymentBeanAlias.payee_name", TradePortalConstants.TEXT_BUNDLE));

      /* payee_account_number - The bank account number for the payee */
      attributeMgr.registerAttribute("payee_account_number", "payee_account_number");
      attributeMgr.registerAlias("payee_account_number", getResourceManager().getText("DomesticPaymentBeanAlias.payee_account_number", TradePortalConstants.TEXT_BUNDLE));

      /* address_line_1 - First line of the address for this payee's bank. */
      attributeMgr.registerAttribute("address_line_1", "address_line_1");
      attributeMgr.registerAlias("address_line_1", getResourceManager().getText("DomesticPaymentBeanAlias.address_line_1", TradePortalConstants.TEXT_BUNDLE));

      /* address_line_2 - Second line of the address for this payee's bank. */
      attributeMgr.registerAttribute("address_line_2", "address_line_2");
      attributeMgr.registerAlias("address_line_2", getResourceManager().getText("DomesticPaymentBeanAlias.address_line_2", TradePortalConstants.TEXT_BUNDLE));

      /* address_line_3 - Third line of the address for this payee's bank.

 */
      attributeMgr.registerAttribute("address_line_3", "address_line_3");
      attributeMgr.registerAlias("address_line_3", getResourceManager().getText("DomesticPaymentBeanAlias.address_line_3", TradePortalConstants.TEXT_BUNDLE));

      /* bank_charges_type - Indicates the distribution of bank charges for the instrument. */
      attributeMgr.registerReferenceAttribute("bank_charges_type", "bank_charges_type", "BANK_CHARGES_TYPE");
      attributeMgr.registerAlias("bank_charges_type", getResourceManager().getText("DomesticPaymentBeanAlias.bank_charges_type", TradePortalConstants.TEXT_BUNDLE));

      /* payee_bank_code - Beneficiary Bank Code and Branch Code combined.

         * this field will only be required for Electronic Payments ( where Payment
         method = ACH/GIRO, RTGS, BKT)
 */
      attributeMgr.registerAttribute("payee_bank_code", "payee_bank_code");
      /*#KMEhta IR T36000026662 on 1st Apr 2014 Start*/
      attributeMgr.registerAlias("payee_bank_code", getResourceManager().getText("DomesticPaymentBeanAlias.payee_bank_code", TradePortalConstants.TEXT_BUNDLE));
      /*#KMEhta IR T36000026662 on 1st Apr 2014 End*/
      /* payee_description - This field allows for a text description to be entered as an explanation
         of the payment for the recipient. */
      attributeMgr.registerAttribute("payee_description", "payee_description");
      attributeMgr.registerAlias("payee_description", getResourceManager().getText("DomesticPaymentBeanAlias.payee_description", TradePortalConstants.TEXT_BUNDLE));

      /* payee_bank_name - The name of the Payee's Bank. */
      attributeMgr.registerAttribute("payee_bank_name", "payee_bank_name");
      attributeMgr.registerAlias("payee_bank_name", getResourceManager().getText("DomesticPaymentBeanAlias.payee_bank_name", TradePortalConstants.TEXT_BUNDLE));

      /* payee_address_line_1 - First line of the Beneficiary's address */
      attributeMgr.registerAttribute("payee_address_line_1", "payee_address_line_1");
      attributeMgr.registerAlias("payee_address_line_1", getResourceManager().getText("DomesticPaymentBeanAlias.payee_address_line_1", TradePortalConstants.TEXT_BUNDLE));

      /* payee_address_line_2 - Second line of the beneficiary's address. */
      attributeMgr.registerAttribute("payee_address_line_2", "payee_address_line_2");
      attributeMgr.registerAlias("payee_address_line_2", getResourceManager().getText("DomesticPaymentBeanAlias.payee_address_line_2", TradePortalConstants.TEXT_BUNDLE));

      /* payee_address_line_3 - Third line of the beneficiary's address. */
      attributeMgr.registerAttribute("payee_address_line_3", "payee_address_line_3");
      attributeMgr.registerAlias("payee_address_line_3", getResourceManager().getText("DomesticPaymentBeanAlias.payee_address_line_3", TradePortalConstants.TEXT_BUNDLE));

      /* payment_method_type - Payment Method Type. */
      attributeMgr.registerReferenceAttribute("payment_method_type", "payment_method_type", "PAYMENT_METHOD");
      attributeMgr.registerAlias("payment_method_type", getResourceManager().getText("DomesticPaymentBeanAlias.payment_method_type", TradePortalConstants.TEXT_BUNDLE));

      /* value_date - The value date is equal to the Execution Date + the # of Offset days that
         are valid for the Payment method and country of the debit account selected. */
      attributeMgr.registerAttribute("value_date", "value_date", "DateAttribute");
      attributeMgr.registerAlias("value_date", getResourceManager().getText("DomesticPaymentBeanAlias.value_date", TradePortalConstants.TEXT_BUNDLE));

      /* country - Beneficiary's country */
      attributeMgr.registerReferenceAttribute("country", "country", "COUNTRY");
      attributeMgr.registerAlias("country", getResourceManager().getText("DomesticPaymentBeanAlias.country", TradePortalConstants.TEXT_BUNDLE));

      /* payee_address_line_4 - Additional line used for capturing the beneficiary address. This will be
         used primarily for check payments */
      attributeMgr.registerAttribute("payee_address_line_4", "payee_address_line_4");
      attributeMgr.registerAlias("payee_address_line_4", getResourceManager().getText("DomesticPaymentBeanAlias.payee_address_line_4", TradePortalConstants.TEXT_BUNDLE));

      /* payee_fax_number - Beneficiary's Fax Number */
      attributeMgr.registerAttribute("payee_fax_number", "payee_fax_number");
      attributeMgr.registerAlias("payee_fax_number", getResourceManager().getText("DomesticPaymentBeanAlias.payee_fax_number", TradePortalConstants.TEXT_BUNDLE));

      /* payee_email - Beneficiary's email */
      attributeMgr.registerAttribute("payee_email", "payee_email");
      attributeMgr.registerAlias("payee_email", getResourceManager().getText("DomesticPaymentBeanAlias.payee_email", TradePortalConstants.TEXT_BUNDLE));

      /* payee_instruction_number - Instruction Number to be printed on the cheques- for other payment types
         it will be printed on beneficiary advice */
      attributeMgr.registerAttribute("payee_instruction_number", "payee_instruction_number");
      attributeMgr.registerAlias("payee_instruction_number", getResourceManager().getText("DomesticPaymentBeanAlias.payee_instruction_number", TradePortalConstants.TEXT_BUNDLE));

      /* payee_branch_code -  */
      attributeMgr.registerAttribute("payee_branch_code", "payee_branch_code");
      attributeMgr.registerAlias("payee_branch_code", getResourceManager().getText("DomesticPaymentBeanAlias.payee_branch_code", TradePortalConstants.TEXT_BUNDLE));

      /* delivery_method - Identify the method to be used when a customer wants a cheques to be delivered
         once printed and to whom it will be delivered. */
      attributeMgr.registerReferenceAttribute("delivery_method", "delivery_method", "DELIVERY_METHOD_DELIVER_TO");
      attributeMgr.registerAlias("delivery_method", getResourceManager().getText("DomesticPaymentBeanAlias.delivery_method", TradePortalConstants.TEXT_BUNDLE));

      /* payable_location - Cheque's payable location. */
      attributeMgr.registerAttribute("payable_location", "payable_location");
      attributeMgr.registerAlias("payable_location", getResourceManager().getText("DomesticPaymentBeanAlias.payable_location", TradePortalConstants.TEXT_BUNDLE));

      /* print_location - Cheque's print location. */
      attributeMgr.registerAttribute("print_location", "print_location");
      attributeMgr.registerAlias("print_location", getResourceManager().getText("DomesticPaymentBeanAlias.print_location", TradePortalConstants.TEXT_BUNDLE));

      /* mailing_address_line_1 - First line of the mailing address used for sending cheques. */
      attributeMgr.registerAttribute("mailing_address_line_1", "mailing_address_line_1");
      attributeMgr.registerAlias("mailing_address_line_1", getResourceManager().getText("DomesticPaymentBeanAlias.mailing_address_line_1", TradePortalConstants.TEXT_BUNDLE));

      /* mailing_address_line_2 - Second line of the mailing address used for sending cheques. */
      attributeMgr.registerAttribute("mailing_address_line_2", "mailing_address_line_2");
      attributeMgr.registerAlias("mailing_address_line_2", getResourceManager().getText("DomesticPaymentBeanAlias.mailing_address_line_2", TradePortalConstants.TEXT_BUNDLE));

      /* mailing_address_line_3 - Third line of the mailing address used for sending cheques. */
      attributeMgr.registerAttribute("mailing_address_line_3", "mailing_address_line_3");
      attributeMgr.registerAlias("mailing_address_line_3", getResourceManager().getText("DomesticPaymentBeanAlias.mailing_address_line_3", TradePortalConstants.TEXT_BUNDLE));

      /* mailing_address_line_4 - Fourth line of the mailing address used for sending cheques. */
      attributeMgr.registerAttribute("mailing_address_line_4", "mailing_address_line_4");
      attributeMgr.registerAlias("mailing_address_line_4", getResourceManager().getText("DomesticPaymentBeanAlias.mailing_address_line_4", TradePortalConstants.TEXT_BUNDLE));

      /* payee_branch_name -  */
      attributeMgr.registerAttribute("payee_branch_name", "payee_branch_name");
      attributeMgr.registerAlias("payee_branch_name", getResourceManager().getText("DomesticPaymentBeanAlias.payee_branch_name", TradePortalConstants.TEXT_BUNDLE));

      /* customer_reference -  */
      attributeMgr.registerAttribute("customer_reference", "customer_reference");
      attributeMgr.registerAlias("customer_reference", getResourceManager().getText("DomesticPaymentBeanAlias.customer_reference", TradePortalConstants.TEXT_BUNDLE));

      /* other_charges_instructions -  */
      attributeMgr.registerAttribute("other_charges_instructions", "other_charges_instructions");
      attributeMgr.registerAlias("other_charges_instructions", getResourceManager().getText("DomesticPaymentBeanAlias.other_charges_instructions", TradePortalConstants.TEXT_BUNDLE));

      /* central_bank_reporting_1 -  */
      attributeMgr.registerAttribute("central_bank_reporting_1", "central_bank_reporting_1");
      attributeMgr.registerAlias("central_bank_reporting_1", getResourceManager().getText("DomesticPaymentBeanAlias.central_bank_reporting_1", TradePortalConstants.TEXT_BUNDLE));

      /* central_bank_reporting_2 -  */
      attributeMgr.registerAttribute("central_bank_reporting_2", "central_bank_reporting_2");
      attributeMgr.registerAlias("central_bank_reporting_2", getResourceManager().getText("DomesticPaymentBeanAlias.central_bank_reporting_2", TradePortalConstants.TEXT_BUNDLE));

      /* central_bank_reporting_3 -  */
      attributeMgr.registerAttribute("central_bank_reporting_3", "central_bank_reporting_3");
      attributeMgr.registerAlias("central_bank_reporting_3", getResourceManager().getText("DomesticPaymentBeanAlias.central_bank_reporting_3", TradePortalConstants.TEXT_BUNDLE));

      //BSL CR-655 02/22/11 Begin
      /* reporting_code_1 -  */
      attributeMgr.registerAttribute("reporting_code_1", "reporting_code_1");
      attributeMgr.registerAlias("reporting_code_1", getResourceManager().getText("DomesticPaymentBeanAlias.reporting_code_1", TradePortalConstants.TEXT_BUNDLE));

      /* reporting_code_2 -  */
      attributeMgr.registerAttribute("reporting_code_2", "reporting_code_2");
      attributeMgr.registerAlias("reporting_code_2", getResourceManager().getText("DomesticPaymentBeanAlias.reporting_code_2", TradePortalConstants.TEXT_BUNDLE));
      //BSL CR-655 02/22/11 End

      /* payee_bank_country - Beneficiary Bank's country. */
      attributeMgr.registerReferenceAttribute("payee_bank_country", "payee_bank_country", "COUNTRY");
      attributeMgr.registerAlias("payee_bank_country", getResourceManager().getText("DomesticPaymentBeanAlias.payee_bank_country", TradePortalConstants.TEXT_BUNDLE));
      
      /* source_template_dp_oid - oid of the template dp from where it was originally copied */
      attributeMgr.registerAttribute("source_template_dp_oid", "source_template_dp_oid", "NumberAttribute");
      
      /* sequence_number - Sequence Number of this DomesticPayment within the transaction. */
      attributeMgr.registerAttribute("sequence_number", "sequence_number", "NumberAttribute");
      
      /* payment_status - Beneficiary Payment Status.   */
      attributeMgr.registerReferenceAttribute("payment_status", "payment_status", "BENEFICIARY_PAYMENT_STATUS");
      
      /* payment_system_ref - The beneficiary level reference returned from the payment/clearing system
         which can be used to reference the individual payment in correspondence
         with the Payment system. */
      attributeMgr.registerAttribute("payment_system_ref", "payment_system_ref");
      
      /* error_text - Error text can be returned from the Payment system or can be manually entered
         in the TPS to be sent to the Portal. The Error Text, if present, should
         indicate why a specific beneficiary payment has been rejected. */
      attributeMgr.registerAttribute("error_text", "error_text");
      
      /* payment_ben_email_sent_flag - Whether the email has been sent to the beneficiary. */
      attributeMgr.registerAttribute("payment_ben_email_sent_flag", "payment_ben_email_sent_flag", "IndicatorAttribute");
      
        /* Pointer to the parent Transaction */
      attributeMgr.registerAttribute("transaction_oid", "p_transaction_oid", "ParentIDAttribute");

      /* Pointer to the component PaymentParty */
      /* FirstIntermediaryBank -  */
      attributeMgr.registerAttribute("c_FirstIntermediaryBank", "c_FIRST_INTERMEDIARY_BANK", "NumberAttribute");

      /* Pointer to the component PaymentParty */
      /* SecondIntermediaryBank -  */
      attributeMgr.registerAttribute("c_SecondIntermediaryBank", "c_SECOND_INTERMEDIARY_BANK", "NumberAttribute");

      /* beneficiary_party_oid -  */
      attributeMgr.registerAssociation("beneficiary_party_oid", "a_beneficiary_party_oid", "Party");
	  //BSL IR#PUL032965444 03/31/11 Remove
	  /* SHILPAR CR-597 - oid of INVOICE_DETAIL*/
      //attributeMgr.registerAttribute("invoice_detail_oid", "invoice_detail_oid", "NumberAttribute");

      /* MANOHAR CR-597 - User Defined fields 1-10*/
      attributeMgr.registerAttribute("user_defined_field_1", "user_defined_field_1");
      attributeMgr.registerAttribute("user_defined_field_2", "user_defined_field_2");
      attributeMgr.registerAttribute("user_defined_field_3", "user_defined_field_3");
      attributeMgr.registerAttribute("user_defined_field_4", "user_defined_field_4");
      attributeMgr.registerAttribute("user_defined_field_5", "user_defined_field_5");
      attributeMgr.registerAttribute("user_defined_field_6", "user_defined_field_6");
      attributeMgr.registerAttribute("user_defined_field_7", "user_defined_field_7");
      attributeMgr.registerAttribute("user_defined_field_8", "user_defined_field_8");
      attributeMgr.registerAttribute("user_defined_field_9", "user_defined_field_9");
      attributeMgr.registerAttribute("user_defined_field_10", "user_defined_field_10");
      
       // Narayan Rel 8.3 CR 857 07/15/2013 Start
      /* dmst_pmt_panel_auth_range - panel range associated to beneficiary. */
      attributeMgr.registerAttribute("dmst_pmt_panel_auth_range", "a_dmst_pmt_panel_auth_range", "NumberAttribute");
     // Narayan Rel 8.3 CR 857 07/15/2013 End
      
      //CR913 - only for Payables
      attributeMgr.registerAttribute("payable_payment_seq_num", "payable_payment_seq_num", "NumberAttribute");
      
      // Narayan Rel 9.2 CR 966 09/15/2014 Start
      /*allow_pay_amt_modification - indicator to check where amount field is editable or not for fixed payment*/
      attributeMgr.registerAttribute("allow_pay_amt_modification", "allow_pay_amt_modification", "IndicatorAttribute");
      
      /*allow_pay_detail_modification - indicator to check where amount field is editable or not for fixed payment*/
      attributeMgr.registerAttribute("allow_pay_detail_modification",  "allow_pay_detail_modification", "IndicatorAttribute");
      
      /*allow_inv_detail_modification - indicator to check where amount field is editable or not for fixed payment*/
      attributeMgr.registerAttribute("allow_inv_detail_modification",  "allow_inv_detail_modification", "IndicatorAttribute");     
      // Narayan Rel 9.2 CR 966 09/15/2014 End
      
   }

  /*
   * Register the components of the business object
   */
   protected void registerComponents() throws RemoteException, AmsException
   {
      /* Register the components defined in the Ancestor class */
      super.registerComponents();

      /* FirstIntermediaryBank -  */
      registerOneToOneComponent("FirstIntermediaryBank", "PaymentParty", "c_FirstIntermediaryBank");

      /* SecondIntermediaryBank -  */
      registerOneToOneComponent("SecondIntermediaryBank", "PaymentParty", "c_SecondIntermediaryBank");
      }






}
