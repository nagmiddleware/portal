


package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;
/**
 * A threshold group is a set of threshold settings.  There are two types of
 * thresholds: a LC threshold and a daily limit threshold.  An LC threshold
 * a rule that indicates the maximum value of an LC that the user can authorize.
 * A daily limit threshold is the maximum total value that a user can authorize
 * in a single day.
 *
 * An LC threshold and a daily threshold exists for every type of instrument
 * type / transaction type combination that can be created in the Trade Portal.
 *
 *     Copyright  � 2003
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public interface ThresholdGroupHome extends EJBHome
{
   public ThresholdGroup create()
      throws RemoteException, CreateException, AmsException;

   public ThresholdGroup create(ClientServerDataBridge csdb)
      throws RemoteException, CreateException, AmsException;
}
