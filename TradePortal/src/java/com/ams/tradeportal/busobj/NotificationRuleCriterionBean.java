 
package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;

/*
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */



/**
 * This EJB class holds notification rule criterion objects.

 *
 */
public class NotificationRuleCriterionBean extends NotificationRuleCriterionBean_Base
{

  /**
   * Descendent implemented method that identifies listing object
   * criteria.  
   * 
   * Used to identify one or more listTypes and list processing criteria.
   *
   * @param listTypeName The name of the listtype (as specified by the
   * developer)
   * @return The qsuedo SQL used for specifying the list criteria.
   *
   *			      
   */
  public String getListCriteria(String type)
  {
    if (type.equals("forCorpOrgTransaction"))
      {
	 return "p_notify_rule_oid  = {0} and instrument_type_or_category  = {1} and transaction_type_or_action  = {2}" ;

      }
      return "";
  }
  
  @Override
	protected void userValidate() throws AmsException, RemoteException {
	
	}

	  
}
