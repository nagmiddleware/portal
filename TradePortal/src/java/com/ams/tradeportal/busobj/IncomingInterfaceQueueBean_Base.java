
  
/*
 * This file is generated from the model.  Normally it should not be modified manually.  
 * Contact the modeler first.  (In a pinch, it can be modified first and then retrofitted 
 * to the model.)
 *
 */
package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;


/*
 * This business object stores data that has been received from the middleware,
 * but has not yet been processed by the InboundAgent.   
 * The MQAgent picks data up off of the queue and places it into this table.
 * The InboundAgent picks data up off of this table and processes it.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class IncomingInterfaceQueueBean_Base extends TradePortalBusinessObjectBean
{
private static final Logger LOG = LoggerFactory.getLogger(IncomingInterfaceQueueBean_Base.class);

  
  /* 
   * Register the attributes and associations of the business object
   */
   protected void registerAttributes() throws AmsException
   {  

      /* Register attributes defined in the Ancestor class */
      super.registerAttributes();
      
      /* queue_oid - Unique identifier */
      attributeMgr.registerAttribute("queue_oid", "queue_oid", "ObjectIDAttribute");
      
      /* date_received - Timestamp in GMT of when the message was received */
      attributeMgr.registerAttribute("date_received", "date_received", "DateTimeAttribute");
      
      /* date_sent - Timestamp in GMT of when this message was sent in OTL */
      attributeMgr.registerAttribute("date_sent", "date_sent", "DateTimeAttribute");
      
      /* msg_text - The XML data for the message */
      attributeMgr.registerAttribute("msg_text", "msg_text");
      
      /* msg_type - The type of message to be processed.  This indicates what data is contained
         in the XML - a transaction or mail message */
      attributeMgr.registerReferenceAttribute("msg_type", "msg_type", "QUEUE_MESSAGE_TYPE");
      
      /* status - Lifecycle status of the message. */
      attributeMgr.registerReferenceAttribute("status", "status", "INCOMING_Q_STATUS");
      
      /* client_bank - Not currently used */
      attributeMgr.registerAttribute("client_bank", "client_bank");
      
      /* message_id - An ID for the message.  This ID is generated on OTL */
      attributeMgr.registerAttribute("message_id", "message_id");
      
      /* reply_to_message_id - Used for confirmation messages only.  Refers to the message that this message
         is confirming */
      attributeMgr.registerAttribute("reply_to_message_id", "reply_to_message_id");
      
      /* agent_id - The name of the agent that is currently dealing with this message.   This
         prevents multiple agents from accessing the same dat */
      attributeMgr.registerAttribute("agent_id", "agent_id");
      
      /* confirmation_error_text - Any errors resulting from attempting to confirm this message. */
      attributeMgr.registerAttribute("confirmation_error_text", "confirmation_error_text");
      
      /* unpackaging_error_text - Errors from unpackaging */
      attributeMgr.registerAttribute("unpackaging_error_text", "unpackaging_error_text");
      
      /* complete_instrument_id - The prefix, instrument number, and suffix concatenated together. */
      attributeMgr.registerAttribute("complete_instrument_id", "complete_instrument_id");
      
      /* transaction_type_code - Code indicating the type of transaction.   Examples of types are Issue,
         Amend, Assignment of Proceeds, etc. */
      attributeMgr.registerAttribute("transaction_type_code", "transaction_type_code");
      
      /* transaction_oid - The OID of the transaction or main object on which an action was performed
         that required placing a row in the incoming queue.  This is not an association
         so it can store OID of heterogeneous objects. */
      attributeMgr.registerAttribute("transaction_oid", "a_transaction_oid", "NumberAttribute");
      
      /* datetime_msgbroker - Date and Time when the message is recirved on MQ message broker. */
      attributeMgr.registerAttribute("datetime_msgbroker", "datetime_msgbroker", "DateTimeAttribute");
      
      /* datetime_unpack_start - Date and Time when the unpackaging starts for this message. */
      attributeMgr.registerAttribute("datetime_unpack_start", "datetime_unpack_start", "DateTimeAttribute");
      
      /* datetime_unpack_end - Date and Time when the unpackaging ends for this message. */
      attributeMgr.registerAttribute("datetime_unpack_end", "datetime_unpack_end", "DateTimeAttribute");
      
      /* instrument_oid - The instrument on which an action was performed that required placing a
         row in the incoming queue. */
      attributeMgr.registerAssociation("instrument_oid", "a_instrument_oid", "Instrument");
      
      /* mail_message_oid - The mail message on which an action was performed that required placing
         a row in the incoming queue */
      attributeMgr.registerAssociation("mail_message_oid", "a_mail_message_oid", "MailMessage");
      
   }
   
 
   
 
 
   
}
