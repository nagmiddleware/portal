
package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;


/**
 * The contractual terms that comprise a transaction.  Data entered by the
 * user on the transaction forms pages will be stored in the Terms business
 * object.
 *
 * Attributes for this business object are dynamically registered.  That is,
 * all of the attributes defined here are not registered for all instances.
 * Terms objects have a terms manager object associated with them.  The terms
 * manager object determines which attributes should be registered and handles
 * any instance-specific method calls.
 *
 * For more information, see the "Terms Manager Delegation" section of the
 * Trade Portal Infrastructure Document.
 *
 *     Copyright  � 2003
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public interface Terms extends TradePortalBusinessObject
{
	public TermsParty populateTermsParty(com.amsinc.ecsg.util.DocumentHandler doc, java.lang.String componentIdentifer) throws RemoteException;

	public void copyTermsPartyInfo(com.ams.tradeportal.busobj.Terms source) throws RemoteException, AmsException;

	public void copy(com.ams.tradeportal.busobj.Terms source) throws RemoteException, AmsException;

	public DocumentHandler getTermsAttributesDoc(com.amsinc.ecsg.util.DocumentHandler termsDoc) throws RemoteException, AmsException;

	public boolean setTermsAttributesFromDoc(com.amsinc.ecsg.util.DocumentHandler inputDoc) throws RemoteException, AmsException;

	public void setPODates(java.util.Date lastShipmentDate) throws RemoteException, AmsException;

	public boolean unpackageWorkItem(com.amsinc.ecsg.util.DocumentHandler inputDoc) throws RemoteException, AmsException;

	public void deleteShipmentTerms(java.lang.String instrumentOid, java.lang.String transactionOid, java.lang.String shipmentOid,java.lang.String userOid) throws RemoteException, AmsException;

	public com.ams.tradeportal.busobj.ShipmentTerms createFirstShipment() throws RemoteException, AmsException;
	
	//Leelavathi -IR#T36000015642 10thMay2013 CR-737 Rel-8.2- Added createPmtTermsTenorDtl which is used while copying object 
	public com.ams.tradeportal.busobj.PmtTermsTenorDtl createPmtTermsTenorDtl() throws RemoteException, AmsException;
	//Leelavathi -IR#T36000018142 13/06/2013 CR-737 Rel-8.2- Added createAdditionalReqDocs which is used while copying object
	public com.ams.tradeportal.busobj.AdditionalReqDoc createAdditionalReqDoc() throws RemoteException, AmsException;
	
	public com.ams.tradeportal.busobj.ShipmentTerms getFirstShipment() throws RemoteException, AmsException;

	public void updateExpiryDateFromPO(com.ams.tradeportal.busobj.Instrument instrument, java.util.Date lastShipmentDate) throws RemoteException, AmsException;

    // TLE - 11/09/06 - IR#ANUG110757179 - Add Begin
    public void setImportIndicator(java.lang.String value) throws RemoteException, AmsException;

    public java.lang.String getImportIndicator() throws RemoteException, AmsException;
    // TLE - 11/09/06 - IR#ANUG110757179 - Add End
    
    //cquinton 4/10/2011 Rel 7.0.0 irbkul040547648 add utility methods for getting terms parties
    public TermsParty getTermsPartyByPartyType(String partyType) throws AmsException, RemoteException;
    public String getNextEmptyTermsParty() throws AmsException, RemoteException;
    //cquinton 7/6/2011 Rel 7.0 - add version of getNextEmptyTermsParty()
    // to allow getting the next empty terms party starting at
    // an initial specified component
    public String getNextEmptyTermsParty(String componentId) throws AmsException, RemoteException;
    public boolean doesTermsPartyComponentExist(String componentId) throws AmsException, RemoteException;
    //cquinton 4/10/2011 Rel 7.0.0 irbkul040547648 end
    
    public void updateAssignedInvoiceStatus(String instrumentOid, String transactionOid,Map map) throws AmsException, RemoteException;//SHR CR708 Rel8.1.1
}
