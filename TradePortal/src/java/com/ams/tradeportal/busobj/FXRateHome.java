package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;
/**
 * A foreign exchange rate between one currency and the corporate customer's
 * base currency.  This is populated by the corporate customer users.
 * 
 * FX Rates are used for threshold checking and for grouping by amount during
 * Auto LC Creation.

 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface FXRateHome extends EJBHome
{
   public FXRate create()
      throws RemoteException, CreateException, AmsException;

   public FXRate create(ClientServerDataBridge csdb)
      throws RemoteException, CreateException, AmsException;
}
