package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;


/**
 * A business (corporate or bank) that will be a party to an instrument in
 * the portal.    This business object is part of reference data.   The TermsParty
 * business object contains parties that are actually a part of instruments.
 * 
 * When a party is added to an instrument, the data for the party is copied
 * out of the Party business object and into the TermsParty business object.
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface Party extends ReferenceData
{   
}
