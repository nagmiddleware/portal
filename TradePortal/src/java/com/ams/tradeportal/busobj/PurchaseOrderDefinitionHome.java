
  

package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;
/**
 * Describes the mapping of an new structured uploaded file to purchase order
 * line items stored in the database.   The fields contained in the file are
 * described, the file format is specified, and the goods description format
 * is provided.


 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface PurchaseOrderDefinitionHome extends EJBHome
{
   public PurchaseOrderDefinition create()
      throws RemoteException, CreateException, AmsException;

   public PurchaseOrderDefinition create(ClientServerDataBridge csdb)
      throws RemoteException, CreateException, AmsException;
}
