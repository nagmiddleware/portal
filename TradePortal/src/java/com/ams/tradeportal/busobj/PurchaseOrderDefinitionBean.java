package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;

import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.StringFunction;

/*
 *
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */


public class PurchaseOrderDefinitionBean extends PurchaseOrderDefinitionBean_Base {
private static final Logger LOG = LoggerFactory.getLogger(PurchaseOrderDefinitionBean.class);
	
	
	   public void userValidate() throws java.rmi.RemoteException, com.amsinc.ecsg.frame.AmsException {
			
  			 
		      cleanup();
		      //Part to validate is a local attribute, therefore the data is NOT stored in the db.
		      if (this.getAttribute("part_to_validate").equals(TradePortalConstants.INV_GENERAL) && this.hasChangesPending()) {
		              // Validating the General section
		          validateGeneralTab ();			  
				  performAutoBinding(); //Srinivasu_D Rel8.0 IR# RBUM032655407 04/26/2012
					

		      }else if(this.getAttribute("part_to_validate").equals(TradePortalConstants.INV_FILE)) {
		             //Validating the File Definition section
		          validateFileDefinitionTab ();
		      }
	   
      }
	  
	 public void validateGeneralTab () throws java.rmi.RemoteException, com.amsinc.ecsg.frame.AmsException {

		 LOG.debug("========= Validating the General Tab data in the PurchaseOrderDefinitionBean Object ==========");
	     String name = getAttribute("name");
	     String ownerOrg = this.getAttribute("owner_org_oid");
		 String incoterm = this.getAttribute("incoterm_req");
		 String incotermLoc = this.getAttribute("incoterm_loc_req");
		 boolean throwError=false;
		 StringBuffer defaultFlagSql = new StringBuffer();
		 DocumentHandler tempDoc;
		//Incoterm validation
		if(incoterm != null ){
			if(TradePortalConstants.INDICATOR_YES.equals(incoterm)){
				if(incotermLoc == null){
					throwError=true;
				}else if(TradePortalConstants.INDICATOR_NO.equals(incotermLoc)){
					throwError=true;
				}
			}
		}
		if(incotermLoc != null){
			if(TradePortalConstants.INDICATOR_YES.equals(incotermLoc)){
				if(incoterm == null){
					throwError=true;
				}else if(TradePortalConstants.INDICATOR_NO.equals(incoterm)){
					throwError=true;
				}
			}
		}

		if(throwError){
				this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.INCOTERM_SELECTION);

		}
	     //Make sure there is no other name in the db that matches this one.

	     if (!isUnique( "name", name, " and A_OWNER_ORG_OID=" + ownerOrg ))
	      { 
		    this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
			                              TradePortalConstants.ALREADY_EXISTS, name, attributeMgr.getAlias("name"));
		  }
	    //Srinivasu_D Rel8.0 IR# BRUM032665368 03/29/2012 Start
	      //Make sure there is only one po definition in the db that is marked as the default.
		 //Ignore the check if the default_flag is set to 'N'.
		 String defaultFlag = this.getAttribute("default_flag");
		 	
		 if ( TradePortalConstants.INDICATOR_YES.equals(defaultFlag) ) {

		    if (!isUnique("default_flag", defaultFlag, " AND A_OWNER_ORG_OID=" + ownerOrg ))
		    {
		    	//LSuresh R91 IR T36000026319 - SQLINJECTION FIX
		    	defaultFlagSql.append("select name from purchase_order_definition ");
		        defaultFlagSql.append("where a_owner_org_oid = ? ");
		        
		        defaultFlagSql.append(" AND default_flag = ? ");
		        

		        tempDoc = DatabaseQueryBean.getXmlResultSet( defaultFlagSql.toString(),false,this.getAttribute("owner_org_oid"),TradePortalConstants.INDICATOR_YES );
		        LOG.debug( "*************** TempDoc == {}" , tempDoc.toString() );

		        this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
			                              TradePortalConstants.PO_DEF_DEFAULT_DEFINITION,
			                              tempDoc.getAttribute("/ResultSetRecord(0)/NAME"));
		    }
		 }
	     //Srinivasu_D Rel8.0 IR# BRUM032665368 03/29/2012 End
	     //if field is marked as required for Seller or Buyer, then it's label should be present.
	     
	     for(int x = 1; x<=TradePortalConstants.PO_BUYER_OR_SELLER_USER_DEF_FIELD; x++ ){
	    	 if((TradePortalConstants.INDICATOR_YES).equals(this.getAttribute("buyer_users_def" + x + "_req")) && 
	    			 StringFunction.isBlank( this.getAttribute("buyer_users_def" + x + "_label") )   ){	    		 
	    		 
	    		 this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                         TradePortalConstants.LABEL_REQUIRED, "Buyer User Def Label " + x);
	    	 }
	    	
	    	 if((TradePortalConstants.INDICATOR_YES).equals(this.getAttribute("seller_users_def" + x + "_req")) && 
	    			 StringFunction.isBlank( this.getAttribute("seller_users_def" + x + "_label") )   ){	    		 
	    		 this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                         TradePortalConstants.LABEL_REQUIRED, "Seller User Def Label " + x);
	    	 }
	    	 
	     }
	     
       // if field is marked as required for Line item type/value, then it's label should be present.
	     
         for(int x = 1; x<=TradePortalConstants.PO_LINE_ITEM_TYPE_OR_VALUE_FIELD; x++ ){
	    	 if((TradePortalConstants.INDICATOR_YES).equals(this.getAttribute("prod_chars_ud" + x + "_req")) && 
	    			 StringFunction.isBlank( this.getAttribute("prod_chars_ud" + x + "_label") )   ){	    		 
	    		 
	    		 this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                         TradePortalConstants.LABEL_REQUIRED, "Product Chars UD " + x + " Type/Value Label");
	    	 }
	    		    	 
	     }  
    
     // If 'Line Item Details Not Required Indicator' indicator is selected, then none of the PO Line Item Detail field names can be selected.  
     //If both are selected, upon save the system will present the user with an error         
         if ((TradePortalConstants.INDICATOR_YES).equals(this.getAttribute("line_item_detail_provided"))){
        	 int warning = 0;
			 	 if((TradePortalConstants.INDICATOR_YES).equals(this.getAttribute("line_item_num_req"))&& (warning == 0)){
        		 this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,TradePortalConstants.PO_LINE_ITEM_REQUIRED);
        		 warning++;
        	 }
        	 if((TradePortalConstants.INDICATOR_YES).equals(this.getAttribute("unit_price_req"))&& (warning == 0)){
        		 this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,TradePortalConstants.PO_LINE_ITEM_REQUIRED);
        		 warning++;
        	 }
        	 if((TradePortalConstants.INDICATOR_YES).equals(this.getAttribute("unit_of_measure_req"))&& (warning == 0)){
        		 this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,TradePortalConstants.PO_LINE_ITEM_REQUIRED);
        		 warning++;
        	 }
        	 if((TradePortalConstants.INDICATOR_YES).equals(this.getAttribute("quantity_req"))&& (warning == 0)){
        		 this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,TradePortalConstants.PO_LINE_ITEM_REQUIRED);
        		 warning++;
        	 }
        	 if((TradePortalConstants.INDICATOR_YES).equals(this.getAttribute("quantity_var_plus_req"))&& (warning == 0)){
        		 this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,TradePortalConstants.PO_LINE_ITEM_REQUIRED);
        		 warning++;
        	 }

             for(int x = 1; x <= TradePortalConstants.PO_LINE_ITEM_TYPE_OR_VALUE_FIELD ; x++ ){
            	 if((TradePortalConstants.INDICATOR_YES).equals(this.getAttribute("prod_chars_ud" + x + "_req"))&& (warning == 0)){
            		 this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,TradePortalConstants.PO_LINE_ITEM_REQUIRED);
            		 warning++;
            	 }           	
              }
       	 
         }  
         
	 } 
		//Srinivasu_D Rel8.0 IR# RBUM032655407 04/26/2012  Start
	public void performAutoBinding () throws AmsException, RemoteException {
		
		String summaryAttribute = null;
		String attribute = null;
		String dbDataArray [] = new String [TradePortalConstants.PO_SUMMARY_NUMBER_OF_FIELDS]; 
		int tempCounter = 0;
		for( int x = 1; x <= (TradePortalConstants.PO_SUMMARY_NUMBER_OF_FIELDS - 1); x++ ) {
			summaryAttribute = this.getAttribute("po_data_field" + x); 
			if(StringFunction.isNotBlank(summaryAttribute)){				 
				dbDataArray[x-1]=summaryAttribute;
				tempCounter++;
			}
		}
		
		if(tempCounter <= 0){
			 this.setAttribute("po_data_field1","purchase_order_num");
			 this.setAttribute("po_data_field2","purchase_order_type");
			 this.setAttribute("po_data_field3","issue_date");
			 this.setAttribute("po_data_field4","currency");
			 this.setAttribute("po_data_field5","amount");
			 this.setAttribute("po_data_field6","seller_name");
		}

		ArrayList dbSummaryList = new ArrayList(TradePortalConstants.PO_GOODS_NUMBER_OF_FIELDS);
		ArrayList uiSummaryList = new ArrayList(TradePortalConstants.PO_GOODS_NUMBER_OF_FIELDS);
		

		for( int x = 1; x <= TradePortalConstants.PO_GOODS_NUMBER_OF_FIELDS; x++ ) { 
			summaryAttribute = this.getAttribute("po_summary_field" + x );
			if(StringFunction.isNotBlank(summaryAttribute)){				 
				dbSummaryList.add(summaryAttribute);
			}
		}

		attribute = this.getAttribute("incoterm_req");  
		if( TradePortalConstants.INDICATOR_YES.equals(attribute)) 
			uiSummaryList.add("incoterm");	

		attribute = this.getAttribute("incoterm_loc_req");  
		if( TradePortalConstants.INDICATOR_YES.equals(attribute)) 
			uiSummaryList.add("incoterm_location");	

		attribute = this.getAttribute("partial_ship_allowed_req");  
		if( TradePortalConstants.INDICATOR_YES.equals(attribute)) 
			uiSummaryList.add("partial_shipment_allowed");

		attribute = this.getAttribute("goods_desc_req");  
		if( TradePortalConstants.INDICATOR_YES.equals(attribute)) 
			uiSummaryList.add("goods_description");

		attribute = this.getAttribute("latest_shipment_date_req");  
		if( TradePortalConstants.INDICATOR_YES.equals(attribute)) 
			uiSummaryList.add("latest_shipment_date");

		// checking for all 10 user defined Buyers and sellers in PO definition whether it is being used in file upload order.
		for(int x = 1; x <= TradePortalConstants.PO_BUYER_OR_SELLER_USER_DEF_FIELD ; x++ ){

			attribute = this.getAttribute("buyer_users_def" + x + "_req");
			if( TradePortalConstants.INDICATOR_YES.equals(attribute)) 
				uiSummaryList.add("buyer_user_def" + x + "_label");	

			attribute = this.getAttribute("seller_users_def" + x + "_req");
			if( TradePortalConstants.INDICATOR_YES.equals(attribute)) 
				uiSummaryList.add("seller_user_def" + x + "_label");		
		} 

		if(dbSummaryList.size()>0){

			String tempAttr = null;
			Iterator uiIte =  uiSummaryList.iterator();
			while(uiIte.hasNext()){
				tempAttr = (String) uiIte.next();				
				if(!dbSummaryList.contains(tempAttr)){
					dbSummaryList.add(tempAttr);
				}
			}

			ArrayList tempDBSummaryList = new ArrayList(dbSummaryList);
			Iterator dbIte = dbSummaryList.iterator();
			while(dbIte.hasNext()){
				tempAttr = (String) dbIte.next();					
				if(!uiSummaryList.contains(tempAttr)){
					tempDBSummaryList.remove(tempAttr);
				}
			}	
			//Now loop through the data in the attribute list - hashtable and re-put them into the bus Obj.
			for( int x = 0; x < tempDBSummaryList.size(); x++ ) {            
				this.setAttribute( "po_summary_field" + (x+1), (String)tempDBSummaryList.get( x ) );
			}
		} else {
			if(uiSummaryList.size()>0){
				String tempSummaryAttr = null;
				for( int x = 0; x < uiSummaryList.size(); x++ ) {
					tempSummaryAttr = (String)uiSummaryList.get( x );				
					if( StringFunction.isNotBlank( tempSummaryAttr ) ) {				
						this.setAttribute( "po_summary_field" + (x+1), tempSummaryAttr );
					} 
				}
			}	
		}	
		//PO LIne Item assigning 		 

		ArrayList dbPOList = new ArrayList(TradePortalConstants.PO_LINE_ITEM_NUMBER_OF_FIELDS);
		ArrayList uiPOList = new ArrayList(TradePortalConstants.PO_LINE_ITEM_NUMBER_OF_FIELDS);

		for( int x = 1; x <= TradePortalConstants.PO_LINE_ITEM_NUMBER_OF_FIELDS; x++ ) {               
			if(StringFunction.isNotBlank(this.getAttribute("po_line_item_field" + x ))){				   
				dbPOList.add(this.getAttribute("po_line_item_field" + x ));
			}
		}

		attribute = this.getAttribute("line_item_num_req");  
		if( TradePortalConstants.INDICATOR_YES.equals(attribute)) 
			uiPOList.add("line_item_num");		

		attribute = this.getAttribute("unit_price_req");  
		if( TradePortalConstants.INDICATOR_YES.equals(attribute)) 
			uiPOList.add("unit_price");	        

		attribute = this.getAttribute("unit_of_measure_req");     
		if( TradePortalConstants.INDICATOR_YES.equals(attribute)) 
			uiPOList.add("unit_of_measure");        

		attribute = this.getAttribute("quantity_req");  
		if( TradePortalConstants.INDICATOR_YES.equals(attribute)) 
			uiPOList.add("quantity");	        

		attribute = this.getAttribute("quantity_var_plus_req");  
		if( TradePortalConstants.INDICATOR_YES.equals(attribute)) 
			uiPOList.add("quantity_variance_plus");       

		// checking for all 7 line item item type/value  whether it is being used in file upload order.
		for(int x = 1; x <= TradePortalConstants.PO_LINE_ITEM_TYPE_OR_VALUE_FIELD ; x++ ){                           
			attribute = this.getAttribute("prod_chars_ud" + x + "_req");
			if( TradePortalConstants.INDICATOR_YES.equals(attribute)) 
				uiPOList.add("prod_chars_ud" + x + "_label");	

		}  
		if(dbPOList.size()>0){

			String tempVariable = null;
			Iterator uiIte =  uiPOList.iterator();
			while(uiIte.hasNext()){
				tempVariable = (String) uiIte.next();				
				if(!dbPOList.contains(tempVariable)){
					dbPOList.add(tempVariable);
				}
			}

			ArrayList tempDBList = new ArrayList(dbPOList);
			Iterator dbIte = dbPOList.iterator();
			while(dbIte.hasNext()){
				tempVariable = (String) dbIte.next();					
				if(!uiPOList.contains(tempVariable)){
					tempDBList.remove(tempVariable);
				}
			}
			

			//Now loop through the data in the attribute list - hashtable and re-put them into the bus Obj.
			for( int x = 0; x < tempDBList.size(); x++ ) {  
				this.setAttribute( "po_line_item_field" + (x+1), (String)tempDBList.get( x ) );
			}
		} else {
			if(uiPOList.size()>0){
				String tempVar = null;
				for( int x = 0; x < uiPOList.size(); x++ ) {
					tempVar = (String)uiPOList.get( x );				
					if( StringFunction.isNotBlank( tempVar ) ) {				 
						this.setAttribute( "po_line_item_field" + (x+1), tempVar );
					} 
				}
			}	
		}

	}
	 //Srinivasu_D Rel8.0 IR# RBUM032655407 04/26/2012  End
	 public void cleanup () throws  AmsException, RemoteException {   

 
		 HashSet hGoodsSet = new HashSet();
		 for( int x = 1; x <= TradePortalConstants.PO_GOODS_NUMBER_OF_FIELDS; x++ ) {
			 String uploadOrder = getAttribute("po_summary_field" + x);
			 if( StringFunction.isNotBlank( uploadOrder ) )               
				 hGoodsSet.add( uploadOrder );

		 }	

 
		 HashSet hLineItemSet = new HashSet();
		 for( int x = 1; x <= TradePortalConstants.PO_LINE_ITEM_NUMBER_OF_FIELDS; x++ ) {
			 String uploadOrder = this.getAttribute("po_line_item_field" + x);
			 if( StringFunction.isNotBlank( uploadOrder ) ) {
				 hLineItemSet.add( uploadOrder );
			 } 
		 }

		 removeNonRequiredFields ( hGoodsSet, "latest_shipment_date_req","latest_shipment_date"); 
		 removeNonRequiredFields ( hGoodsSet, "incoterm_req","incoterm");
		 removeNonRequiredFields ( hGoodsSet, "incoterm_loc_req","incoterm_location");
		 removeNonRequiredFields ( hGoodsSet, "partial_ship_allowed_req","partial_shipment_allowed");
		 removeNonRequiredFields ( hGoodsSet, "goods_desc_req","goods_description");
		 for (int x = 1; x <= TradePortalConstants.PO_BUYER_OR_SELLER_USER_DEF_FIELD; x++) {
			 removeNonRequiredFields ( hGoodsSet, "buyer_users_def" + x + "_req", "buyer_user_def" + x + "_label");				
			 removeNonRequiredFields ( hGoodsSet, "seller_users_def" + x + "_req","seller_user_def" + x + "_label");				
		 }


		 removeNonRequiredFields ( hLineItemSet, "line_item_num_req","line_item_num");
		 removeNonRequiredFields ( hLineItemSet, "unit_price_req","unit_price");
		 removeNonRequiredFields ( hLineItemSet, "unit_of_measure_req","unit_of_measure");
		 removeNonRequiredFields ( hLineItemSet, "quantity_req","quantity");
		 removeNonRequiredFields ( hLineItemSet, "quantity_var_plus_req","quantity_variance_plus");
		 for (int x = 1; x <= TradePortalConstants.PO_LINE_ITEM_TYPE_OR_VALUE_FIELD; x++) {
			 removeNonRequiredFields ( hLineItemSet, "prod_chars_ud" + x + "_req","prod_chars_ud" + x + "_label");										
		 }


		 // The rest of this method will look at the user selected list of file definition (order)
		 // and if the user left gaps in the selected list: ie made a selection for #2 and #4 but not for #3,
		 // then we need to move the #4 selection into Order #3.

		 // checking for PO Summary file order
		 Hashtable  attributesHash = new Hashtable( TradePortalConstants.PO_GOODS_NUMBER_OF_FIELDS );
		 String     attUploadOrder = null;
		 int        key = 1;
		 for( int x = 1; x <= TradePortalConstants.PO_GOODS_NUMBER_OF_FIELDS; x++ ) { 
			 attUploadOrder = this.getAttribute("po_summary_field" + x );
			 if( StringFunction.isNotBlank( attUploadOrder ) && hGoodsSet.contains(attUploadOrder) ) {
				 attributesHash.put( new Integer(key++), attUploadOrder );
			 }
		 }

		 //Now loop through the data in the attribute list - hashtable and re-put them into the bus Obj.
		 for( int x = 1; x <= TradePortalConstants.PO_GOODS_NUMBER_OF_FIELDS; x++ ) {        	
			 this.setAttribute( "po_summary_field" + x, (String)attributesHash.get( new Integer(x) ) );
		 }


		 //checking for Line Item Detail file order         
		 attributesHash = new Hashtable( TradePortalConstants.PO_LINE_ITEM_NUMBER_OF_FIELDS );   
		 key = 1;
		 for( int x = 1; x <= TradePortalConstants.PO_LINE_ITEM_NUMBER_OF_FIELDS; x++ ) {
			 attUploadOrder = this.getAttribute("po_line_item_field" + x );
			 if( StringFunction.isNotBlank( attUploadOrder ) && hLineItemSet.contains(attUploadOrder)) {
				 attributesHash.put( new Integer(key++), attUploadOrder );
			 }
		 }

		 //Now loop through the data in the attribute list - hashtable and re-put them into the bus Obj.
		 for( int x = 1; x <= TradePortalConstants.PO_LINE_ITEM_NUMBER_OF_FIELDS; x++ ) {
			 this.setAttribute( "po_line_item_field" + x, (String)attributesHash.get( new Integer(x) ) );
		 }
	 }  
	 

    public void validateFileDefinitionTab ()
       throws java.rmi.RemoteException, com.amsinc.ecsg.frame.AmsException {   
    	
    	 LOG.debug("========= Validating the File Definition Tab data in the Purchase OrderDefinition Bean Object ==========");
         
    	 //If the user tries to select the same option more than once in Summary Data- issue an error. 
    	 HashSet hSummarySet = new HashSet();
         Hashtable orderList = new Hashtable( TradePortalConstants.PO_SUMMARY_NUMBER_OF_FIELDS );        
	     for( int x = 1; x <= TradePortalConstants.PO_SUMMARY_NUMBER_OF_FIELDS; x++ ) {

          
		   String uploadOrder = this.getAttribute("po_data_field" + x);
			
           if( StringFunction.isNotBlank( uploadOrder ) ) {               
               if ( !hSummarySet.add( uploadOrder ) ) {

                   String uploadOrderDescription = uploadOrder;

                   this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                                                     TradePortalConstants.PO_SUMMARY_FIELD_ALREADY_SELECTED,
                                                     uploadOrderDescription,
                                                     (String)orderList.get( uploadOrder ) );
               }else{
                   orderList.put( uploadOrder, String.valueOf(x) );
               }
            }
          }
         //If the user tries to select the same option more than once in PO Goods Data- issue an error. 
    	 HashSet hGoodsSet = new HashSet();
         Hashtable orderGoodsList = new Hashtable( TradePortalConstants.PO_GOODS_NUMBER_OF_FIELDS );    
		 //Srinivasu_D Rel8.0 IR# RBUM032655407 03/29/2012  Start
	    // for( int x = 1; x <= TradePortalConstants.PO_GOODS_NUMBER_OF_FIELDS; x++ ) {
		 String sumCount =	getAttribute("summary_count"); 
		 int summaryCount =0;
		 if(StringFunction.isNotBlank(sumCount)){
			summaryCount = Integer.parseInt(sumCount);
		 }
		 //Srinivasu_D Rel8.0 IR# RBUM032655407 03/29/2012  End
         for( int x = 1; x <= summaryCount; x++ ) {
		   String uploadOrder = getAttribute("po_summary_field" + x);
			
           if( StringFunction.isNotBlank( uploadOrder ) ) {               
               if ( !hGoodsSet.add( uploadOrder ) ) {

                   String uploadOrderDescription = uploadOrder;

                   this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                                                     TradePortalConstants.PO_GOODS_FIELD_ALREADY_SELECTED,
                                                     uploadOrderDescription,
                                                     (String)orderGoodsList.get( uploadOrder ) );
               }else{
            	   orderGoodsList.put( uploadOrder, String.valueOf(x) );
               }
            }
          }	
	      //Srinivasu_D Rel8.0 IR# RBUM032655407 03/29/2012  Start
	     String poItemCount =	getAttribute("po_line_item_count"); 
		 int poLineItemCount =0;
		 if(StringFunction.isNotBlank(poItemCount)){
			poLineItemCount = Integer.parseInt(poItemCount);
		 }
		 //Srinivasu_D Rel8.0 IR# RBUM032655407 03/29/2012  End

       // If the user tries to select the same option more than once in Line Item Detail- issue an error. 
	     HashSet hLineItemSet = new HashSet();
         Hashtable orderItemList = new Hashtable( TradePortalConstants.PO_LINE_ITEM_NUMBER_OF_FIELDS );
	     for( int x = 1; x <= TradePortalConstants.PO_LINE_ITEM_NUMBER_OF_FIELDS; x++ ) {
			//for( int x = 1; x <= poLineItemCount; x++ ) {
	        String uploadOrder = this.getAttribute("po_line_item_field" + x);
	           if( StringFunction.isNotBlank( uploadOrder ) ) {
				
	               // If the user tries to select the same option more than once - issue an error.
	               if ( !hLineItemSet.add( uploadOrder ) ) {

	                   String uploadOrderDescription = uploadOrder;//this.getAttribute(uploadOrder + "_field_name");

	                   this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
	                                                     TradePortalConstants.PO_LINE_ITEM_FIELD_ALREADY_SELECTED,
	                                                     uploadOrderDescription,
	                                                     (String)orderItemList.get( uploadOrder ) );
	               }else{
	            	   orderItemList.put( uploadOrder, String.valueOf(x) );
	               }

	           }
	        }   
	   
	     //Validate that all the fields defined in the General tab is selected in the File Definition
         //tab.  If A defined field is NOT used on the Inv Details tab - then throw a warning to the user.
        
	     //PO Summary data required check
	     int warningCount= 0;		
		 warningCount = lookupFieldNameInOrderList ( hSummarySet, "purchase_order_num", warningCount);
         warningCount = lookupFieldNameInOrderList ( hSummarySet, "purchase_order_type", warningCount);
		 warningCount = lookupFieldNameInOrderList ( hSummarySet, "issue_date", warningCount);		 
         warningCount = lookupFieldNameInOrderList ( hSummarySet, "currency", warningCount);
         warningCount = lookupFieldNameInOrderList ( hSummarySet, "amount", warningCount);
         warningCount = lookupFieldNameInOrderList ( hSummarySet, "seller_name", warningCount);  

         if((TradePortalConstants.INDICATOR_YES).equals(this.getAttribute("latest_shipment_date_req")))
         warningCount = lookupFieldNameInOrderList ( hGoodsSet, "latest_shipment_date", warningCount); 


		 if((TradePortalConstants.INDICATOR_YES).equals(this.getAttribute("incoterm_req")))
         warningCount = lookupFieldNameInOrderList ( hGoodsSet, "incoterm", warningCount);
          		
 		if((TradePortalConstants.INDICATOR_YES).equals(this.getAttribute("incoterm_loc_req")))
 			warningCount = lookupFieldNameInOrderList ( hGoodsSet, "incoterm_location", warningCount);

 		if((TradePortalConstants.INDICATOR_YES).equals(this.getAttribute("partial_ship_allowed_req")))
 			warningCount = lookupFieldNameInOrderList ( hGoodsSet, "partial_shipment_allowed", warningCount);

 		if((TradePortalConstants.INDICATOR_YES).equals(this.getAttribute("goods_desc_req")))
 			warningCount = lookupFieldNameInOrderList ( hGoodsSet, "goods_description", warningCount);

		//Commented for check/uncheck functionality
		for (int x = 1; x <= TradePortalConstants.PO_BUYER_OR_SELLER_USER_DEF_FIELD; x++) {
			if ((TradePortalConstants.INDICATOR_YES).equals(getAttribute("buyer_users_def" + x + "_req"))) {
				warningCount = lookupFieldNameInOrderList ( hGoodsSet, "buyer_user_def" + x + "_label", warningCount);				
			   }
			if ((TradePortalConstants.INDICATOR_YES).equals(getAttribute("seller_users_def" + x + "_req"))) {
				warningCount = lookupFieldNameInOrderList ( hGoodsSet, "seller_user_def" + x + "_label", warningCount);				
			  }
		 }
                  				
 		
		if((TradePortalConstants.INDICATOR_YES).equals(this.getAttribute("line_item_num_req")))
       	 warningCount = lookupFieldNameInOrderList ( hLineItemSet, "line_item_num", warningCount);
		
		if((TradePortalConstants.INDICATOR_YES).equals(this.getAttribute("unit_price_req")))
			warningCount = lookupFieldNameInOrderList ( hLineItemSet, "unit_price", warningCount);

		if((TradePortalConstants.INDICATOR_YES).equals(this.getAttribute("unit_of_measure_req")))
			warningCount = lookupFieldNameInOrderList ( hLineItemSet, "unit_of_measure", warningCount);

		if((TradePortalConstants.INDICATOR_YES).equals(this.getAttribute("quantity_req")))
			warningCount = lookupFieldNameInOrderList ( hLineItemSet, "quantity", warningCount);

		if((TradePortalConstants.INDICATOR_YES).equals(this.getAttribute("quantity_var_plus_req")))
			warningCount = lookupFieldNameInOrderList ( hLineItemSet, "quantity_variance_plus", warningCount);

		for (int x = 1; x <= TradePortalConstants.PO_LINE_ITEM_TYPE_OR_VALUE_FIELD; x++) {
			if ((TradePortalConstants.INDICATOR_YES).equals(getAttribute("prod_chars_ud" + x + "_req"))) {
				warningCount = lookupFieldNameInOrderList ( hLineItemSet, "prod_chars_ud" + x + "_label", warningCount);										
			   }
		}
    
}  

    
    /**
     * The business rule in question requires that before a value be deleted, that we look up
     * the data on the two other tabs and make sure that this value is not referenced there.  If this value is referenced
     * on either of the other tabs, then issue an error.  This method uses BusinessObjects.getAttributes
     * method to build a hashtable of value that the field might exist in. if the Hashtable does 'contain'
     * the field being removed by the user then we have a problem. - issue an Error!
     * @param Hashtable fileDefHash    - List of values on the File Def tab for Summary or Line item detail.
     * @param String FieldName           - Field being removed by the user.
     * @param String Physical Field Name - Value stored on the tab that needs to be looked up.
     * @exception java.rmi.RemoteException
     * @exception com.amsinc.ecsg.frame.AmsException
     */   
    
 public void lookupValueOnOtherTabs(Hashtable fileDefHash, String fieldName, String physicalFieldName)
    throws java.rmi.RemoteException, com.amsinc.ecsg.frame.AmsException {

  if( TradePortalConstants.INDICATOR_NO.equals(fieldName) ) {      
      if( fileDefHash.contains( physicalFieldName ) ) {
	            this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
		                               TradePortalConstants.PO_USE_FOR_INV_FILE_DESCRIPTION,
		                               physicalFieldName, resMgr.getText("POStructureDefinition.FileFormat", TradePortalConstants.TEXT_BUNDLE));
      }
   }
}    

 
 
 
 /**
 * @param hset
 * @param fieldName
 * @param lookUpValue
 * @throws java.rmi.RemoteException
 * @throws com.amsinc.ecsg.frame.AmsException
 */
protected void removeNonRequiredFields (HashSet hset, String fieldName, String lookUpValue)
	        throws java.rmi.RemoteException, com.amsinc.ecsg.frame.AmsException {
			
	     if( !TradePortalConstants.INDICATOR_YES.equals(getAttribute(fieldName))){
	    	 hset.remove(lookUpValue);
	     }

	 }
 
 
 /**
  *  This method is designed to lookup a passed in value in the passed in HashSet by trying
  *  to add itto the current list.  If the added value is returned true, then the user has NOT
  *  added this value to the list yet.  In this case we issue a warning back to the user.
  *  This is all contingent that the value was populated on the PO Upload Definition object to
  *  begin with...
  *
  *  @param HashSet hset  - list of values already added based on selection in the File Definition tab.
  *  @param String  vlaue - this is the value to lookup in the list.
  *  @param int warningCount - the number of warnings issued so far
  *  @return int the new warningCount after running the method
  *  @exception java.rmi.RemoteException
  *  @exception com.amsinc.ecsg.frame.AmsException
  */
 public int lookupFieldNameInOrderList (HashSet hset, String lookUpValue, int warningCount)
        throws java.rmi.RemoteException, com.amsinc.ecsg.frame.AmsException {
		
     //  String attributeValue = this.getAttribute( lookUpValue );
			
       if ( StringFunction.isNotBlank( lookUpValue ) ) {

          if( hset.add( lookUpValue ) ) {

              if(warningCount == 0)
               {
                 this.getErrorManager().issueError( TradePortalConstants.ERR_CAT_1,
			                                       TradePortalConstants.MISSING_PO_SUMMARY_ORDER_FIELDS );
                 return warningCount + 1;
               }
          }
       }
      return warningCount;
 }
	 
 /**
  *This implements the pre delete checks that were required by the business rules in the use case for the Purchase Order
  *Defintition.  The 2 checks are:  <BR>
  * 1:  Check for an Association from the PO Line Item to the PO Upload Definition object prior to deletion.
  *	2:  Check for an Association from the PO Line Item to the LC Creation Rule object prior to deletion.
  *If either of these associations exist then we need to issue an error.
  *
  * @exception java.rmi.RemoteException
  * @exception com.amsinc.ecsg.frame.AmsException
  */
 public void userDelete() throws java.rmi.RemoteException, com.amsinc.ecsg.frame.AmsException {

      //Verify that a PO Line Item is not associated with this PO Upload Definition- if so
      //we can't let the user delete this definition.  Ie: issue an error.
	    if (isAssociated("PurchaseOrder", "byPurchaseOrderDefinition",
		    new String[] {getAttribute("owner_org_oid"), getAttribute("purchase_order_definition_oid")} )) {

			    this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
			                                      TradePortalConstants.PO_STRUCTURED_ITEM_UPLOADED_WITH_THIS_DEF);
		}

      //Verify that an LC Creation Rule is not associated with this PO Upload Definition- if so
      //we can't let the user delete this definition.  Ie: issue an error.
	    if (isAssociated("LCCreationRule", "byPurchaseOrderDefinition",
	        new String[] {getAttribute("owner_org_oid"), getAttribute("purchase_order_definition_oid")} )) {

			    this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
			                                      TradePortalConstants.CANT_DELETE_PO_UPLD_DEFN,
			                                      getAttribute("name") );
		}
 }


}
