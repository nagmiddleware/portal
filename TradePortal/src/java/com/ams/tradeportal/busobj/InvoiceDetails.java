

  

package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;


/**
 * Invoice Goods.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface InvoiceDetails extends TradePortalBusinessObject
{   
	//BSL IR#PUL032965444 04/04/11
	//public void updateInvoiceDetail (DocumentHandler inputDoc,DocumentHandler outputDoc) throws RemoteException, AmsException;
	public void updateInvoiceDetail (DocumentHandler inputDoc, DocumentHandler outputDoc, DomesticPayment dp) throws RemoteException, AmsException;
}
