
  

package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;
/**
 * Invoice.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface FavoriteTaskCustomizationHome extends EJBHome
{
   public FavoriteTaskCustomization create()
      throws RemoteException, CreateException, AmsException;

   public FavoriteTaskCustomization create(ClientServerDataBridge csdb)
      throws RemoteException, CreateException, AmsException;
}
