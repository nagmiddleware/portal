 
package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/*
 * The Payable Invoice Payment Instructions for Bank defined Trading Parter.
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class PayableInvPayInstBean extends PayableInvPayInstBean_Base
{
private static final Logger LOG = LoggerFactory.getLogger(PayableInvPayInstBean.class);            
  
}
