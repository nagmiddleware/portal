
  

package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;




/*
 * The rule that indicates which calendar is linked to each currency.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class CurrencyCalendarRuleBean_Base extends TradePortalBusinessObjectBean
{
private static final Logger LOG = LoggerFactory.getLogger(CurrencyCalendarRuleBean_Base.class);

  
  /* 
   * Register the attributes and associations of the business object
   */
   protected void registerAttributes() throws AmsException
   {  

      /* Register attributes defined in the Ancestor class */
      super.registerAttributes();
      
      /* currency_calendar_rule_oid - Unique Identifier */
      attributeMgr.registerAttribute("currency_calendar_rule_oid", "currency_calendar_rule_oid", "ObjectIDAttribute");
      
      /* opt_lock - Optimistic lock attribute
         See jPylon documentation for details on how this works */
      attributeMgr.registerAttribute("opt_lock", "opt_lock", "OptimisticLockAttribute");
      
      /* rule_name - Rule Name */
      attributeMgr.registerAttribute("rule_name", "rule_name");
      attributeMgr.requiredAttribute("rule_name");
      attributeMgr.registerAlias("rule_name", getResourceManager().getText("CurrencyCalendarRuleBeanAlias.rule_name", TradePortalConstants.TEXT_BUNDLE));
      
      /* currency - Currency */
      attributeMgr.registerReferenceAttribute("currency", "currency", "CURRENCY_CODE");
      attributeMgr.requiredAttribute("currency");
      attributeMgr.registerAlias("currency", getResourceManager().getText("CurrencyCalendarRuleBeanAlias.currency", TradePortalConstants.TEXT_BUNDLE));
      
      /* tp_calendar_oid -  */
      attributeMgr.registerAssociation("tp_calendar_oid", "a_tp_calendar_oid", "TPCalendar");
      attributeMgr.requiredAttribute("tp_calendar_oid");
      attributeMgr.registerAlias("tp_calendar_oid",getResourceManager().getText("CurrencyCalendarRuleBeanAlias.tp_calendar_oid",TradePortalConstants.TEXT_BUNDLE));
      
   }
   
 
   
 
 
   
}
