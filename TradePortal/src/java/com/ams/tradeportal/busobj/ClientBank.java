package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;


/**
 * A client bank that has chosen to use the Trade Portal for their customers'
 * trade processing.   
 * 
 * Each bank has a separate instrument number (ID) range.  When instruments
 * are created, this range is used to determine the valid range of instrument
 * numbers.
 * 
 * Information about instrument ID ranges, security, branding, and default
 * templates are stored in a ClientBank record.
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface ClientBank extends ReferenceDataOwner
{   
	public long useNextInstrumentNumber() throws RemoteException;

	public String getDefaultTemplateOid(java.lang.String instrumentType) throws RemoteException, AmsException;

	public void setDefaultTemplateOid(java.lang.String instrumentType, java.lang.String templateOid) throws RemoteException, AmsException;
	
	public void getDataFromReferenceCache (long OID) throws RemoteException, AmsException;

}
