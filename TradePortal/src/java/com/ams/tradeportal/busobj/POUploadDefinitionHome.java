
  

package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;
/**
 * Describes the mapping of an uploaded file to purchase order line items stored
 * in the database.   The fields contained in the file are described, the file
 * format is specified, and the goods description format is provided.
 * 
 * Can also be used to describe the process of manually entering purchase order
 * data.  The fields are used to define the fields that the user will enter,
 * the order in which they will appear and their format in goods description.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface POUploadDefinitionHome extends EJBHome
{
   public POUploadDefinition create()
      throws RemoteException, CreateException, AmsException;

   public POUploadDefinition create(ClientServerDataBridge csdb)
      throws RemoteException, CreateException, AmsException;
}
