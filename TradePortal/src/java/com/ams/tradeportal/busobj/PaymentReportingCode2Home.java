
  

package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;
/**
 * Payment Reporting Code 2
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface PaymentReportingCode2Home extends EJBHome
{
   public PaymentReportingCode2 create()
      throws RemoteException, CreateException, AmsException;

   public PaymentReportingCode2 create(ClientServerDataBridge csdb)
      throws RemoteException, CreateException, AmsException;
}
