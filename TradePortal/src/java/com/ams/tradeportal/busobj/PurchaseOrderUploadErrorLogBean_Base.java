
  
/*
 * This file is generated from the model.  Normally it should not be modified manually.  
 * Contact the modeler first.  (In a pinch, it can be modified first and then retrofitted 
 * to the model.)
 *
 */
package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;


/*
 * The Error Logs of uploading Purchase Order File for each purchase order.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class PurchaseOrderUploadErrorLogBean_Base extends BusinessObjectBean
{
private static final Logger LOG = LoggerFactory.getLogger(PurchaseOrderUploadErrorLogBean_Base.class);

  
  /* 
   * Register the attributes and associations of the business object
   */
   protected void registerAttributes() throws AmsException
   {  
      /* po_upload_error_log_oid - Unique identifier */
      attributeMgr.registerAttribute("po_upload_error_log_oid", "po_upload_error_log_oid", "ObjectIDAttribute");
      
      /* ben_name - Beneficiary Info (Name, customer ref, amount) */
      attributeMgr.registerAttribute("ben_name", "ben_name");
      
      /* error_msg - Error message. */
      attributeMgr.registerAttribute("error_msg", "error_msg");
      
      /* po_number - The line number in the upload file. */
      attributeMgr.registerAttribute("po_number", "po_number");
      
      /* Pointer to the parent PurchaseOrderFileUpload */
       attributeMgr.registerAttribute("po_file_upload_oid", "p_po_file_upload_oid", "ParentIDAttribute");
      
   }
   
 
   
 
 
   
}
