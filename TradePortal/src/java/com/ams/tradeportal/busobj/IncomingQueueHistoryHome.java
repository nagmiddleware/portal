package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;
/**
 * After processing, data stored as IncomingInterfaceQueue business objects
 * is moved to be stored as IncomingQueueHistory.  This is done to prevenet
 * the IncomingInterfaceQueue table from becoming too large.
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface IncomingQueueHistoryHome extends EJBHome
{
   public IncomingQueueHistory create()
      throws RemoteException, CreateException, AmsException;

   public IncomingQueueHistory create(ClientServerDataBridge csdb)
      throws RemoteException, CreateException, AmsException;
}
