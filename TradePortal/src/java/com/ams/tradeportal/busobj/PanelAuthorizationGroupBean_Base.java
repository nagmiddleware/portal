


package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;




/*
 * A PanelAuthorizationGroup consists of a set of PalenAuthorizationRanges
 * to specifiy the requirements of panel authorization.
 *
 *     Copyright  © 2003
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class PanelAuthorizationGroupBean_Base extends ReferenceDataBean
{
private static final Logger LOG = LoggerFactory.getLogger(PanelAuthorizationGroupBean_Base.class);


  /*
   * Register the attributes and associations of the business object
   */
   protected void registerAttributes() throws AmsException
   {

      /* Register attributes defined in the Ancestor class */
      super.registerAttributes();

      /* panel_auth_group_oid - Unique identifier */
      attributeMgr.registerAttribute("panel_auth_group_oid", "panel_auth_group_oid", "ObjectIDAttribute");
      attributeMgr.registerReferenceAttribute("instrument_group_type", "instrument_group_type", "PANEL_INST_GROUP");
      attributeMgr.requiredAttribute("instrument_group_type");
      attributeMgr.registerAlias("instrument_group_type", getResourceManager().getText("PanelAuthorizationGroupDetail.instrumenttype", TradePortalConstants.TEXT_BUNDLE));
      attributeMgr.registerAttribute("payment_type", "payment_type");
      attributeMgr.registerReferenceAttribute("ccy_basis", "ccy_basis", "CURRENCY_BASIS");
      attributeMgr.requiredAttribute("ccy_basis");
      attributeMgr.registerAlias("ccy_basis", getResourceManager().getText("PanelAuthorizationGroupDetail.currbasis", TradePortalConstants.TEXT_BUNDLE));
      attributeMgr.registerAttribute("panelgroupconfpaymentinst", "panelgroupconfpaymentinst", "IndicatorAttribute");
            
      attributeMgr.registerAttribute("ACH_pymt_method_ind", "ACH_pymt_method_ind", "IndicatorAttribute");
      attributeMgr.registerAttribute("BCHK_pymt_method_ind", "BCHK_pymt_method_ind", "IndicatorAttribute");
      attributeMgr.registerAttribute("BKT_pymt_method_ind", "BKT_pymt_method_ind", "IndicatorAttribute");
      attributeMgr.registerAttribute("CBFT_pymt_method_ind", "CBFT_pymt_method_ind", "IndicatorAttribute");
      attributeMgr.registerAttribute("CCHK_pymt_method_ind", "CCHK_pymt_method_ind", "IndicatorAttribute");
      attributeMgr.registerAttribute("RTGS_pymt_method_ind", "RTGS_pymt_method_ind", "IndicatorAttribute");
      attributeMgr.registerAttribute("FTBA_pymt_method_ind", "FTBA_pymt_method_ind", "IndicatorAttribute");
      attributeMgr.registerAttribute("matchin_approval_ind", "matchin_approval_ind", "IndicatorAttribute");
      attributeMgr.registerAttribute("inv_auth_ind", "inv_auth_ind", "IndicatorAttribute");
      attributeMgr.registerAttribute("credit_auth_ind", "credit_auth_ind", "IndicatorAttribute");
      attributeMgr.registerAttribute("AIR_ind", "AIR_ind", "IndicatorAttribute");
      attributeMgr.registerAttribute("ATP_ind", "ATP_ind", "IndicatorAttribute");
      attributeMgr.registerAttribute("EXP_COL_ind", "EXP_COL_ind", "IndicatorAttribute");
      attributeMgr.registerAttribute("EXP_DLC_ind", "EXP_DLC_ind", "IndicatorAttribute");
      attributeMgr.registerAttribute("EXP_OCO_ind", "EXP_OCO_ind", "IndicatorAttribute");
      attributeMgr.registerAttribute("IMP_DLC_ind", "IMP_DLC_ind", "IndicatorAttribute");
      attributeMgr.registerAttribute("LRQ_ind", "LRQ_ind", "IndicatorAttribute");
      attributeMgr.registerAttribute("GUA_ind", "GUA_ind", "IndicatorAttribute");
      attributeMgr.registerAttribute("SLC_ind", "SLC_ind", "IndicatorAttribute");
      attributeMgr.registerAttribute("SHP_ind", "SHP_ind", "IndicatorAttribute");
      attributeMgr.registerAttribute("RQA_ind", "RQA_ind", "IndicatorAttribute");
      attributeMgr.registerAttribute("SP_ind", "SP_ind", "IndicatorAttribute");
      attributeMgr.registerAttribute("DCR_ind", "DCR_ind", "IndicatorAttribute");

      //Rel9.0 CR 913 Start
      attributeMgr.registerAttribute("pay_mgm_inv_auth_ind","pay_mgm_inv_auth_ind","IndicatorAttribute");
      attributeMgr.registerAttribute("pay_upl_inv_auth_ind","pay_upl_inv_auth_ind","IndicatorAttribute");
      //Rel9.0 CR 913 End
            
      /* panel_auth_group_id - The ID to be displayed in dropdown. */
      attributeMgr.registerAttribute("panel_auth_group_id", "panel_auth_group_id");
      attributeMgr.requiredAttribute("panel_auth_group_id");
      attributeMgr.registerAlias("panel_auth_group_id", getResourceManager().getText("PanelAuthorizationGroupBeanAlias.panel_auth_group_id", TradePortalConstants.TEXT_BUNDLE));

        /* Pointer to the parent CorporateOrganization */
      attributeMgr.registerAttribute("corp_org_oid", "p_corp_org_oid", "ParentIDAttribute");

   
   /* Pointer to the component PanelAuthorizationRange */
   attributeMgr.registerAttribute("panel_range_1_oid", "panel_range_1_oid", "NumberAttribute");
   attributeMgr.registerAttribute("panel_range_2_oid", "panel_range_2_oid", "NumberAttribute");
   attributeMgr.registerAttribute("panel_range_3_oid", "panel_range_3_oid", "NumberAttribute");
   attributeMgr.registerAttribute("panel_range_4_oid", "panel_range_4_oid", "NumberAttribute");
   attributeMgr.registerAttribute("panel_range_5_oid", "panel_range_5_oid", "NumberAttribute");
   attributeMgr.registerAttribute("panel_range_6_oid", "panel_range_6_oid", "NumberAttribute");
   
   //Rel9.2 CR 914A 
   attributeMgr.registerAttribute("pay_credit_note_auth_ind","pay_credit_note_auth_ind","IndicatorAttribute");
   
 //SSikhakolli - Rel-9.4 CR-818 - adding new attribute
   attributeMgr.registerAttribute("settlement_instr_ind" , "settlement_instr_ind", "IndicatorAttribute");
   attributeMgr.registerAttribute("import_col_ind" , "import_col_ind", "IndicatorAttribute");
   }

   /*
   * Register the components of the business object
   */
   protected void registerComponents() throws RemoteException, AmsException
   {
   /* Register the components defined in the Ancestor class */
   super.registerComponents();
   registerOneToOneComponent("panel_range_1", "PanelAuthorizationRange", "panel_range_1_oid");
   registerOneToOneComponent("panel_range_2", "PanelAuthorizationRange", "panel_range_2_oid");
   registerOneToOneComponent("panel_range_3", "PanelAuthorizationRange", "panel_range_3_oid");
   registerOneToOneComponent("panel_range_4", "PanelAuthorizationRange", "panel_range_4_oid");
   registerOneToOneComponent("panel_range_5", "PanelAuthorizationRange", "panel_range_5_oid");
   registerOneToOneComponent("panel_range_6", "PanelAuthorizationRange", "panel_range_6_oid");
   }
}