
  

package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;
/**
 * Details for the various actions performed by Admin user on non-integrated customer transaction
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface BankTransactionHistoryHome extends EJBHome
{
   public BankTransactionHistory create()
      throws RemoteException, CreateException, AmsException;

   public BankTransactionHistory create(ClientServerDataBridge csdb)
      throws RemoteException, CreateException, AmsException;
}
