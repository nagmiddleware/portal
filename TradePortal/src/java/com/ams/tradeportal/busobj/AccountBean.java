
package com.ams.tradeportal.busobj;
import java.rmi.RemoteException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.StringFunction;



/*
 *
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class AccountBean extends AccountBean_Base
{
private static final Logger LOG = LoggerFactory.getLogger(AccountBean.class);

/**
* Performs any special "validate" processing that is specific to the
* descendant of BusinessObject. Specifically, it checks to see whether
* or not the the Account Number and Currency information has been entered correctly
*/
public void userValidate()
	throws RemoteException, AmsException {

	String acctOwnerType = this.getAttribute("owner_object_type");
	String acctNumber = this.getAttribute("account_number");
	String acctCurrency = this.getAttribute("currency");
	if (acctOwnerType.equals(TradePortalConstants.PARTY)) {
		if (acctNumber.equals("")) {
			this.getErrorManager().issueError(
							  TradePortalConstants.ERR_CAT_1,
							  TradePortalConstants.ACCT_NUMBER_MISSING);
		}
	}
	if (acctOwnerType.equals(TradePortalConstants.CORPORATE_ORG)) {
		if (acctNumber.equals("")) {
			this.getErrorManager().issueError(
				TradePortalConstants.ERR_CAT_1,
				TradePortalConstants.ACCT_NUMBER_MISSING);
		}
		if (acctCurrency.equals("")) {
			String currency = this.getResourceManager().getText(
				"CorpCust.CustomerAccountCurrency",
				TradePortalConstants.TEXT_BUNDLE);
			this.getErrorManager().issueError(
				TradePortalConstants.ERR_CAT_1,
				TradePortalConstants.CUST_ACCTS_DATA_MISSING_ERR,
				currency,acctNumber);
		}
	}
}
// End userValidate

	public String getBaseCurrencyOfAccount() throws RemoteException, AmsException
    {
	    CorporateOrganization corpOrg =null;
	    if (TradePortalConstants.INDICATOR_YES.equals(this.getAttribute("othercorp_customer_indicator")))
		    corpOrg = (CorporateOrganization) createServerEJB("CorporateOrganization", Long.parseLong(this.getAttribute("other_account_owner_oid")));
	    else
		    corpOrg = (CorporateOrganization) createServerEJB("CorporateOrganization", Long.parseLong(this.getAttribute("owner_oid")));

	    return corpOrg.getAttribute("base_currency_code");
    }

	public String getClientBankOfAccount() throws RemoteException, AmsException
    {
	    CorporateOrganization corpOrg =null;
	    if (TradePortalConstants.INDICATOR_YES.equals(this.getAttribute("othercorp_customer_indicator")))
		    corpOrg = (CorporateOrganization) createServerEJB("CorporateOrganization", Long.parseLong(this.getAttribute("other_account_owner_oid")));
	    else
		    corpOrg = (CorporateOrganization) createServerEJB("CorporateOrganization", Long.parseLong(this.getAttribute("owner_oid")));

	    return corpOrg.getAttribute("client_bank_oid");
    }

	public String getOwnerOfAccount() throws RemoteException, AmsException
    {
	     if (TradePortalConstants.INDICATOR_YES.equals(this.getAttribute("othercorp_customer_indicator")))
		    return this.getAttribute("other_account_owner_oid");
	    else
		    return this.getAttribute("owner_oid");
    }

// NSX 10/07/11 - CR-581/640 Rel. 7.1 - Begin
/**
 * This method returns the country code 
 * stored with the account, or the translated ISO country code from REFDATA.
 *
 * @return String bank branch country if available.
 * @throws RemoteException
 * @exception   com.amsinc.ecsg.frame.AmsException, RemoteException
 */
public String getBankBranchCountryInISO() throws RemoteException, AmsException     {

	String country = getAttribute("bank_country_code");
	if (StringFunction.isNotBlank(country))
	{
		
		StringBuffer selectCountrySQL = new StringBuffer();

		selectCountrySQL.append("SELECT ADDL_VALUE FROM REFDATA WHERE CODE = ? AND TABLE_TYPE = ? ");

		DocumentHandler countryDoc = DatabaseQueryBean.getXmlResultSet(selectCountrySQL.toString(), false, country,TradePortalConstants.COUNTRY_ISO3116_3 );

		if (countryDoc != null)
			country = countryDoc.getAttribute("/ResultSetRecord(0)/ADDL_VALUE");
	}
	return country;
 }
	// NSX 10/07/11 - CR-581/640 Rel. 7.1 - End
}
