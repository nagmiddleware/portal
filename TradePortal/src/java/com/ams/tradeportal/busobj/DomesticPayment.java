



package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;

import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;


/**
 *
 *
 *     Copyright  � 2003
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public interface DomesticPayment extends TradePortalBusinessObject
{
	public void createNewDomesticPayments(String originalTransOid, String newTransactionOid)
	throws RemoteException, AmsException;

	//IAZ CR-586 IR-VRUK091652765 Add Overload CreateNew Method
	public void createNewDomesticPayments(String originalTransOid, String newTransactionOid, boolean fixedTemplateFlag)
	throws RemoteException, AmsException;

	public boolean updateDomesticPayment (DocumentHandler inputDoc, DocumentHandler outputDoc)
	throws RemoteException, AmsException;

	public String verifyDomesticPayment (DocumentHandler inputDoc, Hashtable pmValueDates,
			String creditAccountBankCountryCode, String debitAccountBankCountryCode) //trudden CR-564 08/31/10 Performance improvement for VerifyDomesticPayment
	throws RemoteException, AmsException;

	public String verifyDomesticPayment (DocumentHandler inputDoc, Hashtable pmValueDates,
			String creditAccountBankCountryCode, String debitAccountBankCountryCode,
			boolean isFixedWidthInitiallyConsideredFileUpload) //BSL IR# PBUL042033374 04/29/11 - Add overload
	throws RemoteException, AmsException;
	
	public String verifyDomesticPayment (DocumentHandler inputDoc, Hashtable pmValueDates,
			String creditAccountBankCountryCode, String debitAccountBankCountryCode,
			boolean isFixedWidthInitiallyConsideredFileUpload, boolean isErrorFoundInPaneProcess)
	throws RemoteException, AmsException;

	public String deleteDomesticPayment (DocumentHandler inputDoc)
	throws RemoteException, AmsException;
	
	public DocumentHandler processDPTerms(DocumentHandler inputDoc, int numberOfPayees) throws RemoteException, AmsException;      //NSX - PMUL012565883 Re. 6.1.0
	
	public DocumentHandler calculatePaymentDate(DocumentHandler inputDoc, String debitAccountBankCountryCode) throws RemoteException, AmsException; 	// NSX 10/07/11 - CR-581/640 Rel. 7.1 -
	
	public String setAllAttributes (String[] attributePaths, String[] attributeValues,String corpOrgName)
     throws RemoteException, AmsException;


}
