

  

package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;


/**
 * Business Object to store aliases (alternate names/identifiers) to be used
 * as indexes on the Customer File depending on the specific Message Category
 * for the Incoming Generic Message
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface CustomerAlias extends TradePortalBusinessObject
{   
}
