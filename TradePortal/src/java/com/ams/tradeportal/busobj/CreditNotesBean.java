package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.frame.ObjectIDCache;
import com.amsinc.ecsg.util.DateTimeUtility;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.StringFunction;

public class CreditNotesBean extends CreditNotesBean_Base {
private static final Logger LOG = LoggerFactory.getLogger(CreditNotesBean.class);


	/**
	 * Performs any special "issueOptimisticLockError" processing that is 
	 * specific to the descendant of BusinessObject.
	 * 
	 * @throws AmsException
	 * @throws RemoteException
	 */
	protected void issueOptimisticLockError() throws AmsException, RemoteException {
		if (!getRetryOptLock()) {
			getErrorManager().issueError(getClass().getName(),
					TradePortalConstants.UPLOAD_INV_OPT_LOCK_EXCEPTION);
		}
	}

	/**
	 * Identifies and retrieves the Matching Rule for this invoice.
	 * First it looks for a rule with a matching buyer_name.  If no match is
	 * found for the buyer_name, it looks for a rule with a matching buyer_id.
	 * 
	 * @throws AmsException
	 * @throws RemoteException
	 */
	public ArMatchingRule getMatchingRule() throws AmsException, RemoteException {
		final String outerSelect = "SELECT DISTINCT ar_matching_rule_oid"
			+ " FROM ar_matching_rule WHERE ";
		final String innerSelect = " OR ar_matching_rule_oid IN"
			+ " (SELECT p_ar_matching_rule_oid FROM";

		StringBuilder sqlBuilder = null;
		DocumentHandler resultSet = null;
		String tradePartnerOid = null;
        String corpOrgOid = getAttribute("corp_org_oid");
		
        String buyerName = TradePortalConstants.INVOICE_TYPE_REC_MGMT.equals(getAttribute("invoice_classification"))?getAttribute("buyer_name"):getAttribute("seller_name");//SHR CR708 Rel8.1.1 
		
		if (StringFunction.isNotBlank(buyerName)) {
			buyerName = buyerName.toUpperCase().trim();

			sqlBuilder = new StringBuilder(outerSelect);
			sqlBuilder.append("p_corp_org_oid = ?");
			sqlBuilder.append(" and (buyer_name = unistr(?)");
			sqlBuilder.append(innerSelect);
			sqlBuilder.append(" ar_buyer_name_alias WHERE buyer_name_alias = unistr(?)))"); // Nar -MKUM061235356

			resultSet = DatabaseQueryBean.getXmlResultSet(sqlBuilder.toString(), false, corpOrgOid,buyerName, buyerName);
			if (resultSet != null) {
				tradePartnerOid = resultSet.getAttribute("/ResultSetRecord/AR_MATCHING_RULE_OID");
			}
		}

		if (tradePartnerOid == null) {
			String buyerId = TradePortalConstants.INVOICE_TYPE_REC_MGMT.equals(getAttribute("invoice_classification"))?getAttribute("buyer_id"):getAttribute("seller_id");//SHR CR708 Rel8.1.1
			if (StringFunction.isNotBlank(buyerId)) {
				buyerId = buyerId.toUpperCase().trim();

				sqlBuilder = new StringBuilder(outerSelect);
				sqlBuilder.append(" p_corp_org_oid = ? and (buyer_id = unistr(?)");
				sqlBuilder.append(innerSelect);
				sqlBuilder.append(" ar_buyer_id_alias WHERE buyer_id_alias = unistr(?)))"); // Nar -MKUM061235356

				resultSet = DatabaseQueryBean.getXmlResultSet(sqlBuilder.toString(), false, corpOrgOid, buyerId, buyerId);
				if (resultSet != null) {
					tradePartnerOid = resultSet.getAttribute("/ResultSetRecord/AR_MATCHING_RULE_OID");
				}
			}
		}

		if (StringFunction.isNotBlank(tradePartnerOid)) {
			ArMatchingRule matchingRule = (ArMatchingRule)createServerEJB("ArMatchingRule",
					Long.parseLong(tradePartnerOid));
			setAttribute("tp_rule_name", matchingRule.getAttribute("buyer_name"));
			// In case buyer_name or buyer_id are blank or an alias value,
			// update with the actual values from the rule.
			if(TradePortalConstants.INVOICE_TYPE_REC_MGMT.equals(getAttribute("invoice_classification")) && StringFunction.isBlank(getAttribute("buyer_name"))){
			setAttribute("buyer_name", matchingRule.getAttribute("buyer_name"));
			//setAttribute("buyer_id", matchingRule.getAttribute("buyer_id"));
			}
			if(TradePortalConstants.INVOICE_TYPE_PAY_MGMT.equals(getAttribute("invoice_classification")) && StringFunction.isBlank(getAttribute("seller_name"))){
				setAttribute("seller_name", matchingRule.getAttribute("buyer_name"));
			//	setAttribute("seller_id",matchingRule.getAttribute("buyer_id"));
			}

			return matchingRule;
		}

		return null;
	}
	/* Perform processing for a New instance of the business object */

    protected void userNewObject() throws AmsException, RemoteException
    {
        /* Perform any New Object processing defined in the Ancestor class */
        super.userNewObject();
        this.setAttribute("creation_date_time", DateTimeUtility.getGMTDateTime(true, false));//do not use override date
    }
	
  
    //Get Trade partner name based on invoice type.
    
    public String getTpRuleName() throws AmsException, RemoteException {
    	String tpName  =getTpRuleName(null);
    	
    	return tpName;
    }
    
    public String getTpRuleName(ArMatchingRule matchingRule) throws AmsException, RemoteException {
    	String tpName  = StringFunction.isNotBlank(getAttribute("seller_name"))?getAttribute("seller_name"):getAttribute("seller_id");
		
    	return tpName;
		}

    
    
    public void close(String uploadUser)throws AmsException, RemoteException{
    	
        this.setAttribute("credit_note_status", TradePortalConstants.CREDIT_NOTE_STATUS_CLO); 

        InvoiceHistory invoiceHistory = (InvoiceHistory) this.createServerEJB("InvoiceHistory");
        invoiceHistory.newObject();
        invoiceHistory.setAttribute("upload_invoice_oid", this.getAttribute("upload_credit_note_oid"));
        invoiceHistory.setAttribute("invoice_status", TradePortalConstants.CREDIT_NOTE_STATUS_CLO);
        invoiceHistory.setAttribute("action", TradePortalConstants.CREDIT_NOTE_STATUS_CLO);
        invoiceHistory.setAttribute("action_datetime", DateTimeUtility.getGMTDateTime());
        invoiceHistory.setAttribute("user_oid",uploadUser);
        
        int Success = invoiceHistory.save();
        
        if(Success == 1){
        	this.save();
       }

    }
    
    // Nar CR-914A Rel9.2.0.0 02/01/2015 Begin
    
    /**
     * This method is used to modify Invoice Credit Note relation data to maintain current applied amount.
     * @param creditNoteAppliedInvList
     * @throws RemoteException
     * @throws AmsException
     */
    public boolean modifyInvoiceCreditRelationData (List<DocumentHandler> creditNoteAppliedInvList ) throws RemoteException, AmsException {
		
    	boolean isCreditNoteApplied = false;
    	Map<String, String> invoiceData = new HashMap<String, String>();
    	StringBuilder inSql = null;
	
    	for ( DocumentHandler doc : creditNoteAppliedInvList ) {
    		if ( inSql == null) {
    			inSql = new StringBuilder("?");
    		}
    		else {
    			inSql.append(", ?");
    		}
    		invoiceData.put(doc.getAttribute("/CreditNoteAppliedtoInvoiceID"), doc.getAttribute("/CreditNoteAmountApplied"));
    	}
    	
    	List<Object> list = new ArrayList<Object>();
    	list.add(this.getAttribute("upload_credit_note_oid"));
    	list.add(this.getAttribute("corp_org_oid"));
    	list.addAll(invoiceData.keySet());
    	String sqlStatement = "SELECT unique inv.invoice_reference_id as invoice_reference_id, inv.invoice_oid as invoice_oid, " +
    		                  "(case when rel.a_upload_credit_note_oid != ? then null else rel.invoice_credit_note_rel_oid end) as invoice_credit_note_rel_oid, " +
    		                  "rel.credit_note_applied_amount as credit_note_applied_amount " +
                              "FROM  invoice inv, invoices_credit_notes_relation rel " +
                              "WHERE inv.invoice_oid = rel.a_upload_invoice_oid ( + ) and inv.a_corp_org_oid = ? "  +
                              "and inv.invoice_reference_id in (" +  inSql  + ") ";
       DocumentHandler appliedinvoiceDoc = DatabaseQueryBean.getXmlResultSet(sqlStatement, true, list);      
       if ( appliedinvoiceDoc != null) {
    	List<DocumentHandler> appliedinvoiceList = appliedinvoiceDoc.getFragmentsList("/ResultSetRecord");
        PreparedStatement pstmt1 = null;
	    PreparedStatement pstmt2 = null;
	    Connection con = null;
	    try{
	     con = DatabaseQueryBean.connect(true);
	     con.setAutoCommit(false);
         for (DocumentHandler doc : appliedinvoiceList) {

    	   if ( StringFunction.isNotBlank(doc.getAttribute("/INVOICE_CREDIT_NOTE_REL_OID"))) {// update applied amount
    		   if (pstmt1 == null) {
    			   String sql = "UPDATE invoices_credit_notes_relation SET credit_note_applied_amount = ? WHERE invoice_credit_note_rel_oid = ?";
    			   pstmt1 = con.prepareStatement(sql); 
    		   }
    		   String appiedAmount = invoiceData.get(doc.getAttribute("/INVOICE_REFERENCE_ID"));
    		   BigDecimal preAppliedAmount = BigDecimal.ZERO;
    		   if ( StringFunction.isNotBlank(doc.getAttribute("/CREDIT_NOTE_APPLIED_AMOUNT")) ){
    			   preAppliedAmount = doc.getAttributeDecimal("/CREDIT_NOTE_APPLIED_AMOUNT");
    		   }
    		   BigDecimal totalAppliedamt = preAppliedAmount.subtract( new BigDecimal(appiedAmount));
    		   String relationOid = doc.getAttribute("/INVOICE_CREDIT_NOTE_REL_OID");
    		   updatePreAppliedInvData(pstmt1, totalAppliedamt, relationOid);
    	   }
    	   else{
    		   if ( pstmt2 == null ) { // insert applied amount in relation table
    			   String sql = "INSERT into invoices_credit_notes_relation (invoice_credit_note_rel_oid, credit_note_applied_amount, a_upload_credit_note_oid, a_upload_invoice_oid) " +
       	                   " VALUES (?,?,?,?)";
    			   pstmt2 = con.prepareStatement(sql);
    		   }
    		   BigDecimal appiedAmount = BigDecimal.ZERO;    		   
    		   if(StringFunction.isNotBlank(invoiceData.get(doc.getAttribute("/INVOICE_REFERENCE_ID")))){
    			   appiedAmount = (new BigDecimal(invoiceData.get(doc.getAttribute("/INVOICE_REFERENCE_ID")))).negate();
    		   }
    		   insertPreAppliedInvData( pstmt2, appiedAmount, this.getAttribute("upload_credit_note_oid"), 
    				   doc.getAttribute("/INVOICE_OID")  );
    	   }
       }
        
       if ( pstmt1 != null ){
    	   pstmt1.executeBatch();
       }
       if ( pstmt2 != null ){
    	   pstmt2.executeBatch();
       }
       
       con.commit();
       isCreditNoteApplied = true;
       
	  }catch( AmsException | SQLException e )
	  {
          throw new AmsException(e);
	  }
	  finally {
		  try {
			 if ( pstmt1 != null) pstmt1.close();
			 if ( pstmt2 != null) pstmt2.close();
			 if (con != null) con.close();			  
		  }catch( SQLException e ){
			  LOG.error("CreditNotesBean: SQL exception caught at modifyInvoiceCreditRelationData(): ",e);
		  }		  
	  }
     }
       
     return isCreditNoteApplied;
	}

	/**
     * This method is used to create new entry for applied amount to invoice by credit note.
     * @param pstmt
     * @param appliedAmount
     * @param creditNoteOid
     * @param invoiceOid
     * @throws SQLException
     * @throws AmsException
     */
    private static void insertPreAppliedInvData( PreparedStatement pstmt, BigDecimal appliedAmount, String creditNoteOid, String invoiceOid) 
    		throws SQLException, AmsException{
    	
    	pstmt.setLong(1, ObjectIDCache.getInstance(AmsConstants.OID).generateObjectID());
    	pstmt.setBigDecimal(2, appliedAmount); 
   		pstmt.setLong(3, Long.parseLong(creditNoteOid));
   		pstmt.setLong(4, Long.parseLong(invoiceOid));   		 		
   		pstmt.addBatch();
    }
    
    /**
     * This method is used to update applied amount of invoice.
     * @param pstmt
     * @param appliedAmount
     * @param relationOid
     * @throws SQLException
     * @throws AmsException
     */
    private static void updatePreAppliedInvData( PreparedStatement pstmt, BigDecimal appliedAmount, String relationOid) 
    		throws SQLException, AmsException{
    	
   		pstmt.setBigDecimal(1, appliedAmount); 
    	pstmt.setLong(2, Long.parseLong(relationOid));
   		pstmt.addBatch();
    }
    
    /**
     * This method is used to save credit note data come sin INVSTO message
     * @param invoiceDoc
     * @throws RemoteException
     * @throws AmsException
     * @throws SQLException 
     */
    public void setPayableCreditNoteData ( DocumentHandler invoiceDoc ) throws RemoteException, AmsException {
    	
    	 String creditNoteStatus = invoiceDoc.getAttribute("/InvoiceStat");
    	 if ( StringFunction.isNotBlank(invoiceDoc.getAttribute("/InvoiceProcessingStatus"))) {
    		 creditNoteStatus = invoiceDoc.getAttribute("/InvoiceProcessingStatus");
    	 }
    	 this.setAttribute("credit_note_status", creditNoteStatus);
    	 BigDecimal appliedAmt = null;
    	 if( StringFunction.isNotBlank(invoiceDoc.getAttribute("/CreditNoteAmountApplied")) ) {
    		appliedAmt = invoiceDoc.getAttributeDecimal("/CreditNoteAmountApplied");
    		if ( appliedAmt.compareTo(BigDecimal.ZERO) != 0 && appliedAmt.signum() == 1) {
    			appliedAmt = appliedAmt.negate();
    		}
        	this.setAttribute("credit_note_applied_amount", appliedAmt.toPlainString() );
    	 }
		 this.setAttribute("utilized_amount", invoiceDoc.getAttribute("/CreditNoteAmountUtilized"));
		 this.setAttribute("credit_note_applied_status", invoiceDoc.getAttribute("/CreditNoteApplicationStatus"));
		 this.setAttribute("credit_note_utilised_status", invoiceDoc.getAttribute("/CreditNoteUtilizationStatus"));
		 
		 if ( TradePortalConstants.CREDIT_NOTE_STATUS_DEC.equals(creditNoteStatus) )  {
			 try {
			    unlinkAppliedInvoices( this.getAttribute("upload_credit_note_oid") );	
			 } catch( SQLException e) {
				 throw new AmsException(e);
			 }	
		 }
    } 
   // Nar CR-914A Rel9.2.0.0 02/01/2015 End
   /**
    * This method is used to unlink applied invoices if credit note has been declined.
    * @param invoiceDoc
    * @throws AmsException
    * @throws RemoteException
    * @throws SQLException
    */
	private void unlinkAppliedInvoices( String uploadCreditNoteOid )
			throws AmsException, SQLException {

		String deleteSql = "DELETE FROM invoices_credit_notes_relation WHERE a_upload_credit_note_oid = ?";
		DatabaseQueryBean.executeUpdate(deleteSql, true, new Object[] { uploadCreditNoteOid });
	}
	
	
	 public int save() throws NumberFormatException, AmsException, RemoteException {
	    	int ret = super.save();
	    	
	    	if(ret==1){
	    	String userOid = this.getAttribute("user_oid");
	    	String corpOrgOid = this.getAttribute("corp_org_oid");
	    	String action = this.getAttribute("action");
	    	if(StringFunction.isNotBlank(action)){
			 if(StringFunction.isBlank(userOid) && StringFunction.isNotBlank(corpOrgOid)){
				 CorporateOrganization corpOrgEJB = (CorporateOrganization) this.createServerEJB("CorporateOrganization", Long.parseLong(corpOrgOid));
				 userOid  = corpOrgEJB.getAttribute("upload_system_user_oid");
			 }
			 InvoiceHistory invHistory = (InvoiceHistory) this.createServerEJB("InvoiceHistory");
			 invHistory.newObject();
			 invHistory.setAttribute("action_datetime", DateTimeUtility.getGMTDateTime(true, true));
			 invHistory.setAttribute("action", action);
			 invHistory.setAttribute("invoice_status", this.getAttribute("credit_note_status"));
			 invHistory.setAttribute("upload_invoice_oid", this.getAttribute("upload_credit_note_oid"));
			 invHistory.setAttribute("user_oid", userOid);
			 if(TradePortalConstants.INDICATOR_YES.equals(this.getAttribute("reqPanleAuth"))){
				 User user = (User) this.createServerEJB("User", Long.parseLong(userOid));
				 String userPanelAuthLevel = user.getAttribute("panel_authority_code");
				 invHistory.setAttribute("panel_authority_code", userPanelAuthLevel);
			 }
			 
			 if(invHistory.save(false)!=1)
			 {
				 LOG.info("Error occured while creating Invoice History Log from CRNote for action: {} ",action);
				 return 0;
			 }
			
	    	}
	    	
	    	if(StringFunction.isNotBlank(this.getAttribute("remove_crnote_oid"))){
	    		CreditNotes oldInvData = (CreditNotes)this.createServerEJB( "CreditNotes",
	                                 Long.parseLong(this.getAttribute("remove_crnote_oid")));
	           
	             if (oldInvData != null) {
	                  oldInvData.delete();
	           }
	    	}
	      }
	    return 1;
		}
	
}
