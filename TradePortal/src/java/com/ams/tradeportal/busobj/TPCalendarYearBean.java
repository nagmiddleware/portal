
package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import java.util.*;
import com.ams.tradeportal.common.*;

/*
 *
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class TPCalendarYearBean extends TPCalendarYearBean_Base
{
private static final Logger LOG = LoggerFactory.getLogger(TPCalendarYearBean.class);

 /**
	* This method calculates Next business Day for given Execute/Start day, number of offset days and weekend pattern
	* for the year of this Calendar.   .
	* @param String strInputDate - Execute/Start date
	* @param int valueDateOffset - number of offset days
	* @param int weekend1 - day of week for the first weekend day (Sun = 1; Sat = 7)
	* @param int weekend2 - day of week for the second weekend day (Sun = 1; Sat = 7)
	* @return String nextBsuinessDay -- in jPylon DateTime format "MM/dd/yyyy 00:00:00"
    * @exception   com.amsinc.ecsg.frame.AmsException, RemoteException
   */

	public String getNextBusinessDay(String strInputDate, int valueDateOffset, int weekend1, int weekend2)
	throws RemoteException, AmsException
	  {

		//String strInputDate = inputDoc.getAttribute("/Transaction/payment_date");
		//int intYear 	= (new Integer(strInputDate.substring(6,10))).intValue();

        //Populate Year's Initial Week.
        int intYear = this.getAttributeInteger("year");
		GregorianCalendar cal = new GregorianCalendar(intYear, 0, 1);
		LOG.debug("TPCalendarYearBean::First Day of Week this year: {}" , cal.get(Calendar.DAY_OF_WEEK));

		int firstDay = cal.get(Calendar.DAY_OF_WEEK) -1;
		int daysInWeek [] = {0,0,0,0,0,0,0};
		int tc = 0;

		for (tc = 0; tc < 7; tc++)
		{
			if (tc < firstDay)
				daysInWeek [tc] = 8 - firstDay + (tc);
			else
				daysInWeek[tc] = tc - firstDay + 1;
		}


        //Obtain Year-String with Holidays for this year and Update it with Weekends
        String calendarStr = this.getAttribute("days_in_year");

        //LOG.info("1: " + calendarStr);
        int daysThisYear = 365;
        if (cal.isLeapYear(intYear))
        	daysThisYear = 366;
		tc = 1;
		StringBuffer tmpStrBuf = new StringBuffer(calendarStr);
		//LOG.info("2 " + tmpStrBuf);
		int maxWeekend1 = 1000;
		if ((weekend1 > 0)&&(weekend1 < 8))
			maxWeekend1 = daysInWeek [weekend1 - 1];
		int maxWeekend2 = 1000;
		if ((weekend2 > 0)&&(weekend2 < 8))
			maxWeekend2 = daysInWeek [weekend2 - 1];
		while ((maxWeekend1 <= daysThisYear)||(maxWeekend1 <= daysThisYear))
		{
			if (maxWeekend1 <= daysThisYear)
				tmpStrBuf.replace(maxWeekend1 -1, maxWeekend1, "1");
			if (maxWeekend2 <= daysThisYear)
				tmpStrBuf.replace(maxWeekend2 -1, maxWeekend2, "1");
			//IAZ IR-SWUK012248661 01/22/2010 Begin
			/*maxWeekend1 += tc*7;
			maxWeekend2 += tc*7;
			tc++;*/
			maxWeekend1 += 7;
			maxWeekend2 += 7;
			//IAZ IR-SWUK012248661 01/22/2010 End
		}
		calendarStr = tmpStrBuf.toString();
		LOG.debug("Final Year String: {}" , calendarStr);


		//Determine Start Date in the Year-String
		int intMonth 	= (new Integer(strInputDate.substring(0,2))).intValue() -1;
		int intDay 		= (new Integer(strInputDate.substring(3,5))).intValue();
		int ic = 0;
        LOG.debug("input is {} ; {} ; {}" ,
        		new Object[] {intMonth, intDay , intYear});

		int daysInMonth[] = {31,28,31,30,31,30,31,31,30,31,30,31};
		if (cal.isLeapYear(intYear))
			daysInMonth[1] = 29;

		int calDate = 0;
		int tmpDate = 0;
		for (ic = 0; ic < intMonth; ic++)
			tmpDate += daysInMonth[ic];

		calDate = tmpDate + intDay;// + valueDateOffset;
		LOG.debug("TPCalendarYearBean::Execution/Start Day: {}" , calDate);

		//Calculate next buisness day in the Year-String by adding offset days to execute/start day and
		//   taking in consideartion all holdiays and weekends
        tc = 0;

        //Handle special case of offset days = 0: still need to handle holidays/weekends on this date
        if (valueDateOffset == 0)
        while (tc == 0)
        {
			if (calendarStr.substring(calDate-1, calDate).equals("0"))
			    tc++;
			else
				calDate++;
		}

        //Handle the rest of cases
        //Need to check Day 0 as it may be a Holiday itself, as well as to be a fisrt of several holidays --
        // so we check date before incrementing day pointer calDate
        //Need to chek the last day too - so we run till tc counter = offset days (otherwise, we miss last day)
        // but as soon as we pass number of offset days, we must not increemnt pointer any longer.
        //while(tc < valueDateOffset)									//IAZ IR-SWUK012248661 01/22/2010 CHF
        while(tc <= valueDateOffset)									//IAZ IR-SWUK012248661 01/22/2010 CHT
        {
			//calDate++;
			LOG.debug("{} : {} {}" ,new Object[]{tc, calDate , calendarStr.substring(calDate-1, calDate)});
			if (calendarStr.substring(calDate-1, calDate).equals("0"))
				tc++;
			if (tc <= valueDateOffset)									//IAZ IR-SWUK012248661 01/22/2010 Add
				calDate++;
        }

		LOG.debug("TPCalendarYearBean::End Date: {}" , calDate);

		//Get "Normal" Month and Day
		for (intMonth = ic; (tmpDate + daysInMonth[intMonth]) < calDate; intMonth++)
		{
			tmpDate += daysInMonth[intMonth];
		}
		intDay = calDate - tmpDate;

		LOG.debug("PCalendarYearBean::Final::intMonth: {} ; intDate:{}" , intMonth ,intDay);

		return TPDateTimeUtility.buildJPylonDateTime(Integer.toString(intDay), Integer.toString(intMonth +1), Integer.toString(intYear));

	  }
        
            public boolean isHoliday(java.util.Calendar date) throws AmsException, RemoteException {

                int dayOfYear = date.get(Calendar.DAY_OF_YEAR);
                String daysInYear = this.getAttribute("days_in_year");
                if (daysInYear.charAt(dayOfYear - 1) == '1') {
                    return true;
                }
                return false;
                }

}
