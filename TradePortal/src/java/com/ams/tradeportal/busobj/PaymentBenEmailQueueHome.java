
  

package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;
/**
 * Placing a row into this table will cause an e-mail to be sent for each beneficiaries
 * of the associated FTDP transaction. 
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface PaymentBenEmailQueueHome extends EJBHome
{
   public PaymentBenEmailQueue create()
      throws RemoteException, CreateException, AmsException;

   public PaymentBenEmailQueue create(ClientServerDataBridge csdb)
      throws RemoteException, CreateException, AmsException;
}
