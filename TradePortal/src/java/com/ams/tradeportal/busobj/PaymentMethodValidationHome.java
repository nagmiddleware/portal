
  

package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;
/**
 * Rule for Payment Method Validation.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface PaymentMethodValidationHome extends EJBHome
{
   public PaymentMethodValidation create()
      throws RemoteException, CreateException, AmsException;

   public PaymentMethodValidation create(ClientServerDataBridge csdb)
      throws RemoteException, CreateException, AmsException;
}
