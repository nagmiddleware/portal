

  

package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;


/**
 * 
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface ShipmentTerms extends TradePortalBusinessObject
{   
	public void copy(com.ams.tradeportal.busobj.ShipmentTerms source) throws RemoteException, AmsException;

	public void setPOGoodsDescription(com.amsinc.ecsg.frame.ComponentList poLineItemList, int numberOfPOLineItems) throws RemoteException, AmsException;

	public void setPOGoodsDescription(java.util.Vector poOids, boolean incremental) throws RemoteException, AmsException;

	public void setPOGoodsDescription(java.util.Vector poOids) throws RemoteException, AmsException;

	public void setPOGoodsDescription(com.ams.tradeportal.busobj.POLineItem poLineItem) throws RemoteException, AmsException;

	public void validateRequiredAttributes(String[] requiredAttributes, String shipmentNumber) throws RemoteException, RemoteException, AmsException;

	public java.lang.String getAlias(java.lang.String attributeName) throws RemoteException, RemoteException;

	public void removeAssignedPOs(java.lang.String instrumentOid, java.lang.String transactionOid) throws RemoteException, AmsException;
	//ShilpaR CR707 Rel8.0
	public void updateAssignedStrucPOStatus(java.lang.String instrumentOid, java.lang.String transactionOid, Map map) throws RemoteException, AmsException;

}
