package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;

public class InvoiceLineItemDetailBean_Base extends TradePortalBusinessObjectBean {
private static final Logger LOG = LoggerFactory.getLogger(InvoiceLineItemDetailBean_Base.class);

	 
	/*
	 * Describes the mapping of an uploaded file to Invoice definiton.   
	 * The fields contained in the file are described, the file
	 * format is specified.
	 * 
	 * Can also be used to describe order of Invoice Summary and line Item detail data
	 *     Copyright  � 2003                         
	 *     American Management Systems, Incorporated 
	 *     All rights reserved
	 */
	  
	  /* 
	   * Register the attributes and associations of the business object
	   */
	   protected void registerAttributes() throws AmsException
	   {  

	      /* Register attributes defined in the Ancestor class */
	      super.registerAttributes();
	      
	      /* inv_upload_definition_oid - Unique identifier */
	      attributeMgr.registerAttribute("upload_line_item_detail_oid", "upload_line_item_detail_oid", "ObjectIDAttribute");
	      /*  line item ID at the line item details level */
	      attributeMgr.registerAttribute("line_item_id", "line_item_id");
	      /*  unit price of the item being shipped under the invoice */
	      //attributeMgr.registerAttribute("unit_price", "unit_price","NumberAttribute");
	      attributeMgr.registerAttribute("unit_price", "unit_price", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
	      /* This is the unit of measure applicable to the price */
	      attributeMgr.registerAttribute("unit_of_measure_price", "unit_of_measure_price");
	      /* invoiced quantity of the item being shipped under the invoice */
	      attributeMgr.registerAttribute("inv_quantity", "inv_quantity", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
	      /* This is the unit of measure applicable to the quantity */
	      attributeMgr.registerAttribute("unit_of_measure_quantity", "unit_of_measure_quantity");
	      /* user defined product characteristics fields in the PPM (and the TSU Invoice format) */
	      attributeMgr.registerAttribute("prod_chars_ud1_type", "prod_chars_ud1_type");
	      attributeMgr.registerAttribute("prod_chars_ud2_type", "prod_chars_ud2_type");
	      attributeMgr.registerAttribute("prod_chars_ud3_type", "prod_chars_ud3_type");
	      attributeMgr.registerAttribute("prod_chars_ud4_type", "prod_chars_ud4_type");
	      attributeMgr.registerAttribute("prod_chars_ud5_type", "prod_chars_ud5_type");
	      attributeMgr.registerAttribute("prod_chars_ud6_type", "prod_chars_ud6_type");
	      attributeMgr.registerAttribute("prod_chars_ud7_type", "prod_chars_ud7_type");
	      /* user defined product characteristics field values in the PPM (and the TSU Invoice format) */
	      attributeMgr.registerAttribute("prod_chars_ud1_val", "prod_chars_ud1_val");
	      attributeMgr.registerAttribute("prod_chars_ud2_val", "prod_chars_ud2_val");
	      attributeMgr.registerAttribute("prod_chars_ud3_val", "prod_chars_ud3_val");
	      attributeMgr.registerAttribute("prod_chars_ud4_val", "prod_chars_ud4_val");
	      attributeMgr.registerAttribute("prod_chars_ud5_val", "prod_chars_ud5_val");
	      attributeMgr.registerAttribute("prod_chars_ud6_val", "prod_chars_ud6_val");
	      attributeMgr.registerAttribute("prod_chars_ud7_val", "prod_chars_ud7_val");
	      attributeMgr.registerAttribute("invoice_summary_goods_oid", "p_invoice_summary_goods_oid", "ParentIDAttribute");
	      /* derived_line_item_Ind - Indicates whether or not this Invoice Line Item was automatically
          added by system as opposed to being added by a user.

          Y=this INV line item was derived
          N=this INV line item was added by user */
	      attributeMgr.registerAttribute("derived_line_item_ind", "derived_line_item_ind", "IndicatorAttribute");//SHR CR708 Rel8.1.1
	        
	   }
	}
