

/*
 * This file is generated from the model.  Normally it should not be modified manually.
 * Contact the modeler first.  (In a pinch, it can be modified first and then retrofitted
 * to the model.)
 *
 */
package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;


/*
 * Parties and corporate customers can be set up to be related to accounts.
 * This account data is used on the loan request and funds transfer instruments.
 *
 *     Copyright  � 2003
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class ReferenceDataPendingBean_Base extends TradePortalBusinessObjectBean
{
private static final Logger LOG = LoggerFactory.getLogger(ReferenceDataPendingBean_Base.class);


  /*
   * Register the attributes and associations of the business object
   */
   protected void registerAttributes() throws AmsException
   {

      /* Register attributes defined in the Ancestor class */
      super.registerAttributes();

      /* account_oid - Unique identifier */
      attributeMgr.registerAttribute("pending_data_oid", "pending_data_oid", "ObjectIDAttribute");

      /* change_type - Indicates the type of change (Create, Update, Delete) that occurred on the
      reference data that the AuditLog record corresponds to. */
      attributeMgr.registerReferenceAttribute("change_type", "change_type", "CHANGE_TYPE");

      /* class_name - The type of reference data that was deleted.  The EJB Name(for example:
      User, CorporateOrganization) of the data that was added, deleted, or changed. */
      attributeMgr.registerAttribute("class_name", "class_name");
      
      /* owner_org_oid - A pointer to the organization (CorporateOrganization, BankOrganizationGroup,
      ClientBank, or GlobalOrganization) that owns or owned the reference data
      that this AuditLog record corresponds to. */
      attributeMgr.registerAttribute("owner_org_oid", "owner_org_oid", "NumberAttribute");
      
      /* opt_lock - Optimistic lock attribute
      See jPylon documentation for details on how this works */
      attributeMgr.registerAttribute("opt_lock", "opt_lock", "OptimisticLockAttribute");
   
      /* date_time_stamp - The date and time (stored in the database in the GMT time zone) at which
      this record was inserted into the database. */
      attributeMgr.registerAttribute("date_time_stamp", "date_time_stamp", "DateTimeAttribute");
   
      /* change_user_oid - The OID of the user that performed the actioin that resulted in this AuditLog
      record being created.  Since users are never deleted from the database (they
      are deactivated), this can be used to get the name of the user */
      attributeMgr.registerAttribute("change_user_oid", "change_user_oid", "NumberAttribute");

      /* changed_object_oid - The OID of the object that this AuditLog record corresponds to.   The OID
      may not actually correspond to an OID that exists in the database if the
      reference data object has been deleted. */
      attributeMgr.registerAttribute("changed_object_oid", "changed_object_oid", "NumberAttribute");
      
      attributeMgr.registerAttribute("bus_id", "bus_id");
      
      attributeMgr.registerAttribute("name", "name");
      
      attributeMgr.registerAttribute("changed_object_data", "changed_object_data");
      attributeMgr.registerAttribute("ownership_level", "ownership_level");
      
      //jgadela 10/10/2013 - Rel 8.3 IR T36000021772  -[START] Added to identify to which user type has created the pending data. this is used while approving the pending record by particular user group
      attributeMgr.registerAttribute("changed_user_security_type", "changed_user_security_type");
      
//      attributeMgr.registerAttribute("unique_field_1", "unique_field_1");
//      attributeMgr.registerAttribute("unique_field_2", "unique_field_2");
//      attributeMgr.registerAttribute("unique_field_3", "unique_field_3");
//      attributeMgr.registerAttribute("unique_field_4", "unique_field_4");
//      attributeMgr.registerAttribute("unique_field_5", "unique_field_5");
//      attributeMgr.registerAttribute("unique_field_6", "unique_field_6");
//      attributeMgr.registerAttribute("unique_field_7", "unique_field_7");
//      attributeMgr.registerAttribute("unique_field_8", "unique_field_8");
//      attributeMgr.registerAttribute("unique_field_9", "unique_field_9");
//      attributeMgr.registerAttribute("unique_field_10", "unique_field_10");
      
   }


}
