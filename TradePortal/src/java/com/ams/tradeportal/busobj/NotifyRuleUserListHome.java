
  

package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;
/**
 * Allows Notification Rules to be defined differently for particular transaction
 * types and separates the decision criteria to send an email or a notification
 * message for each transaction type.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface NotifyRuleUserListHome extends EJBHome
{
   public NotifyRuleUserList create()
      throws RemoteException, CreateException, AmsException;

   public NotifyRuleUserList create(ClientServerDataBridge csdb)
      throws RemoteException, CreateException, AmsException;
}
