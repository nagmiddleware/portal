
  

package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;




/*
 * Allows Notification Rules to be defined differently for particular transaction
 * types and separates the decision criteria to send an email or a notification
 * message for each transaction type.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class NotifyRuleCriterionBean_Base extends TradePortalBusinessObjectBean
{
private static final Logger LOG = LoggerFactory.getLogger(NotifyRuleCriterionBean_Base.class);

  
  /* 
   * Register the attributes and associations of the business object
   */
   protected void registerAttributes() throws AmsException
   {  

      /* Register attributes defined in the Ancestor class */
      super.registerAttributes();

      /* criterion_oid - Unique Identifier */
      attributeMgr.registerAttribute("criterion_oid", "criterion_oid", "ObjectIDAttribute");
      
      /* setting - Determines if an email or notification message will be sent to the Corporate
         Customer. */
      attributeMgr.registerReferenceAttribute("setting", "setting", "NOTIF_RULE_SETTING");
      /*It is commented for defect #3009 by dpatra date 08/08/2012*/
       //attributeMgr.requiredAttribute("setting");
       
       attributeMgr.registerAlias("setting", getResourceManager().getText("NotifyRuleCriterionBeanAlias.setting", TradePortalConstants.TEXT_BUNDLE));
      
      /* transaction_type - Determines the transaction type that this rule relates to. */
      attributeMgr.registerReferenceAttribute("transaction_type", "transaction_type", "TRANSACTION_TYPE");
      attributeMgr.requiredAttribute("transaction_type");
      
      /* instr_notify_category - Represents the instrument category that this criterion relates to which
         allows notification rules to be categorized by transaction types and groups
         these rules based on category on separate tabs. */
      attributeMgr.registerReferenceAttribute("instr_notify_category", "instr_notify_category", "INSTRUMENT_CATEGORY");
      attributeMgr.requiredAttribute("instr_notify_category");
      
      /* criterion_type - Indicates whether this criterion relates to an email or a notification. */
      attributeMgr.registerReferenceAttribute("criterion_type", "criterion_type", "NOTIF_RULE_TYPE");
      attributeMgr.requiredAttribute("criterion_type");
      
        /* Pointer to the parent NotificationRule */
      attributeMgr.registerAttribute("notify_rule_oid", "p_notify_rule_oid", "ParentIDAttribute");
   
   }
   
 
   
 
 
   
}
