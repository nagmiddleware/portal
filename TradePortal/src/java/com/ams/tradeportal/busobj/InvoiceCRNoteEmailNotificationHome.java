package com.ams.tradeportal.busobj;

import java.rmi.RemoteException;

import javax.ejb.CreateException;
import javax.ejb.EJBHome;

import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.ClientServerDataBridge;

/**
 * Invoice CR Email Notifications for a Corporate Customer.  The time to send next email for the buyer is
 * calculated based on his input interval and stored in this table 
 *
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface InvoiceCRNoteEmailNotificationHome extends EJBHome
{
	public InvoiceCRNoteEmailNotification create()
    throws RemoteException, CreateException, AmsException;

    public InvoiceCRNoteEmailNotification create(ClientServerDataBridge csdb)
    throws RemoteException, CreateException, AmsException;
}
