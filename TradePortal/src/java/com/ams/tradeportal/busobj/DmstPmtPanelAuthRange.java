
package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;


/**
 * A DmstPmtPanelAuthRange consists of a set of panel range for beneficiary
 * of payment transaction.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface DmstPmtPanelAuthRange extends BusinessObject
{   
}
