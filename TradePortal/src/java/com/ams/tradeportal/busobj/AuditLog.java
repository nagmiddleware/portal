package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;


/**
 * An AuditLog record is created whenever a piece of reference data is inserted,
 * updated or deleted.   It is used for reporting purposes to track changes
 * to reference data.
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface AuditLog extends TradePortalBusinessObject
{   
}
