package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;

import java.rmi.*;
import java.util.*;
import java.math.*;

import javax.ejb.*;

import com.ams.tradeportal.common.*;


/**
 * Describes the mapping of an uploaded file to Invoice Definition stored
 * in the database.   The fields contained in the file are described, the file
 * format is specified, and the goods description format is provided.
 *
 *     Copyright  � 2003
 *     American Management Systems, Incorporated
 *     All rights reserved
 */

public class InvoiceOfferGroupBean extends InvoiceOfferGroupBean_Base {
private static final Logger LOG = LoggerFactory.getLogger(InvoiceOfferGroupBean.class);

	protected void preSave() throws AmsException {
		super.preSave();
		try {
			// update credit note indicator to Y if credit note is applied to any invoice in group.
			// group can be updated through unpackager or user UI action so better to validate group before save.
			updateCreditNoteInd();
		} catch (RemoteException e) {
			throw new AmsException(e);
		}
	}

	private void updateCreditNoteInd() throws RemoteException, AmsException {
		String whereClause = " a_invoice_offer_group_oid = ? and total_credit_note_amount > ? ";
		int recCount = DatabaseQueryBean.getCount("INVOICE_OID", "INVOICE", whereClause, true, 
				new Object[] {getAttribute("invoice_offer_group_oid"), 0} );
		if ( recCount > 0 ) {
			setAttribute("credit_note_ind", "Y");
		}
		else {
			setAttribute("credit_note_ind", "N");
		}
	}
	
	
}
