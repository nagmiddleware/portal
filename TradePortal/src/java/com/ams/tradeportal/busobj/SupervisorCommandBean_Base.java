
  

package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;




/*
 * As organizations are added to the Trade Portal, a corresponding entry must
 * be made using the Supervisor software and stored in the reports repository.
 * Supervisor sets permissions for each organization.   
 * 
 * When a corporate organization or bank organization group is added to the
 * portal, an entry is placed into this table containing the commands that
 * Supervisor needs to run in order to add the organization to the reports
 * repository.   A nightly job on the reports server will examine this table,
 * run the command on Supervisor, and then delete the rows from this table.
 * 
 * Supervisor users for the global organization or for the client bank organizations
 * must be added manually; they are not added through placing commands in this
 * table.
 * 
 * For more information on the use of this object, please see the documentation
 * of how the Trade Portal is integrated with reporting.

 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class SupervisorCommandBean_Base extends TradePortalBusinessObjectBean
{
private static final Logger LOG = LoggerFactory.getLogger(SupervisorCommandBean_Base.class);

  
  /* 
   * Register the attributes and associations of the business object
   */
   protected void registerAttributes() throws AmsException
   {  

      /* Register attributes defined in the Ancestor class */
      super.registerAttributes();
      
      /* supervisor_cmd_oid - Unique identifier */
      attributeMgr.registerAttribute("supervisor_cmd_oid", "supervisor_cmd_oid", "ObjectIDAttribute");
      
      /* command - The command that needs to be run using Supervisor to add a particular corporate
         organization or bank organization group to the reports repository. */
      attributeMgr.registerAttribute("command", "command");
      
      /* creation_timestamp - Timestamp (in GMT) of when this business object instance was created */
      attributeMgr.registerAttribute("creation_timestamp", "creation_timestamp", "DateTimeAttribute");
      
   }
   
 
   
 
 
   
}
