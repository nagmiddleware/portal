
  
/*
 * This file is generated from the model.  Normally it should not be modified manually.  
 * Contact the modeler first.  (In a pinch, it can be modified first and then retrofitted 
 * to the model.)
 *
 */
package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;


/*
 * The Bank/Branch with the Bank/Branch code and address information.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class BankBranchRuleBean_Base extends TradePortalBusinessObjectBean
{
private static final Logger LOG = LoggerFactory.getLogger(BankBranchRuleBean_Base.class);

  
  /* 
   * Register the attributes and associations of the business object
   */
   protected void registerAttributes() throws AmsException
   {  

      /* Register attributes defined in the Ancestor class */
      super.registerAttributes();
      
      /* bank_branch_rule_oid - Object Identifier of the Bank Branch Rule. */
      attributeMgr.registerAttribute("bank_branch_rule_oid", "bank_branch_rule_oid", "ObjectIDAttribute");
      
      /* bank_branch_code - Bank/Branch Code */
      attributeMgr.registerAttribute("bank_branch_code", "bank_branch_code");
      attributeMgr.requiredAttribute("bank_branch_code");
      attributeMgr.registerAlias("bank_branch_code", getResourceManager().getText("BankBranchRuleBeanAlias.bank_branch_code", TradePortalConstants.TEXT_BUNDLE));
      
      /* bank_name - Bank Name */
      attributeMgr.registerAttribute("bank_name", "bank_name");
      attributeMgr.requiredAttribute("bank_name");
      attributeMgr.registerAlias("bank_name", getResourceManager().getText("BankBranchRuleBeanAlias.bank_name", TradePortalConstants.TEXT_BUNDLE));
      
      /* branch_name - Branch Name */
      attributeMgr.registerAttribute("branch_name", "branch_name");
      attributeMgr.registerAlias("branch_name", getResourceManager().getText("BankBranchRuleBeanAlias.branch_name", TradePortalConstants.TEXT_BUNDLE));
      
      /* address_line_1 - Address Line 1 */
      attributeMgr.registerAttribute("address_line_1", "address_line_1");
      
      /* address_line_2 - Address Line 2 */
      attributeMgr.registerAttribute("address_line_2", "address_line_2");
      
      /* opt_lock - Optimistic lock attribute.  We need this to take advantage the locking in
         the framework of RefDataMediator.  But we are not inheriting from ReferenceData
         since we do not need the other attributes.
         See jPylon documentation for details on how this works */
      attributeMgr.registerAttribute("opt_lock", "opt_lock", "OptimisticLockAttribute");
      
      /* payment_method - The Payment Method that is valid for the Bank/Branch and Country combination. */
      attributeMgr.registerReferenceAttribute("payment_method", "payment_method", "PAYMENT_METHOD");
      attributeMgr.requiredAttribute("payment_method");
      attributeMgr.registerAlias("payment_method", getResourceManager().getText("BankBranchRuleBeanAlias.payment_method", TradePortalConstants.TEXT_BUNDLE));
      
      /* address_city - City */
      attributeMgr.registerAttribute("address_city", "address_city");
      attributeMgr.registerAlias("address_city", getResourceManager().getText("BankBranchRuleBeanAlias.address_city", TradePortalConstants.TEXT_BUNDLE));
      
      /* address_state_province - State or province */
      attributeMgr.registerAttribute("address_state_province", "address_state_province");
      
      /* address_country - Country */
      attributeMgr.registerReferenceAttribute("address_country", "address_country", "COUNTRY");
      attributeMgr.registerAlias("address_country", getResourceManager().getText("BankBranchRuleBeanAlias.address_country", TradePortalConstants.TEXT_BUNDLE));
      
      /* unicode_indicator - Whether the Bank Branch Rule attributes have unicode characters that are
         not compatible with WIN-1252 (WE8MSWIN1252) encoding.  Such Bank Branches
         can only be used for Beneficiary Bank, not for First Intermediary Bank or
         Second Intermediary Bank. */
      attributeMgr.registerAttribute("unicode_indicator", "unicode_indicator", "IndicatorAttribute");
      
   }
   
 
   
 
 
   
}
