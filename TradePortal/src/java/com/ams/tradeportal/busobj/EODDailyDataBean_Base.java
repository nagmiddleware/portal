
  

package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;




/*
 * An Incoming End of Day Transaction Request message will be sent to the portal
 * by the bank.  There will be one of these records per account each day.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class EODDailyDataBean_Base extends TradePortalBusinessObjectBean
{
private static final Logger LOG = LoggerFactory.getLogger(EODDailyDataBean_Base.class);

  
  /* 
   * Register the attributes and associations of the business object
   */
   protected void registerAttributes() throws AmsException
   {  

      /* Register attributes defined in the Ancestor class */
      super.registerAttributes();
      
      /* eod_daily_data_oid - Unique Identifier */
      attributeMgr.registerAttribute("eod_daily_data_oid", "eod_daily_data_oid", "ObjectIDAttribute");
      
      /* balance_date - The date for which balances are being reported in YYYYMMDD format. */
      attributeMgr.registerAttribute("balance_date", "balance_date", "DateTimeAttribute");
      attributeMgr.requiredAttribute("balance_date");
      
      /* source - Indicates the source/type of account. */
      attributeMgr.registerAttribute("source", "source");
      attributeMgr.requiredAttribute("source");
      
      /* bank_routing_id - The bank routing id for the account */
      attributeMgr.registerAttribute("bank_routing_id", "bank_routing_id");
      
      /* statement_number - First 4 bytes of the statement number. */
      attributeMgr.registerAttribute("statement_number", "statement_number", "NumberAttribute");
      attributeMgr.requiredAttribute("statement_number");
      
      /* opening_balance - Opening Balance (signed amount). There will be an explicit decimal point.
         First character is always either a "+", which represents a Credit or a "-"
         which represents a Debit. Right justified. Zero filled */
      attributeMgr.registerAttribute("opening_balance", "opening_balance", "TradePortalSignedDecimalAttribute", "com.ams.tradeportal.common");
      attributeMgr.requiredAttribute("opening_balance");
      
      /* closing_balance - Closing Balance (signed amount). There will be an explicit decimal point.
         First character is always either a "+", which represents a Credit or a "-"
         which represents a Debit. Right justified. Zero filled */
      attributeMgr.registerAttribute("closing_balance", "closing_balance", "TradePortalSignedDecimalAttribute", "com.ams.tradeportal.common");
      attributeMgr.requiredAttribute("closing_balance");
      
      /* total_debit_movement - Total debit movement amount (unsigned). Right justified and zero filled.
         Explicit decimal. */
      attributeMgr.registerAttribute("total_debit_movement", "total_debit_movement", "TradePortalSignedDecimalAttribute", "com.ams.tradeportal.common");
      attributeMgr.requiredAttribute("total_debit_movement");
      
      /* number_of_debits - Total number of debits */
      attributeMgr.registerAttribute("number_of_debits", "number_of_debits", "NumberAttribute");
      attributeMgr.requiredAttribute("number_of_debits");
      
      /* total_credit_movement - Total credit movement amount (unsigned). Right justified and zero filled.
         Explicit decimal. */
      attributeMgr.registerAttribute("total_credit_movement", "total_credit_movement", "TradePortalSignedDecimalAttribute", "com.ams.tradeportal.common");
      attributeMgr.requiredAttribute("total_credit_movement");
      
      /* number_of_credits - Total number of  credits */
      attributeMgr.registerAttribute("number_of_credits", "number_of_credits", "NumberAttribute");
      attributeMgr.requiredAttribute("number_of_credits");
      
      /* debit_interest_rate - Interest rate applied if the account is in debit, otherwise zero. Assumed
         3 decimal places */
      attributeMgr.registerAttribute("debit_interest_rate", "debit_interest_rate", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      
      /* credit_interest_rate - Interest rate applied if the account is in credit, otherwise zero. Assumed
         3 decimal places */
      attributeMgr.registerAttribute("credit_interest_rate", "credit_interest_rate", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      
      /* overdraft_limit - Overdraft limit. Unsigned amount. */
      attributeMgr.registerAttribute("overdraft_limit", "overdraft_limit", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      
      /* debit_interest_accrued - Debit Interest Accrued (unsigned amount) Right justified and zero filled. */
      attributeMgr.registerAttribute("debit_interest_accrued", "debit_interest_accrued", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      
      /* credit_interest_accrued - Credit Interest Accrued (unsigned amount) Right justified and zero filled. */
      attributeMgr.registerAttribute("credit_interest_accrued", "credit_interest_accrued", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      
      /* fid_accrued - Financial Institutions Duty Accrued (unsigned amount) Right justified and
         zero filled. */
      attributeMgr.registerAttribute("fid_accrued", "fid_accrued", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      
      /* badt_accrued - Business Account Deposit Tax, (unsigned amount) Right justified and zero
         filled. */
      attributeMgr.registerAttribute("badt_accrued", "badt_accrued", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      
      /* next_posting_date - Next posting date in YYYYMMDD format */
      attributeMgr.registerAttribute("next_posting_date", "next_posting_date", "DateTimeAttribute");
      attributeMgr.requiredAttribute("next_posting_date");
      
      /* date_time_received - Date and time on which EOD Data was received. */
      attributeMgr.registerAttribute("date_time_received", "date_time_received", "DateTimeAttribute");
      
        /* Pointer to the parent Account */
      attributeMgr.registerAttribute("account_oid", "p_account_oid", "ParentIDAttribute");
   
   }
   
  /* 
   * Register the components of the business object
   */
   protected void registerComponents() throws RemoteException, AmsException
   {  
      /* Register the components defined in the Ancestor class */
      super.registerComponents();
      
      /* EODDailyTransactionList -  */
      registerOneToManyComponent("EODDailyTransactionList","EODDailyTransactionList");
   }

 
   
 
 
   
}
