

package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;




/*
 * The alias for the AR Buyer ID in an ARMatchingRule
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class TradePartnerMarginRulesBean_Base extends TradePortalBusinessObjectBean
{
private static final Logger LOG = LoggerFactory.getLogger(TradePartnerMarginRulesBean_Base.class);

  
  /* 
   * Register the attributes and associations of the business object
   */
   protected void registerAttributes() throws AmsException
   {  
      /* ar_buyer_id_alias_oid - Unique Identifier */
      attributeMgr.registerAttribute("trade_margin_rule_oid", "trade_margin_rule_oid", "ObjectIDAttribute");
      
      /* buyer_id_alias - Buyer ID Alias */
      attributeMgr.registerAttribute("currency_code", "currency_code");      
      attributeMgr.registerAlias("currency_code", getResourceManager().getText("TradingPartnerDetails.BankDefinedRulesCurrency", TradePortalConstants.TEXT_BUNDLE));

	   attributeMgr.registerAttribute("margin_instrument_type", "margin_instrument_type");      
      attributeMgr.registerAlias("margin_instrument_type", getResourceManager().getText("TradingPartnerDetails.BankDefinedRulesInstrumentType", TradePortalConstants.TEXT_BUNDLE));

	   attributeMgr.registerAttribute("threshold_amount", "threshold_amount", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");      
       attributeMgr.registerAlias("threshold_amount", getResourceManager().getText("TradingPartnerDetails.BankDefinedRulesThreshold", TradePortalConstants.TEXT_BUNDLE));
      

	   attributeMgr.registerAttribute("rate_type", "rate_type");      
       attributeMgr.registerAlias("rate_type", getResourceManager().getText("TradingPartnerDetails.BankDefinedRulesRateType", TradePortalConstants.TEXT_BUNDLE));

	   attributeMgr.registerAttribute("margin", "margin");      
       attributeMgr.registerAlias("margin", getResourceManager().getText("TradingPartnerDetails.BankDefinedRulesMargin", TradePortalConstants.TEXT_BUNDLE));

        /* Pointer to the parent ArMatchingRule */
		 attributeMgr.registerAttribute("ar_matching_rule_oid", "p_ar_matching_rule_oid", "ParentIDAttribute");

   }
   
 
   
 
 
   
}
