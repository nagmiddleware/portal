
package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;




/*
 *
 *
 *     Copyright  © 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class CurrencyCalendarRuleBean extends CurrencyCalendarRuleBean_Base
{
private static final Logger LOG = LoggerFactory.getLogger(CurrencyCalendarRuleBean.class);

	 public String getListCriteria(String type) 
	   {
		  // Retrieve TPCalendar oid
		  if (type.equals("byTPCalendarOID")) {
			 return "a_tp_calendar_oid = {0}";
		  }
		  return "";
	   }
	 
	 protected void userValidate ()  throws AmsException, RemoteException {
	        if (!isUnique("currency", getAttribute("currency"), "")) {
	            this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
	                      TradePortalConstants.CURRENCY_CALENDAR_RULE_ALREADY_EXISTS, 
	                      getAttribute("currency"));

	        }
	    }
}
