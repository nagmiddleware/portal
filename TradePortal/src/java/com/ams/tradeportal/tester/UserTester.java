
  

package com.ams.tradeportal.tester;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import com.ams.tradeportal.busobj.*;


/*
 * Client application to test the User Business Object.  Creates a new instance of the
 * object, sets attributes on it, and attempts to save it to the database.  The time required to execute
 * the test is calculated and written to "System.Out".  Multiple iterations of the test can be executed
 * by specifying a command line argument.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class UserTester
{
private static final Logger LOG = LoggerFactory.getLogger(UserTester.class);

   public static void main (String[] argv) throws RemoteException, RemoveException, AmsException
   {
      /* Initialize the Object Server */
      LOG.info("Tester: Start of Test");
      try{
         ServerDeploy sd = null;
         LOG.info("Tester: Initializing Object Server");
         sd = (ServerDeploy)EJBObjectFactory.createClientEJB("t3://localhost:7001","ServerDeploy");  
         sd.init("t3://localhost:7001");
      }catch (Exception e) {}  
    
      /* Determine how many times to execute the test */
      int timesToExecute = new Integer(argv[0]).intValue();
      if (timesToExecute == 0) { timesToExecute = 1; }

      /* Start timing how long the test takes */
      ExecutionTimer timer = new ExecutionTimer();
      timer.setStart();

      /* Perform the test the number of times specified by the user */
      for(int i=0; !(i >= timesToExecute); i++)
         {executeTest();}
     
      /* Write out how long the test took. */
      timer.setEnd();
      LOG.info("Tester: End of Test");
      LOG.info("Tester: Execution Time per Call: " + timer.getTime() / timesToExecute);
   }


   protected static void executeTest() throws RemoteException, AmsException, RemoveException
   {
      User testObject = null;

      /* Test the basic functionaility of the User object.  This includes creating a
         new instance, setting attributes on it, and saving it to the database. */
      try{
         /* Create a new instance of the object */
         LOG.info("Tester: Creating instance of class User");
         testObject = (User)EJBObjectFactory.createClientEJB("t3://localhost:7001","User");
         LOG.info("Tester: Calling newObject() method");
         testObject.newObject();

         /* Set attrbutes on the object */
         LOG.info("Tester: Setting attribute date_deactivated to '02/28/2000 12:13:54'");
         testObject.setAttribute("date_deactivated", "02/28/2000 12:13:54");
   
         LOG.info("Tester: Setting attribute activation_status to 'O'");
         testObject.setAttribute("activation_status", "O");
   
         LOG.info("Tester: Setting attribute first_name to 'TEST'");
         testObject.setAttribute("first_name", "TEST");
   
         LOG.info("Tester: Setting attribute last_name to 'TEST'");
         testObject.setAttribute("last_name", "TEST");
   
         LOG.info("Tester: Setting attribute telephone_num to 'TEST'");
         testObject.setAttribute("telephone_num", "TEST");
   
         LOG.info("Tester: Setting attribute middle_initial to 'TEST'");
         testObject.setAttribute("middle_initial", "TEST");
   
         LOG.info("Tester: Setting attribute fax_num to 'TEST'");
         testObject.setAttribute("fax_num", "TEST");
   
         LOG.info("Tester: Setting attribute email_addr to 'TEST'");
         testObject.setAttribute("email_addr", "TEST");
   
         LOG.info("Tester: Setting attribute password_change_date to '02/28/2000 12:13:54'");
         testObject.setAttribute("password_change_date", "02/28/2000 12:13:54");
   
         LOG.info("Tester: Setting attribute newPassword to 'TEST'");
         testObject.setAttribute("newPassword", "TEST");
   
         LOG.info("Tester: Setting attribute password to 'TEST'");
         testObject.setAttribute("password", "TEST");
   
         LOG.info("Tester: Setting attribute retypePassword to 'TEST'");
         testObject.setAttribute("retypePassword", "TEST");
   
         LOG.info("Tester: Setting attribute new_user_ind to 'Y'");
         testObject.setAttribute("new_user_ind", "Y");
   
         LOG.info("Tester: Setting attribute encryption_salt to 'TEST'");
         testObject.setAttribute("encryption_salt", "TEST");
   
         LOG.info("Tester: Setting attribute locked_out_ind to 'Y'");
         testObject.setAttribute("locked_out_ind", "Y");
   
         LOG.info("Tester: Setting attribute default_wip_view to 'O'");
         testObject.setAttribute("default_wip_view", "O");
   
         LOG.info("Tester: Setting attribute customer_access_ind to 'Y'");
         testObject.setAttribute("customer_access_ind", "Y");
   
         LOG.info("Tester: Setting attribute login_id to 'TEST'");
         testObject.setAttribute("login_id", "TEST");
   
         LOG.info("Tester: Setting attribute timezone to 'O'");
         testObject.setAttribute("timezone", "O");
   
         LOG.info("Tester: Setting attribute ownership_level to 'O'");
         testObject.setAttribute("ownership_level", "O");
   
         LOG.info("Tester: Setting attribute locale to 'O'");
         testObject.setAttribute("locale", "O");
   
         LOG.info("Tester: Setting attribute opt_lock to 'TEST'");
         testObject.setAttribute("opt_lock", "TEST");
   
         LOG.info("Tester: Setting attribute user_identifier to 'TEST'");
         testObject.setAttribute("user_identifier", "TEST");
   
         LOG.info("Tester: Setting attribute invalid_login_attempt_count to '7'");
         testObject.setAttribute("invalid_login_attempt_count", "7");
   
         LOG.info("Tester: Setting attribute user_multi_part_trans_view to 'Y'");
         testObject.setAttribute("user_multi_part_trans_view", "Y");
   
         LOG.info("Tester: Setting attribute certificate_id to 'TEST'");
         testObject.setAttribute("certificate_id", "TEST");
   
         LOG.info("Tester: Setting attribute display_extended_listviews to 'Y'");
         testObject.setAttribute("display_extended_listviews", "Y");
   
         LOG.info("Tester: Setting attribute authentication_method to 'O'");
         testObject.setAttribute("authentication_method", "O");
   
         LOG.info("Tester: Setting attribute doc_prep_ind to 'Y'");
         testObject.setAttribute("doc_prep_ind", "Y");
   
         LOG.info("Tester: Setting attribute doc_prep_username to 'TEST'");
         testObject.setAttribute("doc_prep_username", "TEST");
   
         LOG.info("Tester: Setting attribute use_data_while_in_sub_access to 'Y'");
         testObject.setAttribute("use_data_while_in_sub_access", "Y");
   
         LOG.info("Tester: Setting attribute sso_id to 'TEST'");
         testObject.setAttribute("sso_id", "TEST");
   
         LOG.info("Tester: Setting attribute reporting_type to 'TEST'");
         testObject.setAttribute("reporting_type", "TEST");
   
         LOG.info("Tester: Setting attribute authorize_own_input_ind to 'Y'");
         testObject.setAttribute("authorize_own_input_ind", "Y");
   
         LOG.info("Tester: Setting attribute panel_authority_code to 'O'");
         testObject.setAttribute("panel_authority_code", "O");
   
         //BSL 09/07/11 CR663 Rel 7.1 Begin
         LOG.info("Tester: Setting attribute registered_sso_id to 'TEST'");
         testObject.setAttribute("registered_sso_id", "TEST");
   
         LOG.info("Tester: Setting attribute registration_login_id to 'TEST'");
         testObject.setAttribute("registration_login_id", "TEST");
   
         LOG.info("Tester: Setting attribute registration_password to 'TEST'");
         testObject.setAttribute("registration_password", "TEST");
         //BSL 09/07/11 CR663 Rel 7.1 End
   
         LOG.info("Tester: Setting association client_bank_oid to ''");
         testObject.setAttribute("client_bank_oid", "");
   
         LOG.info("Tester: Setting association security_profile_oid to ''");
         testObject.setAttribute("security_profile_oid", "");
   
         LOG.info("Tester: Setting association cust_access_sec_profile_oid to ''");
         testObject.setAttribute("cust_access_sec_profile_oid", "");
   
         LOG.info("Tester: Setting association threshold_group_oid to ''");
         testObject.setAttribute("threshold_group_oid", "");
   
         LOG.info("Tester: Setting association work_group_oid to ''");
         testObject.setAttribute("work_group_oid", "");
   

         /* Save the object to the database */
         LOG.info("Tester: Calling save() method");
         testObject.save();
         LOG.info("Tester: Save complete"); 
  
      /* If an Exception occurs, print out the call stack so we can determine where it came from */
      }catch(Exception e) {
         e.printStackTrace();

      /* Print out any errors that were issued by the Business Object */
      }finally {
         List<IssuedError> errors = testObject.getIssuedErrors();
         for(IssuedError errorObject : errors) 
         {
            
            LOG.info(errorObject.getErrorText());
         }
         testObject.remove();
      }
   }
}
