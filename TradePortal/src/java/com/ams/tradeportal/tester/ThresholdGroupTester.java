


package com.ams.tradeportal.tester;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import com.ams.tradeportal.busobj.*;


/*
 * Client application to test the ThresholdGroup Business Object.  Creates a new instance of the
 * object, sets attributes on it, and attempts to save it to the database.  The time required to execute
 * the test is calculated and written to "System.Out".  Multiple iterations of the test can be executed
 * by specifying a command line argument.
 *
 *     Copyright  � 2003
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class ThresholdGroupTester
{
private static final Logger LOG = LoggerFactory.getLogger(ThresholdGroupTester.class);

   public static void main (String[] argv) throws RemoteException, RemoveException, AmsException
   {
      /* Initialize the Object Server */
      LOG.info("Tester: Start of Test");
      try{
         ServerDeploy sd = null;
         LOG.info("Tester: Initializing Object Server");
         sd = (ServerDeploy)EJBObjectFactory.createClientEJB("t3://localhost:7001","ServerDeploy");
         sd.init("t3://localhost:7001");
      }catch (Exception e) {}

      /* Determine how many times to execute the test */
      int timesToExecute = new Integer(argv[0]).intValue();
      if (timesToExecute == 0) { timesToExecute = 1; }

      /* Start timing how long the test takes */
      ExecutionTimer timer = new ExecutionTimer();
      timer.setStart();

      /* Perform the test the number of times specified by the user */
      for(int i=0; !(i >= timesToExecute); i++)
         {executeTest();}

      /* Write out how long the test took. */
      timer.setEnd();
      LOG.info("Tester: End of Test");
      LOG.info("Tester: Execution Time per Call: " + timer.getTime() / timesToExecute);
   }


   protected static void executeTest() throws RemoteException, AmsException, RemoveException
   {
      ThresholdGroup testObject = null;

      /* Test the basic functionaility of the ThresholdGroup object.  This includes creating a
         new instance, setting attributes on it, and saving it to the database. */
      try{
         /* Create a new instance of the object */
         LOG.info("Tester: Creating instance of class ThresholdGroup");
         testObject = (ThresholdGroup)EJBObjectFactory.createClientEJB("t3://localhost:7001","ThresholdGroup");
         LOG.info("Tester: Calling newObject() method");
         testObject.newObject();

         /* Set attrbutes on the object */
         LOG.info("Tester: Setting attribute threshold_group_name to 'TEST'");
         testObject.setAttribute("threshold_group_name", "TEST");

         LOG.info("Tester: Setting attribute import_LC_issue_thold to '5.2'");
         testObject.setAttribute("import_LC_issue_thold", "5.2");

         LOG.info("Tester: Setting attribute import_LC_amend_thold to '5.2'");
         testObject.setAttribute("import_LC_amend_thold", "5.2");

         LOG.info("Tester: Setting attribute import_LC_discr_thold to '5.2'");
         testObject.setAttribute("import_LC_discr_thold", "5.2");

         LOG.info("Tester: Setting attribute standby_LC_issue_thold to '5.2'");
         testObject.setAttribute("standby_LC_issue_thold", "5.2");

         LOG.info("Tester: Setting attribute standby_LC_amend_thold to '5.2'");
         testObject.setAttribute("standby_LC_amend_thold", "5.2");

         LOG.info("Tester: Setting attribute standby_LC_discr_thold to '5.2'");
         testObject.setAttribute("standby_LC_discr_thold", "5.2");

         LOG.info("Tester: Setting attribute export_LC_issue_thold to '5.2'");
         testObject.setAttribute("export_LC_issue_thold", "5.2");

         LOG.info("Tester: Setting attribute export_LC_amend_transfer_thold to '5.2'");
         testObject.setAttribute("export_LC_amend_transfer_thold", "5.2");

         LOG.info("Tester: Setting attribute export_LC_issue_assign_thold to '5.2'");
         testObject.setAttribute("export_LC_issue_assign_thold", "5.2");

         LOG.info("Tester: Setting attribute export_LC_discr_thold to '5.2'");
         testObject.setAttribute("export_LC_discr_thold", "5.2");

         LOG.info("Tester: Setting attribute guarantee_issue_thold to '5.2'");
         testObject.setAttribute("guarantee_issue_thold", "5.2");

         LOG.info("Tester: Setting attribute guarantee_amend_thold to '5.2'");
         testObject.setAttribute("guarantee_amend_thold", "5.2");

         LOG.info("Tester: Setting attribute airwaybill_issue_thold to '5.2'");
         testObject.setAttribute("airwaybill_issue_thold", "5.2");

         LOG.info("Tester: Setting attribute ship_guarantee_issue_thold to '5.2'");
         testObject.setAttribute("ship_guarantee_issue_thold", "5.2");

         LOG.info("Tester: Setting attribute export_collection_issue_thold to '5.2'");
         testObject.setAttribute("export_collection_issue_thold", "5.2");

         LOG.info("Tester: Setting attribute export_collection_amend_thold to '5.2'");
         testObject.setAttribute("export_collection_amend_thold", "5.2");
         //Vasavi CR 524 03/31/2010 Begin
         LOG.info("Tester: Setting attribute new_export_coll_issue_thold to '5.2'");
         testObject.setAttribute("new_export_coll_issue_thold", "5.2");

         LOG.info("Tester: Setting attribute new_export_coll_amend_thold to '5.2'");
         testObject.setAttribute("new_export_coll_amend_thold", "5.2");
         //Vasavi CR 524 03/31/2010 End
         LOG.info("Tester: Setting attribute inc_standby_LC_discr_thold to '5.2'");
         testObject.setAttribute("inc_standby_LC_discr_thold", "5.2");

         LOG.info("Tester: Setting attribute loan_request_issue_thold to '5.2'");
         testObject.setAttribute("loan_request_issue_thold", "5.2");

         LOG.info("Tester: Setting attribute funds_transfer_issue_thold to '5.2'");
         testObject.setAttribute("funds_transfer_issue_thold", "5.2");

         LOG.info("Tester: Setting attribute import_LC_discr_dlimit to '5.2'");
         testObject.setAttribute("import_LC_discr_dlimit", "5.2");

         LOG.info("Tester: Setting attribute import_LC_issue_dlimit to '5.2'");
         testObject.setAttribute("import_LC_issue_dlimit", "5.2");

         LOG.info("Tester: Setting attribute import_LC_amend_dlimit to '5.2'");
         testObject.setAttribute("import_LC_amend_dlimit", "5.2");

         LOG.info("Tester: Setting attribute standby_LC_issue_dlimit to '5.2'");
         testObject.setAttribute("standby_LC_issue_dlimit", "5.2");

         LOG.info("Tester: Setting attribute standby_LC_amend_dlimit to '5.2'");
         testObject.setAttribute("standby_LC_amend_dlimit", "5.2");

         LOG.info("Tester: Setting attribute standby_LC_discr_dlimit to '5.2'");
         testObject.setAttribute("standby_LC_discr_dlimit", "5.2");

         LOG.info("Tester: Setting attribute export_LC_issue_dlimit to '5.2'");
         testObject.setAttribute("export_LC_issue_dlimit", "5.2");

         LOG.info("Tester: Setting attribute export_LC_amend_transfer_dlim to '5.2'");
         testObject.setAttribute("export_LC_amend_transfer_dlim", "5.2");

         LOG.info("Tester: Setting attribute export_LC_issue_assign_dlimit to '5.2'");
         testObject.setAttribute("export_LC_issue_assign_dlimit", "5.2");

         LOG.info("Tester: Setting attribute export_LC_discr_dlimit to '5.2'");
         testObject.setAttribute("export_LC_discr_dlimit", "5.2");

         LOG.info("Tester: Setting attribute guarantee_issue_dlimit to '5.2'");
         testObject.setAttribute("guarantee_issue_dlimit", "5.2");

         LOG.info("Tester: Setting attribute guarantee_amend_dlimit to '5.2'");
         testObject.setAttribute("guarantee_amend_dlimit", "5.2");

         LOG.info("Tester: Setting attribute airwaybill_issue_dlimit to '5.2'");
         testObject.setAttribute("airwaybill_issue_dlimit", "5.2");

         LOG.info("Tester: Setting attribute ship_guarantee_issue_dlimit to '5.2'");
         testObject.setAttribute("ship_guarantee_issue_dlimit", "5.2");

         LOG.info("Tester: Setting attribute export_collection_issue_dlimit to '5.2'");
         testObject.setAttribute("export_collection_issue_dlimit", "5.2");

         LOG.info("Tester: Setting attribute export_collection_amend_dlimit to '5.2'");
         testObject.setAttribute("export_collection_amend_dlimit", "5.2");
         //Vasavi CR 524 03/31/2010 Begin
         LOG.info("Tester: Setting attribute new_export_coll_issue_dlimit to '5.2'");
         testObject.setAttribute("new_export_coll_issue_dlimit", "5.2");

         LOG.info("Tester: Setting attribute new_export_coll_amend_dlimit to '5.2'");
         testObject.setAttribute("new_export_coll_amend_dlimit", "5.2");
         //Vasavi CR 524 03/31/2010 End
         LOG.info("Tester: Setting attribute inc_standby_LC_discr_dlimit to '5.2'");
         testObject.setAttribute("inc_standby_LC_discr_dlimit", "5.2");

         LOG.info("Tester: Setting attribute opt_lock to 'TEST'");
         testObject.setAttribute("opt_lock", "TEST");

         LOG.info("Tester: Setting attribute loan_request_issue_dlimit to '5.2'");
         testObject.setAttribute("loan_request_issue_dlimit", "5.2");

         LOG.info("Tester: Setting attribute funds_transfer_issue_dlimit to '5.2'");
         testObject.setAttribute("funds_transfer_issue_dlimit", "5.2");

         LOG.info("Tester: Setting attribute request_advise_issue_thold to '5.2'");
         testObject.setAttribute("request_advise_issue_thold", "5.2");

         LOG.info("Tester: Setting attribute request_advise_amend_thold to '5.2'");
         testObject.setAttribute("request_advise_amend_thold", "5.2");

         LOG.info("Tester: Setting attribute request_advise_discr_thold to '5.2'");
         testObject.setAttribute("request_advise_discr_thold", "5.2");

         LOG.info("Tester: Setting attribute request_advise_issue_dlimit to '5.2'");
         testObject.setAttribute("request_advise_issue_dlimit", "5.2");

         LOG.info("Tester: Setting attribute request_advise_amend_dlimit to '5.2'");
         testObject.setAttribute("request_advise_amend_dlimit", "5.2");

         LOG.info("Tester: Setting attribute request_advise_discr_dlimit to '5.2'");
         testObject.setAttribute("request_advise_discr_dlimit", "5.2");

         LOG.info("Tester: Setting attribute approval_to_pay_discr_dlimit to '5.2'");
         testObject.setAttribute("approval_to_pay_discr_dlimit", "5.2");

         LOG.info("Tester: Setting attribute approval_to_pay_discr_thold to '5.2'");
         testObject.setAttribute("approval_to_pay_discr_thold", "5.2");

         LOG.info("Tester: Setting attribute approval_to_pay_issue_dlimit to '5.2'");
         testObject.setAttribute("approval_to_pay_issue_dlimit", "5.2");

         LOG.info("Tester: Setting attribute approval_to_pay_issue_thold to '5.2'");
         testObject.setAttribute("approval_to_pay_issue_thold", "5.2");

         LOG.info("Tester: Setting attribute approval_to_pay_amend_dlimit to '5.2'");
         testObject.setAttribute("approval_to_pay_amend_dlimit", "5.2");

         LOG.info("Tester: Setting attribute approval_to_pay_amend_thold to '5.2'");
         testObject.setAttribute("approval_to_pay_amend_thold", "5.2");

         LOG.info("Tester: Setting attribute ar_match_response_thold to '5.2'");
         testObject.setAttribute("ar_match_response_thold", "5.2");

         LOG.info("Tester: Setting attribute ar_match_response_dlimit to '5.2'");
         testObject.setAttribute("ar_match_response_dlimit", "5.2");

         LOG.info("Tester: Setting attribute ar_approve_discount_thold to '5.2'");
         testObject.setAttribute("ar_approve_discount_thold", "5.2");

         LOG.info("Tester: Setting attribute ar_approve_discount_dlimit to '5.2'");
         testObject.setAttribute("ar_approve_discount_dlimit", "5.2");

         LOG.info("Tester: Setting attribute transfer_btw_accts_issue_thold to '5.2'");
         testObject.setAttribute("transfer_btw_accts_issue_thold", "5.2");

         LOG.info("Tester: Setting attribute transfer_btw_accts_issue_dlimit to '5.2'");
         testObject.setAttribute("transfer_btw_accts_issue_dlimit", "5.2");

         LOG.info("Tester: Setting attribute domestic_payment_issue_thold to '5.2'");
         testObject.setAttribute("domestic_payment_issue_thold", "5.2");

         LOG.info("Tester: Setting attribute domestic_payment_issue_dlimit to '5.2'");
         testObject.setAttribute("domestic_payment_issue_dlimit", "5.2");

         LOG.info("Tester: Setting attribute ar_close_invoice_thold to '5.2'");
         testObject.setAttribute("ar_close_invoice_thold", "5.2");

         LOG.info("Tester: Setting attribute ar_close_invoice_dlimit to '5.2'");
         testObject.setAttribute("ar_close_invoice_dlimit", "5.2");

         LOG.info("Tester: Setting attribute ar_finance_invoice_thold to '5.2'");
         testObject.setAttribute("ar_finance_invoice_thold", "5.2");

         LOG.info("Tester: Setting attribute ar_finance_invoice_dlimit to '5.2'");
         testObject.setAttribute("ar_finance_invoice_dlimit", "5.2");

         LOG.info("Tester: Setting attribute ar_dispute_invoice_thold to '5.2'");
         testObject.setAttribute("ar_dispute_invoice_thold", "5.2");

         LOG.info("Tester: Setting attribute ar_dispute_invoice_dlimit to '5.2'");
         testObject.setAttribute("ar_dispute_invoice_dlimit", "5.2");

         //Pratiksha CR-509 DDI 12/09/2009 Begin
    	 LOG.info("Tester: Setting attribute direct_debit_thold to '5.2'");
             testObject.setAttribute("direct_debit_thold", "5.2");

    	 LOG.info("Tester: Setting attribute direct_debit_dlimit to '5.2'");
             testObject.setAttribute("direct_debit_dlimit", "5.2");
    	//Pratiksha CR-509 DDI 12/09/2009 End

         /* Save the object to the database */
         LOG.info("Tester: Calling save() method");
         testObject.save();
         LOG.info("Tester: Save complete");

      /* If an Exception occurs, print out the call stack so we can determine where it came from */
      }catch(Exception e) {
         e.printStackTrace();

      /* Print out any errors that were issued by the Business Object */
      }finally {
         List<IssuedError> errors = testObject.getIssuedErrors();
         for(IssuedError errorObject : errors) 
         {
            
            LOG.info(errorObject.getErrorText());
         }
         testObject.remove();
      }
   }
}
