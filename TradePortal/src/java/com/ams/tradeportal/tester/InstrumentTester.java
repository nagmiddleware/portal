
  

package com.ams.tradeportal.tester;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import com.ams.tradeportal.busobj.*;


/*
 * Client application to test the Instrument Business Object.  Creates a new instance of the
 * object, sets attributes on it, and attempts to save it to the database.  The time required to execute
 * the test is calculated and written to "System.Out".  Multiple iterations of the test can be executed
 * by specifying a command line argument.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class InstrumentTester
{
private static final Logger LOG = LoggerFactory.getLogger(InstrumentTester.class);

   public static void main (String[] argv) throws RemoteException, RemoveException, AmsException
   {
      /* Initialize the Object Server */
      LOG.info("Tester: Start of Test");
      try{
         ServerDeploy sd = null;
         LOG.info("Tester: Initializing Object Server");
         sd = (ServerDeploy)EJBObjectFactory.createClientEJB("t3://localhost:7001","ServerDeploy");  
         sd.init("t3://localhost:7001");
      }catch (Exception e) {}  
    
      /* Determine how many times to execute the test */
      int timesToExecute = new Integer(argv[0]).intValue();
      if (timesToExecute == 0) { timesToExecute = 1; }

      /* Start timing how long the test takes */
      ExecutionTimer timer = new ExecutionTimer();
      timer.setStart();

      /* Perform the test the number of times specified by the user */
      for(int i=0; !(i >= timesToExecute); i++)
         {executeTest();}
     
      /* Write out how long the test took. */
      timer.setEnd();
      LOG.info("Tester: End of Test");
      LOG.info("Tester: Execution Time per Call: " + timer.getTime() / timesToExecute);
   }


   protected static void executeTest() throws RemoteException, AmsException, RemoveException
   {
      Instrument testObject = null;

      /* Test the basic functionaility of the Instrument object.  This includes creating a
         new instance, setting attributes on it, and saving it to the database. */
      try{
         /* Create a new instance of the object */
         LOG.info("Tester: Creating instance of class Instrument");
         testObject = (Instrument)EJBObjectFactory.createClientEJB("t3://localhost:7001","Instrument");
         LOG.info("Tester: Calling newObject() method");
         testObject.newObject();

         /* Set attrbutes on the object */
         LOG.info("Tester: Setting attribute instrument_type_code to 'O'");
         testObject.setAttribute("instrument_type_code", "O");
   
         LOG.info("Tester: Setting attribute template_flag to 'Y'");
         testObject.setAttribute("template_flag", "Y");
   
         LOG.info("Tester: Setting attribute a_counter_party_oid to '7'");
         testObject.setAttribute("a_counter_party_oid", "7");
   
         LOG.info("Tester: Setting attribute original_transaction_oid to '7'");
         testObject.setAttribute("original_transaction_oid", "7");
   
         LOG.info("Tester: Setting attribute language to 'O'");
         testObject.setAttribute("language", "O");
   
         LOG.info("Tester: Setting attribute import_indicator to 'Y'");
         testObject.setAttribute("import_indicator", "Y");
   
         LOG.info("Tester: Setting attribute instrument_num to 'TEST'");
         testObject.setAttribute("instrument_num", "TEST");
   
         LOG.info("Tester: Setting attribute instrument_prefix to 'TEST'");
         testObject.setAttribute("instrument_prefix", "TEST");
   
         LOG.info("Tester: Setting attribute instrument_suffix to 'TEST'");
         testObject.setAttribute("instrument_suffix", "TEST");
   
         LOG.info("Tester: Setting attribute complete_instrument_id to 'TEST'");
         testObject.setAttribute("complete_instrument_id", "TEST");
   
         LOG.info("Tester: Setting attribute instrument_status to 'O'");
         testObject.setAttribute("instrument_status", "O");
   
         LOG.info("Tester: Setting attribute copy_of_ref_num to 'TEST'");
         testObject.setAttribute("copy_of_ref_num", "TEST");
   
         LOG.info("Tester: Setting attribute from_express_template to 'Y'");
         testObject.setAttribute("from_express_template", "Y");
   
         LOG.info("Tester: Setting attribute active_transaction_oid to '7'");
         testObject.setAttribute("active_transaction_oid", "7");
   
         LOG.info("Tester: Setting attribute issue_date to '02/28/2000 12:13:54'");
         testObject.setAttribute("issue_date", "02/28/2000 12:13:54");
   
         LOG.info("Tester: Setting attribute copy_of_expiry_date to '02/28/2000 12:13:54'");
         testObject.setAttribute("copy_of_expiry_date", "02/28/2000 12:13:54");
   
         LOG.info("Tester: Setting attribute copy_of_instrument_amount to '5.2'");
         testObject.setAttribute("copy_of_instrument_amount", "5.2");
   
         LOG.info("Tester: Setting attribute opening_bank_ref_num to 'TEST'");
         testObject.setAttribute("opening_bank_ref_num", "TEST");
   
         LOG.info("Tester: Setting attribute purge_date to '02/28/2000 12:13:54'");
         testObject.setAttribute("purge_date", "02/28/2000 12:13:54");
   
         LOG.info("Tester: Setting attribute copy_of_confirmation_ind to 'Y'");
         testObject.setAttribute("copy_of_confirmation_ind", "Y");
   
         LOG.info("Tester: Setting attribute present_docs_within_days to '7'");
         testObject.setAttribute("present_docs_within_days", "7");
   
         LOG.info("Tester: Setting attribute vendor_id to 'TEST'");
         testObject.setAttribute("vendor_id", "TEST");
   
         LOG.info("Tester: Setting attribute otl_po_invoice_portfolio_uoid to 'TEST'");
         testObject.setAttribute("otl_po_invoice_portfolio_uoid", "TEST");
   
         LOG.info("Tester: Setting attribute otl_instrument_uoid to 'TEST'");
         testObject.setAttribute("otl_instrument_uoid", "TEST");
   
         LOG.info("Tester: Setting association client_bank_oid to ''");
         testObject.setAttribute("client_bank_oid", "");
   
         LOG.info("Tester: Setting association corp_org_oid to ''");
         testObject.setAttribute("corp_org_oid", "");
   
         LOG.info("Tester: Setting association op_bank_org_oid to ''");
         testObject.setAttribute("op_bank_org_oid", "");
   
         LOG.info("Tester: Setting association related_instrument_oid to ''");
         testObject.setAttribute("related_instrument_oid", "");
         
         
         LOG.info("Tester: Setting attribute fixed_payment_flag to 'Y'");
         testObject.setAttribute("fixed_payment_flag", "Y");
            

         /* Save the object to the database */
         LOG.info("Tester: Calling save() method");
         testObject.save();
         LOG.info("Tester: Save complete"); 
  
      /* If an Exception occurs, print out the call stack so we can determine where it came from */
      }catch(Exception e) {
         e.printStackTrace();

      /* Print out any errors that were issued by the Business Object */
      }finally {
         List<IssuedError> errors = testObject.getIssuedErrors();
         for(IssuedError errorObject : errors) 
         {
            
            LOG.info(errorObject.getErrorText());
         }
         testObject.remove();
      }
   }
}
