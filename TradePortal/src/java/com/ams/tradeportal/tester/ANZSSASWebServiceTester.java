/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ams.tradeportal.tester;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.thales_esecurity.ssas.*;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.util.Date;
import javax.net.ssl.*;

/**
 *
 * @author weian.zhu
 */
public class ANZSSASWebServiceTester {
private static final Logger LOG = LoggerFactory.getLogger(ANZSSASWebServiceTester.class);
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception  {
        LOG.info("usage 1: java -cp ANZSSASWebServiceTest.jar com.ams.tradeportal.tester.ANZSSASWebServiceTester [type=GEMALTO] service_url=... [user_key=...] [content_data=...] [signature=...] [transaction_id=...] [ignore_cert=Y/N]");
        LOG.info("usage 2: java -cp ANZSSASWebServiceTest.jar com.ams.tradeportal.tester.ANZSSASWebServiceTester type=VASCO_Signature service_url=... [token_application_id=...] [signature_field=...]  [signature=...] [transaction_id=...] [ignore_cert=Y/N]");
        if (args.length < 1) {
            return;
        }

        ANZSSASWebServiceTester test = new ANZSSASWebServiceTester();
        for (int i = 0; i < args.length; i++) {
            String[] parms = args[i].split("=");
            if ("type".equals(parms[0])) {
                test.type = parms[1];
            }
            else if ("user_key".equals(parms[0])) {
                test.userKey = parms[1];
            }
            else if ("signature".contains(parms[0])){
                test.signature = parms[1];
            }
            else if ("content_data".contains(parms[0])){
                test.contentData = parms[1];
            }
            else if ("service_url".equals(parms[0])){
                test.serviceURL = parms[1];
            }
            else if ("transaction_id".equals(parms[0])){
                test.transID = parms[1];
            }        
            else if ("ignore_cert".equals(parms[0])) {
                test.ignoreCert = parms[1];
            }        
            else if ("token_application_id".equals(parms[0])) {
                test.tokenApplicationID = parms[1];
            }     
            else if ("signature_field".equals(parms[0])) {
                test.signatureFields[0] = parms[1];
            }     
        }
        
        if ("Y".equals(test.ignoreCert)) {
            TrustManager[] trustAllCerts = new TrustManager[] { 
              new X509TrustManager() {
                public X509Certificate[] getAcceptedIssuers() { 
                  return new X509Certificate[0]; 
                }
                public void checkClientTrusted(X509Certificate[] certs, String authType) {}
                public void checkServerTrusted(X509Certificate[] certs, String authType) {}
            }};

            // Ignore differences between given hostname and certificate hostname
            HostnameVerifier hv = new HostnameVerifier() {
              public boolean verify(String hostname, SSLSession session) { return true; }
            };

            // Install the all-trusting trust manager
            try {
              SSLContext sc = SSLContext.getInstance("SSL");
              sc.init(null, trustAllCerts, new SecureRandom());
              HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
              HttpsURLConnection.setDefaultHostnameVerifier(hv);
            } catch (Exception e) {
            LOG.info (e.getMessage());
            }
    
        }
        
        if ("GEMALTO".equals(test.type)) {
            test.testANZSSASWebServiceGemalto();
        }
        else if ("VASCO_Signature".equals(test.type)){
            test.testANZSSASVascoSignature();
        }
        else {
            LOG.info("Wrong type " + test.type);
        }
    }    
    
    private String serviceURL ;
    private String userKey = "ssoid";
    private String contentData = "content";
    private String signature = "123456";
    private String ignoreCert = "N";
    private String type = "GEMALTO";
    private String transID = "000001";
    private String tokenApplicationID = "vascoidSIGONLH";
    private String[] signatureFields = {"123456"};
    private void testANZSSASWebServiceGemalto() throws Exception {
        
        Date currentDate = new Date();
        LOG.info("Current Time = " + currentDate);
                  LOG.info("SSAS Soap Endpoint Address="+serviceURL);
                  LOG.info("transID="+transID);
                  LOG.info("ExtraData.UserKey="+userKey);
                  LOG.info("signature="+signature);
                  LOG.info("ContenData="+this.contentData);

                  // Create an instance of the SOAP service.
 		   SSASSoapServiceLocator soapService = new SSASSoapServiceLocator();
 		   // Set the URL explicitly based on configuration
 		   soapService.setSSASSoapEndpointAddress(serviceURL); 

 		   BasicResponse basicResponse = null;
 		   SSASSoap soapCtx = soapService.getSSASSoap();
                   
                   String [] extraDataKeys = {"UserKey"};
                   String [] extraDataValues = {userKey};
                  ExtraData extraData = new ExtraData(extraDataKeys, extraDataValues);
                  ContentData contentData = new ContentData(this.contentData.getBytes(), false);

                  CSCResponse response = soapCtx.doCSCPKCS7(transID, extraData, signature.getBytes(), contentData);

 		   //
 		   
 		   // Check the response.
 		   basicResponse = response.getBasicResponse();
 		   // Check the status.
 		   String status = basicResponse.getStatus();
                   LOG.info("stauts = " + status); 			   
 		   if (status.equalsIgnoreCase("ERROR")) {
 			   SSASError errors[] = basicResponse.getErrors();
                           if (errors.length > 0) {
                               for (int i = 0; i < errors.length; i++) {
                                   String errorCode = getGemaltoErrorCode(errors[i].getDescription());
 				   LOG.info("AdditionalInfo: " + errors[i].getAdditionalInfo());
                                   LOG.info("Description: "+errors[i].getDescription());
                                   LOG.info("Error Name: "+ errors[i].getName());
                                   LOG.info("Translated Error Msg Key" + errorCode);
                               }
                           }
                           else {
                               LOG.info("errors.length = " + errors.length);
                           }
 		   }
        return;
    }
    
    private void testANZSSASVascoSignature() throws Exception {
        Date currentDate = new Date();
        LOG.info("Current Time = " + currentDate);
                  LOG.info("SSAS Soap Endpoint Address="+serviceURL);
                  LOG.info("transID="+transID);
                  LOG.info("tokenApplicationID="+tokenApplicationID);
                  LOG.info("signature="+signature);
                  LOG.info("ContenData="+this.contentData);

         		   // Create an instance of the SOAP service.
 		   SSASSoapServiceLocator soapService = new SSASSoapServiceLocator();
 		   // Set the URL explicitly based on configuration
 		   soapService.setSSASSoapEndpointAddress(serviceURL); 

 		   BasicResponse basicResponse = null;
 		   DigiPassResponse response = null;
 		   SSASSoap soapCtx = soapService.getSSASSoap();
 		   //
 		   //Check the type of SIG being performed, if otp call the doDigiPassVerifyPassword method
 		   //otherwise call doDigiPassVerifySignature 
 		   //
                    response = soapCtx.doDigiPassVerifySignature(transID, null, tokenApplicationID, signatureFields, signature);
 		   basicResponse = response.getBasicResponse();
 		   // Check the status.
 		   String status = basicResponse.getStatus();
                   LOG.info("stauts = " + status); 			   
 		   if (status.equalsIgnoreCase("ERROR")) {
 			   SSASError errors[] = basicResponse.getErrors();
                           if (errors.length > 0) {
                               for (int i = 0; i < errors.length; i++) {
                                   String errorCode = getGemaltoErrorCode(errors[i].getDescription());
 				   LOG.info("AdditionalInfo: " + errors[i].getAdditionalInfo());
                                   LOG.info("Description: "+errors[i].getDescription());
                                   LOG.info("Error Name: "+ errors[i].getName());
                                   LOG.info("Translated Error Msg Key" + errorCode);
                               }
                           }
                           else {
                               LOG.info("errors.length = " + errors.length);
                           }
 		   }
                           

    }
    
static final private String SSAS_DESC_SIG_MISSING    = "Missing signature to check";
  static final private String SSAS_DESC_REVCHK_ERROR   = "Revocation check error";
  static final private String SSAS_DESC_CERT_EXPIRED   = "Certificate has expired";
  static final private String SSAS_DESC_CERT_NOPATH    = "No path found for the certificate within the current PKI";
  static final private String SSAS_DESC_FAILED_VERIFY  = "Signature verification failed";
  static final private String SSAS_DESC_INVALID_FORMAT = "Invalid format of the signature message";
  static final private String SSAS_DESC_NO_DIR_SERVER  = "Directory server not found";
  static final private String SSAS_DESC_DIR_BIND_FAIL  = "Unable to bind to the directory";
  static final private String SSAS_DESC_DIR_NO_USER    = "User not found in directory";
  static final private String SSAS_DESC_DIR_NO_CERT    = "Signing certificate not found in directory";
  static final private String SSAS_DESC_CERT_MISMATCH  = "Signing certificate not matched directory certificate";
private String getGemaltoErrorCode (String ssasMessage) {
    String msg = "GEMALTO_ERROR.MESG_STATUS_DEFAULT";

    if (ssasMessage != null)
    {
      if (ssasMessage.contains(SSAS_DESC_SIG_MISSING))
        msg = "GEMALTO_ERROR.MESG_DESC_SIG_MISSING";
      else if (ssasMessage.contains(SSAS_DESC_REVCHK_ERROR))
        msg = "GEMALTO_ERROR.MESG_DESC_REVCHK_ERROR";
      else if (ssasMessage.contains(SSAS_DESC_CERT_EXPIRED))
        msg = "GEMALTO_ERROR.MESG_DESC_CERT_EXPIRED";
      else if (ssasMessage.contains(SSAS_DESC_CERT_NOPATH))
        msg = "GEMALTO_ERROR.MESG_DESC_CERT_NOPATH";
      else if (ssasMessage.contains(SSAS_DESC_FAILED_VERIFY))
        msg = "GEMALTO_ERROR.MESG_DESC_FAILED_VERIFY";  
      else if (ssasMessage.contains(SSAS_DESC_INVALID_FORMAT))
        msg = "GEMALTO_ERROR.MESG_DESC_INVALID_FORMAT";
      else if (ssasMessage.contains(SSAS_DESC_NO_DIR_SERVER))
        msg = "GEMALTO_ERROR.MESG_STATUS_DEFAULT";  
      else if (ssasMessage.contains(SSAS_DESC_DIR_BIND_FAIL))
        msg = "GEMALTO_ERROR.MESG_STATUS_DEFAULT";
      else if (ssasMessage.contains(SSAS_DESC_DIR_NO_USER))
        msg = "GEMALTO_ERROR.MESG_DESC_DIR_NO_USER";  
      else if (ssasMessage.contains(SSAS_DESC_DIR_NO_CERT))
        msg = "GEMALTO_ERROR.MESG_DESC_DIR_NO_CERT";
      else if (ssasMessage.contains(SSAS_DESC_CERT_MISMATCH))
        msg = "GEMALTO_ERROR.MESG_DESC_CERT_MISMATCH";  
    }
    
    return msg;
  }      
}
