
  

package com.ams.tradeportal.tester;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import com.ams.tradeportal.busobj.*;


/*
 * Client application to test the Party Business Object.  Creates a new instance of the
 * object, sets attributes on it, and attempts to save it to the database.  The time required to execute
 * the test is calculated and written to "System.Out".  Multiple iterations of the test can be executed
 * by specifying a command line argument.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class PartyTester
{
private static final Logger LOG = LoggerFactory.getLogger(PartyTester.class);

   public static void main (String[] argv) throws RemoteException, RemoveException, AmsException
   {
      /* Initialize the Object Server */
      LOG.info("Tester: Start of Test");
      try{
         ServerDeploy sd = null;
         LOG.info("Tester: Initializing Object Server");
         sd = (ServerDeploy)EJBObjectFactory.createClientEJB("t3://localhost:7001","ServerDeploy");  
         sd.init("t3://localhost:7001");
      }catch (Exception e) {}  
    
      /* Determine how many times to execute the test */
      int timesToExecute = new Integer(argv[0]).intValue();
      if (timesToExecute == 0) { timesToExecute = 1; }

      /* Start timing how long the test takes */
      ExecutionTimer timer = new ExecutionTimer();
      timer.setStart();

      /* Perform the test the number of times specified by the user */
      for(int i=0; !(i >= timesToExecute); i++)
         {executeTest();}
     
      /* Write out how long the test took. */
      timer.setEnd();
      LOG.info("Tester: End of Test");
      LOG.info("Tester: Execution Time per Call: " + timer.getTime() / timesToExecute);
   }


   protected static void executeTest() throws RemoteException, AmsException, RemoveException
   {
      Party testObject = null;

      /* Test the basic functionaility of the Party object.  This includes creating a
         new instance, setting attributes on it, and saving it to the database. */
      try{
         /* Create a new instance of the object */
         LOG.info("Tester: Creating instance of class Party");
         testObject = (Party)EJBObjectFactory.createClientEJB("t3://localhost:7001","Party");
         LOG.info("Tester: Calling newObject() method");
         testObject.newObject();

         /* Set attrbutes on the object */
         LOG.info("Tester: Setting attribute name to 'TEST'");
         testObject.setAttribute("name", "TEST");
   
         LOG.info("Tester: Setting attribute ownership_level to 'O'");
         testObject.setAttribute("ownership_level", "O");
   
         LOG.info("Tester: Setting attribute opt_lock to 'TEST'");
         testObject.setAttribute("opt_lock", "TEST");
   
         LOG.info("Tester: Setting attribute ownership_type to 'O'");
         testObject.setAttribute("ownership_type", "O");
   
         LOG.info("Tester: Setting attribute contact_name to 'TEST'");
         testObject.setAttribute("contact_name", "TEST");
   
         LOG.info("Tester: Setting attribute contact_title to 'TEST'");
         testObject.setAttribute("contact_title", "TEST");
   
         LOG.info("Tester: Setting attribute contact_phone to 'TEST'");
         testObject.setAttribute("contact_phone", "TEST");
   
         LOG.info("Tester: Setting attribute contact_fax to 'TEST'");
         testObject.setAttribute("contact_fax", "TEST");
   
         LOG.info("Tester: Setting attribute contact_email to 'TEST'");
         testObject.setAttribute("contact_email", "TEST");
   
         LOG.info("Tester: Setting attribute telex_1 to 'TEST'");
         testObject.setAttribute("telex_1", "TEST");
   
         LOG.info("Tester: Setting attribute telex_answer_back_1 to 'TEST'");
         testObject.setAttribute("telex_answer_back_1", "TEST");
   
         LOG.info("Tester: Setting attribute fax_1 to 'TEST'");
         testObject.setAttribute("fax_1", "TEST");
   
         LOG.info("Tester: Setting attribute fax_2 to 'TEST'");
         testObject.setAttribute("fax_2", "TEST");
   
         LOG.info("Tester: Setting attribute swift_address_part1 to 'TEST'");
         testObject.setAttribute("swift_address_part1", "TEST");
   
         LOG.info("Tester: Setting attribute swift_address_part2 to 'TEST'");
         testObject.setAttribute("swift_address_part2", "TEST");
   
         LOG.info("Tester: Setting attribute address_line_1 to 'TEST'");
         testObject.setAttribute("address_line_1", "TEST");
   
         LOG.info("Tester: Setting attribute address_line_2 to 'TEST'");
         testObject.setAttribute("address_line_2", "TEST");
   
         LOG.info("Tester: Setting attribute party_type_code to 'O'");
         testObject.setAttribute("party_type_code", "O");
   
         LOG.info("Tester: Setting attribute address_city to 'TEST'");
         testObject.setAttribute("address_city", "TEST");
   
         LOG.info("Tester: Setting attribute address_state_province to 'TEST'");
         testObject.setAttribute("address_state_province", "TEST");
   
         LOG.info("Tester: Setting attribute address_country to 'O'");
         testObject.setAttribute("address_country", "O");
   
         LOG.info("Tester: Setting attribute address_postal_code to 'TEST'");
         testObject.setAttribute("address_postal_code", "TEST");
   
         LOG.info("Tester: Setting attribute OTL_customer_id to 'TEST'");
         testObject.setAttribute("OTL_customer_id", "TEST");
   
         LOG.info("Tester: Setting attribute phone_number to 'TEST'");
         testObject.setAttribute("phone_number", "TEST");
   
         LOG.info("Tester: Setting attribute branch_code to 'TEST'");
         testObject.setAttribute("branch_code", "TEST");
   
         LOG.info("Tester: Setting attribute vendor_id to 'TEST'");
         testObject.setAttribute("vendor_id", "TEST");
         LOG.info("Tester: Setting association designated_bank_oid to ''");
         testObject.setAttribute("designated_bank_oid", "");
   

         /* Save the object to the database */
         LOG.info("Tester: Calling save() method");
         testObject.save();
         LOG.info("Tester: Save complete"); 
  
      /* If an Exception occurs, print out the call stack so we can determine where it came from */
      }catch(Exception e) {
         e.printStackTrace();

      /* Print out any errors that were issued by the Business Object */
      }finally {
         List<IssuedError> errors = testObject.getIssuedErrors();
         for(IssuedError errorObject : errors) 
         {
            
            LOG.info(errorObject.getErrorText());
         }
         testObject.remove();
      }
   }
}
