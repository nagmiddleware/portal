
  

package com.ams.tradeportal.tester;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import com.ams.tradeportal.busobj.*;


/*
 * Client application to test the Transaction Business Object.  Creates a new instance of the
 * object, sets attributes on it, and attempts to save it to the database.  The time required to execute
 * the test is calculated and written to "System.Out".  Multiple iterations of the test can be executed
 * by specifying a command line argument.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class TransactionTester
{
private static final Logger LOG = LoggerFactory.getLogger(TransactionTester.class);

   public static void main (String[] argv) throws RemoteException, RemoveException, AmsException
   {
      /* Initialize the Object Server */
      LOG.info("Tester: Start of Test");
      try{
         ServerDeploy sd = null;
         LOG.info("Tester: Initializing Object Server");
         sd = (ServerDeploy)EJBObjectFactory.createClientEJB("t3://localhost:7001","ServerDeploy");  
         sd.init("t3://localhost:7001");
      }catch (Exception e) {}  
    
      /* Determine how many times to execute the test */
      int timesToExecute = new Integer(argv[0]).intValue();
      if (timesToExecute == 0) { timesToExecute = 1; }

      /* Start timing how long the test takes */
      ExecutionTimer timer = new ExecutionTimer();
      timer.setStart();

      /* Perform the test the number of times specified by the user */
      for(int i=0; !(i >= timesToExecute); i++)
         {executeTest();}
     
      /* Write out how long the test took. */
      timer.setEnd();
      LOG.info("Tester: End of Test");
      LOG.info("Tester: Execution Time per Call: " + timer.getTime() / timesToExecute);
   }


   protected static void executeTest() throws RemoteException, AmsException, RemoveException
   {
      Transaction testObject = null;

      /* Test the basic functionaility of the Transaction object.  This includes creating a
         new instance, setting attributes on it, and saving it to the database. */
      try{
         /* Create a new instance of the object */
         LOG.info("Tester: Creating instance of class Transaction");
         testObject = (Transaction)EJBObjectFactory.createClientEJB("t3://localhost:7001","Transaction");
         LOG.info("Tester: Calling newObject() method");
         testObject.newObject();

         /* Set attrbutes on the object */
         LOG.info("Tester: Setting attribute transaction_type_code to 'O'");
         testObject.setAttribute("transaction_type_code", "O");
   
         LOG.info("Tester: Setting attribute transaction_status to 'O'");
         testObject.setAttribute("transaction_status", "O");
   
         LOG.info("Tester: Setting attribute sequence_num to '7'");
         testObject.setAttribute("sequence_num", "7");
   
         LOG.info("Tester: Setting attribute show_on_notifications_tab to 'Y'");
         testObject.setAttribute("show_on_notifications_tab", "Y");
   
         LOG.info("Tester: Setting attribute transaction_status_date to '02/28/2000 12:13:54'");
         testObject.setAttribute("transaction_status_date", "02/28/2000 12:13:54");
   
         LOG.info("Tester: Setting attribute copy_of_amount to '5.2'");
         testObject.setAttribute("copy_of_amount", "5.2");
   
         LOG.info("Tester: Setting attribute copy_of_currency_code to 'O'");
         testObject.setAttribute("copy_of_currency_code", "O");
   
         LOG.info("Tester: Setting attribute authorization_errors to 'TEST'");
         testObject.setAttribute("authorization_errors", "TEST");
   
         LOG.info("Tester: Setting attribute copy_of_instr_type_code to 'O'");
         testObject.setAttribute("copy_of_instr_type_code", "O");
   
         LOG.info("Tester: Setting attribute first_authorize_status_date to '02/28/2000 12:13:54'");
         testObject.setAttribute("first_authorize_status_date", "02/28/2000 12:13:54");
   
         LOG.info("Tester: Setting attribute second_authorize_status_date to '02/28/2000 12:13:54'");
         testObject.setAttribute("second_authorize_status_date", "02/28/2000 12:13:54");
   
         LOG.info("Tester: Setting attribute transaction_as_text to 'TEST'");
         testObject.setAttribute("transaction_as_text", "TEST");
   
         LOG.info("Tester: Setting attribute standby_using_guarantee_form to 'Y'");
         testObject.setAttribute("standby_using_guarantee_form", "Y");
   
         LOG.info("Tester: Setting attribute return_message_id to 'TEST'");
         testObject.setAttribute("return_message_id", "TEST");
   
         LOG.info("Tester: Setting attribute instrument_amount to '5.2'");
         testObject.setAttribute("instrument_amount", "5.2");
   
         LOG.info("Tester: Setting attribute available_amount to '5.2'");
         testObject.setAttribute("available_amount", "5.2");
   
         LOG.info("Tester: Setting attribute liability_amt_in_limit_curr to '5.2'");
         testObject.setAttribute("liability_amt_in_limit_curr", "5.2");
   
         LOG.info("Tester: Setting attribute equivalent_amount to '5.2'");
         testObject.setAttribute("equivalent_amount", "5.2");
   
         LOG.info("Tester: Setting attribute base_currency_code to 'O'");
         testObject.setAttribute("base_currency_code", "O");
   
         LOG.info("Tester: Setting attribute liability_amt_in_base_curr to '5.2'");
         testObject.setAttribute("liability_amt_in_base_curr", "5.2");
   
         LOG.info("Tester: Setting attribute limit_currency_code to 'O'");
         testObject.setAttribute("limit_currency_code", "O");
   
         LOG.info("Tester: Setting attribute notification_date to '02/28/2000 12:13:54'");
         testObject.setAttribute("notification_date", "02/28/2000 12:13:54");
   
         LOG.info("Tester: Setting attribute globaltrade_msg to 'TEST'");
         testObject.setAttribute("globaltrade_msg", "TEST");
   
         LOG.info("Tester: Setting attribute display_change_transaction to 'Y'");
         testObject.setAttribute("display_change_transaction", "Y");
   
         LOG.info("Tester: Setting attribute conversion_indicator to 'Y'");
         testObject.setAttribute("conversion_indicator", "Y");
   
         LOG.info("Tester: Setting attribute rejection_reason_text to 'TEST'");
         testObject.setAttribute("rejection_reason_text", "TEST");
   
         LOG.info("Tester: Setting attribute rejection_indicator to 'TEST'");
         testObject.setAttribute("rejection_indicator", "TEST");
   
         LOG.info("Tester: Setting attribute date_started to '02/28/2000 12:13:54'");
         testObject.setAttribute("date_started", "02/28/2000 12:13:54");
   
         LOG.info("Tester: Setting attribute resulting_port_act_uoid to 'TEST'");
         testObject.setAttribute("resulting_port_act_uoid", "TEST");
   
         LOG.info("Tester: Setting attribute prior_active_port_act_uoid to 'TEST'");
         testObject.setAttribute("prior_active_port_act_uoid", "TEST");
   
         LOG.info("Tester: Setting attribute otl_transaction_uoid to 'TEST'");
         testObject.setAttribute("otl_transaction_uoid", "TEST");
   
         LOG.info("Tester: Setting attribute sequence_date to '02/28/2000 12:13:54'");
         testObject.setAttribute("sequence_date", "02/28/2000 12:13:54");
   
         LOG.info("Tester: Setting attribute payment_date to '02/28/2000 12:13:54'");
         testObject.setAttribute("payment_date", "02/28/2000 12:13:54");
   
         LOG.info("Tester: Setting attribute authorizing_panel_code_1 to 'O'");
         testObject.setAttribute("authorizing_panel_code_1", "O");
   
         LOG.info("Tester: Setting attribute authorizing_panel_code_2 to 'O'");
         testObject.setAttribute("authorizing_panel_code_2", "O");
   
         LOG.info("Tester: Setting attribute authorizing_panel_code_3 to 'O'");
         testObject.setAttribute("authorizing_panel_code_3", "O");
   
         LOG.info("Tester: Setting attribute authorizing_panel_code_4 to 'O'");
         testObject.setAttribute("authorizing_panel_code_4", "O");
   
         LOG.info("Tester: Setting attribute authorizing_panel_code_5 to 'O'");
         testObject.setAttribute("authorizing_panel_code_5", "O");
   
         LOG.info("Tester: Setting attribute authorizing_panel_code_6 to 'O'");
         testObject.setAttribute("authorizing_panel_code_6", "O");
   
         LOG.info("Tester: Setting attribute third_authorize_status_date to '02/28/2000 12:13:54'");
         testObject.setAttribute("third_authorize_status_date", "02/28/2000 12:13:54");
   
         LOG.info("Tester: Setting attribute fourth_authorize_status_date to '02/28/2000 12:13:54'");
         testObject.setAttribute("fourth_authorize_status_date", "02/28/2000 12:13:54");
   
         LOG.info("Tester: Setting attribute fifth_authorize_status_date to '02/28/2000 12:13:54'");
         testObject.setAttribute("fifth_authorize_status_date", "02/28/2000 12:13:54");
   
         LOG.info("Tester: Setting attribute sixth_authorize_status_date to '02/28/2000 12:13:54'");
         testObject.setAttribute("sixth_authorize_status_date", "02/28/2000 12:13:54");
   
         LOG.info("Tester: Setting attribute daily_limit_exceeded_indicator to 'Y'");
         testObject.setAttribute("daily_limit_exceeded_indicator", "Y");
   
         LOG.info("Tester: Setting attribute uploaded_ind to 'Y'");
         testObject.setAttribute("uploaded_ind", "Y");
   
         LOG.info("Tester: Setting association assigned_to_user_oid to ''");
         testObject.setAttribute("assigned_to_user_oid", "");
   
         LOG.info("Tester: Setting association first_authorizing_user_oid to ''");
         testObject.setAttribute("first_authorizing_user_oid", "");
   
         LOG.info("Tester: Setting association second_authorizing_user_oid to ''");
         testObject.setAttribute("second_authorizing_user_oid", "");
   
         LOG.info("Tester: Setting association last_entry_user_oid to ''");
         testObject.setAttribute("last_entry_user_oid", "");
   
         LOG.info("Tester: Setting association first_authorizing_work_group_oid to ''");
         testObject.setAttribute("first_authorizing_work_group_oid", "");
   
         LOG.info("Tester: Setting association second_authorizing_work_group_oid to ''");
         testObject.setAttribute("second_authorizing_work_group_oid", "");
   
         LOG.info("Tester: Setting association third_authorizing_user_oid to ''");
         testObject.setAttribute("third_authorizing_user_oid", "");
   
         LOG.info("Tester: Setting association fourth_authorizing_user_oid to ''");
         testObject.setAttribute("fourth_authorizing_user_oid", "");
   
         LOG.info("Tester: Setting association fifth_authorizing_user_oid to ''");
         testObject.setAttribute("fifth_authorizing_user_oid", "");
   
         LOG.info("Tester: Setting association sixth_authorizing_user_oid to ''");
         testObject.setAttribute("sixth_authorizing_user_oid", "");
   
         LOG.info("Tester: Setting association third_authorizing_work_group_oid to ''");
         testObject.setAttribute("third_authorizing_work_group_oid", "");
   
         LOG.info("Tester: Setting association fourth_authorizing_work_group_oid to ''");
         testObject.setAttribute("fourth_authorizing_work_group_oid", "");
   
         LOG.info("Tester: Setting association fifth_authorizing_work_group_oid to ''");
         testObject.setAttribute("fifth_authorizing_work_group_oid", "");
   
         LOG.info("Tester: Setting association sixth_authorizing_work_group_oid to ''");
         testObject.setAttribute("sixth_authorizing_work_group_oid", "");
   

         /* Save the object to the database */
         LOG.info("Tester: Calling save() method");
         testObject.save();
         LOG.info("Tester: Save complete"); 
  
      /* If an Exception occurs, print out the call stack so we can determine where it came from */
      }catch(Exception e) {
         e.printStackTrace();

      /* Print out any errors that were issued by the Business Object */
      }finally {
         List<IssuedError> errors = testObject.getIssuedErrors();
         for(IssuedError errorObject : errors) 
         {
            
            LOG.info(errorObject.getErrorText());
         }
         testObject.remove();
      }
   }
}
