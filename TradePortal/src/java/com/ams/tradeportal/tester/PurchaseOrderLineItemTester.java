
  

package com.ams.tradeportal.tester;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import com.ams.tradeportal.busobj.*;


/*
 * Client application to test the PurchaseOrderLineItem Business Object.  Creates a new instance of the
 * object, sets attributes on it, and attempts to save it to the database.  The time required to execute
 * the test is calculated and written to "System.Out".  Multiple iterations of the test can be executed
 * by specifying a command line argument.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class PurchaseOrderLineItemTester
{
private static final Logger LOG = LoggerFactory.getLogger(PurchaseOrderLineItemTester.class);

   public static void main (String[] argv) throws RemoteException, RemoveException, AmsException
   {
      /* Initialize the Object Server */
      LOG.info("Tester: Start of Test");
      try{
         ServerDeploy sd = null;
         LOG.info("Tester: Initializing Object Server");
         sd = (ServerDeploy)EJBObjectFactory.createClientEJB("t3://localhost:7001","ServerDeploy");  
         sd.init("t3://localhost:7001");
      }catch (Exception e) {}  
    
      /* Determine how many times to execute the test */
      int timesToExecute = new Integer(argv[0]).intValue();
      if (timesToExecute == 0) { timesToExecute = 1; }

      /* Start timing how long the test takes */
      ExecutionTimer timer = new ExecutionTimer();
      timer.setStart();

      /* Perform the test the number of times specified by the user */
      for(int i=0; !(i >= timesToExecute); i++)
         {executeTest();}
     
      /* Write out how long the test took. */
      timer.setEnd();
      LOG.info("Tester: End of Test");
      LOG.info("Tester: Execution Time per Call: " + timer.getTime() / timesToExecute);
   }


   protected static void executeTest() throws RemoteException, AmsException, RemoveException
   {
      PurchaseOrderLineItem testObject = null;

      /* Test the basic functionaility of the PurchaseOrderLineItem object.  This includes creating a
         new instance, setting attributes on it, and saving it to the database. */
      try{
         /* Create a new instance of the object */
         LOG.info("Tester: Creating instance of class PurchaseOrderLineItem");
         testObject = (PurchaseOrderLineItem)EJBObjectFactory.createClientEJB("t3://localhost:7001","PurchaseOrderLineItem");
         LOG.info("Tester: Calling newObject() method");
         testObject.newObject();

         /* Set attrbutes on the object */
         LOG.info("Tester: Setting attribute line_item_num to 'TEST'");
         testObject.setAttribute("line_item_num", "TEST");
   
         LOG.info("Tester: Setting attribute unit_price to '7'");
         testObject.setAttribute("unit_price", "7");
   
         LOG.info("Tester: Setting attribute unit_of_measure to 'TEST'");
         testObject.setAttribute("unit_of_measure", "TEST");
   
         LOG.info("Tester: Setting attribute quantity to '7'");
         testObject.setAttribute("quantity", "7");
   
         LOG.info("Tester: Setting attribute quantity_variance_plus to '7'");
         testObject.setAttribute("quantity_variance_plus", "7");
   
         LOG.info("Tester: Setting attribute prod_chars_ud1_label to 'TEST'");
         testObject.setAttribute("prod_chars_ud1_label", "TEST");
   
         LOG.info("Tester: Setting attribute prod_chars_ud2_label to 'TEST'");
         testObject.setAttribute("prod_chars_ud2_label", "TEST");
   
         LOG.info("Tester: Setting attribute prod_chars_ud3_label to 'TEST'");
         testObject.setAttribute("prod_chars_ud3_label", "TEST");
   
         LOG.info("Tester: Setting attribute prod_chars_ud4_label to 'TEST'");
         testObject.setAttribute("prod_chars_ud4_label", "TEST");
   
         LOG.info("Tester: Setting attribute prod_chars_ud5_label to 'TEST'");
         testObject.setAttribute("prod_chars_ud5_label", "TEST");
   
         LOG.info("Tester: Setting attribute prod_chars_ud6_label to 'TEST'");
         testObject.setAttribute("prod_chars_ud6_label", "TEST");
   
         LOG.info("Tester: Setting attribute prod_chars_ud7_label to 'TEST'");
         testObject.setAttribute("prod_chars_ud7_label", "TEST");
   
         LOG.info("Tester: Setting attribute prod_chars_ud1_value to 'TEST'");
         testObject.setAttribute("prod_chars_ud1_value", "TEST");
   
         LOG.info("Tester: Setting attribute prod_chars_ud2_value to 'TEST'");
         testObject.setAttribute("prod_chars_ud2_value", "TEST");
   
         LOG.info("Tester: Setting attribute prod_chars_ud3_value to 'TEST'");
         testObject.setAttribute("prod_chars_ud3_value", "TEST");
   
         LOG.info("Tester: Setting attribute prod_chars_ud4_value to 'TEST'");
         testObject.setAttribute("prod_chars_ud4_value", "TEST");
   
         LOG.info("Tester: Setting attribute prod_chars_ud5_value to 'TEST'");
         testObject.setAttribute("prod_chars_ud5_value", "TEST");
   
         LOG.info("Tester: Setting attribute prod_chars_ud6_value to 'TEST'");
         testObject.setAttribute("prod_chars_ud6_value", "TEST");
   
         LOG.info("Tester: Setting attribute prod_chars_ud7_value to 'TEST'");
         testObject.setAttribute("prod_chars_ud7_value", "TEST");
   
         LOG.info("Tester: Setting attribute derived_line_item_ind to 'Y'");
         testObject.setAttribute("derived_line_item_ind", "Y");
   

         /* Save the object to the database */
         LOG.info("Tester: Calling save() method");
         testObject.save();
         LOG.info("Tester: Save complete"); 
  
      /* If an Exception occurs, print out the call stack so we can determine where it came from */
      }catch(Exception e) {
         e.printStackTrace();

      /* Print out any errors that were issued by the Business Object */
      }finally {
         List<IssuedError> errors = testObject.getIssuedErrors();
         for(IssuedError errorObject : errors) 
         {
            
            LOG.info(errorObject.getErrorText());
         }
         testObject.remove();
      }
   }
}
