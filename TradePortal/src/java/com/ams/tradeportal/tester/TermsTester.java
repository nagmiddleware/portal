
  

package com.ams.tradeportal.tester;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import com.ams.tradeportal.busobj.*;


/*
 * Client application to test the Terms Business Object.  Creates a new instance of the
 * object, sets attributes on it, and attempts to save it to the database.  The time required to execute
 * the test is calculated and written to "System.Out".  Multiple iterations of the test can be executed
 * by specifying a command line argument.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class TermsTester
{
private static final Logger LOG = LoggerFactory.getLogger(TermsTester.class);

   public static void main (String[] argv) throws RemoteException, RemoveException, AmsException
   {
      /* Initialize the Object Server */
      LOG.info("Tester: Start of Test");
      try{
         ServerDeploy sd = null;
         LOG.info("Tester: Initializing Object Server");
         sd = (ServerDeploy)EJBObjectFactory.createClientEJB("t3://localhost:7001","ServerDeploy");  
         sd.init("t3://localhost:7001");
      }catch (Exception e) {}  
    
      /* Determine how many times to execute the test */
      int timesToExecute = new Integer(argv[0]).intValue();
      if (timesToExecute == 0) { timesToExecute = 1; }

      /* Start timing how long the test takes */
      ExecutionTimer timer = new ExecutionTimer();
      timer.setStart();

      /* Perform the test the number of times specified by the user */
      for(int i=0; !(i >= timesToExecute); i++)
         {executeTest();}
     
      /* Write out how long the test took. */
      timer.setEnd();
      LOG.info("Tester: End of Test");
      LOG.info("Tester: Execution Time per Call: " + timer.getTime() / timesToExecute);
   }


   protected static void executeTest() throws RemoteException, AmsException, RemoveException
   {
      Terms testObject = null;

      /* Test the basic functionaility of the Terms object.  This includes creating a
         new instance, setting attributes on it, and saving it to the database. */
      try{
         /* Create a new instance of the object */
         LOG.info("Tester: Creating instance of class Terms");
         testObject = (Terms)EJBObjectFactory.createClientEJB("t3://localhost:7001","Terms");
         LOG.info("Tester: Calling newObject() method");
         testObject.newObject();

         /* Set attrbutes on the object */
         LOG.info("Tester: Setting attribute accounts_receivable_type to 'O'");
         testObject.setAttribute("accounts_receivable_type", "O");
   
         LOG.info("Tester: Setting attribute additional_conditions to 'TEST'");
         testObject.setAttribute("additional_conditions", "TEST");
   
         LOG.info("Tester: Setting attribute addl_amounts_covered to 'TEST'");
         testObject.setAttribute("addl_amounts_covered", "TEST");
   
         LOG.info("Tester: Setting attribute addl_doc_indicator to 'Y'");
         testObject.setAttribute("addl_doc_indicator", "Y");
   
         LOG.info("Tester: Setting attribute addl_doc_text to 'TEST'");
         testObject.setAttribute("addl_doc_text", "TEST");
   
         LOG.info("Tester: Setting attribute adjustment_type to 'O'");
         testObject.setAttribute("adjustment_type", "O");
   
         LOG.info("Tester: Setting attribute amendment_date to '02/28/2000 12:13:54'");
         testObject.setAttribute("amendment_date", "02/28/2000 12:13:54");
   
         LOG.info("Tester: Setting attribute amendment_details to 'TEST'");
         testObject.setAttribute("amendment_details", "TEST");
   
         LOG.info("Tester: Setting attribute amount to '5.2'");
         testObject.setAttribute("amount", "5.2");
   
         LOG.info("Tester: Setting attribute amount_currency_code to 'O'");
         testObject.setAttribute("amount_currency_code", "O");
   
         LOG.info("Tester: Setting attribute amt_tolerance_neg to '7'");
         testObject.setAttribute("amt_tolerance_neg", "7");
   
         LOG.info("Tester: Setting attribute amt_tolerance_pos to '7'");
         testObject.setAttribute("amt_tolerance_pos", "7");
   
         LOG.info("Tester: Setting attribute app_debit_acct_charges to 'TEST'");
         testObject.setAttribute("app_debit_acct_charges", "TEST");
   
         LOG.info("Tester: Setting attribute app_debit_acct_principal to 'TEST'");
         testObject.setAttribute("app_debit_acct_principal", "TEST");
   
         LOG.info("Tester: Setting attribute assignees_account_number to 'TEST'");
         testObject.setAttribute("assignees_account_number", "TEST");
   
         LOG.info("Tester: Setting attribute attention_of to 'TEST'");
         testObject.setAttribute("attention_of", "TEST");
   
         LOG.info("Tester: Setting attribute auto_extend to 'Y'");
         testObject.setAttribute("auto_extend", "Y");
         //Leelavathi IR#T36000016452 Rel-8.2 06/10/2013 Begin
         //commenting out, because RQA instrument has Available_by at two places.
         LOG.info("Tester: Setting attribute available_by to 'O'");
         /*testObject.setAttribute("available_by", "O");*/
         testObject.setAttribute("payment_type", "O");
         //Leelavathi IR#T36000016452 Rel-8.2 06/10/2013 End
         LOG.info("Tester: Setting attribute available_with_party to 'O'");
         testObject.setAttribute("available_with_party", "O");
   
         LOG.info("Tester: Setting attribute bank_action_required to 'O'");
         testObject.setAttribute("bank_action_required", "O");
   
         LOG.info("Tester: Setting attribute bank_charges_type to 'O'");
         testObject.setAttribute("bank_charges_type", "O");
   
         LOG.info("Tester: Setting attribute branch_code to 'TEST'");
         testObject.setAttribute("branch_code", "TEST");
   
         LOG.info("Tester: Setting attribute cert_origin_copies to '7'");
         testObject.setAttribute("cert_origin_copies", "7");
   
         LOG.info("Tester: Setting attribute cert_origin_indicator to 'Y'");
         testObject.setAttribute("cert_origin_indicator", "Y");
   
         LOG.info("Tester: Setting attribute cert_origin_originals to '7'");
         testObject.setAttribute("cert_origin_originals", "7");
   
         LOG.info("Tester: Setting attribute cert_origin_text to 'TEST'");
         testObject.setAttribute("cert_origin_text", "TEST");
   
         LOG.info("Tester: Setting attribute characteristic_type to 'O'");
         testObject.setAttribute("characteristic_type", "O");
   
         LOG.info("Tester: Setting attribute collect_interest_at to 'TEST'");
         testObject.setAttribute("collect_interest_at", "TEST");
   
         LOG.info("Tester: Setting attribute collect_interest_at_indicator to 'Y'");
         testObject.setAttribute("collect_interest_at_indicator", "Y");
   
         LOG.info("Tester: Setting attribute collection_charges_includ_txt to 'TEST'");
         testObject.setAttribute("collection_charges_includ_txt", "TEST");
   
         LOG.info("Tester: Setting attribute collection_charges_type to 'O'");
         testObject.setAttribute("collection_charges_type", "O");
   
         LOG.info("Tester: Setting attribute collection_date to '02/28/2000 12:13:54'");
         testObject.setAttribute("collection_date", "02/28/2000 12:13:54");
   
         LOG.info("Tester: Setting attribute comm_invoice_copies to '7'");
         testObject.setAttribute("comm_invoice_copies", "7");
   
         LOG.info("Tester: Setting attribute comm_invoice_indicator to 'Y'");
         testObject.setAttribute("comm_invoice_indicator", "Y");
   
         LOG.info("Tester: Setting attribute comm_invoice_originals to '7'");
         testObject.setAttribute("comm_invoice_originals", "7");
   
         LOG.info("Tester: Setting attribute comm_invoice_text to 'TEST'");
         testObject.setAttribute("comm_invoice_text", "TEST");
   
         LOG.info("Tester: Setting attribute coms_chrgs_foreign_acct_curr to 'O'");
         testObject.setAttribute("coms_chrgs_foreign_acct_curr", "O");
   
         LOG.info("Tester: Setting attribute coms_chrgs_foreign_acct_num to 'TEST'");
         testObject.setAttribute("coms_chrgs_foreign_acct_num", "TEST");
   
         LOG.info("Tester: Setting attribute coms_chrgs_other_text to 'TEST'");
         testObject.setAttribute("coms_chrgs_other_text", "TEST");
   
         LOG.info("Tester: Setting attribute coms_chrgs_our_account_num to 'TEST'");
         testObject.setAttribute("coms_chrgs_our_account_num", "TEST");
   
         LOG.info("Tester: Setting attribute confirmation_indicator to 'Y'");
         testObject.setAttribute("confirmation_indicator", "Y");
   
         LOG.info("Tester: Setting attribute confirmation_type to 'O'");
         testObject.setAttribute("confirmation_type", "O");
   
         LOG.info("Tester: Setting attribute covered_by_fec_number to 'TEST'");
         testObject.setAttribute("covered_by_fec_number", "TEST");
   
         LOG.info("Tester: Setting attribute discount_rate to '5.2'");
         testObject.setAttribute("discount_rate", "5.2");
   
         LOG.info("Tester: Setting attribute do_not_waive_refused_charges to 'Y'");
         testObject.setAttribute("do_not_waive_refused_charges", "Y");
   
         LOG.info("Tester: Setting attribute do_not_waive_refused_interest to 'Y'");
         testObject.setAttribute("do_not_waive_refused_interest", "Y");
   
         LOG.info("Tester: Setting attribute drafts_required to 'Y'");
         testObject.setAttribute("drafts_required", "Y");
   
         LOG.info("Tester: Setting attribute drawing_number to 'TEST'");
         testObject.setAttribute("drawing_number", "TEST");
   
         LOG.info("Tester: Setting attribute drawn_on_party to 'O'");
         testObject.setAttribute("drawn_on_party", "O");
   
         LOG.info("Tester: Setting attribute ex_rt_variation_adjust to 'O'");
         testObject.setAttribute("ex_rt_variation_adjust", "O");
   
         LOG.info("Tester: Setting attribute expiry_date to '02/28/2000 12:13:54'");
         testObject.setAttribute("expiry_date", "02/28/2000 12:13:54");
   
         LOG.info("Tester: Setting attribute export_discrepancy_instr to 'O'");
         testObject.setAttribute("export_discrepancy_instr", "O");
   
         LOG.info("Tester: Setting attribute fax_number to 'TEST'");
         testObject.setAttribute("fax_number", "TEST");
   
         LOG.info("Tester: Setting attribute fec_amount to '5.2'");
         testObject.setAttribute("fec_amount", "5.2");
   
         LOG.info("Tester: Setting attribute fec_maturity_date to '02/28/2000 12:13:54'");
         testObject.setAttribute("fec_maturity_date", "02/28/2000 12:13:54");
   
         LOG.info("Tester: Setting attribute fec_rate to '5.2'");
         testObject.setAttribute("fec_rate", "5.2");
   
         LOG.info("Tester: Setting attribute finance_drawing to 'Y'");
         testObject.setAttribute("finance_drawing", "Y");
   
         LOG.info("Tester: Setting attribute finance_drawing_num_days to '7'");
         testObject.setAttribute("finance_drawing_num_days", "7");
   
         LOG.info("Tester: Setting attribute finance_drawing_type to 'O'");
         testObject.setAttribute("finance_drawing_type", "O");
   
         LOG.info("Tester: Setting attribute finance_type to 'O'");
         testObject.setAttribute("finance_type", "O");
   
         LOG.info("Tester: Setting attribute finance_type_text to 'TEST'");
         testObject.setAttribute("finance_type_text", "TEST");
   
         LOG.info("Tester: Setting attribute first_shipment_oid to '7'");
         testObject.setAttribute("first_shipment_oid", "7");
   
         LOG.info("Tester: Setting attribute funds_xfer_settle_type to 'O'");
         testObject.setAttribute("funds_xfer_settle_type", "O");
   
         LOG.info("Tester: Setting attribute guar_accept_instr to 'Y'");
         testObject.setAttribute("guar_accept_instr", "Y");
   
         LOG.info("Tester: Setting attribute guar_advise_issuance to 'Y'");
         testObject.setAttribute("guar_advise_issuance", "Y");
   
         LOG.info("Tester: Setting attribute guar_bank_standard_text to 'TEST'");
         testObject.setAttribute("guar_bank_standard_text", "TEST");
   
         LOG.info("Tester: Setting attribute guar_bank_std_phrase_type to 'O'");
         testObject.setAttribute("guar_bank_std_phrase_type", "O");
   
         LOG.info("Tester: Setting attribute guar_customer_text to 'TEST'");
         testObject.setAttribute("guar_customer_text", "TEST");
   
         LOG.info("Tester: Setting attribute guar_deliver_by to 'O'");
         testObject.setAttribute("guar_deliver_by", "O");
   
         LOG.info("Tester: Setting attribute guar_deliver_to to 'O'");
         testObject.setAttribute("guar_deliver_to", "O");
   
         LOG.info("Tester: Setting attribute guar_expiry_date_type to 'O'");
         testObject.setAttribute("guar_expiry_date_type", "O");
   
         LOG.info("Tester: Setting attribute guar_valid_from_date to '02/28/2000 12:13:54'");
         testObject.setAttribute("guar_valid_from_date", "02/28/2000 12:13:54");
   
         LOG.info("Tester: Setting attribute guar_valid_from_date_type to 'O'");
         testObject.setAttribute("guar_valid_from_date_type", "O");
   
         LOG.info("Tester: Setting attribute guarantee_issue_type to 'O'");
         testObject.setAttribute("guarantee_issue_type", "O");
   
         LOG.info("Tester: Setting attribute import_discrepancy_instr to 'O'");
         testObject.setAttribute("import_discrepancy_instr", "O");
   
         LOG.info("Tester: Setting attribute in_advance_disposition to 'O'");
         testObject.setAttribute("in_advance_disposition", "O");
   
         LOG.info("Tester: Setting attribute in_arrears_disposition to 'O'");
         testObject.setAttribute("in_arrears_disposition", "O");
   
         LOG.info("Tester: Setting attribute in_case_of_need_contact to 'Y'");
         testObject.setAttribute("in_case_of_need_contact", "Y");
   
         LOG.info("Tester: Setting attribute in_case_of_need_contact_text to 'TEST'");
         testObject.setAttribute("in_case_of_need_contact_text", "TEST");
   
         LOG.info("Tester: Setting attribute in_case_of_need_use_instruct to 'O'");
         testObject.setAttribute("in_case_of_need_use_instruct", "O");
   
         LOG.info("Tester: Setting attribute ins_policy_copies to '7'");
         testObject.setAttribute("ins_policy_copies", "7");
   
         LOG.info("Tester: Setting attribute ins_policy_indicator to 'Y'");
         testObject.setAttribute("ins_policy_indicator", "Y");
   
         LOG.info("Tester: Setting attribute ins_policy_originals to '7'");
         testObject.setAttribute("ins_policy_originals", "7");
   
         LOG.info("Tester: Setting attribute ins_policy_text to 'TEST'");
         testObject.setAttribute("ins_policy_text", "TEST");
   
         LOG.info("Tester: Setting attribute instr_advise_accept_by_telco to 'Y'");
         testObject.setAttribute("instr_advise_accept_by_telco", "Y");
   
         LOG.info("Tester: Setting attribute instr_advise_non_accept_by_tel to 'Y'");
         testObject.setAttribute("instr_advise_non_accept_by_tel", "Y");
   
         LOG.info("Tester: Setting attribute instr_advise_non_pmt_by_telco to 'Y'");
         testObject.setAttribute("instr_advise_non_pmt_by_telco", "Y");
   
         LOG.info("Tester: Setting attribute instr_advise_pmt_by_telco to 'Y'");
         testObject.setAttribute("instr_advise_pmt_by_telco", "Y");
   
         LOG.info("Tester: Setting attribute instr_note_for_non_accept to 'Y'");
         testObject.setAttribute("instr_note_for_non_accept", "Y");
   
         LOG.info("Tester: Setting attribute instr_note_for_non_pmt to 'Y'");
         testObject.setAttribute("instr_note_for_non_pmt", "Y");
   
         LOG.info("Tester: Setting attribute instr_other to 'TEST'");
         testObject.setAttribute("instr_other", "TEST");
   
         LOG.info("Tester: Setting attribute instr_protest_for_non_accept to 'Y'");
         testObject.setAttribute("instr_protest_for_non_accept", "Y");
   
         LOG.info("Tester: Setting attribute instr_protest_for_non_pmt to 'Y'");
         testObject.setAttribute("instr_protest_for_non_pmt", "Y");
   
         LOG.info("Tester: Setting attribute instr_release_doc_on_accept to 'Y'");
         testObject.setAttribute("instr_release_doc_on_accept", "Y");
   
         LOG.info("Tester: Setting attribute instr_release_doc_on_pmt to 'Y'");
         testObject.setAttribute("instr_release_doc_on_pmt", "Y");
   
         LOG.info("Tester: Setting attribute insurance_endorse_value_plus to '5.2'");
         testObject.setAttribute("insurance_endorse_value_plus", "5.2");
   
         LOG.info("Tester: Setting attribute insurance_risk_type to 'O'");
         testObject.setAttribute("insurance_risk_type", "O");
   
         LOG.info("Tester: Setting attribute interest_debit_acct_num to 'TEST'");
         testObject.setAttribute("interest_debit_acct_num", "TEST");
   
         LOG.info("Tester: Setting attribute interest_to_be_paid to 'O'");
         testObject.setAttribute("interest_to_be_paid", "O");
   
         LOG.info("Tester: Setting attribute internal_instructions to 'TEST'");
         testObject.setAttribute("internal_instructions", "TEST");
   
         LOG.info("Tester: Setting attribute irrevocable to 'Y'");
         testObject.setAttribute("irrevocable", "Y");
   
         LOG.info("Tester: Setting attribute issuer_ref_num to 'TEST'");
         testObject.setAttribute("issuer_ref_num", "TEST");
   
         LOG.info("Tester: Setting attribute linked_instrument_id to 'TEST'");
         testObject.setAttribute("linked_instrument_id", "TEST");
   
         LOG.info("Tester: Setting attribute linked_instrument_type to 'O'");
         testObject.setAttribute("linked_instrument_type", "O");
   
         LOG.info("Tester: Setting attribute linked_tran_seq_num to '7'");
         testObject.setAttribute("linked_tran_seq_num", "7");
   
         LOG.info("Tester: Setting attribute linked_transaction_type to 'O'");
         testObject.setAttribute("linked_transaction_type", "O");
   
         LOG.info("Tester: Setting attribute loan_maturity_debit_acct to 'TEST'");
         testObject.setAttribute("loan_maturity_debit_acct", "TEST");
   
         LOG.info("Tester: Setting attribute loan_maturity_debit_other_txt to 'TEST'");
         testObject.setAttribute("loan_maturity_debit_other_txt", "TEST");
   
         LOG.info("Tester: Setting attribute loan_maturity_debit_type to 'O'");
         testObject.setAttribute("loan_maturity_debit_type", "O");
   
         LOG.info("Tester: Setting attribute loan_proceeds_credit_acct to 'TEST'");
         testObject.setAttribute("loan_proceeds_credit_acct", "TEST");
   
         LOG.info("Tester: Setting attribute loan_proceeds_credit_other_txt to 'TEST'");
         testObject.setAttribute("loan_proceeds_credit_other_txt", "TEST");
   
         LOG.info("Tester: Setting attribute loan_proceeds_credit_type to 'O'");
         testObject.setAttribute("loan_proceeds_credit_type", "O");
   
         LOG.info("Tester: Setting attribute loan_proceeds_pmt_amount to '5.2'");
         testObject.setAttribute("loan_proceeds_pmt_amount", "5.2");
   
         LOG.info("Tester: Setting attribute loan_proceeds_pmt_curr to 'O'");
         testObject.setAttribute("loan_proceeds_pmt_curr", "O");
   
         LOG.info("Tester: Setting attribute loan_rate to '5.2'");
         testObject.setAttribute("loan_rate", "5.2");
   
         LOG.info("Tester: Setting attribute loan_start_date to '02/28/2000 12:13:54'");
         testObject.setAttribute("loan_start_date", "02/28/2000 12:13:54");
   
         LOG.info("Tester: Setting attribute loan_terms_fixed_maturity_dt to '02/28/2000 12:13:54'");
         testObject.setAttribute("loan_terms_fixed_maturity_dt", "02/28/2000 12:13:54");
   
         LOG.info("Tester: Setting attribute loan_terms_number_of_days to '7'");
         testObject.setAttribute("loan_terms_number_of_days", "7");
   
         LOG.info("Tester: Setting attribute loan_terms_type to 'O'");
         testObject.setAttribute("loan_terms_type", "O");
   
         LOG.info("Tester: Setting attribute maturity_covered_by_fec_number to 'TEST'");
         testObject.setAttribute("maturity_covered_by_fec_number", "TEST");
   
         LOG.info("Tester: Setting attribute maturity_fec_amount to '5.2'");
         testObject.setAttribute("maturity_fec_amount", "5.2");
   
         LOG.info("Tester: Setting attribute maturity_fec_maturity_date to '02/28/2000 12:13:54'");
         testObject.setAttribute("maturity_fec_maturity_date", "02/28/2000 12:13:54");
   
         LOG.info("Tester: Setting attribute maturity_fec_rate to '5.2'");
         testObject.setAttribute("maturity_fec_rate", "5.2");
   
         LOG.info("Tester: Setting attribute maturity_use_fec to 'Y'");
         testObject.setAttribute("maturity_use_fec", "Y");
   
         LOG.info("Tester: Setting attribute maturity_use_mkt_rate to 'Y'");
         testObject.setAttribute("maturity_use_mkt_rate", "Y");
   
         LOG.info("Tester: Setting attribute maturity_use_other to 'Y'");
         testObject.setAttribute("maturity_use_other", "Y");
   
         LOG.info("Tester: Setting attribute maturity_use_other_text to 'TEST'");
         testObject.setAttribute("maturity_use_other_text", "TEST");
   
         LOG.info("Tester: Setting attribute maximum_credit_amount to 'TEST'");
         testObject.setAttribute("maximum_credit_amount", "TEST");
   
         LOG.info("Tester: Setting attribute message_to_beneficiary to 'TEST'");
         testObject.setAttribute("message_to_beneficiary", "TEST");
   
         LOG.info("Tester: Setting attribute multiple_related_instruments to 'TEST'");
         testObject.setAttribute("multiple_related_instruments", "TEST");
   
         LOG.info("Tester: Setting attribute opening_bank_instr_num to 'TEST'");
         testObject.setAttribute("opening_bank_instr_num", "TEST");
   
         LOG.info("Tester: Setting attribute operative to 'Y'");
         testObject.setAttribute("operative", "Y");
   
         LOG.info("Tester: Setting attribute other_req_doc_1_copies to '7'");
         testObject.setAttribute("other_req_doc_1_copies", "7");
   
         LOG.info("Tester: Setting attribute other_req_doc_1_indicator to 'Y'");
         testObject.setAttribute("other_req_doc_1_indicator", "Y");
   
         LOG.info("Tester: Setting attribute other_req_doc_1_name to 'TEST'");
         testObject.setAttribute("other_req_doc_1_name", "TEST");
   
         LOG.info("Tester: Setting attribute other_req_doc_1_originals to '7'");
         testObject.setAttribute("other_req_doc_1_originals", "7");
   
         LOG.info("Tester: Setting attribute other_req_doc_1_text to 'TEST'");
         testObject.setAttribute("other_req_doc_1_text", "TEST");
   
         LOG.info("Tester: Setting attribute other_req_doc_2_copies to '7'");
         testObject.setAttribute("other_req_doc_2_copies", "7");
   
         LOG.info("Tester: Setting attribute other_req_doc_2_indicator to 'Y'");
         testObject.setAttribute("other_req_doc_2_indicator", "Y");
   
         LOG.info("Tester: Setting attribute other_req_doc_2_name to 'TEST'");
         testObject.setAttribute("other_req_doc_2_name", "TEST");
   
         LOG.info("Tester: Setting attribute other_req_doc_2_originals to '7'");
         testObject.setAttribute("other_req_doc_2_originals", "7");
   
         LOG.info("Tester: Setting attribute other_req_doc_2_text to 'TEST'");
         testObject.setAttribute("other_req_doc_2_text", "TEST");
   
         LOG.info("Tester: Setting attribute other_req_doc_3_copies to '7'");
         testObject.setAttribute("other_req_doc_3_copies", "7");
   
         LOG.info("Tester: Setting attribute other_req_doc_3_indicator to 'Y'");
         testObject.setAttribute("other_req_doc_3_indicator", "Y");
   
         LOG.info("Tester: Setting attribute other_req_doc_3_name to 'TEST'");
         testObject.setAttribute("other_req_doc_3_name", "TEST");
   
         LOG.info("Tester: Setting attribute other_req_doc_3_originals to '7'");
         testObject.setAttribute("other_req_doc_3_originals", "7");
   
         LOG.info("Tester: Setting attribute other_req_doc_3_text to 'TEST'");
         testObject.setAttribute("other_req_doc_3_text", "TEST");
   
         LOG.info("Tester: Setting attribute other_req_doc_4_copies to '7'");
         testObject.setAttribute("other_req_doc_4_copies", "7");
   
         LOG.info("Tester: Setting attribute other_req_doc_4_indicator to 'Y'");
         testObject.setAttribute("other_req_doc_4_indicator", "Y");
   
         LOG.info("Tester: Setting attribute other_req_doc_4_name to 'TEST'");
         testObject.setAttribute("other_req_doc_4_name", "TEST");
   
         LOG.info("Tester: Setting attribute other_req_doc_4_originals to '7'");
         testObject.setAttribute("other_req_doc_4_originals", "7");
   
         LOG.info("Tester: Setting attribute other_req_doc_4_text to 'TEST'");
         testObject.setAttribute("other_req_doc_4_text", "TEST");
   
         LOG.info("Tester: Setting attribute other_settlement_instructions to 'TEST'");
         testObject.setAttribute("other_settlement_instructions", "TEST");
   
         LOG.info("Tester: Setting attribute overseas_validity_date to '02/28/2000 12:13:54'");
         testObject.setAttribute("overseas_validity_date", "02/28/2000 12:13:54");
   
         LOG.info("Tester: Setting attribute packing_list_copies to '7'");
         testObject.setAttribute("packing_list_copies", "7");
   
         LOG.info("Tester: Setting attribute packing_list_indicator to 'Y'");
         testObject.setAttribute("packing_list_indicator", "Y");
   
         LOG.info("Tester: Setting attribute packing_list_originals to '7'");
         testObject.setAttribute("packing_list_originals", "7");
   
         LOG.info("Tester: Setting attribute packing_list_text to 'TEST'");
         testObject.setAttribute("packing_list_text", "TEST");
   
         LOG.info("Tester: Setting attribute part_payments_percent to '7'");
         testObject.setAttribute("part_payments_percent", "7");
   
         LOG.info("Tester: Setting attribute part_payments_type to 'O'");
         testObject.setAttribute("part_payments_type", "O");
   
         LOG.info("Tester: Setting attribute partial_shipment_allowed to 'Y'");
         testObject.setAttribute("partial_shipment_allowed", "Y");
   
         LOG.info("Tester: Setting attribute payment_instructions to 'TEST'");
         testObject.setAttribute("payment_instructions", "TEST");
   
         LOG.info("Tester: Setting attribute place_of_expiry to 'TEST'");
         testObject.setAttribute("place_of_expiry", "TEST");
   
         LOG.info("Tester: Setting attribute pmt_instr_multiple_phrase to 'O'");
         testObject.setAttribute("pmt_instr_multiple_phrase", "O");
   
         LOG.info("Tester: Setting attribute pmt_terms_days_after_type to 'O'");
         testObject.setAttribute("pmt_terms_days_after_type", "O");
   
         LOG.info("Tester: Setting attribute pmt_terms_fixed_maturity_date to '02/28/2000 12:13:54'");
         testObject.setAttribute("pmt_terms_fixed_maturity_date", "02/28/2000 12:13:54");
   
         LOG.info("Tester: Setting attribute pmt_terms_num_days_after to '7'");
         testObject.setAttribute("pmt_terms_num_days_after", "7");
   
         LOG.info("Tester: Setting attribute pmt_terms_percent_invoice to '7'");
         testObject.setAttribute("pmt_terms_percent_invoice", "7");
   
         LOG.info("Tester: Setting attribute pmt_terms_type to 'O'");
         testObject.setAttribute("pmt_terms_type", "O");
   
         LOG.info("Tester: Setting attribute present_air_waybill to 'Y'");
         testObject.setAttribute("present_air_waybill", "Y");
   
         LOG.info("Tester: Setting attribute present_bill_of_lading to 'Y'");
         testObject.setAttribute("present_bill_of_lading", "Y");
   
         LOG.info("Tester: Setting attribute present_bills_of_exchange to 'Y'");
         testObject.setAttribute("present_bills_of_exchange", "Y");
   
         LOG.info("Tester: Setting attribute present_cert_of_origin to 'Y'");
         testObject.setAttribute("present_cert_of_origin", "Y");
   
         LOG.info("Tester: Setting attribute present_commercial_invoice to 'Y'");
         testObject.setAttribute("present_commercial_invoice", "Y");
   
         LOG.info("Tester: Setting attribute present_docs_days_user_ind to 'Y'");
         testObject.setAttribute("present_docs_days_user_ind", "Y");
   
         LOG.info("Tester: Setting attribute present_docs_within_days to '7'");
         testObject.setAttribute("present_docs_within_days", "7");
   
         LOG.info("Tester: Setting attribute present_insurance to 'Y'");
         testObject.setAttribute("present_insurance", "Y");
   
         LOG.info("Tester: Setting attribute present_non_neg_bill_of_lading to 'Y'");
         testObject.setAttribute("present_non_neg_bill_of_lading", "Y");
   
         LOG.info("Tester: Setting attribute present_num_air_waybill to 'TEST'");
         testObject.setAttribute("present_num_air_waybill", "TEST");
   
         LOG.info("Tester: Setting attribute present_num_bill_of_lading to 'TEST'");
         testObject.setAttribute("present_num_bill_of_lading", "TEST");
   
         LOG.info("Tester: Setting attribute present_num_bills_of_exchange to 'TEST'");
         testObject.setAttribute("present_num_bills_of_exchange", "TEST");
   
         LOG.info("Tester: Setting attribute present_num_cert_of_origin to 'TEST'");
         testObject.setAttribute("present_num_cert_of_origin", "TEST");
   
         LOG.info("Tester: Setting attribute present_num_commercial_invoice to 'TEST'");
         testObject.setAttribute("present_num_commercial_invoice", "TEST");
   
         LOG.info("Tester: Setting attribute present_num_insurance to 'TEST'");
         testObject.setAttribute("present_num_insurance", "TEST");
   
         LOG.info("Tester: Setting attribute present_num_non_neg_bill_ladng to 'TEST'");
         testObject.setAttribute("present_num_non_neg_bill_ladng", "TEST");
   
         LOG.info("Tester: Setting attribute present_num_packing_list to 'TEST'");
         testObject.setAttribute("present_num_packing_list", "TEST");
   
         LOG.info("Tester: Setting attribute present_other to 'Y'");
         testObject.setAttribute("present_other", "Y");
   
         LOG.info("Tester: Setting attribute present_other_text to 'TEST'");
         testObject.setAttribute("present_other_text", "TEST");
   
         LOG.info("Tester: Setting attribute present_packing_list to 'Y'");
         testObject.setAttribute("present_packing_list", "Y");
   
         LOG.info("Tester: Setting attribute purpose_type to 'TEST'");
         testObject.setAttribute("purpose_type", "TEST");
   
         LOG.info("Tester: Setting attribute reference_number to 'TEST'");
         testObject.setAttribute("reference_number", "TEST");
   
         LOG.info("Tester: Setting attribute related_instrument_id to 'TEST'");
         testObject.setAttribute("related_instrument_id", "TEST");
   
         LOG.info("Tester: Setting attribute release_by_bank_on to '02/28/2000 12:13:54'");
         testObject.setAttribute("release_by_bank_on", "02/28/2000 12:13:54");
   
         LOG.info("Tester: Setting attribute revolve to 'Y'");
         testObject.setAttribute("revolve", "Y");
   
         LOG.info("Tester: Setting attribute settlement_foreign_acct_curr to 'O'");
         testObject.setAttribute("settlement_foreign_acct_curr", "O");
   
         LOG.info("Tester: Setting attribute settlement_foreign_acct_num to 'TEST'");
         testObject.setAttribute("settlement_foreign_acct_num", "TEST");
   
         LOG.info("Tester: Setting attribute settlement_our_account_num to 'TEST'");
         testObject.setAttribute("settlement_our_account_num", "TEST");
   
         LOG.info("Tester: Setting attribute special_bank_instructions to 'TEST'");
         testObject.setAttribute("special_bank_instructions", "TEST");
   
         LOG.info("Tester: Setting attribute tender_order_contract_details to 'TEST'");
         testObject.setAttribute("tender_order_contract_details", "TEST");
   
         LOG.info("Tester: Setting attribute tenor_1 to 'TEST'");
         testObject.setAttribute("tenor_1", "TEST");
   
         LOG.info("Tester: Setting attribute tenor_1_amount to '5.2'");
         testObject.setAttribute("tenor_1_amount", "5.2");
   
         LOG.info("Tester: Setting attribute tenor_1_draft_number to 'TEST'");
         testObject.setAttribute("tenor_1_draft_number", "TEST");
   
         LOG.info("Tester: Setting attribute tenor_2 to 'TEST'");
         testObject.setAttribute("tenor_2", "TEST");
   
         LOG.info("Tester: Setting attribute tenor_2_amount to '5.2'");
         testObject.setAttribute("tenor_2_amount", "5.2");
   
         LOG.info("Tester: Setting attribute tenor_2_draft_number to 'TEST'");
         testObject.setAttribute("tenor_2_draft_number", "TEST");
   
         LOG.info("Tester: Setting attribute tenor_3 to 'TEST'");
         testObject.setAttribute("tenor_3", "TEST");
   
         LOG.info("Tester: Setting attribute tenor_3_amount to '5.2'");
         testObject.setAttribute("tenor_3_amount", "5.2");
   
         LOG.info("Tester: Setting attribute tenor_3_draft_number to 'TEST'");
         testObject.setAttribute("tenor_3_draft_number", "TEST");
   
         LOG.info("Tester: Setting attribute tenor_4 to 'TEST'");
         testObject.setAttribute("tenor_4", "TEST");
   
         LOG.info("Tester: Setting attribute tenor_4_amount to '5.2'");
         testObject.setAttribute("tenor_4_amount", "5.2");
   
         LOG.info("Tester: Setting attribute tenor_4_draft_number to 'TEST'");
         testObject.setAttribute("tenor_4_draft_number", "TEST");
   
         LOG.info("Tester: Setting attribute tenor_5 to 'TEST'");
         testObject.setAttribute("tenor_5", "TEST");
   
         LOG.info("Tester: Setting attribute tenor_5_amount to '5.2'");
         testObject.setAttribute("tenor_5_amount", "5.2");
   
         LOG.info("Tester: Setting attribute tenor_5_draft_number to 'TEST'");
         testObject.setAttribute("tenor_5_draft_number", "TEST");
   
         LOG.info("Tester: Setting attribute tenor_6 to 'TEST'");
         testObject.setAttribute("tenor_6", "TEST");
   
         LOG.info("Tester: Setting attribute tenor_6_amount to '5.2'");
         testObject.setAttribute("tenor_6_amount", "5.2");
   
         LOG.info("Tester: Setting attribute tenor_6_draft_number to 'TEST'");
         testObject.setAttribute("tenor_6_draft_number", "TEST");
   
         LOG.info("Tester: Setting attribute terms_source_type to 'O'");
         testObject.setAttribute("terms_source_type", "O");
   
         LOG.info("Tester: Setting attribute total_rate to '5.2'");
         testObject.setAttribute("total_rate", "5.2");
   
         LOG.info("Tester: Setting attribute tracer_details to 'TEST'");
         testObject.setAttribute("tracer_details", "TEST");
   
         LOG.info("Tester: Setting attribute tracer_send_type to 'O'");
         testObject.setAttribute("tracer_send_type", "O");
   
         LOG.info("Tester: Setting attribute transferrable to 'Y'");
         testObject.setAttribute("transferrable", "Y");
   
         LOG.info("Tester: Setting attribute ucp_details to 'TEST'");
         testObject.setAttribute("ucp_details", "TEST");
   
         LOG.info("Tester: Setting attribute ucp_version to 'O'");
         testObject.setAttribute("ucp_version", "O");
   
         LOG.info("Tester: Setting attribute ucp_version_details_ind to 'Y'");
         testObject.setAttribute("ucp_version_details_ind", "Y");
   
         LOG.info("Tester: Setting attribute urc_pub_num to 'TEST'");
         testObject.setAttribute("urc_pub_num", "TEST");
   
         LOG.info("Tester: Setting attribute urc_year to 'TEST'");
         testObject.setAttribute("urc_year", "TEST");
   
         LOG.info("Tester: Setting attribute usance_maturity_date to '02/28/2000 12:13:54'");
         testObject.setAttribute("usance_maturity_date", "02/28/2000 12:13:54");
   
         LOG.info("Tester: Setting attribute use_fec to 'Y'");
         testObject.setAttribute("use_fec", "Y");
   
         LOG.info("Tester: Setting attribute use_mkt_rate to 'Y'");
         testObject.setAttribute("use_mkt_rate", "Y");
   
         LOG.info("Tester: Setting attribute use_other to 'Y'");
         testObject.setAttribute("use_other", "Y");
   
         LOG.info("Tester: Setting attribute use_other_text to 'TEST'");
         testObject.setAttribute("use_other_text", "TEST");
   
         LOG.info("Tester: Setting attribute value_date to '02/28/2000 12:13:54'");
         testObject.setAttribute("value_date", "02/28/2000 12:13:54");
   
         LOG.info("Tester: Setting attribute work_item_number to 'TEST'");
         testObject.setAttribute("work_item_number", "TEST");
   
         LOG.info("Tester: Setting attribute work_item_priority to 'TEST'");
         testObject.setAttribute("work_item_priority", "TEST");
   
         LOG.info("Tester: Setting attribute financed_invoices_details_text to 'TEST'");
         testObject.setAttribute("financed_invoices_details_text", "TEST");
   
         LOG.info("Tester: Setting attribute financing_backed_by_buyer_ind to 'Y'");
         testObject.setAttribute("financing_backed_by_buyer_ind", "Y");
   
         LOG.info("Tester: Setting attribute funding_amount to '5.2'");
         testObject.setAttribute("funding_amount", "5.2");
   
         LOG.info("Tester: Setting attribute financed_pos_details_text to 'TEST'");
         testObject.setAttribute("financed_pos_details_text", "TEST");
   
         LOG.info("Tester: Setting attribute invoice_only_ind to 'Y'");
         testObject.setAttribute("invoice_only_ind", "Y");
   
         LOG.info("Tester: Setting attribute invoice_due_date to '02/28/2000 12:13:54'");
         testObject.setAttribute("invoice_due_date", "02/28/2000 12:13:54");
   
         LOG.info("Tester: Setting attribute unapplied_amount to '5.2'");
         testObject.setAttribute("unapplied_amount", "5.2");
   
         LOG.info("Tester: Setting attribute financing_allowed to 'Y'");
         testObject.setAttribute("financing_allowed", "Y");
   
         LOG.info("Tester: Setting attribute finance_percentage to '5.2'");
         testObject.setAttribute("finance_percentage", "5.2");
   
         LOG.info("Tester: Setting attribute equivalent_exch_amount to '5.2'");
         testObject.setAttribute("equivalent_exch_amount", "5.2");
   
         LOG.info("Tester: Setting attribute payment_settled_amount to '5.2'");
         testObject.setAttribute("payment_settled_amount", "5.2");
   
         LOG.info("Tester: Setting attribute equivalent_exch_amt_ccy to 'O'");
         testObject.setAttribute("equivalent_exch_amt_ccy", "O");
   
         LOG.info("Tester: Setting attribute transfer_fx_rate to '5.2'");
         testObject.setAttribute("transfer_fx_rate", "5.2");
   
         LOG.info("Tester: Setting attribute credit_account_oid to '7'");
         testObject.setAttribute("credit_account_oid", "7");
   
         LOG.info("Tester: Setting attribute debit_account_oid to '7'");
         testObject.setAttribute("debit_account_oid", "7");
   
         LOG.info("Tester: Setting attribute transfer_description to 'TEST'");
         testObject.setAttribute("transfer_description", "TEST");
   
         LOG.info("Tester: Setting attribute payment_charges_account_oid to '7'");
         testObject.setAttribute("payment_charges_account_oid", "7");
   
         LOG.info("Tester: Setting attribute debit_account_pending_amount to '5.2'");
         testObject.setAttribute("debit_account_pending_amount", "5.2");
   
         LOG.info("Tester: Setting attribute credit_account_pending_amount to '5.2'");
         testObject.setAttribute("credit_account_pending_amount", "5.2");
   

         /* Save the object to the database */
         LOG.info("Tester: Calling save() method");
         testObject.save();
         LOG.info("Tester: Save complete"); 
  
      /* If an Exception occurs, print out the call stack so we can determine where it came from */
      }catch(Exception e) {
         e.printStackTrace();

      /* Print out any errors that were issued by the Business Object */
      }finally {
         List<IssuedError> errors = testObject.getIssuedErrors();
         for(IssuedError errorObject : errors) 
         {
            
            LOG.info(errorObject.getErrorText());
         }
         testObject.remove();
      }
   }
}
