package com.ams.tradeportal.tester;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import com.ams.tradeportal.busobj.*;


/*
 * Client application to test the MailMessage Business Object.  Creates a new instance of the
 * object, sets attributes on it, and attempts to save it to the database.  The time required to execute
 * the test is calculated and written to "System.Out".  Multiple iterations of the test can be executed
 * by specifying a command line argument.
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class MailMessageTester
{
private static final Logger LOG = LoggerFactory.getLogger(MailMessageTester.class);

   public static void main (String[] argv) throws RemoteException, RemoveException, AmsException
   {
      /* Initialize the Object Server */
      LOG.info("Tester: Start of Test");
      try{
         ServerDeploy sd = null;
         LOG.info("Tester: Initializing Object Server");
         sd = (ServerDeploy)EJBObjectFactory.createClientEJB("t3://localhost:7001","ServerDeploy");  
         sd.init("t3://localhost:7001");
      }catch (Exception e) {}  
    
      /* Determine how many times to execute the test */
      int timesToExecute = new Integer(argv[0]).intValue();
      if (timesToExecute == 0) { timesToExecute = 1; }

      /* Start timing how long the test takes */
      ExecutionTimer timer = new ExecutionTimer();
      timer.setStart();

      /* Perform the test the number of times specified by the user */
      for(int i=0; !(i >= timesToExecute); i++)
         {executeTest();}
     
      /* Write out how long the test took. */
      timer.setEnd();
      LOG.info("Tester: End of Test");
      LOG.info("Tester: Execution Time per Call: " + timer.getTime() / timesToExecute);
   }


   protected static void executeTest() throws RemoteException, AmsException, RemoveException
   {
      MailMessage testObject = null;

      /* Test the basic functionaility of the MailMessage object.  This includes creating a
         new instance, setting attributes on it, and saving it to the database. */
      try{
         /* Create a new instance of the object */
         LOG.info("Tester: Creating instance of class MailMessage");
         testObject = (MailMessage)EJBObjectFactory.createClientEJB("t3://localhost:7001","MailMessage");
         LOG.info("Tester: Calling newObject() method");
         testObject.newObject();

         /* Set attrbutes on the object */
         LOG.info("Tester: Setting attribute message_text to 'TEST'");
         testObject.setAttribute("message_text", "TEST");
   
         LOG.info("Tester: Setting attribute message_subject to 'TEST'");
         testObject.setAttribute("message_subject", "TEST");
   
         LOG.info("Tester: Setting attribute discrepancy_flag to 'Y'");
         testObject.setAttribute("discrepancy_flag", "Y");
   
         LOG.info("Tester: Setting attribute message_source_type to 'O'");
         testObject.setAttribute("message_source_type", "O");
   
         LOG.info("Tester: Setting attribute message_status to 'O'");
         testObject.setAttribute("message_status", "O");
   
         LOG.info("Tester: Setting attribute unread_flag to 'Y'");
         testObject.setAttribute("unread_flag", "Y");
   
         LOG.info("Tester: Setting attribute last_update_date to '02/28/2000 12:13:54'");
         testObject.setAttribute("last_update_date", "02/28/2000 12:13:54");
   
         LOG.info("Tester: Setting attribute opt_lock to 'TEST'");
         testObject.setAttribute("opt_lock", "TEST");
   
         LOG.info("Tester: Setting attribute is_reply to 'Y'");
         testObject.setAttribute("is_reply", "Y");
   
         LOG.info("Tester: Setting attribute discrepancy_amount to '5.2'");
         testObject.setAttribute("discrepancy_amount", "5.2");
   
         LOG.info("Tester: Setting attribute discrepancy_currency_code to 'O'");
         testObject.setAttribute("discrepancy_currency_code", "O");
   
         LOG.info("Tester: Setting attribute sequence_number to 'TEST'");
         testObject.setAttribute("sequence_number", "TEST");
   
         LOG.info("Tester: Setting attribute work_item_number to 'TEST'");
         testObject.setAttribute("work_item_number", "TEST");
   
         LOG.info("Tester: Setting attribute response_transaction_oid to '7'");
         testObject.setAttribute("response_transaction_oid", "7");
   
         LOG.info("Tester: Setting association last_routed_by_user_oid to ''");
         testObject.setAttribute("last_routed_by_user_oid", "");
   
         LOG.info("Tester: Setting association assigned_to_user_oid to ''");
         testObject.setAttribute("assigned_to_user_oid", "");
   
         LOG.info("Tester: Setting association related_instrument_oid to ''");
         testObject.setAttribute("related_instrument_oid", "");
   
         LOG.info("Tester: Setting association assigned_to_corp_org_oid to ''");
         testObject.setAttribute("assigned_to_corp_org_oid", "");
   
         LOG.info("Tester: Setting association last_entry_user_oid to ''");
         testObject.setAttribute("last_entry_user_oid", "");
   

         /* Save the object to the database */
         LOG.info("Tester: Calling save() method");
         testObject.save();
         LOG.info("Tester: Save complete"); 
  
      /* If an Exception occurs, print out the call stack so we can determine where it came from */
      }catch(Exception e) {
         e.printStackTrace();

      /* Print out any errors that were issued by the Business Object */
      }finally {
         List<IssuedError> errors = testObject.getIssuedErrors();
         for(IssuedError errorObject : errors) 
         {
            
            LOG.info(errorObject.getErrorText());
         }
         testObject.remove();
      }
   }
}
