
  

package com.ams.tradeportal.tester;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import com.ams.tradeportal.busobj.*;


/*
 * Client application to test the BankOrganizationGroup Business Object.  Creates a new instance of the
 * object, sets attributes on it, and attempts to save it to the database.  The time required to execute
 * the test is calculated and written to "System.Out".  Multiple iterations of the test can be executed
 * by specifying a command line argument.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class BankOrganizationGroupTester
{
private static final Logger LOG = LoggerFactory.getLogger(BankOrganizationGroupTester.class);

   public static void main (String[] argv) throws RemoteException, RemoveException, AmsException
   {
      /* Initialize the Object Server */
      LOG.info("Tester: Start of Test");
      try{
         ServerDeploy sd = null;
         LOG.info("Tester: Initializing Object Server");
         sd = (ServerDeploy)EJBObjectFactory.createClientEJB("t3://localhost:7001","ServerDeploy");  
         sd.init("t3://localhost:7001");
      }catch (Exception e) {}  
    
      /* Determine how many times to execute the test */
      int timesToExecute = new Integer(argv[0]).intValue();
      if (timesToExecute == 0) { timesToExecute = 1; }

      /* Start timing how long the test takes */
      ExecutionTimer timer = new ExecutionTimer();
      timer.setStart();

      /* Perform the test the number of times specified by the user */
      for(int i=0; !(i >= timesToExecute); i++)
         {executeTest();}
     
      /* Write out how long the test took. */
      timer.setEnd();
      LOG.info("Tester: End of Test");
      LOG.info("Tester: Execution Time per Call: " + timer.getTime() / timesToExecute);
   }


   protected static void executeTest() throws RemoteException, AmsException, RemoveException
   {
      BankOrganizationGroup testObject = null;

      /* Test the basic functionaility of the BankOrganizationGroup object.  This includes creating a
         new instance, setting attributes on it, and saving it to the database. */
      try{
         /* Create a new instance of the object */
         LOG.info("Tester: Creating instance of class BankOrganizationGroup");
         testObject = (BankOrganizationGroup)EJBObjectFactory.createClientEJB("t3://localhost:7001","BankOrganizationGroup");
         LOG.info("Tester: Calling newObject() method");
         testObject.newObject();

         /* Set attrbutes on the object */
         LOG.info("Tester: Setting attribute name to 'TEST'");
         testObject.setAttribute("name", "TEST");
   
         LOG.info("Tester: Setting attribute activation_status to 'O'");
         testObject.setAttribute("activation_status", "O");
   
         LOG.info("Tester: Setting attribute date_deactivated to '02/28/2000 12:13:54'");
         testObject.setAttribute("date_deactivated", "02/28/2000 12:13:54");
   
         LOG.info("Tester: Setting attribute opt_lock to 'TEST'");
         testObject.setAttribute("opt_lock", "TEST");
   
         LOG.info("Tester: Setting association default_user_oid to ''");
         testObject.setAttribute("default_user_oid", "");
   
         LOG.info("Tester: Setting attribute description to 'TEST'");
         testObject.setAttribute("description", "TEST");
   
         LOG.info("Tester: Setting attribute branding_directory to 'TEST'");
         testObject.setAttribute("branding_directory", "TEST");
   
         LOG.info("Tester: Setting attribute support_information to 'TEST'");
         testObject.setAttribute("support_information", "TEST");
   
         LOG.info("Tester: Setting attribute sender_name to 'TEST'");
         testObject.setAttribute("sender_name", "TEST");
   
         LOG.info("Tester: Setting attribute sender_email_address to 'TEST'");
         testObject.setAttribute("sender_email_address", "TEST");
   
         LOG.info("Tester: Setting attribute system_name to 'TEST'");
         testObject.setAttribute("system_name", "TEST");
   
         LOG.info("Tester: Setting attribute bank_url to 'TEST'");
         testObject.setAttribute("bank_url", "TEST");
   
         LOG.info("Tester: Setting attribute email_language to 'O'");
         testObject.setAttribute("email_language", "O");
   
         LOG.info("Tester: Setting attribute email_language_2 to 'O'");
         testObject.setAttribute("email_language_2", "O");
   
         LOG.info("Tester: Setting attribute sender_name_2 to 'TEST'");
         testObject.setAttribute("sender_name_2", "TEST");
   
         LOG.info("Tester: Setting attribute do_not_reply_ind to 'Y'");
         testObject.setAttribute("do_not_reply_ind", "Y");
   
         LOG.info("Tester: Setting attribute sender_email_address_2 to 'TEST'");
         testObject.setAttribute("sender_email_address_2", "TEST");
   
         LOG.info("Tester: Setting attribute system_name_2 to 'TEST'");
         testObject.setAttribute("system_name_2", "TEST");
   
         LOG.info("Tester: Setting attribute bank_url_2 to 'TEST'");
         testObject.setAttribute("bank_url_2", "TEST");
   
         LOG.info("Tester: Setting attribute brand_button_ind to 'Y'");
         testObject.setAttribute("brand_button_ind", "Y");
   

         /* Save the object to the database */
         LOG.info("Tester: Calling save() method");
         testObject.save();
         LOG.info("Tester: Save complete"); 
  
      /* If an Exception occurs, print out the call stack so we can determine where it came from */
      }catch(Exception e) {
         e.printStackTrace();

      /* Print out any errors that were issued by the Business Object */
      }finally {
         List<IssuedError> errors = testObject.getIssuedErrors();
         for(IssuedError errorObject : errors) 
         {
            
            LOG.info(errorObject.getErrorText());
         }
         testObject.remove();
      }
   }
}
