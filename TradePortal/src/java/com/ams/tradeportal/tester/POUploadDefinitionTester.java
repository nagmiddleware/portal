
  

package com.ams.tradeportal.tester;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import com.ams.tradeportal.busobj.*;


/*
 * Client application to test the POUploadDefinition Business Object.  Creates a new instance of the
 * object, sets attributes on it, and attempts to save it to the database.  The time required to execute
 * the test is calculated and written to "System.Out".  Multiple iterations of the test can be executed
 * by specifying a command line argument.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class POUploadDefinitionTester
{
private static final Logger LOG = LoggerFactory.getLogger(POUploadDefinitionTester.class);

   public static void main (String[] argv) throws RemoteException, RemoveException, AmsException
   {
      /* Initialize the Object Server */
      LOG.info("Tester: Start of Test");
      try{
         ServerDeploy sd = null;
         LOG.info("Tester: Initializing Object Server");
         sd = (ServerDeploy)EJBObjectFactory.createClientEJB("t3://localhost:7001","ServerDeploy");  
         sd.init("t3://localhost:7001");
      }catch (Exception e) {}  
    
      /* Determine how many times to execute the test */
      int timesToExecute = new Integer(argv[0]).intValue();
      if (timesToExecute == 0) { timesToExecute = 1; }

      /* Start timing how long the test takes */
      ExecutionTimer timer = new ExecutionTimer();
      timer.setStart();

      /* Perform the test the number of times specified by the user */
      for(int i=0; !(i >= timesToExecute); i++)
         {executeTest();}
     
      /* Write out how long the test took. */
      timer.setEnd();
      LOG.info("Tester: End of Test");
      LOG.info("Tester: Execution Time per Call: " + timer.getTime() / timesToExecute);
   }


   protected static void executeTest() throws RemoteException, AmsException, RemoveException
   {
      POUploadDefinition testObject = null;

      /* Test the basic functionaility of the POUploadDefinition object.  This includes creating a
         new instance, setting attributes on it, and saving it to the database. */
      try{
         /* Create a new instance of the object */
         LOG.info("Tester: Creating instance of class POUploadDefinition");
         testObject = (POUploadDefinition)EJBObjectFactory.createClientEJB("t3://localhost:7001","POUploadDefinition");
         LOG.info("Tester: Calling newObject() method");
         testObject.newObject();

         /* Set attrbutes on the object */
         LOG.info("Tester: Setting attribute name to 'TEST'");
         testObject.setAttribute("name", "TEST");
   
         LOG.info("Tester: Setting attribute description to 'TEST'");
         testObject.setAttribute("description", "TEST");
   
         LOG.info("Tester: Setting attribute file_format to 'O'");
         testObject.setAttribute("file_format", "O");
   
         LOG.info("Tester: Setting attribute delimiter_char to 'O'");
         testObject.setAttribute("delimiter_char", "O");
   
         LOG.info("Tester: Setting attribute include_column_header to 'Y'");
         testObject.setAttribute("include_column_header", "Y");
   
         LOG.info("Tester: Setting attribute include_footer to 'Y'");
         testObject.setAttribute("include_footer", "Y");
   
         LOG.info("Tester: Setting attribute footer_line_1 to 'TEST'");
         testObject.setAttribute("footer_line_1", "TEST");
   
         LOG.info("Tester: Setting attribute footer_line_2 to 'TEST'");
         testObject.setAttribute("footer_line_2", "TEST");
   
         LOG.info("Tester: Setting attribute footer_line_3 to 'TEST'");
         testObject.setAttribute("footer_line_3", "TEST");
   
         LOG.info("Tester: Setting attribute po_num_field_name to 'TEST'");
         testObject.setAttribute("po_num_field_name", "TEST");
   
         LOG.info("Tester: Setting attribute po_num_size to '7'");
         testObject.setAttribute("po_num_size", "7");
   
         LOG.info("Tester: Setting attribute po_num_datatype to 'O'");
         testObject.setAttribute("po_num_datatype", "O");
   
         LOG.info("Tester: Setting attribute item_num_field_name to 'TEST'");
         testObject.setAttribute("item_num_field_name", "TEST");
   
         LOG.info("Tester: Setting attribute item_num_size to '7'");
         testObject.setAttribute("item_num_size", "7");
   
         LOG.info("Tester: Setting attribute item_num_datatype to 'O'");
         testObject.setAttribute("item_num_datatype", "O");
   
         LOG.info("Tester: Setting attribute ben_name_field_name to 'TEST'");
         testObject.setAttribute("ben_name_field_name", "TEST");
   
         LOG.info("Tester: Setting attribute ben_name_size to '7'");
         testObject.setAttribute("ben_name_size", "7");
   
         LOG.info("Tester: Setting attribute ben_name_datatype to 'O'");
         testObject.setAttribute("ben_name_datatype", "O");
   
         LOG.info("Tester: Setting attribute currency_field_name to 'TEST'");
         testObject.setAttribute("currency_field_name", "TEST");
   
         LOG.info("Tester: Setting attribute currency_size to '7'");
         testObject.setAttribute("currency_size", "7");
   
         LOG.info("Tester: Setting attribute currency_datatype to 'O'");
         testObject.setAttribute("currency_datatype", "O");
   
         LOG.info("Tester: Setting attribute amount_field_name to 'TEST'");
         testObject.setAttribute("amount_field_name", "TEST");
   
         LOG.info("Tester: Setting attribute amount_size to '7'");
         testObject.setAttribute("amount_size", "7");
   
         LOG.info("Tester: Setting attribute amount_datatype to 'O'");
         testObject.setAttribute("amount_datatype", "O");
   
         LOG.info("Tester: Setting attribute last_ship_dt_field_name to 'TEST'");
         testObject.setAttribute("last_ship_dt_field_name", "TEST");
   
         LOG.info("Tester: Setting attribute last_ship_dt_size to '7'");
         testObject.setAttribute("last_ship_dt_size", "7");
   
         LOG.info("Tester: Setting attribute last_ship_dt_datatype to 'O'");
         testObject.setAttribute("last_ship_dt_datatype", "O");
   
         LOG.info("Tester: Setting attribute other1_field_name to 'TEST'");
         testObject.setAttribute("other1_field_name", "TEST");
   
         LOG.info("Tester: Setting attribute other1_size to '7'");
         testObject.setAttribute("other1_size", "7");
   
         LOG.info("Tester: Setting attribute other1_datatype to 'O'");
         testObject.setAttribute("other1_datatype", "O");
   
         LOG.info("Tester: Setting attribute other2_field_name to 'TEST'");
         testObject.setAttribute("other2_field_name", "TEST");
   
         LOG.info("Tester: Setting attribute other2_size to '7'");
         testObject.setAttribute("other2_size", "7");
   
         LOG.info("Tester: Setting attribute other2_datatype to 'O'");
         testObject.setAttribute("other2_datatype", "O");
   
         LOG.info("Tester: Setting attribute other3_field_name to 'TEST'");
         testObject.setAttribute("other3_field_name", "TEST");
   
         LOG.info("Tester: Setting attribute other3_size to '7'");
         testObject.setAttribute("other3_size", "7");
   
         LOG.info("Tester: Setting attribute other3_datatype to 'O'");
         testObject.setAttribute("other3_datatype", "O");
   
         LOG.info("Tester: Setting attribute other4_field_name to 'TEST'");
         testObject.setAttribute("other4_field_name", "TEST");
   
         LOG.info("Tester: Setting attribute other4_size to '7'");
         testObject.setAttribute("other4_size", "7");
   
         LOG.info("Tester: Setting attribute other4_datatype to 'O'");
         testObject.setAttribute("other4_datatype", "O");
   
         LOG.info("Tester: Setting attribute other5_field_name to 'TEST'");
         testObject.setAttribute("other5_field_name", "TEST");
   
         LOG.info("Tester: Setting attribute other5_size to '7'");
         testObject.setAttribute("other5_size", "7");
   
         LOG.info("Tester: Setting attribute other5_datatype to 'O'");
         testObject.setAttribute("other5_datatype", "O");
   
         LOG.info("Tester: Setting attribute other6_field_name to 'TEST'");
         testObject.setAttribute("other6_field_name", "TEST");
   
         LOG.info("Tester: Setting attribute other6_size to '7'");
         testObject.setAttribute("other6_size", "7");
   
         LOG.info("Tester: Setting attribute other6_datatype to 'O'");
         testObject.setAttribute("other6_datatype", "O");
   
         LOG.info("Tester: Setting attribute other7_field_name to 'TEST'");
         testObject.setAttribute("other7_field_name", "TEST");
   
         LOG.info("Tester: Setting attribute other7_size to '7'");
         testObject.setAttribute("other7_size", "7");
   
         LOG.info("Tester: Setting attribute other7_datatype to 'O'");
         testObject.setAttribute("other7_datatype", "O");
   
         LOG.info("Tester: Setting attribute other8_field_name to 'TEST'");
         testObject.setAttribute("other8_field_name", "TEST");
   
         LOG.info("Tester: Setting attribute other8_size to '7'");
         testObject.setAttribute("other8_size", "7");
   
         LOG.info("Tester: Setting attribute other8_datatype to 'O'");
         testObject.setAttribute("other8_datatype", "O");
   
         LOG.info("Tester: Setting attribute other9_field_name to 'TEST'");
         testObject.setAttribute("other9_field_name", "TEST");
   
         LOG.info("Tester: Setting attribute other9_size to '7'");
         testObject.setAttribute("other9_size", "7");
   
         LOG.info("Tester: Setting attribute other9_datatype to 'O'");
         testObject.setAttribute("other9_datatype", "O");
   
         LOG.info("Tester: Setting attribute other10_field_name to 'TEST'");
         testObject.setAttribute("other10_field_name", "TEST");
   
         LOG.info("Tester: Setting attribute other10_size to '7'");
         testObject.setAttribute("other10_size", "7");
   
         LOG.info("Tester: Setting attribute other10_datatype to 'O'");
         testObject.setAttribute("other10_datatype", "O");
   
         LOG.info("Tester: Setting attribute other11_field_name to 'TEST'");
         testObject.setAttribute("other11_field_name", "TEST");
   
         LOG.info("Tester: Setting attribute other11_size to '7'");
         testObject.setAttribute("other11_size", "7");
   
         LOG.info("Tester: Setting attribute other11_datatype to 'O'");
         testObject.setAttribute("other11_datatype", "O");
   
         LOG.info("Tester: Setting attribute other12_field_name to 'TEST'");
         testObject.setAttribute("other12_field_name", "TEST");
   
         LOG.info("Tester: Setting attribute other12_size to '7'");
         testObject.setAttribute("other12_size", "7");
   
         LOG.info("Tester: Setting attribute other12_datatype to 'O'");
         testObject.setAttribute("other12_datatype", "O");
   
         LOG.info("Tester: Setting attribute other13_field_name to 'TEST'");
         testObject.setAttribute("other13_field_name", "TEST");
   
         LOG.info("Tester: Setting attribute other13_size to '7'");
         testObject.setAttribute("other13_size", "7");
   
         LOG.info("Tester: Setting attribute other13_datatype to 'O'");
         testObject.setAttribute("other13_datatype", "O");
   
         LOG.info("Tester: Setting attribute other14_field_name to 'TEST'");
         testObject.setAttribute("other14_field_name", "TEST");
   
         LOG.info("Tester: Setting attribute other14_size to '7'");
         testObject.setAttribute("other14_size", "7");
   
         LOG.info("Tester: Setting attribute other14_datatype to 'O'");
         testObject.setAttribute("other14_datatype", "O");
   
         LOG.info("Tester: Setting attribute other15_field_name to 'TEST'");
         testObject.setAttribute("other15_field_name", "TEST");
   
         LOG.info("Tester: Setting attribute other15_size to '7'");
         testObject.setAttribute("other15_size", "7");
   
         LOG.info("Tester: Setting attribute other15_datatype to 'O'");
         testObject.setAttribute("other15_datatype", "O");
   
         LOG.info("Tester: Setting attribute other16_field_name to 'TEST'");
         testObject.setAttribute("other16_field_name", "TEST");
   
         LOG.info("Tester: Setting attribute other16_size to '7'");
         testObject.setAttribute("other16_size", "7");
   
         LOG.info("Tester: Setting attribute other16_datatype to 'O'");
         testObject.setAttribute("other16_datatype", "O");
   
         LOG.info("Tester: Setting attribute other17_field_name to 'TEST'");
         testObject.setAttribute("other17_field_name", "TEST");
   
         LOG.info("Tester: Setting attribute other17_size to '7'");
         testObject.setAttribute("other17_size", "7");
   
         LOG.info("Tester: Setting attribute other17_datatype to 'O'");
         testObject.setAttribute("other17_datatype", "O");
   
         LOG.info("Tester: Setting attribute other18_field_name to 'TEST'");
         testObject.setAttribute("other18_field_name", "TEST");
   
         LOG.info("Tester: Setting attribute other18_size to '7'");
         testObject.setAttribute("other18_size", "7");
   
         LOG.info("Tester: Setting attribute other18_datatype to 'O'");
         testObject.setAttribute("other18_datatype", "O");
   
         LOG.info("Tester: Setting attribute other19_field_name to 'TEST'");
         testObject.setAttribute("other19_field_name", "TEST");
   
         LOG.info("Tester: Setting attribute other19_size to '7'");
         testObject.setAttribute("other19_size", "7");
   
         LOG.info("Tester: Setting attribute other19_datatype to 'O'");
         testObject.setAttribute("other19_datatype", "O");
   
         LOG.info("Tester: Setting attribute other20_field_name to 'TEST'");
         testObject.setAttribute("other20_field_name", "TEST");
   
         LOG.info("Tester: Setting attribute other20_size to '7'");
         testObject.setAttribute("other20_size", "7");
   
         LOG.info("Tester: Setting attribute other20_datatype to 'O'");
         testObject.setAttribute("other20_datatype", "O");
   
         LOG.info("Tester: Setting attribute other21_field_name to 'TEST'");
         testObject.setAttribute("other21_field_name", "TEST");
   
         LOG.info("Tester: Setting attribute other21_size to '7'");
         testObject.setAttribute("other21_size", "7");
   
         LOG.info("Tester: Setting attribute other21_datatype to 'O'");
         testObject.setAttribute("other21_datatype", "O");
   
         LOG.info("Tester: Setting attribute other22_field_name to 'TEST'");
         testObject.setAttribute("other22_field_name", "TEST");
   
         LOG.info("Tester: Setting attribute other22_size to '7'");
         testObject.setAttribute("other22_size", "7");
   
         LOG.info("Tester: Setting attribute other22_datatype to 'O'");
         testObject.setAttribute("other22_datatype", "O");
   
         LOG.info("Tester: Setting attribute other23_field_name to 'TEST'");
         testObject.setAttribute("other23_field_name", "TEST");
   
         LOG.info("Tester: Setting attribute other23_size to '7'");
         testObject.setAttribute("other23_size", "7");
   
         LOG.info("Tester: Setting attribute other23_datatype to 'O'");
         testObject.setAttribute("other23_datatype", "O");
   
         LOG.info("Tester: Setting attribute other24_field_name to 'TEST'");
         testObject.setAttribute("other24_field_name", "TEST");
   
         LOG.info("Tester: Setting attribute other24_size to '7'");
         testObject.setAttribute("other24_size", "7");
   
         LOG.info("Tester: Setting attribute other24_datatype to 'O'");
         testObject.setAttribute("other24_datatype", "O");
   
         LOG.info("Tester: Setting attribute upload_order_1 to 'O'");
         testObject.setAttribute("upload_order_1", "O");
   
         LOG.info("Tester: Setting attribute upload_order_2 to 'O'");
         testObject.setAttribute("upload_order_2", "O");
   
         LOG.info("Tester: Setting attribute upload_order_3 to 'O'");
         testObject.setAttribute("upload_order_3", "O");
   
         LOG.info("Tester: Setting attribute upload_order_4 to 'O'");
         testObject.setAttribute("upload_order_4", "O");
   
         LOG.info("Tester: Setting attribute upload_order_5 to 'O'");
         testObject.setAttribute("upload_order_5", "O");
   
         LOG.info("Tester: Setting attribute upload_order_6 to 'O'");
         testObject.setAttribute("upload_order_6", "O");
   
         LOG.info("Tester: Setting attribute upload_order_8 to 'O'");
         testObject.setAttribute("upload_order_8", "O");
   
         LOG.info("Tester: Setting attribute upload_order_7 to 'O'");
         testObject.setAttribute("upload_order_7", "O");
   
         LOG.info("Tester: Setting attribute upload_order_9 to 'O'");
         testObject.setAttribute("upload_order_9", "O");
   
         LOG.info("Tester: Setting attribute upload_order_10 to 'O'");
         testObject.setAttribute("upload_order_10", "O");
   
         LOG.info("Tester: Setting attribute upload_order_11 to 'O'");
         testObject.setAttribute("upload_order_11", "O");
   
         LOG.info("Tester: Setting attribute upload_order_12 to 'O'");
         testObject.setAttribute("upload_order_12", "O");
   
         LOG.info("Tester: Setting attribute upload_order_13 to 'O'");
         testObject.setAttribute("upload_order_13", "O");
   
         LOG.info("Tester: Setting attribute upload_order_14 to 'O'");
         testObject.setAttribute("upload_order_14", "O");
   
         LOG.info("Tester: Setting attribute upload_order_15 to 'O'");
         testObject.setAttribute("upload_order_15", "O");
   
         LOG.info("Tester: Setting attribute upload_order_16 to 'O'");
         testObject.setAttribute("upload_order_16", "O");
   
         LOG.info("Tester: Setting attribute upload_order_17 to 'O'");
         testObject.setAttribute("upload_order_17", "O");
   
         LOG.info("Tester: Setting attribute upload_order_18 to 'O'");
         testObject.setAttribute("upload_order_18", "O");
   
         LOG.info("Tester: Setting attribute upload_order_19 to 'O'");
         testObject.setAttribute("upload_order_19", "O");
   
         LOG.info("Tester: Setting attribute upload_order_20 to 'O'");
         testObject.setAttribute("upload_order_20", "O");
   
         LOG.info("Tester: Setting attribute upload_order_21 to 'O'");
         testObject.setAttribute("upload_order_21", "O");
   
         LOG.info("Tester: Setting attribute upload_order_22 to 'O'");
         testObject.setAttribute("upload_order_22", "O");
   
         LOG.info("Tester: Setting attribute upload_order_24 to 'O'");
         testObject.setAttribute("upload_order_24", "O");
   
         LOG.info("Tester: Setting attribute upload_order_25 to 'O'");
         testObject.setAttribute("upload_order_25", "O");
   
         LOG.info("Tester: Setting attribute upload_order_26 to 'O'");
         testObject.setAttribute("upload_order_26", "O");
   
         LOG.info("Tester: Setting attribute upload_order_27 to 'O'");
         testObject.setAttribute("upload_order_27", "O");
   
         LOG.info("Tester: Setting attribute upload_order_28 to 'O'");
         testObject.setAttribute("upload_order_28", "O");
   
         LOG.info("Tester: Setting attribute upload_order_29 to 'O'");
         testObject.setAttribute("upload_order_29", "O");
   
         LOG.info("Tester: Setting attribute upload_order_30 to 'O'");
         testObject.setAttribute("upload_order_30", "O");
   
         LOG.info("Tester: Setting attribute goods_descr_order_1 to 'O'");
         testObject.setAttribute("goods_descr_order_1", "O");
   
         LOG.info("Tester: Setting attribute goods_descr_order_2 to 'O'");
         testObject.setAttribute("goods_descr_order_2", "O");
   
         LOG.info("Tester: Setting attribute goods_descr_order_3 to 'O'");
         testObject.setAttribute("goods_descr_order_3", "O");
   
         LOG.info("Tester: Setting attribute goods_descr_order_4 to 'O'");
         testObject.setAttribute("goods_descr_order_4", "O");
   
         LOG.info("Tester: Setting attribute goods_descr_order_5 to 'O'");
         testObject.setAttribute("goods_descr_order_5", "O");
   
         LOG.info("Tester: Setting attribute goods_descr_order_6 to 'O'");
         testObject.setAttribute("goods_descr_order_6", "O");
   
         LOG.info("Tester: Setting attribute goods_descr_order_7 to 'O'");
         testObject.setAttribute("goods_descr_order_7", "O");
   
         LOG.info("Tester: Setting attribute goods_descr_order_8 to 'O'");
         testObject.setAttribute("goods_descr_order_8", "O");
   
         LOG.info("Tester: Setting attribute goods_descr_order_9 to 'O'");
         testObject.setAttribute("goods_descr_order_9", "O");
   
         LOG.info("Tester: Setting attribute goods_descr_order_10 to 'O'");
         testObject.setAttribute("goods_descr_order_10", "O");
   
         LOG.info("Tester: Setting attribute default_flag to 'Y'");
         testObject.setAttribute("default_flag", "Y");
   
         LOG.info("Tester: Setting attribute goods_descr_order_11 to 'O'");
         testObject.setAttribute("goods_descr_order_11", "O");
   
         LOG.info("Tester: Setting attribute goods_descr_order_12 to 'O'");
         testObject.setAttribute("goods_descr_order_12", "O");
   
         LOG.info("Tester: Setting attribute goods_descr_order_13 to 'O'");
         testObject.setAttribute("goods_descr_order_13", "O");
   
         LOG.info("Tester: Setting attribute goods_descr_order_14 to 'O'");
         testObject.setAttribute("goods_descr_order_14", "O");
   
         LOG.info("Tester: Setting attribute goods_descr_order_15 to 'O'");
         testObject.setAttribute("goods_descr_order_15", "O");
   
         LOG.info("Tester: Setting attribute goods_descr_order_16 to 'O'");
         testObject.setAttribute("goods_descr_order_16", "O");
   
         LOG.info("Tester: Setting attribute goods_descr_order_17 to 'O'");
         testObject.setAttribute("goods_descr_order_17", "O");
   
         LOG.info("Tester: Setting attribute goods_descr_order_18 to 'O'");
         testObject.setAttribute("goods_descr_order_18", "O");
   
         LOG.info("Tester: Setting attribute goods_descr_order_19 to 'O'");
         testObject.setAttribute("goods_descr_order_19", "O");
   
         LOG.info("Tester: Setting attribute goods_descr_order_20 to 'O'");
         testObject.setAttribute("goods_descr_order_20", "O");
   
         LOG.info("Tester: Setting attribute goods_descr_order_21 to 'O'");
         testObject.setAttribute("goods_descr_order_21", "O");
   
         LOG.info("Tester: Setting attribute goods_descr_order_22 to 'O'");
         testObject.setAttribute("goods_descr_order_22", "O");
   
         LOG.info("Tester: Setting attribute goods_descr_order_23 to 'O'");
         testObject.setAttribute("goods_descr_order_23", "O");
   
         LOG.info("Tester: Setting attribute goods_descr_order_24 to 'O'");
         testObject.setAttribute("goods_descr_order_24", "O");
   
         LOG.info("Tester: Setting attribute goods_descr_order_25 to 'O'");
         testObject.setAttribute("goods_descr_order_25", "O");
   
         LOG.info("Tester: Setting attribute goods_descr_order_26 to 'O'");
         testObject.setAttribute("goods_descr_order_26", "O");
   
         LOG.info("Tester: Setting attribute goods_descr_order_27 to 'O'");
         testObject.setAttribute("goods_descr_order_27", "O");
   
         LOG.info("Tester: Setting attribute goods_descr_order_28 to 'O'");
         testObject.setAttribute("goods_descr_order_28", "O");
   
         LOG.info("Tester: Setting attribute goods_descr_order_29 to 'O'");
         testObject.setAttribute("goods_descr_order_29", "O");
   
         LOG.info("Tester: Setting attribute goods_descr_order_30 to 'O'");
         testObject.setAttribute("goods_descr_order_30", "O");
   
         LOG.info("Tester: Setting attribute part_to_validate to 'TEST'");
         testObject.setAttribute("part_to_validate", "TEST");
   
         LOG.info("Tester: Setting attribute opt_lock to 'TEST'");
         testObject.setAttribute("opt_lock", "TEST");
   
         LOG.info("Tester: Setting attribute upload_order_23 to 'O'");
         testObject.setAttribute("upload_order_23", "O");
   
         LOG.info("Tester: Setting attribute po_text_datatype to 'O'");
         testObject.setAttribute("po_text_datatype", "O");
   
         LOG.info("Tester: Setting attribute po_text_field_name to 'TEST'");
         testObject.setAttribute("po_text_field_name", "TEST");
   
         LOG.info("Tester: Setting attribute po_text_size to '7'");
         testObject.setAttribute("po_text_size", "7");
   
         LOG.info("Tester: Setting attribute date_format to 'O'");
         testObject.setAttribute("date_format", "O");
   
         LOG.info("Tester: Setting attribute include_po_text to 'Y'");
         testObject.setAttribute("include_po_text", "Y");
   
         LOG.info("Tester: Setting attribute definition_type to 'O'");
         testObject.setAttribute("definition_type", "O");
   
         LOG.info("Tester: Setting attribute upload_order_31 to 'O'");
         testObject.setAttribute("upload_order_31", "O");
   
         LOG.info("Tester: Setting association owner_org_oid to ''");
         testObject.setAttribute("owner_org_oid", "");
   

         /* Save the object to the database */
         LOG.info("Tester: Calling save() method");
         testObject.save();
         LOG.info("Tester: Save complete"); 
  
      /* If an Exception occurs, print out the call stack so we can determine where it came from */
      }catch(Exception e) {
         e.printStackTrace();

      /* Print out any errors that were issued by the Business Object */
      }finally {
         List<IssuedError> errors = testObject.getIssuedErrors();
         for(IssuedError errorObject : errors) 
         {
            
            LOG.info(errorObject.getErrorText());
         }
         testObject.remove();
      }
   }
}
