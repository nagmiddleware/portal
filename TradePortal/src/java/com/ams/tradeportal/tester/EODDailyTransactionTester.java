
  

package com.ams.tradeportal.tester;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import com.ams.tradeportal.busobj.*;


/*
 * Client application to test the EODDailyTransaction Business Object.  Creates a new instance of the
 * object, sets attributes on it, and attempts to save it to the database.  The time required to execute
 * the test is calculated and written to "System.Out".  Multiple iterations of the test can be executed
 * by specifying a command line argument.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class EODDailyTransactionTester
{
private static final Logger LOG = LoggerFactory.getLogger(EODDailyTransactionTester.class);

   public static void main (String[] argv) throws RemoteException, RemoveException, AmsException
   {
      /* Initialize the Object Server */
      LOG.info("Tester: Start of Test");
      try{
         ServerDeploy sd = null;
         LOG.info("Tester: Initializing Object Server");
         sd = (ServerDeploy)EJBObjectFactory.createClientEJB("t3://localhost:7001","ServerDeploy");  
         sd.init("t3://localhost:7001");
      }catch (Exception e) {}  
    
      /* Determine how many times to execute the test */
      int timesToExecute = new Integer(argv[0]).intValue();
      if (timesToExecute == 0) { timesToExecute = 1; }

      /* Start timing how long the test takes */
      ExecutionTimer timer = new ExecutionTimer();
      timer.setStart();

      /* Perform the test the number of times specified by the user */
      for(int i=0; !(i >= timesToExecute); i++)
         {executeTest();}
     
      /* Write out how long the test took. */
      timer.setEnd();
      LOG.info("Tester: End of Test");
      LOG.info("Tester: Execution Time per Call: " + timer.getTime() / timesToExecute);
   }


   protected static void executeTest() throws RemoteException, AmsException, RemoveException
   {
      EODDailyTransaction testObject = null;

      /* Test the basic functionaility of the EODDailyTransaction object.  This includes creating a
         new instance, setting attributes on it, and saving it to the database. */
      try{
         /* Create a new instance of the object */
         LOG.info("Tester: Creating instance of class EODDailyTransaction");
         testObject = (EODDailyTransaction)EJBObjectFactory.createClientEJB("t3://localhost:7001","EODDailyTransaction");
         LOG.info("Tester: Calling newObject() method");
         testObject.newObject();

         /* Set attrbutes on the object */
         LOG.info("Tester: Setting attribute transaction_date to '02/28/2000 12:13:54'");
         testObject.setAttribute("transaction_date", "02/28/2000 12:13:54");
   
         LOG.info("Tester: Setting attribute transaction_type to 'TEST'");
         testObject.setAttribute("transaction_type", "TEST");
   
         LOG.info("Tester: Setting attribute transaction_reference to 'TEST'");
         testObject.setAttribute("transaction_reference", "TEST");
   
         LOG.info("Tester: Setting attribute transaction_amount to '5.2'");
         testObject.setAttribute("transaction_amount", "5.2");
   
         LOG.info("Tester: Setting attribute transaction_narrative to 'TEST'");
         testObject.setAttribute("transaction_narrative", "TEST");
   
         LOG.info("Tester: Setting attribute effective_date to '02/28/2000 12:13:54'");
         testObject.setAttribute("effective_date", "02/28/2000 12:13:54");
   
         LOG.info("Tester: Setting attribute trace_id to 'TEST'");
         testObject.setAttribute("trace_id", "TEST");
   
         LOG.info("Tester: Setting attribute transaction_code to 'TEST'");
         testObject.setAttribute("transaction_code", "TEST");
   
         LOG.info("Tester: Setting attribute auxiliary_dom to 'TEST'");
         testObject.setAttribute("auxiliary_dom", "TEST");
   
         LOG.info("Tester: Setting attribute ex_auxiliary_dom to 'TEST'");
         testObject.setAttribute("ex_auxiliary_dom", "TEST");
   
         LOG.info("Tester: Setting attribute bai_code to 'TEST'");
         testObject.setAttribute("bai_code", "TEST");
   
         LOG.info("Tester: Setting attribute remitter_name to 'TEST'");
         testObject.setAttribute("remitter_name", "TEST");
   
         LOG.info("Tester: Setting attribute lodgment_reference to 'TEST'");
         testObject.setAttribute("lodgment_reference", "TEST");
   
         LOG.info("Tester: Setting attribute short_description to 'TEST'");
         testObject.setAttribute("short_description", "TEST");
   
         LOG.info("Tester: Setting attribute number_collection_items to '7'");
         testObject.setAttribute("number_collection_items", "7");
   
         LOG.info("Tester: Setting attribute sequence_number to '7'");
         testObject.setAttribute("sequence_number", "7");
   

         /* Save the object to the database */
         LOG.info("Tester: Calling save() method");
         testObject.save();
         LOG.info("Tester: Save complete"); 
  
      /* If an Exception occurs, print out the call stack so we can determine where it came from */
      }catch(Exception e) {
         e.printStackTrace();

      /* Print out any errors that were issued by the Business Object */
      }finally {
         List<IssuedError> errors = testObject.getIssuedErrors();
         for(IssuedError errorObject : errors) 
         {
            
            LOG.info(errorObject.getErrorText());
         }
         testObject.remove();
      }
   }
}
