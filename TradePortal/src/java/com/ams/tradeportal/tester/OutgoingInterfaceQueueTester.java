package com.ams.tradeportal.tester;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import com.ams.tradeportal.busobj.*;


/*
 * Client application to test the OutgoingInterfaceQueue Business Object.  Creates a new instance of the
 * object, sets attributes on it, and attempts to save it to the database.  The time required to execute
 * the test is calculated and written to "System.Out".  Multiple iterations of the test can be executed
 * by specifying a command line argument.
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class OutgoingInterfaceQueueTester
{
private static final Logger LOG = LoggerFactory.getLogger(OutgoingInterfaceQueueTester.class);

   public static void main (String[] argv) throws RemoteException, RemoveException, AmsException
   {
      /* Initialize the Object Server */
      LOG.info("Tester: Start of Test");
      try{
         ServerDeploy sd = null;
         LOG.info("Tester: Initializing Object Server");
         sd = (ServerDeploy)EJBObjectFactory.createClientEJB("t3://localhost:7001","ServerDeploy");  
         sd.init("t3://localhost:7001");
      }catch (Exception e) {}  
    
      /* Determine how many times to execute the test */
      int timesToExecute = new Integer(argv[0]).intValue();
      if (timesToExecute == 0) { timesToExecute = 1; }

      /* Start timing how long the test takes */
      ExecutionTimer timer = new ExecutionTimer();
      timer.setStart();

      /* Perform the test the number of times specified by the user */
      for(int i=0; !(i >= timesToExecute); i++)
         {executeTest();}
     
      /* Write out how long the test took. */
      timer.setEnd();
      LOG.info("Tester: End of Test");
      LOG.info("Tester: Execution Time per Call: " + timer.getTime() / timesToExecute);
   }


   protected static void executeTest() throws RemoteException, AmsException, RemoveException
   {
      OutgoingInterfaceQueue testObject = null;

      /* Test the basic functionaility of the OutgoingInterfaceQueue object.  This includes creating a
         new instance, setting attributes on it, and saving it to the database. */
      try{
         /* Create a new instance of the object */
         LOG.info("Tester: Creating instance of class OutgoingInterfaceQueue");
         testObject = (OutgoingInterfaceQueue)EJBObjectFactory.createClientEJB("t3://localhost:7001","OutgoingInterfaceQueue");
         LOG.info("Tester: Calling newObject() method");
         testObject.newObject();

         /* Set attrbutes on the object */
         LOG.info("Tester: Setting attribute date_created to '02/28/2000 12:13:54'");
         testObject.setAttribute("date_created", "02/28/2000 12:13:54");
   
         LOG.info("Tester: Setting attribute date_sent to '02/28/2000 12:13:54'");
         testObject.setAttribute("date_sent", "02/28/2000 12:13:54");
   
         LOG.info("Tester: Setting attribute status to 'O'");
         testObject.setAttribute("status", "O");
   
         LOG.info("Tester: Setting attribute confirm_status to 'O'");
         testObject.setAttribute("confirm_status", "O");
   
         LOG.info("Tester: Setting attribute msg_text to 'TEST'");
         testObject.setAttribute("msg_text", "TEST");
   
         LOG.info("Tester: Setting attribute packaging_error_text to 'TEST'");
         testObject.setAttribute("packaging_error_text", "TEST");
   
         LOG.info("Tester: Setting attribute unpackaging_error_text to 'TEST'");
         testObject.setAttribute("unpackaging_error_text", "TEST");
   
         LOG.info("Tester: Setting attribute msg_type to 'O'");
         testObject.setAttribute("msg_type", "O");
   
         LOG.info("Tester: Setting attribute message_id to 'TEST'");
         testObject.setAttribute("message_id", "TEST");
   
         LOG.info("Tester: Setting attribute reply_to_message_id to 'TEST'");
         testObject.setAttribute("reply_to_message_id", "TEST");
   
         LOG.info("Tester: Setting attribute agent_id to 'TEST'");
         testObject.setAttribute("agent_id", "TEST");
   
         LOG.info("Tester: Setting attribute mail_message_oid to '7'");
         testObject.setAttribute("mail_message_oid", "7");
   
         LOG.info("Tester: Setting association instrument_oid to ''");
         testObject.setAttribute("instrument_oid", "");
   
         LOG.info("Tester: Setting association transaction_oid to ''");
         testObject.setAttribute("transaction_oid", "");
   

         /* Save the object to the database */
         LOG.info("Tester: Calling save() method");
         testObject.save();
         LOG.info("Tester: Save complete"); 
  
      /* If an Exception occurs, print out the call stack so we can determine where it came from */
      }catch(Exception e) {
         e.printStackTrace();

      /* Print out any errors that were issued by the Business Object */
      }finally {
         List<IssuedError> errors = testObject.getIssuedErrors();
         for(IssuedError errorObject : errors) 
         {
            
            LOG.info(errorObject.getErrorText());
         }
         testObject.remove();
      }
   }
}
