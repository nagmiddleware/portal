
  

package com.ams.tradeportal.tester;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import com.ams.tradeportal.busobj.*;


/*
 * Client application to test the BankBranchRule Business Object.  Creates a new instance of the
 * object, sets attributes on it, and attempts to save it to the database.  The time required to execute
 * the test is calculated and written to "System.Out".  Multiple iterations of the test can be executed
 * by specifying a command line argument.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class BankBranchRuleTester
{
private static final Logger LOG = LoggerFactory.getLogger(BankBranchRuleTester.class);

   public static void main (String[] argv) throws RemoteException, RemoveException, AmsException
   {
      /* Initialize the Object Server */
      LOG.info("Tester: Start of Test");
      try{
         ServerDeploy sd = null;
         LOG.info("Tester: Initializing Object Server");
         sd = (ServerDeploy)EJBObjectFactory.createClientEJB("t3://localhost:7001","ServerDeploy");  
         sd.init("t3://localhost:7001");
      }catch (Exception e) {}  
    
      /* Determine how many times to execute the test */
      int timesToExecute = new Integer(argv[0]).intValue();
      if (timesToExecute == 0) { timesToExecute = 1; }

      /* Start timing how long the test takes */
      ExecutionTimer timer = new ExecutionTimer();
      timer.setStart();

      /* Perform the test the number of times specified by the user */
      for(int i=0; !(i >= timesToExecute); i++)
         {executeTest();}
     
      /* Write out how long the test took. */
      timer.setEnd();
      LOG.info("Tester: End of Test");
      LOG.info("Tester: Execution Time per Call: " + timer.getTime() / timesToExecute);
   }


   protected static void executeTest() throws RemoteException, AmsException, RemoveException
   {
      BankBranchRule testObject = null;

      /* Test the basic functionaility of the BankBranchRule object.  This includes creating a
         new instance, setting attributes on it, and saving it to the database. */
      try{
         /* Create a new instance of the object */
         LOG.info("Tester: Creating instance of class BankBranchRule");
         testObject = (BankBranchRule)EJBObjectFactory.createClientEJB("t3://localhost:7001","BankBranchRule");
         LOG.info("Tester: Calling newObject() method");
         testObject.newObject();

         /* Set attrbutes on the object */
         LOG.info("Tester: Setting attribute bank_branch_code to 'TEST'");
         testObject.setAttribute("bank_branch_code", "TEST");
   
         LOG.info("Tester: Setting attribute bank_name to 'TEST'");
         testObject.setAttribute("bank_name", "TEST");
   
         LOG.info("Tester: Setting attribute branch_name to 'TEST'");
         testObject.setAttribute("branch_name", "TEST");
   
         LOG.info("Tester: Setting attribute address_line_1 to 'TEST'");
         testObject.setAttribute("address_line_1", "TEST");
   
         LOG.info("Tester: Setting attribute address_line_2 to 'TEST'");
         testObject.setAttribute("address_line_2", "TEST");
   
         LOG.info("Tester: Setting attribute opt_lock to 'TEST'");
         testObject.setAttribute("opt_lock", "TEST");
   
         LOG.info("Tester: Setting attribute payment_method to 'O'");
         testObject.setAttribute("payment_method", "O");
   
         LOG.info("Tester: Setting attribute address_city to 'TEST'");
         testObject.setAttribute("address_city", "TEST");
   
         LOG.info("Tester: Setting attribute address_state_province to 'TEST'");
         testObject.setAttribute("address_state_province", "TEST");
   
         LOG.info("Tester: Setting attribute address_country to 'O'");
         testObject.setAttribute("address_country", "O");
   

         /* Save the object to the database */
         LOG.info("Tester: Calling save() method");
         testObject.save();
         LOG.info("Tester: Save complete"); 
  
      /* If an Exception occurs, print out the call stack so we can determine where it came from */
      }catch(Exception e) {
         e.printStackTrace();

      /* Print out any errors that were issued by the Business Object */
      }finally {
         List<IssuedError> errors = testObject.getIssuedErrors();
         for(IssuedError errorObject : errors) 
         {
            
            LOG.info(errorObject.getErrorText());
         }
         testObject.remove();
      }
   }
}
