
  

package com.ams.tradeportal.tester;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import com.ams.tradeportal.busobj.*;


/*
 * Client application to test the ClientBank Business Object.  Creates a new instance of the
 * object, sets attributes on it, and attempts to save it to the database.  The time required to execute
 * the test is calculated and written to "System.Out".  Multiple iterations of the test can be executed
 * by specifying a command line argument.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class ClientBankTester
{
private static final Logger LOG = LoggerFactory.getLogger(ClientBankTester.class);

   public static void main (String[] argv) throws RemoteException, RemoveException, AmsException
   {
      /* Initialize the Object Server */
      LOG.info("Tester: Start of Test");
      try{
         ServerDeploy sd = null;
         LOG.info("Tester: Initializing Object Server");
         sd = (ServerDeploy)EJBObjectFactory.createClientEJB("t3://localhost:7001","ServerDeploy");  
         sd.init("t3://localhost:7001");
      }catch (Exception e) {}  
    
      /* Determine how many times to execute the test */
      int timesToExecute = new Integer(argv[0]).intValue();
      if (timesToExecute == 0) { timesToExecute = 1; }

      /* Start timing how long the test takes */
      ExecutionTimer timer = new ExecutionTimer();
      timer.setStart();

      /* Perform the test the number of times specified by the user */
      for(int i=0; !(i >= timesToExecute); i++)
         {executeTest();}
     
      /* Write out how long the test took. */
      timer.setEnd();
      LOG.info("Tester: End of Test");
      LOG.info("Tester: Execution Time per Call: " + timer.getTime() / timesToExecute);
   }


   protected static void executeTest() throws RemoteException, AmsException, RemoveException
   {
      ClientBank testObject = null;

      /* Test the basic functionaility of the ClientBank object.  This includes creating a
         new instance, setting attributes on it, and saving it to the database. */
      try{
         /* Create a new instance of the object */
         LOG.info("Tester: Creating instance of class ClientBank");
         testObject = (ClientBank)EJBObjectFactory.createClientEJB("t3://localhost:7001","ClientBank");
         LOG.info("Tester: Calling newObject() method");
         testObject.newObject();

         /* Set attrbutes on the object */
         LOG.info("Tester: Setting attribute name to 'TEST'");
         testObject.setAttribute("name", "TEST");
   
         LOG.info("Tester: Setting attribute activation_status to 'O'");
         testObject.setAttribute("activation_status", "O");
   
         LOG.info("Tester: Setting attribute date_deactivated to '02/28/2000 12:13:54'");
         testObject.setAttribute("date_deactivated", "02/28/2000 12:13:54");
   
         LOG.info("Tester: Setting attribute opt_lock to 'TEST'");
         testObject.setAttribute("opt_lock", "TEST");
   
         LOG.info("Tester: Setting association default_user_oid to ''");
         testObject.setAttribute("default_user_oid", "");
   
         LOG.info("Tester: Setting attribute instr_id_range_start to '7'");
         testObject.setAttribute("instr_id_range_start", "7");
   
         LOG.info("Tester: Setting attribute instr_id_range_end to '7'");
         testObject.setAttribute("instr_id_range_end", "7");
   
         LOG.info("Tester: Setting attribute branding_directory to 'TEST'");
         testObject.setAttribute("branding_directory", "TEST");
   
         LOG.info("Tester: Setting attribute bank_min_password_len to '7'");
         testObject.setAttribute("bank_min_password_len", "7");
   
         LOG.info("Tester: Setting attribute bank_change_password_days to '7'");
         testObject.setAttribute("bank_change_password_days", "7");
   
         LOG.info("Tester: Setting attribute bank_max_failed_attempts to '7'");
         testObject.setAttribute("bank_max_failed_attempts", "7");
   
         LOG.info("Tester: Setting attribute corp_min_password_length to '7'");
         testObject.setAttribute("corp_min_password_length", "7");
   
         LOG.info("Tester: Setting attribute corp_change_password_days to '7'");
         testObject.setAttribute("corp_change_password_days", "7");
   
         LOG.info("Tester: Setting attribute corp_max_failed_attempts to '7'");
         testObject.setAttribute("corp_max_failed_attempts", "7");
   
         LOG.info("Tester: Setting attribute fax_1 to 'TEST'");
         testObject.setAttribute("fax_1", "TEST");
   
         LOG.info("Tester: Setting attribute telex_1 to 'TEST'");
         testObject.setAttribute("telex_1", "TEST");
   
         LOG.info("Tester: Setting attribute telex_answer_back_1 to 'TEST'");
         testObject.setAttribute("telex_answer_back_1", "TEST");
   
         LOG.info("Tester: Setting attribute fax_2 to 'TEST'");
         testObject.setAttribute("fax_2", "TEST");
   
         LOG.info("Tester: Setting attribute swift_address_part1 to 'TEST'");
         testObject.setAttribute("swift_address_part1", "TEST");
   
         LOG.info("Tester: Setting attribute swift_address_part2 to 'TEST'");
         testObject.setAttribute("swift_address_part2", "TEST");
   
         LOG.info("Tester: Setting attribute phone_number to 'TEST'");
         testObject.setAttribute("phone_number", "TEST");
   
         LOG.info("Tester: Setting attribute address_city to 'TEST'");
         testObject.setAttribute("address_city", "TEST");
   
         LOG.info("Tester: Setting attribute address_country to 'O'");
         testObject.setAttribute("address_country", "O");
   
         LOG.info("Tester: Setting attribute address_line_1 to 'TEST'");
         testObject.setAttribute("address_line_1", "TEST");
   
         LOG.info("Tester: Setting attribute address_line_2 to 'TEST'");
         testObject.setAttribute("address_line_2", "TEST");
   
         LOG.info("Tester: Setting attribute address_postal_code to 'TEST'");
         testObject.setAttribute("address_postal_code", "TEST");
   
         LOG.info("Tester: Setting attribute address_state_province to 'TEST'");
         testObject.setAttribute("address_state_province", "TEST");
   
         LOG.info("Tester: Setting attribute OTL_id to 'TEST'");
         testObject.setAttribute("OTL_id", "TEST");
   
         LOG.info("Tester: Setting attribute guar_def_tmplt_oid to '7'");
         testObject.setAttribute("guar_def_tmplt_oid", "7");
   
         LOG.info("Tester: Setting attribute export_coll_def_tmplt_oid to '7'");
         testObject.setAttribute("export_coll_def_tmplt_oid", "7");
   
         /* Vasavi CR 524 03/31/2010 Begin */
         LOG.info("Tester: Setting attribute new_export_coll_def_tmplt_oid to '7'");
         testObject.setAttribute("new_export_coll_def_tmplt_oid", "7");
         /* Vasavi CR 524 03/31/2010 End */
   
         LOG.info("Tester: Setting attribute shipping_guar_def_tmplt_oid to '7'");
         testObject.setAttribute("shipping_guar_def_tmplt_oid", "7");
   
         LOG.info("Tester: Setting attribute air_waybill_def_tmplt_oid to '7'");
         testObject.setAttribute("air_waybill_def_tmplt_oid", "7");
   
         LOG.info("Tester: Setting attribute standby_lc_def_tmplt_oid to '7'");
         testObject.setAttribute("standby_lc_def_tmplt_oid", "7");
   
         LOG.info("Tester: Setting attribute loan_req_def_tmplt_oid to '7'");
         testObject.setAttribute("loan_req_def_tmplt_oid", "7");
   
         LOG.info("Tester: Setting attribute funds_transfer_def_tmplt_oid to '7'");
         testObject.setAttribute("funds_transfer_def_tmplt_oid", "7");
   
         LOG.info("Tester: Setting attribute import_lc_def_tmplt_oid to '7'");
         testObject.setAttribute("import_lc_def_tmplt_oid", "7");
   
         LOG.info("Tester: Setting attribute authentication_method to 'O'");
         testObject.setAttribute("authentication_method", "O");
   
         LOG.info("Tester: Setting attribute verify_logon_digital_sig to 'Y'");
         testObject.setAttribute("verify_logon_digital_sig", "Y");
   
         LOG.info("Tester: Setting attribute require_certs_loan_and_funds to 'Y'");
         testObject.setAttribute("require_certs_loan_and_funds", "Y");
   
         LOG.info("Tester: Setting attribute bank_password_addl_criteria to 'Y'");
         testObject.setAttribute("bank_password_addl_criteria", "Y");
   
         LOG.info("Tester: Setting attribute corp_password_addl_criteria to 'Y'");
         testObject.setAttribute("corp_password_addl_criteria", "Y");
   
         LOG.info("Tester: Setting attribute bank_password_history to 'Y'");
         testObject.setAttribute("bank_password_history", "Y");
   
         LOG.info("Tester: Setting attribute corp_password_history to 'Y'");
         testObject.setAttribute("corp_password_history", "Y");
   
         LOG.info("Tester: Setting attribute bank_password_history_count to '7'");
         testObject.setAttribute("bank_password_history_count", "7");
   
         LOG.info("Tester: Setting attribute corp_password_history_count to '7'");
         testObject.setAttribute("corp_password_history_count", "7");
   
         LOG.info("Tester: Setting attribute instrument_purge_type to 'O'");
         testObject.setAttribute("instrument_purge_type", "O");
   
         LOG.info("Tester: Setting attribute brand_button_ind to 'Y'");
         testObject.setAttribute("brand_button_ind", "Y");
   
         LOG.info("Tester: Setting attribute override_swift_length_ind to 'Y'");
         testObject.setAttribute("override_swift_length_ind", "Y");
   
         LOG.info("Tester: Setting attribute request_advise_def_tmplt_oid to '7'");
         testObject.setAttribute("request_advise_def_tmplt_oid", "7");
   
         LOG.info("Tester: Setting attribute approval_to_pay_def_tmplt_oid to '7'");
         testObject.setAttribute("approval_to_pay_def_tmplt_oid", "7");
   
         LOG.info("Tester: Setting attribute corp_auth_method to 'O'");
         testObject.setAttribute("corp_auth_method", "O");
   
         LOG.info("Tester: Setting attribute doc_prep_url to 'TEST'");
         testObject.setAttribute("doc_prep_url", "TEST");
   
         LOG.info("Tester: Setting attribute require_certs_fund_final_auth to 'Y'");
         testObject.setAttribute("require_certs_fund_final_auth", "Y");
   
         LOG.info("Tester: Setting attribute xfer_bet_accts_def_tmplt_oid to '7'");
         testObject.setAttribute("xfer_bet_accts_def_tmplt_oid", "7");
   
         LOG.info("Tester: Setting attribute domestic_payment_def_tmplt_oid to '7'");
         testObject.setAttribute("domestic_payment_def_tmplt_oid", "7");
   
         LOG.info("Tester: Setting attribute authorization_cert_auth_URL to 'TEST'");
         testObject.setAttribute("authorization_cert_auth_URL", "TEST");
   
         //BSL 09/07/11 CR663 Rel 7.1 Begin
         LOG.info("Tester: Setting attribute registration_header to 'TEST'");
         testObject.setAttribute("registration_header", "TEST");
   
         LOG.info("Tester: Setting attribute registration_text to 'TEST'");
         testObject.setAttribute("registration_text", "TEST");
         //BSL 09/07/11 CR663 Rel 7.1 End

         /* Save the object to the database */
         LOG.info("Tester: Calling save() method");
         testObject.save();
         LOG.info("Tester: Save complete"); 
  
      /* If an Exception occurs, print out the call stack so we can determine where it came from */
      }catch(Exception e) {
         e.printStackTrace();

      /* Print out any errors that were issued by the Business Object */
      }finally {
         List<IssuedError> errors = testObject.getIssuedErrors();
         for(IssuedError errorObject : errors) 
         {
            
            LOG.info(errorObject.getErrorText());
         }
         testObject.remove();
      }
   }
}
