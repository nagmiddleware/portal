
  

package com.ams.tradeportal.tester;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import com.ams.tradeportal.busobj.*;


/*
 * Client application to test the TermsParty Business Object.  Creates a new instance of the
 * object, sets attributes on it, and attempts to save it to the database.  The time required to execute
 * the test is calculated and written to "System.Out".  Multiple iterations of the test can be executed
 * by specifying a command line argument.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class TermsPartyTester
{
private static final Logger LOG = LoggerFactory.getLogger(TermsPartyTester.class);

   public static void main (String[] argv) throws RemoteException, RemoveException, AmsException
   {
      /* Initialize the Object Server */
      LOG.info("Tester: Start of Test");
      try{
         ServerDeploy sd = null;
         LOG.info("Tester: Initializing Object Server");
         sd = (ServerDeploy)EJBObjectFactory.createClientEJB("t3://localhost:7001","ServerDeploy");  
         sd.init("t3://localhost:7001");
      }catch (Exception e) {}  
    
      /* Determine how many times to execute the test */
      int timesToExecute = new Integer(argv[0]).intValue();
      if (timesToExecute == 0) { timesToExecute = 1; }

      /* Start timing how long the test takes */
      ExecutionTimer timer = new ExecutionTimer();
      timer.setStart();

      /* Perform the test the number of times specified by the user */
      for(int i=0; !(i >= timesToExecute); i++)
         {executeTest();}
     
      /* Write out how long the test took. */
      timer.setEnd();
      LOG.info("Tester: End of Test");
      LOG.info("Tester: Execution Time per Call: " + timer.getTime() / timesToExecute);
   }


   protected static void executeTest() throws RemoteException, AmsException, RemoveException
   {
      TermsParty testObject = null;

      /* Test the basic functionaility of the TermsParty object.  This includes creating a
         new instance, setting attributes on it, and saving it to the database. */
      try{
         /* Create a new instance of the object */
         LOG.info("Tester: Creating instance of class TermsParty");
         testObject = (TermsParty)EJBObjectFactory.createClientEJB("t3://localhost:7001","TermsParty");
         LOG.info("Tester: Calling newObject() method");
         testObject.newObject();

         /* Set attrbutes on the object */
         LOG.info("Tester: Setting attribute terms_party_type to 'O'");
         testObject.setAttribute("terms_party_type", "O");
   
         LOG.info("Tester: Setting attribute name to 'TEST'");
         testObject.setAttribute("name", "TEST");
   
         LOG.info("Tester: Setting attribute address_line_1 to 'TEST'");
         testObject.setAttribute("address_line_1", "TEST");
   
         LOG.info("Tester: Setting attribute address_line_2 to 'TEST'");
         testObject.setAttribute("address_line_2", "TEST");
   
         LOG.info("Tester: Setting attribute address_line_3 to 'TEST'");
         testObject.setAttribute("address_line_3", "TEST");
   
         LOG.info("Tester: Setting attribute address_city to 'TEST'");
         testObject.setAttribute("address_city", "TEST");
   
         LOG.info("Tester: Setting attribute address_state_province to 'TEST'");
         testObject.setAttribute("address_state_province", "TEST");
   
         LOG.info("Tester: Setting attribute address_country to 'O'");
         testObject.setAttribute("address_country", "O");
   
         LOG.info("Tester: Setting attribute address_postal_code to 'TEST'");
         testObject.setAttribute("address_postal_code", "TEST");
   
         LOG.info("Tester: Setting attribute phone_number to 'TEST'");
         testObject.setAttribute("phone_number", "TEST");
   
         LOG.info("Tester: Setting attribute OTL_customer_id to 'TEST'");
         testObject.setAttribute("OTL_customer_id", "TEST");
   
         LOG.info("Tester: Setting attribute branch_code to 'TEST'");
         testObject.setAttribute("branch_code", "TEST");
   
         LOG.info("Tester: Setting attribute contact_name to 'TEST'");
         testObject.setAttribute("contact_name", "TEST");
   
         LOG.info("Tester: Setting attribute acct_num to 'TEST'");
         testObject.setAttribute("acct_num", "TEST");
   
         LOG.info("Tester: Setting attribute acct_currency to 'O'");
         testObject.setAttribute("acct_currency", "O");
   
         LOG.info("Tester: Setting attribute acct_choices to 'TEST'");
         testObject.setAttribute("acct_choices", "TEST");
   
         LOG.info("Tester: Setting attribute address_seq_num to '7'");
         testObject.setAttribute("address_seq_num", "7");
   
         LOG.info("Tester: Setting attribute address_search_indicator to 'Y'");
         testObject.setAttribute("address_search_indicator", "Y");
   
         LOG.info("Tester: Setting attribute vendor_id to 'TEST'");
         testObject.setAttribute("vendor_id", "TEST");

         /* Save the object to the database */
         LOG.info("Tester: Calling save() method");
         testObject.save();
         LOG.info("Tester: Save complete"); 
  
      /* If an Exception occurs, print out the call stack so we can determine where it came from */
      }catch(Exception e) {
         e.printStackTrace();

      /* Print out any errors that were issued by the Business Object */
      }finally {
         List<IssuedError> errors = testObject.getIssuedErrors();
         for(IssuedError errorObject : errors) 
         {
            
            LOG.info(errorObject.getErrorText());
         }
         testObject.remove();
      }
   }
}
