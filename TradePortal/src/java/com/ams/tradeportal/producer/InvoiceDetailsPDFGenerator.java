package com.ams.tradeportal.producer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.rmi.*;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Vector;

import com.amsinc.ecsg.util.*;
import com.ams.tradeportal.common.*;
import com.ams.tradeportal.busobj.*;
import com.amsinc.ecsg.frame.*;


/**
 * This class is designed to Build an XML Document with values from various
 * business objects then pass this data through an XSL stylesheet format the
 * data for display then pass the result on to a cocoon wrapper to display the
 * result in a PDF format. The method: populateXmlDoc does most of the work,
 * however get stream puts it all together and returns the result (a Reader
 * object). The Corresponding XSL file is: InvoiceDetailsPDFGenerator.xsl.
 * 
 * Copyright � 2001 American Management Systems, Incorporated All rights
 * reserved
 */
public class InvoiceDetailsPDFGenerator extends DocumentProducer {
private static final Logger LOG = LoggerFactory.getLogger(InvoiceDetailsPDFGenerator.class);

	private InvoicesSummaryData invoicesSummaryData = null;
	private String useCurrency = null;
	private ResourceManager resMgr = null;
	private DocumentHandler invoiceSummaryGoodsResult = null;
	private DocumentHandler invoiceListDetailResult = null;
	private ArMatchingRule invoiceTradingPartnerDetailResult = null;
	SimpleDateFormat isoDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
	private DocumentHandler creditNotesAppliedDetails = null; //CR914A Rel9.2

	/**
	 * This method is the method that gathers all the data and builds the Xml
	 * document to be returned to the caller for display. The Xsl template is
	 * expecting the Xml doc to send data out in a specific format...ie in
	 * sections. If you scroll down the left side you can easily jump around the
	 * xml doc looking for specific section letters.
	 * 
	 * @param Output
	 *            Xml Doc - This is the resulting Xml doc that will be returned.
	 * @param Key
	 *            - This is the Transaction oid the will help us get all our
	 *            data. With out this oid, no data can be retrieved.
	 * @param Locale
	 *            - This is the current users locale, it will help in
	 *            establishing the client Server data bridge as well as building
	 *            various B.O.s
	 * @param BankPrefix
	 *            - This may be removed in the future.
	 * @param ContextPath
	 *            - the context path under which this request was made
	 * @return OutputDoc - Send out the result of building our Document.
	 */

	protected DocumentHandler populateXmlDoc(DocumentHandler outputDoc,
			String key, String locale, String bankPrefix, String contextPath)
			throws RemoteException, AmsException {

		Long invoiceOid = new Long(key);

		// Setup the logo file location, context path and locale instance
		// variables
		if ( "blueyellow".equals(bankPrefix) ) {
			bankPrefix = TradePortalConstants.DEFAULT_BRANDING;
		  }
		 if ( "anztransac".equals(bankPrefix) ) {
			 bankPrefix = "anz";			 
		  }
		 bankLogo = bankPrefix + "/images";

		formContextPath = contextPath;
		formLocale = locale;
		
		ClientServerDataBridge csdb = new ClientServerDataBridge();
		csdb.setLocaleName(locale);

		String serverLocation = JPylonProperties.getInstance().getString(
				"serverLocation");

		// Get access to the Reference Data manager -- this is a singleton
		refDataMgr = ReferenceDataManager.getRefDataMgr();

		// Create invoiceSummaryData
		invoicesSummaryData = (InvoicesSummaryData) EJBObjectFactory
				.createClientEJB(serverLocation, "InvoicesSummaryData",
						invoiceOid.longValue(), csdb);

		useCurrency = invoicesSummaryData.getAttribute("currency");
		if (StringFunction.isBlank(useCurrency)) {
			useCurrency = "2";
		}
		resMgr = new ResourceManager(csdb);
		String invoiceID = invoicesSummaryData.getAttribute("invoice_id");
		//Srinivasu_D Rel8.2 IR#16433 start
		String invClassification = invoicesSummaryData.getAttribute("invoice_classification");
		String buyerOrSeller = null;
		if(StringFunction.isNotBlank(invClassification) && TradePortalConstants.INVOICE_TYPE_REC_MGMT.equals(invClassification)){
			buyerOrSeller = invoicesSummaryData.getAttribute("buyer_name");
		} else if(StringFunction.isNotBlank(invClassification) && TradePortalConstants.INVOICE_TYPE_PAY_MGMT.equals(invClassification)){
			buyerOrSeller = invoicesSummaryData.getAttribute("seller_name");
		}
		//Srinivasu_D Rel8.2 IR#16433 end
		StringBuffer invoiceTradingPartnerDetailSQL = new StringBuffer();
		invoiceTradingPartnerDetailSQL
				.append("select address_line_1, address_line_2,")
				.append(" address_city ")
				.append(" from ar_matching_rule")
				.append(" where buyer_name = " + "'"
						//+ invoicesSummaryData.getAttribute("buyer_name") + "'");
						+ buyerOrSeller + "'");
						
		invoiceTradingPartnerDetailResult = invoicesSummaryData.getMatchingRule();

		//jgadela  R92 - SQL Injection FIX
		String invoiceSummaryGoodsSQL   ="select * from invoice_summary_goods  where p_invoice_summary_data_oid = ? ";
		invoiceSummaryGoodsResult = DatabaseQueryBean.getXmlResultSet(invoiceSummaryGoodsSQL, false, new Object[]{invoiceOid});
		
		//CR-914A Rel9.2 Start
	    String creditNoteSQL = "select r.credit_note_applied_amount, c.invoice_id from INVOICES_CREDIT_NOTES_RELATION r,CREDIT_NOTES c"
		               + " where r.a_upload_credit_note_oid=c.upload_credit_note_oid and  r.a_upload_invoice_oid = ?";
	     
		creditNotesAppliedDetails = DatabaseQueryBean.getXmlResultSet(creditNoteSQL, false, new Object[]{invoiceOid});			
		//CR-914A Rel9.2 End

		try {
			// Section 'A' -- Title Bank Logo and document reference #
			sectionA(outputDoc, invoiceID);
			// Section 'B' -- Invoice Details
			sectionB1(outputDoc); //first row
			sectionB2(outputDoc); //second row
			// Section 'C' -- Buyer & Trading Partner Details
			sectionC(outputDoc, invoiceTradingPartnerDetailResult);
			// Section 'D' -- Invoice Summary details
			sectionD(outputDoc, invoiceSummaryGoodsResult);
			//Vshah - Rel8.2 - IR#T36000015267 - BEGIN
			// Section 'E' -- Payment Details
			sectionE(outputDoc);
			//Vshah - Rel8.2 - IR#T36000015267- END
			//CR-914A Rel9.2 start
			// Section 'F' -- Credit Notes Applied Details
			sectionF(outputDoc, creditNotesAppliedDetails);
			//CR-914A Rel9.2 End
			LOG.debug("OutputDoc == " + outputDoc.toString());

		} finally { // remove any beans for clean up if they have data
			beanRemove( invoicesSummaryData, "InvoicesSummaryData");
		}

		return outputDoc;

	}

	/**
	 * SectionA corresponds to the portion of the XML document labeled
	 * 'SectionA'. The following is an example of what the resulting Xml should
	 * look like: 
	 * <SectionA><!-- Header Info --> 
	 * 	<Title> 
	 * 		<Line1>Invoice Details</Line1> 
	 * 		<Line2></Line2> 
	 * 	</Title>
	 *	<Logo>http://localhost:7001//portal/images/blueyellow/en/</Logo>
 	 * </SectionA>
	 * 
 	 * 			DocumentHandler.setAttribute does not allow us to add same 
 	 * 			elements multiple times as it just updates existing elements.
	 * 			To overcome above limitation elements are added to the output 
	 * 			document using the DocumentHandler.addComponent method. 
	 * 			This method always adds elements to the top of the document. 
	 * 			So to maintain the order of the elements we need to add the 
	 * 			elements in reverse order.
	 *
	 * @param Output
	 *            Document - This is the Xml document being built to be sent out
	 *            for display.
	 */
	protected void sectionA(DocumentHandler outputDoc, String invoiceId)
			throws RemoteException, AmsException {
		// Customizations Here
		outputDoc.setAttribute("/SectionA/Title/Line1",
				"Invoice Details");
		// Call Parent Method
	}

	/**
	 * SectionB corresponds to the portion of the XML document labeled
	 * 'SectionB'. The following is an example of what the resulting Xml should
	 * look like: 
	 * 		<SectionB><!-- Invoice Details Info -->
	 * 			<InvoiceDetails>
	 * 				<field> <!-- each field -->
	 * 					<name> <!-- field name -->
	 * 					</name>	
	 * 					<value> <!-- field value -->
	 * 					</value>
	 * 				</field>
	 * 			</InvoiceDetails> 
	 * 		</SectionB>
 	 * 			DocumentHandler.setAttribute does not allow us to add same 
 	 * 			elements multiple times as it just updates existing elements.
	 * 			To overcome above limitation elements are added to the output 
	 * 			document using the DocumentHandler.addComponent method. 
	 * 			This method always adds elements to the top of the document. 
	 * 			So to maintain the order of the elements we need to add the 
	 * 			elements in reverse order.
	 * 
	 * @param Output
	 *            Document - This is the Xml document being built to be sent out
	 *            for display.
	 */

	protected void sectionB1(DocumentHandler outputDoc) throws RemoteException,
			AmsException {

		DocumentHandler tempDoc = null;
		String date = "";

		if (!TradePortalConstants.INV_LINKED_INSTR_PYB
				.equals(invoicesSummaryData
						.getAttribute("linked_to_instrument_type"))) {

			tempDoc = new DocumentHandler();
			tempDoc.setAttribute(
					"/field/name",
					resMgr.getText("UploadInvoiceDetail.InvoiceAmount",
							TradePortalConstants.TEXT_BUNDLE) + "  ");
			tempDoc.setAttribute("/field/value", TPCurrencyUtility
					.getDisplayAmountWithCurrency(
							invoicesSummaryData.getAttribute("amount"),
							useCurrency, formLocale));
			outputDoc.addComponent("/SectionB1/InvoiceDetails/", tempDoc);
		}

		if (TradePortalConstants.INV_LINKED_INSTR_PYB
				.equals(invoicesSummaryData
						.getAttribute("linked_to_instrument_type"))) {
			// invoice amount
			tempDoc = new DocumentHandler();
			tempDoc.setAttribute(
					"/field/name",
					resMgr.getText("UploadInvoiceDetail.InvoiceAmount",
							TradePortalConstants.TEXT_BUNDLE) + "  ");
			tempDoc.setAttribute("/field/value", TPCurrencyUtility
					.getDisplayAmountWithCurrency(
							invoicesSummaryData.getAttribute("amount"),
							useCurrency, formLocale));
			outputDoc.addComponent("/SectionB1/InvoiceDetails/", tempDoc);

			// send to supplier date
			tempDoc = new DocumentHandler();
			tempDoc.setAttribute("/field/name", resMgr.getText(
					"UploadInvoiceDetail.SendToSuppDate",
					TradePortalConstants.TEXT_BUNDLE));
			date = invoicesSummaryData.getAttribute("send_to_supplier_date");
			if (StringFunction.isNotBlank(date)) {
				date = isoDateFormat.format(invoicesSummaryData
						.getAttributeDate("send_to_supplier_date"));
				date = TPDateTimeUtility.formatDate(date,
						TPDateTimeUtility.SHORT, formLocale);
			}

			tempDoc.setAttribute("/field/value", date);
			outputDoc.addComponent("/SectionB1/InvoiceDetails/", tempDoc);
		}

		// payment date
		tempDoc = new DocumentHandler();

		tempDoc.setAttribute(
				"/field/name",
				resMgr.getText("UploadInvoiceDetail.PaymentDate",
						TradePortalConstants.TEXT_BUNDLE) + " ");

		date = invoicesSummaryData.getAttribute("payment_date");
		if (StringFunction.isNotBlank(date)) {
			date = isoDateFormat.format(invoicesSummaryData
					.getAttributeDate("payment_date"));
			date = TPDateTimeUtility.formatDate(date, TPDateTimeUtility.SHORT,
					formLocale);
		}

		// due date
		tempDoc.setAttribute("/field/value", date);
		outputDoc.addComponent("/SectionB1/InvoiceDetails/", tempDoc);

		tempDoc = new DocumentHandler();
		tempDoc.setAttribute("/field/name", resMgr
				.getText("UploadInvoiceDetail.DueDate",
						TradePortalConstants.TEXT_BUNDLE));

		date = invoicesSummaryData.getAttribute("due_date");
		if (StringFunction.isNotBlank(date)) {
			date = isoDateFormat.format(invoicesSummaryData
					.getAttributeDate("due_date"));
			date = TPDateTimeUtility.formatDate(date, TPDateTimeUtility.SHORT,
					formLocale);
		}

		// issue date
		tempDoc.setAttribute("/field/value", date);

		outputDoc.addComponent("/SectionB1/InvoiceDetails/", tempDoc);

		tempDoc = new DocumentHandler();
		tempDoc.setAttribute("/field/name", resMgr.getText(
				"UploadInvoiceDetail.IssueDate",
				TradePortalConstants.TEXT_BUNDLE));
		date = invoicesSummaryData.getAttribute("issue_date");
		if (StringFunction.isNotBlank(date)) {
			date = isoDateFormat.format(invoicesSummaryData
					.getAttributeDate("issue_date"));
			date = TPDateTimeUtility.formatDate(date, TPDateTimeUtility.SHORT,
					formLocale);
		}

		// section header
		tempDoc.setAttribute("/field/value", date);
		outputDoc.addComponent("/SectionB1/InvoiceDetails/", tempDoc);

		tempDoc = new DocumentHandler();
		tempDoc.setAttribute("/section-header/name", resMgr.getText(
				"UploadInvoiceDetail.InvoiceNumber",
				TradePortalConstants.TEXT_BUNDLE));
		tempDoc.setAttribute("/section-header/value",
				invoicesSummaryData.getAttribute("invoice_id"));
		outputDoc.addComponent("/SectionB1/InvoiceDetails/", tempDoc);
	}

	protected void sectionB2(DocumentHandler outputDoc) throws RemoteException,
	AmsException {

		DocumentHandler tempDoc = null;

		// linked to
		tempDoc = new DocumentHandler();
		tempDoc.setAttribute("/field/name", resMgr.getText(
				"UploadInvoiceDetail.LinkedToInstType",
				TradePortalConstants.TEXT_BUNDLE));
		tempDoc.setAttribute("/field/value", refDataMgr.getDescr(
				TradePortalConstants.UPLOAD_INV_LINKED_INSTR_TY,
				invoicesSummaryData.getAttribute("linked_to_instrument_type"),
				formLocale));
		outputDoc.addComponent("/SectionB2/InvoiceDetails/", tempDoc);
		
		if (TradePortalConstants.INV_LINKED_INSTR_PYB
				.equals(invoicesSummaryData
						.getAttribute("linked_to_instrument_type"))) {
		
		tempDoc = new DocumentHandler();
		tempDoc.setAttribute("/field/name", resMgr.getText(
						"UploadInvoices.EndToEndId",
						TradePortalConstants.TEXT_BUNDLE));
		tempDoc.setAttribute("/field/value", 
						invoicesSummaryData.getAttribute("end_to_end_id"));
		outputDoc.addComponent("/SectionB2/InvoiceDetails/", tempDoc);
		}
		// invoice status
		tempDoc = new DocumentHandler();
		tempDoc.setAttribute("/field/name", resMgr.getText(
				"UploadInvoiceDetail.InvoiceStatus",
				TradePortalConstants.TEXT_BUNDLE));
		tempDoc.setAttribute("/field/value", refDataMgr.getDescr(
				TradePortalConstants.UPLOAD_INVOICE_STATUS,
				invoicesSummaryData.getAttribute("invoice_status"), formLocale));
		outputDoc.addComponent("/SectionB2/InvoiceDetails/", tempDoc);

		if (!TradePortalConstants.INV_LINKED_INSTR_PYB
				.equals(invoicesSummaryData
						.getAttribute("linked_to_instrument_type"))) {
			// Net Invoice Amount
			tempDoc = new DocumentHandler();
			tempDoc.setAttribute("/field/name", resMgr.getText(
					"UploadInvoiceDetail.NetFinanceAmount",
					TradePortalConstants.TEXT_BUNDLE));
			tempDoc.setAttribute("/field/value", TPCurrencyUtility
					.getDisplayAmountWithCurrency(invoicesSummaryData
							.getAttribute("net_invoice_finance_amount"),
							useCurrency, formLocale));
			outputDoc.addComponent("/SectionB2/InvoiceDetails/", tempDoc);

			//
			tempDoc = new DocumentHandler();
			tempDoc.setAttribute("/field/name", resMgr.getText(
					"UploadInvoiceDetail.EstimatedInterest",
					TradePortalConstants.TEXT_BUNDLE));
			tempDoc.setAttribute("/field/value", TPCurrencyUtility
					.getDisplayAmountWithCurrency(invoicesSummaryData
							.getAttribute("estimated_interest_amount"),
							useCurrency, formLocale));
			outputDoc.addComponent("/SectionB2/InvoiceDetails/", tempDoc);

			//
			tempDoc = new DocumentHandler();
			tempDoc.setAttribute("/field/name", resMgr.getText(
					"UploadInvoiceDetail.FinancedAmount",
					TradePortalConstants.TEXT_BUNDLE));
			tempDoc.setAttribute("/field/value", TPCurrencyUtility
					.getDisplayAmountWithCurrency(invoicesSummaryData
							.getAttribute("invoice_finance_amount"),
							useCurrency, formLocale));
			outputDoc.addComponent("/SectionB2/InvoiceDetails/", tempDoc);

		}

		if (TradePortalConstants.INV_LINKED_INSTR_PYB
				.equals(invoicesSummaryData
						.getAttribute("linked_to_instrument_type"))) {

			// Net Invoice Amount
			tempDoc = new DocumentHandler();
			tempDoc.setAttribute("/field/name", resMgr.getText(
					"UploadInvoices.NetInvoiceAmount",
					TradePortalConstants.TEXT_BUNDLE));
			BigDecimal net_inv_amt = StringFunction
					.isNotBlank(invoicesSummaryData
							.getAttribute("payment_amount")) ? new BigDecimal(
									invoicesSummaryData.getAttribute("payment_amount"))
			: new BigDecimal(invoicesSummaryData.getAttribute("amount"));
									BigDecimal totalCRAmt = BigDecimal.ZERO;
									if (StringFunction.isNotBlank(invoicesSummaryData
											.getAttribute("total_credit_note_amount"))) {
										totalCRAmt = new BigDecimal(
												invoicesSummaryData
												.getAttribute("total_credit_note_amount"));
									}

									net_inv_amt = net_inv_amt.subtract(totalCRAmt.abs());
									String temp = net_inv_amt.toString();

									if (StringFunction.isNotBlank(net_inv_amt.toPlainString())) {
										temp = TPCurrencyUtility.getDisplayAmountWithCurrency(
												net_inv_amt.toPlainString(), useCurrency, formLocale);
									}
									tempDoc.setAttribute("/field/value", temp);
									outputDoc.addComponent("/SectionB2/InvoiceDetails/", tempDoc);

									// Credit Note Amount
									tempDoc = new DocumentHandler();
									tempDoc.setAttribute("/field/name", resMgr.getText(
											"UploadInvoices.CreditNoteAppliedAmt",
											TradePortalConstants.TEXT_BUNDLE));
									String cr_note_amt = invoicesSummaryData
											.getAttribute("total_credit_note_amount");
									if (StringFunction.isNotBlank(cr_note_amt)) {
										cr_note_amt = TPCurrencyUtility.getDisplayAmountWithCurrency(
												cr_note_amt, useCurrency, formLocale);
									}
									tempDoc.setAttribute("/field/value", cr_note_amt);
									outputDoc.addComponent("/SectionB2/InvoiceDetails/", tempDoc);

									// Payment Amount
									tempDoc = new DocumentHandler();
									tempDoc.setAttribute("/field/name", resMgr.getText(
											"UploadInvoiceDetail.EarlyPaymentAmount",
											TradePortalConstants.TEXT_BUNDLE));
									String amt = invoicesSummaryData.getAttribute("payment_amount");
									if (StringFunction.isNotBlank(amt)) {
										amt = TPCurrencyUtility.getDisplayAmountWithCurrency(amt,
												useCurrency, formLocale);
									}
									tempDoc.setAttribute("/field/value", amt);
									outputDoc.addComponent("/SectionB2/InvoiceDetails/", tempDoc);
		}

}	
	
	/**
	 * SectionC corresponds to the portion of the XML document labeled
	 * 'SectionC'. The following is an example of what the resulting Xml should
	 * look like: 
	 * 			<BuyerDetails>
	 * 				<field> <!-- each field -->
	 * 					<name> <!-- field name -->
	 * 					</name>	
	 * 					<value> <!-- field value -->
	 * 					</value>
	 * 				</field>
	 * 			</BuyerDetails> 
	 * 
	 * @param Output
	 *            Document - This is the Xml document being built to be sent out
	 *            for display.
	 */
	protected void sectionC(DocumentHandler outputDoc,
			ArMatchingRule rule)
			throws RemoteException, AmsException {

		DocumentHandler tempDoc = null;
		// Override Code goes here
		String addressLine1 = null;
		String addressLine2 = null;
		String addressCity = null;
		String addressCountry = "";
		//Buyer details added
		String buyerId = null;
		String buyerAddLine1 = null;
	    String buyerAddLine2 = null;
	    String buyerAddCity  = null; 
	    String buyerAddCtry  = null;
	    String buyerName = null;
		if (rule != null) {
			addressLine1 = rule
					.getAttribute("address_line_1");
			addressLine2 = rule
					.getAttribute("address_line_2");
			addressCity = rule
					.getAttribute("address_city");
			addressCountry = rule
					.getAttribute("address_country");
			buyerName = rule.getAttribute("buyer_name");
		}
		String corpOrgOid = invoicesSummaryData.getAttribute("corp_org_oid");
		
		//jgadela  R92 - SQL Injection FIX
		String buyerDetailsSQL = " select name, address_line_1, address_line_2, address_city, address_country from corporate_org where organization_oid = ? ";		
		LOG.info("buyerDetailsSQL.toString():"+buyerDetailsSQL.toString());	
		DocumentHandler buyerDetails  = DatabaseQueryBean.getXmlResultSet(buyerDetailsSQL, false, new Object[]{corpOrgOid});		
		
		if (buyerDetails != null) {
			
			buyerId = buyerDetails.getAttribute("/ResultSetRecord(0)/NAME");
			buyerAddLine1 = buyerDetails.getAttribute("/ResultSetRecord(0)/ADDRESS_LINE_1");
			buyerAddLine2 = buyerDetails.getAttribute("/ResultSetRecord(0)/ADDRESS_LINE_2");
			buyerAddCity = buyerDetails.getAttribute("/ResultSetRecord(0)/ADDRESS_CITY");
			buyerAddCtry = buyerDetails.getAttribute("/ResultSetRecord(0)/ADDRESS_COUNTRY");
		}
		LOG.info("buyerId:"+buyerId +"\t buyerAddLine1:"+buyerAddLine1+"\t buyerAddLine2:"+buyerAddLine2+"\t buyerAddCity:"+buyerAddCity+"\t buyerAddCtry:"+buyerAddCtry);
		//SHR start updates
		tempDoc = new DocumentHandler();
		tempDoc.setAttribute("/field/name", resMgr.getTextIncludingEmptyString(
				"UploadInvoiceDetail.AddressCity",
				TradePortalConstants.TEXT_BUNDLE));
		tempDoc.setAttribute("/field/value", addressCity + " "+addressCountry);
		outputDoc.addComponent("/SectionC/BuyerDetails", tempDoc);

		tempDoc = new DocumentHandler();
		tempDoc.setAttribute("/field/name", resMgr.getTextIncludingEmptyString(
				"UploadInvoiceDetail.AddressLine2",
				TradePortalConstants.TEXT_BUNDLE));
		tempDoc.setAttribute("/field/value", addressLine2);
		outputDoc.addComponent("/SectionC/BuyerDetails", tempDoc);

		tempDoc = new DocumentHandler();
		tempDoc.setAttribute("/field/name", resMgr.getText(
				"UploadInvoiceDetail.AddressLine1",
				TradePortalConstants.TEXT_BUNDLE));
		tempDoc.setAttribute("/field/value", addressLine1);
		outputDoc.addComponent("/SectionC/BuyerDetails", tempDoc);
		
		tempDoc = new DocumentHandler();
		tempDoc.setAttribute("/field/name", resMgr.getText(
				"UploadInvoiceDetail.NameIdentity",
				TradePortalConstants.TEXT_BUNDLE));
		tempDoc.setAttribute("/field/value",buyerName);
		outputDoc.addComponent("/SectionC/BuyerDetails", tempDoc);		
		
		tempDoc = new DocumentHandler();
		tempDoc.setAttribute("/field/name", resMgr.getTextIncludingEmptyString(
				"UploadInvoiceDetail.AddressCity",
				TradePortalConstants.TEXT_BUNDLE));
		tempDoc.setAttribute("/field/value", buyerAddCity);
		outputDoc.addComponent("/SectionC/BuyerDetails", tempDoc);

		tempDoc = new DocumentHandler();
		tempDoc.setAttribute("/field/name", resMgr.getTextIncludingEmptyString(
				"UploadInvoiceDetail.AddressLine2",
				TradePortalConstants.TEXT_BUNDLE));
		tempDoc.setAttribute("/field/value", buyerAddLine2);
		outputDoc.addComponent("/SectionC/BuyerDetails", tempDoc);

		tempDoc = new DocumentHandler();
		tempDoc.setAttribute("/field/name", resMgr.getText(
				"UploadInvoiceDetail.AddressLine1",
				TradePortalConstants.TEXT_BUNDLE));
		tempDoc.setAttribute("/field/value", buyerAddLine1);
		outputDoc.addComponent("/SectionC/BuyerDetails", tempDoc);
		
		tempDoc = new DocumentHandler();
		tempDoc.setAttribute("/field/name", resMgr.getText(
				"UploadInvoiceDetail.Identifier",
				TradePortalConstants.TEXT_BUNDLE));
		tempDoc.setAttribute("/field/value",
				buyerId);
		outputDoc.addComponent("/SectionC/BuyerDetails", tempDoc);
		
		//SHR end updates
		tempDoc = new DocumentHandler();
		tempDoc.setAttribute("/section-header/name", resMgr.getText(
				"UploadInvoiceDetail.TradingPartnerInfo",
				TradePortalConstants.TEXT_BUNDLE));
		outputDoc.addComponent("/SectionC/BuyerDetails", tempDoc);
		
		tempDoc = new DocumentHandler();
		if (TradePortalConstants.INVOICE_TYPE_PAY_MGMT
				.equals(invoicesSummaryData
						.getAttribute("invoice_classification"))) {
		tempDoc.setAttribute("/section-header/name", resMgr.getText(
				"UploadInvoiceDetail.BuyerInfo",TradePortalConstants.TEXT_BUNDLE));
		}else{
			tempDoc.setAttribute("/section-header/name", resMgr.getText(
					"InvoicesOfferedDetail.Seller",TradePortalConstants.TEXT_BUNDLE));
		}
		outputDoc.addComponent("/SectionC/BuyerDetails", tempDoc);
	}

	/**
	 * SectionD corresponds to the portion of the XML document labeled
	 * 'SectionD'. The following is an example of what the resulting Xml should
	 * look like: 
	 * 		<SectionD><!-- Applicant and Beneficiary-->
	 * 			<InvSummGoodsDetails>
	 * 				<field> <!-- each field -->
	 * 					<name> <!-- field name -->
	 * 					</name>	
	 * 					<value> <!-- field value -->
	 * 					</value>
	 * 				</field>
	 * 				<LineItemDetails>
	 * 					<field> <!-- each field -->
	 * 						<name> <!-- field name -->
	 * 						</name>	
	 * 						<value> <!-- field value -->
	 * 						</value>
	 * 					</field>
	 *	 			</LineItemDetails> 
	 * 			</InvSummGoodsDetails> 
 	 * 		</SectionD>
 	 * 			DocumentHandler.setAttribute does not allow us to add same 
 	 * 			elements multiple times as it just updates existing elements.
	 * 			To overcome above limitation elements are added to the output 
	 * 			document using the DocumentHandler.addComponent method. 
	 * 			This method always adds elements to the top of the document. 
	 * 			So to maintain the order of the elements we need to add the 
	 * 			elements in reverse order.
	 * @param Output
	 *            Document - This is the Xml document being built to be sent out
	 *            for display.
	 */

	protected void sectionD(DocumentHandler outputDoc,
			DocumentHandler invoiceSummaryGoodsResult) throws RemoteException,
			AmsException {
		// Overridden Code
		DocumentHandler tempDoc = null;
		DocumentHandler tempDoc1 = null;
		if (invoiceSummaryGoodsResult != null) {
			Vector summaryGoodsVector = invoiceSummaryGoodsResult
					.getFragments("/ResultSetRecord/");
			int totalSummaryGoods = summaryGoodsVector.size();
			for (int goods = totalSummaryGoods - 1; goods >= 0; goods--) {
				tempDoc1 = new DocumentHandler();
				tempDoc1.setAttribute("/InvSummGoodsDetails", "");

				for (int i = TradePortalConstants.INV_BUYER_OR_SELLER_USER_DEF_FIELD; i >= 1; i--) {
					if (!StringFunction.isBlank(invoiceSummaryGoodsResult
							.getAttribute("/ResultSetRecord(" + goods
									+ ")/SELLER_USERS_DEF" + i + "_LABEL"))) {
						tempDoc = new DocumentHandler();
						tempDoc.setAttribute("/field/name",
								invoiceSummaryGoodsResult
										.getAttribute("/ResultSetRecord("
												+ goods + ")/SELLER_USERS_DEF"
												+ i + "_LABEL"));						
						tempDoc.setAttribute("/field/value",
								invoiceSummaryGoodsResult
										.getAttribute("/ResultSetRecord("
												+ goods + ")/SELLER_USERS_DEF"
												+ i + "_VALUE"));
						tempDoc1.addComponent("/InvSummGoodsDetails", tempDoc);
					}
				}
				for (int i = TradePortalConstants.INV_BUYER_OR_SELLER_USER_DEF_FIELD; i >= 1; i--) {
					if (!StringFunction.isBlank(invoiceSummaryGoodsResult
							.getAttribute("/ResultSetRecord(" + goods
									+ ")/BUYER_USERS_DEF" + i + "_LABEL"))
									&& !"Buyer User Definition Label".equals(invoiceSummaryGoodsResult
										.getAttribute("/ResultSetRecord("
												+ goods + ")/BUYER_USERS_DEF"
												+ i + "_VALUE"))) {
						tempDoc = new DocumentHandler();
						tempDoc.setAttribute("/field/name",
								invoiceSummaryGoodsResult
										.getAttribute("/ResultSetRecord("
												+ goods + ")/BUYER_USERS_DEF"
												+ i + "_LABEL"));
						tempDoc.setAttribute("/field/value",
								invoiceSummaryGoodsResult
										.getAttribute("/ResultSetRecord("
												+ goods + ")/BUYER_USERS_DEF"
												+ i + "_VALUE"));
						tempDoc1.addComponent("/InvSummGoodsDetails", tempDoc);
					}
				}

				tempDoc = new DocumentHandler();
				tempDoc.setAttribute("/field/name", resMgr.getText(
						"UploadInvoiceDetail.PurchaseOrderId",
						TradePortalConstants.TEXT_BUNDLE));
				tempDoc.setAttribute("/field/value", invoiceSummaryGoodsResult
						.getAttribute("/ResultSetRecord(" + goods
								+ ")/PURCHASE_ORDER_ID"));
				tempDoc1.addComponent("/InvSummGoodsDetails", tempDoc);

				tempDoc = new DocumentHandler();
				tempDoc.setAttribute("/field/name", resMgr.getText(
						"UploadInvoiceDetail.ActualShpDateNoBreak",
						TradePortalConstants.TEXT_BUNDLE));
				
				String date = invoiceSummaryGoodsResult.getAttribute("/ResultSetRecord(" + goods + ")/ACTUAL_SHIP_DATE");
				if(StringFunction.isNotBlank(date)){					
					date = TPDateTimeUtility.formatDate(date, TPDateTimeUtility.SHORT, formLocale);					
				}
				tempDoc.setAttribute("/field/value", date);
				tempDoc1.addComponent("/InvSummGoodsDetails", tempDoc);

				//BSL IR NLUM040346023 04/11/2012 BEGIN
				tempDoc = new DocumentHandler();
				tempDoc.setAttribute("/field/name",
						resMgr.getText("UploadInvoiceDetail.Carrier",
								TradePortalConstants.TEXT_BUNDLE));
				tempDoc.setAttribute("/field/value", 
						invoiceSummaryGoodsResult.getAttribute("/ResultSetRecord("
								+ goods + ")/CARRIER"));
				tempDoc1.addComponent("/InvSummGoodsDetails", tempDoc);
				//BSL IR NLUM040346023 04/11/2012 END

				tempDoc = new DocumentHandler();
				tempDoc.setAttribute("/field/name", resMgr.getText(
						"UploadInvoiceDetail.Vessel",
						TradePortalConstants.TEXT_BUNDLE));
				tempDoc.setAttribute("/field/value", invoiceSummaryGoodsResult
						.getAttribute("/ResultSetRecord(" + goods + ")/VESSEL"));
				tempDoc1.addComponent("/InvSummGoodsDetails", tempDoc);

				tempDoc = new DocumentHandler();
				tempDoc.setAttribute("/field/name", resMgr.getText(
						"UploadInvoiceDetail.PortofDischarge",
						TradePortalConstants.TEXT_BUNDLE));
				tempDoc.setAttribute("/field/value", invoiceSummaryGoodsResult
						.getAttribute("/ResultSetRecord(" + goods
								+ ")/COUNTRY_OF_DISCHARGE"));
				tempDoc1.addComponent("/InvSummGoodsDetails", tempDoc);

				tempDoc = new DocumentHandler();
				tempDoc.setAttribute("/field/name", resMgr.getText(
						"UploadInvoiceDetail.PortofLoading",
						TradePortalConstants.TEXT_BUNDLE));
				tempDoc.setAttribute("/field/value", invoiceSummaryGoodsResult
						.getAttribute("/ResultSetRecord(" + goods
								+ ")/COUNTRY_OF_LOADING"));
				tempDoc1.addComponent("/InvSummGoodsDetails", tempDoc);

				tempDoc = new DocumentHandler();
				tempDoc.setAttribute("/field/name", resMgr.getText(
						"UploadInvoiceDetail.Incoterm",
						TradePortalConstants.TEXT_BUNDLE));
				tempDoc.setAttribute("/field/value", invoiceSummaryGoodsResult
						.getAttribute("/ResultSetRecord(" + goods
								+ ")/INCOTERM"));
				tempDoc1.addComponent("/InvSummGoodsDetails", tempDoc);


				tempDoc = new DocumentHandler();
				tempDoc.setAttribute("/field/name", resMgr.getText(
						"UploadInvoiceDetail.GoodsDescr",
						TradePortalConstants.TEXT_BUNDLE));
				tempDoc.setAttribute("/field/value", invoiceSummaryGoodsResult
						.getAttribute("/ResultSetRecord(" + goods
								+ ")/GOODS_DESCRIPTION"));
				tempDoc1.addComponent("/InvSummGoodsDetails", tempDoc);

				tempDoc = new DocumentHandler();
				tempDoc.setAttribute("/section-header/name", resMgr.getText(
						"UploadInvoiceDetail.InvoiceSummaryDetail",
						TradePortalConstants.TEXT_BUNDLE));
				tempDoc1.addComponent("/InvSummGoodsDetails", tempDoc);
				//jgadela  R92 - SQL Injection FIX
				String summaryGoodsOid = invoiceSummaryGoodsResult.getAttribute("/ResultSetRecord(" + goods+ ")/UPLOAD_INVOICE_GOODS_OID");
				String invoiceListDetailSQL = "select * from invoice_line_item_detail  where p_invoice_summary_goods_oid = ? ";
				invoiceListDetailResult = DatabaseQueryBean.getXmlResultSet(
						invoiceListDetailSQL, false, new Object[]{summaryGoodsOid});
				if (invoiceListDetailResult != null) {
					Vector lineItemVector = invoiceListDetailResult
							.getFragments("/ResultSetRecord/");
					int totalLineItem = lineItemVector.size();
					DocumentHandler tempLineDoc = null;
					DocumentHandler tempLineDoc1 = null;
					for (int lineItem = totalLineItem - 1; lineItem >= 0; lineItem--) {
						tempLineDoc1 = new DocumentHandler();
						tempLineDoc1.setAttribute("/LineItems", "");
						
						for (int i = TradePortalConstants.INV_LINE_ITEM_TYPE_OR_VALUE_FIELD; i >= 1; i--) {
							if (!StringFunction
									.isBlank(invoiceListDetailResult
											.getAttribute("/ResultSetRecord("
													+ lineItem
													+ ")/PROD_CHARS_UD" + i
													+ "_VAL"))) {
								tempLineDoc = new DocumentHandler();
								tempLineDoc
										.setAttribute(
												"/field/name",
												resMgr.getText(
														"UploadInvoiceDetail.ProdUserDfn",
														TradePortalConstants.TEXT_BUNDLE)
														+ " " + i + " Value");
								tempLineDoc
										.setAttribute(
												"/field/value",
												invoiceListDetailResult
														.getAttribute("/ResultSetRecord("
																+ lineItem
																+ ")/PROD_CHARS_UD"
																+ i + "_VAL"));
								tempLineDoc1.addComponent("/LineItems",
										tempLineDoc);
							}
							if (!StringFunction
									.isBlank(invoiceListDetailResult
											.getAttribute("/ResultSetRecord("
													+ lineItem
													+ ")/PROD_CHARS_UD" + i
													+ "_TYPE"))) {
								tempLineDoc = new DocumentHandler();
								tempLineDoc
										.setAttribute(
												"/field/name",
												resMgr.getText(
														"UploadInvoiceDetail.ProdUserDfn",
														TradePortalConstants.TEXT_BUNDLE)
														+ " " + i + " Type");
								tempLineDoc
										.setAttribute(
												"/field/value",
												invoiceListDetailResult
														.getAttribute("/ResultSetRecord("
																+ lineItem
																+ ")/PROD_CHARS_UD"
																+ i + "_TYPE"));
								tempLineDoc1.addComponent("/LineItems",
										tempLineDoc);
							}
						}

						//BSL IR NLUM040346023 04/11/2012 BEGIN
						tempLineDoc = new DocumentHandler();
						tempLineDoc.setAttribute("/field/name",
								resMgr.getText("UploadInvoiceDetail.UnitOfMeasureQuantity",
										TradePortalConstants.TEXT_BUNDLE));
						tempLineDoc.setAttribute("/field/value",
								invoiceListDetailResult.getAttribute("/ResultSetRecord("
										+ lineItem + ")/UNIT_OF_MEASURE_QUANTITY"));
						tempLineDoc1.addComponent("/LineItems", tempLineDoc);

						tempLineDoc = new DocumentHandler();
						tempLineDoc.setAttribute("/field/name",
								resMgr.getText("UploadInvoiceDetail.UnitOfMeasurePrice",
										TradePortalConstants.TEXT_BUNDLE));
						tempLineDoc.setAttribute("/field/value",
								invoiceListDetailResult.getAttribute("/ResultSetRecord("
										+ lineItem + ")/UNIT_OF_MEASURE_PRICE"));
						tempLineDoc1.addComponent("/LineItems", tempLineDoc);
						//BSL IR NLUM040346023 04/11/2012 END

						tempLineDoc = new DocumentHandler();
						tempLineDoc.setAttribute("/field/name", resMgr.getText(
								"UploadInvoiceDetail.InvolvedQuty",
								TradePortalConstants.TEXT_BUNDLE));
						tempLineDoc.setAttribute("/field/value",
								invoiceListDetailResult
										.getAttribute("/ResultSetRecord("
												+ lineItem + ")/INV_QUANTITY"));
						tempLineDoc1.addComponent("/LineItems", tempLineDoc);

						tempLineDoc = new DocumentHandler();
						tempLineDoc.setAttribute("/field/name", resMgr.getText(
								"UploadInvoiceDetail.unitPrice",
								TradePortalConstants.TEXT_BUNDLE));
						tempLineDoc.setAttribute("/field/value",
								invoiceListDetailResult
										.getAttribute("/ResultSetRecord("
												+ lineItem + ")/UNIT_PRICE"));
						tempLineDoc1.addComponent("/LineItems", tempLineDoc);

						tempLineDoc = new DocumentHandler();
						tempLineDoc
								.setAttribute(
										"/section-header/name",
										resMgr.getText(
												"UploadInvoiceDetail.LineItemDetail",
												TradePortalConstants.TEXT_BUNDLE));
						tempLineDoc1.addComponent("/LineItems", tempLineDoc);

						tempDoc1.addComponent("/InvSummGoodsDetails",
								tempLineDoc1);
					}
				}
				outputDoc.addComponent("/SectionD", tempDoc1);
			}
		}

	}

	//Vshah - Rel8.2 - IR#T36000015267 - BEGIN
	/**
	 * SectionE corresponds to the portion of the XML document labeled
	 * 'SectionE'. The following is an example of what the resulting Xml should
	 * look like: 
	 * 			<PaymentDetails>
	 * 				<field> <!-- each field -->
	 * 					<name> <!-- field name -->
	 * 					</name>	
	 * 					<value> <!-- field value -->
	 * 					</value>
	 * 				</field>
	 * 			</PaymentDetails> 
	 * 
	 * @param Output
	 *            Document - This is the Xml document being built to be sent out
	 *            for display.
	 */
	protected void sectionE(DocumentHandler outputDoc) throws RemoteException, AmsException {

		DocumentHandler tempDoc = null;
		if (outputDoc == null) return;
		int outDocLen = outputDoc.toString().length();
		
		if(StringFunction.isNotBlank(invoicesSummaryData.getAttribute("reporting_code_2"))){
			tempDoc = new DocumentHandler();
			tempDoc.setAttribute("/field/name", resMgr.getText(
					"InvoiceUploadRequest.reporting_code_2",
					TradePortalConstants.TEXT_BUNDLE));
			tempDoc.setAttribute("/field/value",
					invoicesSummaryData.getAttribute("reporting_code_2"));
			outputDoc.addComponent("/SectionE/PaymentDetails/", tempDoc);
			}

		if(StringFunction.isNotBlank(invoicesSummaryData.getAttribute("reporting_code_1"))){
			tempDoc = new DocumentHandler();
			tempDoc.setAttribute("/field/name", resMgr.getText(
					"InvoiceUploadRequest.reporting_code_1",
					TradePortalConstants.TEXT_BUNDLE));
			tempDoc.setAttribute("/field/value",
					invoicesSummaryData.getAttribute("reporting_code_1"));
			outputDoc.addComponent("/SectionE/PaymentDetails/", tempDoc);
			}
		
		if(StringFunction.isNotBlank(invoicesSummaryData.getAttribute("central_bank_rep3"))){
		tempDoc = new DocumentHandler();		
		tempDoc.setAttribute("/field/name", resMgr.getText(
				"UploadInvoiceDetail.CentralReporting3",
				TradePortalConstants.TEXT_BUNDLE));
		tempDoc.setAttribute("/field/value",
				invoicesSummaryData.getAttribute("central_bank_rep3"));
		outputDoc.addComponent("/SectionE/PaymentDetails/", tempDoc);
		}
		if(StringFunction.isNotBlank(invoicesSummaryData.getAttribute("central_bank_rep2"))){
		tempDoc = new DocumentHandler();
		tempDoc.setAttribute("/field/name", resMgr.getText(
				"UploadInvoiceDetail.CentralReporting2",
				TradePortalConstants.TEXT_BUNDLE));
		tempDoc.setAttribute("/field/value",
				invoicesSummaryData.getAttribute("central_bank_rep2"));
		outputDoc.addComponent("/SectionE/PaymentDetails/", tempDoc);
		}
		if(StringFunction.isNotBlank(invoicesSummaryData.getAttribute("central_bank_rep1"))){
		tempDoc = new DocumentHandler();
		tempDoc.setAttribute("/field/name", resMgr.getText(
				"UploadInvoiceDetail.CentralReporting1",
				TradePortalConstants.TEXT_BUNDLE));
		tempDoc.setAttribute("/field/value",
				invoicesSummaryData.getAttribute("central_bank_rep1"));
		outputDoc.addComponent("/SectionE/PaymentDetails/", tempDoc);
		}
		if(StringFunction.isNotBlank(invoicesSummaryData.getAttribute("ben_bank_province"))){
		tempDoc = new DocumentHandler();
		tempDoc.setAttribute("/field/name", resMgr.getText(
				"UploadInvoiceDetail.BenBankProvinceCountry",
				TradePortalConstants.TEXT_BUNDLE));
		tempDoc.setAttribute(
				"/field/value",
				invoicesSummaryData.getAttribute("ben_bank_province")
						+ "	 "
						+ invoicesSummaryData
								.getAttribute("ben_branch_country"));
		outputDoc.addComponent("/SectionE/PaymentDetails/", tempDoc);
		}
		
		if(StringFunction.isNotBlank(invoicesSummaryData.getAttribute("ben_bank_city"))){
		tempDoc = new DocumentHandler();
		tempDoc.setAttribute("/field/name", resMgr.getText(
				"UploadInvoiceDetail.BenBankCity",
				TradePortalConstants.TEXT_BUNDLE));
		tempDoc.setAttribute("/field/value",
				invoicesSummaryData.getAttribute("ben_bank_city"));
		outputDoc.addComponent("/SectionE/PaymentDetails/", tempDoc);
		}
		
		if(StringFunction.isNotBlank(invoicesSummaryData.getAttribute("ben_branch_add2"))){
		tempDoc = new DocumentHandler();
		tempDoc.setAttribute("/field/name", resMgr.getText(
				"UploadInvoiceDetail.BenBankBranchAddr2",
				TradePortalConstants.TEXT_BUNDLE));
		tempDoc.setAttribute("/field/value",
				invoicesSummaryData.getAttribute("ben_branch_add2"));
		outputDoc.addComponent("/SectionE/PaymentDetails/", tempDoc);
		}
		
		if(StringFunction.isNotBlank(invoicesSummaryData.getAttribute("ben_branch_add1"))){
		tempDoc = new DocumentHandler();
		tempDoc.setAttribute("/field/name", resMgr.getText(
				"UploadInvoiceDetail.BenBankBranchAddr1",
				TradePortalConstants.TEXT_BUNDLE));
		tempDoc.setAttribute("/field/value",
				invoicesSummaryData.getAttribute("ben_branch_add1"));
		outputDoc.addComponent("/SectionE/PaymentDetails/", tempDoc);
		}
		if(StringFunction.isNotBlank(invoicesSummaryData.getAttribute("ben_bank_sort_code"))){
			tempDoc = new DocumentHandler();
			tempDoc.setAttribute("/field/name", resMgr.getText(
					"InvoiceUploadRequest.ben_bank_sort_code",
					TradePortalConstants.TEXT_BUNDLE));
			tempDoc.setAttribute("/field/value",
					invoicesSummaryData.getAttribute("ben_bank_sort_code"));
			outputDoc.addComponent("/SectionE/PaymentDetails/", tempDoc);
			}
		if(StringFunction.isNotBlank(invoicesSummaryData.getAttribute("ben_branch_code"))){
		tempDoc = new DocumentHandler();
		tempDoc.setAttribute("/field/name", resMgr.getText(
				"UploadInvoiceDetail.BenBankBranchCode",
				TradePortalConstants.TEXT_BUNDLE));
		tempDoc.setAttribute("/field/value",
				invoicesSummaryData.getAttribute("ben_branch_code"));
		outputDoc.addComponent("/SectionE/PaymentDetails/", tempDoc);
		}
		
		
		if(StringFunction.isNotBlank(invoicesSummaryData.getAttribute("ben_bank_name"))){
		tempDoc = new DocumentHandler();
		tempDoc.setAttribute("/field/name", resMgr.getText(
				"UploadInvoiceDetail.BenBankName",
				TradePortalConstants.TEXT_BUNDLE));
		tempDoc.setAttribute("/field/value",
				invoicesSummaryData.getAttribute("ben_bank_name"));
		outputDoc.addComponent("/SectionE/PaymentDetails/", tempDoc);
		}
		
		if(StringFunction.isNotBlank(invoicesSummaryData.getAttribute("charges"))){
		tempDoc = new DocumentHandler();
		tempDoc.setAttribute("/field/name", resMgr
				.getText("UploadInvoiceDetail.Charges",
						TradePortalConstants.TEXT_BUNDLE));
		tempDoc.setAttribute("/field/value",
				invoicesSummaryData.getAttribute("charges"));
		outputDoc.addComponent("/SectionE/PaymentDetails/", tempDoc);
		}
		if(StringFunction.isNotBlank(invoicesSummaryData.getAttribute("ben_email_addr"))){
			tempDoc = new DocumentHandler();
			tempDoc.setAttribute("/field/name", resMgr.getText(
					"InvoiceUploadRequest.ben_email_addr",
					TradePortalConstants.TEXT_BUNDLE));
			tempDoc.setAttribute("/field/value",
					invoicesSummaryData.getAttribute("ben_email_addr"));
			outputDoc.addComponent("/SectionE/PaymentDetails/", tempDoc);
			}
		if(StringFunction.isNotBlank(invoicesSummaryData.getAttribute("ben_country"))){
		tempDoc = new DocumentHandler();
		tempDoc.setAttribute("/field/name", resMgr.getText(
				"UploadInvoiceDetail.BenCountry",
				TradePortalConstants.TEXT_BUNDLE));
		tempDoc.setAttribute("/field/value",
				invoicesSummaryData.getAttribute("ben_country"));
		outputDoc.addComponent("/SectionE/PaymentDetails/", tempDoc);
		}
		
		if(StringFunction.isNotBlank(invoicesSummaryData.getAttribute("ben_add3"))){
		tempDoc = new DocumentHandler();
		tempDoc.setAttribute("/field/name", resMgr.getText(
				"UploadInvoiceDetail.BenAddr3",
				TradePortalConstants.TEXT_BUNDLE));
		tempDoc.setAttribute("/field/value",
				invoicesSummaryData.getAttribute("ben_add3"));
		outputDoc.addComponent("/SectionE/PaymentDetails/", tempDoc);
		}
		
		if(StringFunction.isNotBlank(invoicesSummaryData.getAttribute("ben_add2"))){
		tempDoc = new DocumentHandler();
		tempDoc.setAttribute("/field/name", resMgr.getText(
				"UploadInvoiceDetail.BenAddr2",
				TradePortalConstants.TEXT_BUNDLE));
		tempDoc.setAttribute("/field/value",
				invoicesSummaryData.getAttribute("ben_add2"));
		outputDoc.addComponent("/SectionE/PaymentDetails/", tempDoc);
		}
		
		if(StringFunction.isNotBlank(invoicesSummaryData.getAttribute("ben_add1"))){
		tempDoc = new DocumentHandler();
		tempDoc.setAttribute("/field/name", resMgr.getText(
				"UploadInvoiceDetail.BenAddr1",
				TradePortalConstants.TEXT_BUNDLE));
		tempDoc.setAttribute("/field/value",
				invoicesSummaryData.getAttribute("ben_add1"));
		outputDoc.addComponent("/SectionE/PaymentDetails/", tempDoc);
		}
		
		if(StringFunction.isNotBlank(invoicesSummaryData.getAttribute("ben_acct_num"))){
		tempDoc = new DocumentHandler();
		tempDoc.setAttribute("/field/name", resMgr.getText(
				"UploadInvoiceDetail.BenAcctNumber",
				TradePortalConstants.TEXT_BUNDLE));
		tempDoc.setAttribute("/field/value",
				invoicesSummaryData.getAttribute("ben_acct_num"));
		outputDoc.addComponent("/SectionE/PaymentDetails/", tempDoc);
		}
		
		if(StringFunction.isNotBlank(invoicesSummaryData.getAttribute("pay_method"))){
		tempDoc = new DocumentHandler();
		tempDoc.setAttribute("/field/name", resMgr.getText(
				"UploadInvoiceDetail.PaymentMethod",
				TradePortalConstants.TEXT_BUNDLE));
		tempDoc.setAttribute("/field/value", refDataMgr.getDescr(
				TradePortalConstants.PMT_METHOD,
				invoicesSummaryData.getAttribute("pay_method"), formLocale));
		outputDoc.addComponent("/SectionE/PaymentDetails/", tempDoc);
		}
		
		int newLen = outputDoc.toString().length();
		LOG.info("newLen:"+newLen+"\t oldlen:"+outDocLen);
		if(newLen>outDocLen){
		tempDoc = new DocumentHandler();
		tempDoc.setAttribute("/section-header/name", resMgr.getText(
				"UploadInvoiceDetail.PaymentDetails",
				TradePortalConstants.TEXT_BUNDLE));

		outputDoc.addComponent("/SectionE/PaymentDetails/", tempDoc);
		}
		
	}
	//Vshah - Rel8.2 - IR#T36000015267 - END
	
	/**
	 * vdesingu - CR914A - Rel 9.2
	 * 
	 * SectionF corresponds to the portion of the XML document labeled
	 * 'SectionF'. The following is an example of what the resulting Xml should
	 * look like: 
	 * 			<CreditNotes>
	 * 				<section-header><name></name></section-header>
	 * 				<InvoiceID>
	 * 					<field><name></name><value></value></field>
	 * 				</InvoiceID>
	 * 				<CreditAmount>
	 * 					<field><name></name><value></value></field>
	 * 				</CreditAmount>
	 * 				<Label>
	 * 					<field><name></name></field>
	 * 				</Label>
	 * 			</CreditNotes> 
	 * 
	 * @param Output
	 *            Document - This is the Xml document being built to be sent out
	 *            for display.
	 */
	
	protected void sectionF(DocumentHandler outputDoc, DocumentHandler creditNoteDoc) throws RemoteException, AmsException {
		DocumentHandler tempDoc = null;
		
		if(null != creditNoteDoc){
			List<DocumentHandler> creditNoteResult = creditNoteDoc.getFragmentsList("/ResultSetRecord/");
			
			
			for (DocumentHandler temp : creditNoteResult) {

				tempDoc = new DocumentHandler();
				tempDoc.setAttribute("/field/name", resMgr.getText(
						"UploadInvoices.CreditNoteId",
						TradePortalConstants.TEXT_BUNDLE));
				tempDoc.setAttribute("/field/value", temp.getAttribute("/INVOICE_ID"));		
				outputDoc.addComponent("/SectionF/CreditNotes/InvoiceID", tempDoc);
				
				tempDoc = new DocumentHandler();
				tempDoc.setAttribute("/field/name", resMgr.getText(
						"UploadInvoices.AppliedAmt",
						TradePortalConstants.TEXT_BUNDLE));
				tempDoc.setAttribute("/field/value", temp.getAttribute("/CREDIT_NOTE_APPLIED_AMOUNT"));		
				outputDoc.addComponent("/SectionF/CreditNotes/CreditAmount", tempDoc);
			}
		}
		tempDoc = new DocumentHandler();
		tempDoc.setAttribute("/field/name", resMgr.getText(
				"UploadInvoices.AppliedAmt",
				TradePortalConstants.TEXT_BUNDLE));

		outputDoc.addComponent("/SectionF/CreditNotes/Label", tempDoc);
		
		tempDoc = new DocumentHandler();
		tempDoc.setAttribute("/field/name", resMgr.getText(
				"UploadInvoices.CreditNoteId",
				TradePortalConstants.TEXT_BUNDLE));

		outputDoc.addComponent("/SectionF/CreditNotes/Label", tempDoc);		
		tempDoc = new DocumentHandler();
		tempDoc.setAttribute("/section-header/name", resMgr.getText(
				"UploadInvoices.CreditNoteAppliedDetails",
				TradePortalConstants.TEXT_BUNDLE));

		outputDoc.addComponent("/SectionF/CreditNotes/", tempDoc);
	}
	
	/**
	 * This method is meant simply to test an object to see if it needs to be removed,
	 * If so, then we try to remove it.  If we get an exception, It would be probably
	 * Because we removed a parent of a component when we tried to remove the component
	 * object.
	 *
	 * @param BusinessObject - The object we're trying to remove.
	 * @param Object Type    - This is the type of object being sent out in the error message.
	 */
	private void beanRemove( TradePortalBusinessObject busObj, String objectType )
	{
		if( busObj != null )
		{   try{
					busObj.remove();
			}catch(Exception e)
			{   LOG.info("Failed in removal of " + objectType + " Bean: Invoice Details PDF Generator");
				LOG.info("This could be because this object is a component of a parent object previously removed.");
			}
		}
	}

}