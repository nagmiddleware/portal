package com.ams.tradeportal.producer;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import javax.ejb.RemoveException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.busobj.BankOrganizationGroup;
import com.ams.tradeportal.busobj.CorporateOrganization;
import com.ams.tradeportal.busobj.DomesticPayment;
import com.ams.tradeportal.busobj.Instrument;
import com.ams.tradeportal.busobj.InvoiceDetails;
import com.ams.tradeportal.common.TPCurrencyUtility;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.ClientServerDataBridge;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.frame.EJBObjectFactory;
import com.amsinc.ecsg.frame.ReferenceDataManager;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.JPylonProperties;
import com.amsinc.ecsg.util.StringFunction;

/**
 * This class is designed to Build an XML Document with values from various business objects then pass this data through an XSL
 * stylesheet format the data for display then pass the result on to a cocoon wrapper to display the result in a PDF format. The
 * method: populateXmlDoc does most of the work, however get stream puts it all together and returns the result (a Reader object).
 * The Corresponding XSL file is: EmailAttachment.xsl.
 * 
 * Copyright � 2001 American Management Systems, Incorporated All rights reserved Vasavi CR 524 03/31/2010
 */
public class EmailAttachment {
	private static final Logger LOG = LoggerFactory.getLogger(EmailAttachment.class);

	private static final String EMAIL_HEADING = "EmailAttachment.EmailMessage";
	private static final String EMAIL_TEXT = "EmailAttachment.EmailText";
	private static final String BANK_GROUP = "EmailAttachment.BankGroup";
	private static final String SUBJECT = "EmailAttachment.Subject";
	private static final String ON_BEHALF_OF = "EmailAttachment.OnBehalfOf";
	private static final String WE_HAVE_PAID = "EmailAttachment.WeHavePaid";
	private static final String VIA = "EmailAttachment.Via";
	private static final String ON_ACCOUNT = "EmailAttachment.OnAccount";
	private static final String AT = "EmailAttachment.At";
	private static final String PROCESSING_DATE = "EmailAttachment.ProcessingDate";
	private static final String AMOUNT = "EmailAttachment.Amount";
	private static final String OUR_REFERENCE = "EmailAttachment.OurReference";
	private static final String PAYMENT_CURRENCY = "EmailAttachment.PaymentCurrency";
	private static final String PAYMENT_DETAILS = "EmailAttachment.PaymentDetails";
	private static final String INVOICE_DETAILS = "EmailAttachment.InvoiceDetails";
	private static final String DISCLAIMER = "EmailAttachment.Disclaimer";
	private static final String DEFAULT_SENDER_NAME = "EmailAttachment.DefaultSenderName";

	/**
	 * getStream's goal is to build a long string with all the data and return a reader based on the string's data.
	 */
	// SL - 10/29/2014 R9.1 IR T36000030417 [START] - Ordering Party name is corrected in Beneficiary Email - Parent child organisation
	public Reader getStream(String contextPath, Map<String, String> parms, Map<String, Object> attributes, String orderingPartyName)
			throws IOException, AmsException {
		DomesticPayment domesticPayment = (DomesticPayment) attributes.get("domestic_payment");
		BankOrganizationGroup bankOrg = (BankOrganizationGroup) attributes.get("bank_org");
		CorporateOrganization corpOrg = (CorporateOrganization) attributes.get("corp_org");
		Instrument instrument = (Instrument) attributes.get("instrument");

		String locale = parms.get("locale");
		String bankPrefix = parms.get("bank_prefix");

		DocumentHandler xmlDoc = populateXmlDoc(getBlankDocumentHandler(), domesticPayment, bankOrg, corpOrg, instrument, locale,
				bankPrefix, contextPath, orderingPartyName);

		return new StringReader(xmlDoc.toString());
	}

	protected DocumentHandler getBlankDocumentHandler() {
		return new DocumentHandler("<EmailAttachment></EmailAttachment>", false);
	}

	/**
	 * Returns root of the URL for image files located on Portal server.
	 */
	protected String getImageLocation() {
		String bundleName = "TradePortal";
		String imageLocation;

		try {
			ResourceBundle portalProperties = PropertyResourceBundle.getBundle(bundleName);
			imageLocation = portalProperties.getString("producerRoot") + TradePortalConstants.IMAGES_PATH;
		} catch (Exception e) {
			LOG.info("Cannot resolve image location from " + bundleName + " resource bundle. Exception is ", e);
			imageLocation = TradePortalConstants.IMAGES_PATH;
		}

		return imageLocation;
	}

	/**
	 * This method is the method that gathers all the data and builds the Xml document to be returned to the caller for display. The
	 * Xsl template is expecting the Xml doc to send data out in a specific format...ie in sections.
	 * 
	 * @param Output
	 *            Xml Doc - This is the resulting Xml doc that will be returned.
	 * @param Key
	 *            - This is the domestic oid the will help us get all our data. With out this oid, no data can be retrieved.
	 * @param Locale
	 *            - This is the current users locale
	 * @param BankPrefix
	 *            - This may be removed in the future.
	 * @return OutputDoc - Send out the result of building our Document.
	 */
	protected DocumentHandler populateXmlDoc(DocumentHandler outputDoc, DomesticPayment domesticPayment,
			BankOrganizationGroup bankOrg, CorporateOrganization corpOrg, Instrument instru, String locale, String bankPrefix,
			String contextPath, String orderingPartyName) throws RemoteException, AmsException {
		
		if ("blueyellow".equals(bankPrefix)) {
			bankPrefix = TradePortalConstants.DEFAULT_BRANDING;
		}
		if ("anztransac".equals(bankPrefix)) {
			bankPrefix = "anz";
		}
		String logo = bankPrefix + "/images";

		ClientServerDataBridge csdb = new ClientServerDataBridge();
		csdb.setLocaleName(locale);
		Locale lc = new Locale(csdb.getLocaleName().substring(0, 2), csdb.getLocaleName().substring(3, 5));
		PropertyResourceBundle bundle = (PropertyResourceBundle) PropertyResourceBundle.getBundle("TextResources", lc);

		String serverLocation = JPylonProperties.getInstance().getString("serverLocation");

		// Must get the RefDataMgr with the server location, if the default constructor is used
		// the data will not be read on the AgentServer.
		ReferenceDataManager.getRefDataMgr(serverLocation);
		

		String ordrPartyName;

		if (TradePortalConstants.INV_LINKED_INSTR_PYB.equals(instru.getAttribute("instrument_type_code"))) {
			ordrPartyName = corpOrg.getAttribute("name");

		} else {
			ordrPartyName = orderingPartyName;
		}

		// CJR - 6/27/2011 - IR DEUL062459962 Begin
		String senderName = bankOrg.getAttribute("bene_sender_name");

		if (StringFunction.isBlank(senderName)) {
			senderName = bundle.getString(DEFAULT_SENDER_NAME);
		}
		if (TradePortalConstants.INV_LINKED_INSTR_PYB.equals(instru.getAttribute("instrument_type_code"))) {
			senderName = bankOrg.getAttribute("payable_sender_name");
			if (StringFunction.isBlank(senderName)) {
				senderName = bundle.getString("PaymentBeneficiaryEmail.PayablesDefaultSenderName");
			}
		}
		// CJR - 6/27/2011 - IR DEUL062459962 End

		/* Section 'A' -- Logo/ Email Heading / Email Text / Bank Group */
		sectionA(outputDoc, bundle, logo, senderName, contextPath);

		/*
		 * Section 'B' -- To/ On behalf of/ We have paid/ Via/ On account/ At/ Processing Date/Amount/ Our Reference/Amount Currency
		 */
		// sectionB(outputDoc, domesticPayment, ordrPartyName, bundle); //BSL IR POUL032966481 Add instrument
		sectionB(outputDoc, instru, domesticPayment, ordrPartyName, bundle, locale);

		/* Section 'C' -- Payment Details/ Invoice Details */// Rel 9.0 IR T36000029995 - Adding locale to the method signature
		sectionC(outputDoc, instru, domesticPayment, serverLocation, bundle, locale);

		/* Section 'D' -- Disclaimer */
		sectionD(outputDoc, bundle);

		return outputDoc;
	}

	private void sectionA(DocumentHandler outputDoc, PropertyResourceBundle bundle, String logo, String senderName,
			String contextPath) throws RemoteException, AmsException {
		String imageLocation = getImageLocation();

		outputDoc.setAttribute("/SectionA/Logo", imageLocation + logo + "/");
		outputDoc.setAttribute("/SectionA/EmailMessage/", bundle.getString(EMAIL_HEADING));
		outputDoc.setAttribute("/SectionA/EmailText/", bundle.getString(EMAIL_TEXT));
		outputDoc.setAttribute("/SectionA/BankGroupLabel/", bundle.getString(BANK_GROUP));
		outputDoc.setAttribute("/SectionA/BankGroupValue/", senderName);
	}

	private void sectionB(DocumentHandler outputDoc, Instrument instrument, DomesticPayment domesticPayment, String ordrPartyName,
			PropertyResourceBundle bundle, String locale) throws RemoteException, AmsException {
		String payeeName = domesticPayment.getAttribute("payee_name");
		StringBuilder subject = new StringBuilder();
		subject.append(instrument.getAttribute("complete_instrument_id"));
		subject.append("-");
		subject.append(domesticPayment.getAttribute("sequence_number"));
		String paymentMethod = domesticPayment.getAttribute("payment_method_type");

		// CR 913 start
		if (TradePortalConstants.INV_LINKED_INSTR_PYB.equals(instrument.getAttribute("instrument_type_code"))
				&& StringFunction.isNotBlank(paymentMethod)) {
			String descr = ReferenceDataManager.getRefDataMgr().getDescr("PAYMENT_METHOD", paymentMethod, locale);
			// Rel9.0 IR#T36000030408 Description to be removed from all othe payment methods except IACC
			if (!paymentMethod.equalsIgnoreCase(descr) && paymentMethod.equals(TradePortalConstants.PAYMENT_METHOD_IACC))
				paymentMethod = paymentMethod + " (" + descr + ")";
		}
		// CR 913 end

		String payeeAcctNum = domesticPayment.getAttribute("payee_account_number");
		String onAcct;

		if (StringFunction.isBlank(payeeAcctNum)) {
			onAcct = TradePortalConstants.NOT_APPLICABLE; // Manohar - IR#HNUL070449897 -7/05/2011- display as Not Applicable if the onAcct is empty
		} else if (payeeAcctNum.length() > 4) {
			onAcct = "####".concat(payeeAcctNum.substring(payeeAcctNum.length() - 4));
		} else {
			onAcct = "####".concat(payeeAcctNum);
		}
		String bankCode = domesticPayment.getAttribute("payee_bank_code");
		String branchCode = domesticPayment.getAttribute("payee_branch_code");
		String bankName = domesticPayment.getAttribute("payee_bank_name");

		// CJR - DEUL061746418 - 6/20/11 Begin
		String valueDate = null;
		Date valDate = domesticPayment.getAttributeDate("value_date");
		if (valDate != null) {
			SimpleDateFormat formatter = new SimpleDateFormat("dd MMM yyyy", bundle.getLocale());
			valueDate = formatter.format(valDate);
		}
		// CJR - DEUL061746418 - 6/20/11 End

		String amt = domesticPayment.getAttribute("amount");

		String ourRef = instrument.getAttribute("complete_instrument_id");
		String paymentCurrency = domesticPayment.getAttribute("amount_currency_code");

		outputDoc.setAttribute("/SectionB/SubjectLabel/", bundle.getString(SUBJECT));
		outputDoc.setAttribute("/SectionB/SubjectValue/", subject.toString());
		outputDoc.setAttribute("/SectionB/PartyNameLabel/", bundle.getString(ON_BEHALF_OF));
		outputDoc.setAttribute("/SectionB/PartyNameValue/", ordrPartyName);
		outputDoc.setAttribute("/SectionB/WeHavePaidLabel/", bundle.getString(WE_HAVE_PAID));
		outputDoc.setAttribute("/SectionB/WeHavePaidValue/", payeeName);
		outputDoc.setAttribute("/SectionB/PaymentMethodLabel/", bundle.getString(VIA));
		outputDoc.setAttribute("/SectionB/PaymentMethodValue/", paymentMethod);
		outputDoc.setAttribute("/SectionB/OnAccountLabel/", bundle.getString(ON_ACCOUNT));
		outputDoc.setAttribute("/SectionB/OnAccountValue/", onAcct);
		outputDoc.setAttribute("/SectionB/AtLabel/", bundle.getString(AT));
		outputDoc.setAttribute("/SectionB/BankCodeValue/", bankCode);
		outputDoc.setAttribute("/SectionB/BranchCodeValue/", branchCode);
		outputDoc.setAttribute("/SectionB/BankNameValue/", bankName);
		outputDoc.setAttribute("/SectionB/ValueDateLabel/", bundle.getString(PROCESSING_DATE));
		outputDoc.setAttribute("/SectionB/ValueDateValue/", valueDate);
		outputDoc.setAttribute("/SectionB/AmountLabel/", bundle.getString(AMOUNT));


		outputDoc.setAttribute("/SectionB/AmountValue/", TPCurrencyUtility.getDisplayAmount(amt, paymentCurrency, locale));

		outputDoc.setAttribute("/SectionB/OurRefLabel/", bundle.getString(OUR_REFERENCE));
		outputDoc.setAttribute("/SectionB/OurRefValue/", ourRef);
		outputDoc.setAttribute("/SectionB/PaymentCurrencyLabel/", bundle.getString(PAYMENT_CURRENCY));
		outputDoc.setAttribute("/SectionB/PaymentCurrencyValue/", paymentCurrency);
	}

	private void sectionC(DocumentHandler outputDoc, Instrument instrument, DomesticPayment domesticPayment, String serverLocation,
			PropertyResourceBundle bundle, String locale)
					throws RemoteException, AmsException 	{
		String paymentDetails = domesticPayment.getAttribute("payee_description");
		if (StringFunction.isBlank(paymentDetails)) {
			paymentDetails = TradePortalConstants.NOT_APPLICABLE; // Manohar - IR#HNUL070449897 -7/05/2011- display as Not Applicable if empty
		}

		String invoice_details;

		Map<String, List<String>> invoiceDetailsGroupedOnEndToEndId = new HashMap<>();
		InvoiceDetails invoiceDetails = getInvoiceDetails(domesticPayment, serverLocation);
		if (invoiceDetails != null) {
			invoice_details = invoiceDetails.getAttribute("payee_invoice_details");
			if (StringFunction.isBlank(invoice_details)) {
				invoice_details = TradePortalConstants.NOT_APPLICABLE; // Manohar - IR#HNUL070449897 -7/05/2011- display as Not Applicable if empty
			}

			try {
				invoiceDetails.remove();
			} catch (RemoveException e) {
				throw new AmsException("Rethrowing exception: " + e.toString());
			}
			
		} else {
			invoice_details = TradePortalConstants.NOT_APPLICABLE; // CJR - IR#HNUL070449897 -8/11/2011- Must also display Not Applicable if Invoice Details do not exist.
		}
		if (!TradePortalConstants.INV_LINKED_INSTR_PYB.equals(instrument.getAttribute("instrument_type_code"))) {
			outputDoc.setAttribute("/SectionC/PaymentDetailsLabel/", bundle.getString(PAYMENT_DETAILS));
			outputDoc.setAttribute("/SectionC/PaymentDetailsValue/", paymentDetails);
		}
		outputDoc.setAttribute("/SectionC/InvoiceDetailsLabel/", bundle.getString(INVOICE_DETAILS));

		if (TradePortalConstants.INV_LINKED_INSTR_PYB.equals(instrument.getAttribute("instrument_type_code"))
				&& !TradePortalConstants.NOT_APPLICABLE.equals(invoice_details)) {

			String[] values = invoice_details.split("\n", -1);
			SimpleDateFormat formatter = new SimpleDateFormat("dd MMM yyyy", bundle.getLocale());
			SimpleDateFormat formatter1 = new SimpleDateFormat("MMddyyyy");
			Date date;
			for (String value : values) {
				String[] invValues = value.split(", ", -1);
				if (StringFunction.isNotBlank(invValues[3])) {
					try {
						date = formatter1.parse(invValues[3]);
						invValues[3] = formatter.format(date);
					} catch (ParseException e) {
						LOG.debug("Exception while formatting date-" + e);
					}
				}
				GroupInvoiceDetails(invValues, invoiceDetailsGroupedOnEndToEndId);
			}
			createGroupedOutput(outputDoc, invoiceDetailsGroupedOnEndToEndId, locale);
		} else {
			outputDoc.setAttribute("/SectionC/InvoiceDetailsValue/", invoice_details);
		}
		LOG.info("EmailAttachment output: " + outputDoc);
	}

	private InvoiceDetails getInvoiceDetails(DomesticPayment dp, String serverLocation)
			throws NumberFormatException, RemoteException, AmsException {
		InvoiceDetails invoiceDetails = null;

		String sql = "select invoice_detail_oid from invoice_details where domestic_payment_oid = ? ";
		DocumentHandler resultSet = DatabaseQueryBean.getXmlResultSet(sql, false,
				new Object[] { dp.getAttribute("domestic_payment_oid") });

		if (resultSet != null) {
			List<DocumentHandler> resultList = resultSet.getFragmentsList("/ResultSetRecord");
			if (resultList.size() == 1) {
				String invoiceDetailsOid = resultSet.getAttribute("//ResultSetRecord/INVOICE_DETAIL_OID");
				if (!StringFunction.isBlank(invoiceDetailsOid)) {
					invoiceDetails = (InvoiceDetails) EJBObjectFactory.createClientEJB(serverLocation, "InvoiceDetails", Long.parseLong(invoiceDetailsOid));
				}
			}
		}
		return invoiceDetails;
	}

	private void sectionD(DocumentHandler outputDoc, PropertyResourceBundle bundle) {
		outputDoc.setAttribute("/SectionD/Disclaimer/", bundle.getString(DISCLAIMER));
	}

	private void GroupInvoiceDetails(String[] values, Map<String, List<String>> groupedVals) {
		String theKey = "_";
		if (StringFunction.isNotBlank(values[4])) {
			theKey = values[4];
		}
		List<String> list;
		List<String> existingList;
		if (groupedVals.containsKey(theKey)) {
			existingList = groupedVals.get(theKey);
			Collections.addAll(existingList, values);
			groupedVals.put(theKey, existingList);
		} else {
			list = new ArrayList<>();
			Collections.addAll(list, values);
			groupedVals.put(theKey, list);
		}
	}

	private void createGroupedOutput(DocumentHandler outputDoc, Map<String, List<String>> groupedVals, String locale) {
		
       /*     <InvoiceDetailsValue>
        *       </EndToEndID>
                <RowEntry>
                    <InvoiceID>Id92345</InvoiceID>
                    <Currency>USD</Currency>
                    <Amount>1,230.00</Amount>
                    <PaymentDate>01 Jan 2015</PaymentDate>
                    <Type>CN</Type>
                </RowEntry>
                <id>ED1</id>
                <subtotal>0</subtotal>
                <ccy>USD</ccy>
            </EndToEndID>
        </InvoiceDetailsValue>            
            *
            */
		DocumentHandler invoiceDoc = new DocumentHandler();
		DocumentHandler tempDoc = new DocumentHandler();
		DocumentHandler endToEnd = new DocumentHandler();
		DocumentHandler endToEndDoc = new DocumentHandler();
		DocumentHandler rowEntryDoc = new DocumentHandler();
		BigDecimal totalAmount = BigDecimal.ZERO;

		Iterator<String> iterator = groupedVals.keySet().iterator();
		String ccy = "";

		// The order of values: InvoiceID, CCY, Amount, Due Date, End to End ID, Credit Note/Invoice Indicator
		while (iterator.hasNext()) {
			String endToEndVal = iterator.next();
			List<String> list = groupedVals.get(endToEndVal);
			int idx;
			BigDecimal subtotal = BigDecimal.ZERO;

			for (int j = 0; j < list.size(); j = j + 6) {
				// Example: [Id92345, USD, 2,234.00, 01 Jan 2015, ED1, CN]
				idx = j;
				tempDoc.setAttribute("/RowEntry/InvoiceID", list.get(idx));
				ccy = list.get(idx + 1);
				tempDoc.setAttribute("/RowEntry/Currency", ccy);
				String amt = list.get(idx + 2);
				tempDoc.setAttribute("/RowEntry/Amount", TPCurrencyUtility.getDisplayAmount(amt, ccy, locale));
				if (StringFunction.isNotBlank(amt)) {
					try {
						subtotal = subtotal.add(new BigDecimal(amt));
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				tempDoc.setAttribute("/RowEntry/PaymentDate", list.get(idx + 3));
				tempDoc.setAttribute("/RowEntry/Type", list.get(idx + 5));
				rowEntryDoc.addComponent("/", tempDoc);
				tempDoc = new DocumentHandler();
			}

			try {
				totalAmount = totalAmount.add(subtotal); // since credit notes are negative, addition of a negative will lead to a subtraction.
			} catch (Exception e) {
				e.printStackTrace();
			}
			endToEnd.setAttribute("/EndToEndID/id", endToEndVal);
			endToEnd.setAttribute("/EndToEndID/subtotal", TPCurrencyUtility.getDisplayAmount(subtotal.toPlainString(), ccy, locale));
			endToEnd.setAttribute("/EndToEndID/ccy", ccy);
			endToEnd.addComponent("/EndToEndID", rowEntryDoc);
			rowEntryDoc = new DocumentHandler();
			endToEndDoc.addComponent("/", endToEnd);
			endToEnd = new DocumentHandler();
		}

		invoiceDoc.addComponent("/InvoiceDetailsValue", endToEndDoc);
		outputDoc.addComponent("/SectionC", invoiceDoc);
		// update amount value
		outputDoc.setAttribute("/SectionB/AmountValue/", TPCurrencyUtility.getDisplayAmount(totalAmount.toPlainString(), ccy, locale));
	}
}