package com.ams.tradeportal.producer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.*;

import com.amsinc.ecsg.util.*;
import com.ams.tradeportal.busobj.TermsParty;
import com.ams.tradeportal.busobj.util.InstrumentServices;
import com.ams.tradeportal.common.*;
import com.amsinc.ecsg.frame.*;

/**
 * This class is designed to Build an XML Document with values from various business objects
 * then pass this data through an XSL stylesheet format the data for display then pass the
 * result on to a cocoon wrapper to display the result in a PDF format.  The method:
 * populateXmlDoc does most of the work, however get stream puts it all together and returns
 * the result (a Reader object).  The Corresponding XSL file is: AIRReleaseApplication.xsl.
 */

public class AIRReleaseApplication extends DocumentProducer {
private static final Logger LOG = LoggerFactory.getLogger(AIRReleaseApplication.class);
	/**
	 * This method is the method that gathers all the data and builds the Xml document to be returned
	 * to the caller for display.  The Xsl template is expecting the Xml doc to send data out in a specific
	 * format...ie in sections.  If you scroll down the left side you can easily jump around the xml doc
	 * looking for specific section letters.
	 *
	 * @param Output Xml Doc - This is the resulting Xml doc that will be returned.
	 * @param Key            - This is the Transaction oid the will help us get all our data.
	 *                         With out this oid, no data can be retrieved.
	 * @param Locale         - This is the current users locale, it will help in establishing
	 *                         the client Server data bridge as well as building various B.O.s
	 * @param BankPrefix     - This may be removed in the future.
	 * @param ContextPath    - the context path under which this request was made
	 * @return OutputDoc     - Send out the result of building our Document.
	 */
	protected DocumentHandler populateXmlDoc(DocumentHandler outputDoc, String key, String locale, String bankPrefix, String contextPath)
	throws RemoteException, AmsException
	{
		// Call Ancestor Code first to setup all transaction objects
		super.populateXmlDoc(outputDoc, key, locale, bankPrefix, contextPath);

		try{
			//Section 'A' -- Title Bank Logo and document reference #
			sectionA( outputDoc);
			//Section 'B' -- Operational Bank Org info
			sectionB( outputDoc);
			//Section 'C' -- Op Bank Org Name Only
			sectionC( outputDoc);
			//Section 'D' -- Applicant and Beneficiary
			sectionD( outputDoc);
			sectionD1( outputDoc);
			//Section 'E' -- Account Party and Advising Bank
			sectionE( outputDoc);
			sectionF( outputDoc);
		    sectionG( outputDoc);
			sectionT( outputDoc, locale);

			LOG.debug("OutputDoc == " + outputDoc.toString() );

		}finally{  //remove any beans for clean up if they have data
			cleanUpBeans();
		}

		return outputDoc;

	}

	/**
	 * SectionA corresponds to the portion of the XML document labeled 'SectionA'.
	 * The following is an example of what the resulting Xml should look like:
	 *	<SectionA><!-- Header Info -->
        <Title>
            <Line1>Application and Agreement for</Line1>
            <Line2>Air Waybill Endorsement</Line2>
        </Title>
        <Logo>http://localhost:7003/portal/themes/default/images/</Logo>
        <TransInfo>
            <BankInstrumentID>Bank Instrument ID</BankInstrumentID>
            <InstrumentID>ILC1256US01P</InstrumentID>  
            <TransactionID>ILC1256US01P</TransactionID>
            <ReferenceNumber>Applicant's ref Number</ReferenceNumber>
            <ApplicationDate>17 May 2012</ApplicationDate>
        </TransInfo>
	 *	</SectionA>
	 *
	 * @param Output Document       - This is the Xml document being built to be sent out for display.
 	 */
	protected void sectionA(DocumentHandler outputDoc)
	throws RemoteException, AmsException
	{
		// Customizations Here
		outputDoc.setAttribute( "/SectionA/Title/Line1", "Application and Agreement for");
		outputDoc.setAttribute( "/SectionA/Title/Line2", "Air Waybill Endorsement");

		// Call Parent Method
		super.sectionA(outputDoc);

	}


	/**
	 * SectionB corresponds to the portion of the XML document labeled 'SectionB'.
	 * The following is an example of what the resulting Xml should look like:
	 *	<SectionB><!-- Operational Bank Org Info -->
	 *		<Name>Operational Bank Org Name</Name>
	 *		<AddressLine1>Operational Bank Org Addr Line 1</AddressLine1>
	 *		<AddressLine2>Operational Bank Org Addr Line 2</AddressLine2>
	 *		<AddressStateProvince>Operational Bank Org City + Province</AddressStateProvince>
	 *		<AddressCountryPostCode>Operational Bank Org Country + Post Code</AddressCountryPostCode>
	 *		<PhoneNumber>Operational Bank Org Telephone #</PhoneNumber>
	 *		<Fax>Operational Bank Org Fax #</Fax>
	 *		<Swift>Operational Bank Org Address</Swift>
	 *		<Telex>Operational Bank Org Telex # Operational Bank Org Answer Back</Telex>
	 *	</SectionB>
	 *
	 * @param Output Document           - This is the Xml document being built to be sent out for display.
	 */

	protected void sectionB(DocumentHandler outputDoc)
	throws RemoteException, AmsException
	{
		// Call Parent Method
		super.sectionB(outputDoc);
	}


	/**
	 * SectionC corresponds to the portion of the XML document labeled 'SectionC'.
	 * The following is an example of what the resulting Xml should look like:
	 * 	<SectionC><!-- Operational Bank org Disclaimer -->
	 * 		<Name>Operational Bank Org Name</Name>
	 *	</SectionC>
	 *
	 * @param Output Document           - This is the Xml document being built to be sent out for display.
	 */
	protected void sectionC(DocumentHandler outputDoc)
	throws RemoteException, AmsException
	{
		// Call Parent Method
		super.sectionC(outputDoc);
	}



	/**
	 * SectionD corresponds to the portion of the XML document labeled 'SectionD'.
	 * The following is an example of what the resulting Xml should look like:
    <SectionD>
        <Party ID="Applicant">
            <Label>Applicant</Label>
            <Name>Bases</Name>
            <AddressLine1>928 Willow Ave</AddressLine1>
            <AddressLine2>Line2</AddressLine2>
            <AddressStateProvince>Hoboken</AddressStateProvince>
            <AddressCountryPostCode>United States</AddressCountryPostCode>
        </Party>
        <Party ID="Beneficiary">
            <Label>Release To Party</Label>
            <Name>Bases</Name>
            <AddressLine1>445 Lexington Ave</AddressLine1>
            <AddressStateProvince>New York, NY</AddressStateProvince>
            <AddressCountryPostCode>United States</AddressCountryPostCode>
        </Party>
     
    </SectionD>
	 *
	 * @param Output Document             - This is the Xml document being built to be sent out for display.
	 */

	protected void sectionD(DocumentHandler outputDoc)
	throws RemoteException, AmsException
	{

		String benePArty = terms.getAttribute("c_FirstTermsParty");
		if (InstrumentServices.isBlank(benePArty)) {
			TermsParty applicantParty = (TermsParty) terms
					.getComponentHandle("SecondTermsParty");
			createPartyAddressXml(outputDoc, applicantParty, "SectionD",
					"Applicant");
		} else {
			TermsParty beneficiaryParty = (TermsParty) terms
					.getComponentHandle("FirstTermsParty");
			TermsParty applicantParty = (TermsParty) terms
					.getComponentHandle("SecondTermsParty");
			TermsParty releaseToParty = (TermsParty) terms
					.getComponentHandle("ThirdTermsParty");

			createPartyAddressXml(outputDoc, applicantParty, "SectionD",
					"Applicant");
			createPartyAddressXml(outputDoc, releaseToParty, "SectionD",
					"Release To Party");

			addNonNullDocData(beneficiaryParty.getAttribute("phone_number"),
					outputDoc, "/SectionD/Party(Beneficiary)/PhoneNumber/");
		}
		
	}
	
	/**
	 * 
	 *   <SectionD1>
        <Party ID="FreightForwarder">
            <Label>Freight Forwarder</Label>
            <Name>Balls</Name>
            <AddressLine1>aad1</AddressLine1>
            <AddressLine2>aad2</AddressLine2>
            <AddressStateProvince>city name</AddressStateProvince>
            <AddressCountryPostCode>country</AddressCountryPostCode>
            <CarrierFax>12345678</CarrierFax>
            <CarrierAttentionOf>attn of</CarrierAttentionOf>
        </Party>        
    </SectionD1>
	 * 
	 */
	protected void sectionD1(DocumentHandler outputDoc)
	throws RemoteException, AmsException
	{
		TermsParty freightFrowarderParty = (TermsParty) terms.getComponentHandle("FirstTermsParty");	
		createPartyAddressXml(outputDoc, freightFrowarderParty, "SectionD1","Freight Forwarder Carrier");		
		addNonNullDocData(terms.getAttribute("fax_number"), outputDoc, "/SectionD1/Party/CarrierFax");
		addNonNullDocData(terms.getAttribute("attention_of"), outputDoc, "/SectionD1/Party/CarrierAttentionOf");
		//outputDoc.setAttribute("/Document/SectionD1[1]/Party[1]/Label[1]", "Freight Forwarder / Carrier");
	}

	/**
	 * SectionE corresponds to the portion of the XML document labeled 'SectionE'.
	 * The following is an example of what the resulting Xml should look like:
    <SectionE>
        <Party ID="Freight">
            <Label>Freight Forwarder</Label>
            <Name>Bases</Name>
            <AddressLine1>445 Lexington Ave</AddressLine1>
            <AddressLine2>445 Lexington Ave</AddressLine2>
            <AddressStateProvince>New York, NY</AddressStateProvince>
            <AddressCountryPostCode>United States</AddressCountryPostCode>
        </Party>         
        
    </SectionE>
 	 *
	 * @param Output Document             - This is the Xml document being built to be sent out for display.
	 */
	protected void sectionE(DocumentHandler outputDoc)
	throws RemoteException, AmsException
	{
		// Call Parent Method
		super.sectionE(outputDoc);
	}


	/**
	 * SectionF corresponds to the portion of the XML document labeled 'SectionF'.
	 * The following is an example of what the resulting Xml should look like:
    <SectionF>
        <AmountDetail>
            <CurrencyCodeAmount>USD 300.00</CurrencyCodeAmount>
            <CurrencyValueAmountInWords>United States Dollars three hundred and 00/100
            </CurrencyValueAmountInWords>
        </AmountDetail>
        <RelatedInstrumentId>
            <InstrumentID>Display value of 'Related Instrument Id' field </InstrumentID>
        </RelatedInstrumentId>
    </SectionF>
	 *
	 * @param Output Document       - This is the Xml document being built to be sent out for display.
	 */

	protected void sectionF(DocumentHandler outputDoc)
	throws RemoteException, AmsException
	{
		String currencyCode = null;
		String currencyAmount = null;
		String currencyCodeAndAmount = null;

		currencyCode = terms.getAttribute("amount_currency_code");
		currencyAmount = terms.getAttribute("amount");
		currencyAmount = TPCurrencyUtility.getDisplayAmount(currencyAmount,
				currencyCode, formLocale);

		currencyCodeAndAmount = appendAttributes(currencyCode, currencyAmount,
				" ");

		outputDoc.setAttribute("/SectionF/AmountDetail/CurrencyCodeAmount/",
				currencyCodeAndAmount);

		if (isNotEmpty(currencyCode))
			currencyCode = refDataMgr.getDescr(
					TradePortalConstants.CURRENCY_CODE, currencyCode,
					formLocale);
		if (isNotEmpty(currencyAmount))
			currencyAmount = SpellItOut.spellOutNumber(currencyAmount,
					formLocale);

		currencyCodeAndAmount = appendAttributes(currencyCode, currencyAmount,
				" ");

		outputDoc.setAttribute(
				"/SectionF/AmountDetail/CurrencyValueAmountInWords/",
				currencyCodeAndAmount);		

		// Add Related instrument id
		addNonNullDocData(terms.getAttribute("related_instrument_id"), outputDoc, "/SectionF/RelatedInstrumentId/InstrumentID");
	}


	/**
	 * SectionG corresponds to the portion of the XML document labeled 'SectionG'.
	 * The following is an example of what the resulting Xml should look like:
    <SectionG>
        <ShipmentDetails>
            <FlightNumber>flight number</FlightNumber>
            <AirWaybillNumber> air waybill number</AirWaybillNumber>
            <GoodsDescription>goods desc </GoodsDescription>
        </ShipmentDetails>
    </SectionG>
	 *
	 * @param Output Document       - This is the Xml document being built to be sent out for display.
	 */

	protected void sectionG(DocumentHandler outputDoc) throws RemoteException, AmsException
	{
		
		addNonNullDocData( terms.getFirstShipment().getAttribute("carrier_name_flight_num"), outputDoc, "/SectionG/ShipmentDetails/FlightNumber");
		addNonNullDocData(terms.getFirstShipment().getAttribute("air_waybill_num"), outputDoc, "/SectionG/ShipmentDetails/AirWaybillNumber");
		addNonNullDocData(terms.getFirstShipment().getAttribute("goods_description"), outputDoc, "/SectionG/ShipmentDetails/GoodsDescription");
		
	}



	/**
	 * SectionL corresponds to the portion of the XML document labeled 'SectionK'.
	 * The following is an example of what the resulting Xml should look like:
    <SectionT>
        <InstructionsToBank>instructions to bank details text goes here</InstructionsToBank>
        <CommissionsAndCharges>
            <DebitOurs>Debit: Our Account Number  </DebitOurs>
            <DebitForeign>Debit: Foreign Currency Account Number  </DebitForeign>
            <Currency> Currency of Account dropdown</Currency>
            <AdditionalInstructions>Additional Instructions </AdditionalInstructions>
        </CommissionsAndCharges>
    </SectionT>
	 *
	 * @param Output Document       - This is the Xml document being built to be sent out for display.
	 */
	protected void sectionT(DocumentHandler outputDoc, String locale)
	throws RemoteException, AmsException
	{
        String languageDesc = ReferenceDataManager.getRefDataMgr().getDescr("INSTRUMENT_LANGUAGE",instrument.getAttribute("language"),locale);		
		addNonNullDocData(languageDesc, outputDoc, "/SectionT/InstructionsToBank");
		addNonNullDocData(terms.getAttribute("coms_chrgs_our_account_num"), outputDoc, "/SectionT/CommissionsAndCharges/DebitOurs");
		addNonNullDocData(terms.getAttribute("coms_chrgs_foreign_acct_num"), outputDoc, "/SectionT/CommissionsAndCharges/DebitForeign");
		addNonNullDocData(terms.getAttribute("coms_chrgs_foreign_acct_curr"), outputDoc, "/SectionT/CommissionsAndCharges/Currency");
		addNonNullDocData(terms.getAttribute("special_bank_instructions"), outputDoc, "/SectionT/CommissionsAndCharges/AdditionalInstructions");
		
	}
	
}