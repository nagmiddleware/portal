package com.ams.tradeportal.producer;
import java.rmi.RemoteException;
import java.util.Vector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.busobj.Instrument;
import com.ams.tradeportal.busobj.OperationalBankOrganization;
import com.ams.tradeportal.busobj.Terms;
import com.ams.tradeportal.busobj.TermsParty;
import com.ams.tradeportal.busobj.TradePortalBusinessObject;
import com.ams.tradeportal.busobj.Transaction;
import com.ams.tradeportal.common.ConfigSettingManager;
import com.ams.tradeportal.common.TPCurrencyUtility;
import com.ams.tradeportal.common.TradePortalConstants;
import com.ams.tradeportal.common.TransactionType;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.ClientServerDataBridge;
import com.amsinc.ecsg.frame.EJBObjectFactory;
import com.amsinc.ecsg.frame.ReferenceDataManager;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.JPylonProperties;
import com.amsinc.ecsg.util.StringFunction;

/**
 * This class is designed to Build an XML Document with values from various business objects
 * then pass this data through an XSL stylesheet format the data for display then pass the 
 * result on to a cocoon wrapper to display the result in a PDF format.  The method: 
 * populateXmlDoc does most of the work, however get stream puts it all together and returns
 * the result (a Reader object).  The Corresponding XSL file is: ExportCollection.xsl.
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */

public class ExportCollection extends DocumentProducer {
	private static final Logger LOG = LoggerFactory.getLogger(ExportCollection.class);
	protected DocumentHandler getBlankDocumentHandler() {
		return new DocumentHandler("<ExportCollection></ExportCollection>", false);
	}
//BSL Cocoon Upgrade 10/13/11 Rel 7.0 END

	/**
	 * This method is the method that gathers all the data and builds the Xml document to be returned
	 * to the caller for display.  The Xsl template is expecting the Xml doc to send data out in a specific 
	 * format...ie in sections.  If you scroll down the left side you can easily jump around the xml doc
	 * looking for specific section letters.  
	 *
	 * @param Output Xml Doc - This is the resulting Xml doc that will be returned.
	 * @param Key            - This is the Transaction oid the will help us get all our data.
	 *                         With out this oid, no data can be retrieved.
	 * @param Locale         - This is the current users locale, it will help in establishing 
	 *                         the client Server data bridge as well as building various B.O.s
	 * @param BankPrefix     - This may be removed in the future.
	 * @param ContextPath    - the context path under which this request was made
	 * @return OutputDoc     - Send out the result of building our Document.
	 */
	protected DocumentHandler populateXmlDoc(DocumentHandler outputDoc, String key, String locale, String bankPrefix, String contextPath)
	throws RemoteException, AmsException
	{
		Long transactionOid    = new Long(key);
		if ( "blueyellow".equals(bankPrefix) ) {
			bankPrefix = TradePortalConstants.DEFAULT_BRANDING;
		  }
		 if ( "anztransac".equals(bankPrefix) ) {
			 bankPrefix = "anz";			 
		  }
		String logo = bankPrefix + "/images";

		String transactionType = null;
		
		//Business Objects needed for outgoing data
		Transaction trans           = null;
		Instrument instrument       = null;
		OperationalBankOrganization opBankOrg = null;
		Terms terms                 = null;     //Current ACTIVE terms
		Terms termsOriginal         = null;     //Original Terms account prior to amendment.
		Transaction transOriginal   = null;
		TermsParty draweeTermsParty = null;     //Drawee  (as defined in the Terms oid fields)
		TermsParty drawerTermsParty = null;     //Drawer
		TermsParty bankTermsParty   = null;     //Collecting Bank
		TermsParty caseOfNeedTermsParty = null; //Case of Need - Added due to change request log - 6/27/01
		
		ClientServerDataBridge csdb = new ClientServerDataBridge();
		csdb.setLocaleName(locale);
		
		String serverLocation             = JPylonProperties.getInstance().getString("serverLocation");
		
		//Get access to the Reference Data manager  -- this is a singleton
		ReferenceDataManager refDataMgr = ReferenceDataManager.getRefDataMgr();
		
		// Nar CR-938 rel9500 02/25/2016 - set indicator for custom Enable PDF config setting.
		// get Custom PDF indicator
	     enableCustomPDF = ConfigSettingManager.getConfigSetting("PORTAL_APP", "PORTAL", "EnableCustomPDF", "N");
			
		//Create Transaction
		trans = (Transaction)EJBObjectFactory.createClientEJB( serverLocation,
												"Transaction",transactionOid.longValue(), csdb);

		//A transaction type can be: "ISS" / "AMD" / "TRC" -- this variable will help 
		//reduce the number of times we need to fetch this information, by keeping it in resident memory.
		transactionType = trans.getAttribute("transaction_type_code");
					
		//set up a Instrument object
		instrument = (Instrument)EJBObjectFactory.createClientEJB(serverLocation,
													"Instrument", trans.getAttributeLong("instrument_oid"), csdb);
													
		//set up a Operational Bank Org object
		opBankOrg = (OperationalBankOrganization)EJBObjectFactory.createClientEJB(serverLocation,
													"OperationalBankOrganization", 
													 instrument.getAttributeLong("op_bank_org_oid"), csdb);
			
			  
		//set up a Terms component off of the transaction
		terms = (Terms)trans.getComponentHandle( "CustomerEnteredTerms" );
		if( !transactionType.equals( TransactionType.ISSUE ) )
		{
			transOriginal = (Transaction)EJBObjectFactory.createClientEJB(serverLocation,
													"Transaction", instrument.getAttributeLong("original_transaction_oid"), csdb);
				
			termsOriginal = (Terms) transOriginal.getComponentHandle( "CustomerEnteredTerms" );
			draweeTermsParty = (TermsParty)termsOriginal.getComponentHandle("FirstTermsParty");      //Drawee
			drawerTermsParty = (TermsParty)termsOriginal.getComponentHandle("SecondTermsParty");     //Drawer
			bankTermsParty   = (TermsParty)termsOriginal.getComponentHandle("ThirdTermsParty");      //Collection Bank
			// The case of need party is not required and can therefore be empty.
			// Do not get it if there is not component.
			String partyOid = termsOriginal.getAttribute("c_FourthTermsParty");
			if (StringFunction.isNotBlank(partyOid)) {
				caseOfNeedTermsParty = (TermsParty)termsOriginal.getComponentHandle("FourthTermsParty");  //Case Of Need
			} else {
				caseOfNeedTermsParty = null;
			}

		}
		else
		{
			  //*** Terms is a 1 to 1 component ***

			//set up a Terms Party Object(s)
			draweeTermsParty = (TermsParty)terms.getComponentHandle("FirstTermsParty");      //Drawee
			drawerTermsParty = (TermsParty)terms.getComponentHandle("SecondTermsParty");     //Drawer
			bankTermsParty   = (TermsParty)terms.getComponentHandle("ThirdTermsParty");      //Collection Bank
			// The case of need party is not required and can therefore be empty.
			// Do not get it if there is not component.
			String partyOid = terms.getAttribute("c_FourthTermsParty");
			if (StringFunction.isNotBlank(partyOid)) {
				caseOfNeedTermsParty = (TermsParty)terms.getComponentHandle("FourthTermsParty");  //Case Of Need
			} else {
				caseOfNeedTermsParty = null;
			}
		}
			  //TermsParty(s) are a 1 to 1 components
		try{      
					 
 //Section 'A' -- Bank Logo and document reference #            
			  sectionA( outputDoc, terms, logo, instrument, contextPath, transactionType );
			  
 //Section 'B' -- Operational Bank Org info    
			  sectionB( outputDoc, opBankOrg, transactionType, terms, termsOriginal, locale, bankTermsParty, refDataMgr );
			  
 //Section 'C'
              if( transactionType.equals( TransactionType.TRACER ) )
			    sectionC( outputDoc, opBankOrg, transactionType, terms, locale, termsOriginal);
			  else
			    sectionC( outputDoc, opBankOrg, transactionType, terms, locale, null );      
			  
 //Section 'D' -- Building 2 vectors so that sorting the data is done automatically by the vector index(s).        
			  sectionD( outputDoc, draweeTermsParty, drawerTermsParty, refDataMgr, locale );
			   
 //Section 'E' -- Collection Amount / Date
			  if( transactionType.equals( TransactionType.ISSUE ) )
				sectionE( outputDoc, null, transactionType, terms, locale, refDataMgr); 
			  else
				sectionE( outputDoc, termsOriginal, transactionType, terms, locale, refDataMgr);
			  
 //Section 'F' -- Tenor info
			  sectionF( outputDoc, terms, termsOriginal, transactionType, refDataMgr, locale, trans );
			   
 //Section 'G' -- Documents enclosed...     
			  
			  if( transactionType.equals( TransactionType.ISSUE ) )
			  {
				sectionG( outputDoc, terms);           
			  
 //Section 'H' -- Instructions  
				sectionH( outputDoc, terms, caseOfNeedTermsParty);
				
			  }//end of transactionType == Issue
			  
 //Section 'I' -- Additional Instructions             
			  sectionI( outputDoc, terms, transactionType );
 
 //Section 'J' -- Settlement Instructions
			  sectionJ( outputDoc, terms, transactionType );
			  
 //Section 'K' -- Uniform Rules for Collection - year/publication number            
			  sectionK( outputDoc, terms, transactionType, termsOriginal );
				
 //Section 'L' -- Final Instruction               
			  outputDoc.setAttribute( "/SectionL/","" );   
					   
LOG.debug("OutputDoc == " + outputDoc.toString() );

	  }finally{  //remove any beans for clean up if they have data
		beanRemove( caseOfNeedTermsParty, "Case of Need Terms Party");
		beanRemove( trans, "Transaction");
		beanRemove( transOriginal, "TransactionOriginal");
		beanRemove( instrument, "Instrument");
		beanRemove( opBankOrg, "Operational Bank Org");
		beanRemove( termsOriginal, "TermsOriginal");
 
	  }

	  return outputDoc;


	}


	/**
	 * SectionA corresponds to the portion of the XML document labeled 'SectionA'.
	 * The following is an example of what the resulting Xml should look like:
	 * <ExportCollection>
	 *  <SectionA>
	 *    <Logo Bank="ANZ"></Logo>
	 *    <CollectionReference>123456789</CollectionReference>
	 *  </SectionA>
	 * </ExportCollection>
	 *
	 * @param Output Document       - This is the Xml document being built to be sent out for display.
	 * @param Terms Business Object - This is the object which we will get our data from for the XML doc.
	 * @param logo                  - This is the passed in Bank logo identifier.
	 */
	 private void sectionA(DocumentHandler outputDoc, TradePortalBusinessObject terms, String logo, Instrument instrument, String contextPath, String transactionType ) throws RemoteException, AmsException
	 {	
		//BSL Cocoon Upgrade 10/28/11 Rel 7.0 BEGIN
		//PropertyResourceBundle portalProperties = (PropertyResourceBundle) PropertyResourceBundle.getBundle("TradePortal");
		//String imageLocation = portalProperties.getString("producerRoot")+"/"+contextPath+"/images/";
		String imageLocation = getImageLocation();
		//BSL Cocoon Upgrade 10/28/11 Rel 7.0 END
		// Nar CR-938 rel9500 02/25/2016 - Add PDF header for custom PDF.
		if ( TradePortalConstants.INDICATOR_YES.equals(enableCustomPDF) ) {
			if ( TransactionType.ISSUE.equals(transactionType) ) {
				outputDoc.setAttribute( "/SectionA/Title/Line1", "Direct Send Collection");
			} else if ( TransactionType.AMEND.equals(transactionType) ) {
				outputDoc.setAttribute( "/SectionA/Title/Line1", "Direct Send Collection Amendment");
			}
		}

		outputDoc.setAttribute( "/SectionA/Logo", imageLocation + logo + "/");
		outputDoc.setAttribute( "/SectionA/CollectionReference/", instrument.getAttribute("complete_instrument_id") );
	 }


	/**
	 * SectionB corresponds to the portion of the XML document labeled 'SectionB'.
	 * The following is an example of what the resulting Xml should look like:
	 * <ExportCollection>
	 *   <SectionB>
	 *     <BankAddress>
	 *       <ComplexAddressLine>
	 *          <BankOrgName>ANZ Banking Group (New Zealand) Limited</BankOrgName>
	 *          <CollectionDateTag></CollectionDateTag>
	 *          <DateValue>27 March 2001</DateValue>
	 *       </ComplexAddressLine>
	 *       <ComplexAddressLine>
	 *          <BankOrgName>ANZ Banking Group (New Zealand) Limited</BankOrgName>
	 *          <AmendmentDateTag></AmendmentDateTag>
	 *          <DateValue>27 March 2001</DateValue>
	 *       </ComplexAddressLine>
	 *       <ComplexAddressLine>
	 *          <BankOrgName>ANZ Banking Group (New Zealand) Limited</BankOrgName>
	 *          <TracerDateTag></TracerDateTag>
	 *          <DateValue>27 March 2001</DateValue>
	 *       </ComplexAddressLine>
	 *       <BankAddressLine>International Trade Services Department</BankAddressLine>
	 *       <BankAddressLine>Cnr Queen and Victoria Streets</BankAddressLine>
	 *       <BankAddressLine>PO Box 62, Auckland</BankAddressLine>
	 *       <BankAddressLine>New Zealand</BankAddressLine>
	 *       <TelephoneNumber>(64-9) 358 9373</TelephoneNumber>
	 *       <FaxNumber>(64-9) 358 9388</FaxNumber>
	 *       <BankSwift>ANZBNZ22102</BankSwift>
	 *       <BankTelex>AA68210</BankTelex>
	 *     </BankAddress>
	 *     <CustomerAddress>
	 *       <CustomerAddressLine>International Bank for Collections</CustomerAddressLine>
	 *       <CustomerAddressLine>Suite 505, Export Trade House</CustomerAddressLine>
	 *       <CustomerAddressLine>400-05 5th Avenue</CustomerAddressLine>
	 *       <CustomerAddressLine>New York, NY</CustomerAddressLine>
	 *       <CustomerAddressLine>USA 10016</CustomerAddressLine>
	 *     </CustomerAddress>
	 *   </SectionB>
	 * <ExportCollection>
	 *
	 * @param Output Document           - This is the Xml document being built to be sent out for display.
	 * @param opBankOrg Business Object - This is the object which we will get our data from for the XML doc.
	 * @param TransactionType           - The type of transaction curently being analyzed: 'Amend', 'Issue', 'Tracer'.
	 * @param Terms Business Object     - This is the object which we will get our data from for the XML doc.
	 * @param TermsOriginal Bus.Object  - This is the original Transaction terms data definition.
	 * @param locale                    - This is the locale for the current user.
	 * @param TermsParty3 Bus Object    - This is the object which we will get our data from for the XML doc.
	 * @param Reference DataMgr - The RefData Mgr will help us convert the refdata code to it's description. <BR>         
	 */
	 
	 private void sectionB(DocumentHandler outputDoc, TradePortalBusinessObject opBankOrg,
						   String transactionType,    TradePortalBusinessObject terms,
													  TradePortalBusinessObject termsOriginal,
						   String locale,             TradePortalBusinessObject bankTermsParty,
						   ReferenceDataManager  refDataMgr) 
			 throws RemoteException, AmsException
	 {
			  String dateString = "";
			
			  outputDoc.setAttribute( "/SectionB/BankAddress/ComplexAddressLine/BankOrgName/",
									  opBankOrg.getAttribute("name") );
			  
			  //Determine which transaction type message to send out...
			  appendXmlAttribute( outputDoc, transactionType, "/SectionB/BankAddress/ComplexAddressLine/", 
											 "CollectionDateTag/", "AmendmentDateTag/", "TracerDateTag/", "" );
			  
			  if( !transactionType.equals( TransactionType.ISSUE ) )
			  {
				dateString = formatDate( terms, "collection_date", locale); 
			  }else{
				dateString = formatDate( terms, "collection_date", locale); 
			  }  
				
			  if( !dateString.equals("") )
			  {
				outputDoc.setAttribute("/SectionB/BankAddress/ComplexAddressLine/DateValue/", dateString );
			  }
			  
			  outputDoc.setAttribute( "/SectionB/BankAddress/BankAddressLine(1)/",
									  opBankOrg.getAttribute("address_line_1" ) );                                            
			  
			  addNonNullDocData( opBankOrg.getAttribute("address_line_2"), outputDoc, 
								 "/SectionB/BankAddress/BankAddressLine(2)/");                                            
			  
							
			  //Building a string for the City and state line...easier to read 
			  //and construct logic in event no data is in the database.
			  String cityState = null;
			  cityState = appendAttributes(opBankOrg.getAttribute("address_city"), 
										   opBankOrg.getAttribute("address_state_province"), ", " );
			  
			  if( isNotEmpty( cityState ) )
				outputDoc.setAttribute( "/SectionB/BankAddress/BankAddressLine(3)/",
										cityState );
			  
			  //Determine the string to send for the country and postal_codes...
			  String countryZip = null;
			  
			  String countryDescr;    
			  if(refDataMgr.checkCode(TradePortalConstants.COUNTRY, opBankOrg.getAttribute("address_country"), locale))
				{
					countryDescr = refDataMgr.getDescr(TradePortalConstants.COUNTRY, opBankOrg.getAttribute("address_country"), locale);
				}
			  else
				{
					countryDescr = "";  
				}              
			  
			  countryZip = appendAttributes(countryDescr, 
											opBankOrg.getAttribute("address_postal_code"), ", ");
			  
			  if( isNotEmpty( countryZip ) )
				outputDoc.setAttribute( "/SectionB/BankAddress/BankAddressLine(4)/",
										countryZip );
			  addNonNullDocData( opBankOrg.getAttribute("phone_number"), outputDoc, 
								 "/SectionB/BankAddress/TelephoneNumber/");
			  addNonNullDocData( opBankOrg.getAttribute("fax_1"), outputDoc, 
								 "/SectionB/BankAddress/FaxNumber/");

			  
			  String swift = opBankOrg.getAttribute("swift_address_part1") + opBankOrg.getAttribute("swift_address_part2");
			  //only send out the swift data if we have swift data...
			  if( isNotEmpty( swift ) )
				outputDoc.setAttribute( "/SectionB/BankAddress/BankSwift/", swift );

			  addNonNullDocData( opBankOrg.getAttribute("telex_1"), outputDoc, 
								 "/SectionB/BankAddress/BankTelex/");
			  
			  outputDoc.setAttribute( "/SectionB/CustomerAddress/CustomerAddressLine(1)/",
									  bankTermsParty.getAttribute("name") );
			  addNonNullDocData( bankTermsParty.getAttribute("address_line_1"), outputDoc, 
								 "/SectionB/CustomerAddress/CustomerAddressLine(2)/");
			  addNonNullDocData( bankTermsParty.getAttribute("address_line_2"), outputDoc, 
								 "/SectionB/CustomerAddress/CustomerAddressLine(3)/");
									  
			  //Building a string for the City and state line...easier to read 
			  //and construct logic in event no data is in the database.
			  cityState = appendAttributes(bankTermsParty.getAttribute("address_city"), 
										   bankTermsParty.getAttribute("address_state_province"), ", ");
			  
			  if( isNotEmpty( cityState ) )
				outputDoc.setAttribute( "/SectionB/CustomerAddress/CustomerAddressLine(4)/",
										cityState );
										
 
			 if(refDataMgr.checkCode(TradePortalConstants.COUNTRY, bankTermsParty.getAttribute("address_country"), locale))
			  {
				countryDescr = refDataMgr.getDescr(TradePortalConstants.COUNTRY, bankTermsParty.getAttribute("address_country"), locale);
			  }
			  else
			  {
				countryDescr = "";  
			  }                                        
									  
			  //Determine the string to send for the country and postal_codes...
			  countryZip = appendAttributes(countryDescr,
											bankTermsParty.getAttribute("address_postal_code"), ", ");
			  
			  if( isNotEmpty( countryZip ) )
				outputDoc.setAttribute( "/SectionB/CustomerAddress/CustomerAddressLine(5)/",
										countryZip );
	 }


	/**
	 * SectionC corresponds to the portion of the XML document labeled 'SectionC'.
	 * The following is an example of what the resulting Xml should look like:
	 * <ExportCollection>
	 *   <SectionC>
	 *      <IssueStatement></IssueStatement>
	 *      <AmendStatement></AmendStatement>
	 *      <TracerStatement></TracerStatement>
	 *   </SectionC>
	 * </ExportCollection>
	 *
	 * @param Output Document           - This is the Xml document being built to be sent out for display.
	 * @param opBankOrg Business Object - This is the object which we will get our data from for the XML doc.
	 * @param TransactionType           - The type of transaction curently being analyzed: 'Amend', 'Issue', 'Tracer'.
	 * @param Terms Business Object     - This is the object which we will get our data from for the XML doc.
	 * @param locale                    - This is the users current Locale.
	 *
	 */
	 
	 private void sectionC(DocumentHandler outputDoc, TradePortalBusinessObject opBankOrg,
						   String transactionType,    TradePortalBusinessObject terms,
						   String locale,             TradePortalBusinessObject termsOriginal) 
			 throws RemoteException, AmsException
	 {
			  String dateString = "";
			 			
			  //Determine which transaction type message to send out...
			  if( transactionType.equals( TransactionType.TRACER ) )
			  {
				//Format the date to be displayed 
				dateString = termsOriginal!=null?formatDate( termsOriginal, "collection_date", locale):"";
				if( !dateString.equals("") )
				{
					outputDoc.setAttribute("/SectionC/TracerStatementDate/", dateString );
				}
				
				outputDoc.setAttribute("/SectionC/TracerStatementBankOrg/", opBankOrg.getAttribute("name") );
			  
			  }else
				appendXmlAttribute( outputDoc, transactionType, "/SectionC/", 
									"IssueStatement/", "AmendStatement/", "TracerStatement/", "" );                  
	 }


	/**
	 * SectionD corresponds to the portion of the XML document labeled 'SectionD'.
	 * The following is an example of what the resulting Xml should look like:
	 * <ExportCollection>
	 *   <SectionD>
	 *      <DrawerDraweeRow>
	 *         <DrawerDraweeCell>Sinochem Jiangsu Wuxi</DrawerDraweeCell>
	 *         <DrawerDraweeCell>Rothmans Co.</DrawerDraweeCell>
	 *      </DrawerDraweeRow>
	 *      <DrawerDraweeRow>
	 *         <DrawerDraweeCell>Import / Export Coprporation</DrawerDraweeCell>
	 *         <DrawerDraweeCell>100 Sauva St.</DrawerDraweeCell>
	 *      </DrawerDraweeRow>
	 *      <DrawerDraweeRow>
	 *         <DrawerDraweeCell>Japan</DrawerDraweeCell>
	 *         <DrawerDraweeCell>French Quarter</DrawerDraweeCell>
	 *      </DrawerDraweeRow>
	 *      <DrawerDraweeRow>
	 *         <DrawerDraweeCell></DrawerDraweeCell>
	 *         <DrawerDraweeCell>Montreal QU</DrawerDraweeCell>
	 *      </DrawerDraweeRow>
	 *      <DrawerDraweeRow>
	 *         <DrawerDraweeCell></DrawerDraweeCell>
	 *         <DrawerDraweeCell>Canada H2Y 1M2</DrawerDraweeCell>
	 *      </DrawerDraweeRow>
	 *   </SectionD>
	 * </ExportCollection>
	 *
	 * @param Output Document             - This is the Xml document being built to be sent out for display.
	 * @param TermsParty1 Business Object - This is the object which we will get our data from for the XML doc.
	 * @param TermsParty2 Business Object - This is the object which we will get our data from for the XML doc.
	 * @param Reference DataMgr - The RefData Mgr will help us convert the refdata code to it's description.      
	 * @param Locale            - This is the users current locale. 
	 *
	 */
	 
	 private void sectionD(DocumentHandler outputDoc, TradePortalBusinessObject draweeTermsParty,
													  TradePortalBusinessObject drawerTermsParty,
													  ReferenceDataManager refDataMgr,
													  String locale) 
			 throws RemoteException, AmsException
	 {
		Vector drawer     = new Vector();
		Vector drawee     = new Vector();
		String cityState  = "";
		String countryZip = "";

			  //The essential problem is, whatever data that does Not exist in the database, because not all
			  //the field are required to be filled out, we don't want to display a blank space for missing
			  //data.  So by adding everything into a vector, we can easily eliminate the blank spaces as we
			  //only add data to the vector if we find data.  Then it's just a matter of looping through the
			  //vectors to output the data.
			  
			  //Drawer-Drawee : Name
			  drawer = addNonNullElement( drawerTermsParty.getAttribute("name"), drawer);
			  drawee = addNonNullElement( draweeTermsParty.getAttribute("name"), drawee);
			  
			  //Drawer-Drawee : Address Line 1
			  drawer = addNonNullElement( drawerTermsParty.getAttribute("address_line_1"), drawer);
			  drawee = addNonNullElement( draweeTermsParty.getAttribute("address_line_1"), drawee);
			  
			  //Drawer-Drawee : Address Line 2  (if it exists)
			  drawer = addNonNullElement( drawerTermsParty.getAttribute("address_line_2"), drawer);
			  drawee = addNonNullElement( draweeTermsParty.getAttribute("address_line_2"), drawee);
			  
			  //Drawer-Drawee : Address Line 3  (if it exists)
			  drawer = addNonNullElement( drawerTermsParty.getAttribute("address_line_3"), drawer);
			  drawee = addNonNullElement( draweeTermsParty.getAttribute("address_line_3"), drawee);
			  
			  //Drawer-Drawee : City - State  (if either one exists)
			  cityState =  appendAttributes(drawerTermsParty.getAttribute("address_city"), 
											drawerTermsParty.getAttribute("address_state_province"), ", " );
			  if( isNotEmpty( cityState ) )
				drawer.addElement( cityState );
			  
			  cityState =  appendAttributes(draweeTermsParty.getAttribute("address_city"), 
											draweeTermsParty.getAttribute("address_state_province"), ", " );
			  if( isNotEmpty( cityState ) )
				drawee.addElement( cityState );
			  
			  //Drawer : Country - PostalCode
			String countryDescr;    
			if(refDataMgr.checkCode(TradePortalConstants.COUNTRY, drawerTermsParty.getAttribute("address_country"), locale))
			{
				countryDescr = refDataMgr.getDescr(TradePortalConstants.COUNTRY, drawerTermsParty.getAttribute("address_country"), locale);
			}
			else
			{
				countryDescr = "";  
			}              
			  
			  
			  countryZip = appendAttributes(countryDescr,  
											drawerTermsParty.getAttribute("address_postal_code"), ", " );
			  drawer.addElement( countryZip );
			  
  
			  if(refDataMgr.checkCode(TradePortalConstants.COUNTRY, draweeTermsParty.getAttribute("address_country"), locale))
			  {
				countryDescr = refDataMgr.getDescr(TradePortalConstants.COUNTRY, draweeTermsParty.getAttribute("address_country"), locale);
			  }
			 else
			  {
				countryDescr = "";  
			  }              
			  
			  countryZip = appendAttributes(countryDescr, 
											draweeTermsParty.getAttribute("address_postal_code"), ", " );
			  drawee.addElement( countryZip );
			  
			  //Extracting data out of vectors and putting it into the output document.
			  int max;
			  if( drawer.size() >= drawee.size() )          //Determine the larger of the two vectors -
					max = drawer.size();                    //so we know how long to loop(thru the data)
			  else
					max = drawee.size();
					
			  for(int x=0; x<max; x++)                      //Loop through vectors pushing data out 
			  {                                             //to the Document Handler, If no data is
				int rowNum = x+1;                                //found insert a place holder until the other
					  //drawer                              //terms_Party is finished.
				if( drawer.size() < (x+1) )
				{                                           //insert a blank placeholder here
					outputDoc.setAttribute("/SectionD/DrawerDraweeRow(" + rowNum + ")/DrawerDraweeCell(1)/", "");
					
				}else{                                      //get actual data
					outputDoc.setAttribute("/SectionD/DrawerDraweeRow(" + rowNum + ")/DrawerDraweeCell(1)/", 
										   (String)drawer.elementAt(x) );
				}
				
					 //drawee
				if( drawee.size() < (x+1) )
				{                                           //insert a blank placeholder here
					outputDoc.setAttribute("/SectionD/DrawerDraweeRow(" + rowNum + ")/DrawerDraweeCell(2)/", "");
					
				}else{                                      //get actual data
					outputDoc.setAttribute("/SectionD/DrawerDraweeRow(" + rowNum + ")/DrawerDraweeCell(2)/", 
										   (String)drawee.elementAt(x) );
				}
			  }
	 }


	/**
	 * SectionE corresponds to the portion of the XML document labeled 'SectionE'.
	 * The SpellItOut Class in the presentation package handles the conversion of the numeric data into it's
	 * textual representation.  Of note: the text of that number is displayed in lower case.
	 * The following is an example of what the resulting Xml should look like:
	 * <ExportCollection>
	 *   <SectionE>
	 *      <CollectionTable>
	 *         <CollectionRow>USD 108,650.06</CollectionRow>
	 *            <AmendTracerRow>
	 *              <CollectionAmount>USD 108,650.06</CollectionAmount>
	 *              <CollectionDate>27 February 2001</CollectionDate>
	 *            </AmendTracerRow>
	 *      </CollectionTable>
	 *      <CollectionAmount>United States Dollars One hundred and eight throusand six hundred and fifty and 6/100
	 *      </CollectionAmount>
	 *      <NewCollection>
	 *         <CollectionAmount>USD 108,650.06</CollectionAmount>
	 *         <CollectionAmountWords>United States Dollars One hundred and eight throusand six hundred and fifty and 6/100</CollectionAmountWords>
	 *      </NewCollection>
	 *   </SectionE>
	 * </ExportCollection>
	 *
	 * @param Output Document           - This is the Xml document being built to be sent out for display.
	 * @param Terms Original Business Object - This is the object which we will get our data from for the XML doc.
	 * @param TransactionType           - The type of transaction curently being analyzed: 'Amend', 'Issue', 'Tracer'.
	 * @param Terms Business Object     - This is the object which we will get our data from for the XML doc.
	 * @param locale                    - This is the current users Locale.
	 * @param ReferenceDataMgr          - This converts the ref data code into it's description.
	 */
	 
	 private void sectionE(DocumentHandler outputDoc, TradePortalBusinessObject termsOriginal,
						   String transactionType,    TradePortalBusinessObject terms,
						   String locale,             ReferenceDataManager refDataMgr)
			 throws RemoteException, AmsException
	 {
			  //Need to handle special logic here in the event that an amendment has been made, 
			  //you would have a current amount and an original amount: ie 2 differing pieces of 
			  //data to display.  This is the only example of this logic, so the code is kept here.
			  String codeAndCurrency = null;
			  String collectionDate = null;
			  String xmlPathAmount = null;
			  String currencyCode = null;
			  String currencyAmount = null;
			  
			  if ( transactionType.equals( TransactionType.AMEND ) )            //Amendment
			  {
 				codeAndCurrency = appendAttributes ( termsOriginal.getAttribute("amount_currency_code"), 
                  			 TPCurrencyUtility.getDisplayAmount(termsOriginal.getAttribute("amount"),             
                                                                            termsOriginal.getAttribute("amount_currency_code"), 
                                                                            locale), " " );

				collectionDate = formatDate( termsOriginal, "collection_date", locale);
				xmlPathAmount = "/SectionE/CollectionTable/AmendTracerRow/CollectionAmount/";
				currencyCode = termsOriginal.getAttribute("amount_currency_code");
				currencyAmount = termsOriginal.getAttribute("amount");
				
			  }else if ( transactionType.equals( TransactionType.ISSUE ) )      //Issue
			  {
				codeAndCurrency = appendAttributes ( terms.getAttribute("amount_currency_code"), 
								     TPCurrencyUtility.getDisplayAmount(terms.getAttribute("amount"), terms.getAttribute("amount_currency_code"), locale), " " );
				xmlPathAmount = "/SectionE/CollectionTable/CollectionRow/";
				currencyCode = terms.getAttribute("amount_currency_code");
				currencyAmount = terms.getAttribute("amount");

			  }else                                                                  //Tracer
			  {
				codeAndCurrency = appendAttributes ( termsOriginal.getAttribute("amount_currency_code"), 
								     TPCurrencyUtility.getDisplayAmount(termsOriginal.getAttribute("amount"), termsOriginal.getAttribute("amount_currency_code"), locale), " " );
				collectionDate = formatDate( termsOriginal, "collection_date", locale);      
				xmlPathAmount = "/SectionE/CollectionTable/AmendTracerRow/CollectionAmount/";
				currencyCode = termsOriginal.getAttribute("amount_currency_code");
				currencyAmount = termsOriginal.getAttribute("amount");
			  }
			  
			  //send out the CurrencyCode and Amounts to Doc
			  if( isNotEmpty( codeAndCurrency ) ) 
				outputDoc.setAttribute( xmlPathAmount, codeAndCurrency );
			  
			  //if a collection date is defined then we know we're in amend/tracer mode...
			  if( isNotEmpty( collectionDate ) )
				outputDoc.setAttribute( "/SectionE/CollectionTable/AmendTracerRow/CollectionDate/", 
										collectionDate );
														  
			  //Textual representation of the Currency Amount
			  if( isNotEmpty( codeAndCurrency ) ) 
			  {
			String decimalPoint = refDataMgr.getAdditionalValue(TradePortalConstants.CURRENCY_CODE, currencyCode, locale);
				//Get the ref data description for the stored code
				currencyCode   = refDataMgr.getDescr( TradePortalConstants.CURRENCY_CODE, currencyCode, locale );
				//Convert the amount to it's textual representation....(all lower case)
			currencyAmount = SpellItOut.spellOutNumber(currencyAmount, locale, decimalPoint);
				outputDoc.setAttribute( "/SectionE/CollectionAmount", currencyCode + " " + currencyAmount);
			  }
			  //Now for the new amount in the event we're in Amendment mode - display the new value for the amount
			  if( transactionType.equals( TransactionType.AMEND ) )
			  {
 				codeAndCurrency = appendAttributes ( terms.getAttribute("amount_currency_code"), 
                  			 TPCurrencyUtility.getDisplayAmount(terms.getAttribute("amount"),             
                                                                            terms.getAttribute("amount_currency_code"), 
                                                                            locale), " " );

				if( isNotEmpty( codeAndCurrency ) ) 
				{
					outputDoc.setAttribute( "/SectionE/NewCollection/CollectionAmount/", codeAndCurrency );
					currencyCode   = terms.getAttribute("amount_currency_code");
					currencyAmount = terms.getAttribute("amount");
				String decimalPoint = refDataMgr.getAdditionalValue(TradePortalConstants.CURRENCY_CODE, currencyCode, locale);
					//Get the ref data description for the stored code
					currencyCode   = refDataMgr.getDescr( TradePortalConstants.CURRENCY_CODE, currencyCode, locale );
					//Convert the amount to it's textual representation....(all lower case)
				currencyAmount = SpellItOut.spellOutNumber(currencyAmount, locale, decimalPoint);

					outputDoc.setAttribute( "/SectionE/NewCollection/CollectionAmountWords/", 
											currencyCode + " " + currencyAmount);
				}
			  }
	 }


	/**
	 * SectionF corresponds to the portion of the XML document labeled 'SectionF'.
	 * The following is an example of what the resulting Xml should look like:
	 * <ExportCollection>
	 *   <SectionF>
	 *      <Sight></Sight>
	 *      <CashAgnDocs></CashAgnDocs>
	 *      <NumberOfDays>
	 *         <TenorDays>10</TenorDays>
         *         <TenorType>Issue</TenorType>
	 *      </NumberOfDays>
	 *      <AtFixedMature>12 Mar 2002</AtFixedMature>
	 *      <MultipleTenors>
	 *         <TenorLine(1)>
	 *            <RowNumber>1</RowNumber>
	 *            <Tenor>Sight</Tenor>
	 *            <Amount>USD 50,000.00</Amount>
	 *            <DraftNumber>80001</DraftNumber>
	 *         </TenorLine>
	 *         <TenorLine(2)>
	 *            <RowNumber>2</RowNumber>
	 *            <Tenor>30 days sight</Tenor>
	 *            <Amount>USD 25,000.00</Amount>
	 *            <DraftNumber>30002</DraftNumber>
	 *          </TenorLine>
	 *          <TenorLine(3)>
	 *             <RowNumber>3</RowNumber>
	 *             <Tenor>90 days sight</Tenor>
	 *             <Amount>USD 25,000.00</Amount>
	 *             <DraftNumber>80003</DraftNumber>
	 *          </TenorLine>
	 *          <TenorLine(4)>
	 *             <RowNumber>4</RowNumber>
	 *             <Tenor>Fixed Maturity Date-31 Oct 01</Tenor>
	 *             <Amount>USD 50,000.00</Amount>
	 *             <DraftNumber>80005</DraftNumber>
	 *          </TenorLine>
	 *       </MultipleTenors>
	 *    </SectionF>
	 * </ExportCollection>
	 *<STRONG>
	 *Please Note: there can be up to 6 tenors (as defined by the object Model)
	 *</STRONG>
	 *
	 * @param Output Document       - This is the Xml document being built to be sent out for display.
	 * @param Terms Business Object - This is the object which we will get our data from for the XML doc.
	 * @param TermsOriginal         - This is the terms of the original transaction.
	 * @param TransactionType       - The type of transaction curently being analyzed: 'Amend', 'Issue', 'Tracer'.
	 * @param RefDataMgr            - This is the Reference Data manager so that we can convert the code in the
	 *                                in the refdata table to it's corresponding description.
	 * @param locale                - This is the users current Locale.
	 * @param Trans Business Object - This is the object which we will get our data from for the XML doc.
	 */
	
	 private void sectionF(DocumentHandler outputDoc, TradePortalBusinessObject terms, TradePortalBusinessObject termsOriginal,
						   String transactionType,    ReferenceDataManager refDataMgr,
						   String locale,             TradePortalBusinessObject trans)    
			 throws RemoteException, AmsException
	 {
			  String dateString = "";
			  String tenorTest = null;
                          String currencyCode = null;
			  
			  if( transactionType.equals( TransactionType.ISSUE ) )
			  {
				  String paymentTermsType = terms.getAttribute("pmt_terms_type");
				  outputDoc.setAttribute( "/SectionF/Issue/", "");
				  
				  if( paymentTermsType.equals( TradePortalConstants.PMT_SIGHT ) )                   //Sight
				  {
					outputDoc.setAttribute( "/SectionF/Sight/", "");
				  
				  
				  }else if( paymentTermsType.equals( TradePortalConstants.PMT_DAYS_AFTER ) )        //Days After Event
				  { 
					//Get the ref data description for the stored code
					String desc = refDataMgr.getDescr(TradePortalConstants.PMT_TERMS_DAYS_AFTER, 
													  terms.getAttribute("pmt_terms_days_after_type"),
													  locale);
					outputDoc.setAttribute( "/SectionF/NumberOfDays/TenorType/", desc);
					outputDoc.setAttribute( "/SectionF/NumberOfDays/TenorDays/", 
										 terms.getAttribute("pmt_terms_num_days_after") );
					
					
				  }else if( paymentTermsType.equals( TradePortalConstants.PMT_FIXED_MATURITY ) )    //At Fixed Maturity
				  {
					dateString = formatDate( terms, "pmt_terms_fixed_maturity_date", locale);
					if( !dateString.equals("") )
					{
						outputDoc.setAttribute( "/SectionF/AtFixedMature/", dateString );
					}
					
					
				  }else if( paymentTermsType.equals( TradePortalConstants.PMT_CASH_AGAINST ) )      //Cash Against Documents
				  {
					outputDoc.setAttribute( "/SectionF/CashAgnDocs/", "");
					
				  }else                                                                             //Other
				  {
					for(int x=1; x <= 6; x++)      //6 is the number of possible tenors from the db definition
					{
						//If the next tenor has no data quit the loop of retrieving upto 6 tenor(s)
						tenorTest = terms.getAttribute("tenor_" + x );
						
						if( tenorTest.equals("") )   
							break;
						
                                                currencyCode   = terms.getAttribute("amount_currency_code");
						addTenorToList( terms, trans, outputDoc, x, currencyCode, locale);
					
					}//End of Loop
				  }//End of paymentTermsType 
				  
				  
			  //End ofTransactionType == 'Issue'
			  }else if( transactionType.equals( TransactionType.AMEND ) )
			  {
				  outputDoc.setAttribute( "/SectionF/Amendment/", "");

				//If the transactionType is Amendment, then just buid whatever List of tenors there are...
					for(int x=1; x <= 6; x++)       //6 is the number of possible tenors from the db definition
					{
						//If the next tenor has no data quit the loop of retrieving upto 6 tenor(s)
						tenorTest = terms.getAttribute("tenor_" + x );
						
						if( tenorTest.equals("") )   
							break;

						currencyCode   = termsOriginal.getAttribute("amount_currency_code");
						addTenorToList( terms, trans, outputDoc, x, currencyCode, locale);
					
					}
			   }//End of paymentTermsType  
			  
			  //If the transaction Type is Tracer, don't bother creating anylist at all.
	 }


	/**
	 * SectionG corresponds to the portion of the XML document labeled 'SectionG'.
	 * The following is an example of what the resulting Xml should look like:
	 * <ExportCollection>
	 *   <SectionG>
	 *      <BillOfEx>2</BillOfEx>
	 *      <CommInvoice>5</CommInvoice>
	 *      <BillOfLading>3/3</BillOfLading>
	 *      <NonNegBillOfLading>3</NonNegBillOfLading>
	 *      <PackingList>5</PackingList>
	 *      <DocumentText>3 copies of phitosanitary certificate.  2 copies of weight list. 4 copies of visaed proforma invoice. 
	 *      </DocumentText>
	 *   </SectionG>
	 * </ExportCollection>
	 *
	 * @param Output Document       - This is the Xml document being built to be sent out for display.
	 * @param Terms Business Object - This is the object which we will get our data from for the XML doc.
	 *
	 */
	 
	 private void sectionG(DocumentHandler outputDoc, TradePortalBusinessObject terms) throws RemoteException, AmsException
	 {
			  // This may be a little painful, but each check box for each different field needs 
			  //to be evaluated. So this has the potential to look cumbersome.
			 
				//Bill Of Exchange
				if( checkFlagValue( terms.getAttribute("present_bills_of_exchange") ) )
					addNonNullDocData( terms.getAttribute("present_num_bills_of_exchange"), outputDoc, 
									   "/SectionG/BillOfEx/");
				
				//Commercial Invoice
				if( checkFlagValue( terms.getAttribute("present_commercial_invoice") ) )
					addNonNullDocData( terms.getAttribute("present_num_commercial_invoice"), outputDoc, 
									   "/SectionG/CommInvoice/");
				
				//Bill of Lading
				if( checkFlagValue( terms.getAttribute("present_bill_of_lading") ) )
					addNonNullDocData( terms.getAttribute("present_num_bill_of_lading"), outputDoc, 
									   "/SectionG/BillOfLading/");
				
				//Non-Negotiable Bill Of Lading
				if( checkFlagValue( terms.getAttribute("present_non_neg_bill_of_lading") ) )
					addNonNullDocData( terms.getAttribute("present_num_non_neg_bill_ladng"), outputDoc, 
									   "/SectionG/NonNegBillOfLading/");
				
				//AirwayBill
				if( checkFlagValue( terms.getAttribute("present_air_waybill") ) )
					addNonNullDocData( terms.getAttribute("present_num_air_waybill"), outputDoc, 
									   "/SectionG/AirWaybill/");
				
				//Insurance Policy/Certificate 
				if( checkFlagValue( terms.getAttribute("present_insurance") ) )
					addNonNullDocData( terms.getAttribute("present_num_insurance"), outputDoc, 
									   "/SectionG/InsurancePolicy/");
				
				//Certificate of Origin 
				if( checkFlagValue( terms.getAttribute("present_cert_of_origin") ) )
					addNonNullDocData( terms.getAttribute("present_num_cert_of_origin"), outputDoc, 
									   "/SectionG/CertificateOfOrigin/");
				
				//Packing List
				if( checkFlagValue( terms.getAttribute("present_packing_list") ) )
					addNonNullDocData( terms.getAttribute("present_num_packing_list"), outputDoc, 
									   "/SectionG/PackingList/");
				
				//Document 'Other' text 
				if( checkFlagValue( terms.getAttribute("present_other") ) )
					addNonNullDocData( terms.getAttribute("present_other_text"), outputDoc, 
									   "/SectionG/DocumentText/");
	 }


	/**
	 * SectionH corresponds to the portion of the XML document labeled 'SectionH'.
	 * The following is an example of what the resulting Xml should look like:
	 * <ExportCollection>
	 *   <SectionH>
	 *      <ReleaseDocOnPay></ReleaseDocOnPay>
	 *      <ReleaseDocOnAccept></ReleaseDocOnAccept>
	 *      <AdvisePayByTelecom></AdvisePayByTelecom>
	 *      <AdviseAcceptByTelecom></AdviseAcceptByTelecom>
	 *      <AdviseNonPayByTelecom></AdviseNonPayByTelecom>
	 *      <AdviseNonAcceptByTelecom></AdviseNonAcceptByTelecom>
	 *      <NoteForNonPay></NoteForNonPay>
	 *      <ProtestForNonPay></ProtestForNonPay>
	 *      <NoteForNonAccept></NoteForNonAccept>
	 *      <ProtestForNonAccept></ProtestForNonAccept>
	 *      <IncludeOurCharges>USD 25.00</IncludeOurCharges>
	 *      <OverseasCharges></OverseasCharges>
	 *      <AllCharges></AllCharges>
	 *      <DontWaiveCharges></DontWaiveCharges>
	 *      <DontWaiveInterest></DontWaiveInterest>
	 *      <CollectInterestAt>10% p.a. for late payment</CollectInterestAt>
	 *      <InCaseOfNeed>
	 *      <Text>Mr. John Doe,44-207-999,3214</Text>
	 *      <HasAuthority></HasAuthority>
	 *      <HasNoAuthority></HasNoAuthority>
	 *    </InCaseOfNeed>
	 *   </SectionH>
	 * </ExportCollection>
	 *
	 * @param Output Document       - This is the Xml document being built to be sent out for display.
	 * @param Terms Business Object - This is the object which we will get our data from for the XML doc.
	 *
	 */
	 
	 private void sectionH(DocumentHandler outputDoc, TradePortalBusinessObject terms, TradePortalBusinessObject caseOfNeedTermsParty)
						throws RemoteException, AmsException
	 {
				//Release Documents on Payment
				if( checkFlagValue( terms.getAttribute("instr_release_doc_on_pmt") ) )
					outputDoc.setAttribute( "/SectionH/ReleaseDocOnPay/", "" );
				
				//Release Documents on Acceptance
				if( checkFlagValue( terms.getAttribute("instr_release_doc_on_accept") ) )
					outputDoc.setAttribute( "/SectionH/ReleaseDocOnAccept/", "" );
			  
				//Advise Payment by Telecommunication
				if( checkFlagValue( terms.getAttribute("instr_advise_pmt_by_telco") ) )
					outputDoc.setAttribute( "/SectionH/AdvisePayByTelecom/", "");
			  
				//Advise acceptance by telecommunication
				if( checkFlagValue( terms.getAttribute("instr_advise_accept_by_telco") ) )
					outputDoc.setAttribute( "/SectionH/AdviseAcceptByTelecom/", "");
			  
				//Advise Non Payment by telecommunication
				if( checkFlagValue( terms.getAttribute("instr_advise_non_pmt_by_telco") ) )
					outputDoc.setAttribute( "/SectionH/AdviseNonPayByTelecom/", "");
			  
				//Advise Non Acceptance by telecommunication
				if( checkFlagValue( terms.getAttribute("instr_advise_non_accept_by_tel") ) )
					outputDoc.setAttribute( "/SectionH/AdviseNonAcceptByTelecom/", "");
			  
				//Note For Non Payment
				if( checkFlagValue( terms.getAttribute("instr_note_for_non_pmt") ) )
					outputDoc.setAttribute( "/SectionH/NoteForNonPay/", "");
			  
				//Protest For Non Payment
				if( checkFlagValue( terms.getAttribute("instr_protest_for_non_pmt") ) )
					outputDoc.setAttribute( "/SectionH/ProtestForNonPay/", "");
			  
				//Note For Non Acceptance
				if( checkFlagValue( terms.getAttribute("instr_note_for_non_accept") ) )    
					outputDoc.setAttribute( "/SectionH/NoteForNonAccept/", "");
			  
				//Protest for Non Acceptance
				if( checkFlagValue( terms.getAttribute("instr_protest_for_non_accept") ) )    
					outputDoc.setAttribute( "/SectionH/ProtestForNonAccept/", "");
			  
				//Charges
				String chargeType = terms.getAttribute("collection_charges_type");

		//*************************************************************************************************
		//***  Upcoming changes to RefData will affect this code here ***   Collection Charges info   *****
		//*************************************************************************************************
				if( isNotEmpty( chargeType ) )
				{
					//All Possible charges
					if( chargeType.equals( TradePortalConstants.COLLECTION_CHARGES_TYPE_ALL ) ) 
					{       addNonNullDocData( terms.getAttribute("collection_charges_includ_txt"), outputDoc, 
											   "/SectionH/IncludeOurCharges/" );
			  
					//Overseas -- charges by drawee
					}else if( chargeType.equals( TradePortalConstants.COLLECTION_CHARGES_TYPE_DRAWEE ) ) 
					{           outputDoc.setAttribute( "/SectionH/OverseasCharges/", ""); 
											   
					//All charges for account of drawer
					}else if( chargeType.equals( TradePortalConstants.COLLECTION_CHARGES_TYPE_DRAWER ) ) 
					{           outputDoc.setAttribute( "/SectionH/AllCharges/", "");
					}
				}
		//*************************************************************************************************              
				
				//Dont Waive refused charges
				if( checkFlagValue( terms.getAttribute("do_not_waive_refused_charges") ) )
					outputDoc.setAttribute( "/SectionH/DontWaiveCharges/", "");
				   
				//Dont Waive refused interest    
				if( checkFlagValue( terms.getAttribute("do_not_waive_refused_interest") ) )
					outputDoc.setAttribute( "/SectionH/DontWaiveInterest/", "");
				
				//Collect Interest At            
				if( checkFlagValue( terms.getAttribute("collect_interest_at_indicator") ) )
					addNonNullDocData( terms.getAttribute("collect_interest_at"), outputDoc, 
									   "/SectionH/CollectInterestAt/" );
			  
		//*************************************************************************************************
		//***  Upcoming changes to RefData will affect this code here ***   In Case Of Need info      *****
		//*************************************************************************************************
				//In Case Of Need
				String authority = terms.getAttribute("in_case_of_need_use_instruct");
				String caseOfNeedText = terms.getAttribute("in_case_of_need_contact_text");

				if (caseOfNeedTermsParty != null) {
					addNonNullDocData( caseOfNeedTermsParty.getAttribute("name"), outputDoc, 
													  "/SectionH/InCaseOfNeed/Name/");
					addNonNullDocData( caseOfNeedTermsParty.getAttribute("address_line_1"), outputDoc, 
													  "/SectionH/InCaseOfNeed/AddressLine1/");
					addNonNullDocData( caseOfNeedTermsParty.getAttribute("address_line_2"), outputDoc, 
													  "/SectionH/InCaseOfNeed/AddressLine2/");
					addNonNullDocData( caseOfNeedTermsParty.getAttribute("address_line_3"), outputDoc, 
													  "/SectionH/InCaseOfNeed/AddressLine3/");
				}
				if( isNotEmpty( authority ) )
				{
					//Accept Instructions from...
					if( authority.equals( TradePortalConstants.IN_CASE_OF_NEED_ACCEPT ) )
					{
						outputDoc.setAttribute("/SectionH/InCaseOfNeed/HasAuthority/", "");
						 
					//Only use Instructions as Guidance 
					}else if( authority.equals( TradePortalConstants.IN_CASE_OF_NEED_GUIDANCE ) )
						outputDoc.setAttribute("/SectionH/InCaseOfNeed/HasNoAuthority/", "");
				}
				
				if( isNotEmpty( caseOfNeedText ) )
				{
					outputDoc.setAttribute("/SectionH/InCaseOfNeed/AdditionalInstructions/", "");
				}
				addNonNullDocData( terms.getAttribute("in_case_of_need_contact_text"), outputDoc, 
									"/SectionH/InCaseOfNeed/Text/");
					
		//*************************************************************************************************              
	 }


	/**
	 * SectionI corresponds to the portion of the XML document labeled 'SectionI'.
	 * The following is an example of what the resulting Xml should look like:
	 * <ExportCollection>
	 *   <SectionI>Attached are drafts to replace the original drafts.
	 *   </SectionI>
	 * </ExportCollection>
	 *
	 * @param Output Document       - This is the Xml document being built to be sent out for display.
	 * @param Terms Business Object - This is the object which we will get our data from for the XML doc.
	 * @param TransactionType       - The type of transaction curently being analyzed: 'Amend', 'Issue', 'Tracer'.
	 *
	 */
	
	 private void sectionI(DocumentHandler outputDoc, TradePortalBusinessObject terms,
						   String transactionType)    throws RemoteException, AmsException
	 {
			  //Issue
			  if( transactionType.equals( TransactionType.ISSUE ) )
			  {
				addNonNullDocData( terms.getAttribute("instr_other"), outputDoc, "/SectionI/" );
			  
			  //Amendment
			  }else if( transactionType.equals( TransactionType.AMEND ) )
			  {
				addNonNullDocData( terms.getAttribute("amendment_details"), outputDoc, "/SectionI/" );
			  
			  //Tracer
			  }else if( transactionType.equals( TransactionType.TRACER ) )
			  {
				addNonNullDocData( terms.getAttribute("tracer_details"), outputDoc, "/SectionI/" );
			  }

	 }


	/**
	 * SectionJ corresponds to the portion of the XML document labeled 'SectionJ'.
	 * The following is an example of what the resulting Xml should look like:
	 * <ExportCollection>
	 *   <SectionJ>Please credit ANZ New Zealand USD account at ANZ New York, CHIPS UID 123456 ABA 123456789
	 *             via SQIFT of other electronic message, under Authenticated SWIFT (ANZBNZ22102) or Tested Telex
	 *             (AA68210) advise to us, quoting our reference number and marked attention International Services.
	 *   </SectionJ>
	 * </ExportCollection>
	 *
	 * @param Output Document       - This is the Xml document being built to be sent out for display.
	 * @param Terms Business Object - This is the object which we will get our data from for the XML doc.
	 * @param TransactionType       - The type of transaction curently being analyzed: 'Amend', 'Issue', 'Tracer'.
	 *
	 */
		 
	 private void sectionJ(DocumentHandler outputDoc, TradePortalBusinessObject terms,
						   String transactionType)    throws RemoteException, AmsException
	 {
			  //Issue
			  if( transactionType.equals( TransactionType.ISSUE ) )
				addNonNullDocData( terms.getAttribute("payment_instructions"), outputDoc, "/SectionJ/" );
	 }


	/**
	 * SectionK corresponds to the portion of the XML document labeled 'SectionK'.
	 * The following is an example of what the resulting Xml should look like:
	 * <ExportCollection>
	 *   <SectionK>
	 *      <Line1>1995</Line1>
	 *      <Line2>522</Line2>
	 *   </SectionK>
	 *   <SectionL></SectionL>
	 * </ExportCollection>
	 *
	 * @param Output Document       - This is the Xml document being built to be sent out for display.
	 * @param Terms Business Object - This is the object which we will get our data from for the XML doc.
	 * @param TermsOriginal Bus.Object - This is the original transactions Terms data.
	 *
	 */
	 
	 private void sectionK(DocumentHandler outputDoc, TradePortalBusinessObject terms,
						   String transactionType,    TradePortalBusinessObject termsOriginal) 
			 throws RemoteException, AmsException
	 {
			  String year;
			  String pubNum;
			  //Get the URC Year/Publication number- 
			  //If the transactionType is Issue: get the data from the Terms Bean
			  //Otherwise get it from the original Transactions Terms Bean
			  if( !transactionType.equals( TransactionType.ISSUE ) && termsOriginal!=null)
			  {
				LOG.debug("******************** Terms Original Oid == " + termsOriginal.getAttribute("terms_oid"));
				year = termsOriginal.getAttribute("urc_year");
				pubNum = termsOriginal.getAttribute("urc_pub_num");
			  }else{              
				year = terms.getAttribute("urc_year");
				pubNum = terms.getAttribute("urc_pub_num");
			  }
			  
			  //Revision Year
			  if( (year == null) || (year.equals("")) )
				outputDoc.setAttribute( "/SectionK/Line1/", "" );
			  else
				outputDoc.setAttribute( "/SectionK/Line1/", year );
				
			  //Publication number  
			  if( (pubNum == null) || (pubNum.equals("")) )
				outputDoc.setAttribute( "/SectionK/Line2/", "" );
			  else
				outputDoc.setAttribute( "/SectionK/Line2/"," "+pubNum );  
	 
	 }

	/**
	 * This method is meant to build 1 tenor's data and add it to the outgoing document handler.
	 * This is used in a loop to easily build all of our tenors.
	 *
	 * @param BusinessObject   - This is the terms object.
	 * @param BusinessObject   - This is the transaction object.
	 * @param DocumentHandler  - This is the Xml Document the data will get put into.
	 * @param LoopCounter      - This keeps track of what Tenor we're working on at the moment.
	 * @param CurrencyCode     - This is the currency of the transaction.
	 * @param Locale           - This is the current users Locale.
	 */
	
	private void addTenorToList( TradePortalBusinessObject terms, TradePortalBusinessObject trans,
									   DocumentHandler outputDoc, int loopCntr,
                                                                           String currencyCode, String locale)
			throws RemoteException, AmsException
	{
		 String tenorAmount = null;
                 String displayAmount = null;
                 String currencyAndAmount = null;
		 Integer convert = new Integer( loopCntr );
		 

		 outputDoc.setAttribute("/SectionF/MultipleTenors/TenorLine(" + loopCntr + ")/RowNumber/", convert.toString() );
		 outputDoc.setAttribute("/SectionF/MultipleTenors/TenorLine(" + loopCntr + ")/Tenor/",
								terms.getAttribute("tenor_" + loopCntr) );
											   
		 //couldn't use method appendAttributes() because the appended data is from different tables.
		 tenorAmount = terms.getAttribute("tenor_" + loopCntr + "_amount");
                 displayAmount = TPCurrencyUtility.getDisplayAmount(tenorAmount, currencyCode, locale);
                 currencyAndAmount = currencyCode + " " + displayAmount;
		 outputDoc.setAttribute("/SectionF/MultipleTenors/TenorLine(" + loopCntr + ")/Amount/", currencyAndAmount);
		 outputDoc.setAttribute("/SectionF/MultipleTenors/TenorLine(" + loopCntr + ")/DraftNumber/", 
								terms.getAttribute("tenor_" + loopCntr + "_draft_number") );
			  
	}

	
	/**
	 * This method is meant simply to test an object to see if it needs to be removed,
	 * If so, then we try to remove it.  If we get an exception, It would be probably
	 * Because we removed a parent of a component when we tried to remove the component
	 * object.
	 *
	 * @param BusinessObject - The object we're trying to remove.
	 * @param Object Type    - This is the type of object being sent out in the error message.
	 */
	private void beanRemove( TradePortalBusinessObject busObj, String objectType )
	{
		if( busObj != null ) 
		{   try{
					busObj.remove();
			}catch(Exception e)
			{   LOG.info("Failed in removal of " + objectType + " Bean: Export Collection Class");
				LOG.info("This could be because this object is a component of a parent object previously removed.");
			}
		}
	}
}