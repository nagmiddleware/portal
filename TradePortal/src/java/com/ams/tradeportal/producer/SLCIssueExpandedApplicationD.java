/**
 * 
 */
package com.ams.tradeportal.producer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.rmi.*;

import com.ams.tradeportal.busobj.TermsParty;
import com.ams.tradeportal.common.InstrumentType;
import com.ams.tradeportal.common.TPCurrencyUtility;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.ReferenceDataManager;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.StringFunction;

/**

 *
 */
public class SLCIssueExpandedApplicationD extends SLCIssueApplicationD{
private static final Logger LOG = LoggerFactory.getLogger(SLCIssueExpandedApplicationD.class);
	
	
	
	protected void sectionSExpandedIssue(DocumentHandler outputDoc) throws RemoteException, AmsException {
	
			//super. sectionSExpandedIssue(outputDoc);
		
			// Validity
			String validFromType = terms.getAttribute("guar_valid_from_date_type");		
			if (validFromType.equals(TradePortalConstants.DATE_OF_ISSUE))
				outputDoc.setAttribute("/SectionSExpanded/Validity/ValidFrom", "Date of Issue");
			if (validFromType.equals(TradePortalConstants.OTHER_VALID_FROM_DATE))
				outputDoc.setAttribute("/SectionSExpanded/Validity/ValidFrom", formatDate(terms, "guar_valid_from_date", formLocale));

			String validToType = terms.getAttribute("guar_expiry_date_type");
			if (validToType.equals(TradePortalConstants.VALIDITY_DATE))
				outputDoc.setAttribute("/SectionSExpanded/Validity/ValidTo", formatDate(terms, "expiry_date", formLocale));
			if (validToType.equals(TradePortalConstants.OTHER_EXPIRY_DATE))
				outputDoc.setAttribute("/SectionSExpanded/Validity/ValidTo", "Refer Instructions to Bank");

			// Delivery Instructions
			String deliverToType = terms.getAttribute("guar_deliver_to");
			if (deliverToType.equals(TradePortalConstants.DELIVER_TO_APPLICANT))
				outputDoc.setAttribute("/SectionSExpanded/DeliveryInstructions/DeliverTo", "Applicant");
			if (deliverToType.equals(TradePortalConstants.DELIVER_TO_BENEFICIARY))
				outputDoc.setAttribute("/SectionSExpanded/DeliveryInstructions/DeliverTo", "Beneficiary");
			if (deliverToType.equals(TradePortalConstants.DELIVER_TO_AGENT))
				outputDoc.setAttribute("/SectionSExpanded/DeliveryInstructions/DeliverTo", "Agent (details below)");
			if (deliverToType.equals(TradePortalConstants.DELIVER_TO_OTHER))
				outputDoc.setAttribute("/SectionSExpanded/DeliveryInstructions/DeliverTo", "See Instructions to Bank");

			String deliverByType = terms.getAttribute("guar_deliver_by");
			if (deliverByType.equals(TradePortalConstants.DELIVER_BY_TELEX))
				outputDoc.setAttribute("/SectionSExpanded/DeliveryInstructions/DeliverBy", "Swift");
			if (deliverByType.equals(TradePortalConstants.DELIVER_BY_REG_MAIL))
				outputDoc.setAttribute("/SectionSExpanded/DeliveryInstructions/DeliverBy", "Registered Mail");
			if (deliverByType.equals(TradePortalConstants.DELIVER_BY_MAIL))
				outputDoc.setAttribute("/SectionSExpanded/DeliveryInstructions/DeliverBy", "Mail");
			if (deliverByType.equals(TradePortalConstants.DELIVER_BY_COURIER))
				outputDoc.setAttribute("/SectionSExpanded/DeliveryInstructions/DeliverBy", "Courier");

			// Agent Details
			// TermsParty overseasBank = null;     // Overseas Bank Terms Party (Third Terms Party)	
			String addressLine2 = "";
			String cityState  = "";
			String countryZip = "";
			String countryDescr = "";

			TermsParty agentBank = null;     // Agent Bank Terms Party (Fourth Terms Party)
			if (deliverToType.equals(TradePortalConstants.DELIVER_TO_AGENT)){
				String fourthTermsPartyOid = terms.getAttribute("c_FourthTermsParty");
				if ( isNotEmpty( fourthTermsPartyOid)) {
					agentBank = (TermsParty)terms.getComponentHandle("FourthTermsParty");
					addNonNullDocData( agentBank.getAttribute("name"), outputDoc, "/SectionSExpanded//DeliveryInstructions/DeliverTo/Agent/Name/");
					addNonNullDocData( agentBank.getAttribute("contact_name"), outputDoc, "/SectionSExpanded/DeliveryInstructions/DeliverTo/Agent/ContactName/");
					addNonNullDocData( agentBank.getAttribute("address_line_1"), outputDoc, "/SectionSExpanded//DeliveryInstructions/DeliverTo/Agent/AddressLine1/");
					addressLine2 = agentBank.getAttribute("address_line_2");
					if( isNotEmpty( addressLine2 ) )
						outputDoc.setAttribute( "/SectionSExpanded/DeliveryInstructions/DeliverTo/Agent/AddressLine2", addressLine2 );
					cityState = appendAttributes(agentBank.getAttribute("address_city"), agentBank.getAttribute("address_state_province"), ", " );
					if( isNotEmpty( cityState ) )
						outputDoc.setAttribute( "/SectionSExpanded//DeliveryInstructions/DeliverTo/Agent/AddressStateProvince/", cityState );
					if(refDataMgr.checkCode(TradePortalConstants.COUNTRY, agentBank.getAttribute("address_country"), formLocale))
						countryDescr = refDataMgr.getDescr(TradePortalConstants.COUNTRY, agentBank.getAttribute("address_country"), formLocale);
					else
						countryDescr = "";          
					countryZip = appendAttributes(countryDescr, agentBank.getAttribute("address_postal_code"), ", ");
					if( isNotEmpty( countryZip ) )
						outputDoc.setAttribute( "/SectionSExpanded//DeliveryInstructions/DeliverTo/Agent/AddressCountryPostCode/", countryZip );

					addNonNullDocData( agentBank.getAttribute("phone_number"), outputDoc, "/SectionSExpanded/DeliveryInstructions/DeliverTo/Agent/PhoneNumber/");
				}
			}

			//guar_accept_instr   guar_advise_issuance
			String instructions = null;
			String acceptInstructions = terms.getAttribute("guar_accept_instr");
			String adviseIssuance = terms.getAttribute("guar_advise_issuance");
			if (acceptInstructions.equals(TradePortalConstants.INDICATOR_YES) && (adviseIssuance.equals(TradePortalConstants.INDICATOR_YES)))
				instructions = "Accept Instructions from this party and Advise Issuance";
			else if(acceptInstructions.equals(TradePortalConstants.INDICATOR_YES))
				instructions = "Accept Instructions from this party";
			else if(adviseIssuance.equals(TradePortalConstants.INDICATOR_YES))
				instructions = "Advise Issuance";
			else ;
			// Do Nothing
			addNonNullDocData(instructions, outputDoc, "/SectionSExpanded/DeliveryInstructions/DeliverTo/Instructions");

			LOG.info("Inside sectionSExpandedIssue() ........after instructions.");

			// Contract Details
			addNonNullDocData(terms.getAttribute("tender_order_contract_details"), outputDoc, "/SectionSExpanded/DeliveryInstructions/DeliverTo/ContractDetails");

			String characteristicType = "";			
			if(refDataMgr.checkCode(TradePortalConstants.CHARACTERISTIC_TYPE, terms.getAttribute("characteristic_type"), formLocale))
				characteristicType = refDataMgr.getDescr(TradePortalConstants.CHARACTERISTIC_TYPE, terms.getAttribute("characteristic_type"), formLocale);

			if(InstrumentType.STANDBY_LC.equals(instrument.getAttribute("instrument_type_code"))){
				if (TradePortalConstants.DELIVARY_TO_OTHER_OPTION.equals(characteristicType))
					outputDoc.setAttribute("/SectionSExpanded/DetailedInformation/TypeOfStandbyLC", "See Instructions to Bank");
				else
					addNonNullDocData( characteristicType, outputDoc, "/SectionSExpanded/DetailedInformation/TypeOfStandbyLC");
			}else{
				if (TradePortalConstants.DELIVARY_TO_OTHER_OPTION.equals(characteristicType))
					outputDoc.setAttribute("/SectionSExpanded/DetailedInformation/TypeOfGuarantee", "See Instructions to Bank");
				else
					addNonNullDocData( characteristicType, outputDoc, "/SectionSExpanded/DetailedInformation/TypeOfGuarantee");
			}

			String currencyCode = null;
			String currencyAmount = null;
			String currencyCodeAndAmount = null;

			currencyCode = terms.getAttribute("amount_currency_code");
			currencyAmount = terms.getAttribute("amount");
			currencyAmount = TPCurrencyUtility.getDisplayAmount(currencyAmount,	currencyCode, formLocale);

			currencyCodeAndAmount = appendAttributes(currencyCode, currencyAmount,	" ");
			addNonNullDocData( currencyCodeAndAmount, outputDoc, "/SectionSExpanded/DetailedInformation/CurrencyCodeAmount");

			String currencyCodeAndAmountInWords = null;

			if (isNotEmpty(currencyCode))
				currencyCode = refDataMgr.getDescr(	TradePortalConstants.CURRENCY_CODE, currencyCode,	formLocale);
			if (isNotEmpty(currencyAmount))
				currencyAmount = SpellItOut.spellOutNumber(currencyAmount, formLocale);

			currencyCodeAndAmountInWords = appendAttributes(currencyCode, currencyAmount, " ");
			addNonNullDocData(currencyCodeAndAmountInWords, outputDoc, "/SectionSExpanded/DetailedInformation/CurrencyValueAmountInWords");



			String expiryPlace = terms.getAttribute("place_of_expiry");
			if (TradePortalConstants.DELIVARY_TO_OTHER_OPTION.equals(expiryPlace))
				outputDoc.setAttribute("/SectionSExpanded/DetailedInformation/ExpiryPlace", "See Instructions to Bank");
			else
				outputDoc.setAttribute("/SectionSExpanded/DetailedInformation/ExpiryPlace", expiryPlace);	


			// Issuing Bank
			String issueType = terms.getAttribute("guarantee_issue_type");
			if (TradePortalConstants.GUAR_ISSUE_APPLICANTS_BANK.equals(issueType))
				outputDoc.setAttribute("/SectionSExpanded/DetailedInformation/IssuingBank/IssuedBy", "Applicant's Bank");
			if (TradePortalConstants.GUAR_ISSUE_OVERSEAS_BANK.equals(issueType))
				outputDoc.setAttribute("/SectionSExpanded/DetailedInformation/IssuingBank/IssuedBy", "Overseas Bank");

			if(TradePortalConstants.GUAR_ISSUE_OVERSEAS_BANK.equals(issueType)){
				String validityDate = terms.getAttribute("overseas_validity_date");
				if (isNotEmpty(validityDate))
					outputDoc.setAttribute("/SectionSExpanded/DetailedInformation/IssuingBank/Date", formatDate(terms, "overseas_validity_date", formLocale));

				TermsParty overseasBank = null;     // Overseas Bank Terms Party (Third Terms Party)	

				String thirdTermsPartyOid = terms.getAttribute("c_ThirdTermsParty");
				if ( isNotEmpty( thirdTermsPartyOid)) {
					overseasBank = (TermsParty)terms.getComponentHandle("ThirdTermsParty");
					addNonNullDocData( overseasBank.getAttribute("name"), outputDoc, "/SectionSExpanded/DetailedInformation/IssuingBank/OverseasBank/Name");
					addNonNullDocData( overseasBank.getAttribute("address_line_1"), outputDoc, "/SectionSExpanded/DetailedInformation/IssuingBank/OverseasBank/AddressLine1");
					addressLine2 = overseasBank.getAttribute("address_line_2");
					if( isNotEmpty( addressLine2 ) )
						outputDoc.setAttribute( "/SectionSExpanded/DetailedInformation/IssuingBank/OverseasBank/AddressLine2", addressLine2 );
					cityState = appendAttributes(overseasBank.getAttribute("address_city"), overseasBank.getAttribute("address_state_province"), ", " );
					if( isNotEmpty( cityState ) )
						outputDoc.setAttribute( "/SectionSExpanded/DetailedInformation/IssuingBank/OverseasBank/AddressStateProvince", cityState );
					if(refDataMgr.checkCode(TradePortalConstants.COUNTRY, overseasBank.getAttribute("address_country"), formLocale))
						countryDescr = refDataMgr.getDescr(TradePortalConstants.COUNTRY, overseasBank.getAttribute("address_country"), formLocale);
					else
						countryDescr = "";          
					countryZip = appendAttributes(countryDescr, overseasBank.getAttribute("address_postal_code"), ", ");
					if( isNotEmpty( countryZip ) )
						outputDoc.setAttribute( "/SectionSExpanded/DetailedInformation/IssuingBank/OverseasBank/AddressCountryPostCode", countryZip );
					addNonNullDocData( overseasBank.getAttribute("phone_number"),	outputDoc, "/SectionSExpanded/DetailedInformation/IssuingBank/OverseasBank/PhoneNumber");
				}
			}
		
	}
	
	
	
	protected void sectionL(DocumentHandler outputDoc)	throws RemoteException, AmsException{
		//super. sectionL(outputDoc);
		
		String autoExtInd = terms.getAttribute("auto_extension_ind");

		if (TradePortalConstants.INDICATOR_YES.equals(autoExtInd)) {

			String maxAutoExtAllowed = terms.getAttribute("max_auto_extension_allowed");
			String autoExtPeriod     = terms.getAttribute("auto_extension_period");
			String autoExtDays       = terms.getAttribute("auto_extension_days");
			String notifyBeneDays    = terms.getAttribute("notify_bene_days");

			outputDoc.setAttribute("/SectionL/AutoExtension/ExtAllowed", "Yes");
			addNonNullDocData(formatDate(terms, "final_expiry_date", formLocale), outputDoc, "/SectionL/AutoExtension/FinalExpiryDate");

			addNonNullDocData( maxAutoExtAllowed, outputDoc, "/SectionL/AutoExtension/MaxExtAllowed");

			addNonNullDocData(refDataMgr.getDescr(TradePortalConstants.AUTO_EXTEND_PERIOD_TYPE, autoExtPeriod, formLocale), outputDoc, "/SectionL/AutoExtension/AutoExtPeriod");
			addNonNullDocData(autoExtDays, outputDoc, "/SectionL/AutoExtension/AutoExtDays");

			addNonNullDocData(notifyBeneDays, outputDoc, "/SectionL/AutoExtension/NotifyBeneDays");

		}

		String value = terms.getAttribute("ucp_version_details_ind");
		if(StringFunction.isNotBlank(value) && value.equals(TradePortalConstants.INDICATOR_YES)){
			//outputDoc.setAttribute( "/SectionL/ICCApplicableRules", "Yes");  //Rel 9.3.5  IR-42291
			outputDoc.setAttribute( "/SectionL/ICCPVersion", ReferenceDataManager.getRefDataMgr().getDescr("ICC_GUIDELINES", terms.getAttribute("ucp_version")));

			value = terms.getAttribute("ucp_details");
			if(StringFunction.isNotBlank(value))
				outputDoc.setAttribute( "/SectionL/ICCPDetails", value);
		}

		// <AdditionalConditions>

		addNonNullDocData(terms.getAttribute("guar_customer_text"), outputDoc, "/SectionL/AdditionalConditions/Terms/Customer");
		addNonNullDocData(terms.getAttribute("guar_bank_standard_text"), outputDoc, "/SectionL/AdditionalConditions/Terms/Bank");


		
	}
	
	

}
