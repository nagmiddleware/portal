package com.ams.tradeportal.producer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.RemoteException;
import java.util.Vector;

import com.ams.tradeportal.busobj.PurchaseOrder;
import com.ams.tradeportal.busobj.TradePortalBusinessObject;
import com.ams.tradeportal.common.Debug;
import com.ams.tradeportal.common.TPCurrencyUtility;
import com.ams.tradeportal.common.TPDateTimeUtility;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.ClientServerDataBridge;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.frame.EJBObjectFactory;
import com.amsinc.ecsg.frame.ReferenceDataManager;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.JPylonProperties;
import com.amsinc.ecsg.util.ResourceManager;
import com.amsinc.ecsg.util.StringFunction;



/**
 * This class is designed to Build an XML Document with values from various
 * business objects then pass this data through an XSL stylesheet format the
 * data for display then pass the result on to a cocoon wrapper to display the
 * result in a PDF format. The method: populateXmlDoc does most of the work,
 * however get stream puts it all together and returns the result (a Reader
 * object). The Corresponding XSL file is: InvoiceDetailsPDFGenerator.xsl.
 * 
 * Copyright � 2001 American Management Systems, Incorporated All rights
 * reserved
 */
public class PODetailsPDFGenerator extends DocumentProducer {
private static final Logger LOG = LoggerFactory.getLogger(PODetailsPDFGenerator.class);

	private PurchaseOrder purchaseOrder = null;
	private String useCurrency = null;
	private ResourceManager resMgr = null;
    private DocumentHandler purchaseOrderLineItemsList = null;
   
    private String dateFormat = "MM/dd/yyyy";
	/**
	 * This method is the method that gathers all the data and builds the Xml
	 * document to be returned to the caller for display. The Xsl template is
	 * expecting the Xml doc to send data out in a specific format...ie in
	 * sections. If you scroll down the left side you can easily jump around the
	 * xml doc looking for specific section letters.
	 * 
	 * @param outputDoc
	 *            Xml Doc - This is the resulting Xml doc that will be returned.
	 * @param key
	 *            - This is the Transaction oid the will help us get all our
	 *            data. With out this oid, no data can be retrieved.
	 * @param locale
	 *            - This is the current users locale, it will help in
	 *            establishing the client Server data bridge as well as building
	 *            various B.O.s
	 * @param bankPrefix
	 *            - This may be removed in the future.
	 * @param contextPath
	 *            - the context path under which this request was made
	 * @return OutputDoc - Send out the result of building our Document.
	 */

	protected DocumentHandler populateXmlDoc(DocumentHandler outputDoc,
			String key, String locale, String bankPrefix, String contextPath)
			throws RemoteException, AmsException {

		Long poOid = new Long(key);

		// Setup the logo file location, context path and locale instance
		// variables
		if ( "blueyellow".equals(bankPrefix) ) {
			bankPrefix = TradePortalConstants.DEFAULT_BRANDING;
		  }
		 if ( "anztransac".equals(bankPrefix) ) {
			 bankPrefix = "anz";			 
		  }
		 bankLogo = bankPrefix + "/images";
		
		formContextPath = contextPath;
		formLocale = locale;

		ClientServerDataBridge csdb = new ClientServerDataBridge();
		csdb.setLocaleName(locale);

		String serverLocation = JPylonProperties.getInstance().getString(
				"serverLocation");

		// Get access to the Reference Data manager -- this is a singleton
		refDataMgr = ReferenceDataManager.getRefDataMgr();

		// Create Purchase Order
		purchaseOrder = (PurchaseOrder) EJBObjectFactory
				.createClientEJB(serverLocation, "PurchaseOrder",
						poOid.longValue(), csdb);
        String poNumber = purchaseOrder.getAttribute("purchase_order_num");

        useCurrency = purchaseOrder.getAttribute("currency");
        if (StringFunction.isBlank(useCurrency)) {
          useCurrency = "2";
        }
        resMgr = new ResourceManager(csdb);


        StringBuilder purchaseOrderLineItemListSQL = new StringBuilder();


        purchaseOrderLineItemListSQL.append("select * from purchase_order_line_item")
        .append(" where p_purchase_order_oid = '" + poOid + "'" + " order by lpad(line_item_num, 70)");

        purchaseOrderLineItemsList = DatabaseQueryBean.getXmlResultSet(
                purchaseOrderLineItemListSQL.toString(), false);

        try {
			// Section 'A' -- Title Bank Logo and document reference #
			sectionA(outputDoc, poNumber);
			// Section 'B' -- PO Details
			sectionB(outputDoc);
			// Section 'C' -- Buyer & Trading Partner Details
			sectionC(outputDoc);
//			// Section 'D' -- Invoice Summary details
			sectionD(outputDoc);

			LOG.debug("OutputDoc == " + outputDoc.toString());

		} finally { // remove any beans for clean up if they have data
			beanRemove(purchaseOrder, "PurchaseOrder");
		}

		return outputDoc;

	}

	/**
	 * SectionA corresponds to the portion of the XML document labeled
	 * 'SectionA'. The following is an example of what the resulting Xml should
	 * look like: 
	 * <SectionA><!-- Header Info --> 
	 * 	<Title> 
	 * 		<Line1>Purcase Order Details</Line1>
	 * 		<Line2></Line2> 
	 * 	</Title>
	 *	<Logo>http://localhost:7001//portal/images/blueyellow/en/</Logo>
 	 * </SectionA>
	 * 
 	 * 			DocumentHandler.setAttribute does not allow us to add same 
 	 * 			elements multiple times as it just updates existing elements.
	 * 			To overcome above limitation elements are added to the output 
	 * 			document using the DocumentHandler.addComponent method. 
	 * 			This method always adds elements to the top of the document. 
	 * 			So to maintain the order of the elements we need to add the 
	 * 			elements in reverse order.
	 *
	 * @param outputDoc
	 *            Document - This is the Xml document being built to be sent out
	 *            for display.
     * @param poNumber
	 */
	protected void sectionA(DocumentHandler outputDoc, String poNumber)
			throws RemoteException, AmsException {
		// Customizations Here
		outputDoc.setAttribute("/SectionA/Title/Line1",
				"Purchase Order Details");
		// Call Parent Method
	}

	/**
	 * SectionB corresponds to the portion of the XML document labeled
	 * 'SectionB'. The following is an example of what the resulting Xml should
	 * look like: 
	 * 		<SectionB><!-- PO Details Info -->
	 * 			<InvoiceDetails>
	 * 				<field> <!-- each field -->
	 * 					<name> <!-- field name -->
	 * 					</name>	
	 * 					<value> <!-- field value -->
	 * 					</value>
	 * 				</field>
	 * 			</InvoiceDetails> 
	 * 		</SectionB>
 	 * 			DocumentHandler.setAttribute does not allow us to add same 
 	 * 			elements multiple times as it just updates existing elements.
	 * 			To overcome above limitation elements are added to the output 
	 * 			document using the DocumentHandler.addComponent method. 
	 * 			This method always adds elements to the top of the document. 
	 * 			So to maintain the order of the elements we need to add the 
	 * 			elements in reverse order.
	 * 
	 * @param outputDoc
	 *            Document - This is the Xml document being built to be sent out
	 *            for display.
	 */

	protected void sectionB(DocumentHandler outputDoc) throws RemoteException,
			AmsException {
		// Customize Code
        DocumentHandler tempDoc = null;

        tempDoc = new DocumentHandler();
        tempDoc.setAttribute("/field/name", resMgr.getText(
                "PurchaseOrderDetails.PartialShipAllow",
                TradePortalConstants.TEXT_BUNDLE));
        tempDoc.setAttribute("/field/value",
                purchaseOrder.getAttribute("partial_shipment_allowed"));
        outputDoc.addComponent("/SectionB/PODetails/", tempDoc);

        tempDoc = new DocumentHandler();
        tempDoc.setAttribute("/field/name", resMgr.getText(
                "PurchaseOrderDetails.SellerName",
                TradePortalConstants.TEXT_BUNDLE));
        tempDoc.setAttribute("/field/value", purchaseOrder
                        .getAttribute("seller_name"));
        outputDoc.addComponent("/SectionB/PODetails/", tempDoc);

        tempDoc = new DocumentHandler();
        tempDoc.setAttribute("/field/name", resMgr.getText(
                "PurchaseOrderDetails.IssueDate",
                TradePortalConstants.TEXT_BUNDLE));
        tempDoc.setAttribute("/field/value", TPDateTimeUtility.formatDate(
                TPDateTimeUtility.convertToISODate(purchaseOrder.getAttribute("issue_date"),dateFormat),
                TPDateTimeUtility.SHORT, formLocale));
        outputDoc.addComponent("/SectionB/PODetails/", tempDoc);

        tempDoc = new DocumentHandler();
        tempDoc.setAttribute("/field/name", resMgr.getText(
                "PurchaseOrderDetails.Amount",
                TradePortalConstants.TEXT_BUNDLE));
        tempDoc.setAttribute("/field/value", TPCurrencyUtility
                .getDisplayAmount(purchaseOrder
                        .getAttribute("amount"), useCurrency,
                        formLocale));
        outputDoc.addComponent("/SectionB/PODetails/", tempDoc);

        tempDoc = new DocumentHandler();
        tempDoc.setAttribute("/field/name", resMgr.getText(
                "PurchaseOrderDetails.Currency",
                TradePortalConstants.TEXT_BUNDLE));
        tempDoc.setAttribute("/field/value",
                purchaseOrder.getAttribute("currency"));
        outputDoc.addComponent("/SectionB/PODetails/", tempDoc);

        tempDoc = new DocumentHandler();
        tempDoc.setAttribute("/field/name", resMgr.getText(
                "PurchaseOrderDetails.PurchaseOrderType",
                TradePortalConstants.TEXT_BUNDLE));
        tempDoc.setAttribute("/field/value",
                purchaseOrder.getAttribute("purchase_order_type"));
        outputDoc.addComponent("/SectionB/PODetails/", tempDoc);

        tempDoc = new DocumentHandler();
        tempDoc.setAttribute("/field/name", resMgr
                .getText("PurchaseOrderDetails.PurchaseOrderNum",
                        TradePortalConstants.TEXT_BUNDLE));
        tempDoc.setAttribute("/field/value",
                purchaseOrder.getAttribute("purchase_order_num"));
        outputDoc.addComponent("/SectionB/PODetails/", tempDoc);

        tempDoc = new DocumentHandler();
        tempDoc.setAttribute("/field/name", resMgr.getText(
                "PurchaseOrderDetails.DateTime",
                TradePortalConstants.TEXT_BUNDLE));
        tempDoc.setAttribute("/field/value", TPDateTimeUtility.formatDateTime(
                purchaseOrder.getAttribute("creation_date_time"),
                TPDateTimeUtility.SHORT, formLocale));
        outputDoc.addComponent("/SectionB/PODetails/", tempDoc);

        tempDoc = new DocumentHandler();
        tempDoc.setAttribute("/section-header/name", resMgr.getText(
                "PurchaseOrderDetails.PurchaseOrderNum",
                TradePortalConstants.TEXT_BUNDLE));
        tempDoc.setAttribute("/section-header/value",
                purchaseOrder.getAttribute("purchase_order_num"));
        outputDoc.addComponent("/SectionB/PODetails/", tempDoc);

    }

	/**
	 * SectionC corresponds to the portion of the XML document labeled
	 * 'SectionC'. The following is an example of what the resulting Xml should
	 * look like: 
	 * 			<BuyerDetails>
	 * 				<field> <!-- each field -->
	 * 					<name> <!-- field name -->
	 * 					</name>	
	 * 					<value> <!-- field value -->
	 * 					</value>
	 * 				</field>
	 * 			</BuyerDetails> 
	 * 
	 * @param outputDoc
	 *            Document - This is the Xml document being built to be sent out
	 *            for display.
	 */
	protected void sectionC(DocumentHandler outputDoc)
			throws RemoteException, AmsException {
        DocumentHandler tempDoc = null;
        DocumentHandler tempDoc1 = null;
                tempDoc1 = new DocumentHandler();
                tempDoc1.setAttribute("/POSummDetails", "");

                for (int i = TradePortalConstants.INV_BUYER_OR_SELLER_USER_DEF_FIELD; i >= 1; i--) {
                    if (!StringFunction.isBlank(purchaseOrder
                            .getAttribute("seller_user_def" + i + "_label"))) {
                        tempDoc = new DocumentHandler();
                        tempDoc.setAttribute(
                                "/field/name",purchaseOrder
                                .getAttribute("seller_user_def"
                                        + i + "_label"));
                        tempDoc.setAttribute("/field/value",
                                purchaseOrder
                                        .getAttribute("seller_user_def"
                                                + i + "_value"));
                        tempDoc1.addComponent("/POSummDetails", tempDoc);
                    }
                }
                for (int i = TradePortalConstants.INV_BUYER_OR_SELLER_USER_DEF_FIELD; i >= 1; i--) {
                    if (!StringFunction.isBlank(purchaseOrder
                            .getAttribute("buyer_user_def" + i + "_label"))) {
                        tempDoc = new DocumentHandler();
                        tempDoc.setAttribute(
                                "/field/name",
                                purchaseOrder
                                        .getAttribute("buyer_user_def" + i + "_label"));
                        tempDoc.setAttribute("/field/value",
                                purchaseOrder
                                        .getAttribute("buyer_user_def" + i + "_value"));
                        tempDoc1.addComponent("/POSummDetails", tempDoc);
                    }
                }

                tempDoc = new DocumentHandler();
                tempDoc.setAttribute("/field/name", resMgr.getText(
                        "PurchaseOrderDetails.GoodsDesc",
                        TradePortalConstants.TEXT_BUNDLE));
                tempDoc.setAttribute("/field/value", purchaseOrder
                        .getAttribute("goods_description"));
                tempDoc1.addComponent("/POSummDetails", tempDoc);

                tempDoc = new DocumentHandler();
                tempDoc.setAttribute("/field/name", resMgr.getText(
                        "PurchaseOrderDetails.LatestShipDate",
                        TradePortalConstants.TEXT_BUNDLE));
                tempDoc.setAttribute("/field/value",TPDateTimeUtility.formatDate(
                        TPDateTimeUtility.convertToISODate(purchaseOrder.getAttribute("latest_shipment_date"),dateFormat),
                        TPDateTimeUtility.SHORT, formLocale) );
                tempDoc1.addComponent("/POSummDetails", tempDoc);

                tempDoc = new DocumentHandler();
                tempDoc.setAttribute("/field/name",
                        resMgr.getText("PurchaseOrderDetails.IncotermLoc",
                                TradePortalConstants.TEXT_BUNDLE));
                tempDoc.setAttribute("/field/value",
                        purchaseOrder.getAttribute("incoterm_location"));
                tempDoc1.addComponent("/POSummDetails", tempDoc);

                tempDoc = new DocumentHandler();
                tempDoc.setAttribute("/field/name", resMgr.getText(
                        "PurchaseOrderDetails.Incoterm",
                        TradePortalConstants.TEXT_BUNDLE));
                tempDoc.setAttribute("/field/value", purchaseOrder
                        .getAttribute("incoterm"));
                tempDoc1.addComponent("/POSummDetails", tempDoc);

                outputDoc.addComponent("/SectionC", tempDoc1);

    }

	/**
	 * SectionD corresponds to the portion of the XML document labeled
	 * 'SectionD'. The following is an example of what the resulting Xml should
	 * look like: 
	 * 		<SectionD><!-- Applicant and Beneficiary-->
	 * 			<InvSummGoodsDetails>
	 * 				<field> <!-- each field -->
	 * 					<name> <!-- field name -->
	 * 					</name>	
	 * 					<value> <!-- field value -->
	 * 					</value>
	 * 				</field>
	 * 				<LineItemDetails>
	 * 					<field> <!-- each field -->
	 * 						<name> <!-- field name -->
	 * 						</name>	
	 * 						<value> <!-- field value -->
	 * 						</value>
	 * 					</field>
	 *	 			</LineItemDetails> 
	 * 			</InvSummGoodsDetails> 
 	 * 		</SectionD>
 	 * 			DocumentHandler.setAttribute does not allow us to add same 
 	 * 			elements multiple times as it just updates existing elements.
	 * 			To overcome above limitation elements are added to the output 
	 * 			document using the DocumentHandler.addComponent method. 
	 * 			This method always adds elements to the top of the document. 
	 * 			So to maintain the order of the elements we need to add the 
	 * 			elements in reverse order.
	 * @param outputDoc
	 *            Document - This is the Xml document being built to be sent out
	 *            for display.
     *
     */

	protected void sectionD(DocumentHandler outputDoc
			) throws RemoteException,
			AmsException {
        if (purchaseOrderLineItemsList != null) {
        Vector summaryGoodsVector = purchaseOrderLineItemsList
        .getFragments("/ResultSetRecord/");
        int totalLineItem = summaryGoodsVector.size();
            DocumentHandler tempLineDoc = null;
            DocumentHandler tempLineDoc1 = null;
            for (int lineItem = totalLineItem - 1; lineItem >= 0; lineItem--) {

                tempLineDoc1 = new DocumentHandler();
                tempLineDoc1.setAttribute("/LineItems", "");

                for (int i = TradePortalConstants.INV_LINE_ITEM_TYPE_OR_VALUE_FIELD; i >= 1; i--) {
                    if (!StringFunction
                            .isBlank(purchaseOrderLineItemsList
                                    .getAttribute("/ResultSetRecord("
                                            + lineItem
                                            + ")/PROD_CHARS_UD" + i
                                            + "_LABEL"))) {
                        tempLineDoc = new DocumentHandler();
                        tempLineDoc
                                .setAttribute(
                                        "/field/name",
                                        purchaseOrderLineItemsList
                                                .getAttribute("/ResultSetRecord("
                                                        + lineItem
                                                        + ")/PROD_CHARS_UD"
                                                        + i + "_LABEL"));
                        tempLineDoc
                                .setAttribute(
                                        "/field/value",
                                        purchaseOrderLineItemsList
                                                .getAttribute("/ResultSetRecord("
                                                        + lineItem
                                                        + ")/PROD_CHARS_UD"
                                                        + i + "_VALUE"));
                        tempLineDoc1.addComponent("/LineItems",
                                tempLineDoc);
                    }
                }


                tempLineDoc = new DocumentHandler();
                tempLineDoc.setAttribute("/field/name",
                        resMgr.getText("PurchaseOrderUploadRequest.quantity",
                                TradePortalConstants.TEXT_BUNDLE));
                tempLineDoc.setAttribute("/field/value",
                        purchaseOrderLineItemsList.getAttribute("/ResultSetRecord("
                                + lineItem + ")/QUANTITY"));
                tempLineDoc1.addComponent("/LineItems", tempLineDoc);

                tempLineDoc = new DocumentHandler();
                tempLineDoc.setAttribute("/field/name", resMgr.getText(
                        "PurchaseOrderUploadRequest.unit_of_measure",
                        TradePortalConstants.TEXT_BUNDLE));
                tempLineDoc.setAttribute("/field/value",
                        purchaseOrderLineItemsList
                                .getAttribute("/ResultSetRecord("
                                        + lineItem + ")/UNIT_OF_MEASURE"));
                tempLineDoc1.addComponent("/LineItems", tempLineDoc);

                tempLineDoc = new DocumentHandler();
                tempLineDoc.setAttribute("/field/name", resMgr.getText(
                        "PurchaseOrderUploadRequest.unit_price",
                        TradePortalConstants.TEXT_BUNDLE));
                tempLineDoc.setAttribute("/field/value",
                        purchaseOrderLineItemsList
                                .getAttribute("/ResultSetRecord("
                                        + lineItem + ")/UNIT_PRICE"));
                tempLineDoc1.addComponent("/LineItems", tempLineDoc);

                tempLineDoc = new DocumentHandler();
                tempLineDoc.setAttribute("/field/name", resMgr.getText(
                        "PurchaseOrderUploadRequest.line_item_num",
                        TradePortalConstants.TEXT_BUNDLE));
                tempLineDoc.setAttribute("/field/value",
                        purchaseOrderLineItemsList
                                .getAttribute("/ResultSetRecord("
                                        + lineItem + ")/LINE_ITEM_NUM"));
                tempLineDoc1.addComponent("/LineItems", tempLineDoc);

                tempLineDoc = new DocumentHandler();
                tempLineDoc
                        .setAttribute(
                                "/section-header/name",
                                resMgr.getText(
                                        "UploadInvoiceDetail.LineItemDetail",
                                        TradePortalConstants.TEXT_BUNDLE));
                tempLineDoc1.addComponent("/LineItems", tempLineDoc);

                outputDoc.addComponent("/SectionD",
                        tempLineDoc1);
            }
        }


    }

        /**
	 * This method is meant simply to test an object to see if it needs to be removed,
	 * If so, then we try to remove it.  If we get an exception, It would be probably
	 * Because we removed a parent of a component when we tried to remove the component
	 * object.
	 *
	 * @param busObj - The object we're trying to remove.
	 * @param objectType - This is the type of object being sent out in the error message.
	 */
	private void beanRemove( TradePortalBusinessObject busObj, String objectType )
	{
		if( busObj != null )
		{   try{
					busObj.remove();
			}catch(Exception e)
			{   LOG.info("Failed in removal of " + objectType + " Bean: Invoice Details PDF Generator");
				LOG.info("This could be because this object is a component of a parent object previously removed.");
			}
		}
	}

}