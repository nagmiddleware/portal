package com.ams.tradeportal.producer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.*;
import java.text.SimpleDateFormat;
import java.util.Vector;

import com.amsinc.ecsg.util.*;
import com.ams.tradeportal.common.*;
import com.ams.tradeportal.busobj.*;
import com.ams.tradeportal.busobj.util.InstrumentServices;
import com.amsinc.ecsg.frame.*;

//RKAZI CR-710 02/02/2012 Rel 8.0 Add

/**
 * This class is designed to Build an XML Document with values from various
 * business objects then pass this data through an XSL stylesheet format the
 * data for display then pass the result on to a cocoon wrapper to display the
 * result in a PDF format. The method: populateXmlDoc does most of the work,
 * however get stream puts it all together and returns the result (a Reader
 * object). The Corresponding XSL file is: InvoicesOfferedDetailPDFGenerator.xsl.
 * 
 * Copyright � 2001 American Management Systems, Incorporated All rights
 * reserved
 */
public class InvoicesOfferedDetailPDFGenerator extends DocumentProducer {
private static final Logger LOG = LoggerFactory.getLogger(InvoicesOfferedDetailPDFGenerator.class);

	SimpleDateFormat isoDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");

	/**
	 * This method is the method that gathers all the data and builds the Xml
	 * document to be returned to the caller for display. The Xsl template is
	 * expecting the Xml doc to send data out in a specific format...ie in
	 * sections. If you scroll down the left side you can easily jump around the
	 * xml doc looking for specific section letters.
	 * 
	 * @param Output
	 *            Xml Doc - This is the resulting Xml doc that will be returned.
	 * @param Key
	 *            - This is the Transaction oid the will help us get all our
	 *            data. With out this oid, no data can be retrieved.
	 * @param Locale
	 *            - This is the current users locale, it will help in
	 *            establishing the client Server data bridge as well as building
	 *            various B.O.s
	 * @param BankPrefix
	 *            - This may be removed in the future.
	 * @param ContextPath
	 *            - the context path under which this request was made
	 * @return OutputDoc - Send out the result of building our Document.
	 */

	protected DocumentHandler populateXmlDoc(DocumentHandler outputDoc,
			String key, String locale, String bankPrefix, String contextPath)
			throws RemoteException, AmsException {
	ResourceManager resMgr = null;
	DocumentHandler invoiceGoodsResult = null;
	DocumentHandler invoiceOfferedLogResult = null;

		Long invoiceOid = new Long(key);

		// Setup the logo file location, context path and locale instance
		// variables
		if ( "blueyellow".equals(bankPrefix) ) {
			bankPrefix = TradePortalConstants.DEFAULT_BRANDING;
		  }
		 if ( "anztransac".equals(bankPrefix) ) {
			 bankPrefix = "anz";			 
		  }
		 bankLogo = bankPrefix + "/images";

		formContextPath = contextPath;
		formLocale = locale;
		
		ClientServerDataBridge csdb = new ClientServerDataBridge();
		csdb.setLocaleName(locale);

		String serverLocation = JPylonProperties.getInstance().getString("serverLocation");

		// Get access to the Reference Data manager -- this is a singleton
		refDataMgr = ReferenceDataManager.getRefDataMgr();

		// Create invoiceSummaryData
		Invoice invoice = (Invoice) EJBObjectFactory.createClientEJB(serverLocation, "Invoice", invoiceOid.longValue(), csdb);
                
                String invoiceOfferGroupOid = invoice.getAttribute("invoice_offer_group_oid");
                InvoiceOfferGroup invoiceOfferGroup = null;
                if (invoiceOfferGroupOid!=null && invoiceOfferGroupOid.length() > 0) {
                    invoiceOfferGroup = (InvoiceOfferGroup) EJBObjectFactory.createClientEJB(serverLocation, "InvoiceOfferGroup", Long.parseLong(invoiceOfferGroupOid), csdb);
                }

                resMgr = new ResourceManager(csdb);
        //jgadela R92 - SQL INJECTION FIX        
		String invoiceGoodsSQL = "select * from invoice_goods WHERE p_invoice_oid = ? ";
		invoiceGoodsResult = DatabaseQueryBean.getXmlResultSet(invoiceGoodsSQL.toString(), false, new Object[]{invoiceOid});
		
		//jgadela R92 - SQL INJECTION FIX   
		String invoiceHistorySQL = "SELECT to_char(ih.action_datetime, 'dd Mon yyyy HH24:MI:SS') As DateAndTime, ih.action As Action, ih.invoice_status	As Status, "
                        +"(CASE WHEN usr.user_oid IS NULL THEN '' ELSE usr.user_identifier|| ' / ' ||  usr.first_name  || ' ' ||  usr.last_name END) As UserInfo"
                        +" FROM invoice_history ih, users usr  WHERE usr.user_oid (+) = ih.a_user_oid "
                        +" AND ih.p_upload_invoice_oid = ?  order by ih.action_datetime";
		DocumentHandler invoiceHistoryResult = DatabaseQueryBean.getXmlResultSet(invoiceHistorySQL.toString(), false, new Object[]{invoiceOid});

		try {
			// Section 'A' -- Title Bank Logo and document reference #
			sectionA(outputDoc, resMgr);
			// Section 'B' -- Invoice Details
			sectionB1(outputDoc, invoice, invoiceOfferGroup, resMgr);
			sectionB2(outputDoc, invoice, invoiceOfferGroup, resMgr);
			// Section 'C' -- Rejection Reason
			sectionC(outputDoc, invoice, resMgr);
			// Section 'D' -- Buyer & Seller Details
			sectionD(outputDoc, invoice, resMgr);
			// Section 'E' -- Invoice Goods details                        
			sectionE(outputDoc, invoiceGoodsResult, resMgr);
                        // Section 'F' -- Invoice history
                        sectionF(outputDoc, invoiceHistoryResult, resMgr);
			LOG.debug("OutputDoc == " + outputDoc.toString());

		} finally { // remove any beans for clean up if they have data
			beanRemove( invoice, "Invoice");
		}

		return outputDoc;

	}

	/**
	 * SectionA corresponds to the portion of the XML document labeled
	 * 'SectionA'. The following is an example of what the resulting Xml should
	 * look like: 
	 * <SectionA><!-- Header Info --> 
	 * 	<Title> 
	 * 		<Line1>Invoice Details</Line1> 
	 * 		<Line2></Line2> 
	 * 	</Title>
	 *	<Logo>http://localhost:7001//portal/images/blueyellow/en/</Logo>
 	 * </SectionA>
	 * 
 	 * 			DocumentHandler.setAttribute does not allow us to add same 
 	 * 			elements multiple times as it just updates existing elements.
	 * 			To overcome above limitation elements are added to the output 
	 * 			document using the DocumentHandler.addComponent method. 
	 * 			This method always adds elements to the top of the document. 
	 * 			So to maintain the order of the elements we need to add the 
	 * 			elements in reverse order.
	 *
	 * @param Output
	 *            Document - This is the Xml document being built to be sent out
	 *            for display.
	 */
	protected void sectionA(DocumentHandler outputDoc, ResourceManager resMgr)
			throws RemoteException, AmsException {
		// Customizations Here
		outputDoc.setAttribute("/SectionA/Title/Line1", resMgr.getText("InvoicesOfferedDetail.InvoiceDetails",TradePortalConstants.TEXT_BUNDLE));
		// Call Parent Method
	}

	/**
	 * SectionB corresponds to the portion of the XML document labeled
	 * 'SectionB'. The following is an example of what the resulting Xml should
	 * look like: 
	 * 		<SectionB><!-- Invoice Details Info -->
	 * 			<InvoiceDetails>
	 * 				<field> <!-- each field -->
	 * 					<name> <!-- field name -->
	 * 					</name>	
	 * 					<value> <!-- field value -->
	 * 					</value>
	 * 				</field>
	 * 			</InvoiceDetails> 
	 * 		</SectionB>
 	 * 			DocumentHandler.setAttribute does not allow us to add same 
 	 * 			elements multiple times as it just updates existing elements.
	 * 			To overcome above limitation elements are added to the output 
	 * 			document using the DocumentHandler.addComponent method. 
	 * 			This method always adds elements to the top of the document. 
	 * 			So to maintain the order of the elements we need to add the 
	 * 			elements in reverse order.
	 * 
	 * @param Output
	 *            Document - This is the Xml document being built to be sent out
	 *            for display.
	 */

	protected void sectionB1(DocumentHandler outputDoc, Invoice invoice, InvoiceOfferGroup invoiceOfferGroup, ResourceManager resMgr) throws RemoteException, AmsException {
		DocumentHandler tempDoc = null;
		String date = "";
		
		 String currency = invoice.getAttribute("currency_code");
		 String invoiceStatus = refDataMgr.getDescr("SUPPLIER_PORTAL_INVOICE_STATUS", invoice.getAttribute("supplier_portal_invoice_status"), formLocale);		
		
		//Adjusted Payment Amount
		tempDoc = new DocumentHandler();
		tempDoc.setAttribute("/field/name", resMgr.getText("InvoicesOffered.PaymentAmount", TradePortalConstants.TEXT_BUNDLE));
		tempDoc.setAttribute("/field/value", 
				TPCurrencyUtility.getDisplayAmount(invoice.getAttribute("invoice_payment_amount"), currency, formLocale));
		outputDoc.addComponent("/SectionB1/InvoiceDetails/", tempDoc);		
		
		
		tempDoc = new DocumentHandler();
		tempDoc.setAttribute("/field/name", resMgr.getText("InvoicesOfferedDetail.InvoiceAmount", TradePortalConstants.TEXT_BUNDLE));
		tempDoc.setAttribute("/field/value", 
				TPCurrencyUtility.getDisplayAmount(invoice.getAttribute("invoice_total_amount"), currency, formLocale));
		outputDoc.addComponent("/SectionB1/InvoiceDetails/", tempDoc);
		
		tempDoc = new DocumentHandler();
		tempDoc.setAttribute("/field/name", resMgr.getText("InvoicesOfferedDetail.Ccy", TradePortalConstants.TEXT_BUNDLE));
		tempDoc.setAttribute("/field/value", invoice.getAttribute("currency_code"));
		outputDoc.addComponent("/SectionB1/InvoiceDetails/", tempDoc);
		
		tempDoc = new DocumentHandler();
		tempDoc.setAttribute("/field/name", resMgr.getText("InvoicesOfferedDetail.PaymentDate", TradePortalConstants.TEXT_BUNDLE));
		date = invoice.getAttribute("invoice_payment_date");
		if(StringFunction.isNotBlank(date)){
			date = isoDateFormat.format(invoice.getAttributeDate("invoice_payment_date"));
			date = TPDateTimeUtility.formatDate(date, TPDateTimeUtility.SHORT, formLocale);
		}
		tempDoc.setAttribute("/field/value", date);
		outputDoc.addComponent("/SectionB1/InvoiceDetails/", tempDoc);
		
		tempDoc = new DocumentHandler();
		tempDoc.setAttribute("/field/name", resMgr.getText("InvoicesOfferedDetail.DueDate", TradePortalConstants.TEXT_BUNDLE));
		date = invoice.getAttribute("invoice_due_date");
		if(StringFunction.isNotBlank(date)){
			date = isoDateFormat.format(invoice.getAttributeDate("invoice_due_date"));
			date = TPDateTimeUtility.formatDate(date, TPDateTimeUtility.SHORT, formLocale);
		}
		tempDoc.setAttribute("/field/value", date);
		outputDoc.addComponent("/SectionB1/InvoiceDetails/", tempDoc);
		
		tempDoc = new DocumentHandler();
		tempDoc.setAttribute("/field/name", resMgr.getText("InvoicesOfferedDetail.IssueDate", TradePortalConstants.TEXT_BUNDLE));
		date = invoice.getAttribute("invoice_issue_datetime");
		if(StringFunction.isNotBlank(date)){
			date = isoDateFormat.format(invoice.getAttributeDate("invoice_issue_datetime"));
			date = TPDateTimeUtility.formatDate(date, TPDateTimeUtility.SHORT, formLocale);
		}
		tempDoc.setAttribute("/field/value", date);
		outputDoc.addComponent("/SectionB1/InvoiceDetails/", tempDoc);
		
		tempDoc = new DocumentHandler();
		tempDoc.setAttribute("/section-header/name", resMgr.getText("InvoicesOfferedDetail.InvoiceID", TradePortalConstants.TEXT_BUNDLE));
		tempDoc.setAttribute("/section-header/value",invoice.getAttribute("invoice_reference_id") + " - (" + invoiceStatus + ")");
		outputDoc.addComponent("/SectionB1/InvoiceDetails/", tempDoc);
	}

	protected void sectionB2(DocumentHandler outputDoc, Invoice invoice, InvoiceOfferGroup invoiceOfferGroup, ResourceManager resMgr) throws RemoteException, AmsException {
		DocumentHandler tempDoc = null;
		
		tempDoc = new DocumentHandler();
		tempDoc.setAttribute("/field/name", resMgr.getText("InvoicesOfferedDetail.InvoiceGroup", TradePortalConstants.TEXT_BUNDLE));
                String invoiceOfferGroupName = null;
                if (invoiceOfferGroup!=null) {
                    invoiceOfferGroupName = invoiceOfferGroup.getAttribute("name");
                }
                tempDoc.setAttribute("/field/value",invoiceOfferGroupName);
		outputDoc.addComponent("/SectionB2/InvoiceDetails/", tempDoc);
		
		tempDoc = new DocumentHandler();
                String invoiceStatus = refDataMgr.getDescr("SUPPLIER_PORTAL_INVOICE_STATUS", invoice.getAttribute("supplier_portal_invoice_status"), formLocale);
		tempDoc.setAttribute("/field/name", resMgr.getText("UploadInvoiceDetail.InvoiceStatus", TradePortalConstants.TEXT_BUNDLE));
		tempDoc.setAttribute("/field/value", invoiceStatus);
		outputDoc.addComponent("/SectionB2/InvoiceDetails/", tempDoc);
		
                String currency = invoice.getAttribute("currency_code");
		tempDoc = new DocumentHandler();
		tempDoc.setAttribute("/field/name", resMgr.getText("InvoicesOfferedDetail.TotalNetAmount", TradePortalConstants.TEXT_BUNDLE));
		tempDoc.setAttribute("/field/value", 
				TPCurrencyUtility.getDisplayAmount(invoice.getAttribute("net_amount_offered"), currency, formLocale));
		outputDoc.addComponent("/SectionB2/InvoiceDetails/", tempDoc);
		
		//Invoice Amount After Adjustment
		tempDoc = new DocumentHandler();
		tempDoc.setAttribute("/field/name", resMgr.getText("InvoicesOffered.AmountAfterAdj", TradePortalConstants.TEXT_BUNDLE));
		tempDoc.setAttribute("/field/value", 
				TPCurrencyUtility.getDisplayAmount(invoice.getAttribute("inv_amount_after_adj"), currency, formLocale));
		outputDoc.addComponent("/SectionB2/InvoiceDetails/", tempDoc);	
		
		//Credit Notes Applied Amount
		tempDoc = new DocumentHandler();
		tempDoc.setAttribute("/field/name", resMgr.getText("InvoicesOffered.CreditNoteAppliedAmt", TradePortalConstants.TEXT_BUNDLE));
		tempDoc.setAttribute("/field/value", 
				TPCurrencyUtility.getDisplayAmount(invoice.getAttribute("total_credit_note_amount"), currency, formLocale));
		outputDoc.addComponent("/SectionB2/InvoiceDetails/", tempDoc);		
	}	
	
	protected void sectionC(DocumentHandler outputDoc, Invoice invoice, ResourceManager resMgr) throws RemoteException, AmsException {
		DocumentHandler tempDoc = null;
		
		tempDoc = new DocumentHandler();
		tempDoc.setAttribute("/field/name", resMgr.getText("InvoicesOfferedDetail.RejectedInvoiceReason", TradePortalConstants.TEXT_BUNDLE));
		tempDoc.setAttribute("/field/value",invoice.getAttribute("rejection_reason_text"));
		outputDoc.addComponent("/SectionC/InvoiceDetails/", tempDoc);
        }
        
	/**
	 * SectionC corresponds to the portion of the XML document labeled
	 * 'SectionC'. The following is an example of what the resulting Xml should
	 * look like: 
	 * 			<BuyerDetails>
	 * 				<field> <!-- each field -->
	 * 					<name> <!-- field name -->
	 * 					</name>	
	 * 					<value> <!-- field value -->
	 * 					</value>
	 * 				</field>
	 * 			</BuyerDetails> 
	 * 
	 * @param Output
	 *            Document - This is the Xml document being built to be sent out
	 *            for display.
	 */
	protected void sectionD(DocumentHandler outputDoc, Invoice invoice, ResourceManager resMgr) throws RemoteException, AmsException {
		DocumentHandler tempDoc = null;
		
		tempDoc = new DocumentHandler();
		tempDoc.setAttribute("/field/name", null);
		tempDoc.setAttribute("/field/value", invoice.getAttribute("seller_address_line_2"));
		outputDoc.addComponent("/SectionD/BuyerDetails", tempDoc);
		
		tempDoc = new DocumentHandler();
		tempDoc.setAttribute("/field/name", resMgr.getText("InvoicesOfferedDetail.AddressLine1", TradePortalConstants.TEXT_BUNDLE));
		tempDoc.setAttribute("/field/value", invoice.getAttribute("seller_address_line_1"));
		outputDoc.addComponent("/SectionD/BuyerDetails", tempDoc);
		
		tempDoc = new DocumentHandler();
		tempDoc.setAttribute("/field/name", resMgr.getText("InvoicesOfferedDetail.Identifier", TradePortalConstants.TEXT_BUNDLE));
		tempDoc.setAttribute("/field/value", invoice.getAttribute("seller_party_identifier"));
		outputDoc.addComponent("/SectionD/BuyerDetails", tempDoc);	
		
		tempDoc = new DocumentHandler();
		tempDoc.setAttribute("/field/name", resMgr.getTextIncludingEmptyString("InvoicesOfferedDetail.AddressLine2", TradePortalConstants.TEXT_BUNDLE));
		tempDoc.setAttribute("/field/value", invoice.getAttribute("buyer_address_line_2"));
		outputDoc.addComponent("/SectionD/BuyerDetails", tempDoc);
		
		tempDoc = new DocumentHandler();
		tempDoc.setAttribute("/field/name", resMgr.getText("InvoicesOfferedDetail.AddressLine1", TradePortalConstants.TEXT_BUNDLE));
		tempDoc.setAttribute("/field/value", invoice.getAttribute("buyer_address_line_1"));
		outputDoc.addComponent("/SectionD/BuyerDetails", tempDoc);
				
		tempDoc = new DocumentHandler();
		tempDoc.setAttribute("/field/name", resMgr.getText("InvoicesOfferedDetail.Identifier", TradePortalConstants.TEXT_BUNDLE));
		tempDoc.setAttribute("/field/value", invoice.getAttribute("buyer_party_identifier"));
		outputDoc.addComponent("/SectionD/BuyerDetails", tempDoc);	
		
		tempDoc = new DocumentHandler();
		tempDoc.setAttribute("/section-header/name", resMgr.getText("InvoicesOfferedDetail.Seller", TradePortalConstants.TEXT_BUNDLE));
		outputDoc.addComponent("/SectionD/BuyerDetails", tempDoc);
		
		tempDoc = new DocumentHandler();
		tempDoc.setAttribute("/section-header/name", resMgr.getText("InvoicesOfferedDetail.Buyer", TradePortalConstants.TEXT_BUNDLE));
		outputDoc.addComponent("/SectionD/BuyerDetails", tempDoc);
	}

	/**
	 * 
	 * @param Output
	 *            Document - This is the Xml document being built to be sent out
	 *            for display.
	 */

	protected void sectionE(DocumentHandler outputDoc, DocumentHandler invoiceGoodsDoc, ResourceManager resMgr) throws RemoteException, AmsException {
		DocumentHandler tempDoc = null;
		DocumentHandler tempDoc1 = null;
		
		if (invoiceGoodsDoc != null) {
			Vector invoiceGoodsVector = invoiceGoodsDoc.getFragments("/ResultSetRecord/");
			int numberOfInvoiceGoods = invoiceGoodsVector.size();
			
			for(int i= 0; i<numberOfInvoiceGoods; i++) {
                            tempDoc = new DocumentHandler();
                            
                            //seller user defined fields
                            tempDoc.setAttribute("/SellerDefinedFieldsLabel", resMgr.getText("InvoicesOffered.SellerDefinedFields", TradePortalConstants.TEXT_BUNDLE));
							for (int j = 0; j < 10; j++) {
								if (StringFunction
										.isNotBlank(((DocumentHandler) invoiceGoodsVector
												.get(i))
												.getAttribute("/SELLER_INFORMATION_LABEL"
														+ j))) {
									tempDoc1 = new DocumentHandler();
									tempDoc1.setAttribute("/SellerInfo/Label", ((DocumentHandler)invoiceGoodsVector.get(i)).getAttribute("/SELLER_INFORMATION_LABEL" + j));
									tempDoc1.setAttribute("/SellerInfo/Value", ((DocumentHandler) invoiceGoodsVector.get(i)).getAttribute("/SELLER_INFORMATION" + j));
									tempDoc.addComponent("/SellerDefinedFields",tempDoc1);
								}
							}                                                    
							
                            tempDoc.setAttribute("/GoodsDescriptionLabel", resMgr.getText("InvoicesOfferedDetail.GoodsDescription", TradePortalConstants.TEXT_BUNDLE));
                            tempDoc.setAttribute("/GoodsDescription", ((DocumentHandler)invoiceGoodsVector.get(i)).getAttribute("/GOODS_DESCRIPTION"));
                            String purchaseOrderIssueDate = ((DocumentHandler)invoiceGoodsVector.get(i)).getAttribute("/PO_ISSUE_DATETIME");
                    		if(StringFunction.isNotBlank(purchaseOrderIssueDate)){
                    			purchaseOrderIssueDate = TPDateTimeUtility.formatDate(purchaseOrderIssueDate, TPDateTimeUtility.SHORT, formLocale);
                    		}
                            tempDoc.setAttribute("/POIssueDateLabel", 
                                            resMgr.getTextIncludingEmptyString("InvoicesOfferedDetail.PurchaseOrderIssueDate", 
                                                            TradePortalConstants.TEXT_BUNDLE));
                            tempDoc.setAttribute("/POIssueDate", purchaseOrderIssueDate);
                            String purchaseOrderID = ((DocumentHandler)invoiceGoodsVector.get(i)).getAttribute("/PO_REFERENCE_ID");

                            tempDoc.setAttribute("/POIDLabel", 
                                            resMgr.getTextIncludingEmptyString("InvoicesOfferedDetail.PurchaseOrderID", 
                                                            TradePortalConstants.TEXT_BUNDLE));
                            tempDoc.setAttribute("/POID", purchaseOrderID);

                            tempDoc.setAttribute("/section-header/name", resMgr.getText("InvoicesOfferedDetail.InvoiceSummaryDetail", TradePortalConstants.TEXT_BUNDLE));
                            outputDoc.addComponent("/SectionE/InvoiceGoodsDetails", tempDoc);
                        }
		}
	}
        
	/**
	 * 
	 * @param Output
	 *            Document - This is the Xml document being built to be sent out
	 *            for display.
	 */

	protected void sectionF(DocumentHandler outputDoc, DocumentHandler invoiceHistoryDoc, ResourceManager resMgr) throws RemoteException, AmsException {
		DocumentHandler tempDoc = null;

                outputDoc.setAttribute("/SectionF/InvoiceHistory/section-header/name", resMgr.getText("InvoicesOfferedDetail.InvoiceLog", TradePortalConstants.TEXT_BUNDLE));
                outputDoc.setAttribute("/SectionF/InvoiceHistory/name(0)", resMgr.getText("UploadInvoiceSummary.DateAndTime", TradePortalConstants.TEXT_BUNDLE));
                outputDoc.setAttribute("/SectionF/InvoiceHistory/name(1)", resMgr.getText("UploadInvoiceSummary.Action", TradePortalConstants.TEXT_BUNDLE));
                outputDoc.setAttribute("/SectionF/InvoiceHistory/name(2)", resMgr.getText("UploadInvoiceSummary.UserInfo", TradePortalConstants.TEXT_BUNDLE));
                outputDoc.setAttribute("/SectionF/InvoiceHistory/name(3)", resMgr.getText("UploadInvoiceSummary.Status", TradePortalConstants.TEXT_BUNDLE));

		if (invoiceHistoryDoc != null) {
			Vector<DocumentHandler> invoiceHistoryVector = invoiceHistoryDoc.getFragments("/ResultSetRecord/");
			int numberOfInvoiceHistory = invoiceHistoryVector.size();			
			
			for(int i= 0; i<numberOfInvoiceHistory; i++) {
                            tempDoc = new DocumentHandler();
                            String actionDate = invoiceHistoryVector.get(i).getAttribute("/DATEANDTIME");
                            outputDoc.setAttribute("/SectionF/InvoiceHistory/Log("+i+")/value(0)", actionDate);
                            outputDoc.setAttribute("/SectionF/InvoiceHistory/Log("+i+")/value(1)", refDataMgr.getDescr("UPLOAD_INVOICE_ACTION_TYPE", invoiceHistoryVector.get(i).getAttribute("/ACTION")));
                            outputDoc.setAttribute("/SectionF/InvoiceHistory/Log("+i+")/value(2)", invoiceHistoryVector.get(i).getAttribute("/USERINFO"));
                            outputDoc.setAttribute("/SectionF/InvoiceHistory/Log("+i+")/value(3)", refDataMgr.getDescr("SUPPLIER_PORTAL_INVOICE_STATUS", invoiceHistoryVector.get(i).getAttribute("/STATUS")));

                        }
		}
	}        

	/**
	 * This method is meant simply to test an object to see if it needs to be removed,
	 * If so, then we try to remove it.  If we get an exception, It would be probably
	 * Because we removed a parent of a component when we tried to remove the component
	 * object.
	 *
	 * @param BusinessObject - The object we're trying to remove.
	 * @param Object Type    - This is the type of object being sent out in the error message.
	 */
	private void beanRemove( Invoice invoiceOfferedData2, String objectType )
	{
		if( invoiceOfferedData2 != null )
		{   try{
					invoiceOfferedData2.remove();
			}catch(Exception e)
			{   LOG.info("Failed in removal of " + objectType + " Bean: Invoices Offered Details PDF Generator");
				LOG.info("This could be because this object is a component of a parent object previously removed.");
			}
		}
	}

}