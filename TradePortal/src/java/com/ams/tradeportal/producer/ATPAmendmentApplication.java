package com.ams.tradeportal.producer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.*;

import com.amsinc.ecsg.util.*;
import com.ams.tradeportal.common.*;
import com.amsinc.ecsg.frame.*;

/**
 * This class is designed to Build an XML Document with values from various business objects
 * then pass this data through an XSL stylesheet format the data for display then pass the
 * result on to a cocoon wrapper to display the result in a PDF format.  The method:
 * populateXmlDoc does most of the work, however get stream puts it all together and returns
 * the result (a Reader object).  The Corresponding XSL file is: ATPAmendApplication.xsl.
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
//BSL Cocoon Upgrade 09/28/11 Rel 7.0 BEGIN
//public class ATPAmendmentApplication extends DocumentProducer implements Status {
//	String headerStringPart2 = "/xsl/ATPAmendmentApplication.xsl\" type=\"text/xsl\"?>" + "<?cocoon-process type=\"xslt\"?>";
//
//	public Reader getStream(HttpServletRequest request) throws IOException, AmsException {
//		DocumentHandler xmlDoc = new DocumentHandler("<Document></Document>",false);
//		String myChildString = this.populateXmlDoc(xmlDoc, EncryptDecrypt.decryptAlphaNumericString(request.getParameter("transaction")), request.getParameter("locale"), request.getParameter("bank_prefix"), request.getContextPath()).toString();
//	    PropertyResourceBundle portalProperties = (PropertyResourceBundle) PropertyResourceBundle.getBundle("TradePortal");
//		String producerRoot = portalProperties.getString("producerRoot");
//		String myXmlString = headerStringPart1+producerRoot+"/"+request.getContextPath()+headerStringPart2+myChildString;
//		return new StringReader(myXmlString);
//	}
//
//	public String getStatus() {
//		return "ATPAmendmentApplication Producer";
//	}
public class ATPAmendmentApplication extends DocumentProducer {
private static final Logger LOG = LoggerFactory.getLogger(ATPAmendmentApplication.class);	
//BSL Cocoon Upgrade 09/28/11 Rel 7.0 END
	/**
	 * This method is the method that gathers all the data and builds the Xml document to be returned
	 * to the caller for display.  The Xsl template is expecting the Xml doc to send data out in a specific
	 * format...ie in sections.  If you scroll down the left side you can easily jump around the xml doc
	 * looking for specific section letters.
	 *
	 * @param Output Xml Doc - This is the resulting Xml doc that will be returned.
	 * @param Key            - This is the Transaction oid the will help us get all our data.
	 *                         With out this oid, no data can be retrieved.
	 * @param Locale         - This is the current users locale, it will help in establishing
	 *                         the client Server data bridge as well as building various B.O.s
	 * @param BankPrefix     - This may be removed in the future.
	 * @param ContextPath    - the context path under which this request was made
	 * @return OutputDoc     - Send out the result of building our Document.
	 */
	protected DocumentHandler populateXmlDoc(DocumentHandler outputDoc, String key, String locale, String bankPrefix, String contextPath)
	throws RemoteException, AmsException
	{

		// Call Ancestor Code first to setup all transaction objects
		super.populateXmlDoc(outputDoc, key, locale, bankPrefix, contextPath);

		try{
			//Section 'A' -- Title Bank Logo and document reference #
			sectionA( outputDoc);
			//Section 'B' -- Operational Bank Org info
			sectionB( outputDoc);
			//Section 'C' -- Op Bank Org Name Only
			sectionC( outputDoc);


			//Section 'N' -- Amendment Details
			sectionN( outputDoc);
			//Section 'O' -- Account Party
			sectionO( outputDoc);
			//Section 'P' -- Shipment Information
			sectionP( outputDoc);
			//Section 'Q' -- Goods Description
			sectionQ( outputDoc);
			//Section 'R' -- Additional Conditions
			sectionR( outputDoc);

			//Section 'M' -- Important Notice
			outputDoc.setAttribute( "/SectionM/","");


			LOG.debug("OutputDoc == " + outputDoc.toString() );

		}finally{  //remove any beans for clean up if they have data
			cleanUpBeans();
		}

		return outputDoc;

	}

	/**
	 * SectionA corresponds to the portion of the XML document labeled 'SectionA'.
	 * The following is an example of what the resulting Xml should look like:
	 *	<SectionA><!-- Header Info -->
	 *		<Title>
	 *			<Line1>Application and Agreement for</Line1>
	 *			<Line2>Irrevocable Commercial Letter of Credit</Line2>
	 *		</Title>
	 *		<Logo>http://localhost:7001//portal/images/blueyellow/en/</Logo>
	 *		<TransInfo>
	 *			<InstrumentID>ILC80US01P</InstrumentID>
	 *			<ReferenceNumber>123456REF</ReferenceNumber>
	 *			<ApplicationDate>05 October 2005</ApplicationDate>
	 *			<AmendmentDate>05 November 2005</AmendmentDate>
	 *		</TransInfo>
	 *	</SectionA>
	 *
	 * @param Output Document       - This is the Xml document being built to be sent out for display.
	 */
	protected void sectionA(DocumentHandler outputDoc)
	throws RemoteException, AmsException
	{
		// Customizations Here
		outputDoc.setAttribute( "/SectionA/Title/Line1", "Application for Amendment to");
		outputDoc.setAttribute( "/SectionA/Title/Line2", "Commercial Approval to Pay");

		// Call Parent Method
		super.sectionA(outputDoc);
	}


	/**
	 * SectionB corresponds to the portion of the XML document labeled 'SectionB'.
	 * The following is an example of what the resulting Xml should look like:
	 *	<SectionB><!-- Operational Bank Org Info -->
	 *		<Name>Operational Bank Org Name</Name>
	 *		<AddressLine1>Operational Bank Org Addr Line 1</AddressLine1>
	 *		<AddressLine2>Operational Bank Org Addr Line 2</AddressLine2>
	 *		<AddressStateProvince>Operational Bank Org City + Province</AddressStateProvince>
	 *		<AddressCountryPostCode>Operational Bank Org Country + Post Code</AddressCountryPostCode>
	 *		<PhoneNumber>Operational Bank Org Telephone #</PhoneNumber>
	 *		<Fax>Operational Bank Org Fax #</Fax>
	 *		<Swift>Operational Bank Org Address</Swift>
	 *		<Telex>Operational Bank Org Telex # Operational Bank Org Answer Back</Telex>
	 *	</SectionB>
	 *
	 * @param Output Document           - This is the Xml document being built to be sent out for display.
	 */

	protected void sectionB(DocumentHandler outputDoc)
	throws RemoteException, AmsException
	{
		// Customize Code

		// Call Parent Method
		super.sectionB(outputDoc);
	}

	/**
	 * SectionC corresponds to the portion of the XML document labeled 'SectionC'.
	 * The following is an example of what the resulting Xml should look like:
	 * 	<SectionC><!-- Operational Bank org Disclaimer -->
	 * 		<Name>Operational Bank Org Name</Name>
	 *	</SectionC>
	 *
	 * @param Output Document           - This is the Xml document being built to be sent out for display.
	 */
	protected void sectionC(DocumentHandler outputDoc)
	throws RemoteException, AmsException
	{
		// Override Code goes here

		// Call Parent Method
		super.sectionC(outputDoc);
	}

	/**
	 * SectionN corresponds to the portion of the XML document labeled 'SectionK'.
	 * The following is an example of what the resulting Xml should look like:
	 * 	<SectionN><!-- Amendment Details -->
	 * 		<IssueDate>11 December 2005</IssueDate>
	 * 		<LCCurrencyAmount>USD 12345.00</LCCurrencyAmount>
	 * 		<IncreaseBy>GBP 1234.12</IncreaseBy>
	 * 		<DecreaseBy>EUR 123.32</DecreaseBy>
	 * 		<NewCurrencyAmount>USD 4323.43</NewCurrencyAmount>
	 * 		<AmtTolerancePos>32</AmtTolerancePos>
	 * 		<AmtToleranceNeg>3</AmtToleranceNeg>
	 * 		<ExpiryDate>4 December 2003</ExpiryDate>
	 * 	</SectionN>
	 *
	 * @param Output Document       - This is the Xml document being built to be sent out for display.
	 *
	 */
	protected void sectionN(DocumentHandler outputDoc)
	throws RemoteException, AmsException
	{
		// Call ancestor Script
		super.sectionN(outputDoc);

		// Override code
	}

	/**
	 * SectionO corresponds to the portion of the XML document labeled 'SectionK'
	 * The following is an example of what the resulting Xml should look like:
	 * 	<SectionO><!-- Account Party -->
	 * 		<Name>Applicant Bank Name Goes Here</Confirmation>
	 * 	</SectionO>
	 *
	 * @param Output Document       - This is the Xml document being built to be sent out for display.
	 */
	protected void sectionO(DocumentHandler outputDoc)
	throws RemoteException, AmsException
	{
		//TAILE
		//// Call Ancestor Script
		//super.sectionO(outputDoc);
        //
		//// Override

		String instrumentOid = instrument.getAttribute("instrument_oid");
		//jgadela R92 - SQL INJECTION FIX
		String sqlQuery = "select p.name "
				+" from terms_party p, terms t, transaction tr"
				+" where tr.transaction_oid ="
				+"       (select max(transaction_oid)"
				+"          from transaction"
				+"         where transaction_status_date = (select max(transaction_status_date)"
				+"                                            from transaction"
				+"                                           where p_instrument_oid = ? "
				+"                                             and (transaction_type_code = ?  or transaction_type_code = ? or transaction_type_code = ?)"
				+"                                             and transaction_status = ?)"
				+"           and p_instrument_oid =? "
				+"           and (transaction_type_code = ? or transaction_type_code = ? or transaction_type_code = ?)"
				+"           and transaction_status = ?)"
				+" and tr.c_bank_release_terms_oid = t.terms_oid"
         		+" and (p.terms_party_oid = t.c_first_terms_party_oid"
         		+"      or p.terms_party_oid = t.c_second_terms_party_oid"
         		+"      or p.terms_party_oid = t.c_third_terms_party_oid"
         		+"      or p.terms_party_oid = t.c_fourth_terms_party_oid"
         		+"      or p.terms_party_oid = t.c_fifth_terms_party_oid)"
         		+" and p.terms_party_type = ?";
		
		
		Object sqlParams[] = new Object[11];
		sqlParams[0] = instrumentOid;
		sqlParams[1] = TransactionType.ISSUE;
		sqlParams[2] = TransactionType.AMEND;
		sqlParams[3] = TransactionType.CHANGE;
		sqlParams[4] = TransactionStatus.PROCESSED_BY_BANK;
		sqlParams[5] = instrumentOid;
		sqlParams[6] = TransactionType.ISSUE;
		sqlParams[7] = TransactionType.AMEND;
		sqlParams[8] = TransactionType.CHANGE;
		sqlParams[9] = TransactionStatus.PROCESSED_BY_BANK;
		sqlParams[10] = TermsPartyType.ATP_BUYER;

		DocumentHandler sqlQueryResult = DatabaseQueryBean.getXmlResultSet(sqlQuery, false, sqlParams);
		if (sqlQueryResult!=null)
			outputDoc.setAttribute("/SectionO/Name/", sqlQueryResult.getAttribute("/ResultSetRecord(0)/NAME"));

		//TAILE TAILE TAILE   - NEED TO REMOVE THIS
		super.sectionO(outputDoc);
		//TAILE TAILE TAILE


        //TAILE
	}


	/**
	 * SectionP corresponds to the portion of the XML document labeled 'SectionK'.
	 * The following is an example of what the resulting Xml should look like:
	 * 	<SectionP><!-- New Shipment Information -->
	 * 		<ShipmentDate>11 December 2005</ShipmentDate>
	 * 		<Incoterm>Delivery Ex Quay (Duty Paid) Somewhere</Incoterm>
	 * 		<From>Some Load of Port</From>
	 * 		<To>Final Destination 3 the movie</To>
	 * 	</SectionP>
	 *
	 * @param Output Document       - This is the Xml document being built to be sent out for display.
	 */
	protected void sectionP(DocumentHandler outputDoc)
	throws RemoteException, AmsException
	{
		// Call Parent Method
		super.sectionP(outputDoc);

	}


	/**
	 * SectionQ corresponds to the portion of the XML document labeled 'SectionK'.
	 * The following is an example of what the resulting Xml should look like:
	 * 	<SectionQ><!-- Goods Description -->
	 * 		<GoodsDescription>Goods Description</GoodsDescription>
	 * 	</SectionQ>
	 *
	 * @param Output Document       - This is the Xml document being built to be sent out for display.
	 *
	 */
	protected void sectionQ(DocumentHandler outputDoc)
	throws RemoteException, AmsException
	{
		// Call Parent Method
		super.sectionQ(outputDoc);

	}

	/**
	 * SectionR corresponds to the portion of the XML document labeled 'SectionK'.
	 * The following is an example of what the resulting Xml should look like:
	 * 	<SectionR><!-- Account Party -->
	 * 		<AdditionalConditions>Additional Conditions go here</Confirmation>
	 * 	</SectionR>
	 *
	 * @param Output Document       - This is the Xml document being built to be sent out for display.
	 */
	protected void sectionR(DocumentHandler outputDoc)
	throws RemoteException, AmsException
	{
		  //		( Invoice Only - EEZE - CR 375 09/14/2007 )
		String invoiceOnly = terms.getAttribute("invoice_only_ind");
		if (invoiceOnly.equals(TradePortalConstants.INDICATOR_YES))
			outputDoc.setAttribute( "/SectionR/InvoiceOnly/", "Invoice Only");
		
		 //		 Invoice Due Date - EEZE - CR 375 09/14/2007
		String invoiceDueDate = terms.getAttribute("invoice_due_date");
		if (isNotEmpty(invoiceDueDate))
			addNonNullDocData( formatDate(terms, "invoice_due_date", formLocale), outputDoc, "/SectionR/InvoiceDueDate");


		//SHR CR708 Rel8.1.1 -Invoice Details Start

		//jgadela R92 - SQL INJECTION FIX
		String strQry = "SELECT INVOICE_ID,to_char(ISSUE_DATE,'dd Mon yy') as issue_date,"
			+" to_char((case when payment_date is null then due_date else payment_date end),'dd Mon yy') as PAYDATE,CURRENCY, "
			+" AMOUNT, SELLER_NAME FROM invoices_summary_data WHERE a_terms_oid =?  ";

		DocumentHandler resultsSetDoc = DatabaseQueryBean.getXmlResultSet(strQry, false, new Object[]{terms.getAttribute("terms_oid")});
		if (resultsSetDoc != null) {
		 outputDoc.addComponent("/SectionR/InvoiceDetails/", resultsSetDoc);
		}
		//SHR CR708 Rel8.1.1 -Invoice Details End
		// Call Parent Method
		super.sectionR(outputDoc);
	}
}