package com.ams.tradeportal.producer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.*;

import com.amsinc.ecsg.util.*;
import com.ams.tradeportal.common.*;
import com.ams.tradeportal.busobj.*;
import com.amsinc.ecsg.frame.*;

/**
 * This class is designed to Build an XML Document with values from various business objects
 * then pass this data through an XSL stylesheet format the data for display then pass the 
 * result on to a cocoon wrapper to display the result in a PDF format.  The method: 
 * populateXmlDoc does most of the work, however get stream puts it all together and returns
 * the result (a Reader object).  The Corresponding XSL file is: DLCIssueApplication.xsl.
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
//BSL Cocoon Upgrade 09/28/11 Rel 7.0 BEGIN
//public class SLCIssueApplicationD extends DocumentProducer implements Status {
//String headerStringPart2 = "/xsl/SLCIssueApplicationD.xsl\" type=\"text/xsl\"?>" + "<?cocoon-process type=\"xslt\"?>";        
//
//public Reader getStream(HttpServletRequest request) throws IOException, AmsException {
//	DocumentHandler xmlDoc = new DocumentHandler("<Document></Document>",false);
//	String myChildString = this.populateXmlDoc(xmlDoc, EncryptDecrypt.decryptAlphaNumericString(request.getParameter("transaction")), request.getParameter("locale"), request.getParameter("bank_prefix"), request.getContextPath()).toString();
//    PropertyResourceBundle portalProperties = (PropertyResourceBundle) PropertyResourceBundle.getBundle("TradePortal");
//	String producerRoot = portalProperties.getString("producerRoot");
//	String myXmlString = headerStringPart1+producerRoot+"/"+request.getContextPath()+headerStringPart2+myChildString;
//	return new StringReader(myXmlString);
//}   
//
//public String getStatus() {
//	return "SLCIssueApplicationS Producer";
//}
public class SLCIssueApplicationD extends DocumentProducer {
	private static final Logger LOG = LoggerFactory.getLogger(SLCIssueApplicationD.class);
//BSL Cocoon Upgrade 09/28/11 Rel 7.0 END
	/**
	 * This method is the method that gathers all the data and builds the Xml document to be returned
	 * to the caller for display.  The Xsl template is expecting the Xml doc to send data out in a specific 
	 * format...ie in sections.  If you scroll down the left side you can easily jump around the xml doc
	 * looking for specific section letters.  
	 *
	 * @param Output Xml Doc - This is the resulting Xml doc that will be returned.
	 * @param Key            - This is the Transaction oid the will help us get all our data.
	 *                         With out this oid, no data can be retrieved.
	 * @param Locale         - This is the current users locale, it will help in establishing 
	 *                         the client Server data bridge as well as building various B.O.s
	 * @param BankPrefix     - This may be removed in the future.
	 * @param ContextPath    - the context path under which this request was made
	 * @return OutputDoc     - Send out the result of building our Document.
	 */
	protected DocumentHandler populateXmlDoc(DocumentHandler outputDoc, String key, String locale, String bankPrefix, String contextPath)
	throws RemoteException, AmsException
	{
		// Call Ancestor Code first to setup all transaction objects
		super.populateXmlDoc(outputDoc, key, locale, bankPrefix, contextPath);

		try{     
			String instrumentType = instrument.getAttribute("instrument_type_code");
			//Section 'A' -- Title Bank Logo and document reference #            
			sectionA( outputDoc);
			//Section 'B' -- Operational Bank Org info    
			sectionB( outputDoc);
			//Section 'C' -- Op Bank Org Name Only
			sectionC( outputDoc);			
			//Section 'D' -- Applicant and Beneficiary        
			sectionD( outputDoc);
			//Section 'E' -- Account Party (if necessary)
			sectionE( outputDoc);
			//Section 'F' -- Amount Details
			if(!isExpandedFormRequest(instrumentType))
				sectionF( outputDoc);
			//Section 'S' -- Issuing Bank/Validity/Delivery/Agent/Details/Terms/Instructions
			if(!isExpandedFormRequest(instrumentType))
				sectionS( outputDoc);
			else
				sectionSExpandedIssue(outputDoc);
			//
			if(isExpandedFormRequest(instrumentType))
				sectionL(outputDoc);
			
			if(isExpandedFormRequest(instrumentType)){
				sectionT1( outputDoc, locale);
				checkForSettlementInstructions(outputDoc); //New design update
				sectionT2( outputDoc);
				sectionU1( outputDoc);
				sectionU3( outputDoc);			
			}
			// jgadela 06/18.2014 IR T36000026067 - Added code for GUA pdf and made the important notice text dynamic.
			//Section 'M' -- Important Notice              
			//outputDoc.setAttribute( "/SectionM/","");   
			sectionM( outputDoc);
			LOG.debug("OutputDoc == " + outputDoc.toString() );
			
		}finally{  //remove any beans for clean up if they have data
			cleanUpBeans();
		}
		
		return outputDoc;
		
	}
	
	
	/**
	 * SectionA corresponds to the portion of the XML document labeled 'SectionA'.
	 * The following is an example of what the resulting Xml should look like:
	 *	<SectionA><!-- Header Info -->
	 *		<Title>
	 *			<Line1>Application and Agreement for</Line1>
	 *			<Line2>Irrevocable Commercial Letter of Credit</Line2>
	 *		</Title>
	 *		<Logo>http://localhost:7001//portal/images/blueyellow/en/</Logo>
	 *		<TransInfo>
	 *			<InstrumentNumLabel>ILC80US01P</InstrumentNumLabel>
	 *			<InstrumentID>ILC80US01P</InstrumentID>
	 *			<ReferenceNumber>123456REF</ReferenceNumber>
	 *			<ApplicationDate>05 October 2005</ApplicationDate>
	 *			<AmendmentDate>05 November 2005</AmendmentDate>
	 *		</TransInfo>
	 *	</SectionA>
	 *
	 * @param Output Document       - This is the Xml document being built to be sent out for display.
	 */
	protected void sectionA(DocumentHandler outputDoc) 
	throws RemoteException, AmsException
	{	
		// jgadela 06/18.2014 IR T36000026067 - Added code for GUA pdf and made the label dynamic [begin]
		String instrumentType = instrument.getAttribute("instrument_type_code");
		outputDoc.setAttribute( "/SectionA/Title/Line1", "Application and Agreement for");   
		// Customizations Here
		//MEer CR-1027
		if(InstrumentType.GUARANTEE.equalsIgnoreCase(instrumentType)){
			if(isExpandedFormRequest(instrumentType)){
				outputDoc.setAttribute( "/SectionA/Title/Line2", "Issuance of Guarantee"); 
			}else{
				outputDoc.setAttribute( "/SectionA/Title/Line2", "Irrevocable Outgoing Guarantee"); 
				outputDoc.setAttribute( "/SectionA/TransInfo/InstrumentNumLabel", "Guarantee Number:"); 
			}
		}else{
			outputDoc.setAttribute( "/SectionA/Title/Line2", "Irrevocable Standby Letter of Credit"); 
			if(!isExpandedFormRequest(instrumentType))
				outputDoc.setAttribute( "/SectionA/TransInfo/InstrumentNumLabel", "Letter of Credit Number:"); 
		}
		// jgadela 06/18.2014 IR T36000026067 - made the label dynamic [end]
		// Call Parent Method
		super.sectionA(outputDoc);
		
	}	
			
	
	/**
	 * SectionB corresponds to the portion of the XML document labeled 'SectionB'.
	 * The following is an example of what the resulting Xml should look like:	 
	 *	<SectionB><!-- Operational Bank Org Info -->
	 *		<Name>Operational Bank Org Name</Name>
	 *		<AddressLine1>Operational Bank Org Addr Line 1</AddressLine1>
	 *		<AddressLine2>Operational Bank Org Addr Line 2</AddressLine2>
	 *		<AddressStateProvince>Operational Bank Org City + Province</AddressStateProvince>
	 *		<AddressCountryPostCode>Operational Bank Org Country + Post Code</AddressCountryPostCode>
	 *		<PhoneNumber>Operational Bank Org Telephone #</PhoneNumber>
	 *		<Fax>Operational Bank Org Fax #</Fax>
	 *		<Swift>Operational Bank Org Address</Swift>
	 *		<Telex>Operational Bank Org Telex # Operational Bank Org Answer Back</Telex>
	 *	</SectionB>
	 *
	 * @param Output Document           - This is the Xml document being built to be sent out for display.
	 */
	
	protected void sectionB(DocumentHandler outputDoc) 
	throws RemoteException, AmsException
	{
		// Customize Code
		
		// Call Parent Method
		super.sectionB(outputDoc);
	}
	
	
	/**
	 * SectionC corresponds to the portion of the XML document labeled 'SectionC'.
	 * The following is an example of what the resulting Xml should look like:
	 * 	<SectionC><!-- Operational Bank org Disclaimer -->
	 * 		<Name>Operational Bank Org Name</Name>
	 *	</SectionC>
	 *
	 * @param Output Document           - This is the Xml document being built to be sent out for display.
	 */
	protected void sectionC(DocumentHandler outputDoc) 
	throws RemoteException, AmsException
	{
		// jgadela 06/18.2014 IR T36000026067 - - Added code for GUA pdf and made the text dynamic [begin]
		String name = opBankOrg.getAttribute("name");
		// Override Code goes here
		String instrumentType = instrument.getAttribute("instrument_type_code");
		
		String deliverTo = "";
        String deliverBy = "";
        if (terms.isAttributeRegistered("guar_deliver_to")){
		    deliverTo = terms.getAttribute("guar_deliver_to");//"BEN";//
            if (TradePortalConstants.DELIVER_TO_APPLICANT.equals(deliverTo))
                deliverTo = TradePortalConstants.DELIVARY_TO_APPLICANT_OPTION;
            if (TradePortalConstants.DELIVER_TO_BENEFICIARY.equals(deliverTo))
                deliverTo = TradePortalConstants.DELIVARY_TO_BENEFICIARY_OPTION;
            if (TradePortalConstants.DELIVER_TO_AGENT.equals(deliverTo))
                deliverTo = TradePortalConstants.DELIVARY_TO_AGENT_OPTION;
            if (TradePortalConstants.DELIVER_TO_OTHER.equals(deliverTo))
                deliverTo = TradePortalConstants.DELIVARY_TO_OTHER_OPTION;
        }
        if (terms.isAttributeRegistered("guar_deliver_by")){
            deliverBy = terms.getAttribute("guar_deliver_by");//"S";//
            if (TradePortalConstants.DELIVER_BY_TELEX.equals(deliverBy) || TradePortalConstants.DELIVER_BY_SWIFT.equals(deliverBy))
                deliverBy = TradePortalConstants.DELIVARY_BY_SWIFT_OPTION;
            if (TradePortalConstants.DELIVER_BY_REG_MAIL.equals(deliverBy))
                deliverBy = TradePortalConstants.DELIVARY_BY_REGISTERED_MAIL_OPTION;
            if (TradePortalConstants.DELIVER_BY_MAIL.equals(deliverBy))
                deliverBy = TradePortalConstants.DELIVARY_BY_MAIL_OPTION;
            if (TradePortalConstants.DELIVER_BY_COURIER.equals(deliverBy))
                deliverBy = TradePortalConstants.DELIVARY_BY_COURIER_OPTION;
        }		
		
		
		if(InstrumentType.GUARANTEE.equalsIgnoreCase(instrumentType)){
			outputDoc.setAttribute( "/SectionC/Name", "We (\"Applicant\") request you, "+name+" (\"Bank\") to issue an irrevocable Outgoing Guarantee  with the following terms and conditions for delivery to the  beneficiary named  below (\""+deliverTo+"\") by: "+deliverBy); 
		}else{
			outputDoc.setAttribute( "/SectionC/Name", "We (\"Applicant\") request you, "+name+" (\"Bank\") to issue an irrevocable standby letter of credit  (\"Credit\") with the following terms and conditions for delivery to the  beneficiary named  below (\""+deliverTo+"\") by: "+deliverBy); 
		}
		// Call Parent Method
		//super.sectionC(outputDoc);
		// jgadela 06/18.2014 IR T36000026067 - made the text dynamic [end]
	}
	
	
	/**
	 * SectionD corresponds to the portion of the XML document labeled 'SectionD'.
	 * The following is an example of what the resulting Xml should look like:
	 *	<SectionD><!-- Applicant and Beneficiary-->
	 *		<Applicant>
	 *			<Name>Applicant Name</Name>
	 *			<AddressLine1>Applicant Address Line 1</AddressLine1>
	 *			<AddressLine2>Applicant Address Line 2</AddressLine2>
	 *			<AddressLine3>Applicant Address Line 3</AddressLine3>
	 *			<AddressStateProvince>Applicant City + Province</AddressStateProvince>
	 *			<AddressCountryPostCode>Applicant Country + Post Code</AddressCountryPostCode>
	 *		</Applicant>
	 *		<Beneficiary>
	 *			<Name>Beneficiary Name</Name>
	 *			<AddressLine1>Beneficiary Address Line 1</AddressLine1>
	 *			<AddressLine2>Beneficiary Address Line 2</AddressLine2>
	 *			<AddressLine3>Beneficiary Address Line 3</AddressLine3>
	 *			<AddressStateProvince>Beneficiary City + Province</AddressStateProvince>
	 *			<AddressCountryPostCode>Beneficiary Country + Post Code</AddressCountryPostCode>
	 *			<PhoneNumber>Phone Number</PhoneNumber>
	 *		</Beneficiary>
	 *	</SectionD>
	 *
	 * @param Output Document             - This is the Xml document being built to be sent out for display.
	 */
	
	protected void sectionD(DocumentHandler outputDoc) 
	throws RemoteException, AmsException
	{
		// Override Code Here
		
		// Call Parent Method
		super.sectionD(outputDoc);

	}
	
	/**
	 * SectionE corresponds to the portion of the XML document labeled 'SectionE'.
	 * The following is an example of what the resulting Xml should look like:
	 *	<SectionE><!-- Account Party and Advising/Corresponding Bank -->
	 *		<AccountParty>
	 *			<Name>Account Party Name</Name>
	 *			<AddressLine1>Account Party Address Line 1</AddressLine1>
	 *			<AddressLine2>Account Party Address Line 2</AddressLine2>
	 *			<AddressLine3>Account Party Address Line 3</AddressLine3>
	 *			<AddressStateProvince>Account Party City + Province</AddressStateProvince>
	 *			<AddressCountryPostCode>Account Party Country + Post Code</AddressCountryPostCode>
	 *		</AccountParty>
	 *		<CorrespondentBank>
	 *			<Name>Advising Bank Name</Name>
	 *			<AddressLine1>Advising Bank Address Line 1</AddressLine1>
	 *			<AddressLine2>Advising Bank Address Line 2</AddressLine2>
	 *			<AddressLine3>Advising Bank Address Line 3</AddressLine3>
	 *			<AddressStateProvince>Advising Bank City + Province</AddressStateProvince>
	 *			<AddressCountryPostCode>Advising Bank Country + Post Code</AddressCountryPostCode>
	 *		</CorrespondentBank>
	 *		<AdvisingBank>
	 *			<Name>Advising Bank Name</Name>
	 *			<AddressLine1>Advising Bank Address Line 1</AddressLine1>
	 *			<AddressLine2>Advising Bank Address Line 2</AddressLine2>
	 *			<AddressLine3>Advising Bank Address Line 3</AddressLine3>
	 *			<AddressStateProvince>Advising Bank City + Province</AddressStateProvince>
	 *			<AddressCountryPostCode>Advising Bank Country + Post Code</AddressCountryPostCode>
	 *		</AdvisingBank>
	 *	</SectionE>
 	 *
	 * @param Output Document             - This is the Xml document being built to be sent out for display.
	 */
	protected void sectionE(DocumentHandler outputDoc) 
	throws RemoteException, AmsException
	{
		// Override Code
		
		// Call Parent Method
		super.sectionE(outputDoc);
	}
	
	/**
	 * SectionF corresponds to the portion of the XML document labeled 'SectionF'.
	 * The following is an example of what the resulting Xml should look like:
	 *	<SectionF><!-- Amount Detail  -->
	 *		<AmountDetail>
	 *			<CurrencyCodeAmount>USD 12345.01</CurrencyCodeAmount>
	 *			<CurrencyValueAmountInWords>US Dollars Twelve Thousand Three Hundred Fourty Five and .01</CurrencyValueAmountInWords>
	 *			<Type>Characteristic Type>/Type>
	 *	</SectionF>
	 *
	 * @param Output Document       - This is the Xml document being built to be sent out for display.
	 */
	protected void sectionF(DocumentHandler outputDoc)    
	throws RemoteException, AmsException
	{
		
		// Call Parent Code
		//super.sectionF(outputDoc);
		
		// Override Ancestor Code here
		
		
			String currencyCode = null;
			String currencyAmount = null;
			String currencyCodeAndAmount = null;

			currencyCode = terms.getAttribute("amount_currency_code");
			currencyAmount = terms.getAttribute("amount");
			currencyAmount = TPCurrencyUtility.getDisplayAmount(currencyAmount, currencyCode, formLocale);

			currencyCodeAndAmount = appendAttributes(currencyCode, currencyAmount, " ");

			outputDoc.setAttribute("/SectionF/AmountDetail/CurrencyCodeAmount/", currencyCodeAndAmount);

			if( isNotEmpty( currencyCode ) ) 
				currencyCode = refDataMgr.getDescr( TradePortalConstants.CURRENCY_CODE, currencyCode, formLocale );
			if( isNotEmpty( currencyAmount ) ) 
				currencyAmount = SpellItOut.spellOutNumber( currencyAmount, formLocale );

			currencyCodeAndAmount = appendAttributes(currencyCode, currencyAmount, " ");

			outputDoc.setAttribute( "/SectionF/AmountDetail/CurrencyValueAmountInWords/", currencyCodeAndAmount );

			String characteristicType = terms.getAttribute("characteristic_type");
			if( isNotEmpty( characteristicType )) {
				characteristicType = refDataMgr.getDescr( TradePortalConstants.CHARACTERISTIC_TYPE, characteristicType, formLocale);
				addNonNullDocData( characteristicType, outputDoc, "/SectionF/AmountDetail/Type");
			}
		

	}
	
	/**
	 * SectionS corresponds to the portion of the XML document labeled 'SectionF'.
	 * The following is an example of what the resulting Xml should look like:
	 * 	<SectionS>
	 * 		<IssuingBank>
	 * 			<IssuedBy>Applicant's Bank/Overseas Bank</IssuedBy>
	 * 			<Date>23 September 2006</Date>
	 * 			<OverseasBank>
	 * 				<Name>Applicant Name</Name>
	 * 				<AddressLine1>Applicant Address Line 1</AddressLine1>
	 * 				<AddressLine2>Applicant Address Line 2</AddressLine2>
	 * 				<AddressStateProvince>Applicant City + Province</AddressStateProvince>
	 * 				<AddressCountryPostCode>Applicant Country + Post Code</AddressCountryPostCode>
	 * 			</OverseasBank>
	 * 		</IssuingBank>
	 * 		<Validity>
	 * 			<ValidFrom>Date of Issue/06 December 2006</ValidFrom>
	 * 			<ValidTo>Other/5 January 2007</ValidTo>
	 * 		</Validity>
	 * 		<DeliveryInstructions>
	 * 			<DeliverTo>Beneficary</DeliverTo>
	 * 			<DeliverBy>Registered Mail</DeliverBy>
	 * 		</DeliveryInstructions>
	 * 		<AgentDetails>
	 * 			<Agent>
	 * 				<Name>Applicant Name</Name>
	 * 				<AddressLine1>Applicant Address Line 1</AddressLine1>
	 * 				<AddressLine2>Applicant Address Line 2</AddressLine2>
	 * 				<AddressStateProvince>Applicant City + Province</AddressStateProvince>
	 * 				<AddressCountryPostCode>Applicant Country + Post Code</AddressCountryPostCode>
	 * 				<ContactName>Contact Name</ContactName>
	 * 				<PhoneNumber>Phone Number</PhoneNumber>
	 * 			</Agent>
	 * 			<Instructions>Accept instructions from this party/Advise Issuance</Instructions>
	 * 		</AgentDetails>
	 * 		<ContractDetails>Details of Tender/order/Contract Text goes here</ContractDetails>
	 * 		<Terms>
	 * 			<Customer>Customer Terms Text</Customer>
	 * 			<Bank>Bank Terms Text</Bank>
	 * 		</Terms>
	 * 		<BankInstructions>Instructions to Bank Text</BankInstructions>
	 * 	</SectionS>
	 *
	 * @param Output Document       - This is the Xml document being built to be sent out for display.
	 */
	private void sectionS(DocumentHandler outputDoc)    
	throws RemoteException, AmsException
	{

		 // Issuing Bank
		 String issueType = terms.getAttribute("guarantee_issue_type");
		 if (issueType.equals(TradePortalConstants.GUAR_ISSUE_APPLICANTS_BANK))
			 outputDoc.setAttribute("/SectionS/IssuingBank/IssuedBy", "Applicant's Bank");
		 if (issueType.equals(TradePortalConstants.GUAR_ISSUE_OVERSEAS_BANK))
			 outputDoc.setAttribute("/SectionS/IssuingBank/IssuedBy", "Overseas Bank");
		 
		 String validityDate = terms.getAttribute("overseas_validity_date");
		 if (isNotEmpty(validityDate))
			 outputDoc.setAttribute("/SectionS/IssuingBank/Date", formatDate(terms, "overseas_validity_date", formLocale));

		 TermsParty overseasBank = null;     // Overseas Bank Terms Party (Third Terms Party)	
		 String addressLine2 = "";
		 String cityState  = "";
		 String countryZip = "";
		 String countryDescr = "";
		 String thirdTermsPartyOid = terms.getAttribute("c_ThirdTermsParty");
		 if ( isNotEmpty( thirdTermsPartyOid)) {
			 overseasBank = (TermsParty)terms.getComponentHandle("ThirdTermsParty");
			 addNonNullDocData( overseasBank.getAttribute("name"), outputDoc, "/SectionS/IssuingBank/OverseasBank/Name/");
			 addNonNullDocData( overseasBank.getAttribute("address_line_1"), outputDoc, "/SectionS/IssuingBank/OverseasBank/AddressLine1/");
			 addressLine2 = overseasBank.getAttribute("address_line_2");
			 if( isNotEmpty( addressLine2 ) )
				 outputDoc.setAttribute( "/SectionS/IssuingBank/OverseasBank/AddressLine2", addressLine2 );
			 cityState = appendAttributes(overseasBank.getAttribute("address_city"), overseasBank.getAttribute("address_state_province"), ", " );
			 if( isNotEmpty( cityState ) )
				 outputDoc.setAttribute( "/SectionS/IssuingBank/OverseasBank/AddressStateProvince/", cityState );
			 if(refDataMgr.checkCode(TradePortalConstants.COUNTRY, overseasBank.getAttribute("address_country"), formLocale))
				 countryDescr = refDataMgr.getDescr(TradePortalConstants.COUNTRY, overseasBank.getAttribute("address_country"), formLocale);
			 else
				 countryDescr = "";          
			 countryZip = appendAttributes(countryDescr, overseasBank.getAttribute("address_postal_code"), ", ");
			 if( isNotEmpty( countryZip ) )
				 outputDoc.setAttribute( "/SectionS/IssuingBank/OverseasBank/AddressCountryPostCode/", countryZip );
		 }
		 
		 // Validity
		 String validFromType = terms.getAttribute("guar_valid_from_date_type");
		 if (validFromType.equals(TradePortalConstants.DATE_OF_ISSUE))
			 outputDoc.setAttribute("/SectionS/Validity/ValidFrom", "Date of Issue");
		 if (validFromType.equals(TradePortalConstants.OTHER_VALID_FROM_DATE))
			 outputDoc.setAttribute("/SectionS/Validity/ValidFrom", formatDate(terms, "guar_valid_from_date", formLocale));
			 
		 String validToType = terms.getAttribute("guar_expiry_date_type");
		 if (validToType.equals(TradePortalConstants.VALIDITY_DATE))
			 outputDoc.setAttribute("/SectionS/Validity/ValidTo", formatDate(terms, "expiry_date", formLocale));
		 if (validToType.equals(TradePortalConstants.OTHER_EXPIRY_DATE))
			 outputDoc.setAttribute("/SectionS/Validity/ValidTo", "Other - see Instructions to Bank");
			 
		 // Delivery Instructions
		 String deliverToType = terms.getAttribute("guar_deliver_to");
		 if (deliverToType.equals(TradePortalConstants.DELIVER_TO_APPLICANT))
			 outputDoc.setAttribute("/SectionS/DeliveryInstructions/DeliverTo", "Applicant");
		 if (deliverToType.equals(TradePortalConstants.DELIVER_TO_BENEFICIARY))
			 outputDoc.setAttribute("/SectionS/DeliveryInstructions/DeliverTo", "Beneficiary");
		 if (deliverToType.equals(TradePortalConstants.DELIVER_TO_AGENT))
			 outputDoc.setAttribute("/SectionS/DeliveryInstructions/DeliverTo", "Agent");
		 if (deliverToType.equals(TradePortalConstants.DELIVER_TO_OTHER))
			 outputDoc.setAttribute("/SectionS/DeliveryInstructions/DeliverTo", "Other - see Instructions to Bank");
		 
		 String deliverByType = terms.getAttribute("guar_deliver_by");
		 if (deliverByType.equals(TradePortalConstants.DELIVER_BY_TELEX))
			 outputDoc.setAttribute("/SectionS/DeliveryInstructions/DeliverBy", "Swift");
		 if (deliverByType.equals(TradePortalConstants.DELIVER_BY_REG_MAIL))
			 outputDoc.setAttribute("/SectionS/DeliveryInstructions/DeliverBy", "Registered Mail");
		 if (deliverByType.equals(TradePortalConstants.DELIVER_BY_MAIL))
			 outputDoc.setAttribute("/SectionS/DeliveryInstructions/DeliverBy", "Mail");
		 if (deliverByType.equals(TradePortalConstants.DELIVER_BY_COURIER))
			 outputDoc.setAttribute("/SectionS/DeliveryInstructions/DeliverBy", "Courier");
		 
		 // Agent Details
		 TermsParty agentBank = null;     // Agent Bank Terms Party (Fourth Terms Party)	
		 String fourthTermsPartyOid = terms.getAttribute("c_FourthTermsParty");
		 if ( isNotEmpty( fourthTermsPartyOid)) {
			 agentBank = (TermsParty)terms.getComponentHandle("FourthTermsParty");
			 addNonNullDocData( agentBank.getAttribute("name"), outputDoc, "/SectionS/AgentDetails/Agent/Name/");
			 addNonNullDocData( agentBank.getAttribute("address_line_1"), outputDoc, "/SectionS/AgentDetails/Agent/AddressLine1/");
			 addressLine2 = agentBank.getAttribute("address_line_2");
			 if( isNotEmpty( addressLine2 ) )
				 outputDoc.setAttribute( "/SectionS/AgentDetails/Agent/AddressLine2", addressLine2 );
			 cityState = appendAttributes(agentBank.getAttribute("address_city"), agentBank.getAttribute("address_state_province"), ", " );
			 if( isNotEmpty( cityState ) )
				 outputDoc.setAttribute( "/SectionS/AgentDetails/Agent/AddressStateProvince/", cityState );
			 if(refDataMgr.checkCode(TradePortalConstants.COUNTRY, agentBank.getAttribute("address_country"), formLocale))
				 countryDescr = refDataMgr.getDescr(TradePortalConstants.COUNTRY, agentBank.getAttribute("address_country"), formLocale);
			 else
				 countryDescr = "";          
			 countryZip = appendAttributes(countryDescr, agentBank.getAttribute("address_postal_code"), ", ");
			 if( isNotEmpty( countryZip ) )
				 outputDoc.setAttribute( "/SectionS/AgentDetails/Agent/AddressCountryPostCode/", countryZip );
			 addNonNullDocData( agentBank.getAttribute("contact_name"), outputDoc, "/SectionS/AgentDetails/Agent/ContactName/");
			 addNonNullDocData( agentBank.getAttribute("phone_number"), outputDoc, "/SectionS/AgentDetails/Agent/PhoneNumber/");
		 }
		 
		 
		 //guar_accept_instr   guar_advise_issuance
		 String instructions = null;
		 String acceptInstructions = terms.getAttribute("guar_accept_instr");
		 String adviseIssuance = terms.getAttribute("guar_advise_issuance");
		 if (acceptInstructions.equals(TradePortalConstants.INDICATOR_YES) && (adviseIssuance.equals(TradePortalConstants.INDICATOR_YES)))
			 instructions = "Accept Instructions from this party and Advise Issuance";
		 else if(acceptInstructions.equals(TradePortalConstants.INDICATOR_YES))
			 instructions = "Accept Instructions from this party";
		 else if(adviseIssuance.equals(TradePortalConstants.INDICATOR_YES))
			 instructions = "Advise Issuance";
		 else ;
		 // Do Nothing
		 addNonNullDocData(instructions, outputDoc, "/SectionS/AgentDetails/Instructions");
		 
		 // Contract Details
		 addNonNullDocData(terms.getAttribute("tender_order_contract_details"), outputDoc, "/SectionS/ContractDetails");
		 
		 // Terms
		 addNonNullDocData(terms.getAttribute("guar_customer_text"), outputDoc, "/SectionS/Terms/Customer");
		 addNonNullDocData(terms.getAttribute("guar_bank_standard_text"), outputDoc, "/SectionS/Terms/Bank");
		 
		 // Bank Instructions
		 addNonNullDocData(terms.getAttribute("special_bank_instructions"), outputDoc, "/SectionS/BankInstructions");
		 
		//Sandeep - Rel-8.3 CR-752 Dev 07/01/2013 - Begin 
		//ICC Publications
		 String value = terms.getAttribute("ucp_version_details_ind");
		 if(StringFunction.isNotBlank(value) && value.equals(TradePortalConstants.INDICATOR_YES)){
			 outputDoc.setAttribute( "/SectionS/ICCPublications/", "");
			 outputDoc.setAttribute( "/SectionS/ICCApplicableRules/", "Yes");
			 outputDoc.setAttribute( "/SectionS/ICCPVersion/", ReferenceDataManager.getRefDataMgr().getDescr("ICC_GUIDELINES", terms.getAttribute("ucp_version")));
			
			 value = terms.getAttribute("ucp_details");
				if(StringFunction.isNotBlank(value))
					outputDoc.setAttribute( "/SectionS/ICCPDetails/", value);
		 }
		//Sandeep - Rel-8.3 CR-752 Dev 07/01/2013 - End
		 
		 String autoExtInd = terms.getAttribute("auto_extension_ind");

		 if (TradePortalConstants.INDICATOR_YES.equals(autoExtInd)) {

			 String maxAutoExtAllowed = terms.getAttribute("max_auto_extension_allowed");
			 String autoExtPeriod     = terms.getAttribute("auto_extension_period");
			 String autoExtDays       = terms.getAttribute("auto_extension_days");
			 String notifyBeneDays    = terms.getAttribute("notify_bene_days");

			 outputDoc.setAttribute("/SectionS/AutoExtension/ExtAllowed", "Yes");
			 outputDoc.setAttribute("/SectionS/AutoExtension/MaxExtAllowed", maxAutoExtAllowed);
			 outputDoc.setAttribute("/SectionS/AutoExtension/AutoExtPeriod",
					 refDataMgr.getDescr(TradePortalConstants.AUTO_EXTEND_PERIOD_TYPE, autoExtPeriod, formLocale));
			 outputDoc.setAttribute("/SectionS/AutoExtension/AutoExtDays", autoExtDays);
			 outputDoc.setAttribute("/SectionS/AutoExtension/FinalExpiryDate", formatDate(terms, "final_expiry_date", formLocale));
			 outputDoc.setAttribute("/SectionS/AutoExtension/NotifyBeneDays", notifyBeneDays);
			 
		 }
	}
	
	
	protected void sectionT1(DocumentHandler outputDoc, String locale)
	throws RemoteException, AmsException
	{
        String languageDesc = ReferenceDataManager.getRefDataMgr().getDescr("INSTRUMENT_LANGUAGE",instrument.getAttribute("language"),locale);		
		addNonNullDocData(languageDesc, outputDoc,"/SectionT1/InstructionsToBank/Issue");
		addNonNullDocData( terms.getAttribute("special_bank_instructions"), outputDoc, "/SectionT1/InstructionsToBank/Additional");
	}	
	
	/**
	 *	 * The following is an example of what the resulting Xml should look like:
    <SectionT2>
        <SettlementInstructions>
            <DebitOurAccount>
                Debit: Our Account Number 
            </DebitOurAccount>
            <BranchCode>
                Branch Code
            </BranchCode>
            <DebitForeignAccount>
                Debit: Foreign Currency Account Number 
            </DebitForeignAccount>
            <CurrencyOfAccount>
                Currency of Account dropdown
            </CurrencyOfAccount>            
        </SettlementInstructions>  
    </SectionT2>  
	 *
	 * @param Output Document       - This is the Xml document being built to be sent out for display.
	 */
	protected void sectionT2(DocumentHandler outputDoc)
	throws RemoteException, AmsException
	{

		addNonNullDocData(terms.getAttribute("settlement_our_account_num"), outputDoc, "/SectionT2/SettlementInstructions/DebitOurAccount");
		addNonNullDocData(terms.getAttribute("branch_code"), outputDoc, "/SectionT2/SettlementInstructions/BranchCode");
		addNonNullDocData(terms.getAttribute("settlement_foreign_acct_num"), outputDoc, "/SectionT2/SettlementInstructions/DebitForeignAccount");
		addNonNullDocData(terms.getAttribute("settlement_foreign_acct_curr"), outputDoc, "/SectionT2/SettlementInstructions/CurrencyOfAccount");		
	}
	
	
	protected void sectionU1(DocumentHandler outputDoc)
	throws RemoteException, AmsException
	{

		addNonNullDocData(terms.getAttribute("coms_chrgs_our_account_num"), outputDoc, "/SectionU1/CommisionAndCharges/DebitOurAccount");
		addNonNullDocData(terms.getAttribute("coms_chrgs_foreign_acct_num"), outputDoc, "/SectionU1/CommisionAndCharges/DebitForeignAccount");
		addNonNullDocData(terms.getAttribute("coms_chrgs_foreign_acct_curr"), outputDoc, "/SectionU1/CommisionAndCharges/CurrencyOfAccount");
	}
	
	protected void sectionU3(DocumentHandler outputDoc) throws RemoteException, AmsException{
		addNonNullDocData(terms.getAttribute("coms_chrgs_other_text"), outputDoc, "/SectionU3/AdditionalInstructions");
		// terms.getAttribute("internal_instructions")-for Internal Instructions
		
	}
	/* @param Output Document       - This is the Xml document being built to be sent out for display.
	 */
	protected void sectionM(DocumentHandler outputDoc) 
	throws RemoteException, AmsException
	{	// jgadela 06/18.2014 IR T36000026067 - Added code for GUA pdf and made the important notice text dynamic [end]
		String instrumentType = instrument.getAttribute("instrument_type_code");
		// Customizations Here
		if(InstrumentType.GUARANTEE.equalsIgnoreCase(instrumentType)){
			outputDoc.setAttribute( "/SectionM/NoticeLine1", "Applicant understands that the risk is greater if Applicant requests a  Guarantee which requires only a draft without any supporting documentation. Typically,  Guarantee's require the beneficiary to provide some statement of alleged non-performance or default in order to obtain payment. However, a beneficiary that can obtain a Guarantee available only against presentation of a draft or a demand, relieves itself of any documentary requirements.");
			outputDoc.setAttribute( "/SectionM/NoticeLine2", "Applicant understands that the final form of Guarantee may be subject to such revisions and changes as are deemed necessary or appropriate by Bank and Applicant hereby consents to such revisions and changes."); 
			if(!isExpandedFormRequest(instrumentType))
				outputDoc.setAttribute( "/SectionM/NoticeLine3", "The opening of the Credit is subject to the terms and conditions as set forth in the guarantee agreement to which the Applicant agrees and, if Applicant's continuing agreement is lodged with Bank, subject to the terms and provisions set forth therein.");
			else
				outputDoc.setAttribute( "/SectionM/NoticeLine3", "The issuance of the Guarantee is subject to the terms and conditions as set forth in the Guarantee Agreement to which the Applicant agrees and, if Applicant�s continuing agreement is lodged with Bank, subject to the terms and provisions set forth therein.");

		}else{
			if ( TradePortalConstants.INDICATOR_YES.equals(enableCustomPDF) ) {
		    	outputDoc.setAttribute( "/SectionM/NoticeLine1", "Applicant understands that the final form of Credit may be subject to such revisions and changes as are deemed necessary or appropriate by Bank and Applicant hereby consents to such revisions and changes.");
		    	outputDoc.setAttribute( "/SectionM/NoticeLine3", "The opening of the Credit shall be subject to the terms of the Continuing Letter of Credit Agreement or Continuing Standby Letter of Credit Agreement entered into between Applicant and Bank.");
		    } else {
		    	outputDoc.setAttribute( "/SectionM/NoticeLine1", "Applicant understands that the risk is greater if Applicant requests a standby letter of credit which requires only a draft without any supporting documentation. Typically, standby letters of credit require the beneficiary to provide some statement of alleged non-performance or default in order to obtain payment. However, a beneficiary that can obtain a standby letter of credit available only against presentation of a draft or a demand, relieves itself of any documentary requirements.");
		    	outputDoc.setAttribute( "/SectionM/NoticeLine2", "Applicant understands that the final form of Credit may be subject to such revisions and changes as are deemed necessary or appropriate by Bank and Applicant hereby consents to such revisions and changes.");
		    	outputDoc.setAttribute( "/SectionM/NoticeLine3", "The opening of the Credit is subject to the terms and conditions as set forth in the Standby Letter of Credit Agreement to which the Applicant agrees and, if Applicant's continuing agreement is lodged with Bank, subject to the terms and provisions set forth therein.");
		    }
		}
		//outputDoc.setAttribute( "/SectionM/","");  
		// jgadela 06/18.2014 IR T36000026067 - made the important notice text dynamic [end]
	}
	
	
	//For Validity, delivery Instructions, Detailed Information
	protected void sectionSExpandedIssue(DocumentHandler outputDoc) throws RemoteException, AmsException {
		
	}

	
	//For Other Conditions
	protected void sectionL(DocumentHandler outputDoc) throws RemoteException, AmsException {

	}
	

	
}