package com.ams.tradeportal.producer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.Reader;

import javax.servlet.http.HttpServletRequest;

import org.apache.avalon.framework.service.ServiceException;
import org.apache.cocoon.ProcessingException;
import org.apache.cocoon.ResourceNotFoundException;
import org.apache.cocoon.environment.ObjectModelHelper;
import org.apache.cocoon.environment.Request;
import org.apache.cocoon.generation.ServiceableGenerator;
import org.apache.excalibur.xml.sax.SAXParser;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.amsinc.ecsg.frame.AmsException;

public abstract class AbstractProducerGenerator extends ServiceableGenerator {
private static final Logger LOG = LoggerFactory.getLogger(AbstractProducerGenerator.class);

	/**
	 * Generate XML document as Reader.
	 */
	public abstract Reader getStream(HttpServletRequest request) throws IOException, AmsException;

	/**
	 * Generate XML data out of request InputStream.
	 */
	public void generate() throws SAXException, ProcessingException {
		SAXParser parser = null;
		Request request = ObjectModelHelper.getRequest(this.objectModel);

		try {
			Reader xmlReader = getStream(request);
			InputSource source = new InputSource(xmlReader);

			parser = (SAXParser) this.manager.lookup(SAXParser.ROLE);
			parser.parse(source, super.xmlConsumer);
		}
		catch (IOException e) {
			getLogger().error("AbstractProducerGenerator.generate()", e);
			throw new ResourceNotFoundException("AbstractProducerGenerator could not find resource", e);
		}
		catch (SAXException e) {
			getLogger().error("AbstractProducerGenerator.generate()", e);
			throw(e);
		}
		catch (ServiceException e) {
			getLogger().error("Could not get parser", e);
			throw new ProcessingException("Exception in AbstractProducerGenerator.generate()", e);
		} 
		catch (AmsException e) {
			getLogger().error("Could not generate document in Portal", e);
			throw new ProcessingException("Portal exception in AbstractProducerGenerator.generate()", e);
		}
		finally {
			this.manager.release(parser);
		}
	}
}
