/**
 *
 */
package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;
import java.util.List;

/**

 *
 */
public class SubsidiaryAccountsDataView     extends AbstractDBDataView {
private static final Logger LOG = LoggerFactory.getLogger(SubsidiaryAccountsDataView.class);

	private final List<Object> sqlParams = new ArrayList<>();

	/**
	 * Constructor
	 */
	public SubsidiaryAccountsDataView() {
		super();
	}

    protected String buildDynamicSQLCriteria(Map<String,String> parameters) {
        StringBuilder sql = new StringBuilder();

        String subOrgOid = parameters.get("custName");
        String corporateOrgOid = parameters.get("corporateOrgOid");
        String accountName = parameters.get("accountName");

        if ( subOrgOid!=null ) {
            //limit to just that subsidiary
            sql.append(" and a.p_owner_oid = ? "); //subOrgOid  
            sqlParams.add(subOrgOid);
        }
        else { 
            //get all subsidiaries for the given corporate org
            //todo: throw an exception if corporate org is missing
            sql.append(" and p_owner_oid in (");
            sql.append("  select organization_oid from corporate_org");
            sql.append("   connect by prior organization_oid = p_parent_corp_org_oid");
            sql.append("   start with p_parent_corp_org_oid = ?)"); //corporateOrgOid
            sqlParams.add(corporateOrgOid);
        }
                
        // Account list should not contain accounts from sub-subsidiaries of this subsidiary.
        sql.append(" and (othercorp_customer_indicator = ? or othercorp_customer_indicator is null)");
        sqlParams.add("N");

        if(accountName != null) {
            //just hardcode this as the parameter version doesn't work inside a literal
            //cquinton 12/3/2012 changed so it works as startswith rather than contains
            sql.append(" and upper(account_name) like upper(?)");
            sqlParams.add(accountName + "%");
        }
        
        return sql.toString();
    }

    protected int setDynamicSQLValues(PreparedStatement statement, int startVarIdx,
            Map<String,String> parameters)
            throws SQLException {

    	return setSqlVarsToPreparedStmt(statement, startVarIdx, sqlParams);
    }
}
