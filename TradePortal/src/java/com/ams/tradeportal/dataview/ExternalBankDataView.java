package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;
import java.util.List;
import com.ams.tradeportal.common.TradePortalConstants;

public class ExternalBankDataView extends AbstractDBDataView{
private static final Logger LOG = LoggerFactory.getLogger(ExternalBankDataView.class);
	
	private final List<Object> sqlParams = new ArrayList<>();
	
		/**
		 * Constructor
		 */
		public ExternalBankDataView() {
			super();
		}

	    protected String buildDynamicSQLCriteria(Map<String,String> parameters) {


	        	String sql =" and a.p_owner_bank_oid = ? and a.activation_status = ? ";
	          	sqlParams.add( getUserSession().getClientBankOid());
	          	sqlParams.add(TradePortalConstants.ACTIVE);
	      
	        return sql;
	    }
	    
	    protected int setDynamicSQLValues(PreparedStatement statement, int startVarIdx,
	            Map<String,String> parameters)
	            throws SQLException {

	    	return setSqlVarsToPreparedStmt(statement, startVarIdx, sqlParams);
	    }

}
