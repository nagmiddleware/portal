/**
 *
 */
package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;
import java.util.List;

import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.util.EncryptDecrypt;

/**
 *
 *
 */
public class InvoiceFileUploadDataView     extends AbstractDBDataView
{
private static final Logger LOG = LoggerFactory.getLogger(InvoiceFileUploadDataView.class);
	
	private final List<Object> sqlParams = new ArrayList<>();
	/**
	 * Constructor
	 */
	public InvoiceFileUploadDataView() {
		super();
	}


	final static String ALL_WORK = "common.allWork";
    final static String MY_WORK = "common.myWork";
	
    protected String buildDynamicSQLCriteria(Map<String,String> parameters) {
        StringBuilder dynamicWhereClause = new StringBuilder();

        String selectedWorkflow = parameters.get("selectedWorkflow");
        String userOid = getUserSession().getUserOid();
        String userOrgOid = getUserSession().getOwnerOrgOid();
        String displayedValue = parameters.get("displayedValue");

        if (displayedValue != null){
        	selectedWorkflow = EncryptDecrypt.decryptStringUsingTripleDes(selectedWorkflow, getUserSession().getSecretKey());
        }

        if (selectedWorkflow.equals(MY_WORK))
        {
           dynamicWhereClause.append("and INVOICE_FILE_UPLOADS.a_user_oid = ? "); //Rkazi - 05/24/2011 IR POUL051854748 changed
  								     // a_user_oid to a_assigned_to_user_oid.
           sqlParams.add(userOid);
        }
        else if (selectedWorkflow.equals(ALL_WORK))
        {
           dynamicWhereClause.append("and INVOICE_FILE_UPLOADS.a_owner_org_oid in (");
           dynamicWhereClause.append(" select organization_oid");
           dynamicWhereClause.append(" from corporate_org");
           dynamicWhereClause.append(" where activation_status = ? start with organization_oid = ? ");
           dynamicWhereClause.append(" connect by prior organization_oid = p_parent_corp_org_oid)");
           
           sqlParams.add(TradePortalConstants.ACTIVE);
           sqlParams.add(userOrgOid);
        }
        else
        {
           dynamicWhereClause.append("and INVOICE_FILE_UPLOADS.a_owner_org_oid  = ? ");
           sqlParams.add(selectedWorkflow);
        }


        return dynamicWhereClause.toString();
    }

    protected int setDynamicSQLValues(PreparedStatement statement, int startVarIdx,
            Map<String,String> parameters)
            throws SQLException {

    	return setSqlVarsToPreparedStmt(statement, startVarIdx, sqlParams);
    }
}
