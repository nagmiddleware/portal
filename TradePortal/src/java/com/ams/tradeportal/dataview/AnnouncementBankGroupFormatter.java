package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.util.ResourceManager;


/**
 * AnnouncementBankGroupFormatter finds the locale specific value for
 * some specific bank_group column values that come back from the dataview
 * or returns the bank group name as given.
 */
public class AnnouncementBankGroupFormatter extends AbstractCustomDataViewColumnFormatter {
private static final Logger LOG = LoggerFactory.getLogger(AnnouncementBankGroupFormatter.class);

	/**
	 * Format data.
	 * @param obj - assumes a string value with special values
	 *     'All' and 'Multiple' requiring localization.
	 */
    public String format(Object obj) {
        String formattedData = "";

        ResourceManager resMgr = this.getResourceManager();

        String value = (String) obj;

        if ( "All".equals(value) ) {
            formattedData = resMgr.getText( "dataview.announcement.BankGroup.All",
                TradePortalConstants.TEXT_BUNDLE );
        }
        else if ( "Multiple".equals(value) ) {
            formattedData = resMgr.getText( "dataview.announcement.BankGroup.Multiple",
                TradePortalConstants.TEXT_BUNDLE );
        }
        else {
            formattedData = value;
        }

        return formattedData;
    }
}
