/**
 *
 */
package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Map;

import com.ams.tradeportal.common.SQLParamFilter;
import com.ams.tradeportal.common.TradePortalConstants;

/**
 *
 *
 */
public class InvoiceGroupDataView extends AbstractDBDataView
{
private static final Logger LOG = LoggerFactory.getLogger(InvoiceGroupDataView.class);
	/**
	 * Constructor
	 */
	public InvoiceGroupDataView() {
		super();
	}

    protected String buildDynamicSQLCriteria(Map<String,String> parameters) {

    	StringBuilder dynamicWhereClause     = new StringBuilder();
    	String userOrgOid = getUserSession().getOwnerOrgOid();
    	String invoiceStatusType = SQLParamFilter.filter(parameters.get("selectedStatus"));

    	if (!TradePortalConstants.STATUS_ALL.equals(invoiceStatusType)) {
            dynamicWhereClause.append(" and (i.invoice_status = '");
            dynamicWhereClause.append(invoiceStatusType);
            dynamicWhereClause.append("'");
            if (TradePortalConstants.UPLOAD_INV_STATUS_AUTH.equals(invoiceStatusType)) {
                dynamicWhereClause.append(" or i.invoice_status = '");
                dynamicWhereClause.append(TradePortalConstants.UPLOAD_INV_STATUS_FIN_AUTH);
                dynamicWhereClause.append("'");
            }
            else if (TradePortalConstants.UPLOAD_INV_STATUS_PAR_AUTH.equals(invoiceStatusType)) {
                dynamicWhereClause.append(" or i.invoice_status = '");
                dynamicWhereClause.append(TradePortalConstants.UPLOAD_INV_STATUS_FIN_PAR_AUTH);
                dynamicWhereClause.append("'");
            }
            else if (TradePortalConstants.UPLOAD_INV_STATUS_AUTH_FAILED.equals(invoiceStatusType)) {
                dynamicWhereClause.append(" or i.invoice_status = '");
                dynamicWhereClause.append(TradePortalConstants.UPLOAD_INV_STATUS_FIN_AUTH_FAILED);
                dynamicWhereClause.append("'");
            }
            dynamicWhereClause.append(")");
         }
         dynamicWhereClause.append(" and i.a_corp_org_oid = ").append(userOrgOid);

        return dynamicWhereClause.toString();

    }

    protected int setDynamicSQLValues(PreparedStatement statement, int startVarIdx,
            Map<String,String> parameters)
            throws SQLException {
        int varCount = 0;



        return varCount;
    }
}
