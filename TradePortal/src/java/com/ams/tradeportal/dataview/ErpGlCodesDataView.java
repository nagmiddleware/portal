/**
 *
 */
package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Map;

public class ErpGlCodesDataView     extends AbstractDBDataView {
private static final Logger LOG = LoggerFactory.getLogger(ErpGlCodesDataView.class);


	public ErpGlCodesDataView() {
		super();
	}

    protected String buildDynamicSQLCriteria(Map<String,String> parameters) {


 
        return " where p_owner_oid in (?)";
    }

    protected int setDynamicSQLValues(PreparedStatement statement, int startVarIdx,
            Map<String,String> parameters)
            throws SQLException {
    	
    	int varCount = 0;
    	statement.setString(startVarIdx+varCount, getUserSession().getOwnerOrgOid());
        varCount++;
        return varCount;
    }

}
