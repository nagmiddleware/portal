/**
 *
 */
package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;
import java.util.List;
import com.ams.tradeportal.busobj.util.SecurityAccess;
import com.ams.tradeportal.busobj.webbean.CorporateOrganizationWebBean;
import com.ams.tradeportal.common.SQLParamFilter;
import com.ams.tradeportal.common.TradePortalConstants;
import com.ams.tradeportal.common.TransactionStatus;
import com.amsinc.ecsg.util.EncryptDecrypt;
import com.amsinc.ecsg.web.BeanManager;

/**

 *
 */
public class InstrumentsPendingTransactionsDataView     extends AbstractDBDataView {
private static final Logger LOG = LoggerFactory.getLogger(InstrumentsPendingTransactionsDataView.class);
    
	final static String ALL_WORK = "common.allWork";
    final static String MY_WORK = "common.myWork";
    final static String ALL_INSTRUMENTS = "common.all";
    private final List<Object> sqlParams = new ArrayList<>();
    
    /**
     * Constructor
     */
    public InstrumentsPendingTransactionsDataView() {
    	super();
    }
    
    protected String buildDynamicSQLCriteria(Map<String,String> parameters) {
    	StringBuilder sql = new StringBuilder();
        String selectedStatus = SQLParamFilter.filter(parameters.get("selectedStatus")); 
        String encryptedSelectedWorkflow = parameters.get("selectedWorkflow");
        
        boolean filterAllCashMgmtTrans = false;
 
        BeanManager beanMgr = this.getBeanManager();
        
        // Set the filterAllCashMgmtTrans flag.  This is used to filter out all the cash management transactions or all but International payments.
        // We do not filter the international payments if the corporate organization does not use transfer between accounts and domestic payments.
        CorporateOrganizationWebBean corpOrg  = beanMgr.createBean(CorporateOrganizationWebBean.class, "CorporateOrganization");
        corpOrg.getById(getUserSession().getOwnerOrgOid());
       

        if(corpOrg.getAttribute("allow_panel_auth_for_pymts").equals(TradePortalConstants.INDICATOR_YES)){
          boolean allowXferBtwnAccts = corpOrg.getAttribute("allow_xfer_btwn_accts_panel").equals(TradePortalConstants.INDICATOR_YES);   
          boolean allowDomesticPayments = corpOrg.getAttribute("allow_domestic_payments_panel").equals(TradePortalConstants.INDICATOR_YES);   
          filterAllCashMgmtTrans = (allowXferBtwnAccts || allowDomesticPayments) && SecurityAccess.canViewDomesticPaymentOrTransfer(getUserSession().getSecurityRights());
        } else {
          boolean allowXferBtwnAccts = corpOrg.getAttribute("allow_xfer_btwn_accts").equals(TradePortalConstants.INDICATOR_YES);
          boolean allowDomesticPayments = corpOrg.getAttribute("allow_domestic_payments").equals(TradePortalConstants.INDICATOR_YES);     
          filterAllCashMgmtTrans = (allowXferBtwnAccts || allowDomesticPayments) && SecurityAccess.canViewDomesticPaymentOrTransfer(getUserSession().getSecurityRights());
        }
       
        String confInd  = getConfidentialInd();
 	  
 	
        
        
        List<String> statuses = new ArrayList<>();
        int numStatuses = 0;
        
        //parameter check start
        if(selectedStatus == null || selectedStatus.equals(" "))selectedStatus="";
        
        if (TradePortalConstants.STATUS_READY.equals(selectedStatus) )
        {
           statuses.add(TransactionStatus.READY_TO_AUTHORIZE);
           statuses.add(TransactionStatus.AUTHORIZE_PENDING);
        }
        else if (TradePortalConstants.STATUS_PARTIAL.equals(selectedStatus) )
        {
           statuses.add(TransactionStatus.PARTIALLY_AUTHORIZED);
        }
        else if (TradePortalConstants.STATUS_AUTH_FAILED.equals(selectedStatus) )
        {
           statuses.add(TransactionStatus.AUTHORIZE_FAILED);
        }
        else if (TradePortalConstants.STATUS_STARTED.equals(selectedStatus) )
        {
           statuses.add(TransactionStatus.STARTED);
        }
        else if (TradePortalConstants.STATUS_REJECTED_BY_BANK.equals(selectedStatus) )
        {
           statuses.add(TransactionStatus.REJECTED_BY_BANK);
        }
        
        else if (TradePortalConstants.STATUS_READY_TO_CHECK.equals(selectedStatus) )
        {
           statuses.add(TransactionStatus.READY_TO_CHECK);
        }
        else if (TradePortalConstants.STATUS_REPAIR.equals(selectedStatus) )
        {
           statuses.add(TransactionStatus.REPAIR);
        }
        
        else // default is ALL
        {
           statuses.add(TransactionStatus.STARTED);
           statuses.add(TransactionStatus.AUTHORIZE_FAILED);
           statuses.add(TransactionStatus.AUTHORIZE_PENDING);
           statuses.add(TransactionStatus.PARTIALLY_AUTHORIZED);
           statuses.add(TransactionStatus.READY_TO_AUTHORIZE);
           statuses.add(TransactionStatus.REJECTED_BY_BANK);
         
           statuses.add(TransactionStatus.READY_TO_CHECK);
           statuses.add(TransactionStatus.REPAIR);
      
           selectedStatus = TradePortalConstants.STATUS_ALL;
        }
    
       
        // Build a where clause using the vector of statuses we just set up.
        StringBuilder statusStr = new StringBuilder("");
        numStatuses = statuses.size();
        for (int i=0; i<numStatuses; i++) {
        	statusStr.append(statuses.get(i));
            if (i < numStatuses - 1) {
            	statusStr.append(", ");
            }
        }
    
        sql.append(" and a.transaction_status in (");
        sql.append(prepareDynamicSqlStrForInClause(statusStr.toString(),sqlParams));
        sql.append(") ");
    
        if ( encryptedSelectedWorkflow!=null && encryptedSelectedWorkflow.length()>0 ) {
            String selectedWorkflow = EncryptDecrypt.decryptStringUsingTripleDes(
                encryptedSelectedWorkflow, getUserSession().getSecretKey());

            if (MY_WORK.equals(selectedWorkflow)) {
                sql.append("and a.a_assigned_to_user_oid = ?"); //userOid
                sqlParams.add(getUserSession().getUserOid());
            }
            else if (ALL_WORK.equals(selectedWorkflow)) {
               sql.append("and b.a_corp_org_oid in (");
               sql.append(" select organization_oid");
               sql.append(" from corporate_org");
               sql.append(" where activation_status = '");
               sql.append(TradePortalConstants.ACTIVE);
               sql.append("' start with organization_oid = ?"); //userOrgOid
               sql.append(" connect by prior organization_oid = p_parent_corp_org_oid)");
               sqlParams.add(getUserSession().getOwnerOrgOid());
            }
            else
            {
               sql.append("and b.a_corp_org_oid = ?"); //selectedWorkflow
               sqlParams.add(selectedWorkflow);
           	}
        }
        else {
            //selectedWorkflow is required
            sql.append("and 1=0");
        }
    
        // Add additional where clause to remove Cash Management transactions if necessary.
    
        if(filterAllCashMgmtTrans){
            sql.append(" and b.instrument_type_code not in('FTBA','FTDP','FTRQ','DDI'"); //SRUK011182777 - added 'DDI' transactions exclusion
        } 
        else {
            sql.append(" and b.instrument_type_code not in('FTBA','FTDP','DDI'");        //SRUK011182777 - added 'DDI' transactions exclusion
        }
    	sql.append(" ) ");
    	
    	if (TradePortalConstants.INDICATOR_NO.equals(confInd))
    	   {
    	      //IAZ IR-RDUK091546587 Use INSTRUMENTS table with Instrument History View for Conf Indicator Check
    	      String tableNameAlias = "tr";										
    	      sql.append(" and (").append(tableNameAlias).append(".confidential_indicator = 'N' OR ");
    	      sql.append(tableNameAlias).append(".confidential_indicator is null) ");
    	   }
    	return sql.toString();
    }
    
    protected int setDynamicSQLValues(PreparedStatement statement, int startVarIdx,
            Map<String,String> parameters)
            throws SQLException {
    	return setSqlVarsToPreparedStmt(statement, startVarIdx, sqlParams);
    }
}
