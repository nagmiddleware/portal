/**
 *
 */
package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;
import java.util.List;

import com.ams.tradeportal.common.SQLParamFilter;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.util.EncryptDecrypt;
import com.amsinc.ecsg.util.StringFunction;
/**
 *
 *
 *
 */
public class PayablesCreditNoteListDataView     extends AbstractDBDataView
{
private static final Logger LOG = LoggerFactory.getLogger(PayablesCreditNoteListDataView.class);
	private final List<Object> sqlParams = new ArrayList<>();
	/**
	 * Constructor
	 */
	public PayablesCreditNoteListDataView() {

		super();
	}

	   protected String buildDynamicSQLCriteria(Map<String,String> parameters) {
		   
		StringBuilder      whereClause     = new StringBuilder();
    	StringBuilder 	  countClause     = new StringBuilder();
    	String parentId = parameters.get("parentId");
    	if(StringFunction.isNotBlank(parentId)){
    		whereClause.append(buildDynamicSQLCriteriaChild(parameters));
    		countClause.append(buildDynamicSQLCriteriaChildCount(parameters));
    	}else{
    		whereClause.append(buildDynamicSQLCriteriaParent(parameters));
    		countClause.append(buildDynamicSQLCriteriaParentCount(parameters));
    	}
    	
    	super.fullSQL = whereClause.toString();
    	super.fullCountSQL = countClause.toString();

        return null;
    }

    private String buildDynamicSQLCriteriaParent(Map<String,String> parameters) {
    	StringBuilder      dynamicWhereClause     = new StringBuilder();
    	StringBuilder      where     = new StringBuilder();
    	String sort=SQLParamFilter.filter(parameters.get("sort"));
    	String invoiceStatusType = SQLParamFilter.filter(parameters.get("invoiceStatusType"));
    	
    	//MEer IR-35813 Update query to include Credit Note Status in the payables credit note list grid
    	String userOrgOid=getUserSession().getOwnerOrgOid();
    	String selectouter=" SELECT rowKey,upload_invoice_oid,CRInvoiceID,Ccy,TradingPartner,CHILDREN,Amount,AppliedAmount,AppliedStatus,ViewPDF,RemainingAmount,EndToEndId, Status,StatusCode, PanelStatus,DocName,Hash";
    	String select="SELECT DISTINCT o.upload_credit_note_oid AS rowKey,o.upload_credit_note_oid AS upload_invoice_oid, o.invoice_id AS CRInvoiceID,o.currency AS Ccy,o.end_to_end_id AS EndToEndId," +
    			"o.seller_name AS TradingPartner,'true' AS CHILDREN, NVL(o.amount,0) AS Amount,NVL(o.credit_note_applied_amount,0)*(-1) as AppliedAmount,"+
    			"(NVL(o.amount,0) -  NVL(o.credit_note_applied_amount,0))   as RemainingAmount, o.credit_note_applied_status as AppliedStatus, o.credit_note_status as Status,o.credit_note_status as StatusCode,"+
    			"decode(o.credit_note_status, 'PAUT', 'View Authorisations','FAUT', 'View Authorisations', null) as PanelStatus,"+
    			"d.IMAGE_ID as ViewPDF, d.DOC_NAME as DocName,d.HASH as Hash" ;
    				
    	String from=" FROM CREDIT_NOTES o left outer join Document_image d on o.upload_credit_note_oid = d.p_transaction_oid";
    	where.append(" WHERE  o.a_corp_org_oid = ? and (o.portal_originated_ind = ? or o.portal_approval_req_ind =? ) and  o.credit_note_status not in (?,?,?)");
    	sqlParams.add(userOrgOid);
    	sqlParams.add("Y");
    	sqlParams.add("Y");
    	sqlParams.add("PBB");
    	sqlParams.add("ASN");
    	sqlParams.add("DEC");
    	if (StringFunction.isNotBlank(invoiceStatusType) && !TradePortalConstants.STATUS_ALL.equals(invoiceStatusType)) {
    		where.append(" and o.credit_note_status = ?");//MEer IR-35813 search grid based on credit note status instead of applied status 
    		sqlParams.add(invoiceStatusType);
    		
    	}

    	dynamicWhereClause.append(selectouter).append(" from ( ").append(selectouter).append(" , rownum row_num from ( ").append(select).append(from).append(where);
    	//Added for sorting in Grid
    	if(StringFunction.isNotBlank(sort)){
    		if ( sort.startsWith("-")) {
    			sort = sort.substring(1);
    			sort = SQLParamFilter.filter(sort);
    			dynamicWhereClause.append(" order by ").append(sort).append(" desc ");
            } else {
            	dynamicWhereClause.append(" order by ").append(sort).append(" asc ");
            }
        }
    	dynamicWhereClause.append(" ) ) where row_num > ? and row_num <= ?");
    	return dynamicWhereClause.toString();


	}

    private String buildDynamicSQLCriteriaParentCount(Map<String,String> parameters) {
    	StringBuilder      dynamicWhereClause     = new StringBuilder();
    	StringBuilder      where     = new StringBuilder();
    	String invoiceStatusType = SQLParamFilter.filter(parameters.get("invoiceStatusType"));
    	String select=" SELECT count (DISTINCT o.upload_credit_note_oid) ";
    	String from=" FROM CREDIT_NOTES o";
    	where.append(" WHERE  o.a_corp_org_oid = ? and (o.portal_originated_ind = ? or o.portal_approval_req_ind =? ) and  o.credit_note_status not in (?,?,?)");
    	if (StringFunction.isNotBlank(invoiceStatusType) && !TradePortalConstants.STATUS_ALL.equals(invoiceStatusType)) {
    		where.append(" and o.credit_note_status = ?");
    	}
    	dynamicWhereClause.append(select).append(from).append(where);

    	return dynamicWhereClause.toString();
	}

    private String buildDynamicSQLCriteriaChild(Map<String,String> parameters) {
    	String parentId = EncryptDecrypt.decryptStringUsingTripleDes(parameters.get("parentId"), getUserSession().getSecretKey());
    	StringBuilder      dynamicWhereClause     = new StringBuilder();
    	
    	String select=" SELECT rowKey,upload_invoice_oid,CRInvoiceID,InvoiceSummaryID,Ccy,TradingPartner,CHILDREN,Amount,AppliedAmount,AppliedStatus,ViewPDF,RemainingAmount,EndToEndId,Status,StatusCode,PanelStatus, DocName,Hash FROM( SELECT i.upload_invoice_oid AS rowKey, i.upload_invoice_oid AS upload_invoice_oid,i.invoice_id AS CRInvoiceID,i.invoice_id AS InvoiceSummaryID, i.currency AS Ccy," +
    			"i.seller_name AS TradingPartner,'false' AS CHILDREN, rownum as row_num, case when (i.payment_amount is null or i.payment_amount=0) then i.amount else i.payment_amount end as Amount,i.end_to_end_id as EndToEndId,"+
    	"c.credit_note_applied_amount as  AppliedAmount,'' as AppliedStatus, i.invoice_status as Status, '' as StatusCode, '' as PanelStatus, '' as ViewPDF, '' as DocName,'' as Hash, "+
			" (CASE WHEN i.payment_amount is NULL THEN ( NVL(i.amount,0) +  NVL(i.total_credit_note_amount,0)) "+
			" ELSE (NVL(i.payment_amount,0) + NVL(i.total_credit_note_amount,0)) END) as RemainingAmount";
    	String from=" FROM invoices_summary_data i,CREDIT_NOTES o,INVOICES_CREDIT_NOTES_RELATION c";
    	String where=" WHERE c.a_upload_credit_note_oid = o.upload_credit_note_oid and c.a_upload_invoice_oid = i.upload_invoice_oid ";
    	dynamicWhereClause.append(select).append(from).append(where);
    	if(StringFunction.isNotBlank(parentId)){
    	dynamicWhereClause.append(" AND o.upload_credit_note_oid =?");
    	sqlParams.add(parentId);
    	}
    	
    	dynamicWhereClause.append(" UNION ");
    	
    	String select2="  SELECT i.upload_invoice_oid AS rowKey, i.upload_invoice_oid AS upload_invoice_oid,i.invoice_id AS CRInvoiceID,i.invoice_id AS InvoiceSummaryID, i.currency AS Ccy," +
		"i.seller_name AS TradingPartner,'false' AS CHILDREN, rownum as row_num, case when (i.payment_amount is null or i.payment_amount=0) then i.amount else i.payment_amount end as Amount,i.end_to_end_id as EndToEndId,"+
		"0.00 as  AppliedAmount,'' as AppliedStatus, i.invoice_status as Status, '' as StatusCode, '' as PanelStatus,'' as ViewPDF, '' as DocName,'' as Hash, "+
		" (CASE WHEN i.payment_amount is NULL THEN ( NVL(i.amount,0) +  NVL(i.total_credit_note_amount,0)) "+
		" ELSE (NVL(i.payment_amount,0) + NVL(i.total_credit_note_amount,0)) END) as RemainingAmount";
    	String from2=" FROM invoices_summary_data i,CREDIT_NOTES o";
    	String where2=" WHERE i.end_to_end_id = o.end_to_end_id";
    	dynamicWhereClause.append(select2).append(from2).append(where2);
    	if(StringFunction.isNotBlank(parentId)){
    		dynamicWhereClause.append(" AND o.upload_credit_note_oid =?");
    		sqlParams.add(parentId);
    	}
    	dynamicWhereClause.append(" )WHERE row_num > ? AND row_num  <= ?");

    	return dynamicWhereClause.toString();


	}

    private String buildDynamicSQLCriteriaChildCount(Map<String,String> parameters) {
    	String parentId = EncryptDecrypt.decryptStringUsingTripleDes(parameters.get("parentId"), getUserSession().getSecretKey());
    	StringBuilder      dynamicWhereClause     = new StringBuilder();
    	String select=" SELECT count(invoiceOid)  FROM (SELECT i.upload_invoice_oid AS invoiceOid  ";
    	String from=" FROM invoices_summary_data i,CREDIT_NOTES o,INVOICES_CREDIT_NOTES_RELATION c";
    	String where=" WHERE c.a_upload_credit_note_oid = o.upload_credit_note_oid and c.a_upload_invoice_oid = i.upload_invoice_oid ";
    	dynamicWhereClause.append(select).append(from).append(where);
    	if(StringFunction.isNotBlank(parentId)){
    		dynamicWhereClause.append(" AND o.upload_credit_note_oid  =?");
    	}
    	dynamicWhereClause.append(" UNION ");
    	
    	String select2=" SELECT i.upload_invoice_oid AS invoiceOid ";
    	String from2=" FROM invoices_summary_data i,CREDIT_NOTES o";
    	String where2=" WHERE i.end_to_end_id = o.end_to_end_id ";
    	dynamicWhereClause.append(select2).append(from2).append(where2);
    	if(StringFunction.isNotBlank(parentId)){
    		dynamicWhereClause.append(" AND o.upload_credit_note_oid  =?");
    	}
    	dynamicWhereClause.append(" ) ");
    	return dynamicWhereClause.toString();


	}

	protected int setDynamicSQLValues(PreparedStatement statement, int startVarIdx,
            Map<String,String> parameters)
            throws SQLException {
		return setSqlVarsToPreparedStmt(statement, startVarIdx, sqlParams);
    }
	
	protected boolean ignorePassedInParam(Map<String,String> parameters){
    	return StringFunction.isNotBlank(parameters.get("parentId"));
		
	}
	

}
