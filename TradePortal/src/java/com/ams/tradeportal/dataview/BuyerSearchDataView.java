/**
 *
 */
package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Map;

import com.ams.tradeportal.common.SQLParamFilter;

/**
 *
 *
 */
public class BuyerSearchDataView extends AbstractDBDataView {
private static final Logger LOG = LoggerFactory.getLogger(BuyerSearchDataView.class);

    /**
     * Constructor
     */
    public BuyerSearchDataView() {
        super();
    }

    @Override
    protected String buildDynamicSQLCriteria(Map<String, String> parameters) {

        StringBuilder sql = new StringBuilder();

        String parentOrgID = getUserSession().getOwnerOrgOid();
        String filterText1 = SQLParamFilter.filter(parameters.get("filterText1"));
        String filterText2 = SQLParamFilter.filter(parameters.get("filterText2"));

        sql.append(" where ar_matching_rule.p_corp_org_oid in (").append(parentOrgID).append(")");

        if (filterText1 != null && !filterText1.equals("")) {
            String sqlFilter = SQLParamFilter.filter(filterText1);
            sql.append(" and ( upper(ar_matching_rule.buyer_name) like '").append(sqlFilter).append("%')");

        }
        if (filterText2 != null && !filterText2.equals("")) {
            String sqlFilter = SQLParamFilter.filter(filterText2);
            sql.append(" and ( upper(ar_matching_rule.buyer_id) like '").append(sqlFilter).append("%')");

        }

        return sql.toString();
    }

    @Override
    protected int setDynamicSQLValues(PreparedStatement statement, int startVarIdx,
            Map<String, String> parameters)
            throws SQLException {
        int varCount = 0;

        return varCount;
    }
}
