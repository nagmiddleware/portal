/**
 *
 */
package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;
import java.util.List;

import com.ams.tradeportal.common.TermsPartyType;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.util.StringFunction;

/**
 *
 *
 */
public class PartySearchNewDataView extends AbstractDBDataView {
private static final Logger LOG = LoggerFactory.getLogger(PartySearchNewDataView.class);

    private final List<Object> sqlParams = new ArrayList<>();

    /**
     * Constructor
     */
    public PartySearchNewDataView() {
        super();
    }

    @Override
    protected String buildDynamicSQLCriteria(Map<String, String> parameters) {
        StringBuffer sql = new StringBuffer();

        String partyType = parameters.get("partyType");

        boolean userFlag = Boolean.parseBoolean(parameters.get("userFlag"));
        String tempOwnerOrgOid = parameters.get("tempOwnerOrgOid");

        String clientBankID = getUserSession().getClientBankOid();
        String bogID = getUserSession().getBogOid();
        String globalID = getUserSession().getGlobalOrgOid();
        String parentOrgID = getUserSession().getOwnerOrgOid();
        String ownershipLevel = getUserSession().getOwnershipLevel();

        // Nar IR-T36000016045 rel 8.1.0.5 Begin
        boolean includeSubAccessUserOrg = getUserSession().showOrgDataUnderSubAccess();
        String subAccessUserOrgID = null;
        if (includeSubAccessUserOrg) {
            subAccessUserOrgID = getUserSession().getSavedUserSession().getOwnerOrgOid();
        }
        // Nar IR-T36000016045 rel 8.1.0.5 End
        String filterText = parameters.get("filterText");
        String unicodeIndicator = parameters.get("unicodeIndicator");
        String partyTypeCode = "";
        if (partyType.equals(TermsPartyType.INSURING_PARTY)) {

            partyTypeCode = parameters.get("partyTypeCode");
            sql = new StringBuffer("where party_type_code in (");
            String[] params = partyTypeCode.split(",");
            if (params != null) {
                for (int x = 0; x < params.length; x++) {
                    sql.append("?");
                    sqlParams.add(params[x].replaceAll("'", ""));
                    if (x < params.length - 1) { //Rel9.2 IR#T36000034904
                        sql.append(",");
                    }
                }
            }
            sql.append(")");

            sql.append("and p_owner_org_oid in (");
        } else {
            partyTypeCode = parameters.get("partyTypeCode");
            sql = new StringBuffer("where party_type_code = ? ");
            sqlParams.add(partyTypeCode);
            sql.append("and p_owner_org_oid in (");
        }

        if (userFlag) {
            sql.append("?,");
            sqlParams.add(tempOwnerOrgOid);
        }

        sql.append("?,?");
        sqlParams.add(parentOrgID);
        sqlParams.add(globalID);

        if (ownershipLevel.equals(TradePortalConstants.OWNER_BOG) || ownershipLevel.equals(TradePortalConstants.OWNER_CORPORATE)) {
            sql.append(",?");
            sqlParams.add(clientBankID);
            if (ownershipLevel.equals(TradePortalConstants.OWNER_CORPORATE)) {
                sql.append(",?");
                sqlParams.add(bogID);
            }
        }

        // Also include party from the user's actual organization if using subsidiary access
        if (includeSubAccessUserOrg) {
            sql.append(",?"); // Nar IR-T36000016045 Rel 8.1.0.5
            sqlParams.add(subAccessUserOrgID);
        }

        sql.append(")");

        if (filterText != null && !filterText.equals("")) {
            sql.append("and upper(name) like unistr(?) ");
            sqlParams.add(StringFunction.toUnistr(filterText) + "%");
        }

        if (TradePortalConstants.INDICATOR_NO.equals(unicodeIndicator) && unicodeIndicator != null) {
            sql.append(" and unicode_indicator = 'N'");
        }

        return sql.toString();
    }

    @Override
    protected int setDynamicSQLValues(PreparedStatement statement, int startVarIdx,
            Map<String, String> parameters)
            throws SQLException {

        return setSqlVarsToPreparedStmt(statement, startVarIdx, sqlParams);
    }
}
