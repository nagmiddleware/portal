/**
 *
 */
package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;
import java.sql.PreparedStatement;
import java.util.Map;
import java.sql.SQLException;

import com.ams.tradeportal.common.TradePortalConstants;
import com.ams.tradeportal.common.Debug;
import com.ams.tradeportal.common.NumberValidator;
import com.ams.tradeportal.common.SQLParamFilter;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.InvalidAttributeValueException;
import com.amsinc.ecsg.frame.MediatorServices;
import com.amsinc.ecsg.util.StringFunction;

/**
 *
 *
 */
public class InvoicesDataView     extends AbstractDBDataView
{
private static final Logger LOG = LoggerFactory.getLogger(InvoicesDataView.class);
	/**
	 * Constructor
	 */
	public final static String INVOICE_SEARCH_STATUS_CLOSED    = "CLOSED";
	public InvoicesDataView() {
		super();
	}

    protected String buildDynamicSQLCriteria(Map<String,String> parameters) {

    	StringBuilder dynamicWhereClause     = new StringBuilder();
        String selectedStatus = SQLParamFilter.filter(parameters.get("selectedStatus"));
        String userOrgOid = getUserSession().getOwnerOrgOid();
        MediatorServices medService = new MediatorServices();
        /*Params for search*/
        String invoiceID = SQLParamFilter.filter(parameters.get("invoiceID"));
        String finance = SQLParamFilter.filter(parameters.get("finance"));
        String dispute = SQLParamFilter.filter(parameters.get("dispute"));
        String dateType = SQLParamFilter.filter(parameters.get("dateType"));
        String fromDate = SQLParamFilter.filter(parameters.get("fromDate"));
        String toDate = SQLParamFilter.filter(parameters.get("toDate"));
        String currency = SQLParamFilter.filter(parameters.get("currency"));
        String amountType = SQLParamFilter.filter(parameters.get("amountType"));

        String amountFrom = parameters.get("amountFrom");
        String amountTo = parameters.get("amountTo");
        String partyFilterText = SQLParamFilter.filter(parameters.get("partyFilterText"));
        String poNumber = SQLParamFilter.filter(parameters.get("poNumber"));


        String datePattern = null;
        try{
        	if(null != parameters.get("dPattern")){
        		datePattern = java.net.URLDecoder.decode(parameters.get("dPattern"), "UTF-8");
        	}else{
        		datePattern = "";
        	}
        }catch (UnsupportedEncodingException e) {
			LOG.debug("Exceptiom in DateFormat Decoding : "+e);
		}
        dynamicWhereClause.append(" and ");
        if (!TradePortalConstants.STATUS_ALL.equals(selectedStatus))
        {
        	if (TradePortalConstants.INVOICE_SEARCH_STATUS_OPEN.equals(selectedStatus)) {
        		dynamicWhereClause.append(" i.invoice_status not in (");
        		dynamicWhereClause.append("'CLO','CFN'");
        		dynamicWhereClause.append(") and ");
        	}

        }

           dynamicWhereClause.append(" i.a_corp_org_oid = ");
           dynamicWhereClause.append(userOrgOid);
           
           
        // SHR T36000007952- Search by Dispute
           if (StringFunction.isNotBlank(dispute) && TradePortalConstants.INDICATOR_YES.equals(dispute))
           {
        	  dynamicWhereClause.append(" and i.disputed_indicator = '"+dispute+"'");
           }

        // Search by Finance

           if (finance != null && !finance.equals("")) {
             if (TradePortalConstants.INDICATOR_YES.equals(finance)) {
              dynamicWhereClause.append(" and ");
          //SHR -use just FIN    dynamicWhereClause.append(" finance_status in ('FIN','AFF') ");// PPRAKASH IR ALUJ031649735
              dynamicWhereClause.append(" finance_status in ('FIN') ");
             }

           }

        // Search by the invoice ID
           //SHR T36000007952- modify
           if (StringFunction.isNotBlank(invoiceID)) {
        	 dynamicWhereClause.append(" and ");
        	 dynamicWhereClause.append(" upper(invoice_reference_id) like '%");
             dynamicWhereClause.append(SQLParamFilter.filter(invoiceID.toUpperCase()));
             dynamicWhereClause.append("%'");
      
         }

           // Search by date type
           if (dateType!=null && !dateType.equals("")){
        	   String expandedDateType = "";
        	   if (dateType.equals("DDT")){
        	       expandedDateType = "invoice_due_date";
        	     }
        	     else if (dateType.equals("IDT")){
        	    	 expandedDateType = "invoice_issue_datetime";
        	     }//Rel9.0 IR#T36000029821 - add a condition for payment date
        	     else if (dateType.equals("PDT")){
        	    	 expandedDateType = "invoice_payment_date";
        	     }        	   
        	     else {
        	       expandedDateType = "";
        	     }

               if (fromDate!=null && !fromDate.equals("")) {
            dynamicWhereClause.append(" and ");
            dynamicWhereClause.append(expandedDateType + " >= TO_DATE('");
         	dynamicWhereClause.append(fromDate);
         	dynamicWhereClause.append("','"+datePattern+"')");
               }

         	if (toDate!=null && !toDate.equals("")) {
         	  dynamicWhereClause.append(" and ");
         	  dynamicWhereClause.append(expandedDateType + " <= TO_DATE('");
         	  dynamicWhereClause.append(toDate);
         	  dynamicWhereClause.append("','"+datePattern+"')");
         	}
           }

           // Search by currency
           if (currency != null && !currency.equals("")) {

        	   dynamicWhereClause.append(" and ");
        	   dynamicWhereClause.append(" currency_code='" + currency + "'");
           }

           // Search by Amount
           if(amountType!= null && !StringFunction.isBlank(amountType)) {
        	   String expandedAmountType = "";
        	   if(amountType.equals("INA")){
        		      expandedAmountType = "invoice_total_amount";
        		    }
        		    else if (amountType.equals("DSA")){
        		      expandedAmountType = "invoice_discount_amount";
        		    }
        		    else if (amountType.equals("CNA")){
        		      expandedAmountType = "invoice_credit_note_amount";
        		    }
        		    else if (amountType.equals("FNA")){
        		      expandedAmountType = "finance_amount";
        		    }
        		    else {
        		      expandedAmountType = "";
        		    }
        	   //SHR T36000007952 updates start
             if (StringFunction.isNotBlank(amountFrom)) {
               amountFrom = amountFrom.trim();
               try {
                 String amount =NumberValidator.getNonInternationalizedValue(amountFrom,getResourceManager().getResourceLocale());
                 if (StringFunction.isNotBlank(amountFrom)) {
                	 dynamicWhereClause.append(" and ");
                	 dynamicWhereClause.append(expandedAmountType + " >= ");
                	 dynamicWhereClause.append(amount);

                 }
               } catch (InvalidAttributeValueException e) {
            	   try {
       				medService.getErrorManager().issueError(
       						TradePortalConstants.ERR_CAT_1,
       						TradePortalConstants.INVALID_CURRENCY_FORMAT,
       						amountFrom);
       			} catch (AmsException e1) {
       				e1.printStackTrace();
       			}
               }
             }

             if (StringFunction.isNotBlank(amountTo)) {
               amountTo = amountTo.trim();
               try {
                 String amount = NumberValidator.getNonInternationalizedValue(amountTo,getResourceManager().getResourceLocale());
         		if (StringFunction.isNotBlank(amountTo)) {
         			dynamicWhereClause.append(" and ");
         			dynamicWhereClause.append(expandedAmountType + " <= ");
         			dynamicWhereClause.append(amount);

         		}
               } catch (InvalidAttributeValueException e) {
            	   try {
       				medService.getErrorManager().issueError(
       						TradePortalConstants.ERR_CAT_1,
       						TradePortalConstants.INVALID_CURRENCY_FORMAT,
       						amountTo);
       			} catch (AmsException e1) {
       				e1.printStackTrace();
       			}
               }
             }
           //SHR T36000007952 updates end
           }

           if ( partyFilterText != null && !partyFilterText.equals("")) {
             partyFilterText = partyFilterText.trim();
             dynamicWhereClause.append(" and ");
             //SHR T36000007952 - use Upper() for the buyer id
             dynamicWhereClause.append(" upper(buyer_party_identifier) like '%");
             dynamicWhereClause.append(SQLParamFilter.filter(partyFilterText.toUpperCase()));
             dynamicWhereClause.append("%'");
           }

           if (poNumber != null && !poNumber.equals("")) {
        	 dynamicWhereClause.append(" and ");
        	 dynamicWhereClause.append(" i.invoice_oid in (select p_invoice_oid from invoice_goods where upper(po_reference_id) like '%");
             dynamicWhereClause.append(poNumber.toUpperCase());
             dynamicWhereClause.append("%')");
           }


           return dynamicWhereClause.toString();
    	}

    protected int setDynamicSQLValues(PreparedStatement statement, int startVarIdx,
            Map<String,String> parameters)
            throws SQLException {
        int varCount = 0;

        return varCount;
    }
}
