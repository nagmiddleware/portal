/**
 *
 */
package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Map;
import java.util.List;
import java.util.ArrayList;

import com.ams.tradeportal.common.TradePortalConstants;
import com.ams.tradeportal.common.TransactionType;

/**
 *
 *
 */
public class RemovePODataView     extends AbstractDBDataView
{
private static final Logger LOG = LoggerFactory.getLogger(RemovePODataView.class);
	private final List<Object> sqlParams = new ArrayList<>();
	/**
	 * Constructor
	 */
	public RemovePODataView() {
		super();
	}

    protected String buildDynamicSQLCriteria(Map<String,String> parameters) {

    	StringBuilder sql = new StringBuilder();

        String userOrgOid = parameters.get("userOrgOid");
        String transactionOid = parameters.get("transactionOid");
        String shipmentOid = parameters.get("shipmentOid");
        String transactionType = parameters.get("transactionType");
        String uploadDefinitionOid = parameters.get("uploadDefinitionOid");
        String sourceType = parameters.get("sourceType");
        
        //parameter check start
        if(userOrgOid == null)userOrgOid="";
        if(transactionOid == null)transactionOid="";
        if(uploadDefinitionOid == null)uploadDefinitionOid="";
        if(transactionType == null) transactionType = "";
        if(sourceType == null) sourceType = "";
        
        //parameter check end

        
        sql.append("where a_owner_org_oid = ?  ");
       	sql.append(" and a_assigned_to_trans_oid = ? ");
       	sqlParams.add(userOrgOid);
    	sqlParams.add(transactionOid);

       	if(!"".equals(sourceType)){
       		sql.append(" and source_type = ? ");
       		sqlParams.add(sourceType);
       	}
       	
       	sql.append(" and p_shipment_oid = ? ");
    	sqlParams.add(shipmentOid);
       	
       	if (transactionType.equals(TransactionType.AMEND))
        {
           sql.append(" and auto_added_to_amend_ind = ? ");
       	sqlParams.add(TradePortalConstants.INDICATOR_NO);
        }
        
  
        sql.append(" group by po_num, ben_name, currency, a_owner_org_oid, a_source_upload_definition_oid ");        
     
       
    	return sql.toString();
    }

    protected int setDynamicSQLValues(PreparedStatement statement, int startVarIdx,
            Map<String,String> parameters)
            throws SQLException {

    	return setSqlVarsToPreparedStmt(statement, startVarIdx, sqlParams);
    }
}
