/**
 *
 */
package com.ams.tradeportal.dataview;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.common.SQLParamFilter;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.util.EncryptDecrypt;
import com.amsinc.ecsg.util.StringFunction;

/**
 *
 *
 */
public class MailMessagesDataView     extends AbstractDBDataView
{
private static final Logger LOG = LoggerFactory.getLogger(MailMessagesDataView.class);

	private final List<Object> sqlParams = new ArrayList<>();
	/**
	 * Constructor
	 */
	public MailMessagesDataView() {
		super();
	}

	//cquinton 4/9/2013 Rel PR ir#15704 use resource keys instead of localized values
   public final static String MY_MAIL_AND_UNASSIGNED = "Mail.MeAndUnassigned";
   public final static String MY_MAIL_ONLY           = "Mail.Me";
   public final static String ALL_MAIL               = "Mail.AllMail";
	public final static String ALL_MAIL_MESS = "Mail.AllMailMESS";

    protected String buildDynamicSQLCriteria(Map<String,String> parameters) {
        //explicitly return just the number of records specified in
        // the showCount url parameter
        //assume it exists, we do some defaulting below if there are problems

        StringBuilder sql = new StringBuilder();
        StringBuilder sql1 ;


        String userOid = getUserSession().getUserOid();
        String userOrgOid = getUserSession().getOwnerOrgOid();
        String currentMailOption = parameters.get("currentMailOption");
        String currentFolder = SQLParamFilter.filter(parameters.get("currentFolder"));
        
        //cquinton 4/9/2013 Rel PR ir#15704 fix issue with mail option defaulting incorrectly
        // when not given need to default to no data
        
        // The following code constructs the sql to use for the dynamic where clause listview
        // component and each of the folder totals adjacent to the folder images; these totals
        // (as well as the listview) depend on which mail option is currently selected in the
        // dropwdown list.
        if (currentMailOption != null && currentMailOption.length()>0) {
            
            currentMailOption = EncryptDecrypt.decryptStringUsingTripleDes(
                currentMailOption, getUserSession().getSecretKey());

            if (MY_MAIL_ONLY.equals(currentMailOption)){
                sql.append("and a.a_assigned_to_user_oid = ? ");
                sqlParams.add(userOid);
            }
            else if (MY_MAIL_AND_UNASSIGNED.equals(currentMailOption)){
                sql.append("and (a.a_assigned_to_user_oid = ? or (a.a_assigned_to_corp_org_oid = ?  and a.a_assigned_to_user_oid is null))");
                sqlParams.add(userOid);
                sqlParams.add(userOrgOid);
            }
            else if (ALL_MAIL.equals(currentMailOption)){
            	sql1 = new StringBuilder();
                sql1.append(" select organization_oid");
                sql1.append(" from corporate_org");
                sql1.append(" where activation_status = ? start with organization_oid = ? ");
                //jgadela 08/28/2014 R91 IR T36000031638 - Fixed the ALL MAIL option issue
                sqlParams.add(TradePortalConstants.ACTIVE);
                sqlParams.add(userOrgOid);
                sql1.append(" connect by prior organization_oid = p_parent_corp_org_oid)");
                
                sql.append("and a.a_assigned_to_corp_org_oid in (");
                sql.append(sql1.toString());
            }
            else {
            	sql.append("and a.a_assigned_to_corp_org_oid = ? ");
                sqlParams.add(currentMailOption);
            }
        }
        else {
            sql.append("and 1=0");
        }
         
         if( currentFolder != null && !currentFolder.equals("")){
        	 if (currentFolder.equals(TradePortalConstants.INBOX_FOLDER)){
        		 sql.append(" and message_status in ('");
        		 sql.append(TradePortalConstants.REC);
        		 sql.append("', '");
        		 sql.append(TradePortalConstants.REC_ASSIGNED);
        		 sql.append("', '");
        		 sql.append(TradePortalConstants.SENT_ASSIGNED);
        		 sql.append("') ");
        	 }else if (currentFolder.equals(TradePortalConstants.DRAFTS_FOLDER)){
        		 sql.append(" and message_status = '");
        		 sql.append(TradePortalConstants.DRAFT);
        		 sql.append("' ");
        	 }else if (currentFolder.equals(TradePortalConstants.SENT_TO_BANK_FOLDER)){
        		 sql.append(" and message_status = '");
        		 sql.append(TradePortalConstants.SENT_TO_BANK);
        		 sql.append("' ");
        	 }
        }
       //Added by Jaya Mamidala - CR49930 -  Start
         String unreadFlag = SQLParamFilter.filter(parameters.get("UnreadFlag"));
        
		if (StringFunction.isNotBlank(unreadFlag) && !ALL_MAIL_MESS.equals(unreadFlag))
         {
         	if("N".equals(unreadFlag))
         	{
         	sql.append(" and a.unread_flag = ?");
         	}
         	else 
         	{
         	sql.append(" and (a.unread_flag is null or a.unread_flag <> ?)");
         	}
             sqlParams.add("N");
         }
         /*CR 913 - Rel 9.0 
          *Filter mail messages for funding_amount and funding_date is NULL*/
         sql.append(" and a.funding_amount is NULL");
         sql.append(" and a.funding_date is NULL ");
      
        return sql.toString();
    }

    protected int setDynamicSQLValues(PreparedStatement statement, int startVarIdx,
            Map<String,String> parameters)
            throws SQLException {

    	return setSqlVarsToPreparedStmt(statement, startVarIdx, sqlParams);
    }
}
