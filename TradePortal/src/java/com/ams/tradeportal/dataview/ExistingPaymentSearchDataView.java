/**
 *
 */
package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.*;

import com.ams.tradeportal.busobj.webbean.CorporateOrganizationWebBean;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.EncryptDecrypt;
import com.amsinc.ecsg.util.StringFunction;

/**
 *
 *
 */
public class ExistingPaymentSearchDataView     extends AbstractDBDataView  {
private static final Logger LOG = LoggerFactory.getLogger(ExistingPaymentSearchDataView.class);
	private final List<Object> sqlParams = new ArrayList<>();
	/**
	 * Constructor
	 */
	public ExistingPaymentSearchDataView() {
		super();
	}

    protected String buildDynamicSQLCriteria(Map<String,String> parameters) {

        StringBuilder sql = new StringBuilder();


        String confInd  = getConfidentialInd();
        String userOrgOid = parameters.get("userOrgOid");
        String status = parameters.get("status");
        String instrumentType = parameters.get("instrumentType");
        String currency = parameters.get("currency");
        String amountFrom = parameters.get("amountFrom");
        String amountTo = parameters.get("amountTo");
        String otherParty = parameters.get("otherParty");
        String instrumentId = parameters.get("instrumentId");
        String refNum = parameters.get("refNum");
        String searchType = parameters.get("searchType");
        /* 	KMehta IR-T36000039686 Rel 9400 on 11-August-2015 Change - Begin */	
		String instTypeCond = new String(); 
		/* 	KMehta IR-T36000039686 Rel 9400 on 11-August-2015 Change - End */
		// R 92 IR 36415 
		
		if(StringFunction.isNotBlank(userOrgOid)){
			userOrgOid= EncryptDecrypt.decryptStringUsingTripleDes(userOrgOid, getUserSession().getSecretKey());
		}
		
		CorporateOrganizationWebBean corpOrg  = getBeanManager().createBean(CorporateOrganizationWebBean.class, "CorporateOrganization");
		corpOrg.getById(userOrgOid);
  	    boolean sepFlag = false;
		if(TradePortalConstants.INDICATOR_YES.equals(corpOrg.getAttribute("allow_xfer_btwn_accts"))){
				instTypeCond= "FTBA";
				sepFlag = true;
		}
		if(TradePortalConstants.INDICATOR_YES.equals(corpOrg.getAttribute("allow_domestic_payments"))){
			   if(sepFlag){
				   instTypeCond+=",";
			   }
			   instTypeCond += "FTDP";
			   sepFlag = true;
		}
		if(TradePortalConstants.INDICATOR_YES.equals(corpOrg.getAttribute("allow_funds_transfer"))){
			   if(sepFlag){
				   instTypeCond+=",";
			   }
			   instTypeCond += "FTRQ";
		}

        //Added required status in 'All' condition and re-formated the if-else cases
        if("All".equals(status)){
        	status = "'ACT','PND','EXP','CAN','CLO','DEA','LIQ','LQB','LQC','RPI','PRP','FUR'";
        }
        if("Active".equals(status)){
        	status = "'ACT','PND','EXP'";
        }
        if("Inactive".equals(status)){
        	status = "'CAN','CLO','DEA','LIQ','LQB','LQC','RPI','PRP','FUR'";
        }
     
        String userOrgPlaceHolder = "";
       	if(userOrgOid != null){
       		//userOrgOid= EncryptDecrypt.decryptStringUsingTripleDes(userOrgOid, getUserSession().getSecretKey());
       		String  ALL_ORGANIZATIONS = getResourceManager().getText("AuthorizedTransactions.AllOrganizations",TradePortalConstants.TEXT_BUNDLE);
	     	 if (ALL_ORGANIZATIONS.equalsIgnoreCase(userOrgOid)) {
	        	 StringBuilder sqlQuery = new StringBuilder();
	         	   sqlQuery.append("select organization_oid, name");
	         	   sqlQuery.append(" from corporate_org");
	         	   sqlQuery.append(" where activation_status = ? start with organization_oid = ? ");
	         	   sqlQuery.append(" connect by prior organization_oid = p_parent_corp_org_oid");
	         	   sqlQuery.append(" order by ");
	         	   sqlQuery.append(getResourceManager().localizeOrderBy("name"));
	         	   StringBuilder orgList = new StringBuilder();
	         	   try{
	         		DocumentHandler hierarchyDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery.toString(), false, TradePortalConstants.ACTIVE, 

getOwnerOrgOid());
	
	         		   List<DocumentHandler> orgListDoc = hierarchyDoc.getFragmentsList("/ResultSetRecord");
	         		   int totalOrganizations = orgListDoc.size();
	
	         		   DocumentHandler orgDoc = null;
	         		   for (int i = 0; i < totalOrganizations; i++) {
	         			   orgDoc = orgListDoc.get(i);
	         			   orgList.append("?");
	         			   sqlParams.add(orgDoc.getAttribute("ORGANIZATION_OID"));
	         			   if (i < (totalOrganizations - 1) ) {
	         				   orgList.append(", ");
	         			   }
	         		   }
	         	   }catch(AmsException e){
	         		   e.printStackTrace();
	         	   }
	         	  userOrgPlaceHolder = orgList.toString();
	     	 }else{
	     		userOrgPlaceHolder = "?";
	     		sqlParams.add(userOrgOid);
	     	 }
       	}
        else {
            //selectedOrgOid is required. if not found return nothing
            sql.append(" and 1=0");
        }        
      

       	if (StringFunction.isNotBlank(userOrgOid)){
        	sql.append(" and i.a_corp_org_oid in (" + userOrgPlaceHolder+ ") ");
       	}

		//Srinivasu_D IR#T36000036415 Rel9.2 01/31/2015 - Start - Handling TBA/FTBA/FTDP
		if(StringFunction.isNotBlank(instTypeCond)){
			 sql.append(" and i.instrument_type_code in ("+prepareDynamicSqlStrForInClause(instTypeCond,sqlParams)+")");
		}
		//Srinivasu_D IR#T36000036415 Rel9.2 01/31/2015 - End

        if (StringFunction.isNotBlank(status)){
       		sql.append(" and i.instrument_status in ("+prepareDynamicSqlStrForInClause(status.toUpperCase(),sqlParams)+") ");
        }
        if(TradePortalConstants.INDICATOR_NO.equals(confInd)){
			sql.append(" and ( i.confidential_indicator = 'N' OR  i.confidential_indicator is null) ");
        }

        if ("Advanced".equals(searchType)){
        	if (StringFunction.isNotBlank(instrumentType)){
            	sql.append(" and i.instrument_type_code = ? ");
            	sqlParams.add(instrumentType);
        	}

        	if (StringFunction.isNotBlank(otherParty)){
        		sql.append(" and upper(p.name) like ? ");
        		sqlParams.add("%"+otherParty.toUpperCase()+"%");
        	}

        	if (StringFunction.isNotBlank(currency)){
        		sql.append(" and o.copy_of_currency_code = ? ");
        		sqlParams.add(currency);
        	}

        	if (StringFunction.isNotBlank(amountFrom)){
        		sql.append(" and i.copy_of_instrument_amount >= ? ");
        		sqlParams.add(amountFrom);
        	}

        	if (StringFunction.isNotBlank(amountTo)){
        		sql.append(" and i.copy_of_instrument_amount <= ? ");
        		sqlParams.add(amountTo);
        	}
        } else if ("Basic".equals(searchType)){
        	if (StringFunction.isNotBlank(instrumentId)){
        		sql.append(" and upper(i.complete_instrument_id) like ? ");
        		sqlParams.add("%"+instrumentId.toUpperCase()+"%");
        	}

        	if (StringFunction.isNotBlank(refNum)){
        		sql.append(" and upper(i.copy_of_ref_num) like ? ");
        		sqlParams.add("%"+refNum.toUpperCase()+"%");
        	}

        	if (StringFunction.isNotBlank(instrumentType)){
        		sql.append(" and i.instrument_type_code = ? ");
        		sqlParams.add(instrumentType);
        	}
        }

    	
    	return sql.toString();
    }

    protected int setDynamicSQLValues(PreparedStatement statement, int startVarIdx,
            Map<String,String> parameters)       throws SQLException {
    	
    	return setSqlVarsToPreparedStmt(statement, startVarIdx, sqlParams);
    }
}
