/**
 *
 */
package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Map;


/**
 *
 *
 */
public class PanelGroupsDataView    extends AbstractDBDataView
{
private static final Logger LOG = LoggerFactory.getLogger(PanelGroupsDataView.class);
	/**
	 * Constructor
	 */
	public PanelGroupsDataView() {
		super();
	}

    protected String buildDynamicSQLCriteria(Map<String,String> parameters) {

        // W Zhu 10/24/2012 T36000006807 #333 get parameters internally to avoid parameter tampering
        String where ="WHERE pg.p_corp_org_oid IN (select t.* from ( select CONNECT_BY_ROOT organization_oid as parent_oid from corporate_org where organization_oid = ? "
        + " connect by prior   organization_oid = p_parent_corp_org_oid and INHERIT_PANEL_AUTH_IND = 'Y' order by level) t where rownum>0 ) AND corg.ORGANIZATION_OID=pg.p_corp_org_oid";

        return where;
    }

    protected int setDynamicSQLValues(PreparedStatement statement, int startVarIdx,
            Map<String,String> parameters)
            throws SQLException {

    	 String parentOrgID = getUserSession().getOwnerOrgOid();
       	return setSqlVarsToPreparedStmt(statement, startVarIdx, new Object[]{parentOrgID});
    }
}
