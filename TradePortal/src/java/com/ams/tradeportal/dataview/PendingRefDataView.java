/**
 *
 */
package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;
import java.util.List;

import com.ams.tradeportal.busobj.util.SecurityAccess;
import com.ams.tradeportal.busobj.webbean.SessionWebBean;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsException;

/**
 *
 *
 */
public class PendingRefDataView     extends AbstractDBDataView {
private static final Logger LOG = LoggerFactory.getLogger(PendingRefDataView.class);

	private final List<Object> sqlParams = new ArrayList<>();
	/**
	 * Constructor
	 */
	public PendingRefDataView() {
		super();
	}

    protected String buildDynamicSQLCriteria(Map<String,String> parameters) {

    	StringBuilder dynamicWhere = new StringBuilder("");

        String loginRights = null;
        boolean showNothing = true;
        String  userSecurityType  = null;

        SessionWebBean userSession = getUserSession().getSavedUserSession();
      //jgadela 10/10/2013 - Rel 8.3 IR T36000021772  - Modified code to get userSession.
        // Retrieve the user's security type (i.e., ADMIN or NON_ADMIN)
        if (userSession == null) {
        	userSession = getUserSession();
        	userSecurityType = getUserSession().getSecurityType();
        }else {
      	   	userSecurityType = getUserSession().getSavedUserSessionSecurityType();
        }

        loginRights = userSession.getSecurityRights();

        String loggedInUser 		= userSession.getUserOid();
        String clientBankID = userSession.getClientBankOid();
        String currentSessionUserSecType = getUserSession().getSecurityType();
        //jgadela 10/10/2013 - Rel 8.3 IR T36000021772  - Modified the block security conditions and sql logic - added condition CHANGED_USER_SECURITY_TYPE = 'ADMIN'
        // This block is used to filter the REF data types for admin user level
        if(userSecurityType.equals(TradePortalConstants.ADMIN)){

        	if(!TradePortalConstants.OWNER_GLOBAL.equals(userSession.getOwnershipLevel())){

        		if(currentSessionUserSecType.equalsIgnoreCase("ADMIN") ){
        			dynamicWhere.append(" and (a.owner_org_oid = ? ");
           		 	sqlParams.add(clientBankID);
        		}
        		else{
        			dynamicWhere.append(" and (a.owner_org_oid = ? ");
           			sqlParams.add(getUserSession().getOwnerOrgOid());
        		}
            	dynamicWhere.append(" or  a.owner_org_oid in  ("+getBOGUserObjectsSql()+"))");
        	}
        	// Adding SQL logic to get Admin users  ref data type, and also for the CORPORATE users in the case of
        	if (SecurityAccess.hasRights(loginRights, SecurityAccess.MAINTAIN_ADM_USERS) ||
        			(currentSessionUserSecType.equalsIgnoreCase(TradePortalConstants.OWNER_TYPE_NON_ADMIN) && SecurityAccess.hasRights(getUserSession().getSecurityRights(), SecurityAccess.MAINTAIN_USERS))) {

        		if(currentSessionUserSecType.equalsIgnoreCase("ADMIN") ){
                	dynamicWhere.append("and a.ownership_level != 'CORPORATE'");

                }

        		sqlParams.add(loggedInUser);

            	dynamicWhere.append("and ((a.CHANGED_OBJECT_OID != ? and " +"(CHANGE_USER_OID in ("+getAdminUserObjectsSql(parameters)+") and CLASS_NAME in ('User')) ");
            	
            	// If the current user security type is NON_ADMIN(In case if the adminuser logged in  accesses the corporate customer page), show only corporate users, which are
            	// modified by admin users.

            	if (SecurityAccess.hasRights(loginRights, SecurityAccess.MAINTAIN_ADM_USERS) && SecurityAccess.hasRights(getUserSession().getSecurityRights(), SecurityAccess.MAINTAIN_USERS) &&
            			currentSessionUserSecType.equalsIgnoreCase(TradePortalConstants.OWNER_TYPE_NON_ADMIN)) {
            		dynamicWhere.append("and a.ownership_level = 'CORPORATE'");

            	}

            	if(TradePortalConstants.OWNER_TYPE_ADMIN.equalsIgnoreCase(currentSessionUserSecType))
            	dynamicWhere.append(" and CHANGED_USER_SECURITY_TYPE = 'ADMIN')");

            	else
            		dynamicWhere.append(" )");


            	showNothing = false;
            }
        	// Adding SQL logic to get CorporateOrganization  ref data type, In this case we have to check the access rights on current session,
        	// since corporate user does not have access to CorporateOrganization.
        	//If not ASP user then check for CorporateOrganization  ref data type
        	if ((!TradePortalConstants.OWNER_GLOBAL.equals(userSession.getOwnershipLevel())) && (SecurityAccess.hasRights(getUserSession().getSecurityRights(), SecurityAccess.MAINTAIN_CORP_BY_BG) || SecurityAccess.hasRights(loginRights, SecurityAccess.MAINTAIN_CORP_BY_CB))) {


            	if(showNothing){
            		dynamicWhere.append(" and ( ( (a.CHANGED_OBJECT_OID in  ("+getCorpCustObjectsSql(parameters)+") and a.CLASS_NAME in ('CorporateOrganization')) " +
            			                         " or (a.CHANGE_USER_OID in ("+getAdminUserObjectsSql(parameters)+") and a.CLASS_NAME in ('CorporateOrganization') and CHANGE_TYPE= 'C')     )");
            	}else{
            		dynamicWhere.append(" or ((a.CHANGED_OBJECT_OID in  ("+getCorpCustObjectsSql(parameters)+") and CLASS_NAME in ('CorporateOrganization') ) " +
            				"or (CHANGE_USER_OID in ("+getAdminUserObjectsSql(parameters)+") and CLASS_NAME in ('CorporateOrganization') and CHANGE_TYPE= 'C')        )");
            	}

            	showNothing = false;
            }
        	//jgadela 10/10/2013 - Rel 8.3 IR T36000021772
        	// Adding SQL logic to get panel auth groups, which are modified by admin.
        	//In this case we have to check the access rights on current session, since corporate user does not have access to CorporateOrganization.
        	if (SecurityAccess.hasRights(getUserSession().getSecurityRights(), SecurityAccess.MAINTAIN_PANEL_AUTH_GRP)) {
        		if(TradePortalConstants.OWNER_TYPE_ADMIN.equalsIgnoreCase(currentSessionUserSecType))
                	dynamicWhere.append(" and CHANGED_USER_SECURITY_TYPE = 'ADMIN')");

            	if(showNothing){
            		/*IR#T36000021910 added owner_org_id*/
            		dynamicWhere.append("and ( (a.CHANGE_USER_OID in ("+getAdminUserObjectsSql(parameters)+") and a.CLASS_NAME in ('PanelAuthorizationGroup')  and OWNER_ORG_OID = ? )");
            		sqlParams.add(getUserSession().getOwnerOrgOid());
            	}else{
            		/*IR#T36000021910 added owner_org_id*/
            		dynamicWhere.append("or (a.CHANGE_USER_OID in ("+getAdminUserObjectsSql(parameters)+") and a.CLASS_NAME in ('PanelAuthorizationGroup') and OWNER_ORG_OID = ? )");
            		sqlParams.add(getUserSession().getOwnerOrgOid());
            	}

            	showNothing = false;
            }

        	if(showNothing){
        		dynamicWhere.append("and 1=0");
        	}else{
        		dynamicWhere.append(" )");
        	}

          }// This block is used to filter the REF data types for admin user level
          else{
        	//logged in user should see the change
        	  //display only changes at corporate level made by ADMIN/Corp user
        	  dynamicWhere.append(" and a.owner_org_oid = ? ");
        	  sqlParams.add(getUserSession().getOwnerOrgOid());
        	  //dynamicWhere.append(" and u.a_client_bank_oid = "+ clientBankID);
        	  dynamicWhere.append("and a.ownership_level = 'CORPORATE'");
        	  // Adding SQL logic to get CorporateUser   ref data type
        	  //MEeer Rel 8.3 IR-T36000022010 Corporate users not allowed to view admin user changes
        	  if (SecurityAccess.hasRights(getUserSession().getSecurityRights(), SecurityAccess.MAINTAIN_USERS)) {
        		 // Corporate users are allowed to view admin user changes
        		// SureshL IR-T36000028771 06/03/2014 Start 
        		  dynamicWhere.append(" and ((CLASS_NAME = 'User' and a.CHANGED_OBJECT_OID != ?) ");//IR#24486 vishal sarkary SQL syntex  Error 
        		  sqlParams.add(loggedInUser);
              	  showNothing = false;
              }

               if (SecurityAccess.hasRights(getUserSession().getSecurityRights(), SecurityAccess.MAINTAIN_PANEL_AUTH_GRP)){
              	if(showNothing){
              		dynamicWhere.append(" and (CLASS_NAME = 'PanelAuthorizationGroup' ");
    			}else{
    				dynamicWhere.append(" or CLASS_NAME = 'PanelAuthorizationGroup' "); 
 
    			}
              	showNothing = false;
              }

			if(showNothing){
				dynamicWhere.append("and 1=0");
			}else{
				dynamicWhere.append(" )");
        	}
          }
         return dynamicWhere.toString();
    }

    protected int setDynamicSQLValues(PreparedStatement statement, int startVarIdx,
            Map<String,String> parameters)
            throws SQLException {

    	return setSqlVarsToPreparedStmt(statement, startVarIdx, sqlParams);
    }


    private String getAdminUserObjectsSql(Map<String,String> parameters) {

    	StringBuilder sql = new StringBuilder("");
    	SessionWebBean userSession = getUserSession().getSavedUserSession();

        if (userSession == null) {
        	userSession = getUserSession();
        }

    	String bogID = userSession.getBogOid();
    	String clientBankID = userSession.getClientBankOid();
    	String ownershipLevel = userSession.getOwnershipLevel();

    	sql.append("select a.user_oid from users a, org_names_view org where activation_status = 'ACTIVE'  and a.p_owner_org_oid=org.organization_oid ");


         if (ownershipLevel.equals(TradePortalConstants.OWNER_GLOBAL)) {
            sql.append("and (a.ownership_level = '");
            sql.append(TradePortalConstants.OWNER_GLOBAL);
            sql.append("' or a.ownership_level = '");
            sql.append(TradePortalConstants.OWNER_BANK);
            sql.append("')");
         }
         else if (ownershipLevel.equals(TradePortalConstants.OWNER_BANK) && getUserSession().getSecurityType().equalsIgnoreCase("ADMIN")) {
            sql.append(" and ((a.a_client_bank_oid = ? ");
            sqlParams.add(clientBankID);
            sql.append(" and (a.ownership_level = '");
            sql.append(TradePortalConstants.OWNER_BANK);
            sql.append("' or a.ownership_level = '");
            sql.append(TradePortalConstants.OWNER_BOG);
            sql.append("')) or a.ownership_level = '");
            sql.append(TradePortalConstants.OWNER_GLOBAL);
            sql.append("')");
         }
         else if (ownershipLevel.equals(TradePortalConstants.OWNER_BANK) && getUserSession().getSecurityType().equalsIgnoreCase("NON_ADMIN")) {
             sql.append(" and a.a_client_bank_oid = ? ");
             sqlParams.add(clientBankID);
             sql.append(" and (a.ownership_level = '");
             sql.append(TradePortalConstants.OWNER_BANK);
             sql.append("' or a.ownership_level = '");
             sql.append(TradePortalConstants.OWNER_BOG);
             sql.append("' or a.ownership_level = '");
             sql.append(TradePortalConstants.OWNER_CORPORATE);
             sql.append("')");
          }
         else {
            sql.append(" and a.p_owner_org_oid = ? ");
            sql.append(" and a.ownership_level = ? ");
            sqlParams.add(bogID);
            sqlParams.add(ownershipLevel);
         }

    	return sql.toString();
    }

 private String getBOGUserObjectsSql() {

    	SessionWebBean userSession = getUserSession().getSavedUserSession();

        if (userSession == null) {
        	userSession = getUserSession();
        }

    	String clientBankID = userSession.getClientBankOid();

    	String sql="select organization_oid from bank_organization_group  where activation_status = 'ACTIVE' and p_client_bank_oid = ? ";
    	sqlParams.add(clientBankID);


    	return sql;
    }

    private String getCorpCustObjectsSql(Map<String,String> parameters) {

    	StringBuilder sql = new StringBuilder("");

    	String bogID = getUserSession().getBogOid();
    	String clientBankID = getUserSession().getClientBankOid();
    	String ownershipLevel = getUserSession().getOwnershipLevel();

    	sql.append("select   a.organization_oid from corporate_org a, bank_organization_group b where  a.a_bank_org_group_oid = b.organization_oid");

    	if(ownershipLevel.equals(TradePortalConstants.OWNER_BANK)){
        		sql.append(" and b.p_client_bank_oid = ? ");
        		sqlParams.add(clientBankID);
        }
        else if (ownershipLevel.equals(TradePortalConstants.OWNER_BOG)){
        		sql.append(" and a_bank_org_group_oid = ? ");
        		sqlParams.add(bogID);
        }
        else {
            sql.append(" and 1=0");
        }

    	return sql.toString();
    }


 protected String getLinkAction(DataViewManager.DataViewColumn column) throws AmsException {

	  String   linkAction = "";

	  String refDataType =  (String)getDataValue("DataType");
	  String userType =  (String)getDataValue("OwnershipLevel");

	  if("User".equalsIgnoreCase(refDataType) && ("BANK".equalsIgnoreCase(userType) || "BOG".equalsIgnoreCase(userType)  )){
	    	linkAction = "selectAdminUser";
	  }else if("CorporateOrganization".equalsIgnoreCase(refDataType)){
	    	linkAction = "selectCorporateCustomer";

	  }else if("User".equalsIgnoreCase(refDataType) && "CORPORATE".equalsIgnoreCase(userType) ){
	    	linkAction = "selectUser";
	  }
	  else if("PanelAuthorizationGroup".equalsIgnoreCase(refDataType)){
	    	linkAction = "selectPanelGroup";
	  }

      return linkAction;
    }

 /**
  * @return
  */
 protected String getAdditionalURLParm() {
                 return "&fromAction=APPROVE";

 }
}
