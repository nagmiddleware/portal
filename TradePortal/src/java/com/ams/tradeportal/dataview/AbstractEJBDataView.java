/**
 *
 */
package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;

import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.util.DocumentHandler;

public abstract class AbstractEJBDataView extends AbstractDataView {
private static final Logger LOG = LoggerFactory.getLogger(AbstractEJBDataView.class);

    int start = 0;
    int count = 0;
    String sortColumnName = null;
    List<DocumentHandler> resultSet = null;
    private int index = -1;
    private int totalRecordCount = 0;

    public void setPageStart(int start) {
        this.start = start;
        index = start - 1;
    }

    public void setPageCount(int count) {
        this.count = count;
    }

    public void setSortColumnName(String sortColumnName) {
        this.sortColumnName = sortColumnName;
    }

    /**
	 * Get data view data.
	 *
	 * @param viewName - view name which have a data view object of same name
	 * @param parameters - all url parameters passed to the data view servlet
	 * @param fmt - the format to return. by default JSON
	 * @return
	 * @throws java.lang.Exception
	 */
	@Override
	public String getData(String viewName, Map<String, String> parameters, String fmt)
	        throws Exception {
				return getData(viewName, parameters, fmt, true);
			}

	/**
     * Get data view data.
     *
     * @param viewName - view name which have a data view object of same name
     * @param parameters - all url parameters passed to the data view servlet
     * @param fmt - the format to return. by default JSON
     * @return
     * @throws java.lang.Exception
     */
    @Override
    public String getData(String viewName, Map<String, String> parameters, String fmt, Boolean encrypt)
            throws Exception {
        super.getData(viewName, parameters, fmt, encrypt); //IR 16481
        //ensure all the basic stuff is passed in
        if (null == viewName || null == parameters || null == fmt) {
            throw new Exception("Required arguments are missing");
        }

        String start = parameters.get("start");
        String count = parameters.get("count");
        String sortColumnName = parameters.get("sort");
        String parentId = parameters.get("parentId");

        DataViewManager.DataViewMetadata metadata = getDataViewMetadata(viewName);
        setPageStart(Integer.parseInt(start));
        setPageCount(Integer.parseInt(count));
        setSortColumnName(sortColumnName);

        setFormatType(fmt);
        setSortColumn(metadata, parameters);
        // should include count as well as data
        DocumentHandler doc = getDataFromEjbSource(parameters);
        if (doc != null) {
            doc = sort(doc);
            resultSet = doc.getFragmentsList("/ResultSetRecord");
            totalRecordCount = resultSet.size();
        }

        String output = renderJSON(metadata, totalRecordCount, parentId, parameters, encrypt);
        return output;
    }


    /*
     * The subclass should provide implementation to this method.
     */
    abstract protected DocumentHandler getDataFromEjbSource(Map<String, String> parameters) throws Exception;


    /* (non-Javadoc)
     * @see com.ams.tradeportal.dataview.AbstractDataView#hasNext()
     */
    @Override
    protected boolean hasNext() throws AmsException {
        index++;
        return (index < start + count && index < totalRecordCount);

    }

    /**
     * @param xmlDoc
     * @return
     */
    public DocumentHandler sort(DocumentHandler xmlDoc) {

        String sortOrder = null;

        if (sortColumnName != null && sortColumnName.length() > 0) {
			//QueryReadStore prepends a hyphen to the json field name
            // when the column should be displayed descending
            if (sortColumnName.startsWith("-")) {
                sortOrder = "DSC";
                sortColumnName = sortColumnName.substring(1, sortColumnName.length());
            } else {
                sortOrder = "ASC";
            }
        }

        List<DocumentHandler> vecPage = xmlDoc.getFragmentsList("/ResultSetRecord");
        int size = vecPage.size();

        for (int j = 0; j < size; j++) {
            for (int i = 1; i < (size - j); i++) {
                DocumentHandler node1 = vecPage.get(i - 1);
                DocumentHandler node2 = vecPage.get(i);

                if (sortColumnName == null) {
                    sortColumnName = "NAME";
                    sortOrder = "ASC";
                }

                String str1 = node1.getAttribute("/" + sortColumnName);
                String str2 = node2.getAttribute("/" + sortColumnName);

                if (stringCompareTo(str1, str2, sortOrder)) {
                    vecPage.set(i - 1, node2);
                    vecPage.set(i, node1);
                }
            }
        }

        DocumentHandler sortedDoc = new DocumentHandler();
        for (int i = 0; i < vecPage.size(); i++) {
            DocumentHandler node = vecPage.get(i);
            sortedDoc.addComponent("/ResultSetRecord(" + i + ")", node);
        }
        return sortedDoc;

    }

    /**
     * @param str1
     * @param str2
     * @param sortOrder
     * @return
     */
    boolean stringCompareTo(String str1, String str2, String sortOrder) {

        if (str1 == null) {
            str1 = "0";
        }
        if (str2 == null) {
            str2 = "0";
        }

        str1 = str1.trim();
        str2 = str2.trim();

        str1 = str1.toUpperCase();
        str2 = str2.toUpperCase();

        int val = str1.compareTo(str2);

        boolean flag = false;

        if ("ASC".equals(sortOrder) || sortOrder == null) {
            if (val > 0) {
                flag = true;
            }
        } else if ("DSC".equals(sortOrder)) {
            if (val < 0) {
                flag = true;
            }
        }

        return flag;
    }


    /* (non-Javadoc)
     * @see com.ams.tradeportal.dataview.AbstractDataView#getDataValue(java.lang.String)
     */
    @Override
    protected Object getDataValue(String col) throws AmsException {

        String dataValue = resultSet.get(index).getAttribute("/" + col);

        return dataValue;

    }

}
