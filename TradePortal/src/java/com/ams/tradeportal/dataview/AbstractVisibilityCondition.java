package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.busobj.webbean.SessionWebBean;
import com.amsinc.ecsg.util.ResourceManager;
import com.amsinc.ecsg.web.BeanManager;

/**
 * AbstractVisibilityCondition must be implemented
 * by all visibility conditions.  A visibility condition is used by the
 * DataGridFactory to determine when a column is visible or not.
 */
public abstract class AbstractVisibilityCondition {
private static final Logger LOG = LoggerFactory.getLogger(AbstractVisibilityCondition.class);

   

    /**
     * This is called for column that has visibility condition defined.
     */
    public abstract boolean execute(SessionWebBean userSession, BeanManager beanMgr, ResourceManager resMgr);
}
