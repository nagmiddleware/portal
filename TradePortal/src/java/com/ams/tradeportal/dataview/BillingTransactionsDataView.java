
/**
*  08/01/2015 New Version 
*  R 94 CR 932
*/
package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Map;

import com.ams.tradeportal.common.SQLParamFilter;
import com.amsinc.ecsg.util.EncryptDecrypt;

/**

*
*/
public class BillingTransactionsDataView     extends AbstractDBDataView {
private static final Logger LOG = LoggerFactory.getLogger(BillingTransactionsDataView.class);

	/**
	 * Constructor
	 */
	public BillingTransactionsDataView() {
		super();
	}

   protected String buildDynamicSQLCriteria(Map<String,String> parameters) {
       StringBuilder sql = new StringBuilder();

     
       //InstrumentOid is encrypted for security
       String encryptedInstrumentOid = parameters.get("InstrumentOid");
       if (encryptedInstrumentOid!=null && encryptedInstrumentOid.length()>0) {
           sql.append(" and a.transaction_oid in (select original_transaction_oid as transaction_oid ");
           sql.append("from instrument where a_related_instrument_oid = ? ");
           sql.append(" union ");
           sql.append("select transaction_oid from transaction ");
           sql.append("where p_instrument_oid = ? ");
           sql.append(")");
       }
       else {
           //instrumentOid is required
           sql.append(" and 1=0");
       }
    
       
       return sql.toString();
   }

   protected int setDynamicSQLValues(PreparedStatement statement, int startVarIdx,
           Map<String,String> parameters)
           throws SQLException {
       int varCount = 0;
       
  
       //InstrumentOid is encrypted for security
       String encryptedInstrumentOid = parameters.get("InstrumentOid");
       if (encryptedInstrumentOid!=null && encryptedInstrumentOid.length()>0) {
           String instrumentOid = EncryptDecrypt.decryptStringUsingTripleDes(
               encryptedInstrumentOid, getUserSession().getSecretKey());
           instrumentOid = SQLParamFilter.filterNumber(instrumentOid);
           
           statement.setString(startVarIdx+varCount, instrumentOid);
           varCount++;
           statement.setString(startVarIdx+varCount, instrumentOid);
           varCount++;
       }
    
       
       return varCount;
   }
}
