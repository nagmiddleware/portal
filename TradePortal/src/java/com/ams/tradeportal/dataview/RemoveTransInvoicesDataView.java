/**
 *
 */
package com.ams.tradeportal.dataview;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.busobj.util.InvoiceUtility;
import com.ams.tradeportal.common.TPDateTimeUtility;
import com.amsinc.ecsg.util.StringFunction;

/**
 *
 *
 */
public class RemoveTransInvoicesDataView extends AbstractDBDataView {
private static final Logger LOG = LoggerFactory.getLogger(RemoveTransInvoicesDataView.class);

    private final List<Object> sqlParams = new ArrayList<>();

    /**
     * Constructor
     */
    public RemoveTransInvoicesDataView() {
        super();
    }

    @Override
    protected String buildDynamicSQLCriteria(Map<String, String> parameters) {

        StringBuilder sql = new StringBuilder();

        String userOrgOid = getUserSession().getOwnerOrgOid();
        String uploadDefinitionOid = parameters.get("uploadDefinitionOid");
        String currency = parameters.get("currency");
        String beneficiaryName = parameters.get("beneficiaryName");
        String beneName = parameters.get("beneName");
        String sourceType = parameters.get("sourceType");
        String poNumber = parameters.get("poNumber");
        String amountFrom = parameters.get("amountFrom");
        String amountTo = parameters.get("amountTo");
        String dueDateFrom = parameters.get("dueDateFrom");
        String dueDateTo = parameters.get("dueDateTo");
        String invClassification = parameters.get("invClassification");
        String transactionOid = parameters.get("transactionOid");
        String importIndicator = parameters.get("importIndicator");
        String financeType = parameters.get("financeType");
        String buyerBacked = parameters.get("buyerBacked");

        if (!StringFunction.isBlank(dueDateFrom)) {
            dueDateFrom = TPDateTimeUtility.convertToDBDate(dueDateFrom, "MM/dd/yyyy");
        }
        if (!StringFunction.isBlank(dueDateTo)) {
            dueDateTo = TPDateTimeUtility.convertToDBDate(dueDateTo, "MM/dd/yyyy");
        }
       // boolean hasPOLineItems = Boolean.parseBoolean(parameters.get("hasPOLineItems"));
        // boolean isEncrypted = Boolean.parseBoolean(parameters.get("encrypted"));

        //parameter check start
        if (transactionOid == null) {
            transactionOid = "";
        }
        if (userOrgOid == null) {
            userOrgOid = "";
        }
        if (uploadDefinitionOid == null) {
            uploadDefinitionOid = "";
        }
        if (currency == null | "null".equals(currency)) {
            currency = "";
        }
        if (beneficiaryName == null) {
            beneficiaryName = "";
        }
        if (beneName == null) {
            beneName = "";
        }
        if (sourceType == null) {
            sourceType = "";
        }
        if (poNumber == null) {
            poNumber = "";
        }
        if (amountFrom == null) {
            amountFrom = "";
        }
        if (amountTo == null) {
            amountTo = "";
        }
        if (dueDateFrom == null) {
            dueDateFrom = "";
        }
        if (dueDateTo == null) {
            dueDateTo = "";
        }
        //parameter check end

        sql.append("where A_CORP_ORG_OID = ? ");
        sqlParams.add(userOrgOid);

        String loanTpes = InvoiceUtility.getLoanType(importIndicator, financeType, buyerBacked);

        sql.append(" and (LOAN_TYPE IN (");
        sql.append(prepareDynamicSqlStrForInClause(loanTpes, sqlParams));

        sql.append(" ) or LOAN_TYPE IS NULL) ");
        if (StringFunction.isNotBlank(invClassification)) {
            sql.append(" and INVOICE_CLASSIFICATION = ? ");
            sqlParams.add(invClassification);
        }

        if (!StringFunction.isBlank(transactionOid)) {
            sql.append(" and A_TRANSACTION_OID = ? ");
            sqlParams.add(transactionOid);
        }
        // Search by the PO Number
        if (!poNumber.equals("")) {
            sql.append(" and upper(invoice_id) like ? ");
            sqlParams.add(poNumber.toUpperCase() + "%");
        }
        if (!("").equals(beneName)) {
            String name = "buyer_name";
            if ("R".equals(invClassification)) {
                name = "buyer_name";
            } else if ("P".equals(invClassification)) {
                name = "seller_name";
            }

            sql.append(" and upper(").append(name).append(") like ? ");
            sqlParams.add(beneName.toUpperCase() + "%");
        }

        if (!("").equals(currency)) {
            sql.append(" and upper(currency) like ? ");
            sqlParams.add(currency.toUpperCase() + "%");
        }
        if (!("").equals(amountFrom) && !("").equals(amountTo)) {
            sql.append(" and amount >= ? ");
            sqlParams.add(amountFrom);
            sql.append(" and amount <=  ? ");
            sqlParams.add(amountTo);
        }
        if (!("").equals(amountFrom) && ("").equals(amountTo)) {
            sql.append(" and amount >=? ");
            sqlParams.add(amountFrom);
        }
        if (("").equals(amountFrom) && !("").equals(amountTo)) {
            sql.append(" and amount <=? ");
            sqlParams.add(amountTo);
        }

        if (!("").equals(dueDateFrom) && !("").equals(dueDateTo)) {
            sql.append(" and CASE WHEN PAYMENT_DATE IS NULL THEN DUE_DATE ELSE PAYMENT_DATE END BETWEEN ? and ? ");
            sqlParams.add(dueDateFrom);
            sqlParams.add(dueDateTo);
        }
        if (!("").equals(dueDateFrom) && ("").equals(dueDateTo)) {
            sql.append(" and CASE WHEN PAYMENT_DATE IS NULL THEN DUE_DATE ELSE PAYMENT_DATE END  >= ? ");
            sqlParams.add(dueDateFrom);
        }
        if (("").equals(dueDateFrom) && !("").equals(dueDateTo)) {
            sql.append(" and CASE WHEN PAYMENT_DATE IS NULL THEN DUE_DATE ELSE PAYMENT_DATE END <= ? ");
            sqlParams.add(dueDateTo);
        }

        sql.append(" ");

        return sql.toString();
    }

    @Override
    protected int setDynamicSQLValues(PreparedStatement statement, int startVarIdx,
            Map<String, String> parameters)
            throws SQLException {
        return setSqlVarsToPreparedStmt(statement, startVarIdx, sqlParams);
    }
}
