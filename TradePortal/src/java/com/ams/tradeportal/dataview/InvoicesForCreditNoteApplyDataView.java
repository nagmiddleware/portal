package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;
import java.util.List;

import com.ams.tradeportal.common.NumberValidator;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.InvalidAttributeValueException;
import com.amsinc.ecsg.util.DateTimeUtility;
import com.amsinc.ecsg.util.StringFunction;

public class InvoicesForCreditNoteApplyDataView extends AbstractDBDataView {
private static final Logger LOG = LoggerFactory.getLogger(InvoicesForCreditNoteApplyDataView.class);

    private final List<Object> sqlParams = new ArrayList<>();

    public InvoicesForCreditNoteApplyDataView() {
        super();
    }

    @Override
    protected String buildDynamicSQLCriteria(Map<String,String> parameters) throws DataViewException {

        StringBuilder dynamicWhereClause = new StringBuilder();
        String userOrgOid = getUserSession().getOwnerOrgOid();

        String invoiceID = parameters.get("invoiceID");

        String dateType = parameters.get("dateType");
        String fromDate = parameters.get("fromDate");
        String toDate = parameters.get("toDate");
        String amountType = parameters.get("amountType");
        String amountFrom = parameters.get("amountFrom");
        String amountTo = parameters.get("amountTo");
        String loginLocale = parameters.get("loginLocale");

        String creditNoteCurrency = parameters.get("creditNoteCurrency");
        String creditNoteSeller = parameters.get("creditNoteSeller");
        String creditNoteOid = parameters.get("creditNoteOid");
        
       
        String sellerName = parameters.get("sellerName");

        //due/payment date should be equal to or greater than current business date
        try {
            dynamicWhereClause.append(" and (case when  payment_date is null then due_date else payment_date end >= to_date('"
                    + DateTimeUtility.getGMTDateTime()
                    + "', 'mm/dd/yyyy hh24:mi:ss'))");
        } catch (AmsException e1) {
            throw new DataViewException("Error obtaining business date and converting it. File: InvoicesForCreditNoteApplyDataView.java", e1);
        }
        dynamicWhereClause.append(" and a_corp_org_oid = ");
        dynamicWhereClause.append(userOrgOid);

        //currency,seller_id
        dynamicWhereClause.append(" and currency=  ? ");
        sqlParams.add(creditNoteCurrency);

        if (StringFunction.isNotBlank(sellerName)) {

            dynamicWhereClause.append(" and seller_name = ? ");
            sqlParams.add(sellerName);
        } else if (StringFunction.isNotBlank(creditNoteSeller)) {

            dynamicWhereClause.append(" and seller_id = ? ");
            sqlParams.add(creditNoteSeller);
        }

        dynamicWhereClause.append(" and upload_invoice_oid not in (select a_upload_invoice_oid from invoices_credit_notes_relation "
                + "where a_upload_credit_note_oid = ? )");
        sqlParams.add(creditNoteOid);

        // Search by the invoice ID
        if (invoiceID != null && !invoiceID.equals("")) {
            dynamicWhereClause.append(" and ");
            dynamicWhereClause.append(" upper(INVOICE_ID) like ? ");
            sqlParams.add("%" + invoiceID.toUpperCase() + "%");
        }

		// Search by date type
        if (dateType != null && !dateType.equals("")) {

            String expandedDateType = "";

            if (dateType.equals("DDT")) {
                expandedDateType = "due_date";
            } else if (dateType.equals("IDT")) {
                expandedDateType = "issue_date";
            } else if (dateType.equals("PDT")) {
                expandedDateType = "payment_date";
            }

            if (fromDate != null && !fromDate.equals("")) {
                dynamicWhereClause.append(" and ");
                dynamicWhereClause.append(expandedDateType + " >= to_date(?,'mm/dd/yyyy')");
                sqlParams.add(fromDate);
            }

            if (toDate != null && !toDate.equals("")) {
                dynamicWhereClause.append(" and ");
                dynamicWhereClause.append(expandedDateType + " <= to_date(?,'mm/dd/yyyy')");
                sqlParams.add(toDate);
            }
        }

        // Search by Amount
        if (amountType != null && !StringFunction.isBlank(amountType)) {

            String expandedAmountType = "";
            if (amountType.equals("INA")) {
                expandedAmountType = "amount";
            } else if (amountType.equals("PYA")) {
                expandedAmountType = "payment_amount";
            }

            if (amountFrom != null && !amountFrom.equals("")) {
                amountFrom = amountFrom.trim();
                try {
                    String amount = NumberValidator.getNonInternationalizedValue(amountFrom, loginLocale);
                    if(StringFunction.isNotBlank(amount)){
                        dynamicWhereClause.append(" and ");
                        dynamicWhereClause.append(expandedAmountType + " >= ? ");
                        sqlParams.add(amount);
                    }
                } catch (InvalidAttributeValueException e) {

                }

            }

            if (amountTo != null && !amountTo.equals("")) {

                amountTo = amountTo.trim();
                try {
                    String amount = NumberValidator.getNonInternationalizedValue(amountTo, loginLocale);
                    if (!amount.equals("")) {
                        dynamicWhereClause.append(" and ");
                        dynamicWhereClause.append(expandedAmountType + " <= ? ");
                        sqlParams.add(amount);
                    }
                } catch (InvalidAttributeValueException e) {
                }
            }
        }

        return dynamicWhereClause.toString();
    }

    @Override
    protected int setDynamicSQLValues(PreparedStatement statement, int startVarIdx,
            Map<String,String> parameters)
            throws SQLException {
        return setSqlVarsToPreparedStmt(statement, startVarIdx, sqlParams);
    }
}
