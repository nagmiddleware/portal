/**
 *
 */
package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;
import java.util.List;

import com.ams.tradeportal.common.TradePortalConstants;


/**
 *
 *
 */
public class NotificationRulesDataView     extends AbstractDBDataView
{
private static final Logger LOG = LoggerFactory.getLogger(NotificationRulesDataView.class);

	private final List<Object> sqlParams = new ArrayList<>();
	/**
	 * Constructor
	 */
	public NotificationRulesDataView() {
		super();
	}

    protected String buildDynamicSQLCriteria(Map<String,String> parameters) {

        String globalID = getUserSession().getGlobalOrgOid();
        String parentOrgID = getUserSession().getOwnerOrgOid();
        
        StringBuilder sql = new StringBuilder();
        sql.append(" and n.p_owner_org_oid in (?,?");
        sqlParams.add(parentOrgID);
        sqlParams.add(globalID);
	    sql.append(")");

        return sql.toString();
    }

    protected int setDynamicSQLValues(PreparedStatement statement, int startVarIdx,
            Map<String,String> parameters)
            throws SQLException {
    	return setSqlVarsToPreparedStmt(statement, startVarIdx, sqlParams);
    }
}
