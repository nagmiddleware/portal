/**
 *
 */
package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Map;

import com.ams.tradeportal.busobj.util.SecurityAccess;
import com.ams.tradeportal.busobj.webbean.CorporateOrganizationWebBean;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.util.EncryptDecrypt;
import com.amsinc.ecsg.web.BeanManager;
import com.ams.tradeportal.common.SQLParamFilter;

/**

 *
 */
public class InstrumentsAuthorizedTransactionsDataView extends AbstractDBDataView {
private static final Logger LOG = LoggerFactory.getLogger(InstrumentsAuthorizedTransactionsDataView.class);

    //cquinton 3/26/2013 use constant instead of translated value for ALL
    static final String ALL_ORGS = "AuthorizedTransactions.AllOrganizations";

    /**
     * Constructor
     */
    public InstrumentsAuthorizedTransactionsDataView() {
        super();
    }

    @Override
    protected String buildDynamicSQLCriteria(Map<String,String> parameters) {
        StringBuilder sql = new StringBuilder();

        String encryptedSelectedOrg = parameters.get("selectedOrg");

        if (encryptedSelectedOrg != null && encryptedSelectedOrg.length() > 0) {
            String selectedOrg = EncryptDecrypt.decryptStringUsingTripleDes(
                    encryptedSelectedOrg, getUserSession().getSecretKey());
            if (ALL_ORGS.equals(selectedOrg)) {
                sql.append("and b.a_corp_org_oid in (");
                sql.append(" select organization_oid");
                sql.append(" from corporate_org");
                sql.append(" where activation_status = '");
                sql.append(TradePortalConstants.ACTIVE);
                sql.append("' start with organization_oid = ?"); //userOrgOid
                sql.append(" connect by prior organization_oid = p_parent_corp_org_oid)");
            } else {
                sql.append("and b.a_corp_org_oid = ?"); //selectedOrg
            }
        } else {
            //selectedOrgOid is required
            sql.append("and 1=0");
        }

        // Add additional where clause to remove Cash Management transactions if necessary.    
        // boolean filterAllCashMgmtTrans =Boolean.parseBoolean(parameters.get("filterAllCashMgmtTrans"));
        //MEerupula Rel 8.4 IR-23915 STARTS
        boolean filterAllCashMgmtTrans = false;
        BeanManager beanMgr = this.getBeanManager();
        String confInd = getConfidentialInd();   //DK IR T36000024539 Rel8.4 02/04/2014

        // Set the filterAllCashMgmtTrans flag.  This is used to filter out all the cash management transactions or all but International payments.
        // We do not filter the international payments if the corporate organization does not use transfer between accounts and domestic payments.
        CorporateOrganizationWebBean corpOrg = beanMgr.createBean(CorporateOrganizationWebBean.class, "CorporateOrganization");
        corpOrg.getById(getUserSession().getOwnerOrgOid());

        if (corpOrg.getAttribute("allow_panel_auth_for_pymts").equals(TradePortalConstants.INDICATOR_YES)) {
            boolean allowXferBtwnAccts = corpOrg.getAttribute("allow_xfer_btwn_accts_panel").equals(TradePortalConstants.INDICATOR_YES);
            boolean allowDomesticPayments = corpOrg.getAttribute("allow_domestic_payments_panel").equals(TradePortalConstants.INDICATOR_YES);
            filterAllCashMgmtTrans = (allowXferBtwnAccts || allowDomesticPayments) && SecurityAccess.canViewDomesticPaymentOrTransfer(getUserSession().getSecurityRights());
        } else {
            boolean allowXferBtwnAccts = corpOrg.getAttribute("allow_xfer_btwn_accts").equals(TradePortalConstants.INDICATOR_YES);
            boolean allowDomesticPayments = corpOrg.getAttribute("allow_domestic_payments").equals(TradePortalConstants.INDICATOR_YES);
            filterAllCashMgmtTrans = (allowXferBtwnAccts || allowDomesticPayments) && SecurityAccess.canViewDomesticPaymentOrTransfer(getUserSession().getSecurityRights());
        }
        //MEerupula Rel 8.4 IR-23915 ENDS

        if (filterAllCashMgmtTrans) {
            sql.append(" and b.instrument_type_code not in('FTBA','FTDP','FTRQ','DDI')");
        } else {
            sql.append(" and b.instrument_type_code not in('FTBA','FTDP','DDI')");
        }

        if (TradePortalConstants.INDICATOR_NO.equalsIgnoreCase(confInd)) //DK IR T36000024539 Rel8.4 02/04/2014
        {
            sql.append(" AND (tr.confidential_indicator  = 'N' OR tr.confidential_indicator IS NULL)");
        }

        return sql.toString();
    }

    @Override
    protected int setDynamicSQLValues(PreparedStatement statement, int startVarIdx,
            Map<String,String> parameters)
            throws SQLException {
        int varCount = 0;

        String encryptedSelectedOrg = parameters.get("selectedOrg");
        if (encryptedSelectedOrg != null && encryptedSelectedOrg.length() > 0) {
            String selectedOrg = EncryptDecrypt.decryptStringUsingTripleDes(
                    encryptedSelectedOrg, getUserSession().getSecretKey());
            if (ALL_ORGS.equals(selectedOrg)) {
                String userOrgOid = getUserSession().getOwnerOrgOid();

                statement.setString(startVarIdx + varCount, userOrgOid);
                varCount++;
            } else {
                selectedOrg = SQLParamFilter.filterNumber(selectedOrg);

                statement.setString(startVarIdx + varCount, selectedOrg);
                varCount++;
            }
        }

        return varCount;
    }
}
