/**
 *
 */
package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Map;

/**
 *
 *
 */
public class UsersDataView extends AbstractDBDataView {
private static final Logger LOG = LoggerFactory.getLogger(UsersDataView.class);

    /**
     * Constructor
     */
    public UsersDataView() {
        super();
    }

    @Override
    protected String buildDynamicSQLCriteria(Map<String,String> parameters) {

        return " and a.p_owner_org_oid = ? ";
    }

    @Override
    protected int setDynamicSQLValues(PreparedStatement statement, int startVarIdx,
            Map<String,String> parameters)
            throws SQLException {

        int varCount = 0;
        statement.setString(startVarIdx + varCount, getUserSession().getOwnerOrgOid());
        varCount++;

        return varCount;
    }
}
