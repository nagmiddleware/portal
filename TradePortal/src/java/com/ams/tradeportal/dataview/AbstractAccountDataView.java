package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.ams.tradeportal.agents.AcctBalanceQuery;
import com.ams.tradeportal.busobj.ClientBank;
import com.ams.tradeportal.busobj.CorporateOrganization;
import com.ams.tradeportal.busobj.webbean.SessionWebBean;
import com.ams.tradeportal.busobj.webbean.UserWebBean;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.BusinessObject;
import com.amsinc.ecsg.frame.ComponentList;
import com.amsinc.ecsg.frame.EJBObjectFactory;
import com.amsinc.ecsg.util.JPylonProperties;
import com.amsinc.ecsg.util.ResourceManager;
import com.amsinc.ecsg.web.BeanManager;

import java.util.HashMap;

/**

 */
public abstract class AbstractAccountDataView
        extends AbstractDBDataView {
private static final Logger LOG = LoggerFactory.getLogger(AbstractAccountDataView.class);

    /**
	 * vdesingu Rel8.4 CR-590 : Get Account data view data.
	 *
	 * @param viewName - view name which have a data view object of same name
	 * @param parameters - all url parameters passed to the data view servlet
	 * @param fmt - the format to return. by default JSON
	 * @return
	 * @throws java.lang.Exception
	 */
	@Override
	public String getData(String viewName, Map<String, String> parameters, String fmt)
	        throws Exception {
				return getData(viewName, parameters, fmt, true);
			}

	/**
     * vdesingu Rel8.4 CR-590 : Get Account data view data.
     *
     * @param viewName - view name which have a data view object of same name
     * @param parameters - all url parameters passed to the data view servlet
     * @param fmt - the format to return. by default JSON
     * @return
     * @throws java.lang.Exception
     */
    @Override
    public String getData(String viewName, Map<String, String> parameters, String fmt, Boolean encrypt)
            throws Exception {

        SessionWebBean userSession = getUserSession();

        String refreshStr = null;
        //Check the refresh parameter value.
        if (null != parameters) {
            if (null != parameters.get("refresh")) {
                refreshStr = parameters.get("refresh");
            }
        }
        if ((null != refreshStr && "true".equalsIgnoreCase(refreshStr))) {
            refreshAccounts(); //Call Account Interface to get latest balance.
            userSession.setAcctBalancesRefreshed(true);//Set accout Balances refresh flag to true.
        }
        //call to construct grid data
        return super.getData(viewName, parameters, fmt, encrypt);

    }

    /*Call Account Interface to get latest balance.*/
    public String refreshAccounts() {

        SessionWebBean userSession = getUserSession();
        ResourceManager resMgr = this.getResourceManager();
        BeanManager beanMgr = this.getBeanManager();

        String userOid = userSession.getUserOid();
        String corpOrgOid = userSession.getOwnerOrgOid();
        String userLocale = userSession.getUserLocale();

        // if the user locale not specified default to en_US
        if ((userLocale == null) || (userLocale.equals(""))) {
            userLocale = "en_US";
        }

        int retCode = 0; //IAZ IR-TOUJ042348632 09/30/09 Add
        try {
            String serverLocation = JPylonProperties.getInstance().getString("serverLocation");

            List<BusinessObject> accounts = new ArrayList<>();
            UserWebBean user = beanMgr.createBean(UserWebBean.class, "User");
            user.getById(userOid);

            ClientBank clientBank = (ClientBank) EJBObjectFactory.createClientEJB(serverLocation, "ClientBank",
                    Long.parseLong(user.getAttribute("client_bank_oid")), resMgr.getCSDB());
            clientBank.getDataFromReferenceCache(Long.parseLong(user.getAttribute("client_bank_oid")));

            CorporateOrganization corpOrg = (CorporateOrganization) EJBObjectFactory.createClientEJB(serverLocation,
                    "CorporateOrganization", Long.parseLong(corpOrgOid), resMgr.getCSDB());
            ComponentList acctList = (ComponentList) corpOrg.getComponentHandle("AccountList");

            int tot = acctList.getObjectCount();
            Map<String, BusinessObject> accountMap = new HashMap<>(tot);

            for (int i = 0; i < tot; i++) {
                acctList.scrollToObjectByIndex(i);
                BusinessObject busObj = acctList.getBusinessObject();
                accounts.add(busObj);
                accountMap.put(busObj.getAttribute("account_oid"), busObj);
            }
            Map map;
            try {
                map = (new AcctBalanceQuery()).queryAcctBalance(userOid, null, clientBank, corpOrg, accounts);
            } catch (AmsException ex) {
                if (TradePortalConstants.MISSING_REQUIRED_FIELDS.equals(ex.getMessage())) {
                    this.getErrorManager().issueError(
                            TradePortalConstants.ERR_CAT_1, TradePortalConstants.BAL_QUERY_MISSING_FIELD);
                    return "Error - Balance Query Missing Field";
                } else {
                    throw ex;
                }
            }
            int tot_errrors_returned = -1;
            if (map != null) {
                corpOrg = (CorporateOrganization) EJBObjectFactory.createClientEJB(serverLocation,
                        "CorporateOrganization", Long.parseLong(corpOrgOid), resMgr.getCSDB());
                acctList = (ComponentList) corpOrg.getComponentHandle("AccountList");
                tot = acctList.getObjectCount();
                for (int i = 0; i < tot; i++) {
                    acctList.scrollToObjectByIndex(i);
                    BusinessObject busObj = acctList.getBusinessObject();
                    String key = busObj.getAttribute("account_oid");
                    BusinessObject updBusObj = accountMap.get(key);
                    if (updBusObj != null) {
                        busObj.setAttribute("current_balance", updBusObj.getAttribute("current_balance"));
                        busObj.setAttribute("available_funds", updBusObj.getAttribute("available_funds"));
                        busObj.setAttribute("interest_rate", updBusObj.getAttribute("interest_rate"));
                        busObj.setAttribute("last_retrieved_from_bank_date", updBusObj.getAttribute("last_retrieved_from_bank_date"));
                        busObj.setAttribute("status_code", updBusObj.getAttribute("status_code"));
                    }
                }

                corpOrg.touch();
                corpOrg.setRetryOptLock(true);	//IAZ IR-TOUJ042348632 09/30/09 Add
                retCode = corpOrg.save(false); //IAZ IR-TOUJ042348632 09/30/09 CHT
                tot_errrors_returned = map.size();
                LOG.info("AccountQueryMediator: Total number of accounts that had error status code " + tot_errrors_returned);
            }

            //IAZ IR-TOUJ042348632 09/30/09 Begin
            if (retCode == -2) {
                LOG.info("Optimistic Lock Exception Updating Account Balances on Refresh.");
            }

            if (map == null || tot_errrors_returned > 0 || retCode != 1) {
                //IAZ IR-TOUJ042348632 09/30/09 End
                this.getErrorManager().initialize(); // clear 
                this.getErrorManager().issueError(
                        TradePortalConstants.ERR_CAT_1, TradePortalConstants.ACCT_BAL_NOT_AVAILABLE);
            }

        } catch (Exception e) {
            LOG.info("Exception occured in AccountQueryMediator: " + e.getMessage());
            e.printStackTrace();
        }

        return "";

    }

}
