/**
 *
 */
package com.ams.tradeportal.dataview;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.PropertyResourceBundle;
import java.util.StringTokenizer;
import java.util.Vector;

import com.ams.tradeportal.common.Debug;
import com.ams.tradeportal.common.SQLParamFilter;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.EncryptDecrypt;
import com.amsinc.ecsg.util.StringFunction;
import com.businessobjects.rebean.wi.REException;
import com.businessobjects.rebean.wi.ReportEngines;
import com.crystaldecisions.sdk.exception.SDKException;
import com.crystaldecisions.sdk.framework.CrystalEnterprise;
import com.crystaldecisions.sdk.framework.IEnterpriseSession;
import com.crystaldecisions.sdk.framework.ISessionMgr;
import com.crystaldecisions.sdk.occa.infostore.IInfoObject;
import com.crystaldecisions.sdk.occa.infostore.IInfoObjects;
import com.crystaldecisions.sdk.occa.infostore.IInfoStore;
import com.crystaldecisions.sdk.plugin.CeProgID;

import java.util.Map;

/**
 *
 *
 */
public class ReportsDataView extends AbstractEJBDataView {
	private static final Logger LOG = LoggerFactory.getLogger(ReportsDataView.class);

	String reportType = null;
	String selectedCategory = null;

	/**
	 * Constructor
	 */
	public ReportsDataView() {
		super();
	}

	private final int startRow = 0;
	private final boolean debug = true;

	@Override
	protected DocumentHandler getDataFromEjbSource(Map<String, String> parameters) throws Exception {

		int iRepoType = 0;
		int iCatID = 0;
		int totalRows = 0;
		String currentCategory = "";
		String strQuery = null;
		String catIDSQL = "";

		IInfoObjects infoObjs = null;
		IInfoObject infoObj = null;

		// WebIntelligence variables
		IInfoObjects webiDocumentList = null;
		IInfoStore infoStore = null;
		String repoType = null;
		DocumentHandler doc = new DocumentHandler();
		IEnterpriseSession boSession = null;
		ISessionMgr sessionMgr = null;
		ReportEngines reportEngines = null;

		String userName = null;
		String userPass = null;
		PropertyResourceBundle portalProperties = (PropertyResourceBundle) PropertyResourceBundle.getBundle("TradePortal");

		String cmsName = portalProperties.getString("CMSName");
		String cmsAuth = portalProperties.getString("Authentication");

		String filterSReportname = parameters.get("filterSReportname");
		if (filterSReportname == null) {
			filterSReportname = "";
		} else {
			filterSReportname = SQLParamFilter.filter(filterSReportname);
		}

		String filterCReportname = parameters.get("filterCReportname");
		if (filterCReportname == null) {
			filterCReportname = "";
		} else {
			filterCReportname = SQLParamFilter.filter(filterCReportname);
		}

		reportType = parameters.get("reportType");
		if (reportType == null) {
			reportType = "";
		}
		selectedCategory = parameters.get("selectedCategory");

		String categories = parameters.get("categories");
		if (categories == null) {
			categories = "";
		}

		String[] parts = categories.split(",");
		String temp = "";
		for (int i = 0; i < parts.length; i++) {
			temp = temp + "'" + SQLParamFilter.filter(parts[i]) + "'";
			if (i != (parts.length - 1)) {
				temp = temp + ", ";
			}
		}
		categories = temp;

		String reportingUserSuffix = getUserSession().getReportingUserSuffix();

		if (reportingUserSuffix == null) {
			// rbhaduri - 30th Nov 09 - IR KAUJ112336153 Modify Begin - M And T
			// Bank Change - get reporting suffix based on the level of the user logged in. For ASP level
			// user get the get the suffix from global organization table. But for all other
			// levels, get the suffix from the client bank table

			String userOwnerShipLevel = null;
			userOwnerShipLevel = getUserSession().getOwnershipLevel();

			if (userOwnerShipLevel.equals(TradePortalConstants.OWNER_GLOBAL)) {
				// Get the reporting user suffix from the Global Org to which this org belongs
				String reportingUserSuffixSql = "select reporting_user_suffix from global_organization where organization_oid =?  ";

				DocumentHandler result = DatabaseQueryBean.getXmlResultSet(reportingUserSuffixSql, false, new Object[] { getUserSession().getGlobalOrgOid() });

				reportingUserSuffix = result.getAttribute("/ResultSetRecord(0)/REPORTING_USER_SUFFIX");

				// Put it on the session so that later accesses of this page
				// don't have to look it up
				getUserSession().setReportingUserSuffix(reportingUserSuffix);
			} else {
				String clientBankOID = getUserSession().getClientBankOid();

				// Get the reporting user suffix from the Client Bank of the
				// logged in user
				String reportingUserSuffixSql = "select reporting_user_suffix from client_bank where organization_oid = ? ";

				DocumentHandler result = DatabaseQueryBean.getXmlResultSet(reportingUserSuffixSql, false, new Object[] { clientBankOID });

				reportingUserSuffix = result.getAttribute("/ResultSetRecord(0)/REPORTING_USER_SUFFIX");

				// Put it on the session so that later accesses of this page
				// don't have to look it up
				getUserSession().setReportingUserSuffix(reportingUserSuffix);

			} // rbhaduri - 30th Nov 09 - IR KAUJ112336153 Modify End - M And T Bank Change

		}

		// rbhaduri - 30th Nov 09 - IR KAUJ112336153 - added system out
		LOG.debug("*** <<Reporting Suffix>>: {}" , reportingUserSuffix);

		// Place the reporting user suffix on to the beginning of the OID
		// The suffix is used to distinguish between organizations being processed
		// by different ASPs that might be on the same reports server.
		userName = getUserSession().getOwnerOrgOid() + reportingUserSuffix;

		// get password from the property file
		userPass = getUserSession().getOwnerOrgOid() + reportingUserSuffix.toLowerCase();

		LOG.debug("*** Reports Dataview - reportType>>: {}  categories>>: {}" , reportType, categories);
		LOG.debug("*** Reports Dataview - userName>>: {}    userPass: {}" , userName, userPass);
		LOG.debug("*** Reports Dataview - cmsName>>: {} cmsAuth>>: {}" , cmsName, cmsAuth);

		try {
			sessionMgr = (ISessionMgr) CrystalEnterprise.getSessionMgr();
			boSession = ((com.crystaldecisions.sdk.framework.ISessionMgr) sessionMgr).logon(userName, userPass, cmsName, cmsAuth);
			reportEngines = (ReportEngines) boSession.getService("ReportEngines");
			getUserSession().setReportLoginInd(true); // Nar IR-T36000031433 Rel 9.2.0.0

			LOG.debug("*** Reports Dataview - sessionMgr>>: {}" , sessionMgr);
			LOG.debug("*** Reports Dataview - boSession>>: {}" , boSession);
			LOG.debug("*** Reports Dataview - reportEngines>>: {}" , reportEngines);
		} catch (REException rex) {
			LOG.error("*** <<REException in ReportsDataView>>: {}  {}" , rex.getErrorCode() , rex.getMessage());
			LOG.error("Exception occured:" , rex);
			// cquinton 1/25/2013 issue an error
			this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.WEBI_SESSION_ERROR);
			return null;
		} catch (SDKException sdk) {
			LOG.error("*** <<SDKException in ReportsDataView>>: {} " , sdk.getMessage());
			LOG.error("Exception occured:" , sdk);
			// cquinton 1/25/2013 in general it is not a good idea to suppress errors!
			// for a specific issue with reporting user name missing and reporting that back
			// issue an error. this can be displayed when dataviews are
			// called directly, however still don't have approach for datagrid calling this
			// todo: provide framework for datagrid to display these errors...
			// BO User is not setup in CMS
			StringTokenizer stk = new StringTokenizer(sdk.toString(), "OCA_Abuse");
			if (stk.hasMoreTokens()) {
				LOG.debug("BO user is not setup in CMS --> {}" , userName);
				this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.REPORT_USER_NOT_SETUP);
				return null;
			} else {
				this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.WEBI_SESSION_ERROR);
				return null;
			}

		} catch (Exception e) {
			LOG.error("*** <<Exception in ReportsDataView>>: {} " , e.getMessage(), e);
			// cquinton 1/25/2013 issue an error
			this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.WEBI_SERVER_ERROR);
			return null;
		}

		if (reportType.equals(TradePortalConstants.STANDARD_REPORTS)) {
			// iRepoType = WIDocuments.RT_CORPORATE; commented by kajal bhatt
			// not require in boxir2
			repoType = CeProgID.CATEGORY;
		} else {
			// iRepoType = WIDocuments.RT_PERSONAL; commented by kajal bhatt not
			// require in boxir2
			repoType = CeProgID.PERSONALCAT;
		}

		try {
			String strCurUser = boSession.getUserInfo().getUserName();
			infoStore = (IInfoStore) boSession.getService("InfoStore");

			if (reportType.equals(TradePortalConstants.CUSTOM_REPORTS)) {

				strQuery = "SELECT * FROM CI_INFOOBJECTS WHERE SI_KIND='PersonalCategory' AND SI_OWNER='" + strCurUser + "'";
			} else {

				strQuery = "SELECT * FROM CI_INFOOBJECTS WHERE SI_KIND='Category' AND SI_NAME in (" + categories + ")";

			}

			LOG.debug("*** ListViewHandler.getData 1st SQL : {}" , strQuery);

			if (infoStore != null) {
				infoObjs = infoStore.query(strQuery);

				// Rel 7.0 CR-603 W Zhu 4/26/2011
				// Get all categories.
				if (reportType.equals(TradePortalConstants.STANDARD_REPORTS)) {
					for (int i = 0; i < infoObjs.size(); i++) {
						infoObj = (IInfoObject) infoObjs.get(i);
						iCatID = infoObj.getID();
						if (i > 0) {
							catIDSQL += ",";
						}
						catIDSQL += "'" + String.valueOf(iCatID) + "'";
					}
					if (catIDSQL.equals("")) {
						catIDSQL = "''"; // empty category, might be caused by
					} // invalid categories configured for the user.
					currentCategory = catIDSQL;
				} else {
					if (infoObjs.size() > 0) {
						infoObj = (IInfoObject) infoObjs.get(0);
						iCatID = infoObj.getID();
						currentCategory = "" + iCatID;
					}
				}

				// The iCat will be 0 if All Categories is created by Admin user
				// during migtation. We need to run the following SQL
				// to get the correct ID for All Categories.
				if (iCatID == 0) {
					LOG.debug("*** Inside no personal Cat");

					if (reportType.equals(TradePortalConstants.CUSTOM_REPORTS)) {
						strQuery = "SELECT top 1 SI_ID FROM CI_INFOOBJECTS WHERE SI_KIND='PersonalCategory' AND SI_OWNER= '"
								+ strCurUser + "'";

						LOG.debug("*** 2nd SQL {}" , strQuery);
						infoObjs = infoStore.query(strQuery);

						if (infoObjs.size() > 0) {
							infoObj = (IInfoObject) infoObjs.get(0);
							iCatID = infoObj.getID();

							LOG.debug("*** Inside no personal Cat - iCatID {}" , iCatID);

						}

					}

				}

			}
			if (infoStore != null) {
				if (reportType.equals(TradePortalConstants.CUSTOM_REPORTS)) {
					strQuery = "SELECT SI_NAME , SI_DESCRIPTION, SI_ID FROM CI_INFOOBJECTS WHERE MEMBERS (\"SI_NAME='Category-Document'\", \"SI_PARENTID= "
							+ iCatID
							+ "\") AND SI_PROGID NOT IN('CystalEnterprise.Folder', 'CrystalEnterprise.FavoritesFolder', 'CrystalEnterprise.Inbox', 'CrystalEnterprise.Category', 'CrystalEnterprise.PersonalCategory') AND SI_INSTANCE = 0 "
							+ " AND SI_NAME LIKE '%" + filterCReportname + "%' order by SI_NAME";
					LOG.debug("*** ListViewHandler Custom Reports {}" , strQuery);
				} else {
					strQuery = "SELECT SI_NAME, SI_DESCRIPTION, SI_ID FROM CI_INFOOBJECTS WHERE MEMBERS (\"SI_NAME='Category-Document'\",\"SI_ID in ("
							+ catIDSQL + ")\") AND SI_NAME LIKE '%" + filterSReportname + "%' order by SI_NAME";
					LOG.debug("*** ListViewHandler Standard reports {}" , strQuery);
				}
				webiDocumentList = infoStore.query(strQuery);
			}
		} catch (SDKException e) {
			LOG.error("Exception occured " , e);
		}

		// the list of documents will be retrieved in a recordset format
		// webiRS is pointer to collection of documents
		// webiRS = webiDocumentList.getRecordset(true); commented by kajal
		// bhatt this method is not supported in xir2
		// totalRows = webiRS.getRecordCount(); commented by kajal bhatt, this
		// method is not supported in xir2
		totalRows = webiDocumentList != null ? webiDocumentList.getResultSize() : 0;

		LOG.debug("*** totalRows = {}" , totalRows);
		LOG.debug("*** Report Category = {}  Current Category =%{}%" , iCatID, currentCategory);


		if (totalRows != 0) {
			doc = getReportData(webiDocumentList, totalRows, iRepoType);

			LOG.debug(" *** <<doc obj>>: {}" , doc);
		}

		return doc;
	}

	/**
	 * This method retrieves report name, report id and description from WebIntelligence rcord set. The query result is set in a
	 * DocumentHandler
	 *
	 * @return DocumentHandler - Result set
	 * @param count
	 *            int - Row count
	 */
	private DocumentHandler getReportData(IInfoObjects webiDocs, int count, int iReportType) {
		int repCount = 0;
		int docID = 0;
		Integer ID = null;
		Integer repType = null;
		String name = "";
		String desc = "";

		Vector repName = new Vector();
		Vector repdocID = new Vector();
		Vector repDesc = new Vector();

		DocumentHandler reportData = new DocumentHandler();

		// loop through the record set and retrieve report name, ID and
		// description.
		for (int iIndex = startRow; iIndex < count + startRow; iIndex++) {
			repCount++;
			IInfoObject webiReport = (IInfoObject) webiDocs.get(iIndex);

			name = (String) webiReport.getTitle();
			name = StringFunction.xssHtmlToChars(name);
			repName.addElement(name);

			// Retrieve report ID and add it to report ID vector
			docID = webiReport.getID();
			repdocID.addElement(new Integer(docID));

			desc = (String) webiReport.getDescription();
			desc = StringFunction.xssHtmlToChars(desc);
			repDesc.addElement(desc);

		}

		// Building result set DocHandler
		for (int rowIndex = 0; rowIndex < repdocID.size(); rowIndex++) {

			for (int columnIndex = 0; columnIndex < 1; columnIndex++) {

				name = (String) repName.elementAt(rowIndex);
				desc = (String) repDesc.elementAt(rowIndex);
				ID = (Integer) repdocID.elementAt(rowIndex);
				repType = new Integer(iReportType);

				reportData.setAttribute("/ResultSetRecord(" + rowIndex + ")/DOCID", ID.toString());
				reportData.setAttribute("/ResultSetRecord(" + rowIndex + ")/NAME", name);
				reportData.setAttribute("/ResultSetRecord(" + rowIndex + ")/COMMENTS", desc);
				reportData.setAttribute("/ResultSetRecord(" + rowIndex + ")/REPORTTYPE", repType.toString());

			}

		}

		return reportData;
	}

	public DocumentHandler getDataInXmlFormat(Map<String, String> parameters) throws Exception {

		int iRepoType = 0;
		int iCatID = 0;
		int totalRows = 0;
		String currentCategory = "";
		String strQuery = null;
		String catIDSQL = "";

		IInfoObjects infoObjs = null;
		IInfoObject infoObj = null;

		// WebIntelligence variables
		IInfoObjects webiDocumentList = null;
		IInfoStore infoStore = null;
		String repoType = null;
		DocumentHandler doc = new DocumentHandler();
		IEnterpriseSession boSession = null;
		ISessionMgr sessionMgr = null;
		ReportEngines reportEngines = null;

		String reportType = parameters.get("reportType");
		if (reportType == null) {
			reportType = "";
		}

		String userName = parameters.get("userName");
		if (userName == null) {
			userName = "";
		}

		String userPass = parameters.get("userPass");
		if (userPass == null) {
			userPass = "";
		}

		String cmsName = parameters.get("cmsName");
		if (cmsName == null) {
			cmsName = "";
		}

		String cmsAuth = parameters.get("cmsAuth");
		if (cmsAuth == null) {
			cmsAuth = "";
		}

		String categories = parameters.get("categories");
		if (categories == null) {
			categories = "";
		}
		
		
		LOG.debug("*** Reports Dataview - reportType>>: {}  categories>>: {}" , reportType, categories);
		LOG.debug("*** Reports Dataview - userName>>: {}    userPass: {}" , userName, userPass);
		LOG.debug("*** Reports Dataview - cmsName>>: {} cmsAuth>>: {}" , cmsName, cmsAuth);


		try {
			sessionMgr = (ISessionMgr) CrystalEnterprise.getSessionMgr();
			boSession = ((com.crystaldecisions.sdk.framework.ISessionMgr) sessionMgr).logon(userName, userPass, cmsName, cmsAuth);
			reportEngines = (ReportEngines) boSession.getService("ReportEngines");
			getUserSession().setReportLoginInd(true); // Nar IR-T36000031433 Rel 9.2.0.0

			LOG.debug("*** Reports Dataview - sessionMgr>>: {}" , sessionMgr);
			LOG.debug("*** Reports Dataview - boSession>>: {}" , boSession);
			LOG.debug("*** Reports Dataview - reportEngines>>:{} " , reportEngines);

		} catch (REException rex) {
			LOG.error("*** <<REException in Reports Home>>: {}  {} " , rex.getErrorCode() , rex.getMessage());
			LOG.error("*** <<REException stack ***", rex);
		} catch (SDKException sdk) {
			LOG.error("*** <<SDKException in Reports Home>>: {}" , sdk.getMessage(), sdk);
		} catch (Exception e) {
			LOG.error("*** <<Exception in Reports Home>>: {} " , e.getMessage(), e);
		}

		if (reportType.equals(TradePortalConstants.STANDARD_REPORTS)) {
			// iRepoType = WIDocuments.RT_CORPORATE; commented by kajal bhatt not require in boxir2
			repoType = CeProgID.CATEGORY;
		} else {
			// iRepoType = WIDocuments.RT_PERSONAL; commented by kajal bhatt not require in boxir2
			repoType = CeProgID.PERSONALCAT;
		}

		try {
			String strCurUser = boSession.getUserInfo().getUserName();
			infoStore = (IInfoStore) boSession.getService("InfoStore");

			if (reportType.equals(TradePortalConstants.CUSTOM_REPORTS)) {

				strQuery = "SELECT * FROM CI_INFOOBJECTS WHERE SI_KIND='PersonalCategory' AND SI_OWNER='" + strCurUser + "'";
			} else {

				strQuery = "SELECT * FROM CI_INFOOBJECTS WHERE SI_KIND='Category' AND SI_NAME in (" + categories + ")";

			}

			LOG.debug("*** ListViewHandler.getData 1st SQL : {}" , strQuery);

			if (infoStore != null) {
				infoObjs = infoStore.query(strQuery);

				// Rel 7.0 CR-603 W Zhu 4/26/2011
				// Get all categories.
				if (reportType.equals(TradePortalConstants.STANDARD_REPORTS)) {
					for (int i = 0; i < infoObjs.size(); i++) {
						infoObj = (IInfoObject) infoObjs.get(i);
						iCatID = infoObj.getID();
						if (i > 0) {
							catIDSQL += ",";
						}
						catIDSQL += "'" + String.valueOf(iCatID) + "'";
					}
					if (catIDSQL.equals("")) {
						catIDSQL = "''"; // empty category, might be caused by
					} // invalid categories configured for
					// the user.
					currentCategory = catIDSQL;
				} else {
					if (infoObjs.size() > 0) {
						infoObj = (IInfoObject) infoObjs.get(0);
						iCatID = infoObj.getID();
						currentCategory = "" + iCatID;
					}
				}

				// The iCat will be 0 if All Categories is created by Admin user
				// during migtation. We need to run the following SQL
				// to get the correct ID for All Categories.
				if (iCatID == 0) {
					LOG.debug("*** Inside no personal Cat");

					if (reportType.equals(TradePortalConstants.CUSTOM_REPORTS)) {
						strQuery = "SELECT top 1 SI_ID FROM CI_INFOOBJECTS WHERE SI_KIND='PersonalCategory' AND SI_OWNER= '"
								+ strCurUser + "'";

						LOG.debug("*** 2nd SQL {}" , strQuery);
						infoObjs = infoStore.query(strQuery);

						if (infoObjs.size() > 0) {
							infoObj = (IInfoObject) infoObjs.get(0);
							iCatID = infoObj.getID();

							LOG.debug("*** Inside no personal Cat - iCatID {}" , iCatID);

						}

					}

				}

			}
			if (infoStore != null) {
				if (reportType.equals(TradePortalConstants.CUSTOM_REPORTS)) {
					strQuery = "SELECT SI_NAME , SI_DESCRIPTION, SI_ID FROM CI_INFOOBJECTS WHERE MEMBERS (\"SI_NAME='Category-Document'\", \"SI_PARENTID= "
							+ iCatID
							+ "\") AND SI_PROGID NOT IN('CystalEnterprise.Folder', 'CrystalEnterprise.FavoritesFolder', 'CrystalEnterprise.Inbox', 'CrystalEnterprise.Category', 'CrystalEnterprise.PersonalCategory') AND SI_INSTANCE = 0 order by SI_NAME";
					LOG.debug("*** ListViewHandler Custom Reports {}" , strQuery);
				} else {
					strQuery = "SELECT SI_NAME, SI_DESCRIPTION, SI_ID FROM CI_INFOOBJECTS WHERE MEMBERS (\"SI_NAME='Category-Document'\",\"SI_ID in ("
							+ catIDSQL + ")\") order by SI_NAME";
					LOG.debug("*** ListViewHandler Standard reports {}" , strQuery);
				}
				webiDocumentList = infoStore.query(strQuery);
			}
		} catch (SDKException e) {
			LOG.error("Exception occured {} " , e.getMessage(), e);
		}

		// the list of documents will be retrieved in a recordset format
		// webiRS is pointer to collection of documents
		// webiRS = webiDocumentList.getRecordset(true); commented by kajal
		// bhatt this method is not supported in xir2
		// totalRows = webiRS.getRecordCount(); commented by kajal bhatt, this
		// method is not supported in xir2
		totalRows = webiDocumentList != null ? webiDocumentList.getResultSize() : 0;

		LOG.debug("*** totalRows = {}" , totalRows);
		LOG.debug("*** Report Category = {}   Category=%{}%" , iCatID,currentCategory);

		if (totalRows != 0) {
			doc = getReportData(webiDocumentList, totalRows, iRepoType);

			LOG.debug(" *** <<doc obj>>: {}" , doc);
		}
		return doc;
	}

	/**
	 * @param column
	 * @return
	 */
	protected String getLinkAction(DataViewManager.DataViewColumn column) {
		String userAction = null;
		if (reportType.equals(TradePortalConstants.STANDARD_REPORTS)) {
			userAction = "goToStandardReports";
		}
		if (reportType.equals(TradePortalConstants.CUSTOM_REPORTS)) {
			userAction = "goToCustomReports";
		}
		return userAction;
	}

	/**
	 * @return
	 */
	protected String getAdditionalURLParm() {
		//if it not null, base64 encode this url parameter, otherwise use the value as is
		if(StringFunction.isNotBlank(selectedCategory)) selectedCategory = EncryptDecrypt.stringToBase64String(selectedCategory);
		return "&selectedCategory=" + selectedCategory;

	}

	public String getData(String viewName, Map<String, String> parameters, String fmt, Boolean encrypt) throws Exception {
		return super.getData(viewName, parameters, fmt, false); // IR 16481
	}

}
