/**
 *
 */
package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;
import java.util.List;

import com.amsinc.ecsg.util.EncryptDecrypt;

/**
 *
 *
 */
public class PaymentMatchInvoiceDataView extends AbstractDBDataView  {
private static final Logger LOG = LoggerFactory.getLogger(PaymentMatchInvoiceDataView.class);
	private final List<Object> sqlParams = new ArrayList<>();
	/**
	 * Constructor
	 */
	public PaymentMatchInvoiceDataView () {
		super();
	}

	protected String buildDynamicSQLCriteria(Map<String,String> parameters) {
		StringBuilder sql = new StringBuilder();
		try {
			String payRemitInvoiceOid  = parameters.get("payRemitInvoiceOid");
		    String payRemitOid 		   = parameters.get("payRemitOid");
		    String withoutremititemsInd   = parameters.get("withoutRemitItemsInd");
		    if (payRemitOid != null && payRemitOid.trim().length() > 0){
		    		sql.append(" and PAY_MATCH_RESULT.P_PAY_REMIT_OID = ? ");
		    		sqlParams.add(payRemitOid);
		    }

		    if(payRemitInvoiceOid != null && payRemitInvoiceOid.trim().length() > 0 && withoutremititemsInd !=null && !Boolean.valueOf(withoutremititemsInd)){
		    	payRemitInvoiceOid = EncryptDecrypt.decryptStringUsingTripleDes(payRemitInvoiceOid, getUserSession().getSecretKey());
		    	sql.append(" and PAY_MATCH_RESULT.A_PAY_REMIT_INVOICE_OID = ? ");
		    	sqlParams.add(payRemitInvoiceOid);
		    }
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return sql.toString();
	}

	protected int setDynamicSQLValues(PreparedStatement statement,
			int startVarIdx, Map<String,String> parameters)
			throws SQLException {
		return setSqlVarsToPreparedStmt(statement, startVarIdx, sqlParams);
	}
}
