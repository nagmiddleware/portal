/**
 * 
 */
package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;
import java.util.List;

import com.amsinc.ecsg.util.EncryptDecrypt;
import com.amsinc.ecsg.util.StringFunction;


/**
 * 
 *
 */
public class RemoveStructurePODataView     extends AbstractDBDataView {
private static final Logger LOG = LoggerFactory.getLogger(RemoveStructurePODataView.class);
	private final List<Object> sqlParams = new ArrayList<>();
	/**
	 * Constructor
	 */
	public RemoveStructurePODataView() {
		super();
	}

    protected String buildDynamicSQLCriteria(Map<String,String> parameters) {

    	StringBuilder sql = new StringBuilder();
        
    	String userOrgOid = parameters.get("userOrgOid");
        String transactionOid = parameters.get("transactionOid");
        String shipmentOid = parameters.get("shipmentOid");
        String beneficiaryName = parameters.get("beneficiaryName");
        String poNum = parameters.get("poNumber");
        String poCurrency = parameters.get("currency");
        String amountFrom = parameters.get("amountFrom");
        String amountTo = parameters.get("amountTo");
 
        userOrgOid = EncryptDecrypt.decryptStringUsingTripleDes(userOrgOid, getUserSession().getSecretKey());
        transactionOid = EncryptDecrypt.decryptStringUsingTripleDes(transactionOid, getUserSession().getSecretKey());
        shipmentOid = EncryptDecrypt.decryptStringUsingTripleDes(shipmentOid, getUserSession().getSecretKey());
        
        sql.append("where a_owner_org_oid = ? ");
        sqlParams.add(userOrgOid);
        sql.append(" and a_transaction_oid = ? ");
        sqlParams.add(transactionOid);
        sql.append(" and a_shipment_oid = ? ");
        sqlParams.add(shipmentOid);
        sql.append(" and purchase_order_type != 'AMD' ");
        
        if(StringFunction.isNotBlank(poNum)){
        	sql.append(" and upper(purchase_order_num) like ? ");
        	sqlParams.add(poNum.toUpperCase()+"%");
		}
		if(StringFunction.isNotBlank(poCurrency)){
			sql.append(" and upper(currency) = ? ");
			sqlParams.add((poCurrency.substring(0, 3)).toUpperCase());
		}
		if(StringFunction.isNotBlank(beneficiaryName)){
			sql.append(" and upper(seller_name) like ? ");
			sqlParams.add(beneficiaryName.toUpperCase()+"%");
		}
		if(StringFunction.isNotBlank(amountFrom) && StringFunction.isNotBlank(amountTo)){
			sql.append(" and amount between ? and ? ");
			sqlParams.add(amountFrom);
			sqlParams.add(amountTo);
		}	
        
        		
       	sql.append(" group by purchase_order_num,seller_name, currency,amount, creation_date_time,purchase_order_oid,issue_date, latest_shipment_date"); 
       	
    	return sql.toString();
    }

    protected int setDynamicSQLValues(PreparedStatement statement, int startVarIdx, 
            Map<String,String> parameters) 
            throws SQLException {
        
    	return setSqlVarsToPreparedStmt(statement, startVarIdx, sqlParams);
    }
}
