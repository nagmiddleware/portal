/*
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Special subclass of the <code>Exception</code> class used for generic
 * data view exceptions.
 * 
 * todo: add the dataview name?
 */
public class DataViewException extends Exception
{
private static final Logger LOG = LoggerFactory.getLogger(DataViewException.class);
    /**
     * Constructs an <code>DataViewException</code> with no specified detail message.
     */
    public DataViewException ()
    {
        super ();
    }
    
    /**
     * Constructs an <code>DataViewException</code> with no specified detail message and a cause.
     * @param cause - chained exception
     */
    public DataViewException (Throwable cause)
    {        
        super (cause);
    }

    /**
     * Constructs an <code>DataViewException</code> with the specified detail
     * message.
     *
     * @param s - the detail message.
     */
    public DataViewException (String s)
    {
        super(s);
    }
    
    /**
     * Constructs an <code>DataViewException</code> with the specified detail
     * message and cause.
     *
     * @param s - the detail message.
     * @param cause - chained exception
     */    
    public DataViewException(String s, Throwable cause) {
        super(s,cause);        
    }
}
