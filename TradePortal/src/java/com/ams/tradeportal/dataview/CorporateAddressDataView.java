/**
 *
 */
package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Map;

/**
 *
 *
 */
public class CorporateAddressDataView extends AbstractDBDataView {
private static final Logger LOG = LoggerFactory.getLogger(CorporateAddressDataView.class);

    /**
     * Constructor
     */
    public final static String INVOICE_SEARCH_STATUS_CLOSED = "CLOSED";

    public CorporateAddressDataView() {

        super();
    }

    @Override
    protected String buildDynamicSQLCriteria(Map<String, String> parameters) {

        return "where p_corp_org_oid = ?";
    }

    @Override
    protected int setDynamicSQLValues(PreparedStatement statement, int startVarIdx,
            Map<String, String> parameters)
            throws SQLException {
        int varCount = 0;

        statement.setString(startVarIdx + varCount, parameters.get("corpOrgOid"));
        varCount++;

        return varCount;
    }
}
