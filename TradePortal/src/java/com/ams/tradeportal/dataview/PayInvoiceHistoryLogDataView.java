/**
 *
 */
package com.ams.tradeportal.dataview;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.util.EncryptDecrypt;

/**
 *
 */
public class PayInvoiceHistoryLogDataView extends AbstractDBDataView {

	private final List<Object> sqlParams = new ArrayList<>();

	/**
	 * Constructor
	 */
	public PayInvoiceHistoryLogDataView() {
		super();
	}

	@Override
	protected String buildDynamicSQLCriteria(Map<String, String> parameters) {

		StringBuilder whereClause = new StringBuilder();
		StringBuilder countClause = new StringBuilder();

		whereClause.append(buildDynamicSQLCriteriaIn(parameters));
		countClause.append(buildDynamicSQLCriteriaCountIn(parameters));


		super.fullSQL = whereClause.toString();
		super.fullCountSQL = countClause.toString();

		return null;

	}

	private String buildDynamicSQLCriteriaIn(Map<String, String> parameters) {
		StringBuilder dynamicWhereClause = new StringBuilder();
		String select1 = "select invoice_history_oid, DateAndTime, Action, UserInfo, "
				+ "PanelLevel, Status,  row_num from (select  a.invoice_history_oid,"
				+ "a.action_datetime as DateAndTime, a.action as Action, "
				+ "(CASE WHEN a.a_user_oid IS NULL THEN '' ELSE u.user_identifier  || ' / ' ||  u.first_name  || ' ' ||  u.last_name END)  as UserInfo, "
				+ "a.panel_authority_code as PanelLevel,  a.invoice_status as Status, " + "     rownum as row_num           ";
		String from1 = " from INVOICE_HISTORY a,invoice b, USERS u    ";
		String where1 = " where a.a_user_oid = u.user_oid (+) and b.invoice_oid= a.p_upload_invoice_oid  and b.a_corp_org_oid=?"
				+ " and b.link_to_instrument_type= '" + TradePortalConstants.INVOICE_TYPE_PAY_MGMT
				+ "' and b.invoice_reference_id = ? and b.supplier_portal_invoice_ind ='" + TradePortalConstants.INDICATOR_NO + "'";
		dynamicWhereClause.append(select1).append(from1).append(where1);
		Long longval = Long.parseLong(getUserSession().getOwnerOrgOid());
		sqlParams.add(longval);
		sqlParams.add(EncryptDecrypt.decryptStringUsingTripleDes(parameters.get("invoice_id"), getUserSession().getSecretKey()));

		dynamicWhereClause.append(" UNION ");

		String select2 = " select  a.invoice_history_oid,  a.action_datetime as DateAndTime,"
				+ " a.action as Action, (CASE WHEN a.a_user_oid IS NULL THEN '' ELSE u.user_identifier  || ' / ' ||  u.first_name  || ' ' ||  u.last_name END)  as UserInfo, "
				+ "a.panel_authority_code as PanelLevel, a.invoice_status as Status  " + ", rownum as row_num   ";
		String from2 = " from INVOICE_HISTORY a,invoices_summary_data b, USERS u ";
		String where2 = " where a.a_user_oid = u.user_oid (+)  and b.upload_invoice_oid= a.p_upload_invoice_oid  and b.a_corp_org_oid= ?"
				+ " and b.linked_to_instrument_type='" + TradePortalConstants.PAYABLES_MGMT + "' and  b.invoice_id = ? ";

		dynamicWhereClause.append(select2).append(from2).append(where2);
		longval = Long.parseLong(getUserSession().getOwnerOrgOid());
		sqlParams.add(longval);

		sqlParams.add(EncryptDecrypt.decryptStringUsingTripleDes(parameters.get("invoice_id"), getUserSession().getSecretKey()));

		dynamicWhereClause.append(" ) WHERE row_num > ? AND row_num  <= ?  order by DateAndTime ");
		return dynamicWhereClause.toString();

	}

	private String buildDynamicSQLCriteriaCountIn(Map<String, String> parameters) {
		StringBuilder dynamicWhereClause = new StringBuilder();
		String select1 = " SELECT count(invoiceOid) FROM (SELECT a.invoice_history_oid AS invoiceOid";
		String from1 = " from INVOICE_HISTORY a,invoice b, USERS u    ";
		String where1 = " where a.a_user_oid = u.user_oid (+) and b.invoice_oid= a.p_upload_invoice_oid  and b.a_corp_org_oid=?"
				+ " and b.link_to_instrument_type= '" + TradePortalConstants.INVOICE_TYPE_PAY_MGMT
				+ "' and b.invoice_reference_id = ? " + "and b.supplier_portal_invoice_ind ='" + TradePortalConstants.INDICATOR_NO + "'";

		dynamicWhereClause.append(select1).append(from1).append(where1);

		dynamicWhereClause.append(" UNION ");

		String select2 = " SELECT a.invoice_history_oid AS invoiceOid";
		String from2 = " from INVOICE_HISTORY a,invoices_summary_data b, USERS u ";
		String where2 = " where a.a_user_oid = u.user_oid (+)  and b.upload_invoice_oid= a.p_upload_invoice_oid  and b.a_corp_org_oid= ?"
				+ " and b.linked_to_instrument_type='" + TradePortalConstants.PAYABLES_MGMT + "' and  b.invoice_id = ? ";
		dynamicWhereClause.append(select2).append(from2).append(where2);

		dynamicWhereClause.append(" ) ");

		return dynamicWhereClause.toString();
	}

	@Override
	protected int setDynamicSQLValues(PreparedStatement statement, int startVarIdx, Map<String, String> parameters)
			throws SQLException {

		return setSqlVarsToPreparedStmt(statement, startVarIdx, sqlParams);
	}
}
