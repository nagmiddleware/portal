/**
 *
 */
package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.ams.tradeportal.common.NumberValidator;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.InvalidAttributeValueException;
import com.amsinc.ecsg.frame.MediatorServices;
import com.amsinc.ecsg.util.StringFunction;

/**
 *
 *
 */
public class InvoicesFvdOfferedDataView    extends AbstractDBDataView
{
private static final Logger LOG = LoggerFactory.getLogger(InvoicesFvdOfferedDataView.class);
	private final List<Object> sqlParams = new ArrayList<>();
	/**
	 * Constructor
	 */
	public InvoicesFvdOfferedDataView() {
		super();
	}

    protected String buildDynamicSQLCriteria(Map<String,String> parameters) {


    	StringBuilder dynamicWhereClause     = new StringBuilder();
    	MediatorServices medService = new MediatorServices();

        String userOrgOid = getOwnerOrgOid();
        String currency = parameters.get("currency");
    	String amountFrom = parameters.get("amountFrom");
        String amountTo = parameters.get("amountTo");
        String dateFrom = parameters.get("dateFrom");
        String dateTo = parameters.get("dateTo");
        String fvDateFrom = parameters.get("fvDateFrom");
        String fvDateTo = parameters.get("fvDateTo");
        String datePattern = getUserSession().getDatePattern();

        if (userOrgOid != null && !userOrgOid.equals("")) {
	        dynamicWhereClause.append(" and i.a_corp_org_oid = ? ");
	        sqlParams.add(userOrgOid);
        }
        else {
            dynamicWhereClause.append(" and 1=0"); //for security no access if userOrgOid is not set
        }
        
        if (currency != null && !currency.equals("")) {
     	   dynamicWhereClause.append(" and i.currency=? ");
     	   sqlParams.add(currency);
        }
        
        if (dateFrom!=null && !dateFrom.equals("")) {
            dynamicWhereClause.append(" and i.due_payment_date >= TO_DATE(?,'").append(datePattern).append("')");
         	sqlParams.add(dateFrom);
        }
       	if (dateTo!=null && !dateTo.equals("")) {
         	  dynamicWhereClause.append(" and i.due_payment_date <= TO_DATE(?,'").append(datePattern).append("')");
         	  sqlParams.add(dateTo);
        }
       	if (fvDateFrom!=null && !fvDateFrom.equals("")) {
             dynamicWhereClause.append(" and i.future_value_date >= TO_DATE(?,'").append(datePattern).append("')");
         	 sqlParams.add(fvDateFrom);
        }
       	if (fvDateTo!=null && !fvDateTo.equals("")) {
         	  dynamicWhereClause.append(" and i.future_value_date <= TO_DATE(?,'").append(datePattern).append("')");
         	  sqlParams.add(fvDateTo);
         }
       	if (StringFunction.isNotBlank(amountFrom)) {
            try {
            	
            	amountFrom  = NumberValidator.getNonInternationalizedValue(amountFrom.trim(),getResourceManager().getResourceLocale());
              	 dynamicWhereClause.append(" and i.total_amount >= ? ");
              	 sqlParams.add(amountFrom);
              	
            } catch (InvalidAttributeValueException e) {
         	   try {
    				medService.getErrorManager().issueError(
    						TradePortalConstants.ERR_CAT_1,
    						TradePortalConstants.INVALID_CURRENCY_FORMAT,
    						amountFrom);
    			} catch (AmsException e1) {
    				e1.printStackTrace();
    			}
            }
          }

          if (StringFunction.isNotBlank(amountTo)) {
            try {
            	amountTo  = NumberValidator.getNonInternationalizedValue(amountTo.trim(),getResourceManager().getResourceLocale());
            	dynamicWhereClause.append(" and i.total_amount <=?  ");
      			sqlParams.add(amountTo);
            } catch (InvalidAttributeValueException e) {
         	   try {
    				medService.getErrorManager().issueError(
    						TradePortalConstants.ERR_CAT_1,
    						TradePortalConstants.INVALID_CURRENCY_FORMAT,
    						amountTo);
    			} catch (AmsException e1) {
    				e1.printStackTrace();
    			}
            }

          }
        return dynamicWhereClause.toString();
    
    }

    protected int setDynamicSQLValues(PreparedStatement statement, int startVarIdx,
            Map<String,String> parameters)
            throws SQLException {
    	return setSqlVarsToPreparedStmt(statement, startVarIdx, sqlParams);
    }
}
