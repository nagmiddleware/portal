/**
 *
 */
package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;
import java.sql.PreparedStatement;
import java.util.Map;
import java.sql.SQLException;

import com.ams.tradeportal.common.TradePortalConstants;
import com.ams.tradeportal.common.Debug;
import com.ams.tradeportal.common.NumberValidator;
import com.ams.tradeportal.common.SQLParamFilter;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.InvalidAttributeValueException;
import com.amsinc.ecsg.frame.MediatorServices;
import com.amsinc.ecsg.util.StringFunction;

/**
 *
 *
 */
public class PayablesMgmtInvoicesDataView extends AbstractDBDataView {
private static final Logger LOG = LoggerFactory.getLogger(PayablesMgmtInvoicesDataView.class);
	/**
	 * Constructor
	 */
	
	public PayablesMgmtInvoicesDataView() {
		super();
	}

	protected String buildDynamicSQLCriteria(Map<String,String> parameters) {

		StringBuilder dynamicWhereClause = new StringBuilder();
		String userOrgOid = getUserSession().getOwnerOrgOid();
		MediatorServices medService = new MediatorServices();
		/* Params for search */
		String invoiceID = SQLParamFilter.filter(parameters.get("invoiceID"));
		String dateType = SQLParamFilter.filter(parameters.get("dateType"));
		String fromDate = SQLParamFilter.filter(parameters.get("fromDate"));
		String toDate = SQLParamFilter.filter(parameters.get("toDate"));
		String currency = SQLParamFilter.filter(parameters.get("currency"));
		String amountType = SQLParamFilter.filter(parameters.get("amountType"));
		String amountFrom = parameters.get("amountFrom");
		String amountTo = parameters.get("amountTo");
		String sellerIdentifier = SQLParamFilter.filter(parameters
				.get("sellerIdentifier"));
		String endToEndId =	parameters.get("endToEndId");	
		
		String datePattern = null;
		try {
			if (null != parameters.get("dPattern")) {
				datePattern = java.net.URLDecoder.decode(
						parameters.get("dPattern"), "UTF-8");
			} else {
				datePattern = "";
			}
		} catch (UnsupportedEncodingException e) {
			LOG.debug("Exceptiom in DateFormat Decoding : " + e);
			e.printStackTrace();
		}
		dynamicWhereClause.append(" and i.a_corp_org_oid = ");
		dynamicWhereClause.append(userOrgOid);
		// Search by the invoice ID
		if (StringFunction.isNotBlank(invoiceID)) {
			dynamicWhereClause.append(" and ");
			dynamicWhereClause.append(" upper(invoice_reference_id) like '%");
			dynamicWhereClause.append(SQLParamFilter.filter(invoiceID
					.toUpperCase()));
			dynamicWhereClause.append("%'");
		}

		// Search by date type
		if (StringFunction.isNotBlank(dateType)) {
			String expandedDateType = "";
			if (dateType.equals("DDT")) {
				expandedDateType = "invoice_due_date";
			} else if (dateType.equals("IDT")) {
				expandedDateType = "invoice_issue_datetime";
			} else if (dateType.equals("PDT")) {
				expandedDateType = "invoice_payment_date";
			}
			if (StringFunction.isNotBlank(fromDate)) {
				dynamicWhereClause.append(" and ");
				dynamicWhereClause.append(expandedDateType + " >= TO_DATE('");
				dynamicWhereClause.append(fromDate);
				dynamicWhereClause.append("','" + datePattern + "')");
			}

			if (StringFunction.isNotBlank(toDate)) {
				dynamicWhereClause.append(" and ");
				dynamicWhereClause.append(expandedDateType + " <= TO_DATE('");
				dynamicWhereClause.append(toDate);
				dynamicWhereClause.append("','" + datePattern + "')");
			}
		}

		// Search by currency
		if (StringFunction.isNotBlank(currency)) {
			dynamicWhereClause.append(" and ");
			dynamicWhereClause.append(" currency_code='" + currency + "'");
		}

		// Search by Amount
		if (StringFunction.isNotBlank(amountType)) {
			String expandedAmountType = "";
			if (amountType.equals("INA")) {
				expandedAmountType = "invoice_total_amount";
			} else if (amountType.equals("PYA")) {
				expandedAmountType = "invoice_payment_amount";
			}

			if (StringFunction.isNotBlank(amountFrom)) {
				amountFrom = amountFrom.trim();
				try {
					String amount = NumberValidator
							.getNonInternationalizedValue(amountFrom,
									getResourceManager().getResourceLocale());
					if (StringFunction.isNotBlank(amountFrom)) {
						dynamicWhereClause.append(" and ");
						dynamicWhereClause.append(expandedAmountType + " >= ");
						dynamicWhereClause.append(amount);

					}
				} catch (InvalidAttributeValueException e) {
					try {
						medService.getErrorManager().issueError(
								TradePortalConstants.ERR_CAT_1,
								TradePortalConstants.INVALID_CURRENCY_FORMAT,
								amountFrom);
					} catch (AmsException e1) {
						e1.printStackTrace();
					}
				}
			}

			if (StringFunction.isNotBlank(amountTo)) {
				amountTo = amountTo.trim();
				try {
					String amount = NumberValidator
							.getNonInternationalizedValue(amountTo,
									getResourceManager().getResourceLocale());
					if (StringFunction.isNotBlank(amountTo)) {
						dynamicWhereClause.append(" and ");
						dynamicWhereClause.append(expandedAmountType + " <= ");
						dynamicWhereClause.append(amount);

					}
				} catch (InvalidAttributeValueException e) {
					try {
						medService.getErrorManager().issueError(
								TradePortalConstants.ERR_CAT_1,
								TradePortalConstants.INVALID_CURRENCY_FORMAT,
								amountTo);
					} catch (AmsException e1) {
						e1.printStackTrace();
					}
				}
			}
		}

		if (StringFunction.isNotBlank(sellerIdentifier)) {
			sellerIdentifier = sellerIdentifier.trim();
			dynamicWhereClause.append(" and ");
			dynamicWhereClause.append(" upper(seller_party_identifier) like '%");
			dynamicWhereClause.append(SQLParamFilter.filter(sellerIdentifier
					.toUpperCase()));
			dynamicWhereClause.append("%'");
		}
		//Srinivasu_d IR#39318 Rel9.3 05/12/2015 - End to End Id filtering
		if(StringFunction.isNotBlank(endToEndId)){
			endToEndId = endToEndId.trim();
			dynamicWhereClause.append(" and ");
			dynamicWhereClause.append(" upper(end_to_end_id) like '%");
			dynamicWhereClause.append(endToEndId.toUpperCase());
			dynamicWhereClause.append("%'");
		}
		//Srinivasu_d IR#39318 Rel9.3 05/12/2015 -End
		return dynamicWhereClause.toString();
	}

	protected int setDynamicSQLValues(PreparedStatement statement,
			int startVarIdx, Map<String,String> parameters)
			throws SQLException {
		int varCount = 0;

		return varCount;
	}
}
