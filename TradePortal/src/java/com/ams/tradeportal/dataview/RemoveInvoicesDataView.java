/**
 * 
 */
package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import java.io.UnsupportedEncodingException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;
import java.util.List;

import com.ams.tradeportal.common.Debug;
import com.amsinc.ecsg.util.StringFunction;


/**
 * 
 *
 */
public class RemoveInvoicesDataView    extends AbstractDBDataView {
private static final Logger LOG = LoggerFactory.getLogger(RemoveInvoicesDataView.class);
	private final List<Object> sqlParams = new ArrayList<>();
	/**
	 * Constructor
	 */
	public RemoveInvoicesDataView() {
		super();
	}

    protected String buildDynamicSQLCriteria(Map<String,String> parameters) {
    	
    	StringBuilder dynamicWhereClause = new StringBuilder();
        
        String userOrgOid = getUserSession().getOwnerOrgOid();
        String beneName = parameters.get("beneName");
        String invID = parameters.get("InvID");
        String currency = parameters.get("Currency");
        String amountFrom = parameters.get("AmountFrom");
        String amountTo = parameters.get("AmountTo");
        String fromDate = parameters.get("DateFrom");
        String toDate = parameters.get("DateTo");
        String tranOid = parameters.get("tranOid");

        String datePattern = null;
        try{
        	if(null != parameters.get("dPattern")){
        		datePattern = java.net.URLDecoder.decode(parameters.get("dPattern"), "UTF-8");
        	}else{
        		datePattern = "";
        	}
        }catch (UnsupportedEncodingException e) {
			LOG.debug("Exceptiom in DateFormat Decoding : "+e);
		}
        
        dynamicWhereClause.append(" where deleted_ind != ? ");
        sqlParams.add("Y");
        
        dynamicWhereClause.append(" and a_corp_org_oid = ? ");
    	sqlParams.add(userOrgOid);

    	dynamicWhereClause.append(" and a_transaction_oid = ? ");
    	sqlParams.add(tranOid);
    	
    	
    	if(StringFunction.isNotBlank(invID) && "null"!=invID){
			dynamicWhereClause.append(" and upper(invoice_id) like ? ");
			sqlParams.add(invID.toUpperCase()+"%");
		}
		if(StringFunction.isNotBlank(currency) && "null"!=currency){
			dynamicWhereClause.append(" and upper(currency) = ? ");
			sqlParams.add(currency);
		}
		if(StringFunction.isNotBlank(beneName) && "null"!=beneName){
			dynamicWhereClause.append(" and upper(seller_name) like ");
			sqlParams.add(beneName.toUpperCase()+"%");
		}
		if(StringFunction.isNotBlank(amountFrom) && StringFunction.isNotBlank(amountTo)){
			dynamicWhereClause.append(" and amount between ? and ? ");
			sqlParams.add(amountFrom);
			sqlParams.add(amountTo);
		}
		
		if (StringFunction.isNotBlank(fromDate)){
	    	 if(StringFunction.isNotBlank(toDate)) {
	    		dynamicWhereClause.append(" and ((");
	            dynamicWhereClause.append("due_date" + " >= TO_DATE(?,'"+datePattern+"')");
	              
	         	dynamicWhereClause.append(" and ");
	         	dynamicWhereClause.append("due_date" + " <= TO_DATE(?,'"+datePattern+"'))");
	         	dynamicWhereClause.append("or (");
	         	dynamicWhereClause.append("payment_date" + " >= TO_DATE(?,'"+datePattern+"')");
	              
	         	dynamicWhereClause.append(" and ");
	         	dynamicWhereClause.append("payment_date" + " <= TO_DATE(?,'"+datePattern+"')))");
	         	
	         	sqlParams.add(fromDate);
	         	sqlParams.add(toDate);
	         	sqlParams.add(fromDate);
	         	sqlParams.add(toDate);
	         }
	    	 else{
		    	 dynamicWhereClause.append(" and (");
		    	 dynamicWhereClause.append("due_date" + " >= TO_DATE(?,'"+datePattern+"')");
		    	 dynamicWhereClause.append("or ");
		    	 dynamicWhereClause.append("payment_date" + " >= TO_DATE(?,'"+datePattern+"')");
		         
		         sqlParams.add(fromDate);
		         sqlParams.add(fromDate);
		    	   
	    	 }
		}
		else if(StringFunction.isNotBlank(toDate)) {
	    	  dynamicWhereClause.append(" and (");
	    	  dynamicWhereClause.append("due_date" + " <= TO_DATE(?,'"+datePattern+"'))");
		      dynamicWhereClause.append("or ");
		      dynamicWhereClause.append("payment_date" + " <= TO_DATE(?,'"+datePattern+"')))");
	         	
	          sqlParams.add(toDate);
	          sqlParams.add(toDate);
	      }
    		
		  dynamicWhereClause.append(" group by invoice_id,seller_name, currency,amount,issue_date,creation_date_time,due_date,payment_date,upload_invoice_oid"); 
       	
    	return dynamicWhereClause.toString();
    }

    protected int setDynamicSQLValues(PreparedStatement statement, int startVarIdx, 
            Map<String,String> parameters) 
            throws SQLException {
        
    	return setSqlVarsToPreparedStmt(statement, startVarIdx, sqlParams);
    }
}
