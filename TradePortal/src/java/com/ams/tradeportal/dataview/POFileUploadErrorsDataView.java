/**
 *
 */
package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Map;
/**
 *
 *
 */
public class POFileUploadErrorsDataView
    extends AbstractDBDataView
{
private static final Logger LOG = LoggerFactory.getLogger(POFileUploadErrorsDataView.class);
	/**
	 * Constructor
	 */
	public POFileUploadErrorsDataView() {
		super();
	}

    protected String buildDynamicSQLCriteria(Map<String,String> parameters) {
        return " and p_po_file_upload_oid = ? ";
    }

    protected int setDynamicSQLValues(PreparedStatement statement, int startVarIdx,
            Map<String,String> parameters)
            throws SQLException {
        int varCount = 0;
      //jgadela - 10/13/2014 - Rel 9.1 IR#T36000026319 - SQL Injection fix 
        statement.setString(startVarIdx+varCount, parameters.get("po_file_upload_oid"));
        varCount++;

        return varCount;
    }
}
