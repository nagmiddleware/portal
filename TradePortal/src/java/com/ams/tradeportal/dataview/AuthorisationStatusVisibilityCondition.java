/*CR-857 Prateep New Column based on corporate customer setting*/
package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.busobj.webbean.SessionWebBean;
import com.ams.tradeportal.busobj.webbean.TransactionWebBean;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.util.ResourceManager;
import com.amsinc.ecsg.web.BeanManager;

public class AuthorisationStatusVisibilityCondition extends AbstractVisibilityCondition {
private static final Logger LOG = LoggerFactory.getLogger(AuthorisationStatusVisibilityCondition.class);

	@Override
	public boolean execute(SessionWebBean userSession, BeanManager beanMgr, ResourceManager resMgr) {
		boolean visible=false;

		TransactionWebBean transaction  = (TransactionWebBean)beanMgr.getBean("Transaction");
		if (TradePortalConstants.INDICATOR_YES.equals(transaction.getAttribute("bene_panel_auth_ind"))) {
			visible=true;
		}

		return visible;
	}
	
}
