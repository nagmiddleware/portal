/**
 *
 */
package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

/**
 *Created this class to maintain dynamic SQL on the invoice pages.
 *This will be used in Print functionality on invoice pages.
 *
 */
   
public class InvoiceDataViewUtil {
private static final Logger LOG = LoggerFactory.getLogger(InvoiceDataViewUtil.class);
	private Map map = new HashMap();

	public Map getMap() {
		return map;
	}

	static InvoiceDataViewUtil obj = new InvoiceDataViewUtil();

	public void setMap(Map map) {
		this.map = map;
	}

	private InvoiceDataViewUtil() {

	}

	public static InvoiceDataViewUtil getMoreSQL() {
		if (obj == null) {
			obj = new InvoiceDataViewUtil();
		}
		return obj;
	}

}
