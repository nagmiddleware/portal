/**
 *
 */
package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Map;
import java.util.List;
import java.util.ArrayList;

import com.ams.tradeportal.common.Debug;
import com.ams.tradeportal.common.NumberValidator;
import com.ams.tradeportal.common.SQLParamFilter;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.EncryptDecrypt;

/**
 *
 *
 */
public class DirectDebitHistoryDataView extends AbstractDBDataView {
private static final Logger LOG = LoggerFactory.getLogger(DirectDebitHistoryDataView.class);

    private final List<Object> sqlParams = new ArrayList<>();

    static final String ALL_ORGS = "AuthorizedTransactions.AllOrganizations";

    /**
     * Constructor
     */
    public DirectDebitHistoryDataView() {
        super();
    }

    @Override
    protected String buildDynamicSQLCriteria(Map<String,String> parameters) {
        StringBuilder sql = new StringBuilder();

        //cquinton 3/26/2013 Rel PR ir#15207 changes for security

        String userOrgOid = getUserSession().getOwnerOrgOid();
        String loginLocale = getUserSession().getUserLocale();

        String encryptedSelectedOrg = parameters.get("selectedOrg");
        if (encryptedSelectedOrg != null && encryptedSelectedOrg.length() > 0) {
            String selectedOrg = EncryptDecrypt.decryptStringUsingTripleDes(
                    encryptedSelectedOrg, getUserSession().getSecretKey());

            if (ALL_ORGS.equals(selectedOrg)) {
                StringBuilder sqlQuery = new StringBuilder();
                sqlQuery.append("select organization_oid, name");
                sqlQuery.append(" from corporate_org");
                sqlQuery.append(" where activation_status = ? start with organization_oid = ? connect by prior organization_oid = p_parent_corp_org_oid");
                sqlQuery.append(" order by ");
                sqlQuery.append(getResourceManager().localizeOrderBy("name"));

                DocumentHandler hierarchyDoc = null;
                try {
                    hierarchyDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery.toString(), false, TradePortalConstants.ACTIVE, userOrgOid);
                } catch (AmsException e) {
                    LOG.debug("Exception caught in DirectDebitHistoryDataView.java while calling Database Query Bean. This exception is cirrently not caught.");
                }

                StringBuilder orgList = new StringBuilder();
                List<DocumentHandler> orgListDoc = hierarchyDoc.getFragmentsList("/ResultSetRecord");
                int totalOrganizations = orgListDoc.size();

                for (int i = 0; i < totalOrganizations; i++) {
                    DocumentHandler orgDoc = orgListDoc.get(i);
                    orgList.append("?");
                    sqlParams.add(orgDoc.getAttribute("ORGANIZATION_OID"));
                    if (i < (totalOrganizations - 1)) {
                        orgList.append(", ");
                    }
                }
                // Build a comma separated list of the orgs
                sql.append(" and i.a_corp_org_oid in (" + orgList.toString() + ")");
            } else {
                sql.append(" and i.a_corp_org_oid = ? ");
                sqlParams.add(selectedOrg);
            }
        } else {
            //selectedOrg is required
            sql.append(" and 1=0");
        }

        try {

            String inactive = parameters.get("inactive");
            String active = parameters.get("active");
            String instrumentId = parameters.get("InstrumentId");
            String creditAcct = parameters.get("CreditAcct");
            String refNum = parameters.get("Reference");
            String filterType = parameters.get("searchType");
            String fromDate = parameters.get("fromDate");
            String toDate = parameters.get("toDate");
            String fromAmount = parameters.get("fromAmount");
            String toAmount = parameters.get("toAmount");

            String datePattern = null;
            try {
                if (null != parameters.get("dPattern")) {
                    datePattern = java.net.URLDecoder.decode(parameters.get("dPattern"), "UTF-8");
                } else {
                    datePattern = "";
                }
            } catch (UnsupportedEncodingException e) {
                // TODO Auto-generated catch block
                LOG.debug("Exceptiom in DateFormat Decoding : " + e);
            }

            if (active.equalsIgnoreCase("true") && inactive.equalsIgnoreCase("true")) {
                // do nothing
            } else if (inactive.equalsIgnoreCase("true")) {
                sql.append(" and i.instrument_status in ('CAN', 'CLO', 'DEA', 'LIQ', 'LQB', 'LQC')");
            } else if (active.equalsIgnoreCase("true")) {
                sql.append(" and i.instrument_status in ('ACT', 'PND', 'EXP')");
            } else if (active.equalsIgnoreCase("false") && inactive.equalsIgnoreCase("false")) {//added else if for IR5161
                sql.append(" and i.instrument_status not in ('ACT', 'PND', 'EXP','CAN', 'CLO', 'DEA', 'LIQ', 'LQB', 'LQC')");
            }

            StringBuilder addlSearch = new StringBuilder();
            boolean appendSql = false;

            if (filterType.equalsIgnoreCase("S")) {

                boolean appendOR = false;
                addlSearch.append("AND ( ");
                // basic search; use OR condition
                if (instrumentId != null && instrumentId.trim().length() > 0) {
                    addlSearch.append("upper(i.complete_instrument_id) like ? "); //"%'" is safe for URLdecoding.
                    appendOR = true;
                    appendSql = true;
                    sqlParams.add("%" + instrumentId.toUpperCase() + "%");
                }
                if (creditAcct != null && creditAcct.trim().length() > 0) {
                    if (appendOR) {
                        addlSearch.append(" OR ");
                    }
                    //Jyoti modified for IR5161 starts
                    addlSearch.append("a.ACCOUNT_OID in (?)");
                    //IR5161 ends
                    appendOR = true;
                    appendSql = true;
                    sqlParams.add(creditAcct);
                }
                if (refNum != null && refNum.trim().length() > 0) {
                    if (appendOR) {
                        addlSearch.append(" OR ");
                    }
                    addlSearch.append("upper(i.copy_of_ref_num) like ? ");
                    appendSql = true;
                    sqlParams.add("%" + refNum.toUpperCase() + "%");
                }
                addlSearch.append(" ) ");

            } else {

                addlSearch.append(" AND ");
                boolean appendAND = false;
                // advanced search; AND all the search criteria
                if (instrumentId != null && instrumentId.trim().length() > 0) {
                    addlSearch.append("upper(i.complete_instrument_id) like ? "); //"%'" is safe for URLdecoding.
                    appendAND = true;
                    appendSql = true;
                    sqlParams.add("%" + instrumentId.toUpperCase() + "%");
                }
                if (creditAcct != null && creditAcct.trim().length() > 0) {
                    if (appendAND) {
                        addlSearch.append(" AND ");
                    }
                    //Jyoti modified for IR5161 starts
                    addlSearch.append("a.ACCOUNT_OID in ( ?)");
                    //IR5161 Ends
                    appendAND = true;
                    appendSql = true;
                    sqlParams.add(creditAcct);

                }
                if (refNum != null && refNum.trim().length() > 0) {
                    if (appendAND) {
                        addlSearch.append(" AND ");
                    }
                    addlSearch.append("upper(i.copy_of_ref_num) like ? ");
                    appendAND = true;
                    appendSql = true;
                    sqlParams.add("%" + refNum.toUpperCase() + "%");
                }
                if (fromAmount != null && fromAmount.trim().length() > 0) {
                    if (appendAND) {
                        addlSearch.append(" AND ");
                    }
                    addlSearch.append(" i.copy_of_instrument_amount >= ? ");
                    sqlParams.add(NumberValidator.getNonInternationalizedValue(fromAmount,
                            loginLocale));

                    appendAND = true;
                    appendSql = true;
                }
                if (toAmount != null && toAmount.trim().length() > 0) {
                    if (appendAND) {
                        addlSearch.append(" AND ");
                    }
                    addlSearch.append(" i.copy_of_instrument_amount <=? ");
                    sqlParams.add(NumberValidator.getNonInternationalizedValue(toAmount,
                            loginLocale));
                    appendAND = true;
                    appendSql = true;
                }
                if (fromDate != null && fromDate.trim().length() > 0) {
                    if (appendAND) {
                        addlSearch.append(" AND ");
                    }
                    addlSearch.append(" o.payment_date >=  to_date(?,'" + SQLParamFilter.filter(datePattern) + "')");
                    sqlParams.add(fromDate);
                    appendAND = true;
                    appendSql = true;
                }
                if (toDate != null && toDate.trim().length() > 0) {
                    if (appendAND) {
                        addlSearch.append(" AND ");
                    }
                    addlSearch.append(" o.payment_date <= to_date(?,'" + SQLParamFilter.filter(datePattern) + "')");
                    sqlParams.add(toDate);
                    appendSql = true;
                }

            }

            if (appendSql) {
                sql.append(addlSearch);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return sql.toString();
    }

    @Override
    protected int setDynamicSQLValues(PreparedStatement statement,
            int startVarIdx, Map<String,String> parameters)
            throws SQLException {

        return setSqlVarsToPreparedStmt(statement, startVarIdx, sqlParams);
    }

}
