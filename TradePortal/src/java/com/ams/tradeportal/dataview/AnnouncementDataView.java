/**
 *
 */
package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Map;

import com.ams.tradeportal.common.TradePortalConstants;

/**
 *
 *
 */
public class AnnouncementDataView extends AbstractDBDataView {
private static final Logger LOG = LoggerFactory.getLogger(AnnouncementDataView.class);

    /**
     * Constructor
     */
    public AnnouncementDataView() {
        super();
    }

    @Override
    protected String buildDynamicSQLCriteria(Map<String, String> parameters) {

        StringBuilder sql = new StringBuilder();

        String ownershipLevel = this.getOwnershipLevel();

        if (TradePortalConstants.OWNER_BANK.equals(ownershipLevel)) {
            sql.append("where p_owner_org_oid = ?"); //ownerOrgOid
        } else if (ownershipLevel.equals(TradePortalConstants.OWNER_BOG)) {
            sql.append("where (p_owner_org_oid = ?"); //ownerOrgOid
            sql.append(" or (p_owner_org_oid = ?"); //clientBankOid
            sql.append(" and (all_bogs_ind='Y'");
            sql.append(" or exists (select 1 from announcement_bank_group");
            sql.append(" where p_announcement_oid=a.announcement_oid");
            sql.append(" and a_bog_oid = ?))))"); //ownerOrgOid            
        } else if (ownershipLevel.equals(TradePortalConstants.OWNER_GLOBAL)) {
            // ASP user sees all;
        } else {
            // corporate user sees nothing
            sql.append("where 1=0 ");
        }

        return sql.toString();
    }

    @Override
    protected int setDynamicSQLValues(PreparedStatement statement, int startVarIdx,
            Map<String, String> parameters)
            throws SQLException {
        int varCount = 0;

        String ownershipLevel = this.getOwnershipLevel();

        if (TradePortalConstants.OWNER_BANK.equals(ownershipLevel)) {
            statement.setString(startVarIdx + varCount, this.getOwnerOrgOid());
            varCount++;
        } else if (ownershipLevel.equals(TradePortalConstants.OWNER_BOG)) {
            statement.setString(startVarIdx + varCount, this.getOwnerOrgOid());
            varCount++;
            statement.setString(startVarIdx + varCount, this.getClientBankOid());
            varCount++;
            statement.setString(startVarIdx + varCount, this.getOwnerOrgOid());
            varCount++;
        }

        return varCount;
    }
}
