/**
 *
 */
package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;
import java.util.List;

import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.util.EncryptDecrypt;

/**
 *
 *
 */
public class CashMgmtFutureValueDataView extends AbstractDBDataView
{
private static final Logger LOG = LoggerFactory.getLogger(CashMgmtFutureValueDataView.class);
	private final List<Object> sqlParams = new ArrayList<>();
	final static String ALL_WORK = "common.allWork";
    final static String MY_WORK = "common.myWork";
	
    /**
	 * Constructor
	 */
	public CashMgmtFutureValueDataView() {
		super();
	}

    protected String buildDynamicSQLCriteria(Map<String,String> parameters) {

    	StringBuilder dynamicWhereClause     = new StringBuilder();

    	String workflow = parameters.get("workflow");
    	String userOid = getUserSession().getUserOid();
    	String userOrgOid = getUserSession().getOwnerOrgOid();
    	 
    	
    	String confInd  = this.getConfidentialInd();
    	  
    	workflow = EncryptDecrypt.decryptStringUsingTripleDes(workflow, getUserSession().getSecretKey());

    	if (MY_WORK.equalsIgnoreCase(workflow))
        {
           dynamicWhereClause.append("and a.a_assigned_to_user_oid = ? ");
           sqlParams.add(userOid);
        }
        else if (ALL_WORK.equalsIgnoreCase(workflow))
        {
           dynamicWhereClause.append("and b.a_corp_org_oid in (");
           dynamicWhereClause.append(" select organization_oid");
           dynamicWhereClause.append(" from corporate_org");
           dynamicWhereClause.append(" where activation_status = ? start with organization_oid = ? ");
           dynamicWhereClause.append(" connect by prior organization_oid = p_parent_corp_org_oid)");
           sqlParams.add(TradePortalConstants.ACTIVE);
           sqlParams.add(userOrgOid);
        }
        else
        {
           dynamicWhereClause.append("and b.a_corp_org_oid = ? ");
           sqlParams.add(workflow);
        }


        if (confInd.equalsIgnoreCase(TradePortalConstants.INDICATOR_NO))
        {
         	 dynamicWhereClause.append(" and ( t.confidential_indicator = 'N' OR ");
         	 dynamicWhereClause.append(" t.confidential_indicator is null) ");
        }

       
        return dynamicWhereClause.toString();

    }

    protected int setDynamicSQLValues(PreparedStatement statement, int startVarIdx,
            Map<String,String> parameters)
            throws SQLException {
    	return setSqlVarsToPreparedStmt(statement, startVarIdx, sqlParams);
    }
}
