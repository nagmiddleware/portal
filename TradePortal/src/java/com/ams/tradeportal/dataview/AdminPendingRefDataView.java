/**
 *
 */
package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;
import java.util.List;

import com.ams.tradeportal.busobj.util.SecurityAccess;
import com.ams.tradeportal.busobj.webbean.SecurityProfileWebBean;
import com.ams.tradeportal.busobj.webbean.SessionWebBean;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.web.BeanManager;

/**
 *
 *
 */
public class AdminPendingRefDataView extends AbstractDBDataView {
private static final Logger LOG = LoggerFactory.getLogger(AdminPendingRefDataView.class);
	private final List<Object> sqlParams = new ArrayList<>();
	/**
	 * Constructor
	 */
	public AdminPendingRefDataView() {
		super();
	}

	protected String buildDynamicSQLCriteria(Map<String,String> parameters) {

		StringBuilder dynamicWhere = new StringBuilder("");

		String loginRights = null;
		//String adminUserIDsSql = null;
		boolean showNothing = true;
		String userSecurityType = null;

		SessionWebBean userSession = getUserSession().getSavedUserSession();

		if (userSession == null) {
			userSession = getUserSession();
			userSecurityType = getUserSession().getSecurityType();
		} else {
			userSecurityType = getUserSession()
					.getSavedUserSessionSecurityType();
		}
		String clientBankID = userSession.getClientBankOid();
		loginRights = userSession.getSecurityRights();
		String loggedInUser = userSession.getUserOid();

		String custAccesssecurity_right = "";
		BeanManager beanMgr = getBeanManager();
		SecurityProfileWebBean custAccessSecurityProfile = beanMgr.createBean(SecurityProfileWebBean.class, "SecurityProfile");
		custAccessSecurityProfile.getById(userSession
				.getCustomerAccessSecurityProfileOid());
		custAccesssecurity_right = custAccessSecurityProfile
				.getAttribute("security_rights");

		if (userSecurityType.equals(TradePortalConstants.ADMIN)) {
			//logged in user should see own changes

			if(!TradePortalConstants.OWNER_GLOBAL.equals(userSession.getOwnershipLevel())){
				dynamicWhere.append(" and (u.a_client_bank_oid is null or u.a_client_bank_oid = ? )");// this will be client_bank oid when we login as BANKADMIN
				sqlParams.add(clientBankID);
			}
		
				sqlParams.add(loggedInUser);
				dynamicWhere.append("and ((a.CHANGED_OBJECT_OID != ? and " + "(CHANGE_USER_OID in (" + getAdminUserObjectsSql(parameters, sqlParams)
						+ ") and CLASS_NAME in ('User')) ");
				


				if (SecurityAccess.hasRights(getUserSession()
						.getSecurityRights(), SecurityAccess.MAINTAIN_USERS)
						//even if its admin we need to show corporate user changes
					
								) {
					dynamicWhere.append("and a.ownership_level = ? ");
					sqlParams.add("CORPORATE");
				}
				//even changes made at corporate user level should be displayed

				dynamicWhere.append(" )");
				showNothing = false;
			

			if (SecurityAccess.hasRights(custAccesssecurity_right,
					SecurityAccess.MAINTAIN_PANEL_AUTH_GRP)) {

				if (showNothing) {
					dynamicWhere
							.append("and ( (CHANGE_USER_OID in ("
									+ getAdminUserObjectsSql(parameters, sqlParams)
									+ ") and CLASS_NAME in (?) " +
											")");
					sqlParams.add("PanelAuthorizationGroup");
				} else {
					dynamicWhere
							.append("or (CHANGE_USER_OID in ("
									+ getAdminUserObjectsSql(parameters, sqlParams)
									+ ") and CLASS_NAME in (?) " +
											")");
					sqlParams.add("PanelAuthorizationGroup");
				}

				showNothing = false;
			}

			if (showNothing) {
				dynamicWhere.append("and 1=0");
			} else {
				dynamicWhere.append(" )");

			}

		}

		return dynamicWhere.toString();
	}

	protected int setDynamicSQLValues(PreparedStatement statement,
			int startVarIdx, Map<String,String> parameters)
			throws SQLException {
		return setSqlVarsToPreparedStmt(statement, startVarIdx, sqlParams);
	}

	private String getAdminUserObjectsSql(Map<String,String> parameters, List sqlParams) {

		StringBuilder sql = new StringBuilder("");
		SessionWebBean userSession = getUserSession().getSavedUserSession();

		if (userSession == null) {
			userSession = getUserSession();
		}

		String bogID = userSession.getBogOid();
		String clientBankID = userSession.getClientBankOid();
		String ownershipLevel = userSession.getOwnershipLevel();

		// The User who made the corporate level changes can be 
		sql.append("select a.user_oid from users a, org_names_view org where activation_status = ?  and a.p_owner_org_oid=org.organization_oid ");
		sqlParams.add("ACTIVE");
		
		//If ASP login then all the corporate level changes should be visible
		if (ownershipLevel.equals(TradePortalConstants.OWNER_GLOBAL)) {
			sql.append("and (a.ownership_level = ? or a.ownership_level = ? or a.ownership_level = ?)");
			sqlParams.add(TradePortalConstants.OWNER_CORPORATE);
			sqlParams.add(TradePortalConstants.OWNER_GLOBAL);
			sqlParams.add(TradePortalConstants.OWNER_BANK);
		} 
		//If BANK ADMIN login then the corporate level changes mad by BANK ADMIN/BOG/CORPORATE USER should be visible
		else if (ownershipLevel.equals(TradePortalConstants.OWNER_BANK)) {
			sql.append(" and a.a_client_bank_oid = ? and (a.ownership_level = ? or a.ownership_level = ? or a.ownership_level = ?)");
			sqlParams.add(clientBankID);
			sqlParams.add(TradePortalConstants.OWNER_CORPORATE);
			sqlParams.add(TradePortalConstants.OWNER_BANK);
			sqlParams.add(TradePortalConstants.OWNER_BOG);
		} 
		//should not come here as only ASP/BANK Admin can invoke this dataview
		else {
			sql.append(" and a.p_owner_org_oid = ? and a.ownership_level = ? ");
			sqlParams.add(bogID);
			sqlParams.add(ownershipLevel);
		}
		return sql.toString();
	}

	

	protected String getLinkAction(DataViewManager.DataViewColumn column)
			throws AmsException {

		String linkAction = "";

		String refDataType = (String) getDataValue("DataType");
		String userType = (String) getDataValue("OwnershipLevel");

		if ("User".equalsIgnoreCase(refDataType)
				&& ("BANK".equalsIgnoreCase(userType) || "BOG"
						.equalsIgnoreCase(userType))) {
			linkAction = "selectAdminUser";
		} else if ("CorporateOrganization".equalsIgnoreCase(refDataType)) {
			linkAction = "selectCorporateCustomer";

		} else if ("User".equalsIgnoreCase(refDataType)
				&& "CORPORATE".equalsIgnoreCase(userType)) {
			linkAction = "selectUser";
		} else if ("PanelAuthorizationGroup".equalsIgnoreCase(refDataType)) {
			linkAction = "selectPanelGroup";
		}

		return linkAction;
	}
	
	/**
	 * @return
	 */
	protected String getAdditionalURLParm() {
		return "&fromAction=APPROVE";

	}
}
