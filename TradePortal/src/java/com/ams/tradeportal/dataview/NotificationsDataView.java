/**
 *
 */
package com.ams.tradeportal.dataview;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.common.SQLParamFilter;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.util.EncryptDecrypt;
import com.amsinc.ecsg.util.StringFunction;

/**
 *
 *
 */
public class NotificationsDataView extends AbstractDBDataView {
private static final Logger LOG = LoggerFactory.getLogger(NotificationsDataView.class);

    private final List<Object> sqlParams = new ArrayList<>();
    //some options for the org and status search parameters
    final static String ALL_NOTIFS = "Notifications.All";
	final static String ALL_NOTIFS_RUN = "Notifications.AllRUN";

    /**
     * Constructor
     */
    public NotificationsDataView() {
        super();
    }

    @Override
    protected String buildDynamicSQLCriteria(Map<String,String> parameters) {

        StringBuilder sql = new StringBuilder();
        StringBuilder dynamicWhereClause = new StringBuilder();

        String where = "";
        String userOrgOid = getUserSession().getOwnerOrgOid();
        String currentOrgOption = parameters.get("currentOrgOption");
        String confInd = getConfidentialInd();

        String status = SQLParamFilter.filter(parameters.get("status"));
        String unreadFlag = SQLParamFilter.filter(parameters.get("UnreadFlag"));

        if (currentOrgOption == null) {
            currentOrgOption = "";
        }
        if (confInd == null) {
            confInd = "";
        }
        if (status == null) {
            status = "";
        }

        //cquinton 4/9/2013 Rel PR ir#15704 default to no org
        if (currentOrgOption != null && currentOrgOption.length() > 0) {

            currentOrgOption = EncryptDecrypt.decryptStringUsingTripleDes(currentOrgOption, getUserSession().getSecretKey());

            //W Zhu 10/24/12 T36000006807 #333 set where clause for null parameter to prevent unauthorized access
            if (NotificationsDataView.ALL_NOTIFS.equals(currentOrgOption)) {
                where = " and b.a_corp_org_oid in (select organization_oid "
                        + " from corporate_org where activation_status = 'ACTIVE' "
                        + " start with organization_oid = ? "
                        + " connect by prior organization_oid = p_parent_corp_org_oid)";
                sqlParams.add(userOrgOid);
            } else {
                where = " and b.a_corp_org_oid = ? ";
                sqlParams.add(currentOrgOption);
            }
        } else {
            //currentorgoption is required
            where = " and 1=0";
        }

        if (!"".equals(confInd)) {
            if (TradePortalConstants.INDICATOR_NO.equals(confInd)) {
                //Use INSTRUMENT table with Notification List View for Conf Indicator Check
                String tableNameAlias = "b";
                dynamicWhereClause.append(" and (").append(tableNameAlias).append(".confidential_indicator = 'N' OR ");
                dynamicWhereClause.append(tableNameAlias).append(".confidential_indicator is null) ");
            }
        }
        
        if (!"".equals(status) && !status.equals(NotificationsDataView.ALL_NOTIFS)) {
            dynamicWhereClause.append(" and a.transaction_status in (?)");
            sqlParams.add(status);
        }
      //Added by Jaya Mamidala - CR49930 -  Start
		if (StringFunction.isNotBlank(unreadFlag) && !ALL_NOTIFS_RUN.equals(unreadFlag))
        {
        	if("N".equals(unreadFlag))
        	{
                 dynamicWhereClause.append(" and a.unread_flag = ?");
        	}
        	else 
        	{
        		dynamicWhereClause.append(" and (a.unread_flag is null or a.unread_flag <> ?)");
        	}
            sqlParams.add("N");
        }
       //Jaya Mamidala - CR49930 -  END
        where = where + dynamicWhereClause.toString();
        sql.append(where);
        return sql.toString();
    }

    @Override
    protected int setDynamicSQLValues(PreparedStatement statement, int startVarIdx,
            Map<String,String> parameters)
            throws SQLException {

        return setSqlVarsToPreparedStmt(statement, startVarIdx, sqlParams);
    }

    public String buildQuery(DataViewManager.DataViewMetadata metadata,
            String dynamicWhereClause ) {

    	StringBuilder builtSQL = new StringBuilder(super.buildQuery(metadata, dynamicWhereClause));
    	if (metadata.columns[sortColumn].columnKey.equalsIgnoreCase("Amount")){
    	String a = "decode(b.instrument_type_code/*not_column_name*/, 'BIL'/*not_column_name*/, null/*not_column_name*/, a.copy_of_amount/*not_column_name*/) ";
    	builtSQL.replace(builtSQL.indexOf(a),builtSQL.indexOf(a)+a.length()," a.copy_of_amount ");
    	a = a.replace("b.instrument_type_code", "InstrumentType");
    	a = a.replace("a.copy_of_amount", "Amount");
    	builtSQL.replace(builtSQL.lastIndexOf("Amount, Status"),builtSQL.lastIndexOf("Amount, Status")+6,a + " AS Amount " );
    	}

    	return builtSQL.toString();
    }
}
