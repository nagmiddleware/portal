package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.common.TradePortalConstants;

/**
 * 
 */
public class RequestFXRateFormatter extends AbstractCustomDataViewColumnFormatter {
private static final Logger LOG = LoggerFactory.getLogger(RequestFXRateFormatter.class);

    /**
	 * Format data.
	 */
    public String format(Object obj) {
        String formattedData = ""; //default

        String value = (String) obj;


        String REQUEST = "1";
        if (REQUEST.equals(value)) {
            formattedData = getResourceManager().getText("common.Request",TradePortalConstants.TEXT_BUNDLE);       
        }

        return formattedData;
        
        
    }
}
