/**
 *
 */
package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Map;

/**

 *
 */
public class ThresholdGroupDataView
    extends AbstractDBDataView {
private static final Logger LOG = LoggerFactory.getLogger(ThresholdGroupDataView.class);

	/**
	 * Constructor
	 */
	public ThresholdGroupDataView() {
		super();
	}

    protected String buildDynamicSQLCriteria(Map<String,String> parameters) {
    	//jgadela - 07/15/2014 - Rel 9.1 IR#T36000026319 - SQL Injection fix
    	String sql = null;

         if (parameters.get("pOid") != null) {
       	   sql = " where p_corp_org_oid = ?";
         }
		 return sql;
    }

    protected int setDynamicSQLValues(PreparedStatement statement, int startVarIdx,
            Map<String,String> parameters)
            throws SQLException {
    	//jgadela - 07/15/2014 - Rel 9.1 IR#T36000026319 - SQL Injection fix
    	int varCount = 0;
        String oid = parameters.get("pOid");
        if (oid != null) {
                statement.setString(startVarIdx+varCount, oid);
                varCount++;
        }

        return varCount;
    }
}
