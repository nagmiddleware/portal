/**
 *
 */
package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;
import java.util.List;

import com.ams.tradeportal.common.TradePortalConstants;
/**
 *
 *
 */
public class LockedUsersDataView     extends AbstractDBDataView
{
private static final Logger LOG = LoggerFactory.getLogger(LockedUsersDataView.class);
	
	private final List<Object> sqlParams = new ArrayList<>();
	/**
	 * Constructor
	 */
	public final static String INVOICE_SEARCH_STATUS_CLOSED    = "CLOSED";
	public LockedUsersDataView() {

		super();
	}

    protected String buildDynamicSQLCriteria(Map<String,String> parameters) {

    	StringBuilder      dynamicWhereClause     = new StringBuilder();

        String clientBankID = getUserSession().getClientBankOid();
        String ownershipLevel = getUserSession().getOwnershipLevel();

    	if (ownershipLevel.equals(TradePortalConstants.OWNER_GLOBAL)) {
    	       // Don't filter the users at all - show all locked users in the
    	       // database
    	       
	    }
	    else if (ownershipLevel.equals(TradePortalConstants.OWNER_BANK)) {
	       // Show all users under the client bank

	    	dynamicWhereClause.append(" and a.a_client_bank_oid = ? ");
 	        sqlParams.add(clientBankID); 
	    }
	    else {
	       // For BOG users, display the same set of users as for a client
	       // bank, but leave out the bank level admin users

	    	dynamicWhereClause.append(" and a.a_client_bank_oid = ? and a.ownership_level <> ? ");
 	        sqlParams.add(clientBankID);
 	        sqlParams.add(TradePortalConstants.OWNER_BANK);
	    }
        return dynamicWhereClause.toString();
    }

    protected int setDynamicSQLValues(PreparedStatement statement, int startVarIdx,
            Map<String,String> parameters)
            throws SQLException {

    	return setSqlVarsToPreparedStmt(statement, startVarIdx, sqlParams);
    }
}
