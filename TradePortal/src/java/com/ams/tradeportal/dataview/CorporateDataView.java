/**
 *
 */
package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;
import java.util.List;

import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.util.EncryptDecrypt;
import com.amsinc.ecsg.util.StringFunction;

/**
 *
 *
 */
public class CorporateDataView extends AbstractDBDataView {
private static final Logger LOG = LoggerFactory.getLogger(CorporateDataView.class);

    private final List<Object> sqlParams = new ArrayList<>();

    /**
     * Constructor
     */
    public CorporateDataView() {
        super();
    }

    @Override
    protected String buildDynamicSQLCriteria(Map<String, String> parameters) {
        StringBuilder sql = new StringBuilder();
        /**
         * vName=CorporateDataView&selectedClientBankID=pEKhUlxxTQ0=&filterText=abcd&clientBankID=728526&bogID=null&isClientBankUser=true
         *
         */
        String bogID = getUserSession().getBogOid();
        String clientBankID = getUserSession().getClientBankOid();
        String ownershipLevel = getUserSession().getOwnershipLevel();

        String selectedClientBankID = parameters.get("selectedClientBankID");
        selectedClientBankID = EncryptDecrypt.decryptStringUsingTripleDes(selectedClientBankID, getUserSession().getSecretKey());
        String filterText = parameters.get("filterText");

        if (clientBankID == null) {
            clientBankID = "";
        }
        if (bogID == null) {
            bogID = "";
        }
        if (selectedClientBankID == null) {
            selectedClientBankID = "";
        }
        if (filterText == null) {
            filterText = "";
        }

        /* pgedupudi - Rel-9.3 CR-976A 03/30/2015 Start */
        if (StringFunction.isNotBlank(getUserSession()
                .getBankGrpRestrictRuleOid())) {
            sql.append(" and b.organization_oid not in ( ");
            List filteredBankGRoups = filterBankGroups(getUserSession()
                    .getBankGrpRestrictRuleOid());
            for (int iLoop = 0; iLoop < filteredBankGRoups.size(); iLoop++) {

                sql.append(" ? ");
                sqlParams.add(filteredBankGRoups.get(iLoop));
                if (iLoop != filteredBankGRoups.size() - 1) {
                    sql.append(" , ");
                }
            }
            sql.append(" ) ");

        }
        /* pgedupudi - Rel-9.3 CR-976A 03/30/2015 Start */

        if (ownershipLevel.equals(TradePortalConstants.OWNER_BANK)) {

            sql.append(" and b.p_client_bank_oid = ? ");
            sqlParams.add(clientBankID);
        } else if (ownershipLevel.equals(TradePortalConstants.OWNER_BOG)) {

            sql.append(" and a_bank_org_group_oid = ? ");
            sqlParams.add(bogID);
        } // W Zhu 1/15/2012 T36000010296 #333 do not allow access if the ownership is not bank or bog 
        else {
            sql.append(" and 1=0");
        }

        sql.append(" and a.activation_status = ? ");
        sqlParams.add(TradePortalConstants.ACTIVE);

        if (!"".equals(selectedClientBankID)) {
            sql.append(" and a_bank_org_group_oid = ? ");
            sqlParams.add(selectedClientBankID);
        }

        if (!"".equals(filterText)) {
            sql.append(" and upper(a.name) like ? ");
            sqlParams.add("%" + filterText.toUpperCase() + "%");
        }

        return sql.toString();
    }

    @Override
    protected int setDynamicSQLValues(PreparedStatement statement, int startVarIdx,
            Map<String, String> parameters)
            throws SQLException {
        return setSqlVarsToPreparedStmt(statement, startVarIdx, sqlParams);
    }

}
