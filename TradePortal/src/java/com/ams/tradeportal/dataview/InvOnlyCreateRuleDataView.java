package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;
import java.util.List;

import com.amsinc.ecsg.util.EncryptDecrypt;

public class InvOnlyCreateRuleDataView extends AbstractDBDataView{
private static final Logger LOG = LoggerFactory.getLogger(InvOnlyCreateRuleDataView.class);
	
	private final List<Object> sqlParams = new ArrayList<>();

	public InvOnlyCreateRuleDataView (){
		super();
	}
 
	protected String buildDynamicSQLCriteria(Map<String,String> parameters) {

 		
 		
		String inDef = parameters.get("invDef");
		String inType = parameters.get("ruleType");
		String subAccessUserOrgID = parameters.get("subAccessUserOrgID");
		String parentOrgID = parameters.get("parentOrgID");
  		StringBuilder dSql = new StringBuilder();
 
		if (inType != null && inType.trim().length() > 0){
			dSql.append(" and upper(r.instrument_type_code) = ? ");
			sqlParams.add(inType.toUpperCase());
		}
		
		if (inDef != null && inDef.trim().length() > 0){                                 
            dSql.append(" and  r.a_invoice_def = ? ");
            Object ob = new Object();
            ob = Integer.parseInt(EncryptDecrypt.decryptStringUsingTripleDes(inDef, getUserSession().getSecretKey()));
            sqlParams.add(ob);
		}

		
		dSql.append(" and r.a_owner_org_oid in (?" );
 	      // Possibly include the subsidiary access user org's data also
		sqlParams.add(parentOrgID);
	    
		if(null != subAccessUserOrgID && subAccessUserOrgID.trim().length() >0 && !"null".equalsIgnoreCase(subAccessUserOrgID)){
			dSql.append(",?");
			sqlParams.add(subAccessUserOrgID);
		}

        dSql.append(")");
        return dSql.toString();
	}

 
	protected int setDynamicSQLValues(PreparedStatement statement,
			int startVarIdx, Map<String,String> parameters)
			throws SQLException {
		return setSqlVarsToPreparedStmt(statement, startVarIdx, sqlParams);
	}
}
