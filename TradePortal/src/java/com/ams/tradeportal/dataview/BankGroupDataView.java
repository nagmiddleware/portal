/**
 *
 */
package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;
import java.util.List;

import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.util.StringFunction;

/**
 *
 *
 */
public class BankGroupDataView extends AbstractDBDataView {
private static final Logger LOG = LoggerFactory.getLogger(BankGroupDataView.class);

    /**
     * Constructor
     */
    public BankGroupDataView() {
        super();
    }
    private final List<Object> sqlParams = new ArrayList<>();

    @Override
    protected String buildDynamicSQLCriteria(Map<String, String> parameters) {
        StringBuilder sql = new StringBuilder();

        String parentOrgID = getUserSession().getOwnerOrgOid();

        if (parentOrgID != null) {
            sql.append("where p_client_bank_oid = ?  and activation_status = ?");
            sqlParams.add(getUserSession().getOwnerOrgOid());
            sqlParams.add(TradePortalConstants.ACTIVE);

            /* pgedupudi - Rel-9.3 CR-976A 03/30/2015 Start */
            if (StringFunction.isNotBlank(getUserSession()
                    .getBankGrpRestrictRuleOid())) {
                sql.append(" and organization_oid not in ( ");
                List filteredBankGRoups = filterBankGroups(getUserSession()
                        .getBankGrpRestrictRuleOid());
                for (int iLoop = 0; iLoop < filteredBankGRoups.size(); iLoop++) {
                    sql.append(" ? ");
                    sqlParams.add(filteredBankGRoups.get(iLoop));
                    if (iLoop != filteredBankGRoups.size() - 1) {
                        sql.append(" , ");
                    }
                }
                sql.append(" ) ");

            }
            /* pgedupudi - Rel-9.3 CR-976A 03/30/2015 End */
        }
        return sql.toString();
    }

    @Override
    protected int setDynamicSQLValues(PreparedStatement statement, int startVarIdx,
            Map<String, String> parameters)
            throws SQLException {

        return setSqlVarsToPreparedStmt(statement, startVarIdx, sqlParams);

    }

}
