/**
 *
 */
package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Map;
import java.util.List;
import java.util.ArrayList;


/**
 *
 *
 */
public class PurchaseOrdersDataview     extends AbstractDBDataView
{
private static final Logger LOG = LoggerFactory.getLogger(PurchaseOrdersDataview.class);
	private final List<Object> sqlParams = new ArrayList<>();
	/**
	 * Constructor
	 */
	public PurchaseOrdersDataview() {
		super();
	}

    protected String buildDynamicSQLCriteria(Map<String,String> parameters) {
        StringBuilder sql = new StringBuilder();
        String userOrgOid = getUserSession().getOwnerOrgOid();
        String poNumber = parameters.get("poNumber");
        String lineItemNumber = parameters.get("lineItemNumber");
        String beneName = parameters.get("beneName");

        if(poNumber == null)poNumber="";
        if(lineItemNumber == null)lineItemNumber="";
        if(beneName == null)beneName="";

        sql.append("and p.a_owner_org_oid = ? ");
        sqlParams.add(userOrgOid);

        // Search by the PO Number

        if(!"".equals(poNumber) && "".equals(lineItemNumber) && "".equals(beneName)){
            sql.append(" and upper(po_num) like upper(?) ");
            sqlParams.add("%" + poNumber+"%");
        }

        // Search by the PO Number and the Line Item Number
        if(!"".equals(poNumber) && !"".equals(lineItemNumber) && "".equals(beneName)){
             sql.append(" and upper(po_num) like upper(?)  and upper(item_num) like upper(?)");
             sqlParams.add("%" + poNumber+"%");
             sqlParams.add(lineItemNumber+"%");
        }

        // Search by the PO Number and the Beneficiary Name
        if(!"".equals(poNumber) && "".equals(lineItemNumber) && !"".equals(beneName)){
            sql.append(" and upper(po_num) like upper(?)  and upper(ben_name) like upper(?) ");
            sqlParams.add("%" + poNumber+"%");
            sqlParams.add("%" + beneName+"%");
        }

        // Search by the PO Number and the Line Item Number and the Beneficiary Name
        if(!"".equals(poNumber) && !"".equals(lineItemNumber) && !"".equals(beneName)){
            sql.append(" and upper(po_num) like upper(?) and upper(item_num) like upper(?) and upper(ben_name) like upper(?)");
            
            sqlParams.add("%" + poNumber+"%");
            sqlParams.add(lineItemNumber+"%");
            sqlParams.add("%" + beneName+"%");
        }

        // Search by the Beneficiary Name
        if("".equals(poNumber) && "".equals(lineItemNumber) && !"".equals(beneName)){
            sql.append(" and upper(ben_name) like upper(?)");
            sqlParams.add("%" + beneName+"%");
        }

        // Build the where clause - All fields are empty
        if("".equals(poNumber) && "".equals(lineItemNumber) && "".equals(beneName)){
            sql.append("");
        }


        return sql.toString();
    }

    protected int setDynamicSQLValues(PreparedStatement statement, int startVarIdx,
            Map<String,String> parameters)
            throws SQLException {

    	return setSqlVarsToPreparedStmt(statement, startVarIdx, sqlParams);
    }
}
