/**
 *
 */
package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;
import java.util.List;

import com.ams.tradeportal.common.NumberValidator;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.EncryptDecrypt;
import com.amsinc.ecsg.util.StringFunction;

/**
 *
 *
 */
public class ExistingDirectDebitSearchDataView extends AbstractDBDataView {
private static final Logger LOG = LoggerFactory.getLogger(ExistingDirectDebitSearchDataView.class);

    private final List<Object> sqlParams = new ArrayList<>();

    /**
     * Constructor
     */
    public ExistingDirectDebitSearchDataView() {
        super();
    }

    @Override
    protected String buildDynamicSQLCriteria(Map<String, String> parameters) {

        StringBuilder sql = new StringBuilder();
        try {

            String inactive = parameters.get("inactive");
            String active = parameters.get("active");
            String org = parameters.get("organisation");
            String instrumentId = parameters.get("InstrumentId");
            String creditAcct = parameters.get("CreditAcct");
            String refNum = parameters.get("Reference");
            String filterType = parameters.get("searchType");
            String fromDate = parameters.get("fromDate");
            String toDate = parameters.get("toDate");
            String fromAmount = parameters.get("fromAmount");
            String toAmount = parameters.get("toAmount");

            if (active.equalsIgnoreCase("false") && inactive.equalsIgnoreCase("false")) {
                sql.append(" and i.instrument_status not in ('CAN', 'CLO', 'DEA', 'LIQ', 'LQB', 'LQC', 'ACT', 'PND', 'EXP')");
            } else if (active.equalsIgnoreCase("true") && inactive.equalsIgnoreCase("true")) {
                sql.append(" and i.instrument_status in ('CAN', 'CLO', 'DEA', 'LIQ', 'LQB', 'LQC', 'ACT', 'PND', 'EXP')");
            } else if (active.equalsIgnoreCase("false") && inactive.equalsIgnoreCase("true")) {
                sql.append(" and i.instrument_status in ('CAN', 'CLO', 'DEA', 'LIQ', 'LQB', 'LQC')");
            } else if (active.equalsIgnoreCase("true") && inactive.equalsIgnoreCase("false")) {
                sql.append(" and i.instrument_status in ('ACT', 'PND', 'EXP')");
            }

            String userOrgPlaceHolder = "";
            if (org != null) {
                org = EncryptDecrypt.decryptStringUsingTripleDes(org, getUserSession().getSecretKey());
                String ALL_ORGANIZATIONS = getResourceManager().getText("AuthorizedTransactions.AllOrganizations", TradePortalConstants.TEXT_BUNDLE);
                if (ALL_ORGANIZATIONS.equalsIgnoreCase(org)) {
                    StringBuilder sqlQuery = new StringBuilder();
                    sqlQuery.append("select organization_oid, name");
                    sqlQuery.append(" from corporate_org");
                    sqlQuery.append(" where activation_status = ? start with organization_oid = ? ");
                    sqlQuery.append(" connect by prior organization_oid = p_parent_corp_org_oid");
                    sqlQuery.append(" order by ");
                    sqlQuery.append(getResourceManager().localizeOrderBy("name"));
                    StringBuilder orgListPlaceHolder = new StringBuilder();
                    try {
                        DocumentHandler hierarchyDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery.toString(), false, TradePortalConstants.ACTIVE, getOwnerOrgOid());

                        List<DocumentHandler> orgListDoc = hierarchyDoc.getFragmentsList("/ResultSetRecord");
                        int totalOrganizations = orgListDoc.size();

                        DocumentHandler orgDoc = null;
                        for (int i = 0; i < totalOrganizations; i++) {
                            orgDoc = orgListDoc.get(i);
                            orgListPlaceHolder.append("?");
                            sqlParams.add(orgDoc.getAttribute("ORGANIZATION_OID"));
                            if (i < (totalOrganizations - 1)) {
                                orgListPlaceHolder.append(", ");
                            }
                        }
                    } catch (AmsException e) {
                        e.printStackTrace();
                    }
                    userOrgPlaceHolder = orgListPlaceHolder.toString();
                } else {
                    userOrgPlaceHolder = "?";
                    sqlParams.add(org);
                }
            } //cquinton 4/9/2013 Rel PR ir#15704 begin
            else {
                //selectedOrgOid is required. if not found return nothing
                sql.append(" and 1=0");
            }
		        //cquinton 4/9/2013 Rel PR ir#15704 end

            if (StringFunction.isNotBlank(org)) {
                sql.append(" and i.a_corp_org_oid in( " + userOrgPlaceHolder + " )");
            }

            if (filterType.equalsIgnoreCase("S")) {
                // basic search; use OR condition
                if (instrumentId != null && instrumentId.trim().length() > 0) {
                    sql.append(" and upper(i.complete_instrument_id) like ? ");
                    sqlParams.add("%" + instrumentId.toUpperCase() + "%");
                }
                if (creditAcct != null && creditAcct.trim().length() > 0) {
                    sql.append(" and a.ACCOUNT_OID = ? ");
                    sqlParams.add(creditAcct);
                }
                if (refNum != null && refNum.trim().length() > 0) {
                    sql.append(" and upper(i.copy_of_ref_num) like ? ");
                    sqlParams.add("%" + refNum.toUpperCase() + "%");
                }
            } else if (filterType.equalsIgnoreCase("A")) {
                // advanced search; AND all the search criteria
                if (instrumentId != null && instrumentId.trim().length() > 0) {
                    sql.append(" and upper(i.complete_instrument_id) like ? "); //"%'" is safe for URLdecoding.
                    sqlParams.add("%" + instrumentId.toUpperCase() + "%");
                }
                if (creditAcct != null && creditAcct.trim().length() > 0) {
                    sql.append(" and a.ACCOUNT_OID = ? ");
                    sqlParams.add(creditAcct);
                }
                if (refNum != null && refNum.trim().length() > 0) {
                    sql.append(" and upper(i.copy_of_ref_num) like ? ");
                    sqlParams.add("%" + refNum.toUpperCase() + "%");
                }
                if (fromAmount != null && fromAmount.trim().length() > 0) {
                    sql.append(" and i.copy_of_instrument_amount >= ?  ");
                    sqlParams.add(NumberValidator.getNonInternationalizedValue(fromAmount, getUserSession().getUserLocale()));
                }
                if (toAmount != null && toAmount.trim().length() > 0) {
                    sql.append(" and i.copy_of_instrument_amount <= ? ");
                    sqlParams.add(NumberValidator.getNonInternationalizedValue(toAmount, getUserSession().getUserLocale()));
                }
                if (fromDate != null && fromDate.trim().length() > 0) {
                    sql.append(" and o.payment_date >=  to_date(?,'mm-dd-yyyy')");
                    sqlParams.add(fromDate.replaceAll("/", "-"));
                }
                if (toDate != null && toDate.trim().length() > 0) {
                    sql.append(" and o.payment_date <= to_date(?,'mm-dd-yyyy')");
                    sqlParams.add(toDate.replaceAll("/", "-"));
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return sql.toString();
    }

    @Override
    protected int setDynamicSQLValues(PreparedStatement statement, int startVarIdx,
            Map<String, String> parameters) throws SQLException {

        return setSqlVarsToPreparedStmt(statement, startVarIdx, sqlParams);
    }
}
