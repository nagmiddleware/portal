/**
 *
 */
package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;
import java.util.List;

import com.ams.tradeportal.common.SQLParamFilter;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.util.EncryptDecrypt;
import com.amsinc.ecsg.util.StringFunction;
/**
 *
 *
 */
public class BankMailMessagesDataView     extends AbstractDBDataView
{
private static final Logger LOG = LoggerFactory.getLogger(BankMailMessagesDataView.class);

	private final List<Object> sqlParams = new ArrayList<>();
	/**
	 * Constructor
	 */
	public BankMailMessagesDataView() {
		super();
	}
	
	public final static String ALL_MAIL               = "BankMail.AllMail";

    protected String buildDynamicSQLCriteria(Map<String,String> parameters) {

        StringBuilder sql = new StringBuilder();
        String userOrgOid = getUserSession().getOwnerOrgOid();
        String currentMailOption = parameters.get("currentMailOption");
        String currentFolder = SQLParamFilter.filter(parameters.get("currentFolder"));
        String ownerShip = getUserSession().getOwnershipLevel();
            sql.append(" AND c.cust_is_not_intg_tps = ? ");
            sqlParams.add(TradePortalConstants.INDICATOR_YES);
            if (StringFunction.isNotBlank(getUserSession().getBankGrpRestrictRuleOid() )) {
       		 sql.append(" and bog.organization_oid not in ( ");
                sql.append("select BANK_ORGANIZATION_GROUP_OID from BANK_GRP_FOR_RESTRICT_RULES where P_BANK_GRP_RESTRICT_RULES_OID= ? ");
                sqlParams.add(getUserSession().getBankGrpRestrictRuleOid());
                sql.append(") ");
            } 
        if(TradePortalConstants.OWNER_BANK.equals(ownerShip)){       	
        	sql.append("and c.a_client_bank_oid = ? ");
        	sqlParams.add(userOrgOid);
        } else {        
        	sql.append("and c.a_bank_org_group_oid = ? "); 
        	sqlParams.add(userOrgOid);
        	
         }
      
        
        	 
       if ( StringFunction.isNotBlank(currentMailOption) ) {	
        	currentMailOption = EncryptDecrypt.decryptStringUsingTripleDes( currentMailOption, getUserSession().getSecretKey() );
        	sql.append(" and c.name = ? ");
        	sqlParams.add(currentMailOption);
       }
         
         if( StringFunction.isNotBlank(currentFolder) ){
        	 if (currentFolder.equals(TradePortalConstants.INBOX_FOLDER)){
        		 sql.append(" and message_status = ? ");
        		 sqlParams.add( TradePortalConstants.REC );
        	 }else if (currentFolder.equals(TradePortalConstants.DRAFTS_FOLDER)){
        		 sql.append(" and message_status = ? ");
        		 sqlParams.add( TradePortalConstants.DRAFT );
        	 }else if (currentFolder.equals(TradePortalConstants.SENT_TO_CUST_FOLDER)){
        		 sql.append(" and message_status = ? ");
        		 sqlParams.add( TradePortalConstants.SENT_TO_CUST );
        	 }
        }
        
             
        return sql.toString();
    }

    protected int setDynamicSQLValues(PreparedStatement statement, int startVarIdx,
            Map<String,String> parameters)
            throws SQLException {

    	return setSqlVarsToPreparedStmt(statement, startVarIdx, sqlParams);
    }
}
