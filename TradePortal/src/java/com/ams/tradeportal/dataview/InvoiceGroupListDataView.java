package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.List;

public class InvoiceGroupListDataView extends AbstractDBDataView {
private static final Logger LOG = LoggerFactory.getLogger(InvoiceGroupListDataView.class);

    private final List<Object> sqlParams = new ArrayList<>();

    @Override
    protected String buildDynamicSQLCriteria(Map<String, String> parameters) {

        StringBuilder dynamicWhereClause = new StringBuilder();

        String userOrgOid = getUserSession().getOwnerOrgOid();
        String invoiceGroupOid = parameters.get("invoiceGroupOid");

        dynamicWhereClause.append("and i.a_corp_org_oid = ? ");
        sqlParams.add(userOrgOid);
        dynamicWhereClause.append(" and i.a_invoice_group_oid =?  ");
        sqlParams.add(invoiceGroupOid);
        //This piece of code is needed to print(view pdf) the invoice list
        HashMap map = new HashMap();
        map.put("InvoiceGroupListDataView", dynamicWhereClause.toString());
        map.put("params", parameters);
        map.put("paramsDynamicSqlPlaceHolder", sqlParams);
        InvoiceDataViewUtil.getMoreSQL().setMap(map);

        return dynamicWhereClause.toString();
    }

    @Override
    protected int setDynamicSQLValues(PreparedStatement statement, int startVarIdx,
            Map<String, String> parameters)
            throws SQLException {
        return setSqlVarsToPreparedStmt(statement, startVarIdx, sqlParams);
    }
}
