/**
 * Ravindra - Rel8200 CR-708B - Initial Version
 */
package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Map;
import java.util.List;
import java.util.ArrayList;

import com.amsinc.ecsg.util.EncryptDecrypt;
import java.util.HashMap;

/**
 *
 *
 */
public class InvoicesOfferedUserDefinedGroupDetailDataView extends AbstractDBDataView {
private static final Logger LOG = LoggerFactory.getLogger(InvoicesOfferedUserDefinedGroupDetailDataView.class);

    private final List<Object> sqlParams = new ArrayList<>();

    /**
     * Constructor
     */
    public InvoicesOfferedUserDefinedGroupDetailDataView() {
        super();
    }

    @Override
    protected String buildDynamicSQLCriteria(Map<String, String> parameters) {

        sqlParams.add(EncryptDecrypt.decryptStringUsingTripleDes(parameters.get("invoiceGroupOid"), getUserSession().getSecretKey()));

        String qry = " and i.a_invoice_offer_group_oid = ? ";
        //This piece of code is needed to print(view pdf) the invoice list
        HashMap map = new HashMap();
        map.put("InvoicesOfferedGroupListDataView", qry);
        map.put("paramsDynamicSqlPlaceHolder", sqlParams);

        InvoiceDataViewUtil.getMoreSQL().setMap(map);

        return qry;
    }

    @Override
    protected int setDynamicSQLValues(PreparedStatement statement, int startVarIdx,
            Map<String, String> parameters)
            throws SQLException {
        return setSqlVarsToPreparedStmt(statement, startVarIdx, sqlParams);
    }

}
