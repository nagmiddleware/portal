/**
 *
 */
package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.util.Map;

import java.sql.SQLException;

/**
 *
 *
 */
public class PaymentMethodValidationDataView     extends AbstractDBDataView
{
private static final Logger LOG = LoggerFactory.getLogger(PaymentMethodValidationDataView.class);
	/**
	 * Constructor
	 */
	public PaymentMethodValidationDataView() {
		super();
	}

    protected String buildDynamicSQLCriteria(Map<String,String> parameters) {
    	

    	return "";
    }

    protected int setDynamicSQLValues(PreparedStatement statement, int startVarIdx,
            Map<String,String> parameters)
            throws SQLException {

        return 0;
    }
}
