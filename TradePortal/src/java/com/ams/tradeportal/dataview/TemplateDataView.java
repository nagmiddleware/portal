/**
 *
 */
package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;
import java.util.List;

import com.ams.tradeportal.busobj.webbean.CorporateOrganizationWebBean;
import com.ams.tradeportal.common.InstrumentType;
import com.ams.tradeportal.common.TradePortalConstants;

/**
 *
 *
 */
public class TemplateDataView     extends AbstractDBDataView {
private static final Logger LOG = LoggerFactory.getLogger(TemplateDataView.class);

	private final List<Object> sqlParams = new ArrayList<>();
	/**
	 * Constructor
	 */
	public TemplateDataView() {
		super();
	}

    protected String buildDynamicSQLCriteria(Map<String,String> parameters) {

        StringBuilder sql = new StringBuilder();

        String clientBankID = getUserSession().getClientBankOid();
        String bogID = getUserSession().getBogOid();
        String globalID = getUserSession().getGlobalOrgOid();
        String parentOrgID = getUserSession().getOwnerOrgOid();
        String ownershipLevel = getUserSession().getOwnershipLevel();
        boolean includeSubAccessUserOrg = getUserSession().showOrgDataUnderSubAccess();
        String subAccessUserOrgID = null;
        if(includeSubAccessUserOrg) {
            subAccessUserOrgID = getUserSession().getSavedUserSession().getOwnerOrgOid();
        }
        String confInd  = getConfidentialInd();
    	
		String templateName = parameters.get("templateName");
		if (templateName != null && templateName.trim().length() > 0){
			sql.append(" and upper(t.name) like ? ");
			sqlParams.add("%"+templateName.toUpperCase()+"%");
		}


        sql.append(" and p_owner_org_oid in (?,?");
        sqlParams.add(parentOrgID);
        sqlParams.add(globalID);
        if(ownershipLevel != null){
	        if (ownershipLevel.equals(TradePortalConstants.OWNER_BOG) || ownershipLevel.equals(TradePortalConstants.OWNER_CORPORATE)) {
	            sql.append(",?");
	            sqlParams.add(clientBankID);
	            if (ownershipLevel.equals(TradePortalConstants.OWNER_CORPORATE)){
	                sql.append(",?");
	                sqlParams.add(bogID);
	            }
	        }
        }

        if(includeSubAccessUserOrg){
           sql.append(",?");
           sqlParams.add(subAccessUserOrgID);
        }

        sql.append(")");
    	CorporateOrganizationWebBean corporateOrg = getBeanManager().createBean(CorporateOrganizationWebBean.class,"CorporateOrganization");
		corporateOrg.setAttribute("organization_oid", parentOrgID);
		corporateOrg.getDataFromAppServer();
		if (corporateOrg.getAttribute("standbys_use_guar_form_only").equals(TradePortalConstants.INDICATOR_YES) && corporateOrg.getAttribute("standbys_use_either_form").equals(TradePortalConstants.INDICATOR_NO)) {
			sql.append(" and (n.standby_using_guarantee_form = '" + TradePortalConstants.INDICATOR_YES +"' and i.instrument_type_code = '" + InstrumentType.STANDBY_LC +"' or not(i.instrument_type_code = '" + InstrumentType.STANDBY_LC +"'))");
		} else if (corporateOrg.getAttribute("standbys_use_guar_form_only").equals(TradePortalConstants.INDICATOR_NO) && corporateOrg.getAttribute("standbys_use_either_form").equals(TradePortalConstants.INDICATOR_NO)) {
			sql.append(" and (n.standby_using_guarantee_form = '" + TradePortalConstants.INDICATOR_NO +"' and i.instrument_type_code = '" + InstrumentType.STANDBY_LC +"' or not(i.instrument_type_code = '" + InstrumentType.STANDBY_LC +"'))");
		}

		if (!TradePortalConstants.INDICATOR_YES.equals(confInd)){
			sql.append(" and t.confidential_indicator = '" + TradePortalConstants.INDICATOR_NO +"'");
		}

        return sql.toString();
    }

    protected int setDynamicSQLValues(PreparedStatement statement, int startVarIdx,
            Map<String,String> parameters)
            throws SQLException {
    	
    	return setSqlVarsToPreparedStmt(statement, startVarIdx, sqlParams);
    }
}
