/**
 *
 */
package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Map;
import java.util.List;
import java.util.ArrayList;
import com.ams.tradeportal.common.Debug;
import com.ams.tradeportal.common.SQLParamFilter;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.util.EncryptDecrypt;
import com.amsinc.ecsg.util.StringFunction;

/**
 *
 *Rel 9.0 CR 913 - Data View used to get all data related to Pre Debit Funding Mail Messages
 */
public class HomeDebitFundingMailMsgDataView     extends AbstractDBDataView
{
private static final Logger LOG = LoggerFactory.getLogger(HomeDebitFundingMailMsgDataView.class);
	private final List<Object> sqlParams = new ArrayList<>();
    //some options for the org search parameter
    final static String MY_MAIL_AND_UNASSIGNED = "Mail.MeAndUnassigned";
    final static String ALL_MAIL = "Mail.AllMail";

	/**
	 * Constructor
	 */
	public HomeDebitFundingMailMsgDataView() {
		super();
	}

    protected String buildDynamicSQLCriteria(Map<String,String> parameters) {

        StringBuilder sql = new StringBuilder();

        String selectedOrgOid = parameters.get("org");
       
        String unencryptedOrgOid = EncryptDecrypt.decryptStringUsingTripleDes(selectedOrgOid, getUserSession().getSecretKey());
        String payDateFrom = parameters.get("payDateFrom");
        String payDateTo = parameters.get("payDateTo");
        String currency = parameters.get("currency");
        String datePattern = null;
        
        try{
        	if(null != parameters.get("dPattern")){
        		datePattern = java.net.URLDecoder.decode(parameters.get("dPattern"), "UTF-8");
        	}else{
        		datePattern = "";
        	}
        }catch (UnsupportedEncodingException e) {
			
			LOG.debug("Exceptiom in DateFormat Decoding : "+e);
		}
        if (null != currency && !"".equals(currency)) {
        	sql.append(" and a.funding_currency = ? ");
        	sqlParams.add(currency);
        }
        boolean hasDate = false;
        
    
        StringBuilder temp = new StringBuilder();
        if (null != payDateFrom && !"".equals(payDateFrom)) {
        	temp.append(" and a.funding_date >= TO_DATE(?,?)");
        	hasDate = true;
        	sqlParams.add(payDateFrom);
        	sqlParams.add(datePattern);
        }

        if (null != payDateTo && !"".equals(payDateTo)) {
        	temp.append(" and a.funding_date <= TO_DATE(?,?)");
        	hasDate = true;
        	sqlParams.add(payDateTo);
        	sqlParams.add(datePattern);
        }
        
        //Rel9.0 IR#T36000029874 - Payment Date Filter conditions corrected
        sql.append(" and (a.funding_amount is not NULL OR a.funding_date is not null)");
        if(hasDate){
           	sql.append(temp);
        }
        
        
        if (HomeDebitFundingMailMsgDataView.MY_MAIL_AND_UNASSIGNED.equals(unencryptedOrgOid)) {
            sql.append("and (a.a_assigned_to_user_oid = ?"); //userOid
            sql.append(" or (a.a_assigned_to_corp_org_oid = ?"); //ownerOrgOid
            sql.append(" and a.a_assigned_to_user_oid is null))");
            sqlParams.add(this.getUserOid());
            sqlParams.add(this.getOwnerOrgOid());
        }
        else if (HomeDebitFundingMailMsgDataView.ALL_MAIL.equals(unencryptedOrgOid)) {
            sql.append("and a.a_assigned_to_corp_org_oid in (");
            sql.append( "select organization_oid");
            sql.append( " from corporate_org");
            sql.append( " where activation_status = ? start with organization_oid = ?"); //ownerOrgOid
            sql.append( " connect by prior organization_oid = p_parent_corp_org_oid)");
            sqlParams.add(TradePortalConstants.ACTIVE);
            sqlParams.add(this.getOwnerOrgOid());
        }
        else if ( unencryptedOrgOid != null && unencryptedOrgOid.length()>0 ) {
            sql.append("and a.a_assigned_to_corp_org_oid = ?"); //selected org
            sqlParams.add(unencryptedOrgOid);
        }
        else {
            //selectedOrgOid is required. if not found return nothing
            sql.append(" and 1=0");
        }        
        String unreadFlag = SQLParamFilter.filter(parameters.get("UnreadFlag"));
        
        LOG.info("unreadFlagunreadFlag"+unreadFlag);
        
        if (StringFunction.isNotBlank(unreadFlag)) 
        {
        	if("N".equals(unreadFlag))
        	{
           		sql.append(" and a.unread_flag = ?");
        	}
        	else 
        	{
          		sql.append(" and (a.unread_flag = 'Y' or a.unread_flag <> ?)");
        	}
        	sqlParams.add(unreadFlag);	
        }
        //inbox folder
        sql.append(" and message_status in (?, ?, ?) ");  
        sqlParams.add(TradePortalConstants.REC);
        sqlParams.add(TradePortalConstants.REC_ASSIGNED);
        sqlParams.add(TradePortalConstants.SENT_ASSIGNED);
     
        return sql.toString();
    }

    protected int setDynamicSQLValues(PreparedStatement statement, int startVarIdx,
            Map<String,String> parameters)
            throws SQLException {
    	return setSqlVarsToPreparedStmt(statement, startVarIdx, sqlParams);    }
}
