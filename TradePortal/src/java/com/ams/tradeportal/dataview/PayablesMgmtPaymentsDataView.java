/**
 *
 */
package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;
import java.sql.PreparedStatement;
import java.util.Map;

import java.sql.SQLException;

import com.ams.tradeportal.common.Debug;
import com.ams.tradeportal.common.SQLParamFilter;
import com.amsinc.ecsg.util.StringFunction;

/**

 *
 */
public class PayablesMgmtPaymentsDataView
    extends AbstractDBDataView {
private static final Logger LOG = LoggerFactory.getLogger(PayablesMgmtPaymentsDataView.class);
    
	/**
     * Constructor
     */
    public PayablesMgmtPaymentsDataView() {
    	super();
    }
    
    protected String buildDynamicSQLCriteria(Map<String,String> parameters) {
    	StringBuilder      dynamicWhereClause     = new StringBuilder();
    	String Active=SQLParamFilter.filter(parameters.get("Active"));
    	String InActive=SQLParamFilter.filter(parameters.get("InActive"));
    	String InqAmountFrom=SQLParamFilter.filterNumber(parameters.get("amountFrom"));
    	String InqAmountTo=SQLParamFilter.filterNumber(parameters.get("amountTo"));
    	String Currency=SQLParamFilter.filter(parameters.get("currency"));
    	String payDateFrom = SQLParamFilter.filter(parameters.get("payDateFrom"));
        String payDateTo = SQLParamFilter.filter(parameters.get("payDateTo"));  
        String datePattern = null;
        try{
        	if(StringFunction.isNotBlank(parameters.get("dPattern"))){
        		datePattern = java.net.URLDecoder.decode(parameters.get("dPattern"), "UTF-8");
        	}else{
        		datePattern = "";
        	}
        }catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			LOG.debug("Exceptiom in DateFormat Decoding : "+e);
		}
        
        String UserOrgOid=getUserSession().getOwnerOrgOid();
        
        if(StringFunction.isNotBlank(Active) && Active.equals("Y")){
    		if(InActive!=null && !InActive.equals("") && InActive.equals("Y")){
    			dynamicWhereClause.append(" and b.instrument_status in ('ACT','PND','EXP','CLO','CAN','DEA','LIQ','LQB','LQC','DEL')");
    		}
    		else {
    			dynamicWhereClause.append(" and b.instrument_status in ('ACT','PND','EXP')");
    		}	
    	}
    	else if(StringFunction.isNotBlank(InActive) && InActive.equals("Y")){
    		dynamicWhereClause.append(" and b.instrument_status in ('CLO','CAN','DEA','LIQ','LQB','LQC','DEL')");
    	}
    	else if((StringFunction.isBlank(Active) || Active.equals("N")) && (StringFunction.isBlank(InActive) || InActive.equals("N"))) {
    		dynamicWhereClause.append(" and b.instrument_status in ('') ");
    	}
    	
    	if(StringFunction.isNotBlank(Currency)){
    		dynamicWhereClause.append(" and a.copy_of_currency_code='"+Currency+"'");
    	}
    	if(StringFunction.isNotBlank(InqAmountFrom)){
    		dynamicWhereClause.append(" and a.copy_of_amount >= "+InqAmountFrom);
    	}
    	if(StringFunction.isNotBlank(InqAmountTo)){
    		dynamicWhereClause.append(" and a.copy_of_amount <= "+InqAmountTo);
    	}
    	if (StringFunction.isNotBlank(payDateFrom)) {
	  		 dynamicWhereClause.append(" and TRUNC(a.transaction_status_date) >= TO_DATE('");
	  		 dynamicWhereClause.append(payDateFrom);
	  		 dynamicWhereClause.append("','"+datePattern+"')");
        }

       if (StringFunction.isNotBlank(payDateTo)) {
    	   dynamicWhereClause.append(" and TRUNC(a.transaction_status_date) <= TO_DATE('"); //  DK IR T36000024022 Rel8.4 01/10/2014
    	   dynamicWhereClause.append(payDateTo);
    	   dynamicWhereClause.append("','"+datePattern+"')");
       } 
    	//Added by dillip for defect #T36000005146
    	if(StringFunction.isNotBlank(UserOrgOid)){
    		dynamicWhereClause.append(" and b.a_corp_org_oid = "+UserOrgOid);
    	}
    	//Ended Here
    	
    	return dynamicWhereClause.toString();
    }
    
    protected int setDynamicSQLValues(PreparedStatement statement, int startVarIdx,
            Map<String,String> parameters)
            throws SQLException {
        int varCount = 0;
        
        return varCount;
    }
}
