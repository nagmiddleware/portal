/**
 *
 */
package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;
import java.util.List;

import com.amsinc.ecsg.util.EncryptDecrypt;

/**
 *
 *
 */
public class CreationRuleStructPODataView extends AbstractDBDataView {
private static final Logger LOG = LoggerFactory.getLogger(CreationRuleStructPODataView.class);

    private final List<Object> sqlParams = new ArrayList<>();

    /**
     * Constructor
     */
    public CreationRuleStructPODataView() {
        super();
    }

    @Override
    protected String buildDynamicSQLCriteria(Map<String, String> parameters) {
        StringBuilder sql = new StringBuilder();

        boolean includeSubAccessUserOrg = getUserSession().showOrgDataUnderSubAccess();
        String subAccessUserOrgID = null;
        if (includeSubAccessUserOrg) {
            subAccessUserOrgID = getUserSession().getSavedUserSession().getOwnerOrgOid();
        }
        String parentOrgID = getUserSession().getOwnerOrgOid();
        String ruletype = parameters.get("ruletype");
        String podef = parameters.get("podef");
        // DK IR T36000016490 Rel8.4 01/07/2014 starts
        String ruleName = parameters.get("ruleName");
        String templateName = parameters.get("templateName");

        if (ruleName != null && ruleName.trim().length() > 0) {
            sql.append(" and upper(l.name) like ? ");
            sqlParams.add("%" + ruleName.toUpperCase() + "%");
        }

        if (templateName != null && templateName.trim().length() > 0) {
            sql.append(" and upper(t.name) like ? ");
            sqlParams.add("%" + templateName.toUpperCase() + "%");
        }

        // DK IR T36000016490 Rel8.4 01/07/2014 ends
        if (ruletype != null && ruletype.trim().length() > 0) {
            sql.append(" and upper(l.instrument_type_code) = ? ");
            sqlParams.add(ruletype.toUpperCase());
        }

        if (podef != null && podef.trim().length() > 0) {
            sql.append(" and p.PURCHASE_ORDER_DEFINITION_OID  = ?");
            sqlParams.add(EncryptDecrypt.decryptStringUsingTripleDes(podef, getUserSession().getSecretKey()));
        }

        sql.append(" and l.a_owner_org_oid in (?");
        sqlParams.add(parentOrgID);

        // Possibly include the subsidiary access user org's data also
        if (includeSubAccessUserOrg) {
            sql.append(",?");
            sqlParams.add(subAccessUserOrgID);
        }

        sql.append(")");

        return sql.toString();
    }

    @Override
    protected int setDynamicSQLValues(PreparedStatement statement,
            int startVarIdx, Map<String, String> parameters)
            throws SQLException {

        return setSqlVarsToPreparedStmt(statement, startVarIdx, sqlParams);
    }

}
