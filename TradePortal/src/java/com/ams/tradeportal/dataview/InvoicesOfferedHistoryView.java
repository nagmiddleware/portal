
package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.ams.tradeportal.busobj.CorporateOrganization;
import com.ams.tradeportal.common.NumberValidator;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.EJBObjectFactory;
import com.amsinc.ecsg.frame.InvalidAttributeValueException;
import com.amsinc.ecsg.frame.MediatorServices;
import com.amsinc.ecsg.util.StringFunction;


/**
 *
 *
 */
public class InvoicesOfferedHistoryView    extends AbstractDBDataView
{
private static final Logger LOG = LoggerFactory.getLogger(InvoicesOfferedHistoryView.class);
	private List<Object> sqlParams = new ArrayList();
	/**
	 * Constructor
	 */
	public InvoicesOfferedHistoryView() {
		super();
	}

    protected String buildDynamicSQLCriteria(Map<String,String> parameters) throws DataViewException {

    	StringBuilder dynamicWhereClause     = new StringBuilder();
    	MediatorServices medService = new MediatorServices();
    	String selectedStatus = parameters.get("selectedStatus");
        String userOrgOid = getOwnerOrgOid();
        String invoiceID = parameters.get("invoiceID");
    	String currency = parameters.get("currency");
    	String amountFrom = parameters.get("amountFrom");
        String amountTo = parameters.get("amountTo");
        String dateFrom = parameters.get("dateFrom");
        String dateTo = parameters.get("dateTo");
        String relatedInstrumentID = parameters.get("relatedInstrumentID");
        String datePattern = getUserSession().getDatePattern();
        
        String showInactive = parameters.get("showInactive");
        if (!TradePortalConstants.STATUS_ALL.equals(selectedStatus))
        { 
        	dynamicWhereClause.append(" and i.supplier_portal_invoice_status = ? ");
        	sqlParams.add(selectedStatus);
        }else if (TradePortalConstants.STATUS_ALL.equals(selectedStatus))
        {
        	if(StringFunction.isBlank(showInactive) || showInactive.equals("N")){
        		dynamicWhereClause.append(" and i.supplier_portal_invoice_status NOT IN('"+TradePortalConstants.SP_STATUS_PROCESSED_BY_BANK+"','"+TradePortalConstants.SP_STATUS_INVOICE_PAID+"')");
        	}
        }
       
        if (userOrgOid != null && !userOrgOid.equals("")) {
	        dynamicWhereClause.append(" and i.a_corp_org_oid = ? ");
	        sqlParams.add(userOrgOid);
        }
        else {
            dynamicWhereClause.append(" and 1=0"); //for security no access if userOrgOid is not set
        }
        
        if (StringFunction.isNotBlank(invoiceID)) {
      	   dynamicWhereClause.append(" and upper(i.invoice_reference_id) like ? ");
      	   sqlParams.add("%"+invoiceID.toUpperCase()+"%");
         }
        
        if (StringFunction.isNotBlank(currency)) {
     	   dynamicWhereClause.append(" and i.currency_code = ? ");
     	   sqlParams.add(currency);
        }
        
       	if (StringFunction.isNotBlank(amountFrom)) {
            try {
            	 amountFrom = NumberValidator.getNonInternationalizedValue(amountFrom,getResourceManager().getResourceLocale());
              	 dynamicWhereClause.append(" and i.invoice_total_amount >= ? ");
             	 sqlParams.add(amountFrom);
            } catch (InvalidAttributeValueException e) {
         	   try {
    				medService.getErrorManager().issueError(
    						TradePortalConstants.ERR_CAT_1,
    						TradePortalConstants.INVALID_CURRENCY_FORMAT,
    						amountFrom);
    			} catch (AmsException e1) {
    				
    				e1.printStackTrace();
    			}

            }

          }

          if (StringFunction.isNotBlank(amountTo)) {
            try {
            	amountTo = NumberValidator.getNonInternationalizedValue(amountTo,getResourceManager().getResourceLocale());
            	dynamicWhereClause.append(" and i.invoice_total_amount <= ? ");
      			sqlParams.add(amountTo);
            } catch (InvalidAttributeValueException e) {
         	   try {
    				medService.getErrorManager().issueError(
    						TradePortalConstants.ERR_CAT_1,
    						TradePortalConstants.INVALID_CURRENCY_FORMAT,
    						amountTo);
    			} catch (AmsException e1) {
    				
    				e1.printStackTrace();
    			}
            }

          }

          if (StringFunction.isNotBlank(dateFrom)) {
              dynamicWhereClause.append(" and i.invoice_due_date >= TO_DATE(?,'").append(datePattern).append("')");
	          sqlParams.add(dateFrom);
          }
      
          if (StringFunction.isNotBlank(dateTo)) {
	       	  dynamicWhereClause.append(" and i.invoice_due_date <= TO_DATE(?,'").append(datePattern).append("')");
	          sqlParams.add(dateTo);
       	  }
     	
     	  if (StringFunction.isNotBlank(relatedInstrumentID)) {
         	  dynamicWhereClause.append(" and upper(i.instrument_id) like ? ");
         	  sqlParams.add("%" + relatedInstrumentID.toUpperCase() + "%");
           }
          
            String serverLocation = this.getFormManager().getServerLocation();
            try {
                CorporateOrganization userOrg = (CorporateOrganization)
                EJBObjectFactory.createClientEJB(serverLocation, "CorporateOrganization",Long.parseLong(userOrgOid));
                if (!"Y".equals(userOrg.getAttribute("allow_non_prtl_org_instr_ind"))) {
                    // Do not select instrument_oid from instrument table and therefore disable URL link if allow_non_prtl_org_instr_ind is not checked.  
                    dynamicWhereClause.append(" and -1 = ins.instrument_oid (+)");
                }
            }
            catch (Exception e) {
                throw new DataViewException(e);
            }
         
        return dynamicWhereClause.toString();
    }

    protected int setDynamicSQLValues(PreparedStatement statement, int startVarIdx,
            Map<String,String> parameters)
            throws SQLException {
    	return setSqlVarsToPreparedStmt(statement, startVarIdx, sqlParams);
    }
}
