/**
 * 
 */
package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.busobj.webbean.SessionWebBean;
import com.amsinc.ecsg.util.ResourceManager;
import com.amsinc.ecsg.web.BeanManager;

/**

 *
 */
public class BankInstrumentIDColumnVisibilityCondition extends AbstractVisibilityCondition {
private static final Logger LOG = LoggerFactory.getLogger(BankInstrumentIDColumnVisibilityCondition.class);
	
@Override
	
	public boolean execute(SessionWebBean userSession, BeanManager beanMgr, ResourceManager resMgr) {
		boolean visible=userSession.isCustNotIntgTPS();

		
		LOG.debug("Exiting BankInstrumentIDVisibilityCondition--- Customer Not Integrated with TPS flag: {} ", visible );
		
		return visible;
	}

}
