/**
 *
 */
package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;
import java.util.List;

import com.ams.tradeportal.common.TradePortalConstants;

/**
 *
 *
 */
public class TemplateBankDataView     extends AbstractDBDataView {
private static final Logger LOG = LoggerFactory.getLogger(TemplateBankDataView.class);

	private final List<Object> sqlParams = new ArrayList<>();
	
	/**
	 * Constructor
	 */
	public TemplateBankDataView() {
		super();
	}

    protected String buildDynamicSQLCriteria(Map<String,String> parameters) {


    	StringBuilder sql = new StringBuilder();

        String clientBankID = getUserSession().getClientBankOid();
        String bogID = getUserSession().getBogOid();
        String globalID = getUserSession().getGlobalOrgOid();
        String parentOrgID = getUserSession().getOwnerOrgOid();
        String ownershipLevel = getUserSession().getOwnershipLevel();
        boolean includeSubAccessUserOrg = getUserSession().showOrgDataUnderSubAccess();

		String templateName = parameters.get("templateName");
		if (templateName != null && templateName.trim().length() > 0){
			sql.append(" and upper(t.name) like ? ");
			sqlParams.add("%"+templateName.toUpperCase()+"%");
		}

        sql.append(" and p_owner_org_oid in (?,?");
        sqlParams.add(parentOrgID);
        sqlParams.add(globalID);
        
        if(ownershipLevel != null){
	        if (ownershipLevel.equals(TradePortalConstants.OWNER_BOG) || ownershipLevel.equals(TradePortalConstants.OWNER_CORPORATE)) {
	            sql.append(",?");
	            sqlParams.add(clientBankID);
	            if (ownershipLevel.equals(TradePortalConstants.OWNER_CORPORATE)){
	                sql.append(",?");
	                sqlParams.add(bogID);
	            }
	        }
        }

        if(includeSubAccessUserOrg){
           sql.append(",?");
           sqlParams.add(bogID);
        }

        sql.append(")");
      

        return sql.toString();
      }

    protected int setDynamicSQLValues(PreparedStatement statement, int startVarIdx,
            Map<String,String> parameters)
            throws SQLException {
    	
    	return setSqlVarsToPreparedStmt(statement, startVarIdx, sqlParams);
    }
}
