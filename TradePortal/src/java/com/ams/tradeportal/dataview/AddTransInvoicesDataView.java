/**
 *
 */
package com.ams.tradeportal.dataview;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.busobj.util.InvoiceUtility;
import com.ams.tradeportal.common.TPDateTimeUtility;
import com.amsinc.ecsg.util.StringFunction;

/**
 *
 *
 */
public class AddTransInvoicesDataView     extends AbstractDBDataView
{
private static final Logger LOG = LoggerFactory.getLogger(AddTransInvoicesDataView.class);
	private final List<Object> sqlParams = new ArrayList<>();
	
	/**
	 * Constructor
	 */
	public AddTransInvoicesDataView() {
		super();
	}

        @Override
    protected String buildDynamicSQLCriteria(Map<String,String> parameters) {
    

    	StringBuilder sql = new StringBuilder();

        String userOrgOid = getUserSession().getOwnerOrgOid();
        String uploadDefinitionOid = parameters.get("uploadDefinitionOid");
        String currency = parameters.get("currency");
        String beneficiaryName = parameters.get("beneficiaryName");
        String beneName = parameters.get("beneName");
        String sourceType = parameters.get("sourceType");
        String poNumber = parameters.get("poNumber");
        String amountFrom = parameters.get("amountFrom");
        String amountTo = parameters.get("amountTo");
        String dueDateFrom = parameters.get("dueDateFrom");
        String dueDateTo = parameters.get("dueDateTo");
        String invClassification = parameters.get("invClassification");
        String importIndicator = parameters.get("importIndicator");
        String financeType = parameters.get("financeType");
        String buyerBacked = parameters.get("buyerBacked");
        String paymentMethod = parameters.get("paymentMethod");
        
        if (!StringFunction.isBlank(dueDateFrom)){
        	dueDateFrom = TPDateTimeUtility.convertToDBDate(dueDateFrom, getUserSession().getDatePattern());
        }
        if (!StringFunction.isBlank(dueDateTo)){
        	dueDateTo = TPDateTimeUtility.convertToDBDate(dueDateTo, getUserSession().getDatePattern());
        }
        boolean hasPOLineItems = Boolean.parseBoolean(parameters.get("hasPOLineItems"));
        

        //parameter check start
        if(userOrgOid == null)userOrgOid="";
        if(uploadDefinitionOid == null)uploadDefinitionOid="";
        if(currency == null | "null".equals(currency)) currency="";
        if(beneficiaryName == null)beneficiaryName="";
        if(beneName == null)beneName="";
        if(sourceType == null)sourceType="";
        if(poNumber == null)poNumber="";
        if(amountFrom == null)amountFrom="";
        if(amountTo == null)amountTo="";
        if(dueDateFrom == null)dueDateFrom="";
        if(dueDateTo == null)dueDateTo="";
        if(invClassification == null)invClassification="";
        if(paymentMethod == null)paymentMethod="";
     

       	sql.append(" and A_CORP_ORG_OID = ? ");
       	sqlParams.add(userOrgOid);

       	sql.append(" and (LOAN_TYPE IN (?) "); 
       	sqlParams.add(InvoiceUtility.getLoanType(importIndicator,financeType,buyerBacked));
       	sql.append(" or LOAN_TYPE IS NULL) ");
       	
       	if (StringFunction.isNotBlank(invClassification)){
       		sql.append(" and INVOICE_CLASSIFICATION = ? ");
       		sqlParams.add(invClassification);
       	}
       	if (StringFunction.isNotBlank(paymentMethod)){
       		sql.append(" and pay_method = ? ");
       		sqlParams.add(paymentMethod);
       	}
        if(hasPOLineItems) {
        	if (!"".equals(uploadDefinitionOid)){
        		sql.append(" and a_source_upload_definition_oid = ? ");
        		sqlParams.add(uploadDefinitionOid);
        	}

        	if (!"".equals(currency)){
        		sql.append(" and currency = ? ");
        		sqlParams.add(currency);
        	}
        	sql.append(" and ben_name = ? ");
            sqlParams.add(beneficiaryName);
            sql.append(" and A_TRANSACTION_OID is null ");
        } else {
        	sql.append(" and A_TRANSACTION_OID is null");

        	// If no uploaded po line items are assigned to this shipment check if a currency
        	// was passed in.  The currency passed in would be the transaction's currency if it
        	// exists or would be the currency of manual po line items assigned to other
        	// shipments of this transaction
          	//currency = xmlDoc.getAttribute("/In/SearchForPO/currency");
        	if (!StringFunction.isBlank(currency)){
        		sql.append(" and currency = ? ");
        		sqlParams.add(currency);
           }
        }
        
     // Search by the PO Number
        if (!poNumber.equals("") ) {
            sql.append(" and upper(invoice_id) like upper(?)");
            sqlParams.add(poNumber+"%");
        }
        if (!("").equals(beneName)){
        	String name = "buyer_name";
        	if ("R".equals(invClassification)){
        		name = "buyer_name";
        	}else if ("P".equals(invClassification)){
        		name = "seller_name";
        	}
        		
            sql.append(" and upper(").append(name).append(") like upper(?)");
            sqlParams.add(beneName+"%");
        }
 
        if (!("").equals(currency)){
            sql.append(" and upper(currency) like upper(?) ");
            sqlParams.add(currency+"%");
        }
        
        if (!("").equals(amountFrom) && !("").equals(amountTo)){
            sql.append(" and amount >= ? ");
            sql.append(" and amount <= ? ");
            
            sqlParams.add(amountFrom);
            sqlParams.add(amountTo);
        }
       
        if (!("").equals(amountFrom) && ("").equals(amountTo)){
            sql.append(" and amount >= ? ");
            sqlParams.add(amountFrom);
        }
        
        if (("").equals(amountFrom) && !("").equals(amountTo)){
            sql.append(" and amount <= ? ");
            sqlParams.add(amountTo);
        }

        if (!("").equals(dueDateFrom) && !("").equals(dueDateTo)){
            sql.append(" and CASE WHEN PAYMENT_DATE IS NULL THEN DUE_DATE ELSE PAYMENT_DATE END BETWEEN ? and ? ");
            sqlParams.add(dueDateFrom);
            sqlParams.add(dueDateTo);
        }
        if (!("").equals(dueDateFrom) && ("").equals(dueDateTo)){
            sql.append(" and CASE WHEN PAYMENT_DATE IS NULL THEN DUE_DATE ELSE PAYMENT_DATE END  >= ? ");
            sqlParams.add(dueDateFrom);
        }
        if (("").equals(dueDateFrom) && !("").equals(dueDateTo)){
            sql.append(" and CASE WHEN PAYMENT_DATE IS NULL THEN DUE_DATE ELSE PAYMENT_DATE END <= ?  ");
            sqlParams.add(dueDateTo);
        }
        
    	return sql.toString();
    }

        @Override
    protected int setDynamicSQLValues(PreparedStatement statement, int startVarIdx,
            Map<String,String> parameters)
            throws SQLException {

    	return setSqlVarsToPreparedStmt(statement, startVarIdx, sqlParams);
    }
}
