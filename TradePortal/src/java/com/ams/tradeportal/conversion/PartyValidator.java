/*
 * @(#)PartyValidator.java	1.0 2002/10/29
 *
 * Copyright 2002 American Management Systems.
 */


package com.ams.tradeportal.conversion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.common.*;
import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.util.*;

/**
 * Trade Portal Party Validator class.  This class is
 * used to validate an XML document prior to submission to the
 * Reference Data Mediator.
 *
 * @version		1.0 29 Oct 2002

 */
public class PartyValidator extends Validator {
private static final Logger LOG = LoggerFactory.getLogger(PartyValidator.class);
	/*
	 * Constructors
	 */

	/**
	 * Create a Trade Portal Party Validator given the Party's Client Bank.
	 * Takes a Message Log object to log errors.
	 *
	 * @param clientBank - Party's Client Bank.
	 *
	 * @param errorLog - Message Log object to log errors.
	 */
	public PartyValidator(String clientBank,
						  MessageLog errorLog) {
		// Call Ancestor Constructor
		super(clientBank, errorLog);
	}

	/*
	 * Public Methods
	 */

	/**
	 * Perform validations on the incoming data row and document.
	 *
	 * @param dataRow - Array of parsed data fields.
	 *
	 * @param document - XML Document to be validated.
	 */
	public boolean validate(ArrayList dataRow,
							DocumentHandler document) {
		// Call Ancestor Validator
		if (!super.validate(dataRow, document)) {
			return false;
		}

		// Validate Party Type Code
		if (!checkPartyType(dataRow, document)) {
			// Validation failed
			return false;
		}

		// Document validated
		return true;
	}

	/*
	 * Local Methods
	 */

	/**
	 * Check Party Type Code where only Corporate Orgs with
	 * Allow Create Bank Party = 'Y' can create a Party with
	 * Type Code of 'BANK'.
	 *
	 * @param dataRow - Array of parsed data fields.
	 *
	 * @param document - XML Document to be validated.
	 */
	protected boolean checkPartyType(ArrayList dataRow,
									 DocumentHandler document) {
		Query query;
		String customerName;
		String createParty;
		String partyType;

		// Get Party Type Code from document
		partyType = document.getAttribute("/Party/party_type_code");
		// If Party Type is not 'BANK', no need to validate
		if ((partyType == null)
			|| (!partyType.equals(TradePortalConstants.OWNER_BANK))) {
			return true;
		}

		/*
		 * Only Corporate Orgs with Allow Create Bank Party = 'Y' are
		 * allowed to create a Party with Party Type Code = 'BANK'
		 */
		try {
			// Get Corporate Customer Name
			customerName = dataRow.get(
				TemplateMapper.DATA_FIELD_CUSTOMER).toString();
		} catch (IndexOutOfBoundsException e) {
			errorLog.error("Unable to get Corporate Customer Name field");
			return false;
		}

		// Check for empty Customer
		if ((customerName == null)
			|| (customerName.length() == 0)) {
			errorLog.error("Corporate Customer Name is blank");
			return false;
		}

		// Initialize Query
		query = new Query(errorLog);

		// User where clause for specific Customer Name
		// Query to get Allow Create Party field from data row
		createParty = query.getQueryResult("allow_create_bank_parties",
			"co.allow_create_bank_parties allow_create_bank_parties",
			"corporate_org co, client_bank cb",
			Conversion.createCustomerWhereClause(customerName, clientBank));

		// If Allow Create Bank Party is not 'Y', validation failed
		if (!createParty.equals(TradePortalConstants.INDICATOR_YES)) {
			errorLog.error("Customer is not allowed to create bank parties: "
				+ customerName);
			return false;
		}

		// Validation passed
		return true;
	}
}
