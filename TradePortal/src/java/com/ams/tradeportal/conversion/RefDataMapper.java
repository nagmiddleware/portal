/*
 * @(#)RefDataMapper.java	1.0 2002/10/29
 *
 * Copyright 2002 American Management Systems.
 */


package com.ams.tradeportal.conversion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.common.*;
import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.util.*;

/**
 * Trade Portal Reference Data Mapper class.  This class is
 * used to map a row of Reference Data into an XML document for use with
 * a Trade Portal Mediator. This mapper uses a mapper data array to
 * translate the data fields into document attributes.
 *
 * @version		1.0 29 Oct 2002

 */
public class RefDataMapper extends TemplateMapper {
private static final Logger LOG = LoggerFactory.getLogger(RefDataMapper.class);
	/*
	 * Constructors
	 */

	/**
	 * Create a Trade Portal Reference Data Mapper given a mapper name, an
	 * ArrayList of specific Reference Data mapper data,
	 * a Trade Portal User ID, and valid Client Bank.
	 * Also takes a Message Log object to log errors.
	 *
	 * @param name - Mapper name.
	 *
	 * @param mapperData - ArrayList of ArrayList rows of mapper data to
	 * translate data fields into attribute values in an XML document.
	 *
	 * @param userID - Trade Portal User ID.
	 *
	 * @param clientBank - An existing Client Bank.
	 *
	 * @param errorLog - Message Log object to log errors.
	 */
	public RefDataMapper(String name,
						 MapperData mapperData,
						 String userID,
						 String clientBank,
						 MessageLog errorLog) {
		// Call Ancestor constructor
		super(name, mapperData, userID, clientBank, errorLog);
	}


	/*
	 * Public Methods
	 */

	/**
	 * Validate incoming data row.
	 * Attempt to populate XML document with incoming data row using
	 * the Reference Data mapper data.
	 * Return XML document if successful, else null.
	 *
	 * @param dataRow - Row of parsed data fields.
	 *
	 * @return DocumentHandler, XML document generated using data row with
	 * the stored mapper data.
	 */
	public DocumentHandler populate(ArrayList dataRow) {
		DocumentHandler document;
		Query query;
		String prefix;
		String field;
		String value;

		// Validate incoming data row, exit if failure
		if (!validate(dataRow)) {
			return null;
		}

		// Log populate document with special Reference Data attributes
		errorLog.debug("Populating XML document with "
			+ mapperName + " mapper data...");

		// Call Ancestor to create XML document with auto-populated fields
		document = super.populate(dataRow);

		// If null document returned, return null
		if (document == null) {
			return null;
		}

		// Populate Reference Data template specific attributes to document
		// Get Reference Data specific Prefix
		prefix = Conversion.DOC_ROOT + mapperName;

		// Initialize Database Query
		query = new Query(errorLog);

		// Owner Op Org - Obtain from Database using Corporate Customer Name
		field = dataRow.get(DATA_FIELD_CUSTOMER).toString();
		// Use where clause for specific Customer Name
		value = query.getQueryResult("customer_oid",
			"co.organization_oid customer_oid",
			"corporate_org co, client_bank cb",
			Conversion.createCustomerWhereClause(field, clientBank));
		if (value.length() == 0) {
			errorLog.error("Unable to locate Corporate Customer: " + field
				+ " under Client Bank: " + clientBank);
			return null;
		}
		// Set Owner Org Oid
		document.setAttribute(prefix + "/owner_org_oid", value);

		// Login Info
		if (!query.performQuery("u.user_oid user_oid, sp.security_rights security_rights",
			"users u, security_profile sp",
			Conversion.createGlobalUserWhereClause(userID))) {
			// Unable to find Proponix level User ID
			errorLog.error("Unable to locate Proponix level User ID: "
				+ userID);
			return null;
		}
		// Login User oid
		document.setAttribute("/LoginUser/user_oid",
			query.getQueryResult("user_oid"));

		// Security Rights
		document.setAttribute("/LoginUser/security_rights",
			query.getQueryResult("security_rights"));

		// Log populate Reference Data document with Mapper complete
		errorLog.debug("Populating Reference Data XML document complete.");

		// Return populated XML document
		return document;
	}

	/**
	 * Validate the incoming data row against the Template mapper data.
	 *
	 * @param dataRow - Row of parsed data fields.
	 *
	 * @return boolean, whether data row is valid.
	 */
	protected boolean validate(ArrayList dataRow) {
		// Call Ancestor Validate
		if (!super.validate(dataRow)) {
			return false;
		}

		/*
		 * Check that number of incoming data fields is not greater
		 * than the number of rows for mapping.
		 */
		if (dataRow.size() > mapperData.size()) {
			// More incoming data fields than mapper rows
			errorLog.error("Incoming data row has too many fields: "
				+ dataRow.size() + ", Max allowed by " + mapperName
				+ " mapper: " + mapperData.size());
			return false;
		}

		// Validation Successful
		return true;
	}
}
