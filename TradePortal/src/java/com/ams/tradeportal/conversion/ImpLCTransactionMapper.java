/*
 * @(#)TemplateMapper.java	1.0 2002/10/29
 *
 * Copyright 2002 American Management Systems.
 */


package com.ams.tradeportal.conversion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.common.*;
import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.util.*;

/**
 * Trade Portal Import LC Transaction Mapper class.  This class is
 * used to map a row of data into an XML document for use with
 * a Trade Portal Mediator. This mapper uses a mapper data Template to
 * translate the data fields into document attributes.
 *
 * @version		1.0 29 Oct 2002

 */
public class ImpLCTransactionMapper extends TemplateMapper{
private static final Logger LOG = LoggerFactory.getLogger(ImpLCTransactionMapper.class);
	/*
	 * Constants
	 */

	/** Default Time */
	public static final String DEFAULT_TIME				= " 00:00:00";

	/* Override Terms Party Fields */

	/** Terms Party Type Code Field Index */
	public static final int TERMS_PARTY_TYPE			= 0;

	/** Terms Party Name Field Index */
	public static final int TERMS_PARTY_NAME			= 1;

	/** Use Existing Party Indicator Field Index */
	public static final int TERMS_PARTY_USE				= 2;

	/** Terms Party Address Type Field Index */
	public static final int TERMS_PARTY_ADDRESS_TYPE	= 3;

	/** Terms Party Maxiumum Number of Fields */
	public static final int TERMS_PARTY_MAXFIELDS		= 4;

	/* Address Population Types */

	/** Full Address with Phone Number */
	public static final String ADDRESS_FULL_PHONE		= "FP";

	/** Full Address */
	public static final String ADDRESS_FULL				= "F";

	/** Combined Address */
	public static final String ADDRESS_COMBINED			= "C";

	/* Organization Level Fields */

	/** Organization Level Code Field Index */
	public static final int ORG_LEVEL_CODE				= 0;

	/** Organization Level Name Field Index */
	public static final int ORG_LEVEL_NAME				= 1;

	/** Organization Oid Field Index */
	public static final int ORG_LEVEL_OID				= 2;

	/** Organization Level Maximum Number of Fields */
	public static final int ORG_LEVEL_MAXFIELDS			= 3;

	/* Parsing Literals */

	/** Field Separator */
	public static final String FIELD_SEPARATOR			= "|";

	/*
	 * Constructors
	 */

	/**
	 * Create a Trade Portal Import LC Transaction Mapper given a
	 * mapper name, an ArrayList of specific Transaction mapper data,
	 * a Trade Portal User ID, and valid Client Bank.
	 * Also takes a Message Log object to log errors.
	 *
	 * @param name - Mapper name.
	 *
	 * @param mapperData - ArrayList of ArrayList rows of mapper data to
	 * translate data fields into attribute values in an XML document.
	 *
	 * @param userID - Trade Portal User ID.
	 *
	 * @param clientBank - An existing Client Bank.
	 *
	 * @param errorLog - Message Log object to log errors.
	 */
	public ImpLCTransactionMapper(String name,
								  MapperData mapperData,
								  String userID,
								  String clientBank,
								  MessageLog errorLog) {
		// Call Ancestor constructor
		super(name, mapperData, userID, clientBank, errorLog);
	}

	/**
	 * Validate incoming data row.
	 * Attempt to populate XML document with incoming data row using
	 * the Import LC Transaction mapper data.
	 * Return XML document if successful, else null.
	 *
	 * @param dataRow - Row of parsed data fields.
	 *
	 * @param templateDoc - Template document used to extract data in order
	 * to populate the Transaction document for submission.
	 *
	 * @return DocumentHandler, XML document generated using data row with
	 * the stored mapper data.
	 */
	public DocumentHandler populate(ArrayList dataRow,
									DocumentHandler templateDoc) {
		DocumentHandler document;
		Query query;
		int i;
		String field;
		String value;
		String[][] termsParty;
		StringBuffer whereClause;

		// Validate incoming Data row.  Exit if validation fails.
		if (!validate(dataRow)) {
			return null;
		}

		// Log populate Import LC Tranasction document with Mapper
		errorLog.debug("Populating Import LC Transaction document...");

		// Check for empty Template
		if (templateDoc == null) {
			// Import LC Template is empty
			errorLog.error("Import LC Template is empty, unable to create Transaction.");
			return null;
		}

		// Call Ancestor to create XML document with auto-populated fields
		document = super.populate(dataRow);

		// If null document returned, return null
		if (document == null) {
			return null;
		}

		// Populate special Import LC Transaction XML Document attributes

		// Transaction Oid - From Template
		document.setAttribute("/Transaction/transaction_oid",
			templateDoc.getAttribute("/Transaction/oid"));

		// Template fields
		// Template Oid - From Template
		document.setAttribute("/Template/template_oid",
			templateDoc.getAttribute("/Template/oid"));
		// Template Name - From Template
		document.setAttribute("/Template/name",
			templateDoc.getAttribute("/name"));

		// Initialize Query object
		query = new Query(errorLog);

		// Instrument Oid - Obtain from Database on Template table
		field = templateDoc.getAttribute("/Template/oid");
		whereClause = new StringBuffer();
		whereClause.append("template_oid = '" + field + "'");
		value = query.getQueryResult("c_template_instr_oid",
			"c_template_instr_oid", "template", whereClause.toString());
		// Set Instrument Oid
		document.setAttribute("/Instrument/instrument_oid", value);

		// Login User fields
		// User Oid - From Template
		document.setAttribute("/LoginUser/user_oid",
			templateDoc.getAttribute("/userOid"));
		// Security Rights - From Template
		document.setAttribute("/LoginUser/security_rights",
			templateDoc.getAttribute("/securityRights"));

		// Terms fields
		// Terms Oid - Obtain from Database on Transaction table
		field = templateDoc.getAttribute("/Transaction/oid");
		whereClause = new StringBuffer();
		whereClause.append("transaction_oid = '" + field + "'");
		value = query.getQueryResult("c_cust_enter_terms_oid",
			"c_cust_enter_terms_oid", "transaction", whereClause.toString());
		// Set Terms Oid
		document.setAttribute("/Terms/terms_oid", value);

		// Overwrite attributes as necessary
		errorLog.debug("Overriding Date and Address attributes...");

		// Format dates to add default time "00:00:00"
		// Expiry Date
		correctDate(document, "/Terms/expiry_date");
		// Latest Shipment Date
		correctDate(document, "/Terms/ShipmentTermsList/latest_shipment_date");
		// FEC Maturity Date
		correctDate(document, "/Terms/fec_maturity_date");

		// Initialize Terms Party Fields
		termsParty = new String[][] {
			{TermsPartyType.BENEFICIARY, "FirstTermsParty",
				"Beneficiary Party Use Existing", ADDRESS_FULL_PHONE},
			{TermsPartyType.APPLICANT, "SecondTermsParty",
				"", ADDRESS_FULL},
			{TermsPartyType.ADVISING_BANK, "ThirdTermsParty",
				"", ADDRESS_FULL},
			{TermsPartyType.NOTIFY_PARTY, "FourthTermsParty",
				"Notify Party Use Existing", ADDRESS_COMBINED},
			{TermsPartyType.OTHER_CONSIGNEE, "FifthTermsParty",
				"Other Consignee Party Use Existing", ADDRESS_COMBINED}};

		// Loop to Re-Populate Terms with existing Parties if desired
		for (i = 0; i < termsParty.length; i++) {
			if (!overrideTermsParty(dataRow, document, termsParty[i])) {
				// Failed to overwrite a Party
				errorLog.error("Failed to re-populate "
					+ termsParty[i][TERMS_PARTY_NAME]);
				return null;
			}
		}

		// Log populate Import LC Tranasction document with Mapper complete
		errorLog.debug("Populating Import LC Transaction document complete.");

		// Return populated XML document
		return document;
	}

	/**
	 * Correct date field, if it exists, by appending default time.
	 *
	 * @param document - XML Document.
	 *
	 * @param field - Path of date attribute to be updated.
	 */
	protected void correctDate(DocumentHandler document,
							   String field)
	{
		String date;

		// Check for empty document or empty field
		if ((document == null)
			|| (field == null)
			|| (field.length() == 0)) {
			// Do nothing
			return;
		}

		// Get date field
		date = document.getAttribute(field);

		// If date field exists, append default time
		if (date != null) {
			// Append time to date
			document.setAttribute(field, date + DEFAULT_TIME);
		}
	}

	/**
	 * Check to Override a Terms Party in the incoming document
	 * with existing Party data from the Database,
	 * for the specified Party Type.  If Terms Party data was set in the
	 * document, other attributes must also be set in the XML document.
	 *
	 * @param dataRow - Incoming row of parsed data.
	 *
	 * @param document - XML document to update the Terms Party attributes.
	 *
	 * @param termsParty - Array of Terms Party fields used to determine
	 * whether an existing Terms Party should be located and used.
	 *
	 * @return boolean, whether Terms Party was re-populated successfully.
	 */
	protected boolean overrideTermsParty(ArrayList dataRow,
										 DocumentHandler document,
										 String[] termsParty)
	{
		DocumentHandler termsDoc;
		String useExisting;
		String customerName;

		// Check for Terms party existing document
		termsDoc = document.getFragment("/Terms/"
			+ termsParty[TERMS_PARTY_NAME]);
		if (termsDoc == null) {
			// Terms party not exist, no need to overwrite Party
			return true;
		}

		// Verify that Terms Party array has required number of fields
		if (termsParty.length < TERMS_PARTY_MAXFIELDS) {
			errorLog.error("Incorrect number of fields to Override Terms Party: "
				+ termsParty.length + ", Required fields: "
				+ TERMS_PARTY_MAXFIELDS);
			// Failed to update Terms Party
			return false;
		}

		// Set common Terms attributes
		// Terms Party Oid
		termsDoc.setAttribute("/terms_party_oid", "");
		// Terms Party Type
		termsDoc.setAttribute("/terms_party_type",
			termsParty[TERMS_PARTY_TYPE]);

		try {
			// Get Corporate Customer Name
			customerName = dataRow.get(DATA_FIELD_CUSTOMER).toString();
		} catch (IndexOutOfBoundsException e) {
			// Unable to get Corporate Customer Name
			errorLog.error("Unable to retrieve Corporate Customer Name");
			return false;
		}

		// Check for empty Customer Name
		if (customerName.length() == 0) {
			// Customer Name is required field
			errorLog.error("Corporate Customer Name field is blank in record");

			// Return Failure
			return false;
		}

		// Check if Party data must be retrieved from the Database
		if (termsParty[TERMS_PARTY_USE].length() == 0) {
			// Party data must be retrieved
			useExisting = TradePortalConstants.INDICATOR_YES;
		} else {
			// Check 'Use Existing' indicator for retrieving party
			try {
				// Get Use Existing Party Flag
				useExisting = dataRow.get(
					mapperData.findRow(termsParty[TERMS_PARTY_USE])).toString();
			} catch(IndexOutOfBoundsException e) {
				errorLog.error("Unable to retrieve Terms Party field: "
					+ TERMS_PARTY_USE);
				return false;
			}
		}

		// If blank or yes, populate document with existing Party from Database
		if ((useExisting.length() == 0)
			|| (useExisting.equals(TradePortalConstants.INDICATOR_YES))) {
			// Populate with a full address and phone number
			if (!populatePartyAddress(termsDoc, customerName,
				termsParty[TERMS_PARTY_ADDRESS_TYPE])) {
				// Failed to populate Party address
				return false;
			}
		}

		// Party data population complete
		return true;
	}

	/**
	 * Locate Party Address in Database and populate into Terms document.
	 * Search based on Party name and owner org, in this level order:
	 * Corporate Customer, Bank Org Group, Client Bank, and Proponix.
	 * Customer Name is used to perform the searches by level.
	 * If Party found, populate, otherwise return failure.
	 * Populate address based on Address Type.
	 *
	 * @param termsDoc - Terms document fragment from Transaction document.
	 *
	 * @param customerName - Corporate Customer Name.
	 *
	 * @param addressType - Address population type constant.
	 *
	 * @return boolean, whether Party address re-populated successfully.
	 */
	protected boolean populatePartyAddress(DocumentHandler termsDoc,
										   String customerName,
										   String addressType)
	{
		Query queryResults;
		String sValue;
		StringBuffer addressLine;

		// Locate Party info under the specified Customer
		queryResults = locatePartyAddress(termsDoc, customerName);

		// If no party found, return failure
		if (queryResults.count() < 1) {
			errorLog.error("No Party data to populate for Customer: " +
				customerName);
			return false;
		}

		// Populate Terms fragment with first Party found
		// Common attributes
		// Name
		termsDoc.setAttribute("/name",
			queryResults.getQueryResult("name"));
		// Address Line 1
		termsDoc.setAttribute("/address_line_1",
			queryResults.getQueryResult("address_line_1"));

		// Address Line 2
		termsDoc.setAttribute("/address_line_2",
			queryResults.getQueryResult("address_line_2"));

		// Attributes based on Address Type
		// Check for Full Address
		if ((addressType.equals(ADDRESS_FULL_PHONE))
			|| addressType.equals(ADDRESS_FULL)) {
			// City
			termsDoc.setAttribute("/address_city",
				queryResults.getQueryResult("address_city"));
			// State/Province
			termsDoc.setAttribute("/address_state_province",
				queryResults.getQueryResult("address_state_province"));
			// Postal Code
			termsDoc.setAttribute("/address_postal_code",
				queryResults.getQueryResult("address_postal_code"));
			// Country
			termsDoc.setAttribute("/address_country",
				queryResults.getQueryResult("address_country"));

			// Check to set Phone Number
			if (addressType.equals(ADDRESS_FULL_PHONE)) {
				termsDoc.setAttribute("/phone_number",
					queryResults.getQueryResult("phone_number"));
			}
		} else if (addressType.equals(ADDRESS_COMBINED)) {
			// Combined Address
			// Concatenate fields to form Address Line 3
				addressLine = new StringBuffer();
				// City
				addressLine.append(queryResults.getQueryResult("address_city"));
				// State/Province
				sValue =  queryResults.getQueryResult("address_state_province");
				if (sValue.length() > 0) {
					addressLine.append(", " +sValue);
				}
				// Postal Code
				sValue = queryResults.getQueryResult("address_postal_code");
				if (sValue.length() > 0) {
					addressLine.append(" " + sValue);
				}
				// Country
				sValue = queryResults.getQueryResult("address_country");
				if (sValue.length() > 0) {
					addressLine.append(" " + sValue);
				}
				// Set Address Line 3
				termsDoc.setAttribute("/address_line_3", addressLine.toString());
		} else {
			// Incorrect Address Type
			errorLog.error("Invalid Address Type chosen: " + addressType);
			return false;
		}

		// Populate complete
		return true;
	}

	/**
	* Locate Party Address in Database and populate into Terms document.
	* Search based on name and owner org, in this order:
	* Corporate Customer, Bank Org Group, Client Bank, and Proponix.
	* If Party found, return query results, else return empty query.
	*
	* @param termsDoc - Terms document fragment from Transaction document.
	*
	* @param customerName - Corporate Customer Name.
	*
	* @return Query - Query object containing results of Address query.
	*/
	protected Query locatePartyAddress(DocumentHandler termsDoc,
									   String customerName) {
		Query query;
		int i;
		int cityPos;
		String partyName;
		String customerOid;
		String bogOid;
		String clientBankOid;
		String partyNameField;
		String partyCityField;
		String[][] orgLevels;
		StringBuffer whereClause;

		// Initialize Query object
		query = new Query(errorLog);

		// Determine Customer (Corporate Org), Bank Org Group, and Client Bank Oids
		// Search using Customer Name and City
		whereClause = new StringBuffer();
		whereClause.append("co.name = '" + customerName + "'");
		whereClause.append(" and co.activation_status = '" +
			TradePortalConstants.ACTIVE + "'");
		whereClause.append(" and co.a_bank_org_group_oid = bog.organization_oid");
		whereClause.append(" and bog.activation_status = '" +
			TradePortalConstants.ACTIVE + "'");
		whereClause.append(" and bog.p_client_bank_oid = cb.organization_oid");
		whereClause.append(" and cb.otl_id = '" + clientBank + "'");
		whereClause.append(" and cb.activation_status = '" +
			TradePortalConstants.ACTIVE + "'");
		// Get Customer Oid
		customerOid = query.getQueryResult("co_oid",
			"co.organization_oid co_oid, bog.organization_oid bog_oid, cb.organization_oid cb_oid",
			"corporate_org co, bank_organization_group bog, client_bank cb",
			whereClause.toString());

		// Get Bank Org Group Oid
		bogOid = query.getQueryResult("bog_oid");

		// Get Client Bank Oid
		clientBankOid = query.getQueryResult("cb_oid");

		// Check for failed search
		if ((customerOid.length() == 0)
			|| (bogOid.length() == 0)
			|| (clientBankOid.length() ==0)) {
			errorLog.error("Corporate Customer not found in Database: "
				+ customerName + " for Client Bank: " + clientBank);
			return new Query(errorLog);
		}

		// Get Party Name
		partyName = termsDoc.getAttribute("/name");

		// Check for missing Party Name
		if ((partyName == null)
			|| (partyName.length() == 0)) {
			errorLog.error("Party Name not found in Terms section of document");
			return new Query(errorLog);
		}

		// Party Name can be a combination of Party Name and City fields
		// Search for Party Name field separator |
		cityPos = partyName.indexOf(FIELD_SEPARATOR);
		// Check to parse incoming Party Name into possible name and city
		if (cityPos >= 0) {
			try {
				// Get Party Name section
				partyNameField = partyName.substring(0, cityPos);
				// Get City section
				partyCityField = partyName.substring(cityPos + 1,
					partyName.length());

				// Check for blank Party Name section
				if (partyNameField.length() == 0) {
					// Party Name is required
					errorLog.error("Party Name section is blank in Terms section of document: "
						+ partyName);
				}
			} catch (IndexOutOfBoundsException e) {
				// Failure during parse
				errorLog.error("Failure while parsing Party Name from Terms section of document: "
					+ partyName);

				return new Query(errorLog);
			}
		} else {
			// Else only search by Party Name
			// Set Party Name
			partyNameField = partyName;
			// Set City to null, since it will not be used in search
			partyCityField = null;
		}

		// Initialize Corporate Levels array
		// Corporate Customer -> Bank Org Group -> Client Bank -> Global
		orgLevels = new String[][] {
			{TradePortalConstants.OWNER_CORPORATE, "Corporate Customer",
				customerOid},
			{TradePortalConstants.OWNER_BOG, "Bank Org Group",
				bogOid},
			{TradePortalConstants.OWNER_BANK, "Client Bank",
				clientBankOid},
			{TradePortalConstants.OWNER_GLOBAL, "Global",
				""}};

		// Loop to search for Party at different corporate levels
		for (i = 0; i < orgLevels.length; i++) {
			// Create where clause for Party Name, City, and Ownership Level
			whereClause = new StringBuffer();
			whereClause.append("p.name = '" + partyNameField + "'");
			// Check to append city search if specified
			if (partyCityField != null) {
				whereClause.append(" and p.address_city = '" + partyCityField + "'");
			}
			whereClause.append(" and p.ownership_level = '"
				+ orgLevels[i][ORG_LEVEL_CODE] +"'");
			// Check for Owner Oid if Level is not Global
			if (!orgLevels[i][ORG_LEVEL_CODE].equals(TradePortalConstants.OWNER_GLOBAL)) {
				whereClause.append(" and p.p_owner_org_oid = '"
					+ orgLevels[i][ORG_LEVEL_OID] + "'");
			}
			// If query succeeds, return results
			if ((query.performQuery("*", "party p", whereClause.toString()))
				&& (query.count() > 0)) {
				// If more than one Party is found, give a warning
				if (query.count() > 1) {
					errorLog.warning("More than one "
						+ orgLevels[i][ORG_LEVEL_NAME]
						+ " level party found for Customer: "
						+ customerName + ", Party Name: " + partyNameField
						+ ".  First party found will be used.");
				}

				// Return query results
				return query;
			}
		}

		// No Party Found
		// Check for null city
		if (partyCityField == null) {
			// Set to default
			partyCityField = "None Specified";
		}
		// Log error
		errorLog.error("No Parties found for Customer: " + customerName +
			", Party Name: " + partyNameField + ", Party City: "
			+ partyCityField);
		return new Query(errorLog);
	}

	/**
	 * Validate the incoming data row against the Template mapper data.
	 *
	 * @param dataRow - Row of parsed data fields.
	 *
	 * @return boolean, whether data row is valid.
	 */
	protected boolean validate(ArrayList dataRow) {
		// Call Ancestor Validate
		if (!super.validate(dataRow)) {
			return false;
		}

		/*
		 * Check that number of incoming data fields is not greater
		 * than the number of rows for mapping.
		 */
		if (dataRow.size() > mapperData.size()) {
			// More incoming data fields than mapper rows
			errorLog.error("Incoming data row has too many fields: "
				+ dataRow.size() + ", Max allowed by " + mapperName
				+ " mapper: " + mapperData.size());
			return false;
		}

		// Validation Successful
		return true;
	}
}
