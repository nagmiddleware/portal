/*
 * @(#)Parser.java	1.0 2002/10/29
 *
 * Copyright 2002 American Management Systems.
 */


package com.ams.tradeportal.conversion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.util.*;
import java.util.*;

/**
 * Trade Portal Parser class.  This is used to parse lines of data from CSV
 * format into unformatted fields of data.
 *
 * @version		1.0 29 Oct 2002

 */
public class Parser {
private static final Logger LOG = LoggerFactory.getLogger(Parser.class);
	/**
	 * Constants
	 */

	/* Parsing Literals */

	/** One Double Quote */
	public static final String DOUBLE_QUOTE			= "\"";

	/** Two Double Quotes */
	public static final String DOUBLE_QUOTE_TWO			= "\"\"";

	/** New Line String */
	public static final String NEWLINE_STRING			= "\\n";

	/** New Line Symbol in String Format */
	public static final String NEWLINE_CHAR				= "\n";

	/** End Token */
	public static final String END_TOKEN				= "\"";

	/** CSV End Token */
	public static final String END_TOKEN_CSV			= "\",";

	/** Character Double Quote */
	public static final char CHAR_DOUBLE_QUOTE			= '"';


	/**
	 * Members
	 */

	/** Is Parser Valid Flag */
	protected boolean objectIsValid = false;

	/** Error Log */
	protected MessageLog errorLog = null;


	/*
	 * Constructors
	 */

	/**
	 * Create a Trade Portal Parser object.
	 * Takes in a Message Log object to log errors.
	 *
	 * @param errorLog - Error log object
	 */
	public Parser(MessageLog errorLog) {
		// Check for null Error Log
		if (errorLog == null) {
			LOG.info("Error, Import LC Template Mapper Error Log is invalid");
			return;
		}
		// Store Error Log
		this.errorLog = errorLog;

		objectIsValid = true;
	}


	/*
	 * Public Methods
	 */

	/**
	 * Parse the string of data into an ArrayList of data fields.
	 * Row is in CSV format (Comma-Separated Values).
	 * If string is empty or has an invalid format, return null.
	 * CSV formatting Rules:
	 * 1) Values are separated by commas
	 * 2) If comma exists in value, surround value with ""
	 * 3) If a " exists, surround value with "" and change internal " to ""
	 * 4) If a newline string "\\n" exists, change it to a newline char '\n'
	 * ? Check for incorrect number of double quotes "
	 *
	 * @param line - Line of data.
	 *
	 * @return ArrayList, list of unformatted data fields.
	 */
	public ArrayList parseCSVLinetoArray(String line) {
		ArrayList dataRow;
		boolean done;
		boolean endToken;
		int startPos;
		int currentPos;
		int nextPos;
		int quoteCount;
		String token;

		// Check for valid Error Log
		if (!objectIsValid) {
			// Log error to console
			LOG.info("Parser is invalid");

			return null;
		}

		// Check for empty line
		if ((line == null)
			|| (line.length() == 0)) {
			return null;
		}

		// Initialize Data Array
		dataRow = new ArrayList();

		// Parse input string into individual, unformatted values
		// Loop to extract values separated by commas
		done = false;
		startPos = 0;
		currentPos = 0;
		while (!done) {
			// Initialize token
			token = "";

			try {
				// Check for end of string, add an empty token.
				if (startPos >= line.length()) {
					// Add final token and exit
					done = true;
				} else if (line.indexOf(DOUBLE_QUOTE,
					startPos) == startPos) {
					/*
					 * Else check for existence of special values in current token.
					 * If special chars exist, first char in token is a ".
					 */
					currentPos = startPos + 1;
					endToken = false;
					// Find end of token
					while (!endToken) {
						// Check for text after current position
						if (currentPos >= line.length()) {
							// Invalid format, missing end "
							return null;
						}
						// Find an end of token sequence, middle of string -> ",
						nextPos = line.indexOf(END_TOKEN_CSV, currentPos);

						/*
						 * If end of middle token not found,
						 * then check for end of final token -> "
						 */
						if (nextPos == -1) {
							// Check for final "
							if (!line.endsWith(DOUBLE_QUOTE)) {
								// Invalid format, missing end of token sequence -> exit
								return null;
							}

							// Set last position
							currentPos = line.length();

							// Final end of token sequence
							endToken = true;
							// Thus Reached end of line
							done = true;
						} else {
							/*
							 * Found sequence -> ",
							 * Check that the " is not really a ""
							 * Loop backwards to count number of consecutive "
							 * within the token, if any.
							 */
							quoteCount = 0;
							while ((nextPos - quoteCount > currentPos)
								&& (line.charAt(nextPos - 1 - quoteCount) ==
								CHAR_DOUBLE_QUOTE)) {
								quoteCount++;
							}

							/*
							 * If an even internal quote count, then it is end
							 * of token.  Odd internal quote count -> internal
							 * two double quote "",
							 */
							if (quoteCount % 2 == 0) {
								endToken = true;
							}

							// Increment next position to end of token sequence -> ,
							currentPos = nextPos + 1;
						}
					}

					// Parse current token
					token = line.substring(startPos, currentPos);

					// Convert token to unformatted string
					token = parseCSVTokentoString(token);
					// Verify that token is valid
					if (token == null) {
						return null;
					}

					// Increment start position beyond end of token sequence -> ,
					startPos = currentPos + 1;
				} else {
					/*
					 * No special chars in current token.
					 * Locate normal end of token -> ,
					 */
					currentPos = line.indexOf(',', startPos);

					// If end of token sequence found, parse token minus end sequence
					if (currentPos > -1) {
						// Parse token
						token = line.substring(startPos, currentPos);

						// Increment start position beyond end of token sequence -> ,
						startPos = currentPos + 1;
					} else {
						// End of token not exist, parse token to end of string.
						token = line.substring(startPos);

						// Reached end of line
						done = true;
					}
				}
			} catch (IndexOutOfBoundsException e) {
				/*
				 * Invalid data format,
				 * catch String.charAt() and String.substring() calls.
				 */
				errorLog.error("Unable to parse line at Start pos: " +
					startPos + ", End pos: " + currentPos);
				return null;
			}

			// Check token for invalid chars?

			// Add token to Data Array
			dataRow.add(token);
		}

		return dataRow;
	}


	/*
	 * Local Methods
	 */

	/**
	 * Parse the token of data into an unformatted string.
	 * Value is in CSV format (Comma-Separated Values)
	 * Return null string if format is invalid.
	 * CSV token unformatting Rules:
	 * 1) Token is surrounded by "", remove them
	 * 2) Change internal "" to "
	 * 3) If a newline string "\\n" exists, change it to a newline char '\n'
	 * ? Check for incorrect number of double quotes "
	 *
	 * @param token - Single token of data in CSV format.
	 *
	 * @return String, unformatted token.
	 */
	protected String parseCSVTokentoString(String token) {
		int i;
		String sReplace;
		String sReplaceWith;
		String[][] saReplace;

		// Check for empty row, should contain "<text>"
		if ((token == null)
			|| (token.length() <= 2)
			|| (!token.startsWith(DOUBLE_QUOTE))
			|| (!token.endsWith(DOUBLE_QUOTE))) {
			// Token is empty or invalid, return null
			errorLog.error("CSV token is invalid: " + token);

			return null;
		}

		// Remove surrounding "
		try {
			token = token.substring(1, token.length() - 1);
		} catch (IndexOutOfBoundsException e) {
			// Parsing error, return null
			errorLog.error("Unable to remove surrounding \" token: " + token);

			return null;
		}

		// Initialize replacement strings
		saReplace = new String[][] {
			// Replace "" with "
			{DOUBLE_QUOTE_TWO, DOUBLE_QUOTE},
			// Replace \n with '\n'
			{NEWLINE_STRING, NEWLINE_CHAR}};

		// Loop to replace strings
		for (i = 0; i < saReplace.length; i++) {
			token = StringService.change(token, saReplace[i][0],
			saReplace[i][1]);
		}


		// Return unformatted token
		return token;
	}
}
