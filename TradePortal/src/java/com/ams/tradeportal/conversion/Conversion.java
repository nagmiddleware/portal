/*
 * @(#)Conversion.java	1.0 2002/10/29
 *
 * Copyright 2002 American Management Systems.
 */


package com.ams.tradeportal.conversion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.common.*;
import com.ams.tradeportal.busobj.*;
import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;

import java.util.*;
import java.io.*;
import java.rmi.*;
import javax.ejb.*;

/**
 * Trade Portal Conversion class to import data files in CSV format into
 * the Trade Portal Database.
 *
 * @version		1.0 29 Oct 2002

 */
public class Conversion implements Runnable {
private static final Logger LOG = LoggerFactory.getLogger(Conversion.class);
	/*
	 * Constants
	 */

	/* Incoming Parameter Fields */

	/** Client Bank Parameter Index */
	public static final int PARAM_CLIENT_BANK			= 0;

	/** File Name Parameter Index */
	public static final int PARAM_FILE_NAME				= 1;

	/** File Type Parameter Index */
	public static final int PARAM_FILE_TYPE				= 2;

	/** User ID Parameter Index */
	public static final int PARAM_USER_ID				= 3;

	/** Max Number of Errors Parameter Index */
	public static final int PARAM_ERROR_MAX				= 4;

	/** Reference Data Mapper File Name Parameter Index */
	public static final int PARAM_MAPPER_FILE_NAME		= 5;

	/** Max Parameter Index Fields */
	public static final int PARAM_FIELD_MAX				= 6;

	/* Input File Types */

	/** Phrase File Type */
	public static final String FILETYPE_PHRASE			= "PHRASE";

	/** Party File Type */
	public static final String FILETYPE_PARTY			= "PARTY";

	/** Import LC Template File Type */
	public static final String FILETYPE_IMP_TEMPLATE	= "IMP_TEMPLATE";

	/** Import LC Transaction File Type */
	public static final String FILETYPE_IMP_TRANS		= "IMP_TRANS";

	/** Reference Data File Collection Type */
	public static final String FILETYPE_REFDATA			= "REF_DATA";

	/* Reference Data Mapper Names */

	/** Phrase Mapper Name */
	public static final String MAPPER_NAME_PHRASE			= "Phrase";

	/** Party Mapper Name */
	public static final String MAPPER_NAME_PARTY			= "Party";

	/** Import LC Template Mapper Name */
	public static final String MAPPER_NAME_IMPLC_TEMPLATE	= "ImpLCTemplate";

	/** Import LC Template Mapper Name */
	public static final String MAPPER_NAME_IMPLC_TRANS		= "ImpLCTransaction";

	/* Reference Data Table Headers */

	/** File Type Table Header */
	public static final String TABLE_HEADER_FILETYPE	= "Field Name";

	/** Ref Data Table Header */
	public static final String TABLE_HEADER_REFDATA		= "Preset Category";

	/* Locales */

	/** Locale - US English */
	public static final String LOCALE_ENGLISH_US		= "en_US";

	/* Error Logging */

	/** Debugging On Flag */
	public static boolean FLAG_DEBUG_LOG				= false;

	/** Error Log File Extension */
	public static final String FILE_EXT_ERROR			= ".err";

	/** Incorrect Records Log File Extension */
	public static final String FILE_EXT_RECORDS			= ".fix";

	/* Parsing Literals */

	/** Backslash Literal */
	public static final String BACKSLASH				= "\\";

	/** File Extension Literal */
	public static final String FILE_EXTENSION			= ".";

	/** Document Root Literal */
	public static final String DOC_ROOT					= "/";


	/*
	 * Members
	 */

	/** List of Reference Data File Types */
	protected ArrayList refDataFileTypes = null;

	/** List of Reference Data Table Header Types */
	protected ArrayList refDataFileHeaderTypes = null;

	/** Reference Data storage */
	protected Hashtable refDataMapperStore = null;

	/** Array of Incoming Parameters */
	protected ArrayList inputParams = null;

	/** Flag of whether Parameters are valid */
	protected boolean inputParamValid = false;

	/** Flag of whether Log Files are valid */
	protected boolean logFilesValid = false;

	/** Error Log File */
	protected MessageLog errorLog = null;

	/** Incorrect Records Log File */
	protected MessageLog recordLog = null;


	/*
	 * Constructors
	 */

	/**
	 * Default Trade Portal Conversion class Constructor.
	 *
	 * @param params - Array of string parameters entered during the
	 * initial program call.
	 */
	public Conversion(String params[]) {
		// Initialize Reference Data Types
		refDataFileTypes = new ArrayList();
		refDataFileTypes.add(FILETYPE_PHRASE);
		refDataFileTypes.add(FILETYPE_PARTY);
		refDataFileTypes.add(FILETYPE_IMP_TEMPLATE);
		refDataFileTypes.add(FILETYPE_IMP_TRANS);
		refDataFileTypes.add(FILETYPE_REFDATA);

		// Initialize Reference Data Table Header Types
		refDataFileHeaderTypes = new ArrayList();
		refDataFileHeaderTypes.add(TABLE_HEADER_FILETYPE);
		refDataFileHeaderTypes.add(TABLE_HEADER_REFDATA);

		// Initialize the Reference Data storage
		refDataMapperStore = new Hashtable();

		// Initialize the Incoming Parameter List
		inputParams = new ArrayList();

		// Initialize Log Files
		logFilesValid = initializeLogs(params);

		// If Log Files invalid, exit
		if (!logFilesValid) {
			return;
		}

		// Log Conversion object initialized
		errorLog.debug("Conversion Object Created.", true);

		// Verify Incoming Parameter List
		inputParamValid = verifyParams(params);
	}


	/*
	 * Public Methods
	 */

	/**
	 * Flag for whether program parameters are valid and
	 * whether the Error and Record log files opened successfully.
	 *
	 * @return boolean, whether program parameters and log files are valid.
	 */
	public boolean isParamValid() {
		return (inputParamValid)
			&& (logFilesValid);
	}

	/**
	 * Get specified Program Input Parameter, given index constant.
	 * Return empty string if Parameter not found.
	 *
	 * @param index - Index of desired program parameter.
	 *
	 * @return String, program parameter specified by index.
	 */
	public String getParam(int index)
	{
		try {
			// Return Program Parameter
			return inputParams.get(index).toString();
		} catch (NullPointerException e) {
			// Parameter array is null
			return "";
		} catch (IndexOutOfBoundsException e) {
			// Invalid index specified
			return "";
		}
	}

	/**
	 * Log.an error in the Error Log
	 *
	 * @param message - Message to be logged.
	 */
	public void logError(String message) {
		try {
			// Log the error to the Error Log file
			errorLog.error(message, true);
		} catch (NullPointerException e) {
			// Invalid Error Log, print error to screen
			LOG.info(message);
		}
	}

	/**
	 * Open the input data file in the path specified as a program parameter
	 * and process the data into the Trade Portal database.
	 * The file is in CSV format.
	 * Data is read in from the file line by line as separated records and
	 * are processed individually.
	 * Return whether or not processing completed fully.
	 *
	 * @return boolean, whether or not Input data file was processed fully.
	 */
	public boolean processInputDataFile() {
		BufferedReader reader;
		boolean done;
		long currentRow;
		long successRows;
		long numErrors;
		long maxErrors;
		String filePath;
		String line;

		// Log Parse input file
		errorLog.debug("Parsing input data file: " + getParam(PARAM_FILE_NAME));

		// Check if parameters are valid
		if (!inputParamValid) {
			errorLog.error("Unable to parse input file, parameters are invalid");
			return false;
		}

		try {
			// Get Maxiumum number of errors
			maxErrors = Long.parseLong(getParam(PARAM_ERROR_MAX));
		} catch (NumberFormatException e) {
			// Number is invalid
			errorLog.error("Max Number of Errors parameter is invalid: "
				+ getParam(PARAM_ERROR_MAX));

			return false;
		}

		// Get file path
		filePath = getParam(PARAM_FILE_NAME);

		// Check for empty path
		if (filePath.length() == 0) {
			errorLog.error("No input data file specified");
			return false;
		}

		// Log processing Input Data file
		errorLog.debug("Processing input data file: " + filePath);
		String filepathcheck=StringFunction.validateFilePath(filePath);
		if(filepathcheck!=null){
			try {
			// Open the file for reading
			reader = new BufferedReader(new FileReader(filePath));
			} 
			catch (FileNotFoundException e) {
			// Incorrect file path
			errorLog.error("Input file not found: " + filePath, true);
			return false;
		}
		}else{
			
			errorLog.error("Input file not found: " + filePath, true);
			return false;
			
		}
		

		// Processing file done flag
		done = false;
		// Row count
		currentRow = 1;
		// Number of successful records
		successRows = 0;
		// Number of failed records
		numErrors = 0;
		// Loop to read in and process data row by row
		while (!done) {
			try {
				// Read line from data file
				line = reader.readLine();
			} catch (IOException e) {
				// Unable to read line from file
				errorLog.error("Failed to read file: " + filePath
					+ ", Row: " + currentRow);
				return false;
			}

			// Check if end of stream.  If not,parse and process line.
			if (line == null) {
				// Log end of file
				errorLog.debug("End of Input File", true);

				// End of file, exit loop
				done = true;
			} else {
				// Log parsing and processing line
				errorLog.debug("Parsing and processing Record: " + currentRow);

				/*
				 * Attempt to parse and process line.  If failure returned,
				 * log the error and increment the error count.
				 */
				if (parseProcessLine(line)) {
					// Log success
					errorLog.debug("Parsing and processing complete for Record: "
						+ currentRow, true);

					// Increment successful record count
					successRows++;
				} else {
					// Log unparsed line
					errorLog.log("Failed on Record: " + currentRow + "\n"
						+ line);

					// Log incorrect record in Records File
					recordLog.log(line);

					// Increment failed record count
					numErrors++;

					// Check for max number of errors reached
					if (numErrors >= maxErrors) {
						// Log error
						errorLog.error("Maximum number of errors reached: "
							+ numErrors, true);

						// Done processing
						done = true;
					}
				}
			}

			// Increment row
			currentRow++;
		}

		try {
			// Close the file
			reader.close();
		} catch (IOException e) {
			// End of file, so just log warning and continue
			errorLog.warning("Unable to close file: " + filePath, true);
		}

		// Log number of successfully processed rows
		errorLog.debug("Successfully processed " + successRows + " records.");

		// Log number of failed rows
		errorLog.debug("Number of Failed records: " + numErrors);

		// Log parsing file complete
		errorLog.debug("Parsing input file complete.", true);

		// Processing complete
		return true;
	}

	/**
	 * Open the Reference Data Mapper file for parsing and import.
	 * Mapper file is in CSV format.
	 * Return whether a Hashtable containing the organized Reference Data
	 * was successfully created and stored as a member object
	 * The Hashtable will contain items having a key of
	 * File Type or Reference Data.
	 *
	 * @return boolean, whether or not Reference Data Mapper imported fully.
	 */
	public boolean importRefDataMapperFile()
	{
		ArrayList dataRow;
		ArrayList mapperData;
		BufferedReader reader;
		Hashtable mapperDataStore;
		Parser parser;
		boolean done;
		long currentRow;
		String filePath;
		String line;
		String currentRefDataType;
		String newRefDataType;
		String header;

		// Get file path
		filePath = getParam(PARAM_MAPPER_FILE_NAME);

		// Check for empty path
		if (filePath.length() == 0) {
			errorLog.error("No Reference Data mapper file specified");
			return false;
		}

		// Log Import Reference Data file
		errorLog.debug("Importing Reference Data File: " + filePath);

		// Open the file for reading
		String filepathcheck=StringFunction.validateFilePath(filePath);
		if(filepathcheck!=null){
		try {
			reader = new BufferedReader(new FileReader(filePath));
		} catch (FileNotFoundException e) {
			// Invalid file path
			errorLog.error("Reference Data file not found: " + filePath);

			return false;
		}
		}else{
			errorLog.error("Reference Data file not found: " + filePath);
			return false;
		}
		// Initialize Ref Data data storage
		mapperDataStore = new Hashtable();

		// Initialize Ref Data collection per File Type
		mapperData = new ArrayList();

		// Initialize Parser
		parser = new Parser(errorLog);

		// Initialize Ref Data Mapper Types
		currentRefDataType = "";
		newRefDataType = "";
		// Initialize current row index
		currentRow = 1;
		// Initialize done processing file flag
		done = false;
		// Loop to read in and process data row by row
		while (!done) {
			try {
				// Read line
				line = reader.readLine();
			} catch (IOException e) {
				// Unable to read line
				errorLog.error("Unable to read Reference Data file: "
					+ filePath + ", Row: " + currentRow);
				return false;
			}

			// Parse line of data into an ArrayList
			dataRow = parser.parseCSVLinetoArray(line);

			// Check to add data row to data storage
			if ((dataRow != null)
				&& (dataRow.size() > 0)) {
				/*
				 * Look for new RefData Mapper Header.
				 * If row is a known header row, then create a new Mapper.
				 * Else attempt to add row to mapper data.  Skip column
				 * header row.
				 */
				if (isNewRefData(dataRow)) {
					// Get new Ref Data type
					newRefDataType = dataRow.get(0).toString();

					// Store a previous Ref Data list if a File Type
					// is currently being imported
					if (currentRefDataType.length() > 0) {
						// Store current Ref Data
						mapperDataStore.put(currentRefDataType, mapperData);

						// Initialize new Ref Data storage
						mapperData = new ArrayList();
					}

					// Save new Ref Data header
					currentRefDataType = newRefDataType;
				} else {
					// Get first field value
					header = dataRow.get(0).toString();
					// If header is valid, then add row to Ref Data
					if ((header.length() > 0)
						&& (refDataFileHeaderTypes.indexOf(header) == -1)) {
						// Add data row to current Ref Data file
						mapperData.add(dataRow);
					}
				}
			}

			// Check if end of stream
			if (line == null) {
				// Store final Ref Data file
				if (currentRefDataType.length() > 0) {
					mapperDataStore.put(currentRefDataType, mapperData);
				}

				// Parsing complete
				done = true;
			}

			// Increment current Row index
			currentRow++;
		}

		try {
			// Close the file
			reader.close();
		} catch (IOException e) {
			// End of file, so log warning and continue
			errorLog.warning("Unable to close Reference Data file: "
				+ filePath);
		}

		// Save generated data storage
		refDataMapperStore = mapperDataStore;

		// Log Import Reference Data file Complete
		errorLog.debug("Reference Data File imported successfully", true);

		// Processing complete
		return true;
	}

	/**
	 * Create a where clause that specifies an active Corporate Customer Name
	 * and Client Bank.  Corporate Org table is shorted to "co"
	 * and Client Bank table is shorted to "cb".
	 *
	 * @param customerName - Customer Name.
	 *
	 * @param clientBank - Customer's Client Bank.
	 *
	 * @return String, where clause using Customer Name and Client Bank.
	 */
	public static String createCustomerWhereClause(String customerName,
												   String clientBank) {
		StringBuffer whereClause;

		// Initialize where clause
		whereClause = new StringBuffer();

		/*
		 * Create SQL where clause incorporating Customer Name
		 * and Client Bank.
		 */
		whereClause.append("co.name = '" + customerName + "'");
		whereClause.append(" and co.activation_status = '" +
			TradePortalConstants.ACTIVE + "'");
		whereClause.append(" and co.a_client_bank_oid = cb.organization_oid");
		whereClause.append(" and cb.otl_id = '" + clientBank + "'");
		whereClause.append(" and cb.activation_status = '" +
			TradePortalConstants.ACTIVE + "'");

		// Return where clause
		return whereClause.toString();
	}

	/**
	 * Create a where clause that specifies an active Global User ID and
	 * the user's security rights.
	 * Users table is shorted to "u" and Security Profile table is
	 * shorted to "sp".
	 *
	 * @param userID - Global User ID.
	 *
	 * @return String, where clause using Global User ID.
	 */
	public static String createGlobalUserWhereClause(String userID) {
		StringBuffer whereClause;

		// Initialize where clause
		whereClause = new StringBuffer();

		/*
		 * Create SQL where clause incorporating Global User ID and
		 * security rights.
		 */
		whereClause.append("u.user_identifier = '" + userID + "'");
		whereClause.append(" and u.activation_status = '" +
			TradePortalConstants.ACTIVE + "'");
		whereClause.append(" and u.ownership_level = '"
			+ TradePortalConstants.OWNER_GLOBAL + "'");
		whereClause.append(" and u.a_security_profile_oid = sp.security_profile_oid");

		// Return where clause
		return whereClause.toString();
	}


	/*
	 * Local Methods
	 */

	/**
	 * Parse and Process incoming line of data.
	 * Data record is in CSV format.
	 *
	 * @param line - Data row.
	 *
	 * @return boolean, whether data was parsed and processed successfully.
	 */
	protected boolean parseProcessLine(String line) {
		ArrayList dataRow;
		Parser parser;

		// Initialize parser
		parser = new Parser(errorLog);

		// Log parsing line
		errorLog.debug("Parsing line...");

		// Parse line of data into an ArrayList
		dataRow = parser.parseCSVLinetoArray(line);

		// Check for empty parsed data
		if (dataRow == null) {
			errorLog.error("Failed to parse line");

			// Failed to parse line
			return false;
		}

		// Log processing parsed line
		errorLog.debug("Processing parsed line...");

		// Process data row into XML Document and submit to Mediator
		if (!processDataRow(dataRow)) {
			errorLog.error("Failed to process parsed data row");

			// Failed to process data row
			return false;
		}

		// Log processing parsed line
		errorLog.debug("Parsing and processing line complete.");

		// Parsing and processing successful
		return true;
	}

	/**
	 * Check first field for the start of a new set of Reference Data.
	 *
	 * @param dataRow - Incoming row of Reference Data Mapper data.
	 *
	 * @return boolean, if a matching header field is found.
	 */
	protected boolean isNewRefData(ArrayList dataRow) {
		int i;
		String header;

		// Verify data in the Data Row
		if ((dataRow == null)
			|| (dataRow.size() < 1)) {
			return false;
		}

		// Check first field value for the start of a file type collection
		header = dataRow.get(0).toString();

		// Loop through known Ref Data headers
		for (i = 0; i < refDataFileTypes.size(); i++) {
			// Check to start new set of Reference Data
			if (refDataFileTypes.indexOf(header) > -1) {
				// Match found
				return true;
			}
		}

		// No matching header type found
		return false;
	}

	/**
	 * Return specified Mapper object.
	 * Return null if mapper type is invalid.
	 *
	 * @param mapperType - A string specifying the type of
	 * mapper to return.  Mappers are stored with the Mapper's table header
	 * as the key.
	 *
	 * @param mapperName - A string specifying the name of the
	 * mapper.  Mapper names can be used to identify the mapper and set
	 * mapper-specific document attributes.
	 *
	 * @return Mapper object, specific reference data mapper,
	 * or null if mapper type is invalid.
	 */
	protected Mapper getMapper(String mapperType,
							   String mapperName) {
		// Choose mapper based on type
		if ((mapperType.equals(FILETYPE_PHRASE))
		 ||(mapperType.equals(FILETYPE_PARTY))) {
			// Create and return new Ref Data Mapper
			return new RefDataMapper(mapperName,
				getMapperData(mapperType), getParam(PARAM_USER_ID),
				getParam(PARAM_CLIENT_BANK), errorLog);
		} else if (mapperType.equals(FILETYPE_IMP_TEMPLATE)) {
			// Create and return new Ref Data Mapper
			return new ImpLCTemplateMapper(mapperName,
				getMapperData(mapperType), getParam(PARAM_USER_ID),
				getParam(PARAM_CLIENT_BANK), errorLog);
		} else if (mapperType.equals(FILETYPE_IMP_TRANS)) {
			// Create and return new Ref Data Mapper
			return new ImpLCTransactionMapper(mapperName,
				getMapperData(mapperType), getParam(PARAM_USER_ID),
				getParam(PARAM_CLIENT_BANK), errorLog);
		} else {
			// Invalid mapper type
			errorLog.error("Invalid mapper type chosen: " + mapperType);
			return null;
		}
	}

	/**
	 * Return specified Mapper Data object.
	 * Return default invalid object if valid object not found.
	 *
	 * @param mapperType - A string specifying the type of mapper data
	 * to return.  Mapper data is stored with the Mapper's table header
	 * as the key.
	 *
	 * @return MapperData object, specific reference data mapper.
	 */
	protected MapperData getMapperData(String mapperType) {
		ArrayList mapperData;

		try {
			// Get Ref Data Mapper array, given the mapper name
			mapperData = (ArrayList)refDataMapperStore.get(mapperType);
		} catch (ClassCastException e) {
			// Invalid Ref Data Mapper data format
			mapperData = null;
		} catch (NullPointerException e) {
			// Null Ref Data Mapper data
			mapperData = null;
		}

		// Create and return new Ref Data Mapper object of specified type
		return new MapperData(mapperType, mapperData, errorLog);
	}

	/**
	 * Return specified document validator, given a mapper type.
	 *
	 * @param mapperType - A string specifying the type of validator
	 * to return, which corresponds to the type of data mapper being used.
	 *
	 * @return Validator object, specific document validator.
	 */
	protected Validator getValidator(String mapperType) {
		// Create a Validator based on the Type
		if (mapperType.equals(FILETYPE_PHRASE)) {
			// Phrase Validator
			return new Validator(getParam(PARAM_CLIENT_BANK),
				errorLog);
		} else if (mapperType.equals(FILETYPE_PARTY)) {
			// Party Validator
			return new PartyValidator(getParam(PARAM_CLIENT_BANK),
				errorLog);
		} else if (mapperType.equals(FILETYPE_IMP_TEMPLATE)) {
			// Import LC Template Validator
			return new Validator(getParam(PARAM_CLIENT_BANK),
				errorLog);
		} else {
			// Invalid document Type
			errorLog.error("Invalid document Validator type: " + mapperType);
			return null;
		}
	}

	/**
	 * Process the incoming parsed data row.
	 * Choose a process based on the File Type parameter.
	 * Map the data to an XML Document and submit the document to
	 * a Mediator based on the File Type.
	 *
	 * @param dataRow - List of parsed data fields.
	 *
	 * @return boolean, whether row of data processed successfully.
	 */
	protected boolean processDataRow(ArrayList dataRow) {
		DocumentHandler outputDoc;
		long templateOid;
		String fileType;

		// Check for null data
		if (dataRow == null) {
			return false;
		}

		// Get the File Type parameter
		fileType = getParam(PARAM_FILE_TYPE);

		// Choose a submission process based on File Type
		if (fileType.equals(FILETYPE_PHRASE)) {
			// Map Phrase Data to document and submit
			if(submitDataRow(dataRow, fileType,
				MAPPER_NAME_PHRASE, null, "RefDataMediator") == null) {
				// Submission failed
				return false;
			}
		} else if (fileType.equals(FILETYPE_PARTY)) {
			// Map Party Data to document and submit
			if (submitDataRow(dataRow, fileType,
				MAPPER_NAME_PARTY, null, "RefDataMediator") == null) {
				// Submission failed
				return false;
			}
		} else if (fileType.equals(FILETYPE_IMP_TEMPLATE)) {
			// Import LC Template Mapper, get output document
			outputDoc = submitDataRow(dataRow, fileType,
				MAPPER_NAME_IMPLC_TEMPLATE, null, "CreateTransactionMediator");

			// If Template output document is empty, submission failed, exit
			if (outputDoc == null) {
				return false;
			}

			try {
				/*
				 * Store the Template Oid from the Document in case
				 * the Template needs to be deleted
				 */
				templateOid = outputDoc.getAttributeLong("/Template/oid");
			}
			catch (AmsException e)
			{
				// Unable to get Template Oid
				errorLog.error("Unable to get Template Oid from Import LC Template");
				return false;
			}

			// Submit template document to the Import LC Transaction Mediator
			outputDoc = submitDataRow(dataRow, FILETYPE_IMP_TRANS,
				MAPPER_NAME_IMPLC_TRANS, outputDoc, "TransactionMediator");

			/*
			 * If Transaction output document is empty,
			 * the previously created Template must be removed
			 */
			if (outputDoc == null) {
				// Failed to create Transaction -> delete the Template
				errorLog.error("Failed to create Transaction, deleting parent Template with Oid: "
					+ templateOid);

				// Delete the generated Template
				deleteTemplate(templateOid);

				// Return failure
				return false;
			}
		} else {
			// No matching File Type -> Failure
			errorLog.error("Incorrect File Type: " + fileType);
			return false;
		}

		// Data Row processed successfully
		return true;
	}

	/**
	 * Map the incoming data to an XML Document and submit
	 * the document to the specified mediator.
	 *
	 * @param dataRow - Row of parsed data fields.
	 *
	 * @param - mapperType - Type of Reference Data Mapper used to
	 * populate the XML document.
	 *
	 * @param - mapperName - Name of Reference Data Mapper used for
	 * attribute population in the XML document.
	 *
	 * @param templateDoc - Template document which can be used by the mapper
	 * to populate attributes with prior data.
	 *
	 * @param - mediatorName - Specified Mediator through which the data
	 * is submitted.
	 *
	 * @return boolean, whether the Reference Data was submitted to the
	 * Reference Data Mediator successfully.
	 */
	protected DocumentHandler submitDataRow(ArrayList dataRow,
											String mapperType,
											String mapperName,
											DocumentHandler templateDoc,
											String mediatorName)
	{
		DocumentHandler inputDoc;
		DocumentHandler outputDoc;
		MediatorCaller mediatorCaller;
		Mapper mapper;
		Validator validator;

		// Log submit Import LC Template Data
		errorLog.debug("Process Data through " + mediatorName
			+ " Mediator Caller using " + mapperName + " mapper ...");

		// Check for null data
		if (dataRow == null) {
			return null;
		}

		// Create a data to document mapper
		mapper = getMapper(mapperType, mapperName);
		// Check for null mapper
		if (mapper == null) {
			return null;
		}

		// Create XML Document using Mapper
		inputDoc = mapper.populate(dataRow, templateDoc);

		// Check for empty Document
		if (inputDoc == null) {
			// Failed to populate the document
			errorLog.error("Failed to populate the document for submission to "
				+ mediatorName);
			return null;
		}

		// Get a document validator
		validator = getValidator(getParam(PARAM_FILE_TYPE));
		// Check for empty Validator
		if (validator == null) {
			// Invalid validator
			return null;
		}

		// Validate the document prior to submission
		if (!validator.validate(dataRow, inputDoc)) {
			// Validation failed
			errorLog.error("Document being submitted to "
				+ mediatorName + " is invalid");
			return null;
		}

		// Create a Mediator Caller
		mediatorCaller = new MediatorCaller(mediatorName, errorLog);

		// Process the document through the Mediator Caller
		outputDoc = mediatorCaller.processDoc(inputDoc);

		// Check for valid output document
		if (outputDoc == null) {
			errorLog.error("Failed while submitting document to "
				+ mediatorName + " Mediator Caller");
			return null;
		}

		// Log submit Import LC Template Data
		errorLog.debug("Processing Data through " + mediatorName
			+ " Mediator Caller complete.");

		// Return output document
		return outputDoc;
	}

	/**
	 * Delete the specified Template from the Database.
	 *
	 * @param templateOid - Oid of Template to be deleted.
	 *
	 * @param boolean, whether Template was deleted successfully.
	 */
	protected boolean deleteTemplate(long templateOid)
	{
		ClientServerDataBridge cSDB;
		JPylonProperties jPylonProperties;
		Template template;
		String ejbServerLocation;

		// Log external delete template
		errorLog.debug("Deleting Template with Oid: " + templateOid
			+ "...");

		// Verify Template Oid
		if (templateOid == 0) {
			errorLog.error("Template Oid not present to delete Import LC Template");
			return false;
		}

		try {
			// Determine the server location
			jPylonProperties = JPylonProperties.getInstance();
			ejbServerLocation = jPylonProperties.getString ("serverLocation");
		} catch (AmsException e) {
			// Failed to locate server
			errorLog.error("Unable to determine server location");
			return false;
		}

		// Log server location
		errorLog.debug("Server Location: " + ejbServerLocation);

		// Create the CSDB - needed to create the mediator
		cSDB = new ClientServerDataBridge();
		// Set locale for US english language
		cSDB.setLocaleName(LOCALE_ENGLISH_US);

		try {
			// Create the mediator using the specified type and locale
			template = (Template)EJBObjectFactory.createClientEJB(
				ejbServerLocation, "Template", cSDB);

			// Check that mediator was created
			if (template == null) {
				errorLog.error("Unable to create Template");
				return false;
			}
		} catch (AmsException e) {
				errorLog.error("Unable to create Template");

			// Clear mediator
			template = null;
		} catch (RemoteException e) {
			errorLog.error("Unable to connect to server with a Template");

			// Clear mediator
			template = null;
		}

		try {
			// Get a handle to an instance of the template.
			// Get the Template data for this Oid
			if(template!=null){
				template.getData(templateOid);

				// Delete the Template from the Database
				template.delete();	
			}			
		} catch (AmsException e) {
			// Unable to delete the Template
			errorLog.error("Unable to delete the Template with Oid: "
				+ templateOid);

			// Clear Template
			template = null;
		} catch (RemoteException e) {
			// Unable to delete the Template
			errorLog.error("Unable to connect to server to delete the Template with Oid: "
				+ templateOid);

			// Clear Template
			template = null;
		}

		try {
			// Remove the Template EJB if it exists
			if (template != null) {
				template.remove();
			}
		} catch (RemoteException e) {
			// Log warning and continue
			errorLog.warning("Unable to close Template EJB on the server, continuing...");
		} catch (RemoveException e) {
			// Log warning and continue
			errorLog.warning("Unable to close Template EJB, continuing...");
		}

		// Log delete template complete
		errorLog.debug("Delete Template complete.");

		return true;
	}

	/**
	 * Verify input arguments.
	 * If valid, store the incoming parameters.
	 *
	 * @param args - Array of program parameters.
	 *
	 * @return boolean, whether parameters are valid.
	 */
	protected boolean verifyParams(String params[])
	{
		Query query;
		int i;
		int maxErrors;
		String value;
		StringBuffer whereClause;

		// Log verify paramters
		errorLog.debug("Verifying Program Parameters...");

		// Check for argument list
		if (params == null) {
			errorLog.error("Parameter array is null.  Conversion verification failed."
				+ PARAM_FIELD_MAX, true);
			return false;
		}
		// Check for the correct number of arguments
		if (params.length != PARAM_FIELD_MAX) {
			errorLog.error("Incorrect number of params: " + params.length
				+ ", Required number: " + PARAM_FIELD_MAX, true);
			return false;
		}

		// Verify the parameters
		// Initialize Query object
		query = new Query(errorLog);

		// Note: Input File name verified when opening the data file
		// Note: Reference Data File name verified when opening the data file
		// Client Bank
		whereClause = new StringBuffer();
		whereClause.append("cb.otl_id = '" + params[PARAM_CLIENT_BANK] + "'");
		whereClause.append(" and cb.activation_status = '" +
			TradePortalConstants.ACTIVE + "'");
		value = query.getQueryResult("organization_oid",
			"cb.organization_oid organization_oid", "client_bank cb",
			whereClause.toString());
		if (value.length() == 0) {
			errorLog.error("Invalid Trade Portal Client Bank: " +
				params[PARAM_CLIENT_BANK], true);
			return false;
		}

		// User ID - Must be valid Proponix Level User
		value = query.getQueryResult("user_oid", "u.user_oid user_oid",
			"users u, security_profile sp",
			createGlobalUserWhereClause(params[PARAM_USER_ID]));
		if (value.length() == 0) {
			errorLog.error("Invalid Trade Portal Proponix User ID: "
				+ params[PARAM_USER_ID], true);
			return false;
		}

		// File Type
		if (refDataFileTypes.indexOf(params[PARAM_FILE_TYPE]) < 0) {
			errorLog.error("Invalid file type: "
				+ params[PARAM_FILE_TYPE], true);
			return false;
		}
		// Max number of errors
		try {
			maxErrors = Integer.parseInt(params[PARAM_ERROR_MAX]);
		} catch (NumberFormatException e) {
			errorLog.error("Invalid Max Number of Errors: "
				+ params[PARAM_ERROR_MAX], true);
			return false;
		}

		// Store the incoming parameters
		for (i = 0; i < PARAM_FIELD_MAX; i++) {
			inputParams.add(params[i]);
		}

		// Log parameters
		errorLog.debug("Verified Program Parameters: "
			+ inputParams.toString(), true);

		// Parameters verified
		return true;
	}

	/**
	 * Initialize Error Log and Incorrect Record Log Files
	 *
	 * @param params - List of parameters.
	 *
	 * @return boolean, whether log files were initialized successfully.
	 */
	protected boolean initializeLogs(String params[]) {
		String filePath;

		// Check for file name parameter
		if (params.length <= PARAM_FILE_NAME) {
			LOG.info("Error: Incorrect number of parameters: "
				+ params.length + ", Required number: " + PARAM_FIELD_MAX);
			return false;
		}

		// Get file path
		filePath = params[PARAM_FILE_NAME];
		// Check for empty path
		if ((filePath == null)
			|| (filePath.length() == 0)) {
			LOG.info("Error: Empty file path parameter");
			return false;
		}

		// Open Error Log
		errorLog = openLog(filePath, FILE_EXT_ERROR);
		// Verify Error Log
		if (!errorLog.isOpen()) {
			// Log failed to open
			return false;
		}

		// Open Incorrect Record Log
		recordLog = openLog(filePath, FILE_EXT_RECORDS);
		// Verify Reocrd Log
		if (!recordLog.isOpen()) {
			// Log failed to open
			return false;
		}

		// Record error file names
		errorLog.debug("Error Log: " + errorLog.getFilePath()
			+ "\nIncorrect Record Log: " + recordLog.getFilePath());

		// Log files initialized
		return true;
	}

	/**
	 * Initialize a Log File given a file path and return it.
	 *
	 * @param filePath - Log file path.
	 *
	 * @return MessageLog, Log file for recording messages.
	 */
	protected MessageLog openLog(String filePath,
								 String fileExtension) {
		MessageLog logFile;
		String fullPath;

		// Get Log File path
		fullPath = truncateFilePath(filePath);

		// Open Log file
		logFile = new MessageLog(fullPath + fileExtension,
			FLAG_DEBUG_LOG);

		// Check for valid file
		if (!logFile.isOpen()) {
			LOG.info("Error: Unable to open Log File: "
				+ filePath + fileExtension);
			return new MessageLog();
		}

		// Return log file
		return logFile;
	}

	/**
	 * Truncate extension from file path and return it
	 *
	 * @param filePath - File path to be truncated
	 *
	 * @return String, File path with file extension removed.
	 */
	protected String truncateFilePath(String filePath) {
		int directoryPos;
		int extensionPos;
		String newPath;

		// Convert File Name to Log File name
		// Get possible directory marker '\'
		directoryPos = filePath.lastIndexOf(BACKSLASH);
		// Get location of extension
		extensionPos = filePath.lastIndexOf(FILE_EXTENSION);
		// Check if extension exists in file name
		if ((extensionPos > directoryPos)
			&& (extensionPos >= 0)) {
			try {
				// Replace file extension with Log file extension
				newPath = filePath.substring(0, extensionPos);
			} catch (IndexOutOfBoundsException e) {
				// Log error
				LOG.info("Unable to remove extension from file path: "
					+ filePath);

				// Use incoming path
				newPath = filePath;
			}
		} else {
			// Else use full file path
			newPath = filePath;
		}

		// Return truncated file path
		return newPath;
	}

	/**
	 * Print out ArrayList Data.
	 *
	 * @param data = List of data rows.
	 */
	protected void printArrayList(ArrayList data) {
		ArrayList dataRow;
		int i;

		// Verify incoming array
		if (data == null) {
			errorLog.debug("Array Table is empty");
			return;
		}

		// Loop to print out data
		for (i = 0; i < data.size(); i++) {
			try {
				// Extract ArrayList contain a row of data
				dataRow = (ArrayList)data.get(i);
			} catch (ClassCastException e) {
				errorLog.debug("Invalid data row: " + (i + 1));

				// Skip to next row
				continue;
			}

			// Check for Empty array
			if (dataRow == null) {
				errorLog.debug("Empty Row: " + (i + 1));
				// Skip to next row
				continue;
			}

			// Print row
			errorLog.debug("Row " + (i + 1) + ": " + dataRow.toString());
		}

		// Print complete
		return;
	}

	/**
	 * Print Reference Data to Error Log
	 */
	public void printRefData() {
		// Print out Ref Data to screen
		// Phrase Data
		errorLog.debug("Printing Phrase Data...\n");
		printArrayList(getMapperData(FILETYPE_PHRASE).getData());
		errorLog.debug("\nPhrase Data Printing Complete.", true);
		// Party Data
		errorLog.debug("Printing Party Data...\n");
		printArrayList(getMapperData(FILETYPE_PARTY).getData());
		errorLog.debug("\nParty Data Printing Complete.", true);
		// Import LC Template Data
		errorLog.debug("Printing Import LC Template Data...\n");
		printArrayList(getMapperData(FILETYPE_IMP_TEMPLATE).getData());
		errorLog.debug("\nImport LC Template Data Printing Complete.", true);
		// Import LC Transaction Data
		errorLog.debug("Printing Import LC Transaction Data...\n");
		printArrayList(getMapperData(FILETYPE_IMP_TRANS).getData());
		errorLog.debug("\nImport LC Transaction Data Printing Complete.", true);
		// Reference Data
		errorLog.debug("Printing Reference Data...\n");
		printArrayList(getMapperData(FILETYPE_REFDATA).getData());
		errorLog.debug("\nReference Data Printing Complete.", true);
	}


	/*
	 * Main Program
	 */

	/**
	 * Main Program Run Call to upload a data file into the Trade Portal Database.
	 *
	 * @param argc - Array of program parameters.
	 */
	public void run(){
		// Check for valid parameters
		if (!isParamValid()) {
			// One or more parameters are invalid, exit
			return;
		}

		// Import Reference Data and verify returned data storage
		if (!importRefDataMapperFile()) {
			logError("Parsing Reference Data CSV file failed.");
			return;
		}

		// Test Print Reference Data
		//oConversion.printRefData();
		//if (true) return;

		// Parse the input data file
		processInputDataFile();
	}
}
