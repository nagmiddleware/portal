package com.ams.tradeportal.agents;


import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.RemoveException;

import com.ams.tradeportal.busobj.OutgoingInterfaceQueue;
import com.ams.tradeportal.common.MessageType;
import com.ams.tradeportal.common.TradePortalConstants;
import com.ams.tradeportal.common.XMLUtility;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.ClientServerDataBridge;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.frame.EJBObjectFactory;
import com.amsinc.ecsg.frame.ErrorManager;
import com.amsinc.ecsg.frame.Mediator;
import com.amsinc.ecsg.util.DateTimeUtility;
import com.amsinc.ecsg.util.DocumentHandler;


/**
 *   Here goes detailed description of OutboundAgent functionality
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class OutboundAgent extends TradePortalAgent
{
//private static final Logger LOG = LoggerFactory.getLogger(OutboundAgent.class);

   private  String  queue_oid;              // this field is set by Trade Portal and Middleware teams
   private  String  date_sent;              // this field is set by Trade Portal and Middleware teams
   private  String  status;                 // this field is set by Trade Portal and Middleware teams
   private  String  confirm_status;
   private  String  msg_text;
   private  String  packaging_error_text;
   private  String  msg_type;               // this field is set by Trade Portal and Middleware teams
   protected  String  outgoingQueueStatusToProcess = STATUS_START;


   @Override
   public void setAgentId(String agentID)
   {
	   super.setAgentId(agentID);
                // W Zhu Rel 8.2 T3600015221 use configurable outgoingQueueStatusToProcess                
                try {
                    outgoingQueueStatusToProcess = properties.getString("OutgoingQueueStatusToProcess").trim();
                    System.out.println("OutgoingQueueStatusToProcess="+outgoingQueueStatusToProcess);
                }
                catch (java.util.MissingResourceException e) {
                    
                }
   }


   /**   This method is an actual implementation if TradePortalAgent's lookForWorkAndDoIt().
    *    This method is called every 10 seconds or so. Actual frequency is softcoded through
    *    agentTimerInterval parameter in AgentConfiguration.properties file.
    *    It returns -1 if exception occured during processing, or MQ Series became unaccessible,
    *    or some other problem occured. It returns 1 if everything went successfully.
    *    @return int
    */
   @Override
    protected int lookForWorkAndDoIt() throws RemoteException, RemoveException {

        try {

            ////////////////////////////////////////////////
            // BEGIN PROCESSING OF:    isOrphan == TRUE   //
            ////////////////////////////////////////////////
            if (isOrphan == true) {
                /*
                Since having AGENT_ID as a column on outgoing_queue table prevents other agents from
                accessing same row, and there are no threads in current implementation of TradePortalAgent:
                1.  no need to selecting orphans "for update"
                2.  no need to setAutoCommit(false)
                */

               /*Modified the sql so that the oldest row is picked up.
                 using just rownum = 1 doesn't always return the oldest row. Hence
                 sorting it by date_received and then using rownum = 1 to make sure
                 only one row is returned
                */
		 	    //MDB CR-609 11/30/10 Begin
                String selectOrphanSQL = "Select queue_oid from (Select queue_oid from outgoing_queue where agent_id=? and " +
                                         "(status=? or status=?) and date_created <= TO_DATE('" +
                                         DateTimeUtility.getGMTDateTime(false) + "','MM/DD/YYYY HH24:MI:SS') order by date_created) where rownum = 1";
		 	    //MDB CR-609 11/30/10 End
               
                query.setSQL(selectOrphanSQL, new Object[]{AGENT_ID, STATUS_INITIALIZED, STATUS_PACKAGED});
                query.getRecords();

                ////////////////////////////////////////////////
                // BEGIN PROCESSING OF:    ORPHAN FOUND       //
                ////////////////////////////////////////////////
                if (query.getRecordCount() == 1){
                    isOrphan = true;
                    queue_oid = query.getRecordValue("queue_oid");
                    logInfo("Orphan found! Start processing orphan " + queue_oid + "...");
                    outgoing_queue = (OutgoingInterfaceQueue) EJBObjectFactory.createClientEJB(ejbServerLocation, "OutgoingInterfaceQueue", Long.parseLong(queue_oid));
                    msg_type    = outgoing_queue.getAttribute("msg_type");
                    status      = outgoing_queue.getAttribute("status");


                ////////// START PROCESS: STATUS_INITIALIZED /////////
                if (status.equals(STATUS_INITIALIZED)){
                    int processSuccess = processAfterStatusInitialized();
                    logInfo("Finished processing 'Initialized' Orphan");
                    return processSuccess;
                }
                ////////// END PROCESS: STATUS_INITIALIZED  //////////

					// //////// START PROCESS: STATUS_PACKAGED ////////////
					if (status.equals(STATUS_PACKAGED)) {
						int processSuccess = processAfterStatusPackaged();
						logInfo("Finished processing 'Packaged' Orphan");
						return processSuccess;
					}
					// //////// END PROCESS: STATUS_PACKAGED //////////////

            }
            ////////////////////////////////////////////////
            // END PROCESSING OF:    ORPHAN FOUND         //
            ////////////////////////////////////////////////


            ////////////////////////////////////////////////
            // BEGIN PROCESSING OF:    ORPHAN NOT FOUND   //
            ////////////////////////////////////////////////
            else{
                logInfo("No orphans found");
                isOrphan = false;
                // CR-451 TRudden 01/28/2009 Begin -- Changes to skip sleep period when more work exists
                //return success here, so Agent immediately tries to process records with STATUS_RECEIVED
                return AGENT_SUCCESS;
             // CR-451 TRudden 01/28/2009 End
            }

            ////////////////////////////////////////////////
            // END PROCESSING OF:    ORPHAN NOT FOUND     //
            ////////////////////////////////////////////////
            }
        }
        catch(AmsException e){
            logError("While processing orphan", e);
            return AGENT_FAILURE;
        }
        ////////////////////////////////////////////////
        // END PROCESSING OF:   isOrphan == TRUE      //
        ////////////////////////////////////////////////



        ////////////////////////////////////////////////
        // BEGIN PROCESSING OF: isOrphan == FALSE     //
        ////////////////////////////////////////////////
        /*
        Even though having agent_id as a column on outgoing_queue table prevents other agents from
        accessing same row, and there are no threads in current implementation of TradePortalAgent,
        multiple agents can run on the same JVM.
        Use following settings till status is updated to STATUS_INITIALIZED, and agent_id is
        updated to AGENT_ID.
        1. need to selecting new message "for update"
        2. need to setAutoCommit(false)
        */
        try {

           /*Modified the where clause so that the oldest row is picked up.
           using just rownum = 1 doesn't always return the oldest row. Hence
           sorting it by date_received and then using rownum = 1 to make sure
           only one row is returned
           */
        	List<Object> selectSQLParams = new ArrayList<Object>();
        	selectSQLParams.add(outgoingQueueStatusToProcess);
        	List<Object> updateSQLParams = new ArrayList<Object>();
        	updateSQLParams.add(AGENT_ID);
        	updateSQLParams.add(STATUS_INITIALIZED);

	 	    //MDB CR-609 11/30/10 Begin
            long queue_oidLong = DatabaseQueryBean.selectForUpdate( "outgoing_queue",
                                                                    "queue_oid",
                                                                    "queue_oid = (select queue_oid from (select queue_oid from outgoing_queue where agent_id is NULL and status=? and date_created <= TO_DATE('" +  DateTimeUtility.getGMTDateTime(false) +
                                         							"','MM/DD/YYYY HH24:MI:SS') order by date_created) where rownum = 1)",
                                                                    "agent_id = ?, status= ?",
                                                                    false, false, selectSQLParams, updateSQLParams);
	 	    //MDB CR-609 11/30/10 End

            ////////////////////////////////////////////////
            // BEGIN PROCESSING OF: NEW MESSAGE FOUND     //
            ////////////////////////////////////////////////
            //logInfo("Select a new message with queue_oid = " + queue_oidLong);
            if(queue_oidLong != 0){
                logInfo("New message found. Start processing new message " + queue_oidLong + "...");
                outgoing_queue = (OutgoingInterfaceQueue) EJBObjectFactory.createClientEJB(ejbServerLocation, "OutgoingInterfaceQueue", queue_oidLong);
                msg_type = outgoing_queue.getAttribute("msg_type");

                int processSuccess = processAfterStatusInitialized();
                logInfo("Finished processing new message");
                return processSuccess;
            }
        }
        catch(AmsException e){
            logError("While processing new message", e);
            return AGENT_FAILURE;
        }
            ////////////////////////////////////////////////
            // END PROCESSING OF: NEW MESSAGE FOUND       //
            ////////////////////////////////////////////////

            ////////////////////////////////////////////////
            // BEGIN PROCESSING OF: NEW MESSAGE NOT FOUND //
            ////////////////////////////////////////////////
        /*ivalavala -- Begin Modify -- 06/04/2002 IR#IDUC051660872
         Modified HistorySQL not to include messages with status='E' and
         confirm status='E', this facilitates easy resubmission of the messages.
        */
        try{
            String historySQL = "Select queue_oid from outgoing_queue where " +
                                    "agent_id=? " +
                                      " and (status=? " +
                                        " and (msg_type=? "    +
                                            " or msg_type=? "    +                  //CR-593 Rel. 7.0
                                            " or msg_type=? "    + 						//CR-707 Rel 8.0
                                            " or msg_type=? "    +
                                            " or msg_type=? "    +   //SHR CR708 Rel8.1.1
											" or confirm_status=? ))"    +
                                    " and rownum=1" ;
            query.setSQL(historySQL, new Object[]{AGENT_ID, STATUS_COMPLETED, MessageType.CONFIRMATION, MessageType.PAYBACK, MessageType.POM, MessageType.BULKINVUPL,MessageType.INM,CONFIRM_STATUS_COMPLETED});
            query.getRecords();

            ////// BEGIN PROCESSING: NO MESSAGE TO ARCHIVE /////////
            if (query.getRecordCount() != 1){
              // CR-451 TRudden 01/28/2009 Begin -- Changes to skip sleep period when more work exists
                return AGENT_NO_RECORDS_TO_PROCESS;
             // CR-451 TRudden 01/28/2009 End
            }
            ////// END PROCESSING: NO MESSAGE TO ARCHIVE ///////////


            ////// BEGIN PROCESSING: MESSAGE TO ARCHIVE ////////////
            logInfo("Processed message found. Start archiving ... ");
            queue_oid = query.getRecordValue("queue_oid");
            outgoing_queue = (OutgoingInterfaceQueue) EJBObjectFactory.createClientEJB(ejbServerLocation, "OutgoingInterfaceQueue", Long.parseLong(queue_oid));
            outgoing_queue.moveToHistory();
            logInfo("Finished archiving processed message");
            return AGENT_SUCCESS;
            ////// BEGIN PROCESSING: MESSAGE TO ARCHIVE ////////////

            ////////////////////////////////////////////////
            // END PROCESSING OF: NEW MESSAGE NOT FOUND   //
            ////////////////////////////////////////////////
        }
        catch(AmsException e){
            logError("While archiving processed message", e);
            return AGENT_FAILURE;
        }
        ////////////////////////////////////////////////
        // END PROCESSING OF: isOrphan == FALSE       //
        ////////////////////////////////////////////////

   }// end of lookForWorkAndDoIt()






   protected int processAfterStatusInitialized() throws RemoteException, RemoveException {

      Mediator somePackagerMediator = null;
      DocumentHandler   inputDoc                = new DocumentHandler();
      DocumentHandler   outputDoc               = new DocumentHandler();
      boolean   transactionSuccess      = true;

      //capture the pack end date/time
      String datetime_pack_start = "";
      try {
          // W Zhu CR-564 6.1 12/10/2010 pass false for override.  Do not use override datetime, which stays the same and makes pack_seconds = 0.
          datetime_pack_start = DateTimeUtility.getGMTDateTime(false, false);
      }
      catch (AmsException e){
          //log and ignore - non-critical processing
          logError("Problem obtaining pack start date time. Ignoring.", e);
      }
      try {
          outgoing_queue.setAttribute("datetime_pack_start",datetime_pack_start);
          outgoing_queue.save();
      }
      catch (AmsException ex) {
          //log and ignore - non-critical processing
          logError("Problem saving pack start date time. Ignoring.",ex);
       }

      // Create an instance of packager depending on msg_type
      if (determinePackager(msg_type) == null){
            logError("No packager exists for this msg_type: " + msg_type);
            mqWrapper.attemptToPutMessageOnQueue("No packager exists for this msg_type: " + msg_type, mqWrapper.mqErrorQueue);
            return AGENT_FAILURE;
      }
      try {
            somePackagerMediator = (Mediator)EJBObjectFactory.createClientEJB(ejbServerLocation, determinePackager(msg_type));
      }
      catch(AmsException e){
            logError("While creating an instance of client EJBs: somePackagerMediator", e);
            return AGENT_FAILURE;
      }

      try{
            inputDoc.setAttribute("/In/packaging_error_text",     outgoing_queue.getAttribute("packaging_error_text"));
            inputDoc.setAttribute("/In/unpackaging_error_text",   outgoing_queue.getAttribute("unpackaging_error_text"));
            inputDoc.setAttribute("/In/msg_type",                 outgoing_queue.getAttribute("msg_type"));
            inputDoc.setAttribute("/In/message_id",               outgoing_queue.getAttribute("message_id"));
            inputDoc.setAttribute("/In/reply_to_message_id",      outgoing_queue.getAttribute("reply_to_message_id"));
            inputDoc.setAttribute("/In/mail_message_oid",         outgoing_queue.getAttribute("mail_message_oid"));
            inputDoc.setAttribute("/In/instrument_oid",           outgoing_queue.getAttribute("instrument_oid"));
            inputDoc.setAttribute("/In/transaction_oid",          outgoing_queue.getAttribute("transaction_oid"));
            inputDoc.setAttribute("/In/process_parameters",       outgoing_queue.getAttribute("process_parameters")); 
            inputDoc.setAttribute("/In/queue_oid", 				  outgoing_queue.getAttribute("queue_oid"));
	  }
      catch(AmsException e){
            logError("While setting attributes for inputDoc", e);
            return AGENT_FAILURE;
      }

      ClientServerDataBridge csdb = new ClientServerDataBridge();
      csdb.setLocaleName(LOCALE_NAME);

      try{
          outputDoc = somePackagerMediator.performService(inputDoc, csdb);
          somePackagerMediator.remove();
          
      }
      catch (Exception e){
            //somePackagerMediator.remove(); //???
            logError("While calling performService on somePackagerMediator "+determinePackager(msg_type), e);
      }

      finally {
        try{
            int severityLevel   = 0;
            List<DocumentHandler>  errorList   = outputDoc.getFragmentsList("/Error/errorlist/error");
            for (DocumentHandler errorDoc : errorList){
                severityLevel = errorDoc.getAttributeInt("/severity");
                if (severityLevel >= ErrorManager.ERROR_SEVERITY){
                    transactionSuccess = false;
                }
            }
         }
         catch (AmsException e) {
            logError("Error occured while reading errors ", e);
         }
      }

 	  //MDB CR-609 11/30/10 Begin
      int processSuccess;
      if (outputDoc.getAttribute("/Out/Param/FVDAuthorization") == null)
      {
	      logInfo("transactionSuccess: " + transactionSuccess);

	      // Default the top level tag of the XML
	      String topLevelTag = "/Out/Proponix/";

	      // For the outgoing message to GT, change it
	      if (msg_type.equals(MessageType.GT))
	       {
	           topLevelTag = "/Out/GTProponix/";
	       }
	      /*
	      Extract everything under the top level tag   section in outputDoc for successfully       packaged message
	      Extract everything under "/Error"          section in outputDoc for unsuccessfully     packaged message
	      */
	      // W Zhu 12/29/08 5.0 dev trouble-shooting.  Check empty message.  Otherwise it could crash the whole agent.
	      String packagedMessage = "";
	      if (outputDoc.getFragment(topLevelTag)!= null) {
	          packagedMessage = outputDoc.getFragment(topLevelTag).toString();
	      }

	      String packagedError   = outputDoc.toString(); //return In, Out, and Error sections
	      
	      //RKAZI REL 9.1  T36000032474 10/31/2014 - Start
	      //For PAYBACK message
	      if (MessageType.PAYBACK.equals(msg_type)){
	    	  try {
				StringBuilder outputSB = AgentsUtility.addPaybackErrors(inputDoc.getComponent("/In"), outputDoc);
				if (outputSB.length() > 0){
					int subHeaderEndPosition = packagedMessage.indexOf("</SubHeader>");// need to add BeneficiaryErros after FileName tag
					String partPackagedMsg = packagedMessage.substring(0, subHeaderEndPosition );
					partPackagedMsg = partPackagedMsg.concat(outputSB.toString());
					partPackagedMsg = partPackagedMsg.concat(packagedMessage.substring(subHeaderEndPosition));
					packagedMessage = partPackagedMsg;
				}
			} catch (Exception e) {
				logError("Error ading Payback errors : ", e);
				e.printStackTrace();
			}
	      }
			//RKAZI REL 9.1  T36000032474 10/31/2014 - End

	      /*
	      DTD validation for OutboundAgent should take place here: after packager has returned outputDoc.
	      Initialize isXmlValidAgainstDtd=true, in the case when IS_DTD_VALIDATION is turned off
	      in AgentConfiguration.properties file.
	      */

	      ////////////////////////////////////////////////
	      //        BEGIN DTD VALIDATION                //
	      ////////////////////////////////////////////////
	      boolean isXmlValidAgainstDtd = true;
		  // Srinivasu_D CR-707 Rel 8.0 03/14/2012 Added isOutgoingMessage() condition
	      if (isOutgoingMessage(packagedMessage) && IS_DTD_VALIDATION_ON.equals("true")) {
	          // W Zhu 12/29/08 5.0 dev trouble-shooting.  Use packagedMessage to avoid NullPointerException and be more efficient.
	          String dtdString = determineDTDType(msg_type);
	          String dtdPlusXmlString = dtdString + packagedMessage;
	        //IValavala POUM080661913. add agent_id param
	        isXmlValidAgainstDtd = XMLUtility.validateXMLAgainstDTDFully(dtdPlusXmlString, AGENT_ID);
	        logInfo("isXmlValidAgainstDtd: "  + isXmlValidAgainstDtd);

	        /*
	        If message doesn't pass DTD validation, then errorFlag has to be set up, and errorText to be updated.
	        Care has to be taken not to overwrite errorText, but rather concatinate new errorText to original errorText.
	        IMPORTANT: do not use (') character in the body of errorText to avoid following Oracle error:
	        ORA-00933: SQL command not properly ended
	        */
	        if (isXmlValidAgainstDtd == false){
	            logError("Message did not pass DTD validation");
	            packagedError      = "Message did not pass DTD validation. See log for details " + packagedError;
	        }
	      }
	      ////////////////////////////////////////////////
	      //          END DTD VALIDATION                //
	      ////////////////////////////////////////////////

	      try{
	            /*
	            If transaction fails or DTD validation fails: set packaging_error_text to "Error/" Section plus
	            possible DTD validation error.
	            If transaction succeeds and no DTD validation failure: set msg_text to contents of /Out Section
	            */
	            if (transactionSuccess == false || isXmlValidAgainstDtd == false){
	                packaging_error_text = packagedError;
	            }
	            else{
	                packaging_error_text = ""; //make sure that value gets reset!!!
	            }
	            msg_text = packagedMessage;

	            outgoing_queue.setAttribute("msg_text", msg_text);
	            outgoing_queue.setAttribute("packaging_error_text", packaging_error_text);
	            outgoing_queue.setAttribute("status", STATUS_PACKAGED);
	            outgoing_queue.save();
	      }
	      catch(AmsException e){
	            logError("While setting attributes (packaging_error_text, msg_text, status) on outgoing_queue table", e);
	            return AGENT_FAILURE;
	      }

	      processSuccess = processAfterStatusPackaged();
	  }
  	  else
  	  	processSuccess = AGENT_SUCCESS;
 	  //MDB CR-609 11/30/10 End

      return processSuccess;

    } // end of processAfterStatusInitialized()




	protected int processAfterStatusPackaged() throws RemoteException {

      int mqSuccess = -1;
      String processParameters = null;
      String doNotSendMQInd = null;

      /*
      If packaging_error_text is empty, put message on Output Queue
      If packaging_error_text is not empty, put message on Error Queue
      */

      try
      {
        packaging_error_text = outgoing_queue.getAttribute("packaging_error_text");
        msg_text             = outgoing_queue.getAttribute("msg_text");
        processParameters =   outgoing_queue.getAttribute("process_parameters");//Added for Rel9.3.5 CR1028
      }
       catch (AmsException e){
	              logError("While obtaining msg_Text and/or packaging_error_Text and/or process_parameters", e);
	              return AGENT_FAILURE;
	        }
      
      
      doNotSendMQInd = AgentsUtility.getParm("DoNotSendMQ", processParameters);//Added for Rel9.3.5 CR1028
      
      //Add new condition for Rel9.3.5 CR1028 -- If doNotSendMQInd = 'Y', then do not put the message on either outgoing MQ queue or error queue
      if(!TradePortalConstants.INDICATOR_YES.equals(doNotSendMQInd)){
	      if (packaging_error_text == null || packaging_error_text.equals("")){
			  // Srinivasu_D CR-707 Rel 8.0 03/14/2012 Start
				if(isOutgoingMessage(msg_text))
					 mqSuccess = mqWrapper.attemptToPutMessageOnQueue(msg_text, mqWrapper.mqOutputQueue);
				else
					mqSuccess = 1;
				// Srinivasu_D CR-707 Rel 8.0 03/14/2012 End
	      }
	      else{
	            mqSuccess = mqWrapper.attemptToPutMessageOnQueue(packaging_error_text, mqWrapper.mqErrorQueue);
	      }
      }
      else{
    	  mqSuccess = 1;
      }
      if (mqSuccess != 1) { return AGENT_FAILURE; }


      try{
          // W Zhu CR-564 6.1 12/10/2010 pass false for override.  Do not use override datetime, which stays the same.
            date_sent = DateTimeUtility.getGMTDateTime(false, false);
      }
      catch (AmsException e){
            logError("While obtaining GMT date and time using DateTimeUtility", e);
            return AGENT_FAILURE;
      }

      try{
            /*
            There is no change in confirm_status for confimation messages. If it is regular
            message being packaged and there were no packaging errors, then update confirm_status
            to CONFIRM_STATUS_PENDING.
            */
            confirm_status = "";
            if (packaging_error_text == null || packaging_error_text.equals("")){
                status = STATUS_COMPLETED;
                //Add new condition for Rel9.3.5 CR-1028 - Update the confirm_status to 'C', so that it gets moved to history
                if (!isOutgoingMessage(msg_text) || TradePortalConstants.INDICATOR_YES.equals(doNotSendMQInd)) {
                    confirm_status = CONFIRM_STATUS_COMPLETED; // W Zhu 5/1/2013 CR-708B if there is no message going out, mark the confirm_status as C so that it will be archived.
                }
                else if (!(msg_type.equals(MessageType.CONFIRMATION) || msg_type.equals(MessageType.PAYBACK))) {    //CR-593 Rel. 7.0
                    confirm_status = CONFIRM_STATUS_PENDING;
                }
            }
            else{
                status = STATUS_ERROR;
            }

            outgoing_queue.setAttribute("date_sent",        date_sent);
            outgoing_queue.setAttribute("status",           status);
            outgoing_queue.setAttribute("confirm_status",   confirm_status);
            outgoing_queue.save();
            return AGENT_SUCCESS;
      }
      catch(AmsException e){
            logError("While setting attributes (date_sent, status, confirm_status) on outgoing_queue table", e);
            return AGENT_FAILURE;
      }

    }
    
   /**
    * Based upon the msg_type, determine the name of the packager to call.  This
    * is hard-coded so the addition of any new msg_type's will require additions
    * of if-statements below.  The name of the packager must match the name of
    * the packager's remote interface. If msg_type is unrecognized, then this
    * situation is considered security breach.
    *
    * Also, there is no PurgePackagerMediator for MSG_TYPE_PURGE or
    * HistoryPackagerMediator for MSG_TYPE_HISTORY.
    *

    * @param   String The type of the message to be packaged.
    * @return  String The name of the EJB packager to call
    */
  
   protected String determinePackager(String msg_type){

        if (msg_type.equals(MessageType.CONFIRMATION)) return "ConfirmationPackagerMediator";
        if (msg_type.equals(MessageType.TPL))          return "TPLPackagerMediator";
        if (msg_type.equals(MessageType.MAIL))         return "MailMessagePackagerMediator";
        if (msg_type.equals(MessageType.GT))           return "GlobalTradePackagerMediator";
        if (msg_type.equals(MessageType.PAYINVIN))		return "PayInvPackagerMediator";
        if (msg_type.equals(MessageType.RPMRULE))      return "RPMRULEPackagerMediator";
	    //rbhaduri - 24th Nov 08 - CR-434 - added INVSTI message type
        if (msg_type.equals(MessageType.INVSTI))      	return "INVSTIPackagerMediator";
		// ShilpaR Cr710
		if (msg_type.equals(MessageType.BULKINVUPL)) 	return "BULKINVUploadPackagerMediator";
		if (msg_type.equals(MessageType.INVUPL)) 		return "INVUploadPackagerMediator";
        if (msg_type.equals(MessageType.DIRECTDEBIT)) 	return "DIRDEBPackagerMediator";
        if (msg_type.equals(MessageType.PAYBACK)) 	 	return "PAYBACKPackagerMediator";  //CJR CR-593
        if (msg_type.equals(MessageType.PAYCOMP)) 		return "PAYCOMPPackagerMediator";  //CJR CR-593
		if (msg_type.equals(MessageType.TRIG)) 		return "TRIGPackagerMediator";  //NAR CR-710 rel 8.0
		if (msg_type.equals(MessageType.POM)) 			return "POMPackagerMediator";	   //Srinivasu_D CR707 Rel 8.0 03/14/2012
		if (msg_type.equals(MessageType.POR)) 		    return "PORPackagerMediator";  // ShilpaR CR-707 Rel 8.0
		if (msg_type.equals(MessageType.INM)) 			return "INMPackagerMediator";	//SHR CR-708 Rel 8.1.1
		if (msg_type.equals(MessageType.CUSREF)) 		return "CUSREFPackagerMediator"; //IValavala CR-741 Rel 8.2
        // If no packager exists for this msg_type, then return null
        return null;

   } 

   @Override
   protected String determineDTDType(String msg_type){

        String dtdString = null;

        if (msg_type.equals(MessageType.CONFIRMATION))  { dtdString  = CONFIRMATION_DTD;}
        if (msg_type.equals(MessageType.TPL))           { dtdString  = TPL_OUTBOUND_DTD;}
        if (msg_type.equals(MessageType.MAIL))          { dtdString  = MAIL_OUTBOUND_DTD;}
        if (msg_type.equals(MessageType.GT))            { dtdString = GLOBALTRADE_DTD; }
        if (msg_type.equals(MessageType.PAYINVIN))		 { dtdString  = PAYINV_DTD;}
        if (msg_type.equals(MessageType.RPMRULE)) 	     { dtdString = RPMRULE_DTD; }
	    //rbhaduri - 24th Nov 08 - CR-434 - added INVSTI message type
	    if (msg_type.equals(MessageType.INVSTI)) 	     { dtdString = INVSTI_DTD; }
		if (msg_type.equals(MessageType.INVUPL))        {	dtdString = UPLOAD_INV_DTD; }
	    if (msg_type.equals(MessageType.DIRECTDEBIT)) 	 { dtdString = DIRDEB_DTD; }
	    if (msg_type.equals(MessageType.PAYBACK)) 	     { dtdString = PAYBACK_DTD; }  //CJR CR-593
	    if (msg_type.equals(MessageType.PAYCOMP)) 	     { dtdString = PAYCOMP_DTD; }  //CJR CR-593
		if (msg_type.equals(MessageType.TRIG)) 	     { dtdString = INV_TRIGGER_DTD; }  //NAR CR-710 rel 8.0
		if (msg_type.equals(MessageType.POR)) 	         { dtdString = POR_DTD; }  // ShilpaR CR-707 Rel 8.0
		if (msg_type.equals(MessageType.CUSREF))        { dtdString = CUSREF_DTD; } //IValavala CR-741 Rel 8.2
        return dtdString;
    }

   // W Zhu 5/1/2013 CR-708B add parameter outDoc.  Allow packager to decide not to send out message.  This is useful for TRIGGER message from FVD authorized invoice, where the invoice may fail validation during packaging.
   protected boolean isOutgoingMessage(String outDoc){

   		boolean isPOMMsg =true;
   		//SHR CR708 Rel8.1.1 modified
   		if(msg_type.equalsIgnoreCase(MessageType.POM) || msg_type.equalsIgnoreCase(MessageType.BULKINVUPL) || msg_type.equalsIgnoreCase(MessageType.INM)
                        || "<Proponix>NoOutgoingMessage</Proponix>".equals(outDoc))
   				isPOMMsg =false;

   		return isPOMMsg;
   	}
   @Override
   protected  boolean connectToMQ() {
    return true;	
   }
   
  }