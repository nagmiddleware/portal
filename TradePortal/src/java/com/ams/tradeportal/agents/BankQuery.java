package com.ams.tradeportal.agents;
import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.busobj.ClientBank;
import com.ams.tradeportal.busobj.CorporateOrganization;
import com.ams.tradeportal.busobj.OperationalBankOrganization;
import com.ams.tradeportal.common.TPDateTimeUtility;
import com.ams.tradeportal.common.TradePortalConstants;
import com.ams.tradeportal.common.UniversalMessage;
import com.ams.tradeportal.common.XMLUtility;
import com.amsinc.ecsg.frame.AmsConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.frame.EJBObjectFactory;
import com.amsinc.ecsg.frame.ObjectIDCache;
import com.amsinc.ecsg.util.DateTimeUtility;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.GMTUtility;
import com.amsinc.ecsg.util.StringFunction;

/**
 * This class packages account balance query request and puts the message
 * in input queue. It also unpackages the response and updates the account
 * object with the response data.
 *
 */
public abstract class BankQuery {
private static final Logger LOG = LoggerFactory.getLogger(BankQuery.class);

    private static final String LEADING_ZERO ="0000000000";
	protected static final String SERVICE_RESPONSE_STATUS__COMPLETE = "C";
	protected static final String SERVICE_RESPONSE_STATUS__RECEIVED = "R";
	protected static final String SERVICE_RESPONSE_STATUS__ERROR    = "E";
	protected static final String SUCCESS_STATUS = "0";
	protected static final PropertyResourceBundle properties =
		(PropertyResourceBundle)ResourceBundle.getBundle("AgentConfiguration");
	protected static final String DTD_PATH              = properties.getString("dtdPath");
	protected static final String IS_DTD_VALIDATION_ON  = properties.getString("isDTDValidationOn");
	protected static final String CREATE_SQL = "insert into SERVICE_RESPONSE_HISTORY (service_response_hist_oid, time_created, message_id, a_user_oid, A_TRANSACTION_OID, msg_type)values(?,?,?,?,?,?)";
	protected static final String SENT_SQL = "update SERVICE_RESPONSE_HISTORY set correlation_id=?, time_sent=?, request_msg=? where service_response_hist_oid=?";
	protected static final String RECEIVED_SQL = "update SERVICE_RESPONSE_HISTORY set time_received=?, status = ?, response_msg=? where correlation_id=?";
	protected static final String UNPACK_SQL = "update SERVICE_RESPONSE_HISTORY set time_unpack_end=?, status = ? where correlation_id=?";
	
	protected static final boolean CORR_ID_WORKAROUND = "Y".equals(TradePortalConstants.getPropertyValue("TradePortal","CorrIDWorkAround","N"));  //SEUK083078753 - 09/01/10 
	protected static final String SERVERLOC =  TradePortalConstants.getPropertyValue("jPylon", "serverLocation", null);
	
	protected MQWrapper mqWrapper = null;
	private  String message_type = null;   // sub-class needs to define this
	private  String request_DTD   = null;  // sub-class needs to define this
	private  String response_DTD  = null;  // sub-class needs to define this
	//IValavala POUM080661913 add agentID;
	private  String agentID = "";
	
	
	/**
	 * Contructor
	 */

	protected BankQuery(String messageType, String agent, String requestDTD, String responseDTD) {
		mqWrapper = new MQWrapper();
		mqWrapper.setAgentID(agent);
		message_type = messageType;
		request_DTD = requestDTD;
		response_DTD = responseDTD;
		//IValavala POUM080661913
		this.agentID = agent;
	}

	protected static String getDTD(String dtdFile) {
		String dtd = null;
		if   ("true".equals(IS_DTD_VALIDATION_ON)) {
			dtd = XMLUtility.readFromFile(dtdFile);
		}
		return dtd;
	}
	
	/**
	 * This method querys account balance and populates the accounts with response data. It takes the
	 * user_oid, ClientBankWebBean, CorporateOrganization and list of AccountWebBeans as input.
	 * Those accounts which failed account balance query is returned in map with status.
	 * If the returned map is empty, that means all account query was successful.
	 * If the reutrned map is null, it means some error (dtd validation etc.) occurred.
	 *
	 * @param String user_oid
	 * @param ClientBank clientBank
	 * @param CorporateOrganization corpOrg
	 * @param List Accounts
	 * @return Map failed Accounts
	 * @throws AmsException 
	 * @throws RemoteException 
	 *
	 *
	 */
	protected Object query(String user_oid, String transactionOID, ClientBank clientBank, CorporateOrganization corpOrg, Object object) throws RemoteException, AmsException {
		Object returnObject = null;
		int rCode = mqWrapper.connectToMQ();
		try{
		if (rCode == 1) {
			byte[] correlationID = sendRequest(user_oid, transactionOID, clientBank,corpOrg,object);
			if (correlationID != null) {
				
				// SEUK083078753 - 09/01/10 -  Begin
				// workaround for ANZ CorrelationID mismatch 
				// subtract one from last byte as correlationID received in response from ANZ is
				// off by -1.
				if (CORR_ID_WORKAROUND) {
				   correlationID[23]-=1;
				}   
				// SEUK083078753 - 09/01/10 -  End
				returnObject= receiveReponse(correlationID,object);
				}
			} 
		}
		finally{
			mqWrapper.disconnectFromMQ();
		}
		return returnObject;
	}

	/**
	 * This method sends account balance query request. It returns CorrelationId. It takes the
	 * user_oid, ClientBank, CorporateOrganization and list of AccountWebBeans as input.
	 *
	 * @param String user_oid
	 * @param ClientBank clientBank
	 * @param CorporateOrganization corpOrg
	 * @param List Accounts
	 * @return byte[] CorrelationId
	 * @throws AmsException 
	 * @throws RemoteException 
	 *
	 */
	protected byte[] queryAsync(String user_oid, String transactionOID, ClientBank clientBank, CorporateOrganization corpOrg, Object object) throws RemoteException, AmsException {
		byte[] correlationID = null;
		int rCode = mqWrapper.connectToMQ();
		if (rCode == 1) {
			correlationID = sendRequest(user_oid, transactionOID, clientBank,corpOrg,object);
			mqWrapper.disconnectFromMQ();
		}
		return correlationID;
	}

	/**
	 * This method retrieve the account balance query response from MQ queue using given correlationID.
	 * It populates given account objects with response received. Those accounts which failed account
	 * balance query is returned in map with status. If the returned map is empty, that means all
	 * account query was successful.
	 *
	 * @param byte[] correlationID
	 * @return Map failed Accounts
	 * @throws AmsException 
	 * @throws RemoteException 
	 *
	 *
	 */
	protected Object retrieveReponse(byte[] correlationID, Object object) throws RemoteException, AmsException {
		Object returnObject = null;
		int rCode = mqWrapper.connectToMQ();
		if (rCode == 1) {
			returnObject= receiveReponse(correlationID,object);
			mqWrapper.disconnectFromMQ();
		}
		return returnObject;
	}

	/**
	 * This method sends account balance query request. It returns CorrelationId. It takes the
	 * user_oid, ClientBank, CorporateOrganization and list of AccountWebBeans as input.
	 * @throws AmsException 
	 * @throws RemoteException 
	 */
	protected byte[] sendRequest(String user_oid, String transactionOID, ClientBank clientBank, CorporateOrganization corpOrg, Object object) throws RemoteException, AmsException {

			String messageId = Long.toString(ObjectIDCache.getInstance(AmsConstants.BANKQUERY).generateObjectID(false));
			String serviceResponseHistoryOid = messageId;
			/* IValera - IR# JOUI111280267 - 02/18/2009 - Change Begin */
			/* To avoid duplication of message id across servers, appends last character of 'serverName' to new message id */
			PropertyResourceBundle portalProperties = (PropertyResourceBundle)PropertyResourceBundle.getBundle("AgentConfiguration");
			String serverName = portalProperties.getString("serverName");
			
			// NSX 04/20/2010 - RDUK041649583 Begin
			messageId += serverName.substring(serverName.length() - 2);
			// NSX 04/20/2010 - RDUK041649583 End
			
			/* IValera - IR# JOUI111280267 - 02/18/2009 - Change End */
			int len = messageId.length(); 
			if (len < 10) {
				messageId=LEADING_ZERO.substring(0,10-len) + messageId;
			}
			else if (len > 10) {
				messageId=messageId.substring(len-10,len);
			}
			Date gmtDate = GMTUtility.getGMTDateTime();

		try(Connection con = DatabaseQueryBean.connect(true);
			PreparedStatement pStmt =  con.prepareStatement(CREATE_SQL)) {
			
			pStmt.setString(1, serviceResponseHistoryOid);
			pStmt.setTimestamp(2, new java.sql.Timestamp(GMTUtility.getGMTDateTime(true,false).getTime())); //time created     // NSX  IR# NTUL121466003 Rel. 7.1.0  12/12/11
			pStmt.setString(3, messageId);
			pStmt.setString(4, user_oid);
			pStmt.setString(5, transactionOID);
			pStmt.setString(6, this.message_type);
			
			pStmt.executeUpdate();
			
		} catch (AmsException | SQLException e) {
			LOG.error("Error occured creating ServiceResponseHistory log", e);
		}

		DocumentHandler request = packageRequest(messageId,gmtDate,user_oid,clientBank,corpOrg,object);
		//VALIDATE request against DTD
		if (!isValidateXML(request_DTD, request)) {
			throw new AmsException(TradePortalConstants.MISSING_REQUIRED_FIELDS);
		}

		String requestStr = request.toString();
		LOG.debug("Sending Request XML==> {}", requestStr);
		byte[] correlationID = mqWrapper.sendMessageAsync(requestStr);
 
    	if (correlationID != null) {

    		try(Connection con = DatabaseQueryBean.connect(true);
    			PreparedStatement pStmt =  con.prepareStatement(SENT_SQL)) {
    			
    			pStmt.setBytes(1, correlationID);
    			pStmt.setTimestamp(2, new java.sql.Timestamp(GMTUtility.getGMTDateTime(true,false).getTime())); //time_sent       // NSX  IR# NTUL121466003 Rel. 7.1.0  12/12/11
    			pStmt.setString(3, requestStr);
    			pStmt.setString(4, serviceResponseHistoryOid);
    			pStmt.executeUpdate();
    			
			} catch (AmsException | SQLException e) {
				LOG.error("Error occured updating ServiceResponseHistory log", e);
			}
    	}

		return correlationID;
	}

	/**
	 * This method retrieves the account balance query response from MQ queue using given correlationID.
     * It populates given account objects with response received. Those accounts which failed account
	 * balance query is returned in map with status. If the returned map is empty, that means all
	 * account query was successful.
	 * @throws AmsException 
	 * @throws RemoteException 
	 *
	 */
	protected Object receiveReponse(byte[] correlationID, Object object) throws RemoteException, AmsException {
		Object returnObject = null;
		String responseStr = mqWrapper.attemptToGetMessageFromQueue(correlationID,false);
		LOG.debug("Received response XML==> {}" , responseStr);

		String status;
		if (StringFunction.isNotBlank(responseStr)) {
			status = SERVICE_RESPONSE_STATUS__RECEIVED;
		}
		else {
			status = SERVICE_RESPONSE_STATUS__ERROR;
		}

		try(Connection con = DatabaseQueryBean.connect(true);
			PreparedStatement pStmt =  con.prepareStatement(RECEIVED_SQL)) {
			pStmt.setTimestamp(1, new java.sql.Timestamp(GMTUtility.getGMTDateTime(true,false).getTime())); //received         // NSX  IR# NTUL121466003 Rel. 7.1.0  12/12/11
			pStmt.setString(2,status);
			pStmt.setString(3,responseStr);
			pStmt.setBytes(4,correlationID);
			pStmt.executeUpdate();
			
		} catch (AmsException | SQLException e) {
			LOG.error("Error occured updating ServiceResponseHistory log", e);
		}

		if (StringFunction.isNotBlank(responseStr)) {
			
			DocumentHandler response = new DocumentHandler(responseStr,true);
			response = response.getFragment("/Proponix");
			//VALIDATE response against DTD
			//NOTE: above response.getFragment returns null if not valid, 
			// include null condition as an error situation here
			if (response!= null && isValidateXML(response_DTD, response)) {
        		returnObject = unPackageResponse(response,object);
    			status = SERVICE_RESPONSE_STATUS__COMPLETE;
			} else { //error
				returnObject = null; //explicitly set to null
				status = SERVICE_RESPONSE_STATUS__ERROR;
			}
			
			try(Connection con = DatabaseQueryBean.connect(true);
				PreparedStatement pStmt =  con.prepareStatement(UNPACK_SQL)) {
				
				pStmt.setTimestamp(1, new java.sql.Timestamp(GMTUtility.getGMTDateTime(true,false).getTime())); //unpack end    // NSX  IR# NTUL121466003 Rel. 7.1.0  12/12/11
				pStmt.setString(2,status);
				pStmt.setBytes(3,correlationID);
				pStmt.executeUpdate();
				
			} catch (AmsException | SQLException e) {
				LOG.error("Error occured updating ServiceResponseHistory log", e);
			}
			
		}
		return returnObject;
	}

	/**
	 * Validates give DocumentHandler against the passed in dtd
	 *
	 * @param String dtd
	 * @param DocumentHandler doc
	 * @return boolean
	 *
	 */
	protected boolean isValidateXML(String dtd, DocumentHandler doc) {
		boolean isXmlValidAgainstDtd = true;
		if   ("true".equals(IS_DTD_VALIDATION_ON)) {
			String requestXml = doc.toString();
			String dtdPlusXmlString = dtd + requestXml;
			//IVAlavala POUM080661913/ add agentID
			isXmlValidAgainstDtd = XMLUtility.validateXMLAgainstDTDFully(dtdPlusXmlString, this.agentID);
			if (!isXmlValidAgainstDtd) {
				LOG.error("DTD validation failed ==> {}" , requestXml);
			}
			else {
				LOG.debug("Is XML valid: {}"  , isXmlValidAgainstDtd);
			}
		}
		else {
			LOG.info("isDTDValidationOn is set to {}, isValidateXML step skipped ...", IS_DTD_VALIDATION_ON);
		}
		return isXmlValidAgainstDtd;
	}

	/**
	 * Validates give DocumentHandler against the passed in dtd
	 *
	 * @param String dtd
	 * @param DocumentHandler doc
	 * @return boolean
	 * @throws RemoteException 
	 *
	 */
	protected String getCurrentDateTime(CorporateOrganization corpOrg) throws RemoteException {
		
		return getCurrentDateTime(corpOrg,"yyyy-MM-dd'T'HH:mm:ss.SSSZZZ" );
	}
	
	/**
	 * Validates give DocumentHandler against the passed in dtd
	 *
	 * @param String dtd
	 * @param DocumentHandler doc
	 * @return boolean
	 * @throws RemoteException 
	 *
	 */
	protected String getCurrentDateTime(CorporateOrganization corpOrg, String format) throws RemoteException {
		SimpleDateFormat formatter = new SimpleDateFormat(format);
		String timeStamp=null;
		try {
			Date aDate = TPDateTimeUtility.getLocalDateTime( DateTimeUtility.getGMTDateTime(), corpOrg.getAttribute("timezone_for_daily_limit"));
			timeStamp = formatter.format(aDate);
		} catch (AmsException e) {
			LOG.error("Error occured getting currentDateTime", e);
		}
		return timeStamp;
	}

	/**
	 * This method packages the account balance query request
	 * @throws AmsException 
	 * @throws RemoteException 
	 *
	 *
	 */
	protected DocumentHandler packageRequest(String messageId, Date sentDate, String user_oid, ClientBank clientBank, CorporateOrganization corpOrg, Object object) throws RemoteException, AmsException {
		DocumentHandler doc = new DocumentHandler();
		
		long opOrgOid = corpOrg.getAttributeLong("first_op_bank_org");
		OperationalBankOrganization opBankOrg = (OperationalBankOrganization) EJBObjectFactory.createClientEJB(SERVERLOC,"OperationalBankOrganization", opOrgOid);
		packageHeader(sentDate, messageId, clientBank, corpOrg, opBankOrg, doc, object);
		packageSubHeader(user_oid, corpOrg, doc, object);
		packageBody(messageId, corpOrg, opBankOrg, doc, object);
		return doc.getFragment("/Proponix");
	}


	/**
	 * This method packages the header section of request xml
	 * @throws AmsException 
	 * @throws RemoteException 
	 *
	 */
	protected void packageHeader(Date sentDate, String messageID, ClientBank clientBank, CorporateOrganization corpOrg, OperationalBankOrganization opBankOrg, DocumentHandler outputDoc, Object object) throws RemoteException, AmsException
	{
		String headerPath = UniversalMessage.headerPath;
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
		String sDate =  formatter.format(sentDate);
		formatter = new SimpleDateFormat("HHmm");
		String sTime = formatter.format(sentDate);
		
		setAttribute(outputDoc,headerPath+"/DestinationID",clientBank.getAttribute("OTL_id"));
		outputDoc.setAttribute(headerPath+"/SenderID",TradePortalConstants.SENDER_ID);     // NSX CR-542 03/04/10
		setAttribute(outputDoc,headerPath+"/OperationOrganizationID", opBankOrg.getAttribute("Proponix_id"));
		outputDoc.setAttribute(headerPath+"/MessageType",this.message_type);
		outputDoc.setAttribute(headerPath+"/DateSent", sDate);
		outputDoc.setAttribute(headerPath+"/TimeSent",sTime);
		outputDoc.setAttribute(headerPath+"/MessageID",messageID);

	}


	/**
	 * This method packages the subheader section of request xml
	 * @throws AmsException 
	 * @throws RemoteException 
	 *
	 */
	protected void packageSubHeader(String user_oid, CorporateOrganization corpOrg, DocumentHandler outputDoc, Object object) throws RemoteException, AmsException
	{
		outputDoc.setAttribute(UniversalMessage.subHeaderPath+"/CustomerID", corpOrg.getAttribute("name"));
		setAttribute(outputDoc,UniversalMessage.subHeaderPath+"/PortalUserID", user_oid); //***current user id
	}


	/**
	 * This method packages the body section of request xml
	 *
	 */
	abstract protected void packageBody(String messageID, CorporateOrganization corpOrg, OperationalBankOrganization opBankOrg, DocumentHandler outputDoc, Object object) throws RemoteException, AmsException;

	/**
	 * This method unpackages the Account balance query response and updates the corresponding accountWebBean.
	 * Those accounts that failed the query is returned in map.
	 *
	 */
	abstract protected Object unPackageResponse(DocumentHandler doc, Object object) throws RemoteException, AmsException;
	
	protected void setAttribute(DocumentHandler doc,String name, String value) {
		if (!StringFunction.isBlank(value)) {
			doc.setAttribute(name, value);
		}
	}
		
}
