package com.ams.tradeportal.agents;


import java.io.IOException;
import java.rmi.RemoteException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.MissingResourceException;

import javax.ejb.RemoveException;

import com.ams.tradeportal.common.PaymentFileProcessor;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AgentLockException;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.ClientServerDataBridge;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.frame.LockingManager;

/**
 * Here goes detailed description of PaymentUploadAgent functionality Copyright
 * � 2001 American Management Systems, Incorporated All rights reserved
 */
public class PaymentUploadAgent extends TradePortalAgent {
//private static final Logger LOG = LoggerFactory.getLogger(PaymentUploadAgent.class);

    private static final String AGENT_TYPE = "PAYUPLOAD";

	private boolean reservationAcquired = false;
    private boolean clearReservationAtStartup = true;
    private String IS_PAYUPLOAD_LOGGING_ON;
    private boolean logReservationInfo = false;
    private String validationStatusToProcess = TradePortalConstants.PYMT_UPLOAD_VALIDATION_PENDING;

    

    // W Zhu Rel 8.4 CR-599 use configurable PaymentValidationStatusToProcess                
    @Override
    public void setAgentId(String agentID) {
        super.setAgentId(agentID);
        try {
            validationStatusToProcess = properties.getString("PaymentValidationStatusToProcess").trim();
            logInfo("PaymentValidationStatusToProcess=" + validationStatusToProcess);
        } catch (java.util.MissingResourceException e) {

        }

    }

    /**
     * This method is an actual implementation if TradePortalAgent's
     * lookForWorkAndDoIt(). This method is called every 10 seconds or so.
     * Actual frequency is softcoded through agentTimerInterval parameter in
     * AgentConfiguration.properties file. It returns -1 if exception occured
     * during processing, or MQ Series became unaccessible, or some other
     * problem occured. It returns 1 if everything went successfully.
     *
     * @return int
     */
    @Override
    protected int lookForWorkAndDoIt() throws RemoteException, RemoveException {

        try {
            IS_PAYUPLOAD_LOGGING_ON = properties.getString("isPayUploadLoggingOn");
        } catch (MissingResourceException e) {

        }
        if (IS_PAYUPLOAD_LOGGING_ON != null && IS_PAYUPLOAD_LOGGING_ON.equals("true")) {
            logReservationInfo = true;
        }

        //IR DUM050356373 - IValavala - 08/10/2012.
        if (!reservationAcquired) {
            //First clear any reservations pending in case agent terminated previously
            if (clearReservationAtStartup) {
                try {
                    LockingManager.unlockAgent(AGENT_ID, AGENT_TYPE, false);
                    clearReservationAtStartup = false;
                } catch (AmsException | SQLException e) {
                    logError("While clearing agent reservation at startup", e);
                    return AGENT_FAILURE;
                }
            } // End of clearReservationAtStartup

        } // End of !reservationAcquired
        try {

						////////////////////////////////////////////////
            // BEGIN PROCESSING OF:    isOrphan == TRUE   //
            ////////////////////////////////////////////////
            if (isOrphan == true) {

                String selectOrphanSQL = "Select PAYMENT_FILE_UPLOAD_OID from (Select PAYMENT_FILE_UPLOAD_OID from PAYMENT_FILE_UPLOAD where agent_id=? and "
                        + "(validation_status=?) order by creation_timestamp) where rownum = 1";
                query.setSQL(selectOrphanSQL, new Object[]{AGENT_ID, TradePortalConstants.PYMT_UPLOAD_VALIDATION_IN_PROGRESS});
                query.getRecords();

							////////////////////////////////////////////////
                // BEGIN PROCESSING OF:    ORPHAN FOUND       //
                ////////////////////////////////////////////////
                if (query.getRecordCount() == 1) {
                    isOrphan = true;
                    String s_oid = query.getRecordValue("PAYMENT_FILE_UPLOAD_OID");
                    logInfo("Orphan found! Start processing orphan " + s_oid + "...");
                    ClientServerDataBridge csdb = new ClientServerDataBridge();
                    csdb.setLocaleName(LOCALE_NAME);
                    PaymentFileProcessor processor = new PaymentFileProcessor(AGENT_ID, Long.parseLong(s_oid), ejbServerLocation, csdb);
                    processor.processOrphan();
                } ////////////////////////////////////////////////
                // BEGIN PROCESSING OF:    ORPHAN NOT FOUND   //
                ////////////////////////////////////////////////
                else {
                    logInfo("No orphans found");
                    isOrphan = false;
                    return AGENT_SUCCESS;
                }
            }

        } catch (AmsException e) {
            logError("While processing orphan", e);
            return AGENT_FAILURE;
        }
				////////////////////////////////////////////////
        // END PROCESSING OF:   isOrphan == TRUE      //
        ////////////////////////////////////////////////

				////////////////////////////////////////////////
        // BEGIN PROCESSING OF: isOrphan == FALSE     //
        ////////////////////////////////////////////////
        try {


            ////IR DUM050356373 - IValavala - 08/10/2012 - Start. Add reservation to get work first
            try {
                reservationAcquired = LockingManager.lockAgent(AGENT_ID, AGENT_TYPE, false);

            } catch (AgentLockException ae) {
                if (logReservationInfo) {
                    logInfo("Unable to acquire reservation for getting work for Agent ID: " + AGENT_ID + " for type: " + AGENT_TYPE
                            + "  Reservation is currently held by Agent ID: " + ae.lockedbyAgentId() + " for type: " + ae.agentType());
                }
                reservationAcquired = false;

            } catch (SQLException | AmsException se) {
                logError("While acquiring reservation", se);
                return AGENT_FAILURE;
            }

            long oidLong = 0;
            if (reservationAcquired) {

                if (logReservationInfo) {
                    logInfo("Acquired reservation to look for work for Agent ID: " + AGENT_ID + " of type: " + AGENT_TYPE);
                }

                // CR-593 Rel. 7.0.0 Added manual upload payment priority over H2H sent payment file
                List<Object> selectSQLParams = new ArrayList<Object>();
                selectSQLParams.add(validationStatusToProcess);
                List<Object> updateSQLParams = new ArrayList<Object>();
                updateSQLParams.add(AGENT_ID);

                oidLong = DatabaseQueryBean.selectForUpdate("PAYMENT_FILE_UPLOAD",
                        "PAYMENT_FILE_UPLOAD_OID",
                        "PAYMENT_FILE_UPLOAD_OID = (select PAYMENT_FILE_UPLOAD_OID from (select PAYMENT_FILE_UPLOAD_OID, (case  when h2h_source is null then 0 else 1 end) as priority from PAYMENT_FILE_UPLOAD where agent_id is NULL and validation_status=? "
                        + " order by priority, creation_timestamp) where rownum = 1)",
                        "agent_id = ?",
                        false, true, selectSQLParams, updateSQLParams); //MDB Rel6.1 IR# PDUL012842185 1/28/11 - added NOWAIT to select for update statement so there will be no contention between agents

                //Clear reservation so other agents can get work. //IR DUM050356373 - IValavala - 08/10/2012
                try {
                    LockingManager.unlockAgent(AGENT_ID, AGENT_TYPE, false);
                } catch (AmsException | SQLException e) {
                    logError("While clearing agent reservation at startup", e);
                    return AGENT_FAILURE;
                }
                if (logReservationInfo) {
                    logInfo("Cleared reservation to look for work for Agent ID: " + AGENT_ID + " of type: " + AGENT_TYPE);
                }

            }

					////////////////////////////////////////////////
            // BEGIN PROCESSING OF: NEW MESSAGE FOUND     //
            ////////////////////////////////////////////////
            if (oidLong != 0) {
                logInfo("New message found. Start processing new message..." + oidLong);

                long startMessage = System.currentTimeMillis();

                ClientServerDataBridge csdb = new ClientServerDataBridge();
                csdb.setLocaleName(LOCALE_NAME);
                PaymentFileProcessor processor = new PaymentFileProcessor(AGENT_ID, oidLong, ejbServerLocation, csdb);

                processor.processPaymentFile();

                logInfo("Finished processing new message " + oidLong);
                logInfo("Complete time = " + (System.currentTimeMillis() - startMessage)
                        + "\tmilliseconds");
                return AGENT_SUCCESS;
            }
        } catch (AmsException e) {
            //MDB Rel6.1 IR# PDUL012842185 1/28/11 - Begin
            if (e.getMessage().equalsIgnoreCase(TradePortalConstants.ROW_LOCKED)) {
                logInfo("Contention issue between agents - next agent will try and process the next message");
                return AGENT_SUCCESS;
            }
            //MDB Rel6.1 IR# PDUL012842185 1/28/11 - End

            logError("While processing new message", e);
            return AGENT_FAILURE;
        } catch (NumberFormatException | IOException e) {
            logError("While processing new message", e);
            return AGENT_FAILURE;
        }
            ////////////////////////////////////////////////
        // END PROCESSING OF: NEW MESSAGE FOUND       //
        ////////////////////////////////////////////////
        ///////////////////////////////////////////////////
        // NEED Archive process similar to OutboundAgent//
        //////////////////////////////////////////////////

				////////////////////////////////////////////////
        // END PROCESSING OF: NEW MESSAGE FOUND       //
        ////////////////////////////////////////////////
				///////////////////////////////////////////////////
        // NEED Archive process similar to OutboundAgent//
        //////////////////////////////////////////////////
        return AGENT_NO_RECORDS_TO_PROCESS;

    }

    @Override
    protected  boolean connectToMQ() {
     return false;	
    }
}
