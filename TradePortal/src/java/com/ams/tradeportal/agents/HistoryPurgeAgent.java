package com.ams.tradeportal.agents;


import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.util.*;
import java.rmi.*;
import java.text.*;

/**  
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class HistoryPurgeAgent extends TradePortalAgent
{
//private static final Logger LOG = LoggerFactory.getLogger(HistoryPurgeAgent.class);
	 
	
    // Number of days that history records stay in the table
    // before being purged
    private long agingDays;
    
    // Overrides the timer interval to be one day
    // This agent will only run once a day
    public HistoryPurgeAgent()
    {
       TIMER_INTERVAL = 86400000;   

       // Get the number of days history records stay in the table
       // Default to 90 if no setting is present
       try
        {
          agingDays = Long.parseLong(properties.getString("maxHistoryAge"));       
        }
       catch(Exception e)
        {
          agingDays = 90;
        }
    }
    
   /*
    *  Executes two delete statements against the database to 
    *  remove any queue history records that are older than the
    *  aging period defined above.
    *
    *   @return int
    */
    @Override
    protected int lookForWorkAndDoIt() throws RemoteException
    {        
        try
        {           
            logInfo("Beginning to purge queue history records older than "+agingDays+" days");
            
            // Compute the date for which all earlier records must be deleted
            long latestHistoryDate = 
                   GMTUtility.getGMTDateTime(false).getTime() - (86400000 * agingDays);            
            SimpleDateFormat formatter = new SimpleDateFormat("MM-dd-yyyy");
            String lastestHistoryDateString = 
                        formatter.format(new Date(latestHistoryDate));

            // Build and execute delete for in the incoming queue history table
            String deleteStatement = "delete from incoming_q_history" +
                                     " where date_received < to_date(?, 'MM-DD-YYYY')";
           
            DatabaseQueryBean.executeUpdate(deleteStatement, false, new Object[]{lastestHistoryDateString});

            // Build and execute delete for in the outgoing queue history table            
            deleteStatement = "delete from outgoing_q_history" +
                              " where date_sent < to_date(?, 'MM-DD-YYYY')";
                     
            DatabaseQueryBean.executeUpdate(deleteStatement, false, new Object[]{lastestHistoryDateString});  

            // Nar CR-914A Rel 9.2.0.0 02/11/2015 delete applied invoice detail entry 
            deleteStatement = "DELETE FROM cred_applied_inv_detail" +
                    " WHERE date_sent is not null AND date_sent < to_date(?, 'MM-DD-YYYY')";
           
            DatabaseQueryBean.executeUpdate( deleteStatement, false, new Object[] { lastestHistoryDateString } ); 
            
            logInfo("Finished purging queue history records older than "+agingDays+" days");            
        }
        catch(Exception e) 
        { 
           System.out.println("Error occurred while purging instrument history records...");
           e.printStackTrace();
           return AGENT_FAILURE;
        }
            
        // CR-451 TRudden 01/28/2009 Begin -- Changes to skip sleep period when more work exists
        /** Since this agent does bulk processing, always return
         * no more records to process, which ensures the thread will sleep before the next call 
         * */   
          return AGENT_NO_RECORDS_TO_PROCESS;
       // CR-451 TRudden 01/28/2009 End  
   }
    
    @Override
    protected  boolean connectToMQ() {
     return false;	
    }
}
