package com.ams.tradeportal.agents;


import java.util.*;
import java.rmi.*;
import javax.ejb.*;
import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import com.ams.tradeportal.busobj.*;
import com.ams.tradeportal.mediator.*;
import com.ams.tradeportal.common.*;


/**
 *   Here goes detailed description of OutboundAgent functionality
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class AutoLCAgent extends TradePortalAgent
{
//private static final Logger LOG = LoggerFactory.getLogger(AutoLCAgent.class);

	
    private String poDataFile   = null;
    private String xml          = null;
    private String poOid        = null;


    /**
     *
     *
     *
     */
    @Override
    protected int lookForWorkAndDoIt() throws RemoteException, RemoveException
    {
        

        try {
          


            ////////////////////////////////////////////////
            // BEGIN PROCESSING OF:    isOrphan == TRUE   //
            ////////////////////////////////////////////////

            if (isOrphan == true) {
                /*
                Since having AGENT_ID as a column on active_po_upload table prevents other agents from
                accessing same row, and there are no threads in current implementation of TradePortalAgent:
                1.  no need to selecting orphans "for update"
                2.  no need to setAutoCommit(false)
                */
                String selectOrphanSQL =    "Select ACTIVE_PO_UPLOAD_OID, PO_UPLOAD_PARAMETERS, PO_DATA_FILE, A_CORP_ORG_OID from active_po_upload where agent_id=?" +
                                            " and status=? and rownum=1";
                Debug.debug("selectOrphanSQL -> " + selectOrphanSQL);
                query.setSQL(selectOrphanSQL, new Object[]{AGENT_ID,STATUS_INITIALIZED});
                query.getRecords();

                ////////////////////////////////////////////////
                // BEGIN PROCESSING OF:    ORPHAN FOUND       //
                ////////////////////////////////////////////////
                if (query.getRecordCount() == 1){
                    logInfo("Orphan found! Start processing orphan...");
                    isOrphan = true;

                    //Create the document to send to the mediator
                    xml = query.getRecordValue("PO_UPLOAD_PARAMETERS");
                    Debug.debug("xml -> " + xml);
                    DocumentHandler xmlDoc = new DocumentHandler(xml, false);
                    Debug.debug("xmlDoc -> " + xmlDoc.toString());

                    // If a PO Data File was uploaded, add it to the Auto LC Create parameters
                    // xml doc. Also, add the corporate org oid since it's not already in the file.
                    poDataFile = query.getRecordValue("PO_DATA_FILE");
                    if (poDataFile != null)
                    {
                       xmlDoc.setAttribute("/In/AutoLCCreateInfo/poDataFileContents", poDataFile);
                       xmlDoc.setAttribute("../corporateOrgOid", query.getRecordValue("A_CORP_ORG_OID"));
                    }

                    // Create a new instance of the Auto LC Mediator and call performService()
                    Debug.debug("Creating instance of AutoLCCreateMediator");
                    AutoLCCreateMediator mediator = (AutoLCCreateMediator)EJBObjectFactory.createClientEJB(ejbServerLocation,
                                                                                                           "AutoLCCreateMediator");
                    Debug.debug("Calling AutoLCCreateMediator.performService");
                    ClientServerDataBridge csdb = new ClientServerDataBridge();
                    String locale = xmlDoc.getAttribute("/In/User/locale");
                    Debug.debug ("Setting locale to -> " + locale);
                    csdb.setLocaleName(locale);
                    DocumentHandler result = mediator.performService(xmlDoc, csdb);
                    mediator.remove();

                    Debug.debug("XML RETURNED:\n" + result.toString());

                    //Now we need to delete that row
                    poOid = query.getRecordValue("ACTIVE_PO_UPLOAD_OID");
                    Debug.debug("Delete DB row from active_po_upload with oid -> " + poOid);
                    ActivePOUpload deleteMe = (ActivePOUpload) EJBObjectFactory.createClientEJB(ejbServerLocation,
                                                                                                  "ActivePOUpload");
                    deleteMe.getData(Long.parseLong(poOid));
                    deleteMe.delete();
                    deleteMe.remove();

                    
                    logInfo("Orphan found! End processing orphan...");
                    

                    return AGENT_SUCCESS;
            }
            ////////////////////////////////////////////////
            // END PROCESSING OF:    ORPHAN FOUND         //
            ////////////////////////////////////////////////


            ////////////////////////////////////////////////
            // BEGIN PROCESSING OF:    ORPHAN NOT FOUND   //
            ////////////////////////////////////////////////
            else{
                logInfo("No orphans found");
                isOrphan = false;
                // CR-451 TRudden 01/28/2009 Begin -- Changes to skip sleep period when more work exists
                return AGENT_NO_RECORDS_TO_PROCESS;
     	       // CR-451 TRudden 01/28/2009 End 
            }
            ////////////////////////////////////////////////
            // END PROCESSING OF:    ORPHAN NOT FOUND     //
            ////////////////////////////////////////////////
            }

        }
        catch(AmsException e){
            e.printStackTrace();
            logError("While processing orphan", e);
            return AGENT_FAILURE;
        }
        ////////////////////////////////////////////////
        // END PROCESSING OF:   isOrphan == TRUE      //
        ////////////////////////////////////////////////



        ////////////////////////////////////////////////
        // BEGIN PROCESSING OF: isOrphan == FALSE     //
        ////////////////////////////////////////////////
        /*
        Even though having agent_id as a column on active_po_upload table prevents other agents from
        accessing same row, and there are no threads in current implementation of TradePortalAgent,
        multiple agents can run on the same JVM.
        Use following settings till status is updated to STATUS_INITIALIZED, and agent_id is
        updated to AGENT_ID.
        1. need to selecting new message "for update"
        2. need to setAutoCommit(false)
        */
        try {
        	List<Object> selectSQLParams = new ArrayList<Object>();
        	selectSQLParams.add(STATUS_START);
        	List<Object> updateSQLParams = new ArrayList<Object>();
        	updateSQLParams.add(AGENT_ID);
        	updateSQLParams.add(STATUS_INITIALIZED);

            long poOidLong = DatabaseQueryBean.selectForUpdate( "active_po_upload",
                                                                    "active_po_upload_oid",
                                                                    "agent_id is NULL and status=? and rownum=1",
                                                                    "agent_id = ?, status= ?",
                                                                    false, false, selectSQLParams, updateSQLParams);
            //Debug.debug("Checking for active_po_uploads.  Oid follows (0 means none found) -> " + poOidLong);
            ////////////////////////////////////////////////
            // BEGIN PROCESSING OF: NEW ACTIVE PO UPLOAD  //
            ////////////////////////////////////////////////
            if(poOidLong != 0){
                logInfo("New active_po_upload found. Start processing...");

                //Get a handle to the active_po_upload date via an EJB
                ActivePOUpload activePO = (ActivePOUpload) EJBObjectFactory.createClientEJB(ejbServerLocation,
                                                                                            "ActivePOUpload");
                activePO.getData(poOidLong);

                //Create the document to send to the mediator
                xml = activePO.getAttribute("po_upload_parameters");
                
                DocumentHandler xmlDoc = new DocumentHandler(xml, false);
                

                // If a PO Data File was uploaded, add it to the Auto LC Create parameters
                // xml doc. Also, add the corporate org oid since it's not already in the file.
                poDataFile = activePO.getAttribute("po_data_file");
                if (poDataFile != null)
                {
                   xmlDoc.setAttribute("/In/AutoLCCreateInfo/poDataFileContents", poDataFile);
                   xmlDoc.setAttribute("../corporateOrgOid", activePO.getAttribute("corp_org_oid"));
                }

                // Create a new instance of the Auto LC Mediator and call performService()
                Debug.debug("Creating instance of AutoLCCreateMediator");
                AutoLCCreateMediator mediator = (AutoLCCreateMediator)EJBObjectFactory.createClientEJB(ejbServerLocation,
                                                                                                       "AutoLCCreateMediator");
                Debug.debug("Calling AutoLCCreateMediator.performService");
                ClientServerDataBridge csdb = new ClientServerDataBridge();
                String locale = xmlDoc.getAttribute("/In/User/locale");
             
                csdb.setLocaleName(locale);
                DocumentHandler result = mediator.performService(xmlDoc, csdb);
                mediator.remove();
                Debug.debug("XML RETURNED:\n" + result.toString());

                //Now we need to delete that row
                activePO.delete();
                activePO.remove();

 
                logInfo("New active_po_upload end processing...");
                

                return AGENT_SUCCESS;
            }
            ////////////////////////////////////////////////
            // END PROCESSING OF: NEW MESSAGE FOUND       //
            ////////////////////////////////////////////////

        }
        catch(Exception e){
            e.printStackTrace();
            logError("While processing new message", e);
            return AGENT_FAILURE;
        }
        ////////////////////////////////////////////////
        // END PROCESSING OF: isOrphan == FALSE       //
        ////////////////////////////////////////////////

        //you really won't ever get to this return statement,
        //but having it here gives me a warm fuzzy feeling
        // CR-451 TRudden 01/28/2009 Begin -- Changes to skip sleep period when more work exists
        return AGENT_NO_RECORDS_TO_PROCESS;
	     // CR-451 TRudden 01/28/2009 End 
    }// end of lookForWorkAndDoIt()

    @Override
    protected  boolean connectToMQ() {
     return false;	
    }
    
}// end of OutboundAgent class