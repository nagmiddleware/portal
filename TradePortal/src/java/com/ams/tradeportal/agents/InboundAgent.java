
package com.ams.tradeportal.agents;


import java.rmi.RemoteException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.PropertyResourceBundle;
import java.util.TimeZone;

import javax.ejb.RemoveException;

import com.ams.tradeportal.busobj.IncomingInterfaceQueue;
import com.ams.tradeportal.busobj.OutgoingInterfaceQueue;
import com.ams.tradeportal.common.InstrumentType;
import com.ams.tradeportal.common.MessageType;
import com.ams.tradeportal.common.TradePortalConstants;
import com.ams.tradeportal.common.TransactionType;
import com.ams.tradeportal.common.UniversalMessage;
import com.ams.tradeportal.common.XMLUtility;
import com.amsinc.ecsg.frame.AmsConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.ClientServerDataBridge;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.frame.EJBObjectFactory;
import com.amsinc.ecsg.frame.ErrorManager;
import com.amsinc.ecsg.frame.Mediator;
import com.amsinc.ecsg.frame.ObjectIDCache;
import com.amsinc.ecsg.util.DateTimeUtility;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.StringFunction;

/**  InboundAgent looks up MQ Incoming Queue for new messages.
 *   Incoming Queue is not shared, so every messages must be read.
 *   InboundAgent looks up for new messages with the frequency specified
 *   by timeInterval parameter in AgentConfiguration.properties file.
 *   If no message is found, InboundAgent does nothing. If message is found,
 *   InboundAgent does the following:
 *   ...
 *   ...
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */

public class InboundAgent extends TradePortalAgent
{
//private static final Logger LOG = LoggerFactory.getLogger(InboundAgent.class);
   protected  String  queue_oid="";                  // this field is set by MQAgent only
   protected  String  date_received="";              // this field is set by MQAgent only
   protected  String  date_sent="";                  // this field is set by InboundAgent (date-time when OTL sent message)
   protected  String  msg_text="";                   // this field is set by MQAgent only
   protected  String  msg_type="";
   protected  String  status="";                     // this field is set by MQAgent and InboundAgent
   protected  String  message_id="";
   protected  String  reply_to_message_id="";
   protected  String  agent_id="";
   protected  String  confirmation_error_text="";
   protected  String  unpackaging_error_text="";
   protected  String  completeInstrumentID = "";
   protected  String  transactionTypeCode  = "";
   protected  String  clientBank           = "";
   protected  String  senderID = "";
   protected  String  messageTypesToProcess = null;
   protected  String  incomingQueueStatusToProcess = STATUS_RECEIVED;



   // W Zhu 2/20/08 WEUH121436595 BEGIN
   /**
    * Retrieve messageTypesToProcess when agent ID is set.  messageTypesToProcess is an optional
    * configuration parameter.
    */
   @Override
   public void setAgentId(String agentID)
   {
	   super.setAgentId(agentID);
		try {
			messageTypesToProcess = properties.getString("messageTypesToProcess."+agentID).trim();
		}
		catch (java.util.MissingResourceException e){
			try {
				messageTypesToProcess = properties.getString("messageTypesToProcess").trim();
			}
			catch (java.util.MissingResourceException e2) {
			}
		}
                // W Zhu Rel 8.2 T3600015221 use configurable IncomingQueueStatusToProcess
                try {
                    incomingQueueStatusToProcess = properties.getString("IncomingQueueStatusToProcess").trim();
                    System.out.println("IncomingQueueStatusToProcess="+incomingQueueStatusToProcess);
                }
                catch (java.util.MissingResourceException e) {

                }

		//PPX-208 - Ravindra B - 03/18/2011 - Start
        removeInstrumentLock(agentID);
        //PPX-208 - Ravindra B - 03/18/2011 - End
   }
   // W Zhu 2/20/08 WEUH121436595 END


   /**   This method is an actual implementation if TradePortalAgent's lookForWorkAndDoIt().
    *    This method is called every 10 seconds or so. Actual frequency is softcoded through
    *    agentTimerInterval parameter in AgentConfiguration.properties file.
    *    It returns -1 if exception occured during processing, or MQ Series became unaccessible,
    *    or some other problem occured. It returns 1 if everything went successfully.
    *
    *   @return int
    */
   @Override
    protected int lookForWorkAndDoIt() throws RemoteException, RemoveException
    {
        try{

            ////////////////////////////////////////////////
            // BEGIN PROCESSING OF:    isOrphan == TRUE   //
            ////////////////////////////////////////////////
            if (isOrphan == true) {
            /*
            Since having AGENT_ID as a column on incoming_queue table prevents other agents from
            accessing same row, and there are no threads in current implementation of TradePortalAgent:
            1.  no need to selecting orphans "for update"
            2.  no need to setAutoCommit(false)
            */

             /*Modified the sql clause so that the oldest row is picked up.
	           using just rownum = 1 doesn't always return the oldest row. Hence
	           sorting it by date_received and then using rownum = 1 to make sure
	           only one row is returned
             */
            String selectOrphanSQL = "Select queue_oid from (select queue_oid from incoming_queue where agent_id=?" +
                                     " and (status=? or status=?) order by date_received)" +
                                     " where rownum = 1";
            logInfo(selectOrphanSQL);
            query.setSQL(selectOrphanSQL, new Object[]{AGENT_ID, STATUS_INITIALIZED,STATUS_UNPACKAGED });
            query.getRecords();

            ////////////////////////////////////////////////
            // BEGIN PROCESSING OF:    ORPHAN FOUND       //
            ////////////////////////////////////////////////
            if (query.getRecordCount() == 1){
                logInfo("Orphan found! Start processing orphan...");
                isOrphan = true;
                queue_oid = query.getRecordValue("queue_oid");
                //logInfo("Processing Orphan with queue_oid = " + queue_oid);
                incoming_queue = (IncomingInterfaceQueue) EJBObjectFactory.createClientEJB(ejbServerLocation, "IncomingInterfaceQueue", Long.parseLong(queue_oid));
                msg_text    = incoming_queue.getAttribute("msg_text");
                status      = incoming_queue.getAttribute("status");


                ////////// START PROCESS: STATUS_INITIALIZED /////////
                if (status.equals(STATUS_INITIALIZED)){
                    int processSuccess = processAfterStatusInitialized();
                    logInfo("Finished processing 'Initialized' Orphan");
                    return processSuccess;
                }
                ////////// END PROCESS: STATUS_INITIALIZED  //////////


                ////////// START PROCESS: STATUS_UNPACKAGED ////////////
                if (status.equals(STATUS_UNPACKAGED)){
                    int processSuccess = processAfterStatusUnpackaged();
                    logInfo("Finished processing 'Unpackaged' Orphan");
                    return processSuccess;
                }
                ////////// END PROCESS: STATUS_UNPACKAGED //////////////

            }
            ////////////////////////////////////////////////
            // END PROCESSING OF:    ORPHAN FOUND         //
            ////////////////////////////////////////////////


            ////////////////////////////////////////////////
            // BEGIN PROCESSING OF:    ORPHAN NOT FOUND   //
            ////////////////////////////////////////////////
            else{
                logInfo("No orphans found");
                isOrphan = false;
             // CR-451 TRudden 01/28/2009 Begin -- Changes to skip sleep period when more work exists
                //return success here, so Agent immediately tries to process records with STATUS_RECEIVED
                return AGENT_SUCCESS;
             // CR-451 TRudden 01/28/2009 End
            }
            ////////////////////////////////////////////////
            // END PROCESSING OF:    ORPHAN NOT FOUND     //
            ////////////////////////////////////////////////
            }
        }
        catch(AmsException e){
            logError("While processing orphan", e);
            return AGENT_FAILURE;
        }
        ////////////////////////////////////////////////
        // END PROCESSING OF:   isOrphan == TRUE      //
        ////////////////////////////////////////////////



        ////////////////////////////////////////////////
        // BEGIN PROCESSING OF: isOrphan == FALSE     //
        ////////////////////////////////////////////////
        /*
        Even though having AGENT_ID as a column on incoming_queue table prevents other agents from
        accessing same row, and there are no threads in current implementation of TradePortalAgent,
        multiple agents can run on the same JVM.
        Use following settings till status is updated to STATUS_INITIALIZED, and agent_id is
        updated to AGENT_ID.
        1. need to selecting new message "for update"
        2. need to setAutoCommit(false)
        */
        try{

         /*Modified the where clause so that the oldest row is picked up.
           using just rownum = 1 doesn't always return the oldest row. Hence
           sorting it by date_received and then using rownum = 1 to make sure
           only one row is returned
           */
        	String messageTypesClause = "";
        	if (messageTypesToProcess != null && !messageTypesToProcess.equals("")) {
        		messageTypesClause = "and msg_type in ("+messageTypesToProcess+")";
        	}
        	List<Object> whereSqlParams = new ArrayList<Object>();
        	whereSqlParams.add(incomingQueueStatusToProcess);
        	whereSqlParams.add(STATUS_RESUBMIT);
        	whereSqlParams.add(AGENT_ID);

			List<Object> updateSqlParams = new ArrayList<Object>();
			updateSqlParams.add(AGENT_ID);
			updateSqlParams.add(STATUS_INITIALIZED);

                // W Zhu T3600015221 use configurable incomingQueueStatusToProcess
            long queue_oidLong = DatabaseQueryBean.selectForUpdate( "incoming_queue",
                                                                    "queue_oid",
                                                                    "queue_oid = (select queue_oid from  (select queue_oid from incoming_queue where (( agent_id is NULL and "
                                                                    + " status=?) or ( status=? and agent_id = ?)) "
                                                                    + messageTypesClause + " and msg_text is NOT NULL" + " order by date_received) where rownum =1)",
                                                                    "agent_id = ?, status= ?",
                                                                    false, true, whereSqlParams, updateSqlParams);  // Rel7.0 IR# LMUL041251241 04/29/11  

            ////////////////////////////////////////////////
            // BEGIN PROCESSING OF: NEW MESSAGE FOUND     //
            ////////////////////////////////////////////////
            if(queue_oidLong != 0){
                logInfo("New message found. Start processing new message " + queue_oidLong + "...");
                incoming_queue = (IncomingInterfaceQueue) EJBObjectFactory.createClientEJB(ejbServerLocation, "IncomingInterfaceQueue", queue_oidLong);
                msg_text = incoming_queue.getAttribute("msg_text");

                int processSuccess = processAfterStatusInitialized();
                logInfo("Finished processing new message " + queue_oidLong);
                return processSuccess;
            }
        }
        catch(AmsException e){
        	// Rel7.0 IR# LMUL041251241 04/29/11  - Begin
			if (e.getMessage().equalsIgnoreCase(TradePortalConstants.ROW_LOCKED))
			{
				logInfo("Contention issue between agents - next agent will try and process the next message");
				return AGENT_SUCCESS;
			}
			//Rel7.0 IR# LMUL041251241 04/29/11   - End

            logError("While processing new message", e);
            return AGENT_FAILURE;
        }
            ////////////////////////////////////////////////
            // END PROCESSING OF: NEW MESSAGE FOUND       //
            ////////////////////////////////////////////////



            ////////////////////////////////////////////////
            // BEGIN PROCESSING OF: NEW MESSAGE NOT FOUND //
            ////////////////////////////////////////////////
        try{
            String historySQL = "Select queue_oid from incoming_queue where agent_id=?" +
                                " and (status=? or status=? or status=?)" +   // NSX - CR-542 03/16/10
                                " and rownum=1" ;
            query.setSQL(historySQL, new Object[]{AGENT_ID,STATUS_COMPLETED, STATUS_ERROR, STATUS_MESSAGE_ROUTED });
            query.getRecords();

            ////// BEGIN PROCESSING: NO MESSAGE TO ARCHIVE /////////
            if (query.getRecordCount() != 1){
         // CR-451 TRudden 01/28/2009 Begin -- Changes to skip sleep period when more work exists
                return AGENT_NO_RECORDS_TO_PROCESS;
         // CR-451 TRudden 01/28/2009 End
            }
            ////// END PROCESSING: NO MESSAGE TO ARCHIVE ///////////


            ////// BEGIN PROCESSING: MESSAGE TO ARCHIVE ////////////
            queue_oid = query.getRecordValue("queue_oid");
            logInfo("Processed message found. Start archiving message " + queue_oid + "... ");
            incoming_queue = (IncomingInterfaceQueue) EJBObjectFactory.createClientEJB(ejbServerLocation, "IncomingInterfaceQueue", Long.parseLong(queue_oid));
            incoming_queue.moveToHistory();
            logInfo("Finished archiving processed message " + queue_oid);
            return AGENT_SUCCESS;
            ////// BEGIN PROCESSING: MESSAGE TO ARCHIVE ////////////

            ////////////////////////////////////////////////
            // END PROCESSING OF: NEW MESSAGE NOT FOUND   //
            ////////////////////////////////////////////////
        }
        catch(AmsException e){
            logError("While archiving processed message", e);
            return AGENT_FAILURE;
        }
        ////////////////////////////////////////////////
        // END PROCESSING OF: isOrphan == FALSE       //
        ////////////////////////////////////////////////

   }// end of lookForWorkAndDoIt()


   /**
    * Based upon the msg_type, determine the name of the unpackager to call.  This
    * is hard-coded so the addition of any new msg_type's will require additions
    * of if-statements below.  The name of the unpackager must match the name of
    * the unpackager's remote interface. If msg_type is unrecognized, then this
    * situation is considered security breach.
    *

    * @param   String The type of the message to be unpackaged.
    * @return  String The name of the EJB unpackager to call
    */
   protected String determineUnPackager(String msg_type){

        if (msg_type.equals(MessageType.CONFIRMATION)) return "ConfirmationUnPackagerMediator";
        if (msg_type.equals(MessageType.TPL))          return "TPLUnPackagerMediator";
        if (msg_type.equals(MessageType.TPLH))          return "TPLUnPackagerMediator";//Leelavathi PPX-208 29/4/2011 added
        if (msg_type.equals(MessageType.PURGE))        return "PurgeUnPackagerMediator";
        if (msg_type.equals(MessageType.HISTORY))      return "TPLUnPackagerMediator"; // this is important
        if (msg_type.equals(MessageType.MAIL))         return "MailMessageUnPackagerMediator";
        if (msg_type.equals(MessageType.INSSTAT))      return "INSSTATUnPackagerMediator";
        if (msg_type.equals(MessageType.INSSTATH))      return "INSSTATUnPackagerMediator";//Leelavathi PPX-208 29/4/2011 added
        if (msg_type.equals(MessageType.INSSTATR))      return "INSSTATUnPackagerMediator";//Leelavathi PPX-208 29/4/2011 added
		if (msg_type.equals(MessageType.WORKSTAT))     return "WorkItemUnPackagerMediator";
		if (msg_type.equals(MessageType.REFDATA))      return "REFDATAUnPackagerMediator";
    	if (msg_type.equals(MessageType.PAYINVOT))     return "PAYINVOTUnPackagerMediator";
    	if (msg_type.equals(MessageType.EODTRNIN))     return "EODTransUnPackagerMediator";	// peter ng - 3rd Dec 08 - CR-451
	    if (msg_type.equals(MessageType.ARINVOICE))    return "RMInvUnPackagerMediator";
	    if (msg_type.equals(MessageType.INVSTO))       return "INVSTOUnPackagerMediator"; //Vshah 12/11/2008 INVSTO UnPackager
	    if (msg_type.equals(MessageType.TPLFXRT))      return "FXRateUnPackagerMediator"; //  CR-469 NShrestha 05/18/2009
	    if (msg_type.equals(MessageType.PAYSTAT))      return "PayStatUnPackagerMediator"; //  CR-596 Narayan 11/22/2010
	    if (msg_type.equals(MessageType.PAYFILE))      return "PAYFILEUnPackagerMediator";  // CR-593 NSX 04/04/11  Rel. 7.0.0

	    if (msg_type.equals(MessageType.DATAIN))      return "GenericIncomingDataUnPackagerMediator";  // CR-587 MJ 17/04/11  Rel. 7.1
	    if (msg_type.equals(MessageType.INTEREST_RATE))      return "InterestDiscountRateUnPackagerMediator";  // Srini_D CR-713 Rel 8.0 10/18/2011
	    if (msg_type.equals(MessageType.PINCNAPP))           return "PINCNAPPUnPackagerMediator";//CR1006
	    // If no unpackager exists for this msg_type, then return null
        return null;

   } // end of determineUnPackager()



    protected int processAfterStatusInitialized()  throws RemoteException, AmsException {

        Mediator someUnPackagerMediator;
        DocumentHandler     messageDoc;

        DocumentHandler     inputDoc    = new DocumentHandler();
        DocumentHandler     outputDoc   = new DocumentHandler();

        /*
        Initialize transactionSuccess to true so that if message does not pass DTD validation,
        then no unpackaged is called. In this case following if-statement: if(transactionSuccess == false || ...)
        does not fail automatically.
        */
        boolean transactionSuccess              = true;
        boolean transactionResubmit				= false;
        boolean hasDuplicateMessageIdExisted    = false;
        boolean messageRouted                   = false;       // NSX - CR-542 03/16/10

        String  packagedError                   = "";

        date_sent               = "";
        msg_type                = "";
        message_id              = "";

		reply_to_message_id     = "";
        confirmation_error_text = "";

        String msgBrokerDateTime = ""; //initialize

        //capture the unpack end date/time
        String datetime_unpack_start = "";
        try{
            // W Zhu CR-564 6.1 12/10/2010 pass false for override.  Do not use override datetime, which stays the same and makes pack_seconds = 0.
            datetime_unpack_start = DateTimeUtility.getGMTDateTime(false, false);
        }
        catch (AmsException e){
            //log and ignore - non-critical processing
            logError("Problem obtaining unpack start date time. Ignoring.", e);
        }
        incoming_queue.setAttribute("datetime_unpack_start",datetime_unpack_start);
        if (incoming_queue.save() < 0) {
            //log and ignore - non-critical processing
            logError("Problem saving unpack start date time. Ignoring.");
         }


        /*
        DTD Validation should take place here even before unpackager is called, since some XML
        processing needs to be performed. Initialize isXmlValidAgainstDtd=true, in the case when
        IS_DTD_VALIDATION is turned off in AgentConfiguration.properties file.
        */

        ////////////////////////////////////////////////
        //        BEGIN DTD VALIDATION                //
        ////////////////////////////////////////////////
        boolean isXmlValidAgainstDtd = true;
        if (IS_DTD_VALIDATION_ON.equals("true")) {
            String xmlString = msg_text;
            int beginIndex  = msg_text.indexOf("<MessageType>");
            int endIndex    = msg_text.indexOf("</MessageType>");

            if (endIndex == -1 || beginIndex == -1 || beginIndex > endIndex){
                isXmlValidAgainstDtd = false;
                logInfo("Pre-DTD Validation of MessageType attribute failed");
            }
            else{
                msg_type = msg_text.substring(beginIndex + "<MessageType>".length(), endIndex);
                String dtdString = determineDTDType(msg_type);
                logInfo("msg_type " + msg_type);
                String dtdPlusXmlString = dtdString + xmlString;
                //IValavala POUM080661913
                isXmlValidAgainstDtd = XMLUtility.validateXMLAgainstDTDFully(dtdPlusXmlString,AGENT_ID);
            }
            logInfo("isXmlValidAgainstDtd: "  + isXmlValidAgainstDtd);
        }
        ////////////////////////////////////////////////
        //          END DTD VALIDATION                //
        ////////////////////////////////////////////////

        //////////////////////////////////////////////////////////////////
        //      BEGIN PROCESSING OF: isXmlValidAgainstDtd = TRUE        //
        //////////////////////////////////////////////////////////////////
        if (isXmlValidAgainstDtd == true){

            /*
            Even though incoming message has just been DTD-validated, it is still
            possible that msg_text contains invalid data which causes DocumentHandler
            constructor to fail (maybe a bug in DocumentHandler). This, in turn, creates "poisoned" orphan.
            */
            try{
                messageDoc = new DocumentHandler(msg_text, true, true); //Note we keep the carriage return here for swift message
            }
            catch(Exception e){
                unpackaging_error_text = "DocumentHandler constructor failed since msg_text contains invalid data. " + msg_text;
                logError(unpackaging_error_text);
                try{
                    incoming_queue.setAttribute("unpackaging_error_text", unpackaging_error_text);
                    // W Zhu 11/14/2007 WEUH111359884 BEGIN
                    // Check return value of save().  Some data could be invalid.
                    if (incoming_queue.save() < 0) {
                    	throw new AmsException("Cannot update incoming_queue data.");
                     }
                    // W Zhu 11/14/2007 WEUH111359884 END
                }
                catch(AmsException sube){
                    logError("This message contains wrong data. Manual intervension is required", sube);
                    return AGENT_FAILURE;
                }
                processAfterStatusUnpackaged();
                return AGENT_FAILURE;
            }

            if(messageDoc.getAttribute("/Proponix/SubHeader/InstrumentIdentification/Instrument/InstrumentID") != null)
            {
            	String existingInstrumentTypeCode = null;
            	try
            	{
            		existingInstrumentTypeCode = getExistingInstrumentTypeCode(messageDoc.getAttribute("/Proponix/SubHeader/InstrumentIdentification/Instrument/InstrumentID"));
            	}
            	catch(AmsException e)
            	{
            		logError("InboundAgent.processAfterStatusInitialized(): Caught the following exection while trying to get existing instrument type code for instrument "
            				+messageDoc.getAttribute("/Proponix/SubHeader/InstrumentIdentification/Instrument/InstrumentID"), e);
            		return AGENT_FAILURE;
            	}

            	String existingInstrumentProponixCustomerID = null;
            	try
            	{
            		existingInstrumentProponixCustomerID = getExistingInstrumentProponixCustomerID(messageDoc.getAttribute("/Proponix/SubHeader/InstrumentIdentification/Instrument/InstrumentID"));
            	}
            	catch(AmsException e)
            	{
            		logError("InboundAgent.processAfterStatusInitialized(): Caught the following exection while trying to get existing Proponix Customer ID for instrument "
            				+messageDoc.getAttribute("/Proponix/SubHeader/InstrumentIdentification/Instrument/InstrumentID"), e);
            		return AGENT_FAILURE;
            	}

            	if((existingInstrumentTypeCode != null)
            		&& (existingInstrumentProponixCustomerID != null)
            		&& (messageDoc.getAttribute(UniversalMessage.instrumentPath+"/InstrumentTypeCode") != null)
            		&& (messageDoc.getAttribute("/Proponix/SubHeader/CustomerID") != null)
            		&& (existingInstrumentTypeCode.equals(InstrumentType.REQUEST_ADVISE))
            		&& (existingInstrumentProponixCustomerID.equals(messageDoc.getAttribute("/Proponix/SubHeader/CustomerID")))
           			&& (messageDoc.getAttribute(UniversalMessage.instrumentPath+"/InstrumentTypeCode").equals(InstrumentType.EXPORT_DLC)))
           		{
					messageDoc.setAttribute(UniversalMessage.instrumentPath+"/InstrumentTypeCode", InstrumentType.REQUEST_ADVISE);

					if((messageDoc.getAttribute(UniversalMessage.transactionPath+"/TransactionTypeCode") != null)
							&& (messageDoc.getAttribute(UniversalMessage.transactionPath+"/TransactionTypeCode").equals(TransactionType.ADVISE)))
					{
						messageDoc.setAttribute(UniversalMessage.transactionPath+"/TransactionTypeCode", TransactionType.ISSUE);
					}
           		}
            }

            /*
            The difference between InboundAgent and OutboundAgent in processAfterStatusInitialized()
            is that InboundAgent has to set a lot more fileds in addition to status = STATUS_PACKAGED/STATUS_UNPACKAGED.
            Following are such fields extracted from messageDoc, where messageDoc is XML representation
            of a msg_text.

            Save the completeInstrumentID and transactionTypeCode to Incoming_Queue to facilitate identification
            of the message related to a particular Instrument & transaction. For History messages with more
            than one transaction, transactionTypeCode of the first transaction is saved.
            */
            date_sent                   = "";

            String dateSent             = messageDoc.getAttribute("/Proponix/Header/DateSent");
            String timeSent             = messageDoc.getAttribute("/Proponix/Header/TimeSent");

            completeInstrumentID = messageDoc.getAttribute("/Proponix/SubHeader/InstrumentIdentification/Instrument/InstrumentID");
            transactionTypeCode  = messageDoc.getAttribute("/Proponix/Body/InstrumentTransaction/Transaction/TransactionTypeCode");
            clientBank           = messageDoc.getAttribute("/Proponix/SubHeader/InstrumentIdentification/ClientBank/OTLID");

            msg_type                = messageDoc.getAttribute("/Proponix/Header/MessageType");
            message_id              = messageDoc.getAttribute("/Proponix/Header/MessageID");
			senderID				= messageDoc.getAttribute("/Proponix/Header/SenderID");
            if(msg_type.equals(MessageType.MAIL) || msg_type.equals(MessageType.INSSTAT) || msg_type.equals(MessageType.WORKSTAT))
            {
			 completeInstrumentID = messageDoc.getAttribute("/Proponix/SubHeader/InstrumentID");
           	 transactionTypeCode  = messageDoc.getAttribute("/Proponix/SubHeader/TransactionTypeCode");
           	 clientBank           = messageDoc.getAttribute("/Proponix/SubHeader/ClientBank/OTLID");

			}

            reply_to_message_id     = "";
            confirmation_error_text = "";

            if (msg_type.equals(MessageType.CONFIRMATION)){
                reply_to_message_id     = messageDoc.getAttribute("/Proponix/Body/ReplytoMessageID");

                if (messageDoc.getAttribute("/Proponix/Body/ProcessingStatus").equals(PROCESSING_STATUS_FAIL)){
                    confirmation_error_text = messageDoc.getAttribute("/Proponix/Body/ErrorText");
                }
            }



            //IR - SEUK021238026 - BEGIN.
			try {
			   	if (msg_type.equals(MessageType.CONFIRMATION) && senderID.equals(SENDER_ID_ANZ)) {
			   		dateSent = DateTimeUtility.formatDate(convertStringDateToDate(dateSent), "MM/dd/yyyy HH:mm:ss");
			   		timeSent = timeSent.substring(0, 2) + ":" + timeSent.substring(2,4) + ":00" ;
			   	}
			}
			catch(AmsException e)
			{
			   	logError("InboundAgent.processAfterStatusInitialized(): Caught the following error while converting date sent " + dateSent , e);
			   	return AGENT_FAILURE;
			}
            //IR - SEUK021238026 - END.

			//get the message broker timestamp
            String msgBrokerDate = messageDoc.getAttribute("/Proponix/Timestamp/Date");
            String msgBrokerTime = messageDoc.getAttribute("/Proponix/Timestamp/Time");
            if ( msgBrokerDate != null && msgBrokerDate.length() == 8 &&
                 msgBrokerTime != null && msgBrokerTime.length() == 6 ) {
	            //validate the date by converting to a date and formatting properly
		        try{
	        	    SimpleDateFormat msgBrokerDateTimeFormatter = new SimpleDateFormat("yyyyMMdd HHmmss");
	        	    msgBrokerDateTimeFormatter.setTimeZone(TimeZone.getTimeZone("GMT")); //explicitly set GMT
		            Date d = msgBrokerDateTimeFormatter.parse(msgBrokerDate + " " + msgBrokerTime);
		            msgBrokerDateTime = DateTimeUtility.convertDateToDateTimeString(d);
		        }catch(Exception e){
		            logError("InboundAgent.processAfterStatusInitialized(): " +
		                "Error converting message broker date: " + msgBrokerDate +
		                " and time: " + msgBrokerTime + " to java.util.Date object. Ignoring. " +
		                "Nested exception: " + e.toString());
		        }
            } else {
	            logError("InboundAgent.processAfterStatusInitialized(): " +
		                "Invalid message broker date: " + (msgBrokerDate==null?"null":msgBrokerDate) +
		                " and time: " + (msgBrokerTime==null?"null":msgBrokerTime) +
		                ". Ignoring.");
            }

            /*
            Now when most of attributes are set, InboundAgent should wait until unpackager performs service below.
            Based on unpackager's transaction success, InboundAgent will update above attributes in addition
            to status = STATUS_UNPACKAGED, etc. Create an instance of unpackager depending on msg_type.
            */
            if (determineUnPackager(msg_type) == null){
                logError("No unpackager exists for this msg_type: " + msg_type);
                mqWrapper.attemptToPutMessageOnQueue("No unpackager exists for this msg_type: " + msg_type, mqWrapper.mqErrorQueue);
                return AGENT_FAILURE;
            }
            try {
                someUnPackagerMediator = (Mediator)EJBObjectFactory.createClientEJB(ejbServerLocation, determineUnPackager(msg_type)/*, csdb*/);
            }
            catch(AmsException e){
                logError("While creating an instance of client EJBs: someUnPackagerMediator", e);
                return AGENT_FAILURE;
            }

            ClientServerDataBridge csdb = new ClientServerDataBridge();
            csdb.setLocaleName(LOCALE_NAME);

            inputDoc.addComponent("/In", messageDoc);

            //PPX-208 - Ravindra B - 03/18/2011 - Start
            //AgentId is passing to UnPackagerMediatorBean
            inputDoc.setAttribute("/In/AgentID", AGENT_ID);
            //PPX-208 - Ravindra B - 03/18/2011 - End

            try{
                // W Zhu 6/16/2015 Rel 9.3 T36000031888 use string to avoid stack overflow.
                //outputDoc = someUnPackagerMediator.performService(inputDoc, csdb);
                String outputDocString = someUnPackagerMediator.performService(inputDoc.toString(), csdb);
                outputDoc = new DocumentHandler(outputDocString, false, true);
                System.out.println("outputDoc:"+outputDoc);
                someUnPackagerMediator.remove();
            }
            catch (Exception e){
                logError("While calling performService on "+determineUnPackager(msg_type), e);
            }

            finally {
                try{
                    int severityLevel   = 0;
                    List<DocumentHandler>  errorList   = outputDoc.getFragmentsList("/Error/errorlist/error");
                    for (DocumentHandler errorDoc: errorList) {
                        severityLevel = errorDoc.getAttributeInt("/severity");
                        if (severityLevel >= ErrorManager.ERROR_SEVERITY){
                            transactionSuccess = false;
                        }
                    }
                }
                catch (Exception e) {
                    logError("Error occured while reading errors ", e);
                }
            }

            //IValavala CheckIf Resubmit only if there were no errors.
            String resubmitInd;
            resubmitInd = outputDoc.getAttribute("/Out/Param/ResubmitMsg");
            if (resubmitInd!= null && resubmitInd.equals("Y")) transactionResubmit = true;
            logInfo("transactionSuccess: " + transactionSuccess);
            logInfo("transactionResubmit: " + transactionResubmit);

            /*
            There is no extraction of "/Out/Proponix" from outputDoc in InboundAgent.
            This extraction takes place only in OutboundAgent since that agent produces XML.
            Extract everything under "/In" and "/Error" sections in outputDoc for unsuccessfully unpackaged message.
            */
            if(outputDoc.getFragment("/Error/") != null)
             {
                inputDoc.setComponent("/Error", outputDoc.getFragment("/Error/"));
             }
            else
             {
                inputDoc.setAttribute("/Error", "Null Error Fragment");
             }

            packagedError = inputDoc.toString();
            //packagedError = inputDoc.toString() + outputDoc.getFragment("/Error/").toString();

            //////////////////////////////////////////////////////////////////
            //             BEGIN CHECK FOR DUPLICATE MESSAGES               //
            //////////////////////////////////////////////////////////////////
            if (transactionSuccess) {
				hasDuplicateMessageIdExisted = doesDBContainDuplicateMessageId(message_id, incoming_queue.getAttribute("queue_oid"));
				if (hasDuplicateMessageIdExisted == true){
					logError("Message with same message_id=" + message_id +" already exists. No processing is required");
					packagedError = "Message with same message_id=" + message_id + " already exists. No processing is required";
				}
			}
            //////////////////////////////////////////////////////////////////
            //             END CHECK FOR DUPLICATE MESSAGES                 //
            //////////////////////////////////////////////////////////////////

        }
        //////////////////////////////////////////////////////////////////
        //        END PROCESSING OF: isXmlValidAgainstDtd = TRUE        //
        //////////////////////////////////////////////////////////////////

        //////////////////////////////////////////////////////////////////
        //      BEGIN PROCESSING OF: isXmlValidAgainstDtd = FALSE       //
        //////////////////////////////////////////////////////////////////
        else {
            logError("Message did not pass DTD validation");
            packagedError = "Message did not pass DTD validation. See log for details " + msg_text;

            //Get SenderID and MessageID. iv Jul 31 2006
            int beginSIndex  = msg_text.indexOf("<SenderID>");
		    int endSIndex    = msg_text.indexOf("</SenderID>");

			 if (endSIndex == -1 || beginSIndex == -1 || beginSIndex > endSIndex){
			            }
			 else{
			       senderID = msg_text.substring(beginSIndex + "<SenderID>".length(), endSIndex);
		 		}
            int beginMIndex  = msg_text.indexOf("<MessageID");
		    int endMIndex    = msg_text.indexOf("</MessageID>");

			 if (endMIndex == -1 || beginMIndex == -1 || beginMIndex > endMIndex){
			            }
			 else{
			       message_id = msg_text.substring(beginMIndex + "<MessageID>".length(), endMIndex);
		 		}

        }
        //////////////////////////////////////////////////////////////////
        //        END PROCESSING OF: isXmlValidAgainstDtd = FALSE       //
        //////////////////////////////////////////////////////////////////

        String mailMessageOid= "";
        String transactionOid  = "";
        String instrumentOid   = "";

        try{


            if (transactionSuccess == false || isXmlValidAgainstDtd == false || hasDuplicateMessageIdExisted == true){
                unpackaging_error_text = packagedError;
            }
            else{
                unpackaging_error_text = ""; //make sure that value gets reset!!!
                mailMessageOid =  outputDoc.getAttribute("/Out/Param/MailMessageOid");
                transactionOid =  outputDoc.getAttribute("/Out/Param/TransactionOid");
                instrumentOid  =  outputDoc.getAttribute("/Out/Param/InstrumentOid");


                // NSX - CR-542 03/16/10 - Begin
                if ((msg_type.equals(MessageType.EODTRNIN) || msg_type.equals(MessageType.PAYFILE)) && "true".equals(outputDoc.getAttribute("/Out/Param/messageRouted"))){   // CR-593 NSX 04/04/11  Rel. 7.0.0
                	//  set status to STATUS_MESSAGE_ROUTED for routed messages
                	status = STATUS_MESSAGE_ROUTED;
                	messageRouted = true;





                }
                else {
                	status = STATUS_UNPACKAGED;
                }
                // NSX - CR-542 03/16/10 - End


            }
            /*
            Following attributes currently are to be set unconditionally. These are error
            prone attributes from external data. So, if problems occur, place them into above else-bracket.
            */

            incoming_queue.setAttribute("date_sent",                date_sent);
            incoming_queue.setAttribute("msg_type",                 msg_type);
            incoming_queue.setAttribute("confirmation_error_text",  confirmation_error_text);

            //Following attributes are to be set unconditionally
            incoming_queue.setAttribute("message_id",               message_id);
            incoming_queue.setAttribute("reply_to_message_id",      reply_to_message_id);
            incoming_queue.setAttribute("unpackaging_error_text",   unpackaging_error_text);
             incoming_queue.setAttribute("status",                   status);
            incoming_queue.setAttribute("mail_message_oid",         mailMessageOid);
            incoming_queue.setAttribute("instrument_oid",           instrumentOid);
            incoming_queue.setAttribute("transaction_oid",          transactionOid);
            incoming_queue.setAttribute("complete_instrument_id",   completeInstrumentID);
            incoming_queue.setAttribute("transaction_type_code",    transactionTypeCode);
            incoming_queue.setAttribute("client_bank",              clientBank);
            incoming_queue.setAttribute("datetime_msgbroker",       msgBrokerDateTime);


            //IValavala CR454
            if (transactionResubmit){
                /* Set date_received to current GMT time */
                try{
                    date_received = DateTimeUtility.getGMTDateTime(false);
                }
                catch (AmsException e){
                    logError("While obtaining GMT date and time using JPylon DateTimeUtility", e);
                }
                incoming_queue.setAttribute("date_received",   date_received);
                incoming_queue.setAttribute("status",           STATUS_RESUBMIT);
            }
            // W Zhu 11/14/2007 WEUH111359884 BEGIN
            // Check return value of save().  Some data could be invalid.
            if (incoming_queue.save() < 0) {
            	throw new AmsException("Cannot update incoming_queue data.");
             }
            // W Zhu 11/14/2007 WEUH111359884 END



      }
      catch(AmsException e){
            /*
            What if date_sent="15/15/2001 20::1:00" or "// 00:00:00" ? Since InboundAgent has no control over
            external data, extra care should be taken in case of so-called "poisoned" messages.
            These messages will never be able to get out of 'Initialized' state. If left untreated,
            these messages result in "poisoned" orphans that never get out of 'Initilized' state either,
            thus causing InboundAgent to indefinitely attempt to process them at a start up.
            */
            try{
                long queue_oidLong = incoming_queue.getAttributeLong("queue_oid");
                removeIncomingInterfaceQueue();
                incoming_queue = (IncomingInterfaceQueue) EJBObjectFactory.createClientEJB(ejbServerLocation, "IncomingInterfaceQueue", queue_oidLong);

                unpackaging_error_text = "While setting attributes (date_sent, msg_type, message_id, reply_to_message_id, " +
                                         "confirmation_error_text, unpackaging_error_text, status) on incoming_queue table ";
                logError(unpackaging_error_text, e);
                unpackaging_error_text += e.getMessage();
                incoming_queue.setAttribute("unpackaging_error_text", unpackaging_error_text + e.getMessage());
                // W Zhu 11/14/2007 WEUH111359884 BEGIN
                // Check return value of save().  Some data could be invalid.
                if (incoming_queue.save() < 0) {
                	throw new AmsException("Cannot update incoming_queue data.");
                 }
                // W Zhu 11/14/2007 WEUH111359884 END
            }
            catch(AmsException sube){
                logError("This message contains wrong data. Manual intervension is required", sube);
                return AGENT_FAILURE;
            }

            if (!messageRouted) {            // NSX - CR-542 03/16/10
            	processAfterStatusUnpackaged();

            }                                // NSX - CR-542 03/16/10
            return AGENT_FAILURE;
      }

      //IValavala CR454. If resubmit/messageRouted, no need to create Confirmation messages.
      //if (transactionResubmit) return AGENT_SUCCESS;                  // NSX - CR-542 03/16/10
      if (transactionResubmit || messageRouted) return AGENT_SUCCESS;   // NSX - CR-542 03/16/10

      int processSuccess = processAfterStatusUnpackaged();



      return processSuccess;

    } // end of processAfterStatusInitialized()


    protected int processAfterStatusUnpackaged() throws RemoteException {

        int mqSuccess = -1;

        /*
        Following code applies to both regular and confirmational messages.
        Update status depending on existance of unpackaging_error_text.
        */

        try{


            if (unpackaging_error_text == null || unpackaging_error_text.equals("")){
                   status = STATUS_COMPLETED;
            }
            else{
                status = STATUS_ERROR;
                mqSuccess = mqWrapper.attemptToPutMessageOnQueue(unpackaging_error_text, mqWrapper.mqErrorQueue);
                if (mqSuccess != 1) { return AGENT_FAILURE; }
            }
            incoming_queue.setAttribute("status", status);

            //capture the unpack end date/time
            //Note that unpacking in this context includes changing to complete/error status
            String datetime_unpack_end = "";
            try{
                // W Zhu CR-564 6.1 12/10/2010 pass false for override.  Do not use override datetime, which stays the same and makes pack_seconds = 0.
                datetime_unpack_end = DateTimeUtility.getGMTDateTime(false, false);
            }
            catch (AmsException e){
                //log and ignore - non-critical processing
                logError("Problem obtaining unpack end date time", e);
            }
            incoming_queue.setAttribute("datetime_unpack_end",datetime_unpack_end);

            // W Zhu 11/14/2007 WEUH111359884 BEGIN
            // Check return value of save().  Some data could be invalid.
            if (incoming_queue.save() < 0) {
            	throw new AmsException("Cannot update incoming_queue data.");
             }
            // W Zhu 11/14/2007 WEUH111359884 END



        }
        catch(AmsException e){
            logError("In processAfterStatusUnpackaged while setting status in incoming_queue table", e);
            return AGENT_FAILURE;
        }


            /*
            Following is creation of outgoing event for regular message only. (i.e. not a confirmation message)
            Outgoing event for InboundAgent is insertion of a row into outgoing_queue table with status = STATUS_START.
            There are no outgoing events for confirmation messages, since no confirmation message is expected in reply
            to a confirmation message.
            */
            if (    msg_type.equals(MessageType.TPL)       ||
            		msg_type.equals(MessageType.TPLH)      ||//Leelavathi PPX-208 29/4/2011 added
                    msg_type.equals(MessageType.HISTORY)   ||
                    msg_type.equals(MessageType.MAIL)      ||
                    msg_type.equals(MessageType.PURGE)     ||
                    msg_type.equals(MessageType.INSSTAT)   ||
                    msg_type.equals(MessageType.INSSTATH)  ||//Leelavathi PPX-208 29/4/2011 added
                    msg_type.equals(MessageType.INSSTATR)  ||//Leelavathi PPX-208 29/4/2011 added
		            msg_type.equals(MessageType.WORKSTAT)  ||
		            msg_type.equals(MessageType.REFDATA)   ||
            	    msg_type.equals(MessageType.PAYINVOT)  ||
            	    msg_type.equals(MessageType.EODTRNIN)  ||
		            msg_type.equals(MessageType.INVSTO)    ||
        		    msg_type.equals(MessageType.TPLFXRT)   ||   //  CR-469 NShrestha 05/18/2009
            	    msg_type.equals(MessageType.ARINVOICE) ||
            	    msg_type.equals(MessageType.PAYSTAT)   ||   // CR-596 Narayan 11/22/2010
            	    msg_type.equals(MessageType.DATAIN)   ||
            	    msg_type.equals(MessageType.PAYFILE)  ||  // CR-593 NSX 04/04/11  Rel. 7.0.0
            	    msg_type.equals(MessageType.PINCNAPP) || //CR1006
            	    msg_type.equals(MessageType.INTEREST_RATE))  // Srini_D CR-713 Rel 8.0 10/18/2011
                    { //begin of first if-statement

                // Create message_id on outgoing_queue table
                String new_message_id = null;
                try{
                    new_message_id  = Long.toString(ObjectIDCache.getInstance(AmsConstants.MESSAGE).generateObjectID(false));
                    /* IValera - IR# JOUI111280267 - 02/18/2009 - Change Begin */
                    /* To avoid duplication of message id across servers, appends last character of 'serverName' to new message id */
                    PropertyResourceBundle portalProperties = (PropertyResourceBundle)PropertyResourceBundle.getBundle("AgentConfiguration");
                    String serverName = portalProperties.getString("serverName");
                    new_message_id += serverName.substring(serverName.length() - 2);

                }
                catch(AmsException e){
                    logError("While creating new message_id on outgoing_queue table", e);
                    return AGENT_FAILURE;
                }

                // Create date_created on outgoing_queue table
                String date_created;
                try{
                    date_created = DateTimeUtility.getGMTDateTime(false);
                }
                catch (AmsException e){
                    logError("While obtaining GMT date and time using DateTimeUtility", e);
                    return AGENT_FAILURE;
                }

                /*
                If unpackaging_error_text is empty:
                1. insert a row into a outgoing_queue table where msg_type=MSG_TYPE_CONFIRMATION

                If unpackaging_error_text is not empty:
                Note: don't update unpackaging_error_text on incoming_queue table, since it's done in processAfterStatusInitialized()
                1. insert a row into a outgoing_queue table where msg_type=MSG_TYPE_CONFIRMATION
                   and outgoing_queue.unpackaging_error_text = incoming_queue.unpackaging_error_text
                */

				/*If senderID is UBC append it to reply_to_message_id*/
				String message_id_sender;
				message_id_sender = message_id ;
				logInfo("senderID = "+senderID+" message_id_sender = "+message_id_sender+ " message_id = "+message_id);


                try{
                    outgoing_queue = (OutgoingInterfaceQueue) EJBObjectFactory.createClientEJB(ejbServerLocation, "OutgoingInterfaceQueue");
                    outgoing_queue.newObject();
                    outgoing_queue.setAttribute("date_created",             date_created);
                    outgoing_queue.setAttribute("status",                   STATUS_START);
                    outgoing_queue.setAttribute("date_created",             date_created);
                    outgoing_queue.setAttribute("unpackaging_error_text",   unpackaging_error_text);
                     // CR-593 NSX 04/04/11  Rel. 7.0.0  - Begin
                    if (MessageType.PAYFILE.equals(msg_type)) {
                        outgoing_queue.setAttribute("msg_type",              MessageType.PAYBACK);
                     }
                    else {
                    	// CR-593 NSX 04/04/11  Rel. 7.0.0  - End
                        outgoing_queue.setAttribute("msg_type",                 MessageType.CONFIRMATION);
                    }  // CR-593 NSX 04/04/11  Rel. 7.0.0  -
                    outgoing_queue.setAttribute("message_id",               new_message_id);
                    outgoing_queue.setAttribute("reply_to_message_id",      message_id_sender);    //reply_to_message_id translates to message_id
                    // W Zhu 11/14/2007 WEUH111359884 BEGIN
                    // Check return value of save().  Some data could be invalid.
                    // IValavala 05/02/2009 BEGIN. Add destinationID to process_params
                    //   CR-593 NSX 04/04/11  Rel. 7.0.0  - Begin
                    if (MessageType.PAYFILE.equals(msg_type)) {
                    	DocumentHandler messageDoc = new DocumentHandler(msg_text, true, true);
                    	DocumentHandler processDoc = messageDoc.getFragment("/Proponix/SubHeader");
                    	processDoc.setAttribute("/SenderID", senderID); // Nar CR694A Rel9.0
                    	outgoing_queue.setAttribute("process_parameters",    processDoc.toString());
                    }
                    else
                    	// CR-593 NSX 04/04/11  Rel. 7.0.0  - End
                    	if (senderID!=null && !senderID.equals(TradePortalConstants.SENDER_ID_PROPONIX)) {
    					outgoing_queue.setAttribute("process_parameters",    "destination_id=" + senderID );
    				}

                    //PMitnala Rel9.1 - CR 970 START
                    //For MsgType = MSG_TYPE_PAYFILE, If Transaction level edits are successful, that means if unpackaging_error_text is empty,
                    //do not insert a row into a outgoing_queue table. Proceed for Beneficiary level validations and then send a PAYBACK message
                    if (MessageType.PAYFILE.equals(msg_type) && StringFunction.isBlank(unpackaging_error_text)) {
                    	logInfo("Outgoing PAYBACK message to be sent after Beneficiary Level Edits for message_id:"+message_id);
                    }else {
                    	if (outgoing_queue.save() < 0) {
	                    	throw new AmsException("Cannot update outgoing_queue data.");
	                     }
                    }
                    //PMitnala Rel9.1 - CR 970 END
                    // W Zhu 11/14/2007 WEUH111359884 END
                    return AGENT_SUCCESS;
                }

                catch(AmsException e){
                    logError("processAfterStatusUnpackaged(): While setting attributes of outgoing_queue table", e);
                    return AGENT_FAILURE;
                }
          }//end of first if-statement
          return AGENT_SUCCESS;
    }//end of processAfterStatusUnpackaged()


   /**
    * This method checks for duplicate messages_id (generated by OTL) on incoming_queue table.
    * Having duplicate message_id happens when MQAgent succeeds in writing new message into DB,
    * but fails to commit to MQManager (thus leaving the message on Incoming Queue ).
    * Also, since some minimal XML processing is needed, this method has to be called after validateXMLAgainstDTD().
    *
    * @param DocumentHandler messageDoc
    * @return boolean
    */
    protected boolean doesDBContainDuplicateMessageId(String message_id, String queueOid) throws RemoteException {
        try {
            logInfo("Select queue_oid from incoming_queue where message_id='" + message_id + "' and queue_oid <> '" + queueOid + "'");
            query.setSQL("Select queue_oid from incoming_queue where message_id=? and queue_oid <> ?", new Object[]{message_id, queueOid});
            query.getRecords();
            if (query.getRecordCount() == 1) {
                logError("Duplicate message_id=" + message_id + " is found");
                return true;
            }
        } catch (AmsException e) {
            logError("While performing check for duplicate message_id", e);
        }
        return false;
    }// end of checkForDuplicateMessageId()


   @Override
     protected String determineDTDType(String msg_type){

        String dtdString = null;

        if (msg_type.equals(MessageType.CONFIRMATION))  { dtdString  = CONFIRMATION_DTD;}
        else if (msg_type.equals(MessageType.TPL))           { dtdString  = TPL_INBOUND_DTD;}
        else if (msg_type.equals(MessageType.TPLH))           { dtdString  = TPLH_INBOUND_DTD;}//Leelavathi PPX-208 29/4/2011 added
        else if (msg_type.equals(MessageType.PURGE))         { dtdString  = PURGE_DTD;}
        else if (msg_type.equals(MessageType.HISTORY))       { dtdString  = HISTORY_DTD;}
        else if (msg_type.equals(MessageType.MAIL))          { dtdString  = MAIL_INBOUND_DTD;}
        else if (msg_type.equals(MessageType.INSSTAT))       { dtdString  = INSSTAT_INBOUND_DTD;}
        else if (msg_type.equals(MessageType.INSSTATH))       { dtdString  = INSSTATH_INBOUND_DTD;}//Leelavathi PPX-208 29/4/2011 added
        else if (msg_type.equals(MessageType.INSSTATR))       { dtdString  = INSSTATR_INBOUND_DTD;}//Leelavathi PPX-208 29/4/2011 added
        else if (msg_type.equals(MessageType.WORKSTAT))      { dtdString  = WORKITEM_INBOUND_DTD;}
        else if (msg_type.equals(MessageType.REFDATA))       { dtdString  = REFDATA_INBOUND_DTD;}
        else if (msg_type.equals(MessageType.PAYINVOT))      {dtdString  = PAYINV_DTD;}
        else if (msg_type.equals(MessageType.EODTRNIN))      {dtdString  = EODTRNIN_DTD;}	// peter ng - 3rd Dec 08 - CR-451
	else if (msg_type.equals(MessageType.ARINVOICE))     {dtdString  = ARINV_DTD;}
	else if (msg_type.equals(MessageType.INVSTO))        {dtdString  = INVSTO_DTD;} //Vshah 12/11/2008 INVSTO UnPackager
        else if (msg_type.equals(MessageType.TPLFXRT))       {dtdString  = FXRATE_INBOUND_DTD;}  //  CR-469 NShrestha 05/18/2009
        else if (msg_type.equals(MessageType.PAYSTAT))       {dtdString  = PAYSTAT_DTD;}  //  CR-596 Narayan 11/22/2010
        else if (msg_type.equals(MessageType.PAYFILE))       {dtdString  = PAYFILE_DTD;}  // CR-593 NSX 04/04/11  Rel. 7.0.0
        else if (msg_type.equals(MessageType.DATAIN))       {dtdString  = GENERIC_INCOMING_DTD;}
        else if (msg_type.equals(MessageType.INTEREST_RATE))       {dtdString  = INTEREST_RATE_DTD; }// Srini_D CR-713 Rel 8.0 10/18/2011
        else if (msg_type.equals(MessageType.PINCNAPP))       {dtdString  = PINCNAPP_DTD;} //CR1006

        return dtdString;
    }

    /**
     *
     * @param completeInstrumentID
     * @return
     */
    private String getExistingInstrumentTypeCode(String completeInstrumentID) throws AmsException {
        if (completeInstrumentID != null) {
            String sqlStatement = "select INSTRUMENT_TYPE_CODE "
                    + "from INSTRUMENT "
                    + "where COMPLETE_INSTRUMENT_ID=?";
            DocumentHandler resultXML = DatabaseQueryBean.getXmlResultSet(sqlStatement, false, new Object[]{completeInstrumentID});

            if ((resultXML != null)
                    && (resultXML.getAttribute("/ResultSetRecord(0)/INSTRUMENT_TYPE_CODE") != null)) {
                return resultXML.getAttribute("/ResultSetRecord(0)/INSTRUMENT_TYPE_CODE");
            }
        }

        return null;
    }

    /**
     *
     * @param completeInstrumentID
     * @return
     * @throws AmsException
     */
    private String getExistingInstrumentProponixCustomerID(String completeInstrumentID) throws AmsException {
        if (completeInstrumentID != null) {
            String sqlStatement = "SELECT PROPONIX_CUSTOMER_ID "
                    + "FROM CORPORATE_ORG, INSTRUMENT "
                    + "WHERE CORPORATE_ORG.organization_oid=INSTRUMENT.a_corp_org_oid "
                    + "AND INSTRUMENT.complete_instrument_id=?";
            DocumentHandler resultXML = DatabaseQueryBean.getXmlResultSet(sqlStatement, false, new Object[]{completeInstrumentID});

            if ((resultXML != null)
                    && (resultXML.getAttribute("/ResultSetRecord(0)/PROPONIX_CUSTOMER_ID") != null)) {
                return resultXML.getAttribute("/ResultSetRecord(0)/PROPONIX_CUSTOMER_ID");
            }
        }
        return null;
    }

    // peter ng - 3rd Dec 08 - CR-451
    private static Date convertStringDateToDate(String stringDate) throws AmsException {
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
            return formatter.parse(stringDate);
        } catch (Exception e) {
            throw new AmsException("Error converting date: " + stringDate + " to java.util.Date object. Nested exception: " + e.getMessage());
        }
    }

    /**
     * This method deletes AgentId from incoming_queue table for given messageID
     *
     * @param agentId
     * @return
     * @throws AmsException
     */
    private static synchronized void removeInstrumentLock(String agentId) {

        String sqlStatement = "DELETE FROM INSTRUMENT_LOCK WHERE AGENT_ID = ?";
        try {
            DatabaseQueryBean.executeUpdate(sqlStatement, false, new Object[]{agentId});
        } catch (AmsException | SQLException e) {
            // TODO Auto-generated catch block
            System.out.println("Problem with DB connection.");
            e.printStackTrace();
        }

    }

    @Override
    protected  boolean connectToMQ() {
     return true;
    }

}// end of InboundAgent class
