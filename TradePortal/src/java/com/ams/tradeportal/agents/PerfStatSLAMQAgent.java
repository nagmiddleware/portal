package com.ams.tradeportal.agents;


import com.ams.tradeportal.common.TPDateTimeUtility;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.util.DateTimeUtility;
import com.amsinc.ecsg.util.StringFunction;
import com.ibm.mq.MQException;

/*
 *     Copyright  ?2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class PerfStatSLAMQAgent extends TradePortalAgent {
//private static final Logger LOG = LoggerFactory.getLogger(PerfStatSLAMQAgent.class);

    private static final String PORTAL_SLA_QUEUE = "PortalSLAQueue";
    private String msg_text;

    @Override
    public void setAgentId(String agentId) {
        super.setAgentId(agentId);
        try {

            mqWrapper.mqInputQueue = properties.getString("mqInputQueue.slaQueue").trim();
        } catch (Exception e) {
            mqWrapper.mqInputQueue = PORTAL_SLA_QUEUE;
            e.printStackTrace();
        }
        logInfo("SLA Queue Name : " + mqWrapper.mqInputQueue);
    }

    @Override
    public int lookForWorkAndDoIt() {
        /*  Read with a syncpoint off the MQ Input Queue. */
        try {
            msg_text = mqWrapper.attemptToGetMessageFromQueue();
        } catch (Exception e) {
            e.printStackTrace();
            return AGENT_SUCCESS;
        }
        if (msg_text != null && !msg_text.equals("")) {
        	// W Zhu 12/20/07 WAUH121261057 #22 BEGIN 
            // Log the header and subheader or at most 500 characters.
            String displayMsgText;

            displayMsgText = msg_text;

            logInfo("New message found on Portasl SLA Input Queue (" + msg_text.length() + " Bytes): " + displayMsgText);
            // W Zhu 12/20/07 WAUH121261057 #22 END            
            boolean saveSuccess = saveMessageToDB(msg_text);
            if (saveSuccess == true) {
                try {
                    mqWrapper.commit();
                } catch (MQException mqe) {
                    logError("MQ Manager failed to commit", mqe);
                }
            } else {
                try {
                    mqWrapper.backout();
                } catch (MQException mqe) {
                    logError("MQ Manager failed to backout", mqe);
                }
            }
            logInfo("Finished processing new message found on MQ Input Queue");
        } else {
            return AGENT_NO_RECORDS_TO_PROCESS;

        }
        return AGENT_SUCCESS;
    }

    protected boolean saveMessageToDB(String msg_text) {

        /* Set date_received to current GMT time */
//        String date_received;
//        try {
//            date_received = DateTimeUtility.getGMTDateTime(false);
//        } catch (AmsException e) {
//            logError("While obtaining GMT date and time using JPylon DateTimeUtility", e);
//            return false;
//        }

        int saveSuccess = -1;

        // W Zhu 2/20/08 WEUH121436595 BEGIN
        // Set message type so that we could use different agents to process different msg types.
        String messageType = null;
        int messageTypeBeginIndex = msg_text.indexOf("<MessageType>");
        int messageTypeEndIndex = msg_text.indexOf("</MessageType>");
        if (messageTypeBeginIndex > -1 && messageTypeEndIndex > -1 && messageTypeEndIndex > messageTypeBeginIndex) {
            messageType = msg_text.substring(messageTypeBeginIndex + "<MessageType>".length(), messageTypeEndIndex);
        }
        // W Zhu 2/20/08 WEUH121436595 END
        try {

            String messageId = "";

            int messageIdBeginIndex;
            int messageIdEndIndex;

            messageIdBeginIndex = msg_text.indexOf("<MessageID>");
            messageIdEndIndex = msg_text.indexOf("</MessageID>");
            if (messageIdBeginIndex > -1 && messageIdEndIndex > -1 && messageIdEndIndex > messageIdBeginIndex) {
                messageId = msg_text.substring(messageIdBeginIndex + "<MessageID>".length(), messageIdEndIndex);
            }
            int timeStampBeginIndex;
            int timeStampEndIndex;
            String timeStamp = null;
            timeStampBeginIndex = msg_text.indexOf("<Timestamp>");
            timeStampEndIndex = msg_text.indexOf("</Timestamp>");
            if (timeStampBeginIndex > -1 && timeStampEndIndex > -1 && timeStampEndIndex > timeStampBeginIndex) {
                timeStamp = msg_text.substring(timeStampBeginIndex + "<Timestamp>".length(), timeStampEndIndex);
                if (StringFunction.isNotBlank(timeStamp)) {
                    try {
                        int dateBeginIndex;
                        int dateEndIndex;
                        String date = null;
                        dateBeginIndex = timeStamp.indexOf("<Date>");
                        dateEndIndex = timeStamp.indexOf("</Date>");
                        if (dateBeginIndex > -1 && dateEndIndex > -1 && dateEndIndex > dateBeginIndex) {
                            date = timeStamp.substring(dateBeginIndex + "<Date>".length(), dateEndIndex);
                        }
                        int timeBeginIndex;
                        int timeEndIndex;
                        String time = null;
                        timeBeginIndex = timeStamp.indexOf("<Time>");
                        timeEndIndex = timeStamp.indexOf("</Time>");
                        if (timeBeginIndex > -1 && timeEndIndex > -1 && timeEndIndex > timeBeginIndex) {
                            time = timeStamp.substring(timeBeginIndex + "<Time>".length(), timeEndIndex);
                        }
                        if (time == null | date == null) {
                            timeStamp = null;
                        } else {
                            timeStamp = date + " " + time;
                            timeStamp = DateTimeUtility.formatDate(TPDateTimeUtility.convertDateStringToDate(timeStamp, "yyyyMMdd HHmmss"), "dd-MMM-yyyy HH:mm:ss");
                        }
                    } catch (Exception e) {
                        timeStamp = null;
                    }
                }
            }
            if (timeStamp == null | messageId == null) {
                saveSuccess = -1;
            } else {

                String sb = "UPDATE OUTGOING_Q_HISTORY set XML_SENT_TIME =to_date(?,'DD-MON-YYYY HH24:MI:SS') WHERE MESSAGE_ID=?";
                try {
                    saveSuccess = DatabaseQueryBean.executeUpdate(sb, false, new Object[]{timeStamp, messageId});
                } catch (Exception e) {
                    saveSuccess = -1;
                }
                if (saveSuccess < 1){
                	 String sb1 = "UPDATE OUTGOING_QUEUE set XML_SENT_TIME =to_date(?,'DD-MON-YYYY HH24:MI:SS') WHERE MESSAGE_ID=?";
                     try {
                         saveSuccess = DatabaseQueryBean.executeUpdate(sb1, false, new Object[]{timeStamp, messageId});
                     } catch (Exception e) {
                         saveSuccess = -1;
                     }	
                }
            }

            if (saveSuccess < 0) {
                logError("Error is occured While updating xmlSentTime, msg_text, messageId and msg_type to outgoing_q_history table");
                return false;
            } else if (saveSuccess == 0) {
                return false;
            } else {
                logInfo("Updated message messageId=" + messageId + ", msg_type=" + messageType);
            }

        } catch (Exception e) {
            logError("While updating xmlSentTime, msg_text, messageId and msg_type to outgoing_q_history table", e);
            e.printStackTrace();
            return false;
        } finally {
            //Should I check error section or what? Ask Ken.
        }
        return true;
    }

    @Override
    protected  boolean connectToMQ() {
     return true;	
    }
}
