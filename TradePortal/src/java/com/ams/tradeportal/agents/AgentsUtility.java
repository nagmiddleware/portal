package com.ams.tradeportal.agents;


import java.util.List;

import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.StringFunction;

public class AgentsUtility {
//private static final Logger LOG = LoggerFactory.getLogger(AgentsUtility.class);

	   /**
	    * This method is added here for payback message since the XML generated has too many nodes when 
	    * 1000 + beneficiaries are uploaded and all of them error out. This causes stackoverflow issues 
	    * when this huge xml is passed over RMI from Weblogic to Agent.
	    * @param inputDoc
	    * @param outputDoc
	    * @return
	 * @throws AmsException 
	    */
	    public static StringBuilder addPaybackErrors(DocumentHandler inputDoc, DocumentHandler outputDoc) throws AmsException {

	    	
	    	String paymentFileUploadOid = "";
	    	int paymentUploadErrorLogTotal = 0;
	    	StringBuilder outputSB = new StringBuilder();
	    	String sql = "Select payment_file_upload_oid from payment_file_upload where message_id = ?" ;
	    	DocumentHandler resultSet = DatabaseQueryBean.getXmlResultSet(sql, false , new Object[] {inputDoc.getAttribute("/reply_to_message_id")});
			
	    	if ( resultSet != null) {
				paymentFileUploadOid = resultSet.getAttribute("/ResultSetRecord/PAYMENT_FILE_UPLOAD_OID");
				
				//CR-1001 Rel9.4 Adding Reporting Code Errors
				sql = "select payment_upload_error_log_oid, beneficiary_info, error_msg, line_number, reporting_code_error_ind from PAYMENT_UPLOAD_ERROR_LOG where P_PAYMENT_FILE_UPLOAD_OID = ? order by line_number asc";
				
				resultSet = DatabaseQueryBean.getXmlResultSet(sql,   false,  new Object[]{paymentFileUploadOid});
				if (resultSet != null){
	    			List<DocumentHandler> resultSetList = resultSet.getFragmentsList("/ResultSetRecord");
	            	paymentUploadErrorLogTotal = resultSetList.size();

	            	StringBuilder sbBene = new StringBuilder("");
	            	StringBuilder sbRep = new StringBuilder("");
                                    for (DocumentHandler benDoc : resultSetList) {
                                    	String reportingErrInd = benDoc.getAttribute("/REPORTING_CODE_ERROR_IND");
                                    	String bene_info = benDoc.getAttribute("/BENEFICIARY_INFO");
                                        String error_msg = benDoc.getAttribute("/ERROR_MSG");
                                        error_msg = error_msg.replaceAll("- ", "");
                                        error_msg = error_msg.replaceAll("\'", "");
                                        String[] bene_details = bene_info.split(", ");
                                        if(StringFunction.isNotBlank(reportingErrInd) && TradePortalConstants.INDICATOR_NO.equals(reportingErrInd)){
	                                        sbBene.append("<").append("BeneficiaryError").append(">");
	                                        sbBene.append("<").append("LineNo").append(">");
	                                        sbBene.append(benDoc.getAttribute("/LINE_NUMBER"));
	                                        sbBene.append("</").append("LineNo").append(">");
	                                        sbBene.append("<").append("CustomerRef").append(">");
	                                        sbBene.append(bene_details[1]);
	                                        sbBene.append("</").append("CustomerRef").append(">");
	                                        if(bene_details.length == 3) {
	                                            sbBene.append("<").append("Amount").append(">");
	                                            sbBene.append(bene_details[2]);
	                                            sbBene.append("</").append("Amount").append(">");
	                                        }else {
	                                            sbBene.append("<").append("Amount").append(">");
	                                            sbBene.append("");
	                                            sbBene.append("</").append("Amount").append(">");
	                                        }
	                                        sbBene.append("<").append("BeneficiaryErrorText").append(">");
	                                        sbBene.append(error_msg);
	                                        sbBene.append("</").append("BeneficiaryErrorText").append(">");
	                                        sbBene.append("</").append("BeneficiaryError").append(">");
                                    	}//Rel 9.4 CR-1001. Package Reporting code errors if present and check for the condition if the errors can be packaged
                                        else if(StringFunction.isNotBlank(reportingErrInd) && TradePortalConstants.INDICATOR_YES.equals(reportingErrInd)
                                    			&& !TradePortalConstants.INDICATOR_NO.equals(outputDoc.getAttribute("/Out/Param/PackageReportingCodeErrors"))){
	                                        sbRep.append("<").append("ReportingCodeError").append(">");
	                                        sbRep.append("<").append("LineNo").append(">");
	                                        sbRep.append(benDoc.getAttribute("/LINE_NUMBER"));
	                                        sbRep.append("</").append("LineNo").append(">");
	                                        sbRep.append("<").append("CustomerRef").append(">");
	                                        sbRep.append(bene_details[1]);
	                                        sbRep.append("</").append("CustomerRef").append(">");
	                                        if(bene_details.length == 3) {
	                                            sbRep.append("<").append("Amount").append(">");
	                                            sbRep.append(bene_details[2]);
	                                            sbRep.append("</").append("Amount").append(">");
	                                        }else {
	                                            sbRep.append("<").append("Amount").append(">");
	                                            sbRep.append("");
	                                            sbRep.append("</").append("Amount").append(">");
	                                        }
	                                        sbRep.append("<").append("ErrorText").append(">");
	                                        sbRep.append(error_msg);
	                                        sbRep.append("</").append("ErrorText").append(">");
	                                        sbRep.append("</").append("ReportingCodeError").append(">");
                                    	}
                                    }
	                if(paymentUploadErrorLogTotal>0 && sbBene.length()>0) {
	                	outputSB.append("<BeneficiaryErrors>");
	                	outputSB.append(sbBene);
	                	outputSB.append("</BeneficiaryErrors>");
	                }
	                if(paymentUploadErrorLogTotal>0 && sbRep.length()>0) {
	                	outputSB.append("<ReportingCodeErrors>");
	                	outputSB.append(sbRep);
	                	outputSB.append("</ReportingCodeErrors>");
	                }
				}
			}

	    	return outputSB;
	}
	   
	    /**This method is added for CR-1028, but can be treated as a utility method. 
	     * Purpose of this method is to fetch parameter values from 'process_parameters' that have been set in the Outgoing_queue
	     * 
	     * @param parmName
	     * @param processingParms
	     * @return parmValue
	     */
	    
	    public static String getParm(String parmName, String processingParms) {

			int locationOfParmName, startOfParmValue, endOfParmValue;
			String parmValue;

			locationOfParmName = processingParms.indexOf(parmName);

			// Search for the whole word only.
			// For example, getParm("invoice_oid=0|insvoice_staus=Authorised",
			// "insvoice_staus" should return 1.
			// Keep searching if the character before or after the occurrence is not
			// a word delimiter

			while ((locationOfParmName > 0
					&& processingParms.charAt(locationOfParmName - 1) != ' ' && processingParms
					.charAt(locationOfParmName - 1) != '|')
					|| (locationOfParmName >= 0
							&& locationOfParmName < processingParms.length()
									- parmName.length()
							&& processingParms.charAt(locationOfParmName
									+ parmName.length()) != ' ' && processingParms
							.charAt(locationOfParmName + parmName.length()) != '=')) {

				locationOfParmName = processingParms.indexOf(parmName,
						locationOfParmName + parmName.length());

			}

			if (locationOfParmName == -1) {
				return "";
			}

			startOfParmValue = processingParms.indexOf("=", locationOfParmName);

			if (startOfParmValue == -1) {
				return "";
			}

			startOfParmValue++;

			endOfParmValue = processingParms.indexOf("|", startOfParmValue);

			if (endOfParmValue == -1) {
				endOfParmValue = processingParms.length();
			}

			parmValue = processingParms.substring(startOfParmValue, endOfParmValue);

			return parmValue;
		}



}
