package com.ams.tradeportal.agents;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.*;

import com.ams.tradeportal.busobj.Account;
import com.ams.tradeportal.busobj.ClientBank;
import com.ams.tradeportal.busobj.CorporateOrganization;
import com.ams.tradeportal.busobj.OperationalBankOrganization;
import com.ams.tradeportal.common.Debug;
import com.ams.tradeportal.common.MessageType;
import com.ams.tradeportal.common.TradePortalConstants;
import com.ams.tradeportal.common.UniversalMessage;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.StringFunction;
import org.w3c.dom.Document;

/**
 * This class is responsible for packaging and unpackaging account transaction query request.
 *
 */
public class AcctTransactionQuery extends BankQuery{
private static final Logger LOG = LoggerFactory.getLogger(AcctTransactionQuery.class);

	protected static final String AGENT_ID      = "trnInq";
	protected static final String MAX_REC       = "300";
	protected static final String MESSGE_TYPE   = MessageType.ACCT_TRANSACTION_REQUEST;
	static protected final String REQUEST_DTD   = getDTD(DTD_PATH+"acctTransactionOutbound.dtd");
	static protected final String RESPONSE_DTD  = getDTD(DTD_PATH+"acctTransactionInbound.dtd");


	/**
	 * Contructor
	 */

	public AcctTransactionQuery() {
		this(MESSGE_TYPE,AGENT_ID,REQUEST_DTD,RESPONSE_DTD);
	}
	
	private AcctTransactionQuery(String messageType, String agent, String requestDTD, String responseDTD) {
		super (messageType, agent,requestDTD,responseDTD);
	}
	
	/**
	 * This method querys account transaction and populates the accounts with response data. It takes the
	 * user_oid, ClientBank, CorporateOrganization and AccountWebBean as input.
	 *
	 * @param String user_oid
	 * @param ClientBank clientBank
	 * @param CorporateOrganization corpOrg
	 * @param AccountWebBean account
	 * @return DocumentHandler 
	 * @throws AmsException 
	 * @throws RemoteException 
	 *
	 */
	public DocumentHandler queryTransaction(String user_oid, ClientBank clientBank, CorporateOrganization corpOrg, Account account) throws RemoteException, AmsException {
		
		return (DocumentHandler)query(user_oid, null, clientBank, corpOrg, account);
		
	}

	/**
	 * This method sends account transaction query request. It returns CorrelationId. It takes the
	 * user_oid, ClientBank, CorporateOrganization and AccountWebBean as input.
	 *
	 * @param String user_oid
	 * @param ClientBank clientBank
	 * @param CorporateOrganization corpOrg
	 * @param AccountWebBean account
	 * @return byte[] CorrelationId
	 * @throws AmsException 
	 * @throws RemoteException 
	 *
	 */
	public byte[] queryTransactionAsync(String user_oid, ClientBank clientBank, CorporateOrganization corpOrg, Account account) throws RemoteException, AmsException {
		
		return queryAsync( user_oid, null, clientBank, corpOrg, account);
	}

	/**
	 * This method retrieve the account transaction query response from MQ queue using given correlationID.
	 *
	 * @param byte[] correlationID
	 * @return DocumentHandler
	 * @throws AmsException 
	 * @throws RemoteException 
	 *
	 */
	public DocumentHandler retrieveReponse(byte[] correlationID, Account account) throws RemoteException, AmsException {
		
		return (DocumentHandler)super.retrieveReponse(correlationID, account);
	}

	/**
	 * This method packages the subheader section of request xml
	 * @throws AmsException 
	 * @throws RemoteException 
	 *
	 */
        @Override
	protected void packageSubHeader(String user_oid, CorporateOrganization corpOrg, DocumentHandler outputDoc, Object object) throws RemoteException, AmsException
	{
		super.packageSubHeader(user_oid, corpOrg, outputDoc, object);
		Account account = (Account) object; 
		setAttribute(outputDoc,UniversalMessage.subHeaderPath+"/AcctId", account.getAttribute("account_number"));
		setAttribute(outputDoc,UniversalMessage.subHeaderPath+"/CurCode", account.getAttribute("currency")); 
	}
	
	/**
	 * This method packages the body section of request xml
	 * @throws AmsException 
	 * @throws RemoteException 
	 *
	 */
        @Override
	protected void packageBody(String messageID, CorporateOrganization corpOrg, OperationalBankOrganization opBankOrg, DocumentHandler outputDoc, Object object) throws RemoteException, AmsException

	{

		Account account = (Account) object; 
	    String currentDate = getCurrentDateTime(corpOrg);
	    String sDate = currentDate.substring(0,10) + "T23:59:00.000" + currentDate.substring(23) ;
	    Date startDate = null;
	    
	    try{
	    	SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZZZ");
            startDate = formatter.parse(sDate);
            GregorianCalendar cal = (GregorianCalendar) GregorianCalendar.getInstance();
            cal.setTime(startDate);
            //Vshah IR#MLUK030254165 - BEGIN - Commented below line and Add a new line beneath it.
            cal.add(GregorianCalendar.DAY_OF_MONTH, -1); 
            //Vshah IR#MLUK030254165 - END 	
            sDate = formatter.format(cal.getTime());
            
        }catch(Exception e){
        	LOG.error("Error occured getting start date", e);
        }

		DocumentHandler doc = null;
		doc = new DocumentHandler();
		doc.setAttribute("/AcctTrnInqRq/RqUID", messageID + "-1");
		doc.setAttribute("/AcctTrnInqRq/MsgRqHdr/SignonRq/ClientDt",currentDate);
		setAttribute(doc,"/AcctTrnInqRq/MsgRqHdr/SignonRq/ClientApp/Org",opBankOrg.getAttribute("Proponix_id"));  
		doc.setAttribute("/AcctTrnInqRq/MsgRqHdr/SignonRq/ClientApp/Name",TradePortalConstants.PORTAL);
		doc.setAttribute("/AcctTrnInqRq/MsgRqHdr/SignonRq/ClientApp/Version",TradePortalConstants.APP_VERSION);
		
		doc.setAttribute("/AcctTrnInqRq/RecCtrlIn/MaxRec",MAX_REC);

		setAttribute(doc,"/AcctTrnInqRq/AcctTrnSel/AcctRef/AcctRec/AcctId",account.getAttribute("account_number"));
		setAttribute(doc,"/AcctTrnInqRq/AcctTrnSel/AcctRef/AcctRec/AcctInfo/CurCode",account.getAttribute("currency"));
		setAttribute(doc,"/AcctTrnInqRq/AcctTrnSel/AcctRef/AcctRec/AcctInfo/FIIdent/BranchIdent",account.getAttribute("source_system_branch"));
		setAttribute(doc,"/AcctTrnInqRq/AcctTrnSel/AcctRef/AcctRec/AcctInfo/FIIdent/Country",account.getAttribute("bank_country_code"));
		
		setAttribute(doc,"/AcctTrnInqRq/AcctTrnSel/SelRangeDt/StartDt",sDate);
		setAttribute(doc,"/AcctTrnInqRq/AcctTrnSel/SelRangeDt/EndDt",currentDate);
		
		outputDoc.addComponent(UniversalMessage.acctInqRqPath, doc);


	}


	/**
	 * This method unpackages the Account transaction query response 
	 * Those accounts that failed the query is returned in map.
	 *
	 */
        @Override
	protected Object unPackageResponse(DocumentHandler doc, Object object) {
		DocumentHandler respDoc = null, recDoc=null;

		String curCode = doc.getAttribute("/SubHeader/CurCode");
		DocumentHandler acctRs = doc.getFragment("/Body/AcctTrnInqRs");
		
		if (acctRs != null) {
			String statusCode = acctRs.getAttribute("/Status/StatusCode");
			respDoc = new DocumentHandler();
			if (StringFunction.isBlank(statusCode) || SUCCESS_STATUS.equals(statusCode)) {
				List<DocumentHandler> rsList = acctRs.getFragmentsList("/AcctTrnRec");
				int i=0;
				for (DocumentHandler tranRecNode : rsList ) {
                    //RKAZI 07/26/2012 Rel 8.1 CQ-T36000003623 Added check to limit the no. of rows to 300 - START
                    if (i == TradePortalConstants.RECENT_TRANSACTION_MAX_ROWS){
                        break;
                    }
                    //RKAZI 07/26/2012 Rel 8.1 CQ-T36000003623 Added check to limit the no. of rows to 300 - END
			        recDoc = new DocumentHandler();
					recDoc.setAttribute("/ResultSetRecord/ACCTTRNID",tranRecNode.getAttribute("/AcctTrnId"));
					recDoc.setAttribute("/ResultSetRecord/DATEPOSTED",tranRecNode.getAttribute("/AcctTrnEnvr/PostedDt"));
					recDoc.setAttribute("/ResultSetRecord/DESCRIPTION",tranRecNode.getAttribute("/AcctTrnEnvr/Memo"));
					recDoc.setAttribute("/ResultSetRecord/RUNNINGBAL",tranRecNode.getAttribute("/AcctTrnEnvr/StmtRunningBal/Amt"));
					recDoc.setAttribute("/ResultSetRecord/AMOUNT",tranRecNode.getAttribute("/AcctTrnEnvr/CurAmt/Amt"));
					recDoc.setAttribute("/ResultSetRecord/CURRENCYCODE",curCode);
					if(null!=recDoc.getDocumentNode("/ResultSetRecord")){
						recDoc.getDocumentNode("/ResultSetRecord").setIdName(""+i++);
						respDoc.addComponent("/", recDoc);
					}	
				}
			}
			else {
				LOG.warn(" error returned for Transaction query - statusCode: {}" , statusCode );
				respDoc.addComponent("/", acctRs.getFragment("/Status"));
			}
		}

		return respDoc;
	}
}
