package com.ams.tradeportal.agents;


import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;
import java.rmi.*;
import javax.ejb.*;

import com.amsinc.ecsg.frame.*;
import com.ams.tradeportal.busobj.*;
import com.ams.tradeportal.common.XMLUtility;

/*
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public abstract class TradePortalAgent implements Runnable {
//private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(TradePortalAgent.class);

   // Collection of confirm statuses:
   protected static final String CONFIRM_STATUS_PENDING     = "P";
   protected static final String CONFIRM_STATUS_COMPLETED   = "C";
   protected static final String CONFIRM_STATUS_ERROR       = "E";

   // Collection of statuses:
   public static final String STATUS_START          = "S";
   public static final String STATUS_RECEIVED       = "R";
   public static final String STATUS_INITIALIZED    = "I";
   public static final String STATUS_PACKAGED       = "P";
   public static final String STATUS_UNPACKAGED     = "U";
   public static final String STATUS_COMPLETED      = "C";
   public static final String STATUS_ERROR          = "E";
   public static final String STATUS_RESUBMIT       = "T";
   public static final String STATUS_MESSAGE_ROUTED = "M";         // NSX - CR-542 03/16/10

   // Collection of processing statuses:
   protected static final String PROCESSING_STATUS_FAIL = "MSGFAIL";
   protected static final String PROCESSING_STATUS_OKAY = "MSGOKAY";

   // Collection of destination id's:
   protected static final String DESTINATION_ID_PROPONIX   = "PRO";
   protected static final String DESTINATION_ID_ANZ        = "ANZ";
   protected static final String DESTINATION_ID_OTL        = "OTL";
   protected static final String DESTINATION_ID_TPL        = "TPL";

   // Collection of sender id's:
   protected static final String SENDER_ID_PROPONIX        = "PRO";
   protected static final String SENDER_ID_ANZ             = "ANZ";
   protected static final String SENDER_ID_OTL             = "OTL";
   protected static final String SENDER_ID_TPL             = "TPL";

   // This PropertyResourceBundle contains all configuration parameters from AgentConfiguration.properties file
   protected static final PropertyResourceBundle properties =
        (PropertyResourceBundle)ResourceBundle.getBundle("AgentConfiguration");

   protected int    TIMER_INTERVAL        = Integer.parseInt(properties.getString("agentTimerInterval"));
   protected static final String LOCALE_NAME           = properties.getString("agentLocaleName");
   protected static final String IS_INFO_LOGGING_ON    = properties.getString("isInfoLoggingOn");
   protected static final String IS_ERROR_LOGGING_ON   = properties.getString("isErrorLoggingOn");
   protected static final String IS_DTD_VALIDATION_ON  = properties.getString("isDTDValidationOn");
   protected static final String DTD_PATH              = properties.getString("dtdPath");
   protected static final String loggerCategory        = properties.getString("loggerServerName");

   protected static final Logger logger            = Logger.getLogger();

   protected static final String CONFIRMATION_DTD         = XMLUtility.readFromFile(DTD_PATH + "confirmation.dtd");
   protected static final String TPL_INBOUND_DTD          = XMLUtility.readFromFile(DTD_PATH + "tplInbound.dtd");
   protected static final String TPL_OUTBOUND_DTD         = XMLUtility.readFromFile(DTD_PATH + "tplOutbound.dtd");
   protected static final String PURGE_DTD                = XMLUtility.readFromFile(DTD_PATH + "purge.dtd");
   protected static final String TPLH_INBOUND_DTD         = TPL_INBOUND_DTD;//Leelavathi PPX-208 29/4/2011 added
   protected static final String HISTORY_DTD              = TPL_INBOUND_DTD;
   protected static final String MAIL_INBOUND_DTD         = XMLUtility.readFromFile(DTD_PATH + "mailInbound.dtd");
   protected static final String MAIL_OUTBOUND_DTD        = XMLUtility.readFromFile(DTD_PATH + "mailOutbound.dtd");
   protected static final String INSSTAT_INBOUND_DTD      = XMLUtility.readFromFile(DTD_PATH + "insStatInbound.dtd");
   protected static final String INSSTATH_INBOUND_DTD     = INSSTAT_INBOUND_DTD;//Leelavathi PPX-208 29/4/2011 added
   protected static final String INSSTATR_INBOUND_DTD     = INSSTAT_INBOUND_DTD;//Leelavathi PPX-208 29/4/2011 added
   protected static final String WORKITEM_INBOUND_DTD     = XMLUtility.readFromFile(DTD_PATH + "workItemInbound.dtd");
   protected static final String GLOBALTRADE_DTD          = XMLUtility.readFromFile(DTD_PATH + "globalTradeOutbound.dtd");
   protected static final String REFDATA_INBOUND_DTD      = XMLUtility.readFromFile(DTD_PATH + "refDataInbound.dtd");
   protected static final String PAYINV_DTD               = XMLUtility.readFromFile(DTD_PATH + "payInvOtInbound.dtd");
   protected static final String RPMRULE_DTD              = XMLUtility.readFromFile(DTD_PATH + "RPMRule.dtd");
   protected static final String INVSTI_DTD               = XMLUtility.readFromFile(DTD_PATH + "invStat.dtd"); //rbhaduri - 24th Nov 08 - CR-434
   protected static final String UPLOAD_INV_DTD           = XMLUtility.readFromFile(DTD_PATH + "uploadInv.dtd"); //ShilpaR CR710 Rel 8.0
   protected static final String EODTRNIN_DTD             = XMLUtility.readFromFile(DTD_PATH + "eodtransactions.dtd"); //peter ng - 26th Nov 08 - CR-451
   protected static final String ARINV_DTD                = XMLUtility.readFromFile(DTD_PATH + "arInv.dtd"); //PRAMEY - 01st Dec 08 - CR-434
   protected static final String INVSTO_DTD               = INVSTI_DTD;  //XMLUtility.readFromFile(DTD_PATH + "invStat.dtd"); //Vshah 12/11/2008 INVSTO UnPackager
   protected static final String FXRATE_INBOUND_DTD       = XMLUtility.readFromFile(DTD_PATH + "FXRateInbound.dtd"); //  CR-469 NShrestha 05/18/2009
   protected static final String DIRDEB_DTD               = XMLUtility.readFromFile(DTD_PATH + "DirectDebit.dtd");
   protected static final String PAYSTAT_DTD              = XMLUtility.readFromFile(DTD_PATH + "PayStateInbound.dtd");   //  CR-596 Narayan 11/22/2010
   protected static final String PAYBACK_DTD              = XMLUtility.readFromFile(DTD_PATH + "payback.dtd");   //  CR-593 CJR 04/06/2011
   protected static final String PAYCOMP_DTD              = XMLUtility.readFromFile(DTD_PATH + "paycomp.dtd");   //  CR-593 CJR 04/06/2011
   protected static final String PAYFILE_DTD              = XMLUtility.readFromFile(DTD_PATH + "payfile.dtd");     // CR-593 NSX 04/04/11  Rel. 7.0.0
   protected static final String GENERIC_INCOMING_DTD     = XMLUtility.readFromFile(DTD_PATH + "genericIncomingDataMsg.dtd");     // CR-587 manohar 17/06/11  Rel. 7.1.0
   protected static final String INTEREST_RATE_DTD        = XMLUtility.readFromFile(DTD_PATH + "intDiscountRate.dtd");     // Srini_D CR-713 Rel 8.0 10/18/2011
   protected static final String INV_TRIGGER_DTD          = XMLUtility.readFromFile(DTD_PATH + "invtrigger.dtd");     // CR-710 NAR 03/08/2012  Rel. 8.0.0.0
   protected static final String POR_DTD                  = XMLUtility.readFromFile(DTD_PATH + "POR.dtd");     // ShilpaR CR-707 Rel 8.0
   protected static final String CUSREF_DTD               = XMLUtility.readFromFile(DTD_PATH + "CUSREF.dtd"); //IValavala CR-741 Rel 8.2
   protected static final String PINCNAPP_DTD             = XMLUtility.readFromFile(DTD_PATH + "pincnapp.dtd"); //CR1006
   
   /*   Class variables */
   public        String  AGENT_ID = "";
   protected     Thread  myThread;

   protected boolean isOrphan                   = true;
   protected boolean appServerConnectionProblem = false;

   /* The location of the EJB Application Server that will be used for processing the interface events. */
   protected String        ejbServerLocation =
        "t3://" + properties.getString("ejbServerIPAddress") + ":" + properties.getString("ejbServerPort");

   /* The IBM MQ Series Queue Manager */
   protected    MQWrapper  mqWrapper;

   protected IncomingInterfaceQueue   incoming_queue;
   protected OutgoingInterfaceQueue   outgoing_queue;
   protected QueryListView            query;
   protected ServerDeploy             serverDeploy;

// CR-451 TRudden 01/28/2009 Begin -- Changes to skip sleep period when more work exists
   /* Agent Execution Status of SUCCESS means the agent processed a record, and there are more records to process in the queue.  */
   protected static final int AGENT_SUCCESS   = 1;
   /* Agent Execution Status of NO_RECORDS_TO_PROCESS means the agent was executed but did not have a record to process. */
   protected static final int AGENT_NO_RECORDS_TO_PROCESS = 0;
   /* Agent Execution Status of FAILURE means the agent attempted to process a record, but did not complete the processing. */
   protected static final int AGENT_FAILURE   = -1;
// CR-451 TRudden 01/28/2009 End
   //////////////////////////////////////////////////////////////////////////////////////////

   /*
    * Construct the Trade Portal Agent.
    */
    public TradePortalAgent ()
    {
   	//ivalavala IR POUM080661913 setAgentID for routing to proper log file
        
        connectToServers();
        isOrphan = true;
        createQueryBean();

        //AAlubala - IR#KJUM082054781 Rel8.2 - 10/05/2012 [BEGIN]
        //Add debugging to identify why sometimes agents take long to start
        //When loading refdata, each agent id will be appended to the log output
        //It will be easier to see which output corresponds to which agent
        //since there are multiple instances running
        //Currently, it is not possible to pin down which output message corresponds to which
        //agent (thread) so, it is not easy to know when a particular agent is stuck
        //and on what action
        long agentId = Thread.currentThread().getId();

        logInfo(agentId+ ": Loading refdata ...");

        //BSL IR NLUM031655810 03/20/2012 Rel8.0 BEGIN
        // Must initialize ref data mgr with the server location.  If default
        // constructor is used the data will not be read on the AgentServer.
        ReferenceDataManager.getRefDataMgr(ejbServerLocation);
        //BSL IR NLUM031655810 03/20/2012 Rel8.0 END

        logInfo(agentId+ ": Loading refdata done.");


    }

    /**
     * Encapsulates creation of the query list view used by the agents.
     */
    private boolean createQueryBean() {
        try {
            query = (QueryListView) EJBObjectFactory.createClientEJB(ejbServerLocation, "QueryListView");
        } catch (Exception e) {
            logError("Instantiation of QueryListView failed - app server is probably down");
            return false;
        }
        return true;
    }


    /**
     * connectToServers()
     *
     * This method is only called in the constructor.  The functionality contain within has
     * been removed from the default constructor, so that it can be overridden by subclasses.
     * Because default constructors automatically call super(), we couldn't effectively
     * override the default constructor.
     */
    protected final void connectToServers() {
        connectToLoggerServer(); // W Zhu 4/20/12 Sys Test troubleshooting Connect to Logger Server first.
        connectToEJBServer();
        if (connectToMQ()) {
        	mqWrapper = new MQWrapper();
            mqWrapper.connectToMQ();
            mqWrapper.setAgentID(AGENT_ID);
        }
    }

    protected abstract boolean connectToMQ();

	public void setAgentId(String agentId) {
        this.AGENT_ID = agentId;
        if (connectToMQ()) {
            mqWrapper.setAgentID(agentId);  // W Zhu 2/20/08 WEUH121436595
        }
    }

    public void setMyThread(Thread myThread) {
        this.myThread = myThread;
    }

   @Override
    public void run(){

        //AAlubala - IR#KJUM082054781 Rel8.2 - 10/05/2012 [BEGIN]
        //Add debugging to identify why sometimes agents take long to start
        //When loading refdata, each agent id will be appended to the log output
        //It will be easier to see which output corresponds to which agent
        //since there are multiple instances running
        //Currently, it is not possible to pin down which output message corresponds to which
        //agent (thread) so, it is not easy to know when a particular agent is stuck
        //and on what action
    	long agentId = Thread.currentThread().getId();

        System.out.println("Thread Started");
        logInfo("Agent ["+ agentId +"] started ... " );
        
        //IR#KJUM082054781 - [END]

        if (Thread.currentThread ().isInterrupted()){
             if (connectToMQ()) mqWrapper.disconnectFromMQ();
             removeEJBs();
             return;
        }

        for (;;){
            if(appServerConnectionProblem)
             {
                // Refresh ServerDeploy
                connectToEJBServer();

                if (createQueryBean())
                 {
                   DataSourceFactory.refreshCachedValues();
                   appServerConnectionProblem=false;
                   System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\nAgent is now connected to app server");
                 }
             }
         // CR-451 TRudden 01/28/2009 Begin -- Changes to skip sleep period when more work exists
             int agentReturnStatus = AGENT_NO_RECORDS_TO_PROCESS;
         // CR-451 TRudden 01/28/2009 End

            if(!appServerConnectionProblem)
             {
                try
                 {
                	agentReturnStatus = lookForWorkAndDoIt();
                 }
                catch(RemoteException re)
                 {
                   re.printStackTrace();
                   appServerConnectionProblem=true;
                 }
                catch(RemoveException re)
                 {
                   appServerConnectionProblem=true;
                 }
             }

            if (Thread.currentThread ().isInterrupted()){
                if (connectToMQ()) mqWrapper.disconnectFromMQ();
                removeEJBs();
                return;
            }
            else{
                try{
     // CR-451 TRudden 01/28/2009 Begin -- Changes to skip sleep period when more work exists
                	/** When the agent returns SUCCESS, there may be more work in the queue,
                	 * which we want to process immediately.  Only sleep after errors or calls
                	 * where there was no work to process.
                	 */
                	if (agentReturnStatus != AGENT_SUCCESS) {
                		//AGENT_FAILURE, AGENT_NO_RECORDS_TO_PROCESS
                        Thread.sleep(TIMER_INTERVAL);
                	}
     // CR-451 TRudden 01/28/2009 end
                    removeIncomingInterfaceQueue();
                    removeOutgoingInterfaceQueue();
                    // do not call removeQueryListView()
                }
                catch(InterruptedException ie){
                    if (connectToMQ()) mqWrapper.disconnectFromMQ();
                    removeEJBs();
                    break;
                }
            }
        }
    }


    /*
     Search for events to be processed and process them.  The details of this method
     must be implemented in the descendent class.
     */
    protected int lookForWorkAndDoIt() throws RemoteException, RemoveException {
        return AGENT_FAILURE;
    }

    protected int connectToEJBServer() {
        try {
            serverDeploy = (ServerDeploy) EJBObjectFactory.createClientEJB(ejbServerLocation, "ServerDeploy");
            serverDeploy.init(ejbServerLocation);
        } catch (Exception e) {
            logError("Unable to Connect to EJB Application Server", e);
            e.printStackTrace();
            return -1;
        }

        logInfo("\n___________________________________");
        logInfo("Connected to EJB Application Server");
        return 1;
    }



   /**
   This method is used to connect to JPylon LoggerServer - an independent program, designed to
   facilitate logging. LoggerServer takes care of openning and closing log files, monitores log
   file size, etc. It runs as a thread, and uses sockets for reading incoming messages. Following are
   default settings.
   Constants for different levels of logging:
   DEBUG_LOG_SEVERITY = 2;
   ERROR_LOG_SEVERITY = 5;
   WARN_LOG_SEVERITY = 3;
   INFO_LOG_SEVERITY = 1;

   Initialization parameters:
   serverLocation = "localhost";
   port = 4000;
   retryCount = 0;
    */
    protected int connectToLoggerServer() {

        System.out.println("Connected to Logger Server");
        return 1;
    }

    /*   Should be called by agents, packagers and unpackagers to log errors.
     Note: Logger doesn't like new line symbols - "\n"
     */
    public int logError(String errorText, Exception e) {
        if (IS_ERROR_LOGGING_ON.equals("true")) {

            StringBuilder errorStack = new StringBuilder();
            for (StackTraceElement ste : e.getStackTrace()) {
                errorStack.append(ste).append("\r\n");
            }

            Logger.log(loggerCategory, AGENT_ID, AGENT_ID + ": ERROR: " + errorText + "- Exception Message: " + e.getMessage() + "- Stack Trace:\r\n" + errorStack.toString(), 5);
        }
        return -1;
    }

    /* Should be called by agents, packagers and unpackagers to log errors. */
    public int logError(String errorText) {
        if (IS_ERROR_LOGGING_ON.equals("true")) {
            Logger.log(loggerCategory, AGENT_ID, AGENT_ID + ": ERROR: " + errorText, 5);
        }
        return 1;
    }

    /* Should be called by agents, packagers and unpackagers to log information as needed. */
    public final int logInfo(String infoText) {
        if (IS_INFO_LOGGING_ON.equals("true")) {
            Logger.log(loggerCategory, AGENT_ID, AGENT_ID + ": INFO: " + infoText, 1);
        }
        return 1;
    }


    /* Actual implementation is left for */
    protected String determineDTDType(String msg_type) {
        return "";
    }

    protected void removeEJBs() {
        removeQueryListView();
        removeIncomingInterfaceQueue();
        removeOutgoingInterfaceQueue();
        removeServerDeploy();
    }

    protected void removeQueryListView() {
        try {
            if (query != null) {
                query.remove();
                query = null;
            }
        } catch (Exception e) {
            logError("While removing instance of QueryListView", e);
        }
    }

    protected void removeIncomingInterfaceQueue() {
        try {
            if (incoming_queue != null) {
                incoming_queue.remove();
                incoming_queue = null;
            }
        } catch (Exception e) {
            logError("While removing instance of IncomingInterfaceQueue", e);
        }
    }

    protected void removeOutgoingInterfaceQueue() {
        try {
            if (outgoing_queue != null) {
                outgoing_queue.remove();
                outgoing_queue = null;
            }
        } catch (Exception e) {
            logError("While removing instance of OutgoingInterfaceQueue", e);
        }
    }

    protected void removeServerDeploy() {
        try {
            if (serverDeploy != null) {
                serverDeploy.remove();
                serverDeploy = null;
            }
        } catch (Exception e) {
            logError("While removing instance of serverDeploy", e);
        }
    }


}// end of TradePortalAgent class