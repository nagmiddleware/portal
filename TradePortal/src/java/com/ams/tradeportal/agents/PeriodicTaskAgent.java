package com.ams.tradeportal.agents;


import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;

import javax.ejb.RemoveException;

import com.ams.tradeportal.busobj.CorporateOrganization;
import com.ams.tradeportal.busobj.PeriodicTaskHistory;
import com.ams.tradeportal.common.ConfigSettingManager;
import com.ams.tradeportal.common.Debug;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.ClientServerDataBridge;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.frame.EJBObjectFactory;
import com.amsinc.ecsg.frame.Mediator;
import com.amsinc.ecsg.util.DateTimeUtility;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.GMTUtility;
import com.amsinc.ecsg.util.JPylonProperties;
import com.amsinc.ecsg.util.StringFunction;



public class PeriodicTaskAgent extends TradePortalAgent
{
//private static final Logger LOG = LoggerFactory.getLogger(PeriodicTaskAgent.class);
	private static final SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");

	// Periods of Time
	private static long MILLIS_IN_HOUR = 1000 * 60 * 60;
	private static long MILLIS_IN_DAY = MILLIS_IN_HOUR * 24;
	private static long agingDays;
	private static long serviceResponseAgingDays;
	private static long maxGenericIncDataMessageAge;
	private static long maxEODDailyTransactionAge;
	private static long maxPOUploadDataAge;//ShilpaR CR707
	private static long ptlsvcsServiceLogAgingDays;//MEer Rel8.4 CR-934A
	private static long ptlsvcsServiceSigningLogAgingDays;
	private static Map<String,TimeWindowData> timeWindows = new HashMap<>();
	Timer timer;
	
	 


	// Use this to read date/time data out of the database using the query bean
	private static SimpleDateFormat isoDateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");

	ClientServerDataBridge csdb = new ClientServerDataBridge();

	// Overrides the timer interval to be once every two minutes
	public PeriodicTaskAgent()
	{
		// Set the timer so that this agent will look to see whether or not it is time to run
		// every two minutes
		TIMER_INTERVAL = 120000;

		// The CSDB used when calling the mediator.  No need to re-create it each time
		csdb.setLocaleName("en_US");

		// Set the default timezone to UTC (GMT)
		// This is so that date objects will be expressed in GMT
		TimeZone.setDefault(TimeZone.getTimeZone("UTC"));

		// Get the number of days history records stay in the table (for history purge)
		// Default to 90 if no setting is present
		try
		{
			agingDays = Long.parseLong(properties.getString("maxHistoryAge"));
		}
		catch(Exception e)
		{
			agingDays = 90;
		}

		// Get the number of days service response history records stay in the table (for history purge)
		try
		{
			serviceResponseAgingDays = Long.parseLong(properties.getString("serviceResponseMaxHistoryAge"));
		}
		catch(Exception e)
		{
			serviceResponseAgingDays = 7; // Default
		}

		try // CR 587 - archive Generic Inc Data message. Default period is 180 days
		{
			maxGenericIncDataMessageAge = Long.parseLong(properties.getString("maxGenericIncDataMessageAge"));
		}
		catch(Exception e)
		{
			maxGenericIncDataMessageAge = 180; // default
		}

		try 
		{
			maxEODDailyTransactionAge = Long.parseLong(properties.getString("maxEODDailyTransactionAge"));
		}
		catch(Exception e)
		{
			maxEODDailyTransactionAge = 360; // default
		}
		try //ShilpaR CR707 Rel8.0
		{
			maxPOUploadDataAge = Long.parseLong(properties.getString("maxPOUploadDataAge"));
		}
		catch(Exception e)
		{
			maxPOUploadDataAge = 90; // default
		}

 
		try
		{
			JPylonProperties jPylonProperties = JPylonProperties.getInstance();
			String configFilePath = jPylonProperties.getString("configFilePath");
			String pathToConfig = configFilePath + "/PeriodicTasks.xml";
			DocumentHandler configXml = new DocumentHandler(pathToConfig);

			List<DocumentHandler> taskList = configXml.getFragmentsList("PeriodicTask");
			// Loop through each of the SMTP servers in the list
			for (DocumentHandler taskXml : taskList) {
				String taskType = taskXml.getAttribute("TaskType");			
				String hour     = taskXml.getAttribute("../HourToRun");
				String minute   = taskXml.getAttribute("../MinuteToRun");
				String window   = taskXml.getAttribute("../WindowToRun");	
				String fixedInterval = taskXml.getAttribute("../FixedIntervalToRun");	
				TimeWindowData twd = new TimeWindowData(hour, minute, window, fixedInterval);
				timeWindows.put(taskType, twd);
				if(Integer.parseInt(fixedInterval)>0){ // Applies only to 3 new tasks added in CR-1006 Rel 9.3
					new FixedIntervalTask(taskType, Integer.parseInt(fixedInterval));
				}
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			logError("Could not read list of periodic tasks", e);
		}

	}
	
	
	/**
	 * connectToServers()
	 *
	 * This method is called by the default constructor of this object's super class
	 * IValavala 26 Nov 08. Commenting this so the ancestors function is called.
	 * we now need to connect to MQ as well for the CM_Browse_Request_queue task
	 
	 */
	/*  protected void connectToServers()
     {
	connectToEJBServer();
	connectToLoggerServer();
     }
	 */
	
	


	/**
	 * Performs the work of the agent.
	 *
	 *
	 */
	protected int lookForWorkAndDoIt() throws RemoteException
	{

		Date rightNow = null;

 		try
		{
			rightNow = serverDeploy.getServerDate(false);
 
		}
		catch (AmsException e)
		{
			logError("Could not obtain current date from server.  Cannot proceed with checking to run periodic task.", e);
			return AGENT_FAILURE;
		}

		
 		if(isItTimeToRun(TradePortalConstants.DAILY_EMAIL_TASK_TYPE, rightNow))
 			runDailyEmailMediator();

 		if(isItTimeToRun(TradePortalConstants.HISTORY_PURGE_TASK_TYPE, rightNow))
 			runHistoryPurge(rightNow);  

 		if(isItTimeToRun(TradePortalConstants.BROWSE_CM_MQQUEUE_TASK_TYPE, rightNow))
 			runCMMQBrowse(rightNow);
 		// CR-587 added to archive the Generic Inc data message
 		if(isItTimeToRun(TradePortalConstants. ARC_GEN_INC_DATA_MESSAGE_TASK_TYPE, rightNow))
 			runArchiveGenIncDataMessage (rightNow);
 		if(isItTimeToRun(TradePortalConstants.ARC_EOD_TRANSACTION_MESSAGE_TASK_TYPE, rightNow))
 			runArchiveEODTransactionDataMessage (rightNow);

 		//CR914A-R9.2
 		if(isItTimeToRun(TradePortalConstants.EOD_PURGE_CRNOTES_INVOICES,rightNow))
 			runPurgeCrNoteAndInvoices (rightNow);
		
  
		//Rel 8.3 CR 776
//		try {
			processSupplierPortalEmails(rightNow);
//		} catch (AmsException e) {
//			logError("Error in agent processing supplier portal emails. ", e);
//			e.printStackTrace();
//			return AGENT_FAILURE;
//		}


		// CR-451 TRudden 01/28/2009 Begin -- Changes to skip sleep period when more work exists
		/** Since this agent has its own mechanism for determining when to run, always return
		 * no more records to process, which ensures the thread will sleep before the next call */   
		return AGENT_NO_RECORDS_TO_PROCESS;
		// CR-451 TRudden 01/28/2009 End  





	}

	

	/**o	
	 * Call the archive method for EODTransactionDataMessage and EOD Daily Data
	 *
	 *
	 */
	private void runArchiveEODTransactionDataMessage(Date rightNow)
	{
		try{


			// Create a record in the database that this task is being run.
			// The record of when the task has run is important in the agent's determining when this task should be run

			PeriodicTaskHistory taskHistory = null;
			taskHistory = (PeriodicTaskHistory) EJBObjectFactory.createClientEJB(ejbServerLocation, "PeriodicTaskHistory");
			taskHistory.newObject();
			taskHistory.setAttribute("task_type", TradePortalConstants.ARC_EOD_TRANSACTION_MESSAGE_TASK_TYPE);
			taskHistory.setAttribute("run_timestamp", formatter.format(rightNow));
			taskHistory.save();
			taskHistory.remove();      


		}
		catch(Exception e)
		{
			e.printStackTrace();
			return;
		}

		try
		{
			long latestHistoryDate =GMTUtility.getGMTDateTime(false).getTime() - (MILLIS_IN_DAY * maxEODDailyTransactionAge);
			SimpleDateFormat formatter = new SimpleDateFormat("MM-dd-yyyy");
			String lastestHistoryDateString = formatter.format(new Date(latestHistoryDate));

			String insertStatement = "INSERT INTO EOD_DAILY_TRANSACTION_HIST (EOD_DAILY_TRANS_HIST_OID , TRANSACTION_DATE , TRANSACTION_TYPE ,TRANSACTION_REFERENCE ,"+
					"TRANSACTION_NARRATIVE , EFFECTIVE_DATE , TRACE_ID , TRANSACTION_CODE , AUXILIARY_DOM , EX_AUXILIARY_DOM , BAI_CODE , REMITTER_NAME ,"+
					"LODGMENT_REFERENCE , SHORT_DESCRIPTION , NUMBER_COLLECTION_ITEMS , P_EOD_DAILY_DATA_OID , SEQUENCE_NUMBER, DATE_TIME_RECEIVED ) " +
					"SELECT EOD_DAILY_TRANS_OID, TRANSACTION_DATE , TRANSACTION_TYPE ,TRANSACTION_REFERENCE , " +
					"TRANSACTION_NARRATIVE , EFFECTIVE_DATE , TRACE_ID , TRANSACTION_CODE , AUXILIARY_DOM , EX_AUXILIARY_DOM , BAI_CODE , REMITTER_NAME ,"+
					"LODGMENT_REFERENCE , SHORT_DESCRIPTION , NUMBER_COLLECTION_ITEMS , P_EOD_DAILY_DATA_OID , SEQUENCE_NUMBER, DATE_TIME_RECEIVED  FROM EOD_DAILY_TRANSACTION WHERE " +
					"DATE_TIME_RECEIVED < TO_DATE(?, 'MM-DD-YYYY')";


			DatabaseQueryBean.executeUpdate(insertStatement, false ,new Object[]{lastestHistoryDateString});

			String deleteStatement = "DELETE EOD_DAILY_TRANSACTION WHERE " +
					"DATE_TIME_RECEIVED < TO_DATE(?, 'MM-DD-YYYY')";

			DatabaseQueryBean.executeUpdate(deleteStatement, false,new Object[]{lastestHistoryDateString});

		}
		catch(Exception e)
		{
			e.printStackTrace();
			return;
		}


		try
		{
			long latestHistoryDate =GMTUtility.getGMTDateTime(false).getTime() - (MILLIS_IN_DAY * maxEODDailyTransactionAge);
			SimpleDateFormat formatter = new SimpleDateFormat("MM-dd-yyyy");
			String lastestHistoryDateString = formatter.format(new Date(latestHistoryDate));

			String insertStatement = "INSERT INTO EOD_DAILY_DATA_HIST (EOD_DAILY_DATA_HIST_OID  , BALANCE_DATE  , SOURCE  ,BANK_ROUTING_ID  ,"+
					"STATEMENT_NUMBER  , OPENING_BALANCE  , CLOSING_BALANCE  , TOTAL_DEBIT_MOVEMENT  , NUMBER_OF_DEBITS  , TOTAL_CREDIT_MOVEMENT  , NUMBER_OF_CREDITS  , DEBIT_INTEREST_RATE  ,"+
					"CREDIT_INTEREST_RATE  , OVERDRAFT_LIMIT  , DEBIT_INTEREST_ACCRUED  , CREDIT_INTEREST_ACCRUED  , FID_ACCRUED , BADT_ACCRUED , NEXT_POSTING_DATE , DATE_TIME_RECEIVED, P_ACCOUNT_OID  ) " +
					"SELECT EOD_DAILY_DATA_OID  , BALANCE_DATE  , SOURCE  ,BANK_ROUTING_ID  ,"+
					"STATEMENT_NUMBER  , OPENING_BALANCE  , CLOSING_BALANCE  , TOTAL_DEBIT_MOVEMENT  , NUMBER_OF_DEBITS  , TOTAL_CREDIT_MOVEMENT  , NUMBER_OF_CREDITS  , DEBIT_INTEREST_RATE  ,"+
					"CREDIT_INTEREST_RATE  , OVERDRAFT_LIMIT  , DEBIT_INTEREST_ACCRUED  , CREDIT_INTEREST_ACCRUED  , FID_ACCRUED , BADT_ACCRUED , NEXT_POSTING_DATE ,DATE_TIME_RECEIVED, P_ACCOUNT_OID   FROM EOD_DAILY_DATA WHERE " +
					"DATE_TIME_RECEIVED < TO_DATE(?, 'MM-DD-YYYY')";

			DatabaseQueryBean.executeUpdate(insertStatement, false, new Object[]{lastestHistoryDateString});

			String deleteStatement = "DELETE EOD_DAILY_DATA WHERE " +
					"DATE_TIME_RECEIVED < TO_DATE(?, 'MM-DD-YYYY')";

			DatabaseQueryBean.executeUpdate(deleteStatement, false, new Object[]{lastestHistoryDateString});

		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	/**o	
	 * Call the archive method for Generic Inc Data Message
	 *
	 *
	 */
	private void runArchiveGenIncDataMessage(Date rightNow)
	{


		try
		{
			// Add a record to the periodic task history to keep track of when this was run
			PeriodicTaskHistory taskHistory = null;
			taskHistory = (PeriodicTaskHistory) EJBObjectFactory.createClientEJB(ejbServerLocation, "PeriodicTaskHistory");
			taskHistory.newObject();
			taskHistory.setAttribute("task_type", TradePortalConstants.ARC_GEN_INC_DATA_MESSAGE_TASK_TYPE);
			taskHistory.setAttribute("run_timestamp", formatter.format(rightNow));
			taskHistory.save();
			taskHistory.remove();
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return;
		}



		try
		{
			long latestHistoryDate =GMTUtility.getGMTDateTime(false).getTime() - (MILLIS_IN_DAY * maxGenericIncDataMessageAge);
			SimpleDateFormat formatter = new SimpleDateFormat("MM-dd-yyyy");
			String lastestHistoryDateString = formatter.format(new Date(latestHistoryDate));

			String insertStatement = "INSERT INTO GENERIC_INC_DATA_HIST (GEN_INC_DATA_HIST_OID, CORPORATE_ORG_OID, DATE_TIME_SENT,MESSAGE_CATEGORY, ACCT_NUMBER,"+
					"CURRENCY, SOURCE_SYSTEM_BRANCH, CUSTOMER_ALIAS, USER_DEFINED_1, USER_DEFINED_2, USER_DEFINED_3, USER_DEFINED_4, USER_DEFINED_5,"+
					"USER_DEFINED_6, USER_DEFINED_7, USER_DEFINED_8, USER_DEFINED_9, USER_DEFINED_10, USER_DEFINED_11, USER_DEFINED_12,"+
					" USER_DEFINED_13, USER_DEFINED_14, USER_DEFINED_15, USER_DEFINED_16, USER_DEFINED_17, USER_DEFINED_18, USER_DEFINED_19, USER_DEFINED_20, "+
					"USER_DEFINED_21, USER_DEFINED_22, USER_DEFINED_23, USER_DEFINED_24,USER_DEFINED_25, USER_DEFINED_26, USER_DEFINED_27,"+
					"USER_DEFINED_CURR1,USER_DEFINED_AMOUNT1, USER_DEFINED_CURR2, USER_DEFINED_AMOUNT2, USER_DEFINED_DATE, RECEIPT_DATE_TIME) " +
					"SELECT GEN_INC_DATA_OID, CORPORATE_ORG_OID, DATE_TIME_SENT,MESSAGE_CATEGORY, ACCT_NUMBER, "+
					"CURRENCY, SOURCE_SYSTEM_BRANCH, CUSTOMER_ALIAS, USER_DEFINED_1, USER_DEFINED_2, USER_DEFINED_3, USER_DEFINED_4, USER_DEFINED_5," +
					"USER_DEFINED_6, USER_DEFINED_7, USER_DEFINED_8, USER_DEFINED_9, USER_DEFINED_10, USER_DEFINED_11, USER_DEFINED_12, "+
					" USER_DEFINED_13, USER_DEFINED_14, USER_DEFINED_15, USER_DEFINED_16, USER_DEFINED_17, USER_DEFINED_18, USER_DEFINED_19, USER_DEFINED_20, " +
					"USER_DEFINED_21, USER_DEFINED_22, USER_DEFINED_23, USER_DEFINED_24,USER_DEFINED_25, USER_DEFINED_26, USER_DEFINED_27, " +
					"USER_DEFINED_CURR1,USER_DEFINED_AMOUNT1, USER_DEFINED_CURR2, USER_DEFINED_AMOUNT2, USER_DEFINED_DATE, RECEIPT_DATE_TIME FROM GENERIC_INC_DATA WHERE " +
					"RECEIPT_DATE_TIME < TO_DATE(?, 'MM-DD-YYYY')";


			DatabaseQueryBean.executeUpdate(insertStatement, false, new Object[]{lastestHistoryDateString});

			String deleteStatement = "DELETE GENERIC_INC_DATA WHERE " +
					"RECEIPT_DATE_TIME < TO_DATE(?, 'MM-DD-YYYY')";

			DatabaseQueryBean.executeUpdate(deleteStatement, false, new Object[]{lastestHistoryDateString});

		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	/**
	 * Call the CM MQBrowse
	 *
	 *
	 */
	private void runCMMQBrowse(Date rightNow)
	{

		try
		{
			// Add a record to the periodic task history to keep track of when this was run
			PeriodicTaskHistory taskHistory = null;
			taskHistory = (PeriodicTaskHistory) EJBObjectFactory.createClientEJB(ejbServerLocation, "PeriodicTaskHistory");
			taskHistory.newObject();
			taskHistory.setAttribute("task_type", TradePortalConstants.BROWSE_CM_MQQUEUE_TASK_TYPE);
			taskHistory.setAttribute("run_timestamp", formatter.format(rightNow));
			taskHistory.save();
			taskHistory.remove();
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return;
		}
		logInfo("Starting CM MQQueue Browse for discarding expired messages");
		PropertyResourceBundle properties =
				(PropertyResourceBundle)ResourceBundle.getBundle("AgentConfiguration");
		String cMBalResponseQueue        = properties.getString("mqInputQueue.balInq");
		String cMTrnResponseQueue        = properties.getString("mqInputQueue.trnInq");
		mqWrapper.attemptToBrowseMessageOnQueue(cMBalResponseQueue);
		mqWrapper.attemptToBrowseMessageOnQueue(cMTrnResponseQueue);



	}

	/**
	 * Call the daily email mediator
	 *
	 *
	 */
	private void runDailyEmailMediator()
	{
		logInfo("Calling mediator for daily email process...");
		Mediator dailyEmailMediator = null;
		try
		{
			dailyEmailMediator = (Mediator)EJBObjectFactory.createClientEJB(ejbServerLocation, "DailyEmailMediator");
		}
		catch(Exception e)
		{
			e.printStackTrace();
			logError("While creating an instance of DailyEmailMediator:", e);
			return;
		}

		DocumentHandler inputDoc = new DocumentHandler();
		inputDoc.addComponent("/In", new DocumentHandler());

		try
		{
			// Call the mediator
			DocumentHandler outputDoc = dailyEmailMediator.performService(inputDoc, csdb);
			dailyEmailMediator.remove();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			logError("While running an instance of DailyEmailMediator:", e);
		}
	}

	/**
	 * Calls the daily purge process
	 *
	 *
	 */
	private void runHistoryPurge(Date rightNow)
	{
		try
		{
			String ptlWebServiceLogAge = ConfigSettingManager.getConfigSetting("PORTAL_AGENT", "AGENT", "Portal_WebService_Log_Age", "90");
			ptlsvcsServiceLogAgingDays =  Long.parseLong(ptlWebServiceLogAge);
			logInfo("Beginning to purge history records older than "+agingDays+" days, (" + serviceResponseAgingDays + " days for Service Response History) and ("+ptlsvcsServiceLogAgingDays+" days for Web Service Log)");

			//Srinivasu_D Rel9.1 WS 09/03/2014 - Purging for Transactions Signing tables
			String ptlWebServiceSigningLogAge = ConfigSettingManager.getConfigSetting("PORTAL_AGENT", "AGENT", "Ptlsvcs_Transaction_Signing_Age", "90");
			ptlsvcsServiceSigningLogAgingDays =  Long.parseLong(ptlWebServiceSigningLogAge);
			logInfo("Beginning to purge history records older than "+agingDays+" days, (" + serviceResponseAgingDays + " days for Service Response History) and ("+ptlsvcsServiceSigningLogAgingDays+" days for Web Service Log)");
			//Srinivasu_D Rel9.1 WS 09/03/2014 - End

			try
			{
				// Add a record to the periodic task history to keep track of when this was run
				PeriodicTaskHistory taskHistory = null;
				taskHistory = (PeriodicTaskHistory) EJBObjectFactory.createClientEJB(ejbServerLocation, "PeriodicTaskHistory");
				taskHistory.newObject();
				taskHistory.setAttribute("task_type", TradePortalConstants.HISTORY_PURGE_TASK_TYPE);
				taskHistory.setAttribute("run_timestamp", formatter.format(rightNow));
				taskHistory.save();
				taskHistory.remove();
			}
			catch(Exception e)
			{
				e.printStackTrace();
				return;
			}

			// Compute the date for which all earlier records must be deleted
			long latestHistoryDate =
					GMTUtility.getGMTDateTime(false).getTime() - (MILLIS_IN_DAY * agingDays);
			SimpleDateFormat formatter = new SimpleDateFormat("MM-dd-yyyy");
			String lastestHistoryDateString =
					formatter.format(new Date(latestHistoryDate));

			// Build and execute delete for in the incoming queue history table
			String deleteStatement = "delete from incoming_q_history" +
					" where date_received < to_date(?, 'MM-DD-YYYY')";

			DatabaseQueryBean.executeUpdate(deleteStatement, false, new Object[]{lastestHistoryDateString});

			// Build and execute delete for in the outgoing queue history table
			deleteStatement = "delete from outgoing_q_history" +
					" where date_sent < to_date(?, 'MM-DD-YYYY')";

			DatabaseQueryBean.executeUpdate(deleteStatement, false, new Object[]{lastestHistoryDateString});

			// Delete all email triggers that could not be sent after the allowed time period
			deleteStatement = "delete from email_trigger" +
					" where creation_timestamp < to_date(?, 'MM-DD-YYYY')";

			DatabaseQueryBean.executeUpdate(deleteStatement, false, new Object[]{lastestHistoryDateString});

			// Delete all periodic task history records that are older that a certain number of days
			deleteStatement = "delete from periodic_task_history" +
					" where run_timestamp < to_date(?, 'MM-DD-YYYY')";

			DatabaseQueryBean.executeUpdate(deleteStatement, false, new Object[]{lastestHistoryDateString});

			// Delete all user session history records that are older that a certain number of days
			deleteStatement = "delete from user_session_history" +
					" where logout_timestamp < to_date(?, 'MM-DD-YYYY')";

			DatabaseQueryBean.executeUpdate(deleteStatement, false, new Object[]{lastestHistoryDateString});


			//calculate lastestHistoryDateString using serviceResponseAgingDays
			latestHistoryDate = GMTUtility.getGMTDateTime(false).getTime() - (MILLIS_IN_DAY * serviceResponseAgingDays);
			formatter = new SimpleDateFormat("MM-dd-yyyy");
			lastestHistoryDateString = formatter.format(new Date(latestHistoryDate));
			// Delete all serviceresponsehistory records that are older that a certain number of days
			deleteStatement = "delete from SERVICE_RESPONSE_HISTORY" +
					" where time_sent < to_date(?, 'MM-DD-YYYY')";

			DatabaseQueryBean.executeUpdate(deleteStatement, false, new Object[]{lastestHistoryDateString});

			// CR-597 crhodes 3/1/20011 Begin  
			// Build and execute delete for in the payment_upload_error_log table
			deleteStatement = "delete from payment_upload_error_log where p_payment_file_upload_oid in (" +
					"select payment_file_upload_oid from payment_file_upload where " +
					"creation_timestamp < to_date(?, 'MM-DD-YYYY'))";

			DatabaseQueryBean.executeUpdate(deleteStatement, false, new Object[]{lastestHistoryDateString});

			deleteStatement = "delete from payment_file_upload where " +
					"creation_timestamp < to_date(?, 'MM-DD-YYYY')";

			DatabaseQueryBean.executeUpdate(deleteStatement, false, new Object[]{lastestHistoryDateString});
			// CR-597 crhodes 3/1/20011 End            

			// ShilpaR CR707 Begin  
			long latestPODataDate =
					GMTUtility.getGMTDateTime(false).getTime() - (MILLIS_IN_DAY * maxPOUploadDataAge);
			formatter = new SimpleDateFormat("MM-dd-yyyy");
			String lastestPODataDateString =
					formatter.format(new Date(latestPODataDate));


			// Build and execute delete in the po_upload_error_log table
			deleteStatement = "delete from po_upload_error_log where p_po_file_upload_oid in (" +
					"select po_file_upload_oid from po_file_uploads where " +
					"creation_timestamp < to_date(?, 'MM-DD-YYYY'))";

			DatabaseQueryBean.executeUpdate(deleteStatement, false, new Object[]{lastestPODataDateString});

			deleteStatement = "delete from po_file_uploads where " +
					"creation_timestamp < to_date(?, 'MM-DD-YYYY')";

			DatabaseQueryBean.executeUpdate(deleteStatement, false, new Object[]{lastestPODataDateString});
			// ShilpaR CR707 End     

			//MEer Rel 8.4 CR-934A STARTS
			//calculate lastestDate to purge service logs using ptlsvcsServiceLogAgingDays

			long  latestWebServiceLogsDate = GMTUtility.getGMTDateTime(false).getTime() - (MILLIS_IN_DAY * ptlsvcsServiceLogAgingDays);
			formatter = new SimpleDateFormat("MM-dd-yyyy");
			String lastestServiceLogsDateString = formatter.format(new Date(latestWebServiceLogsDate));
			// Delete all portal web service log records that are older than a value(certain number of days) configured in CONFIG_SETTING table
			deleteStatement = "delete from ptlsvcs_service_log" +
					" where request_timestamp < to_date(?, 'MM-DD-YYYY')";

			DatabaseQueryBean.executeUpdate(deleteStatement, false, new Object[]{lastestServiceLogsDateString});

			//MEer Rel 8.4 CR-934A ENDS 

			//Srinivasu_D Rel9.1 CR-934B - start
			//calculate lastestDate to purge service logs using ptlsvcsServiceLogAgingDays

			long  latestWebServiceSigningLogsDate = GMTUtility.getGMTDateTime(false).getTime() - (MILLIS_IN_DAY * ptlsvcsServiceSigningLogAgingDays);
			formatter = new SimpleDateFormat("MM-dd-yyyy");
			String lastestServiceSigningLogsDateString = formatter.format(new Date(latestWebServiceSigningLogsDate));
			// Delete all portal web service log records that are older than a value(certain number of days) configured in CONFIG_SETTING table
			deleteStatement = "delete from ptlsvcs_transaction_signing" +
					" where creation_timestamp < to_date(?, 'MM-DD-YYYY')";

			DatabaseQueryBean.executeUpdate(deleteStatement, false, new Object[]{lastestServiceSigningLogsDateString});

			deleteStatement = "delete from ptlsvcs_tran_signing_tran" +
					" where creation_timestamp < to_date(?, 'MM-DD-YYYY')";

			DatabaseQueryBean.executeUpdate(deleteStatement, false, new Object[]{lastestServiceSigningLogsDateString});

			//Srinivasu_D Rel9.1 CR-934B - end


			logInfo("Finished purging history records older than "+agingDays+" days, (" + serviceResponseAgingDays + " days for Service Response History) and ("+ptlsvcsServiceLogAgingDays+" days for Web Service logs)");
		}
		catch(Exception e)
		{
			System.out.println("Error occurred while purging history records...");
			e.printStackTrace();
		}
	}

	/**
	 * This methods checks to see whether the passed in task should be run.
	 * The target time and a time window is specified in a configuration file.
	 * The task is allowed to run during this time window.   If it does not
	 * run during that window (due to server being down, etc), the task is skipped for that day.
	 *
	 * @param periodicTaskType - the task to check and see if it should be run
	 * @return boolean - true if the task should be run
	 */
	private boolean isItTimeToRun(String periodicTaskType, Date nowDate)
	{
		// Determine the allowed times that this task can run
		TimeWindowData timeWindowData = (TimeWindowData) timeWindows.get(periodicTaskType);
		if(timeWindowData == null){
			logInfo("The periodic task type ["+periodicTaskType+"] has NOT Found in the Configuration file.");
			return false;
		}
		// The timestamp of when this task last ran.
		// It will either be read from the database, or
		// will be defaulted to zero if the task has never has run before
		// If defaulted to 0, the task will run the first time that the current time is within the window
		long lastRunDate = 0;

		// Numerical representation of the date/time right now
		long now;

		// Look in the database at when this task last ran
		try
		{
			now = nowDate.getTime();
			String sql = "select max(run_timestamp) as run_timestamp from periodic_task_history where task_type = ?";
			query.setSQL(sql, new Object[]{periodicTaskType});
			query.getRecords();
		
			if(query.getRecordCount() > 0)
			{
				query.scrollToRow(0);
				String dateString = query.getRecordValue("RUN_TIMESTAMP");

				if((dateString==null) || dateString.equals(""))
					// This will only happen when they're aren't any rows in the history table yet
					lastRunDate = 0;
				else
					lastRunDate = isoDateFormatter.parse(query.getRecordValue("RUN_TIMESTAMP")).getTime();
			}
		}
		catch(Exception e)
		{
			// There was a problem getting the last run date.   Show error and return false
			logError("Could not get last run date to determine if periodic task should be run", e);
			e.printStackTrace();
			return false;
		}



		// The target time to run is the specified date and time today
		GregorianCalendar targetTodayCal = (GregorianCalendar) GregorianCalendar.getInstance();
		targetTodayCal.setTime(nowDate);
		// Set the target time to be today at the hour and minute from the config file

		targetTodayCal.set(Calendar.HOUR_OF_DAY, timeWindowData.hourToRun);
		targetTodayCal.set(Calendar.MINUTE, timeWindowData.minuteToRun);
		targetTodayCal.set(Calendar.SECOND, 0);

		// These values are used to determine if right now is within a window to run
		long windowPeriod = timeWindowData.windowHours * MILLIS_IN_HOUR;
		long targetToday = targetTodayCal.getTime().getTime();
		

		// We need to know the previous target too.   It could be that we're not in today's window,
		// but that we're still in yesterday's window.
		// For example, assume the time to run is 5 PM with a window of 12 hours.
		//  If the current time is 9AM, we're not in today's window or yesterday's window
		//  If the current time is 6PM, we're in today's window
		//  If the current time is 2AM, we're still in yesterday's window
		// Set the previous target to be 24 hours before today's target
		long previousTarget = targetToday - MILLIS_IN_DAY;

		Debug.debug("---------------------------------------------------");
		Debug.debug("Task Type = "+periodicTaskType);
		Debug.debug("Now = "+new Date(now));
		Debug.debug("Target Today = "+new Date(targetToday));
		Debug.debug("End of Window Today = "+new Date(targetToday + windowPeriod));
		Debug.debug("Target Yesterday = "+new Date(previousTarget));
		Debug.debug("End of Window Yesterday = "+new Date(previousTarget + windowPeriod));
		Debug.debug("Last Run Date = "+new Date(lastRunDate));
		Debug.debug("---------------------------------------------------");



		// Is it currently within today's window?
		if(  ((now >= targetToday) && (now <= (targetToday + windowPeriod))) )
		{
			// We're in today's window

			// Is the last run date before today's target?  This indicates that it has not been
			// run today
			if(lastRunDate < targetToday)
			{
				// Task has not yet run for today's window.
				// Last, check to make sure we're not in an excluded day
				if(!isExcludedDay(targetToday))
					return true;
			}
		}
		// Is it currently still within yesterday's window?
		else if( ((now >= previousTarget) && (now <= (previousTarget + windowPeriod)))  )
		{
			// We're in the previous (yesterday's) window
			// Is the last run date before the previous target?  This indicates that it has not been
			// run for yesterday's target
			if(lastRunDate < previousTarget)
			{
				// Task has not yet run for yesterday's window (maybe there was a problem yesterday)
				// Last, check to make sure we're not in an excluded day
				if(!isExcludedDay(previousTarget))
					return true;
			}
		}
		

		return false;
	}


	private void processSupplierPortalEmails(Date rightNow) {

		DocumentHandler resultSet = null;
		String corpOrgOid = null;
		String spEmailOid = null;
		StringBuffer whereClause = null;

		try{

			String dateTime =  DateTimeUtility.convertDateToDateTimeString(rightNow);   
			// Send an email for each corp org supplier for every user given interval if there are any  
			// new invoices available for financing

			StringBuilder sql = new StringBuilder();

			sql.append("select sp_email_oid, p_corp_organization_oid ");
			sql.append(" from SP_EMAIL_NOTIFICATIONS ");
			sql.append(" where  ");
			sql.append("next_sp_email_due");
			sql.append("<= TO_DATE("); 
			sql.append("?, 'MM/dd/yyyy HH24:mi:ss')");

			//System.out.println("Query to get ListofSuppliers: ["+ sql.toString()+"]");
			resultSet = DatabaseQueryBean.getXmlResultSet(sql.toString(), false, new Object[]{dateTime});

			if(resultSet == null)
			{
				//logInfo("Finised run of supplier portal email process. No suppliers with email due. Sent 0 e-mails. ");
				return ;
			}

			List<DocumentHandler> corpOrgSupplersList = resultSet.getFragmentsList("/ResultSetRecord");
			//logInfo("The number of suppliers whose email is due if invoices available are [ "+ corpOrgSupplersList.size()+ "]");


			for (int i=0; i< corpOrgSupplersList.size(); i++){

				corpOrgOid = resultSet.getAttribute("/ResultSetRecord("+ i +")/P_CORP_ORGANIZATION_OID");
				spEmailOid = resultSet.getAttribute("/ResultSetRecord("+ i +")/SP_EMAIL_OID");	

				//logInfo("Start processing emails for supplier with OrgId: ["+ corpOrgOid +"]");
				whereClause = new StringBuffer();
				List<Object> sqlParams = new ArrayList<Object>();
				whereClause.append(" a_corp_org_oid =? and supplier_portal_invoice_ind = ? ");
				whereClause.append(" and sp_email_sent_ind <> ? ");
				//IR 29169- trigger email only for BUYER_APPROVED invoice
				// whereClause.append("and supplier_portal_invoice_status  <> '" + TradePortalConstants.SP_STATUS_INELLIGIBLE +"'");
				whereClause.append("and supplier_portal_invoice_status = ?");
				sqlParams.add(corpOrgOid);
				sqlParams.add(TradePortalConstants.INDICATOR_YES);
				sqlParams.add(TradePortalConstants.INDICATOR_YES);
				sqlParams.add(TradePortalConstants.SP_STATUS_BUYER_APPROVED);

				int  invoiceCount = DatabaseQueryBean.getCount("invoice_oid", "invoice", whereClause.toString(),false,  sqlParams);
				//logInfo("The supplier with OrgId "+  corpOrgOid  +" has "+   invoiceCount +" awaiting invocies")  ;


				if(invoiceCount>0){
					logInfo("Calling mediator for supplier with corpOrgOid..."+ corpOrgOid+" which has "+   invoiceCount +" awaiting invocies for email");
					Mediator spEmailMediator = null;
					try
					{
						spEmailMediator = (Mediator)EJBObjectFactory.createClientEJB(ejbServerLocation, "SPEmailMediator");

					}
					catch(Exception e)
					{
						e.printStackTrace();
						logError("While creating an instance of SPEmailMediator:", e);
						return;
					}

					DocumentHandler inputDoc = new DocumentHandler();
					inputDoc.addComponent("/In", new DocumentHandler());
					inputDoc.setAttribute("corpOrgOid", corpOrgOid);
					inputDoc.setAttribute("spEmailOid", spEmailOid);
					inputDoc.setAttribute("invoiceCount", String.valueOf(invoiceCount));

					try
					{
						DocumentHandler outputDoc = spEmailMediator.performService(inputDoc, csdb);
						spEmailMediator.remove();

						if( outputDoc != null){
							String result = outputDoc.getAttribute("/Out/result");
							if("ERROR".equals(result)){	   
								String errorMsg = outputDoc.getAttribute("/Out/result/errorMessage");
								logError("For Supplier OrgId: "+corpOrgOid+" " + errorMsg);
							}else if("SUCCESS".equals(result)){
								logInfo("Email sent successfully to supplier with OrgOid : "+ corpOrgOid);
							}
						}

					}catch (Exception e){
						e.printStackTrace();
						logError("While running an instance of SPEmailMediator:", e);
						return;
					}
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
			logError("Problem occurred while processing supplier portal emails..", e);
			return;
		}
	}


	private void runPurgeCrNoteAndInvoices(Date runDate){

		logInfo("starting purgeCrNoteandInvoices task ...");

		try{
			String invoiceMessage = "";
			String crNoteMessage = "";
		
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");  
			String now =  sdf.format(runDate);
			String sql = "select UPLOAD_CREDIT_NOTE_OID from credit_notes where expiry_date < to_date(?,'yyyy-MM-DD') "
			+ " and (credit_note_status = ? or credit_note_status = ? ) and portal_approval_req_ind != ?";

			query.setSQL(sql, new Object[]{now, "FAUT", "AVA","Y"}); 
			query.getRecords();

			int rowCount = query.getRecordCount();

			if (rowCount >0){
			
				Mediator uploadInvoiceMediator =(Mediator)EJBObjectFactory.createClientEJB(ejbServerLocation, "UploadInvoiceMediator");
 				DocumentHandler     inputDoc    = new DocumentHandler();
 				ClientServerDataBridge csdb = new ClientServerDataBridge();
				csdb.setLocaleName(LOCALE_NAME);
				 
 				inputDoc.setAttribute("/In/Update/ButtonPressed", TradePortalConstants.CREDIT_NOTE_PURGE);
				DocumentHandler outputDoc = uploadInvoiceMediator.performService(inputDoc, csdb);
 				 
				 
  				String status = outputDoc.getAttribute("/Out/CreditNoteInv/PurgeStatus");
  				String toBe =outputDoc.getAttribute("/Out/CreditNote/ToBeClosed");
  				String total =outputDoc.getAttribute("/Out/CreditNote/PurgeCount");
   				
  				if (StringFunction.isNotBlank(status) && TradePortalConstants.INV_CRNOTE_PURGE_ERROR.equals(status) ){
  					logInfo("CreditNotePurge:Severe error occured while purging credit notes. Please see application server logs for more details" )  ;
  				}else{
  					logInfo("Successfully closed <" + total +">  out of  <" + toBe + "> CreditNotes to be closed");
   					if (!total.equals(toBe)){
  						logInfo("Please see application server logs for details on failed CreditNotes");
  					}
  				}
 				uploadInvoiceMediator.remove();
 			}
 
			String invSql = "select UPLOAD_INVOICE_OID from invoices_summary_data where invoice_status in ('" 
					+ TradePortalConstants.PAYABLE_UPLOAD_INV_STATUS_PENDING_ACTION +"','" 
					+ TradePortalConstants.UPLOAD_INV_STATUS_AUTH_FAILED +"','" 
					+ TradePortalConstants.PAYABLE_UPLOAD_INV_STATUS_CANCELLED +"','" 
					+ TradePortalConstants.PAYABLE_UPLOAD_INV_STATUS_UNASSIGNED + "') and "
					+ " portal_approval_req_ind != ? and case when  payment_date is null then due_date else payment_date end < to_date(?,'yyyy-MM-DD')";
 			
			
			query.setSQL(invSql, new Object[]{now, "Y"}); 
			query.getRecords();
			
			rowCount = query.getRecordCount();
			
			if (rowCount >0){
				
				Mediator uploadInvoiceMediator =(Mediator)EJBObjectFactory.createClientEJB(ejbServerLocation, "UploadInvoiceMediator");
 				DocumentHandler     inputDoc    = new DocumentHandler();
 				ClientServerDataBridge csdb = new ClientServerDataBridge();
				csdb.setLocaleName(LOCALE_NAME);
				 
 				inputDoc.setAttribute("/In/Update/ButtonPressed", TradePortalConstants.INVOICE_PURGE);
				DocumentHandler outputDoc = uploadInvoiceMediator.performService(inputDoc, csdb);
  				
  				
  			 
  				String status = outputDoc.getAttribute("/Out/CreditNoteInv/PurgeStatus");
  				String toBe =outputDoc.getAttribute("/Out/Invoice/ToBeClosed");
  				String total =outputDoc.getAttribute("/Out/Invoice/PurgeCount");
   				
  				if (StringFunction.isNotBlank(status) && TradePortalConstants.INV_CRNOTE_PURGE_ERROR.equals(status) ){
  					logInfo("InvoicePurge:Severe error occured while purging Invoices. Please see application server logs for more details" )  ;
  				}else{
  					logInfo("Successfully closed <" + total +">  out of  <" + toBe + "> Invoices to be closed");
  					
  					if (!total.equals(toBe)){
  						logInfo("Please see application server logs for details on failed Invoices");
  					}
  				}
  						
  				uploadInvoiceMediator.remove();
 			}
  			logInfo("Credit Note,Invoice purging completed" )  ;
			
		} 
		catch(Exception e)
		{
 			
			logError("Could not purge expired credit note ", e);
			e.printStackTrace();
 		}
	
		finally{
			PeriodicTaskHistory taskHistory = null;
			
			try {
				
				taskHistory = (PeriodicTaskHistory) EJBObjectFactory.createClientEJB(ejbServerLocation, "PeriodicTaskHistory");
				taskHistory.newObject();
				taskHistory.setAttribute("task_type", TradePortalConstants.EOD_PURGE_CRNOTES_INVOICES);
				taskHistory.setAttribute("run_timestamp", formatter.format(runDate));
				taskHistory.save();
				taskHistory.remove();
				
			} catch (RemoteException e) {
 				e.printStackTrace();
			} catch (AmsException e) {
 				e.printStackTrace();
			} catch (RemoveException rme){
				rme.printStackTrace();
			}
 		}
 
	}
	
	
	
	/*
	 * MEer Rel 9.3 CR-1006 Periodic Task Agent
	 * Three new task types added to run every 10 minutes throughout the day.
	 *Customers will be receiving an email notification  for all eligible awaiting invoices or credit notes.
	 * 
	 * 
	 */
	private void runInvoiceCrnApprovalEmail(String taskType) {
		
		String notifTransType ="";
		String emailSentInd ="";
		String nextEmailDue = "";
		DocumentHandler resultSet = null;
		String corpOrgOid = null;
		String invCRNEmailNotifyOid = null;
		StringBuilder whereClause = null;
		Date	rightNow = null;
		String instrType = TradePortalConstants.PAYABLES_MGMT;
		CorporateOrganization corpOrg = null;
		try{	
			rightNow = serverDeploy.getServerDate(false);
			//logInfo("Entering InvoiceCRNoteApprovalEmail Notification task ...for ["+taskType+"] at "+ rightNow);
			
			if(TradePortalConstants.PINA_EMAIL_TASK_TYPE.equals(taskType)){
				emailSentInd = "PIN_NOTIFY_EMAIL_SENT_IND";
				notifTransType = TradePortalConstants.PIN_TRANS_TYPE;
				nextEmailDue = "next_pin_email_due";
			}else if(TradePortalConstants.PCNA_EMAIL_TASK_TYPE.equals(taskType)){
				emailSentInd = "PCN_NOTIY_EMAIL_SENT_IND";
				notifTransType = TradePortalConstants.PCN_TRANS_TYPE;
				nextEmailDue = "next_pcn_email_due";
			}else if(TradePortalConstants.RINA_EMAIL_TASK_TYPE.equals(taskType)){
				emailSentInd = "RIN_NOTIFY_EMAIL_SENT_IND";
				notifTransType = TradePortalConstants.RIN_TRANS_TYPE;
				nextEmailDue = "next_rin_email_due";
				instrType = TradePortalConstants.REC;
			}
			String dateTime =  DateTimeUtility.convertDateToDateTimeString(rightNow);   
			
			StringBuilder sql = new StringBuilder();
			List<Object> sqlParams = new ArrayList();

			//Rel9.5 CR-927B - Changed query to join with Notification rules tables
			sql.append("select i.inv_crn_email_notify_oid, i.p_corp_organization_oid ");
			sql.append(" from INVOICE_CRN_EMAIL_NOTIFICATION i, CORPORATE_ORG  c, NOTIFICATION_RULE n, NOTIFICATION_RULE_CRITERION r where ");
			sql.append(" i.p_corp_organization_oid=c.organization_oid and ");
			sql.append(nextEmailDue);
			sql.append("<=  TO_DATE(?,'MM/dd/yyyy HH24:mi:ss') and ");
			sql.append(" i.p_corp_organization_oid =n.p_owner_org_oid and r.p_notify_rule_oid=n.notification_rule_oid and ");
			sql.append(" r.instrument_type_or_category=? and r.transaction_type_or_action=? and r.send_email_setting=? ");
			sqlParams.add(dateTime);
			sqlParams.add(TradePortalConstants.HTWOH_INV_INST_TYPE);
			sqlParams.add(notifTransType);
			sqlParams.add(TradePortalConstants.INDICATOR_YES);


			//System.out.println("Query to get List of customers to receive email notifications: ["+ sql.toString()+"]");
			resultSet = DatabaseQueryBean.getXmlResultSet(sql.toString(), false, sqlParams);


			if(resultSet == null)
			{
				logInfo("Finised run of invoice and credit notes email notification process. No cutomers with email due. Sent 0 e-mails. ");
				return ;
			}

			List<DocumentHandler> corpOrgList = resultSet.getFragmentsList("/ResultSetRecord");
			//logInfo("The number of customers with email due if there are awaiting invoices or credit notes are [ "+ corpOrgList.size()+ "]");


			for (int i=0; i< corpOrgList.size(); i++){

				corpOrgOid = resultSet.getAttribute("/ResultSetRecord("+ i +")/P_CORP_ORGANIZATION_OID");
				invCRNEmailNotifyOid = resultSet.getAttribute("/ResultSetRecord("+ i +")/INV_CRN_EMAIL_NOTIFY_OID");	
				whereClause = new StringBuilder();
				List<Object> sqlParm = new ArrayList<Object>();
				int  invCrnCount =0;

				whereClause.append(" a_corp_org_oid =?"); 
				whereClause.append(" and ");
				whereClause.append(emailSentInd);
				whereClause.append(" <> ?");
				whereClause.append(" and linked_to_instrument_type = ?");
				whereClause.append(" and PORTAL_APPROVAL_REQ_IND = ?");

				sqlParm.add(corpOrgOid); 
				sqlParm.add(TradePortalConstants.INDICATOR_YES); 
				sqlParm.add(instrType); 
				sqlParm.add(TradePortalConstants.INDICATOR_YES); 

				if(TradePortalConstants.PCNA_EMAIL_TASK_TYPE.equals(taskType)){
					invCrnCount = DatabaseQueryBean.getCount("UPLOAD_CREDIT_NOTE_OID", "credit_notes", whereClause.toString(),false,  sqlParm);
				}else{
					invCrnCount = DatabaseQueryBean.getCount("UPLOAD_INVOICE_OID", "invoices_summary_data", whereClause.toString(),false,  sqlParm);
				}

				//logInfo("The customer with OrgId "+  corpOrgOid  +" has "+   invCrnCount +" awaiting invocies/credit notes")  ;

				if(invCrnCount>0){
					logInfo("Calling mediator for invCNApproval Email with corpOrgOid..."+ corpOrgOid +" has "+   invCrnCount +" awaiting invocies/credit notes");


					Mediator invCRNEmailMediator = null;
					try
					{
						invCRNEmailMediator = (Mediator)EJBObjectFactory.createClientEJB(ejbServerLocation, "InvoiceCRNoteApprovalEmailMediator");
					}
					catch(Exception e)
					{
						e.printStackTrace();
						logError("While creating an instance of InvCRNEmailMediator:", e);
						return;
					}

					DocumentHandler inputDoc = new DocumentHandler();
					inputDoc.addComponent("/In", new DocumentHandler());
					inputDoc.setAttribute("corpOrgOid", corpOrgOid);
					inputDoc.setAttribute("invCRNEmailOid", invCRNEmailNotifyOid);
					inputDoc.setAttribute("emailSentInd", emailSentInd);
					inputDoc.setAttribute("invCRNCount", String.valueOf(invCrnCount));
					inputDoc.setAttribute("taskType", taskType);
					inputDoc.setAttribute("nextEmailDue", nextEmailDue);
					try
					{
						DocumentHandler outputDoc = invCRNEmailMediator.performService(inputDoc, csdb);
						invCRNEmailMediator.remove();

						if( outputDoc != null){
							String result = outputDoc.getAttribute("/Out/result");
							if("ERROR".equals(result)){	   
								String errorMsg = outputDoc.getAttribute("/Out/result/errorMessage");
								logError("For Customer OrgId: "+corpOrgOid+" " + errorMsg);
							}else if("SUCCESS".equals(result)){
								logInfo("Email sent successfully to customer with OrgOid : "+ corpOrgOid);
							}
						}

					}catch (Exception e){
						e.printStackTrace();
						logError("While running an instance of invCRNoteApprovalEmailMediator:", e);
						return;
					}
				}

			}
			//logInfo("Finished processing invoices and credit notes approval notification emails ....");  

		}catch (Exception e) {
			e.printStackTrace();
			logError("Problem occurred while processing invoices and credit notes approval emails..", e);
			return;
		}


		finally{
			PeriodicTaskHistory taskHistory = null;

			try {

				taskHistory = (PeriodicTaskHistory) EJBObjectFactory.createClientEJB(ejbServerLocation, "PeriodicTaskHistory");
				taskHistory.newObject();
				taskHistory.setAttribute("task_type", taskType);
				taskHistory.setAttribute("run_timestamp", formatter.format(rightNow));
				taskHistory.save();
				taskHistory.remove();

			} catch (RemoteException e) {
				e.printStackTrace();
			} catch (AmsException e) {
				e.printStackTrace();
			} catch (RemoveException rme){
				rme.printStackTrace();
			}
		}
	//	logInfo("Exiting InvoiceCRNoteApprovalEmail Notification task ..."+ rightNow);


	}
	
	

	/**
	 * Inner class used to represent the window in which a task can run
	 */
	private class TimeWindowData
	{
		int hourToRun;
		int minuteToRun;
		int windowHours;
		int fixedIntervalSecondsToRun; //Rel 9.3 CR-1006- Fixed Interval is considered in SECONDS.

		public TimeWindowData(String hour, String minute, String window, String fixedInterval)
		{
			hourToRun   = Integer.parseInt(hour);
			minuteToRun = Integer.parseInt(minute);
			windowHours = Integer.parseInt(window); 
			fixedIntervalSecondsToRun = Integer.parseInt(fixedInterval);
			
		}
	}
	
	
	
	/**
	 * MEer CR-1006 Rel 9.3 
	 * Inner Timer class used to schedule a task in Fixed periodic intervals throughout the day using java Timer API.
	 * The interval can be read from Configuration file(FixedIntervalToRun - this is in seconds).
	 * It will not run on Saturdays and Sundays.
	 * 
	 */
	private class FixedIntervalTask {

		public  FixedIntervalTask( String periodicTaskType, int fixedIntervalSecondsToRun){	
			
			try{
				RepeatTask task = null;
				logInfo("Inside Inner class to schedule periodic task ["+periodicTaskType+"] for every "+fixedIntervalSecondsToRun+" seconds");
				timer = new Timer(periodicTaskType);
				task = new RepeatTask(periodicTaskType);
				//timer.scheduleAtFixedRate(task, 0, fixedIntervalSecondsToRun*1000);// scheduling task at interval (delays the task for 0 second, and then run task at fixed interval from Configuration file given in seconds)
				//timer.scheduleAtFixedRate(task, serverDeploy.getServerDate(false), fixedIntervalSecondsToRun*1000);//  Schedules the specified task for repeated interval, beginning at the specified time.
				timer.scheduleAtFixedRate(task, serverDeploy.getServerDate(false), fixedIntervalSecondsToRun*1000);
			}catch(Exception e){
				logError("Error in scheduling TimerTask...");
				e.printStackTrace();
				timer.cancel();
				timer.purge();			
			}
		}
		
		
		/**
		 * Inner class extends TimerTask and overrides run() to execute actual task.
		 */
		class RepeatTask extends TimerTask {	

			String taskType="";
			Date rightNow = null;

			public RepeatTask(String periodicTaskType) {
				taskType = periodicTaskType;
			}

			public void run() {	
				try {
					rightNow = serverDeploy.getServerDate(false);
				} catch (RemoteException rme) {
					rme.printStackTrace();
				} catch (AmsException ex) {
					ex.printStackTrace();
				}

				logInfo("Inside TimerTask to execute periodic task ["+taskType+"] to send approval emails to customers having awaiting invoices or credit notes."+rightNow);			
				if(!isExcludedDay(rightNow.getTime())){
					runInvoiceCrnApprovalEmail(taskType);	
				}
			}		
		}
		
	}

	

	/**
	 * This methods checks to see whether or not the passed in date falls
	 * on a Saturday or Sunday. In the future, this could be expanded to
	 * check for business days and holidays.
	 *
	 * @param dateToCheck - the timestamp being evaluated
	 * @return boolean - true if the date passed in falls on an excluded day
	 */
	private boolean isExcludedDay(long dateToCheck)
	{
		GregorianCalendar toCheck = (GregorianCalendar) GregorianCalendar.getInstance();
		toCheck.setTime(new Date(dateToCheck));
		int dayOfWeek = toCheck.get(Calendar.DAY_OF_WEEK);
		if((dayOfWeek == Calendar.SATURDAY) || (dayOfWeek == Calendar.SUNDAY))
			return true;
		else
			return false;
	}

	@Override
    protected  boolean connectToMQ() {
     return true;	
    }
	
	
	

	
}
