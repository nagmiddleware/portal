package com.ams.tradeportal.agents;



import java.io.IOException;
import java.util.MissingResourceException;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.Logger;
import com.ibm.mq.MQC;
import com.ibm.mq.MQEnvironment;
import com.ibm.mq.MQException;
import com.ibm.mq.MQGetMessageOptions;
import com.ibm.mq.MQMessage;
import com.ibm.mq.MQPutMessageOptions;
import com.ibm.mq.MQQueue;
import com.ibm.mq.MQQueueManager;

/**
 *   Here goes detailed description of MQWrapper functionality
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class MQWrapper{
//private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(MQWrapper.class);

   // This PropertyResourceBundle contains all configuration parameters from AgentConfiguration.properties file
   protected static final PropertyResourceBundle properties =
        (PropertyResourceBundle)ResourceBundle.getBundle("AgentConfiguration");

   // Collection of MQ parameters:

   protected String mqInputQueue = null;
   protected String mqOutputQueue = null;
   protected String mqErrorQueue = null;
   protected int mqWaitInterval;  //CR-451 NShrestha 11/19/2008

   private static final String MQ_CLIENT         = properties.getString("mqClient");
   private static final int    MQ_RETRY_COUNT    = Integer.parseInt(properties.getString("mqRetryCount"));
   private static final int    MQ_RETRY_INTERVAL = Integer.parseInt(properties.getString("mqRetryInterval"));
   
   /* The IBM MQ Series Queue Manager */
   private    MQQueueManager  qMgr;

   private  int     mqCurrentRetryCount;
   private  String  agentID;
   private static String loggerCategory;

    static
    {
        loggerCategory        = properties.getString("loggerServerName");
    }

    // W Zhu 2/20/08 WEUH121436595 BEGIN
    /**
     * Set the ID of the agent that uses this MQWrapper.
     * Retrieves the queue information according to the ID.
     * If there is no queue configured for this agent ID, retrieve the generic queue.
     * Currently the generic queue names are required parameters.
     * This method needs to be called before trying to get or put any messages on queue.
     *
     * @param agentID
     */
    public void setAgentID(String agentID){
    	this.agentID = agentID;
    	try {
    		mqInputQueue   = properties.getString("mqInputQueue."+agentID);
    	}
    	catch (MissingResourceException e){
       		mqInputQueue = properties.getString("mqInputQueue");
    	}

    	try {
    		mqOutputQueue   = properties.getString("mqOutputQueue."+agentID);
    	}
    	catch (MissingResourceException e){
    		mqOutputQueue = properties.getString("mqOutputQueue");
    	}

    	try {
    		mqErrorQueue   = properties.getString("mqErrorQueue."+agentID);
    	}
    	catch (MissingResourceException e){
    		mqErrorQueue = properties.getString("mqErrorQueue");
    	}
   
    	try {
    		mqWaitInterval   = Integer.parseInt(properties.getString("mqWaitInterval."+agentID));
    	}
    	catch (MissingResourceException e){
    		mqWaitInterval = Integer.parseInt(properties.getString("mqWaitInterval"));
    	}
    	

    }
    // W Zhu 2/20/08 WEUH121436595 END

   protected void commit() throws MQException {
        qMgr.commit();
   }

   protected void backout() throws MQException {
        qMgr.backout();
   }

   protected int connectToMQ(){
	   String mqServerPort="";
	   try {
         if (MQ_CLIENT.equals("true")){
            MQEnvironment.hostname = properties.getString("mqServerIPAddress");
            MQEnvironment.channel  = properties.getString("mqServerChannel");
            //IValavala T36000016381. Rel 8.2 Get mqServerPort if specified in AgentConfiguration file
            
            try {
            	mqServerPort = properties.getString("mqServerPort");
    		}
    		catch (java.util.MissingResourceException e){
    		}
            if (!((mqServerPort == null || mqServerPort.trim().length() < 1))){
            	MQEnvironment.port  = Integer.parseInt(mqServerPort);
            }
          //IValavala End T36000016381. Rel 8.2
            
         }

         qMgr = new MQQueueManager(properties.getString("mqManager"));
      }
      catch (Exception e){
    	//IValavala IR POUM080661913. Pass AGENT_ID to Logger.log method
    	  Logger.log(loggerCategory, agentID, "MQWrapper: Unable to Connect to MQ Series " + e.getMessage(), 5);
    	  Logger.log(loggerCategory, agentID, "MQWrapper: host: '" + properties.getString("mqServerIPAddress") + "' channel: '" + properties.getString("mqServerChannel") + "' port: '" + mqServerPort + "' Default Port is: '1414' qMgr: '" + properties.getString("mqManager") + "'", 5);
          return -1;
      }
      Logger.log(loggerCategory, agentID, "MQWrapper: Connected to MQ", 1);
      return 1;
   }

   protected int disconnectFromMQ()
   {
        try {
            qMgr.disconnect();
          //IValavala IR POUM080661913. Pass AGENT_ID to Logger.log method
            Logger.log(loggerCategory, agentID, "MQWrapper: Disconnecting from MQ Series", 1);
        }
        catch (Exception e) {
        	Logger.log(loggerCategory, agentID, "Disconnect from MQ Series Failed " + e.getMessage(), 5);
            return -1;
        }
        return 1;
   }

     /**
      * This method is a wrapper around BrowseMessageOnQueue. It attemps to read message from an MQ Input Queue
      * even if MQ connection is down, or MQ Manager becomes temporarily unavailable. Number of attemps is limited
      * by MQ_RETRY_COUNT parameter which is set in AgentConfiguration.properties file.
      *
      *
      * @return String
      */
      protected String attemptToBrowseMessageOnQueue(String browseQueue){
           String retrievedMsgText = null;
           try {
               retrievedMsgText = browseMessageOnQueue(browseQueue);
               mqCurrentRetryCount = 0; // reset mqCurrentRetryCount to zero if message retrieved successfully
           }catch (AmsException amex) {

               mqCurrentRetryCount = mqCurrentRetryCount + 1; // increment mqCurrentRetryCount if message retrieved unsuccessfully

               if (mqCurrentRetryCount < MQ_RETRY_COUNT){
                   try{
                       System.out.println(agentID + ": MQWrapper: attemptToBrowseMessageOnQueue: About to sleep for " + MQ_RETRY_INTERVAL + " msec");
                     //IValavala IR POUM080661913. Pass AGENT_ID to Logger.log method
                       Logger.log(loggerCategory, agentID, agentID + ": MQWrapper:  attemptToGetMessageFromQueue(): About to sleep for " + MQ_RETRY_INTERVAL + " msec", 5);
                       Thread.sleep(MQ_RETRY_INTERVAL);
                       return "";
                   }
                   catch(InterruptedException ie){
                       System.out.println(agentID + ": MQWrapper: Sleeping was interrupted in attemptToBrowseMessageOnQueue " + ie.getMessage());
                       Logger.log(loggerCategory, agentID, agentID + ": MQWrapper: Sleeping was interrupted in attemptToGetMessageFromQueue() " + ie.getMessage(), 5);
                   }
               }
               if (mqCurrentRetryCount >= MQ_RETRY_COUNT) {
                   System.out.println(agentID + ": MQWrapper: Unable to get message from MQ Input Queue");
                   Logger.log(loggerCategory, agentID, agentID + ": MQWrapper: Unable to get message from MQ Input Queue", 5);
                   disconnectFromMQ();
                   connectToMQ();
                   return "";
               }
           }

           return retrievedMsgText;
      }// end of attemptToBrowseMessageOnQueue()

   protected String browseMessageOnQueue(String browseQueue) throws AmsException {

        String retrievedMsgText = null;

        try {
            // For browsing from MQ (1 of 2)
            int openOptions = MQC.MQOO_INPUT_SHARED + MQC.MQOO_BROWSE + MQC.MQOO_INQUIRE;

            MQQueue inputQueue =    qMgr.accessQueue(browseQueue,
                                        openOptions,
                                        null,           // default q manager
                                        null,           // no dynamic q name
                                        null);          // no alternate user id

            // get the message ...
            // First define a MQ message buffer to receive the message into..
            MQMessage retrievedMessage = new MQMessage();

            // Set the get message options..
            MQGetMessageOptions gmo = new MQGetMessageOptions();  // accept the defaults same as MQGMO_DEFAULT
            gmo.options = MQC.MQGMO_BROWSE_NEXT;

            if (inputQueue.getCurrentDepth() < 1){
                inputQueue.close();
                return retrievedMsgText = "";
            }

            // Browse the message in the queue..
            inputQueue.get(retrievedMessage,
                        gmo

                        );

            int retrievedMsgLength = retrievedMessage.getMessageLength();
            retrievedMsgText = retrievedMessage.readString(retrievedMsgLength);
            inputQueue.close();
        }
        // Was it an MQ error?
        catch (MQException e){
            System.out.println(agentID + ": MQWrapper: An MQ error occurred : Completion code " +
                                e.completionCode +
                                " Reason code: " +
                                e.reasonCode + " Message: " +
                                e.getMessage());
            //IValavala IR POUM080661913 
            Logger.log(loggerCategory, agentID, agentID + ": MQWrapper: An MQ error occurred : Completion code " +
                                e.completionCode +
                                " Reason code: " +
                                e.reasonCode + " Message: " +
                                e.getMessage(),
                                5);
            throw new AmsException();


        }
        // Was it a Java buffer space error?
        catch (IOException e){
            System.out.println(agentID + ": MQWrapper: An error occurred whilst writing to the message buffer: " + e.getMessage());
          //IValavala IR POUM080661913 
            Logger.log(loggerCategory, agentID, agentID + ": MQWrapper: An error occurred whilst writing to the message buffer: " + e.getMessage(), 5);
            throw new AmsException();
        }
        return retrievedMsgText;

   }// end of getMessageFromQueue

   protected String getMessageFromQueue(byte[] correlationId) throws AmsException {

        String retrievedMsgText = null;
     // CR-451 NShrestha 11/29/2008 Begin
        boolean sendReceiveMsg = false;
        if (correlationId != null) {
        	sendReceiveMsg = true;
        }
     // CR-451 NShrestha 11/29/2008 End
        try {
            // For browsing from MQ (1 of 2)
            //int openOptions = MQC.MQOO_INPUT_SHARED + MQC.MQOO_BROWSE + MQC.MQOO_INQUIRE;


            // For reading and removing from MQ (1 of 1)
            int openOptions = MQC.MQOO_INPUT_AS_Q_DEF + MQC.MQOO_INQUIRE;




            // Now specify the queue that we wish to open, and the open options...
            MQQueue inputQueue =    qMgr.accessQueue(mqInputQueue,
                                        openOptions,
                                        null,           // default q manager
                                        null,           // no dynamic q name
                                        null);          // no alternate user id

            // get the message ...
            // First define a MQ message buffer to receive the message into..
            MQMessage retrievedMessage = new MQMessage();
            // CR-486 We retrieve message using encoding 1208 (Unicode UTF-8).
            // MQ server does the conversion for us if the message is encoded differently.
            // Since Unicode UTF-8 encompass about all characters in other encodings, we are safe to retrieve using it.
            retrievedMessage.characterSet = 1208;
            
            if (sendReceiveMsg) {
                retrievedMessage.correlationId = correlationId;
            }

            // Set the get message options..
            MQGetMessageOptions gmo = new MQGetMessageOptions();  // accept the defaults same as MQGMO_DEFAULT

            if (sendReceiveMsg) {
               gmo.options = MQC.MQGMO_CONVERT | MQC.MQGMO_WAIT | MQC.MQGMO_SYNCPOINT;
               // Specify the wait time in milliseconds
  		       gmo.waitInterval = mqWaitInterval;
            }
            else {
            	gmo.options = MQC.MQGMO_SYNCPOINT;
            	// Check whether any messages are available on MQ Input Queue

            	if (inputQueue.getCurrentDepth() < 1){
            		inputQueue.close();
            		return retrievedMsgText = "";
            	}
            }
            // get the message off the queue..
            inputQueue.get(retrievedMessage,
                        gmo
                        );


            int retrievedMsgLength = retrievedMessage.getMessageLength();
            // CR-486 use readStringOfByteLength
            retrievedMsgText = retrievedMessage.readStringOfByteLength(retrievedMsgLength); 

            // Close the queue
            inputQueue.close();
        }
        // Was it an MQ error?
        catch (MQException e){
        	//IValavala IR POUM080661913 
        	Logger.log(loggerCategory, agentID, agentID + ": MQWrapper: An MQ error occurred : Completion code " +
                                e.completionCode +
                                " Reason code: " +
                                e.reasonCode + " Message: " +
                                e.getMessage(),
                                5);
            throw new AmsException();
        }
        // Was it a Java buffer space error?
        catch (IOException e){
        	//IValavala IR POUM080661913 
        	Logger.log(loggerCategory, agentID, agentID + ": MQWrapper: An error occurred whilst writing to the message buffer: " + e.getMessage(), 5);
            throw new AmsException();
        }
        return retrievedMsgText;

   }// end of getMessageFromQueue

  /**
   * This method is a wrapper around getMessageFromQueue(). It attemps to read message from an MQ Input Queue
   * even if MQ connection is down, or MQ Manager becomes temporarily unavailable. Number of attemps is limited
   * by MQ_RETRY_COUNT parameter which is set in AgentConfiguration.properties file.
   *
   *
   * @return String
   */
   protected String attemptToGetMessageFromQueue(byte[] correlation, boolean checkRetry){
        String retrievedMsgText = null;
        try {
            retrievedMsgText = getMessageFromQueue(correlation);
            mqCurrentRetryCount = 0; // reset mqCurrentRetryCount to zero if message retrieved successfully
        }catch (AmsException amex) {
        	if (checkRetry)  {
            mqCurrentRetryCount = mqCurrentRetryCount + 1; // increment mqCurrentRetryCount if message retrieved unsuccessfully

            if (mqCurrentRetryCount < MQ_RETRY_COUNT){
                try{
                	//IValavala IR POUM080661913 
                	Logger.log(loggerCategory, agentID, agentID + ": MQWrapper:  attemptToGetMessageFromQueue(): About to sleep for " + MQ_RETRY_INTERVAL + " msec", 5);
                    Thread.sleep(MQ_RETRY_INTERVAL);
                    return "";
                }
                catch(InterruptedException ie){
                	Logger.log(loggerCategory, agentID, agentID + ": MQWrapper: Sleeping was interrupted in attemptToGetMessageFromQueue() " + ie.getMessage(), 5);
                }
            }
            if (mqCurrentRetryCount >= MQ_RETRY_COUNT) {
            	Logger.log(loggerCategory, agentID, agentID + ": MQWrapper: Unable to get message from MQ Input Queue", 5);
                disconnectFromMQ();
                connectToMQ();
                return "";
            }
        }
        }

        return retrievedMsgText;
   }// end of attemptToGetMessageFromQueue()

   protected String attemptToGetMessageFromQueue(){
	  return attemptToGetMessageFromQueue(null,true);
   }

   protected int putMessageOnQueue(String packagedMessage, String typeOfQueue, MQMessage correlation){
	   boolean sendReceiveMsg = false;

       if (correlation != null) {
       	sendReceiveMsg = true;
       }

       try {

         // W Zhu 2/25/08 Do not display the full message.  It could be very long.
         String displayMessageText;
         if (packagedMessage.length() > 700) {
       	    displayMessageText = packagedMessage.substring(0, 400) + "\n ... \n" + packagedMessage.substring(packagedMessage.length() - 300);
         }
         else {
       	    displayMessageText = packagedMessage;
         }
         //IValavala IR POUM080661913 
         Logger.log(loggerCategory, agentID, agentID + ": MQWrapper: Message to be put on " + typeOfQueue + " Queue: " + displayMessageText, 1);

         int openOptions = MQC.MQOO_OUTPUT;

         MQQueue outputQueue =   qMgr.accessQueue(
                                   typeOfQueue,
                                   openOptions,
                                   null,           // default q manager
                                   null,           // no dynamic q name
                                   null);          // no alternate user id

         MQMessage outputMessage = new MQMessage();

         // CR-486 begin
         // Send the message to MQ using the encoding as set by the AgentConfiguration.properties.
         // First try the encoding configured for the message type.  e.g.
         //   OutgoingMessageEncoding.TPL = 1208 
         // If not configured, then try the encoding configured for the output message in general
         //   OutgoingMessageEncoding = 1208
         // If not configured, then set to MQC.MQCCSI_Q_MGR, using the default encoding of the Queue 
         // Manager, which is usually 819. 
         // Common values for encodings:
         //    819  - ISO-8859-1/Latin1/IBM819
         //    1208 - Unicode UTF-8 (Preferred Unicode encoding)
         //    1200 - Unicode UTF-16 (strictly speaking USC-2)
         int messageTypeIndexBegin = packagedMessage.indexOf("<MessageType>");
         int messageTypeIndexEnd = packagedMessage.indexOf("</MessageType>");
         String messageType = "";
         outputMessage.characterSet = MQC.MQCCSI_Q_MGR;  // Default encoding
         if (messageTypeIndexBegin > -1 && messageTypeIndexEnd > messageTypeIndexBegin) {
             messageType = packagedMessage.substring(messageTypeIndexBegin + "<MessageType>".length(), messageTypeIndexEnd);
         }
         try {
             outputMessage.characterSet = Integer.parseInt(properties.getString("OutgoingMessageEncoding."+messageType));
         }
         catch (MissingResourceException e){
             try {
                 outputMessage.characterSet = Integer.parseInt(properties.getString("OutgoingMessageEncoding"));
             }
             catch (MissingResourceException e1) {                 
             }
         }
         catch (NumberFormatException e) {
             e.printStackTrace();
         }
         // CR-486 end

         if (sendReceiveMsg) {
        	 outputMessage.messageId = MQC.MQMI_NONE;
        	 outputMessage.format = MQC.MQFMT_STRING;
        	 outputMessage.encoding = MQC.MQENC_NATIVE;
        	 outputMessage.replyToQueueName = mqInputQueue;
         }
         else{
             int DestinationIDBeginIndex;
             int DestinationIDEndIndex;
             String destination = null;
             DestinationIDBeginIndex = packagedMessage.indexOf("<DestinationID>");
             DestinationIDEndIndex = packagedMessage.indexOf("</DestinationID>");
             if (DestinationIDBeginIndex > -1 && DestinationIDEndIndex > -1 && DestinationIDEndIndex > DestinationIDBeginIndex) {
                 destination = packagedMessage.substring(DestinationIDBeginIndex + "<DestinationID>".length(), DestinationIDEndIndex);
                 if (destination.equals("ANZ")){
                	 ////IValavala IR POUM080661913 
                     Logger.log(loggerCategory, agentID, agentID + ": MQWrapper: Message sent to  " + destination + " With CCSID: " + MQC.MQCCSI_Q_MGR + " Encoding: " + MQC.MQENC_NATIVE, 1);
                     outputMessage.messageId = MQC.MQMI_NONE;
                     outputMessage.format = MQC.MQFMT_STRING;
                     outputMessage.encoding = MQC.MQENC_NATIVE;
                 }
             }
         }
         outputMessage.writeString(packagedMessage);

         MQPutMessageOptions pmo = new MQPutMessageOptions();  // accept the defaults, same
         if (sendReceiveMsg) {
        	 pmo.options = MQC.MQPMO_NEW_CORREL_ID;
         }

         outputQueue.put(outputMessage, pmo);

         if (sendReceiveMsg) {
        	 correlation.correlationId = outputMessage.correlationId;
         }

         // Close the queue
         outputQueue.close();
         return 1;
       }
       // Was it an MQ error?
       catch (MQException e){
    	   ////IValavala IR POUM080661913 
    	   Logger.log(loggerCategory, agentID, agentID + ": MQWrapper: An MQ error occurred : Completion code: " +
                               e.completionCode +
                               " Reason code: " +
                               e.reasonCode + " Message: " +
                               e.getMessage(), 5);
           return -1;
       }
       // Was it a Java buffer space error?
       catch (IOException e){
    	   Logger.log(loggerCategory, agentID, agentID + ": MQWrapper: An error occurred whilst writing to the message buffer: " + e.getMessage(), 5);
           return -1;
       }
       catch (Exception e){
           // W Zhu 3/5/2014 IR T36000024954 sometimes there is strange NullPointerException with qMgr being null.
    	   Logger.log(loggerCategory, agentID, agentID + ": MQWrapper: An error occurred whilst writing to the message buffer: " + e.getMessage() + ";qMgr = " + qMgr, 5);
           return -1;
       }

  }//end of putMessageOnQueue()

   protected int attemptToPutMessageOnQueue(String packagedMessage, String typeOfQueue, MQMessage correlation){
	   for (int counter = 0; counter < MQ_RETRY_COUNT; counter++){
		   int mqSuccess = putMessageOnQueue(packagedMessage, typeOfQueue, correlation);
		   if (mqSuccess == 1) {
			   break;
		   }
		   if (mqSuccess != 1 ){
			   try{
				 //IValavala IR POUM080661913
				   Logger.log(loggerCategory, agentID, agentID + ": MQWrapper:  attemptToPutMessageToQueue(): About to sleep for " + MQ_RETRY_INTERVAL + " msec", 5);
				   Thread.sleep(MQ_RETRY_INTERVAL);
				   disconnectFromMQ();
				   connectToMQ();
			   }
			   catch(InterruptedException ie){
				   Logger.log(loggerCategory, agentID, agentID + ": MQWrapper: Sleeping was interrupted " + ie.getMessage(), 5);
			   }
		   }

		   if (mqSuccess != 1 && counter == MQ_RETRY_COUNT - 1) {
			   Logger.log(loggerCategory, agentID, agentID + ": MQWrapper: Unable to put message on MQ Queue: " + typeOfQueue, 5);
			   disconnectFromMQ();
			   connectToMQ();
			   return -1;
		   }
	   }
	   return 1;
   }


   public int sendMessageToQueue(String packagedMessage, String typeOfQueue){
       int rCode = connectToMQ();
       
       if (rCode == 1) {
           rCode = attemptToPutMessageOnQueue(packagedMessage, typeOfQueue);
           disconnectFromMQ();
       }
       
       return rCode;
       
   }
   

   protected int attemptToPutMessageOnQueue(String packagedMessage, String typeOfQueue){

        return attemptToPutMessageOnQueue(packagedMessage, typeOfQueue, null);
    }//end of attemptToPutMessageOnQueue()

    /**
	 * This method sends and receives message
	 *
	 */
    protected String sendReceiveMessage(String packagedMessage){
    	String result = null;
        MQMessage temp = new MQMessage();
    	int rcode = attemptToPutMessageOnQueue(packagedMessage, mqOutputQueue, temp);
    	if (rcode == 1) {
    		result = attemptToGetMessageFromQueue(temp.correlationId,false);
    	}
    	return result;
    }

    /**
	 * This method sends message and returns correlation ID
	 *
	 */
    protected byte[] sendMessageAsync(String packagedMessage){
        MQMessage temp = new MQMessage();
    	int rcode = attemptToPutMessageOnQueue(packagedMessage, mqOutputQueue, temp);
    	if (rcode != 1) {
    		return null;
    	}
    	return temp.correlationId;
    }
}// end of MQWrapper class
