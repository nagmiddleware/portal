package com.ams.tradeportal.servlet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class LogLevelServlet
 */
public class LogLevelServlet extends HttpServlet {
	private static final Logger LOG = LoggerFactory
			.getLogger(LogLevelServlet.class);
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public LogLevelServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		doTheWork(request, response);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doTheWork(request, response);
	}

	public void doTheWork(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		String packageName = request.getParameter("Package");
		String logLevel = request.getParameter("Level");
		boolean loaded = false;
		
		try {
			if(Class.forName(packageName)!=null){
				loaded = true;
			}
		} catch (Exception ex) {
			try {
				if(Package.getPackage(packageName)!=null){
					loaded = true;
				}
			} catch (Exception e) {
				response.sendError(response.SC_INTERNAL_SERVER_ERROR,
						"Provided package/class is not found");
			}
		}finally {
			if(loaded){
				org.apache.log4j.Logger logger4j = org.apache.log4j.Logger
						.getLogger(packageName);
				logger4j.setLevel(org.apache.log4j.Level.toLevel(logLevel));
				out.println("Log level changed successfully");
			}else{
				response.sendError(response.SC_INTERNAL_SERVER_ERROR,
						"Provided package/class is not found");
			}
			
			out.close();
		}
		

	}

}
