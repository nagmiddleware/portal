package com.ams.tradeportal.servlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.PropertyResourceBundle;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.resource.ResourceException;
import javax.resource.cci.ConnectionFactory;
import javax.resource.cci.Interaction;
import javax.resource.cci.MappedRecord;
import javax.resource.cci.RecordFactory;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.ams.tradeportal.common.ImagingIdAndPassword;
import com.ams.tradeportal.common.ImagingServices;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsConstants;
import com.amsinc.ecsg.util.EncryptDecrypt;
import com.filenet.is.ra.cci.FN_IS_CciConnectionSpec;
import com.filenet.is.ra.cci.FN_IS_CciInteractionSpec;

/**
 * The GetImageServlet is used by TPS JBoss server 
 * to retrieve images from FileNet server.  This allows the JBoss to 
 * access the images without installing the ISRA.
 * 
 * It is mapped to URL /portal/internal/GetImageServlet so that it is not accessible
 * externally via the Apache server.  It can only be accessed internally within CGI
 *
 *     Copyright  2015
 *     CGI, Incorporated
 *     All rights reserved
 *
 */
public class GetImageServlet extends HttpServlet {
private static final Logger LOG = LoggerFactory.getLogger(GetImageServlet.class);

    public static final int DEFAULT_GET_IMAGE_MAX_ATTEMPTS = 3;

    //codes to indicate result of getImage() method
    private static final int GET_IMAGE_SUCCESS = 0;
    private static final int GET_IMAGE_VIEW_ERROR = 1;
    private static final int GET_IMAGE_HASH_ERROR = 2;
    private StringBuffer error = new StringBuffer();
   

    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doTheWork(request, response);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doTheWork(request, response);
    }

    public void doTheWork(HttpServletRequest request, HttpServletResponse response)
            throws ServletException,IOException {
        long startTime = System.currentTimeMillis();
        PropertyResourceBundle servletProperties = (PropertyResourceBundle)
            PropertyResourceBundle.getBundle("TradePortal");

        boolean success = false;
	String docId =request.getParameter("doc_id");
        String imageHash = request.getParameter("hash");
		
        String pageNumber = request.getParameter(TradePortalConstants.PAGE_NUMBER);

        int maxNumberOfAttempts = DEFAULT_GET_IMAGE_MAX_ATTEMPTS;
        try {
            String tmp = servletProperties.getString("getImageMaxNumberOfAttempts");
            maxNumberOfAttempts = Integer.parseInt(tmp);
        } catch (Exception ex) {
            //do nothing just attempt once
            maxNumberOfAttempts = DEFAULT_GET_IMAGE_MAX_ATTEMPTS;
        }
        //
        int attemptReturn = 0;
        for (int attempt = 0; attempt < maxNumberOfAttempts; attempt++) {
                 attemptReturn = getImage(
                                docId, pageNumber, imageHash, "",
                                attempt+1, startTime, 
                                response );
                if ( attemptReturn == GET_IMAGE_SUCCESS ) {
                        success = true;
                        break;
                }
        }

		
        if ( !success ) {
                response.setContentType("text/plain; charset=UTF-8");
	    	ServletOutputStream sos = response.getOutputStream();
	    	String errorMessage = "";
                errorMessage = 
                    "The image you are attempting to view is not available. Error Code = " + String.valueOf(attemptReturn) ;
			sos.write(errorMessage.getBytes());
	        sos.flush();
	        sos.close();
		}
		
    }

    
    // created independent method to get the image to allow for retry logic
    /**
     * Attempt to get the image.
     * If successful, sets the image content on the http response, including setting content type.
     * If error, writes the specific error to the log and returns an error indicator.
     * To simply this method, it returns the following enumeration types:
     *   0 - success
     *   1 - view error
     *   2 - hash error
     */
    private int getImage(String docId, String pageNumber, String imageHash, String fromWhere,
            int attempt, long startTime,
            HttpServletResponse response) {
        int returnCode = GET_IMAGE_VIEW_ERROR; //default
        //cquinton 11/29/2011 Rel 7.1 close the connection
        javax.resource.cci.Connection connection = null;
        try {
            PropertyResourceBundle servletProperties = (PropertyResourceBundle)
                PropertyResourceBundle.getBundle("TradePortal");
            Context context = new InitialContext();
            javax.resource.cci.ConnectionFactory connectionFactory = (ConnectionFactory)
                context.lookup(servletProperties.getString(TradePortalConstants.ISRA_JNDI));

            ImagingIdAndPassword idandpass = ImagingServices.getNextIdAndPassword();
            FN_IS_CciConnectionSpec connectionSpec = new FN_IS_CciConnectionSpec(idandpass.getId(),idandpass.getPassword(),0);
            //cquinton 11/29/2011 Rel 7.1 close the connection
            connection = connectionFactory.getConnection(connectionSpec);

            if (connection != null)
            {
                Interaction interaction = connection.createInteraction();
                FN_IS_CciInteractionSpec interactionSpec = new FN_IS_CciInteractionSpec();
                RecordFactory recordFactory = connectionFactory.getRecordFactory();


                interactionSpec.setFunctionName("GetDocumentContent");

                MappedRecord inputRecord = recordFactory.createMappedRecord("DocContentRequest");
                inputRecord.put(TradePortalConstants.DOCUMENT_ID, Long.valueOf(docId));
                inputRecord.put(TradePortalConstants.PAGE_NUMBER, Integer.valueOf(pageNumber));

                MappedRecord outputRecord = (MappedRecord) interaction.execute(interactionSpec, inputRecord);

                String fileName = (String) outputRecord.get("file_name");
                String mimeType = (String) outputRecord.get("mime_type");
                InputStream inputStream = (InputStream)outputRecord.get("stream");
                LOG.info("GetImageServlet mimeType ->" + mimeType);


                    //get data from the stream
                    int availableBytes = inputStream.available();
                    byte[] data = new byte[availableBytes] ;
                    inputStream.read(data);
                    inputStream.close();

                    //after reading the image and before writing out, compare the hash
                    if ( validateImageHash( data, imageHash ) ) {
                        //setup the response
                        response.setContentType(mimeType);
                        if ( "application/octet-stream".equals( mimeType) ) {
                            response.setContentType("application/pdf");
                        }
                        response.encodeURL(fileName);
                        response.setContentLength(availableBytes);
                        response.addHeader("Content-disposition", "filename=" + response.encodeURL(fileName) + ";");

                        ServletOutputStream sos = response.getOutputStream();

                        sos.write(data);
                        sos.flush();
                        sos.close();

                        long stopTime = System.currentTimeMillis();
                        LOG.info("GetImageServlet Doc id " + docId + " served in " + (stopTime - startTime) + " ms." +
                            " ContentType=" + response.getContentType() +
                            " Attempt=" + attempt );
                        returnCode = GET_IMAGE_SUCCESS;
                    } else {
                        //hash comparison failed!
                        LOG.info("GetImageServlet: Hash comparison failure. " +
                            "Doc id " + docId +
                            " Attempt=" + attempt );
                        returnCode = GET_IMAGE_HASH_ERROR;
                    }
            }
            else {
                LOG.info("GetImageServlet: Connection null. " +
                    "Doc id " + docId +
                    " Attempt=" + attempt );
                returnCode = GET_IMAGE_VIEW_ERROR;
            }
        }
        catch(ResourceException re){
            //cquinton 11/2/2011 Rel 7.1 start - catch special condition of record not found.
            // print out an error, but no need to do a stack trace...
            if ( "FN_IS_RA_10363".equalsIgnoreCase( re.getErrorCode() ) ) {
                LOG.info("GetImageServlet: Document ID " +
                    docId + " not found!" +
                    " Attempt=" + attempt +
                    " " + re.getMessage() );
            } else {
                LOG.info("GetImageServlet: Resource Exception: " +
                    "Doc id " + docId +
                    " Attempt=" + attempt + " " +
                    " " + re.getMessage() );
                re.printStackTrace();
            }
            //cquinton 11/2/2011 Rel 7.1 end
            returnCode = GET_IMAGE_VIEW_ERROR;
        }
        catch(NamingException ne){
            LOG.info("GetImageServlet: Naming Exception: " +
                "Doc id " + docId +
                " Attempt=" + attempt + " " +
                " " + ne.getMessage() );
            ne.printStackTrace();
            returnCode = GET_IMAGE_VIEW_ERROR;
        }
        catch(Exception e){
            LOG.info("GetImageServlet: Exception: " +
                "Doc id " + docId +
                " Attempt=" + attempt + " " +
                " " + e.getMessage() );
            e.printStackTrace();
            returnCode = GET_IMAGE_VIEW_ERROR;
        }
        //cquinton 11/29/2011 Rel 7.1 close the connection
        finally {
            if ( connection!=null ) {
                try {
                    connection.close();
                    connection=null;
                }
                catch(Exception e){
                    LOG.info("GetImageServlet: Problem closing connection: " +
                        " " + e.toString() );
                    connection=null;
                }
            }
        }

        return returnCode;
    }


	/**
	 * Validate the image hash.
	 *
	 * @param data - the image as a byte array - the byte array
	 *               length must be the actual length of the image
	 * @param compareHash
	 * @return
	 */
    private boolean validateImageHash( byte[] data, String compareHash ) {
        boolean isValid = false;

        if ( TradePortalConstants.NO_HASH.equals(compareHash) ) {
            //this is a special value to signify null hash value pulled from db
            //we don't use a blank hash value because its sent as a parm
            // and if we allowed that a request without a 'hash' parm
            // would be allowed to view images - its a simple security mechanism
            //a null hash value in the database means the image was stored
            // pre hash implementation, so allow it
            isValid = true;
        } else {
            if ( compareHash != null && compareHash.length() > 0 ) {
                //attempt to compare
                //use image length as the secret key
                String hashKey = String.valueOf(data.length);
                try {
                    byte[] keyBytes = hashKey.getBytes( AmsConstants.UTF8_ENCODING );
                    //the hash result will be exactly 128bits becuase its md5
                    //this is 16 bytes
                    byte[] hashValueBytes =
                        EncryptDecrypt.createHmacMD5Hash( data, keyBytes );
                    //the following converts the hash bytes as a hex string
                    //it doubles the length using the left shift operator
                    //because each byte is represented by 2 characters -
                    // which means that calculated hash is always 32 characters long
                    String calculatedHash = EncryptDecrypt.bytesToHexString( hashValueBytes );

                    if (compareHash.equals(calculatedHash)) { //note calculatedHash could be null!
                        //all is good
                        isValid = true;
                    }
                } catch ( Exception ex ) {
                    System.err.println("GetImageServlet: Problem validating image hash: " + ex.getMessage() );
                    ex.printStackTrace();
                }
            }
        }
        return isValid;
    }

    
}
