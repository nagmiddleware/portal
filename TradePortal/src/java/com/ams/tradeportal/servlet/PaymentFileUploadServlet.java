package com.ams.tradeportal.servlet;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.PushbackInputStream;
import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.util.Iterator;
import java.util.List;

import javax.crypto.SecretKey;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadBase.SizeLimitExceededException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.busobj.PaymentFileUpload;
import com.ams.tradeportal.busobj.webbean.SessionWebBean;
import com.ams.tradeportal.common.AbstractPaymentFileDetail;
import com.ams.tradeportal.common.ConfigSettingManager;
import com.ams.tradeportal.common.PaymentFileDetails;
import com.ams.tradeportal.common.PaymentFileProcessor;
import com.ams.tradeportal.common.PaymentFileWithInvoiceDetails; // Manohar - CR 597 added
import com.ams.tradeportal.common.TradePortalConstants;
import com.ams.tradeportal.common.TransactionType;
import com.amsinc.ecsg.frame.AmsConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.ClientServerDataBridge;
import com.amsinc.ecsg.frame.EJBObjectFactory;
import com.amsinc.ecsg.frame.ErrorManager;
import com.amsinc.ecsg.frame.MediatorServices;
import com.amsinc.ecsg.frame.NavigationManager;
import com.amsinc.ecsg.util.DateTimeUtility;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.EncryptDecrypt;
import com.amsinc.ecsg.util.JPylonProperties;
import com.amsinc.ecsg.util.ResourceManager;
import com.amsinc.ecsg.util.StringFunction;
import com.amsinc.ecsg.web.FormManager;
/*
 * @(#)PaymentFileUploadServlet
 *
 */
import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;




/**
 * The PaymentFileUploadServlet class is used when a user
 * uploads a Payment data file to the Trade Portal for processing.
 *
 * CR-507 12/2009 - Payment Transaction Enhancement
 * Release 5.2.0.0
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 *
 */

/**
 *
 *  Payment File Format - pipe-delimited
 */

public class PaymentFileUploadServlet extends HttpServlet {
private static final Logger LOG = LoggerFactory.getLogger(PaymentFileUploadServlet.class);

	private static int BOM_SIZE = 3;
	private static final long MAX_FILE_SIZE = Long
			.parseLong(TradePortalConstants.getPropertyValue("TradePortal",
					"uploadFileMaxSize", "23068672")); // manohar 3/10/2010 - CR597 updated form 3.5MB to 4.5MB; 4/5/2011 - updated to 10MB; 6/14/2011 -CR-597 Scope Change - updated to 20MB
	private static final String MAX_FILE_SIZE_IN_MB = getMaxFileSizeInMB(MAX_FILE_SIZE);
	//cquinton 2/18/2011 Rel 6.1.0 ir ncul021753695 add max benes. default max is 10,000
	private static final long MAX_FILE_BENES = Long.parseLong(TradePortalConstants.getPropertyValue("TradePortal", "uploadFileMaxBenes", "10000"));
	private static final long MAX_PAY_DEF_BASED_FILE_SIZE = Long.parseLong("10485760"); // added new limit for CR 599 based payment files.
	private static final String MAX_PAY_DEF_BASED_FILE_SIZE_IN_MB = getMaxFileSizeInMB(MAX_PAY_DEF_BASED_FILE_SIZE);
	// IR SEUL041149384 Rel7.0.0.0 - 04/18/2011	

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doTheWork(request, response);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doTheWork(request, response);
	}

	/**
	 * This method implements the standard servlet API for GET and POST
	 * requests. It handles uploading of files to the Trade Portal.
	 *
	 * @param      javax.servlet.http.HttpServletRequest  request  - the Http servlet request object
	 * @param      javax.servlet.http.HttpServletResponse response - the Http servlet response object
	 * @exception  javax.servlet.ServletException
	 * @exception  java.io.IOException
	 * @return     void
	 * @throws AmsException
	 */

	public void doTheWork(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    	//T36000005819 W Zhu 9/24/12 Do not allow direct access to the servlet.
    	if (NavigationManager.bookmarkedAccessDenied(request, response, this)) {
    		return;
    	}
        

        
		// Retrieve the form manager and resource manager from the session so that
		// we can issue errors or create an payment upload record if necessary
		String fileName = null;
		HttpSession session = request.getSession(true);
		ResourceManager resourceManager = (ResourceManager) session.getAttribute("resMgr");
		FormManager formManager = (FormManager) session.getAttribute("formMgr");
		SessionWebBean userSession = (SessionWebBean) session.getAttribute("userSession");



		// Retrieve all form data, including the file contents
		MediatorServices mediatorServices = new MediatorServices();
		try {
			long t1 = System.currentTimeMillis();

			mediatorServices.getErrorManager().setLocaleName(userSession.getUserLocale());

			if (!ServletFileUpload.isMultipartContent(request)){
				throw new RuntimeException("Request does not have multipart....");
			}

			// Create a factory for disk-based file items
			DiskFileItemFactory factory = new DiskFileItemFactory();
			//factory.setSizeThreshold(100000);


			// Create a new file upload handler
			ServletFileUpload upload = new ServletFileUpload(factory);
			upload.setSizeMax(MAX_FILE_SIZE);


			// Parse the request
			LOG.debug("Before parsing request..");
			List items = upload.parseRequest(request);
			LOG.debug("After parsing request..");

			long t2 = System.currentTimeMillis();

			// Process the uploaded items
                            String payDefOid = null;
                            SecretKey secretKey = userSession.getSecretKey();

				Iterator iter = items.iterator();
				FileItem payFileItem = null;  
			    String formDataNi="";				
				while (iter.hasNext()) {
					FileItem item = (FileItem) iter.next();
					if (!item.isFormField()) {
						fileName=FilenameUtils.getName(item.getName());
                                                payFileItem = item;
					}
                                        else {
						String name = item.getFieldName();
						String value = item.getString();
						if (name.startsWith("paymentDefinitionOid")
								&& value != null && value.length() > 0) {
							payDefOid = EncryptDecrypt.decryptStringUsingTripleDes(value, secretKey);
						}
                        if (name.startsWith("paymentDefinitionOid") && StringFunction.isBlank(value)){
                            try {
                                mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.NO_PAYMENT_DEFINITION_IDENTIFIED);
                                handleError(mediatorServices, formManager);
                                return;
                            } catch (AmsException e1) {

                                e1.printStackTrace();
                            }

                        }
						if (name.startsWith("ni") && StringFunction.isNotBlank(value)) {
							formDataNi = value;
						}                        
                    }
				}
				//csrf check
				//compare form data ni to the one in hashtable in formManager
				//
			    if (formManager == null) {
					throw new RuntimeException("formManager is NULL");				    	
			    }else{
			      if (StringFunction.isNotBlank(formDataNi)){
			        formManager.getServletInvocation (formDataNi,request); 
			      }else{
			    	  throw new RuntimeException("form ni value is missing");
			      }
			    }				
				if (StringFunction.isNotBlank(payDefOid)){ 
	                if (payFileItem != null && payFileItem.getSize() > MAX_PAY_DEF_BASED_FILE_SIZE){
	        			try {
	        				mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.PAYMENT_FILE_SIZE_EXCEEDED,MAX_PAY_DEF_BASED_FILE_SIZE_IN_MB +"");
	        				handleError(mediatorServices, formManager);
	        				return;
	        			} catch (AmsException e1) {
	        				LOG.info("PaymentFileUploadServlet: Error occured during upload, file:'" + fileName +"' Reason:" + e1.getMessage());
	        				e1.printStackTrace();
	        				throw new ServletException(e1);
	        			}
	
	                }
				}
			try {
								if(payFileItem!=null)
                                processUploadedFile(payFileItem,payDefOid,resourceManager, userSession, mediatorServices);

			} catch (AmsException e) {
				// TODO Auto-generated catch block
				LOG.info("PaymentFileUploadServlet: Error occured during upload, file:'" + fileName +"' Reason:" + e.getMessage());
				e.printStackTrace();

			}

			long t3 = System.currentTimeMillis();

			try {
				handleError(mediatorServices, formManager);
			} catch (AmsException e) {
				// TODO Auto-generated catch block
				//e.printStackTrace();
				throw new ServletException(e);
			}
			return;

		}catch (SizeLimitExceededException ex) {
			try {
				mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.PAYMENT_FILE_SIZE_EXCEEDED,MAX_FILE_SIZE_IN_MB +"");
				handleError(mediatorServices, formManager);
			} catch (AmsException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			LOG.info("PaymentFileUploadServlet: Error occured during upload, file:'" + fileName +"' Reason:" + ex.getMessage());
			ex.printStackTrace();
			throw new ServletException(ex);
		}
		catch (Exception e) {
			try {
				mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.ERR_SAVING_PAYMENTUPLOAD_FILE);
				handleError(mediatorServices, formManager);
			} catch (AmsException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			LOG.info("PaymentFileUploadServlet: Error occured during upload, file:'" + fileName +"' Reason:" + e.getMessage());
			e.printStackTrace();

			throw new ServletException(e);
		}
	}


	protected void handleError (MediatorServices mediatorServices,FormManager formManager) throws AmsException, ServletException, IOException {

		LOG.debug("Inside hanldeError..");
		DocumentHandler xmlDoc = new DocumentHandler();

		mediatorServices.addErrorInfo();
		xmlDoc.setComponent("/Error", mediatorServices.getErrorDoc());
		xmlDoc.setAttribute("/In/HasErrors", TradePortalConstants.INDICATOR_YES);
		formManager.storeInDocCache("default.doc", xmlDoc);

	}

	public void processUploadedFile(FileItem item, String payDefOid, ResourceManager resourceManager,SessionWebBean userSession, MediatorServices mediatorServices) throws AmsException   {


		PaymentFileUpload paymentfile = null;
		File file = null;
		String fileName=FilenameUtils.getName(item.getName());

		//BSL IR PAUL052054282 05/25/11 BEGIN - Delete the Content Type check
		
		//cquinton 2/21/2011 ir#psul022135406 - change maximum file length to 40 chars
		if (fileName.length() > AbstractPaymentFileDetail.MAX_FILE_NAME_SIZE) {
			mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
			        TradePortalConstants.PAYMENT_FILENAME_SIZE_EXCEEDED);
			return;
		}


		try  {

			paymentfile = createPaymentFileUpload();
			file = getTempFile(paymentfile);

			if (writetoDisk(item,file,payDefOid,resourceManager, userSession, mediatorServices)){
				paymentfile.setAttribute("validation_status", TradePortalConstants.PYMT_UPLOAD_VALIDATION_PENDING);
				paymentfile.setAttribute("payment_file_name", fileName);
				paymentfile.setAttribute("creation_timestamp", DateTimeUtility.getGMTDateTime(true,false));
				paymentfile.setAttribute("user_oid", userSession.getUserOid());
				paymentfile.setAttribute("owner_org_oid", userSession.getOwnerOrgOid());
                                paymentfile.setAttribute("payment_definition_oid", payDefOid);
                                paymentfile.setAttribute("system_name", "MIDDLEWARE");
                                paymentfile.setAttribute("server_name", userSession.getServerName());
				if (paymentfile.save() != 1) {
					throw new RuntimeException("error saving PaymentFileUpload");
				}
			}else {
				cleanup(file);
			}
		}
		catch(Exception e){
			LOG.info("PaymentFileUploadServlet: Error occured during upload, file:'" + fileName +"' Reason:" + e.getMessage());
			e.printStackTrace();
			cleanup(file);
			if (mediatorServices.getErrorManager().getIssuedErrorsList().size() ==0) {
				mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.ERR_SAVING_PAYMENTUPLOAD_FILE);
			}
		}
		//cquinton 5/16/2011 Rel 7.0.0 perf cleanup ejb reference
		finally {
		    if (paymentfile!=null) {
		        try {
		            paymentfile.remove();
		        } catch (Exception ex) {}
		    }
		}
	}


	protected PaymentFileUpload createPaymentFileUpload() throws AmsException, RemoteException {
		LOG.debug("Creating PaymentFileUplaod..");
		String serverLocation = JPylonProperties.getInstance().getString("serverLocation");
		PaymentFileUpload paymentfile = (PaymentFileUpload)EJBObjectFactory.createClientEJB(serverLocation, "PaymentFileUpload");
		paymentfile.newObject();
		return paymentfile;
	}



	/**
	 * This method returns the encoding type by checking the Byte Order Marker
	 *
	 * @param      byte[] - byte array
	 * @return     String
	 * @throws IOException
	 */
	private String getEncoding(PushbackInputStream is) throws IOException  {
		String encoding = "ASCII";
		int read=0;
		byte bom[] = new byte[BOM_SIZE];


		is.read(bom, 0, bom.length);


		if ((bom[0] == (byte)0xFF) && (bom[1] == (byte)0xFE )) {
			encoding = "UTF-16LE";
			read=2;
		}
		else if ((bom[0] == (byte)0xFE) && (bom[1] == (byte)0xFF )) {
			encoding = "UTF-16BE";
			read=2;
		}
		else if ((bom[0] == (byte)0xEF) && (bom[1] == (byte)0xBB ) && (bom[2] == (byte)0xBF )) {
			encoding = "UTF-8";
			read=3;
		}
		is.unread(bom,read,BOM_SIZE-read);
		return encoding;
	}

	/**
	 * This method returns the Uploaded Payment file
	 *
	 * @param      PaymentFileUpload -
	 * @return     File
	 * @throws RemoteException
	 * @throws AMSException
	 *
	 */
	protected File getTempFile(PaymentFileUpload paymentfile) throws RemoteException, AmsException {
		LOG.debug("Getting temporary file..");
		File file = null;
		String fileName = paymentfile.getAttribute("payment_file_upload_oid");
		String folder=TradePortalConstants.getPropertyValue("AgentConfiguration", "uploadFolder", "c:/temp/");
		String fname = folder + fileName + PaymentFileDetails.FILE_EXTENSION;

		file = new File(fname);

		LOG.debug("Getting temporary file. Filename: " + file.getAbsolutePath());

		return file;
	}


	/**
	 * This method validates the uploaded file and writes to temporary folder
	 *
	 * @param item
	 * @param file
	 * @param paymentfile
	 * @param resourceManager
	 * @param userSession
	 * @param mediatorServices
	 * @return
	 * @throws Exception
	 */
	public boolean writetoDisk(FileItem item, File file, String payDefOid, ResourceManager resourceManager,SessionWebBean userSession, MediatorServices mediatorServices) throws Exception {

		boolean success = true;
		String confidentialInd = null;
		String paymentMethod = null;
		String currency = null; //cquinton 2/7/2011 Rel 6.1.0 ir#haul020365676
		boolean hasError =false;

		BufferedWriter fs = null;
		BufferedReader reader = null;
		//CR-921
		String fxContractNumber=null;
		String fxRate=null;

		try{
			LOG.debug("Writing file to disk...");
			InputStream is = item.getInputStream();
			PushbackInputStream pis = new PushbackInputStream(is,BOM_SIZE);

			InputStreamReader isr =	new InputStreamReader(pis,getEncoding(pis));
			reader = new BufferedReader(isr);

			fs = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file),AmsConstants.UTF8_ENCODING));
                        
                        // W Zhu  10 Jan 2014 Rel 8.4 CR-599 For user defined definition, just save file and process later.
                        if (payDefOid !=null && payDefOid.length()> 0) {
                            long start = System.nanoTime();
                            String onlineValidation = ConfigSettingManager.getConfigSetting("Portal", "PaymentUpload", "AsynchronousValidationOnly", "N");
                            boolean errorFound = false;
                            if ("Y".equals(onlineValidation)) {
                                char[] buffer = new char[8000];
                                int readSize;
                                while ((readSize = reader.read(buffer)) != -1) {
                                    fs.write(buffer, 0, readSize);
                                }
                            }
                            else {
                                ClientServerDataBridge csdb = new ClientServerDataBridge();
				csdb.setLocaleName("en");
                                String serverLocation = JPylonProperties.getInstance().getString("serverLocation");
                                PaymentFileProcessor proc = new PaymentFileProcessor("Online", -1, serverLocation, csdb);
                                ErrorManager errorMgr = mediatorServices.getErrorManager();
                                proc.uploadPaymentFile(reader, fs, Long.valueOf(payDefOid), errorMgr, userSession);
                                if (errorMgr.getErrorCount() > 0) {
                                    errorFound = true;
                                } 
                            }
                            try {
                                    fs.close();
                            } catch (IOException e) {
                                    // ignore
                            }
                            try {
                                    reader.close();
                            } catch (IOException e) {
                                    // ignore
                            }
                            long end = System.nanoTime();
                            LOG.debug("PaymentFileUploadServlet time to save file (ns) = " + (end - start));
                            return !errorFound;
			}

			int ctr=0;
			String sLine = "";
			String debitAccountNo = null;
			String paymentCurrency = null;
			String executionDate = null;


			LOG.debug("Processing lines..");

			// Read the first line to determine if it's an Invoice file or not.
			sLine = reader.readLine();
			boolean isInvoiceFile = sLine!=null?sLine.startsWith("HDR"):false;
			String[] headerFields = null;
			String[] payLines = null;
			String[] c = null;
			if(isInvoiceFile){
				// Parse the header line then get the next line from the file.
				headerFields = PaymentFileWithInvoiceDetails.parseHeaderLine(sLine);
				fs.write(sLine);
				fs.newLine();
				sLine = reader.readLine();
			}

			while (( !hasError && (reader != null) && (sLine != null))) {

				if (StringFunction.isBlank(sLine)){
					sLine = reader.readLine();
					continue;
				}
				// manohar - CR 597 starts
				String[] record = null;
				if (isInvoiceFile) {
					// The firstline should begin with 'PAY', then keep reading lines while additional lines are 'INV'
					if(!sLine.startsWith(TransactionType.PAYMENT)){
						String[] substitutionValues = {"",""};
						substitutionValues[0] = TradePortalConstants.PAYMENT_LINE;
						substitutionValues[1] = TransactionType.PAYMENT;
						mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.INVALID_PREFIX, substitutionValues, null);

						hasError = true;
						break;
					}
					payLines = new String[1001];
					int lineIndex = 0;
					payLines[lineIndex] = sLine;
					sLine = reader.readLine();
					while(sLine != null && sLine.startsWith(TradePortalConstants.INVOICE)){
						// IR SEUL041149384 Rel7.0.0.0 - 04/18/2011
						if(sLine.length() > PaymentFileWithInvoiceDetails.MAX_INV_DTL_CHAR){
							mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.INV_CHAR_EXCEEDED);
							hasError = true;
                            break;
						}
						// IR SEUL041149384 Rel7.0.0.0 - 04/18/2011
						 if(lineIndex >= PaymentFileWithInvoiceDetails.MAX_INV_LINES){ // manohar - SEUL041149384 - to check max of 100 invoice details; 6/14/2011- CR-597 Scope Change - updated to 1000 invoice details
                           mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.INV_LINE_EXCEEDED);
							 hasError = true;
                           break;
						 }

						lineIndex++;
						payLines[lineIndex] = sLine;
						sLine = reader.readLine();
						// manohar - IR SBUL040767216 Rel7.0.0.0 - 05/18/2011 -Begin
						if(null !=sLine && !(sLine.startsWith(TransactionType.PAYMENT)||sLine.startsWith(TradePortalConstants.INVOICE))){
							String[] substitutionValues = {"",""};
							substitutionValues[0] = TradePortalConstants.INVOICE_LINE;
							substitutionValues[1] = TradePortalConstants.INVOICE;
							mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.INVALID_PREFIX, substitutionValues, null);
							hasError = true;
							break;
						}// manohar - IR SBUL040767216 Rel7.0.0.0 - 05/18/2011-End
					}
					record = PaymentFileWithInvoiceDetails.parsePaymentRecord(headerFields, payLines);

				} else { // manohar - CR 597 end					
					record = PaymentFileDetails
					.parsePaymentLineItem(sLine, PaymentFileDetails.MAX_PAYMENT_FILE_FIELDS_NUMBER);
				}
				// manohar - CR 597 starts
				if (isInvoiceFile) {
					debitAccountNo = record[PaymentFileWithInvoiceDetails.DEBIT_ACCOUNT_NO];
					paymentCurrency = record[PaymentFileWithInvoiceDetails.PAYMENT_CURRENCY];
					paymentMethod = record[PaymentFileWithInvoiceDetails.PAYMENT_METHOD];
					if (PaymentFileWithInvoiceDetails.validatePaymentLineItemReqFields(paymentMethod, paymentCurrency, debitAccountNo,ctr, resourceManager,
							mediatorServices)) {
						hasError = true;
						break;
					}
					//manohar - SBUL040548203 - 04/14/2011 - begin
					executionDate= record[PaymentFileWithInvoiceDetails.EXECUTION_DATE];
					if (PaymentFileWithInvoiceDetails.validateExecutionDate(executionDate, ctr, resourceManager, mediatorServices)) {
						hasError = true;
						break;
					}//manohar - SBUL040548203 - 04/14/2011 - end
					if (PaymentFileWithInvoiceDetails.validatePaymentLineItem(
							record, ctr, resourceManager,
							mediatorServices)) {
						hasError = true;
						break;
					}
				} else {
					debitAccountNo = record[PaymentFileDetails.DEBIT_ACCOUNT_NO];
					paymentCurrency = record[PaymentFileDetails.PAYMENT_CURRENCY];
					paymentMethod = record[PaymentFileDetails.PAYMENT_METHOD];
					if (PaymentFileDetails.validatePaymentLineItemReqFields(paymentMethod, paymentCurrency, debitAccountNo,ctr, resourceManager,
							mediatorServices)) {
						hasError = true;
						break;
					}// manohar - CR 597 end
					if (PaymentFileDetails.validatePaymentLineItem(record, ctr,
							resourceManager, userSession, mediatorServices)) {
						hasError = true;
						break;
					}
				}

				if (ctr == 0) {
					// manohar - CR 597 starts
					if (isInvoiceFile) {
						currency = record[PaymentFileWithInvoiceDetails.PAYMENT_CURRENCY];
						confidentialInd = record[PaymentFileWithInvoiceDetails.CONFIDENTIAL_INDICATOR];
						//CR-921 
						//Initialize FX Contract Number and Rate Fixed File
						fxContractNumber = record[PaymentFileWithInvoiceDetails.FX_CONTRACT_NUMBER];
						fxRate = record[PaymentFileWithInvoiceDetails.FX_RATE];
						hasError = PaymentFileWithInvoiceDetails
								.validateHeader(debitAccountNo,paymentCurrency,paymentMethod,currency,confidentialInd, executionDate, ctr,
										resourceManager, userSession,
										mediatorServices, fxContractNumber, fxRate);//CR-921
						if(!hasError){
							hasError = PaymentFileWithInvoiceDetails.validatePaymentCurrencyCountry(mediatorServices, record);
						}
					}else { 
						currency = record[PaymentFileDetails.PAYMENT_CURRENCY];
						confidentialInd = record[PaymentFileDetails.CONFIDENTIAL_INDICATOR];
						executionDate= record[PaymentFileDetails.EXECUTION_DATE];
						//CR-921
						fxContractNumber = record[PaymentFileDetails.FX_CONTRACT_NUMBER];
						fxRate = record[PaymentFileDetails.FX_RATE];						
						hasError = PaymentFileDetails
								.validateHeader(debitAccountNo,paymentCurrency,paymentMethod,currency,confidentialInd,executionDate, ctr,
										resourceManager, userSession,
										mediatorServices, fxContractNumber, fxRate); //CR-921
						// manohar - CR 597 end						
					}

				} else {
					// cquinton 2/18/2011 - if over the maximum number of
					// beneficiaries throw an error
					if ((ctr + 1) > MAX_FILE_BENES) {
						mediatorServices
								.getErrorManager()
								.issueError(
										TradePortalConstants.ERR_CAT_1,
										TradePortalConstants.PAYMENT_FILE_BENES_EXCEEDED);
						hasError = true;
					}

					// cquinton 2/4/2011 Rel 6.1.0 ir#hhul020368424 start
					// no longer check that debit account number is the same for
					// each beneficiary in the payment file. just take the first
					// one					
					// cquinton 2/4/2011 Rel 6.1.0 ir#hhul020368424 end
					// manohar - CR 597 starts
					if (isInvoiceFile) {
						if (!paymentMethod
								.equals(record[PaymentFileWithInvoiceDetails.PAYMENT_METHOD])) {
							mediatorServices
									.getErrorManager()
									.issueError(
											TradePortalConstants.ERR_CAT_1,
											TradePortalConstants.PAY_MTHD_NOT_SAME_FOR_BENE);
							hasError = true;
						}
					}else{// manohar - CR 597 end
						if (!paymentMethod
								.equals(record[PaymentFileDetails.PAYMENT_METHOD])) {
							mediatorServices
									.getErrorManager()
									.issueError(
											TradePortalConstants.ERR_CAT_1,
											TradePortalConstants.PAY_MTHD_NOT_SAME_FOR_BENE);
							hasError = true;
						}
					}

					// cquinton 2/7/2011 Rel 6.1.0 ir#haul020365676 start
					// validate currency on upload
					// manohar - CR 597 starts
					if (isInvoiceFile) {
						if (!currency
								.equals(record[PaymentFileWithInvoiceDetails.PAYMENT_CURRENCY])) {
							mediatorServices
									.getErrorManager()
									.issueError(
											TradePortalConstants.ERR_CAT_1,
											TradePortalConstants.PAY_CURRENCY_NOT_SAME_FOR_BENE);
							hasError = true;
						}
					}else{ // manohar - CR 597 end
						if (!currency
								.equals(record[PaymentFileDetails.PAYMENT_CURRENCY])) {
							mediatorServices
									.getErrorManager()
									.issueError(
											TradePortalConstants.ERR_CAT_1,
											TradePortalConstants.PAY_CURRENCY_NOT_SAME_FOR_BENE);
							hasError = true;
						}
					}
					// cquinton 2/7/2011 Rel 6.1.0 ir#haul020365676 end
				}

				if(isInvoiceFile){
					int i = 0;
					while(i <= PaymentFileWithInvoiceDetails.MAX_INV_LINES && payLines[i] != null){ // manohar - SEUL041149384 - to check max of 100 invoice details
						fs.write(payLines[i]);
						fs.newLine();
						i++;
					}
				}else{
					fs.write(sLine);
					fs.newLine();
					sLine = reader.readLine();
				}

				ctr++;
			}

			success = !hasError;
		} catch (RuntimeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw e;
		}
		finally {

			if (fs != null) {
				try {
					fs.close();
				} catch (IOException e) {
					// ignore
				}

			}

			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					// ignore
				}
			}

		}

		return success;
	}


	protected static String getMaxFileSizeInMB(long maxFileSize){
		float f = (float) (maxFileSize/(1024.0 * 1024.0));
		BigDecimal dec = new BigDecimal(f);
		dec = dec.setScale(1,BigDecimal.ROUND_DOWN);
		return dec.toString();

	}

	/**
	 * Performs cleanup
	 *
	 * @param file
	 *
	 * @return
	 */
	protected void cleanup(File file) {
		LOG.debug("Inside cleanup..");
		//delete file
		if (file !=null && !file.delete()) {

			LOG.info("Error: Could not delete file: " + file.getAbsoluteFile());
		}

	}



}
