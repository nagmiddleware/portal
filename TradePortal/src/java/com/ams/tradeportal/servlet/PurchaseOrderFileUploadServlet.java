package com.ams.tradeportal.servlet;
import java.io.IOException;
import java.rmi.RemoteException;
import java.util.Date;
import java.util.Hashtable;

import javax.crypto.SecretKey;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.busobj.webbean.SessionWebBean;
import com.ams.tradeportal.common.TPDateTimeUtility;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.ClientServerDataBridge;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.frame.EJBObjectFactory;
import com.amsinc.ecsg.frame.MediatorServices;
import com.amsinc.ecsg.frame.NavigationManager;
import com.amsinc.ecsg.frame.ObjectIDCache;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.EncryptDecrypt;
import com.amsinc.ecsg.util.JPylonProperties;
import com.amsinc.ecsg.util.ResourceManager;
import com.amsinc.ecsg.web.FormManager;
/*
 * @(#)PurchaseOrderFileUploadServlet
 *
 */
import com.ams.tradeportal.busobj.util.AutoLCLogger;
import com.ams.tradeportal.busobj.ActivePOUpload;

/**
 * The PurchaseOrderFileUploadServlet class is used when a user wishes to
 * upload a Purchase Order data file to the Trade Portal for processing.
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 *

 */

public class PurchaseOrderFileUploadServlet extends HttpServlet
{
private static final Logger LOG = LoggerFactory.getLogger(PurchaseOrderFileUploadServlet.class);
   public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
   {
      doTheWork(request, response);
   }

   public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
   {
      doTheWork(request, response);
   }

   /**
    * This method implements the standard servlet API for GET and POST
    * requests. It handles uploading of files to the Trade Portal. It
    * currently processes only PO Data file uploads.
    *

    * @param      javax.servlet.http.HttpServletRequest  request  - the Http servlet request object
    * @param      javax.servlet.http.HttpServletResponse response - the Http servlet response object
    * @exception  javax.servlet.ServletException
    * @exception  java.io.IOException
    * @return     void
    */
   public void doTheWork(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
   {
    	//T36000005819 W Zhu 9/24/12 Do not allow direct access to the servlet.
    	if (NavigationManager.bookmarkedAccessDenied(request, response, this)) {
    		return;
    	}
     
        
      RequestParser   requestParser     = new RequestParser();
  
      try
      {
          uploadPurchaseOrderData(request, response, requestParser);      // NSX CR-486 05/05/2010
      }
      catch (Exception ex)
      {
         LOG.debug("Exception in PurchaseOrderFileUploadServlet!");
         ex.printStackTrace();
      }
   }

   /**
    * This method handles the uploading of Purchase Order data to the Trade Portal.
    *

    * @param      javax.servlet.http.HttpServletRequest  request  - the Http servlet request object
    * @param      javax.servlet.http.HttpServletResponse response - the Http servlet response object
    * @param      java.util.Hashtable formData - all the data selected by the user for this request,
    *                                            including file path and contents
    * @exception  javax.servlet.ServletException
    * @exception  java.io.IOException
    * @exception  com.amsinc.ecsg.frame.AmsException
    * @exception  java.rmi.RemoteException
    * @return     void
    */
   public void uploadPurchaseOrderData(HttpServletRequest request, HttpServletResponse response, RequestParser requestParser)
                                       throws ServletException, IOException, AmsException, RemoteException
   {
	   String    poDataFileContents     = null;
	   Hashtable       formData          = null;
	  
      DocumentHandler          poUploadParametersDoc  = null;
      ResourceManager          resourceManager        = null;
      SessionWebBean           userSession            = null;
      FormManager              formManager            = null;
      HttpSession              session                = null;
      String                   poItemsGroupingOption  = null;
      String                   poUploadSequenceNumber = null;
      String                   poUploadDefinition     = null;
      String                   poDataFilePath         = null;
      String                   subsidiaryAccessOrgOid = null;
      String                   poDataOption           = null;
      String                   userOrgOid             = null;
      // Retrieve all form data, including the file contents
      formData = requestParser.parseRequest(request);   // NSX CR-486 05/05/2010

      String poUploadInstrOption = null;
      // First we need to retrieve the form manager and resource manager from the session so that
      // we can issue errors or create an active PO upload record if necessary
      session         = request.getSession(true);
      resourceManager = (ResourceManager) session.getAttribute("resMgr");
      formManager     = (FormManager)     session.getAttribute("formMgr");
      userSession     = (SessionWebBean)  session.getAttribute("userSession");

      // Get the user's organization oid
      userOrgOid = userSession.getOwnerOrgOid();

      // If the user's organization has active PO Data files currently uploading, issue an error and return
      if (hasActivePOUploads(formManager, userOrgOid, userSession.getUserLocale()))
      {
         return;
      }

      // Get the full path of the PO data file
      poDataFilePath = (String) formData.get("filePath");

      // Retrieve the PO data option (i.e., either group PO items using LC creation rules OR make
      // them available to be manually added to transactions)
      poDataOption = (String) formData.get("PODataOption");
      poItemsGroupingOption = (String) formData.get("POItemsGroupingOption");
      poUploadInstrOption = (String) formData.get("POUploadInstrOption");

      // Check to make sure a PO Upload Definition was selected from the dropdown list; if nothing
      // was selected, issue an error and return the user back to the page
      poUploadDefinition = (String) formData.get("POUploadDefinition");

      //In TPRefresh, 2 radio buttons have been changed to 1 checkbox. Incase of unchecked checkbox, no value comes in form data.
      //Hence in case of null,i.e, unchecked, default value is being assigned here.
      if(poItemsGroupingOption == null){
    	  poItemsGroupingOption = TradePortalConstants.USE_ONLY_FILE_PO_LINE_ITEMS;
      }else{
    	  poItemsGroupingOption = (String) formData.get("POItemsGroupingOption");
      }
      if (!isPOUploadDefinitionValid(formManager, poUploadDefinition, poDataOption, poItemsGroupingOption, poUploadInstrOption, userSession.getUserLocale()))
      {
         return;
      }

      // If user is using subsidiary access and is able to see their own reference data while doing so, include the org OID here
      if(userSession.showOrgDataUnderSubAccess())
          subsidiaryAccessOrgOid = userSession.getSavedUserSession().getOwnerOrgOid();

      // If we get here, a PO Upload Definition was selected; go ahead and decrypt it
	//	W Zhu 8/22/2012 Rel 8.1 T36000004579 add SecretKey parameter to allow different keys for each session for better security.
	SecretKey secretKey = ((SessionWebBean)request.getSession(false).getAttribute("userSession")).getSecretKey();
      poUploadDefinition = EncryptDecrypt.decryptStringUsingTripleDes(poUploadDefinition, secretKey);

      // Get the actual PO data file
      //poDataFileContents = (ByteArrayOutputStream) formData.get("fileContent");   // NSX CR-486 05/05/2010
      poDataFileContents = requestParser.getFileContentAsString();     // NSX CR-486 05/05/2010

      // Check to make sure a valid PO Data file was selected; if nothing was entered/selected,
      // if an invalid file path was entered, or if an invalid file was selected, issue an
      // appropriate error and return
      if (!isPODataFileValid(formManager, poDataFilePath, poDataFileContents, poUploadDefinition,
                             poDataOption, poItemsGroupingOption, poUploadInstrOption, userSession.getUserLocale()))
      {
         return;
      }


      // If the PO data option specified by the user is to group the PO items based on LC creation
      // rules, then get the grouping option that was selected as well
      if (poDataOption.equals(TradePortalConstants.GROUP_PO_LINE_ITEMS))
      {
         
         poUploadInstrOption = (String) formData.get("POUploadInstrOption");

         if (poUploadInstrOption == null || poUploadInstrOption.equals("") || poItemsGroupingOption == null || poItemsGroupingOption.equals("")) {
            issuePOUploadError(formManager, TradePortalConstants.AUTO_UPLOAD_INSTR_TYPE_MISSING, poUploadDefinition,
                            poDataOption, poItemsGroupingOption, poUploadInstrOption,userSession.getUserLocale());
             return;
	   }
        
      }


      // Retrieve the sequence number for this PO upload
      poUploadSequenceNumber = String.valueOf(ObjectIDCache.getInstance(TradePortalConstants.AUTOLC_SEQUENCE).generateObjectID());

      // Retrieve all necessary data for the PO Data parsing background process to run after
      // the upload has finished
      poUploadParametersDoc = getPOUploadParametersDoc(userSession, poUploadSequenceNumber, poUploadDefinition,
                                                       poDataOption, poItemsGroupingOption, poUploadInstrOption ,poDataFilePath, subsidiaryAccessOrgOid);

      // Save all data to the active PO upload table and write an appropriate message to the
      // log file
      saveActivePOUpload(resourceManager, formManager, userSession, poUploadSequenceNumber,
                         poDataFileContents, poUploadParametersDoc.toString());

      // Set the current page to go to after the file upload request has been processed
      formManager.setCurrPage("UploadsHome", true);
   }

   /**
    * This method determines whether or not the user's org currently has other
    * Purchase Orders being uploaded. If it does, the method will issue an
    * error message indicating this to the user.
    *

    * @param      com.amsinc.ecsg.web.FormManager formManager - the form manager object for the user's session
    * @param      java.lang.String userOrgOid - the user's organization oid
    * @param      java.lang.String localeName - used for error processing
    * @exception  com.amsinc.ecsg.frame.AmsException
    * @return     boolean - indicates whether or not the user's org has active PO's uploading
    *                       (true  - the user's org has active PO's uploading
    *                        false - the user's org does not have active PO's uploading)
    */
   private boolean hasActivePOUploads(FormManager formManager, String userOrgOid, String localeName) throws AmsException
   {

      // If the user's org currently has active PO's uploading, issue an appropriate error
      // message and set the next page to return to
      if (DatabaseQueryBean.getCount("active_po_upload_oid", "active_po_upload", "a_corp_org_oid = ? ", false, new Object []{userOrgOid}) != 0)
      {
         issuePOUploadError(formManager, TradePortalConstants.AUTOLC_PROCESS_RUNNING, localeName);

         // Set the current page to go to after the file upload request has been processed
         formManager.setCurrPage("UploadsHome", true);

         return true;
      }

      return false;
   }

   /**
    * This method determines whether or not a PO upload definition was selected
    * by the user. If one wasn't, the method will issue an error message
    * indicating this to the user.
    *

    * @param      com.amsinc.ecsg.web.FormManager formManager - the form manager object for the user's session
    * @param      java.lang.String poUploadDefinition - the oid of the PO upload definition if one was selected
    * @param      java.lang.String poDataOption - the PO data option that was selected
    * @param      java.lang.String poItemsGroupingOption - the PO items grouping option that was selected
    * @param      java.lang.String localeName - used for error processing
    * @exception  com.amsinc.ecsg.frame.AmsException
    * @return     boolean - indicates whether or not a PO upload definition was selected by the user
    *                       (true  - a PO upload definition was selected
    *                        false - a PO upload definition was not selected)
    */
   private boolean isPOUploadDefinitionValid(FormManager formManager, String poUploadDefinition, String poDataOption,
                                             String poItemsGroupingOption, String poUploadInstrOption,String localeName) throws AmsException
   {
      // If a PO upload definition wasn't selected, issue an error message indicating this to the user
      if ((poUploadDefinition == null) || (poUploadDefinition.equals("")))
      {
         issuePOUploadError(formManager, TradePortalConstants.SELECT_PO_DEFN_FOR_UPLOAD, poUploadDefinition,
                            poDataOption, poItemsGroupingOption, poUploadInstrOption ,localeName);

         // Set the current page to go to after the file upload request has been processed
         formManager.setCurrPage("UploadPOData", true);

         return false;
      }

      return true;
   }

   /**
    * This method determines whether or not a valid PO Data file was selected
    * by the user. If nothing was entered/selected, if an invalid file path
    * was entered, or if an invalid file was selected, the method will issue
    * an error message indicating this to the user.
    *

    * @param      com.amsinc.ecsg.web.FormManager formManager - the form manager object for the user's session
    * @param      java.lang.String poDataFilePath - the full path of the PO Data file if one was selected
    * @param      java.lang.String poDataFileContents - the contents of the PO Data file if one was selected
    * @param      java.lang.String poUploadDefinition - the oid of the PO upload definition if one was selected
    * @param      java.lang.String poDataOption - the PO data option that was selected
    * @param      java.lang.String poItemsGroupingOption - the PO items grouping option that was selected
    * @param      java.lang.String localeName - locale name used for error processing
    * @exception  com.amsinc.ecsg.frame.AmsException
    * @return     boolean - indicates whether or not a valid PO Data file was selected by the user
    *                       (true  - a valid PO Data file was selected
    *                        false - a valid PO Data file was not selected)
    */
   private boolean isPODataFileValid(FormManager formManager, String poDataFilePath, String poDataFileContents,
                                     String poUploadDefinition, String poDataOption, String poItemsGroupingOption, String poUploadInstrOption,
                                     String localeName)
                                     throws AmsException
   {
      // If nothing was entered/selected, if an invalid file path was entered, or if an
      // invalid file was selected, issue an error message indicating this to the user.
      if ((poDataFilePath.equals("")) || (poDataFileContents.trim().equals("")))
      {
         issuePOUploadError(formManager, TradePortalConstants.FILE_NOT_FOUND, poUploadDefinition, poDataOption,
                            poItemsGroupingOption, poUploadInstrOption, localeName);

         // Set the current page to go to after the file upload request has been processed
         formManager.setCurrPage("UploadPOData", true);

         return false;
      }

      return true;
   }

   /**
    * This method saves PO and user data to an XML document for a PO parsing
    * background process to pick up after the PO upload has finished.
    *

    * @param      com.ams.tradeportal.busobj.webbean.SessionWebBean userSession - the current user's Trade Portal session
    * @param      java.lang.String poUploadSequenceNumber - the upload sequence number for the current PO upload
    * @param      java.lang.String poUploadDefinition - the oid of the PO upload definition if one was selected
    * @param      java.lang.String poDataOption - the PO data option that was selected
    * @param      java.lang.String poItemsGroupingOption - the PO items grouping option that was selected
    * @param      java.lang.String poUploadInstrOption- the instrument type (LC, ATP, etc.) option that was selected
    * @param      java.lang.String poDataFilePath - the full path of the PO Data file if one was selected
    * @exception  com.amsinc.ecsg.frame.AmsException
    * @return     com.amsinc.ecsg.util.DocumentHandler - an XML document containing all necessary PO data
    *                                                    for the PO parsing background process to function
    */
   private DocumentHandler getPOUploadParametersDoc(SessionWebBean userSession, String poUploadSequenceNumber,
                                                    String poUploadDefinition, String poDataOption,
                                                    String poItemsGroupingOption, String poUploadInstrOption, String poDataFilePath, String subsidiaryAccessOrgOid)
                                                    throws AmsException
   {
      DocumentHandler   poUploadParametersDoc = null;

      // Set up the PO upload parameters doc for the background process to pick up
      poUploadParametersDoc = new DocumentHandler();
      poUploadParametersDoc.setAttribute("/In/AutoLCCreateInfo/uploadDefinitionOid", poUploadDefinition);
      poUploadParametersDoc.setAttribute("../uploadSequenceNumber",                  poUploadSequenceNumber);
      poUploadParametersDoc.setAttribute("../poDataOption",                          poDataOption);
      poUploadParametersDoc.setAttribute("../clientBankOid",                         userSession.getClientBankOid());
      poUploadParametersDoc.setAttribute("../poDataFilePath",                        poDataFilePath);

      if(subsidiaryAccessOrgOid != null)
         poUploadParametersDoc.setAttribute("../subsidiaryAccessOrgOid",                subsidiaryAccessOrgOid);


      if (poItemsGroupingOption != null)
      {
         poUploadParametersDoc.setAttribute("../poGroupingOption", poItemsGroupingOption);
      }

      if (poUploadInstrOption!= null)
      {
         poUploadParametersDoc.setAttribute("../poUploadInstrOption", poUploadInstrOption);
      }
      
      poUploadParametersDoc.setAttribute("/In/User/userOid",    userSession.getUserOid());
      poUploadParametersDoc.setAttribute("../timeZone",         userSession.getTimeZone());
      poUploadParametersDoc.setAttribute("../locale",           userSession.getUserLocale());
      poUploadParametersDoc.setAttribute("../securityRights",   userSession.getSecurityRights());
      poUploadParametersDoc.setAttribute("../baseCurrencyCode", userSession.getBaseCurrencyCode());

      return poUploadParametersDoc;
   }

   /**
    * This method saves all PO upload data to the active PO upload table and
    * writes an appropriate message to the user's organization log file.
    *

    * @param      com.amsinc.ecsg.web.ResourceManager resourceManager - the resource manager for the user's session
    * @param      com.ams.tradeportal.busobj.webbean.SessionWebBean userSession - the current user's Trade Portal session
    * @param      java.lang.String poUploadSequenceNumber - the upload sequence number for the current PO upload
    * @param      java.lang.String poDataFileContents - the contents of the PO Data file
    * @param      java.lang.String poUploadParameters - the PO upload parameters necessary for the PO parsing background
    *                                                   process to function
    * @exception  com.amsinc.ecsg.frame.AmsException
    * @exception  java.rmi.RemoteException
    * @return     void
    */
   private void saveActivePOUpload(ResourceManager resourceManager, FormManager formManager, SessionWebBean userSession,
                                   String poUploadSequenceNumber, String poDataFileContents, String poUploadParameters)
                                   throws AmsException, RemoteException
   {
      ClientServerDataBridge   csdb                   = null;
      ActivePOUpload           activePOUpload         = null;
      String[]                 attributeValues        = null;
      String[]                 attributePaths         = null;
      String                   serverLocation         = null;
      String                   creationDate           = null;
      String                   userOrgOid             = null;
      String                   localDate              = null;
      String                   userName               = null;
      String                   userOid                = null;
      Date                     currentLocalDate       = null;

      // Get the user's oid and organization oid
      userOid    = userSession.getUserOid();
      userOrgOid = userSession.getOwnerOrgOid();

      // Create a new row in the active_po_upload table to indicate that a PO is being
      // uploaded for the user's organization
      serverLocation = JPylonProperties.getInstance().getString("serverLocation");

      csdb = resourceManager.getCSDB();

      activePOUpload = (ActivePOUpload) EJBObjectFactory.createClientEJB(serverLocation, "ActivePOUpload", csdb);

      activePOUpload.newObject();

      // Get the date and time this upload was performed for use in the log message
      creationDate = activePOUpload.getAttribute("creation_timestamp");

      // Set all necessary attributes and save the active PO upload object
      attributeValues = new String[4];
      attributePaths  = new String[4];

      attributePaths[0]  = "corp_org_oid";
      attributePaths[1]  = "po_data_file";
      attributePaths[2]  = "po_upload_parameters";
      attributePaths[3]  = "status";

      attributeValues[0] = userOrgOid;
      attributeValues[1] = poDataFileContents;
      attributeValues[2] = poUploadParameters;
      attributeValues[3] = "S";

      activePOUpload.setAttributes(attributePaths, attributeValues);
      activePOUpload.save();

      // Get the creation date for this PO Upload, adjusted to the user's locale and timezone
      currentLocalDate = TPDateTimeUtility.getLocalDateTime(creationDate, userSession.getTimeZone());
      localDate        = TPDateTimeUtility.formatDateTime(currentLocalDate, userSession.getUserLocale());

      // Get the user's name for the log message and info message about to be issued
      userName = getUserName(userOid);

      // Write a message to the log file indicating that a PO upload request has been
      // submitted
      logPurchaseOrderUploadMessage(userOrgOid, userName, localDate, poUploadSequenceNumber);

      // Issue an informational message to the user indicating that the LC Creation
      // Process has been started
      issuePurchaseOrderUploadMessage(formManager, userName, localDate, userSession.getUserLocale());

      try
       {
         activePOUpload.remove();
       }
      catch(javax.ejb.RemoveException re)
       {
         re.printStackTrace();
         LOG.info("Remove exception removing active PO upload in saveActivePOUpload...");
       }
   }

   /**
    * This method retrieves the full name (i.e., first name and last name) of
    * the user performing the PO upload.
    *

    * @param      java.lang.String userOid - the user's oid
    * @exception  com.amsinc.ecsg.frame.AmsException
    * @return     java.lang.String - the user's full name (first name + " " + last name)
    */
   private String getUserName(String userOid) throws AmsException
   {
      DocumentHandler   resultSet   = null;
      StringBuffer      userName    = null;
      String            userNameSql = null;

      // Compose the SQL to retrieve the user's first and last name
      userNameSql = "select first_name, last_name from users where user_oid = ?" ;

      // Get the user's name from the users table
      resultSet = DatabaseQueryBean.getXmlResultSet(userNameSql, false, new Object[]{userOid});

      userName = new StringBuffer();
      userName.append(resultSet.getAttribute("/ResultSetRecord/FIRST_NAME"));
      userName.append(" ");
      userName.append(resultSet.getAttribute("/ResultSetRecord/LAST_NAME"));

      return userName.toString();
   }

   /**
    * This method writes a message to the user's organization log file
    * indicating that the user just performed a PO upload, as well as the time
    * it occurred at.
    *

    * @param      java.lang.String userOrgOid - the user's organization oid
    * @param      java.lang.String userName - the user's name (first name + " " + last name)
    * @param      java.lang.String localDate - the creation date for this PO Upload, adjusted to the user's locale
    *                                          and timezone
    * @param      java.lang.String poUploadSequenceNumber - the upload sequence number for the current PO upload
    * @exception  com.amsinc.ecsg.frame.AmsException
    * @return     void
    */
   private void logPurchaseOrderUploadMessage(String userOrgOid, String userName, String localDate,
                                              String poUploadSequenceNumber) throws AmsException
   {
      AutoLCLogger   logger = null;

      // Retrieve the log for the user's organization
      logger = AutoLCLogger.getInstance(userOrgOid);

      logger.addLogMessage(userOrgOid, poUploadSequenceNumber, TradePortalConstants.LCLOG_UPLOAD_START,
                           userName, localDate);
   }

   /**
    * This method issues an informational message to the user indicating that
    * the user just performed a PO upload, as well as the time it occurred at.
    *

    * @param      java.lang.String userName - the user's name (first name + " " + last name)
    * @param      java.lang.String localDate - the creation date for this PO Upload, adjusted to the user's locale
    *                                          and timezone
    * @param      java.lang.String localeName - locale used to generate the error
    * @exception  com.amsinc.ecsg.frame.AmsException
    * @return     void
    */
   private void issuePurchaseOrderUploadMessage(FormManager formManager, String userName, String localDate, String localeName)
                                                throws AmsException
   {
      MediatorServices   mediatorServices = null;
      DocumentHandler    xmlDoc           = null;

      xmlDoc = formManager.getFromDocCache();

      mediatorServices = new MediatorServices();
      mediatorServices.getErrorManager().setLocaleName(localeName);
      mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                                                    TradePortalConstants.LCLOG_UPLOAD_START, userName, localDate);
      mediatorServices.addErrorInfo();

      xmlDoc.setComponent("/Error", mediatorServices.getErrorDoc());

      formManager.storeInDocCache("default.doc", xmlDoc);
   }

   /**
    * This method issues an error message using the error code passed in. It
    * also stores appropriate error info in the XML doc currently in the doc
    * cache so that the errors will apear in the page that's returned to.
    * (Note: this method is only called when the error message causes us to
    * go to a new page, and NOT the same one we just came from)
    *

    * @param      com.amsinc.ecsg.web.FormManager formManager - the form manager object for the user's session
    * @param      java.lang.String errorCode - the unique code of the error to issue
    * @param      java.lang.String localeName - the locale name used for error processing
    * @exception  com.amsinc.ecsg.frame.AmsException
    * @return     void
    */
   private void issuePOUploadError(FormManager formManager, String errorCode, String localeName) throws AmsException
   {
      issuePOUploadError(formManager, errorCode, null, null, null, null, localeName);
   }

   /**
    * This method issues an error message using the error code passed in. It
    * also stores appropriate error info and any data previously selected by
    * the user in the XML doc currently in the doc cache so that the errors
    * will apear in the page that's returned to.
    *

    * @param      com.amsinc.ecsg.web.FormManager formManager - the form manager object for the user's session
    * @param      java.lang.String errorCode - the unique code of the error to issue
    * @param      java.lang.String poUploadDefinition - the oid of the PO upload definition if one was selected
    * @param      java.lang.String poDataOption - the PO data option that was selected
    * @param      java.lang.String poItemsGroupingOption - the PO items grouping option that was selected
    * @param      java.lang.String localeName - locale used to generate the error
    * @exception  com.amsinc.ecsg.frame.AmsException
    * @return     void
    */
   private void issuePOUploadError(FormManager formManager, String errorCode, String poUploadDefinition,
                                   String poDataOption, String poItemsGroupingOption, String poUploadInstrOption,String localeName) throws AmsException
   {
      MediatorServices   mediatorServices = null;
      DocumentHandler    xmlDoc           = null;

      xmlDoc = formManager.getFromDocCache();

      mediatorServices = new MediatorServices();
      mediatorServices.getErrorManager().setLocaleName(localeName);
      mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, errorCode);
      mediatorServices.addErrorInfo();

      xmlDoc.setComponent("/Error", mediatorServices.getErrorDoc());
      xmlDoc.setAttribute("/In/HasErrors", TradePortalConstants.INDICATOR_YES);

      xmlDoc.setAttribute("/In/PODataOption", poDataOption);

      // If we're going back to the same page that made the PO upload request, return all
      // data selected by the user on the page
      if (poUploadDefinition != null)
      {
         xmlDoc.setAttribute("/In/POUploadDefinition", poUploadDefinition);

      }

      if (TradePortalConstants.GROUP_PO_LINE_ITEMS.equals(poDataOption))
      {
         xmlDoc.setAttribute("/In/POItemsGroupingOption", poItemsGroupingOption);

         //TLE - 07/29/07 - CR-375 - Add Begin
         xmlDoc.setAttribute("/In/POUploadInstrOption", poUploadInstrOption);
         //TLE - 07/29/07 - CR-375 - Add End
      }

      formManager.storeInDocCache("default.doc", xmlDoc);
   }
}
