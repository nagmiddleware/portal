package com.ams.tradeportal.servlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PushbackInputStream;
import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.crypto.SecretKey;
import javax.ejb.EJBObject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadBase.SizeLimitExceededException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FilenameUtils;

import com.ams.tradeportal.busobj.PurchaseOrderDefinition;
import com.ams.tradeportal.busobj.PurchaseOrderFileUpload;
import com.ams.tradeportal.busobj.util.InstrumentServices;
import com.ams.tradeportal.busobj.webbean.SessionWebBean;
import com.ams.tradeportal.common.Debug;
import com.ams.tradeportal.common.POUploadFileDetails;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.ClientServerDataBridge;
import com.amsinc.ecsg.frame.EJBObjectFactory;
import com.amsinc.ecsg.frame.MediatorServices;
import com.amsinc.ecsg.frame.NavigationManager;
import com.amsinc.ecsg.util.DateTimeUtility;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.EncryptDecrypt;
import com.amsinc.ecsg.util.JPylonProperties;
import com.amsinc.ecsg.util.ResourceManager;
import com.amsinc.ecsg.util.StringFunction;
import com.amsinc.ecsg.web.FormManager;

/**
 * The POStructuredFileUploadServlet class is used when a user
 * uploads a PO file to the Trade Portal for processing.
 *
 * Shilpa R CR-707
 * Release 8.0.0.0
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 *
 */

/**
 * 
 * PO File Format - purchase order file can be formatted as comma, tab,
 * semi-colon or pipe-delimited
 */

public class POStructuredFileUploadServlet extends HttpServlet {
private static final Logger LOG = LoggerFactory.getLogger(POStructuredFileUploadServlet.class);

	private static final int BOM_SIZE = 3;
	private static final long MAX_FILE_SIZE = Long
			.parseLong(TradePortalConstants.getPropertyValue("TradePortal",
					"uploadPOFileMaxSize", "23068672")); // 22MB
	private static final String MAX_FILE_SIZE_IN_MB = getMaxFileSizeInMB();
	private static final long MAX_FILE_PO_LINE_ITEMS = Long
			.parseLong(TradePortalConstants.getPropertyValue("TradePortal",
					"uploadPOFileMaxLineItems", "600"));
	private static final long MAX_FILE_POS = Long
	.parseLong(TradePortalConstants.getPropertyValue("TradePortal",
			"uploadPOPerFile", "10000"));

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doTheWork(request, response);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doTheWork(request, response);
	}

	/**
	 * This method implements the standard servlet API for GET and POST
	 * requests. It handles uploading of files to the Trade Portal.
	 * 
	 * @param javax
	 *            .servlet.http.HttpServletRequest request - the Http servlet
	 *            request object
	 * @param javax
	 *            .servlet.http.HttpServletResponse response - the Http servlet
	 *            response object
	 * @exception javax.servlet.ServletException
	 * @exception java.io.IOException
	 * @return void
	 * @throws AmsException
	 */

	public void doTheWork(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
        
    	//T36000005819 W Zhu 6/21/13 Do not allow direct access to the servlet.
    	if (NavigationManager.bookmarkedAccessDenied(request, response, this)) {
    		return;
    	}

        
		//Hashtable formData = null;
		//RequestParser requestParser = new RequestParser();
		// formData = requestParser.parseRequest(request);
		// Retrieve the form manager and resource manager from the session so
		// that
		// we can issue errors or create an po upload record if necessary
		String fileName = null;
		HttpSession session = request.getSession(true);
		ResourceManager resourceManager = (ResourceManager) session
				.getAttribute("resMgr");
		FormManager formManager = (FormManager) session.getAttribute("formMgr");
		SessionWebBean userSession = (SessionWebBean) session
				.getAttribute("userSession");
		MediatorServices mediatorServices = new MediatorServices();

		String poDataOption = "";
		String poItemsGroupingOption = "";
		String poUploadInstrOption = "";
		try {
			
			mediatorServices.getErrorManager().setLocaleName(
					userSession.getUserLocale());

			if (!ServletFileUpload.isMultipartContent(request)) {
				throw new RuntimeException(
						"Request does not have multipart....");
			}
			
			// Create a factory for disk-based file items
			DiskFileItemFactory factory = new DiskFileItemFactory();
			// factory.setSizeThreshold(100000);

			// Create a new file upload handler
			LOG.debug("Creating ServletFileUpload..");
			ServletFileUpload upload = new ServletFileUpload(factory);
			upload.setSizeMax(MAX_FILE_SIZE);

			// Parse the request
			LOG.debug("Before parsing request..");
			List items = upload.parseRequest(request);
			LOG.debug("After parsing request..");

			
			// Process the uploaded items
			try {
				//	W Zhu 8/22/2012 Rel 8.1 T36000004579 add SecretKey parameter to allow different keys for each session for better security.
				SecretKey secretKey = ((SessionWebBean)request.getSession(false).getAttribute("userSession")).getSecretKey();
				Iterator iter = items.iterator();
				boolean a = false;
				boolean b = false;
				FileItem invFileItem = null;
				String poDefOID = null;
				FileItem item = null;
			    String formDataNi="";
				while (iter.hasNext()) {
					item = (FileItem) iter.next();
					if (!item.isFormField()) {
						fileName = FilenameUtils.getName(item.getName());
						invFileItem = item;
						a = true;

					} else {
						String name = item.getFieldName();
						String value = item.getString();
						if (name.startsWith("PODefinitionName")
								&& null != value && !value.equals("")) {
							value = EncryptDecrypt.decryptStringUsingTripleDes( value, secretKey );
							poDefOID = value;
							b = true;
						} else if (name.startsWith("POUploadDataOption")
								&& null != value && !value.equals("")) {
							poDataOption = value;
						} else if (name.startsWith("POUploadInstrOption")
								&& null != value && !value.equals("")) {
							poUploadInstrOption = value;
						} else if (name.startsWith("POUploadGroupingOption")
								&& null != value && !value.equals("")) {
							poItemsGroupingOption = value;
						}
						else if (name.startsWith("ni") && StringFunction.isNotBlank(value)) {
							formDataNi = value;
						}						
					}
				}
				//csrf check
				//compare form data ni to the one in hashtable in formManager
				//
			    if (formManager == null) {
					throw new RuntimeException("formManager is NULL");				    	
			    }else{
			      if (StringFunction.isNotBlank(formDataNi)){
			        formManager.getServletInvocation (formDataNi,request); 
			      }else{
			    	  throw new RuntimeException("form ni value is missing");
			      }
			    }				
				if (a && b) {
					processUploadedFile(poDefOID, invFileItem, poDataOption,
							poUploadInstrOption, poItemsGroupingOption,
							resourceManager, userSession, mediatorServices);
					a = false;
					b = false;
				}

			} catch (AmsException e) {
				System.out
						.println("PurchaseOrderFileUploadServlet: Error occured during upload, file:'"
								+ fileName + "' Reason:" + e.getMessage());
				e.printStackTrace();
			}

			
			try {
				handleError(mediatorServices, formManager);
			} catch (AmsException e) {
			
				throw new ServletException(e);
			}
			return;

		} catch (SizeLimitExceededException ex) {
			try {
				mediatorServices.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.PO_FILE_SIZE_EXCEEDED,
						MAX_FILE_SIZE_IN_MB + "");
				handleError(mediatorServices, formManager);
			} catch (AmsException e1) {
				e1.printStackTrace();
			}
			System.out
					.println("PurchaseOrderFileUploadServlet: Error occured during upload, file:'"
							+ fileName + "' Reason:" + ex.getMessage());
			ex.printStackTrace();
			throw new ServletException(ex);
		} catch (Exception e) {
			try {
				mediatorServices.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.ERR_SAVING_POUPLOAD_FILE);
				handleError(mediatorServices, formManager);
			} catch (AmsException e1) {
				e1.printStackTrace();
			}
			System.out
					.println("PurchaseOrderFileUploadServlet: Error occured during upload, file:'"
							+ fileName + "' Reason:" + e.getMessage());
			e.printStackTrace();

			throw new ServletException(e);
		}
	}

	protected void handleError(MediatorServices mediatorServices,
			FormManager formManager) throws AmsException, ServletException,
			IOException {

		LOG.debug("Inside hanldeError..");
		DocumentHandler xmlDoc = new DocumentHandler();

		mediatorServices.addErrorInfo();
		xmlDoc.setComponent("/Error", mediatorServices.getErrorDoc());
		xmlDoc.setAttribute("/In/HasErrors", TradePortalConstants.INDICATOR_YES);
		formManager.storeInDocCache("default.doc", xmlDoc);

	}

	public void processUploadedFile(String poDefOID, FileItem item,
			String poDataOption, String poUploadInstrOption,
			String poItemsGroupingOption, ResourceManager resourceManager,
			SessionWebBean userSession, MediatorServices mediatorServices)
			throws AmsException {

		PurchaseOrderFileUpload poFile = null;
		File file = null;
		String fileName = FilenameUtils.getName(item.getName());
		try {

			poFile = createPOFileUpload();
			file = getTempFile(poFile);
			poFile.setAttribute("po_file_name", fileName);
			poFile.setAttribute("user_oid", userSession.getUserOid());
			poFile.setAttribute("owner_org_oid", userSession.getOwnerOrgOid());
			poFile.setAttribute("validation_status",
					TradePortalConstants.PYMT_UPLOAD_VALIDATION_PENDING);
			poFile.setAttribute("system_name", "MIDDLEWARE");
            poFile.setAttribute("server_name", userSession.getServerName());
			//create doc for po_upload_parameters
			DocumentHandler doc = new DocumentHandler();
			doc.setAttribute("/In/User/userOid", userSession.getUserOid());
			doc.setAttribute("../timeZone", userSession.getTimeZone());
			doc.setAttribute("../locale", userSession.getUserLocale());
			doc.setAttribute("../securityRights",
					userSession.getSecurityRights());
			doc.setAttribute("../baseCurrencyCode",
					userSession.getBaseCurrencyCode());
            doc.setAttribute("../corporateOrgOid",userSession.getOwnerOrgOid());
            doc.setAttribute("../clientBankOid",userSession.getClientBankOid());
            doc.setAttribute("../poDataFilePath",fileName);
			doc.setAttribute("/In/PurchaseOrderUploadInfo/POUploadDataOption",
					poDataOption);
			doc.setAttribute("../POUploadInstrOption", poUploadInstrOption);
			doc.setAttribute("../POUploadGroupingOption", poItemsGroupingOption);
			poFile.setAttribute("po_upload_parameters",doc.toString());
			
			if (writetoDisk(poDefOID, item, file, poFile, resourceManager,
					userSession, mediatorServices)) {
				
				if (poFile.save() != 1) {
					throw new RuntimeException(
							"error saving PurchaseOrderFileUpload");
				}
			}

		} catch (Exception e) {
			System.out
					.println("PurchaseOrderFileUploadServlet: Error occured during upload, file:'"
							+ fileName + "' Reason:" + e.getMessage());
			e.printStackTrace();
			cleanup(file);
			if (mediatorServices.getErrorManager().getIssuedErrorsList().size() == 0) {
				mediatorServices.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.ERR_SAVING_POUPLOAD_FILE);
			}
		} finally {
			if (poFile != null) {
				try {
					poFile.remove();
				} catch (Exception ex) {
				}
			}
		}
	}

	protected PurchaseOrderFileUpload createPOFileUpload() throws AmsException,
			RemoteException {
		
		String serverLocation = JPylonProperties.getInstance().getString(
				"serverLocation");
		PurchaseOrderFileUpload poFile = (PurchaseOrderFileUpload) EJBObjectFactory
				.createClientEJB(serverLocation, "PurchaseOrderFileUpload");
		poFile.newObject();
		poFile.setAttribute("creation_timestamp",
				DateTimeUtility.getGMTDateTime(true, false));

		return poFile;
	}

	protected void removeEJBReference(EJBObject objRef) {
		try {
			if (objRef != null) {
				objRef.remove();
				objRef = null;
			}
		} catch (Exception e) {
			LOG.info("while removing EJB reference ...'"
					+ "' Reason:" + e.getMessage());
			e.printStackTrace();
		}
	}

	/**
	 * This method returns the encoding type by checking the Byte Order Marker
	 * 
	 * @param byte[] - byte array
	 * @return String
	 * @throws IOException
	 */
	private String getEncoding(PushbackInputStream is) throws IOException {
		String encoding = "ASCII";
		int read = 0;
		byte bom[] = new byte[BOM_SIZE];

		is.read(bom, 0, bom.length);

		if ((bom[0] == (byte) 0xFF) && (bom[1] == (byte) 0xFE)) {
			encoding = "UTF-16LE";
			read = 2;
		} else if ((bom[0] == (byte) 0xFE) && (bom[1] == (byte) 0xFF)) {
			encoding = "UTF-16BE";
			read = 2;
		} else if ((bom[0] == (byte) 0xEF) && (bom[1] == (byte) 0xBB)
				&& (bom[2] == (byte) 0xBF)) {
			encoding = "UTF-8";
			read = 3;
		}
		is.unread(bom, read, BOM_SIZE - read);
		return encoding;
	}

	/**
	 * This method returns the Uploaded po file
	 * 
	 * @param PurchaseOrderFileUpload
	 *            -
	 * @return File
	 * @throws RemoteException
	 * @throws AMSException
	 * 
	 */
	protected File getTempFile(PurchaseOrderFileUpload poFile)
			throws RemoteException, AmsException {
		LOG.debug("Getting temporary file..");
		File file = null;
		String fileName = poFile.getAttribute("po_file_upload_oid");
		String folder = TradePortalConstants.getPropertyValue(
				"AgentConfiguration", "uploadFolder", "c:/temp/");
		String fname = folder + fileName + POUploadFileDetails.FILE_EXTENSION;

		file = new File(fname);

		LOG.debug("Getting temporary file. Filename: "
				+ file.getAbsolutePath());

		return file;
	}

	/**
	 * This method validates the uploaded file and writes to temporary folder
	 * 
	 * @param item
	 * @param file
	 * @param POfile
	 * @param resourceManager
	 * @param userSession
	 * @param mediatorServices
	 * @return
	 * @throws Exception
	 */
	public boolean writetoDisk(String poDefOID, FileItem item, File file,
			PurchaseOrderFileUpload poFileUpload,
			ResourceManager resourceManager, SessionWebBean userSession,
			MediatorServices mediatorServices) throws Exception {

		boolean success = true;

		BufferedWriter fs = null;
		BufferedReader reader = null;

		try {

			
			InputStream is = item.getInputStream();
			PushbackInputStream pis = new PushbackInputStream(is, BOM_SIZE);

			InputStreamReader isr = new InputStreamReader(pis, getEncoding(pis));
			reader = new BufferedReader(isr);

			fs = new BufferedWriter(new OutputStreamWriter(
					new FileOutputStream(file), AmsConstants.UTF8_ENCODING));

			String sLine = "";

			String serverLocation = JPylonProperties.getInstance().getString(
					"serverLocation");
			ClientServerDataBridge csdb = resourceManager.getCSDB();
			PurchaseOrderDefinition poDefinition = (PurchaseOrderDefinition) EJBObjectFactory
					.createClientEJB(serverLocation, "PurchaseOrderDefinition",
							Long.parseLong(poDefOID), csdb);
			poFileUpload.setAttribute("a_upload_definition_oid",
					poDefinition.getAttribute("purchase_order_definition_oid"));
			String delimiterChar = poDefinition.getAttribute("delimiter_char");
			LOG.debug("Processing lines..");
			String[] record = null;
			sLine = reader.readLine();
			HashMap map = new HashMap();
			String includeColumHeader = poDefinition.getAttribute("include_column_headers");
			POUploadFileDetails.getPODefFieldCount(poDefinition, map);
			if(TradePortalConstants.INDICATOR_YES.equals(includeColumHeader)){
				sLine = reader.readLine();
				//if header is included and sLine is blank, it indicates that 
				//the input file only has a header or next line after header is empty
				//so return false for validation - no need to validate empty files
				if(StringFunction.isBlank(sLine)){
					success = false;
				}				
			}
			int i=0;
			int lineItemCount=0;
			long poCount = 0;
			
			String poNumVar = "";
			while ((reader != null) && (sLine != null)) {
				HashMap details = new HashMap();
				if (InstrumentServices.isBlankOrWhiteSpace(sLine)) {
					sLine = reader.readLine();
					continue;
				}
				else {
					i++;
					record = POUploadFileDetails.parsePOLineItem(delimiterChar, sLine);

					if (record == null) {
						mediatorServices
								.getErrorManager()
								.issueError(
										TradePortalConstants.ERR_CAT_1,
										TradePortalConstants.FILE_FORMAT_NOT_VALID_DELIMITED);
						return false;
					}
					
					//IR ANUM040485783 start
					boolean isValidFieldCount =POUploadFileDetails.validatePOLineItem(record,mediatorServices,map);
					if (!isValidFieldCount) {
						return false;
					}
					//IR ANUM040485783 end
					
					//get the purchase order number
					String poNum = POUploadFileDetails.getUniqueID(
							record, mediatorServices,poDefinition,i,"po_data_field","purchase_order_num");
					if(i==1) {
						lineItemCount++;
						poCount++;
						poNumVar=poNum;
					}
					else if(StringFunction.isNotBlank(poNumVar) && poNumVar.equals(poNum)){
						lineItemCount++;
					 }
					else {
						poCount++;
						poNumVar=poNum;
					}
					
					if (lineItemCount > MAX_FILE_PO_LINE_ITEMS){
						mediatorServices
						.getErrorManager()
						.issueError(
								TradePortalConstants.ERR_CAT_1,
								TradePortalConstants.PO_FILE_LINE_ITEMS_EXCEEDED,String.valueOf(MAX_FILE_PO_LINE_ITEMS));
						return false;
					}
					if (poCount > MAX_FILE_POS){
						mediatorServices
						.getErrorManager()
						.issueError(
								TradePortalConstants.ERR_CAT_1,
								TradePortalConstants.POS_PER_FILE_EXCEEDED,String.valueOf(MAX_FILE_POS));
						return false;
					}
					fs.write(sLine);
					fs.newLine();
					sLine = reader.readLine();
				}
			
			}

		} catch (RuntimeException e) {
			e.printStackTrace();
			throw e;
		} finally {
			if (fs != null) {
				try {
					fs.close();
				} catch (IOException e) {
					e.printStackTrace();
				}

			}
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return success;
	}

	protected static String getMaxFileSizeInMB() {
		float f = (float) (MAX_FILE_SIZE / (1024.0 * 1024.0));
		BigDecimal dec = new BigDecimal(f);
		dec = dec.setScale(1, BigDecimal.ROUND_DOWN);
		return dec.toString();

	}

	/**
	 * Performs cleanup
	 * 
	 * @param file
	 * 
	 * @return
	 */
	protected void cleanup(File file) {
		LOG.debug("Inside cleanup..");
		// delete file
		if (file != null && !file.delete()) {

			LOG.info("Error: Could not delete file: "
					+ file.getAbsoluteFile());
		}

	}
}
