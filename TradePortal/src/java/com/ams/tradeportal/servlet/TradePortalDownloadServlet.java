package com.ams.tradeportal.servlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.busobj.webbean.*;
import com.ams.tradeportal.common.*;
import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import com.amsinc.ecsg.web.*;

import javax.crypto.SecretKey;
import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.*;
import java.math.*;

//Xi R2 JARS
import com.businessobjects.rebean.wi.*;


/**
 * The TradePortalDownloadServlet class is used when a user wishes to download
 * Import LC - Issue data, Export LC - Advise terms data, or report data.
 *
 *     Copyright  ?2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 *

 */

public class TradePortalDownloadServlet extends HttpServlet
{
private static final Logger LOG = LoggerFactory.getLogger(TradePortalDownloadServlet.class);
   @Override
   public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
   {
      doTheWork(request, response);
   }

   @Override
   public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
   {
      doTheWork(request, response);
   }

   /**
    * This method implements the standard servlet API for GET and POST
    * requests. It retrieves Import LC - Issue data, Export LC - Advise
    * terms data, or report data and sends it back as a Trade Portal
    * specific content type so that it may be saved locally as a file
    * by the user.
    *

    * @param      request  - the Http servlet request object
    * @param      response - the Http servlet response object
    * @exception  javax.servlet.ServletException
    * @exception  java.io.IOException
    * @return     void
    */
    public void doTheWork(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //T36000005819 W Zhu 9/24/12 Do not allow direct access to the servlet.
        if (NavigationManager.bookmarkedAccessDenied(request, response, this)) {
            return;
        }

        String downloadRequestType = request.getParameter(TradePortalConstants.DOWNLOAD_REQUEST_TYPE);
      //IR-GAUI051582080 Krishna 05/19/2008
        //Commented the line of code "out.println(xmlDoc.toString());" at all places through out the file and added
        // a line of code "out.println(StringFunction.xssHtmlToChars(xmlDoc.toString()));" below that, to convert xssHtml codes back to
        //original characters.

        try {
            if (downloadRequestType.equals(TradePortalConstants.DOWNLOAD_IMPORT_LC_ISSUE_DATA)) {
                downloadImportLCData(request, response);
            } else if (downloadRequestType.equals(TradePortalConstants.DOWNLOAD_ATP_ISSUE_DATA)) {
                downloadApprovalToPayData(request, response);
            } else if (downloadRequestType.equals(TradePortalConstants.DOWNLOAD_OUTGOING_STANDBY)) {
                TradePortalDownloadData.downloadOutgoingStandbyGuaranteeData(request, response);
            } else if (downloadRequestType.equals(TradePortalConstants.DOWNLOAD_GUARANTEE_ISSUE_DATA)) {
                TradePortalDownloadData.downloadGuaranteeData(request, response);
            } else if (downloadRequestType.equals(TradePortalConstants.DOWNLOAD_ADVICE_TERMS_DATA)) {
                downloadAdviceTermsData(request, response);
            } else if (downloadRequestType.equals(TradePortalConstants.REPORT_DOWNLOAD)) {
                downloadReport(request, response);
			} else if (downloadRequestType.equals(TradePortalConstants.DOWNLOAD_AUTHORISED_TRANSACTION_XML)) {//Added for Rel9.3.5 - CR-1028
				downloadAuthorisedTransactionXML(request, response);
            } else {
                LOG.debug("Download Request Type Not Specified Or Invalid!");
            }
        } catch (Exception ex) {
            LOG.debug("Exception in TradePortalDownloadServlet!");
            ex.printStackTrace();
        }
    }

   /**
    * This method is used to download Import LC data for the instrument currently being viewed
    * in the Import LC Issue transaction detail page.
    *

    * @param      request - the Http servlet request object
    * @exception  javax.servlet.ServletException
    * @exception  java.io.IOException
    * @return     void
    */
   private void downloadImportLCData(HttpServletRequest request, HttpServletResponse response)
                                     throws ServletException, IOException, AmsException
   {
      ServletOutputStream   out               = null;
      InstrumentWebBean     instrument        = null;
      TermsPartyWebBean     termsParty        = null;
      com.amsinc.ecsg.util.ResourceManager       resourceManager   = null;
      TermsWebBean          terms             = null;
      StringBuilder          defaultFileName   = null;
      TransactionWebBean     transaction 	  = null;
     
      BeanManager           beanManager       = null;
      HttpSession           session           = null;
      boolean               isValidTermsParty = false;
  
      String                resourceLocale    = null;
   
      SessionWebBean 		userSession 	  = null;
      
      // First we need to retrieve the resource manager from the session so that we can retrieve
      // internationalized field name strings
      session         = request.getSession(false);
      resourceManager = (com.amsinc.ecsg.util.ResourceManager) session.getAttribute("resMgr");
      beanManager     = (BeanManager)     session.getAttribute("beanMgr");
      userSession = (SessionWebBean) session.getAttribute("userSession");
      
      // Retrieve the http servlet output stream
      out = response.getOutputStream();

      // Retrieve the locale from resource manager; this should be currently set to the user's locale
      resourceLocale = resourceManager.getResourceLocale();
      /*KMehta - 19 Feb 2015 - Rel9.3 IR-T36000013054 - Add  - Begin 
       * Made changes to resourceManager.getText to accept the locale
       * en_US as defined in public String getText(String key, String bundle, String localeName) 
       * Even when the locale is Traditional Chinese , Vietnamese. the download servlet would be displayed in english.
       */
      
      // Next, retrieve all web beans in the session pertaining to the Import LC instrument currently being viewed
      instrument   = (InstrumentWebBean)  beanManager.getBean("Instrument");
      terms        = (TermsWebBean)       beanManager.getBean("Terms");

      String instrumentType = instrument.getAttribute("instrument_type_code");
      // Set the default file name to use for the Import LC/Request Advise data file to be downloaded
      
    //MEer Rel 9.2 IR-38100 Retrieve customer entered terms from transaction
      transaction   = (TransactionWebBean)  beanManager.getBean("Transaction");
      String custEnteredTermsOid = transaction.getAttribute("c_CustomerEnteredTerms");    
      
      defaultFileName = new StringBuilder();

      if (instrumentType.equals(InstrumentType.REQUEST_ADVISE))
    	  defaultFileName.append(resourceManager.getText("RequestAdviseIssue.DownloadFileNamePrefix",
                  TradePortalConstants.TEXT_BUNDLE, "en_US"));

      //rkrishna CR 375-D ATP-ISS 07/21/2007 Begin
      else if(instrumentType.equals(InstrumentType.APPROVAL_TO_PAY))
    	defaultFileName.append(resourceManager.getText("ApprovalToPayIssue.DownloadFileNamePrefix",
                                                     TradePortalConstants.TEXT_BUNDLE, "en_US"));
      //rkrishna CR 375-D ATP-ISS 07/21/2007 End
      else
    	  defaultFileName.append(resourceManager.getText("ImportDLCIssue.DownloadFileNamePrefix",
                                                     TradePortalConstants.TEXT_BUNDLE, "en_US"));

      defaultFileName.append(instrument.getAttribute("complete_instrument_id").replace('/', '_'));

      if (instrumentType.equals(InstrumentType.REQUEST_ADVISE))
    	  defaultFileName.append(resourceManager.getText("RequestAdviseIssue.DownloadFileNameSuffix",
                                                     TradePortalConstants.TEXT_BUNDLE, "en_US"));
      //rkrishna CR 375-D ATP-ISS 07/21/2007 Begin
      else if(instrumentType.equals(InstrumentType.APPROVAL_TO_PAY))
    	  defaultFileName.append(resourceManager.getText("ApprovalToPayIssue.DownloadFileNameSuffix",
                                                     TradePortalConstants.TEXT_BUNDLE, "en_US"));
      //rkrishna CR 375-D ATP-ISS 07/21/2007 End
      else
    	  defaultFileName.append(resourceManager.getText("ImportDLCIssue.DownloadFileNameSuffix",
              TradePortalConstants.TEXT_BUNDLE, "en_US"));

      // Set the http servlet response content type and content disposition (i.e., default filename)
      setResponseContent(response, TradePortalConstants.DOWNLOAD_TXT_CONTENT_TYPE, defaultFileName.toString());

	  // One instance of TermsPartyWebBean that will be used to retrieve different term party information
	termsParty = beanManager.createBean(TermsPartyWebBean.class,  "TermsParty");

	downloadImportLCDataInstrumentID(out, instrument, resourceManager);

	downloadImportLCDataBeneficiary(
		out,
		terms,
		resourceManager,
		isValidTermsParty,
		resourceLocale,
		termsParty);

	downloadImportLCDataApplicant(
		out,
		terms,
		resourceManager,
		resourceLocale,
		termsParty);

	if (instrumentType.equals(InstrumentType.REQUEST_ADVISE)) { // It is a Request to Advise
		downloadRequestAdviseDataApplicantBank(
				out,
				terms,
				resourceManager,
				resourceLocale,
				termsParty);
		downloadRequestAdviseDataAdviseThroughBank(
				out,
				terms,
				resourceManager,
				resourceLocale,
				termsParty);
		downloadRequestAdviseDataReimbursingBank(
				out,
				terms,
				resourceManager,
				resourceLocale,
				termsParty);
		downloadRequestAdviseDataIssuer(
				out,
				terms,
				resourceManager,
				resourceLocale,
				termsParty);

	}
	else { // It is an Import LC Issue
		downloadImportLCDataAdvisingBank(
		out,
		terms,
		resourceManager,
		resourceLocale,
		termsParty);
	}

	downloadImportLCDataTermsGeneral(
		out,
		terms,
		instrumentType,
		instrument,
		resourceManager,
		resourceLocale,
		userSession);

	downloadImportLCDataDocs(out, terms, resourceManager, resourceLocale);

    // One instance of ShipmentTermsWebBean that will be used to retrieve data for each ShipmentTerms object.
	ShipmentTermsWebBean shipmentTerms = beanManager.createBean(ShipmentTermsWebBean.class,  "ShipmentTerms");
	//MEer IR3-8100 get Terms object for customer entered terms
	if(StringFunction.isNotBlank(custEnteredTermsOid)){
		terms = transaction.registerCustomerEnteredTerms();	
		if(!terms.isValid()){
			LOG.info("Error occurred retrieving TERMS deatils with termsOid: " + transaction.getAttribute("c_CustomerEnteredTerms"));
			}
	}

	downloadImportLCDataAllShipmentTerms(
		out,
		terms,
		resourceManager,
		termsParty,
		resourceLocale,
		shipmentTerms);

	downloadImportLCDataOtherConditions(out, terms, resourceManager);

   }

   /**
	* Download instrument id
	*
	* @param out
	* @param instrument
	* @param resourceManager
	* @throws IOException
	*/
   private void downloadImportLCDataInstrumentID(
	   ServletOutputStream out,
	   InstrumentWebBean instrument,
	   com.amsinc.ecsg.util.ResourceManager resourceManager)
	   throws IOException {
	  
		 // Retrieve the instrument number of the Import LC instrument currently being viewed
	     StringBuilder downloadData = new StringBuilder();
		 downloadData.append(resourceManager.getText("ImportDLCIssue.ImportLCNumber", TradePortalConstants.TEXT_BUNDLE, "en_US"));
		 downloadData.append(" : ");
		 downloadData.append(instrument.getAttribute("complete_instrument_id"));
		 out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
		 out.println("");
   }


/**
 * Download the general Terms data
 *
 * @param out
 * @param terms
 * @param resourceManager
 * @param resourceLocale
 * @throws IOException
 * @throws AmsException
 */
private void downloadImportLCDataTermsGeneral(
	ServletOutputStream out,
	TermsWebBean terms,
	String instrumentType,
	InstrumentWebBean instrument,
	com.amsinc.ecsg.util.ResourceManager resourceManager,
	String resourceLocale, SessionWebBean userSession)
	throws IOException, AmsException {
	
	String paymentTermsType;
	
	String bankChargesType;
	String currencyCode;
	  // Retrieve the Applicant's reference number, the Import LC transaction's expiry date, currency, amount,
	  // amount tolerance, payment terms, and bank charges
	StringBuilder downloadData = new StringBuilder();
	  downloadData.append(resourceManager.getText("transaction.ApplRefNumber", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	  downloadData.append(" : ");
	  downloadData.append(terms.getAttribute("reference_number"));
	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
	  out.println("");

	  if (instrumentType.equals(InstrumentType.REQUEST_ADVISE)) { // Request Advise add Issuer Ref Num and Issue Date
		  // Issuer Reference Number
		  downloadData = new StringBuilder();
		  downloadData.append(resourceManager.getText("RequestAdviseIssue.IssuerReferenceNumber", TradePortalConstants.TEXT_BUNDLE, "en_US"));
		  downloadData.append(" : ");
		  downloadData.append(terms.getAttribute("issuer_ref_num"));
		  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
		  out.println("");
		  // Issue Date
		  downloadData = new StringBuilder();
		  downloadData.append(resourceManager.getText("RequestAdviseIssue.IssueDate", TradePortalConstants.TEXT_BUNDLE, "en_US"));
		  downloadData.append(" : ");
		  downloadData.append(TPDateTimeUtility.formatDate(instrument.getAttribute("issue_date"), TPDateTimeUtility.LONG,
                  resourceLocale));
		  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
		  out.println("");
	  }

	  downloadData = new StringBuilder();
	  downloadData.append(resourceManager.getText("ImportDLCIssue.ExpiryDate", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	  downloadData.append(" : ");
	  downloadData.append(TPDateTimeUtility.formatDate(terms.getAttribute("expiry_date"), TPDateTimeUtility.LONG,
	                                                   resourceLocale));
	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
	  out.println("");

	  if (instrumentType.equals(InstrumentType.REQUEST_ADVISE)) { // Request Advise add Expiry Place
		  // Expiry Place
		  downloadData = new StringBuilder();
		  downloadData.append(resourceManager.getText("RequestAdviseIssue.ExpiryPlace", TradePortalConstants.TEXT_BUNDLE, "en_US"));
		  downloadData.append(" : ");
		  downloadData.append(terms.getAttribute("place_of_expiry"));
		  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
		  out.println("");
	  }

	  // Retrieve the currency code of the transaction
	  currencyCode = terms.getAttribute("amount_currency_code");

	  downloadData = new StringBuilder();
	  downloadData.append(resourceManager.getText("transaction.Currency", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	  downloadData.append(" : ");
	  downloadData.append(currencyCode);
	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
	  out.println("");

	  downloadData = new StringBuilder();
	  downloadData.append(resourceManager.getText("transaction.Amount", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	  downloadData.append(" : ");
	  downloadData.append(TPCurrencyUtility.getDisplayAmount(terms.getAttribute("amount"), currencyCode,
	                                                         resourceLocale));
	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
	  out.println("");

	  String notExceeding = "";
	  if (instrumentType.equals(InstrumentType.REQUEST_ADVISE)) { // Request Advise Check to See if not Exceeding is Necessary and put in Amount Details title
		 downloadData = new StringBuilder();
		 downloadData.append(resourceManager.getText("RequestAdviseIssue.AmountDetails", TradePortalConstants.TEXT_BUNDLE, "en_US"));
		 downloadData.append(" -");
		 out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
		 notExceeding = terms.getAttribute("maximum_credit_amount");
		 if (!StringFunction.isBlank(notExceeding)) {
			 downloadData = new StringBuilder();
			 downloadData.append(resourceManager.getText("RequestAdviseIssue.MaximumCreditAmount", TradePortalConstants.TEXT_BUNDLE, "en_US"));
			 downloadData.append(" : ");
			 downloadData.append(notExceeding);
			 out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
			 out.println("");
		 }
	  }
	  if (StringFunction.isBlank(notExceeding)) {
		  downloadData = new StringBuilder();
		  downloadData.append(resourceManager.getText("ImportDLCIssue.AmtTolerance", TradePortalConstants.TEXT_BUNDLE, "en_US"));
		  downloadData.append(" : ");
		  downloadData.append(resourceManager.getText("transaction.Plus", TradePortalConstants.TEXT_BUNDLE, "en_US"));
		  downloadData.append(terms.getAttribute("amt_tolerance_pos"));
		  downloadData.append(resourceManager.getText("transaction.Percent", TradePortalConstants.TEXT_BUNDLE, "en_US"));
		  downloadData.append(" ");
		  downloadData.append(resourceManager.getText("transaction.Minus", TradePortalConstants.TEXT_BUNDLE, "en_US"));
		  downloadData.append(terms.getAttribute("amt_tolerance_neg"));
		  downloadData.append(resourceManager.getText("transaction.Percent", TradePortalConstants.TEXT_BUNDLE, "en_US"));
		  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
		  out.println("");
	  }

	  if (instrumentType.equals(InstrumentType.REQUEST_ADVISE)) { // Request Advise add Additional Amounts Covered, Availability, and Draft Details
		  // Additional Amounts Covered
		  downloadData = new StringBuilder();
		  downloadData.append(resourceManager.getText("RequestAdviseIssue.AdditionalAmountsCovered", TradePortalConstants.TEXT_BUNDLE, "en_US"));
		  downloadData.append(" : ");
		  downloadData.append(terms.getAttribute("addl_amounts_covered"));
		  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
		  out.println("");
		  // Availability
		  downloadData = new StringBuilder();
		  downloadData.append(resourceManager.getText("RequestAdviseIssue.Availability", TradePortalConstants.TEXT_BUNDLE, "en_US"));
		  downloadData.append(" -");
		  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
		  downloadData = new StringBuilder();
		  downloadData.append(resourceManager.getText("RequestAdviseIssue.AvailBy", TradePortalConstants.TEXT_BUNDLE, "en_US"));
		  downloadData.append(" : ");
		  //Leelavathi IR#T36000016452 Rel-8.2 06/10/2013 Begin 
		  //commenting out, because RQA instrument has Available_by at two places.
		  downloadData.append(ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.AVAILABLE_BY,
                  /*terms.getAttribute("available_by")*/terms.getAttribute("payment_type"),
                  resourceLocale));
		  //Leelavathi IR#T36000016452 Rel-8.2 06/10/2013 End
		  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
		  downloadData = new StringBuilder();
		  downloadData.append(resourceManager.getText("RequestAdviseIssue.AvailWithPty", TradePortalConstants.TEXT_BUNDLE, "en_US"));
		  downloadData.append(" : ");
		  downloadData.append(ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.AVAILABLE_WITH_PARTY,
                  terms.getAttribute("available_with_party"),
                  resourceLocale));
		  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
		  out.println("");
		  // Draft Details
		  downloadData = new StringBuilder();
		  downloadData.append(resourceManager.getText("RequestAdviseIssue.DraftDetails", TradePortalConstants.TEXT_BUNDLE, "en_US"));
		  downloadData.append("- ");
		  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
		  downloadData = new StringBuilder();
		  downloadData.append(resourceManager.getText("RequestAdviseIssue.DraftReqd", TradePortalConstants.TEXT_BUNDLE, "en_US"));
		  downloadData.append(" : ");
		  if (TradePortalConstants.INDICATOR_YES.equals(terms.getAttribute("drafts_required"))) {
			  downloadData.append(resourceManager.getText("common.Yes", TradePortalConstants.TEXT_BUNDLE, "en_US"));
			  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
			  downloadData = new StringBuilder();
			  downloadData.append(resourceManager.getText("RequestAdviseIssue.DrawnOnPty", TradePortalConstants.TEXT_BUNDLE, "en_US"));
			  downloadData.append(" : ");
			  downloadData.append(ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.DRAWN_ON_PARTY,
	                  terms.getAttribute("drawn_on_party"),
	                  resourceLocale));
		  } else {
			  downloadData.append(resourceManager.getText("common.No", TradePortalConstants.TEXT_BUNDLE, "en_US"));
		  }
		  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
		  out.println("");
	  }

	  // Retrieve the transaction's payment terms type
	 
	  downloadData = new StringBuilder();
	  downloadData.append(resourceManager.getText("ImportDLCIssue.PmtTerms", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	  downloadData.append(" : ");
	  

	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

	  downloadData = new StringBuilder();
	  setPaymentTenorTerms(downloadData, terms, resourceLocale, userSession, out);
	  out.println("");
	  
	  downloadData = new StringBuilder();
	  downloadData.append(resourceManager.getText("ImportDLCIssue.PmtTerms",
	                                              TradePortalConstants.TEXT_BUNDLE, "en_US"));
	  downloadData.append(" : ");
	  String strFor=resourceManager.getText("ImportDLCIssue.For", TradePortalConstants.TEXT_BUNDLE, "en_US");
	  downloadData.append(strFor.substring(0, strFor.indexOf("&")));
	  downloadData.append(" ");
	  downloadData.append(terms.getAttribute("pmt_terms_percent_invoice"));
	  downloadData.append(" ");
	  downloadData.append(resourceManager.getText("ImportDLCIssue.InvValuePercent", TradePortalConstants.TEXT_BUNDLE, "en_US"));

	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
	  out.println("");

	  // Retrieve the transaction's bank charges type
	  bankChargesType = terms.getAttribute("bank_charges_type");
	  if (StringFunction.isBlank(bankChargesType)){
		  bankChargesType = "";
	  }
	  downloadData = new StringBuilder();
	  downloadData.append(resourceManager.getText("ImportDLCIssue.BankCharges", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	  downloadData.append(" : ");

	  if (TradePortalConstants.CHARGE_OUR_ACCT.equals(bankChargesType))
	  {
	     downloadData.append(resourceManager.getText("ImportDLCIssue.AllOurAcct", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	  }
	  else if (TradePortalConstants.CHARGE_BEN_FOR_OUTSIDE.equals(bankChargesType))
	  {
	     downloadData.append(resourceManager.getText("ImportDLCIssue.AllOutside", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	  }
	  else if (TradePortalConstants.CHARGE_OTHER.equals(bankChargesType))
	  {
	     downloadData.append(resourceManager.getText("ImportDLCIssue.Other", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	  }

	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
	  out.println("");
}

protected void setPaymentTenorTerms(StringBuilder downloadData, TermsWebBean terms, String resourceLocale, SessionWebBean userSession, ServletOutputStream out) throws IOException {
	try {
		
		ReferenceDataManager refDataMgr = ReferenceDataManager.getRefDataMgr();
		String percentAmountRadio 			= terms.getAttribute("percent_amount_dis_ind");
		String currencyCode 				= terms.getAttribute("amount_currency_code");
		String termsAmount 					= terms.getAttribute("amount");
		String termsPmtType 				= terms.getAttribute("payment_type");
		String specialTenorTermsText			= terms.getAttribute("special_tenor_text");
		SimpleDateFormat isoDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
		if ( TradePortalConstants.SPECIAL.equals(termsPmtType)){
			if (StringFunction.isNotBlank(specialTenorTermsText))
				downloadData.append(specialTenorTermsText);
			
			return;
		}
		List<AmsEntityWebBean> pmtTermsTenorDtlList = terms.getChildren("PmtTermsTenorDtl","pmt_terms_tenor_dtl_oid","p_terms_oid"); 
		for(AmsEntityWebBean pmtTermsTenorDtl : pmtTermsTenorDtlList) {     
			
			String tenorType = pmtTermsTenorDtl.getAttribute("tenor_type");
			String daysAfterType = pmtTermsTenorDtl.getAttribute("days_after_type");
			String maturityDate = pmtTermsTenorDtl.getAttribute("maturity_date");
			String numberOfDaysAfter = pmtTermsTenorDtl.getAttribute("num_days_after");
			String tenorAmount = 	pmtTermsTenorDtl.getAttribute("amount");
			String tenorPercent = 	pmtTermsTenorDtl.getAttribute("percent");
			
			String tempAmountStr = "";
			if(percentAmountRadio.equalsIgnoreCase("A")){
				tempAmountStr =  TPCurrencyUtility.getDisplayAmount(tenorAmount, currencyCode, resourceLocale);
				
			}else if(percentAmountRadio.equalsIgnoreCase("P")){
		    	BigDecimal finPCTValue = BigDecimal.ZERO;
		    	if ((tenorPercent != null)&&(!"".equals(tenorPercent))) {
		    		finPCTValue = (new BigDecimal(tenorPercent)).divide(new BigDecimal(100.00));
		    	}
		    	if (finPCTValue.compareTo(BigDecimal.ZERO) != 0){
		    		BigDecimal tenorAmountTmp = new BigDecimal(termsAmount == null ? "0.00" : termsAmount) ;
		    		BigDecimal tempAmount = finPCTValue.multiply(tenorAmountTmp);
					String trnxCurrencyCode = terms.getAttribute("copy_of_currency_code");
					int numberOfCurrencyPlaces = 2;

					if (trnxCurrencyCode != null && !"".equals(trnxCurrencyCode)) {
						numberOfCurrencyPlaces = Integer.parseInt(ReferenceDataManager
								.getRefDataMgr().getAdditionalValue(
										TradePortalConstants.CURRENCY_CODE,
										trnxCurrencyCode));
					}
					tempAmount = tempAmount.setScale(numberOfCurrencyPlaces,
							BigDecimal.ROUND_HALF_UP);
					tempAmountStr =  TPCurrencyUtility.getDisplayAmount(tempAmount.toString(), currencyCode, resourceLocale);
		    	}

			}
			downloadData.append(currencyCode);
			downloadData.append(" ");
			downloadData.append(tempAmountStr);
			if (TradePortalConstants.PMT_SIGHT.equals(tenorType)){
				downloadData.append(" at ");
			}else{
				downloadData.append(" by ");
			}
			downloadData.append(refDataMgr.getDescr(TradePortalConstants.TENOR, tenorType, resourceLocale ));
			downloadData.append(" ");
			if (StringFunction.isNotBlank(daysAfterType)){
				downloadData.append(numberOfDaysAfter);
				downloadData.append(" days after ");
				downloadData.append(refDataMgr.getDescr(TradePortalConstants.PMT_TERMS_DAYS_AFTER, daysAfterType, resourceLocale ));
			}
			if (StringFunction.isNotBlank(maturityDate)){
				downloadData.append(" at fixed maturity. Maturity date of ");
				String date = pmtTermsTenorDtl.getAttribute("maturity_date");
				if(StringFunction.isNotBlank(date)){
					date = isoDateFormat.format(isoDateFormat.parse(pmtTermsTenorDtl.getAttribute("maturity_date")));
					date = TPDateTimeUtility.formatDate(date, TPDateTimeUtility.SHORT, resourceLocale);
				}
				downloadData.append(date);
				
			}
			  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
			  downloadData = new StringBuilder();
		}
		
	} catch (NumberFormatException e) {
		e.printStackTrace();
	} catch (AmsException e) {
		e.printStackTrace();
	} catch (ParseException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
}

protected static void setAutoExtTerms(StringBuilder downloadData, TermsWebBean terms, com.amsinc.ecsg.util.ResourceManager resourceManager) {
	
	try {
		String autoExtInd 			= terms.getAttribute("auto_extension_ind");
		
		if (TradePortalConstants.INDICATOR_YES.equals(autoExtInd)){
			String maxAutoExtAllowed	= terms.getAttribute("max_auto_extension_allowed");
			String autoExtPeriod 		= terms.getAttribute("auto_extension_period");
			String autoExtDays 			= terms.getAttribute("auto_extension_days");
			String notifyBeneDays 		= terms.getAttribute("notify_bene_days");
			String finalExpiryDate 		= terms.getAttribute("final_expiry_date");
			if(StringFunction.isNotBlank(finalExpiryDate)){
				finalExpiryDate = TPDateTimeUtility.formatDate(finalExpiryDate, TPDateTimeUtility.LONG, resourceManager.getResourceLocale());
			}
			
			downloadData.append(System.getProperty("line.separator"));
		    downloadData.append(resourceManager.getText("ImportDLCIssue.AutoExtensionTerms", TradePortalConstants.TEXT_BUNDLE, "en_US"));
		    downloadData.append(" - ");
		    downloadData.append(System.getProperty("line.separator"));
		    downloadData.append(System.getProperty("line.separator"));
		    
			downloadData.append(resourceManager.getText("ImportDLCIssue.AutoExtension", TradePortalConstants.TEXT_BUNDLE, "en_US")).append(" : ").append("Yes");
			downloadData.append(System.getProperty("line.separator"));
			downloadData.append(resourceManager.getText("ImportDLCIssue.MaxNumber", TradePortalConstants.TEXT_BUNDLE, "en_US")).append(" : ").append(maxAutoExtAllowed);
			downloadData.append(System.getProperty("line.separator"));
			downloadData.append(resourceManager.getText("ImportDLCIssue.ExtensionPeriod", TradePortalConstants.TEXT_BUNDLE, "en_US")).append(" : ").append(
					ReferenceDataManager.getRefDataMgr().getDescr(
							TradePortalConstants.AUTO_EXTEND_PERIOD_TYPE, autoExtPeriod, resourceManager.getResourceLocale() ));
			downloadData.append(System.getProperty("line.separator"));
			downloadData.append(resourceManager.getText("ImportDLCIssue.NumOfDays", TradePortalConstants.TEXT_BUNDLE, "en_US")).append(" : ").append(autoExtDays);
			downloadData.append(System.getProperty("line.separator"));
			downloadData.append(resourceManager.getText("TermsBeanAlias.final_expiry_date", TradePortalConstants.TEXT_BUNDLE, "en_US")).append(" : ").append(finalExpiryDate);
			downloadData.append(System.getProperty("line.separator"));
			downloadData.append(resourceManager.getText("TermsBeanAlias.notify_bene_days", TradePortalConstants.TEXT_BUNDLE, "en_US")).append(" : ").append(notifyBeneDays);
			downloadData.append(System.getProperty("line.separator"));
		}
		
	} catch (AmsException e) {
		e.printStackTrace();
	}
		

}


/**
 * Download the Advising Bank data
 *
 * @param out
 * @param terms
 * @param resourceManager
 * @param resourceLocale
 * @param termsParty
 * @throws IOException
 * @throws AmsException
 */
private void downloadImportLCDataAdvisingBank(
	ServletOutputStream out,
	TermsWebBean terms,
	com.amsinc.ecsg.util.ResourceManager resourceManager,
	String resourceLocale,
	TermsPartyWebBean termsParty)
	throws IOException, AmsException {
	StringBuilder downloadData;
	boolean isValidTermsParty;
	String termsPartyOid;
	  // Retrieve the Advising Bank terms party from the database
	  termsPartyOid = terms.getAttribute("c_ThirdTermsParty");

	  if (!StringFunction.isBlank(termsPartyOid))
	  {
	     termsParty.setAttribute("terms_party_oid", termsPartyOid);
	     termsParty.getDataFromAppServer();

	     isValidTermsParty = true;
	  }
	  else
	  {
	     isValidTermsParty = false;
	  }

	  // Start retrieving all saved Advising Bank data of the Import LC being viewed
	  downloadData = new StringBuilder();
	  downloadData.append(resourceManager.getText("ImportDLCIssue.AdvisingBank", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	  downloadData.append(" - ");
	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
	  out.println("");

	  downloadData = new StringBuilder();
	  downloadData.append(resourceManager.getText("ImportDLCIssue.Name", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	  downloadData.append(" : ");
	  if (isValidTermsParty)
	  {
	     downloadData.append(termsParty.getAttribute("name"));
	  }
	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

	  downloadData = new StringBuilder();
	  downloadData.append(resourceManager.getText("ImportDLCIssue.PhoneNumber", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	  downloadData.append(" : ");
	  if (isValidTermsParty)
	  {
	     downloadData.append(termsParty.getAttribute("phone_number"));
	  }
	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

	  downloadData = new StringBuilder();
	  downloadData.append(resourceManager.getText("ImportDLCIssue.AddressLine1", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	  downloadData.append(" : ");
	  if (isValidTermsParty)
	  {
	     downloadData.append(termsParty.getAttribute("address_line_1"));
	  }
	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

	  downloadData = new StringBuilder();
	  downloadData.append(resourceManager.getText("ImportDLCIssue.AddressLine2", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	  downloadData.append(" : ");
	  if (isValidTermsParty)
	  {
	     downloadData.append(termsParty.getAttribute("address_line_2"));
	  }
	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

	  downloadData = new StringBuilder();
	  downloadData.append(resourceManager.getText("ImportDLCIssue.City", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	  downloadData.append(" : ");
	  if (isValidTermsParty)
	  {
	     downloadData.append(termsParty.getAttribute("address_city"));
	  }
	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

	  downloadData = new StringBuilder();
	  downloadData.append(resourceManager.getText("ImportDLCIssue.ProvinceState", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	  downloadData.append(" : ");
	  if (isValidTermsParty)
	  {
	     downloadData.append(termsParty.getAttribute("address_state_province"));
	  }
	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

	  downloadData = new StringBuilder();
	  downloadData.append(resourceManager.getText("ImportDLCIssue.Country", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	  downloadData.append(" : ");
	  if (isValidTermsParty && (!StringFunction.isBlank(termsParty.getAttribute("address_country"))))
	  {
	     downloadData.append(ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.COUNTRY,
	                                                                       termsParty.getAttribute("address_country"),
	                                                                       resourceLocale));
	  }
	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

	  downloadData = new StringBuilder();
	  downloadData.append(resourceManager.getText("ImportDLCIssue.PostalCode", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	  downloadData.append(" : ");
	  if (isValidTermsParty)
	  {
	     downloadData.append(termsParty.getAttribute("address_postal_code"));
	  }
	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
	  out.println("");

}

/**
 * Download the Applicant Bank data
 *
 * @param out
 * @param terms
 * @param resourceManager
 * @param resourceLocale
 * @param termsParty
 * @throws IOException
 * @throws AmsException
 */
private void downloadRequestAdviseDataApplicantBank(
	ServletOutputStream out,
	TermsWebBean terms,
	com.amsinc.ecsg.util.ResourceManager resourceManager,
	String resourceLocale,
	TermsPartyWebBean termsParty)
	throws IOException, AmsException {
	StringBuilder downloadData;
	boolean isValidTermsParty;
	String termsPartyOid;
	  // Retrieve the Applicant Bank terms party from the database
	  termsPartyOid = terms.getAttribute("c_ThirdTermsParty");

	  if (!StringFunction.isBlank(termsPartyOid))
	  {
	     termsParty.setAttribute("terms_party_oid", termsPartyOid);
	     termsParty.getDataFromAppServer();

	     isValidTermsParty = true;
	  }
	  else
	  {
	     isValidTermsParty = false;
	  }

	  // Start retrieving all saved Advising Bank data of the Import LC being viewed
	  downloadData = new StringBuilder();
	  downloadData.append(resourceManager.getText("RequestAdviseIssue.AdvisingBank", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	  downloadData.append(" - ");
	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
	  out.println("");

	  downloadData = new StringBuilder();
	  downloadData.append(resourceManager.getText("RequestAdviseIssue.Name", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	  downloadData.append(" : ");
	  if (isValidTermsParty)
	  {
	     downloadData.append(termsParty.getAttribute("name"));
	  }
	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

	  downloadData = new StringBuilder();
	  downloadData.append(resourceManager.getText("RequestAdviseIssue.PhoneNumber", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	  downloadData.append(" : ");
	  if (isValidTermsParty)
	  {
	     downloadData.append(termsParty.getAttribute("phone_number"));
	  }
	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

	  downloadData = new StringBuilder();
	  downloadData.append(resourceManager.getText("RequestAdviseIssue.AddressLine1", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	  downloadData.append(" : ");
	  if (isValidTermsParty)
	  {
	     downloadData.append(termsParty.getAttribute("address_line_1"));
	  }
	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

	  downloadData = new StringBuilder();
	  downloadData.append(resourceManager.getText("RequestAdviseIssue.AddressLine2", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	  downloadData.append(" : ");
	  if (isValidTermsParty)
	  {
	     downloadData.append(termsParty.getAttribute("address_line_2"));
	  }
	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

	  downloadData = new StringBuilder();
	  downloadData.append(resourceManager.getText("RequestAdviseIssue.City", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	  downloadData.append(" : ");
	  if (isValidTermsParty)
	  {
	     downloadData.append(termsParty.getAttribute("address_city"));
	  }
	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

	  downloadData = new StringBuilder();
	  downloadData.append(resourceManager.getText("RequestAdviseIssue.ProvinceState", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	  downloadData.append(" : ");
	  if (isValidTermsParty)
	  {
	     downloadData.append(termsParty.getAttribute("address_state_province"));
	  }
	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

	  downloadData = new StringBuilder();
	  downloadData.append(resourceManager.getText("RequestAdviseIssue.Country", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	  downloadData.append(" : ");
	  if (isValidTermsParty && (!StringFunction.isBlank(termsParty.getAttribute("address_country"))))
	  {
	     downloadData.append(ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.COUNTRY,
	                                                                       termsParty.getAttribute("address_country"),
	                                                                       resourceLocale));
	  }
	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

	  downloadData = new StringBuilder();
	  downloadData.append(resourceManager.getText("RequestAdviseIssue.PostalCode", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	  downloadData.append(" : ");
	  if (isValidTermsParty)
	  {
	     downloadData.append(termsParty.getAttribute("address_postal_code"));
	  }
	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
	  out.println("");

}

/**
 * Download the Advise Through Bank data
 *
 * @param out
 * @param terms
 * @param resourceManager
 * @param resourceLocale
 * @param termsParty
 * @throws IOException
 * @throws AmsException
 */
private void downloadRequestAdviseDataAdviseThroughBank(
	ServletOutputStream out,
	TermsWebBean terms,
	com.amsinc.ecsg.util.ResourceManager resourceManager,
	String resourceLocale,
	TermsPartyWebBean termsParty)
	throws IOException, AmsException {
	StringBuilder downloadData;
	boolean isValidTermsParty;
	String termsPartyOid;
	  // Retrieve the Advise Through Bank terms party from the database
	  termsPartyOid = terms.getAttribute("c_FourthTermsParty");

	  if (!StringFunction.isBlank(termsPartyOid))
	  {
	     termsParty.setAttribute("terms_party_oid", termsPartyOid);
	     termsParty.getDataFromAppServer();

	     isValidTermsParty = true;
	  }
	  else
	  {
	     isValidTermsParty = false;
	  }

	  // Start retrieving all saved Advise Through Bank data of the Request to Advise being viewed
	  downloadData = new StringBuilder();
	  downloadData.append(resourceManager.getText("RequestAdviseIssue.AdviseThroughBank", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	  downloadData.append(" - ");
	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
	  out.println("");

	  downloadData = new StringBuilder();
	  downloadData.append(resourceManager.getText("RequestAdviseIssue.Name", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	  downloadData.append(" : ");
	  if (isValidTermsParty)
	  {
	     downloadData.append(termsParty.getAttribute("name"));
	  }
	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

	  downloadData = new StringBuilder();
	  downloadData.append(resourceManager.getText("RequestAdviseIssue.PhoneNumber", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	  downloadData.append(" : ");
	  if (isValidTermsParty)
	  {
	     downloadData.append(termsParty.getAttribute("phone_number"));
	  }
	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

	  downloadData = new StringBuilder();
	  downloadData.append(resourceManager.getText("RequestAdviseIssue.AddressLine1", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	  downloadData.append(" : ");
	  if (isValidTermsParty)
	  {
	     downloadData.append(termsParty.getAttribute("address_line_1"));
	  }
	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

	  downloadData = new StringBuilder();
	  downloadData.append(resourceManager.getText("RequestAdviseIssue.AddressLine2", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	  downloadData.append(" : ");
	  if (isValidTermsParty)
	  {
	     downloadData.append(termsParty.getAttribute("address_line_2"));
	  }
	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

	  downloadData = new StringBuilder();
	  downloadData.append(resourceManager.getText("RequestAdviseIssue.City", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	  downloadData.append(" : ");
	  if (isValidTermsParty)
	  {
	     downloadData.append(termsParty.getAttribute("address_city"));
	  }
	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

	  downloadData = new StringBuilder();
	  downloadData.append(resourceManager.getText("RequestAdviseIssue.ProvinceState", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	  downloadData.append(" : ");
	  if (isValidTermsParty)
	  {
	     downloadData.append(termsParty.getAttribute("address_state_province"));
	  }
	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

	  downloadData = new StringBuilder();
	  downloadData.append(resourceManager.getText("RequestAdviseIssue.Country", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	  downloadData.append(" : ");
	  if (isValidTermsParty && (!StringFunction.isBlank(termsParty.getAttribute("address_country"))))
	  {
	     downloadData.append(ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.COUNTRY,
	                                                                       termsParty.getAttribute("address_country"),
	                                                                       resourceLocale));
	  }
	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

	  downloadData = new StringBuilder();
	  downloadData.append(resourceManager.getText("RequestAdviseIssue.PostalCode", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	  downloadData.append(" : ");
	  if (isValidTermsParty)
	  {
	     downloadData.append(termsParty.getAttribute("address_postal_code"));
	  }
	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
	  out.println("");

}

/**
 * Download the Reimbursing Bank data
 *
 * @param out
 * @param terms
 * @param resourceManager
 * @param resourceLocale
 * @param termsParty
 * @throws IOException
 * @throws AmsException
 */
private void downloadRequestAdviseDataReimbursingBank(
	ServletOutputStream out,
	TermsWebBean terms,
	com.amsinc.ecsg.util.ResourceManager resourceManager,
	String resourceLocale,
	TermsPartyWebBean termsParty)
	throws IOException, AmsException {
	StringBuilder downloadData;
	boolean isValidTermsParty;
	String termsPartyOid;
	  // Retrieve the Reimbursing Bank terms party from the database
	  termsPartyOid = terms.getAttribute("c_FifthTermsParty");

	  if (!StringFunction.isBlank(termsPartyOid))
	  {
	     termsParty.setAttribute("terms_party_oid", termsPartyOid);
	     termsParty.getDataFromAppServer();

	     isValidTermsParty = true;
	  }
	  else
	  {
	     isValidTermsParty = false;
	  }

	  // Start retrieving all saved Reimbursing Bank data of the Request to Advise being viewed
	  downloadData = new StringBuilder();
	  downloadData.append(resourceManager.getText("RequestAdviseIssue.ReimbursingBank", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	  downloadData.append(" - ");
	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
	  out.println("");

	  downloadData = new StringBuilder();
	  downloadData.append(resourceManager.getText("RequestAdviseIssue.Name", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	  downloadData.append(" : ");
	  if (isValidTermsParty)
	  {
	     downloadData.append(termsParty.getAttribute("name"));
	  }
	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

	  downloadData = new StringBuilder();
	  downloadData.append(resourceManager.getText("RequestAdviseIssue.PhoneNumber", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	  downloadData.append(" : ");
	  if (isValidTermsParty)
	  {
	     downloadData.append(termsParty.getAttribute("phone_number"));
	  }
	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

	  downloadData = new StringBuilder();
	  downloadData.append(resourceManager.getText("RequestAdviseIssue.AddressLine1", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	  downloadData.append(" : ");
	  if (isValidTermsParty)
	  {
	     downloadData.append(termsParty.getAttribute("address_line_1"));
	  }
	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

	  downloadData = new StringBuilder();
	  downloadData.append(resourceManager.getText("RequestAdviseIssue.AddressLine2", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	  downloadData.append(" : ");
	  if (isValidTermsParty)
	  {
	     downloadData.append(termsParty.getAttribute("address_line_2"));
	  }
	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

	  downloadData = new StringBuilder();
	  downloadData.append(resourceManager.getText("RequestAdviseIssue.City", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	  downloadData.append(" : ");
	  if (isValidTermsParty)
	  {
	     downloadData.append(termsParty.getAttribute("address_city"));
	  }
	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

	  downloadData = new StringBuilder();
	  downloadData.append(resourceManager.getText("RequestAdviseIssue.ProvinceState", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	  downloadData.append(" : ");
	  if (isValidTermsParty)
	  {
	     downloadData.append(termsParty.getAttribute("address_state_province"));
	  }
	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

	  downloadData = new StringBuilder();
	  downloadData.append(resourceManager.getText("RequestAdviseIssue.Country", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	  downloadData.append(" : ");
	  if (isValidTermsParty && (!StringFunction.isBlank(termsParty.getAttribute("address_country"))))
	  {
	     downloadData.append(ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.COUNTRY,
	                                                                       termsParty.getAttribute("address_country"),
	                                                                       resourceLocale));
	  }
	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

	  downloadData = new StringBuilder();
	  downloadData.append(resourceManager.getText("RequestAdviseIssue.PostalCode", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	  downloadData.append(" : ");
	  if (isValidTermsParty)
	  {
	     downloadData.append(termsParty.getAttribute("address_postal_code"));
	  }
	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
	  out.println("");

}

/**
 * Download the Issuer data
 *
 * @param out
 * @param terms
 * @param resourceManager
 * @param resourceLocale
 * @param termsParty
 * @throws IOException
 * @throws AmsException
 */
private void downloadRequestAdviseDataIssuer(
	ServletOutputStream out,
	TermsWebBean terms,
	com.amsinc.ecsg.util.ResourceManager resourceManager,
	String resourceLocale,
	TermsPartyWebBean termsParty)
	throws IOException, AmsException {
	StringBuilder downloadData;
	boolean isValidTermsParty;
	String termsPartyOid;
	  // Retrieve the Issuer terms party from the database
	  termsPartyOid = terms.getAttribute("c_SixthTermsParty");

	  if (!StringFunction.isBlank(termsPartyOid))
	  {
	     termsParty.setAttribute("terms_party_oid", termsPartyOid);
	     termsParty.getDataFromAppServer();

	     isValidTermsParty = true;
	  }
	  else
	  {
	     isValidTermsParty = false;
	  }

	  // Start retrieving all saved Issuer data of the Request to Advise being viewed
	  downloadData = new StringBuilder();
	  downloadData.append(resourceManager.getText("RequestAdviseIssue.Issuer", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	  downloadData.append(" - ");
	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
	  out.println("");

	  downloadData = new StringBuilder();
	  downloadData.append(resourceManager.getText("RequestAdviseIssue.Name", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	  downloadData.append(" : ");
	  if (isValidTermsParty)
	  {
	     downloadData.append(termsParty.getAttribute("name"));
	  }
	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

	  downloadData = new StringBuilder();
	  downloadData.append(resourceManager.getText("RequestAdviseIssue.PhoneNumber", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	  downloadData.append(" : ");
	  if (isValidTermsParty)
	  {
	     downloadData.append(termsParty.getAttribute("phone_number"));
	  }
	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

	  downloadData = new StringBuilder();
	  downloadData.append(resourceManager.getText("RequestAdviseIssue.AddressLine1", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	  downloadData.append(" : ");
	  if (isValidTermsParty)
	  {
	     downloadData.append(termsParty.getAttribute("address_line_1"));
	  }
	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

	  downloadData = new StringBuilder();
	  downloadData.append(resourceManager.getText("RequestAdviseIssue.AddressLine2", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	  downloadData.append(" : ");
	  if (isValidTermsParty)
	  {
	     downloadData.append(termsParty.getAttribute("address_line_2"));
	  }
	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

	  downloadData = new StringBuilder();
	  downloadData.append(resourceManager.getText("RequestAdviseIssue.City", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	  downloadData.append(" : ");
	  if (isValidTermsParty)
	  {
	     downloadData.append(termsParty.getAttribute("address_city"));
	  }
	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

	  downloadData = new StringBuilder();
	  downloadData.append(resourceManager.getText("RequestAdviseIssue.ProvinceState", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	  downloadData.append(" : ");
	  if (isValidTermsParty)
	  {
	     downloadData.append(termsParty.getAttribute("address_state_province"));
	  }
	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

	  downloadData = new StringBuilder();
	  downloadData.append(resourceManager.getText("RequestAdviseIssue.Country", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	  downloadData.append(" : ");
	  if (isValidTermsParty && (!StringFunction.isBlank(termsParty.getAttribute("address_country"))))
	  {
	     downloadData.append(ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.COUNTRY,
	                                                                       termsParty.getAttribute("address_country"),
	                                                                       resourceLocale));
	  }
	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

	  downloadData = new StringBuilder();
	  downloadData.append(resourceManager.getText("RequestAdviseIssue.PostalCode", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	  downloadData.append(" : ");
	  if (isValidTermsParty)
	  {
	     downloadData.append(termsParty.getAttribute("address_postal_code"));
	  }
	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
	  out.println("");

}

/**
 * Download the Applicant data
 * @param out
 * @param terms
 * @param resourceManager
 * @param resourceLocale
 * @param termsParty
 * @throws IOException
 * @throws AmsException
 */
private void downloadImportLCDataApplicant(
	ServletOutputStream out,
	TermsWebBean terms,
	com.amsinc.ecsg.util.ResourceManager resourceManager,
	String resourceLocale,
	TermsPartyWebBean termsParty)
	throws IOException, AmsException {
	StringBuilder downloadData;
	boolean isValidTermsParty;
	String termsPartyOid;
	  // Retrieve the Applicant terms party from the database
	  termsPartyOid = terms.getAttribute("c_SecondTermsParty");

	  if (!StringFunction.isBlank(termsPartyOid))
	  {
	     termsParty.setAttribute("terms_party_oid", termsPartyOid);
	     termsParty.getDataFromAppServer();

	     isValidTermsParty = true;
	  }
	  else
	  {
	     isValidTermsParty = false;
	  }

	  // Start retrieving all saved Applicant data of the Import LC being viewed
	  downloadData = new StringBuilder();
	  downloadData.append(resourceManager.getText("ImportDLCIssue.Applicant", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	  downloadData.append(" - ");
	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
	  out.println("");

	  downloadData = new StringBuilder();
	  downloadData.append(resourceManager.getText("ImportDLCIssue.Name", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	  downloadData.append(" : ");
	  if (isValidTermsParty)
	  {
	     downloadData.append(termsParty.getAttribute("name"));
	  }
	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

	  downloadData = new StringBuilder();
	  downloadData.append(resourceManager.getText("ImportDLCIssue.PhoneNumber", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	  downloadData.append(" : ");
	  if (isValidTermsParty)
	  {
	     downloadData.append(termsParty.getAttribute("phone_number"));
	  }
	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

	  downloadData = new StringBuilder();
	  downloadData.append(resourceManager.getText("ImportDLCIssue.AddressLine1", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	  downloadData.append(" : ");
	  if (isValidTermsParty)
	  {
	     downloadData.append(termsParty.getAttribute("address_line_1"));
	  }
	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

	  downloadData = new StringBuilder();
	  downloadData.append(resourceManager.getText("ImportDLCIssue.AddressLine2", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	  downloadData.append(" : ");
	  if (isValidTermsParty)
	  {
	     downloadData.append(termsParty.getAttribute("address_line_2"));
	  }
	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

	  downloadData = new StringBuilder();
	  downloadData.append(resourceManager.getText("ImportDLCIssue.City", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	  downloadData.append(" : ");
	  if (isValidTermsParty)
	  {
	     downloadData.append(termsParty.getAttribute("address_city"));
	  }
	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

	  downloadData = new StringBuilder();
	  downloadData.append(resourceManager.getText("ImportDLCIssue.ProvinceState", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	  downloadData.append(" : ");
	  if (isValidTermsParty)
	  {
	     downloadData.append(termsParty.getAttribute("address_state_province"));
	  }
	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

	  downloadData = new StringBuilder();
	  downloadData.append(resourceManager.getText("ImportDLCIssue.Country", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	  downloadData.append(" : ");
	  if (isValidTermsParty && (!StringFunction.isBlank(termsParty.getAttribute("address_country"))))
	  {
	     downloadData.append(ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.COUNTRY,
	                                                                       termsParty.getAttribute("address_country"),
	                                                                       resourceLocale));
	  }
	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

	  downloadData = new StringBuilder();
	  downloadData.append(resourceManager.getText("ImportDLCIssue.PostalCode", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	  downloadData.append(" : ");
	  if (isValidTermsParty)
	  {
	     downloadData.append(termsParty.getAttribute("address_postal_code"));
	  }
	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
	  out.println("");

}

/**
 * Download the Beneficiary data
 *
 * @param out
 * @param terms
 * @param resourceManager
 * @param isValidTermsParty
 * @param resourceLocale
 * @param termsParty
 * @throws IOException
 * @throws AmsException
 */
private void downloadImportLCDataBeneficiary(
	ServletOutputStream out,
	TermsWebBean terms,
	com.amsinc.ecsg.util.ResourceManager resourceManager,
	boolean isValidTermsParty,
	String resourceLocale,
	TermsPartyWebBean termsParty)
	throws IOException, AmsException {
	StringBuilder downloadData;
	String termsPartyOid;
	  // Retrieve the Beneficiary terms party from the database
	  termsPartyOid = terms.getAttribute("c_FirstTermsParty");


	  if (!StringFunction.isBlank(termsPartyOid))
	  {
	     termsParty.setAttribute("terms_party_oid", termsPartyOid);
	     termsParty.getDataFromAppServer();

	     isValidTermsParty = true;
	  }

	  // Start retrieving all saved Beneficiary data of the Import LC being viewed
	  downloadData = new StringBuilder();
	  downloadData.append(resourceManager.getText("ImportDLCIssue.Beneficiary", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	  downloadData.append(" - ");
	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
	  out.println("");

	  downloadData = new StringBuilder();
	  downloadData.append(resourceManager.getText("ImportDLCIssue.Name", TradePortalConstants.TEXT_BUNDLE,"en_US"));
	  downloadData.append(" : ");
	  if (isValidTermsParty)
	  {
	     downloadData.append(termsParty.getAttribute("name"));
	  }
	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

	  downloadData = new StringBuilder();
	  downloadData.append(resourceManager.getText("ImportDLCIssue.PhoneNumber", TradePortalConstants.TEXT_BUNDLE,"en_US"));
	  downloadData.append(" : ");
	  if (isValidTermsParty)
	  {
	     downloadData.append(termsParty.getAttribute("phone_number"));
	  }
	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

	  downloadData = new StringBuilder();
	  downloadData.append(resourceManager.getText("ImportDLCIssue.AddressLine1", TradePortalConstants.TEXT_BUNDLE,"en_US"));
	  downloadData.append(" : ");
	  if (isValidTermsParty)
	  {
	     downloadData.append(termsParty.getAttribute("address_line_1"));
	  }
	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

	  downloadData = new StringBuilder();
	  downloadData.append(resourceManager.getText("ImportDLCIssue.AddressLine2", TradePortalConstants.TEXT_BUNDLE,"en_US"));
	  downloadData.append(" : ");
	  if (isValidTermsParty)
	  {
	     downloadData.append(termsParty.getAttribute("address_line_2"));
	  }
	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

	  downloadData = new StringBuilder();
	  downloadData.append(resourceManager.getText("ImportDLCIssue.City", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	  downloadData.append(" : ");
	  if (isValidTermsParty)
	  {
	     downloadData.append(termsParty.getAttribute("address_city"));
	  }
	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

	  downloadData = new StringBuilder();
	  downloadData.append(resourceManager.getText("ImportDLCIssue.ProvinceState", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	  downloadData.append(" : ");
	  if (isValidTermsParty)
	  {
	     downloadData.append(termsParty.getAttribute("address_state_province"));
	  }
	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

	  downloadData = new StringBuilder();
	  downloadData.append(resourceManager.getText("ImportDLCIssue.Country", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	  downloadData.append(" : ");
	  if (isValidTermsParty && (!StringFunction.isBlank(termsParty.getAttribute("address_country"))))
	  {
	     downloadData.append(ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.COUNTRY,
	                                                                       termsParty.getAttribute("address_country"),
	                                                                       resourceLocale));
	  }
	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

	  downloadData = new StringBuilder();
	  downloadData.append(resourceManager.getText("ImportDLCIssue.PostalCode", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	  downloadData.append(" : ");
	  if (isValidTermsParty)
	  {
	     downloadData.append(termsParty.getAttribute("address_postal_code"));
	  }
	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
	  out.println("");
}

/**
 * Download the data of the required documents
 *
 * @param out
 * @param terms
 * @param resourceManager
 * @param resourceLocale
 * @throws IOException
 * @throws AmsException
 */
private void downloadImportLCDataDocs(
	ServletOutputStream out,
	TermsWebBean terms,
	com.amsinc.ecsg.util.ResourceManager resourceManager,
	String resourceLocale)
	throws IOException, AmsException {
	StringBuilder downloadData;
	  // *****************DOCS REQUIRED SECTION***********************************//
	  // Retrieve all information pertaining to any necessary documents required for the Import LC instrument
	  if (TradePortalConstants.INDICATOR_YES.equals(terms.getAttribute("comm_invoice_indicator")))
	  {
	     downloadData = new StringBuilder();
	     downloadData.append(resourceManager.getText("ImportDLCIssue.CommercialInv", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	     downloadData.append(" - ");
	     out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

	     downloadData = new StringBuilder();
	     downloadData.append(resourceManager.getText("ImportDLCIssue.Originals", TradePortalConstants.TEXT_BUNDLE,"en_US"));
	     downloadData.append(" : ");
	     downloadData.append(terms.getAttribute("comm_invoice_originals"));
	     out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

	     downloadData = new StringBuilder();
	     downloadData.append(resourceManager.getText("ImportDLCIssue.Copies", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	     downloadData.append(" : ");
	     downloadData.append(terms.getAttribute("comm_invoice_copies"));
	     out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

	     downloadData = new StringBuilder();
	     downloadData.append(resourceManager.getText("ImportDLCIssue.DocumentText", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	     downloadData.append(" : ");
	     downloadData.append(terms.getAttribute("comm_invoice_text"));
	     out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
	     out.println("");
	  }

	  if (TradePortalConstants.INDICATOR_YES.equals(terms.getAttribute("packing_list_indicator")))
	  {
	     downloadData = new StringBuilder();
	     downloadData.append(resourceManager.getText("ImportDLCIssue.PackingList", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	     downloadData.append(" - ");
	     out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

	     downloadData = new StringBuilder();
	     downloadData.append(resourceManager.getText("ImportDLCIssue.Originals", TradePortalConstants.TEXT_BUNDLE,"en_US"));
	     downloadData.append(" : ");
	     downloadData.append(terms.getAttribute("packing_list_originals"));
	     out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

	     downloadData = new StringBuilder();
	     downloadData.append(resourceManager.getText("ImportDLCIssue.Copies", TradePortalConstants.TEXT_BUNDLE,"en_US"));
	     downloadData.append(" : ");
	     downloadData.append(terms.getAttribute("packing_list_copies"));
	     out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

	     downloadData = new StringBuilder();
	     downloadData.append(resourceManager.getText("ImportDLCIssue.DocumentText", TradePortalConstants.TEXT_BUNDLE,"en_US"));
	     downloadData.append(" : ");
	     downloadData.append(terms.getAttribute("packing_list_text"));
	     out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
	     out.println("");
	  }

	  if (TradePortalConstants.INDICATOR_YES.equals(terms.getAttribute("cert_origin_indicator")))
	  {
	     downloadData = new StringBuilder();
	     downloadData.append(resourceManager.getText("ImportDLCIssue.OriginCertificate",
	                                                 TradePortalConstants.TEXT_BUNDLE,"en_US"));
	     downloadData.append(" - ");
	     out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

	     downloadData = new StringBuilder();
	     downloadData.append(resourceManager.getText("ImportDLCIssue.Originals", TradePortalConstants.TEXT_BUNDLE,"en_US"));
	     downloadData.append(" : ");
	     downloadData.append(terms.getAttribute("cert_origin_originals"));
	     out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

	     downloadData = new StringBuilder();
	     downloadData.append(resourceManager.getText("ImportDLCIssue.Copies", TradePortalConstants.TEXT_BUNDLE,"en_US"));
	     downloadData.append(" : ");
	     downloadData.append(terms.getAttribute("cert_origin_copies"));
	     out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

	     downloadData = new StringBuilder();
	     downloadData.append(resourceManager.getText("ImportDLCIssue.DocumentText", TradePortalConstants.TEXT_BUNDLE,"en_US"));
	     downloadData.append(" : ");
	     downloadData.append(terms.getAttribute("cert_origin_text"));
	     out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
	     out.println("");
	  }

	  if (TradePortalConstants.INDICATOR_YES.equals(terms.getAttribute("ins_policy_indicator")))
	  {
	     downloadData = new StringBuilder();
	     downloadData.append(resourceManager.getText("ImportDLCIssue.InsPolicyCert",
	                                                 TradePortalConstants.TEXT_BUNDLE,"en_US"));
	     downloadData.append(" - ");
	     out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

	     downloadData = new StringBuilder();
	     downloadData.append(resourceManager.getText("ImportDLCIssue.Originals", TradePortalConstants.TEXT_BUNDLE,"en_US"));
	     downloadData.append(" : ");
	     downloadData.append(terms.getAttribute("ins_policy_originals"));
	     out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

	     downloadData = new StringBuilder();
	     downloadData.append(resourceManager.getText("ImportDLCIssue.Copies", TradePortalConstants.TEXT_BUNDLE,"en_US"));
	     downloadData.append(" : ");
	     downloadData.append(terms.getAttribute("ins_policy_copies"));
	     out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

	     downloadData = new StringBuilder();
	     downloadData.append(resourceManager.getText("ImportDLCIssue.Endorsement", TradePortalConstants.TEXT_BUNDLE,"en_US"));
	     downloadData.append(" : ");
	     downloadData.append(resourceManager.getText("ImportDLCIssue.EndorsedInBlank",
	                                                 TradePortalConstants.TEXT_BUNDLE,"en_US"));
	     downloadData.append(" ");
	     downloadData.append(terms.getAttribute("insurance_endorse_value_plus"));
	     downloadData.append(resourceManager.getText("ImportDLCIssue.Covering", TradePortalConstants.TEXT_BUNDLE,"en_US"));
	     downloadData.append(" ");

	     if (!StringFunction.isBlank(terms.getAttribute("insurance_risk_type")))
	     {
	        downloadData.append(ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.INSURANCE_RISK_TYPE,
	                                                                          terms.getAttribute("insurance_risk_type"),
	                                                                          resourceLocale));
	     }
	     out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

	     downloadData = new StringBuilder();
	     downloadData.append(resourceManager.getText("ImportDLCIssue.DocumentText", TradePortalConstants.TEXT_BUNDLE,"en_US"));
	     downloadData.append(" : ");
	     downloadData.append(terms.getAttribute("ins_policy_text"));
	     out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
	     out.println("");
	  }

	  // This next part includes the four user-specified documents

	  if (TradePortalConstants.INDICATOR_YES.equals(terms.getAttribute("other_req_doc_1_indicator")))
	  {
	     downloadData = new StringBuilder();
	     downloadData.append(terms.getAttribute("other_req_doc_1_name"));
	     downloadData.append(" - ");
	     out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

	     downloadData = new StringBuilder();
	     downloadData.append(resourceManager.getText("ImportDLCIssue.Originals", TradePortalConstants.TEXT_BUNDLE,"en_US"));
	     downloadData.append(" : ");
	     downloadData.append(terms.getAttribute("other_req_doc_1_originals"));
	     out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

	     downloadData = new StringBuilder();
	     downloadData.append(resourceManager.getText("ImportDLCIssue.Copies", TradePortalConstants.TEXT_BUNDLE,"en_US"));
	     downloadData.append(" : ");
	     downloadData.append(terms.getAttribute("other_req_doc_1_copies"));
	     out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

	     downloadData = new StringBuilder();
	     downloadData.append(resourceManager.getText("ImportDLCIssue.DocumentText", TradePortalConstants.TEXT_BUNDLE,"en_US"));
	     downloadData.append(" : ");
	     downloadData.append(terms.getAttribute("other_req_doc_1_text"));
	     out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
	     out.println("");
	  }

	  if (TradePortalConstants.INDICATOR_YES.equals(terms.getAttribute("other_req_doc_2_indicator")))
	  {
	     downloadData = new StringBuilder();
	     downloadData.append(terms.getAttribute("other_req_doc_2_name"));
	     downloadData.append(" - ");
	     out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

	     downloadData = new StringBuilder();
	     downloadData.append(resourceManager.getText("ImportDLCIssue.Originals", TradePortalConstants.TEXT_BUNDLE,"en_US"));
	     downloadData.append(" : ");
	     downloadData.append(terms.getAttribute("other_req_doc_2_originals"));
	     out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

	     downloadData = new StringBuilder();
	     downloadData.append(resourceManager.getText("ImportDLCIssue.Copies", TradePortalConstants.TEXT_BUNDLE,"en_US"));
	     downloadData.append(" : ");
	     downloadData.append(terms.getAttribute("other_req_doc_2_copies"));
	     out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

	     downloadData = new StringBuilder();
	     downloadData.append(resourceManager.getText("ImportDLCIssue.DocumentText", TradePortalConstants.TEXT_BUNDLE,"en_US"));
	     downloadData.append(" : ");
	     downloadData.append(terms.getAttribute("other_req_doc_2_text"));
	     out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
	     out.println("");
	  }

	  if (TradePortalConstants.INDICATOR_YES.equals(terms.getAttribute("other_req_doc_3_indicator")))
	  {
	     downloadData = new StringBuilder();
	     downloadData.append(terms.getAttribute("other_req_doc_3_name"));
	     downloadData.append(" - ");
	     out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

	     downloadData = new StringBuilder();
	     downloadData.append(resourceManager.getText("ImportDLCIssue.Originals", TradePortalConstants.TEXT_BUNDLE,"en_US"));
	     downloadData.append(" : ");
	     downloadData.append(terms.getAttribute("other_req_doc_3_originals"));
	     out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

	     downloadData = new StringBuilder();
	     downloadData.append(resourceManager.getText("ImportDLCIssue.Copies", TradePortalConstants.TEXT_BUNDLE,"en_US"));
	     downloadData.append(" : ");
	     downloadData.append(terms.getAttribute("other_req_doc_3_copies"));
	     out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

	     downloadData = new StringBuilder();
	     downloadData.append(resourceManager.getText("ImportDLCIssue.DocumentText", TradePortalConstants.TEXT_BUNDLE,"en_US"));
	     downloadData.append(" : ");
	     downloadData.append(terms.getAttribute("other_req_doc_3_text"));
	     out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
	     out.println("");
	  }



	  if (TradePortalConstants.INDICATOR_YES.equals(terms.getAttribute("other_req_doc_4_indicator")))
	  {
	     downloadData = new StringBuilder();
	     downloadData.append(terms.getAttribute("other_req_doc_4_name"));
	     downloadData.append(" - ");
	     out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

	     downloadData = new StringBuilder();
	     downloadData.append(resourceManager.getText("ImportDLCIssue.Originals", TradePortalConstants.TEXT_BUNDLE,"en_US"));
	     downloadData.append(" : ");
	     downloadData.append(terms.getAttribute("other_req_doc_4_originals"));
	     out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

	     downloadData = new StringBuilder();
	     downloadData.append(resourceManager.getText("ImportDLCIssue.Copies", TradePortalConstants.TEXT_BUNDLE,"en_US"));
	     downloadData.append(" : ");
	     downloadData.append(terms.getAttribute("other_req_doc_4_copies"));
	     out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

	     downloadData = new StringBuilder();
	     downloadData.append(resourceManager.getText("ImportDLCIssue.DocumentText", TradePortalConstants.TEXT_BUNDLE,"en_US"));
	     downloadData.append(" : ");
	     downloadData.append(terms.getAttribute("other_req_doc_4_text"));
	     out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
	     out.println("");
	  }

	  List<AmsEntityWebBean> additionalReqDocList = terms.getChildren("AdditionalReqDoc","addl_req_doc_oid","p_terms_oid"); 
	  for(AmsEntityWebBean additionalReqDoc : additionalReqDocList) {        
			String addlDocRequiredInd = additionalReqDoc.getAttribute("addl_req_doc_ind");
			  if (TradePortalConstants.INDICATOR_YES.equals(addlDocRequiredInd))
			  {
			     downloadData = new StringBuilder();
			     downloadData.append(additionalReqDoc.getAttribute("addl_req_doc_name"));
			     downloadData.append(" - ");
			     out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

			     downloadData = new StringBuilder();
			     downloadData.append(resourceManager.getText("ImportDLCIssue.Originals", TradePortalConstants.TEXT_BUNDLE, "en_US"));
			     downloadData.append(" : ");
			     downloadData.append(additionalReqDoc.getAttribute("addl_req_doc_originals"));
			     out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

			     downloadData = new StringBuilder();
			     downloadData.append(resourceManager.getText("ImportDLCIssue.Copies", TradePortalConstants.TEXT_BUNDLE, "en_US"));
			     downloadData.append(" : ");
			     downloadData.append(additionalReqDoc.getAttribute("addl_req_doc_copies"));
			     out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

			     downloadData = new StringBuilder();
			     downloadData.append(resourceManager.getText("ImportDLCIssue.DocumentText", TradePortalConstants.TEXT_BUNDLE, "en_US"));
			     downloadData.append(" : ");
			     downloadData.append(additionalReqDoc.getAttribute("addl_req_doc_text"));
			     out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
			     out.println("");
			  }

		}

	  if (TradePortalConstants.INDICATOR_YES.equals(terms.getAttribute("addl_doc_indicator")))
	  {
	     downloadData = new StringBuilder();
	     downloadData.append(resourceManager.getText("ImportDLCIssue.AddlDocs1", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	     downloadData.append(" ");
	     downloadData.append(resourceManager.getText("ImportDLCIssue.AddlDocs2", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	     downloadData.append(" - ");
	     out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

	     downloadData = new StringBuilder();
	     downloadData.append(resourceManager.getText("ImportDLCIssue.DocumentText", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	     downloadData.append(" : ");
	     downloadData.append(terms.getAttribute("addl_doc_text"));
	     out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
	     out.println("");
	  }
}

/**
 * Download the other conditions for LC Data
 *
 * @param out
 * @param terms
 * @param resourceManager
 * @throws IOException
 */
private void downloadImportLCDataOtherConditions(
	ServletOutputStream out,
	TermsWebBean terms,
	com.amsinc.ecsg.util.ResourceManager resourceManager)
	throws IOException, AmsException {
	StringBuilder downloadData;
	String confirmationType;
	  //*****************************OTHER CONDITIONS SUBSECTION**************************************/
	  // Retrieve all saved other conditions data for the Issue Import LC transaction
	  downloadData = new StringBuilder();
	  downloadData.append(resourceManager.getText("ImportDLCIssue.DownloadOtherConditions",
	                                              TradePortalConstants.TEXT_BUNDLE, "en_US"));
	  downloadData.append(" - ");
	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
	  out.println("");

	  downloadData = new StringBuilder();
	  downloadData.append(resourceManager.getText("ImportDLCIssue.Confirmation", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	  downloadData.append(" : ");

	  confirmationType = terms.getAttribute("confirmation_type");
	  if (StringFunction.isBlank(confirmationType)){
		  confirmationType = "";
	  }
	  if (confirmationType.equals(TradePortalConstants.CONFIRM_NOT_REQD))
	  {
		  String strConfirmNotrequired=resourceManager.getText("ImportDLCIssue.ConfirmNotReqd1", TradePortalConstants.TEXT_BUNDLE, "en_US");
		  if (strConfirmNotrequired!=null){
			  strConfirmNotrequired=strConfirmNotrequired.replaceAll("<b>", "");
			  strConfirmNotrequired=strConfirmNotrequired.replaceAll("</b>", "");
			  downloadData.append(strConfirmNotrequired);
		  }

            
	  }
	  else if (confirmationType.equals(TradePortalConstants.BANK_TO_ADD))
	  {
		  String strBankAdd1=resourceManager.getText("ImportDLCIssue.BankToAdd1", TradePortalConstants.TEXT_BUNDLE, "en_US");
		  if (strBankAdd1!=null){
			  strBankAdd1=strBankAdd1.replaceAll("<b>", "");
			  strBankAdd1=strBankAdd1.replaceAll("</b>", "");
			  downloadData.append(strBankAdd1);
		  }
         
	  }
	  else if (confirmationType.equals(TradePortalConstants.BANK_MAY_ADD))
	  {
		  String strBankMayAdd=resourceManager.getText("ImportDLCIssue.BankMayAdd1", TradePortalConstants.TEXT_BUNDLE, "en_US");

		  if (strBankMayAdd!=null){
			  strBankMayAdd=strBankMayAdd.replaceAll("<b>", "");
			  strBankMayAdd=strBankMayAdd.replaceAll("</b>", "");
			  downloadData.append(strBankMayAdd);
		  }
          
	  }
	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

	  downloadData = new StringBuilder();
	  downloadData.append(resourceManager.getText("ImportDLCIssue.Transferable", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	  downloadData.append(" : ");

	  if (TradePortalConstants.INDICATOR_YES.equals(terms.getAttribute("transferrable")))
	  {
	     downloadData.append(resourceManager.getText("common.Yes", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	  }
	  else
	  {
	     downloadData.append(resourceManager.getText("common.No", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	  }
	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

	  downloadData = new StringBuilder();
	  downloadData.append(resourceManager.getText("ImportDLCIssue.DownloadRevolve", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	  downloadData.append(" : ");

	  if (TradePortalConstants.INDICATOR_YES.equals(terms.getAttribute("revolve")))
	  {
	     downloadData.append(resourceManager.getText("common.Yes", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	  }
	  else
	  {
	     downloadData.append(resourceManager.getText("common.No", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	  }
	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
	  out.println("");

	//Sandeep - Rel-8.3 CR-752 Dev 06/11/2013 - Begin
	  if (TradePortalConstants.INDICATOR_YES.equals(terms.getAttribute("ucp_version_details_ind"))){
		  downloadData = new StringBuilder();
		  downloadData.append(resourceManager.getText("common.ICCPublications", TradePortalConstants.TEXT_BUNDLE, "en_US"));
		  downloadData.append(" - ");
		  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
		  
		  downloadData = new StringBuilder();
		  downloadData.append(resourceManager.getText("common.ICCApplicableRules", TradePortalConstants.TEXT_BUNDLE, "en_US"));
		  downloadData.append(" : ");
		  downloadData.append(resourceManager.getText("common.Yes", TradePortalConstants.TEXT_BUNDLE, "en_US"));
		  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
		  
		  downloadData = new StringBuilder();
		  downloadData.append(resourceManager.getText("common.ICCVersion", TradePortalConstants.TEXT_BUNDLE, "en_US"));
		  downloadData.append(" : ");
		  downloadData.append(ReferenceDataManager.getRefDataMgr().getDescr("ICC_GUIDELINES", terms.getAttribute("ucp_version")));
		  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
		  
		  if (StringFunction.isNotBlank(terms.getAttribute("ucp_details"))){
			  downloadData = new StringBuilder();
			  downloadData.append(resourceManager.getText("common.ICCDetails", TradePortalConstants.TEXT_BUNDLE, "en_US"));
			  downloadData.append(" : ");
			  downloadData.append(terms.getAttribute("ucp_details"));
			  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
			  out.println("");
		  }
	  }
	//Sandeep - Rel-8.3 CR-752 Dev 06/11/2013 - End  
	  
	  downloadData = new StringBuilder();
	  downloadData.append(resourceManager.getText("ImportDLCIssue.AdditionalConditions",
	                                               TradePortalConstants.TEXT_BUNDLE, "en_US"));
	  downloadData.append(" : ");
	  downloadData.append(terms.getAttribute("additional_conditions"));
	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
	  out.println("");
}


/**
 * Download the ShipmentTerms
 *
 * @param out
 * @param terms
 * @param resourceManager
 * @param termsParty
 * @param resourceLocale
 * @param shipmentTerms
 * @throws IOException
 * @throws AmsException
 */
private void downloadImportLCDataAllShipmentTerms(
	ServletOutputStream out,
	TermsWebBean terms,
	com.amsinc.ecsg.util.ResourceManager resourceManager,
	TermsPartyWebBean termsParty,
	String resourceLocale,
	ShipmentTermsWebBean shipmentTerms)
	throws IOException, AmsException {
	
	 DocumentHandler resultSet = DatabaseQueryBean.getXmlResultSet("select shipment_oid from shipment_terms where p_terms_oid = ? order by creation_date", false, new Object[]{terms.getAttribute("terms_oid")});
	
	if (resultSet != null) {
		Vector shipmentTermsOids = resultSet.getFragments("/ResultSetRecord");
		int numberOfShipmentTerms = shipmentTermsOids.size();
		for (int i=0; i<numberOfShipmentTerms; i++) {
			String shipmentOid = ((DocumentHandler)shipmentTermsOids.elementAt(i)).getAttribute("/SHIPMENT_OID");
			shipmentTerms.setAttribute("shipment_oid", shipmentOid);			
			shipmentTerms.getDataFromAppServer();

			downloadImportLCDataShipmentTerms(
					out,
					terms,
					resourceManager,
					termsParty,
					resourceLocale,
					shipmentTerms,
					i+1,
					numberOfShipmentTerms);

		}

	}
}


/**
 * Download one ShipmentTerms
 *
 * @param out
 * @param terms
 * @param resourceManager
 * @param termsParty
 * @param resourceLocale
 * @param shipmentTerms
 * @param indexOfShipmentTerms
 * @param numberOfShipmentTerms
 * @throws IOException
 * @throws AmsException
 */
private void downloadImportLCDataShipmentTerms(
	ServletOutputStream out,
	TermsWebBean terms,
	com.amsinc.ecsg.util.ResourceManager resourceManager,
	TermsPartyWebBean termsParty,
	String resourceLocale,
	ShipmentTermsWebBean shipmentTerms,
	int indexOfShipmentTerms,
	int numberOfShipmentTerms)
	throws IOException, AmsException {
	StringBuilder downloadData;
	boolean isValidTermsParty;
	String consignmentType;
	String markedFreight;
	String termsPartyOid;

	/* e.g. Transport Document(s) and Shipment 1 of 2*/
	if (numberOfShipmentTerms > 1) {
	   out.println("");
	   downloadData = new StringBuilder();
	   downloadData.append(resourceManager.getText("ImportDLCIssue.TransportDocs", TradePortalConstants.TEXT_BUNDLE, "en_US"))
	               .append("/")
	               .append(resourceManager.getText("ImportDLCIssue.Shipment", TradePortalConstants.TEXT_BUNDLE, "en_US"))
		           .append(" ")
	               .append(indexOfShipmentTerms)
	               .append("")
				   .append(resourceManager.getText("ImportDLCIssue.Of", TradePortalConstants.TEXT_BUNDLE, "en_US"))
				   .append("")
		           .append(numberOfShipmentTerms)
				   .append(" - ");
		out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

		downloadData = new StringBuilder();
		downloadData.append(resourceManager.getText("ImportDLCIssue.Description", TradePortalConstants.TEXT_BUNDLE, "en_US"))
		            .append(" : ")
		            .append(shipmentTerms.getAttribute("description"));
		out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

		out.println("");
	}

	  //*****************************TRANSPORT DOCUMENTS SUBSECTION**************************************/
	  // Retrieve all saved transport document data for the Issue Import LC transaction
	  if (TradePortalConstants.INDICATOR_YES.equals(shipmentTerms.getAttribute("trans_doc_included")))
	  {
		if (numberOfShipmentTerms <= 1) {
	       downloadData = new StringBuilder();
	       downloadData.append(resourceManager.getText("ImportDLCIssue.TransportDocs", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	       downloadData.append(" - ");
		   out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
		}

	     downloadData = new StringBuilder();
	     downloadData.append(resourceManager.getText("ImportDLCIssue.DocType", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	     downloadData.append(" : ");
	     downloadData.append(ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.TRANS_DOC_TYPE ,
	                                                                          shipmentTerms.getAttribute("trans_doc_type"),
	                                                                          resourceLocale));
	     out.println(StringFunction.xssHtmlToChars(downloadData.toString()));



	     downloadData = new StringBuilder();
	     downloadData.append(resourceManager.getText("ImportDLCIssue.Originals", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	     downloadData.append(" : ");
	     downloadData.append(shipmentTerms.getAttribute("trans_doc_originals"));
	     out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

	     downloadData = new StringBuilder();
	     downloadData.append(resourceManager.getText("ImportDLCIssue.Copies", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	     downloadData.append(" : ");
	     downloadData.append(shipmentTerms.getAttribute("trans_doc_copies"));
	     out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

	     downloadData = new StringBuilder();
	     downloadData.append(resourceManager.getText("ImportDLCIssue.DocumentText", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	     downloadData.append(" : ");
	     downloadData.append(shipmentTerms.getAttribute("trans_doc_text"));
	     out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
	     out.println("");
	  }

	  // Retrieve the Issue Import LC transaction's Consignment information
	  downloadData = new StringBuilder();
	  downloadData.append(resourceManager.getText("ImportDLCIssue.Consignment", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	  downloadData.append(" : ");

	  consignmentType = shipmentTerms.getAttribute("trans_doc_consign_type");
	  if (StringFunction.isBlank(consignmentType)){
		  consignmentType = "";
	  }

	  if (consignmentType.equals(TradePortalConstants.CONSIGN_ORDER))
	  {
	     downloadData.append(resourceManager.getText("ImportDLCIssue.ConsignedToOrderOf",
	                                                 TradePortalConstants.TEXT_BUNDLE, "en_US"));
	     downloadData.append(" ");

	     if (!StringFunction.isBlank(shipmentTerms.getAttribute("trans_doc_consign_order_of_pty")))
	     {
	        downloadData.append(ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.CONSIGNMENT_PARTY_TYPE,
			shipmentTerms.getAttribute("trans_doc_consign_order_of_pty"),
	                                                                resourceLocale));
	     }
	  }
	  else if (consignmentType.equals(TradePortalConstants.CONSIGN_PARTY))
	  {
	     downloadData.append(resourceManager.getText("ImportDLCIssue.ConsignedTo", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	     downloadData.append(" ");

	     if (!StringFunction.isBlank(shipmentTerms.getAttribute("trans_doc_consign_to_pty")))
	     {
	        downloadData.append(ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.CONSIGNMENT_PARTY_TYPE,
			shipmentTerms.getAttribute("trans_doc_consign_to_pty"),
	                                                                resourceLocale));
	     }
	  }

	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
	  out.println("");

	  // Retrieve the Issue Import LC transaction's Marked Freight information
	  downloadData = new StringBuilder();
	  downloadData.append(resourceManager.getText("ImportDLCIssue.MarkedFreight", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	  downloadData.append(" : ");

	  markedFreight = shipmentTerms.getAttribute("trans_doc_marked_freight");
	  if (StringFunction.isBlank(markedFreight)){
		  markedFreight = "";
	  }

	  if (markedFreight.equals(TradePortalConstants.PREPAID))
	  {
	     downloadData.append(resourceManager.getText("ImportDLCIssue.Prepaid", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	  }
	  else if (markedFreight.equals(TradePortalConstants.COLLECT))
	  {
	     downloadData.append(resourceManager.getText("ImportDLCIssue.Collect", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	  }

	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
	  out.println("");

	  // Retrieve the Notify terms party from the database
	  termsPartyOid = shipmentTerms.getAttribute("c_NotifyParty");

	  if (!StringFunction.isBlank(termsPartyOid))
	  {
	     termsParty.setAttribute("terms_party_oid", termsPartyOid);
	     termsParty.getDataFromAppServer();

	     isValidTermsParty = true;
	  }
	  else
	  {
	     isValidTermsParty = false;
	  }

	  // Start retrieving all saved Notify Party data of the Import LC being viewed
	  downloadData = new StringBuilder();
	  downloadData.append(resourceManager.getText("ImportDLCIssue.NotifyParty", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	  downloadData.append(" - ");
	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
	  out.println("");

	  downloadData = new StringBuilder();
	  downloadData.append(resourceManager.getText("ImportDLCIssue.Name", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	  downloadData.append(" : ");
	  if (isValidTermsParty)
	  {
	     downloadData.append(termsParty.getAttribute("name"));
	  }
	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

	  downloadData = new StringBuilder();
	  downloadData.append(resourceManager.getText("ImportDLCIssue.PhoneNumber", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	  downloadData.append(" : ");
	  if (isValidTermsParty)
	  {
	     downloadData.append(termsParty.getAttribute("phone_number"));
	  }
	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

	  downloadData = new StringBuilder();
	  downloadData.append(resourceManager.getText("ImportDLCIssue.AddressLine1", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	  downloadData.append(" : ");
	  if (isValidTermsParty)
	  {
	     downloadData.append(termsParty.getAttribute("address_line_1"));
	  }
	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

	  downloadData = new StringBuilder();
	  downloadData.append(resourceManager.getText("ImportDLCIssue.AddressLine2", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	  downloadData.append(" : ");
	  if (isValidTermsParty)
	  {
	     downloadData.append(termsParty.getAttribute("address_line_2"));
	  }
	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

	  downloadData = new StringBuilder();
	  downloadData.append(resourceManager.getText("ImportDLCIssue.AddressLine3", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	  downloadData.append(" : ");
	  if (isValidTermsParty)
	  {
	     downloadData.append(termsParty.getAttribute("address_line_3"));
	  }
	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
	  out.println("");

	  // Retrieve the Other Consignee terms party from the database
	  termsPartyOid = shipmentTerms.getAttribute("c_OtherConsigneeParty");

	  if (!StringFunction.isBlank(termsPartyOid))
	  {
	     termsParty.setAttribute("terms_party_oid", termsPartyOid);
	     termsParty.getDataFromAppServer();

	     isValidTermsParty = true;
	  }
	  else
	  {
	     isValidTermsParty = false;
	  }

	  // Start retrieving all saved Other Consignee data of the Import LC being viewed
	  downloadData = new StringBuilder();
	  downloadData.append(resourceManager.getText("ImportDLCIssue.OtherConsignee", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	  downloadData.append(" - ");
	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
	  out.println("");

	  downloadData = new StringBuilder();
	  downloadData.append(resourceManager.getText("ImportDLCIssue.Name", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	  downloadData.append(" : ");
	  if (isValidTermsParty)
	  {
	     downloadData.append(termsParty.getAttribute("name"));
	  }
	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

	  downloadData = new StringBuilder();
	  downloadData.append(resourceManager.getText("ImportDLCIssue.PhoneNumber", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	  downloadData.append(" : ");
	  if (isValidTermsParty)
	  {
	     downloadData.append(termsParty.getAttribute("phone_number"));
	  }
	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

	  downloadData = new StringBuilder();
	  downloadData.append(resourceManager.getText("ImportDLCIssue.AddressLine1", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	  downloadData.append(" : ");
	  if (isValidTermsParty)
	  {
	     downloadData.append(termsParty.getAttribute("address_line_1"));
	  }
	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

	  downloadData = new StringBuilder();
	  downloadData.append(resourceManager.getText("ImportDLCIssue.AddressLine2", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	  downloadData.append(" : ");
	  if (isValidTermsParty)
	  {
	     downloadData.append(termsParty.getAttribute("address_line_2"));
	  }
	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

	  downloadData = new StringBuilder();
	  downloadData.append(resourceManager.getText("ImportDLCIssue.AddressLine3", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	  downloadData.append(" : ");
	  if (isValidTermsParty)
	  {
	     downloadData.append(termsParty.getAttribute("address_line_3"));
	  }
	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
	  out.println("");

	  if (TradePortalConstants.INDICATOR_YES.equals(shipmentTerms.getAttribute("trans_addl_doc_included")))
	  {
	     downloadData = new StringBuilder();
	     downloadData.append(resourceManager.getText("ImportDLCIssue.AddlTransportDocs",
	                                                 TradePortalConstants.TEXT_BUNDLE, "en_US"));
	     downloadData.append(" - ");
	     out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
	     out.println("");

	     downloadData = new StringBuilder();
	     downloadData.append(resourceManager.getText("ImportDLCIssue.DocumentText", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	     downloadData.append(" : ");
	     downloadData.append(shipmentTerms.getAttribute("trans_addl_doc_text"));
	     out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
	     out.println("");
	  }

	  //*****************************SHIPMENT SUBSECTION**************************************/
	  // Retrieve all saved shipment data for the Issue Import LC transaction
	  if (numberOfShipmentTerms <= 1) {
	     downloadData = new StringBuilder();
	     downloadData.append(resourceManager.getText("ImportDLCIssue.DownloadShipment", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	     downloadData.append(" - ");
	     out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
	     out.println("");
	  }

	  downloadData = new StringBuilder();
	  downloadData.append(resourceManager.getText("ImportDLCIssue.TranshipAllowed", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	  downloadData.append(" : ");

	  if (TradePortalConstants.INDICATOR_YES.equals(shipmentTerms.getAttribute("transshipment_allowed")))
	  {
	     downloadData.append(resourceManager.getText("common.Yes", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	  }
	  else
	  {
	     downloadData.append(resourceManager.getText("common.No", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	  }
	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

	  downloadData = new StringBuilder();
	  downloadData.append(resourceManager.getText("ImportDLCIssue.PartialAllowed", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	  downloadData.append(" : ");

	  if (TradePortalConstants.INDICATOR_YES.equals(terms.getAttribute("partial_shipment_allowed")))
	  {
	     downloadData.append(resourceManager.getText("common.Yes", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	  }
	  else
	  {
	     downloadData.append(resourceManager.getText("common.No", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	  }
	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

	  downloadData = new StringBuilder();
	  downloadData.append(resourceManager.getText("ImportDLCIssue.LastestShipDate", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	  downloadData.append(" : ");
	  downloadData.append(TPDateTimeUtility.formatDate(shipmentTerms.getAttribute("latest_shipment_date"),
	                                                   TPDateTimeUtility.LONG, resourceLocale));
	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

	  downloadData = new StringBuilder();
	  downloadData.append(resourceManager.getText("ImportDLCIssue.ShippingTerm", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	  downloadData.append(" : ");

	  if (!StringFunction.isBlank(shipmentTerms.getAttribute("incoterm")))
	  {
	     downloadData.append(ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.INCOTERM,
		shipmentTerms.getAttribute("incoterm"),
	                                                                       resourceLocale));
	  }
	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

	  downloadData = new StringBuilder();
	  downloadData.append(resourceManager.getText("ImportDLCIssue.ShipTermLocation", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	  downloadData.append(" : ");
	  downloadData.append(shipmentTerms.getAttribute("incoterm_location"));
	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

	  downloadData = new StringBuilder();
	  downloadData.append(resourceManager.getText("ImportDLCIssue.DownloadShipmentFrom",
	                                              TradePortalConstants.TEXT_BUNDLE, "en_US"));
	  downloadData.append(" : ");
	  downloadData.append(shipmentTerms.getAttribute("shipment_from"));
	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

	  downloadData = new StringBuilder();
	  downloadData.append(resourceManager.getText("ImportDLCIssue.DownloadShipmentFrom",
	                                              TradePortalConstants.TEXT_BUNDLE, "en_US"));
	  downloadData.append(" : ");
	  downloadData.append(shipmentTerms.getAttribute("shipment_from_loading"));
	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

	  downloadData = new StringBuilder();
	  downloadData.append(resourceManager.getText("ImportDLCIssue.DownloadShipmentTo",
	                                              TradePortalConstants.TEXT_BUNDLE, "en_US"));
	  downloadData.append(" : ");
	  downloadData.append(shipmentTerms.getAttribute("shipment_to_discharge"));
	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

	  downloadData = new StringBuilder();
	  downloadData.append(resourceManager.getText("ImportDLCIssue.DownloadShipmentTo",
	                                              TradePortalConstants.TEXT_BUNDLE, "en_US"));
	  downloadData.append(" : ");
	  downloadData.append(shipmentTerms.getAttribute("shipment_to"));
	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

	  downloadData = new StringBuilder();
	  downloadData.append(resourceManager.getText("ImportDLCIssue.TimePeriod", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	  downloadData.append(" : ");
	  downloadData.append(resourceManager.getText("ImportDLCIssue.PresentDocs", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	  downloadData.append(" - ");
	  downloadData.append(terms.getAttribute("present_docs_within_days"));
	  downloadData.append(" - ");
	  downloadData.append(resourceManager.getText("ImportDLCIssue.DayOfShipment", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

	  downloadData = new StringBuilder();
	  downloadData.append(resourceManager.getText("ImportDLCIssue.GoodsDesc", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	  downloadData.append(" : ");
	  downloadData.append(shipmentTerms.getAttribute("goods_description"));
	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
	  out.println("");

	  if(!"".equals(shipmentTerms.getAttribute("po_line_items")))
	   {
	     downloadData = new StringBuilder();
	     downloadData.append(resourceManager.getText("ImportDLCIssue.POLineItems", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	     downloadData.append(" : ");
	     out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
	     StringTokenizer newLineTokenizer = new StringTokenizer( StringFunction.xssHtmlToChars(shipmentTerms.getAttribute("po_line_items")), "\n");

	     while(newLineTokenizer.hasMoreTokens())
	      {
	         out.println(newLineTokenizer.nextToken());
	      }

	     out.println("");
	   }

	  downloadStructuredPOs(out,  shipmentTerms, resourceManager);    // IR# SUUM060556386  Rel. 8.0.0
}


// IR# SUUM060556386  Rel. 8.0.0 - Begin -

private void downloadStructuredPOs(ServletOutputStream out, ShipmentTermsWebBean shipmentTerms, com.amsinc.ecsg.util.ResourceManager rMgr) throws AmsException, IOException {

	final int PO_NUM_FIELDLEN = 35;
	final int CUR_FIELDLEN = 3;
	final int AMOUNT_FIELDLEN = 22;
	final int CREATION_DATE_FIELDLEN = 12;
	final int ISSUE_DATE_FIELDLEN = 11;
	final int LATEST_SHIP_DATE_FIELDLEN = 14;


	String sql = "select purchase_order_num,  currency, amount, creation_date_time,issue_date,latest_shipment_date  from PURCHASE_ORDER " +
			" where a_shipment_oid = ? order by upper(purchase_order_num) ";
	

	DocumentHandler pDoc = DatabaseQueryBean.getXmlResultSet(sql, false, new Object[]{shipmentTerms.getAttribute("shipment_oid")});

	if(pDoc == null) return;
	StringBuilder downloadData = new StringBuilder();
	downloadData.append(rMgr.getText("POLineItemDetail.PurchaseOrders", TradePortalConstants.TEXT_BUNDLE, "en_US")).append(" : ");
	out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

	downloadData = new StringBuilder();
	
	downloadData.append(padRight(rMgr.getText("POStructuredList.purchase_order_num", TradePortalConstants.TEXT_BUNDLE, "en_US"),PO_NUM_FIELDLEN));
	downloadData.append(padRight(rMgr.getText("POStructuredList.currency", TradePortalConstants.TEXT_BUNDLE, "en_US"), CUR_FIELDLEN));
	downloadData.append(padRight(StringFunction.padLeft(rMgr.getText("POStructuredList.amount", TradePortalConstants.TEXT_BUNDLE, "en_US"), AMOUNT_FIELDLEN),AMOUNT_FIELDLEN));
	downloadData.append(padRight(rMgr.getText("POStructuredList.creation_date_time", TradePortalConstants.TEXT_BUNDLE, "en_US"),CREATION_DATE_FIELDLEN));
	downloadData.append(padRight(rMgr.getText("POStructuredList.issue_date", TradePortalConstants.TEXT_BUNDLE, "en_US"),ISSUE_DATE_FIELDLEN));
	downloadData.append(rMgr.getText("POStructuredList.latest_shipment_date", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

	downloadData = new StringBuilder();
	downloadData.append(padRight(StringFunction.repeat("-",PO_NUM_FIELDLEN),PO_NUM_FIELDLEN));
	downloadData.append(padRight(StringFunction.repeat("-",CUR_FIELDLEN),CUR_FIELDLEN));
	downloadData.append(padRight(StringFunction.repeat("-",AMOUNT_FIELDLEN),AMOUNT_FIELDLEN));
	downloadData.append(padRight(StringFunction.repeat("-",CREATION_DATE_FIELDLEN),CREATION_DATE_FIELDLEN));
	downloadData.append(padRight(StringFunction.repeat("-",ISSUE_DATE_FIELDLEN),ISSUE_DATE_FIELDLEN));
	downloadData.append(StringFunction.repeat("-",LATEST_SHIP_DATE_FIELDLEN));
	out.println(downloadData.toString());

	List<DocumentHandler> poList = pDoc.getFragmentsList("/ResultSetRecord");

	for (DocumentHandler poDoc : poList) {
		downloadData = new StringBuilder();
		downloadData.append(padRight(poDoc.getAttribute("/PURCHASE_ORDER_NUM"), PO_NUM_FIELDLEN));
		downloadData.append(padRight(poDoc.getAttribute("/CURRENCY"), CUR_FIELDLEN));
		
		String val = TPCurrencyUtility.getDisplayAmount(poDoc.getAttribute("/AMOUNT"), poDoc.getAttribute("/CURRENCY"), rMgr.getResourceLocale());
		//T36000037806-Start
 		downloadData.append(padRight(val, AMOUNT_FIELDLEN));
 		//T36000037806-End
		
		val = TPDateTimeUtility.formatDate(poDoc.getAttribute("/CREATION_DATE_TIME"), TPDateTimeUtility.SHORT, rMgr.getResourceLocale());
		downloadData.append(padRight(val,CREATION_DATE_FIELDLEN));

		val = TPDateTimeUtility.formatDate(poDoc.getAttribute("/ISSUE_DATE"), TPDateTimeUtility.SHORT, rMgr.getResourceLocale());
		downloadData.append(padRight(val, ISSUE_DATE_FIELDLEN));

		val = poDoc.getAttribute("/LATEST_SHIPMENT_DATE");
		if (StringFunction.isNotBlank(val)) {
			val = TPDateTimeUtility.formatDate(poDoc.getAttribute("/LATEST_SHIPMENT_DATE"), TPDateTimeUtility.SHORT, rMgr.getResourceLocale());
			downloadData.append(val);
		}		

		out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
	}
	out.println("");
}


private String padRight(String val, int fieldLen) {
	if (val == null) val="";
	fieldLen+=2;
	if (fieldLen > val.length()) {
		val = StringFunction.padRight(val, fieldLen);
	}

	return val;
}


// IR# SUUM060556386  Rel. 8.0.0 - End -

   /**
    * This method is used to download Advice Terms data for the Export Letter of Credit
    * instrument currently being viewed in the Instrument Summary page.
    *

    * @param      request - the Http servlet request object
    * @exception  javax.servlet.ServletException
    * @exception  java.io.IOException
    * @return     void
    */
   private void downloadAdviceTermsData(HttpServletRequest request, HttpServletResponse response)
                                        throws ServletException, IOException, AmsException
   {
      CorporateOrganizationWebBean   corporateOrg          = null;
      ClientServerDataBridge         csdb                  = null;
      ServletOutputStream            out                   = null;
      TransactionWebBean             originalTransaction   = null;
      InstrumentWebBean              instrument            = null;
      DocumentHandler                lastAmendmentDatesDoc = null;
      DocumentHandler                lastPaymentDatesDoc   = null;
      DocumentHandler                xmlDoc                = null;
      com.amsinc.ecsg.util.ResourceManager                resourceManager       = null;
      SessionWebBean                 userSession           = null;
      TermsWebBean                   terms                 = null;
      SwiftMessage                   swiftMessage          = null;
      StringBuilder                   defaultFileName       = null;
      StringBuilder                   addressLine3          = null;
      StringBuilder                   sqlQuery              = null;
      BeanManager                    beanManager           = null;
      HttpSession                    session               = null;
      Hashtable                      swiftMessageData      = null;
     
      String			     dnldAdvTermsFormat	   = null;
      String                         swiftMessageSource    = null;
      String                         swiftMessageValue     = null;
      String                         lastAmendmentDate     = null;
      String                         lastPaymentDate       = null;
      String                         availableAmount       = null;
      String                         swiftMessageId        = null;
      String                         instrumentOid         = null;
      String                         userTimeZone          = null;
      String                         docPrepValue          = null;
      
      // First we need to retrieve the resource manager from the session so that we can retrieve
      // internationalized field name strings
      session         = request.getSession(false);
      resourceManager = (com.amsinc.ecsg.util.ResourceManager) session.getAttribute("resMgr");
      beanManager     = (BeanManager)     session.getAttribute("beanMgr");
      userSession     = (SessionWebBean)  session.getAttribute("userSession");

      // Retrieve the http servlet output stream
      out = response.getOutputStream();


      // Retrieve the current instrument summary oid that's stored in the user's session
      instrumentOid = (String) session.getAttribute(TradePortalConstants.INSTRUMENT_SUMMARY_OID);

      // Next, create an Instrument Web Bean and populate it using the current instrument summary oid
      // that's stored in the user's session
      instrument = beanManager.createBean(InstrumentWebBean.class, "Instrument");
      instrument.getById(instrumentOid);

      // Get the user's time zone so that we can format all necessary date and time fields accordingly;
      // also, retrieve its organization oid
      userTimeZone = userSession.getTimeZone();

      // Get the user org's OTL Customer ID
      corporateOrg = beanManager.createBean(CorporateOrganizationWebBean.class, "CorporateOrganization");

      corporateOrg.getById(instrument.getAttribute("corp_org_oid"));

      // Construct the address line 3 string
      addressLine3 = new StringBuilder();
      addressLine3.append(corporateOrg.getAttribute("address_city"));
      addressLine3.append(", ");
      addressLine3.append(corporateOrg.getAttribute("address_state_province"));
      addressLine3.append(" ");
      addressLine3.append(corporateOrg.getAttribute("address_postal_code"));

      // Get the available amount for the Current Balance tag
      originalTransaction = beanManager.createBean(TransactionWebBean.class, "Transaction");

      originalTransaction.getById(instrument.getAttribute("original_transaction_oid"));

      availableAmount = originalTransaction.getAttribute("available_amount");

      // Get the bank released terms associated with the original transaction
      csdb = resourceManager.getCSDB();

      originalTransaction.setClientServerDataBridge(csdb);

      terms = originalTransaction.registerBankReleasedTerms();

      //If the download foramt is SWIFT, then just write out the SWIFT content with a little
      //formatting.

      dnldAdvTermsFormat = corporateOrg.getAttribute("dnld_adv_terms_format");
      if (StringFunction.isBlank(dnldAdvTermsFormat)){
    	  dnldAdvTermsFormat = "";
      }
      if (dnldAdvTermsFormat.equals(TradePortalConstants.DNLD_SWIFT)) {

         defaultFileName = new StringBuilder();
         defaultFileName.append(instrument.getAttribute("opening_bank_ref_num"));
         defaultFileName.append(resourceManager.getText("InstrumentSummary.DownloadTXTSuffix",
                                                     TradePortalConstants.TEXT_BUNDLE, "en_US"));
         setResponseContent(response, TradePortalConstants.DOWNLOAD_TXT_CONTENT_TYPE,
                         defaultFileName.toString().replace('/', '_'));

         
         // IValera - IR# DFUJ052743752 - 29 May, 2009 - Change Begin
         // Gets the value of TRANSACTION_AS_TEXT column for current TRANSACTION
         String rawSwift = originalTransaction.getAttribute("transaction_as_text");
         // Below line does two things
         // - It replaces any NEW LINE characters with BLANK string
         // - Decripts the HTML codes to ASCII codes
         rawSwift = StringFunction.xssHtmlToChars(rawSwift.replaceAll("[\\r\\n]", ""));

         int tokenCount = 1;
         int bufferLen = rawSwift.length();
         char[] charBuffer = new char[bufferLen];
         // Copies TRANSACTION_AS_TEXT value to CHAR array
         rawSwift.getChars(0, bufferLen, charBuffer, 0);

         /*
          * Starts going through each character in CHAR array
          *
          * Below loop does the magic to format singale line SWIFT into proper format
          * In SWIFT formating below two points are important:
          * - 1st line should starts with SWIFT...
          * - 2nd line onwards all the new lines should start with :00A: format;
          *   this means that new line will always have : at 1st & again at the 5th position
          *
          * Now what below logic does is, it checks for 1st occurence of : in CHAR array
          * & checks the value of 5th character from that position. If the value is :
          * it changes value of variable to 2(two); which will insert NEW LINE in output stream
          *
          */
         for (int i = 0; i < bufferLen; i++) {
        	 // Checks whether the curent character is ':' or not
        	 if (charBuffer[i] == ':') {
        		 // When current character is : & it is found 1st time;
        		 // Updates variable to 1
        		 if (tokenCount == 0) {
        			 tokenCount++;
        		 }
        		 else if (tokenCount == 1) {
            		 // If token is already found, checks for 5th position character value
        			 // If 5th position value is :, sets variable to 2
        			 if (charBuffer[i + 4] == ':') {
        				 tokenCount++;
        			 }
        		 }
        	 }

        	 // If value is 2 (valid token found), prints NEW LINE to output stream
        	 if (tokenCount == 2) {
        		 out.println();
        		 tokenCount = 0;
        	 }

        	 // Prints current character to output stream
        	 out.print(charBuffer[i]);

	     }
         // IValera - IR# DFUJ052743752 - 29 May, 2009 - Change End

        return;
        }


      // Get the SWIFT Message that created the Advise transaction
      swiftMessage = new SwiftMessage(originalTransaction.getAttribute("transaction_as_text"));

      // Get the relevant data from the Swift message, its ID, and its source
      swiftMessageData   = swiftMessage.getSwiftMessageData();
      swiftMessageId     = swiftMessage.getSwiftMessageId();
      swiftMessageSource = swiftMessage.getSwiftMessageSource();

      // Get the last payment date for the Export Letter of Credit
      sqlQuery = new StringBuilder();

      sqlQuery.append("select transaction_status_date");
      sqlQuery.append(" from transaction");
      sqlQuery.append(" where p_instrument_oid =?  ");
      sqlQuery.append(" and transaction_type_code = ? order by transaction_status_date desc");

      lastPaymentDatesDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery.toString(), false, instrumentOid, TransactionType.PAYMENT);
      if (lastPaymentDatesDoc != null)
      {
         // The last payment date should be the first date in the results doc
         lastPaymentDate = lastPaymentDatesDoc.getAttribute("/ResultSetRecord(0)/TRANSACTION_STATUS_DATE");

         // Next, convert the date so that it's in 'MM/dd/yyyy HH:mm:ss' format
         lastPaymentDate = DateTimeUtility.convertTimestampToDateTimeString(lastPaymentDate);

         // Finally, convert the date so that it's in 'YYMMDD' format
         lastPaymentDate = TPDateTimeUtility.getDocPrepFormattedDate(lastPaymentDate, TPDateTimeUtility.SHORT,
                                                                     userTimeZone);
      }
      else
      {
         lastPaymentDate = "";
      }

      // Get the last amendment date for the Export Letter of Credit
      sqlQuery = new StringBuilder();

      sqlQuery.append("select transaction_status_date");
      sqlQuery.append(" from transaction");
      sqlQuery.append(" where p_instrument_oid = ? and transaction_type_code = ? order by transaction_status_date desc");

      lastAmendmentDatesDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery.toString(), false, instrumentOid, TransactionType.AMEND );
      if (lastAmendmentDatesDoc != null)
      {
         // The last amendment date should be the first date in the results doc
         lastAmendmentDate = lastAmendmentDatesDoc.getAttribute("/ResultSetRecord(0)/TRANSACTION_STATUS_DATE");

         // Next, convert the date so that it's in 'MM/dd/yyyy HH:mm:ss' format
         lastAmendmentDate = DateTimeUtility.convertTimestampToDateTimeString(lastAmendmentDate);

         // Finally, convert the date so that it's in 'YYMMDD' format
         lastAmendmentDate = TPDateTimeUtility.getDocPrepFormattedDate(lastAmendmentDate, TPDateTimeUtility.SHORT,
                                                                       userTimeZone);
      }
      else
      {
         lastAmendmentDate = "";
      }

      // Set the default file name to use for the Advice Terms data file to be downloaded
      defaultFileName = new StringBuilder();
      if(swiftMessageData!=null){
    	  defaultFileName.append((String) swiftMessageData.get("CreditNumber"));  
      }
      
      defaultFileName.append(resourceManager.getText("InstrumentSummary.DownloadXMLSuffix",
                                                     TradePortalConstants.TEXT_BUNDLE, "en_US"));
      // Set the http servlet response content type and content disposition (i.e., default filename)
      setResponseContent(response, TradePortalConstants.DOWNLOAD_XML_CONTENT_TYPE,
                         defaultFileName.toString().replace('/', '_'));

      // Retrieve the instrument number of the Import LC instrument currently being viewed
      xmlDoc = new DocumentHandler("<Proponix></Proponix>", false);
      xmlDoc.setAttribute("/Header/MessageType", TradePortalConstants.DOC_PREP);
      xmlDoc.setAttribute("../DateSent", TPDateTimeUtility.getDocPrepFormattedDate(TPDateTimeUtility.LONG, userTimeZone));
      xmlDoc.setAttribute("../TimeSent", TPDateTimeUtility.getDocPrepFormattedTime(userTimeZone));
      xmlDoc.setAttribute("../InstrumentID", instrument.getAttribute("complete_instrument_id"));

      xmlDoc.setAttribute("/Body/OTL/SystemCustomerID", corporateOrg.getAttribute("Proponix_customer_id"));
      xmlDoc.setAttribute("../Name",                    corporateOrg.getAttribute("name"));
      xmlDoc.setAttribute("../AddressLine1",            corporateOrg.getAttribute("address_line_1"));
      xmlDoc.setAttribute("../AddressLine2",            corporateOrg.getAttribute("address_line_2"));
      xmlDoc.setAttribute("../AddressLine3",            addressLine3.toString());
      xmlDoc.setAttribute("../CreditReference",         instrument.getAttribute("complete_instrument_id"));
      xmlDoc.setAttribute("../../Source",               swiftMessageSource);
      xmlDoc.setAttribute("../Type",                    TransactionType.ADVISE);
      xmlDoc.setAttribute("../Status",                  TransactionStatus.PROCESSED_BY_BANK);
      if(swiftMessageData!=null)
      xmlDoc.setAttribute("../CreditNumber",            (String) swiftMessageData.get("CreditNumber"));

      if ((swiftMessageId.equals(TradePortalConstants.SWIFT_MESSAGE_700)) ||
          (swiftMessageId.equals(TradePortalConstants.SWIFT_MESSAGE_710)) ||
          (swiftMessageId.equals(TradePortalConstants.SWIFT_MESSAGE_720)))
      {
         xmlDoc.setAttribute("../CreditForm/CreditFormLine1", (String) swiftMessageData.get("CreditFormLine1"));
         xmlDoc.setAttribute("../CreditFormLine2",            (String) swiftMessageData.get("CreditFormLine2"));

         swiftMessageValue = (String) swiftMessageData.get("IssueDate");
         if (swiftMessageValue != null)
         {
            xmlDoc.setAttribute("../../IssueDate", swiftMessageValue);
         }

         xmlDoc.setAttribute("/Body/ExpiryDate", (String) swiftMessageData.get("ExpiryDate"));
         xmlDoc.setAttribute("../ExpiryPlace",   (String) swiftMessageData.get("ExpiryPlace"));



		 swiftMessageValue = (String) swiftMessageData.get("ApplicableRules");
		 if (swiftMessageValue != null)
		 {
			xmlDoc.setAttribute("/Body/ApplicableRules", swiftMessageValue);
	  	 }


         swiftMessageValue = (String) swiftMessageData.get("IssuingBankName");
         if (swiftMessageValue != null)
         {
			if ((swiftMessageId.equals(TradePortalConstants.SWIFT_MESSAGE_710)))
			{
				xmlDoc.setAttribute("../IssuingBank/Name", swiftMessageValue);
            	xmlDoc.setAttribute("../AddressLine1",     (String) swiftMessageData.get("IssuingBankAddressLine1"));
            	xmlDoc.setAttribute("../AddressLine2",     (String) swiftMessageData.get("IssuingBankAddressLine2"));

            	swiftMessageValue = (String) swiftMessageData.get("IssuingBankAddressLine3");
            	if (swiftMessageValue != null)
            	{
               		xmlDoc.setAttribute("../AddressLine3", swiftMessageValue);
            	}
			}
			else if((swiftMessageId.equals(TradePortalConstants.SWIFT_MESSAGE_720)))
			{
				xmlDoc.setAttribute("../IssuingBankOfOriginalDocumentaryCredit/Name", swiftMessageValue);
				xmlDoc.setAttribute("../AddressLine1",     (String) swiftMessageData.get("IssuingBankAddressLine1"));
				xmlDoc.setAttribute("../AddressLine2",     (String) swiftMessageData.get("IssuingBankAddressLine2"));

				swiftMessageValue = (String) swiftMessageData.get("IssuingBankAddressLine3");
				if (swiftMessageValue != null)
				{
					xmlDoc.setAttribute("../AddressLine3", swiftMessageValue);
				}
			}
         }
         else
         {
			swiftMessageValue = (String) swiftMessageData.get("NonBankIssuerName");
			if (swiftMessageValue != null)
			{
				if ((swiftMessageId.equals(TradePortalConstants.SWIFT_MESSAGE_710)))
				{
					xmlDoc.setAttribute("../NonBankIssuer/Name", swiftMessageValue);
					xmlDoc.setAttribute("../AddressLine1",     (String) swiftMessageData.get("NonBankIssuerAddressLine1"));
					xmlDoc.setAttribute("../AddressLine2",     (String) swiftMessageData.get("NonBankIssuerAddressLine2"));

					swiftMessageValue = (String) swiftMessageData.get("NonBankIssuerAddressLine3");
					if (swiftMessageValue != null)
					{
				 	  	xmlDoc.setAttribute("../AddressLine3", swiftMessageValue);
					}
				}
				else if((swiftMessageId.equals(TradePortalConstants.SWIFT_MESSAGE_720)))
				{
					xmlDoc.setAttribute("../NonBankIssuerOfTheOriginalDocumentaryCredit/Name", swiftMessageValue);
					xmlDoc.setAttribute("../AddressLine1",     (String) swiftMessageData.get("NonBankIssuerAddressLine1"));
					xmlDoc.setAttribute("../AddressLine2",     (String) swiftMessageData.get("NonBankIssuerAddressLine2"));

					swiftMessageValue = (String) swiftMessageData.get("NonBankIssuerAddressLine3");
					if (swiftMessageValue != null)
					{
						xmlDoc.setAttribute("../AddressLine3", swiftMessageValue);
					}
				}
			}
         } //IR GOUH051562143 Rajat - end

         swiftMessageValue = (String) swiftMessageData.get("ApplicantBankName");
         if (swiftMessageValue != null)
         {
            xmlDoc.setAttribute("/Body/ApplicantBank/Name", swiftMessageValue);
            xmlDoc.setAttribute("../AddressLine1",          (String) swiftMessageData.get("ApplicantBankAddressLine1"));
            xmlDoc.setAttribute("../AddressLine2",          (String) swiftMessageData.get("ApplicantBankAddressLine2"));

            swiftMessageValue = (String) swiftMessageData.get("ApplicantBankAddressLine3");
            if (swiftMessageValue != null)
            {
               xmlDoc.setAttribute("../AddressLine3", swiftMessageValue);
            }
         }

         xmlDoc.setAttribute("/Body/Applicant/Name", (String) swiftMessageData.get("ApplicantName"));
         xmlDoc.setAttribute("../AddressLine1",      (String) swiftMessageData.get("ApplicantAddressLine1"));
         xmlDoc.setAttribute("../AddressLine2",      (String) swiftMessageData.get("ApplicantAddressLine2"));

         swiftMessageValue = (String) swiftMessageData.get("ApplicantAddressLine3");
         if (swiftMessageValue != null)
         {
            xmlDoc.setAttribute("../AddressLine3", swiftMessageValue);
         }

         xmlDoc.setAttribute("../../Beneficiary1/Name", (String) swiftMessageData.get("Beneficiary1Name"));
         xmlDoc.setAttribute("../AddressLine1",         (String) swiftMessageData.get("Beneficiary1AddressLine1"));
         xmlDoc.setAttribute("../AddressLine2",         (String) swiftMessageData.get("Beneficiary1AddressLine2"));

         swiftMessageValue = (String) swiftMessageData.get("Beneficiary1AddressLine3");
         if (swiftMessageValue != null)
         {
            xmlDoc.setAttribute("../AddressLine3", swiftMessageValue);
         }

         if (swiftMessageId.equals(TradePortalConstants.SWIFT_MESSAGE_720))
         {
            swiftMessageValue = (String) swiftMessageData.get("Beneficiary2Name");
            if (swiftMessageValue != null)
            {
               xmlDoc.setAttribute("../../Beneficiary2/Name", swiftMessageValue);
               xmlDoc.setAttribute("../AddressLine1",         (String) swiftMessageData.get("Beneficiary2AddressLine1"));
               xmlDoc.setAttribute("../AddressLine2",         (String) swiftMessageData.get("Beneficiary2AddressLine2"));

               swiftMessageValue = (String) swiftMessageData.get("Beneficiary2AddressLine3");
               if (swiftMessageValue != null)
               {
                  xmlDoc.setAttribute("../AddressLine3", swiftMessageValue);
               }
            }
         }

         xmlDoc.setAttribute("../../CreditCurrency", (String) swiftMessageData.get("CreditCurrency"));
         xmlDoc.setAttribute("../CreditAmount",      (String) swiftMessageData.get("CreditAmount"));

         swiftMessageValue = (String) swiftMessageData.get("PlusTolerance");
         if (swiftMessageValue != null)
         {
            xmlDoc.setAttribute("../AmountTolerance/PlusTolerance", swiftMessageValue);
            xmlDoc.setAttribute("../MinusTolerance",                (String) swiftMessageData.get("MinusTolerance"));
         }

         swiftMessageValue = (String) swiftMessageData.get("MaximumAmount");
         if (swiftMessageValue != null)
         {
            xmlDoc.setAttribute("/Body/MaximumAmount", swiftMessageValue);
         }

         swiftMessageValue = (String) swiftMessageData.get("AdditionalCover");
         if (swiftMessageValue != null)
         {
            xmlDoc.setAttribute("/Body/AdditionalCover", swiftMessageValue);
         }

         xmlDoc.setAttribute("/Body/AvailableWith", (String) swiftMessageData.get("AvailableWith"));
         xmlDoc.setAttribute("../AvailableBy",      (String) swiftMessageData.get("AvailableBy"));
         xmlDoc.setAttribute("../Tenor",            (String) swiftMessageData.get("Tenor"));

         swiftMessageValue = (String) swiftMessageData.get("DraweeBankIdentifierCode");
         if (swiftMessageValue != null)
         {
            xmlDoc.setAttribute("../DraweeBank/BankIdentifierCode", swiftMessageValue);
         }
         else
         {
            swiftMessageValue = (String) swiftMessageData.get("DraweeBankName");
            if (swiftMessageValue != null)
            {
               xmlDoc.setAttribute("../DraweeBank/Name", swiftMessageValue);
               xmlDoc.setAttribute("../AddressLine1",    (String) swiftMessageData.get("DraweeBankAddressLine1"));
               xmlDoc.setAttribute("../AddressLine2",    (String) swiftMessageData.get("DraweeBankAddressLine2"));

               swiftMessageValue = (String) swiftMessageData.get("DraweeBankAddressLine3");
               if (swiftMessageValue != null)
               {
                  xmlDoc.setAttribute("../AddressLine3", swiftMessageValue);
               }
            }
         }

         swiftMessageValue = (String) swiftMessageData.get("MixedPaymentDetails");
         if (swiftMessageValue != null)
         {
            xmlDoc.setAttribute("/Body/MixedPaymentDetails", swiftMessageValue);
         }

         swiftMessageValue = (String) swiftMessageData.get("DeferredPaymentDetails");
         if (swiftMessageValue != null)
         {
            xmlDoc.setAttribute("/Body/DeferredPaymentDetails", swiftMessageValue);
         }

         swiftMessageValue = (String) swiftMessageData.get("PartialShipment");
         if (swiftMessageValue != null)
         {
            xmlDoc.setAttribute("/Body/Shipment/PartialShipment", swiftMessageValue);
         }

         swiftMessageValue = (String) swiftMessageData.get("Transshipment");
         if (swiftMessageValue != null)
         {
            xmlDoc.setAttribute("/Body/Shipment/Transshipment", swiftMessageValue);
         }

	

        swiftMessageValue = (String) swiftMessageData.get("ShipFrom");
         if (swiftMessageValue != null)
         {
            xmlDoc.setAttribute("/Body/Shipment/PlaceOfTakingInCharge.DispatchFrom.PlaceOfReceipt", swiftMessageValue);
         }


		swiftMessageValue = (String) swiftMessageData.get("PortOfLoading");
		if (swiftMessageValue != null)
		{
			  xmlDoc.setAttribute("/Body/Shipment/PortOfLoading.AirportOfDeparture", swiftMessageValue);
		}

		swiftMessageValue = (String) swiftMessageData.get("PortOfDischarge");
		if (swiftMessageValue != null)
		{
			  xmlDoc.setAttribute("/Body/Shipment/PortOfDischarge.AirportOfDestination", swiftMessageValue);
		}

         swiftMessageValue = (String) swiftMessageData.get("ShipTo");
         if (swiftMessageValue != null)
         {
            xmlDoc.setAttribute("/Body/Shipment/PlaceOfFinalDestination.ForTransportationTo.PlaceOfDelivery", swiftMessageValue);
         }

	//IR GOUH051562143 Rajat - end

         swiftMessageValue = (String) swiftMessageData.get("LatestShipDate");
         if (swiftMessageValue != null)
         {
            xmlDoc.setAttribute("/Body/Shipment/LatestShipDate", swiftMessageValue);
         }

         swiftMessageValue = (String) swiftMessageData.get("ShipmentPeriod");
         if (swiftMessageValue != null)
         {
            xmlDoc.setAttribute("/Body/Shipment/ShipmentPeriod", swiftMessageValue);
         }

         swiftMessageValue = (String) swiftMessageData.get("GoodsDescription");
         if (swiftMessageValue != null)
         {
            xmlDoc.setAttribute("/Body/Shipment/GoodsDescription", swiftMessageValue);
         }

        // Narayan IR-NIUK102059836 31-Oct-10 Begin
        swiftMessageValue = (String) swiftMessageData.get("GoodsDescriptionPart2");
         if (swiftMessageValue != null)
         {
           xmlDoc.setAttribute("/Body/Shipment/GoodsDescriptionPart2",swiftMessageValue);
        }
       // Narayan IR-NIUK102059836 31-Oct-10 End

         swiftMessageValue = (String) swiftMessageData.get("Charges");
         if (swiftMessageValue != null)
         {
            xmlDoc.setAttribute("/Body/Charges", swiftMessageValue);
         }

         swiftMessageValue = (String) swiftMessageData.get("PresentationPeriod");
         if (swiftMessageValue != null)
         {
            xmlDoc.setAttribute("/Body/PresentationPeriod", swiftMessageValue);
         }

         swiftMessageValue = (String) swiftMessageData.get("ConfirmationInstructions");
         if (swiftMessageValue != null)
         {
            xmlDoc.setAttribute("/Body/ConfirmationInstructions", swiftMessageValue);
         }

         if (!swiftMessageId.equals(TradePortalConstants.SWIFT_MESSAGE_700))
         {
            swiftMessageValue = (String) swiftMessageData.get("SendingBankName");
            if (swiftMessageValue != null)
            {
               xmlDoc.setAttribute("/Body/SendingBank/Name", swiftMessageValue);
               xmlDoc.setAttribute("../AddressLine1",        (String) swiftMessageData.get("SendingBankAddressLine1"));
               xmlDoc.setAttribute("../AddressLine2",        (String) swiftMessageData.get("SendingBankAddressLine2"));

               swiftMessageValue = (String) swiftMessageData.get("SendingBankAddressLine3");
               if (swiftMessageValue != null)
               {
                  xmlDoc.setAttribute("../AddressLine3", swiftMessageValue);
               }
            }
         }
      }
      else
      {
         swiftMessageValue = (String) swiftMessageData.get("GoodsDescription");
         if (swiftMessageValue != null)
         {
            xmlDoc.setAttribute("/Body/Shipment/GoodsDescription", swiftMessageValue);
         }
      }

      swiftMessageValue = (String) swiftMessageData.get("DocumentsRequired");
      if (swiftMessageValue != null)
      {
         xmlDoc.setAttribute("/Body/DocumentsRequired", swiftMessageValue);
      }

     // Narayan IR-NIUK102059836 31-Oct-10 Begin
     swiftMessageValue = (String) swiftMessageData.get("DocumentsRequiredPart2");
      if (swiftMessageValue != null)
      {
         xmlDoc.setAttribute("/Body/DocumentsRequiredPart2", swiftMessageValue);
      }
     // Narayan IR-NIUK102059836 31-Oct-10 End

      swiftMessageValue = (String) swiftMessageData.get("AdditionalConditions");
      if (swiftMessageValue != null)
      {
         xmlDoc.setAttribute("/Body/AdditionalConditions", swiftMessageValue);
      }

      // Narayan IR-NIUK102059836 31-Oct-10 Begin
       swiftMessageValue = (String) swiftMessageData.get("AdditionalConditionsPart2");
        if (swiftMessageValue != null)
       {
          xmlDoc.setAttribute("/Body/AdditionalConditionsPart2",swiftMessageValue);
        }

      // Narayan IR-NIUK102059836 31-Oct-10 End

      docPrepValue = instrument.getAttribute("complete_instrument_id");
      if ((docPrepValue != null) && (!docPrepValue.equals("")))
      {
         xmlDoc.setAttribute("/Body/BankReference", docPrepValue);
      }

      if ((availableAmount != null) && (!availableAmount.equals("")))
      {
         xmlDoc.setAttribute("/Body/CurrentBalance", availableAmount);
      }

      if ((lastPaymentDate != null) && (!lastPaymentDate.equals("")))
      {
         xmlDoc.setAttribute("/Body/DateLastDrawn", lastPaymentDate);
      }

      docPrepValue = originalTransaction.getAttribute("liability_amt_in_limit_curr");
      if ((docPrepValue != null) && (!docPrepValue.equals("")))
      {
         xmlDoc.setAttribute("/Body/LiabilityAmount", docPrepValue);
      }

      if ((lastAmendmentDate != null) && (!lastAmendmentDate.equals("")))
      {
         xmlDoc.setAttribute("/Body/LastAmendedDate", lastAmendmentDate);
      }

      docPrepValue = terms.getAttribute("pmt_terms_num_days_after");
      if ((docPrepValue != null) && (!docPrepValue.equals("")))
      {
         xmlDoc.setAttribute("/Body/TenorDays", docPrepValue);
      }

          //IR-GAUI051582080 Krishna 05/19/2008 Begin
	  //Commented the following line and added a line below that, to convert xssHtml codes back to
	  //original characters.
      out.println(xssHtmlToCharsForXmlDoc(StringFunction.xssHtmlToChars(xmlDoc.toString())));
         //IR-GAUI051582080 Krishna 05/19/2008 End
   }


        /**
    * This method is used to download report data for the report currently being viewed
    * in a report detail page.
    *

    * @param      request - the Http servlet request object
    * @exception  javax.servlet.ServletException
    * @exception  java.io.IOException
    * @return     void
    */
  private void downloadReport(HttpServletRequest request, HttpServletResponse response)
                                 throws ServletException, IOException, AmsException
     {
           ServletOutputStream     out                 = null;
          String                  storageToken        = null;
          String                  reportName          = null;
          
          StringBuilder            defaultFileName     = null;
   		  ReportEngine			cdzReportEngine     = null;
  		DocumentInstance		widDoc				= null;
  		String eol = "\n";      //End of line
      	String vd = "\"";       //Value delimiter
      	String cs = ",";        //Column separator



          HttpSession session = request.getSession(false);
      
  		storageToken = request.getParameter("storageToken");



  		out = response.getOutputStream();

  		//E6.5 changes
  		cdzReportEngine = (ReportEngine) session.getAttribute("ReportEngine");

  		if (cdzReportEngine.isReady())
      	{
  			widDoc = cdzReportEngine.getDocumentFromStorageToken(storageToken);
       	}

  		// Set the default file name to use for the report data file to be downloaded
          defaultFileName = new StringBuilder();

  		//Retrieve the report name from the report
          if(widDoc!=null){
        	  reportName = widDoc.getProperties().getProperty(PropertiesType.NAME, "");
          }

  		defaultFileName.append((reportName+".csv").replace('/', '_'));

  		// Set the http servlet response content type and content disposition (i.e., default filename)
  		setResponseContent(response, TradePortalConstants.DOWNLOAD_CSV_CONTENT_TYPE, defaultFileName.toString());


  		if(widDoc!=null){
  		DataProviders arrDataProviders = widDoc.getDataProviders();

  		if (null != arrDataProviders && arrDataProviders.getCount()>0)
  		{
  			int iNbDP = arrDataProviders.getCount();

          	for (int i=0; i<iNbDP; i++)
              {

  				DataProvider objDP = arrDataProviders.getItem(i);

  				Recordset objRec = objDP.getResult(0);

  				if (null != objRec)
                  {
  					//Get Headers
  					String strHeader = "";
  					int k = 0;
  					int iNbColumn = objRec.getColumnCount();

  					for (k=0; k<iNbColumn; k++)
                      {

  						String strColumn = objRec.getColumnName(k);

  						if (k == 0)
  						{
  						   strHeader += vd + strColumn + vd;

  						}
                          else
  						{
  						   strHeader += cs + vd + strColumn + vd;

  						}

                      }

  					strHeader += eol;
                      out.print(strHeader);
                      
  					//Flush to output
                      out.flush();

                     //Get Data
                     boolean blnEndOfFile = false;
                     // Narayan IR-EAUL061747135 Rel 7.1 Begin
                     /**
                      *  While downloading report in CSV format, date is coming for BOXI R3 in 'EEE MMM dd HH:mm:ss zzz yyyy' format, so
                      *  we need to conver it to 'M/d/yyyy H:mm'format same as BOXI R2.
                      *  here getting configuration parameter 'csvDefaultDateFormat' and 'csvRequiredDateFormat' from TradePortal.properties
                      * if it is not defined, then it will use default value.csvDefaultDateFormat and csvRequiredDateFormat are optional property.
                      */
                     PropertyResourceBundle portalProperties     = null;
                     SimpleDateFormat dateFormatter1             = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy");
                     SimpleDateFormat dateFormatter2             = new SimpleDateFormat("M/d/yyyy H:mm");

                     /**
                      * if object data type in reporting universe is Numeric and data more than 12 digits, then
                      * data in report display data in scientific characters.
                      * as if data is in '123456789012345' then in report it is coming as scintific format'12345678+E0E'
                      * below format '#.0########' is used to convert scintific to again in number with one default decimal as per requirement
                      */

                     DecimalFormat     numberFormat               = new DecimalFormat("#.0########"); // Narayan IR-KRUL102144615  Rel 7.1.0.2

                     portalProperties = (PropertyResourceBundle) PropertyResourceBundle.getBundle("TradePortal");
                     try{
                    	    dateFormatter1 = new SimpleDateFormat(portalProperties.getString("csvDefaultDateFormat"));
                       }
                     catch(MissingResourceException e){
                    	    // Ignore - assgined default date format if property not mentioned in property file
                    	  }
                     catch(Exception e){
                    	    	LOG.debug("date format csvDefaultDateFormat:" + portalProperties.getString("csvDefaultDateFormat")
                    	    			                   + " defined in property file is not correct");
                    	    	e.printStackTrace();
                    	    }

                     try{
                    	    dateFormatter2 = new SimpleDateFormat(portalProperties.getString("csvRequiredDateFormat"));
                      }
                     catch(MissingResourceException e){
                        //Ignore - assgined default date format if property not mentioned in property file
                    	 }
                     catch(Exception e){
                 	    	LOG.debug("date format csvRequiredDateFormat=" + portalProperties.getString("csvRequiredDateFormat")
	    			                   + " defined in property file is not correct. taking default date format as - M/d/yyyy H:mm ");

                 	    	e.printStackTrace();
                 	    }

                    try{
                    	numberFormat         = new DecimalFormat(portalProperties.getString("repCSVNumberForamt"));
                    }
                    catch(MissingResourceException e){
                     //ignore - take the default decimal format.
                 	 }
                    catch(Exception e){
              	    	LOG.debug("Number format repCSVNumberForamt=" + portalProperties.getString("repCSVNumberForamt")
	    			                   + " defined in property file is not correct");
              	    	e.printStackTrace();
              	    }

                    //Narayan IR-EAUL061747135 Rel 7.1 End
                     do
  					{
  						String strRowValues = "";
  						for (k=0; k<iNbColumn; k++)
  						{
  							String strValue = "";
  							try
  							{
  								//if returened cell object is not null, then only should call toString.
  								//Narayan IR-EAUL061747135 Rel 7.1.0.2 Begin
  								Object obj = objRec.getCellObject(k);
                                if(null != obj){
  									strValue = obj.toString();
  								 }
                                //Narayan IR-EAUL061747135 Rel 7.1.0.2 End
                               try{
  								//Narayan IR-EAUL061747135 Rel 7.1 Begin
  								   if(obj instanceof Date){
  									 Date d1 = dateFormatter1.parse(strValue);
  									 strValue = dateFormatter2.format(d1);
  								   }
  								   //Narayan IR-KRUL102144615  Rel 7.1.0.2 Begin
  								 else if(obj instanceof Double){
  									 strValue = numberFormat.format(obj);
  								   }
                                  // Narayan IR-KRUL102144615  Rel 7.1.0.2 End
                              }
  							 catch(Exception e)	{
  								LOG.debug("Issue with foramt parsing");
  								 e.printStackTrace();
  							  }
  							}
  						       //Narayan IR-EAUL061747135 Rel 7.1 End
                               catch(Exception e)
  							 {
  								blnEndOfFile = true;
  								break;
                                }

  							if (k == 0)
  								strRowValues += vd + strValue + vd;
                              else
  								strRowValues += cs + vd + strValue + vd;
                          }

  						if (!blnEndOfFile)
  						{
  							strRowValues += eol;
  							out.print(strRowValues);
  							//Flush to output
  							out.flush();

  							if (objRec.isLast())
  								blnEndOfFile = true;
  							else
  								objRec.next();
                          }
                      }
                     while(!blnEndOfFile);
  				}

               }


  		} //End (null != arrDataProviders && arrDataProviders.getCount()>0)
     }
          out.close();

  		//Garbage Collection
  		widDoc = null;


}



  //	rkrishna CR 375-D ATP-ISS 07/21/2007 Begin
   /**
    * This method is used to download Approval To Pay data for the instrument currently being viewed
    * in the Approval To Pay Issue transaction detail page.
    *

    * @param      request - the Http servlet request object
    * @exception  javax.servlet.ServletException
    * @exception  java.io.IOException
    * @return     void
    */
   private void downloadApprovalToPayData(HttpServletRequest request, HttpServletResponse response)
                                     throws ServletException, IOException, AmsException
   {
      ServletOutputStream   out               = null;
      InstrumentWebBean     instrument        = null;
      TermsPartyWebBean     termsParty        = null;
      TransactionWebBean    transaction       = null;
      com.amsinc.ecsg.util.ResourceManager       resourceManager   = null;
      TermsWebBean          terms             = null;
      StringBuilder          defaultFileName   = null;
      
      BeanManager           beanManager       = null;
      HttpSession           session           = null;
      boolean               isValidTermsParty = false;
      
      String                resourceLocale    = null;
      

      // First we need to retrieve the resource manager from the session so that we can retrieve
      // internationalized field name strings
      session         = request.getSession(false);
      resourceManager = (com.amsinc.ecsg.util.ResourceManager) session.getAttribute("resMgr");
      beanManager     = (BeanManager)     session.getAttribute("beanMgr");

      // Retrieve the http servlet output stream
      out = response.getOutputStream();

    
      // Retrieve the locale from resource manager; this should be currently set to the user's locale
      resourceLocale = resourceManager.getResourceLocale();

      // Next, retrieve all web beans in the session pertaining to the Approval To Pay instrument currently being viewed
      instrument   = (InstrumentWebBean)  beanManager.getBean("Instrument");
      terms        = (TermsWebBean)       beanManager.getBean("Terms");
      //MEer Rel 9.2 IR-38100
     transaction   = (TransactionWebBean)  beanManager.getBean("Transaction");

      String instrumentType = instrument.getAttribute("instrument_type_code");
      String custEnteredTermsOid = transaction.getAttribute("c_CustomerEnteredTerms");
     
      // Set the default file name to use for the Approval To Pay/Request Advise data file to be downloaded
      defaultFileName = new StringBuilder();

      if (instrumentType.equals(InstrumentType.REQUEST_ADVISE))
    	  defaultFileName.append(resourceManager.getText("RequestAdviseIssue.DownloadFileNamePrefix",
                  TradePortalConstants.TEXT_BUNDLE, "en_US"));
      else
    	  defaultFileName.append(resourceManager.getText("ApprovalToPayIssue.DownloadFileNamePrefix",
                                                     TradePortalConstants.TEXT_BUNDLE, "en_US"));

      defaultFileName.append(instrument.getAttribute("complete_instrument_id").replace('/', '_'));

      if (instrumentType.equals(InstrumentType.REQUEST_ADVISE))
    	  defaultFileName.append(resourceManager.getText("RequestAdviseIssue.DownloadFileNameSuffix",
                                                     TradePortalConstants.TEXT_BUNDLE, "en_US"));
      else
    	  defaultFileName.append(resourceManager.getText("ApprovalToPayIssue.DownloadFileNameSuffix",
              TradePortalConstants.TEXT_BUNDLE, "en_US"));

      // Set the http servlet response content type and content disposition (i.e., default filename)
      setResponseContent(response, TradePortalConstants.DOWNLOAD_TXT_CONTENT_TYPE, defaultFileName.toString());

	  // One instance of TermsPartyWebBean that will be used to retrieve different term party information
	termsParty = beanManager.createBean(TermsPartyWebBean.class, "TermsParty");

	downloadApprovalToPayDataInstrumentID(out, instrument, resourceManager);

	downloadApprovalToPayDataBeneficiary(
		out,
		terms,
		resourceManager,
		isValidTermsParty,
		resourceLocale,
		termsParty);

	downloadApprovalToPayDataApplicant(
		out,
		terms,
		resourceManager,
		resourceLocale,
		termsParty);

	if (instrumentType.equals(InstrumentType.REQUEST_ADVISE)) { // It is a Request to Advise
		downloadRequestAdviseDataApplicantBank(
				out,
				terms,
				resourceManager,
				resourceLocale,
				termsParty);
		downloadRequestAdviseDataAdviseThroughBank(
				out,
				terms,
				resourceManager,
				resourceLocale,
				termsParty);
		downloadRequestAdviseDataReimbursingBank(
				out,
				terms,
				resourceManager,
				resourceLocale,
				termsParty);
		downloadRequestAdviseDataIssuer(
				out,
				terms,
				resourceManager,
				resourceLocale,
				termsParty);

	}
	else { // It is an Approval To Pay Issue
		downloadApprovalToPayDataAdvisingBank(
		out,
		terms,
		resourceManager,
		resourceLocale,
		termsParty);
	}

	downloadApprovalToPayDataTermsGeneral(
		out,
		terms,
		instrumentType,
		instrument,
		resourceManager,
		resourceLocale);

	downloadApprovalToPayDataDocs(out, terms, resourceManager, resourceLocale);

    // One instance of ShipmentTermsWebBean that will be used to retrieve data for each ShipmentTerms object.
	ShipmentTermsWebBean shipmentTerms = beanManager.createBean(ShipmentTermsWebBean.class, "ShipmentTerms");
	//MEer Rel 9.2 IR-38100
	if(StringFunction.isNotBlank(custEnteredTermsOid)){
		terms = transaction.registerCustomerEnteredTerms();
		if(!terms.isValid()){
			LOG.info("Error occurred retrieving TERMS deatils with termsOid: " + transaction.getAttribute("c_CustomerEnteredTerms"));
			}
	}

	downloadApprovalToPayDataAllShipmentTerms(
		out,
		terms,
		resourceManager,
		termsParty,
		resourceLocale,
		shipmentTerms);

	downloadApprovalToPayDataOtherConditions(out, terms, resourceManager);

   }
   /**
	* Download instrument id
	*
	* @param out
	* @param instrument
	* @param resourceManager
	* @throws IOException
	*/
  private void downloadApprovalToPayDataInstrumentID(
	   ServletOutputStream out,
	   InstrumentWebBean instrument,
	   com.amsinc.ecsg.util.ResourceManager resourceManager)
	   throws IOException {
	   StringBuilder downloadData;
		 // Retrieve the instrument number of the Approval To Pay instrument currently being viewed
		 downloadData = new StringBuilder();
		 downloadData.append(resourceManager.getText("ApprovalToPayIssue.ATPNumber", TradePortalConstants.TEXT_BUNDLE, "en_US"));
		 downloadData.append(" : ");
		 downloadData.append(instrument.getAttribute("complete_instrument_id"));
		 out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
		 out.println("");
  }
  /**
   * Download the general Terms data
   *
   * @param out
   * @param terms
   * @param resourceManager
   * @param resourceLocale
   * @throws IOException
   * @throws AmsException
   */
  private void downloadApprovalToPayDataTermsGeneral(
  	ServletOutputStream out,
  	TermsWebBean terms,
  	String instrumentType,
  	InstrumentWebBean instrument,
  	com.amsinc.ecsg.util.ResourceManager resourceManager,
  	String resourceLocale)
  	throws IOException, AmsException {
  	StringBuilder downloadData;
  	
  	String bankChargesType;
  	String currencyCode;
  	  // Retrieve the Buyer's reference number, the Approval To Pay transaction's expiry date, currency, amount,
  	  // amount tolerance, payment terms, and bank charges
  	  downloadData = new StringBuilder();
  	  downloadData.append(resourceManager.getText("transaction.BuyerRefNumber", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	  downloadData.append(" : ");
  	  downloadData.append(terms.getAttribute("reference_number"));
  	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
  	  out.println("");

  	  if (instrumentType.equals(InstrumentType.REQUEST_ADVISE)) { // Request Advise add Issuer Ref Num and Issue Date
  		  // Issuer Reference Number
  		  downloadData = new StringBuilder();
  		  downloadData.append(resourceManager.getText("RequestAdviseIssue.IssuerReferenceNumber", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  		  downloadData.append(" : ");
  		  downloadData.append(terms.getAttribute("issuer_ref_num"));
  		  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
  		  out.println("");
  		  // Issue Date
  		  downloadData = new StringBuilder();
  		  downloadData.append(resourceManager.getText("RequestAdviseIssue.IssueDate", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  		  downloadData.append(" : ");
  		  downloadData.append(TPDateTimeUtility.formatDate(instrument.getAttribute("issue_date"), TPDateTimeUtility.LONG,
                    resourceLocale));
  		  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
  		  out.println("");
  	  }

  	  downloadData = new StringBuilder();
  	  downloadData.append(resourceManager.getText("ApprovalToPayIssue.ExpiryDate", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	  downloadData.append(" : ");
  	  downloadData.append(TPDateTimeUtility.formatDate(terms.getAttribute("expiry_date"), TPDateTimeUtility.LONG,
  	                                                   resourceLocale));
  	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
  	  out.println("");

  	  if (instrumentType.equals(InstrumentType.REQUEST_ADVISE)) { // Request Advise add Expiry Place
  		  // Expiry Place
  		  downloadData = new StringBuilder();
  		  downloadData.append(resourceManager.getText("RequestAdviseIssue.ExpiryPlace", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  		  downloadData.append(" : ");
  		  downloadData.append(terms.getAttribute("place_of_expiry"));
  		  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
  		  out.println("");
  	  }

  	  // Retrieve the currency code of the transaction
  	  currencyCode = terms.getAttribute("amount_currency_code");

  	  downloadData = new StringBuilder();
  	  downloadData.append(resourceManager.getText("transaction.Currency", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	  downloadData.append(" : ");
  	  downloadData.append(currencyCode);
  	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
  	  out.println("");

  	  downloadData = new StringBuilder();
  	  downloadData.append(resourceManager.getText("transaction.Amount", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	  downloadData.append(" : ");
  	  downloadData.append(TPCurrencyUtility.getDisplayAmount(terms.getAttribute("amount"), currencyCode,
  	                                                         resourceLocale));
  	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
  	  out.println("");

  	  String notExceeding = "";
  	  if (instrumentType.equals(InstrumentType.REQUEST_ADVISE)) { // Request Advise Check to See if not Exceeding is Necessary and put in Amount Details title
  		 downloadData = new StringBuilder();
  		 downloadData.append(resourceManager.getText("RequestAdviseIssue.AmountDetails", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  		 downloadData.append(" -");
  		 out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
  		 notExceeding = terms.getAttribute("maximum_credit_amount");
  		 if (!StringFunction.isBlank(notExceeding)) {
  			 downloadData = new StringBuilder();
  			 downloadData.append(resourceManager.getText("RequestAdviseIssue.MaximumCreditAmount", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  			 downloadData.append(" : ");
  			 downloadData.append(notExceeding);
  			 out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
  			 out.println("");
  		 }
  	  }
  	  if (StringFunction.isBlank(notExceeding)) {
  		  downloadData = new StringBuilder();
  		  downloadData.append(resourceManager.getText("ApprovalToPayIssue.AmtTolerance", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  		  downloadData.append(" : ");
  		  downloadData.append(resourceManager.getText("transaction.Plus", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  		  downloadData.append(terms.getAttribute("amt_tolerance_pos"));
  		  downloadData.append(resourceManager.getText("transaction.Percent", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  		  downloadData.append(" ");
  		  downloadData.append(resourceManager.getText("transaction.Minus", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  		  downloadData.append(terms.getAttribute("amt_tolerance_neg"));
  		  downloadData.append(resourceManager.getText("transaction.Percent", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  		  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
  		  out.println("");
  	  }

  	  if (instrumentType.equals(InstrumentType.REQUEST_ADVISE)) { // Request Advise add Additional Amounts Covered, Availability, and Draft Details
  		  // Additional Amounts Covered
  		  downloadData = new StringBuilder();
  		  downloadData.append(resourceManager.getText("RequestAdviseIssue.AdditionalAmountsCovered", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  		  downloadData.append(" : ");
  		  downloadData.append(terms.getAttribute("addl_amounts_covered"));
  		  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
  		  out.println("");
  		  // Availability
  		  downloadData = new StringBuilder();
  		  downloadData.append(resourceManager.getText("RequestAdviseIssue.Availability", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  		  downloadData.append(" -");
  		  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
  		  downloadData = new StringBuilder();
  		  downloadData.append(resourceManager.getText("RequestAdviseIssue.AvailBy", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  		  downloadData.append(" : ");
  		  //Leelavathi IR#T36000016452 Rel-8.2 06/10/2013 Begin
  		  //commenting out, because RQA instrument has Available_by at two places.
  		  downloadData.append(ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.AVAILABLE_BY,
                    terms.getAttribute("payment_type"),
                    resourceLocale));
  		  //Leelavathi IR#T36000016452 Rel-8.2 06/10/2013 End
  		  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
  		  downloadData = new StringBuilder();
  		  downloadData.append(resourceManager.getText("RequestAdviseIssue.AvailWithPty", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  		  downloadData.append(" : ");
  		  downloadData.append(ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.AVAILABLE_WITH_PARTY,
                    terms.getAttribute("available_with_party"),
                    resourceLocale));
  		  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
  		  out.println("");
  		  // Draft Details
  		  downloadData = new StringBuilder();
  		  downloadData.append(resourceManager.getText("RequestAdviseIssue.DraftDetails", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  		  downloadData.append("- ");
  		  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
  		  downloadData = new StringBuilder();
  		  downloadData.append(resourceManager.getText("RequestAdviseIssue.DraftReqd", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  		  downloadData.append(" : ");
  		  if (terms.getAttribute("drafts_required").equals(TradePortalConstants.INDICATOR_YES)) {
  			  downloadData.append(resourceManager.getText("common.Yes", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  			  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
  			  downloadData = new StringBuilder();
  			  downloadData.append(resourceManager.getText("RequestAdviseIssue.DrawnOnPty", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  			  downloadData.append(" : ");
  			  downloadData.append(ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.DRAWN_ON_PARTY,
  	                  terms.getAttribute("drawn_on_party"),
  	                  resourceLocale));
  		  } else {
  			  downloadData.append(resourceManager.getText("common.No", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  		  }
  		  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
  		  out.println("");
  	  }

  	  // Retrieve the transaction's bank charges type
  	  bankChargesType = terms.getAttribute("bank_charges_type");

  	  downloadData = new StringBuilder();
  	  downloadData.append(resourceManager.getText("ApprovalToPayIssue.BankCharges", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	  downloadData.append(" : ");
  	  
  	  if (TradePortalConstants.CHARGE_OUR_ACCT.equals(bankChargesType))
  	  {
  	     downloadData.append(resourceManager.getText("ApprovalToPayIssue.AllOurAcct", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	  }
  	  else if (TradePortalConstants.CHARGE_BEN_FOR_OUTSIDE.equals(bankChargesType))
  	  {
  	     downloadData.append(resourceManager.getText("ApprovalToPayIssue.AllOutside", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	  }
  	  else if (TradePortalConstants.CHARGE_OTHER.equals(bankChargesType))
  	  {
  	     downloadData.append(resourceManager.getText("ApprovalToPayIssue.Other", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	  }

  	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
  	  out.println("");
  }
  /**
   * Download the Advising Bank data
   *
   * @param out
   * @param terms
   * @param resourceManager
   * @param resourceLocale
   * @param termsParty
   * @throws IOException
   * @throws AmsException
   */
  private void downloadApprovalToPayDataAdvisingBank(
  	ServletOutputStream out,
  	TermsWebBean terms,
  	com.amsinc.ecsg.util.ResourceManager resourceManager,
  	String resourceLocale,
  	TermsPartyWebBean termsParty)
  	throws IOException, AmsException {
  	StringBuilder downloadData;
  	boolean isValidTermsParty;
  	String termsPartyOid;
  	  // Retrieve the Advising Bank terms party from the database
  	  termsPartyOid = terms.getAttribute("c_ThirdTermsParty");

  	  if (!StringFunction.isBlank(termsPartyOid))
  	  {
  	     termsParty.setAttribute("terms_party_oid", termsPartyOid);
  	     termsParty.getDataFromAppServer();

  	     isValidTermsParty = true;
  	  }
  	  else
  	  {
  	     isValidTermsParty = false;
  	  }

  	  // Start retrieving all saved Advising Bank data of the Approval To Pay being viewed
  	  downloadData = new StringBuilder();
  	  downloadData.append(resourceManager.getText("ApprovalToPayIssue.InsuringParty", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	  downloadData.append(" - ");
  	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
  	  out.println("");

  	  downloadData = new StringBuilder();
  	  downloadData.append(resourceManager.getText("ApprovalToPayIssue.Name", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	  downloadData.append(" : ");
  	  if (isValidTermsParty)
  	  {
  	     downloadData.append(termsParty.getAttribute("name"));
  	  }
  	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

  	  downloadData = new StringBuilder();
  	  downloadData.append(resourceManager.getText("ApprovalToPayIssue.PhoneNumber", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	  downloadData.append(" : ");
  	  if (isValidTermsParty)
  	  {
  	     downloadData.append(termsParty.getAttribute("phone_number"));
  	  }
  	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

  	  downloadData = new StringBuilder();
  	  downloadData.append(resourceManager.getText("ApprovalToPayIssue.AddressLine1", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	  downloadData.append(" : ");
  	  if (isValidTermsParty)
  	  {
  	     downloadData.append(termsParty.getAttribute("address_line_1"));
  	  }
  	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

  	  downloadData = new StringBuilder();
  	  downloadData.append(resourceManager.getText("ApprovalToPayIssue.AddressLine2", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	  downloadData.append(" : ");
  	  if (isValidTermsParty)
  	  {
  	     downloadData.append(termsParty.getAttribute("address_line_2"));
  	  }
  	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

  	  downloadData = new StringBuilder();
  	  downloadData.append(resourceManager.getText("ApprovalToPayIssue.City", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	  downloadData.append(" : ");
  	  if (isValidTermsParty)
  	  {
  	     downloadData.append(termsParty.getAttribute("address_city"));
  	  }
  	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

  	  downloadData = new StringBuilder();
  	  downloadData.append(resourceManager.getText("ApprovalToPayIssue.ProvinceState", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	  downloadData.append(" : ");
  	  if (isValidTermsParty)
  	  {
  	     downloadData.append(termsParty.getAttribute("address_state_province"));
  	  }
  	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

  	  downloadData = new StringBuilder();
  	  downloadData.append(resourceManager.getText("ApprovalToPayIssue.Country", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	  downloadData.append(" : ");
  	  if (isValidTermsParty && (!StringFunction.isBlank(termsParty.getAttribute("address_country"))))
  	  {
  	     downloadData.append(ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.COUNTRY,
  	                                                                       termsParty.getAttribute("address_country"),
  	                                                                       resourceLocale));
  	  }
  	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

  	  downloadData = new StringBuilder();
  	  downloadData.append(resourceManager.getText("ApprovalToPayIssue.PostalCode", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	  downloadData.append(" : ");
  	  if (isValidTermsParty)
  	  {
  	     downloadData.append(termsParty.getAttribute("address_postal_code"));
  	  }
  	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
  	  out.println("");

  }
  /**
   * Download the Buyer data
   * @param out
   * @param terms
   * @param resourceManager
   * @param resourceLocale
   * @param termsParty
   * @throws IOException
   * @throws AmsException
   */
  private void downloadApprovalToPayDataApplicant(
  	ServletOutputStream out,
  	TermsWebBean terms,
  	com.amsinc.ecsg.util.ResourceManager resourceManager,
  	String resourceLocale,
  	TermsPartyWebBean termsParty)
  	throws IOException, AmsException {
  	StringBuilder downloadData;
  	boolean isValidTermsParty;
  	String termsPartyOid;
  	  // Retrieve the Buyer terms party from the database
  	  termsPartyOid = terms.getAttribute("c_SecondTermsParty");

  	  if (!StringFunction.isBlank(termsPartyOid))
  	  {
  	     termsParty.setAttribute("terms_party_oid", termsPartyOid);
  	     termsParty.getDataFromAppServer();

  	     isValidTermsParty = true;
  	  }
  	  else
  	  {
  	     isValidTermsParty = false;
  	  }

  	  // Start retrieving all saved Buyer data of the Approval To Pay being viewed
  	  downloadData = new StringBuilder();
  	  downloadData.append(resourceManager.getText("ApprovalToPayIssue.Buyer", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	  downloadData.append(" - ");
  	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
  	  out.println("");

  	  downloadData = new StringBuilder();
  	  downloadData.append(resourceManager.getText("ApprovalToPayIssue.Name", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	  downloadData.append(" : ");
  	  if (isValidTermsParty)
  	  {
  	     downloadData.append(termsParty.getAttribute("name"));
  	  }
  	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

  	  downloadData = new StringBuilder();
  	  downloadData.append(resourceManager.getText("ApprovalToPayIssue.PhoneNumber", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	  downloadData.append(" : ");
  	  if (isValidTermsParty)
  	  {
  	     downloadData.append(termsParty.getAttribute("phone_number"));
  	  }
  	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

  	  downloadData = new StringBuilder();
  	  downloadData.append(resourceManager.getText("ApprovalToPayIssue.AddressLine1", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	  downloadData.append(" : ");
  	  if (isValidTermsParty)
  	  {
  	     downloadData.append(termsParty.getAttribute("address_line_1"));
  	  }
  	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

  	  downloadData = new StringBuilder();
  	  downloadData.append(resourceManager.getText("ApprovalToPayIssue.AddressLine2", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	  downloadData.append(" : ");
  	  if (isValidTermsParty)
  	  {
  	     downloadData.append(termsParty.getAttribute("address_line_2"));
  	  }
  	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

  	  downloadData = new StringBuilder();
  	  downloadData.append(resourceManager.getText("ApprovalToPayIssue.City", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	  downloadData.append(" : ");
  	  if (isValidTermsParty)
  	  {
  	     downloadData.append(termsParty.getAttribute("address_city"));
  	  }
  	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

  	  downloadData = new StringBuilder();
  	  downloadData.append(resourceManager.getText("ApprovalToPayIssue.ProvinceState", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	  downloadData.append(" : ");
  	  if (isValidTermsParty)
  	  {
  	     downloadData.append(termsParty.getAttribute("address_state_province"));
  	  }
  	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

  	  downloadData = new StringBuilder();
  	  downloadData.append(resourceManager.getText("ApprovalToPayIssue.Country", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	  downloadData.append(" : ");
  	  if (isValidTermsParty && (!StringFunction.isBlank(termsParty.getAttribute("address_country"))))
  	  {
  	     downloadData.append(ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.COUNTRY,
  	                                                                       termsParty.getAttribute("address_country"),
  	                                                                       resourceLocale));
  	  }
  	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

  	  downloadData = new StringBuilder();
  	  downloadData.append(resourceManager.getText("ApprovalToPayIssue.PostalCode", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	  downloadData.append(" : ");
  	  if (isValidTermsParty)
  	  {
  	     downloadData.append(termsParty.getAttribute("address_postal_code"));
  	  }
  	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
  	  out.println("");

  }
  /**
   * Download the Beneficiary data
   *
   * @param out
   * @param terms
   * @param resourceManager
   * @param isValidTermsParty
   * @param resourceLocale
   * @param termsParty
   * @throws IOException
   * @throws AmsException
   */
  private void downloadApprovalToPayDataBeneficiary(
  	ServletOutputStream out,
  	TermsWebBean terms,
  	com.amsinc.ecsg.util.ResourceManager resourceManager,
  	boolean isValidTermsParty,
  	String resourceLocale,
  	TermsPartyWebBean termsParty)
  	throws IOException, AmsException {
  	StringBuilder downloadData;
  	String termsPartyOid;
  	  // Retrieve the Beneficiary terms party from the database
  	  termsPartyOid = terms.getAttribute("c_FirstTermsParty");


  	  if (!StringFunction.isBlank(termsPartyOid))
  	  {
  	     termsParty.setAttribute("terms_party_oid", termsPartyOid);
  	     termsParty.getDataFromAppServer();

  	     isValidTermsParty = true;
  	  }

  	  // Start retrieving all saved Beneficiary data of the Approval To Pay being viewed
  	  downloadData = new StringBuilder();
  	  downloadData.append(resourceManager.getText("ApprovalToPayIssue.Seller", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	  downloadData.append(" - ");
  	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
  	  out.println("");

  	  downloadData = new StringBuilder();
  	  downloadData.append(resourceManager.getText("ApprovalToPayIssue.Name", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	  downloadData.append(" : ");
  	  if (isValidTermsParty)
  	  {
  	     downloadData.append(termsParty.getAttribute("name"));
  	  }
  	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

  	  downloadData = new StringBuilder();
  	  downloadData.append(resourceManager.getText("ApprovalToPayIssue.PhoneNumber", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	  downloadData.append(" : ");
  	  if (isValidTermsParty)
  	  {
  	     downloadData.append(termsParty.getAttribute("phone_number"));
  	  }
  	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

  	  downloadData = new StringBuilder();
  	  downloadData.append(resourceManager.getText("ApprovalToPayIssue.AddressLine1", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	  downloadData.append(" : ");
  	  if (isValidTermsParty)
  	  {
  	     downloadData.append(termsParty.getAttribute("address_line_1"));
  	  }
  	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

  	  downloadData = new StringBuilder();
  	  downloadData.append(resourceManager.getText("ApprovalToPayIssue.AddressLine2", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	  downloadData.append(" : ");
  	  if (isValidTermsParty)
  	  {
  	     downloadData.append(termsParty.getAttribute("address_line_2"));
  	  }
  	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

  	  downloadData = new StringBuilder();
  	  downloadData.append(resourceManager.getText("ApprovalToPayIssue.City", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	  downloadData.append(" : ");
  	  if (isValidTermsParty)
  	  {
  	     downloadData.append(termsParty.getAttribute("address_city"));
  	  }
  	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

  	  downloadData = new StringBuilder();
  	  downloadData.append(resourceManager.getText("ApprovalToPayIssue.ProvinceState", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	  downloadData.append(" : ");
  	  if (isValidTermsParty)
  	  {
  	     downloadData.append(termsParty.getAttribute("address_state_province"));
  	  }
  	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

  	  downloadData = new StringBuilder();
  	  downloadData.append(resourceManager.getText("ApprovalToPayIssue.Country", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	  downloadData.append(" : ");
  	  if (isValidTermsParty && (!StringFunction.isBlank(termsParty.getAttribute("address_country"))))
  	  {
  	     downloadData.append(ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.COUNTRY,
  	                                                                       termsParty.getAttribute("address_country"),
  	                                                                       resourceLocale));
  	  }
  	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

  	  downloadData = new StringBuilder();
  	  downloadData.append(resourceManager.getText("ApprovalToPayIssue.PostalCode", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	  downloadData.append(" : ");
  	  if (isValidTermsParty)
  	  {
  	     downloadData.append(termsParty.getAttribute("address_postal_code"));
  	  }
  	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
  	  out.println("");
  }
  /**
   * Download the data of the required documents
   *
   * @param out
   * @param terms
   * @param resourceManager
   * @param resourceLocale
   * @throws IOException
   * @throws AmsException
   */
  private void downloadApprovalToPayDataDocs(
  	ServletOutputStream out,
  	TermsWebBean terms,
  	com.amsinc.ecsg.util.ResourceManager resourceManager,
  	String resourceLocale)
  	throws IOException, AmsException {
  	StringBuilder downloadData;
  	  // *****************DOCS REQUIRED SECTION***********************************//
  	  // Retrieve all information pertaining to any necessary documents required for the Approval To Pay instrument
  	  if (TradePortalConstants.INDICATOR_YES.equals(terms.getAttribute("comm_invoice_indicator")))
  	  {
  	     downloadData = new StringBuilder();
  	     downloadData.append(resourceManager.getText("ApprovalToPayIssue.CommercialInv", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	     downloadData.append(" - ");
  	     out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

  	     downloadData = new StringBuilder();
  	     downloadData.append(resourceManager.getText("ApprovalToPayIssue.Originals", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	     downloadData.append(" : ");
  	     downloadData.append(terms.getAttribute("comm_invoice_originals"));
  	     out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

  	     downloadData = new StringBuilder();
  	     downloadData.append(resourceManager.getText("ApprovalToPayIssue.Copies", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	     downloadData.append(" : ");
  	     downloadData.append(terms.getAttribute("comm_invoice_copies"));
  	     out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

  	     downloadData = new StringBuilder();
  	     downloadData.append(resourceManager.getText("ApprovalToPayIssue.DocumentText", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	     downloadData.append(" : ");
  	     downloadData.append(terms.getAttribute("comm_invoice_text"));
  	     out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
  	     out.println("");
  	  }

  	  if (TradePortalConstants.INDICATOR_YES.equals(terms.getAttribute("packing_list_indicator")))
  	  {
  	     downloadData = new StringBuilder();
  	     downloadData.append(resourceManager.getText("ApprovalToPayIssue.PackingList", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	     downloadData.append(" - ");
  	     out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

  	     downloadData = new StringBuilder();
  	     downloadData.append(resourceManager.getText("ApprovalToPayIssue.Originals", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	     downloadData.append(" : ");
  	     downloadData.append(terms.getAttribute("packing_list_originals"));
  	     out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

  	     downloadData = new StringBuilder();
  	     downloadData.append(resourceManager.getText("ApprovalToPayIssue.Copies", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	     downloadData.append(" : ");
  	     downloadData.append(terms.getAttribute("packing_list_copies"));
  	     out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

  	     downloadData = new StringBuilder();
  	     downloadData.append(resourceManager.getText("ApprovalToPayIssue.DocumentText", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	     downloadData.append(" : ");
  	     downloadData.append(terms.getAttribute("packing_list_text"));
  	     out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
  	     out.println("");
  	  }

  	  if (TradePortalConstants.INDICATOR_YES.equals(terms.getAttribute("cert_origin_indicator")))
  	  {
  	     downloadData = new StringBuilder();
  	     downloadData.append(resourceManager.getText("ApprovalToPayIssue.OriginCertificate",
  	                                                 TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	     downloadData.append(" - ");
  	     out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

  	     downloadData = new StringBuilder();
  	     downloadData.append(resourceManager.getText("ApprovalToPayIssue.Originals", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	     downloadData.append(" : ");
  	     downloadData.append(terms.getAttribute("cert_origin_originals"));
  	     out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

  	     downloadData = new StringBuilder();
  	     downloadData.append(resourceManager.getText("ApprovalToPayIssue.Copies", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	     downloadData.append(" : ");
  	     downloadData.append(terms.getAttribute("cert_origin_copies"));
  	     out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

  	     downloadData = new StringBuilder();
  	     downloadData.append(resourceManager.getText("ApprovalToPayIssue.DocumentText", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	     downloadData.append(" : ");
  	     downloadData.append(terms.getAttribute("cert_origin_text"));
  	     out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
  	     out.println("");
  	  }

  	  if (TradePortalConstants.INDICATOR_YES.equals(terms.getAttribute("ins_policy_indicator")))
  	  {
  	     downloadData = new StringBuilder();
  	     downloadData.append(resourceManager.getText("ApprovalToPayIssue.InsPolicyCert",
  	                                                 TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	     downloadData.append(" - ");
  	     out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

  	     downloadData = new StringBuilder();
  	     downloadData.append(resourceManager.getText("ApprovalToPayIssue.Originals", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	     downloadData.append(" : ");
  	     downloadData.append(terms.getAttribute("ins_policy_originals"));
  	     out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

  	     downloadData = new StringBuilder();
  	     downloadData.append(resourceManager.getText("ApprovalToPayIssue.Copies", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	     downloadData.append(" : ");
  	     downloadData.append(terms.getAttribute("ins_policy_copies"));
  	     out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

  	     downloadData = new StringBuilder();
  	     downloadData.append(resourceManager.getText("ApprovalToPayIssue.Endorsement", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	     downloadData.append(" : ");
  	     downloadData.append(resourceManager.getText("ApprovalToPayIssue.EndorsedInBlank",
  	                                                 TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	     downloadData.append(" ");
  	     downloadData.append(terms.getAttribute("insurance_endorse_value_plus"));
  	     downloadData.append(resourceManager.getText("ApprovalToPayIssue.Covering", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	     downloadData.append(" ");

  	     if (!StringFunction.isBlank(terms.getAttribute("insurance_risk_type")))
  	     {
  	        downloadData.append(ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.INSURANCE_RISK_TYPE,
  	                                                                          terms.getAttribute("insurance_risk_type"),
  	                                                                          resourceLocale));
  	     }
  	     out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

  	     downloadData = new StringBuilder();
  	     downloadData.append(resourceManager.getText("ApprovalToPayIssue.DocumentText", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	     downloadData.append(" : ");
  	     downloadData.append(terms.getAttribute("ins_policy_text"));
  	     out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
  	     out.println("");
  	  }

  	  // This next part includes the four user-specified documents

  	  if (TradePortalConstants.INDICATOR_YES.equals(terms.getAttribute("other_req_doc_1_indicator")))
  	  {
  	     downloadData = new StringBuilder();
  	     downloadData.append(terms.getAttribute("other_req_doc_1_name"));
  	     downloadData.append(" - ");
  	     out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

  	     downloadData = new StringBuilder();
  	     downloadData.append(resourceManager.getText("ApprovalToPayIssue.Originals", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	     downloadData.append(" : ");
  	     downloadData.append(terms.getAttribute("other_req_doc_1_originals"));
  	     out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

  	     downloadData = new StringBuilder();
  	     downloadData.append(resourceManager.getText("ApprovalToPayIssue.Copies", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	     downloadData.append(" : ");
  	     downloadData.append(terms.getAttribute("other_req_doc_1_copies"));
  	     out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

  	     downloadData = new StringBuilder();
  	     downloadData.append(resourceManager.getText("ApprovalToPayIssue.DocumentText", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	     downloadData.append(" : ");
  	     downloadData.append(terms.getAttribute("other_req_doc_1_text"));
  	     out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
  	     out.println("");
  	  }

  	  if (TradePortalConstants.INDICATOR_YES.equals(terms.getAttribute("other_req_doc_2_indicator")))
  	  {
  	     downloadData = new StringBuilder();
  	     downloadData.append(terms.getAttribute("other_req_doc_2_name"));
  	     downloadData.append(" - ");
  	     out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

  	     downloadData = new StringBuilder();
  	     downloadData.append(resourceManager.getText("ApprovalToPayIssue.Originals", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	     downloadData.append(" : ");
  	     downloadData.append(terms.getAttribute("other_req_doc_2_originals"));
  	     out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

  	     downloadData = new StringBuilder();
  	     downloadData.append(resourceManager.getText("ApprovalToPayIssue.Copies", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	     downloadData.append(" : ");
  	     downloadData.append(terms.getAttribute("other_req_doc_2_copies"));
  	     out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

  	     downloadData = new StringBuilder();
  	     downloadData.append(resourceManager.getText("ApprovalToPayIssue.DocumentText", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	     downloadData.append(" : ");
  	     downloadData.append(terms.getAttribute("other_req_doc_2_text"));
  	     out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
  	     out.println("");
  	  }

  	  if (TradePortalConstants.INDICATOR_YES.equals(terms.getAttribute("other_req_doc_3_indicator")))
  	  {
  	     downloadData = new StringBuilder();
  	     downloadData.append(terms.getAttribute("other_req_doc_3_name"));
  	     downloadData.append(" - ");
  	     out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

  	     downloadData = new StringBuilder();
  	     downloadData.append(resourceManager.getText("ApprovalToPayIssue.Originals", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	     downloadData.append(" : ");
  	     downloadData.append(terms.getAttribute("other_req_doc_3_originals"));
  	     out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

  	     downloadData = new StringBuilder();
  	     downloadData.append(resourceManager.getText("ApprovalToPayIssue.Copies", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	     downloadData.append(" : ");
  	     downloadData.append(terms.getAttribute("other_req_doc_3_copies"));
  	     out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

  	     downloadData = new StringBuilder();
  	     downloadData.append(resourceManager.getText("ApprovalToPayIssue.DocumentText", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	     downloadData.append(" : ");
  	     downloadData.append(terms.getAttribute("other_req_doc_3_text"));
  	     out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
  	     out.println("");
  	  }



  	  if (TradePortalConstants.INDICATOR_YES.equals(terms.getAttribute("other_req_doc_4_indicator")))
  	  {
  	     downloadData = new StringBuilder();
  	     downloadData.append(terms.getAttribute("other_req_doc_4_name"));
  	     downloadData.append(" - ");
  	     out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

  	     downloadData = new StringBuilder();
  	     downloadData.append(resourceManager.getText("ApprovalToPayIssue.Originals", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	     downloadData.append(" : ");
  	     downloadData.append(terms.getAttribute("other_req_doc_4_originals"));
  	     out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

  	     downloadData = new StringBuilder();
  	     downloadData.append(resourceManager.getText("ApprovalToPayIssue.Copies", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	     downloadData.append(" : ");
  	     downloadData.append(terms.getAttribute("other_req_doc_4_copies"));
  	     out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

  	     downloadData = new StringBuilder();
  	     downloadData.append(resourceManager.getText("ApprovalToPayIssue.DocumentText", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	     downloadData.append(" : ");
  	     downloadData.append(terms.getAttribute("other_req_doc_4_text"));
  	     out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
  	     out.println("");
  	  }

		List<AmsEntityWebBean> additionalReqDocList = terms.getChildren("AdditionalReqDoc","addl_req_doc_oid","p_terms_oid"); 
		
		for(AmsEntityWebBean additionalReqDoc : additionalReqDocList) {      
			String addlDocRequiredInd = additionalReqDoc.getAttribute("addl_req_doc_ind");
			  if (TradePortalConstants.INDICATOR_YES.equals(addlDocRequiredInd))
			  {
			     downloadData = new StringBuilder();
			     downloadData.append(additionalReqDoc.getAttribute("addl_req_doc_name"));
			     downloadData.append(" - ");
			     out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

			     downloadData = new StringBuilder();
			     downloadData.append(resourceManager.getText("ApprovalToPayIssue.Originals", TradePortalConstants.TEXT_BUNDLE, "en_US"));
			     downloadData.append(" : ");
			     downloadData.append(additionalReqDoc.getAttribute("addl_req_doc_originals"));
			     out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

			     downloadData = new StringBuilder();
			     downloadData.append(resourceManager.getText("ApprovalToPayIssue.Copies", TradePortalConstants.TEXT_BUNDLE, "en_US"));
			     downloadData.append(" : ");
			     downloadData.append(additionalReqDoc.getAttribute("addl_req_doc_copies"));
			     out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

			     downloadData = new StringBuilder();
			     downloadData.append(resourceManager.getText("ApprovalToPayIssue.DocumentText", TradePortalConstants.TEXT_BUNDLE, "en_US"));
			     downloadData.append(" : ");
			     downloadData.append(additionalReqDoc.getAttribute("addl_req_doc_text"));
			     out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
			     out.println("");
			  }

		}


  	  if (TradePortalConstants.INDICATOR_YES.equals(terms.getAttribute("addl_doc_indicator")))
  	  {
  	     downloadData = new StringBuilder();
  	     downloadData.append(resourceManager.getText("ApprovalToPayIssue.AddlDocs1", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	     downloadData.append(" ");
  	     downloadData.append(resourceManager.getText("ApprovalToPayIssue.AddlDocs2", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	     downloadData.append(" - ");
  	     out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

  	     downloadData = new StringBuilder();
  	     downloadData.append(resourceManager.getText("ApprovalToPayIssue.DocumentText", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	     downloadData.append(" : ");
  	     downloadData.append(terms.getAttribute("addl_doc_text"));
  	     out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
  	     out.println("");
  	  }
  }
  /**
   * Download the other conditions for ATP Data
   *
   * @param out
   * @param terms
   * @param resourceManager
   * @throws IOException
   */
  private void downloadApprovalToPayDataOtherConditions(
  	ServletOutputStream out,
  	TermsWebBean terms,
  	com.amsinc.ecsg.util.ResourceManager resourceManager)
  	throws IOException {
  	StringBuilder downloadData;
  	  //*****************************OTHER CONDITIONS SUBSECTION**************************************/
  	  // Retrieve all saved other conditions data for the Issue Approval To Pay transaction
  	  downloadData = new StringBuilder();
  	  downloadData.append(resourceManager.getText("ApprovalToPayIssue.DownloadOtherConditions",
  	                                              TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	  downloadData.append(" - ");
  	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
  	  //IR T36000018742 Start
  	  downloadData = new StringBuilder();
  	  downloadData.append(resourceManager.getText("ApprovalToPayIssue.AprovedInvOnly",
            TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	  downloadData.append(":");
  	 if (TradePortalConstants.INDICATOR_YES.equals(terms.getAttribute("invoice_only_ind"))) 
		  downloadData.append(resourceManager.getText("common.Yes", TradePortalConstants.TEXT_BUNDLE, "en_US"));
	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
  	  downloadData = new StringBuilder();
  	  downloadData.append(resourceManager.getText("PaymentDiscountDetail.InvoiceDueDate",
                TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	  if(StringFunction.isNotBlank(terms.getAttribute("invoice_due_date")))
  	  downloadData.append(TPDateTimeUtility.formatDate(terms.getAttribute("invoice_due_date"), TPDateTimeUtility.LONG,
  			resourceManager.getResourceLocale()));
  	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
  	  out.println("");
  	  downloadData = new StringBuilder();
  	  downloadData.append(resourceManager.getText("InvoicesOfferedDetail.InvoiceDetails",
            TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	  out.print(StringFunction.xssHtmlToChars(downloadData.toString()));
  	  out.println(":");
  	  downloadData = new StringBuilder();
  	  downloadData.append(terms.getAttribute("invoice_details"));
  	  //IR T36000018742 End
  	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
  	  out.println("");
  	  downloadData = new StringBuilder();
  	  downloadData.append(resourceManager.getText("ApprovalToPayIssue.AdditionalConditions",
  	                                               TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	  downloadData.append(" : ");
  	  downloadData.append(terms.getAttribute("additional_conditions"));
  	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
  	  out.println("");
  }
  /**
   * Download the ShipmentTerms
   *
   * @param out
   * @param terms
   * @param resourceManager
   * @param termsParty
   * @param resourceLocale
   * @param shipmentTerms
   * @throws IOException
   * @throws AmsException
   */
  private void downloadApprovalToPayDataAllShipmentTerms(
  	ServletOutputStream out,
  	TermsWebBean terms,
  	com.amsinc.ecsg.util.ResourceManager resourceManager,
  	TermsPartyWebBean termsParty,
  	String resourceLocale,
  	ShipmentTermsWebBean shipmentTerms)
  	throws IOException, AmsException {
  	
	//MEer IR-38100 Retrieve shipment terms for corresponding customer entered terms
	  DocumentHandler resultSet = DatabaseQueryBean.getXmlResultSet("select shipment_oid from shipment_terms where p_terms_oid = ? order by creation_date", false, new Object[]{terms.getAttribute("terms_oid")});
	
	  if (resultSet != null) {
		
		  Vector shipmentTermsOids = resultSet.getFragments("/ResultSetRecord");
		  int numberOfShipmentTerms = shipmentTermsOids.size();
		  for (int i=0; i<numberOfShipmentTerms; i++) {
			  String shipmentOid = ((DocumentHandler)shipmentTermsOids.elementAt(i)).getAttribute("/SHIPMENT_OID");			 
			  shipmentTerms.setAttribute("shipment_oid", shipmentOid);
			  shipmentTerms.getDataFromAppServer();

			  downloadApprovalToPayDataShipmentTerms(
					  out,
					  terms,
					  resourceManager,
					  termsParty,
					  resourceLocale,
					  shipmentTerms,
					  i+1,
					  numberOfShipmentTerms);

		  }

	  }

  }

  

  /**
   * Download one ShipmentTerms
   *
   * @param out
   * @param terms
   * @param resourceManager
   * @param termsParty
   * @param resourceLocale
   * @param shipmentTerms
   * @param indexOfShipmentTerms
   * @param numberOfShipmentTerms
   * @throws IOException
   * @throws AmsException
   */
  private void downloadApprovalToPayDataShipmentTerms(
  	ServletOutputStream out,
  	TermsWebBean terms,
  	com.amsinc.ecsg.util.ResourceManager resourceManager,
  	TermsPartyWebBean termsParty,
  	String resourceLocale,
  	ShipmentTermsWebBean shipmentTerms,
  	int indexOfShipmentTerms,
  	int numberOfShipmentTerms)
  	throws IOException, AmsException {
  	StringBuilder downloadData;
  	boolean isValidTermsParty;
  	String consignmentType;
  	String markedFreight;
  	String termsPartyOid;

  	/* e.g. Transport Document(s) and Shipment 1 of 2*/
  	if (numberOfShipmentTerms > 1) {
  	   out.println("");
  	   downloadData = new StringBuilder();
  	   downloadData.append(resourceManager.getText("ApprovalToPayIssue.TransportDocs", TradePortalConstants.TEXT_BUNDLE, "en_US"))
  	               .append("/")
  	               .append(resourceManager.getText("ApprovalToPayIssue.Shipment", TradePortalConstants.TEXT_BUNDLE, "en_US"))
  		           .append(" ")
  	               .append(indexOfShipmentTerms)
  	               .append("")
  				   .append(resourceManager.getText("ApprovalToPayIssue.Of", TradePortalConstants.TEXT_BUNDLE, "en_US"))
  				   .append("")
  		           .append(numberOfShipmentTerms)
  				   .append(" - ");
  		out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

  		downloadData = new StringBuilder();
  		downloadData.append(resourceManager.getText("ApprovalToPayIssue.Description", TradePortalConstants.TEXT_BUNDLE, "en_US"))
  		            .append(" : ")
  		            .append(shipmentTerms.getAttribute("description"));
  		out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

  		out.println("");
  	}

  	  //*****************************TRANSPORT DOCUMENTS SUBSECTION**************************************/
  	  // Retrieve all saved transport document data for the Issue Approval To Pay transaction
  	  if (TradePortalConstants.INDICATOR_YES.equals(shipmentTerms.getAttribute("trans_doc_included")))
  	  {
  		if (numberOfShipmentTerms <= 1) {
  	       downloadData = new StringBuilder();
  	       downloadData.append(resourceManager.getText("ApprovalToPayIssue.TransportDocs", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	       downloadData.append(" - ");
  		   out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
  		}

  	     downloadData = new StringBuilder();
  	     downloadData.append(resourceManager.getText("ApprovalToPayIssue.DocType", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	     downloadData.append(" : ");
  	     downloadData.append(ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.TRANS_DOC_TYPE ,
  	                                                                          shipmentTerms.getAttribute("trans_doc_type"),
  	                                                                          resourceLocale));
  	     out.println(StringFunction.xssHtmlToChars(downloadData.toString()));



  	     downloadData = new StringBuilder();
  	     downloadData.append(resourceManager.getText("ApprovalToPayIssue.Originals", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	     downloadData.append(" : ");
  	     downloadData.append(shipmentTerms.getAttribute("trans_doc_originals"));
  	     out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

  	     downloadData = new StringBuilder();
  	     downloadData.append(resourceManager.getText("ApprovalToPayIssue.Copies", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	     downloadData.append(" : ");
  	     downloadData.append(shipmentTerms.getAttribute("trans_doc_copies"));
  	     out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

  	     downloadData = new StringBuilder();
  	     downloadData.append(resourceManager.getText("ApprovalToPayIssue.DocumentText", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	     downloadData.append(" : ");
  	     downloadData.append(shipmentTerms.getAttribute("trans_doc_text"));
  	     out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
  	     out.println("");
  	  }

  	  // Retrieve the Issue Approval To Pay transaction's Consignment information
  	  downloadData = new StringBuilder();
  	  downloadData.append(resourceManager.getText("ApprovalToPayIssue.Consignment", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	  downloadData.append(" : ");

  	  consignmentType = shipmentTerms.getAttribute("trans_doc_consign_type");

  	  if (TradePortalConstants.CONSIGN_ORDER.equals(consignmentType))
  	  {
  	     downloadData.append(resourceManager.getText("ApprovalToPayIssue.ConsignedToOrderOf",
  	                                                 TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	     downloadData.append(" ");

  	     if (!StringFunction.isBlank(shipmentTerms.getAttribute("trans_doc_consign_order_of_pty")))
  	     {
  	        downloadData.append(ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.CONSIGNMENT_PARTY_TYPE,
  			shipmentTerms.getAttribute("trans_doc_consign_order_of_pty"),
  	                                                                resourceLocale));
  	     }
  	  }
  	  else if (TradePortalConstants.CONSIGN_PARTY.equals(consignmentType))
  	  {
  	     downloadData.append(resourceManager.getText("ApprovalToPayIssue.ConsignedTo", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	     downloadData.append(" ");

  	     if (!StringFunction.isBlank(shipmentTerms.getAttribute("trans_doc_consign_to_pty")))
  	     {
  	        downloadData.append(ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.CONSIGNMENT_PARTY_TYPE,
  			shipmentTerms.getAttribute("trans_doc_consign_to_pty"),
  	                                                                resourceLocale));
  	     }
  	  }

  	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
  	  out.println("");

  	  // Retrieve the Issue Approval To Pay transaction's Marked Freight information
  	  downloadData = new StringBuilder();
  	  downloadData.append(resourceManager.getText("ApprovalToPayIssue.MarkedFreight", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	  downloadData.append(" : ");

  	  markedFreight = shipmentTerms.getAttribute("trans_doc_marked_freight");

  	  if (TradePortalConstants.PREPAID.equals(markedFreight))
  	  {
  	     downloadData.append(resourceManager.getText("ApprovalToPayIssue.Prepaid", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	  }
  	  else if (TradePortalConstants.COLLECT.equals(markedFreight))
  	  {
  	     downloadData.append(resourceManager.getText("ApprovalToPayIssue.Collect", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	  }

  	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
  	  out.println("");

  	  // Retrieve the Notify terms party from the database
  	  termsPartyOid = shipmentTerms.getAttribute("c_NotifyParty");

  	  if (!StringFunction.isBlank(termsPartyOid))
  	  {
  	     termsParty.setAttribute("terms_party_oid", termsPartyOid);
  	     termsParty.getDataFromAppServer();

  	     isValidTermsParty = true;
  	  }
  	  else
  	  {
  	     isValidTermsParty = false;
  	  }

  	  // Start retrieving all saved Notify Party data of the Approval To Pay being viewed
  	  downloadData = new StringBuilder();
  	  downloadData.append(resourceManager.getText("ApprovalToPayIssue.NotifyParty", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	  downloadData.append(" - ");
  	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
  	  out.println("");

  	  downloadData = new StringBuilder();
  	  downloadData.append(resourceManager.getText("ApprovalToPayIssue.Name", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	  downloadData.append(" : ");
  	  if (isValidTermsParty)
  	  {
  	     downloadData.append(termsParty.getAttribute("name"));
  	  }
  	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

  	  downloadData = new StringBuilder();
  	  downloadData.append(resourceManager.getText("ApprovalToPayIssue.PhoneNumber", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	  downloadData.append(" : ");
  	  if (isValidTermsParty)
  	  {
  	     downloadData.append(termsParty.getAttribute("phone_number"));
  	  }
  	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

  	  downloadData = new StringBuilder();
  	  downloadData.append(resourceManager.getText("ApprovalToPayIssue.AddressLine1", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	  downloadData.append(" : ");
  	  if (isValidTermsParty)
  	  {
  	     downloadData.append(termsParty.getAttribute("address_line_1"));
  	  }
  	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

  	  downloadData = new StringBuilder();
  	  downloadData.append(resourceManager.getText("ApprovalToPayIssue.AddressLine2", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	  downloadData.append(" : ");
  	  if (isValidTermsParty)
  	  {
  	     downloadData.append(termsParty.getAttribute("address_line_2"));
  	  }
  	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

  	  downloadData = new StringBuilder();
  	  downloadData.append(resourceManager.getText("ApprovalToPayIssue.AddressLine3", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	  downloadData.append(" : ");
  	  if (isValidTermsParty)
  	  {
  	     downloadData.append(termsParty.getAttribute("address_line_3"));
  	  }
  	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
  	  out.println("");

  	  // Retrieve the Other Consignee terms party from the database
  	  termsPartyOid = shipmentTerms.getAttribute("c_OtherConsigneeParty");

  	  if (!StringFunction.isBlank(termsPartyOid))
  	  {
  	     termsParty.setAttribute("terms_party_oid", termsPartyOid);
  	     termsParty.getDataFromAppServer();

  	     isValidTermsParty = true;
  	  }
  	  else
  	  {
  	     isValidTermsParty = false;
  	  }

  	  // Start retrieving all saved Other Consignee data of the Approval To Pay being viewed
  	  downloadData = new StringBuilder();
  	  downloadData.append(resourceManager.getText("ApprovalToPayIssue.OtherConsignee", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	  downloadData.append(" - ");
  	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
  	  out.println("");

  	  downloadData = new StringBuilder();
  	  downloadData.append(resourceManager.getText("ApprovalToPayIssue.Name", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	  downloadData.append(" : ");
  	  if (isValidTermsParty)
  	  {
  	     downloadData.append(termsParty.getAttribute("name"));
  	  }
  	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

  	  downloadData = new StringBuilder();
  	  downloadData.append(resourceManager.getText("ApprovalToPayIssue.PhoneNumber", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	  downloadData.append(" : ");
  	  if (isValidTermsParty)
  	  {
  	     downloadData.append(termsParty.getAttribute("phone_number"));
  	  }
  	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

  	  downloadData = new StringBuilder();
  	  downloadData.append(resourceManager.getText("ApprovalToPayIssue.AddressLine1", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	  downloadData.append(" : ");
  	  if (isValidTermsParty)
  	  {
  	     downloadData.append(termsParty.getAttribute("address_line_1"));
  	  }
  	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

  	  downloadData = new StringBuilder();
  	  downloadData.append(resourceManager.getText("ApprovalToPayIssue.AddressLine2", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	  downloadData.append(" : ");
  	  if (isValidTermsParty)
  	  {
  	     downloadData.append(termsParty.getAttribute("address_line_2"));
  	  }
  	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

  	  downloadData = new StringBuilder();
  	  downloadData.append(resourceManager.getText("ApprovalToPayIssue.AddressLine3", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	  downloadData.append(" : ");
  	  if (isValidTermsParty)
  	  {
  	     downloadData.append(termsParty.getAttribute("address_line_3"));
  	  }
  	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
  	  out.println("");

  	  if (shipmentTerms.getAttribute("trans_addl_doc_included").equals(TradePortalConstants.INDICATOR_YES))
  	  {
  	     downloadData = new StringBuilder();
  	     downloadData.append(resourceManager.getText("ApprovalToPayIssue.AddlTransportDocs",
  	                                                 TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	     downloadData.append(" - ");
  	     out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
  	     out.println("");

  	     downloadData = new StringBuilder();
  	     downloadData.append(resourceManager.getText("ApprovalToPayIssue.DocumentText", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	     downloadData.append(" : ");
  	     downloadData.append(shipmentTerms.getAttribute("trans_addl_doc_text"));
  	     out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
  	     out.println("");
  	  }

  	  //*****************************SHIPMENT SUBSECTION**************************************/
  	  // Retrieve all saved shipment data for the Issue Approval To Pay transaction
  	  if (numberOfShipmentTerms <= 1) {
  	     downloadData = new StringBuilder();
  	     downloadData.append(resourceManager.getText("ApprovalToPayIssue.DownloadShipment", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	     downloadData.append(" - ");
  	     out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
  	     out.println("");
  	  }

  	  downloadData = new StringBuilder();
  	  downloadData.append(resourceManager.getText("ApprovalToPayIssue.TranshipAllowed", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	  downloadData.append(" : ");

  	  if (TradePortalConstants.INDICATOR_YES.equals(shipmentTerms.getAttribute("transshipment_allowed")))
  	  {
  	     downloadData.append(resourceManager.getText("common.Yes", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	  }
  	  else
  	  {
  	     downloadData.append(resourceManager.getText("common.No", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	  }
  	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

  	  downloadData = new StringBuilder();
  	  downloadData.append(resourceManager.getText("ApprovalToPayIssue.PartialAllowed", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	  downloadData.append(" : ");

  	  if (TradePortalConstants.INDICATOR_YES.equals(terms.getAttribute("partial_shipment_allowed")))
  	  {
  	     downloadData.append(resourceManager.getText("common.Yes", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	  }
  	  else
  	  {
  	     downloadData.append(resourceManager.getText("common.No", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	  }
  	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

  	  downloadData = new StringBuilder();
  	  downloadData.append(resourceManager.getText("ApprovalToPayIssue.LastestShipDate", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	  downloadData.append(" : ");
  	  downloadData.append(TPDateTimeUtility.formatDate(shipmentTerms.getAttribute("latest_shipment_date"),
  	                                                   TPDateTimeUtility.LONG, resourceLocale));
  	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

  	  downloadData = new StringBuilder();
  	  downloadData.append(resourceManager.getText("ApprovalToPayIssue.ShippingTerm", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	  downloadData.append(" : ");

  	  if (!StringFunction.isBlank(shipmentTerms.getAttribute("incoterm")))
  	  {
  	     downloadData.append(ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.INCOTERM,
  		shipmentTerms.getAttribute("incoterm"),
  	                                                                       resourceLocale));
  	  }
  	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

  	  downloadData = new StringBuilder();
  	  downloadData.append(resourceManager.getText("ApprovalToPayIssue.ShipTermLocation", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	  downloadData.append(" : ");
  	  downloadData.append(shipmentTerms.getAttribute("incoterm_location"));
  	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

  	  downloadData = new StringBuilder();
  	  downloadData.append(resourceManager.getText("ApprovalToPayIssue.DownloadShipmentFrom",
  	                                              TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	  downloadData.append(" : ");
  	  downloadData.append(shipmentTerms.getAttribute("shipment_from"));
  	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

  	  downloadData = new StringBuilder();
  	  downloadData.append(resourceManager.getText("ApprovalToPayIssue.DownloadShipmentFrom",
  	                                              TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	  downloadData.append(" : ");
  	  downloadData.append(shipmentTerms.getAttribute("shipment_from_loading"));
  	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

  	  downloadData = new StringBuilder();
  	  downloadData.append(resourceManager.getText("ApprovalToPayIssue.DownloadShipmentTo",
  	                                              TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	  downloadData.append(" : ");
  	  downloadData.append(shipmentTerms.getAttribute("shipment_to_discharge"));
  	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

  	  downloadData = new StringBuilder();
  	  downloadData.append(resourceManager.getText("ApprovalToPayIssue.DownloadShipmentTo",
  	                                              TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	  downloadData.append(" : ");
  	  downloadData.append(shipmentTerms.getAttribute("shipment_to"));
  	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

  	  downloadData = new StringBuilder();
  	  downloadData.append(resourceManager.getText("ApprovalToPayIssue.TimePeriod", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	  downloadData.append(" : ");
  	  downloadData.append(resourceManager.getText("ApprovalToPayIssue.PresentDocs", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	  downloadData.append(" - ");
  	  downloadData.append(terms.getAttribute("present_docs_within_days"));
  	  downloadData.append(" - ");
  	  downloadData.append(resourceManager.getText("ApprovalToPayIssue.DayOfShipment", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

  	  downloadData = new StringBuilder();
  	  downloadData.append(resourceManager.getText("ApprovalToPayIssue.GoodsDesc", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	  downloadData.append(" : ");
  	  downloadData.append(shipmentTerms.getAttribute("goods_description"));
  	  out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
  	  out.println("");

  	  if(!shipmentTerms.getAttribute("po_line_items").equals(""))
  	   {
  	     downloadData = new StringBuilder();
  	     downloadData.append(resourceManager.getText("ApprovalToPayIssue.POLineItems", TradePortalConstants.TEXT_BUNDLE, "en_US"));
  	     downloadData.append(" : ");
  	     out.println(StringFunction.xssHtmlToChars(downloadData.toString()));

  	     StringTokenizer newLineTokenizer = new StringTokenizer( shipmentTerms.getAttribute("po_line_items"), "\n");

  	     while(newLineTokenizer.hasMoreTokens())
  	      {
  	         out.println(newLineTokenizer.nextToken());
  	      }

  	     out.println("");
  	   }

  	   downloadStructuredPOs(out,  shipmentTerms, resourceManager);   // IR# SUUM060556386  Rel. 8.0.0
  }
  
  /**
   * Added as part of Rel9.3.5 CR-1028
   * Ability to download xml files from Portal
   * @param request
   * @param response
   * @throws ServletException
   * @throws IOException
   * @throws AmsException
   */

  private void downloadAuthorisedTransactionXML(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException, AmsException
	{
	com.amsinc.ecsg.util.ResourceManager       resourceManager   = null;
	HttpSession           					   session           = null;
	String                					   resourceLocale    = null;
	String									   logFileName		 = null;
	String 									   zipFileName		 = null;
	String 									   xmlFileName		 = null;
	String 									   xmlFileExtn		 = null;
	String 									   transactionOid	 = null;
	String 									   msgText			 = null;
	String 				 sqlStatementToUpdateIndicator	 		 = null;
	int										   totalCount		 = 0;
	int										   failedCount		 = 0;
	SessionWebBean 							   userSession 	  	 = null;
	SecretKey 								   secretKey 		 = null;
	StringBuilder 							   logFileContent 	 = new StringBuilder();
	List<String> 							   transactionOidList= new ArrayList<String>();
	
	
	// First we need to retrieve the resource manager from the session so that we can retrieve
	// internationalized field name strings
	session         = request.getSession(false);
	resourceManager = (com.amsinc.ecsg.util.ResourceManager) session.getAttribute("resMgr");
	userSession = (SessionWebBean) session.getAttribute("userSession");
	String userTimeZone = userSession.getTimeZone();
	
	
    try {
        java.lang.reflect.Method getSecretKey = userSession.getClass().getMethod("getSecretKey", new Class[]{});
        secretKey = ((SecretKey) getSecretKey.invoke(userSession, new Object[]{}));
    } catch (Exception e) {
    	LOG.debug("downloadAuthorisedTransactionXML::Error initializing the secretKey");
        e.printStackTrace();
    }
	// Retrieve the locale from resource manager; this should be currently set to the user's locale
	resourceLocale = resourceManager.getResourceLocale();
	
	Date gmtDateTime = GMTUtility.getGMTDateTime(true,false);
	SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
	
    xmlFileExtn = resourceManager.getText("AuthorizedXMLTransactions.DownloadXMLSuffix",TradePortalConstants.TEXT_BUNDLE, resourceLocale);
    //Create Log file
	try{
		DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
		Date currentDateTimeForTimeZone = TPDateTimeUtility.getLocalDateTime((formatter.format(gmtDateTime)).toString(), userSession.getTimeZone());
		logFileName = resourceManager.getText("AuthorizedXMLTransactions.T360DownloadFileName",TradePortalConstants.TEXT_BUNDLE, resourceLocale) + 
					  dateFormat.format(currentDateTimeForTimeZone).toString() +
					  resourceManager.getText("AuthorizedXMLTransactions.DownloadLOGSuffix",TradePortalConstants.TEXT_BUNDLE, resourceLocale);
		zipFileName = resourceManager.getText("AuthorizedXMLTransactions.T360DownloadFileName",TradePortalConstants.TEXT_BUNDLE, resourceLocale) + 
				  	  dateFormat.format(currentDateTimeForTimeZone).toString() +
				      resourceManager.getText("AuthorizedXMLTransactions.DownloadZIPSuffix",TradePortalConstants.TEXT_BUNDLE, resourceLocale);
	}catch(Exception e){
		LOG.debug("TradePortalDownloadServlet::downloadAuthorisedTransactionXML::Exception while formatting current datetime. Naming the file names to default");
		logFileName = resourceManager.getText("AuthorizedXMLTransactions.T360DownloadFileName",TradePortalConstants.TEXT_BUNDLE, resourceLocale) + 
				      resourceManager.getText("AuthorizedXMLTransactions.DownloadLOGSuffix",TradePortalConstants.TEXT_BUNDLE, resourceLocale);
		zipFileName = resourceManager.getText("AuthorizedXMLTransactions.T360DownloadFileName",TradePortalConstants.TEXT_BUNDLE, resourceLocale) + 
			  	  	  resourceManager.getText("AuthorizedXMLTransactions.DownloadZIPSuffix",TradePortalConstants.TEXT_BUNDLE, resourceLocale);
	}
    												
    Enumeration paramNames = request.getParameterNames();
	
	//Read throught the request parameters and create a list of transactionOids selected from the grid
    while(paramNames.hasMoreElements()) {
       String paramName = (String)paramNames.nextElement();
       if(paramName.startsWith("transactionOid")){
    	   transactionOidList.add(EncryptDecrypt.decryptStringUsingTripleDes(request.getParameter(paramName), secretKey));
       }
    }
    
    LOG.debug("TradePortalDownloadServlet::downloadAuthorisedTransactionXML:: Adding xml files to zip file Begins");
    totalCount = transactionOidList.size();
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    ZipOutputStream zos = new ZipOutputStream(baos); 
    for(int i=0; i<transactionOidList.size();i++){
    	String instrumentID = null;
	    try{
	    	Map<String,String> transactiondetailsMap = new HashMap<String,String>(); 
	    	transactionOid = transactionOidList.get(i).toString();
		    transactiondetailsMap = getTransactionDetails(transactionOid, xmlFileExtn, userTimeZone);
		    if(transactiondetailsMap.isEmpty()){
		    	String sql = "select complete_instrument_id from INSTRUMENT, TRANSACTION where instrument_oid = p_instrument_oid and transaction_oid=? ";
		    	DocumentHandler iDoc = DatabaseQueryBean.getXmlResultSet(sql, false, new Object[]{transactionOid});
		    	if(iDoc!=null){
		    		instrumentID = iDoc.getAttribute("/ResultSetRecord(0)/COMPLETE_INSTRUMENT_ID");
		    	}
		    }
	    	msgText = transactiondetailsMap.get("msgText").toString();
	    	xmlFileName = transactiondetailsMap.get("xmlFileName").toString();
	    	instrumentID = transactiondetailsMap.get("instrumentID").toString();
	    	addToZipFile(xmlFileName, zos, msgText);
	    	
	  		sqlStatementToUpdateIndicator = "UPDATE transaction SET downloaded_xml_ind = ? WHERE downloaded_xml_ind =? and transaction_oid = ?";
	  		DatabaseQueryBean.executeUpdate(sqlStatementToUpdateIndicator, true, new Object[]{TradePortalConstants.INDICATOR_YES,TradePortalConstants.INDICATOR_NO,transactionOid});	  		
	  		sqlStatementToUpdateIndicator = "UPDATE outgoing_q_history SET downloaded_xml_ind =? WHERE downloaded_xml_ind =? and a_transaction_oid = ?";
	  		DatabaseQueryBean.executeUpdate(sqlStatementToUpdateIndicator, true, new Object[]{TradePortalConstants.INDICATOR_YES,TradePortalConstants.INDICATOR_NO,transactionOid});
	  		
	  		logFileContent.append("Adding XML file for Instrument ID- '" + instrumentID + "' to zip file successful").append("\r\n");
		}catch (Exception e){
			failedCount = failedCount + 1;
			logFileContent.append("Exception in adding xml file for Instrument ID- '" + instrumentID + "' to zip file").append("\r\n");
			LOG.debug("TradePortalDownloadServlet::downloadAuthorisedTransactionXML::Exception in adding xml file for InstrumentID- '" + instrumentID + "' to zip file");
	        e.printStackTrace();        
	    }
	    finally{
	        baos.close();
	    }
    }
    LOG.debug("Downloaded "+ (totalCount-failedCount)+" out of "+totalCount+" files succesfully." );
    logFileContent.append("Downloaded "+ (totalCount-failedCount)+" out of "+totalCount+" files succesfully." );
    addToZipFile(logFileName,zos,logFileContent.toString());
    LOG.debug("TradePortalDownloadServlet::downloadAuthorisedTransactionXML:: Adding xml files to zip file End");
    
    zos.close();
    setResponseContent(response, TradePortalConstants.DOWNLOAD_ZIP_CONTENT_TYPE, zipFileName.toString());
    response.getOutputStream().write(baos.toByteArray());
    response.flushBuffer();
    
   }
  
  /**
   * This method adds an entry to Zip file
   * @param fileName - FileName
   * @param zos - ZipOutputStream
   * @param data - FileContent
   * @throws FileNotFoundException
   * @throws IOException
   */
  public static void addToZipFile(String fileName, ZipOutputStream zos, String data) throws FileNotFoundException, IOException {

		LOG.debug("Adding file - '" + fileName + "' to zip file");
		ZipEntry zipEntry = new ZipEntry(fileName);
		zos.putNextEntry(zipEntry);
		byte[] byteData = data.getBytes();
		zos.write(byteData, 0, byteData.length);
		zos.closeEntry();
	}
  
  /**
   * This method fetches msg_text/instrumentID/fileName for the transaction to be downloaded
   * @param transactionOid
   * @param fileExtension
   * @param userTimeZone
   * @return Map (populated with details)
   * @throws AmsException
   */
  private Map getTransactionDetails(String transactionOid, String fileExt, String userTimeZone) throws AmsException {
		
		String queueOid = "";
	    String msg_text = "";
		String instrumentID = "";
		StringBuffer fileName = new StringBuffer();
		Map<String,String> transactionDetailsMap = new HashMap<String,String>();
		//Fetch the latest record for a transaction from outgoing_q_history. So, in case of rejected and re-authorized transactions, the latest record is pulled
		String sql = "select max(o.queue_history_oid) as QueueOid, b.complete_instrument_id, a.transaction_type_code, a.display_transaction_oid as transactionID, to_char( a.transaction_status_date,'dd/MM/yyyy hh:mi:ss AM') as TRANSACTIONDATE "
				+ " from transaction a, instrument b, outgoing_q_history o"
				+ " where a.p_instrument_oid = b.instrument_oid and b.instrument_oid = o.a_instrument_oid and a.transaction_oid = o.a_transaction_oid and a.transaction_oid=? "
                + " GROUP BY b.complete_instrument_id, a.transaction_type_code, a.display_transaction_oid, TO_CHAR( a.transaction_status_date,'dd/MM/yyyy hh:mi:ss AM') ";
		
		try {
		DocumentHandler iDoc = DatabaseQueryBean.getXmlResultSet(sql, false, new Object[]{transactionOid});
	    	if(iDoc!=null){
	    		String authDate = null;
	    		SimpleDateFormat displayFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss aa");
			    SimpleDateFormat outputFormat = new SimpleDateFormat("yyyyMMddHHmmss");
			    SimpleDateFormat userDateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
			    Date date = null;
			    String displayDate = null;
	    		queueOid = iDoc.getAttribute("/ResultSetRecord(0)/QUEUEOID");
	    		authDate = iDoc.getAttribute("/ResultSetRecord(0)/TRANSACTIONDATE");
				
				 date = displayFormat.parse(authDate);
		         Date authDateTimeForTimeZone = TPDateTimeUtility.getLocalDateTime((userDateFormat.format(date)).toString(), userTimeZone);//Convert the dateTime to User's timezone
		         displayDate = outputFormat.format(authDateTimeForTimeZone);
		        
				 fileName.append(iDoc.getAttribute("/ResultSetRecord(0)/COMPLETE_INSTRUMENT_ID")).append("_")
					.append(iDoc.getAttribute("/ResultSetRecord(0)/TRANSACTION_TYPE_CODE")).append("_")
					.append(iDoc.getAttribute("/ResultSetRecord(0)/TRANSACTIONID")).append("_")
					.append(displayDate)
					.append(fileExt);
				 instrumentID = iDoc.getAttribute("/ResultSetRecord(0)/COMPLETE_INSTRUMENT_ID");
				 
				 //Fetch msg_text of the queue_history_oid from outgoing_q_history table
				 String sqlMsgText = "select msg_text from outgoing_q_history where queue_history_oid=? ";
				try(Connection con = DatabaseQueryBean.connect(true);
						PreparedStatement pStmt = con.prepareStatement(sqlMsgText)){
				 pStmt.setString(1, queueOid);
				 try (ResultSet results = pStmt.executeQuery()){
					 if ( results.next() ) {
					 java.sql.NClob nclob = results.getNClob("msg_text");
					 msg_text = nclob.getSubString(1, (int)nclob.length());
					 }
				 }catch (SQLException e) {
						e.printStackTrace();
					}
				}catch (SQLException e) {
					e.printStackTrace();
				}
				 transactionDetailsMap.put("msgText", msg_text);
				 transactionDetailsMap.put("xmlFileName", fileName.toString());
				 transactionDetailsMap.put("instrumentID", instrumentID);
				 
	    	}
		}	catch(ParseException pe){
	         pe.printStackTrace();
	       }
    	
		return transactionDetailsMap;
 }
  
//rkrishna CR 375-D ATP-ISS 07/21/2007 End
  /*KMehta - 19 Feb 2015 - Rel9.3 IR-T36000013054 - Add  - End */

   /**
    * This method sets the Http response's content type to a Trade Portal-specific
    * application type so that a Save file dialog will be displayed, and it sets the
    * content disposition so that a default file name can be used.
    *

    * @param   response        - the Http servlet response object
    * @param   contentType     - the Trade Portal specific content type for the
    *                            file being downloaded
    * @param   defaultFileName - the default name of the file to be downloaded
    * @return  void
    */
   public void setResponseContent(HttpServletResponse response, String contentType, String defaultFileName)
   {
      StringBuilder   contentDisposition = new StringBuilder();

      // Set the content type to a TradePortal-specific application type so that it can be saved as a local file
      response.setContentType(contentType);

      contentDisposition.append("attachment;filename=");
      contentDisposition.append(defaultFileName);

      // Set the content disposition so that the filename uses a unique naming convention
      response.setHeader("Content-Disposition", contentDisposition.toString());
   }

   //While parsing XML Nodes and data manipulation  the Inputdata with xssHtml Data like  "&#60;" got converted to "&amp;#60;"
   //So,this method converts the xssHtml with extra "&amp;" in it, converts back to actual characters
   private static String xssHtmlToCharsForXmlDoc(String value) {
	   Hashtable htmlToChar = new Hashtable();
	   htmlToChar.put("&amp;#60;", "<");
	   htmlToChar.put("&amp;#62;", ">");
	   htmlToChar.put("&amp;#38;", "&");
	   htmlToChar.put("&amp;#34;", "\"");
	   htmlToChar.put("&amp;#39;", "'");
	   htmlToChar.put("&amp;#40;", "(");
	   htmlToChar.put("&amp;#41;", ")");
	   /*KMehta Rel 9500 IR T36000043474 on 26 Oct 2015 Add-Begin*/ 
	   htmlToChar.put("&amp;#47;", "/");
	   /*KMehta Rel 9500 IR T36000043474 on 26 Oct 2015 Add-End*/
	   for (Enumeration keys = htmlToChar.keys(); keys.hasMoreElements();) {
		   String charAsHtml = (String) keys.nextElement();
		   String charAsLiteral = (String) htmlToChar.get(charAsHtml);
		   while (value.indexOf(charAsHtml) != -1) {
			   value = value.substring(0, value.indexOf(charAsHtml))
			   + charAsLiteral
			   + value.substring(value.indexOf(charAsHtml)+charAsHtml.length());
		   }
	   }
	   return value;
   } 
}
