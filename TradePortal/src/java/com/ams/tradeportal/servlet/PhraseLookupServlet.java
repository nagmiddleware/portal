package com.ams.tradeportal.servlet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;
import java.util.List;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.ams.tradeportal.busobj.Phrase;
import com.ams.tradeportal.busobj.webbean.SessionWebBean;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.EJBObjectFactory;
import com.amsinc.ecsg.frame.ErrorManager;
import com.amsinc.ecsg.frame.IssuedError;
import com.amsinc.ecsg.frame.NavigationManager;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.EncryptDecrypt;
import com.amsinc.ecsg.util.JPylonProperties;
import com.amsinc.ecsg.util.ResourceManager;

public class PhraseLookupServlet extends HttpServlet {
	private static final Logger LOG = LoggerFactory.getLogger(PhraseLookupServlet.class);

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doTheWork(request, response);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doTheWork(request, response);
	}

	/**
	 * This method implements the standard servlet API for GET and POST requests. It handles uploading of files to the Trade Portal.
	 *
	 * @param javax
	 *            .servlet.http.HttpServletRequest request - the Http servlet request object
	 * @param javax
	 *            .servlet.http.HttpServletResponse response - the Http servlet response object
	 * @exception javax.servlet.ServletException
	 * @exception java.io.IOException
	 * @return void
	 * @throws AmsException
	 */

	public void doTheWork(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		// T36000005819 W Zhu 9/24/12 Do not allow direct access to the servlet.
		if (NavigationManager.bookmarkedAccessDenied(request, response, this)) {
			return;
		}

		String output = "<response>";

		String phraseText = null;

		// Retrieve the form manager and resource manager from the session so
		// that
		// we can issue errors or create an phrase look up if necessary
		HttpSession session = request.getSession(true);
		ResourceManager resourceManager = (ResourceManager) session.getAttribute("resMgr");
		// cquinton 3/2/2013 add error manager for user errors
		// error manager collects user errors that are to be returned
		ErrorManager errMgr = new ErrorManager();
		errMgr.setLocaleName(resourceManager.getResourceLocale());

		String oId = request.getParameter("SelectedPhrase");
		// Maxlength is as long as current phrase can be, before which it is truncated
		String maxlengthStr = request.getParameter("Maxlength"); // note this could be zero just to get an error
		// TextAreaMaxLength is the total length of the text area
		String textAreaMaxlength = request.getParameter("TextAreaMaxlength");

		try {

			// W Zhu 9/11/2012 Rel 8.1 T36000004579 add key parameter.
			javax.crypto.SecretKey secretKey = null;
			if (session != null) {
				SessionWebBean userSession = (SessionWebBean) session.getAttribute("userSession");
				secretKey = userSession.getSecretKey();
			}

			String oIdStr = EncryptDecrypt.decryptStringUsingTripleDes(oId, secretKey);
			long lOid = 0;
			if (oId != null) {
				lOid = Long.parseLong(oIdStr);
			}

			String serverLocation = JPylonProperties.getInstance().getString("serverLocation");
			Phrase phrase = (Phrase) EJBObjectFactory.createClientEJB(serverLocation, "Phrase");
			phrase.newObject();

			phrase.getData(lOid);
			String myPhraseText = phrase.getAttribute("phrase_text");

			// Get the max length attribute from the input doc. If not a number
			// default to -1 (indicating no max length).
			int maxLength = -1;
			maxLength = Integer.parseInt(maxlengthStr);

			if (myPhraseText.length() > maxLength) {
				phraseText = myPhraseText.substring(0, maxLength);
				errMgr.issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.TEXT_TOO_LONG, textAreaMaxlength);
			} else {
				phraseText = myPhraseText;
			}

		} catch (AmsException e) {
			e.printStackTrace();

		} catch (Exception e) {
			LOG.info("Exception caught in PhraseLookupServlet");
			e.printStackTrace();
		}

		if (errMgr.getErrorCount() > 0 && errMgr.getMaxErrorSeverity() >= ErrorManager.ERROR_SEVERITY) {
			output += "<responseCode>ERROR</responseCode>";
		} else {
			// success includes warnings, infos
			output += "<responseCode>SUCCESS</responseCode>";
		}
		// for now always write out the phrase text, but
		// this really should only be written on success or warning
		// however for now we want the 'too long' error to display
		// that should really be a warning so behavior can continue
		output += "<phrase>" + phraseText + "</phrase>";
		String errorXml = getErrorXml(errMgr);
		output += "<errorXml>" + errorXml + "</errorXml>";
		output += "</response>";

		response.setCharacterEncoding("UTF-8");
		response.setContentType("text/xml");
		response.setHeader("Cache-Control", "no-cache");
		PrintWriter pw = response.getWriter();

		pw.write(output);
		pw.flush();
	}

	private String getErrorXml(ErrorManager errMgr) {
		String errorXml = getErrorDoc(errMgr).toString();
		// cquinton 3/4/2013 this is not ideal, but something in the
		// xml.toString() create single quotes as &apos;
		// IE doesn't like that, we change to &#39;
		errorXml = scrubOutput(errorXml);
		return errorXml;
	}

	/**
	 * Generate an xml error document for list of errors.
	 */
	public DocumentHandler getErrorDoc(ErrorManager errMgr) {
		DocumentHandler errorDoc = new DocumentHandler();
		int errorId = 1;
		int severity = 0;
		int maxErrorSeverity = 0;

		List<IssuedError> errorList = errMgr.getIssuedErrorsList();
		for (IssuedError err : errorList) {
			severity = err.getSeverity();
			if (severity > maxErrorSeverity) {
				maxErrorSeverity = severity;
			}

			String code = err.getErrorCode();
			String msg = err.getErrorText();

			errorDoc.setAttribute("/errorlist/error(" + errorId + ")/code", code);
			errorDoc.setAttribute("/errorlist/error(" + errorId + ")/message", msg);
			errorDoc.setAttributeInt("/errorlist/error(" + errorId + ")/severity", severity);
			errorId++;
		}
		errorDoc.setAttributeInt("/maxerrorseverity", maxErrorSeverity);
		return errorDoc;
	}

	// ugly - todo: build the xml above in better fashion
	private String scrubOutput(String output) {
		if (output != null && output.length() > 0) {
			int idx = -1;
			int attempts = 0; // use this to ensure we don't have a runaway train
			while ((idx = output.indexOf("&apos;")) >= 0 && attempts < 100) {
				String before = output.substring(0, idx);
				String after = output.substring(idx + "%apos;".length());
				output = before + "&#39;" + after;
				attempts++;
			}
		}
		return output;
	}
}
