
package com.ams.tradeportal.BTMUeBizWebService;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ServiceInput complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ServiceInput">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="soakyoutsuuheader" type="{http://soa.bk.mufg.jp/data/header/}Soakyoutsuuheader"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceInput", namespace = "http://soa.bk.mufg.jp/data/header/", propOrder = {
    "soakyoutsuuheader"
})
@XmlSeeAlso({
    ExecuteInput.class
})
public class ServiceInput {

    @XmlElement(required = true, nillable = true)
    protected Soakyoutsuuheader soakyoutsuuheader;

    /**
     * Gets the value of the soakyoutsuuheader property.
     * 
     * @return
     *     possible object is
     *     {@link Soakyoutsuuheader }
     *     
     */
    public Soakyoutsuuheader getSoakyoutsuuheader() {
        return soakyoutsuuheader;
    }

    /**
     * Sets the value of the soakyoutsuuheader property.
     * 
     * @param value
     *     allowed object is
     *     {@link Soakyoutsuuheader }
     *     
     */
    public void setSoakyoutsuuheader(Soakyoutsuuheader value) {
        this.soakyoutsuuheader = value;
    }

}
