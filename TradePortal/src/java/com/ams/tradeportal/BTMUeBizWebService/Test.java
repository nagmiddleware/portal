package com.ams.tradeportal.BTMUeBizWebService;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.ws.BindingProvider;

/**
 *

 */
public class Test  {
    
    public String signedDataField = "123456";
    public String signature = "123456";
    public String serviceURL = "";
    public String customerID = "customer";
    public String userID = "user";
    public String ignoreCert = "N";
    public ExecuteInput executeInput;
    
    
    public  ExecuteOutput testBTMUeBizWebService() throws UnknownHostException, JAXBException {
        
        // Test ignore cert
        if ("Y".equals(ignoreCert)) {
            TrustManager[] trustAllCerts = new TrustManager[] { 
              new X509TrustManager() {
                public X509Certificate[] getAcceptedIssuers() { 
                  return new X509Certificate[0]; 
                }
                public void checkClientTrusted(X509Certificate[] certs, String authType) {}
                public void checkServerTrusted(X509Certificate[] certs, String authType) {}
            }};

            // Ignore differences between given hostname and certificate hostname
            HostnameVerifier hv = new HostnameVerifier() {
              public boolean verify(String hostname, SSLSession session) { return true; }
            };

            // Install the all-trusting trust manager
            try {
              SSLContext sc = SSLContext.getInstance("SSL");
              sc.init(null, trustAllCerts, new SecureRandom());
              HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
              HttpsURLConnection.setDefaultHostnameVerifier(hv);
            } catch (Exception e) {
            System.out.println (e.getMessage());
            }
    
        }
        
        ObjectFactory objFactory = new ObjectFactory();
        executeInput = objFactory.createExecuteInput(); 
        
        // /executeInput/soakyoutsuuheader
        Soakyoutsuuheader soukyousuuheader = objFactory.createSoakyoutsuuheader();
        executeInput.setSoakyoutsuuheader(soukyousuuheader);
        String serviceId = createServiceid();
        soukyousuuheader.setOriginalserviceid(serviceId);
        soukyousuuheader.setSourceserviceid(serviceId);
        soukyousuuheader.setServiceid(serviceId); 
        soukyousuuheader.setRequestTimestamp(timestamp2.get());
        soukyousuuheader.setSoushinRetryFlag(false);
        soukyousuuheader.setRequestSystemId("TPL");
        soukyousuuheader.setSoakyoutsuuheaderVersion("1.0.0");
        soukyousuuheader.setServiceVersion("1.0.0");
        
        OtpVerifySignatureRequestmessage otpVerifySignatureRequestmessage = objFactory.createOtpVerifySignatureRequestmessage();
        executeInput.setOtpVerifySignatureRequestmessage(otpVerifySignatureRequestmessage);
        
        // /executeInput/otpVerifySignatureRequestmessage/commonInformation/
        CommonInformation commonInformation = objFactory.createCommonInformation();        
        otpVerifySignatureRequestmessage.setCommonInformation(commonInformation);
        commonInformation.setCustomerId(customerID); //replace with real one in mediator
        commonInformation.setUserId(userID); // replace
        commonInformation.setSystemId("007");
        commonInformation.setEntryNo("test"); // replace 
        commonInformation.setStructureType("0"); //replace
        
        // /executeInput/otpVerifySignatureRequestmessage/deviceInformation
        DeviceInformation deviceInformation  = objFactory.createDeviceInformation();
        otpVerifySignatureRequestmessage.setDeviceInformation(deviceInformation);
        deviceInformation.getApplicationId().add("2110");
        deviceInformation.getApplicationId().add("1010");

        // /executeInput/otpVerifySignatureRequestmessage/authenticationInformation
        AuthenticationInformation authenticationInformation = objFactory.createAuthenticationInformation();
        otpVerifySignatureRequestmessage.setAuthenticationInformation(authenticationInformation);
        authenticationInformation.setSignature(signature);
        authenticationInformation.getSignedDataFields().add(signedDataField);

        JAXBContext jc = JAXBContext.newInstance(ExecuteInput.class);
        Marshaller m = jc.createMarshaller();
        m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        JAXBElement<ExecuteInput> jx = objFactory.createExecuteInput(executeInput);
        m.marshal(jx, System.out);
        
        EbzOTPVerifySignatureService service = new EbzOTPVerifySignatureService();
        EbzOTPVerifySignature port = service.getEbzOTPVerifySignaturePort();
        BindingProvider bp = (BindingProvider)port;
        bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, serviceURL);
        
        ExecuteOutput executeOutput = port.execute(executeInput);
        JAXBElement<ExecuteOutput> jxo = objFactory.createExecuteOutput(executeOutput);
        jc = JAXBContext.newInstance(ExecuteOutput.class);
        m = jc.createMarshaller();
        m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        m.marshal(jxo, System.out);
        return executeOutput;
    }
    
    private static ThreadLocal<String> timestamp = new ThreadLocal<String>() {
            protected String initialValue() {
                    return new String();
            }
    };
    private static ThreadLocal<String> timestamp2 = new ThreadLocal<String>() {
            protected String initialValue() {
                    return new String();
            }
    };
    private static ThreadLocal<Integer> counter = new ThreadLocal<Integer>() {
            protected Integer initialValue() {
                    return new Integer(0);
            }
    };

        

    private static String createServiceid() throws UnknownHostException {

            final int HOST_NAME_LENGTH = 8;
            final int APPL_SERVER_NAME_LENGTH = 8;
            final int THREADID_LENGTH = 16;
            final int SERVICE_NAME_LENGTH = 50;
            final String TIMESTAMP_FORMAT = "yyyyMMddHHmmssSSS";
            final String TIMESTAMP_FORMAT2 = "yyyy-MM-dd HH:mm:ss.SSS";
            final char CHAR_UNDER_SCORE = '_';
            final char CHAR_ZERO = '0';
            InetAddress localHost = InetAddress.getLocalHost();
            String localHostName = localHost.getHostName();

            if (localHostName.length() >= HOST_NAME_LENGTH) {
                    localHostName = localHostName.substring(0, HOST_NAME_LENGTH);
            } else {
                    localHostName = rightPad(localHostName, HOST_NAME_LENGTH, CHAR_UNDER_SCORE);
            }

            // app Server Name
            String appServerName  = "testserver";

            if (appServerName.length() >= APPL_SERVER_NAME_LENGTH) {
                    appServerName = appServerName.substring(0,APPL_SERVER_NAME_LENGTH);
            } else if (appServerName.length() < APPL_SERVER_NAME_LENGTH) {
                    appServerName = rightPad(appServerName, 8, CHAR_UNDER_SCORE);
            }


            // ThreadID
            Thread current = Thread.currentThread();
            String threadId = Long.toHexString(current.getId());

            if (threadId.length() < THREADID_LENGTH) {
                    threadId = leftPad(threadId, THREADID_LENGTH, CHAR_ZERO);
            }

            // servicename
            String serviceName = "EbzOTPVerifySignaturePort";
            if (serviceName.length() >= SERVICE_NAME_LENGTH) {
                    serviceName = serviceName.substring(0, SERVICE_NAME_LENGTH);
            } else {
                    serviceName = rightPad(serviceName, SERVICE_NAME_LENGTH, CHAR_UNDER_SCORE);
            }

            // DateTime and Counter
            Date currentDate = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat(TIMESTAMP_FORMAT);
            String currentTimestamp = sdf.format(currentDate);

            int currentCounter;
            if (currentTimestamp.equals(timestamp.get())) {
                    currentCounter = counter.get().intValue() + 1;
                    if (currentCounter == 10) {
                            while (currentTimestamp.equals(timestamp.get())) {
                                    currentCounter = 0;
                                    currentDate = new Date();
                                    currentTimestamp = sdf.format(currentDate);
                            }
                    }
            } else {
                    currentCounter = 0;
            }

            timestamp.set(currentTimestamp);
            counter.set(new Integer(currentCounter));

            // tempstamp2 has the value to be used in requestTimestamp field
            SimpleDateFormat sdf2 = new SimpleDateFormat(TIMESTAMP_FORMAT2);
            String currentTimestamp2 = sdf2.format(currentDate);
            timestamp2.set(currentTimestamp2);
            
            StringBuilder serviceID = new StringBuilder();
            serviceID.append(localHostName).append(appServerName).append(threadId)
                     .append(serviceName).append(currentTimestamp).append(currentCounter);

            return serviceID.toString();

	}

	private static String leftPad(String str, int size, char padchar) {

		if (str == null || size <= str.length()) {
			return str;
		}

		int padsize = size - str.length();
		StringBuffer sb = new StringBuffer();

		for (int i = 0; i < padsize; i++) {
			sb.append(padchar);
		}

		sb.append(str);
		return sb.toString();
	}

	/**
	 * </DD>
	 * </DL>
	 * 
	 * @param String
	 * @param int size 
	 * @param char padchar 
	 * 
	 * @return String 
	 */
	private static String rightPad(String str, int size, char padchar) {

		if (str == null || size <= str.length()) {
			return str;
		}

		int padsize = size - str.length();
		StringBuffer sb = new StringBuffer();
		sb.append(str);

		for (int i = 0; i < padsize; i++) {
			sb.append(padchar);
		}

		return sb.toString();
	}

}
