
package com.ams.tradeportal.BTMUeBizWebService;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for otpVerifySignatureResponsemessage complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="otpVerifySignatureResponsemessage">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="commonInformation" type="{http://soa.bk.mufg.jp/ebz/common}commonInformation" minOccurs="0"/>
 *         &lt;element name="deviceInformation" type="{http://soa.bk.mufg.jp/ebz/otpverifysignature}deviceInformation" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "otpVerifySignatureResponsemessage", propOrder = {
    "commonInformation",
    "deviceInformation"
})
public class OtpVerifySignatureResponsemessage {

    protected CommonInformation commonInformation;
    protected DeviceInformation deviceInformation;

    /**
     * Gets the value of the commonInformation property.
     * 
     * @return
     *     possible object is
     *     {@link CommonInformation }
     *     
     */
    public CommonInformation getCommonInformation() {
        return commonInformation;
    }

    /**
     * Sets the value of the commonInformation property.
     * 
     * @param value
     *     allowed object is
     *     {@link CommonInformation }
     *     
     */
    public void setCommonInformation(CommonInformation value) {
        this.commonInformation = value;
    }

    /**
     * Gets the value of the deviceInformation property.
     * 
     * @return
     *     possible object is
     *     {@link DeviceInformation }
     *     
     */
    public DeviceInformation getDeviceInformation() {
        return deviceInformation;
    }

    /**
     * Sets the value of the deviceInformation property.
     * 
     * @param value
     *     allowed object is
     *     {@link DeviceInformation }
     *     
     */
    public void setDeviceInformation(DeviceInformation value) {
        this.deviceInformation = value;
    }

}
