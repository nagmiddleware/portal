
package com.ams.tradeportal.BTMUeBizWebService;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Soakyoutsuuheader complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Soakyoutsuuheader">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="originalserviceid" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="sourceserviceid" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="serviceid" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="requestTimestamp" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="responseTimestamp" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="serviceshorikekka" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="responsemessagelostumu" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="retrykahi" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="soushinRetryFlag" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="requestUserId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="requestSystemId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="soakyoutsuuheaderVersion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="serviceVersion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Soakyoutsuuheader", namespace = "http://soa.bk.mufg.jp/data/header/", propOrder = {
    "originalserviceid",
    "sourceserviceid",
    "serviceid",
    "requestTimestamp",
    "responseTimestamp",
    "serviceshorikekka",
    "responsemessagelostumu",
    "retrykahi",
    "soushinRetryFlag",
    "requestUserId",
    "requestSystemId",
    "soakyoutsuuheaderVersion",
    "serviceVersion"
})
public class Soakyoutsuuheader {

    @XmlElement(required = true, nillable = true)
    protected String originalserviceid;
    @XmlElement(required = true, nillable = true)
    protected String sourceserviceid;
    @XmlElement(required = true, nillable = true)
    protected String serviceid;
    @XmlElement(required = true, nillable = true)
    protected String requestTimestamp;
    @XmlElement(required = true, nillable = true)
    protected String responseTimestamp;
    @XmlElement(required = true, nillable = true)
    protected String serviceshorikekka;
    protected boolean responsemessagelostumu;
    protected boolean retrykahi;
    protected boolean soushinRetryFlag;
    @XmlElement(required = true, nillable = true)
    protected String requestUserId;
    @XmlElement(required = true, nillable = true)
    protected String requestSystemId;
    @XmlElement(required = true, nillable = true)
    protected String soakyoutsuuheaderVersion;
    @XmlElement(required = true, nillable = true)
    protected String serviceVersion;

    /**
     * Gets the value of the originalserviceid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOriginalserviceid() {
        return originalserviceid;
    }

    /**
     * Sets the value of the originalserviceid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOriginalserviceid(String value) {
        this.originalserviceid = value;
    }

    /**
     * Gets the value of the sourceserviceid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSourceserviceid() {
        return sourceserviceid;
    }

    /**
     * Sets the value of the sourceserviceid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSourceserviceid(String value) {
        this.sourceserviceid = value;
    }

    /**
     * Gets the value of the serviceid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceid() {
        return serviceid;
    }

    /**
     * Sets the value of the serviceid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceid(String value) {
        this.serviceid = value;
    }

    /**
     * Gets the value of the requestTimestamp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRequestTimestamp() {
        return requestTimestamp;
    }

    /**
     * Sets the value of the requestTimestamp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRequestTimestamp(String value) {
        this.requestTimestamp = value;
    }

    /**
     * Gets the value of the responseTimestamp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResponseTimestamp() {
        return responseTimestamp;
    }

    /**
     * Sets the value of the responseTimestamp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResponseTimestamp(String value) {
        this.responseTimestamp = value;
    }

    /**
     * Gets the value of the serviceshorikekka property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceshorikekka() {
        return serviceshorikekka;
    }

    /**
     * Sets the value of the serviceshorikekka property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceshorikekka(String value) {
        this.serviceshorikekka = value;
    }

    /**
     * Gets the value of the responsemessagelostumu property.
     * 
     */
    public boolean isResponsemessagelostumu() {
        return responsemessagelostumu;
    }

    /**
     * Sets the value of the responsemessagelostumu property.
     * 
     */
    public void setResponsemessagelostumu(boolean value) {
        this.responsemessagelostumu = value;
    }

    /**
     * Gets the value of the retrykahi property.
     * 
     */
    public boolean isRetrykahi() {
        return retrykahi;
    }

    /**
     * Sets the value of the retrykahi property.
     * 
     */
    public void setRetrykahi(boolean value) {
        this.retrykahi = value;
    }

    /**
     * Gets the value of the soushinRetryFlag property.
     * 
     */
    public boolean isSoushinRetryFlag() {
        return soushinRetryFlag;
    }

    /**
     * Sets the value of the soushinRetryFlag property.
     * 
     */
    public void setSoushinRetryFlag(boolean value) {
        this.soushinRetryFlag = value;
    }

    /**
     * Gets the value of the requestUserId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRequestUserId() {
        return requestUserId;
    }

    /**
     * Sets the value of the requestUserId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRequestUserId(String value) {
        this.requestUserId = value;
    }

    /**
     * Gets the value of the requestSystemId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRequestSystemId() {
        return requestSystemId;
    }

    /**
     * Sets the value of the requestSystemId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRequestSystemId(String value) {
        this.requestSystemId = value;
    }

    /**
     * Gets the value of the soakyoutsuuheaderVersion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSoakyoutsuuheaderVersion() {
        return soakyoutsuuheaderVersion;
    }

    /**
     * Sets the value of the soakyoutsuuheaderVersion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSoakyoutsuuheaderVersion(String value) {
        this.soakyoutsuuheaderVersion = value;
    }

    /**
     * Gets the value of the serviceVersion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceVersion() {
        return serviceVersion;
    }

    /**
     * Sets the value of the serviceVersion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceVersion(String value) {
        this.serviceVersion = value;
    }

}
