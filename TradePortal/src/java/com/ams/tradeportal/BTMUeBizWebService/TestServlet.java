package com.ams.tradeportal.BTMUeBizWebService;
import com.ams.tradeportal.common.ConfigSettingManager;
import com.amsinc.ecsg.frame.AmsException;
import java.io.IOException;
import java.io.PrintStream;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Properties;
import java.util.PropertyResourceBundle;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.ws.BindingProvider;

/**
 *

 */
public class TestServlet extends HttpServlet {

    
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException{
        Test test = new Test();
        try {
            String signedDataField = request.getParameter("signed_data_field");
            if (signedDataField != null && !"".equals(signedDataField )) {
                test.signedDataField = signedDataField;
            }
            String signature = request.getParameter("signature");            
            if (signature != null && !"".equals(signature)) {
                test.signature = signature;
            }
            String serviceURL = request.getParameter("service_url");
            if (serviceURL != null && !"".equals(serviceURL )) {
                test.serviceURL = serviceURL;
            }
            else {
                test.serviceURL = ConfigSettingManager.getConfigSetting("PORTAL_APP", "PORTAL", "Vasco_BTMUeBiz_EndpointAddress", "");
            }
            String customerID = request.getParameter("customer_id");            
            if (customerID != null && !"".equals(customerID)) {
                test.customerID = customerID;
            }
            String userID = request.getParameter("user_id");            
            if (userID != null && !"".equals(userID)) {
                test.userID = userID;
            }
            String ignoreCert = request.getParameter("ignore_cert");            
            if (ignoreCert != null && !"".equals(ignoreCert)) {
                test.ignoreCert = userID;
            }
            
            ExecuteOutput executeOutput = test.testBTMUeBizWebService();
            response.setContentType("text/plain");
            ServletOutputStream sos = response.getOutputStream();
            
            ObjectFactory objFactory = new ObjectFactory();
            
            JAXBElement<ExecuteInput> jxo1 = objFactory.createExecuteInput(test.executeInput);
            JAXBContext jc1 = JAXBContext.newInstance(ExecuteInput.class);
            Marshaller m1 = jc1.createMarshaller();
            m1.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            m1.marshal(jxo1, sos);
            
            JAXBContext jc = JAXBContext.newInstance(ExecuteOutput.class);
            Marshaller m = jc.createMarshaller();
            m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            JAXBElement<ExecuteOutput> jxo = objFactory.createExecuteOutput(executeOutput);
            m.marshal(jxo, sos);
            sos.println("<Vasco_BTMUeBiz_EndpointAddress>" + test.serviceURL + "</Vasco_BTMUeBiz_EndpointAddress>");

            sos.flush();
            sos.close();
        }
        catch (Exception e) {
            response.setContentType("text/plain");
            ServletOutputStream sos = response.getOutputStream();
            sos.println("<Vasco_BTMUeBiz_EndpointAddress>" + test.serviceURL + "</Vasco_BTMUeBiz_EndpointAddress>");
            e.printStackTrace(new PrintStream(sos));
            sos.flush();
            sos.close();
        }
    }
    

}
