package com.ams.tradeportal.mediator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class is used for adding one or more Structured PO to an Import LC
 * Issue or Amend transaction. If POs  are being assigned to a
 * transaction that previously had no POs , the class will ensure
 * that all of the selected POs  have the same PO upload definition
 * oid, seller name, and currency. If all selected POs  are
 * valid, the transaction's amount, last shipment date, expiry date are updated accordingly.
 *
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */

import java.math.BigDecimal;
import java.math.MathContext;
import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Vector;
import java.util.ArrayList;

import com.ams.tradeportal.busobj.ArMatchingRule;
import com.ams.tradeportal.busobj.CorporateOrganization;
import com.ams.tradeportal.busobj.Instrument;
import com.ams.tradeportal.busobj.InvoicesSummaryData;
import com.ams.tradeportal.busobj.ShipmentTerms;
import com.ams.tradeportal.busobj.Terms;
import com.ams.tradeportal.busobj.TermsParty;
import com.ams.tradeportal.busobj.Transaction;
import com.ams.tradeportal.busobj.util.InvoiceUtility;
import com.ams.tradeportal.busobj.util.POLineItemUtility;
import com.ams.tradeportal.common.TPDateTimeUtility;
import com.ams.tradeportal.common.TermsPartyType;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.ComponentList;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.frame.MediatorBean;
import com.amsinc.ecsg.frame.MediatorServices;
import com.amsinc.ecsg.frame.ReferenceDataManager;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.StringFunction;

public class AddTransInvoicesMediatorBean extends MediatorBean {
	private static final Logger LOG = LoggerFactory.getLogger(AddTransInvoicesMediatorBean.class);
	// The following variables have been declared transient because the mediator
	// is stateless and they are initialized in only one place.
	private transient MediatorServices mediatorServices = null;
	private transient Transaction transaction = null;
	private transient Instrument instrument = null;
	private transient boolean hasstrucPOs = false;
	private transient String uploadDefinitionOid = null;
	private transient String beneficiaryName = null;
	private transient String currency = null;
	private transient Vector invoiceOidsList = null;
	private transient String invoiceOidsSql = null;
	private transient Terms terms = null;
	private transient int numberOfInvoices = 0;
	private transient long shipmentTermsOid = 0;

	private transient String userLocale = null;
	private transient String userOID = null;

	/**
	 * This method performs addition of POs The format expected from the input doc is:
	 *
	 * <POLineItemList> <POLineItem ID="0"><poLineItemOid>1000001</POLineItem>
	 * <POLineItem ID="1"><poLineItemOid>1000002</POLineItem> <POLineItem
	 * ID="2"><poLineItemOid>1000003</POLineItem> <POLineItem
	 * ID="3"><poLineItemOid>1000004</POLineItem> <POLineItem
	 * ID="6"><poLineItemOid>1000001</POLineItem> </POLineItemList>
	 *
	 * This is a non-transactional mediator that executes transactional methods in business objects
	 *
	 * @param inputDoc
	 *            The input XML document (&lt;In&gt; section of overall Mediator XML document)
	 * @param outputDoc
	 *            The output XML document (&lt;Out&gt; section of overall Mediator XML document)
	 * @param newMediatorServices
	 *            Associated class that contains lots of the auxillary functionality for a mediator.
	 */
	public DocumentHandler execute(DocumentHandler inputDoc, DocumentHandler outputDoc, MediatorServices newMediatorServices)
			throws RemoteException, AmsException {

		mediatorServices = newMediatorServices;
		Vector invoicesList = inputDoc.getFragments("/InvoicesList/Invoice");
		long transactionOid = inputDoc.getAttributeLong("/Transaction/transactionOid");
		hasstrucPOs = Boolean.valueOf(inputDoc.getAttribute("../hasStructuredPO")).booleanValue();
		shipmentTermsOid = inputDoc.getAttributeLong("/Terms/ShipmentTermsList/shipment_oid");
		numberOfInvoices = invoicesList.size();
		userLocale = mediatorServices.getCSDB().getLocaleName();
		userOID = inputDoc.getAttribute("/User/userOid");
		// if the user locale not specified default to en_US
		if ((userLocale == null) || (userLocale.equals(""))) {
			userLocale = "en_US";
		}
		String buttonPressed = inputDoc.getAttribute("/Update/ButtonPressed");

		if (!TradePortalConstants.BUTTON_ASSIGN_TRANS_INVOICES.equals(buttonPressed)) {
			return outputDoc;
		}
		// If nothing was selected by the user, issue an error indicating this and return
		if (numberOfInvoices == 0) {
			String substitutionParm = mediatorServices.getResourceManager().getText("StructuredPOAction.Add", TradePortalConstants.TEXT_BUNDLE);

			mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.NO_ITEM_SELECTED, substitutionParm);
			addInvoices(transactionOid, outputDoc);
			return outputDoc;
		}

		// Get a list of all the PO oids that were selected
		String multipleInvoices = TradePortalConstants.INDICATOR_NO;
		invoiceOidsList = InvoiceUtility.getInvoiceOids(invoicesList, multipleInvoices, TradePortalConstants.PO_STRUCT_ADD);

		// Build the SQL to use in numerous queries throughout the mediator
		String requiredOid = "upload_invoice_oid";// SHR CR708 Rel8.1
		invoiceOidsSql = POLineItemUtility.getPOLineItemOidsSql(invoiceOidsList, requiredOid);

		// If POs don't currently exist for the transaction, make sure that all  of
		// the POs selected by the user have the same PO upload definition,
		// beneficiary, and currency; if they don't, issue an error indicating
		// this. On the other hand, if the transaction *does* currently have POs assigned to it,
		// we don't need to validate anything since it has already been done in
		// the listview (i.e., the listview ensures that these fields match the corresponding
		// fields for the transaction).
		if (invoiceOidsList != null && invoiceOidsList.size() > 0) {
			if (!validateSameCurrency(invoiceOidsSql, transactionOid)) {
				mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.DIFFERENT_INVOICE_CURRENCY);
			} else {
				long invoiceOid = Long.valueOf((String) invoiceOidsList.elementAt(0)).longValue();
				InvoicesSummaryData invoiceSumData = (InvoicesSummaryData) mediatorServices.createServerEJB("InvoicesSummaryData", invoiceOid);

				uploadDefinitionOid = invoiceSumData.getAttribute("upload_definition_oid");

				beneficiaryName = invoiceSumData.getAttribute("seller_name") != null ? invoiceSumData.getAttribute("seller_name") : invoiceSumData.getAttribute("buyer_name");
				currency = invoiceSumData.getAttribute("currency");

				try {
					transaction = (Transaction) mediatorServices.createServerEJB("Transaction", transactionOid);

					// Needs to instantiate instrument object for updating amendment transaction
					long instrumentOid = transaction.getAttributeLong("instrument_oid");
					instrument = (Instrument) mediatorServices.createServerEJB("Instrument", instrumentOid);

					addInvoicesList(invoiceOidsList, outputDoc, inputDoc);
				} catch (Exception ex) {
					LOG.error("Exception occured in AddStructuredPOMediator: " , ex);
				}
			}
		}
		// If the transaction previously did not have POs assigned to it, set
		// the PO upload definition oid, beneficiary, and currency in the xml
		// doc so that
		// the jsp loads up the correct data in the listview (otherwise, the
		// user could
		// potentially add PO that don't have matching PO upload definitions,
		// beneficiaries, and currencies).

		if (hasstrucPOs) {
			outputDoc.setAttribute("/Transaction/uploadDefinitionOid", uploadDefinitionOid);
			outputDoc.setAttribute("../beneficiaryName", beneficiaryName);
			outputDoc.setAttribute("../currency", currency);
		}

		// set default text to goods description if it is blank
		if (StringFunction.isBlank(inputDoc.getAttribute("/Terms/ShipmentTermsList/goods_description"))) {
			outputDoc.setAttribute("/ChangePOs/goods_description", TradePortalConstants.getPropertyValue("TextResources", "StructuredPurchaseOrders.defaultGoodsDescription", null));
		} else {
			outputDoc.setAttribute("/ChangePOs/goods_description", inputDoc.getAttribute("/Terms/ShipmentTermsList/goods_description"));
		}

		outputDoc.setAttribute("/ChangePOs/isStructuredPO", TradePortalConstants.INDICATOR_YES);
		return outputDoc;
	}

	private boolean validateSameCurrency(String invoiceOidsSql, long transactionOid) throws AmsException {
		String sameCurrencySQL = "select count(distinct(currency)) currencyCount from invoices_summary_data where " + invoiceOidsSql
				+ " or (a_transaction_oid in (?)) ";

		DocumentHandler resultDoc = DatabaseQueryBean.getXmlResultSet(sameCurrencySQL, false, new Object[] { transactionOid });
		int currCount = resultDoc.getAttributeInt("/ResultSetRecord(0)/CURRENCYCOUNT");

		return (currCount == 1);
	}

	private boolean validateSamePaymentMethod(String invoiceOidsSql, String transactionOid) throws AmsException {
		String sameCurrencySQL = "select count(distinct(pay_method)) payMethodCount from invoices_summary_data where "
				+ invoiceOidsSql + " or (a_transaction_oid in (?)) ";

		DocumentHandler resultDoc = DatabaseQueryBean.getXmlResultSet(sameCurrencySQL, false, new Object[] { transactionOid });
		int currCount = resultDoc.getAttributeInt("/ResultSetRecord(0)/PAYMETHODCOUNT");

		return (currCount <= 1);
	}

	private boolean validateSameTradingPartner(String invoiceOidsSql, boolean useSeller, String transactionOid) throws AmsException {
		String name = "buyer_name";
		if (useSeller) {
			name = "seller_name";
		}
		String sameTradingPartnerSQL = "select count(distinct(" + name + ")) tradingpartnerCount from invoices_summary_data where "
				+ invoiceOidsSql + " or (a_transaction_oid in (?)) ";
		DocumentHandler resultDoc = DatabaseQueryBean.getXmlResultSet(sameTradingPartnerSQL, false, new Object[] { transactionOid });
		int currCount = resultDoc.getAttributeInt("/ResultSetRecord(0)/TRADINGPARTNERCOUNT");

		return (currCount == 1);
	}

	private boolean validateSameDate(String invoiceOidsSql, String transactionOid) throws AmsException {
		String sameDateSQL = "select  count(distinct(case when payment_date is null then due_date else payment_date end)) paymentDateCount from invoices_summary_data where "
				+ invoiceOidsSql + " or (a_transaction_oid in (?)) ";
		DocumentHandler resultDoc = DatabaseQueryBean.getXmlResultSet(sameDateSQL, false, new Object[] { transactionOid });
		int payDateCount = resultDoc.getAttributeInt("/ResultSetRecord(0)/PAYMENTDATECOUNT");
		boolean isSameDate = false;
		if (payDateCount == 1) {
			isSameDate = true;
		}
		return (isSameDate);
	}

	/**
	 * This method keeps the previously added POs by the user to the transaction he/she is currently viewing when error comes up if
	 * user has not selected any PO and still hit the add selected items button.
	 *
	 * @param long
	 *            transactionOid
	 * @param DocumentHandler
	 *            - the mediator's output doc
	 * @exception com.amsinc.ecsg.frame.AmsException
	 * @exception java.rmi.RemoteException
	 * @return void
	 */
	private void addInvoices(long transactionOid, DocumentHandler outputDoc) throws AmsException, RemoteException {
		transaction = (Transaction) mediatorServices.createServerEJB("Transaction", transactionOid);

		// Get the terms associated with the current transaction and its
		// shipment terms
		terms = (Terms) transaction.getComponentHandle("CustomerEnteredTerms");
		ComponentList shipmentTermsList = (ComponentList) terms.getComponentHandle("ShipmentTermsList");

		ShipmentTerms shipmentTerms = (ShipmentTerms) shipmentTermsList.getComponentObject(shipmentTermsOid);

		// Get data from Terms and set it in the Out section of the XML
		// That way. when the user is returned to the page of transaction, the
		// updated data will
		// appear along with other data that they entered
		outputDoc.setAttribute("/ChangePOs/amount_currency_code", terms.getAttribute("amount_currency_code"));

		// Convert date formats so that the Out section of the XML is properly
		// updated
		SimpleDateFormat iso = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
		SimpleDateFormat jPylon = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");

		try {
			if (!StringFunction.isBlank(shipmentTerms.getAttribute("latest_shipment_date"))) {
				Date date = jPylon.parse(shipmentTerms.getAttribute("latest_shipment_date"));
				outputDoc.setAttribute("/ChangePOs/latest_shipment_date", iso.format(date));
			}

			if (!StringFunction.isBlank(terms.getAttribute("expiry_date"))) {
				Date date = jPylon.parse(terms.getAttribute("expiry_date"));
				outputDoc.setAttribute("/ChangePOs/expiry_date", iso.format(date));
			}
		} catch (Exception ex) {
			LOG.error("Exception occured in addInvoices: " , ex);
		}
	}

	/**
	 * This method adds the POs selected by the user to the transaction that he/she is currently viewing. After all PO have been
	 * saved, the amount, date fields are all re-derived to reflect the new item values.
	 * 
	 * @param inputDoc
	 *
	 * @param java
	 *            .util.Vector strucPOOidsList - the list of PO line item oids
	 * @param DocumentHandler
	 *            - the mediator's output doc
	 * @exception com.amsinc.ecsg.frame.AmsException
	 * @exception java.rmi.RemoteException
	 * @return void
	 */
	private void addInvoicesList(Vector strucPOOidsList, DocumentHandler outputDoc, DocumentHandler inputDoc)
			throws AmsException, RemoteException {
		Vector invoiceList = new Vector(10);
		String instrument_type_code = inputDoc.getAttribute("/Instrument/instrument_type_code");
		String financeType = inputDoc.getAttribute("/Terms/finance_type");
		String transactionType = transaction.getAttribute("transaction_type_code");
		String transactionOid = transaction.getAttribute("transaction_oid");
		String instrumentOid = transaction.getAttribute("instrument_oid");
		String termsOid = transaction.getAttribute("c_CustomerEnteredTerms");
		String corpOrgOid = instrument.getAttribute("corp_org_oid");
		String lnType = null;
		String buyerBackedFin = inputDoc.getAttribute("/Terms/financing_backed_by_buyer_ind");
		String importIndicator = inputDoc.getAttribute("/Instrument/import_indicator");
		if (TradePortalConstants.TRADE_LOAN_REC.equals(financeType) || TradePortalConstants.TRADE_LOAN_PAY.equals(financeType)) {
			lnType = TradePortalConstants.TRADE_LOAN;
		} else if (TradePortalConstants.BUYER_REQUESTED_FINANCING.equals(financeType)) {
			if (TradePortalConstants.INDICATOR_YES.equals(buyerBackedFin)) {
				lnType = TradePortalConstants.INV_FIN_BB;
			} else {
				lnType = TradePortalConstants.INV_FIN_BR;
			}
		} else if (TradePortalConstants.INDICATOR_NO.equals(importIndicator)) {
			lnType = TradePortalConstants.EXPORT_FIN;
		} else if (TradePortalConstants.BUYER_REQUESTED_FINANCING.equals(financeType)) {
			lnType = TradePortalConstants.INV_FIN_REC;
		}
		if (TradePortalConstants.INDICATOR_NO.equals(importIndicator) || TradePortalConstants.INDICATOR_X.equals(importIndicator)) {
			if (!validateSameDate(invoiceOidsSql, transactionOid)) {
				mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.DIFFERENT_INVOICE_PAYMENTORDUEDATES);
				return;
			}
			boolean useSeller = TradePortalConstants.BUYER_REQUESTED_FINANCING.equals(financeType);
			if (!validateSameTradingPartner(invoiceOidsSql, useSeller, transactionOid)) {
				mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.DIFFERENT_INVOICE_TRADINGPARTNERS);
				return;
			}
		}
		if (TradePortalConstants.INDICATOR_T.equals(importIndicator) && TradePortalConstants.TRADE_LOAN_PAY.equals(financeType)) {
			if (!validateSamePaymentMethod(invoiceOidsSql, transactionOid)) {
				mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.DIFFERENT_INVOICE_PAYMENTMETHOD);
				return;
			}

		}

		String invoiceDueDate = null;
		String invClassification = null;
		ArMatchingRule matchingRule = null;
		String tpName = "";
		// Update the assigned to transaction indicators for each PO line item being added to the transaction
		InvoicesSummaryData invoiceSummData = null;
		long invoiceOid = 0;
		String invType = null;
		for (int i = 0; i < numberOfInvoices; i++) {
			invoiceOid = Long.parseLong((String) strucPOOidsList.elementAt(i));
			invoiceSummData = (InvoicesSummaryData) mediatorServices.createServerEJB("InvoicesSummaryData", invoiceOid);
			if (i == 0) {
				invClassification = invoiceSummData.getAttribute("invoice_classification");
				if (TradePortalConstants.INDICATOR_NO.equals(importIndicator) || TradePortalConstants.INDICATOR_X.equals(importIndicator)) {
					invoiceDueDate = invoiceSummData.getAttribute("due_date");

				}
			}
			String transOID = invoiceSummData.getAttribute("transaction_oid");
			String instruOID = invoiceSummData.getAttribute("instrument_oid");
			invType = invoiceSummData.getAttribute("invoice_type");
			String instrType = invoiceSummData.getAttribute("linked_to_instrument_type");
			String finType = invoiceSummData.getAttribute("loan_type");

			matchingRule = invoiceSummData.getMatchingRule();
			tpName = invoiceSummData.getTpRuleName();

			invoiceSummData.setAttribute("tp_rule_name", tpName);

			// if in case PO is already associated then issue error.
			if (StringFunction.isNotBlank(instruOID) || StringFunction.isNotBlank(transOID)) {
				mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.PO_ALREADY_ASSOCIATED, invoiceSummData.getAttribute("invoice_id"));
				continue;
			}
			if (StringFunction.isBlank(instrType)) {
				invoiceSummData.setAttribute("linked_to_instrument_type", instrument_type_code);
			}
			if (StringFunction.isBlank(finType)) {
				invoiceSummData.setAttribute("loan_type", lnType);
			}

			invoiceSummData.setAttribute("transaction_oid", transactionOid);
			invoiceSummData.setAttribute("instrument_oid", instrumentOid);
			invoiceSummData.setAttribute("terms_oid", termsOid);
			invoiceSummData.setAttribute("user_oid", userOID);
			invoiceSummData.setAttribute("action", TradePortalConstants.PO_ACTION_LINK_TO_INSTR);
			invoiceSummData.setAttribute("status", TradePortalConstants.PO_STATUS_ASSIGNED);
			invoiceSummData.setAttribute("invoice_status", TradePortalConstants.PO_STATUS_ASSIGNED);
			invoiceList.addElement(invoiceSummData.getAttribute("invoice_id"));
			// IR T36000031466 RPasupulati adding fininace Amount to Invoice summary data table Start
			CorporateOrganization corpOrg = (CorporateOrganization) mediatorServices.createServerEJB("CorporateOrganization");
			corpOrg.getData(Long.parseLong(corpOrgOid));
			String value = corpOrg.getAttribute("inv_percent_to_fin_trade_loan");
			BigDecimal percentFinanceAllowed = StringFunction.isBlank(value) ? BigDecimal.ZERO
					: new BigDecimal(value).divide(new BigDecimal("100.0"), MathContext.DECIMAL64);

			value = invoiceSummData.getAttribute("amount");
			BigDecimal invoiceAmount = StringFunction.isBlank(value) ? BigDecimal.ZERO
					: new BigDecimal(value, MathContext.DECIMAL64);

			BigDecimal financeAmount = invoiceAmount.multiply(percentFinanceAllowed);
			invoiceSummData.setAttribute("invoice_finance_amount", financeAmount.toString());
			// IR T36000031466 RPasupulati adding fininace Amount to Invoice summary data table Ends

			invoiceSummData.save();

			invoiceSummData = null;
		}

		// Get the terms associated with the current transaction and its
		// shipment terms
		terms = (Terms) transaction.getComponentHandle("CustomerEnteredTerms");
		ComponentList shipmentTermsList = (ComponentList) terms.getComponentHandle("ShipmentTermsList");

		ShipmentTerms shipmentTerms = (ShipmentTerms) shipmentTermsList.getComponentObject(shipmentTermsOid);

		// Derive the amounts (Transaction.amount etc), dates
		// (Terms.expirty_date,
		// ShipmentTerms.latest_shipment_date etc).
		// Note all the PO have been saved into database by now.

		String sb = " select invoice_id,upload_invoice_oid from invoices_summary_data where a_transaction_oid = ?";

		DocumentHandler resultSet = DatabaseQueryBean.getXmlResultSet(sb, false,
				new Object[] { transaction.getAttribute("transaction_oid") });
		Vector alreadyAsstInvoices = new Vector();
		alreadyAsstInvoices.addAll(strucPOOidsList);
		List<DocumentHandler> rowList = resultSet.getFragmentsList("/ResultSetRecord");
		for (DocumentHandler row : rowList) {
			String invoiceNum = row.getAttribute("/INVOICE_ID");
			invoiceList.addElement(invoiceNum);
			alreadyAsstInvoices.add(row.getAttribute("/UPLOAD_INVOICE_OID"));
		}

		InvoiceUtility.deriveTransactionCurrencyAndAmountsFromInvoice(transaction, terms, invoiceOidsList, instrument, invoiceList, invType);
		if (TradePortalConstants.TRADE_LOAN_REC.equals(financeType)) {
			CorporateOrganization corpOrg = (CorporateOrganization) mediatorServices.createServerEJB("CorporateOrganization");
			corpOrg.getData(Long.parseLong(corpOrgOid));
			String attributeValue = corpOrg.getAttribute("inv_percent_to_fin_trade_loan");

			BigDecimal finPCTValue = BigDecimal.ZERO;
			if ((attributeValue != null) && (!attributeValue.equals(""))) {
				finPCTValue = (new BigDecimal(attributeValue)).divide(new BigDecimal(100.00));
			}
			if (finPCTValue.compareTo(BigDecimal.ZERO) != 0) {
				BigDecimal termAmount = terms.getAttributeDecimal("amount");
				BigDecimal tempAmount = finPCTValue.multiply(termAmount);
				String trnxCurrencyCode = transaction.getAttribute("copy_of_currency_code");
				int numberOfCurrencyPlaces = 2;

				if (trnxCurrencyCode != null && !trnxCurrencyCode.equals("")) {
					numberOfCurrencyPlaces = Integer.parseInt(ReferenceDataManager.getRefDataMgr().getAdditionalValue(TradePortalConstants.CURRENCY_CODE, trnxCurrencyCode));
				}
				tempAmount = tempAmount.setScale(numberOfCurrencyPlaces, BigDecimal.ROUND_HALF_UP);
				terms.setAttribute("amount", String.valueOf(tempAmount));
				// Set the counterparty for the instrument and some "copy of" fields
				transaction.setAttribute("copy_of_amount", terms.getAttribute("amount"));
				transaction.setAttribute("instrument_amount", terms.getAttribute("amount"));
				instrument.setAttribute("copy_of_instrument_amount", transaction.getAttribute("instrument_amount"));

			}
		}
		String requiredOid = "upload_invoice_oid";
		String invoiceOidsSql = POLineItemUtility.getPOLineItemOidsSql(alreadyAsstInvoices, requiredOid);

		if (TradePortalConstants.INDICATOR_T.equals(importIndicator) && TradePortalConstants.TRADE_LOAN_PAY.equals(financeType)
				&& TradePortalConstants.INVOICE_TYPE_PAY_MGMT.equals(invClassification)) {
			String payMethodSQL = "select distinct(PAY_METHOD) PAYMETHOD from invoices_summary_data where " + invoiceOidsSql;
			DocumentHandler resultDoc = DatabaseQueryBean.getXmlResultSet(payMethodSQL, false, new ArrayList<Object>());
			if (resultDoc != null) {
				String payMethod = resultDoc.getAttribute("/ResultSetRecord(0)/PAYMETHOD");
				if (StringFunction.isNotBlank(payMethod)) {
					terms.setAttribute("payment_method", payMethod);
					int singleBeneCount = InvoiceUtility.isSingleBeneficairyInvoice(invoiceOidsSql);
					if (singleBeneCount == 1) {
						terms.setAttribute("loan_proceeds_credit_type", TradePortalConstants.CREDIT_BEN_ACCT);
						InvoiceUtility.populateTerms(terms, (String) strucPOOidsList.get(0), strucPOOidsList.size(), mediatorServices);
					} else if (singleBeneCount > 1) {
						terms.setAttribute("loan_proceeds_credit_type", TradePortalConstants.CREDIT_MULTI_BEN_ACCT);
						// IR 29777 start
						if (StringFunction.isBlank(terms.getAttribute("c_FirstTermsParty"))) {
							terms.newComponent("FirstTermsParty");
						}
						TermsParty firstTermsParty = (TermsParty) terms.getComponentHandle("FirstTermsParty");
						
						firstTermsParty.setAttribute("terms_party_type", TermsPartyType.BENEFICIARY);
						InvoicesSummaryData invoice = (InvoicesSummaryData) mediatorServices.createServerEJB("InvoicesSummaryData",
								Long.parseLong((String) alreadyAsstInvoices.get(0)));
						if (!TradePortalConstants.EXPORT_FIN.equals(invoice.getAttribute("loan_type")))
							firstTermsParty.setAttribute("name", mediatorServices.getResourceManager().getText("Common.MultBenesEntered", TradePortalConstants.TEXT_BUNDLE));
						// IR 29777 end

						int paymentChargeCount = InvoiceUtility.isSinglePaymentCharge(invoiceOidsSql);
						if (paymentChargeCount == 1) {

							terms.setAttribute("bank_charges_type", invoice.getAttribute("charges"));
						} else if (paymentChargeCount > 1) {
							terms.setAttribute("bank_charges_type", TradePortalConstants.CHARGE_UPLOAD_FW_SHARE);
							InvoiceUtility.setBankChargesForInvoices(invoiceOidsSql);
							mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.INFO_SHARED_BANK_CHARGES);

						}

					}

				}

			}
		}

		if (TradePortalConstants.INDICATOR_NO.equals(importIndicator) || TradePortalConstants.INDICATOR_X.equals(importIndicator)) {
			String dueDateSQL = "select distinct(case when payment_date is null then due_date else payment_date end ) DUEDATE from invoices_summary_data where " + invoiceOidsSql;
			DocumentHandler resultDoc = DatabaseQueryBean.getXmlResultSet(dueDateSQL, false, new ArrayList<Object>());
			invoiceDueDate = resultDoc.getAttribute("/ResultSetRecord(0)/DUEDATE");

			if (StringFunction.isNotBlank(invoiceDueDate)) {
				try {
					SimpleDateFormat iso = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
					SimpleDateFormat jPylon = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
					Date date = jPylon.parse(invoiceDueDate);
					terms.setAttribute("loan_terms_fixed_maturity_dt", TPDateTimeUtility.convertISODateToJPylonDate(iso.format(date)));
					terms.setAttribute("loan_terms_type", TradePortalConstants.LOAN_FIXED_MATURITY);
				} catch (Exception e) {
					LOG.error("Exception while dealing with attributes loan_terms_fixed_maturity_dt/loan_terms_type .", e);
				}
			}
			if (StringFunction.isBlank(terms.getAttribute("c_FirstTermsParty"))) {
				long termsPartyOID = terms.newComponent("FirstTermsParty");
				TermsParty firstTermsParty = (TermsParty) terms.getComponentHandle("FirstTermsParty");
				String b = firstTermsParty.getAttribute("terms_party_oid");
				// String benName = matchingRule!=null?matchingRule.getAttribute("buyer_name"):"";
				beneficiaryName = tpName;

				firstTermsParty.setAttribute("name", tpName);
				if (matchingRule != null) {
					firstTermsParty.setAttribute("address_line_1", matchingRule.getAttribute("address_line_1"));
					firstTermsParty.setAttribute("address_line_2", matchingRule.getAttribute("address_line_2"));
					firstTermsParty.setAttribute("address_city", matchingRule.getAttribute("address_city"));
					firstTermsParty.setAttribute("address_state_province", matchingRule.getAttribute("address_state"));
					firstTermsParty.setAttribute("address_country", matchingRule.getAttribute("address_country"));
					firstTermsParty.setAttribute("address_postal_code", matchingRule.getAttribute("postalcode"));
					firstTermsParty.setAttribute("OTL_customer_id", matchingRule.getAttribute("tps_customer_id"));
				}
				firstTermsParty.setAttribute("vendor_id", instrument.getAttribute("vendor_id"));

			}

		}
		if (TradePortalConstants.SELLER_REQUESTED_FINANCING.equals(financeType)) {
			InvoicesSummaryData invoice = (InvoicesSummaryData) mediatorServices.createServerEJB("InvoicesSummaryData", Long.parseLong(String.valueOf(invoiceOid)));
			ArMatchingRule rule = invoice.getMatchingRule();
			if (rule != null) {
				TermsParty firstTermsParty = null;
				try {
					firstTermsParty = (TermsParty) terms.getComponentHandle("FirstTermsParty");
				} catch (Exception e) {
					LOG.trace("Exception while loading existing FirstTermsParty component. Creating New component.");
					long firstTermsPartyOid = terms.newComponent("FirstTermsParty");
					firstTermsParty = (TermsParty) terms.getComponentHandle("FirstTermsParty");
				}
				firstTermsParty.setAttribute("OTL_customer_id", rule.getAttribute("tps_customer_id"));
			}
		}

		outputDoc.setAttribute("/ChangePOs/fromStrucPO", "Y");
		String termsPartyOid = terms.getAttribute("c_FirstTermsParty");
		if (!StringFunction.isBlank(terms.getAttribute("c_FirstTermsParty"))) {
			TermsParty firstTermsParty = (TermsParty) terms.getComponentHandle("FirstTermsParty");
			String b = firstTermsParty.getAttribute("terms_party_oid");
			firstTermsParty.setAttribute("name", beneficiaryName);
		}

		transaction.save(false);
		instrument.save(false);
		mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.ADD_INVOICE_SUCCESS,
				String.valueOf(numberOfInvoices));
	}

}
