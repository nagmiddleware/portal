package com.ams.tradeportal.mediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import java.rmi.*;
import java.util.*;
import java.text.*;
import java.math.*;

import com.amsinc.ecsg.util.*;
import com.amsinc.ecsg.frame.*;

import com.ams.tradeportal.busobj.*;
import com.ams.tradeportal.common.*;
import com.ams.tradeportal.busobj.util.*;

/*
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class PurchaseOrderMediatorBean extends MediatorBean
{
private static final Logger LOG = LoggerFactory.getLogger(PurchaseOrderMediatorBean.class);
    
  // This SQL selects the user's name
  private static String USER_SQL = "select first_name, last_name from users where user_oid = ";

  // Business methods
  public DocumentHandler execute(DocumentHandler inputDoc, DocumentHandler outputDoc, MediatorServices mediatorServices)
	 throws RemoteException, AmsException {

	// Based on the button clicked, process an appropriate action.
	String buttonPressed = inputDoc.getAttribute("/Update/ButtonPressed");
	mediatorServices.debug("The button pressed is " + buttonPressed);	
	
	if (buttonPressed.equals(TradePortalConstants.BUTTON_DELETE_PO)) {
		outputDoc = deletePurchaseOrderItems(inputDoc, mediatorServices);
	} else if (buttonPressed.equals(TradePortalConstants.BUTTON_DELETE)) {
		outputDoc = deletePurchaseOrderItems(inputDoc, mediatorServices);
      //TLE - 07/29/07 - CR-375 - Add Begin 
	//} else if (buttonPressed.equals(TradePortalConstants.BUTTON_CREATE_FROM_PO)) {
      //     outputDoc = kickoffLCCreationProcess(inputDoc, mediatorServices);
      } else if (buttonPressed.equals(TradePortalConstants.BUTTON_CREATE_LC_FROM_PO)|| buttonPressed.equals(TradePortalConstants.BUTTON_CREATE_ATP_FROM_PO)) {				
		outputDoc = kickoffPOCreationProcess(inputDoc, mediatorServices);
					
      //TLE - 07/29/07 - CR-375 - Add End 
		//Srinivasu_D CR-707 Rel8.0 03/06/2012 Start 
	}else if (buttonPressed.equals(TradePortalConstants.BUTTON_DELETE_PO_STRUCTURED)) {
		outputDoc = deletePurchaseOrderStructuredItems(inputDoc, mediatorServices);
	}  
	else if (buttonPressed.equals(TradePortalConstants.BUTTON_CREATE_ATP_FROM_PO_STRUCTURED) || buttonPressed.equals(TradePortalConstants.BUTTON_CREATE_LC_FROM_PO_STRUCTURED)) {
		outputDoc = kickOffPOFileUploadProcess(inputDoc,mediatorServices);		
	}  //Srinivasu_D CR-707 Rel8.0 03/06/2012 End
	else {
		LOG.info("Error in PurchaseOrderMediator - unknown button: " + buttonPressed);
	}
	
	return outputDoc;
  }                        

    //Srinivasu_D CR-707 Rel8.0 03/06/2012 Start 
	/**
	 * This method performs delete operation i.e. setting DELETED_IND as Y in PURCHASE_ORDER table. 
	 * @param inputDoc
	 * @param mediatorServices
	 * @return
	 * @throws RemoteException
	 * @throws AmsException
	 */
	public DocumentHandler deletePurchaseOrderStructuredItems(DocumentHandler inputDoc, MediatorServices mediatorServices)
	throws RemoteException, AmsException {
	
	DocumentHandler purchaseOrderItem = null;
	Vector          poList = null;
	PurchaseOrder      poOrder = null;
	String          poOrderOid = null;
	int             count = 0;
	String poNum = null;
	String poLineItemOid = null;
	BusinessObject busObj = null; 
	String userOid = null;
	long result=0;
	userOid = inputDoc.getAttribute("/User/userOid");

	// Populate variables with inputDocument information: 
	poList = inputDoc.getFragments("/PurchaseOrderList/PurchaseOrder");
	count  = poList.size();	
	mediatorServices.debug("PurchaseOrderMediator: number of po structured items to delete: " + count); 
	
	if (count <= 0) {
		mediatorServices.getErrorManager().issueError(
			TradePortalConstants.ERR_CAT_1, 
			TradePortalConstants.NO_ITEM_SELECTED, 
			mediatorServices.getResourceManager().getText(
				"TransactionAction.Delete", 
				TradePortalConstants.TEXT_BUNDLE)); 
	}

	// Loop through the list of po line item oids, deleting each one.
	
	for (int i = 0; i < count; i++) {
		purchaseOrderItem = (DocumentHandler) poList.get(i);
		poOrderOid = purchaseOrderItem.getAttribute("/POLineItemOid");
		
		mediatorServices.debug("PurchaseOrderMediator: po line item oid to delete: " + poOrderOid); 		
		
		try {			
			if(!InstrumentServices.isBlank(poOrderOid)){					
			// Deleting by calling PurchaseOrder EJB			
			poOrder = (PurchaseOrder) mediatorServices.createServerEJB("PurchaseOrder",Long.parseLong(poOrderOid));
			poNum = poOrder.getAttribute("purchase_order_num");
			result = poOrder.deletePO(userOid);
			if(result!=0){
				{
				mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1, 
					TradePortalConstants.DELETE_SUCCESSFUL, 
					poNum); 
			  }
			}	
		   
		  }		
		}
		catch(Exception e){		
			e.printStackTrace();
			}

	}
	
	return (new DocumentHandler());

}
	//Srinivasu_D CR-707 Rel8.0 03/06/2012 End 
	
/**
   * This method performs deletions of purchase order line items
   * The format expected from the input doc is:
   *     
   * <PurchaseOrderList>
   *<PurchaseOrder ID="0"><POLineItemOid>1000001/1200001/AMD</POLineItemOid></PurchaseOrder>
   *<PurchaseOrder ID="1"><POLineItemOid>1000002/1200002/ISS</POLineItemOid></PurchaseOrder>
   *<PurchaseOrder ID="2"><POLineItemOid>1000003/1200003/TRN</POLineItemOid></PurchaseOrder>
   *<PurchaseOrder ID="3"><POLineItemOid>1000004/1200004/ASG</POLineItemOid></PurchaseOrder>
   *<PurchaseOrder ID="6"><POLineItemOid>1000001/1200007/ISS</POLineItemOid></PurchaseOrder>
   * </PurchaseOrderList>
   * <User>
   *    <userOid>1001</userOid>
   *    <securityRights>302231454903657293676543</securityRights>
   * </User>
   *
   * This is a non-transactional mediator that executes transactional
   * methods in business objects
   *
   * @param inputDoc The input XML document (&lt;In&gt; section of overall
   *                 Mediator XML document)
   * @param mediatorServices Associated class that contains lots of the
   *                 auxillary functionality for a mediator.
   * @return outputDoc A blank document
   */

public DocumentHandler deletePurchaseOrderItems(DocumentHandler inputDoc, MediatorServices mediatorServices)
	throws RemoteException, AmsException {

	DocumentHandler purchaseOrderItem = null;
	Vector          poList = null;
	POLineItem      poLineItem = null;
	String          poLineItemOid = null;
	String          poNum = null;
	String          lineNum = null;
	int             count = 0;

	// Populate variables with inputDocument information: purchase ordern list,
	// user oid and security rights.
	poList = inputDoc.getFragments("/PurchaseOrderList/PurchaseOrder");
	count  = poList.size();

	mediatorServices.debug(
		"PurchaseOrderMediator: number of po items to delete: " + count); 

	if (count <= 0) {
		mediatorServices.getErrorManager().issueError(
			TradePortalConstants.ERR_CAT_1, 
			TradePortalConstants.NO_ITEM_SELECTED, 
			mediatorServices.getResourceManager().getText(
				"TransactionAction.Delete", 
				TradePortalConstants.TEXT_BUNDLE)); 
	}

	// Loop through the list of po line item oids, deleting each one.
	for (int i = 0; i < count; i++) {
		purchaseOrderItem = (DocumentHandler) poList.get(i);
		poLineItemOid = purchaseOrderItem.getAttribute("/POLineItemOid");

		mediatorServices.debug(
			"PurchaseOrderMediator: po line item oid to delete: " + poLineItemOid); 

		try {
			// create and load the po line item and call delete
			poLineItem = 
				(POLineItem) mediatorServices.createServerEJB(
					"POLineItem", 
					Long.parseLong(poLineItemOid)); 
			poNum = poLineItem.getAttribute("po_num");
			lineNum = poLineItem.getAttribute("item_num");

			poLineItem.delete();
			
			if (this.isTransSuccess(mediatorServices.getErrorManager())) {
				mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1, 
					TradePortalConstants.DELETE_SUCCESSFUL, 
					poNum + " " + lineNum); 
			}

		} finally {
		}

	}
	
	return (new DocumentHandler());

}

/**
   * This method adds an entry to the auto lc create queue to perform a lc creation
   * process
   *
   * @param inputDoc The input XML document (&lt;In&gt; section of overall
   *                 Mediator XML document)
   * @param mediatorServices Associated class that contains lots of the
   *                 auxillary functionality for a mediator.
   * @return outputDoc A blank document
   */

public DocumentHandler kickoffPOCreationProcess(DocumentHandler inputDoc, MediatorServices mediatorServices)
	throws RemoteException, AmsException {

	mediatorServices.debug("PurchaseOrderMediator: adding entry to AutoLCCreate queue");
	mediatorServices.debug("inputDoc ->" + inputDoc.toString());
	mediatorServices.debug("whatever " + inputDoc.getComponent("/Update").toString());	
    String where = "A_CORP_ORG_OID = ?";
    if (DatabaseQueryBean.getCount("ACTIVE_PO_UPLOAD_OID", "ACTIVE_PO_UPLOAD", where, true, new Object[]{inputDoc.getAttribute("/AutoLCCreateInfo/corporateOrgOid")}) != 0)
    {
        mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                                                      TradePortalConstants.AUTOLC_PROCESS_RUNNING);
        return (new DocumentHandler());
    }
    
    //instantiate the ActivePOUpload, set a few attributes, and save it
    //the DB record will be picked up later by the AutoLCAgent background process
    ActivePOUpload activePO = (ActivePOUpload) mediatorServices.createServerEJB("ActivePOUpload");
    activePO.newObject();
    
    //build the xmlDoc...we need to wrap everything in <In> tags so that the AutoLCCreateMediator will work
    DocumentHandler xmlDoc = new DocumentHandler ();
    xmlDoc.setComponent("/In", inputDoc);
    mediatorServices.debug("xmlDoc -> " + xmlDoc.toString());
    
    //set the attributes
    activePO.setAttribute("po_upload_parameters", xmlDoc.toString());
    activePO.setAttribute("status", "S");
    activePO.setAttribute("corp_org_oid", inputDoc.getAttribute("/AutoLCCreateInfo/corporateOrgOid"));
  
    //save the active_po_upload record
    activePO.save(false); //false means don't validate just save
  //Rpasupulati IR T36000017209 Start
    Date currentLocalDate =GMTUtility.getGMTDateTime();
    String localDate        = TPDateTimeUtility.formatDateTime(currentLocalDate, inputDoc.getAttribute("/User/locale"));

    // Get the user's name for the log message and info message about to be issued
    String userName = getUserName(inputDoc.getAttribute("/User/userOid"));
    String userOrgOid = inputDoc.getAttribute("/User/ownerOrgOid");
    String poUploadSequenceNumber = String.valueOf(ObjectIDCache.getInstance(TradePortalConstants.AUTOLC_SEQUENCE).generateObjectID());
    logPurchaseOrderUploadMessage(userOrgOid, userName, localDate, poUploadSequenceNumber);
   
    //Rpasupulati IR T36000017209 Ends
    
    mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                                                  TradePortalConstants.LCLOG_CREATE_LC_START,
                                                  getUserName(inputDoc.getAttribute("/User/userOid")),
                                                  DateTimeUtility.convertDateToDateString(GMTUtility.getGMTDateTime()));
    
	return (new DocumentHandler());

}



/**
 * Getter for the user name.  If it has not been set, a database lookup is
 * done using the userOid.  The first and last names are appended together to
 * form the name.
 * 
 * @return java.lang.String
 */
private String getUserName(String userOid) 
	throws RemoteException, AmsException {

    StringBuffer sql = new StringBuffer(USER_SQL);

	sql.append(" ? ");
	
	DocumentHandler resultSet = DatabaseQueryBean.getXmlResultSet(sql.toString(), true, new Object[]{userOid});

    return resultSet.getAttribute("/ResultSetRecord/FIRST_NAME") + " " +
	       resultSet.getAttribute("/ResultSetRecord/LAST_NAME");
}

//Srinivasu_D CR-707 Rel8.0 03/06/2012 Start 
/**
 * This method creates an entry for PurchaseOrderUploadInfo in po_file_uploads table indicates a PO is auto created.
 * @param inputDoc
 * @param mediatorServices
 * @return
 * @throws RemoteException
 * @throws AmsException
 */
public DocumentHandler kickOffPOFileUploadProcess(DocumentHandler inputDoc, MediatorServices mediatorServices)
		throws RemoteException, AmsException {

	String buttonPressed = inputDoc.getAttribute("/Update/ButtonPressed");	
	DocumentHandler poParameterDoc = new DocumentHandler();
	poParameterDoc.setAttribute("/POUploadDataOption","G");
	poParameterDoc.setAttribute("/POUploadInstrOption","");
	poParameterDoc.setAttribute("/POUploadGroupingOption","");
	poParameterDoc.setAttribute("/AutoCreated",TradePortalConstants.INDICATOR_YES);
	
    //instantiate the PurchaseOrderFileUploadBean, set a few attributes, and save it
    //the DB record will be picked up later by the PurchaseOrderFileUploadAgent background process
    PurchaseOrderFileUpload pofileUpload = (PurchaseOrderFileUpload) mediatorServices.createServerEJB("PurchaseOrderFileUpload");
    pofileUpload.newObject();    
    //build the xmlDoc...we need to wrap everything in <In> tags so that the AutoLCCreateMediator will work
    DocumentHandler xmlDoc = new DocumentHandler ();
    xmlDoc.setComponent("/In", inputDoc);
	xmlDoc.setComponent("/In/PurchaseOrderUploadInfo",poParameterDoc);
    xmlDoc.setAttribute("/In/User/corporateOrgOid", inputDoc.getAttribute("/AutoLCCreateInfo/corporateOrgOid"));
    xmlDoc.setAttribute("/In/User/clientBankOid", inputDoc.getAttribute("/AutoLCCreateInfo/clientBankOid"));
    mediatorServices.debug("xmlDoc -> " + xmlDoc.toString());   
    
//    if(buttonPressed.equals(TradePortalConstants.BUTTON_CREATE_LC_FROM_PO_STRUCTURED))
//    	xmlDoc.setAttribute("/In/PurchaseOrderUploadInfo/POUploadInstrOption",TradePortalConstants.AUTO_LC_PO_UPLOAD);
//    else
//    	xmlDoc.setAttribute("/In/PurchaseOrderUploadInfo/POUploadInstrOption",TradePortalConstants.AUTO_ATP_PO_UPLOAD);
    //set the attributes
    pofileUpload.setAttribute("po_file_name", "Automated");
    pofileUpload.setAttribute("validation_status", TradePortalConstants.PYMT_UPLOAD_VALIDATION_PENDING);
    //pofileUpload.setAttribute("creation_timestamp", DateTimeUtility.getGMTDateTime().toString()); //BSL IR BOUM032964572 04/13/2012 - add parameters to not use override date
    pofileUpload.setAttribute("creation_timestamp",
            DateTimeUtility.getGMTDateTime(true, false));
    pofileUpload.setAttribute("po_upload_parameters", xmlDoc.toString());
    
    pofileUpload.setAttribute("owner_org_oid", inputDoc.getAttribute("/User/ownerOrgOid"));
    pofileUpload.setAttribute("user_oid", inputDoc.getAttribute("/User/userOid"));   
    
    //save the active_po_upload record
    pofileUpload.save(false); //false means don't validate just save
	
		//Srinivasu_D CR-707 Rel8.0 03/24/2012 Start 
	if (this.isTransSuccess(mediatorServices.getErrorManager())) {
				mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1, 
					TradePortalConstants.PO_ATP_LC_CREATE_SUCCESS); 
			}
	//Srinivasu_D CR-707 Rel8.0 03/24/2012 End 

	return (new DocumentHandler());
	}

	//Srinivasu_D CR-707 Rel8.0 03/06/2012 End 
//Rpasupulati IR T36000017209 Start
private void logPurchaseOrderUploadMessage(String userOrgOid, String userName, String localDate,
        String poUploadSequenceNumber) throws AmsException
{
	AutoLCLogger   logger = null;

	// Retrieve the log for the user's organization
	logger = AutoLCLogger.getInstance(userOrgOid);

	logger.addLogMessage(userOrgOid, poUploadSequenceNumber, TradePortalConstants.LCLOG_CREATE_LC_START,
			userName, localDate);
}

//Rpasupulati IR T36000017209 End
}