package com.ams.tradeportal.mediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import com.ams.tradeportal.busobj.FavoriteTaskCustomization;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.frame.MediatorBean;
import com.amsinc.ecsg.frame.MediatorServices;
import com.amsinc.ecsg.util.DocumentHandler;

/*
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class FavoriteTaskCustomizationMediatorBean extends MediatorBean {
private static final Logger LOG = LoggerFactory.getLogger(FavoriteTaskCustomizationMediatorBean.class);


	// Business methods
	public DocumentHandler execute(DocumentHandler inputDoc,
			DocumentHandler outputDoc, MediatorServices mediatorServices)
			throws RemoteException, AmsException {

		// Based on the button clicked, process an appropriate action.

		String buttonPressed = inputDoc.getAttribute("/Update/ButtonPressed");

		if ((TradePortalConstants.BUTTON_SAVE).equals(buttonPressed)) {			
				outputDoc = performSave(inputDoc, mediatorServices);	
		}

		return outputDoc;
	}

	private DocumentHandler performSave(DocumentHandler inputDoc,
			MediatorServices mediatorServices) throws AmsException, RemoteException {
		// Declare variables

		DocumentHandler outputDoc = new DocumentHandler();
		List<String> deleteList = new ArrayList<String>();
		Vector<DocumentHandler> favoriteTaskList = null;
		String userOid = null;
		String favoriteType = null;
		DocumentHandler favoriteTaskDoc = null;
		String favoriteTaskOid = "";
		FavoriteTaskCustomization favoriteTaskCustomization = null;

		// for now we just update Favorite Link customization sections
		// other things could be added later.

		try {
			userOid = inputDoc.getAttribute("/FavoriteTaskCustomization/user_oid");
			favoriteType = inputDoc.getAttribute("/FavoriteTaskCustomization/favorite_type");
			// first retrieve the existing entries to see if there are any to
			// delete...
			//jgadela R91 IR T36000026319 - SQL INJECTION FIX
			String deleteSql = "select favorite_task_cust_oid from favorite_task_customization"
					+" where a_user_oid = ? and favorite_type =?";
			
			DocumentHandler deleteDoc = DatabaseQueryBean.getXmlResultSet(deleteSql, false, userOid, favoriteType);
			if (deleteDoc != null) {
				Vector favoriteList = deleteDoc.getFragments("/ResultSetRecord");
				for (int i = 0; i < favoriteList.size(); i++) {
					DocumentHandler favoriteDoc = (DocumentHandler) favoriteList.get(i);
					String favoriteTaskOidAttr = favoriteDoc
							.getAttribute("/FAVORITE_TASK_CUST_OID");
					deleteList.add(favoriteTaskOidAttr);
				}
			}

			// insert/update - flag the existing ones updated
			favoriteTaskList = inputDoc.getFragments("/FavoriteTaskCustomization/fav_list");

			for (int i = 0; i < favoriteTaskList.size(); i++) {
				// Retrieve the next item
				favoriteTaskDoc = (DocumentHandler) favoriteTaskList.elementAt(i);

				// todo: verify display order is unique.

				// Get a handle to an instance of the favorite task object
				favoriteTaskCustomization = (FavoriteTaskCustomization) mediatorServices.createServerEJB("FavoriteTaskCustomization");

				// Retrieve the unique oid
				favoriteTaskOid = favoriteTaskDoc.getAttribute("/favorite_task_cust_oid");
				if (favoriteTaskOid != null
						&& favoriteTaskOid.length() > 0
						&& !"0".equals(favoriteTaskOid)) { // update
					favoriteTaskCustomization.getData(Long.parseLong(favoriteTaskOid));
					// Set the optimistic lock attribute to make sure hasn't
					// been changed by someone else
					// since it was first retrieved
					favoriteTaskCustomization.setAttribute("opt_lock",favoriteTaskDoc.getAttribute("/opt_lock"));

					int deleteIdx = deleteList.indexOf(favoriteTaskOid);
					if (deleteIdx >= 0) {
						deleteList.remove(deleteIdx);
					}
				} else { // insert
					favoriteTaskCustomization.newObject();
				}

				// Set object attributes equal to input arguments and save
				favoriteTaskCustomization.setAttribute("user_oid", userOid);
				favoriteTaskCustomization.setAttribute("display_order", favoriteTaskDoc.getAttribute("/display_order"));
				favoriteTaskCustomization.setAttribute("favorite_id", favoriteTaskDoc.getAttribute("/favorite_id"));
				favoriteTaskCustomization.setAttribute("favorite_type", favoriteType);
                favoriteTaskCustomization.setAttribute("addl_params", favoriteTaskDoc.getAttribute("/addl_params"));
				
				favoriteTaskCustomization.save();
				
			}

			// delete those still in delete list
			for (int i = 0; i < deleteList.size(); i++) {
				favoriteTaskOid = deleteList.get(i);

				// Get a handle to an instance of the FX rate object
				favoriteTaskCustomization = (FavoriteTaskCustomization) mediatorServices
						.createServerEJB("FavoriteTaskCustomization");
				favoriteTaskCustomization.getData(Long.parseLong(favoriteTaskOid));
				favoriteTaskCustomization.delete();
			}
		} catch (AmsException e) {
			System.out
					.println("AmsException occurred in FavoriteTaskCustomizationMediator: "
							+ e.toString());
		} catch (RemoteException e) {
			System.out
					.println("RemoteException occurred in FavoriteTaskCustomizationMediator: "
							+ e.toString());
		} finally {
			// If all were updated successfully without errors, issue info
			// message indicating this
			if (this.isTransSuccess(mediatorServices.getErrorManager())) {
				
				// jgadela 08/12/2014 R8.4 IR T36000026788 - [START] Corrected the message format for the favorite reports.
				String msgTitle  = null;
				if(TradePortalConstants.FAV_TYPE_REPORT.equalsIgnoreCase(favoriteType)){
					msgTitle  = mediatorServices.getResourceManager().getText(
						"MyLinks.FavoriteReports",
						TradePortalConstants.TEXT_BUNDLE);
				}else{
					msgTitle  = mediatorServices.getResourceManager().getText(
							"FavoriteTask.Title",
							TradePortalConstants.TEXT_BUNDLE);
				}
				
				mediatorServices.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.UPDATE_SUCCESSFUL, msgTitle
						);
				// jgadela 08/12/2014 R8.4 IR T36000026788 - [END] 
			}
		}
		return outputDoc;
	}
		
}
