package com.ams.tradeportal.mediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.common.*;
import com.ams.tradeportal.busobj.*;
import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import javax.ejb.*;
import java.rmi.*;
import java.util.Vector;

/*
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class INSSTATUnPackagerMediatorBean extends MediatorBean {
private static final Logger LOG = LoggerFactory.getLogger(INSSTATUnPackagerMediatorBean.class);

/*
 * Manages MailMessage UnPackaging..
 */

       public DocumentHandler execute(DocumentHandler unpackagerDoc, DocumentHandler outputDoc, MediatorServices mediatorServices)
                                                                                        throws RemoteException, AmsException
 {

      SessionContext mContext = mediatorServices.mContext;
      boolean unpackage = true;
      String instrumentOid = null ;
      String assignedToCorpOrgOid = null ;
      String aClientBankOid = null ;
      String clientBankOid = null;
      int instrumentCount = 0;
      int count = 0;
      String whereClause;
    //  MailMessage mailMessage = (MailMessage)  mediatorServices.createServerEJB("MailMessage");
     Instrument instrument = (Instrument) mediatorServices.createServerEJB("Instrument");



     try
      {
    	 
    	  String agentId=unpackagerDoc.getAttribute("/AgentID");
		  String opBankOrgProponixID   =  unpackagerDoc.getAttribute("/Proponix/Header/OperationOrganizationID");
		  if(opBankOrgProponixID == null || opBankOrgProponixID.equals(""))
		  	{
		  	  mediatorServices.debug("No opBankOrg to update");
		  	  instrument.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,TradePortalConstants.VALUE_MISSING,"OperationOrganizationID", "/Proponix/Header/OperationOrganizationID");
		      mediatorServices.debug("Debug 1");
		      unpackage=false;
		    }
		  else
		  	{
				String sqlStatement = " SELECT ORGANIZATION_OID FROM OPERATIONAL_BANK_ORG WHERE PROPONIX_ID = ? AND ACTIVATION_STATUS = ?";
						DocumentHandler resultXML = DatabaseQueryBean.getXmlResultSet(sqlStatement, false, opBankOrgProponixID, TradePortalConstants.ACTIVE);
						if(resultXML == null)
						{
							instrument.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE ,TradePortalConstants.NO_MATCHING_VALUE_FOUND, "OperationalBankOrg");
							unpackage = false;
						}
						else
						{
							//make sure that we don't have more than one result
							Vector v = resultXML.getFragments("/ResultSetRecord(0)");
							if (v.size() > 1) {
								instrument.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,TradePortalConstants.MORE_THAN_ONE_VALUE_FOUND, "OperationalBankOrg");
								unpackage = false;
							}
							int rid = 0;//We only need the first record in the result set so use id = '1'
							String opOrgOid = resultXML.getAttribute("/ResultSetRecord(" + rid + ")/ORGANIZATION_OID");

							if(opOrgOid==null || opOrgOid.equals(""))
							 {
								instrument.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE ,TradePortalConstants.NO_MATCHING_VALUE_FOUND, "OperationalBankOrg");
								unpackage = false;
						 	  }
							else
								unpackage = true;
						}
//IValavala End
		    }

		   String clientBankOTLID = unpackagerDoc.getAttribute("/Proponix/SubHeader/ClientBank/OTLID");
	  	   if(clientBankOTLID==null || clientBankOTLID.equals(""))
	  	   {
	  		   mediatorServices.debug("No clientBank to update");
	  		   instrument.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,TradePortalConstants.VALUE_MISSING,"ClientBank OTLID", "/Proponix/SubHeader/ClientBank/OTLID");
               mediatorServices.debug("Debug 5");
               unpackage = false;
	  	   }
	  	   else
	  	   {
	  	    whereClause  = "OTL_ID = ? and activation_status =?";
	  		count = DatabaseQueryBean.getCount("OTL_ID","CLIENT_BANK",whereClause,false,clientBankOTLID,TradePortalConstants.ACTIVE);
	  		if(count==1)
	  		 {
	  	  	  ClientBank clientBank = (ClientBank)mediatorServices.createServerEJB("ClientBank");
	  	  	  clientBank.find("OTL_id",clientBankOTLID);
	  	  	  clientBankOid = clientBank.getAttribute("organization_oid");
	  	  	  unpackage = true;
	  	  	  mediatorServices.debug("Debug 51");
	  		 }
	  	    if(count==0)
	  	     {
	  		 instrument.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE ,TradePortalConstants.NO_MATCHING_VALUE_FOUND, "ClientBank");
		     unpackage = false;
		     mediatorServices.debug("Debug 6");
		     }
	  		if(count>1)
	  		 {
	  		 instrument.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,TradePortalConstants.MORE_THAN_ONE_VALUE_FOUND, "ClientBank");
		     unpackage = false;
		     mediatorServices.debug("Debug 7");
		     }
	       }

           String completeInstrumentID   = unpackagerDoc.getAttribute("/Proponix/SubHeader/InstrumentID");

           if(completeInstrumentID == null || completeInstrumentID.equals(""))
             {
              String corpOrg_Proponix_cust_id = unpackagerDoc.getAttribute("/Proponix/SubHeader/CustomerID");
             String sqlStatement = " SELECT ORGANIZATION_OID FROM  CORPORATE_ORG WHERE PROPONIX_CUSTOMER_ID = ?";

             DocumentHandler resultXML = DatabaseQueryBean.getXmlResultSet(sqlStatement, false, new Object[]{corpOrg_Proponix_cust_id});
             if(resultXML == null)
 			  {
 			 	mediatorServices.debug("No customerID ");
 			 	assignedToCorpOrgOid = "";
 			  }
 			  else
 			  {
				int rid = 0;
			    assignedToCorpOrgOid = resultXML.getAttribute("/ResultSetRecord(" + rid + ")/ORGANIZATION_OID");
			  }
			 instrumentOid = "";

 			 }
           else
              {
                 whereClause  = "COMPLETE_INSTRUMENT_ID = ?";
                instrumentCount = DatabaseQueryBean.getCount("INSTRUMENT_OID","INSTRUMENT",whereClause,false,new Object[]{completeInstrumentID});
                mediatorServices.debug("Debug 81: Instrument count =" + instrumentCount);

                String corpOrg_Proponix_cust_id = unpackagerDoc.getAttribute("/Proponix/SubHeader/CustomerID");
           /*    if(instrumentCount == 1)
               {
				   instrument.getData(Long.parseLong(instrumentOid));
			   }*/
               if(instrumentCount ==0)
              {
				  unpackage = false;
				  mediatorServices.debug("Debug 82: Instrument count is zero" );
                  instrument.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE ,TradePortalConstants.NO_MATCHING_VALUE_FOUND,"Instrument");//SUUJ031039400
			  }
                if(instrumentCount > 0)
              {
 			    if(corpOrg_Proponix_cust_id==null || corpOrg_Proponix_cust_id.equals(""))
 			     {
 			   	    mediatorServices.debug("No corpOrg to update");
 			   	    mediatorServices.debug("Debug 9");
 			   	 // mailMessage.getErrorManager().issueError("Unpackaging Error" , "No  value found at " + "/Proponix/SubHeader/CustomerID");
 	   		      instrument.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE , TradePortalConstants.VALUE_MISSING,"CustomerID", "/Proponix/SubHeader/CustomerID");

 	   		      unpackage = false;
 	   		     }
 	   		     else
 	   		     {
                     // W Zhu 4/21/09 SOUJ041661270 add ACTIVATION_STATUS in where clause.
 	   		         whereClause  = "PROPONIX_CUSTOMER_ID = ? AND ACTIVATION_STATUS = ?";
				    count = DatabaseQueryBean.getCount("PROPONIX_CUSTOMER_ID","CORPORATE_ORG",whereClause,false,corpOrg_Proponix_cust_id,TradePortalConstants.ACTIVE);
				    if(count==0)
					 {
					  instrument.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE , TradePortalConstants.NO_MATCHING_VALUE_FOUND,"CustomerID(Corporate_Org)");
					  unpackage = false;
					  mediatorServices.debug("Debug 10");
		             }
		            if(count==1)
		            {
		             unpackage = true;
                     // W Zhu 4/21/09 SOUJ041661270 add ACTIVATION_STATUS in where clause.
                     String sqlStatement = " SELECT INSTRUMENT_OID, INSTRUMENT.A_CLIENT_BANK_OID , ORGANIZATION_OID FROM INSTRUMENT, CORPORATE_ORG WHERE " +
                                      "  A_CORP_ORG_OID = ORGANIZATION_OID AND COMPLETE_INSTRUMENT_ID = ?"+
                                      " AND PROPONIX_CUSTOMER_ID = ?" +
                                      " AND ACTIVATION_STATUS = ?";


                      DocumentHandler resultXML = DatabaseQueryBean.getXmlResultSet(sqlStatement, false, completeInstrumentID, corpOrg_Proponix_cust_id, TradePortalConstants.ACTIVE);
                       if(resultXML == null)
 				       {
 				 	    unpackage = false;
 				 	    mediatorServices.debug("No relatedInstrumentID from SQL");
 				 	   instrument.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE ,TradePortalConstants.NO_MATCHING_VALUE_FOUND,"RelatedInstrument");


				        }
				      else
				       {
                        int rid = 0;
                        instrumentOid    = resultXML.getAttribute("/ResultSetRecord(" + rid + ")/INSTRUMENT_OID");
                        assignedToCorpOrgOid = resultXML.getAttribute("/ResultSetRecord(" + rid + ")/ORGANIZATION_OID");
                        aClientBankOid        = resultXML.getAttribute("/ResultSetRecord(" + rid + ")/A_CLIENT_BANK_OID");
                        if( instrumentOid ==null ||  instrumentOid.equals(""))
                        {
                          unpackage = false;
                          mediatorServices.debug("No relatedInstrumentID to update");
                          mediatorServices.debug("Debug 1");

                          instrument.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE ,TradePortalConstants.NO_MATCHING_VALUE_FOUND,"RelatedInstrument");

					    }
					    else
					    {
						  instrument.getData(Long.parseLong(instrumentOid));
						}
                        if(assignedToCorpOrgOid == null || assignedToCorpOrgOid.equals(""))
                        {
                          unpackage = false;
                          mediatorServices.debug("No assignedToCorpOrgOid to update");

                         instrument.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE ,TradePortalConstants.NO_MATCHING_VALUE_FOUND,"RelatedInstrument");
					    }
				      }

                      if(unpackage == true)
				       {
					    if(clientBankOid!=null && clientBankOid.equals(aClientBankOid))
					     unpackage = true;
					     else
					     {
						//  instrument.getErrorManager().issueError("instrument Unpackaging Error", "ClientBank does not match with Instrument's ClientBank");
					     instrument.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE ,TradePortalConstants.VALUE_DOESNOT_MATCH,"ClientBank","Instrument's ClientBank");
					     unpackage = false;
					     }
					  //  unpackagerDoc.getAttribute("/Proponix/SubHeader/ClientBank/OTLID");

				        }

			         }//end of if count==1
                    else { //if count > 1
                        // W Zhu 4/21/09 SOUJ041661270 add error message for more than one corporate_org found for the CustomerID
                        mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE ,TradePortalConstants.MORE_THAN_ONE_VALUE_FOUND, "CustomerID");
                    }
				    }//end of else
                   }//end of instrument count > 0
			  }//end of else
           
         //PPX-208 - Ravindra B - 03/18/2011 - Start.
           if(instrumentOid != null){
				try{
					String messageId = unpackagerDoc.getAttribute("/Proponix/Header/MessageID");
					//String agentId= getAgentID(messageId);
					if(agentId==null){
						agentId= getAgentID(messageId);
					}
					LockingManager.lockBusinessObject(Long.parseLong(instrumentOid),
							agentId, true);
					mediatorServices.debug("INSSTATUnPackagerMediatorBean: Locking instrument "
					    + instrumentOid + "successful");
			    }
			    catch (InstrumentLockException e){
			    	//Instrument is Previously locked.
			    	outputDoc.setAttribute("/Param/ResubmitMsg","Y");
					return outputDoc;
			    }
           }
		    try{
			    //PPX-208 - Ravindra B - 03/18/2011 - End.	
           //PPX-208 - Adarsha - Added If condition for handling out of order message. 
	          if(!isOutOfOrderMessage(unpackagerDoc, instrumentOid)){
              Transaction transaction = getActiveTransaction(instrument);
			  if(transaction != null)
			    {
			      unpackage(transaction, instrument, unpackagerDoc);
			      instrument.save();
				    }
	          }		    
	          //PPX208- 03/14/2011 -Adarsha - Start
		    }//try end			    
		    finally{
		    	if(instrumentOid != null){
		    	try {
		              LockingManager.unlockBusinessObjectByInstrument(Long.parseLong(instrumentOid), true);
		          } catch (InstrumentLockException e) {
		              // this should never happen
		              e.printStackTrace();
		          }
		    	}
		    }
	  //PPX208- 03/14/2011 -Adarsha - End
			//Pass the parameters to be updated in the Incoming_queue to the Inbound agent
			String activeTranOid = instrument.getAttribute("active_transaction_oid");
	        outputDoc.setAttribute("/Param/TransactionOid",activeTranOid);
            outputDoc.setAttribute("/Param/InstrumentOid",instrumentOid);


	    } //end of try

       catch (RemoteException e) {
            mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,TradePortalConstants.TASK_NOT_SUCCESSFUL,"Message Unpackaging");

           e.printStackTrace();

       }
       catch (AmsException e) {
          mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,TradePortalConstants.TASK_NOT_SUCCESSFUL,"Message Unpackaging");
           e.printStackTrace();


       }
       catch (Exception e) {
             mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,TradePortalConstants.TASK_NOT_SUCCESSFUL,"Message Unpackaging");
            e.printStackTrace();

       }

    return outputDoc;
 }

protected Transaction getActiveTransaction(Instrument instrument) throws RemoteException, AmsException
 {
			  String activeTransactionOid = instrument.getAttribute("active_transaction_oid");
			  	 if(activeTransactionOid==null) activeTransactionOid = "";
			     if(activeTransactionOid.equals(""))
			     {
				    instrument.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE ,TradePortalConstants.NO_MATCHING_VALUE_FOUND,"Active Transaction to perform REVALUE or DEAL_CHANGE");
					return null;
			     }
				 else
				 {
			  	     return (Transaction)instrument.getComponentHandle("TransactionList",Long.parseLong(activeTransactionOid));
			      }
 }

        /**
        * This method takes the transaction object
        * and populates the database with values from Position section
        * of the unpackagerDoc to the Instrument Object
        * @param instrument, Instrument object
        * @param transactionIndex, int value representing the transaction.
        * @param unpackagerDoc, DocumentHandler object with the XML
        * author iv
        */
   protected void unpackage(Transaction transaction, Instrument instrument, DocumentHandler unpackagerDoc)
                                                             throws RemoteException, AmsException

     {
      /* Unpackaging the Position details */
         String insStatPath = "/Proponix/Body";
         transaction.setAttribute("liability_amt_in_limit_curr",   unpackagerDoc.getAttribute(insStatPath+"/LiabilityAmountInLimitCurrency"));
         transaction.setAttribute("available_amount",              unpackagerDoc.getAttribute(insStatPath+"/AvailableAmount"));
         transaction.setAttribute("equivalent_amount",             unpackagerDoc.getAttribute(insStatPath+"/EquivalentAmount"));
         transaction.setAttribute("liability_amt_in_base_curr",    unpackagerDoc.getAttribute(insStatPath+"/LiabilityAmountInBaseCurrency"));
         transaction.setAttribute("base_currency_code",            unpackagerDoc.getAttribute(insStatPath+"/InstrumentBaseCurrencyCode"));
         transaction.setAttribute("limit_currency_code",           unpackagerDoc.getAttribute(insStatPath+"/InstrumentLimitCurrencyCode"));
         transaction.setAttribute("instrument_amount",             unpackagerDoc.getAttribute(insStatPath+"/CurrentInstrumentAmount"));
         //PPX-208 - Adarsha K S - 03/09/2011 - Start
         transaction.setAttribute("sequence_date", unpackagerDoc.getAttribute(insStatPath+"/SequenceDate"));
         transaction.setAttribute("revalue_date", unpackagerDoc.getAttribute(insStatPath+"/RevalueDate"));
         //PPX-208 - Adarsha K S - 03/09/2011 - End
         instrument.setAttribute("copy_of_instrument_amount",      unpackagerDoc.getAttribute(insStatPath+"/CurrentInstrumentAmount"));
         instrument.setAttribute("instrument_status",              unpackagerDoc.getAttribute(insStatPath+"/InstrumentStatus"));
   }
   
 //PPX-208 - Adarsha K S - 03/09/2011 - Start
	/**
	 * 
	 * @param unpackagerDoc
	 * @param instrumentOid
	 * @return
	 * @throws AmsException 
	 */
	
	protected boolean isOutOfOrderMessage(DocumentHandler unpackagerDoc,String instrumentOid) throws AmsException{
		String insStatPath = "/Proponix/Body";
		String sequenceDate = unpackagerDoc.getAttribute(insStatPath+"/SequenceDate");
		if( sequenceDate != null && sequenceDate.trim().length() > 0 ) {

			String whereClause = "p_instrument_oid = ? AND SEQUENCE_DATE > to_date(?, 'mm/dd/yyyy HH24:MI:SS') AND TRANSACTION_STATUS = ?";
			int numberOfLaterTransaction = DatabaseQueryBean.getCount("transaction_oid", "transaction", whereClause, false, instrumentOid, sequenceDate, TransactionStatus.PROCESSED_BY_BANK);
			if (numberOfLaterTransaction == 0){
				String revalueDate = unpackagerDoc.getAttribute(insStatPath+"/RevalueDate");
				if(revalueDate != null && revalueDate.trim().length() > 0){
					whereClause = "p_instrument_oid = ? AND SEQUENCE_DATE = to_date(?, 'mm/dd/yyyy HH24:MI:SS') AND TRANSACTION_STATUS = ? AND REVALUE_DATE > to_date(?, 'mm/dd/yyyy HH24:MI:SS')";
					numberOfLaterTransaction = DatabaseQueryBean.getCount("transaction_oid", "transaction", whereClause, false, instrumentOid, sequenceDate, TransactionStatus.PROCESSED_BY_BANK, revalueDate);
					return numberOfLaterTransaction != 0;
				}else{
					return false;
				}
			}
			
		}else{
			return false;
		}
		
		
		return true;
	}
	
	/**
	 * This method retrieves AgentId from incoming_queue table for given messageID
	 * @param messageId
	 * @return agentId
	 * @throws AmsException 
	 */
	
	protected String getAgentID(String messageId) throws AmsException{
		String agentId=null;
		
		String sqlStatement = "SELECT AGENT_ID FROM INCOMING_QUEUE WHERE MESSAGE_ID = ? ";
		DocumentHandler resultXML = DatabaseQueryBean.getXmlResultSet(sqlStatement, false, new Object[]{messageId});
		if(resultXML != null){
			agentId = resultXML.getAttribute("/ResultSetRecord(0)/AGENT_ID");
		}
		
		return agentId;
	}
	//PPX-208 - Adarsha K S - 03/09/2011 - End

}

