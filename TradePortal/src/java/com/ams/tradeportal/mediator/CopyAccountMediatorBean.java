package com.ams.tradeportal.mediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.RemoteException;
import java.util.Hashtable;
import java.util.StringTokenizer;
import java.util.Vector;

import com.ams.tradeportal.busobj.Account;
import com.ams.tradeportal.busobj.util.InstrumentServices;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.MediatorBean;
import com.amsinc.ecsg.frame.MediatorServices;
import com.amsinc.ecsg.util.DocumentHandler;
//Maheswar CR 610 07/03/2011 Added New
/*
 * Manages the Change Password and Unlock User Transaction.
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class CopyAccountMediatorBean extends MediatorBean
{
private static final Logger LOG = LoggerFactory.getLogger(CopyAccountMediatorBean.class);
   /*
	* Called by the processRequest() method of the Ancestor class to process the designated
	* transaction.  The input parameters for the transaction are received in an XML document
	* (inputDoc).  The execute() method is responsible for calling one or more business objects
	* to process the transaction and populating the outputDoc XML document with the results
	* of the transaction. 
	*/
   public DocumentHandler execute(DocumentHandler inputDoc, DocumentHandler outputDoc, MediatorServices mediatorServices)
								  throws RemoteException, AmsException
   {
	   
	   String otherAccountOids         = inputDoc.getAttribute("/CorporateOrganization/otherAccountOids");
	   String buttonPressed = inputDoc.getAttribute("/Update/ButtonPressed");
		mediatorServices.debug("The button pressed is " + buttonPressed);	
		if (buttonPressed.equals(TradePortalConstants.BUTTON_ADD_ACCOUNTS) && otherAccountOids !=null)
	    { 
	    
			 Vector acctVector = inputDoc.getFragments("/CorporateOrganization/AccountList");
			 int acctvectorSize = acctVector.size();
			 String acctNum = null;
			 String crncy = null;
			 Hashtable accountNumberCurrencyHash = new Hashtable();
			 //hashtable holds all the AcctNum and currency of the original Account.
			 //Later step while adding a new Account if it finds a match with the original Account
			 //it throws error that two accounts cannot have same accountnum and currency. 
			 
			//if it doesnot match with the accountnum and currency it copies the AccountRecords 
			//from DB ,changes the owner id to the original corporateorganization, and changes the 
			 //indicator suggesting that this Account is copied from original and it is a subsidary Account
			for (int i=0;i<acctvectorSize;i++)
			{
				acctNum = inputDoc.getAttribute("/CorporateOrganization/AccountList("+i+")/account_number");
				crncy = inputDoc.getAttribute("/CorporateOrganization/AccountList("+i+")/currency");
				accountNumberCurrencyHash.put(acctNum, crncy);
			}
		   	 
//		   	int accInsertionIndex= acctVector.size();
		    StringTokenizer st= new StringTokenizer(otherAccountOids,",");
		    Account account;
		    Account newAccount;
		    //parses all the Account oids since it was passed with comma separated values from the premeidator.
		   	    
		    while(st.hasMoreTokens())
		    {
		    	String objectID  = st.nextToken();
		    	if(!InstrumentServices.isBlank(objectID))
		    	{
			    	DocumentHandler tmpXML = new DocumentHandler();
					account= (Account) mediatorServices.createServerEJB("Account",Long.parseLong(objectID));
							
					newAccount = (Account) mediatorServices.createServerEJB("Account");
					newAccount.newObject();
					account.populateXmlDoc(tmpXML);
					newAccount.populateFromXmlDoc(tmpXML);
					newAccount.setAttribute("other_account_owner_oid", account.getAttribute("owner_oid"));
					newAccount.setAttribute("owner_oid",inputDoc.getAttribute("/CorporateOrganization/organization_oid"));
					newAccount.setAttribute("othercorp_customer_indicator", "Y");
					acctNum = newAccount.getAttribute("account_number");
					crncy = newAccount.getAttribute("currency");
					
					if (crncy.equals(accountNumberCurrencyHash.get(acctNum))) {
						mediatorServices.getErrorManager().issueError(
		   						TradePortalConstants.ERR_CAT_1,
		   						TradePortalConstants.CUST_ACCTS_DATA_DUPLICATED,
		   						acctNum, crncy);
		   			}
					else{			
					
						newAccount.save();
					}
//Below code is comment because initial plan was to populate the inputDoc which could be
//persisted to the cache.
//					inputDoc = populateInputDoc(newAccount,inputDoc,accInsertionIndex);
//					accInsertionIndex++;
				
		    	}
		    }	
	    }
          return outputDoc;
   }
/*
private DocumentHandler populateInputDoc(Account newAccount,DocumentHandler inputDoc,int accInsertionIndex)  throws AmsException, RemoteException{
	
	    	  
	inputDoc.setAttribute("/CorporateOrganization/AccountList("+accInsertionIndex+")/account_oid",newAccount.getAttribute("account_oid"));
	inputDoc.setAttribute("/CorporateOrganization/AccountList("+accInsertionIndex+")/account_number",newAccount.getAttribute("account_number"));
	inputDoc.setAttribute("/CorporateOrganization/AccountList("+accInsertionIndex+")/currency",newAccount.getAttribute("currency"));
	inputDoc.setAttribute("/CorporateOrganization/AccountList("+accInsertionIndex+")/account_name",newAccount.getAttribute("account_name"));
	inputDoc.setAttribute("/CorporateOrganization/AccountList("+accInsertionIndex+")/account_type",newAccount.getAttribute("account_type"));
	inputDoc.setAttribute("/CorporateOrganization/AccountList("+accInsertionIndex+")/bank_country_code",newAccount.getAttribute("bank_country_code"));
	inputDoc.setAttribute("/CorporateOrganization/AccountList("+accInsertionIndex+")/source_system_branch",newAccount.getAttribute("source_system_branch"));
	inputDoc.setAttribute("/CorporateOrganization/AccountList("+accInsertionIndex+")/available_for_direct_debit",newAccount.getAttribute("available_for_direct_debit"));
	inputDoc.setAttribute("/CorporateOrganization/AccountList("+accInsertionIndex+")/available_for_loan_request",newAccount.getAttribute("available_for_loan_request"));
	inputDoc.setAttribute("/CorporateOrganization/AccountList("+accInsertionIndex+")/available_for_internatnl_pay",newAccount.getAttribute("available_for_internatnl_pay"));
	inputDoc.setAttribute("/CorporateOrganization/AccountList("+accInsertionIndex+")/available_for_xfer_btwn_accts",newAccount.getAttribute("available_for_xfer_btwn_accts"));	
	inputDoc.setAttribute("/CorporateOrganization/AccountList("+accInsertionIndex+")/available_for_domestic_pymts",newAccount.getAttribute("available_for_domestic_pymts"));
	inputDoc.setAttribute("/CorporateOrganization/AccountList("+accInsertionIndex+")/deactivate_indicator",newAccount.getAttribute("deactivate_indicator"));
	inputDoc.setAttribute("/CorporateOrganization/AccountList("+accInsertionIndex+")/panel_auth_group_oid",newAccount.getAttribute("panel_auth_group_oid"));
	inputDoc.setAttribute("/CorporateOrganization/AccountList("+accInsertionIndex+")/pending_transaction_balance",newAccount.getAttribute("pending_transaction_balance"));
	//inputDoc.setAttribute("/CorporateOrganization/AccountList("+accInsertionIndex+")/pending_transac_credit_balance",newAccount.getAttribute("pending_transac_credit_balance"));
	inputDoc.setAttribute("/CorporateOrganization/AccountList("+accInsertionIndex+")/proponix_customer_id",newAccount.getAttribute("proponix_customer_id"));
	inputDoc.setAttribute("/CorporateOrganization/AccountList("+accInsertionIndex+")/fx_rate_group",newAccount.getAttribute("fx_rate_group"));
	inputDoc.setAttribute("/CorporateOrganization/AccountList("+accInsertionIndex+")/op_bank_org_oid",newAccount.getAttribute("op_bank_org_oid"));
	inputDoc.setAttribute("/CorporateOrganization/AccountList("+accInsertionIndex+")/othercorp_customer_indicator",newAccount.getAttribute("othercorp_customer_indicator"));
   	 
	return inputDoc;
}         */  
   
  

}