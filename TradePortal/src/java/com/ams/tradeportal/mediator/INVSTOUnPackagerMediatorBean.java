package com.ams.tradeportal.mediator;

import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.busobj.CorporateOrganization;
import com.ams.tradeportal.busobj.CreditNotes;
import com.ams.tradeportal.busobj.Invoice;
import com.ams.tradeportal.busobj.InvoicesSummaryData;
import com.ams.tradeportal.busobj.util.InvoiceUtility;
import com.ams.tradeportal.common.ConfigSettingManager;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.frame.MediatorBean;
import com.amsinc.ecsg.frame.MediatorServices;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.StringFunction;

/*
*
*     Copyright  � 2008
*     American Management Systems, Incorporated
*     All rights reserved
*/

public class INVSTOUnPackagerMediatorBean extends MediatorBean {
	private static final Logger LOG = LoggerFactory.getLogger(INVSTOUnPackagerMediatorBean.class);

	private String invstoRemoveEJB = "Y";
	private final Map<String, String> SUPP_PORTAL_INVOICE_STATUS_TO_ACTION_MAP = new HashMap<String, String>();

	{
		SUPP_PORTAL_INVOICE_STATUS_TO_ACTION_MAP.put(TradePortalConstants.SP_STATUS_DECLINTED_BY_BANK,
				TradePortalConstants.UPLOAD_INV_ACTION_BANK_DECLINED);
		SUPP_PORTAL_INVOICE_STATUS_TO_ACTION_MAP.put(TradePortalConstants.SP_STATUS_REJECTED_BY_BANK,
				TradePortalConstants.UPLOAD_INV_ACTION_BANK_REJECTED);
		SUPP_PORTAL_INVOICE_STATUS_TO_ACTION_MAP.put(TradePortalConstants.SP_STATUS_PROCESSED_BY_BANK,
				TradePortalConstants.UPLOAD_INV_ACTION_BANK_PROCESSED);
	}

	private final Map<String, String> CREDIT_NOTE_STATUS_TO_ACTION_MAP = new HashMap<String, String>();

	{
		CREDIT_NOTE_STATUS_TO_ACTION_MAP.put(TradePortalConstants.CREDIT_NOTE_STATUS_DEC,
				TradePortalConstants.UPLOAD_INV_ACTION_BANK_DECLINED);
		CREDIT_NOTE_STATUS_TO_ACTION_MAP.put(TradePortalConstants.CREDIT_NOTE_STATUS_PBB,
				TradePortalConstants.UPLOAD_INV_ACTION_BANK_PROCESSED_CN);
		CREDIT_NOTE_STATUS_TO_ACTION_MAP.put(TradePortalConstants.CREDIT_NOTE_STATUS_PBB_BAL,
				TradePortalConstants.UPLOAD_INV_ACTION_BANK_PROCESSED_CN);
		CREDIT_NOTE_STATUS_TO_ACTION_MAP.put(TradePortalConstants.CREDIT_NOTE_STATUS_ASN,
				TradePortalConstants.UPLOAD_INV_ACTION_BANK_PROCESSED);
	}

	public DocumentHandler execute(DocumentHandler unpackagerDoc, DocumentHandler outputDoc, MediatorServices mediatorServices)
			throws RemoteException, AmsException {
		invstoRemoveEJB = ConfigSettingManager.getConfigSetting("PORTAL_APP", "PORTAL", "INVSTO_REMOV_EJB", "Y");
		try {
			String invoiceID = "";
			String invoiceOTLUoid = "";
			String invoiceStatus = "";
			String messageSubType = "";
			List<DocumentHandler> invoiceList;
			Map<String, String> corpOrdIdToSysUserIdMap = new HashMap<String, String>();

			messageSubType = unpackagerDoc.getAttribute("/Proponix/SubHeader/MsgSubType");
			String instrumentId = unpackagerDoc.getAttribute("/Proponix/SubHeader/InstrumentId");
			invoiceList = unpackagerDoc.getFragmentsList("/Proponix/Body/Invoice");
			String customerID = unpackagerDoc.getAttribute("/Proponix/SubHeader/CustomerID");
			String corpOrg = null;
			if (StringFunction.isNotBlank(customerID)) {
				try {
					String sqlStatement = " SELECT ORGANIZATION_OID FROM CORPORATE_ORG WHERE PROPONIX_CUSTOMER_ID = ? AND ACTIVATION_STATUS = ?";
					DocumentHandler resultXML = DatabaseQueryBean.getXmlResultSet(sqlStatement, false, customerID, TradePortalConstants.ACTIVE);
					corpOrg = resultXML.getAttribute("/ResultSetRecord(0)/ORGANIZATION_OID");
				} catch (Exception e) {
					mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE, TradePortalConstants.NO_MATCHING_VALUE_FOUND, "Corporate Organization " + corpOrg);
					e.printStackTrace();
					return outputDoc;
				}
			}
			for (DocumentHandler invoiceDoc : invoiceList) {
				boolean isUpdateLog = false;
				boolean isPayableCred = false;
				String logAction = "";
				String systemUserId = "";
				invoiceID = invoiceDoc.getAttribute("/InvoiceId");
				invoiceOTLUoid = invoiceDoc.getAttribute("/InvoiceOTLUoid");
				if (TradePortalConstants.INDICATOR_YES.equals(invoiceDoc.getAttribute("/CreditNoteInd"))) {
					isPayableCred = true;
				}
				StringBuilder sqlStatement = new StringBuilder("");
				if (isPayableCred) {
					sqlStatement.append("SELECT count(*) the_count, max(upload_credit_note_oid) the_oid FROM credit_notes");
				} else {
					sqlStatement.append("SELECT count(*) the_count, max(invoice_oid) the_oid FROM invoice");
				}
				sqlStatement.append(" WHERE otl_invoice_uoid = ? and a_corp_org_oid = ?");
				DocumentHandler resultXML = DatabaseQueryBean.getXmlResultSet(sqlStatement.toString(), true, new Object[] { invoiceOTLUoid, corpOrg });
				int theCount = resultXML.getAttributeInt("/ResultSetRecord(0)/THE_COUNT");

				if (theCount > 1) {
					mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE, TradePortalConstants.MORE_THAN_ONE_VALUE_FOUND, "Invoice with otl_invoice_uoid = " + invoiceOTLUoid);
					continue;
				}
				if (theCount == 0) {
					mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE, TradePortalConstants.NO_MATCHING_VALUE_FOUND, "Invoice with otl_invoice_uoid = " + invoiceOTLUoid);
					continue;
				}
				String invoiceOid = resultXML.getAttribute("/ResultSetRecord(0)/THE_OID");
				// Nar CR914A Release9.2 01/21/2015 Begin
				if (isPayableCred) {
					setPayableCreditNoteAttribute(invoiceDoc, invoiceOid, corpOrdIdToSysUserIdMap, mediatorServices);
				}
				// Nar CR914A Release9.2 01/21/2015 End
				else {
					Invoice invoice = (Invoice) mediatorServices.createServerEJB("Invoice");
					invoice.getData(Long.parseLong(invoiceOid));

					invoiceStatus = invoiceDoc.getAttribute("/InvoiceStat");
					if (StringFunction.isNotBlank(invoiceStatus)) {
						invoice.setAttribute("invoice_status", invoiceStatus);
					}
					// CR999 start
					if (StringFunction.isNotBlank(invoiceDoc.getAttribute("/EndToEndID"))) {
						invoice.setAttribute("end_to_end_id", invoiceDoc.getAttribute("/EndToEndID"));
					}
					// CR999 end

					if (messageSubType.equals(TradePortalConstants.MSG_SUBTYPE_FINANCEREQUESTED)) {
						invoice.setAttribute("finance_status", TradePortalConstants.INVOICE_APPROVED_FOR_FINANCE);
					} else if (messageSubType.equals(TradePortalConstants.MSG_SUBTYPE_CREDITNOTE)) {
						invoice.setAttribute("invoice_credit_note_amount", invoiceDoc.getAttribute("/EventAmt"));
						invoice.setAttribute("invoice_outstanding_amount", invoiceDoc.getAttribute("/OutstndAmt"));// IR
																													// VAUJ012765910
					} else if (messageSubType.equals(TradePortalConstants.MSG_SUBTYPE_CREATEUSANCE)) {

						invoice.setAttribute("finance_percentage", invoiceDoc.getAttribute("/FinPct"));
						invoice.setAttribute("finance_amount", invoiceDoc.getAttribute("/EventAmt"));
						invoice.setAttribute("invoice_outstanding_amount", invoiceDoc.getAttribute("/OutstndAmt"));// IR
																													// VAUJ012765910
						invoice.setAttribute("finance_status", TradePortalConstants.INVOICE_FINANCED); // IR RUJ020963611

						// Ravindra - Rel800 CR710 - 20th Dec 2011 - Start
						if (TradePortalConstants.INDICATOR_YES.equals(invoice.getAttribute("portal_originated_ind"))) {
							/**
							 * Uploaded Invoices database record has to access using the invoice id as the key and should be updated
							 * with as status of �Financed�, only if the Portal Originated indicator = Y
							 */

							String uploadInvSql = " SELECT count(*) count, max(upload_invoice_oid) oid FROM invoices_summary_data "
									+ " WHERE invoice_id = ? AND a_corp_org_oid = ? ";
							DocumentHandler resultDocXML = DatabaseQueryBean.getXmlResultSet(uploadInvSql, true, new Object[] { invoiceID, corpOrg });
							int count = resultDocXML.getAttributeInt("/ResultSetRecord(0)/COUNT");
							if (count > 1) {
								mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,
										TradePortalConstants.MORE_THAN_ONE_VALUE_FOUND,
										"Invoice in Invoices Summary Data with Invoice ID:= " + invoiceID);
							} /*
								 * else if ( count == 0 ) {
								 * mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,
								 * TradePortalConstants.NO_MATCHING_VALUE_FOUND,
								 * "Invoice in Invoices Summary Data with Invoice ID:= " + invoiceID); }
								 */
							else if (count == 1) {
								String oid = resultDocXML.getAttribute("/ResultSetRecord(0)/OID");
								InvoicesSummaryData invoicesSummaryData = (InvoicesSummaryData) mediatorServices.createServerEJB("InvoicesSummaryData", Long.parseLong(oid));
								invoicesSummaryData.setAttribute("invoice_status", TradePortalConstants.UPLOAD_INV_STATUS_FINANCED);
								if (invoicesSummaryData.save() < 0) {
									mediatorServices.getErrorManager().addErrors(invoicesSummaryData.getErrorManager().getIssuedErrorsList());
								}
								mediatorServices.removeServerEJB(invoicesSummaryData);// T36000031888
							}
						}

						// WZhu Yogesh 2/25/13 Rel 8.2 CR-708B
						String supplierPortalInvoiceStatus = invoiceDoc.getAttribute("/SupplierPortalInvoiceStatus");
						if (!StringFunction.isBlank(supplierPortalInvoiceStatus)) {
							invoice.setAttribute("supplier_portal_invoice_status", supplierPortalInvoiceStatus);
							if (TradePortalConstants.SP_STATUS_DECLINTED_BY_BANK.equals(supplierPortalInvoiceStatus)) {
								invoice.setAttribute("net_amount_offered", null);
							}
							if (supplierPortalInvoiceStatus.equals(TradePortalConstants.SP_STATUS_PROCESSED_BY_BANK)) {
								invoice.setAttribute("instrument_id", instrumentId);
								invoice.updateAssociatedInvoiceOfferGroup();
							}
							String unlinkReasonText = invoiceDoc.getAttribute("/UnlinkReasonText");
							invoice.setAttribute("rejection_reason_text", unlinkReasonText);

							// If the invoice is rejected by bank, it needs to
							// be regrouped to be availble to the seller again.
							if (supplierPortalInvoiceStatus.equals(TradePortalConstants.SP_STATUS_REJECTED_BY_BANK)) {
								invoice.setAttribute("invoice_offer_group_oid", null);
								invoice.calculateInvoiceOfferNetAmountOffered();
								invoice.updateAssociatedInvoiceOfferGroup();
							}
							logAction = SUPP_PORTAL_INVOICE_STATUS_TO_ACTION_MAP.get(supplierPortalInvoiceStatus);
							String corpOrgOid = invoice.getAttribute("corp_org_oid");
							systemUserId = corpOrdIdToSysUserIdMap.get(corpOrgOid);
							if (systemUserId == null) {
								systemUserId = getSystemUserIdFromCorpOrg(corpOrgOid, mediatorServices);
								corpOrdIdToSysUserIdMap.put(corpOrgOid, systemUserId);
							}
							isUpdateLog = true;
						}
					} else if ((messageSubType.equals(TradePortalConstants.MSG_SUBTYPE_AUTOMATCH))
							|| (messageSubType.equals(TradePortalConstants.MSG_SUBTYPE_MANUALMATCH))) {

						invoice.setAttribute("pending_payment_amount", invoiceDoc.getAttribute("/TotPendPayAmt"));
						invoice.setAttribute("invoice_outstanding_amount", invoiceDoc.getAttribute("/OutstndAmt"));// IR
																													// VAUJ012765910

					} else if (messageSubType.equals(TradePortalConstants.MSG_SUBTYPE_APPLIEDPAYMENT)) {
						invoice.setAttribute("pending_payment_amount", invoiceDoc.getAttribute("/TotPendPayAmt"));
						invoice.setAttribute("closed_indicator", TradePortalConstants.INDICATOR_YES);
						invoice.setAttribute("invoice_payment_amount", invoiceDoc.getAttribute("/EventAmt"));
						invoice.setAttribute("invoice_outstanding_amount", invoiceDoc.getAttribute("/OutstndAmt"));// IR
																													// VAUJ012765910

					} else if (messageSubType.equals(TradePortalConstants.MSG_SUBTYPE_LIQUIDATE)) {

						invoice.setAttribute("finance_amount", invoiceDoc.getAttribute("/EventAmt"));
						invoice.setAttribute("invoice_outstanding_amount", invoiceDoc.getAttribute("/OutstndAmt"));// IR
																													// VAUJ012765910
						if (TradePortalConstants.INVOICE_STATUS_CLOSED.equals(invoiceDoc.getAttribute("/InvoiceStat"))) // IR
																														// RUJ020963611
							invoice.setAttribute("finance_status", TradePortalConstants.INVOICE_REPAID_LIQUIDATE);

					} else if (messageSubType.equals(TradePortalConstants.MSG_SUBTYPE_CLOSED)) {
						invoice.setAttribute("closed_indicator", TradePortalConstants.INDICATOR_YES);
						invoice.setAttribute("invoice_outstanding_amount", "0"); // IR UMUJ030240243

					} else if (messageSubType.equals(TradePortalConstants.MSG_SUBTYPE_PPY)) {
						invoice.setAttribute("rejection_reason_text", invoiceDoc.getAttribute("/UnlinkReasonText"));
						// Nar CR-914A Rel9.2.0.0 03/12/2015 Begin
						// If creditNoteAppliedAmount tag is not blank, then it means Applied amount has been changed TPS.
						if (StringFunction.isNotBlank(invoiceDoc.getAttribute("/CreditNoteAmountApplied"))) {
							invoice.updateCreidtNoteAppliedAmout(invoiceDoc);
						}
						// Nar CR-914A Rel9.2.0.0 03/12/2015 End
						String supplierPortalInvoiceStatus = invoiceDoc.getAttribute("/SupplierPortalInvoiceStatus");
						if (TradePortalConstants.SP_STATUS_INVOICE_PAID.equals(supplierPortalInvoiceStatus)) {
							invoice.setAttribute("supplier_portal_invoice_status", supplierPortalInvoiceStatus);
							invoice.setAttribute("net_amount_offered", null);
							setNetPaidAmount(invoice);
							invoice.updateAssociatedInvoiceOfferGroup();
						}
					}

					// CR913 Start - update supplier portal status ( for CUS we are already doing it above)
					String supplierPortalInvoiceStatus = invoiceDoc.getAttribute("/SupplierPortalInvoiceStatus");
					if (StringFunction.isNotBlank(supplierPortalInvoiceStatus) && !messageSubType.equals(TradePortalConstants.MSG_SUBTYPE_CREATEUSANCE)) {
						invoice.setAttribute("supplier_portal_invoice_status", supplierPortalInvoiceStatus);
					}
					// CR913 End

					int returnValue = invoice.save();

					if (returnValue < 0) {
						mediatorServices.getErrorManager().addErrors(invoice.getErrorManager().getIssuedErrorsList());
					} else if (isUpdateLog) {
						InvoiceUtility.createInvoiceHistory(invoice.getAttribute("invoice_oid"), supplierPortalInvoiceStatus,
								logAction, systemUserId, "", mediatorServices);
					}
					if ("Y".equals(invstoRemoveEJB)) { // T36000031888
						mediatorServices.removeServerEJB(invoice);
					}
				}
			}
		} catch (AmsException e) {
			mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE, TradePortalConstants.TASK_NOT_SUCCESSFUL, "Message Unpackaging");
			e.printStackTrace();
		} catch (Exception e) {
			mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE, TradePortalConstants.TASK_NOT_SUCCESSFUL, "Message Unpackaging");
			e.printStackTrace();
		}
		return outputDoc;
	}

	/**
	 * This method is used to set Payable credit note attribute.
	 * 
	 * @param invoice
	 * @param invoiceDoc
	 * @throws Exception
	 */
	private void setPayableCreditNoteAttribute(DocumentHandler invoiceDoc, String creditNoteOid,
			Map<String, String> corpOrdIdToSysUserIdMap, MediatorServices mediatorServices) throws Exception {

		CreditNotes creditNote = (CreditNotes) mediatorServices.createServerEJB("CreditNotes", Long.parseLong(creditNoteOid));
		creditNote.setPayableCreditNoteData(invoiceDoc);
		int returnValue = creditNote.save();
		if (returnValue > 0) {
			String corpOrgOid = creditNote.getAttribute("corp_org_oid");
			String creditNoteStatus = invoiceDoc.getAttribute("/InvoiceStat");
			if (StringFunction.isNotBlank(invoiceDoc.getAttribute("/InvoiceProcessingStatus"))) {
				creditNoteStatus = invoiceDoc.getAttribute("/InvoiceProcessingStatus");
			}
			String systemUserId = corpOrdIdToSysUserIdMap.get(corpOrgOid);
			if (systemUserId == null) {
				systemUserId = getSystemUserIdFromCorpOrg(corpOrgOid, mediatorServices);
				corpOrdIdToSysUserIdMap.put(corpOrgOid, systemUserId);
			}
			InvoiceUtility.createInvoiceHistory(creditNote.getAttribute("upload_credit_note_oid"), creditNoteStatus,
					CREDIT_NOTE_STATUS_TO_ACTION_MAP.get(creditNoteStatus), systemUserId, "", mediatorServices);
		} else {
			mediatorServices.getErrorManager().addErrors(creditNote.getErrorManager().getIssuedErrorsList());
		}
		mediatorServices.removeServerEJB(creditNote);// T36000031888

	}

	private String getSystemUserIdFromCorpOrg(String corpOrgOid, MediatorServices mediatorServices)
			throws Exception, RemoteException, AmsException {
		CorporateOrganization corpOrg = (CorporateOrganization) mediatorServices.createServerEJB("CorporateOrganization", Long.parseLong(corpOrgOid));
		String systemUserOid = corpOrg.getAttribute("upload_system_user_oid");
		if (systemUserOid == null) {
			systemUserOid = "";
		}
		mediatorServices.removeServerEJB(corpOrg); // T36000031888
		return systemUserOid;
	}

	private void setNetPaidAmount(Invoice invoice) throws RemoteException, AmsException {
		String payAmt = invoice.getAttribute("invoice_payment_amount");
		String invAmount = (StringFunction.isNotBlank(payAmt) && new BigDecimal(payAmt).compareTo(BigDecimal.ZERO) != 0) ? payAmt : invoice.getAttribute("invoice_total_amount");
		BigDecimal invoiceAmount = new BigDecimal(invAmount);
		invoiceAmount = invoiceAmount.subtract(invoice.getAttributeDecimal("total_credit_note_amount"));
		invoice.setAttribute("net_amount_paid", invoiceAmount.toString());
	}
}
