package com.ams.tradeportal.mediator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class is used for adding one or more PO line items to an Import LC
 * Issue or Amend transaction. If PO line items are being assigned to a
 * transaction that previously had no PO line items, the class will ensure
 * that all of the selected PO line items have the same PO upload definition
 * oid, beneficiary name, and currency. If all selected PO line items are
 * valid, the transaction's amount, last shipment date, expiry date, and PO
 * line items description are updated accordingly.
 *

 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */

import com.amsinc.ecsg.util.*;

import java.rmi.*;
import java.util.*;
import java.text.*;

import com.amsinc.ecsg.frame.*;
import com.ams.tradeportal.busobj.*;
import com.ams.tradeportal.common.*;
import com.ams.tradeportal.busobj.util.*;

public class AddPOLineItemsMediatorBean extends MediatorBean {
	private static final Logger LOG = LoggerFactory.getLogger(AddPOLineItemsMediatorBean.class);
	// The following variables have been declared transient because the mediator
	// is stateless and they are initialized in only one place.
	private transient MediatorServices mediatorServices = null;
	private transient Transaction transaction = null;
	private transient Instrument instrument = null;
	private transient boolean hasPOLineItems = false;
	private transient String uploadDefinitionOid = null;
	private transient String beneficiaryName = null;
	private transient String currency = null;
	private transient Vector poLineItemOidsList = null;
	private transient String poLineItemOidsSql = null;
	private transient Terms terms = null;
	private transient int numberOfPOLineItems = 0;
	private transient int numberOfCurrencyPlaces = 0;
	private transient long shipmentTermsOid = 0;

	private transient String userLocale = null;

	/**
     * This method performs addition of PO Line Items. The format expected from the input doc is:
     *
     * <POLineItemList>
     *	<POLineItem ID="0"><poLineItemOid>1000001</POLineItem>
     *	<POLineItem ID="1"><poLineItemOid>1000002</POLineItem>
     *	<POLineItem ID="2"><poLineItemOid>1000003</POLineItem>
     *	<POLineItem ID="3"><poLineItemOid>1000004</POLineItem>
     *	<POLineItem ID="6"><poLineItemOid>1000001</POLineItem>
     * </POLineItemList>
	 *
	 * This is a non-transactional mediator that executes transactional methods in business objects
	 *
	 * @param inputDoc
	 *            The input XML document (&lt;In&gt; section of overall Mediator XML document)
	 * @param outputDoc
	 *            The output XML document (&lt;Out&gt; section of overall Mediator XML document)
	 * @param newMediatorServices
	 *            Associated class that contains lots of the auxillary functionality for a mediator.
	 */
	public DocumentHandler execute(DocumentHandler inputDoc, DocumentHandler outputDoc, MediatorServices newMediatorServices)
			throws RemoteException, AmsException {
		DocumentHandler poLineItemDoc = null;

		mediatorServices = newMediatorServices;
		Vector poLineItemsList = inputDoc.getFragments("/POLineItemList/POLineItem");
		long transactionOid = inputDoc.getAttributeLong("/Transaction/transactionOid");
		shipmentTermsOid = inputDoc.getAttributeLong("/Terms/ShipmentTermsList/shipment_oid");
		hasPOLineItems = Boolean.valueOf(inputDoc.getAttribute("../hasPOLineItems")).booleanValue();
		numberOfPOLineItems = poLineItemsList.size();

		userLocale = mediatorServices.getCSDB().getLocaleName();

		// if the user locale not specified default to en_US
		if ((userLocale == null) || (userLocale.equals(""))) {
			userLocale = "en_US";
		}

		// TLE - 09/28/06 - CR381 - Add Begin
		String buttonPressed = inputDoc.getAttribute("/Update/ButtonPressed");

		if (buttonPressed.equals(TradePortalConstants.BUTTON_FILTER)) {
			return outputDoc;
		}
		// TLE - 09/28/06 - CR381 - Add End

		// If nothing was selected by the user, issue an error indicating this and return
		if (numberOfPOLineItems == 0) {
			String substitutionParm = mediatorServices.getResourceManager().getText("POLineItemAction.Add", TradePortalConstants.TEXT_BUNDLE);

			mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.NO_ITEM_SELECTED, substitutionParm);

			addPOLineItems(transactionOid, outputDoc);
			return outputDoc;
		}

		// PHILC - CR380
		// Get a list of all the PO line item oids that were selected
		String multiplePOLineItems = inputDoc.getAttribute("/Transaction/multiplePOLineItems");

		poLineItemOidsList = POLineItemUtility.getPOLineItemOids(poLineItemsList, multiplePOLineItems, TradePortalConstants.PO_ADD);
		// Leelavathi IR#T36000016913 30/07/2013 Begin

		if (multiplePOLineItems.equals(TradePortalConstants.INDICATOR_YES))
			// Leelavathi IR#T36000016913 30/07/2013 End
			numberOfPOLineItems = poLineItemOidsList.size();
		// PHILC - CR380

		// Build the SQL to use in numerous queries throughout the mediator
		String requiredOid = "po_line_item_oid";// SHR CR708 Rel8.1.1
		poLineItemOidsSql = POLineItemUtility.getPOLineItemOidsSql(poLineItemOidsList, requiredOid);
		if (poLineItemOidsList != null && poLineItemOidsList.size() > 0) {
			long poLineItemOid = Long.parseLong((String) poLineItemOidsList.elementAt(0));
			POLineItem poLineItem = (POLineItem) mediatorServices.createServerEJB("POLineItem", poLineItemOid);

			uploadDefinitionOid = poLineItem.getAttribute("source_upload_definition_oid");
			beneficiaryName = poLineItem.getAttribute("ben_name");
			currency = poLineItem.getAttribute("currency");
		}
		// If PO line items don't currently exist for the transaction, make sure that all of
		// the PO line items selected by the user have the same PO upload definition,
		// beneficiary, and currency; if they don't, issue an error indicating this. On the
		// other hand, if the transaction *does* currently have PO line items assigned to it,
		// we don't need to validate anything since it has already been done in the listview
		// (i.e., the listview ensures that these fields match the corresponding fields for
		// the transaction).
		// Leelavathi IR#T36000016913 30/07/2013 Begin
		if (!hasPOLineItems) {
			if (validatePOLineItems(multiplePOLineItems) == false) {
				mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.PO_LINE_ITEM_MISMATCH);
				return outputDoc;
			}
		}

		// Leelavathi IR#T36000016913 30/07/2013 End
		try {
			transaction = (Transaction) mediatorServices.createServerEJB("Transaction", transactionOid);

			// Needs to instantiate instrument object for updating amendment transaction
			String transactionType = transaction.getAttribute("transaction_type_code");
			if (transactionType.equals(TransactionType.AMEND)) {
				long instrumentOid = transaction.getAttributeLong("instrument_oid");
				instrument = (Instrument) mediatorServices.createServerEJB("Instrument", instrumentOid);
			}

			addPOLineItems(poLineItemOidsList, outputDoc);
		} catch (Exception ex) {
			LOG.error("Exception occured in AddPOLineItemsMediator: " , ex);
		}

		// If the transaction previously did not have PO line items assigned to it, set
		// the PO upload definition oid, beneficiary, and currency in the xml doc so that
		// the jsp loads up the correct data in the listview (otherwise, the user could
		// potentially add PO line items that don't have matching PO upload definitions,
		// beneficiaries, and currencies).
		if (!hasPOLineItems) {
			outputDoc.setAttribute("/Transaction/uploadDefinitionOid", uploadDefinitionOid);
			outputDoc.setAttribute("../beneficiaryName", beneficiaryName);
			outputDoc.setAttribute("../currency", currency);
		}

		return outputDoc;
	}

	/**
	 * This method determines whether or not the PO upload definition oid, beneficiary name, and currency are the same for all PO
	 * line items selected by the user.
	 *
	 * @exception com.amsinc.ecsg.frame.AmsException
	 * @exception java.rmi.RemoteException
     * @return     boolean - indicates whether or not validation was successful
     *                       (true  - the PO upload definition oid, beneficiary name, and currency are the same for all
     *                                selected PO line items
     *                        false - the PO upload definition oid, beneficiary name, and currency are not the same for
     *                                all selected PO line items)
	 */
	private boolean validatePOLineItems(String multiplePOLineItems) throws AmsException, RemoteException {

		long poLineItemOid = Long.parseLong((String) poLineItemOidsList.elementAt(0));
		POLineItem poLineItem = (POLineItem) mediatorServices.createServerEJB("POLineItem", poLineItemOid);

		uploadDefinitionOid = poLineItem.getAttribute("source_upload_definition_oid");
		beneficiaryName = poLineItem.getAttribute("ben_name");
		currency = poLineItem.getAttribute("currency");

		String whereClause = poLineItemOidsSql + " and a_source_upload_definition_oid = ? and ben_name = ? and currency = ?";

		int validationTotal = DatabaseQueryBean.getCount("po_line_item_oid", "po_line_item", whereClause, false, uploadDefinitionOid, beneficiaryName, currency);

		return (validationTotal == numberOfPOLineItems);
	}

	/**
	 * This method keeps the previously added PO line items by the user to the transaction he/she is currently viewing when error
	 * comes up if user has not selected any PO and still hit the add selected items button.
	 *
	 * @param long
	 *            transactionOid
	 * @param DocumentHandler
	 *            - the mediator's output doc
	 * @exception com.amsinc.ecsg.frame.AmsException
	 * @exception java.rmi.RemoteException
	 * @return void
	 */
	private void addPOLineItems(long transactionOid, DocumentHandler outputDoc) throws AmsException, RemoteException {
		transaction = (Transaction) mediatorServices.createServerEJB("Transaction", transactionOid);

		// Get the terms associated with the current transaction and its shipment terms
		terms = (Terms) transaction.getComponentHandle("CustomerEnteredTerms");
		ComponentList shipmentTermsList = (ComponentList) terms.getComponentHandle("ShipmentTermsList");

		ShipmentTerms shipmentTerms = (ShipmentTerms) shipmentTermsList.getComponentObject(shipmentTermsOid);

		// Get data from Terms and set it in the Out section of the XML
		// That way. when the user is returned to the page of transaction, the updated data will
		// appear along with other data that they entered
		outputDoc.setAttribute("/ChangePOs/po_line_items", shipmentTerms.getAttribute("po_line_items"));
		outputDoc.setAttribute("/ChangePOs/goods_description", shipmentTerms.getAttribute("goods_description"));
		outputDoc.setAttribute("/ChangePOs/amount", terms.getAttribute("amount"));
		outputDoc.setAttribute("/ChangePOs/amount_currency_code", terms.getAttribute("amount_currency_code"));

		outputDoc.setAttribute("/ChangePOs/name", beneficiaryName);

		// Convert date formats so that the Out section of the XML is properly updated
		SimpleDateFormat iso = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
		SimpleDateFormat jPylon = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");

		try {
			if (StringFunction.isNotBlank(shipmentTerms.getAttribute("latest_shipment_date"))) {
				Date date = jPylon.parse(shipmentTerms.getAttribute("latest_shipment_date"));
				outputDoc.setAttribute("/ChangePOs/latest_shipment_date", iso.format(date));
			}

			if (StringFunction.isNotBlank(terms.getAttribute("expiry_date"))) {
				Date date = jPylon.parse(terms.getAttribute("expiry_date"));
				outputDoc.setAttribute("/ChangePOs/expiry_date", iso.format(date));
			}
		} catch (Exception ex) {
            LOG.error("Exception occured in AddPOLineItemsMediator:addPOLineItems method " , ex);
		}
	}

	/**
	 * This method adds the PO line items selected by the user to the transaction that he/she is currently viewing. It does this by
	 * setting each PO line item's 'assigned_to_trans_oid' attribute and saving. After all PO line items have been saved, the
	 * amount, date, and PO line items description fields are all re-derived to reflect the new item values. An error is issued if
	 * the updated length of the Goods Description field exceeds 6500 characters.
	 *

	 * @param java.util.Vector
	 *            poLineItemOidsList - the list of PO line item oids
	 * @param DocumentHandler
	 *            - the mediator's output doc
	 * @exception com.amsinc.ecsg.frame.AmsException
	 * @exception java.rmi.RemoteException
	 * @return void
	 */
	private void addPOLineItems(Vector poLineItemOidsList, DocumentHandler outputDoc) throws AmsException, RemoteException {
		Vector shipmentPOsList = new Vector(10);

		// First, get the transaction type to determine whether we should update the
		// PO line items' auto_added_to_amend_ind if necssary (for Amend transactions)
		String transactionType = transaction.getAttribute("transaction_type_code");
		String transactionOid = transaction.getAttribute("transaction_oid");
		String instrumentOid = transaction.getAttribute("instrument_oid");

		// Update the assigned to transaction indicators for each PO line item being
		// added to the transaction
		POLineItem poLineItem = null;
		long poLineItemOid = 0;
		for (int i = 0; i < numberOfPOLineItems; i++) {
			poLineItemOid = Long.parseLong((String) poLineItemOidsList.elementAt(i));
			poLineItem = (POLineItem) mediatorServices.createServerEJB("POLineItem", poLineItemOid);

			if (transactionType.equals(TransactionType.AMEND)) {
				poLineItem.setAttribute("auto_added_to_amend_ind", TradePortalConstants.INDICATOR_NO);
			}

			poLineItem.setAttribute("assigned_to_trans_oid", transactionOid);
			poLineItem.setAttribute("active_for_instrument", instrumentOid);
			poLineItem.setAttribute("shipment_oid", String.valueOf(shipmentTermsOid));
			poLineItem.save();
			shipmentPOsList.addElement(String.valueOf(poLineItemOid));
			poLineItem = null;
		}

		// Add the POs already associated to the shipment to shipmentPOsList
		// These are used in the derive process later
		String alreadyAssociatedSql = "select po_line_item_oid from po_line_item where p_shipment_oid = ?";
		DocumentHandler resultSet = DatabaseQueryBean.getXmlResultSet(alreadyAssociatedSql, false, new Object[] { shipmentTermsOid });

		if (resultSet == null)
			resultSet = new DocumentHandler();

		List<DocumentHandler> alreadyAssociatedList = resultSet.getFragmentsList("/ResultSetRecord");
		for (DocumentHandler alreadyAssociatedPO : alreadyAssociatedList) {
			String poOid = alreadyAssociatedPO.getAttribute("/PO_LINE_ITEM_OID");
			shipmentPOsList.addElement(poOid);
		}

		// Get the terms associated with the current transaction and its shipment terms
		terms = (Terms) transaction.getComponentHandle("CustomerEnteredTerms");
		ComponentList shipmentTermsList = (ComponentList) terms.getComponentHandle("ShipmentTermsList");
		int numShipments = shipmentTermsList.getObjectCount();

		ShipmentTerms shipmentTerms = (ShipmentTerms) shipmentTermsList.getComponentObject(shipmentTermsOid);

		// Derive the amounts (Transaction.amount etc), dates (Terms.expirty_date,
		// ShipmentTerms.latest_shipment_date etc) and ShipmentTerms.po_line_item.
		// Note all the PO Line Items have been saved into database by now.
		// Add phrase "The following Purchase Order items have been added" to ShipmentTerms.goods_description.
		if (transactionType.equals(TransactionType.ISSUE)) {
			// <Amit - IR EEUE032659500 -05/09/2005 >
			// logu & amit IR EEUE032659500 -06/30/2005
			boolean isStructuredPO = false;
			POLineItemUtility.deriveTransactionDataFromPO(isStructuredPO, transaction, shipmentTerms, numShipments, shipmentPOsList,
					Long.parseLong(uploadDefinitionOid), getSessionContext(), userLocale);
			// </Amit - IR EEUE032659500 -05/09/2005 >
		}
		if (transactionType.equals(TransactionType.AMEND)) {

			// <Amit - IR EEUE032659500 -05/09/2005 >
			// logu & amit IR EEUE032659500 -06/30/2005
			POLineItemUtility.deriveAmendmentTransactionDataFromPOLocale(false, transaction, shipmentTerms, numShipments,
					shipmentPOsList, Long.parseLong(uploadDefinitionOid), getSessionContext(), instrument, null, userLocale);
			// </Amit - IR EEUE032659500 -05/09/2005 >
			String goodsDescription = shipmentTerms.getAttribute("goods_description");

			String textToAdd = mediatorServices.getResourceManager().getText("AddPOLineItems.POItemsAdded", TradePortalConstants.TEXT_BUNDLE);
			int index = StringFunction.isNotBlank(goodsDescription) ? goodsDescription.indexOf(textToAdd) : -1;

			if (index == -1) {
				if (StringFunction.isNotBlank(goodsDescription)) {
					goodsDescription += "\n\n";
				}

				goodsDescription += textToAdd;

				shipmentTerms.setAttribute("goods_description", goodsDescription);

				if (goodsDescription.length() > 6500) {
					String endText = goodsDescription.substring(6480, 6500);
					String alias = mediatorServices.getResourceManager().getText("ShipmentTermsBeanAlias.goods_description", TradePortalConstants.TEXT_BUNDLE);

					mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.TEXT_TOO_LONG_SHOULD_END, alias, endText);
					return;
				}
			}

		}

		// Get data from Terms and set it in the Out section of the XML
		// That way. when the user is returned to the page of transaction, the updated data will
		// appear along with other data that they entered
		outputDoc.setAttribute("/ChangePOs/po_line_items", shipmentTerms.getAttribute("po_line_items"));
		outputDoc.setAttribute("/ChangePOs/goods_description", shipmentTerms.getAttribute("goods_description"));
		outputDoc.setAttribute("/ChangePOs/amount", terms.getAttribute("amount"));
		outputDoc.setAttribute("/ChangePOs/amount_currency_code", terms.getAttribute("amount_currency_code"));

		outputDoc.setAttribute("/ChangePOs/name", beneficiaryName);

		// Convert date formats so that the Out section of the XML is properly updated
		SimpleDateFormat iso = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
		SimpleDateFormat jPylon = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");

		try {
			if (StringFunction.isNotBlank(shipmentTerms.getAttribute("latest_shipment_date"))) {
				Date date = jPylon.parse(shipmentTerms.getAttribute("latest_shipment_date"));
				outputDoc.setAttribute("/ChangePOs/latest_shipment_date", iso.format(date));
			}

			if (StringFunction.isNotBlank(terms.getAttribute("expiry_date"))) {
				Date date = jPylon.parse(terms.getAttribute("expiry_date"));
				outputDoc.setAttribute("/ChangePOs/expiry_date", iso.format(date));
			}
		} catch (Exception ex) {
            LOG.error("Exception occured in AddPOLineItemsMediator:addPOLineItems method " , ex);

		}
		mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
				TradePortalConstants.ADD_PO_LINE_ITEMS_SUCCESS, String.valueOf(numberOfPOLineItems));

		if (!StringFunction.isBlank(terms.getAttribute("c_FirstTermsParty"))) {
			TermsParty firstTermsParty = (TermsParty) terms.getComponentHandle("FirstTermsParty");
			firstTermsParty.setAttribute("name", beneficiaryName);
		}

		transaction.save(false);
	}

}
