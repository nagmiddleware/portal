package com.ams.tradeportal.mediator;

import javax.ejb.*;
import java.rmi.*;

import com.amsinc.ecsg.frame.*;

/*
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface AddPOLineItemsMediatorHome extends javax.ejb.EJBHome
{

  public AddPOLineItemsMediator create()
		throws CreateException, RemoteException;}