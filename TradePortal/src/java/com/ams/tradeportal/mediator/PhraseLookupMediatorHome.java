package com.ams.tradeportal.mediator;

import javax.ejb.*;
import java.rmi.*;

/*
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface PhraseLookupMediatorHome extends javax.ejb.EJBHome
{

  public PhraseLookupMediator create()
		throws CreateException, RemoteException;}