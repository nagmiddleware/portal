package com.ams.tradeportal.mediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class is used for removing one or more PO line items from an Import LC
 * Issue or Amend transaction. Once the PO line items have been removed from a
 * transaction, the method will update the transaction's amount, last shipment
 * date, expiry date, and PO line items description accordingly.
 *

 */

import com.amsinc.ecsg.util.*;

import javax.ejb.*;
import java.rmi.*;

import javax.naming.*;
import javax.ejb.*;
import java.util.*;
import java.math.*;
import java.text.*;

import com.amsinc.ecsg.frame.*;
import com.ams.tradeportal.busobj.*;
import com.ams.tradeportal.common.*;
import com.ams.tradeportal.busobj.util.*;

/*
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class RemovePOLineItemsMediatorBean extends MediatorBean
{
private static final Logger LOG = LoggerFactory.getLogger(RemovePOLineItemsMediatorBean.class);
	// logu & amit  IR EEUE032659500 -06/30/2005
	private transient String 			userLocale 			= null;
	// END logu & amit  IR EEUE032659500 -06/30/2005

	// The following variables have been declared transient because the mediator
	// is stateless and they are initialized in only one place.
	private transient MediatorServices   mediatorServices    = null;
	private transient Transaction        transaction         = null;
	private transient Instrument         instrument          = null;
	private transient String             poLineItemOidsSql   = null;
	private transient Vector             poLineItemOidsList  = null;
	private transient String             uploadDefinitionOid = null;
	private transient Terms              terms               = null;
	private transient int                numberOfPOLineItems = 0;
	private transient String             trnxCurrencyCode    = null;	// GGAYLE - 10/16/2003 - IR GOUD101666564
	private transient int                numberOfCurrencyPlaces = 0; // GGAYLE - 10/16/2003 - IR GOUD101666564
	private transient long               shipmentTermsOid    = 0;

	/**
	 * This method performs deletion of PO Line Items
	 * The format expected from the input doc is:
	 *
	 * <POLineItemList>
	 *	<POLineItem ID="0"><poLineItemOid>1000001</POLineItem>
	 *	<POLineItem ID="1"><poLineItemOid>1000002</POLineItem>
	 *	<POLineItem ID="2"><poLineItemOid>1000003</POLineItem>
	 *	<POLineItem ID="3"><poLineItemOid>1000004</POLineItem>
	 *	<POLineItem ID="6"><poLineItemOid>1000001</POLineItem>
	 * </POLineItemList>
	 *
	 * This is a non-transactional mediator that executes transactional
	 * methods in business objects
	 *
	 * @param inputDoc The input XML document (&lt;In&gt; section of overall
	 * Mediator XML document)
	 * @param outputDoc The output XML document (&lt;Out&gt; section of overall
	 * Mediator XML document)
	 * @param mediatorServices Associated class that contains lots of the
	 * auxillary functionality for a mediator.
	 */
	public DocumentHandler execute(DocumentHandler inputDoc, DocumentHandler outputDoc,
			MediatorServices newMediatorServices) throws RemoteException, AmsException
			{
		DocumentHandler   poLineItemDoc       = null;
		String            substitutionParm    = null;
		Vector            poLineItemsList     = null;
		long              transactionOid      = 0;

		mediatorServices    = newMediatorServices;
		poLineItemsList     = inputDoc.getFragments("/POLineItemList/POLineItem");
		transactionOid      = inputDoc.getAttributeLong("/Transaction/transactionOid");
		shipmentTermsOid    = inputDoc.getAttributeLong("/Terms/ShipmentTermsList/shipment_oid");
		// Based on the button clicked, process an appropriate action.
		String buttonPressed = inputDoc.getAttribute("/Update/ButtonPressed");
		mediatorServices.debug("The button pressed is " + buttonPressed);

		numberOfPOLineItems = poLineItemsList.size();

		// logu & amit  IR EEUE032659500 -06/30/2005
		userLocale 		  = mediatorServices.getCSDB().getLocaleName();

		// if the user locale not specified default to en_US
		if((userLocale == null) || (userLocale.equals("")))
		{
			userLocale		  = "en_US";
		}
		//END logu & amit  IR EEUE032659500 -06/30/2005

		// If nothing was selected by the user, issue an error indicating this and return
		if (numberOfPOLineItems <= 0)
		{
			substitutionParm = mediatorServices.getResourceManager().getText("POLineItemAction.Remove",
					TradePortalConstants.TEXT_BUNDLE);

			mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.NO_ITEM_SELECTED, substitutionParm);

			return outputDoc;
		}

		// PHILC - CR380
		// Get a list of all the PO line item oids that were selected
		String deletePOLineItems = TradePortalConstants.INDICATOR_NO;
		deletePOLineItems = inputDoc.getAttribute("/Transaction/deletePOLineItems");
		String multiplePOLineItems = TradePortalConstants.INDICATOR_NO;
		multiplePOLineItems = inputDoc.getAttribute("/Transaction/multiplePOLineItems");
		poLineItemOidsList = POLineItemUtility.getPOLineItemOids(poLineItemsList, multiplePOLineItems, TradePortalConstants.PO_REMOVE);
		if (multiplePOLineItems.equals(TradePortalConstants.INDICATOR_YES))
			numberOfPOLineItems = poLineItemOidsList.size();
		// PHILC - CR380

		// Build the SQL to use in numerous queries throughout the mediator
		String requiredOid = "po_line_item_oid";//SHR CR708 Rel8.1.1 
		poLineItemOidsSql  = POLineItemUtility.getPOLineItemOidsSql(poLineItemOidsList,requiredOid);

        // TLE - 11/21/06 - IR#PNUG110739563 - Begin
		//// PHILC - CR380
		//if (deletePOLineItems.equals(TradePortalConstants.INDICATOR_YES)) {
		//	deletePOLineItems(poLineItemOidsList);
		//} else {
		//// PHILC - CR380
		// TLE - 11/21/06 - IR#PNUG110739563 - End
		try
			{
				transaction = (Transaction) mediatorServices.createServerEJB("Transaction", transactionOid);

				// Needs to instantiate instrument object for updating amendment transaction.
				String transactionType = transaction.getAttribute("transaction_type_code");
				if (transactionType.equals(TransactionType.AMEND))
				{
					long instrumentOid   = transaction.getAttributeLong("instrument_oid");
					instrument = (Instrument)mediatorServices.createServerEJB("Instrument", instrumentOid);
				}

				removePOLineItems(outputDoc);

				//TLE - 11/21/06 - IR#PNUG110739563 - Begin
				if (deletePOLineItems.equals(TradePortalConstants.INDICATOR_YES)) {
					deletePOLineItems(poLineItemOidsList);
				}
				//TLE - 11/21/06 - IR#PNUG110739563 - End

				transaction.save(false);
			}
			catch (Exception e)
			{
				LOG.info("Exception occured in RemovePOLineItemsMediator: " + e.getMessage());
				e.printStackTrace();
			}
		//TLE - 11/21/06 - IR#PNUG110739563 - Begin
		//}
		//TLE - 11/21/06 - IR#PNUG110739563 - End

		return outputDoc;
	}

	/**
	 * This method removes the PO line items selected by the user from the
	 * transaction that he/she is currently viewing. If the PO line items were
	 * uploaded, they are disassociated from the transaction.  If the PO line
	 * items were manually entered, they are deleted. After all PO line items
	 * have been saved, the amount, date, and PO line items description fields
	 * are all re-derived to reflect the removed item values.
	 *
	 * @param      DocumentHandler - the mediator's output doc
	 * @exception  com.amsinc.ecsg.frame.AmsException
	 * @exception  java.rmi.RemoteException
	 * @return     void
	 */
	private void removePOLineItems(DocumentHandler outputDoc) throws AmsException, RemoteException
	{
		POLineItem     poLineItem                   = null;
		String         transactionType              = null;
		String         transactionOid               = null;
		String         whereClause                  = null;
		String         totalAmount                  = null;
		long           poLineItemOid                = 0;
		int            numberOfPOLineItemsRemaining = 0;
		Vector         associatedPoList             = new Vector();

		// First, get the transaction type to determine whether we should update the
		// PO line items' auto_added_to_amend_ind if necssary (for Amend transactions)
		transactionType = transaction.getAttribute("transaction_type_code");
		transactionOid  = transaction.getAttribute("transaction_oid");

		// Get a list of the POs already associated with this instrument
		String alreadyAssociatedSql = "select po_line_item_oid from po_line_item where p_shipment_oid = ?";
		DocumentHandler resultSet = DatabaseQueryBean.getXmlResultSet(alreadyAssociatedSql, true, new Object[]{shipmentTermsOid});

		if (resultSet == null)
			resultSet = new DocumentHandler();

		Vector alreadyAssociatedList = resultSet.getFragments("/ResultSetRecord");
		for (int i=0; i < alreadyAssociatedList.size(); i++)
		{
			String poOid = ((DocumentHandler) alreadyAssociatedList.elementAt(i)).getAttribute("/PO_LINE_ITEM_OID");
			associatedPoList.addElement(poOid);
		}


		// Update the assigned to transaction indicators for each PO line item being
		// added to the transaction
		for (int i = 0; i < numberOfPOLineItems; i++)
		{
			poLineItemOid = Long.valueOf((String) poLineItemOidsList.elementAt(i)).longValue();
			poLineItem    = (POLineItem) mediatorServices.createServerEJB("POLineItem", poLineItemOid);

			uploadDefinitionOid = poLineItem.getAttribute("source_upload_definition_oid");

			// Manually entered POs should be deleted and uploaded POs are just un-associated with the transaction
			if (poLineItem.getAttribute("source_type").equals(TradePortalConstants.PO_SOURCE_MANUAL))
			{
				poLineItem.delete();
			}
			else
			{
				if (transactionType.equals(TransactionType.AMEND))
				{
					poLineItem.setAttribute("previous_po_line_item_oid", null);
					poLineItem.setAttribute("auto_added_to_amend_ind",      TradePortalConstants.INDICATOR_NO);
				}
				poLineItem.setAttribute("assigned_to_trans_oid", null);
				poLineItem.setAttribute("active_for_instrument", null);
				poLineItem.setAttribute("shipment_oid", null);
			}

			associatedPoList.remove(poLineItemOidsList.elementAt(i));
			poLineItem.save();
			poLineItem = null;
		}

		// Get the terms associated with the current transaction
		terms = (Terms) transaction.getComponentHandle("CustomerEnteredTerms");
		ComponentList shipmentTermsList = (ComponentList) terms.getComponentHandle("ShipmentTermsList");
		int numShipments = shipmentTermsList.getObjectCount();

		ShipmentTerms shipmentTerms = (ShipmentTerms)shipmentTermsList.getComponentObject(shipmentTermsOid);

		// Derive the amounts (Transaction.amount etc), dates (Terms.expirty_date,
		// ShipmentTerms.latest_shipment_date etc) and ShipmentTerms.po_line_item.
		// Note all the PO Line Items have been saved into database by now.
		if(transactionType.equals(TransactionType.ISSUE))
		{
			// logu & amit  IR EEUE032659500 -06/30/2005
			POLineItemUtility.deriveTransactionDataFromPO(false,transaction, shipmentTerms, numShipments, associatedPoList, Long.parseLong(uploadDefinitionOid), getSessionContext(),userLocale);
		}
		else
		{
			// logu & amit  IR EEUE032659500 -06/30/2005
			POLineItemUtility.deriveAmendmentTransactionDataFromPOLocale(false,transaction, shipmentTerms, numShipments,
					associatedPoList, Long.parseLong(uploadDefinitionOid), getSessionContext(), instrument, null, userLocale);

		}

		// Get data from Terms and set it in the Out section of the XML
		// That way. when the user is returned to the page, the updated data will
		// appear along with other data that they entered
		outputDoc.setAttribute("/ChangePOs/po_line_items", shipmentTerms.getAttribute("po_line_items"));
		outputDoc.setAttribute("/ChangePOs/goods_description", shipmentTerms.getAttribute("goods_description"));
		outputDoc.setAttribute("/ChangePOs/amount", terms.getAttribute("amount"));
		outputDoc.setAttribute("/ChangePOs/amount_currency_code", terms.getAttribute("amount_currency_code"));

		// Convert date formats so that the Out section of the XML is properly updated
		SimpleDateFormat iso = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
		SimpleDateFormat jPylon = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");

		try
		{
			if(!InstrumentServices.isBlank(shipmentTerms.getAttribute("latest_shipment_date")))
			{
				Date date = jPylon.parse(shipmentTerms.getAttribute("latest_shipment_date"));
				outputDoc.setAttribute("/ChangePOs/latest_shipment_date", iso.format(date));
			}

			if(!InstrumentServices.isBlank(terms.getAttribute("expiry_date")))
			{
				Date date = jPylon.parse(terms.getAttribute("expiry_date"));
				outputDoc.setAttribute("/ChangePOs/expiry_date", iso.format(date));
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

		// Issue an informational message indicating to the user that the selected PO line items
		// have been removed successfully and save the transaction
		mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
				TradePortalConstants.REMOVE_PO_LINE_ITEMS_SUCCESS,
				String.valueOf(numberOfPOLineItems));

	}

	/**
	 * This method performs deletions of purchase order line items
	 * The format expected from the input doc is:
	 *
	 *
	 * This is a non-transactional mediator that executes transactional
	 * methods in business objects
	 *
	 * @param Vector poOidList - List of PO Oids to delete
	 * @return void
	 */

	private void deletePOLineItems(Vector poOidList)
	throws RemoteException, AmsException {

		DocumentHandler purchaseOrderItem = null;
		Vector          poList = null;
		POLineItem      poLineItem = null;
		String          poLineItemOid = null;
		String          poNum = null;
		String          lineNum = null;
		int             count = 0;

		// Populate variables with inputDocument information: purchase ordern list,
		// user oid and security rights.
		count  = poOidList.size();

		mediatorServices.debug("PurchaseOrderMediator: number of po items to delete: " + count);

		if (count <= 0) {
			mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.NO_ITEM_SELECTED,
					mediatorServices.getResourceManager().getText(
							"TransactionAction.Delete",
							TradePortalConstants.TEXT_BUNDLE));
		}

		// Loop through the list of po line item oids, deleting each one.
		for (int i = 0; i < count; i++) {
			poLineItemOid = (String)poOidList.elementAt(i);

			mediatorServices.debug(
					"PurchaseOrderMediator: po line item oid to delete: " + poLineItemOid);

			try {
				// create and load the po line item and call delete
				poLineItem =
					(POLineItem) mediatorServices.createServerEJB(
							"POLineItem",
							Long.parseLong(poLineItemOid));
				poNum = poLineItem.getAttribute("po_num");
				lineNum = poLineItem.getAttribute("item_num");

				poLineItem.delete();

				if (this.isTransSuccess(mediatorServices.getErrorManager())) {
					mediatorServices.getErrorManager().issueError(
							TradePortalConstants.ERR_CAT_1,
							TradePortalConstants.DELETE_SUCCESSFUL,
							poNum + " " + lineNum);
				}
			} finally {
			}
		}
	}

}
