package com.ams.tradeportal.mediator;


import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;

/*
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface GenericIncomingDataUnPackagerMediator extends Mediator{ }
