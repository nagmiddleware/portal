package com.ams.tradeportal.mediator;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;
import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.Vector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.busobj.ArMatchingRule;
import com.ams.tradeportal.busobj.ClientBank;
import com.ams.tradeportal.busobj.CorporateOrganization;
import com.ams.tradeportal.busobj.Instrument;
import com.ams.tradeportal.busobj.InvoiceFileUpload;
import com.ams.tradeportal.busobj.InvoiceGroup;
import com.ams.tradeportal.busobj.InvoicesSummaryData;
import com.ams.tradeportal.busobj.PanelAuthorizationGroup;
import com.ams.tradeportal.busobj.Terms;
import com.ams.tradeportal.busobj.TermsParty;
import com.ams.tradeportal.busobj.Transaction;
import com.ams.tradeportal.busobj.User;
import com.ams.tradeportal.busobj.util.InvoiceUtility;
import com.ams.tradeportal.busobj.util.POLineItemUtility;
import com.ams.tradeportal.busobj.util.PanelAuthProcessor;
import com.ams.tradeportal.busobj.util.PanelAuthUtility;
import com.ams.tradeportal.busobj.util.SecurityAccess;
import com.ams.tradeportal.common.InstrumentType;
import com.ams.tradeportal.common.SQLParamFilter;
import com.ams.tradeportal.common.TPCurrencyUtility;
import com.ams.tradeportal.common.TPDateTimeUtility;
import com.ams.tradeportal.common.TradePortalConstants;
import com.ams.tradeportal.common.TransactionStatus;
import com.ams.tradeportal.common.cache.Cache;
import com.ams.tradeportal.common.cache.TPCacheManager;
import com.amsinc.ecsg.frame.AmsConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.ClientServerDataBridge;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.frame.ErrorManager;
import com.amsinc.ecsg.frame.InvalidObjectIdentifierException;
import com.amsinc.ecsg.frame.IssuedError;
import com.amsinc.ecsg.frame.MediatorBean;
import com.amsinc.ecsg.frame.MediatorServices;
import com.amsinc.ecsg.frame.ReferenceDataManager;
import com.amsinc.ecsg.util.DateTimeUtility;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.EncryptDecrypt;
import com.amsinc.ecsg.util.GMTUtility;
import com.amsinc.ecsg.util.JPylonProperties;
import com.amsinc.ecsg.util.ResourceManager;
import com.amsinc.ecsg.util.StringFunction;
import com.amsinc.ecsg.web.BeanManager;


/*
 *
 *     Copyright  � 2008
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class InvoiceGroupMediatorBean extends MediatorBean {
private static final Logger LOG = LoggerFactory.getLogger(InvoiceGroupMediatorBean.class);

	private transient Transaction transaction = null;
	private transient Terms terms = null;
	private transient Instrument newInstr = null;

	private String SELECT_SQL = "";
	private Set<String> invOIds = new TreeSet<>();
	private boolean loanReqStatus = true;
	  // This additional logic is needed to make sure the rules are sorted in following order always at the top
	  // 1. RULE with max number of INV DATA ITEM Columns defined 
	  // 2. if more than 1 rule has max number of colmns defined then the Rule with widest min max days range is always at the top
	  // This ensure that invoice are matched against rule which are very specific in nature as this rule is the best match for the selected
	  // invoices.
	  // In short we are trying to find a rule which best matches the invoice for grouping. 

	  private static final String EXTRA_SELECT_SQL =" ,  due_or_pay_max_days- due_or_pay_min_days AS daysDiff ,  " +
								       "CASE    WHEN inv_only_create_rule_max_amt IS NULL    THEN 0    ELSE 1  END +  " +
									   "CASE    WHEN due_or_pay_max_days IS NULL    AND due_or_pay_min_days  IS NULL    THEN 0    ELSE 1  END +  " +
									   "CASE    WHEN inv_data_item1 IS NULL    THEN 0    ELSE 1  END +  " +
									   "CASE    WHEN inv_data_item2 IS NULL    THEN 0    ELSE 1  END +  " +
									   "CASE    WHEN inv_data_item3 IS NULL    THEN 0    ELSE 1  END +  " +
									   "CASE    WHEN inv_data_item4 IS NULL    THEN 0    ELSE 1  END +  " +
									   "CASE    WHEN inv_data_item5 IS NULL    THEN 0    ELSE 1  END +  " +
									   "CASE    WHEN inv_data_item6 IS NULL    THEN 0    ELSE 1  END AS colCount,  " +
									   "CASE    WHEN inv_only_create_rule_max_amt IS NULL    THEN 1    ELSE 0    END +  " +
									   "CASE    WHEN due_or_pay_max_days IS NULL    AND due_or_pay_min_days  IS NULL    THEN 1    ELSE 0  END AS tempCount ,  " +
									   "(  CASE    WHEN inv_only_create_rule_max_amt IS NULL    THEN 0    ELSE 1  END +  " +
									   "CASE    WHEN due_or_pay_max_days IS NULL    AND due_or_pay_min_days  IS NULL    THEN 0    ELSE 1  END +  " +
									   "CASE    WHEN inv_data_item1 IS NULL    THEN 0    ELSE 1  END +  " +
									   "CASE    WHEN inv_data_item2 IS NULL    THEN 0    ELSE 1  END +  " +
									   "CASE    WHEN inv_data_item3 IS NULL    THEN 0    ELSE 1  END +  " +
									   "CASE    WHEN inv_data_item4 IS NULL    THEN 0    ELSE 1  END +  " +
									   "CASE    WHEN inv_data_item5 IS NULL    THEN 0    ELSE 1  END +  " +
									   "CASE    WHEN inv_data_item6 IS NULL    THEN 0    ELSE 1  END) - (  " +
									   "CASE     WHEN inv_only_create_rule_max_amt IS NULL    THEN 1    ELSE 0    END +  " +
									   "CASE    WHEN due_or_pay_max_days IS NULL    AND due_or_pay_min_days  IS NULL    THEN 1    ELSE 0  END) as colSort,  " +
									   "CASE    WHEN due_or_pay_max_days IS NULL    AND due_or_pay_min_days  IS NULL    AND inv_only_create_rule_max_amt IS NULL    THEN 0    ELSE 1  END as tCol ";
	private static final String ORDERBY_CLAUSE = " ORDER BY   tCol DESC nulls last, colSort DESC nulls last,  inv_only_create_rule_max_amt DESC nulls last ,  " +
	   										 " daysDiff DESC nulls last,  creation_date_time DESC nulls last ";
	private static final String INVOICE_ORDERBY = "order by buyer_name, currency, invdate, a_upload_definition_oid, amount desc";


	/**
	 * Called by the processRequest() method of the Ancestor class to process the designated
	 * transaction.  The input parameters for the transaction are received in an XML document
	 * (inputDoc).  The execute() method is responsible for calling one or more business objects
	 * to process the transaction and populating the outputDoc XML document with the results
	 * of the transaction.
	 */
	public DocumentHandler execute(DocumentHandler inputDoc, DocumentHandler outputDoc, MediatorServices mediatorServices)
	throws RemoteException, AmsException {
		// Based on the button clicked, process an appropriate action.
		String buttonPressed = inputDoc.getAttribute("/Update/ButtonPressed");
		LOG.debug("The button pressed is " + buttonPressed+"\tinputDoc:"+inputDoc);

		try {
			if (TradePortalConstants.BUTTON_INV_DELETE_INVOICES.equals(buttonPressed) || TradePortalConstants.BUTTON_INV_DELETE_LRQ_INVOICES.equals(buttonPressed)
					|| "PayDeleteInvoices".equals(buttonPressed)) {
				outputDoc = deleteSelectedInvoice(inputDoc, mediatorServices);
			}
			else if (TradePortalConstants.BUTTON_INV_ASSIGN_INSTR_TYPE.equals(buttonPressed)) {
				outputDoc = assignInstrType(inputDoc, mediatorServices);
			}
			
			else if (TradePortalConstants.BUTTON_INV_AUTHORIZE_INVOICES.equals(buttonPressed) || TradePortalConstants.BUTTON_INV_PROXYAUTHORIZE_INVOICES.equals(buttonPressed)) {
				outputDoc = authorizeInvoice(inputDoc, true,
						TradePortalConstants.UPLOAD_INV_STATUS_AVL_FOR_AUTH,
						TradePortalConstants.UPLOAD_INV_STATUS_RAA,
						TradePortalConstants.UPLOAD_INV_STATUS_FIN_AUTH,
						TradePortalConstants.UPLOAD_INV_STATUS_FIN_PAR_AUTH,
						TradePortalConstants.UPLOAD_INV_STATUS_FIN_AUTH_FAILED,
						mediatorServices);
			}
			
			else if (TradePortalConstants.BUTTON_INV_AUTHORIZE_CREDIT_NOTE.equals(buttonPressed) || TradePortalConstants.BUTTON_INV_PROXYAUTHORIZE_CREDIT_NOTE.equals(buttonPressed)) {
				outputDoc = authorizeInvoice(inputDoc, false,
						TradePortalConstants.UPLOAD_INV_STATUS_PEND_ACT_CREDIT_NOTE,
						TradePortalConstants.UPLOAD_INV_STATUS_RAA,
						TradePortalConstants.UPLOAD_INV_STATUS_AUTH,
						TradePortalConstants.UPLOAD_INV_STATUS_PAR_AUTH,
						TradePortalConstants.UPLOAD_INV_STATUS_AUTH_FAILED,
						mediatorServices);
			}
            // NAR IR- MAUM051043378 created new method to close action
			else if (TradePortalConstants.BUTTON_INV_CLOSE_INVOICE.equals(buttonPressed)) {
				outputDoc = closeInvoice(inputDoc, mediatorServices);
			}
			else if (TradePortalConstants.BUTTON_INV_DELETE_DOCUMENT.equals(buttonPressed) || "PayDeleteDocument".equals(buttonPressed)) {
				outputDoc = deleteAttachedDocuments(inputDoc, mediatorServices);
			}
			else if (TradePortalConstants.BUTTON_INV_ACCEPT_FINANCING.equals(buttonPressed)) {
				outputDoc = acceptFinancing(inputDoc, mediatorServices);
			}
			else if (TradePortalConstants.BUTTON_INV_REMOVE_FINANCING.equals(buttonPressed)) {
				outputDoc = removeFinancing(inputDoc, mediatorServices);
			}
			else if (TradePortalConstants.BUTTON_INV_APPLY_PAYMENT_DATE.equals(buttonPressed)) {
				outputDoc = applyPaymentDate(inputDoc, mediatorServices);
			}
				
			else if (TradePortalConstants.BUTTON_INV_CLEAR_PAYMENT_DATE.equals(buttonPressed) || "PayClearPaymentDate".equals(buttonPressed) || 
					TradePortalConstants.PAY_PGM_BUTTON_CLEAR_PAYMENT_DATE.equals(buttonPressed)) {
				outputDoc = clearPaymentDate(inputDoc, mediatorServices);			}
			else if ((TradePortalConstants.BUTTON_INV_ASSIGN_LOAN_TYPE).equals(buttonPressed)) {
		   	       outputDoc = assignLoanType(inputDoc, mediatorServices);
		        }
			else if ((TradePortalConstants.BUTTON_INV_CREATE_LOAN_REQ).equals(buttonPressed)) {
		   	       outputDoc = createLoanRequest(inputDoc, mediatorServices);
		        }
		
			else if (TradePortalConstants.BUTTON_INV_ATTACH_DOCUMENT.equals(buttonPressed) || "PayAttachDocument".equals(buttonPressed)) {
				outputDoc = attachDocument(inputDoc, mediatorServices);
			}
			
			else if ((TradePortalConstants.BUTTON_INV_CREATE_LOAN_REQ_BY_RULES).equals(buttonPressed) || TradePortalConstants.REC_LIST_CREATE_LOAN_REQ_BY_RULES.equals(buttonPressed)) {
		   	       outputDoc = createLoanRequestByRules(inputDoc, mediatorServices);
		        }
			
			else if (TradePortalConstants.BUTTON_INV_RESET_TO_AUTHORIZE.equals(buttonPressed)) {
				outputDoc = resetToAuthorize(inputDoc, mediatorServices);
			}
			else if (TradePortalConstants.BUTTON_NOTIFY_PANEL_AUTH_USER.equals(buttonPressed)){
 	   			outputDoc = performNotifyPanelAuthUser(inputDoc, mediatorServices);
 	   		}
			else if (TradePortalConstants.REC_GROUP_BUTTON_DECLINE.equals(buttonPressed)){
 	   			outputDoc = declineInvoice(inputDoc, mediatorServices);
 	   		}
			else {
				LOG.info("Error in InvoiceGroupMediator - unknown button: " + buttonPressed);
			}
		}
		

        catch (AmsException | RemoteException e) {
			e.printStackTrace();
			throw e;
		}

		return outputDoc;
	}

	/**
	 * This method performs to set invoice status as 'Deleted'.
	 * this method doesn't delete invoice from database, this only set status as deleted invoice.
	 */
	private DocumentHandler deleteSelectedInvoice(DocumentHandler inputDoc,
			MediatorServices mediatorServices)
	throws RemoteException, AmsException {
		mediatorServices.debug("InvoiceGroupMediatorBean.deleteSelectedInvoice: Start: ");
		String userOid = inputDoc.getAttribute("/InvoiceGroupList/userOid");
		String securityRights = inputDoc.getAttribute("/InvoiceGroupList/securityRights");
		String buttonPressed = inputDoc.getAttribute("/Update/ButtonPressed");
		
		BigInteger secAcc = SecurityAccess.RECEIVABLES_DELETE_INVOICE;
		if( "PayDeleteInvoices".equals(buttonPressed)){
			 secAcc = SecurityAccess.PAYABLES_DELETE_INVOICE;
		}
		if (!SecurityAccess.hasRights(securityRights, secAcc)) {
			// user does not have appropriate authority.
			mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.NO_ACTION_INVOICE_APPROVE);
			return new DocumentHandler();
		}

		List<DocumentHandler> invoiceGroupList = inputDoc.getFragmentsList("/InvoiceGroupList/InvoiceGroup");
		if (invoiceGroupList.isEmpty()) {
			mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.NO_ITEM_SELECTED,
					mediatorServices.getResourceManager().getText(
							"UploadInvoiceAction.Delete",
							TradePortalConstants.TEXT_BUNDLE));
			return new DocumentHandler();
		}
		mediatorServices.debug("InvoiceGroupMediatorBean: number of invoice groups selected: "
				+ invoiceGroupList.size());

		// Process each Invoice Group in the list
		for (DocumentHandler invoiceGroupItem : invoiceGroupList) {

			String[] invoiceData = invoiceGroupItem.getAttribute("InvoiceData").split("/");
			String invoiceGroupOid = invoiceData[0];
			String optLock = invoiceData[1];
			InvoiceGroup invoiceGroup = (InvoiceGroup) mediatorServices.createServerEJB("InvoiceGroup",
					Long.parseLong(invoiceGroupOid));
			String invoiceStatus = invoiceGroup.getAttribute("invoice_status");
			String groupDescription = invoiceGroup.getDescription();

			if("PayDeleteInvoices".equals(buttonPressed)){//for payables
				if (!InvoiceUtility.isPayableInvoiceDeletable(invoiceStatus)) {
                    mediatorServices.getErrorManager().issueError(PayableInvoiceGroupMediator.class.getName(),
                            TradePortalConstants.TRANSACTION_CANNOT_PROCESS,
                            mediatorServices.getResourceManager().getText("InvoiceGroups.InvoiceGroup", TradePortalConstants.TEXT_BUNDLE),
                            groupDescription,
                            ReferenceDataManager.getRefDataMgr().getDescr("UPLOAD_INVOICE_STATUS", invoiceStatus, mediatorServices.getCSDB().getLocaleName()),
                            mediatorServices.getResourceManager().getText("UploadInvoiceAction.Deleted", TradePortalConstants.TEXT_BUNDLE));
                    
                    
                    continue; // Move on to next Invoice Group
                    
                }
			}
			else{//for receivables
			// only Pending Action and Authorization Failed invoices can be deleted

			if (!InvoiceUtility.isDeletable(invoiceStatus)) {
		
				mediatorServices.getErrorManager().issueError(InvoiceGroupMediator.class.getName(),
						TradePortalConstants.TRANSACTION_CANNOT_PROCESS,
						mediatorServices.getResourceManager().getText("InvoiceGroups.InvoiceGroup", TradePortalConstants.TEXT_BUNDLE),
						groupDescription,
						ReferenceDataManager.getRefDataMgr().getDescr("UPLOAD_INVOICE_STATUS", invoiceStatus, mediatorServices.getCSDB().getLocaleName()),
						mediatorServices.getResourceManager().getText("UploadInvoiceAction.Deleted", TradePortalConstants.TEXT_BUNDLE));

				continue; // Move on to next Invoice Group
			
			}			 
			}
			String newStatus = TradePortalConstants.UPLOAD_INV_STATUS_DELETED;
			invoiceGroup.setAttribute("invoice_status", newStatus);
			invoiceGroup.setAttribute("opt_lock", optLock);
			int success = invoiceGroup.save();

			if (success == 1)  {
			
				String sql = "select UPLOAD_INVOICE_OID,A_TRANSACTION_OID,A_INSTRUMENT_OID from INVOICES_SUMMARY_DATA where A_INVOICE_GROUP_OID = ? ";
				DocumentHandler	invoiceListDoc = DatabaseQueryBean.getXmlResultSet(sql, true, new Object[]{invoiceGroupOid});
				if (invoiceListDoc != null) {
					List<DocumentHandler> records = invoiceListDoc.getFragmentsList("/ResultSetRecord");
					String action =TradePortalConstants.UPLOAD_INV_ACTION_DELETED;
					for (DocumentHandler doc : records) {
						String invoiceOid = doc.getAttribute("UPLOAD_INVOICE_OID");
						String transactionOid = doc.getAttribute("/A_TRANSACTION_OID");
                        String instrumentOid = doc.getAttribute("/A_INSTRUMENT_OID");
						if (StringFunction.isNotBlank(invoiceOid)) {
							InvoicesSummaryData invoiceSummaryData =
								(InvoicesSummaryData) mediatorServices.createServerEJB("InvoicesSummaryData",
										Long.parseLong(invoiceOid));
							if(StringFunction.isNotBlank(instrumentOid) && StringFunction.isNotBlank(transactionOid)){
                                newStatus = TradePortalConstants.PAYABLE_UPLOAD_INV_STATUS_UNASSIGNED;
                                action =TradePortalConstants.PAYABLE_UPLOAD_INV_ACTION_UNLINKED;
                                invoiceSummaryData.setAttribute("instrument_oid",null);
                                invoiceSummaryData.setAttribute("transaction_oid",null);
                                invoiceSummaryData.setAttribute("terms_oid",null);
                            }
							
							invoiceSummaryData.setAttribute("tp_rule_name", invoiceSummaryData.getTpRuleName());
							invoiceSummaryData.setAttribute("invoice_status",newStatus);
							invoiceSummaryData.setAttribute("action", action);
                            invoiceSummaryData.setAttribute("user_oid", userOid);
							int invoiceSuccess = invoiceSummaryData.save();
							String newGroupOid = invoiceSummaryData.getAttribute("invoice_group_oid");
							if (invoiceSuccess == 1) {

								if(StringFunction.isNotBlank(newGroupOid) && !invoiceGroupOid.equals(newGroupOid)){
									updateAttachmentDetails(newGroupOid,mediatorServices);
							 }
							}
						}
					}
				}

				// display message
				mediatorServices.getErrorManager().issueError(InvoiceGroupMediator.class.getName(),
						TradePortalConstants.REC_INVOICE_DELETE_SUCCESSFUL,
						mediatorServices.getResourceManager().getText("InvoiceGroups.InvoiceGroup", TradePortalConstants.TEXT_BUNDLE),
						groupDescription);
			}
		}
		mediatorServices.debug("InvoiceGroupMediatorBean.deleteSelectedInvoice: End: ");
		return new DocumentHandler();
	}

	/**
	 * This method performs to assign Instrument Type to all selected Invoices.
	 * for this 8.0 release the system will not provide a pop up window � the system will automatically assign the invoice(s) to a
	 * receivables management instrument type.  However, after the Portal Refresh is complete we will incorporate the Pop Up window
	 * concept for other instrument types.
	 */
	private DocumentHandler assignInstrType(DocumentHandler inputDoc,
			MediatorServices mediatorServices)
	throws RemoteException, AmsException {
		mediatorServices.debug("InvoiceGroupMediatorBean.assignInstrType: Start: ");
		String userOid = inputDoc.getAttribute("/InvoiceGroupList/userOid");
		String loanType       = inputDoc.getAttribute("/InvoiceGroupList/loan_type");
		String instrumentType = inputDoc.getAttribute("/InvoiceGroupList/linked_to_instrument_type");
		String corpOrgOid = inputDoc.getAttribute("/InvoiceGroupList/ownerOrg");
		String transactionStatus = null;
		String securityRights = inputDoc.getAttribute("/InvoiceGroupList/securityRights");
		if (!SecurityAccess.hasRights(securityRights, SecurityAccess.RECEIVABLES_ASSIGN_INSTR_TYPE)) {
			// user does not have appropriate authority.
			mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.NO_ACTION_MATCH_NOTICE_AUTH,
					mediatorServices.getResourceManager().getText(
							"SecurityProfileDetail.PayAssignInstrType",
							TradePortalConstants.TEXT_BUNDLE));
			return new DocumentHandler();
		}
		List<DocumentHandler> invoiceGroupList = inputDoc.getFragmentsList("/InvoiceGroupList/InvoiceGroup");

		if (invoiceGroupList.isEmpty()) {
			mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.NO_ITEM_SELECTED,
					mediatorServices.getResourceManager().getText(
							"UploadInvoiceAction.AssignInstrType",
							TradePortalConstants.TEXT_BUNDLE));
			return new DocumentHandler();
		}
		mediatorServices.debug("InvoiceGroupMediatorBean: number of invoice groups selected: "
				+ invoiceGroupList.size());
	
		//validate loan type against REFDATA-LOANTYPE
			Vector loanList = InvoiceUtility.getLoanTypesFromRefData();
			LOG.info("loanList:"+loanList);
			if(StringFunction.isNotBlank(loanType)){
				 if(!loanList.contains(loanType)){
					 loanType="";
				 }
			}
			LOG.info("finally loanType:"+loanType);
		// Process each Invoice Group in the list
		for (int i = 0; i < invoiceGroupList.size(); i++) {
			DocumentHandler invoiceGroupItem = invoiceGroupList.get(i);
			String[] invoiceData = invoiceGroupItem.getAttribute("InvoiceData").split("/");
			String invoiceGroupOid = invoiceData[0];
			String optLock = invoiceData[1];
			InvoiceGroup invoiceGroup = (InvoiceGroup) mediatorServices.createServerEJB("InvoiceGroup",
					Long.parseLong(invoiceGroupOid));
			String oldStatus = invoiceGroup.getAttribute("invoice_status");
			String oldInstrType = invoiceGroup.getAttribute("linked_to_instrument_type");
			String groupDescription = invoiceGroup.getDescription();
			boolean groupHasBeenSaved = false;

			// only Pending Action - No Instr Type invoices can be assigned an
			// instrument type
			if (!TradePortalConstants.UPLOAD_INV_STATUS_PEND_ACT_NO_INSTR_TYPE.equals(oldStatus)) {
				mediatorServices.getErrorManager().issueError(InvoiceGroupMediator.class.getName(),
						TradePortalConstants.TRANSACTION_CANNOT_PROCESS,
						mediatorServices.getResourceManager().getText("InvoiceGroups.InvoiceGroup", TradePortalConstants.TEXT_BUNDLE),
						groupDescription,
						ReferenceDataManager.getRefDataMgr().getDescr("UPLOAD_INVOICE_STATUS", oldStatus, mediatorServices.getCSDB().getLocaleName()),
						mediatorServices.getResourceManager().getText("UploadInvoiceAction.AssignInstrType", TradePortalConstants.TEXT_BUNDLE));


				continue; // Move on to next Invoice Group
				
			}
			
				  // check if payment is null or not while assigning TRADE_LOAN 
				 boolean isPayDateNull = InvoiceUtility.isPaymentDateNull(invoiceGroupOid);	
				 CorporateOrganization corporateOrganization = (CorporateOrganization) mediatorServices.createServerEJB("CorporateOrganization",
							Long.parseLong(corpOrgOid));

				
				 
			if(InstrumentType.LOAN_RQST.equals(instrumentType) && TradePortalConstants.TRADE_LOAN.equals(loanType) && !isPayDateNull &&
					TradePortalConstants.BANK_DEF_PAYMENT_ALLOW_NO.equals(corporateOrganization.getAttribute("use_payment_date_allowed_ind"))){
					
					mediatorServices.getErrorManager().issueError(
			    			  UploadInvoiceMediator.class.getName(),
			    			  TradePortalConstants.TRADE_LOAN_NO_PAYMENT_DATE,mediatorServices.getResourceManager().getText("InvoiceGroups.InvoiceGroup", TradePortalConstants.TEXT_BUNDLE) +" - "+groupDescription);
			    	  return new DocumentHandler();
					
			}

		
			if(TradePortalConstants.INV_LINKED_INSTR_REC.equals(instrumentType)){
				loanType = null;
			}			
	
			invoiceGroup.setAttribute("linked_to_instrument_type", instrumentType);
			invoiceGroup.setAttribute("opt_lock", optLock);

			String sql = "select UPLOAD_INVOICE_OID from INVOICES_SUMMARY_DATA where A_INVOICE_GROUP_OID = ?  ";
			DocumentHandler	invoiceListDoc = DatabaseQueryBean.getXmlResultSet(sql, true, new Object[]{invoiceGroupOid});
			if (invoiceListDoc != null) {
				int success = 0;
				String newGroupOid = null;
				List<DocumentHandler> records = invoiceListDoc.getFragmentsList("/ResultSetRecord");
				 JPylonProperties jPylonProperties = JPylonProperties.getInstance();
					String serverLocation             = jPylonProperties.getString("serverLocation");
					BeanManager beanMgr = new BeanManager();
					beanMgr.setServerLocation(serverLocation);

				for (DocumentHandler doc : records) {
					String invoiceOid = doc.getAttribute("UPLOAD_INVOICE_OID");
						
					if (StringFunction.isNotBlank(invoiceOid)) {
						InvoicesSummaryData invoiceSummaryData =
							(InvoicesSummaryData) mediatorServices.createServerEJB("InvoicesSummaryData",
									Long.parseLong(invoiceOid));
						//Srinivasu_D IR#11999 Rel 8.2 02/23/2013 start
						String invoiceId		   = invoiceSummaryData.getAttribute("invoice_id");
						String transactionOid    = invoiceSummaryData.getAttribute("transaction_oid");
						if(StringFunction.isNotBlank(transactionOid)){
							  Transaction tran = (Transaction) mediatorServices.createServerEJB("Transaction", Long.parseLong(transactionOid));
								 transactionStatus = tran.getAttribute("transaction_status");
								 LOG.debug("transactionStatus:"+transactionStatus);
						  }
						if(!TransactionStatus.AUTHORIZED.equals(transactionStatus)){
							invoiceGroup.setAttribute("linked_to_instrument_type", instrumentType);

							if(InstrumentType.LOAN_RQST.equals(instrumentType)){
								invoiceGroup.setAttribute("loan_type", loanType);
							}
						}else {
							 mediatorServices.getErrorManager().issueError(
			     	                UploadInvoiceMediator.class.getName(),
			     	                TradePortalConstants.REC_INVOICE_ASSIGN_INSTRUMENT_TYPE_NOT_ALLOW, invoiceId);
			     	   	 continue; // Move on to next Invoice
						}
					
						invoiceSummaryData.setAttribute("linked_to_instrument_type",instrumentType);
						
						String newStatus = null;
						if (invoiceSummaryData.isCreditNote()) {
							newStatus = TradePortalConstants.UPLOAD_INV_STATUS_PEND_ACT_CREDIT_NOTE;
						}
						else {
							try {
								if(InstrumentType.LOAN_RQST.equals(instrumentType) && 
										(StringFunction.isNotBlank(loanType) && TradePortalConstants.EXPORT_FIN.equals(loanType))){
									 LOG.debug("loan Type:"+loanType);
									invoiceSummaryData.calculateInterestDiscount(corpOrgOid);
								 }
								 else if(TradePortalConstants.REC.equals(instrumentType)){								 
									invoiceSummaryData.calculateInterestDiscount(beanMgr);
									newStatus = TradePortalConstants.UPLOAD_INV_STATUS_AVL_FOR_FIN;
								 }
								
							}
							catch (AmsException e) {
								IssuedError is = e.getIssuedError();
								if (is != null) {
									String errorCode = is.getErrorCode();
									//BSL IR NNUM040526786 04/05/2012 BEGIN - add Pct Val To Fn Zero and combine three if branches w/identical logic
									if (TradePortalConstants.ERR_UPLOAD_INV_TENOR_DAYS_LESS.equals(errorCode) ||
											TradePortalConstants.ERR_UPLOAD_INV_DUE_PMT_DAYS_DIFF.equals(errorCode) ||
											TradePortalConstants.ERR_UPLOAD_INV_PCT_VAL_TO_FIN_ZERO.equals(errorCode))
									{
										if(TradePortalConstants.REC.equals(instrumentType)){
											newStatus = TradePortalConstants.UPLOAD_INV_STATUS_PEND_ACT_UNABLE_TO_FIN;
										}
										else{
											mediatorServices.getErrorManager().issueError(
									    			  UploadInvoiceMediator.class.getName(),
									    			  errorCode,invoiceSummaryData.getAttribute("invoice_id"));
									    	  return new DocumentHandler();
										}
										
									}
									//BSL IR NNUM040526786 04/05/2012 END
									else {
										mediatorServices.debug("InvoiceGroupMediatorBean.assignInstrType: Unexpected error: " + e.toString());
										throw e;
									}
								}
								else {
									mediatorServices.debug("InvoiceGroupMediatorBean.assignInstrType: Unexpected error: " + e.toString());
									throw e;
								}
							}
						}

						if (!groupHasBeenSaved) {
							 if(TradePortalConstants.REC.equals(instrumentType)){
								 invoiceGroup.setAttribute("invoice_status", newStatus);
							 }else if(InstrumentType.LOAN_RQST.equals(instrumentType)){
								 invoiceSummaryData.setAttribute("loan_type", loanType);
								 newStatus = TradePortalConstants.UPLOAD_INV_STATUS_PEND_ACT;
								 invoiceGroup.setAttribute("invoice_status", newStatus);
							 }
							if ((StringFunction.isNotBlank(newStatus) && !newStatus.equals(oldStatus))
									|| (StringFunction.isNotBlank(oldInstrType)
											&& !oldInstrType.equals(instrumentType)))
							{
								// check that another matching group does not already exist.
								newGroupOid = InvoiceUtility.findMatchingInvoiceGroup(invoiceGroup);

								if (StringFunction.isNotBlank(newGroupOid)
										&& !newGroupOid.equals(invoiceGroupOid)) {
									// there is another match, so assign the other group's OID
									// to these invoices and delete this group
									success = invoiceGroup.delete();
								}
								else {
									success = invoiceGroup.save();
								}
							}
							else {
								success = invoiceGroup.save();
							}

							if (success == 1) {
								groupHasBeenSaved = true;

								mediatorServices.getErrorManager().issueError(InvoiceGroupMediator.class.getName(),
										TradePortalConstants.INV_INSTR_ASSIGNED_SUCCESSFUL,
										ReferenceDataManager.getRefDataMgr().getDescr("INSTRUMENT_TYPE", instrumentType, mediatorServices.getCSDB().getLocaleName()),groupDescription);
							}
						}
						if(TradePortalConstants.REC.equals(instrumentType)){							 
						invoiceSummaryData.setAttribute("invoice_status", newStatus);
						}
						else if(InstrumentType.LOAN_RQST.equals(instrumentType)){
							invoiceSummaryData.setAttribute("loan_type", loanType);
							 newStatus = TradePortalConstants.UPLOAD_INV_STATUS_PEND_ACT;
							 invoiceSummaryData.setAttribute("invoice_status", newStatus);
						 }
					
						 String tpName=invoiceSummaryData.getTpRuleName();
						 
						 
						LOG.debug("buyerNam:"+tpName);
						invoiceSummaryData.setAttribute("tp_rule_name", tpName);						
						if (StringFunction.isNotBlank(newGroupOid)) {
							invoiceSummaryData.setAttribute("invoice_group_oid",
									newGroupOid);
						}
						invoiceSummaryData.setAttribute("action", TradePortalConstants.UPLOAD_INV_ACTION_ASSIGN_INSTR_TYPE);
                        invoiceSummaryData.setAttribute("user_oid", userOid);
						if (invoiceSummaryData.save() == 1) {

							if(StringFunction.isNotBlank(newGroupOid)){
											updateAttachmentDetails(newGroupOid,mediatorServices);
							}
					
							}
					}
				}
			}
		}
		mediatorServices.debug("InvoiceGroupMediatorBean.assignInstrType: End: ");
		return new DocumentHandler();
	}

	/**
	 * This method performs the Authorization or Partial Authorization of all
	 * selected Invoices.
	 */
	private DocumentHandler authorizeInvoice(DocumentHandler inputDoc,
			boolean isFinanceable, String statusAvailable,String statusRAA, String statusAuth,
			String statusParAuth, String statusAuthFailed,
			MediatorServices mediatorServices)
	throws RemoteException, AmsException {
		mediatorServices.debug("InvoiceGroupMediatorBean.authorizeInvoice: Start: ");
		String userOid = inputDoc.getAttribute("/InvoiceGroupList/userOid");
		String corpOrgOid = inputDoc.getAttribute("/InvoiceGroupList/ownerOrg");
		String securityRights = inputDoc.getAttribute("/InvoiceGroupList/securityRights");
	
		String userOrgOid = null;
		User user = (User) mediatorServices.createServerEJB("User",
				Long.parseLong(userOid));
		userOrgOid = user.getAttribute("owner_org_oid");
		// ***********************************************************************
		// check to make sure that the user is allowed to Authorize the
		// invoice - the user has to be a corporate user (not an admin
		// user) and have the rights to Authorize a Receivables
		// ***********************************************************************
		if (!TradePortalConstants.OWNER_CORPORATE.equals(user.getAttribute("ownership_level"))) {
			mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.ADMIN_USER_CANNOT_AUTHORIZE,
					user.getAttribute("first_name"),
					user.getAttribute("last_name"));
			return new DocumentHandler();
		}
		else if (!SecurityAccess.hasRights(securityRights, SecurityAccess.RECEIVABLES_AUTHORIZE_INVOICE)) {
			// user does not have appropriate authority.
			mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.NO_ACTION_INVOICE_AUTH);
			return new DocumentHandler();
		}

		List<DocumentHandler> invoiceGroupList = inputDoc.getFragmentsList("/InvoiceGroupList/InvoiceGroup");
		if (invoiceGroupList.isEmpty()) {
			mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.NO_ITEM_SELECTED,
					mediatorServices.getResourceManager().getText(
							"UploadInvoiceAction.Authorize",
							TradePortalConstants.TEXT_BUNDLE));
			return new DocumentHandler();
		}
		mediatorServices.debug("InvoiceGroupMediatorBean: number of invoice groups selected: "
				+ invoiceGroupList.size());

		String userWorkGroupOid = user.getAttribute("work_group_oid");
		Map<String, String> tradingPartnerXmlCache = new HashMap<String, String>();
		CorporateOrganization corpOrg = null;

		// Process each Invoice Group in the list
		GROUPS_LIST: //BSL IR NLUM042039026 04/23/2012 Rel 8.0 - add label
		for (int i = 0; i < invoiceGroupList.size(); i++) {
			DocumentHandler invoiceGroupItem = invoiceGroupList.get(i);
			String[] invoiceData = invoiceGroupItem.getAttribute("InvoiceData").split("/");
			String invoiceGroupOid = invoiceData[0];
			String optLock = invoiceData[1];
			Map<String, String> newGroup = new HashMap();
			InvoiceGroup invoiceGroup = (InvoiceGroup) mediatorServices.createServerEJB("InvoiceGroup",
					Long.parseLong(invoiceGroupOid));
			String invoiceStatus = invoiceGroup.getAttribute("invoice_status");
			String groupDescription = invoiceGroup.getDescription();

			// only certain invoice statuses can be authorized.
			if (!(statusAvailable.equals(invoiceStatus) || statusRAA.equals(invoiceStatus) ||
					statusParAuth.equals(invoiceStatus) ||
					statusAuthFailed.equals(invoiceStatus))) {
				mediatorServices.getErrorManager().issueError(
						InvoiceGroupMediator.class.getName(),
						TradePortalConstants.TRANSACTION_CANNOT_PROCESS,
						mediatorServices.getResourceManager().getText("InvoiceGroups.InvoiceGroup", TradePortalConstants.TEXT_BUNDLE),
						groupDescription,
						ReferenceDataManager.getRefDataMgr().getDescr("UPLOAD_INVOICE_STATUS", invoiceStatus, mediatorServices.getCSDB().getLocaleName()),
						mediatorServices.getResourceManager().getText("TransactionAction.Authorized", TradePortalConstants.TEXT_BUNDLE));

		
				continue; // Move on to next Invoice Group
			
			}

			boolean groupHasBeenSaved = false;
			invoiceGroup.setAttribute("opt_lock", optLock);
		
			String invoiceGroupAmount = null;
			String invoiceGroupCurrency = null;
			String panelAuthTransactionStatus = null;
			String panelGroupOid = null;
			String panelOplockVal = null;
			String panelRangeOid = null;
			BigDecimal amountInBaseToPanelGroup = null ; 
			if (corpOrg == null) {
				corpOrg = (CorporateOrganization) mediatorServices.createServerEJB("CorporateOrganization",
						Long.parseLong(corpOrgOid));
			}
			
			String groupAmountsql = "select AMOUNT, CURRENCY from INVOICE_GROUP_VIEW where INVOICE_GROUP_OID = ? ";
			DocumentHandler invoiceAmountDoc = DatabaseQueryBean.getXmlResultSet(groupAmountsql, true, new Object[]{invoiceGroupOid});
			if(invoiceAmountDoc !=null){
				invoiceGroupAmount = (invoiceAmountDoc.getFragment("/ResultSetRecord")).getAttribute("AMOUNT");
				invoiceGroupCurrency = (invoiceAmountDoc.getFragment("/ResultSetRecord")).getAttribute("CURRENCY");
			}
			
			//CR 913Checking Threshold amount and daily limit.Rpasupulati start
			
			String baseCurrency=null;
			String thresholdGroupOid=null;
			thresholdGroupOid	= user.getAttribute("threshold_group_oid");
			baseCurrency=corpOrg.getAttribute("base_currency_code");
			if(invoiceGroupAmount!=null && thresholdGroupOid!=null){
						
			String selectClause="select distinct upload_invoice_oid,currency as currency_code,amount as payment_amount from invoices_summary_data ";
			String clientBankOid = inputDoc.getAttribute("/InvoiceGroupList/clientBankOid");
			if( !(InvoiceUtility.checkThresholds(invoiceGroupAmount,invoiceGroupCurrency,baseCurrency,
					"upload_rec_inv_thold","upload_rec_inv_dlimit", userOrgOid,clientBankOid,user,selectClause,false,"", mediatorServices))){
					//if errorsFound is true returning new DocumentHandler
						return new DocumentHandler();
				}
			}
			
			//CR 913Checking Threshold amount and daily limit.
			//Rpasupulati end

			boolean isPanelAuthEnabled = false;
			String instrType = null;
			if(TradePortalConstants.UPLOAD_INV_STATUS_PEND_ACT_CREDIT_NOTE.equals(statusAvailable)){
				if(corpOrg.isPanelAuthEnabled(TradePortalConstants.RECEIVABLES_CREDIT_NOTE)){
					isPanelAuthEnabled = true;
					instrType = TradePortalConstants.RECEIVABLES_CREDIT_NOTE;
				}
			}else if(TradePortalConstants.UPLOAD_INV_STATUS_AVL_FOR_AUTH.equals(statusAvailable)){
				if(corpOrg.isPanelAuthEnabled(TradePortalConstants.INVOICE)){
					isPanelAuthEnabled = true;
					instrType = TradePortalConstants.INVOICE;
				}
			}
		    if(isPanelAuthEnabled){
				PanelAuthorizationGroup panelAuthGroup = null;
				CorporateOrganization transactionOwnerOrg = (CorporateOrganization) mediatorServices.createServerEJB("CorporateOrganization",  Long.parseLong(corpOrgOid));
				if(StringFunction.isBlank(invoiceGroup.getAttribute("panel_auth_group_oid"))) {
					
					panelGroupOid = PanelAuthUtility.getPanelAuthorizationGroupOid(instrType, corpOrg, mediatorServices.getErrorManager());   				
    				if(StringFunction.isNotBlank(panelGroupOid)){
    					panelAuthGroup = (PanelAuthorizationGroup) mediatorServices.createServerEJB("PanelAuthorizationGroup",
    							Long.parseLong(panelGroupOid));   					
    				}else{
    					// issue error as panel group is required if panel auth defined for instrument type
    					mediatorServices.getErrorManager().issueError(
    								TradePortalConstants.ERR_CAT_1,
    								AmsConstants.REQUIRED_ATTRIBUTE, 
    								mediatorServices.getResourceManager().getText("CorpCust.PanelGroupId", TradePortalConstants.TEXT_BUNDLE));
    					// if panel id is blank, return the flow back.
    					return new DocumentHandler();
    				}
					
					invoiceGroup.setAttribute("panel_auth_group_oid", panelAuthGroup.getAttribute("panel_auth_group_oid"));
					invoiceGroup.setAttribute("panel_oplock_val", panelAuthGroup.getAttribute("opt_lock"));
					
					String baseCurrencyToPanelGroup = PanelAuthUtility.getBaseCurrency(transactionOwnerOrg, panelAuthGroup, null, TradePortalConstants.INVOICE);
		 		    amountInBaseToPanelGroup =  PanelAuthUtility.getAmountInBaseCurrency
		 		   (invoiceGroupCurrency, invoiceGroupAmount, baseCurrencyToPanelGroup, corpOrgOid, 
		 				   TradePortalConstants.INVOICE, null, false, mediatorServices.getErrorManager()); 
					
					panelRangeOid = PanelAuthUtility.getPanelRange(panelAuthGroup.getAttribute("panel_auth_group_oid"), amountInBaseToPanelGroup);
					if(StringFunction.isBlank(panelRangeOid)){
    	    			mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
    							TradePortalConstants.NO_PANEL_RANGE_FOR_AMOUNT_INST_TYPE, amountInBaseToPanelGroup.toString(), TradePortalConstants.NEW_RECEIVABLES );
    	    			continue;
    	    		}
					invoiceGroup.setAttribute("panel_auth_range_oid", panelRangeOid);
				}
				
				panelGroupOid  = invoiceGroup.getAttribute("panel_auth_group_oid");
			    panelOplockVal = invoiceGroup.getAttribute("panel_oplock_val");
			    panelRangeOid =  invoiceGroup.getAttribute("panel_auth_range_oid");
		    }
	       
			/*CR 821 - End */
			StringBuilder sql = new StringBuilder("select UPLOAD_INVOICE_OID from INVOICES_SUMMARY_DATA where A_INVOICE_GROUP_OID = ? ");
			DocumentHandler	invoiceListDoc = DatabaseQueryBean.getXmlResultSet(sql.toString(), true, new Object[]{invoiceGroupOid});
			if (invoiceListDoc != null) {
				List<DocumentHandler> records = invoiceListDoc.getFragmentsList("/ResultSetRecord");
				 String tpName=null;
					 Date gmtDate = GMTUtility.getGMTDateTime();
					 SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
				for (DocumentHandler doc : records) {
					String invoiceOid = doc.getAttribute("UPLOAD_INVOICE_OID");

					if (StringFunction.isNotBlank(invoiceOid)) {
						InvoicesSummaryData invoiceSummaryData =
							(InvoicesSummaryData) mediatorServices.createServerEJB("InvoicesSummaryData",
									Long.parseLong(invoiceOid));

						if(TradePortalConstants.INDICATOR_YES.equals(invoiceSummaryData.getAttribute("portal_approval_requested_ind"))){
							statusAuth =TradePortalConstants.UPLOAD_INV_STATUS_AUTH;
							statusParAuth=TradePortalConstants.UPLOAD_INV_STATUS_PAR_AUTH;
							statusAuthFailed=TradePortalConstants.UPLOAD_INV_STATUS_AUTH_FAILED;
						}
						boolean isCreditNote            = invoiceSummaryData.isCreditNote();
						String firstAuthorizer          = invoiceSummaryData.getAttribute("first_authorizing_user_oid");
						String firstAuthorizerWorkGroup = invoiceSummaryData.getAttribute("first_authorizing_work_group_oid");
						String newStatus = null;
						invoiceSummaryData.setAttribute("action", TradePortalConstants.UPLOAD_INV_ACTION_AUTH);
                        invoiceSummaryData.setAttribute("user_oid", userOid);
						// ***********************************************************************
						// If dual authorization is required, set the transaction state to
						// partially authorized if it has not been previously authorized and set
						// the transaction to authorized if there has been a previous
						// authorizer.
						// The second authorizer must not be the same as the first authorizer
						// not the same work groups, depending on the corporate org setting.
						// If dual authorization is not required, set the invoice state to
						// authorized.
						// ***********************************************************************
						String authorizeRights  = null;
						String authorizeType = null;

						String name =invoiceSummaryData.getAttribute("buyer_name");
						String buyerId =invoiceSummaryData.getAttribute("buyer_id");
						if(name==null)name="";
						if(buyerId==null)buyerId="";
							
						String key =  name+ buyerId;			
						String ruleOid = null;	
						ArMatchingRule rule=null;
						if (tradingPartnerXmlCache.containsKey(key)) {
							// retrieve from cache
							ruleOid = tradingPartnerXmlCache.get(key); 
							rule= (ArMatchingRule) mediatorServices.createServerEJB("ArMatchingRule", Long.parseLong(ruleOid));
							
						}
						else {
							rule = invoiceSummaryData.getMatchingRule();
							if(rule!=null){
								ruleOid=	rule.getAttribute("ar_matching_rule_oid");
								tradingPartnerXmlCache.put(key, ruleOid);
							}
						}     
						if(rule==null){
							mediatorServices.getErrorManager().issueError(
									PayRemitMediator.class.getName(),
									TradePortalConstants.TP_RULE_FOR_AUTH,mediatorServices.getResourceManager().getText("UploadInvoices.InvoiceID",TradePortalConstants.TEXT_BUNDLE),
									invoiceSummaryData.getAttribute("invoice_id"));
							continue GROUPS_LIST;
						}
						if(StringFunction.isBlank(tpName)){
							 tpName = rule!=null?rule.getAttribute("buyer_name"):StringFunction.isNotBlank(name)?name:buyerId;
							}
						
						if (rule != null) {
							if (isCreditNote) {
								authorizeRights = rule.getAttribute("credit_note");
							}
							else {
								authorizeRights = rule.getAttribute("receivable_invoice");
							}	

							// IR T36000016682 If Corporate customer profile setting is applicable for authorization, 
							// Trading Partner level rule is not applicable. 
							if (isCreditNote) {
								authorizeType = rule.getAttribute("credit_authorise");
							}
							else {
								authorizeType = rule.getAttribute("receivable_authorise");
							}

								if (TradePortalConstants.DUAL_AUTH_REQ_INV_CORPORATE.equals(authorizeType)) {
									// corporate org needs to be retrieved at most once
									// for all groups being processed, so do it lazily
									if (corpOrg == null) {
										corpOrg = (CorporateOrganization) mediatorServices.createServerEJB("CorporateOrganization",
												Long.parseLong(corpOrgOid));
									}

									if (isCreditNote) {
										authorizeRights = corpOrg.getAttribute("allow_credit_note");
										authorizeType = corpOrg.getAttribute("dual_auth_credit_note");
									}
									else {
										authorizeRights = corpOrg.getAttribute("allow_arm_invoice");
										authorizeType = corpOrg.getAttribute("dual_auth_arm_invoice");
									}
								}
						}

						if (!TradePortalConstants.INDICATOR_YES.equals(authorizeRights)) {
							mediatorServices.getErrorManager().issueError(
									InvoiceGroupMediator.class.getName(),
									TradePortalConstants.NO_ACTION_TRANSACTION_AUTH,
									groupDescription,
									mediatorServices.getResourceManager().getText(
											"TransactionAction.Authorize",
											TradePortalConstants.TEXT_BUNDLE));

							continue GROUPS_LIST; // Move on to next Invoice Group
						}

						if (isFinanceable && !TradePortalConstants.INDICATOR_YES.equals(invoiceSummaryData.getAttribute("portal_approval_requested_ind"))) {
							boolean isCompliant = true;
							try {
								InvoiceUtility.calculateNumTenorDays(rule,gmtDate,formatter,invoiceSummaryData.getAttribute("due_date"),invoiceSummaryData.getAttribute("payment_date"));// checks compliance
							}
							catch (AmsException e) {
								isCompliant = false;

								IssuedError is = e.getIssuedError();
								if (is != null) {
									String errorCode = is.getErrorCode();
									//BSL IR NNUM040526786 04/05/2012 BEGIN - add Pct Val To Fn Zero and combine three if branches w/identical logic
									if (TradePortalConstants.ERR_UPLOAD_INV_TENOR_DAYS_LESS.equals(errorCode) ||
											TradePortalConstants.ERR_UPLOAD_INV_DUE_PMT_DAYS_DIFF.equals(errorCode) ||
											TradePortalConstants.ERR_UPLOAD_INV_PCT_VAL_TO_FIN_ZERO.equals(errorCode))
									{
										newStatus = TradePortalConstants.UPLOAD_INV_STATUS_PEND_ACT_UNABLE_TO_FIN;
									}
									//BSL IR NNUM040526786 04/05/2012 END
									else {
										mediatorServices.debug("InvoiceGroupMediatorBean.authorizeInvoice: Unexpected error: " + e.toString());
										throw e;
									}
								}
								else {
									mediatorServices.debug("InvoiceGroupMediatorBean.authorizeInvoice: Unexpected error: " + e.toString());
									throw e;
								}
							}

							if (!isCompliant) {
								if (!groupHasBeenSaved) {
									// if the first invoice is non-compliant, the group
									// should be set to FIN_AUTH so that the invoice
									// can be reassigned to a group properly
									invoiceGroup.setAttribute("invoice_status",
											statusAuth);

									if (invoiceGroup.save() == 1) {
										groupHasBeenSaved = true;

										mediatorServices.getErrorManager().issueError(
												InvoiceGroupMediator.class.getName(),
												TradePortalConstants.TRANSACTION_PROCESSED,
												mediatorServices.getResourceManager().getText("InvoiceGroups.InvoiceGroup", TradePortalConstants.TEXT_BUNDLE),
												groupDescription,
												mediatorServices.getResourceManager().getText("TransactionAction.Authorized", TradePortalConstants.TEXT_BUNDLE));
									}
								}

								invoiceSummaryData.setAttribute("invoice_status", newStatus);
								if(StringFunction.isNotBlank(newGroup.get(newStatus))){
						        	 invoiceSummaryData.setAttribute("bypass_grouping", "Y");
						        	 invoiceSummaryData.setAttribute("invoice_group_oid", newGroup.get(newStatus));
						        }
								invoiceSummaryData.save();
								String newGroupOid = invoiceSummaryData.getAttribute("invoice_group_oid");
								if(!newGroup.containsKey(newStatus)){
								newGroup.put(newStatus,newGroupOid);
								}
								continue;
							}
						}

						if (TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS.equals(authorizeType)
								&& StringFunction.isBlank(userWorkGroupOid))
						{
							newStatus = statusAuthFailed;

							if (!groupHasBeenSaved) {
								invoiceGroup.setAttribute("invoice_status", newStatus);
								if (invoiceGroup.save() == 1) {
									groupHasBeenSaved = true;

									mediatorServices.getErrorManager().issueError(
											InvoiceGroupMediator.class.getName(),
											TradePortalConstants.TRANS_WORK_GROUP_REQUIRED);
								}
							}

							invoiceSummaryData.setAttribute("invoice_status", newStatus);
							
							LOG.debug("buyerName<:"+tpName);
							invoiceSummaryData.setAttribute("tp_rule_name", tpName);
							if(StringFunction.isNotBlank(newGroup.get(newStatus))){
					        	 invoiceSummaryData.setAttribute("bypass_grouping", "Y");
					        	 invoiceSummaryData.setAttribute("invoice_group_oid", newGroup.get(newStatus));
					        }
							invoiceSummaryData.save();
							String newGroupOid = invoiceSummaryData.getAttribute("invoice_group_oid");
							if(!newGroup.containsKey(newStatus)){
							newGroup.put(newStatus,newGroupOid);
							}
							
						}
						else if (TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS.equals(authorizeType)
								|| TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS.equals(authorizeType))
						{
							if (StringFunction.isBlank(firstAuthorizer)) {
								// Two authorizers required and first authorizer is EMPTY: Set to partially authorized.
								newStatus = statusParAuth;

								if (!groupHasBeenSaved) {
									invoiceGroup.setAttribute("invoice_status", newStatus);
									if (invoiceGroup.save() == 1) {
										groupHasBeenSaved = true;

										mediatorServices.getErrorManager().issueError(
												InvoiceGroupMediator.class.getName(),
												TradePortalConstants.TRANS_PARTIALLY_AUTHORIZED,
												mediatorServices.getResourceManager().getText("InvoiceGroups.InvoiceGroup",TradePortalConstants.TEXT_BUNDLE),
												groupDescription);
									}
								}

								invoiceSummaryData.setAttribute("first_authorizing_user_oid", userOid);
								invoiceSummaryData.setAttribute("first_authorizing_work_group_oid", userWorkGroupOid);
								invoiceSummaryData.setAttribute("invoice_status", newStatus);
								invoiceSummaryData.setAttribute("tp_rule_name", tpName);
								if(StringFunction.isNotBlank(newGroup.get(newStatus))){
						        	 invoiceSummaryData.setAttribute("bypass_grouping", "Y");
						        	 invoiceSummaryData.setAttribute("invoice_group_oid", newGroup.get(newStatus));
						        }
								invoiceSummaryData.save();
								String newGroupOid = invoiceSummaryData.getAttribute("invoice_group_oid");
								if(!newGroup.containsKey(newStatus)){
								newGroup.put(newStatus,newGroupOid);
								}
									
							}
							else {// firstAuthorizer not empty
								if (TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS.equals(authorizeType)
										&& userWorkGroupOid.equals(firstAuthorizerWorkGroup)) {
									// Two work groups required and this user belongs to the
									// same work group as the first authorizer: Error.
									newStatus = statusAuthFailed;
									if (!groupHasBeenSaved) {
										invoiceGroup.setAttribute("invoice_status", newStatus);
										if (invoiceGroup.save() == 1) {
											groupHasBeenSaved = true;

											mediatorServices.getErrorManager().issueError(
													InvoiceGroupMediator.class.getName(),
													TradePortalConstants.TRANS_DIFF_WORK_GROUP_REQUIRED);
										}
									}
									invoiceSummaryData.setAttribute("tp_rule_name", tpName);
									invoiceSummaryData.setAttribute("invoice_status", newStatus);
									if(StringFunction.isNotBlank(newGroup.get(newStatus))){
							        	 invoiceSummaryData.setAttribute("bypass_grouping", "Y");
							        	 invoiceSummaryData.setAttribute("invoice_group_oid", newGroup.get(newStatus));
							        }
									invoiceSummaryData.save();
									String newGroupOid = invoiceSummaryData.getAttribute("invoice_group_oid");
									if(!newGroup.containsKey(newStatus)){
									newGroup.put(newStatus,newGroupOid);
									}
								}
								else if (firstAuthorizer.equals(userOid)) {
									// Two authorizers required and this user is the same as the
									// first authorizer. Give error.
									// Note requiring two work groups also implies requiring two
									// users.
									newStatus = statusAuthFailed;
									if (!groupHasBeenSaved) {
										invoiceGroup.setAttribute("invoice_status", newStatus);
										if (invoiceGroup.save() == 1) {
											groupHasBeenSaved = true;

											mediatorServices.getErrorManager().issueError(
													InvoiceGroupMediator.class.getName(),
													TradePortalConstants.DIFFERENT_SECOND_AUTH_REQRUIED);
										}
									}
									
									invoiceSummaryData.setAttribute("tp_rule_name", tpName);
									invoiceSummaryData.setAttribute("invoice_status", newStatus);
									if(StringFunction.isNotBlank(newGroup.get(newStatus))){
							        	 invoiceSummaryData.setAttribute("bypass_grouping", "Y");
							        	 invoiceSummaryData.setAttribute("invoice_group_oid", newGroup.get(newStatus));
							        }
									invoiceSummaryData.save();
									String newGroupOid = invoiceSummaryData.getAttribute("invoice_group_oid");
									if(!newGroup.containsKey(newStatus)){
									newGroup.put(newStatus,newGroupOid);
									}
								}
								else {// Authorize
									newStatus = statusAuth;

									if (!groupHasBeenSaved) {
										invoiceGroup.setAttribute("invoice_status", newStatus);
										if (invoiceGroup.save() == 1) {
											groupHasBeenSaved = true;

											mediatorServices.getErrorManager().issueError(
													InvoiceGroupMediator.class.getName(),
													TradePortalConstants.TRANSACTION_PROCESSED,
													mediatorServices.getResourceManager().getText("InvoiceGroups.InvoiceGroup", TradePortalConstants.TEXT_BUNDLE),
													groupDescription,
													mediatorServices.getResourceManager().getText("TransactionAction.Authorized", TradePortalConstants.TEXT_BUNDLE));
										}
									}

									invoiceSummaryData.setAttribute("second_authorizing_user_oid", userOid);
									invoiceSummaryData.setAttribute("second_authorizing_work_group_oid", userWorkGroupOid);
									invoiceSummaryData.setAttribute("second_authorize_status_date", DateTimeUtility.getGMTDateTime());
									invoiceSummaryData.setAttribute("invoice_status", newStatus);
									
									invoiceSummaryData.setAttribute("tp_rule_name", tpName);
									if(StringFunction.isNotBlank(newGroup.get(newStatus))){
							        	 invoiceSummaryData.setAttribute("bypass_grouping", "Y");
							        	 invoiceSummaryData.setAttribute("invoice_group_oid", newGroup.get(newStatus));
							        }
									
									if (invoiceSummaryData.save() == 1) {
										String newGroupOid = invoiceSummaryData.getAttribute("invoice_group_oid");
										if(!newGroup.containsKey(newStatus)){
											newGroup.put(newStatus,newGroupOid);
											}
										if(StringFunction.isNotBlank(newGroupOid)){
											updateAttachmentDetails(newGroupOid,mediatorServices);
									}
										InvoiceUtility.createOutgoingQueueAndIssueSuccess(newStatus,
												invoiceOid, isCreditNote, mediatorServices);
									}
								}
							}
					  //Pavani CR 821 Rel 8.3 START		
						}else if(TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH.equals(authorizeType)) {
							invoiceSummaryData.setAttribute("reqPanleAuth", TradePortalConstants.INDICATOR_YES);
							PanelAuthProcessor panelAuthProcessor = new PanelAuthProcessor(user, TradePortalConstants.INVOICE, mediatorServices.getErrorManager());
						    panelAuthProcessor.init(panelGroupOid, new String[]{panelRangeOid}, panelOplockVal);
					
						       panelAuthTransactionStatus = panelAuthProcessor.process(invoiceGroup, panelRangeOid, false);
							if(StringFunction.isBlank(invoiceSummaryData.getAttribute("panel_auth_group_oid"))){
								invoiceSummaryData.setAttribute("panel_auth_group_oid", invoiceGroup.getAttribute("panel_auth_group_oid"));
								invoiceSummaryData.setAttribute("panel_oplock_val", invoiceGroup.getAttribute("panel_oplock_val"));
							}
			    	       if (TradePortalConstants.AUTHTRUE.equals(panelAuthTransactionStatus)) {
			    	    	   newStatus = statusAuth;

								if (!groupHasBeenSaved) {
									invoiceGroup.setAttribute("invoice_status", newStatus);
									if (invoiceGroup.save() == 1) {
										groupHasBeenSaved = true;

										mediatorServices.getErrorManager().issueError(
												InvoiceGroupMediator.class.getName(),
												TradePortalConstants.TRANSACTION_PROCESSED,
												mediatorServices.getResourceManager().getText("InvoiceGroups.InvoiceGroup", TradePortalConstants.TEXT_BUNDLE),
												groupDescription,
												mediatorServices.getResourceManager().getText("TransactionAction.Authorized", TradePortalConstants.TEXT_BUNDLE));
									}
								}

								invoiceSummaryData.setAttribute("second_authorizing_user_oid", userOid);
								invoiceSummaryData.setAttribute("second_authorizing_work_group_oid", userWorkGroupOid);
								invoiceSummaryData.setAttribute("second_authorize_status_date", DateTimeUtility.getGMTDateTime());
								invoiceSummaryData.setAttribute("invoice_status", newStatus);
								invoiceSummaryData.setAttribute("tp_rule_name", tpName);
								if (invoiceSummaryData.save() == 1) {
									String newGroupOid = invoiceSummaryData.getAttribute("invoice_group_oid");
									if(StringFunction.isNotBlank(newGroupOid)){
										updateAttachmentDetails(newGroupOid,mediatorServices);
								}
									InvoiceUtility.createOutgoingQueueAndIssueSuccess(newStatus,
											invoiceOid, isCreditNote, mediatorServices);
								}
			    	       }else if (TradePortalConstants.AUTHPARTL.equals(panelAuthTransactionStatus)) {
			    	    	   newStatus = statusParAuth;

								if (!groupHasBeenSaved) {
									invoiceGroup.setAttribute("invoice_status", newStatus);
									if (invoiceGroup.save() == 1) {
										groupHasBeenSaved = true;

										mediatorServices.getErrorManager().issueError(
												InvoiceGroupMediator.class.getName(),
												TradePortalConstants.TRANS_PARTIALLY_AUTHORIZED,
												mediatorServices.getResourceManager().getText("InvoiceGroups.InvoiceGroup",TradePortalConstants.TEXT_BUNDLE),
												groupDescription);
									}
								}

								invoiceSummaryData.setAttribute("first_authorizing_user_oid", userOid);
								invoiceSummaryData.setAttribute("first_authorizing_work_group_oid", userWorkGroupOid);
								invoiceSummaryData.setAttribute("invoice_status", newStatus);
								invoiceSummaryData.setAttribute("tp_rule_name", tpName);
								invoiceSummaryData.save();
			               }else if (TradePortalConstants.AUTHFALSE.equals(panelAuthTransactionStatus)) {
			            	   newStatus = statusAuthFailed;

								if (!groupHasBeenSaved) {
									invoiceGroup.setAttribute("invoice_status", newStatus);
									if (invoiceGroup.save() == 1) {
										groupHasBeenSaved = true;
									}
								}

								invoiceSummaryData.setAttribute("invoice_status", newStatus);
								invoiceSummaryData.setAttribute("tp_rule_name", tpName);
								invoiceSummaryData.save();
			    	       } 
						}
						//Pavani CR 821 Rel 8.3 END
						else {
							// dual auth ind is NO, first auth is EMPTY
							newStatus = statusAuth;

							if (!groupHasBeenSaved) {
								invoiceGroup.setAttribute("invoice_status", newStatus);
								if (invoiceGroup.save() == 1) {
									groupHasBeenSaved = true;

									mediatorServices.getErrorManager().issueError(
											InvoiceGroupMediator.class.getName(),
											TradePortalConstants.TRANSACTION_PROCESSED,
											mediatorServices.getResourceManager().getText("InvoiceGroups.InvoiceGroup", TradePortalConstants.TEXT_BUNDLE),
											groupDescription,
											mediatorServices.getResourceManager().getText("TransactionAction.Authorized", TradePortalConstants.TEXT_BUNDLE));
								}
							}

							invoiceSummaryData.setAttribute("first_authorizing_user_oid", userOid);
							invoiceSummaryData.setAttribute("first_authorizing_work_group_oid", userWorkGroupOid);
							invoiceSummaryData.setAttribute("first_authorize_status_date", DateTimeUtility.getGMTDateTime());
							invoiceSummaryData.setAttribute("invoice_status", newStatus);
							invoiceSummaryData.setAttribute("tp_rule_name", tpName);
							if(StringFunction.isNotBlank(newGroup.get(newStatus))){
					        	 invoiceSummaryData.setAttribute("bypass_grouping", "Y");
					        	 invoiceSummaryData.setAttribute("invoice_group_oid", newGroup.get(newStatus));
					        }
							invoiceSummaryData.save();
							String newGroupOid = invoiceSummaryData.getAttribute("invoice_group_oid");
							if(!newGroup.containsKey(newStatus)){
							newGroup.put(newStatus,newGroupOid);
							}
							InvoiceUtility.createOutgoingQueueAndIssueSuccess(newStatus,
										invoiceOid, isCreditNote, mediatorServices);
							
						}
					}
				}
			}
		}
		return new DocumentHandler();
	}



	/**
	 * This method implements the Accept Financing functionality. It calculates
	 * the financing amount, Net Financing amount based on discount/interest
	 * rate.
	 *
	 * @param inputDoc
	 * @param mediatorServices
	 * @return DocumentHandler
	 * @throws RemoteException
	 * @throws AmsException
	 */
	private DocumentHandler acceptFinancing(DocumentHandler inputDoc,
			MediatorServices mediatorServices)
	throws RemoteException, AmsException {
		mediatorServices.debug("InvoiceGroupMediatorBean.acceptFinancing: Start: ");
		String userOid = inputDoc.getAttribute("/InvoiceGroupList/userOid");
		String securityRights = inputDoc.getAttribute("/InvoiceGroupList/securityRights");

		// ***********************************************************************
		// check to make sure that the user is allowed to Accept Finance the
		// invoice - the user has to be a corporate user (not an admin
		// user) and have the rights to Accept Finance a Receivables
		// ***********************************************************************
		User user = (User) mediatorServices.createServerEJB("User",
				Long.parseLong(userOid));
		if (!TradePortalConstants.OWNER_CORPORATE.equals(user.getAttribute("ownership_level"))) {
			mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.ADMIN_USER_CANNOT_ACCEPT_FINANCING,
					user.getAttribute("first_name"),
					user.getAttribute("last_name"));
			return new DocumentHandler();
		}
		else if (!SecurityAccess.hasRights(securityRights, SecurityAccess.RECEIVABLES_APPROVE_FINANCING)) {
			// user does not have appropriate authority.
			mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.NO_ACTION_INVOICE_APPROVE);
			return new DocumentHandler();
		}

		List<DocumentHandler> invoiceGroupList = inputDoc.getFragmentsList("/InvoiceGroupList/InvoiceGroup");
		if (invoiceGroupList.isEmpty()) {
			mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.NO_ITEM_SELECTED,
					mediatorServices.getResourceManager().getText(
							"UploadInvoiceAction.UploadInvoiceAcceptFinancingText",
							TradePortalConstants.TEXT_BUNDLE));
			return new DocumentHandler();
		}
		mediatorServices.debug("InvoiceGroupMediatorBean: number of invoice groups selected: "
				+ invoiceGroupList.size());
		// Process each Invoice Group in the list
		for (int i = 0; i < invoiceGroupList.size(); i++) {
			DocumentHandler invoiceGroupItem = invoiceGroupList.get(i);
			String[] invoiceData = invoiceGroupItem.getAttribute("InvoiceData").split("/");
			String invoiceGroupOid = invoiceData[0];
			String optLock = invoiceData[1];
			InvoiceGroup invoiceGroup = (InvoiceGroup) mediatorServices.createServerEJB("InvoiceGroup",
					Long.parseLong(invoiceGroupOid));
			String invoiceStatus = invoiceGroup.getAttribute("invoice_status");
			String groupDescription = invoiceGroup.getDescription();

			// only invoice status 'Available for Financing' invoices can be
			// approved for financing.
			if (!TradePortalConstants.UPLOAD_INV_STATUS_AVL_FOR_FIN.equals(invoiceStatus)) {
				mediatorServices.getErrorManager().issueError(
						InvoiceGroupMediator.class.getName(),
						TradePortalConstants.TRANSACTION_CANNOT_PROCESS,
						mediatorServices.getResourceManager().getText("InvoiceGroups.InvoiceGroup", TradePortalConstants.TEXT_BUNDLE),
						groupDescription,
						ReferenceDataManager.getRefDataMgr().getDescr("UPLOAD_INVOICE_STATUS", invoiceStatus, mediatorServices.getCSDB().getLocaleName()),
						mediatorServices.getResourceManager().getText("InvoiceSummary.AcceptFinancing", TradePortalConstants.TEXT_BUNDLE));

				continue; // Move on to next Invoice Group
			}

			
			String newStatus = TradePortalConstants.UPLOAD_INV_STATUS_AVL_FOR_AUTH;
			boolean isCompliant = true;
				String sql = "select UPLOAD_INVOICE_OID from INVOICES_SUMMARY_DATA where A_INVOICE_GROUP_OID = ? ";
				DocumentHandler	invoiceListDoc = DatabaseQueryBean.getXmlResultSet(sql, true, new Object[]{invoiceGroupOid});
				if (invoiceListDoc != null) {
					List<DocumentHandler> records = invoiceListDoc.getFragmentsList("/ResultSetRecord");
					
					ArMatchingRule rule = null;
					String tpName = null;
					Map<String,String> newGroup = new HashMap();
					Map<String,String> dueDateMap = new HashMap();
				
					try(Connection con = DatabaseQueryBean.connect(false);
							PreparedStatement pStmt1 = con
									.prepareStatement("UPDATE INVOICES_SUMMARY_DATA  SET INVOICE_STATUS=?, a_invoice_group_oid=? where upload_invoice_oid =?");
							PreparedStatement pStmt2 = con
									.prepareStatement("INSERT into INVOICE_HISTORY (INVOICE_HISTORY_OID, ACTION_DATETIME, ACTION, "
							+ "INVOICE_STATUS, P_UPLOAD_INVOICE_OID, A_USER_OID) VALUES (?,?,?,?,?,?)")){
					
					con.setAutoCommit(false);
					 
					     
					 Date gmtDate = GMTUtility.getGMTDateTime();
					 SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
					 
					for (DocumentHandler doc : records) { 
						String invoiceOid = doc.getAttribute("UPLOAD_INVOICE_OID");

						if (StringFunction.isNotBlank(invoiceOid)) {
							InvoicesSummaryData invoiceSummaryData =
								(InvoicesSummaryData) mediatorServices.createServerEJB("InvoicesSummaryData",
										Long.parseLong(invoiceOid));
							
							if(rule==null){
								rule =invoiceSummaryData.getMatchingRule();
								
							}
							String dueDate = invoiceSummaryData.getAttribute("due_date");
							String payDate = invoiceSummaryData.getAttribute("payment_date");
							
							isCompliant = true;
							try {
								if(StringFunction.isBlank(dueDateMap.get(dueDate+payDate))){
								InvoiceUtility.calculateNumTenorDays(rule,gmtDate,formatter,dueDate,payDate);//checks compliance
								}
								else{
									newStatus = dueDateMap.get(dueDate+payDate);
								}
							}
							catch (AmsException e) {
								isCompliant = false;

								IssuedError is = e.getIssuedError();
								if (is != null) {
									String errorCode = is.getErrorCode();
									//BSL IR NNUM040526786 04/05/2012 BEGIN - add Pct Val To Fn Zero and combine three if branches w/identical logic
									if (TradePortalConstants.ERR_UPLOAD_INV_TENOR_DAYS_LESS.equals(errorCode) ||
											TradePortalConstants.ERR_UPLOAD_INV_DUE_PMT_DAYS_DIFF.equals(errorCode) ||
											TradePortalConstants.ERR_UPLOAD_INV_PCT_VAL_TO_FIN_ZERO.equals(errorCode))
									{
										newStatus = TradePortalConstants.UPLOAD_INV_STATUS_PEND_ACT_UNABLE_TO_FIN;
										mediatorServices.getErrorManager().issueError(InvoiceGroupMediator.class.getName(), errorCode, groupDescription);
									}
									//BSL IR NNUM040526786 04/05/2012 END
									else {
										mediatorServices.debug("InvoiceGroupMediatorBean.acceptFinancing: Unexpected error: " + e.toString());
										throw e;
									}
								}
								else {
									mediatorServices.debug("InvoiceGroupMediatorBean.acceptFinancing: Unexpected error: " + e.toString());
									throw e;
								}
							}
							if(!dueDateMap.containsKey(dueDate+payDate)){
								dueDateMap.put(dueDate+payDate, newStatus);
							}
							
							invoiceSummaryData.setAttribute("invoice_status",newStatus);
							if(StringFunction.isBlank(tpName)){
								String buyerName = invoiceSummaryData.getAttribute("buyer_name");
								tpName = rule!=null?rule.getAttribute("buyer_name"):StringFunction.isNotBlank(buyerName)?buyerName:invoiceSummaryData.getAttribute("buyer_id");
							}
							invoiceSummaryData.setAttribute("tp_rule_name", tpName);
							//if we have already found new group fro the status then bypass grouping while saving
					        if(StringFunction.isNotBlank(newGroup.get(newStatus))){
					        	 invoiceSummaryData.setAttribute("bypass_grouping", "Y");
					        	 invoiceSummaryData.setAttribute("invoice_group_oid", newGroup.get(newStatus));
					        }else{
					        	invoiceSummaryData.verifyInvoiceGroup();
					        }
					        String newGroupOid = invoiceSummaryData.getAttribute("invoice_group_oid");
							if(!newGroup.containsKey(newStatus)){
								newGroup.put(newStatus,newGroupOid);
							}
					        pStmt1.setString(1, newStatus);
							pStmt1.setString(2, newGroupOid);
							pStmt1.setString(3, invoiceOid);
							pStmt1.addBatch();
							pStmt1.clearParameters();
							InvoiceUtility.createPurchaseOrderHisotryLog(
									pStmt2,TradePortalConstants.UPLOAD_INV_ACTION_FIN_APP,
									newStatus,
									invoiceOid, userOid);
							
						}
					}
					pStmt1.executeBatch();
					pStmt2.executeBatch();
					con.commit();
					
					}
					catch(SQLException e){
						isCompliant = false;
							e.printStackTrace();
					}
					if(!newGroup.isEmpty()){
						updateAttachmentDetails(newGroup.values(),mediatorServices,invoiceGroupOid);
					}
					
				}
				
				invoiceGroup.setAttribute("invoice_status", newStatus);
				invoiceGroup.setAttribute("opt_lock", optLock);
				int success = invoiceGroup.save();
				if(success==1 && isCompliant){
				// display message
				mediatorServices.getErrorManager().issueError(InvoiceGroupMediator.class.getName(),
						TradePortalConstants.INV_FINANCING_ACCEPTED,
						mediatorServices.getResourceManager().getText("InvoiceGroups.InvoiceGroup", TradePortalConstants.TEXT_BUNDLE),
						groupDescription);
				}
		}
		mediatorServices.debug("InvoiceGroupMediatorBean.acceptFinancing: End: ");
		return new DocumentHandler();
	}

	/**
	 * Deletes attached documents marked by the user on a Invoice
	 *
	 * @param inputDoc com.amsinc.ecsg.util.DocumentHandler
	 * @param mediatorServices com.amsinc.ecsg.frame.MediatorServices
	 * @return com.amsinc.ecsg.util.DocumentHandler
	 */
	private DocumentHandler deleteAttachedDocuments(DocumentHandler inputDoc,
			MediatorServices mediatorServices) throws RemoteException,
			AmsException {
		String securityRights = inputDoc.getAttribute("/InvoiceGroupList/securityRights");
		LOG.info("Inside deleteAttachedDoc:"+inputDoc);
		// ***********************************************************************
		// check to make sure that the user is allowed to delete documents from
		// the invoice - the user has to be a corporate user (not an admin
		// user) and have the rights to approve financing or authorize
		// ***********************************************************************


		if (!SecurityAccess.hasRights(securityRights, SecurityAccess.RECEIVABLES_APPROVE_FINANCING)
				&& !SecurityAccess.hasRights(securityRights, SecurityAccess.RECEIVABLES_AUTHORIZE_INVOICE)) {
			// user does not have appropriate authority.
			mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.ERR_UPLOAD_INV_CANNOT_DELETE_DOC);
			return new DocumentHandler();
		}

		List<DocumentHandler> invoiceGroupList = inputDoc.getFragmentsList("/InvoiceGroupList/InvoiceGroup");

		if (invoiceGroupList.isEmpty()) {
			mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.NO_ITEM_SELECTED,
					mediatorServices.getResourceManager().getText(
							"UploadInvoiceAction.UploadInvoiceDeleteDocumentText",
							TradePortalConstants.TEXT_BUNDLE));
			return new DocumentHandler();
		}

		// Loop through the list of invoice group OIDs
		for (DocumentHandler invoiceItem : invoiceGroupList) {
			String[] invoiceData = invoiceItem.getAttribute("InvoiceData").split("/");
			String invoiceGroupOid = invoiceData[0];
			String optLock = invoiceData[1];

			InvoiceGroup invoiceGroup = (InvoiceGroup) mediatorServices.createServerEJB("InvoiceGroup",
					Long.parseLong(invoiceGroupOid));

			String invoiceStatus = invoiceGroup.getAttribute("invoice_status");
			String groupDescription = invoiceGroup.getDescription();

		    // only delete documents from invoices with status other than 'Authorized', 'Financed', and 'Deleted'.
			if ((TradePortalConstants.INVOICE_TYPE_REC_MGMT.equals(invoiceGroup.getAttribute("invoice_classification")) //for rec
					&&!(TradePortalConstants.UPLOAD_INV_STATUS_AVL_FOR_FIN.equals(invoiceStatus)
					|| TradePortalConstants.UPLOAD_INV_STATUS_AVL_FOR_AUTH.equals(invoiceStatus)
					|| TradePortalConstants.UPLOAD_INV_STATUS_PAR_AUTH.equals(invoiceStatus)
					|| TradePortalConstants.UPLOAD_INV_STATUS_AUTH_FAILED.equals(invoiceStatus)
					|| TradePortalConstants.UPLOAD_INV_STATUS_FIN_AUTH_FAILED.equals(invoiceStatus)
					|| TradePortalConstants.UPLOAD_INV_STATUS_FIN_PAR_AUTH.equals(invoiceStatus)
					|| TradePortalConstants.UPLOAD_INV_STATUS_PEND_ACT_NO_INSTR_TYPE.equals(invoiceStatus)
					|| TradePortalConstants.UPLOAD_INV_STATUS_PEND_ACT_UNABLE_TO_FIN.equals(invoiceStatus)
					|| TradePortalConstants.PAYABLE_UPLOAD_INV_STATUS_PENDING_ACTION.equals(invoiceStatus)
					|| TradePortalConstants.UPLOAD_INV_STATUS_PEND_ACT_CREDIT_NOTE.equals(invoiceStatus)))
					//for pay
					|| (TradePortalConstants.INVOICE_TYPE_PAY_MGMT.equals(invoiceGroup.getAttribute("invoice_classification")) 
							&& !(TradePortalConstants.UPLOAD_INV_STATUS_PEND_ACT.equals(invoiceStatus)
							|| TradePortalConstants.PAYABLE_UPLOAD_INV_STATUS_ASSIGNED.equals(invoiceStatus)
							|| TradePortalConstants.UPLOAD_INV_STATUS_AVL_FOR_AUTH.equals(invoiceStatus)
							|| TradePortalConstants.PAYABLE_UPLOAD_INV_STATUS_UNASSIGNED.equals(invoiceStatus))))
		    {
		   	    mediatorServices.getErrorManager().issueError(
		                InvoiceGroupMediator.class.getName(),
		                TradePortalConstants.ERR_UPLOAD_INV_CANNOT_DELETE_DOC,
		                mediatorServices.getResourceManager().getText("InvoiceGroups.InvoiceGroup", TradePortalConstants.TEXT_BUNDLE),
		                groupDescription,
		                ReferenceDataManager.getRefDataMgr().getDescr("UPLOAD_INVOICE_STATUS", invoiceStatus, mediatorServices.getCSDB().getLocaleName()));
		   	    return new DocumentHandler();
		    }
			
			String imageOid = checkDocumentAlreadyAttached(invoiceGroupOid, mediatorServices, groupDescription);
															
			if (StringFunction.isNotBlank(imageOid)) {
				invoiceGroup.setAttribute("opt_lock", optLock);
				invoiceGroup.save();
				
				StringBuilder doWhereClause = new StringBuilder();
				doWhereClause.append("p_transaction_oid in (");
				doWhereClause.append("select upload_invoice_oid from invoices_summary_data where a_invoice_group_oid =? ) ");
				

				StringBuilder updateStatement = new StringBuilder();
				updateStatement.append("update document_image");
				updateStatement.append(" set p_transaction_oid = ").append(0);
				updateStatement.append(" where ").append(doWhereClause);

				Vector rowDocImageOIDs = DatabaseQueryBean.selectRowsForUpdate("document_image",  
						"doc_image_oid", doWhereClause.toString(), updateStatement.toString(), false, new Object[]{invoiceGroupOid}, new Object[]{});
				
				if(rowDocImageOIDs != null){
					
					invoiceGroup = (InvoiceGroup) mediatorServices.createServerEJB("InvoiceGroup",
							Long.parseLong(invoiceGroupOid));	
						invoiceGroup.setAttribute("attachment_ind",  TradePortalConstants.INDICATOR_NO);
						int status = invoiceGroup.save();
						LOG.debug("Updated status:"+status);	
					 // display delete PDF success message if any of document is deleted from list invoices of group ...
					mediatorServices.getErrorManager().issueError(
								TradePortalConstants.ERR_CAT_1,
								TradePortalConstants.DOC_DELETED_SUCCESSFULLY,
								groupDescription, "");
				}
			}
		}

		return new DocumentHandler();
	}

	/**
	 * This method applies payment date to invoices.
	 */
	private DocumentHandler applyPaymentDate(DocumentHandler inputDoc,
			MediatorServices mediatorServices)
	throws RemoteException, AmsException {
		LOG.debug("InvoiceGroupMediatorBean.applyPaymentDate: Start: "+inputDoc);
		String userOid = inputDoc.getAttribute("/InvoiceGroupList/userOid");
		String securityRights = inputDoc.getAttribute("/InvoiceGroupList/securityRights");
		String paymentDate = inputDoc.getAttribute("/InvoiceGroupList/payment_date");	
		String corpOrgOid = inputDoc.getAttribute("/InvoiceGroupList/ownerOrg");
		User user = (User) mediatorServices.createServerEJB("User", Long.parseLong(userOid));
		String datePattern = user.getAttribute("datepattern");
        try{			
		    SimpleDateFormat formatter = new SimpleDateFormat(datePattern);
			SimpleDateFormat reqformatter = new SimpleDateFormat("MM/dd/yyyy");
		    Date formatPaymentDate   =  formatter.parse(paymentDate);
		    paymentDate = reqformatter.format(formatPaymentDate);
		}catch(Exception e){
			mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.WEBI_INVALID_DATE_ERROR);
			return new DocumentHandler();		
		}

		if (!SecurityAccess.hasRights(securityRights, SecurityAccess.RECEIVABLES_APPLY_PAYMENT_DATE) ) {
			// user does not have appropriate authority.
			mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.NO_ACTION_MATCH_NOTICE_AUTH,
					mediatorServices.getResourceManager().getText(
							"SecurityProfileDetail.ApproveForPayment",
									TradePortalConstants.TEXT_BUNDLE));
			return new DocumentHandler();
		}

		List<DocumentHandler> invoiceGroupList = inputDoc.getFragmentsList("/InvoiceGroupList/InvoiceGroup");
		if (invoiceGroupList.isEmpty()) {
			mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.NO_ITEM_SELECTED,
					mediatorServices.getResourceManager().getText(
							"UploadInvoiceAction.ApplyPaymentDate",
							TradePortalConstants.TEXT_BUNDLE));
			return new DocumentHandler();
		}
		else if (invoiceGroupList.size() > 1) {
			mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.SELECT_ONLY_ONE_ITEM,
					mediatorServices.getResourceManager().getText(
							"InvoiceGroups.InvoiceGroup",
							TradePortalConstants.TEXT_BUNDLE),
					mediatorServices.getResourceManager().getText(
							"UploadInvoiceAction.ApplyPaymentDate",
							TradePortalConstants.TEXT_BUNDLE));
			return new DocumentHandler();
		}

		//There is only one Invoice Group to process
		DocumentHandler invoiceGroupItem = invoiceGroupList.get(0);
		String[] invoiceData = invoiceGroupItem.getAttribute("InvoiceData").split("/");
		String invoiceGroupOid = invoiceData[0];
		String optLock = invoiceData[1];
		InvoiceGroup invoiceGroup = (InvoiceGroup) mediatorServices.createServerEJB("InvoiceGroup",
				Long.parseLong(invoiceGroupOid));

		String invoiceStatus = invoiceGroup.getAttribute("invoice_status");
		String groupDescription = invoiceGroup.getDescription();

		// Payment Date can be only assigned to invoices with status 'Available for financing'/Pending Action
		if (!(TradePortalConstants.UPLOAD_INV_STATUS_AVL_FOR_FIN.equals(invoiceStatus) || TradePortalConstants.PAYABLE_UPLOAD_INV_STATUS_PENDING_ACTION.equals(invoiceStatus))) {
			mediatorServices.getErrorManager().issueError(InvoiceGroupMediator.class.getName(),
					TradePortalConstants.INV_INVALID_ACTION_PAY_DATE,
					mediatorServices.getResourceManager().getText("InvoiceGroups.InvoiceGroup", TradePortalConstants.TEXT_BUNDLE),
					groupDescription,
					ReferenceDataManager.getRefDataMgr().getDescr("UPLOAD_INVOICE_STATUS", invoiceStatus, mediatorServices.getCSDB().getLocaleName()),
					mediatorServices.getResourceManager().getText("UploadInvoiceAction.ApplyPaymentDate", TradePortalConstants.TEXT_BUNDLE));

			return new DocumentHandler();
		}

		// Narayan IR - MJUM050951373 Begin	- if any invoice of group has issue date greater than payment date throw error.	
		String invSql = "select ISSUE_DATE, portal_approval_req_ind, END_TO_END_ID, INVOICE_CLASSIFICATION from INVOICES_SUMMARY_DATA where A_INVOICE_GROUP_OID = ? ";
		DocumentHandler	invoicesListDoc = DatabaseQueryBean.getXmlResultSet(invSql, false, new Object[]{invoiceGroupOid}); 
		if (invoicesListDoc != null) {	
			List<DocumentHandler> records = invoicesListDoc.getFragmentsList("/ResultSetRecord");

			for (DocumentHandler doc : records) {
				String issueDate = DateTimeUtility.convertTimestampToDateString(doc.getAttribute("ISSUE_DATE"));
				//Rel 9.3 CR 1006 BEGIN
				String end2EndId = doc.getAttribute("END_TO_END_ID");
				String appReqInd = doc.getAttribute("PORTAL_APPROVAL_REQ_IND");
				String invClassification = doc.getAttribute("INVOICE_CLASSIFICATION");

				if(TradePortalConstants.INVOICE_TYPE_PAY_MGMT.equals(invClassification) && TradePortalConstants.INDICATOR_YES.equals(appReqInd) &&  StringFunction.isNotBlank(end2EndId)){
					// issue error that Payment Date cannot be modified
			        mediatorServices.getErrorManager().issueError(
			        		TradePortalConstants.ERR_CAT_1,
			                TradePortalConstants.PAYMENT_DATE_CANNOT_BE_MODIFIED);
			        return new DocumentHandler();
				}
				//Rel 9.3 CR 1006 END
				
				if ((DateTimeUtility.convertStringDateToDate(paymentDate)).before(DateTimeUtility.convertStringDateToDate(issueDate))) 
			      {
						mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.INVALID_INV_PAYMENT_DATE);
						return new DocumentHandler();
				  }
			}
		}	
		// Narayan IR - MJUM050951373 End
		String oldGroupDate = invoiceGroup.getAttribute("payment_date");
        invoiceGroup.setAttribute("payment_date", paymentDate);
		invoiceGroup.setAttribute("opt_lock", optLock);

		String newGroupOid = null;
		int success;
		if (StringFunction.isNotBlank(paymentDate) &&
				!paymentDate.equals(oldGroupDate)) {
			// check that another matching group does not already exist.
			newGroupOid = InvoiceUtility.findMatchingInvoiceGroup(invoiceGroup);

			if (StringFunction.isNotBlank(newGroupOid)
					&& !newGroupOid.equals(invoiceGroupOid)) {
				// there is another match, so assign the other group's OID
				// to these invoices and delete this group
				success = invoiceGroup.delete();
			}
			else {
				success = invoiceGroup.save();
			}
		}
		else {
			success = invoiceGroup.save();
		}

		boolean issuedSuccessMessage = false;

		if (success == 1)  {
			String sql = "select UPLOAD_INVOICE_OID, INVOICE_ID,LINKED_TO_INSTRUMENT_TYPE,LOAN_TYPE from INVOICES_SUMMARY_DATA where A_INVOICE_GROUP_OID = ? ";
			DocumentHandler	invoiceListDoc = DatabaseQueryBean.getXmlResultSet(sql, true, new Object[]{invoiceGroupOid});
			if (invoiceListDoc != null) {
				ArMatchingRule matchingRule = null;
				JPylonProperties jPylonProperties = JPylonProperties.getInstance();
				String serverLocation             = jPylonProperties.getString("serverLocation");
				BeanManager beanMgr = new BeanManager();
				beanMgr.setServerLocation(serverLocation);
				List<DocumentHandler> records = invoiceListDoc.getFragmentsList("/ResultSetRecord");

				for (DocumentHandler doc : records) {
					String invoiceOid = doc.getAttribute("/UPLOAD_INVOICE_OID");
					String invoiceId = doc.getAttribute("/INVOICE_ID");
					String instType = doc.getAttribute("/LINKED_TO_INSTRUMENT_TYPE");
					String loanType = doc.getAttribute("/LOAN_TYPE");
					if (StringFunction.isNotBlank(invoiceOid)) {
						InvoicesSummaryData invoiceSummaryData =
							(InvoicesSummaryData) mediatorServices.createServerEJB("InvoicesSummaryData",
									Long.parseLong(invoiceOid));
						
						if(InstrumentType.LOAN_RQST.equals(instType) && TradePortalConstants.TRADE_LOAN.equals(loanType)){
							CorporateOrganization corpObj = (CorporateOrganization)mediatorServices.createServerEJB("CorporateOrganization",
									Long.parseLong(corpOrgOid));
							
								if(TradePortalConstants.BANK_DEF_PAYMENT_ALLOW_NO.equals(corpObj.getAttribute("use_payment_date_allowed_ind"))){
									mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
											TradePortalConstants.TRADE_LOAN_RULES_RESTRICT_PAYMENT_DATE);

									return new DocumentHandler();
								}
						}
						// Each invoice in the group uses the same trading partner
						 if (matchingRule == null) {
							 if(TradePortalConstants.INVOICE_TYPE_REC_MGMT.equals(invoiceSummaryData.getAttribute("invoice_classification"))){
							matchingRule = invoiceSummaryData.getMatchingRule();
							 }
							String payAllow = matchingRule!=null?matchingRule.getAttribute("payment_day_allow"):"";
							// Ensure the trading partner rule allows Payment Date
							if (TradePortalConstants.REC.equals(instType) && TradePortalConstants.BANK_DEF_PAYMENT_ALLOW_NO.equals(payAllow)) {
								mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
										TradePortalConstants.PARTNER_RULES_RESTRICT_PAYMENT_DATE);

								return new DocumentHandler();
							}
						}

						String oldPaymentDate = invoiceSummaryData.getAttribute("payment_date");
						boolean isCompliant = true;
						try {
							if (StringFunction.isNotBlank(newGroupOid)) {
								invoiceSummaryData.setAttribute("invoice_group_oid",
										newGroupOid);
							}

							invoiceSummaryData.setAttribute("payment_date",
									paymentDate);
							if(InstrumentType.LOAN_RQST.equals(instType) && TradePortalConstants.TRADE_LOAN.equals(loanType))
							{
//								invoiceSummaryData.calculateInterestDiscount(corpOrgOid);
							}
							else if(TradePortalConstants.REC.equals(instType)){ 
								LOG.debug("calling old matching rule");
								invoiceSummaryData.calculateInterestDiscount(matchingRule,beanMgr);
							}
						}
						catch (AmsException e) {
							isCompliant = false;

							IssuedError is = e.getIssuedError();
							if (is != null) {
								String errorCode = is.getErrorCode();
								//BSL IR NNUM040526786 04/05/2012 BEGIN - add Pct Val To Fn Zero and combine three if branches w/identical logic
								if (TradePortalConstants.ERR_UPLOAD_INV_TENOR_DAYS_LESS.equals(errorCode) ||
										TradePortalConstants.ERR_UPLOAD_INV_DUE_PMT_DAYS_DIFF.equals(errorCode) ||
										TradePortalConstants.ERR_TRADE_INV_DUE_PMT_DAYS_DIFF.equals(errorCode) ||
										TradePortalConstants.ERR_UPLOAD_INV_PCT_VAL_TO_FIN_ZERO.equals(errorCode))
								{
									mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
											errorCode, invoiceId);

									// Reset payment date to its former value
									invoiceSummaryData.setAttribute("payment_date",
											oldPaymentDate);
								}
								//BSL IR NNUM040526786 04/05/2012 END
								else {
									mediatorServices.debug("InvoiceGroupMediatorBean.applyPaymentDate: Unexpected error: " + e.toString());
									throw e;
								}
							}
							else {
								mediatorServices.debug("InvoiceGroupMediatorBean.applyPaymentDate: Unexpected error: " + e.toString());
								throw e;
							}
						}

						String status = invoiceSummaryData.getAttribute("invoice_status");
						String tpName ="";
						 if(TradePortalConstants.INVOICE_TYPE_PAY_MGMT.equals(invoiceSummaryData.getAttribute("invoice_classification"))) {
							tpName = StringFunction.isNotBlank(invoiceSummaryData.getAttribute("seller_name"))?invoiceSummaryData.getAttribute("seller_name"):invoiceSummaryData.getAttribute("seller_id");
						 }
						 else{
							 if(matchingRule!=null)
									tpName =matchingRule.getAttribute("buyer_name"); 
							//IR 22580 - if no TP rule then fetch name from invoice	 
								 else
									 tpName= StringFunction.isNotBlank(invoiceSummaryData.getAttribute("buyer_name"))?invoiceSummaryData.getAttribute("buyer_name"):invoiceSummaryData.getAttribute("buyer_id");
						
						 }
						invoiceSummaryData.setAttribute("tp_rule_name", tpName);
						invoiceSummaryData.setAttribute("action",  TradePortalConstants.UPLOAD_INV_ACTION_APPLY_PAYMENT_DATE);
				        invoiceSummaryData.setAttribute("user_oid", userOid);
						int invoiceSuccess = invoiceSummaryData.save();
						if (isCompliant && invoiceSuccess == 1) {

							newGroupOid = invoiceSummaryData.getAttribute("invoice_group_oid");
							if(StringFunction.isNotBlank(newGroupOid)){
									updateAttachmentDetails(newGroupOid,mediatorServices);
							}
							if (!issuedSuccessMessage) {
								// display message
								mediatorServices.getErrorManager().issueError(InvoiceGroupMediator.class.getName(),
										TradePortalConstants.INV_ASSIGN_PAYMENT_DATE,
										mediatorServices.getResourceManager().getText("InvoiceGroups.InvoiceGroup", TradePortalConstants.TEXT_BUNDLE),
										groupDescription);

								issuedSuccessMessage = true;
							}
						}
					}
				}
			}
		}
		mediatorServices.debug("InvoiceGroupMediatorBean.applyPaymentDate: End: ");
		return new DocumentHandler();
	}

	/**
	 * Searches for an existing invoice group in the database that matches the
	 * supplied InvoiceGroup.
	 * @return OID of the Invoice Group if a match is found; otherwise null.
	 * @throws AmsException
	 * @throws RemoteException
	 */
	

	/**
	 * Attaches document to the invoice group
	 *
	 * @return com.amsinc.ecsg.util.DocumentHandler
	 * @param inputDoc
	 *            com.amsinc.ecsg.util.DocumentHandler
	 * @param mediatorServices
	 *            com.amsinc.ecsg.frame.MediatorServices
	 */
	private DocumentHandler attachDocument(DocumentHandler inputDoc,
			MediatorServices mediatorServices) throws RemoteException,
			AmsException {
		String securityRights = inputDoc.getAttribute("/InvoiceGroupList/securityRights");
		String buttonPressed = inputDoc.getAttribute("/Update/ButtonPressed");
		// ***********************************************************************
		// check to make sure that the user is allowed to attach documents to
		// the invoice - the user has to be a corporate user (not an admin
		// user) and have the rights to approve financing or authorize
		// ***********************************************************************


		if (!"PayAttachDocument".equals(buttonPressed) && !SecurityAccess.hasRights(securityRights, SecurityAccess.RECEIVABLES_APPROVE_FINANCING)
				&& !SecurityAccess.hasRights(securityRights, SecurityAccess.RECEIVABLES_AUTHORIZE_INVOICE)) {
			// user does not have appropriate authority.
			mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.ERR_UPLOAD_INV_CANNOT_ATTACH_DOC);
			return new DocumentHandler();
		}

		List<DocumentHandler> invoiceGroupList = inputDoc.getFragmentsList("/InvoiceGroupList/InvoiceGroup");

		if (invoiceGroupList.isEmpty()) {
			mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.NO_ITEM_SELECTED,
					mediatorServices.getResourceManager().getText(
							"UploadInvoiceAction.UploadInvoiceAttachDocumentText",
							TradePortalConstants.TEXT_BUNDLE));
			return new DocumentHandler();
		}

		// Loop through the list of invoice group OIDs
		for (DocumentHandler invoiceItem : invoiceGroupList) {
			String[] invoiceData = invoiceItem.getAttribute("InvoiceData").split("/");
			String invoiceGroupOid = invoiceData[0];
			String optLock = invoiceData[1];
			InvoiceGroup invoiceGroup = (InvoiceGroup) mediatorServices.createServerEJB("InvoiceGroup",
					Long.parseLong(invoiceGroupOid));

			String invoiceStatus = invoiceGroup.getAttribute("invoice_status");
			String groupDescription = invoiceGroup.getDescription();

		    // only attach documents to invoices with status other than 'Authorized', 'Financed', and 'Deleted'.
			if ((TradePortalConstants.INVOICE_TYPE_REC_MGMT.equals(invoiceGroup.getAttribute("invoice_classification")) 
					&& (!(TradePortalConstants.UPLOAD_INV_STATUS_AVL_FOR_FIN.equals(invoiceStatus)
					|| TradePortalConstants.UPLOAD_INV_STATUS_AVL_FOR_AUTH.equals(invoiceStatus)
					|| TradePortalConstants.UPLOAD_INV_STATUS_PAR_AUTH.equals(invoiceStatus)
					|| TradePortalConstants.UPLOAD_INV_STATUS_AUTH_FAILED.equals(invoiceStatus)
					|| TradePortalConstants.UPLOAD_INV_STATUS_FIN_AUTH_FAILED.equals(invoiceStatus)
					|| TradePortalConstants.UPLOAD_INV_STATUS_FIN_PAR_AUTH.equals(invoiceStatus)
					|| TradePortalConstants.UPLOAD_INV_STATUS_PEND_ACT_NO_INSTR_TYPE.equals(invoiceStatus)
					|| TradePortalConstants.UPLOAD_INV_STATUS_PEND_ACT_UNABLE_TO_FIN.equals(invoiceStatus)
 				    || TradePortalConstants.PAYABLE_UPLOAD_INV_STATUS_PENDING_ACTION.equals(invoiceStatus)
					|| TradePortalConstants.UPLOAD_INV_STATUS_PEND_ACT_CREDIT_NOTE.equals(invoiceStatus))))
					
					 || (TradePortalConstants.INVOICE_TYPE_PAY_MGMT.equals(invoiceGroup.getAttribute("invoice_classification")) 
								&& !(TradePortalConstants.UPLOAD_INV_STATUS_PEND_ACT.equals(invoiceStatus)
								|| TradePortalConstants.PAYABLE_UPLOAD_INV_STATUS_ASSIGNED.equals(invoiceStatus)
								|| TradePortalConstants.UPLOAD_INV_STATUS_AVL_FOR_AUTH.equals(invoiceStatus)
								|| TradePortalConstants.PAYABLE_UPLOAD_INV_STATUS_UNASSIGNED.equals(invoiceStatus))))
		    {
		   	    mediatorServices.getErrorManager().issueError(
		                UploadInvoiceMediator.class.getName(),
		                TradePortalConstants.ERR_UPLOAD_INV_CANNOT_ATTACH_DOC,
		                mediatorServices.getResourceManager().getText("InvoiceGroups.InvoiceGroup", TradePortalConstants.TEXT_BUNDLE),
		                groupDescription,
		                ReferenceDataManager.getRefDataMgr().getDescr("UPLOAD_INVOICE_STATUS", invoiceStatus, mediatorServices.getCSDB().getLocaleName()));
		    }

			//
			//AAlubala - IR#LSUM051041577
			//Only one doc attachment allowed per invoice group
			//If invoice group already has a doc attached, throw error
			boolean groupAlreadyHasDocAttached = groupAlreadyHasDocAttached(invoiceGroupOid);
			if (groupAlreadyHasDocAttached){
				mediatorServices.getErrorManager().issueError(
						InvoiceGroupMediator.class.getName(),
						TradePortalConstants.ERR_UPLOAD_INV_DOC_ALREADY_ATTACHED_TO_GRP,
						groupDescription);
			}			

			invoiceGroup.setAttribute("opt_lock", optLock);
			invoiceGroup.save();
		}

		return new DocumentHandler();
	}

	private String checkDocumentAlreadyAttached(String invoiceGroupOid, MediatorServices mediatorServices, String groupDescription)
	throws RemoteException, AmsException {
		String sql = "select doc_image_oid from document_image where p_transaction_oid in ( select upload_invoice_oid from invoices_summary_data where a_invoice_group_oid = ?)";
		DocumentHandler	imagesListDoc = DatabaseQueryBean.getXmlResultSet(sql, false, new Object[]{invoiceGroupOid});
		if (imagesListDoc != null){
			return imagesListDoc.getAttribute("/ResultSetRecord(0)/DOC_IMAGE_OID");
		}else{			
			mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.DOC_NOT_PRESENT_TO_DEL,
					groupDescription);
			
		 return null;	
		}
	}
	
	/**

	*This method returns @return true if an invoice in the group has a doc attached
	*
	**/
	private boolean groupAlreadyHasDocAttached(String invoiceGroupOid)
	throws RemoteException, AmsException {
		boolean docAttached = false;
		String sql = "select doc_image_oid from document_image where p_transaction_oid in (select upload_invoice_oid from invoices_summary_data where a_invoice_group_oid = ?)";
		DocumentHandler	imagesListDoc = DatabaseQueryBean.getXmlResultSet(sql, false, new Object[]{invoiceGroupOid});
		if (imagesListDoc != null){
			docAttached = true;
		}

		return docAttached;
	}	
 
	/** NAR IR- MAUM051043378
	 * This method performs the close action of all
	 * selected Invoices.
	 */
	private DocumentHandler closeInvoice(DocumentHandler inputDoc, MediatorServices mediatorServices)
	throws RemoteException, AmsException {
		mediatorServices.debug("InvoiceGroupMediatorBean.authorizeInvoice: Start: ");
		String userOid = inputDoc.getAttribute("/InvoiceGroupList/userOid");
		User user = (User) mediatorServices.createServerEJB("User",
				Long.parseLong(userOid));

		String userOrgOid = user.getAttribute("owner_org_oid");

		List<DocumentHandler> invoiceGroupList = inputDoc.getFragmentsList("/InvoiceGroupList/InvoiceGroup");
		if (invoiceGroupList.isEmpty()) {
			mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.NO_ITEM_SELECTED,
					mediatorServices.getResourceManager().getText("UploadInvoiceAction.CloseInvoices", TradePortalConstants.TEXT_BUNDLE));
			return new DocumentHandler();
		}
		mediatorServices.debug("InvoiceGroupMediatorBean: number of invoice groups selected: "
				+ invoiceGroupList.size());

		for (DocumentHandler invoiceGroupItem : invoiceGroupList) {

			String[] invoiceData = invoiceGroupItem.getAttribute("InvoiceData").split("/");
			String invoiceGroupOid = invoiceData[0];
			String optLock = invoiceData[1];

			InvoiceGroup invoiceGroup = (InvoiceGroup) mediatorServices.createServerEJB("InvoiceGroup",
					Long.parseLong(invoiceGroupOid));
			String invoiceStatus = invoiceGroup.getAttribute("invoice_status");
			String groupDescription = invoiceGroup.getDescription();

			// only unable to finance invoice status can be closed.
			if (!(TradePortalConstants.UPLOAD_INV_STATUS_PEND_ACT_UNABLE_TO_FIN.equals(invoiceStatus) || TradePortalConstants.UPLOAD_INV_STATUS_DBC.equals(invoiceStatus))) {
				mediatorServices.getErrorManager().issueError(
						InvoiceGroupMediator.class.getName(),
						TradePortalConstants.TRANSACTION_CANNOT_PROCESS,
						mediatorServices.getResourceManager().getText("InvoiceGroups.InvoiceGroup", TradePortalConstants.TEXT_BUNDLE),
						groupDescription,
						ReferenceDataManager.getRefDataMgr().getDescr("UPLOAD_INVOICE_STATUS", invoiceStatus, mediatorServices.getCSDB().getLocaleName()),
						mediatorServices.getResourceManager().getText("TransactionAction.Closed", TradePortalConstants.TEXT_BUNDLE));

				continue; // Move on to next Invoice Group
			}

			boolean groupHasBeenSaved = false;
			invoiceGroup.setAttribute("opt_lock", optLock);

			//CR 913Checking Threshold amount and daily limit.
			//Rpasupulati start
			String groupAmountsql = "select AMOUNT, CURRENCY from INVOICE_GROUP_VIEW where INVOICE_GROUP_OID = ?";
			DocumentHandler invoiceAmountDoc = DatabaseQueryBean.getXmlResultSet(groupAmountsql, true, new Object[]{invoiceGroupOid});
			String amount = "";
			String currency = "";
			
			if(invoiceAmountDoc !=null){
				currency = (invoiceAmountDoc.getFragment("/ResultSetRecord")).getAttribute("CURRENCY");
				amount = (invoiceAmountDoc.getFragment("/ResultSetRecord")).getAttribute("AMOUNT");
			}
			
			String corpOrgOid = invoiceGroup.getAttribute("corp_org_oid");
			String thresholdGroupOid = user.getAttribute("threshold_group_oid");
			
		   CorporateOrganization corpOrg = (CorporateOrganization) mediatorServices.createServerEJB("CorporateOrganization",
						Long.parseLong(corpOrgOid));
			
			if(StringFunction.isNotBlank(thresholdGroupOid)){
				String baseCurrency=corpOrg.getAttribute("base_currency_code");		
				String selectClause="select distinct upload_invoice_oid,currency as currency_code,amount as payment_amount from invoices_summary_data ";
				String clientBankOid = inputDoc.getAttribute("/InvoiceGroupList/clientBankOid");
					if(!(InvoiceUtility.checkThresholds(amount,currency,baseCurrency,
							"upload_rec_inv_thold","upload_rec_inv_dlimit",
							userOrgOid,clientBankOid,user,selectClause,false,"",
							mediatorServices))){
						//if errorsFound is true returning new DocumentHandler
						return new DocumentHandler();
				
			}
			}
			//CR 913Checking Threshold amount and daily limit.
			//Rpasupulati end
			String sql = "select UPLOAD_INVOICE_OID from INVOICES_SUMMARY_DATA where A_INVOICE_GROUP_OID = ?";
			DocumentHandler	invoiceListDoc = DatabaseQueryBean.getXmlResultSet(sql, true, new Object[]{invoiceGroupOid});
			
			if (invoiceListDoc != null) {
				List<DocumentHandler> records = invoiceListDoc.getFragmentsList("/ResultSetRecord");
				for (DocumentHandler doc : records) {
					String invoiceOid = doc.getAttribute("UPLOAD_INVOICE_OID");

					if (StringFunction.isNotBlank(invoiceOid)) {
						InvoicesSummaryData invoiceSummaryData = (InvoicesSummaryData) mediatorServices.createServerEJB("InvoicesSummaryData",
									                              Long.parseLong(invoiceOid));						

								if (!groupHasBeenSaved) {
									invoiceGroup.setAttribute("invoice_status", TradePortalConstants.UPLOAD_INV_STATUS_CLOSED);

									if (invoiceGroup.save() == 1) {
										groupHasBeenSaved = true;

										mediatorServices.getErrorManager().issueError(
												InvoiceGroupMediator.class.getName(),
												TradePortalConstants.TRANSACTION_PROCESSED,
												mediatorServices.getResourceManager().getText("InvoiceGroups.InvoiceGroup", TradePortalConstants.TEXT_BUNDLE),
												groupDescription,
												mediatorServices.getResourceManager().getText("TransactionAction.Closed", TradePortalConstants.TEXT_BUNDLE));
									}
								}

								invoiceSummaryData.setAttribute("invoice_status", TradePortalConstants.UPLOAD_INV_STATUS_CLOSED);
								invoiceSummaryData.setAttribute("action",  TradePortalConstants.UPLOAD_INV_ACTION_CLOSE);
						        invoiceSummaryData.setAttribute("user_oid", userOid);
								if(invoiceSummaryData.save() == 1){

									String newGroupOid = invoiceSummaryData.getAttribute("invoice_group_oid");
										if(StringFunction.isNotBlank(newGroupOid)){
											updateAttachmentDetails(newGroupOid,mediatorServices);
									}
										InvoiceUtility.createOutgoingQueueAndIssueSuccess(TradePortalConstants.UPLOAD_INV_STATUS_CLOSED,
											invoiceOid, false, mediatorServices);
								}
								continue;
						}						
				}
			}
		}
		return new DocumentHandler();
	}

	
	private DocumentHandler clearPaymentDate(DocumentHandler inputDoc, MediatorServices mediatorServices) 
	throws RemoteException, AmsException {
		mediatorServices.debug("InvoiceGroupMediatorBean.clearPaymentDate: Start: ");

		
		String userOid = inputDoc.getAttribute("/InvoiceGroupList/userOid");
		String securityRights = inputDoc.getAttribute("/InvoiceGroupList/securityRights");
		String buttonPressed = inputDoc.getAttribute("/Update/ButtonPressed");
		if( "PayClearPaymentDate".equals(buttonPressed) || 
					TradePortalConstants.PAY_PGM_BUTTON_CLEAR_PAYMENT_DATE.equals(buttonPressed)){
			if (!SecurityAccess.hasRights(securityRights, SecurityAccess.PAY_CLEAR_PAYMENT_DATE)) {
				// user does not have appropriate authority.
				mediatorServices.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.NO_ACTION_MATCH_NOTICE_AUTH,
						mediatorServices.getResourceManager().getText(
								"SecurityProfileDetail.PayClearPaymentDate",
								TradePortalConstants.TEXT_BUNDLE));
				return new DocumentHandler();
			}
		}
		else{//rec
			if (!SecurityAccess.hasRights(securityRights, SecurityAccess.RECEIVABLES_CLEAR_PAYMENT_DATE)) {
				// user does not have appropriate authority.
				mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.NO_ACTION_MATCH_NOTICE_AUTH,
					mediatorServices.getResourceManager().getText(
							"SecurityProfileDetail.PayClearPaymentDate",
							TradePortalConstants.TEXT_BUNDLE));
			return new DocumentHandler();
			}
		}
		
		List<DocumentHandler> invoiceGroupList = inputDoc.getFragmentsList("/InvoiceGroupList/InvoiceGroup");
		if (invoiceGroupList.isEmpty()) {
			mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.NO_ITEM_SELECTED,
					mediatorServices.getResourceManager().getText(
							"UploadInvoices.ClearPaymentDate",
							TradePortalConstants.TEXT_BUNDLE));
			return new DocumentHandler();
		}
		else if (invoiceGroupList.size() > 1) {
			mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.SELECT_ONLY_ONE_ITEM,
					mediatorServices.getResourceManager().getText("InvoiceGroups.InvoiceGroup",TradePortalConstants.TEXT_BUNDLE),
					mediatorServices.getResourceManager().getText("UploadInvoices.ClearPaymentDate",TradePortalConstants.TEXT_BUNDLE));
			return new DocumentHandler();
		}

		//There is only one Invoice Group to process
		DocumentHandler invoiceGroupItem = invoiceGroupList.get(0);
		String[] invoiceData = invoiceGroupItem.getAttribute("InvoiceData").split("/");
		String invoiceGroupOid = invoiceData[0];
		InvoiceGroup invoiceGroup = (InvoiceGroup) mediatorServices.createServerEJB("InvoiceGroup",
				Long.parseLong(invoiceGroupOid));
		String groupDescription = invoiceGroup.getDescription();
		String invoiceStatus = invoiceGroup.getAttribute("invoice_status");
		if(TradePortalConstants.PAY_PGM_BUTTON_CLEAR_PAYMENT_DATE.equals(buttonPressed)){
			if(!InvoiceUtility.isPayableInvoiceUpdatable(invoiceStatus)){
				mediatorServices.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,TradePortalConstants.PAYABLE_MGMT_INVALID_STATUS,groupDescription);
				return new DocumentHandler();
			}
		}

		boolean issuedSuccessMessage = false;
		String sql = "select UPLOAD_INVOICE_OID, INVOICE_ID from INVOICES_SUMMARY_DATA where A_INVOICE_GROUP_OID = ?";
		DocumentHandler	invoiceListDoc = DatabaseQueryBean.getXmlResultSet(sql, true, new Object[]{invoiceGroupOid});
		if (invoiceListDoc != null) {
			List<DocumentHandler> records = invoiceListDoc.getFragmentsList("/ResultSetRecord");

			for (DocumentHandler doc : records) {
				String invoiceOid = doc.getAttribute("/UPLOAD_INVOICE_OID");
				String invoiceId = doc.getAttribute("/INVOICE_ID");

				if (StringFunction.isNotBlank(invoiceOid)) {
					InvoicesSummaryData invoiceSummaryData =
						(InvoicesSummaryData) mediatorServices.createServerEJB("InvoicesSummaryData",
								Long.parseLong(invoiceOid));
				  // Clear payment until invoices are not linked to any instrument
				 if("R".equals(invoiceSummaryData.getAttribute("invoice_classification"))&& (StringFunction.isNotBlank(invoiceSummaryData.getAttribute("instrument_oid")) || StringFunction.isNotBlank(invoiceSummaryData.getAttribute("transaction_oid")))){
		      
		    	  mediatorServices.getErrorManager().issueError(	
    					  TradePortalConstants.ERR_CAT_1,
		    			  TradePortalConstants.REC_INVOICE_CLEAR_PAYMENT_NOT_ALLOW, 		    			 
		    			  invoiceId );
		    	  return new DocumentHandler();
				 } 
					String oldPaymentDate = invoiceSummaryData.getAttribute("payment_date");
					boolean isCompliant = true;
					try {						
						
						invoiceSummaryData.setAttribute("payment_date",	null);
					}
					catch (AmsException e) {
						isCompliant = false;

						IssuedError is = e.getIssuedError();
						if (is != null) {
							String errorCode = is.getErrorCode();
							//BSL IR NNUM040526786 04/05/2012 BEGIN - add Pct Val To Fn Zero and combine three if branches w/identical logic
							if (TradePortalConstants.ERR_UPLOAD_INV_TENOR_DAYS_LESS.equals(errorCode) ||
									TradePortalConstants.ERR_UPLOAD_INV_DUE_PMT_DAYS_DIFF.equals(errorCode) ||
									TradePortalConstants.ERR_UPLOAD_INV_PCT_VAL_TO_FIN_ZERO.equals(errorCode))
							{
								mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
										errorCode, invoiceId);

								// Reset payment date to its former value
								invoiceSummaryData.setAttribute("payment_date",
										oldPaymentDate);
							}
							//BSL IR NNUM040526786 04/05/2012 END
							else {
								mediatorServices.debug("InvoiceGroupMediatorBean.applyPaymentDate: Unexpected error: " + e.toString());
								throw e;
							}
						}
						else {
							mediatorServices.debug("InvoiceGroupMediatorBean.applyPaymentDate: Unexpected error: " + e.toString());
							throw e;
						}
					}
					 String tpName= invoiceSummaryData.getTpRuleName();
					invoiceSummaryData.setAttribute("tp_rule_name", tpName);
					invoiceSummaryData.setAttribute("action",  TradePortalConstants.UPLOAD_INV_ACTION_CLEAR_PAYMENT_DATE);
			        invoiceSummaryData.setAttribute("user_oid", userOid);
					int invoiceSuccess = invoiceSummaryData.save();
					if (isCompliant && invoiceSuccess == 1) {

						String newGroupOid = invoiceSummaryData.getAttribute("invoice_group_oid");
						if(StringFunction.isNotBlank(newGroupOid)){
								updateAttachmentDetails(newGroupOid,mediatorServices);
						}
						if (!issuedSuccessMessage) {
							// display message
							mediatorServices.getErrorManager().issueError(InvoiceGroupMediator.class.getName(),
									TradePortalConstants.INV_CLEAR_PAYMENT_DATE,
									mediatorServices.getResourceManager().getText("InvoiceGroups.InvoiceGroup", TradePortalConstants.TEXT_BUNDLE) +" - "+groupDescription);

							issuedSuccessMessage = true;
						}
					}
				}
				}
			}
	   return (new DocumentHandler());
      } 

	  private DocumentHandler assignLoanType(DocumentHandler inputDoc, MediatorServices mediatorServices) 
				throws RemoteException, AmsException {
			mediatorServices.debug("InvoiceGroupMediatorBean.assignLoanType: Start: ");
			String userOid = inputDoc.getAttribute("/InvoiceGroupList/userOid");
			String loanType = inputDoc.getAttribute("/InvoiceGroupList/loan_type");
			String corpOrgOid  = inputDoc.getAttribute("/InvoiceGroupList/ownerOrg");
			String securityRights = inputDoc.getAttribute("/InvoiceGroupList/securityRights");
			if (!SecurityAccess.hasRights(securityRights, SecurityAccess.RECEIVABLES_ASSIGN_LOAN_TYPE)) {
				// user does not have appropriate authority.
				mediatorServices.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.NO_ACTION_MATCH_NOTICE_AUTH,
						mediatorServices.getResourceManager().getText(
								"SecurityProfileDetail.PayAssignLoanType",
								TradePortalConstants.TEXT_BUNDLE));
				return new DocumentHandler();
			}
			List<DocumentHandler> invoiceGroupList = inputDoc.getFragmentsList("/InvoiceGroupList/InvoiceGroup");

			if (invoiceGroupList.isEmpty()) {
				mediatorServices.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.NO_ITEM_SELECTED,
						mediatorServices.getResourceManager().getText(
								"UploadInvoices.LoanType",
								TradePortalConstants.TEXT_BUNDLE));
				return new DocumentHandler();
			}
			mediatorServices.debug("InvoiceGroupMediatorBean: number of invoice groups selected: "
					+ invoiceGroupList.size());
		  
			// Process each Invoice Group in the list
			for (int i = 0; i < invoiceGroupList.size(); i++) {
				DocumentHandler invoiceGroupItem = invoiceGroupList.get(i);
				String[] invoiceData = invoiceGroupItem.getAttribute("InvoiceData").split("/");
				String invoiceGroupOid = invoiceData[0];
				String optLock = invoiceData[1];
				InvoiceGroup invoiceGroup = (InvoiceGroup) mediatorServices.createServerEJB("InvoiceGroup",
						Long.parseLong(invoiceGroupOid));
				String oldStatus = invoiceGroup.getAttribute("invoice_status");
				String instrType = invoiceGroup.getAttribute("linked_to_instrument_type");	
				String tradingPartnerName = invoiceGroup.getAttribute("trading_partner_name");	
				String groupDescription = invoiceGroup.getDescription();
				boolean groupHasBeenSaved = false;

				// only Pending Action - No Instr Type invoices can be assigned an
				// instrument type
				if(TradePortalConstants.INV_LINKED_INSTR_REC.equals(instrType)){

					   mediatorServices.getErrorManager().issueError(
			    			  UploadInvoiceMediator.class.getName(),
			    			  TradePortalConstants.REC_INVOICE_ASSIGN_LOAN_TYPE_NOT_ALLOW,tradingPartnerName);
			    	  return new DocumentHandler();
				  }
				if(TradePortalConstants.RECEIVABLE_UPLOAD_INV_STATUS_ASSIGNED.equals(oldStatus) || TradePortalConstants.PAYABLE_UPLOAD_INV_STATUS_DELETED.equals(oldStatus)){
					 mediatorServices.getErrorManager().issueError(
		     	               UploadInvoiceMediator.class.getName(),
		     	               TradePortalConstants.TRANSACTION_CANNOT_PROCESS, 
		     	               mediatorServices.getResourceManager().getText("InvoiceGroups.InvoiceGroup",TradePortalConstants.TEXT_BUNDLE),
		     	               groupDescription ,
		     	               ReferenceDataManager.getRefDataMgr().getDescr("UPLOAD_INVOICE_STATUS", oldStatus, mediatorServices.getCSDB().getLocaleName()),
		     	               mediatorServices.getResourceManager().getText("UploadInvoiceAction.AssignLoanType", TradePortalConstants.TEXT_BUNDLE));
					 return new DocumentHandler();
				}
				   // check if payment is null or not while assigning TRADE_LOAN 
				 boolean isPayDateNull = InvoiceUtility.isPaymentDateNull(invoiceGroupOid);
				 CorporateOrganization corporateOrganization = (CorporateOrganization) mediatorServices.createServerEJB("CorporateOrganization",
							Long.parseLong(corpOrgOid));
				 LOG.info("isPayDateNull:"+isPayDateNull);
				 
				if(TradePortalConstants.TRADE_LOAN.equals(loanType) && !isPayDateNull &&
						TradePortalConstants.BANK_DEF_PAYMENT_ALLOW_NO.equals(corporateOrganization.getAttribute("use_payment_date_allowed_ind"))){
					LOG.info("thorwing this error");
					mediatorServices.getErrorManager().issueError(
			    			  UploadInvoiceMediator.class.getName(),
			    			  TradePortalConstants.TRADE_LOAN_NO_PAYMENT_DATE,mediatorServices.getResourceManager().getText("InvoiceGroups.InvoiceGroup", TradePortalConstants.TEXT_BUNDLE) +" - "+tradingPartnerName);
			    	  return new DocumentHandler();
				}
					invoiceGroup.setAttribute("loan_type", loanType);
					invoiceGroup.setAttribute("opt_lock", optLock);
					
					String sql = "select UPLOAD_INVOICE_OID from INVOICES_SUMMARY_DATA where A_INVOICE_GROUP_OID = ? ";
					DocumentHandler	invoiceListDoc = DatabaseQueryBean.getXmlResultSet(sql, true, new Object[]{invoiceGroupOid});
					if (invoiceListDoc != null) {
						int success = 0;
						String newGroupOid = null;
						List<DocumentHandler> records = invoiceListDoc.getFragmentsList("/ResultSetRecord");

						for (DocumentHandler doc : records) {
							String invoiceOid = doc.getAttribute("UPLOAD_INVOICE_OID");

							if (StringFunction.isNotBlank(invoiceOid)) {
								InvoicesSummaryData invoiceSummaryData =
									(InvoicesSummaryData) mediatorServices.createServerEJB("InvoicesSummaryData",
											Long.parseLong(invoiceOid));
								String amount = invoiceSummaryData.getAttribute("amount");
								String newStatus = null;
									try {
										
										 if(StringFunction.isNotBlank(loanType) && TradePortalConstants.EXPORT_FIN.equals(loanType)){
											 LOG.debug("amount:"+amount);
											 invoiceSummaryData.calculateInterestDiscount(corpOrgOid);
										}
										else if(StringFunction.isNotBlank(loanType) && TradePortalConstants.TRADE_LOAN.equals(loanType)){
											LOG.debug("corpOrgOid:"+corpOrgOid);
									    }										
										else { 
											invoiceSummaryData.setAttribute("net_invoice_finance_amount", null);
										}
									}
									catch (AmsException e) {
										LOG.info("Inside exceptoin");
										IssuedError is = e.getIssuedError();
										if (is != null) {
											String errorCode = is.getErrorCode();
											LOG.info("errorCode:"+errorCode);
											//BSL IR NNUM040526786 04/05/2012 BEGIN - add Pct Val To Fn Zero and combine three if branches w/identical logic
											if (TradePortalConstants.ERR_UPLOAD_INV_TENOR_DAYS_LESS.equals(errorCode) ||
													TradePortalConstants.ERR_UPLOAD_INV_DUE_PMT_DAYS_DIFF.equals(errorCode) ||
													TradePortalConstants.ERR_TRADE_INV_DUE_PMT_DAYS_DIFF.equals(errorCode) ||
													TradePortalConstants.ERR_UPLOAD_INV_PCT_VAL_TO_FIN_ZERO.equals(errorCode))
											{
												   mediatorServices.getErrorManager().issueError(
											    			  UploadInvoiceMediator.class.getName(),
											    			  errorCode,invoiceSummaryData.getAttribute("invoice_id"));
											    	  return new DocumentHandler();

											}
											//BSL IR NNUM040526786 04/05/2012 END
											else {	LOG.info("InvoiceGroupMediatorBean.assignLoanType: Unexpected error: " + e.toString());
												mediatorServices.debug("InvoiceGroupMediatorBean.assignLoanType: Unexpected error: " + e.toString());
												throw e;
											}
										}
										else {	LOG.info("2 InvoiceGroupMediatorBean.assignLoanType: Unexpected error: " + e.toString());
											mediatorServices.debug("InvoiceGroupMediatorBean.assignLoanType: Unexpected error: " + e.toString());
											throw e;
										}
									}
								LOG.info("groupHasBeenSaved:"+groupHasBeenSaved);
									if (!groupHasBeenSaved) {
										invoiceGroup.setAttribute("loan_type", loanType);
											// check that another matching group does not already exist.
											newGroupOid = InvoiceUtility.findMatchingInvoiceGroup(invoiceGroup);
											LOG.info("newgroup:"+newGroupOid);
											if (StringFunction.isNotBlank(newGroupOid)
													&& !newGroupOid.equals(invoiceGroupOid)) {
												// there is another match, so assign the other group's OID
												// to these invoices and delete this group
												success = invoiceGroup.delete();
												LOG.info("success:"+success);
											}
											else {LOG.info("else :");
												success = invoiceGroup.save();
											}
										if (success == 1) {
											groupHasBeenSaved = true;

											mediatorServices.getErrorManager().issueError(InvoiceGroupMediator.class.getName(),
													TradePortalConstants.INV_ASSIGN_LOAN_TYPE,
													groupDescription);
										}
									}
									invoiceSummaryData.setAttribute("loan_type", loanType);
									 String tpName= invoiceSummaryData.getTpRuleName();
									invoiceSummaryData.setAttribute("tp_rule_name", tpName);
									if (StringFunction.isNotBlank(newGroupOid)) {
										invoiceSummaryData.setAttribute("invoice_group_oid",
												newGroupOid);
									}
									invoiceSummaryData.setAttribute("action",  TradePortalConstants.UPLOAD_INV_ACTION_ASSIGN_INSTR_TYPE);
							        invoiceSummaryData.setAttribute("user_oid", userOid);
									
									if (invoiceSummaryData.save() == 1) {

										newGroupOid = invoiceSummaryData.getAttribute("invoice_group_oid");
										if(StringFunction.isNotBlank(newGroupOid)){
											updateAttachmentDetails(newGroupOid,mediatorServices);
									}
								}
								}
							}
						}
					}
					mediatorServices.debug("InvoiceGroupMediatorBean.assignLoanType: End: ");
					return new DocumentHandler();
				}
	 
	  
		/**
		 * This method creates an ATP Instrument for Receivable invoices
		 * @param inputDoc
		 * @param mediatorServices
		 * @return DocumentHandler
		 * @throws RemoteException
		 * @throws AmsException
		 */
		private DocumentHandler createLoanRequest(DocumentHandler inputDoc,
				MediatorServices mediatorServices)
		throws RemoteException, AmsException {
			
			LOG.debug("InvoiceGroupMediatorBean.createLoanRequest: Start: "+inputDoc);		
			String userOid = inputDoc.getAttribute("/InvoiceGroupList/userOid");
			String securityRights = inputDoc.getAttribute("/InvoiceGroupList/securityRights");
			String clientBankOid = inputDoc.getAttribute("/InvoiceGroupList/clientBankOid");	
			String opOrgOid = inputDoc.getAttribute("/InvoiceGroupList/bankBranch");
			 Vector invoiceOidsVector = new Vector();		
			 DocumentHandler instrCreateDoc = null;
			 DocumentHandler  uploadInvoiceDoc = null;
			 String uploadInvOid= null;
			 String instrumentOid = null;
			 String transactionOid = null;
			 String corpOrgOid = null;		 
			 String firstInvoiceGroupOid = null;
			 String [] groupIds=null;
			 String newGroupOid = null;
			 Vector tempOids = new Vector();
			if (!(SecurityAccess.hasRights(securityRights, SecurityAccess.RECEIVABLES_CREATE_LOAN_REQ))) {
				// user does not have appropriate authority.
				mediatorServices.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.REC_INV_LRQ_NO_ACCESS);
				return new DocumentHandler();
			}
			
			List<DocumentHandler> invoiceGroupList = inputDoc.getFragmentsList("/InvoiceGroupList/InvoiceGroup");
			if (invoiceGroupList.isEmpty()) {
				mediatorServices.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.NO_ITEM_SELECTED,
						mediatorServices.getResourceManager().getText(
								"UploadInvoices.InvLRCreateLRQ",
								TradePortalConstants.TEXT_BUNDLE));
				return new DocumentHandler();
			}

			if(StringFunction.isBlank(opOrgOid)){

				mediatorServices.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.PAYABLE_INVOICE_BANK_BRANCH_REQ);
				return new DocumentHandler();
			}

			groupIds = new String[ invoiceGroupList.size()]; 		
			for (int i = 0; i < invoiceGroupList.size(); i++) {
				DocumentHandler invoiceGroupItem = invoiceGroupList.get(i);
			String[] invoiceData = invoiceGroupItem.getAttribute("InvoiceData").split("/");
			firstInvoiceGroupOid = invoiceData[0];
			groupIds[i]=invoiceData[0];	
			tempOids.add(invoiceData[0]);
			}
			 LOG.debug("tempOids"+tempOids);
			//LoanType check for the selected invoices
			 InvoiceGroup invoiceGroup = null;
			 String loanType= "";
			 if(tempOids.size()>0){
				 invoiceGroup = (InvoiceGroup) mediatorServices.createServerEJB("InvoiceGroup",
							Long.parseLong((String)tempOids.get(0)));
				String tpName = invoiceGroup.getDescription();
				loanType = invoiceGroup.getAttribute("loan_type");
				LOG.debug("tpName:"+tpName+"\t loanType:"+loanType);
				if(StringFunction.isBlank(loanType)){
					 mediatorServices.getErrorManager().issueError(
								TradePortalConstants.ERR_CAT_1,
								TradePortalConstants.INST_LOAN_TYPE_MISSED_GROUP);							
						return new DocumentHandler();
				 }
				String dupName = null;
				String dupLoanType = null;
				 for(int i=1;i<tempOids.size();i++){
					 invoiceGroup = (InvoiceGroup) mediatorServices.createServerEJB("InvoiceGroup",
								Long.parseLong((String)tempOids.get(i)));
					 dupName = 	invoiceGroup.getDescription();
					 dupLoanType = invoiceGroup.getAttribute("loan_type");
					 LOG.debug("dupName:"+dupName+"\t dupLoanType:"+dupLoanType);
					 if(StringFunction.isBlank(dupLoanType)){
						 mediatorServices.getErrorManager().issueError(
									TradePortalConstants.ERR_CAT_1,
									TradePortalConstants.INST_LOAN_TYPE_MISSED_GROUP);							
							return new DocumentHandler();
					 }
					 if(!loanType.equals(dupLoanType)){
						 mediatorServices.getErrorManager().issueError(
									TradePortalConstants.ERR_CAT_1,
									TradePortalConstants.LOAN_REQ_MULTIPLE_LOAN_TYPES_SELECTED);							
							return new DocumentHandler();
					 }
				 }
			 }

			if(firstInvoiceGroupOid!=null) {
			 invoiceGroup = (InvoiceGroup) mediatorServices.createServerEJB("InvoiceGroup",
						Long.parseLong(firstInvoiceGroupOid));			
			corpOrgOid = invoiceGroup.getAttribute("corp_org_oid");			
			// Retrieve the set of invoices for the passing group id
			Vector invoicesGroupDoc =  getInvoices(groupIds,corpOrgOid);
			
			if(invoicesGroupDoc!=null && invoicesGroupDoc.size()>0){
					 for(int pos = 0; pos<invoicesGroupDoc.size();pos++){
					 uploadInvoiceDoc = (DocumentHandler) invoicesGroupDoc.get(pos);	
				 
				String instrOid = uploadInvoiceDoc.getAttribute("/A_INSTRUMENT_OID");
				String tranOid = uploadInvoiceDoc.getAttribute("/A_TRANSACTION_OID");
				//if already associated then do not create an instrument
				if(StringFunction.isNotBlank(tranOid) && StringFunction.isNotBlank(instrOid)){
					Instrument	instr = (Instrument) mediatorServices.createServerEJB("Instrument",Long.parseLong(instrOid));
					mediatorServices.getErrorManager().issueError(PayableInvoiceGroupMediator.class.getName(),
							TradePortalConstants.INSTRUMENT_EXISTS,
							mediatorServices.getResourceManager().getText("UploadInvoices.LRQMessage", TradePortalConstants.TEXT_BUNDLE) +" - " +instr.getAttribute("complete_instrument_id"),uploadInvoiceDoc.getAttribute("/INVOICE_ID"));
					invoicesGroupDoc.removeElementAt(pos);
					pos--;
				}
					 }
			}
			if(invoicesGroupDoc==null || invoicesGroupDoc.size()==0)
			return new DocumentHandler();
			
			
			 InstrumentProperties instProp = new InstrumentProperties();
			 instProp.setCorporateOrgOid(corpOrgOid);
			 instProp.setSecurityRights(securityRights);
			 instProp.setUserOid(userOid);
			 instProp.setClientBankOid(clientBankOid);
			 instProp.setOpOrgOid(opOrgOid);
			 instrCreateDoc = processInvoiceGroups(instProp,invoicesGroupDoc,mediatorServices);
			 
			 String instType = instrCreateDoc!= null? instrCreateDoc.getAttribute("/instrumentType"):null;
			 mediatorServices.debug("instType:"+instType);
			
			 if(instType != null) {
				 if (invoicesGroupDoc != null) {
					 for (int j=0; j<invoicesGroupDoc.size(); j++) {
						 DocumentHandler row = (DocumentHandler) invoicesGroupDoc.elementAt(j);
						 String invOid = row.getAttribute("/UPLOAD_INVOICE_OID");
						 invoiceOidsVector.addElement(invOid);
					 }
			}
	        Map impFinBuyerBackMap = InvoiceUtility.getFinanceType(loanType, TradePortalConstants.INVOICE_TYPE_REC_MGMT );

	        String importIndicator = (String)impFinBuyerBackMap.get("importIndicator");
	        String financeType = (String)impFinBuyerBackMap.get("financeType");
	        String buyerBacked = (String)impFinBuyerBackMap.get("buyerBacked");
	 				 
			  // Create instrument using the document
			DocumentHandler doc = createLRQInstrument(instrCreateDoc, mediatorServices,invoiceOidsVector);
			transactionOid = doc.getAttribute("/transactionOid");
		    	if(transactionOid!=null){
		    			transaction = (Transaction) mediatorServices.createServerEJB("Transaction",Long.parseLong(transactionOid));
				        instrumentOid = transaction.getAttribute("instrument_oid");
				        LOG.debug("Instrument OID:"+instrumentOid);
				 }
		    	
		    	String completeInstrId = "";
		    		
		    	if(instrumentOid!=null){
			        newInstr = (Instrument) mediatorServices.createServerEJB("Instrument",Long.parseLong(instrumentOid));
			        completeInstrId = newInstr.getAttribute("complete_instrument_id");
			        if (StringFunction.isNotBlank(importIndicator)){
			        	newInstr.setAttribute("import_indicator", importIndicator);
			        }

			        LOG.debug("Instrument Num:"+completeInstrId);
		    	}
		    	
		    	Vector invoiceIDVector = new Vector();
		    	boolean success = true;
		    	
		    	terms = (Terms) transaction.getComponentHandle("CustomerEnteredTerms");
		        if (StringFunction.isNotBlank(financeType)){
		        	terms.setAttribute("finance_type", financeType);
		        }
		        if (StringFunction.isNotBlank(buyerBacked)){
		        	terms.setAttribute("financing_backed_by_buyer_ind", buyerBacked);
		        }

				ArMatchingRule rule = null;
				String tempDate = "";
				String tpName="";
				boolean isDiffCharges = false;
				InvoicesSummaryData invoice =
					(InvoicesSummaryData) mediatorServices.createServerEJB("InvoicesSummaryData",
							Long.parseLong((String)invoiceOidsVector.get(0)));
				
				 tempDate = StringFunction.isNotBlank(invoice
						.getAttribute("payment_date")) ? invoice
						.getAttribute("payment_date") : invoice.getAttribute("due_date");
				try(Connection con = DatabaseQueryBean.connect(false);
						PreparedStatement pStmt1 = con
								.prepareStatement("UPDATE INVOICES_SUMMARY_DATA  SET INVOICE_STATUS=?, A_INSTRUMENT_OID=?, A_TRANSACTION_OID=?, "
										+ "A_TERMS_OID=?, a_invoice_group_oid=? , status = ? ,invoice_finance_amount=? where upload_invoice_oid =?");
						PreparedStatement pStmt2 = con
								.prepareStatement("INSERT into INVOICE_HISTORY (INVOICE_HISTORY_OID, ACTION_DATETIME, ACTION, "
						+ "INVOICE_STATUS, P_UPLOAD_INVOICE_OID, A_USER_OID) VALUES (?,?,?,?,?,?)")) {
					
					con.setAutoCommit(false);
					//Rpasupulati IR T36000031466 adding invoice_finance_amount
					
					String charge = invoice.getAttribute("charges");
					for (int i = 0; i < invoiceOidsVector.size(); i++) {
					uploadInvOid =  (String) invoiceOidsVector.get(i);	
					
					InvoicesSummaryData inv =
							(InvoicesSummaryData) mediatorServices.createServerEJB("InvoicesSummaryData",
									Long.parseLong(uploadInvOid));
					invoiceIDVector.addElement(inv.getAttribute("invoice_id"));
					if(StringFunction.isNotBlank(inv.getAttribute("charges"))&& !inv.getAttribute("charges").equalsIgnoreCase(charge)){
							isDiffCharges = true;
					}
					inv.setAttribute("invoice_status", TradePortalConstants.PO_STATUS_ASSIGNED);
					inv.setAttribute("status", TradePortalConstants.PO_STATUS_ASSIGNED);
					//Rpasupulati IR T36000031466 start
					String financeAmt = null;
					if (TradePortalConstants.TRADE_LOAN_REC.equals(financeType)){
				    	CorporateOrganization corpOrg = (CorporateOrganization) mediatorServices.createServerEJB("CorporateOrganization");
				    	corpOrg.getData(Long.parseLong(corpOrgOid));
				    	String finPercent = corpOrg.getAttribute("inv_percent_to_fin_trade_loan");
				    	BigDecimal percentFinanceAllowed = StringFunction.isBlank(finPercent) ? BigDecimal.ZERO :
							new BigDecimal(finPercent).divide(new BigDecimal("100.0"),
									MathContext.DECIMAL64);

				    	finPercent = inv.getAttribute("amount");
						BigDecimal invoiceAmount = StringFunction.isBlank(finPercent) ? BigDecimal.ZERO :
							new BigDecimal(finPercent, MathContext.DECIMAL64);

						BigDecimal financeAmount = invoiceAmount.multiply(percentFinanceAllowed);
						financeAmt = financeAmount.toString();
				    	inv.setAttribute("invoice_finance_amount",financeAmount.toString());
				    	
					}
					//Rpasupulati IR T36000031466 end
					if(TradePortalConstants.INVOICE_TYPE_REC_MGMT.equals(invoice.getAttribute("invoice_classification"))){
			  			rule =invoice.getMatchingRule();
			  		 }
					tpName= inv.getTpRuleName(rule);
					
					inv.setAttribute("tp_rule_name",tpName);
					inv.verifyInvoiceGroup();
					newGroupOid = inv.getAttribute("invoice_group_oid");
					inv=null; 
					pStmt1.setString(1, TradePortalConstants.PO_STATUS_ASSIGNED);
					pStmt1.setString(2,instrumentOid);
					pStmt1.setString(3,transactionOid);
					pStmt1.setString(4,String.valueOf(terms.getAttribute("terms_oid")));
					pStmt1.setString(5,newGroupOid);
					pStmt1.setString(6,TradePortalConstants.PO_STATUS_ASSIGNED);
					pStmt1.setString(7,financeAmt );//Rpasupulati IR T36000031466
					pStmt1.setString(8,uploadInvOid);
					pStmt1.addBatch();
					pStmt1.clearParameters();
					InvoiceUtility.createPurchaseOrderHisotryLog(
							pStmt2,
							TradePortalConstants.PAYABLE_UPLOAD_INV_ACTION_LINK_INST,
							TradePortalConstants.PAYABLE_UPLOAD_INV_STATUS_ASSIGNED,
							(String) invoiceOidsVector.elementAt(i), userOid);
				}
					pStmt1.executeBatch();
					pStmt2.executeBatch();
					con.commit();
			
			} catch (AmsException e) {
				e.printStackTrace();
			} catch (SQLException e) {
				 success = false;
				e.printStackTrace();
			}
		   			// Success message to User
				if(success){
					if(newGroupOid!=null && !firstInvoiceGroupOid.equals(newGroupOid)){
	 					
	  					updateAttachmentDetails(newGroupOid,mediatorServices);
				}
		   			mediatorServices.getErrorManager().issueError(PayableInvoiceGroupMediator.class.getName(),
							TradePortalConstants.INSERT_SUCCESSFUL,
							mediatorServices.getResourceManager().getText("UploadInvoices.LRQMessage", TradePortalConstants.TEXT_BUNDLE) +" - " +completeInstrId);
		    	   }else
		    	   {
		    		   mediatorServices.getErrorManager().issueError(InvoiceGroupMediator.class.getName(),
								TradePortalConstants.PAYABLE_INV_ATP_FAILED);
		    	   }
			  
		   		long oid = terms.newComponent("FirstTermsParty");
		  		TermsParty termsParty = (TermsParty) terms
		  				.getComponentHandle("FirstTermsParty");
		  		LOG.debug("termsParty handle acquired -> "
		  				+ termsParty.getAttribute("terms_party_oid"));
	    		InvoiceUtility.deriveTransactionCurrencyAndAmountsFromInvoice(
		  				transaction, terms, invoiceOidsVector, newInstr,invoiceIDVector,"");
		  		terms.setAttribute("invoice_only_ind",
		  				TradePortalConstants.INDICATOR_YES);
		  		SimpleDateFormat jPylon = new SimpleDateFormat("MM/dd/yyyy");
		  		
		  		if(isDiffCharges){
		  			terms.setAttribute("bank_charges_type",TradePortalConstants.CHARGE_UPLOAD_FW_SHARE);
		  		}else
		  			terms.setAttribute("bank_charges_type",invoice.getAttribute("charges"));
		  		
		  		try {
		  			if (!StringFunction.isBlank(tempDate)) {
		  				Date date = jPylon.parse(tempDate);
		  				terms.setAttribute("invoice_due_date",
		  						DateTimeUtility.convertDateToDateTimeString(date));
		  			}
		  		} catch (Exception e) {
		  			e.printStackTrace();
		  		}

		  		TermsParty firstTermsParty = (TermsParty) terms
				.getComponentHandle("FirstTermsParty");
				if(!(TradePortalConstants.EXPORT_FIN.equals(invoice.getAttribute("loan_type"))
						|| TradePortalConstants.SELLER_REQUESTED_FINANCING.equals(financeType))){
				firstTermsParty.setAttribute("name", tpName);
				if(rule!=null){
				firstTermsParty.setAttribute("address_line_1",rule.getAttribute("address_line_1"));
				firstTermsParty.setAttribute("address_line_2",rule.getAttribute("address_line_2"));
				firstTermsParty.setAttribute("address_city",rule.getAttribute("address_city"));
				firstTermsParty.setAttribute("address_state_province",rule.getAttribute("address_state"));
				firstTermsParty.setAttribute("address_country",rule.getAttribute("address_country"));
				firstTermsParty.setAttribute("address_postal_code",rule.getAttribute("postalcode"));
				}
				}
		        if (TradePortalConstants.TRADE_LOAN_REC.equals(financeType)){
			    	CorporateOrganization corpOrg = (CorporateOrganization) mediatorServices.createServerEJB("CorporateOrganization");
			    	corpOrg.getData(Long.valueOf(corpOrgOid));
			    	String attributeValue = corpOrg.getAttribute("inv_percent_to_fin_trade_loan");
			    	
			    	BigDecimal finPCTValue = BigDecimal.ZERO;
			    	if ((attributeValue != null)&&(!attributeValue.equals(""))) {
			    		finPCTValue = (new BigDecimal(attributeValue)).divide(new BigDecimal(100.00));
			    	}
			    	if (finPCTValue.compareTo(BigDecimal.ZERO) != 0){
			    		BigDecimal tempAmount = BigDecimal.ZERO;
			    		BigDecimal termAmount = terms.getAttributeDecimal("amount") ;
			    		tempAmount = finPCTValue.multiply(termAmount);
						String trnxCurrencyCode = transaction.getAttribute("copy_of_currency_code");
						int numberOfCurrencyPlaces = 2;

						if (trnxCurrencyCode != null && !trnxCurrencyCode.equals("")) {
							numberOfCurrencyPlaces = Integer.parseInt(ReferenceDataManager
									.getRefDataMgr().getAdditionalValue(
											TradePortalConstants.CURRENCY_CODE,
											trnxCurrencyCode));
						}
						tempAmount = tempAmount.setScale(numberOfCurrencyPlaces,
								BigDecimal.ROUND_HALF_UP);
			    		terms.setAttribute("amount", String.valueOf(tempAmount));
			    	}
			    
			    	
		        }
		  		// Set the counterparty for the instrument and some "copy of" fields
		  		newInstr.setAttribute("a_counter_party_oid",
		  				terms.getAttribute("c_FirstTermsParty"));
		  		transaction
		  				.setAttribute("copy_of_amount", terms.getAttribute("amount"));
		  		transaction.setAttribute("instrument_amount",
		  				terms.getAttribute("amount"));
		  		newInstr.setAttribute("copy_of_instrument_amount",
		  				transaction.getAttribute("instrument_amount"));

		  		if (terms.isAttributeRegistered("expiry_date")) {
		  			newInstr.setAttribute("copy_of_expiry_date",
		  					terms.getAttribute("expiry_date"));
		  		}

		  		if (terms.isAttributeRegistered("reference_number")) {
		  			newInstr.setAttribute("copy_of_ref_num",
		  					terms.getAttribute("reference_number"));
		  		}
				if (TradePortalConstants.INDICATOR_NO.equals(newInstr.getAttribute("import_indicator")) || 
						TradePortalConstants.INDICATOR_X.equals(newInstr.getAttribute("import_indicator"))){
			        String requiredOid = "upload_invoice_oid";
					 String invoiceOidsSql = POLineItemUtility.getPOLineItemOidsSql(
							 invoiceOidsVector, requiredOid);
			        
 
					 boolean useSeller = TradePortalConstants.BUYER_REQUESTED_FINANCING.equals(terms.getAttribute("finance_type")) ;
					String invoiceDueDate = null;
					if (InvoiceUtility.validateSameCurrency(invoiceOidsSql) && 
							InvoiceUtility.validateSameDate(invoiceOidsSql) && 
							InvoiceUtility.validateSameTradingPartner(invoiceOidsSql, useSeller)){
						StringBuilder dueDateSQL = new StringBuilder("select distinct(case when payment_date is null then due_date else payment_date end ) DUEDATE " +
								" from invoices_summary_data where ");
						dueDateSQL.append(invoiceOidsSql);
						DocumentHandler resultDoc = DatabaseQueryBean.getXmlResultSet(dueDateSQL.toString(), false, new ArrayList<Object>());
						invoiceDueDate = resultDoc.getAttribute("/ResultSetRecord(0)/DUEDATE");
					}
					

					if (StringFunction.isNotBlank(invoiceDueDate)){

						try {
							SimpleDateFormat iso = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
							SimpleDateFormat jPylon1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
							
								Date date = jPylon1.parse(invoiceDueDate);

								terms.setAttribute("loan_terms_fixed_maturity_dt",TPDateTimeUtility.convertISODateToJPylonDate(iso.format(date)));
								
						} catch (Exception e) {
							e.printStackTrace();
						}

						
					}
				}
		  		
		  		transaction.save(false);
		  		LOG.debug("Saving instrument with oid '"
		  				+ newInstr.getAttribute("instrument_oid") + "'...");
		  		newInstr.save(false);// save without validate
		  		LOG.debug("Instrument saved");
			  }
			}
			LOG.debug("InvoiceGroupMediatorBean.createLoanRequest: End: ");
			return new DocumentHandler();
			
		}
		
		/**
		 * This method returns Invoices for a selected Invoice Group
		 * @param groupIds
		 * @param corpOrgOid
		 * @return Vector
		 * @throws AmsException
		 * @throws RemoteException
		 */
		public Vector getInvoices(String[] groupIds,String corpOrgOid) throws AmsException, RemoteException {		
				
			//Vector grpRecords = null;
			Vector rows = null;
			StringBuilder invGroupSql = new StringBuilder();
			StringBuilder whereClause = new StringBuilder(200);
			if(groupIds!=null && groupIds.length>0){
				for(int start=0;start<groupIds.length;start++){
					whereClause.append(groupIds[start]).append(", ");
				}
			}
			String finalWhereClause = "";
			if(whereClause.toString().length()>0){
				finalWhereClause = whereClause.toString().substring(0,whereClause.toString().length()-2);
			}
			invGroupSql.append(" select UPLOAD_INVOICE_OID,BUYER_NAME,CURRENCY,AMOUNT,DUE_DATE,PAYMENT_DATE,INVOICE_ID,A_INSTRUMENT_OID,A_TRANSACTION_OID," +
					" INVOICE_STATUS,A_CORP_ORG_OID,LOAN_TYPE,LINKED_TO_INSTRUMENT_TYPE from invoices_summary_data where A_INVOICE_GROUP_OID in ( "+finalWhereClause+" ) and a_corp_org_oid=?");

			DocumentHandler	invoicesGroupDoc = DatabaseQueryBean.getXmlResultSet(invGroupSql.toString(), false, new Object[]{corpOrgOid}); 
			if(invoicesGroupDoc!=null)
				rows = invoicesGroupDoc.getFragments("/ResultSetRecord");
			return rows;	
		}
	  
		
		 /**
		  * This method invokes the Instrument EJB.
		  * successful creation of a new Instrument returns a transction_oid
		  * @param inputDoc
		  * @param mediatorServices
		  * @return String
		  * @throws AmsException
		  * @throws RemoteException
		  */
		 private DocumentHandler createLRQInstrument(DocumentHandler inputDoc, MediatorServices mediatorServices,Vector invOids)
			      throws AmsException, RemoteException {			
			 	 LOG.debug("createLRQInstrument()->"+inputDoc+"\t:"+invOids);
			     newInstr = (Instrument) mediatorServices.createServerEJB("Instrument");	
			     DocumentHandler outputDoc = new DocumentHandler();

			        String benName              = inputDoc.getAttribute("/benName"); //the ben_name of the invoice line items
			        String currency             = inputDoc.getAttribute("/currency"); //the currency of the invoice line items
			        String newTransactionOid    = null; //the oid of the transaction created for these invoice line items
			        
			        //Create a new instrument
			        newTransactionOid = newInstr.createNew(inputDoc);
			        LOG.debug("New instrument created with an original transacion oid of -> " + newTransactionOid);

			        if (newTransactionOid == null)
			        {
			        	LOG.debug("newTransactionOid is null.  Error in this.createNew()");
			            return outputDoc; //outputDoc is null
			        }
			        else
			            outputDoc.setAttribute("/transactionOid", newTransactionOid);

			        LOG.debug("ben_name     = " + benName);
			        LOG.debug("currency     = " + currency);

			        /************************************************************************
			         * Instantiate the transaction, terms, and termsParty and set the fields
			         * derived from the po line item data
			         ************************************************************************/

			        //get a handle to the terms object through the transaction then set the values on terms
			        //we'll also have to create a termsParty object and associate it to the terms object
			        Transaction transaction = (Transaction) newInstr.getComponentHandle("TransactionList", Long.valueOf(newTransactionOid));
			        LOG.debug("Transaction handle acquired -> " + transaction.getAttribute("transaction_oid"));

			       
			        try
			        {
			            terms = (Terms) transaction.getComponentHandle("CustomerEnteredTerms");
			            LOG.debug("terms handle acquired -> " + terms.getAttribute("terms_oid"));
			            outputDoc.setAttribute("/termsOid", terms.getAttribute("terms_oid"));
			        }
			        catch (Exception e)
			        {
			            LOG.debug("Caught error getting handle to terms");
			            e.printStackTrace();
			        }

			        long oid = terms.newComponent("FirstTermsParty");
			        TermsParty termsParty = (TermsParty) terms.getComponentHandle("FirstTermsParty");
			        LOG.debug("termsParty handle acquired -> " + termsParty.getAttribute("terms_party_oid"));

			        return outputDoc;
			  
		 }
	
		private class InstrumentProperties {
			/**
			 * This class holds the properties to be set for Instrument EJB
			 */
			 private String corporateOrgOid = null;
			 private String securityRights = null;
			 private String userOid = null;
			 private String benName = null;
			 private String currency =null;
			 private BigDecimal amount = null;
			 private String shipDate = null;
			 private String instrumentOid = null;
			 private String opOrgOid = null;
			 private String clientBankOid = null;
			public String getClientBankOid() {
				return clientBankOid;
			}
			public void setClientBankOid(String clientBankOid) {
				this.clientBankOid = clientBankOid;
			}
			public String getOpOrgOid() {
				return opOrgOid;
			}
			public void setOpOrgOid(String opOrgOid) {
				this.opOrgOid = opOrgOid;
			}
			public String getInstrumentOid() {
				return instrumentOid;
			}
			public void setInstrumentOid(String instrumentOid) {
				this.instrumentOid = instrumentOid;
			}
			public String getCorporateOrgOid() {
				return corporateOrgOid;
			}
			public void setCorporateOrgOid(String corporateOrgOid) {
				this.corporateOrgOid = corporateOrgOid;
			}
			public String getSecurityRights() {
				return securityRights;
			}
			public void setSecurityRights(String securityRights) {
				this.securityRights = securityRights;
			}
			public String getUserOid() {
				return userOid;
			}
			public void setUserOid(String userOid) {
				this.userOid = userOid;
			}
			public String getBenName() {
				return benName;
			}
			public void setBenName(String benName) {
				this.benName = benName;
			}
			public String getCurrency() {
				return currency;
			}
			public void setCurrency(String currency) {
				this.currency = currency;
			}
			public BigDecimal getAmount() {
				return amount;
			}
			public void setAmount(BigDecimal amount) {
				this.amount = amount;
			}
			public String getShipDate() {
				return shipDate;
			}
			public void setShipDate(String shipDate) {
				this.shipDate = shipDate;
			}
		 }

		/**
		 * This method loops through the invoices of a selected invoice group
		 * and validate based on Seller name/Currency name/Amount
		 * @param instrumentProp
		 * @param invGroupList
		 * @param mediatorServices
		 * @return DocumentHandler
		 * @throws RemoteException
		 * @throws AmsException
		 */
		private DocumentHandler processInvoiceGroups(InstrumentProperties instrumentProp, Vector invGroupList, MediatorServices mediatorServices)
			      throws RemoteException, AmsException {		
			   
			   LOG.debug("processInvoiceGroups()-> "+invGroupList);
			   String buyerName = null;
			   String currency =null;
			   String paymentDate = null;		
			   String dueDate = null;
			   String loanType = null;
			   String invalidBuyerName = null;
			   String invalidCurrency = null;
			   String invalidPaymentDate = null;
			   String invalidDueDate = null;				   
			   DocumentHandler invGroupDoc = null;
			   DocumentHandler nxtInvGroupDoc = null;
			   DocumentHandler instrCreateDoc = null;
			   BigDecimal amount = BigDecimal.ZERO;
			   BigDecimal nextAmount = BigDecimal.ZERO;
			   String amt = null;
			   BigDecimal totalAmount = BigDecimal.ZERO;
			   String date = null;
			   String invalidDate = null;
			   if(invGroupList!=null && invGroupList.size()>0){
			   int pos = 0;   		 
					  
					  invGroupDoc = (DocumentHandler) invGroupList.get(pos);	
					 
					  buyerName = invGroupDoc.getAttribute("/BUYER_NAME");				  
					  currency =  invGroupDoc.getAttribute("/CURRENCY");				
					  paymentDate = invGroupDoc.getAttribute("/PAYMENT_DATE");		
					  dueDate = invGroupDoc.getAttribute("/DUE_DATE");	
					  loanType = invGroupDoc.getAttribute("/LOAN_TYPE");	
					  if(StringFunction.isBlank(paymentDate))
						  date = dueDate; 
					  else
						  date = paymentDate;
					  LOG.debug("date:"+date);
					  amt = invGroupDoc.getAttribute("/AMOUNT");				  
					  if(amt!=null)
						  amount = BigDecimal.valueOf(Double.parseDouble(amt));					 
					  totalAmount = amount;
					  int subIdx = pos+1;

				        while (subIdx < invGroupList.size()) {
				            	
				        		nxtInvGroupDoc = (DocumentHandler) invGroupList.get(subIdx);			        		
				        		invalidBuyerName = nxtInvGroupDoc.getAttribute("/BUYER_NAME");
				            	invalidCurrency =  nxtInvGroupDoc.getAttribute("/CURRENCY");
				            	invalidPaymentDate = nxtInvGroupDoc.getAttribute("/PAYMENT_DATE");
				            	invalidDueDate = nxtInvGroupDoc.getAttribute("/DUE_DATE");	
								String am =  nxtInvGroupDoc.getAttribute("/AMOUNT");
								if(StringFunction.isBlank(invalidPaymentDate))
									invalidDate = invalidDueDate;
								else
									invalidDate =invalidPaymentDate;
								LOG.debug("invalid Date:"+invalidDate);
								 if(am!=null)
									  nextAmount = BigDecimal.valueOf(Double.parseDouble(am));	
								 LOG.debug("loanType:"+loanType);
								 if(TradePortalConstants.TRADE_LOAN.equals(loanType)){
									  if(!currency.equals(invalidCurrency)){
									 //if(!currency.equals(invalidCurrency)){
										 mediatorServices.getErrorManager().issueError(
													TradePortalConstants.ERR_CAT_1,
													TradePortalConstants.INVALID_LOAN_REQ_TRADE_LOAN_GROUP);
											return new DocumentHandler();
									 }
									 else{
											totalAmount = totalAmount.add(nextAmount);
											
									}
								 }else if(TradePortalConstants.EXPORT_FIN.equals(loanType) || TradePortalConstants.INV_FIN_REC.equals(loanType)){
									if(!(buyerName.equals(invalidBuyerName) && currency.equals(invalidCurrency) && date.equals(invalidDate))){								  
									  mediatorServices.getErrorManager().issueError(
												TradePortalConstants.ERR_CAT_1,
												TradePortalConstants.INVALID_LOAN_REQ_EXP_FINANCE_GROUP);
										return new DocumentHandler();
								}else{
										totalAmount = totalAmount.add(nextAmount);
										
								}
							  }
								 LOG.info("subIdx ++ "+subIdx);
								 subIdx++;
				            }
				       
					  instrumentProp.setAmount(totalAmount);
					  instrumentProp.setCurrency(currency);
					  instrumentProp.setShipDate(date);
					  instrumentProp.setBenName(buyerName);
					
					  // Build the input document used to create a new instrument
				      instrCreateDoc = buildInstrumentCreateDoc(instrumentProp);    
				  
			   }
			   
			return instrCreateDoc;
		  }
		
		/**
		 * This method finds the attachments available or not for the new group
		 * and updates the Attachment_Ind for that group respectively.
		 * Use this method for group level
		 * @param newGroupOid
		 * @param mediatorServices
		 * @return
		 * @throws AmsException
		 * @throws RemoteException
		 */
		private int updateAttachmentDetails(String newGroupOid, MediatorServices mediatorServices) {
			
			int status = 0;
			LOG.debug("updateAttachmentDetails() : newGroupOid:"+newGroupOid);
			String attachmentAvailable = TradePortalConstants.INDICATOR_NO;
			String sql = "select count(i.UPLOAD_INVOICE_OID) as COUNT from INVOICES_SUMMARY_DATA i left outer join DOCUMENT_IMAGE d on i.UPLOAD_INVOICE_OID = d.P_TRANSACTION_OID where i.A_INVOICE_GROUP_OID = ? "
					+ "and i.UPLOAD_INVOICE_OID is not null and d.DOC_IMAGE_OID is not null";

			DocumentHandler invoiceListDoc;
			try {
				invoiceListDoc = DatabaseQueryBean.getXmlResultSet(sql, false, new Object[]{newGroupOid});
			
			if (invoiceListDoc != null) {
				int count = invoiceListDoc.getAttributeInt("/ResultSetRecord(0)/COUNT");
					if (count>0) { 	
						attachmentAvailable =  TradePortalConstants.INDICATOR_YES;
					}
				
				LOG.debug("attachmentAvailable:"+attachmentAvailable);
				String sqlupt = "UPDATE invoice_group SET attachment_ind = ? WHERE invoice_group_oid=?";
				int resultCount = DatabaseQueryBean.executeUpdate(sqlupt, false, new Object[]{attachmentAvailable,newGroupOid});
				LOG.debug("resultCount:"+resultCount);
				
				}
			}catch(AmsException | NumberFormatException | SQLException e){
					//it just something wrong in showing attachment ind, 
					e.printStackTrace();
				}
			return status;
		}
		
		private int updateAttachmentDetails(Collection<String> c, MediatorServices mediatorServices,String invoiceGroupOid) {
			
			String newGroupOid="";
			int status = 0;
		    Iterator<String> itr = c.iterator();
		    while (itr.hasNext()) {
		    	 newGroupOid= itr.next();
		    if(!invoiceGroupOid.equals(newGroupOid)){
			
			String attachmentAvailable = TradePortalConstants.INDICATOR_NO;
			String sql = "select count(i.UPLOAD_INVOICE_OID) as COUNT from INVOICES_SUMMARY_DATA i left outer join DOCUMENT_IMAGE d on i.UPLOAD_INVOICE_OID = d.P_TRANSACTION_OID "
 + "where i.A_INVOICE_GROUP_OID = ? and i.UPLOAD_INVOICE_OID is not null and d.DOC_IMAGE_OID is not null ";
			
			try{
			DocumentHandler	invoiceListDoc = DatabaseQueryBean.getXmlResultSet(sql, false, new Object[]{newGroupOid});
			if (invoiceListDoc != null) {
				int count = invoiceListDoc.getAttributeInt("/ResultSetRecord(0)/COUNT");
					if (count>0) { 	
						attachmentAvailable =  TradePortalConstants.INDICATOR_YES;
					}
				
				LOG.debug("attachmentAvailable:"+attachmentAvailable);
				String sqlupt = "UPDATE invoice_group SET attachment_ind = ? WHERE invoice_group_oid=?";
				int resultCount = DatabaseQueryBean.executeUpdate(sqlupt, false, new Object[]{attachmentAvailable,newGroupOid});
				LOG.debug("resultCount:"+resultCount);
				
				}
			}
			catch(AmsException | NumberFormatException |SQLException e){
				//its just something wrong in showing attachment ind, 
				e.printStackTrace();
			}
		    }
			
		    }
			return status;
		}
	
		/**
		 * This method prepares instrument document which will be input to
		 * Instrument EJB for creation a new ATP instrument
		 * @param instrumentProp
		 * @return DocumentHandler
		 * @throws AmsException
		 */
		private DocumentHandler buildInstrumentCreateDoc(InstrumentProperties instrumentProp) throws AmsException {
			 	
				
			    DocumentHandler instrCreateDoc = new DocumentHandler();			   
			    instrCreateDoc.setAttribute("/instrumentOid", null); 		   
				instrCreateDoc.setAttribute("/instrumentType",InstrumentType.LOAN_RQST);
				instrCreateDoc.setAttribute("/clientBankOid", instrumentProp.getClientBankOid());
				instrCreateDoc.setAttribute("/bankBranch", instrumentProp.getOpOrgOid());
			    instrCreateDoc.setAttribute("/copyType", TradePortalConstants.FROM_BLANK);		   
			    instrCreateDoc.setAttribute("/userOid", instrumentProp.getUserOid());
			    instrCreateDoc.setAttribute("/ownerOrg", instrumentProp.getCorporateOrgOid());
			    instrCreateDoc.setAttribute("/securityRights", instrumentProp.getSecurityRights());
			    instrCreateDoc.setAttribute("/benName", instrumentProp.getBenName());
			    instrCreateDoc.setAttribute("/currency", instrumentProp.getCurrency());
			    instrCreateDoc.setAttribute("/shipDate", instrumentProp.getShipDate());
			    instrCreateDoc.setAttributeDecimal("/amount", instrumentProp.getAmount());	 
			    LOG.debug("instrCreateDoc:"+instrCreateDoc);
			return instrCreateDoc;
			 
		 }
		
		/**
		  * This method handles the creation of Loan Request Instrument based on rules.
		  *  These rules are based on Loan Creation Rules.
		  * @param inputDoc
		  * @param mediatorServices
		  * @return
		  * @throws RemoteException
		  * @throws AmsException
		  */
		 private DocumentHandler createLoanRequestByRules(DocumentHandler inputDoc, MediatorServices mediatorServices) 
		throws RemoteException, AmsException {
			LOG.debug("InvoiceGroupMediatorBean.createLoanRequestByRules()...."+inputDoc);
			Vector invoiceList = null;
			Vector invoiceOidList = new Vector();
			Vector invoiceOidsVector = new Vector();
			String groupIds[] = null;
			String invoiceOid = null;
			DocumentHandler invoiceItem = null;
			DocumentHandler outputDoc =  new DocumentHandler();
			DocumentHandler  uploadInvoiceDoc = null;
			boolean success =false;
			int count = 0;
			Vector tempOids = new Vector();
			invoiceList				  = inputDoc.getFragments("/InvoiceGroupList/InvoiceGroup");
			String userOid			  = inputDoc.getAttribute("/InvoiceGroupList/userOid");
			String loanType			  = inputDoc.getAttribute("/InvoiceGroupList/loan_type");
			String securityRights	  = inputDoc.getAttribute("/InvoiceGroupList/securityRights");
			String clientBankOid	  = inputDoc.getAttribute("/InvoiceGroupList/clientBankOid");	
			String corpOrgOid 		  = inputDoc.getAttribute("/InvoiceGroupList/ownerOrg");
			String timeZone 		  = inputDoc.getAttribute("/InvoiceGroupList/timeZone");
			String currencyCode 	  = inputDoc.getAttribute("/InvoiceGroupList/baseCurrencyCode");		
			if (!(SecurityAccess.hasRights(securityRights, SecurityAccess.RECEIVABLES_CREATE_LOAN_REQ))) {
				// user does not have appropriate authority.
				mediatorServices.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.REC_INV_LRQ_NO_ACCESS);
				return new DocumentHandler();
			}
			setBaseCurrency(currencyCode);
			setUserOid(userOid);
			setClientBankOid(clientBankOid);
			setCorporateOrgOid(corpOrgOid);
			setMediatorServices(mediatorServices);
			setTimeZone(timeZone);	
			 CorporateOrganization corporateOrg = (CorporateOrganization) mediatorServices
		        .createServerEJB("CorporateOrganization");
			corporateOrg.getData(Integer.parseInt(corpOrgOid));
			setUsePaymentDate(corporateOrg.getAttribute("use_payment_date_allowed_ind"));
			setSecurityRights(securityRights);
			ClientServerDataBridge csdb = mediatorServices.getCSDB();
	        userLocale = csdb.getLocaleName();
			setUserLocale(userLocale);
			count = invoiceList.size();	
			LOG.debug("InvoiceGroupMediatorBean: number of invoices selected : " + count);
			
			if (count <= 0) 
			{
				mediatorServices.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.NO_ITEM_SELECTED,
						mediatorServices.getResourceManager().getText("UploadInvoices.CreateLoanReqByRules", TradePortalConstants.TEXT_BUNDLE));
				return new DocumentHandler();
			}
			groupIds = new String[count];
			// Loop through the list of invoice oids, and processing each one in the invoice list
		    for (int i = 0; i < count; i++) 
		    {
			  invoiceItem = (DocumentHandler) invoiceList.get(i);
			  LOG.debug(" value is :"+invoiceItem.getAttribute("/InvoiceData"));
			  if(StringFunction.isBlank(invoiceItem.getAttribute("/InvoiceData"))){
				mediatorServices.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.NO_ITEM_SELECTED,
						mediatorServices.getResourceManager().getText("UploadInvoices.CreateLoanReqByRules", TradePortalConstants.TEXT_BUNDLE));
				return new DocumentHandler();
			  }
			  String[] invoiceData = invoiceItem.getAttribute("/InvoiceData").split("/");
			  invoiceOid = invoiceData[0];
			  invoiceOidList.add(invoiceOid);	
			  groupIds[i]=invoiceData[0];	
			  tempOids.add(invoiceData[0]);
		    }
		    LOG.info("invoiceOidList->"+tempOids);
		    
		    //LoanType check for the selected invoices
		     InvoiceGroup invoiceGroup = null;
			 if(tempOids.size()>0){
				 for(int i=0;i<tempOids.size();i++){
				 invoiceGroup = (InvoiceGroup) mediatorServices.createServerEJB("InvoiceGroup",
							Long.parseLong((String)tempOids.get(i)));
				String tpName = invoiceGroup.getDescription();
				 loanType = invoiceGroup.getAttribute("loan_type");
				LOG.debug("tpName:"+tpName+"\t loanType:"+loanType);
				if(StringFunction.isBlank(loanType)){
					 mediatorServices.getErrorManager().issueError(
								TradePortalConstants.ERR_CAT_1,
								TradePortalConstants.INST_LOAN_TYPE_MISSED_GROUP);							
						return new DocumentHandler();
				 }
				}
			 }
	        //Get the invoices for grouping
		    Vector invoicesGroupDoc =  getInvoices(groupIds,corpOrgOid);
		    Vector tempGroupDoc = invoicesGroupDoc;
		    LOG.info("invoicesGroupDoc->"+tempGroupDoc +"\t:"+invoicesGroupDoc);
		    
		    if(tempGroupDoc!=null && tempGroupDoc.size()>0){
				 for(int pos = 0; pos<tempGroupDoc.size();pos++){
				 uploadInvoiceDoc = (DocumentHandler) tempGroupDoc.get(pos);	
			 
			String linkedToInstType = uploadInvoiceDoc.getAttribute("/LINKED_TO_INSTRUMENT_TYPE");			
			//if Instrument Type is blank or not LRQ type then do not create an instrument
			 if(StringFunction.isBlank(linkedToInstType) || !InstrumentType.LOAN_RQST.equals(linkedToInstType)){
			      
		    	  mediatorServices.getErrorManager().issueError(
		    			  UploadInvoiceMediator.class.getName(),
		    			  TradePortalConstants.REC_INVOICE_LOAN_REQ_RULES_NOT_ALLOW);
		    	 
		    	  tempGroupDoc.removeElementAt(pos);
					pos--;
			   }				
	    	 }
		  }	
		   if(invoicesGroupDoc!=null && invoicesGroupDoc.size()>0){
			   
			 for(int pos = 0; pos<invoicesGroupDoc.size();pos++){
					 uploadInvoiceDoc = (DocumentHandler) invoicesGroupDoc.get(pos);	
				 
				String instrOid = uploadInvoiceDoc.getAttribute("/A_INSTRUMENT_OID");
				String tranOid = uploadInvoiceDoc.getAttribute("/A_TRANSACTION_OID");
				//if already associated then do not create an instrument
				if(StringFunction.isNotBlank(tranOid) && StringFunction.isNotBlank(instrOid)){
					Instrument	instr = (Instrument) mediatorServices.createServerEJB("Instrument",Long.parseLong(instrOid));
					mediatorServices.getErrorManager().issueError(PayableInvoiceGroupMediator.class.getName(),
							TradePortalConstants.INSTRUMENT_EXISTS,
							mediatorServices.getResourceManager().getText("UploadInvoices.LRQMessage", TradePortalConstants.TEXT_BUNDLE) +" - " +instr.getAttribute("complete_instrument_id"),uploadInvoiceDoc.getAttribute("/INVOICE_ID"));
					invoicesGroupDoc.removeElementAt(pos);
					pos--;
				}
			 }
		}
		    if(invoicesGroupDoc==null || invoicesGroupDoc.size()==0)
				return new DocumentHandler();  
		    if (invoicesGroupDoc != null) {
				 for (int j=0; j<invoicesGroupDoc.size(); j++) {
					 DocumentHandler row = (DocumentHandler) invoicesGroupDoc.elementAt(j);
					 String invOid = row.getAttribute("/UPLOAD_INVOICE_OID");
					 invoiceOidsVector.addElement(invOid);
				 }
		 }
		    
		    	
		         
		   try{

			   String creationDateTime = DateTimeUtility.getGMTDateTime(true, false);
			     groupInvoices(inputDoc,invoiceOidsVector);				
				
				 LOG.debug("size:"+invOIds.size()+"\t:"+invOIds.toString());
				
				 if (invOIds.size() > 0){				
				
				   String uploadInvOid = null;
				   Vector invoiceIDVector = null;
					invoiceIDVector = new Vector(invOIds);					
					LOG.debug("invoiceIDVector:"+invoiceIDVector);
					InvoicesSummaryData invoiceSummaryData = null;					
					for (int i = 0; i < invoiceIDVector.size(); i++) {
						
					uploadInvOid =  (String) invoiceIDVector.get(i);					
	 				invoiceSummaryData =
							(InvoicesSummaryData) mediatorServices.createServerEJB("InvoicesSummaryData",
									Long.parseLong(uploadInvOid));	
	 				 String tpName= invoiceSummaryData.getTpRuleName();
					invoiceSummaryData.setAttribute("tp_rule_name", tpName);
	 				int saveYN = invoiceSummaryData.save();
	 				LOG.debug("SaveYN :"+saveYN);		
	 				
				}	
				
				 String sql = "select distinct ins.COMPLETE_INSTRUMENT_ID INS_NUM, " +
			                "ins.instrument_type_code INS_TYP, tran.transaction_type_code TRAN_TYP " +
			                "from invoices_summary_data isd inner join instrument ins on isd.A_INSTRUMENT_OID = ins.INSTRUMENT_OID  " +
			                "inner join transaction tran on ins.original_transaction_oid = tran.transaction_oid " +
			                "and ";
			        List<Object> sqlprmLst = new ArrayList<Object>();
			        sql+= POLineItemUtility.getInvoiceItemOidsSqlWithPlaceHolder(new Vector(invOIds), "isd.upload_invoice_oid",sqlprmLst);
			        DocumentHandler resultSetDoc = DatabaseQueryBean.getXmlResultSet(sql, false, sqlprmLst);		  
			        outputDoc.addComponent("/Out",resultSetDoc);
			        updateFileUpload(inputDoc,outputDoc, creationDateTime);

			        Vector newGroupOids = POLineItemUtility.getInvoiceGroupOidsForInvoiceOids(new Vector(invOIds));
					LOG.debug("new group oid:"+newGroupOids);
			        updateAttachmentDetails(tempOids,newGroupOids, mediatorServices);
				success =true;
			  }else if(loanReqStatus && invOIds.size() == 0){
						 mediatorServices.getErrorManager().issueError(
			    			  UploadInvoiceMediator.class.getName(),
			    			  TradePortalConstants.AUTO_LOAN_REQ_BY_RULES_NO_ITEMS_FOR_LRC_RULE);
			 }
	
			} catch (AmsException e) {
			
				e.printStackTrace();
			} 
		   invOIds = new TreeSet<String>(); 
			   if(success)
				   mediatorServices.getErrorManager().issueError(
							TradePortalConstants.ERR_CAT_1, 
							TradePortalConstants.AUTO_LOAN_REQ_RULE_CREATION_SUCCESS); 
		  
		   return outputDoc;
		   
	      } 
		
		 private void updateFileUpload(DocumentHandler inputDoc,
			DocumentHandler outputDoc, String creationDateTime) throws RemoteException, AmsException {
		boolean success = true;

		InvoiceFileUpload invoiceUpload = (InvoiceFileUpload) mediatorServices
				.createServerEJB("InvoiceFileUpload");
		invoiceUpload.newObject();
		invoiceUpload.setAttribute("invoice_file_name", "Automated");
		invoiceUpload.setAttribute("creation_timestamp", creationDateTime);


		invoiceUpload.setAttribute("owner_org_oid",
				inputDoc.getAttribute("/InvoiceGroupList/ownerOrg"));
		invoiceUpload.setAttribute("user_oid",
				inputDoc.getAttribute("/InvoiceGroupList/userOid"));

		DocumentHandler resultsParamDoc = new DocumentHandler();
		DocumentHandler errorDoc = outputDoc.getFragment("/Error");
		if (errorDoc != null) {
			String maxErrorSeverity = errorDoc.getAttribute("maxerrorseverity");
			if ("5".equals(maxErrorSeverity)) {
				List<DocumentHandler> errVector = errorDoc.getFragmentsList("/errorlist/error");
				for (DocumentHandler errDoc : errVector) {
					String errMessage = errDoc.getAttribute("message");
					InvoiceUtility.insertToInvErrorLog("groupingErr- ",
							invoiceUpload
									.getAttribute("invoice_file_upload_oid"),
							errMessage, null);
					InvoiceUtility.insertToInvErrorLog("groupingErr- ",
							invoiceUpload
									.getAttribute("invoice_file_upload_oid"),
							"Error returned: " + errorDoc.toString(), "N");
				}
				success = false;
			}
		}

		if (success) {
			resultsParamDoc.addComponent("/", outputDoc.getFragment("/Out"));
			List<DocumentHandler> v = resultsParamDoc.getFragmentsList("/ResultSetRecord");
			String errorCode = "";
			if (errorDoc != null) {
				String maxSeverity = errorDoc.getAttribute("/maxerrorseverity");
				if ("1".equals(maxSeverity)) {
					List<DocumentHandler> errVector = errorDoc
							.getFragmentsList("/errorlist/error");
					for (DocumentHandler errDoc : errVector) {
						if ("I182".equals(errDoc.getAttribute("/code"))) {
							errorCode = errDoc.getAttribute("/code");
						} else if ("I477".equals(errDoc.getAttribute("/code"))) {
							errorCode = errDoc.getAttribute("/code");
						}
					}
				}
			}
			if (v.size() > 0) {
				String resultParams = resultsParamDoc.toString();
				invoiceUpload.setAttribute("inv_result_parameters",
						resultParams);
			} else if ("".equals(errorCode) || errorCode == null) { 
				IssuedError ie = ErrorManager.findErrorCode(
						TradePortalConstants.ERROR_GROUPING_INVS, userLocale);
				String errMsg = ie.getErrorText();
				InvoiceUtility.insertToInvErrorLog("groupingErr- ",
						invoiceUpload.getAttribute("invoice_file_upload_oid"),
						errMsg, null);
				InvoiceUtility.insertToInvErrorLog("groupingErr- ",
						invoiceUpload.getAttribute("invoice_file_upload_oid"),
						"No Output returned: " + errorDoc!=null?errorDoc.toString():"", "N");
			} else {
				IssuedError ie = ErrorManager.findErrorCode(errorCode,
						userLocale);
				if (ie != null) {
					String errMsg = ie.getErrorText();
					InvoiceUtility.insertToInvErrorLog("groupingErr- ",
							invoiceUpload
									.getAttribute("invoice_file_upload_oid"),
							errMsg, null);
					InvoiceUtility.insertToInvErrorLog("groupingErr- ",
							invoiceUpload
									.getAttribute("invoice_file_upload_oid"),
							"No Output returned: " + errorDoc!=null?errorDoc.toString():"", "N");
				}
			}
			invoiceUpload.setAttribute("validation_status",
					TradePortalConstants.PYMT_UPLOAD_VALIDATION_SUCCESSFUL);

		}else{
			invoiceUpload.setAttribute("validation_status",
					TradePortalConstants.PYMT_UPLOAD_VALIDATION_FAILED);

		}
		invoiceUpload.setAttribute("completion_timestamp", DateTimeUtility.getGMTDateTime(true, false));
		
	    //save the active_po_upload record
		invoiceUpload.save(false); //false means don't validate just save

	}
	

		/**
		  * This method groups the invoices based on loan request rules.
		  * @param inputDoc
		  * @param invoiceOidList
		  * @return
		  * @throws AmsException
		  * @throws RemoteException
		  */
		 private DocumentHandler groupInvoices(DocumentHandler inputDoc, Vector invoiceOidList) throws AmsException, RemoteException {
			 LOG.debug("groupInvoices()....");	
			 
			 Vector loanReqRulesList = null;
			 DocumentHandler loanRequestDoc = null;
			 Vector invoiceItemList = null;
			 int count =0;		
			 StringBuilder invoiceOidBuf = new StringBuilder();
			 Iterator it = invoiceOidList.iterator();
			 //boolean errorFlag = false;
			 while(it.hasNext()){
				 invoiceOidBuf.append(it.next()).append(",");
			 }
			 invoiceOidBuf.delete(invoiceOidBuf.toString().length()-1,invoiceOidBuf.toString().length());		
			 SELECT_SQL = "select name,inv_only_create_rule_desc,inv_only_create_rule_template,inv_only_create_rule_max_amt,due_or_pay_max_days,due_or_pay_min_days,a_op_bank_org_oid, " +
		 		"pregenerated_sql " +
		 		EXTRA_SELECT_SQL +
		 		"  from inv_only_create_rule where a_invoice_def in (select a_upload_definition_oid from invoices_summary_data  " +
		 		" where upload_invoice_oid in ("+invoiceOidBuf.toString()+")) and a_owner_org_oid in ( ";

			 LOG.debug(" SELECT SQL After OIDS:"+SELECT_SQL);
			 
			 loanReqRulesList = getLoanRequestRules(getCorporateOrgOid());
		     
		     //Get the distinct loan types for the list of invoicesOids and apply rules on those list
		     String loanTypeSQL = " select distinct loan_type loantype from invoices_summary_data where upload_invoice_oid in (";
		     Vector loanList = InvoiceUtility.getDistinctLoanTypes(loanTypeSQL,invoiceOidBuf.toString(),getCorporateOrgOid());
		     String loanType = "";
		     
		     if(loanList!=null && loanList.size()>0){	
		    	
			     for(int cnt =0;cnt<loanList.size();cnt++){
			    	 loanType = (String)loanList.get(cnt);
			     
			     for (int i = 0; i < loanReqRulesList.size(); i++) {
				      // Get the Loan Request rule and pull some values off it.
			    	 loanRequestDoc = (DocumentHandler) loanReqRulesList.elementAt(i);      
					 LOG.debug("loanRequestDoc:"+loanRequestDoc);
				      // Get the getInvoiceItems for this loan request rules
			    	 invoiceItemList = getInvoiceItems(invoiceOidBuf.toString(),loanRequestDoc,loanType);		  

					  LOG.info("count->:"+count);
				      if (invoiceItemList.size() < 1) {
				    	  LOG.debug("No rules InvoiceItems available...");
				      } else {
				        // Now start chunking to po line items.
				    	  chunkInvoices(invoiceItemList, loanRequestDoc);
				        count++; 
				      }
				    }
			      }
		     }

		     if(count!=0)
		     logUnassignedInvoiceCount(invoiceOidBuf.toString());
			 LOG.info("count:"+count);
				return loanRequestDoc;
			
			 
		 }
	private void logUnassignedInvoiceCount(String invoiceOids)
			throws AmsException, RemoteException {
		int count = 0;

		// If we're processing for a specific upload sequence, print a message
		// for how many items are unassigned from that sequence.
		if (StringFunction.isNotBlank(invoiceOids)) {
			String sql = " a_transaction_oid is NULL and a_corp_org_oid = ? ";
			List<Object> sqlprmLst = new ArrayList<Object>();
			sqlprmLst.add(getCorporateOrgOid());
	        String placeHolderStr = SQLParamFilter.preparePlaceHolderStrForInClause(invoiceOids, sqlprmLst);
			sql += " and upload_invoice_oid in (" +placeHolderStr+ ")";
			
			count = DatabaseQueryBean.getCount("upload_invoice_oid", "INVOICES_SUMMARY_DATA", sql, false, sqlprmLst);
			if (count > 0) {
				mediatorServices.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.NO_MATCH_RULE_INV,
						String.valueOf(count));
			}

		}

	}
		 /**
		  * This method processes the invoices which are already
		  * passed through the rules and groups invoices for creating 
		  * loan request instrument
		  * @param invoiceItemList
		  * @param creationRuleDoc
		  * @throws RemoteException
		  * @throws AmsException
		  */
		 private void chunkInvoices(Vector invoiceItemList, DocumentHandler creationRuleDoc)
			      throws RemoteException, AmsException {
			 
			    LOG.debug("inside chunkInvoices()....invoiceItemList "+invoiceItemList);
			    BigDecimal invoiceAmount;
			    BigDecimal maxAmount = null;
			    BigDecimal convertedMaxAmount = null;
			    BigDecimal totalAmount = BigDecimal.ZERO;

			    int itemCount = 0;
			    String lastBenName = "";
			    String lastCurrency = "";
			    String latestDuepaymentDt = null;
			    String lastUploadInvoiceOid = "";
			    String benName = "";
			    String currency = "";
			  
			    String duePaymentDt;
			    String loanType ="";
			    String uploadInvoiceOid = "";
			    String loanReqRuleName = creationRuleDoc.getAttribute("/NAME");
				LOG.debug("loanReqRuleName:"+loanReqRuleName);
			    Vector invoiceOids = null;
			    DocumentHandler invoiceItemDoc = null;		  
			    int invoiceListCount = 0;

			    CorporateOrganization corporateOrg = (CorporateOrganization) mediatorServices
			        .createServerEJB("CorporateOrganization");
			    try {
			    	corporateOrg.getData(Integer.parseInt(getCorporateOrgOid()));
			    } catch (InvalidObjectIdentifierException e) {
			    	  mediatorServices.getErrorManager().issueError(
			          TradePortalConstants.ERR_CAT_1, TradePortalConstants.ORG_NOT_EXIST);
			    }
			    long clientBankOid = corporateOrg.getAttributeLong("client_bank_oid");
			    ClientBank clientBank = (ClientBank) mediatorServices.createServerEJB("ClientBank");
			    try {
			      clientBank.getData(clientBankOid);
			    } catch (InvalidObjectIdentifierException e) {
			      mediatorServices.getErrorManager().issueError(
			          TradePortalConstants.ERR_CAT_1, TradePortalConstants.INVALID_CLIENT_BANK_ORG);
			    }
			
			    // Determine the number of Invoices allowed per instrument
			    //int maximumInvoicesInInstr = MAX_ITEMS_ON_INVOICE;		    
			    String value = creationRuleDoc.getAttribute("/INV_ONLY_CREATE_RULE_MAX_AMT");
				
			    if (StringFunction.isNotBlank(value)) {
			      maxAmount = new BigDecimal(value);
			    } else {
			      maxAmount = null;

			    }
				Vector invOids = new Vector();
				
			    for (int x = 0; x < invoiceItemList.size(); x++) {
					invoiceItemDoc = (DocumentHandler) invoiceItemList.elementAt(x);
					invOids.add(invoiceItemDoc.getAttribute("/UPLOAD_INVOICE_OID"));
						 	    		
			    }
				LOG.debug("invOids:"+invOids);
			    //This is to check if amount to validated against rules if invoice rules found>1
			    String amtSQL = "select count(distinct (a_upload_definition_oid)) a_upload_definition_oid from " +
			    		"invoices_summarY_data where ";
		        List<Object> sqlprmLst = new ArrayList<Object>();
		        amtSQL+= POLineItemUtility.getInvoiceItemOidsSqlWithPlaceHolder(invOids, "UPLOAD_INVOICE_OID",sqlprmLst);
		        DocumentHandler amoutDoc = DatabaseQueryBean.getXmlResultSet(amtSQL, false, sqlprmLst);	
		        List<DocumentHandler> resultsVector = amoutDoc.getFragmentsList("/ResultSetRecord/");
		        if( resultsVector.get(0) != null )
		        {
		          DocumentHandler loanDoc = resultsVector.get(0);
		       
		          String amountCnt = loanDoc.getAttribute("/A_UPLOAD_DEFINITION_OID");
		          if(Integer.parseInt(amountCnt)>1){
		        	   maxAmount = null;
		          }
		        } 
			    
			    for (int x = 0; x < invoiceItemList.size(); x++) {
			      // We will use this variable to count the number of Invoice line items fully processed, i.e. not skipped.
			    	invoiceListCount++;

			      invoiceItemDoc = (DocumentHandler) invoiceItemList.elementAt(x);
				  LOG.debug("invoiceItemDoc:"+invoiceItemDoc);
			      // Pull off values from the document that drive the processing.
				  invoiceAmount = invoiceItemDoc.getAttributeDecimal("/AMOUNT");
			      benName = invoiceItemDoc.getAttribute("/BUYER_NAME");
			      currency = invoiceItemDoc.getAttribute("/CURRENCY");
			      uploadInvoiceOid = invoiceItemDoc.getAttribute("/UPLOAD_INVOICE_OID");		     
			      duePaymentDt = invoiceItemDoc.getAttribute("/INVDATE"); 
			      loanType = invoiceItemDoc.getAttribute("/LOAN_TYPE"); 
				  LOG.debug("invoiceAmount:"+invoiceAmount+"\t benName:"+benName+"\t currency:"+currency+"\t uploadINv oid:"+uploadInvoiceOid+"\t duePaymentDt:"+duePaymentDt);
			      invoiceOids = new Vector();		        

			        lastBenName = benName;
			        lastCurrency = currency;		      
			        latestDuepaymentDt = duePaymentDt;
			        lastUploadInvoiceOid = uploadInvoiceOid;
			        // Add the oid to the Invoice oid vector.
			        invoiceOids.add(uploadInvoiceOid);
					LOG.debug("first adding into invoiceOids:"+invoiceOids+"\t lastUploadInvoiceOid:"+lastUploadInvoiceOid);
			        convertedMaxAmount = getConvertedMaxAmount(maxAmount,getBaseCurrency(), currency, creationRuleDoc);
					LOG.debug("convertedMaxAmount:"+convertedMaxAmount);
			        itemCount = 1;
			        totalAmount = invoiceAmount;
			        
			        if ((convertedMaxAmount == null ||
			        		totalAmount.compareTo(convertedMaxAmount) <= 0)) {
			            // Find Invoices that can be added to this group
			            int subIdx = x + 1;

			            while (subIdx < invoiceItemList.size()) {
			            	invoiceItemDoc = (DocumentHandler) invoiceItemList.elementAt(subIdx);

			                // Pull off values from the document that drive the processing.
			                invoiceAmount = invoiceItemDoc.getAttributeDecimal("/AMOUNT");
			                benName = invoiceItemDoc.getAttribute("/BUYER_NAME");
			                currency = invoiceItemDoc.getAttribute("/CURRENCY");
			                uploadInvoiceOid = invoiceItemDoc.getAttribute("/UPLOAD_INVOICE_OID");		             
			                duePaymentDt = invoiceItemDoc.getAttribute("/INVDATE");
			                loanType = invoiceItemDoc.getAttribute("/LOAN_TYPE");
							LOG.debug(" loanType:"+loanType+"\t -> poamount:"+invoiceAmount+"\t benName:"+benName+"\t currency:"+currency+"\t uploadInvoiceOid:"+uploadInvoiceOid+"\t duePaymentDt :"+duePaymentDt);
			               
						    if(TradePortalConstants.TRADE_LOAN.equals(loanType)){
			            		if (!currency.equals(lastCurrency)){
			            			break;
			            		}
							 } else {
				                if (!benName.equals(lastBenName) ||
				                		!currency.equals(lastCurrency) ||
				                		!duePaymentDt.equals(latestDuepaymentDt))		                	
				                {
				                	// None of the remaining Invoices can be added to this group
				                    break;
				                }
			            	} 
						    LOG.debug("invoiceAmount:"+invoiceAmount);
			                BigDecimal newTotal = totalAmount.add(invoiceAmount);
			                if (convertedMaxAmount == null ||
			                		newTotal.compareTo(convertedMaxAmount) <= 0) {
			                	// Add this Invoice to the group
			                	invoiceOids.add(uploadInvoiceOid);		                 
								LOG.debug("adding into poOids:"+invoiceOids+"\t uploadInvoiceOid:"+uploadInvoiceOid);
								invoiceItemList.remove(subIdx);
								invoiceListCount++;
			                    itemCount++;
			                    totalAmount = newTotal;
							  
			                	if (duePaymentDt != null) {
			                		if (latestDuepaymentDt == null) {
			                			latestDuepaymentDt = duePaymentDt;
			                		}
			                		else if (duePaymentDt.compareTo(latestDuepaymentDt) > 0) {
			                			latestDuepaymentDt = duePaymentDt;
			                		}
			                	}
			                }
			                else {
			                	// This Invoice would bring the total above the max
			                	subIdx++;
			                }
			            }
			        }
			        if(loanReqStatus){
			        // Build the input document used to create a new instrument
			        DocumentHandler instrCreateDoc = buildInstrumentCreateDoc(creationRuleDoc,
			        		lastBenName, lastCurrency, totalAmount, latestDuepaymentDt, invoiceOids);
			        Map impFinBuyerBackMap = InvoiceUtility.getFinanceType(loanType, TradePortalConstants.INVOICE_TYPE_REC_MGMT );
				    instrCreateDoc.setAttribute("/importIndicator", (String)impFinBuyerBackMap.get("importIndicator"));
				    instrCreateDoc.setAttribute("/financeType", (String)impFinBuyerBackMap.get("financeType"));
				    instrCreateDoc.setAttribute("/buyerBacked", (String)impFinBuyerBackMap.get("buyerBacked"));
				    //IR 22917 - no bene name for EXP_FIN and SEL_REQ
				    if(TradePortalConstants.EXPORT_FIN.equals(loanType)
							|| TradePortalConstants.SELLER_REQUESTED_FINANCING.equals((String)impFinBuyerBackMap.get("financeType")))
				    	instrCreateDoc.setAttribute("/benName", "");
				    	
			        // Create instrument using the document, assign the invoice items, and create log records.
			        try {
			        	createLRQInstrumentFromInvoiceData(instrCreateDoc, invoiceOids, loanReqRuleName);
			        	invOIds.addAll(invoiceOids);
		
			        }
			        catch (AmsException e) {
			        	// This is probably a sql error but could be anything.
			        	// Post this exception to the log.
			        	LOG.info(getCorporateOrgOid()+"\t"+ e.getMessage());
			        	e.printStackTrace();
			        }
			       }
			    } 
		 }
		 
		 /**
		  * This method prepares the doc object required for creating an Instrument 
		  * with appropriate attributes for Loan Req Instrument.
		  * @param creationRuleDoc
		  * @param benName
		  * @param currency
		  * @param amount
		  * @param latestShipDt
		  * @param invoiceOids
		  * @return
		  */
		 private DocumentHandler buildInstrumentCreateDoc(DocumentHandler creationRuleDoc,
			      String benName, String currency, BigDecimal amount,
			      String latestShipDt, Vector invoiceOids) {
			 
			 LOG.debug("buildInstrumentCreateDoc () creationRuleDoc->"+creationRuleDoc);
			 LOG.debug("benName:"+benName+"\t currency:"+currency+"\t amount:"+amount+"\t latestShipDt:"+latestShipDt+"\t invoiceOids:"+invoiceOids);
			   DocumentHandler instrCreateDoc = new DocumentHandler();
				String secretKeyString = getMediatorServices().getCSDB().getCSDBValue("SecretKey");
				byte[] keyBytes = com.amsinc.ecsg.util.EncryptDecrypt.base64StringToBytes(secretKeyString);
				javax.crypto.SecretKey secretKey = new javax.crypto.spec.SecretKeySpec(keyBytes, "DESede/CBC/PKCS5Padding");
				String templateOid = creationRuleDoc.getAttribute("/INV_ONLY_CREATE_RULE_TEMPLATE");
				String encryptedString = null;
				if (StringFunction.isNotBlank(templateOid)){
					encryptedString = EncryptDecrypt.encryptStringUsingTripleDes(templateOid, secretKey);
				}
			    instrCreateDoc.setAttribute("/instrumentOid", encryptedString);
			    instrCreateDoc.setAttribute("/clientBankOid", getClientBankOid());
			    instrCreateDoc.setAttribute("/instrumentType",InstrumentType.LOAN_RQST);
			    instrCreateDoc.setAttribute("/bankBranch", creationRuleDoc.getAttribute("/A_OP_BANK_ORG_OID"));
			    if(StringFunction.isBlank(templateOid)){
			    instrCreateDoc.setAttribute("/copyType", TradePortalConstants.FROM_BLANK);
				}else {
					instrCreateDoc.setAttribute("/copyType", TradePortalConstants.FROM_TEMPL);
				}
			    instrCreateDoc.setAttribute("/userOid", getUserOid());
			    instrCreateDoc.setAttribute("/ownerOrg", getCorporateOrgOid());
			    instrCreateDoc.setAttribute("/securityRights", getSecurityRights());
			    instrCreateDoc.setAttribute("/benName", benName);
			    instrCreateDoc.setAttribute("/currency", currency);
			    instrCreateDoc.setAttribute("/shipDate", latestShipDt);
			    instrCreateDoc.setAttributeDecimal("/amount", amount);
			    instrCreateDoc.setAttribute("/uploadIndicator", TradePortalConstants.INDICATOR_YES);
			    if (StringFunction.isBlank(userLocale)) {
			      userLocale = "en_US";
			    }
			    instrCreateDoc.setAttribute("/locale", userLocale);

			    for (int i = 0; i < invoiceOids.size(); i++) {
			      instrCreateDoc.setAttribute("/InvoiceLines(" + i + ")/uploadInvoiceOid",
			          (String) invoiceOids.elementAt(i));
			    }

			    getMediatorServices().debug("Creation rules document is" + instrCreateDoc.toString());
			    return instrCreateDoc;
			  }
		 
		 /**
		  * This method invokes the Instrument EJB and writes the invoice 
		  * details in INVOICE_HISTORY table.
		  * @param inputDoc
		  * @param invoiceOids
		  * @param lcRuleName
		  * @return
		  * @throws AmsException
		  * @throws RemoteException
		  */
		 private DocumentHandler createLRQInstrumentFromInvoiceData(DocumentHandler inputDoc,
			      Vector invoiceOids, String lcRuleName)
			      throws AmsException, RemoteException {
			 
			    LOG.debug("Entered createLRQInstrumentFromInvoiceData(). inputDoc follows ->\n"  + inputDoc.toString());
			    MediatorServices mediatorServices = getMediatorServices();		    
			    DocumentHandler outputDoc = null;
			    //Create a new instrument
			    Instrument newInstr = (Instrument) mediatorServices.createServerEJB("Instrument");
			    outputDoc = newInstr.createLoanReqForInvoiceData(inputDoc, invoiceOids, lcRuleName);
			    LOG.debug("Returned from instrument.createInstrumentFromPOData()\n"+ "Returned document follows -> " + outputDoc.toString());
			    try{
			    // Now that the transaction has been created, assign this transaction
			    // oid to all the line items on the transaction.
				LOG.debug("invoiceOids:"+invoiceOids+"\t outputDoc:"+outputDoc);
				Transaction transaction = (Transaction) mediatorServices.createServerEJB("Transaction");
				String transOid = outputDoc.getAttribute("/transactionOid");
				if (transOid != null){
				long transactionOid = outputDoc.getAttributeLong("/transactionOid");
				transaction.getData(transactionOid);
				Terms terms = (Terms) transaction.getComponentHandle("CustomerEnteredTerms");
				String termsOid = terms.getAttribute("terms_oid");
				setInstrumentOid(newInstr.getAttribute("instrument_oid"));
				setTransactionOid(outputDoc.getAttribute("/transactionOid"));
				setTermsOid(termsOid);
				        POLineItemUtility.updateInvoiceTransAssignment(invoiceOids, outputDoc.getAttribute("/transactionOid"), newInstr.getAttribute("instrument_oid"), 
				             TradePortalConstants.RECEIVABLE_UPLOAD_INV_STATUS_ASSIGNED,termsOid);
				        
				        POLineItemUtility.createInvoiceHistoryLog (invoiceOids,TradePortalConstants.INVOICE_ACTION_AUTOCREATE,
				        		TradePortalConstants.RECEIVABLE_UPLOAD_INV_STATUS_ASSIGNED,getUserOid());
						if (TradePortalConstants.INDICATOR_NO.equals(newInstr.getAttribute("import_indicator")) || 
								TradePortalConstants.INDICATOR_X.equals(newInstr.getAttribute("import_indicator"))){
					        String requiredOid = "upload_invoice_oid";
							 String invoiceOidsSql = POLineItemUtility.getPOLineItemOidsSql(
									 invoiceOids, requiredOid);
					        
		 
							 boolean useSeller = TradePortalConstants.BUYER_REQUESTED_FINANCING.equals(terms.getAttribute("finance_type")) ;
							String invoiceDueDate = null;
							if (InvoiceUtility.validateSameCurrency(invoiceOidsSql) && 
									InvoiceUtility.validateSameDate(invoiceOidsSql) && 
									InvoiceUtility.validateSameTradingPartner(invoiceOidsSql, useSeller)){
								StringBuilder dueDateSQL = new StringBuilder("select distinct(case when payment_date is null then due_date else payment_date end ) DUEDATE " +
										" from invoices_summary_data where ");
								dueDateSQL.append(invoiceOidsSql);
								DocumentHandler resultDoc = DatabaseQueryBean.getXmlResultSet(dueDateSQL.toString(), false, new ArrayList<Object>());
								invoiceDueDate = resultDoc.getAttribute("/ResultSetRecord(0)/DUEDATE");
							}
							

							if (StringFunction.isNotBlank(invoiceDueDate)){

								try {
									SimpleDateFormat iso = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
									SimpleDateFormat jPylon1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
									
										Date date = jPylon1.parse(invoiceDueDate);

										terms.setAttribute("loan_terms_fixed_maturity_dt",TPDateTimeUtility.convertISODateToJPylonDate(iso.format(date)));
										//IR 22915 - save the changes
										transaction.save(false);
								} catch (Exception e) {
									e.printStackTrace();
								}

								
							}
						}
				        
				}else{
			         mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
			                 TradePortalConstants.ERROR_CREATING_ISTRUMENT);
					
				}
			        
			    } catch (AmsException e){
			        String [] substitutionValues = {"",""};
			        substitutionValues[0] = e.getMessage();
			        substitutionValues[1] = "0";
			         mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
			                 AmsConstants.SQL_ERROR, substitutionValues);
			         e.printStackTrace();
			    }
			   

			    return outputDoc;
			  }
		 
		 /**
		  * This method returns the set of invoices which pass through the
		  * rules defined for those invoice/invoices
		  * @param invoiceOids
		  * @param creationRuleDoc
		  * @return
		  * @throws AmsException
		  * @throws RemoteException
		  */
		 private Vector getInvoiceItems(String invoiceOids,DocumentHandler creationRuleDoc,String loanType) throws AmsException,
	     RemoteException {
			 	LOG.debug("getInvoiceItems()....invoiceOids ->"+invoiceOids);
			 	String date = ""; 	
			  
			 	if(TradePortalConstants.INDICATOR_YES.equals(getUsePaymentDate()))
			 		date = " cast (case when payment_date is null then due_date else payment_date end as date ) invdate ";
			 	else
			 		date = " due_date invdate ";
			 	
			 	List<Object> sqlParmsLst = new ArrayList(); 
			 	String INVOICE_SELECT_SQL = "select upload_invoice_oid,invoice_id,buyer_name,loan_type," +
				"currency,amount,p_invoice_file_upload_oid,issue_date,"+date+" from invoices_view " +
				"where nvl(deleted_ind,'N')!='Y' and a_transaction_oid is NULL and upload_invoice_oid in (?) and a_corp_org_oid = ";
			 	sqlParmsLst.add(invoiceOids);
			 	sqlParmsLst.add(getCorporateOrgOid());
			 	
			   StringBuilder invoiceSql = new StringBuilder(INVOICE_SELECT_SQL);
			
			   if(StringFunction.isNotBlank(loanType)){
				   invoiceSql.append(" and loan_type = ? ");
				   sqlParmsLst.add(loanType);
			   }
			   
			   // Add condition for last_ship_dt if both min and max are set;
			   String minDays = creationRuleDoc.getAttribute("/DUE_OR_PAY_MIN_DAYS");
			   String maxDays = creationRuleDoc.getAttribute("/DUE_OR_PAY_MAX_DAYS");
			   LOG.debug("mindays:"+minDays+"\t max days:"+maxDays);
			   if (StringFunction.isNotBlank(minDays)  && StringFunction.isNotBlank(maxDays)) {	   
				  
			     // First get the current local date
			     String currentGmtDate = DateTimeUtility.getGMTDateTime();
			     Date currentLocalDate = TPDateTimeUtility.getLocalDateTime(currentGmtDate, getTimeZone());		  
			     SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yy");		     
			     String today = formatter.format(currentLocalDate);
			     LOG.debug("today:"+today);
			     if(TradePortalConstants.INDICATOR_YES.equals(getUsePaymentDate()))
			    	 invoiceSql.append(" and case when payment_date is null then due_date else payment_date end between TO_DATE(");
			     else
			    	 invoiceSql.append(" and due_date between TO_DATE(");		     
			     
			     invoiceSql.append(" ?)? and  TO_DATE(?')?");
			     
			     sqlParmsLst.add(today);
			     sqlParmsLst.add(minDays);
			     sqlParmsLst.add(today);
			     sqlParmsLst.add(maxDays);
			   }
			  
			   // Add condition for the remaining criteria from the lc creation rule
			   String criteria = creationRuleDoc.getAttribute("/PREGENERATED_SQL");
			
			   if (StringFunction.isNotBlank(criteria)) {
				   invoiceSql.append(" and ");
				   invoiceSql.append(criteria);
			   }
			  
			   // Finally add the orderby clause.
			   invoiceSql.append(" ");
			   invoiceSql.append(INVOICE_ORDERBY);
			

			   DocumentHandler resultSet = DatabaseQueryBean.getXmlResultSet(invoiceSql.toString(), false, sqlParmsLst);
			
			   if (resultSet == null)
				   resultSet = new DocumentHandler();
			
			   getMediatorServices().debug("invoice list is " + resultSet.toString());
			   return resultSet.getFragments("/ResultSetRecord");

	  }
		
		 /**
		  * This method returns the Loan Request rules
		  * @param corporateOrgOid
		  * @return
		  * @throws AmsException
		  */
		private Vector getLoanRequestRules(String corporateOrgOid) throws AmsException {		 
			
			 LOG.debug("getCreationRules() corporateOrgOid:"+corporateOrgOid);
			 	List<Object> sqlParmsLst = new ArrayList();
			    StringBuilder loanReqSQL = new StringBuilder(SELECT_SQL);
			    loanReqSQL.append("?");
			    sqlParmsLst.add(corporateOrgOid);

			    // Loop back through all parent orgs for oids to include in search for LoanRequest Creation rules
			    do {
			      String parentCorpSql = "select p_parent_corp_org_oid from corporate_org where organization_oid = ? AND activation_status=?";

			      DocumentHandler parentCorpOrg = DatabaseQueryBean.getXmlResultSet(parentCorpSql, false, corporateOrgOid, TradePortalConstants.ACTIVE);
			      if( parentCorpOrg != null )
			      {
			        List<DocumentHandler> resultsVector = parentCorpOrg.getFragmentsList("/ResultSetRecord/");
			        if( resultsVector.get(0) != null )
			        {
			          DocumentHandler parentOid = resultsVector.get(0);
			          String parentCorpOid = "";
			          parentCorpOid = parentOid.getAttribute("/P_PARENT_CORP_ORG_OID");
			          if (StringFunction.isNotBlank(parentCorpOid)) {
			        	  loanReqSQL.append(",?");
			            corporateOrgOid = parentCorpOid;
			            sqlParmsLst.add(corporateOrgOid);
			          } else {  // No more parents, so break out of loop
			            break;
			          }
			        } else {  // No more parents, so break out of loop
			          break;
			        }
			      } else { // no more parents, so break out of loop
			        break;
			      }
			    } while (true);		  

			    loanReqSQL.append(") ");
			    loanReqSQL.append(" and instrument_type_code = ? ");
			    loanReqSQL.append(ORDERBY_CLAUSE);
			    sqlParmsLst.add(InstrumentType.LOAN_RQST);

			    DocumentHandler resultSet = DatabaseQueryBean.getXmlResultSet(loanReqSQL.toString(), false, sqlParmsLst);

			    if (resultSet == null)
			      resultSet = new DocumentHandler();

			    LOG.debug("LoanRequest rule list doc is " + resultSet.toString());
			    
			    return resultSet.getFragments("/ResultSetRecord");

			  }
		
		 /**
		  * This method converts the given amount based on currency
		  * @param fromAmount
		  * @param fromCurrency
		  * @param toCurrency
		  * @param creationRuleDoc
		  * @return
		  */
		 private BigDecimal getConvertedMaxAmount(BigDecimal fromAmount,
			      String fromCurrency, String toCurrency, DocumentHandler creationRuleDoc) {

			    BigDecimal convertedAmount = null;
				String param = TradePortalConstants.FX_LOAN_REQUEST;
			    try {
			     
			      if ((fromAmount == null) ||
			    		  fromAmount.compareTo(BigDecimal.ZERO) == 0) {
			        return null;
			      }

			      // No need to convert if the from and to currencies are the same
			      if (fromCurrency.equals(toCurrency)) {
			        return fromAmount;
			      }

			      String templateCurrency = null;
			      String sql;
			      String multiplyIndicator = null;
			      BigDecimal rate = null;
			      DocumentHandler fxRateDoc = null;

			      ReferenceDataManager rdm = ReferenceDataManager.getRefDataMgr();

			      // Test whether the po line item currency code is a valid Trade
			      // Portal currency.
			      if (rdm.checkCode(TradePortalConstants.CURRENCY_CODE, toCurrency,
			          getUserLocale())) {
			        // Get the fx rate info for the po line item currency
			        fxRateDoc = getFxRateDoc(getCorporateOrgOid(), toCurrency);
			        if (fxRateDoc == null) {
			          // No fx rate found, isse warning and return a blank amount.
			          // This indicates that maxAmount is not used for grouping.
			          // Give error with the po line item currency.
						
			        	getMediatorServices().getErrorManager().issueError(getCorporateOrgOid(),
			              TradePortalConstants.LOANREQ_LOG_NO_FX_RATE, toCurrency,param);
			        		loanReqStatus = false;
			          return null;
			        } else {
			          // Found an fx rate for the template currency.  Use it.
			        }
			      } else {
			        // Currency code for the po line item is not valid.  Use
			        // the currency code from the instrument creation rule's template.
			        sql = "select copy_of_currency_code from template, transaction where p_instrument_oid = c_template_instr_oid and template_oid = ?";
			        DocumentHandler templateDoc = null;
			        try {
			          templateDoc = DatabaseQueryBean.getXmlResultSet(sql, true, new Object[]{creationRuleDoc.getAttribute("/LOAN_REQ_CREATION_TEMPLATE")});
			        } catch (AmsException e) {
			          templateDoc = null;
			        }
					 
			        if (templateDoc == null) {
			          // Can't get the currency code off the template.  Issue a warning and return
			          // a null amount.  This indicates NOT to use maxAmount for grouping.

			        	getMediatorServices().getErrorManager().issueError(getCorporateOrgOid(),
			              TradePortalConstants.LOANREQ_LOG_INVALID_CURRENCY,param,
			              creationRuleDoc.getAttribute("/NAME"));
			        	  loanReqStatus = false;
			          return null;
			        } else {
			          // We got the template currency but it may be blank.  We're only
			          // concerned with the first record (there should not be more).
			          templateCurrency = templateDoc
			              .getAttribute("/ResultSetRecord(0)/COPY_OF_CURRENCY_CODE");
			          if (StringFunction.isBlank(templateCurrency)) {
			            // Can't get the currency code off the template.  Issue a warning and return
			            // a null amount.  This indicates NOT to use maxAmount for grouping.

			        	  getMediatorServices().getErrorManager().issueError(getCorporateOrgOid(),
			                TradePortalConstants.LOANREQ_LOG_INVALID_CURRENCY,param,
			                creationRuleDoc.getAttribute("/NAME"));
			        	    loanReqStatus = false;
			            return null;
			          } else {
			            // Currency is good so attempt to get the fx rate doc.
			            fxRateDoc = getFxRateDoc(getCorporateOrgOid(),
			                templateCurrency);

			            // Don't look for an FX rate if the template's currency is the base currency
			            // No FX rate is needed in that case
			            if (!templateCurrency.equals(getBaseCurrency())) {
			              if (fxRateDoc == null) {
			                // No fx rate found, isse warning and return a blank amount.
			                // This indicates that maxAmount is not used for grouping.
			                // Give error using the template currency.

			            	  getMediatorServices().getErrorManager().issueError(getCorporateOrgOid(),
			                    TradePortalConstants.LOANREQ_LOG_NO_FX_RATE,param,
			                    templateCurrency);
			            	    loanReqStatus = false;
			                return null;
			              } else {
			                // Found an fx rate for the template currency.  Use it.
			              }
			            } else {
			              // Base Currency and Template currency are identical
			              // No need for conversion.  Return the from amount
			              return fromAmount;
			            }
			          } // end templateCurrency == null
			        } // end templateDoc == null
			      } // end checkCode

			      // We now have an fx rate doc (although the values on the doc may be
			      // blank.)  Attempt to convert the amount.
			      multiplyIndicator = fxRateDoc.getAttribute("/MULTIPLY_INDICATOR");
			      rate = fxRateDoc.getAttributeDecimal("/RATE");

			      if (multiplyIndicator != null && rate != null) {
			        // We're converting FROM the base currency TO the po line item
			        // currency so we need to do the opposite of the multiplyIndicator
			        // (the indicator is set for converting FROM some currency TO the
			        // base currency)
			        if (multiplyIndicator.equals(TradePortalConstants.MULTIPLY)) {
			          convertedAmount = fromAmount.divide(rate,
			              BigDecimal.ROUND_HALF_UP);
			        } else {
			          convertedAmount = fromAmount.multiply(rate);
			        }
			      } else {
			        // We have bad data in the fx rate record.  Issue warning and return
			        // null amount (indicating that maxAmount is not used for grouping)

			        getMediatorServices().getErrorManager().issueError(getCorporateOrgOid(),
			            TradePortalConstants.LOANREQ_LOG_NO_FX_RATE,
			            fxRateDoc.getAttribute("/CURRENCY_CODE"));
			            loanReqStatus = false;
			        return null;
			      }
			    } catch (AmsException e) {
			      LOG.debug("Exception found getting converted max amount:");
			      LOG.info(e.toString());
			      e.printStackTrace();
			    }
				LOG.debug("convertedAmount:"+convertedAmount);
			    return convertedAmount;
			  }

		
		/**
		 * This method returns fx rates for the given currency. 
		 * @param corporateOrg
		 * @param currency
		 * @return
		 */
		private DocumentHandler getFxRateDoc(String corporateOrg, String currency) {
		
			Cache fxCache = TPCacheManager.getInstance().getCache(TradePortalConstants.FX_RATE_CACHE);
			DocumentHandler resultSet = (DocumentHandler) fxCache.get(corporateOrg + "|" + currency);
		
			if (resultSet != null) {
				List<DocumentHandler> fxRateDocs = resultSet.getFragmentsList("/ResultSetRecord");
				return fxRateDocs.get(0);
			}
		
			return null;
		
		}
		
		/**
		 * Below the attributes holds for Instrument creation
		 */
		private String corporateOrgOid = null;
		private String securityRights = null;
		private String userOid = null;
		private String benName = null;
		private String currency =null;
		private BigDecimal amount = null;
		private String shipDate = null;
		private String instrumentOid = null;
		private String opOrgOid = null;
		private String clientBankOid = null;
		private MediatorServices mediatorServices;
		private String timeZone = null;
		private transient String userLocale = null;
		private transient String baseCurrency = null;
		private transient String usePaymentDate = null;
		private String transactionOid = null;
		private String termsOid = null;
		
		public String getTermsOid() {
			return termsOid;
		}

		public void setTermsOid(String termsOid) {
			this.termsOid = termsOid;
		}
			public String getTransactionOid() {
			return transactionOid;
		}

		public void setTransactionOid(String transactionOid) {
			this.transactionOid = transactionOid;
		}

			public String getUsePaymentDate() {
				return usePaymentDate;
			}
			public void setUsePaymentDate(String usePaymentDate) {
				this.usePaymentDate = usePaymentDate;
			}
			public String getBaseCurrency() {
				return baseCurrency;
			}
			public void setBaseCurrency(String baseCurrency) {
				this.baseCurrency = baseCurrency;
			}
			public String getUserLocale() {
				return userLocale;
			}
			public void setUserLocale(String userLocale) {
				this.userLocale = userLocale;
			}
			public String getTimeZone() {
				return timeZone;
			}
			public void setTimeZone(String timeZone) {
				this.timeZone = timeZone;
			}
			public MediatorServices getMediatorServices() {
				return mediatorServices;
			}
			public void setMediatorServices(MediatorServices mediatorServices) {
				this.mediatorServices = mediatorServices;
			}
			public String getClientBankOid() {
				return clientBankOid;
			}
			public void setClientBankOid(String clientBankOid) {
				this.clientBankOid = clientBankOid;
			}
			public String getOpOrgOid() {
				return opOrgOid;
			}
			public void setOpOrgOid(String opOrgOid) {
				this.opOrgOid = opOrgOid;
			}
			public String getInstrumentOid() {
				return instrumentOid;
			}
			public void setInstrumentOid(String instrumentOid) {
				this.instrumentOid = instrumentOid;
			}
			public String getCorporateOrgOid() {
				return corporateOrgOid;
			}
			public void setCorporateOrgOid(String corporateOrgOid) {
				this.corporateOrgOid = corporateOrgOid;
			}
			public String getSecurityRights() {
				return securityRights;
			}
			public void setSecurityRights(String securityRights) {
				this.securityRights = securityRights;
			}
			public String getUserOid() {
				return userOid;
			}
			public void setUserOid(String userOid) {
				this.userOid = userOid;
			}
			public String getBenName() {
				return benName;
			}
			public void setBenName(String benName) {
				this.benName = benName;
			}
			public String getCurrency() {
				return currency;
			}
			public void setCurrency(String currency) {
				this.currency = currency;
			}
			public BigDecimal getAmount() {
				return amount;
			}
			public void setAmount(BigDecimal amount) {
				this.amount = amount;
			}
			public String getShipDate() {
				return shipDate;
			}
			public void setShipDate(String shipDate) {
				this.shipDate = shipDate;
			}
			
		
			public void updateAttachmentDetails(Vector groupOids,Vector oidsList, MediatorServices mediatorSer) throws AmsException{
				
			
				LOG.debug("updateAttachmentDetails() : groupOid:"+groupOids +"\t oidsList:"+oidsList);		
					
				Vector removeList = new Vector();
				Vector addList = new Vector();
				if(oidsList!=null && oidsList.size()>0){
					Iterator it = oidsList.iterator();
					while(it.hasNext()){
						groupOids.add(it.next());
					}
				}
				LOG.debug("groupOids:"+groupOids);
				
				String groupSearchSQL = "select d.DOC_IMAGE_OID from INVOICES_SUMMARY_DATA i "
				+	" inner join DOCUMENT_IMAGE d on i.UPLOAD_INVOICE_OID = d.P_TRANSACTION_OID where i.A_INVOICE_GROUP_OID = ?";
				Iterator it = groupOids.iterator();
				while(it.hasNext()){
					String groupOid = (String)it.next();
					
					DocumentHandler oldGroupDoc = DatabaseQueryBean.getXmlResultSet(groupSearchSQL, false, new Object[]{groupOid});
					LOG.debug("oldGroupDoc:"+oldGroupDoc);
					Vector oldGroupList = null;
					 //Searching for image_id in document_image table if any attachments available for exisitng group oid
					if(oldGroupDoc!=null){
						oldGroupList = oldGroupDoc.getFragments("/ResultSetRecord");
						LOG.debug("GroupList:"+oldGroupList);
						if(oldGroupList!=null && oldGroupList.size()>=1){
							addList.add(groupOid);
						}
						
					}else{
							removeList.add(groupOid);
						}
				}	
				LOG.debug("before entering in to old sql :"+removeList);
				//Updating the group when attachment founds with NO
				String removeSQL ="UPDATE invoice_group SET attachment_ind = ?' WHERE ";
				if(removeList.size()>0){
					List<Object> sqlprmLst = new ArrayList<Object>();
					sqlprmLst.add(TradePortalConstants.INDICATOR_NO);
			        removeSQL+= POLineItemUtility.getInvoiceItemOidsSqlWithPlaceHolder(removeList, "invoice_group_oid",sqlprmLst);
				try {
					
					int resultCount = DatabaseQueryBean.executeUpdate(removeSQL, false, sqlprmLst);
					LOG.debug("resultCount:"+resultCount);
					
				} catch (SQLException e) {
					LOG.debug("exception while updating attach ind"+e.getMessage());
					e.printStackTrace();
					}		
				}			
				LOG.debug("before entering in to new sql :->"+addList);
				//Updating the group when attachment founds with YES
				String addSQL = "UPDATE invoice_group SET attachment_ind = ? WHERE ";
				if(addList.size()>0){
					List<Object> sqlprmLst = new ArrayList<Object>();
					sqlprmLst.add(TradePortalConstants.INDICATOR_YES);
			        addSQL+= POLineItemUtility.getInvoiceItemOidsSqlWithPlaceHolder(addList, "invoice_group_oid",sqlprmLst);
				try {
				
					int resultCount = DatabaseQueryBean.executeUpdate(addSQL, false, sqlprmLst);
					LOG.debug("resultCount:"+resultCount);
				
				} catch (SQLException e) {
					LOG.debug("exception while updating attach ind"+e.getMessage());
					e.printStackTrace();
					}		
				}			

				
			}
			
			/**CR 821 - Rel 8.3
			 * This method changes the status of Invoice Group to 'Available for Financing'
			 *  where the group may then be broken up if needed.
			 *
			 * @param inputDoc
			 * @param mediatorServices
			 * @return DocumentHandler
			 * @throws RemoteException
			 * @throws AmsException
			 */
			private DocumentHandler resetToAuthorize(DocumentHandler inputDoc,
					MediatorServices mediatorServices)
			throws RemoteException, AmsException {
				mediatorServices.debug("InvoiceGroupMediatorBean.resetToAuthorize: Start: ");

				// ***********************************************************************
				// check to make sure that the user is allowed to Reset to Authorise the
				// invoice - the user has to be a corporate user (not an admin
				// user) and have the rights to Accept Finance a Receivables
				// ***********************************************************************
				
				List<DocumentHandler> invoiceGroupList = inputDoc.getFragmentsList("/InvoiceGroupList/InvoiceGroup");
				if (invoiceGroupList.isEmpty()) {
					mediatorServices.getErrorManager().issueError(
							TradePortalConstants.ERR_CAT_1,
							TradePortalConstants.NO_ITEM_SELECTED,
							mediatorServices.getResourceManager().getText(
									"UploadInvoiceAction.UploadInvoiceResetToAuthorizeText",
									TradePortalConstants.TEXT_BUNDLE));
					return new DocumentHandler();
				}
				mediatorServices.debug("InvoiceGroupMediatorBean: number of invoice groups selected: "
						+ invoiceGroupList.size());

				// Process each Invoice Group in the list
				for (int i = 0; i < invoiceGroupList.size(); i++) {
					DocumentHandler invoiceGroupItem = invoiceGroupList.get(i);
					String[] invoiceData = invoiceGroupItem.getAttribute("InvoiceData").split("/");
					String invoiceGroupOid = invoiceData[0];
					String optLock = invoiceData[1];
					InvoiceGroup invoiceGroup = (InvoiceGroup) mediatorServices.createServerEJB("InvoiceGroup",
							Long.parseLong(invoiceGroupOid));
					String invoiceStatus = invoiceGroup.getAttribute("invoice_status");
					String groupDescription = invoiceGroup.getDescription();

					// only invoice status 'Available for Authorisation' and 'Partially Authorised' invoices can be
					// reset to authorise
					if (!(TradePortalConstants.UPLOAD_INV_STATUS_PAR_AUTH.equals(invoiceStatus) || TradePortalConstants.UPLOAD_INV_STATUS_FIN_PAR_AUTH.equals(invoiceStatus)
							|| TradePortalConstants.UPLOAD_INV_STATUS_AVL_FOR_AUTH.equals(invoiceStatus))) {
						mediatorServices.getErrorManager().issueError(
								InvoiceGroupMediator.class.getName(),
								TradePortalConstants.TRANSACTION_CANNOT_PROCESS,
								mediatorServices.getResourceManager().getText("InvoiceGroups.InvoiceGroup", TradePortalConstants.TEXT_BUNDLE),
								groupDescription,
								ReferenceDataManager.getRefDataMgr().getDescr("UPLOAD_INVOICE_STATUS", invoiceStatus, mediatorServices.getCSDB().getLocaleName()),
								mediatorServices.getResourceManager().getText("InvoiceSummary.ResetAuthorised", TradePortalConstants.TEXT_BUNDLE));

						continue; // Move on to next Invoice Group
					}

					String newStatus = TradePortalConstants.UPLOAD_INV_STATUS_AVL_FOR_FIN;
					invoiceGroup.setAttribute("invoice_status", newStatus);
					invoiceGroup.setAttribute("opt_lock", optLock);
					invoiceGroup.setAttribute("trading_partner_name", groupDescription);
					invoiceGroup.setAttribute("panel_auth_group_oid", null);
					invoiceGroup.setAttribute("panel_oplock_val", null);
					invoiceGroup.setAttribute("panel_auth_range_oid", null);
					int success = invoiceGroup.save();

					if (success == 1)  {
						String sql = "select UPLOAD_INVOICE_OID from INVOICES_SUMMARY_DATA where A_INVOICE_GROUP_OID = ? ";
						DocumentHandler	invoiceListDoc = DatabaseQueryBean.getXmlResultSet(sql, true, new Object[]{invoiceGroupOid});
						if (invoiceListDoc != null) {
							List<DocumentHandler> records = invoiceListDoc.getFragmentsList("/ResultSetRecord");
							for (DocumentHandler doc : records) {
								String invoiceOid = doc.getAttribute("UPLOAD_INVOICE_OID");

								if (StringFunction.isNotBlank(invoiceOid)) {
									InvoicesSummaryData invoiceSummaryData =
										(InvoicesSummaryData) mediatorServices.createServerEJB("InvoicesSummaryData",
												Long.parseLong(invoiceOid));

									invoiceSummaryData.setAttribute("invoice_status",newStatus);
									invoiceSummaryData.setAttribute("tp_rule_name", invoiceSummaryData.getTpRuleName());
									invoiceSummaryData.setAttribute("panel_auth_group_oid", null);
									invoiceSummaryData.setAttribute("panel_oplock_val", null);
									invoiceGroup.setAttribute("panel_auth_range_oid", null);

									invoiceSummaryData.save();
									
								}
							}
						}

						String sqlStatementToPanelAuth = "delete from panel_authorizer where owner_object_type = 'INVOICE_GROUP' and p_owner_oid = ? ";
						try {
							DatabaseQueryBean.executeUpdate(sqlStatementToPanelAuth, false, new Object[]{invoiceGroupOid});
						} catch (SQLException e) {
							LOG.debug("InvoiceGroupMediatorBean.resetToAUthorize: Error in deleting from panel_authorizer");
							e.printStackTrace();
						}
						// display message
						mediatorServices.getErrorManager().issueError(InvoiceGroupMediator.class.getName(),
								TradePortalConstants.INV_RESET_TO_AUTHORIZE,
								mediatorServices.getResourceManager().getText("InvoiceGroups.InvoiceGroup", TradePortalConstants.TEXT_BUNDLE),
								groupDescription);
					}
				}
				mediatorServices.debug("InvoiceGroupMediatorBean.resetToAUthorize: End: ");
				return new DocumentHandler();
			}

			private DocumentHandler performNotifyPanelAuthUser(DocumentHandler inputDoc, MediatorServices mediatorServices)
			throws RemoteException, AmsException {

				LOG.debug("InvoiceGroupMediatorBean:::performNotifyPanelAuthUser");
				String invoiceGroupOid = inputDoc.getAttribute("/InvoiceGroup/invoiceGroupOid");
				 
		    	 // create email for all selected users to notification
			    List<DocumentHandler> beneficiaryDetailsList = inputDoc.getFragmentsList("/EmailList/user");
			    StringBuilder emailReceivers = new StringBuilder();
			    String userOid = null;
			    String ownerOrgOid = null;
			    DocumentHandler outputDoc = new DocumentHandler();
			    for(DocumentHandler doc : beneficiaryDetailsList){
				    userOid = doc.getAttribute("/userOid");
					User user = (User) mediatorServices.createServerEJB("User", Long.parseLong(userOid));
					String userMailID = user.getAttribute("email_addr");
					ownerOrgOid = user.getAttribute("owner_org_oid");
					if(StringFunction.isNotBlank(userMailID)){
						emailReceivers.append(userMailID).append(",");
					}
			    }

			    if(StringFunction.isNotBlank(emailReceivers.toString())){
			      	//Retrieve Corporate Org
				   
						// Get the data from transaction that will be used to build the email message
						String amount = "";
						String currency = "";
						if (StringFunction.isNotBlank(invoiceGroupOid)) {
							String groupAmountsql = "select AMOUNT, CURRENCY from INVOICE_GROUP_VIEW where INVOICE_GROUP_OID = ?";
							DocumentHandler invoiceAmountDoc = DatabaseQueryBean.getXmlResultSet(groupAmountsql, true, new Object[]{invoiceGroupOid});
							if(invoiceAmountDoc !=null){
								currency = (invoiceAmountDoc.getFragment("/ResultSetRecord")).getAttribute("CURRENCY");
								amount = (invoiceAmountDoc.getFragment("/ResultSetRecord")).getAttribute("AMOUNT");
								amount = TPCurrencyUtility.getDisplayAmount(amount, currency, mediatorServices.getResourceManager().getResourceLocale());
							}
						}

						StringBuilder emailContent = new StringBuilder();
						 emailContent.append(mediatorServices.getResourceManager().getText("EmailTrigger.PendingPanelBodyInvoice", TradePortalConstants.TEXT_BUNDLE));
						 emailContent.append(TradePortalConstants.NEW_LINE);
							
						// Include PayRemit Amount and currency
						if (StringFunction.isNotBlank(amount)) {
							emailContent.append(mediatorServices.getResourceManager().getText("EmailTrigger.Amount",TradePortalConstants.TEXT_BUNDLE)+ " ");
							emailContent.append(currency+" "+amount);
							emailContent.append(TradePortalConstants.NEW_LINE);
						}
						
						emailContent.append(TradePortalConstants.NEW_LINE);
						// call generic method to trigger email
						InvoiceUtility.performNotifyPanelAuthUser(ownerOrgOid, emailContent, emailReceivers, mediatorServices);
				  }
		        
				return outputDoc;
			}
	
			
			private class InvoiceProperties{
				private String userOid = "";
				private String corpOrgOid = "";
				private String groupDescription = "";
				private String newGroupOid = "";
				private String userWorkGroupOid = "";
				private MediatorServices mediatorServices = null;
				private boolean groupHasBeenSaved = false;
				private String subsAuth = "";
				private String panelGroupOid = "";
				private String panelOplockVal = "";
				private String panelRangeOid  = ""; 
		        private String panelLevelCode = "";
				
				public String getPanelLevelCode() {
					return panelLevelCode;
				}
				public void setPanelLevelCode(String panelLevelCode) {
					this.panelLevelCode = panelLevelCode;
				}		
				public String getPanelGroupOid() {
					return panelGroupOid;
				}
				public void setPanelGroupOid(String panelGroupOid) {
					this.panelGroupOid = panelGroupOid;
				}
				public String getPanelOplockVal() {
					return panelOplockVal;
				}
				public void setPanelOplockVal(String panelOplockVal) {
					this.panelOplockVal = panelOplockVal;
				}
				public String getPanelRangeOid() {
					return panelRangeOid;
				}
				public void setPanelRangeOid(String panelRangeOid) {
					this.panelRangeOid = panelRangeOid;
				}
				public String getUserOid() {
					return userOid;
				}
				public void setUserOid(String userOid) {
					this.userOid = userOid;
				}
				public String getCorpOrgOid() {
					return corpOrgOid;
				}
				public void setCorpOrgOid(String corpOrgOid) {
					this.corpOrgOid = corpOrgOid;
				}
				public String getGroupDescription() {
					return groupDescription;
				}
				public void setGroupDescription(String goodsDescription) {
					this.groupDescription = goodsDescription;
				}
				public MediatorServices getMediatorServices() {
					return mediatorServices;
				}
				public void setMediatorServices(MediatorServices mediatorServices) {
					this.mediatorServices = mediatorServices;
				}
				public String getNewGroupOid() {
					return newGroupOid;
				}
				public void setNewGroupOid(String newGroupOid) {
					this.newGroupOid = newGroupOid;
				}
				public String getUserWorkGroupOid() {
					return userWorkGroupOid;
				}
				public void setUserWorkGroupOid(String userWorkGroupOid) {
					this.userWorkGroupOid = userWorkGroupOid;
				}
				public boolean isGroupHasBeenSaved() {
					return groupHasBeenSaved;
				}
				public void setGroupHasBeenSaved(boolean groupHasBeenSaved) {
					this.groupHasBeenSaved = groupHasBeenSaved;
				}
				public String getSubsAuth() {
					return subsAuth;
				}
				public void setSubsAuth(String subsAuth) {
					this.subsAuth = subsAuth;
				}
				
			}
					
			private DocumentHandler declineInvoice(DocumentHandler inputDoc,MediatorServices mediatorServices)
			throws RemoteException, AmsException {
			String userOid = inputDoc.getAttribute("/InvoiceGroupList/userOid");
			String corpOrgOid = inputDoc.getAttribute("/InvoiceGroupList/ownerOrg");
			String securityRights = inputDoc.getAttribute("/InvoiceGroupList/securityRights");
			String subsAuth =inputDoc.getAttribute("/Authorization/authorizeAtParentViewOrSubs");
			String buyerName = null;
			String userOrgOid = null;
			if (!SecurityAccess.hasRights(securityRights, SecurityAccess.REC_INVOICE_DECLINE_AUTH)) {
				// user does not have appropriate authority.
				mediatorServices.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.NO_ACTION_MATCH_NOTICE_AUTH,
						mediatorServices.getResourceManager().getText(
								"CorpCust.PayablesMgmtInvoiceDecline",
								TradePortalConstants.TEXT_BUNDLE));
				return new DocumentHandler();
			}
			ResourceManager resMgr = mediatorServices.getResourceManager();
			ErrorManager errMgr = mediatorServices.getErrorManager();
			User user = (User) mediatorServices.createServerEJB("User",
					Long.parseLong(userOid));
			userOrgOid = user.getAttribute("owner_org_oid");
			List<DocumentHandler> invoiceGroupList = inputDoc.getFragmentsList("/InvoiceGroupList/InvoiceGroup");
			if (invoiceGroupList == null || invoiceGroupList.size() == 0) {
				errMgr.issueError(TradePortalConstants.ERR_CAT_1,TradePortalConstants.NO_ITEM_SELECTED,
						resMgr.getText("UploadInvoiceAction.Authorize",TradePortalConstants.TEXT_BUNDLE));
				return new DocumentHandler();
			}
			
			String userWorkGroupOid = user.getAttribute("work_group_oid");
			CorporateOrganization corpOrg = null;
			InvoiceProperties invProp = new InvoiceProperties();
			invProp.setCorpOrgOid(corpOrgOid);
			invProp.setUserOid(userOid);
			invProp.setUserWorkGroupOid(userWorkGroupOid);
			invProp.setMediatorServices(mediatorServices);
			invProp.setSubsAuth(subsAuth);
			// Process each Invoice Group in the list
			GROUPS_LIST: 
			for (DocumentHandler invoiceGroupItem : invoiceGroupList) {
		           String[] invoiceData = invoiceGroupItem.getAttribute("InvoiceData").split("/");
		           String invoiceGroupOid = invoiceData[0];
		           String optLock = invoiceData[1];
		           invProp.setGroupHasBeenSaved(false);
		           InvoiceGroup invoiceGroup = (InvoiceGroup) mediatorServices.createServerEJB("InvoiceGroup",
		                     Long.parseLong(invoiceGroupOid));
		           String invoiceStatus = invoiceGroup.getAttribute("invoice_status");
		           String groupDescription = invoiceGroup.getAttribute("trading_partner_name");
		           invProp.setGroupDescription(groupDescription);
		                
		           // only certain invoice statuses can be authorized.
		           if (!InvoiceUtility.isInvoiceCRDeclinable(resMgr.getText("InvoiceGroups.InvoiceGroup",
		                     TradePortalConstants.TEXT_BUNDLE),groupDescription,invoiceStatus,mediatorServices)) {
		                 continue;
		            }
		              
		                invoiceGroup.setAttribute("opt_lock", optLock);
		                String invoiceGroupAmount = null;
		                String invoiceGroupCurrency = null;
		                String invoiceGroupPayAmt = "";
		               
		                if (corpOrg == null) {
		                    corpOrg = (CorporateOrganization) mediatorServices.createServerEJB("CorporateOrganization",
		                            Long.parseLong(corpOrgOid));
		                }
		                String authorizeType = corpOrg.getAttribute("dual_auth_rec_dec_inv");
		                
		                
		                String clientBankOid = corpOrg.getAttribute("client_bank_oid");
		                String groupAmountsql = "select AMOUNT, PAYMENT_AMOUNT, CURRENCY from INVOICE_GROUP_VIEW where INVOICE_GROUP_OID = ? ";
		                DocumentHandler invoiceAmountDoc = DatabaseQueryBean.getXmlResultSet(groupAmountsql, true, new Object[]{invoiceGroupOid});
		                if(invoiceAmountDoc !=null){
		                    invoiceGroupAmount = (invoiceAmountDoc.getFragment("/ResultSetRecord")).getAttribute("AMOUNT");
		                    invoiceGroupCurrency = (invoiceAmountDoc.getFragment("/ResultSetRecord")).getAttribute("CURRENCY");
		                    invoiceGroupPayAmt =  (invoiceAmountDoc.getFragment("/ResultSetRecord")).getAttribute("PAYMENT_AMOUNT");
		                }
		                // Check threshold amount start
		                
		                String baseCurrency = corpOrg.getAttribute("base_currency_code");
		                invoiceGroupPayAmt  = StringFunction.isNotBlank(invoiceGroupPayAmt)?invoiceGroupPayAmt:invoiceGroupAmount;
		                
		                String selectClause = "select distinct upload_invoice_oid, currency as currency_code , case when (payment_amount is null or payment_amount=0) " +
		                        "then amount else payment_amount end as payment_amount from invoices_summary_data ";
		                
		                boolean isValid = InvoiceUtility.checkThresholds(invoiceGroupPayAmt,invoiceGroupCurrency , baseCurrency,
		                        "upload_rec_inv_thold", "upload_rec_inv_dlimit", userOrgOid,clientBankOid, user ,selectClause, false,subsAuth, mediatorServices);
		                if(!isValid){
		                    return new DocumentHandler();
		                }
		                
		                //threshold check end
		                
		                String sql = "select UPLOAD_INVOICE_OID from INVOICES_SUMMARY_DATA where A_INVOICE_GROUP_OID = ? ";
		                DocumentHandler	invoiceListDoc = DatabaseQueryBean.getXmlResultSet(sql, true, new Object[]{invoiceGroupOid});
		                if (invoiceListDoc != null) {
		                    List<DocumentHandler> records = invoiceListDoc.getFragmentsList("/ResultSetRecord");
		                    for (DocumentHandler doc : records) {
		                        String invoiceOid = doc.getAttribute("UPLOAD_INVOICE_OID");
		                        
		                        if (StringFunction.isNotBlank(invoiceOid)) {
		                            InvoicesSummaryData invoiceSummaryData =
		                                    (InvoicesSummaryData) mediatorServices.createServerEJB("InvoicesSummaryData",Long.parseLong(invoiceOid));
		                            if(!TradePortalConstants.INDICATOR_YES.equals(invoiceSummaryData.getAttribute("portal_approval_requested_ind"))){
		                            	//This case will never happen in prod as client will not decline if invoices r not H2H uploaded
		                            	errMgr.issueError(TradePortalConstants.ERR_CAT_1,TradePortalConstants.CANNOT_DECLINE);
		                            		return new DocumentHandler();
		                        }
		                   				 
		                            String firstAuthorizer          = invoiceSummaryData.getAttribute("first_authorizing_user_oid");
		                            String firstAuthorizerWorkGroup = invoiceSummaryData.getAttribute("first_authorizing_work_group_oid");
		                            String newStatus = invoiceSummaryData.getAttribute("invoice_status");
		                            boolean isSendToTPS= false;
		                            boolean isUpdateAttachment = false;
		                            
		                            // ***********************************************************************
		                            // If dual authorization is required, set the transaction state to
		                            // partially authorized if it has not been previously authorized and set
		                            // the transaction to authorized if there has been a previous
		                            // authorizer.
		                            // The second authorizer must not be the same as the first authorizer
		                            // not the same work groups, depending on the corporate org setting.
		                            // If dual authorization is not required, set the invoice state to
		                            // authorized.
		                            // ***********************************************************************
		                            
		                            String tpName=invoiceSummaryData.getTpRuleName();
		                            invoiceSummaryData.setAttribute("tp_rule_name", tpName);
		                            
		                            if (TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS.equals(authorizeType)
		                                    && StringFunction.isBlank(userWorkGroupOid))
		                            {
		                                newStatus = TradePortalConstants.UPLOAD_INV_STATUS_DCF;
		                                
		                                if (!invProp.isGroupHasBeenSaved()) {
		                                    saveInvoiceGroup(newStatus, invoiceGroup,invProp);
		                                    errMgr.issueError(InvoiceGroupMediator.class.getName(),TradePortalConstants.DEC_WORK_GROUP_REQUIRED,
		                                    		resMgr.getText("CorpCust.RecDeclineInvoice", TradePortalConstants.TEXT_BUNDLE));
		                                }
		                                if (StringFunction.isNotBlank(invProp.getNewGroupOid())) {
		                                    invoiceSummaryData.setAttribute("invoice_group_oid",
		                                            invProp.getNewGroupOid());
		                                }
		                                saveInvoiceSummaryData(newStatus, invoiceSummaryData, invoiceOid, isSendToTPS, isUpdateAttachment, invProp);
		                            }
		                            else if (TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS.equals(authorizeType)
		                                    || TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS.equals(authorizeType))
		                            {
		                            	declineWithDiffWorkGroup(invoiceSummaryData, invoiceGroup,  firstAuthorizer,
		                                        firstAuthorizerWorkGroup,  authorizeType, errMgr, resMgr,invProp);
		                            }
		                            else {
		                                // dual auth ind is NO, first auth is EMPTY
		                                newStatus = TradePortalConstants.UPLOAD_INV_STATUS_DBC;
		                                if (!invProp.isGroupHasBeenSaved()) {
		                                    saveInvoiceGroup(newStatus, invoiceGroup, invProp);
		                                    errMgr.issueError(InvoiceGroupMediator.class.getName(),TradePortalConstants.TRANSACTION_PROCESSED,
		                                            resMgr.getText("InvoiceGroups.InvoiceGroup", TradePortalConstants.TEXT_BUNDLE),
		                                            groupDescription,resMgr.getText("TransactionAction.Declined", TradePortalConstants.TEXT_BUNDLE));
		                                    
		                                }
		                                if (StringFunction.isNotBlank(invProp.getNewGroupOid())) {
		                                    invoiceSummaryData.setAttribute("invoice_group_oid",
		                                            invProp.getNewGroupOid());
		                                }
		                                invoiceSummaryData.setAttribute("first_authorizing_user_oid", userOid);
		                                invoiceSummaryData.setAttribute("first_authorizing_work_group_oid", userWorkGroupOid);
		                                invoiceSummaryData.setAttribute("first_authorize_status_date", DateTimeUtility.getGMTDateTime());
		                                isSendToTPS = true;
		                                saveInvoiceSummaryData(newStatus, invoiceSummaryData, invoiceOid, isSendToTPS, isUpdateAttachment, invProp);
		                            }
		                            
		                        }
		                    }
		                }
		            
			}
			return new DocumentHandler();
		}
			private void saveInvoiceGroup(String newStatus, InvoiceGroup invoiceGroup, InvoiceProperties invProp) throws RemoteException, AmsException{
				
				int success=0;
				if (StringFunction.isNotBlank(newStatus) && !newStatus.equals(invoiceGroup.getAttribute("invoice_status")))
				{
					invoiceGroup.setAttribute("invoice_status", newStatus);
					String newGroupOid = InvoiceUtility.findMatchingInvoiceGroup(invoiceGroup);
					invProp.setNewGroupOid(newGroupOid);
					// check that another matching group does not already exist.
					if (StringFunction.isNotBlank(newGroupOid)
							&& !newGroupOid.equals(invoiceGroup.getAttribute("invoice_group_oid"))) {
						// there is another match, so assign the other group's OID
						// to these invoices and delete this group
						success = invoiceGroup.delete();
					}
					else {
						success = invoiceGroup.save();
					}
				}
				if (success == 1) {
					invProp.setGroupHasBeenSaved(true);
				}
		}
		
		private void saveInvoiceSummaryData(String newStatus, InvoicesSummaryData invoiceSummaryData,String invoiceOid,
				boolean isSendToTPS,boolean isUpdateAttachment,InvoiceProperties invProp) throws RemoteException, AmsException{
			saveInvoiceSummaryData(newStatus, invoiceSummaryData, invoiceOid, 
					isSendToTPS, isUpdateAttachment, invProp, false);
		}
		
		private void saveInvoiceSummaryData(String newStatus, InvoicesSummaryData invoiceSummaryData,String invoiceOid,
				boolean isSendToTPS,boolean isUpdateAttachment,InvoiceProperties invProp, boolean isPanelAuth) throws RemoteException, AmsException
				{
			String newGroupOid =invoiceSummaryData.getAttribute("invoice_group_oid");
		    invoiceSummaryData.setAttribute("invoice_status", newStatus);
		    String tpName=invoiceSummaryData.getTpRuleName();
	        invoiceSummaryData.setAttribute("tp_rule_name", tpName);
	        invoiceSummaryData.setAttribute("action",  TradePortalConstants.UPLOAD_INV_ACTION_AUTH);
	        invoiceSummaryData.setAttribute("user_oid", invProp.getUserOid());
	        invoiceSummaryData.setAttribute("reqPanleAuth", isPanelAuth? TradePortalConstants.INDICATOR_YES:TradePortalConstants.INDICATOR_NO);
	        if(invoiceSummaryData.save() == 1) {
				if(isSendToTPS)
				InvoiceUtility.createOutgoingQueueAndIssueSuccess(newStatus,
						invoiceOid, false, invProp.getMediatorServices());
				if(isUpdateAttachment)
					updateAttachmentDetails(newGroupOid,invProp.getMediatorServices());
			}
		}
		private void declineWithDiffWorkGroup(InvoicesSummaryData invoiceSummaryData,InvoiceGroup invoiceGroup, String firstAuthorizer
	    		,String firstAuthorizerWorkGroup,String authorizeType,
	    		ErrorManager errMgr,ResourceManager resMgr,InvoiceProperties invProp) throws RemoteException, AmsException
	    	{
	    		//String invoiceId = invoiceSummaryData.getAttribute("invoice_id");
	    		String invoiceOid = invoiceSummaryData.getAttribute("upload_invoice_oid");
	    		String newStatus="";
	    		
	    		boolean isSendToTPS=false;
	    		boolean isUpdateAttachment = false;
	    		if (StringFunction.isBlank(firstAuthorizer)) {
					// Two authorizers required and first authorizer is EMPTY: Set to partially authorized.
					  newStatus = TradePortalConstants.UPLOAD_INV_STATUS_PDC;

					if (!invProp.isGroupHasBeenSaved()) {
						saveInvoiceGroup(newStatus, invoiceGroup,invProp);
						errMgr.issueError(PayableInvoiceGroupMediator.class.getName(),TradePortalConstants.TRANS_PARTIALLY_AUTHORIZED,
									resMgr.getText("InvoiceGroups.InvoiceGroup",TradePortalConstants.TEXT_BUNDLE),invProp.getGroupDescription());
						
					}

					invoiceSummaryData.setAttribute("first_authorizing_user_oid", invProp.getUserOid());
					invoiceSummaryData.setAttribute("first_authorizing_work_group_oid", invProp.getUserWorkGroupOid());
					invoiceSummaryData.setAttribute("invoice_status", newStatus);
				}
				else {// firstAuthorizer not empty
					if (TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS.equals(authorizeType)
							&& invProp.getUserWorkGroupOid().equals(firstAuthorizerWorkGroup)) {
						// Two work groups required and this user belongs to the same work group as the first authorizer: Error.
						 newStatus = TradePortalConstants.UPLOAD_INV_STATUS_DCF;
						if (!invProp.isGroupHasBeenSaved()) {
							saveInvoiceGroup(newStatus, invoiceGroup,invProp);
							errMgr.issueError(PayableInvoiceGroupMediator.class.getName(),TradePortalConstants.TRANS_DIFF_WORK_GROUP_REQUIRED);
							
						}
						invoiceSummaryData.setAttribute("invoice_status", newStatus);
						
					}
					else if (firstAuthorizer.equals(invProp.getUserOid())) {
						// Two authorizers required and this user is the same as the
						// first authorizer. Give error.
						// Note requiring two work groups also implies requiring two users.
						 newStatus = TradePortalConstants.UPLOAD_INV_STATUS_DCF;
						if (!invProp.isGroupHasBeenSaved()) {
							
							saveInvoiceGroup(newStatus, invoiceGroup,invProp);
							errMgr.issueError(PayableInvoiceGroupMediator.class.getName(),TradePortalConstants.DIFFERENT_SECOND_AUTH_REQRUIED);
							
						}
						invoiceSummaryData.setAttribute("invoice_status", newStatus);
						
					}
					else {// Authorize
						newStatus = TradePortalConstants.UPLOAD_INV_STATUS_DBC;

						if (!invProp.isGroupHasBeenSaved()) {
							saveInvoiceGroup(newStatus, invoiceGroup,invProp);
							errMgr.issueError(PayableInvoiceGroupMediator.class.getName(),TradePortalConstants.TRANSACTION_PROCESSED,
									resMgr.getText("InvoiceGroups.InvoiceGroup", TradePortalConstants.TEXT_BUNDLE),invProp.getGroupDescription(),
									resMgr.getText("TransactionAction.Declined", TradePortalConstants.TEXT_BUNDLE));
						}

						invoiceSummaryData.setAttribute("second_authorizing_user_oid", invProp.getUserOid());
						invoiceSummaryData.setAttribute("second_authorizing_work_group_oid", invProp.getUserWorkGroupOid());
						invoiceSummaryData.setAttribute("second_authorize_status_date", DateTimeUtility.getGMTDateTime());
						invoiceSummaryData.setAttribute("invoice_status", newStatus);
						isSendToTPS= true;
						isUpdateAttachment = true;
					}
					
				}
	    		if (StringFunction.isNotBlank(invProp.getNewGroupOid())) {
					invoiceSummaryData.setAttribute("invoice_group_oid",
							invProp.getNewGroupOid());
				}
	    		 saveInvoiceSummaryData(newStatus, invoiceSummaryData, invoiceOid, isSendToTPS, isUpdateAttachment, invProp);
	    	     
	    	}
		
		
		
		private DocumentHandler removeFinancing(DocumentHandler inputDoc,
				MediatorServices mediatorServices)
		throws RemoteException, AmsException {
			String userOid = inputDoc.getAttribute("/InvoiceGroupList/userOid");
			String securityRights = inputDoc.getAttribute("/InvoiceGroupList/securityRights");

			if ( !SecurityAccess.hasRights(securityRights, SecurityAccess.RECEIVABLES_REMOVE_INVOICE)) {
				// user does not have appropriate authority.
				mediatorServices.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.NO_ACTION_MATCH_NOTICE_AUTH,
						mediatorServices.getResourceManager().getText(
								"UploadInvoiceAction.RemoveFinancing",
								TradePortalConstants.TEXT_BUNDLE));
				return new DocumentHandler();
			}

			List<DocumentHandler> invoiceGroupList = inputDoc.getFragmentsList("/InvoiceGroupList/InvoiceGroup");
			if (invoiceGroupList.isEmpty()) {
				mediatorServices.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.NO_ITEM_SELECTED,
						 mediatorServices.getResourceManager().getText("UploadInvoiceAction.RemoveFinancing",TradePortalConstants.TEXT_BUNDLE));
				return new DocumentHandler();
			}
			// Process each Invoice Group in the list
			for (int i = 0; i < invoiceGroupList.size(); i++) {
				DocumentHandler invoiceGroupItem = invoiceGroupList.get(i);
				String[] invoiceData = invoiceGroupItem.getAttribute("InvoiceData").split("/");
				String invoiceGroupOid = invoiceData[0];
				String optLock = invoiceData[1];
				InvoiceGroup invoiceGroup = (InvoiceGroup) mediatorServices.createServerEJB("InvoiceGroup",
						Long.parseLong(invoiceGroupOid));
				String invoiceStatus = invoiceGroup.getAttribute("invoice_status");
				String groupDescription = invoiceGroup.getDescription();

				if (!(TradePortalConstants.UPLOAD_INV_STATUS_AVL_FOR_FIN.equals(invoiceStatus) || TradePortalConstants.UPLOAD_INV_STATUS_AVL_FOR_AUTH.equals(invoiceStatus)
						||  TradePortalConstants.UPLOAD_INV_STATUS_PAR_AUTH.equals(invoiceStatus)) ) {
					mediatorServices.getErrorManager().issueError(
							InvoiceGroupMediator.class.getName(),
							TradePortalConstants.TRANSACTION_CANNOT_PROCESS,
							mediatorServices.getResourceManager().getText("InvoiceGroups.InvoiceGroup", TradePortalConstants.TEXT_BUNDLE),
							groupDescription,
							ReferenceDataManager.getRefDataMgr().getDescr("UPLOAD_INVOICE_STATUS", invoiceStatus, mediatorServices.getCSDB().getLocaleName()),
							mediatorServices.getResourceManager().getText("InvoiceSummary.RemoveFinancing", TradePortalConstants.TEXT_BUNDLE));

					continue; // Move on to next Invoice Group
				}
				String sql = "select UPLOAD_INVOICE_OID from INVOICES_SUMMARY_DATA where A_INVOICE_GROUP_OID = ? ";
					DocumentHandler	invoiceListDoc = DatabaseQueryBean.getXmlResultSet(sql, true, new Object[]{invoiceGroupOid});
					String newStatus =TradePortalConstants.UPLOAD_INV_STATUS_PEND_ACT_UNABLE_TO_FIN;
					if (invoiceListDoc != null) {
						List<DocumentHandler> records = invoiceListDoc.getFragmentsList("/ResultSetRecord");
						Map<String,String> newGroup = new HashMap();
						
						String tpName = null;
						for (DocumentHandler doc : records) { 
							String invoiceOid = doc.getAttribute("UPLOAD_INVOICE_OID");

							if (StringFunction.isNotBlank(invoiceOid)) {
								InvoicesSummaryData invoiceSummaryData =
									(InvoicesSummaryData) mediatorServices.createServerEJB("InvoicesSummaryData",
											Long.parseLong(invoiceOid));
								String invoiceId = invoiceSummaryData.getAttribute("invoice_id");
								
								try 
									{

										invoiceSummaryData.setAttribute("invoice_finance_amount", null);
										invoiceSummaryData.setAttribute("net_invoice_finance_amount", null);
										invoiceSummaryData.setAttribute("estimated_interest_amount", null);
										invoiceSummaryData.setAttribute("invoice_status", TradePortalConstants.UPLOAD_INV_STATUS_PEND_ACT_UNABLE_TO_FIN);
										// Set the optimistic lock attribute to make sure the invoice hasn't been changed by someone else since it was first retrieved
										invoiceSummaryData.setAttribute("opt_lock", optLock);//BSL CR710 02/03/2012 Rel8.0 ADD
										if(StringFunction.isBlank(tpName))
										  tpName=invoiceSummaryData.getTpRuleName(); 
										invoiceSummaryData.setAttribute("tp_rule_name", tpName);
										invoiceSummaryData.setAttribute("action",  TradePortalConstants.UPLOAD_INV_ACTION_RMV_FINANCING);
								        invoiceSummaryData.setAttribute("user_oid",userOid);
								        if(StringFunction.isNotBlank(newGroup.get(newStatus))){
								        	 invoiceSummaryData.setAttribute("bypass_grouping", "Y");
								        	 invoiceSummaryData.setAttribute("invoice_group_oid", newGroup.get(newStatus));
								        }
								        int success = invoiceSummaryData.save();
								        String newGroupOid = invoiceSummaryData.getAttribute("invoice_group_oid");
										if(!newGroup.containsKey(newStatus)){
											newGroup.put(newStatus,newGroupOid);
										}
										
									} 
									catch (Exception e) 
									{
										e.printStackTrace();
									}
								}
							
							}
						if(!newGroup.isEmpty()){
							updateAttachmentDetails(newGroup.values(),mediatorServices,invoiceGroupOid);
						}
					}
						invoiceGroup.setAttribute("invoice_status", newStatus);
						invoiceGroup.setAttribute("opt_lock", optLock);
						int success = invoiceGroup.save();
						if (success == 1) 
						{   
							// display message...
							mediatorServices.getErrorManager().issueError(
									UploadInvoiceMediator.class.getName(),
									TradePortalConstants.INV_FINANCING_REMOVED,
									mediatorServices.getResourceManager().getText("InvoiceGroups.InvoiceGroup", TradePortalConstants.TEXT_BUNDLE),
									groupDescription);

						 }
						
				}
			return new DocumentHandler();
		}
	   	
		
 }
