package com.ams.tradeportal.mediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CharsetEncoder;
import java.nio.charset.CodingErrorAction;
import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Vector;

import javax.ejb.EJBObject;

import com.ams.tradeportal.busobj.ArMatchingRule;
import com.ams.tradeportal.busobj.ClientBank;
import com.ams.tradeportal.busobj.CorporateOrganization;
import com.ams.tradeportal.busobj.CreditNotes;
import com.ams.tradeportal.busobj.CreditNotesGoods;
import com.ams.tradeportal.busobj.CreditNotesLineItemDetail;
import com.ams.tradeportal.busobj.InvoiceLineItemDetail;
import com.ams.tradeportal.busobj.InvoiceSummaryGoods;
import com.ams.tradeportal.busobj.InvoicesSummaryData;
import com.ams.tradeportal.busobj.OperationalBankOrganization;
import com.ams.tradeportal.common.TPDateTimeUtility;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.BusinessObject;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.frame.MediatorBean;
import com.amsinc.ecsg.frame.MediatorServices;
import com.amsinc.ecsg.util.DateTimeUtility;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.StringFunction;
import com.ams.tradeportal.common.Debug;

/*
 * Manages INV Message Packaging..
 *
 *     Copyright  � 2008
 *     American Management Systems, Incorporated
 *     All rights reserved
 */

public class INVUploadPackagerMediatorBean extends MediatorBean {
private static final Logger LOG = LoggerFactory.getLogger(INVUploadPackagerMediatorBean.class);
	

	protected String AGENT_ID = "";
	PreparedStatement pStmt = null;

	/*
	 * This mediator is now used to package message for uploaded invoices in
	 * portal Take the invoices_summary_data OID as input, go to that
	 * invoicesSummaryData object and extract the invoice ID and its status
	 */

	public DocumentHandler execute(DocumentHandler inputDoc,
			DocumentHandler outputDoc, MediatorServices mediatorServices)
			throws RemoteException, AmsException {
		try {

			String processParameters = null;
			String invoiceOid = null;
			String corp_org_oid = null;
			String bank_org_oid = null;
			String messageID = null;
			String operationOrganizationID = null;
			String sellerBIC = "";
			String invCustomerID = null;
			ArMatchingRule tradePartnerRule = null;

			String messageType = TradePortalConstants.INVOICE; // it is "INV"
			String destinationID = TradePortalConstants.DESTINATION_ID_PROPONIX;
			String senderID = TradePortalConstants.SENDER_ID;
			String dateSent = null;
			String timeSent = null;
			String timeStamp = DateTimeUtility.getGMTDateTime(false);

			OperationalBankOrganization operationalBankOrganization = null;
			CorporateOrganization corpOrg = null;
			
			processParameters = inputDoc.getAttribute("/process_parameters");
			boolean isCreditNoteInd = false;
			if("Y".equals(getParm("creditNoteInd", processParameters))){
				isCreditNoteInd = true;
			}
			messageID = inputDoc.getAttribute("/message_id"); // using same message ID in packaging xml which was generated while inserting INV message
			invoiceOid = getParm("InvoiceOID", processParameters);

			dateSent = timeStamp;
			timeSent = timeStamp.substring(11, 19);
			
			mediatorServices.debug("Ready to Instantiate InvoicesSummaryData Object with oid "+ invoiceOid);
			BusinessObject invoiceSummData = null;
			if(isCreditNoteInd) {
			     invoiceSummData = (CreditNotes) mediatorServices.createServerEJB("CreditNotes", Long.parseLong(invoiceOid.trim()));
			} else {
				 invoiceSummData = (InvoicesSummaryData) mediatorServices.createServerEJB("InvoicesSummaryData", Long.parseLong(invoiceOid.trim()));
			}
			// Find out the corporate org and instantiate corporate org
			corp_org_oid = invoiceSummData.getAttribute("corp_org_oid");
			mediatorServices.debug("Ready to instantiate corporate org ");
			corpOrg = (CorporateOrganization) mediatorServices.createServerEJB(
					"CorporateOrganization", Long.parseLong(corp_org_oid));

			invCustomerID = corpOrg.getAttribute("Proponix_customer_id");

			// Find the operational org from the corporate org just instantiated
			bank_org_oid = corpOrg.getAttribute("first_op_bank_org");
			mediatorServices
					.debug("Ready to instantiate operational bank org ");
			operationalBankOrganization = (OperationalBankOrganization) mediatorServices
					.createServerEJB("OperationalBankOrganization",
							Long.parseLong(bank_org_oid));
			if (operationalBankOrganization != null) {
				operationOrganizationID = operationalBankOrganization
						.getAttribute("Proponix_id");
				if (operationalBankOrganization
						.getAttribute("swift_address_part1") != null)
					sellerBIC = operationalBankOrganization
							.getAttribute("swift_address_part1")
							+ operationalBankOrganization
									.getAttribute("swift_address_part2");

			}
			long clientBankOid = corpOrg.getAttributeLong("client_bank_oid");
			ClientBank clientBank = (ClientBank) mediatorServices
					.createServerEJB("ClientBank", clientBankOid);

			String clientBankID = clientBank != null ? clientBank
					.getAttribute("OTL_id") : "";
			tradePartnerRule = getMatchingRule(invoiceSummData, mediatorServices);
			outputDoc = new DocumentHandler();

			outputDoc.setAttribute("/Proponix/Header/DestinationID",
					destinationID);
			outputDoc.setAttribute("../SenderID", senderID);
			outputDoc.setAttribute("/Proponix/Header/OperationOrganizationID",
					operationOrganizationID);
			outputDoc.setAttribute("../MessageType", messageType);
			outputDoc.setAttribute("../DateSent", dateSent);
			outputDoc.setAttribute("../TimeSent", timeSent);
			outputDoc.setAttribute("../MessageID", messageID);
			outputDoc.setAttribute("../ClientBankID", clientBankID);
			updateInvoiceSubHeaderSection(outputDoc, invoiceSummData,
					tradePartnerRule, invCustomerID,mediatorServices, processParameters, corpOrg, isCreditNoteInd);
			updateInvoiceBodySection(outputDoc, sellerBIC, invoiceSummData,
					tradePartnerRule, corpOrg, mediatorServices,
					processParameters, isCreditNoteInd);


		} catch (RemoteException e) {
			mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_MIDDLEWARE,
					TradePortalConstants.TASK_NOT_SUCCESSFUL,
					"Uploaded Invoice Packaging");
			e.printStackTrace();
		} catch (AmsException e) {
			mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_MIDDLEWARE,
					TradePortalConstants.TASK_NOT_SUCCESSFUL,
					"Uploaded Invoice Packaging");
			e.printStackTrace();
		} catch (Exception e) {
			mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_MIDDLEWARE,
					TradePortalConstants.TASK_NOT_SUCCESSFUL,
					"Uploaded Invoice Packaging");
			e.printStackTrace();
		}


		return outputDoc;
	}// end of execute()

	public void updateInvoiceSubHeaderSection(DocumentHandler outputDoc,
			BusinessObject invoiceSummData,
			ArMatchingRule tradePartnerRule, String invCustomerID,MediatorServices mediatorServices,
			String processParameters, CorporateOrganization corpOrg, boolean isCreditNoteInd) throws RemoteException, AmsException {

		outputDoc.setAttribute("/Proponix/SubHeader/ComrclDocRef/Id",
				invoiceSummData.getAttribute("invoice_id"));
		String date = "";
		if (StringFunction.isNotBlank(invoiceSummData
				.getAttribute("issue_date"))) {
			date = TPDateTimeUtility.packagingInvoiceDate(invoiceSummData
					.getAttribute("issue_date"));
		}
		outputDoc.setAttribute("../DtOfIsse", date);
		outputDoc.setAttribute("/Proponix/SubHeader/SequenceOfTotal",
				getParm("Sequence", processParameters));
		//SHR CR708 Rel8.1.1 Start get invoice type from the INM packager
		outputDoc.setAttribute("../InvoiceType", getParm("invoiceType", processParameters));
		//SHR CR708 Rel8.1.1 End
		String amend ="";
		if(!isCreditNoteInd){
			if(StringFunction.isNotBlank(getParm("invoiceType", processParameters))){
				if("UNI".equals(getParm("invoiceType", processParameters))){
					amend = StringFunction.isNotBlank(invoiceSummData.getAttribute("amend_seq_no"))?invoiceSummData.getAttribute("amend_seq_no"):"0";
				}
				else{
					amend = StringFunction.isNotBlank(invoiceSummData.getAttribute("amend_seq_no"))?invoiceSummData.getAttribute("amend_seq_no"):"1";
				}
			}
		}
		outputDoc.setAttribute("../AmendSeqNo", amend);
		outputDoc.setAttribute("../LinkToInstrType",
				invoiceSummData.getAttribute("linked_to_instrument_type"));
		outputDoc.setAttribute("../CustomerID", invCustomerID);
		outputDoc.setAttribute("../MatchingStatus", "UMAT");
		String status="";
		if(!isCreditNoteInd){
			status = invoiceSummData.getAttribute("invoice_status");
		}
		else{
			status = invoiceSummData.getAttribute("credit_note_status");
		}
		if(TradePortalConstants.UPLOAD_INV_STATUS_DBC.equals(status)){
			outputDoc.setAttribute("../InvoiceStatus", TradePortalConstants.UPLOAD_INV_STATUS_DBC);
		}else{
			outputDoc.setAttribute("../InvoiceStatus", "APP");
		}

		if (StringFunction.isNotBlank(invoiceSummData
				.getAttribute("due_date"))) {
			date = TPDateTimeUtility.packagingInvoiceDate(invoiceSummData
					.getAttribute("due_date"));
		}
		outputDoc.setAttribute("../InvoiceDueDate", date);
		String finvalue = "";
		if(TradePortalConstants.INV_LINKED_INSTR_PYB.equals(invoiceSummData.getAttribute("linked_to_instrument_type"))){
			outputDoc.setAttribute("/Proponix/SubHeader/FinanceStatus", "");
		}
		else{
		if (StringFunction.isNotBlank(invoiceSummData
				.getAttribute("net_invoice_finance_amount"))) {
			outputDoc.setAttribute("../FinanceStatus", "AFF");
			finvalue = tradePartnerRule!=null?tradePartnerRule.getAttribute("invoice_value_finance"):"";
		}

		else {
			outputDoc.setAttribute("/Proponix/SubHeader/FinanceStatus", "NFN");
		}
		}
		//SHR CR708 Rel8.1.1 Start
		outputDoc.setAttribute("/Proponix/SubHeader/GroupReferenceNumber", getParm("GroupReferenceNumber", processParameters));
		outputDoc.setAttribute("../GroupTotalNo", getParm("GroupTotalNo", processParameters));
		//SHR CR708 Rel8.1.1 End
		outputDoc.setAttribute("../PurposeType", "");
		String finprcnt = corpOrg.getAttribute("inv_percent_to_fin_trade_loan");
		if (!StringFunction.isBlank(finvalue) &&
				TradePortalConstants.REC.equals(invoiceSummData.getAttribute("linked_to_instrument_type"))) {
			int percentFinanceAllowed = Integer.parseInt(finvalue);
			BigDecimal percent = new BigDecimal(percentFinanceAllowed)
					.divide(new BigDecimal(100));
			outputDoc.setAttribute("../FinancePercentage", percent.toString());
		}
		//IR 22634 - For Rec Trade Loan and Export Loans send the finance percentage value
		else if ((!isCreditNoteInd) && StringFunction.isNotBlank(finprcnt) && 
				(TradePortalConstants.TRADE_LOAN.equals(invoiceSummData.getAttribute("loan_type")) || 
						TradePortalConstants.EXPORT_FIN.equals(invoiceSummData.getAttribute("loan_type")))  && 
				TradePortalConstants.INVOICE_TYPE_REC_MGMT.equals(invoiceSummData.getAttribute("invoice_classification"))){
			BigDecimal percent = new BigDecimal(finprcnt)
										.divide(new BigDecimal(100));
			outputDoc.setAttribute("../FinancePercentage", percent.toString());
			
		} 
		//IR-22634- For PAYABLES-all loan types - send 100%		
		else if (TradePortalConstants.INVOICE_TYPE_PAY_MGMT.equals(invoiceSummData.getAttribute("invoice_classification"))){
			BigDecimal percent = new BigDecimal("100")
										.divide(new BigDecimal(100));
			outputDoc.setAttribute("../FinancePercentage", percent.toString());
			
		} else {
			outputDoc.setAttribute("../FinancePercentage", "");
		}
		if(TradePortalConstants.INV_LINKED_INSTR_PYB.equals(invoiceSummData.getAttribute("linked_to_instrument_type"))){
			outputDoc.setAttribute("../FinancingType", "");
		}
		else{
		if (TradePortalConstants.REC.equals(invoiceSummData.getAttribute("linked_to_instrument_type"))){
			outputDoc.setAttribute("../FinancingType", "RFI");
		}else if (TradePortalConstants.TRADE_LOAN.equals(invoiceSummData.getAttribute("loan_type"))){
			if (TradePortalConstants.INVOICE_TYPE_REC_MGMT.equals(invoiceSummData.getAttribute("invoice_classification"))){
				outputDoc.setAttribute("../FinancingType", TradePortalConstants.TRADE_LOAN_REC);	
			} else if (TradePortalConstants.INVOICE_TYPE_PAY_MGMT.equals(invoiceSummData.getAttribute("invoice_classification"))){
				outputDoc.setAttribute("../FinancingType", TradePortalConstants.TRADE_LOAN_PAY);	
			}
		}else{
			outputDoc.setAttribute("../FinancingType", "RFI");
		}
		}
		if (StringFunction.isNotBlank(invoiceSummData
				.getAttribute("payment_date"))) {
			date = TPDateTimeUtility.packagingInvoiceDate(invoiceSummData
					.getAttribute("payment_date"));
			outputDoc.setAttribute("../PaymentDate", date);
		}
		// SHR CR-708 Rel8.1.1 Start
		String lineItemOid = getParm("StartLineItemOID", processParameters);
		if (StringFunction.isNotBlank(lineItemOid)) {
			String derivedLineItemInd = null;
			if(isCreditNoteInd){
				CreditNotesLineItemDetail busObj = (CreditNotesLineItemDetail) mediatorServices
					.createServerEJB("CreditNotesLineItemDetail", Long.parseLong(lineItemOid));
			 derivedLineItemInd = busObj.getAttribute("derived_line_item_ind");
			} else {
				 InvoiceLineItemDetail busObj = (InvoiceLineItemDetail) mediatorServices
							.createServerEJB("InvoiceLineItemDetail", Long.parseLong(lineItemOid));
				 derivedLineItemInd = busObj.getAttribute("derived_line_item_ind");
			}
			outputDoc.setAttribute("../DerivedLineItem", derivedLineItemInd);
		}
		outputDoc.setAttribute("../PayablesReceivablesDesignation", invoiceSummData.getAttribute("invoice_classification"));
                // WZhu Yogesh 2/25/13 Rel 8.2 CR-708B 
		outputDoc.setAttribute("../BuyerTPSCustomerID", invoiceSummData.getAttribute("buyer_tps_customer_id"));
		outputDoc.setAttribute("../SellerTPSCustomerID", invoiceSummData.getAttribute("seller_tps_customer_id"));                
		//SHR CR-708 Rel8.1.1 End
		
		//CR 913 start
		outputDoc.setAttribute("../PaymentAmount", invoiceSummData.getAttribute("payment_amount"));
		String suppDate = invoiceSummData.getAttribute("send_to_supplier_date");
		if (StringFunction.isNotBlank(suppDate)) {
		date = TPDateTimeUtility.packagingInvoiceDate(suppDate);
		outputDoc.setAttribute("../SendToSupplierDate", date); 
		}
		outputDoc.setAttribute("../BuyerAccountCCY", invoiceSummData.getAttribute("buyer_acct_currency"));
		outputDoc.setAttribute("../BuyerAccountNum", invoiceSummData.getAttribute("buyer_acct_num"));
		//CR 913 end
		
		// Nar - CR 914A Release 9.2.0.0 01/12/2015 Begin
		appendCreditNoteDetailInSubHeader(processParameters, outputDoc, invoiceSummData, isCreditNoteInd);		
		// Nar - CR 914A Release 9.2.0.0 01/12/2015 End
		
	}

	public void updateInvoiceBodySection(DocumentHandler outputDoc, String BIC,
			BusinessObject invoiceSummData,
			ArMatchingRule tradePartnerRule, CorporateOrganization corpOrg,
			MediatorServices mediatorServices, String processParameters, boolean isCreditNoteInd)
			throws AmsException, RemoteException {

        
		outputDoc.setAttribute("/Proponix/Body/BuyrBk/BIC", "");// optional
		// now
		outputDoc.setAttribute("/Proponix/Body/SellrBk/BIC", BIC);
		// outputDoc.setAttribute("/Proponix/Body/NewComrclDataSet/DataSetId/","");//TODO
		// not needed now



		DocumentHandler goodsDoc = new DocumentHandler();
		goodsDoc.setAttribute(
				"/Proponix/Body/NewComrclDataSet/ComrclDocRef/Id",
				invoiceSummData.getAttribute("invoice_id"));
		String date = "";
		// convert from mm/DD/YYYY to yyyy-MM-dd
		if (StringFunction.isNotBlank(invoiceSummData
				.getAttribute("issue_date"))) {
			date = TPDateTimeUtility.packagingInvoiceDate(invoiceSummData
					.getAttribute("issue_date"));
		}
		goodsDoc.setAttribute(
				"/Proponix/Body/NewComrclDataSet/ComrclDocRef/DtOfIsse", date);
		// SHR CR-708 Rel8.1.1 Start
		String buyer_id,buyer_name = "";
		String currency = invoiceSummData.getAttribute("currency");
		if(TradePortalConstants.INVOICE_TYPE_PAY_MGMT.equals(invoiceSummData.getAttribute("invoice_classification"))){
			 buyer_id = invoiceSummData.getAttribute("seller_id");
			 buyer_name = invoiceSummData.getAttribute("seller_name");

			 if (corpOrg != null) {
					goodsDoc.setAttribute("/Proponix/Body/NewComrclDataSet/Buyr/Nm",
							corpOrg.getAttribute("name"));
					String street1 = corpOrg.getAttribute("address_line_1");
					String street2 = corpOrg.getAttribute("address_line_2");
					//IR T36000016816 - add space b/w address1 and address2
					street1 = StringFunction.isNotBlank(street1) ? street1+" "
							+ street2 : street2;

					goodsDoc.setAttribute(
							"/Proponix/Body/NewComrclDataSet/Buyr/PstlAdr/StrtNm",
							street1);
					goodsDoc.setAttribute("../PstCdId",
							corpOrg.getAttribute("address_postal_code"));
					goodsDoc.setAttribute("../TwnNm",
							corpOrg.getAttribute("address_city"));
					goodsDoc.setAttribute("../CtrySubDvsn",
							corpOrg.getAttribute("address_state_province"));
					goodsDoc.setAttribute("../Ctry",
							corpOrg.getAttribute("address_country"));
				}
				// SHR CR-708 Rel8.1.1 End

				 if (!StringFunction.isBlank(buyer_name))
					goodsDoc.setAttribute("/Proponix/Body/NewComrclDataSet/Sellr/Nm",
							buyer_name);

				 else if (!StringFunction.isBlank(buyer_id))
					goodsDoc.setAttribute("/Proponix/Body/NewComrclDataSet/Sellr/BEI",
							buyer_id);

				if (tradePartnerRule != null) {
					String street1 = tradePartnerRule.getAttribute("address_line_1");
					String street2 = tradePartnerRule.getAttribute("address_line_2");
					//IR T36000016816 - add space b/w address1 and address2
					street1 = StringFunction.isNotBlank(street1) ? street1 + " "
							+ street2 : street2;
					goodsDoc.setAttribute(
							"/Proponix/Body/NewComrclDataSet/Sellr/PstlAdr/StrtNm",
							street1);
					goodsDoc.setAttribute("../PstCdId",
							tradePartnerRule.getAttribute("postalcode"));

					goodsDoc.setAttribute("../TwnNm",
							tradePartnerRule.getAttribute("address_city"));
					goodsDoc.setAttribute("../CtrySubDvsn",
							tradePartnerRule.getAttribute("address_state"));
					goodsDoc.setAttribute("../Ctry",
							tradePartnerRule.getAttribute("address_country"));

				}
		}
		else{
		 buyer_id = invoiceSummData.getAttribute("buyer_id");
		 buyer_id= replaceNonWin1252Chars(buyer_id); // IR T36000042640 Rel9.5 04/06/2016
		 buyer_name = invoiceSummData.getAttribute("buyer_name");
		 buyer_name = replaceNonWin1252Chars(buyer_name); // IR T36000042640 Rel9.5 04/06/2016
		 if (!StringFunction.isBlank(buyer_name))
				goodsDoc.setAttribute("/Proponix/Body/NewComrclDataSet/Buyr/Nm",
						buyer_name);

			 else if (!StringFunction.isBlank(buyer_id))
				goodsDoc.setAttribute("/Proponix/Body/NewComrclDataSet/Buyr/BEI",
						buyer_id);

			if (tradePartnerRule != null) {
				String street1 = tradePartnerRule.getAttribute("address_line_1");
				String street2 = tradePartnerRule.getAttribute("address_line_2");
				//IR T36000016816 - add space b/w address1 and address2
				street1 = StringFunction.isNotBlank(street1) ? street1+ " "
						+ street2 : street2;
				goodsDoc.setAttribute(
						"/Proponix/Body/NewComrclDataSet/Buyr/PstlAdr/StrtNm",
						street1);
				goodsDoc.setAttribute("../PstCdId",
						tradePartnerRule.getAttribute("postalcode"));

				goodsDoc.setAttribute("../TwnNm",
						tradePartnerRule.getAttribute("address_city"));
				goodsDoc.setAttribute("../CtrySubDvsn",
						tradePartnerRule.getAttribute("address_state"));
				goodsDoc.setAttribute("../Ctry",
						tradePartnerRule.getAttribute("address_country"));

			}

		 if (corpOrg != null) {
				goodsDoc.setAttribute("/Proponix/Body/NewComrclDataSet/Sellr/Nm",
						corpOrg.getAttribute("name"));
				String street1 = corpOrg.getAttribute("address_line_1");
				String street2 = corpOrg.getAttribute("address_line_2");
				//IR T36000016816 - add space b/w address1 and address2
				street1 = StringFunction.isNotBlank(street1) ? street1 + " "
						+ street2 : street2;

				goodsDoc.setAttribute(
						"/Proponix/Body/NewComrclDataSet/Sellr/PstlAdr/StrtNm",
						street1);
				goodsDoc.setAttribute("../PstCdId",
						corpOrg.getAttribute("address_postal_code"));
				goodsDoc.setAttribute("../TwnNm",
						corpOrg.getAttribute("address_city"));
				goodsDoc.setAttribute("../CtrySubDvsn",
						corpOrg.getAttribute("address_state_province"));
				goodsDoc.setAttribute("../Ctry",
						corpOrg.getAttribute("address_country"));
			}
			// SHR CR-708 Rel8.1.1 End


		}
		// update goods
		BusinessObject lineItems = null;
		BusinessObject goods = null;
		Set<Long> goodsOIDSet = new HashSet<Long>();
		String invStartLineItemOID = getParm("StartLineItemOID",
				processParameters);
		String invEndLineItemOID = getParm("EndLineItemOID", processParameters);
		//jgadela  R92 - SQL Injection FIX
		String sql = null;
		if(isCreditNoteInd){
		  sql = " select upload_credit_line_item_oid as upload_line_item_detail_oid from credit_notes_line_items where upload_credit_line_item_oid between ? and ? order by upload_credit_line_item_oid";
		} else {
		  sql = " select upload_line_item_detail_oid from invoice_line_item_detail where upload_line_item_detail_oid between ? and ? order by upload_line_item_detail_oid";	
		}
		DocumentHandler transprtDoc = new DocumentHandler();
		DocumentHandler goodsDoc1 = new DocumentHandler();
		DocumentHandler resultSet = DatabaseQueryBean.getXmlResultSet(sql, true, invStartLineItemOID, invEndLineItemOID);
		if (resultSet != null) {
			Vector list1 = resultSet.getFragments("/ResultSetRecord");
			int goodsNum = 0;
			String id = "";
			String desc = "";
			boolean isNew = true;
			for (int i = 0; i < list1.size(); i++) {
				try {

					long lineItemOid = ((DocumentHandler) list1.elementAt(i))
							.getAttributeLong("/UPLOAD_LINE_ITEM_DETAIL_OID");
					 long goodsOID;
					if(isCreditNoteInd){
					   lineItems = (CreditNotesLineItemDetail) mediatorServices
							.createServerEJB("CreditNotesLineItemDetail",
									lineItemOid);
					   goodsOID = lineItems
							.getAttributeLong("credit_notes_goods_oid");
					} else {
						lineItems = (InvoiceLineItemDetail) mediatorServices
								.createServerEJB("InvoiceLineItemDetail",
										lineItemOid);
						goodsOID = lineItems
								.getAttributeLong("invoice_summary_goods_oid");
					}
					isNew = goodsOIDSet.add(goodsOID);
					if (isNew)
						goodsNum++;
					if(isCreditNoteInd){
					    goods = (CreditNotesGoods) mediatorServices
							.createServerEJB("CreditNotesGoods", goodsOID);
					} else {
						goods = (InvoiceSummaryGoods) mediatorServices
								.createServerEJB("InvoiceSummaryGoods", goodsOID);
					}

					if (goods != null) {
						desc = goods.getAttribute("goods_description");
						if(!isCreditNoteInd){
						 id = goods.getAttribute("purchase_order_id");
						}
						id = StringFunction.isNotBlank(id) ? id : "1";
						goodsDoc.setAttribute(
								"/Proponix/Body/NewComrclDataSet/Goods("
										+ goodsNum + ")/PurchsOrdrRef/Id", id);
						// date format changed to be yyyy-MM-dd
						goodsDoc.setAttribute(
								"/Proponix/Body/NewComrclDataSet/Goods("
										+ goodsNum + ")/PurchsOrdrRef/DtOfIsse",
								date);

						if (lineItems != null) {
							String value = lineItems
									.getAttribute("line_item_id");
							Debug.debug("Goods...LineItems"
									+ goods.getAttribute("goods_description")
									+ value);

							if (StringFunction.isBlank(value))
								value = "1";
							goodsDoc.setAttribute(
									"/Proponix/Body/NewComrclDataSet/Goods("
											+ goodsNum + ")/ComrclLineItms("
											+ i + ")/LineItmId", value);

							goodsDoc.setAttribute(
									"/Proponix/Body/NewComrclDataSet/Goods("
											+ goodsNum + ")/ComrclLineItms("
											+ i + ")/Qty/UnitOfMeasrCd",
									lineItems
											.getAttribute("unit_of_measure_quantity"));
							goodsDoc.setAttribute(
									"/Proponix/Body/NewComrclDataSet/Goods("
											+ goodsNum + ")/ComrclLineItms("
											+ i + ")/Qty/Val",
									lineItems.getAttribute("inv_quantity"));
							goodsDoc.setAttribute(
									"/Proponix/Body/NewComrclDataSet/Goods("
											+ goodsNum + ")/ComrclLineItms("
											+ i + ")/UnitPric/UnitOfMeasrCd",
									lineItems
											.getAttribute("unit_of_measure_price"));
							String unitPrices = lineItems.getAttribute("unit_price");
							if(isCreditNoteInd && !unitPrices.startsWith("-")){
								unitPrices = "-" + unitPrices;
							}
							goodsDoc.setAttribute(
									"/Proponix/Body/NewComrclDataSet/Goods("
											+ goodsNum + ")/ComrclLineItms("
											+ i + ")/UnitPric/Amt[Ccy="
											+ currency + "]",
											unitPrices);

							for (int k = 0; k < 7; k++) {
								String type = lineItems
										.getAttribute("prod_chars_ud" + (k + 1)
												+ "_type");
								if (StringFunction.isNotBlank(type)) {

									goodsDoc.setAttribute(
											"/Proponix/Body/NewComrclDataSet/Goods("
													+ goodsNum
													+ ")/ComrclLineItms(" + i
													+ ")/PdctChrtcs(" + k
													+ ")/OthrPdctChrtcs/Id",
											lineItems
													.getAttribute("prod_chars_ud"
															+ (k + 1) + "_val"));
									goodsDoc.setAttribute(
											"/Proponix/Body/NewComrclDataSet/Goods("
													+ goodsNum
													+ ")/ComrclLineItms(" + i
													+ ")/PdctChrtcs(" + k
													+ ")/OthrPdctChrtcs/IdTp",
											type);
								} else
									break;
							}
							BigDecimal lineAmt = new BigDecimal(unitPrices)
									.multiply(new BigDecimal(lineItems
											.getAttribute("inv_quantity")));
							goodsDoc.setAttribute(
									"/Proponix/Body/NewComrclDataSet/Goods("
											+ goodsNum + ")/ComrclLineItms("
											+ i + ")/TtlAmt[Ccy=" + currency
											+ "]", lineAmt.toString());

						}

							goodsDoc1.setAttribute(
									"/Proponix/Body/NewComrclDataSet/Goods("
											+ goodsNum
											+ ")/LineItmsTtlAmt[Ccy="
											+ currency + "]",
									invoiceSummData.getAttribute("amount"));
							goodsDoc1.setAttribute(
									"/Proponix/Body/NewComrclDataSet/Goods("
											+ goodsNum + ")/Incotrms/Cd",
									goods.getAttribute("incoterm"));
							goodsDoc1.setAttribute(
									"/Proponix/Body/NewComrclDataSet/Goods("
											+ goodsNum + ")/Incotrms/Lctn", "");// reqtag
							goodsDoc1.setAttribute(
									"/Proponix/Body/NewComrclDataSet/Goods("
											+ goodsNum + ")/TtlNetAmt[Ccy="
											+ currency + "]",
									invoiceSummData.getAttribute("amount"));
							// buyer defined
							for (int l = 0; l < TradePortalConstants.INV_BUYER_OR_SELLER_USER_DEF_FIELD; l++) {
								String label = goods
										.getAttribute("buyer_users_def"
												+ (l + 1) + "_label");
								if (StringFunction.isNotBlank(label)) {
									goodsDoc1.setAttribute(
											"/Proponix/Body/NewComrclDataSet/Goods("
													+ goodsNum
													+ ")/BuyrDfndInf(" + l
													+ ")/Labl", label);
									goodsDoc1.setAttribute(
											"/Proponix/Body/NewComrclDataSet/Goods("
													+ goodsNum
													+ ")/BuyrDfndInf(" + l
													+ ")/Inf",
											goods.getAttribute("buyer_users_def"
													+ (l + 1) + "_value"));
								} else
									break;
							}

							// Seller defined
							for (int l = 0; l < TradePortalConstants.INV_BUYER_OR_SELLER_USER_DEF_FIELD; l++) {
								String label = goods
										.getAttribute("seller_users_def"
												+ (l + 1) + "_label");
								if (StringFunction.isNotBlank(label)) {
									goodsDoc1.setAttribute(
											"/Proponix/Body/NewComrclDataSet/Goods("
													+ goodsNum
													+ ")/SellrDfndInf(" + l
													+ ")/Labl", label);
									goodsDoc1.setAttribute(
											"/Proponix/Body/NewComrclDataSet/Goods("
													+ goodsNum
													+ ")/SellrDfndInf(" + l
													+ ")/Inf",
											goods.getAttribute("seller_users_def"
													+ (l + 1) + "_value"));
								} else
									break;
						}
						transprtDoc.setAttribute(
								"/Proponix/Body/NewTrnsprtDataSet/TrnsprtInf("
										+ goodsNum + ")/TrnsprtDocRef/Id", "");
						transprtDoc.setAttribute("../DtOfIsse", "");
						transprtDoc.setAttribute(
								"/Proponix/Body/NewTrnsprtDataSet/TrnsprtInf("
										+ goodsNum
										+ ")/TrnsprtdGoods/PurchsOrdrRef/Id",
								id);
						transprtDoc
								.setAttribute(
										"/Proponix/Body/NewTrnsprtDataSet/TrnsprtInf("
												+ goodsNum
												+ ")/TrnsprtdGoods/PurchsOrdrRef/DtOfIsse",
										date);
						transprtDoc.setAttribute(
								"/Proponix/Body/NewTrnsprtDataSet/TrnsprtInf("
										+ goodsNum
										+ ")/TrnsprtdGoods/GoodsDesc", desc);

						//BSL IR MRUM051154567 05/10/2012 Rel 8.0 BEGIN
						transprtDoc.setAttribute("/Proponix/Body/NewTrnsprtDataSet/TrnsprtInf(" + goodsNum + ")/RtgSummry/IndvTrnsprt/TrnsprtByAir/DprtureAirprt", "");
						transprtDoc.setAttribute("/Proponix/Body/NewTrnsprtDataSet/TrnsprtInf(" + goodsNum + ")/RtgSummry/IndvTrnsprt/TrnsprtByAir/DstnAirprt", "");
						//BSL IR MRUM051154567 05/10/2012 Rel 8.0 END
						transprtDoc.setAttribute("/Proponix/Body/NewTrnsprtDataSet/TrnsprtInf(" + goodsNum + ")/RtgSummry/IndvTrnsprt/TrnsprtByAir/AirCrrierNm", goods.getAttribute("carrier")); //Vshah - IR#DNUM050340625
						transprtDoc
								.setAttribute(
										"/Proponix/Body/NewTrnsprtDataSet/TrnsprtInf("
												+ goodsNum
												+ ")/RtgSummry/IndvTrnsprt/TrnsprtBySea/PortOfLoadng",
										goods.getAttribute("country_of_loading"));
						transprtDoc
								.setAttribute(
										"/Proponix/Body/NewTrnsprtDataSet/TrnsprtInf("
												+ goodsNum
												+ ")/RtgSummry/IndvTrnsprt/TrnsprtBySea/PortOfDschrge",
										goods.getAttribute("country_of_discharge"));

						transprtDoc.setAttribute("/Proponix/Body/NewTrnsprtDataSet/TrnsprtInf("+ goodsNum + ")/RtgSummry/IndvTrnsprt/TrnsprtBySea/VsslNm", goods.getAttribute("vessel"));//Vshah - IR#DNUM050340625
                        // Nar - IR#DEUM051649979 Begin
						String actualShpimentDate = goods.getAttribute("actual_ship_date");
						if (StringFunction.isNotBlank(actualShpimentDate))
						{
							actualShpimentDate = TPDateTimeUtility.packagingInvoiceDate(actualShpimentDate);
							transprtDoc.setAttribute("/Proponix/Body/NewTrnsprtDataSet/TrnsprtInf("+ goodsNum + ")/ActlShipmntDt", actualShpimentDate);
						}

                        // Nar - IR#DEUM051649979 End

					}
				} catch (Exception e) {

				} finally {
					removeEJBReference(goods);
					removeEJBReference(lineItems);

				}

			}
			Vector goodsVector = goodsDoc1.getFragments("/Proponix/Body/NewComrclDataSet/Goods");
			for (int i = 0; i < goodsVector.size(); i++) {
				goodsDoc.mergeDocument("/Proponix/Body/NewComrclDataSet/Goods("+(i+1)+")",
					goodsDoc1.getFragment("/Proponix/Body/NewComrclDataSet/Goods("+(i+1)+")/LineItmsTtlAmt"));

			}
			outputDoc.setComponent("/Proponix/Body/NewComrclDataSet",
					goodsDoc.getFragment("/Proponix/Body/NewComrclDataSet"));

			outputDoc.setAttribute("/Proponix/Body/NewComrclDataSet/PmtTerms",
					"");// required
						// in
						// dtd
			outputDoc
					.setAttribute(
							"/Proponix/Body/NewComrclDataSet/SttlmTerms/BnfcryAcct",
							"");// required in dtd
			outputDoc
					.setComponent(
							"/Proponix/Body/NewTrnsprtDataSet",
							transprtDoc
									.getFragment("/Proponix/Body/NewTrnsprtDataSet"));

		}
		
		String itemOid;
		if(isCreditNoteInd){
			itemOid =  invoiceSummData.getAttribute("upload_credit_note_oid");
		}else {
			itemOid =  invoiceSummData.getAttribute("upload_invoice_oid");
		}
		packageAttachedDocuments( itemOid,  outputDoc);   // IR# NUM042044530  Rel. 8.0.0
		//RKAZI CR709 Rel 8.2 03/13/2013 - Start
		String payMethod = (String) invoiceSummData.getAttribute("pay_method");
		if (StringFunction.isNotBlank(payMethod)){
			packagepaymentBeneficiary(invoiceSummData,outputDoc,isCreditNoteInd);				//RKAZI CR709 Rel 8.2 01/25/2013 - Add
		}
		//RKAZI CR709 Rel 8.2 03/13/2013 - End
		// Nar CR-914A Rel9.2 01/08/2014 Begin
		if(isCreditNoteInd){
			String credAppliedInvDetailOid = getParm("credAppliedInvDetailOid", processParameters);
			if(StringFunction.isNotBlank(credAppliedInvDetailOid)){
				String credAppliedInvDetail = getCredAppliedInvDetail(credAppliedInvDetailOid);
				String creditNotePreApplicationStatus = getParm("creditNotePreApplicationStatus", processParameters);
				packageAppliedInvoiceInCreditNotes(creditNotePreApplicationStatus, credAppliedInvDetail, outputDoc);
			}
			
		} else if(StringFunction.isNotBlank(invoiceSummData.getAttribute("total_credit_note_amount"))) {
			packageAppliedCreditNotesInInvocie(invoiceSummData,outputDoc);
		}
		// Nar CR-914 Rel9.2 01/08/2014 End
	}

				//RKAZI CR709 Rel 8.2 01/25/2013 -Start
	private void packagepaymentBeneficiary(BusinessObject invoiceSummData,
			DocumentHandler outputDoc, boolean isCreditNoteInd) throws RemoteException, AmsException {
		// TODO Auto-generated method stub
		String email=invoiceSummData.getAttribute("ben_email_addr");
		if(StringFunction.isNotBlank(email)){
			email = email.replaceAll(":", ",");
		}
		outputDoc.setAttribute("/Proponix/Body/PaymentBeneficiary/PaymentMethod", invoiceSummData.getAttribute("pay_method"));
		outputDoc.setAttribute("/Proponix/Body/PaymentBeneficiary/BeneficiaryAccountNum", invoiceSummData.getAttribute("ben_acct_num"));
		outputDoc.setAttribute("/Proponix/Body/PaymentBeneficiary/BeneficiaryAddressLn1", invoiceSummData.getAttribute("ben_add1"));
		outputDoc.setAttribute("/Proponix/Body/PaymentBeneficiary/BeneficiaryAddressLn2", invoiceSummData.getAttribute("ben_add2"));
		outputDoc.setAttribute("/Proponix/Body/PaymentBeneficiary/BeneficiaryAddressLn3", invoiceSummData.getAttribute("ben_add3"));
		outputDoc.setAttribute("/Proponix/Body/PaymentBeneficiary/BeneficiaryCountry", invoiceSummData.getAttribute("ben_country"));
		outputDoc.setAttribute("/Proponix/Body/PaymentBeneficiary/BeneficiaryEmailAddr", email);
		outputDoc.setAttribute("/Proponix/Body/PaymentBeneficiary/BeneficiaryBankName", invoiceSummData.getAttribute("ben_bank_name"));
		outputDoc.setAttribute("/Proponix/Body/PaymentBeneficiary/BeneficaryBankBranchCode", invoiceSummData.getAttribute("ben_branch_code"));
		outputDoc.setAttribute("/Proponix/Body/PaymentBeneficiary/BeneficiaryBankSortCode", invoiceSummData.getAttribute("ben_bank_sort_code"));
		outputDoc.setAttribute("/Proponix/Body/PaymentBeneficiary/BeneficiaryBankAddressLn1", invoiceSummData.getAttribute("ben_branch_add1"));
		outputDoc.setAttribute("/Proponix/Body/PaymentBeneficiary/BeneficiaryBankAddressLn2", invoiceSummData.getAttribute("ben_branch_add2"));
		outputDoc.setAttribute("/Proponix/Body/PaymentBeneficiary/BeneficiaryBankCity", invoiceSummData.getAttribute("ben_bank_city"));
		outputDoc.setAttribute("/Proponix/Body/PaymentBeneficiary/BeneficiaryBankProvince", invoiceSummData.getAttribute("ben_bank_province"));
		outputDoc.setAttribute("/Proponix/Body/PaymentBeneficiary/BeneficiaryBankCountry", invoiceSummData.getAttribute("ben_branch_country"));
		outputDoc.setAttribute("/Proponix/Body/PaymentBeneficiary/Charges", invoiceSummData.getAttribute("charges"));
		outputDoc.setAttribute("/Proponix/Body/PaymentBeneficiary/CentralBankReporting1", invoiceSummData.getAttribute("central_bank_rep1"));
		outputDoc.setAttribute("/Proponix/Body/PaymentBeneficiary/CentralBankReporting2", invoiceSummData.getAttribute("central_bank_rep2"));
		outputDoc.setAttribute("/Proponix/Body/PaymentBeneficiary/CentralBankReporting3", invoiceSummData.getAttribute("central_bank_rep3"));
		//CR1001 Rel9.4 START - package reporting codes if paymentMethod is either ACH or RTGS
		if(!isCreditNoteInd){
			outputDoc.setAttribute("/Proponix/Body/PaymentBeneficiary/ReportingCode1", invoiceSummData.getAttribute("reporting_code_1"));
			outputDoc.setAttribute("/Proponix/Body/PaymentBeneficiary/ReportingCode2", invoiceSummData.getAttribute("reporting_code_2"));
		}
		//CR1001 Rel9.4 END
		
	}
				//RKAZI CR709 Rel 8.2 01/25/2013 -End
	// IR# NUM042044530  Rel. 8.0.0 - Begin -
	// Nar CR-914A Rel9.2 01/08/2014 Begin
	/**
	 * This method is used to package credit note id and amount applied to
	 * Invoice. this will generate XML as mentioned below 
	 * <CreditNoteDetails>
	 * <InvoicePreAppliedByCreditNotes>
	 *   <InvoiceCreditNote> 
	 *       <CreditNoteId>AAA</CreditNoteId>
	 *       <AmountPreApplied>2,500.00</AmountPreApplied>
	 *   </ InvoiceCreditNote >
	 *   <InvoiceCreditNote> 
	 *       <CreditNoteId>BBB</ CreditNoteId >
	 *       <AmountPreApplied>2,5000.00</AmountPreApplied>
	 *   </ InvoiceCreditNote >
	 * </InvoicePreAppliedByCreditNotes> 
	 * <CreditNoteDetails>
	 * 
	 * @param invoiceSummData
	 * @param outputDoc
	 * @throws RemoteException
	 * @throws AmsException
	 * 
	 * 
	 */
	private void packageAppliedCreditNotesInInvocie(BusinessObject invoiceSummData, DocumentHandler outputDoc) 
	    throws RemoteException, AmsException{
		  BigDecimal totalCreditAmt = invoiceSummData.getAttributeDecimal("total_credit_note_amount");	
		  if(totalCreditAmt.compareTo(BigDecimal.ZERO) ==  -1){ // since total credit amount get save in DB with - sign
			String sql = "SELECT c.invoice_id, i.credit_note_applied_amount" +
                                  " FROM credit_notes c, invoices_credit_notes_relation i" +
                                  " WHERE c.upload_credit_note_oid = i.a_upload_credit_note_oid and i.a_upload_invoice_oid = ?"; 
		    DocumentHandler resultSet = DatabaseQueryBean.getXmlResultSet(sql, true, new String[]{invoiceSummData.getAttribute("upload_invoice_oid")});		    			    			    
		    if (resultSet != null) {
		    	Debug.debug("result set for applied credit in INV:" + resultSet.toString());
				List<DocumentHandler> list1 = resultSet.getFragmentsList("/ResultSetRecord");
				DocumentHandler creditInvoiceDetail = new DocumentHandler();
				DocumentHandler creditNoteDetails = new DocumentHandler();
				for (DocumentHandler doc : list1) {
						creditNoteDetails.setAttribute("/InvoiceCreditNote/CreditNoteId", doc.getAttribute("/INVOICE_ID"));
						BigDecimal preAppliedAmt = doc.getAttributeDecimal("/CREDIT_NOTE_APPLIED_AMOUNT");
						if ( preAppliedAmt.signum() == -1) {
							preAppliedAmt = preAppliedAmt.negate();
						}
						creditNoteDetails.setAttribute("/InvoiceCreditNote/AmountPreApplied", preAppliedAmt.toPlainString());	
						creditInvoiceDetail.addComponent("/CreditNoteDetails/InvoicePreAppliedByCreditNotes", creditNoteDetails);
				}
				outputDoc.setComponent("/Proponix/Body/CreditNoteDetails",creditInvoiceDetail.getFragment("/CreditNoteDetails"));
			}			    
		  }
		
	}
	
	/**
	 * This method is used to add invoice detial on whch credit note has been
	 * applied. will construct the XML as below.
	 * 
	 * <CreditNoteDetails> 
	 * <CreditNotePreApplicationStatus> PAP </CreditNotePreApplicationStatus >
	 *  <CreditNotePreAppliedToInvoices>
	 *    <CreditNoteInvoice> 
	 *      <InvoiceId>1</InvoiceId>
	 *      <AmountPreApplied>-100</AmountPreApplied>
	 *  </CreditNoteInvoice>
	 *  <CreditNoteInvoice> 
	 *     <InvoiceId>2</InvoiceId>
	 *     <AmountPreApplied>-200</AmountPreApplied> 
	 *  </CreditNoteInvoice>
	 * </CreditNotePreAppliedToInvoices> 
	 * <CreditNoteDetails>
	 * 
	 * @param creditNotePreApplicationStatus
	 * @param appliedInvDetailToCred
	 * @param outputDoc
	 */
	private void packageAppliedInvoiceInCreditNotes(String creditNotePreApplicationStatus, String appliedInvDetailToCred, DocumentHandler outputDoc) {
       
		DocumentHandler invCreditDetail = new DocumentHandler();
		DocumentHandler invoiceDetails = new DocumentHandler();
		String[] invItems = appliedInvDetailToCred.split(";");
		invCreditDetail.setAttribute("/CreditNoteDetails/CreditNotePreApplicationStatus", creditNotePreApplicationStatus);
		for(String invItem : invItems){
			String[] invDetail = invItem.split(",");
			invoiceDetails.setAttribute("/CreditNoteInvoice/InvoiceId", invDetail[0]);
			invoiceDetails.setAttribute("/CreditNoteInvoice/AmountPreApplied", invDetail[1]);	
			invCreditDetail.addComponent("/CreditNoteDetails/CreditNotePreAppliedToInvoices", invoiceDetails);
		}
		outputDoc.setComponent("/Proponix/Body/CreditNoteDetails", invCreditDetail.getFragment("/CreditNoteDetails"));
    }

	// Nar CR-914A Rel9.2 01/08/2014 End
	
	/**
	 *
	 * @param transactionOid
	 * @param outputDoc
	 * @throws RemoteException
	 * @throws AmsException
	 */
	private void packageAttachedDocuments(String transactionOid, DocumentHandler outputDoc) throws RemoteException, AmsException {

		//jgadela  R92 - SQL Injection FIX
		String sql = "select IMAGE_ID, NUM_PAGES, IMAGE_FORMAT, IMAGE_BYTES, DOC_NAME, HASH "
				+ "from DOCUMENT_IMAGE where FORM_TYPE = ? and P_TRANSACTION_OID  = ? ";

		DocumentHandler resultXML = DatabaseQueryBean.getXmlResultSet(sql, true, TradePortalConstants.DOC_IMG_FORM_TYPE_PORTAL_ATTACHED, transactionOid);
		if ((resultXML != null) && (resultXML.getFragments("/ResultSetRecord/").size() > 0)) {
			Vector resultVector = resultXML.getFragments("/ResultSetRecord/");



			outputDoc.setAttribute("/Proponix/Body/PDFDocuments/TotalNumberOfEntries", String.valueOf(resultVector.size()));
			String documentImagePath = "/Proponix/Body/PDFDocuments/PDFDocument";

			for (int i = 0; i < resultVector.size(); i++) {
				DocumentHandler myDoc = (DocumentHandler) resultVector.elementAt(i);
				outputDoc.setAttribute(documentImagePath + "(" + i + ")/FormType", TradePortalConstants.DOC_IMG_FORM_TYPE_PORTAL_ATTACHED.substring(0, 1).toUpperCase());
				outputDoc.setAttribute(documentImagePath + "(" + i + ")/ImageID", myDoc.getAttribute("/IMAGE_ID"));
				outputDoc.setAttribute(documentImagePath + "(" + i + ")/DocumentText", myDoc.getAttribute("/DOC_NAME"));
				outputDoc.setAttribute(documentImagePath + "(" + i + ")/NumPages", myDoc.getAttribute("/NUM_PAGES"));
				outputDoc.setAttribute(documentImagePath + "(" + i + ")/ImageFormat", myDoc.getAttribute("/IMAGE_FORMAT"));
				outputDoc.setAttribute(documentImagePath + "(" + i + ")/ImageBytes", myDoc.getAttribute("/IMAGE_BYTES"));
				outputDoc.setAttribute(documentImagePath + "(" + i + ")/DocName", myDoc.getAttribute("/DOC_NAME"));

				String imageHash = myDoc.getAttribute("/HASH");
				if ( imageHash != null && imageHash.length()>0 ) {
					outputDoc.setAttribute(documentImagePath +   "(" + i + ")/Hash",  imageHash );
				}
			}

			Debug.debug("[INVUploadPackagerMediatorBean.packageAttachedDocuments] After adding attached documents outputDoc = " + outputDoc.toString());
		}
	}

		// IR# NUM042044530  Rel. 8.0.0 - End -

	protected void removeEJBReference(EJBObject objRef) {
		try {
			if (objRef != null) {
				objRef.remove();
				objRef = null;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String getParm(String parmName, String processingParms) {

		int locationOfParmName, startOfParmValue, endOfParmValue;
		String parmValue;

		locationOfParmName = processingParms.indexOf(parmName);

		// Search for the whole word only.
		// For example, getParm("portfolio_activity_uoid=0|activity=1",
		// "activity" should return 1.
		// Keep searching if the character before or after the occurrence is not
		// a word delimiter

		while ((locationOfParmName > 0
				&& processingParms.charAt(locationOfParmName - 1) != ' ' && processingParms
				.charAt(locationOfParmName - 1) != '|')
				|| (locationOfParmName >= 0
						&& locationOfParmName < processingParms.length()
								- parmName.length()
						&& processingParms.charAt(locationOfParmName
								+ parmName.length()) != ' ' && processingParms
						.charAt(locationOfParmName + parmName.length()) != '=')) {

			locationOfParmName = processingParms.indexOf(parmName,
					locationOfParmName + parmName.length());

		}

		if (locationOfParmName == -1) {
			return "";
		}

		startOfParmValue = processingParms.indexOf("=", locationOfParmName);

		if (startOfParmValue == -1) {
			return "";
		}

		startOfParmValue++;

		endOfParmValue = processingParms.indexOf("|", startOfParmValue);

		if (endOfParmValue == -1) {
			endOfParmValue = processingParms.length();
		}

		parmValue = processingParms.substring(startOfParmValue, endOfParmValue);

		return parmValue;
	}

	/**
	 * Identifies and retrieves the Matching Rule for this invoice.
	 * First it looks for a rule with a matching buyer_name.  If no match is
	 * found for the buyer_name, it looks for a rule with a matching buyer_id.
	 * 
	 * @throws AmsException
	 * @throws RemoteException
	 */
	public ArMatchingRule getMatchingRule(BusinessObject obj, MediatorServices mediatorServices) throws AmsException, RemoteException {
		final String outerSelect = "SELECT DISTINCT ar_matching_rule_oid"
			+ " FROM ar_matching_rule WHERE ";
		final String innerSelect = " OR ar_matching_rule_oid IN"
			+ " (SELECT p_ar_matching_rule_oid FROM";

		String sqlBuilder = null;
		DocumentHandler resultSet = null;
		String tradePartnerOid = null;
        String corpOrgOid = obj.getAttribute("corp_org_oid");
		
        String buyerName = TradePortalConstants.INVOICE_TYPE_REC_MGMT.equals(obj.getAttribute("invoice_classification"))?obj.getAttribute("buyer_name"):obj.getAttribute("seller_name");//SHR CR708 Rel8.1.1 
		
		if (StringFunction.isNotBlank(buyerName)) {
			buyerName = buyerName.toUpperCase().trim();

			sqlBuilder = outerSelect + " p_corp_org_oid = ? and (buyer_name = unistr(?) " + innerSelect + " ar_buyer_name_alias WHERE buyer_name_alias = unistr(?)))"; // Nar -MKUM061235356

			resultSet = DatabaseQueryBean.getXmlResultSet(sqlBuilder, false, corpOrgOid, StringFunction.toUnistr(buyerName), StringFunction.toUnistr(buyerName));
			if (resultSet != null) {
				tradePartnerOid = resultSet.getAttribute("/ResultSetRecord/AR_MATCHING_RULE_OID");
			}
		}

		if (tradePartnerOid == null) {
			String buyerId = TradePortalConstants.INVOICE_TYPE_REC_MGMT.equals(obj.getAttribute("invoice_classification"))?obj.getAttribute("buyer_id"):obj.getAttribute("seller_id");//SHR CR708 Rel8.1.1
			if (StringFunction.isNotBlank(buyerId)) {
				buyerId = buyerId.toUpperCase().trim();

				sqlBuilder = outerSelect + " p_corp_org_oid = ? and (buyer_id = unistr(?) " + innerSelect + " ar_buyer_id_alias WHERE buyer_id_alias = unistr(?)))"; // Nar -MKUM061235356

				resultSet = DatabaseQueryBean.getXmlResultSet(sqlBuilder, false, corpOrgOid, StringFunction.toUnistr(buyerId), StringFunction.toUnistr(buyerId));
				if (resultSet != null) {
					tradePartnerOid = resultSet.getAttribute("/ResultSetRecord/AR_MATCHING_RULE_OID");
				}
			}
		}

		if (StringFunction.isNotBlank(tradePartnerOid)) {
			ArMatchingRule matchingRule = (ArMatchingRule) mediatorServices.createServerEJB("ArMatchingRule",
					Long.parseLong(tradePartnerOid));
			obj.setAttribute("tp_rule_name", matchingRule.getAttribute("buyer_name"));
			// In case buyer_name or buyer_id are blank or an alias value,
			// update with the actual values from the rule.
			if(TradePortalConstants.INVOICE_TYPE_REC_MGMT.equals(obj.getAttribute("invoice_classification")) && StringFunction.isBlank(obj.getAttribute("buyer_name"))){
				matchingRule.setAttribute("buyer_name", matchingRule.getAttribute("buyer_name"));
			}
			if(TradePortalConstants.INVOICE_TYPE_PAY_MGMT.equals(obj.getAttribute("invoice_classification")) && StringFunction.isBlank(obj.getAttribute("seller_name"))){
				matchingRule.setAttribute("seller_name", matchingRule.getAttribute("buyer_name"));
			}

			return matchingRule;
		}

		return null;
	}
	
	/**
	 * This method is used to add detail of credit note and also END_2_END_ID if
	 * present.i.e.
	 * <SubHeader>..........
	 *  < CreditNoteInd>123</CreditNoteInd> 
	 *  <CreditNoteUpdateInd>N/U</CreditNoteUpdateInd> 
	 *  <EndToEndID>ED1</EndToEndID>
	 *  <EndToEndTotalNo>001/100</EndToEndTotalNo>
	 *  <CreditNoteExpiryDate>2013-09-13</CreditNoteExpiryDate> 
	 * </SubHeader>
	 * 
	 * @param processParameters
	 * @param outputDoc
	 * @param invoiceSummData
	 * @param isCreditNoteInd
	 * @throws RemoteException
	 * @throws AmsException
	 */
	private void appendCreditNoteDetailInSubHeader(String processParameters,
			DocumentHandler outputDoc, BusinessObject invoiceSummData, boolean isCreditNoteInd) throws RemoteException, AmsException {
		String creditNoteInd = getParm("creditNoteInd", processParameters);
		if(isCreditNoteInd){
			outputDoc.setAttribute("../CreditNoteInd", creditNoteInd);
			outputDoc.setAttribute("../CreditNoteUpdateInd", getParm("creditNoteUpdateInd", processParameters));
		}
		String endToEndID = invoiceSummData.getAttribute("end_to_end_id");
		if(StringFunction.isNotBlank(endToEndID)){
			outputDoc.setAttribute("../EndToEndID", endToEndID);
			String whereClause = "end_to_end_id = ?";
			int invCount = DatabaseQueryBean.getCount("upload_invoice_oid", "invoices_summary_data", whereClause, 
					true, new Object[]{endToEndID});
			int credCount = DatabaseQueryBean.getCount("upload_credit_note_oid", "credit_notes", whereClause, 
					true, new Object[]{endToEndID});
			int endToEndTotalNo = invCount + credCount;
			
			String endToEndData = invoiceSummData.getAttribute("end_to_end_group_num") + "/" + endToEndTotalNo;
			outputDoc.setAttribute("../EndToEndTotalNo", endToEndData);
		}
		if( isCreditNoteInd && StringFunction.isNotBlank(invoiceSummData.getAttribute("expiry_date")) )
		{
			outputDoc.setAttribute("../CreditNoteExpiryDate", TPDateTimeUtility
					.packagingInvoiceDate(invoiceSummData.getAttribute("expiry_date")));
		}
		
	}
	
	/**
	 * This method is used to get applied invoice detail.
	 * @param credAppliedInvDetailOid
	 * @return
	 * @throws AmsException
	 */
	private String getCredAppliedInvDetail(String credAppliedInvDetailOid) throws AmsException {
		
		String credAppliedInvDetail = "";
		String sql = "SELECT invoice_detail FROM cred_applied_inv_detail WHERE cred_applied_inv_detail_oid = ? ";		
		try(Connection con = DatabaseQueryBean.connect(true);
				PreparedStatement pStmt = con.prepareStatement(sql)) {
		    
			pStmt.setLong(1, Long.parseLong(credAppliedInvDetailOid));
			try (ResultSet results = pStmt.executeQuery()){
				if ( results.next() ) {
					java.sql.NClob nclob = results.getNClob("invoice_detail");
					credAppliedInvDetail = nclob.getSubString(1, (int)nclob.length());
				}
			}catch (SQLException e) {
				e.printStackTrace();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} 	
    return credAppliedInvDetail;
   }
	
    /**
     * @param attributeValue
     * @return
     * @throws AmsException
     */
    private String replaceNonWin1252Chars(String attributeValue) throws AmsException {
    	CharsetEncoder encoder = Charset.forName("windows-1252").newEncoder();
        CharsetDecoder decoder = Charset.forName("windows-1252").newDecoder();
        // Default replacement is regular question mark.  Use upside-down question mark to be consistent 
        // with behavior in other non-unicode fields.
        byte[] replacement = {(byte)0xbf};
        encoder.onUnmappableCharacter(CodingErrorAction.REPLACE);
        encoder.replaceWith(replacement);
        
        try {
            if (attributeValue == null) return attributeValue;
            ByteBuffer win1252Value =  encoder.encode(CharBuffer.wrap(attributeValue.toCharArray()));
            String convertedValue = decoder.decode(win1252Value).toString();
            if (!attributeValue.equals(convertedValue)) {
                return convertedValue;
            }
            else return attributeValue;
        }
        catch (CharacterCodingException e) {
            e.printStackTrace();
        }        
        return attributeValue;
    }
	
}
