package com.ams.tradeportal.mediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.common.*;
import com.ams.tradeportal.busobj.*;
import com.ams.tradeportal.busobj.util.*;
import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import javax.ejb.*;
import java.rmi.*;
import java.util.Vector;

/*
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class WorkItemUnPackagerMediatorBean extends INSSTATUnPackagerMediatorBean {
private static final Logger LOG = LoggerFactory.getLogger(WorkItemUnPackagerMediatorBean.class);

/*
 * Manages WorkItem UnPackaging..
 */


protected Transaction getActiveTransaction(Instrument instrument) throws RemoteException, AmsException
 {
   			    String activeTransactionOid = instrument.getAttribute("original_transaction_oid");
			  	 if(activeTransactionOid==null) activeTransactionOid = "";
			     if(activeTransactionOid.equals(""))
			     {
				    instrument.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE ,TradePortalConstants.NO_MATCHING_VALUE_FOUND,"[WORKSTAT] - original_transaction_oid on instrument is either null or blank");
					return null;
			     }
				 else
				 {
                     instrument.setAttribute("active_transaction_oid", activeTransactionOid);
			  	     return (Transaction)instrument.getComponentHandle("TransactionList",Long.parseLong(activeTransactionOid));
			      }			  
 }
 
        /**
        * This method takes the transaction object
        * and populates the database with values from Position section
        * of the unpackagerDoc to the Instrument Object
        * @param instrument, Instrument object
        * @param transactionIndex, int value representing the transaction.
        * @param unpackagerDoc, DocumentHandler object with the XML
        * author iv
        */
   protected void unpackage(Transaction transaction, Instrument instrument, DocumentHandler unpackagerDoc)
                                                             throws RemoteException, AmsException

     {
      /* Unpackaging the Position details */
         String insStatPath = "/Proponix/Body";
         transaction.setAttribute("copy_of_amount", unpackagerDoc.getAttribute(insStatPath+"/Amount"));
		 transaction.setAttribute("instrument_amount",             unpackagerDoc.getAttribute(insStatPath+"/Amount"));
         instrument.setAttribute("copy_of_instrument_amount",      unpackagerDoc.getAttribute(insStatPath+"/Amount"));
         transaction.setAttribute("copy_of_currency_code",      unpackagerDoc.getAttribute(insStatPath+"/Currency"));		 
         transaction.setAttribute("transaction_status",   unpackagerDoc.getAttribute(insStatPath+"/TransactionStatus"));
         instrument.setAttribute("instrument_status",              unpackagerDoc.getAttribute(insStatPath+"/WorkItemStatus"));
		 
         transaction.newComponent("BankReleasedTerms");
		 //mediatorServices.debug("After creating the new transaction I am creating new BnkRelTerms with id = " + tpoid);
         Terms terms = (Terms)transaction.getComponentHandle("BankReleasedTerms");
		 terms.unpackageWorkItem(unpackagerDoc);		 
   }


}

