package com.ams.tradeportal.mediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Vector;

import javax.crypto.spec.SecretKeySpec;

import com.ams.tradeportal.busobj.CustomerErpGlCode;
import com.ams.tradeportal.busobj.OutgoingInterfaceQueue;
import com.ams.tradeportal.busobj.util.InstrumentServices;
import com.ams.tradeportal.busobj.util.SecurityAccess;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.frame.MediatorBean;
import com.amsinc.ecsg.frame.MediatorServices;
import com.amsinc.ecsg.util.DateTimeUtility;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.EncryptDecrypt;
import com.amsinc.ecsg.util.StringFunction;

/*
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class CustomerErpGlCodeMediatorBean extends MediatorBean {
private static final Logger LOG = LoggerFactory.getLogger(CustomerErpGlCodeMediatorBean.class);

	public static final String INSERT_MODE = "C";
	public static final String UPDATE_MODE = "U";
	public static final String DELETE_MODE = "D";

	public DocumentHandler execute(DocumentHandler inputDoc,
			DocumentHandler outputDoc, MediatorServices mediatorServices)
	throws RemoteException, AmsException {
		String updateButtonValue = null;
		String deleteButtonValue = null;
		String currencyCode = null;
		String userLocale = null;
		String updateMode = this.UPDATE_MODE;
		long theOid = 0;
		CustomerErpGlCode customerErpGlCode = null;
		Vector erpGlFragments = new Vector();
		DocumentHandler doc;
		String codeMsg ="";
		String codeDesc = "";
		String codeCategory = "";
		OutgoingInterfaceQueue oiQueue = null;
		String codeIndicator = "";
		String deactivateIndicator = "";
		String erpCode = "";
		String theOwnerOrgOid = "";
		Vector thePayCodes = null;
		Vector theDiscountCodes = null;
		Vector theOverPayCodes = null;
		Vector tradePayPartnerCodes = null;
		Vector tradeOverPayPartnerCodes = null;
		Vector tradeDiscountPartnerCodes = null;		
		DocumentHandler codesDoc = null;
		long pay = 0;
		long discount = 0;
		long overPay = 0;


		updateButtonValue = inputDoc.getAttribute("/Action/saveXPos");
		deleteButtonValue = inputDoc.getAttribute("/Action/deleteXPos");

		if (updateButtonValue != null) {
			updateMode = this.UPDATE_MODE;
		} else if (deleteButtonValue != null) {
			updateMode = this.DELETE_MODE;
		}

		String buttonPressed = inputDoc.getAttribute("/Update/ButtonPressed");
		mediatorServices.debug("The button pressed is " + buttonPressed);

		if (buttonPressed.equalsIgnoreCase(TradePortalConstants.BUTTON_SAVE))
			updateMode = this.UPDATE_MODE;
		else if (buttonPressed.equalsIgnoreCase(TradePortalConstants.BUTTON_DELETE))
			updateMode = this.DELETE_MODE;

		if (updateMode == null) {
			throw new AmsException("Unable to determine update mode.");
		}
		String loginUserRights = inputDoc.getAttribute("/LoginUser/security_rights");
		if ( !SecurityAccess.hasRights(loginUserRights, SecurityAccess.MAINTAIN_ERP_GL_CODES ) ) {
				throw new AmsException("User is trying to maintain: ERP GL Codes without proper access level");
		}
		
		try {
	        String secretKeyString = mediatorServices.getCSDB().getCSDBValue("SecretKey");
	        byte[] keyBytes = EncryptDecrypt.base64StringToBytes(secretKeyString);
	        javax.crypto.SecretKey secretKey = new SecretKeySpec(keyBytes, "DESede/CBC/PKCS5Padding");	
	        
	        theOwnerOrgOid = inputDoc.getAttribute("/CustomerErpGlCode/p_owner_oid");
	       //jgadela R91 IR T36000026319 - SQL INJECTION FIX
			String sqlStmntStr = "select erp_gl_code from customer_erp_gl_code where erp_gl_category = '"+TradePortalConstants.ERP_PAYMENT+
				"' and p_owner_oid in (?) ";			
	        //First, obtain all current codes - This will be used for checking duplicates
	        codesDoc = DatabaseQueryBean.getXmlResultSet(sqlStmntStr, false, new Object[]{theOwnerOrgOid});
	        
	        if (codesDoc != null){
	        	thePayCodes = getValues(codesDoc.getFragments("/ResultSetRecord/"), "ERP_GL_CODE");     
	        }	
	        
	       //jgadela R91 IR T36000026319 - SQL INJECTION FIX
	        sqlStmntStr = "select erp_gl_code from customer_erp_gl_code where erp_gl_category = '"+TradePortalConstants.ERP_DISCOUNT
					     +"' and p_owner_oid in (?) ";

	        codesDoc = DatabaseQueryBean.getXmlResultSet(sqlStmntStr, false, new Object[]{theOwnerOrgOid});
	        
	        if (codesDoc != null){
	        	theDiscountCodes = getValues(codesDoc.getFragments("/ResultSetRecord/"), "ERP_GL_CODE");       
	        }
	        
	        //jgadela R91 IR T36000026319 - SQL INJECTION FIX
	        sqlStmntStr = "select erp_gl_code from customer_erp_gl_code where erp_gl_category = '"+TradePortalConstants.ERP_OVERPAYMENT
	        			  +"' and p_owner_oid in (?) ";
			
	        codesDoc = DatabaseQueryBean.getXmlResultSet(sqlStmntStr, false, new Object[]{theOwnerOrgOid});
	        
	        if (codesDoc != null){
	        	theOverPayCodes = getValues(codesDoc.getFragments("/ResultSetRecord/"), "ERP_GL_CODE");       
	        }
	        
	      //jgadela R91 IR T36000026319 - SQL INJECTION FIX
	        sqlStmntStr = "select count(default_gl_code_ind) as CNT from customer_erp_gl_code where erp_gl_category = '"+TradePortalConstants.ERP_OVERPAYMENT
						  +"' and p_owner_oid in (?) "
			//Rel8.2 IR T36000017025 - add condition to check for default_gl_code = 'Y'
						  +" and default_gl_code_ind = '"+TradePortalConstants.INDICATOR_YES+"'";			
	        codesDoc = DatabaseQueryBean.getXmlResultSet(sqlStmntStr, false, new Object[]{theOwnerOrgOid});
	        
	        if (codesDoc != null){
	        	overPay = codesDoc.getAttributeLong("/ResultSetRecord/CNT");        
	        }	        
	        
	       //jgadela R91 IR T36000026319 - SQL INJECTION FIX
	        sqlStmntStr = "select count(default_gl_code_ind) as CNT  from customer_erp_gl_code  where erp_gl_category = '"+TradePortalConstants.ERP_PAYMENT
					+"' and p_owner_oid in (?) "
			//Rel8.2 IR T36000017025 - add condition to check for default_gl_code = 'Y'
		    		+" and default_gl_code_ind = '"+TradePortalConstants.INDICATOR_YES+"'";
	        codesDoc = DatabaseQueryBean.getXmlResultSet(sqlStmntStr, false, new Object[]{theOwnerOrgOid});
	        
	        if (codesDoc != null){
	        	pay = codesDoc.getAttributeLong("/ResultSetRecord/CNT");        
	        }
	        
	        //jgadela R91 IR T36000026319 - SQL INJECTION FIX
	        sqlStmntStr = "select count(default_gl_code_ind) as CNT from customer_erp_gl_code where erp_gl_category = '"+TradePortalConstants.ERP_DISCOUNT
						  +"' and p_owner_oid in (?) "
			//Rel8.2 IR T36000017025 - add condition to check for default_gl_code = 'Y'
		    			  +" and default_gl_code_ind = '"+TradePortalConstants.INDICATOR_YES+"'";			
	        codesDoc = DatabaseQueryBean.getXmlResultSet(sqlStmntStr, false, new Object[]{theOwnerOrgOid});
	        
	        if (codesDoc != null){
	        	discount = codesDoc.getAttributeLong("/ResultSetRecord/CNT");        
	        }
	        
	      //jgadela R91 IR T36000026319 - SQL INJECTION FIX
	        sqlStmntStr = "select payment_gl_code, over_payment_gl_code, discount_gl_code from ar_matching_rule";
	        //Trade partner
			//sqlStmntStr.append("select default_payment_gl_code, default_overpayment_gl_code, default_discount_gl_code");
			//sqlStmntStr.append(" from trading_partner_rule"); 	
			
	        codesDoc = DatabaseQueryBean.getXmlResultSet(sqlStmntStr, false, new ArrayList<Object>());
	        
	        if (codesDoc != null){
	        	tradePayPartnerCodes = getValues(codesDoc.getFragments("/ResultSetRecord/"), "PAYMENT_GL_CODE");        
	        }
	        if (codesDoc != null){
	        	tradeOverPayPartnerCodes = getValues(codesDoc.getFragments("/ResultSetRecord/"), "OVER_PAYMENT_GL_CODE");        
	        }
	        if (codesDoc != null){
	        	tradeDiscountPartnerCodes = getValues(codesDoc.getFragments("/ResultSetRecord/"), "DISCOUNT_GL_CODE");        
	        }	        
	        
			//
			// Get all fragments from the inputdoc corresponding to each row
			// add them to the bean and save row at a time
			//
			erpGlFragments = inputDoc.getFragments("/CustomerErpGlCode/");
			int available = erpGlFragments.size();
			int blank = 0;
			if (erpGlFragments != null) {
				for (int i = 0; i < erpGlFragments.size(); i++) {
					doc = (DocumentHandler) erpGlFragments.elementAt(i);
					
					//SSikhakolli - Rel 9.0 IR# T36000029332 06/18/2014 - Begin
					if (StringFunction.isNotBlank(doc.getAttribute("/customer_erp_gl_code_oid"))){
						theOid = Long.parseLong(doc.getAttribute("/customer_erp_gl_code_oid"));
					}
					//SSikhakolli - Rel 9.0 IR# T36000029332 06/18/2014 - End
					
					codeIndicator = doc.getAttribute("/default_gl_code_ind");
					deactivateIndicator = doc.getAttribute("/deactivate_ind");
					erpCode = doc.getAttribute("/erp_gl_code");
					theOwnerOrgOid = doc.getAttribute("/p_owner_oid");
					codeDesc = doc.getAttribute("/erp_gl_description");
					codeCategory = doc.getAttribute("/erp_gl_category");
					
					if (theOid == 0) {
						updateMode = this.INSERT_MODE;
					}
					
					
					
					// Get a handle to an instance of the object
					customerErpGlCode = (CustomerErpGlCode) mediatorServices
					.createServerEJB("CustomerErpGlCode");

					//
					if (updateMode.equalsIgnoreCase(this.INSERT_MODE)) {
						mediatorServices
						.debug("Mediator: Creating new ERP GL Code");

						theOid = customerErpGlCode.newObject();
						
						//outputDoc.setAttribute( "oid", EncryptDecrypt.encryptStringUsingTripleDes(""+theOid, secretKey) );						
												
						
					} else {
						mediatorServices.debug("Mediator: Retrieving ERP GL Code "
								+ theOid);

						customerErpGlCode.getData(theOid);
					}

					if (updateMode.equalsIgnoreCase(this.DELETE_MODE)) {
						mediatorServices
						.debug("Mediator: Deleting erpGl object");

						// Delete the ERP
						customerErpGlCode.delete();
						codeMsg = erpCode;
						mediatorServices
						.debug("Mediator: Completed the delete of the ERP GL Code object");
					} else {
						//Validations:
						//If the whole row is empty, skip it. Only validate when at least one item is entered
						//if(StringFunction.isBlank(erpCode) && StringFunction.isBlank(codeDesc) && StringFunction.isBlank(codeCategory) && StringFunction.isBlank(codeIndicator) && StringFunction.isBlank(deactivateIndicator)){
						if(StringFunction.isBlank(erpCode) && StringFunction.isBlank(codeDesc) && StringFunction.isBlank(codeCategory) ){
							//Skip empty row
							blank++;
							if (blank == available){
								mediatorServices.getErrorManager().issueError(
										TradePortalConstants.ERR_CAT_1,
										TradePortalConstants.ERP_CODES_REQUIRED);	
								return outputDoc;	
							}
							continue;
						}
						else if(StringFunction.isBlank(erpCode) || StringFunction.isBlank(codeDesc) || StringFunction.isBlank(codeCategory)){						
							mediatorServices.getErrorManager().issueError(
									TradePortalConstants.ERR_CAT_1,
									TradePortalConstants.ERP_CODES_REQUIRED);	
							//return outputDoc; /* KMehta commented since existing values are lost on error */ 						
					    }
						//Check lengths
						if(erpCode.length()>17 || codeDesc.length()>30){
							//
							mediatorServices.getErrorManager().issueError(
									TradePortalConstants.ERR_CAT_1,
									TradePortalConstants.ERP_GL_CODE_DESCRIPTION_LEN);	
							return outputDoc;						
						}

						if (updateMode.equalsIgnoreCase(this.INSERT_MODE)) {
							if(TradePortalConstants.ERP_PAYMENT.equalsIgnoreCase(codeCategory)){
								if(thePayCodes != null)
								if(thePayCodes.contains(erpCode.toUpperCase())){
									mediatorServices.getErrorManager().issueError(
											TradePortalConstants.ERR_CAT_1,
											TradePortalConstants.ERP_CODES_EXISTS);	
									return outputDoc;						
								}
								if(StringFunction.isNotBlank(codeIndicator) && pay > 0){
									mediatorServices.getErrorManager().issueError(
											TradePortalConstants.ERR_CAT_1,
											TradePortalConstants.ERP_GL_CODE_FOR_PAYMENT_ONLY_ONE);	
									return outputDoc;							
								}						
							}
							if(TradePortalConstants.ERP_OVERPAYMENT.equalsIgnoreCase(codeCategory)){
								if(theOverPayCodes != null)
								if(theOverPayCodes.contains(erpCode.toUpperCase())){
									mediatorServices.getErrorManager().issueError(
											TradePortalConstants.ERR_CAT_1,
											TradePortalConstants.ERP_CODES_EXISTS);	
									return outputDoc;						
								}
								if(StringFunction.isNotBlank(codeIndicator) && overPay > 0){
									mediatorServices.getErrorManager().issueError(
											TradePortalConstants.ERR_CAT_1,
											TradePortalConstants.ERP_GL_CODE_FOR_OVERPAYMENT_ONLY_ONE);	
									return outputDoc;							
								}
							}
							
							if(TradePortalConstants.ERP_DISCOUNT.equalsIgnoreCase(codeCategory)){
								if(theDiscountCodes != null)
								if(theDiscountCodes.contains(erpCode.toUpperCase())){
									mediatorServices.getErrorManager().issueError(
											TradePortalConstants.ERR_CAT_1,
											TradePortalConstants.ERP_CODES_EXISTS);	
									return outputDoc;						
								}
								if(StringFunction.isNotBlank(codeIndicator) && discount > 0){
									mediatorServices.getErrorManager().issueError(
											TradePortalConstants.ERR_CAT_1,
											TradePortalConstants.ERP_GL_CODE_FOR_DISCOUNT_ONLY_ONE);	
									return outputDoc;							
								}						
							}
								
							customerErpGlCode.setAttribute("customer_erp_gl_code_oid",""+theOid);							
							outputDoc.setAttribute( "oid", EncryptDecrypt.encryptStringUsingTripleDes(""+theOid, secretKey) );

						} else {
							outputDoc.setAttribute( "oid", EncryptDecrypt.encryptStringUsingTripleDes(
                                    doc.getAttribute("/customer_erp_gl_code_oid"), secretKey) );	

							//Get the current value if not insert mode
							//If it is checked, don't check, otherwise check
							
								if(TradePortalConstants.ERP_PAYMENT.equalsIgnoreCase(codeCategory)){
									if((!(erpCode.equalsIgnoreCase(customerErpGlCode.getAttribute("erp_gl_code"))) || !(codeCategory.equalsIgnoreCase(customerErpGlCode.getAttribute("erp_gl_category")))) && (thePayCodes != null && thePayCodes.contains(erpCode.toUpperCase()))){
										mediatorServices.getErrorManager().issueError(
												TradePortalConstants.ERR_CAT_1,
												TradePortalConstants.ERP_CODES_EXISTS);	
										return outputDoc;						
									}
									//So, if right now it is NO and you are changing it to YES and there is already more than one in that category
									if(TradePortalConstants.INDICATOR_NO.equalsIgnoreCase(customerErpGlCode.getAttribute("default_gl_code_ind")))
									if(StringFunction.isNotBlank(codeIndicator) && pay > 0){
										if(TradePortalConstants.INDICATOR_YES.equalsIgnoreCase(codeIndicator))
										mediatorServices.getErrorManager().issueError(
												TradePortalConstants.ERR_CAT_1,
												TradePortalConstants.ERP_GL_CODE_FOR_PAYMENT_ONLY_ONE);	
										return outputDoc;							
									}
									//Rel8.2 IR T36000015463 
									//If you deactivate a code that is already used in the trade partner rules, issue an error and return
									//if(!TradePortalConstants.INDICATOR_YES.equalsIgnoreCase(customerErpGlCode.getAttribute("default_gl_code_ind")))
									if(StringFunction.isNotBlank(deactivateIndicator) && TradePortalConstants.INDICATOR_YES.equalsIgnoreCase(deactivateIndicator))
									if(tradePayPartnerCodes != null)
									if(tradePayPartnerCodes.contains(erpCode.toUpperCase())){
										mediatorServices.getErrorManager().issueError(
												TradePortalConstants.ERR_CAT_1,
												TradePortalConstants.ERP_GL_USED_ON_TRADE_PARTNER_W,erpCode);	
										//return outputDoc;						
									}										
								}
								if(TradePortalConstants.ERP_OVERPAYMENT.equalsIgnoreCase(codeCategory)){
									if((!(erpCode.equalsIgnoreCase(customerErpGlCode.getAttribute("erp_gl_code"))) || !(codeCategory.equalsIgnoreCase(customerErpGlCode.getAttribute("erp_gl_category")))) &&  ( theOverPayCodes != null && theOverPayCodes.contains(erpCode.toUpperCase()))){
										mediatorServices.getErrorManager().issueError(
												TradePortalConstants.ERR_CAT_1,
												TradePortalConstants.ERP_CODES_EXISTS);	
										return outputDoc;						
									}
									if(TradePortalConstants.INDICATOR_NO.equalsIgnoreCase(customerErpGlCode.getAttribute("default_gl_code_ind")))
									if(StringFunction.isNotBlank(codeIndicator) && overPay > 0){
										if(TradePortalConstants.INDICATOR_YES.equalsIgnoreCase(codeIndicator))
										mediatorServices.getErrorManager().issueError(
												TradePortalConstants.ERR_CAT_1,
												TradePortalConstants.ERP_GL_CODE_FOR_OVERPAYMENT_ONLY_ONE);	
										return outputDoc;							
									}
									//Rel8.2 IR T36000015463
									//If you deactivate a code that is already used in the trade partner rules, issue an error and return
									//if(!TradePortalConstants.INDICATOR_YES.equalsIgnoreCase(customerErpGlCode.getAttribute("default_gl_code_ind")))
									if(StringFunction.isNotBlank(deactivateIndicator) && TradePortalConstants.INDICATOR_YES.equalsIgnoreCase(deactivateIndicator))
									if(tradeOverPayPartnerCodes != null)
									if(tradeOverPayPartnerCodes.contains(erpCode.toUpperCase())){
										mediatorServices.getErrorManager().issueError(
												TradePortalConstants.ERR_CAT_1,
												TradePortalConstants.ERP_GL_USED_ON_TRADE_PARTNER_W,erpCode);	
										//return outputDoc;						
									}									
								}
								
								if(TradePortalConstants.ERP_DISCOUNT.equalsIgnoreCase(codeCategory)){
									if((!(erpCode.equalsIgnoreCase(customerErpGlCode.getAttribute("erp_gl_code"))) || !(codeCategory.equalsIgnoreCase(customerErpGlCode.getAttribute("erp_gl_category")))) &&  (theDiscountCodes != null && theDiscountCodes.contains(erpCode.toUpperCase()))){
										mediatorServices.getErrorManager().issueError(
												TradePortalConstants.ERR_CAT_1,
												TradePortalConstants.ERP_CODES_EXISTS);	
										return outputDoc;						
									}
									
									if(TradePortalConstants.INDICATOR_NO.equalsIgnoreCase(customerErpGlCode.getAttribute("default_gl_code_ind")))
									if(StringFunction.isNotBlank(codeIndicator) && discount > 0){
										if(TradePortalConstants.INDICATOR_YES.equalsIgnoreCase(codeIndicator))
										mediatorServices.getErrorManager().issueError(
												TradePortalConstants.ERR_CAT_1,
												TradePortalConstants.ERP_GL_CODE_FOR_DISCOUNT_ONLY_ONE);	
										return outputDoc;							
									}
									//Rel8.2 IR T36000015463
									//If you deactivate a code that is already used in the trade partner rules, issue an error and return
									//if(!TradePortalConstants.INDICATOR_YES.equalsIgnoreCase(customerErpGlCode.getAttribute("default_gl_code_ind")))
									if(StringFunction.isNotBlank(deactivateIndicator) && TradePortalConstants.INDICATOR_YES.equalsIgnoreCase(deactivateIndicator))
									if(tradeDiscountPartnerCodes != null)
									if(tradeDiscountPartnerCodes.contains(erpCode.toUpperCase())){
										mediatorServices.getErrorManager().issueError(
												TradePortalConstants.ERR_CAT_1,
												TradePortalConstants.ERP_GL_USED_ON_TRADE_PARTNER_W,erpCode);
										//return outputDoc;						
									}									
							  }								
							
							
						}

						if(StringFunction.isBlank(deactivateIndicator)) deactivateIndicator = TradePortalConstants.INDICATOR_NO;
						if(StringFunction.isBlank(codeIndicator)) codeIndicator = TradePortalConstants.INDICATOR_NO;						
						
						//CR-741 Rel8.2 IR T36000015498
						//Check for unicode chars
			            if (!StringFunction.canEncodeIn_WIN1252(erpCode) || !StringFunction.canEncodeIn_WIN1252(codeDesc) ) {
			            	mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
			                        TradePortalConstants.UNICODE_NOT_ALLOWED,
			                        mediatorServices.getResourceManager().getText("ERPGLCodes.DiscountCodes",
			                                TradePortalConstants.TEXT_BUNDLE));
			                return outputDoc;
			            }						
						
						customerErpGlCode.setAttribute("erp_gl_code",
								erpCode);
						customerErpGlCode.setAttribute("erp_gl_description",
								codeDesc);
						customerErpGlCode.setAttribute("erp_gl_category",
								codeCategory);
						customerErpGlCode.setAttribute("default_gl_code_ind",
								codeIndicator);
						customerErpGlCode.setAttribute("p_owner_oid",
								theOwnerOrgOid);
						customerErpGlCode.setAttribute("deactivate_ind",
								deactivateIndicator);
						if(i==0){
							codeMsg = erpCode;
						}else{
							if(StringFunction.isNotBlank(erpCode)){								
								codeMsg = codeMsg + "," + erpCode;
							}
						}

						// AiA- end
						if(StringFunction.isNotBlank(erpCode)){
							customerErpGlCode.save();
 						}			

					}
				}
				//Now set on Queue
				//


				oiQueue = (OutgoingInterfaceQueue) mediatorServices.createServerEJB("OutgoingInterfaceQueue");
				oiQueue.newObject();
				
				
				oiQueue.setAttribute("date_created",
						DateTimeUtility.getGMTDateTime());
				oiQueue.setAttribute("status",
						TradePortalConstants.OUTGOING_STATUS_STARTED);
				oiQueue.setAttribute("message_id",
						InstrumentServices.getNewMessageID());
				StringBuilder processParameters = new StringBuilder("REFTABLETYPE=ERPGLCODES|REFTABLEOWNEROID=");
				
				oiQueue.setAttribute("process_parameters",
						processParameters.toString()+ theOwnerOrgOid);
				
				oiQueue.setAttribute("msg_type",
						TradePortalConstants.CUSREF);
				
				if (oiQueue.save() != 1) {
					throw new AmsException(
							"Error occurred while inserting ERP GL Code type in Outgoing Queue");
				}					
				//
				//end Queue setting	
			}
		} catch (Exception e) {
			String serverErrorNumber = "SE" + System.currentTimeMillis();
			LOG.info("General exception caught in CustomerErpGlMediator. "
					+ "Server error#" + serverErrorNumber);
			e.printStackTrace();
			// we don't know exactly what this is, so just issue a generic error
			// another option is to just propogate the exception
			mediatorServices.getErrorManager().issueError(getClass().getName(),
					TradePortalConstants.SERVER_ERROR, serverErrorNumber);
		} finally {

			mediatorServices.debug("Mediator: Result of ERP GL update is "
					+ this.isTransSuccess(mediatorServices.getErrorManager()));

			if (this.isTransSuccess(mediatorServices.getErrorManager())) {

				// Save the update record to the ref data audit log
				mediatorServices.debug("Mediator: Creating audit record");

				mediatorServices.debug("Mediator: Finished with audit record");

				// Finally issue a success message based on update type.
				if (updateMode.equalsIgnoreCase(this.DELETE_MODE)) {
					mediatorServices.getErrorManager().issueError(
							TradePortalConstants.ERR_CAT_1,
							TradePortalConstants.DELETE_SUCCESSFUL, codeMsg);
				} else if (updateMode.equalsIgnoreCase(this.INSERT_MODE)) {
 					mediatorServices.getErrorManager().issueError(
							TradePortalConstants.ERR_CAT_1,
							TradePortalConstants.INSERT_SUCCESSFUL, codeMsg);
				} else {
					mediatorServices.getErrorManager().issueError(
							TradePortalConstants.ERR_CAT_1,
							TradePortalConstants.UPDATE_SUCCESSFUL, codeMsg);
				}
			}

		}

		return outputDoc;
	}
	
	private Vector getValues(Vector frags, String xpath){
		Vector<String> values = new Vector<String>();
        DocumentHandler resDoc = null;
        for(int j=0; j<frags.size(); j++){
        	resDoc = (DocumentHandler)frags.elementAt(j);
        	values.addElement(resDoc.getAttribute(xpath).toUpperCase());
        }		
		return values;
	}


}