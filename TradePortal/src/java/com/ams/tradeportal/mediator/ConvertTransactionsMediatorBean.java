package com.ams.tradeportal.mediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.busobj.*;
import com.ams.tradeportal.common.*;
import com.amsinc.ecsg.util.*;
import com.amsinc.ecsg.frame.*;

import java.math.BigDecimal;
import java.rmi.*;
import java.util.*;
import java.security.cert.X509Certificate;
import com.ams.tradeportal.common.cache.Cache;
import com.ams.tradeportal.common.cache.TPCacheManager;
import com.ams.tradeportal.busobj.util.InstrumentServices;
import com.ams.tradeportal.busobj.util.CertificateUtility;
import com.ams.tradeportal.busobj.util.InstrumentAuthentication;

/*
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class ConvertTransactionsMediatorBean extends MediatorBean
{
private static final Logger LOG = LoggerFactory.getLogger(ConvertTransactionsMediatorBean.class);
	//IR-PAUI060561797 Krishna Begin
	protected Hashtable convertedTransactions = null;
    //IR-PAUI060561797 Krishna End
   public DocumentHandler execute(DocumentHandler inputDoc, DocumentHandler outputDoc, MediatorServices mediatorServices)
                                  throws RemoteException, AmsException
   {
  /**
   * This method performs deletions of Transactions
   * The format expected from the input doc is:
   *
   * <TransactionList>
   *	<Transaction ID="0"><transactionData>1000001/1200001/AMD</transactionData></Transaction>
   *	<Transaction ID="1"><transactionData>1000002/1200002/ISS</transactionData></Transaction>
   *	<Transaction ID="2"><transactionData>1000003/1200003/TRN</transactionData></Transaction>
   *	<Transaction ID="3"><transactionData>1000004/1200004/ASG</transactionData></Transaction>
   *	<Transaction ID="6"><transactionData>1000001/1200007/ISS</transactionData></Transaction>
   * </TransactionList>
   * <User>
   *    <userOid>1001</userOid>
   *    <securityRights>302231454903657293676543</securityRights>
   * </User>
   * <Authorization>
   *    <preAuthorize>Y|N</preAuthorize>
   * </Authorization>
   *
   * The oid value consists of transaction oid, a '/', instrument oid, a '/'
   * and the transaction type
   *
   * This is a non-transactional mediator that executes transactional
   * methods in business objects.  It accepts preAuthorize as a parameter which
   * determines if the transaction should be preAuthorized (ie. authorized at the
   * background) or if it should be authorized immediately.
   * For preauthorization, it processes each transaction by
   * locking the instrument (if necessary) and setting the transaction
   * status to Authorised Pending.
   * If there is more than one transaction selected for an instrument,
   * this mediator locks the instrument for the first transaction and gives
   * a lock error for any subsequent transactions.
   * For authorization, it locks the instrument, performs the authorize process
   * and sets the transaction status to Failed, Authorized or Partially Authorized.
   * This method accepts multiple transactions in the format specified above.
   * Note that this mediator has been tested for both preAuthorize and
   * Convert. However, for preAuthorize, the effect of instrument locking
   * on a general level has to be examined further.
   *
   * @param inputDoc The input XML document (&lt;In&gt; section of overall
   * Mediator XML document)
   * @param outputDoc The output XML document (&lt;Out&gt; section of overall
   * Mediator XML document)
   * @param mediatorServices Associated class that contains lots of the
   * auxillary functionality for a mediator.

   */

 	// get each Transaction Oid and process each transaction
	// each transaction is unique within itself

	// populate variables with inputDocument information such as transaction list,
	// user oid and rights, etc

	LOG.debug("PreAuthoriseTransMediator InputDoc: " + inputDoc);
	DocumentHandler transaction = null;
	Vector transactionList = null;
	int count = 0;
	int sep1 = 0;
	int sep2 = 0;
	String userOid = null;
	String loginUserOid = null;
	//VS RVU062382912 Checks if user is at subsidiary access mode or pending transaction leve 
	String convertAtParentViewOrSubs = null;
	String rights = null;
	String userLocale = null;
	Hashtable instrumentHash = null;
	Instrument instrument = null;
	Transaction transObj = null;
	boolean preAuthorize = false;
	boolean preAuthorizeSuccess = false;
	boolean authorizeFailed = false;
	boolean previouslyLocked = false;	// if the instrument was previously
						// locked, don't unlock if there is
						// an error
	
	//AAlubala
	//CR711 Rel8.0 - 01/14/2012
	//Send the proxy User oid to the instrument authorize method
	//so that their panel authority code can be used instead of that of the
	//currently logged in user. 
	//
	//String proxyUser = "";
	//Ravindra - 19th Nov 2012- Rel8100 IR-DEUM070438986 - Start
	//proxyUser = inputDoc.getAttribute("/proxy");
	//proxyUser = outputDoc.getAttribute("/proxy");
	//Ravindra - 19th Nov 2012- Rel8100 IR-DEUM070438986 - End

	LOG.debug("AuthorizeTransactionsMediator: inputDoc: " + inputDoc);

	transactionList = inputDoc.getFragments("/TransactionList/Transaction");
	count = transactionList.size();
	userOid = inputDoc.getAttribute("/User/userOid");
	loginUserOid = inputDoc.getAttribute("/User/userOid");// T36000018805 - Rel 8.2 Add - Store login User Id to use while object locking during offline authorization.
	
	rights = inputDoc.getAttribute("/User/securityRights");
	String proxyUserOid = "";
        // W Zhu 15 May 2013 T36000011771 comment out. Replaces userOid with proxy user's oid.  Use the proxy user's security right and log the authorization with his/her ID.
	proxyUserOid = inputDoc.getAttribute("/proxy");
	proxyUserOid = (StringFunction.isNotBlank(proxyUserOid) && !proxyUserOid.equalsIgnoreCase("null")) ? proxyUserOid : inputDoc.getAttribute("/User/proxyUserOid");
        if (StringFunction.isNotBlank(proxyUserOid) && !proxyUserOid.equalsIgnoreCase("null")) {
            userOid = proxyUserOid;
	    User proxyUser = (User) mediatorServices.createServerEJB("User",Long.parseLong(proxyUserOid));
	    String securityProfileOid = proxyUser.getAttribute("security_profile_oid");
            if(StringFunction.isNotBlank(securityProfileOid))
            {
            	String sqlStatement = "select security_rights from security_profile where security_profile_oid = ?";
            	DocumentHandler resultXML = DatabaseQueryBean.getXmlResultSet(sqlStatement, false, new Object[]{securityProfileOid});
            	String userRights = resultXML.getAttribute("/ResultSetRecord/SECURITY_RIGHTS");
            	//For this Instrument, use the offline user's security rights
            	rights = userRights;
            }
        }
        
	//VS RVU062382912 Setting parameter value and setting default value
	convertAtParentViewOrSubs = inputDoc.getAttribute("/Authorization/authorizeAtParentViewOrSubs");
	if (InstrumentServices.isBlank(convertAtParentViewOrSubs)){
		convertAtParentViewOrSubs = TradePortalConstants.INDICATOR_NO;
	}
        
	preAuthorize =
	  ((inputDoc.getAttribute("/Authorization/preAuthorize"))).equals(
	  TradePortalConstants.INDICATOR_YES)?true:false;

	// get the locale from client server data bridge, used for getting locale
	// specific information when passing error parameters
	userLocale = mediatorServices.getCSDB().getLocaleName();
    if (TradePortalConstants.BUTTON_CONFIRM_FX.equals(inputDoc.getAttribute("/Update/ButtonPressed"))) { 
    	mediatorServices.getCSDB().setCSDBValue(TradePortalConstants.TRAN_ACTION, TradePortalConstants.TRAN_ACTION_CONFIRM_FXDEAL);
    }else {
    	mediatorServices.getCSDB().setCSDBValue(TradePortalConstants.TRAN_ACTION, TradePortalConstants.TRAN_ACTION_AUTHORIZE);     	// NSX CR-574 09/21/10
    }

	mediatorServices.getCSDB().setCSDBValue("user_oid", userOid);     	// NSX CR-574 09/21/10
	//AAlubala 23th Nov 2012- Rel8100 IR-DEUM070438986 - Start
	//also set proxy useroid if available
        // W Zhu 15 May 2013 T36000011771 comment out. AuthorizeTransactionMediator replaces userOid with proxy user's oid.  Should take care of this.
	//
	//if(InstrumentServices.isNotBlank(proxyUser))
	//		mediatorServices.getCSDB().setCSDBValue("proxyUserOid", proxyUser); 
	//
	//end
	LOG.debug("AuthorizeTransactionsMediator: number of transactions: " + count);

	if (count <= 0)
	{
		String errorAction = "TransactionAction.Convert";
		//IAZ CR-472 05/15/09 Begin: "Authenticate" status is not used anymore as Authenticate becomes part of Convert
		/*if (InstrumentServices.isNotBlank(reCertOK))
		{
			errorAction = "TransactionAction.Authenticate";
		}
		*/
		//IAZ CR-472 05/15/09 End
	    mediatorServices.getErrorManager().issueError (getClass().getName(),
		TradePortalConstants.NO_ITEM_SELECTED, mediatorServices.getResourceManager().
		//getText("TransactionAction.Convert",
		getText(errorAction,
	    	TradePortalConstants.TEXT_BUNDLE));
	    return new DocumentHandler();
	}

    // if authentication with certificate is required for authorize and reCertification failed, issue an error and stop.
    // 		handle case of multiple reqs (from pending list view) and single reqt (transaction page)

    String reCertification = inputDoc.getAttribute("/Authorization/reCertification");
	LOG.debug("AuthorizeTransactionMediator:: Recert Reqt: " + reCertification);

	//ir cnuk113043991 - if an authentication failure on list, continue processing
	// so we get into the status change logic.  the authenticated boolean
	// is set so we DO NOT attempt to authorize
	boolean authenticated = true; //default authenticated (backwards?)
    if (TradePortalConstants.INDICATOR_MULTIPLE.equals(reCertification)
            && !CertificateUtility.validateCertificate(inputDoc, mediatorServices)) 
    {
        authenticated = false;
    }        


	// if preAuthorize is selected, only one transaction from an instrument
	// can be processed and locked at one time.  One reason is that
	// the instrument is locked for a transaction processing and is unlocked
	// after the processing is complete
	// instrumentHash is used to store Instrument Oid(key) and
	// Instrument ID (value) of transactions that have been successly
	// preAuthorized and to ensure no additional transactions in that instrument
	// is processed

	instrumentHash = new Hashtable();
    //IR-PAUI060561797 Krishna Begin
    //Initializing Hashtable to store Authorized Transactions
	convertedTransactions = new Hashtable();
    //IR-PAUI060561797 Krishna End
	// a transaction should be locked and processed at the instrument level
	// since the transaction is a component of instrument
	// for each transaction, check to make sure that the instrument has not
	// been previously locked in this list of transactions.
	// Then attempt to lock the instrument and call changeTransactionStatus
	// on the instrument.  preAuthoriseTransaction is a transactional method.
	for (int i = 0; i < count; i++)
	{
	    transaction = (DocumentHandler) transactionList.get(i);
	    String oid = transaction.getAttribute("/transactionData");

	    // previouslyLocked keeps track of an instrument that was not locked
	    // by this mediator and is used to keep the instrument locked if it was
	    // previously locked

	    previouslyLocked = false;
	    preAuthorizeSuccess = false;
	    authorizeFailed = false;

	    // Split up the instrument oid, transaction oid and transaction type
	    sep1 = oid.indexOf('/');
	    sep2 = oid.indexOf('/', sep1 + 1);

	    String transactionOid = oid.substring(0, sep1);
	    String instrumentOid = oid.substring(sep1 + 1, sep2);
	    String transactionType = oid.substring(sep2 + 1);

	    LOG.debug("PreAuthTransactionMediator: oid contents: " + oid
		+ "Instrument oid: " + instrumentOid + " Transaction oid"
		+ transactionOid + " Transaction Type " + transactionType);

	    //ir cnuk113043991 - determine if the transaction requires authentication
	    boolean requireAuthentication = 
	        requireTransactionAuthentication(instrumentOid, transactionOid, 
	            transactionType, mediatorServices);

        //ir ctuk113042306 - if authentication failed and on a list page
	    // and the transaction requires authentication,
	    // first issue an additional error to indicate which transactions
	    // failed authentication.  other errors that follow (such as 
	    // instrument in use) are added to this.  processing continues
	    // in order to attempt a status change if necessary later on
	    if ( !authenticated &&
             TradePortalConstants.INDICATOR_MULTIPLE.equals(reCertification) &&
             requireAuthentication ) {	        
            Instrument iError = (Instrument) 
                mediatorServices.createServerEJB("Instrument",
                Long.parseLong(instrumentOid));
            mediatorServices.getErrorManager().issueError(getClass().getName(),
                TradePortalConstants.INSTRUMENT_AUTHENTICATION_FAILED,
                iError.getAttribute("complete_instrument_id") );
        }

	    // check to see if the instrument has previously been processed
	    // if it has been processed previously, issue error that
	    // user selected multiple transactions for the same instrument
	    // otherwise add the instrument to the hash
	    if (preAuthorize && instrumentHash.containsKey(instrumentOid))
	    {
		mediatorServices.getErrorManager().issueError (getClass().getName(),
		    TradePortalConstants.MULTIPLE_INSTRUMENT_AUTHORIZE,
		    (String) instrumentHash.get(instrumentOid));
		continue;
	    }

	    // Before creating the instrument, attempt to lock the instrument
	    // if it has been previously locked, check to see if it is locked
	    // locked by the current user
	    try
	    {
	    	// T36000018805 - Rel 8.2 Start - use saved login User oid since object is still locked by login user
	    	// while offline authorization may be done by proxy user. Using Proxy user oid causes object locking to fail.
		LockingManager.lockBusinessObject(Long.parseLong(instrumentOid),
		    Long.parseLong(((StringFunction.isNotBlank(proxyUserOid) && !"null".equalsIgnoreCase(proxyUserOid)) ) ? loginUserOid : userOid ), true);
		// T36000018805 - Rel 8.2 End
		LOG.debug("LockAuthoriseTransMediator: Locking instrument "
		    + instrumentOid + "successful");
	    }
	    catch (InstrumentLockException e)
	    {
	    	// T36000018805 - Rel 8.2 Start - use saved login User oid since object is still locked by login user
	    	// while offline authorization may be done by proxy user. Using Proxy user oid causes object locking to fail.
		 if (e.getUserOid()==Long.parseLong(((StringFunction.isNotBlank(proxyUserOid) && !"null".equalsIgnoreCase(proxyUserOid)) ) ? loginUserOid : userOid ))
				// T36000018805 - Rel 8.2 End
	    	{
	    	    previouslyLocked = true;
	    	}
	    	else
	    	{
		    // issue instrument in use error indicating instrument ID,
		    // transaction type, first name and last name of user that has instrument
		    // reserved, then skip processing this instrument

		    mediatorServices.getErrorManager().issueError (getClass ().getName (),
		        TradePortalConstants.TRANSACTION_IN_USE, new String[] {
			e.getInstrumentNumber(),
		    	ReferenceDataManager.getRefDataMgr().getDescr("TRANSACTION_TYPE",
		    	transactionType, userLocale), e.getFirstName(), e.getLastName(),
			mediatorServices.getResourceManager().getText("TransactionAction.Authorized",
	    		TradePortalConstants.TEXT_BUNDLE)});
		    continue;

	    	}
	    }

	    try
	    {
	    	// create and load the instrument and call preAuthTransaction
 	    	instrument = (Instrument) mediatorServices.createServerEJB("Instrument",
		    Long.parseLong(instrumentOid));

			// if preAuthorization is required, call preAuthorizeTransaction that will
			// prepare the instrument for authorization
			if (preAuthorize)
			{
	    	    if (instrument.preAuthorizeTransaction(transactionOid, userOid, rights))
	    	    {
	    	        // put the instrument oid and instrument ID in instrumentHash
	    	    	instrumentHash.put(instrumentOid,
					instrument.getAttribute("complete_instrument_id"));

	    	    	// if preauthorize is successful, do not unlock instrument -
		    		// this will be performed by another mediator after authorizing the transaction
	    	    	// otherwise, unlock the instrument
		    		preAuthorizeSuccess = true;
		    	}
			}
			else
			{
		    	// if preAuthorization is not required, call authorizeTransaction on
		    	// the instrument - that will immediately authorize the transaction
		    	// all errors are saved to the transaction and
		    	// the status is set to FAILED if authorizeTransaction returns a false

		    	try
		    	{
					boolean trxAuthorized = false;
					HashMap params = new HashMap();    	// NSX 10/07/11 - CR-581/640 Rel. 7.1 - 
					//ir cnuk113043991 - rearranged to also account for reCertification = multiple
					if (TradePortalConstants.INDICATOR_YES.equals(reCertification) &&
						  !CertificateUtility.validateCertificate(inputDoc, mediatorServices))
				    {
					    authenticated = false;
				    }

					//only authorize if authenticated (or not required :.))
					if ( authenticated ) {
                        //IR-PAUI060561797 Krishna Begin
						// NSX 10/07/11 - CR-581/640 Rel. 7.1 -	Begin -
						if (InstrumentServices.isCashManagementTypeInstrument(instrument.getAttribute("instrument_type_code"))) {
							processCashMgmtInstrument(inputDoc,  instrument,  transactionOid, mediatorServices);
						}
						mediatorServices.getCSDB().setCSDBValue(TradePortalConstants.TRAN_ACTION, TradePortalConstants.TRAN_ACTION_AUTHORIZE);     
						// NSX IR# RUUL122052305 Rel. 7.1.0 - 07/01/12  -	Begin -
						if (instrument.getErrorManager ().getMaxErrorSeverity () >= ErrorManager.ERROR_SEVERITY)
		                {
							continue;
		                }
						// NSX IR# RUUL122052305 - 07/01/12  -	End -
						// NSX 10/07/11 - CR-581/640 Rel. 7.1 -	End -
						//params.put("proxyUser", proxyUser);
						//Ravindra - 19th Nov 2012- Rel8100 IR-DEUM070438986 - Start
                                                // W Zhu 15 May 2013 T36000011771 comment out. AuthorizeTransactionMediator replaces userOid with proxy user's oid.  Should take care of this.
						//if(InstrumentServices.isNotBlank(proxyUser)){
						//	mediatorServices.getCSDB().setCSDBValue("proxyUserOid", proxyUser);
						//}
						//Ravindra - 19th Nov 2012- Rel8100 IR-DEUM070438986 - End
                        trxAuthorized = instrument.convertTransaction(transactionOid,
                                userOid, convertAtParentViewOrSubs, rights, true,params);
                    } else { //not authenticated                        
                        //drop out if not ready to authorize.
                        //ready to authorize goes through to the status change in !trxAuthorized below
					    Transaction transTmpObj = (Transaction) mediatorServices.createServerEJB("Transaction",
			    	    	Long.parseLong(transactionOid));
			    	    if (!TransactionStatus.READY_TO_AUTHORIZE.equals(transTmpObj.getAttribute("transaction_status")))
			    	    {
			    	        //move on to the next transaction, don't just drop out
                            //return new DocumentHandler();
			    	        continue;
						}
			    	    //drop out if transaction doesn't require authentication
			    	    //NOTE: this likely should move above to "if (authenticated)"
			    	    // but part of different effort
			    	    if ( !requireAuthentication ) {
			    	        continue;
			    	    }
					}
					//ir cnuk113043991 end

                	//Authorized Transaction are stored in Hashtable to be retrived in AuthorizeTransactionsDeleteMessageMediatorBean
		    		//in order to delete only the associated mail message of the authorized transactions.
		    		if (trxAuthorized)
			    	{
		    		  convertedTransactions.put(Integer.toString(i), transactionOid);
			    	}

					// IR# POUL110956820 Rel. 7.1.0 - Begin - 
		    		// Refresh transaction so new rate is displayed
					if (TradePortalConstants.INDICATOR_YES.equals(params.get("RATE_CHANGED"))) {
		    			outputDoc.setAttribute("/Refresh/Transaction", "Y");
		    		}
		    		// IR# POUL110956820 Rel. 7.1.0 - End - 
					
                	//IR-PAUI060561797 Krishna End
		        	if (!trxAuthorized)
		    		{
			    		// the transaction is saved by itself rather than going through the
			    		// Instrument.getComponent because the Instrument contains errors -
			    		// it will not allow a save because the validation code checks
			    		// to see if it has been saved.  Additionally, all saves have to be
			    		// performed at the highest parent level that is instantiated, otherwise
			    		// saves will not be committed
		    		    transObj = (Transaction) mediatorServices.createServerEJB("Transaction",
			    	    	Long.parseLong(transactionOid));
			    		// get accumulated errors from instrument and save the errors in
			    		// the instrument
		    		    	// NSX 10/07/11 - CR-581/640 Rel. 7.1 - Begin
		    		    String tran_status = (String) params.get("transaction_status");
						//MDB DOUL122962200 Rel7.1 1/6/12 - Begin
			    		if (InstrumentServices.isBlank(tran_status))
			    			tran_status = transObj.getAttribute("transaction_status");
						//MDB DOUL122962200 Rel7.1 1/6/12 - End
		    		    if (TransactionStatus.FX_THRESH_EXCEEDED.equals(tran_status) || TransactionStatus.AUTH_PEND_MARKET_RATE.equals(tran_status)) {
		    		    	transObj.setAttribute("transaction_status",tran_status);
		    		    } else {	
							// NSX 10/07/11 - CR-581/640 Rel. 7.1 - End
		    		    transObj.setAttribute("authorization_errors", instrument.getErrorManager().getErrorDoc().toString());
			    		// authorization was not successful and the
			    		// transaction status has to be set to failed
			    		transObj.setAttribute("transaction_status",
			    		    TransactionStatus.AUTHORIZE_FAILED);
		    		    }   	// NSX 10/07/11 - CR-581/640 Rel. 7.1 - 
	    	    		transObj.setAttribute("transaction_status_date",
	      	    		    DateTimeUtility.getGMTDateTime());
			    		authorizeFailed = true;
			    		
			    		//IR-LRUK102236760 Narayan Begin
			    		Instrument instrumentTmpObj = (Instrument) mediatorServices.createServerEJB("Instrument",
				    	    	Long.parseLong(instrumentOid));			    		
			    		String instrumentType = instrumentTmpObj.getAttribute("instrument_type_code");
			    		String action = mediatorServices.getCSDB().getCSDBValue(TradePortalConstants.TRAN_ACTION);
			    		
			    		//creating Transaction History if action is specified and instrument type should be TBA or FTDP or FTRQ. action type should be 'Convert'.
			    		if(InstrumentServices.isNotBlank(action) && TradePortalConstants.BUTTON_AUTHORIZE.equals(action)){		    	
			    		  if(authorizeFailed && (instrumentType.equals(InstrumentType.DOM_PMT) ||  instrumentType.equals(InstrumentType.XFER_BET_ACCTS)
			            		||instrumentType.equals(InstrumentType.FUNDS_XFER))){
			    		    TransactionHistory tranHistory = (TransactionHistory) mediatorServices.createServerEJB("TransactionHistory");			    			
			    			tranHistory.newObject();
			    			tranHistory.setAttribute("action_datetime", DateTimeUtility.getGMTDateTime());
			    			tranHistory.setAttribute("action", action);
			    			tranHistory.setAttribute("transaction_status", transObj.getAttribute("transaction_status"));
			    			tranHistory.setAttribute("transaction_oid", transObj.getAttribute("transaction_oid"));			    			     
			    			String user_oid = mediatorServices.getCSDB().getCSDBValue("user_oid");
			    			tranHistory.setAttribute("user_oid", user_oid);
			    			//Ravindra - 19th Nov 2012- Rel8100 IR-DEUM070438986 - Start
						//set proxyUserOid to Transaction History if the activity has performed by offline user
                                                // W Zhu 15 May 2013 T36000011771 comment out. AuthorizeTransactionMediator replaces userOid with proxy user's oid.  Should take care of this.
							//String proxyUserOid=mediatorServices.getCSDB().getCSDBValue("proxyUserOid");
							//if(InstrumentServices.isNotBlank(proxyUserOid)){
							//	tranHistory.setAttribute("user_oid", proxyUserOid);
							//}
							//Nov 2012- Rel8100 IR-DEUM070438986 - Start
							//using proxyuser directly, since CSDB above may not have the value
							//if(InstrumentServices.isNotBlank(proxyUser)){
							//	tranHistory.setAttribute("user_oid",proxyUser);
							//}
			    			//Ravindra - 19th Nov 2012- Rel8100 IR-DEUM070438986 - End
			    			User user = (User) mediatorServices.createServerEJB("User", Long.parseLong(user_oid));
			    			String userPanelAuthLevel = user.getAttribute("panel_authority_code");
			    			tranHistory.setAttribute("panel_authority_code", userPanelAuthLevel);			    						    				
			    			tranHistory.save(false);			    		
			    		  }
			    		}
			    		
			    		//IR-LRUK102236760 Narayan End
			    		try
			    		{
		    		        transObj.save();
		    		        
		    		        //cnuk113043991 - indicate the transaction needs to be refreshed
                            outputDoc.setAttribute("/Refresh/Transaction", "Y");
			    		}
			    		catch(Exception e)
			    		{
							LOG.info(e.toString());
							e.printStackTrace();
			    		}
		        	}
		     	}
		     	catch(Exception e)
	 	     	{
	    	        LOG.info("Exception occured in AuthorizeTransactionsMediator: " + e.toString());
	    	        e.printStackTrace();
	 	     	}
		 	}
	    }
	    finally
	    {
		// if previously locked or if preAuthorize is successful,
		// do not unlock the instrument
		if (!previouslyLocked && !preAuthorizeSuccess)
	        {
	    	    try
	    	    {
		    	LockingManager.unlockBusinessObjectByInstrument(Long.parseLong(instrumentOid), true);
	    	    }
	    	    catch (InstrumentLockException e)
	    	    {
		        // this should never happen
		        e.printStackTrace();
	    	    }
	 	}
	    }

	}

	LOG.debug("PreAuthTransMediator: " + inputDoc);

	return outputDoc;

  }
   
   // NSX 10/07/11 - CR-581/640 Rel. 7.1 - Begin - 
   
   protected void processCashMgmtInstrument(DocumentHandler inputDoc, Instrument instrument, String transactionOid,MediatorServices mediatorServices) throws RemoteException, AmsException {
	   String buttonPressed = inputDoc.getAttribute("/Update/ButtonPressed");
	   if (TradePortalConstants.BUTTON_CONFIRM_FX.equals(buttonPressed)) {
			DocumentHandler fxResp = inputDoc.getComponent("/FXResp");
			if (fxResp == null) {
				mediatorServices.getErrorManager().issueError (getClass().getName(),
						TradePortalConstants.NO_ITEM_SELECTED, mediatorServices.getResourceManager().
						getText("TransactionAction.Convert",	TradePortalConstants.TEXT_BUNDLE));
				throw new AmsException("error occured, FXDeal response in not present...");
				//return outputDoc;
			}
			updateFXDealData(instrument,transactionOid, fxResp,  mediatorServices);
		} else {
			updateFXContractData(inputDoc,instrument, mediatorServices);     //NSX IR# PSUL112862587 Rel.7.1.0 12/09/11
		}
   }
   
   
   /**
	 * @param transactionOid
	 * @param fxResp
	 * @param mediatorServices
	 * @throws NumberFormatException
	 * @throws RemoteException
	 * @throws AmsException
	 */
	protected void updateFXDealData (Instrument instrument,String transactionOid, DocumentHandler fxResp,  MediatorServices mediatorServices) throws NumberFormatException, RemoteException, AmsException {

		Transaction transObj = (Transaction) instrument.getComponentHandle("TransactionList",  Long.parseLong(transactionOid));
		String action = transObj.getClientServerDataBridge().getCSDBValue(TradePortalConstants.TRAN_ACTION);
		transObj.getClientServerDataBridge().setCSDBValue(TradePortalConstants.TRAN_ACTION, "");
		Terms terms = (Terms)(transObj.getComponentHandle("CustomerEnteredTerms"));

		terms.setAttribute("covered_by_fec_number", fxResp.getAttribute("/ForEXDealId"));
		terms.setAttribute("fec_rate", fxResp.getAttribute("/ExchRate"));
		terms.setAttribute("fx_online_deal_confirm_date", fxResp.getAttribute("/DealDateTime"));
		terms.setAttribute("transfer_fx_rate", fxResp.getAttribute("/ExchRate"));
		terms.setAttribute("equivalent_exch_amount", fxResp.getAttribute("/EquivalentAmt"));
		terms.setAttribute("equivalent_exch_amt_ccy", fxResp.getAttribute("/EquivalentAmtCCY"));
		//terms.setAttribute("display_fx_rate_method", getCalculationMethod(terms));        //IR# DAUM011860866 Rel. 7.1.0   - 01/19/12 -
		terms.setAttribute("display_fx_rate_method", fxResp.getAttribute("/CalcMethod"));      //IR# DAUM011860866 Rel. 7.1.0   - 01/19/12 -

		if (transObj.save() < 0) {
			mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.FX_GET_RATE_GENERAL_ERR);
			throw new AmsException("error occured updating transaction...");
		}
		else {
			//log deal booked warning
			mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.FX_DEAL_SUCCESSFULY_BOOKED);
		}
		transObj.getClientServerDataBridge().setCSDBValue(TradePortalConstants.TRAN_ACTION, action);
	}
	
//IR# DAUM011860866 Rel. 7.1.0   - 01/19/12 - Begin -  Removed unused method 	
//	/**
//	 * @param terms
//	 * @return
//	 * @throws RemoteException
//	 * @throws AmsException
//	 */
//	protected String getCalculationMethod (Terms terms) throws RemoteException, AmsException {
//		String val =null;
//		BigDecimal rate = new BigDecimal(terms.getAttribute("fec_rate"));
//		BigDecimal equi_Amt = new BigDecimal(terms.getAttribute("equivalent_exch_amount"));
//		BigDecimal pay_amt = new BigDecimal(terms.getAttribute("amount"));
//		
//		if (rate.multiply(pay_amt).setScale(equi_Amt.scale(), BigDecimal.ROUND_HALF_UP).compareTo(equi_Amt)==0) {     //NSX IR# DHUL121264557 Rel.7.1.0 12/15/11
//			val =TradePortalConstants.MULTIPLY;
//		}else {
//			val = TradePortalConstants.DIVIDE;
//		}
//		
//		return val;
//	}
//IR# DAUM011860866 Rel. 7.1.0   - 01/19/12 - End -	 
	 
	 
    //public boolean updateFXContractData(DocumentHandler inputDoc, Instrument instrument) throws RemoteException, AmsException{     //NSX IR# PSUL112862587 Rel.7.1.0 12/09/11
    public boolean updateFXContractData(DocumentHandler inputDoc, Instrument instrument, MediatorServices mediatorServices) throws RemoteException, AmsException{    //NSX IR# PSUL112862587 Rel.7.1.0 12/09/11

    		Transaction tran = (Transaction) instrument.getComponentHandle("TransactionList",  Long.valueOf(instrument.getAttribute("original_transaction_oid")).longValue());
    		String tran_status = tran.getAttribute("transaction_status");
    		if (TransactionStatus.FX_THRESH_EXCEEDED.equals(tran_status) || TransactionStatus.AUTH_PEND_MARKET_RATE.equals(tran_status)) {
    			Terms terms = (Terms)(tran.getComponentHandle("CustomerEnteredTerms"));
    			if (InstrumentServices.isBlank(terms.getAttribute("covered_by_fec_number")) ||
    					InstrumentServices.isBlank(terms.getAttribute("fec_rate")))  {
    				if (InstrumentServices.isNotBlank(inputDoc.getAttribute("/Terms/covered_by_fec_number")) ||
    						InstrumentServices.isNotBlank(inputDoc.getAttribute("/Terms/fec_rate"))) {
    					terms.setAttribute("covered_by_fec_number",inputDoc.getAttribute("/Terms/covered_by_fec_number"));
    					terms.setAttribute("fec_rate",inputDoc.getAttribute("/Terms/fec_rate"));

    					calcAmount( inputDoc,  instrument,  terms, mediatorServices);       //NSX IR# PSUL112862587 Rel.7.1.0 12/09/11
    				}
    			}
    		}

    	return true;
    }
	
    //NSX IR# PSUL112862587 Rel.7.1.0 12/09/11 - Begin -
    /**
     * @param inputDoc
     * @param instrument
     * @param terms
     * @param mediatorServices
     * @throws RemoteException
     * @throws AmsException
     */
    protected void calcAmount(DocumentHandler inputDoc, Instrument instrument, Terms terms, MediatorServices mediatorServices) throws RemoteException, AmsException{
    	String rate = terms.getAttribute("fec_rate");
    	String transferCurrency = terms.getAttribute("amount_currency_code");
    	String amount = terms.getAttribute("amount");
    	String ex_ind = null,ex_currency=null, s_ex_amount = null;   
    	BigDecimal ex_amount = null;
    	Account fromAccount = (Account) mediatorServices.createServerEJB("Account",Long.parseLong(terms.getAttribute("debit_account_oid")));
    	String fromCurrency = fromAccount.getAttribute("currency");
		// NSX IR# RUUL122052305 Rel. 7.1.0 - 07/01/12  -	Begin -
    	String baseCurrency = fromAccount.getBaseCurrencyOfAccount();
    	String accOwnerOid = fromAccount.getOwnerOfAccount();
		Vector outMDInd = new Vector(1,1);
		String rateType=null;
		// NSX IR# RUUL122052305 - 07/01/12  -	Begin -
		
    	if (InstrumentServices.isNotBlank(rate)) {
    		int dec = 0;
    		if (InstrumentType.XFER_BET_ACCTS.equals(inputDoc.getAttribute("/Instrument/instrument_type_code")) && fromCurrency.equalsIgnoreCase(transferCurrency))
    		{
    			Account toAccount = (Account) mediatorServices.createServerEJB("Account",Long.parseLong(terms.getAttribute("credit_account_oid")));
    			String toCurrency=toAccount.getAttribute("currency");
				// NSX IR# RUUL122052305 Rel. 7.1.0 - 07/01/12  -	Begin -
    			ex_currency = toCurrency;
    			if (!fromCurrency.equals(baseCurrency) && !toCurrency.equals(baseCurrency) && !fromCurrency.equals(toCurrency)) {

    				ex_ind = instrument.crossRateCriterionProcessing(terms, transferCurrency, toCurrency, rate);
    				dec = TPCurrencyUtility.getDecimalPrecision(toCurrency);
    				if (TradePortalConstants.MULTIPLY.equals(ex_ind))
    	    			ex_amount = (new BigDecimal(amount)).multiply(new BigDecimal(rate)).setScale(dec, BigDecimal.ROUND_HALF_UP);
    	    		else if (TradePortalConstants.DIVIDE.equals(ex_ind))
    	    			ex_amount = (new BigDecimal(amount)).divide(new BigDecimal(rate), dec, BigDecimal.ROUND_HALF_UP);
    			}
    			else {

    				if (fromCurrency.equals(baseCurrency)) 
    					rateType=TradePortalConstants.USE_SELL_RATE;
    				else 
    					rateType=TradePortalConstants.USE_BUY_RATE;
    				if (ex_currency.equals(baseCurrency)) {
    					ex_amount = instrument.getAmountInBaseCurrency(transferCurrency, amount,baseCurrency , accOwnerOid, rateType, new BigDecimal(rate), outMDInd);
    				}
    				else {
    					ex_amount = instrument.getAmounInFXCurrency(ex_currency, baseCurrency, amount, accOwnerOid,rateType, new BigDecimal(rate), outMDInd);

    				}
    				if (!outMDInd.isEmpty() && (outMDInd.size()>= 0))
    					ex_ind = (String)outMDInd.get(0);
    			}
				// NSX IR# RUUL122052305 - 07/01/12  -	End -
    		}
    		else
    		{
			    // NSX IR# RUUL122052305 Rel. 7.1.0 - 07/01/12  -	Begin -
    			ex_currency = fromCurrency;    			
    			if (transferCurrency.equals(fromCurrency))
    			{
    				//update with nothing
    			}
    			else if (transferCurrency.equals(baseCurrency))
    			{
    				rateType = TradePortalConstants.USE_BUY_RATE;
    				ex_amount = instrument.getAmounInFXCurrency(fromCurrency, baseCurrency, amount, accOwnerOid,rateType,new BigDecimal(rate), outMDInd);															

    				if (!outMDInd.isEmpty() && (outMDInd.size()>= 0))									
    					ex_ind = (String)outMDInd.get(0);												
    			}
    			else if (fromCurrency.equals(baseCurrency))
    			{
    				rateType = TradePortalConstants.USE_SELL_RATE;
    				ex_amount = instrument.getAmountInBaseCurrency(transferCurrency, amount, baseCurrency, accOwnerOid, rateType,new BigDecimal(rate), outMDInd);															

    				if (!outMDInd.isEmpty() && (outMDInd.size()>= 0))									
    					ex_ind = (String)outMDInd.get(0);												
    			}
    			else{
				  // NSX IR# RUUL122052305 - 07/01/12  -	End -
    				ex_ind = instrument.crossRateCriterionProcessing(terms, transferCurrency, fromCurrency, rate);
    				dec = TPCurrencyUtility.getDecimalPrecision(fromCurrency);
    				if (TradePortalConstants.MULTIPLY.equals(ex_ind))
    	    			ex_amount = (new BigDecimal(amount)).multiply(new BigDecimal(rate)).setScale(dec, BigDecimal.ROUND_HALF_UP);
    	    		else if (TradePortalConstants.DIVIDE.equals(ex_ind))
    	    			ex_amount = (new BigDecimal(amount)).divide(new BigDecimal(rate), dec, BigDecimal.ROUND_HALF_UP);
    			}      
    		}
    	}

    	if (ex_amount == null)
    	{
    		rate = null;
    		s_ex_amount = null;
    		ex_currency=null;                             		                 	              
    		ex_ind = null;																			
    	}
    	else {
    		s_ex_amount = ex_amount.toString();
    	}
    	terms.setAttribute("equivalent_exch_amount", s_ex_amount);
    	terms.setAttribute("transfer_fx_rate", rate);
    	terms.setAttribute("equivalent_exch_amt_ccy", ex_currency);                 			     
    	terms.setAttribute("display_fx_rate_method", ex_ind);										

    }
   //NSX IR# PSUL112862587 Rel.7.1.0 12/09/11 - End -

  // NSX 10/07/11 - CR-581/640 Rel. 7.1 - End -

   

   //ir cnuk113043991 - start              
   /**
    * Does the transaction require authentication?
	*/
   private boolean requireTransactionAuthentication(String instrumentOid, 
         String transactionOid, String transactionType, MediatorServices mediatorServices) 
         throws RemoteException, AmsException {
      Instrument instrument = (Instrument) 
         mediatorServices.createServerEJB("Instrument",
            Long.parseLong(instrumentOid));
      String instrumentType = instrument.getAttribute("instrument_type_code");
      long clientBankOid = instrument.getAttributeLong("client_bank_oid");
      ClientBank clientBank = (ClientBank) mediatorServices.createServerEJB("ClientBank");
      clientBank.getDataFromReferenceCache(clientBankOid);
      String requireTranAuth = clientBank.getAttribute("require_tran_auth");
      if (InstrumentAuthentication.requireTransactionAuthentication(
             requireTranAuth,
             instrumentType + "_" + transactionType)) {
         return true;
      }
      return false;
   }
   //ir cnuk113043991 - end

   
   /* cr498 moved to CertificateUtility for common use
   private boolean validateCertificate(DocumentHandler inputDoc, MediatorServices mediatorServices)
      throws AmsException, RemoteException {
   // CR-473 BEGIN
   // Validate Smart Card
    String userOid = inputDoc.getAttribute("/User/userOid");
    String reCertOK = inputDoc.getAttribute("/Authorization/reCertOK");
       String logonResponse = inputDoc.getAttribute("/Authorization/logonResponse");
       String logonCertificate = inputDoc.getAttribute("/Authorization/logonCertificate");

       LOG.debug("reCertOK = " + reCertOK);
       
       // If the 2FA failed or SmartCard reading failed ...
       if (TradePortalConstants.INDICATOR_NO.equals(reCertOK)) {
           mediatorServices.getErrorManager().issueError(getClass().getName(),
                   TradePortalConstants.RECERTIFICATION_FAILED);
           return false;
       }
       
       // W Zhu 12/16/09 CR-482 BEGIN
       // Skip validation if recertOK = Y, when we did a dummy recert page or if 2FA succeeded (2FA is validated on TradePortalRecertification.jsp
       // Also skip validation if reCertOK is empty, when there is no recert.
       // Only continue to validate the certificate data if recertOK = M, when we have read the certificate data from Smart Card.
       // TODO: we should unify the action flow for different recertification in the future.
       if (TradePortalConstants.INDICATOR_YES.equals(reCertOK) || InstrumentServices.isBlank(reCertOK)) {
           return true;
       }

   //IAZ CR-473 05/28/09 Begin: Check if this is dummy/dev/test authentication that does not req smartcard and certificate
   //IAZ CR-473 06/04/09 Begin: Get ClientBank Oid directly from User's data
   User user = (User)mediatorServices.createServerEJB("User", Long.parseLong(userOid));
   //ClientBank clientBank = (ClientBank) mediatorServices.createServerEJB("ClientBank");
   //clientBank.getDataFromReferenceCache(user.getAttributeLong("client_bank_oid"));
   //CR-473 06/04/09 End
   //LOG.debug("cert parms " + userOid + " " + user.getAttribute("client_bank_oid"));

   //String certPageURL = clientBank.getAttribute("authorization_cert_auth_URL");
   //LOG.debug("certPageURL " + certPageURL);

   //IAZ CR-473 06/04/09 - If this is a dummy test page, no smart card certification is needed.
   //if (!InstrumentServices.isBlank(certPageURL) && (certPageURL.indexOf(TradePortalConstants.AUTHENT_PAGE_DUMMY) != -1))
   //{
   //	   return true;
   //}
   // CR-482 END

   if (InstrumentServices.isBlank(logonCertificate))
   {
       LOG.info("RECERTIFICATION FAILED: No Logon Certificate Provided");
       mediatorServices.getErrorManager().issueError (getClass().getName(), TradePortalConstants.RECERTIFICATION_FAILED);
       return false;
   }
   //IAZ CR-473 05/28/09 End


   // Get the smart card certificate
   byte[] logonCertificateBytes = logonCertificate.getBytes();
   X509Certificate smartCardCert = EncryptDecrypt.getCertificate(logonCertificateBytes);
   if (smartCardCert == null ) {
       LOG.debug("Invalid certificate : " + logonCertificate);
       LOG.info("Invalid certificate : " + logonCertificate);
       mediatorServices.getErrorManager().issueError (getClass().getName(), TradePortalConstants.RECERTIFICATION_FAILED);
       return false;
   }

   // Check the Challenge String Response matches the original challengge string
   java.security.PublicKey smartCardPublicKey = smartCardCert.getPublicKey();
   byte[] logonResponseBytes = EncryptDecrypt.base64StringToBytes(logonResponse, false);
   byte[] decryptedLogonResponseBytes = EncryptDecrypt.decryptUsingPublicKey(logonResponseBytes, smartCardPublicKey);
   final int CHALLENGE_RESPONSE_FILLER_NUMBER = 24; // The first 24 characters in the response is filler.  Ignore.
   String challengeStringResponse = null;
   try {
       challengeStringResponse = EncryptDecrypt.bytesToBase64String(decryptedLogonResponseBytes, false).substring(CHALLENGE_RESPONSE_FILLER_NUMBER);
   }
   catch (Exception e){
       LOG.debug("logonResponse = " + logonResponse);
       mediatorServices.getErrorManager().issueError (getClass().getName(),
               TradePortalConstants.RECERTIFICATION_FAILED);
       LOG.info("logon response error" + logonResponse);
       return false;
   }
   String challengeString = mediatorServices.getCSDB().getCSDBValue("SmartCardChallengeString");
   String challengeStringKey = mediatorServices.getCSDB().getCSDBValue("SmartCardChallengeStringKey");

   if (challengeString == null || !challengeString.equals(challengeStringResponse)) {
       LOG.debug("challengeStringKey = " + challengeStringKey + "; challengeString = " + challengeString + "; challengeStringResponse = " + challengeStringResponse);
       mediatorServices.getErrorManager().issueError (getClass().getName(),
               TradePortalConstants.RECERTIFICATION_FAILED);
       LOG.info("recertification failed: challengeStringKey = " + challengeStringKey + "; challengeString = " + challengeString + "; challengeStringResponse = " + challengeStringResponse);
       return false;
   }

   // Get the ID on the certificate, which is the CN part of the Subject Distinguished Name
   String subjectDN = smartCardCert.getSubjectDN().getName();
   StringTokenizer parser = new StringTokenizer(subjectDN, "=, ");
   String certificateIDResponse = null;
   while (parser.hasMoreTokens()){
       String token = parser.nextToken();
       if (token.equals("CN") && parser.hasMoreTokens()) {
           certificateIDResponse = parser.nextToken();
           break;
       }
   }

   LOG.debug("getting form the database " + user.getAttribute("certificate_id"));

   // Validate the ID on the certificate is the certificate_id of the user
   //User user = (User)mediatorServices.createServerEJB("User", Long.parseLong(userOid));
   String certificateID = user.getAttribute("certificate_id");
   if (certificateID == null || !certificateID.equals(certificateIDResponse)) {
       LOG.debug("certificateID = " + certificateID + "; certificateIDResponse = " + certificateIDResponse);
       mediatorServices.getErrorManager().issueError (getClass().getName(),
          TradePortalConstants.INCORRECT_CERTIFICATE_ID);
       return false;
   }

   return true;
   }
   // CR-473 Validate Smart Card ends
   */

}