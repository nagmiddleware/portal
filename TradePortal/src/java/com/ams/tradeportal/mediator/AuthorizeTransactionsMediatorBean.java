package com.ams.tradeportal.mediator;

import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.busobj.Account;
import com.ams.tradeportal.busobj.ClientBank;
import com.ams.tradeportal.busobj.Instrument;
import com.ams.tradeportal.busobj.Terms;
import com.ams.tradeportal.busobj.Transaction;
import com.ams.tradeportal.busobj.TransactionHistory;
import com.ams.tradeportal.busobj.User;
import com.ams.tradeportal.busobj.util.CertificateUtility;
import com.ams.tradeportal.busobj.util.InstrumentAuthentication;
import com.ams.tradeportal.busobj.util.InstrumentServices;
import com.ams.tradeportal.busobj.util.SecurityAccess;
import com.ams.tradeportal.common.InstrumentType;
import com.ams.tradeportal.common.TPCurrencyUtility;
import com.ams.tradeportal.common.TradePortalConstants;
import com.ams.tradeportal.common.TransactionStatus;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.frame.ErrorManager;
import com.amsinc.ecsg.frame.InstrumentLockException;
import com.amsinc.ecsg.frame.IssuedError;
import com.amsinc.ecsg.frame.LockingManager;
import com.amsinc.ecsg.frame.MediatorBean;
import com.amsinc.ecsg.frame.MediatorServices;
import com.amsinc.ecsg.frame.ReferenceDataManager;
import com.amsinc.ecsg.util.DateTimeUtility;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.StringFunction;

/*
 *
 *     Copyright  © 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class AuthorizeTransactionsMediatorBean extends MediatorBean {
	private static final Logger LOG = LoggerFactory.getLogger(AuthorizeTransactionsMediatorBean.class);

	protected List<String> authorizedTransactionsOidList = null;

    @Override
	public DocumentHandler execute(DocumentHandler inputDoc, DocumentHandler outputDoc, MediatorServices mediatorServices)
			throws RemoteException, AmsException {
	  /**
	   * This method performs deletions of Transactions
	   * The format expected from the input doc is:
	   *
	   * <TransactionList>
	   *	<Transaction ID="0"><transactionData>1000001/1200001/AMD</transactionData></Transaction>
	   *	<Transaction ID="1"><transactionData>1000002/1200002/ISS</transactionData></Transaction>
	   *	<Transaction ID="2"><transactionData>1000003/1200003/TRN</transactionData></Transaction>
	   *	<Transaction ID="3"><transactionData>1000004/1200004/ASG</transactionData></Transaction>
	   *	<Transaction ID="6"><transactionData>1000001/1200007/ISS</transactionData></Transaction>
	   * </TransactionList>
	   * <User>
	   *    <userOid>1001</userOid>
	   *    <securityRights>302231454903657293676543</securityRights>
	   * </User>
	   * <Authorization>
	   *    <preAuthorize>Y|N</preAuthorize>
	   * </Authorization>
	   *
	   * The oid value consists of transaction oid, a '/', instrument oid, a '/'
	   * and the transaction type
	   *
	   * This is a non-transactional mediator that executes transactional
	   * methods in business objects.  It accepts preAuthorize as a parameter which
	   * determines if the transaction should be preAuthorized (ie. authorized at the
	   * background) or if it should be authorized immediately.
	   * For preauthorization, it processes each transaction by
	   * locking the instrument (if necessary) and setting the transaction
	   * status to Authorised Pending.
	   * If there is more than one transaction selected for an instrument,
	   * this mediator locks the instrument for the first transaction and gives
	   * a lock error for any subsequent transactions.
	   * For authorization, it locks the instrument, performs the authorize process
	   * and sets the transaction status to Failed, Authorized or Partially Authorized.
	   * This method accepts multiple transactions in the format specified above.
	   * Note that this mediator has been tested for both preAuthorize and
	   * Authorize. However, for preAuthorize, the effect of instrument locking
	   * on a general level has to be examined further.
	   *
	   * @param inputDoc The input XML document (&lt;In&gt; section of overall Mediator XML document)
	   * @param outputDoc The output XML document (&lt;Out&gt; section of overall Mediator XML document)
	   * @param mediatorServices Associated class that contains lots of the auxillary functionality for a mediator.
	   */

		// get each Transaction Oid and process each transaction each transaction is unique within itself

		// populate variables with inputDocument information such as transaction list, user oid and rights, etc

		// VS RVU062382912 Checks if user is at subsidiary access mode or pending transaction leve


		List<DocumentHandler> transactionList = inputDoc.getFragmentsList("/TransactionList/Transaction");
		int count = transactionList.size();
		String userOid = inputDoc.getAttribute("/User/userOid");
		String loginUserOid = inputDoc.getAttribute("/User/userOid");

		String rights = inputDoc.getAttribute("/User/securityRights");
		// W Zhu 15 May 2013 T36000011771 comment out. Replaces userOid with proxy user's oid. Use the proxy user's security right
		// and log the authorization with his/her ID.
		String proxyUserOid = inputDoc.getAttribute("/proxy");
		proxyUserOid = (StringFunction.isNotBlank(proxyUserOid) && !proxyUserOid.equalsIgnoreCase("null")) ? proxyUserOid : inputDoc.getAttribute("/User/proxyUserOid");
		if (StringFunction.isNotBlank(proxyUserOid) && !proxyUserOid.equalsIgnoreCase("null")) {
			userOid = proxyUserOid;
			User proxyUser = (User) mediatorServices.createServerEJB("User", Long.parseLong(proxyUserOid));
			String securityProfileOid = proxyUser.getAttribute("security_profile_oid");
			if (StringFunction.isNotBlank(securityProfileOid)) {
				String sqlStatement = "select security_rights from security_profile where security_profile_oid = ?";
            	DocumentHandler resultXML = DatabaseQueryBean.getXmlResultSet(sqlStatement, false, new Object[]{securityProfileOid});
				// For this Instrument, use the offline user's security rights
				rights = resultXML.getAttribute("/ResultSetRecord/SECURITY_RIGHTS");
			}
		}
		// Security check for Logged user
		String buttonPressed = inputDoc.getAttribute("/Update/ButtonPressed");
		if (StringFunction.isBlank(buttonPressed)) {
			buttonPressed = TradePortalConstants.BUTTON_AUTHORIZE;
		}

		SecurityAccess.validateUserRightsForTrans(inputDoc.getAttribute("/Instrument/instrument_type_code"),
				inputDoc.getAttribute("/Transaction/transaction_type_code"), rights, buttonPressed);

		// VS RVU062382912 Setting parameter value and setting default value
		String authorizeAtParentViewOrSubs = inputDoc.getAttribute("/Authorization/authorizeAtParentViewOrSubs");
		if (StringFunction.isBlank(authorizeAtParentViewOrSubs)) {
			authorizeAtParentViewOrSubs = TradePortalConstants.INDICATOR_NO;

			// this value will be used to make action entry in transaction history table to find whether sub panel code
			// should be used or parent panel code.
			mediatorServices.getCSDB().setCSDBValue("subsidiaryAccessTab", TradePortalConstants.INDICATOR_YES);

		}

		boolean preAuthorize = TradePortalConstants.INDICATOR_YES.equals(inputDoc.getAttribute("/Authorization/preAuthorize"));

		// get the locale from client server data bridge, used for getting locale
		// specific information when passing error parameters
		String userLocale = mediatorServices.getCSDB().getLocaleName();
		if (TradePortalConstants.BUTTON_CONFIRM_FX.equals(inputDoc.getAttribute("/Update/ButtonPressed"))) {
    	    mediatorServices.getCSDB().setCSDBValue(TradePortalConstants.TRAN_ACTION, TradePortalConstants.TRAN_ACTION_CONFIRM_FXDEAL);
		} else {
			mediatorServices.getCSDB().setCSDBValue(TradePortalConstants.TRAN_ACTION, TradePortalConstants.TRAN_ACTION_AUTHORIZE); 
		}

		mediatorServices.getCSDB().setCSDBValue("user_oid", userOid); 
		LOG.debug("AuthorizeTransactionsMediator: number of transactions: {}", count);

		if (count <= 0) {
			String errorAction = "TransactionAction.Authorize";
			mediatorServices.getErrorManager().issueError(getClass().getName(), TradePortalConstants.NO_ITEM_SELECTED,
					mediatorServices.getResourceManager().getText(errorAction, TradePortalConstants.TEXT_BUNDLE));
			return new DocumentHandler();
		}

		// if authentication with certificate is required for authorize and reCertification failed, issue an error and stop.
		// handle case of multiple reqs (from pending list view) and single reqt (transaction page)

		String reCertification = inputDoc.getAttribute("/Authorization/reCertification");
		LOG.debug("AuthorizeTransactionMediator:: Recert Reqt: {}", reCertification);

		// ir cnuk113043991 - if an authentication failure on list, continue processing
		// so we get into the status change logic. the authenticated boolean
		// is set so we DO NOT attempt to authorize
		boolean authenticated = true; // default authenticated (backwards?)
		if (TradePortalConstants.INDICATOR_MULTIPLE.equals(reCertification)
				&& !CertificateUtility.validateCertificate(inputDoc, mediatorServices)) {
			authenticated = false;
		}

		// if preAuthorize is selected, only one transaction from an instrument
		// can be processed and locked at one time. One reason is that
		// the instrument is locked for a transaction processing and is unlocked
		// after the processing is complete
		// instrumentHash is used to store Instrument Oid(key) and
		// Instrument ID (value) of transactions that have been successly
		// preAuthorized and to ensure no additional transactions in that instrument
		// is processed

		Map<String, String> instrumentHash = new HashMap<>();
		// IR-PAUI060561797 Krishna Begin
		// Initializing Hashtable to store Authorized Transactions
		authorizedTransactionsOidList = new ArrayList<>();
		// IR-PAUI060561797 Krishna End
		// a transaction should be locked and processed at the instrument level
		// since the transaction is a component of instrument
		// for each transaction, check to make sure that the instrument has not
		// been previously locked in this list of transactions.
		// Then attempt to lock the instrument and call changeTransactionStatus
		// on the instrument. preAuthoriseTransaction is a transactional method.

		/*
		 * KMehta IR-T36000035515 Rel 9300 on 03-Apr-2015 Change - Begin Made changes for non-duplicate amendments to authorize
		 * without error
		 */

		List<String> instrumentOidList = new ArrayList<>();

		/*
		 * Begin IR#T36000030847 Rel. 9.1 Vsarkary Check to throw error message if multiple amendment activities are authorized
		 * together for same instrument
		 */

		for (DocumentHandler transaction : transactionList) {
			String oid = transaction.getAttribute("/transactionData");

			int sep1 = oid.indexOf('/');
			int sep2 = oid.indexOf('/', sep1 + 1);

			String transactionOid = oid.substring(0, sep1);
			String instrumentOid = oid.substring(sep1 + 1, sep2);
			String transactionType = oid.substring(sep2 + 1);

			Transaction transTmpObj = (Transaction) mediatorServices.createServerEJB("Transaction", Long.parseLong(transactionOid));
			if ((transactionType.equalsIgnoreCase("AMD") || transactionType.equalsIgnoreCase("ATR"))
					&& TransactionStatus.READY_TO_AUTHORIZE.equals(transTmpObj.getAttribute("transaction_status"))) {

				instrumentOidList.add(instrumentOid);

				if (isDuplicatesExist(instrumentOidList)) {
					mediatorServices.getErrorManager().issueError(getClass().getName(),	TradePortalConstants.MULTIPLE_INSTRUMENT_AMEND, instrumentHash.get(instrumentOid));
					return new DocumentHandler();
				}
			}
		}
		/* KMehta IR-T36000035515 Rel 9300 on 03-Apr-2015 Change - End */
		/* End IR#T36000030847 Rel. 9.1 Vsarkary */

		for (DocumentHandler transaction : transactionList) {
			String oid = transaction.getAttribute("/transactionData");

			// previouslyLocked keeps track of an instrument that was not locked
			// by this mediator and is used to keep the instrument locked if it was
			// previously locked

			boolean previouslyLocked = false; // if the instrument was previously locked, don't unlock if there is an error
			boolean preAuthorizeSuccess = false;
			boolean authorizeFailed = false;

			// Split up the instrument oid, transaction oid and transaction type
			int sep1 = oid.indexOf('/');
			int sep2 = oid.indexOf('/', sep1 + 1);

			String transactionOid = oid.substring(0, sep1);
			String instrumentOid = oid.substring(sep1 + 1, sep2);
			String transactionType = oid.substring(sep2 + 1);

			//LOG.debug("PreAuthTransactionMediator: oid contents: {} Instrument oid: {} Transaction oid : {} Transaction Type : {}", oid, instrumentOid, transactionOid, transactionType);

			// ir cnuk113043991 - determine if the transaction requires authentication
			boolean requireAuthentication = requireTransactionAuthentication(instrumentOid, transactionOid, transactionType, mediatorServices);

			// ir ctuk113042306 - if authentication failed and on a list page
			// and the transaction requires authentication,
			// first issue an additional error to indicate which transactions
			// failed authentication. other errors that follow (such as
			// instrument in use) are added to this. processing continues
			// in order to attempt a status change if necessary later on
			if (!authenticated && TradePortalConstants.INDICATOR_MULTIPLE.equals(reCertification) && requireAuthentication) {
				Instrument iError = (Instrument) mediatorServices.createServerEJB("Instrument", Long.parseLong(instrumentOid));
				mediatorServices.getErrorManager().issueError(getClass().getName(),
						TradePortalConstants.INSTRUMENT_AUTHENTICATION_FAILED, iError.getAttribute("complete_instrument_id"));
			}

			// check to see if the instrument has previously been processed
			// if it has been processed previously, issue error that
			// user selected multiple transactions for the same instrument
			// otherwise add the instrument to the hash
			if (preAuthorize && instrumentHash.containsKey(instrumentOid)) {
				mediatorServices.getErrorManager().issueError(getClass().getName(),
						TradePortalConstants.MULTIPLE_INSTRUMENT_AUTHORIZE, instrumentHash.get(instrumentOid));
				continue;
			}

			// Before creating the instrument, attempt to lock the instrument
			// if it has been previously locked, check to see if it is locked
			// locked by the current user
			try {
				// T36000018805 - Rel 8.2 Start - use saved login User oid since object is still locked by login user
				// while offline authorization may be done by proxy user. Using Proxy user oid causes object locking to fail.
				LockingManager.lockBusinessObject(Long.parseLong(instrumentOid),
						Long.parseLong((StringFunction.isNotBlank(proxyUserOid) && !"null".equalsIgnoreCase(proxyUserOid)) ? loginUserOid : userOid), true);
				// T36000018805 - Rel 8.2 End
				LOG.debug("LockAuthoriseTransMediator: Locking instrument {} successful", instrumentOid);
			} catch (InstrumentLockException e) {
				// T36000018805 - Rel 8.2 Start - use saved login User oid since object is still locked by login user
				// while offline authorization may be done by proxy user. Using Proxy user oid causes object locking to fail.
				if (e.getUserOid() == Long.parseLong((StringFunction.isNotBlank(proxyUserOid) && !"null".equalsIgnoreCase(proxyUserOid)) ? loginUserOid : userOid))
				// T36000018805 - Rel 8.2 End
				{
					previouslyLocked = true;
				} else {
					// issue instrument in use error indicating instrument ID,
					// transaction type, first name and last name of user that has instrument
					// reserved, then skip processing this instrument

					mediatorServices.getErrorManager().issueError(getClass().getName(), TradePortalConstants.TRANSACTION_IN_USE,
							new String[]{e.getInstrumentNumber(), ReferenceDataManager.getRefDataMgr().getDescr("TRANSACTION_TYPE", transactionType, userLocale),
									e.getFirstName(), e.getLastName(), mediatorServices.getResourceManager().getText("TransactionAction.Authorized", TradePortalConstants.TEXT_BUNDLE)});
					continue;

				}
			}

			try {
				// create and load the instrument and call preAuthTransaction
				Instrument instrument = (Instrument) mediatorServices.createServerEJB("Instrument", Long.parseLong(instrumentOid));

				// if preAuthorization is required, call preAuthorizeTransaction that will
				// prepare the instrument for authorization
				if (preAuthorize) {
					if (instrument.preAuthorizeTransaction(transactionOid, userOid, rights)) {
						// put the instrument oid and instrument ID in instrumentHash
						instrumentHash.put(instrumentOid, instrument.getAttribute("complete_instrument_id"));

						// if preauthorize is successful, do not unlock instrument -
						// this will be performed by another mediator after authorizing the transaction
						// otherwise, unlock the instrument
						preAuthorizeSuccess = true;
					}
				} else {
					// if preAuthorization is not required, call authorizeTransaction on
					// the instrument - that will immediately authorize the transaction
					// all errors are saved to the transaction and
					// the status is set to FAILED if authorizeTransaction returns a false

					try {
						HashMap params = new HashMap(); // NSX 10/07/11 - CR-581/640 Rel. 7.1 -
						// ir cnuk113043991 - rearranged to also account for reCertification = multiple
						if (TradePortalConstants.INDICATOR_YES.equals(reCertification)
								&& !CertificateUtility.validateCertificate(inputDoc, mediatorServices)) {
							authenticated = false;
						}

						// only authorize if authenticated (or not required :.))
						boolean trxAuthorized = false;
						if (authenticated) {
							// IR-PAUI060561797 Krishna Begin

							if (InstrumentServices.isCashManagementTypeInstrument(instrument.getAttribute("instrument_type_code"))) {
								processCashMgmtInstrument(inputDoc, instrument, transactionOid, mediatorServices);
							}
							mediatorServices.getCSDB().setCSDBValue(TradePortalConstants.TRAN_ACTION, TradePortalConstants.TRAN_ACTION_AUTHORIZE);

							if (instrument.getErrorManager().getMaxErrorSeverity() >= ErrorManager.ERROR_SEVERITY) {
								continue;
							}

							trxAuthorized = instrument.authorizeTransaction(transactionOid, userOid, authorizeAtParentViewOrSubs, rights, true, params);
						} else { // not authenticated
							// drop out if not ready to authorize.
							// ready to authorize goes through to the status change in !trxAuthorized below
							Transaction transTmpObj = (Transaction) mediatorServices.createServerEJB("Transaction",
									Long.parseLong(transactionOid));
							if (!TransactionStatus.READY_TO_AUTHORIZE.equals(transTmpObj.getAttribute("transaction_status"))) {
								// move on to the next transaction, don't just drop out
								continue;
							}
							// drop out if transaction doesn't require authentication
							// NOTE: this likely should move above to "if (authenticated)"
							// but part of different effort
							if (!requireAuthentication) {
								continue;
							}
						}
						// ir cnuk113043991 end

						// Authorized Transaction are stored in Hashtable to be retrived in
						// AuthorizeTransactionsDeleteMessageMediatorBean
						// in order to delete only the associated mail message of the authorized transactions.
						if (trxAuthorized) {
							authorizedTransactionsOidList.add(transactionOid);
						}

						if (TradePortalConstants.INDICATOR_YES.equals(params.get("RATE_CHANGED"))) {
							// as transaction status is not getting changed and user action is not performed,
							// so don't make entry in transaction history table.
							mediatorServices.getCSDB().setCSDBValue(TradePortalConstants.TRAN_ACTION, "");

							DocumentHandler transactionDoc = (DocumentHandler) params.get("transactionDoc");
							LOG.debug("TransactionDoc: {}", transactionDoc);
							Transaction transTmpObj = (Transaction) mediatorServices.createServerEJB("Transaction", Long.parseLong(transactionOid));
							transTmpObj.populateFromXmlDoc(transactionDoc);
							transTmpObj.save();

							// Refresh transaction so new rate is displayed
							outputDoc.setAttribute("/Refresh/Transaction", "Y");
						}

						// IR-PAUI060561797 Krishna End
						if (!trxAuthorized) {
							// the transaction is saved by itself rather than going through the
							// Instrument.getComponent because the Instrument contains errors -
							// it will not allow a save because the validation code checks
							// to see if it has been saved. Additionally, all saves have to be
							// performed at the highest parent level that is instantiated, otherwise
							// saves will not be committed
							Transaction transObj = (Transaction) mediatorServices.createServerEJB("Transaction", Long.parseLong(transactionOid));
							// get accumulated errors from instrument and save the errors in the instrument

							String tran_status = (String) params.get("transaction_status");

							if (StringFunction.isBlank(tran_status))
								tran_status = transObj.getAttribute("transaction_status");

							if (TransactionStatus.FX_THRESH_EXCEEDED.equals(tran_status)
									|| TransactionStatus.AUTH_PEND_MARKET_RATE.equals(tran_status)) {
								transObj.setAttribute("transaction_status", tran_status);
							} else {

								transObj.setAttribute("authorization_errors", instrument.getErrorManager().getErrorDoc().toString());
								// authorization was not successful and the transaction status has to be set to failed
								transObj.setAttribute("transaction_status", TransactionStatus.AUTHORIZE_FAILED);
							} // NSX 10/07/11 - CR-581/640 Rel. 7.1 -
							transObj.setAttribute("transaction_status_date", DateTimeUtility.getGMTDateTime());
							authorizeFailed = true;

							// IR-LRUK102236760 Narayan Begin
//							Instrument instrumentTmpObj = (Instrument) mediatorServices.createServerEJB("Instrument", Long.parseLong(instrumentOid));
//							String instrumentType = instrumentTmpObj.getAttribute("instrument_type_code");
							String action = mediatorServices.getCSDB().getCSDBValue(TradePortalConstants.TRAN_ACTION);

							// creating Transaction History if action is specified and instrument type should be TBA or FTDP or
							// FTRQ. action type should be 'Authorize'.
							if (StringFunction.isNotBlank(action) && TradePortalConstants.BUTTON_AUTHORIZE.equals(action)) {
								if (authorizeFailed) {
									TransactionHistory tranHistory = (TransactionHistory) mediatorServices.createServerEJB("TransactionHistory");
									tranHistory.newObject();
									tranHistory.setAttribute("action_datetime", DateTimeUtility.getGMTDateTime());
									tranHistory.setAttribute("action", action);
									tranHistory.setAttribute("transaction_status", transObj.getAttribute("transaction_status"));
									tranHistory.setAttribute("transaction_oid", transObj.getAttribute("transaction_oid"));
									String user_oid = mediatorServices.getCSDB().getCSDBValue("user_oid");
									tranHistory.setAttribute("user_oid", user_oid);
									if (StringFunction.isNotBlank(transObj.getAttribute("panel_auth_group_oid"))) {
										User user = (User) mediatorServices.createServerEJB("User", Long.parseLong(user_oid));
										// Nar IR-T36000021246 01-Oct-2013 ADD Begin
										// modify code to set sub_panel_auth_code if parent is working on behalf of subsidiary
										// through subsiadiary
										// access tab and use subsidiary profile setting is selected.
										String userPanelAuthLevel;
										if (TradePortalConstants.INDICATOR_NO.equals(authorizeAtParentViewOrSubs)
												&& TradePortalConstants.INDICATOR_YES.equals(user.getAttribute("use_subsidiary_profile"))) {
											userPanelAuthLevel = user.getAttribute("sub_panel_authority_code");
										} else {
											userPanelAuthLevel = user.getAttribute("panel_authority_code");
										}
										// Nar IR-T36000021246 01-Oct-2013 ADD END
										tranHistory.setAttribute("panel_authority_code", userPanelAuthLevel);
									}
									tranHistory.save(false);
								}
							}

							// IR-LRUK102236760 Narayan End
			    		  try {
							transObj.save();

							// cnuk113043991 - indicate the transaction needs to be refreshed
							outputDoc.setAttribute("/Refresh/Transaction", "Y");
			    		} catch(Exception ex) {
						   LOG.error("Exception occured saving transaction in AuthorizeTransactionsMediator: ", ex);
			    		}
                      }
					} catch (AmsException | RemoteException | NumberFormatException ex) {
						LOG.error("Exception occured in AuthorizeTransactionsMediator: ", ex);

					}
				}
			} finally {
				// if previously locked or if preAuthorize is successful,
				// do not unlock the instrument
				if (!previouslyLocked && !preAuthorizeSuccess) {
					try {
						LockingManager.unlockBusinessObjectByInstrument(Long.parseLong(instrumentOid), true);
					} catch (InstrumentLockException ex) {
						// this should never happen
						LOG.error("Exception occured Unlocking Instrument: ", ex);
					}
				}
			}

			// NSX - T36000022887, collect issued errors and cleanup EJBs so EJB pool is not exhausted
			// when large number of transactions are authorized at one time
			mediatorServices.cleanUpEjbObjList();
		}



		return outputDoc;

	}



	/* KMehta IR-T36000035515 Rel 9300 on 03-Apr-2015 ADD - Begin */
	private boolean isDuplicatesExist(List<String> listContainingDuplicates) {
		final Set<String> tempSet = new HashSet<>();
		for (String item : listContainingDuplicates) {
			if (!tempSet.add(item)) {
				return true;
			}
		}
		return false;
	}
	/* KMehta IR-T36000035515 Rel 9300 on 03-Apr-2015 ADD - End */

	protected void processCashMgmtInstrument(DocumentHandler inputDoc, Instrument instrument, String transactionOid,
			MediatorServices mediatorServices) throws RemoteException, AmsException {
		String buttonPressed = inputDoc.getAttribute("/Update/ButtonPressed");
		if (TradePortalConstants.BUTTON_CONFIRM_FX.equals(buttonPressed)) {
			DocumentHandler fxResp = inputDoc.getComponent("/FXResp");
			if (fxResp == null) {
				mediatorServices.getErrorManager().issueError(getClass().getName(), TradePortalConstants.NO_ITEM_SELECTED,
						mediatorServices.getResourceManager().getText("TransactionAction.Authorize", TradePortalConstants.TEXT_BUNDLE));
				throw new AmsException("error occured, FXDeal response in not present...");

			}
			updateFXDealData(instrument, transactionOid, fxResp, mediatorServices);
		} else {
			updateFXContractData(inputDoc, instrument, mediatorServices); 
		}
	}

	/**
	 * @param transactionOid
	 * @param fxResp
	 * @param mediatorServices
	 * @throws NumberFormatException
	 * @throws RemoteException
	 * @throws AmsException
	 */
	protected void updateFXDealData(Instrument instrument, String transactionOid, DocumentHandler fxResp,
			MediatorServices mediatorServices) throws NumberFormatException, RemoteException, AmsException {

		Transaction transObj = (Transaction) instrument.getComponentHandle("TransactionList", Long.parseLong(transactionOid));
		String action = transObj.getClientServerDataBridge().getCSDBValue(TradePortalConstants.TRAN_ACTION);
		transObj.getClientServerDataBridge().setCSDBValue(TradePortalConstants.TRAN_ACTION, "");
		Terms terms = (Terms) (transObj.getComponentHandle("CustomerEnteredTerms"));

		terms.setAttribute("covered_by_fec_number", fxResp.getAttribute("/ForEXDealId"));
		terms.setAttribute("fec_rate", fxResp.getAttribute("/ExchRate"));
		terms.setAttribute("fx_online_deal_confirm_date", fxResp.getAttribute("/DealDateTime"));
		terms.setAttribute("transfer_fx_rate", fxResp.getAttribute("/ExchRate"));
		terms.setAttribute("equivalent_exch_amount", fxResp.getAttribute("/EquivalentAmt"));
		terms.setAttribute("equivalent_exch_amt_ccy", fxResp.getAttribute("/EquivalentAmtCCY"));
		// terms.setAttribute("display_fx_rate_method", getCalculationMethod(terms)); //IR# DAUM011860866 Rel. 7.1.0 - 01/19/12 -
		terms.setAttribute("display_fx_rate_method", fxResp.getAttribute("/CalcMethod")); // IR# DAUM011860866 Rel. 7.1.0 - 01/19/12
																							// -

		if (transObj.save() < 0) {
			mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.FX_GET_RATE_GENERAL_ERR);
			throw new AmsException("error occured updating transaction...");
		} else {
			// log deal booked warning
			mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.FX_DEAL_SUCCESSFULY_BOOKED);
		}
		transObj.getClientServerDataBridge().setCSDBValue(TradePortalConstants.TRAN_ACTION, action);
	}

	


	public boolean updateFXContractData(DocumentHandler inputDoc, Instrument instrument, MediatorServices mediatorServices)
			throws RemoteException, AmsException { // NSX IR# PSUL112862587 Rel.7.1.0 12/09/11

		Transaction tran = (Transaction) instrument.getComponentHandle("TransactionList", Long.parseLong(instrument.getAttribute("original_transaction_oid")));
		String tran_status = tran.getAttribute("transaction_status");
		if (TransactionStatus.FX_THRESH_EXCEEDED.equals(tran_status) || TransactionStatus.AUTH_PEND_MARKET_RATE.equals(tran_status)) {
			Terms terms = (Terms) (tran.getComponentHandle("CustomerEnteredTerms"));
			if (StringFunction.isBlank(terms.getAttribute("covered_by_fec_number"))
					|| StringFunction.isBlank(terms.getAttribute("fec_rate"))) {
				if (StringFunction.isNotBlank(inputDoc.getAttribute("/Terms/covered_by_fec_number"))
						|| StringFunction.isNotBlank(inputDoc.getAttribute("/Terms/fec_rate"))) {
					terms.setAttribute("covered_by_fec_number", inputDoc.getAttribute("/Terms/covered_by_fec_number"));
					terms.setAttribute("fec_rate", inputDoc.getAttribute("/Terms/fec_rate"));

					calcAmount(inputDoc, instrument, terms, mediatorServices); // NSX IR# PSUL112862587 Rel.7.1.0 12/09/11
				}
			}
		}

		return true;
	}


	/**
	 * @param inputDoc
	 * @param instrument
	 * @param terms
	 * @param mediatorServices
	 * @throws RemoteException
	 * @throws AmsException
	 */
	protected void calcAmount(DocumentHandler inputDoc, Instrument instrument, Terms terms, MediatorServices mediatorServices)
			throws RemoteException, AmsException {
		String rate = terms.getAttribute("fec_rate");
		String transferCurrency = terms.getAttribute("amount_currency_code");
		String amount = terms.getAttribute("amount");
		String ex_ind = null, ex_currency = null;
		BigDecimal ex_amount = null;
		Account fromAccount = (Account) mediatorServices.createServerEJB("Account", Long.parseLong(terms.getAttribute("debit_account_oid")));
		String fromCurrency = fromAccount.getAttribute("currency");

		String baseCurrency = fromAccount.getBaseCurrencyOfAccount();
		String accOwnerOid = fromAccount.getOwnerOfAccount();
		Vector outMDInd = new Vector(1, 1);


		if (StringFunction.isNotBlank(rate)) {
			int dec;
			String rateType;
			if (InstrumentType.XFER_BET_ACCTS.equals(inputDoc.getAttribute("/Instrument/instrument_type_code"))
					&& fromCurrency.equalsIgnoreCase(transferCurrency)) {
				Account toAccount = (Account) mediatorServices.createServerEJB("Account", Long.parseLong(terms.getAttribute("credit_account_oid")));
				String toCurrency = toAccount.getAttribute("currency");

				ex_currency = toCurrency;
				if (!fromCurrency.equals(baseCurrency) && !toCurrency.equals(baseCurrency) && !fromCurrency.equals(toCurrency)) {

					ex_ind = instrument.crossRateCriterionProcessing(terms, transferCurrency, toCurrency, rate);
					dec = TPCurrencyUtility.getDecimalPrecision(toCurrency);
					if (TradePortalConstants.MULTIPLY.equals(ex_ind))
						ex_amount = (new BigDecimal(amount)).multiply(new BigDecimal(rate)).setScale(dec, BigDecimal.ROUND_HALF_UP);
					else if (TradePortalConstants.DIVIDE.equals(ex_ind))
						ex_amount = (new BigDecimal(amount)).divide(new BigDecimal(rate), dec, BigDecimal.ROUND_HALF_UP);
				} else {

					if (fromCurrency.equals(baseCurrency))
						rateType = TradePortalConstants.USE_SELL_RATE;
					else
						rateType = TradePortalConstants.USE_BUY_RATE;
					if (ex_currency.equals(baseCurrency)) {
						ex_amount = instrument.getAmountInBaseCurrency(transferCurrency, amount, baseCurrency, accOwnerOid, rateType, new BigDecimal(rate), outMDInd);
					} else {
						ex_amount = instrument.getAmounInFXCurrency(ex_currency, baseCurrency, amount, accOwnerOid, rateType, new BigDecimal(rate), outMDInd);

					}
					if (!outMDInd.isEmpty() && (outMDInd.size() >= 0))
						ex_ind = (String) outMDInd.get(0);
				}

			} else {

				ex_currency = fromCurrency;
				if (transferCurrency.equals(fromCurrency)) {
					// update with nothing
				} else if (transferCurrency.equals(baseCurrency)) {
					rateType = TradePortalConstants.USE_BUY_RATE;
					ex_amount = instrument.getAmounInFXCurrency(fromCurrency, baseCurrency, amount, accOwnerOid, rateType, new BigDecimal(rate), outMDInd);

					if (!outMDInd.isEmpty() && (outMDInd.size() >= 0))
						ex_ind = (String) outMDInd.get(0);
				} else if (fromCurrency.equals(baseCurrency)) {
					rateType = TradePortalConstants.USE_SELL_RATE;
					ex_amount = instrument.getAmountInBaseCurrency(transferCurrency, amount, baseCurrency, accOwnerOid, rateType, new BigDecimal(rate), outMDInd);

					if (!outMDInd.isEmpty() && (outMDInd.size() >= 0))
						ex_ind = (String) outMDInd.get(0);
				} else {
					// NSX IR# RUUL122052305 - 07/01/12 - End -
					ex_ind = instrument.crossRateCriterionProcessing(terms, transferCurrency, fromCurrency, rate);
					dec = TPCurrencyUtility.getDecimalPrecision(fromCurrency);
					if (TradePortalConstants.MULTIPLY.equals(ex_ind))
						ex_amount = (new BigDecimal(amount)).multiply(new BigDecimal(rate)).setScale(dec, BigDecimal.ROUND_HALF_UP);
					else if (TradePortalConstants.DIVIDE.equals(ex_ind))
						ex_amount = (new BigDecimal(amount)).divide(new BigDecimal(rate), dec, BigDecimal.ROUND_HALF_UP);
				}
			}
		}

		String s_ex_amount = null;
		if (ex_amount == null) {
			rate = null;
			s_ex_amount = null;
			ex_currency = null;
			ex_ind = null;
		} else {
			s_ex_amount = ex_amount.toString();
		}
		terms.setAttribute("equivalent_exch_amount", s_ex_amount);
		terms.setAttribute("transfer_fx_rate", rate);
		terms.setAttribute("equivalent_exch_amt_ccy", ex_currency);
		terms.setAttribute("display_fx_rate_method", ex_ind);

	}


	


	/**
	 * Does the transaction require authentication?
	 */
	private boolean requireTransactionAuthentication(String instrumentOid, String transactionOid, String transactionType,
			MediatorServices mediatorServices) throws RemoteException, AmsException {
		Instrument instrument = (Instrument) mediatorServices.createServerEJB("Instrument", Long.parseLong(instrumentOid));
		String instrumentType = instrument.getAttribute("instrument_type_code");
		long clientBankOid = instrument.getAttributeLong("client_bank_oid");
		ClientBank clientBank = (ClientBank) mediatorServices.createServerEJB("ClientBank");
		clientBank.getDataFromReferenceCache(clientBankOid);
		String requireTranAuth = clientBank.getAttribute("require_tran_auth");
		return InstrumentAuthentication.requireTransactionAuthentication(requireTranAuth, instrumentType + "_" + transactionType);
	}

}
