package com.ams.tradeportal.mediator;

import java.rmi.RemoteException;
import java.util.List;
import java.util.Map;

import com.ams.tradeportal.busobj.ClientBank;
import com.ams.tradeportal.busobj.CorporateOrganization;
import com.amsinc.ecsg.frame.AmsException;


/*
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface AccountsQueryMediator extends com.amsinc.ecsg.frame.Mediator
{
	public Map queryAcctBalance(String user_oid, String transactionOID, ClientBank clientBank, CorporateOrganization corpOrg, List accounts) throws RemoteException, AmsException ;
}