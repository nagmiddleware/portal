package com.ams.tradeportal.mediator;

import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.busobj.ArMatchingRule;
import com.ams.tradeportal.busobj.CorporateOrganization;
import com.ams.tradeportal.busobj.Instrument;
import com.ams.tradeportal.busobj.InvoicesSummaryData;
import com.ams.tradeportal.busobj.ShipmentTerms;
import com.ams.tradeportal.busobj.Terms;
import com.ams.tradeportal.busobj.Transaction;
import com.ams.tradeportal.busobj.util.InstrumentServices;
import com.ams.tradeportal.busobj.util.InvoiceUtility;
import com.ams.tradeportal.busobj.util.POLineItemUtility;
import com.ams.tradeportal.common.TPDateTimeUtility;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.ComponentList;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.frame.MediatorBean;
import com.amsinc.ecsg.frame.MediatorServices;
import com.amsinc.ecsg.frame.ReferenceDataManager;

/**
 * This class is used for removing one or more PO line items from an Import LC
 * Issue or Amend transaction. Once the PO line items have been removed from a
 * transaction, the method will update the transaction's amount, last shipment
 * date, expiry date, and PO line items description accordingly.
 *
 */
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.StringFunction;

/*
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class RemoveTransInvoicesMediatorBean extends MediatorBean {
	private static final Logger LOG = LoggerFactory.getLogger(RemoveTransInvoicesMediatorBean.class);
	private transient String userLocale = null;
	// The following variables have been declared transient because the mediator
	// is stateless and they are initialized in only one place.
	private transient MediatorServices mediatorServices = null;
	private transient Transaction transaction = null;
	private transient Instrument instrument = null;
	private transient Vector invoiceOidsList = null;
	private transient String uploadDefinitionOid = null;
	private transient Terms terms = null;
	private transient int numberOfInvs = 0;
	private transient long shipmentTermsOid = 0;
	private transient String userOID = null;

	/**
	 * This method performs deletion of PO The format expected from the input doc is:
	 *
	 * <POLineItemList> <POLineItem ID="0"><poLineItemOid>1000001</POLineItem>
	 * <POLineItem ID="1"><poLineItemOid>1000002</POLineItem> <POLineItem
	 * ID="2"><poLineItemOid>1000003</POLineItem> <POLineItem
	 * ID="3"><poLineItemOid>1000004</POLineItem> <POLineItem
	 * ID="6"><poLineItemOid>1000001</POLineItem> </POLineItemList> This is a
	 * non-transactional mediator that executes transactional methods in business objects
	 *
	 * @param inputDoc
	 *            The input XML document (&lt;In&gt; section of overall Mediator XML document)
	 * @param outputDoc
	 *            The output XML document (&lt;Out&gt; section of overall Mediator XML document)
	 * @param mediatorServices
	 *            Associated class that contains lots of the auxillary functionality for a mediator.
	 */
	public DocumentHandler execute(DocumentHandler inputDoc, DocumentHandler outputDoc, MediatorServices newMediatorServices)
			throws RemoteException, AmsException {
		DocumentHandler poLineItemDoc = null;

		mediatorServices = newMediatorServices;
		Vector invoicesList = inputDoc.getFragments("/InvoicesList/Invoice");
		long transactionOid = inputDoc.getAttributeLong("/Transaction/transactionOid");
		shipmentTermsOid = inputDoc.getAttributeLong("/Terms/ShipmentTermsList/shipment_oid");
		// Based on the button clicked, process an appropriate action.
		String buttonPressed = inputDoc.getAttribute("/Update/ButtonPressed");
		if (!TradePortalConstants.BUTTON_UNASSIGN_TRANS_INVOICES.equals(buttonPressed)) {
			return outputDoc;
		}

		userOID = inputDoc.getAttribute("/userOID");
		mediatorServices.debug("The button pressed is " + buttonPressed);

		numberOfInvs = invoicesList.size();

		userLocale = mediatorServices.getCSDB().getLocaleName();

		// if the user locale not specified default to en_US
		if ((userLocale == null) || (userLocale.equals(""))) {
			userLocale = "en_US";
		}

		// If nothing was selected by the user, issue an error indicating this  and return
		if (numberOfInvs <= 0) {
			String substitutionParm = mediatorServices.getResourceManager().getText("StructuredPOAction.Remove",
					TradePortalConstants.TEXT_BUNDLE);

			mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.NO_ITEM_SELECTED, substitutionParm);

			return outputDoc;
		}

		// Get a list of all the PO oids that were selected
		String multipleInvoices = TradePortalConstants.INDICATOR_NO;
		invoiceOidsList = InvoiceUtility.getInvoiceOids(invoicesList, multipleInvoices, TradePortalConstants.PO_STRUCT_ADD);
		if (multipleInvoices.equals(TradePortalConstants.INDICATOR_YES))
			numberOfInvs = invoiceOidsList.size();
		try {
			transaction = (Transaction) mediatorServices.createServerEJB("Transaction", transactionOid);

			// Needs to instantiate instrument object for updating amendment transaction.
			//String transactionType = transaction.getAttribute("transaction_type_code");
			long instrumentOid = transaction.getAttributeLong("instrument_oid");
			instrument = (Instrument) mediatorServices.createServerEJB("Instrument", instrumentOid);

			removeInvoices(outputDoc, inputDoc);
			transaction.save(false);
			instrument.save(false);
		} catch (Exception e) {
			LOG.error("Exception occured in RemoveStructuredPOMediator: " , e);
		}

		if (InstrumentServices.isBlank(inputDoc.getAttribute("/Terms/ShipmentTermsList/goods_description"))) {
			outputDoc.setAttribute("/ChangePOs/goods_description", TradePortalConstants.getPropertyValue("TextResources", "StructuredPurchaseOrders.defaultGoodsDescription", null));
		} else {
			outputDoc.setAttribute("/ChangePOs/goods_description", inputDoc.getAttribute("/Terms/ShipmentTermsList/goods_description"));
		}

		outputDoc.setAttribute("/ChangePOs/isStructuredPO", TradePortalConstants.INDICATOR_YES);
		return outputDoc;
	}

	/**
	 * This method removes the PO selected by the user from the transaction that he/she is currently viewing. If the PO were
	 * uploaded, they are disassociated from the transaction. If the PO line items were manually entered, they are deleted. After
	 * all PO have been saved, the amount, date, and PO description fields are all re-derived to reflect the removed item values.
	 *
	 * @param DocumentHandler
	 *            - the mediator's output doc
	 * @exception com.amsinc.ecsg.frame.AmsException
	 * @exception java.rmi.RemoteException
	 * @return void
	 */
	private void removeInvoices(DocumentHandler outputDoc, DocumentHandler inputDoc) throws AmsException, RemoteException {
		Vector associatedInvList = new Vector();
		Vector associatedInvNumList = new Vector();
		String financeType = inputDoc.getAttribute("/Terms/finance_type");
		String corpOrgOid = instrument.getAttribute("corp_org_oid");
		String importIndicator = inputDoc.getAttribute("/Instrument/import_indicator");
		String transactionOid = transaction.getAttribute("transaction_oid");

		// Get a list of the POs already associated with this instrument
		String alreadyAssociatedSql = "select upload_invoice_oid,invoice_id from invoices_summary_data where a_transaction_oid = ? ";
		DocumentHandler resultSet = DatabaseQueryBean.getXmlResultSet(alreadyAssociatedSql, true, new Object[] { transactionOid });

		if (resultSet == null)
			resultSet = new DocumentHandler();

		List<DocumentHandler> alreadyAssociatedList = resultSet.getFragmentsList("/ResultSetRecord");
		for (DocumentHandler inv : alreadyAssociatedList) {
			String poOid = inv.getAttribute("/UPLOAD_INVOICE_OID");
			associatedInvList.addElement(poOid);
			associatedInvNumList.add(inv.getAttribute("/INVOICE_ID"));
		}

		String invType = null;
		// Update the assigned to transaction indicators for each PO line item being added to the transaction
		String invClassification = null;
		for (int i = 0; i < numberOfInvs; i++) {
			long invoiceOid = Long.parseLong((String)invoiceOidsList.elementAt(i));
			InvoicesSummaryData invSummaryData = (InvoicesSummaryData) mediatorServices.createServerEJB("InvoicesSummaryData", invoiceOid);
			if (i == 0)
				invClassification = invSummaryData.getAttribute("invoice_classification");
			uploadDefinitionOid = invSummaryData.getAttribute("upload_definition_oid");
			invType = invSummaryData.getAttribute("invoice_type");
			String tpName = invSummaryData.getTpRuleName();

			invSummaryData.setAttribute("tp_rule_name", tpName);

			// Manually entered POs should be deleted and uploaded POs are just un-associated with the transaction
			invSummaryData.setAttribute("transaction_oid", null);
			invSummaryData.setAttribute("instrument_oid", null);
			invSummaryData.setAttribute("terms_oid", null);
			invSummaryData.setAttribute("user_oid", userOID);
			invSummaryData.setAttribute("action", TradePortalConstants.PO_ACTION_UNLINK);
			invSummaryData.setAttribute("status", TradePortalConstants.PO_STATUS_UNASSIGNED);
			invSummaryData.setAttribute("invoice_status", TradePortalConstants.PO_STATUS_UNASSIGNED);
			// IR T36000031466 RPasupulati removing fininace Amount Start
			invSummaryData.setAttribute("invoice_finance_amount", null);
			// IR T36000031466 RPasupulati removing fininace Amount End
			associatedInvNumList.remove(invSummaryData.getAttribute("invoice_id"));
			invSummaryData.save();
			associatedInvList.remove(invoiceOidsList.get(i));

			invSummaryData = null;
		}

		// Get the terms associated with the current transaction
		terms = (Terms) transaction.getComponentHandle("CustomerEnteredTerms");
		ComponentList shipmentTermsList = (ComponentList) terms.getComponentHandle("ShipmentTermsList");
		int numShipments = shipmentTermsList.getObjectCount();

		ShipmentTerms shipmentTerms = (ShipmentTerms) shipmentTermsList.getComponentObject(shipmentTermsOid);

		// Derive the amounts (Transaction.amount etc), dates (Terms.expirty_date, ShipmentTerms.latest_shipment_date etc) and  ShipmentTerms.po_line_item.
		// Note all the PO have been saved into database by now.

		InvoiceUtility.deriveTransactionCurrencyAndAmountsFromInvoice(transaction, terms, associatedInvList, instrument, associatedInvNumList, invType);

		// Issue an informational message indicating to the user that the selected PO have been removed successfully and save the transaction
		if (TradePortalConstants.TRADE_LOAN_REC.equals(financeType)) {
			CorporateOrganization corpOrg = (CorporateOrganization) mediatorServices.createServerEJB("CorporateOrganization");
			corpOrg.getData(Long.parseLong(corpOrgOid));
			String attributeValue = corpOrg.getAttribute("inv_percent_to_fin_trade_loan");

			BigDecimal finPCTValue = BigDecimal.ZERO;
			if ((attributeValue != null) && (!attributeValue.equals(""))) {
				finPCTValue = (new BigDecimal(attributeValue)).divide(new BigDecimal(100.00));
			}
			if (finPCTValue.compareTo(BigDecimal.ZERO) != 0) {
				BigDecimal termAmount = terms.getAttributeDecimal("amount");
				BigDecimal tempAmount = finPCTValue.multiply(termAmount);
				String trnxCurrencyCode = transaction.getAttribute("copy_of_currency_code");
				int numberOfCurrencyPlaces = 2;

				if (trnxCurrencyCode != null && !trnxCurrencyCode.equals("")) {
					numberOfCurrencyPlaces = Integer.parseInt(ReferenceDataManager.getRefDataMgr()
							.getAdditionalValue(TradePortalConstants.CURRENCY_CODE, trnxCurrencyCode));
				}
				tempAmount = tempAmount.setScale(numberOfCurrencyPlaces, BigDecimal.ROUND_HALF_UP);
				terms.setAttribute("amount", String.valueOf(tempAmount));
				// Set the counterparty for the instrument and some "copy of" fields
				transaction.setAttribute("copy_of_amount", terms.getAttribute("amount"));
				transaction.setAttribute("instrument_amount", terms.getAttribute("amount"));
				instrument.setAttribute("copy_of_instrument_amount", transaction.getAttribute("instrument_amount"));

			}
		}
		String requiredOid = "upload_invoice_oid";
		String invoiceOidsSql = POLineItemUtility.getPOLineItemOidsSql(associatedInvList, requiredOid);

		if (TradePortalConstants.INDICATOR_T.equals(importIndicator) && TradePortalConstants.TRADE_LOAN_PAY.equals(financeType)
				&& TradePortalConstants.INVOICE_TYPE_PAY_MGMT.equals(invClassification)) {
			if (associatedInvList.size() < 1) {
				try {
					terms.setAttribute("payment_method", null);
					terms.setAttribute("bank_charges_type", null);
					terms.deleteComponent("FirstTermsParty");
					terms.deleteComponent("ThirdTermsParty");
				} catch (Exception e) {
					// log and ignore
					e.printStackTrace();
				}
			} else {
				StringBuilder payMethodSQL = new StringBuilder("select distinct(PAY_METHOD) PAYMETHOD from invoices_summary_data where ");
				payMethodSQL.append(invoiceOidsSql);
				DocumentHandler resultDoc = DatabaseQueryBean.getXmlResultSet(payMethodSQL.toString(), false, new ArrayList<Object>());
				if (resultDoc != null) {
					String payMethod = resultDoc.getAttribute("/ResultSetRecord(0)/PAYMETHOD");
					if (StringFunction.isNotBlank(payMethod)) {
						terms.setAttribute("payment_method", payMethod);
						int singleBeneCount = InvoiceUtility.isSingleBeneficairyInvoice(invoiceOidsSql);
						if (singleBeneCount == 1) {
							terms.setAttribute("loan_proceeds_credit_type", TradePortalConstants.CREDIT_BEN_ACCT);
							InvoiceUtility.populateTerms(terms, (String) associatedInvList.get(0), associatedInvList.size(), mediatorServices);
						} else if (singleBeneCount > 1) {
							terms.setAttribute("loan_proceeds_credit_type", TradePortalConstants.CREDIT_MULTI_BEN_ACCT);
							try {
								terms.setAttribute("payment_method", null);
								terms.setAttribute("bank_charges_type", null);
								terms.deleteComponent("FirstTermsParty");
								terms.deleteComponent("ThirdTermsParty");
							} catch (Exception e) {
								// log and ignore

								e.printStackTrace();
							}

						}

					}

				}
			}
		}

		if (TradePortalConstants.INDICATOR_NO.equals(importIndicator) || TradePortalConstants.INDICATOR_X.equals(importIndicator)) {
			if (associatedInvList.size() < 1) {
				terms.setAttribute("loan_terms_fixed_maturity_dt", null);
				terms.setAttribute("loan_terms_type", null);
				terms.deleteComponent("FirstTermsParty");
				terms.deleteComponent("ThirdTermsParty");
			} else {
				StringBuilder dueDateSQL = new StringBuilder(
						"select distinct(case when payment_date is null then due_date else payment_date end ) DUEDATE "
								+ " from invoices_summary_data where ");
				dueDateSQL.append(invoiceOidsSql);
				DocumentHandler resultDoc = DatabaseQueryBean.getXmlResultSet(dueDateSQL.toString(), false, new ArrayList<Object>());
				if (resultDoc != null) {
					String invoiceDueDate = resultDoc.getAttribute("/ResultSetRecord(0)/DUEDATE");

					if (StringFunction.isNotBlank(invoiceDueDate)) {

						try {
							SimpleDateFormat iso = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
							SimpleDateFormat jPylon = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");

							Date date = jPylon.parse(invoiceDueDate);

							terms.setAttribute("loan_terms_fixed_maturity_dt", TPDateTimeUtility.convertISODateToJPylonDate(iso.format(date)));
							terms.setAttribute("loan_terms_type", TradePortalConstants.LOAN_FIXED_MATURITY);

						} catch (Exception e) {
							e.printStackTrace();
						}

					}
				}
			}
		}
		outputDoc.setAttribute("/ChangePOs/fromStrucPO", "Y");
		mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.REMOVE_INVOICE_SUCCESS,
				String.valueOf(numberOfInvs));

	}

	

}
