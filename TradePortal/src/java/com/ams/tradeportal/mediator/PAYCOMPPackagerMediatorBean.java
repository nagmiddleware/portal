package com.ams.tradeportal.mediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.RemoteException;
import java.util.Date;
import java.util.Vector;

import com.ams.tradeportal.busobj.CorporateOrganization;
import com.ams.tradeportal.busobj.DomesticPayment;
import com.ams.tradeportal.busobj.Instrument;
import com.ams.tradeportal.busobj.PaymentFileUpload;
import com.ams.tradeportal.busobj.Transaction;
import com.ams.tradeportal.busobj.util.InstrumentServices;
import com.ams.tradeportal.common.PGPEncryptDecrypt;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.ComponentList;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.frame.MediatorBean;
import com.amsinc.ecsg.frame.MediatorServices;
import com.amsinc.ecsg.util.DateTimeUtility;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.GMTUtility;
import com.amsinc.ecsg.util.StringFunction;

/**
 * Manages PAYCOMP Message Packaging...
 * 
 * Copyright � 2001 American Management Systems, Incorporated All rights
 * reserved
 */

public class PAYCOMPPackagerMediatorBean extends MediatorBean {
private static final Logger LOG = LoggerFactory.getLogger(PAYCOMPPackagerMediatorBean.class);

	private Instrument _instrument = null;
	private Transaction _transaction = null;
	private CorporateOrganization _corpOrg = null;
	private PaymentFileUpload _paymentFileUpload = null;

	/**
	 * Entrance point into the PAYCOMP Packager.
	 * 
	 * @param inputDoc
	 *            The input XML document (&lt;In&gt; section of overall Mediator
	 *            XML document)
	 * @param outputDoc
	 *            The output XML document (&lt;Out&gt; section of overall
	 *            Mediator XML document)
	 * @param mediatorServices
	 *            Associated class that contains lots of the auxillary
	 *            functionality for a mediator.
	 */
	public DocumentHandler execute(DocumentHandler inputDoc, DocumentHandler outputDoc,
			MediatorServices mediatorServices) throws RemoteException, AmsException {
		initialize(inputDoc, mediatorServices);

		DocumentHandler header = packageHeader(inputDoc);
		DocumentHandler subHeader = packageSubHeader(inputDoc);
		DocumentHandler bodyDoc = packageDomesticPayments(inputDoc);
		
		outputDoc.addComponent("/Proponix/Header/", header);
		outputDoc.addComponent("/Proponix/SubHeader/", subHeader);
		
		String PGPUserID = "PGP_GXS_UserID";
		if(TradePortalConstants.DESTINATION_ID_FLA.equals(_transaction.getAttribute("h2h_source")))
		{
			PGPUserID = "PGP_FLA_UserID";
		}
		String pgpUserId = TradePortalConstants.getPropertyValue("TradePortal", PGPUserID, null); //Nar CR694A Rel9.0		
		String body = bodyDoc.toString(); 
		body = body.substring(9, body.length() - 10);
		String encBody = PGPEncryptDecrypt.encryptAndSign(body, true, pgpUserId);	
		outputDoc.setAttribute("/Proponix/Body/MessageDetail", encBody);
		
		return outputDoc;
	}
	


	private void initialize(DocumentHandler inputDoc, MediatorServices ms)
			throws NumberFormatException, RemoteException, AmsException {
		String instrumentOid = inputDoc.getAttribute("/instrument_oid");
		String transactionOid = inputDoc.getAttribute("/transaction_oid");

		// Get The Required Objects
		_instrument = (Instrument) ms.createServerEJB("Instrument",Long.parseLong(instrumentOid));
		_transaction = (Transaction) _instrument.getComponentHandle("TransactionList",
				Long.parseLong(transactionOid));
		String corpOrgOid = _instrument.getAttribute("corp_org_oid");
		_corpOrg = (CorporateOrganization) ms.createServerEJB(
				"CorporateOrganization", Long.parseLong(corpOrgOid));

		_paymentFileUpload = getPaymentFileUpload(ms, instrumentOid);

	}

	private PaymentFileUpload getPaymentFileUpload(MediatorServices mediatorServices,
			String instrumentOid) throws AmsException, NumberFormatException, RemoteException {
		String sql = "SELECT PAYMENT_FILE_UPLOAD_OID FROM PAYMENT_FILE_UPLOAD WHERE A_INSTRUMENT_OID = ? ";

		DocumentHandler resultXML = DatabaseQueryBean.getXmlResultSet(sql,false, new Object[]{instrumentOid});
		if (resultXML == null) {
			mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,
					TradePortalConstants.NO_MATCHING_VALUE_FOUND, "PaymentFileUpload");
			return null;
		}

		// make sure that we don't have more than one result
		Vector v = resultXML.getFragments("/ResultSetRecord(0)");
		if (v.size() > 1) {
			mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,
					TradePortalConstants.MORE_THAN_ONE_VALUE_FOUND, "PaymentFileUpload");
			return null;
		}

		String paymentFileUploadOid = resultXML
				.getAttribute("/ResultSetRecord(0)/PAYMENT_FILE_UPLOAD_OID");
		if (paymentFileUploadOid == null || paymentFileUploadOid.equals("")) {
			mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,
					TradePortalConstants.NO_MATCHING_VALUE_FOUND, "PaymentFileUpload");
			return null;
		}

		_paymentFileUpload = (PaymentFileUpload) mediatorServices.createServerEJB(
				"PaymentFileUpload", Long.parseLong(paymentFileUploadOid));
		return _paymentFileUpload;

	}

	private DocumentHandler packageSubHeader(DocumentHandler inputDoc) throws AmsException,
			RemoteException {
		DocumentHandler handler = new DocumentHandler();
		handler.setAttribute("/OverallPaymentStatus",_transaction.getAttribute("transaction_status"));
		handler.setAttribute("/CustomerID", _corpOrg.getAttribute("Proponix_customer_id"));
		handler.setAttribute("/GXSRef", _transaction.getAttribute("gxs_ref"));		
		String custFileRef = _transaction.getAttribute("customer_file_ref");
		if (StringFunction.isNotBlank(custFileRef)) {
			handler.setAttribute("/CustomerFileRef", custFileRef);
		}
		handler.setAttribute("/FileName", _paymentFileUpload.getAttribute("payment_file_name"));
		handler.setAttribute("/InstrumentID", _instrument.getAttribute("complete_instrument_id"));

		return handler;
	}

	private DocumentHandler packageHeader(DocumentHandler inputDoc) throws AmsException, RemoteException {

		Date gmtDate = GMTUtility.getGMTDateTime();
		String dateSent = DateTimeUtility.formatDate(gmtDate, "MM/dd/yyyy");
		String timeSent = DateTimeUtility.formatDate(gmtDate, "HH:mm:ss");

		DocumentHandler handler = new DocumentHandler();
		handler.setAttribute("/DestinationID", _transaction.getAttribute("h2h_source")); // Nar CR694A Rel9.0
		handler.setAttribute("/SenderID", TradePortalConstants.SENDER_ID);
		handler.setAttribute("/ClientBank", TradePortalConstants.DESTINATION_ID_ANZ);
		handler.setAttribute("/MessageType", inputDoc.getAttribute("/msg_type"));
		handler.setAttribute("/DateSent", dateSent);
		handler.setAttribute("/TimeSent", timeSent);
		handler.setAttribute("/MessageID", inputDoc.getAttribute("/message_id"));
		return handler;
	}

	private DocumentHandler packageDomesticPayments(DocumentHandler inputDoc) throws RemoteException, AmsException{
		DocumentHandler dps = new DocumentHandler();
	
		ComponentList domesticPayResults = (ComponentList) _transaction.getComponentHandle("DomesticPaymentList");

		for (int i = 0; i < domesticPayResults.getObjectCount(); i++) {
			domesticPayResults.scrollToObjectByIndex(i);
			DomesticPayment dp = (DomesticPayment) domesticPayResults.getBusinessObject();
			DocumentHandler handler = packageDomesticPayment(dp);

			dps.addComponent("/Beneficiary(" + i + ")", handler);
		}
		return dps;
			
	}
	
	private DocumentHandler packageDomesticPayment(DomesticPayment domesticPayment) throws RemoteException, AmsException {
		DocumentHandler handler = new DocumentHandler();

		handler.setAttribute("/BeneName",domesticPayment.getAttribute("payee_name"));
		handler.setAttribute("/Currency",domesticPayment.getAttribute("amount_currency_code"));
		handler.setAttribute("/Amount",domesticPayment.getAttribute("amount"));
		handler.setAttribute("/BeneAccount",domesticPayment.getAttribute("payee_account_number"));
		handler.setAttribute("/BeneSeqID",_instrument.getAttribute("complete_instrument_id") + "-" + domesticPayment.getAttribute("sequence_number"));
		handler.setAttribute("/BenePaymentStatus",domesticPayment.getAttribute("payment_status"));
		String temp = domesticPayment.getAttribute("payment_system_ref");
		if(!InstrumentServices.isBlank(temp)){
			handler.setAttribute("/PaymentSystemRef",temp);
		}
		temp = domesticPayment.getAttribute("error_text");
		if(!InstrumentServices.isBlank(temp)){
			handler.setAttribute("/ErrorText",temp);
		}
		return handler;
	}

}
