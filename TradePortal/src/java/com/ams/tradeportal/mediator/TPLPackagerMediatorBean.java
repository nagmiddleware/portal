package com.ams.tradeportal.mediator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.common.*;
import com.ams.tradeportal.mediator.util.BankUpdateCentreProcessor;
import com.ams.tradeportal.mediator.util.InvoicePackagerUtility;
import com.ams.tradeportal.busobj.*;
import com.ams.tradeportal.busobj.util.*;
import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;

import java.rmi.*;
import java.sql.SQLException;
import java.util.Hashtable;
import java.util.List;
import java.util.Vector;


/*
 * Manages TPL Message Packaging..
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */

public class TPLPackagerMediatorBean extends MediatorBean {
	private static final Logger LOG = LoggerFactory.getLogger(TPLPackagerMediatorBean.class);

	public DocumentHandler execute(DocumentHandler inputDoc, DocumentHandler outputDoc, MediatorServices mediatorServices)
			throws RemoteException, AmsException {
		try {

			String eventTransactionOid = inputDoc.getAttribute("/transaction_oid");
			String eventInstrumentOid = inputDoc.getAttribute("/instrument_oid");
			String messageType = TradePortalConstants.MSG_TRANSACTION;
			LOG.debug("[TPLPackagerMediatorBean] Start Packageing.  This is the transaction id {}", eventTransactionOid);

			String messageID = inputDoc.getAttribute("/message_id");

			// Getting the Universal Message format
			UniversalMessage.getUniversalMessage(outputDoc);


			// Get The Required Objects
			LOG.debug("Ready to Instantiate Instrument Object");
			Instrument instrument = (Instrument) mediatorServices.createServerEJB("Instrument", Long.parseLong(eventInstrumentOid));
			LOG.debug("Ready to instantiate transaction  ");
			Transaction transaction = (Transaction) instrument.getComponentHandle("TransactionList", Long.parseLong(eventTransactionOid));
			String a_client_bank_oid = instrument.getAttribute("client_bank_oid");
			LOG.debug("This is the client_bank_oid ");
			String c_CustomerEnteredTerms = transaction.getAttribute("c_CustomerEnteredTerms");
//			String c_BankReleasedTerms = transaction.getAttribute("c_BankReleasedTerms");
			LOG.debug("TPLPackager: This is the id of  CustomerTerms : {}" , c_CustomerEnteredTerms);
			Terms customerEnteredTerms = (Terms) (transaction.getComponentHandle("CustomerEnteredTerms"));

			String workItemNumber = customerEnteredTerms.getAttribute("work_item_number");

			boolean packageStatus = true;
			LOG.debug("Ready to instantiate clientBank  with id : {}" , a_client_bank_oid);
			ClientBank clientBank = null;
			if (!(a_client_bank_oid == null || a_client_bank_oid.equals(""))) {
				clientBank = (ClientBank) mediatorServices.createServerEJB("ClientBank", Long.parseLong(a_client_bank_oid));
			} else {
				mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE, TradePortalConstants.NO_ASSOCIATED_VALUE, "ClientBank", "Instrument");
				packageStatus = false;
			}
			String a_op_bank_org_oid = instrument.getAttribute("op_bank_org_oid");
			String transactionType = transaction.getAttribute("transaction_type_code");
			String originalInstrumentOid = instrument.getAttribute("related_instrument_oid");
			String originalOpOrgOid = null;
			LOG.debug("Ready to instantiate operBankOrg with Id = : {}" , a_op_bank_org_oid);
			if (transactionType.equals(TransactionType.TRANSFER)
					&& !(originalInstrumentOid == null || originalInstrumentOid.equals(""))) {
				String orginialOpOrgSQL = "SELECT A_OP_BANK_ORG_OID FROM INSTRUMENT WHERE INSTRUMENT_OID = ?";
				DocumentHandler resultXML = DatabaseQueryBean.getXmlResultSet(orginialOpOrgSQL, true, new Object[] { originalInstrumentOid });
				if (resultXML != null) {
					originalOpOrgOid = resultXML.getAttribute("/ResultSetRecord(" + 0 + ")/A_OP_BANK_ORG_OID");
				}
				if (!(originalOpOrgOid == null || originalOpOrgOid.equals(""))) {
					a_op_bank_org_oid = originalOpOrgOid;
				}
			}

			OperationalBankOrganization operationalBankOrganization = null;
			if (!(a_op_bank_org_oid == null || a_op_bank_org_oid.equals(""))) {
				operationalBankOrganization = (OperationalBankOrganization) mediatorServices
						.createServerEJB("OperationalBankOrganization", Long.parseLong(a_op_bank_org_oid));
			} else {
				mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE, TradePortalConstants.NO_ASSOCIATED_VALUE, "Operational Bank Org", "Instrument");
				packageStatus = false;
			}

			String corp_org_oid = instrument.getAttribute("corp_org_oid");
			LOG.debug("Ready to instantiate corpOrg with id = : {}" , corp_org_oid);
			CorporateOrganization corpOrg = null;
			if (!(corp_org_oid.equals("") || corp_org_oid == null))
				corpOrg = (CorporateOrganization) mediatorServices.createServerEJB("CorporateOrganization", Long.parseLong(corp_org_oid));
			else {
				mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE, TradePortalConstants.NO_ASSOCIATED_VALUE, "CorporateOrg", "Instrument");
				packageStatus = false;
			}
			// MEer CR-1026
			String bankOrgGroupOid = "";
			boolean isCustNotIntegratedWithTPS = false;

			if (packageStatus == true && corpOrg != null) {
				bankOrgGroupOid = corpOrg.getAttribute("bank_org_group_oid");
				isCustNotIntegratedWithTPS = corpOrg.isCustNotIntegratedWihTPS();
			}

			BankOrganizationGroup bankOrganizationGroup = null;
			if (StringFunction.isNotBlank(bankOrgGroupOid)) {
				bankOrganizationGroup = (BankOrganizationGroup) mediatorServices.createServerEJB("BankOrganizationGroup", Long.parseLong(bankOrgGroupOid));
				LOG.info("bankOrgGroupOid: {}" , bankOrgGroupOid);
			} else {
				mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE, TradePortalConstants.NO_ASSOCIATED_VALUE, "Bank Organization Group", "Instrument");
				packageStatus = false;
			}

			String includeTTReimburseAllowInd = "";
			if (packageStatus == true && bankOrganizationGroup != null)
				includeTTReimburseAllowInd = bankOrganizationGroup.getAttribute("include_tt_reim_allowed");
			
			boolean includeUserDefinedField = TradePortalConstants.INDICATOR_YES.equals(clientBank.getAttribute("utilize_additional_bank_fields"));

			LOG.debug("Ready to get different Objects Attributes : ");
			LOG.debug("Ready to get Instrument Attributes : ");

			boolean fvdFlag = false; // MDB Rel6.1 IR# RIUL010274654 1/15/11
			if (packageStatus == true) {
				if (transaction.getAttribute("transaction_status").equalsIgnoreCase(TransactionStatus.FVD_AUTHORIZED)) {
					fvdFlag = true; // MDB Rel6.1 IR# RIUL010274654 1/15/11
					try {
						String oid;
						// Rel8.3.0.3 IR#T36000023509 - When panel is enabled, user_oid is fetched from panel_authorizer table - START
						// Rel9.0 IR#T36000025333 - If this was H2H and is set up for STA, ignore checking panels (H2H System User
						// may not have Panel set up as per design)
						if (StringFunction.isNotBlank(transaction.getAttribute("panel_auth_group_oid"))
								&& !(transaction.isH2HSentPayment() && corpOrg.isStraightThroughAuthorize())) {
							if (TradePortalConstants.INDICATOR_YES.equals(transaction.getAttribute("bene_panel_auth_ind"))) {
								oid = getBeneAuthUser(transaction.getAttribute("transaction_oid"), mediatorServices);
							} else {
								oid = getTransAuthUser(transaction.getAttribute("transaction_oid"), mediatorServices);
							}
							transaction.setAttribute("first_authorizing_user_oid", oid);// set first_authorizing_user_oid as there
																						// is validation ahead
						} else {
							// Rel8.3.0.3 IR#T36000023509 - END
							if (StringFunction.isNotBlank(transaction.getAttribute("second_authorizing_user_oid")))
								oid = transaction.getAttribute("second_authorizing_user_oid");
							else
								oid = transaction.getAttribute("first_authorizing_user_oid");
						}
						LOG.debug("TPLPackagerMediatorBean::execute User being used for locking instrument: {}" , oid);

						LockingManager.lockBusinessObject(Long.parseLong(eventInstrumentOid), Long.parseLong(oid), true);
						LOG.debug("TPLPackagerMediatorBean::execute Locking instrument {} successful",eventInstrumentOid);

						if (!processFVD_Authorization(transaction, mediatorServices)) {
							LOG.debug("Authorization FVD failed with transaction id = : {}", eventTransactionOid);
							deleteOutgoingQueue(eventInstrumentOid, eventTransactionOid, mediatorServices);
							outputDoc.setAttribute("/Param/FVDAuthorization", TradePortalConstants.STATUS_AUTH_FAILED);
							try {
								LockingManager.unlockBusinessObjectByInstrument(Long.parseLong(eventInstrumentOid), true);
							} catch (InstrumentLockException e) {
								LOG.error("Error occured while locking Instrument {}", eventInstrumentOid, e);
							}
							return outputDoc;
						}
					} catch (InstrumentLockException e) {
						LOG.error("Processing FVD got a lock exception with instrument id = : {}" , eventInstrumentOid, e);
						outputDoc.setAttribute("/Param/FVDAuthorization", TradePortalConstants.STATUS_AUTH_FAILED);
						return outputDoc;
					}
				}

				packageInstrumentAttributes(instrument, transaction, isCustNotIntegratedWithTPS, outputDoc);
				LOG.debug("Ready to get Transaction Attributes : ");

				// ShilpaR CR707 pass instrument type
				String instrumentTypeCode = instrument.getAttribute("instrument_type_code");

				packageTransactionAttributes(workItemNumber, transaction, instrumentTypeCode, eventInstrumentOid, isCustNotIntegratedWithTPS, outputDoc);

				LOG.debug("Ready to get customerEnteredTerms Attributes : ");

				// IValavala Dec 10 08. Pass transaction, instrument also

				// DK IR T36000018249 Rel9.0 05/12/2013 - added new param corpOrg
				packageTermsAttributes(customerEnteredTerms, transaction, instrument, outputDoc, mediatorServices, corpOrg,
						includeTTReimburseAllowInd, includeUserDefinedField); // MDB POUL110452964 Rel7.1 11/7/11

				LOG.debug("Ready to get clientBank Attributes : ");

				packageClientBankAttributes(clientBank, outputDoc);

				packageheader(messageType, messageID, operationalBankOrganization, outputDoc);

				LOG.debug("Ready to get subHeader Attributes : ");

				packagesubHeader(corpOrg, outputDoc);

				// Nar CR-1132 Rel9500 02/01/2016 Add- Begin
				boolean clientBankIndicator = TradePortalConstants.INDICATOR_YES.equals(transaction.getAttribute("client_bank_ind"));
				if (includeUserDefinedField && clientBankIndicator) {					
					packageUserHeader(instrument, corpOrg, outputDoc);
				} else {
					outputDoc.removeComponent("/Proponix/UserHeader");
				}
				// Nar CR-1132 Rel9500 02/01/2015 Add- End
				// Nar CR-818 Rel9400 08/13/2015 Begin
				String dcrInstrcution = "";
				if (TransactionType.DISCREPANCY.equals(transactionType)
						&& customerEnteredTerms.isAttributeRegistered("import_discrepancy_instr")) {
					dcrInstrcution = customerEnteredTerms.getAttribute("import_discrepancy_instr");
				}
				if (SettlementInstrUtility.isSettlemntInstrVerificationReq(instrument.getAttribute("corp_org_oid"), transactionType,
						dcrInstrcution)) {
					packageSettlementInstructionDetail(customerEnteredTerms, transaction, mediatorServices, outputDoc);
				}
				// Nar CR-818 Rel9400 08/13/2015 End
				LOG.debug("Ready to attach documents : ");
				packageAttachedDocuments(eventTransactionOid, outputDoc);

			}

			else {
				mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,
						TradePortalConstants.TASK_NOT_SUCCESSFUL, "TPL Message Packaging Error");
				return outputDoc;
			}


			// MDB Rel6.1 IR# RIUL010274654 1/15/11 - Begin
			if (fvdFlag) {
				LockingManager.unlockBusinessObjectByInstrument(Long.parseLong(eventInstrumentOid), true);
				transaction.save();
			}
			// MDB Rel6.1 IR# RIUL010274654 1/15/11 - End
			// Nar CR-1029 06/11/2015 Rel 9.4 Begin
			if (TradePortalConstants.INDICATOR_YES.equals(InvoicePackagerUtility.getParm("DoNotSendMQ", inputDoc.getAttribute("/process_parameters")))) {
				mediatorServices.getCSDB().setCSDBValue(TradePortalConstants.TRAN_ACTION,
						TradePortalConstants.BANK_ADMIN_ACTION_AUTHORISED_BY_CUST);
				DocumentHandler imageDoc = outputDoc.getFragment("/Proponix/Body/InstrumentTransaction/DocumentImage");
				BankUpdateCentreProcessor.createTransactionToBank(transaction.getAttribute("transaction_oid"), imageDoc,
						mediatorServices);
			}
			// Nar CR-1029 06/11/2015 Rel 9.4 End

		} catch (RemoteException e) {
			mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,
					TradePortalConstants.TASK_NOT_SUCCESSFUL, "TPL Message Packaging RemoteException - " + e.getMessage());
			LOG.error("Exception occured while packaging TPL message", e);
		} catch (AmsException e) {
			mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,
					TradePortalConstants.TASK_NOT_SUCCESSFUL, "TPL Message Packaging AmsExcetpion - " + e.getMessage());
			LOG.error("Exception occured while packaging TPL message", e);

		} catch (Exception e) {
			mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,
					TradePortalConstants.TASK_NOT_SUCCESSFUL, "TPL Message Packaging Exception - " + e.getMessage());
			LOG.error("Exception occured while packaging TPL message", e);

		}
		return outputDoc;
	}

	/**
	 * This method takes the instrument object and outputDoc and populates the outputDoc with the instrument values required.
	 *
	 * @param instrument,
	 *            Instrument object whose values are being packaged
	 * @param outputDoc,
	 *            DocumentHandler object which is the final output with packaged values. author iv
	 */
	public void packageInstrumentAttributes(Instrument instrument, Transaction transaction, boolean isCustNotIntegratedWithTPS,
			DocumentHandler outputDoc) throws RemoteException, AmsException {

		String instrumentPath = UniversalMessage.instrumentPath;
		// AShahi 11/23/2005 CR-232 Change Begin
		// Added additional attribute 'from_express_template' to be obtained
		// Added fixed_payment_flag attribute IR # T36000019732 from the instrument table
		String[] instrumentAttributes = { "instrument_num", "instrument_prefix", "instrument_suffix", "complete_instrument_id",
				"bank_instrument_id", "instrument_status", "instrument_type_code", "issue_date", "language",
				"from_express_template", "suppress_doc_link_ind", "fixed_payment_flag" };
		// AShahi 11/23/2005 CR-232 Change End
		Hashtable instrumentValues = instrument.getAttributes(instrumentAttributes);

		outputDoc.setAttribute(instrumentPath + "/InstrumentIDNumber", (String) instrumentValues.get("instrument_num"));
		outputDoc.setAttribute(instrumentPath + "/InstrumentIDPrefix", (String) instrumentValues.get("instrument_prefix"));
		outputDoc.setAttribute(instrumentPath + "/InstrumentIDSuffix", (String) instrumentValues.get("instrument_suffix"));
		outputDoc.setAttribute(instrumentPath + "/InstrumentID", (String) instrumentValues.get("complete_instrument_id"));

		outputDoc.setAttribute(instrumentPath + "/InstrumentStatus", (String) instrumentValues.get("instrument_status"));

		String instrumentTypeCode = (String) instrumentValues.get("instrument_type_code");
		if (instrumentTypeCode.equals(InstrumentType.REQUEST_ADVISE)) {
			outputDoc.setAttribute(instrumentPath + "/InstrumentTypeCode", InstrumentType.EXPORT_DLC);

			if ((transaction.getAttribute("transaction_type_code") != null)
					&& transaction.getAttribute("transaction_type_code").equals(TransactionType.ISSUE)) {
				transaction.setAttribute("transaction_type_code", TransactionType.ADVISE);
			}
		} else {
			outputDoc.setAttribute(instrumentPath + "/InstrumentTypeCode", instrumentTypeCode);
		}

		outputDoc.setAttribute(instrumentPath + "/IssueDate", (String) instrumentValues.get("issue_date"));
		// AShahi 11/23/2005 CR-232 Change Begin
		// Added ExpressTemplate tag under the instrument tag
		outputDoc.setAttribute(instrumentPath + "/ExpressTemplateIndicator", (String) instrumentValues.get("from_express_template"));
				// AShahi 11/23/2005 CR-232 Change End

		// Vshah CR-452 Add Begin
		if (instrumentTypeCode.equals(InstrumentType.EXPORT_COL))
			outputDoc.setAttribute(instrumentPath + "/SuppressDocLinkIndicator", (String) instrumentValues.get("suppress_doc_link_ind"));
		// Vshah CR-452 Add End

		outputDoc.setAttribute(UniversalMessage.subHeaderPath + "/Language", (String) instrumentValues.get("language"));

		// ME CR-1026 06/30/2015
		if (isCustNotIntegratedWithTPS) {
			if (InstrumentServices.isValidInstrumentTypeForBTMU(instrument.getAttribute("instrument_oid"), instrumentTypeCode)) {
				outputDoc.setAttribute(instrumentPath + "/ClientBankInstrumentID", (String) instrumentValues.get("bank_instrument_id"));
			}
		}

	}

	/**
	 * This method takes the Transaction object and outputDoc and populates the outputDoc with the transaction values required.
	 *
	 * @param transaction,
	 *            Transaction object whose values are being packaged
	 * @param outputDoc,
	 *            DocumentHandler object which is the final output with packaged values. author iv
	 */

	public void packageTransactionAttributes(String workItemNumber, Transaction transaction, String instrumentType,
			String instrumentOid, boolean isCustNotIntegratedWithTPS, DocumentHandler outputDoc)
					throws RemoteException, AmsException {

		String transactionPath = UniversalMessage.transactionPath;
		String[] transactionAttributes = { "transaction_type_code", "display_transaction_oid", "sequence_num", "transaction_status",
				"transaction_status_date", "standby_using_guarantee_form" };

		Hashtable transactionValues = transaction.getAttributes(transactionAttributes);

		outputDoc.setAttribute(transactionPath + "/TransactionTypeCode", (String) transactionValues.get("transaction_type_code"));

		// Vshah - PPX-220A - Rel8.0 - 04/09/2012 - <BEGIN>
		if ((instrumentType.equals(InstrumentType.IMPORT_DLC) || instrumentType.equals(InstrumentType.EXPORT_DLC))
				&& TransactionType.DISCREPANCY.equals(transactionValues.get("transaction_type_code"))) {
			String sql = "SELECT PRESENTATION_STATUS FROM MAIL_MESSAGE WHERE RESPONSE_TRANSACTION_OID = ?";
			DocumentHandler resultXML = DatabaseQueryBean.getXmlResultSet(sql, true,
					new Object[] { transaction.getAttribute("transaction_oid") });
			if (resultXML != null) {
				String presentationStatus = resultXML.getAttribute("/ResultSetRecord(" + 0 + ")/PRESENTATION_STATUS");
				if (StringFunction.isNotBlank("presentationStatus"))
					outputDoc.setAttribute(transactionPath + "/PresentationStatus", presentationStatus);
			}
		}
		// Vshah - PPX-220A - Rel8.0 - 04/09/2012 - <END>

		// ShilpaR CR707 Rel 8.0 Start
		if (instrumentType.equals(InstrumentType.IMPORT_DLC) || instrumentType.equals(InstrumentType.APPROVAL_TO_PAY)
				|| instrumentType.equals(InstrumentType.LOAN_RQST)) { // RKAZI CR709 Rel 8.2 01/25/2013 -Add

			String whereClause = "a_transaction_oid = ? and a_instrument_oid = ? ";
			// if purchase order is associated to Instrument then set GroupReferenceNumber/GroupReferenceType
			if (DatabaseQueryBean.getCount("purchase_order_oid", "purchase_order", whereClause, true,
					new Object[] { transaction.getAttribute("transaction_oid"), transaction.getAttribute("instrument_oid") }) > 0) {
				String grpRefNum = TradePortalConstants.SENDER_ID + "-" + transaction.getAttribute("transaction_oid");
				outputDoc.setAttribute(transactionPath + "/GroupReferenceNumber", grpRefNum);
				outputDoc.setAttribute(transactionPath + "/GroupReferenceType", TradePortalConstants.PO_GROUPREF_TYPE);
			}
			// SHR CR708 Rel8.1.1 Start -
			// this loop will execute only if there are no POs for ATP (both PO and Inv cant be there for an ATP)
			else if ((instrumentType.equals(InstrumentType.APPROVAL_TO_PAY)
					|| instrumentType.equals(InstrumentType.LOAN_RQST))
					&& DatabaseQueryBean.getCount("upload_invoice_oid", "invoices_summary_data", whereClause, true, new Object[] {
							transaction.getAttribute("transaction_oid"), transaction.getAttribute("instrument_oid") }) > 0) { // RKAZI CR709 Rel 8.2 01/25/2013 -Add
				String grpRefNum = TradePortalConstants.SENDER_ID + "-" + transaction.getAttribute("transaction_oid");
				outputDoc.setAttribute(transactionPath + "/GroupReferenceNumber", grpRefNum);
				outputDoc.setAttribute(transactionPath + "/GroupReferenceType", TradePortalConstants.INV_GROUPREF_TYPE);
			}
			// SHR CR708 Rel8.1.1 End
		}
		// ShilpaR CR707 Rel 8.0 End
		outputDoc.setAttribute(transactionPath + "/SequenceNumber", (String) transactionValues.get("sequence_num"));
		outputDoc.setAttribute(transactionPath + "/TransactionStatus", (String) transactionValues.get("transaction_status"));
		outputDoc.setAttribute(transactionPath + "/TransactionStatusDate",
				(String) transactionValues.get("transaction_status_date"));
		outputDoc.setAttribute(transactionPath + "/StandbyUsingGuaranteeForm",
				(String) transactionValues.get("standby_using_guarantee_form"));

		String rejectionIndicator = transaction.getAttribute("rejection_indicator");
		if ((rejectionIndicator != null) && workItemNumber != null && rejectionIndicator.equals(TradePortalConstants.INDICATOR_A)) {
			outputDoc.setAttribute(transactionPath + "/ReprocessedIndicator", TradePortalConstants.INDICATOR_YES);
			outputDoc.setAttribute(transactionPath + "/ReprocessedWorkItemNumber", workItemNumber);
		}

		// Implementing the business logic if standby_using_guarantee_form = Y
		// then InstrumentTypeCode is set GUA to to facilitate MQSI transformation where it is set to SLC again.
		if (((String) transactionValues.get("standby_using_guarantee_form")).equals("Y")) {
			outputDoc.setAttribute(UniversalMessage.instrumentPath + "/InstrumentTypeCode", InstrumentType.GUARANTEE);
		}

		// ME CR-1026 06/30/2015
		if (isCustNotIntegratedWithTPS) {
			if (InstrumentServices.isValidInstrumentTypeForBTMU(instrumentOid, instrumentType)) {
				outputDoc.setAttribute(transactionPath + "/TransactionID", (String) transactionValues.get("display_transaction_oid"));
				LOG.info(" DISPLAY_TRANSACTION_OID ........ {}" , transactionValues.get("display_transaction_oid"));
			}
		}

	}

	/**
	 * This method takes the Terms object and outputDoc and populates the outputDoc with the CustomerEnteredTerms values associated
	 * with this Transaction.
	 * @param includeUserDefinedField 
	 *
	 * @param customerEnteredTerms,
	 *            Terms object whose values are being packaged
	 * @param transaction,
	 *            transaction object that will used for DomesticParties section
	 * @param outputDoc,
	 *            DocumentHandler object which is being populated with the corresponding CustomerEnteredTems.
	 *
	 */
	public void packageTermsAttributes(Terms customerEnteredTerms, Transaction transaction, Instrument instrument,
			DocumentHandler outputDoc, MediatorServices mediatorServices, CorporateOrganization corpOrg,
			String includeTTReimburseAllowInd, boolean includeUserDefinedField) throws RemoteException, AmsException { // MDB POUL110452964 Rel7.1 11/7/11, DK IR
																						// T36000018249 Rel9.0 05/12/2013 - added new param corpOrg

		DocumentHandler termsDoc = new DocumentHandler();
		String termsPath = UniversalMessage.termsPath;
		String termsPartyPath = UniversalMessage.termsPartyPath;
		termsDoc.addComponent("/Terms", outputDoc.getFragment(termsPath));
		termsDoc.addComponent("/TermsParty", outputDoc.getFragment(termsPartyPath));
		termsDoc = customerEnteredTerms.getTermsAttributesDoc(termsDoc);
		outputDoc.setComponent(termsPath, termsDoc.getFragment("/Terms"));
		// For DomesticPayment, International Payment and Accounts Transfer Add DomesticPayee. CR451 CM
		String instrumentTypeCode = instrument.getAttribute("instrument_type_code");
		boolean isSinglePayment = false; // IAZ IR-WWUK071541992 Add 07/25/10
		String beneficiaryAddressLn4 = "";
		if (instrumentTypeCode.equals(InstrumentType.DOMESTIC_PMT)
				|| instrumentTypeCode.equals(InstrumentType.XFER_BET_ACCTS)
				|| instrumentTypeCode.equals(InstrumentType.FUNDS_XFER)) {
			outputDoc.setAttribute(termsPath + "/LoansAndFunds/DailyLimitExceeded",
					transaction.getAttribute("daily_limit_exceeded_indicator"));
			outputDoc.setAttribute(termsPath + "/LoansAndFunds/PaymentDate", transaction.getAttribute("payment_date"));
			// CR-657 Rel 7.1 Leelavathi 6/21/2011 Begin
			outputDoc.setAttribute(termsPath + "/LoansAndFunds/IndividualDebitInd",
					customerEnteredTerms.getAttribute("individual_debit_ind"));
			// CR-657 Rel 7.1 Leelavathi 6/21/2011 End
			String instrumentId = instrument.getAttribute("complete_instrument_id");
			isSinglePayment = packagePaymentBeneficiary(transaction.getAttribute("transaction_oid"),
					transaction.getAttribute("c_CustomerEnteredTerms"), outputDoc, beneficiaryAddressLn4, instrumentId); // CR-507,
			// IR-WWUK071541992:  Change  domestic_payee  to payment_beneficiary

			// MDB CR-640 Rel7.1 10/13/11 Begin
			String calcMethod = customerEnteredTerms.getAttribute("display_fx_rate_method");
			if ((StringFunction.isNotBlank(calcMethod)) && (calcMethod.trim().equalsIgnoreCase(TradePortalConstants.MULTIPLY)))
				outputDoc.setAttribute(termsPath + "/LoansAndFunds/CalculationMethod", "M");
			else
				outputDoc.setAttribute(termsPath + "/LoansAndFunds/CalculationMethod", "D");
			// MDB PKUL102043568 Rel7.1 10/20/11 End

			if (StringFunction.isNotBlank(customerEnteredTerms.getAttribute("fx_online_deal_confirm_date"))) {
				outputDoc.setAttribute(termsPath + "/LoansAndFunds/FXDealIndicator", TradePortalConstants.INDICATOR_YES);
				outputDoc.setAttribute(termsPath + "/LoansAndFunds/FXRate", customerEnteredTerms.getAttribute("transfer_fx_rate"));
				outputDoc.setAttribute(termsPath + "/Instructions/CommissionAndCharges/CoveredByFECNumber",
						customerEnteredTerms.getAttribute("covered_by_fec_number"));
				outputDoc.setAttribute(termsPath + "/Instructions/CommissionAndCharges/FECRate",
						customerEnteredTerms.getAttribute("fec_rate"));
				outputDoc.setAttribute(termsPath + "/LoansAndFunds/EquivalentExchAmount",
						customerEnteredTerms.getAttribute("equivalent_exch_amount"));
			} else if (StringFunction.isNotBlank(customerEnteredTerms.getAttribute("covered_by_fec_number"))) {
				outputDoc.setAttribute(termsPath + "/LoansAndFunds/FXDealIndicator", TradePortalConstants.INDICATOR_NO);
				outputDoc.setAttribute(termsPath + "/Instructions/CommissionAndCharges/CoveredByFECNumber",
						customerEnteredTerms.getAttribute("covered_by_fec_number"));
				outputDoc.setAttribute(termsPath + "/Instructions/CommissionAndCharges/FECRate",
						customerEnteredTerms.getAttribute("fec_rate"));
			} else {
				outputDoc.setAttribute(termsPath + "/LoansAndFunds/FXDealIndicator", TradePortalConstants.INDICATOR_NO);
				outputDoc.setAttribute(termsPath + "/LoansAndFunds/FXRate", customerEnteredTerms.getAttribute("transfer_fx_rate"));
			}

			// Vshah - 03/02/2012 - Added below condition to bypass conversion rate logic for IP where settlement type is "Other".
			if (!(instrumentTypeCode.equals(InstrumentType.FUNDS_XFER) && TradePortalConstants.XFER_SETTLE_OTHER
					.equals(customerEnteredTerms.getAttribute("funds_xfer_settle_type")))) {

				String conversionRate = "";
				String transferCurrency = customerEnteredTerms.getAttribute("amount_currency_code");

				Account fromAccount = (Account) mediatorServices.createServerEJB("Account");
				fromAccount.getData(Long.parseLong(customerEnteredTerms.getAttribute("debit_account_oid")));
				String baseCurrency = fromAccount.getBaseCurrencyOfAccount();
				String fromCurrency = termsDoc.getAttribute("/Terms/LoansAndFunds/DebitCCY");
				String toCurrency = termsDoc.getAttribute("/Terms/LoansAndFunds/CreditCCY");
				boolean conversionFlag = false; // MDB PRUL11042207 Rel7.1 11/4/11

				// MDB PKUL102043568 Rel7.1 10/20/11 Begin
				if (StringFunction.isNotBlank(toCurrency) && !fromCurrency.equals(baseCurrency)
						&& !toCurrency.equals(baseCurrency) && !fromCurrency.equals(toCurrency)) // TBA
					conversionRate = TradePortalConstants.CONV_RATE_TYPE_INSTRUMENT_TO_THIRD;
				else if (StringFunction.isBlank(toCurrency) && !fromCurrency.equals(baseCurrency)
						&& !transferCurrency.equals(baseCurrency) && !fromCurrency.equals(transferCurrency))
					conversionRate = TradePortalConstants.CONV_RATE_TYPE_INSTRUMENT_TO_THIRD;
				else if ((!transferCurrency.equals(baseCurrency))
						&& ((fromCurrency.equals(baseCurrency)) || (baseCurrency.equals(toCurrency))))
					conversionRate = TradePortalConstants.CONV_RATE_TYPE_INSTRUMENT_TO_BASE;
				// MDB PRUL11042207 Rel7.1 11/4/11 Begin
				else if ((StringFunction.isNotBlank(toCurrency)) && (transferCurrency.equals(baseCurrency))
						&& ((!fromCurrency.equals(baseCurrency)) || (!baseCurrency.equals(toCurrency)))) // TBA
					conversionRate = TradePortalConstants.CONV_RATE_TYPE_BASE_TO_THIRD;
				else if ((StringFunction.isBlank(toCurrency)) && (transferCurrency.equals(baseCurrency))
						&& (!fromCurrency.equals(baseCurrency)))
					conversionRate = TradePortalConstants.CONV_RATE_TYPE_BASE_TO_THIRD;
				else if (StringFunction.isNotBlank(toCurrency) && fromCurrency.equals(toCurrency)
						&& toCurrency.equals(transferCurrency) && !fromCurrency.equals(baseCurrency)) // TBA
				{
					conversionRate = TradePortalConstants.CONV_RATE_TYPE_INSTRUMENT_TO_BASE;
					outputDoc.setAttribute(termsPath + "/LoansAndFunds/FXRate", "");
				} else if (StringFunction.isBlank(toCurrency) && fromCurrency.equals(transferCurrency)
						&& !transferCurrency.equals(baseCurrency)) {
					conversionRate = TradePortalConstants.CONV_RATE_TYPE_INSTRUMENT_TO_BASE;
					outputDoc.setAttribute(termsPath + "/LoansAndFunds/FXRate", "");
				} else if (StringFunction.isNotBlank(toCurrency) && fromCurrency.equals(toCurrency)
						&& toCurrency.equals(transferCurrency) && fromCurrency.equals(baseCurrency)) // TBA
				{
					conversionFlag = true;
					conversionRate = "";
					outputDoc.setAttribute(termsPath + "/LoansAndFunds/FXRate", "");
				} else if (StringFunction.isBlank(toCurrency) && fromCurrency.equals(transferCurrency)
						&& transferCurrency.equals(baseCurrency)) {
					conversionFlag = true;
					conversionRate = "";
					outputDoc.setAttribute(termsPath + "/LoansAndFunds/FXRate", "");
				}
				// MDB PRUL11042207 Rel7.1 11/4/11 End


				if (StringFunction.isNotBlank(customerEnteredTerms.getAttribute("fx_online_deal_confirm_date")))
					conversionRate = conversionRate.concat("B");
				else if (StringFunction.isNotBlank(customerEnteredTerms.getAttribute("covered_by_fec_number")))
					conversionRate = conversionRate.concat("C");
				// MDB PRUL11042207 Rel7.1 11/4/11 Begin
				else if (conversionFlag)
					conversionRate = "";
				// MDB PRUL11042207 Rel7.1 11/4/11 End
				else
					conversionRate = conversionRate.concat("S");

				outputDoc.setAttribute(termsPath + "/LoansAndFunds/ConversionRateType", conversionRate);

				if (instrumentTypeCode.equals(InstrumentType.DOMESTIC_PMT)
						|| instrumentTypeCode.equals(InstrumentType.FUNDS_XFER))
					outputDoc.setAttribute(termsPath + "/LoansAndFunds/EquivalentExchAmtCCY", fromCurrency);
				else // TBA
					outputDoc.setAttribute(termsPath + "/LoansAndFunds/EquivalentExchAmtCCY",
							customerEnteredTerms.getAttribute("equivalent_exch_amt_ccy"));
				// MDB CR-640 Rel7.1 10/13/11 End
			} else {
				outputDoc.setAttribute(termsPath + "/LoansAndFunds/ConversionRateType", "");
				outputDoc.setAttribute(termsPath + "/LoansAndFunds/EquivalentExchAmtCCY", "");

			}
		}
		// RKAZI CR709 Rel 8.2 01/25/2013 -Start
		if (InstrumentType.LOAN_RQST.equals(instrumentTypeCode)) {
			String financeType = customerEnteredTerms.getAttribute("finance_type");
			String transactionOid = transaction.getAttribute("transaction_oid");
			String instrumentId = instrument.getAttribute("complete_instrument_id");
			String custentertermsOid = transaction.getAttribute("c_CustomerEnteredTerms");
			if (TradePortalConstants.INDICATOR_T.equals(instrument.getAttribute("import_indicator"))
					&& (TradePortalConstants.TRADE_LOAN_REC.equals(financeType)
							|| TradePortalConstants.TRADE_LOAN_PAY.equals(financeType))) {
				packageFinancePaymentBeneficiary(transactionOid, custentertermsOid, outputDoc, beneficiaryAddressLn4, instrumentId, customerEnteredTerms);
			}
		}
		// RKAZI CR709 Rel 8.2 01/25/2013 -End
		outputDoc.setComponent(termsPartyPath, termsDoc.getFragment("/TermsParty"));
		// IAZ IR-WWUK071541992 Begin 07/25/10
		if (isSinglePayment)
			// IValavala CR564 POC use beneficiaryAddressLn4 instead
			outputDoc.setAttribute(termsPartyPath + "/FullTermsParty/AddressCity", beneficiaryAddressLn4);
		// IAZ IR-WWUK071541992 End 07/25/10
		// DK IR T36000018249 Rel9.0 05/12/2013 starts
		if (instrumentTypeCode.equals(InstrumentType.DOMESTIC_PMT) || !includeUserDefinedField) {
			int totalTermsParty = Integer.parseInt(termsDoc.getAttribute("/TermsParty/TotalNumberOfEntries"));

			for (int i = 0; i < totalTermsParty; i++) {
			 if ( instrumentTypeCode.equals(InstrumentType.DOMESTIC_PMT) ) {
				if ((TermsPartyType.APPLICANT)
						.equals(termsDoc.getAttribute("/TermsParty/FullTermsParty(" + i + ")/TermsPartyType"))) {
					// IR 31190 start
					// if otl_customer_id of APP party doesn't match any of the existing customer in portal then set the current
					// customer id as OTLCustomerID
					String whereClause = "PROPONIX_CUSTOMER_ID=?";

					if (DatabaseQueryBean.getCount("ORGANIZATION_OID", "CORPORATE_ORG", whereClause, true,
							new Object[] { termsDoc.getAttribute("/TermsParty/FullTermsParty(" + i + ")/OTLCustomerID") }) < 1) {
						outputDoc.setAttribute(termsPartyPath + "/FullTermsParty(" + i + ")/OTLCustomerID",
								corpOrg.getAttribute("Proponix_customer_id"));
					}
				}
			 }
			 // IR 31190 end
			 // IR-T36000047112 Rel 9500 02/26/2026  - Modified code to remove UD defined field if setting is off at client bank level
			 if ( !includeUserDefinedField ) {
				 outputDoc.removeComponent( termsPartyPath + "/FullTermsParty(" + i + ")/UserDefinedField1" );
				 outputDoc.removeComponent( termsPartyPath + "/FullTermsParty(" + i + ")/UserDefinedField2" );
				 outputDoc.removeComponent( termsPartyPath + "/FullTermsParty(" + i + ")/UserDefinedField3" );
				 outputDoc.removeComponent( termsPartyPath + "/FullTermsParty(" + i + ")/UserDefinedField4" );
			 }
			}
		}
		// DK IR T36000018249 Rel9.0 05/12/2013 ends

		// ME CR-1026
		if (TradePortalConstants.INDICATOR_YES.equals(includeTTReimburseAllowInd)) {
			if (InstrumentType.IMPORT_DLC.equals(instrumentTypeCode)
					&& TransactionType.ISSUE.equals(transaction.getAttribute("transaction_type_code"))) {
				LOG.info("TT Reimbursement Allowed Ind [" + customerEnteredTerms.getAttribute("tt_reimbursement_allow_ind") + "]");
				outputDoc.setAttribute(termsPath + "/OtherConditions/TTReimbursementAllowed",
						customerEnteredTerms.getAttribute("tt_reimbursement_allow_ind"));

			}
		}
		if ( !includeUserDefinedField ) {
			int totalTermsParty = Integer.parseInt(termsDoc.getAttribute("/TermsParty/TotalNumberOfEntries"));
		}

	}

	/**
	 * This method takes the Terms object and outputDoc and populates the outputDoc with the CustomerEnteredTerms values associated
	 * with this Transaction.
	 * 
	 * @param instrumentId
	 * @param customerEnteredTerms
	 *
	 * @param transaction,
	 *            transaction object that will used for DomesticParties section
	 * @param outputDoc,
	 *            DocumentHandler object which is being populated with the corresponding DomesticPayee nodes
	 *
	 * @throws RemoteException
	 */

	public boolean packageFinancePaymentBeneficiary(String transactionOid, String custentertermsOid, DocumentHandler outputDoc,
			String beneficiaryAddressLn4, String instrumentId, Terms customerEnteredTerms) // IAZ IR-WWUK071541992 Chg 07/25/10
					throws AmsException, RemoteException {
		boolean isSinglePayment = false; // IAZ IR-WWUK071541992 Add 07/25/10
		int noOfPayees = 0;
		StringBuilder paymentBeneString = new StringBuilder("||");
		if ((transactionOid != null) && (transactionOid.trim().length() > 0)) {

			String sql = "SELECT INV_PAY_INST_OID  , P_TRANSACTION_OID , P_INVOICE_SUMMARY_DATA_OID  , PAY_METHOD , AMOUNT, AMOUNT_CURRENCY_CODE, BEN_NAME , BEN_ACCT_NUM , BEN_ADDRESS_ONE , BEN_ADDRESS_TWO "
					+ ", BEN_ADDRESS_THREE , BEN_COUNTRY , BEN_BANK_NAME , BEN_BRANCH_CODE , BEN_BRANCH_ADDRESS1 , BEN_BRANCH_ADDRESS2 , BEN_BANK_CITY , BEN_BANK_PRVNCE , BEN_BRANCH_COUNTRY "
					+ ", PAYMENT_CHARGES , CENTRAL_BANK_REP1 , CENTRAL_BANK_REP2 , CENTRAL_BANK_REP3 , SEQUENCE_NUMBER, CUSTOMER_REFERENCE AS REFERENCE,BEN_BANK_SORT_CODE"
					+ ", REPORTING_CODE_1, REPORTING_CODE_2" + // Added Reporting codes as part of CR1001 - Rel9.4
					" FROM INVOICE_PAYMENT_INSTRUCTIONS " + " WHERE P_TRANSACTION_OID = ?";


			DocumentHandler resultXML = DatabaseQueryBean.getXmlResultSet(sql, true, new Object[] { transactionOid });
			Vector resultVector = new Vector();

			String loanProceedsCreditType = customerEnteredTerms.getAttribute("loan_proceeds_credit_type");
			if ((resultXML != null) && (resultXML.getFragmentsList("/ResultSetRecord/").size() > 0)) {

				resultVector = resultXML.getFragments("/ResultSetRecord/");
				noOfPayees = resultVector.size();
			} else if (TradePortalConstants.CREDIT_MULTI_BEN_ACCT.equals(loanProceedsCreditType)) {

				sql = "SELECT PAY_METHOD, AMOUNT, CURRENCY AS AMOUNT_CURRENCY_CODE" + ", SELLER_NAME AS BEN_NAME "
						+ ", BEN_ACCT_NUM , BEN_ADDRESS_ONE , BEN_ADDRESS_TWO "
						+ ", BEN_ADDRESS_THREE , BEN_COUNTRY , BEN_BANK_NAME , BEN_BRANCH_CODE , BEN_BRANCH_ADDRESS1 "
						+ ", BEN_BRANCH_ADDRESS2 , BEN_BANK_CITY , BEN_BANK_PRVNCE , BEN_BRANCH_COUNTRY "
						+ ", PAYMENT_CHARGES , CENTRAL_BANK_REP1 , CENTRAL_BANK_REP2 , CENTRAL_BANK_REP3,BEN_BANK_SORT_CODE "
						+ ", REPORTING_CODE_1, REPORTING_CODE_2" + // Added Reporting codes as part of CR1001 - Rel9.4
						" FROM INVOICE_BENEFICIARY_VIEW " + "WHERE A_TRANSACTION_OID = ?";
				

				resultXML = DatabaseQueryBean.getXmlResultSet(sql, true, new Object[] { transactionOid });
				if ((resultXML != null) && (resultXML.getFragmentsList("/ResultSetRecord/").size() > 0)) {

					resultVector = resultXML.getFragments("/ResultSetRecord/");
					noOfPayees = resultVector.size();
				}

			} else if (TradePortalConstants.CREDIT_BEN_ACCT.equals(loanProceedsCreditType)) {
				TermsParty benParty = customerEnteredTerms.getTermsPartyByPartyType(TermsPartyType.BENEFICIARY);
				TermsParty benBankParty = customerEnteredTerms.getTermsPartyByPartyType(TermsPartyType.BENEFICIARY_BANK);

				resultXML = new DocumentHandler();

				resultXML.setAttribute("/AMOUNT_CURRENCY_CODE", customerEnteredTerms.getAttribute("loan_proceeds_pmt_curr"));
				resultXML.setAttribute("/AMOUNT", customerEnteredTerms.getAttribute("loan_proceeds_pmt_amount"));
				resultXML.setAttribute("/PAY_METHOD", customerEnteredTerms.getAttribute("payment_method"));
				resultXML.setAttribute("/PAYMENT_CHARGES", customerEnteredTerms.getAttribute("bank_charges_type"));
				resultXML.setAttribute("/BEN_ACCT_NUM", benParty.getAttribute("acct_num"));
				resultXML.setAttribute("/BEN_NAME", benParty.getAttribute("name"));
				resultXML.setAttribute("/BEN_ADDRESS_ONE", benParty.getAttribute("address_line_1"));
				resultXML.setAttribute("/BEN_ADDRESS_TWO", benParty.getAttribute("address_line_2"));
				resultXML.setAttribute("/BEN_ADDRESS_THREE", benParty.getAttribute("address_line_3"));
				resultXML.setAttribute("/BEN_COUNTRY", benParty.getAttribute("address_country"));
				if (benBankParty != null) {
					resultXML.setAttribute("/BEN_BANK_NAME", benBankParty.getAttribute("name"));
					resultXML.setAttribute("/BEN_BRANCH_CODE", benBankParty.getAttribute("branch_code"));
					resultXML.setAttribute("/BEN_BRANCH_ADDRESS1", benBankParty.getAttribute("address_line_1"));
					resultXML.setAttribute("/BEN_BRANCH_ADDRESS2", benBankParty.getAttribute("address_line_2"));
					resultXML.setAttribute("/BEN_BANK_CITY", benBankParty.getAttribute("address_city"));
					resultXML.setAttribute("/BEN_BANK_PRVNCE", benBankParty.getAttribute("address_state_province"));
					resultXML.setAttribute("/BEN_BRANCH_COUNTRY", benBankParty.getAttribute("address_country"));
					resultXML.setAttribute("/BEN_BANK_SORT_CODE", benBankParty.getAttribute("ben_bank_sort_code"));
				} else {
					resultXML.setAttribute("/BEN_BANK_NAME", "");
					resultXML.setAttribute("/BEN_BRANCH_CODE", "");
					resultXML.setAttribute("/BEN_BRANCH_ADDRESS1", "");
					resultXML.setAttribute("/BEN_BRANCH_ADDRESS2", "");
					resultXML.setAttribute("/BEN_BANK_CITY", "");
					resultXML.setAttribute("/BEN_BANK_PRVNCE", "");
					resultXML.setAttribute("/BEN_BRANCH_COUNTRY", "");
					resultXML.setAttribute("/BEN_BANK_SORT_CODE", "");

				}
				resultXML.setAttribute("/CENTRAL_BANK_REP1", benParty.getAttribute("central_bank_rep1"));
				resultXML.setAttribute("/CENTRAL_BANK_REP2", benParty.getAttribute("central_bank_rep2"));
				resultXML.setAttribute("/CENTRAL_BANK_REP3", benParty.getAttribute("central_bank_rep3"));
				resultXML.setAttribute("/REPORTING_CODE_1", benParty.getAttribute("reporting_code_1"));// Rel9.4 CR-1001
				resultXML.setAttribute("/REPORTING_CODE_2", benParty.getAttribute("reporting_code_2"));// Rel9.4 CR-1001
				resultXML.setAttribute("/REFERENCE", benParty.getAttribute("customer_reference"));

				setPaymentInfo(resultXML, paymentBeneString, instrumentId, 0, "");
				noOfPayees = 1;
				resultVector = new Vector();
			}
			isSinglePayment = resultVector.size() == 1;

			int paymentCounter = 0;

			// CR 564 POC Changes Begin
			for (int i = 0; i < resultVector.size(); i++) {

				paymentCounter++;

				DocumentHandler myDoc = (DocumentHandler) resultVector.elementAt(i);
				setPaymentInfo(myDoc, paymentBeneString, instrumentId, i, loanProceedsCreditType);

				LOG.debug("[TPLPackagerMediatorBean.packagePaymentBeneficiary]: End Processing Payment Bene # {}" , i);
			}

		}

		paymentBeneString = paymentBeneString.append("|");
		LOG.debug("[TPLPackagerMediatorBean.packagePaymentBeneficiary]: End Processing All Payment Benes ");
		outputDoc.setAttribute("/Proponix/Body/InstrumentTransaction/Terms/LoansAndFunds/NumberOfPayees", String.valueOf(noOfPayees));
		outputDoc.setAttribute("/Proponix/Body/InstrumentTransaction/Terms/LoansAndFunds/FinancePaymentBeneficiaryText",
				paymentBeneString.toString()); // CR-507: 1. Change domesticPayee to PaymenBeneficiary.

		return isSinglePayment; // IAZ IR-WWUK071541992 Add 07/25/10

	}

	private void setPaymentInfo(DocumentHandler myDoc, StringBuilder paymentBeneString, String instrumentId, int i,
			String loanProceedsCreditType) {

		String sequenceNumber = myDoc.getAttribute("/SEQUENCE_NUMBER");

		paymentBeneString = addElementToPipeDelimitedText(paymentBeneString,
				instrumentId + "-" + (StringFunction.isBlank(sequenceNumber) ? i : sequenceNumber), false, false);
		paymentBeneString = addElementToPipeDelimitedText(paymentBeneString, myDoc.getAttribute("/PAY_METHOD"), false, false);

		// Beneficiary Account Num
		paymentBeneString = addElementToPipeDelimitedText(paymentBeneString, myDoc.getAttribute("/AMOUNT_CURRENCY_CODE"), false, false);
		// Beneficiary Account Num
		paymentBeneString = addElementToPipeDelimitedText(paymentBeneString, myDoc.getAttribute("/AMOUNT"), false, false);
		// Beneficiary Account Num
		paymentBeneString = addElementToPipeDelimitedText(paymentBeneString, myDoc.getAttribute("/BEN_ACCT_NUM"), false, false);
		// Beneficiary Name
		paymentBeneString = addElementToPipeDelimitedText(paymentBeneString, myDoc.getAttribute("/BEN_NAME"), false, false);
		// Beneficiary Address 1
		paymentBeneString = addElementToPipeDelimitedText(paymentBeneString, myDoc.getAttribute("/BEN_ADDRESS_ONE"), false, false);
		// CentralBankReporting1
		paymentBeneString = addElementToPipeDelimitedText(paymentBeneString, myDoc.getAttribute("/BEN_ADDRESS_TWO"), false, false);
		// Beneficiary Address Three
		paymentBeneString = addElementToPipeDelimitedText(paymentBeneString, myDoc.getAttribute("/BEN_ADDRESS_THREE"), false,
				false);
		// Beneficiary Country
		paymentBeneString = addElementToPipeDelimitedText(paymentBeneString, myDoc.getAttribute("/BEN_COUNTRY"), false, false);
		// Beneficiary Reference
		if (TradePortalConstants.CREDIT_MULTI_BEN_ACCT.equals(loanProceedsCreditType)) {
			paymentBeneString = addElementToPipeDelimitedText(paymentBeneString, "Multiple Invoices", false, false);
		} else {
			paymentBeneString = addElementToPipeDelimitedText(paymentBeneString, myDoc.getAttribute("/REFERENCE"), false, false);
		}
		// Beneficiary Bank Name
		paymentBeneString = addElementToPipeDelimitedText(paymentBeneString, myDoc.getAttribute("/BEN_BANK_NAME"), false, false);
		// Beneficiary Bank Branch Code
		paymentBeneString = addElementToPipeDelimitedText(paymentBeneString, myDoc.getAttribute("/BEN_BRANCH_CODE"), false, false);
		// Beneficiary Bank Branch Address 1
		paymentBeneString = addElementToPipeDelimitedText(paymentBeneString, myDoc.getAttribute("/BEN_BRANCH_ADDRESS1"), false, false);
		// Beneficiary Bank Branch Address 2
		paymentBeneString = addElementToPipeDelimitedText(paymentBeneString, myDoc.getAttribute("/BEN_BRANCH_ADDRESS2"), false, false);
		// Beneficiary Bank Branch City + Beneficiary Bank Branch Province
		String bankAddrLine3 = (StringFunction.isNotBlank(myDoc.getAttribute("/BEN_BANK_CITY"))
				? myDoc.getAttribute("/BEN_BANK_CITY") : "")
				+ (StringFunction.isNotBlank(myDoc.getAttribute("/BEN_BANK_PRVNCE")) ? myDoc.getAttribute("/BEN_BANK_PRVNCE") : "");
		if (StringFunction.isNotBlank(bankAddrLine3)) {
			if (bankAddrLine3.length() > 31) {
				bankAddrLine3 = bankAddrLine3.substring(0, 32);
			}
		} else {
			bankAddrLine3 = "";
		}
		paymentBeneString = addElementToPipeDelimitedText(paymentBeneString, bankAddrLine3, false, false);

		// Beneficiary Bank Country
		paymentBeneString = addElementToPipeDelimitedText(paymentBeneString, myDoc.getAttribute("/BEN_BRANCH_COUNTRY"), false, false);

		String bankChargesType = myDoc.getAttribute("/PAYMENT_CHARGES");
		paymentBeneString = addElementToPipeDelimitedText(paymentBeneString, bankChargesType, false, false);

		// Central Bank Reporting 1
		paymentBeneString = addElementToPipeDelimitedText(paymentBeneString, myDoc.getAttribute("/CENTRAL_BANK_REP1"), false, false);
		// Central Bank Reporting 2
		paymentBeneString = addElementToPipeDelimitedText(paymentBeneString, myDoc.getAttribute("/CENTRAL_BANK_REP2"), false, false);
		// Central Bank Reporting 3
		paymentBeneString = addElementToPipeDelimitedText(paymentBeneString, myDoc.getAttribute("/CENTRAL_BANK_REP3"), false, false);

		paymentBeneString = addElementToPipeDelimitedText(paymentBeneString, myDoc.getAttribute("/BEN_BANK_SORT_CODE"), false, false);

		// CR1001 Rel9.4 START
		// Reporting_Code_1
		paymentBeneString = addElementToPipeDelimitedText(paymentBeneString, myDoc.getAttribute("/REPORTING_CODE_1"), false, false);
		// Reporting_Code_2
		paymentBeneString = addElementToPipeDelimitedText(paymentBeneString, myDoc.getAttribute("/REPORTING_CODE_2"), false, true);
		// CR1001 Rel9.4 END
	}
	// RKAZI CR709 Rel 8.2 01/25/2013 -End

	/**
	 * This method takes the Terms object and outputDoc and populates the outputDoc with the CustomerEnteredTerms values associated
	 * with this Transaction.
	 * 
	 * @param instrumentId
	 *
	 * @param transaction,
	 *            transaction object that will used for DomesticParties section
	 * @param outputDoc,
	 *            DocumentHandler object which is being populated with the corresponding DomesticPayee nodes
	 *
	 */

	public boolean packagePaymentBeneficiary(String transactionOid, String custentertermsOid, DocumentHandler outputDoc,
			String beneficiaryAddressLn4, String instrumentId) // IAZ IR-WWUK071541992 Chg 07/25/10
					throws AmsException {
		boolean isSinglePayment = false; // IAZ IR-WWUK071541992 Add 07/25/10
		int noOfPayees = 0;
		StringBuilder paymentBeneString = new StringBuilder("||");
		if ((transactionOid != null) && (transactionOid.trim().length() > 0)) {

			String sql = "SELECT DOMESTIC_PAYMENT_OID,PAYMENT_METHOD_TYPE,AMOUNT_CURRENCY_CODE,AMOUNT,VALUE_DATE,CUSTOMER_REFERENCE, "
					+ "BANK_CHARGES_TYPE,OTHER_CHARGES_INSTRUCTIONS,PAYEE_DESCRIPTION,PAYEE_ACCOUNT_NUMBER,PAYEE_NAME, PAYEE_ADDRESS_LINE_1, "
					+ "PAYEE_ADDRESS_LINE_2, PAYEE_ADDRESS_LINE_3, PAYEE_ADDRESS_LINE_4, DOMESTIC_PAYMENT.COUNTRY,PAYEE_FAX_NUMBER,PAYEE_EMAIL,PAYEE_INSTRUCTION_NUMBER, "
					+ "PAYEE_BANK_NAME, PAYEE_BANK_CODE, PAYEE_BRANCH_NAME, DOMESTIC_PAYMENT.ADDRESS_LINE_1, DOMESTIC_PAYMENT.ADDRESS_LINE_2, DOMESTIC_PAYMENT.ADDRESS_LINE_3, C_FIRST_INTERMEDIARY_BANK, "
					+ "DELIVERY_METHOD, PAYABLE_LOCATION, PRINT_LOCATION, MAILING_ADDRESS_LINE_1,MAILING_ADDRESS_LINE_2,MAILING_ADDRESS_LINE_3,MAILING_ADDRESS_LINE_4, "
					+ "CENTRAL_BANK_REPORTING_1, CENTRAL_BANK_REPORTING_2, CENTRAL_BANK_REPORTING_3, PAYEE_BANK_COUNTRY, "
					+ "PAYMENT_PARTY_INTER.BANK_NAME INTER_BANK_NAME, PAYMENT_PARTY_INTER.BANK_BRANCH_CODE INTER_BANK_BRANCH_CODE, PAYMENT_PARTY_INTER.BRANCH_NAME INTER_BRANCH_NAME, PAYMENT_PARTY_INTER.COUNTRY INTER_COUNTRY, "
					+ "PAYMENT_PARTY_INTER.PARTY_TYPE INTER_PARTY_TYPE, DOMESTIC_PAYMENT.SEQUENCE_NUMBER, DOMESTIC_PAYMENT.REPORTING_CODE_1, DOMESTIC_PAYMENT.REPORTING_CODE_2 "
					+ "FROM DOMESTIC_PAYMENT, PAYMENT_PARTY PAYMENT_PARTY_INTER " + "WHERE P_TRANSACTION_OID = ?" + " AND "
					+ "DOMESTIC_PAYMENT.C_FIRST_INTERMEDIARY_BANK = PAYMENT_PARTY_INTER.PAYMENT_PARTY_OID(+)";

			DocumentHandler resultXML = DatabaseQueryBean.getXmlResultSet(sql, true, new Object[] { transactionOid });

			if ((resultXML != null) && (resultXML.getFragmentsList("/ResultSetRecord/").size() > 0)) {

				Vector resultVector = resultXML.getFragments("/ResultSetRecord/");

				String sqlOP = "SELECT C_ORDERING_PARTY_OID FROM TERMS WHERE TERMS_OID = ?";
				String sqlOP1 = "SELECT BANK_NAME,ADDRESS_LINE_1, ADDRESS_LINE_2, ADDRESS_LINE_3, ADDRESS_LINE_4, BANK_BRANCH_CODE, BRANCH_NAME,"
						+ "COUNTRY FROM PAYMENT_PARTY WHERE PAYMENT_PARTY_OID = (" + sqlOP + ")";

				DocumentHandler resultXML1 = DatabaseQueryBean.getXmlResultSet(sqlOP1, true, new Object[] { custentertermsOid });
				Vector resultVector1;
				DocumentHandler myDoc21 = null;
				if ((resultXML1 != null) && (resultXML1.getFragmentsList("/ResultSetRecord/").size() > 0)) {
					resultVector1 = resultXML1.getFragments("/ResultSetRecord/");
					myDoc21 = (DocumentHandler) resultVector1.elementAt(0); 
				}

				isSinglePayment = resultVector.size() == 1; 

				// CR 564 POC Changes Begin

				noOfPayees = resultVector.size();
				String bankChargesType;

				for (int i = 0; i < resultVector.size(); i++) {
					DocumentHandler myDoc = (DocumentHandler) resultVector.elementAt(i);

					// IValavala CR564 POC. Add BankChargesType for singlePayee
					if (i == 0 && isSinglePayment) {
						outputDoc.setAttribute("/Proponix/Body/InstrumentTransaction/Terms/LoansAndFunds/BankChargesType",
								myDoc.getAttribute("/BANK_CHARGES_TYPE"));
						outputDoc.setAttribute("/Proponix/Body/InstrumentTransaction/Terms/LoansAndFunds/PayeeAccountNumber",
								myDoc.getAttribute("/PAYEE_ACCOUNT_NUMBER"));
						outputDoc.setAttribute("/Proponix/Body/InstrumentTransaction/Terms/LoansAndFunds/BeneficiaryMessage",
								myDoc.getAttribute("/PAYEE_DESCRIPTION"));
					}

					String sequenceNumber = myDoc.getAttribute("/SEQUENCE_NUMBER");

					paymentBeneString = addElementToPipeDelimitedText(paymentBeneString, instrumentId + "-" + sequenceNumber, false, false);
					paymentBeneString = addElementToPipeDelimitedText(paymentBeneString, myDoc.getAttribute("/PAYMENT_METHOD_TYPE"), false, false);
					paymentBeneString = addElementToPipeDelimitedText(paymentBeneString, myDoc.getAttribute("/AMOUNT_CURRENCY_CODE"), false, false);
					paymentBeneString = addElementToPipeDelimitedText(paymentBeneString, myDoc.getAttribute("/AMOUNT"), false, false);
					paymentBeneString = addElementToPipeDelimitedText(paymentBeneString, myDoc.getAttribute("/VALUE_DATE"), false, false);
					paymentBeneString = addElementToPipeDelimitedText(paymentBeneString, myDoc.getAttribute("/CUSTOMER_REFERENCE"), false, false);
					// IValavala BankChargesType Logic
					bankChargesType = myDoc.getAttribute("/BANK_CHARGES_TYPE");
					if (bankChargesType.equals("A"))
						bankChargesType = "P";
					if (bankChargesType.equals("I"))
						bankChargesType = "E";
					paymentBeneString = addElementToPipeDelimitedText(paymentBeneString, bankChargesType, false, false);
					// VS CR 631 Begin
					paymentBeneString = addElementToPipeDelimitedText(paymentBeneString, "Shared Charges", false, false);
					// VS CR 631 End
					paymentBeneString = addElementToPipeDelimitedText(paymentBeneString, myDoc.getAttribute("/PAYEE_DESCRIPTION"),
							false, false);
					paymentBeneString = addElementToPipeDelimitedText(paymentBeneString,
							myDoc.getAttribute("/PAYEE_ACCOUNT_NUMBER"), false, false);
					paymentBeneString = addElementToPipeDelimitedText(paymentBeneString, myDoc.getAttribute("/PAYEE_NAME"), false,
							false);
					paymentBeneString = addElementToPipeDelimitedText(paymentBeneString,
							myDoc.getAttribute("/PAYEE_ADDRESS_LINE_1"), false, false);
					paymentBeneString = addElementToPipeDelimitedText(paymentBeneString,
							myDoc.getAttribute("/PAYEE_ADDRESS_LINE_2"), false, false);
					paymentBeneString = addElementToPipeDelimitedText(paymentBeneString,
							myDoc.getAttribute("/PAYEE_ADDRESS_LINE_3"), false, false);
					paymentBeneString = addElementToPipeDelimitedText(paymentBeneString,
							myDoc.getAttribute("/PAYEE_ADDRESS_LINE_4"), false, false);
					if (isSinglePayment)
						beneficiaryAddressLn4 = myDoc.getAttribute("/PAYEE_ADDRESS_LINE_4");
					paymentBeneString = addElementToPipeDelimitedText(paymentBeneString, myDoc.getAttribute("/COUNTRY"), false, false);
					paymentBeneString = addElementToPipeDelimitedText(paymentBeneString, myDoc.getAttribute("/PAYEE_FAX_NUMBER"), false, false);
					paymentBeneString = addElementToPipeDelimitedText(paymentBeneString, myDoc.getAttribute("/PAYEE_EMAIL"), false, false);
					paymentBeneString = addElementToPipeDelimitedText(paymentBeneString, myDoc.getAttribute("/PAYEE_INSTRUCTION_NUMBER"), false, false);

					// VS IR KAUK122148279 01/07/10 Rel 6.1.0 uncommented part for Beneficiary Bank
					if (isSinglePayment) {
						paymentBeneString = addElementToPipeDelimitedText(paymentBeneString, myDoc.getAttribute("/PAYEE_BANK_NAME"),
								false, false);
					}
					// cquinton 1/19/2011 Rel 6.1.0 ir#hiul011549374 else necessary for multi payments
					else {
						paymentBeneString = addElementToPipeDelimitedText(paymentBeneString, "", false, false);
					}
					paymentBeneString = addElementToPipeDelimitedText(paymentBeneString, myDoc.getAttribute("/PAYEE_BANK_CODE"), false, false);

					// Ordering Party
					if ((resultXML1 != null) && (resultXML1.getFragmentsList("/ResultSetRecord/").size() > 0)) {
						// OrderingPartyName
						paymentBeneString = addElementToPipeDelimitedText(paymentBeneString, myDoc21.getAttribute("/BANK_NAME"), false, false);
					}
					// cquinton 1/19/2011 Rel 6.1.0 ir#hiul011549374 if not found put empty string
					else {
						paymentBeneString = addElementToPipeDelimitedText(paymentBeneString, "", false, false);
					}

					// First Intermediary Bank part

					// VS IR KAUK122148279 01/07/10 Rel 6.1.0 uncommented part for first intermediary bank
					if (isSinglePayment) {
						// FirstIntermediaryBankName
						paymentBeneString = addElementToPipeDelimitedText(paymentBeneString, myDoc.getAttribute("/INTER_BANK_NAME"), false, false);
					}
					// cquinton 1/19/2011 Rel 6.1.0 ir#hiul011549374 else necessary for multi payments
					else {
						paymentBeneString = addElementToPipeDelimitedText(paymentBeneString, "", false, false);
					}
					// FirstIntermediaryBankBranchCode
					paymentBeneString = addElementToPipeDelimitedText(paymentBeneString,
							myDoc.getAttribute("/INTER_BANK_BRANCH_CODE"), false, false);

					// ChequeDeliveryMethodDeliverTo
					paymentBeneString = addElementToPipeDelimitedText(paymentBeneString, myDoc.getAttribute("/DELIVERY_METHOD"),
							false, false);
					// ChequePayableLocation
					paymentBeneString = addElementToPipeDelimitedText(paymentBeneString, myDoc.getAttribute("/PAYABLE_LOCATION"),
							false, false);
					// ChequePrintLocation
					paymentBeneString = addElementToPipeDelimitedText(paymentBeneString, myDoc.getAttribute("/PRINT_LOCATION"),
							false, false);
					// ChequeMailingAddressLn1
					paymentBeneString = addElementToPipeDelimitedText(paymentBeneString,
							myDoc.getAttribute("/MAILING_ADDRESS_LINE_1"), false, false);
					// ChequeMailingAddressLn2
					paymentBeneString = addElementToPipeDelimitedText(paymentBeneString,
							myDoc.getAttribute("/MAILING_ADDRESS_LINE_2"), false, false);
					// ChequeMailingAddressLn3
					paymentBeneString = addElementToPipeDelimitedText(paymentBeneString,
							myDoc.getAttribute("/MAILING_ADDRESS_LINE_3"), false, false);
					// ChequeMailingAddressLn4
					paymentBeneString = addElementToPipeDelimitedText(paymentBeneString,
							myDoc.getAttribute("/MAILING_ADDRESS_LINE_4"), false, false);
					// CentralBankReporting1
					paymentBeneString = addElementToPipeDelimitedText(paymentBeneString,
							myDoc.getAttribute("/CENTRAL_BANK_REPORTING_1"), false, false);
					// CentralBankReporting2
					paymentBeneString = addElementToPipeDelimitedText(paymentBeneString,
							myDoc.getAttribute("/CENTRAL_BANK_REPORTING_2"), false, false);
					// CentralBankReporting3
					paymentBeneString = addElementToPipeDelimitedText(paymentBeneString,
							myDoc.getAttribute("/CENTRAL_BANK_REPORTING_3"), false, false);
					// ReportingCode1
					paymentBeneString = addElementToPipeDelimitedText(paymentBeneString, myDoc.getAttribute("/REPORTING_CODE_1"), false, false);

					// ReportingCode2
					paymentBeneString = addElementToPipeDelimitedText(paymentBeneString, myDoc.getAttribute("/REPORTING_CODE_2"), false, true);

					LOG.debug("[TPLPackagerMediatorBean.packagePaymentBeneficiary]: End Processing Payment Bene # {}" , i);
				}
			}
		}

		paymentBeneString = paymentBeneString.append("|");
		LOG.debug("[TPLPackagerMediatorBean.packagePaymentBeneficiary]: End Processing All Payment Benes ");
		outputDoc.setAttribute("/Proponix/Body/InstrumentTransaction/Terms/LoansAndFunds/NumberOfPayees", String.valueOf(noOfPayees));
		outputDoc.setAttribute("/Proponix/Body/InstrumentTransaction/Terms/LoansAndFunds/PaymentBeneficiaryText", paymentBeneString.toString()); // CR-507: 1. Change domesticPayee to PaymenBeneficiary.

		return isSinglePayment; // IAZ IR-WWUK071541992 Add 07/25/10

	}

	private StringBuilder addElementToPipeDelimitedText(StringBuilder mainText, String element, boolean firstElement, boolean lastElement) {
		
		if (StringFunction.isBlank(element))
			element = "null";
		mainText = mainText.append("|").append(element);
		
		if (lastElement)
			mainText = mainText.append("||");
	
		return mainText;
	
	}

	/**
	 * This method takes the ClientBank object and outputDoc and populates the outputDoc with the ClientBank values required.
	 *
	 * @param clientBank,
	 *            ClientBank object whose values are being packaged
	 * @param outputDoc,
	 *            DocumentHandler object which is the final output with packaged values. author iv
	 */

	public void packageClientBankAttributes(ClientBank clientBank, DocumentHandler outputDoc) throws RemoteException, AmsException {
		String clientBankPath = UniversalMessage.clientBankPath;

		outputDoc.setAttribute(clientBankPath + "/OTLID", clientBank.getAttribute("OTL_id"));

	}

	/**
	 * This method takes the CorporateOrganization object and outputDoc and populates the outputDoc with the subHeader values
	 * required.
	 *
	 * @param corpOrg,
	 *            CorporateOrganization object whose values are being packaged
	 * @param outputDoc,
	 *            DocumentHandler object which is the final output with packaged values. author iv
	 */

	public void packagesubHeader(CorporateOrganization corpOrg, DocumentHandler outputDoc) throws RemoteException, AmsException {
		String subHeaderPath = UniversalMessage.subHeaderPath;

		outputDoc.setAttribute(subHeaderPath + "/CustomerID", corpOrg.getAttribute("Proponix_customer_id"));
		outputDoc.setAttribute(subHeaderPath + "/TotalNumberOfEntries", "1");
	}

	/**
	 * This method takes the messageType, messageID, OperationalBankOrganization and outputDoc to populate the outputDoc with the
	 * Header values required.
	 *
	 * @param String
	 *            messageType
	 * @param String
	 *            messageID
	 * @param OperationalBankOrganization
	 * @param DocumentHandler
	 *            outputDoc which is the final output with packaged values. author iv
	 */

	public void packageheader(String messageType, String messageID, OperationalBankOrganization operationalBankOrganization,
			DocumentHandler outputDoc) throws RemoteException, AmsException {

		String headerPath = UniversalMessage.headerPath;

		String destinationID = TradePortalConstants.DESTINATION_ID_PROPONIX;
		String senderID = TradePortalConstants.SENDER_ID;

		String operationOrganizationID = operationalBankOrganization.getAttribute("Proponix_id");
		String timeStamp = DateTimeUtility.getGMTDateTime(false);
		String dateSent = timeStamp;
		String timeSent = timeStamp.substring(11, 19);

		outputDoc.setAttribute(headerPath + "/DestinationID", destinationID);
		outputDoc.setAttribute(headerPath + "/SenderID", senderID);
		outputDoc.setAttribute(headerPath + "/OperationOrganizationID", operationOrganizationID);
		outputDoc.setAttribute(headerPath + "/DateSent", dateSent);
		outputDoc.setAttribute(headerPath + "/TimeSent", timeSent);
		outputDoc.setAttribute(headerPath + "/MessageType", messageType);
		outputDoc.setAttribute(headerPath + "/MessageID", messageID);

	}

	/**
	 *
	 * @param transactionOid
	 * @param outputDoc
	 * @throws RemoteException
	 * @throws AmsException
	 */
	private void packageAttachedDocuments(String transactionOid, DocumentHandler outputDoc) throws AmsException {
		if ((transactionOid != null) && (transactionOid.trim().length() > 0)) {

			// cquinton 07/22/2011 Rel 7.1.0 ppx240 - add imageHash
			String sql = "select IMAGE_ID, NUM_PAGES, IMAGE_FORMAT, IMAGE_BYTES, DOC_NAME, HASH  from DOCUMENT_IMAGE "
					+ "where  FORM_TYPE = '" + TradePortalConstants.DOC_IMG_FORM_TYPE_PORTAL_ATTACHED + "' "
					+ "and P_TRANSACTION_OID  = ?";


			DocumentHandler resultXML = DatabaseQueryBean.getXmlResultSet(sql, true, new Object[] { transactionOid });
			if ((resultXML != null) && (resultXML.getFragmentsList("/ResultSetRecord/").size() > 0)) {
				List<DocumentHandler> resultList = resultXML.getFragmentsList("/ResultSetRecord/");

				String documentImagePath = UniversalMessage.documentImagePath;

				outputDoc.setAttribute(documentImagePath + "/TotalNumberOfEntries", String.valueOf(resultList.size()));
				LOG.debug("[TPLPackagerMediatorBean.packageAttachedDocuments] After adding to {}/TotalNumberOfEntries' outputDoc = {}" ,documentImagePath, outputDoc);

				for (int i = 0; i < resultList.size(); i++) {
					DocumentHandler myDoc =  resultList.get(i);
					LOG.debug("[TPLPackagerMediatorBean.packageAttachedDocuments] myDoc = {}" , myDoc);

					outputDoc.setAttribute(documentImagePath + "/FullDocumentImage(" + i + ")/FormType",
							TradePortalConstants.DOC_IMG_FORM_TYPE_PORTAL_ATTACHED.substring(0, 1).toUpperCase());
					outputDoc.setAttribute(documentImagePath + "/FullDocumentImage(" + i + ")/ImageID",
							myDoc.getAttribute("/IMAGE_ID"));
					outputDoc.setAttribute(documentImagePath + "/FullDocumentImage(" + i + ")/DocumentText",
							myDoc.getAttribute("/DOC_NAME"));
					outputDoc.setAttribute(documentImagePath + "/FullDocumentImage(" + i + ")/NumPages",
							myDoc.getAttribute("/NUM_PAGES"));
					outputDoc.setAttribute(documentImagePath + "/FullDocumentImage(" + i + ")/ImageFormat",
							myDoc.getAttribute("/IMAGE_FORMAT"));
					outputDoc.setAttribute(documentImagePath + "/FullDocumentImage(" + i + ")/ImageBytes",
							myDoc.getAttribute("/IMAGE_BYTES"));
					outputDoc.setAttribute(documentImagePath + "/FullDocumentImage(" + i + ")/DocName",
							myDoc.getAttribute("/DOC_NAME"));
					// cquinton 7/22/2011 Rel 7.1.0 ppx240 - add image hash
					String imageHash = myDoc.getAttribute("/HASH");
					if (imageHash != null && imageHash.length() > 0) {
						outputDoc.setAttribute(documentImagePath + "/FullDocumentImage(" + i + ")/Hash", imageHash);
					}
				} 

				LOG.debug("[TPLPackagerMediatorBean.packageAttachedDocuments] After adding attached documents outputDoc = {}"	, outputDoc);

			} 
		} 
	} 

	/**
	 * This method handles authorization processing for future value dated payments
	 *
	 * @param transaction
	 *            com.ams.tradeportal.busobj.Transaction
	 * @param mediatorServices
	 *            com.amsinc.ecsg.frame.MediatorServies
	 * @return boolean - true: authorization successful false: authorization failed
	 */
	private boolean processFVD_Authorization(Transaction transaction, MediatorServices mediatorServices)
			throws RemoteException, AmsException {
		boolean value = true;

		if (transaction.getAttribute("transaction_status").equalsIgnoreCase(TransactionStatus.FVD_AUTHORIZED)) {

			// set transaction action for transaction log
			ClientServerDataBridge csdb = transaction.getClientServerDataBridge();
			if (csdb == null)
				csdb = new ClientServerDataBridge();
			csdb.setCSDBValue(TradePortalConstants.TRAN_ACTION, TradePortalConstants.TRAN_ACTION_AUTOMATED_RELEASE);
			transaction.setClientServerDataBridge(csdb);

			LOG.debug("Outgoing Message is a Future Value Dated Payment Transaction tranaaction id = : {}", transaction.getAttribute("transaction_oid"));
			Instrument instrument = (Instrument) mediatorServices.createServerEJB("Instrument",	Long.parseLong(transaction.getAttribute("instrument_oid")));// MDB Rel6.1 IR# RSUL010339811 1/14/11
			value = instrument.authorizeFVDTransaction(transaction); // MDB Rel6.1 IR# RIUL010274654 1/15/11
		}

		return value;
	}
	
	/**
	 * This method handles deleting a record from the outgoing queue
	 *
	 * @param String
	 *            instrumentOid
	 * @param String
	 *            transactionOid
	 * @param mediatorServices
	 *            com.amsinc.ecsg.frame.MediatorServies
	 */
	private void deleteOutgoingQueue(String instrumentOid, String transactionOid, MediatorServices mediatorServices)
			throws AmsException {

		try {
			String deleteQueueSQL = "DELETE FROM OUTGOING_QUEUE WHERE A_INSTRUMENT_OID = ? AND A_TRANSACTION_OID = ?";
			DatabaseQueryBean.executeUpdate(deleteQueueSQL, false, new Object[] { instrumentOid, transactionOid });
		} catch (SQLException | AmsException  e) {
			mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,
					TradePortalConstants.TASK_NOT_SUCCESSFUL, "TPL purging outgoing queue record");
		}
	}
	

	/**
	 * This method fetches the authorizing user_oid when Panel authorization is enabled at Beneficiary level //Rel8.3.0.3
	 * IR#T36000023509
	 */

	private String getBeneAuthUser(String transactionOid, MediatorServices mediatorServices) throws AmsException {
		String oid = "";

		try {
			String getBeneAuthSQL = "SELECT P.A_USER_OID from DMST_PMT_PANEL_AUTH_RANGE D, PANEL_AUTHORIZER P WHERE D.DMST_PMT_PANEL_AUTH_RANGE_OID= P.P_OWNER_OID AND D.P_TRANSACTION_OID =? "
					+ " AND ROWNUM=1 ORDER BY P.AUTH_DATE_TIME DESC";
			DocumentHandler resultXML = DatabaseQueryBean.getXmlResultSet(getBeneAuthSQL, true, new Object[] { transactionOid });
			if (resultXML != null) {
				oid = resultXML.getAttribute("/ResultSetRecord(" + 0 + ")/A_USER_OID");
			}
		} catch (AmsException e) {
			mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,
					TradePortalConstants.TASK_NOT_SUCCESSFUL, "Fetching user_oid when Beneficiary Panel Authorization is enabled");
		}
		return oid;
	}

	/**
	 * This method fetches the authorizing user_oid when Panel authorization is enabled at Transaction level //Rel8.3.0.3
	 * IR#T36000023509
	 */
	private String getTransAuthUser(String transactionOid, MediatorServices mediatorServices) throws AmsException {
		String oid = "";

		try {
			String getTransAuthSQL = "SELECT A_USER_OID FROM PANEL_AUTHORIZER WHERE P_OWNER_OID = ? AND ROWNUM=1 ORDER BY AUTH_DATE_TIME DESC ";
			DocumentHandler resultXML = DatabaseQueryBean.getXmlResultSet(getTransAuthSQL, true, new Object[] { transactionOid });
			if (resultXML != null) {
				oid = resultXML.getAttribute("/ResultSetRecord(" + 0 + ")/A_USER_OID");
			}
		} catch (AmsException e) {
			mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,
					TradePortalConstants.TASK_NOT_SUCCESSFUL, "Fetching user_oid when Panel Authorization is enabled");
		}
		return oid;
	}

	/**
	 * This method is used package Settlement Instruction details for SIM, SIR and DCR(if settlement include check box checked)
	 * transactions
	 * 
	 * @param customerEnteredTerms
	 * @param transaction
	 * @param mediatorServices
	 * @param outputDoc
	 * @throws RemoteException
	 * @throws AmsException
	 */
	private void packageSettlementInstructionDetail(Terms customerEnteredTerms, Transaction transaction,
			MediatorServices mediatorServices, DocumentHandler outputDoc) throws RemoteException, AmsException {

		String settlementInstructionDetailPath = UniversalMessage.settlementInstructionDetailPath;

		outputDoc.setAttribute(settlementInstructionDetailPath + "/CustomerReference",
				customerEnteredTerms.getAttribute("our_reference"));
		outputDoc.setAttribute(settlementInstructionDetailPath + "/SettlementInstructionWorkItemType",
				transaction.getAttribute("settle_instr_work_item_type"));
		outputDoc.setAttribute(settlementInstructionDetailPath + "/ApplyPaymentOnDate",
				customerEnteredTerms.getAttribute("apply_payment_on_date"));
		outputDoc.setAttribute(settlementInstructionDetailPath + "/PayInFullFinanceRolloverIndicator",
				customerEnteredTerms.getAttribute("pay_in_full_fin_roll_ind"));

		if (StringFunction.isNotBlank(customerEnteredTerms.getAttribute("account_remit_ind"))) {
			outputDoc.setAttribute(settlementInstructionDetailPath + "/AccountRemitIndicator",
					customerEnteredTerms.getAttribute("account_remit_ind"));
		}

		outputDoc.setAttribute(settlementInstructionDetailPath + "/FinanceRolloverFullPartialIndicator",
				customerEnteredTerms.getAttribute("fin_roll_full_partial_ind"));

		if (TradePortalConstants.SETTLEMENT_ACCOUNT_REMIT_TYPE_ACCOUNT
				.equals(customerEnteredTerms.getAttribute("account_remit_ind"))) {

			packageSettlmentDebitAccount(customerEnteredTerms, mediatorServices, transaction.getAttribute("transaction_type_code"),
					outputDoc);

		}

		if (TradePortalConstants.SETTLEMENT_PAY_FULL_FIN_ROLL_TYPE_FINANCE
				.equals(customerEnteredTerms.getAttribute("pay_in_full_fin_roll_ind"))
				|| TradePortalConstants.SETTLEMENT_PAY_FULL_FIN_ROLL_TYPE_ROLL
						.equals(customerEnteredTerms.getAttribute("pay_in_full_fin_roll_ind"))) {

			if (TradePortalConstants.SETTLEMENT_ROLLOVER_TERMS_DAYS
					.equals(customerEnteredTerms.getAttribute("rollover_terms_ind"))) {
				outputDoc.setAttribute(settlementInstructionDetailPath + "/FinanceRolloverNumberOfDays",
						customerEnteredTerms.getAttribute("fin_roll_number_of_days"));
			}

			outputDoc.setAttribute(settlementInstructionDetailPath + "/FinanceRolloverCurrency",
					customerEnteredTerms.getAttribute("fin_roll_curr"));

			if (TradePortalConstants.SETTLEMENT_ROLLOVER_TERMS_MATURITY_DATE
					.equals(customerEnteredTerms.getAttribute("rollover_terms_ind"))) {
				outputDoc.setAttribute(settlementInstructionDetailPath + "/FinanceRolloverMaturityDate",
						customerEnteredTerms.getAttribute("fin_roll_maturity_date"));
			}
		}

		if (TradePortalConstants.SETTLEMENT_FIN_ROLL_FULL_PARTIAL_TYPE_PARTIAL
				.equals(customerEnteredTerms.getAttribute("fin_roll_full_partial_ind"))) {
			outputDoc.setAttribute(settlementInstructionDetailPath + "/FinanceRolloverPartialPayAmount",
					customerEnteredTerms.getAttribute("fin_roll_partial_pay_amt"));
		}

		if (TradePortalConstants.INDICATOR_YES.equals(customerEnteredTerms.getAttribute("other_addl_instructions_ind"))) {
			outputDoc.setAttribute(settlementInstructionDetailPath + "/OtherAdditionalInstructions",
					customerEnteredTerms.getAttribute("other_addl_instructions"));
		}

		outputDoc.setAttribute(settlementInstructionDetailPath + "/DailyRateFecOtherIndicator",
				customerEnteredTerms.getAttribute("daily_rate_fec_oth_ind"));

		if (TradePortalConstants.SETTLEMENT_DAILY_RATE_FEC_TYPE_FXCONTRACT
				.equals(customerEnteredTerms.getAttribute("daily_rate_fec_oth_ind"))) {

			outputDoc.setAttribute(settlementInstructionDetailPath + "/CoveredByFecNumber",
					customerEnteredTerms.getAttribute("settle_covered_by_fec_num"));

			outputDoc.setAttribute(settlementInstructionDetailPath + "/FecRate",
					customerEnteredTerms.getAttribute("settle_fec_rate"));

			outputDoc.setAttribute(settlementInstructionDetailPath + "/FecCurrency",
					customerEnteredTerms.getAttribute("settle_fec_currency"));
		}

		if (TradePortalConstants.SETTLEMENT_DAILY_RATE_FEC_TYPE_OTHER
				.equals(customerEnteredTerms.getAttribute("daily_rate_fec_oth_ind"))) {

			outputDoc.setAttribute(settlementInstructionDetailPath + "/OtherFecText",
					customerEnteredTerms.getAttribute("settle_other_fec_text"));
		}

	}

	/**
	 * This method is used to package all debit account for settlement instrcution message and response.
	 * 
	 * @param customerEnteredTerms
	 * @param mediatorServices
	 * @param outputDoc
	 * @throws RemoteException
	 * @throws AmsException
	 */
	private void packageSettlmentDebitAccount(Terms customerEnteredTerms, MediatorServices mediatorServices, String transType,
			DocumentHandler outputDoc) throws RemoteException, AmsException {

		String settlementInstructionDetailPath = UniversalMessage.settlementInstructionDetailPath;
		outputDoc.setAttribute(settlementInstructionDetailPath + "/PrincipalAccount", "");
		outputDoc.setAttribute(settlementInstructionDetailPath + "/PrincipalAccountCCY", "");
		outputDoc.setAttribute(settlementInstructionDetailPath + "/ChargesAccount", "");
		outputDoc.setAttribute(settlementInstructionDetailPath + "/ChargesAccountCCY", "");
		outputDoc.setAttribute(settlementInstructionDetailPath + "/InterestAccount", "");
		outputDoc.setAttribute(settlementInstructionDetailPath + "/InterestAccountCCY", "");

		String principalAcctOid = customerEnteredTerms.getAttribute("principal_account_oid");
		String chargesAcctOid = customerEnteredTerms.getAttribute("charges_account_oid");
		String interestAcctOid = null;
		if (!TransactionType.DISCREPANCY.equals(transType)) {
			interestAcctOid = customerEnteredTerms.getAttribute("interest_account_oid");
		}
		String sql = " SELECT account_oid, account_number, currency FROM account WHERE account_oid in ( ?, ?, ? ) ";
		DocumentHandler resultXML = DatabaseQueryBean.getXmlResultSet(sql, true,
				new Object[] { principalAcctOid, chargesAcctOid, interestAcctOid });

		if (resultXML != null) {
			List<DocumentHandler> accountDocs = resultXML.getFragmentsList("/ResultSetRecord");
			for (DocumentHandler doc : accountDocs) {
				String resultAcctOid = doc.getAttribute("/ACCOUNT_OID");
				String resultAcctNum = doc.getAttribute("/ACCOUNT_NUMBER");
				String resultAcctCurr = doc.getAttribute("/CURRENCY");
				if (StringFunction.isNotBlank(principalAcctOid) && principalAcctOid.equals(resultAcctOid)) {
					outputDoc.setAttribute(settlementInstructionDetailPath + "/PrincipalAccount", resultAcctNum);
					outputDoc.setAttribute(settlementInstructionDetailPath + "/PrincipalAccountCCY", resultAcctCurr);
				}
				if (StringFunction.isNotBlank(chargesAcctOid) && chargesAcctOid.equals(resultAcctOid)) {
					outputDoc.setAttribute(settlementInstructionDetailPath + "/ChargesAccount", resultAcctNum);
					outputDoc.setAttribute(settlementInstructionDetailPath + "/ChargesAccountCCY", resultAcctCurr);
				}
				if (StringFunction.isNotBlank(interestAcctOid) && interestAcctOid.equals(resultAcctOid)) {
					outputDoc.setAttribute(settlementInstructionDetailPath + "/InterestAccount", resultAcctNum);
					outputDoc.setAttribute(settlementInstructionDetailPath + "/InterestAccountCCY", resultAcctCurr);
				}
			}
		}
	}

	/**
	 * This method is used to include userDefined Header. this userDefined Header is used to non-TPS integrated customer.
	 * 
	 * @param instrument
	 * @param corpOrg
	 * @param outputDoc
	 * @throws AmsException
	 * @throws RemoteException
	 */
	private void packageUserHeader(Instrument instrument, CorporateOrganization corpOrg, DocumentHandler outputDoc)
			throws RemoteException, AmsException {

		String userHeader = UniversalMessage.userHeaderPath;
		if (StringFunction.isNotBlank(corpOrg.getAttribute("user_defined_field_1"))) {
			outputDoc.setAttribute(userHeader + "/UserHeader1", corpOrg.getAttribute("user_defined_field_1"));
		}
		if (StringFunction.isNotBlank(corpOrg.getAttribute("user_defined_field_2"))) {
			outputDoc.setAttribute(userHeader + "/UserHeader2", corpOrg.getAttribute("user_defined_field_2"));
		}
		if (StringFunction.isNotBlank(corpOrg.getAttribute("name"))) {
			outputDoc.setAttribute(userHeader + "/UserHeader3", corpOrg.getAttribute("name"));
		}
		if (StringFunction.isNotBlank(instrument.getAttribute("copy_of_instrument_amount"))) {
			outputDoc.setAttribute(userHeader + "/UserHeader4", instrument.getAttribute("copy_of_instrument_amount"));
		}
		outputDoc.setAttribute(userHeader + "/UserHeader5", "");

	}

}
