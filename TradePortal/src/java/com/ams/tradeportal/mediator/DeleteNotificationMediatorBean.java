package com.ams.tradeportal.mediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;

import com.amsinc.ecsg.frame.*;
import com.ams.tradeportal.busobj.*;
import com.ams.tradeportal.common.*;
import com.ams.tradeportal.busobj.util.SecurityAccess;

/*
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class DeleteNotificationMediatorBean extends MediatorBean
{
private static final Logger LOG = LoggerFactory.getLogger(DeleteNotificationMediatorBean.class);
    //some options for the OrgOption parameter
    final static String ALL_NOTIFS = "Notifications.All";    
    
  /**
   * This method performs deletions of Transactions
   * The format expected from the input doc is:
   *
   * <TransactionList>
   *	<Transaction ID="0"><transactionData>1000001/1200001/AMD</transactionData></Transaction>
   *	<Transaction ID="1"><transactionData>1000002/1200002/ISS</transactionData></Transaction>
   *	<Transaction ID="2"><transactionData>1000003/1200003/TRN</transactionData></Transaction>
   *	<Transaction ID="3"><transactionData>1000004/1200004/ASG</transactionData></Transaction>
   *	<Transaction ID="6"><transactionData>1000001/1200007/ISS</transactionData></Transaction>
   * </TransactionList>
   * <User>
   *    <userOid>1001</userOid>
   *    <securityRights>302231454903657293676543</securityRights>
   * </User>
   *
   * The transactionData value consists of an transaction oid, a '/', instrument oid, a '/',
   * and the transaction type.
   *
   * This is a non-transactional mediator that executes transactional
   * methods in business objects
   *
   * @param inputDoc The input XML document (&lt;In&gt; section of overall
   * Mediator XML document)
   * @param outputDoc The output XML document (&lt;Out&gt; section of overall
   * Mediator XML document)
   * @param mediatorServices Associated class that contains lots of the
   * auxillary functionality for a mediator.
   */

   public DocumentHandler execute(DocumentHandler inputDoc, DocumentHandler outputDoc, MediatorServices mediatorServices)
								  throws RemoteException, AmsException
   {
	  // get each Transaction Oid and process each transaction; each transaction is unique within itself
	  mediatorServices.debug("DeleteNotificationMediator: " + inputDoc);

	  //cquinton 3/5/2013 Rel PR ir#14542 add delete all behavior
	  String buttonPressed = inputDoc.getAttribute("/Update/ButtonPressed");
	  
	   // Nar Rel 9.2.0.0 04-Sep-2014 ADD - Begin
	  SecurityAccess.validateUserRightsForTrans("NOTIFICATION", "", inputDoc.getAttribute("/User/securityRights"), TradePortalConstants.BUTTON_DELETE);
	  // Nar Rel 9.2.0.0 04-Sep-2014 ADD - End
	  
	  if ( TradePortalConstants.BUTTON_DELETE.equals(buttonPressed) ) {
	      outputDoc = deleteNotificationsById(inputDoc, outputDoc, mediatorServices);
	  }
	  else if ( TradePortalConstants.BUTTON_DELETE_ALL.equals(buttonPressed ) ) {
	      outputDoc = deleteNotificationsByRange(inputDoc, outputDoc, mediatorServices);
	  }

	  mediatorServices.debug("DeleteNotificationMediator: " + outputDoc);

	  return outputDoc;
   }

   private DocumentHandler deleteNotificationsById(DocumentHandler inputDoc, 
           DocumentHandler outputDoc, MediatorServices mediatorServices)
                                  throws RemoteException, AmsException {
      DocumentHandler   transaction          = null;
      Instrument        instrument           = null;
      String[]          substitutionParms    = null;
      Vector            transactionsList     = null;
      String            transactionTypeDescr = null;
      String            transactionData      = null;
      String            transactionType      = null;
      String            transactionOid       = null;
      String            instrumentOid        = null;
      String            securityRights       = null;
      String            userLocale           = null;
      String            userOid              = null;
      int               separator1           = 0;
      int               separator2           = 0;
      int               count                = 0;
      List<String> listInstrumentId=new ArrayList<String>();
      boolean isDeleteNotification=false;
      
      // Populate variables with inputDocument information such as transaction list, user oid and
      // security rights, etc
      transactionsList = inputDoc.getFragments("/TransactionList/Transaction");
      userOid          = inputDoc.getAttribute("/User/userOid");
      securityRights   = inputDoc.getAttribute("../securityRights");
      count            = transactionsList.size();

      // Get the locale from client server data bridge, used for getting locale specific
      // information when passing error parameters
      userLocale = mediatorServices.getCSDB().getLocaleName();

      mediatorServices.debug("DeleteNotificationMediator: number of transactions: " + count);

      if (count <= 0) {
         mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                                                       TradePortalConstants.NO_ITEM_SELECTED,
            mediatorServices.getResourceManager().getText("TransactionAction.DeleteNotifications",
                               TradePortalConstants.TEXT_BUNDLE));

         return outputDoc;
      }

      // A transaction should be locked and processed at the instrument level
      // since the transaction is a component of instrument.
      // For each transaction, lock the instrument and call deleteTransaction
      // on the instrument.  deleteTransaction is a transactional method.
      for (int i = 0; i < count; i++)
      {
         transaction     = (DocumentHandler) transactionsList.get(i);
         transactionData = transaction.getAttribute("/transactionData");

         // Split up the instrument oid, transaction oid and transaction type
         // It is possible to use StringTokenizer to break the data up, but since
         // there are only two separators, this is fine for now.
         separator1      = transactionData.indexOf('/');
         separator2      = transactionData.indexOf('/', separator1 + 1);

         transactionOid  = transactionData.substring(0, separator1);
         instrumentOid   = transactionData.substring(separator1 + 1, separator2);

         transactionType = transactionData.substring(separator2 + 1);

         mediatorServices.debug("DeleteNotificationMediator: transactionData contents: " + transactionData +
                                " Instrument oid: " + instrumentOid + " Transaction oid: " + transactionOid +
                                " Transaction Type: " + transactionType);

         // Before creating the instrument, attempt to lock the instrument
         try
         {
            LockingManager.lockBusinessObject(Long.parseLong(instrumentOid), Long.parseLong(userOid), true);

            mediatorServices.debug("DeleteNotificationMediator: Locking instrument " + instrumentOid + " successful");
         }
         catch (InstrumentLockException e)
         {
            // issue "instrument in use error" indicating instrument ID,
            // transaction type, first name and last name of user that has instrument
            // reserved
            transactionTypeDescr = ReferenceDataManager.getRefDataMgr().getDescr("TRANSACTION_TYPE", transactionType,
                                                                                 userLocale);

            substitutionParms = new String[] {e.getInstrumentNumber(), transactionTypeDescr, e.getFirstName(), e.getLastName(),
                               mediatorServices.getResourceManager().getText("TransactionAction.DeleteNotification",
                                   TradePortalConstants.TEXT_BUNDLE)};

            mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                                                          TradePortalConstants.TRANSACTION_IN_USE, substitutionParms);

            continue;
         }

         try
         {
            // create and load the instrument and call deleteTransaction
            instrument = (Instrument) mediatorServices.createServerEJB("Instrument", Long.parseLong(instrumentOid));
            listInstrumentId.add(instrument.getAttribute("complete_instrument_id"));
            if (listInstrumentId.size()== count){
                isDeleteNotification=true;
            }
            if (count<2){
                instrument.deleteNotification(transactionOid, securityRights);
            }else{
                instrument.deleteAllNotification(transactionOid, securityRights,listInstrumentId,isDeleteNotification);
            }
         }
         finally
         {
            try
            {
               LockingManager.unlockBusinessObjectByInstrument(Long.parseLong(instrumentOid), true);
            }
            catch (InstrumentLockException e)
            {
               // this should never happen
               e.printStackTrace();
            }
         }
      }       
      return outputDoc;
   }
   
   

   private DocumentHandler deleteNotificationsByRange(DocumentHandler inputDoc, 
           DocumentHandler outputDoc, MediatorServices mediatorServices) 
                                  throws RemoteException, AmsException {
      DocumentHandler   transaction          = null;
      Instrument        instrument           = null;
      String[]          substitutionParms    = null;
      String            transactionTypeDescr = null;
      String            transactionData      = null;
      String            transactionType      = null;
      String            transactionOid       = null;
      String            instrumentOid        = null;
      String            securityRights       = null;
      String            userLocale           = null;
      int               separator1           = 0;
      int               separator2           = 0;
      int               count                = 0;
      List<String> listInstrumentId=new ArrayList<String>();
      boolean isDeleteNotification=false;
      
      // Populate variables with inputDocument information such as transaction list, user oid and
      // security rights, etc       
      String currentOrgOption = inputDoc.getAttribute("/currentOrgOption");
      String status = inputDoc.getAttribute("/status");
      String userOid = inputDoc.getAttribute("/User/userOid");
      String userOrgOid = inputDoc.getAttribute("/User/userOrgOid");
      String confInd = inputDoc.getAttribute("/User/confInd");
      securityRights = inputDoc.getAttribute("../securityRights");
      
      // decrypt the currentOrgOption
      if ( currentOrgOption!=null && currentOrgOption.length()>0 ) {
          String secretKeyString = mediatorServices.getCSDB().getCSDBValue("SecretKey");
          byte[] keyBytes = com.amsinc.ecsg.util.EncryptDecrypt.base64StringToBytes(secretKeyString);
          javax.crypto.SecretKey secretKey = new javax.crypto.spec.SecretKeySpec(keyBytes, "DESede/CBC/PKCS5Padding");
          currentOrgOption = EncryptDecrypt.decryptStringUsingTripleDes(currentOrgOption, secretKey);
      }
      else {
          //default to users org
          currentOrgOption = userOrgOid;
      }
      
      // Get the locale from client server data bridge, used for getting locale specific
      // information when passing error parameters
      userLocale = mediatorServices.getCSDB().getLocaleName();
     //jgadela R91 IR T36000026319 - SQL INJECTION FIX
      List<Object> sqlParams = new ArrayList();

      //execute the query - note criteria is same as notifications query
      String sql = "select a.transaction_oid, b.instrument_oid, b.complete_instrument_id, b.instrument_type_code, a.transaction_type_code "; 
      sql += "from transaction a, instrument b, terms_party c ";
      sql += "where a.p_instrument_oid = b.instrument_oid and " + 
             "b.a_counter_party_oid = c.terms_party_oid(+) and a.show_on_notifications_tab='Y' and " + 
             "(a.transaction_status in ('PROCESSED_BY_BANK', 'CANCELLED_BY_BANK', 'REJECTED_PAYMENT', 'PARTIALLY_REJECTED_PAYMENT') or " +
             "(a.rejection_indicator is not null and a.transaction_status not in('PROCESSED_BY_BANK'))) ";
      
      String buildDynamicSQLCriteria = buildDynamicSQLCriteria(userOrgOid, currentOrgOption, confInd, status, sqlParams);
      sql = sql + buildDynamicSQLCriteria;
      
      mediatorServices.debug("DeleteAllNotificationMediator: sql: " + sql);
      
      Vector notiVector = null;	
    //jgadela R91 IR T36000031670 - SQL INJECTION FIX
      DocumentHandler notificationList = DatabaseQueryBean.getXmlResultSet(sql, false, sqlParams);
      notiVector = notificationList.getFragments("/ResultSetRecord");
      
      mediatorServices.debug("DeleteNotificationMediator: number of transactions: " + count);
      
      count = notiVector.size();
      if (count <= 0) {
         mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                                                       TradePortalConstants.NO_ITEM_SELECTED,
            mediatorServices.getResourceManager().getText("TransactionAction.DeleteNotifications",
                               TradePortalConstants.TEXT_BUNDLE));

         return outputDoc;
      }
      //spin through and create the instrumetn beans, making sure to lock
      // them so they are not modified when someone else has a lock
      for (int iLoop=0;iLoop<count;iLoop++) {
         DocumentHandler notificationDoc = (DocumentHandler) notiVector.elementAt(iLoop);
      
         transactionOid  = notificationDoc.getAttribute("/TRANSACTION_OID");
         instrumentOid   = notificationDoc.getAttribute("/INSTRUMENT_OID");
         transactionType = notificationDoc.getAttribute("/TRANSACTION_TYPE_CODE");

         mediatorServices.debug("DeleteNotificationMediator: transactionData contents: " + transactionData +
                                " Instrument oid: " + instrumentOid + " Transaction oid: " + transactionOid +
                                " Transaction Type: " + transactionType);
         // Before creating the instrument, attempt to lock the instrument
         try
         {
            LockingManager.lockBusinessObject(Long.parseLong(instrumentOid), Long.parseLong(userOid), true);

            mediatorServices.debug("DeleteNotificationMediator: Locking instrument " + instrumentOid + " successful");
         }
         catch (InstrumentLockException e)
         {
            // issue "instrument in use error" indicating instrument ID,
            // transaction type, first name and last name of user that has instrument
            // reserved
            transactionTypeDescr = ReferenceDataManager.getRefDataMgr().getDescr("TRANSACTION_TYPE", transactionType, userLocale);

            substitutionParms = new String[] {e.getInstrumentNumber(), transactionTypeDescr, e.getFirstName(), e.getLastName(),
                               mediatorServices.getResourceManager().getText("TransactionAction.DeleteNotification",
                                   TradePortalConstants.TEXT_BUNDLE)};

            mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                                                          TradePortalConstants.TRANSACTION_IN_USE, substitutionParms);

            continue;
         }

         try
         {
            // create and load the instrument and call deleteTransaction
            instrument = (Instrument) mediatorServices.createServerEJB("Instrument", Long.parseLong(instrumentOid));
            listInstrumentId.add(instrument.getAttribute("complete_instrument_id"));
            if (listInstrumentId.size()== count){
                isDeleteNotification=true;
            }
            if (count<2){
                instrument.deleteNotification(transactionOid, securityRights);
            }else{
                instrument.deleteAllNotification(transactionOid, securityRights,listInstrumentId,isDeleteNotification);
            }
         }
         finally
         {
            try
            {
               LockingManager.unlockBusinessObjectByInstrument(Long.parseLong(instrumentOid), true);
            }
            catch (InstrumentLockException e)
            {
               // this should never happen
               e.printStackTrace();
            }
         }
      }       
      
      return outputDoc;
   }
   
    protected String buildDynamicSQLCriteria(String userOrgOid, String currentOrgOption, String confInd, String status, List<Object> sqlParams) {

        StringBuffer sql = new StringBuffer();
        StringBuffer dynamicWhereClause = new StringBuffer();

        String where = "";
       
        if(currentOrgOption == null)currentOrgOption="";
        if(confInd == null)confInd="";
        if(status == null)status="";

        if (this.ALL_NOTIFS.equals(currentOrgOption)){
            where = " and b.a_corp_org_oid in (select organization_oid " +
                " from corporate_org where activation_status = ? " +
                " start with organization_oid = ?" + 
                " connect by prior organization_oid = p_parent_corp_org_oid)";
            sqlParams.add("ACTIVE");
            sqlParams.add(userOrgOid);
        }
        else if(currentOrgOption!=null && !currentOrgOption.equals("")){
       	    where = " and b.a_corp_org_oid = ? " ;
            sqlParams.add(currentOrgOption);
        }
        else{
       	    where = " and 1=0" ;
    	}

       	if (TradePortalConstants.INDICATOR_NO.equals(confInd)) {
            //Use INSTRUMENT table with Notification List View for Conf Indicator Check
            String tableNameAlias = "b";
            dynamicWhereClause.append(" and (").append(tableNameAlias).append(".confidential_indicator = 'N' OR ");
            dynamicWhereClause.append(tableNameAlias).append(".confidential_indicator is null) ");
        }

        if (!"".equals(status) && !status.equals(ALL_NOTIFS)) {
            dynamicWhereClause.append(" and a.transaction_status in (?)");
            sqlParams.add(status);
        }

        where = where + dynamicWhereClause.toString();

        sql.append(where);

        LOG.info("***SQL Query: <<Notifications Dataview>>: "+sql.toString());

        return sql.toString();
    }

}