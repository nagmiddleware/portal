package com.ams.tradeportal.mediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.PreparedStatement;

import com.ams.tradeportal.busobj.ActiveUserSession;
import com.ams.tradeportal.busobj.User;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.frame.InstrumentLockException;
import com.amsinc.ecsg.frame.LockingManager;
import com.amsinc.ecsg.frame.MediatorBean;
import com.amsinc.ecsg.frame.MediatorServices;
import com.amsinc.ecsg.util.DateTimeUtility;
import com.amsinc.ecsg.util.DocumentHandler;

/*
 * ReLogon behavior.
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class ReLogonMediatorBean extends MediatorBean
{
private static final Logger LOG = LoggerFactory.getLogger(ReLogonMediatorBean.class);
    /* 
     * Called by the processRequest() method of the Ancestor class to process the designated 
     * transaction.  The input parameters for the transaction are received in an XML document
     * (inputDoc).  The execute() method is responsible for calling one or more business objects
     * to process the transaction and populating the outputDoc XML document with the results
     * of the transaction.
     */
    public DocumentHandler execute(DocumentHandler inputDoc,
            DocumentHandler outputDoc, MediatorServices mediatorServices)
            throws RemoteException, AmsException {
        String userOidStr = inputDoc.getAttribute("/userOid");
        
        User user = getUser( userOidStr, mediatorServices);
        
        //now indicate the user has logged in
        user.setAttribute("last_login_timestamp", user.getAttribute("login_timestamp"));//CR 821 Rel 8.3
        user.setAttribute("login_timestamp",DateTimeUtility.getGMTDateTime());

        //set other user stuff for newuser/expired user
        boolean isExpired = user.isPasswordExpired();
        if( isExpired ) {
            outputDoc.setAttribute("/passwordExpire","true");
        }
        if( user.getAttribute("new_user_ind").equals(TradePortalConstants.INDICATOR_YES)) {
            //cquinton 12/23/2012 security update - cannot just set new user indicator
            // here - we need to wait until the user actually changes their password
            //user.setAttribute("new_user_ind",TradePortalConstants.INDICATOR_NO);
            outputDoc.setAttribute("/newUser","true");
        }

        user.save(false);

        //create active user session
        //Note: an alternative strategy is to create this in LogonMediatorBean
        // so that it is guaranteed to be available.  However, we do 
        // here because it makes sense that the 'virtual' session doesn't actually
        // start until this point.  This only works if it is guaranteed that
        // instrument locks or other resources that are cleared on
        // app server session timeout are not created until this point.
        ActiveUserSession activeSession = (ActiveUserSession) 
            mediatorServices.createServerEJB("ActiveUserSession");
        activeSession.newObject();
        activeSession.setAttribute("user_oid", user.getAttribute("user_oid") );
        activeSession.setAttribute("logon_type","relogon");                    
        activeSession.setAttribute("confirm_relogon",TradePortalConstants.INDICATOR_YES);
        activeSession.save();
                    
        //return the new active user session identifier
        String activeUserSessionOid = activeSession.getAttribute("active_user_session_oid");
        outputDoc.setAttribute("/activeUserSessionOid", activeUserSessionOid);
        
        outputDoc.setAttribute("/lastLoginTimestamp", user.getAttribute("last_login_timestamp")); //CR 821 - Rel 8.3
        
        //logout the users other sessions
        logoutOtherSessions( userOidStr, activeUserSessionOid );
        
        //clear all locks for the user
        try {
            LockingManager.unlockBusinessObjectByUser(new Long(userOidStr), true);
        }
        catch (InstrumentLockException e) {
            // this should never happen
            e.printStackTrace();
        }
	    
	    return outputDoc;
    }
    
    private User getUser(String userOidStr, MediatorServices mediatorServices ) 
            throws AmsException, RemoteException {
        Long userOid = null;
        try {
            userOid = Long.parseLong(userOidStr);
        }
        catch (Exception e) {
            //LOG.info("Logon Mediator error: " + e.toString());
            e.printStackTrace();
            mediatorServices.getErrorManager().issueError(
                getClass().getName(), AmsConstants.INVALID_ATTRIBUTE,
                userOidStr, "user_oid");
        }
            
        // Create the user business object using mediator services and instantiate it
        User user = (User) mediatorServices.createServerEJB("User");
        try {
            user.getData(userOid);
        }
        catch (Exception e) {
            //LOG.info("Logon Mediator error: " + e.toString());
            e.printStackTrace();
            mediatorServices.getErrorManager().issueError(
                getClass().getName(), AmsConstants.INVALID_ATTRIBUTE,
                String.valueOf(userOid), "user_oid");
        }
        
        return user;
    }

    private void logoutOtherSessions( String userOidStr, String activeUserSessionOidStr )
            throws AmsException {

        //set the force_logout flag for the user's other sessions
        //this is checked by header.jsp in the session to determine if 
        // should force logout
        try(Connection con = DatabaseQueryBean.connect(true);
            PreparedStatement pStmt = con.prepareStatement(
                "update active_user_session set " +
                "force_logout='Y' " +
                "where a_user_oid=? and active_user_session_oid!=?" )) {
            
            pStmt.setString(1, userOidStr);
            pStmt.setString(2, activeUserSessionOidStr);
            pStmt.executeUpdate();
            

        } catch (Exception ex) {
            //because this is not vital to processing, ignore it but create a log message
            System.err.println("Problem setting force_logout flag for user's other sessions. " +
                " User Oid: " + userOidStr + " This session: " + activeUserSessionOidStr + 
                " Exception: " + ex.toString());
            throw new AmsException("Problem setting force_logout flag for user's other sessions.", ex);
        }
    }
}