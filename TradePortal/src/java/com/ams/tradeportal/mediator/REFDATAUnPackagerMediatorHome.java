package com.ams.tradeportal.mediator;

import com.amsinc.ecsg.frame.AmsException;
import java.rmi.RemoteException;
import javax.ejb.EJBHome;
import javax.ejb.CreateException;

/*
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface REFDATAUnPackagerMediatorHome extends EJBHome
{
  public REFDATAUnPackagerMediator create() throws RemoteException, AmsException, CreateException;
}

