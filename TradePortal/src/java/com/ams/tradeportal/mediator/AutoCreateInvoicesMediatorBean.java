package com.ams.tradeportal.mediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.math.MathContext;
import java.rmi.RemoteException;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;
import java.util.Vector;
import java.util.List;
import java.util.ArrayList;

import com.ams.tradeportal.busobj.ArMatchingRule;
import com.ams.tradeportal.busobj.ClientBank;
import com.ams.tradeportal.busobj.CorporateOrganization;
import com.ams.tradeportal.busobj.Instrument;
import com.ams.tradeportal.busobj.InvoicesSummaryData;
import com.ams.tradeportal.busobj.Terms;
import com.ams.tradeportal.busobj.TermsParty;
import com.ams.tradeportal.busobj.Transaction;
import com.ams.tradeportal.busobj.util.InvoiceLogger;
import com.ams.tradeportal.busobj.util.InvoiceSummaryDataObject;
import com.ams.tradeportal.busobj.util.InvoiceUtility;
import com.ams.tradeportal.busobj.util.POLineItemUtility;
import com.ams.tradeportal.common.InstrumentType;
import com.ams.tradeportal.common.TPDateTimeUtility;
import com.ams.tradeportal.common.TermsPartyType;
import com.ams.tradeportal.common.TradePortalConstants;
import com.ams.tradeportal.common.cache.Cache;
import com.ams.tradeportal.common.cache.TPCacheManager;
import com.amsinc.ecsg.frame.AmsConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.frame.InvalidObjectIdentifierException;
import com.amsinc.ecsg.frame.MediatorBean;
import com.amsinc.ecsg.frame.MediatorServices;
import com.amsinc.ecsg.frame.ObjectIDCache;
import com.amsinc.ecsg.frame.ReferenceDataManager;
import com.amsinc.ecsg.util.DateTimeUtility;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.EncryptDecrypt;
import com.amsinc.ecsg.util.StringFunction;


/*
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class AutoCreateInvoicesMediatorBean extends MediatorBean {
private static final Logger LOG = LoggerFactory.getLogger(AutoCreateInvoicesMediatorBean.class);
  // This SQL selects information about all the creation rules
  // for an org.

  private String SELECT_SQL = "";

  // This additional logic is needed to make sure the rules are sorted in following order always at the top
  // 1. RULE with max number of criteria Columns defined
  // 2. if more than 1 rule has max number of colmns defined then the Rule with highest max amount defined and then widest min max days range is always at the top
  // This ensure that invoice are matched against rule which are very specific in nature as this rule is the best match for the selected
  // invoices.
  // In short we are trying to find a rule which best matches the invoice for grouping.

  private String EXTRA_SELECT_SQL =" ,  due_or_pay_max_days- due_or_pay_min_days AS daysDiff ,  " +
  								   "CASE    WHEN inv_only_create_rule_max_amt IS NULL    THEN 0    ELSE 1  END +  " +
  								   "CASE    WHEN due_or_pay_max_days IS NULL    AND due_or_pay_min_days  IS NULL    THEN 0    ELSE 1  END +  " +
  								   "CASE    WHEN inv_data_item1 IS NULL    THEN 0    ELSE 1  END +  " +
  								   "CASE    WHEN inv_data_item2 IS NULL    THEN 0    ELSE 1  END +  " +
  								   "CASE    WHEN inv_data_item3 IS NULL    THEN 0    ELSE 1  END +  " +
  								   "CASE    WHEN inv_data_item4 IS NULL    THEN 0    ELSE 1  END +  " +
  								   "CASE    WHEN inv_data_item5 IS NULL    THEN 0    ELSE 1  END +  " +
  								   "CASE    WHEN inv_data_item6 IS NULL    THEN 0    ELSE 1  END AS colCount,  " +
  								   "CASE    WHEN inv_only_create_rule_max_amt IS NULL    THEN 1    ELSE 0    END +  " +
  								   "CASE    WHEN due_or_pay_max_days IS NULL    AND due_or_pay_min_days  IS NULL    THEN 1    ELSE 0  END AS tempCount ,  " +
  								   "(  CASE    WHEN inv_only_create_rule_max_amt IS NULL    THEN 0    ELSE 1  END +  " +
  								   "CASE    WHEN due_or_pay_max_days IS NULL    AND due_or_pay_min_days  IS NULL    THEN 0    ELSE 1  END +  " +
  								   "CASE    WHEN inv_data_item1 IS NULL    THEN 0    ELSE 1  END +  " +
  								   "CASE    WHEN inv_data_item2 IS NULL    THEN 0    ELSE 1  END +  " +
  								   "CASE    WHEN inv_data_item3 IS NULL    THEN 0    ELSE 1  END +  " +
  								   "CASE    WHEN inv_data_item4 IS NULL    THEN 0    ELSE 1  END +  " +
  								   "CASE    WHEN inv_data_item5 IS NULL    THEN 0    ELSE 1  END +  " +
  								   "CASE    WHEN inv_data_item6 IS NULL    THEN 0    ELSE 1  END) - (  " +
  								   "CASE     WHEN inv_only_create_rule_max_amt IS NULL    THEN 1    ELSE 0    END +  " +
  								   "CASE    WHEN due_or_pay_max_days IS NULL    AND due_or_pay_min_days  IS NULL    THEN 1    ELSE 0  END) as colSort,  " +
  								   "CASE    WHEN due_or_pay_max_days IS NULL    AND due_or_pay_min_days  IS NULL    AND inv_only_create_rule_max_amt IS NULL    THEN 0    ELSE 1  END as tCol ";
private static String ORDERBY_CLAUSE = " ORDER BY  tCol DESC nulls last, colSort DESC nulls last,  inv_only_create_rule_max_amt DESC nulls last ,  " +
									   " daysDiff DESC nulls last,  creation_date_time DESC nulls last ";

  // This SQL selects unassigned po line items for an org.  The remaining where clause criteria is added dynamically.  They are
  // ordered by ben_name and currency for processing logic.  They are also ordered by upload date/time to provide some consistency
  // for transaction assignment.
  
  private static String INV_SELECT_SQL = "select upload_invoice_oid, seller_name, buyer_name, currency, amount, "
      + "a_upload_definition_oid, invoice_id " + ", {0} , pay_method , invoice_classification "//BSL IR SRUM042154839 04/24/2012 Rel 8.0 - add PO Def
      + "from invoices_view ";
  
  private static String INV_WHERE_SQL=  " where  NVL(deleted_ind,'N') != 'Y' and a_transaction_oid is NULL "
      + "and a_corp_org_oid = ";

  private transient String invUploadInstrType = null;
  private transient String loanType = null;
  private transient String financeType = null;
  private transient String importIndicator = null;
  private transient String invoiceClassification = null;
  private transient String buyerBacked = null;
  //BSL IR SRUM042154839 04/24/2012 Rel 8.0 BEGIN
//  private static String PO_ORDERBY = "order by seller_name, currency, creation_date_time";
	private static String RECEIVABLE_INVOICE_ORDERBY = "order by buyer_name, currency, invdate , a_upload_definition_oid , amount desc";
	private static String PAYABLE_INVOICE_ORDERBY    = "order by seller_name, currency, invdate, a_upload_definition_oid, amount desc";
  //BSL IR SRUM042154839 04/24/2012 Rel 8.0 END

  // This SQL selects all the log sequence numbers
  // in reverse order for a given corporate org.
  private static String LOG_SELECT_SQL = "select DISTINCT(p_invoice_file_upload_uoid) "
      + "from invoice_upload_error_log " + "where a_corp_org_oid = ";

  private static String LOG_ORDERBY = " order by p_invoice_file_upload_uoid DESC";

  // This SQL is used to delete log records for a given sequence.
  private static String LOG_DELETE_SQL = "delete from auto_lc_create_log "
      + "where sequence_number = ";

  // This indicates the maximum number of po line items that
  // can be assigned to a transaction.
  private static int MAX_ITEMS_ON_PO = 94;

  // This indicates the maximum number of logs to keep for
  // a corporate org.
  private static int MAX_LOGS_TO_KEEP = 2;

  // These are instance variables that are set each and every time the
  // mediator is created.  They are transient because mediators are
  // intended to be stateless.

  private transient MediatorServices mediatorServices;

  private transient InvoiceLogger logger = null;

  private transient String baseCurrency = null;

  private transient String clientBankOid = null;

  private transient String corporateOrgOid = null;

  private transient String logSequence = null;

  private transient String securityRights = null;

  private transient String timeZone = null;

  private transient String uploadSequenceNumber = null;

  private transient String userLocale = null;

  private transient String userName = null;

  private transient String userOid = null;

  private transient long uploadDefinitionOid = 0;

  private transient String uploadFileName = null;

  private transient String subsidiaryAccessOrgOid = null;

  private Set invOIds = new TreeSet<String>();
  // Business methods
  public DocumentHandler execute(DocumentHandler inputDoc,  DocumentHandler outputDoc, MediatorServices mediatorServices)
      throws RemoteException, AmsException {

    String invGroupingOption = null;
    String invDataOption = null;
    String value = null;
    importIndicator = null;
    financeType = null;
    buyerBacked = null;
    setMediatorServices(mediatorServices);

    // Pull values out of the input document and call various setter methods
    value = inputDoc.getAttribute("/User/userOid");
    setUserOid(value);

    value = inputDoc.getAttribute("../timeZone");
    setTimeZone(value);

    value = inputDoc.getAttribute("../locale");
    setUserLocale(value);

    value = inputDoc.getAttribute("../securityRights");
    setSecurityRights(value);

    value = inputDoc.getAttribute("../baseCurrencyCode");
    setBaseCurrency(value);

    value = inputDoc.getAttribute("../corporateOrgOid");
    setCorporateOrgOid(value);

    value = inputDoc.getAttribute("../clientBankOid");
    setClientBankOid(value);

    value = inputDoc.getAttribute("../poDataFilePath");
    setUploadFileName(value);

    value = inputDoc.getAttribute("../uploadSequenceNumber");
    setUploadSequenceNumber(value);

    value = inputDoc.getAttribute("../subsidiaryAccessOrgOid");
    if (value != null)
      setSubsidiaryAccessOrgOid(value);


      // If no upload sequence was given, it means we're grouping everything
    // (all upload sequences).  Therefore, the log sequence needs to be
    // generated.  Otherwise, we use the upload sequence of the log sequence
    if (uploadSequenceNumber == null) {
      long newSeq = ObjectIDCache.getInstance(
          TradePortalConstants.AUTOLC_SEQUENCE).generateObjectID();
      setLogSequence(String.valueOf(newSeq));
    } else {
      setLogSequence(uploadSequenceNumber);
    }

    // If we're coming from the PO Listview page, the button that was pressed will still
    // be set in the xml doc; otherwise (if it doesn't exist), we know that a PO upload
    // just occurred, in which case we should retrieve the PO data option and grouping
    // option (if one exists).
    value = inputDoc.getAttribute("/Update/ButtonPressed");
    if (value == null) {
      invDataOption = inputDoc
          .getAttribute("/InvoiceDataUploadInfo/InvPayDataOption");
      if (invDataOption!=null&&invDataOption.equals(TradePortalConstants.GROUP_PO_LINE_ITEMS)) {
        invGroupingOption = inputDoc.getAttribute("../InvItemsGroupingOption");
        if (invGroupingOption!=null&&invGroupingOption
            .equals(TradePortalConstants.INCLUDE_ALL_PO_LINE_ITEMS)) {
          setUploadSequenceNumber("");
        }
      }

      setUploadDefinitionOid(inputDoc
          .getAttributeLong("../uploadDefinitionOid"));

      invUploadInstrType = inputDoc
          .getAttribute("../InvUploadInstrOption");
      loanType = inputDoc.getAttribute("../loanType");
      invoiceClassification = inputDoc.getAttribute("../invoiceClassification");
      if (StringFunction.isNotBlank(loanType)){
    	  if  (TradePortalConstants.TRADE_LOAN.equals(loanType)){

	    	  importIndicator = TradePortalConstants.INDICATOR_T;
	    	   if (TradePortalConstants.INVOICE_TYPE_REC_MGMT.equals(invoiceClassification)){
	    		   financeType = TradePortalConstants.TRADE_LOAN_REC;
	    	  }else if (TradePortalConstants.INVOICE_TYPE_PAY_MGMT.equals(invoiceClassification)){
	    		  financeType = TradePortalConstants.TRADE_LOAN_PAY;
	    	  }

	      }else if (TradePortalConstants.EXPORT_FIN.equals(loanType)){
	    	  importIndicator = TradePortalConstants.INDICATOR_NO;
	      }else if (TradePortalConstants.INV_FIN_REC.equals(loanType)){
	    	  importIndicator = TradePortalConstants.INDICATOR_X;
	    	  financeType = TradePortalConstants.SELLER_REQUESTED_FINANCING;
	      }else if (TradePortalConstants.INV_FIN_BB.equals(loanType)){
	    	  importIndicator = TradePortalConstants.INDICATOR_X;
	    	  financeType = TradePortalConstants.BUYER_REQUESTED_FINANCING;
	    	  buyerBacked = TradePortalConstants.INDICATOR_YES;
	      }else if (TradePortalConstants.INV_FIN_BR.equals(loanType)){
	    	  importIndicator = TradePortalConstants.INDICATOR_X;
	    	  financeType = TradePortalConstants.BUYER_REQUESTED_FINANCING;
	    	  buyerBacked = TradePortalConstants.INDICATOR_NO;
	      }
      }else{
    	  importIndicator = null;
    	  financeType = null;
    	  buyerBacked = null;
      }
      if (invUploadInstrType == null) {
		  // This happens when user chooses to upload but not creating any automatic instrument.
		  // The user just want to upload PO so that it will be available to adding manually from the instrument
		  invUploadInstrType = "";
	  }

      if (invUploadInstrType.equals(InstrumentType.LOAN_RQST)) {
            SELECT_SQL = "select name,inv_only_create_rule_desc,inv_only_create_rule_template,inv_only_create_rule_max_amt,"
            				+ "due_or_pay_max_days,due_or_pay_min_days,a_op_bank_org_oid, pregenerated_sql "
            				+ EXTRA_SELECT_SQL
                            + " from INV_ONLY_CREATE_RULE "
                            + "where instrument_type_code = '" + InstrumentType.LOAN_RQST + "'"
                            + " and A_INVOICE_DEF "
                            + (( getUploadDefinitionOid() == 0) ? " is not null " : " = " + getUploadDefinitionOid())
                            + " and a_owner_org_oid in (";
      }

      if (invUploadInstrType.equals(TradePortalConstants.AUTO_ATP_PO_UPLOAD)) {
            SELECT_SQL = "select name,inv_only_create_rule_desc,inv_only_create_rule_template,inv_only_create_rule_max_amt,"
							+ "due_or_pay_max_days,due_or_pay_min_days,a_op_bank_org_oid, pregenerated_sql "
							+ EXTRA_SELECT_SQL
							+ " from INV_ONLY_CREATE_RULE "
			                + "where instrument_type_code = '" + TradePortalConstants.AUTO_ATP_PO_UPLOAD + "'"
			                + " and A_INVOICE_DEF "
			                + (( getUploadDefinitionOid() == 0) ? " is not null " : " = " + getUploadDefinitionOid())
			                + " and a_owner_org_oid in (";
      }
    } else {
         // If it gets in here, it means that we are processing active po from the active_po_upload queue
         // where the user placed request(s) by clicking the 'Create ATPs Using Creation Rules' or 'Create LCs Using Creation Rules'
         // button on the PO list view tabpage. The value of the button is kept the xml file stored in active_po_upload.po_upload_parameters.
         if (value.equals(TradePortalConstants.BUTTON_CREATE_LC_FROM_STRUCT_PO)) {
            SELECT_SQL = "select name, description, maximum_amount, pregenerated_sql, "
                            + " min_last_shipment_date, max_last_shipment_date, "
                            + " a_template_oid, a_op_bank_org_oid "
                            + EXTRA_SELECT_SQL
                            + " from lc_creation_rule "
                            + "where instrument_type_code = '" + TradePortalConstants.AUTO_LC_PO_UPLOAD + "'"
                            + " and a_struct_po_upload_def_oid is not null and a_owner_org_oid in (";
            invUploadInstrType = InstrumentType.IMPORT_DLC;
         }
         else if (value.equals(TradePortalConstants.BUTTON_CREATE_ATP_FROM_STRUCT_PO)) {
            SELECT_SQL = "select name, description, maximum_amount, pregenerated_sql, "
                            + " min_last_shipment_date, max_last_shipment_date, "
                            + " a_template_oid, a_op_bank_org_oid "
                            + EXTRA_SELECT_SQL
                            + " from lc_creation_rule "
                            + "where instrument_type_code = '" + TradePortalConstants.AUTO_ATP_PO_UPLOAD + "'"
                            + " and a_struct_po_upload_def_oid is not null and a_owner_org_oid in (";
            invUploadInstrType = InstrumentType.APPROVAL_TO_PAY;
         }
    }

    setLogger(InvoiceLogger.getInstance(getCorporateOrgOid()));
    try{
    	groupInvoices(inputDoc,null);
    }catch (Exception e){
    	LOG.error("Error While Grouping Invoices. ", e);
        getLogger().addLogMessage(getCorporateOrgOid(), getLogSequence(), TradePortalConstants.ERROR_GROUPING_INVS, e.getMessage());
        String[]   substitutionValues = {e.getMessage()};
        boolean[] emptyArray = null;
        mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.ERROR_GROUPING_INVS, substitutionValues, emptyArray);

    }
    if (invOIds.size() > 0){

        String sql = "select distinct ins.COMPLETE_INSTRUMENT_ID INS_NUM, " +
                "ins.instrument_type_code INS_TYP, tran.transaction_type_code TRAN_TYP " +
                "from invoices_summary_data inv inner join instrument ins on inv.A_INSTRUMENT_OID = ins.INSTRUMENT_OID  " +
                "inner join transaction tran on ins.original_transaction_oid = tran.transaction_oid " +
                "and inv.upload_invoice_oid in (";
        List<Object> sqlPrmLst = new ArrayList<Object>();
        StringBuilder sb = new StringBuilder();
        POLineItemUtility.getAllPOLineItemOidsSql(new Vector(invOIds),invOIds.size(),sb,0).toString();

        sql+=sb.toString();
        
        DocumentHandler resultSetDoc = DatabaseQueryBean.getXmlResultSet(sql, false, sqlPrmLst);
        outputDoc.addComponent("/",resultSetDoc);

		   String uploadInvOid = null;
		   Vector invoiceIDVector = null;
		   invoiceIDVector = new Vector(invOIds);
			LOG.debug("invoiceIDVector: {}", invoiceIDVector);
			InvoicesSummaryData invoiceSummaryData = null;
			ArMatchingRule matchingRule = null;
			for (int i = 0; i < invoiceIDVector.size(); i++) {
				uploadInvOid =  (String) invoiceIDVector.get(i);

				 invoiceSummaryData =
						(InvoicesSummaryData) mediatorServices.createServerEJB("InvoicesSummaryData",
								Long.parseLong(uploadInvOid));
				 String invLoanType = invoiceSummaryData.getAttribute("loan_type");
				 String buyerName = "";
				//tp mod shilpa start
				 String tpName= invoiceSummaryData.getTpRuleName();

				//tp mod shilpa end
				 invoiceSummaryData.setAttribute("tp_rule_name", tpName);
				if (StringFunction.isBlank(invLoanType) && InstrumentType.LOAN_RQST.equals(invUploadInstrType)){
					invoiceSummaryData.setAttribute("loan_type", loanType);
					if (TradePortalConstants.EXPORT_FIN.equals(loanType)){
						invoiceSummaryData.calculateInterestDiscount(getCorporateOrgOid());
					}
				}
				if (TradePortalConstants.TRADE_LOAN_REC.equals(financeType)){
			    	CorporateOrganization corpOrg = (CorporateOrganization) mediatorServices.createServerEJB("CorporateOrganization");
			    	corpOrg.getData(Long.valueOf(getCorporateOrgOid()).longValue());
			    	String finPercent = corpOrg.getAttribute("inv_percent_to_fin_trade_loan");
			    	BigDecimal percentFinanceAllowed = StringFunction.isBlank(finPercent) ? BigDecimal.ZERO :
						new BigDecimal(finPercent).divide(new BigDecimal("100.0"),
								MathContext.DECIMAL64);

			    	finPercent = invoiceSummaryData.getAttribute("amount");
					BigDecimal invoiceAmount = StringFunction.isBlank(finPercent) ? BigDecimal.ZERO :
						new BigDecimal(finPercent, MathContext.DECIMAL64);

					BigDecimal financeAmount = invoiceAmount.multiply(percentFinanceAllowed);
					invoiceSummaryData.setAttribute("invoice_finance_amount",financeAmount.toString());
			    	
				}

				int saveYN = invoiceSummaryData.save();
				LOG.debug("SaveYN : {}", saveYN);
			}

    }
      invOIds = new TreeSet<String>();
    LOG.debug("Exiting AutoCreateInvoicesMediator \n\n\n");

    return outputDoc;
  }


  /**
   * Getter for the baseCurrency attribute
   *
   * @return java.lang.String
   */
  private String getBaseCurrency() {
    return baseCurrency;
  }

  /**
   * Getter for the attribute clientBankOid
   * @return java.lang.String
   */
  private String getClientBankOid() {
    return clientBankOid;
  }

  /**
   * This method attempts to convert the given fromAmount (which is in teh given
   * currency which also happens to be the base currency) into the currency of the
   * po line item.  If the fromCurrency and toCurrency are the same, there is no
   * conversion to do.  Otherwise, we look at the currency of the po line item.
   * If it is a valid TradePortal currency (i.e., in refdata manager), we try to
   * get an fx rate using the toCurrency (the po line item currency).  If found,
   * use that information.  Otherwise get the base currency of the template
   * associated with the instrument creation rule.  If we can't find an fx rate for that
   * currency we can't convert and return a null amount.  Since the conversion we
   * are doing is FROM a base currency TO another currency (the opposite of the
   * way the fx rate information is defined, our calculation is opposite of the
   * fx rate info (e.g., we divide if the multiply_indicator is multiply).
   * <p>
   * @return java.math.BigDecimal - the resulting converted amount, null if can't
   *                                convert.
   * @param fromAmount java.math.BigDecimal - The amount to convert from
   * @param fromCurrency java.lang.String - The currency the fromAmount is in
   * @param toCurrency java.lang.String - The currency we want to convert to
   * @param creationRuleDoc DocumentHandler - The instrument creation rule record (as a doc)
   */
  private BigDecimal getConvertedMaxAmount(BigDecimal fromAmount,
      String fromCurrency, String toCurrency, DocumentHandler creationRuleDoc) {

    BigDecimal convertedAmount = null;

    try {
      //BSL IR SRUM042154839 04/24/2012 Rel 8.0 BEGIN - add null check and move before currency check
      // fromAmount may have been defaulted to 0 (to prevent a null
      // pointer).  However, we don't want to set the maxAmount value
      // to 0 since nothing would get grouped.  If the fromAmount is
      // 0, return a null (indicating maxAmount not relevant for
      // grouping).
      if ((fromAmount == null) ||
    		  fromAmount.compareTo(BigDecimal.ZERO) == 0) {
        return null;
      }

      // No need to convert if the from and to currencies are the same
      if (fromCurrency.equals(toCurrency)) {
        return fromAmount;
      }

      String templateCurrency = null;
      String sql;
      String multiplyIndicator = null;
      BigDecimal rate = null;
      DocumentHandler fxRateDoc = null;

      ReferenceDataManager rdm = ReferenceDataManager.getRefDataMgr();

      // Test whether the po line item currency code is a valid Trade
      // Portal currency.
      if (rdm.checkCode(TradePortalConstants.CURRENCY_CODE, toCurrency,
          getUserLocale())) {
        // Get the fx rate info for the po line item currency
        fxRateDoc = getFxRateDoc(getCorporateOrgOid(), toCurrency);
        if (fxRateDoc == null) {
          // No fx rate found, isse warning and return a blank amount.
          // This indicates that maxAmount is not used for grouping.
          // Give error with the po line item currency.

          getLogger().addLogMessage(getCorporateOrgOid(), getLogSequence(),
              TradePortalConstants.LCLOG_NO_FX_RATE, toCurrency);
          return null;
        } else {
          // Found an fx rate for the template currency.  Use it.
        }
      } else {
        // Currency code for the po line item is not valid.  Use
        // the currency code from the instrument creation rule's template.
        sql = "select copy_of_currency_code from template, transaction where p_instrument_oid = c_template_instr_oid and template_oid = ?";
        DocumentHandler templateDoc = null;
        try {
          templateDoc = DatabaseQueryBean.getXmlResultSet(sql, true, new Object[]{creationRuleDoc.getAttribute("/A_TEMPLATE_OID")});
        } catch (AmsException e) {
          templateDoc = null;
        }

        if (templateDoc == null) {
          // Can't get the currency code off the template.  Issue a warning and return
          // a null amount.  This indicates NOT to use maxAmount for grouping.

             getLogger().addLogMessage(getCorporateOrgOid(),
              getLogSequence(),
              TradePortalConstants.LCLOG_INVALID_CURRENCY,
              creationRuleDoc.getAttribute("/NAME"));
          return null;
        } else {
          // We got the template currency but it may be blank.  We're only
          // concerned with the first record (there should not be more).
          templateCurrency = templateDoc
              .getAttribute("/ResultSetRecord(0)/COPY_OF_CURRENCY_CODE");
          if (StringFunction.isBlank(templateCurrency)) {
            // Can't get the currency code off the template.  Issue a warning and return
            // a null amount.  This indicates NOT to use maxAmount for grouping.

            getLogger().addLogMessage(getCorporateOrgOid(),
                getLogSequence(),
                TradePortalConstants.LCLOG_INVALID_CURRENCY,
                creationRuleDoc.getAttribute("/NAME"));
            return null;
          } else {
            // Currency is good so attempt to get the fx rate doc.
            fxRateDoc = getFxRateDoc(getCorporateOrgOid(),
                templateCurrency);

            // Don't look for an FX rate if the template's currency is the base currency
            // No FX rate is needed in that case
            if (!templateCurrency.equals(getBaseCurrency())) {
              if (fxRateDoc == null) {
                // No fx rate found, isse warning and return a blank amount.
                // This indicates that maxAmount is not used for grouping.
                // Give error using the template currency.

                getLogger().addLogMessage(getCorporateOrgOid(),
                    getLogSequence(),
                    TradePortalConstants.LCLOG_NO_FX_RATE,
                    templateCurrency);
                return null;
              } else {
                // Found an fx rate for the template currency.  Use it.
              }
            } else {
              // Base Currency and Template currency are identical
              // No need for conversion.  Return the from amount
              return fromAmount;
            }
          } // end templateCurrency == null
        } // end templateDoc == null
      } // end checkCode

      // We now have an fx rate doc (although the values on the doc may be
      // blank.)  Attempt to convert the amount.
      multiplyIndicator = fxRateDoc.getAttribute("/MULTIPLY_INDICATOR");
      rate = fxRateDoc.getAttributeDecimal("/RATE");

      if (multiplyIndicator != null && rate != null) {
        // We're converting FROM the base currency TO the po line item
        // currency so we need to do the opposite of the multiplyIndicator
        // (the indicator is set for converting FROM some currency TO the
        // base currency)
        if (multiplyIndicator.equals(TradePortalConstants.MULTIPLY)) {
          convertedAmount = fromAmount.divide(rate,
              BigDecimal.ROUND_HALF_UP);
        } else {
          convertedAmount = fromAmount.multiply(rate);
        }
      } else {
        // We have bad data in the fx rate record.  Issue warning and return
        // null amount (indicating that maxAmount is not used for grouping)

        getLogger().addLogMessage(getCorporateOrgOid(),
            getLogSequence(),
            TradePortalConstants.LCLOG_NO_FX_RATE,
            fxRateDoc.getAttribute("/CURRENCY_CODE"));
        return null;
      }
    } catch (AmsException e) {
      LOG.error("Exception found getting converted max amount:", e);
    }

    return convertedAmount;
  }

  /**
   * Getter for the attribute corporateOrgOid
   * @return java.lang.String
   */
  private String getCorporateOrgOid() {
    return corporateOrgOid;
  }


  /**
   * Uses the given corporate org oid and currency to get an fx rate record
   * (as a document handler).  The currency, rate, and multiple_indicator are
   * returned in the document.
   *
   * @return DocumentHandler - result of the query, may be null
   * @param corporateOrg java.lang.String - corporate org oid to use
   * @param currency java.lang.String - currency to use
   */
  private DocumentHandler getFxRateDoc(String corporateOrg, String currency) {

		Cache fxCache = TPCacheManager.getInstance().getCache(TradePortalConstants.FX_RATE_CACHE);
		DocumentHandler resultSet = (DocumentHandler) fxCache.get(corporateOrg + "|" + currency);

		if (resultSet != null) {
			List<DocumentHandler> fxRateDocs = resultSet.getFragmentsList("/ResultSetRecord");
			return fxRateDocs.get(0);
		}

		return null;

	}


    /**
     * Getter for the auto lc logger attribute
     * @return com.ams.tradeportal.busobj.util.StructuredPOLogger
     */
    private InvoiceLogger getLogger() {
        return logger;
    }

  /**
   * Getter for the attribute logSequence
   * @return java.lang.String
   */
  private String getLogSequence() {
    return logSequence;
  }

  /**
   * Getter for the mediator services attribute.
   * @return com.amsinc.ecsg.frame.MediatorServices
   */
  private MediatorServices getMediatorServices() {
    return mediatorServices;
  }

  /**
   * Getter for the attribute security rights
   * @return java.lang.String
   */
  private String getSecurityRights() {
    return securityRights;
  }

  /**
   * Getter for the timeZone attribute
   * @return java.lang.String
   */
  private String getTimeZone() {
    return timeZone;
  }

  /**
   * Getter for the uploadFileName attribute
   * @return java.lang.String
   */
  private String getUploadFileName() {
    return uploadFileName;
  }

  /**
   * Getter for the attribute uploadSequenceNumber
   * @return java.lang.String
   */
  private String getUploadSequenceNumber() {
    return uploadSequenceNumber;
  }

  /**
   * Getter for the user locale attribute
   * @return java.lang.String
   */
  private String getUserLocale() {
    return userLocale;
  }

  /**
   * Getter for the attribute userOid
   * @return java.lang.String
   */
  private String getUserOid() {
    return userOid;
  }

  private long getUploadDefinitionOid() {
    return uploadDefinitionOid;
  }

	 /**
	  * This method groups the invoices based on loan request rules.
	  * @param inputDoc
	  * @param invoiceOidList
	  * @return
	  * @throws AmsException
	  * @throws RemoteException
	  */
	 private DocumentHandler groupInvoices(DocumentHandler inputDoc, Vector invoiceOidList) throws AmsException, RemoteException {
		 LOG.debug("groupInvoices()....");

		 Vector loanReqRulesList = null;
		 DocumentHandler loanRequestDoc = null;
		 Vector invoiceItemList;
		 int count =0;
		 CorporateOrganization corporateOrg = (CorporateOrganization) mediatorServices
	        .createServerEJB("CorporateOrganization");
		 try {
			corporateOrg.getData(Integer.parseInt(getCorporateOrgOid()));
			setUsePaymentDate(corporateOrg.getAttribute("use_payment_date_allowed_ind"));
			setUsePaymentDateForATP(corporateOrg.getAttribute("payment_day_allow"));
			setDaysBeforeATP(corporateOrg.getAttribute("days_before"));
			setDaysAfterATP(corporateOrg.getAttribute("days_after"));
			setDaysBeforeLRQ(corporateOrg.getAttribute("days_before_payment_date"));
			setDaysAfterLRQ(corporateOrg.getAttribute("days_after_payment_date"));
		 } catch (InvalidObjectIdentifierException e) {
			  mediatorServices.getErrorManager().issueError(
		      TradePortalConstants.ERR_CAT_1, TradePortalConstants.ORG_NOT_EXIST);
		 }

		 StringBuilder invoiceOidBuf = new StringBuilder();


		 loanReqRulesList = getLoanRequestRules(getCorporateOrgOid());
		 LOG.debug("loanReqRulesList: {}", loanReqRulesList);
	     if (loanReqRulesList.size() < 1) {
		      // There are no Loan Request Rules available.
			 mediatorServices.getErrorManager().issueError(
	    			  UploadInvoiceMediator.class.getName(),
	    			  TradePortalConstants.AUTO_LOAN_REQ_BY_RULES_NO_RULE_EXISTS);
	    	  return new DocumentHandler();
		    }

	     for (int i = 0; i < loanReqRulesList.size(); i++) {
		      // Get the Loan Request rule and pull some values off it.
	    	 loanRequestDoc = (DocumentHandler) loanReqRulesList.elementAt(i);

		      // Get the getInvoiceItems for this loan request rules
	    	 invoiceItemList = getInvoiceItems(invoiceOidBuf.toString(),loanRequestDoc);
		      String parms3[] = { loanRequestDoc.getAttribute("/NAME") };

		      if (invoiceItemList.size() < 1) {
		    	  LOG.debug("No rules InvoiceItems available...");
		    	  /*
		    	  mediatorServices.getErrorManager().issueError(
		    			  UploadInvoiceMediator.class.getName(),
		    			  TradePortalConstants.AUTO_LOAN_REQ_BY_RULES_NO_ITEMS_FOR_LRC_RULE,parms3);
		    	  */

		      } else {
				if (StringFunction.isNotBlank(loanType)
						&& TradePortalConstants.TRADE_LOAN.equals(loanType)) {
					if (TradePortalConstants.INDICATOR_NO
							.equals(getUsePaymentDate())) {
						if (!validateIsPaymentDateBlank(invoiceItemList)) {
							mediatorServices
									.getErrorManager()
									.issueError(
											UploadInvoiceMediator.class
													.getName(),
											TradePortalConstants.AUTO_LOAN_REQ_BY_RULES_PAYMENT_DATE_NOT_BLANK);
							continue;
						}
					}
				}
		        // Now start chunking to po line items.
		    	  chunkInvoices(invoiceItemList, loanRequestDoc);
		        count++;
		      }
		    }
		    if(count==0){
		        mediatorServices.getErrorManager().issueError(
		  	          TradePortalConstants.ERR_CAT_1,
		  	          TradePortalConstants.AUTO_LOAN_REQ_BY_RULES_NO_ITEMS_FOR_LRC_RULE_OR_INVS_ALRDY_ATTCHD);
		    }
		    //Ir 22613
		    else{
		    logUnassignedInvoiceCount();
		    }
			return loanRequestDoc;


	 }
	 /** IR- 22613
	   * Indicates how many
	   * invoices are still unassigned.
	   */
	  private void logUnassignedInvoiceCount() throws AmsException,
	      RemoteException {
	    int count = 0;

	    if (StringFunction.isNotBlank(getUploadSequenceNumber())) {
	      String where = " a_transaction_oid is NULL and a_corp_org_oid = ? and p_invoice_file_upload_oid = ?";

	      count = DatabaseQueryBean.getCount("upload_invoice_oid", "INVOICES_SUMMARY_DATA", where, false, getCorporateOrgOid(), getUploadSequenceNumber());
	      if(count>0){
	    	  mediatorServices.getErrorManager().issueError(
		  	          TradePortalConstants.ERR_CAT_1,
	            TradePortalConstants.NO_MATCH_RULE_INV,
	            String.valueOf(count), getUploadFileName());
	      }

	    }

	  }
	  
	private boolean validateIsPaymentDateBlank(Vector invoiceItemList)
			throws AmsException {
		Collection<InvoiceSummaryDataObject> coll = invoiceItemList;
		Object[] invSummDataList = coll.toArray();
		DocumentHandler invoiceItemDoc = null;
		Vector invOids = new Vector();
		String uploadInvoiceOid = null;
		for (int i = 0; i < invSummDataList.length; i++) {
			invoiceItemDoc = (DocumentHandler) invoiceItemList.elementAt(i);
			uploadInvoiceOid = invoiceItemDoc
					.getAttribute("/UPLOAD_INVOICE_OID");
			invOids.add(uploadInvoiceOid);
		}
		List<Object> sqlprmLst = new ArrayList<Object>();
		StringBuilder sql = new StringBuilder(
				"Select count(payment_date) as date_Count from invoices_summary_data where ");
		sql.append(POLineItemUtility.getInvoiceItemOidsSqlWithPlaceHolder(invOids,"UPLOAD_INVOICE_OID",sqlprmLst));
		
		LOG.debug("Invoice sql is {}", sql);
		DocumentHandler resultSet = DatabaseQueryBean.getXmlResultSet(sql.toString(),false,sqlprmLst);

		if (resultSet == null)
			resultSet = new DocumentHandler();

		LOG.debug("invoice list is {}", resultSet);
		int dateCount = resultSet
				.getAttributeInt("/ResultSetRecord(0)/DATE_COUNT");
		if (dateCount != 0) {
			StringBuilder updateSQL = new StringBuilder(
					"UPDATE INVOICES_SUMMARY_DATA SET LOAN_TYPE = null where ");
			updateSQL.append(POLineItemUtility.getInvoiceItemOidsSql(invOids,"UPLOAD_INVOICE_OID"));
			
			try {
				DatabaseQueryBean.executeUpdate(updateSQL.toString(),false,new ArrayList<Object>());
			} catch (SQLException e) {
				LOG.error("Exception while updating table INVOICES_SUMMARY_DATA. ", e);
			}
		}
		return dateCount == 0;
	}
		private transient String usePaymentDate = null;

		private String getUsePaymentDate() {
			return usePaymentDate;
		}
		private void setUsePaymentDate(String value){
			usePaymentDate = value;
		}
		private transient String usePaymentDateForATP = null;

		private String getUsePaymentDateForATP() {
			return usePaymentDateForATP;
		}
		private void setUsePaymentDateForATP(String value){
			usePaymentDateForATP = value;
		}
		private transient String daysBeforeATP = null;

		private String getDaysBeforeATP() {
			return daysBeforeATP;
		}
		private void setDaysBeforeATP(String value){
			daysBeforeATP = value;
		}
		private transient String daysAfterATP = null;

		private String getDaysAfterATP() {
			return daysAfterATP;
		}
		private void setDaysAfterATP(String value){
			daysAfterATP = value;
		}

		private transient String daysBeforeLRQ = null;

		private String getDaysBeforeLRQ() {
			return daysBeforeLRQ;
		}
		private void setDaysBeforeLRQ(String value){
			daysBeforeLRQ = value;
		}
		private transient String daysAfterLRQ = null;

		private String getDaysAfterLRQ() {
			return daysAfterLRQ;
		}
		private void setDaysAfterLRQ(String value){
			daysAfterLRQ = value;
		}

	 /**
	  * This method returns the set of invoices which pass through the
	  * rules defined for those invoice/invoices
	  * @param invoiceOids
	  * @param creationRuleDoc
	  * @return
	  * @throws AmsException
	  * @throws RemoteException
	  */
	 private Vector getInvoiceItems(String invoiceOids,DocumentHandler creationRuleDoc) throws AmsException,
     RemoteException {
		 	LOG.debug("getInvoiceItems()....invoiceOids {} ", invoiceOids);
		 	String date = "";
		 	int daysBefore = 0;
		 	int daysAfter = 0;
		   // Add condition for last_ship_dt if both min and max are set;
		    boolean usePayDate = false;
		    if (InstrumentType.LOAN_RQST.equals(invUploadInstrType)){
		    	if(TradePortalConstants.INDICATOR_YES.equals(getUsePaymentDate())){
		    		usePayDate = true;
		    		if (StringFunction.isNotBlank(getDaysBeforeLRQ())){
		    			daysBefore = Integer.valueOf(getDaysBeforeLRQ()).intValue();
		    		}
		    		if (StringFunction.isNotBlank(getDaysAfterLRQ())){
		    			daysAfter = Integer.valueOf(getDaysAfterLRQ()).intValue();
		    		}

		    	}
		    } else if (InstrumentType.APPROVAL_TO_PAY.equals(invUploadInstrType)){
		    	if (TradePortalConstants.INDICATOR_YES.equals(getUsePaymentDateForATP())){
		    		usePayDate =true;
		    		if (StringFunction.isNotBlank(getDaysBeforeATP())){
		    			daysBefore = Integer.valueOf(getDaysBeforeATP()).intValue();
		    		}
		    		if (StringFunction.isNotBlank(getDaysAfterATP())){
		    			daysAfter = Integer.valueOf(getDaysAfterATP()).intValue();
		    		}
		    	}
		    }
	    	if (usePayDate){
	    		date = " cast (case when payment_date is null then due_date else payment_date end as date ) as invdate";
	    	}else {
	    		date = " due_date as invdate ";
	    	}

	    	List<Object> sqlPrmLst = new ArrayList();
		 	Object[] arguments = {date};
		 	StringBuilder invoiceSql = new StringBuilder(INV_SELECT_SQL );
		 	invoiceSql = new StringBuilder(MessageFormat.format(invoiceSql.toString(), arguments));

		   invoiceSql.append(INV_WHERE_SQL);
		   invoiceSql.append("?");
		   sqlPrmLst.add(getCorporateOrgOid());
		   
		    // Add condition for sequence if it is non-blank
		    if (StringFunction.isNotBlank(getUploadSequenceNumber())) {
		    	invoiceSql.append(" and p_invoice_file_upload_oid = ?");
		    	sqlPrmLst.add(getUploadSequenceNumber());
		    }else{
		    	if (StringFunction.isNotBlank(loanType)){
		    		invoiceSql.append(" and ( loan_type = ? or loan_type is null ) ");
		    		sqlPrmLst.add(loanType);
		    	}
		    }
		    if (usePayDate && (TradePortalConstants.TRADE_LOAN.equals(loanType) || InstrumentType.APPROVAL_TO_PAY.equals(invUploadInstrType))){
		    	 invoiceSql.append(" and case when payment_date is null then due_date else payment_date end between ");
			     invoiceSql.append(" due_date - ? and due_date + ?");
			     sqlPrmLst.add(daysBefore);
			     sqlPrmLst.add(daysAfter);
		    }else{
				   String minDays = creationRuleDoc.getAttribute("/DUE_OR_PAY_MIN_DAYS");
				   String maxDays = creationRuleDoc.getAttribute("/DUE_OR_PAY_MAX_DAYS");
				   LOG.debug("mindays:{} \t max days:{}", minDays, maxDays);

			   if (StringFunction.isNotBlank(minDays)  && StringFunction.isNotBlank(maxDays)) {

			     // First get the current local date
			     String currentGmtDate = DateTimeUtility.getGMTDateTime();
			     Date currentLocalDate = TPDateTimeUtility.getLocalDateTime(currentGmtDate, getTimeZone());
			     SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yy");
			     String today = formatter.format(currentLocalDate);
			     LOG.debug("today: {}", today);
		    	 invoiceSql.append(" and case when payment_date is null then due_date else payment_date end between TO_DATE(?)+? and  TO_DATE(?)+?");
			     sqlPrmLst.add(today);
			     sqlPrmLst.add(minDays);
			     sqlPrmLst.add(today);
			     sqlPrmLst.add(maxDays);
			   }
		    }
		   // Add condition for the remaining criteria from the lc creation rule
		   String criteria = creationRuleDoc.getAttribute("/PREGENERATED_SQL");

		   if (StringFunction.isNotBlank(criteria)) {
			   invoiceSql.append(" and ");
			   invoiceSql.append(criteria);
		   }

		   // Finally add the orderby clause.
		   invoiceSql.append(" ");
		   if (TradePortalConstants.INVOICE_TYPE_REC_MGMT.equals(invoiceClassification)){
			  if (TradePortalConstants.TRADE_LOAN.equals(loanType)){
				  StringBuilder tempOrderBy = new StringBuilder(" order by currency, a_upload_definition_oid, amount desc ");
				  invoiceSql.append(tempOrderBy);
			  }else{
				  invoiceSql.append(RECEIVABLE_INVOICE_ORDERBY);
			  }
		  }else if (TradePortalConstants.INVOICE_TYPE_PAY_MGMT.equals(invoiceClassification)){
			  if (TradePortalConstants.TRADE_LOAN.equals(loanType)){
				  StringBuilder tempOrderBy = new StringBuilder(" order by currency, pay_method, a_upload_definition_oid, amount desc ");
				  invoiceSql.append(tempOrderBy);
			  }else{
				  invoiceSql.append(PAYABLE_INVOICE_ORDERBY);
			  }
		  }


		   DocumentHandler resultSet = DatabaseQueryBean.getXmlResultSet(invoiceSql.toString(), false, sqlPrmLst);

		   if (resultSet == null)
			   resultSet = new DocumentHandler();

		   return resultSet.getFragments("/ResultSetRecord");

  }

	 /**
	  * This method returns the Loan Request rules
	  * @param corporateOrgOid
	  * @return
	  * @throws AmsException
	  */
	private Vector getLoanRequestRules(String corporateOrgOid) throws AmsException {

		 LOG.debug("getCreationRules() corporateOrgOid:"+corporateOrgOid);
		    StringBuilder loanReqSQL = new StringBuilder(SELECT_SQL);
		    loanReqSQL.append("?");
		    List<Object> sqlPrmLst = new ArrayList();
		    sqlPrmLst.add(corporateOrgOid);

		    // Loop back through all parent orgs for oids to include in search for LoanRequest Creation rules
		    do {
		      String parentCorpSql = "select p_parent_corp_org_oid from corporate_org where organization_oid = ? AND activation_status=?";
		      DocumentHandler parentCorpOrg = DatabaseQueryBean.getXmlResultSet(parentCorpSql, false, corporateOrgOid, TradePortalConstants.ACTIVE);
		      if( parentCorpOrg != null )
		      {
		    	  List<DocumentHandler> resultsVector = parentCorpOrg.getFragmentsList("/ResultSetRecord/");
		        if( (resultsVector.get(0)) != null )
		        {
		          DocumentHandler parentOid = resultsVector.get(0);
		          String parentCorpOid = "";
		          parentCorpOid = parentOid.getAttribute("/P_PARENT_CORP_ORG_OID");
		          if (StringFunction.isNotBlank(parentCorpOid)) {
		        	  loanReqSQL.append(",? ");
		            corporateOrgOid = parentCorpOid;
		            sqlPrmLst.add(corporateOrgOid);
		          } else {  // No more parents, so break out of loop
		            break;
		          }
		        } else {  // No more parents, so break out of loop
		          break;
		        }
		      } else { // no more parents, so break out of loop
		        break;
		      }
		    } while (true);

		    loanReqSQL.append(") ");
		    loanReqSQL.append(ORDERBY_CLAUSE);

		    DocumentHandler resultSet = DatabaseQueryBean.getXmlResultSet(loanReqSQL.toString(), false, sqlPrmLst);

		    if (resultSet == null)
		      resultSet = new DocumentHandler();


		    return resultSet.getFragments("/ResultSetRecord");

		  }

	 /**
	  * This method processes the invoices which are already
	  * passed through the rules and groups invoices for creating
	  * loan request instrument
	  * @param invoiceItemList
	  * @param creationRuleDoc
	  * @throws RemoteException
	  * @throws AmsException
	  */
	 private void chunkInvoices(Vector invoiceItemList, DocumentHandler creationRuleDoc)
		      throws RemoteException, AmsException {

		    LOG.debug("inside checPOs()....invoiceItemList {}", invoiceItemList);
		    BigDecimal invoiceAmount;
		    BigDecimal maxAmount = null;
		    BigDecimal convertedMaxAmount = null;
		    BigDecimal totalAmount = BigDecimal.ZERO;

		    int itemCount = 0;
		    String lastBenName = "";
		    String lastCurrency = "";
		    String lastPayMethod = "";

		    String latestDuepaymentDt = null;
		    String lastUploadInvoiceOid = "";
		    String benName = "";
		    String currency = "";
		    String payMethod ="";

		    String duePaymentDt;

		    String uploadInvoiceOid = "";
		    String loanReqRuleName = creationRuleDoc.getAttribute("/NAME");
			LOG.debug("loanReqRuleName: {}", loanReqRuleName);
		   
		    Vector invoiceOids = null;
		    DocumentHandler invoiceItemDoc = null;
		    int invoiceListCount = 0;

		    CorporateOrganization corporateOrg = (CorporateOrganization) mediatorServices
		        .createServerEJB("CorporateOrganization");
		    try {
		    	corporateOrg.getData(Integer.parseInt(getCorporateOrgOid()));
		    } catch (InvalidObjectIdentifierException e) {
		    	  mediatorServices.getErrorManager().issueError(
		          TradePortalConstants.ERR_CAT_1, TradePortalConstants.ORG_NOT_EXIST);
		    }
		    long clientBankOid = corporateOrg.getAttributeLong("client_bank_oid");
		    ClientBank clientBank = (ClientBank) mediatorServices.createServerEJB("ClientBank");
		    try {
		      clientBank.getData(clientBankOid);
		    } catch (InvalidObjectIdentifierException e) {
		      mediatorServices.getErrorManager().issueError(
		          TradePortalConstants.ERR_CAT_1, TradePortalConstants.INVALID_CLIENT_BANK_ORG);
		    }

		    // Determine the number of Invoices allowed per instrument
		    //int maximumInvoicesInInstr = MAX_ITEMS_ON_INVOICE;
		    String value = creationRuleDoc.getAttribute("/INV_ONLY_CREATE_RULE_MAX_AMT");

		    if (StringFunction.isNotBlank(value)) {
		      maxAmount = new BigDecimal(value);
		    } else {
		      maxAmount = null;

		    }
		    Vector processedInvoiceOids = new Vector();
		    for (int x = 0; x < invoiceItemList.size(); x++) {
		      // We will use this variable to count the number of Invoice line items fully processed, i.e. not skipped.
		    	invoiceListCount++;

		      invoiceItemDoc = (DocumentHandler) invoiceItemList.elementAt(x);
			  LOG.debug("invoiceItemDoc: {}", invoiceItemDoc);
		      // Pull off values from the document that drive the processing.
			  invoiceAmount = invoiceItemDoc.getAttributeDecimal("/AMOUNT");
			  if (TradePortalConstants.INVOICE_TYPE_PAY_MGMT.equals(invoiceClassification)){
				  benName = invoiceItemDoc.getAttribute("/SELLER_NAME");
			  }else {
				  benName = invoiceItemDoc.getAttribute("/BUYER_NAME");
			  }
		      currency = invoiceItemDoc.getAttribute("/CURRENCY");
		      uploadInvoiceOid = invoiceItemDoc.getAttribute("/UPLOAD_INVOICE_OID");
		      duePaymentDt = invoiceItemDoc.getAttribute("/INVDATE");
		      payMethod = invoiceItemDoc.getAttribute("/PAY_METHOD");
			  //LOG.debug("invoiceAmount: {} \t benName:{} \t currency:{} \t uploadINv oid:{} \t duePaymentDt:{}", invoiceAmount, benName, currency, uploadInvoiceOid, duePaymentDt);
		      invoiceOids = new Vector();

		        lastBenName = benName;
		        lastCurrency = currency;
		        lastPayMethod = payMethod;
		        latestDuepaymentDt = duePaymentDt;
		        lastUploadInvoiceOid = uploadInvoiceOid;
		        // Add the oid to the Invoice oid vector.
		        invoiceOids.add(uploadInvoiceOid);
				LOG.debug("first adding into invoiceOids:{} \t lastUploadInvoiceOid:{}", invoiceOids, lastUploadInvoiceOid);
		        convertedMaxAmount = getConvertedMaxAmount(maxAmount,getBaseCurrency(), currency, creationRuleDoc);
				LOG.debug("convertedMaxAmount: {}", convertedMaxAmount);
		        itemCount = 1;
		        totalAmount = invoiceAmount;

		        if (convertedMaxAmount == null ||
		        		totalAmount.compareTo(convertedMaxAmount) <= 0) {
		            // Find Invoices that can be added to this group
		            int subIdx = x + 1;

		            while (subIdx < invoiceItemList.size()) {
		            	invoiceItemDoc = (DocumentHandler) invoiceItemList.elementAt(subIdx);

		                // Pull off values from the document that drive the processing.
		                invoiceAmount = invoiceItemDoc.getAttributeDecimal("/AMOUNT") ;
						if (TradePortalConstants.INVOICE_TYPE_PAY_MGMT.equals(invoiceClassification)){
							benName = invoiceItemDoc.getAttribute("/SELLER_NAME") == null ? "" : invoiceItemDoc.getAttribute("/SELLER_NAME");
						}else {
							benName = invoiceItemDoc.getAttribute("/BUYER_NAME") == null ? "" : invoiceItemDoc.getAttribute("/BUYER_NAME");
						}
						currency = invoiceItemDoc.getAttribute("/CURRENCY") == null ? "" : invoiceItemDoc.getAttribute("/CURRENCY");
		                uploadInvoiceOid = invoiceItemDoc.getAttribute("/UPLOAD_INVOICE_OID") == null ? "" : invoiceItemDoc.getAttribute("/UPLOAD_INVOICE_OID");
		                duePaymentDt = invoiceItemDoc.getAttribute("/INVDATE") == null ? "" : invoiceItemDoc.getAttribute("/INVDATE");
		                payMethod = invoiceItemDoc.getAttribute("/PAY_METHOD") == null ? "" : invoiceItemDoc.getAttribute("/PAY_METHOD");
						//LOG.debug(" -> poamount:{}\t benName:{}\t currency:{}\t uploadInvoiceOid:{}\t duePaymentDt :", invoiceAmount, benName, currency, uploadInvoiceOid, duePaymentDt);

					    if (InstrumentType.LOAN_RQST.equals(invUploadInstrType) && TradePortalConstants.TRADE_LOAN.equals(loanType)){
					    	if (TradePortalConstants.INVOICE_TYPE_PAY_MGMT.equals(invoiceClassification) && StringFunction.isNotBlank(payMethod)){
				                if (!currency.equals(lastCurrency) || !payMethod.equals(lastPayMethod))
				                {
				                	// None of the remaining INVs can be added to this group
				                    break;
				                }

					    	}else{
				                if (!currency.equals(lastCurrency))
				                {
				                	// None of the remaining INVs can be added to this group
				                    break;
				                }
					    	}
					    }else{
			                if (!benName.equals(lastBenName) ||
			                		!currency.equals(lastCurrency) ||
			                		!duePaymentDt.equals(latestDuepaymentDt))
			                {
			                	// None of the remaining INVs can be added to this group
			                    break;
			                }
					    }
					    LOG.debug("invoiceAmount: {}", invoiceAmount);
		                BigDecimal newTotal = totalAmount.add(invoiceAmount);
		                if (convertedMaxAmount == null ||
		                		newTotal.compareTo(convertedMaxAmount) <= 0) {
		                	// Add this Invoice to the group
		                	invoiceOids.add(uploadInvoiceOid);
							LOG.debug("adding into poOids:{} \t uploadInvoiceOid:{}", invoiceOids, uploadInvoiceOid);
							invoiceItemList.remove(subIdx);
							invoiceListCount++;
		                    itemCount++;
		                    totalAmount = newTotal;

		                	if (duePaymentDt != null) {
		                		if (latestDuepaymentDt == null) {
		                			latestDuepaymentDt = duePaymentDt;
		                		}
		                		else if (duePaymentDt.compareTo(latestDuepaymentDt) > 0) {
		                			latestDuepaymentDt = duePaymentDt;
		                		}
		                	}
		                }
		                else {
		                	// This Invoice would bring the total above the max
		                	subIdx++;
		                }
		            }
		        }

		        Vector passedInvoiceOids = new Vector();
		        if (StringFunction.isNotBlank(lastPayMethod) &&
		        		InstrumentType.LOAN_RQST.equals(invUploadInstrType) &&
		        		TradePortalConstants.TRADE_LOAN.equals(loanType) &&
		        		TradePortalConstants.INVOICE_TYPE_PAY_MGMT.equals(invoiceClassification)){
		        	if (!processedInvoiceOids.contains(invoiceOids.get(0))){

			        	String country = InvoiceUtility.fetchOpOrgCountryForOpOrg(creationRuleDoc.getAttribute("/A_OP_BANK_ORG_OID"));

			        	boolean isPaymentMethodValid = InvoiceUtility.validatePaymentMethodForInvoices(invoiceOids, country, lastCurrency, mediatorServices.getErrorManager(),true);
			        	boolean isBankBranchCodeValid = InvoiceUtility.validateBankInformationForInvoices(invoiceOids, country, lastCurrency, mediatorServices.getErrorManager(),true);
			        	if (!isPaymentMethodValid || !isBankBranchCodeValid){
			        		processedInvoiceOids.addAll(invoiceOids);
			        		continue;
			        	}
			        	//Added Reporting code validations for Rel9.4 CR-1001 - Returns the set of invoiceOids that have passed Reporting code validations
			        	passedInvoiceOids = InvoiceUtility.validateReportingCodesForInvoices(invoiceOids, creationRuleDoc.getAttribute("/A_OP_BANK_ORG_OID"), mediatorServices.getErrorManager(), true);
			        }
		        }else{//In any other case, i.e, LRQ with other Loan types and ATP
		        	passedInvoiceOids = invoiceOids;
		        }
		        // Build the input document used to create a new instrument
		        DocumentHandler instrCreateDoc = buildInstrumentCreateDoc(creationRuleDoc,
		        		lastBenName, lastCurrency, totalAmount, latestDuepaymentDt, passedInvoiceOids);

		        // Create instrument using the document, assign the invoice items, and create log records.
		        try {

		        	createLRQInstrumentFromInvoiceData(instrCreateDoc, passedInvoiceOids, loanReqRuleName);
		        	invOIds.addAll(passedInvoiceOids);

		        }
		        catch (AmsException e) {
		        	// This is probably a sql error but could be anything.
		        	// Post this exception to the log.
		        	LOG.error("Something went wrong for corporate {}", getCorporateOrgOid(), e);
		        }
		    }
	 }



	/**
	  * This method prepares the doc object required for creating an Instrument
	  * with appropriate attributes for Loan Req Instrument.
	  * @param creationRuleDoc
	  * @param benName
	  * @param currency
	  * @param amount
	  * @param latestShipDt
	  * @param invoiceOids
	  * @return
	  */
	 private DocumentHandler buildInstrumentCreateDoc(DocumentHandler creationRuleDoc,
		      String benName, String currency, BigDecimal amount,
		      String latestShipDt, Vector invoiceOids) {

		 LOG.debug("buildInstrumentCreateDoc () creationRuleDoc-> {}", creationRuleDoc);
		// LOG.debug("benName:{}\t currency:{}\t amount:{}\t latestShipDt:{}\t invoiceOids:{}", benName, currency, amount, latestShipDt, invoiceOids);
		   DocumentHandler instrCreateDoc = new DocumentHandler();
			String secretKeyString = getMediatorServices().getCSDB().getCSDBValue("SecretKey");
			String instrumentOid = null;
			javax.crypto.SecretKey secretKey = null;
			String templateOid = creationRuleDoc.getAttribute("/INV_ONLY_CREATE_RULE_TEMPLATE");

			if (secretKeyString != null){
				byte[] keyBytes = com.amsinc.ecsg.util.EncryptDecrypt.base64StringToBytes(secretKeyString);
				secretKey = new javax.crypto.spec.SecretKeySpec(keyBytes, "DESede/CBC/PKCS5Padding");
				if (StringFunction.isNotBlank(templateOid)){
					instrumentOid = EncryptDecrypt.encryptStringUsingTripleDes(templateOid, secretKey);
				}

			}else{
				if (StringFunction.isNotBlank(templateOid)){
					instrumentOid = EncryptDecrypt.encryptStringUsingTripleDes(templateOid, secretKey);
				}
			}
		    instrCreateDoc.setAttribute("/instrumentOid", instrumentOid);
		    instrCreateDoc.setAttribute("/clientBankOid", getClientBankOid());
		    if (TradePortalConstants.AUTO_ATP_PO_UPLOAD.equals(invUploadInstrType)) {
				instrCreateDoc.setAttribute("/instrumentType",
					InstrumentType.APPROVAL_TO_PAY);
			} else {
				instrCreateDoc.setAttribute("/instrumentType",
					InstrumentType.LOAN_RQST);

			}
		    instrCreateDoc.setAttribute("/bankBranch", creationRuleDoc.getAttribute("/A_OP_BANK_ORG_OID"));
			//Srinivasu_D IR#T36000018442 Rel8.2 06/29/2013 - TEMPL if template being used else blank
		    if(StringFunction.isBlank(templateOid)){
		    instrCreateDoc.setAttribute("/copyType", TradePortalConstants.FROM_BLANK);
			}else {
			    instrCreateDoc.setAttribute("/copyType", TradePortalConstants.FROM_TEMPL);
			}
		    instrCreateDoc.setAttribute("/userOid", getUserOid());
		    instrCreateDoc.setAttribute("/ownerOrg", getCorporateOrgOid());
		    instrCreateDoc.setAttribute("/securityRights", getSecurityRights());
		    //IR 22917 - no bene name for EXP_FIN and SEL_REQ
		    if(!(TradePortalConstants.EXPORT_FIN.equals(loanType)
					|| TradePortalConstants.SELLER_REQUESTED_FINANCING.equals(financeType)))
		    instrCreateDoc.setAttribute("/benName", benName);
		    instrCreateDoc.setAttribute("/currency", currency);
		    instrCreateDoc.setAttribute("/shipDate", latestShipDt);
		    instrCreateDoc.setAttributeDecimal("/amount", amount);
		    instrCreateDoc.setAttribute("/importIndicator", importIndicator);
		    instrCreateDoc.setAttribute("/financeType", financeType);
		    instrCreateDoc.setAttribute("/uploadIndicator", TradePortalConstants.INDICATOR_YES);
		    instrCreateDoc.setAttribute("/usePaymentDateInd", getUsePaymentDate());
		    instrCreateDoc.setAttribute("/buyerBacked", buyerBacked);

		    if (StringFunction.isBlank(userLocale)){
		      userLocale = "en_US";
		    }
		    instrCreateDoc.setAttribute("/locale", userLocale);

		    for (int i = 0; i < invoiceOids.size(); i++) {
		      instrCreateDoc.setAttribute("/InvoiceLines(" + i + ")/uploadInvoiceOid",
		          (String) invoiceOids.elementAt(i));
		    }

		    LOG.debug("Creation rules document is {}", instrCreateDoc.toString());
		    return instrCreateDoc;
		  }

	 /**
	  * This method invokes the Instrument EJB and writes the invoice
	  * details in INVOICE_HISTORY table.
	  * @param inputDoc
	  * @param invoiceOids
	  * @param lcRuleName
	  * @return
	  * @throws AmsException
	  * @throws RemoteException
	  */
	 private DocumentHandler createLRQInstrumentFromInvoiceData(DocumentHandler inputDoc,
		      Vector invoiceOids, String lcRuleName)
		      throws AmsException, RemoteException {

		    LOG.debug("Entered createLRQInstrumentFromInvoiceData(). inputDoc follows ->\n {}", inputDoc.toString());
		    MediatorServices mediatorServices = getMediatorServices();
		    DocumentHandler outputDoc = null;
		    //Create a new instrument
		    Instrument newInstr = (Instrument) mediatorServices.createServerEJB("Instrument");
		    outputDoc = newInstr.createLoanReqForInvoiceData(inputDoc, invoiceOids, lcRuleName);

		    LOG.debug("Returned from instrument.createInstrumentFromPOData()\n"+ "Returned document follows -> {}", outputDoc);
		    try{
		    // Now that the transaction has been created, assign this transaction
		    // oid to all the line items on the transaction.
				LOG.debug("invoiceOids: {}"+invoiceOids);
				Transaction transaction = (Transaction) mediatorServices.createServerEJB("Transaction");
				long transactionOid = outputDoc.getAttributeLong("/transactionOid");
				transaction.getData(transactionOid);
				Terms terms = (Terms) transaction.getComponentHandle("CustomerEnteredTerms");
				String termsOid = terms.getAttribute("terms_oid");
		        POLineItemUtility.updateInvoiceTransAssignment(invoiceOids, outputDoc.getAttribute("/transactionOid"), newInstr.getAttribute("instrument_oid"),
			             TradePortalConstants.RECEIVABLE_UPLOAD_INV_STATUS_ASSIGNED,termsOid);
			        POLineItemUtility.createInvoiceHistoryLog (invoiceOids,TradePortalConstants.INVOICE_ACTION_AUTOCREATE,
			        		TradePortalConstants.RECEIVABLE_UPLOAD_INV_STATUS_ASSIGNED,getUserOid());

				String requiredOid = "upload_invoice_oid";
				 String invoiceOidsSql = POLineItemUtility.getPOLineItemOidsSql(invoiceOids, requiredOid);

				if (TradePortalConstants.INDICATOR_T.equals(importIndicator) &&
						TradePortalConstants.TRADE_LOAN.equals(loanType) &&
						TradePortalConstants.INVOICE_TYPE_PAY_MGMT.equals(invoiceClassification)){
					StringBuilder payMethodSQL = new StringBuilder("select distinct(PAY_METHOD) PAYMETHOD from invoices_summary_data where ");
					payMethodSQL.append(invoiceOidsSql);
					DocumentHandler resultDoc = DatabaseQueryBean.getXmlResultSet(payMethodSQL.toString(),false,new ArrayList<Object>());
					if (resultDoc != null){
						String payMethod = resultDoc.getAttribute("/ResultSetRecord(0)/PAYMETHOD");
						if (StringFunction.isNotBlank(payMethod)){
							terms.setAttribute("payment_method",payMethod);
							int singleBeneCount = InvoiceUtility.isSingleBeneficairyInvoice(invoiceOidsSql);
							if (singleBeneCount == 1){
								terms.setAttribute("loan_proceeds_credit_type",TradePortalConstants.CREDIT_BEN_ACCT);
								InvoiceUtility.populateTerms(terms,(String)invoiceOids.get(0),invoiceOids.size(),mediatorServices);
							}else if (singleBeneCount > 1){
								terms.setAttribute("loan_proceeds_credit_type",TradePortalConstants.CREDIT_MULTI_BEN_ACCT);
								//IR 29777 start
								if(StringFunction.isBlank(terms.getAttribute("c_FirstTermsParty")))
					            {
					                 terms.newComponent("FirstTermsParty");
					            }
								TermsParty firstTermsParty = (TermsParty) terms.getComponentHandle("FirstTermsParty");;
								firstTermsParty.setAttribute("terms_party_type", TermsPartyType.BENEFICIARY);

								if(!TradePortalConstants.EXPORT_FIN.equals(loanType))
								firstTermsParty.setAttribute("name", mediatorServices.getResourceManager().getText("Common.MultBenesEntered", TradePortalConstants.TEXT_BUNDLE));
								//IR 29777 end
								int paymentChargeCount=InvoiceUtility.isSinglePaymentCharge(invoiceOidsSql);
								if (paymentChargeCount == 1){
									InvoicesSummaryData invoice =
										(InvoicesSummaryData) mediatorServices.createServerEJB("InvoicesSummaryData",
												Long.parseLong((String)invoiceOids.get(0)));
									terms.setAttribute("bank_charges_type",invoice.getAttribute("charges"));
								}
								else if (paymentChargeCount > 1){
									terms.setAttribute("bank_charges_type",TradePortalConstants.CHARGE_UPLOAD_FW_SHARE);
									InvoiceUtility.setBankChargesForInvoices(invoiceOidsSql);
									 mediatorServices.getErrorManager().issueError(
									          TradePortalConstants.ERR_CAT_1, TradePortalConstants.INFO_SHARED_BANK_CHARGES);
									
								}
							}
							transaction.save(false);
						}

					}
				}
				//IR 31473 start
				if (TradePortalConstants.INDICATOR_T.equals(importIndicator) &&
						TradePortalConstants.TRADE_LOAN.equals(loanType) &&
						TradePortalConstants.INVOICE_TYPE_REC_MGMT.equals(invoiceClassification)){
					int paymentChargeCount=InvoiceUtility.isSinglePaymentCharge(invoiceOidsSql);
					if (paymentChargeCount == 1){
						InvoicesSummaryData invoice =
							(InvoicesSummaryData) mediatorServices.createServerEJB("InvoicesSummaryData",
									Long.parseLong((String)invoiceOids.get(0)));
						terms.setAttribute("bank_charges_type",invoice.getAttribute("charges"));
					}
					else if (paymentChargeCount > 1){
						terms.setAttribute("bank_charges_type",TradePortalConstants.CHARGE_UPLOAD_FW_SHARE);
					}
					transaction.save(false);
				}
				//IR 31473 end
				if (TradePortalConstants.INDICATOR_NO.equals(importIndicator) || TradePortalConstants.INDICATOR_X.equals(importIndicator)){

					 boolean useSeller = TradePortalConstants.BUYER_REQUESTED_FINANCING.equals(financeType) ;
					String invoiceDueDate = null;
					if (InvoiceUtility.validateSameCurrency(invoiceOidsSql) &&
							InvoiceUtility.validateSameDate(invoiceOidsSql) &&
							InvoiceUtility.validateSameTradingPartner(invoiceOidsSql, useSeller)){
						StringBuilder dueDateSQL = new StringBuilder("select distinct(case when payment_date is null then due_date else payment_date end ) DUEDATE " +
								" from invoices_summary_data where ");
						dueDateSQL.append(invoiceOidsSql);
						DocumentHandler resultDoc = DatabaseQueryBean.getXmlResultSet(dueDateSQL.toString(), false, new ArrayList<Object>());
						invoiceDueDate = resultDoc.getAttribute("/ResultSetRecord(0)/DUEDATE");
					}


					if (StringFunction.isNotBlank(invoiceDueDate)){

						try {
							SimpleDateFormat iso = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
							SimpleDateFormat jPylon = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");

								Date date = jPylon.parse(invoiceDueDate);

								terms.setAttribute("loan_terms_fixed_maturity_dt",TPDateTimeUtility.convertISODateToJPylonDate(iso.format(date)));
								transaction.save(false);
						} catch (Exception e) {
							LOG.error("Exception while saving terms / transaction.", e);
						}


					}
				}



		    } catch (AmsException e){
		        String [] substitutionValues = {"",""};
		        substitutionValues[0] = e.getMessage();
		        substitutionValues[1] = "0";
		         mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
		                 AmsConstants.SQL_ERROR, substitutionValues);
		         LOG.error("Exception occured ", e);
		    }


		    return outputDoc;
		  }



  /**
   * Setter for the base currency attribute
   *
   * @param newBaseCurrency java.lang.String
   */
  private void setBaseCurrency(String newBaseCurrency) {
    baseCurrency = newBaseCurrency;
  }

  /**
   * Setter for the clientBankOid attribute
   * @param newClientBankOid java.lang.String
   */
  private void setClientBankOid(String newClientBankOid) {
    clientBankOid = newClientBankOid;
  }

  /**
   * Setter for the corporateOrgOid attribute
   * @param newCorporateOrgOid java.lang.String
   */
  private void setCorporateOrgOid(String newCorporateOrgOid) {
    corporateOrgOid = newCorporateOrgOid;
  }

  /**
   * Setter for the auto upload log attribute
   * @param newLogger com.ams.tradeportal.busobj.util.StructuredPOLogger
   */
  private void setLogger(InvoiceLogger newLogger) {
      logger = newLogger;

  }


  /**
   * Setter for the logSequence attribute
   * @param newLogSequence java.lang.String
   */
  private void setLogSequence(String newLogSequence) {
    logSequence = newLogSequence;
  }

  /**
   * Setter for the mediator services attribute
   * @param newMediatorServices com.amsinc.ecsg.frame.MediatorServices
   */
  private void setMediatorServices(MediatorServices newMediatorServices) {
    mediatorServices = newMediatorServices;
  }

  /**
   * Setter for the security rights attribute
   * @param newSecurityRights java.lang.String
   */
  private void setSecurityRights(String newSecurityRights) {
    securityRights = newSecurityRights;
  }

  /**
   * Setter for the timeZone attribute
   * @param newTimeZone java.lang.String
   */
  private void setTimeZone(String newTimeZone) {
    timeZone = newTimeZone;
  }

  /**
   * Setter for the upload definition oid attribute
   * @param newUploadDefinitionOid long
   */
  private void setUploadDefinitionOid(long newUploadDefinitionOid) {
    uploadDefinitionOid = newUploadDefinitionOid;
  }

  /**
   * Setter for the upload file name attribute
   * @param newUploadFileName java.lang.String
   */
  private void setUploadFileName(String newUploadFileName) {
    uploadFileName = newUploadFileName;
  }

  /**
   * Setter for the uploadSequenceNumber attribute
   * @param newUploadSequenceNumber java.lang.String
   */
  private void setUploadSequenceNumber(
      String newUploadSequenceNumber) {
    uploadSequenceNumber = newUploadSequenceNumber;
  }

  /**
   * Setter for the user locale
   * @param newUserLocale java.lang.String
   */
  private void setUserLocale(String newUserLocale) {
    userLocale = newUserLocale;
  }

  /**
   * Setter for the userName attribute
   * @param newUserName java.lang.String
   */
  private void setUserName(String newUserName) {
    userName = newUserName;
  }

  /**
   * Setter for the userOid attribute
   * @param newUserOid java.lang.String
   */
  private void setUserOid(String newUserOid) {
    userOid = newUserOid;
  }

  /**
   * Setter for the subsidiaryAccessOrgOid attribute
   * @param subsidiaryAccessOrgOid java.lang.String
   */
  private void setSubsidiaryAccessOrgOid(String subsidiaryAccessOrgOid) {
    this.subsidiaryAccessOrgOid = subsidiaryAccessOrgOid;
  }

  /**
   * Getter for the subsidiaryAccessOrgOid attribute
   * @return java.lang.String
   */
  private String getSubsidiaryAccessOrgOid() {
    return this.subsidiaryAccessOrgOid;
  }

  /**
   * createInstrumentFromPOData()
   *
   * This method will call the createNew() method on instrument to create a new ImportLC/ATP/etc.
   * It will then set certain fields on the LC based on what data is in the grouped PO's.
   *
   * INPUTDOC - The input doc must contain the following
   *    /instrumentOid ---------- The existing instruments oid or "0" for a new instr.
   *    /transactionType -------- The type of the transaction being created
   *    /bankBranch ------------- The oid of the Operational Bank Org
   *    /clientBankOid ---------- The oid of the client bank that owns the corporation
   *    /userOid ---------------- The oid of the user creating the doc
   *    /securityRights --------- The security rights of the user that is creating the transaction
   *    /copyType --------------- What we copy a new instrument from (TP CONSTANTS: FROM_INSTR, FROM_TEMPL, & FROM_BLANK)
   *    /ownerOrg --------------- The oid of the user's corporate organization
   *    /benName ---------------- The name of the beneficiary of the PO Line Items
   *    /amount ----------------- The sum of the amounts of the PO Line Items
   *    /currency --------------- The currency of the amounts of the PO Line Items
   *    /shipDate --------------- The last shipment date of the amounts of the PO Line Items
   *
   * OUTPUTDOC - The output doc will contain the following
   *    /transactionOid --------- The oid of the issue tranasction for the new ImportLC/ATP/etc.
   *
   * @param inputDoc - described above
   * @param poOids Vector - list of po line item oids going in this instrument
   * @param poNums Vector - list of po num/line items going in this instrument
   * @param lcRuleName String - name of the lc creation rule causing this instrument creation
   * @return DocumentHandler outputDoc - described above
   */


}