package com.ams.tradeportal.mediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.RemoteException;
import java.util.Vector;

import com.ams.tradeportal.busobj.ClientBank;
import com.ams.tradeportal.busobj.CorporateOrganization;
import com.ams.tradeportal.busobj.OperationalBankOrganization;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.frame.MediatorBean;
import com.amsinc.ecsg.frame.MediatorServices;
import com.amsinc.ecsg.util.DateTimeUtility;
import com.amsinc.ecsg.util.DocumentHandler;
import com.ams.tradeportal.busobj.CustomerErpGlCode;
import com.ams.tradeportal.busobj.CustomerDiscountCode;

public class CUSREFPackagerMediatorBean extends MediatorBean {
private static final Logger LOG = LoggerFactory.getLogger(CUSREFPackagerMediatorBean.class);
	
	public DocumentHandler execute(DocumentHandler inputDoc, DocumentHandler outputDoc, MediatorServices mediatorServices)
	throws RemoteException, AmsException {
	try{
		String messageType   = TradePortalConstants.CUSREF;
		String destinationID = TradePortalConstants.DESTINATION_ID_PROPONIX;
		String senderID      = TradePortalConstants.SENDER_ID;
		String dateSent = null;
		String timeSent = null;
		String corp_org_oid = null;
		String bank_org_oid = null;
		String operationOrganizationID = null;
		String timeStamp = DateTimeUtility.getGMTDateTime(false);
		OperationalBankOrganization operationalBankOrganization = null;
		CorporateOrganization corpOrg = null;
	
		String processParameters = inputDoc.getAttribute("/process_parameters");
		String refDataTableType = getParm("REFTABLETYPE", processParameters);
		//String refDataTableOid = getParm("REFTABLEOID", processParameters);
		//
		String theOwnerOrgOid=getParm("REFTABLEOWNEROID", processParameters);
		corp_org_oid = theOwnerOrgOid; //AiA Rel 8.2 IR T36000011452 03/25/2013

		String messageID = inputDoc.getAttribute("/message_id");

		dateSent = timeStamp;
		timeSent = timeStamp.substring(11, 19);
		
		String code="";
		String description="";
		String gLCategory = "";
		String defaultGLCodeInd = "";
		String deactivateInd = "";

	   StringBuilder sqlStmntStr = new StringBuilder();
	   Vector theCodes = new Vector();
		
		
		if (refDataTableType.equals("DISCCODES")){
			//AiA Rel 8.2 IR T36000011452 03/25/2013
			   sqlStmntStr.append("select discount_code, discount_description, deactivate_ind, customer_disc_code_oid");
			   sqlStmntStr.append(" from customer_discount_code"); 			
			
		}
		
		if (refDataTableType.equals("ERPGLCODES")){		
			//AiA Rel 8.2 IR T36000011452 03/25/2013
			   sqlStmntStr.append("select erp_gl_code, erp_gl_description, erp_gl_category, customer_erp_gl_code_oid, default_gl_code_ind, deactivate_ind");
			   sqlStmntStr.append(" from customer_erp_gl_code"); 			
		}

		//	AiA Rel 8.2 IR T36000011452 03/25/2013
		   sqlStmntStr.append(" where p_owner_oid in (?) ");
		   
		
		//mediatorServices.debug("Ready to instantiate corporate org ");
		corpOrg = (CorporateOrganization) mediatorServices.createServerEJB("CorporateOrganization", Long.parseLong(corp_org_oid));

		String customerID = corpOrg.getAttribute("Proponix_customer_id");

		// Find the operational org from the corporate org just instantiated
		bank_org_oid = corpOrg.getAttribute("first_op_bank_org");
		mediatorServices.debug("Ready to instantiate operational bank org ");
		operationalBankOrganization = (OperationalBankOrganization) mediatorServices.createServerEJB("OperationalBankOrganization",Long.parseLong(bank_org_oid));
		
		if (operationalBankOrganization != null) 
		{
			operationOrganizationID = operationalBankOrganization.getAttribute("Proponix_id");
		}	
		
		long clientBankOid = corpOrg.getAttributeLong("client_bank_oid");
		ClientBank clientBank = (ClientBank) mediatorServices.createServerEJB("ClientBank", clientBankOid);
		String clientBankID = clientBank != null ? clientBank.getAttribute("OTL_id") : "";

		
		outputDoc = new DocumentHandler();

		outputDoc.setAttribute("/Proponix/Header/DestinationID", destinationID);
		outputDoc.setAttribute("/Proponix/Header/SenderID", senderID);
		outputDoc.setAttribute("/Proponix/Header/OperationOrganizationID", operationOrganizationID);
		outputDoc.setAttribute("/Proponix/Header/MessageType", messageType);
		outputDoc.setAttribute("/Proponix/Header/DateSent", dateSent);
		outputDoc.setAttribute("/Proponix/Header/TimeSent", timeSent);
		outputDoc.setAttribute("/Proponix/Header/MessageID", messageID);
		outputDoc.setAttribute("/Proponix/Header/ClientBankID",clientBankID);
		

        outputDoc.setAttribute("/Proponix/SubHeader/CustomerID", customerID);
        outputDoc.setAttribute("/Proponix/SubHeader/RefTableType", refDataTableType);
        
        //AiA Rel 8.2 IR T36000011452 03/25/2013
        //loop
        DocumentHandler codesDoc = DatabaseQueryBean.getXmlResultSet(sqlStmntStr.toString(), false, new Object[]{theOwnerOrgOid});
        DocumentHandler doc1 = null;
        if (codesDoc != null){
        	theCodes = codesDoc.getFragments("/ResultSetRecord");        
        }
        int numItems = theCodes.size();
        for(int ii=0; ii<numItems; ii++){
        	doc1 = (DocumentHandler) theCodes.elementAt(ii);
        	if(doc1 != null){
	        	if (refDataTableType.equals("DISCCODES")){
	        		code = doc1.getAttribute("/DISCOUNT_CODE");
	        		description = doc1.getAttribute("/DISCOUNT_DESCRIPTION");
	        		gLCategory = "";
	        		defaultGLCodeInd= "";
	        		deactivateInd = doc1.getAttribute("/DEACTIVATE_IND");
	        	}else if(refDataTableType.equals("ERPGLCODES")){
	        		code = doc1.getAttribute("/ERP_GL_CODE");
	        		description = doc1.getAttribute("/ERP_GL_DESCRIPTION");
	        		gLCategory = doc1.getAttribute("/ERP_GL_CATEGORY");
	        		defaultGLCodeInd= doc1.getAttribute("/DEFAULT_GL_CODE_IND");
	        		deactivateInd = doc1.getAttribute("/DEACTIVATE_IND");        		
	        	}
        	}
            //Body
            outputDoc.setAttribute("/Proponix/Body/CodeTable("+ii+")/Code", code);
            outputDoc.setAttribute("/Proponix/Body/CodeTable("+ii+")/Description", description);
            outputDoc.setAttribute("/Proponix/Body/CodeTable("+ii+")/GLCategory", gLCategory);
            outputDoc.setAttribute("/Proponix/Body/CodeTable("+ii+")/DefaultGLCodeInd", defaultGLCodeInd);
            outputDoc.setAttribute("/Proponix/Body/CodeTable("+ii+")/DeactivateInd", deactivateInd);        	
        	
        }
        
        

        		
	} catch (RemoteException e) {
		mediatorServices.getErrorManager().issueError(
				TradePortalConstants.ERR_CAT_MIDDLEWARE,
				TradePortalConstants.TASK_NOT_SUCCESSFUL,
				"CUSREFPackagerMediatorBean");
		e.printStackTrace();
	} catch (AmsException e) {
		mediatorServices.getErrorManager().issueError(
				TradePortalConstants.ERR_CAT_MIDDLEWARE,
				TradePortalConstants.TASK_NOT_SUCCESSFUL,
				"CUSREFPackagerMediatorBean");
		e.printStackTrace();

	} catch (Exception e) {
		mediatorServices.getErrorManager().issueError(
				TradePortalConstants.ERR_CAT_MIDDLEWARE,
				TradePortalConstants.TASK_NOT_SUCCESSFUL,
				"CUSREFPackagerMediatorBean");
		e.printStackTrace();

	}
		return outputDoc;
	}
	private String getParm(String parmName, String processingParms) {

		int locationOfParmName, startOfParmValue, endOfParmValue;
		String parmValue;

		locationOfParmName = processingParms.indexOf(parmName);

		// Search for the whole word only.
		// For example, getParm("invoice_oid=0|insvoice_staus=Authorised",
		// "insvoice_staus" should return 1.
		// Keep searching if the character before or after the occurrence is not
		// a word delimiter

		while ((locationOfParmName > 0
				&& processingParms.charAt(locationOfParmName - 1) != ' ' && processingParms
				.charAt(locationOfParmName - 1) != '|')
				|| (locationOfParmName >= 0
						&& locationOfParmName < processingParms.length()
								- parmName.length()
						&& processingParms.charAt(locationOfParmName
								+ parmName.length()) != ' ' && processingParms
						.charAt(locationOfParmName + parmName.length()) != '=')) {

			locationOfParmName = processingParms.indexOf(parmName,
					locationOfParmName + parmName.length());

		}

		if (locationOfParmName == -1) {
			return "";
		}

		startOfParmValue = processingParms.indexOf("=", locationOfParmName);

		if (startOfParmValue == -1) {
			return "";
		}

		startOfParmValue++;

		endOfParmValue = processingParms.indexOf("|", startOfParmValue);

		if (endOfParmValue == -1) {
			endOfParmValue = processingParms.length();
		}

		parmValue = processingParms.substring(startOfParmValue, endOfParmValue);

		return parmValue;
	}	
}