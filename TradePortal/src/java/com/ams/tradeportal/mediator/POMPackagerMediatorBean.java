package com.ams.tradeportal.mediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Vector;

import com.ams.tradeportal.busobj.util.InstrumentServices;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.frame.MediatorBean;
import com.amsinc.ecsg.frame.MediatorServices;
import com.amsinc.ecsg.frame.ObjectIDCache;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.GMTUtility;
import com.amsinc.ecsg.util.StringFunction;



/**
 * POMPackagerMediator will pick up each 'POM' (Purchase Order Master) on the Outgoing Queue and will
 * create a 'POR' (Purchase Order) entry for every purchase order associated with the referenced activity
 *
 *     Copyright  ? 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */

public class POMPackagerMediatorBean extends MediatorBean {
private static final Logger LOG = LoggerFactory.getLogger(POMPackagerMediatorBean.class);

	/*
	 * This mediator is now used to package POM Message for Purchase Orders in
	 * portal Take the transaction_oid as input, go to corresponding Purchase orders
	 * and create a message in outgoing queue.
	 */
	public DocumentHandler execute(DocumentHandler inputDoc, DocumentHandler outputDoc,
			MediatorServices mediatorServices) throws RemoteException, AmsException {
		try(Connection con =DatabaseQueryBean.connect(true);
			PreparedStatement insStmt = con.prepareStatement("INSERT INTO OUTGOING_QUEUE  (QUEUE_OID,DATE_CREATED, STATUS, MSG_TYPE, "
					+ "A_INSTRUMENT_OID, A_TRANSACTION_OID, MESSAGE_ID,PROCESS_PARAMETERS) VALUES (?,?,'" +TradePortalConstants.OUTGOING_STATUS_STARTED+"','" +TradePortalConstants.PO_MSG+"',?,?,?,?)");  // DK IR-ROUM072448406 Rel8.1 08/29/2012
				){
			String transactionOid = null;
			String instrumentOid = null;
			String messageId = null;
			String pOSelectSQL = null;
			String sellerSelctSQL = null;
			DocumentHandler nameList =null;
			DocumentHandler poDoc =null;
			Vector vVector = null;
			Vector poVector = null;
			String purchaseOrderOid = null;
		    String sellerAddress1 = null;
		    String sellerCity = null;
		    String country = null;
		    String groupRef = null;
		    String postalCode = null;
		    int numItems = 0;
		    int poItems = 0;
		    DocumentHandler poParameterDoc = new DocumentHandler();
			DocumentHandler xmlDoc = new DocumentHandler ();
					// DK IR-ROUM072448406 Rel8.1 08/29/2012
			 
			String senderID      = TradePortalConstants.SENDER_ID;
			transactionOid = inputDoc.getAttribute("/transaction_oid");
			instrumentOid = inputDoc.getAttribute("/instrument_oid");
			messageId = inputDoc.getAttribute("/message_id");
			groupRef = senderID+"-"+transactionOid;

//			sellerSelctSQL.append("select name,address_line_1,address_country,address_city,address_postal_code from "+
//						"terms_party tp inner join terms on   terms_party_oid = c_first_terms_party_oid inner join transaction on "+
//						"terms_oid =  C_CUST_ENTER_TERMS_OID where transaction.transaction_oid = "+transactionOid);

			// IR# RSUM051055326
			// get terms party from referenced by original transaction
			sellerSelctSQL = "select name,address_line_1,address_country,address_city,address_postal_code from terms_party tp " +
			                      "inner join terms on terms_party_oid = c_first_terms_party_oid " +
					              "inner join transaction on terms_oid =  C_CUST_ENTER_TERMS_OID " +
			                      "inner join instrument on transaction_oid = original_transaction_oid where instrument_oid = ?";

			nameList = DatabaseQueryBean.getXmlResultSet(sellerSelctSQL, false, new Object[]{instrumentOid});

		    if(nameList!= null){ //NAR IR-RSUM062247144 added condition to check for null
			  vVector = nameList.getFragments("/ResultSetRecord");
			  numItems = vVector.size();

			  DocumentHandler acctDoc = null;
			  for (int jLoop = 0; jLoop < numItems; jLoop++) {
				acctDoc  = (DocumentHandler) vVector.elementAt(jLoop);
				sellerAddress1 = acctDoc.getAttribute("/ADDRESS_LINE_1");
				sellerCity = acctDoc.getAttribute("/ADDRESS_CITY");
				country = acctDoc.getAttribute("/ADDRESS_COUNTRY");
				postalCode = acctDoc.getAttribute("/ADDRESS_POSTAL_CODE");
			  }
			}
			pOSelectSQL = "select purchase_order_oid from purchase_order where a_transaction_oid = ?";

			poDoc = DatabaseQueryBean.getXmlResultSet(pOSelectSQL, false, new Object[]{transactionOid});
			//IR WAUM032836550- execute only if poDoc not null
			if(poDoc!=null){
			poVector = poDoc.getFragments("/ResultSetRecord");
			poItems = poVector.size();

			for (int iLoop = 0; iLoop < poItems; iLoop++) {
			 poDoc =   (DocumentHandler) poVector.elementAt(iLoop);
				purchaseOrderOid = poDoc!=null? poDoc.getAttribute("/PURCHASE_ORDER_OID"):null;

				if(StringFunction.isNotBlank(purchaseOrderOid)){
				int grpNum = iLoop+1;
				poParameterDoc.setAttribute("/PO_oid",purchaseOrderOid);
				poParameterDoc.setAttribute("/GroupReferenceNumber",groupRef);
				poParameterDoc.setAttribute("/GroupTotalNo",grpNum+"/"+poItems);
				poParameterDoc.setAttribute("/GroupReferenceType",TradePortalConstants.PO_GROUPREF_TYPE);
				poParameterDoc.setAttribute("/SellerAddressLine1",sellerAddress1);
				poParameterDoc.setAttribute("/SellerCity",sellerCity);
				poParameterDoc.setAttribute("/SellerPostalCode",postalCode);
				poParameterDoc.setAttribute("/SellerCountry",country);

				xmlDoc.setComponent("/POR_param",poParameterDoc);
				// DK IR-ROUM072448406 Rel8.1 08/29/2012	Start
				createMessage(inputDoc,xmlDoc.toString(),mediatorServices,insStmt);
				}
			 }
			insStmt.executeBatch();
			mediatorServices.debug("added POs:  "  +poItems);
			insStmt.close();
			// DK IR-ROUM072448406 Rel8.1 08/29/2012	End
		  }
		}
		catch (Exception e) {
			mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,
					TradePortalConstants.TASK_NOT_SUCCESSFUL, "POR Message Packaging");
			if(outputDoc != null){
			mediatorServices.debug("The outputDoc generated so far is:" + "\n" + outputDoc.toString());
			}
				e.printStackTrace();

		}
		
		return outputDoc;
	}

		/**
		 * This method read process params from xmlstring and creates a messages for each PO for a given
		 * transaction oid.
		 * @param inputDoc
		 * @param xmlStr
		 * @param mediatorServices
		 * @throws AmsException
		 * @throws RemoteException
		 */
		private void createMessage(DocumentHandler inputDoc, String xmlStr,MediatorServices mediatorServices, PreparedStatement insStmt) throws AmsException, RemoteException{

			String transactionOid = inputDoc.getAttribute("/transaction_oid");
			String instrumentOid = inputDoc.getAttribute("/instrument_oid");
			// DK IR-ROUM072448406 Rel8.1 08/29/2012	Start

			//Ravindra - 12th Oct 2012 - IR-T36000006218 - Start
			//Date should be retrived based on overrideDate indicator not from Java util
			Date date = GMTUtility.getGMTDateTime(true, true);
			//Ravindra - 12th Oct 2012 - IR-T36000006218 - End

			Timestamp time = new Timestamp(date.getTime());
			ObjectIDCache oid = ObjectIDCache.getInstance(AmsConstants.OID);

			try{
				insStmt.setLong(1,oid.generateObjectID());
				insStmt.setTimestamp(2, time);
				insStmt.setString(3, instrumentOid);
				insStmt.setString(4,transactionOid);
				insStmt.setString(5,InstrumentServices.getNewMessageID());
				insStmt.setString(6,xmlStr);
				insStmt.addBatch();
			}
			catch(SQLException e){
				throw new AmsException(e.getMessage());
			}
			// DK IR-ROUM072448406 Rel8.1 08/29/2012	End
		}
}
