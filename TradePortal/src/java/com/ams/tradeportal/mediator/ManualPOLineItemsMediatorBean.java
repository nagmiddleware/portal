package com.ams.tradeportal.mediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class is used for adding manually entered PO line items to a
 * a shipment of an Import LC Issue transaction. 
 *

 *
 *     Copyright  � 2003
 *     American Management Systems, Incorporated
 *     All rights reserved
 */

import com.amsinc.ecsg.util.*;
import javax.ejb.*;
import java.rmi.*;
import javax.naming.*;
import javax.ejb.*;
import java.util.*;
import java.math.*;
import java.text.*;
import com.amsinc.ecsg.frame.*;
import com.ams.tradeportal.busobj.*;
import com.ams.tradeportal.common.*;
import com.ams.tradeportal.busobj.util.*;

public class ManualPOLineItemsMediatorBean extends MediatorBean
{
private static final Logger LOG = LoggerFactory.getLogger(ManualPOLineItemsMediatorBean.class);

   private static final String NEWPOLINEITEM = "NEW";

   // Declare instance variables 
   DocumentHandler    poLineItemDoc           = null;
   Vector             poLineItemsList         = new Vector();
   String             shipmentOid             = "";
   String             transactionOid          = "";
   String             instrumentOid           = "";
   String             prevPOLineItemCurrency  = null;
   long               definitionOid;
   int                numPOLineItems          = 0;
   int                poLineItemCount         = 0;
   int                newPOLineItemCount      = 0;
   int                deletedPOLineItemCount  = 0;
   Hashtable          poNumberList;
   
   // <Amit - IR EEUE032659500 -05/31/2005 >
   String			  userLocale 			  = null;
   // </Amit - IR EEUE032659500 -05/31/2005 >            

  /**
   * This method performs addition of PO Line Items
   * The format expected from the input doc is:
   *
   * <ShipmentTerms>
   *	<POLineItemList ID="0"><poLineItemOid>1000001</POLineItem>
   *	<POLineItemList ID="1"><poLineItemOid>1000002</POLineItem>
   *	<POLineItemList ID="2"><poLineItemOid>1000003</POLineItem>
   *	<POLineItemList ID="3"><poLineItemOid>1000004</POLineItem>
   *	<POLineItemList ID="6"><poLineItemOid>1000001</POLineItem>
   * </ShipmentTerms>
   *
   * This is a transactional mediator that executes transactional
   * methods in business objects
   *
   * @param inputDoc The input XML document 
   * @param outputDoc The output XML document 
   * @param newMediatorServices Associated class that contains lots of the
   * auxillary functionality for a mediator.
   */
   public DocumentHandler execute(DocumentHandler inputDoc, DocumentHandler outputDoc,
                                  MediatorServices mediatorServices) throws RemoteException, AmsException
   {
     String buttonPressed       = "";

     // Reset instance variables
     poLineItemDoc          = null;
     prevPOLineItemCurrency = null;
     poLineItemCount        = 0;
     newPOLineItemCount     = 0;
     deletedPOLineItemCount = 0;
     prevPOLineItemCurrency = "";
     poNumberList           = new Hashtable();

     // Get the instrument oid from the doc
     instrumentOid = inputDoc.getAttribute("/Instrument/instrument_oid");

     // Get the transaction oid from the doc	
     transactionOid = inputDoc.getAttribute("/Transaction/transaction_oid");

     // Get the shipment oid from the doc
     shipmentOid = inputDoc.getAttribute("/ShipmentTerms/shipment_oid");

     // Get the po definition oid
     definitionOid = inputDoc.getAttributeLong("/definitionOid");

     // Retrieve the list of PO Line Items and determine the PO Line Item count
     poLineItemsList = inputDoc.getFragments("/ShipmentTerms/POLineItemList");
     numPOLineItems  = poLineItemsList.size();

     // Determine which button was pressed by the user
     buttonPressed = inputDoc.getAttribute("/Update/ButtonPressed");
     
	 //	<Amit - IR EEUE032659500 -05/31/2005 >
	 userLocale    = mediatorServices.getCSDB().getLocaleName();

	 // if the user locale not specified default to en_US
	 if((userLocale == null) || (userLocale.equals("")))
	 {
	   userLocale		  = "en_US";
	 }
	 //</Amit - IR EEUE032659500 -05/31/2005 >

     if (buttonPressed.equals(TradePortalConstants.BUTTON_ADD_PO_LINE_ITEM))
     {
       return outputDoc;
     }
     else if (buttonPressed.equals(TradePortalConstants.BUTTON_ADD_FIVE_PO_LINE_ITEMS))
     {
       return outputDoc;
     }
     else if (buttonPressed.equals(TradePortalConstants.BUTTON_SAVEANDCLOSE)) 
     {
       getNewPOLineItems(inputDoc);
       if (validatePOLineItems(inputDoc, mediatorServices) == true) 
         return outputDoc;
       performSave(inputDoc, mediatorServices, buttonPressed);
     }
     else if (buttonPressed.equals(TradePortalConstants.BUTTON_DELETE_SELECTED_PO_ITEMS))
     {
       deleteSelectedPOLineItems(inputDoc, mediatorServices);
       if (deletedPOLineItemCount == 0) return outputDoc;
       performSave(inputDoc, mediatorServices, buttonPressed);
     }

     outputDoc.setAttribute("/updatedPODetails", "true");

     return outputDoc;
   }

  /**
   * This method uses the input document to get an instance of the shipment, 
   * populate its data (including the PO Line Item subcomponents) and save them. 
   *
   * This method catches an invalid object identifier.  All other exceptions
   * should be caught by the caller.
   *
   * @return String buttonPressed
   * @param inputDoc com.amsinc.ecsg.util.DocumentHandler
   * @param mediatorServices com.amsinc.ecsg.frame.MediatorServices
   */

   private void performSave(DocumentHandler inputDoc, MediatorServices mediatorServices, String buttonPressed)
	 throws RemoteException, AmsException 
   {
     int            numShipments      = 0;
     int            numPOLineItems    = 0;
     ShipmentTerms  shipmentTerms     = null;
     Transaction    transaction       = null;
     Instrument     instrument        = null;
     Terms          terms             = null;
     ComponentList  shipmentTermsList = null;
     ComponentList  poLineItemList    = null;

     try {

       // Get a handle to an instance of the instrument, transaction, and terms objects.
       instrument = (Instrument) mediatorServices.createServerEJB("Instrument");
       instrument.getData(Long.parseLong(instrumentOid));
       transaction = (Transaction) instrument.getComponentHandle("TransactionList", Long.parseLong(transactionOid));
       terms = (Terms) transaction.getComponentHandle("CustomerEnteredTerms");

       // Get handle to the business object
       shipmentTermsList = (ComponentList) terms.getComponentHandle("ShipmentTermsList");
       numShipments = shipmentTermsList.getObjectCount();
       shipmentTerms = (ShipmentTerms)shipmentTermsList.getComponentObject(Long.parseLong(shipmentOid));
       shipmentTerms.populateFromXmlDoc(inputDoc);

       // Call methods which will derive the transaction amount, transaction date, 
       // and the shipment's good description from manually entered PO Line Items
       poLineItemList = (ComponentList) shipmentTerms.getComponentHandle("POLineItemList");
       numPOLineItems = poLineItemList.getObjectCount();
	
	   //<Amit - IR EEUE032659500 -05/31/2005 >
       boolean isStructuredPO = false;
       POLineItemUtility.deriveTransactionDataFromPO(isStructuredPO,transaction, shipmentTerms, numShipments, poLineItemList, 
                                                     numPOLineItems, definitionOid, getSessionContext(),userLocale);
	   //</Amit - IR EEUE032659500 -05/31/2005 >

       // Save the data through the top level parent, instrument
       instrument.save(false);

     } catch (InvalidObjectIdentifierException ex) {
       // If the object does not exist, issue an error
       mediatorServices.getErrorManager ().issueError (getClass ().getName (), "INF01", ex.getObjectId());
		
     } catch (Exception e) {
       LOG.info("General exception caught in ManualPOLineItemsMediator");
       e.printStackTrace();
     } 

     // Issue an informational message indicating to the user that PO line items
     // have been added, removed, or updated successfully
     if (this.isTransSuccess(mediatorServices.getErrorManager()))
     {
       if ((buttonPressed.equals(TradePortalConstants.BUTTON_SAVEANDCLOSE)) && (newPOLineItemCount > 0)) 
       {
         mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                                                       TradePortalConstants.ADD_PO_LINE_ITEMS_SUCCESS,
                                                       String.valueOf(newPOLineItemCount));
       }
       else if ((buttonPressed.equals(TradePortalConstants.BUTTON_DELETE_SELECTED_PO_ITEMS)) && 
                                                               (deletedPOLineItemCount > 0)) 
       {
         mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                                                       TradePortalConstants.REMOVE_PO_LINE_ITEMS_SUCCESS,
                                                       String.valueOf(deletedPOLineItemCount));
       }
       else
       {
         mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                                                       TradePortalConstants.UPDATE_SUCCESSFUL,
                                                       "PO line items");
       }
     }
   }

  /**
   * This method checks for any new PO Line Items in the document
   *
   * @param inputDoc com.amsinc.ecsg.util.DocumentHandler
   */

   private void getNewPOLineItems(DocumentHandler inputDoc)
	 throws RemoteException, AmsException 
   {
     String poLineItemOid = "";

     // Determine if any new po line items have been added
     for (poLineItemCount=0; poLineItemCount<numPOLineItems; poLineItemCount++)
     {
       poLineItemDoc = (DocumentHandler) poLineItemsList.elementAt(poLineItemCount);
       poLineItemOid = poLineItemDoc.getAttribute("/po_line_item_oid");
       if (InstrumentServices.isBlank(poLineItemOid))
       {
         newPOLineItemCount++;
       }
     }
   }

  /**
   * This method is called when the user selects the Delete All Selected PO Line Items
   * button.  First we determine if POs were selected to be deleted.  Next we delete
   * any PO Line Items that have an oid from the database; PO Line Items that do not
   * have an oid but were selected for deletion are removed in the premediator.  Finally
   * any PO Line Items not selected for deletion are removed from the input document
   * because this data should not be saved at this time.
   *
   * @param inputDoc com.amsinc.ecsg.util.DocumentHandler
   * @param mediatorServices com.amsinc.ecsg.frame.MediatorServices
   */

   private void deleteSelectedPOLineItems(DocumentHandler inputDoc, MediatorServices mediatorServices)
	 throws RemoteException, AmsException 
   {
     DocumentHandler deleteDoc         = null;
     DocumentNode    poLineItemDocRoot = null;
     String          poLineItemOid     = null;
     String          substitutionParm  = null;
     int             poLineItemId      = 0;
     int             deleteCount       = 0;
     Vector          deletePOList      = new Vector();
     POLineItem      poLineItem        = null;

     try {

       deletedPOLineItemCount = Integer.parseInt(inputDoc.getAttribute("/deleteCount"));
       if (deletedPOLineItemCount == 0)
       {
         substitutionParm = mediatorServices.getResourceManager().getText("PODetailsEntry.DeleteMessage",
                                                                          TradePortalConstants.TEXT_BUNDLE);
         mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                                                       TradePortalConstants.NO_ITEM_SELECTED, substitutionParm); 
       }
       else
       { 
         deletePOList = inputDoc.getFragments("/ComponentToDelete");
         deleteCount  = deletePOList.size();

         for (poLineItemCount=0; poLineItemCount<deleteCount; poLineItemCount++) 
         {
	   deleteDoc     = (DocumentHandler) deletePOList.elementAt(poLineItemCount);
           poLineItemOid = deleteDoc.getAttribute("/Oid"); 

           if (!InstrumentServices.isBlank(poLineItemOid))
           {
             poLineItem = (POLineItem) mediatorServices.createServerEJB("POLineItem");
             poLineItem.getData(Long.parseLong(poLineItemOid));
             poLineItem.delete();
           }
         }

         // Remove all PO Line Items from the document that were not selected to be deleted
         // so this data is not saved
         for (poLineItemCount=0; poLineItemCount<numPOLineItems; poLineItemCount++)
         {
           poLineItemDoc = (DocumentHandler) poLineItemsList.elementAt(poLineItemCount);

           // Get the id of the doc's root node
           // We must use this id value instead of simply using the po count because other
           // components may have been removed in the premediator which could cause the 
           // id to be out of sync with the po count
           poLineItemDocRoot = poLineItemDoc.getRootNode();
           poLineItemId      = Integer.parseInt(poLineItemDocRoot.getIdName());

           inputDoc.removeComponent("/ShipmentTerms/POLineItemList("+poLineItemId+")");
         }
       }

     } catch (InvalidObjectIdentifierException ex) {
       // If the object does not exist, issue an error
       mediatorServices.getErrorManager ().issueError (getClass ().getName (), "INF01", ex.getObjectId());
		
     } catch (Exception e) {
       LOG.info("General exception caught in ManualPOLineItemsMediator");
       e.printStackTrace();
     } 
   }

  /**
   * This method is used to validate the po line item data entered by the user. 
   * First, it initializes a PO Line Item Data object with information from the 
   * PO definition. For each PO line item in the document, the method will 
   * validate all the data in the PO line item.
   *
   * @param inputDoc com.amsinc.ecsg.util.DocumentHandler
   * @param mediatorServices com.amsinc.ecsg.frame.MediatorServices
   * @return boolean
   */

   private boolean validatePOLineItems(DocumentHandler inputDoc, MediatorServices mediatorServices)
         throws AmsException, RemoteException
   {
     POLineItemData   poLineItemData            = null;
     boolean          errorsFound               = false;
     boolean          currencyErrorsFound       = false;
     String           poLineItemOid             = null; 
     String           poLineItemNumber          = null;
     String           transactionCurrency       = null;
     String           poLineItemCurrency        = null;
     String           prevPOLineItemCurrency        = null;
     String           corpOrgOid                = null;
     String           lastShipDate              = null;

     // Get the transaction's currency
     transactionCurrency = inputDoc.getAttribute("/transactionCurrency");

     // Get the Corporate Org Oid
     corpOrgOid = inputDoc.getAttribute("/corpOrgOid");

     // Loop through all of the PO Line Items in the input document
     for (poLineItemCount=0; poLineItemCount<numPOLineItems; poLineItemCount++)
     {
       // Set the PO Line Item Number to be passed into the PO Line Item Data object
       // for validation
       poLineItemNumber = String.valueOf(poLineItemCount + 1);

       // First, initialize the PO Line item data object
       poLineItemDoc = (DocumentHandler) poLineItemsList.elementAt(poLineItemCount);
       poLineItemData = new POLineItemData(definitionOid, poLineItemNumber, poLineItemDoc,
                                           mediatorServices.getErrorManager(),
                                           getSessionContext());

       // Validate all the values for each PO line item field; if a required field 
       // is missing or invalid, continue with the next PO line item
       if (poLineItemData.validatePOLineItemData() == false)
       {
	 errorsFound = true;
       }

       // Set the last shipment date after it has been formatted by POLineItemData
       lastShipDate = poLineItemData.getLastShipmentDate();
       if (lastShipDate != null)
       {
         inputDoc.setAttribute("/ShipmentTerms/POLineItemList("+poLineItemCount+")/last_ship_dt", lastShipDate);
       }     

       // Validate PO Line Item currency
       if (currencyErrorsFound == false)
       {
         poLineItemCurrency = poLineItemDoc.getAttribute("/currency");
         if (!InstrumentServices.isBlank(poLineItemCurrency))
         {
           // Set the previous po currency if this is the first po line item currency found
           if (InstrumentServices.isBlank(prevPOLineItemCurrency))
             prevPOLineItemCurrency = poLineItemCurrency;

           currencyErrorsFound = poLineItemData.validatePOLineItemCurrency(poLineItemCurrency, prevPOLineItemCurrency,
                                                                           transactionCurrency, transactionOid,
                                                                           shipmentOid);
           prevPOLineItemCurrency = poLineItemCurrency;
           if (currencyErrorsFound == true)
             errorsFound = true;
         }
       }

       // Validate the PO Line Item Po Number and Item Number combination
       if (doesMatchingPOLineItemExist(poLineItemDoc, corpOrgOid, poLineItemNumber, mediatorServices) == true)
       {
         errorsFound = true;
       }
     }
     return errorsFound;
   }  

  /**
   * This method validates that the PO Line Item's PO Number and Item Number
   * is a unique combination
   *
   * @param poLineItemDoc com.amsinc.ecsg.util.DocumentHandler
   * @param corpOrgOid String
   * @param poLineItemNumber String
   * @param mediatorServices com.amsinc.ecsg.frame.MediatorServices
   * @return boolean
   */

   private boolean doesMatchingPOLineItemExist(DocumentHandler poLineItemDoc, String corpOrgOid, String poLineItemNumber,
                                           MediatorServices mediatorServices) throws AmsException, RemoteException
   {
     boolean           errorsFound            = false;
     DocumentHandler   matchingPOLineItemsDoc = null;
     StringBuilder      sqlQuery               = null;
     String            poLineItemOid          = null;
     String            poNumber               = null;
     String            itemNumber             = null;
     String            htKey                  = "";
     String            matchingPOLineItemOid  = null;

     try {

       // Compose the SQL to retrieve PO Line Items for a particualar Corporate Customer
       // that match the PO number and line item number passed in
       poLineItemOid = poLineItemDoc.getAttribute("/po_line_item_oid");
       
       // The po line item oid is empty if this is a new line item. Set the oid to 'new'
       // indicating this is a new line item in the hashtable of existing po numbers
       if (InstrumentServices.isBlank(poLineItemOid))
       {
         poLineItemOid = NEWPOLINEITEM + poLineItemNumber;
       }

       poNumber      = poLineItemDoc.getAttribute("/po_num");
       itemNumber    = poLineItemDoc.getAttribute("/item_num");
       if (!InstrumentServices.isBlank(poNumber))
       {
         // Create a hashtable key using the PO Number
         htKey = htKey.concat(poNumber);

         List<Object> sqlParmsLst = new ArrayList();
         sqlQuery = new StringBuilder();
         sqlQuery.append("select po_line_item_oid from po_line_item where a_owner_org_oid = ? ");
         sqlQuery.append(" and po_num = ? ");
         sqlParmsLst.add(corpOrgOid);
         sqlParmsLst.add(poNumber);

         // Add item num to the sql if its not null
         if (!InstrumentServices.isBlank(itemNumber))
         {
           // Add Item Number to the hashtable key 
           htKey = htKey.concat(itemNumber);

           sqlQuery.append(" and item_num = ? ");
           sqlParmsLst.add(itemNumber);
         }
         else
         {
           sqlQuery.append(" and item_num is null");
         }

         // Check if any matching po line items are found in memory (input document)
         matchingPOLineItemOid  = (String)poNumberList.get(htKey);
         if ((matchingPOLineItemOid != null) && (!matchingPOLineItemOid.equals(poLineItemOid)))
         {
           mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                                                         TradePortalConstants.PO_LINE_ITEM_MATCH_FOUND,
                                                         poLineItemNumber);
           errorsFound = true;
         }

         // If no match po line items were found in memory, check for matches in the database
         if (errorsFound == false)
         {
           matchingPOLineItemsDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery.toString(), false, sqlParmsLst);
           if (matchingPOLineItemsDoc != null)
           {
             matchingPOLineItemOid = matchingPOLineItemsDoc.getAttribute("/ResultSetRecord(0)/PO_LINE_ITEM_OID");
             if (!matchingPOLineItemOid.equals(poLineItemOid))
             {
               mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                                                             TradePortalConstants.PO_LINE_ITEM_MATCH_FOUND,
                                                             poLineItemNumber);
               errorsFound = true;
             }
           }
         }

         // If this PO Number/Item Number combination did not match an existing PO Line Item
         // then add this to the list of existing items
         if (errorsFound == false)
         {
           poNumberList.put(htKey, poLineItemOid);
         }
       }

     } catch (InvalidObjectIdentifierException ex) {
       // If the object does not exist, issue an error
       mediatorServices.getErrorManager ().issueError (getClass ().getName (), "INF01", ex.getObjectId());
		
     } catch (Exception e) {
       LOG.info("General exception caught in ManualPOLineItemsMediator");
       e.printStackTrace();
     }  
     return errorsFound;
   } 
}