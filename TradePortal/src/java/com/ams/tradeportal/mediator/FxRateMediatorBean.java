package com.ams.tradeportal.mediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.busobj.*;
import com.ams.tradeportal.common.*;
import com.amsinc.ecsg.util.*;
import com.amsinc.ecsg.frame.*;
import javax.ejb.*;
import javax.naming.*;
import java.rmi.*;
import java.util.*;
import com.ams.tradeportal.busobj.util.*;
/*
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class FxRateMediatorBean extends MediatorBean
{
private static final Logger LOG = LoggerFactory.getLogger(FxRateMediatorBean.class);
   public static final String INSERT_MODE = "C";
   public static final String UPDATE_MODE = "U";
   public static final String DELETE_MODE = "D";

   // Business methods
   public DocumentHandler execute(DocumentHandler inputDoc, DocumentHandler outputDoc, MediatorServices mediatorServices)
								  throws RemoteException, AmsException
   {
	  ClientServerDataBridge   csdb              = null;
	  StringBuffer             updateText        = new StringBuffer();
	  FXRate                   fxRate            = null;
	  String                   updateButtonValue = null;
	  String                   deleteButtonValue = null;
	  String                   currencyCode      = null;
	  String                   userLocale        = null;
	  String                   updateMode        = this.UPDATE_MODE;
	  long                     fxRateOid         = 0;

	  // Now determine what update mode the mediator is in. (Note: do
	  // not edit the delete button path to use '..' notation, as the
	  // first path won't be found in the case of delete requests.

	  updateButtonValue = inputDoc.getAttribute("/Action/saveXPos");
	  deleteButtonValue = inputDoc.getAttribute("/Action/deleteXPos");

	  if (updateButtonValue != null)
	  {
		 updateMode = this.UPDATE_MODE;
	  }
	  else if (deleteButtonValue != null)
	  {
		 updateMode = this.DELETE_MODE;
	  }

	  String buttonPressed = inputDoc.getAttribute("/Update/ButtonPressed");
	  mediatorServices.debug("The button pressed is " + buttonPressed);

	  if (buttonPressed.equals(TradePortalConstants.BUTTON_SAVE))
		  updateMode = this.UPDATE_MODE;
	  else if (buttonPressed.equals(TradePortalConstants.BUTTON_DELETE))
		  updateMode = this.DELETE_MODE;

	  if (updateMode == null)
	  {
		 throw new AmsException("Unable to determine update mode.");
	  }

	  // Perform update, delete, or insert.
	  try
	  {
		 // Get the oid from the doc.  '0' indicates a new FX rate so
		 // change the mode.
		 fxRateOid = inputDoc.getAttributeLong("/FXRate/fx_rate_oid");

		 if (fxRateOid == 0)
		 {
			updateMode = this.INSERT_MODE;
		 }

		 // Get a handle to an instance of the object
		 fxRate = (FXRate) mediatorServices.createServerEJB("FXRate");

		 if (updateMode.equals(this.INSERT_MODE))
		 {
			mediatorServices.debug("Mediator: Creating new FX rate");

			fxRate.newObject();
		 }
		 else
		 {
			mediatorServices.debug("Mediator: Retrieving FX rate " + fxRateOid);

			fxRate.getData(fxRateOid);
		 }
		 
		 String loginUserRights = inputDoc.getAttribute("/LoginUser/security_rights");
		 SecurityAccess.validateUserRightsForRefdata("FXRate", loginUserRights, updateMode, inputDoc.getAttribute("/FXRate/owner_org_oid"), inputDoc, fxRate);

		 if (updateMode.equals(this.DELETE_MODE))
		 {
			mediatorServices.debug("Mediator: Deleting FX rate object");

			// Set the optimistic lock attribute to make sure the FX rate hasn't been changed by someone else
			// since it was first retrieved
			fxRate.setAttribute("opt_lock", inputDoc.getAttribute("../opt_lock"));

			// Delete the FX rate
			fxRate.delete();

			mediatorServices.debug("Mediator: Completed the delete of the FX rate object");
		 }
		 else
		 {
			// Set the FX rate's currency code and corporate org oid; this should already be set
			// if we're in update mode
			if (updateMode.equals(this.INSERT_MODE))
			{
			   currencyCode = inputDoc.getAttribute("../currency_code");

			   fxRate.setAttribute("currency_code",     currencyCode);
			   //cquinton - 3/22/2011 Rel 7.0.0 cr610 - change corporate_org_oid change to owner_org_oid
			   //fxRate.setAttribute("corporate_org_oid", inputDoc.getAttribute("../corporate_org_oid"));
               fxRate.setAttribute("owner_org_oid", inputDoc.getAttribute("../owner_org_oid")); //AAlubala Rel 7.0.0.0 CR 610 changed name to owner
			}
			else
			{
			   // Set the optimistic lock attribute to make sure the FX rate hasn't been changed by someone else
			   // since it was first retrieved
			   fxRate.setAttribute("opt_lock", inputDoc.getAttribute("../opt_lock"));
			}

			// Set the new FX rate's multiply indicator
			fxRate.setAttribute("multiply_indicator", inputDoc.getAttribute("../multiply_indicator"));

			// Set the new FX rate's rate
			fxRate.setAttribute("rate", inputDoc.getAttribute("../rate"));
			//CM IAZ 10/14/08 Begin
			fxRate.setAttribute("buy_rate", inputDoc.getAttribute("../buy_rate"));
			fxRate.setAttribute("sell_rate", inputDoc.getAttribute("../sell_rate"));
			//CM IAZ 10/14/08 End
			
			//AAlubala Rel 7.0.0.0 CR 610 02/02/2011
			fxRate.setAttribute("fx_rate_group", inputDoc.getAttribute("../fx_rate_group"));
			
			//AAlubala IR#IAUL050662274 04/27/2011 - Save the base currency for the organization
			fxRate.setAttribute("org_base_currency", inputDoc.getAttribute("../org_base_currency"));

			// DK CR-640 Rel7.1 BEGINS
			//Vshah - Rel7.1 - IR#IR#SNUL122152487 - 01/04/2012 - <BEGIN>
			//Commenting out below 2 lines as these 2 fields are protected from data entry, and only for diplay.
			//It will always be updated via a feed from the TPS. 
			//fxRate.setAttribute("max_spot_rate_amount", inputDoc.getAttribute("../max_spot_rate_amount"));
			//fxRate.setAttribute("max_deal_amount", inputDoc.getAttribute("../max_deal_amount"));
			//Vshah - Rel7.1 - IR#IR#SNUL122152487 - 01/04/2012 - <END>
			// DK CR-640 Rel7.1 ENDS

			// Save the FX rate
			fxRate.save();
			//fxRate.save(true);

			mediatorServices.debug("Mediator: Completed the save of the FX rate object");
		 }
	  }
	  catch (AmsException e)
	  {
		 LOG.info("AmsException occurred in FxRateMediator: " + e.toString());
		 e.printStackTrace();
	  }
	  catch (RemoteException e)
	  {
		 LOG.info("RemoteException occurred in FxRateMediator: " + e.toString());
		 e.printStackTrace();
		 
	  }
	  catch (Exception e){
		  LOG.info("General Exception occured in FXRateMediator: "+e.toString());
		  e.printStackTrace();
	  }
	  finally
	  {
		 mediatorServices.debug("Mediator: Result of FX rate update is "
								+ this.isTransSuccess(mediatorServices.getErrorManager()));

		 if (this.isTransSuccess(mediatorServices.getErrorManager()))
		 {
			// Get the update text for the audit log and error messages; if the FX rate is being inserted,
			// lookup the currency code of the new FX rate and append the description found for it to the
			// update text (this info is already known for update and delete modes).
			updateText.append(inputDoc.getAttribute("/UpdateText/currency_descr"));

			if (updateMode.equals(this.INSERT_MODE))
			{
			   // Get the user's locale from the client server data bridge
	           csdb       = mediatorServices.getCSDB();
	           userLocale = csdb.getLocaleName();

			   updateText.append(ReferenceDataManager.getRefDataMgr().getDescr("CURRENCY_CODE", currencyCode, userLocale));
			}

			// Save the update record to the ref data audit log
			mediatorServices.debug("Mediator: Creating audit record");

			TradePortalAuditor.createRefDataAudit(mediatorServices,
												  fxRate,
												  String.valueOf(fxRateOid),
												  updateText.toString(),
												  inputDoc.getAttribute("/FXRate/owner_org_oid"), //AAlubala Rel7000 CR610 Changed name to owner
												  updateMode,
												  inputDoc.getAttribute("/User/userOid"));

			mediatorServices.debug("Mediator: Finished with audit record");

			// Finally issue a success message based on update type.
			if (updateMode.equals(this.DELETE_MODE))
			{
			   mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
															 TradePortalConstants.DELETE_SUCCESSFUL,
															 updateText.toString());
			}
			else if (updateMode.equals(this.INSERT_MODE))
			{
			   mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
															 TradePortalConstants.INSERT_SUCCESSFUL,
															 updateText.toString());
			}
			else
			{
			   mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
															 TradePortalConstants.UPDATE_SUCCESSFUL,
															 updateText.toString());
			}
		 }
	  }

	  return outputDoc;
   }
}