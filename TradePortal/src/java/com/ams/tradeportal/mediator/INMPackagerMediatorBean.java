package com.ams.tradeportal.mediator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.List;

import com.ams.tradeportal.common.TradePortalConstants;
import com.ams.tradeportal.common.TransactionType;
import com.ams.tradeportal.mediator.util.InvoicePackagerUtility;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.frame.MediatorBean;
import com.amsinc.ecsg.frame.MediatorServices;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.StringFunction;

/**
 * INMPackagerMediatorBean will pick up each 'INM' (Invoice Master) on the Outgoing Queue and will create a 'INV' entry for every
 * invoice associated with the referenced activity
 * 
 * Copyright ? 2001 American Management Systems, Incorporated All rights reserved
 */

public class INMPackagerMediatorBean extends MediatorBean {
	private static final Logger LOG = LoggerFactory.getLogger(INMPackagerMediatorBean.class);

	/*
	 * This mediator is now used to package INM Message for Invoices in portal. Take the transaction_oid as input, go to
	 * corresponding Invoice and create a message in outgoing queue.
	 */
	public DocumentHandler execute(DocumentHandler inputDoc, DocumentHandler outputDoc, MediatorServices mediatorServices)
			throws RemoteException, AmsException {
		try {

			// SHR Rel8.1.1 Start
			try (Connection con = DatabaseQueryBean.connect(true);
					PreparedStatement insStmt = con
							.prepareStatement("INSERT INTO OUTGOING_QUEUE  (QUEUE_OID,DATE_CREATED, STATUS, MSG_TYPE,"
									+ " A_INSTRUMENT_OID, A_TRANSACTION_OID, MESSAGE_ID,PROCESS_PARAMETERS) VALUES" + " (?,?,'"
									+ TradePortalConstants.OUTGOING_STATUS_STARTED + "','" + TradePortalConstants.INVOICE
									+ "',?,?,?,?)")) {
				String senderID = TradePortalConstants.SENDER_ID;
				String transactionOid = inputDoc.getAttribute("/transaction_oid");
				String groupRef = senderID + "-" + transactionOid;

				String invSelectSQL = "select upload_invoice_oid,invoice_type,transaction_type_code from invoices_summary_data , transaction  where transaction_oid=a_transaction_oid and a_transaction_oid = ? ";

				Object tOIDOb = Integer.valueOf(transactionOid);
				DocumentHandler invDoc = DatabaseQueryBean.getXmlResultSet(invSelectSQL, false, new Object[]{tOIDOb});
				if (invDoc != null) {
					List<DocumentHandler> invVector = invDoc.getFragmentsList("/ResultSetRecord");
					int invItems = invVector.size();

					for (int iLoop = 0; iLoop < invItems; iLoop++) {
						invDoc = invVector.get(iLoop);
						if (invDoc != null) {
							String invoiceOid = invDoc.getAttribute("/UPLOAD_INVOICE_OID");
							// if invoice type is AMD then return UNA else if INT then UNI as universal message is associated with it
							String invoiceType = TradePortalConstants.REPLACEMENT.equals(invDoc.getAttribute("/INVOICE_TYPE"))
									&& !TransactionType.ISSUE.equals(invDoc.getAttribute("/TRANSACTION_TYPE_CODE"))
									? TradePortalConstants.UNV_AMD : TradePortalConstants.UNV_INT;

							if (StringFunction.isNotBlank(invoiceOid)) {
								int grpNum = iLoop + 1;
								String processParameter = "|InvoiceOID = " + invoiceOid + "|invoiceType =" + invoiceType
										+ "|GroupReferenceNumber =" + groupRef + "|GroupTotalNo =" + grpNum + "/" + invItems;
								InvoicePackagerUtility.create(inputDoc, mediatorServices, processParameter, insStmt, con);
							}
						}
					}

					insStmt.executeBatch();
					LOG.debug("added Invoices:  {}" , invItems);
				}
			}
		} catch (Exception e) {
			mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE, TradePortalConstants.TASK_NOT_SUCCESSFUL, "INM Message Packaging");
			LOG.error("Exception occured in INMPackagerMediatorBean: ", e );

		}

		return outputDoc;
	}

}
