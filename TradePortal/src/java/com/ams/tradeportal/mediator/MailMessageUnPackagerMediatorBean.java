package com.ams.tradeportal.mediator;
import java.rmi.RemoteException;

import javax.ejb.SessionContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.busobj.ClientBank;
import com.ams.tradeportal.busobj.CorporateOrganization;
import com.ams.tradeportal.busobj.DocumentImage;
import com.ams.tradeportal.busobj.Instrument;
import com.ams.tradeportal.busobj.MailMessage;
import com.ams.tradeportal.busobj.Transaction;
import com.ams.tradeportal.common.TradePortalConstants;
import com.ams.tradeportal.common.TransactionStatus;
import com.ams.tradeportal.common.TransactionType;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.ComponentList;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.frame.MediatorBean;
import com.amsinc.ecsg.frame.MediatorServices;
import com.amsinc.ecsg.util.DateTimeUtility;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.StringFunction;

/*
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class MailMessageUnPackagerMediatorBean extends MediatorBean {
private static final Logger LOG = LoggerFactory.getLogger(MailMessageUnPackagerMediatorBean.class);

	/*
	 * Manages MailMessage UnPackaging..
	 */

	public DocumentHandler execute(DocumentHandler unpackagerDoc, DocumentHandler outputDoc, MediatorServices mediatorServices)
	throws RemoteException, AmsException
	{

		SessionContext mContext = mediatorServices.mContext;
		boolean unpackage = true;
		String relatedInstrumentOid = null ;
		String assignedToCorpOrgOid = null ;
		String aClientBankOid = null ;
		String clientBankOid = null;
		String transactionType = null; //rkrishna CR 375-D ATP 08/22/2007 Added
		int instrumentCount = 0;
		int count = 0;
		String whereClause;
		String CoprNotTPSIntgrInd = TradePortalConstants.INDICATOR_NO;
		String bankInstrumentId = null;
		MailMessage mailMessage = (MailMessage)  mediatorServices.createServerEJB("MailMessage");



		try
		{

			String opBankOrgProponixID   =  unpackagerDoc.getAttribute("/Proponix/Header/OperationOrganizationID");
			if(opBankOrgProponixID == null || opBankOrgProponixID.equals(""))
			{
				mediatorServices.debug("No opBankOrg to update");
				mailMessage.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,TradePortalConstants.VALUE_MISSING,"OperationOrganizationID", "/Proponix/Header/OperationOrganizationID");
				mediatorServices.debug("Debug 1");
				unpackage=false;
			}
			else
			{
				whereClause  = "PROPONIX_ID = ? AND ACTIVATION_STATUS = ? ";
				count = DatabaseQueryBean.getCount("PROPONIX_ID","OPERATIONAL_BANK_ORG",whereClause, false, opBankOrgProponixID, "ACTIVE");
				if(count==1)
				{
					unpackage = true;
					mediatorServices.debug("Debug 2");
				}
				if(count==0)
				{
					mailMessage.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE ,TradePortalConstants.NO_MATCHING_VALUE_FOUND, "OperationalBankOrg");
					unpackage = false;
					mediatorServices.debug("Debug 3");
				}
				if(count>1)
				{
					mailMessage.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,TradePortalConstants.MORE_THAN_ONE_VALUE_FOUND, "OperationalBankOrg");
					unpackage = false;
					mediatorServices.debug("Debug 4");
				}
			}

			String clientBankOTLID = unpackagerDoc.getAttribute("/Proponix/SubHeader/ClientBank/OTLID");
			if(clientBankOTLID==null || clientBankOTLID.equals(""))
			{
				mediatorServices.debug("No clientBank to update");
				mailMessage.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,TradePortalConstants.VALUE_MISSING,"ClientBank OTLID", "/Proponix/SubHeader/ClientBank/OTLID");
				mediatorServices.debug("Debug 5");
				unpackage = false;
			}
			else
			{
				whereClause  = "OTL_ID = ? AND ACTIVATION_STATUS = ? ";
				count = DatabaseQueryBean.getCount("OTL_ID","CLIENT_BANK",whereClause, false, clientBankOTLID, "ACTIVE");
				if(count==1)
				{
					ClientBank clientBank = (ClientBank)mediatorServices.createServerEJB("ClientBank");
					clientBank.find("OTL_id",clientBankOTLID);
					clientBankOid = clientBank.getAttribute("organization_oid");
					unpackage = true;
					mediatorServices.debug("Debug 51");
				}
				if(count==0)
				{
					mailMessage.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE ,TradePortalConstants.NO_MATCHING_VALUE_FOUND, "ClientBank");
					unpackage = false;
					mediatorServices.debug("Debug 6");
				}
				if(count>1)
				{
					mailMessage.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,TradePortalConstants.MORE_THAN_ONE_VALUE_FOUND, "ClientBank");
					unpackage = false;
					mediatorServices.debug("Debug 7");
				}
			}

			String completeInstrumentID   = unpackagerDoc.getAttribute("/Proponix/SubHeader/InstrumentID");

			if(completeInstrumentID == null || completeInstrumentID.equals(""))
			{
				/*Free formatted MailMessages do not have a related Instrumunet */
				mediatorServices.debug("Debug 8: This is a free formatted document with no related instrument");
				String corpOrg_Proponix_cust_id = unpackagerDoc.getAttribute("/Proponix/SubHeader/CustomerID");
				String sqlStatement = " SELECT ORGANIZATION_OID FROM  CORPORATE_ORG WHERE " +
				" PROPONIX_CUSTOMER_ID = ? AND ACTIVATION_STATUS = ?";


				DocumentHandler resultXML = DatabaseQueryBean.getXmlResultSet(sqlStatement ,false, corpOrg_Proponix_cust_id,  "ACTIVE");
				if(resultXML == null)
				{
					mediatorServices.debug("No customerID ");
					assignedToCorpOrgOid = "";
				}
				else
				{
					int rid = 0;
					assignedToCorpOrgOid = resultXML.getAttribute("/ResultSetRecord(" + rid + ")/ORGANIZATION_OID");
				}
				relatedInstrumentOid = "";

			}
			else
			{
				whereClause  = "COMPLETE_INSTRUMENT_ID = ? ";
				instrumentCount = DatabaseQueryBean.getCount("INSTRUMENT_OID","INSTRUMENT",whereClause, false, new Object[]{completeInstrumentID});
				mediatorServices.debug("Debug 81: Instrument count =" + instrumentCount);

				String corpOrg_Proponix_cust_id = unpackagerDoc.getAttribute("/Proponix/SubHeader/CustomerID");

				if(instrumentCount ==0)
				{
					unpackage = true;
					mediatorServices.debug("Debug 82: Instrument count is zero" );
					/* Free formatted messages can contain an Instrument not existing in the TradePortal */
					/* IR # GGUC010935741 -- ivalavala */
					relatedInstrumentOid = "";
					String sqlStatement = " SELECT ORGANIZATION_OID FROM CORPORATE_ORG WHERE " +
					" PROPONIX_CUSTOMER_ID = ? AND ACTIVATION_STATUS = ? ";

					DocumentHandler resultXML = DatabaseQueryBean.getXmlResultSet(sqlStatement, false, corpOrg_Proponix_cust_id, "ACTIVE");
					if(resultXML == null)
					{
						unpackage = false;
						mediatorServices.debug("No CorporateOrg from SQL");
						mailMessage.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE ,TradePortalConstants.NO_MATCHING_VALUE_FOUND,"RelatedInstrument");
					}
					else
					{
						int rid = 0;
						assignedToCorpOrgOid = resultXML.getAttribute("/ResultSetRecord(" + rid + ")/ORGANIZATION_OID");
					}


				}
				if(instrumentCount > 0)
				{
					if(corpOrg_Proponix_cust_id==null || corpOrg_Proponix_cust_id.equals(""))
					{
						mediatorServices.debug("No corpOrg to update");
						mediatorServices.debug("Debug 9");
						//  mailMessage.getErrorManager().issueError("Unpackaging Error" , "No  value found at " + "/Proponix/SubHeader/CustomerID");
						mailMessage.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE , TradePortalConstants.VALUE_MISSING,"CustomerID", "/Proponix/SubHeader/CustomerID");

						unpackage = false;
					}
					else
					{
						whereClause  = "PROPONIX_CUSTOMER_ID = ? AND ACTIVATION_STATUS = ? ";
						count = DatabaseQueryBean.getCount("PROPONIX_CUSTOMER_ID","CORPORATE_ORG",whereClause, false, new Object[]{corpOrg_Proponix_cust_id, "ACTIVE"});
						if(count==0)
						{
							mailMessage.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE , TradePortalConstants.NO_MATCHING_VALUE_FOUND,"CustomerID(Corporate_Org)");
							unpackage = false;
							mediatorServices.debug("Debug 10");
						}
						if(count==1)
						{
							unpackage = true;
							String sqlStatement = " SELECT INSTRUMENT_OID, INSTRUMENT.A_CLIENT_BANK_OID , BANK_INSTRUMENT_ID, ORGANIZATION_OID,  CUST_IS_NOT_INTG_TPS " +
							" FROM INSTRUMENT, CORPORATE_ORG WHERE " +
							"  A_CORP_ORG_OID = ORGANIZATION_OID AND COMPLETE_INSTRUMENT_ID = ? "+
							" AND PROPONIX_CUSTOMER_ID = ? AND ACTIVATION_STATUS = ? ";


							DocumentHandler resultXML = DatabaseQueryBean.getXmlResultSet(sqlStatement, false, new Object[]{completeInstrumentID,corpOrg_Proponix_cust_id,"ACTIVE"});
							if(resultXML == null)
							{
								unpackage = false;
								mediatorServices.debug("No relatedInstrumentID from SQL");
								mailMessage.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE ,TradePortalConstants.NO_MATCHING_VALUE_FOUND,"RelatedInstrument");


							}
							else
							{
								int rid = 0;
								relatedInstrumentOid    = resultXML.getAttribute("/ResultSetRecord(" + rid + ")/INSTRUMENT_OID");
								assignedToCorpOrgOid = resultXML.getAttribute("/ResultSetRecord(" + rid + ")/ORGANIZATION_OID");
								aClientBankOid        = resultXML.getAttribute("/ResultSetRecord(" + rid + ")/A_CLIENT_BANK_OID");
								CoprNotTPSIntgrInd        = resultXML.getAttribute("/ResultSetRecord(" + rid + ")/CUST_IS_NOT_INTG_TPS");
								bankInstrumentId        = resultXML.getAttribute("/ResultSetRecord(" + rid + ")/BANK_INSTRUMENT_ID");
								if( relatedInstrumentOid ==null ||  relatedInstrumentOid.equals(""))
								{
									unpackage = false;
									mediatorServices.debug("No relatedInstrumentID to update");
									mediatorServices.debug("Debug 1");

									mailMessage.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE ,TradePortalConstants.NO_MATCHING_VALUE_FOUND,"RelatedInstrument");

								}
								if(assignedToCorpOrgOid == null || assignedToCorpOrgOid.equals(""))
								{
									unpackage = false;
									mediatorServices.debug("No assignedToCorpOrgOid to update");

									mailMessage.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE ,TradePortalConstants.NO_MATCHING_VALUE_FOUND,"RelatedInstrument");
								}
							}

							//rkrishna CR 375-D ATP 08/22/2007 Commented below line
							//String transactionType = null;
							String transactionSeq = null;
							transactionType = unpackagerDoc.getAttribute("/Proponix/SubHeader/TransactionTypeCode");
							transactionSeq  = unpackagerDoc.getAttribute("/Proponix/SubHeader/SequenceNumber");
							if(unpackage == true)
							{
								if(clientBankOid!=null && clientBankOid.equals(aClientBankOid))
									unpackage = true;
								else
								{
									mailMessage.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE ,TradePortalConstants.VALUE_DOESNOT_MATCH,"ClientBank","Instrument's ClientBank");
									unpackage = false;
								}
							}

						}//end of if count==1
					}//end of else
				}//end of instrument count > 0
			}//end of else

			/*If processedByBank = Y, do not create a mailMessage, just update the
			    Transaction Status  to PROCESSED_BY_BANK
                            Krishna IR-SEUI020540356 06/25/2008
			    if processedByBank = C, do not create a mailMessage, just update the
			    Transaction Status  to CANCELLED_BY_BANK

			 */
			String transaction_oid ="";
			String processedByBank = unpackagerDoc.getAttribute("/Proponix/Body/ProcessedByBank");
			//Krishna IR-SEUI020540356 06/25/2008 Added OR condition for processedByBank = C
			if((processedByBank.equals(TradePortalConstants.TRANSACTION_IS_PROCESSED_BY_BANK) || processedByBank.equals(TradePortalConstants.INDICATOR_CANCEL))  && instrumentCount > 0)
			{
				if (!StringFunction.isBlank(relatedInstrumentOid))
				{
					Instrument instrument   = (Instrument) mediatorServices.createServerEJB("Instrument", Long.parseLong(relatedInstrumentOid));
					transaction_oid =  getRelatedTransaction(unpackagerDoc,instrument,mediatorServices.mContext,mediatorServices);
				}
				if(transaction_oid == null) transaction_oid = "";
				if(transaction_oid == null || transaction_oid.equals("")) 
				{
					// CR-818 if it is settlement MAIL message, then delete the pending settlement instruction request/response related to same work item.
					if ( StringFunction.isNotBlank(unpackagerDoc.getAttribute("/Proponix/Body/WorkItemNumber")) &&  
							!TradePortalConstants.INDICATOR_NO.equals(unpackagerDoc.getAttribute("/Proponix/Body/SettlementInstructionFlag")) )
					{
					 if ( TransactionType.LIQUIDATE.equals(transactionType) || TransactionType.PAYMENT.equals(transactionType) ) {
						 /**
						     * below code is used to delete any un-authorized Settlement Instruction Request and response for customer when PAY or LIQ transaction
						     * has been Rejected or Processed by Bank. Instrument Id and Work item number is required to delete respective settlement request and Response.
						   */
						String workItemNumber = unpackagerDoc.getAttribute("/Proponix/Body/WorkItemNumber");
						String settlementRequestSql = " UPDATE mail_message SET message_status = ? WHERE work_item_number = ? AND a_related_instrument_oid = ? " +
			                        " AND a_assigned_to_corp_org_oid = ? AND settlement_instr_flag != ?  AND (  response_transaction_oid is null OR " +
			                        " Exists (SELECT 1 FROM transaction WHERE transaction_oid = response_transaction_oid AND transaction_status not in (?,?,?,?) )  ) ";
			    	     DatabaseQueryBean.executeUpdate(settlementRequestSql, true, new Object[] { TradePortalConstants.REC_DELETED, workItemNumber, relatedInstrumentOid,
			    	    		 assignedToCorpOrgOid, "N", TransactionStatus.AUTHORIZED, TransactionStatus.PROCESSED_BY_BANK,
			      					TransactionStatus.REJECTED_BY_BANK, TransactionStatus.CANCELLED_BY_BANK });
			    			  
			    		 String settlementResponseSql = " UPDATE transaction T1 SET T1.transaction_status = ? WHERE T1.transaction_type_code = ? AND T1.p_instrument_oid = ? " + 
			    			                  " AND T1.transaction_status not in (?,?,?,?) And Exists (SELECT 1 FROM terms T2 WHERE T1.c_cust_enter_terms_oid = T2.terms_oid " +
			                                  " AND T2.work_item_number = ?) ";
			      	     DatabaseQueryBean.executeUpdate(settlementResponseSql, true, new Object[] { TransactionStatus.DELETED, 
			      					  TransactionType.SIR, relatedInstrumentOid, TransactionStatus.AUTHORIZED, TransactionStatus.PROCESSED_BY_BANK,
			      					TransactionStatus.REJECTED_BY_BANK, TransactionStatus.CANCELLED_BY_BANK, workItemNumber }); 
					  }
					} 
					else
					{
					  mailMessage.getErrorManager().issueError("MailMessage unpackaging error", "No transaction found to update the status to PROCESSED_BY_BANK");
					}
				}
				else
				{
					mediatorServices.getCSDB().setCSDBValue(TradePortalConstants.TRAN_ACTION, TradePortalConstants.TRAN_ACTION_BANK); 
					Transaction transaction = (Transaction)  mediatorServices.createServerEJB("Transaction", Long.parseLong(transaction_oid));
					transaction.setAttribute("sequence_num", unpackagerDoc.getAttribute("/Proponix/SubHeader/SequenceNumber"));
					updateTransactionStatus(unpackagerDoc,transaction,mailMessage);
				}
			}

			else
			{
				mediatorServices.debug("No need to update any Transaction");
				if(unpackage == true)
				{
					mediatorServices.debug("This is unpackagerDoc :" + unpackagerDoc.toString());
					mediatorServices.debug("This is relatedInstrument :" + relatedInstrumentOid);
					mediatorServices.debug("This is corp_org :" + assignedToCorpOrgOid);
					unpackageMailMessageAttributes(mailMessage,unpackagerDoc, relatedInstrumentOid, assignedToCorpOrgOid, mediatorServices,outputDoc);

					String settlementInstrFlag = null;
					if(!TradePortalConstants.INDICATOR_NO.equals(mailMessage.getAttribute("settlement_instr_flag"))){
						settlementInstrFlag = TradePortalConstants.EMAIL_TRIGGER_SETTLEMENT;
					}
					// Get the discrepany flag value
					String discrepancyFlag = mailMessage.getAttribute("discrepancy_flag");
					//rkrishna CR 375-D ATP 08/22/2007 Begin
					String atpNoticeFlag = mailMessage.getAttribute("atp_notice_flag");
					//rkrishna CR 375-D ATP 08/22/2007 End
					String discrepancyAmount = mailMessage.getAttribute("discrepancy_amount");
					String discrepancyCurrency = mailMessage.getAttribute("discrepancy_currency_code");
					// DK IR-SAUM053159077 Rel8.0 06/18/2012 Begin
					String subject 	 = mailMessage.getAttribute("message_subject");
					String fundingFlag = null;
					if ((subject).startsWith("Funding Notice:")){
						fundingFlag = TradePortalConstants.EMAIL_TRIGGER_FUNDING;
					}
					// DK IR-SAUM053159077 Rel8.0 06/18/2012 End
					if ( TradePortalConstants.INDICATOR_YES.equals(CoprNotTPSIntgrInd) && StringFunction.isNotBlank(bankInstrumentId)) {
						mailMessage.setAttribute("bank_instrument_id", bankInstrumentId);
					}
					mailMessage.save();


					//rkrishna CR 375-D ATP 08/22/2007 Begin
					// Call method which determines if an email should be sent upon receipt of this message
					//triggerEmailForMessage(discrepancyFlag, assignedToCorpOrgOid, relatedInstrumentOid, discrepancyCurrency, discrepancyAmount, unpackagerDoc, mediatorServices);
					//Above lines commented to pass accomodate  discrepancyFlag & atpNoticeFlag
					if( transactionType!=null && transactionType.equals(TransactionType.APPROVAL_TO_PAY_RESPONSE) )
						triggerEmailForMessage(atpNoticeFlag, assignedToCorpOrgOid, relatedInstrumentOid, discrepancyCurrency, discrepancyAmount, unpackagerDoc, mediatorServices);
					// DK IR-SAUM053159077 Rel8.0 06/18/2012 Begin
					else if((TradePortalConstants.EMAIL_TRIGGER_FUNDING).equals(fundingFlag))
						triggerEmailForMessage(fundingFlag, assignedToCorpOrgOid, relatedInstrumentOid, discrepancyCurrency, discrepancyAmount, unpackagerDoc, mediatorServices);
					// DK IR-SAUM053159077 Rel8.0 06/18/2012 End
					else if((TradePortalConstants.EMAIL_TRIGGER_SETTLEMENT).equals(settlementInstrFlag))
						triggerEmailForMessage(settlementInstrFlag, assignedToCorpOrgOid, relatedInstrumentOid, discrepancyCurrency, discrepancyAmount, unpackagerDoc, mediatorServices);
					else
						triggerEmailForMessage(discrepancyFlag, assignedToCorpOrgOid, relatedInstrumentOid, discrepancyCurrency, discrepancyAmount, unpackagerDoc, mediatorServices);
					//rkrishna CR 375-D ATP 08/22/2007 End
				}
				else
				{
					mailMessage.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,TradePortalConstants.UNPACKAGING_MISMATCH_DATA);
				}
			} //end of Else  (PROCESSED_BY_BANK != Y)

			//Pass the parameters to be updated in the Incoming_queue to the Inbound agent
			outputDoc.setAttribute("/Param/TransactionOid",transaction_oid);
			outputDoc.setAttribute("/Param/InstrumentOid",relatedInstrumentOid);


		} //end of try

		catch (RemoteException e) {
			mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,TradePortalConstants.TASK_NOT_SUCCESSFUL,"Message Unpackaging");

			e.printStackTrace();

		}
		catch (AmsException e) {
			mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,TradePortalConstants.TASK_NOT_SUCCESSFUL,"Message Unpackaging");
			e.printStackTrace();


		}
		catch (Exception e) {
			mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,TradePortalConstants.TASK_NOT_SUCCESSFUL,"Message Unpackaging");
			e.printStackTrace();

		}

		return outputDoc;
	}


	/**
	 * This method takes the MailMessage object, instrument_oid, corp_org_oid
	 * and populates the database with the value from the unpackagerDoc
	 * @param mailMessage, MailMessage object whose values are being packaged
	 * @param unpackagerDoc, DocumentHandler object with the XML
	 * @param String instrument_oid
	 * @param String corp_org_oid
	 * @param MediatorServices mediatorServices
	 * author iv
	 */


	public void unpackageMailMessageAttributes( MailMessage mailMessage , DocumentHandler unpackagerDoc, String relatedInstrumentOid, String assignedToCorpOrgOid, MediatorServices mediatorServices,DocumentHandler outputDoc)
	throws RemoteException, AmsException

	{ /*
       Updating data for BO:    MailMessage
	 */
		//rkrishna CR 375-D ATP 08/22/2007
		//As atp_notice_flag is added,noOfvalues is chaged to 15 from 14
		//int numberOfValues = 14;
		//CR-419 Krishna numberOfValues is initialized to 18
		// int numberOfValues = 15;
		//Vshah - PPX-220A - Rel8.0 - numberOfValues is initialized to 19
		//int numberOfValues = 18;
		//SSikhakolli - CR-944 Rel-9.1 - 08/08/2014 - 'numberOfValues' is initialized to 20
		int numberOfValues = 20;
		mailMessage.newObject();

		/*clarify about the other three in the xml presentationStatus, ProcessedbyBank */

		// rkrishna CR 375-D ATP 08/22/2007 added "atp_notice_flag"
		// rkrishna CR 375-D ATP 08/22/2007 added "atp_notice_flag"
		//CR-419 Krishna added "presentation_date","discrepancy_text","po_invoice_discrepancy_text"
		//Vshah - PPX-220A - Rel8.0 - Added "presentation_status"
		//SSikhakolli - CR-944 Rel-9.1 - 07/29/2014 - Added 'REFERENCE_NUMBER'
		String [] mailMessageAttributes = {"message_subject","message_text","discrepancy_flag","atp_notice_flag",
				"discrepancy_amount","discrepancy_currency_code", "work_item_number",
				"presentation_date","discrepancy_text","po_invoice_discrepancy_text","presentation_status",
				"related_instrument_oid","assigned_to_corp_org_oid","sequence_number",
				"message_status","unread_flag","last_update_date",
				"message_source_type","is_reply", "reference_number"};
		//rkrishna CR 375-D ATP 08/22/2007 added extra "DiscrepancyFlag"
		//CR-419 Krishna added "PresentationDate","ExamDiscrepancyText","PODiscrepancyText"
		//Vshah - PPX-220A - Rel8.0 - Added "PresentationStatus"
		String [] documentValues   = {"MessageSubject","MessageText","DiscrepancyFlag","DiscrepancyFlag",
				"DiscrepancyAmount","DiscrepancyCurrency","WorkItemNumber","PresentationDate","ExamDiscrepancyText","PODiscrepancyText","PresentationStatus"};
		//rkrishna CR 375-D ATP 08/22/2007 added "N" at 4th place
		//CR-419 Krishna Array Resized to 18
		//Vshah - PPX-220A - Rel8.0 - Array Resized to 19
		//SSikhakolli - CR-944 Rel-9.1 - 07/29/2014 - Array Resized to 20
		String [] mailMessageValues = {"","","N","N","","","","","","","","","","","","","","","",""} ;
		String mailMessageOid = mailMessage.getAttribute("message_oid");
		int numberOfDocumentImages = 0;
		DocumentImage documentImage = null;

		int mailAttrCounter = 0;
		//SSikhakolli - CR-944 Rel-9.1 - 07/29/2014 - condition has been modified to -9 from -8
		for( mailAttrCounter=0; mailAttrCounter<numberOfValues-9; mailAttrCounter++)
		{
			mailMessageValues[mailAttrCounter] = unpackagerDoc.getAttribute("/Proponix/Body/"+documentValues[mailAttrCounter]);
		}

		//If Empty set DiscrepencyFlag to "N"
		if(mailMessageValues[2]==null || mailMessageValues[2].equals(""))
			mailMessageValues[2] = "N";
		String timeStamp   = DateTimeUtility.getGMTDateTime(false);

		mailMessageValues[mailAttrCounter] = relatedInstrumentOid;
		mailMessageValues[mailAttrCounter+1] = assignedToCorpOrgOid;
		mailMessageValues[mailAttrCounter+2] = unpackagerDoc.getAttribute("/Proponix/SubHeader/SequenceNumber");
		mailMessageValues[mailAttrCounter+3] = "REC";
		mailMessageValues[mailAttrCounter+4] = "Y";
		mailMessageValues[mailAttrCounter+5] = timeStamp;
		mailMessageValues[mailAttrCounter+6] = "BANK";
		mailMessageValues[mailAttrCounter+7] = "N";
		//SSikhakolli - CR-944 Rel-9.1 - 07/29/2014 - Added 'REFERENCE_NUMBER'
		mailMessageValues[mailAttrCounter+8] = unpackagerDoc.getAttribute("/Proponix/Body/ReferenceNumber");

		mediatorServices.debug("Step before setAttributes");
		mediatorServices.debug("Attribute " + mailMessageAttributes);
		mediatorServices.debug("Values " +mailMessageValues);
		mailMessage.setAttributes(mailMessageAttributes, mailMessageValues);

		//Leelavathi - Rel800 CR618 - 20th Dec 2011 - Start
		String paymentDate = unpackagerDoc.getAttribute("/Proponix/Body/PaymentDate");
		mailMessage.setAttribute("payment_date",!StringFunction.isBlank(paymentDate)?paymentDate.substring(4,6)+ "/" + paymentDate.substring(6,8)+ "/" +paymentDate.substring(0,4):"");
		//Leelavathi - Rel800 CR618 - 20th Dec 2011 - End
		
		//CR 913 start 
		String fundingDate = unpackagerDoc.getAttribute("/Proponix/Body/FundingDate");
		mailMessage.setAttribute("funding_date",StringFunction.isNotBlank(fundingDate)?fundingDate.substring(4,6)+ "/" + fundingDate.substring(6,8)+ "/" +fundingDate.substring(0,4):"");
		mailMessage.setAttribute("funding_currency",unpackagerDoc.getAttribute("/Proponix/Body/FundingCurrency"));
		mailMessage.setAttribute("funding_amount",unpackagerDoc.getAttribute("/Proponix/Body/FundingAmount"));
		//CR 913 end
		// Nar CR-818 Rel9400 07/21/2015
		if ( StringFunction.isNotBlank(unpackagerDoc.getAttribute("/Proponix/Body/SettlementInstructionFlag")) ) {
			mailMessage.setAttribute( "settlement_instr_flag",unpackagerDoc.getAttribute("/Proponix/Body/SettlementInstructionFlag") );
		}
		
        // W Zhu 1/9/09 SLUJ010768298 handle mail messages that come in before the instrument.
        if (relatedInstrumentOid == null || relatedInstrumentOid.equals("")) {
            mailMessage.setAttribute("complete_instrument_id", unpackagerDoc.getAttribute("/Proponix/SubHeader/InstrumentID"));
        }

		mediatorServices.debug("Done setting Attributes");
		mediatorServices.debug("Before saving MailMessage");

	    numberOfDocumentImages = getDocumentImageCount(unpackagerDoc);
		if (numberOfDocumentImages > 0)
			//Need to unpackage DocumentImages
		{
			for(int dICount = 0; dICount < numberOfDocumentImages; dICount++)
			{
				documentImage = (DocumentImage)  mediatorServices.createServerEJB("DocumentImage");
				mediatorServices.debug("Before getting documentImage");
				documentImage.newObject();
				unpackageDocumentImageAttributes(documentImage,dICount,unpackagerDoc,mailMessageOid, mailMessage);
				documentImage.save();
				mediatorServices.debug("Done saving DocumentImage " + dICount);
			}
		}

		//Pass the mailMessageOid to be updated into the Incoming_queue
		outputDoc.setAttribute("/Param/MailMessageOid",mailMessageOid);
		
	}

	/**
	 * This method takes the DocumentImage object
	 * and populates the database with values from DocumentImage section
	 * of the unpackagerDoc
	 * @param documentImage, DocumentImage object
	 * @param id, int value representing the documentImage
	 * @param unpackagerDoc, DocumentHandler object with the XML
	 * @param mailMessageOid, String value representing the related MailMessage.
	 * @param mediatorServices, MediatorServices
	 * author iv
	 */
	public void unpackageDocumentImageAttributes(DocumentImage documentImage,
			int id, DocumentHandler unpackagerDoc, String mailMessageOid,
			MailMessage mailMessage) throws RemoteException, AmsException {
		if (unpackagerDoc.getAttribute("/Proponix/Body/DocumentImage/FullDocumentImage(" + id + ")/FormType") == null
				|| unpackagerDoc.getAttribute("/Proponix/Body/DocumentImage/FullDocumentImage(" + id + ")/FormType").equals("")) {
			mailMessage.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_MIDDLEWARE,
					TradePortalConstants.VALUE_MISSING,
					"FormType",
					"/Proponix/Body/DocumentImage/FullDocumentImage(" + id + ")/FormType");
			return;
		}

		// [BEGIN] CR-186 - jkok - OTL will send a 'U' but the TradePortal is
		// expecting 'USER' so we do the mapping here
		// [BEGIN] IR-YYUH032255414 - jkok
		if (unpackagerDoc.getAttribute("/Proponix/Body/DocumentImage/FullDocumentImage(" + id + ")/FormType")
				.equals(TradePortalConstants.DOC_IMG_FORM_TYPE_OTL_ATTACHED.substring(0,1).toUpperCase())) {
			documentImage.setAttribute("form_type",
					TradePortalConstants.DOC_IMG_FORM_TYPE_OTL_ATTACHED);
			// [END] IR-YYUH032255414 - jkok
		}
		else {
			documentImage.setAttribute("form_type", unpackagerDoc.getAttribute("/Proponix/Body/DocumentImage/FullDocumentImage(" + id + ")/FormType"));
		}
	
		documentImage.setAttribute("image_id", unpackagerDoc.getAttribute("/Proponix/Body/DocumentImage/FullDocumentImage(" + id + ")/ImageID"));
		documentImage.setAttribute("mail_message_oid", mailMessageOid);
		// [BEGIN] CR-186 - jkok - new values for attachments
		documentImage.setAttribute("doc_name", unpackagerDoc.getAttribute("/Proponix/Body/DocumentImage/FullDocumentImage(" + id + ")/Text"));
		documentImage.setAttribute("image_format", unpackagerDoc.getAttribute("/Proponix/Body/DocumentImage/FullDocumentImage(" + id + ")/ImageFormat"));
		documentImage.setAttribute("image_bytes", unpackagerDoc.getAttribute("/Proponix/Body/DocumentImage/FullDocumentImage(" + id + ")/ImageBytes"));
		documentImage.setAttribute("num_pages", unpackagerDoc.getAttribute("/Proponix/Body/DocumentImage/FullDocumentImage(" + id + ")/NumPages"));
		//cquinton 11/11/2011 Rel 7.1 cnul111048855 unpackage Hash attribute
		String hash = unpackagerDoc.getAttribute("/Proponix/Body/DocumentImage/FullDocumentImage(" + id + ")/Hash");
		if ( hash != null && hash.length()>0 ) {
		    documentImage.setAttribute("hash", hash);
		}
		
	}


	public String  getRelatedTransaction(DocumentHandler unpackagerDoc, Instrument instrument, SessionContext mContext, MediatorServices mediatorServices)
	throws RemoteException, AmsException
	{
		int transactionCount = 0;
		ComponentList componentList   =   (ComponentList)        mediatorServices.createServerEJB("ComponentList");

		componentList = (ComponentList)instrument.getComponentHandle("TransactionList");
		transactionCount = componentList.getObjectCount();
		Transaction transaction = null;
		String transactionPath = "/Proponix/SubHeader";

		mediatorServices.debug("Number of transactions found for this instrument : " + transactionCount);
		String return_message_id_XML             = unpackagerDoc.getAttribute(transactionPath+"/ReturnMessageID");

		//If return_message_id is empty, the transaction doesn't exist in TP
		mediatorServices.debug("This is the returnMessageID from XML : " + return_message_id_XML);
		if(return_message_id_XML == null || return_message_id_XML.equals(""))
		{
			return null;
		}

		//If there is no transaction is retrieved, then create this transaction BO.
		if (transactionCount == 0)
		{
			return null;
		}

		//If there is one or more transactions retrieved, then ...
		if (transactionCount >= 1)
		{
			String transaction_type_code_XML    = unpackagerDoc.getAttribute(transactionPath+"/TransactionTypeCode");
			String return_message_id_BO         = null;
			String transaction_oid			     = null;
			if(transaction_type_code_XML==null)
			{
				mediatorServices.getErrorManager().issueError("Unpackaging Error","TransactionTypeCode not found at " + transactionPath+"/TransactionTypeCode");
			}


			mediatorServices.debug("Need to find a transaction with TypeCode and message_id = " + transaction_type_code_XML +  return_message_id_XML);

			for (int transactionId = 0; transactionId < transactionCount; transactionId++)
			{
				componentList.scrollToObjectByIndex(transactionId);
				transaction = (Transaction)componentList.getBusinessObject();

				return_message_id_BO             = transaction.getAttribute("return_message_id");
				transaction_oid			        = transaction.getAttribute("transaction_oid");

				mediatorServices.debug("Present returnMessageId :" + return_message_id_BO);
				mediatorServices.debug("Present oid :" + transaction_oid  );

				if (return_message_id_XML.equals(return_message_id_BO))
				{
					mediatorServices.debug("Found a matching transaction with id : " + transaction_oid );
					return transaction_oid;
				}

			}
			return null;
		}
		return null;
	}

	/**
	 * This method takes the transaction object
	 * and populates the database with values from Transaction section
	 * of the unpackagerDoc
	 * @param transaction, Transaction object
	 * @param unpackagerDoc, DocumentHandler object with the XML
	 * @param mediatorServices, MediatorServices
	 * author iv
	 */


	public void updateTransactionStatus( DocumentHandler unpackagerDoc, Transaction transaction, MailMessage mailMessage)
	throws RemoteException, AmsException

	{

		String transactionPath = "/Proponix/SubHeader";
		//mediatorServices.debug("This is the transactionPath :"+ transactionPath);
		if(unpackagerDoc.getAttribute(transactionPath+"/TransactionTypeCode") == null ||
				unpackagerDoc.getAttribute(transactionPath+"/TransactionTypeCode").equals(""))
		{
			mailMessage.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE, TradePortalConstants.VALUE_MISSING,"TransactionTypeCode",transactionPath+"/TransactionTypeCode");
			return;
		}
	
		String processedByBank = unpackagerDoc.getAttribute("/Proponix/Body/ProcessedByBank");
		if(StringFunction.isNotBlank(processedByBank))
		{
			//Depending on the type ProcessedByBank-flag save the transaction status
			if(processedByBank.equals(TradePortalConstants.TRANSACTION_IS_PROCESSED_BY_BANK) )
			{
				transaction.setAttribute("transaction_status",    TransactionStatus.PROCESSED_BY_BANK);
			}
			else if(processedByBank.equals(TradePortalConstants.INDICATOR_CANCEL) )
			{
				transaction.setAttribute("transaction_status",    TransactionStatus.CANCELLED_BY_BANK);
			}
		}
		transaction.save(false);
		//Krishna IR-SEUI020540356 06/25/2008 End
	}

	/**
	 * This method determines if the message has any associated document images
	 * @param unpackagerDoc, DocumentHandler
	 *
	 */
	public int getDocumentImageCount( DocumentHandler unpackagerDoc) throws RemoteException, AmsException
	{
		int numberOfDocumentImages = 0;
		String snumberOfDocumentImages = unpackagerDoc.getAttribute("/Proponix/Body/DocumentImage/NumberOfEntries");

		if(snumberOfDocumentImages != null && !(snumberOfDocumentImages.equals("")))
		{
			numberOfDocumentImages = Integer.parseInt(snumberOfDocumentImages);
		}
		return numberOfDocumentImages;
	}

	/**
	 * This method takes the corporate org and determines if
	 * if an email notification should be triggered for this message's
	 * corporate customer
	 *
	 * @param discrepancyFlag, Indicates if this message is a discrepancy
	 * @param assignedToCorpOrgOid, CorpOrg this message belongs to
	 * @param relatedInstrumentOid, Oid of the instrument this message
	 *                              is associated to
	 * @param discrepancyAmount, amount of the discrepancy message
	 * @param unpackagerDoc, DocumentHandler
	 * @param mediatorServices, MediatorServices
	 */
	public void triggerEmailForMessage( String discrepancyFlag, String assignedToCorpOrgOid, String relatedInstrumentOid, String discrepancyCurrency, String discrepancyAmount, DocumentHandler unpackagerDoc, MediatorServices mediatorServices)
	throws RemoteException, AmsException
	{
		String transactionOid = "";
		boolean triggerEmail = false;

		// Check if the corp org assigned to this message has specified a notification rule
		if (!StringFunction.isBlank(assignedToCorpOrgOid))
		{
			CorporateOrganization corpOrg = (CorporateOrganization)mediatorServices.createServerEJB("CorporateOrganization",Long.parseLong(assignedToCorpOrgOid));
			String notifRuleOid = corpOrg.fetchNotificationRule();//Rel9.5 Cr-927B Fetch notification rule for the corporate customer by looking into the 'Notification_Rule' table
			if (!StringFunction.isBlank(notifRuleOid))
			{
				String emailFrequency = corpOrg.getNotificationRuleAttribute("email_frequency");
				if (!emailFrequency.equals(TradePortalConstants.NOTIF_RULE_DAILY)
						&& !emailFrequency.equals(TradePortalConstants.NOTIF_RULE_NOEMAIL))
				{
					// Determine which notification rule email setting should be checked based on the
					// on the mail message type (discrepancy or standard mail message)
					String emailSetting = null;
					if (discrepancyFlag.equals(TradePortalConstants.INDICATOR_YES))
					{
						emailSetting = corpOrg.getNotificationRuleSettingOrFreq(notifRuleOid,"MESSAGES" ,"DISCR" , TradePortalConstants.NOTIF_RULE_EMAIL);
					}
					else if(TradePortalConstants.EMAIL_TRIGGER_SETTLEMENT.equals(discrepancyFlag))
					{
						emailSetting = corpOrg.getNotificationRuleSettingOrFreq(notifRuleOid,"MESSAGES" ,"SETTLEMENT" , TradePortalConstants.NOTIF_RULE_EMAIL);
					}
					else
					{
						emailSetting = corpOrg.getNotificationRuleSettingOrFreq(notifRuleOid,"MESSAGES" ,"MAIL" , TradePortalConstants.NOTIF_RULE_EMAIL);
					}

					// Trigger an email if emails are not sent daily; additionally check if the notification rule specifies
					// that an email should only be triggered when charges or documents exist
					if (TradePortalConstants.NOTIF_RULE_ALWAYS.equals(emailSetting))
					{
						triggerEmail = true;
					}
					else if (TradePortalConstants.NOTIF_RULE_CHRGS_DOCS_ONLY.equals(emailSetting))
					{
						if (getDocumentImageCount(unpackagerDoc) > 0)
						{
							triggerEmail = true;
						}
					}
				}
			}
		}

		if (triggerEmail)
		{
			// Get the message's related transaction
			if (!StringFunction.isBlank(relatedInstrumentOid))
			{
				Instrument instrument   = (Instrument) mediatorServices.createServerEJB("Instrument", Long.parseLong(relatedInstrumentOid));
				transactionOid =  getRelatedTransaction(unpackagerDoc,instrument,mediatorServices.mContext,mediatorServices);
			}

			// Determine which type of email should be created (mail message, discrepancy)
			String emailType = "";
			if (discrepancyFlag.equals(TradePortalConstants.INDICATOR_YES))
			{
				emailType = TradePortalConstants.EMAIL_TRIGGER_DISCREPANCY;
			}
			// DK IR-SAUM053159077 Rel8.0 06/18/2012 Begin
			else if((TradePortalConstants.EMAIL_TRIGGER_FUNDING).equals(discrepancyFlag)){
				emailType = TradePortalConstants.EMAIL_TRIGGER_FUNDING;
			}
			// DK IR-SAUM053159077 Rel8.0 06/18/2012 End
			else if((TradePortalConstants.EMAIL_TRIGGER_SETTLEMENT).equals(discrepancyFlag)){
				emailType = TradePortalConstants.EMAIL_TRIGGER_SETTLEMENT;
			}
			else
			{
				emailType = TradePortalConstants.EMAIL_TRIGGER_MAIL_MSG;
			}

			// Call method to create the corporate org's email
			CorporateOrganization corpOrg = (CorporateOrganization)mediatorServices.createServerEJB("CorporateOrganization",Long.parseLong(assignedToCorpOrgOid));
			corpOrg.createEmailMessage(relatedInstrumentOid, transactionOid, discrepancyCurrency, discrepancyAmount, emailType);
		}
	}

}

