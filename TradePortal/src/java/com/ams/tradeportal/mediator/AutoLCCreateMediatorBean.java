package com.ams.tradeportal.mediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.*;
import java.util.*;
import java.text.*;
import java.math.*;
//import java.lang.*;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;

import com.ams.tradeportal.busobj.*;
import com.ams.tradeportal.common.*;
import com.ams.tradeportal.common.cache.Cache;
import com.ams.tradeportal.common.cache.TPCacheManager;
import com.ams.tradeportal.busobj.util.*;

/*
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class AutoLCCreateMediatorBean extends MediatorBean {
private static final Logger LOG = LoggerFactory.getLogger(AutoLCCreateMediatorBean.class);
  // This SQL selects information about all the creation rules
  // for an org.

  // TLE - 07/29/07 - CR-375 - Add Begin   
  private String SELECT_SQL = "";
  // TLE - 07/29/07 - CR-375 - Add Begin 


  private static String ORDERBY_CLAUSE = "order by creation_date_time DESC";

  // This SQL selects unassigned po line items for an org.  The
  // remaining where clause criteria is added dynamically.  They are
  // ordered by ben_name and currency for processing logic.  They are
  // also ordered by upload date/time to provide some consistency
  // for transaction assignment.
  //
  // GGAYLE - DFUH031368817 - 04/03/2007 - - added po_text column to the query 
  private static String PO_SELECT_SQL = "select po_line_item_oid, ben_name, currency, amount, "
      + " po_num, item_num, last_ship_dt, po_text "
      + "from po_line_item "
      + "where a_assigned_to_trans_oid is NULL "
      + "and source_type = '"
      + TradePortalConstants.PO_SOURCE_UPLOAD
      + "' "
      + "and a_owner_org_oid = ";
  // GGAYLE - DFUH031368817 - 04/03/2007

  //TLE - 07/29/07 - CR-375 - Add Begin 
  String poUploadInstrType = null;
  //TLE - 07/29/07 - CR-375 - Add End 


  private static String PO_ORDERBY = "order by ben_name, currency, creation_date_time";

  // This SQL selects all the log sequence numbers
  // in reverse order for a given corporate org.
  private static String LOG_SELECT_SQL = "select DISTINCT(sequence_number) "
      + "from auto_lc_create_log " + "where a_corp_org_oid = ";

  private static String LOG_ORDERBY = " order by sequence_number DESC";

  // This SQL is used to delete log records for a given sequence.
  private static String LOG_DELETE_SQL = "delete from auto_lc_create_log "
      + "where sequence_number = ";

  // This indicates the maximum number of po line items that
  // can be assigned to a transaction.
  private static int MAX_ITEMS_ON_PO = 94;

  // This indicates the maximum number of logs to keep for
  // a corporate org.
  //Rel9.0 - IR#T36000015007 - Making the log size configurable
  //private static int MAX_LOGS_TO_KEEP = 2;
  private static int MAX_LOGS_TO_KEEP;
  static{
	  try{
		  MAX_LOGS_TO_KEEP= Integer.parseInt(ConfigSettingManager.getConfigSetting("PORTAL_AGENT", "AGENT", "MAX_PO_LOGS_TO_KEEP", "50"));
	  }catch(Exception ex) {
		  MAX_LOGS_TO_KEEP = 50;
		  LOG.warn("Exception while fetching MAX_PO_LOGS_TO_KEEP from ConfigSettingManager ::", ex); 
	  }
  }

  // These are instance variables that are set each and every time the
  // mediator is created.  They are transient because mediators are
  // intended to be stateless.

  private transient MediatorServices mediatorServices;

  private transient AutoLCLogger logger = null;

  private transient java.lang.String baseCurrency = null;

  private transient java.lang.String clientBankOid = null;

  private transient java.lang.String corporateOrgOid = null;

  private transient java.lang.String logSequence = null;

  private transient java.lang.String securityRights = null;

  private transient java.lang.String timeZone = null;

  private transient java.lang.String uploadSequenceNumber = null;

  private transient java.lang.String userLocale = null;

  private transient java.lang.String userName = null;

  private transient java.lang.String userOid = null;

  private transient long uploadDefinitionOid = 0;

  private transient java.lang.String uploadFileName = null;

  private transient java.lang.String subsidiaryAccessOrgOid = null;

  // Business methods
  public DocumentHandler execute(DocumentHandler inputDoc,
      DocumentHandler outputDoc, MediatorServices mediatorServices)
      throws RemoteException, AmsException {
    String poDataFileContents = null;
    String poGroupingOption = null;
    String poDataOption = null;
    String value = null;

    setMediatorServices(mediatorServices);


    // Pull values out of the input document and call various setter methods
    value = inputDoc.getAttribute("/User/userOid");
    setUserOid(value);

    value = inputDoc.getAttribute("../timeZone");
    setTimeZone(value);

    value = inputDoc.getAttribute("../locale");
    setUserLocale(value);

    value = inputDoc.getAttribute("../securityRights");
    setSecurityRights(value);

    value = inputDoc.getAttribute("../baseCurrencyCode");
    setBaseCurrency(value);

    value = inputDoc.getAttribute("/AutoLCCreateInfo/corporateOrgOid");
    setCorporateOrgOid(value);

    value = inputDoc.getAttribute("../clientBankOid");
    setClientBankOid(value);

    value = inputDoc.getAttribute("../poDataFilePath");
    setUploadFileName(value);

    value = inputDoc.getAttribute("../uploadSequenceNumber");
    setUploadSequenceNumber(value);

    value = inputDoc.getAttribute("../subsidiaryAccessOrgOid");
    if (value != null)
      setSubsidiaryAccessOrgOid(value);

    // If no upload sequence was given, it means we're grouping everything
    // (all upload sequences).  Therefore, the log sequence needs to be
    // generated.  Otherwise, we use the upload sequence of the log sequence
    if (uploadSequenceNumber == null) {
      long newSeq = ObjectIDCache.getInstance(
          TradePortalConstants.AUTOLC_SEQUENCE).generateObjectID();
      setLogSequence(String.valueOf(newSeq));
    } else {
      setLogSequence(uploadSequenceNumber);
    }

    // If we're coming from the PO Listview page, the button that was pressed will still
    // be set in the xml doc; otherwise (if it doesn't exist), we know that a PO upload
    // just occurred, in which case we should retrieve the PO data option and grouping
    // option (if one exists).
    value = inputDoc.getAttribute("/Update/ButtonPressed");
    if (value == null) {
      poDataOption = inputDoc
          .getAttribute("/AutoLCCreateInfo/poDataOption");
      // krishna added poDataOption!=null
      if (poDataOption!=null&&poDataOption.equals(TradePortalConstants.GROUP_PO_LINE_ITEMS)) {
        poGroupingOption = inputDoc.getAttribute("../poGroupingOption");
        // krishna added poGroupingOption!=null
        if (poGroupingOption!=null&&poGroupingOption
            .equals(TradePortalConstants.INCLUDE_ALL_PO_LINE_ITEMS)) {
          setUploadSequenceNumber("");
        }
      }

      setUploadDefinitionOid(inputDoc
          .getAttributeLong("../uploadDefinitionOid"));

      // Get the PO data file contents
      poDataFileContents = inputDoc.getAttribute("../poDataFileContents");

      // TLE - 07/29/07 - CR-375 - Add Begin 
      poUploadInstrType = inputDoc
          .getAttribute("../poUploadInstrOption");

      if (poUploadInstrType == null) {
		  // This happens when user chooses to upload but not creating any automatic instrument.
		  // The user just want to upload PO so that it will be available to adding manually from the instrument
		  poUploadInstrType = "";
	  }

      if (poUploadInstrType.equals(TradePortalConstants.AUTO_LC_PO_UPLOAD)) {
            SELECT_SQL = "select name, description, maximum_amount, pregenerated_sql, "
                            + " min_last_shipment_date, max_last_shipment_date, "
                            + " a_template_oid, a_op_bank_org_oid "
                            + "from lc_creation_rule "
                            + "where instrument_type_code = '" + TradePortalConstants.AUTO_LC_PO_UPLOAD + "'"
                            + " and a_owner_org_oid in (";
      }

      if (poUploadInstrType.equals(TradePortalConstants.AUTO_ATP_PO_UPLOAD)) {
            SELECT_SQL = "select name, description, maximum_amount, pregenerated_sql, "
                            + " min_last_shipment_date, max_last_shipment_date, "
                            + " a_template_oid, a_op_bank_org_oid "
                            + "from lc_creation_rule "
                            + "where instrument_type_code = '" + TradePortalConstants.AUTO_ATP_PO_UPLOAD + "'"
                            + " and a_owner_org_oid in (";
      }
      // TLE - 07/29/07 - CR-375 - Add End 
    }

    //TLE - 07/29/07 - CR-375 - Add Begin 
    else {
         // If it gets in here, it means that we are processing active po from the active_po_upload queue
         // where the user placed request(s) by clicking the 'Create ATPs Using Creation Rules' or 'Create LCs Using Creation Rules'
         // button on the PO list view tabpage. The value of the button is kept the xml file stored in active_po_upload.po_upload_parameters.
         if (value.equals(TradePortalConstants.BUTTON_CREATE_LC_FROM_PO)) {
            SELECT_SQL = "select name, description, maximum_amount, pregenerated_sql, "
                            + " min_last_shipment_date, max_last_shipment_date, "
                            + " a_template_oid, a_op_bank_org_oid "
                            + "from lc_creation_rule "
                            + "where instrument_type_code = '" + TradePortalConstants.AUTO_LC_PO_UPLOAD + "'"
                            + " and a_owner_org_oid in (";
            poUploadInstrType = InstrumentType.IMPORT_DLC;
         }
         else if (value.equals(TradePortalConstants.BUTTON_CREATE_ATP_FROM_PO)) {
            SELECT_SQL = "select name, description, maximum_amount, pregenerated_sql, "
                            + " min_last_shipment_date, max_last_shipment_date, "
                            + " a_template_oid, a_op_bank_org_oid "
                            + "from lc_creation_rule "
                            + "where instrument_type_code = '" + TradePortalConstants.AUTO_ATP_PO_UPLOAD + "'"
                            + " and a_owner_org_oid in (";
            poUploadInstrType = InstrumentType.APPROVAL_TO_PAY;
         }
    }
    //TLE - 07/29/07 - CR-375 - Add End 


    setLogger(AutoLCLogger.getInstance(getCorporateOrgOid()));

    deleteOldLogs(getCorporateOrgOid());

    // If we're coming from the Locate and Upload PO Data file page, go ahead and
    // parse the PO Data file contents and create PO line items from it
    if (poDataOption != null) {
      parsePurchaseOrderUploadData(poDataFileContents);

      // If the user elected to make the PO line items available to be manually
      // added to transactions, then simply return (i.e., no grouping should
      // occur).
      if (poDataOption
          .equals(TradePortalConstants.MANUALLY_ADD_PO_LINE_ITEMS)) {
        logUnassignedCount(TradePortalConstants.MANUALLY_ADD_PO_LINE_ITEMS);

        // Issue message to the log that the po line items have completed uploading
        logPurchaseOrderUploadComplete();

        LOG.debug("Exiting AutoLCCreateMediator \n\n\n");
        return outputDoc;
      }
    }

    groupPOs();

    // Issue message to the log that the po line items have completed uploading
    logPurchaseOrderUploadComplete();

    LOG.debug("Exiting AutoLCCreateMediator \n\n\n");

    return outputDoc;
  }

  /**
   * Builds an Input document handler containing the information required to create
   * a new instrument.
   *
   * @return com.amsinc.ecsg.util.DocumentHandler
   * @param creationRuleDoc com.amsinc.ecsg.util.DocumentHandler
   * @param benName java.lang.String
   * @param currency java.lang.String
   * @param amount java.math.BigDecimal
   * @param latestShipDt java.util.Date
   */
  private DocumentHandler buildInstrumentCreateDoc(DocumentHandler creationRuleDoc,
      String benName, String currency, BigDecimal amount,
      String latestShipDt, Vector poOids) {
	
    DocumentHandler instrCreateDoc = new DocumentHandler();
	//Srinivasu_D Fix of IR#T36000018903 Rel8.2 start - passing key using secretkey 
    String secretKeyString = getMediatorServices().getCSDB().getCSDBValue("SecretKey");
	javax.crypto.SecretKey secretKey = null;
	String instrumentOid = null;
	String templateOid = creationRuleDoc.getAttribute("/A_TEMPLATE_OID");
    instrCreateDoc.setAttribute("/clientBankOid", getClientBankOid());
	if (secretKeyString != null){
				byte[] keyBytes = com.amsinc.ecsg.util.EncryptDecrypt.base64StringToBytes(secretKeyString);
				secretKey = new javax.crypto.spec.SecretKeySpec(keyBytes, "DESede/CBC/PKCS5Padding");
				if (StringFunction.isNotBlank(templateOid)){
					instrumentOid = EncryptDecrypt.encryptStringUsingTripleDes(templateOid, secretKey);
				}

			}else{
				if (StringFunction.isNotBlank(templateOid)){
					instrumentOid = EncryptDecrypt.encryptStringUsingTripleDes(templateOid, secretKey);
				}
			}
	LOG.debug("instrumentOid: {}",instrumentOid);
	instrCreateDoc.setAttribute("/instrumentOid", instrumentOid);
	//Srinivasu_D Fix of IR#T36000018903 Rel8.2 end
    //TLE - 07/29/07 - CR-375 - Add Begin 
    if (poUploadInstrType.equals(TradePortalConstants.AUTO_ATP_PO_UPLOAD)) {
		instrCreateDoc.setAttribute("/instrumentType",
			InstrumentType.APPROVAL_TO_PAY);
	} else {
		instrCreateDoc.setAttribute("/instrumentType",
			InstrumentType.IMPORT_DLC);

	}
    //TLE - 07/29/07 - CR-375 - Add End 

    instrCreateDoc.setAttribute("/bankBranch", creationRuleDoc
        .getAttribute("/A_OP_BANK_ORG_OID"));
    instrCreateDoc.setAttribute("/copyType", TradePortalConstants.FROM_TEMPL);
    instrCreateDoc.setAttribute("/userOid", getUserOid());
    instrCreateDoc.setAttribute("/ownerOrg", getCorporateOrgOid());
    instrCreateDoc.setAttribute("/securityRights", getSecurityRights());

    instrCreateDoc.setAttribute("/benName", benName);
    instrCreateDoc.setAttribute("/currency", currency);
    instrCreateDoc.setAttribute("/shipDate", latestShipDt);
    instrCreateDoc.setAttributeDecimal("/amount", amount);

    // <Amit - IR EEUE032659500 -05/15/2005 >
    if(StringFunction.isBlank(userLocale)){
    	userLocale = "en_US";
    }    
    instrCreateDoc.setAttribute("/locale", userLocale);
    // <Amit - IR EEUE032659500 -05/15/2005 >

    for (int i = 0; i < poOids.size(); i++) {
      instrCreateDoc.setAttribute("/POLines(" + i + ")/poLineItemOid",
          (String) poOids.elementAt(i));
    }

    LOG.debug("Creation rules document is {}" , instrCreateDoc);
    return instrCreateDoc;
  }

  /**
   * Process through the vector of po line items, creating a set of the
   * po line items oids.  While doing this increment a line count and
   * accumulate the amount from each po line item.  When the line count
   * exceeds a certain number of lines or the total amount exceeds the passed in max
   * amount, pass the set of po line item oids to the create instrument process.
   * Furthermore, the po line items are ordered by ben_name and currency.
   * Whenever the ben_name or currency is different from the preceeding
   * line, pass the set of po line items oids to the create instrument process.
   * After each call to the instrument create process, reset the po line item
   * oid set to nothing.  Continue processing through the po
   * line items until done.
   *
   * @param poItemList java.util.Vector - the po item records (document)
   * @param maxAmount java.math.BigDecimal - the max amount allowed for the chunk
   * @param lcRuleName java.lang.String - the creation rule used to create this chunk
   * @exception AmsException
   */
  private void chunkPOs(Vector poItemList, DocumentHandler creationRuleDoc)
      throws RemoteException, com.amsinc.ecsg.frame.AmsException {

    BigDecimal poAmount;
    BigDecimal maxAmount = null;
    BigDecimal convertedMaxAmount = null;
    BigDecimal totalAmount = BigDecimal.ZERO;

    int itemCount = 0;

    String lastBenName = "";
    String lastCurrency = "";
    String latestShipDt = null;

    String benName = "";
    String currency = "";
    String oid;
    String shipDt;
    String poNum;
    String itemNum;

    String lcRuleName = creationRuleDoc.getAttribute("/NAME");

    boolean startNewChunk = true;
    boolean goesInThisChunk = true;

    Vector poOids = new Vector();
    Vector poNums = new Vector();

    DocumentHandler poItemDoc = null;

    // GGAYLE - DFUH031368817 - 04/03/2007 - new variables for the invalid Swift character check
    String listOfInvalidCharacters = null;
    String poText = null;
    int poListCount = 0;
    // GGAYLE - DFUH031368817 - 04/03/2007

    // IR-FUG121880779 - PHILC - BEGIN - Get override swift length indicator on client bank through corp org
    CorporateOrganization corporateOrg = (CorporateOrganization) mediatorServices
        .createServerEJB("CorporateOrganization");
    try {
      corporateOrg.getData(java.lang.Integer
          .parseInt(getCorporateOrgOid()));
    } catch (com.amsinc.ecsg.frame.InvalidObjectIdentifierException e) {
      mediatorServices.getErrorManager().issueError(
          TradePortalConstants.ERR_CAT_1,
          TradePortalConstants.ORG_NOT_EXIST);
    }
    long clientBankOid = corporateOrg.getAttributeLong("client_bank_oid");
    ClientBank clientBank = (ClientBank) mediatorServices
        .createServerEJB("ClientBank");
    try {
      clientBank.getData(clientBankOid);
    } catch (com.amsinc.ecsg.frame.InvalidObjectIdentifierException e) {
      mediatorServices.getErrorManager().issueError(
          TradePortalConstants.ERR_CAT_1,
          TradePortalConstants.INVALID_CLIENT_BANK_ORG);
    }
    String swiftOverrideInd = clientBank
        .getAttribute("override_swift_length_ind");
    // IR-FUG121880779 - PHILC - END - Get override swift length indicator on client bank through corp org

    AutoLCLogger logger = AutoLCLogger.getInstance(getCorporateOrgOid());

    // Determine the number of POs allowed per instrument
    int maximumPOsInInstr = MAX_ITEMS_ON_PO;

    long uploadDefnOid = 0;
    uploadDefnOid = getUploadDefinitionOid();

    if (uploadDefnOid != 0) {
        // When the user clicks on the 'Create ATPs using Creation Rules' or 'Create LCs using Creation Rules',
        // we don't have information of the PO definition. So skip it here for that case

      POUploadDefinition poDef = (POUploadDefinition) mediatorServices
        .createServerEJB("POUploadDefinition", uploadDefnOid);
      String includePoText = poDef.getAttribute("include_po_text");

      // IR-FUG121880779 - PHILC - BEGIN/END - add check of swift override indicator on next line to determine whether PO items need length calc
      if (includePoText.equals(TradePortalConstants.INDICATOR_YES)
          && (!swiftOverrideInd
            .equals(TradePortalConstants.INDICATOR_YES))) {
        // Get the maximum characters for the po text field
        int poTextSize = poDef.getAttributeInteger("po_text_size");
        // Determine the number of lines that po_text could take up (65 chars per line)
        int poTextLines = (int) Math.ceil(poTextSize / 65d);

        // Each PO will use up one line for its data in addition to the po_text lines, so add 1
        int linesPerPo = poTextLines + 1;

        // Figure out how many POs will fit into 94 lines
        maximumPOsInInstr = (int) Math.floor(94 / (poTextLines + 1));
      }
    }

    String value = creationRuleDoc.getAttribute("/MAXIMUM_AMOUNT");
    if (StringFunction.isNotBlank(value)) {
      maxAmount = new BigDecimal(value);
    } else {
      maxAmount = BigDecimal.ZERO;
    }

    for (int x = 0; x < poItemList.size(); x++) {
      // GGAYLE - DFUH031368817 - 04/03/2007
      // We will use this variable to count the number of PO line items fully processed, i.e. not skipped.
      poListCount++;
      // GGAYLE - DFUH031368817 - 04/03/2007

      poItemDoc = (DocumentHandler) poItemList.elementAt(x);

      // Pull off values from the document that drive the processing.
      poAmount = poItemDoc.getAttributeDecimal("/AMOUNT");
      benName = poItemDoc.getAttribute("/BEN_NAME");
      currency = poItemDoc.getAttribute("/CURRENCY");
      oid = poItemDoc.getAttribute("/PO_LINE_ITEM_OID");
      poNum = poItemDoc.getAttribute("/PO_NUM");
      itemNum = poItemDoc.getAttribute("/ITEM_NUM");
      shipDt = poItemDoc.getAttribute("/LAST_SHIP_DT");

      // GGAYLE - DFUH031368817 - 04/03/2007
      // Get the po_text from the XML doc and check it for invalid Swift characters
      poText = poItemDoc.getAttribute("/PO_TEXT");

      if (StringFunction.isNotBlank(poText))
      {
        listOfInvalidCharacters = InstrumentServices.checkAutoLCForInvalidSwiftCharacters(poText);

        if (StringFunction.isNotBlank(listOfInvalidCharacters))
        {
          // Log the error message and decrement the PO list counter (not the loop counter x)
          // Since we're skipping this invalid PO line item, we do not count it towards the items fully processed.
          getLogger().addLogMessage(getCorporateOrgOid(),
              getLogSequence(),
              TradePortalConstants.LCLOG_INVALID_SWIFT_CHARS_2PARM, getDisplayedPoNum(poNum, itemNum), listOfInvalidCharacters);
          poListCount--;
          continue;
        }
      }
      // GGAYLE - DFUH031368817 - 04/03/2007


      // Assume the po item goes in the current chunk and we
      // are not bypassing it.
      goesInThisChunk = true;

      LOG.debug("amount {}", poAmount);
      LOG.debug("ben    {}" , benName);
      LOG.debug("curren {}", currency);
      LOG.debug("oid    {}", oid);

      // For the first PO Line item, set several of the variables used
      // to indicate when to start a new chunk based on values from
      // this first po line item.
      //
      // GGAYLE - DFUH031368817 - 04/03/2007
      // Since we introduced new code above that allows for the skipping of a PO line item, we can no longer
      // use the loop counter x to determine the first item being fully processed.
        //if (x == 0) {
      if (poListCount == 1) {
      // GGAYLE - DFUH031368817 - 04/03/2007
        itemCount = 0;
        totalAmount = BigDecimal.ZERO;
        lastBenName = benName;
        lastCurrency = currency;
        startNewChunk = false;
        latestShipDt = shipDt;

        convertedMaxAmount = getConvertedMaxAmount(maxAmount,
            getBaseCurrency(), currency, creationRuleDoc);

        LOG.debug(
            "Converted creation rule max amount {} to {}", maxAmount, (convertedMaxAmount == null ? "null"
                    : convertedMaxAmount));
      }

      // When ben name changes, we need to start a new chunk.
      if (!benName.equals(lastBenName)) {
        goesInThisChunk = false;
        startNewChunk = true;
      }
      // When currency changes, we need to start a new chunk
      if (!currency.equals(lastCurrency)) {
        goesInThisChunk = false;
        startNewChunk = true;
      }

      // Add the po item amount to the total.  If we exceed the
      // (currency converted) max, we need to start a new chunk.
      // A null convertedMaxAmount means max amount is irrelevant
      // for chunking.
      BigDecimal newTotal = totalAmount.add(poAmount);

      if ((convertedMaxAmount != null)
          && (convertedMaxAmount.compareTo(BigDecimal.ZERO) > 0)
          && (newTotal.compareTo(convertedMaxAmount) > 0)) {

        if (poAmount.compareTo(convertedMaxAmount) > 0) {
          String[] parms = { poAmount.toString(),
              getDisplayedPoNum(poNum, itemNum),
              creationRuleDoc.getAttribute("/NAME") };

          getLogger().addLogMessage(getCorporateOrgOid(),
              getLogSequence(),
              TradePortalConstants.LCLOG_AMOUNT_TOO_BIG, parms);
          continue;
        }
        goesInThisChunk = false;
        startNewChunk = true;
      }

      // Increment the item count.  If we exceed the max allowed
      // lines, we need to start a new chunk.
      itemCount++;

      // IR-FUG121880779 - PHILC - BEGIN/END - add check of swift override indicator on next line to determine whether PO items need length calc
      if (itemCount >= maximumPOsInInstr
          && !swiftOverrideInd
              .equals(TradePortalConstants.INDICATOR_YES)) {
        goesInThisChunk = false;
        startNewChunk = true;
      }

      // As we process through the po line items for each chunk,
      // determine the latest shipment date of all the items in
      // the chunk (but only if this po item goes in this chunk.
      // Also, add the po item oid to a vector.
      if (goesInThisChunk) {
        if (shipDt != null) {
          if (latestShipDt == null)
            latestShipDt = shipDt;
          if (shipDt.compareTo(latestShipDt) > 0) {
            latestShipDt = shipDt;
          }
        }

        // Add the oid to the po oid vector.
        poOids.addElement(oid);
        poNums.addElement(getDisplayedPoNum(poNum, itemNum));

        // Set totalAmount to include the
        // new po line item amount.
        totalAmount = newTotal;
      }

      // If we need to start a new chunk, take the set of po line items
      // collected and create the instrument.  After, reset the poOid vector to
      // nothing to start a new chunk.

      if (startNewChunk) {
        // Build the input document used to create a new instrument
        DocumentHandler instrCreateDoc = buildInstrumentCreateDoc(creationRuleDoc,
            lastBenName, lastCurrency, totalAmount, latestShipDt,
            poOids);

        // Create instrument using the document, assign the po line
        // items, and create log records.
        try {
          createInstrumentFromPOData(instrCreateDoc, poOids, poNums, lcRuleName);

          // Add a blank line spacer after log messages for this instrument
          getLogger().addBlankLine(getCorporateOrgOid(),
              getLogSequence());
        } catch (AmsException e) {
          // This is probably a sql error but could be anything.
          // Post this exception to the log.
          getLogger().addLogMessage(getCorporateOrgOid(),
              getLogSequence(), e.getMessage());
        }

        poOids = new Vector();
        poNums = new Vector();
      }

      // If we need to start a new chunk, reset all the variables using
      // information from this po line item.
      if (startNewChunk) {
        itemCount = 0;
        totalAmount = poAmount;
        lastBenName = benName;
        lastCurrency = currency;
        startNewChunk = false;

        // When starting a new chunk, reset the latest ship date.
        // It may be null but that's ok.
        latestShipDt = shipDt;

        // Add the oid to the po oid vector.
        poOids.addElement(oid);
        poNums.addElement(getDisplayedPoNum(poNum, itemNum));

        // Since the currency may have changed, we need to reconvert
        // the instrument maxAmount to this new po line item currency.
        convertedMaxAmount = getConvertedMaxAmount(maxAmount,
            getBaseCurrency(), currency, creationRuleDoc);
        LOG.debug("Converted creation rule max amount {} to {} ", maxAmount, (convertedMaxAmount == null ? "null"
                    : convertedMaxAmount));
      }

    }

    // All finished processing the po line items, send the last set of
    // items to the create instrument process.
    if (poOids.size() > 0) {
      DocumentHandler instrCreateDoc = buildInstrumentCreateDoc(creationRuleDoc, benName,
          currency, totalAmount, latestShipDt, poOids);

      // Create the instrument using the document, assign the po line
      // items, and create log records.
      try {
        createInstrumentFromPOData(instrCreateDoc, poOids, poNums, lcRuleName);
      } catch (AmsException e) {
        // This is probably a sql error but could be anything.
        // Post this exception to the log.
        getLogger().addLogMessage(getCorporateOrgOid(),
            getLogSequence(), e.getMessage());
        LOG.error("Exception while creating instrument from po data .", e);
      }
    }
  }

  /**
   * This method deletes old log "files" for the corporate org.  Basically it
   * works by selecting all the log sequences for the org in reverse order.
   * We will keep some of the logs (based on MAX_LOGS_TO_KEEP) and skip over
   * that many sequences. For the remaining sequences, we delete all log
   * records with that sequence.
   *
   * @param corporateOrgOid java.lang.String
   */
  private void deleteOldLogs(String corporateOrgOid) throws AmsException,
      RemoteException {
	//jgadela R91 IR T36000026319 - SQL INJECTION FIX
    StringBuffer logSql = new StringBuffer(LOG_SELECT_SQL);
    logSql.append("?");
    logSql.append(LOG_ORDERBY);

     //jgadela R91 IR T36000026319 - SQL INJECTION FIX
    DocumentHandler resultSet = DatabaseQueryBean.getXmlResultSet(logSql.toString(), false,  new Object[]{corporateOrgOid});

    if (resultSet == null)
      resultSet = new DocumentHandler();

    Vector logList = resultSet.getFragments("/ResultSetRecord");
    DocumentHandler logDoc = null;
    String sequence;

    // Loop through each of the log sequences, skipping over the first
    // MAX_LOGS_TO_KEEP sequences.  For each one after that, delete
    // the log records.
    for (int x = MAX_LOGS_TO_KEEP; x < logList.size(); x++) {
      logDoc = (DocumentHandler) logList.elementAt(x);

      sequence = logDoc.getAttribute("/SEQUENCE_NUMBER");
    //jgadela R91 IR T36000026319 - SQL INJECTION FIX
      logSql = new StringBuffer(LOG_DELETE_SQL);
      logSql.append("?");

      try {
        int resultCount = DatabaseQueryBean.executeUpdate(logSql.toString(), false,  new Object[]{sequence});
      } catch (java.sql.SQLException e) {
        LOG.error("SQLException executing delete log sql", e);
      }
    }
  }

  /**
   * Getter for the baseCurrency attribute
   *
   * @return java.lang.String
   */
  private java.lang.String getBaseCurrency() {
    return baseCurrency;
  }

  /**
   * Getter for the attribute clientBankOid
   * @return java.lang.String
   */
  private java.lang.String getClientBankOid() {
    return clientBankOid;
  }

  /**
   * This method attempts to convert the given fromAmount (which is in teh given
   * currency which also happens to be the base currency) into the currency of the
   * po line item.  If the fromCurrency and toCurrency are the same, there is no
   * conversion to do.  Otherwise, we look at the currency of the po line item.
   * If it is a valid TradePortal currency (i.e., in refdata manager), we try to
   * get an fx rate using the toCurrency (the po line item currency).  If found,
   * use that information.  Otherwise get the base currency of the template
   * associated with the instrument creation rule.  If we can't find an fx rate for that
   * currency we can't convert and return a null amount.  Since the conversion we
   * are doing is FROM a base currency TO another currency (the opposite of the
   * way the fx rate information is defined, our calculation is opposite of the
   * fx rate info (e.g., we divide if the multiply_indicator is multiply).
   * <p>
   * @return java.math.BigDecimal - the resulting converted amount, null if can't
   *                                convert.
   * @param fromAmount java.math.BigDecimal - The amount to convert from
   * @param fromCurrency java.lang.String - The currency the fromAmount is in
   * @param toCurrency java.lang.String - The currency we want to convert to
   * @param creationRuleDoc DocumentHandler - The instrument creation rule record (as a doc)
   */
  private BigDecimal getConvertedMaxAmount(BigDecimal fromAmount,
      String fromCurrency, String toCurrency, DocumentHandler creationRuleDoc) {

    BigDecimal convertedAmount = null;

    try {
      // No need to convert if the from and to currencies are the same
      if (fromCurrency.equals(toCurrency)) {
        return fromAmount;
      }

      // fromAmount may have been defaulted to 0 (to prevent a null
      // pointer).  However, we don't want to set the maxAmount value
      // to 0 since nothing would get grouped.  If the fromAmount is
      // 0, return a null (indicating maxAmount not relevant for
      // grouping).
      if (fromAmount.compareTo(BigDecimal.ZERO) == 0) {
        return null;
      }

      String templateCurrency = null;
      String multiplyIndicator = null;
      BigDecimal rate = null;
      DocumentHandler fxRateDoc = null;

      ReferenceDataManager rdm = ReferenceDataManager.getRefDataMgr();

      // Test whether the po line item currency code is a valid Trade
      // Portal currency.
      if (rdm.checkCode(TradePortalConstants.CURRENCY_CODE, toCurrency,
          getUserLocale())) {
        // Get the fx rate info for the po line item currency
        fxRateDoc = getFxRateDoc(getCorporateOrgOid(), toCurrency);
        if (fxRateDoc == null) {
          // No fx rate found, isse warning and return a blank amount.
          // This indicates that maxAmount is not used for grouping.
          // Give error with the po line item currency.
          getLogger().addLogMessage(getCorporateOrgOid(),
              getLogSequence(),
              TradePortalConstants.LCLOG_NO_FX_RATE, toCurrency);
          return null;
        } else {
          // Found an fx rate for the template currency.  Use it.
        }
      } else {
        // Currency code for the po line item is not valid.  Use
        // the currency code from the instrument creation rule's template.
    	//jgadela R91 IR T36000026319 - SQL INJECTION FIX  
        String sql = "select copy_of_currency_code from template, transaction where p_instrument_oid = c_template_instr_oid and template_oid = ? ";
        DocumentHandler templateDoc = null;
        try {
          templateDoc = DatabaseQueryBean.getXmlResultSet(sql, true, new Object[]{creationRuleDoc.getAttribute("/A_TEMPLATE_OID")});
        } catch (AmsException e) {
        	LOG.error("Exception while retrieving data from template/transaction", e);
          templateDoc = null;
        }

        if (templateDoc == null) {
          // Can't get the currency code off the template.  Issue a warning and return
          // a null amount.  This indicates NOT to use maxAmount for grouping.
          getLogger().addLogMessage(getCorporateOrgOid(),
              getLogSequence(),
              TradePortalConstants.LCLOG_INVALID_CURRENCY,
              creationRuleDoc.getAttribute("/NAME"));
          return null;
        } else {
          // We got the template currency but it may be blank.  We're only
          // concerned with the first record (there should not be more).
          templateCurrency = templateDoc
              .getAttribute("/ResultSetRecord(0)/COPY_OF_CURRENCY_CODE");
          if (StringFunction.isBlank(templateCurrency)) {
            // Can't get the currency code off the template.  Issue a warning and return
            // a null amount.  This indicates NOT to use maxAmount for grouping.
            getLogger().addLogMessage(getCorporateOrgOid(),
                getLogSequence(),
                TradePortalConstants.LCLOG_INVALID_CURRENCY,
                creationRuleDoc.getAttribute("/NAME"));
            return null;
          } else {
            // Currency is good so attempt to get the fx rate doc.
            fxRateDoc = getFxRateDoc(getCorporateOrgOid(),
                templateCurrency);

            // Don't look for an FX rate if the template's currency is the base currency
            // No FX rate is needed in that case
            if (!templateCurrency.equals(getBaseCurrency())) {
              if (fxRateDoc == null) {
                // No fx rate found, isse warning and return a blank amount.
                // This indicates that maxAmount is not used for grouping.
                // Give error using the template currency.
                getLogger().addLogMessage(getCorporateOrgOid(),
                    getLogSequence(),
                    TradePortalConstants.LCLOG_NO_FX_RATE,
                    templateCurrency);
                return null;
              } else {
                // Found an fx rate for the template currency.  Use it.
              }
            } else {
              // Base Currency and Template currency are identical
              // No need for conversion.  Return the from amount
              return fromAmount;
            }
          } // end templateCurrency == null
        } // end templateDoc == null
      } // end checkCode

      // We now have an fx rate doc (although the values on the doc may be
      // blank.)  Attempt to convert the amount.
      multiplyIndicator = fxRateDoc.getAttribute("/MULTIPLY_INDICATOR");
      rate = fxRateDoc.getAttributeDecimal("/RATE");

      if (multiplyIndicator != null && rate != null) {
        // We're converting FROM the base currency TO the po line item
        // currency so we need to do the opposite of the multiplyIndicator
        // (the indicator is set for converting FROM some currency TO the
        // base currency)
        if (multiplyIndicator.equals(TradePortalConstants.MULTIPLY)) {
          convertedAmount = fromAmount.divide(rate,
              BigDecimal.ROUND_HALF_UP);
        } else {
          convertedAmount = fromAmount.multiply(rate);
        }
      } else {
        // We have bad data in the fx rate record.  Issue warning and return
        // null amount (indicating that maxAmount is not used for grouping)
        getLogger().addLogMessage(getCorporateOrgOid(),
            getLogSequence(),
            TradePortalConstants.LCLOG_NO_FX_RATE,
            fxRateDoc.getAttribute("/CURRENCY_CODE"));
        return null;
      }
    } catch (AmsException e) {
      LOG.error("Exception found getting converted max amount:", e);
    }

    return convertedAmount;
  }

  /**
   * Getter for the attribute corporateOrgOid
   * @return java.lang.String
   */
  private java.lang.String getCorporateOrgOid() {
    return corporateOrgOid;
  }

  /**
   * Returns the displayable form of the ponum and line number.)
   * @return java.lang.String
   * @param poNum java.lang.String
   * @param itemNum java.lang.String
   */
  private String getDisplayedPoNum(String poNum, String itemNum) {
    return poNum + "  " + itemNum;
  }

  /**
   * Uses the given corporate org oid and currency to get an fx rate record
   * (as a document handler).  The currency, rate, and multiple_indicator are
   * returned in the document.
   *
   * @return DocumentHandler - result of the query, may be null
   * @param corporateOrg java.lang.String - corporate org oid to use
   * @param currency java.lang.String - currency to use
   */
  private DocumentHandler getFxRateDoc(String corporateOrg, String currency) {

		Cache fxCache = TPCacheManager.getInstance().getCache(TradePortalConstants.FX_RATE_CACHE);
		DocumentHandler resultSet = (DocumentHandler) fxCache.get(corporateOrg + "|" + currency);

		if (resultSet != null) {
			Vector fxRateDocs = resultSet.getFragments("/ResultSetRecord");
			return (DocumentHandler) fxRateDocs.elementAt(0);
		}

		return null;

	}

  /**
	 * Uses the DatabaseQueryBean to execute sql to retrieve data from the
	 * po_line_item table which are assigned to the given transaction. the result
	 * is a vector of documents, one for each po line item row.
	 * 
	 * @return java.util.Vector
	 * @param corporateOrgOid
	 *           java.lang.String
	 */
  private Vector getCreationRules(String corporateOrgOid) throws AmsException {

    StringBuilder lcSql = new StringBuilder(SELECT_SQL);
    lcSql.append("?");
    List<Object> sqlParams = new ArrayList();
    sqlParams.add(corporateOrgOid);

    // PHILC-BEGIN - IR-FOUH010852128 - Loop back through all parent orgs for oids to include in search for LC Creation rules
    do {
    	//jgadela R91 IR T36000026319 - SQL INJECTION FIX
      String parentCorpSql = "select p_parent_corp_org_oid from corporate_org where organization_oid = ? AND activation_status = ?";
      DocumentHandler parentCorpOrg = DatabaseQueryBean.getXmlResultSet(parentCorpSql, false, corporateOrgOid, TradePortalConstants.ACTIVE);
      if( parentCorpOrg != null )
      {
        Vector resultsVector = parentCorpOrg.getFragments("/ResultSetRecord/");
        if( ((DocumentHandler)resultsVector.elementAt(0)) != null )
        {
          DocumentHandler parentOid = ((DocumentHandler)resultsVector.elementAt(0));
          String parentCorpOid = "";
          parentCorpOid = parentOid.getAttribute("/P_PARENT_CORP_ORG_OID");
          if (StringFunction.isNotBlank(parentCorpOid)) {
            lcSql.append(",?");
            corporateOrgOid = parentCorpOid;
            sqlParams.add(parentCorpOid);
          } else {  // No more parents, so break out of loop
            break;
          }
        } else {  // No more parents, so break out of loop
          break;
        }
      } else { // no more parents, so break out of loop
        break;
      }
    } while (true);
    // PHILC - Commented the previous code out for subsidiary as we are getting all orgs in the family tree
    // If user that uploaded the POs was using subsidiary access and is allowed
    // to use their own data as well as the subsidiary's, include the parent org oid in the query
    // if (getSubsidiaryAccessOrgOid() != null)
    //  lcSql.append(",").append(getSubsidiaryAccessOrgOid());
    // PHILC-END - IR-FOUH010852128 - Loop back through all parent orgs for oids to include in search for LC Creation rules

    lcSql.append(") ");
    lcSql.append(ORDERBY_CLAUSE);

    DocumentHandler resultSet = DatabaseQueryBean.getXmlResultSet(lcSql.toString(), false, sqlParams);

    if (resultSet == null)
      resultSet = new DocumentHandler();

    return resultSet.getFragments("/ResultSetRecord");

  }

  /**
   * Getter for the auto lc logger attribute
   * @return com.ams.tradeportal.autolc.AutoLCLogger
   */
  private AutoLCLogger getLogger() {
    return logger;
  }

  /**
   * Getter for the attribute logSequence
   * @return java.lang.String
   */
  private java.lang.String getLogSequence() {
    return logSequence;
  }

  /**
   * Getter for the mediator services attribute.
   * @return com.amsinc.ecsg.frame.MediatorServices
   */
  private MediatorServices getMediatorServices() {
    return mediatorServices;
  }

  /**
   * Uses the DatabaseQueryBean to execute sql to retrieve data from the
   * po_line_item table which are
   *     1) unassiged,
   *     2) for the given org,
   *     3) for the given upload sequence (if non-blank)
   *     4) with a last_ship_dt between today + mindays and today + maxdays
   *        (where mindays and maxdays are from the lc rule and both are non-blank),
   *     5) meet the remaining lc creation rule criteria,
   *     6) and, were uploaded (as opposed to being manually entered)
   *
   * The result is a vector of documents, one for each po line item row.
   *
   * @return java.util.Vector
   * @param creationRuleDoc DocumentHadler - the current lc rule being considered
   */
  private Vector getPOItems(DocumentHandler creationRuleDoc) throws AmsException,
      RemoteException {

    // The Where clause for the po select will include:
    //     a_assigned_to_trans_oid is null
    //     a_owner_org_oid = x
    //     sequence = y (if y is non-blank)
    //     last_ship_dt between today+mindays and today+maxdays (if both non-blank)
    //     the pregenerated sql from the lc creation rule (criteria)
    // It is ordered by ben_name and currency (to support chunking)
	//jgadela R91 IR T36000026319 - SQL INJECTION FIX
    StringBuilder poSql = new StringBuilder(PO_SELECT_SQL);
    poSql.append("?");
  
    List<Object> sqlParams = new ArrayList();
    sqlParams.add(getCorporateOrgOid());

    // Add condition for sequence if it is non-blank
    if (StringFunction.isNotBlank(getUploadSequenceNumber())) {
      poSql.append(" and upload_sequence_num = ? ");
      sqlParams.add(getUploadSequenceNumber());
    }

    // Add condition for last_ship_dt if both min and max are set;
    String minDays = creationRuleDoc.getAttribute("/MIN_LAST_SHIPMENT_DATE");
    String maxDays = creationRuleDoc.getAttribute("/MAX_LAST_SHIPMENT_DATE");

    if (StringFunction.isNotBlank(minDays)
        && StringFunction.isNotBlank(maxDays)) {

      // The syntax for the last_ship_dt condition will be something like:
      //  "and last_ship_dt between TO_DATE('02-JUL-01')+3 and
      // TO_DATE('02-JUL-01')+6" where today's current date (localized
      // for the user's timezone) is Jul 01, 2001
      // and min_last_shipment_date is 3 and max_last_shipment_date is 6

      // First get the current local date
      String currentGmtDate = DateTimeUtility.getGMTDateTime();
      Date currentLocalDate = TPDateTimeUtility.getLocalDateTime(
          currentGmtDate, getTimeZone());

      SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yy");

      String today = formatter.format(currentLocalDate);

      poSql.append(" and last_ship_dt between TO_DATE(?)+?");
      poSql.append(" and  TO_DATE(?)+?");
      sqlParams.add(today);
      sqlParams.add(minDays);
      sqlParams.add(today);
      sqlParams.add(maxDays);
    }

    // Add condition for the remaining criteria from the lc creation rule
    String criteria = creationRuleDoc.getAttribute("/PREGENERATED_SQL");

    if (StringFunction.isNotBlank(criteria)) {
      poSql.append(" and ");
      poSql.append(criteria);
    }

    // Finally add the orderby clause.
    poSql.append(" ");
    poSql.append(PO_ORDERBY);

  //jgadela R91 IR T36000026319 - SQL INJECTION FIX
    DocumentHandler resultSet = DatabaseQueryBean.getXmlResultSet(poSql.toString(),false, sqlParams);

    if (resultSet == null)
      resultSet = new DocumentHandler();

    return resultSet.getFragments("/ResultSetRecord");

  }

  /**
   * Getter for the attribute security rights
   * @return java.lang.String
   */
  private java.lang.String getSecurityRights() {
    return securityRights;
  }

  /**
   * Getter for the timeZone attribute
   * @return java.lang.String
   */
  private java.lang.String getTimeZone() {
    return timeZone;
  }

  /**
   * Getter for the uploadFileName attribute
   * @return java.lang.String
   */
  private java.lang.String getUploadFileName() {
    return uploadFileName;
  }

  /**
   * Getter for the attribute uploadSequenceNumber
   * @return java.lang.String
   */
  private java.lang.String getUploadSequenceNumber() {
    return uploadSequenceNumber;
  }

  /**
   * Getter for the user locale attribute
   * @return java.lang.String
   */
  private java.lang.String getUserLocale() {
    return userLocale;
  }

  /**
   * Getter for the attribute userOid
   * @return java.lang.String
   */
  private java.lang.String getUserOid() {
    return userOid;
  }

  private long getUploadDefinitionOid() {
    return uploadDefinitionOid;
  }

  /**
   * This is the main method for handling the Group PO's logic.
   * The basic steps for this are as follows:
   * 1. For each LC Creation Rule for the given org (processed in
   *    reverse creation date order)
   * 1.1 Select all the po_line_item records that belong to that org
   *     and are unassigned.  If we're processing for only a single
   *     upload file, then add the criteria that the line items are
   *     for the given upload sequence.
   * 1.2 Begin a tally of the line item count and the total dollar
   *     amount.
   * 1.3 Get a chunk of PO Line items
   * 1.3.1  If the line item count or dollar amount is exceeded for
   *        the current lc creation rule, we're finished with this chunk
   * 1.3.2  If the ben_name is different, finish the chunk.
   * 1.3.3  If the currency is differenet, finish the chunk.
   * 1.4 For a PO Line item chunk, create an LC, passing in the set
   *     of oids from the chunk.  This causes an LC to be created,
   *     the line items assigned to this LC, several LC fields to be
   *     derived, and the goods description to be built,
   *
   * @param corporateOrgOid java.lang.String
   * @param sequence java.lang.String
   */
  private void groupPOs() throws AmsException, RemoteException {

    DocumentHandler creationRuleDoc;

    Vector lcRulesList = getCreationRules(getCorporateOrgOid());
    Vector poItemList;

    Date currentLocalDate = TPDateTimeUtility.getLocalDateTime(
        DateTimeUtility.getGMTDateTime(), getTimeZone());

    String localDate = TPDateTimeUtility.formatDateTime(currentLocalDate,
        getUserLocale());

    getLogger().addBlankLine(getCorporateOrgOid(), getLogSequence());

    getLogger().addLogMessage(getCorporateOrgOid(), getLogSequence(),
        TradePortalConstants.LCLOG_GROUPING_START, localDate);

    if (lcRulesList.size() < 1) {
      // There are no LC creation rules to process.  Post a message
      getLogger().addLogMessage(getCorporateOrgOid(), getLogSequence(),
          TradePortalConstants.LCLOG_NO_LC_RULES);
      return;
    }

    for (int i = 0; i < lcRulesList.size(); i++) {
      // Get the lc creation rule and pull some values off it.
      creationRuleDoc = (DocumentHandler) lcRulesList.elementAt(i);

      String parms2[] = { creationRuleDoc.getAttribute("/NAME") };

      getLogger().addBlankLine(getCorporateOrgOid(), getLogSequence());

      getLogger().addLogMessage(getCorporateOrgOid(), getLogSequence(),
          TradePortalConstants.LCLOG_PROCESSING_LC_RULE, parms2);

      // Get the po items for this lc creation rule
      poItemList = getPOItems(creationRuleDoc);

      // (Note, because the error manager wraps message paramters
      // within single quotes, we can't reuse parms2[] above here
      // because its entries are now wrapped.  If we reused it,
      // the parms would get wrapped again.
      String parms3[] = { creationRuleDoc.getAttribute("/NAME") };

      if (poItemList.size() < 1) {
        getLogger()
            .addLogMessage(
                getCorporateOrgOid(),
                getLogSequence(),
                TradePortalConstants.LCLOG_NO_ITEMS_FOR_LC_RULE,
                parms3);

      } else {
        // Now start chunking to po line items.
        chunkPOs(poItemList, creationRuleDoc);
      }
    }

    // All finished processing through the po line items (for either a
    // single upload sequence or all of them.  Now print an informational
    // message indicating how many are still unassigned.

    logUnassignedCount(TradePortalConstants.GROUP_PO_LINE_ITEMS);

    currentLocalDate = TPDateTimeUtility.getLocalDateTime(DateTimeUtility
        .getGMTDateTime(), getTimeZone());
    localDate = TPDateTimeUtility.formatDateTime(currentLocalDate,
        getUserLocale());
    String[] parms2 = { localDate };

    getLogger().addBlankLine(getCorporateOrgOid(), getLogSequence());

    getLogger().addLogMessage(getCorporateOrgOid(), getLogSequence(),
        TradePortalConstants.LCLOG_CREATE_LC_STOP, parms2);

  }

  /**
   * This methods posts one or two messages to the log.  The first optional
   * message indicates how many po line items for a specific upload sequence were
   * not assigned.  This message is applicable only when the user is processing
   * an upload file.  The second message always prints and indicates how many
   * po line items (in total) are still unassigned.
   */
  private void logUnassignedCount(String poDataOption) throws AmsException,
      RemoteException {
    int count = 0;
  //jgadela R91 IR T36000026319 - SQL INJECTION FIX
    String whereClause = " a_assigned_to_trans_oid is NULL and source_type =? and a_owner_org_oid = ? " ;

    // If we're processing for a specific upload sequence, print a message
    // for how many items are unassigned from that sequence.
    if (StringFunction.isNotBlank(getUploadSequenceNumber())) {
    	//jgadela R91 IR T36000026319 - SQL INJECTION FIX
      StringBuilder whereBySequenceSql = new StringBuilder(whereClause);
      whereBySequenceSql.append(" and upload_sequence_num = ? ");
      count = DatabaseQueryBean.getCount("po_line_item_oid", "PO_LINE_ITEM", whereBySequenceSql.toString(), false , TradePortalConstants.PO_SOURCE_UPLOAD, getCorporateOrgOid(), getUploadSequenceNumber());

      getLogger().addBlankLine(getCorporateOrgOid(), getLogSequence());

      if (poDataOption
          .equals(TradePortalConstants.MANUALLY_ADD_PO_LINE_ITEMS)) {
        getLogger().addLogMessage(getCorporateOrgOid(),
            getLogSequence(),
            TradePortalConstants.LCLOG_UNASSIGNED_ITEMS_UPLOADED,
            String.valueOf(count));

      } else {
        getLogger().addLogMessage(getCorporateOrgOid(),
            getLogSequence(),
            TradePortalConstants.LCLOG_UPLOAD_ITEMS_UNASSIGNED,
            String.valueOf(count), getUploadFileName());
      }
    }

    // In either event, we always get the total count of all unassigned
    // po line items.
  //jgadela R91 IR T36000026319 - SQL INJECTION FIX
    count = DatabaseQueryBean.getCount("po_line_item_oid", "PO_LINE_ITEM", whereClause, false, TradePortalConstants.PO_SOURCE_UPLOAD, getCorporateOrgOid());

    getLogger().addBlankLine(getCorporateOrgOid(), getLogSequence());

    getLogger().addLogMessage(getCorporateOrgOid(), getLogSequence(),
        TradePortalConstants.LCLOG_TOTAL_ITEMS_UNASSIGNED,
        String.valueOf(count));

  }

  /**
   * Setter for the base currency attribute
   *
   * @param newBaseCurrency java.lang.String
   */
  private void setBaseCurrency(java.lang.String newBaseCurrency) {
    baseCurrency = newBaseCurrency;
  }

  /**
   * Setter for the clientBankOid attribute
   * @param newClientBankOid java.lang.String
   */
  private void setClientBankOid(java.lang.String newClientBankOid) {
    clientBankOid = newClientBankOid;
  }

  /**
   * Setter for the corporateOrgOid attribute
   * @param newCorporateOrgOid java.lang.String
   */
  private void setCorporateOrgOid(java.lang.String newCorporateOrgOid) {
    corporateOrgOid = newCorporateOrgOid;
  }

  /**
   * Setter for the auto upload log attribute
   * @param newLogger com.ams.tradeportal.autolc.AutoLCLogger
   */
  private void setLogger(AutoLCLogger newLogger) {
    logger = newLogger;
  }

  /**
   * Setter for the logSequence attribute
   * @param newLogSequence java.lang.String
   */
  private void setLogSequence(java.lang.String newLogSequence) {
    logSequence = newLogSequence;
  }

  /**
   * Setter for the mediator services attribute
   * @param newMediatorServices com.amsinc.ecsg.frame.MediatorServices
   */
  private void setMediatorServices(MediatorServices newMediatorServices) {
    mediatorServices = newMediatorServices;
  }

  /**
   * Setter for the security rights attribute
   * @param newUserOid java.lang.String
   */
  private void setSecurityRights(java.lang.String newSecurityRights) {
    securityRights = newSecurityRights;
  }

  /**
   * Setter for the timeZone attribute
   * @param newTimeZone java.lang.String
   */
  private void setTimeZone(java.lang.String newTimeZone) {
    timeZone = newTimeZone;
  }

  /**
   * Setter for the upload definition oid attribute
   * @param newUploadDefinitionOid long
   */
  private void setUploadDefinitionOid(long newUploadDefinitionOid) {
    uploadDefinitionOid = newUploadDefinitionOid;
  }

  /**
   * Setter for the upload file name attribute
   * @param newUploadFileName java.lang.String
   */
  private void setUploadFileName(java.lang.String newUploadFileName) {
    uploadFileName = newUploadFileName;
  }

  /**
   * Setter for the uploadSequenceNumber attribute
   * @param newUploadSequenceNumber java.lang.String
   */
  private void setUploadSequenceNumber(
      java.lang.String newUploadSequenceNumber) {
    uploadSequenceNumber = newUploadSequenceNumber;
  }

  /**
   * Setter for the user locale
   * @param newUserLocale java.lang.String
   */
  private void setUserLocale(java.lang.String newUserLocale) {
    userLocale = newUserLocale;
  }

  /**
   * Setter for the userName attribute
   * @param newUserName java.lang.String
   */
  private void setUserName(java.lang.String newUserName) {
    userName = newUserName;
  }

  /**
   * Setter for the userOid attribute
   * @param newUserOid java.lang.String
   */
  private void setUserOid(java.lang.String newUserOid) {
    userOid = newUserOid;
  }

  /**
   * Setter for the subsidiaryAccessOrgOid attribute
   * @param subsidiaryAccessOrgOid java.lang.String
   */
  private void setSubsidiaryAccessOrgOid(String subsidiaryAccessOrgOid) {
    this.subsidiaryAccessOrgOid = subsidiaryAccessOrgOid;
  }

  /**
   * Getter for the subsidiaryAccessOrgOid attribute
   * @return java.lang.String
   */
  private String getSubsidiaryAccessOrgOid() {
    return this.subsidiaryAccessOrgOid;
  }

  /**
   * createInstrumentFromPOData()
   *
   * This method will call the createNew() method on instrument to create a new ImportLC/ATP/etc.
   * It will then set certain fields on the LC based on what data is in the grouped PO's.
   *
   * INPUTDOC - The input doc must contain the following
   *    /instrumentOid ---------- The existing instruments oid or "0" for a new instr.
   *    /transactionType -------- The type of the transaction being created
   *    /bankBranch ------------- The oid of the Operational Bank Org
   *    /clientBankOid ---------- The oid of the client bank that owns the corporation
   *    /userOid ---------------- The oid of the user creating the doc
   *    /securityRights --------- The security rights of the user that is creating the transaction
   *    /copyType --------------- What we copy a new instrument from (TP CONSTANTS: FROM_INSTR, FROM_TEMPL, & FROM_BLANK)
   *    /ownerOrg --------------- The oid of the user's corporate organization
   *    /benName ---------------- The name of the beneficiary of the PO Line Items
   *    /amount ----------------- The sum of the amounts of the PO Line Items
   *    /currency --------------- The currency of the amounts of the PO Line Items
   *    /shipDate --------------- The last shipment date of the amounts of the PO Line Items
   *
   * OUTPUTDOC - The output doc will contain the following
   *    /transactionOid --------- The oid of the issue tranasction for the new ImportLC/ATP/etc.
   *
   * @param DocumentHandler inputDoc - described above
   * @param poOids Vector - list of po line item oids going in this instrument
   * @param poNums Vector - list of po num/line items going in this instrument
   * @param lcRuleName String - name of the lc creation rule causing this instrument creation
   * @return DocumentHandler outputDoc - described above
   */

  private DocumentHandler createInstrumentFromPOData(DocumentHandler inputDoc,
      Vector poOids, Vector poNums, String lcRuleName)
      throws AmsException, RemoteException {
    MediatorServices mediatorServices = getMediatorServices();
    LOG.debug("Entered createInstrumentFromPOData(). inputDoc follows ->\n {}", inputDoc);

    DocumentHandler outputDoc = null;

    //Create a new instrument
    Instrument newInstr = (Instrument) mediatorServices
        .createServerEJB("Instrument");
    outputDoc = newInstr.createLCFromPOData(inputDoc, poOids, poNums,
        lcRuleName);
    LOG.debug("Returned to AutoLCCreateMediator.createInstrumentFromPOData() from instrument.createInstrumentFromPOData()\n Returned document follows -> {}", outputDoc);

    // Now that the transaction has been created, assign this transaction
    // oid to all the line items on the transaction.
    POLineItemUtility.updatePOTransAssignment(poOids, outputDoc
        .getAttribute("/transactionOid"), newInstr
        .getAttribute("instrument_oid"), outputDoc
        .getAttribute("/shipmentOid"));

    // Finally, create log records for each po assigned to the transaction
    String instrumentId = newInstr.getAttribute("complete_instrument_id");
    POLineItemUtility.logPOAssignment(instrumentId, lcRuleName, poNums,
        getCorporateOrgOid(), getLogSequence());

    return outputDoc;
  }

  /**
   * This method is used to parse the PO data file that was uploaded by a user
   * for PO line item data. First, it initializes a PO Line Item Data object
   * with information from the PO upload definition that was selected for the
   * PO upload, as well as other information necessary for validation, saving,
   * and updating of various PO line item and transaction objects. For each PO
   * line item string (i.e., line) in the file, the method will set field
   * values in the PO Line Item Data object, will perform numerous checks for
   * existing PO line items, will validate all the data in the PO line item,
   * and will save the data should all validations succeed.
   *

   * @param      java.lang.String poDataFileContents - the contents of the PO data file that was uploaded
   * @exception  com.amsinc.ecsg.frame.AmsException
   * @exception  java.rmi.RemoteException
   * @return     void
   */
  private void parsePurchaseOrderUploadData(String poDataFileContents)
      throws AmsException, RemoteException {
    POLineItemData poLineItemData = null;
    boolean hasMoreLineItemsToProcess = true;
    String currentPOLineItem = null;
    String poNumber = null;
    int newLineCharsIndex = -2; // Note: this is initialized to -2 to account for the 2 new
    // line characters that appear in the file: '\r' and '\n'

    // First, initialize the PO Line item data object with all info necessary for parsing the
    // PO Data file that was uploaded
    poLineItemData = new POLineItemData(getSecurityRights(),
        getCorporateOrgOid(), getUserOid(), getUploadSequenceNumber(),
        getLogSequence(), getUploadDefinitionOid(),
        getSessionContext(), getLogger(), mediatorServices
            .getResourceManager());
    poLineItemData.setInstrumentType(poUploadInstrType);

    // Process each line item in the PO data file
    while (hasMoreLineItemsToProcess) {
      poLineItemData.resetPOLineItemData();
      poDataFileContents = poDataFileContents
          .substring(newLineCharsIndex + 2);

      // Retrieve the index of the return and new line characters, which separate PO
      // line items
      newLineCharsIndex = poDataFileContents.indexOf("\r\n");

      // If there are no more PO line items to process, stop processing them; otherwise,
      // retrieve the next PO line item; if we have a blank line
      if (newLineCharsIndex < 0) {
        hasMoreLineItemsToProcess = false;
        continue;
      }

      // Get the current PO Line item string, excluding the new line characters
      currentPOLineItem = poDataFileContents.substring(0,
          newLineCharsIndex);

      // If we have a blank line, skip to the next PO line item
      if ((newLineCharsIndex == 0)
          || (currentPOLineItem.trim().equals(""))) {
        continue;
      }

      // Set all PO line item field values with the data in the current PO Line item
      // string; if the string doesn't have the correct format for the PO upload
      // definition (either because not enough delimiters exist in the string or the
      // length of the string is greater than the combined lengths of all PO line
      // item fields), continue with the next PO line item
      if (poLineItemData.setPOLineItemFieldValues(currentPOLineItem) == false) {

        continue;
      }

      // Next, make sure a valid PO line number was entered in the string; if one wasn't,
      // log an error message and continue with the next PO line item
      poNumber = poLineItemData.getPONumber();

      if ((poNumber == null) || (poNumber.equals(""))) {
        poLineItemData.logMissingPONumberMessage();
        continue;
      }

      // Check to see if the PO line item has already been uploaded; if a match is found,
      // continue with the next PO line item
      if (poLineItemData.checkForPOLineItemMatch(this.userLocale) == true) {
        continue;
      }

      // If the PO line item wasn't already uploaded, validate all the values for each
      // PO line item field; if a required field is missing or invalid, continue with
      // the next PO line item
      if (poLineItemData.validatePOLineItemData() == false) {
        continue;
      }

      // If all required PO line item fields are present and valid, save the PO line
      // item data
      poLineItemData.savePOLineItemData();
    }

    poLineItemData.resetPOLineItemData();

  }

  /**
   * This method issues an informational message to the user indicating that
   * the po file has completed uploading
   *

   * @exception  com.amsinc.ecsg.frame.AmsException
   * @return     void
   */
  private void logPurchaseOrderUploadComplete() throws AmsException {
    Date currentLocalDate = TPDateTimeUtility.getLocalDateTime(
        DateTimeUtility.getGMTDateTime(), getTimeZone());
    String localDate = TPDateTimeUtility.formatDateTime(currentLocalDate,
        getUserLocale());

    getLogger().addBlankLine(getCorporateOrgOid(), getLogSequence());

    getLogger().addLogMessage(getCorporateOrgOid(), getLogSequence(),
        TradePortalConstants.LCLOG_UPLOAD_STOP, localDate);
  }
}
