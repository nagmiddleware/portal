package com.ams.tradeportal.mediator.util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.RemoteException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.ams.tradeportal.busobj.BankTransactionUpdate;
import com.ams.tradeportal.busobj.DocumentImage;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.ComponentList;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.frame.MediatorServices;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.StringFunction;

public class BankUpdateCentreProcessor {
private static final Logger LOG = LoggerFactory.getLogger(BankUpdateCentreProcessor.class);

	/**
	 * 
	 * @param MailMessage
	 * @param mediatorService
	 * @throws AmsException 
	 * @throws RemoteException 
	 */
	public static void createMailMessageToBank( DocumentHandler MailMessage, MediatorServices mediatorServices ) 
			throws RemoteException, AmsException {
		AdminMailInUnPackager.process(MailMessage, mediatorServices);
	}
	
	/**
	 * 
	 * @param bankMailMessageOid
	 * @param assignCorpOrgOid
	 * @param messageSubject
	 * @param messageText
	 * @param mediatorServices
	 * @throws NumberFormatException
	 * @throws RemoteException
	 * @throws AmsException
	 */
	public static void createMailMessageToCust(String bankMailMessageOid, String assignCorpOrgOid, DocumentHandler inputDoc, MediatorServices mediatorServices ) 
			throws NumberFormatException, RemoteException, AmsException {
		AdminMailOutPackager.process( bankMailMessageOid, assignCorpOrgOid, inputDoc, mediatorServices );
	}

	/**
	 * 
	 * @param bankUpdateTransactionOid
	 * @param mediatorServices
	 */
	public static void createTplMessageToCust ( String bankUpdateTransOid, String bankTransStatus, String transactionOid, 
			DocumentHandler inputDoc, MediatorServices mediatorServices )
			throws NumberFormatException, RemoteException, AmsException {
		AdminTPLMessagePackager.process( bankUpdateTransOid, bankTransStatus, transactionOid, inputDoc, mediatorServices );
	}
	
	
	/**
	 * 
	 * @param transactionOid
	 * @param mediatorService
	 * @throws RemoteException
	 * @throws AmsException
	 */
	public static void createTransactionToBank(String transactionOid, DocumentHandler imageDoc, MediatorServices mediatorServices) 
			throws RemoteException, AmsException {
		
		String bankTransUpdateOid = null;
		BankTransactionUpdate bankTransactionUpdate = ( BankTransactionUpdate ) mediatorServices.createServerEJB("BankTransactionUpdate");
		String sql = " SELECT bank_transaction_update_oid FROM bank_transaction_update WHERE a_transaction_oid = ? ";
		DocumentHandler resultXML = DatabaseQueryBean.getXmlResultSet( sql, true, new Object[]{transactionOid} );		
		if ( resultXML != null ) { 
			bankTransUpdateOid = resultXML.getAttribute("/ResultSetRecord(0)/BANK_TRANSACTION_UPDATE_OID");
		}
		
		if ( StringFunction.isNotBlank(bankTransUpdateOid) ) {
			bankTransactionUpdate.getData( Long.parseLong(bankTransUpdateOid) );
			bankTransactionUpdate.setAttribute( "bank_transaction_status", "" );
			bankTransactionUpdate.setAttribute( "bank_rejection_reason_text", "" );
		} else {
			bankTransactionUpdate.newObject();
			bankTransactionUpdate.setAttribute( "transaction_oid", transactionOid );
		}				
		bankTransactionUpdate.setAttribute( "bank_update_status", TradePortalConstants.BANK_TRANS_UPDATE_STATUS_NOT_STARTED );		
	    if ( imageDoc != null ) {
				int diXMLCount = 0;
				long dioid = 0;
				DocumentImage documentImage = null;
				Integer IdiXMLCount = new Integer(imageDoc.getAttribute("/TotalNumberOfEntries"));
				diXMLCount = IdiXMLCount.intValue();
				String image_id = null;
				if(diXMLCount > 0)
				{
					List<String> imageIds = new ArrayList<String>();
					for (int diXMLIndex = 0; diXMLIndex < diXMLCount; diXMLIndex++)
					{
						mediatorServices.debug("This is the id of the DocImage " + dioid);
						/*Check if a document image with ImageID is already existing*/
						image_id =  imageDoc.getAttribute("/FullDocumentImage(" + diXMLIndex + ")/ImageID");
						imageIds.add(image_id);
					    if(!doesDocumentImageExist( bankTransactionUpdate, mediatorServices, image_id ) )
							{
								dioid = bankTransactionUpdate.newComponent("DocumentImageList");
								documentImage = (DocumentImage)bankTransactionUpdate.getComponentHandle("DocumentImageList",dioid);
								unpackageDocumentImageAttributes(documentImage, diXMLIndex, imageDoc, mediatorServices);
								mediatorServices.debug("TPLUnPackager: DocumentImage is updated with Document_Image_Oid## " + dioid );
							}
						}
					if ( StringFunction.isNotBlank(bankTransUpdateOid) ) { // if it is a re-authorize transaction from customer
						removeCustomerDeletedAttachment( bankTransUpdateOid, imageIds, mediatorServices );
					}
				} else {
					mediatorServices.debug("No Document items to update");
					if ( StringFunction.isNotBlank(bankTransUpdateOid) ) { // if it is a re-authorize transaction from customer
						// delete all attachment from bank transaction which has been deleted from customer
						deleteAllAttachmentFromBankTransaction( bankTransUpdateOid, mediatorServices );
					}
				}
			}
			
		int success = bankTransactionUpdate.save(false);
		if ( success < 0 ) {
			mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,
					TradePortalConstants.TASK_NOT_SUCCESSFUL," unable to create transaction update for Bank Admin User");
		}
	}
	
	/**
	 * This method is used to delete all attachment which is not present at customer side.
	 * @param bankTransUpdateOid
	 * @param imageIds
	 * @param mediatorServices
	 * @throws AmsException 
	 */
	private static void removeCustomerDeletedAttachment( String bankTransUpdateOid, List<String> imageIds, MediatorServices mediatorServices ) 
			throws AmsException {
		
		if ( imageIds.size() > 0 ) {
			StringBuilder sql = new StringBuilder(" UPDATE document_image SET mail_message_oid =  ?, p_transaction_oid = ? "
					+ " WHERE p_transaction_oid = ? AND image_id not in (");
			List<Object> sqlParamList = new ArrayList<Object> ();
			sqlParamList.add(bankTransUpdateOid);
			sqlParamList.add("0");
			sqlParamList.add(bankTransUpdateOid);
			int i = 0;
			for ( String imageID : imageIds ) {
				sqlParamList.add(imageID);
				if ( i == 0 ) {
				   sql.append("?");
				} else {
				   sql.append(",?");	
				}
				i++;
			}
			sql.append(") ");			
			try{
		          DatabaseQueryBean.executeUpdate( sql.toString(), true, sqlParamList );
		    } catch ( SQLException e ) {
		          mediatorServices.debug("Error occured while removing customer attached document");
		          throw new AmsException(e);
		    }
		}
		
	}

	/**
	 * This method is used to delete all attachment for given transaction
	 * @param bankTransUpdateOid
	 * @param mediatorServices
	 * @throws AmsException
	 */
	private static void deleteAllAttachmentFromBankTransaction( String bankTransUpdateOid, MediatorServices mediatorServices ) throws AmsException {

        String sql = " UPDATE document_image SET mail_message_oid =  ?, p_transaction_oid = ? WHERE p_transaction_oid = ?";
        try{
          DatabaseQueryBean.executeUpdate( sql, true, new Object[] { bankTransUpdateOid, 0, bankTransUpdateOid } );
        } catch ( SQLException e ) {
        	mediatorServices.debug("Error occured while deleting Attached document from Bank update centre");
        	throw new AmsException(e);
        }
		
	}

	/**
	 * This method takes the bankTransactionUpdate object
	 * and checks if a particular DocumentImage Components Exist
	 * @param transaction, Transaction object
	 * @param mContext, SessionContext representing the present context.
	 * @param docImageID, String representin the ID on image server. Added with 3.2
	 * author iv
	 */
	public static boolean doesDocumentImageExist(BankTransactionUpdate bankTransactionUpdate, MediatorServices mediatorServices,String docImageID)
	throws RemoteException, AmsException
	{
		boolean doesExist = false;
		String whereClause = " p_transaction_oid = ? AND image_id = ? ";
		int count = DatabaseQueryBean.getCount( "doc_image_oid", "document_image", whereClause, true, new Object[]{ 
				bankTransactionUpdate.getAttribute("bank_transaction_update_oid"), docImageID} );
		
		if (count > 0)
		{
			doesExist =  true;
		}
		return doesExist;
	}
	
	/**
	 * This method takes the DocumentImage object
	 * and populates the database with values from DocumentImage section
	 * of the unpackagerDoc
	 * @param documentImage, DocumentImage object
	 * @param id, int value representing the documentImage
	 * @param transactionIndex, int value representing the transaction.
	 * @param unpackagerDoc, DocumentHandler object with the XML
	 * @param mediatorServices, MediatorServices
	 * author iv
	 */
	public static void unpackageDocumentImageAttributes(DocumentImage documentImage, int id, DocumentHandler imageDoc, MediatorServices mediatorServices )
	throws RemoteException, AmsException
	{

		documentImage.setAttribute( "form_type", TradePortalConstants.DOC_IMG_FORM_TYPE_PORTAL_ATTACHED );
		documentImage.setAttribute("image_id",  imageDoc.getAttribute("/FullDocumentImage(" + id + ")/ImageID"));
		documentImage.setAttribute("num_pages", imageDoc.getAttribute("/FullDocumentImage(" + id + ")/NumPages"));
		documentImage.setAttribute("image_format", imageDoc.getAttribute("/FullDocumentImage(" + id + ")/ImageFormat"));
		documentImage.setAttribute("image_bytes", imageDoc.getAttribute("/FullDocumentImage(" + id + ")/ImageBytes"));
		documentImage.setAttribute("doc_name", imageDoc.getAttribute("/FullDocumentImage(" + id + ")/DocName"));
		String imageHash = imageDoc.getAttribute("/FullDocumentImage(" + id + ")/Hash");
		if ( imageHash != null && imageHash.length()>0 ) {
		    documentImage.setAttribute( "hash", imageHash );
		}
		mediatorServices.debug("Done updating the DocumentImage object number : " + id);
	}
	
	
}
