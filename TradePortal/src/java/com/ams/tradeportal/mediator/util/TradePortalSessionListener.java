package com.ams.tradeportal.mediator.util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Iterator;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import javax.servlet.http.*;

import com.ams.tradeportal.busobj.webbean.SessionWebBean;
import com.ams.tradeportal.busobj.webbean.SessionWebBean.RecentInstrumentRef;
import com.ams.tradeportal.common.*;
import com.amsinc.ecsg.util.*;
import com.amsinc.ecsg.frame.*;


import com.ams.tradeportal.mediator.*;import com.amsinc.ecsg.web.*;

/*
 * Listener which is used to notify the application when the session 
 * times out.  Causes the logout mediator to be called.
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */

public class TradePortalSessionListener implements HttpSessionBindingListener, java.io.Serializable {
private static final Logger LOG = LoggerFactory.getLogger(TradePortalSessionListener.class);
	
    private java.lang.String serverLocation = "";
    private boolean isSessionTimeoutBasedLogout = true;
    private DocumentHandler logoutDoc = null;
    private String userOid = null;
	//cquinton 2/16/2012 Rel 8.0 ppx 255 start
    private String activeUserSessionOid = null;
	//cquinton 2/16/2012 Rel 8.0 ppx 255 end
    public  com.venetica.vbr.client.Repository vbrRepository = null;
    public  com.venetica.vbr.client.User vbrUser = null;
    
    //cquinton 12/13/2012 keep reference to recent instruments here 
    // so we can save them on logout
    private Queue<SessionWebBean.RecentInstrumentRef> recentInstrumentsQueue = null;
    public void setRecentInstrumentsQueue( Queue<SessionWebBean.RecentInstrumentRef> queue ) {
        recentInstrumentsQueue = queue;
    }
    public Queue<SessionWebBean.RecentInstrumentRef> getRecentInstrumentsQueue() {
        return recentInstrumentsQueue;
    }
    


    public TradePortalSessionListener() {
    }

    public void valueBound(HttpSessionBindingEvent event) {
    }


    /**
     * This method is kicked off when the session is unbound.  When this occurs
     * we want to perform a logout.
     * 
     * @param event HttpSessionBindingEvent - the event causing the unbinding
     */
    public void valueUnbound(HttpSessionBindingEvent event) {
    if (this.isSessionTimeoutBasedLogout) {
        //cquinton 2/16/2012 Rel 8.0 ppx255 start
        //unlock instruments for the user if the timeout is for an active user session
        if ( isActiveUserSession() ) {
            try {
                // Unlock any instruments that the user has locked
                LockingManager.unlockBusinessObjectByUser(getUserOid(), false);
            } catch (Exception e) {
                LOG.info("Error during " 
                               + "TradePortalSessionListener.valueUnbound(): "
                               + e.toString()); 
            }
            
            //cquinton 12/13/2012
            //put user preferences in the logout doc
            
            //the recent instruments
            Queue<SessionWebBean.RecentInstrumentRef> recentsQ = getRecentInstrumentsQueue();
            if ( recentsQ != null ) {
                int i = 0;
                for ( Iterator<SessionWebBean.RecentInstrumentRef> iter = recentsQ.iterator();
                      iter.hasNext(); i++) {
                    SessionWebBean.RecentInstrumentRef recentInstr = iter.next();
                    
                    logoutDoc.setAttribute("/In/UserPrefs/RecentInstrument("+i+")/transactionOid", 
                        recentInstr.getTransactionOid());
                    logoutDoc.setAttribute("/In/UserPrefs/RecentInstrument("+i+")/instrumentId", 
                        recentInstr.getInstrumentId());
                    logoutDoc.setAttribute("/In/UserPrefs/RecentInstrument("+i+")/transactionType", 
                        recentInstr.getTransactionType());
                    logoutDoc.setAttribute("/In/UserPrefs/RecentInstrument("+i+")/tranactionStatus", 
                        recentInstr.getTransactionStatus());
                }
            }
        }
        //cquinton 2/16/2012 Rel 8.0 ppx255 end
              
        try
        {
            
            LogoutMediator myMediator = (LogoutMediator) 
                          EJBObjectFactory.createClientEJB(getServerLocation(),
                                                   "LogoutMediator");
            DocumentHandler output =  myMediator.performService(getLogoutDoc());

            myMediator.remove();

        } catch (Exception e) {
            LOG.info("Error during " 
                           + "TradePortalSessionListener.valueUnbound(): "
                           + e.toString()); 
        }

    }
    //Rpasupulati #T36000013473 VeniceBridge commenting start. 
//    try {
//        if (vbrRepository != null)
//        {
//            vbrRepository.logoff();
//        }
//        else
//        {
//            LOG.debug("There is no Venice Bridge repository");
//        }
//    } catch (Exception e) { 
//        LOG.info("VeniceBridge Logoff Failed");
//        e.printStackTrace(); 
//    }
//
//    try {
//        if (vbrUser != null)
//        {
//            vbrUser.freeInstance();       
//        }
//        else
//        {
//            LOG.debug("There is no Venice Bridge user");
//        }
//    } catch (Exception e) { 
//        LOG.info("Venice Bridge Free Instance Failed");
//        e.printStackTrace(); 
//    }
    //Rpasupulati #T36000013473 VeniceBridge commenting end. 
}

    //cquinton 2/16/2012 Rel 8.0 ppx 255 start
    private boolean isActiveUserSession() {
        boolean isActiveSession = false;

        //Note: if activeusersession has not yet been set
        // at logon success or relogon success, we don't have an active user session
        // so no resources to release
        
        if ( activeUserSessionOid != null ) {
            //now check to see if the user session has been forced to logout
            //if so its not actually an active user session, we're just
            // waiting for it to timeout
            try {
                //directly check the database to avoid ejb caching
                String mySql = "select force_logout from active_user_session where active_user_session_oid=?";
                DocumentHandler myResult = DatabaseQueryBean.getXmlResultSet(mySql, false, new Object[]{activeUserSessionOid});
                if( myResult!=null ) {
                    String forceLogoutInd = 
                        myResult.getAttribute("/ResultSetRecord(0)/FORCE_LOGOUT");
                    if ( !TradePortalConstants.INDICATOR_YES.equals( forceLogoutInd ) ) {
                        isActiveSession = true;
                    }
                }
            } catch ( Exception ex ) {
                System.err.println("Problem checking if user session is active. User instrument locks will not be released. " + ex.toString());
                ex.printStackTrace();
            }
        }
        return isActiveSession;
    }
    //cquinton 2/16/2012 Rel 8.0 ppx 255 start


/**
 * Getter for the serverLocation attribute
 * 
 * @return java.lang.String
 */
public java.lang.String getServerLocation() {
	return serverLocation;
}


/**
 * Setter for the inputDoc sent to logout mediator
 *
 * @param newLogoutDoc com.amsinc.ecsg.util.DocumentHandler
 */
public void setLogoutDoc(com.amsinc.ecsg.util.DocumentHandler newLogoutDoc) {
	logoutDoc = newLogoutDoc;
}


/**
 * Setter for the serveLocation attribute
 * 
 * @param newServerLocation java.lang.String
 */
public void setServerLocation(java.lang.String newServerLocation) {
	serverLocation = newServerLocation;
}

/**
 * Setter for the userOid
 *
 * @param userOid 
 */
public void setUserOid(String userOid)
 {
      this.userOid = userOid;
 }

/**
 * Getter for the user oid 
 *
 * @return long
 */
public long getUserOid()
 {
      return Long.parseLong(this.userOid);
 }
   
   //cquinton 2/16/2012 Rel 8.0 ppx255 start 
    /**
     * Setter for the activeUserSessionOid
     *
     * @param activeUserSessionOid 
     */
    public void setActiveUserSessionOid(String activeUserSessionOid)
    {
        this.activeUserSessionOid = activeUserSessionOid;
    }
    
    /**
     * Getter for the activeUserSession oid 
     *
     * @return long
     */
    public long getActiveUserSessionOid()
    {
        return Long.parseLong(this.activeUserSessionOid);
    }
   //cquinton 2/16/2012 Rel 8.0 ppx255 end
    
    
    /**
 * Getter for the inputDoc sent to logout mediator
 * 
 * @return com.amsinc.ecsg.util.DocumentHandler
 */
public DocumentHandler getLogoutDoc() {
	return logoutDoc;
}
}