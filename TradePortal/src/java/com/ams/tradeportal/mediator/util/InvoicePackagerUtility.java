package com.ams.tradeportal.mediator.util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.MissingResourceException;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import com.ams.tradeportal.busobj.util.InstrumentServices;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.MediatorServices;
import com.amsinc.ecsg.frame.ObjectIDCache;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.GMTUtility;

/*
 * Manages INV Message Packaging..
 *
 *     Copyright  © 2008
 *     American Management Systems, Incorporated
 *     All rights reserved
 */

public class InvoicePackagerUtility  {
private static final Logger LOG = LoggerFactory.getLogger(InvoicePackagerUtility.class);
	protected static final PropertyResourceBundle properties = (PropertyResourceBundle) ResourceBundle
			.getBundle("AgentConfiguration");
	
	/*
	 * This utility class is used by INMPackagerMediator and BULKINVMediator.
	 * gets the invoices_summary_data OID as input, and determines the number of inv message to be created based
	 * on MaxLineItems allowed per INV message.
	 * Then creates corresponding INV messages.
	 */

	public static DocumentHandler create(DocumentHandler inputDoc, MediatorServices mediatorServices,String processParameters,PreparedStatement insStmt,Connection con) {
		try {

			String invoiceOid = getParm("InvoiceOID", processParameters);
			String grpRefNum= getParm("GroupReferenceNumber",processParameters);
			String groupTotalNo= getParm("GroupTotalNo",processParameters);
			String invType = getParm("invoiceType", processParameters);
			String creditNoteInd = getParm("creditNoteInd", processParameters);
			String creditNotePreApplicationStatus = null;
			String creditNoteUpdateInd = null;
			String credAppliedInvDetailOid = null;
			if(TradePortalConstants.INDICATOR_YES.equals(creditNoteInd)){
				creditNotePreApplicationStatus = getParm("creditNotePreApplicationStatus", processParameters);
				creditNoteUpdateInd = getParm("creditNoteUpdateInd", processParameters);
				credAppliedInvDetailOid = getParm("credAppliedInvDetailOid", processParameters);
			}
			mediatorServices
					.debug("Ready to Instantiate InvoicesSummaryData Object with oid "
							+ invoiceOid);
			String maxLinItem = "";
			try {
				maxLinItem = properties.getString("maxLinItemsAllowed");
				System.out.print("maxLinItemsAllowed is configured for " +maxLinItem);
			} catch (MissingResourceException e) {
				maxLinItem = "500";
				System.out.print("maxLinItemsAllowed is Not Configured so default to  " +maxLinItem);
			}

			int maxLinItemAllowed = Integer.parseInt(maxLinItem.trim()); // IR T36000016545 

			String sql = null;
			if(TradePortalConstants.INDICATOR_YES.equals(creditNoteInd)){
			   sql = "select upload_credit_line_item_oid from credit_notes_line_items where p_credit_notes_goods_oid in "
					+ "( select upload_credit_goods_oid from credit_notes_goods where p_credit_note_oid= ? )"
					+ " order by upload_credit_line_item_oid";
			}else {
				sql = "select upload_line_item_detail_oid from invoice_line_item_detail where p_invoice_summary_goods_oid in "
						+ "( select upload_invoice_goods_oid from invoice_summary_goods where p_invoice_summary_data_oid= ? )"
						+ " order by upload_line_item_detail_oid";
			}
			try (PreparedStatement pStmt = con.prepareStatement(sql,
					ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_READ_ONLY)){
			pStmt.setObject(1, invoiceOid);
			try (ResultSet rs = pStmt.executeQuery()){

			// Get total number of records in the resultset
			rs.last();
			int lineItemFinalTot = rs.getRow();

			// Get total number of events to be created
			Integer totalNoOfMessages = lineItemFinalTot / maxLinItemAllowed;

			// If totalInvoiceCount is not divisible by NoOfInvoicesPerMessage,
			// there should be one extra event created.
			if ((lineItemFinalTot % maxLinItemAllowed) > 0) {
				totalNoOfMessages++;
			}
			// Set resultset to before first row
			rs.beforeFirst();

			// Create INV events
			Integer totalNoOfMessagesCreated = 0;
			String startLineItemUoid = null;
			String endLineItemUoid = null;
			String processParameter = "";
			while (rs.next()) {
				startLineItemUoid = rs.getString(1);
				// Try to get (current+ NoOfInvoicesPerMessage - 1)th row, if it
				// is not valid, get last row.
				if (rs.relative(maxLinItemAllowed - 1)) {
					endLineItemUoid = rs.getString(1);
				} else if (rs.last()) {
					endLineItemUoid = rs.getString(1);
				}
				++totalNoOfMessagesCreated;

				processParameter = "|InvoiceOID = " + invoiceOid
						+ "|invoiceType =" + invType
						+ "|GroupReferenceNumber =" + grpRefNum
						+ "|GroupTotalNo =" + groupTotalNo
						+ "|StartLineItemOID =" + startLineItemUoid
						+ "|EndLineItemOID =" + endLineItemUoid
						+ "|Sequence = " + totalNoOfMessagesCreated + '/'
						+ totalNoOfMessages;
				if(TradePortalConstants.INDICATOR_YES.equals(creditNoteInd)){
					processParameter = processParameter + "|creditNoteInd ="+ creditNoteInd
							+ "|creditNotePreApplicationStatus ="+ creditNotePreApplicationStatus
							+ "|creditNoteUpdateInd ="+ creditNoteUpdateInd
							+ "|credAppliedInvDetailOid ="+ credAppliedInvDetailOid;
				}
				createINVMessage(inputDoc, processParameter, mediatorServices,insStmt);
			}
		   }
		  }
		} catch (Exception e) {
			LOG.info("exception in InvoicePackagerUtility" + e);
		}
		return inputDoc;
	}


	public static String getParm(String parmName, String processingParms) {

		int locationOfParmName = processingParms.indexOf(parmName);

		// Search for the whole word only.
		// For example, getParm("portfolio_activity_uoid=0|activity=1",
		// "activity" should return 1.
		// Keep searching if the character before or after the occurrence is not
		// a word delimiter

		while ((locationOfParmName > 0
				&& processingParms.charAt(locationOfParmName - 1) != ' ' && processingParms
				.charAt(locationOfParmName - 1) != '|')
				|| (locationOfParmName >= 0
						&& locationOfParmName < processingParms.length()
								- parmName.length()
						&& processingParms.charAt(locationOfParmName
								+ parmName.length()) != ' ' && processingParms
						.charAt(locationOfParmName + parmName.length()) != '=')) {

			locationOfParmName = processingParms.indexOf(parmName,
					locationOfParmName + parmName.length());

		}

		if (locationOfParmName == -1) {
			return "";
		}

		int startOfParmValue = processingParms.indexOf('=', locationOfParmName);

		if (startOfParmValue == -1) {
			return "";
		}

		startOfParmValue++;

		int endOfParmValue = processingParms.indexOf('|', startOfParmValue);

		if (endOfParmValue == -1) {
			endOfParmValue = processingParms.length();
		}

		String parmValue = processingParms.substring(startOfParmValue, endOfParmValue);

		return parmValue;
	}


	/**
	 * This method Creates INV message.
	 *
	 * @param inputDoc
	 * @param xmlStr
	 * @param mediatorServices
	 * @throws AmsException
	 * @throws RemoteException
	 */
	private static void createINVMessage(DocumentHandler inputDoc, String xmlStr,
			MediatorServices mediatorServices,PreparedStatement insStmt) throws AmsException {

		String transactionOid = inputDoc.getAttribute("/transaction_oid");
		String instrumentOid = inputDoc.getAttribute("/instrument_oid");
		
		Date date = GMTUtility.getGMTDateTime(false);
		Timestamp time = new Timestamp(date.getTime());
		ObjectIDCache oid = ObjectIDCache.getInstance(AmsConstants.OID);

		try{
			insStmt.setLong(1,oid.generateObjectID());
			insStmt.setTimestamp(2, time);
			insStmt.setString(3, instrumentOid);
			insStmt.setString(4,transactionOid);
			insStmt.setString(5,InstrumentServices.getNewMessageID());
			insStmt.setString(6,xmlStr);
			insStmt.addBatch();
		
		}
		catch(SQLException e){
			throw new AmsException(e.getMessage());
		}
		
	}

}
