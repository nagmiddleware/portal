package com.ams.tradeportal.mediator.util;
import java.math.BigDecimal;
import java.rmi.RemoteException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.busobj.CorporateOrganization;
import com.ams.tradeportal.busobj.Instrument;
import com.ams.tradeportal.busobj.Terms;
import com.ams.tradeportal.busobj.Transaction;
import com.ams.tradeportal.common.InstrumentType;
import com.ams.tradeportal.common.TPDateTimeUtility;
import com.ams.tradeportal.common.TradePortalConstants;
import com.ams.tradeportal.common.TransactionStatus;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.MediatorServices;
import com.amsinc.ecsg.util.GMTUtility;
import com.amsinc.ecsg.util.StringFunction;


/**
 * Services common to unpackagers.
 */
public class UnPackagerServices
{
private static final Logger LOG = LoggerFactory.getLogger(UnPackagerServices.class);

	private UnPackagerServices ()
	{
	}
    
    /**
     * This method takes the transaction object
     * and updates the account balances
     * @param transaction, Transaction object
     * @param amount, transaction amount
     * @param currency, transaction currency
     * @returns boolean - resubmit flag - true if message needs to be resubmitted
     *     false in normal successful processing case
     */
	//Leelavathi - CR-610 - Rel 7.0.0 - 15-Mar-2011 - Start
	//rename updateAccountBalances to updateLimits
    public static boolean updateLimits(
            Instrument instrument, String origTransactionStatus,
            Transaction transaction, boolean rejectedTransaction,
            java.util.Date originalTransactionStatusDate, MediatorServices mediatorServices)
        throws RemoteException, AmsException
    {
        //note - this is not for general success/failure - only return true if
        // message needs to be retried
        boolean resubmit = false; 

        //Check to make sure OriginalStatus is Authorized for updating.
        //if(origTransactionStatus!= null && origTransactionStatus.equals(TradePortalConstants.TRANS_STATUS_AUTHORIZED)){      // IR# RVUL011972931 - 03/30/11 
        if(TransactionStatus.AUTHORIZED.equals(origTransactionStatus) || TransactionStatus.TRANSACTION_PROCESSING.equals(origTransactionStatus)){    // IR# RVUL011972931 - 03/30/11	
            int retryAttempt = 1;


            while (retryAttempt <= TradePortalConstants.MAX_CORP_ORG_RETRY)
            {
                LOG.debug("updateAccountBalances::retry is " + retryAttempt);


                CorporateOrganization corpOrg =(CorporateOrganization) mediatorServices.createServerEJB("CorporateOrganization",
                        Long.parseLong(instrument.getAttribute("corp_org_oid")));
                Terms terms = (Terms)(transaction.getComponentHandle("CustomerEnteredTerms"));

                // IAZ ER 5.0.1.1 06/29/09 Begin
                // In case this is Int Pmt and debit account is specified as Other, skip updating account
                // balance.  Otherwise, update accounts and set flag to make sire user corp org is also
                // updated
                boolean updateOrg = false;
                if ((!InstrumentType.FUNDS_XFER.equals(instrument.getAttribute("instrument_type_code")))
                        ||(StringFunction.isNotBlank(terms.getAttribute("debit_account_oid"))))
                {

                    updateOrg = true;
                    // IAZ ER 5.0.1.1 06/29/09 End
					//Leelavathi - CR-610 - Rel 7.0.0 - 21-Feb-2011 - Begin

                    

                    if(rejectedTransaction){
                        String opOrgDailyLimitDate = corpOrg.getAttribute("cust_daily_balance_date");
                        //IR PAUJ032377779 check if cust daily balance date is blank
                        if(!(opOrgDailyLimitDate == null || opOrgDailyLimitDate.equals(""))){

                            //IAZ 06/15/09 Begin
                            //if(opOrgDailyLimitDate.substring(0,10).equals(originalTransactionStatusDate.substring(0,10))) {

                            // IAZ ER 5.0.1.1 06/29/09 Begin - Delete TDDTU constructor - the method to call is static
                            if(TPDateTimeUtility.compareDatesInGivenTimeZone(originalTransactionStatusDate, GMTUtility.getGMTDateTime(),
                                    // IAZ ER 5.0.1.1 06/29/09 End
                                    corpOrg.getAttribute("timezone_for_daily_limit")) == 0) {
                                updateOrg = updateDailyLimitAmount( corpOrg, instrument.getAttribute("instrument_type_code"), terms, mediatorServices);
                            }

                            //IAZ 06/15/09 End
                        }
                    }
					//Leelavathi - CR-610 - Rel 7.0.0 - 21-Feb-2011 - End

					// IAZ ER 5.0.1.1 06/29/09 Begin
				}
				//09/09/09 IR MMUJ062173944 Begin
				else
				{
					break;
				}
				//09/09/09 IR MMUJ062173944 End
				//If the origTransactionStatusDate is same as transaction status date, then update dailyLimitAccount as well
				LOG.debug("Is this rejectedTransaction" + rejectedTransaction);
				//boolean updateOrg = true;
				// IAZ ER 5.0.1.1 06/29/09 End


                LOG.debug("updateOrg " + updateOrg);
                // update accounts
                //corpOrg.save();
                if (updateOrg){
                    corpOrg.setRetryOptLock(true);
                    corpOrg.touch();
                    int updateStatus = 0;

                    LOG.debug("Opt Lock is " + true);
                    try {
                        updateStatus = corpOrg.save();
                    }

                    catch (Exception e) {
                        instrument.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,TradePortalConstants.TASK_NOT_SUCCESSFUL,"Saving balances on CorpOrg While Unpackaging");
                        e.printStackTrace();
                        break;
                    }
                    if ( updateStatus == 1  ) break;

                    if ( updateStatus == -1  ){
                        instrument.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,TradePortalConstants.TASK_NOT_SUCCESSFUL,"Locking Issue. Saving balances on CorpOrg While Unpackaging. updateStatus is:" + updateStatus);
                        break;
                    }

                    if ( updateStatus == -2 ){
                        //  instrument.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,TradePortalConstants.TASK_NOT_SUCCESSFUL,"Locking Issue. Saving balances on CorpOrg While Unpackaging. updateStatus is:" + updateStatus);
                        retryAttempt++;
                        LOG.debug("updateStatus " + updateStatus + " RetryAttempt:" + retryAttempt);
                        if( retryAttempt > TradePortalConstants.MAX_CORP_ORG_RETRY ) {
                            //resubmitMsg = true;
                            resubmit = true;
                        }
                    }
                    //09/09/09 IR MMUJ062173944 Begin
                    else
                    {
                        instrument.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,TradePortalConstants.TASK_NOT_SUCCESSFUL,"Saving balances on CorpOrg While Unpackaging");
                        break;
                    }
                    //09/09/09 IR MMUJ062173944 End

                    LOG.debug("updateStatus " + updateStatus);
                }
                //09/09/09 IR MMUJ062173944 Begin
                //else instrument.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,TradePortalConstants.TASK_NOT_SUCCESSFUL,"Saving balances on CorpOrg While Unpackaging");

                else
                {
                    instrument.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,TradePortalConstants.TASK_NOT_SUCCESSFUL,"Saving balances on CorpOrg While Unpackaging");
                    break;
                }
                //09/09/09 IR MMUJ062173944 End
            }

        }
        
        return resubmit;
    }
  //Leelavathi - CR-610 - Rel 7.0.0 - 15-Mar-2011 - End
  
    public static boolean updateDailyLimitAmount( CorporateOrganization corpOrg, 
            String instrumentTypeCode, Terms terms, MediatorServices mediatorServices)
        throws RemoteException, AmsException
    {

        BigDecimal debit_amt = terms.getAttributeDecimal("debit_pending_amount_in_base");
        String debit_amt_base_string = terms.getAttribute("debit_pending_amount_in_base");
        LOG.debug("debit_amt_base_string " + debit_amt_base_string);


        BigDecimal dailyBalanceAmount;
        String dailyBalanceAmountString;
        if (instrumentTypeCode.equals(InstrumentType.DOM_PMT)){
            if (TradePortalConstants.INDICATOR_YES.equals(corpOrg.getAttribute("enforce_daily_limit_dmstc_pymt"))) {
                if (debit_amt_base_string == null || debit_amt_base_string.equals("") ){
                    terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE , TradePortalConstants.VALUE_MISSING,"debit_pending_amount_in_base", "Terms: " + terms.getAttribute("terms_oid") );
                    return false;
                }
                dailyBalanceAmountString = corpOrg.getAttribute("cust_daily_balance_amt_ftdp");
                if (dailyBalanceAmountString == null || dailyBalanceAmountString.equals("") ) {
                    terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE , TradePortalConstants.VALUE_MISSING,"cust_daily_balance_amt_ftdp", "at customer: " + corpOrg.getAttribute("Proponix_customer_id") );
                    return false;
                }
                dailyBalanceAmount = corpOrg.getAttributeDecimal("cust_daily_balance_amt_ftdp");
                //  if (dailyBalanceAmount == null) dailyBalanceAmount = BigDecimal.ZERO;
                LOG.debug("daily Limit Dmstc payment: " + dailyBalanceAmount + "  -- debit amount: " + debit_amt);
                //update debit pending balances
                dailyBalanceAmount=dailyBalanceAmount.subtract(debit_amt);
                corpOrg.setAttribute("cust_daily_balance_amt_ftdp", dailyBalanceAmount.toString());
                LOG.debug("daily Limit Dmstc payment: " + dailyBalanceAmount);
            }

        }
        if (instrumentTypeCode.equals(InstrumentType.XFER_BET_ACCTS)){
            if (TradePortalConstants.INDICATOR_YES.equals(corpOrg.getAttribute("enforce_daily_limit_transfer"))) {
                if (debit_amt_base_string == null || debit_amt_base_string.equals("") ){
                    terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE , TradePortalConstants.VALUE_MISSING,"debit_pending_amount_in_base", "Terms: " + terms.getAttribute("terms_oid") );
                    return false;
                }
                dailyBalanceAmountString = corpOrg.getAttribute("cust_daily_balance_amt_ftba");
                dailyBalanceAmount = corpOrg.getAttributeDecimal("cust_daily_balance_amt_ftba");
                if (dailyBalanceAmountString == null || dailyBalanceAmountString.equals("") ) {
                    terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE , TradePortalConstants.VALUE_MISSING,"cust_daily_balance_amt_ftba", "at customer: " + corpOrg.getAttribute("Proponix_customer_id") );
                    return false;
                }
                //  if (dailyBalanceAmount == null) dailyBalanceAmount = BigDecimal.ZERO;
                LOG.debug("daily Limit Transfer between Accounts: " + dailyBalanceAmount + "  -- debit amount: " + debit_amt);
                //update debit pending balances
                dailyBalanceAmount=dailyBalanceAmount.subtract(debit_amt);
                corpOrg.setAttribute("cust_daily_balance_amt_ftba", dailyBalanceAmount.toString());
                LOG.debug("daily Limit Transfer between Accounts:" + dailyBalanceAmount);
            }

        }
        if (instrumentTypeCode.equals(InstrumentType.FUNDS_XFER)){
            if (TradePortalConstants.INDICATOR_YES.equals(corpOrg.getAttribute("enforce_daily_limit_intl_pymt"))) {
                if (debit_amt_base_string == null || debit_amt_base_string.equals("") ){
                    terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE , TradePortalConstants.VALUE_MISSING,"debit_pending_amount_in_base", "Terms: " + terms.getAttribute("terms_oid") );
                    return false;
                }
                dailyBalanceAmountString = corpOrg.getAttribute("cust_daily_balance_amt_ftrq");
                dailyBalanceAmount = corpOrg.getAttributeDecimal("cust_daily_balance_amt_ftrq");
                if (dailyBalanceAmountString == null || dailyBalanceAmountString.equals("") ) {
                    terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE , TradePortalConstants.VALUE_MISSING,"cust_daily_balance_amt_ftrq", "at customer: " + corpOrg.getAttribute("Proponix_customer_id") );
                    return false;
                }

                LOG.debug("dailyBalanceAmount " + dailyBalanceAmount);
                //      if (dailyBalanceAmount == null) dailyBalanceAmount = BigDecimal.ZERO;
                LOG.debug("daily Limit International payment: " + dailyBalanceAmount + "  -- debit amount: " + debit_amt);
                //update debit pending balances
                dailyBalanceAmount=dailyBalanceAmount.subtract(debit_amt);
                LOG.debug("New dailyBalanceAmount " + dailyBalanceAmount.toString());
                corpOrg.setAttribute("cust_daily_balance_amt_ftrq", dailyBalanceAmount.toString());
                LOG.debug("daily Limit International payment " + dailyBalanceAmount.toString());
            }
        }

        return true;


    }

}