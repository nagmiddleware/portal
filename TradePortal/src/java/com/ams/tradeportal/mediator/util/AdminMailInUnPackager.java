package com.ams.tradeportal.mediator.util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.RemoteException;

import com.ams.tradeportal.busobj.BankMailMessage;
import com.ams.tradeportal.busobj.DocumentImage;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.frame.MediatorServices;
import com.amsinc.ecsg.util.DateTimeUtility;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.StringFunction;

public class AdminMailInUnPackager {
private static final Logger LOG = LoggerFactory.getLogger(AdminMailInUnPackager.class);

	public static void process ( DocumentHandler MailMessage, MediatorServices mediatorServices ) throws RemoteException, AmsException {
		
		BankMailMessage bankMailMessage = ( BankMailMessage ) mediatorServices.createServerEJB("BankMailMessage");
		bankMailMessage.newObject();
		String assignedToCorpOrgOid = "";
		String sqlStatement = " SELECT organization_oid FROM  corporate_org WHERE  PROPONIX_CUSTOMER_ID = ? AND ACTIVATION_STATUS = ? ";
		DocumentHandler resultXML = DatabaseQueryBean.getXmlResultSet(sqlStatement, true, 
				new Object[]{ MailMessage.getAttribute("/Proponix/SubHeader/CustomerID"), "ACTIVE"});
		if(resultXML == null)
		{
			mediatorServices.debug("No customerID ");

		} else {
			assignedToCorpOrgOid = resultXML.getAttribute("/ResultSetRecord(0)/ORGANIZATION_OID");
		}
		
		bankMailMessage.setAttribute( "assigned_to_corp_org_oid", assignedToCorpOrgOid );
		bankMailMessage.setAttribute( "message_subject", MailMessage.getAttribute("/Proponix/Body/MessageSubject") );
		bankMailMessage.setAttribute( "message_text", MailMessage.getAttribute("/Proponix/Body/MessageText") );
		bankMailMessage.setAttribute( "message_source_type", TradePortalConstants.PORTAL );
		bankMailMessage.setAttribute( "message_status", TradePortalConstants.REC );
		bankMailMessage.setAttribute( "unread_flag", TradePortalConstants.INDICATOR_YES );
		bankMailMessage.setAttribute( "is_reply", TradePortalConstants.INDICATOR_NO );
		bankMailMessage.setAttribute( "complete_instrument_id", MailMessage.getAttribute("/Proponix/SubHeader/InstrumentID") );
		bankMailMessage.setAttribute( "last_update_date", DateTimeUtility.getGMTDateTime(false) );
		
		if ( StringFunction.isNotBlank(MailMessage.getAttribute("/Proponix/SubHeader/InstrumentID")) ) {
		   sqlStatement = "SELECT bank_instrument_id FROM instrument WHERE complete_instrument_id = ? AND a_corp_org_oid = ?";
		   resultXML = DatabaseQueryBean.getXmlResultSet(sqlStatement, true, new Object[]{ MailMessage.getAttribute("/Proponix/SubHeader/InstrumentID"), assignedToCorpOrgOid});
		   if( resultXML != null && StringFunction.isNotBlank(resultXML.getAttribute("/ResultSetRecord(0)/BANK_INSTRUMENT_ID")) )
		   {
			   bankMailMessage.setAttribute( "bank_instrument_id", resultXML.getAttribute("/ResultSetRecord(0)/BANK_INSTRUMENT_ID") );

		  }
		}
		String bankMailMessageOid = bankMailMessage.getAttribute("bank_mail_message_oid");		
		int success = bankMailMessage.save();
		
		if ( success != -1 ) {
		   int numberOfDocumentImages = getDocumentImageCount(MailMessage);
		   if (numberOfDocumentImages > 0)
			//Need to unpackage DocumentImages
		   {
			  DocumentImage documentImage = null;
			  for(int dICount = 0; dICount < numberOfDocumentImages; dICount++)
			  {
				documentImage = (DocumentImage)  mediatorServices.createServerEJB("DocumentImage");
				mediatorServices.debug("Before getting documentImage");
				documentImage.newObject();
				unpackageDocumentImageAttributes(documentImage, dICount, MailMessage, bankMailMessageOid, mediatorServices);
				documentImage.save();
				mediatorServices.debug("Done saving DocumentImage " + dICount);
			 }
		   }
		} else {
			mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,
					TradePortalConstants.TASK_NOT_SUCCESSFUL," Unable to create Mail Message for Bank Admin User");
		}
	}
	
	/**
	 * This method determines if the message has any associated document images
	 * @param unpackagerDoc, DocumentHandler
	 *
	 */
	public static int getDocumentImageCount( DocumentHandler MailMessage) throws RemoteException, AmsException
	{
		int numberOfDocumentImages = 0;
		String snumberOfDocumentImages = MailMessage.getAttribute("/Proponix/Body/DocumentImage/NumberOfEntries");

		if( StringFunction.isNotBlank(snumberOfDocumentImages) )
		{
			numberOfDocumentImages = Integer.parseInt(snumberOfDocumentImages);
		}
		return numberOfDocumentImages;
	}
	
	/**
	 * This method takes the DocumentImage object
	 * and populates the database with values from DocumentImage section
	 * of the unpackagerDoc
	 * @param documentImage, DocumentImage object
	 * @param id, int value representing the documentImage
	 * @param unpackagerDoc, DocumentHandler object with the XML
	 * @param mailMessageOid, String value representing the related MailMessage.
	 * @param mediatorServices, MediatorServices
	 * author iv
	 */
	public static void unpackageDocumentImageAttributes(DocumentImage documentImage, int id, DocumentHandler unpackagerDoc, String mailMessageOid,
			MediatorServices mediatorServices) throws RemoteException, AmsException {
		if (unpackagerDoc.getAttribute("/Proponix/Body/DocumentImage/FullDocumentImage(" + id + ")/FormType") == null
				|| unpackagerDoc.getAttribute("/Proponix/Body/DocumentImage/FullDocumentImage(" + id + ")/FormType").equals("")) {
			mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_MIDDLEWARE,
					TradePortalConstants.VALUE_MISSING,
					"FormType",
					"/Proponix/Body/DocumentImage/FullDocumentImage(" + id + ")/FormType");
			return;
		}

		if ("P".endsWith(unpackagerDoc.getAttribute("/Proponix/Body/DocumentImage/FullDocumentImage(" + id + ")/FormType").substring(0,1).toUpperCase())) {
			documentImage.setAttribute("form_type", TradePortalConstants.DOC_IMG_FORM_TYPE_PORTAL_ATTACHED);
		}
		else {
			documentImage.setAttribute("form_type", unpackagerDoc.getAttribute("/Proponix/Body/DocumentImage/FullDocumentImage(" + id + ")/FormType"));
		}
		documentImage.setAttribute("image_id", unpackagerDoc.getAttribute("/Proponix/Body/DocumentImage/FullDocumentImage(" + id + ")/ImageID"));
		documentImage.setAttribute("mail_message_oid", mailMessageOid);
		documentImage.setAttribute("doc_name", unpackagerDoc.getAttribute("/Proponix/Body/DocumentImage/FullDocumentImage(" + id + ")/DocName"));
		documentImage.setAttribute("image_format", unpackagerDoc.getAttribute("/Proponix/Body/DocumentImage/FullDocumentImage(" + id + ")/ImageFormat"));
		documentImage.setAttribute("image_bytes", unpackagerDoc.getAttribute("/Proponix/Body/DocumentImage/FullDocumentImage(" + id + ")/ImageBytes"));
		documentImage.setAttribute("num_pages", unpackagerDoc.getAttribute("/Proponix/Body/DocumentImage/FullDocumentImage(" + id + ")/NumPages"));
		String hash = unpackagerDoc.getAttribute("/Proponix/Body/DocumentImage/FullDocumentImage(" + id + ")/Hash");
		if ( hash != null && hash.length()>0 ) {
		    documentImage.setAttribute("hash", hash);
		}
	}
	
}
