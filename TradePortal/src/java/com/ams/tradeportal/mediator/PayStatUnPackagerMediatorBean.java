package com.ams.tradeportal.mediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.ejb.RemoveException;

import com.ams.tradeportal.agents.TradePortalAgent;
import com.ams.tradeportal.busobj.DomesticPayment;
import com.ams.tradeportal.busobj.Instrument;
import com.ams.tradeportal.busobj.OutgoingInterfaceQueue;
import com.ams.tradeportal.busobj.PaymentBenEmailQueue;
import com.ams.tradeportal.busobj.Transaction;
import com.ams.tradeportal.busobj.util.InstrumentServices;
import com.ams.tradeportal.common.InstrumentType;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.frame.EJBObjectFactory;
import com.amsinc.ecsg.frame.MediatorBean;
import com.amsinc.ecsg.frame.MediatorServices;
import com.amsinc.ecsg.util.DateTimeUtility;
import com.amsinc.ecsg.util.DocumentHandler;
public class PayStatUnPackagerMediatorBean extends MediatorBean {
private static final Logger LOG = LoggerFactory.getLogger(PayStatUnPackagerMediatorBean.class);

    public DocumentHandler execute (DocumentHandler inputDoc, DocumentHandler outputDoc, MediatorServices mediatorServices) 
        throws RemoteException, AmsException
    {
        Transaction        transaction         = null;
        Instrument         instrument          = null;
        DomesticPayment    domesticpayment     = null;
        boolean unpackage = true;
        try
        {

            String opBankOrgProponixID   =  inputDoc.getAttribute("/Proponix/Header/OperationOrganizationID");
            String customerID   =  inputDoc.getAttribute("/Proponix/SubHeader/CustomerID");
            String completeInstrumentId   =  inputDoc.getAttribute("/Proponix/SubHeader/InstrumentID");
            String overallPaymentStatus   =  inputDoc.getAttribute("/Proponix/SubHeader/OverallPaymentStatus");
            String instrumentstatus   =  inputDoc.getAttribute("/Proponix/SubHeader/InstrumentStatus");
            String transactionstatus   =  inputDoc.getAttribute("/Proponix/SubHeader/TransactionStatus");
            String instrumentOid = null;
            String transactionOid = null;
            DocumentHandler beneficiaryDetails = new DocumentHandler();
            Vector beneficiaryDetailsList = inputDoc.getFragments("/Proponix/Body/PaymentBeneficiaryDetails");
            List sequenceNumberList = new ArrayList();
            List paySystemRef = new ArrayList();
            List errorText = new ArrayList();
              
            for (int i = 0; i < beneficiaryDetailsList.size(); i++) {
                beneficiaryDetails = (DocumentHandler)beneficiaryDetailsList.elementAt(i);
                String beneficiarySequenceId = beneficiaryDetails.getAttribute("/BeneficiarySequenceID");

                // TPS send the BeneficiarySequenceID as concatinaton of InstrumentID and sequence number as DPY1426628140-001.taking only sequence number.
                String sequenceNumber = beneficiarySequenceId.substring(beneficiarySequenceId.indexOf('-')+1);
                sequenceNumberList.add(sequenceNumber);
                paySystemRef.add(beneficiaryDetails.getAttribute("/PaymentSystemRef"));
                errorText.add(beneficiaryDetails.getAttribute("/ErrorText"));
                    
            }


            if(opBankOrgProponixID == null || opBankOrgProponixID.equals(""))
            {
                mediatorServices.debug("No opBankOrg to update");
                mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,TradePortalConstants.VALUE_MISSING,"OperationOrganizationID", "/Proponix/Header/OperationOrganizationID");
                unpackage=false;
            }

            if(customerID == null || customerID.equals(""))
            {
                mediatorServices.debug("No customerID to update");
                mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,TradePortalConstants.VALUE_MISSING,"CustomerID", "/Proponix/SubHeader/CustomerID");
                unpackage=false;
            }

            // lookup for the instrument if it is present in TP or not.3.    If the instrument does not exist, send an error if the instrument does not exist for
            //a non-originating transaction.The instrument should always exist as it will have been
            //created in the portal or even if created in the TPS, the UNV would have previously been sent to the Portal and created the instrument.

            // IR# AUL062238375 Rel. 7.0.0 - Begin -
            //String sqlStatementInstrument = "SELECT INSTRUMENT_OID FROM INSTRUMENT WHERE " +
            //    " COMPLETE_INSTRUMENT_ID = '" + completeInstrumentId+ "'" ;
            
            String sqlStatementInstrument = " SELECT INSTRUMENT_OID FROM INSTRUMENT, CORPORATE_ORG WHERE " +
			                                "  A_CORP_ORG_OID = ORGANIZATION_OID AND COMPLETE_INSTRUMENT_ID = ? "+
			                                " AND PROPONIX_CUSTOMER_ID = ?";
            
            // IR# AUL062238375 Rel. 7.0.0 - End -
            
            DocumentHandler resultXML = DatabaseQueryBean.getXmlResultSet(sqlStatementInstrument, false, new Object[]{completeInstrumentId,customerID});

            if(resultXML == null)
            {
                mediatorServices.debug("No completeInstrumentId to update");
                mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,TradePortalConstants.VALUE_MISSING,"InstrumentID", "/Proponix/SubHeader/InstrumentID");
                unpackage=false;
            }
            else{
                instrumentOid = resultXML.getAttribute("/ResultSetRecord(" + 0 + ")/INSTRUMENT_OID");
            }

            // check for the payment status

            if(overallPaymentStatus == null || overallPaymentStatus.equals(""))
            {
                mediatorServices.debug("No overallPaymentStatus");
                mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,TradePortalConstants.VALUE_MISSING,"OverallPaymentStatus", "/Proponix/SubHeader/OverallPaymentStatus");
                unpackage=false;
            }
            else{

                String sqlStatementTransaction = " SELECT TRANSACTION_OID FROM TRANSACTION WHERE  " +
                    " P_INSTRUMENT_OID = ? ";

                resultXML = DatabaseQueryBean.getXmlResultSet(sqlStatementTransaction, false, new Object[]{instrumentOid} );
                if(resultXML != null){
                    transactionOid = resultXML.getAttribute("/ResultSetRecord(" + 0 + ")/TRANSACTION_OID");
                }

                instrument  = (Instrument)  mediatorServices.createServerEJB("Instrument", Long.parseLong(instrumentOid));
                transaction = (Transaction) mediatorServices.createServerEJB("Transaction", Long.parseLong(transactionOid));


                String sqlStatementPayment = " SELECT DOMESTIC_PAYMENT_OID FROM DOMESTIC_PAYMENT WHERE" +
                    "  P_TRANSACTION_OID = ? ";
                    resultXML = DatabaseQueryBean.getXmlResultSet(sqlStatementPayment, false, new Object[]{transactionOid});

                String whereClause  = "P_TRANSACTION_OID = ?";
                int paymentCount = DatabaseQueryBean.getCount("DOMESTIC_PAYMENT_OID","DOMESTIC_PAYMENT",whereClause, false,new Object[]{transactionOid});

                if (overallPaymentStatus.equalsIgnoreCase(TradePortalConstants.PAY_STATUS_COMPLETED)){

                    for (int totalCount=0;totalCount<paymentCount;totalCount++)
                    {
                        String domesticPaymentOid    = resultXML.getAttribute("/ResultSetRecord(" + totalCount + ")/DOMESTIC_PAYMENT_OID");
                        domesticpayment       = (DomesticPayment)  mediatorServices.createServerEJB("DomesticPayment", Long.parseLong(domesticPaymentOid));
                        //cquinton 1/28/2011 Rel 6.1.0 ir#rdul012854364 don't update rejected status
                        if ( !TradePortalConstants.BENEFICIARY_PAYMENT_STATUS_REJECTED.equals( 
                              domesticpayment.getAttribute("payment_status") ) ) {
                            domesticpayment.setAttribute("payment_status", 
                                TradePortalConstants.BENEFICIARY_PAYMENT_STATUS_COMPLETE);
                            int returnValue = domesticpayment.save();
                            if (returnValue < 0) {
                                mediatorServices.getErrorManager().addErrors(domesticpayment.getErrorManager().getIssuedErrorsList());
                            }
                        }
                        domesticpayment.remove();
                    }
                    // CR-597 crhodes 3/1/20011 Begin
                    processPaymentBeneficiaryEmails(mediatorServices, instrument, transactionstatus, transactionOid);
                    // CR-597 crhodes 3/1/20011 End
                    
                    //cquinton 1/27/2011 Rel 6.1.0 ir#rvul012768398
                    //defensively update instrument/transaction status only if its present
                    if (!InstrumentServices.isBlank(instrumentstatus)) {
                        instrument.setAttribute("instrument_status", instrumentstatus);
                        instrument.save();
                    }
                    //cquinton 2/23/2011 Rel 6.1.0 ir#rvul011972931 start
                    String origTransactionStatus = transaction.getAttribute("transaction_status");
                    if (!InstrumentServices.isBlank(transactionstatus)) {
                        transaction.setAttribute("transaction_status", transactionstatus);
                        transaction.save();
                    }
                    //Leelavathi - CR-610 - Rel 7.0.0 - 15-Mar-2011 - Start
                    //update account balances as necessary
                    /*if ( TradePortalConstants.INSTR_STATUS_CLOSED.equals( instrumentstatus ) ||
                         TradePortalConstants.INSTR_STATUS_REJECTED_PAYMENT.equals( instrumentstatus ) ||
                         TradePortalConstants.INSTR_STATUS_PARTIALLY_REJECTED_PAYMENT.equals( instrumentstatus ) ) {
                        UnPackagerServices.
                            updateAccountBalances(instrument, origTransactionStatus,
                                transaction, false,
                                transaction.getAttributeDateTime("transaction_status_date"), 
                                mediatorServices);
                    }*/
                    //Leelavathi - CR-610 - Rel 7.0.0 - 15-Mar-2011 - End
                    //cquinton 2/23/2011 Rel 6.1.0 ir#rvul011972931 start
                    
                }
                else if (overallPaymentStatus.equalsIgnoreCase(TradePortalConstants.PAY_STATUS_FAILED)){

                    for (int totalCount=0;totalCount<paymentCount;totalCount++)
                    {

                        String domesticPaymentOid    = resultXML.getAttribute("/ResultSetRecord(" + totalCount + ")/DOMESTIC_PAYMENT_OID");
                        domesticpayment       = (DomesticPayment)  mediatorServices.createServerEJB("DomesticPayment", Long.parseLong(domesticPaymentOid));
                        //cquinton 1/28/2011 Rel 6.1.0 ir#rdul012854364 don't update rejected status
                        if ( !TradePortalConstants.BENEFICIARY_PAYMENT_STATUS_REJECTED.equals( 
                              domesticpayment.getAttribute("payment_status") ) ) {
                            domesticpayment.setAttribute("payment_status", TradePortalConstants.BENEFICIARY_PAYMENT_STATUS_REJECTED_UNPAID);
                            int returnValue = domesticpayment.save();
                            if (returnValue < 0) {
                                mediatorServices.getErrorManager().addErrors(domesticpayment.getErrorManager().getIssuedErrorsList());
                            }
                        }
                        domesticpayment.remove();
                    }
                    // CR-597 crhodes 3/1/20011 Begin
                    processPaymentBeneficiaryEmails(mediatorServices, instrument, transactionstatus, transactionOid);
                    // CR-597 crhodes 3/1/20011 End
                    
                    //cquinton 1/27/2011 Rel 6.1.0 ir#rvul012768398
                    //defensively update instrument/transaction status only if its present
                    if (!InstrumentServices.isBlank(instrumentstatus)) {
                        instrument.setAttribute("instrument_status", instrumentstatus);
                        instrument.save();
                    }
                    //cquinton 2/23/2011 Rel 6.1.0 ir#rvul011972931 start
                    String origTransactionStatus = transaction.getAttribute("transaction_status");
                    if (!InstrumentServices.isBlank(transactionstatus)) {
                        transaction.setAttribute("transaction_status", transactionstatus);
                        transaction.save();
                    }
                    //Leelavathi - CR-610 - Rel 7.0.0 - 15-Mar-2011 - Start
                    //update account balances as necessary
                    /*if ( TradePortalConstants.INSTR_STATUS_CLOSED.equals( instrumentstatus ) ||
                         TradePortalConstants.INSTR_STATUS_REJECTED_PAYMENT.equals( instrumentstatus ) ||
                         TradePortalConstants.INSTR_STATUS_PARTIALLY_REJECTED_PAYMENT.equals( instrumentstatus ) ) {
                        UnPackagerServices.
                            updateAccountBalances(instrument, origTransactionStatus,
                                transaction, false,
                                transaction.getAttributeDateTime("transaction_status_date"), 
                                mediatorServices);
                    }*/
                    //Leelavathi - CR-610 - Rel 7.0.0 - 15-Mar-2011 - End
                    //cquinton 2/23/2011 Rel 6.1.0 ir#rvul011972931 start
                }


                else if (overallPaymentStatus.equalsIgnoreCase(TradePortalConstants.PAY_STATUS_PARTIAL_COMPLETE)){

                    for (int totalCount=0;totalCount<paymentCount;totalCount++)
                    {

                        String domesticPaymentOid    = resultXML.getAttribute("/ResultSetRecord(" + totalCount + ")/DOMESTIC_PAYMENT_OID");
                        domesticpayment       = (DomesticPayment)  mediatorServices.createServerEJB("DomesticPayment", Long.parseLong(domesticPaymentOid));

                        //cquinton 1/28/2011 Rel 6.1.0 ir#rdul012854364 don't update rejected status
                        if ( !TradePortalConstants.BENEFICIARY_PAYMENT_STATUS_REJECTED.equals( 
                              domesticpayment.getAttribute("payment_status") ) ) {
                            if(sequenceNumberList.contains(domesticpayment.getAttribute("sequence_number"))){
                                domesticpayment.setAttribute("payment_status", TradePortalConstants.BENEFICIARY_PAYMENT_STATUS_REJECTED_UNPAID);   
                                int i = sequenceNumberList.indexOf(domesticpayment.getAttribute("sequence_number"));
                                String aPaySystemRef = (String) paySystemRef.get(i);
                                if(!InstrumentServices.isBlank(aPaySystemRef)){
                                    domesticpayment.setAttribute("payment_system_ref", aPaySystemRef);   
                                }
                                String aErrorText = (String) errorText.get(i);
                                if(!InstrumentServices.isBlank(aErrorText)){
                                    domesticpayment.setAttribute("error_text", aErrorText); 
                                }                                                                                                                                        
                            }
                          
                            else{
                                domesticpayment.setAttribute("payment_status", TradePortalConstants.BENEFICIARY_PAYMENT_STATUS_COMPLETE);
                                        
                            }
                            int returnValue = domesticpayment.save();
                            if (returnValue < 0) {
                                mediatorServices.getErrorManager().addErrors(domesticpayment.getErrorManager().getIssuedErrorsList());
                            }
                        }
                        domesticpayment.remove();
                            
                    }
                    
                    // CR-597 crhodes 3/1/20011 Begin
                    processPaymentBeneficiaryEmails(mediatorServices, instrument, transactionstatus, transactionOid);
                    // CR-597 crhodes 3/1/20011 End
                          
                    //cquinton 1/27/2011 Rel 6.1.0 ir#rvul012768398
                    //defensively update instrument/transaction status only if its present
                    if (!InstrumentServices.isBlank(instrumentstatus)) {
                        instrument.setAttribute("instrument_status", instrumentstatus);
                        instrument.save();
                    }
                    //cquinton 2/23/2011 Rel 6.1.0 ir#rvul011972931 start
                    String origTransactionStatus = transaction.getAttribute("transaction_status");
                    if (!InstrumentServices.isBlank(transactionstatus)) {
                        transaction.setAttribute("transaction_status", transactionstatus);
                        transaction.save();
                    }
                    //Leelavathi - CR-610 - Rel 7.0.0 - 15-Mar-2011 - Start
                    //update account balances as necessary
                    /*if ( TradePortalConstants.INSTR_STATUS_CLOSED.equals( instrumentstatus ) ||
                         TradePortalConstants.INSTR_STATUS_REJECTED_PAYMENT.equals( instrumentstatus ) ||
                         TradePortalConstants.INSTR_STATUS_PARTIALLY_REJECTED_PAYMENT.equals( instrumentstatus ) ) {
                        UnPackagerServices.
                            updateAccountBalances(instrument, origTransactionStatus,
                                transaction, false,
                                transaction.getAttributeDateTime("transaction_status_date"), 
                                mediatorServices);
                    }*/
                    //Leelavathi - CR-610 - Rel 7.0.0 - 15-Mar-2011 - End
                    //cquinton 2/23/2011 Rel 6.1.0 ir#rvul011972931 start
                }
                    
                else if (overallPaymentStatus.equalsIgnoreCase(TradePortalConstants.PAY_STATUS_SINGLE_COMPLETE)){                     
                    for (int totalCount=0;totalCount<paymentCount;totalCount++)
                    {

                        String domesticPaymentOid    = resultXML.getAttribute("/ResultSetRecord(" + totalCount + ")/DOMESTIC_PAYMENT_OID");
                        domesticpayment       = (DomesticPayment)  mediatorServices.createServerEJB("DomesticPayment", Long.parseLong(domesticPaymentOid));

                        if(sequenceNumberList.contains(domesticpayment.getAttribute("sequence_number"))){
                            domesticpayment.setAttribute("payment_status", TradePortalConstants.BENEFICIARY_PAYMENT_STATUS_COMPLETE);
                            int i = sequenceNumberList.indexOf(domesticpayment.getAttribute("sequence_number"));
                            String aPaySystemRef = (String) paySystemRef.get(i);
                            if(!InstrumentServices.isBlank(aPaySystemRef)){
                                domesticpayment.setAttribute("payment_system_ref", aPaySystemRef);   
                            }
                            String aErrorText = (String) errorText.get(i);
                            if(!InstrumentServices.isBlank(aErrorText)){
                                domesticpayment.setAttribute("error_text", aErrorText); 
                            }                                                                                                                                        
                                          
                            int returnValue = domesticpayment.save();
                            domesticpayment.remove();
                            if (returnValue < 0) {
                                mediatorServices.getErrorManager().addErrors(domesticpayment.getErrorManager().getIssuedErrorsList());
                            }
                        }
                    }
                    
                    // CR-597 crhodes 3/1/20011 Begin
                    processPaymentBeneficiaryEmails(mediatorServices, instrument, transactionstatus, transactionOid);
                    // CR-597 crhodes 3/1/20011 Begin
                    
                    whereClause = "(PAYMENT_STATUS IS NULL OR PAYMENT_STATUS = ? ) " +
                        "AND P_TRANSACTION_OID = ? ";
                    int statusCount = DatabaseQueryBean.getCount("DOMESTIC_PAYMENT_OID", "DOMESTIC_PAYMENT", whereClause, false,new Object[]{TradePortalConstants.BENEFICIARY_PAYMENT_STATUS_PENDING,transactionOid});
                    if (statusCount < 1){
                        if (InstrumentServices.isBlank(instrumentstatus)) {
                            mediatorServices.debug("No Instrument Status to update");
                            unpackage=false;
                        } else {
                            instrument.setAttribute("instrument_status", instrumentstatus);
                            instrument.save();
                        }
                        //cquinton 2/23/2011 Rel 6.1.0 ir#rvul011972931 start
                        String origTransactionStatus = transaction.getAttribute("transaction_status");
                        if (InstrumentServices.isBlank(transactionstatus)) {
                            mediatorServices.debug("No Transaction Status to update");
                            unpackage=false;
                        } else {
                            transaction.setAttribute("transaction_status", transactionstatus);
                            transaction.save();
                        }
                        //Leelavathi - CR-610 - Rel 7.0.0 - 15-Mar-2011 - Start
                        //update account balances as necessary
                        /*if ( TradePortalConstants.INSTR_STATUS_CLOSED.equals( instrumentstatus ) ||
                             TradePortalConstants.INSTR_STATUS_REJECTED_PAYMENT.equals( instrumentstatus ) ||
                            TradePortalConstants.INSTR_STATUS_PARTIALLY_REJECTED_PAYMENT.equals( instrumentstatus ) ) {
                            UnPackagerServices.
                                updateAccountBalances(instrument, origTransactionStatus,
                                    transaction, false,
                                    transaction.getAttributeDateTime("transaction_status_date"), 
                                    mediatorServices);
                        }*/
                        //Leelavathi - CR-610 - Rel 7.0.0 - 15-Mar-2011 - End
                        //cquinton 2/23/2011 Rel 6.1.0 ir#rvul011972931 start
                    }
                }


                else if (overallPaymentStatus.equalsIgnoreCase(TradePortalConstants.PAY_STATUS_SINGLE_FAILED)){

                    for (int totalCount=0;totalCount<paymentCount;totalCount++)
                    {

                        String domesticPaymentOid    = resultXML.getAttribute("/ResultSetRecord(" + totalCount + ")/DOMESTIC_PAYMENT_OID");
                        domesticpayment       = (DomesticPayment)  mediatorServices.createServerEJB("DomesticPayment", Long.parseLong(domesticPaymentOid));
                             
                        if(sequenceNumberList.contains(domesticpayment.getAttribute("sequence_number"))){
                            domesticpayment.setAttribute("payment_status", TradePortalConstants.BENEFICIARY_PAYMENT_STATUS_REJECTED_UNPAID);  
                            int i = sequenceNumberList.indexOf(domesticpayment.getAttribute("sequence_number"));
                            String aPaySystemRef = (String) paySystemRef.get(i);
                            if(!InstrumentServices.isBlank(aPaySystemRef)){
                                domesticpayment.setAttribute("payment_system_ref", aPaySystemRef);   
                            }
                            String aErrorText = (String) errorText.get(i);
                            if(!InstrumentServices.isBlank(aErrorText)){
                                domesticpayment.setAttribute("error_text", aErrorText); 
                            }                                                                                                                                        
                            int returnValue = domesticpayment.save();
                            domesticpayment.remove();
                            if (returnValue < 0) {
                                mediatorServices.getErrorManager().addErrors(domesticpayment.getErrorManager().getIssuedErrorsList());
                            }
                        }
                    }
                    
                    // CR-597 crhodes 3/1/20011 Begin
                    processPaymentBeneficiaryEmails(mediatorServices, instrument, transactionstatus, transactionOid);                    
                    // CR-597 crhodes 3/1/20011 Begin
                    
                    whereClause = "(PAYMENT_STATUS IS NULL OR PAYMENT_STATUS = ? ) AND P_TRANSACTION_OID = ? ";
                    int statusCount = DatabaseQueryBean.getCount("DOMESTIC_PAYMENT_OID", "DOMESTIC_PAYMENT", whereClause, false,TradePortalConstants.BENEFICIARY_PAYMENT_STATUS_PENDING, transactionOid);
                    if (statusCount < 1)
                    {
                        if (InstrumentServices.isBlank(instrumentstatus)) {
                            mediatorServices.debug("No Instrument Status to update");
                            unpackage=false;
                        } else {
                            instrument.setAttribute("instrument_status", instrumentstatus);
                            instrument.save();
                        }
                        //cquinton 2/23/2011 Rel 6.1.0 ir#rvul011972931 start
                        String origTransactionStatus = transaction.getAttribute("transaction_status");
                        if (InstrumentServices.isBlank(transactionstatus)) {
                            mediatorServices.debug("No Transaction Status to update");
                            unpackage=false;
                        } else {
                            transaction.setAttribute("transaction_status", transactionstatus);
                            transaction.save();
                        }
                        //Leelavathi - CR-610 - Rel 7.0.0 - 15-Mar-2011 - Start
                        //update account balances as necessary
                        /*if ( TradePortalConstants.INSTR_STATUS_CLOSED.equals( instrumentstatus ) ||
                             TradePortalConstants.INSTR_STATUS_REJECTED_PAYMENT.equals( instrumentstatus ) ||
                            TradePortalConstants.INSTR_STATUS_PARTIALLY_REJECTED_PAYMENT.equals( instrumentstatus ) ) {
                            UnPackagerServices.
                                updateAccountBalances(instrument, origTransactionStatus,
                                    transaction, false,
                                    transaction.getAttributeDateTime("transaction_status_date"), 
                                    mediatorServices);
                        }*/
                        //Leelavathi - CR-610 - Rel 7.0.0 - 15-Mar-2011 - End
                        //cquinton 2/23/2011 Rel 6.1.0 ir#rvul011972931 start
                    }
                }

                else if (overallPaymentStatus.equalsIgnoreCase(TradePortalConstants.PAY_STATUS_REJECTED)){

                    for (int totalCount=0;totalCount<paymentCount;totalCount++)
                    {

                        String domesticPaymentOid    = resultXML.getAttribute("/ResultSetRecord(" + totalCount + ")/DOMESTIC_PAYMENT_OID");
                        domesticpayment       = (DomesticPayment)  mediatorServices.createServerEJB("DomesticPayment", Long.parseLong(domesticPaymentOid));
                         
                        if(sequenceNumberList.contains(domesticpayment.getAttribute("sequence_number"))){
                            domesticpayment.setAttribute("payment_status", TradePortalConstants.BENEFICIARY_PAYMENT_STATUS_REJECTED);  
                            int i = sequenceNumberList.indexOf(domesticpayment.getAttribute("sequence_number"));
                            String aPaySystemRef = (String) paySystemRef.get(i);
                            if(!InstrumentServices.isBlank(aPaySystemRef)){
                                domesticpayment.setAttribute("payment_system_ref", aPaySystemRef);   
                            }
                            String aErrorText = (String) errorText.get(i);
                            if(!InstrumentServices.isBlank(aErrorText)){
                                domesticpayment.setAttribute("error_text", aErrorText); 
                            }                                                                                                                                          
                            int returnValue = domesticpayment.save();
                            domesticpayment.remove();
                            if (returnValue < 0) {
                                mediatorServices.getErrorManager().addErrors(domesticpayment.getErrorManager().getIssuedErrorsList());
                            }

                        }
                    }
                    
                    // CR-597 crhodes 3/1/20011 Begin
                    processPaymentBeneficiaryEmails(mediatorServices, instrument, transactionstatus, transactionOid);
                    // CR-597 crhodes 3/1/20011 End
                         
                    //cquinton 1/27/2011 Rel 6.1.0 ir#rvul012768398
                    //rejected payment status will never have an instrument status or transaction status
                    //instrument.setAttribute("instrument_status", instrumentstatus);
                    //transaction.setAttribute("transaction_status", transactionstatus);
                    //instrument.save();
                    //transaction.save();

                }
                
                // CJR 04/13/2011 CR-593 Begin
                if(transaction.isH2HSentPayment()){
                	if ( TradePortalConstants.INSTR_STATUS_CLOSED.equals( instrumentstatus ) ||
                			TradePortalConstants.INSTR_STATUS_REJECTED_PAYMENT.equals( instrumentstatus ) ||
                			TradePortalConstants.INSTR_STATUS_PARTIALLY_REJECTED_PAYMENT.equals( instrumentstatus ) ) {
                		String date_created = DateTimeUtility.getGMTDateTime(false);
                		String messageId = InstrumentServices.getNewMessageID();
                		OutgoingInterfaceQueue oiQueue = (OutgoingInterfaceQueue) EJBObjectFactory.createServerEJB(getSessionContext(),"OutgoingInterfaceQueue");
                		oiQueue.newObject();
                		oiQueue.setAttribute("date_created", date_created);
                		oiQueue.setAttribute("status", TradePortalAgent.STATUS_START);
                		oiQueue.setAttribute("msg_type", "PAYCOMP");
                		oiQueue.setAttribute("instrument_oid", instrumentOid);
                		oiQueue.setAttribute("transaction_oid", transactionOid);
                		oiQueue.setAttribute("message_id", messageId);
                		oiQueue.save();                    
                	}
                }
                // CJR 04/13/2011 CR-593 End
            }
        }

        catch (RemoteException e) {
            mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,TradePortalConstants.TASK_NOT_SUCCESSFUL,"Message Unpackaging");

            e.printStackTrace();

        }
        catch (AmsException e) {
            mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,TradePortalConstants.TASK_NOT_SUCCESSFUL,"Message Unpackaging");
            e.printStackTrace();


        }
        catch (Exception e) {
            mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,TradePortalConstants.TASK_NOT_SUCCESSFUL,"Message Unpackaging");
            e.printStackTrace();

        }


        return outputDoc;
    }
    
    /**
     * Creates a record in the PaymentBenEmailQueue table for the specified transaction to trigger 
     * beneficiary email notifications if the proper conditions are met.  The transaction must be 
     * PROCESSED_BY_BANK or PARTIALLY_REJECTED_PAYMENT, the instrument type must be DOMESTIC_PAYMENT (FTDP).
     * If the above conditions are met then a record is placed on the PaymentBenEmailQueue for the transaction. 
     * An agent will then process the queue recordswhich will then determine which domestic payments (if any) 
     * should have emails generated for them.
     *  
     * 
     * @param mediatorServices The MediatorServices object to use.
     * @param instrument The current instrument business object.
     * @param transactionStatus The status of the current transaction.
     * @param transactionOID The OID of the current transaction.
     * @throws RemoteException
     * @throws AmsException
     * @throws RemoveException
     */
    protected void processPaymentBeneficiaryEmails(MediatorServices mediatorServices, Instrument instrument, String transactionStatus, String transactionOID) 
    		throws RemoteException, AmsException, RemoveException{
    	
    	String instrumentTypeCode = instrument.getAttribute("instrument_type_code");
    	
    	// Only send emails for Payment Instrument Types.
    	if(!InstrumentType.DOMESTIC_PMT.equals(instrumentTypeCode)){
    		return;
    	}
    	
    	// CR-597 - 3/15/2011 - Design change - Removed requirement to check Transaction status.
    	// Only continue processing if Transaction status = "PROCESSED_BY_BANK" OR "PARTIALLY_REJECTED_PAYMENT"
//    	if(TradePortalConstants.TRANS_STATUS_PROCESSED_BY_BANK.equals(transactionStatus) || 
//    			TradePortalConstants.TRANS_STATUS_PARTIALLY_REJECTED_PAYMENT.equals(transactionStatus)){  
    	
    		PaymentBenEmailQueue emailQueue = (PaymentBenEmailQueue) mediatorServices.createServerEJB("PaymentBenEmailQueue");
    		emailQueue.createNewPaymentBeneficiaryEmailQueueRecord(transactionOID, TradePortalConstants.BENEFICIARY_PAYMENT_STATUS_COMPLETE);
	    	emailQueue.save();
	    	emailQueue.remove();
//    	}
    }  
    

}
