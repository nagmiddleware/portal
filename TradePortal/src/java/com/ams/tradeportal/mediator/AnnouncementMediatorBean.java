package com.ams.tradeportal.mediator;
import java.rmi.RemoteException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.busobj.Announcement;
import com.ams.tradeportal.busobj.AnnouncementBankGroup;
import com.ams.tradeportal.busobj.User;
import com.ams.tradeportal.common.TPDateTimeUtility;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.ClientServerDataBridge;
import com.amsinc.ecsg.frame.MediatorBean;
import com.amsinc.ecsg.frame.MediatorServices;
import com.amsinc.ecsg.util.DateTimeUtility;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.EncryptDecrypt;
import com.amsinc.ecsg.util.StringFunction;

/*
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class AnnouncementMediatorBean extends MediatorBean {
private static final Logger LOG = LoggerFactory.getLogger(AnnouncementMediatorBean.class);
	public static final String INSERT_MODE = "C";
	public static final String UPDATE_MODE = "U";
	public static final String DELETE_MODE = "D";

	// Business methods
	public DocumentHandler execute(DocumentHandler inputDoc,
			DocumentHandler outputDoc, MediatorServices mediatorServices)
			throws RemoteException, AmsException {
		ClientServerDataBridge csdb = null;
		StringBuilder infoMsgItem = new StringBuilder();
		Announcement announcement = null;
		String updateButtonValue = null;
		String deleteButtonValue = null;
		String currencyCode = null;
		String userLocale = null;
		String updateMode = this.UPDATE_MODE;
		long announcementOid = 0;

		// Now determine what update mode the mediator is in. (Note: do
		// not edit the delete button path to use '..' notation, as the
		// first path won't be found in the case of delete requests.

		updateButtonValue = inputDoc.getAttribute("/Action/saveXPos");
		deleteButtonValue = inputDoc.getAttribute("/Action/deleteXPos");

		if (updateButtonValue != null) {
			updateMode = this.UPDATE_MODE;
		} else if (deleteButtonValue != null) {
			updateMode = this.DELETE_MODE;
		}

		String buttonPressed = inputDoc.getAttribute("/Update/ButtonPressed");
		LOG.debug("The button pressed is {}" , buttonPressed);

		if (buttonPressed.equals(TradePortalConstants.BUTTON_SAVE))
			updateMode = this.UPDATE_MODE;
		else if (buttonPressed.equals(TradePortalConstants.BUTTON_DELETE))
			updateMode = this.DELETE_MODE;

		if (updateMode == null) {
			throw new AmsException("Unable to determine update mode.");
		}

		// Perform update, delete, or insert.
		try {
			// Get the oid from the doc. '0' indicates a new FX rate so
			// change the mode.
			if (updateMode.equals(this.DELETE_MODE)) {

				Vector <DocumentHandler> deletions = inputDoc.getFragments("/Announcement");
				Long[] announcementOids = new Long[deletions.size()];
                // W Zhu 9/11/2012 Rel 8.1 T36000004579 add key parameter.
                String secretKeyString = mediatorServices.getCSDB().getCSDBValue("SecretKey");
                byte[] keyBytes = EncryptDecrypt.base64StringToBytes(secretKeyString);
                javax.crypto.SecretKey secretKey = new javax.crypto.spec.SecretKeySpec(keyBytes, "DESede/CBC/PKCS5Padding");
				for (int i =0; i < deletions.size(); i++){
					String temp = deletions.get(i).getAttribute("announcement_oid");
					announcementOids[i] = Long.parseLong(
					EncryptDecrypt.decryptStringUsingTripleDes(temp, secretKey));
					announcement = (Announcement) mediatorServices
							.createServerEJB("Announcement");
					LOG.debug("Mediator: Deleting Announcement for Oid {}", announcementOids[i]);
					announcement.getData(announcementOids[i]);
					String title = announcement.getAttribute("title");
					if ( i>0 ) {
					    infoMsgItem.append(", ");
					}
					infoMsgItem.append(title);
					announcement.delete();
					LOG.debug("Mediator: Completed the delete of the Announcement object {}", announcementOids[i]);
				}
			} else {
				announcementOid = inputDoc
						.getAttributeLong("/Announcement/announcement_oid");
				LOG.debug("announcementoid = {}", announcementOid);
				if (announcementOid == 0) {
					updateMode = this.INSERT_MODE;
				}

				Vector<DocumentHandler> acctVector = inputDoc
						.getFragments("/Announcement/AnnouncementBankGroupList");

				if (updateMode.equals(this.INSERT_MODE)) {
					LOG.debug("Mediator: Creating new Announcement");
					announcement = (Announcement) mediatorServices
							.createServerEJB("Announcement");
					//cquinton 12/5/2012 store off the new announcement oid
					announcementOid = announcement.newObject();
					String start_date = inputDoc
							.getAttribute("/Announcement/start_date");
					announcement.setAttribute("start_date", start_date);
					String end_date = inputDoc
							.getAttribute("/Announcement/end_date");
					announcement.setAttribute("end_date", end_date);
					String title = inputDoc.getAttribute("/Announcement/title");
					announcement.setAttribute("title", title);
					String subject = inputDoc
							.getAttribute("/Announcement/subject");
					announcement.setAttribute("subject", subject);
					String critical_ind = inputDoc
							.getAttribute("/Announcement/critical_ind");
					announcement.setAttribute("critical_ind", critical_ind);
					announcement.setAttribute("owner_org_oid", inputDoc
							.getAttribute("/Announcement/owner_org_oid")); 
					announcement.setAttribute("ownership_level", inputDoc
							.getAttribute("/Announcement/ownership_level"));
					announcement
							.setAttribute("all_bogs_ind", inputDoc
									.getAttribute("/Announcement/all_bogs_ind"));
					announcement.setAttribute("announcement_status", inputDoc
							.getAttribute("/Announcement/announcement_status"));
					for (DocumentHandler doc : acctVector) {
						long bog = announcement
							.newComponent("AnnouncementBankGroupList");
						AnnouncementBankGroup a_bog = (AnnouncementBankGroup) announcement
							.getComponentHandle(
								"AnnouncementBankGroupList", bog);
						LOG.debug("new bog unique id = {}", bog);
						
						String id = doc.getAttribute("bog_oid");
						LOG.debug("setting bog_oid to {}", id);
						a_bog.setAttribute("bog_oid", id);
						
					}
					
					infoMsgItem.append( title );
					
					//cquinton 12/6/2012 - when inserting, need to ensure that the
					// start date is today or greater. do this here instead of
					// business object becuase its ok to save an announcement with 
					// start date earlier than today if the announcement is just being 
					// updated
					if (!StringFunction.isBlank(start_date)) {
                        User user = (User) mediatorServices.createServerEJB("User",
                            inputDoc.getAttributeLong("/User/userOid"));
					    validateStartDate(start_date, user, mediatorServices);
                    }

				} else if (updateMode.equals(this.UPDATE_MODE)){
					LOG.debug("Mediator: Retrieving Announcement {}", announcementOid);
					announcement = (Announcement) mediatorServices
							.createServerEJB("Announcement");
					announcement.getData(announcementOid);
					// Set the optimistic lock attribute to make sure the
					// Announcement hasn't been changed by someone else
					// since it was first retrieved
					announcement.setAttribute("opt_lock",
							inputDoc.getAttribute("/Announcement/opt_lock"));
					String start_date = inputDoc
							.getAttribute("/Announcement/start_date");
					announcement.setAttribute("start_date", start_date);
					String end_date = inputDoc
							.getAttribute("/Announcement/end_date");
					announcement.setAttribute("end_date", end_date);
					String title = inputDoc.getAttribute("/Announcement/title");
					announcement.setAttribute("title", title);
					String subject = inputDoc
							.getAttribute("/Announcement/subject");
					announcement.setAttribute("subject", subject);
					String critical_ind = inputDoc
							.getAttribute("/Announcement/critical_ind");
					announcement.setAttribute("critical_ind", critical_ind);
					announcement.setAttribute("owner_org_oid", inputDoc
							.getAttribute("/Announcement/owner_org_oid")); 
					announcement.setAttribute("ownership_level", inputDoc
							.getAttribute("/Announcement/ownership_level"));
					announcement
							.setAttribute("all_bogs_ind", inputDoc
									.getAttribute("/Announcement/all_bogs_ind"));
					announcement.setAttribute("announcement_status", inputDoc
							.getAttribute("/Announcement/announcement_status"));

					// To update the AnnouncementBankGroupList
					// 1. delete existing children
					// 2. add currently received children
					announcement.deleteComponent("AnnouncementBankGroupList");

					for (DocumentHandler doc : acctVector) {
						long bog = announcement
								.newComponent("AnnouncementBankGroupList");
						AnnouncementBankGroup a_bog = (AnnouncementBankGroup) announcement
								.getComponentHandle(
										"AnnouncementBankGroupList", bog);
						LOG.debug("new bog unique id ={}", bog);
						// LOG.info("setting announcement_oid to "+announcement.getAttribute("announcement_oid"));
						String id = doc.getAttribute("bog_oid");
						LOG.debug("setting bog_oid to {}", id);
						a_bog.setAttribute("bog_oid", id);
						//LOG.debug("AnnouncementBankGroup: {} \t {} \t {}", a_bog.getAttribute("announcement_bank_group_oid"), a_bog.getAttribute("announcement_oid"), a_bog.getAttribute("bog_oid"));
					}
					
                    infoMsgItem.append( title );
				}
				
				//cquinton 12/12/12 save should not happen after a delete
				//and there is no need for retry
                //announcement.setRetryOptLock(true);
                announcement.save();

                //set the oid on the output doc if successful insert
                if (this.INSERT_MODE.equals(updateMode)) {
                    outputDoc.setAttributeLong("/Announcement/announcement_oid", announcementOid);
                }
        
                LOG.debug("Mediator: Completed the save of the Announcement object");
			}
		}
        /*cquinton 12/7/2012 - blindly catching and stamping out exceptions is bad
         * pass these exceptions on to the parent who will do something about them!*/
  		catch (AmsException e) {
  		    throw e;
		} catch (RemoteException e) {
		    throw e;
		} catch (Exception e) {
		    throw new AmsException("General Exception occured in AnnouncementMediator", e);
		} finally {
			LOG.debug("Mediator: Result of Announcement update is {}", this.isTransSuccess(mediatorServices.getErrorManager()));

			if (this.isTransSuccess(mediatorServices.getErrorManager())) {
				// Get the update text for the audit log and error messages; if
				// the FX rate is being inserted,
				// lookup the currency code of the new FX rate and append the
				// description found for it to the
				// update text (this info is already known for update and delete
				// modes).
				if (updateMode.equals(this.INSERT_MODE)) {
					// Get the user's locale from the client server data bridge
					csdb = mediatorServices.getCSDB();
					userLocale = csdb.getLocaleName();

				}

			}
		}
		
		//cquinton 12/7/2012 setting success error code cannot be in a finally clause
		// that means it always works! which is just not true
        if (this.isTransSuccess(mediatorServices.getErrorManager())) {
            // Finally issue a success message based on update type.
            if (updateMode.equals(this.DELETE_MODE)) {
                mediatorServices.getErrorManager().issueError(
                        TradePortalConstants.ERR_CAT_1,
                        TradePortalConstants.DELETE_SUCCESSFUL,
                        infoMsgItem.toString());
            } else if (updateMode.equals(this.INSERT_MODE)) {
                mediatorServices.getErrorManager().issueError(
                        TradePortalConstants.ERR_CAT_1,
                        TradePortalConstants.INSERT_SUCCESSFUL,
                        infoMsgItem.toString());
            } else {
                mediatorServices.getErrorManager().issueError(
                        TradePortalConstants.ERR_CAT_1,
                        TradePortalConstants.UPDATE_SUCCESSFUL,
                        infoMsgItem.toString());
            }
        }
        
		return outputDoc;
	}
	
	//cquinton 12/8/2012 added
	private void validateStartDate(String startDateStr, User user, MediatorServices mediatorServices)
	        throws AmsException, RemoteException {
        try
        {
            Date startDate = 
                DateTimeUtility.convertStringDateToDate(startDateStr);
      
            //get the user date - which is specific to the user timezone!
            String userTimezone = user.getAttribute("timezone");
            String currentGMTTime = DateTimeUtility.getGMTDateTime(true,true);//Nar IR-T36000029999 Rel 9.0 - use override date
            Date currentDate = TPDateTimeUtility.getLocalDateTime(
                currentGMTTime, userTimezone);
            //strip off the time
            SimpleDateFormat isoDateFormatter = new SimpleDateFormat("yyyy-MM-dd");
            String myIsoDate = isoDateFormatter.format(currentDate);
            currentDate = isoDateFormatter.parse(myIsoDate);
      
            if (startDate.before(currentDate)) {
                mediatorServices.getErrorManager().issueError(
                    TradePortalConstants.ERR_CAT_1,
                    TradePortalConstants.STARTDATE_CANNOT_BE_IN_PAST);
            }
        }
        catch (ParseException e)
        {
            //throw an AmsException so it bubbles up
            throw new AmsException("Problem validating start date", e);
        }
	}
}