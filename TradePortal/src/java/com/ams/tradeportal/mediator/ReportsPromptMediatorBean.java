package com.ams.tradeportal.mediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.busobj.*;
import com.ams.tradeportal.common.*;
import com.amsinc.ecsg.util.*;
import com.amsinc.ecsg.frame.*;
import javax.ejb.*;
import javax.naming.*;
import java.rmi.*;
import java.util.*;
import com.ams.tradeportal.busobj.util.*;

/*
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class ReportsPromptMediatorBean extends MediatorBean
{
private static final Logger LOG = LoggerFactory.getLogger(ReportsPromptMediatorBean.class);

   // Business methods
   public DocumentHandler execute(DocumentHandler inputDoc, DocumentHandler outputDoc, MediatorServices mediatorServices)
								  throws RemoteException, AmsException
   {
       LOG.debug("ReportsPromptMediator Executed.  inputDoc follows on next line:\n" + inputDoc.toString());

        Vector values = inputDoc.getFragments("/ListOfValues");
        Vector names = inputDoc.getFragments("/Prompt");
        for (int i=0; i<values.size(); i++)
        {
            DocumentHandler value = (DocumentHandler) values.elementAt(i);
            String promptValue = value.getAttribute("/PromptValue");
            LOG.debug("promptValue " + i + ": '" + promptValue + "'");
            if (promptValue == null || promptValue.equals(""))
            {
                LOG.debug("NULL PROMPTS VALUES FOUND.  RETURNING TO PROMPT PAGE 1.");
                DocumentHandler name = (DocumentHandler) names.elementAt(i);
                mediatorServices.getErrorManager().issueError (TradePortalConstants.ERR_CAT_1,
			    		                                       AmsConstants.REQUIRED_ATTRIBUTE,
                                                               name.getAttribute("/Name"));
            }
        }

        return outputDoc;
   }
}
