package com.ams.tradeportal.mediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.common.*;
import com.ams.tradeportal.busobj.*;
import com.ams.tradeportal.busobj.util.*;
import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import javax.ejb.*;

import java.math.BigDecimal;
import java.rmi.*;

/*
 * Manages POR Message Packaging..
 *
 *     Copyright  � 2008
 *     American Management Systems, Incorporated
 *     All rights reserved
 */

public class PORPackagerMediatorBean extends MediatorBean {
private static final Logger LOG = LoggerFactory.getLogger(PORPackagerMediatorBean.class);

	/*
	 * This mediator is now used to package message for Purchase Order in portal Take
	 * the purchase order OID as input, go to that PO object and extract the
	 * purchase Order attributes
	 */

	public DocumentHandler execute(DocumentHandler inputDoc,
			DocumentHandler outputDoc, MediatorServices mediatorServices)
			throws RemoteException, AmsException {
		try {

			DocumentHandler processParameters = null;
			String poOid = null;
			String corp_org_oid = null;
			String bank_org_oid = null;
			String messageID = null;
			String operationOrganizationID = null;

			String messageType = MessageType.POR; // it is
																	// "POR"
			String destinationID = TradePortalConstants.DESTINATION_ID_PROPONIX;
			String senderID = TradePortalConstants.SENDER_ID;
			String dateSent = null;
			String timeSent = null;
			String timeStamp = DateTimeUtility.getGMTDateTime(false);

			OperationalBankOrganization operationalBankOrganization = null;
			CorporateOrganization corpOrg = null;

			processParameters = new DocumentHandler(inputDoc.getAttribute("/process_parameters"),false);
			String instrumentOid = inputDoc.getAttribute("/instrument_oid");
			poOid = processParameters.getAttribute("/POR_param/PO_oid");

			messageID = inputDoc.getAttribute("/message_id");

			dateSent = timeStamp;
			timeSent = timeStamp.substring(11, 19);

			// Get Purchase Order Object
			mediatorServices
					.debug("Ready to Instantiate Purchase Order Object with oid "
							+ poOid);
			PurchaseOrder purOrder = (PurchaseOrder) mediatorServices
					.createServerEJB("PurchaseOrder", Long.parseLong(poOid));

			// Find out the corporate org and instantiate corporate org
			corp_org_oid = purOrder.getAttribute("owner_org_oid");
			mediatorServices.debug("Ready to instantiate corporate org ");
			corpOrg = (CorporateOrganization) mediatorServices.createServerEJB(
					"CorporateOrganization", Long.parseLong(corp_org_oid));


			// Find the operational org from the corporate org just instantiated
			bank_org_oid = corpOrg.getAttribute("first_op_bank_org");
			mediatorServices
					.debug("Ready to instantiate operational bank org ");
			operationalBankOrganization = (OperationalBankOrganization) mediatorServices
					.createServerEJB("OperationalBankOrganization",
							Long.parseLong(bank_org_oid));
			operationOrganizationID = operationalBankOrganization
					.getAttribute("Proponix_id");

			// get ClientBankID
			long clientBankOid = corpOrg.getAttributeLong("client_bank_oid");
			ClientBank clientBank = (ClientBank) mediatorServices
					.createServerEJB("ClientBank", clientBankOid);

			String clientBankID = clientBank != null ? clientBank
					.getAttribute("OTL_id") : "";

			String customerID = corpOrg.getAttribute("Proponix_customer_id");
			Instrument instrument = (Instrument) mediatorServices
					.createServerEJB("Instrument", Long
							.parseLong(instrumentOid));

			outputDoc.setAttribute("/Proponix/Header/DestinationID", destinationID);
			outputDoc.setAttribute("../SenderID", senderID);
			outputDoc.setAttribute("../OperationOrganizationID", operationOrganizationID);
			outputDoc.setAttribute("../MessageType", messageType);
			outputDoc.setAttribute("../DateSent", dateSent);
			outputDoc.setAttribute("../TimeSent", timeSent);
			outputDoc.setAttribute("../MessageID", messageID);
			outputDoc.setAttribute("../ClientBankID", clientBankID);

			outputDoc.setAttribute("/Proponix/SubHeader/PurchsOrdrRef/Id", purOrder.getAttribute("purchase_order_num"));

			String date = "";
			if (InstrumentServices.isNotBlank(purOrder
					.getAttribute("issue_date"))) {
				date = TPDateTimeUtility.packagingInvoiceDate(purOrder
						.getAttribute("issue_date"));
			}
			outputDoc.setAttribute("../DtOfIsse", date);
			outputDoc.setAttribute("/Proponix/SubHeader/SequenceOfTotal", "001/001");
			String poType = purOrder.getAttribute("purchase_order_type");
			if(TradePortalConstants.INITIAL_INVOICE.equalsIgnoreCase(poType)){
				outputDoc.setAttribute("../PurchaseOrderType", TradePortalConstants.UNV_INT);
			}
			else if(TransactionType.AMEND.equalsIgnoreCase(poType)){
				outputDoc.setAttribute("../PurchaseOrderType", TradePortalConstants.UNV_AMD);
			}

			if(TransactionType.AMEND.equals(poType))
			outputDoc.setAttribute("../AmendSeqNo", purOrder.getAttribute("amend_seq_no"));
			else
			outputDoc.setAttribute("../AmendSeqNo", "00");

			//BSL IR RSUM041136929 04/11/2012 BEGIN
			//outputDoc.setAttribute("../LinkToInstrType", instrument.getAttribute("instrument_type_code"));
			String instrType = instrument.getAttribute("instrument_type_code");

			if (TradePortalConstants.AUTO_LC_PO_UPLOAD.equals(instrType)) {
				// Change IMP_DLC to DLC
				instrType = TradePortalConstants.AUTO_LC_PO_UPLOAD_TPS;
			}

			outputDoc.setAttribute("../LinkToInstrType", instrType);
			//BSL IR RSUM041136929 04/11/2012 END

			outputDoc.setAttribute("../CustomerID", customerID);
			outputDoc.setAttribute("../PurchaseOrderStatus", purOrder.getAttribute("status"));
			outputDoc.setAttribute("../GroupReferenceNumber", processParameters.getAttribute("/POR_param/GroupReferenceNumber"));
			outputDoc.setAttribute("../GroupTotalNo", processParameters.getAttribute("/POR_param/GroupTotalNo"));
			//see if atleast 1 LineItem exists
			ComponentList lineItemsList = (ComponentList) purOrder
			.getComponentHandle("PurchaseOrderLineItemList");
			int itemsTot = lineItemsList.getObjectCount();
			//get derived_line_item value from the 1st line item
			lineItemsList.scrollToObjectByIndex(0);
			BusinessObject busObj = lineItemsList.getBusinessObject();
			outputDoc.setAttribute("../NoOfLineItems", ((Integer)itemsTot).toString());
			outputDoc.setAttribute("../DerivedLineItem", busObj.getAttribute("derived_line_item_ind"));
			//Body Section
			outputDoc.setAttribute("/Proponix/Body/Baseln/PurchsOrdrRef/Id", purOrder.getAttribute("purchase_order_num"));

			outputDoc.setAttribute("../DtOfIsse", date);

			//Buyr
			outputDoc.setAttribute("/Proponix/Body/Baseln/Buyr/OthrSchme", TradePortalConstants.OTLID+customerID);
			outputDoc.setAttribute("../PstlAdr/PstCdId", corpOrg.getAttribute("address_postal_code"));
			outputDoc.setAttribute("../TwnNm", corpOrg.getAttribute("address_city"));
			outputDoc.setAttribute("../Ctry", corpOrg.getAttribute("address_country"));
			//Sellr - terms party. get it from params
			outputDoc.setAttribute("/Proponix/Body/Baseln/Sellr/Nm", purOrder.getAttribute("seller_name"));
			outputDoc.setAttribute("../PstlAdr/StrtNm",  processParameters.getAttribute("/POR_param/SellerAddressLine1"));
			outputDoc.setAttribute("../PstCdId",  processParameters.getAttribute("/POR_param/SellerPostalCode"));
			outputDoc.setAttribute("../TwnNm",  processParameters.getAttribute("/POR_param/SellerCity"));
			outputDoc.setAttribute("../Ctry",  processParameters.getAttribute("/POR_param/SellerCountry"));

			outputDoc.setAttribute("/Proponix/Body/Baseln/BuyrBk/BIC", operationalBankOrganization
					.getAttribute("swift_address_part2"));
			outputDoc.setAttribute("/Proponix/Body/Baseln/SellrBk/BIC", "");//its required in dtd, but no mapping
			//Goods
			outputDoc.setAttribute("../../Goods/GoodsDesc", purOrder.getAttribute("goods_description"));

			outputDoc.setAttribute("../PrtlShipmnt", purOrder.getAttribute("partial_shipment_allowed"));
			if (InstrumentServices.isNotBlank(purOrder
					.getAttribute("latest_shipment_date"))) {
				date = TPDateTimeUtility.packagingInvoiceDate(purOrder
						.getAttribute("latest_shipment_date"));
				outputDoc.setAttribute("../LatstShipmntDt", date);
			}


			busObj = null;
			//BigDecimal totLineAmt = BigDecimal.ZERO;
			for (int i = 0; i < itemsTot; i++) {
				lineItemsList.scrollToObjectByIndex(i);
				busObj = lineItemsList.getBusinessObject();
				outputDoc.setAttribute(
						"/Proponix/Body/Baseln/Goods/LineItmDtls(" + i
								+ ")/LineItmId",
								busObj.getAttribute("line_item_num"));
				outputDoc.setAttribute(
						"../Qty/Val",
								busObj.getAttribute("quantity"));
				outputDoc.setAttribute(
						"../../QtyTlrnce/PlusPct",
								busObj.getAttribute("quantity_variance_plus"));
				outputDoc.setAttribute("../MnsPct",""); //TODO

				outputDoc.setAttribute(
						"../../UnitPric/Amt[Ccy=" + purOrder.getAttribute("currency")
								+ "]",
								busObj.getAttribute("unit_price"));
				
				for (int k = 0; k < 7; k++) {
					String label = busObj.getAttribute("prod_chars_ud" + (k + 1) + "_label");
					if (InstrumentServices.isNotBlank(label)) {
						if( "description".equalsIgnoreCase(label)){
							outputDoc.setAttribute("/Proponix/Body/Baseln/Goods/LineItmDtls(" + i + ")/PdctNm", busObj.getAttribute("prod_chars_ud"	+ (k + 1) + "_value"));
							break; 
						}
					} else
						break;
				}
				
				//PdctChrtcs
				for (int k = 0; k < 7; k++) {
					String label = busObj.getAttribute("prod_chars_ud"
							+ (k + 1) + "_label");
					if (InstrumentServices.isNotBlank(label)) {
						if( !"description".equalsIgnoreCase(label)) {
							outputDoc.setAttribute("/Proponix/Body/Baseln/Goods/LineItmDtls(" + i + ")/PdctChrtcs(" + k + ")/OthrPdctChrtcs/Id",
									busObj.getAttribute("prod_chars_ud" + (k + 1) + "_value"));
							outputDoc.setAttribute(	"../IdTp", label);
						}
					} else
						break;
				}
				BigDecimal lineAmt = new BigDecimal(
						busObj.getAttribute("unit_price"))
						.multiply(new BigDecimal(busObj
								.getAttribute("quantity")));
				//totLineAmt.add(lineAmt);
				outputDoc.setAttribute(
						"/Proponix/Body/Baseln/Goods/LineItmDtls(" + i
						+ ")/TtlAmt[Ccy=" + purOrder.getAttribute("currency")
								+ "]", lineAmt.toString());

			}

			outputDoc.setAttribute(
					"/Proponix/Body/Baseln/Goods/LineItmsTtlAmt[Ccy=" + purOrder.getAttribute("currency")
								+ "]",
					purOrder.getAttribute("amount"));
			outputDoc.setAttribute(
					"../Incotrms/Cd",
					purOrder.getAttribute("incoterm"));
			outputDoc.setAttribute(
					"../Lctn",
					purOrder.getAttribute("incoterm_location"));
			outputDoc.setAttribute(
					"/Proponix/Body/Baseln/Goods/TtlNetAmt[Ccy=" + purOrder.getAttribute("currency")
								+ "]",
					purOrder.getAttribute("amount"));
			//BuyrDfndInf
			for (int l = 0; l < 10; l++) {
				String label = purOrder.getAttribute("buyer_user_def"
						+ (l + 1) + "_label");
				if (InstrumentServices.isNotBlank(label)) {
					outputDoc.setAttribute(
							"/Proponix/Body/Baseln/Goods/BuyrDfndInf(" + l
									+ ")/Labl", label);
					outputDoc.setAttribute(
							"../Inf",
									purOrder.getAttribute("buyer_user_def"
									+ (l + 1) + "_value"));
				} else
					break;
			}
			//SellrDfndInf
			for (int l = 0; l < 10; l++) {
				String label = purOrder.getAttribute("seller_user_def"
						+ (l + 1) + "_label");
				if (InstrumentServices.isNotBlank(label)) {
					outputDoc.setAttribute(
							"/Proponix/Body/Baseln/Goods/SellrDfndInf(" + l
									+ ")/Labl", label);
					outputDoc.setAttribute(
							"../Inf",
									purOrder.getAttribute("seller_user_def"
									+ (l + 1) + "_value"));
				} else
					break;
			}
			outputDoc.setAttribute(
					"/Proponix/Body/Baseln/PmtTerms", "");//required in dtd

LOG.debug(outputDoc.toString());
			return outputDoc;

		} catch (Exception e) {
			mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_MIDDLEWARE,
					TradePortalConstants.CONFIRM_PACKAGER_GENERAL);
			e.printStackTrace();
			return outputDoc;
		}

	}

}