package com.ams.tradeportal.mediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.PropertyResourceBundle;
import java.util.Vector;

import com.ams.tradeportal.common.TPDateTimeUtility;
import com.ams.tradeportal.common.TradePortalConstants;
import com.ams.tradeportal.common.cache.Cache;
import com.ams.tradeportal.common.cache.TPCacheManager;
import com.amsinc.ecsg.frame.AmsConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.frame.MediatorBean;
import com.amsinc.ecsg.frame.MediatorServices;
import com.amsinc.ecsg.frame.ObjectIDCache;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.StringFunction;

/*
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class FXRateUnPackagerMediatorBean extends MediatorBean {
private static final Logger LOG = LoggerFactory.getLogger(FXRateUnPackagerMediatorBean.class);
    
	
	public DocumentHandler execute(DocumentHandler unpackagerDoc, DocumentHandler outputDoc, MediatorServices mediatorServices)
	throws RemoteException, AmsException
	{
		final String BLANK = "   "; 
		String baseCur 	  =  unpackagerDoc.getAttribute("/Proponix/SubHeader/BaseCurrency");
		String fxGroupID  =  unpackagerDoc.getAttribute("/Proponix/SubHeader/FXGroupID");
		String maxSpotRateAmount  =  unpackagerDoc.getAttribute("/Proponix/SubHeader/MaxSpotRateAmount"); // DK CR-640 Rel7.1
		
		if(StringFunction.isBlank(baseCur) || StringFunction.isBlank(fxGroupID))
		{
			mediatorServices.debug("No baseCurrency Code/FX Rate GroupID");
			mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,TradePortalConstants.VALUE_MISSING,"BaseCurrency or FXGroupID", "/Proponix/SubHeader/BaseCurrency or /Proponix/SubHeader/FXGroupID");
		}
		else {
            int len = fxGroupID.length(); 
			if (len < 3) {
				fxGroupID=fxGroupID + BLANK.substring(0,3-len);
			}
			try {
				//Leelavathi - CR-610 - 07-Mar-2011 - Begin
				 String ownershipLevel = TradePortalConstants.getPropertyValue("AgentConfiguration", "fxRateUnpackager.ownershipLevel",TradePortalConstants.OWNER_BANK);

				if(TradePortalConstants.OWNER_BANK.equals(ownershipLevel)){
					if (unpkageFXRateForClientBank (baseCur, fxGroupID, maxSpotRateAmount, unpackagerDoc, outputDoc, mediatorServices)) {
						//refresh cache
						Cache cacheToFlush = (Cache) TPCacheManager.getInstance().getCache(TradePortalConstants.FX_RATE_CACHE);
						if (cacheToFlush != null) {
							cacheToFlush.flush();
						}
					}
				}else if(TradePortalConstants.OWNER_CORPORATE.equals(ownershipLevel)){
					if (unpkageFXRateForCorporate(baseCur, fxGroupID, maxSpotRateAmount, unpackagerDoc, outputDoc, mediatorServices)) {
					//refresh cache
					Cache cacheToFlush = (Cache) TPCacheManager.getInstance().getCache(TradePortalConstants.FX_RATE_CACHE);
					if (cacheToFlush != null) {
						cacheToFlush.flush();
						}
					}
				}
				//Leelavathi - CR-610 - 07-Mar-2011 - End
			}
			catch (Exception e) {
				mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,TradePortalConstants.TASK_NOT_SUCCESSFUL,"Error occured while unpackaging, error: " + e.getMessage());
				e.printStackTrace();
			}
		}
        return outputDoc;
	}
	
	
	/**
	 * @param baseCur
	 * @param fxGroupID
	 * @param unpackagerDoc
	 * @param outputDoc
	 * @param mediatorServices
	 * @return
	 * @throws AmsException
	 */
	//Leelavathi - CR-610 - 07-Mar-2011 - Begin
	protected boolean unpkageFXRateForClientBank(String baseCur, String fxGroupID, String maxSpotRateAmount, DocumentHandler unpackagerDoc, DocumentHandler outputDoc, MediatorServices mediatorServices) throws AmsException {
		long t = System.currentTimeMillis();
		
		String clientBank=unpackagerDoc.getAttribute("/Proponix/Header/ClientBank");
		
		DocumentHandler fxRate = null;
		Vector v1 = unpackagerDoc.getFragments("/Proponix/Body/FXRate");
		Iterator itr1 = v1.iterator();
		try(Connection con =DatabaseQueryBean.connect(true);
			PreparedStatement updStmt = getUpdStmtForClientBank(con);
			PreparedStatement insStmt = getInsStmtForClientBank(con);
			PreparedStatement qryStmt = getQryStmtForClientBank(con)) {
			

			while (itr1.hasNext()) {
				fxRate = (DocumentHandler)itr1.next();
				String cur = fxRate.getAttribute("/FXRateCurrency");
				String sellRate = fxRate.getAttribute("/FXSellRate");
				String nomRate = fxRate.getAttribute("/FXNominalRate");
				String buyRate = fxRate.getAttribute("/FXBuyRate");
				String calcInd = fxRate.getAttribute("/MultiplyDivideInd");
				String maxDealAmount = fxRate.getAttribute("/MaxDealAmount");
				Date date = TPDateTimeUtility.convertDateStringToDate(fxRate.getAttribute("/DateEffective"),"yyyyMMddHHmm");
				Timestamp time = new Timestamp(date.getTime());
				updStmt.setString(1, nomRate);
				updStmt.setString(2, calcInd);
				updStmt.setTimestamp(3, time);
				updStmt.setString(4, buyRate);
				updStmt.setString(5, sellRate);
				updStmt.setString(6, maxSpotRateAmount); // DK CR-640 Rel7.1
				updStmt.setString(7, maxDealAmount); // DK CR-640 Rel7.1
				updStmt.setString(8, baseCur);
				updStmt.setString(9, fxGroupID);
				updStmt.setString(10, cur);
				updStmt.setString(11, clientBank);
				int updCount = updStmt.executeUpdate();
				
				mediatorServices.debug(" updCount: " + updCount + " for currency: " + cur);
				
				if ( updCount <1) {
					//need to insert
					List insertList = getClientBankListForInsert(qryStmt,baseCur,fxGroupID,cur,clientBank);
					Iterator it = insertList.iterator();
					ObjectIDCache oid = ObjectIDCache.getInstance(AmsConstants.OID);
					while (it.hasNext()) {
						String cbOrgOid = (String)it.next();
						insStmt.setLong(1,oid.generateObjectID());
						insStmt.setString(2, cur);
						insStmt.setString(3, nomRate);
						insStmt.setString(4, calcInd);
						insStmt.setTimestamp(5, time);
						insStmt.setString(6, buyRate);
						insStmt.setString(7, sellRate);
						insStmt.setString(8, baseCur);
						insStmt.setString(9, fxGroupID);
						insStmt.setString(10, cbOrgOid);
						insStmt.setString(11, maxSpotRateAmount); // DK CR-640 Rel7.1
						insStmt.setString(12, maxDealAmount); // DK CR-640 Rel7.1
						insStmt.addBatch();
					}
					insStmt.executeBatch();
					mediatorServices.debug("inserted records:  "  + insertList.size());
				}
				
			}
			t= System.currentTimeMillis()-t;
			LOG.info("total time taken in millisecond: " + t );

		} catch (SQLException e) {
			throw new AmsException(e.getMessage());
		}
		return true;
	}
    
	//Leelavathi - CR-610 - 07-Mar-2011 - End
	/**
	 * @param pStmt
	 * @param baseCur
	 * @param fxGroupID
	 * @param cur
	 * @return
	 * @throws SQLException
	 * @throws AmsException
	 */
	
	//Leelavathi - CR-610 - 07-Mar-2011 - Begin
	protected List getClientBankListForInsert(PreparedStatement pStmt, String baseCur, String fxGroupID, String cur, String clientBank) throws SQLException, AmsException {
		List list = new ArrayList();
		
		pStmt.setString(1, clientBank);
		
		ResultSet rs = pStmt.executeQuery();
		while (rs.next()) {
			list.add(rs.getString(1));
		}
		rs.close();
		return list;
	}
	//Leelavathi - CR-610 - 07-Mar-2011 - End
	
	//Leelavathi - CR-610 - 07-Mar-2011 - Begin
	protected PreparedStatement getUpdStmtForClientBank(Connection con) throws SQLException, AmsException {
		return con.prepareStatement("UPDATE FX_RATE F SET F.RATE=?, F.MULTIPLY_INDICATOR=?, F.LAST_UPDATED_DATE=?, F.BUY_RATE=?, F.SELL_RATE=?, F.MAX_SPOT_RATE_AMOUNT=?, F.MAX_DEAL_AMOUNT=? WHERE "+
				"F.BASE_CURRENCY_CODE  = ? AND F.FX_RATE_GROUP = ? AND F.CURRENCY_CODE = ? AND F.P_OWNER_ORG_OID IN (SELECT C.ORGANIZATION_OID FROM CLIENT_BANK C WHERE C.OTL_ID =?) AND F.OWNERSHIP_TYPE = '"+TradePortalConstants.OWNER_TYPE_ADMIN+"'" );
	}
	protected PreparedStatement getInsStmtForClientBank(Connection con) throws SQLException, AmsException {
		return con.prepareStatement("INSERT INTO FX_RATE (FX_RATE_OID, CURRENCY_CODE, RATE, MULTIPLY_INDICATOR, LAST_UPDATED_DATE," +
				" OPT_LOCK, BUY_RATE, SELL_RATE, BASE_CURRENCY_CODE, FX_RATE_GROUP, OWNERSHIP_LEVEL, OWNERSHIP_TYPE, P_OWNER_ORG_OID,MAX_SPOT_RATE_AMOUNT,MAX_DEAL_AMOUNT) values(?,?,?,?,?,1,?,?,?,?,'"+TradePortalConstants.OWNER_BANK+"','"+TradePortalConstants.OWNER_TYPE_ADMIN+"',?,?,?)" );
	}
	protected PreparedStatement getQryStmtForClientBank(Connection con) throws SQLException, AmsException {
		return con.prepareStatement("SELECT C.ORGANIZATION_OID FROM CLIENT_BANK C WHERE C.OTL_ID =?");
		}
	protected boolean unpkageFXRateForCorporate(String baseCur, String fxGroupID, String maxSpotRateAmount,DocumentHandler unpackagerDoc, DocumentHandler outputDoc, MediatorServices mediatorServices) throws AmsException {
		long t = System.currentTimeMillis();
		
		int custCount = DatabaseQueryBean.getCount("base_currency_code", "corporate_org", "base_currency_code = ? and fx_rate_group = ?",false, baseCur, fxGroupID);
		if (custCount <1) {
			LOG.info("No matching corporate customer found");
			return false; //nothing to update
		}
		
		DocumentHandler fxRate = null;
		Vector v1 = unpackagerDoc.getFragments("/Proponix/Body/FXRate");
		Iterator itr1 = v1.iterator();
		try(Connection con =DatabaseQueryBean.connect(true);
			PreparedStatement updStmt = getUpdStmtForCorporate(con);
			PreparedStatement insStmt = getInsStmtForCorporate(con);
			PreparedStatement qryStmt = getQryStmtForCorporate(con)
				) {
			
			while (itr1.hasNext()) {
				fxRate = (DocumentHandler)itr1.next();
				String cur = fxRate.getAttribute("/FXRateCurrency");
				String sellRate = fxRate.getAttribute("/FXSellRate");
				String nomRate = fxRate.getAttribute("/FXNominalRate");
				String buyRate = fxRate.getAttribute("/FXBuyRate");
				String calcInd = fxRate.getAttribute("/MultiplyDivideInd");
				String maxDealAmount = fxRate.getAttribute("/MaxDealAmount");
				Date date = TPDateTimeUtility.convertDateStringToDate(fxRate.getAttribute("/DateEffective"),"yyyyMMddHHmm");
				Timestamp time = new Timestamp(date.getTime());
				updStmt.setString(1, nomRate);
				updStmt.setString(2, calcInd);
				updStmt.setTimestamp(3, time);
				updStmt.setString(4, buyRate);
				updStmt.setString(5, sellRate);
				updStmt.setString(6, maxDealAmount); // DK CR-640 Rel7.1
				updStmt.setString(7, maxDealAmount); // DK CR-640 Rel7.1
				updStmt.setString(8, baseCur);
				updStmt.setString(9, fxGroupID);
				updStmt.setString(10, cur);
				int updCount = updStmt.executeUpdate();
				
				mediatorServices.debug("custCount: "  + custCount + " updCount: " + updCount + " for currency: " + cur);
				
				if (custCount > updCount) {
					//need to insert
					List insertList = getCorpOrgListForInsert(qryStmt,baseCur,fxGroupID,cur);
					Iterator it = insertList.iterator();
					ObjectIDCache oid = ObjectIDCache.getInstance(AmsConstants.OID);
					while (it.hasNext()) {
						String corpOrgOid = (String)it.next();
						insStmt.setLong(1,oid.generateObjectID());
						insStmt.setString(2, cur);
						insStmt.setString(3, nomRate);
						insStmt.setString(4, calcInd);
						insStmt.setTimestamp(5, time);
						insStmt.setString(6, corpOrgOid);
						insStmt.setString(7, buyRate);
						insStmt.setString(8, sellRate);
						insStmt.setString(9, maxSpotRateAmount); // DK CR-640 Rel7.1
						insStmt.setString(10, maxDealAmount); // DK CR-640 Rel7.1
						insStmt.addBatch();
					}
					insStmt.executeBatch();
					mediatorServices.debug("inserted records:  "  + insertList.size());
				}
				
			}
			t= System.currentTimeMillis()-t;
			LOG.info("total time taken in millisecond: " + t );

		} catch (SQLException e) {
			throw new AmsException(e.getMessage());
		}
		
		return true;
	}
    
	
	/**
	 * @param pStmt
	 * @param baseCur
	 * @param fxGroupID
	 * @param cur
	 * @return
	 * @throws SQLException
	 * @throws AmsException
	 */
	protected List getCorpOrgListForInsert(PreparedStatement pStmt, String baseCur, String fxGroupID, String cur) throws SQLException, AmsException {
		List list = new ArrayList();
		pStmt.setString(1, baseCur);
		pStmt.setString(2, fxGroupID);
		pStmt.setString(3, baseCur);
		pStmt.setString(4, fxGroupID);
		pStmt.setString(5, cur);
		ResultSet rs = pStmt.executeQuery();
		while (rs.next()) {
			list.add(rs.getString(1));
		}
		rs.close();
		return list;
	}
	protected PreparedStatement getUpdStmtForCorporate(Connection con) throws SQLException, AmsException {
		
		return con.prepareStatement("UPDATE FX_RATE F SET F.RATE=?, F.MULTIPLY_INDICATOR=?, F.LAST_UPDATED_DATE=?, F.BUY_RATE=?, F.SELL_RATE=?,F.MAX_SPOT_RATE_AMOUNT=?, F.MAX_DEAL_AMOUNT=? WHERE F.FX_RATE_OID IN " +
		" (SELECT F.FX_RATE_OID FROM CORPORATE_ORG O WHERE O.ORGANIZATION_OID = F.P_OWNER_ORG_OID and O.BASE_CURRENCY_CODE = ? and O.fx_rate_group = ? and F.CURRENCY_CODE = ?)" );
	}
	protected PreparedStatement getInsStmtForCorporate(Connection con) throws SQLException, AmsException {
		return con.prepareStatement("insert into FX_RATE (FX_RATE_OID, CURRENCY_CODE, RATE, MULTIPLY_INDICATOR, LAST_UPDATED_DATE," +
		" OPT_LOCK, P_OWNER_ORG_OID, BUY_RATE, SELL_RATE, MAX_SPOT_RATE_AMOUNT, MAX_DEAL_AMOUNT, OWNERSHIP_LEVEL, OWNERSHIP_TYPE) values(?,?,?,?,?,1,?,?,?,?,?'"+TradePortalConstants.OWNER_CORPORATE+"','"+TradePortalConstants.OWNER_TYPE_ADMIN+"')" );
		
	}
	protected PreparedStatement getQryStmtForCorporate(Connection con) throws SQLException, AmsException {
		return con.prepareStatement("select o.organization_oid from corporate_org o where o.base_currency_code = ? and o.fx_rate_group = ? and o.organization_oid not in " +
		"(select F.P_OWNER_ORG_OID from fx_rate F where F.P_OWNER_ORG_OID = o.organization_oid and o.base_currency_code = ? and O.fx_rate_group = ? and F.currency_code = ?)");
	}
	//Leelavathi - CR-610 - 07-Mar-2011 - End

	
}
