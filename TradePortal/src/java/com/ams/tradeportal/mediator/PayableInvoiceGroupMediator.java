package com.ams.tradeportal.mediator;

import javax.ejb.*;

import com.ams.tradeportal.busobj.InvoiceGroup;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.MediatorServices;
import com.amsinc.ecsg.util.DocumentHandler;

import java.rmi.*;
import java.util.Map;

/*
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface PayableInvoiceGroupMediator extends com.amsinc.ecsg.frame.Mediator
{
	public DocumentHandler authorizeEndToEndIDGroup(InvoiceGroup invoiceGroup, Map<String, String> endToEndIdGroupProp, MediatorServices mediatorServices)  
			throws RemoteException, AmsException;
}