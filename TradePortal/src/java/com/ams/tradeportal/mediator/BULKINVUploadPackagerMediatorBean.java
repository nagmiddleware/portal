package com.ams.tradeportal.mediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.PreparedStatement;

import com.ams.tradeportal.busobj.CreditNotes;
import com.ams.tradeportal.busobj.InvoicesSummaryData;
import com.ams.tradeportal.common.TradePortalConstants;
import com.ams.tradeportal.common.TransactionType;
import com.ams.tradeportal.mediator.util.InvoicePackagerUtility;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.frame.MediatorBean;
import com.amsinc.ecsg.frame.MediatorServices;
import com.amsinc.ecsg.util.DocumentHandler;

/*
 * Manages INV Message Packaging..
 *
 *     Copyright  � 2008
 *     American Management Systems, Incorporated
 *     All rights reserved
 */

public class BULKINVUploadPackagerMediatorBean extends MediatorBean {
private static final Logger LOG = LoggerFactory.getLogger(BULKINVUploadPackagerMediatorBean.class);
	
	protected String AGENT_ID = "";
	PreparedStatement pStmt = null;

	/*
	 * This mediator is now used to package message for uploaded invoices in
	 * portal Take the invoices_summary_data OID as input, go to that
	 * invoicesSummaryData object and extract the invoice ID and its status
	 */

	public DocumentHandler execute(DocumentHandler inputDoc,
			DocumentHandler outputDoc, MediatorServices mediatorServices)
			throws RemoteException, AmsException {
		String stm ="INSERT INTO OUTGOING_QUEUE  (QUEUE_OID,DATE_CREATED, STATUS, MSG_TYPE, A_INSTRUMENT_OID, A_TRANSACTION_OID, MESSAGE_ID,PROCESS_PARAMETERS)"
				+ " VALUES (?,?,'" +TradePortalConstants.OUTGOING_STATUS_STARTED+"','" +TradePortalConstants.INVOICE+"',?,?,?,?)";
		try(Connection con = DatabaseQueryBean.connect(true);
			PreparedStatement insStmt = con.prepareStatement(stm); 
			) {

			String processParameters = null;
			String invoiceOid = null;
			String creditNoteInd = null;
			String creditNotePreApplicationStatus = null;
			String creditNoteUpdateInd = null;
			String credAppliedInvDetailOid = null;
			
			processParameters = inputDoc.getAttribute("/process_parameters");
			invoiceOid = getParm("invoice_oid", processParameters);
                        // W Zhu 7/8/2015 Rel 9.3 CR-1006 T36000041041 get invoice_type directly since PayableInvoiceGroupMediatorBean does not pass in the parameter.  
             creditNoteInd = getParm("creditNoteInd", processParameters);
			 String invType = "";
			if(TradePortalConstants.INDICATOR_YES.equals(creditNoteInd)){
				creditNotePreApplicationStatus = getParm("creditNotePreApplicationStatus", processParameters);
				creditNoteUpdateInd = getParm("creditNoteUpdateInd", processParameters);
				credAppliedInvDetailOid = getParm("credAppliedInvDetailOid", processParameters);
				CreditNotes cr = (CreditNotes) mediatorServices.createServerEJB("CreditNotes", Long.parseLong(invoiceOid));
				invType = cr.getAttribute("invoice_type");
			}else{
				 InvoicesSummaryData invoiceSummaryData = (InvoicesSummaryData) mediatorServices.createServerEJB("InvoicesSummaryData", Long.parseLong(invoiceOid));
                  invType = invoiceSummaryData.getAttribute("invoice_type");
			}
			mediatorServices.debug("Ready to Instantiate InvoicesSummaryData Object with oid " + invoiceOid);
			
                        //SHR CR708 Rel8.1.1 Start- modified to call common method to create INV msg

                        //send AMD/INT to TPS
                        // W Zhu 7/8/2015 Rel 9.3 CR-1006 T36000041041 add case for AMD.  
                        // The invoices coming from TPS for apporval could have invoice_type = AMD on invoices_summary_data.  It should be translated to AMD on the INV message.
                        invType = TradePortalConstants.REPLACEMENT.equals(invType) ? TransactionType.AMEND
                                : TransactionType.AMEND.equals(invType) ? TransactionType.AMEND
                                : TradePortalConstants.INITIAL_INVOICE;

                        String processParameter = "|InvoiceOID = " + invoiceOid
                                        + "|invoiceType =" + invType;

                        if(TradePortalConstants.INDICATOR_YES.equals(creditNoteInd)){
                                processParameter = processParameter + "|creditNoteInd ="+ creditNoteInd
                                                + "|creditNotePreApplicationStatus ="+ creditNotePreApplicationStatus
                                                + "|creditNoteUpdateInd ="+ creditNoteUpdateInd
                                                + "|credAppliedInvDetailOid ="+ credAppliedInvDetailOid;
                        }
                        InvoicePackagerUtility.create(inputDoc,  mediatorServices,processParameter,insStmt,con);

                        insStmt.executeBatch();
                        mediatorServices.debug("added Invoices:  "  +invoiceOid);
                      
		} catch (Exception e) {
			LOG.info("exception in BULK" + e);
		}
		return inputDoc;
	}


	public String getParm(String parmName, String processingParms) {

		int locationOfParmName, startOfParmValue, endOfParmValue;
		String parmValue;

		locationOfParmName = processingParms.indexOf(parmName);

		// Search for the whole word only.
		// For example, getParm("portfolio_activity_uoid=0|activity=1",
		// "activity" should return 1.
		// Keep searching if the character before or after the occurrence is not
		// a word delimiter

		while ((locationOfParmName > 0
				&& processingParms.charAt(locationOfParmName - 1) != ' ' && processingParms
				.charAt(locationOfParmName - 1) != '|')
				|| (locationOfParmName >= 0
						&& locationOfParmName < processingParms.length()
								- parmName.length()
						&& processingParms.charAt(locationOfParmName
								+ parmName.length()) != ' ' && processingParms
						.charAt(locationOfParmName + parmName.length()) != '=')) {

			locationOfParmName = processingParms.indexOf(parmName,
					locationOfParmName + parmName.length());

		}

		if (locationOfParmName == -1) {
			return "";
		}

		startOfParmValue = processingParms.indexOf("=", locationOfParmName);

		if (startOfParmValue == -1) {
			return "";
		}

		startOfParmValue++;

		endOfParmValue = processingParms.indexOf("|", startOfParmValue);

		if (endOfParmValue == -1) {
			endOfParmValue = processingParms.length();
		}

		parmValue = processingParms.substring(startOfParmValue, endOfParmValue);

		return parmValue;
	}



}
