package com.ams.tradeportal.mediator;

import javax.ejb.*;
import java.rmi.*;

/*
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface CreateTransactionMediatorHome extends javax.ejb.EJBHome
{

  public CreateTransactionMediator create()
		throws CreateException, RemoteException;}