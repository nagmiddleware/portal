package com.ams.tradeportal.mediator;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;

public interface PurgeUnPackagerMediatorHome extends EJBHome
{
  public PurgeUnPackagerMediator create() throws RemoteException, AmsException, CreateException;
}
