package com.ams.tradeportal.mediator;

import javax.ejb.*;
import java.rmi.*;

import com.amsinc.ecsg.frame.*;

/*
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface ManualPOLineItemsMediatorHome extends javax.ejb.EJBHome
{

  public ManualPOLineItemsMediator create()
		throws CreateException, RemoteException;}