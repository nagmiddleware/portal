package com.ams.tradeportal.mediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.util.*;
import javax.ejb.*;
import java.rmi.*;
import javax.naming.*;
import javax.ejb.*;
import java.util.*;

import com.amsinc.ecsg.frame.*;
import com.ams.tradeportal.busobj.*;
import com.ams.tradeportal.common.*;
import com.ams.tradeportal.busobj.util.*;

/*
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class BankDeleteMessagesMediatorBean extends MediatorBean
{
private static final Logger LOG = LoggerFactory.getLogger(BankDeleteMessagesMediatorBean.class);

   public DocumentHandler execute(DocumentHandler inputDoc, DocumentHandler outputDoc, MediatorServices mediatorServices)
                                  throws RemoteException, AmsException
   {
      // get each Transaction Oid and process each transaction; each transaction is unique within itself
		DocumentHandler messageDoc = null;
		BankMailMessage message = null;
		Vector messageList = null;
		boolean discrepancyIndicator = false;
		String securityRights = null;
		String messageOid = null;
		StringBuffer routeToUserOid = new StringBuffer();
		String routeUserOid = null;
		String userLocale = null;
		String userOid = null;
		String messageTitle = null;
		boolean routeAction = false;
		boolean success = false;
		String routeValidation = null;
		int count = 0;
		String buttonPressed = "";
		String type = "";

      
      // populate variables with inputDocument information such as transaction list, user oid, 
      // and security rights, etc.
      messageList     	   = inputDoc.getFragments("/MessageList/Message");
      userOid              = inputDoc.getAttribute("/User/userOid");
      securityRights       = inputDoc.getAttribute("../securityRights");
      count                = messageList.size();

      mediatorServices.debug("RouteDeleteMessagesMediator: number of transactions: " + count);

     
	   buttonPressed = TradePortalConstants.BUTTON_DELETE;
	   
      if (count <= 0)
      {
      
	  
             mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, 
                                                   TradePortalConstants.NO_ITEM_SELECTED, 
	         mediatorServices.getResourceManager().getText("TransactionAction.Delete",
	    	    			           TradePortalConstants.TEXT_BUNDLE));
	  
         return outputDoc;
      }

   
      for (int i = 0; i < count; i++)
      {
         messageDoc  = (DocumentHandler) messageList.get(i);
         messageOid = messageDoc.getAttribute("/messageOid");
                 
	 success= false;         
         
         if( InstrumentServices.isBlank( messageOid ) ) {
            // This is a new message that has not yet been saved
            message = (BankMailMessage) mediatorServices.createServerEJB("BankMailMessage");
            message.newObject();
         }
         else 
         {
             // create the message and call routeMessage or deleteMessage.
             // this may cause the AmsException or the RemoteException which will
             // be thrown by this method.  
             try
             {
                 message = (BankMailMessage) mediatorServices.createServerEJB("BankMailMessage", Long.parseLong(messageOid));
             }
             catch (Exception e)
             {
                // message must have been deleted - issue error
                mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                     TradePortalConstants.ITEM_DELETED);
                continue;
             }
         }       

         SecurityAccess.validateUserRightsForTrans( type, "", securityRights, buttonPressed );
				
         message.setAttribute("last_update_date", DateTimeUtility.getGMTDateTime() );
		
		message.setAttribute("unread_flag", TradePortalConstants.INDICATOR_YES);

               // If message_source_type isn't set yet (like for new messages), set it to PORTAL
               if(InstrumentServices.isBlank(message.getAttribute("message_source_type")))
                  message.setAttribute("message_source_type", TradePortalConstants.PORTAL); 
      
             
	         messageTitle = message.getAttribute("message_subject");
	
	
	     try
	     {
	     	success = message.deleteMessage(securityRights, userOid);
	     }
	     catch (Exception e)
	     {
		LOG.info("Exception occured in RouteDeleteMessagesMediator: " + e.toString());
		e.printStackTrace();
	     }
	     if (success)
	     {
		mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
		    TradePortalConstants.MESSAGE_PROCESSED, messageTitle,
		    mediatorServices.getResourceManager().getText("MessageAction.Deleted",
	    	    TradePortalConstants.TEXT_BUNDLE));
	    }
	 
      }

      mediatorServices.debug("DeleteRouteTransactionsMediator: " + inputDoc);

      return outputDoc;
   }
}
