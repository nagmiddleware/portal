package com.ams.tradeportal.mediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.common.*;
import com.ams.tradeportal.agents.MQWrapper;
import com.ams.tradeportal.busobj.*;
import com.ams.tradeportal.busobj.util.*;
import com.ams.tradeportal.busobj.webbean.SessionWebBean;
import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;

import java.util.*;
import java.rmi.*;
import java.util.Vector;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.math.BigDecimal;

/*
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class PAYFILEUnPackagerMediatorBean extends MediatorBean {
private static final Logger LOG = LoggerFactory.getLogger(PAYFILEUnPackagerMediatorBean.class);

	/*
	 * Manages PAYFILE Transaction UnPackaging..
	 */

	private static final long MAX_FILE_SIZE = Long.parseLong(TradePortalConstants.getPropertyValue("TradePortal","uploadFileMaxSizeGXS", "10485760")); 
    private static final String MAX_FILE_SIZE_IN_MB = getMaxFileSizeInMB();
    private static final long MAX_FILE_BENES = Long.parseLong(TradePortalConstants.getPropertyValue("TradePortal", "uploadFileMaxBenesGXS", "10000"));

	
	public DocumentHandler execute(DocumentHandler unpackagerDoc, DocumentHandler outputDoc, MediatorServices mediatorServices)
	throws RemoteException, AmsException
	{


		String destinationID =  unpackagerDoc.getAttribute("/Proponix/Header/DestinationID");
		String senderID   =  unpackagerDoc.getAttribute("/Proponix/Header/SenderID");
		String customerID 	  =  unpackagerDoc.getAttribute("/Proponix/SubHeader/CustomerID");
		boolean errorsExists = false;

		try
		{
			validateRequiredField(unpackagerDoc,"Destination ID", "/Proponix/Header/DestinationID", mediatorServices);
			validateRequiredField(unpackagerDoc,"Sender ID", "/Proponix/Header/SenderID", mediatorServices);
			validateSenderID(senderID, mediatorServices);//MEer Rel 9.0 IR-28809 validate Sender Id
			validateRequiredField(unpackagerDoc,"Client Bank", "/Proponix/Header/ClientBank", mediatorServices);
			validateRequiredField(unpackagerDoc,"Date Send", "/Proponix/Header/DateSent", mediatorServices);
			validateRequiredField(unpackagerDoc,"Time Send", "/Proponix/Header/TimeSent", mediatorServices);
			validateRequiredField(unpackagerDoc,"Message ID", "/Proponix/Header/MessageID", mediatorServices);
			validateRequiredField(unpackagerDoc,"Customer ID", "/Proponix/SubHeader/CustomerID", mediatorServices);			
			validateRequiredField(unpackagerDoc,"File Name", "/Proponix/SubHeader/FileName", mediatorServices);
			// Nar CR-694A Rel9.0 validate GXSRef only when sender ID is GXS.
			if(TradePortalConstants.DESTINATION_ID_GXS.equals(senderID))
			{
			  validateRequiredField(unpackagerDoc,"GXS Reference", "/Proponix/SubHeader/GXSRef", mediatorServices);
			  validateMaxLength(unpackagerDoc, "GXS Reference", "/Proponix/SubHeader/GXSRef", 14, mediatorServices); 
			}
			validateMaxLength(unpackagerDoc, "Customer File Reference", "/Proponix/SubHeader/CustomerFileRef", 35, mediatorServices); 
			validateMaxLength(unpackagerDoc, "File Name", "/Proponix/SubHeader/FileName", 40, mediatorServices); 
			
			
			errorsExists = (mediatorServices.getErrorManager().getMaxErrorSeverity () >= ErrorManager.ERROR_SEVERITY );
	
			if(!errorsExists){
				long corp_oid = getCorpOrgOID(customerID,mediatorServices);
				if (corp_oid == -1) {
					handleAccountNotFound(unpackagerDoc, outputDoc, mediatorServices,customerID);
				}
				else {
					CorporateOrganization corp_org = (CorporateOrganization)  mediatorServices.createServerEJB("CorporateOrganization",corp_oid); 
					validateCustomer(corp_org, mediatorServices); 
					unpackagePaymentFile(corp_org, unpackagerDoc,mediatorServices);
				}
			}
			
		} 
		catch (Exception e) {
			if (mediatorServices.getErrorManager().getIssuedErrorsList().size() ==0) {
				mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.ERR_PROCESSING_PAYMENTUPLOAD_FILE);//Rel 9.1 CR 970
			}
			e.printStackTrace();
		}
		
		outputDoc.setAttribute("/Param/DestinationID",destinationID);
		outputDoc.setAttribute("/Param/SenderID",senderID);
		outputDoc.setAttribute("/Param/CustomerID",customerID);

		return outputDoc;
	}

	
	
	/**
	 * @param unpackagerDoc
	 * @param label
	 * @param path
	 * @param mediatorServices
	 * @throws AmsException
	 */
	private void validateRequiredField(DocumentHandler unpackagerDoc, String label, String path, MediatorServices mediatorServices) throws AmsException  {
		
		if(StringFunction.isBlank(unpackagerDoc.getAttribute(path))) 
		{
			mediatorServices.debug("missing required field value: " + path);
			mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,TradePortalConstants.VALUE_MISSING,label, path);
			throw new AmsException("missing required field value: " + path);
		}
	}

	/**
	 * @param unpackagerDoc
	 * @param label
	 * @param path
	 * @param len
	 * @param mediatorServices
	 * @throws AmsException
	 */
	private void validateMaxLength(DocumentHandler unpackagerDoc, String label, String path, int len, MediatorServices mediatorServices) throws AmsException  {
		String val = unpackagerDoc.getAttribute(path);
		if(StringFunction.isNotBlank(val))  
		{
			if (val.length() > len) {
				mediatorServices.debug("max length excedded: " + path);
				mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,TradePortalConstants.MAX_LENGTH_EXCEDDED,label, len +"");
				throw new AmsException("max length excedded: " + path);
			}
		}

	}
	
	/**
	 * @param corp_org
	 * @param mediatorServices
	 * @throws AmsException
	 * @throws RemoteException
	 */
	private void validateCustomer(CorporateOrganization corp_org, MediatorServices mediatorServices) throws AmsException, RemoteException {
		if (!TradePortalConstants.INDICATOR_YES.equals(corp_org.getAttribute("allow_domestic_payments"))) {
			mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,TradePortalConstants.CUSTOMER_NO_ACCESS_PAYMENT);
			throw new AmsException("Payment not enabled for customer...");
		}
		if (StringFunction.isBlank(corp_org.getAttribute("h2h_system_user_oid"))) {
			mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,TradePortalConstants.SYSTEM_USER_NOT_DEFINED);
			throw new AmsException("System User not defined for customer...");
		}
		
	}
	
	/**
	 * @param customerID
	 * @param mediatorServices
	 * @return
	 * @throws AmsException
	 */
	private long  getCorpOrgOID(String customerID, MediatorServices mediatorServices) throws AmsException {
		long corp_org_oid = -1;
		String sql = "select organization_oid  from corporate_org where proponix_customer_id = ? ";

		DocumentHandler resultSet = DatabaseQueryBean.getXmlResultSet(sql, false, new Object[]{customerID});
		mediatorServices.debug("resultSet="+resultSet);
		if (resultSet != null ) {
			Vector resultList = resultSet.getFragments("/ResultSetRecord");
			if (resultList.size() == 1) {     
				corp_org_oid = resultSet.getAttributeLong("//ResultSetRecord/ORGANIZATION_OID");
			} 
		}
		return corp_org_oid;
	}
	
	
	/*
	 *  This method routes the message if route queue is defined. Otherwise, it will log error.
	 * @param unpackagerDoc
	 * @param outputDoc
	 * @param mediatorServices
	 * @param customerID
	 * @throws AmsException
	 */
	private void handleAccountNotFound(DocumentHandler unpackagerDoc, DocumentHandler outputDoc, MediatorServices mediatorServices, String customerID) throws AmsException {
		String routeQueue = getRouteToQueueName();

		if (StringFunction.isBlank(routeQueue)) {
			mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,TradePortalConstants.INVALID_CUSTOMER_ID,customerID);
			LOG.debug("PAYFILE message routing turned off... ");
		}
		else {
			// put message to routeQueue
			MQWrapper mqWrapper = new MQWrapper();
			int rCode = mqWrapper.sendMessageToQueue(unpackagerDoc.getFragment("/Proponix").toString(), routeQueue);
			if (rCode == 1) {
				outputDoc.setAttribute("/Param/messageRouted","true");
			}
			else {
				//IR 16131 start- if there is an error then set the output params
				outputDoc.setAttribute("/Param/messageRouted","false");				
				mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,TradePortalConstants.PAYFILE_FAIL_ROUTE);
				//IR 16131 end
				LOG.info("Error:  Could not route PAYFILE Message to queue " + routeQueue);
			}
		}

	}

	/*
	 * Returns MQ queue name to route message
	 */
	private String getRouteToQueueName() {

		try {
			PropertyResourceBundle properties = (PropertyResourceBundle)ResourceBundle.getBundle("AgentConfiguration");
			return properties.getString("EODMessageRouteToQueue");
		}
		catch (MissingResourceException e){
			return null;
		}
	}


	/**
	 * @param corp_org
	 * @param unpackagerDoc
	 * @param mediatorServices
	 * @throws RemoteException
	 * @throws AmsException
	 */
	private void unpackagePaymentFile(CorporateOrganization corp_org, DocumentHandler unpackagerDoc, MediatorServices mediatorServices)
		throws RemoteException, AmsException
	{
			PaymentFileUpload paymentfile = null;
			File file = null;
			String fileName=unpackagerDoc.getAttribute("/Proponix/SubHeader/FileName");
			String fileContent=unpackagerDoc.getAttribute("/Proponix/Body/MessageDetail");

			if (fileName.length() > AbstractPaymentFileDetail.MAX_FILE_NAME_SIZE) {
				mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, 
				        TradePortalConstants.PAYMENT_FILENAME_SIZE_EXCEEDED);
				return;
			}
			
			InputStream is;
			try {
				is = PGPEncryptDecrypt.verifyAndDecrypt(fileContent);
			} catch (Exception e1) {
				mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.ERROR_DECRYPTING_FILE,e1.getMessage());
				return;
			} 

			
			try  {

				paymentfile = createPaymentFileUpload();
				file = getTempFile(paymentfile);

				if (writetoDisk(is,file,corp_org,mediatorServices.getResourceManager(), mediatorServices)){ 
					paymentfile.setAttribute("validation_status", TradePortalConstants.PYMT_UPLOAD_VALIDATION_PENDING);
					paymentfile.setAttribute("payment_file_name", fileName);
					paymentfile.setAttribute("creation_timestamp", DateTimeUtility.getGMTDateTime(true,false));
					paymentfile.setAttribute("user_oid", corp_org.getAttribute("h2h_system_user_oid"));
					paymentfile.setAttribute("gxs_ref", unpackagerDoc.getAttribute("/Proponix/SubHeader/GXSRef"));
					paymentfile.setAttribute("customer_file_ref", unpackagerDoc.getAttribute("/Proponix/SubHeader/CustomerFileRef"));
					paymentfile.setAttribute("owner_org_oid", corp_org.getAttribute("organization_oid"));
					// Nar CR-694A Rel9.0 05-MAY-2014 ADD - Begin
					paymentfile.setAttribute("h2h_source", unpackagerDoc.getAttribute("/Proponix/Header/SenderID"));
					// Nar CR-694A Rel9.0 05-MAY-2014 ADD - End
					// PMitnala CR-970 Rel9.1 ADD - Begin
					paymentfile.setAttribute("message_id", unpackagerDoc.getAttribute("/Proponix/Header/MessageID"));
					// PMitnala CR-970 Rel9.1 ADD - End
					
					// PMitnala CR-988 Rel9.2 ADD - Begin
					paymentfile.setAttribute("allow_partial_pay_auth", corp_org.getAttribute("allow_partial_pay_auth"));
					// PMitnala CR-988 Rel9.2 ADD - End

					if (paymentfile.save() != 1) {
						throw new AmsException("error saving PaymentFileUpload");
					}
				}else {
					cleanup(file);
				}
			}
			catch(Exception e){
				LOG.info("PaymentFileUploadServlet: Error occured during upload, file:'" + fileName +"' Reason:" + e.getMessage());
				e.printStackTrace();
				cleanup(file);
				if (mediatorServices.getErrorManager().getIssuedErrorsList().size() ==0) {
					mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.ERR_PROCESSING_PAYMENTUPLOAD_FILE);//Rel 9.1 CR 970
				}
			}
		
	}

	
	
	/**
	 * @return
	 * @throws AmsException
	 * @throws RemoteException
	 */
	protected PaymentFileUpload createPaymentFileUpload() throws AmsException, RemoteException {
		LOG.debug("Creating PaymentFileUplaod..");
		PaymentFileUpload paymentfile = (PaymentFileUpload)EJBObjectFactory.createServerEJB(this.mContext, "PaymentFileUpload");
		paymentfile.newObject();
		return paymentfile;
	}
	
	/**
	 * This method returns the Uploaded Payment file
	 *
	 * @param      PaymentFileUpload - 
	 * @return     File
	 * @throws RemoteException
	 * @throws AMSException
	 *  
	 */ 
	protected File getTempFile(PaymentFileUpload paymentfile) throws RemoteException, AmsException {
		LOG.debug("Getting temporary file..");
		File file = null;
		String fileName = paymentfile.getAttribute("payment_file_upload_oid");
		String folder=TradePortalConstants.getPropertyValue("AgentConfiguration", "uploadFolder", "c:/temp/");
		String fname = folder + fileName + PaymentFileDetails.FILE_EXTENSION;

		file = new File(fname);
		
		LOG.debug("Getting temporary file. Filename: " + file.getAbsolutePath());
		
		return file;
	}

	
	/**
	 * This method validates the uploaded file and writes to temporary folder
	 * 
	 * @param item
	 * @param file
	 * @param paymentfile
	 * @param resourceManager
	 * @param userSession
	 * @param mediatorServices
	 * @return
	 * @throws Exception
	 */
	public boolean writetoDisk(InputStream is, File file, CorporateOrganization corp_org, ResourceManager resourceManager, MediatorServices mediatorServices) throws Exception {

		boolean success = true;
		String confidentialInd = null; 
		String paymentMethod = null;
		String currency = null; 
		boolean hasError = false;

		BufferedWriter fs = null;
		BufferedReader reader = null;
		//
		//CR-921
		String fxContractNumber = null;
		String fxRate = null;

		try{
			LOG.debug("Writing file to disk...");

			InputStreamReader isr =	new InputStreamReader(is,AmsConstants.UTF8_ENCODING);
			reader = new BufferedReader(isr);

			fs = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file),AmsConstants.UTF8_ENCODING)); 

			int ctr=0;
			String sLine = null;
			String debitAccountNo = null;
			String paymentCurrency = null;
			String[] headerFields = null;
			String[] payLines = null;
			
			LOG.debug("Processing lines..");
			
			// Read the first line to determine if it's an Invoice file or not.
			sLine = reader.readLine();
			if (sLine!=null && sLine.startsWith("HDR")) {
				// Parse the header line then get the next line from the file.
				headerFields = PaymentFileWithInvoiceDetails.parseHeaderLine(sLine);
				fs.write(sLine);
				fs.newLine();
				sLine = reader.readLine();
			}
			else {
				mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,TradePortalConstants.INVALID_PAYMENT_FILE_FORMAT);
				hasError = true;
			}

			// CJR 4/22/2011 IR RUL042180141 BEGIN 
			// If input line is empty now then there are no 'PAY' lines.
			if(StringFunction.isBlank(sLine)){ 
				mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,TradePortalConstants.INVALID_PAYMENT_FILE_FORMAT);
				hasError = true;
			}
			// CJR 4/22/2011 IR RUL042180141 END
			
			while (( !hasError && (reader != null) && (sLine != null))) {

				if (InstrumentServices.isBlankOrWhiteSpace(sLine)){
					sLine = reader.readLine();
					continue;
				}

				String[] record = null;

					// The firstline should begin with 'PAY', then keep reading lines while additional lines are 'INV'
					if(!sLine.startsWith(TransactionType.PAYMENT)){
						mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,TradePortalConstants.INVALID_PAYMENT_FILE_FORMAT);
						hasError = true;
						break;
					}
					payLines = new String[PaymentFileWithInvoiceDetails.MAX_INV_LINES + 1 ];
					int lineIndex = 0;
					payLines[lineIndex] = sLine;
					sLine = reader.readLine();						
					while(sLine != null && sLine.startsWith(TradePortalConstants.INVOICE)){
						// IR SEUL041149384 Rel7.0.0.0 - 04/18/2011
						if(sLine.length() > PaymentFileWithInvoiceDetails.MAX_INV_DTL_CHAR){
							mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.INV_CHAR_EXCEEDED);
							hasError = true;
                            break;
						}
						// IR SEUL041149384 Rel7.0.0.0 - 04/18/2011
						 if(lineIndex >= PaymentFileWithInvoiceDetails.MAX_INV_LINES){ // manohar - SEUL041149384 - to check max of 100 invoice details
                           mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.INV_LINE_EXCEEDED);
							 hasError = true;
                           break;
						 }	
						
						// manohar - IR SBUL040767216 Rel7.0.0.0 - 05/18/2011 -Begin
							if(!(sLine.startsWith(TransactionType.PAYMENT)||sLine.startsWith(TradePortalConstants.INVOICE))){
								String[] substitutionValues = {"",""};
								substitutionValues[0] = TradePortalConstants.INVOICE_LINE;
								substitutionValues[1] = TradePortalConstants.INVOICE;
								mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.INVALID_PREFIX, substitutionValues, null);
								hasError = true;
								break;
							}// manohar - IR SBUL040767216 Rel7.0.0.0 - 05/18/2011-End
						 
						lineIndex++;
						payLines[lineIndex] = sLine;
						sLine = reader.readLine();
						
					}
					record = PaymentFileWithInvoiceDetails.parsePaymentRecord(headerFields, payLines);


					debitAccountNo = record[PaymentFileWithInvoiceDetails.DEBIT_ACCOUNT_NO];
					paymentCurrency = record[PaymentFileWithInvoiceDetails.PAYMENT_CURRENCY];
					paymentMethod = record[PaymentFileWithInvoiceDetails.PAYMENT_METHOD];
					if (PaymentFileWithInvoiceDetails.validatePaymentLineItemReqFields(paymentMethod, paymentCurrency, debitAccountNo,ctr, resourceManager, mediatorServices)) {
						hasError = true;
						break;
					}
					//manohar - SBUL040548203 - 04/14/2011 - begin
					String executionDate = null;
					executionDate= record[PaymentFileWithInvoiceDetails.EXECUTION_DATE];
					if (PaymentFileWithInvoiceDetails.validateExecutionDate(executionDate, ctr, resourceManager, mediatorServices)) {
						hasError = true;
						break;
					}//manohar - SBUL040548203 - 04/14/2011 - end
					if (PaymentFileWithInvoiceDetails.validatePaymentLineItem(record, ctr, resourceManager, mediatorServices)) {
						hasError = true;
						break;
					}

				if (ctr == 0) {
						currency = record[PaymentFileWithInvoiceDetails.PAYMENT_CURRENCY]; 
						confidentialInd = record[PaymentFileWithInvoiceDetails.CONFIDENTIAL_INDICATOR];
						SessionWebBean userSession = new SessionWebBean();
						userSession.setOwnerOrgOid(corp_org.getAttribute("organization_oid"));
						userSession.setUserOid(corp_org.getAttribute("h2h_system_user_oid"));
						userSession.setSecurityType(TradePortalConstants.NON_ADMIN);
						fxContractNumber = record[PaymentFileWithInvoiceDetails.FX_CONTRACT_NUMBER];
						fxRate = record[PaymentFileWithInvoiceDetails.FX_RATE];
						//Rel9.0 CR-921 - Validate headers including FX Contract Number and Rate
						hasError = PaymentFileWithInvoiceDetails
								.validateHeader(debitAccountNo,paymentCurrency,paymentMethod,currency,confidentialInd,executionDate, ctr, resourceManager, userSession, mediatorServices, true, fxContractNumber, fxRate);
						if(!hasError){
							hasError = PaymentFileWithInvoiceDetails.validatePaymentCurrencyCountry(mediatorServices, record);						
						}

				} else {
					// if over the maximum number of beneficiaries throw an error
					if ((ctr + 1) > MAX_FILE_BENES) {
						mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,TradePortalConstants.PAYMENT_FILE_BENES_EXCEEDED);
						hasError = true;
					}

					if (!paymentMethod.equals(record[PaymentFileWithInvoiceDetails.PAYMENT_METHOD])) {
						mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.PAY_MTHD_NOT_SAME_FOR_BENE);
						hasError = true;
					}

					if (!currency.equals(record[PaymentFileWithInvoiceDetails.PAYMENT_CURRENCY])) {
						mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,TradePortalConstants.PAY_CURRENCY_NOT_SAME_FOR_BENE);
						hasError = true;
					}

				}

				
					int i = 0;
					while(i <= PaymentFileWithInvoiceDetails.MAX_INV_LINES && payLines[i] != null){ // manohar - SEUL041149384 - to check max of 100 invoice details
						fs.write(payLines[i]);
						fs.newLine();
						i++;
					}
				
				ctr++;
			}
			
			if (file.length() > MAX_FILE_SIZE) {
				mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.PAYMENT_FILE_SIZE_EXCEEDED,MAX_FILE_SIZE_IN_MB +"");
				hasError = true;
			}

			success = !hasError;
		} catch (Exception e) {

			e.printStackTrace();
			throw e;

		}
		finally {

			if (fs != null) {
				try {
					fs.close();
				} catch (IOException e) {
					// ignore
				}

			}

			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					// ignore
				}
			}

		}

		return success;
	}

	
	/**
	 * @return
	 */
	protected static String getMaxFileSizeInMB(){
		float f = (float) (MAX_FILE_SIZE/(1024.0 * 1024.0));
		BigDecimal dec = new BigDecimal(f);
		dec = dec.setScale(1,BigDecimal.ROUND_DOWN);
		return dec.toString();
		
	}
	
	/* MEer Rel 9.0 IR-28809
	 * Validate Sender Id for PAYFILE message type.
	 * Sender Id is either GXS or FLA.
	 */
	private void validateSenderID(String senderID, MediatorServices mediatorServices) throws AmsException {
		if(!(TradePortalConstants.DESTINATION_ID_FLA.equalsIgnoreCase(senderID) || TradePortalConstants.DESTINATION_ID_GXS.equalsIgnoreCase(senderID)) ){
			mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,TradePortalConstants.INVALID_SENDER_ID, senderID);
			LOG.debug("SenderID is not valid... ");					
		}
	}


	
	/**
	 * Performs cleanup
	 *
	 * @param file
	 *
	 * @return
	 */
	protected void cleanup(File file) {
		LOG.debug("Inside cleanup..");
		//delete file 
		if (file !=null && !file.delete()) {

			LOG.info("Error: Could not delete file: " + file.getAbsoluteFile());
		}

	}

}
