/*
 * Created on Oct 10, 2007
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.ams.tradeportal.mediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import com.ams.tradeportal.common.TradePortalConstants;
import java.rmi.RemoteException;
/**

 * 
 * Copyright  � 2001
 * American Management Systems, Incorporated
 * All rights reserved
 */
public class REFDATAUnPackagerMediatorBean extends MediatorBean {
private static final Logger LOG = LoggerFactory.getLogger(REFDATAUnPackagerMediatorBean.class);

	public DocumentHandler execute(DocumentHandler unpackagerDoc, DocumentHandler outputDoc, MediatorServices mediatorServices)
																			throws RemoteException, AmsException
	 {

	   String tableType = null;
	   String refTableCode = null;
	   String localeName = null;
	   String description = null;
	   String additionalValue = null; //Pratiksha CR-507 change
	   String whereClause = null;
	   String insertQuery = null;
	   String localeNameString = null;
	   String destinationTable = null;
	   String clientBankID = null;
	   int count = 0;
	   int insertCount = 0;
	   try{
		   tableType = unpackagerDoc.getAttribute("/Proponix/Body/TableType");
		   refTableCode = unpackagerDoc.getAttribute("/Proponix/Body/Code");
		   localeName = unpackagerDoc.getAttribute("/Proponix/Body/LocaleName");
		   description = unpackagerDoc.getAttribute("/Proponix/Body/Description");
		   additionalValue = unpackagerDoc.getAttribute("/Proponix/Body/AdditionalValue"); //CR-507 change
		   destinationTable = unpackagerDoc.getAttribute("/Proponix/Body/DestinationTable");

		   if(StringFunction.isBlank(localeName)) 
				localeNameString = "is NULL";
		   else 
				localeNameString = " ='" + localeName + "'";
		   
		   whereClause  = " table_type = ?" +
		   				  " and code = ?" +
		   				  " and locale_name " + localeNameString ;
		   //CR 741. Use different tables based on destinationTableType
		   //IR T36000019414 - destinationTable can be null here
		   if("BANKREFDATA".equals(destinationTable)){
			   clientBankID = unpackagerDoc.getAttribute("/Proponix/Body/ClientBankID");
			   whereClause = whereClause +" and client_bank_id = ? ";
			   count = DatabaseQueryBean.getCount("*","bankrefdata",whereClause, false, tableType, refTableCode, clientBankID);
			   if(count == 0){
				   if(localeName.equals("") || localeName == null){		
					   //Create the entry in RefData table with empty LocaleName	 
					   insertQuery = "insert into bankrefdata values(?,?,NULL,?,?,?)";  
					   insertCount = DatabaseQueryBean.executeUpdate(insertQuery,true, new Object[]{tableType, refTableCode, description, additionalValue, clientBankID});  		
				   }else{
					   //	Create the entry in RefData table with LocaleName sent in the XML message from OTL
					   insertQuery = "insert into bankrefdata values(?,?,?,?,?,?)"; 
					   insertCount = DatabaseQueryBean.executeUpdate(insertQuery,true, new Object[]{tableType, refTableCode, localeName, description, additionalValue, clientBankID});

					   // Create the entry in RefData table with NULL LocaleName if does not exist
					   whereClause  = " table_type = ? and code = ? and locale_name is NULL and client_bank_id = ?";
					   LOG.info("WhereClause for NULL Insert %%%%%%%%>" + whereClause);
					   count = DatabaseQueryBean.getCount("*","bankrefdata",whereClause, false, new Object[]{tableType,refTableCode, clientBankID});
					   LOG.info("Count for NULL Insert %%%%%%%%>" + count);
					   if(count == 0){
						   insertQuery = "insert into bankrefdata values(?,?,NULL ,?,?,?)"; 
						   insertCount = DatabaseQueryBean.executeUpdate(insertQuery,true,  new Object[]{tableType,refTableCode,description,additionalValue,clientBankID});	
					   }
				   }

			   }else if(count > 0){

				   if(additionalValue == null || additionalValue.equals("")){

					   // Update the description of the ref data 		  
					   LOG.info("update bankrefdata set descr='" + description + "' where " + whereClause);
					   DatabaseQueryBean.selectRowsForUpdate("BANKREFDATA","code",whereClause,"update BANKREFDATA set descr=? " +  
							    " where " + whereClause,false, new Object[]{tableType, refTableCode, clientBankID}, new Object[]{description,tableType, refTableCode, clientBankID});

				   }
				   else
				   {
					   // Update the description of the ref data 		  
					   LOG.info("update bankrefdata set descr='" + description + "', ADDL_VALUE ='" + additionalValue + "' where " + whereClause);
					   DatabaseQueryBean.selectRowsForUpdate("BANKREFDATA","code",whereClause,"update BANKREFDATA set descr=?, ADDL_VALUE =? where " + whereClause,false, new Object[]{tableType, refTableCode,clientBankID}, new Object[]{description,additionalValue,tableType, refTableCode,clientBankID});
				   }
			   }

		   }
		   else{
			   count = DatabaseQueryBean.getCount("*","refdata",whereClause, false, new Object[]{tableType, refTableCode});
			   if(count == 0){
				   if(localeName.equals("") || localeName == null){		
					   //Create the entry in RefData table with empty LocaleName	 
					   insertQuery = "insert into refdata values(?,?,NULL,?,?)";  

					   insertCount = DatabaseQueryBean.executeUpdate(insertQuery,true, new Object[]{tableType, refTableCode,description,additionalValue});  		

				   }else{
					   //	Create the entry in RefData table with LocaleName sent in the XML message from OTL
					   insertQuery = "insert into refdata values(?,?,?,?,?)"; 
					   insertCount = DatabaseQueryBean.executeUpdate(insertQuery,true,false, new Object[]{tableType,refTableCode,localeName, description, additionalValue} );

					   // Create the entry in RefData table with NULL LocaleName if does not exist
					   whereClause  = " table_type = ?  and code =? and locale_name is NULL" ;
					   LOG.info("WhereClause for NULL Insert %%%%%%%%>" + whereClause);
					   count = DatabaseQueryBean.getCount("*","refdata",whereClause , false, new Object[]{tableType,refTableCode});
					   LOG.info("Count for NULL Insert %%%%%%%%>" + count);
					   if(count == 0){
						   insertQuery = "insert into refdata values(?,?,NULL ,?,?)";
						   insertCount = DatabaseQueryBean.executeUpdate(insertQuery,true, new Object[]{tableType,refTableCode,description,additionalValue});	
					   }

				   }

			   }else if(count > 0){

				   if(additionalValue == null || additionalValue.equals("")){

					   // Update the description of the ref data 		  
					   LOG.info("update refdata set descr=? where " + whereClause);
					   DatabaseQueryBean.selectRowsForUpdate("REFDATA","code",whereClause,"update REFDATA set descr=?"  
							   + " where " + whereClause,false, new Object[]{tableType, refTableCode}, new Object[]{description,tableType, refTableCode});

				   }
				   else
				   {
					   // Update the description of the ref data 		  
					   LOG.info("update refdata set descr='" +description  + "', ADDL_VALUE ='" + additionalValue + "' where " + whereClause);
					   DatabaseQueryBean.selectRowsForUpdate("REFDATA","code",whereClause,"update REFDATA set descr=?, ADDL_VALUE =? where " + whereClause,false, new Object[]{tableType, refTableCode}, new Object[]{description,additionalValue,tableType, refTableCode});
				   }
			   }
		   }
	   }catch (AmsException e) {
			mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,TradePortalConstants.TASK_NOT_SUCCESSFUL,"Message Unpackaging");
			 e.printStackTrace();
       }catch (Exception e) {
		    mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,TradePortalConstants.TASK_NOT_SUCCESSFUL,"Message Unpackaging");
			  e.printStackTrace();
	   }
       // W Zhu 2/11/2008 NCUH121502535 refresh ref data manager
       JPylonProperties jPylonProperties = JPylonProperties.getInstance();
       String serverLocation = jPylonProperties.getString ("serverLocation");
       ReferenceDataManager.getRefDataMgr(serverLocation).refresh();
   	   // W Zhu 2/11/2008 NCUH121502535 END
       
		return outputDoc;
	 }
	 
}
