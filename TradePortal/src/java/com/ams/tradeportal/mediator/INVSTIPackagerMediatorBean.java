package com.ams.tradeportal.mediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.common.*;
import com.ams.tradeportal.busobj.*;
import com.ams.tradeportal.busobj.util.*;
import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import javax.ejb.*;
import java.rmi.*;



/*
 * Manages INVSTI Message Packaging..
 *
 *     Copyright  � 2008
 *     American Management Systems, Incorporated
 *     All rights reserved
 */

public class INVSTIPackagerMediatorBean extends MediatorBean {
private static final Logger LOG = LoggerFactory.getLogger(INVSTIPackagerMediatorBean.class);

   /*
    * This mediator is now used to package message for invoices in portal
    * Take the  invoice OID as input, go to that invoice object and extract
    * the invoice ID and its status
    */

  public DocumentHandler execute(DocumentHandler inputDoc, DocumentHandler outputDoc, MediatorServices mediatorServices)
                                                                                    throws RemoteException, AmsException
  {
   try {

	String processParameters	= null;
        String invoiceOid 		= null;
        String corp_org_oid 		= null;
        String bank_org_oid 		= null;
        String messageID   		= null;
        String operationOrganizationID 	= null;
        String invReferenceID 		= null;
        String invMessageSubType 	= null;
        String invCustomerID		= null;
        String invStatus		= null;

        String messageType 	= MessageType.INVSTI; 
        String destinationID    = TradePortalConstants.DESTINATION_ID_PROPONIX;
	String senderID         = TradePortalConstants.SENDER_ID;    // NSX CR-542 03/04/10
	String dateSent         = null;
	String timeSent         = null;
	String timeStamp   	= DateTimeUtility.getGMTDateTime(false);

        OperationalBankOrganization  operationalBankOrganization = null;
        CorporateOrganization corpOrg = null;


        processParameters = inputDoc.getAttribute("/process_parameters");
        invoiceOid = getParm("invoice_oid", processParameters);

        messageID = inputDoc.getAttribute("/message_id");

	dateSent  =  timeStamp;
        timeSent  =  timeStamp.substring(11,19);

        //Get Invoice Object
        mediatorServices.debug("Ready to Instantiate Invoice Object with oid " + invoiceOid);
	Invoice  invoice  = (Invoice)  mediatorServices.createServerEJB("Invoice",  Long.parseLong(invoiceOid));

        invReferenceID = invoice.getAttribute("invoice_reference_id");

        invStatus = getParm("status", processParameters); //PPrakash

		 if(TradePortalConstants.INVOICE_STATUS_FINANCE_REQUESTED.equals(invStatus)){
		         invMessageSubType = TradePortalConstants.MSG_SUBTYPE_FINANCEREQUESTED;
		        }
		        // PPrakash
		        else if( TradePortalConstants.INVOICE_STATUS_DISPUTED.equals(invStatus)){
		        //	else if(invStatus == TradePortalConstants.INVOICE_STATUS_DISPUTED){
		        //PPRAKASH
		           invMessageSubType = TradePortalConstants.MSG_SUBTYPE_DISPUTED;
		        }
		        // PPrakash
		        else if( TradePortalConstants.INVOICE_STATUS_UNDISPUTED.equals(invStatus)){
		        //else if(invStatus == TradePortalConstants.INVOICE_STATUS_OPEN){
		        //PPRAKASH
		        	invMessageSubType = TradePortalConstants.MSG_SUBTYPE_UNDISPUTED;
		        }
		        else if(TradePortalConstants.INVOICE_STATUS_CLOSED.equals(invStatus)){
		         invMessageSubType = TradePortalConstants.MSG_SUBTYPE_CLOSED;
        }

        //Find out the corporate org and instantiate corporate org
        corp_org_oid = invoice.getAttribute("corp_org_oid");
        mediatorServices.debug("Ready to instantiate corporate org " );
        corpOrg = (CorporateOrganization) mediatorServices.createServerEJB("CorporateOrganization",Long.parseLong(corp_org_oid));

        invCustomerID = corpOrg.getAttribute("Proponix_customer_id");

        //Find the operational org from the corporate org just instantiated
        bank_org_oid = corpOrg.getAttribute("first_op_bank_org");
        mediatorServices.debug("Ready to instantiate operational bank org " );
        operationalBankOrganization = (OperationalBankOrganization) mediatorServices.createServerEJB("OperationalBankOrganization", Long.parseLong(bank_org_oid));
        operationOrganizationID = operationalBankOrganization.getAttribute("Proponix_id");


        outputDoc.setAttribute("/Proponix/Header/DestinationID",            destinationID);
        outputDoc.setAttribute("/Proponix/Header/SenderID",                 senderID);
        outputDoc.setAttribute("/Proponix/Header/OperationOrganizationID",  operationOrganizationID);
        outputDoc.setAttribute("/Proponix/Header/MessageType",              messageType);
        outputDoc.setAttribute("/Proponix/Header/DateSent",                 dateSent);
        outputDoc.setAttribute("/Proponix/Header/TimeSent",                 timeSent);
        outputDoc.setAttribute("/Proponix/Header/MessageID",                messageID);

        outputDoc.setAttribute("/Proponix/SubHeader/CustomerID",            invCustomerID);
        outputDoc.setAttribute("/Proponix/SubHeader/MsgSubType",            invMessageSubType);


        outputDoc.setAttribute("/Proponix/Body/Invoice/InvoiceId",          invReferenceID);

        return outputDoc;
      }
      catch (Exception e) {
            mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE, TradePortalConstants.CONFIRM_PACKAGER_GENERAL);
            e.printStackTrace();
            return outputDoc;
      }

  }// end of execute()



    public static String getParm(String parmName, String processingParms) {

        int locationOfParmName, startOfParmValue, endOfParmValue;
        String parmValue;

        locationOfParmName = processingParms.indexOf(parmName);

        // Search for the whole word only.
        // For example, getParm("portfolio_activity_uoid=0|activity=1", "activity" should return 1.
        // Keep searching if the character before or after the occurance is not a word delimitor

        while ((locationOfParmName > 0
                && processingParms.charAt(locationOfParmName - 1) != ' '
                && processingParms.charAt(locationOfParmName - 1) != '|')
                || (locationOfParmName >= 0
                && locationOfParmName < processingParms.length() - parmName.length()
                && processingParms.charAt(locationOfParmName + parmName.length()) != ' '
                && processingParms.charAt(locationOfParmName + parmName.length()) != '=')) {

            locationOfParmName = processingParms.indexOf(parmName, locationOfParmName + parmName.length());

        }

        if (locationOfParmName == -1) {
            return "";
        }

        startOfParmValue = processingParms.indexOf("=", locationOfParmName);

        if (startOfParmValue == -1 ) {
            return "";
        }

        startOfParmValue++;

        endOfParmValue = processingParms.indexOf("|", startOfParmValue);

        if (endOfParmValue == -1) {
            endOfParmValue = processingParms.length();
        }

        parmValue = processingParms.substring(startOfParmValue , endOfParmValue);

        return parmValue;
    }


}// end of INVSTIPackagerMediatorBean class


