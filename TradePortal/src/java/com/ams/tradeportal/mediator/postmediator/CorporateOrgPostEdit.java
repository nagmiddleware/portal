package com.ams.tradeportal.mediator.postmediator;
import java.util.Hashtable;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.ResourceManager;
import com.amsinc.ecsg.util.StringFunction;
import com.amsinc.ecsg.web.AmsServletInvocation;
import com.amsinc.ecsg.web.BeanManager;
import com.amsinc.ecsg.web.FormManager;

import java.util.HashMap;
import java.util.Vector;
/**
*
*
*     Copyright  � 2001
*     American Management Systems, Incorporated
*     All rights reserved
*/
public class CorporateOrgPostEdit implements com.amsinc.ecsg.web.WebAction {
private static final Logger LOG = LoggerFactory.getLogger(CorporateOrgPostEdit.class);



 public void act(AmsServletInvocation reqInfo,
				BeanManager beanMgr,
				FormManager formMgr,
				ResourceManager resMgr,
				String serverLocation,
				HttpServletRequest request,
				HttpServletResponse response,
				Hashtable inputParmMap,
				DocumentHandler inputDoc)
			throws com.amsinc.ecsg.frame.AmsException
 {
   /**
    * In the corresponding pre-mediator action, decimal values are converted
    * from their locale specific formats into a non-localized format.  Because
    * the JSP expects to see in the XML the values that it sent, the original
    * (formatted for the locale) values must be placed back into their original
    * locations.
    */
	
		DocumentHandler outdoc=formMgr.getFromDocCache(reqInfo.getDocCacheName ());
		String organization_oid = outdoc.getAttribute("/In/CorporateOrganization/organization_oid");
		if ("0".equals(organization_oid)) {
		   organization_oid = outdoc.getAttribute("/Out/CorporateOrganization/organization_oid");
		}
		
		if (StringFunction.isNotBlank(organization_oid)) {
			// populate account oid so the accounts are not counted twice in getComponentHandle 
			DocumentHandler doc = DatabaseQueryBean.getXmlResultSet("select account_oid, account_number,currency from account where p_owner_oid = ?", true, new Object[]{organization_oid});
			HashMap map = new HashMap();
			DocumentHandler accountDoc = null;
			Vector results = doc.getFragments("/ResultSetRecord");
			int noOfItems = results.size();
			for (int iLoop=0; iLoop<noOfItems; iLoop++)
			{
				accountDoc = (DocumentHandler) results.elementAt(iLoop);
				String oid = accountDoc.getAttribute ("/ACCOUNT_OID");
				String acctNum = accountDoc.getAttribute ("/ACCOUNT_NUMBER");
				String cur = accountDoc.getAttribute ("/CURRENCY");
				map.put(cur + "|" + acctNum, oid);
			}

			results = outdoc.getFragments("/In/CorporateOrganization/AccountList");
			noOfItems = results.size();
			for (int iLoop=0; iLoop<noOfItems; iLoop++)
			{
				accountDoc = (DocumentHandler) results.elementAt(iLoop);
				String acctNum = accountDoc.getAttribute ("/account_number");
				String cur = accountDoc.getAttribute ("/currency");
				String oid = (String)map.get(cur + "|"+ acctNum);
				accountDoc.setAttribute("/account_oid", oid);
			}
		}
  }

}