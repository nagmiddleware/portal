
	package com.ams.tradeportal.mediator.postmediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



	import com.amsinc.ecsg.web.*;
	import javax.servlet.http.*;
	import java.util.*;

	import com.amsinc.ecsg.frame.ErrorManager;
	import com.amsinc.ecsg.util.*;
	import com.ams.tradeportal.busobj.webbean.SessionWebBean;
	import com.ams.tradeportal.common.*;
	import com.ams.tradeportal.busobj.util.*;

	public class FavoriteLinkPostEdit implements com.amsinc.ecsg.web.WebAction {
private static final Logger LOG = LoggerFactory.getLogger(FavoriteLinkPostEdit.class);



	 public void act(AmsServletInvocation reqInfo, 
					BeanManager beanMgr, 
					FormManager formMgr, 
					ResourceManager resMgr, 
					String serverLocation, 
					HttpServletRequest request, 
					HttpServletResponse response, 
					Hashtable inputParmMap,
					DocumentHandler inputDoc)
				throws com.amsinc.ecsg.frame.AmsException
	 {
	   /**
	    * In the corresponding pre-mediator action, decimal values are converted
	    * from their locale specific formats into a non-localized format.  Because
	    * the JSP expects to see in the XML the values that it sent, the original
	    * (formatted for the locale) values must be placed back into their original
	    * locations.
	    */

/*	 cquinton 11/16/2012 do nothing here, its now done on instrument view    
		 // set the Mediator output to /In fragment if no error.
		 // The transaction page expects the results to be returned in /In fragment.
	     DocumentHandler outputDocument = formMgr.getFromDocCache(reqInfo.getDocCacheName ());
	     String errorSeverity = outputDocument.getAttribute ("/Error/maxerrorseverity");

	     if (errorSeverity != null  && Integer.parseInt (errorSeverity) < ErrorManager.ERROR_SEVERITY)
	     {
	    	 SessionWebBean sessionWebBean = (SessionWebBean)request.getSession().getAttribute("userSession");
	    	 Vector<DocumentHandler> favoriteList = inputDoc.getFragments("/In/FavoriteTaskCustomization/fav_list");
		     DocumentHandler favoriteDoc = null;
		     Integer displayOrder;
		     String favoriteID = null;
		     String addlParams = null;
		      
		     FavoriteTaskObject favTaskOb = null;
		     String favoriteType = inputDoc.getAttribute("/In/FavoriteTaskCustomization/favorite_type");
		     ArrayList<FavoriteTaskObject> selectionData = new ArrayList<FavoriteTaskObject>();
	    	 for (int i = 0; i < favoriteList.size(); i++) {
	    		 	favTaskOb = new FavoriteTaskObject();
					// Retrieve the next item	    		 
					favoriteDoc = (DocumentHandler) favoriteList.elementAt(i);
					displayOrder = Integer.parseInt(favoriteDoc.getAttribute("/display_order"));
					 if("T".equals(favoriteType)){
						 favoriteID = favoriteDoc.getAttribute("/favorite_id").replace("DOT",".");
					 }else{
						 favoriteID = favoriteDoc.getAttribute("/favorite_id");
					 }
					addlParams = favoriteDoc.getAttribute("/addl_params");
					favTaskOb.setDisplayOrder(displayOrder);
					favTaskOb.setFavId(favoriteID);
					favTaskOb.setAddlParams(addlParams);
	    	        selectionData.add(favTaskOb);
	        }	   
	    	 sessionWebBean.setFavoriteTask(favoriteType, selectionData);	    	 	    		    	 
	   }
	   */
	}
  }	 