package com.ams.tradeportal.mediator.postmediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *    rbhaduri - 3rd March, 06 - CR239
 *
 *     Copyright  � 2006                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */

import com.amsinc.ecsg.web.*;
import javax.servlet.http.*;
import java.util.*;

import com.amsinc.ecsg.frame.ErrorManager;
import com.amsinc.ecsg.util.*;
import com.ams.tradeportal.common.*;

public class NewPartyFromSearchPostEdit implements com.amsinc.ecsg.web.WebAction {
private static final Logger LOG = LoggerFactory.getLogger(NewPartyFromSearchPostEdit.class);

    public void act(AmsServletInvocation reqInfo, 
	                BeanManager beanMgr, 
                    FormManager formMgr, 
                    ResourceManager resMgr, 
                    String serverLocation, 
                    HttpServletRequest request, 
                    HttpServletResponse response, 
                    Hashtable inputParmMap,
                    DocumentHandler inputDoc)
            throws com.amsinc.ecsg.frame.AmsException {
	 
        //if no error, set the output oid on the /In document
        //The transaction page expects the results to be returned in /In fragment.
        DocumentHandler outputDocument = formMgr.getFromDocCache(reqInfo.getDocCacheName ());
        String errorSeverity =
            outputDocument.getAttribute ("/Error/maxerrorseverity");
        if (errorSeverity != null  && Integer.parseInt (errorSeverity) < ErrorManager.ERROR_SEVERITY) {
            String newPartyOid = inputDoc.getAttribute("/Out/Party/party_oid");
            //only set if we got it -- it is set on insert, not update
            if ( newPartyOid!=null && newPartyOid.length()>0 ) { 
                inputDoc.setAttribute("/In/NewPartyFromSearchOutput/PartyOid", newPartyOid);
            }
        }
    }
}
