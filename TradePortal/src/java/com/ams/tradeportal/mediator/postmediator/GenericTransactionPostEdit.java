package com.ams.tradeportal.mediator.postmediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */

import com.amsinc.ecsg.web.*;
import javax.servlet.http.*;
import java.util.*;
import com.amsinc.ecsg.util.*;
import com.ams.tradeportal.common.*;
import com.ams.tradeportal.busobj.webbean.*;
import com.amsinc.ecsg.frame.ErrorManager;

public class GenericTransactionPostEdit implements com.amsinc.ecsg.web.WebAction {
private static final Logger LOG = LoggerFactory.getLogger(GenericTransactionPostEdit.class);



 public void act(AmsServletInvocation reqInfo, 
				BeanManager beanMgr, 
				FormManager formMgr, 
				ResourceManager resMgr, 
				String serverLocation, 
				HttpServletRequest request, 
				HttpServletResponse response, 
				Hashtable inputParmMap,
				DocumentHandler inputDoc)
			throws com.amsinc.ecsg.frame.AmsException
 {
   /**
    * In the corresponding pre-mediator action, decimal values are converted
    * from their locale specific formats into a non-localized format.  Because
    * the JSP expects to see in the XML the values that it sent, the original
    * (formatted for the locale) values must be placed back into their original
    * locations.
    */
    doDecimalPostMediatorAction(inputDoc, resMgr, "amount");
    doDecimalPostMediatorAction(inputDoc, resMgr, "fin_roll_partial_pay_amt");
    doDecimalPostMediatorAction(inputDoc, resMgr, "settle_fec_rate");
    saveMyRecentInstrumtntInSession(reqInfo, formMgr, inputDoc, resMgr, beanMgr, request);
    
 }



   /**
    * In the corresponding pre-mediator action, decimal values are converted
    * from their locale specific formats into a non-localized format.  Because
    * the JSP expects to see in the XML the values that it sent, the original
    * (formatted for the locale) values must be placed back into their original
    * locations.
    *
    * @param inputDoc DocumentHandler - the input document of the mediator
    * @param resMgr ResourceManager - used to determine locale of the user
    * @param attribute String - the decimal attribute being handled
    */    
   public void doDecimalPostMediatorAction(DocumentHandler inputDoc, ResourceManager resMgr, String attribute)
    {
      // Set the amount field in the XML to contain the actual amount
      // field entered by the user (This is what the JSP expects)
      // The user-entered amount was placed into a temporary location
      // (/In/Terms/userEnteredAmount) in the corresponding pre-mediator
      if(inputDoc.getAttribute("/In/Terms/userEntered"+attribute) != null)
       {
         inputDoc.setAttribute("/In/Terms/"+attribute, inputDoc.getAttribute("/In/Terms/userEntered"+attribute));
       }
    }
   
   /**
    * In the post-mediator action, the instrument details are put into session to show in the my links recent transaction.
    *
    * @param inputDoc DocumentHandler - the input document of the mediator
    * @param resMgr ResourceManager - used to determine locale of the user
    * @param attribute String - the decimal attribute being handled
    */    
   public void saveMyRecentInstrumtntInSession(AmsServletInvocation reqInfo, FormManager formMgr, DocumentHandler inputDoc, ResourceManager resMgr,
		   BeanManager beanMgr, HttpServletRequest request)
    {
	// set the Mediator output to /In fragment if no error.
		 // The transaction page expects the results to be returned in /In fragment.
       /* cquinton 11/19/2012 comment this out as recent instruments
        * should be set on instrument view not edit...
	    DocumentHandler outputDocument = formMgr.getFromDocCache(reqInfo.getDocCacheName ());
	    String errorSeverity = outputDocument.getAttribute ("/Error/maxerrorseverity");

	    if (errorSeverity != null  && Integer.parseInt (errorSeverity) < ErrorManager.ERROR_SEVERITY)
	    {
	     String compleInstr = null;
	     String instrumentOId = null;
	   	 SessionWebBean sessionWebBean = (SessionWebBean)request.getSession().getAttribute("userSession");
	   	 if(inputDoc != null){
	   		 instrumentOId = inputDoc.getAttribute("/In/Instrument/instrument_oid");
	   		 InstrumentWebBean instrumentB = (InstrumentWebBean) 	beanMgr.getBean("Instrument");
	   		 compleInstr = instrumentB.getAttribute("complete_instrument_id");
	   		 HashMap <String, String>instrument = new HashMap<String, String>();
	   		 if(compleInstr != null && instrumentOId != null){
		   		 instrument.put(instrumentOId, compleInstr); 
		   		 sessionWebBean.setRecentInstrumentsStack(instrument);
	   		 }
	   	 }
	   }
	   */
    }
 
}