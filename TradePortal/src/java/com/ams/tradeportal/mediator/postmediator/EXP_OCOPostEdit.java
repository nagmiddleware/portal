package com.ams.tradeportal.mediator.postmediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */

import com.amsinc.ecsg.web.*;
import javax.servlet.http.*;
import java.util.*;
import com.amsinc.ecsg.util.*;
import com.ams.tradeportal.common.*;

//Vasavi CR 524 03/31/2010 
public class EXP_OCOPostEdit extends GenericTransactionPostEdit implements com.amsinc.ecsg.web.WebAction {
private static final Logger LOG = LoggerFactory.getLogger(EXP_OCOPostEdit.class);



 public void act(AmsServletInvocation reqInfo, 
				BeanManager beanMgr, 
				FormManager formMgr, 
				ResourceManager resMgr, 
				String serverLocation, 
				HttpServletRequest request, 
				HttpServletResponse response, 
				Hashtable inputParmMap,
				DocumentHandler inputDoc)
			throws com.amsinc.ecsg.frame.AmsException
 {

	String value = inputDoc.getAttribute("/In/Transaction/transaction_type_code");

        doDecimalPostMediatorAction(inputDoc, resMgr, "amount");  
        doDecimalPostMediatorAction(inputDoc, resMgr, "tenor_1_amount");
        doDecimalPostMediatorAction(inputDoc, resMgr, "tenor_2_amount");
        doDecimalPostMediatorAction(inputDoc, resMgr, "tenor_3_amount");
        doDecimalPostMediatorAction(inputDoc, resMgr, "tenor_4_amount");
        doDecimalPostMediatorAction(inputDoc, resMgr, "tenor_5_amount");
        doDecimalPostMediatorAction(inputDoc, resMgr, "tenor_6_amount");


	if (!TransactionType.AMEND.equals(value))
	{
           doDecimalPostMediatorAction(inputDoc, resMgr, "fec_amount");  
           doDecimalPostMediatorAction(inputDoc, resMgr, "fec_rate");
	}
	 saveMyRecentInstrumtntInSession(reqInfo, formMgr, inputDoc, resMgr, beanMgr, request);
 }

 
}