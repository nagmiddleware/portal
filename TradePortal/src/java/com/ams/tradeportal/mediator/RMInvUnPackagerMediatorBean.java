/*
 * Created on Oct 10, 2007
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.ams.tradeportal.mediator;
import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.busobj.CorporateOrganization;
import com.ams.tradeportal.busobj.CreditNotes;
import com.ams.tradeportal.busobj.Invoice;
import com.ams.tradeportal.busobj.InvoiceGoods;
import com.ams.tradeportal.busobj.InvoicesSummaryData;
import com.ams.tradeportal.busobj.util.InvoiceUtility;
import com.ams.tradeportal.common.TPDateTimeUtility;
import com.ams.tradeportal.common.TradePortalConstants;
import com.ams.tradeportal.common.TransactionStatus;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.ComponentList;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.frame.MediatorBean;
import com.amsinc.ecsg.frame.MediatorServices;
import com.amsinc.ecsg.util.DateTimeUtility;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.StringFunction;


/**
 * Unpackager that handles Accounts Receivable and Payable message.
 * Accounts Payable is not in the scope as of now
 *
 * Copyright  � 2001
 * American Management Systems, Incorporated
 * All rights reserved
 */
public class RMInvUnPackagerMediatorBean  extends MediatorBean {
private static final Logger LOG = LoggerFactory.getLogger(RMInvUnPackagerMediatorBean.class);
	static private Hashtable<String, String> invAttributeMap;
    static private Hashtable<String, String> sellerPartyAttributeMap;
    static private Hashtable<String, String> buyerPartyAttributeMap;
    static private Hashtable<String, String> billPartyAttributeMap;
    static private Map<String, String> creditNoteAttributeMap;
    static private Map<String, String> paymentDetailsAttributeMap;

	public DocumentHandler execute(DocumentHandler unpackagerDoc, DocumentHandler outputDoc, MediatorServices mediatorServices)
																			throws RemoteException, AmsException
	 {

       unpackageInvoice(unpackagerDoc, mediatorServices);
      
        return outputDoc;

     }

    /**
     * Unpackage XML to create Invoice
     *
     * @param unpackagerDoc
     * @param outputDoc
     * @param mediatorServices
     * @return
     * @throws RemoteException
     * @throws AmsException
     */

    private void unpackageInvoice(DocumentHandler unpackagerDoc, MediatorServices mediatorServices)
    throws RemoteException, AmsException

    {

        Invoice invoice  ;

        /* ********************************************************
         *  Recursively populate the PayRemit and its component
         * ******************************************************/
        String currencyCode = unpackagerDoc.getAttribute("/Proponix/SubHeader/Curr");
        String customerID = unpackagerDoc.getAttribute("/Proponix/SubHeader/CustId");
        String instrmID = unpackagerDoc.getAttribute("/Proponix/SubHeader/InstrmID");
        String invoiceReceiptdatetime = DateTimeUtility.getGMTDateTime(false, false);  //IR - IVUJ110645459
        String corpOrg = null;
        if (customerID != null && !customerID.equals("")) {
          try {
        	
        	String sqlStatement = " SELECT organization_oid FROM corporate_org WHERE proponix_customer_id = ? AND activation_status = ? ";
        	DocumentHandler resultXML = DatabaseQueryBean.getXmlResultSet(sqlStatement, true, new Object[]{customerID, "ACTIVE"});
        	corpOrg = resultXML.getAttribute("/ResultSetRecord(0)/ORGANIZATION_OID");   
        	
          } catch (Exception e) {
            mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE ,TradePortalConstants.NO_MATCHING_VALUE_FOUND, "Corporate Organization " + corpOrg);
            e.printStackTrace();
            return;
          }
        }

        List<DocumentHandler> invoiceDocs = unpackagerDoc.getFragmentsList("/Proponix/Body/Invoice/");
        Map<String, String> corpOrdIdToSysUserIdMap = new HashMap<String, String>();       
        String systemUserId = corpOrdIdToSysUserIdMap.get(corpOrg);
        if (systemUserId == null && StringFunction.isNotBlank(corpOrg)) {
        	systemUserId = getSystemUserIdFromCorpOrg(corpOrg, mediatorServices);
        	corpOrdIdToSysUserIdMap.put(corpOrg, systemUserId);
        }
        System.out.print("Testing duplicate invoices scenario step1 "+invoiceDocs.size());
        Map<String, String> sellerCorpOrdIdMap = new HashMap<String, String>(); 
        for ( DocumentHandler invoiceDoc : invoiceDocs ) {
       	    //check if its an amendment if so update existing invoice
        	if(TradePortalConstants.INDICATOR_YES.equals(invoiceDoc.getAttribute("/AmdInd"))){
        		if ( TradePortalConstants.CREDIT_NOTE.equals(invoiceDoc.getAttribute("/CreditNoteInd")) && 
            			TradePortalConstants.INVOICE_TYPE_PAY_MGMT.equals(invoiceDoc.getAttribute("/LinkToInstrTyp")) ) {
            		processPayableCreditNotes(corpOrg, customerID, unpackagerDoc.getAttribute("/Proponix/SubHeader/ActvType"), invoiceDoc, mediatorServices);
            		continue; // continue for next invoice item
            	}
        		else {
        			updateInvoice(corpOrg,invoiceDoc, mediatorServices,systemUserId,sellerCorpOrdIdMap);
        			continue; 
        		}
        		
        	}
        	//New invoice processing
        	else{ 
        	// Nar CR-914A Rel9.2.0.0 01/29/2015 Begin
        	if ( TradePortalConstants.CREDIT_NOTE.equals(invoiceDoc.getAttribute("/CreditNoteInd")) && 
        			TradePortalConstants.INVOICE_TYPE_PAY_MGMT.equals(invoiceDoc.getAttribute("/LinkToInstrTyp")) ) {
        		processPayableCreditNotes(corpOrg, customerID, unpackagerDoc.getAttribute("/Proponix/SubHeader/ActvType"), invoiceDoc, mediatorServices);
        		continue; // continue for next invoice item
        	}
        	// Nar CR-914A Rel9.2.0.0 01/29/2015 End
        	String sellerCorpOrg = null;
            String sellerID = invoiceDoc.getAttribute("/SellerTPSCustomerID");
            String invoiceStatus = invoiceDoc.getAttribute("/SupplierPortalInvoiceStatus");
            invoice  =(Invoice) mediatorServices.createServerEJB("Invoice");
        	LOG.debug("Testing duplicate invoices scenario step2");
			//T36000037578- start--Check for duplicate ARIINV messages. if 'otl_invoice_uoid' is already in portal DB,message will be duplicate
			if ( TradePortalConstants.PAYABLE_INVOICE.equals(invoiceDoc.getAttribute("/CreditNoteInd"))) {
 
				if (TradePortalConstants.INDICATOR_YES.equals(invoiceDoc.getAttribute("/SupplierPortalInvoiceInd"))) {
               	 sellerCorpOrg= sellerCorpOrdIdMap.get(sellerID);
               	 if(StringFunction.isBlank(sellerCorpOrg)){
    	        		
               	     try {
                        	String sqlStatement = " SELECT organization_oid FROM corporate_org WHERE proponix_customer_id = ? AND activation_status = ? ";
               	        	DocumentHandler resultXML = DatabaseQueryBean.getXmlResultSet(sqlStatement, true, new Object[]{sellerID, "ACTIVE"});
               	        	sellerCorpOrg = resultXML.getAttribute("/ResultSetRecord(0)/ORGANIZATION_OID"); 
               	        	
               	        	sellerCorpOrdIdMap.put(sellerID, sellerCorpOrg);
                        }
                        catch (Exception e) {
                            mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE ,TradePortalConstants.NO_MATCHING_VALUE_FOUND, "Corporate Organization " + sellerID);
                            e.printStackTrace();
                            return;
                        } 
               	 }
               	corpOrg=sellerCorpOrg;
				}
				String invoiceUoid = invoiceDoc.getAttribute("/OTLInvcUoid");
				String sqlStatement = " SELECT count(*) the_count, max(invoice_oid) the_oid FROM invoice "
						+ " WHERE otl_invoice_uoid = ? AND  a_corp_org_oid = ? ";
	        	DocumentHandler resultXML = DatabaseQueryBean.getXmlResultSet(sqlStatement, true, new Object[]{invoiceUoid, corpOrg});
	        	int theCount = resultXML!=null?resultXML.getAttributeInt("/ResultSetRecord(0)/THE_COUNT"):0;
 
				if (theCount > 1) {
					mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE ,TradePortalConstants.DUPLICATE_INVOICE, "Invoice with otl_invoice_uoid " + invoiceUoid);
					continue;
				}
				//CR 913 - if original invoice is already present in portal and TPS send updated status then perform below process
                if (theCount != 0) {
                    String invoiceOid = resultXML.getAttribute("/ResultSetRecord(0)/THE_OID");
                    invoice.getData(Long.parseLong(invoiceOid));
                    if (TradePortalConstants.INDICATOR_YES.equals(invoiceDoc.getAttribute("/SupplierPortalInvoiceInd"))) {
                    invoice.setAttribute("supplier_portal_invoice_status", invoiceStatus);
                    String invoicePaymentDate =invoiceDoc.getAttribute("/InvoicePaymentDate");
                    invoice.setAttribute("invoice_payment_date",!StringFunction.isBlank(invoicePaymentDate)?invoicePaymentDate.substring(4,6)+ "/" + invoicePaymentDate.substring(6,8)+ "/" +invoicePaymentDate.substring(0,4):"");
                    
                    //IR 29798 start
                    invoice.setAttribute("invoice_payment_amount",invoiceDoc.getAttribute("/PaymentAmount"));
							if (TradePortalConstants.SP_STATUS_INELLIGIBLE.equals(invoiceStatus) || TradePortalConstants.SP_STATUS_DECLINTED_BY_BANK.equals(invoiceStatus)) {
								invoice.setAttribute("net_amount_offered", null);
							} else {
								invoice.calculateInvoiceOfferNetAmountOffered();
							}
                                            
                    // CR 914A Begin
                    // if adjustment amount sent by TPS, then save as it is. as it is already calculated in TPS.
                    if ( StringFunction.isNotBlank(invoiceDoc.getAttribute("/NetInvoicePaymentAmount"))) {
                    		invoice.setAttribute("inv_amount_after_adj", invoiceDoc.getAttribute("/NetInvoicePaymentAmount"));
                    		if ( StringFunction.isNotBlank(invoiceDoc.getAttribute("/TotalCreditNoteAppliedAmount"))) {
                    		   updateAppliedAmountforAllINVRecord(invoice.getAttribute("invoice_oid"), invoiceUoid, invoiceDoc.getAttribute("/NetInvoicePaymentAmount"), 
                    				   invoiceDoc.getAttribute("/TotalCreditNoteAppliedAmount") );
                    		}
                    }
                    if ( StringFunction.isNotBlank(invoiceDoc.getAttribute("/TotalCreditNoteAppliedAmount"))) {
                		invoice.setAttribute("total_credit_note_amount", invoiceDoc.getAttribute("/TotalCreditNoteAppliedAmount"));
                    }
                    // CR 914A End 
                    //IR 29798 end  
                    invoice.updateAssociatedInvoiceOfferGroup();
                    }
                    else{
                    	//what to update for buyer?? will there be any ARIInV to update buyer portal? 
                    	invoice.setAttribute("invoice_status", invoiceDoc.getAttribute("/InvoiceStat"));
                    }
                    invoice.save();
                    //log after invoice is saved
                    InvoiceUtility.createInvoiceHistory(invoiceOid, invoiceStatus, 
                        		"SP_UPLOAD",systemUserId,"", mediatorServices);   
                    continue;
                }
 			}
			//T36000037578- end--
       	   
            
        	//Invoke this method to create an invoice object with the Unique identifier
        	invoice.newObject();
        	LOG.debug("Testing duplicate invoices scenario step4 ="+ invoiceDoc.getAttribute("/InvoiceID"));
        	try {
        	invoice.populateFromXmlDoc(invoiceDoc, "/", new MyXMLTagTranslator(invAttributeMap));
        	//CR1006 start
        	DocumentHandler paymentDoc =invoiceDoc.getFragment("/PaymentDetails");
        	if(paymentDoc!=null){
        		invoice.populateFromXmlDoc(paymentDoc, "/", new MyXMLTagTranslator(paymentDetailsAttributeMap));
        	}
        	//CR1006 end
        	 invoice.setAttribute("end_to_end_id",invoiceDoc.getAttribute("/EndToEndID"));
        	currencyCode = invoiceDoc.getAttribute("/Currency");//currency of the invoice in Body tag
        	if (currencyCode!=null && currencyCode.length()>0) invoice.setAttribute("currency_code",currencyCode);
                // For supplier portal invoices, the instrument_id will store the seller finance instrument that will sent later by INVSTO message.
                if (!TradePortalConstants.INDICATOR_YES.equals(invoiceDoc.getAttribute("/SupplierPortalInvoiceInd"))) {
                    invoice.setAttribute("instrument_id",instrmID);
                    
                  }
              //CR913 - set the linkedInstType sent from TPS. It can be either R/P
                invoice.setAttribute("link_to_instrument_type",invoiceDoc.getAttribute("/LinkToInstrTyp"));
                invoice.setAttribute("otl_customer_id",customerID);
                if (corpOrg!=null && corpOrg.length()>0) invoice.setAttribute("corp_org_oid",corpOrg);
                invoice.setAttribute("activity_sequence_datetime", invoiceReceiptdatetime); //IR - IVUJ110645459
                //Ravindra - Rel800 CR710 - 20th Dec 2011 - Start
                String invoicePaymentDate =invoiceDoc.getAttribute("/InvoicePaymentDate");
                invoice.setAttribute("invoice_payment_date",!StringFunction.isBlank(invoicePaymentDate)?invoicePaymentDate.substring(4,6)+ "/" + invoicePaymentDate.substring(6,8)+ "/" +invoicePaymentDate.substring(0,4):"");
                //IValavala Rel800 CR710. If PTL set to Y, else set to N.
                String portalOrigInd =invoiceDoc.getAttribute("/PortalOriginatedInd");
                if ("PTL".equals(portalOrigInd)){
         		  invoice.setAttribute("portal_originated_ind", TradePortalConstants.INDICATOR_YES);
                }
                else{
                      invoice.setAttribute("portal_originated_ind",TradePortalConstants.INDICATOR_NO);
                }
                //Ravindra - Rel800 CR710 - 20th Dec 2011 - End
               
                //IR 28009 start
                invoice.setAttribute("invoice_payment_amount",invoiceDoc.getAttribute("/PaymentAmount"));
                //IR 28009 end
                
                //performs calculations and invoice offer logic
                if (TradePortalConstants.INDICATOR_YES.equals(invoiceDoc.getAttribute("/SupplierPortalInvoiceInd"))) {
                	
                        
                	 invoice.setAttribute("supplier_portal_invoice_status", invoiceStatus);

                    invoice.setAttribute("otl_customer_id",sellerID);
                    invoice.setAttribute("corp_org_oid",sellerCorpOrg);                    
                
						if (TradePortalConstants.SP_STATUS_INELLIGIBLE.equals(invoiceStatus) || TradePortalConstants.SP_STATUS_DECLINTED_BY_BANK.equals(invoiceStatus)) {
							invoice.setAttribute("net_amount_offered", null);
						} else {
							invoice.calculateInvoiceOfferNetAmountOffered();
						}
                    // CR 914A Begin
                    // if adjustment amount sent by TPS, then save as it is. as it is already calculated in TPS.
                    if ( StringFunction.isNotBlank(invoiceDoc.getAttribute("/NetInvoicePaymentAmount"))) {
                        invoice.setAttribute("inv_amount_after_adj", invoiceDoc.getAttribute("/NetInvoicePaymentAmount"));
                    	if ( StringFunction.isNotBlank(invoiceDoc.getAttribute("/TotalCreditNoteAppliedAmount"))) {
                 		   updateAppliedAmountforAllINVRecord(invoice.getAttribute("invoice_oid"), invoiceDoc.getAttribute("/OTLInvcUoid"), 
                 				   invoiceDoc.getAttribute("/NetInvoicePaymentAmount"), invoiceDoc.getAttribute("/TotalCreditNoteAppliedAmount") );
                 		}
                    }
                    // CR 914A End 
		            invoice.updateAssociatedInvoiceOfferGroup();
		            String action ="SP_UPLOAD";
		            //As per AnnC's email
		            if(TradePortalConstants.SP_STATUS_INVOICE_PAID.equals(invoiceStatus)){
		            	action = TradePortalConstants.SP_STATUS_INVOICE_PAID;
		            }
		            InvoiceUtility.createInvoiceHistory(invoice.getAttribute("invoice_oid"), invoice.getAttribute("supplier_portal_invoice_status"), 
                		action,systemUserId,"", mediatorServices);    	
                }
                //CR 913 start - Unpackage Payable Invoice
                
                	unpackagePayInvoice(mediatorServices,invoice,invoiceDoc);
                //}
               //CR 913 end
                
            
        }
        catch(Exception e){
            mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE ,TradePortalConstants.TASK_NOT_SUCCESSFUL, "RMInv Invoice Unpackaging");
            e.printStackTrace();
            return;
        }


        /* ********************************************************
         * Populate the party attributes.  They are hierarchical in XML and OTL but flattened in TP object model.
         * ******************************************************/
        List<DocumentHandler> partyDocs = invoiceDoc.getFragmentsList("/Party/");
        for ( DocumentHandler partyDoc : partyDocs ) {

            String partyType = partyDoc.getAttribute("/PtyType");
            if (partyType.equals("SAR")) {
            	invoice.populateFromXmlDoc(partyDoc, "/", new MyXMLTagTranslator(sellerPartyAttributeMap));
            }
            else if (partyType.equals("BAR")) {
            	invoice.populateFromXmlDoc(partyDoc, "/", new MyXMLTagTranslator(buyerPartyAttributeMap));
            }
            else if (partyType.equals("BIL")) {
            	invoice.populateFromXmlDoc(partyDoc, "/", new MyXMLTagTranslator(billPartyAttributeMap));
            }
        }
        String invOid =invoice.getAttribute("invoice_oid");
        String invStatus =invoice.getAttribute("invoice_status");
        int returnValue = invoice.save();

        if (returnValue < 0) {
            mediatorServices.getErrorManager().addErrors(invoice.getErrorManager().getIssuedErrorsList());
        }
        //CR913 start - create log for both Payables and Receivables. For suplier portal inv, log  is create above
        if (!TradePortalConstants.INDICATOR_YES.equals(invoice.getAttribute("supplier_portal_invoice_ind")))
        	InvoiceUtility.createInvoiceHistory(invOid, invStatus, 
            		 TradePortalConstants.PAYABLES_INVOICE_ACTION_RECEIVED,systemUserId,"", mediatorServices);    	 
        //CR913 end
        }
       }

        return;
	 }

	/**
     * Unpackage XML to Update Invoice
     *
     * @param unpackagerDoc
     * @param outputDoc
     * @param mediatorServices
     * @return
     * @throws RemoteException
     * @throws AmsException
     */

    private void updateInvoice(String corpOrg,DocumentHandler invoiceDoc ,MediatorServices mediatorServices,String systemUserId,Map<String,String> sellerCorpOrdIdMap)
    throws RemoteException, AmsException

    {
	    //String invoiceGoodOid = null;
	    String invoiceReceiptdatetime = DateTimeUtility.getGMTDateTime(false, false);   //IR - IVUJ110645459
	    InvoiceGoods invoiceGoods = null;
        Invoice invoice = null;
        String invoiceOid = "";
        String sellerCorpOrg ="";
        String sellerID = invoiceDoc.getAttribute("/SellerTPSCustomerID");
        String invoiceStatus = invoiceDoc.getAttribute("/SupplierPortalInvoiceStatus");
        if (TradePortalConstants.INDICATOR_YES.equals(invoiceDoc.getAttribute("/SupplierPortalInvoiceInd"))) {
        	 sellerCorpOrg= sellerCorpOrdIdMap.get(sellerID);
        if(StringFunction.isBlank(sellerCorpOrg)){
        try {
         	String sqlStatement = " SELECT organization_oid FROM corporate_org WHERE proponix_customer_id = ? AND activation_status = ? ";
	        	DocumentHandler resultXML = DatabaseQueryBean.getXmlResultSet(sqlStatement, true, new Object[]{sellerID, "ACTIVE"});
	        	sellerCorpOrg = resultXML.getAttribute("/ResultSetRecord(0)/ORGANIZATION_OID");  
	        	sellerCorpOrdIdMap.put(sellerID, sellerCorpOrg);
         }
         catch (Exception e) {
             mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE ,TradePortalConstants.NO_MATCHING_VALUE_FOUND, "Corporate Organization " + sellerID);
             e.printStackTrace();
             return;
         } 
        	 }
        }
        else{
        	sellerCorpOrg =corpOrg;
        }
        int theCount =0;
     	DocumentHandler resultXML =null;
         // For ineligible invoice, the invoice should already exist.
         // For new invoice, the invoice should not exist.
         // There should not be more than one invoice.
         String invoiceUoid = invoiceDoc.getAttribute("/OTLInvcUoid");
        String sqlStatement = " SELECT count(*) the_count, max(invoice_oid) the_oid FROM invoice "
         		+ " WHERE otl_invoice_uoid = ? AND a_corp_org_oid = ? and link_to_instrument_type = '"+invoiceDoc.getAttribute("/LinkToInstrTyp")+"'";
        try{
     	 resultXML = DatabaseQueryBean.getXmlResultSet(sqlStatement, true, new Object[]{invoiceUoid, sellerCorpOrg});
     	 theCount = resultXML.getAttributeInt("/ResultSetRecord(0)/THE_COUNT");
        }
        catch (Exception e) {
            mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE ,TradePortalConstants.NO_MATCHING_VALUE_FOUND, "Invoice with otl_invoice_uoid " + invoiceUoid);
            e.printStackTrace();
            return;
        }   
     	
       if (theCount > 1) {
             mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE ,TradePortalConstants.DUPLICATE_INVOICE, "Invoice with otl_invoice_uoid " + invoiceUoid);
             return;
         }
         //CR 913 - if original invoice is already present in portal and TPS send updated status then perform below process
         if (theCount != 0) {
              invoiceOid = resultXML.getAttribute("/ResultSetRecord(0)/THE_OID");
         }else{
        	 mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE ,TradePortalConstants.NO_MATCHING_VALUE_FOUND, "Invoice with otl_invoice_uoid  " + invoiceUoid);
        	 return;
         }
        
       // long userOid = 0; // dummy user_oid for agent
       // long invoiceOid = invoice.getAttributeLong("invoice_oid");

        //Lock the Invoice object to avoid conflict with the on-line processing.

        try {
            // By the time we are able to lock the object, the data on the object we
            // instantiated before the locking might have changed.  Need to re-instantiate it.
            invoice  = (Invoice) mediatorServices.createServerEJB("Invoice");
            invoice.getData(Long.parseLong(invoiceOid));
            String invoiceDue = invoiceDoc.getAttribute("/InvcDueDt");
		    String invoiceIssue = invoiceDoc.getAttribute("/InvcIssueDt");
            invoice.setAttribute("invoice_due_date",!StringFunction.isBlank(invoiceDue)?invoiceDue.substring(4,6)+ "/" + invoiceDue.substring(6,8)+ "/" +invoiceDue.substring(0,4):"");
	        invoice.setAttribute("invoice_issue_datetime",!StringFunction.isBlank(invoiceIssue)?TPDateTimeUtility.buildJPylonDateTime(invoiceIssue.substring(6,8), invoiceIssue.substring(4,6), invoiceIssue.substring(0,4)):"");
            invoice.setAttribute("finance_percentage",invoiceDoc.getAttribute("/FinPct"));
            invoice.setAttribute("invoice_total_amount",invoiceDoc.getAttribute("/InvcTotalAmt"));
            invoice.setAttribute("finance_status",invoiceDoc.getAttribute("/FinStatus"));
	        invoice.setAttribute("invoice_outstanding_amount",invoiceDoc.getAttribute("/OutstandingAmt"));//IR - MTUJ022476356
		    invoice.setAttribute("activity_sequence_datetime", invoiceReceiptdatetime); //IR - IVUJ110645459
            //Ravindra - Rel800 CR710 - 20th Dec 2011 - Start
            String invoicePaymentDate = invoiceDoc.getAttribute("/InvoicePaymentDate");
            invoice.setAttribute("invoice_payment_date",!StringFunction.isBlank(invoicePaymentDate)?invoicePaymentDate.substring(4,6)+ "/" + invoicePaymentDate.substring(6,8)+ "/" +invoicePaymentDate.substring(0,4):"");
            String portalOrigInd =invoiceDoc.getAttribute("/PortalOriginatedInd");
            if ("PTL".equals(portalOrigInd)){
     		  invoice.setAttribute("portal_originated_ind", TradePortalConstants.INDICATOR_YES);
            }
            else{
                  invoice.setAttribute("portal_originated_ind",TradePortalConstants.INDICATOR_NO);
            }
            //Ravindra - Rel800 CR710 - 20th Dec 2011 - End
            invoice.setAttribute("end_to_end_id",invoiceDoc.getAttribute("/EndToEndID"));
            //sets remaining fields and performs calculations
            //performs calculations and invoice offer logic
            if (TradePortalConstants.INDICATOR_YES.equals(invoice.getAttribute("supplier_portal_invoice_ind"))) {
            	
            	     //CR 913 - if original invoice is already present in portal and TPS send updated status then perform below process
                     if (theCount != 0) {
                         invoice.setAttribute("supplier_portal_invoice_status", invoiceStatus);
                         
                         //IR 29798 start
                         invoice.setAttribute("invoice_payment_amount",invoiceDoc.getAttribute("/PaymentAmount"));
					if (TradePortalConstants.SP_STATUS_INELLIGIBLE.equals(invoiceStatus) || TradePortalConstants.SP_STATUS_DECLINTED_BY_BANK.equals(invoiceStatus)) {
						invoice.setAttribute("net_amount_offered", null);
					} else {
						invoice.calculateInvoiceOfferNetAmountOffered();
					}
                                                 
                         // CR 914A Begin
                         // if adjustment amount sent by TPS, then save as it is. as it is already calculated in TPS.
                         if ( StringFunction.isNotBlank(invoiceDoc.getAttribute("/NetInvoicePaymentAmount"))) {
                         		invoice.setAttribute("inv_amount_after_adj", invoiceDoc.getAttribute("/NetInvoicePaymentAmount"));
                         		if ( StringFunction.isNotBlank(invoiceDoc.getAttribute("/TotalCreditNoteAppliedAmount"))) {
                         		   updateAppliedAmountforAllINVRecord(invoice.getAttribute("invoice_oid"), invoiceUoid, invoiceDoc.getAttribute("/NetInvoicePaymentAmount"), 
                         				   invoiceDoc.getAttribute("/TotalCreditNoteAppliedAmount") );
                         		}
                         }
                         if ( StringFunction.isNotBlank(invoiceDoc.getAttribute("/TotalCreditNoteAppliedAmount"))) {
                     		invoice.setAttribute("total_credit_note_amount", invoiceDoc.getAttribute("/TotalCreditNoteAppliedAmount"));
                         }
                         // CR 914A End 
                         //IR 29798 end  
                         invoice.updateAssociatedInvoiceOfferGroup();
                         invoice.save();
                         //log after invoice is saved
                         InvoiceUtility.createInvoiceHistory(invoiceOid, invoiceStatus, 
                             		"SP_UPLOAD",systemUserId,"", mediatorServices);   
                         return;
                     }
                 
              }
            
            unpackagePayInvoice(mediatorServices,invoice,invoiceDoc);
        
        }
        catch (AmsException e){
            mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE ,TradePortalConstants.TASK_NOT_SUCCESSFUL, "Receivable Invoice Unpackaging");
            e.printStackTrace();
        }

	//IR - MTUJ022476356 - BEGIN...
        List<DocumentHandler> partyDocs = invoiceDoc.getFragmentsList("/Party/");
        for ( DocumentHandler partyDoc : partyDocs ) {
            String partyType = partyDoc.getAttribute("/PtyType");
            if (partyType.equals("SAR")) {
            	invoice.populateFromXmlDoc(partyDoc, "/", new MyXMLTagTranslator(sellerPartyAttributeMap));
            }
            else if (partyType.equals("BAR")) {
            	invoice.populateFromXmlDoc(partyDoc, "/", new MyXMLTagTranslator(buyerPartyAttributeMap));
            }
            else if (partyType.equals("BIL")) {
            	invoice.populateFromXmlDoc(partyDoc, "/", new MyXMLTagTranslator(billPartyAttributeMap));
            }
        }

        List<DocumentHandler> goodsDocs = invoiceDoc.getFragmentsList("/Goods/");
	    if (goodsDocs.size() > 0 ) {
				ComponentList invoiceGoodsList = (ComponentList)invoice.getComponentHandle("InvoiceGoodsList");
				if (invoiceGoodsList.getObjectCount() > 0) {
					invoice.deleteComponent("InvoiceGoodsList");
				}
				for ( DocumentHandler goodDoc :  goodsDocs ) {

					invoiceGoods  =(InvoiceGoods) mediatorServices.createServerEJB("InvoiceGoods");
					invoiceGoods.newObject();

					try {
						invoiceGoods.populateFromXmlDoc(goodDoc, "/", new MyXMLTagTranslator(invAttributeMap));
						invoiceGoods.setAttribute("invoice_oid", invoiceOid);
						invoiceGoods.save();
					}
					catch(Exception e){
						mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE ,TradePortalConstants.TASK_NOT_SUCCESSFUL, "Receivable InvoiceGoods Unpackaging");
						e.printStackTrace();
						return;
					}

				}

			}
	

	int returnValue = invoice.save();
        if (returnValue < 0) {
            mediatorServices.getErrorManager().addErrors(invoice.getErrorManager().getIssuedErrorsList());
        }
	//IR - MTUJ022476356 - END...

        //}
        return ;


	}

	// CR 913 start - Unpackage the Payables Invoice Details and store accordingly
	private void unpackagePayInvoice(MediatorServices mediatorServices,
			Invoice invoice, DocumentHandler invoiceDoc)
			throws RemoteException, AmsException {
		if(TradePortalConstants.INVOICE_TYPE_PAY_MGMT.equalsIgnoreCase(invoiceDoc.getAttribute("/LinkToInstrTyp"))){
		String sendToSupplierDate = invoiceDoc.getAttribute("/SendToSupplierDate");
		invoice.setAttribute("send_to_supplier_date", StringFunction.isNotBlank(sendToSupplierDate) ? sendToSupplierDate.substring(4, 6)
						+ "/" + sendToSupplierDate.substring(6, 8) + "/" + sendToSupplierDate.substring(0, 4) : "");
		invoice.setAttribute("invoice_status", TradePortalConstants.INVOICE_STATUS_ASSIGNED);
		invoice.setAttribute("send_to_supplier_date_tps", invoice.getAttribute("send_to_supplier_date"));
		invoice.setAttribute("invoice_payment_amount_tps", invoice.getAttribute("invoice_payment_amount"));
		invoice.setAttribute("invoice_payment_date_tps", invoice.getAttribute("invoice_payment_date"));
		}

                // W Zhu 7/9/2015 Rel 9.3 CR-1006 T36000040829 couple of changes begin
                // 1. Do not check PortalOrigniatedInd.  If the invoices originally came from H2H and then sent to 
                //    Portal for approval (using PINCNAPP message CR-1006) and then eventually when ARIINV comes back to Portal, 
                //    PortalOriginatedInd == DLD in this case.
                // 2. Do not give error when there is no matching invoices_summary_data (count == 0).  
                //    Since we do not check PortalOriginalInd now, count==0 is possible.
                //    Also , if the invoice was uploaded by buyer and the invoice is 
                //     sent to the supplier portal for approval (CR-708), there is no mactch.
		//if ("PTL".equals(invoiceDoc.getAttribute("/PortalOriginatedInd"))) {
			// get InvoicesSummaryDataBean using invoice_id and update the status to PROCESSED_BY_BANK
			String invoiceID = invoiceDoc.getAttribute("/InvoiceID");
			String uploadInvSql = " SELECT count(*) count, max(upload_invoice_oid) oid FROM invoices_summary_data "
					+ " WHERE invoice_id = ? AND a_corp_org_oid = ? AND invoice_classification = ?";
			DocumentHandler resultDocXML = DatabaseQueryBean.getXmlResultSet(uploadInvSql, true,
					new Object[] { invoiceID, invoice.getAttribute("corp_org_oid"), invoiceDoc.getAttribute("/LinkToInstrTyp")});
			int count = resultDocXML!=null?resultDocXML.getAttributeInt("/ResultSetRecord(0)/COUNT"):0;
			if ( count > 1) {
				mediatorServices.getErrorManager().issueError( TradePortalConstants.ERR_CAT_MIDDLEWARE, TradePortalConstants.MORE_THAN_ONE_VALUE_FOUND,
						"Invoice in Invoices Summary Data with Invoice ID:= "+ invoiceID);
			//} else if ( count == 0) {
			//	mediatorServices.getErrorManager().issueError( TradePortalConstants.ERR_CAT_MIDDLEWARE, TradePortalConstants.NO_MATCHING_VALUE_FOUND,
			//			"Invoice in Invoices Summary Data with Invoice ID:= "+ invoiceID);
			} else if (count == 1){
				String oid = resultDocXML.getAttribute("/ResultSetRecord(0)/OID");
				InvoicesSummaryData invoicesSummaryData = (InvoicesSummaryData) mediatorServices.createServerEJB("InvoicesSummaryData",
								Long.parseLong(oid));
				invoicesSummaryData.setAttribute("invoice_status", TransactionStatus.PROCESSED_BY_BANK);
				invoicesSummaryData.setAttribute("tp_rule_name", invoicesSummaryData.getTpRuleName());
				if (invoicesSummaryData.save() < 0) {
					mediatorServices.getErrorManager().addErrors(invoicesSummaryData.getErrorManager().getIssuedErrorsList());
				}
			}
		//}
                // W Zhu 7/9/2015 Rel 9.3 CR-1006 T36000040829 couple of changes end
	}
    //CR 913 end    

    /**
     *
     * Invoice Translator: The "call-back" class used to translate the tag name and values in
     * RM Invoice XML to object attribute name and values.  This is used for populate the object
     * with the XML file.
     *
     */

   public class MyXMLTagTranslator extends com.amsinc.ecsg.util.XMLTagTranslator {

        private Map<String, String> nameMap;

        public MyXMLTagTranslator (Map<String, String> nameMap) {
            this.nameMap = nameMap;
        }

        /**
         * translate tagName according to nameMap.
         * translate attribute values as necessary.
         */
        public void translateTag(String tagName, String tagValue) {

            attributeName = (String)nameMap.get(tagName);

            if (tagName.equals("InvcDueDt") ||  tagName.equals("ActualShipmentDate")){
            	attributeValue =translateDateTime(tagName,tagValue,false);
           }
            else if (tagName.equals("InvcIssueDt")  || tagName.equals("POIssueDt")) {
                attributeValue = translateDateTime(tagName,tagValue,true);
            }
            else if (tagName.equals("OfferExpiryDate")) {
                attributeValue = translateDateTime2(tagName,tagValue,false);
            }
            else {
                attributeValue = tagValue;
            }

        }

        // 20010102 -> 01/02/2001
        private String translateDateTime(String tagName,String tagValue,boolean addTime){
        	if(!StringFunction.isBlank(tagValue)){
        		String date = tagValue.substring(4,6) + "/"
                    		+ tagValue.substring(6,8) + "/"
                    		+ tagValue.substring(0,4);
        		return addTime?date + " 00:00:00" : date;

        	}
        	return "";
        }

        // 2001-01-02 -> 01/02/2001
        private String translateDateTime2(String tagName,String tagValue,boolean addTime){
        	if(!StringFunction.isBlank(tagValue)){
        		String date = tagValue.substring(5,7) + "/"
                    		+ tagValue.substring(8,10) + "/"
                    		+ tagValue.substring(0,4);
        		return addTime?date + " 00:00:00" : date;

        	}
        	return "";
        }

   }

    /*
     ****************************************************
     static initialization of the attributemap:
     **************************************************
     */
    {
        invAttributeMap = new Hashtable<String, String>();
        //Invoice translates
        invAttributeMap.put("InvoiceID", "invoice_reference_id");
        invAttributeMap.put("OTLInvcUoid", "otl_invoice_uoid");
        invAttributeMap.put("InvcIssueDt", "invoice_issue_datetime");
        invAttributeMap.put("InvcDueDt", "invoice_due_date");
        invAttributeMap.put("Status", "invoice_status");
        invAttributeMap.put("InvcPayStat", "invoice_payment_status");
        invAttributeMap.put("FinStatus", "finance_status");
        invAttributeMap.put("FinPct", "finance_percentage");
        invAttributeMap.put("InvcTotalAmt", "invoice_total_amount");
	    invAttributeMap.put("OutstandingAmt", "invoice_outstanding_amount");
	    invAttributeMap.put("NetInvoicePaymentAmount", "inv_amount_after_adj");
	    invAttributeMap.put("TotalCreditNoteAppliedAmount", "total_credit_note_amount");
	    invAttributeMap.put("SupplierPortalInvoiceInd" , "supplier_portal_invoice_ind");
	    invAttributeMap.put("OfferExpiryDate", "offer_expiry_date");
	    
        invAttributeMap.put("SupplierPortalInvoiceStatus", "supplier_portal_invoice_status");
        invAttributeMap.put("Currency", "currency_code");
        invAttributeMap.put("end_to_end_id", "EndToEndID");//CR 999
         
        // Populating the Goods for the Invoice and this can be repetive
        invAttributeMap.put("Goods", "InvoiceGoodsList");
        invAttributeMap.put("GoodsDescription", "goods_description");
        invAttributeMap.put("PORefId", "po_reference_id");
        invAttributeMap.put("POIssueDt", "po_issue_datetime");
        invAttributeMap.put("LnItmsTtlAmt", "line_items_total_amount");
        invAttributeMap.put("TtlNetAmt", "total_net_amount");
        invAttributeMap.put("SlrLabel1", "seller_information_label1");
        invAttributeMap.put("SlrLabel2", "seller_information_label2");
        invAttributeMap.put("SlrLabel3", "seller_information_label3");
        invAttributeMap.put("SlrLabel4", "seller_information_label4");
        invAttributeMap.put("SlrLabel5", "seller_information_label5");
        invAttributeMap.put("SlrLabel6", "seller_information_label6");
        invAttributeMap.put("SlrLabel7", "seller_information_label7");
        invAttributeMap.put("SlrLabel8", "seller_information_label8");
        invAttributeMap.put("SlrLabel9", "seller_information_label9");
        invAttributeMap.put("SlrLabel10", "seller_information_label10");
        invAttributeMap.put("SlrInfo1", "seller_information1");
        invAttributeMap.put("SlrInfo2", "seller_information2");
        invAttributeMap.put("SlrInfo3", "seller_information3");
        invAttributeMap.put("SlrInfo4", "seller_information4");
        invAttributeMap.put("SlrInfo5", "seller_information5");
        invAttributeMap.put("SlrInfo6", "seller_information6");
        invAttributeMap.put("SlrInfo7", "seller_information7");
        invAttributeMap.put("SlrInfo8", "seller_information8");
        invAttributeMap.put("SlrInfo9", "seller_information9");
        invAttributeMap.put("SlrInfo10", "seller_information10");
        
        
        //CR1006 start
        invAttributeMap.put("IncoTerm", "incoterm");
        invAttributeMap.put("PortOfLoading", "country_of_loading");
        invAttributeMap.put("PortOfDischarge", "country_of_discharge");
        invAttributeMap.put("Vessel", "vessel");
        invAttributeMap.put("Carrier", "carrier");
        invAttributeMap.put("ActualShipmentDate", "actual_ship_date");

        //Payment Details
        paymentDetailsAttributeMap = new HashMap<String, String>();
        paymentDetailsAttributeMap.put("PaymentMethod", "pay_method");
        paymentDetailsAttributeMap.put("BeneficiaryAccountNum", "ben_acct_num");
        paymentDetailsAttributeMap.put("BeneficiaryAddressLn1", "ben_add1");
        paymentDetailsAttributeMap.put("BeneficiaryAddressLn2", "ben_add2");
        paymentDetailsAttributeMap.put("BeneficiaryAddressLn3", "ben_add3");
        paymentDetailsAttributeMap.put("BeneficiaryCountry", "ben_country");
        paymentDetailsAttributeMap.put("BeneficiaryEmailAddr", "ben_email_addr");
        paymentDetailsAttributeMap.put("BeneficiaryBankName", "ben_bank_name");
        
        paymentDetailsAttributeMap.put("BeneficiaryBankBranchCode", "ben_branch_code");
        paymentDetailsAttributeMap.put("BeneficiaryBankSortCode", "ben_bank_sort_code");
        paymentDetailsAttributeMap.put("BeneficiaryBankAddressLn1", "ben_branch_add1");
        paymentDetailsAttributeMap.put("BeneficiaryBankAddressLn2", "ben_branch_add2");
        paymentDetailsAttributeMap.put("BeneficiaryBankCity", "ben_bank_city");
        paymentDetailsAttributeMap.put("BeneficiaryBankProvince", "ben_bank_province");
        paymentDetailsAttributeMap.put("BeneficiaryBankCountry", "ben_branch_country");
        paymentDetailsAttributeMap.put("BankChrgs", "charges");
        paymentDetailsAttributeMap.put("CentralBankReporting1", "central_bank_rep1");
        paymentDetailsAttributeMap.put("CentralBankReporting2", "central_bank_rep2");
        paymentDetailsAttributeMap.put("CentralBankReporting3", "central_bank_rep3");
        //CR 1006 end

        // Flatten the Party node to Invoice itself
        buyerPartyAttributeMap = new Hashtable<String, String>();
        buyerPartyAttributeMap.put("PtyIdent", "buyer_party_identifier");
        buyerPartyAttributeMap.put("Addr1", "buyer_address_line_1");
        buyerPartyAttributeMap.put("Addr2", "buyer_address_line_2");

        sellerPartyAttributeMap = new Hashtable<String, String>();
        sellerPartyAttributeMap.put("PtyIdent", "seller_party_identifier");
        sellerPartyAttributeMap.put("Addr1", "seller_address_line_1");
        sellerPartyAttributeMap.put("Addr2", "seller_address_line_2");

        billPartyAttributeMap = new Hashtable<String, String>();
        billPartyAttributeMap.put("PtyIdent", "bill_party_identifier");
        billPartyAttributeMap.put("Addr1", "bill_address_line_1");
        billPartyAttributeMap.put("Addr2", "bill_address_line_2");
        
        creditNoteAttributeMap = new HashMap<String, String>();
        creditNoteAttributeMap.put("InvoiceID", "invoice_id");
        creditNoteAttributeMap.put("OTLInvcUoid", "otl_invoice_uoid");
        creditNoteAttributeMap.put("InvcDueDt", "expiry_date");
        creditNoteAttributeMap.put("Status", "credit_note_status");
        creditNoteAttributeMap.put("InvcTotalAmt", "amount");
        creditNoteAttributeMap.put("Currency", "currency");
	    creditNoteAttributeMap.put("TotalCreditNoteAppliedAmount", "credit_note_applied_amount");
	    creditNoteAttributeMap.put("CreditNoteAppliedStatus", "credit_note_applied_status");
	    creditNoteAttributeMap.put("end_to_end_id", "EndToEndID");//CR 999
       
	   // Populating the Goods for the Credit Notes and this can be repetive
	    creditNoteAttributeMap.put("Goods", "CreditNotesGoodsList");
	    creditNoteAttributeMap.put("SlrLabel1", "seller_users_def1_label");
        creditNoteAttributeMap.put("SlrLabel2", "seller_users_def2_label");
        creditNoteAttributeMap.put("SlrLabel3", "seller_users_def3_label");
        creditNoteAttributeMap.put("SlrLabel4", "seller_users_def4_label");
        creditNoteAttributeMap.put("SlrLabel5", "seller_users_def5_label");
        creditNoteAttributeMap.put("SlrLabel6", "seller_users_def6_label");
        creditNoteAttributeMap.put("SlrLabel7", "seller_users_def7_label");
        creditNoteAttributeMap.put("SlrLabel8", "seller_users_def8_label");
        creditNoteAttributeMap.put("SlrLabel9", "seller_users_def9_label");
        creditNoteAttributeMap.put("SlrLabel10", "seller_users_def10_label");
        creditNoteAttributeMap.put("SlrInfo1", "seller_users_def1_value");
        creditNoteAttributeMap.put("SlrInfo2", "seller_users_def2_value");
        creditNoteAttributeMap.put("SlrInfo3", "seller_users_def3_value");
        creditNoteAttributeMap.put("SlrInfo4", "seller_users_def4_value");
        creditNoteAttributeMap.put("SlrInfo5", "seller_users_def5_value");
        creditNoteAttributeMap.put("SlrInfo6", "seller_users_def6_value");
        creditNoteAttributeMap.put("SlrInfo7", "seller_users_def7_value");
        creditNoteAttributeMap.put("SlrInfo8", "seller_users_def8_value");
        creditNoteAttributeMap.put("SlrInfo9", "seller_users_def9_value");
        creditNoteAttributeMap.put("SlrInfo10", "seller_users_def10_value");
        creditNoteAttributeMap.put("GoodsDescription", "goods_description");

    }

	private String getSystemUserIdFromCorpOrg(String corpOrgOid,
			MediatorServices mediatorServices) throws RemoteException,
			AmsException {
		CorporateOrganization corpOrg = (CorporateOrganization) mediatorServices
				.createServerEJB("CorporateOrganization",
						Long.parseLong(corpOrgOid));
		String systemUserOid = corpOrg.getAttribute("upload_system_user_oid");
		if (systemUserOid == null) {
			systemUserOid = "";
		}
		return systemUserOid;
	}
	
	// Nar CR-914A Rel9.2.0.0 02/01/2015 Begin
	/**
	 * This method is used to process the payable credit notes.
	 * @param corpOrg
	 * @param customerID
	 * @param activityType
	 * @param creditNoteDoc
	 * @param mediatorServices
	 * @throws RemoteException
	 * @throws AmsException
	 */
    private void processPayableCreditNotes(String corpOrg, String customerID, String activityType, DocumentHandler creditNoteDoc, MediatorServices mediatorServices) 
    		throws RemoteException, AmsException {
    	int count = 0;
    	String sqlStatement = null;
    	DocumentHandler resultXML = null;
        if ( StringFunction.isNotBlank(corpOrg) ) {
 	       sqlStatement = " SELECT count(*) credit_count, max(upload_credit_note_oid) oid FROM credit_notes WHERE invoice_id = ? "
   	   		+ " AND a_corp_org_oid = ?  AND linked_to_instrument_type = ? ";
          resultXML = DatabaseQueryBean.getXmlResultSet(sqlStatement, true, new Object[]{ creditNoteDoc.getAttribute("/InvoiceID"), corpOrg, "PYB" });
          count = resultXML.getAttributeInt("/ResultSetRecord(0)/CREDIT_COUNT"); 
       }
       if ( count == 0 ) {
    	    sqlStatement = " SELECT count(*) credit_count, max(upload_credit_note_oid) oid FROM credit_notes WHERE invoice_id = ? "
		   	   		+ " AND otl_invoice_uoid = ?  AND linked_to_instrument_type = ? ";
		    resultXML = DatabaseQueryBean.getXmlResultSet(sqlStatement, true, new Object[]{ creditNoteDoc.getAttribute("/InvoiceID"),  
		    		creditNoteDoc.getAttribute("/OTLInvcUoid"), "PYB" });
		     count = resultXML.getAttributeInt("/ResultSetRecord(0)/CREDIT_COUNT");
			 
       }
       
       if ( count == 0 ) {
    	   unPackagePayableCreditNote(corpOrg, customerID, creditNoteDoc, mediatorServices);
       } else if ( count == 1 ) {
    	   String creditNoteOid = resultXML.getAttribute("/ResultSetRecord(0)/OID");
    	   updatePayableCreditNote(creditNoteOid, activityType, creditNoteDoc, mediatorServices);
       } else {
    	   mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE, TradePortalConstants.DUPLICATE_CREDIT_NOTE,
					creditNoteDoc.getAttribute("/InvoiceID"));
       }
                  	   		
	}
    
    /**
     * This method is used to update the payable credit note.
     * @param creditNoteOid
     * @param activityType
     * @param creditNoteDoc
     * @param mediatorServices
     * @throws RemoteException
     * @throws AmsException
     */
    private void updatePayableCreditNote(String creditNoteOid, String activityType, DocumentHandler creditNoteDoc, MediatorServices mediatorServices) 
    		throws RemoteException, AmsException {
    	CreditNotes creditNote = (CreditNotes) mediatorServices.createServerEJB("CreditNotes", Long.parseLong(creditNoteOid));
    	creditNote.setAttribute("otl_invoice_uoid", creditNoteDoc.getAttribute("/OTLInvcUoid"));
    	if ( StringFunction.isNotBlank(creditNoteDoc.getAttribute("/Status")) ) {
    		creditNote.setAttribute("credit_note_status", creditNoteDoc.getAttribute("/Status"));
    	}
    	creditNote.setAttribute("end_to_end_id",creditNoteDoc.getAttribute("/EndToEndID"));
    	if ( TradePortalConstants.PAYABLE_ASSIGN_ACTIVY.equals( activityType ) ) {
    	  if( StringFunction.isNotBlank(creditNoteDoc.getAttribute("/TotalCreditNoteAppliedAmount")) ) {
    		  BigDecimal appliedAmt = creditNoteDoc.getAttributeDecimal("/TotalCreditNoteAppliedAmount");
    		  if ( appliedAmt.compareTo(BigDecimal.ZERO) != 0 && appliedAmt.signum() == 1) {
    			creditNote.setAttribute("credit_note_applied_amount", appliedAmt.negate().toPlainString());
    		  }
    	  }
    	  if ( StringFunction.isNotBlank(creditNoteDoc.getAttribute("/CreditNoteAppliedStatus")) ) {
    		creditNote.setAttribute("credit_note_applied_status", creditNoteDoc.getAttribute("/CreditNoteAppliedStatus"));
    	  }    	
          List<DocumentHandler> creditNoteAppliedInvList = creditNoteDoc.getFragmentsList("/CreditNote");
          if ( creditNoteAppliedInvList != null  && !creditNoteAppliedInvList.isEmpty()) {
        	  boolean success = creditNote.modifyInvoiceCreditRelationData ( creditNoteAppliedInvList );  
        	  if ( ! success ) {
        		  mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE, TradePortalConstants.NO_INVOICE_TO_APPLY,
      					creditNoteDoc.getAttribute("/InvoiceID")); 
        		  return;
        	  }
          }
    	}
    	
    	  creditNote.save();
		
	}

	/**
     * This method is used to unpackage the new payable credit notes.
     * @param corpOrg
     * @param customerID
     * @param creditNoteDoc
     * @param mediatorServices
     * @throws RemoteException
     * @throws AmsException
     */
	private void unPackagePayableCreditNote(String corpOrg, String customerID, DocumentHandler creditNoteDoc, MediatorServices mediatorServices) 
			throws RemoteException, AmsException {

		CreditNotes creditNote  =(CreditNotes) mediatorServices.createServerEJB("CreditNotes");
		creditNote.newObject();
		MyXMLTagTranslator myXMLTagTranslator = new MyXMLTagTranslator(creditNoteAttributeMap);
		creditNote.populateFromXmlDoc(creditNoteDoc, "/", myXMLTagTranslator);
		creditNote.setAttribute("buyer_tps_customer_id", customerID);
		creditNote.setAttribute("corp_org_oid", corpOrg);
		if( StringFunction.isNotBlank(creditNoteDoc.getAttribute("/InvcIssueDt")) )
		{
			creditNote.setAttribute("issue_date", myXMLTagTranslator.translateDateTime("InvcIssueDt",creditNoteDoc.getAttribute("/InvcIssueDt"),false));
		}
		if (TradePortalConstants.INDICATOR_YES.equals(creditNoteDoc.getAttribute("/SupplierPortalInvoiceInd"))) {
             creditNote.setAttribute("seller_tps_customer_id", creditNoteDoc.getAttribute("/SellerTPSCustomerID"));   		
	    }
		String credAppliedAmt = creditNoteDoc.getAttribute("/TotalCreditNoteAppliedAmount");
		if( StringFunction.isNotBlank(credAppliedAmt) ){
			BigDecimal amt = new BigDecimal(credAppliedAmt);
			if( amt.compareTo(BigDecimal.ZERO) != 0  && amt.signum() == 1){
				amt = amt.negate(); 
			}
			creditNote.setAttribute("credit_note_applied_amount", amt.toPlainString()); 
		}
		String portalOrigInd =creditNoteDoc.getAttribute("/PortalOriginatedInd");
        if ("PTL".equals(portalOrigInd)){
        	creditNote.setAttribute("portal_originated_ind", TradePortalConstants.INDICATOR_YES);
        }
        else{
        	creditNote.setAttribute("portal_originated_ind",TradePortalConstants.INDICATOR_NO);
        }
        
    	//T36000037578- Override value provided by TPS ('P') with portal value if ('PYB') for Payable
		creditNote.setAttribute("linked_to_instrument_type", TradePortalConstants.INV_LINKED_INSTR_PYB);
		creditNote.setAttribute("end_to_end_id",creditNoteDoc.getAttribute("/EndToEndID"));
        List<DocumentHandler> partyDocs = creditNoteDoc.getFragmentsList("/Party/");
        for ( DocumentHandler partyDoc : partyDocs ) {

            String partyType = partyDoc.getAttribute("/PtyType");
            if ( "SAR".equals(partyType) ) {
            	creditNote.populateFromXmlDoc(partyDoc, "/", new MyXMLTagTranslator(sellerPartyAttributeMap));
            }
            else if ( "BAR".equals(partyType) ) {
            	creditNote.populateFromXmlDoc(partyDoc, "/", new MyXMLTagTranslator(buyerPartyAttributeMap));
            }
        }
        List<DocumentHandler> creditNoteAppliedInvList = creditNoteDoc.getFragmentsList("/CreditNote");
        if ( creditNoteAppliedInvList != null && !creditNoteAppliedInvList.isEmpty()) {
        	boolean success = creditNote.modifyInvoiceCreditRelationData ( creditNoteAppliedInvList );  
      	    if ( ! success ) {
      		  mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE, TradePortalConstants.NO_INVOICE_TO_APPLY,
    					creditNoteDoc.getAttribute("/InvoiceID")); 
      		  return;
      	    }  
        }
               
		creditNote.save();
	}	
	// Nar CR-914A Rel9.2.0.0 02/01/2015 End
	
	/**
	 * This method is used to update applied amount to buyer invoices also for same invoiceOtlUoid.
	 * @param invoiceOid
	 * @param invoiceOtlUoid
	 * @param netInvPayAmt
	 * @param totalCredAppliedAmt
	 * @throws AmsException
	 */
	private void updateAppliedAmountforAllINVRecord(String invoiceOid, String invoiceOtlUoid, String netInvPayAmt, String totalCredAppliedAmt) 
			throws AmsException {
		if ( StringFunction.isNotBlank(invoiceOtlUoid)) {
		   String updateSql = "UPDATE invoice SET inv_amount_after_adj = ?, total_credit_note_amount = ? WHERE otl_invoice_uoid = ?"
				+ " AND invoice_oid != ? AND supplier_portal_invoice_ind != ?";
		   try {
		       DatabaseQueryBean.executeUpdate(updateSql, true, new Object[] { netInvPayAmt, totalCredAppliedAmt, invoiceOtlUoid, invoiceOid, "Y"});
		   } catch(SQLException e) {
			throw new AmsException(e);
		   }
		}
	}
}
