package com.ams.tradeportal.mediator;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;
import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.Vector;

import javax.ejb.RemoveException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.busobj.ArMatchingRule;
import com.ams.tradeportal.busobj.ClientBank;
import com.ams.tradeportal.busobj.CorporateOrganization;
import com.ams.tradeportal.busobj.CreditNotes;
import com.ams.tradeportal.busobj.DocumentImage;
import com.ams.tradeportal.busobj.Instrument;
import com.ams.tradeportal.busobj.InvoiceFileUpload;
import com.ams.tradeportal.busobj.InvoiceGroup;
import com.ams.tradeportal.busobj.InvoicesSummaryData;
import com.ams.tradeportal.busobj.OutgoingInterfaceQueue;
import com.ams.tradeportal.busobj.PanelAuthorizationGroup;
import com.ams.tradeportal.busobj.Terms;
import com.ams.tradeportal.busobj.TermsParty;
import com.ams.tradeportal.busobj.Transaction;
import com.ams.tradeportal.busobj.User;
import com.ams.tradeportal.busobj.util.InstrumentServices;
import com.ams.tradeportal.busobj.util.InvoiceUtility;
import com.ams.tradeportal.busobj.util.POLineItemUtility;
import com.ams.tradeportal.busobj.util.PanelAuthProcessor;
import com.ams.tradeportal.busobj.util.PanelAuthUtility;
import com.ams.tradeportal.busobj.util.SecurityAccess;
import com.ams.tradeportal.common.InstrumentType;
import com.ams.tradeportal.common.MessageType;
import com.ams.tradeportal.common.SQLParamFilter;
import com.ams.tradeportal.common.TPCurrencyUtility;
import com.ams.tradeportal.common.TPDateTimeUtility;
import com.ams.tradeportal.common.TermsPartyType;
import com.ams.tradeportal.common.TradePortalConstants;
import com.ams.tradeportal.common.TransactionStatus;
import com.ams.tradeportal.common.cache.Cache;
import com.ams.tradeportal.common.cache.TPCacheManager;
import com.amsinc.ecsg.frame.AmsConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.ClientServerDataBridge;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.frame.ErrorManager;
import com.amsinc.ecsg.frame.InvalidObjectIdentifierException;
import com.amsinc.ecsg.frame.IssuedError;
import com.amsinc.ecsg.frame.MediatorBean;
import com.amsinc.ecsg.frame.MediatorServices;
import com.amsinc.ecsg.frame.ReferenceDataManager;
import com.amsinc.ecsg.util.DateTimeUtility;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.EncryptDecrypt;
import com.amsinc.ecsg.util.GMTUtility;
import com.amsinc.ecsg.util.JPylonProperties;
import com.amsinc.ecsg.util.StringFunction;
import com.amsinc.ecsg.web.BeanManager;

/*
 *
 *     Copyright  2008
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class UploadInvoiceMediatorBean extends MediatorBean
{
private static final Logger LOG = LoggerFactory.getLogger(UploadInvoiceMediatorBean.class);
	private String SELECT_SQL = "";
	private Set<String> invOIds = new TreeSet<>();
	  // This additional logic is needed to make sure the rules are sorted in following order always at the top
	  // 1. RULE with max number of INV DATA ITEM Columns defined 
	  // 2. if more than 1 rule has max number of colmns defined then the Rule with widest min max days range is always at the top
	  // This ensure that invoice are matched against rule which are very specific in nature as this rule is the best match for the selected
	  // invoices.
	  // In short we are trying to find a rule which best matches the invoice for grouping. 
	  private String EXTRA_SELECT_SQL =" ,  due_or_pay_max_days- due_or_pay_min_days AS daysDiff ,  " +
								       "CASE    WHEN inv_only_create_rule_max_amt IS NULL    THEN 0    ELSE 1  END +  " +
									   "CASE    WHEN due_or_pay_max_days IS NULL    AND due_or_pay_min_days  IS NULL    THEN 0    ELSE 1  END +  " +
									   "CASE    WHEN inv_data_item1 IS NULL    THEN 0    ELSE 1  END +  " +
									   "CASE    WHEN inv_data_item2 IS NULL    THEN 0    ELSE 1  END +  " +
									   "CASE    WHEN inv_data_item3 IS NULL    THEN 0    ELSE 1  END +  " +
									   "CASE    WHEN inv_data_item4 IS NULL    THEN 0    ELSE 1  END +  " +
									   "CASE    WHEN inv_data_item5 IS NULL    THEN 0    ELSE 1  END +  " +
									   "CASE    WHEN inv_data_item6 IS NULL    THEN 0    ELSE 1  END AS colCount,  " +
									   "CASE    WHEN inv_only_create_rule_max_amt IS NULL    THEN 1    ELSE 0    END +  " +
									   "CASE    WHEN due_or_pay_max_days IS NULL    AND due_or_pay_min_days  IS NULL    THEN 1    ELSE 0  END AS tempCount ,  " +
									   "(  CASE    WHEN inv_only_create_rule_max_amt IS NULL    THEN 0    ELSE 1  END +  " +
									   "CASE    WHEN due_or_pay_max_days IS NULL    AND due_or_pay_min_days  IS NULL    THEN 0    ELSE 1  END +  " +
									   "CASE    WHEN inv_data_item1 IS NULL    THEN 0    ELSE 1  END +  " +
									   "CASE    WHEN inv_data_item2 IS NULL    THEN 0    ELSE 1  END +  " +
									   "CASE    WHEN inv_data_item3 IS NULL    THEN 0    ELSE 1  END +  " +
									   "CASE    WHEN inv_data_item4 IS NULL    THEN 0    ELSE 1  END +  " +
									   "CASE    WHEN inv_data_item5 IS NULL    THEN 0    ELSE 1  END +  " +
									   "CASE    WHEN inv_data_item6 IS NULL    THEN 0    ELSE 1  END) - (  " +
									   "CASE     WHEN inv_only_create_rule_max_amt IS NULL    THEN 1    ELSE 0    END +  " +
									   "CASE    WHEN due_or_pay_max_days IS NULL    AND due_or_pay_min_days  IS NULL    THEN 1    ELSE 0  END) as colSort,  " +
									   "CASE    WHEN due_or_pay_max_days IS NULL    AND due_or_pay_min_days  IS NULL    AND inv_only_create_rule_max_amt IS NULL    THEN 0    ELSE 1  END as tCol ";
	  private static String ORDERBY_CLAUSE = " ORDER BY   tCol DESC nulls last, colSort DESC nulls last,  inv_only_create_rule_max_amt DESC nulls last ,  " +
	   										 " daysDiff DESC nulls last,  creation_date_time DESC nulls last ";
	private static String RECEIVABLE_INVOICE_ORDERBY = "order by buyer_name, currency, invdate, a_upload_definition_oid, amount desc";
	private static String PAYABLE_INVOICE_ORDERBY    = "order by seller_name, currency, invdate, a_upload_definition_oid, amount desc";
	private static String INVOICE_SELECT_SQL = "";
	private transient Transaction transaction = null;
	private transient Terms terms = null;
	private transient Instrument newInstr = null;
	private boolean loanReqStatus = true;

  // Business methods
    public DocumentHandler execute(DocumentHandler inputDoc, DocumentHandler outputDoc, MediatorServices mediatorServices)
	throws RemoteException, AmsException {

	String buttonPressed = null;	
	
   
	    // Based on the button clicked, process an appropriate action.
	    buttonPressed = inputDoc.getAttribute("/Update/ButtonPressed");

		if ((TradePortalConstants.BUTTON_INV_DELETE_INVOICES).equals(buttonPressed) || "ListDeleteInvoices".equals(buttonPressed) ||
	    		(TradePortalConstants.PAY_PGM_BUTTON_DELETE_INVOICES).equals(buttonPressed) || TradePortalConstants.BUTTON_INV_DETAIL_DELETE_INVOICES.equals(buttonPressed)) {
		   outputDoc = deleteSelectedInvoice(inputDoc, mediatorServices);
	    } else if(TradePortalConstants.CREDIT_NOTE_PURGE.equals(buttonPressed)){
	    	outputDoc = runPurgeCrNote(mediatorServices);
	    	
	    }else if(TradePortalConstants.INVOICE_PURGE.equals(buttonPressed)){
	    	outputDoc = runPurgeInvoice(mediatorServices);
	    }
	    else if ((TradePortalConstants.BUTTON_INV_ASSIGN_INSTR_TYPE).equals(buttonPressed)|| "ListAssignInstrType".equals(buttonPressed) || "InvAssignInstrType".equals(buttonPressed)) {
   	       outputDoc = assignInstrType(inputDoc, mediatorServices);
        }
	    //AAlubala - CR710 Rel8.0 Check the Proxy / Offline Authorize 
        else if ((TradePortalConstants.BUTTON_INV_AUTHORIZE_INVOICES).equals(buttonPressed) || "ListAuthorizeInvoices".equals(buttonPressed) || (TradePortalConstants.BUTTON_INV_PROXYAUTHORIZE_INVOICES).equals(buttonPressed)){
	       outputDoc = authorizeInvoice(inputDoc, mediatorServices);
        }
        else if (TradePortalConstants.BUTTON_INV_DETAIL_ATTACH_DOCUMENT.equals(buttonPressed) 
        		|| TradePortalConstants.REC_LIST_INV_ATTACH_DOCUMENT.equals(buttonPressed) 
        		|| TradePortalConstants.PAY_PGM_BUTTON_ATTACH_DOCUMENT.equals(buttonPressed)) {
  		   outputDoc = attachDocumentforPayableInvoice(inputDoc, mediatorServices);
  	    }
        else if (((TradePortalConstants.BUTTON_INV_DETAIL_DELETE_DOCUMENT).equals(buttonPressed) 
        		|| (TradePortalConstants.REC_LIST_INV_DELETE_DOCUMENT).equals(buttonPressed)) 
        		|| TradePortalConstants.PAY_PGM_BUTTON_DELETE_DOCUMENT.equals(buttonPressed)) {
 		   outputDoc = deleteAttachedDocForPayableInvoice(inputDoc, mediatorServices);
 	    }
	    else if ((TradePortalConstants.BUTTON_INV_DELETE_DOCUMENT).equals(buttonPressed) || "ListDeleteDocument".equals(buttonPressed)) {
		   outputDoc = deleteAttachedDocuments(inputDoc, mediatorServices);
	    } 
	    else if ((TradePortalConstants.BUTTON_INV_ACCEPT_FINANCING).equals(buttonPressed) || "ListAcceptFinancing".equals(buttonPressed)) {
		   outputDoc = acceptFinancing(inputDoc, mediatorServices);
	    } 
	    else if ((TradePortalConstants.BUTTON_INV_REMOVE_FINANCING).equals(buttonPressed) || "ListRemoveFinancing".equals(buttonPressed)) {
		   outputDoc = removeFinancing(inputDoc, mediatorServices);
	    } 
	    else if ((TradePortalConstants.BUTTON_INV_APPLY_PAYMENT_DATE).equals(buttonPressed) || "ListApplyPaymentDate".equals(buttonPressed)){
  	       outputDoc = applyPaymentDate(inputDoc, mediatorServices);
	    } 
	    else if ((TradePortalConstants.BUTTON_INV_ATTACH_DOCUMENT).equals(buttonPressed) || "ListAttachDocument".equals(buttonPressed)) {
			outputDoc = attachDocument(inputDoc, mediatorServices);
	    }
		else if (TradePortalConstants.BUTTON_INV_RETURN_TO_GROUPS.equals(buttonPressed)) {
			// Do nothing
			outputDoc = new DocumentHandler();
		}
		else if (TradePortalConstants.BUTTON_INV_CLOSE_INVOICE.equals(buttonPressed) || "ListCloseInvoice".equals(buttonPressed)) {
			outputDoc = closeInvoice(inputDoc, mediatorServices);
		}
		else if ((TradePortalConstants.BUTTON_INV_CLEAR_PAYMENT_DATE).equals(buttonPressed) ||
				TradePortalConstants.REC_LIST_CLEAR_PAYMENTDATE.equals(buttonPressed) || "InvClearPaymentDate".equals(buttonPressed)) {
   	       outputDoc = clearPaymentDate(inputDoc, mediatorServices);
        }
		else if ((TradePortalConstants.BUTTON_INV_ASSIGN_LOAN_TYPE).equals(buttonPressed) || ("InvAssignLoanType").equals(buttonPressed)) {
   	       outputDoc = assignLoanType(inputDoc, mediatorServices);
        }
		else if ((TradePortalConstants.BUTTON_INV_CREATE_LOAN_REQ_BY_RULES).equals(buttonPressed) || TradePortalConstants.REC_LIST_CREATE_LOAN_REQ_BY_RULES.equals(buttonPressed)) {
   	       outputDoc = createLoanRequestByRulesForDetail(inputDoc, mediatorServices);
        }
		else if ((TradePortalConstants.BUTTON_INV_CREATE_LOAN_REQ).equals(buttonPressed) || (TradePortalConstants.REC_LIST_INV_CREATE_LOAN_REQ).equals(buttonPressed)) {
	   	       outputDoc = createLoanRequestForDetail(inputDoc, mediatorServices);
	        }
		else if (TradePortalConstants.BUTTON_NOTIFY_PANEL_AUTH_USER.equals(buttonPressed)){
	   			outputDoc = performNotifyPanelAuthUser(inputDoc, mediatorServices);
	   		}
		else if (TradePortalConstants.PAY_INV_BUTTON_MODIFY_SUPP_DATE.equals(buttonPressed)
				|| TradePortalConstants.PAY_INV_BUTTON_APPLY_PAY_AMT.equals(buttonPressed) 
				|| TradePortalConstants.PAY_PGM_BUTTON_APPLY_PAYMENT_DATE.equals(buttonPressed)
				|| TradePortalConstants.PAY_INV_LIST_BUTTON_APPLY_PAY_AMT.equals(buttonPressed)){
	   			outputDoc = performModifyInvoiceData(inputDoc, mediatorServices, buttonPressed);
	   	}
		else if (TradePortalConstants.PAY_MGMT_BUTTON_RESET_SUPP_DATE.equals(buttonPressed) ||
				TradePortalConstants.PAY_MGMT_BUTTON_RESET_PAY_AMOUNT.equals(buttonPressed) || 
				 TradePortalConstants.PAY_PGM_BUTTON_CLEAR_PAYMENT_DATE.equals(buttonPressed)
				 || TradePortalConstants.PAY_INV_LIST_BUTTON_RESET_PAY_AMT.equals(buttonPressed) ){
	   			outputDoc = performResetInvoiceData(inputDoc, mediatorServices,buttonPressed);
	   	}
		else if (TradePortalConstants.PAY_PGM_BUTTON_AUTHORIZ.equals(buttonPressed)){
   			outputDoc = authorisePayPgmInvoice(inputDoc, mediatorServices);
   		}
		else if (TradePortalConstants.PAY_PGM_BUTTON_AUTH_DETAIL_LIST.equals(buttonPressed)){
   			outputDoc = authorisePayPgmInvoiceFromGroupList(inputDoc, mediatorServices);
   		}
		else if (TradePortalConstants.PAY_PGM_BUTTON_RESET_TO_AUTHORIZE.equals(buttonPressed)){
   			outputDoc = resetPayPgmInvoice(inputDoc, mediatorServices);
   		}
		//Rel 9.0 CR 913 END
		else if ("AuthorizeDetailInvoices".equals(buttonPressed)){
		       outputDoc = authorizeInvoiceFromList(inputDoc, mediatorServices);
	        }
		
		else if (TradePortalConstants.PAY_PGM_BUTTON_DECLINE.equals(buttonPressed) || TradePortalConstants.REC_PGM_BUTTON_DECLINE.equals(buttonPressed)
				|| TradePortalConstants.PAY_PGM_DETAIL_BUTTON_DECLINE.equals(buttonPressed) ||TradePortalConstants.REC_PGM_DETAIL_BUTTON_DECLINE.equals(buttonPressed)){
   			outputDoc = declineInvoice(inputDoc, mediatorServices,buttonPressed);
   		}
		else {
			LOG.info("Error in InvoiceMediator - unknown button: " + buttonPressed);
		}
    
    
    
     return outputDoc;
  }
  /**
   * This method performs to set invoice status as 'Deleted'.
   * this method doesn't delete invoice from database, this only set status as deleted invoice.
   * @param inputDoc - mediator get this doc to process
   * @param mediatorServices
   * @return DocumentHandler
   */

    private DocumentHandler deleteSelectedInvoice(DocumentHandler inputDoc, MediatorServices mediatorServices) 
    throws RemoteException, AmsException {
      
	
	String invoiceOid = null;
	String optLock = null;//BSL CR710 02/06/2012 Rel8.0 - implement optimistic locking
	List<DocumentHandler> invoiceList           = inputDoc.getFragmentsList("/UploadInvoiceList/InvoicesSummaryData");
	String userOid        = inputDoc.getAttribute("/UploadInvoiceList/userOid");
	String securityRights = inputDoc.getAttribute("/UploadInvoiceList/securityRights");
	String buttonPressed = inputDoc.getAttribute("/Update/ButtonPressed");
	BigInteger secAcc =SecurityAccess.RECEIVABLES_DELETE_INVOICE;
	
	if(TradePortalConstants.PAY_PGM_BUTTON_DELETE_INVOICES.equals(buttonPressed) || TradePortalConstants.BUTTON_INV_DETAIL_DELETE_INVOICES.equals(buttonPressed)){
		 secAcc =SecurityAccess.PAYABLES_DELETE_INVOICE; //for payables delete check security
	}
	
	if (!SecurityAccess.hasRights(securityRights,secAcc)) {
		// user does not have appropriate authority.
		mediatorServices.getErrorManager().issueError(
			TradePortalConstants.ERR_CAT_1,
			TradePortalConstants.NO_ACTION_MATCH_NOTICE_AUTH,
			mediatorServices.getResourceManager().getText(
					"UploadInvoiceAction.Delete",
					TradePortalConstants.TEXT_BUNDLE));
		return new DocumentHandler();
	}
	
		if (invoiceList.size() <= 0) {
		   mediatorServices.getErrorManager().issueError(
				TradePortalConstants.ERR_CAT_1,
				TradePortalConstants.NO_ITEM_SELECTED,
				mediatorServices.getResourceManager().getText("UploadInvoiceAction.Deleted", TradePortalConstants.TEXT_BUNDLE));
		return (new DocumentHandler());
	   }
	   String action=TradePortalConstants.UPLOAD_INV_ACTION_DELETED;
	   String newStatus = TradePortalConstants.UPLOAD_INV_STATUS_DELETED;
	// Loop through the list of invoice oids, and processing each one in the invoice list
       for ( DocumentHandler invoiceItem :invoiceList) {
	       String[] invoiceData = invoiceItem.getAttribute("/InvoiceData").split("/");
	      invoiceOid = invoiceData[0];
	      optLock = invoiceData[1];
	      
	      try{
	      InvoicesSummaryData invoiceSummaryData = (InvoicesSummaryData) mediatorServices.createServerEJB("InvoicesSummaryData", Long.parseLong(invoiceOid));
	      String invoiceStatus = invoiceSummaryData.getAttribute("invoice_status");
	      String invoiceId     = invoiceSummaryData.getAttribute("invoice_id");
	      String invoiceGroupOid = invoiceSummaryData.getAttribute("invoice_group_oid");
	      // only those invoices can be deleted, whose status is either 'Authorization failed of credit notes or positive invoice' 
	      // or 'Pending Action'
	      if(TradePortalConstants.INVOICE_TYPE_PAY_MGMT.equals(invoiceSummaryData.getAttribute("invoice_classification"))){ //for payables invoice
			if(TradePortalConstants.INV_LINKED_INSTR_PYB.equals(invoiceSummaryData.getAttribute("linked_to_instrument_type")) && !InvoiceUtility.isPayableInvoiceUpdatable(invoiceStatus)){
				 mediatorServices.getErrorManager().issueError(
	     	               UploadInvoiceMediator.class.getName(),
	     	               TradePortalConstants.TRANSACTION_CANNOT_PROCESS, 
	     	               mediatorServices.getResourceManager().getText("UploadInvoices.InvoiceID",TradePortalConstants.TEXT_BUNDLE),
	     	               invoiceId ,
	     	               ReferenceDataManager.getRefDataMgr().getDescr("UPLOAD_INVOICE_STATUS", invoiceStatus, mediatorServices.getCSDB().getLocaleName()),
	     	               mediatorServices.getResourceManager().getText("UploadInvoiceAction.Deleted", TradePortalConstants.TEXT_BUNDLE));
				 continue; // Move on to next Invoice	
			}
			//IR 27049 - validate for non - PYB invoices
			else if (!TradePortalConstants.INV_LINKED_INSTR_PYB.equals(invoiceSummaryData.getAttribute("linked_to_instrument_type")) &&
					!InvoiceUtility.isPayableInvoiceDeletable(invoiceStatus)) {
			      mediatorServices.getErrorManager().issueError(
		     	               UploadInvoiceMediator.class.getName(),
		     	               TradePortalConstants.TRANSACTION_CANNOT_PROCESS, 
		     	               mediatorServices.getResourceManager().getText("UploadInvoices.InvoiceID",TradePortalConstants.TEXT_BUNDLE),
		     	               invoiceId ,
		     	               ReferenceDataManager.getRefDataMgr().getDescr("UPLOAD_INVOICE_STATUS", invoiceStatus, mediatorServices.getCSDB().getLocaleName()),
		     	               mediatorServices.getResourceManager().getText("UploadInvoiceAction.Deleted", TradePortalConstants.TEXT_BUNDLE));
		          continue; // Move on to next Invoice		          		     	        
		       } 
	      }
	      else{ //for rec invoices
	    	  if (!InvoiceUtility.isDeletable(invoiceStatus)) {
			          mediatorServices.getErrorManager().issueError(
			     	               UploadInvoiceMediator.class.getName(),
			     	               TradePortalConstants.TRANSACTION_CANNOT_PROCESS, 
			     	               mediatorServices.getResourceManager().getText("UploadInvoices.InvoiceID",TradePortalConstants.TEXT_BUNDLE),
			     	               invoiceId ,
			     	               ReferenceDataManager.getRefDataMgr().getDescr("UPLOAD_INVOICE_STATUS", invoiceStatus, mediatorServices.getCSDB().getLocaleName()),
			     	               mediatorServices.getResourceManager().getText("UploadInvoiceAction.Deleted", TradePortalConstants.TEXT_BUNDLE));
			          continue; // Move on to next Invoice		          		     	        
			       } 
	      }
			
			   invoiceSummaryData.setAttribute("tp_rule_name", invoiceSummaryData.getTpRuleName());			 
	           
	           if("P".equals(invoiceSummaryData.getAttribute("invoice_classification")) && StringFunction.isNotBlank(invoiceSummaryData.getAttribute("instrument_oid"))
	        		   && StringFunction.isNotBlank(invoiceSummaryData.getAttribute("transaction_oid"))){
					  newStatus = TradePortalConstants.PAYABLE_UPLOAD_INV_STATUS_UNASSIGNED;
					  action =TradePortalConstants.PAYABLE_UPLOAD_INV_ACTION_UNLINKED;
					  invoiceSummaryData.setAttribute("instrument_oid",null);
					 invoiceSummaryData.setAttribute("transaction_oid",null);
				 	 invoiceSummaryData.setAttribute("terms_oid",null);
				  }
				 
	           invoiceSummaryData.setAttribute("invoice_status", newStatus);
	           
	           // Set the optimistic lock attribute to make sure the invoice hasn't been changed by someone else since it was first retrieved
	           invoiceSummaryData.setAttribute("opt_lock", optLock);//BSL CR710 02/03/2012 Rel8.0 ADD
	           invoiceSummaryData.setAttribute("action",  action);
		        invoiceSummaryData.setAttribute("user_oid", userOid);
		       
			   int saveSuccess = invoiceSummaryData.save();
			   String newGroupOid = invoiceSummaryData.getAttribute("invoice_group_oid");
			   if(saveSuccess == 1){

				   if(StringFunction.isNotBlank(newGroupOid) && !invoiceGroupOid.equals(newGroupOid)){
						InvoiceUtility.updateAttachmentDetails(newGroupOid,invoiceGroupOid,invoiceOid,mediatorServices);
					}
				     	// display success message...
					  mediatorServices.getErrorManager().issueError(
								TradePortalConstants.ERR_CAT_1,
								TradePortalConstants.REC_INVOICE_DELETE_SUCCESSFUL,
								mediatorServices.getResourceManager().getText("UploadInvoiceListView.InvoiceID", TradePortalConstants.TEXT_BUNDLE),
								invoiceId);
			   }
	      }
	      catch(Exception e){		   
		   LOG.info("General exception caught in UploadInvoiceMediator:deleteSelectedInvoice" ,e);
	      }
       }	
      return (new DocumentHandler());
    }

   /**
    * This method performs to assign Instrument Type to all seleted Invoices.
    * for this 8.0 release the system will not provide a pop up window  the system will automatically assign the invoice(s) to a 
    * receivables management instrument type.  However, after the Portal Refresh is complete we will incorporate the Pop Up window 
    * concept for other instrument types.
    * intryment can be assigned only when invoice status is 'Pending Action - No Instr Type'
    * @param inputDoc - mediator gets this doc to process
    * @param mediatorServices
    * @return DocumentHandler
    */
    private DocumentHandler assignInstrType(DocumentHandler inputDoc, MediatorServices mediatorServices) 
    throws RemoteException, AmsException {
     List<DocumentHandler> invoiceList = null;
	 String invoiceOid = null;
	 String optLock = null;//BSL CR710 02/06/2012 Rel8.0 - implement optimistic locking

	 int count = 0;
	 invoiceList           = inputDoc.getFragmentsList("/UploadInvoiceList/InvoicesSummaryData");
	 String userOid        = inputDoc.getAttribute("/UploadInvoiceList/userOid");	
	 String loanType       = inputDoc.getAttribute("/UploadInvoiceList/loan_type");
	 String instrumentType = inputDoc.getAttribute("/UploadInvoiceList/linked_to_instrument_type");
	 String corpOrgOid = inputDoc.getAttribute("/UploadInvoiceList/ownerOrg");
	 String buttonPressed = inputDoc.getAttribute("/Update/ButtonPressed");
	 count = invoiceList.size();
	 LOG.debug("InvoiceMediator: number of invoices selected: " + count);
	 if (count <= 0) {
		mediatorServices.getErrorManager().issueError(
				TradePortalConstants.ERR_CAT_1,
				TradePortalConstants.NO_ITEM_SELECTED, 
				mediatorServices.getResourceManager().getText("UploadInvoiceAction.AssignInstrType", TradePortalConstants.TEXT_BUNDLE));
		return (new DocumentHandler());
	 }

	  // Loop through the list of invoice oids, and processing each one in the invoice list
	 JPylonProperties jPylonProperties = JPylonProperties.getInstance();
		String serverLocation             = jPylonProperties.getString("serverLocation");
		BeanManager beanMgr = new BeanManager();
		beanMgr.setServerLocation(serverLocation);
     for (DocumentHandler invoiceItem : invoiceList) {
	  String[] invoiceData = invoiceItem.getAttribute("/InvoiceData").split("/");

	  if(invoiceData.length<=1){

		  mediatorServices.getErrorManager().issueError(
				TradePortalConstants.ERR_CAT_1,
				TradePortalConstants.NO_ITEM_SELECTED, 
				mediatorServices.getResourceManager().getText("UploadInvoiceAction.AssignInstrType", TradePortalConstants.TEXT_BUNDLE));
		return (new DocumentHandler());
	  }
	  //validate loan type against REFDATA-LOANTYPE
			Vector loanList = InvoiceUtility.getLoanTypesFromRefData();
			LOG.info("loanList:"+loanList);
			if(StringFunction.isNotBlank(loanType)){
				 if(!loanList.contains(loanType)){
					 loanType="";
				 }
			}
		LOG.info("finally loanType:"+loanType);

	  invoiceOid = invoiceData[0];
	  optLock = invoiceData[1];
	      try{
	          InvoicesSummaryData  invoiceSummaryData = (InvoicesSummaryData) mediatorServices.createServerEJB("InvoicesSummaryData", Long.parseLong(invoiceOid));
	          String invoiceStatus	   = invoiceSummaryData.getAttribute("invoice_status");
	          String invoiceId		   = invoiceSummaryData.getAttribute("invoice_id");
			  String transactionOid    = invoiceSummaryData.getAttribute("transaction_oid");
 			  String  invoiceGroupOid  = invoiceSummaryData.getAttribute("invoice_group_oid");
 			 String  invoiceType  = invoiceSummaryData.getAttribute("invoice_classification");
 			  
 			 String securityRights = inputDoc.getAttribute("/UploadInvoiceList/securityRights");
 			if (("P".equals(invoiceType) && !SecurityAccess.hasRights(securityRights, SecurityAccess.PAY_ASSIGN_INSTR_TYPE))
 					|| ("R".equals(invoiceType) && !SecurityAccess.hasRights(securityRights, SecurityAccess.RECEIVABLES_ASSIGN_INSTR_TYPE))) {
 				// user does not have appropriate authority.
 				mediatorServices.getErrorManager().issueError(
 						TradePortalConstants.ERR_CAT_1,
 						TradePortalConstants.NO_ACTION_MATCH_NOTICE_AUTH,
 						mediatorServices.getResourceManager().getText(
 								"SecurityProfileDetail.PayAssignInstrType",
 								TradePortalConstants.TEXT_BUNDLE));
 				return new DocumentHandler();
 			}
			  String transactionStatus = null;
			  if(StringFunction.isNotBlank(transactionOid)){
				  Transaction tran = (Transaction) mediatorServices.createServerEJB("Transaction", Long.parseLong(transactionOid));
					 transactionStatus = tran.getAttribute("transaction_status");
					 LOG.debug("transactionStatus:"+transactionStatus);
			  }
			 
			  //Instrument type can be assigned only for 'No Instrument Type' invoice status.
			  if("R".equalsIgnoreCase(invoiceSummaryData.getAttribute("invoice_classification"))){
	     	  if((!TradePortalConstants.UPLOAD_INV_STATUS_PEND_ACT_NO_INSTR_TYPE.equals(invoiceStatus) && ! TradePortalConstants.UPLOAD_INV_STATUS_AVL_FOR_FIN.equals(invoiceStatus))
	     			  || ((TradePortalConstants.BUTTON_INV_ASSIGN_INSTR_TYPE.equals(buttonPressed) && 
					        	(TradePortalConstants.UPLOAD_INV_STATUS_AVL_FOR_AUTH.equals(invoiceStatus) || TradePortalConstants.UPLOAD_INV_STATUS_PAR_AUTH.equals(invoiceStatus))))){
	     	   	    mediatorServices.getErrorManager().issueError(
	     	                UploadInvoiceMediator.class.getName(),
	     	                TradePortalConstants.TRANSACTION_CANNOT_PROCESS, 
	     	                mediatorServices.getResourceManager().getText("UploadInvoices.InvoiceID",TradePortalConstants.TEXT_BUNDLE),
	     	                invoiceId ,
	     	                ReferenceDataManager.getRefDataMgr().getDescr("UPLOAD_INVOICE_STATUS", invoiceStatus, mediatorServices.getCSDB().getLocaleName()),
	     	                mediatorServices.getResourceManager().getText("UploadInvoiceAction.AssignInstrType", TradePortalConstants.TEXT_BUNDLE));
	     	   	 continue; // Move on to next Invoice
	     	        
	     	    } }
			  else if (!TradePortalConstants.UPLOAD_INV_STATUS_PEND_ACT.equals(invoiceStatus)){
				  mediatorServices.getErrorManager().issueError(
	     	                UploadInvoiceMediator.class.getName(),
	     	                TradePortalConstants.TRANSACTION_CANNOT_PROCESS, 
	     	                mediatorServices.getResourceManager().getText("UploadInvoices.InvoiceID",TradePortalConstants.TEXT_BUNDLE),
	     	                invoiceId ,
	     	                ReferenceDataManager.getRefDataMgr().getDescr("UPLOAD_INVOICE_STATUS", invoiceStatus, mediatorServices.getCSDB().getLocaleName()),
	     	                mediatorServices.getResourceManager().getText("UploadInvoiceAction.AssignInstrType", TradePortalConstants.TEXT_BUNDLE));
				  continue;
			  }

			     // check if payment is null or not while assigning TRADE_LOAN 
				 boolean isPayDateNull = InvoiceUtility.isPaymentDateNull(invoiceGroupOid);	
				 CorporateOrganization corporateOrganization = (CorporateOrganization) mediatorServices.createServerEJB("CorporateOrganization",
							Long.parseLong(corpOrgOid));

				 LOG.info("isPayDateNull:"+isPayDateNull);		
			  if(InstrumentType.LOAN_RQST.equals(instrumentType) && TradePortalConstants.TRADE_LOAN.equals(loanType) && !isPayDateNull &&
						TradePortalConstants.BANK_DEF_PAYMENT_ALLOW_NO.equals(corporateOrganization.getAttribute("use_payment_date_allowed_ind"))){
					
					mediatorServices.getErrorManager().issueError(
			    			  UploadInvoiceMediator.class.getName(),
			    			  TradePortalConstants.TRADE_LOAN_NO_PAYMENT_DATE, mediatorServices.getResourceManager().getText("UploadInvoices.Invoice", TradePortalConstants.TEXT_BUNDLE)+ " - "+
										invoiceId);
			    	  return new DocumentHandler();
				}

				if(TradePortalConstants.INV_LINKED_INSTR_REC.equals(instrumentType)  || TradePortalConstants.INV_LINKED_INSTR_PYB.equals(instrumentType)){
					loanType = null;
				}
				if(!TransactionStatus.AUTHORIZED.equals(transactionStatus)){
				invoiceSummaryData.setAttribute("linked_to_instrument_type", instrumentType);

				if(InstrumentType.LOAN_RQST.equals(instrumentType)){
					invoiceSummaryData.setAttribute("loan_type", loanType);
				}
				}else {
					 mediatorServices.getErrorManager().issueError(
	     	                UploadInvoiceMediator.class.getName(),
	     	                TradePortalConstants.REC_INVOICE_ASSIGN_INSTRUMENT_TYPE_NOT_ALLOW, invoiceId);
	     	   	 continue; // Move on to next Invoice
				}
	            // Set the optimistic lock attribute to make sure the invoice hasn't been changed by someone else since it was first retrieved
	            invoiceSummaryData.setAttribute("opt_lock", optLock);//BSL CR710 02/03/2012 Rel8.0 ADD
	            String newStatus = invoiceSummaryData.getAttribute("invoice_status");
	            if (invoiceSummaryData.isCreditNote()) 
	            {
	                	 newStatus = TradePortalConstants.UPLOAD_INV_STATUS_PEND_ACT_CREDIT_NOTE;
	            }
	            else
	               {
				     try 
						{ 
						  if(InstrumentType.LOAN_RQST.equals(instrumentType) && 
								  (StringFunction.isNotBlank(loanType) && TradePortalConstants.EXPORT_FIN.equals(loanType))){
								LOG.debug("loan Type:"+loanType);
								invoiceSummaryData.calculateInterestDiscount(corpOrgOid);
								newStatus = TradePortalConstants.UPLOAD_INV_STATUS_PEND_ACT;
						 }
						  else if(TradePortalConstants.REC.equals(instrumentType)){		
							  invoiceSummaryData.calculateInterestDiscount(beanMgr);
							  newStatus = TradePortalConstants.UPLOAD_INV_STATUS_AVL_FOR_FIN;
						 } 
						  
						}
						catch (AmsException e) 
						{
								IssuedError is = e.getIssuedError();
								if (is != null) {
									String errorCode = is.getErrorCode();
									//BSL IR NNUM040526786 04/05/2012 BEGIN - add Pct Val To Fn Zero and combine three if branches w/identical logic
									if (TradePortalConstants.ERR_UPLOAD_INV_TENOR_DAYS_LESS.equals(errorCode) ||
											TradePortalConstants.ERR_UPLOAD_INV_DUE_PMT_DAYS_DIFF.equals(errorCode) ||
											TradePortalConstants.ERR_UPLOAD_INV_PCT_VAL_TO_FIN_ZERO.equals(errorCode))
									{
										if(TradePortalConstants.REC.equals(instrumentType)){	
										newStatus = TradePortalConstants.UPLOAD_INV_STATUS_PEND_ACT_UNABLE_TO_FIN;
										}
										else {
											   mediatorServices.getErrorManager().issueError(
										    			  UploadInvoiceMediator.class.getName(),
										    			  errorCode,invoiceSummaryData.getAttribute("invoice_id"));
										    	  return new DocumentHandler();

										}
									}
									//BSL IR NNUM040526786 04/05/2012 END
									else {
										mediatorServices.debug("InvoiceGroupMediatorBean.assignInstrType: Unexpected error: " + e.toString());
										throw e;
									}
								}
								else {
									mediatorServices.debug("InvoiceGroupMediatorBean.assignInstrType: Unexpected error: " + e.toString());
									throw e;
								}
						}
				     }

				 String tpName=invoiceSummaryData.getTpRuleName();
				 
					 invoiceSummaryData.setAttribute("tp_rule_name", tpName);
					 if(InstrumentType.LOAN_RQST.equals(instrumentType)){
						 newStatus = TradePortalConstants.UPLOAD_INV_STATUS_PEND_ACT;
					 }
					 else if(TradePortalConstants.INV_LINKED_INSTR_PYB.equals(instrumentType)){
						 newStatus = TradePortalConstants.UPLOAD_INV_STATUS_AVL_FOR_AUTH;
					 }
					 invoiceSummaryData.setAttribute("invoice_status", newStatus);
					 invoiceSummaryData.setAttribute("action",  TradePortalConstants.UPLOAD_INV_ACTION_ASSIGN_INSTR_TYPE);
				        invoiceSummaryData.setAttribute("user_oid", userOid);
				      
				     int saveSuccess = invoiceSummaryData.save();
					 String newGroupOid = invoiceSummaryData.getAttribute("invoice_group_oid");
				     if(saveSuccess == 1){

						if(StringFunction.isNotBlank(newGroupOid) && !invoiceGroupOid.equals(newGroupOid)){
							InvoiceUtility.updateAttachmentDetails(newGroupOid,invoiceGroupOid,invoiceOid,mediatorServices);
						}
					   // display message...
						mediatorServices.getErrorManager().issueError(
								TradePortalConstants.ERR_CAT_1,
								TradePortalConstants.INV_INSTR_ASSIGNED_SUCCESSFUL,
								ReferenceDataManager.getRefDataMgr().getDescr("UPLOAD_INV_LINKED_INSTR_TY", instrumentType, mediatorServices.getCSDB().getLocaleName()),invoiceId);
			         }
	      }
	      catch(Exception e){
	    	  LOG.info("General exception caught in UploadInvoiceMediator:assignInstrType");
	    	  e.printStackTrace();	    	  
	      }	    
     } 
	   
      return (new DocumentHandler());
    }


     /**
      * This method performs authorization of all seleted Invoices.
      * invoice with status 'Authorized failed', 'Partially authorized', 'available for authorization' or 'Pending Action - Credit Notes'
      * @param inputDoc -  this xml input doc which mediator gets
      * @param mediatorServices
      * @return DocumentHandler
      * @throws RemoteException
      * @throws AmsException
      */
     private DocumentHandler authorizeInvoice(DocumentHandler inputDoc, MediatorServices mediatorServices)
     throws RemoteException, AmsException {	  
	
       Vector invoiceList = null;
	   String invoiceOid = null;
	   String optLock = null;//BSL CR710 02/06/2012 Rel8.0 - implement optimistic locking
	   DocumentHandler invoiceItem = null;
	   int count = 0;
	   User user = null;
	   invoiceList           = inputDoc.getFragments("/UploadInvoiceList/InvoicesSummaryData");
	   String corpOrgOid     = inputDoc.getAttribute("/UploadInvoiceList/ownerOrg");
	   String userOid        = inputDoc.getAttribute("/UploadInvoiceList/userOid");
	   String securityRights = inputDoc.getAttribute("/UploadInvoiceList/securityRights");
	   String buttonPressed = inputDoc.getAttribute("/Update/ButtonPressed");
	   count = invoiceList.size();
	   mediatorServices.debug("InvoiceMediator: number of invoices selected: " + count);
	   if (count <= 0) {
			mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.NO_ITEM_SELECTED, 
					mediatorServices.getResourceManager().getText("UploadInvoiceAction.Authorize", TradePortalConstants.TEXT_BUNDLE));
			return new DocumentHandler();
	   }

    // ***********************************************************************
    // check to make sure that the user is allowed to authorize the
    // invoice - the user has to be a corporate user (not an admin
    // user) and have the rights to authorize a Receivables
    // ***********************************************************************
        
       if (!(SecurityAccess.hasRights(securityRights, SecurityAccess.RECEIVABLES_AUTHORIZE_INVOICE))) {
        
    	// issue error that user does not have 'authorize' authority.
        mediatorServices.getErrorManager().issueError(
        		TradePortalConstants.ERR_CAT_1,
                TradePortalConstants.NO_ACTION_INVOICE_AUTH);
        return new DocumentHandler();
    
       }
    
     user    = (User) mediatorServices.createServerEJB("User", Long.parseLong(userOid));
     Map<String, String> tradingPartnerXmlCache = new HashMap<String, String>();
	 CorporateOrganization corpOrg =  (CorporateOrganization) mediatorServices.createServerEJB("CorporateOrganization",
				Long.parseLong(corpOrgOid));
	 SimpleDateFormat formatter=new SimpleDateFormat("MM/dd/yyyy");
	// Loop through the list of invoice oids, and processing each one in the invoice list
	for (int i = 0; i < count; i++) 
	{
		 invoiceItem = (DocumentHandler) invoiceList.get(i);
		 invoiceOid = invoiceItem.getAttribute("/InvoiceData");		 
		 String[] invoiceData = invoiceItem.getAttribute("/InvoiceData").split("/");
		 invoiceOid = invoiceData[0];
		 optLock = invoiceData[1];

		try
		{ 
			InvoicesSummaryData invoiceSummaryData = (InvoicesSummaryData) mediatorServices.createServerEJB("InvoicesSummaryData", Long.parseLong(invoiceOid));
			String invoiceStatus  = invoiceSummaryData.getAttribute("invoice_status");
			String invoiceId      = invoiceSummaryData.getAttribute("invoice_id");
			String invoiceGroupOid  = invoiceSummaryData.getAttribute("invoice_group_oid"); //IR T36000016545 
			boolean isCreditNote  = invoiceSummaryData.isCreditNote();
			boolean isFinanceable = !TradePortalConstants.INDICATOR_YES.equals(invoiceSummaryData.getAttribute("portal_approval_requested_ind")) && InvoiceUtility.isFinanceable(invoiceStatus);
			// only 'Available for Authorization', 'Partially Authorized', 'Auth Failed',
			// 'Unable to Finance', or 'Credit Note' invoices can be authorized, when not navigation from Group
			if (!InvoiceUtility.isAuthorizable(invoiceStatus) || 
					(InvoiceUtility.isAuthorizable(invoiceStatus) && (TradePortalConstants.BUTTON_INV_AUTHORIZE_INVOICES.equals(buttonPressed) || TradePortalConstants.BUTTON_INV_AUTHORIZE_CREDIT_NOTE.equals(buttonPressed)))) {
				mediatorServices.getErrorManager().issueError(
						UploadInvoiceMediator.class.getName(),
						TradePortalConstants.TRANSACTION_CANNOT_PROCESS, mediatorServices.getResourceManager().getText("UploadInvoices.InvoiceID",TradePortalConstants.TEXT_BUNDLE),
						invoiceId ,
						ReferenceDataManager.getRefDataMgr().getDescr("UPLOAD_INVOICE_STATUS", invoiceStatus, mediatorServices.getCSDB().getLocaleName()),
						mediatorServices.getResourceManager().getText("TransactionAction.Authorized", TradePortalConstants.TEXT_BUNDLE));  

				continue; // Move on to next Invoice
			}   

			String authorizeRights    = null;
			String authorizeType      = null;
			String userWorkGroupOid         = user.getAttribute("work_group_oid");
			String firstAuthorizer          = invoiceSummaryData.getAttribute("first_authorizing_user_oid");
			String firstAuthorizerWorkGroup = invoiceSummaryData.getAttribute("first_authorizing_work_group_oid");	  	        

			String buyerName =invoiceSummaryData.getAttribute("buyer_name");
			String buyerId =invoiceSummaryData.getAttribute("buyer_id");
			if(buyerName==null)buyerName="";
			if(buyerId==null)buyerId="";
				
			String key =  buyerName+ buyerId;			
			String ruleOid = null;	
			ArMatchingRule rule =null;
			if (tradingPartnerXmlCache.containsKey(key)) {
				// retrieve from cache
				ruleOid = tradingPartnerXmlCache.get(key); 
				rule= (ArMatchingRule) mediatorServices.createServerEJB("ArMatchingRule", Long.parseLong(ruleOid));
			}
			else {
				rule = invoiceSummaryData.getMatchingRule();
				if(rule!=null){
					ruleOid=	rule.getAttribute("ar_matching_rule_oid");
					tradingPartnerXmlCache.put(key, ruleOid);
				}
			}     
			if(rule==null){
				mediatorServices.getErrorManager().issueError(
						PayRemitMediator.class.getName(),
						TradePortalConstants.TP_RULE_FOR_AUTH,mediatorServices.getResourceManager().getText("UploadInvoices.InvoiceID",TradePortalConstants.TEXT_BUNDLE),
						invoiceId);
				continue; // Move on to next Invoice
			}
			//CR 913Checking Threshold amount and daily limit.
			//Rpasupulati start
			
			String thresholdGroupOid=user.getAttribute("threshold_group_oid");
			String userOrgOid = user.getAttribute("owner_org_oid");	
			
			if(thresholdGroupOid!=null){
				
					String invoiceamount=invoiceSummaryData.getAttribute("amount");			
					String selectClause="select distinct upload_invoice_oid,currency as currency_code, amount  as payment_amount from invoices_summary_data ";
					String clientBankOid = inputDoc.getAttribute("/InvoiceGroupList/clientBankOid");
					
					thresholdGroupOid = user.getAttribute("threshold_group_oid");
					String baseCurrency=corpOrg.getAttribute("base_currency_code");
					
					if(!(InvoiceUtility.checkThresholds(invoiceamount,invoiceSummaryData.getAttribute("currency"),baseCurrency,
					"upload_rec_inv_thold","upload_rec_inv_dlimit",
		            userOrgOid,clientBankOid,user,selectClause,false,"",
		           mediatorServices))){
						//if errorsFound is true returning new DocumentHandler
						return new DocumentHandler();
					}
			}
			
			//CR 913Checking Threshold amount and daily limit.
			//Rpasupulati end
			if(rule != null)
			{
				if (isCreditNote) {
					authorizeRights = rule.getAttribute("credit_note");
				}
				else {
					authorizeRights = rule.getAttribute("receivable_invoice");
				}	 
				
				// IR T36000016682 If Corporate customer profile setting is applicable for authorization, 
				// Trading Partner level rule is not applicable. 
				
					if (isCreditNote) {
						authorizeType = rule.getAttribute("credit_authorise");
					}
					else {
						authorizeType = rule.getAttribute("receivable_authorise");
					}

					if (TradePortalConstants.DUAL_AUTH_REQ_INV_CORPORATE.equals(authorizeType)) {
						// corporate org needs to be retrieved at most once
						// for all groups being processed, so do it lazily
						if (corpOrg == null) {
							corpOrg = (CorporateOrganization) mediatorServices.createServerEJB("CorporateOrganization",
									Long.parseLong(corpOrgOid));
						}

						if (isCreditNote) {
							authorizeRights = corpOrg.getAttribute("allow_credit_note");
							authorizeType = corpOrg.getAttribute("dual_auth_credit_note");
						}
						else {
							authorizeRights = corpOrg.getAttribute("allow_arm_invoice");
							authorizeType = corpOrg.getAttribute("dual_auth_arm_invoice");
						}
					}

			}
			// if user has authority rights then only should process, else thow error 
			//that user doesn't have right to authorize this perticular invoice. every invoice associate to trading partner rule, 
			//so authority type can be diferent for every invoice.it is not depend on user.
			// it is depend on user only when 'look for corporate customer profile' is selected in trading partner refdata of that invoice.
			

 			if (!TradePortalConstants.INDICATOR_YES.equals(authorizeRights)) 
			{
				mediatorServices.getErrorManager().issueError(
						PayRemitMediator.class.getName(),
						TradePortalConstants.NO_ACTION_TRANSACTION_AUTH,mediatorServices.getResourceManager().getText("UploadInvoices.InvoiceID",TradePortalConstants.TEXT_BUNDLE),
						invoiceId ,
						mediatorServices.getResourceManager().getText("TransactionAction.Authorize", TradePortalConstants.TEXT_BUNDLE));
				continue; // Move on to next Invoice

			} 
 			 invoiceSummaryData.setAttribute("action",  TradePortalConstants.UPLOAD_INV_ACTION_AUTH);
		        invoiceSummaryData.setAttribute("user_oid", userOid);
		    
			String newStatus = null;
			boolean isCompliant = true;
			// if two different workgroups required authority is selected, then user must belong to a group
			if ((TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS).equals(authorizeType)
					&& (StringFunction.isBlank(userWorkGroupOid))) 
			{
				if (!isFinanceable)
				{
					newStatus = TradePortalConstants.UPLOAD_INV_STATUS_AUTH_FAILED;
				}
				else
				{
					newStatus = TradePortalConstants.UPLOAD_INV_STATUS_FIN_AUTH_FAILED;  
				}
				mediatorServices.getErrorManager().issueError(
						PayRemitMediator.class.getName(),
						TradePortalConstants.TRANS_WORK_GROUP_REQUIRED);
				invoiceSummaryData.setAttribute("tp_rule_name", invoiceSummaryData.getTpRuleName());
				invoiceSummaryData.setAttribute("invoice_status", newStatus);
				invoiceSummaryData.save();
			}
			//***********************************************************************
			// If dual authorization is required, set the transaction state to
			// partially authorized if it has not been previously authorized and set
			// the transaction to authorized if there has been a previous
			// authorizer.
			// The second authorizer must not be the same as the first authorizer
			// not the same work groups, depending on the corporate org setting.
			// If dual authorization is not required, set the invoice state to
			// authorized.
			// ***********************************************************************    
			else if ((TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS).equals(authorizeType)
					||(TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS).equals(authorizeType))
			{
				newStatus = null;
				if (StringFunction.isBlank(firstAuthorizer)) 
				{
					if (isFinanceable)
					{		
						try {
							InvoiceUtility.calculateNumTenorDays(rule,null,formatter,invoiceSummaryData.getAttribute("due_date"),invoiceSummaryData.getAttribute("payment_date"));// checks compliance
							newStatus = TradePortalConstants.UPLOAD_INV_STATUS_FIN_PAR_AUTH;
						}
						catch (AmsException e) {
							isCompliant = false;
							IssuedError is = e.getIssuedError();
							if (is != null) {
								String errorCode = is.getErrorCode();
								//BSL IR NNUM040526786 04/05/2012 BEGIN - add Pct Val To Fn Zero and combine three if branches w/identical logic
								if (TradePortalConstants.ERR_UPLOAD_INV_TENOR_DAYS_LESS.equals(errorCode) ||
										TradePortalConstants.ERR_UPLOAD_INV_DUE_PMT_DAYS_DIFF.equals(errorCode) ||
										TradePortalConstants.ERR_UPLOAD_INV_PCT_VAL_TO_FIN_ZERO.equals(errorCode))
								{
									newStatus = TradePortalConstants.UPLOAD_INV_STATUS_PEND_ACT_UNABLE_TO_FIN;
								}
								//BSL IR NNUM040526786 04/05/2012 END
								else {
									mediatorServices.debug("InvoiceGroupMediatorBean.authorizeInvoice: Unexpected error: " + e.toString());
									throw e;
								}
							}
							else {
								mediatorServices.debug("InvoiceGroupMediatorBean.authorizeInvoice: Unexpected error: " + e.toString());
								throw e;
							}
						}
					}else{
						newStatus = TradePortalConstants.UPLOAD_INV_STATUS_PAR_AUTH;
					}
					if(isCompliant)
					{
						invoiceSummaryData.setAttribute("first_authorizing_user_oid", userOid);
						invoiceSummaryData.setAttribute("first_authorizing_work_group_oid", userWorkGroupOid);
						invoiceSummaryData.setAttribute("first_authorize_status_date", DateTimeUtility.getGMTDateTime());
					}
					invoiceSummaryData.setAttribute("invoice_status", newStatus);	             
					// Set the optimistic lock attribute to make sure the invoice hasn't been changed by someone else since it was first retrieved
					invoiceSummaryData.setAttribute("opt_lock", optLock);//BSL CR710 02/03/2012 Rel8.0 ADD
					 //tp mod shilpa start
					 String tpName=invoiceSummaryData.getTpRuleName();
					//tp mod shilpa end
					invoiceSummaryData.setAttribute("tp_rule_name", tpName);
					int success = invoiceSummaryData.save();	                   
					if (success == 1)
					{                   
						//display message...
						if(isCompliant){
							mediatorServices.getErrorManager().issueError(
									UploadInvoiceMediator.class.getName(),
									TradePortalConstants.TRANS_PARTIALLY_AUTHORIZED,
									mediatorServices.getResourceManager().getText("UploadInvoices.InvoiceID",TradePortalConstants.TEXT_BUNDLE),
									invoiceId);
						}
					}
				} 
				// if firstAuthorizer is not empty
				else 
				{
					// Two work groups required and this user belongs to the
					// same work group as the first authorizer: Error.
					if ((TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS).equals(authorizeType)
							&& userWorkGroupOid.equals(firstAuthorizerWorkGroup)) 
					{	
						if (!isFinanceable)
						{
							newStatus = TradePortalConstants.UPLOAD_INV_STATUS_AUTH_FAILED;
						}
						else
						{
							newStatus = TradePortalConstants.UPLOAD_INV_STATUS_FIN_AUTH_FAILED;  
						}
						mediatorServices.getErrorManager().issueError(
								PayRemitMediator.class.getName(),
								TradePortalConstants.TRANS_DIFF_WORK_GROUP_REQUIRED);
						invoiceSummaryData.setAttribute("invoice_status", newStatus);
						 String tpName=invoiceSummaryData.getTpRuleName(); 
						invoiceSummaryData.setAttribute("tp_rule_name", tpName);
				        invoiceSummaryData.setAttribute("reqPanleAuth",TradePortalConstants.INDICATOR_YES);

						invoiceSummaryData.save();
					}
					// Two authorizers required and this user is the same as the
					// first authorizer. Give error.
					// Note requiring two work groups also implies requiring two
					// users.
					else if (firstAuthorizer.equals(userOid)) 
					{
						if (!isFinanceable)
						{
							newStatus = TradePortalConstants.UPLOAD_INV_STATUS_AUTH_FAILED;
						}
						else
						{
							newStatus = TradePortalConstants.UPLOAD_INV_STATUS_FIN_AUTH_FAILED;  
						}
						mediatorServices.getErrorManager().issueError(
								PayRemitMediator.class.getName(),
								TradePortalConstants.DIFFERENT_SECOND_AUTH_REQRUIED);
						invoiceSummaryData.setAttribute("invoice_status", newStatus);
						 String tpName=invoiceSummaryData.getTpRuleName(); 
						invoiceSummaryData.setAttribute("tp_rule_name", tpName);
				        invoiceSummaryData.setAttribute("reqPanleAuth", TradePortalConstants.INDICATOR_YES);

						invoiceSummaryData.save() ;
					}
					// Authorize
					else 
					{
						if (isFinanceable)
						{		
							try {
								InvoiceUtility.calculateNumTenorDays(rule,null,formatter,invoiceSummaryData.getAttribute("due_date"),invoiceSummaryData.getAttribute("payment_date"));// checks compliance
								newStatus = TradePortalConstants.UPLOAD_INV_STATUS_FIN_AUTH;
							}
							catch (AmsException e) {
								isCompliant = false;
								IssuedError is = e.getIssuedError();
								if (is != null) {
									String errorCode = is.getErrorCode();
									//BSL IR NNUM040526786 04/05/2012 BEGIN - add Pct Val To Fn Zero and combine three if branches w/identical logic
									if (TradePortalConstants.ERR_UPLOAD_INV_TENOR_DAYS_LESS.equals(errorCode) ||
											TradePortalConstants.ERR_UPLOAD_INV_DUE_PMT_DAYS_DIFF.equals(errorCode) ||
											TradePortalConstants.ERR_UPLOAD_INV_PCT_VAL_TO_FIN_ZERO.equals(errorCode))
									{
										newStatus = TradePortalConstants.UPLOAD_INV_STATUS_PEND_ACT_UNABLE_TO_FIN;
									}
									//BSL IR NNUM040526786 04/05/2012 END
									else {
										mediatorServices.debug("InvoiceGroupMediatorBean.authorizeInvoice: Unexpected error: " + e.toString());
										throw e;
									}
								}
								else {
									mediatorServices.debug("InvoiceGroupMediatorBean.authorizeInvoice: Unexpected error: " + e.toString());
									throw e;
								}
							}
						}else
						{
							newStatus = TradePortalConstants.UPLOAD_INV_STATUS_AUTH;
						}
						if(isCompliant)
						{
							invoiceSummaryData.setAttribute("second_authorizing_user_oid", userOid);
							invoiceSummaryData.setAttribute("second_authorizing_work_group_oid", userWorkGroupOid);
							invoiceSummaryData.setAttribute("second_authorize_status_date", DateTimeUtility.getGMTDateTime());
						} 	
						invoiceSummaryData.setAttribute("invoice_status", newStatus);
						// Set the optimistic lock attribute to make sure the invoice hasn't been changed by someone else since it was first retrieved
						invoiceSummaryData.setAttribute("opt_lock", optLock);//BSL CR710 02/03/2012 Rel8.0 ADD
						 String tpName=invoiceSummaryData.getTpRuleName(); 
						invoiceSummaryData.setAttribute("tp_rule_name", tpName);
						int success = invoiceSummaryData.save();
						if(success == 1)
						{
							if(isCompliant)
							{		                      
								// display message...
								mediatorServices.getErrorManager().issueError(
										UploadInvoiceMediator.class.getName(),
										TradePortalConstants.INV_AUTH_SUCCESSFUL,
										mediatorServices.getResourceManager().getText("UploadInvoices.InvoiceID",TradePortalConstants.TEXT_BUNDLE),
										invoiceId);
								//SHR CR708 Rel8.1.1- modified to include InvoiceType
								createOutgoingQueueAndIssueSuccess(newStatus, invoiceOid,invoiceSummaryData.getAttribute("invoice_type"), isCreditNote, mediatorServices);
							}   
						}
					}
				}
			//Pavani CR 821 Rel 8.3 START
			}else if(TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH.equals(authorizeType) 
					&& corpOrg.isPanelAuthEnabled(TradePortalConstants.INVOICE)){
				PanelAuthorizationGroup panelAuthGroup = null;
				String panelGroupOid = null;
				String panelOplockVal = null;
				String panelRangeOid = null;
				CorporateOrganization transactionOwnerOrg = (CorporateOrganization) mediatorServices.createServerEJB("CorporateOrganization",  Long.parseLong(corpOrgOid));
				
				if(StringFunction.isBlank(invoiceSummaryData.getAttribute("panel_auth_group_oid"))) {
					
					panelGroupOid = PanelAuthUtility.getPanelAuthorizationGroupOid(TradePortalConstants.INVOICE, corpOrg, mediatorServices.getErrorManager());   				
    				if(StringFunction.isNotBlank(panelGroupOid)){
    					panelAuthGroup = (PanelAuthorizationGroup) mediatorServices.createServerEJB("PanelAuthorizationGroup",
    							Long.parseLong(panelGroupOid));   					
    				}else{
    					// issue error as panel group is required if panel auth defined for instrument type
    					mediatorServices.getErrorManager().issueError(
    								TradePortalConstants.ERR_CAT_1,
    								AmsConstants.REQUIRED_ATTRIBUTE, 
    								mediatorServices.getResourceManager().getText("CorpCust.PanelGroupId", TradePortalConstants.TEXT_BUNDLE));
    					// if panel id is blank, return the flow back.
    					return new DocumentHandler();
    				}
					
    				invoiceSummaryData.setAttribute("panel_auth_group_oid", panelAuthGroup.getAttribute("panel_auth_group_oid"));
    				invoiceSummaryData.setAttribute("panel_oplock_val", panelAuthGroup.getAttribute("opt_lock"));
    				
    				String baseCurrencyToPanelGroup = PanelAuthUtility.getBaseCurrency(transactionOwnerOrg, panelAuthGroup, null, TradePortalConstants.INVOICE);
    				
    	    		BigDecimal amountInBaseToPanelGroup =  PanelAuthUtility.getAmountInBaseCurrency
    	    		   (invoiceSummaryData.getAttribute("currency"), invoiceSummaryData.getAttribute("amount"), baseCurrencyToPanelGroup, corpOrgOid, 
    	    				   TradePortalConstants.INVOICE, null, false, mediatorServices.getErrorManager());
    	    		
    	    		panelRangeOid = PanelAuthUtility.getPanelRange(panelAuthGroup.getAttribute("panel_auth_group_oid"), amountInBaseToPanelGroup);
    	    		if(StringFunction.isBlank(panelRangeOid)){
    	    			mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
    							TradePortalConstants.NO_PANEL_RANGE_FOR_AMOUNT_INST_TYPE, amountInBaseToPanelGroup.toString(), TradePortalConstants.INVOICE);
    	    			return new DocumentHandler();
    	    		}
    	    		invoiceSummaryData.setAttribute("panel_auth_range_oid", panelRangeOid);
    				
    			}
				
    			panelGroupOid  = invoiceSummaryData.getAttribute("panel_auth_group_oid");
    		    panelOplockVal = invoiceSummaryData.getAttribute("panel_oplock_val");
    		    panelRangeOid =  invoiceSummaryData.getAttribute("panel_auth_range_oid");   	
    		    
    	       PanelAuthProcessor panelAuthProcessor = new PanelAuthProcessor(user, TradePortalConstants.INVOICE, mediatorServices.getErrorManager());
    	       panelAuthProcessor.init(panelGroupOid, new String[] {panelRangeOid}, panelOplockVal);

    	       String panelAuthTransactionStatus = panelAuthProcessor.process(invoiceSummaryData, panelRangeOid, false);
    	       if (TradePortalConstants.AUTHTRUE.equals(panelAuthTransactionStatus)) {
    	    	   invoiceSummaryData.setAttribute("reqPanleAuth",  TradePortalConstants.INDICATOR_YES);
    	    	   newStatus = null;
    	    	   if (isFinanceable)
    					//BSL IR NLUM042039026 04/20/2012 Rel 8.0 END
    					{
    						try {
								InvoiceUtility.calculateNumTenorDays(rule,null,formatter,invoiceSummaryData.getAttribute("due_date"),invoiceSummaryData.getAttribute("payment_date"));// checks compliance
    							newStatus = TradePortalConstants.UPLOAD_INV_STATUS_FIN_AUTH;
    						}
    						catch (AmsException e) {
    							isCompliant = false;
    							IssuedError is = e.getIssuedError();
    							if (is != null) {
    								String errorCode = is.getErrorCode();
    								//BSL IR NNUM040526786 04/05/2012 BEGIN - add Pct Val To Fn Zero and combine three if branches w/identical logic
    								if (TradePortalConstants.ERR_UPLOAD_INV_TENOR_DAYS_LESS.equals(errorCode) ||
    										TradePortalConstants.ERR_UPLOAD_INV_DUE_PMT_DAYS_DIFF.equals(errorCode) ||
    										TradePortalConstants.ERR_UPLOAD_INV_PCT_VAL_TO_FIN_ZERO.equals(errorCode))
    								{
    									newStatus = TradePortalConstants.UPLOAD_INV_STATUS_PEND_ACT_UNABLE_TO_FIN;
    								}
    								//BSL IR NNUM040526786 04/05/2012 END
    								else {
    									mediatorServices.debug("InvoiceGroupMediatorBean.authorizeInvoice: Unexpected error: " + e.toString());
    									throw e;
    								}
    							}
    							else {
    								mediatorServices.debug("InvoiceGroupMediatorBean.authorizeInvoice: Unexpected error: " + e.toString());
    								throw e;
    							}
    						}
    					}else
    					{
    						newStatus = TradePortalConstants.UPLOAD_INV_STATUS_AUTH;
    					}
    					if(isCompliant)
    					{
    						invoiceSummaryData.setAttribute("first_authorizing_user_oid", userOid);
    						invoiceSummaryData.setAttribute("first_authorizing_work_group_oid", userWorkGroupOid);
    						invoiceSummaryData.setAttribute("first_authorize_status_date", DateTimeUtility.getGMTDateTime());
    					}
    					invoiceSummaryData.setAttribute("invoice_status", newStatus);
    					// Set the optimistic lock attribute to make sure the invoice hasn't been changed by someone else since it was first retrieved
    					invoiceSummaryData.setAttribute("opt_lock", optLock);//BSL CR710 02/03/2012 Rel8.0 ADD
    					 String tpName=invoiceSummaryData.getTpRuleName(); 
    					invoiceSummaryData.setAttribute("tp_rule_name", tpName);
    			       

    					int success = invoiceSummaryData.save();
    					String newGroupOid = invoiceSummaryData.getAttribute("invoice_group_oid");
    					if(success == 1)
    					{   
    						if(StringFunction.isNotBlank(newGroupOid) && !invoiceGroupOid.equals(newGroupOid)){
    							InvoiceUtility.updateAttachmentDetails(newGroupOid,invoiceGroupOid,invoiceOid,mediatorServices);
    						}
    						if(isCompliant){                        
    							// display message...
    							mediatorServices.getErrorManager().issueError(
    									UploadInvoiceMediator.class.getName(),
    									TradePortalConstants.INV_AUTH_SUCCESSFUL,
    									mediatorServices.getResourceManager().getText("UploadInvoices.InvoiceID",TradePortalConstants.TEXT_BUNDLE),
    									invoiceId);	
    							//SHR CR708 Rel8.1.1- modified to include InvoiceType
    							createOutgoingQueueAndIssueSuccess(newStatus, invoiceOid,invoiceSummaryData.getAttribute("invoice_type"), isCreditNote, mediatorServices);

    						}
    					}
    	       }else if (TradePortalConstants.AUTHPARTL.equals(panelAuthTransactionStatus)) {
    	    	   newStatus = null;
   					if (isFinanceable)
   					{		
   						try {
							InvoiceUtility.calculateNumTenorDays(rule,null,formatter,invoiceSummaryData.getAttribute("due_date"),invoiceSummaryData.getAttribute("payment_date"));// checks compliance

   							newStatus = TradePortalConstants.UPLOAD_INV_STATUS_FIN_PAR_AUTH;
   						}
   						catch (AmsException e) {
   							isCompliant = false;
   							IssuedError is = e.getIssuedError();
   							if (is != null) {
   								String errorCode = is.getErrorCode();
   								//BSL IR NNUM040526786 04/05/2012 BEGIN - add Pct Val To Fn Zero and combine three if branches w/identical logic
   								if (TradePortalConstants.ERR_UPLOAD_INV_TENOR_DAYS_LESS.equals(errorCode) ||
   										TradePortalConstants.ERR_UPLOAD_INV_DUE_PMT_DAYS_DIFF.equals(errorCode) ||
   										TradePortalConstants.ERR_UPLOAD_INV_PCT_VAL_TO_FIN_ZERO.equals(errorCode))
   								{
   									newStatus = TradePortalConstants.UPLOAD_INV_STATUS_PEND_ACT_UNABLE_TO_FIN;
   								}
   								//BSL IR NNUM040526786 04/05/2012 END
   								else {
   									mediatorServices.debug("InvoiceGroupMediatorBean.authorizeInvoice: Unexpected error: " + e.toString());
   									throw e;
   								}
   							}
   							else {
   								mediatorServices.debug("InvoiceGroupMediatorBean.authorizeInvoice: Unexpected error: " + e.toString());
   								throw e;
   							}
   						}
   					}else{
   						newStatus = TradePortalConstants.UPLOAD_INV_STATUS_PAR_AUTH;
   					}
   					if(isCompliant)
   					{
   						invoiceSummaryData.setAttribute("first_authorizing_user_oid", userOid);
   						invoiceSummaryData.setAttribute("first_authorizing_work_group_oid", userWorkGroupOid);
   						invoiceSummaryData.setAttribute("first_authorize_status_date", DateTimeUtility.getGMTDateTime());
   					}
   					invoiceSummaryData.setAttribute("invoice_status", newStatus);	             
   					// Set the optimistic lock attribute to make sure the invoice hasn't been changed by someone else since it was first retrieved
   					invoiceSummaryData.setAttribute("opt_lock", optLock);//BSL CR710 02/03/2012 Rel8.0 ADD
   				 String tpName=invoiceSummaryData.getTpRuleName();
   					invoiceSummaryData.setAttribute("tp_rule_name", tpName);
   					int success = invoiceSummaryData.save();	                   
   					if (success == 1)
   					{  //display message...
   						if(isCompliant){
   							mediatorServices.getErrorManager().issueError(
   									UploadInvoiceMediator.class.getName(),
   									TradePortalConstants.TRANS_PARTIALLY_AUTHORIZED,
   									mediatorServices.getResourceManager().getText("UploadInvoices.InvoiceID",TradePortalConstants.TEXT_BUNDLE),
   									invoiceId);
   						}
   					}
    	    	   
    	    	   
               }else if (TradePortalConstants.AUTHFALSE.equals(panelAuthTransactionStatus)) {
            	   newStatus = null;
            	   if (!isFinanceable)
						{
							newStatus = TradePortalConstants.UPLOAD_INV_STATUS_AUTH_FAILED;
						}
						else
						{
							newStatus = TradePortalConstants.UPLOAD_INV_STATUS_FIN_AUTH_FAILED;  
						}
						invoiceSummaryData.setAttribute("invoice_status", newStatus);
						 String tpName=invoiceSummaryData.getTpRuleName();
						invoiceSummaryData.setAttribute("tp_rule_name", tpName);
						invoiceSummaryData.save();
    	       } 
			}
			//Pavani CR 821 Rel 8.3 END
			else 
			{
				// if dual auth ind is NO, then last option is first user can authorize invoice and first authorized oid is blank
				if (isFinanceable)
				{
					try {
						InvoiceUtility.calculateNumTenorDays(rule,null,formatter,invoiceSummaryData.getAttribute("due_date"),invoiceSummaryData.getAttribute("payment_date"));// checks compliance
						newStatus = TradePortalConstants.UPLOAD_INV_STATUS_FIN_AUTH;
					}
					catch (AmsException e) {
						isCompliant = false;
						IssuedError is = e.getIssuedError();
						if (is != null) {
							String errorCode = is.getErrorCode();
							//BSL IR NNUM040526786 04/05/2012 BEGIN - add Pct Val To Fn Zero and combine three if branches w/identical logic
							if (TradePortalConstants.ERR_UPLOAD_INV_TENOR_DAYS_LESS.equals(errorCode) ||
									TradePortalConstants.ERR_UPLOAD_INV_DUE_PMT_DAYS_DIFF.equals(errorCode) ||
									TradePortalConstants.ERR_UPLOAD_INV_PCT_VAL_TO_FIN_ZERO.equals(errorCode))
							{
								newStatus = TradePortalConstants.UPLOAD_INV_STATUS_PEND_ACT_UNABLE_TO_FIN;
							}
							//BSL IR NNUM040526786 04/05/2012 END
							else {
								mediatorServices.debug("InvoiceGroupMediatorBean.authorizeInvoice: Unexpected error: " + e.toString());
								throw e;
							}
						}
						else {
							mediatorServices.debug("InvoiceGroupMediatorBean.authorizeInvoice: Unexpected error: " + e.toString());
							throw e;
						}
					}
				}else
				{
					newStatus = TradePortalConstants.UPLOAD_INV_STATUS_AUTH;
				}
				if(isCompliant)
				{
					invoiceSummaryData.setAttribute("first_authorizing_user_oid", userOid);
					invoiceSummaryData.setAttribute("first_authorizing_work_group_oid", userWorkGroupOid);
					invoiceSummaryData.setAttribute("first_authorize_status_date", DateTimeUtility.getGMTDateTime());
				}
				invoiceSummaryData.setAttribute("invoice_status", newStatus);
				// Set the optimistic lock attribute to make sure the invoice hasn't been changed by someone else since it was first retrieved
				invoiceSummaryData.setAttribute("opt_lock", optLock);//BSL CR710 02/03/2012 Rel8.0 ADD
	             String tpName=rule.getAttribute("buyer_name");
				invoiceSummaryData.setAttribute("tp_rule_name", tpName);
		        invoiceSummaryData.setAttribute("reqPanleAuth",  TradePortalConstants.INDICATOR_YES);

				int success = invoiceSummaryData.save();
				String newGroupOid = invoiceSummaryData.getAttribute("invoice_group_oid");
				if(success == 1)
				{   
					if(StringFunction.isNotBlank(newGroupOid) && !invoiceGroupOid.equals(newGroupOid)){
						InvoiceUtility.updateAttachmentDetails(newGroupOid,invoiceGroupOid,invoiceOid,mediatorServices);
					}
					if(isCompliant){                        
						// display message...
						mediatorServices.getErrorManager().issueError(
								UploadInvoiceMediator.class.getName(),
								TradePortalConstants.INV_AUTH_SUCCESSFUL,
								mediatorServices.getResourceManager().getText("UploadInvoices.InvoiceID",TradePortalConstants.TEXT_BUNDLE),
								invoiceId);	
						//SHR CR708 Rel8.1.1- modified to include InvoiceType
						createOutgoingQueueAndIssueSuccess(newStatus, invoiceOid,invoiceSummaryData.getAttribute("invoice_type"), isCreditNote, mediatorServices);

					}
				}	   
			}
		}       
		catch(Exception e)
		{
			LOG.info("General exception caught in UploadInvoiceMediator:authorizeInvoice");
			e.printStackTrace();
		}
	 }  

	 return (new DocumentHandler());
     }

   
    private void createOutgoingQueueAndIssueSuccess(String status, String invoiceOid,String invType, boolean isCreditNote, MediatorServices mediatorServices)
	throws AmsException, RemoteException {
    	OutgoingInterfaceQueue outgoingMessage = null;
   		String processParameters = null;
   		String messageId = InstrumentServices.getNewMessageID();
   	    outgoingMessage = (OutgoingInterfaceQueue) mediatorServices.createServerEJB("OutgoingInterfaceQueue");
   	    outgoingMessage.newObject();	       		
  		outgoingMessage.setAttribute("message_id", messageId);
   	    outgoingMessage.setAttribute("date_created", DateTimeUtility.getGMTDateTime());
   	    outgoingMessage.setAttribute("status", TradePortalConstants.OUTGOING_STATUS_STARTED);
   	    //SHR CR708 Rel8.1.1- modified to include InvoiceType
		processParameters = "invoice_oid=" + invoiceOid + "|" + "status="+status + "|invoiceType="+invType;
   	    if (isCreditNote)
   	    {
   	      outgoingMessage.setAttribute("msg_type", MessageType.TRIG);   	      
   	    }
   	    else
   	    {
   	     outgoingMessage.setAttribute("msg_type", MessageType.BULKINVUPL);
   	    }
   	    outgoingMessage.setAttribute("process_parameters", processParameters);	       	    

		if (outgoingMessage.save() != 1) {
			throw new AmsException("Error occurred while inserting invoice message type");
		}
	}
	/**
	 * This method implements the Remove Financing functionality. It restores
	 * the invoice status back to what it was before financing was applied.
	 * (CR-710 Rel 8.0 RKAZI)
	 * 
	 * @param inputDoc
	 * @param mediatorServices
	 * @return
	 * @throws RemoteException
	 * @throws AmsException
	 */
	private DocumentHandler removeFinancing(DocumentHandler inputDoc, MediatorServices mediatorServices) 
	throws RemoteException, AmsException {

		mediatorServices.debug("UploadInvoiceMediatorBean.removeFinancing: Start: ");
		String buttonPressed = inputDoc.getAttribute("/Update/ButtonPressed");
		String userOid = inputDoc.getAttribute("/UploadInvoiceList/userOid");
		String securityRights = inputDoc.getAttribute("/UploadInvoiceList/securityRights");
		if ( !SecurityAccess.hasRights(securityRights, SecurityAccess.RECEIVABLES_REMOVE_INVOICE)) {
			// user does not have appropriate authority.
			mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.NO_ACTION_MATCH_NOTICE_AUTH,
					mediatorServices.getResourceManager().getText(
							"UploadInvoiceAction.RemoveFinancing",
							TradePortalConstants.TEXT_BUNDLE));
			return new DocumentHandler();
		}
		int count = 0;
		Vector invoiceList = inputDoc.getFragments("/UploadInvoiceList/InvoicesSummaryData");
		count = invoiceList.size();
		mediatorServices.debug("UploadInvoiceMediatorBean: number of invoices selected: " + count);
		if (count <= 0) 
		{
			mediatorServices.getErrorManager().issueError(
					                   TradePortalConstants.ERR_CAT_1,
					                   TradePortalConstants.NO_ITEM_SELECTED,
					                   mediatorServices.getResourceManager().getText("UploadInvoiceAction.RemoveFinancing",TradePortalConstants.TEXT_BUNDLE));
			return new DocumentHandler();
		}
		DocumentHandler invoiceItem = null;
		InvoicesSummaryData invoiceSummaryData = null;
		String invoiceOid = null;
		String optLock = null;//BSL CR710 02/06/2012 Rel8.0 - implement optimistic locking
		// Loop through the list of invoice oids, and process each one in the
		// list
		for (int i = 0; i < count; i++) 
		{
			invoiceItem = (DocumentHandler) invoiceList.get(i);
			String[] invoiceData = invoiceItem.getAttribute("/InvoiceData").split("/");
			invoiceOid = invoiceData[0];
			optLock = invoiceData[1];
			mediatorServices.debug("UploadInvoiceMediatorBean: invoice item oid to be processed: " + invoiceOid);
			invoiceSummaryData = (InvoicesSummaryData) mediatorServices.createServerEJB("InvoicesSummaryData", Long.parseLong(invoiceOid));
			String invoiceStatus = invoiceSummaryData.getAttribute("invoice_status");
			String invoiceId = invoiceSummaryData.getAttribute("invoice_id");
			String invoiceGroupOid  = invoiceSummaryData.getAttribute("invoice_group_oid");
			// only invoice status 'Available for Authorization' or 'Available for financing'invoices can be
			// Removed from Finance.
			//When navigating from List, only invoices with status other than 'Available for Authorization' and 'Partially Authorized' 
			//can be removed from finance
			if (!((TradePortalConstants.UPLOAD_INV_STATUS_AVL_FOR_AUTH).equals(invoiceStatus) ||
					         (TradePortalConstants.UPLOAD_INV_STATUS_AVL_FOR_FIN).equals(invoiceStatus)) 
					         || (TradePortalConstants.BUTTON_INV_REMOVE_FINANCING.equals(buttonPressed) && 
					        	(TradePortalConstants.UPLOAD_INV_STATUS_AVL_FOR_AUTH.equals(invoiceStatus) || TradePortalConstants.UPLOAD_INV_STATUS_PAR_AUTH.equals(invoiceStatus)))) 
			{

				LOG.info ("&&&&& Unable to authorize because of status");
				mediatorServices.getErrorManager().issueError(
						  UploadInvoiceMediator.class.getName(),
						  TradePortalConstants.TRANSACTION_CANNOT_PROCESS,
						  mediatorServices.getResourceManager().getText("UploadInvoices.InvoiceID", TradePortalConstants.TEXT_BUNDLE),
						  invoiceId,
						  ReferenceDataManager.getRefDataMgr().getDescr("UPLOAD_INVOICE_STATUS", invoiceStatus,mediatorServices.getCSDB().getLocaleName()),
						  mediatorServices.getResourceManager().getText("InvoiceSummary.RemoveFinancing", TradePortalConstants.TEXT_BUNDLE));

               continue; // move to next invoice
			}
			try 
				{

					invoiceSummaryData.setAttribute("invoice_finance_amount", null);
					invoiceSummaryData.setAttribute("net_invoice_finance_amount", null);
					invoiceSummaryData.setAttribute("estimated_interest_amount", null);
					invoiceSummaryData.setAttribute("invoice_status", TradePortalConstants.UPLOAD_INV_STATUS_PEND_ACT_UNABLE_TO_FIN);
					// Set the optimistic lock attribute to make sure the invoice hasn't been changed by someone else since it was first retrieved
					invoiceSummaryData.setAttribute("opt_lock", optLock);//BSL CR710 02/03/2012 Rel8.0 ADD
					 String tpName=invoiceSummaryData.getTpRuleName(); 
					invoiceSummaryData.setAttribute("tp_rule_name", tpName);
					invoiceSummaryData.setAttribute("action",  TradePortalConstants.UPLOAD_INV_ACTION_RMV_FINANCING);
			        invoiceSummaryData.setAttribute("user_oid",userOid);
					int success = invoiceSummaryData.save();
					String newGroupOid = invoiceSummaryData.getAttribute("invoice_group_oid");
					if (success == 1) 
					{   
						if(StringFunction.isNotBlank(newGroupOid) && !invoiceGroupOid.equals(newGroupOid)){
							InvoiceUtility.updateAttachmentDetails(newGroupOid,invoiceGroupOid,invoiceOid,mediatorServices);
						}
						// display message...
						mediatorServices.getErrorManager().issueError(
								UploadInvoiceMediator.class.getName(),
								TradePortalConstants.INV_FINANCING_REMOVED,
								mediatorServices.getResourceManager().getText("UploadInvoices.InvoiceID",TradePortalConstants.TEXT_BUNDLE),
								invoiceId);

					 }
				} 
				catch (Exception e) 
				{
					LOG.info("Exception occured in DeletePaymentFileUploadsMediator: "+ e.toString());
					e.printStackTrace();
				}
		}
		mediatorServices.debug("UploadInvoiceMediatorBean.removeFinancing: End: ");
		return new DocumentHandler();
	}

	/**
	 * This method implements the Accept Financing functionality. It calculates
	 * the financing amount, Net Financing amount based on discount/interest
	 * rate. (CR-710 Rel 8.0 RKAZI)
	 * 
	 * @param inputDoc
	 * @param mediatorServices
	 * @return
	 * @throws RemoteException
	 * @throws AmsException
	 */
	private DocumentHandler acceptFinancing(DocumentHandler inputDoc, MediatorServices mediatorServices) 
	throws RemoteException, AmsException {
	    
		mediatorServices.debug("UploadInvoiceMediatorBean.acceptFinancing: Start: ");
		String userOid = inputDoc.getAttribute("/UploadInvoiceList/userOid");
		
		String securityRights = inputDoc.getAttribute("/UploadInvoiceList/securityRights");
		if ( !SecurityAccess.hasRights(securityRights, SecurityAccess.RECEIVABLES_APPROVE_FINANCING)) {
			// user does not have appropriate authority.
			mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.NO_ACTION_MATCH_NOTICE_AUTH,
					mediatorServices.getResourceManager().getText(
							"SecurityProfileDetail.ApproveFinance",
							TradePortalConstants.TEXT_BUNDLE));
			return new DocumentHandler();
		}
		int count = 0;
		Vector invoiceList = inputDoc.getFragments("/UploadInvoiceList/InvoicesSummaryData");
		count = invoiceList.size();

		mediatorServices.debug("UploadInvoiceMediatorBean: number of invoices selected: " + count);
		if (count <= 0) 
		{
			mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.NO_ITEM_SELECTED,
					mediatorServices.getResourceManager().getText("UploadInvoiceAction.UploadInvoiceAcceptFinancingText", TradePortalConstants.TEXT_BUNDLE));
			return new DocumentHandler();
		}

		DocumentHandler invoiceItem = null;
		InvoicesSummaryData invoiceSummaryData = null;
		String invoiceOid = null;
		String optLock = null;
		SimpleDateFormat formatter=new SimpleDateFormat("MM/dd/yyyy");
		// Loop through the list of invoice oids, and processing each one in the list
		for (int i = 0; i < count; i++) 
		{
			invoiceItem = (DocumentHandler) invoiceList.get(i);
			String[] invoiceData = invoiceItem.getAttribute("/InvoiceData").split("/");
			invoiceOid = invoiceData[0];
			optLock = invoiceData[1];
			invoiceSummaryData = (InvoicesSummaryData) mediatorServices.createServerEJB("InvoicesSummaryData", Long.parseLong(invoiceOid));
			String invoiceStatus = invoiceSummaryData.getAttribute("invoice_status");
			String invoiceId = invoiceSummaryData.getAttribute("invoice_id");
			String invoiceGroupOid  = invoiceSummaryData.getAttribute("invoice_group_oid");
			// only invoice status 'Available for Financing' invoices can be
			// approved for financing.
			if (!((TradePortalConstants.UPLOAD_INV_STATUS_AVL_FOR_FIN).equals(invoiceStatus))) 
			{

				LOG.info ("&&&&& Invoice status UPLOAD_INV_STATUS_AVL_FOR_FIN " + invoiceStatus );
				
				
				mediatorServices.getErrorManager().issueError(
						UploadInvoiceMediator.class.getName(),
						TradePortalConstants.TRANSACTION_CANNOT_PROCESS,
						mediatorServices.getResourceManager().getText("UploadInvoices.InvoiceID", TradePortalConstants.TEXT_BUNDLE),
						invoiceId,
						ReferenceDataManager.getRefDataMgr().getDescr("UPLOAD_INVOICE_STATUS", invoiceStatus, mediatorServices.getCSDB().getLocaleName()),
						mediatorServices.getResourceManager().getText("InvoiceSummary.AcceptFinancing", TradePortalConstants.TEXT_BUNDLE));
				continue; // Move on to next Invoice
				
			}
				try 
				{
					String newStatus = TradePortalConstants.UPLOAD_INV_STATUS_AVL_FOR_AUTH;
					ArMatchingRule rule = invoiceSummaryData.getMatchingRule();
					boolean isCompliant = true;
					try {
						InvoiceUtility.calculateNumTenorDays(rule,null,formatter,invoiceSummaryData.getAttribute("due_date"),invoiceSummaryData.getAttribute("payment_date"));// checks compliance

					}
					catch (AmsException e) {
						isCompliant = false;
						IssuedError is = e.getIssuedError();
						if (is != null) {
							String errorCode = is.getErrorCode();
							//BSL IR NNUM040526786 04/05/2012 BEGIN - add Pct Val To Fn Zero and combine three if branches w/identical logic
							if (TradePortalConstants.ERR_UPLOAD_INV_TENOR_DAYS_LESS.equals(errorCode) ||
									TradePortalConstants.ERR_UPLOAD_INV_DUE_PMT_DAYS_DIFF.equals(errorCode) ||
									TradePortalConstants.ERR_UPLOAD_INV_PCT_VAL_TO_FIN_ZERO.equals(errorCode))
							{
								newStatus = TradePortalConstants.UPLOAD_INV_STATUS_PEND_ACT_UNABLE_TO_FIN;
								mediatorServices.getErrorManager().issueError(UploadInvoiceMediator.class.getName(), errorCode, invoiceId);
							}
							//BSL IR NNUM040526786 04/05/2012 END
							else {
								mediatorServices.debug("InvoiceGroupMediatorBean.acceptFinancing: Unexpected error: " + e.toString());
								throw e;
							}
						}
						else {
							mediatorServices.debug("InvoiceGroupMediatorBean.acceptFinancing: Unexpected error: " + e.toString());
							throw e;
						}
					}
					invoiceSummaryData.setAttribute("invoice_status",newStatus);
					// Set the optimistic lock attribute to make sure the invoice hasn't been changed by someone else since it was first retrieved
					invoiceSummaryData.setAttribute("opt_lock", optLock);//BSL CR710 02/03/2012 Rel8.0 ADD
					 String tpName=invoiceSummaryData.getTpRuleName();
					invoiceSummaryData.setAttribute("tp_rule_name", tpName);
					invoiceSummaryData.setAttribute("action",  TradePortalConstants.UPLOAD_INV_ACTION_FIN_APP);
			        invoiceSummaryData.setAttribute("user_oid",userOid);
					int success = invoiceSummaryData.save();
					String newGroupOid = invoiceSummaryData.getAttribute("invoice_group_oid");
					if (success == 1){
					  
					  if(StringFunction.isNotBlank(newGroupOid) && !invoiceGroupOid.equals(newGroupOid)){
						  InvoiceUtility.updateAttachmentDetails(newGroupOid,invoiceGroupOid,invoiceOid,mediatorServices);
					  }

					   if(isCompliant)
						{  
						// display message...
						mediatorServices.getErrorManager().issueError(
								UploadInvoiceMediator.class.getName(),
								TradePortalConstants.INV_FINANCING_ACCEPTED,
								mediatorServices.getResourceManager().getText("UploadInvoices.InvoiceID", TradePortalConstants.TEXT_BUNDLE),
								invoiceId);
						}
					 }
				} 
				catch (AmsException ae) 
				{
					LOG.info("Exception occured in DeletePaymentFileUploadsMediator: "+ ae.toString());
					ae.printStackTrace();
				}
		}
		mediatorServices.debug("UploadInvoiceMediatorBean.acceptFinancing: End: ");
		return new DocumentHandler();
	}

	/**
	 * Deletes attached documents marked by the user on a Invoice (CR-710 Rel
	 * 8.0 RKAZI)
	 * 
	 * @return com.amsinc.ecsg.util.DocumentHandler
	 * @param inputDoc
	 *            com.amsinc.ecsg.util.DocumentHandler
	 * @param mediatorServices
	 *            com.amsinc.ecsg.frame.MediatorServices
	 */
	private DocumentHandler deleteAttachedDocuments(DocumentHandler inputDoc, MediatorServices mediatorServices) 
	throws RemoteException, AmsException {
		
		DocumentHandler outputDoc = new DocumentHandler();
		int count = 0;
		Vector invoiceList = inputDoc.getFragments("/UploadInvoiceList/InvoicesSummaryData");
		count = invoiceList.size();
		DocumentHandler invoiceItem = null;
		InvoicesSummaryData invoiceSummaryData = null;
		String invoiceOid = null;

		mediatorServices.debug("UploadInvoiceMediatorBean: number of invoices selected: " + count);
		if (count <= 0) 
		{
			mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.NO_ITEM_SELECTED,
					mediatorServices.getResourceManager().getText("UploadInvoiceAction.UploadInvoiceDeleteDocumentText", TradePortalConstants.TEXT_BUNDLE));
			return inputDoc;
		}

		// Loop through the list of invoice oids, and processing each one in the
		// list
		for (int i = 0; i < count; i++) 
		{
			invoiceItem = (DocumentHandler) invoiceList.get(i);
			String[] invoiceData = invoiceItem.getAttribute("/InvoiceData").split("/");
			invoiceOid = invoiceData[0];
			
			invoiceSummaryData = (InvoicesSummaryData) mediatorServices.createServerEJB("InvoicesSummaryData", Long.parseLong(invoiceOid));
			String invoiceId = invoiceSummaryData.getAttribute("invoice_id");
			String invoiceStatus = invoiceSummaryData.getAttribute("invoice_status");
			
		    // only delete documents from invoices with status other than 'Authorized', 'Financed', and 'Deleted'.
			
			//SHR IR T36000006018 Start
		    if ((TradePortalConstants.INVOICE_TYPE_REC_MGMT.equals(invoiceSummaryData.getAttribute("invoice_classification")) 
					&& !(TradePortalConstants.UPLOAD_INV_STATUS_AVL_FOR_FIN.equals(invoiceStatus)
					|| TradePortalConstants.UPLOAD_INV_STATUS_AVL_FOR_AUTH.equals(invoiceStatus)
					|| TradePortalConstants.UPLOAD_INV_STATUS_PAR_AUTH.equals(invoiceStatus)
					|| TradePortalConstants.UPLOAD_INV_STATUS_AUTH_FAILED.equals(invoiceStatus)
					|| TradePortalConstants.UPLOAD_INV_STATUS_FIN_AUTH_FAILED.equals(invoiceStatus)
					|| TradePortalConstants.UPLOAD_INV_STATUS_FIN_PAR_AUTH.equals(invoiceStatus)
					|| TradePortalConstants.UPLOAD_INV_STATUS_PEND_ACT_NO_INSTR_TYPE.equals(invoiceStatus)
					|| TradePortalConstants.UPLOAD_INV_STATUS_PEND_ACT_UNABLE_TO_FIN.equals(invoiceStatus)
					|| TradePortalConstants.PAYABLE_UPLOAD_INV_STATUS_PENDING_ACTION.equals(invoiceStatus)
					|| TradePortalConstants.UPLOAD_INV_STATUS_PEND_ACT_CREDIT_NOTE.equals(invoiceStatus))) 
					|| (TradePortalConstants.INVOICE_TYPE_PAY_MGMT.equals(invoiceSummaryData.getAttribute("invoice_classification")) 
					&& !(TradePortalConstants.UPLOAD_INV_STATUS_PEND_ACT.equals(invoiceStatus))))
		    {
		   	    mediatorServices.getErrorManager().issueError(
		                UploadInvoiceMediator.class.getName(),
		                TradePortalConstants.ERR_UPLOAD_INV_CANNOT_DELETE_DOC, 
		                mediatorServices.getResourceManager().getText("UploadInvoices.InvoiceID",TradePortalConstants.TEXT_BUNDLE),
		                invoiceId ,
		                ReferenceDataManager.getRefDataMgr().getDescr("UPLOAD_INVOICE_STATUS", invoiceStatus, mediatorServices.getCSDB().getLocaleName()));  

		   	    return outputDoc;
		     }   	
		   
			String imageOid = checkDocumentAlreadyAttached(invoiceOid);
			if(StringFunction.isNotBlank(imageOid))
			{
				Long docId = new Long(imageOid);
				// Save the transaction oid in the mail message oid column. Also
				// set transaction_oid to 0 to further flag that it was a deleted
				// image for a transaction.
				DocumentImage documentImageInstance = (DocumentImage) mediatorServices.createServerEJB("DocumentImage", docId.longValue());
				String docName = documentImageInstance.getAttribute("doc_name");
				documentImageInstance.setAttribute("transaction_oid", "0");
				//documentImageInstance.setAttribute("mail_message_oid", "0");
				if(documentImageInstance.save() == 1){ //Nar IR-MMUM050954397
					
                    // display success message...
					mediatorServices.getErrorManager().issueError(
								TradePortalConstants.ERR_CAT_1,
								TradePortalConstants.DOC_DELETED_SUCCESSFULLY,
								invoiceId, docName);
				}
				// Nar IR-MMUM050954397 if user perform action delete document from invoices and there is no any document attach, then
				// inform user that no document is deleted as it doesn't have any attached document.
			}else{
				mediatorServices.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.DOC_NOT_PRESENT_TO_DEL,
						invoiceId);
			}
		}
				
	   return outputDoc;
	}

/**
 * This method performs to apply payment date to invoices.
 */

	private DocumentHandler applyPaymentDate(DocumentHandler inputDoc, MediatorServices mediatorServices) 
	throws RemoteException, AmsException {
    
		Vector invoiceList = null;
		String invoiceOid = null;
		String optLock = null;//BSL CR710 02/06/2012 Rel8.0 - implement optimistic locking
		DocumentHandler invoiceItem = null;
		int count = 0;
		invoiceList           = inputDoc.getFragments("/UploadInvoiceList/InvoicesSummaryData");
		String userOid        = inputDoc.getAttribute("/UploadInvoiceList/userOid");
		String paymentDate    = inputDoc.getAttribute("/UploadInvoiceList/payment_date");
		String corpOrgOid	  = inputDoc.getAttribute("/UploadInvoiceList/ownerOrg");
		String securityRights = inputDoc.getAttribute("/UploadInvoiceList/securityRights");
		User user = (User) mediatorServices.createServerEJB("User", Long.parseLong(userOid));
		String datePattern = user.getAttribute("datepattern");
		try{
			
		    SimpleDateFormat formatter = new SimpleDateFormat(datePattern); 
			SimpleDateFormat reqformatter = new SimpleDateFormat("MM/dd/yyyy");
		    Date formatPaymentDate   =  formatter.parse(paymentDate);
		    paymentDate = reqformatter.format(formatPaymentDate);
		}catch(Exception e){
			mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.WEBI_INVALID_DATE_ERROR);
			return new DocumentHandler();		
		}
		count = invoiceList.size();
		mediatorServices.debug("InvoiceMediator: number of invoices selected: " + count);
		invoiceItem = (DocumentHandler) invoiceList.get(0);
		String[] invoiceData = invoiceItem.getAttribute("/InvoiceData").split("/");
		invoiceOid = invoiceData[0];
		InvoicesSummaryData invoiceSummaryData = (InvoicesSummaryData) mediatorServices.createServerEJB("InvoicesSummaryData", Long.parseLong(invoiceOid));
		if (count <= 0 || StringFunction.isBlank(invoiceItem.getAttribute("/InvoiceData")))
		{
			mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.NO_ITEM_SELECTED,
					mediatorServices.getResourceManager().getText("UploadInvoiceAction.ApplyPaymentDate", TradePortalConstants.TEXT_BUNDLE));
			return inputDoc;
		}
		String invoiceType = invoiceSummaryData.getAttribute("invoice_classification");
		if (("P".equals(invoiceType) && !SecurityAccess.hasRights(securityRights, SecurityAccess.PAYABLES_APPLY_PAYDATE)) || ("R".equals(invoiceType) && !SecurityAccess.hasRights(securityRights, SecurityAccess.RECEIVABLES_APPLY_PAYMENT_DATE))) {
			// issue error that user does not have 'authorize' authority.
			mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.NO_ACTION_INVOICE_AUTH);
			return new DocumentHandler();
		}
	
		//CR914A  start-The same  Due/Payment date and Send To Supplier Date must be present on all invoices in the same E2E group. 
		//If a user selects one or more E2E invoices, but not all of the invoices in the End to End group, the user will get an error message		
		if ("P".equals(invoiceType) && !selectedInvoicesHaveSameEndtoEndId(invoiceList)) {
			mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.INVALID_DATE_FOR_END_TO_END_GROUP);
			return new DocumentHandler();
		}
		 ArMatchingRule matchingRule = null;
		 JPylonProperties jPylonProperties = JPylonProperties.getInstance();
			String serverLocation             = jPylonProperties.getString("serverLocation");
			BeanManager beanMgr = new BeanManager();
			beanMgr.setServerLocation(serverLocation);


		// Loop through the list of invoice oids, and processing each one in the invoice list
	    for (int i = 0; i < count; i++) 
	    {
		  invoiceItem = (DocumentHandler) invoiceList.get(i);
			invoiceData = invoiceItem.getAttribute("/InvoiceData").split("/");
		  invoiceOid = invoiceData[0];
		  optLock = invoiceData[1];
		   try
		   {
				invoiceSummaryData = (InvoicesSummaryData) mediatorServices.createServerEJB("InvoicesSummaryData", Long.parseLong(invoiceOid));

		      String invoiceStatus = invoiceSummaryData.getAttribute("invoice_status");
		      String invoiceId     = invoiceSummaryData.getAttribute("invoice_id");
		      String instType     = invoiceSummaryData.getAttribute("linked_to_instrument_type");
			  String loanType	  =	invoiceSummaryData.getAttribute("loan_type");
			  String invoiceGroupOid  = invoiceSummaryData.getAttribute("invoice_group_oid");
			  loanType = StringFunction.isNotBlank(loanType)? loanType.trim():null;
		      // Payment Date can be assigned only those invoices whose status is 'Available for financing'
		      String isAllowed ="";
		      String rule = "";
		      //Rel9.3 CR 1006 BEGIN
		      String end2EndId = invoiceSummaryData.getAttribute("end_to_end_id");
		      String appReqInd = invoiceSummaryData.getAttribute("portal_approval_requested_ind");
		      
		      if("P".equals(invoiceType) && TradePortalConstants.INDICATOR_YES.equals(appReqInd) &&  StringFunction.isNotBlank(end2EndId)){
		    	 // issue error that Payment Date cannot be modified
			        mediatorServices.getErrorManager().issueError(
			        		TradePortalConstants.ERR_CAT_1,
			                TradePortalConstants.PAYMENT_DATE_CANNOT_BE_MODIFIED);
			        return new DocumentHandler();
		      }
		      //Rel9.3 CR 1006 END
		      if(TradePortalConstants.INV_LINKED_INSTR_PYB.equals(invoiceSummaryData.getAttribute("linked_to_instrument_type"))
		    		   && !InvoiceUtility.isPayableInvoiceUpdatable(invoiceStatus)){
					mediatorServices.getErrorManager().issueError(
							TradePortalConstants.ERR_CAT_1,TradePortalConstants.PAYABLE_MGMT_INVALID_STATUS,invoiceId);
					continue;
			   }
		      if(TradePortalConstants.INVOICE_TYPE_REC_MGMT.equals(invoiceSummaryData.getAttribute("invoice_classification"))){
		     
		    	  if (!(TradePortalConstants.UPLOAD_INV_STATUS_AVL_FOR_FIN.equals(invoiceStatus) || TradePortalConstants.PAYABLE_UPLOAD_INV_STATUS_PENDING_ACTION.equals(invoiceStatus))) {
		    	  LOG.info ("&&&&& UPLOAD_INV_STATUS_AVL_FOR_FIN");
		    	  mediatorServices.getErrorManager().issueError(
		    			  UploadInvoiceMediator.class.getName(),
		    			  TradePortalConstants.INV_INVALID_ACTION_PAY_DATE, 
		    			  mediatorServices.getResourceManager().getText("UploadInvoices.InvoiceID",TradePortalConstants.TEXT_BUNDLE),
		    			  invoiceId ,
		    			  ReferenceDataManager.getRefDataMgr().getDescr("UPLOAD_INVOICE_STATUS", invoiceStatus, mediatorServices.getCSDB().getLocaleName()),
		    			  mediatorServices.getResourceManager().getText("UploadInvoiceAction.ApplyPaymentDate", TradePortalConstants.TEXT_BUNDLE));
		    	  continue; 
			  }
		      matchingRule = invoiceSummaryData.getMatchingRule();
	          isAllowed = matchingRule!=null?matchingRule.getAttribute("payment_day_allow"):"";
		      rule = mediatorServices.getResourceManager().getText("UploadInvoice.TradingPartner", TradePortalConstants.TEXT_BUNDLE);
			
		      }
		      else if(TradePortalConstants.INVOICE_TYPE_PAY_MGMT.equals(invoiceSummaryData.getAttribute("invoice_classification"))){
		    	  rule = mediatorServices.getResourceManager().getText("UploadInvoice.CorpCust", TradePortalConstants.TEXT_BUNDLE);
		      }
		      // Ensure the trading partner rule allows Payment Date
		      if (TradePortalConstants.REC.equals(instType) && TradePortalConstants.BANK_DEF_PAYMENT_ALLOW_NO.equals(isAllowed)) {
		    	  mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
		    			  TradePortalConstants.PARTNER_RULES_RESTRICT_PAYMENT_DATE,rule);

		    	  return new DocumentHandler();
		      }
			   LOG.debug("instType->:"+instType+"\t loanType:"+loanType);
		      if(InstrumentType.LOAN_RQST.equals(instType) && TradePortalConstants.TRADE_LOAN.equals(loanType)){
					CorporateOrganization corpObj = (CorporateOrganization)mediatorServices.createServerEJB("CorporateOrganization",
							Long.parseLong(invoiceSummaryData.getAttribute("corp_org_oid")));
					
						if(TradePortalConstants.BANK_DEF_PAYMENT_ALLOW_NO.equals(corpObj.getAttribute("use_payment_date_allowed_ind"))){
							mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
									TradePortalConstants.TRADE_LOAN_RULES_RESTRICT_PAYMENT_DATE);

							return new DocumentHandler();
					} 
						
			}
              // Nar IR-MJUM050951373 Begin- payment data should not be less than issue date
		      if ((DateTimeUtility.convertStringDateToDate(paymentDate)).before(DateTimeUtility.convertStringDateToDate(invoiceSummaryData.getAttribute("issue_date")))) 
		      {
					mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.INVALID_INV_PAYMENT_DATE);
					continue; // Move on to next Invoice
			  }
              // Nar IR-MJUM050951373 End 
		      String oldPaymentDate = invoiceSummaryData.getAttribute("payment_date");
		      invoiceSummaryData.setAttribute("payment_date", paymentDate);
			  boolean isCompliant = true;
			  if(TradePortalConstants.INVOICE_TYPE_REC_MGMT.equals(invoiceSummaryData.getAttribute("invoice_classification"))){
			  try {
					  if(TradePortalConstants.REC.equals(instType)){ 
						    LOG.debug("old calcualtaion");
							invoiceSummaryData.calculateInterestDiscount(beanMgr);
					  }
					  else if(InstrumentType.LOAN_RQST.equals(instType) && TradePortalConstants.TRADE_LOAN.equals(loanType)){
							LOG.debug("corpOrgOid:"+corpOrgOid);
					  }
					
				}
				catch (AmsException e) {
					isCompliant = false;
					IssuedError is = e.getIssuedError();
					if (is != null) {
						String errorCode = is.getErrorCode();
						//BSL IR NNUM040526786 04/05/2012 BEGIN - add Pct Val To Fn Zero and combine three if branches w/identical logic
						if (TradePortalConstants.ERR_UPLOAD_INV_TENOR_DAYS_LESS.equals(errorCode) ||
								TradePortalConstants.ERR_UPLOAD_INV_DUE_PMT_DAYS_DIFF.equals(errorCode) ||
								TradePortalConstants.ERR_TRADE_INV_DUE_PMT_DAYS_DIFF.equals(errorCode) ||
								TradePortalConstants.ERR_UPLOAD_INV_PCT_VAL_TO_FIN_ZERO.equals(errorCode))
						{
							mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
									errorCode, invoiceId);
							
							// Reset payment date to its former value
							invoiceSummaryData.setAttribute("payment_date",
									oldPaymentDate);
						}
						//BSL IR NNUM040526786 04/05/2012 END
						else {
							mediatorServices.debug("InvoiceGroupMediatorBean.applyPaymentDate: Unexpected error: " + e.toString());
							throw e;
						}
					}
					else {
						mediatorServices.debug("InvoiceGroupMediatorBean.applyPaymentDate: Unexpected error: " + e.toString());
						throw e;
					}
				}}
		        // Set the optimistic lock attribute to make sure the invoice hasn't been changed by someone else since it was first retrieved
		        invoiceSummaryData.setAttribute("opt_lock", optLock);//BSL CR710 02/03/2012 Rel8.0 ADD
		        String tpName=invoiceSummaryData.getTpRuleName();
		        invoiceSummaryData.setAttribute("tp_rule_name", tpName);
		        invoiceSummaryData.setAttribute("action",  TradePortalConstants.UPLOAD_INV_ACTION_APPLY_PAYMENT_DATE);
		        invoiceSummaryData.setAttribute("user_oid",userOid);
				int saveSuccess = invoiceSummaryData.save();
				String newGroupOid = invoiceSummaryData.getAttribute("invoice_group_oid");
				if(saveSuccess == 1 && isCompliant)
				  {
					if(StringFunction.isNotBlank(newGroupOid) && !invoiceGroupOid.equals(newGroupOid)){
						InvoiceUtility.updateAttachmentDetails(newGroupOid,invoiceGroupOid,invoiceOid,mediatorServices);
					}
							// display success message...
								mediatorServices.getErrorManager().issueError(InvoiceGroupMediator.class.getName(),
										TradePortalConstants.INV_ASSIGN_PAYMENT_DATE,
										mediatorServices.getResourceManager().getText("UploadInvoices.Invoice", TradePortalConstants.TEXT_BUNDLE),
										invoiceId);
				   }	
		   }
		   catch(Exception e)
		   {		   
			   LOG.info("General exception caught in UploadInvoiceMediator:applyPaymentInvoice");
			   e.printStackTrace();
		   }
		   
	    }	
	     
	   return (new DocumentHandler());
	}
	 
	/**
	 * Deletes attached documents marked by the user on a Invoice (CR-710 Rel
	 * 8.0 RKAZI)
	 * 
	 * @return com.amsinc.ecsg.util.DocumentHandler
	 * @param inputDoc
	 *            com.amsinc.ecsg.util.DocumentHandler
	 * @param mediatorServices
	 *            com.amsinc.ecsg.frame.MediatorServices
	 */
	private DocumentHandler attachDocument(DocumentHandler inputDoc, MediatorServices mediatorServices) 
	throws RemoteException, AmsException {

		DocumentHandler outputDoc = new DocumentHandler();
		int count = 0;
		Vector invoiceList = inputDoc.getFragments("/UploadInvoiceList/InvoicesSummaryData");
		count = invoiceList.size();
		DocumentHandler invoiceItem = null;
		InvoicesSummaryData invoiceSummaryData = null;
		String invoiceOid = null;
		mediatorServices.debug("UploadInvoiceMediatorBean: number of invoices selected: " + count);
		if (count <= 0) 
		{
			mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.NO_ITEM_SELECTED,
					mediatorServices.getResourceManager().getText("UploadInvoiceAction.UploadInvoiceAttachDocumentText", TradePortalConstants.TEXT_BUNDLE));
		return inputDoc;
		}

		// Loop through the list of invoice oids, and processing each one in the
		// list
		for (int i = 0; i < count; i++) 
		{
			invoiceItem = (DocumentHandler) invoiceList.get(i);
			String[] invoiceData = invoiceItem.getAttribute("/InvoiceData").split("/");
			invoiceOid = invoiceData[0];
			invoiceSummaryData = (InvoicesSummaryData) mediatorServices.createServerEJB("InvoicesSummaryData", Long.parseLong(invoiceOid));
			String invoiceId = invoiceSummaryData.getAttribute("invoice_id");
			String invoiceStatus = invoiceSummaryData.getAttribute("invoice_status");
			
		    // only attach documents to invoices with status other than 'Authorized', 'Financed', and 'Deleted'.
			if ((TradePortalConstants.INVOICE_TYPE_REC_MGMT.equals(invoiceSummaryData.getAttribute("invoice_classification"))
				&& !(TradePortalConstants.UPLOAD_INV_STATUS_AVL_FOR_FIN.equals(invoiceStatus)
					|| TradePortalConstants.UPLOAD_INV_STATUS_AVL_FOR_AUTH.equals(invoiceStatus)
					|| TradePortalConstants.UPLOAD_INV_STATUS_PAR_AUTH.equals(invoiceStatus)
					|| TradePortalConstants.UPLOAD_INV_STATUS_AUTH_FAILED.equals(invoiceStatus)
					|| TradePortalConstants.UPLOAD_INV_STATUS_FIN_AUTH_FAILED.equals(invoiceStatus)
					|| TradePortalConstants.UPLOAD_INV_STATUS_FIN_PAR_AUTH.equals(invoiceStatus)
					|| TradePortalConstants.UPLOAD_INV_STATUS_PEND_ACT_NO_INSTR_TYPE.equals(invoiceStatus)
					|| TradePortalConstants.UPLOAD_INV_STATUS_PEND_ACT_UNABLE_TO_FIN.equals(invoiceStatus)
					|| TradePortalConstants.PAYABLE_UPLOAD_INV_STATUS_PENDING_ACTION.equals(invoiceStatus)
					|| TradePortalConstants.UPLOAD_INV_STATUS_PEND_ACT_CREDIT_NOTE.equals(invoiceStatus)))
					|| (TradePortalConstants.INVOICE_TYPE_PAY_MGMT.equals(invoiceSummaryData.getAttribute("invoice_classification")) 
					&& !(TradePortalConstants.UPLOAD_INV_STATUS_PEND_ACT.equals(invoiceStatus)
						|| TradePortalConstants.PAYABLE_UPLOAD_INV_STATUS_ASSIGNED.equals(invoiceStatus)
						|| TradePortalConstants.PAYABLE_UPLOAD_INV_STATUS_UNASSIGNED.equals(invoiceStatus))))
		    {
		        
		   	    mediatorServices.getErrorManager().issueError(
		                UploadInvoiceMediator.class.getName(),
		                TradePortalConstants.ERR_UPLOAD_INV_CANNOT_ATTACH_DOC, 
		                mediatorServices.getResourceManager().getText("UploadInvoices.InvoiceID",TradePortalConstants.TEXT_BUNDLE),
		                invoiceId ,
		                ReferenceDataManager.getRefDataMgr().getDescr("UPLOAD_INVOICE_STATUS", invoiceStatus, mediatorServices.getCSDB().getLocaleName()));  
		        // return true to NOT set the transaction status to failed
		     }   

			
			//if Document already attached then throw error.
			String imageOid = checkDocumentAlreadyAttached(invoiceOid);
			if (StringFunction.isNotBlank(imageOid))
			{
				mediatorServices.getErrorManager().issueError(
						UploadInvoiceMediator.class.getName(),
						TradePortalConstants.ERR_UPLOAD_INV_DOC_ALREADY_ATTACHED,
						invoiceId);
			}
		}		
		
	  return outputDoc;
	}
	
	private String checkDocumentAlreadyAttached(String invoiceOid) 
	throws AmsException{
		String sql = "select DOC_IMAGE_OID from DOCUMENT_IMAGE where p_transaction_oid = ?";
		DocumentHandler	imagesListDoc = DatabaseQueryBean.getXmlResultSet(sql, true, new Object[]{invoiceOid});
		if (imagesListDoc != null)
		{
			return imagesListDoc.getAttribute("/ResultSetRecord(0)/DOC_IMAGE_OID"); 
		}
	return null;
	}


	/**
	 * Close invoices ( NAR IR- MAUM051043378 )
	 * 
	 * @return com.amsinc.ecsg.util.DocumentHandler
	 * @param inputDoc
	 *            com.amsinc.ecsg.util.DocumentHandler
	 * @param mediatorServices
	 *            com.amsinc.ecsg.frame.MediatorServices
	 */
	private DocumentHandler closeInvoice(DocumentHandler inputDoc, MediatorServices mediatorServices) 
	throws RemoteException, AmsException {

		DocumentHandler outputDoc = new DocumentHandler();
		int count = 0;
		String optLock = null;
		Vector invoiceList = inputDoc.getFragments("/UploadInvoiceList/InvoicesSummaryData");
		String userOid        = inputDoc.getAttribute("/UploadInvoiceList/userOid");
		count = invoiceList.size();
		DocumentHandler invoiceItem = null;
		InvoicesSummaryData invoiceSummaryData = null;
		String invoiceOid = null;

		mediatorServices.debug("UploadInvoiceMediatorBean: number of invoices selected: " + count);
		if (count <= 0) 
		{
			mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.NO_ITEM_SELECTED,
					mediatorServices.getResourceManager().getText("UploadInvoiceAction.CloseInvoices", TradePortalConstants.TEXT_BUNDLE));
			return new DocumentHandler();
		}

		// Loop through the list of invoice oids, and processing each one in the
		// list
		for (int i = 0; i < count; i++) 
		{
			invoiceItem = (DocumentHandler) invoiceList.get(i);
			String[] invoiceData = invoiceItem.getAttribute("/InvoiceData").split("/");
			invoiceOid = invoiceData[0];
			optLock = invoiceData[1];
			
			invoiceSummaryData = (InvoicesSummaryData) mediatorServices.createServerEJB("InvoicesSummaryData", Long.parseLong(invoiceOid));
			String invoiceId = invoiceSummaryData.getAttribute("invoice_id");
			String invoiceStatus = invoiceSummaryData.getAttribute("invoice_status");
			String invoiceGroupOid  = invoiceSummaryData.getAttribute("invoice_group_oid");
		    // close invoices only for status 'Unable to Finance'.			
			if (!(TradePortalConstants.UPLOAD_INV_STATUS_PEND_ACT_UNABLE_TO_FIN.equals(invoiceStatus) || TradePortalConstants.UPLOAD_INV_STATUS_DBC.equals(invoiceStatus) ))
		    {
				mediatorServices.getErrorManager().issueError(
						UploadInvoiceMediator.class.getName(),
						TradePortalConstants.TRANSACTION_CANNOT_PROCESS,
						mediatorServices.getResourceManager().getText("UploadInvoices.InvoiceID", TradePortalConstants.TEXT_BUNDLE),
						invoiceId,
						ReferenceDataManager.getRefDataMgr().getDescr("UPLOAD_INVOICE_STATUS", invoiceStatus, mediatorServices.getCSDB().getLocaleName()),
						mediatorServices.getResourceManager().getText("TransactionAction.Closed", TradePortalConstants.TEXT_BUNDLE));  

				continue; // Move on to next Invoice
		     }   			

	           invoiceSummaryData.setAttribute("invoice_status", TradePortalConstants.UPLOAD_INV_STATUS_CLOSED);
	           // Set the optimistic lock attribute to make sure the invoice hasn't been changed by someone else since it was first retrieved
	           invoiceSummaryData.setAttribute("opt_lock", optLock);
	           invoiceSummaryData.setAttribute("action",  TradePortalConstants.UPLOAD_INV_ACTION_CLOSE);
		        invoiceSummaryData.setAttribute("user_oid",userOid);
			   int saveSuccess = invoiceSummaryData.save();
			   String newGroupOid = invoiceSummaryData.getAttribute("invoice_group_oid");
			   if(saveSuccess == 1){

				 if(StringFunction.isNotBlank(newGroupOid) && !invoiceGroupOid.equals(newGroupOid)){
					 InvoiceUtility.updateAttachmentDetails(newGroupOid,invoiceGroupOid,invoiceOid,mediatorServices);
				 }
				     //SHR CR708 Rel8.1.1- modified to include InvoiceType
				     String invType = invoiceSummaryData.getAttribute("invoice_type");
					 createOutgoingQueueAndIssueSuccess(TradePortalConstants.UPLOAD_INV_STATUS_CLOSED, invoiceOid,invType, false, mediatorServices);
                    						
						// display success message...
					  mediatorServices.getErrorManager().issueError(
								TradePortalConstants.ERR_CAT_1,
								TradePortalConstants.CLOSE_SUCCESSFUL,
								invoiceId);
			   }
		}
				
	   return outputDoc;
	}
	
	/**
	 * Attaches documents marked by the user on a Invoice (CR-708 Rel
	 * 8.1 )
	 * 
	 * @return com.amsinc.ecsg.util.DocumentHandler
	 * @param inputDoc
	 *            com.amsinc.ecsg.util.DocumentHandler
	 * @param mediatorServices
	 *            com.amsinc.ecsg.frame.MediatorServices
	 */
	private DocumentHandler attachDocumentforPayableInvoice(DocumentHandler inputDoc, MediatorServices mediatorServices) 
			throws RemoteException, AmsException {
				
				DocumentHandler outputDoc = new DocumentHandler();
				int count = 0;
				Vector invoiceList = inputDoc.getFragments("/UploadInvoiceList/InvoicesSummaryData");
				count = invoiceList.size();
				DocumentHandler invoiceItem = null;
				InvoicesSummaryData invoiceSummaryData = null;
				String invoiceOid = null;
				mediatorServices.debug("UploadInvoiceMediatorBean: number of invoices selected: " + count);
				if (count <= 0) 
				{
					mediatorServices.getErrorManager().issueError(
							TradePortalConstants.ERR_CAT_1,
							TradePortalConstants.NO_ITEM_SELECTED,
							mediatorServices.getResourceManager().getText("UploadInvoiceAction.UploadInvoiceAttachDocumentText", TradePortalConstants.TEXT_BUNDLE));
				}			
					// Loop through the list of invoice oids, and processing each one in the
					// list	
				for (int i = 0; i < count; i++) 
				{
					invoiceItem = (DocumentHandler) invoiceList.get(i);
					String[] invoiceData = invoiceItem.getAttribute("/InvoiceData").split("/");
					invoiceOid = invoiceData[0];
					invoiceSummaryData = (InvoicesSummaryData) mediatorServices.createServerEJB("InvoicesSummaryData", Long.parseLong(invoiceOid));
					String invoiceId = invoiceSummaryData.getAttribute("invoice_id");
					String invoiceStatus = invoiceSummaryData.getAttribute("invoice_status");
					
				    // only attach documents to invoices with status other than 'Authorized', 'Financed', and 'Deleted'.			
					if ((TradePortalConstants.INVOICE_TYPE_REC_MGMT.equals(invoiceSummaryData.getAttribute("invoice_classification"))
						&& !(TradePortalConstants.UPLOAD_INV_STATUS_AVL_FOR_FIN.equals(invoiceStatus)
							|| TradePortalConstants.UPLOAD_INV_STATUS_AVL_FOR_AUTH.equals(invoiceStatus)
							|| TradePortalConstants.UPLOAD_INV_STATUS_PAR_AUTH.equals(invoiceStatus)
							|| TradePortalConstants.UPLOAD_INV_STATUS_AUTH_FAILED.equals(invoiceStatus)
							|| TradePortalConstants.UPLOAD_INV_STATUS_FIN_AUTH_FAILED.equals(invoiceStatus)
							|| TradePortalConstants.UPLOAD_INV_STATUS_FIN_PAR_AUTH.equals(invoiceStatus)
							|| TradePortalConstants.UPLOAD_INV_STATUS_PEND_ACT_NO_INSTR_TYPE.equals(invoiceStatus)
							|| TradePortalConstants.UPLOAD_INV_STATUS_PEND_ACT_UNABLE_TO_FIN.equals(invoiceStatus)
							|| TradePortalConstants.PAYABLE_UPLOAD_INV_STATUS_PENDING_ACTION.equals(invoiceStatus)
							|| TradePortalConstants.UPLOAD_INV_STATUS_PEND_ACT_CREDIT_NOTE.equals(invoiceStatus)))
							|| (TradePortalConstants.INVOICE_TYPE_PAY_MGMT.equals(invoiceSummaryData.getAttribute("invoice_classification")) 
							&& !(TradePortalConstants.UPLOAD_INV_STATUS_PEND_ACT.equals(invoiceStatus)
								|| TradePortalConstants.PAYABLE_UPLOAD_INV_STATUS_ASSIGNED.equals(invoiceStatus)
								|| TradePortalConstants.UPLOAD_INV_STATUS_AVL_FOR_AUTH.equals(invoiceStatus)
								|| TradePortalConstants.PAYABLE_UPLOAD_INV_STATUS_UNASSIGNED.equals(invoiceStatus))))
				
				    {
				        
				   	    mediatorServices.getErrorManager().issueError(
				                UploadInvoiceMediator.class.getName(),
				                TradePortalConstants.ERR_UPLOAD_INV_CANNOT_ATTACH_DOC, 
				                mediatorServices.getResourceManager().getText("UploadInvoices.InvoiceID",TradePortalConstants.TEXT_BUNDLE),
				                invoiceId ,
				                ReferenceDataManager.getRefDataMgr().getDescr("UPLOAD_INVOICE_STATUS", invoiceStatus, mediatorServices.getCSDB().getLocaleName()));  
				        // return true to NOT set the transaction status to failed
				     }   

					
					//if Document already attached then throw error.
					String imageOid = checkDocumentAlreadyAttached(invoiceOid);
					if (StringFunction.isNotBlank(imageOid))
					{
						mediatorServices.getErrorManager().issueError(
								UploadInvoiceMediator.class.getName(),
								TradePortalConstants.ERR_UPLOAD_INV_DOC_ALREADY_ATTACHED,
								invoiceId);
					}
				}		
									
			  return outputDoc;
			}
	
	
	/**
	 * Deletes attached documents marked by the user on a Invoice (708)
	 * 
	 * @return com.amsinc.ecsg.util.DocumentHandler
	 * @param inputDoc
	 *            com.amsinc.ecsg.util.DocumentHandler
	 * @param mediatorServices
	 *            com.amsinc.ecsg.frame.MediatorServices
	 */
	private DocumentHandler deleteAttachedDocForPayableInvoice(DocumentHandler inputDoc, MediatorServices mediatorServices) 
	throws RemoteException, AmsException {
		
		DocumentHandler outputDoc = new DocumentHandler();
		int count = 0;
		Vector invoiceList = inputDoc.getFragments("/UploadInvoiceList/InvoicesSummaryData");
		count = invoiceList.size();
		DocumentHandler invoiceItem = null;
		InvoicesSummaryData invoiceSummaryData = null;
		String invoiceOid = null;
		String invGroupOid = null;
		boolean removeGroup =false;
		mediatorServices.debug("UploadInvoiceMediatorBean: number of invoices selected: " + count);
		LOG.debug("invoiceList:"+(invoiceList == null || invoiceList.size() == 0));
		if (invoiceList == null || invoiceList.size() == 0) {
				mediatorServices.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.NO_ITEM_SELECTED,
						mediatorServices.getResourceManager().getText(
								"UploadInvoiceAction.UploadInvoiceDeleteDocumentText",
								TradePortalConstants.TEXT_BUNDLE));
				return new DocumentHandler();
			}
		invoiceItem = (DocumentHandler) invoiceList.get(0);
		String[] invoiceData = invoiceItem.getAttribute("/InvoiceData").split("/");
		invoiceOid = invoiceData[0];
		invoiceSummaryData = (InvoicesSummaryData) mediatorServices.createServerEJB("InvoicesSummaryData", Long.parseLong(invoiceOid));
		String invoiceId = invoiceSummaryData.getAttribute("invoice_id");
		invGroupOid =  invoiceSummaryData.getAttribute("invoice_group_oid");
		
		int	invCount = DatabaseQueryBean.getCount("UPLOAD_INVOICE_OID", "INVOICES_SUMMARY_DATA", "A_INVOICE_GROUP_OID = ?", false, new Object[]{invGroupOid});		
		
		int	imagesCount = DatabaseQueryBean.getCount("DOC_IMAGE_OID", "DOCUMENT_IMAGE", "P_TRANSACTION_OID in " +
				" (select upload_invoice_oid from invoices_summary_data where  a_invoice_group_oid = ?)", false, new Object[]{invGroupOid});

		
		if((count == invCount) || (imagesCount == 1))
			removeGroup = true;
		
		for (int i = 0; i < count; i++) 
		{
			invoiceItem = (DocumentHandler) invoiceList.get(i);
			invoiceData = invoiceItem.getAttribute("/InvoiceData").split("/");
			invoiceOid = invoiceData[0];
			//invoices[i]=invoiceOid;
			invoiceSummaryData = (InvoicesSummaryData) mediatorServices.createServerEJB("InvoicesSummaryData", Long.parseLong(invoiceOid));
			invoiceId = invoiceSummaryData.getAttribute("invoice_id");
			String invoiceStatus = invoiceSummaryData.getAttribute("invoice_status");
			invGroupOid =  invoiceSummaryData.getAttribute("invoice_group_oid");
			
		    // only delete documents from invoices with status other than 'Authorized', 'Financed', and 'Deleted'.
			
		    if ((TradePortalConstants.INVOICE_TYPE_REC_MGMT.equals(invoiceSummaryData.getAttribute("invoice_classification")) 
					&& !(TradePortalConstants.UPLOAD_INV_STATUS_AVL_FOR_FIN.equals(invoiceStatus)
					|| TradePortalConstants.UPLOAD_INV_STATUS_AVL_FOR_AUTH.equals(invoiceStatus)
					|| TradePortalConstants.UPLOAD_INV_STATUS_PAR_AUTH.equals(invoiceStatus)
					|| TradePortalConstants.UPLOAD_INV_STATUS_AUTH_FAILED.equals(invoiceStatus)
					|| TradePortalConstants.UPLOAD_INV_STATUS_FIN_AUTH_FAILED.equals(invoiceStatus)
					|| TradePortalConstants.UPLOAD_INV_STATUS_FIN_PAR_AUTH.equals(invoiceStatus)
					|| TradePortalConstants.UPLOAD_INV_STATUS_PEND_ACT_NO_INSTR_TYPE.equals(invoiceStatus)
					|| TradePortalConstants.UPLOAD_INV_STATUS_PEND_ACT_UNABLE_TO_FIN.equals(invoiceStatus)
				    || TradePortalConstants.PAYABLE_UPLOAD_INV_STATUS_PENDING_ACTION.equals(invoiceStatus)
					|| TradePortalConstants.UPLOAD_INV_STATUS_PEND_ACT_CREDIT_NOTE.equals(invoiceStatus))) 
					|| (TradePortalConstants.INVOICE_TYPE_PAY_MGMT.equals(invoiceSummaryData.getAttribute("invoice_classification")) 
					&& !(TradePortalConstants.UPLOAD_INV_STATUS_PEND_ACT.equals(invoiceStatus)
					|| TradePortalConstants.PAYABLE_UPLOAD_INV_STATUS_ASSIGNED.equals(invoiceStatus)
					|| TradePortalConstants.UPLOAD_INV_STATUS_AVL_FOR_AUTH.equals(invoiceStatus)
					|| TradePortalConstants.PAYABLE_UPLOAD_INV_STATUS_UNASSIGNED.equals(invoiceStatus))))
		    {
		   	    mediatorServices.getErrorManager().issueError(
		                UploadInvoiceMediator.class.getName(),
		                TradePortalConstants.ERR_UPLOAD_INV_CANNOT_DELETE_DOC, 
		                mediatorServices.getResourceManager().getText("UploadInvoices.InvoiceID",TradePortalConstants.TEXT_BUNDLE),
		                invoiceId ,
		                ReferenceDataManager.getRefDataMgr().getDescr("UPLOAD_INVOICE_STATUS", invoiceStatus, mediatorServices.getCSDB().getLocaleName()));  

		   	    return outputDoc;
		     }   	
		   
			String imageOid = checkDocumentAlreadyAttached(invoiceOid);
			if(StringFunction.isNotBlank(imageOid))
			{
				Long docId = new Long(imageOid);
				// Save the transaction oid in the mail message oid column. Also
				// set transaction_oid to 0 to further flag that it was a deleted
				// image for a transaction.
				DocumentImage documentImageInstance = (DocumentImage) mediatorServices.createServerEJB("DocumentImage", docId.longValue());
				String docName = documentImageInstance.getAttribute("doc_name");
				documentImageInstance.setAttribute("transaction_oid", "0");
				if(documentImageInstance.save() == 1){ //Nar IR-MMUM050954397
					updateAttachmentStatus(invGroupOid,mediatorServices);
                    // display success message...
					mediatorServices.getErrorManager().issueError(
								TradePortalConstants.ERR_CAT_1,
								TradePortalConstants.DOC_DELETED_SUCCESSFULLY,
								invoiceId, docName);
				}
				// Nar IR-MMUM050954397 if user perform action delete document from invoices and there is no any document attach, then
				// inform user that no document is deleted as it doesn't have any attached document.
			}else{
				mediatorServices.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.DOC_NOT_PRESENT_TO_DEL,
						invoiceId);
			}
		}
		
		
		if(removeGroup){
			
			String imageOid = checkDocumentAlreadyAttached(invGroupOid);
			
			if(StringFunction.isNotBlank(imageOid))
			{
				Long docId = new Long(imageOid);
				// Save the transaction oid in the mail message oid column. Also
				// set transaction_oid to 0 to further flag that it was a deleted
				// image for a transaction.
				DocumentImage documentImageInstance = (DocumentImage) mediatorServices.createServerEJB("DocumentImage", docId.longValue());
				documentImageInstance.setAttribute("transaction_oid", "0");
				int status = documentImageInstance.save();
				LOG.info("Removing for group.....");
				updateAttachmentStatus(invGroupOid,mediatorServices);
			}
		}
				
	   return outputDoc;
	}
	
	private int updateAttachmentStatus(String invGroupOid, MediatorServices mediatorServices) 
			throws RemoteException, AmsException {
		LOG.info("updateAttachmentStatus()..invGroupOid:"+invGroupOid);
		String attachmentStatus = "";
		int	imagesCount = DatabaseQueryBean.getCount("DOC_IMAGE_OID", "DOCUMENT_IMAGE", "P_TRANSACTION_OID in " +
				" (select upload_invoice_oid from invoices_summary_data where  a_invoice_group_oid = ?)", false, new Object[]{invGroupOid});
		LOG.info("imagesCount:"+imagesCount);
		
		if(imagesCount>0)
			attachmentStatus = TradePortalConstants.INDICATOR_YES;
		else
			attachmentStatus = TradePortalConstants.INDICATOR_NO;
		
		InvoiceGroup invoiceGroup = (InvoiceGroup) mediatorServices.createServerEJB("InvoiceGroup",
				Long.parseLong(invGroupOid));	
			invoiceGroup.setAttribute("attachment_ind", attachmentStatus);
			int status = invoiceGroup.save();
			LOG.info("Updated status:"+status);			
		
		return status;
	}

	/**
	 * This method clears/removes the assigned Payment date to the selected invoice/invoices
	 * @param inputDoc
	 * @param mediatorServices
	 * @return
	 * @throws RemoteException
	 * @throws AmsException
	 */
	private DocumentHandler clearPaymentDate(DocumentHandler inputDoc, MediatorServices mediatorServices) 
	throws RemoteException, AmsException {
		LOG.debug("clearPaymentDate()...."+inputDoc);
		Vector invoiceList = null;
		String invoiceOid = null;
		String optLock = null; //optimistic locking
		DocumentHandler invoiceItem = null;
		int count = 0;
		invoiceList           = inputDoc.getFragments("/UploadInvoiceList/InvoicesSummaryData");
		String userOid        = inputDoc.getAttribute("/UploadInvoiceList/userOid");
		String buttonPressed =  inputDoc.getAttribute("/Update/ButtonPressed");
		String securityRights = inputDoc.getAttribute("/UploadInvoiceList/securityRights");
		count = invoiceList.size();
		LOG.debug("UploadInvoiceMediatorBean: number of invoices selected : " + count);
		if (count <= 0) 
		{
			mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.NO_ITEM_SELECTED,
					mediatorServices.getResourceManager().getText("UploadInvoiceAction.ClearPaymentDate", TradePortalConstants.TEXT_BUNDLE));
			return new DocumentHandler();
		}

		// Loop through the list of invoice oids, and processing each one in the invoice list
	    for (int i = 0; i < count; i++) 
	    {
		  invoiceItem = (DocumentHandler) invoiceList.get(i);
		  String[] invoiceData = invoiceItem.getAttribute("/InvoiceData").split("/");
		  invoiceOid = invoiceData[0];
		  optLock = invoiceData[1];
		   try
		   {
		      InvoicesSummaryData invoiceSummaryData = (InvoicesSummaryData) mediatorServices.createServerEJB("InvoicesSummaryData", Long.parseLong(invoiceOid));
		      String  invoiceType  = invoiceSummaryData.getAttribute("invoice_classification");
				if (("P".equals(invoiceType) && !SecurityAccess.hasRights(securityRights, SecurityAccess.PAY_CLEAR_PAYMENT_DATE))
						|| ("R".equals(invoiceType) && !SecurityAccess.hasRights(securityRights, SecurityAccess.RECEIVABLES_CLEAR_PAYMENT_DATE))) {
					// issue error that user does not have 'authorize' authority.
					mediatorServices.getErrorManager().issueError(
							TradePortalConstants.ERR_CAT_1,
							TradePortalConstants.NO_ACTION_MATCH_NOTICE_AUTH,
							mediatorServices.getResourceManager().getText(
									"SecurityProfileDetail.PayClearPaymentDate",
									TradePortalConstants.TEXT_BUNDLE));
			        return new DocumentHandler();
				}
		      String invoiceStatus = invoiceSummaryData.getAttribute("invoice_status");
		      String invoiceId     = invoiceSummaryData.getAttribute("invoice_id");
		      String invoiceGroupOid  = invoiceSummaryData.getAttribute("invoice_group_oid");
		     
		      if(TradePortalConstants.INV_LINKED_INSTR_PYB.equals(invoiceSummaryData.getAttribute("linked_to_instrument_type"))
		    		   && !InvoiceUtility.isPayableInvoiceUpdatable(invoiceStatus)){
					mediatorServices.getErrorManager().issueError(
							TradePortalConstants.ERR_CAT_1,TradePortalConstants.PAYABLE_MGMT_INVALID_STATUS,invoiceId);
					continue;
			   }
		      // Clear payment until invoices are not linked to any instrument 
		      if(!TradePortalConstants.INV_LINKED_INSTR_PYB.equals(invoiceSummaryData.getAttribute("linked_to_instrument_type"))
		    		  && ((StringFunction.isNotBlank(invoiceSummaryData.getAttribute("instrument_oid")) || StringFunction.isNotBlank(invoiceSummaryData.getAttribute("transaction_oid")))
		      || (TradePortalConstants.BUTTON_INV_CLEAR_PAYMENT_DATE.equals(buttonPressed) && 
	        	(TradePortalConstants.UPLOAD_INV_STATUS_AVL_FOR_AUTH.equals(invoiceStatus) || TradePortalConstants.UPLOAD_INV_STATUS_PAR_AUTH.equals(invoiceStatus)))))
		      {
		    	  mediatorServices.getErrorManager().issueError(
					      TradePortalConstants.ERR_CAT_1,
		    			  TradePortalConstants.REC_INVOICE_CLEAR_PAYMENT_NOT_ALLOW, 		    			
		    			  invoiceId );
		    	  return new DocumentHandler();
			  }    	     
		   
           
		      String dbPaymentDate = invoiceSummaryData.getAttribute("payment_date");
			  LOG.debug("dbPaymentDate :"+dbPaymentDate);
			  if(StringFunction.isBlank(dbPaymentDate)){

					mediatorServices.getErrorManager().issueError(
									TradePortalConstants.ERR_CAT_1,
									TradePortalConstants.INV_CLEAR_PAYMENT_DATE_NOT_AVAILABLE,
									invoiceId);
					return new DocumentHandler();
			  }
			  String tpName=invoiceSummaryData.getTpRuleName();
			  
			  invoiceSummaryData.setAttribute("tp_rule_name",tpName);
			  invoiceSummaryData.setAttribute("payment_date", null);
			  boolean isCompliant = true;
			  
		        // Set the optimistic lock attribute to make sure the invoice hasn't been changed by someone else since it was first retrieved
		        invoiceSummaryData.setAttribute("opt_lock", optLock);
		        invoiceSummaryData.setAttribute("action",  TradePortalConstants.UPLOAD_INV_ACTION_CLEAR_PAYMENT_DATE);
		        invoiceSummaryData.setAttribute("user_oid",userOid);
				int saveSuccess = invoiceSummaryData.save();
				String newGroupOid = invoiceSummaryData.getAttribute("invoice_group_oid");
				if(saveSuccess == 1 && isCompliant)
				  {
					if(StringFunction.isNotBlank(newGroupOid) && !invoiceGroupOid.equals(newGroupOid)){
						InvoiceUtility.updateAttachmentDetails(newGroupOid,invoiceGroupOid,invoiceOid,mediatorServices);
					}
					 		// display success message...
						mediatorServices.getErrorManager().issueError(
									TradePortalConstants.ERR_CAT_1,
									TradePortalConstants.INV_CLEAR_PAYMENT_DATE,
									invoiceId);
				   }	
		   }
		   catch(Exception e)
		   {		   
			   LOG.info("General exception caught in UploadInvoiceMediatorBean:ClearPaymentDate");
			   e.printStackTrace();
		   }
		   
	    }
	     
	   return (new DocumentHandler());
      } 
	  
	/**
	 * This method assigns the Loan type to the selected Invoices.
	 * @param inputDoc
	 * @param mediatorServices
	 * @return
	 * @throws RemoteException
	 * @throws AmsException
	 */
	  private DocumentHandler assignLoanType(DocumentHandler inputDoc, MediatorServices mediatorServices) 
				throws RemoteException, AmsException {
		LOG.debug("assignLoanType()...."+inputDoc);
		Vector invoiceList = null;
		String invoiceOid = null;
		String optLock = null; //optimistic locking
		DocumentHandler invoiceItem = null;
		int count = 0;
		invoiceList           = inputDoc.getFragments("/UploadInvoiceList/InvoicesSummaryData");
		String userOid        = inputDoc.getAttribute("/UploadInvoiceList/userOid");
		String loanType   =  inputDoc.getAttribute("/UploadInvoiceList/loan_type");
		String corpOrgOid = inputDoc.getAttribute("/UploadInvoiceList/ownerOrg");
		String securityRights = inputDoc.getAttribute("/UploadInvoiceList/securityRights");
		count = invoiceList.size();
		LOG.debug("UploadInvoiceMediatorBean: number of invoices selected : " + count);
		if (count <= 0) 
		{
			mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.NO_ITEM_SELECTED,
					mediatorServices.getResourceManager().getText("UploadInvoiceAction.AssignLoanType", TradePortalConstants.TEXT_BUNDLE));
			return new DocumentHandler();
		}

		// Loop through the list of invoice oids, and processing each one in the invoice list
	    for (int i = 0; i < count; i++) 
	    {
		  invoiceItem = (DocumentHandler) invoiceList.get(i);
		  
		  if(StringFunction.isBlank(invoiceItem.getAttribute("/InvoiceData"))){
			mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.NO_ITEM_SELECTED,
					mediatorServices.getResourceManager().getText("UploadInvoiceAction.AssignLoanType", TradePortalConstants.TEXT_BUNDLE));
			return new DocumentHandler();
		  }
		  String[] invoiceData = invoiceItem.getAttribute("/InvoiceData").split("/");
		  invoiceOid = invoiceData[0];
		  optLock = invoiceData[1];
		   try
		   {
		      InvoicesSummaryData invoiceSummaryData = (InvoicesSummaryData) mediatorServices.createServerEJB("InvoicesSummaryData", Long.parseLong(invoiceOid));
		      String  invoiceType  = invoiceSummaryData.getAttribute("invoice_classification");
				if (("P".equals(invoiceType) && !SecurityAccess.hasRights(securityRights, SecurityAccess.PAY_ASSIGN_LOAN_TYPE))
						|| ("R".equals(invoiceType) && !SecurityAccess.hasRights(securityRights, SecurityAccess.RECEIVABLES_ASSIGN_LOAN_TYPE))) {
					
					mediatorServices.getErrorManager().issueError(
							TradePortalConstants.ERR_CAT_1,
							TradePortalConstants.NO_ACTION_MATCH_NOTICE_AUTH,
							mediatorServices.getResourceManager().getText(
									"SecurityProfileDetail.PayAssignLoanType",
									TradePortalConstants.TEXT_BUNDLE));
			        return new DocumentHandler();
				}
		      String invoiceStatus 		= invoiceSummaryData.getAttribute("invoice_status");
		      String invoiceId     		= invoiceSummaryData.getAttribute("invoice_id");		      
		      String instrumentType     = invoiceSummaryData.getAttribute("linked_to_instrument_type");	
			  String invoiceGroupOid    = invoiceSummaryData.getAttribute("invoice_group_oid");	
			  String amount = invoiceSummaryData.getAttribute("amount");
			  if(StringFunction.isBlank(instrumentType)){

				   mediatorServices.getErrorManager().issueError(
		    			  UploadInvoiceMediator.class.getName(),
		    			  TradePortalConstants.REC_INVOICE_ASSIGN_LOAN_TYPE_NOT_AVAIL,invoiceId);
		    	  return new DocumentHandler();
			  }
			  if(TradePortalConstants.INV_LINKED_INSTR_PYB.equals(invoiceSummaryData.getAttribute("linked_to_instrument_type"))){
					mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
							TradePortalConstants.PAYABLE_MGMT_INVALID_ACTION,mediatorServices.getResourceManager().getText("UploadInvoiceAction.AssignLoanType",
							TradePortalConstants.TEXT_BUNDLE),invoiceId,invoiceSummaryData.getAttribute("linked_to_instrument_type"));
						continue; // Move on to next Invoice
			 }
			 if(TradePortalConstants.RECEIVABLE_UPLOAD_INV_STATUS_ASSIGNED.equals(invoiceStatus)|| TradePortalConstants.PAYABLE_UPLOAD_INV_STATUS_DELETED.equals(invoiceStatus)){
					
					 mediatorServices.getErrorManager().issueError(
		     	               UploadInvoiceMediator.class.getName(),
		     	               TradePortalConstants.LOAN_TYPE_NO_STATUS_CHANGE, 
		     	               mediatorServices.getResourceManager().getText("UploadInvoices.InvoiceID",TradePortalConstants.TEXT_BUNDLE),
		     	               invoiceId ,
		     	               ReferenceDataManager.getRefDataMgr().getDescr("UPLOAD_INVOICE_STATUS", invoiceStatus, mediatorServices.getCSDB().getLocaleName()),
		     	               mediatorServices.getResourceManager().getText("UploadInvoices.AssignLoanType", TradePortalConstants.TEXT_BUNDLE));
			    	  return new DocumentHandler();
				  }
		      if(TradePortalConstants.INV_LINKED_INSTR_REC.equals(instrumentType)){
		      
		    	  mediatorServices.getErrorManager().issueError(
		    			  UploadInvoiceMediator.class.getName(),
		    			  TradePortalConstants.REC_INVOICE_ASSIGN_LOAN_TYPE_NOT_ALLOW, 
		    			  mediatorServices.getResourceManager().getText("UploadInvoices.InvoiceID",TradePortalConstants.TEXT_BUNDLE),
		    			  invoiceId );
		    	  return new DocumentHandler();
			  } 
				
				 // check if payment is null or not while assigning TRADE_LOAN 
				 boolean isPayDateNull = InvoiceUtility.isPaymentDateNull(invoiceGroupOid);	
				 CorporateOrganization corporateOrganization = (CorporateOrganization) mediatorServices.createServerEJB("CorporateOrganization",
							Long.parseLong(corpOrgOid));
				 
				 LOG.info("isPayDateNull:"+isPayDateNull);
			  if(TradePortalConstants.TRADE_LOAN.equals(loanType) && !isPayDateNull &&
						TradePortalConstants.BANK_DEF_PAYMENT_ALLOW_NO.equals(corporateOrganization.getAttribute("use_payment_date_allowed_ind"))){
					
					mediatorServices.getErrorManager().issueError(
			    			  UploadInvoiceMediator.class.getName(),
			    			  TradePortalConstants.TRADE_LOAN_NO_PAYMENT_DATE, mediatorServices.getResourceManager().getText("UploadInvoices.Invoice", TradePortalConstants.TEXT_BUNDLE)+ " - "+
										invoiceId);
			    	  return new DocumentHandler();
				}

			  //Recalculating the Interest Discount rate amount while assigning loan type
		      try {
				  if(StringFunction.isNotBlank(loanType) && TradePortalConstants.EXPORT_FIN.equals(loanType)){
				   	LOG.debug("before invoking calculateInterestDiscount amount:"+amount);
					invoiceSummaryData.calculateInterestDiscount(corpOrgOid);
				  }
				else if(StringFunction.isNotBlank(loanType) && TradePortalConstants.TRADE_LOAN.equals(loanType)){
											LOG.debug("corpOrgOid:"+corpOrgOid);
				}	
				else {
					  invoiceSummaryData.setAttribute("net_invoice_finance_amount", null);
				  }
		      
		      }catch(AmsException e){
		    	  IssuedError is = e.getIssuedError();
					if (is != null) {
						String errorCode = is.getErrorCode();
						//- add Pct Val To Fn Zero and combine three if branches w/identical logic
						if (TradePortalConstants.ERR_UPLOAD_INV_TENOR_DAYS_LESS.equals(errorCode) ||
								TradePortalConstants.ERR_UPLOAD_INV_DUE_PMT_DAYS_DIFF.equals(errorCode) ||
								TradePortalConstants.ERR_TRADE_INV_DUE_PMT_DAYS_DIFF.equals(errorCode) ||
								TradePortalConstants.ERR_UPLOAD_INV_PCT_VAL_TO_FIN_ZERO.equals(errorCode))
						{
							   mediatorServices.getErrorManager().issueError(
						    			  UploadInvoiceMediator.class.getName(),
						    			  errorCode,invoiceSummaryData.getAttribute("invoice_id"));
						    	  return new DocumentHandler();

						}
						
						else {
							mediatorServices.debug("UploadInvoiceMediatorBean.assignInstrType: Unexpected error: " + e.toString());
							throw e;
						}
					}
					else {
						mediatorServices.debug("UploadInvoiceMediatorBean.assignInstrType: Unexpected error: " + e.toString());
						throw e;
					}
		      }
			  boolean isCompliant = true;			  
		        // Set the optimistic lock attribute to make sure the invoice hasn't been changed by someone else since it was first retrieved
			  	invoiceSummaryData.setAttribute("loan_type", loanType);  
			 	invoiceSummaryData.setAttribute("opt_lock", optLock);
		        String tpName=invoiceSummaryData.getTpRuleName(); 
				invoiceSummaryData.setAttribute("tp_rule_name", tpName);
				invoiceSummaryData.setAttribute("action",  TradePortalConstants.UPLOAD_INV_ACTION_ASSIGN_LOAN_TYPE);
		        invoiceSummaryData.setAttribute("user_oid",userOid);
				int saveSuccess = invoiceSummaryData.save();
				String newGroupOid = invoiceSummaryData.getAttribute("invoice_group_oid");
				if(saveSuccess == 1 && isCompliant)
				  {
					if(StringFunction.isNotBlank(newGroupOid) && !invoiceGroupOid.equals(newGroupOid)){
						InvoiceUtility.updateAttachmentDetails(newGroupOid,invoiceGroupOid,invoiceOid,mediatorServices);
					}
							// display success message...
						mediatorServices.getErrorManager().issueError(
									TradePortalConstants.ERR_CAT_1,
									TradePortalConstants.INV_ASSIGN_LOAN_TYPE,
									invoiceId);
				   }	   
		   
		   }catch(AmsException e){
			   e.printStackTrace();
		   }
		   
	    }
	     
	   return (new DocumentHandler());
      } 
	  
	 /**
	  * This method handles the creation of Loan Request Instrument based on rules.
	  *  These rules are based on Loan Creation Rules.
	  * @param inputDoc
	  * @param mediatorServices
	  * @return
	  * @throws RemoteException
	  * @throws AmsException
	  */
	 private DocumentHandler createLoanRequestByRulesForDetail(DocumentHandler inputDoc, MediatorServices mediatorServices) 
	throws RemoteException, AmsException {
		LOG.debug("createLoanRequestByRules()...."+inputDoc);
		Vector invoiceList = null;
		Vector invoiceOidList = new Vector();
		String invoiceOid = null;
		String optLock = null; //optimistic locking
		DocumentHandler invoiceItem = null;
		String invoiceStatus = null;
		String invoiceId = null;
		String instrumentType = null;
		DocumentHandler outputDoc =  new DocumentHandler();
		int count = 0;
		String instrOid = null;
		String tranOid = null;
		boolean success =false;
		String [] invoiceIds=null;
		invoiceList				  = inputDoc.getFragments("/UploadInvoiceList/InvoicesSummaryData");
		String userOid			  = inputDoc.getAttribute("/UploadInvoiceList/userOid");
		String loanType			  = inputDoc.getAttribute("/UploadInvoiceList/loan_type");
		String securityRights	  = inputDoc.getAttribute("/UploadInvoiceList/securityRights");
		String clientBankOid	  = inputDoc.getAttribute("/UploadInvoiceList/clientBankOid");	
		String corpOrgOid 		  = inputDoc.getAttribute("/UploadInvoiceList/ownerOrg");
		String timeZone 		  = inputDoc.getAttribute("/UploadInvoiceList/timeZone");
		String currencyCode 	  = inputDoc.getAttribute("/UploadInvoiceList/baseCurrencyCode");		
		String fromPayDetail 	 = inputDoc.getAttribute("/UploadInvoiceList/fromPayDetail");
		 InstrumentProperties instProp = new InstrumentProperties();
		 instProp.setCorporateOrgOid(corpOrgOid);
		 instProp.setSecurityRights(securityRights);
		 instProp.setUserOid(userOid);
		 instProp.setClientBankOid(clientBankOid);
		 instProp.setCurrency(currencyCode);
		 
		 instProp.setMediatorServices(mediatorServices);
		 instProp.setTimeZone(timeZone);	
		//IR 22652 - get correct allow payment date ind for LRQ
		CorporateOrganization corporateOrg = (CorporateOrganization) mediatorServices
	        .createServerEJB("CorporateOrganization");
		corporateOrg.getData(Integer.parseInt(corpOrgOid));
		instProp.setUsePaymentDate(corporateOrg.getAttribute("use_payment_date_allowed_ind"));
		
		instProp.setSecurityRights(securityRights);
		ClientServerDataBridge csdb = mediatorServices.getCSDB();
        String userLocale = csdb.getLocaleName();
        instProp.setUserLocale(userLocale);
		count = invoiceList.size();	
		LOG.debug("UploadInvoiceMediatorBean: number of invoices selected : " + count);
		
		if (count <= 0) 
		{
			mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.NO_ITEM_SELECTED,
					mediatorServices.getResourceManager().getText("UploadInvoices.CreateLoanReqByRules", TradePortalConstants.TEXT_BUNDLE));
			return new DocumentHandler();
		}
		int errorCount =0;
		invoiceIds = new String[count]; 	
		// Loop through the list of invoice oids, and processing each one in the invoice list
	    for (int i = 0; i < count; i++) 
	    {
		  invoiceItem = (DocumentHandler) invoiceList.get(i);
		  LOG.debug(" value is :"+invoiceItem.getAttribute("/InvoiceData"));
		  if(StringFunction.isBlank(invoiceItem.getAttribute("/InvoiceData"))){
			mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.NO_ITEM_SELECTED,
					mediatorServices.getResourceManager().getText("UploadInvoices.CreateLoanReqByRules", TradePortalConstants.TEXT_BUNDLE));
			return new DocumentHandler();
		  }
		  String[] invoiceData = invoiceItem.getAttribute("/InvoiceData").split("/");
		  invoiceOid = invoiceData[0];
		  optLock = invoiceData[1];
		  invoiceOidList.add(invoiceOid);
		  invoiceIds[i]=invoiceData[0];	
		  
		   try
		   {
		      InvoicesSummaryData invoiceSummaryData = (InvoicesSummaryData) mediatorServices.createServerEJB("InvoicesSummaryData", Long.parseLong(invoiceOid));
		      String  invoiceType  = invoiceSummaryData.getAttribute("invoice_classification");
				if (("P".equals(invoiceType) && !SecurityAccess.hasRights(securityRights, SecurityAccess.PAY_CREATE_LOAN_REQ))
						|| ("R".equals(invoiceType) && !SecurityAccess.hasRights(securityRights, SecurityAccess.RECEIVABLES_CREATE_LOAN_REQ))) {
					
					mediatorServices.getErrorManager().issueError(
							TradePortalConstants.ERR_CAT_1,
							TradePortalConstants.NO_ACTION_MATCH_NOTICE_AUTH,
							mediatorServices.getResourceManager().getText(
									"SecurityProfileDetail.PayCreateLoanReq",
									TradePortalConstants.TEXT_BUNDLE));
			        return new DocumentHandler();
				}
		      invoiceStatus = invoiceSummaryData.getAttribute("invoice_status");
		      invoiceId = invoiceSummaryData.getAttribute("invoice_id");
			  instrumentType = invoiceSummaryData.getAttribute("linked_to_instrument_type");
			  instrOid = invoiceSummaryData.getAttribute("instrument_oid");
			  tranOid = invoiceSummaryData.getAttribute("transaction_oid");	
		     //if Instrument Type is blank or not LRQ type then do not create an instrument
		      if(StringFunction.isBlank(instrumentType) || !InstrumentType.LOAN_RQST.equals(instrumentType)){
		      
		    	  mediatorServices.getErrorManager().issueError(
		    			  UploadInvoiceMediator.class.getName(),
		    			  TradePortalConstants.REC_INVOICE_LOAN_REQ_RULES_NOT_ALLOW);
		    	  return new DocumentHandler();
			  } 
		      if(StringFunction.isNotBlank(tranOid) && StringFunction.isNotBlank(instrOid)){
					Instrument	instr = (Instrument) mediatorServices.createServerEJB("Instrument",Long.parseLong(instrOid));				
				
					mediatorServices.getErrorManager().issueError(UploadInvoiceMediator.class.getName(),
						TradePortalConstants.LRQ_INSTRUMENT_EXISTS,
						mediatorServices.getResourceManager().getText("UploadInvoices.LRQMessage", TradePortalConstants.TEXT_BUNDLE) +" - " +instr.getAttribute("complete_instrument_id"),invoiceId);					
					errorCount++;
				}
			
		   }
		   catch(Exception e)
		   {		   
			   LOG.info("General exception caught in UploadInvoiceMediatorBean:createLoanRequestByRules");
			   e.printStackTrace();
		   }	   
	    }
		if(errorCount>0){
			   return new DocumentHandler();
		 }

	    //LoanType check for the selected invoices
		if(invoiceIds!=null && invoiceIds.length>0){
			
			Vector invoicesDoc =  getInvoicesDetail(invoiceIds,corpOrgOid,fromPayDetail);	
			LOG.debug("invoicesDoc:"+invoicesDoc);
			DocumentHandler row = null;
			String tpName = null;
			 if (invoicesDoc != null && invoicesDoc.size()>0) {				 
				 
				 for (int j=0; j<invoicesDoc.size(); j++) {
					  row = (DocumentHandler) invoicesDoc.elementAt(j);
					  loanType = row.getAttribute("/LOAN_TYPE");
					  tpName = row.getAttribute("/TP_NAME");
					  LOG.debug("loanType:"+loanType+"\t tpName:"+tpName);
					  if(StringFunction.isBlank(loanType)){
							 mediatorServices.getErrorManager().issueError(
										TradePortalConstants.ERR_CAT_1,
										TradePortalConstants.INST_LOAN_TYPE_MISSED_GROUP);							
								return new DocumentHandler();
						 }
				 }
			 }
		}
	
	   try{
		   String creationDateTime = DateTimeUtility.getGMTDateTime(true, false);
	    	 Vector oldGroupOids = POLineItemUtility.getInvoiceGroupOidsForInvoiceOids(invoiceOidList);
			 LOG.debug("oldGroupOids:"+oldGroupOids);
			 groupInvoices(inputDoc,invoiceOidList,instProp);
			
			 LOG.debug("size:"+invOIds.size()+"\t:"+invOIds.toString());
			 if (invOIds.size() > 0){
				 String uploadInvOid = null;
				   Vector invoiceIDVector = new Vector(invOIds);					
					LOG.debug("invoiceIDVector:"+invoiceIDVector);
					InvoicesSummaryData invoiceSummaryData = null;	
                 for (Object anInvoiceIDVector : invoiceIDVector) {
						
                     uploadInvOid = (String) anInvoiceIDVector;
	 				invoiceSummaryData =
							(InvoicesSummaryData) mediatorServices.createServerEJB("InvoicesSummaryData",
									Long.parseLong(uploadInvOid));	 	 				
	 				
			             String tpName=invoiceSummaryData.getTpRuleName(); 
					invoiceSummaryData.setAttribute("tp_rule_name", tpName);
	 				int saveYN = invoiceSummaryData.save();
	 				LOG.debug("SaveYN :"+saveYN);		
	 				
				}					
				
				 String sql = "select distinct ins.COMPLETE_INSTRUMENT_ID INS_NUM, " +
			                "ins.instrument_type_code INS_TYP, tran.transaction_type_code TRAN_TYP " +
			                "from invoices_summary_data isd inner join instrument ins on isd.A_INSTRUMENT_OID = ins.INSTRUMENT_OID  " +
			                "inner join transaction tran on ins.original_transaction_oid = tran.transaction_oid " +
			                "and ";
				 	List<Object> sqlprmLst = new ArrayList<Object>();
			        sql+= POLineItemUtility.getInvoiceItemOidsSqlWithPlaceHolder(new Vector(invOIds), "isd.upload_invoice_oid",sqlprmLst);
			        DocumentHandler resultSetDoc = DatabaseQueryBean.getXmlResultSet(sql, false, sqlprmLst);		  
			        outputDoc.addComponent("/Out",resultSetDoc);
			        LOG.debug("outputDoc->"+outputDoc);				        
			        updateFileUpload(inputDoc,outputDoc, creationDateTime,instProp);
			      //  LOG.debug("before invoice group for assigned invoices:"+sb.toString());
					Vector newGroupOids = POLineItemUtility.getInvoiceGroupOidsForInvoiceOids(new Vector(invOIds));
					LOG.debug("new group oid:"+newGroupOids);
					updateAttachmentDetails(oldGroupOids,newGroupOids, mediatorServices);
					LOG.debug("completed...attachment..");
				success =true;
			  } else if(loanReqStatus && invOIds.size() == 0){
				   mediatorServices.getErrorManager().issueError(
			    			  UploadInvoiceMediator.class.getName(),
			    			  TradePortalConstants.AUTO_LOAN_REQ_BY_RULES_NO_ITEMS_FOR_LRC_RULE);
			  }
	
			} catch (AmsException e) {
			
				e.printStackTrace();
			}
		   invOIds = new TreeSet<String>(); 
			   if(success)
				   mediatorServices.getErrorManager().issueError(
							TradePortalConstants.ERR_CAT_1, 
							TradePortalConstants.AUTO_LOAN_REQ_RULE_CREATION_SUCCESS); 
	   return outputDoc;
	   
      } 

	 private void updateFileUpload(DocumentHandler inputDoc,
			DocumentHandler outputDoc, String creationDateTime,InstrumentProperties instProp) throws RemoteException, AmsException {
		boolean success = true;

		InvoiceFileUpload invoiceUpload = (InvoiceFileUpload) instProp.getMediatorServices()
				.createServerEJB("InvoiceFileUpload");
		invoiceUpload.newObject();
		invoiceUpload.setAttribute("invoice_file_name", "Automated");
		invoiceUpload.setAttribute("creation_timestamp", creationDateTime);
		invoiceUpload.setAttribute("owner_org_oid",
				inputDoc.getAttribute("/UploadInvoiceList/ownerOrg"));
		invoiceUpload.setAttribute("user_oid",
				inputDoc.getAttribute("/UploadInvoiceList/userOid"));

		DocumentHandler resultsParamDoc = new DocumentHandler();
		DocumentHandler errorDoc = outputDoc.getFragment("/Error");
		if (errorDoc != null) {
			String maxErrorSeverity = errorDoc.getAttribute("maxerrorseverity");
			if ("5".equals(maxErrorSeverity)) {
				Vector errVector = errorDoc.getFragments("/errorlist/error");
				for (Object err : errVector) {
					DocumentHandler errDoc = (DocumentHandler) err;
					String errMessage = errDoc.getAttribute("message");
					InvoiceUtility.insertToInvErrorLog("groupingErr- ",
							invoiceUpload
									.getAttribute("invoice_file_upload_oid"),
							errMessage, null);
					InvoiceUtility.insertToInvErrorLog("groupingErr- ",
							invoiceUpload
									.getAttribute("invoice_file_upload_oid"),
							"Error returned: " + errorDoc.toString(), "N");
				}
				success = false;
			}
		}

		if (success) {
			resultsParamDoc.addComponent("/", outputDoc.getFragment("/Out"));
			Vector v = resultsParamDoc.getFragments("/ResultSetRecord");
			String errorCode = "";
			if (errorDoc != null) {
				String maxSeverity = errorDoc.getAttribute("/maxerrorseverity");
				if ("1".equals(maxSeverity)) {
					Vector errVector = errorDoc
							.getFragments("/errorlist/error");
					for (Object err : errVector) {
						DocumentHandler errDoc = (DocumentHandler) err;
						if ("I182".equals(errDoc.getAttribute("/code"))) {
							errorCode = errDoc.getAttribute("/code");
						} else if ("I477".equals(errDoc.getAttribute("/code"))) {
							errorCode = errDoc.getAttribute("/code");
						}
					}
				}
			}
			if (v.size() > 0) {
				String resultParams = resultsParamDoc.toString();
				invoiceUpload.setAttribute("inv_result_parameters",
						resultParams);
			} else if (StringFunction.isBlank(errorCode)) { 
				IssuedError ie = ErrorManager.findErrorCode(
						TradePortalConstants.ERROR_GROUPING_INVS, instProp.getUserLocale());
				String errMsg = ie.getErrorText();
				InvoiceUtility.insertToInvErrorLog("groupingErr- ",
						invoiceUpload.getAttribute("invoice_file_upload_oid"),
						errMsg, null);
				InvoiceUtility.insertToInvErrorLog("groupingErr- ",
						invoiceUpload.getAttribute("invoice_file_upload_oid"),
						"No Output returned: " + errorDoc.toString(), "N");
			} else {
				IssuedError ie = ErrorManager.findErrorCode(errorCode,
						instProp.getUserLocale());
				if (ie != null) {
					String errMsg = ie.getErrorText();
					InvoiceUtility.insertToInvErrorLog("groupingErr- ",
							invoiceUpload
									.getAttribute("invoice_file_upload_oid"),
							errMsg, null);
					if (errorDoc!=null) InvoiceUtility.insertToInvErrorLog("groupingErr- ",
							invoiceUpload
									.getAttribute("invoice_file_upload_oid"),
							"No Output returned: " + errorDoc.toString(), "N");
				}
			}
			invoiceUpload.setAttribute("validation_status",
					TradePortalConstants.PYMT_UPLOAD_VALIDATION_SUCCESSFUL);

		}else{
			invoiceUpload.setAttribute("validation_status",
					TradePortalConstants.PYMT_UPLOAD_VALIDATION_FAILED);

		}
		invoiceUpload.setAttribute("completion_timestamp", DateTimeUtility.getGMTDateTime(true, false));
		
	    //save the active_po_upload record
		invoiceUpload.save(false); //false means don't validate just save

	}	
	 
	 /**
	  * This method groups the invoices based on loan request rules.
	  * @param inputDoc
	  * @param invoiceOidList
	  * @return
	  * @throws AmsException
	  * @throws RemoteException
	  */
	 private DocumentHandler groupInvoices(DocumentHandler inputDoc, Vector invoiceOidList,InstrumentProperties instProp) throws AmsException, RemoteException {
		 LOG.debug("groupInvoices()....");	
		 
		 Vector loanReqRulesList = null;
		 DocumentHandler loanRequestDoc = null;
		 Vector invoiceItemList = null;
		 int count =0;		
		 StringBuilder invoiceOidBuf = new StringBuilder();
		 String fromPayDetail = inputDoc.getAttribute("/UploadInvoiceList/fromPayDetail");
		 LOG.debug("groupInvoices().fromPayDetail:"+fromPayDetail);
         for (Object anInvoiceOidList : invoiceOidList) {
             invoiceOidBuf.append(anInvoiceOidList).append(",");
         }
		 invoiceOidBuf.delete(invoiceOidBuf.toString().length()-1,invoiceOidBuf.toString().length());		
		
		 SELECT_SQL = "select name,inv_only_create_rule_desc,inv_only_create_rule_template,inv_only_create_rule_max_amt,due_or_pay_max_days,due_or_pay_min_days,a_op_bank_org_oid, " +
		 		"pregenerated_sql " +
		 		EXTRA_SELECT_SQL +
		 		" from inv_only_create_rule where a_invoice_def in (select a_upload_definition_oid from invoices_summary_data  " +
		 		" where upload_invoice_oid in ("+invoiceOidBuf.toString()+")) and a_owner_org_oid in ( ";
		
 
		 loanReqRulesList = getLoanRequestRules(instProp.getCorporateOrgOid());
		 LOG.debug("loanReqRulesList:"+loanReqRulesList);
	   //Get the distinct loan types for the list of invoicesOids and apply rules on those list
	     String loanTypeSQL = " select distinct loan_type loantype from invoices_summary_data where upload_invoice_oid in (";
	     Vector loanList = InvoiceUtility.getDistinctLoanTypes(loanTypeSQL,invoiceOidBuf.toString(),instProp.getCorporateOrgOid());
	     String loanType = "";
	     
	     if(loanList!=null && loanList.size()>0){	
	    	
             for (Object aLoanList : loanList) {
                 loanType = (String) aLoanList;
		     
		     for (int i = 0; i < loanReqRulesList.size(); i++) {
			      // Get the Loan Request rule and pull some values off it.
		    	 loanRequestDoc = (DocumentHandler) loanReqRulesList.elementAt(i);      
				 LOG.debug("loanRequestDoc:"+loanRequestDoc);
			      // Get the getInvoiceItems for this loan request rules
		    	 invoiceItemList = getInvoiceItems(invoiceOidBuf.toString(),loanRequestDoc,loanType,fromPayDetail,instProp);		  
			      LOG.info("count-->"+count);
			      if (invoiceItemList.size() < 1) {
			    	  LOG.debug("No rules InvoiceItems available...");
	
			      } else {
			        // Now start chunking to po line items.
			    	  chunkInvoices(invoiceItemList, loanRequestDoc,fromPayDetail,instProp);
			        count++; 
			      }
			    }
		   }
	     }
	     if(count !=0){
	     logUnassignedInvoiceCount(invoiceOidBuf.toString(),instProp);
	     }
		 LOG.debug("count:"+count);
			return loanRequestDoc;
		
		 
	 }
	private void logUnassignedInvoiceCount(String invoiceOids,InstrumentProperties instProp)
			throws AmsException {
		int count = 0;
		List<Object> sqlprmLst = new ArrayList<Object>();
		String whereClause = " a_transaction_oid is NULL and a_corp_org_oid = ? ";
		sqlprmLst.add(instProp.getCorporateOrgOid());

		// If we're processing for a specific upload sequence, log a message
		// for how many items are unassigned from that sequence.
		if (StringFunction.isNotBlank(invoiceOids)) {
	        String placeHolderStr = SQLParamFilter.preparePlaceHolderStrForInClause(invoiceOids, sqlprmLst);
	        whereClause += " and upload_invoice_oid in ("+ placeHolderStr + ")";

			count = DatabaseQueryBean.getCount("upload_invoice_oid", "INVOICES_SUMMARY_DATA", whereClause, false, sqlprmLst);
			if (count > 0) {
				instProp.getMediatorServices().getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.NO_MATCH_RULE_INV,
						String.valueOf(count));
			}

		}

	}

	 /**
	  * This method processes the invoices which are already
	  * passed through the rules and groups invoices for creating 
	  * loan request instrument
	  * @param invoiceItemList
	  * @param creationRuleDoc
	  * @throws RemoteException
	  * @throws AmsException
	  */
	 private void chunkInvoices(Vector invoiceItemList, DocumentHandler creationRuleDoc,String fromPayDetail,InstrumentProperties instProp)
		      throws RemoteException, AmsException {
		 
		    LOG.debug("inside checPOs()....invoiceItemList "+invoiceItemList);
		    BigDecimal invoiceAmount;
		    BigDecimal maxAmount = null;
		    BigDecimal convertedMaxAmount = null;
		    BigDecimal totalAmount = BigDecimal.ZERO;

		    int itemCount = 0;
		    String lastBenName = "";
		    String lastCurrency = "";
		    String lastPayMethod = "";  
		    String latestDuepaymentDt = null;
		    String lastUploadInvoiceOid = "";
		    String benName = "";
		    String currency = "";
		    String payMethod ="";
		    String oid;
		    String duePaymentDt;
		    String loanType = "";
		    String uploadInvoiceOid = "";
		    String loanReqRuleName = creationRuleDoc.getAttribute("/NAME");
			LOG.debug("loanReqRuleName:"+loanReqRuleName);		    
		    
		    Vector invoiceOids = null;
		    DocumentHandler invoiceItemDoc = null;		  
		    int invoiceListCount = 0;

		    CorporateOrganization corporateOrg = (CorporateOrganization) instProp.getMediatorServices()
		        .createServerEJB("CorporateOrganization");
		    try {
		    	corporateOrg.getData(Integer.parseInt(instProp.getCorporateOrgOid()));
		    } catch (InvalidObjectIdentifierException e) {
		    	instProp.getMediatorServices().getErrorManager().issueError(
		          TradePortalConstants.ERR_CAT_1, TradePortalConstants.ORG_NOT_EXIST);
		    }
		    long clientBankOid = corporateOrg.getAttributeLong("client_bank_oid");
		    ClientBank clientBank = (ClientBank) instProp.getMediatorServices().createServerEJB("ClientBank");
		    try {
		      clientBank.getData(clientBankOid);
		    } catch (InvalidObjectIdentifierException e) {
		    	instProp.getMediatorServices().getErrorManager().issueError(
		          TradePortalConstants.ERR_CAT_1, TradePortalConstants.INVALID_CLIENT_BANK_ORG);
		    }
		
		    // Determine the number of Invoices allowed per instrument
		    //int maximumInvoicesInInstr = MAX_ITEMS_ON_INVOICE;		    
		    String value = creationRuleDoc.getAttribute("/INV_ONLY_CREATE_RULE_MAX_AMT");
			
		    if (StringFunction.isNotBlank(value)) {
		      maxAmount = new BigDecimal(value);
		    } else {
		      maxAmount = null;

		    }
		     LOG.info("invoiceItemList->"+invoiceItemList);
		     Vector invOids = new Vector();
			 for (int x = 0; x < invoiceItemList.size(); x++) {
					invoiceItemDoc = (DocumentHandler) invoiceItemList.elementAt(x);
					invOids.add(invoiceItemDoc.getAttribute("/UPLOAD_INVOICE_OID"));
						 	    		
			 }
			LOG.info("invOids:"+invOids);
		    //This is to check if amount to validated against rules if invoice rules found>1
		    String amtSQL = "select count(distinct (a_upload_definition_oid)) a_upload_definition_oid from " +
		    		"invoices_summarY_data where ";
		    List<Object> sqlprmLst = new ArrayList<Object>();
	        amtSQL+= POLineItemUtility.getInvoiceItemOidsSqlWithPlaceHolder(invOids, "UPLOAD_INVOICE_OID",sqlprmLst);

	        DocumentHandler amoutDoc = DatabaseQueryBean.getXmlResultSet(amtSQL, false, sqlprmLst);	
	        Vector resultsVector = amoutDoc.getFragments("/ResultSetRecord/");
	        if( resultsVector.elementAt(0) != null )
	        {
	          DocumentHandler loanDoc = ((DocumentHandler)resultsVector.elementAt(0));
	       
	          String amountCnt = loanDoc.getAttribute("/A_UPLOAD_DEFINITION_OID");
	          if(Integer.parseInt(amountCnt)>1){
	        	  maxAmount = null;
	          }
	        } 
		    Vector processedInvoiceOids = new Vector();
		    for (int x = 0; x < invoiceItemList.size(); x++) {
		      // We will use this variable to count the number of Invoice line items fully processed, i.e. not skipped.
		    	invoiceListCount++;

		      invoiceItemDoc = (DocumentHandler) invoiceItemList.elementAt(x);
			  LOG.debug("invoiceItemDoc:"+invoiceItemDoc);
		      // Pull off values from the document that drive the processing.
			  invoiceAmount = invoiceItemDoc.getAttributeDecimal("/AMOUNT");
			  if(!StringFunction.isBlank(fromPayDetail) && TradePortalConstants.PAYBLE_INVOICE_PAGE.equals(fromPayDetail))
				  benName = invoiceItemDoc.getAttribute("/SELLER_NAME");
			  else
				  benName = invoiceItemDoc.getAttribute("/BUYER_NAME");
		      currency = invoiceItemDoc.getAttribute("/CURRENCY");
		      uploadInvoiceOid = invoiceItemDoc.getAttribute("/UPLOAD_INVOICE_OID");		     
		      duePaymentDt = invoiceItemDoc.getAttribute("/INVDATE"); 
		      payMethod = invoiceItemDoc.getAttribute("/PAY_METHOD"); 
		      loanType = invoiceItemDoc.getAttribute("/LOAN_TYPE"); 
			  LOG.debug("loanType:"+loanType+"invoiceAmount:"+invoiceAmount+"\t benName:"+benName+"\t currency:"+currency+"\t uploadINv oid:"+uploadInvoiceOid+"\t duePaymentDt:"+duePaymentDt);
		      invoiceOids = new Vector();		        

		        lastBenName = benName;
		        lastCurrency = currency;	
		        lastPayMethod = payMethod;
		        latestDuepaymentDt = duePaymentDt;
		        lastUploadInvoiceOid = uploadInvoiceOid;
		        // Add the oid to the Invoice oid vector.
		        invoiceOids.add(uploadInvoiceOid);
				LOG.debug("first adding into invoiceOids:"+invoiceOids+"\t lastUploadInvoiceOid:"+lastUploadInvoiceOid);
		        convertedMaxAmount = getConvertedMaxAmount(maxAmount,instProp.getCurrency(), currency, creationRuleDoc,instProp);
				LOG.debug("convertedMaxAmount:"+convertedMaxAmount);
		        itemCount = 1;
		        totalAmount = invoiceAmount;

		        if ((convertedMaxAmount == null ||
		        		totalAmount.compareTo(convertedMaxAmount) <= 0)) {
		            // Find Invoices that can be added to this group
		            int subIdx = x + 1;

		            while (subIdx < invoiceItemList.size()) {
		            	invoiceItemDoc = (DocumentHandler) invoiceItemList.elementAt(subIdx);

		                // Pull off values from the document that drive the processing.
		                invoiceAmount = invoiceItemDoc.getAttributeDecimal("/AMOUNT");
		                if(!StringFunction.isBlank(fromPayDetail) && TradePortalConstants.PAYBLE_INVOICE_PAGE.equals(fromPayDetail))
		  				  benName = invoiceItemDoc.getAttribute("/SELLER_NAME");
		  			  else
		  				  benName = invoiceItemDoc.getAttribute("/BUYER_NAME");
		                currency = invoiceItemDoc.getAttribute("/CURRENCY");
		                uploadInvoiceOid = invoiceItemDoc.getAttribute("/UPLOAD_INVOICE_OID");		             
		                duePaymentDt = invoiceItemDoc.getAttribute("/INVDATE");
		                payMethod = invoiceItemDoc.getAttribute("/PAY_METHOD") == null ? "" : invoiceItemDoc.getAttribute("/PAY_METHOD");
		                loanType = invoiceItemDoc.getAttribute("/LOAN_TYPE");
						LOG.debug(" loanType:"+loanType+"\t -> poamount:"+invoiceAmount+"\t benName:"+benName+"\t currency:"+currency+"\t uploadInvoiceOid:"+uploadInvoiceOid+"\t duePaymentDt :"+duePaymentDt);
		               
					    if(TradePortalConstants.TRADE_LOAN.equals(loanType)){
					    	if (StringFunction.isNotBlank(payMethod) && TradePortalConstants.PAYBLE_INVOICE_PAGE.equals(fromPayDetail)){
				                if (!currency.equals(lastCurrency) || !payMethod.equals(lastPayMethod))		                	
				                {
				                	// None of the remaining INVs can be added to this group
				                    break;
				                }

					    	}else{
				                if (!currency.equals(lastCurrency))		                	
				                {
				                	// None of the remaining INVs can be added to this group
				                    break;
				                }
					    	}
						 } else {
			                if (!benName.equals(lastBenName) ||
			                		!currency.equals(lastCurrency) ||
			                		!duePaymentDt.equals(latestDuepaymentDt))		                	
			                {
			                	// None of the remaining Invoices can be added to this group
			                    break;
			                }
		            	} 
					    LOG.debug("invoiceAmount:"+invoiceAmount);
		                BigDecimal newTotal = totalAmount.add(invoiceAmount);
		                if (convertedMaxAmount == null ||
		                		newTotal.compareTo(convertedMaxAmount) <= 0) {
		                	// Add this Invoice to the group
		                	invoiceOids.add(uploadInvoiceOid);		                 
							LOG.debug("adding into invoiceOids:"+invoiceOids+"\t uploadInvoiceOid:"+uploadInvoiceOid);
							invoiceItemList.remove(subIdx);
							invoiceListCount++;
		                    itemCount++;
		                    totalAmount = newTotal;
						  
		                	if (duePaymentDt != null) {
		                		if (StringFunction.isBlank(latestDuepaymentDt)) {
		                			latestDuepaymentDt = duePaymentDt;
		                		}
		                		else if (duePaymentDt.compareTo(latestDuepaymentDt) > 0) {
		                			latestDuepaymentDt = duePaymentDt;
		                		}
		                	}
		                }
		                else {
		                	// This Invoice would bring the total above the max
		                	subIdx++;
		                }
		            }
		        }

				 LOG.debug(" status loanReqStatus:"+loanReqStatus);
				if(loanReqStatus){
			        Map impFinBuyerBackMap = InvoiceUtility.getFinanceType(loanType, TradePortalConstants.PAYBLE_INVOICE_PAGE.equals(fromPayDetail) ? 
	        				TradePortalConstants.INVOICE_TYPE_PAY_MGMT : TradePortalConstants.INVOICE_TYPE_REC_MGMT );
		        	 
			        if (StringFunction.isNotBlank(lastPayMethod) && 
			        		TradePortalConstants.TRADE_LOAN.equals(loanType) &&
			        		TradePortalConstants.TRADE_LOAN_PAY.equals(impFinBuyerBackMap.get("financeType"))){
			        	if (!processedInvoiceOids.contains(invoiceOids.get(0))){
		        			

				        	String country = InvoiceUtility.fetchOpOrgCountryForOpOrg(creationRuleDoc.getAttribute("/A_OP_BANK_ORG_OID"));
				        	
				        	boolean isPaymentMethodValid = InvoiceUtility.validatePaymentMethodForInvoices(invoiceOids, country, lastCurrency, instProp.getMediatorServices().getErrorManager(),false);
				        	boolean isBankBranchCodeValid = InvoiceUtility.validateBankInformationForInvoices(invoiceOids, country, lastCurrency, instProp.getMediatorServices().getErrorManager(),false);
				        	if (!isPaymentMethodValid || !isBankBranchCodeValid){
				        		processedInvoiceOids.addAll(invoiceOids);
				        		continue;
				        	}
				        }		
			        }
					
		        // Build the input document used to create a new instrument
		        DocumentHandler instrCreateDoc = buildInstrumentCreateDoc(creationRuleDoc,
		        		lastBenName, lastCurrency, totalAmount, latestDuepaymentDt, invoiceOids,instProp);
			    instrCreateDoc.setAttribute("/importIndicator", (String)impFinBuyerBackMap.get("importIndicator"));
			    instrCreateDoc.setAttribute("/financeType", (String)impFinBuyerBackMap.get("financeType"));
			    instrCreateDoc.setAttribute("/buyerBacked", (String)impFinBuyerBackMap.get("buyerBacked"));
			    //IR 22917 - no bene name for EXP_FIN and SEL_REQ  
			    if(TradePortalConstants.EXPORT_FIN.equals(loanType)
						|| TradePortalConstants.SELLER_REQUESTED_FINANCING.equals(impFinBuyerBackMap.get("financeType")))
			    	instrCreateDoc.setAttribute("/benName", "");
		        // Create instrument using the document, assign the invoice items, and create log records.
		        try {
		        	createLRQInstrumentFromInvoiceData(instrCreateDoc, invoiceOids, loanReqRuleName,instProp);
		        	invOIds.addAll(invoiceOids);
	
		        }
		        catch (AmsException e) {
		        	// This is probably a sql error but could be anything.
		        	// Post this exception to the log.
		        	LOG.info(instProp.getCorporateOrgOid()+"\t"+ e.getMessage());
		        	e.printStackTrace();
		        }
				 }
		    } 
	 }
	 
	 /**
	  * This method prepares the doc object required for creating an Instrument 
	  * with appropriate attributes for Loan Req Instrument.
	  * @param creationRuleDoc
	  * @param benName
	  * @param currency
	  * @param amount
	  * @param latestShipDt
	  * @param invoiceOids
	  * @return
	  */
	 private DocumentHandler buildInstrumentCreateDoc(DocumentHandler creationRuleDoc,
		      String benName, String currency, BigDecimal amount,
		      String latestShipDt, Vector invoiceOids,InstrumentProperties instProp) {
		 
		 LOG.debug("buildInstrumentCreateDoc () creationRuleDoc->"+creationRuleDoc);
		 LOG.debug("benName:"+benName+"\t currency:"+currency+"\t amount:"+amount+"\t latestShipDt:"+latestShipDt+"\t invoiceOids:"+invoiceOids);
		   DocumentHandler instrCreateDoc = new DocumentHandler();
			String secretKeyString = instProp.getMediatorServices().getCSDB().getCSDBValue("SecretKey");
			byte[] keyBytes = com.amsinc.ecsg.util.EncryptDecrypt.base64StringToBytes(secretKeyString);
			javax.crypto.SecretKey secretKey = new javax.crypto.spec.SecretKeySpec(keyBytes, "DESede/CBC/PKCS5Padding");
			String templateOid = creationRuleDoc.getAttribute("/INV_ONLY_CREATE_RULE_TEMPLATE");
			String encryptedString = null;
			if (StringFunction.isNotBlank(templateOid)){
				encryptedString = EncryptDecrypt.encryptStringUsingTripleDes(templateOid, secretKey);
			}
		    instrCreateDoc.setAttribute("/instrumentOid", encryptedString);
		    instrCreateDoc.setAttribute("/clientBankOid", instProp.getClientBankOid());
		    instrCreateDoc.setAttribute("/instrumentType",InstrumentType.LOAN_RQST);
		    instrCreateDoc.setAttribute("/bankBranch", creationRuleDoc.getAttribute("/A_OP_BANK_ORG_OID"));
			//Srinivasu_D IR#T36000018442 Rel8.2 06/29/2013 - TEMPL if template being used else blank
		    if(StringFunction.isBlank(templateOid)){
		    instrCreateDoc.setAttribute("/copyType", TradePortalConstants.FROM_BLANK);
			}else {
			   instrCreateDoc.setAttribute("/copyType", TradePortalConstants.FROM_TEMPL);
			}
		    instrCreateDoc.setAttribute("/userOid", instProp.getUserOid());
		    instrCreateDoc.setAttribute("/ownerOrg", instProp.getCorporateOrgOid());
		    instrCreateDoc.setAttribute("/securityRights", instProp.getSecurityRights());
		    instrCreateDoc.setAttribute("/benName", benName);
		    instrCreateDoc.setAttribute("/currency", currency);
		    instrCreateDoc.setAttribute("/shipDate", latestShipDt);
		    instrCreateDoc.setAttributeDecimal("/amount", amount);
		    instrCreateDoc.setAttribute("/uploadIndicator", TradePortalConstants.INDICATOR_YES);
		    if (instProp.getUserLocale().equals("") || instProp.getUserLocale() == null) {
		    	instProp.setUserLocale("en_US");
		    }
		    instrCreateDoc.setAttribute("/locale", instProp.getUserLocale());

		    for (int i = 0; i < invoiceOids.size(); i++) {
		      instrCreateDoc.setAttribute("/InvoiceLines(" + i + ")/uploadInvoiceOid",
		          (String) invoiceOids.elementAt(i));
		    }

		    instProp.getMediatorServices().debug("Creation rules document is" + instrCreateDoc.toString());
		    return instrCreateDoc;
		  }
	 
	 /**
	  * This method invokes the Instrument EJB and writes the invoice 
	  * details in INVOICE_HISTORY table.
	  * @param inputDoc
	  * @param invoiceOids
	  * @param lcRuleName
	  * @return
	  * @throws AmsException
	  * @throws RemoteException
	  */
	 private DocumentHandler createLRQInstrumentFromInvoiceData(DocumentHandler inputDoc,
		      Vector invoiceOids, String lcRuleName,InstrumentProperties instProp)
		      throws AmsException, RemoteException {
		 
		    LOG.debug("Entered createLRQInstrumentFromInvoiceData(). inputDoc follows ->\n"  + inputDoc.toString());
		    MediatorServices mediatorServices = instProp.getMediatorServices();		    
		    DocumentHandler outputDoc = null;
			//Create a new instrument
		    Instrument newInstr = (Instrument) mediatorServices.createServerEJB("Instrument");
		    outputDoc = newInstr.createLoanReqForInvoiceData(inputDoc, invoiceOids, lcRuleName);
		    LOG.debug("Returned from instrument.createInstrumentFromPOData()\n"+ "Returned document follows -> " + outputDoc.toString());
		    try{
		    // Now that the transaction has been created, assign this transaction
		    // oid to all the line items on the transaction.
			LOG.debug("invoiceOids:"+invoiceOids+"\t outputDoc:"+outputDoc);
			Transaction transaction = (Transaction) mediatorServices.createServerEJB("Transaction");
			String transOid = outputDoc.getAttribute("/transactionOid");
			if (transOid != null){
			long transactionOid = outputDoc.getAttributeLong("/transactionOid");
			transaction.getData(transactionOid);
			Terms terms = (Terms) transaction.getComponentHandle("CustomerEnteredTerms");
			String termsOid = terms.getAttribute("terms_oid");
			instProp.setInstrumentOid(newInstr.getAttribute("instrument_oid"));
			instProp.setTransactionOid(outputDoc.getAttribute("/transactionOid"));
			instProp.setTermsOid(termsOid);
			
		        POLineItemUtility.updateInvoiceTransAssignment(invoiceOids, outputDoc.getAttribute("/transactionOid"), newInstr.getAttribute("instrument_oid"), 
		             TradePortalConstants.RECEIVABLE_UPLOAD_INV_STATUS_ASSIGNED, termsOid);
		        
		        POLineItemUtility.createInvoiceHistoryLog (invoiceOids,TradePortalConstants.INVOICE_ACTION_AUTOCREATE,
		        		TradePortalConstants.RECEIVABLE_UPLOAD_INV_STATUS_ASSIGNED,instProp.getUserOid());

		        String requiredOid = "upload_invoice_oid";
				 String invoiceOidsSql = POLineItemUtility.getPOLineItemOidsSql(
						 invoiceOids, requiredOid);
		        
				if (TradePortalConstants.INDICATOR_NO.equals(newInstr.getAttribute("import_indicator")) || 
						TradePortalConstants.INDICATOR_X.equals(newInstr.getAttribute("import_indicator"))){
					 
					 boolean useSeller = TradePortalConstants.BUYER_REQUESTED_FINANCING.equals(terms.getAttribute("finance_type")) ;
					String invoiceDueDate = null;
					if (InvoiceUtility.validateSameCurrency(invoiceOidsSql) && 
							InvoiceUtility.validateSameDate(invoiceOidsSql) && 
							InvoiceUtility.validateSameTradingPartner(invoiceOidsSql, useSeller)){
						String dueDateSQL = "select distinct(case when payment_date is null then due_date else payment_date end ) DUEDATE " +
								" from invoices_summary_data where "+invoiceOidsSql;
						DocumentHandler resultDoc = DatabaseQueryBean.getXmlResultSet(dueDateSQL,false,new ArrayList<Object>());
						invoiceDueDate = resultDoc.getAttribute("/ResultSetRecord(0)/DUEDATE");
					}

					if (StringFunction.isNotBlank(invoiceDueDate)){

						try {
							SimpleDateFormat iso = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
							SimpleDateFormat jPylon = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
							
								Date date = jPylon.parse(invoiceDueDate);

								terms.setAttribute("loan_terms_fixed_maturity_dt",TPDateTimeUtility.convertISODateToJPylonDate(iso.format(date)));
								transaction.save(false);
						} catch (Exception e) {
							e.printStackTrace();
						}
						
					}
				}


				}else{
			         mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
			                 TradePortalConstants.ERROR_CREATING_ISTRUMENT);
					
				}
		        
		    } catch (AmsException e){
		        String [] substitutionValues = {"",""};
		        substitutionValues[0] = e.getMessage();
		        substitutionValues[1] = "0";
		         mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
		                 AmsConstants.SQL_ERROR, substitutionValues);
		         e.printStackTrace();
		    }
		   

		    return outputDoc;
		  }
	 
	 /**
	  * This method returns the set of invoices which pass through the
	  * rules defined for those invoice/invoices
	  * @param invoiceOids
	  * @param creationRuleDoc
	  * @return
	  * @throws AmsException
	  * @throws RemoteException
	  */
	 private Vector getInvoiceItems(String invoiceOids,DocumentHandler creationRuleDoc,String loanType, String fromPayable,InstrumentProperties instProp) throws AmsException {
		 	LOG.debug("getInvoiceItems()....invoiceOids ->"+invoiceOids +"\t loanType:"+loanType+"\t fromPayable:"+fromPayable);
		 	String date = ""; 	
		  
		 	if(TradePortalConstants.INDICATOR_YES.equals(instProp.getUsePaymentDate()))
		 		date = " cast (case when payment_date is null then due_date else payment_date end as date ) invdate ";
		 	else
		 		date = " due_date invdate ";
		 	//Handling for Payables & Receivables 
		 	if(!StringFunction.isBlank(fromPayable) && TradePortalConstants.PAYBLE_INVOICE_PAGE.equals(fromPayable))
			 	INVOICE_SELECT_SQL = "select upload_invoice_oid,invoice_id,seller_name,loan_type," +
				"currency,amount,p_invoice_file_upload_oid,issue_date,"+date+", pay_method from invoices_view " +
				"where nvl(deleted_ind,'N')!='Y' and a_transaction_oid is NULL and upload_invoice_oid in ("+invoiceOids+") and a_corp_org_oid = ";
		 	else
		 		INVOICE_SELECT_SQL = "select upload_invoice_oid,invoice_id,buyer_name,loan_type," +
 				"currency,amount,p_invoice_file_upload_oid,issue_date,"+date+", pay_method from invoices_view " +
 				"where nvl(deleted_ind,'N')!='Y' and a_transaction_oid is NULL and upload_invoice_oid in ("+invoiceOids+") and a_corp_org_oid = ";
		 	
		   StringBuilder invoiceSql = new StringBuilder(INVOICE_SELECT_SQL);
		   invoiceSql.append(" ? ");
		   List<Object> sqlPrmsLst = new ArrayList<Object>();
		   sqlPrmsLst.add(instProp.getCorporateOrgOid());
		   
		   if(StringFunction.isNotBlank(loanType)){
			   invoiceSql.append(" and loan_type = ? ");
			   sqlPrmsLst.add(loanType);
		   }
		   
		   // Add condition for last_ship_dt if both min and max are set;
		   String minDays = creationRuleDoc.getAttribute("/DUE_OR_PAY_MIN_DAYS");
		   String maxDays = creationRuleDoc.getAttribute("/DUE_OR_PAY_MAX_DAYS");
		   LOG.debug("mindays:"+minDays+"\t max days:"+maxDays);
		   if (StringFunction.isNotBlank(minDays)  && StringFunction.isNotBlank(maxDays)) {	   
			  
		     // First get the current local date
		     String currentGmtDate = DateTimeUtility.getGMTDateTime();
		     Date currentLocalDate = TPDateTimeUtility.getLocalDateTime(currentGmtDate, instProp.getTimeZone());		  
		     SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yy");		     
		     String today = formatter.format(currentLocalDate);
		     LOG.debug("today:"+today);
		     if(TradePortalConstants.INDICATOR_YES.equals(instProp.getUsePaymentDate())){
		    	 invoiceSql.append(" and case when payment_date is null then due_date else payment_date end  between TO_DATE(");
		     }else{
		    	 invoiceSql.append(" and due_date between TO_DATE(");
		     }
		     
		     invoiceSql.append("?)+? and  TO_DATE(?)+? ");
		     
		     sqlPrmsLst.add(today);
		     sqlPrmsLst.add(minDays);
		     sqlPrmsLst.add(today);
		     sqlPrmsLst.add(maxDays);
		   }
		  
		   // Add condition for the remaining criteria from the lc creation rule
		   String criteria = creationRuleDoc.getAttribute("/PREGENERATED_SQL");
		
		   if (StringFunction.isNotBlank(criteria)) {
			   invoiceSql.append(" and ");
			   invoiceSql.append(criteria);
		   }
		  
		   // Finally add the orderby clause.
		   invoiceSql.append(" ");
		   if(!StringFunction.isBlank(fromPayable) && TradePortalConstants.PAYBLE_INVOICE_PAGE.equals(fromPayable))
			   invoiceSql.append(PAYABLE_INVOICE_ORDERBY);
		   else
			   invoiceSql.append(RECEIVABLE_INVOICE_ORDERBY);  
		   LOG.debug("Invoice sql is " + invoiceSql.toString());
		   DocumentHandler resultSet = DatabaseQueryBean.getXmlResultSet(invoiceSql.toString(), false, sqlPrmsLst);
		
		   if (resultSet == null)
			   resultSet = new DocumentHandler();
		
		   instProp.getMediatorServices().debug("invoice list is " + resultSet.toString());
		   return resultSet.getFragments("/ResultSetRecord");

  }
	
	 /**
	  * This method returns the Loan Request rules
	  * @param corporateOrgOid
	  * @return
	  * @throws AmsException
	  */
	private Vector getLoanRequestRules(String corporateOrgOid) throws AmsException {		 
		
		 LOG.debug("getCreationRules() corporateOrgOid:"+corporateOrgOid);
		    StringBuilder loanReqSQL = new StringBuilder(SELECT_SQL);
		    List<Object> sqlPrmsLst = new ArrayList<Object>();
		    loanReqSQL.append(" ? ");
		    sqlPrmsLst.add(corporateOrgOid);

		    // Loop back through all parent orgs for oids to include in search for LoanRequest Creation rules
		    do {
		      String parentCorpSql = "select p_parent_corp_org_oid from corporate_org where organization_oid = ? AND activation_status = ? ";
			  LOG.debug("final sql to return rules :"+parentCorpSql);
		      DocumentHandler parentCorpOrg = DatabaseQueryBean.getXmlResultSet(parentCorpSql, false, corporateOrgOid, TradePortalConstants.ACTIVE);
		      if( parentCorpOrg != null )
		      {
		        Vector resultsVector = parentCorpOrg.getFragments("/ResultSetRecord/");
		        if( resultsVector.elementAt(0) != null )
		        {
		          DocumentHandler parentOid = ((DocumentHandler)resultsVector.elementAt(0));
		          String parentCorpOid = "";
		          parentCorpOid = parentOid.getAttribute("/P_PARENT_CORP_ORG_OID");
		          if (StringFunction.isNotBlank(parentCorpOid)) {
		        	  loanReqSQL.append(",?");
		            corporateOrgOid = parentCorpOid;
		            sqlPrmsLst.add(corporateOrgOid);
		          } else {  // No more parents, so break out of loop
		            break;
		          }
		        } else {  // No more parents, so break out of loop
		          break;
		        }
		      } else { // no more parents, so break out of loop
		        break;
		      }
		    } while (true);		  

		    loanReqSQL.append(") ");
		    loanReqSQL.append(" and instrument_type_code = 'LRQ' ");
		    loanReqSQL.append(ORDERBY_CLAUSE);

		    LOG.debug("LoanRequest SQL is" + loanReqSQL.toString());
		    DocumentHandler resultSet = DatabaseQueryBean.getXmlResultSet(loanReqSQL.toString(), false, sqlPrmsLst);

		    if (resultSet == null)
		      resultSet = new DocumentHandler();

		    LOG.debug("LoanRequest rule list doc is " + resultSet.toString());
		    
		    return resultSet.getFragments("/ResultSetRecord");

		  }
	
	 /**
	  * This method converts the given amount based on currency
	  * @param fromAmount
	  * @param fromCurrency
	  * @param toCurrency
	  * @param creationRuleDoc
	  * @return
	  */
	 private BigDecimal getConvertedMaxAmount(BigDecimal fromAmount,
		      String fromCurrency, String toCurrency, DocumentHandler creationRuleDoc,InstrumentProperties instProp) {

		    BigDecimal convertedAmount = null;
		    String param = TradePortalConstants.FX_LOAN_REQUEST;
		    try {
		     
		      if ((fromAmount == null) ||
		    		  fromAmount.compareTo(BigDecimal.ZERO) == 0) {
		        return null;
		      }

		      // No need to convert if the from and to currencies are the same
		      if (fromCurrency != null && fromCurrency.equals(toCurrency)) {
		        return fromAmount;
		      }

		      String templateCurrency = null;
		      String sql;
		      String multiplyIndicator = null;
		      BigDecimal rate = null;
		      DocumentHandler fxRateDoc = null;

		      ReferenceDataManager rdm = ReferenceDataManager.getRefDataMgr();

		      // Test whether the po line item currency code is a valid Trade
		      // Portal currency.
		      if (rdm.checkCode(TradePortalConstants.CURRENCY_CODE, toCurrency,
		    		  instProp.getUserLocale())) {
		        // Get the fx rate info for the po line item currency
		        fxRateDoc = getFxRateDoc(instProp.getCorporateOrgOid(), toCurrency);
		        if (fxRateDoc == null) {
		          // No fx rate found, isse warning and return a blank amount.
		          // This indicates that maxAmount is not used for grouping.
		          // Give error with the po line item currency.
					
		        	instProp.getMediatorServices().getErrorManager().issueError(instProp.getCorporateOrgOid(),
		             // getLogSequence(),
		              TradePortalConstants.LOANREQ_LOG_NO_FX_RATE, toCurrency,param);
		        	loanReqStatus = false;
		        	//break;
		        	return null;
		          
		        } else {
		          // Found an fx rate for the template currency.  Use it.
		        }
		      } else {
		        // Currency code for the po line item is not valid.  Use
		        // the currency code from the instrument creation rule's template.
		        sql = "select copy_of_currency_code from template, transaction where p_instrument_oid = c_template_instr_oid and template_oid = ?";
		        DocumentHandler templateDoc = null;
		        try {
		          templateDoc = DatabaseQueryBean.getXmlResultSet(sql, true, new Object[]{creationRuleDoc.getAttribute("/INV_ONLY_CREATE_RULE_TEMPLATE")});
		        } catch (AmsException e) {
		          templateDoc = null;
		          //loanReqStatus = false;
		        }
		        
		        if (templateDoc == null) {
		          // Can't get the currency code off the template.  Issue a warning and return
		          // a null amount.  This indicates NOT to use maxAmount for grouping.

		        	instProp.getMediatorServices().getErrorManager().issueError(instProp.getCorporateOrgOid(),
		             // getLogSequence(),
		              TradePortalConstants.LOANREQ_LOG_INVALID_CURRENCY,param,
		              creationRuleDoc.getAttribute("/NAME"));
		        		loanReqStatus = false;
		        		//break;
		          return null;
		        } else {
		          // We got the template currency but it may be blank.  We're only
		          // concerned with the first record (there should not be more).
		          templateCurrency = templateDoc
		              .getAttribute("/ResultSetRecord(0)/COPY_OF_CURRENCY_CODE");
		          if (StringFunction.isBlank(templateCurrency)) {
		            // Can't get the currency code off the template.  Issue a warning and return
		            // a null amount.  This indicates NOT to use maxAmount for grouping.

		        	  instProp.getMediatorServices().getErrorManager().issueError(instProp.getCorporateOrgOid(),
		                //getLogSequence(),
		                TradePortalConstants.LOANREQ_LOG_INVALID_CURRENCY,param,
		                creationRuleDoc.getAttribute("/NAME"));
		        	  loanReqStatus = false;
		        	  //break;
		            return null;
		          } else {
		            // Currency is good so attempt to get the fx rate doc.
		            fxRateDoc = getFxRateDoc(instProp.getCorporateOrgOid(),
		                templateCurrency);

		            // Don't look for an FX rate if the template's currency is the base currency
		            // No FX rate is needed in that case
		            if (!templateCurrency.equals(instProp.getCurrency())) {
		              if (fxRateDoc == null) {
		                // No fx rate found, isse warning and return a blank amount.
		                // This indicates that maxAmount is not used for grouping.
		                // Give error using the template currency.

		            	  instProp.getMediatorServices().getErrorManager().issueError(instProp.getCorporateOrgOid(),
		                    //getLogSequence(),
		                    TradePortalConstants.LOANREQ_LOG_NO_FX_RATE,
		                    templateCurrency);
		            	  loanReqStatus = false;
		            	 // break;
		                return null;
		              } else {
		                // Found an fx rate for the template currency.  Use it.
		              }
		            } else {
		              // Base Currency and Template currency are identical
		              // No need for conversion.  Return the from amount
		              return fromAmount;
		            }
		          } // end templateCurrency == null
		        } // end templateDoc == null
		      } // end checkCode

		      // We now have an fx rate doc (although the values on the doc may be
		      // blank.)  Attempt to convert the amount.
		      multiplyIndicator = fxRateDoc.getAttribute("/MULTIPLY_INDICATOR");
		      rate = fxRateDoc.getAttributeDecimal("/RATE");

		      if (multiplyIndicator != null && rate != null) {
		        // We're converting FROM the base currency TO the po line item
		        // currency so we need to do the opposite of the multiplyIndicator
		        // (the indicator is set for converting FROM some currency TO the
		        // base currency)
		        if (multiplyIndicator.equals(TradePortalConstants.MULTIPLY)) {
		          convertedAmount = fromAmount.divide(rate,
		              BigDecimal.ROUND_HALF_UP);
		        } else {
		          convertedAmount = fromAmount.multiply(rate);
		        }
		      } else {
		        // We have bad data in the fx rate record.  Issue warning and return
		        // null amount (indicating that maxAmount is not used for grouping)

		    	  instProp.getMediatorServices().getErrorManager().issueError(instProp.getCorporateOrgOid(),
		            //getLogSequence(),
		            TradePortalConstants.LOANREQ_LOG_NO_FX_RATE,
		            fxRateDoc.getAttribute("/CURRENCY_CODE"));
		        loanReqStatus = false;
		        //break;
		        return null;
		      }
		    } catch (AmsException e) {
		    	//loanReqStatus = false;
		      LOG.debug("Exception found getting converted max amount:");
		      LOG.info(e.toString());
		      e.printStackTrace();
		    }
			LOG.debug("loanReqStatus:"+loanReqStatus);
		    return convertedAmount;
		  }

	
	/**
	 * This method returns fx rates for the given currency. 
	 * @param corporateOrg
	 * @param currency
	 * @return
	 */
	private DocumentHandler getFxRateDoc(String corporateOrg, String currency) {
	
		Cache fxCache = TPCacheManager.getInstance().getCache(TradePortalConstants.FX_RATE_CACHE);
		DocumentHandler resultSet = (DocumentHandler) fxCache.get(corporateOrg + "|" + currency);
	
		if (resultSet != null) {
			Vector fxRateDocs = resultSet.getFragments("/ResultSetRecord");
			return (DocumentHandler) fxRateDocs.elementAt(0);
		}
	
		return null;
	
	}
	
 	 

	  /**
		 * This method creates an ATP Instrument for Receivable invoices
		 * @param inputDoc
		 * @param mediatorServices
		 * @return DocumentHandler
		 * @throws RemoteException
		 * @throws AmsException
		 */
		private DocumentHandler createLoanRequestForDetail(DocumentHandler inputDoc,
				MediatorServices mediatorServices)
		throws RemoteException, AmsException {
			
			LOG.debug("UploadInvoiceMediatorBean.createLoanRequest: Start: "+inputDoc);		
			String userOid = inputDoc.getAttribute("/UploadInvoiceList/userOid");
			String securityRights = inputDoc.getAttribute("/UploadInvoiceList/securityRights");
			String clientBankOid = inputDoc.getAttribute("/UploadInvoiceList/clientBankOid");	
			String opOrgOid = inputDoc.getAttribute("/UploadInvoiceList/bankBranch");
			String corpOrgOid = inputDoc.getAttribute("/UploadInvoiceList/ownerOrg");
			String fromPayDetail = inputDoc.getAttribute("/UploadInvoiceList/fromPayDetail");
			LOG.debug("fromPayDetail:"+fromPayDetail);
			 Vector invoiceOidsVector = new Vector();		
			 DocumentHandler instrCreateDoc = null;			 
			 String uploadInvOid= null;			
			 String instrumentOid = null;
			 String transactionOid = null;
			 //String corpOrgOid = null;		 
			 String firstInvoiceGroupOid = null;
			 String [] invoiceIds=null;
			 String newGroupOid = null;
			 List<String> invoicesList=new ArrayList();
			 Vector invoicesDoc = new Vector();
			 Vector tempOids = new Vector();
			 //IR 22616 - for payables check payables related security check
			 if(TradePortalConstants.PAYBLE_INVOICE_PAGE.equalsIgnoreCase(fromPayDetail)){
				 if (!(SecurityAccess.hasRights(securityRights, SecurityAccess.PAY_CREATE_LOAN_REQ))) {
						// user does not have appropriate authority.
						mediatorServices.getErrorManager().issueError(
								TradePortalConstants.ERR_CAT_1,
								TradePortalConstants.REC_INV_LRQ_NO_ACCESS);
						return new DocumentHandler();
					}
			 }
			 else if (!(SecurityAccess.hasRights(securityRights, SecurityAccess.RECEIVABLES_CREATE_LOAN_REQ))) {
				// user does not have appropriate authority.
				mediatorServices.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.REC_INV_LRQ_NO_ACCESS);
				return new DocumentHandler();
			}
			
			Vector<DocumentHandler> invoiceGroupList = inputDoc.getFragments("/UploadInvoiceList/InvoicesSummaryData");
			if (invoiceGroupList == null || invoiceGroupList.size() == 0) {
				mediatorServices.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.NO_ITEM_SELECTED,
						mediatorServices.getResourceManager().getText(
								"UploadInvoices.InvLRCreateLRQ",
								TradePortalConstants.TEXT_BUNDLE));
				return new DocumentHandler();
			}

			if(StringFunction.isBlank(opOrgOid)){

				mediatorServices.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.PAYABLE_INVOICE_BANK_BRANCH_REQ);
				return new DocumentHandler();
			}

			invoiceIds = new String[ invoiceGroupList.size()]; 	
			
			for (int i = 0; i < invoiceGroupList.size(); i++) {
				DocumentHandler invoiceGroupItem = invoiceGroupList.get(i);
			String[] invoiceData = invoiceGroupItem.getAttribute("InvoiceData").split("/");
			invoiceIds[i]=invoiceData[0];	
			invoicesList.add(invoiceData[0]);
			tempOids.add(invoiceData[0]);
			}
			 LOG.debug("invoiceIds:"+tempOids);

			 String loanType ="";
			 if(invoicesList!=null && invoicesList.size()>0){
					
					invoicesDoc =  getInvoicesDetail(invoiceIds,corpOrgOid,fromPayDetail);
					String dupLoanType = null;
					String  dupName = null;
					 if (invoicesDoc != null && invoicesDoc.size()>0) {
						 DocumentHandler row = (DocumentHandler) invoicesDoc.elementAt(0);
						 loanType = row.getAttribute("/LOAN_TYPE");
						 String tpName = row.getAttribute("/TP_NAME");
						 LOG.debug("loanType:"+loanType+"\t tpName:"+tpName);
						 
						 
						 if(StringFunction.isBlank(loanType)){
							 mediatorServices.getErrorManager().issueError(
										TradePortalConstants.ERR_CAT_1,
										TradePortalConstants.INST_LOAN_TYPE_MISSED_GROUP);							
								return new DocumentHandler();
						 }
						 for (int j=0; j<invoicesDoc.size(); j++) {
							  row = (DocumentHandler) invoicesDoc.elementAt(j);
							  dupLoanType = row.getAttribute("/LOAN_TYPE");
							  dupName = row.getAttribute("/TP_NAME");
							  LOG.debug("dupLoanType:"+dupLoanType+"\t dupName:"+dupName);
								if(!InstrumentType.LOAN_RQST.equals(row.getAttribute("/LINKED_TO_INSTRUMENT_TYPE"))){
										mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,TradePortalConstants.PAYABLE_MGMT_INVALID_ACTION,
												mediatorServices.getResourceManager().getText("UploadInvoices.InvLRCreateLRQ", TradePortalConstants.TEXT_BUNDLE),
												tpName,row.getAttribute("/LINKED_TO_INSTRUMENT_TYPE"));
									continue; // Move on to next Invoice
								}
							  if(StringFunction.isBlank(dupLoanType)){
									 mediatorServices.getErrorManager().issueError(
												TradePortalConstants.ERR_CAT_1,
												TradePortalConstants.INST_LOAN_TYPE_MISSED_GROUP);							
										return new DocumentHandler();
								 }
								if(!loanType.equals(dupLoanType)){
									 mediatorServices.getErrorManager().issueError(
												TradePortalConstants.ERR_CAT_1,
												TradePortalConstants.LOAN_REQ_MULTIPLE_LOAN_TYPES_SELECTED);							
										return new DocumentHandler();
								 }
						 }
				}
						for(int pos = 0; pos<invoicesList.size();pos++){
							String  invOid = invoicesList.get(pos);	
							 InvoicesSummaryData invoiceSummaryData =
									(InvoicesSummaryData) mediatorServices.createServerEJB("InvoicesSummaryData",
											Long.parseLong(invOid));
						String instrOid = invoiceSummaryData.getAttribute("instrument_oid");
						String tranOid = invoiceSummaryData.getAttribute("transaction_oid");	
						String invoiceId = invoiceSummaryData.getAttribute("invoice_id");
						firstInvoiceGroupOid = invoiceSummaryData.getAttribute("invoice_group_oid");
						//if already associated then do not create an instrument
						if(StringFunction.isNotBlank(tranOid) && StringFunction.isNotBlank(instrOid)){
							Instrument	instr = (Instrument) mediatorServices.createServerEJB("Instrument",Long.parseLong(instrOid));				
							mediatorServices.getErrorManager().issueError(UploadInvoiceMediator.class.getName(),
								TradePortalConstants.LRQ_INSTRUMENT_EXISTS,
								mediatorServices.getResourceManager().getText("UploadInvoices.LRQMessage", TradePortalConstants.TEXT_BUNDLE) +" - " +instr.getAttribute("complete_instrument_id"),invoiceId);
							invoicesList.remove(pos);
							pos--;
						}
					}
				}
			 
			if(invoicesList==null || invoicesList.size()==0)
			return new DocumentHandler();
			
			
			 InstrumentProperties instProp = new InstrumentProperties();
			 instProp.setCorporateOrgOid(corpOrgOid);
			 instProp.setSecurityRights(securityRights);
			 instProp.setUserOid(userOid);
			 instProp.setClientBankOid(clientBankOid);
			 instProp.setOpOrgOid(opOrgOid);
			 if(StringFunction.isNotBlank(fromPayDetail) && TradePortalConstants.PAYBLE_INVOICE_PAGE.equals(fromPayDetail))
				 instProp.setFromPayables(fromPayDetail);
			 instrCreateDoc = processInvoiceGroups(instProp,invoicesDoc,mediatorServices);
			 
			 String instType = instrCreateDoc!= null? instrCreateDoc.getAttribute("/instrumentType"):null;
			 mediatorServices.debug("instType:"+instType);
			
			 if(instType != null) {
				 if (invoicesDoc != null) {
					 for (int j=0; j<invoicesDoc.size(); j++) {
						 DocumentHandler row = (DocumentHandler) invoicesDoc.elementAt(j);
						 String invOid = row.getAttribute("/UPLOAD_INVOICE_OID");
						 invoiceOidsVector.addElement(invOid);
					 }
			}
			        Map impFinBuyerBackMap = InvoiceUtility.getFinanceType(loanType, 
			        		TradePortalConstants.PAYBLE_INVOICE_PAGE.equals(fromPayDetail) ? 
			        				TradePortalConstants.INVOICE_TYPE_PAY_MGMT : TradePortalConstants.INVOICE_TYPE_REC_MGMT );

			        String importIndicator = (String)impFinBuyerBackMap.get("importIndicator");
			        String financeType = (String)impFinBuyerBackMap.get("financeType");
			        String buyerBacked = (String)impFinBuyerBackMap.get("buyerBacked");
			        
			        if ( TradePortalConstants.TRADE_LOAN.equals(loanType) && TradePortalConstants.TRADE_LOAN_PAY.equals(financeType) ){
			        	String country = InvoiceUtility.fetchOpOrgCountryForOpOrg(opOrgOid);
			        	
			        	boolean isPaymentMethodValid = InvoiceUtility.validatePaymentMethodForInvoices(invoiceOidsVector, country, instProp.getCurrency(), mediatorServices.getErrorManager(),false);
			        	boolean isBankBranchCodeValid = InvoiceUtility.validateBankInformationForInvoices(invoiceOidsVector, country, instProp.getCurrency(), mediatorServices.getErrorManager(),false);
			        	if (!isPaymentMethodValid || !isBankBranchCodeValid){
			        		return new DocumentHandler();
			        	}
			        		
			        }
				 
			  // Create instrument using the document
			DocumentHandler doc = createLRQInstrument(instrCreateDoc, mediatorServices,invoiceOidsVector);
			transactionOid = doc.getAttribute("/transactionOid");
		    	if(StringFunction.isNotBlank(transactionOid)){
		    			transaction = (Transaction) mediatorServices.createServerEJB("Transaction",Long.parseLong(transactionOid));
				        instrumentOid = transaction.getAttribute("instrument_oid");
				        LOG.debug("Instrument OID:"+instrumentOid);
				 }
		    	
		    	String completeInstrId = "";
		    		
		    	if(StringFunction.isNotBlank(instrumentOid)){
			        newInstr = (Instrument) mediatorServices.createServerEJB("Instrument",Long.parseLong(instrumentOid));
			        completeInstrId = newInstr.getAttribute("complete_instrument_id");
			        if (StringFunction.isNotBlank(importIndicator)){
			        	newInstr.setAttribute("import_indicator", importIndicator);
			        }

			        LOG.debug("Instrument Num:"+completeInstrId);
		    	}
		    	
		    	Vector invoiceIDVector = new Vector();
		    	boolean success = true;
		    	
		    	terms = (Terms) transaction.getComponentHandle("CustomerEnteredTerms");
		        if (StringFunction.isNotBlank(financeType)){
		        	terms.setAttribute("finance_type", financeType);
		        }
		        if (StringFunction.isNotBlank(buyerBacked)){
		        	terms.setAttribute("financing_backed_by_buyer_ind", buyerBacked);
		        }
		    	
				ArMatchingRule rule = null;
				String tempDate = "";
				String tpName="";
				InvoicesSummaryData invoice =
					(InvoicesSummaryData) mediatorServices.createServerEJB("InvoicesSummaryData",
							Long.parseLong((String)invoiceOidsVector.get(0)));
				
				 tempDate = StringFunction.isNotBlank(invoice
						.getAttribute("payment_date")) ? invoice
						.getAttribute("payment_date") : invoice.getAttribute("due_date");
				try(Connection con = DatabaseQueryBean.connect(false);
						PreparedStatement pStmt1 = con
								.prepareStatement("UPDATE INVOICES_SUMMARY_DATA  SET INVOICE_STATUS=?, A_INSTRUMENT_OID=?, A_TRANSACTION_OID=?, "
										+ "A_TERMS_OID=?, a_invoice_group_oid=?, status=?,invoice_finance_amount=?  where upload_invoice_oid =?");//Rpasupulati IR T36000031466 adding invoice_finance_amount
						PreparedStatement pStmt2 = con
										.prepareStatement("INSERT into INVOICE_HISTORY (INVOICE_HISTORY_OID, ACTION_DATETIME, ACTION, "
								+ "INVOICE_STATUS, P_UPLOAD_INVOICE_OID, A_USER_OID) VALUES (?,?,?,?,?,?)")) {
					
					con.setAutoCommit(false);
					
					for (int i = 0; i < invoiceOidsVector.size(); i++) {
					uploadInvOid =  (String) invoiceOidsVector.get(i);	
					
					InvoicesSummaryData inv =
							(InvoicesSummaryData) mediatorServices.createServerEJB("InvoicesSummaryData",
									Long.parseLong(uploadInvOid));
					invoiceIDVector.addElement(inv.getAttribute("invoice_id"));
					inv.setAttribute("status", TradePortalConstants.PO_STATUS_ASSIGNED);
					inv.setAttribute("invoice_status", TradePortalConstants.PO_STATUS_ASSIGNED);
					//Rpasupulati IR T36000031466 start
					String financeAmt = null;
					if (TradePortalConstants.TRADE_LOAN_REC.equals(financeType)){
				    	CorporateOrganization corpOrg = (CorporateOrganization) mediatorServices.createServerEJB("CorporateOrganization");
				    	corpOrg.getData(Long.parseLong(corpOrgOid));
				    	String finPercent = corpOrg.getAttribute("inv_percent_to_fin_trade_loan");
				    	BigDecimal percentFinanceAllowed = StringFunction.isBlank(finPercent) ? BigDecimal.ZERO :
							new BigDecimal(finPercent).divide(new BigDecimal("100.0"),
									MathContext.DECIMAL64);

				    	finPercent = inv.getAttribute("amount");
						BigDecimal invoiceAmount = StringFunction.isBlank(finPercent) ? BigDecimal.ZERO :
							new BigDecimal(finPercent, MathContext.DECIMAL64);

						BigDecimal financeAmount = invoiceAmount.multiply(percentFinanceAllowed);
						financeAmt = financeAmount.toString();
				    	inv.setAttribute("invoice_finance_amount",financeAmount.toString());
				    	
					}
					//Rpasupulati IR T36000031466 end
					if(TradePortalConstants.INVOICE_TYPE_REC_MGMT.equals(inv.getAttribute("invoice_classification"))){
						 rule = inv.getMatchingRule();
						 }
					  tpName=inv.getTpRuleName(rule);//rule = invoice.getMatchingRule();
					inv.setAttribute("tp_rule_name",tpName);
					inv.verifyInvoiceGroup();
					newGroupOid = inv.getAttribute("invoice_group_oid");
					inv=null; 
					pStmt1.setString(1, TradePortalConstants.PO_STATUS_ASSIGNED);
					pStmt1.setString(2,instrumentOid);
					pStmt1.setString(3,transactionOid);
					pStmt1.setString(4,String.valueOf(terms.getAttribute("terms_oid")));
					pStmt1.setString(5,newGroupOid);
					pStmt1.setString(6,TradePortalConstants.PO_STATUS_ASSIGNED);
					pStmt1.setString(7,financeAmt );//Rpasupulati IR T36000031466
					pStmt1.setString(8,uploadInvOid);
					pStmt1.addBatch();
					pStmt1.clearParameters();
					InvoiceUtility.createPurchaseOrderHisotryLog(
							pStmt2,
							TradePortalConstants.PAYABLE_UPLOAD_INV_ACTION_LINK_INST,
							TradePortalConstants.PAYABLE_UPLOAD_INV_STATUS_ASSIGNED,
							(String) invoiceOidsVector.elementAt(i), userOid);
				}
					pStmt1.executeBatch();
					pStmt2.executeBatch();
					con.commit();
				} catch (AmsException e) {
				e.printStackTrace();
			} catch (SQLException e) {
				 success = false;
				e.printStackTrace();
			}
		    	
		   			
		   			// Success message to User
				if(success){
					if(newGroupOid!=null && !firstInvoiceGroupOid.equals(newGroupOid)){
	 					
	  					updateAttachmentDetails(newGroupOid,mediatorServices);
				}
		   			mediatorServices.getErrorManager().issueError(PayableInvoiceGroupMediator.class.getName(),
							TradePortalConstants.INSERT_SUCCESSFUL,
							mediatorServices.getResourceManager().getText("UploadInvoices.LRQMessage", TradePortalConstants.TEXT_BUNDLE) +" - " +completeInstrId);
		    	   }else
		    	   {
		    		   mediatorServices.getErrorManager().issueError(InvoiceGroupMediator.class.getName(),
								TradePortalConstants.PAYABLE_INV_ATP_FAILED);
		    	   }
				//IR T36000033328 Rpasupulati start.
				TermsParty termsParty = null;
				 if(StringFunction.isNotBlank(terms.getAttribute("c_FirstTermsParty"))){
			        	termsParty = (TermsParty) terms.getComponentHandle("FirstTermsParty");
			        }
			        if(StringFunction.isNotBlank("c_SecondTermsParty")){
			        	termsParty = (TermsParty) terms.getComponentHandle("SecondTermsParty");	
			        }
			      //IR T36000033328 Rpasupulati end.
		   		
		  		LOG.debug("termsParty handle acquired -> "
		  				+ termsParty.getAttribute("terms_party_oid"));
	    		InvoiceUtility.deriveTransactionCurrencyAndAmountsFromInvoice(
		  				transaction, terms, invoiceOidsVector, newInstr,invoiceIDVector,"");
		  		terms.setAttribute("invoice_only_ind",
		  				TradePortalConstants.INDICATOR_YES);
		  		SimpleDateFormat jPylon = new SimpleDateFormat("MM/dd/yyyy");
		  		
		  		try {
		  			if (!StringFunction.isBlank(tempDate)) {
		  				Date date = jPylon.parse(tempDate);
		  				terms.setAttribute("invoice_due_date",
		  						DateTimeUtility.convertDateToDateTimeString(date));
		  			}
		  		} catch (Exception e) {
		  			e.printStackTrace();
		  		}
				if(!(TradePortalConstants.EXPORT_FIN.equals(invoice.getAttribute("loan_type")) 
						|| TradePortalConstants.SELLER_REQUESTED_FINANCING.equals(financeType))){
					termsParty.setAttribute("name", tpName);
				if(rule!=null){
					termsParty.setAttribute("address_line_1", rule.getAttribute("address_line_1"));
					termsParty.setAttribute("address_line_2",rule.getAttribute("address_line_2"));
					termsParty.setAttribute("address_city",rule.getAttribute("address_city"));
					termsParty.setAttribute("address_state_province",rule.getAttribute("address_state"));
					termsParty.setAttribute("address_country",rule.getAttribute("address_country"));
					termsParty.setAttribute("address_postal_code",rule.getAttribute("postalcode"));
				}
				}
				//IR T36000033328 Rpasupulati Ends.
		        if (TradePortalConstants.TRADE_LOAN_REC.equals(financeType)){
			    	CorporateOrganization corpOrg = (CorporateOrganization) mediatorServices.createServerEJB("CorporateOrganization");
			    	corpOrg.getData(Long.parseLong(corpOrgOid));
			    	String attributeValue = corpOrg.getAttribute("inv_percent_to_fin_trade_loan");
			    	
			    	BigDecimal finPCTValue = BigDecimal.ZERO;
			    	if ((attributeValue != null)&&(!attributeValue.equals(""))) {
			    		finPCTValue = (new BigDecimal(attributeValue)).divide(new BigDecimal(100.00));
			    	}
			    	if (finPCTValue.compareTo(BigDecimal.ZERO) != 0){
			    		BigDecimal tempAmount = BigDecimal.ZERO;
			    		BigDecimal termAmount = terms.getAttributeDecimal("amount") ;
			    		tempAmount = finPCTValue.multiply(termAmount);
						String trnxCurrencyCode = transaction.getAttribute("copy_of_currency_code");
						int numberOfCurrencyPlaces = 2;

						if (trnxCurrencyCode != null && !trnxCurrencyCode.equals("")) {
							numberOfCurrencyPlaces = Integer.parseInt(ReferenceDataManager
									.getRefDataMgr().getAdditionalValue(
											TradePortalConstants.CURRENCY_CODE,
											trnxCurrencyCode));
						}
						tempAmount = tempAmount.setScale(numberOfCurrencyPlaces,
								BigDecimal.ROUND_HALF_UP);
			    		terms.setAttribute("amount", String.valueOf(tempAmount));
			    	}
			    
			    	
		        }
		  		// Set the counterparty for the instrument and some "copy of" fields
		  		newInstr.setAttribute("a_counter_party_oid",
		  				terms.getAttribute("c_FirstTermsParty"));
		  		transaction
		  				.setAttribute("copy_of_amount", terms.getAttribute("amount"));
		  		transaction.setAttribute("instrument_amount",
		  				terms.getAttribute("amount"));
		  		newInstr.setAttribute("copy_of_instrument_amount",
		  				transaction.getAttribute("instrument_amount"));

		  		if (terms.isAttributeRegistered("expiry_date")) {
		  			newInstr.setAttribute("copy_of_expiry_date",
		  					terms.getAttribute("expiry_date"));
		  		}

		  		if (terms.isAttributeRegistered("reference_number")) {
		  			newInstr.setAttribute("copy_of_ref_num",
		  					terms.getAttribute("reference_number"));
		  		}
		  		
				String requiredOid = "upload_invoice_oid";
				 String invoiceOidsSql = POLineItemUtility.getPOLineItemOidsSql(
						 invoiceOidsVector, requiredOid);		  		
				if (TradePortalConstants.INDICATOR_T.equals(newInstr.getAttribute("import_indicator")) && 
						TradePortalConstants.TRADE_LOAN_PAY.equals(terms.getAttribute("finance_type"))){
					String payMethodSQL = "select distinct(PAY_METHOD) PAYMETHOD from invoices_summary_data where "+invoiceOidsSql;
					DocumentHandler resultDoc = DatabaseQueryBean.getXmlResultSet(payMethodSQL,false,new ArrayList<Object>());
					if (resultDoc != null){
						String payMethod = resultDoc.getAttribute("/ResultSetRecord(0)/PAYMETHOD");
						if (StringFunction.isNotBlank(payMethod)){
							terms.setAttribute("payment_method",payMethod);
							int singleBeneCount = InvoiceUtility.isSingleBeneficairyInvoice(invoiceOidsSql);
							if (singleBeneCount == 1){
								terms.setAttribute("loan_proceeds_credit_type",TradePortalConstants.CREDIT_BEN_ACCT);
								InvoiceUtility.populateTerms(terms,(String)invoiceOidsVector.get(0),invoiceOidsVector.size(), mediatorServices);
							}else if (singleBeneCount > 1){
								terms.setAttribute("loan_proceeds_credit_type",TradePortalConstants.CREDIT_MULTI_BEN_ACCT);
								//IR 29777 start
								if(StringFunction.isBlank(terms.getAttribute("c_FirstTermsParty")))
					            {
					                 terms.newComponent("FirstTermsParty");
					            }
								TermsParty firstTermsParty = (TermsParty) terms.getComponentHandle("FirstTermsParty");;
								firstTermsParty.setAttribute("terms_party_type", TermsPartyType.BENEFICIARY);

								if(!TradePortalConstants.EXPORT_FIN.equals(invoice.getAttribute("loan_type")))
								firstTermsParty.setAttribute("name", mediatorServices.getResourceManager().getText("Common.MultBenesEntered", TradePortalConstants.TEXT_BUNDLE));
								//IR 29777 end
								
								int paymentChargeCount=InvoiceUtility.isSinglePaymentCharge(invoiceOidsSql);
								if (paymentChargeCount == 1){
									
									terms.setAttribute("bank_charges_type",invoice.getAttribute("charges"));
								}
								else if (paymentChargeCount > 1){
									terms.setAttribute("bank_charges_type",TradePortalConstants.CHARGE_UPLOAD_FW_SHARE);
									InvoiceUtility.setBankChargesForInvoices(invoiceOidsSql);
									mediatorServices.getErrorManager().issueError(
									          TradePortalConstants.ERR_CAT_1, TradePortalConstants.INFO_SHARED_BANK_CHARGES);
									 
								}
							}
							
						}
						
					}
				}

				if (TradePortalConstants.INDICATOR_NO.equals(newInstr.getAttribute("import_indicator")) || 
						TradePortalConstants.INDICATOR_X.equals(newInstr.getAttribute("import_indicator"))){
			        
 
					 boolean useSeller = TradePortalConstants.BUYER_REQUESTED_FINANCING.equals(terms.getAttribute("finance_type")) ;
					String invoiceDueDate = null;
					if (InvoiceUtility.validateSameCurrency(invoiceOidsSql) && 
							InvoiceUtility.validateSameDate(invoiceOidsSql) && 
							InvoiceUtility.validateSameTradingPartner(invoiceOidsSql, useSeller)){
						String dueDateSQL = "select distinct(case when payment_date is null then due_date else payment_date end ) DUEDATE "
								+ "from invoices_summary_data where "+invoiceOidsSql;
						DocumentHandler resultDoc = DatabaseQueryBean.getXmlResultSet(dueDateSQL,false,new ArrayList<Object>());
						invoiceDueDate = resultDoc.getAttribute("/ResultSetRecord(0)/DUEDATE");
					}
					

					if (StringFunction.isNotBlank(invoiceDueDate)){

						try {
							SimpleDateFormat iso = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
							SimpleDateFormat jPylon1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
							
								Date date = jPylon1.parse(invoiceDueDate);

								terms.setAttribute("loan_terms_fixed_maturity_dt",TPDateTimeUtility.convertISODateToJPylonDate(iso.format(date)));
								
						} catch (Exception e) {
							e.printStackTrace();
						}

						
					}
				}


		  		transaction.save(false);
		  		LOG.debug("Saving instrument with oid '"
		  				+ newInstr.getAttribute("instrument_oid") + "'...");
		  		newInstr.save(false);// save without validate
		  		LOG.debug("Instrument saved");
			  }
			
			LOG.debug("InvoiceGroupMediatorBean.createLoanRequest: End: ");
			return new DocumentHandler();
			
		}


/**
 * This method retrives details for selected invoices
 * @param invoiceOids
 * @param corpOrgOid
 * @return
 * @throws AmsException
 * @throws RemoteException
 */
public Vector getInvoicesDetail(String[] invoiceOids,String corpOrgOid,String fromPayable) throws AmsException, RemoteException {				

	
	String invGroupSql = null;
	StringBuilder whereClause = new StringBuilder(200);
	Vector rows = null;
	if(invoiceOids!=null && invoiceOids.length>0){
        for (String invoiceOid : invoiceOids) {
            whereClause.append(invoiceOid + ", ");
		}
	}
	String finalWhereClause = "";
	if(whereClause.toString().length()>0){
		finalWhereClause = whereClause.toString();
		finalWhereClause = whereClause.toString().substring(0,whereClause.toString().length()-2);
	}
	
	//To handle Payables
	if(!StringFunction.isBlank(fromPayable) && TradePortalConstants.PAYBLE_INVOICE_PAGE.equals(fromPayable)){
	invGroupSql = " select UPLOAD_INVOICE_OID,SELLER_NAME TP_NAME,CURRENCY,AMOUNT,DUE_DATE,PAYMENT_DATE,INVOICE_ID,A_INSTRUMENT_OID,A_TRANSACTION_OID," +
				" INVOICE_STATUS,A_CORP_ORG_OID,LOAN_TYPE,LINKED_TO_INSTRUMENT_TYPE "
				+ "from invoices_summary_data where UPLOAD_INVOICE_OID in ("+finalWhereClause+" ) and a_corp_org_oid=?";
		
	}else //To handle Receivables
	{
	invGroupSql = " select UPLOAD_INVOICE_OID,BUYER_NAME TP_NAME,CURRENCY,AMOUNT,DUE_DATE,PAYMENT_DATE,INVOICE_ID,A_INSTRUMENT_OID,A_TRANSACTION_OID," +
			" INVOICE_STATUS,A_CORP_ORG_OID,LOAN_TYPE,LINKED_TO_INSTRUMENT_TYPE "
			+ "from invoices_summary_data where UPLOAD_INVOICE_OID in ("+finalWhereClause+" ) and a_corp_org_oid=?";
	}
	LOG.debug("invGroupSql->"+invGroupSql);
	
	DocumentHandler	invoicesGroupDoc = DatabaseQueryBean.getXmlResultSet(invGroupSql, false, new Object[]{corpOrgOid}); 
	if(invoicesGroupDoc!=null)
	  rows = invoicesGroupDoc.getFragments("/ResultSetRecord");
	return rows;	
}

		 /**
		  * This method invokes the Instrument EJB.
		  * successful creation of a new Instrument returns a transction_oid
		  * @param inputDoc
		  * @param mediatorServices
		  * @return String
		  * @throws AmsException
		  * @throws RemoteException
		  */
		 private DocumentHandler createLRQInstrument(DocumentHandler inputDoc, MediatorServices mediatorServices,Vector invOids)
			      throws AmsException, RemoteException {			
			 	 LOG.debug("createLRQInstrument()->"+inputDoc+"\t:"+invOids);
			 	 newInstr = (Instrument) mediatorServices.createServerEJB("Instrument");	
			     DocumentHandler outputDoc = new DocumentHandler();

			        String benName              = inputDoc.getAttribute("/benName"); //the ben_name of the invoice line items
			        String currency             = inputDoc.getAttribute("/currency"); //the currency of the invoice line items
			        String newTransactionOid    = null; //the oid of the transaction created for these invoice line items
			        
			        //Create a new instrument
			        newTransactionOid = newInstr.createNew(inputDoc);
			        LOG.debug("New instrument created with an original transacion oid of -> " + newTransactionOid);

			        if (newTransactionOid == null)
			        {
			        	LOG.debug("newTransactionOid is null.  Error in this.createNew()");
			            return outputDoc; //outputDoc is null
			        }
			        else
			            outputDoc.setAttribute("/transactionOid", newTransactionOid);

			        LOG.debug("ben_name     = " + benName);
			        LOG.debug("currency     = " + currency);

			        /************************************************************************
			         * Instantiate the transaction, terms, and termsParty and set the fields
			         * derived from the po line item data
			         ************************************************************************/

			        //get a handle to the terms object through the transaction then set the values on terms
			        //we'll also have to create a termsParty object and associate it to the terms object
			        Transaction transaction = (Transaction) newInstr.getComponentHandle("TransactionList",
			                Long.parseLong(newTransactionOid));
			        LOG.debug("Transaction handle acquired -> " + transaction.getAttribute("transaction_oid"));

			       
			        try
			        {
			            terms = (Terms) transaction.getComponentHandle("CustomerEnteredTerms");
			            LOG.debug("terms handle acquired -> " + terms.getAttribute("terms_oid"));
			            outputDoc.setAttribute("/termsOid", terms.getAttribute("terms_oid"));
			        }
			        catch (Exception e)
			        {
			            LOG.debug("Caught error getting handle to terms");
			            e.printStackTrace();
			        }
			      //IR T36000033328 Rpasupulati start.
			        String firstParty = terms.getAttribute("c_FirstTermsParty");
			        String secondParty = terms.getAttribute("c_SecondTermsParty");
			        TermsParty termsParty = null;
			        if(StringFunction.isNotBlank(firstParty)){
			        	termsParty = (TermsParty) terms.getComponentHandle("FirstTermsParty");
			        }
			        if(StringFunction.isNotBlank(secondParty)){
			        	termsParty = (TermsParty) terms.getComponentHandle("SecondTermsParty");	
			        }
			      //IR T36000033328 Rpasupulati Ends.
			        LOG.debug("termsParty handle acquired -> " + termsParty.getAttribute("terms_party_oid"));

			        return outputDoc;
			  
		 }
	
	private class InstrumentProperties {
		/**
		 * This class holds the properties to be set for Instrument EJB
		 */
		private String corporateOrgOid = null;
		private String securityRights = null;
		private String userOid = null;
		private String benName = null;
		private String currency = null;
		private BigDecimal amount = null;
		private String shipDate = null;
		private String instrumentOid = null;
		private String opOrgOid = null;
		private String clientBankOid = null;
		private String fromPayables = null;
		private MediatorServices mediatorServices = null;
		private String userLocale = null;
		private String timeZone = null;
		private String usePaymentDate = null;
		private String transactionOid = null;
		private String termsOid = null;
		private String thold =null;
		private String dlimit =null;
		private String authType =null;
		BigInteger securityAccess = null;
		
		public BigInteger getSecurityAccess() {
			return securityAccess;
		}

		public void setSecurityAccess(BigInteger securityAccess) {
			this.securityAccess = securityAccess;
		}

		public String getAuthType() {
			return authType;
		}

		public void setAuthType(String authType) {
			this.authType = authType;
		}

		public String getThold() {
			return thold;
		}

		public void setThold(String thold) {
			this.thold = thold;
		}

		public String getDlimit() {
			return dlimit;
		}

		public void setDlimit(String dlimit) {
			this.dlimit = dlimit;
		}

		public String getTermsOid() {
			return termsOid;
		}

		public void setTermsOid(String termsOid) {
			this.termsOid = termsOid;
		}

		public String getTransactionOid() {
			return transactionOid;
		}

		public void setTransactionOid(String transactionOid) {
			this.transactionOid = transactionOid;
		}

		public String getFromPayables() {
			return fromPayables;
		}

		public void setFromPayables(String fromPayables) {
			this.fromPayables = fromPayables;
		}

		public String getClientBankOid() {
			return clientBankOid;
		}

		public void setClientBankOid(String clientBankOid) {
			this.clientBankOid = clientBankOid;
		}

		public String getOpOrgOid() {
			return opOrgOid;
		}

		public void setOpOrgOid(String opOrgOid) {
			this.opOrgOid = opOrgOid;
		}

		public String getInstrumentOid() {
			return instrumentOid;
		}

		public void setInstrumentOid(String instrumentOid) {
			this.instrumentOid = instrumentOid;
		}

		public String getCorporateOrgOid() {
			return corporateOrgOid;
		}

		public void setCorporateOrgOid(String corporateOrgOid) {
			this.corporateOrgOid = corporateOrgOid;
		}

		public String getSecurityRights() {
			return securityRights;
		}

		public void setSecurityRights(String securityRights) {
			this.securityRights = securityRights;
		}

		public String getUserOid() {
			return userOid;
		}

		public void setUserOid(String userOid) {
			this.userOid = userOid;
		}

		public String getBenName() {
			return benName;
		}

		public void setBenName(String benName) {
			this.benName = benName;
		}

		public String getCurrency() {
			return currency;
		}

		public void setCurrency(String currency) {
			this.currency = currency;
		}

		public BigDecimal getAmount() {
			return amount;
		}

		public void setAmount(BigDecimal amount) {
			this.amount = amount;
		}

		public String getShipDate() {
			return shipDate;
		}

		public void setShipDate(String shipDate) {
			this.shipDate = shipDate;
		}

		public String getUserLocale() {
			return userLocale;
		}

		public void setUserLocale(String userLocale) {
			this.userLocale = userLocale;
		}

		public String getTimeZone() {
			return timeZone;
		}

		public void setTimeZone(String timeZone) {
			this.timeZone = timeZone;
		}

		public MediatorServices getMediatorServices() {
			return mediatorServices;
		}

		public void setMediatorServices(MediatorServices mediatorServices) {
			this.mediatorServices = mediatorServices;
		}

		public String getUsePaymentDate() {
			return usePaymentDate;
		}

		public void setUsePaymentDate(String usePaymentDate) {
			this.usePaymentDate = usePaymentDate;
		}
	}

		/**
		 * This method loops through the invoices of a selected invoice group
		 * and validate based on Seller name/Currency name/Amount
		 * @param instrumentProp
		 * @param invGroupList
		 * @param mediatorServices
		 * @return DocumentHandler
		 * @throws RemoteException
		 * @throws AmsException
		 */
		private DocumentHandler processInvoiceGroups(InstrumentProperties instrumentProp, Vector invGroupList, MediatorServices mediatorServices)
			      throws AmsException {
			   
			   LOG.debug("processInvoiceGroups()-> "+invGroupList);
			   String buyerName = null;
			   String currency =null;
			   String paymentDate = null;		
			   String dueDate = null;
			   String loanType = null;
			   String invalidBuyerName = null;
			   String invalidCurrency = null;
			   String invalidPaymentDate = null;
			   String invalidDueDate = null;				   
			   DocumentHandler invGroupDoc = null;
			   DocumentHandler nxtInvGroupDoc = null;
			   DocumentHandler instrCreateDoc = null;
			   BigDecimal amount = BigDecimal.ZERO;
			   BigDecimal nextAmount = BigDecimal.ZERO;
			   String amt = null;
			   BigDecimal totalAmount = BigDecimal.ZERO;  		  
			   String date = null;
			   String invalidDate = null;
			   if(invGroupList!=null && invGroupList.size()>0){
			   int pos = 0;   		 
					  
					  invGroupDoc = (DocumentHandler) invGroupList.get(pos);
					  buyerName = invGroupDoc.getAttribute("/TP_NAME");
					  currency =  invGroupDoc.getAttribute("/CURRENCY");				
					  paymentDate = invGroupDoc.getAttribute("/PAYMENT_DATE");		
					  dueDate = invGroupDoc.getAttribute("/DUE_DATE");	
					  loanType = invGroupDoc.getAttribute("/LOAN_TYPE");	
					  if(StringFunction.isBlank(paymentDate))
						  date = dueDate; 
					  else
						  date = paymentDate;
					  LOG.debug("date:"+date);
					  amt = invGroupDoc.getAttribute("/AMOUNT");				  
					  if(amt!=null)
						  amount = BigDecimal.valueOf(Double.parseDouble(amt));					 
					  totalAmount = amount;
					  int subIdx = pos+1;

				        while (subIdx < invGroupList.size()) {
				            	
				        		nxtInvGroupDoc = (DocumentHandler) invGroupList.get(subIdx);
				        		 invalidBuyerName = nxtInvGroupDoc.getAttribute("/TP_NAME"); 
				            	invalidCurrency =  nxtInvGroupDoc.getAttribute("/CURRENCY");
				            	invalidPaymentDate = nxtInvGroupDoc.getAttribute("/PAYMENT_DATE");
				            	invalidDueDate = nxtInvGroupDoc.getAttribute("/DUE_DATE");	
								String am =  nxtInvGroupDoc.getAttribute("/AMOUNT");
								if(StringFunction.isBlank(invalidPaymentDate))
									invalidDate = invalidDueDate;
								else
									invalidDate =invalidPaymentDate;
								LOG.debug("invalid Date:"+invalidDate);
								 if(am!=null)
									  nextAmount = BigDecimal.valueOf(Double.parseDouble(am));	
								 LOG.debug("loanType:"+loanType);
								 if(TradePortalConstants.TRADE_LOAN.equals(loanType)){
									  if(!currency.equals(invalidCurrency)){
										 mediatorServices.getErrorManager().issueError(
													TradePortalConstants.ERR_CAT_1,
													TradePortalConstants.INVALID_LOAN_REQ_TRADE_LOAN_GROUP);
											return new DocumentHandler();
									 }
									 else{
											totalAmount = totalAmount.add(nextAmount);
											
									}
								 }else {// if(TradePortalConstants.EXPORT_FIN.equals(loanType) || TradePortalConstants.INV_FIN_REC.equals(loanType)){
									if(!(buyerName.equals(invalidBuyerName) && currency.equals(invalidCurrency) && date.equals(invalidDate))){	
										String errCode = null;
										if(TradePortalConstants.EXPORT_FIN.equals(loanType) || TradePortalConstants.INV_FIN_REC.equals(loanType)){
											errCode = TradePortalConstants.INVALID_LOAN_REQ_EXP_FINANCE_GROUP;
									}
									else if (TradePortalConstants.INV_FIN_BR.equals(loanType) || TradePortalConstants.INV_FIN_BB.equals(loanType)){
										errCode = TradePortalConstants.INVALID_LOAN_REQ_PAYABLE_GROUP;
									}
									 mediatorServices.getErrorManager().issueError(
												TradePortalConstants.ERR_CAT_1,errCode);
										return new DocumentHandler();
								}else{
										totalAmount = totalAmount.add(nextAmount);
										
								}
							  }
								 LOG.info("subIdx ++ "+subIdx);
								 subIdx++;
				            }
				       
					  instrumentProp.setAmount(totalAmount);
					  instrumentProp.setCurrency(currency);
					  instrumentProp.setShipDate(date);
					  instrumentProp.setBenName(buyerName);
					
					  // Build the input document used to create a new instrument
				      instrCreateDoc = buildLoanReqInstrumentCreateDoc(instrumentProp);    
				  
			   }
			   
			return instrCreateDoc;
		  }
		
		/**
		 * This method prepares instrument document which will be input to
		 * Instrument EJB for creation a new ATP instrument
		 * @param instrumentProp
		 * @return DocumentHandler
		 * @throws AmsException
		 */
		private DocumentHandler buildLoanReqInstrumentCreateDoc(InstrumentProperties instrumentProp) {
			 	
				
			    DocumentHandler instrCreateDoc = new DocumentHandler();			   
			    instrCreateDoc.setAttribute("/instrumentOid", null); 		   
				instrCreateDoc.setAttribute("/instrumentType",InstrumentType.LOAN_RQST);
				instrCreateDoc.setAttribute("/clientBankOid", instrumentProp.getClientBankOid());
				instrCreateDoc.setAttribute("/bankBranch", instrumentProp.getOpOrgOid());
			    instrCreateDoc.setAttribute("/copyType", TradePortalConstants.FROM_BLANK);		   
			    instrCreateDoc.setAttribute("/userOid", instrumentProp.getUserOid());
			    instrCreateDoc.setAttribute("/ownerOrg", instrumentProp.getCorporateOrgOid());
			    instrCreateDoc.setAttribute("/securityRights", instrumentProp.getSecurityRights());
			    instrCreateDoc.setAttribute("/benName", instrumentProp.getBenName());
			    instrCreateDoc.setAttribute("/currency", instrumentProp.getCurrency());
			    instrCreateDoc.setAttribute("/shipDate", instrumentProp.getShipDate());
			    instrCreateDoc.setAttributeDecimal("/amount", instrumentProp.getAmount());	 
			    LOG.debug("instrCreateDoc:"+instrCreateDoc);
			return instrCreateDoc;
			 
		 }
		
		/**
		 * This method finds the attachments available or not for the new group
		 * and updates the Attachment_Ind for that group respectively.
		 * Use this method for group level
		 * @param newGroupOid
		 * @param mediatorServices
		 * @return
		 * @throws AmsException
		 * @throws RemoteException
		 */
		private int updateAttachmentDetails(String newGroupOid, MediatorServices mediatorServices)
				throws AmsException, RemoteException {
			
			
			LOG.debug("updateAttachmentDetails() : newGroupOid:"+newGroupOid);
			int status = 0;
			String attachmentAvailable = TradePortalConstants.INDICATOR_NO;
			String sql = "select i.UPLOAD_INVOICE_OID, d.DOC_IMAGE_OID "
					+ "from INVOICES_SUMMARY_DATA i left outer join DOCUMENT_IMAGE d on i.UPLOAD_INVOICE_OID = d.P_TRANSACTION_OID "
					+ "where i.A_INVOICE_GROUP_OID = ?";
			LOG.debug("newGroupOid->"+newGroupOid+"\tsql:"+sql);
			DocumentHandler	invoiceListDoc = DatabaseQueryBean.getXmlResultSet(sql, false, new Object[]{newGroupOid});
			if (invoiceListDoc != null) {
				Vector<DocumentHandler> invoiceList = invoiceListDoc.getFragments("/ResultSetRecord");
				for (DocumentHandler invoiceDoc : invoiceList) {
					String invoiceOid = invoiceDoc.getAttribute("/UPLOAD_INVOICE_OID");
					String docImageOid = invoiceDoc.getAttribute("/DOC_IMAGE_OID");
					LOG.debug("invoiceOid:"+invoiceOid+"\tdocImageOid:"+docImageOid);
					if (StringFunction.isNotBlank(invoiceOid) &&
							StringFunction.isNotBlank(docImageOid)) { 	
						attachmentAvailable =  TradePortalConstants.INDICATOR_YES;
						break;
					}
				}
				LOG.debug("attachmentAvailable:"+attachmentAvailable);
				   InvoiceGroup invoiceGroup = (InvoiceGroup) mediatorServices.createServerEJB("InvoiceGroup",
								Long.parseLong(newGroupOid));	
					invoiceGroup.setAttribute("attachment_ind", attachmentAvailable);
					status = invoiceGroup.save();
					LOG.debug("Updated status:"+status);	
				
			}
			return status;
		}
		
		

	 public void updateAttachmentDetails(Vector groupOids,Vector oidsList, MediatorServices mediatorSer) throws AmsException{
				
			
				LOG.debug("updateAttachmentDetails() : groupOid:"+groupOids +"\t oidsList:"+oidsList);		
					
				Vector removeList = new Vector();
				Vector addList = new Vector();
				if(oidsList!=null && oidsList.size()>0){
					Iterator it = oidsList.iterator();
					while(it.hasNext()){
						groupOids.add(it.next());
					}
				}
				LOG.debug("groupOids:"+groupOids);
				
				String groupSearchSQL = "select d.DOC_IMAGE_OID "
						+ "from INVOICES_SUMMARY_DATA i left outer join DOCUMENT_IMAGE d on i.UPLOAD_INVOICE_OID = d.P_TRANSACTION_OID "
						+ "where i.A_INVOICE_GROUP_OID = ?";
         for (Object groupOid1 : groupOids) {
             String groupOid = (String) groupOid1;
					
					LOG.debug("groupSearchSQL:"+groupSearchSQL+groupOid);
					DocumentHandler oldGroupDoc = DatabaseQueryBean.getXmlResultSet(groupSearchSQL, false, new Object[]{groupOid});	
					LOG.debug("oldGroupDoc:"+oldGroupDoc);
					Vector oldGroupList = null;
					 //Searching for image_id in document_image table if any attachments available for exisitng group oid
					if(oldGroupDoc!=null){
						oldGroupList = oldGroupDoc.getFragments("/ResultSetRecord");
						LOG.debug("GroupList:"+oldGroupList);
						if(oldGroupList!=null && oldGroupList.size()>=1){
							addList.add(groupOid);
						}
					}else{
							removeList.add(groupOid);
						}
				}
					
				LOG.debug("before entering in to old sql :"+removeList);
				//Updating the group when attachment founds with NO
				String removeSQL = "UPDATE invoice_group SET attachment_ind = ? WHERE ";
				if(removeList.size()>0){
					List<Object> sqlprmLst = new ArrayList<Object>();
					sqlprmLst.add(TradePortalConstants.INDICATOR_NO);
			        removeSQL+= POLineItemUtility.getInvoiceItemOidsSqlWithPlaceHolder(removeList, "invoice_group_oid",sqlprmLst);
			
				try {
					
					LOG.debug("updateSQL:"+removeSQL.toString());
					int resultCount = DatabaseQueryBean.executeUpdate(removeSQL, false, sqlprmLst);
					LOG.debug("resultCount:"+resultCount);
					
				} catch (SQLException e) {
					LOG.debug("exception while updating attach ind"+e.getMessage());
					e.printStackTrace();
					}		
				}			
				LOG.debug("before entering in to new sql :->"+addList);
				//Updating the group when attachment founds with YES
				String addSQL = "UPDATE invoice_group SET attachment_ind = ? WHERE ";
				if(addList.size()>0){
					List<Object> sqlprmLst = new ArrayList<Object>();
					sqlprmLst.add(TradePortalConstants.INDICATOR_YES);
			        addSQL+= POLineItemUtility.getInvoiceItemOidsSqlWithPlaceHolder(addList, "invoice_group_oid",sqlprmLst);
				
				try {
				
					LOG.debug("addSQL:"+addSQL.toString());
					int resultCount = DatabaseQueryBean.executeUpdate(addSQL, false, sqlprmLst);
					LOG.debug("resultCount:"+resultCount);
				
				} catch (SQLException e) {
					LOG.debug("exception while updating attach ind"+e.getMessage());
					e.printStackTrace();
					}		
				}			

				
			}
	 
	
	 private DocumentHandler performNotifyPanelAuthUser(DocumentHandler inputDoc, MediatorServices mediatorServices)
		throws RemoteException, AmsException {

			LOG.debug("UploadInvoiceMediatorBean:::performNotifyPanelAuthUser");
			String invoiceSummaryOid = inputDoc.getAttribute("/InvoiceSummary/invoiceSummaryOid");
			 
	    	 // create email for all selected users to notification
		    Vector<DocumentHandler> beneficiaryDetailsList = inputDoc.getFragments("/EmailList/user");
		    StringBuilder emailReceivers = new StringBuilder();
		    String userOid = null;
		    String ownerOrgOid = null;
		    DocumentHandler outputDoc = new DocumentHandler();
		    for(DocumentHandler doc : beneficiaryDetailsList){
			    userOid = doc.getAttribute("/userOid");
				User user = (User) mediatorServices.createServerEJB("User", Long.parseLong(userOid));
				String userMailID = user.getAttribute("email_addr");
				ownerOrgOid = user.getAttribute("owner_org_oid");
				if(StringFunction.isNotBlank(userMailID)){
					emailReceivers.append(userMailID + ",");
				}
		    }

		    if(StringFunction.isNotBlank(emailReceivers.toString())){
		      		// Get the data from transaction that will be used to build the email message
					String amount = "";
					String currency = "";
					if (StringFunction.isNotBlank(invoiceSummaryOid)) {
						InvoicesSummaryData invoicesSummaryData =(InvoicesSummaryData) mediatorServices.createServerEJB("InvoicesSummaryData", Long.parseLong(invoiceSummaryOid));
						currency = invoicesSummaryData.getAttribute("currency");
				        amount = invoicesSummaryData.getAttribute("amount");
				        amount = TPCurrencyUtility.getDisplayAmount(amount, currency, mediatorServices.getResourceManager().getResourceLocale());
					}

					StringBuilder emailContent = new StringBuilder();
					  
					 emailContent.append(mediatorServices.getResourceManager().getText("EmailTrigger.PendingPanelBodyInvoice", TradePortalConstants.TEXT_BUNDLE));
					 emailContent.append(TradePortalConstants.NEW_LINE);
					// Include PayRemit Amount and currency
					if (StringFunction.isNotBlank(amount)) {
						emailContent.append(mediatorServices.getResourceManager().getText("EmailTrigger.Amount",TradePortalConstants.TEXT_BUNDLE)+ " ");
						emailContent.append(currency+" "+amount);
						emailContent.append(TradePortalConstants.NEW_LINE);
					}

					emailContent.append(TradePortalConstants.NEW_LINE);
					// call generic method to trigger email
					InvoiceUtility.performNotifyPanelAuthUser(ownerOrgOid, emailContent, emailReceivers, mediatorServices);
					 }
	        
			return outputDoc;

		}
		/**
		 * 
		 * @param inputDoc
		 * @param mediatorServices
		 * @param buttonPressed
		 * @return
		 * @throws RemoteException
		 * @throws AmsException
		 * Modifies Supplier Date or Early Payment Amount
		 */
		private DocumentHandler performModifyInvoiceData(DocumentHandler inputDoc, MediatorServices mediatorServices, String buttonPressed) throws AmsException,
				NumberFormatException, RemoteException {

			String modifyAttr = null;
			String modifyVal = null;
			String buttonTextVal = null;
			String blankMsgTextVal = null;
			String successMsgVal = null;
			Vector<DocumentHandler> invoiceList = inputDoc.getFragments("/UploadInvoiceList/InvoicesSummaryData");
			int count = 0;
			DocumentHandler invoiceItem = null;
			String invoiceOid = null;
			String optLock = null;
			count = invoiceList.size();
			String invAction ="";
			String securityRights	  = inputDoc.getAttribute("/UploadInvoiceList/securityRights");
			//Set attributes based on buttonPressed
			if(TradePortalConstants.PAY_INV_BUTTON_MODIFY_SUPP_DATE.equals(buttonPressed)) {
				if (!SecurityAccess.hasRights(securityRights, SecurityAccess.UPLOAD_PAY_MGM_APPLY_SUPPLIER_DATE)) {
					// user does not have appropriate authority.
					mediatorServices.getErrorManager().issueError(
							TradePortalConstants.ERR_CAT_1,
							TradePortalConstants.NO_ACTION_MATCH_NOTICE_AUTH,
							mediatorServices.getResourceManager().getText(
									"SecurityProfileDetail.ApplySendtoSuppDate",
									TradePortalConstants.TEXT_BUNDLE));
					return new DocumentHandler();
				}
				modifyAttr = "send_to_supplier_date";
				modifyVal = inputDoc.getAttribute("/UploadInvoiceList/supplier_date");
				blankMsgTextVal = mediatorServices.getResourceManager().getText("InvoiceUploadRequest.supplier_date",TradePortalConstants.TEXT_BUNDLE);
				buttonTextVal = mediatorServices.getResourceManager().getText("UploadInvoices.InvPayModifySuppDate",TradePortalConstants.TEXT_BUNDLE);
				successMsgVal = TradePortalConstants.PAY_PRGM_MODIFY_SUPP_DATE;
				invAction = TradePortalConstants.UPLOAD_INV_ACTION_MODIFY_SUPPLIER_DATE;
			}else if(TradePortalConstants.PAY_INV_BUTTON_APPLY_PAY_AMT.equals(buttonPressed) || TradePortalConstants.PAY_INV_LIST_BUTTON_APPLY_PAY_AMT.equals(buttonPressed)) {
				if (!SecurityAccess.hasRights(securityRights, SecurityAccess.UPLOAD_PAY_MGM_APPLY_EARLY_PAY_AMOUNT)) {
					// user does not have appropriate authority.
					mediatorServices.getErrorManager().issueError(
							TradePortalConstants.ERR_CAT_1,
							TradePortalConstants.NO_ACTION_MATCH_NOTICE_AUTH,
							mediatorServices.getResourceManager().getText(
									"SecurityProfileDetail.ApplyEarlyPayAmt",
									TradePortalConstants.TEXT_BUNDLE));
					return new DocumentHandler();
				}
				modifyAttr = "payment_amount";
				modifyVal = inputDoc.getAttribute("/UploadInvoiceList/payment_amount");
				blankMsgTextVal = mediatorServices.getResourceManager().getText("UploadInvoices.EarlyPaymentAmt",TradePortalConstants.TEXT_BUNDLE);
				buttonTextVal = mediatorServices.getResourceManager().getText("UploadInvoices.InvPayApplyEarlyAmt",TradePortalConstants.TEXT_BUNDLE);
				successMsgVal = TradePortalConstants.PAY_PRGM_APPLY_PAYMENT_AMT;
				invAction = TradePortalConstants.UPLOAD_INV_ACTION_APPLY_PAYMENT_AMT;
			}
			else if(TradePortalConstants.PAY_PGM_BUTTON_APPLY_PAYMENT_DATE.equals(buttonPressed)) {
				if (!SecurityAccess.hasRights(securityRights, SecurityAccess.PAYABLES_APPLY_PAYDATE)) {
					// user does not have appropriate authority.
					mediatorServices.getErrorManager().issueError(
							TradePortalConstants.ERR_CAT_1,
							TradePortalConstants.NO_ACTION_MATCH_NOTICE_AUTH,
							mediatorServices.getResourceManager().getText(
									"UploadInvoiceAction.ApplyPaymentDate",
									TradePortalConstants.TEXT_BUNDLE));
					return new DocumentHandler();
				}
				modifyAttr = "payment_date";
				modifyVal = inputDoc.getAttribute("/UploadInvoiceList/payment_date");
				blankMsgTextVal = mediatorServices.getResourceManager().getText("UploadInvoices.PaymentDate",TradePortalConstants.TEXT_BUNDLE);
				buttonTextVal = mediatorServices.getResourceManager().getText("UploadInvoices.InvPayApplyPaymentDate",TradePortalConstants.TEXT_BUNDLE);
				successMsgVal = TradePortalConstants.PAY_PRGM_MODIFY_PAYMENT_DATE;
				invAction = TradePortalConstants.UPLOAD_INV_ACTION_APPLY_PAYMENT_DATE;
			}
			mediatorServices.debug("InvoiceMediator: number of invoices selected: " + count);
			invoiceItem = invoiceList.get(0);
			//Check if the value is blank
			if(StringFunction.isBlank(modifyVal)){

				mediatorServices.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						AmsConstants.REQUIRED_ATTRIBUTE, blankMsgTextVal);
				return new DocumentHandler();
			}
			
			//Validate supplier date, in case of 'Modify Supplier Date'
			User user = (User) mediatorServices.createServerEJB("User", Long.parseLong(inputDoc.getAttribute("/UploadInvoiceList/userOid")));
			String datePattern = user.getAttribute("datepattern");
			if(TradePortalConstants.PAY_INV_BUTTON_MODIFY_SUPP_DATE.equals(buttonPressed)
					|| TradePortalConstants.PAY_PGM_BUTTON_APPLY_PAYMENT_DATE.equals(buttonPressed)) {
				try{
	
				    SimpleDateFormat formatter = new SimpleDateFormat(datePattern);
					SimpleDateFormat reqformatter = new SimpleDateFormat("MM/dd/yyyy");
				    Date formatSupplierDate   =  formatter.parse(modifyVal);
				    modifyVal = reqformatter.format(formatSupplierDate);
				}catch(Exception e){
					mediatorServices.getErrorManager().issueError(
							TradePortalConstants.ERR_CAT_1,
							TradePortalConstants.WEBI_INVALID_DATE_ERROR);
					return new DocumentHandler();
				}
			}
			
			//Loop through the invoices to apply the modifies value and save.
			for (int i = 0; i < count; i++) 
		    {
			  invoiceItem = invoiceList.get(i);
			  String[] invoiceData = invoiceItem.getAttribute("/InvoiceData").split("/");
			  invoiceOid = invoiceData[0];
			  optLock = invoiceData[1];
			  try
			   {
			      if (StringFunction.isNotBlank(invoiceOid)) {
						InvoicesSummaryData invoiceSummaryData = (InvoicesSummaryData) mediatorServices.createServerEJB("InvoicesSummaryData",
										Long.parseLong(invoiceOid));
						 String status = invoiceSummaryData.getAttribute("invoice_status");	
						 String invoiceId     = invoiceSummaryData.getAttribute("invoice_id");
						if(!TradePortalConstants.INV_LINKED_INSTR_PYB.equals(invoiceSummaryData.getAttribute("linked_to_instrument_type"))){
								mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
										TradePortalConstants.PAYABLE_MGMT_INVALID_ACTION,
										buttonTextVal,invoiceId,invoiceSummaryData.getAttribute("linked_to_instrument_type"));
									continue; // Move on to next Invoice
						}
						if(!InvoiceUtility.isPayableInvoiceUpdatable(status)){
							mediatorServices.getErrorManager().issueError(
									TradePortalConstants.ERR_CAT_1,TradePortalConstants.PAYABLE_MGMT_INVALID_STATUS,invoiceId);
							continue;
						}
						//Validate -ve payment amount
						if(TradePortalConstants.PAY_INV_BUTTON_APPLY_PAY_AMT.equals(buttonPressed) ||  TradePortalConstants.PAY_INV_LIST_BUTTON_APPLY_PAY_AMT.equals(buttonPressed)){
								if(modifyVal.startsWith("-") ){
									mediatorServices.getErrorManager().issueError(
									TradePortalConstants.ERR_CAT_1,
									TradePortalConstants.INVALID_PAYMENT_AMOUNT);
									continue;
								}
								//pay amt-credit note applied amt cannot be -ve
								BigDecimal crNoteAmount =invoiceSummaryData.getAttributeDecimal("total_credit_note_amount");
								BigDecimal payAmt = new BigDecimal(modifyVal);
								BigDecimal remAmt =payAmt.subtract(crNoteAmount.negate());
								if( remAmt.compareTo(BigDecimal.ZERO) < 0){
									mediatorServices.getErrorManager().issueError(
											TradePortalConstants.ERR_CAT_1,
											TradePortalConstants.INVALID_PAY_AMT_WITH_CRNOTE);
									continue;
								}
						}
						
						if(TradePortalConstants.BUTTON_MODIFY_SUPPLIER_DATE.equals(buttonPressed)){
							String payDate = StringFunction.isNotBlank(invoiceSummaryData.getAttribute("payment_date"))?
									invoiceSummaryData.getAttribute("payment_date"):invoiceSummaryData.getAttribute("due_date");
							//if 'Send to Supplier Date' is greater  than or equal to the earliest of payment date/due date, OR 
							// if it is less than current system date then error
							Date paymentDate = TPDateTimeUtility.convertDateStringToDate(payDate,"MM/dd/yyyy");
							Date supplierDate = TPDateTimeUtility.convertDateStringToDate(modifyVal,"MM/dd/yyyy");
							if(!supplierDate.before(paymentDate)){
								mediatorServices.getErrorManager().issueError(
										TradePortalConstants.ERR_CAT_1,
										TradePortalConstants.INVALID_SUPPLIER_DATE,
										"", null);
								continue;
							}
							if(supplierDate.before(GMTUtility.getGMTDateTime())){
								mediatorServices.getErrorManager().issueError(
										TradePortalConstants.ERR_CAT_1,
										TradePortalConstants.INVALID_SUPPLIER_DATE_WITH_SYS_DATE,
										"", null);
								continue;
							}
						  }
		
						else if(TradePortalConstants.PAY_PGM_BUTTON_APPLY_PAYMENT_DATE.equals(buttonPressed) && StringFunction.isNotBlank(invoiceSummaryData.getAttribute("issue_date"))){
							if ((DateTimeUtility.convertStringDateToDate(modifyVal)).before(DateTimeUtility.convertStringDateToDate(invoiceSummaryData.getAttribute("issue_date"))))
						      {
									mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.INVALID_INV_PAYMENT_DATE);
									return new DocumentHandler();
							  }
						}
						
						String invoiceGroupOid = invoiceSummaryData.getAttribute("invoice_group_oid");
						invoiceSummaryData.setAttribute(modifyAttr, modifyVal);
						invoiceSummaryData.setAttribute("opt_lock", optLock);
				       
				        String tpName=invoiceSummaryData.getTpRuleName();
				        invoiceSummaryData.setAttribute("tp_rule_name", tpName);
				        invoiceSummaryData.setAttribute("action",  invAction);
				        invoiceSummaryData.setAttribute("user_oid",inputDoc.getAttribute("/UploadInvoiceList/userOid"));
						
						int success = invoiceSummaryData.save();
						String newGroupOid = invoiceSummaryData.getAttribute("invoice_group_oid");
						if (success > 0) {
							if(StringFunction.isNotBlank(newGroupOid) && !invoiceGroupOid.equals(newGroupOid)){
								InvoiceUtility.updateAttachmentDetails(newGroupOid,invoiceGroupOid,invoiceOid,mediatorServices);
							}
							// display message
							mediatorServices.getErrorManager().issueError(InvoiceGroupMediator.class.getName(),
									successMsgVal,
									mediatorServices.getResourceManager().getText("UploadInvoices.Invoice", TradePortalConstants.TEXT_BUNDLE)+" ",invoiceId);
						}
					}
				}catch(Exception e)
				{		   
					   LOG.info("General exception caught in UploadInvoiceMediator:modifySupplierDate");
					   e.printStackTrace();
				}
			}
			return new DocumentHandler();
		}

		/**
		 * 
		 * @param inputDoc
		 * @param mediatorServices
		 * @param buttonPressed
		 * @return
		 * @throws RemoteException
		 * @throws AmsException
		 * Resets Supplier Date or Early Payment Amount
		 */
		private DocumentHandler performResetInvoiceData(DocumentHandler inputDoc, 
				MediatorServices mediatorServices, String buttonPressed) throws RemoteException, AmsException {
			
			mediatorServices.debug("InvoiceGroupMediatorBean.performResetInvoiceData: Start: ");
			Vector<DocumentHandler> invoiceList = inputDoc.getFragments("/UploadInvoiceList/InvoicesSummaryData");
			String resetAttr = null;
			String successMsgVal = null;
			String blankMsgTextVal = null;
			int count = 0;
			DocumentHandler invoiceItem = null;
			String invoiceOid = null;
			String optLock = null;
			count = invoiceList.size();
			mediatorServices.debug("InvoiceMediator: number of invoices selected: " + count);
			String invAction="";
			String buttonTextVal ="";
			String securityRights	  = inputDoc.getAttribute("/UploadInvoiceList/securityRights");
			//Set attributes based on buttonPressed
			if(TradePortalConstants.PAY_MGMT_BUTTON_RESET_SUPP_DATE.equals(buttonPressed)) {
				if (!SecurityAccess.hasRights(securityRights, SecurityAccess.UPLOAD_PAY_MGM_RESET_SUPPLIER_DATE)) {
					// user does not have appropriate authority.
					mediatorServices.getErrorManager().issueError(
							TradePortalConstants.ERR_CAT_1,
							TradePortalConstants.NO_ACTION_MATCH_NOTICE_AUTH,
							mediatorServices.getResourceManager().getText(
									"SecurityProfileDetail.ResetSendtoSuppDate",
									TradePortalConstants.TEXT_BUNDLE));
					return new DocumentHandler();
				}
				resetAttr = "send_to_supplier_date";
				successMsgVal = TradePortalConstants.PAY_PRGM_RESET_SUPP_DATE;
				invAction = TradePortalConstants.UPLOAD_INV_ACTION_RESET_SUPPLIER_DATE;
				buttonTextVal = mediatorServices.getResourceManager().getText("UploadInvoices.InvPayResetSuppDate",TradePortalConstants.TEXT_BUNDLE);
			}else if(TradePortalConstants.PAY_MGMT_BUTTON_RESET_PAY_AMOUNT.equals(buttonPressed) || TradePortalConstants.PAY_INV_LIST_BUTTON_RESET_PAY_AMT.equals(buttonPressed)) {
				if (!SecurityAccess.hasRights(securityRights, SecurityAccess.UPLOAD_PAY_MGM_RESET_EARLY_PAY_AMOUNT)) {
					// user does not have appropriate authority.
					mediatorServices.getErrorManager().issueError(
							TradePortalConstants.ERR_CAT_1,
							TradePortalConstants.NO_ACTION_MATCH_NOTICE_AUTH,
							mediatorServices.getResourceManager().getText(
									"SecurityProfileDetail.ResetEarlyPayAmt",
									TradePortalConstants.TEXT_BUNDLE));
					return new DocumentHandler();
				}
				resetAttr = "payment_amount";
				successMsgVal = TradePortalConstants.PAY_PRGM_RESET_PAYMENT_AMT;
				invAction = TradePortalConstants.UPLOAD_INV_ACTION_RESET_PAYMENT_AMT;
				buttonTextVal = mediatorServices.getResourceManager().getText("UploadInvoices.InvPayResetEarlyAmt",TradePortalConstants.TEXT_BUNDLE);
			}
			else if(TradePortalConstants.PAY_PGM_BUTTON_CLEAR_PAYMENT_DATE.equals(buttonPressed)) {
				if (!SecurityAccess.hasRights(securityRights, SecurityAccess.PAY_CLEAR_PAYMENT_DATE)) {
					// user does not have appropriate authority.
					mediatorServices.getErrorManager().issueError(
							TradePortalConstants.ERR_CAT_1,
							TradePortalConstants.NO_ACTION_MATCH_NOTICE_AUTH,
							mediatorServices.getResourceManager().getText(
									"SecurityProfileDetail.PayClearPaymentDate",
									TradePortalConstants.TEXT_BUNDLE));
					return new DocumentHandler();
				}
				resetAttr = "payment_date";
				successMsgVal = TradePortalConstants.PAY_PRGM_RESET_PAYMENT_DATE;
				invAction = TradePortalConstants.UPLOAD_INV_ACTION_CLEAR_PAYMENT_DATE;
				buttonTextVal = mediatorServices.getResourceManager().getText("UploadInvoices.InvPayClearPaymentDate",TradePortalConstants.TEXT_BUNDLE);
			}
			
			//Loop through the invoices to reset the corresponding attribute
			for (int i = 0; i < count; i++) 
		    {
			  invoiceItem = invoiceList.get(i);
			  String[] invoiceData = invoiceItem.getAttribute("/InvoiceData").split("/");
			  invoiceOid = invoiceData[0];
			  optLock = invoiceData[1];
			  try
			   {
			      if (StringFunction.isNotBlank(invoiceOid)) {
						InvoicesSummaryData invoiceSummaryData = (InvoicesSummaryData) mediatorServices.createServerEJB("InvoicesSummaryData",
										Long.parseLong(invoiceOid));
						String status = invoiceSummaryData.getAttribute("invoice_status");	
						String invoiceId     = invoiceSummaryData.getAttribute("invoice_id");
						if(!TradePortalConstants.INV_LINKED_INSTR_PYB.equals(invoiceSummaryData.getAttribute("linked_to_instrument_type"))){
							mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
									TradePortalConstants.PAYABLE_MGMT_INVALID_ACTION,
									buttonTextVal,invoiceId,invoiceSummaryData.getAttribute("linked_to_instrument_type"));
								continue; // Move on to next Invoice
						}
						if(!InvoiceUtility.isPayableInvoiceUpdatable(status)){
							mediatorServices.getErrorManager().issueError(
									TradePortalConstants.ERR_CAT_1,TradePortalConstants.PAYABLE_MGMT_INVALID_STATUS,invoiceId);
							continue;
						}
						invoiceSummaryData.setAttribute(resetAttr, null);
						invoiceSummaryData.setAttribute("opt_lock", optLock);//BSL CR710 02/03/2012 Rel8.0 ADD
				       
				        String tpName=invoiceSummaryData.getTpRuleName();
				        invoiceSummaryData.setAttribute("tp_rule_name", tpName);
				        invoiceSummaryData.setAttribute("action",  invAction);
				        invoiceSummaryData.setAttribute("user_oid",inputDoc.getAttribute("/UploadInvoiceList/userOid"));
						int success = invoiceSummaryData.save();
						if (success > 0) {
							// display message
							mediatorServices.getErrorManager().issueError(InvoiceGroupMediator.class.getName(),
									successMsgVal,
									mediatorServices.getResourceManager().getText("UploadInvoices.Invoice", TradePortalConstants.TEXT_BUNDLE)+" ",invoiceId);
						}
					}
				}catch(Exception e)
				{		   
					   LOG.info("General exception caught in UploadInvoiceMediator:performResetInvoiceData");
					   e.printStackTrace();
				}
			}
			return new DocumentHandler();
	     
		}
	
     private DocumentHandler resetPayPgmInvoice(DocumentHandler inputDoc, MediatorServices mediatorServices) {
			
			Vector<DocumentHandler> invoiceList = inputDoc.getFragments("/UploadInvoiceList/InvoicesSummaryData");
			String resetAttr = null;
			String successMsgVal = null;
			String blankMsgTextVal = null;
			int count = 0;
			DocumentHandler invoiceItem = null;
			String invoiceOid = null;
			String optLock = null;
			count = invoiceList.size();
			for (int i = 0; i < count; i++) 
		    {
			  invoiceItem = invoiceList.get(i);
			  String[] invoiceData = invoiceItem.getAttribute("/InvoiceData").split("/");
			  invoiceOid = invoiceData[0];
			  optLock = invoiceData[1];
			  try
			   {
			      if (StringFunction.isNotBlank(invoiceOid)) {
						InvoicesSummaryData invoiceSummaryData = (InvoicesSummaryData) mediatorServices.createServerEJB("InvoicesSummaryData",
										Long.parseLong(invoiceOid));
							
						String invoiceId     = invoiceSummaryData.getAttribute("invoice_id");
						if(!TradePortalConstants.INV_LINKED_INSTR_PYB.equals(invoiceSummaryData.getAttribute("linked_to_instrument_type"))){
							mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
									TradePortalConstants.PAYABLE_MGMT_INVALID_ACTION,mediatorServices.getResourceManager().getText("InvoiceSummary.ResetAuthorised",
									TradePortalConstants.TEXT_BUNDLE),invoiceId,invoiceSummaryData.getAttribute("linked_to_instrument_type"));
								continue; // Move on to next Invoice
						}
						if(!TradePortalConstants.UPLOAD_INV_STATUS_PAR_AUTH.equals(invoiceSummaryData.getAttribute("invoice_status"))){
							 mediatorServices.getErrorManager().issueError(
				     	               UploadInvoiceMediator.class.getName(),
				     	               TradePortalConstants.TRANSACTION_CANNOT_PROCESS, 
				     	               mediatorServices.getResourceManager().getText("UploadInvoices.InvoiceID",TradePortalConstants.TEXT_BUNDLE),
				     	               invoiceId ,
				     	               ReferenceDataManager.getRefDataMgr().getDescr("UPLOAD_INVOICE_STATUS", invoiceSummaryData.getAttribute("invoice_status"), mediatorServices.getCSDB().getLocaleName()),
				     	               mediatorServices.getResourceManager().getText("UploadInvoices.ListResetToAuthorize", TradePortalConstants.TEXT_BUNDLE));
							continue;
						}
						invoiceSummaryData.setAttribute("invoice_status", TradePortalConstants.UPLOAD_INV_STATUS_AVL_FOR_AUTH);
						invoiceSummaryData.setAttribute("opt_lock", optLock);
				       
				        String tpName=invoiceSummaryData.getTpRuleName();
				        invoiceSummaryData.setAttribute("tp_rule_name", tpName);
				        invoiceSummaryData.setAttribute("action",   TradePortalConstants.UPLOAD_INV_ACTION_RESET_AUTH);
				        invoiceSummaryData.setAttribute("user_oid",inputDoc.getAttribute("/UploadInvoiceList/userOid"));
						int success = invoiceSummaryData.save();
						if (success > 0) {
							String deletePanelAuth = "delete from panel_authorizer where owner_object_type = 'INVOICES_SUMMARY_DATA' and p_owner_oid=?";
							try {
								DatabaseQueryBean.executeUpdate(deletePanelAuth, false, new Object[]{invoiceOid});
							} catch (SQLException e) {
								LOG.debug("UploadInvoiceMediatorBean.resetToAUthorize: Error in deleting from panel_authorizer");
								e.printStackTrace();
							}
							// display message
							mediatorServices.getErrorManager().issueError(UploadInvoiceMediator.class.getName(),
									TradePortalConstants.INV_RESET_TO_AUTHORIZE,
									mediatorServices.getResourceManager().getText("UploadInvoices.InvoiceID", TradePortalConstants.TEXT_BUNDLE),
									invoiceId);}
					}
				}catch(Exception e)
				{		   
					   LOG.info("General exception caught in UploadInvoiceMediator:performResetInvoiceData");
					   e.printStackTrace();
				}
			}
			return new DocumentHandler();
	     
		}
		//Payable Invoice "PYB" authorise only
	private DocumentHandler authorisePayPgmInvoice(DocumentHandler inputDoc,
			MediatorServices mediatorServices) throws RemoteException,
			AmsException {

		Vector invoiceList = null;
		String invoiceOid = null;
		String optLock = null;
		DocumentHandler invoiceItem = null;
		int count = 0;
		User user = null;
		ArMatchingRule matchingRule = null;
		String buyerName = null;
		invoiceList = inputDoc.getFragments("/UploadInvoiceList/InvoicesSummaryData");
		String corpOrgOid = inputDoc.getAttribute("/UploadInvoiceList/ownerOrg");
		String userOid = inputDoc.getAttribute("/UploadInvoiceList/userOid");
		String subsAuth = inputDoc.getAttribute("/Authorization/authorizeAtParentViewOrSubs");
		String securityRights = inputDoc.getAttribute("/UploadInvoiceList/securityRights");
		if (!SecurityAccess.hasRights(securityRights, SecurityAccess.UPLOAD_PAY_MGM_AUTH)) {
			// user does not have appropriate authority.
			mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.NO_ACTION_MATCH_NOTICE_AUTH,
					mediatorServices.getResourceManager().getText(
							"UploadInvoiceAction.Authorize",
							TradePortalConstants.TEXT_BUNDLE));
			return new DocumentHandler();
		}
		count = invoiceList.size();
		
		// ***********************************************************************
		// check to make sure that the user is allowed to authorize the
		// invoice - the user has to be a corporate user (not an admin
		// user) and have the rights to authorize a Receivables
		// ***********************************************************************

		user = (User) mediatorServices.createServerEJB("User",
				Long.parseLong(userOid));
		CorporateOrganization corpOrg = null;
		InvoiceGroup invoiceGroup =  null;
		Set<String> endToEndIDSet = new HashSet<String>();
		// Loop through the list of invoice oids, and processing each one in the
		// invoice list
		for (int i = 0; i < count; i++) {
			invoiceItem = (DocumentHandler) invoiceList.get(i);
			invoiceOid = invoiceItem.getAttribute("/InvoiceData");
			String[] invoiceData = invoiceItem.getAttribute("/InvoiceData")
					.split("/");
			invoiceOid = invoiceData[0];
			optLock = invoiceData[1];

			try {
				InvoicesSummaryData invoiceSummaryData = (InvoicesSummaryData) mediatorServices
						.createServerEJB("InvoicesSummaryData",Long.parseLong(invoiceOid));
				String invoiceStatus = invoiceSummaryData.getAttribute("invoice_status");
				String invoiceId = invoiceSummaryData.getAttribute("invoice_id");
				String invoiceGroupOid = invoiceSummaryData.getAttribute("invoice_group_oid"); 
				
				if(!TradePortalConstants.INV_LINKED_INSTR_PYB.equals(invoiceSummaryData.getAttribute("linked_to_instrument_type"))){
					mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
							TradePortalConstants.PAYABLE_MGMT_INVALID_ACTION,mediatorServices.getResourceManager().getText("TransactionAction.Authorized",
									TradePortalConstants.TEXT_BUNDLE),invoiceId,invoiceSummaryData.getAttribute("linked_to_instrument_type"));
						continue; // Move on to next Invoice
				}
				// only 'Available for Authorization', 'Partially Authorized',
				// 'Auth Failed', invoices can be authorized, when not navigation from Group
			    
			    if( StringFunction.isNotBlank(invoiceSummaryData.getAttribute("end_to_end_id")) )
			    {
			    	if( endToEndIDSet.add(invoiceSummaryData.getAttribute("end_to_end_id")) )
					{
			    		// only 'Available for Authorization', 'Partially Authorized',
						// 'Auth Failed', invoices can be authorized, when not navigation from Group
					    if (!InvoiceUtility.isPayableInvoiceAuthorizable(mediatorServices.getResourceManager().getText(
										"InvoiceUploadRequest.invoice_id",TradePortalConstants.TEXT_BUNDLE), invoiceId,invoiceStatus, mediatorServices)) {
							continue; // Move on to next Invoice
						}
			
			    		Map<String, String> endToEndIdGroupProp = new HashMap<String, String>();
						endToEndIdGroupProp.put("corpOrgOid", corpOrgOid);
						endToEndIdGroupProp.put("userOid", userOid);
						endToEndIdGroupProp.put("userWorkGroupOid", user.getAttribute("work_group_oid"));
						endToEndIdGroupProp.put("subsAuth", subsAuth);
						InvoiceGroup endToEndIDinvoiceGroup = (InvoiceGroup) mediatorServices.createServerEJB("InvoiceGroup", 
								Long.parseLong(invoiceGroupOid));
						PayableInvoiceGroupMediator payableGroupMediator = (PayableInvoiceGroupMediator) mediatorServices.createServerEJB("PayableInvoiceGroupMediator");
						payableGroupMediator.authorizeEndToEndIDGroup(endToEndIDinvoiceGroup, endToEndIdGroupProp, mediatorServices);
			    	}
			    }
			    else{
			    	// only 'Available for Authorization', 'Partially Authorized',
					// 'Auth Failed', invoices can be authorized, when not navigation from Group
				    if (!InvoiceUtility.isPayableInvoiceAuthorizable(mediatorServices.getResourceManager().getText(
									"InvoiceUploadRequest.invoice_id",TradePortalConstants.TEXT_BUNDLE), invoiceId,invoiceStatus, mediatorServices)) {
						continue; // Move on to next Invoice
					}
		
				String authorizeRights = null;
				String authorizeType = null;
				String userWorkGroupOid = user.getAttribute("work_group_oid");
				String firstAuthorizer = invoiceSummaryData.getAttribute("first_authorizing_user_oid");
				String firstAuthorizerWorkGroup = invoiceSummaryData.getAttribute("first_authorizing_work_group_oid");

				// corporate org needs to be retrieved at most once
				// for all groups being processed, so do it lazily
				if (corpOrg == null) {
					corpOrg = (CorporateOrganization) mediatorServices.createServerEJB("CorporateOrganization",Long.parseLong(corpOrgOid));
				}
				authorizeType = corpOrg.getAttribute("dual_auth_upload_pay_inv");
				// Check threshold amount start
				
				String clientBankOid = corpOrg.getAttribute("client_bank_oid"); //IR 27064 -Use correct ClientBankOid
				String baseCurrency = corpOrg.getAttribute("base_currency_code");
				String invCurrency = invoiceSummaryData.getAttribute("currency");
				String invoiceAmt = (StringFunction.isNotBlank(invoiceSummaryData.getAttribute("payment_amount")) 
				&& invoiceSummaryData.getAttributeDecimal("payment_amount").compareTo(BigDecimal.ZERO) != 0)?
						invoiceSummaryData.getAttribute("payment_amount") : invoiceSummaryData.getAttribute("amount");

				String selectClause = "select distinct upload_invoice_oid, currency as currency_code, case when (payment_amount is null or payment_amount=0) "
						+ "then amount else payment_amount end as payment_amount from invoices_summary_data ";

				boolean isValid = InvoiceUtility.checkThresholds(invoiceAmt,invCurrency, baseCurrency, "upload_pay_inv_thold",
						"upload_pay_inv_dlimit", corpOrgOid, clientBankOid,user, selectClause, false, subsAuth, mediatorServices);
				if (!isValid) {
					return new DocumentHandler();
				}
				// threshold check end
				
				// if user has authority rights then only should process, else
				// thow error
				// that user doesn't have right to authorize this perticular
				// invoice. every invoice associate to trading partner rule,
				// so authority type can be diferent for every invoice.it is not
				// depend on user.
				// it is depend on user only when 'look for corporate customer
				// profile' is selected in trading partner refdata of that
				// invoice.
				String newStatus = null;
				 invoiceSummaryData.setAttribute("action",   TradePortalConstants.UPLOAD_INV_ACTION_AUTH);
			     invoiceSummaryData.setAttribute("user_oid",userOid);
					
				// if two different workgroups required authority is selected,
				// then user must belong to a group
				if ((TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS).equals(authorizeType)
						&& (StringFunction.isBlank(userWorkGroupOid))) {

					newStatus = TradePortalConstants.UPLOAD_INV_STATUS_AUTH_FAILED;

					mediatorServices.getErrorManager().issueError(UploadInvoiceMediator.class.getName(),
							TradePortalConstants.TRANS_WORK_GROUP_REQUIRED);
					invoiceSummaryData.setAttribute("tp_rule_name",invoiceSummaryData.getTpRuleName());
					invoiceSummaryData.setAttribute("invoice_status", newStatus);
					invoiceSummaryData.save() ;
				}
				// ***********************************************************************
				// If dual authorization is required, set the transaction state
				// to
				// partially authorized if it has not been previously authorized
				// and set
				// the transaction to authorized if there has been a previous
				// authorizer.
				// The second authorizer must not be the same as the first
				// authorizer
				// not the same work groups, depending on the corporate org
				// setting.
				// If dual authorization is not required, set the invoice state
				// to
				// authorized.
				// ***********************************************************************
				else if ((TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS).equals(authorizeType)
						|| (TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS).equals(authorizeType)) {
					authoriseWithDiffWorkGroup(invoiceSummaryData, invoiceId,invoiceOid, userOid, firstAuthorizer, optLock,
							firstAuthorizerWorkGroup, userWorkGroupOid,authorizeType, mediatorServices);

				} else if (TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH
						.equals(authorizeType)
						&& corpOrg
								.isPanelAuthEnabled(TradePortalConstants.PAYABLES_PROGRAM_INVOICE)) {
					try {
						panelAuth(invoiceSummaryData, invoiceId, invoiceOid,userOid, invCurrency, invoiceAmt,
								userWorkGroupOid, corpOrg, corpOrgOid, user,optLock, mediatorServices);
					} catch (AmsException e) {
						IssuedError is = e.getIssuedError();
						if (is != null) {
							String errorCode = is.getErrorCode();
							if (TradePortalConstants.REQUIRED_ATTRIBUTE.equals(errorCode)
									|| TradePortalConstants.NO_PANEL_RANGE_FOR_AMOUNT_INST_TYPE.equals(errorCode)) {
								return new DocumentHandler();
							} else
								throw (e);
						}
					}
				} else {
					newStatus = TradePortalConstants.UPLOAD_INV_STATUS_AUTH;

					invoiceSummaryData.setAttribute("first_authorizing_user_oid", userOid);
					invoiceSummaryData.setAttribute("first_authorizing_work_group_oid",userWorkGroupOid);
					invoiceSummaryData.setAttribute("first_authorize_status_date",DateTimeUtility.getGMTDateTime());

					invoiceSummaryData.setAttribute("invoice_status", newStatus);
					invoiceSummaryData.setAttribute("opt_lock", optLock);
					String tpName = invoiceSummaryData.getTpRuleName();
					invoiceSummaryData.setAttribute("tp_rule_name", tpName);
					int success = invoiceSummaryData.save();
					String newGroupOid = invoiceSummaryData
							.getAttribute("invoice_group_oid");
					if (success == 1) {
						if (StringFunction.isNotBlank(newGroupOid)&& !invoiceGroupOid.equals(newGroupOid)) {
							InvoiceUtility.updateAttachmentDetails(newGroupOid,
									invoiceGroupOid, invoiceOid,mediatorServices);
						}
							// display message...
							mediatorServices.getErrorManager().issueError(UploadInvoiceMediator.class.getName(),TradePortalConstants.INV_AUTH_SUCCESSFUL,
											mediatorServices.getResourceManager().getText("UploadInvoices.InvoiceID",TradePortalConstants.TEXT_BUNDLE),
											invoiceId);
							InvoiceUtility.createOutgoingQueueAndIssueSuccess(newStatus, invoiceOid, false,mediatorServices);

						
					}
				}
			  }
			} catch (Exception e) {
				System.out
						.println("General exception caught in UploadInvoiceMediator:authorizeInvoice");
				e.printStackTrace();
			}
		}

		return (new DocumentHandler());
	}

	private void authoriseWithDiffWorkGroup(InvoicesSummaryData invoiceSummaryData, String invoiceId,String invoiceOid, String userOid, String firstAuthorizer,
			String optLock, String firstAuthorizerWorkGroup,String userWorkGroupOid, String authorizeType,
			MediatorServices mediatorServices) throws RemoteException,AmsException {
				
		String newStatus = "";
		String tpName = invoiceSummaryData.getTpRuleName();
		invoiceSummaryData.setAttribute("tp_rule_name", tpName);
		if (StringFunction.isBlank(firstAuthorizer)) {
			newStatus = TradePortalConstants.UPLOAD_INV_STATUS_PAR_AUTH;
			invoiceSummaryData.setAttribute("first_authorizing_user_oid",
					userOid);
			invoiceSummaryData.setAttribute("first_authorizing_work_group_oid",
					userWorkGroupOid);
			invoiceSummaryData.setAttribute("first_authorize_status_date",
					DateTimeUtility.getGMTDateTime());

			invoiceSummaryData.setAttribute("invoice_status", newStatus);

			// Set the optimistic lock attribute to make sure the invoice hasn't
			// been changed by someone else since it was first retrieved
			invoiceSummaryData.setAttribute("opt_lock", optLock);
			int success = invoiceSummaryData.save();
			if (success == 1) {
				// display message...

				mediatorServices.getErrorManager().issueError(
						UploadInvoiceMediator.class.getName(),
						TradePortalConstants.TRANS_PARTIALLY_AUTHORIZED,
						mediatorServices.getResourceManager().getText(
								"UploadInvoices.InvoiceID",
								TradePortalConstants.TEXT_BUNDLE), invoiceId);

			}
		}
		// if firstAuthorizer is not empty
		else {
			// Two work groups required and this user belongs to the
			// same work group as the first authorizer: Error.
			if ((TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS)
					.equals(authorizeType)
					&& userWorkGroupOid.equals(firstAuthorizerWorkGroup)) {
				newStatus = TradePortalConstants.UPLOAD_INV_STATUS_AUTH_FAILED;

				mediatorServices.getErrorManager().issueError(UploadInvoiceMediator.class.getName(),
						TradePortalConstants.TRANS_DIFF_WORK_GROUP_REQUIRED);
				invoiceSummaryData.setAttribute("opt_lock", optLock);
				invoiceSummaryData.setAttribute("invoice_status", newStatus);
				invoiceSummaryData.save();
			}
			// Two authorizers required and this user is the same as the
			// first authorizer. Give error.
			// Note requiring two work groups also implies requiring two
			// users.
			else if (firstAuthorizer.equals(userOid)) {
				newStatus = TradePortalConstants.UPLOAD_INV_STATUS_AUTH_FAILED;

				mediatorServices.getErrorManager().issueError(UploadInvoiceMediator.class.getName(),
						TradePortalConstants.DIFFERENT_SECOND_AUTH_REQRUIED);
				invoiceSummaryData.setAttribute("invoice_status", newStatus);
				invoiceSummaryData.setAttribute("opt_lock", optLock);
			invoiceSummaryData.save();
			}
			// Authorize
			else {
				newStatus = TradePortalConstants.UPLOAD_INV_STATUS_AUTH;

				invoiceSummaryData.setAttribute("second_authorizing_user_oid",
						userOid);
				invoiceSummaryData.setAttribute(
						"second_authorizing_work_group_oid", userWorkGroupOid);
				invoiceSummaryData.setAttribute("second_authorize_status_date",
						DateTimeUtility.getGMTDateTime());

				invoiceSummaryData.setAttribute("invoice_status", newStatus);
				invoiceSummaryData.setAttribute("opt_lock", optLock);
				
				int success = invoiceSummaryData.save();
				if (success == 1) {
					// display message...
					mediatorServices.getErrorManager().issueError(UploadInvoiceMediator.class.getName(),
							TradePortalConstants.INV_AUTH_SUCCESSFUL,mediatorServices.getResourceManager().getText(
									"UploadInvoices.InvoiceID",TradePortalConstants.TEXT_BUNDLE),invoiceId);
					InvoiceUtility.createOutgoingQueueAndIssueSuccess(newStatus, invoiceOid, false, mediatorServices);

				}
			}
		}
		
	}
		
	 /* Panel Auth process for Payables Invoices*/
    private void panelAuth(InvoicesSummaryData invoiceSummaryData,String invoiceId,String invoiceOid,String userOid,
    		String invoiceCurrency,String invoiceAmount,
    		String userWorkGroupOid ,CorporateOrganization corpOrg,String corpOrgOid,User user,String optLock,
    		MediatorServices mediatorServices)throws RemoteException, AmsException {
    	PanelAuthorizationGroup panelAuthGroup = null;
		String panelGroupOid = null;
		String panelOplockVal = null;
		String panelRangeOid = null;
		String newStatus = "";
        invoiceSummaryData.setAttribute("reqPanleAuth",TradePortalConstants.INDICATOR_YES);

		if(StringFunction.isBlank(invoiceSummaryData.getAttribute("panel_auth_group_oid"))) {
			
			panelGroupOid = PanelAuthUtility.getPanelAuthorizationGroupOid(TradePortalConstants.PAYABLES_PROGRAM_INVOICE, corpOrg, mediatorServices.getErrorManager());   				
			if(StringFunction.isNotBlank(panelGroupOid)){
				panelAuthGroup = (PanelAuthorizationGroup) mediatorServices.createServerEJB("PanelAuthorizationGroup",
						Long.parseLong(panelGroupOid));   					
			}else{
				// if panel id is blank, return the flow back.
				IssuedError is = new IssuedError();
				is.setErrorCode(TradePortalConstants.REQUIRED_ATTRIBUTE);
				throw new AmsException(is);
			}
			
			invoiceSummaryData.setAttribute("panel_auth_group_oid", panelAuthGroup.getAttribute("panel_auth_group_oid"));
			invoiceSummaryData.setAttribute("panel_oplock_val", panelAuthGroup.getAttribute("opt_lock"));
			
			String baseCurrencyToPanelGroup = PanelAuthUtility.getBaseCurrency(corpOrg, panelAuthGroup, null, TradePortalConstants.INVOICE);
			//IR 27059- use payment_amount if present
			String payAmount = StringFunction.isNotBlank(invoiceSummaryData.getAttribute("payment_amount"))?
					invoiceSummaryData.getAttribute("payment_amount"):invoiceSummaryData.getAttribute("amount");
    		BigDecimal amountInBaseToPanelGroup =  PanelAuthUtility.getAmountInBaseCurrency
    		   (invoiceSummaryData.getAttribute("currency"),payAmount, baseCurrencyToPanelGroup, corpOrgOid, 
    				   TradePortalConstants.INVOICE, null, false, mediatorServices.getErrorManager());
    		
    		panelRangeOid = PanelAuthUtility.getPanelRange(panelAuthGroup.getAttribute("panel_auth_group_oid"), amountInBaseToPanelGroup);
    		if(StringFunction.isBlank(panelRangeOid)){

    			IssuedError is = new IssuedError();
				is.setErrorCode(TradePortalConstants.NO_PANEL_RANGE_FOR_AMOUNT_INST_TYPE);
				throw new AmsException(is);
    		}
    		invoiceSummaryData.setAttribute("panel_auth_range_oid", panelRangeOid);
			
		}
		
		panelGroupOid  = invoiceSummaryData.getAttribute("panel_auth_group_oid");
	    panelOplockVal = invoiceSummaryData.getAttribute("panel_oplock_val");
	    panelRangeOid =  invoiceSummaryData.getAttribute("panel_auth_range_oid");   	
	    
       PanelAuthProcessor panelAuthProcessor = new PanelAuthProcessor(user, TradePortalConstants.INVOICE, mediatorServices.getErrorManager());
       panelAuthProcessor.init(panelGroupOid, new String[] {panelRangeOid}, panelOplockVal);

       String panelAuthTransactionStatus = panelAuthProcessor.process(invoiceSummaryData, panelRangeOid, false);
       String tpName=invoiceSummaryData.getTpRuleName(); 
		invoiceSummaryData.setAttribute("tp_rule_name", tpName);
		
       if (TradePortalConstants.AUTHTRUE.equals(panelAuthTransactionStatus)) {
			newStatus = TradePortalConstants.UPLOAD_INV_STATUS_AUTH;

			invoiceSummaryData.setAttribute("first_authorizing_user_oid",userOid);
			invoiceSummaryData.setAttribute("first_authorizing_work_group_oid",userWorkGroupOid);
			invoiceSummaryData.setAttribute("first_authorize_status_date",DateTimeUtility.getGMTDateTime());

			invoiceSummaryData.setAttribute("invoice_status", newStatus);
			invoiceSummaryData.setAttribute("opt_lock", optLock);
			String invoiceGroupOid = invoiceSummaryData.getAttribute("invoice_group_oid");
			int success = invoiceSummaryData.save();
			String newGroupOid = invoiceSummaryData.getAttribute("invoice_group_oid");
			if (success == 1) {
				if (StringFunction.isNotBlank(newGroupOid) && !invoiceGroupOid.equals(newGroupOid)) {
					InvoiceUtility.updateAttachmentDetails(newGroupOid,invoiceGroupOid, invoiceOid, mediatorServices);
				}
				
				// display message...
				mediatorServices.getErrorManager().issueError(
						UploadInvoiceMediator.class.getName(),TradePortalConstants.INV_AUTH_SUCCESSFUL,
						mediatorServices.getResourceManager().getText(
								"UploadInvoices.InvoiceID",TradePortalConstants.TEXT_BUNDLE), invoiceId);
				InvoiceUtility.createOutgoingQueueAndIssueSuccess(newStatus,invoiceOid, false, mediatorServices);

			}
       }else if (TradePortalConstants.AUTHPARTL.equals(panelAuthTransactionStatus)) {
    	  		
			newStatus = TradePortalConstants.UPLOAD_INV_STATUS_PAR_AUTH;

			invoiceSummaryData.setAttribute("first_authorizing_user_oid",userOid);
			invoiceSummaryData.setAttribute("first_authorizing_work_group_oid",userWorkGroupOid);
			invoiceSummaryData.setAttribute("first_authorize_status_date",DateTimeUtility.getGMTDateTime());

			invoiceSummaryData.setAttribute("invoice_status", newStatus);
			invoiceSummaryData.setAttribute("opt_lock", optLock);
			int success = invoiceSummaryData.save();
			if (success == 1) {
				// display message...
				mediatorServices.getErrorManager().issueError(UploadInvoiceMediator.class.getName(),
						TradePortalConstants.TRANS_PARTIALLY_AUTHORIZED,mediatorServices.getResourceManager().getText(
								"UploadInvoices.InvoiceID",TradePortalConstants.TEXT_BUNDLE), invoiceId);
			}
    	   
    	   
       }else if (TradePortalConstants.AUTHFALSE.equals(panelAuthTransactionStatus)) {
    	  
				newStatus = TradePortalConstants.UPLOAD_INV_STATUS_AUTH_FAILED;
				invoiceSummaryData.setAttribute("invoice_status", newStatus);
				invoiceSummaryData.setAttribute("opt_lock", optLock);
				invoiceSummaryData.save();
       } 
    }
    
	//Payable Invoice "PYB" authorise only
	private DocumentHandler authorisePayPgmInvoiceFromGroupList(DocumentHandler inputDoc,
			MediatorServices mediatorServices) throws RemoteException,
			AmsException {

		String invoiceOid = null;
		String optLock = null;
		DocumentHandler invoiceItem = null;
		int count = 0;
		ArMatchingRule matchingRule = null;
		String buyerName = null;
		CorporateOrganization corpOrg = null;
		InvoiceGroup invoiceGroup =  null;
		String panelGroupOid = null;
		String panelOplockVal = null;
		String panelRangeOid = null;
		String newStatus = "";
		String panelAuthTransactionStatus = "";
		Vector invoiceList = inputDoc.getFragments("/UploadInvoiceList/InvoicesSummaryData");
		String corpOrgOid = inputDoc.getAttribute("/UploadInvoiceList/ownerOrg");
		String userOid = inputDoc.getAttribute("/UploadInvoiceList/userOid");
		String subsAuth = inputDoc.getAttribute("/Authorization/authorizeAtParentViewOrSubs");
		String invoiceGroupPayAmt="";
		String invoiceGroupCurrency ="";
		String securityRights = inputDoc.getAttribute("/UploadInvoiceList/securityRights");
		if (!SecurityAccess.hasRights(securityRights, SecurityAccess.UPLOAD_PAY_MGM_AUTH)) {
			// user does not have appropriate authority.
			mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.NO_ACTION_MATCH_NOTICE_AUTH,
					mediatorServices.getResourceManager().getText(
							"UploadInvoiceAction.Authorize",
							TradePortalConstants.TEXT_BUNDLE));
			return new DocumentHandler();
		}
		count = invoiceList.size();
		
		if (corpOrg == null) {
			corpOrg = (CorporateOrganization) mediatorServices.createServerEJB("CorporateOrganization",Long.parseLong(corpOrgOid));
		}
		User user = (User) mediatorServices.createServerEJB("User",
				Long.parseLong(userOid));
		String authorizeType = corpOrg.getAttribute("dual_auth_upload_pay_inv");
		String clientBankOid = corpOrg.getAttribute("client_bank_oid"); 
		String baseCurrency = corpOrg.getAttribute("base_currency_code");
		Set<String> endToEndIDSet = new HashSet<String>();
		try {
			List<Object> sqlPrmLst = new ArrayList<Object>();								
			StringBuilder sql = new StringBuilder("select sum( case when (payment_amount is null or payment_amount=0) then amount else payment_amount end) as AMOUNT "
					+ "from invoices_summary_data where upload_invoice_oid in (");
			for (int i = 0; i < count; i++) {
				invoiceItem = (DocumentHandler) invoiceList.get(i);
				invoiceOid = invoiceItem.getAttribute("/InvoiceData");
				String[] invoiceData = invoiceItem.getAttribute("/InvoiceData").split("/");
				sql.append("?");
				invoiceOid = invoiceData[0];
				sqlPrmLst.add(invoiceOid);
				if (i != (count - 1)) 
				sql.append(",");
			}
			sql.append(")");
			DocumentHandler resultXml = DatabaseQueryBean.getXmlResultSet(sql.toString(), false, sqlPrmLst);
			if (resultXml != null) {
				DocumentHandler record = resultXml.getFragment("/ResultSetRecord");
				if(record!=null){
					invoiceGroupPayAmt = record.getAttribute("/AMOUNT");
				}
			}
			
			//pick one of the invoice from the list and get the status and currency
			InvoicesSummaryData invoiceSummaryData = (InvoicesSummaryData) mediatorServices
						.createServerEJB("InvoicesSummaryData",Long.parseLong(invoiceOid));
			invoiceGroupCurrency = invoiceSummaryData.getAttribute("currency");
			String invoiceStatus = invoiceSummaryData.getAttribute("invoice_status");
			String invoiceId = invoiceSummaryData.getAttribute("invoice_id");
			
			//IR 27465 - auth failed invoice cannot be regrouped
			if((TradePortalConstants.UPLOAD_INV_STATUS_PAR_AUTH.equals(invoiceStatus) || TradePortalConstants.UPLOAD_INV_STATUS_AUTH_FAILED.equals(invoiceStatus))){
				mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,TradePortalConstants.INVALID_GROUP_AUTH);
				return new DocumentHandler();
			}
			if(!TradePortalConstants.INV_LINKED_INSTR_PYB.equals(invoiceSummaryData.getAttribute("linked_to_instrument_type"))){
				mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.PAYABLE_MGMT_INVALID_ACTION,mediatorServices.getResourceManager().getText("TransactionAction.Authorized",
								TradePortalConstants.TEXT_BUNDLE),invoiceId,invoiceSummaryData.getAttribute("linked_to_instrument_type"));
				return new DocumentHandler();
			}
			
			
		    if( StringFunction.isNotBlank(invoiceSummaryData.getAttribute("end_to_end_id")) )
		    {
		    	if( endToEndIDSet.add(invoiceSummaryData.getAttribute("end_to_end_id")) )
				{	// create Map to store END_TO_END_ID info
		    		// only 'Available for Authorization', 'Auth Failed', invoices can be authorized, when navigating from Group
					//( partially auth invoices are checked above)
				    if (!InvoiceUtility.isPayableInvoiceAuthorizable(mediatorServices.getResourceManager().getText(
									"InvoiceUploadRequest.invoice_id",TradePortalConstants.TEXT_BUNDLE), invoiceId,invoiceStatus, mediatorServices)) {
				    	return new DocumentHandler();
					}
				Map<String, String> endToEndIdGroupProp = new HashMap<String, String>();
				endToEndIdGroupProp.put("corpOrgOid", corpOrgOid);
				endToEndIdGroupProp.put("userOid", userOid);
				endToEndIdGroupProp.put("userWorkGroupOid", user.getAttribute("work_group_oid"));
				endToEndIdGroupProp.put("subsAuth", subsAuth);
				InvoiceGroup endToEndIDinvoiceGroup = (InvoiceGroup) mediatorServices.createServerEJB("InvoiceGroup", 
						Long.parseLong(invoiceSummaryData.getAttribute("invoice_group_oid")));
				PayableInvoiceGroupMediator payableGroupMediator = (PayableInvoiceGroupMediator) mediatorServices.createServerEJB("PayableInvoiceGroupMediator");
				payableGroupMediator.authorizeEndToEndIDGroup(endToEndIDinvoiceGroup, endToEndIdGroupProp, mediatorServices);
				}
		    }
			else
			{
				// only 'Available for Authorization', 'Auth Failed', invoices can be authorized, when navigating from Group
				//( partially auth invoices are checked above)
			    if (!InvoiceUtility.isPayableInvoiceAuthorizable(mediatorServices.getResourceManager().getText(
								"InvoiceUploadRequest.invoice_id",TradePortalConstants.TEXT_BUNDLE), invoiceId,invoiceStatus, mediatorServices)) {
			    	return new DocumentHandler();
				}
			// Check threshold amount start	
			String selectClause = "select distinct upload_invoice_oid, currency as currency_code, case when (payment_amount is null or payment_amount=0) "
					+ "then amount else payment_amount end as payment_amount from invoices_summary_data ";

			boolean isValid = InvoiceUtility.checkThresholds(invoiceGroupPayAmt,invoiceGroupCurrency, baseCurrency, "upload_pay_inv_thold",
					"upload_pay_inv_dlimit", corpOrgOid, clientBankOid,user, selectClause, false, subsAuth, mediatorServices);
			if (!isValid) {
				return new DocumentHandler();
			}
			// threshold check end
			
			InvoiceProperties invoiceProp = new InvoiceProperties();
			invoiceProp.setOldInvoiceStatus(invoiceStatus);
			invoiceProp.setUser(user);
			invoiceProp.setUserOid(userOid);
			invoiceProp.setCorpOrg(corpOrg);
			invoiceProp.setCorpOrgOid(corpOrgOid);
			
			
			//If status is avaliable_for_Auth and corp setting is panel auth or dual auth then create INVOICE_GROUP first
			if((TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH.equals(authorizeType) &&
		  			corpOrg.isPanelAuthEnabled(TradePortalConstants.PAYABLES_PROGRAM_INVOICE)) ||
					((TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS.equals(authorizeType)
					|| TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS.equals(authorizeType)) 
					//&& StringFunction.isBlank(invoiceSummaryData.getAttribute("first_authorizing_user_oid")) //IR-27465
					)){
				invoiceSummaryData.setAttribute("invoice_status",TradePortalConstants.UPLOAD_INV_STATUS_PAR_AUTH);
				invoiceSummaryData.setAttribute("tp_rule_name", invoiceSummaryData.getTpRuleName());
				String newGrpOid= invoiceSummaryData.createInvoiceGroup();
				invoiceProp.setNewGroupOid(newGrpOid);
				invoiceSummaryData.setAttribute("invoice_status",invoiceProp.getOldInvoiceStatus());
				invoiceGroup = (InvoiceGroup) mediatorServices.createServerEJB("InvoiceGroup",Long.parseLong(newGrpOid));
				invoiceProp.setDescription(invoiceGroup.getDescription());
			}
			
			//for Panel Auth
			if(TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH.equals(authorizeType) &&
			  			corpOrg.isPanelAuthEnabled(TradePortalConstants.PAYABLES_PROGRAM_INVOICE) && invoiceGroup!=null) {
				PanelAuthorizationGroup panelAuthGroup = null;
				if(StringFunction.isBlank(invoiceGroup.getAttribute("panel_auth_group_oid"))) {
					
					panelGroupOid = PanelAuthUtility.getPanelAuthorizationGroupOid(TradePortalConstants.PAYABLES_PROGRAM_INVOICE, corpOrg, mediatorServices.getErrorManager());   				
					if(StringFunction.isNotBlank(panelGroupOid)){
						panelAuthGroup = (PanelAuthorizationGroup) mediatorServices.createServerEJB("PanelAuthorizationGroup",
								Long.parseLong(panelGroupOid));   					
					}else{
						// issue error as panel group is required if panel auth defined for instrument type
						mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,AmsConstants.REQUIRED_ATTRIBUTE, 
								mediatorServices.resMgr.getText("CorpCust.PanelGroupId", TradePortalConstants.TEXT_BUNDLE));
						// if panel id is blank, return the flow back.
						IssuedError is = new IssuedError();
						is.setErrorCode(TradePortalConstants.REQUIRED_ATTRIBUTE);
						throw new AmsException(is);
					}
				
					invoiceGroup.setAttribute("panel_auth_group_oid", panelAuthGroup.getAttribute("panel_auth_group_oid"));
					invoiceGroup.setAttribute("panel_oplock_val", panelAuthGroup.getAttribute("opt_lock"));
					
					String baseCurrencyToPanelGroup = PanelAuthUtility.getBaseCurrency(corpOrg, panelAuthGroup, null, TradePortalConstants.INVOICE);
		 		   BigDecimal amountInBaseToPanelGroup =  PanelAuthUtility.getAmountInBaseCurrency
		 		   (invoiceGroupCurrency, invoiceGroupPayAmt, baseCurrencyToPanelGroup, corpOrgOid, 
		 				   TradePortalConstants.INVOICE, null, false, mediatorServices.getErrorManager()); 
					
					panelRangeOid = PanelAuthUtility.getPanelRange(panelAuthGroup.getAttribute("panel_auth_group_oid"), amountInBaseToPanelGroup);
					if(StringFunction.isBlank(panelRangeOid)){
						mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,TradePortalConstants.NO_PANEL_RANGE_FOR_AMOUNT_INST_TYPE, 
		    					amountInBaseToPanelGroup.toString(), TradePortalConstants.INVOICE );
		    			IssuedError is = new IssuedError();
						is.setErrorCode(TradePortalConstants.NO_PANEL_RANGE_FOR_AMOUNT_INST_TYPE);
						throw new AmsException(is);
		    		}
					invoiceGroup.setAttribute("panel_auth_range_oid", panelRangeOid);
				}
				
			 panelGroupOid  = invoiceGroup.getAttribute("panel_auth_group_oid");
			 panelOplockVal = invoiceGroup.getAttribute("panel_oplock_val");
			 panelRangeOid =  invoiceGroup.getAttribute("panel_auth_range_oid");
			 PanelAuthProcessor panelAuthProcessor = new PanelAuthProcessor(user, TradePortalConstants.INVOICE, mediatorServices.getErrorManager());
		     panelAuthProcessor.init(panelGroupOid, new String[]{panelRangeOid}, panelOplockVal);

		     panelAuthTransactionStatus = panelAuthProcessor.process(invoiceGroup, panelRangeOid, false);
		     invoiceProp.setPanelAuthStatus(panelAuthTransactionStatus);
		     if(TradePortalConstants.AUTHPARTL.equals(panelAuthTransactionStatus) || TradePortalConstants.AUTHFALSE.equals(panelAuthTransactionStatus)){
		    	 invoiceProp.setAuthInGroupLevel(true);
		    	 invoiceProp.setInvoiceGroup(invoiceGroup);
		     }
		     if(TradePortalConstants.AUTHFALSE.equals(panelAuthTransactionStatus)){
		    	 mediatorServices.getErrorManager().getIssuedErrors().removeAllElements();
		     }
			}
		
		// Loop through the list of invoice oids, and processing each one in the
		// invoice list
			for (int i = 0; i < count; i++) {
				invoiceItem = (DocumentHandler) invoiceList.get(i);
				invoiceOid = invoiceItem.getAttribute("/InvoiceData");
				String[] invoiceData = invoiceItem.getAttribute("/InvoiceData")
					.split("/");
				invoiceOid = invoiceData[0];
				optLock = invoiceData[1];
				invoiceSummaryData = (InvoicesSummaryData) mediatorServices
						.createServerEJB("InvoicesSummaryData",Long.parseLong(invoiceOid));
				invoiceStatus = invoiceSummaryData.getAttribute("invoice_status");
				invoiceId = invoiceSummaryData.getAttribute("invoice_id");
				invoiceSummaryData.setAttribute("tp_rule_name", invoiceSummaryData.getTpRuleName());
				invoiceProp.setInvoiceId(invoiceId);
				invoiceProp.setInvoiceOid(invoiceOid);
				String invoiceGroupOid = invoiceSummaryData.getAttribute("invoice_group_oid"); 
			    
				String authorizeRights = null;
				String userWorkGroupOid = user.getAttribute("work_group_oid");
				String firstAuthorizer = invoiceSummaryData.getAttribute("first_authorizing_user_oid");
				String firstAuthorizerWorkGroup = invoiceSummaryData.getAttribute("first_authorizing_work_group_oid");
				invoiceSummaryData.setAttribute("action",  TradePortalConstants.UPLOAD_INV_ACTION_AUTH);
		        invoiceSummaryData.setAttribute("user_oid",userOid);
		 
				// if user has authority rights then only should process, else
				// thow error
				// that user doesn't have right to authorize this perticular
				// invoice. every invoice associate to trading partner rule,
				// so authority type can be diferent for every invoice.it is not
				// depend on user.
				// it is depend on user only when 'look for corporate customer
				// profile' is selected in trading partner refdata of that
				// invoice.
				// if two different workgroups required authority is selected,
				// then user must belong to a group
				if ((TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS).equals(authorizeType)
						&& (StringFunction.isBlank(userWorkGroupOid))) {

					newStatus = TradePortalConstants.UPLOAD_INV_STATUS_AUTH_FAILED;

					mediatorServices.getErrorManager().issueError(UploadInvoiceMediator.class.getName(),
							TradePortalConstants.TRANS_WORK_GROUP_REQUIRED);
					invoiceSummaryData.setAttribute("invoice_status", newStatus);
					invoiceSummaryData.save();
					invoiceSummaryData.remove();
				}
				// ***********************************************************************
				// If dual authorization is required, set the transaction state
				// to  partially authorized if it has not been previously authorized
				// and set the transaction to authorized if there has been a previous
				// authorizer. The second authorizer must not be the same as the first
				// authorizer not the same work groups, depending on the corporate org
				// setting. If dual authorization is not required, set the invoice state
				// to authorized.
				// ***********************************************************************
				else if ((TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS).equals(authorizeType)
						|| (TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS).equals(authorizeType)) {
					authoriseWithDiffWorkGroupFromGroupList(invoiceSummaryData, firstAuthorizer, optLock,
							firstAuthorizerWorkGroup, userWorkGroupOid,authorizeType, mediatorServices,invoiceProp);

				} else if (TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH
						.equals(authorizeType)
						&& corpOrg
								.isPanelAuthEnabled(TradePortalConstants.PAYABLES_PROGRAM_INVOICE)) {
					try {
						panelAuthFromGroupList(invoiceSummaryData,userWorkGroupOid, optLock, mediatorServices,invoiceProp);
					} catch (AmsException e) {
						IssuedError is = e.getIssuedError();
						if (is != null) {
							String errorCode = is.getErrorCode();
							if (TradePortalConstants.REQUIRED_ATTRIBUTE.equals(errorCode)
									|| TradePortalConstants.NO_PANEL_RANGE_FOR_AMOUNT_INST_TYPE.equals(errorCode)) {
								return new DocumentHandler();
							} else
								throw (e);
						}
					}
				} else {
					newStatus = TradePortalConstants.UPLOAD_INV_STATUS_AUTH;

					invoiceSummaryData.setAttribute("first_authorizing_user_oid", userOid);
					invoiceSummaryData.setAttribute("first_authorizing_work_group_oid",userWorkGroupOid);
					invoiceSummaryData.setAttribute("first_authorize_status_date",DateTimeUtility.getGMTDateTime());

					invoiceSummaryData.setAttribute("invoice_status", newStatus);
					invoiceSummaryData.setAttribute("opt_lock", optLock);
					int success = invoiceSummaryData.save();
					String newGroupOid = invoiceSummaryData
							.getAttribute("invoice_group_oid");
					invoiceSummaryData.remove();
					if (success == 1) {
						if (StringFunction.isNotBlank(newGroupOid)&& !invoiceGroupOid.equals(newGroupOid)) {
							InvoiceUtility.updateAttachmentDetails(newGroupOid,
									invoiceGroupOid, invoiceOid,mediatorServices);
						}
						
							// display message...
							mediatorServices.getErrorManager().issueError(UploadInvoiceMediator.class.getName(),TradePortalConstants.INV_AUTH_SUCCESSFUL,
											mediatorServices.getResourceManager().getText("UploadInvoices.InvoiceID",TradePortalConstants.TEXT_BUNDLE),
											invoiceId);
							
							InvoiceUtility.createOutgoingQueueAndIssueSuccess(newStatus, invoiceOid, false,mediatorServices);
						
						}
					}
				}
			 }
			} catch (Exception e) {
				System.out
						.println("General exception caught in UploadInvoiceMediator:authorizeInvoice");
				e.printStackTrace();
			}
		return (new DocumentHandler());
	} 
	
	 /* Panel Auth process for Payables Invoices*/
    private void panelAuthFromGroupList(InvoicesSummaryData invoiceSummaryData,String userWorkGroupOid ,String optLock,
    		MediatorServices mediatorServices,InvoiceProperties invProp)throws RemoteException, AmsException {
    	PanelAuthorizationGroup panelAuthGroup = null;
		String panelGroupOid = null;
		String panelOplockVal = null;
		String panelRangeOid = null;
		String newStatus = "";
		String panelAuthTransactionStatus = invProp.getPanelAuthStatus();
        invoiceSummaryData.setAttribute("reqPanleAuth",  TradePortalConstants.INDICATOR_YES);

		if(!invProp.isAuthInGroupLevel()){
			if(StringFunction.isBlank(invoiceSummaryData.getAttribute("panel_auth_group_oid"))) {
			
			panelGroupOid = PanelAuthUtility.getPanelAuthorizationGroupOid(TradePortalConstants.PAYABLES_PROGRAM_INVOICE, invProp.getCorpOrg(), mediatorServices.getErrorManager());   				
			if(StringFunction.isNotBlank(panelGroupOid)){
				panelAuthGroup = (PanelAuthorizationGroup) mediatorServices.createServerEJB("PanelAuthorizationGroup",
						Long.parseLong(panelGroupOid));   					
			}else{
				// if panel id is blank, return the flow back.
				IssuedError is = new IssuedError();
				is.setErrorCode(TradePortalConstants.REQUIRED_ATTRIBUTE);
				throw new AmsException(is);
			}
			
			invoiceSummaryData.setAttribute("panel_auth_group_oid", panelAuthGroup.getAttribute("panel_auth_group_oid"));
			invoiceSummaryData.setAttribute("panel_oplock_val", panelAuthGroup.getAttribute("opt_lock"));
			
			String baseCurrencyToPanelGroup = PanelAuthUtility.getBaseCurrency( invProp.getCorpOrg(), panelAuthGroup, null, TradePortalConstants.INVOICE);
			//IR 27059- use payment_amount if present
			String payAmount = StringFunction.isNotBlank(invoiceSummaryData.getAttribute("payment_amount"))?
					invoiceSummaryData.getAttribute("payment_amount"):invoiceSummaryData.getAttribute("amount");
    		BigDecimal amountInBaseToPanelGroup =  PanelAuthUtility.getAmountInBaseCurrency
    		   (invoiceSummaryData.getAttribute("currency"),payAmount, baseCurrencyToPanelGroup, invProp.getCorpOrgOid(), 
    				   TradePortalConstants.INVOICE, null, false, mediatorServices.getErrorManager());
    		
    		panelRangeOid = PanelAuthUtility.getPanelRange(panelAuthGroup.getAttribute("panel_auth_group_oid"), amountInBaseToPanelGroup);
    		if(StringFunction.isBlank(panelRangeOid)){

    			IssuedError is = new IssuedError();
				is.setErrorCode(TradePortalConstants.NO_PANEL_RANGE_FOR_AMOUNT_INST_TYPE);
				throw new AmsException(is);
    		}
    		invoiceSummaryData.setAttribute("panel_auth_range_oid", panelRangeOid);
			
		}
		
		panelGroupOid  = invoiceSummaryData.getAttribute("panel_auth_group_oid");
	    panelOplockVal = invoiceSummaryData.getAttribute("panel_oplock_val");
	    panelRangeOid =  invoiceSummaryData.getAttribute("panel_auth_range_oid");   	
	    
       PanelAuthProcessor panelAuthProcessor = new PanelAuthProcessor(invProp.getUser(), TradePortalConstants.INVOICE, mediatorServices.getErrorManager());
       panelAuthProcessor.init(panelGroupOid, new String[] {panelRangeOid}, panelOplockVal);

        panelAuthTransactionStatus = panelAuthProcessor.process(invoiceSummaryData, panelRangeOid, false);
      }
		
       if (TradePortalConstants.AUTHTRUE.equals(panelAuthTransactionStatus)) {
			newStatus = TradePortalConstants.UPLOAD_INV_STATUS_AUTH;

			invoiceSummaryData.setAttribute("first_authorizing_user_oid",invProp.getUserOid());
			invoiceSummaryData.setAttribute("first_authorizing_work_group_oid",userWorkGroupOid);
			invoiceSummaryData.setAttribute("first_authorize_status_date",DateTimeUtility.getGMTDateTime());

			invoiceSummaryData.setAttribute("invoice_status", newStatus);
			invoiceSummaryData.setAttribute("opt_lock", optLock);
			String invoiceGroupOid = invoiceSummaryData.getAttribute("invoice_group_oid");
			int success = invoiceSummaryData.save();
			String newGroupOid = invoiceSummaryData.getAttribute("invoice_group_oid");
			if (success == 1) {
				if (StringFunction.isNotBlank(newGroupOid) && !invoiceGroupOid.equals(newGroupOid)) {
					InvoiceUtility.updateAttachmentDetails(newGroupOid,invoiceGroupOid, invProp.getInvoiceOid(), mediatorServices);
				}
				
				// display message...
				mediatorServices.getErrorManager().issueError(
						UploadInvoiceMediator.class.getName(),TradePortalConstants.INV_AUTH_SUCCESSFUL,
						mediatorServices.getResourceManager().getText(
								"UploadInvoices.InvoiceID",TradePortalConstants.TEXT_BUNDLE), invProp.getInvoiceId());
				InvoiceUtility.createOutgoingQueueAndIssueSuccess(newStatus,invProp.getInvoiceOid(), false, mediatorServices);

			}
       }else if (TradePortalConstants.AUTHPARTL.equals(panelAuthTransactionStatus)) {
    	  		
			newStatus = TradePortalConstants.UPLOAD_INV_STATUS_PAR_AUTH;

			invoiceSummaryData.setAttribute("first_authorizing_user_oid",invProp.getUserOid());
			invoiceSummaryData.setAttribute("first_authorizing_work_group_oid",userWorkGroupOid);
			invoiceSummaryData.setAttribute("first_authorize_status_date",DateTimeUtility.getGMTDateTime());

			invoiceSummaryData.setAttribute("invoice_status", newStatus);
			invoiceSummaryData.setAttribute("opt_lock", optLock);
			InvoiceGroup invoiceGroup = invProp.getInvoiceGroup();
			if(StringFunction.isNotBlank(invProp.getNewGroupOid()) && invoiceGroup!=null){
					invoiceSummaryData.setAttribute("invoice_group_oid",invProp.getNewGroupOid());
					
					invoiceSummaryData.setAttribute("panel_auth_group_oid",invoiceGroup.getAttribute("panel_auth_group_oid"));
					invoiceSummaryData.setAttribute("panel_oplock_val",invoiceGroup.getAttribute("panel_oplock_val"));
					invoiceSummaryData.setAttribute("panel_auth_range_oid",invoiceGroup.getAttribute("panel_auth_range_oid"));
					if(!invProp.isDisplayMsg()){
						invoiceGroup.save();
						// display message...
						mediatorServices.getErrorManager().issueError(UploadInvoiceMediator.class.getName(),
								TradePortalConstants.TRANS_PARTIALLY_AUTHORIZED,mediatorServices.getResourceManager().getText(
										"InvoiceGroups.InvoiceGroup",TradePortalConstants.TEXT_BUNDLE), invProp.getDescription());
						invProp.setDisplayMsg(true);
					}
			}
			int success = invoiceSummaryData.save();
			
    	   
       }else if (TradePortalConstants.AUTHFALSE.equals(panelAuthTransactionStatus)) {
    	  
				newStatus = TradePortalConstants.UPLOAD_INV_STATUS_AUTH_FAILED;
				invoiceSummaryData.setAttribute("invoice_status", newStatus);
				invoiceSummaryData.setAttribute("opt_lock", optLock);
				//IR 27465 start
				InvoiceGroup invoiceGroup = invProp.getInvoiceGroup();
				if(StringFunction.isNotBlank(invProp.getNewGroupOid()) && invoiceGroup!=null){
						invoiceSummaryData.setAttribute("invoice_group_oid",invProp.getNewGroupOid());
						if(!invProp.isDisplayMsg()){
							invoiceGroup.setAttribute("invoice_status", newStatus);
							invoiceGroup.save();
							mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,TradePortalConstants.AUTHORISER_NOT_SPECIFIED);
							invProp.setDisplayMsg(true);
						}
				}//IR 27465 end
				invoiceSummaryData.save();
       } 
    }
    
    private void authoriseWithDiffWorkGroupFromGroupList(InvoicesSummaryData invoiceSummaryData,  String firstAuthorizer,
			String optLock, String firstAuthorizerWorkGroup,String userWorkGroupOid, String authorizeType,
			MediatorServices mediatorServices,InvoiceProperties invProp) throws RemoteException,AmsException, RemoveException {
				
		String newStatus = "";
		if (StringFunction.isBlank(firstAuthorizer)) {
			newStatus = TradePortalConstants.UPLOAD_INV_STATUS_PAR_AUTH;
			invoiceSummaryData.setAttribute("first_authorizing_user_oid",
					invProp.getUserOid());
			invoiceSummaryData.setAttribute("first_authorizing_work_group_oid",
					userWorkGroupOid);
			invoiceSummaryData.setAttribute("first_authorize_status_date",
					DateTimeUtility.getGMTDateTime());

			invoiceSummaryData.setAttribute("invoice_status", newStatus);

			if(StringFunction.isNotBlank(invProp.getNewGroupOid())){
			invoiceSummaryData.setAttribute("invoice_group_oid",invProp.getNewGroupOid());
			}
			// Set the optimistic lock attribute to make sure the invoice hasn't
			// been changed by someone else since it was first retrieved
			invoiceSummaryData.setAttribute("opt_lock", optLock);
			int success = invoiceSummaryData.save();
			if (success == 1) {
				// display message...
				if(!invProp.isDisplayMsg()){
					
				mediatorServices.getErrorManager().issueError(
						UploadInvoiceMediator.class.getName(),
						TradePortalConstants.TRANS_PARTIALLY_AUTHORIZED,
						mediatorServices.getResourceManager().getText(
								"InvoiceGroups.InvoiceGroup",
								TradePortalConstants.TEXT_BUNDLE), invProp.getDescription());
				invProp.setDisplayMsg(true);
				}

			}
			invoiceSummaryData.remove();
		}
		// if firstAuthorizer is not empty
		else {
			// Two work groups required and this user belongs to the
			// same work group as the first authorizer: Error.
			if ((TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS)
					.equals(authorizeType)
					&& userWorkGroupOid.equals(firstAuthorizerWorkGroup)) {
				newStatus = TradePortalConstants.UPLOAD_INV_STATUS_AUTH_FAILED;

				mediatorServices.getErrorManager().issueError(UploadInvoiceMediator.class.getName(),
						TradePortalConstants.TRANS_DIFF_WORK_GROUP_REQUIRED);
				invoiceSummaryData.setAttribute("opt_lock", optLock);
				invoiceSummaryData.setAttribute("invoice_status", newStatus);
				//IR 27465 start
				InvoiceGroup invoiceGroup = invProp.getInvoiceGroup();
				if(StringFunction.isNotBlank(invProp.getNewGroupOid()) && invoiceGroup!=null){
						invoiceSummaryData.setAttribute("invoice_group_oid",invProp.getNewGroupOid());
						if(!invProp.isDisplayMsg()){
							invoiceGroup.setAttribute("invoice_status", newStatus);
							invoiceGroup.save();
							invProp.setDisplayMsg(true);
						}
				}
				//IR 27465 end
				invoiceSummaryData.save() ;
				invoiceSummaryData.remove();
			}
			// Two authorizers required and this user is the same as the
			// first authorizer. Give error.
			// Note requiring two work groups also implies requiring two
			// users.
			else if (firstAuthorizer.equals(invProp.getUserOid())) {
				newStatus = TradePortalConstants.UPLOAD_INV_STATUS_AUTH_FAILED;

				mediatorServices.getErrorManager().issueError(UploadInvoiceMediator.class.getName(),
						TradePortalConstants.DIFFERENT_SECOND_AUTH_REQRUIED);
				invoiceSummaryData.setAttribute("invoice_status", newStatus);
				invoiceSummaryData.setAttribute("opt_lock", optLock); 
				//IR 27465 start
				InvoiceGroup invoiceGroup = invProp.getInvoiceGroup();
				if(StringFunction.isNotBlank(invProp.getNewGroupOid()) && invoiceGroup!=null){
						invoiceSummaryData.setAttribute("invoice_group_oid",invProp.getNewGroupOid());
						if(!invProp.isDisplayMsg()){
							invoiceGroup.setAttribute("invoice_status", newStatus);
							invoiceGroup.save();
							invProp.setDisplayMsg(true);
						}
				}
				//IR 27465 end
				invoiceSummaryData.save();
				invoiceSummaryData.remove();
			}
			// Authorize
			else {
				newStatus = TradePortalConstants.UPLOAD_INV_STATUS_AUTH;

				invoiceSummaryData.setAttribute("second_authorizing_user_oid",
						invProp.getUserOid());
				invoiceSummaryData.setAttribute(
						"second_authorizing_work_group_oid", userWorkGroupOid);
				invoiceSummaryData.setAttribute("second_authorize_status_date",
						DateTimeUtility.getGMTDateTime());

				invoiceSummaryData.setAttribute("invoice_status", newStatus);
				invoiceSummaryData.setAttribute("opt_lock", optLock);
				
				int success = invoiceSummaryData.save();
				invoiceSummaryData.remove();
				if (success == 1) {
					
					// display message...
					mediatorServices.getErrorManager().issueError(UploadInvoiceMediator.class.getName(),
							TradePortalConstants.INV_AUTH_SUCCESSFUL,mediatorServices.getResourceManager().getText(
									"UploadInvoices.InvoiceID",TradePortalConstants.TEXT_BUNDLE),invProp.getInvoiceId());
					InvoiceUtility.createOutgoingQueueAndIssueSuccess(newStatus, invProp.getInvoiceOid(), false, mediatorServices);

				}
			}
		}
		
	}

	private class InvoiceProperties {
		private String oldInvoiceStatus = "";
		private String buttonPressed = "";
		private User user = null;
		private CorporateOrganization corpOrg = null;
		private InvoiceGroup invoiceGroup = null;
		private String corpOrgOid = "";
		private String newGroupOid = "";
		private String invoiceId = "";
		private String userOid = "";
		private String invoiceOid = "";
		private String description = "";
		private String panelAuthStatus = "";
		private boolean isAuthInGroupLevel = false;
		private boolean displayMsg =false;
		public boolean isDisplayMsg() {
			return displayMsg;
		}
		public void setDisplayMsg(boolean displayMsg) {
			this.displayMsg = displayMsg;
		}
		public boolean isAuthInGroupLevel() {
			return isAuthInGroupLevel;
		}
		public void setAuthInGroupLevel(boolean isPanelInGroupLevel) {
			this.isAuthInGroupLevel = isPanelInGroupLevel;
		}
		public String getPanelAuthStatus() {
			return panelAuthStatus;
		}
		public void setPanelAuthStatus(String panelAuthStatus) {
			this.panelAuthStatus = panelAuthStatus;
		}
		public String getOldInvoiceStatus() {
			return oldInvoiceStatus;
		}
		public void setOldInvoiceStatus(String oldInvoiceStatus) {
			this.oldInvoiceStatus = oldInvoiceStatus;
		}
		public String getButtonPressed() {
			return buttonPressed;
		}
		public void setButtonPressed(String buttonPressed) {
			this.buttonPressed = buttonPressed;
		}
		public User getUser() {
			return user;
		}
		public void setUser(User user) {
			this.user = user;
		}
		public String getNewGroupOid() {
			return newGroupOid;
		}
		public void setNewGroupOid(String newGroupOid) {
			this.newGroupOid = newGroupOid;
		}
		public CorporateOrganization getCorpOrg() {
			return corpOrg;
		}
		public void setCorpOrg(CorporateOrganization corpOrg) {
			this.corpOrg = corpOrg;
		}
		public String getCorpOrgOid() {
			return corpOrgOid;
		}
		public void setCorpOrgOid(String corpOrgOid) {
			this.corpOrgOid = corpOrgOid;
		}
		public String getInvoiceId() {
			return invoiceId;
		}
		public void setInvoiceId(String invocieId) {
			this.invoiceId = invocieId;
		}
		public String getDescription() {
			return description;
		}
		public void setDescription(String description) {
			this.description = description;
		}
		public String getUserOid() {
			return userOid;
		}
		public void setUserOid(String userOid) {
			this.userOid = userOid;
		}
		public String getInvoiceOid() {
			return invoiceOid;
		}
		public void setInvoiceOid(String invoiceOid) {
			this.invoiceOid = invoiceOid;
		}
		public InvoiceGroup getInvoiceGroup() {
			return invoiceGroup;
		}
		public void setInvoiceGroup(InvoiceGroup invoiceGroup) {
			this.invoiceGroup = invoiceGroup;
		}
		
	}
	
	   /**
     * This method performs authorization of all seleted Invoices.
     * invoice with status 'Authorized failed', 'Partially authorized', 'available for authorization' or 'Pending Action - Credit Notes'
     * @param inputDoc -  this xml input doc which mediator gets
     * @param mediatorServices
     * @return DocumentHandler
     * @throws RemoteException
     * @throws AmsException
     */
    private DocumentHandler authorizeInvoiceFromList(DocumentHandler inputDoc, MediatorServices mediatorServices)
    throws RemoteException, AmsException {	  
	
      Vector invoiceList = null;
	   String invoiceOid = null;
	   String optLock = null;//BSL CR710 02/06/2012 Rel8.0 - implement optimistic locking
	   DocumentHandler invoiceItem = null;
	   int count = 0;
	   invoiceList           = inputDoc.getFragments("/UploadInvoiceList/InvoicesSummaryData");
	   String corpOrgOid     = inputDoc.getAttribute("/UploadInvoiceList/ownerOrg");
	   String userOid        = inputDoc.getAttribute("/UploadInvoiceList/userOid");
	   String securityRights = inputDoc.getAttribute("/UploadInvoiceList/securityRights");
	   String buttonPressed = inputDoc.getAttribute("/Update/ButtonPressed");
	   InvoiceGroup invoiceGroup =  null;
		String panelGroupOid = null;
		String panelOplockVal = null;
		String panelRangeOid = null;
		String newStatus = "";
		String panelAuthTransactionStatus = "";
		if (!SecurityAccess.hasRights(securityRights, SecurityAccess.RECEIVABLES_AUTHORIZE_INVOICE)) {
			// user does not have appropriate authority.
			mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.NO_ACTION_MATCH_NOTICE_AUTH,
					mediatorServices.getResourceManager().getText(
							"UploadInvoiceAction.Authorize",
							TradePortalConstants.TEXT_BUNDLE));
			return new DocumentHandler();
		}
	   count = invoiceList.size();
	   mediatorServices.debug("InvoiceMediator: number of invoices selected: " + count);
	   if (count <= 0) {
			mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.NO_ITEM_SELECTED, 
					mediatorServices.getResourceManager().getText("UploadInvoiceAction.Authorize", TradePortalConstants.TEXT_BUNDLE));
			return new DocumentHandler();
	   }
	   String subsAuth = inputDoc.getAttribute("/Authorization/authorizeAtParentViewOrSubs");
		String invoiceGroupPayAmt="";
		String invoiceGroupCurrency ="";
		CorporateOrganization corpOrg = (CorporateOrganization) mediatorServices.createServerEJB("CorporateOrganization",
				Long.parseLong(corpOrgOid));
		User user = (User) mediatorServices.createServerEJB("User",Long.parseLong(userOid));
		String authorizeType = corpOrg.getAttribute("dual_auth_upload_pay_inv");
		String clientBankOid = corpOrg.getAttribute("client_bank_oid"); 
		String baseCurrency = corpOrg.getAttribute("base_currency_code");
    
     String baseTradingPartnerSql = "SELECT RECEIVABLE_INVOICE, RECEIVABLE_AUTHORISE, CREDIT_NOTE, CREDIT_AUTHORISE"
			+ " FROM AR_MATCHING_RULE WHERE P_CORP_ORG_OID = " + corpOrgOid;
	 Map<String, DocumentHandler> tradingPartnerXmlCache = new HashMap<String, DocumentHandler>();
	 List<Object> sqlPrmLst = new ArrayList<Object>();	
	 StringBuilder sql = new StringBuilder("select sum( case when payment_amount is null then amount else payment_amount end) as AMOUNT "
	 		+ "from invoices_summary_data where upload_invoice_oid in (");
		for (int i = 0; i < count; i++) {
			invoiceItem = (DocumentHandler) invoiceList.get(i);
			invoiceOid = invoiceItem.getAttribute("/InvoiceData");
			String[] invoiceData = invoiceItem.getAttribute("/InvoiceData").split("/");
			sql.append("?");
			invoiceOid = invoiceData[0];
			sqlPrmLst.add(invoiceOid);
			if (i != (count - 1)) 
			sql.append(",");
		}
		sql.append(")");
		DocumentHandler resultXml = DatabaseQueryBean.getXmlResultSet(sql.toString(), false, sqlPrmLst);
		if (resultXml != null) {
			DocumentHandler record = resultXml.getFragment("/ResultSetRecord");
			if(record!=null){
				invoiceGroupPayAmt = record.getAttribute("/AMOUNT");
			}
		}
		//pick one of the invoice from the list and get the status and currency
		InvoicesSummaryData invoiceSummaryData = (InvoicesSummaryData) mediatorServices
					.createServerEJB("InvoicesSummaryData",Long.parseLong(invoiceOid));
		invoiceGroupCurrency = invoiceSummaryData.getAttribute("currency");
		String invoiceStatus = invoiceSummaryData.getAttribute("invoice_status");
		String invoiceId = invoiceSummaryData.getAttribute("invoice_id");
		boolean isCreditNote  = invoiceSummaryData.isCreditNote();
		boolean isFinanceable = !TradePortalConstants.INDICATOR_YES.equals(invoiceSummaryData.getAttribute("portal_approval_requested_ind")) &&  InvoiceUtility.isFinanceable(invoiceStatus);
		
		String authorizeRights    = null;
		ArMatchingRule rule = invoiceSummaryData.getMatchingRule();
		if(rule != null)
		{
			if (isCreditNote) {
				authorizeRights = rule.getAttribute("credit_note");
				authorizeType = rule.getAttribute("credit_authorise");
			}
			else {
				authorizeRights = rule.getAttribute("receivable_invoice");
				authorizeType = rule.getAttribute("receivable_authorise");
			}	 
			if (TradePortalConstants.DUAL_AUTH_REQ_INV_CORPORATE.equals(authorizeType)) {
				if (isCreditNote) {
					authorizeRights = corpOrg.getAttribute("allow_credit_note");
					authorizeType = corpOrg.getAttribute("dual_auth_credit_note");
				}
				else {
					authorizeRights = corpOrg.getAttribute("allow_arm_invoice");
					authorizeType = corpOrg.getAttribute("dual_auth_arm_invoice");
				}
			}
		}
		if (!TradePortalConstants.INDICATOR_YES.equals(authorizeRights)) 
		{
			mediatorServices.getErrorManager().issueError(
					PayRemitMediator.class.getName(),
					TradePortalConstants.NO_ACTION_TRANSACTION_AUTH,
					invoiceId ,
					mediatorServices.getResourceManager().getText("TransactionAction.Authorize", TradePortalConstants.TEXT_BUNDLE));
			return new DocumentHandler();

		} 
		
		// Check threshold amount start
		
		String selectClause = "select distinct upload_invoice_oid, currency as currency_code,amount as payment_amount from invoices_summary_data ";

		boolean isValid = InvoiceUtility.checkThresholds(invoiceGroupPayAmt,invoiceGroupCurrency, baseCurrency, "upload_rec_inv_thold",
				"upload_rec_inv_dlimit", corpOrgOid, clientBankOid,user, selectClause, false, subsAuth, mediatorServices);
		if (!isValid) {
			return new DocumentHandler();
		}
		// threshold check end
		
		InvoiceProperties invoiceProp = new InvoiceProperties();
		invoiceProp.setOldInvoiceStatus(invoiceStatus);
		invoiceProp.setUser(user);
		invoiceProp.setUserOid(userOid);
		invoiceProp.setCorpOrg(corpOrg);
		invoiceProp.setCorpOrgOid(corpOrgOid);
		if((TradePortalConstants.UPLOAD_INV_STATUS_PAR_AUTH.equals(invoiceStatus) 
				|| TradePortalConstants.UPLOAD_INV_STATUS_FIN_PAR_AUTH.equals(invoiceStatus))){
			mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,TradePortalConstants.INVALID_GROUP_AUTH);
			return new DocumentHandler();
		}
		if(!TradePortalConstants.INV_LINKED_INSTR_REC.equals(invoiceSummaryData.getAttribute("linked_to_instrument_type"))){
			mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.PAYABLE_MGMT_INVALID_ACTION,mediatorServices.getResourceManager().getText("TransactionAction.Authorized",
							TradePortalConstants.TEXT_BUNDLE),invoiceId,invoiceSummaryData.getAttribute("linked_to_instrument_type"));
			return new DocumentHandler();
		}
		// only 'Available for Authorization', 'Auth Failed', invoices can be authorized, when navigating from Group
		//( partially auth invoices are checked above)
	    if (!InvoiceUtility.isAuthorizable(invoiceStatus)){
	    	mediatorServices.getErrorManager().issueError(
					UploadInvoiceMediator.class.getName(),
					TradePortalConstants.TRANSACTION_CANNOT_PROCESS, mediatorServices.getResourceManager().getText("UploadInvoices.InvoiceID",TradePortalConstants.TEXT_BUNDLE),
					invoiceId ,
					ReferenceDataManager.getRefDataMgr().getDescr("UPLOAD_INVOICE_STATUS", invoiceStatus, mediatorServices.getCSDB().getLocaleName()),
					mediatorServices.getResourceManager().getText("TransactionAction.Authorized", TradePortalConstants.TEXT_BUNDLE));  

			
	    	return new DocumentHandler();
		}
		
		//If status is avaliable_for_Auth and corp setting is panel auth or dual auth then create INVOICE_GROUP first
		if((TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH.equals(authorizeType) &&
	  			corpOrg.isPanelAuthEnabled(TradePortalConstants.INVOICE)) ||
				((TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS.equals(authorizeType)
				|| TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS.equals(authorizeType)) 
				&& StringFunction.isBlank(invoiceSummaryData.getAttribute("first_authorizing_user_oid")))){
			invoiceSummaryData.setAttribute("invoice_status",TradePortalConstants.UPLOAD_INV_STATUS_PAR_AUTH);
			invoiceSummaryData.setAttribute("tp_rule_name", invoiceSummaryData.getTpRuleName());
			String newGrpOid= invoiceSummaryData.createInvoiceGroup();
			invoiceProp.setNewGroupOid(newGrpOid);
			invoiceSummaryData.setAttribute("invoice_status",invoiceProp.getOldInvoiceStatus());
			invoiceGroup = (InvoiceGroup) mediatorServices.createServerEJB("InvoiceGroup",Long.parseLong(newGrpOid));
			invoiceProp.setDescription(invoiceGroup.getDescription());
		}
		
		//for Panel Auth
		if(TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH.equals(authorizeType) &&
		  			corpOrg.isPanelAuthEnabled(TradePortalConstants.INVOICE) && invoiceGroup!=null) {
			PanelAuthorizationGroup panelAuthGroup = null;
			if(StringFunction.isBlank(invoiceGroup.getAttribute("panel_auth_group_oid"))) {
				
				panelGroupOid = PanelAuthUtility.getPanelAuthorizationGroupOid(TradePortalConstants.INVOICE, corpOrg, mediatorServices.getErrorManager());   				
				if(StringFunction.isNotBlank(panelGroupOid)){
					panelAuthGroup = (PanelAuthorizationGroup) mediatorServices.createServerEJB("PanelAuthorizationGroup",
							Long.parseLong(panelGroupOid));   					
				}else{
					// issue error as panel group is required if panel auth defined for instrument type
					mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,AmsConstants.REQUIRED_ATTRIBUTE, 
							mediatorServices.resMgr.getText("CorpCust.PanelGroupId", TradePortalConstants.TEXT_BUNDLE));
					// if panel id is blank, return the flow back.
					IssuedError is = new IssuedError();
					is.setErrorCode(TradePortalConstants.REQUIRED_ATTRIBUTE);
					throw new AmsException(is);
				}
			
				invoiceGroup.setAttribute("panel_auth_group_oid", panelAuthGroup.getAttribute("panel_auth_group_oid"));
				invoiceGroup.setAttribute("panel_oplock_val", panelAuthGroup.getAttribute("opt_lock"));
				
				String baseCurrencyToPanelGroup = PanelAuthUtility.getBaseCurrency(corpOrg, panelAuthGroup, null, TradePortalConstants.INVOICE);
	 		   BigDecimal amountInBaseToPanelGroup =  PanelAuthUtility.getAmountInBaseCurrency
	 		   (invoiceGroupCurrency, invoiceGroupPayAmt, baseCurrencyToPanelGroup, corpOrgOid, 
	 				   TradePortalConstants.INVOICE, null, false, mediatorServices.getErrorManager()); 
				
				panelRangeOid = PanelAuthUtility.getPanelRange(panelAuthGroup.getAttribute("panel_auth_group_oid"), amountInBaseToPanelGroup);
				if(StringFunction.isBlank(panelRangeOid)){
					mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,TradePortalConstants.NO_PANEL_RANGE_FOR_AMOUNT_INST_TYPE, 
	    					amountInBaseToPanelGroup.toString(), TradePortalConstants.INVOICE );
	    			IssuedError is = new IssuedError();
					is.setErrorCode(TradePortalConstants.NO_PANEL_RANGE_FOR_AMOUNT_INST_TYPE);
					throw new AmsException(is);
	    		}
				invoiceGroup.setAttribute("panel_auth_range_oid", panelRangeOid);
			}
			
		 panelGroupOid  = invoiceGroup.getAttribute("panel_auth_group_oid");
		 panelOplockVal = invoiceGroup.getAttribute("panel_oplock_val");
		 panelRangeOid =  invoiceGroup.getAttribute("panel_auth_range_oid");
		 PanelAuthProcessor panelAuthProcessor = new PanelAuthProcessor(user, TradePortalConstants.INVOICE, mediatorServices.getErrorManager());
	     panelAuthProcessor.init(panelGroupOid, new String[]{panelRangeOid}, panelOplockVal);

	     panelAuthTransactionStatus = panelAuthProcessor.process(invoiceGroup, panelRangeOid, false);
	     invoiceProp.setPanelAuthStatus(panelAuthTransactionStatus);
	     if(TradePortalConstants.AUTHPARTL.equals(panelAuthTransactionStatus)){
	    	 invoiceProp.setAuthInGroupLevel(true);
	    	 invoiceProp.setInvoiceGroup(invoiceGroup);
	     }
	     else if(TradePortalConstants.AUTHFALSE.equals(panelAuthTransactionStatus)){
	    	 mediatorServices.getErrorManager().getIssuedErrors().removeAllElements();
	     }
		}
		SimpleDateFormat formatter=new SimpleDateFormat("MM/dd/yyyy");
	// Loop through the list of invoice oids, and processing each one in the invoice list
	for (int i = 0; i < count; i++) 
	{
		 invoiceItem = (DocumentHandler) invoiceList.get(i);
		 invoiceOid = invoiceItem.getAttribute("/InvoiceData");		 
		 String[] invoiceData = invoiceItem.getAttribute("/InvoiceData").split("/");
		 invoiceOid = invoiceData[0];
		 optLock = invoiceData[1];

		try
		{ 
			invoiceSummaryData = (InvoicesSummaryData) mediatorServices.createServerEJB("InvoicesSummaryData", Long.parseLong(invoiceOid));
			invoiceStatus  = invoiceSummaryData.getAttribute("invoice_status");
			invoiceId      = invoiceSummaryData.getAttribute("invoice_id");
			String invoiceGroupOid  = invoiceSummaryData.getAttribute("invoice_group_oid"); 
			String tpName=invoiceSummaryData.getTpRuleName(); 
			invoiceSummaryData.setAttribute("tp_rule_name", tpName);
			String userWorkGroupOid         = user.getAttribute("work_group_oid");
			String firstAuthorizer          = invoiceSummaryData.getAttribute("first_authorizing_user_oid");
			String firstAuthorizerWorkGroup = invoiceSummaryData.getAttribute("first_authorizing_work_group_oid");	  	        
			invoiceSummaryData.setAttribute("action",  TradePortalConstants.UPLOAD_INV_ACTION_AUTH);
	        invoiceSummaryData.setAttribute("user_oid", userOid);

			boolean isCompliant = true;
			// if two different workgroups required authority is selected, then user must belong to a group
			if ((TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS).equals(authorizeType)
					&& (StringFunction.isBlank(userWorkGroupOid))) 
			{
				newStatus = !isFinanceable?TradePortalConstants.UPLOAD_INV_STATUS_AUTH_FAILED:TradePortalConstants.UPLOAD_INV_STATUS_FIN_AUTH_FAILED;
				
				mediatorServices.getErrorManager().issueError(
						PayRemitMediator.class.getName(),
						TradePortalConstants.TRANS_WORK_GROUP_REQUIRED);
				invoiceSummaryData.setAttribute("tp_rule_name", invoiceSummaryData.getTpRuleName());
				invoiceSummaryData.setAttribute("invoice_status", newStatus);
				
				invoiceSummaryData.save() ;
			}
			//***********************************************************************
			// If dual authorization is required, set the transaction state to
			// partially authorized if it has not been previously authorized and set
			// the transaction to authorized if there has been a previous
			// authorizer.
			// The second authorizer must not be the same as the first authorizer
			// not the same work groups, depending on the corporate org setting.
			// If dual authorization is not required, set the invoice state to
			// authorized.
			// ***********************************************************************    
			else if ((TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS).equals(authorizeType)
					||(TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS).equals(authorizeType))
			{
				if (StringFunction.isBlank(firstAuthorizer)) 
				{
					if (isFinanceable)
					{		
						try {
							InvoiceUtility.calculateNumTenorDays(rule,null,formatter,invoiceSummaryData.getAttribute("due_date"),invoiceSummaryData.getAttribute("payment_date"));// checks compliance
							newStatus = TradePortalConstants.UPLOAD_INV_STATUS_FIN_PAR_AUTH;
						}
						catch (AmsException e) {
							isCompliant = false;
							IssuedError is = e.getIssuedError();
							if (is != null) {
								String errorCode = is.getErrorCode();
								if (TradePortalConstants.ERR_UPLOAD_INV_TENOR_DAYS_LESS.equals(errorCode) ||
										TradePortalConstants.ERR_UPLOAD_INV_DUE_PMT_DAYS_DIFF.equals(errorCode) ||
										TradePortalConstants.ERR_UPLOAD_INV_PCT_VAL_TO_FIN_ZERO.equals(errorCode))
								{
									newStatus = TradePortalConstants.UPLOAD_INV_STATUS_PEND_ACT_UNABLE_TO_FIN;
								}
								else {
									mediatorServices.debug("InvoiceGroupMediatorBean.authorizeInvoice: Unexpected error: " + e.toString());
									throw e;
								}
							}
							else {
								mediatorServices.debug("InvoiceGroupMediatorBean.authorizeInvoice: Unexpected error: " + e.toString());
								throw e;
							}
						}
					}else{
						newStatus = TradePortalConstants.UPLOAD_INV_STATUS_PAR_AUTH;
					}
					if(isCompliant)
					{
						invoiceSummaryData.setAttribute("first_authorizing_user_oid", userOid);
						invoiceSummaryData.setAttribute("first_authorizing_work_group_oid", userWorkGroupOid);
						invoiceSummaryData.setAttribute("first_authorize_status_date", DateTimeUtility.getGMTDateTime());
					}
					invoiceSummaryData.setAttribute("invoice_status", newStatus);	             
					// Set the optimistic lock attribute to make sure the invoice hasn't been changed by someone else since it was first retrieved
					invoiceSummaryData.setAttribute("opt_lock", optLock);//BSL CR710 02/03/2012 Rel8.0 ADD
					if(StringFunction.isNotBlank(invoiceProp.getNewGroupOid())){
						invoiceSummaryData.setAttribute("invoice_group_oid",invoiceProp.getNewGroupOid());
						}
					int success = invoiceSummaryData.save();	                   
					if (success == 1)
					{                   
						//display message...
						if(isCompliant && !invoiceProp.isDisplayMsg()){
							mediatorServices.getErrorManager().issueError(
									UploadInvoiceMediator.class.getName(),TradePortalConstants.TRANS_PARTIALLY_AUTHORIZED,
									mediatorServices.getResourceManager().getText("UploadInvoices.InvoiceID",TradePortalConstants.TEXT_BUNDLE),
									invoiceId);
							invoiceProp.setDisplayMsg(true);
						}
					}
				} 
				// if firstAuthorizer is not empty
				else 
				{
					// Two work groups required and this user belongs to the
					// same work group as the first authorizer: Error.
					if ((TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS).equals(authorizeType)
							&& userWorkGroupOid.equals(firstAuthorizerWorkGroup)) 
					{	
						newStatus = !isFinanceable?TradePortalConstants.UPLOAD_INV_STATUS_AUTH_FAILED:TradePortalConstants.UPLOAD_INV_STATUS_FIN_AUTH_FAILED;
						mediatorServices.getErrorManager().issueError(
								PayRemitMediator.class.getName(),
								TradePortalConstants.TRANS_DIFF_WORK_GROUP_REQUIRED);
						invoiceSummaryData.setAttribute("invoice_status", newStatus);
						invoiceSummaryData.save() ;
					}
					// Two authorizers required and this user is the same as the
					// first authorizer. Give error.
					// Note requiring two work groups also implies requiring two
					// users.
					else if (firstAuthorizer.equals(userOid)) 
					{
						newStatus = !isFinanceable?TradePortalConstants.UPLOAD_INV_STATUS_AUTH_FAILED:TradePortalConstants.UPLOAD_INV_STATUS_FIN_AUTH_FAILED;
						mediatorServices.getErrorManager().issueError(
								PayRemitMediator.class.getName(),
								TradePortalConstants.DIFFERENT_SECOND_AUTH_REQRUIED);
						invoiceSummaryData.setAttribute("invoice_status", newStatus);
						invoiceSummaryData.save();
					}
					// Authorize
					else 
					{
						if (isFinanceable)
						{		
							try {
								InvoiceUtility.calculateNumTenorDays(rule,null,formatter,invoiceSummaryData.getAttribute("due_date"),invoiceSummaryData.getAttribute("payment_date"));// checks compliance
								newStatus = TradePortalConstants.UPLOAD_INV_STATUS_FIN_AUTH;
							}
							catch (AmsException e) {
								isCompliant = false;
								IssuedError is = e.getIssuedError();
								if (is != null) {
									String errorCode = is.getErrorCode();
									if (TradePortalConstants.ERR_UPLOAD_INV_TENOR_DAYS_LESS.equals(errorCode) ||
											TradePortalConstants.ERR_UPLOAD_INV_DUE_PMT_DAYS_DIFF.equals(errorCode) ||
											TradePortalConstants.ERR_UPLOAD_INV_PCT_VAL_TO_FIN_ZERO.equals(errorCode))
									{
										newStatus = TradePortalConstants.UPLOAD_INV_STATUS_PEND_ACT_UNABLE_TO_FIN;
									}
									else {
										mediatorServices.debug("InvoiceGroupMediatorBean.authorizeInvoice: Unexpected error: " + e.toString());
										throw e;
									}
								}
								else {
									mediatorServices.debug("InvoiceGroupMediatorBean.authorizeInvoice: Unexpected error: " + e.toString());
									throw e;
								}
							}
						}else
						{
							newStatus = TradePortalConstants.UPLOAD_INV_STATUS_AUTH;
						}
						if(isCompliant)
						{
							invoiceSummaryData.setAttribute("second_authorizing_user_oid", userOid);
							invoiceSummaryData.setAttribute("second_authorizing_work_group_oid", userWorkGroupOid);
							invoiceSummaryData.setAttribute("second_authorize_status_date", DateTimeUtility.getGMTDateTime());
						} 	
						invoiceSummaryData.setAttribute("invoice_status", newStatus);
						// Set the optimistic lock attribute to make sure the invoice hasn't been changed by someone else since it was first retrieved
						invoiceSummaryData.setAttribute("opt_lock", optLock);
						int success = invoiceSummaryData.save();
						if(success == 1)
						{
							if(isCompliant)
							{		                      
								// display message...
								mediatorServices.getErrorManager().issueError(
										UploadInvoiceMediator.class.getName(),
										TradePortalConstants.INV_AUTH_SUCCESSFUL,
										mediatorServices.getResourceManager().getText("UploadInvoices.InvoiceID",TradePortalConstants.TEXT_BUNDLE),
										invoiceId);
								createOutgoingQueueAndIssueSuccess(newStatus, invoiceOid,invoiceSummaryData.getAttribute("invoice_type"), isCreditNote, mediatorServices);
							}   
						}
					}
				}
			}else if(TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH.equals(authorizeType) 
					&& corpOrg.isPanelAuthEnabled(TradePortalConstants.INVOICE)){
				PanelAuthorizationGroup panelAuthGroup = null;
		        invoiceSummaryData.setAttribute("reqPanleAuth", TradePortalConstants.INDICATOR_YES);

				if(!invoiceProp.isAuthInGroupLevel()){
						
				if(StringFunction.isBlank(invoiceSummaryData.getAttribute("panel_auth_group_oid"))) {
					panelGroupOid = PanelAuthUtility.getPanelAuthorizationGroupOid(TradePortalConstants.INVOICE, corpOrg, mediatorServices.getErrorManager());   				
   				if(StringFunction.isBlank(panelGroupOid)){
   					// issue error as panel group is required if panel auth defined for instrument type
   					mediatorServices.getErrorManager().issueError(
   								TradePortalConstants.ERR_CAT_1,
   								AmsConstants.REQUIRED_ATTRIBUTE, 
   								mediatorServices.getResourceManager().getText("CorpCust.PanelGroupId", TradePortalConstants.TEXT_BUNDLE));
   					// if panel id is blank, return the flow back.
   					return new DocumentHandler();
   				}
   				panelAuthGroup = (PanelAuthorizationGroup) mediatorServices.createServerEJB("PanelAuthorizationGroup",
							Long.parseLong(panelGroupOid));   		
   				invoiceSummaryData.setAttribute("panel_auth_group_oid", panelAuthGroup.getAttribute("panel_auth_group_oid"));
   				invoiceSummaryData.setAttribute("panel_oplock_val", panelAuthGroup.getAttribute("opt_lock"));
   				
   				String baseCurrencyToPanelGroup = PanelAuthUtility.getBaseCurrency(corpOrg, panelAuthGroup, null, TradePortalConstants.INVOICE);
   				
   	    		BigDecimal amountInBaseToPanelGroup =  PanelAuthUtility.getAmountInBaseCurrency
   	    		   (invoiceSummaryData.getAttribute("currency"), invoiceSummaryData.getAttribute("amount"), baseCurrencyToPanelGroup, corpOrgOid, 
   	    				   TradePortalConstants.INVOICE, null, false, mediatorServices.getErrorManager());
   	    		
   	    		panelRangeOid = PanelAuthUtility.getPanelRange(panelAuthGroup.getAttribute("panel_auth_group_oid"), amountInBaseToPanelGroup);
   	    		if(StringFunction.isBlank(panelRangeOid)){
   	    			mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
   							TradePortalConstants.NO_PANEL_RANGE_FOR_AMOUNT_INST_TYPE, amountInBaseToPanelGroup.toString(), TradePortalConstants.INVOICE);
   	    			return new DocumentHandler();
   	    		}
   	    		invoiceSummaryData.setAttribute("panel_auth_range_oid", panelRangeOid);
   			}
				
   			panelGroupOid  = invoiceSummaryData.getAttribute("panel_auth_group_oid");
   		    panelOplockVal = invoiceSummaryData.getAttribute("panel_oplock_val");
   		    panelRangeOid =  invoiceSummaryData.getAttribute("panel_auth_range_oid");   	
   		    
   	       PanelAuthProcessor panelAuthProcessor = new PanelAuthProcessor(user, TradePortalConstants.INVOICE, mediatorServices.getErrorManager());
   	       panelAuthProcessor.init(panelGroupOid, new String[] {panelRangeOid}, panelOplockVal);

   	        panelAuthTransactionStatus = panelAuthProcessor.process(invoiceSummaryData, panelRangeOid, false);
			}
   	       if (TradePortalConstants.AUTHTRUE.equals(panelAuthTransactionStatus)) {
   	    	   newStatus = null;
   	    	   if (isFinanceable)
   					{
   						try {
							InvoiceUtility.calculateNumTenorDays(rule,null,formatter,invoiceSummaryData.getAttribute("due_date"),invoiceSummaryData.getAttribute("payment_date"));// checks compliance
   							newStatus = TradePortalConstants.UPLOAD_INV_STATUS_FIN_AUTH;
   						}
   						catch (AmsException e) {
   							isCompliant = false;
   							IssuedError is = e.getIssuedError();
   							if (is != null) {
   								String errorCode = is.getErrorCode();
   								if (TradePortalConstants.ERR_UPLOAD_INV_TENOR_DAYS_LESS.equals(errorCode) ||
   										TradePortalConstants.ERR_UPLOAD_INV_DUE_PMT_DAYS_DIFF.equals(errorCode) ||
   										TradePortalConstants.ERR_UPLOAD_INV_PCT_VAL_TO_FIN_ZERO.equals(errorCode))
   								{
   									newStatus = TradePortalConstants.UPLOAD_INV_STATUS_PEND_ACT_UNABLE_TO_FIN;
   								}
   								else {
   									mediatorServices.debug("InvoiceGroupMediatorBean.authorizeInvoice: Unexpected error: " + e.toString());
   									throw e;
   								}
   							}
   							else {
   								mediatorServices.debug("InvoiceGroupMediatorBean.authorizeInvoice: Unexpected error: " + e.toString());
   								throw e;
   							}
   						}
   					}else
   					{
   						newStatus = TradePortalConstants.UPLOAD_INV_STATUS_AUTH;
   					}
   					if(isCompliant)
   					{
   						invoiceSummaryData.setAttribute("first_authorizing_user_oid", userOid);
   						invoiceSummaryData.setAttribute("first_authorizing_work_group_oid", userWorkGroupOid);
   						invoiceSummaryData.setAttribute("first_authorize_status_date", DateTimeUtility.getGMTDateTime());
   					}
   					invoiceSummaryData.setAttribute("invoice_status", newStatus);
   					// Set the optimistic lock attribute to make sure the invoice hasn't been changed by someone else since it was first retrieved
   					invoiceSummaryData.setAttribute("opt_lock", optLock);//BSL CR710 02/03/2012 Rel8.0 ADD
   					int success = invoiceSummaryData.save();
   					String newGroupOid = invoiceSummaryData.getAttribute("invoice_group_oid");
   					if(success == 1)
   					{   
   						if(StringFunction.isNotBlank(newGroupOid) && !invoiceGroupOid.equals(newGroupOid)){
   							InvoiceUtility.updateAttachmentDetails(newGroupOid,invoiceGroupOid,invoiceOid,mediatorServices);
   						}
   						if(isCompliant){                        
   							// display message...
   							mediatorServices.getErrorManager().issueError(
   									UploadInvoiceMediator.class.getName(),
   									TradePortalConstants.INV_AUTH_SUCCESSFUL,
   									mediatorServices.getResourceManager().getText("UploadInvoices.InvoiceID",TradePortalConstants.TEXT_BUNDLE),
   									invoiceId);	
   							createOutgoingQueueAndIssueSuccess(newStatus, invoiceOid,invoiceSummaryData.getAttribute("invoice_type"), isCreditNote, mediatorServices);

   						}
   					}
   	       }else if (TradePortalConstants.AUTHPARTL.equals(panelAuthTransactionStatus)) {
   	    	   newStatus = null;
  					if (isFinanceable)
  					{		
  						try {
							InvoiceUtility.calculateNumTenorDays(rule,null,formatter,invoiceSummaryData.getAttribute("due_date"),invoiceSummaryData.getAttribute("payment_date"));// checks compliance
  							newStatus = TradePortalConstants.UPLOAD_INV_STATUS_FIN_PAR_AUTH;
  						}
  						catch (AmsException e) {
  							isCompliant = false;
  							IssuedError is = e.getIssuedError();
  							if (is != null) {
  								String errorCode = is.getErrorCode();
  								if (TradePortalConstants.ERR_UPLOAD_INV_TENOR_DAYS_LESS.equals(errorCode) ||
  										TradePortalConstants.ERR_UPLOAD_INV_DUE_PMT_DAYS_DIFF.equals(errorCode) ||
  										TradePortalConstants.ERR_UPLOAD_INV_PCT_VAL_TO_FIN_ZERO.equals(errorCode))
  								{
  									newStatus = TradePortalConstants.UPLOAD_INV_STATUS_PEND_ACT_UNABLE_TO_FIN;
  								}
  								else {
  									mediatorServices.debug("InvoiceGroupMediatorBean.authorizeInvoice: Unexpected error: " + e.toString());
  									throw e;
  								}
  							}
  							else {
  								mediatorServices.debug("InvoiceGroupMediatorBean.authorizeInvoice: Unexpected error: " + e.toString());
  								throw e;
  							}
  						}
  					}else{
  						newStatus = TradePortalConstants.UPLOAD_INV_STATUS_PAR_AUTH;
  					}
  					if(isCompliant)
  					{
  						invoiceSummaryData.setAttribute("first_authorizing_user_oid", userOid);
  						invoiceSummaryData.setAttribute("first_authorizing_work_group_oid", userWorkGroupOid);
  						invoiceSummaryData.setAttribute("first_authorize_status_date", DateTimeUtility.getGMTDateTime());
  					}
  					invoiceSummaryData.setAttribute("invoice_status", newStatus);	             
  					// Set the optimistic lock attribute to make sure the invoice hasn't been changed by someone else since it was first retrieved
  					invoiceSummaryData.setAttribute("opt_lock", optLock);//BSL CR710 02/03/2012 Rel8.0 ADD
  					
  					if(StringFunction.isNotBlank(invoiceProp.getNewGroupOid()) && invoiceGroup!=null){
  							invoiceSummaryData.setAttribute("invoice_group_oid",invoiceProp.getNewGroupOid());
  							
  							invoiceSummaryData.setAttribute("panel_auth_group_oid",invoiceGroup.getAttribute("panel_auth_group_oid"));
  							invoiceSummaryData.setAttribute("panel_oplock_val",invoiceGroup.getAttribute("panel_oplock_val"));
  							invoiceSummaryData.setAttribute("panel_auth_range_oid",invoiceGroup.getAttribute("panel_auth_range_oid"));
  							if(!invoiceProp.isDisplayMsg()){
  								invoiceGroup.save();
  								// display message...
  								mediatorServices.getErrorManager().issueError(UploadInvoiceMediator.class.getName(),
  										TradePortalConstants.TRANS_PARTIALLY_AUTHORIZED,mediatorServices.getResourceManager().getText(
  												"InvoiceGroups.InvoiceGroup",TradePortalConstants.TEXT_BUNDLE), invoiceProp.getDescription());
  								invoiceProp.setDisplayMsg(true);
  							}
  					}
  					int success = invoiceSummaryData.save();	                   
  					
              }else if (TradePortalConstants.AUTHFALSE.equals(panelAuthTransactionStatus)) {
           	   		newStatus =!isFinanceable?TradePortalConstants.UPLOAD_INV_STATUS_AUTH_FAILED:TradePortalConstants.UPLOAD_INV_STATUS_FIN_AUTH_FAILED;
				  	invoiceSummaryData.setAttribute("invoice_status", newStatus);
					invoiceSummaryData.save();
   	       		} 
			}
			else 
			{
				// if dual auth ind is NO, then last option is first user can authorize invoice and first authorized oid is blank
				if (isFinanceable)
				{
					try {
						InvoiceUtility.calculateNumTenorDays(rule,null,formatter,invoiceSummaryData.getAttribute("due_date"),invoiceSummaryData.getAttribute("payment_date"));// checks compliance
						newStatus = TradePortalConstants.UPLOAD_INV_STATUS_FIN_AUTH;
					}
					catch (AmsException e) {
						isCompliant = false;
						IssuedError is = e.getIssuedError();
						if (is != null) {
							String errorCode = is.getErrorCode();
							//BSL IR NNUM040526786 04/05/2012 BEGIN - add Pct Val To Fn Zero and combine three if branches w/identical logic
							if (TradePortalConstants.ERR_UPLOAD_INV_TENOR_DAYS_LESS.equals(errorCode) ||
									TradePortalConstants.ERR_UPLOAD_INV_DUE_PMT_DAYS_DIFF.equals(errorCode) ||
									TradePortalConstants.ERR_UPLOAD_INV_PCT_VAL_TO_FIN_ZERO.equals(errorCode))
							{
								newStatus = TradePortalConstants.UPLOAD_INV_STATUS_PEND_ACT_UNABLE_TO_FIN;
							}
							//BSL IR NNUM040526786 04/05/2012 END
							else {
								e.printStackTrace();
								throw e;
							} 
						}
						else {
							e.printStackTrace();
							throw e;
						}
					}
				}else
				{
					newStatus = TradePortalConstants.UPLOAD_INV_STATUS_AUTH;
				}
				if(isCompliant)
				{
					invoiceSummaryData.setAttribute("first_authorizing_user_oid", userOid);
					invoiceSummaryData.setAttribute("first_authorizing_work_group_oid", userWorkGroupOid);
					invoiceSummaryData.setAttribute("first_authorize_status_date", DateTimeUtility.getGMTDateTime());
				}
				invoiceSummaryData.setAttribute("invoice_status", newStatus);
				// Set the optimistic lock attribute to make sure the invoice hasn't been changed by someone else since it was first retrieved
				invoiceSummaryData.setAttribute("opt_lock", optLock);//BSL CR710 02/03/2012 Rel8.0 ADD
				int success = invoiceSummaryData.save();
				String newGroupOid = invoiceSummaryData.getAttribute("invoice_group_oid");
				if(success == 1)
				{   
					if(StringFunction.isNotBlank(newGroupOid) && !invoiceGroupOid.equals(newGroupOid)){
						InvoiceUtility.updateAttachmentDetails(newGroupOid,invoiceGroupOid,invoiceOid,mediatorServices);
					}
					
					if(isCompliant){                        
						// display message...
						mediatorServices.getErrorManager().issueError(
								UploadInvoiceMediator.class.getName(),
								TradePortalConstants.INV_AUTH_SUCCESSFUL,
								mediatorServices.getResourceManager().getText("UploadInvoices.InvoiceID",TradePortalConstants.TEXT_BUNDLE),
								invoiceId);	
						createOutgoingQueueAndIssueSuccess(newStatus, invoiceOid,invoiceSummaryData.getAttribute("invoice_type"), isCreditNote, mediatorServices);
					}
				}	   
			}
		}       
		catch(Exception e)
		{
			e.printStackTrace();
		}
	 }  

	 return (new DocumentHandler());
    }
    
    private Boolean selectedInvoicesHaveSameEndtoEndId(Vector invoices)
    		throws AmsException, NumberFormatException, RemoteException{
    	Boolean haveSameE2E = false;
		List<Object> sqlprmLst = new ArrayList<Object>();
        DocumentHandler invoiceItem;
        String[] invoiceData;
		StringBuilder whereClause = new StringBuilder("upload_invoice_oid in (");
        for (int i = 0; invoices != null && i < invoices.size(); i++) {
			invoiceItem = (DocumentHandler) invoices.get(i);
			invoiceData = invoiceItem.getAttribute("/InvoiceData").split("/");
			sqlprmLst.add(invoiceData[0]);
			whereClause.append("?");
			if (i < invoices.size() - 1) {
				whereClause.append(",");
			}

        }
		whereClause.append(")");
        //all selected invoices with non-null end to end id
        StringBuilder invoiceoidSQL =new StringBuilder( "select count(upload_invoice_oid) as cnt from invoices_summary_data where end_to_end_id is not null and ");
		invoiceoidSQL.append(whereClause);
        //all invoices in system matching the corresponding end to end id (non-null)
		StringBuilder end2endSQL = new StringBuilder("select count(upload_invoice_oid) as cnt from invoices_summary_data where end_to_end_id in (select distinct end_to_end_id from invoices_summary_data where end_to_end_id is not null and ");
		end2endSQL.append(whereClause);
		end2endSQL.append(")");

		DocumentHandler invoiceOidCount = DatabaseQueryBean.getXmlResultSet(invoiceoidSQL.toString(), true, sqlprmLst);
		DocumentHandler e2eCount = DatabaseQueryBean.getXmlResultSet(end2endSQL.toString(), true, sqlprmLst);
		
		if(e2eCount != null && invoiceOidCount != null){
			//the counts should match up, otherwise it will mean that not all invoices for a given end-to-end id were selected
			if(invoiceOidCount.getAttributeInt("/ResultSetRecord/CNT") != e2eCount.getAttributeInt("/ResultSetRecord/CNT")){
				haveSameE2E = false;
			}else{
				haveSameE2E = true;
			}
		}
    	return haveSameE2E;
    }
  
	private DocumentHandler runPurgeCrNote(MediatorServices mediatorServices) throws AmsException,RemoteException{

		int purged =0;
		int toBe =0;

		LOG.debug("UploadInvoiceMediatorBean: Starting to purge expired Credit Notes");
		DocumentHandler outDoc = new DocumentHandler();
		DocumentHandler crDoc = null;

		try{

			Date overRiddenDate = null;
			overRiddenDate = GMTUtility.getGMTDateTime(true, true);


			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");  
			String now =  sdf.format(overRiddenDate);

			String crSql = "select a.UPLOAD_CREDIT_NOTE_OID,b.A_UPLOAD_SYSTEM_USER_OID from credit_notes a,corporate_org b where a.expiry_date < to_date(?,'yyyy-MM-DD') "
					+ "and (a.credit_note_status ='FAUT' or a.credit_note_status ='AVA') and a.A_CORP_ORG_OID = b.ORGANIZATION_OID and a. portal_approval_req_ind != 'Y' ";

			LOG.debug("UploadInvoiceMediatorBean: SQL identifying Credit Notes to purge" + crSql);

			crDoc = DatabaseQueryBean.getXmlResultSet(crSql,false,new Object[]{now}); 

		} catch (AmsException amse){

			mediatorServices.debug("exception in runPurgeCrNote " + amse.getMessage());
			amse.printStackTrace();
			outDoc.setAttribute("/CreditNoteInv/PurgeStatus", TradePortalConstants.INV_CRNOTE_PURGE_ERROR);

		}			


		if(crDoc!=null){
		Vector crVector = crDoc.getFragments("/ResultSetRecord");
		LOG.debug("UploadInvoiceMediatorBean: Found "+ crVector.size() + " Credit Notes to purge");
		toBe =crVector.size();
		
		for (Object object : crVector) {

			try{
				
				DocumentHandler childDoc = (DocumentHandler)object;
				long creditNoteOid =Long.parseLong(childDoc.getAttribute("/UPLOAD_CREDIT_NOTE_OID"));
				String uploadUser = childDoc.getAttribute("/A_UPLOAD_SYSTEM_USER_OID");
				CreditNotes creditNote = (CreditNotes) mediatorServices.createServerEJB("CreditNotes", creditNoteOid);
				creditNote.close(uploadUser);
				creditNote.remove();
				purged++;
				
			} catch (Exception ex){
 				mediatorServices.debug("exception in runPurgeCrNote " + ex.getMessage());
				ex.printStackTrace();
 			}
 		}

		LOG.debug("UploadInvoiceMediatorBean: Completed purging of  "+ crVector.size() + " Credit Notes");
		}
		outDoc.setAttribute("/CreditNote/ToBeClosed", "" +  toBe);
		outDoc.setAttribute("/CreditNote/PurgeCount", "" + purged);
	

		return outDoc;
	}

	private DocumentHandler runPurgeInvoice(MediatorServices mediatorServices) throws AmsException,RemoteException{

		int purged =0;
		int toBe =0;

		DocumentHandler invDoc = null;
		DocumentHandler outDoc = new DocumentHandler();

		try{

			LOG.debug("UploadInvoiceMediatorBean: Starting to purge expired invoices");
			Date overRiddenDate = GMTUtility.getGMTDateTime(true, true);
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");  
			String now =  sdf.format(overRiddenDate);

			String invSql = "select a.UPLOAD_INVOICE_OID,b.A_UPLOAD_SYSTEM_USER_OID "
					+ "from invoices_summary_data a,corporate_org b "
					+ "where a.invoice_status in (?,?,?,?) "
					+ "and a.A_CORP_ORG_OID = b.ORGANIZATION_OID and  portal_approval_req_ind != 'Y' "
					+ "and  case when  payment_date is null then due_date else payment_date end < to_date(?,'yyyy-MM-DD')";

			invDoc = DatabaseQueryBean.getXmlResultSet(invSql,false,TradePortalConstants.PAYABLE_UPLOAD_INV_STATUS_PENDING_ACTION,
					TradePortalConstants.UPLOAD_INV_STATUS_AUTH_FAILED,TradePortalConstants.PAYABLE_UPLOAD_INV_STATUS_CANCELLED,
					TradePortalConstants.PAYABLE_UPLOAD_INV_STATUS_UNASSIGNED,now); 
			LOG.debug("UploadInvoiceMediatorBean: SQL identifying invoices to purge" + invSql);
		}catch(AmsException ame){
			mediatorServices.debug("exception in runPurgeCrNote " + ame.getMessage());
			ame.printStackTrace();
			outDoc.setAttribute("/CreditNoteInv/PurgeStatus", TradePortalConstants.INV_CRNOTE_PURGE_ERROR);
			return outDoc;
		}


		Vector invVector = null;
		if (invDoc != null ){

			invVector = invDoc.getFragments("/ResultSetRecord");
			LOG.debug("UploadInvoiceMediatorBean: Found "+ invVector.size() + " invoices to purge");
			toBe = invVector.size();
 
			for (Object invObject : invVector) {

				try{
					DocumentHandler invChildDoc = (DocumentHandler)invObject;
					String invOid = invChildDoc.getAttribute("/UPLOAD_INVOICE_OID");
					String uploadUser =  invChildDoc.getAttribute("/A_UPLOAD_SYSTEM_USER_OID ");
					InvoicesSummaryData invoice = (InvoicesSummaryData)  mediatorServices.createServerEJB("InvoicesSummaryData", Long.parseLong(invOid.trim()));
					invoice.close(uploadUser);
					invoice.remove();
					purged++;

				} catch (Exception  ex){
					mediatorServices.debug("exception in runPurgeInvoice " + ex.getMessage());
					ex.printStackTrace();					 
				} 
			}
		}
		outDoc.setAttribute("/Invoice/ToBeClosed", "" +  toBe);
		outDoc.setAttribute("/Invoice/PurgeCount", "" + purged);
		 
		return outDoc;
	}
	private DocumentHandler declineInvoice(DocumentHandler inputDoc,
			MediatorServices mediatorServices,String buttonPressed) throws RemoteException,
			AmsException {
		InstrumentProperties pro = new InstrumentProperties();
		String corpOrgOid     = inputDoc.getAttribute("/UploadInvoiceList/ownerOrg");
		   String userOid        = inputDoc.getAttribute("/UploadInvoiceList/userOid");
		   String securityRights = inputDoc.getAttribute("/UploadInvoiceList/securityRights");
		AbstractInvoiceProcessor processor = getProcessor(buttonPressed);
		pro.setSecurityAccess(processor.getDecSecurityAccess());
		pro.setMediatorServices(mediatorServices);
		pro.setSecurityRights(securityRights);
		pro.setUserOid(userOid);
		processor.setAuthParams(pro);
		processor.setButtonAction("CorpCust.PayablesMgmtInvoiceDecline");
		
		if(!processor.isValidDecSecurity(pro)){
			return new DocumentHandler();
		}
		
		return processor.declineInvoiceProcess(inputDoc, mediatorServices, pro);
	}

	 public abstract class  AbstractInvoiceProcessor {
		 private String buttonAction ="";
		 public String getButtonAction() {
			return buttonAction;
		}
		public void setButtonAction(String buttonAction) {
			this.buttonAction = buttonAction;
		}
		private BigInteger securityAccess = null;
		 public BigInteger getDecSecurityAccess() {
			return securityAccess;
		}
		public void setSecurityAccess(BigInteger securityAccess) {
			this.securityAccess = securityAccess;
		}
		public abstract void setAuthParams(InstrumentProperties pro);
		public boolean isValidDecSecurity(InstrumentProperties prop) throws RemoteException, AmsException{
			 if (!SecurityAccess.hasRights(prop.getSecurityRights(), prop.getSecurityAccess())) {
					// user does not have appropriate authority.
				 prop.getMediatorServices().getErrorManager().issueError(
							TradePortalConstants.ERR_CAT_1,
							TradePortalConstants.NO_ACTION_MATCH_NOTICE_AUTH,
							prop.getMediatorServices().getResourceManager().getText(
									getButtonAction(),
									TradePortalConstants.TEXT_BUNDLE));
					return false;
				}
			 return true;
		 }
		
		 public DocumentHandler declineInvoiceProcess(DocumentHandler inputDoc,
					MediatorServices mediatorServices,InstrumentProperties pro ) throws RemoteException,
					AmsException {
			 List<DocumentHandler> invoiceList = null;
				String invoiceOid = null;
				String optLock = null;
				int count = 0;
				User user = null;
				ArMatchingRule matchingRule = null;
				String buyerName = null;
				invoiceList = inputDoc.getFragmentsList("/UploadInvoiceList/InvoicesSummaryData");
				String corpOrgOid = inputDoc.getAttribute("/UploadInvoiceList/ownerOrg");
				String userOid = inputDoc.getAttribute("/UploadInvoiceList/userOid");
				String subsAuth = inputDoc.getAttribute("/Authorization/authorizeAtParentViewOrSubs");
				count = invoiceList.size();
				
				// ***********************************************************************
				// check to make sure that the user is allowed to authorize the
				// invoice - the user has to be a corporate user (not an admin
				// user) and have the rights to authorize a Receivables
				// ***********************************************************************

				user = (User) mediatorServices.createServerEJB("User",
						Long.parseLong(userOid));
				CorporateOrganization corpOrg = (CorporateOrganization) mediatorServices.createServerEJB("CorporateOrganization",Long.parseLong(corpOrgOid));
				String selectClause = "select distinct upload_invoice_oid, currency as currency_code, case when (payment_amount is null or payment_amount=0) "
						+ "then amount else payment_amount end as payment_amount from invoices_summary_data ";

				// Loop through the list of invoice oids, and processing each one in the
				// invoice list
				for (DocumentHandler invoiceItem :invoiceList) {
					invoiceOid = invoiceItem.getAttribute("/InvoiceData");
					String[] invoiceData = invoiceItem.getAttribute("/InvoiceData")
							.split("/");
					invoiceOid = invoiceData[0];
					optLock = invoiceData[1];

					try {
						InvoicesSummaryData invoiceSummaryData = (InvoicesSummaryData) mediatorServices
								.createServerEJB("InvoicesSummaryData",Long.parseLong(invoiceOid));
						String invoiceStatus = invoiceSummaryData.getAttribute("invoice_status");
						String invoiceId = invoiceSummaryData.getAttribute("invoice_id");
						String invoiceGroupOid = invoiceSummaryData.getAttribute("invoice_group_oid"); 

						  if (!InvoiceUtility.isInvoiceCRDeclinable(mediatorServices.getResourceManager().getText("InvoiceUploadRequest.invoice_id",
				                     TradePortalConstants.TEXT_BUNDLE),invoiceId,invoiceStatus,mediatorServices)) {
				                 continue;
				            }
						  if(!TradePortalConstants.INDICATOR_YES.equals(invoiceSummaryData.getAttribute("portal_approval_requested_ind"))){
                          	//This case will never happen in prod as client will not decline if invoices r not H2H uploaded
							  mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,TradePortalConstants.CANNOT_DECLINE);
                          		return new DocumentHandler();
                      }
						String userWorkGroupOid = user.getAttribute("work_group_oid");
						String firstAuthorizer = invoiceSummaryData.getAttribute("first_authorizing_user_oid");
						String firstAuthorizerWorkGroup = invoiceSummaryData.getAttribute("first_authorizing_work_group_oid");

						// Check threshold amount start
						
						String clientBankOid = corpOrg.getAttribute("client_bank_oid"); //IR 27064 -Use correct ClientBankOid
						String baseCurrency = corpOrg.getAttribute("base_currency_code");
						String invCurrency = invoiceSummaryData.getAttribute("currency");
						String invoiceAmt = (StringFunction.isNotBlank(invoiceSummaryData.getAttribute("payment_amount")) 
						&& invoiceSummaryData.getAttributeDecimal("payment_amount").compareTo(BigDecimal.ZERO) != 0)?
								invoiceSummaryData.getAttribute("payment_amount") : invoiceSummaryData.getAttribute("amount");

						
						boolean isValid = InvoiceUtility.checkThresholds(invoiceAmt,invCurrency, baseCurrency, pro.getThold(),
								pro.getDlimit(), corpOrgOid, clientBankOid,user, selectClause, false, subsAuth, mediatorServices);
						if (!isValid) {
							return new DocumentHandler();
						}
						// threshold check end
						// if two different workgroups required authority is selected,
						// then user must belong to a group
						invoiceSummaryData.setAttribute("user_oid", userOid);
						invoiceSummaryData.setAttribute("action", TradePortalConstants.UPLOAD_INV_ACTION_AUTH);
						invoiceSummaryData.setAttribute("tp_rule_name",invoiceSummaryData.getTpRuleName());
						if ((TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS).equals(pro.getAuthType())
								&& (StringFunction.isBlank(userWorkGroupOid))) {
							mediatorServices.getErrorManager().issueError(UploadInvoiceMediator.class.getName(),TradePortalConstants.DEC_WORK_GROUP_REQUIRED,
									 mediatorServices.getResourceManager().getText("CorpCust.RecDeclineInvoice", TradePortalConstants.TEXT_BUNDLE));
							invoiceSummaryData.setAttribute("invoice_status", TradePortalConstants.UPLOAD_INV_STATUS_DCF);
							invoiceSummaryData.save();
						}
						else if ((TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS).equals(pro.getAuthType())
								|| (TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS).equals(pro.getAuthType())) {
							declineWithDiffWorkGroup(invoiceSummaryData, invoiceId,invoiceOid, userOid, firstAuthorizer, optLock,
									firstAuthorizerWorkGroup, userWorkGroupOid,pro.getAuthType(), mediatorServices);

						} else {
							 String newStatus = TradePortalConstants.UPLOAD_INV_STATUS_DBC;

							invoiceSummaryData.setAttribute("first_authorizing_user_oid", userOid);
							invoiceSummaryData.setAttribute("first_authorizing_work_group_oid",userWorkGroupOid);
							invoiceSummaryData.setAttribute("first_authorize_status_date",DateTimeUtility.getGMTDateTime());

							invoiceSummaryData.setAttribute("invoice_status", newStatus);
							invoiceSummaryData.setAttribute("opt_lock", optLock);
							
							int success = invoiceSummaryData.save();
							String newGroupOid = invoiceSummaryData
									.getAttribute("invoice_group_oid");
							if (success == 1) {
								if (StringFunction.isNotBlank(newGroupOid)&& !invoiceGroupOid.equals(newGroupOid)) {
									InvoiceUtility.updateAttachmentDetails(newGroupOid,
											invoiceGroupOid, invoiceOid,mediatorServices);
								}
									// display message...
									mediatorServices.getErrorManager().issueError(UploadInvoiceMediator.class.getName(),TradePortalConstants.DECLINED_INVOICE,
													invoiceId);
									InvoiceUtility.createOutgoingQueueAndIssueSuccess(newStatus, invoiceOid, false,mediatorServices);
  						}
						}
					  
					} catch (Exception e) {
						System.out
								.println("General exception caught in UploadInvoiceMediator:authorizeInvoice");
						e.printStackTrace();
					}
				}

				return (new DocumentHandler());
			 
		 }
		 private void declineWithDiffWorkGroup(InvoicesSummaryData invoiceSummaryData, String invoiceId,String invoiceOid, String userOid, String firstAuthorizer,
					String optLock, String firstAuthorizerWorkGroup,String userWorkGroupOid, String authorizeType,
					MediatorServices mediatorServices) throws RemoteException,AmsException {
						
				if (StringFunction.isBlank(firstAuthorizer)) {
					invoiceSummaryData.setAttribute("first_authorizing_user_oid",
							userOid);
					invoiceSummaryData.setAttribute("first_authorizing_work_group_oid",
							userWorkGroupOid);
					invoiceSummaryData.setAttribute("first_authorize_status_date",
							DateTimeUtility.getGMTDateTime());

					invoiceSummaryData.setAttribute("invoice_status", TradePortalConstants.UPLOAD_INV_STATUS_PDC);

					// Set the optimistic lock attribute to make sure the invoice hasn't
					// been changed by someone else since it was first retrieved
					invoiceSummaryData.setAttribute("opt_lock", optLock);
					int success = invoiceSummaryData.save();
					if (success == 1) {
						// display message...
						mediatorServices.getErrorManager().issueError(UploadInvoiceMediator.class.getName(),
								TradePortalConstants.TRANS_PARTIALLY_AUTHORIZED,
								mediatorServices.getResourceManager().getText("UploadInvoices.InvoiceID",
										TradePortalConstants.TEXT_BUNDLE), invoiceId);

					}
				}
				// if firstAuthorizer is not empty
				else {
					// Two work groups required and this user belongs to the
					// same work group as the first authorizer: Error.
					if ((TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS)
							.equals(authorizeType)
							&& userWorkGroupOid.equals(firstAuthorizerWorkGroup)) {
						mediatorServices.getErrorManager().issueError(UploadInvoiceMediator.class.getName(),
								TradePortalConstants.DEC_WORK_GROUP_REQUIRED);
						invoiceSummaryData.setAttribute("opt_lock", optLock);
						invoiceSummaryData.setAttribute("invoice_status",  TradePortalConstants.UPLOAD_INV_STATUS_DCF);
						invoiceSummaryData.save();
					}
					// Two authorizers required and this user is the same as the
					// first authorizer. Give error.
					// Note requiring two work groups also implies requiring two
					// users.
					else if (firstAuthorizer.equals(userOid)) {
						mediatorServices.getErrorManager().issueError(UploadInvoiceMediator.class.getName(),
								TradePortalConstants.DIFFERENT_SECOND_AUTH_REQRUIED);
						invoiceSummaryData.setAttribute("invoice_status", TradePortalConstants.UPLOAD_INV_STATUS_DCF);
						invoiceSummaryData.setAttribute("opt_lock", optLock);
						invoiceSummaryData.save();
					}
					// Authorize
					else {
						String newStatus = TradePortalConstants.UPLOAD_INV_STATUS_DBC;

						invoiceSummaryData.setAttribute("second_authorizing_user_oid",
								userOid);
						invoiceSummaryData.setAttribute(
								"second_authorizing_work_group_oid", userWorkGroupOid);
						invoiceSummaryData.setAttribute("second_authorize_status_date",
								DateTimeUtility.getGMTDateTime());

						invoiceSummaryData.setAttribute("invoice_status", newStatus);
						invoiceSummaryData.setAttribute("opt_lock", optLock);
						
						int success = invoiceSummaryData.save();
						if (success == 1) {
							// display message...
							mediatorServices.getErrorManager().issueError(UploadInvoiceMediator.class.getName(),TradePortalConstants.DECLINED_INVOICE,
									invoiceId);
							InvoiceUtility.createOutgoingQueueAndIssueSuccess(newStatus, invoiceOid, false, mediatorServices);

						}
					}
				}
				
			}
	 }
	 private AbstractInvoiceProcessor getProcessor(String button){
		 if(TradePortalConstants.PAY_PGM_BUTTON_DECLINE.equals(button)||
				 TradePortalConstants.PAY_PGM_DETAIL_BUTTON_DECLINE.equals(button) ){
			 return new PayableInvoiceProcessor();
		 }if(TradePortalConstants.REC_PGM_BUTTON_DECLINE.equals(button)||
				 TradePortalConstants.REC_PGM_DETAIL_BUTTON_DECLINE.equals(button) ){
			 return new ReceivableInvoiceProcessor();
		 }
		 		
		 return new PayableInvoiceProcessor();
	 }
	 
	 public class PayableInvoiceProcessor extends AbstractInvoiceProcessor {
		 public BigInteger getDecSecurityAccess() {
				return SecurityAccess.PAY_INVOICE_DECLINE_AUTH;
			}
		public void setAuthParams(InstrumentProperties pro){
			 pro.setThold("upload_pay_inv_thold");
			 pro.setDlimit("upload_pay_inv_dlimit");
			 pro.setAuthType("dual_auth_upload_pay_inv");
		}
		
	 }
	 public class ReceivableInvoiceProcessor extends AbstractInvoiceProcessor {
		 public BigInteger getDecSecurityAccess() {
				return SecurityAccess.REC_INVOICE_DECLINE_AUTH;
			}
		 public void setAuthParams(InstrumentProperties pro){
			 pro.setThold("upload_rec_inv_thold");
			 pro.setDlimit("upload_rec_inv_dlimit");
			 pro.setAuthType("dual_auth_arm_invoice");
		 }
	 }
	 
  } 
