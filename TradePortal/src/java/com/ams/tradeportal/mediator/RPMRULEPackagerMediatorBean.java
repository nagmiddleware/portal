package com.ams.tradeportal.mediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.common.*;
import com.ams.tradeportal.busobj.*;
import com.ams.tradeportal.busobj.util.*;
import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;

import javax.ejb.*;

import java.rmi.*;
import java.util.Hashtable;

/*     @author Vshah
 *     Manages RPMRULE Message Packaging..
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */

public class RPMRULEPackagerMediatorBean extends MediatorBean {
private static final Logger LOG = LoggerFactory.getLogger(RPMRULEPackagerMediatorBean.class);


  public DocumentHandler execute(DocumentHandler inputDoc, DocumentHandler outputDoc, MediatorServices mediatorServices)
                                                                                    throws RemoteException, AmsException
  {
	  try
	  {
		  String destinationID    			= TradePortalConstants.DESTINATION_ID_PROPONIX;
		  String senderID         			= TradePortalConstants.SENDER_ID;    // NSX CR-542 03/04/10
		  String operationOrganizationID	= null;
		  String messageType 	  			= MessageType.RPMRULE; //it is "RPMRULE" 
		  String dateSent         			= null;
		  String timeSent         			= null;
		  String timeStamp   				= DateTimeUtility.getGMTDateTime(false);
		  String messageID   	  			= null;
                  String processParam				= null;
		  String deletedInd				= "N";
		  
		  //IR - Reverted. Date and time transformation happens in MQSI. (04/01/2013)
		  //AAlubala - IR T36000015385 - 03/29/2013 - Date format should not include timestamp
		  //YYYYMMDD and the time field should be HHMM 
		  //
		  //dateSent  =  DateTimeUtility.getCurrentDate("yyyyMMdd");
	      //timeSent  =  DateTimeUtility.getCurrentDate("HHmm");	      
	      //IR T36000015385 - END
	      
		  dateSent  =  timeStamp;
	      timeSent  =  timeStamp.substring(11,19);
	      
		  String arMatchingRuleOid      	= null;
		  String corp_org_oid				= null;
		  String bank_org_oid				= null;
		  String parms[]  = new String[1];
		  
		  OperationalBankOrganization  operationalBankOrganization = null;
		  ArMatchingRule  arMatchingRule = null;
		  CorporateOrganization corpOrg = null;
		  GenericList buyerAliasNameList = null;
	      	  GenericList buyerAliasIdList = null;
		  
		  messageID =  inputDoc.getAttribute("/message_id");
	      arMatchingRuleOid = inputDoc.getAttribute("/transaction_oid");
	      processParam = inputDoc.getAttribute("/process_parameters");
	      if (StringFunction.isNotBlank(processParam))
	      {
	    	  deletedInd = "Y";
	      }	
	      

	      if (TradePortalConstants.INDICATOR_NO.equals(deletedInd))
	      {
	      arMatchingRule  = (ArMatchingRule)  mediatorServices.createServerEJB("ArMatchingRule",  Long.parseLong(arMatchingRuleOid));
	      corp_org_oid = arMatchingRule.getAttribute("corp_org_oid");
	      corpOrg = (CorporateOrganization) mediatorServices.createServerEJB("CorporateOrganization",Long.parseLong(corp_org_oid));
	      bank_org_oid = corpOrg.getAttribute("first_op_bank_org");
	      mediatorServices.debug("Ready to instantiate operational bank org " ); 
	      operationalBankOrganization = (OperationalBankOrganization) mediatorServices.createServerEJB("OperationalBankOrganization", Long.parseLong(bank_org_oid));
	      operationOrganizationID = operationalBankOrganization.getAttribute("Proponix_id");
	      }
	       else
	      {
	    	  operationOrganizationID = getParm("OperationOrganizationID", processParam);
	      } 
	      
	      /******************************** START OF HEADER SECTION  **********************************************************/
	      outputDoc.setAttribute("/Proponix/Header/DestinationID",            destinationID);
	      outputDoc.setAttribute("/Proponix/Header/SenderID",                 senderID);
	      outputDoc.setAttribute("/Proponix/Header/OperationOrganizationID",  operationOrganizationID);
	      outputDoc.setAttribute("/Proponix/Header/MessageType",              messageType);
	      outputDoc.setAttribute("/Proponix/Header/DateSent",                 dateSent);
	      outputDoc.setAttribute("/Proponix/Header/TimeSent",                 timeSent);
	      outputDoc.setAttribute("/Proponix/Header/MessageID",                messageID);
	        
	      /******************************** START OF SUB-HEADER SECTION  **********************************************************/

	      if (TradePortalConstants.INDICATOR_NO.equals(deletedInd))
	      { 	
	      outputDoc.setAttribute("/Proponix/SubHeader/CustomerID",            corpOrg.getAttribute("Proponix_customer_id"));
	      outputDoc.setAttribute("/Proponix/SubHeader/BuyerIDPrimary",        arMatchingRule.getAttribute("buyer_id"));
	      outputDoc.setAttribute("/Proponix/SubHeader/BuyerNamePrimary",      arMatchingRule.getAttribute("buyer_name"));
	      }
	      else
	      {
	    	  outputDoc.setAttribute("/Proponix/SubHeader/CustomerID",            getParm("CustomerID", processParam));
	    	  outputDoc.setAttribute("/Proponix/SubHeader/BuyerIDPrimary",        getParm("BuyerIDPrimary", processParam));
		      outputDoc.setAttribute("/Proponix/SubHeader/BuyerNamePrimary",      getParm("BuyerNamePrimary", processParam));
		      outputDoc.setAttribute("/Proponix/SubHeader/DeletedInd",            deletedInd);
	      } 	
	      
	      /******************************** START OF BODY SECTION  **********************************************************/
	      //This is for Body/BuyerAliasNameSata/BuyerAliasName
	   if (TradePortalConstants.INDICATOR_NO.equals(deletedInd))
	   {		
	      try
	      {
	    	  buyerAliasNameList = (GenericList) EJBObjectFactory.createServerEJB(getSessionContext(), "GenericList");
	    	  parms[0] = arMatchingRuleOid;
	    	  buyerAliasNameList.prepareList("ArBuyerNameAlias", "byArMatchingRuleOid", parms);
	    	  buyerAliasNameList.getData();
	      
	    	  int buyerAliasNameCount = buyerAliasNameList.getObjectCount();
			
	    	  if (buyerAliasNameCount > 0)
	    	  {
	    		  DocumentHandler buyerAliasNameHandler = new DocumentHandler();
	    		  String buyerAliasName = "";
	    		  for(int i=0; i< buyerAliasNameCount; i++)
	          	  {
	    		  	buyerAliasNameList.scrollToObjectByIndex(i); 
	    		  	buyerAliasNameHandler.setAttribute("/BuyerAliasName",buyerAliasNameList.getAttribute("buyer_name_alias"));
	    		  	buyerAliasName = buyerAliasName + buyerAliasNameHandler.getFragment("/BuyerAliasName").toString();
	          	  }		            
	    	      	  
	    		  buyerAliasName = "<BuyerAliasNameData>" + buyerAliasName + "</BuyerAliasNameData>";
	    	  	  DocumentHandler tmpAliasNameDoc = new DocumentHandler(buyerAliasName,true);
	    	   	  outputDoc.setComponent("/Proponix/Body/BuyerAliasNameData", tmpAliasNameDoc.getFragment("/BuyerAliasNameData") );
	    	  }
	      }
	      catch (NullPointerException e)
	      {
	  		  LOG.info("RPMRULEPackagerMediator - Null on query");
	  	  }
	      catch (Exception e)
	      {
	  		  LOG.info(e.toString());
	  	  }
	      finally 
	      {
	    	//Remove the listview bean
	  		try 
	  		{
	  			if (buyerAliasNameList != null)
	  				buyerAliasNameList.remove();
	  			 } catch (RemoveException ex) {
	  			 } catch (NoSuchObjectException e) {
	  			 } catch (RemoteException e) {
	  			 }
	      }
	    
	   	  
	      //This is for Body/BuyerAliasIDData/BuyerAliasID
	      try
	      {
	    	  	buyerAliasIdList = (GenericList) EJBObjectFactory.createServerEJB(getSessionContext(), "GenericList");
	    	  	parms[0] = arMatchingRuleOid;
	    	  	buyerAliasIdList.prepareList("ArBuyerIdAlias", "byArMatchingRuleOid", parms);
	    	  	buyerAliasIdList.getData();
      
	    	  	int buyerAliasIdCount = buyerAliasIdList.getObjectCount();
	    	  	
	    	  	if (buyerAliasIdCount > 0)
		    	{
	    	  		DocumentHandler buyerAliasIDHandler = new DocumentHandler();
		    		String buyerAliasID = "";
	    	  		for(int i=0; i< buyerAliasIdCount; i++)
	    	  		{
	    	  			buyerAliasIdList.scrollToObjectByIndex(i); 
	    	  			buyerAliasIDHandler.setAttribute("/BuyerAliasID",buyerAliasIdList.getAttribute("buyer_id_alias"));
	    	  			buyerAliasID = buyerAliasID + buyerAliasIDHandler.getFragment("/BuyerAliasID").toString();
	    	  		}
	    	  		buyerAliasID = "<BuyerAliasIDData>" + buyerAliasID + "</BuyerAliasIDData>";
		    	  	DocumentHandler tmpAliasIDDoc = new DocumentHandler(buyerAliasID,true);
		    	   	outputDoc.setComponent("/Proponix/Body/BuyerAliasIDData", tmpAliasIDDoc.getFragment("/BuyerAliasIDData") );
		    	}
	      }
	      catch (NullPointerException e)
	      {
	    	  LOG.info("RPMRULEPackagerMediator - Null on query");
	      }
	      catch (Exception e)
	      {
	    	  LOG.info(e.toString());
	      }
	      finally 
	      {
	    	  //Remove the listview bean
	    	  try 
	    	  {
	    		  if (buyerAliasIdList != null)
	    			  buyerAliasIdList.remove();
  			  } catch (RemoveException ex) {
  			  } catch (NoSuchObjectException e) {
  			  } catch (RemoteException e) {
  			  }
          }
	      	
	      //This is for Body/ToleranceData
	      outputDoc.setAttribute("/Proponix/Body/ToleranceData/PartialPayToPortalInd",            arMatchingRule.getAttribute("partial_pay_to_portal_indicator"));
	      outputDoc.setAttribute("/Proponix/Body/ToleranceData/NoToleranceInd",                   arMatchingRule.getAttribute("no_tolerance_indicator"));
	      outputDoc.setAttribute("/Proponix/Body/ToleranceData/ARMTolerancePctPlus",              arMatchingRule.getAttribute("tolerance_percent_plus"));
	      outputDoc.setAttribute("/Proponix/Body/ToleranceData/ARMTolerancePctMinus",             arMatchingRule.getAttribute("tolerance_percent_minus"));
	      outputDoc.setAttribute("/Proponix/Body/ToleranceData/ARMToleranceAmtPlus",              arMatchingRule.getAttribute("tolerance_amount_plus"));
	      outputDoc.setAttribute("/Proponix/Body/ToleranceData/ARMToleranceAmtMinus",             arMatchingRule.getAttribute("tolerance_amount_minus"));
		}
	  else
	  {
	    	  outputDoc.setAttribute("/Proponix/Body/ToleranceData", "");
	  }
	      
	   //START
	   //CR741 Rel 8.2 - AAlubala - 12/03/2012
	   //Package Default GL Code data
	   //Body/DefaultGLData
	    outputDoc.setAttribute("/Proponix/Body/DefaultGLData/DefaultPaymentGLCode",            arMatchingRule.getAttribute("payment_gl_code"));
	      outputDoc.setAttribute("/Proponix/Body/DefaultGLData/DefaultOverPaymentGLCode",                   arMatchingRule.getAttribute("over_payment_gl_code"));
	      outputDoc.setAttribute("/Proponix/Body/DefaultGLData/DefaultDiscountGLCode",              arMatchingRule.getAttribute("discount_gl_code"));
	  
	      //CR741 - END
	   
	      
   	  }
	  catch (RemoteException e) {
	       mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,TradePortalConstants.TASK_NOT_SUCCESSFUL,"RPMRULE Message Packaging");
	       e.printStackTrace();
	    }
	  catch (AmsException e) {
	        mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,TradePortalConstants.TASK_NOT_SUCCESSFUL,"RPMRULE Message Packaging");
	        e.printStackTrace();

	    }
	 catch (Exception e) {
	       mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,TradePortalConstants.TASK_NOT_SUCCESSFUL,"RPMRULE Message Packaging");
	       e.printStackTrace();

	    } 	  
	 
       return outputDoc;
  }

public static String getParm(String parmName, String processingParms) {

      int locationOfParmName, startOfParmValue, endOfParmValue;
      String parmValue;

      locationOfParmName = processingParms.indexOf(parmName);

      // Search for the whole word only.
      // For example, getParm("portfolio_activity_uoid=0|activity=1", "activity" should return 1.
      // Keep searching if the character before or after the occurance is not a word delimitor

      while ((locationOfParmName > 0
              && processingParms.charAt(locationOfParmName - 1) != ' '
              && processingParms.charAt(locationOfParmName - 1) != '|')
              || (locationOfParmName >= 0
              && locationOfParmName < processingParms.length() - parmName.length()
              && processingParms.charAt(locationOfParmName + parmName.length()) != ' '
              && processingParms.charAt(locationOfParmName + parmName.length()) != '=')) {

          locationOfParmName = processingParms.indexOf(parmName, locationOfParmName + parmName.length());

      }

      if (locationOfParmName == -1) {
          return "";
      }

      startOfParmValue = processingParms.indexOf("=", locationOfParmName);

      if (startOfParmValue == -1 ) {
          return "";
      }

      startOfParmValue++;

      endOfParmValue = processingParms.indexOf("|", startOfParmValue);

      if (endOfParmValue == -1) {
          endOfParmValue = processingParms.length();
      }

      parmValue = processingParms.substring(startOfParmValue , endOfParmValue);

      return parmValue;
  }

  
}

