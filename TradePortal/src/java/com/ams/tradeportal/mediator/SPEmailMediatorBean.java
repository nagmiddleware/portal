package com.ams.tradeportal.mediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.RemoteException;

import com.ams.tradeportal.busobj.CorporateOrganization;
import com.ams.tradeportal.busobj.SPEmailNotifications;
import com.ams.tradeportal.busobj.util.InstrumentServices;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.frame.MediatorBean;
import com.amsinc.ecsg.frame.MediatorServices;
import com.amsinc.ecsg.util.DateTimeUtility;
import com.amsinc.ecsg.util.DocumentHandler;

public class SPEmailMediatorBean extends MediatorBean {
private static final Logger LOG = LoggerFactory.getLogger(SPEmailMediatorBean.class);
	
	
public DocumentHandler execute(DocumentHandler inputDoc, DocumentHandler outputDoc, MediatorServices mediatorServices)
	throws RemoteException, AmsException
	{
	  CorporateOrganization corpOrg = null;
      SPEmailNotifications spEmailNotifications = null;
      String spEmailOid = null;
	  int emailsSent = 0;
      String corpOrgOid = null;
      String invoiceCount = null;
      String errorMessage = null;
      int updateInvRecCnt = 0;
     
        try
         {

            String  nextEmailDue =  null;
            String instrumentOid = null;
			String transactionOid = null;
          
            if(inputDoc != null){ 
            	corpOrgOid =    inputDoc.getAttribute("corpOrgOid");
            	invoiceCount =  inputDoc.getAttribute("invoiceCount");
            	spEmailOid =    inputDoc.getAttribute("spEmailOid");
            }
            LOG.info("Starting run of supplier portal email process for supplier with OrgId: "+corpOrgOid );
            corpOrg	= (CorporateOrganization) mediatorServices.createServerEJB("CorporateOrganization",Long.parseLong(corpOrgOid));
            //Rel9.5 CR-927B Fetch email frequency from Notification rule
            String notifRuleOid = corpOrg.fetchNotificationRule();
            String emailFreq = corpOrg.getNotificationRuleSettingOrFreq(notifRuleOid, TradePortalConstants.SUPP_PORT_INST_TYPE, TradePortalConstants.SUPP_PORT_INST_TYPE, TradePortalConstants.NOTIF_RULE_EMAIL_FREQ);
            
           
            //jgadela R91 IR T36000026319 - SQL INJECTION FIX
             String sql = "UPDATE INVOICE SET  sp_email_sent_ind = '"+ TradePortalConstants.INDICATOR_YES +"' where  "+
                         " a_corp_org_oid = ?  and supplier_portal_invoice_ind = ? "+
                         " and sp_email_sent_ind <> ? and supplier_portal_invoice_status  not in (?,?)";
        	// sql.append("and supplier_portal_invoice_status  <> '" + TradePortalConstants.SP_STATUS_INELLIGIBLE +"'");
        	 
        	
        	  Object sqlParams[] = new Object[5];
        	  sqlParams[0] = corpOrgOid;
        	  sqlParams[1] = TradePortalConstants.INDICATOR_YES;
        	  sqlParams[2] = TradePortalConstants.INDICATOR_YES;
        	  sqlParams[3] = TradePortalConstants.SP_STATUS_INELLIGIBLE;
        	  sqlParams[4] = TradePortalConstants.SP_STATUS_BUYER_APPROVED_ALLELIGIBLE;

             updateInvRecCnt  = DatabaseQueryBean.executeUpdate(sql, false, sqlParams);
             
             corpOrg.createEmailMessage(instrumentOid, transactionOid,  String.valueOf(updateInvRecCnt), " ", TradePortalConstants.EMAIL_TRIGGER_SUPPLIER_PORTAL);
 			 emailsSent++;
 		
 			 spEmailNotifications= (SPEmailNotifications) mediatorServices.createServerEJB("SPEmailNotifications",Long.parseLong(spEmailOid));
            
 			
 			 if(InstrumentServices.isNotBlank(emailFreq)){
 				 nextEmailDue = DateTimeUtility.addMinutesToGMTTime(Integer.parseInt(emailFreq));
 				 LOG.info("User given interval:["+emailFreq+"] minutes and nextEmailDue is ["+nextEmailDue+"]");
 			 }
              spEmailNotifications.setAttribute("next_sp_email_due", nextEmailDue);
              spEmailNotifications.save();
              
              outputDoc.setAttribute("result", "SUCCESS");
 
         }catch(Exception e){
        	 	String serverErrorNumber = "SE" + System.currentTimeMillis();
        	 	errorMessage  = "Error occurred while processing email: errorCode: " + serverErrorNumber+ " errorDescription: "+e.getMessage();
        	 	System.err.println(errorMessage + 
        		        " Server error#" + serverErrorNumber + " " +
        		        " with error: " + e.toString());
        		e.printStackTrace();
        		outputDoc.setAttribute("result", "ERROR");
        		outputDoc.setAttribute("errorMessage", errorMessage);
         }

         LOG.info("Finised run of supplier portal email process. Sent "+emailsSent+" e-mails. ");
         return outputDoc;
}

}
