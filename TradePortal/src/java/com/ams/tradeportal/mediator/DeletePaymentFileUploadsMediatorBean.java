package com.ams.tradeportal.mediator;
import java.rmi.RemoteException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.busobj.PaymentFileUpload;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.MediatorBean;
import com.amsinc.ecsg.frame.MediatorServices;
import com.amsinc.ecsg.util.DocumentHandler;
/*
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class DeletePaymentFileUploadsMediatorBean extends MediatorBean
{
private static final Logger LOG = LoggerFactory.getLogger(DeletePaymentFileUploadsMediatorBean.class);
  /**
   * This method performs deletions of Payment Upload Files
   * The format expected from the input doc is:
   *
   * <TransactionList>
   *	<PaymentFileUpload ID="0"><paymentFileUploadData>1/VALIDATION_SUCCESSFUL</paymentFileUploadData></PaymentFileUpload>
   *	<PaymentFileUpload ID="1"><paymentFileUploadData>2/VALIDATION_IN_PROGRESS</paymentFileUploadData></PaymentFileUpload>
   *	<PaymentFileUpload ID="2"><paymentFileUploadData>3/VALIDATED_WITH_ERRORS</paymentFileUploadData></PaymentFileUpload>
   * </TransactionList>
   * <User>
   *    <userOid>1001</userOid>
   *    <securityRights>302231454903657293676543</securityRights>
   * </User>
   *
   *
   * This is a non-transactional mediator that executes transactional
   * methods in business objects
   *
   * @param inputDoc The input XML document (&lt;In&gt; section of overall
   * Mediator XML document)
   * @param outputDoc The output XML document (&lt;Out&gt; section of overall
   * Mediator XML document)
   * @param mediatorServices Associated class that contains lots of the
   * auxillary functionality for a mediator.
   */

   public DocumentHandler execute(DocumentHandler inputDoc, DocumentHandler outputDoc, MediatorServices mediatorServices)
                                  throws RemoteException, AmsException
   {
      // get each Payment File Upload Oid and process each payment upload file; each payment upload file is unique within itself
      DocumentHandler   docUpload	         = null;
      PaymentFileUpload uploadFile           = null;
      List<DocumentHandler>  uploadList		 = null;
      String            uploadData 		     = null;
      String		    uploadOid			 = null;
      String		    validationStatus	 = null;
      int               count                = 0;
      int               separator1           = 0;

      // populate variables with inputDocument information
      uploadList 	   = inputDoc.getFragmentsList("/PaymentFileUploadList/PaymentFileUpload");
	  count            = uploadList.size();

	  LOG.debug("DeletePaymentFileUploadsMediator: number of payment upload files to delete: {}" , count);

	  if (count <= 0)
	  {
	     mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
	                                                   TradePortalConstants.NO_ITEM_SELECTED,
											   		   mediatorServices.getResourceManager().getText("TransactionAction.Deleted",
							   						   TradePortalConstants.TEXT_BUNDLE));

	     return outputDoc;
	  }

      for (int i = 0; i < count; i++)
      {
         docUpload  = (DocumentHandler) uploadList.get(i);
         uploadData = docUpload.getAttribute("/paymentFileUploadData");

         // Split up the payment_file_upload_oid, transaction_status
         separator1 = uploadData.indexOf('/');
         uploadOid  = uploadData.substring(0, separator1);
         validationStatus = uploadData.substring(separator1 + 1);

//         LOG.debug("DeletePaymentFileUploadsMediator: uploadData contents: {} Upload oid: {} Validation status: {} ", uploadData, uploadOid, validationStatus);

         try
         {
			//MDB Rel6.1 IR# PRUL013144627 2/1/11 - Begin
			if ((TradePortalConstants.PYMT_UPLOAD_VALIDATION_FAILED.equals(validationStatus)) ||
			    (TradePortalConstants.PYMT_UPLOAD_FILE_REJECTED.equals(validationStatus)))
			{
		        uploadFile = (PaymentFileUpload) mediatorServices.createServerEJB("PaymentFileUpload");
            	uploadFile.deletePaymentFileUpload(uploadOid);
			}
			//MDB Rel6.1 IR# PRUL013144627 2/1/11 - End
			else
		  		mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
	      		                                              TradePortalConstants.PYMT_UPLOAD_DELETE_FILE_NOT_REJECTED);
         }
		 catch(Exception e)
		 {
		    LOG.error("Exception occured in DeletePaymentFileUploadsMediator: " , e);
		 }
      }

      return outputDoc;
   }
}
