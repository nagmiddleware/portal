package com.ams.tradeportal.mediator.premediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */

import com.amsinc.ecsg.web.*;
import javax.servlet.http.*;
import java.util.*;
import com.amsinc.ecsg.util.*;
import com.ams.tradeportal.common.*;
import com.ams.tradeportal.busobj.util.*;

public class TPCalendarPreEdit  implements com.amsinc.ecsg.web.WebAction {
private static final Logger LOG = LoggerFactory.getLogger(TPCalendarPreEdit.class);
	/**
	 * FTDPPreEdit constructor comment.
	 */
	public TPCalendarPreEdit() {
		super();
	}

	/**
	 * Performs different logic based on the type of transaction (ISS or AMD)
	 *
	 * But basically converts the month, day, year fields for each of the
	 * dates into single date fields.  This allows populateFromXml to work.
	 *
	 * In addition, explicitly sets all 'missing' checkbox fields to N.
	 * (Checkbox fields which are not checked are not sent in the document,
	 * we must force an N to cause unchecked values to be save correctly.)
	 *
	 * Also, perform logic to determine the account number and currency
	 * selected for the beneficiary -- this may come from a radio button
	 * value or an enterable field.
	 */
	public void act(AmsServletInvocation reqInfo,
			BeanManager beanMgr,
			FormManager formMgr,
			ResourceManager resMgr,
			String serverLocation,
			HttpServletRequest request,
			HttpServletResponse response,
			Hashtable inputParmMap,
			DocumentHandler inputDoc)
	throws com.amsinc.ecsg.frame.AmsException
	{
		String value;

		// get the input document
		if (inputDoc == null)
		{
			LOG.info("Input document to TPCalendarPreEdit is null");
			return;
		}

		String weekend = inputDoc.getAttribute("/In/TPCalendar/weekend_day_1");
		if ("-1".equals(weekend))
		{
			inputDoc.setAttribute("/In/TPCalendar/weekend_day_1", "");
		}

		weekend = inputDoc.getAttribute("/In/TPCalendar/weekend_day_2");
		if ("-1".equals(weekend))
		{
			inputDoc.setAttribute("/In/TPCalendar/weekend_day_2", "");
		}

	}

}