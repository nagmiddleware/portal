package com.ams.tradeportal.mediator.premediator;

/**
 * Premediator for PartyDetail.  Handles Account List.
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */

import com.amsinc.ecsg.web.*;
import javax.servlet.http.*;
import java.util.*;

import com.amsinc.ecsg.util.*;
import com.amsinc.ecsg.frame.*;
import com.ams.tradeportal.common.*;
import java.rmi.*;
import com.ams.tradeportal.busobj.util.*;

public class NotificationRuleDetailPreEdit implements com.amsinc.ecsg.web.WebAction
{
	/**
	 * NotificationRuleDetailPreEdit constructor comment.
	 */
	public NotificationRuleDetailPreEdit()
	{
		super();
	}

	/**
	 *
	 */
	public void act(AmsServletInvocation reqInfo,
			BeanManager beanMgr,
			FormManager formMgr,
			ResourceManager resMgr,
			String serverLocation,
			HttpServletRequest request,
			HttpServletResponse response,
			Hashtable inputParmMap,
			DocumentHandler inputDoc) throws AmsException
			{

		// Retrieve the input document
		if (inputDoc == null)
		{
			System.out.println("Input document to NotificationRuleDetailPreEdit is null");

			return;
		}
		System.out.println("Raw InputDoc:"+inputDoc); 

		//Srinivasu D CR#927b Rel9.5 02/01/2016 - modified inputdoc
		Set clearSet = new HashSet();
		String templateInd = inputDoc.getAttribute("/In/NotificationRule/template_ind");
		int iLoop;
		Vector acctVector = inputDoc.getFragments("/In/NotificationRule/NotificationRuleCriterionList");
		if(acctVector != null && acctVector.size()>0) {
			int numItems = acctVector.size();
			System.out.println(numItems);		   
			int rLoop=0;
			int delLoop = 0;
			inputDoc.setAttribute("/In/NotificationRule/opt_lock", "");
			DocumentHandler tempDoc = new DocumentHandler();
			for (iLoop=0; iLoop<numItems; iLoop++)
			{
				String oid = inputDoc.getAttribute("/In/NotificationRule/NotificationRuleCriterionList("+iLoop+")/criterion_oid");
				String sendEmailSetting = inputDoc.getAttribute("/In/NotificationRule/NotificationRuleCriterionList("+iLoop+")/send_email_setting");
				String sendNotifSetting = inputDoc.getAttribute("/In/NotificationRule/NotificationRuleCriterionList("+iLoop+")/send_notif_setting");
				String freqSetting = inputDoc.getAttribute("/In/NotificationRule/NotificationRuleCriterionList("+iLoop+")/notify_email_freq");
				String instType = inputDoc.getAttribute("/In/NotificationRule/NotificationRuleCriterionList("+iLoop+")/instrument_type_or_category");
				if(StringFunction.isNotBlank(freqSetting) && TradePortalConstants.INDICATOR_NO.equals(sendEmailSetting)){
					inputDoc.setAttribute("/In/NotificationRule/NotificationRuleCriterionList("+iLoop+")/notify_email_freq","");
				}
				if(StringFunction.isNotBlank(sendEmailSetting) || StringFunction.isNotBlank(sendNotifSetting)){

					if(StringFunction.isNotBlank(oid) && "0".equals(oid)){
						inputDoc.setAttribute("/In/NotificationRule/NotificationRuleCriterionList("+iLoop+")/criterion_oid", "");
					}
					tempDoc.addComponent("/In/NotificationRule/NotificationRuleCriterionList("+rLoop+")",	
							inputDoc.getFragment("/In/NotificationRule/NotificationRuleCriterionList("+iLoop+")"));
					rLoop++;
					inputDoc.removeComponent("/In/NotificationRule/NotificationRuleCriterionList("+iLoop+")");

				}
				else if(StringFunction.isBlank(sendEmailSetting) && StringFunction.isBlank(sendNotifSetting)){	

					if(StringFunction.isNotBlank(oid) && !"0".equals(oid)) {						

						DocumentHandler deleteDoc = new DocumentHandler();
						deleteDoc.setAttribute("/Name", "NotificationRuleCriterion");
						deleteDoc.setAttribute("/Oid",oid);
						inputDoc.addComponent("/In/ComponentToDelete("+delLoop+")", deleteDoc);
						inputDoc.removeComponent("/In/NotificationRule/NotificationRuleCriterionList("+iLoop+")");
						delLoop++;	
						clearSet.add(instType);
					}else{		
						if(StringFunction.isBlank(sendEmailSetting) && StringFunction.isBlank(sendNotifSetting)) {
							inputDoc.removeComponent("/In/NotificationRule/NotificationRuleCriterionList("+iLoop+")");

						}
					}								
				}			
			}

			if(null != tempDoc.getFragment("/In/NotificationRule")){
				inputDoc.addComponent("/In/NotificationRule/", tempDoc.getFragment("/In/NotificationRule"));
			}

			/*for (int iLoop1=8; iLoop1<numItems; iLoop1++)
		   	{ 
				inputDoc.removeComponent("/In/NotificationRule/NotifyRuleCriterionList("+iLoop1+")");
			}*/

			// notification rule user doc setup start here		
			if(TradePortalConstants.INDICATOR_NO.equals(templateInd)) {
				ArrayList userOidList = new ArrayList();
				StringBuffer userOidBuf = new StringBuffer();
				Hashtable <String,String>userCriteriaHT = new Hashtable<String,String>();
				String formattedDefUserIds ="";
				StringBuilder dbuserOidBuf = new StringBuilder();
				String defaultUserIds = inputDoc.getAttribute("/In/NotificationRule/defaultNotificationUserIds");

				if(StringFunction.isNotBlank(defaultUserIds)){
					formattedDefUserIds =	getUserList(defaultUserIds);
				}
				inputDoc.setAttribute("/In/NotificationRule/default_notification_user_ids",formattedDefUserIds);

				Vector updateVec = inputDoc.getFragments("/In/NotificationRule/NotificationRuleCriterionList");
				int itemsCnt = updateVec.size();
				String tempUserIds =null;
				String tempDbUserIds = null;
				//	System.out.println("itemsCnt itemsCnt;"+itemsCnt);
				for (int jLoop=0; jLoop<itemsCnt; jLoop++)
				{
					//Set the email ids and additional email address to blank if Send Email Setting is Never
					String sendEmailSetting = inputDoc.getAttribute("/In/NotificationRule/NotificationRuleCriterionList("+jLoop+")/send_email_setting");
					if(TradePortalConstants.NOTIF_RULE_NONE.equals(sendEmailSetting)){
						inputDoc.setAttribute("/In/NotificationRule/NotificationRuleCriterionList("+jLoop+")/notificationUserIds","0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0");
						inputDoc.setAttribute("/In/NotificationRule/NotificationRuleCriterionList("+jLoop+")/additional_email_addr","");
					}
				}
				for (int jLoop=0; jLoop<itemsCnt; jLoop++)
				{
					tempUserIds = inputDoc.getAttribute("/In/NotificationRule/NotificationRuleCriterionList("+jLoop+")/notificationUserIds");
					tempDbUserIds = inputDoc.getAttribute("/In/NotificationRule/NotificationRuleCriterionList("+jLoop+")/dbNotificationUserIds");
					//if(!("0".equals(tempUserIds) || "null".equals(tempUserIds))) {
					userOidBuf.append(tempUserIds+",");
					dbuserOidBuf.append(tempDbUserIds+",");
					//}
				}
				if(userOidBuf.length()>1){
					userOidBuf.delete(userOidBuf.length()-1,userOidBuf.length());
				}
				if(dbuserOidBuf.length()>1){
					dbuserOidBuf.delete(dbuserOidBuf.length()-1, dbuserOidBuf.length());
				}
				if(StringFunction.isNotBlank(dbuserOidBuf.toString())){
					String firstFormatOids =	getUserList(dbuserOidBuf.toString());
					String arr[] = firstFormatOids.split(",");
					if(arr!=null && arr.length>0) {
						for(int i=0;i<arr.length;i++) {	
							if(!userOidList.contains(arr[i])){
								userOidList.add(arr[i]);
							}
						}
					}
				}

				StringBuffer selectClause = new StringBuffer("select notify_rule_user_list_oid,p_user_oid,p_criterion_oid from notify_rule_user_list ");
				selectClause.append("where p_user_oid in "+StringFunction.toSQLString(userOidList));

				Vector notifyUserVector = new Vector();
				DocumentHandler notifyUserDoc = null;
				if(userOidList.size()>0) {
					notifyUserDoc = DatabaseQueryBean.getXmlResultSet(selectClause.toString(),false);				 
				}

				if(notifyUserDoc != null){
					notifyUserVector = notifyUserDoc.getFragments("/ResultSetRecord");
					for(int x=0;x<notifyUserVector.size();x++) {
						DocumentHandler notifyUsrDoc = (DocumentHandler) notifyUserVector.elementAt(x);
						String dbCriterion = notifyUsrDoc.getAttribute("/P_CRITERION_OID");
						String dbUserListOid = notifyUsrDoc.getAttribute("/NOTIFY_RULE_USER_LIST_OID");
						String dbUserOid = notifyUsrDoc.getAttribute("/P_USER_OID");
						//System.out.println("dbCriterion:"+dbCriterion+"\tdbUserListOid;"+dbUserListOid+"\t dbUserOid:"+dbUserOid);
						userCriteriaHT.put(dbCriterion+"_"+dbUserOid,dbUserListOid);
					}
				}


				String formattedUserIds = null;
				String dbFormattedUserIds = null;
				List tobeDeletedIds = new ArrayList();
				List <String>deletedOids = new ArrayList<String>();
				for (int jLoop=0; jLoop<itemsCnt; jLoop++)
				{
					String userIds = inputDoc.getAttribute("/In/NotificationRule/NotificationRuleCriterionList("+jLoop+")/notificationUserIds");
					String dbUserIds = inputDoc.getAttribute("/In/NotificationRule/NotificationRuleCriterionList("+jLoop+")/dbNotificationUserIds");
					String criterionOid = inputDoc.getAttribute("/In/NotificationRule/NotificationRuleCriterionList("+jLoop+")/criterion_oid");
					//System.out.println("userIds:"+userIds+"\t dbUserIds:"+dbUserIds+"\t criterionOid:"+criterionOid);
					DocumentHandler ruleDoc = null;
					List uniqueArr = null;
					int cnt =0;
					List dbUniqueArr = null;
					if(StringFunction.isNotBlank(criterionOid) && !"0".equals(criterionOid)) {
						formattedUserIds = getUserList(userIds);
						uniqueArr = getUniqueUserIds(formattedUserIds);
						dbFormattedUserIds = getUserList(dbUserIds);
						dbUniqueArr = getUniqueUserIds(dbFormattedUserIds);
						//System.out.println("uniqueArr:"+uniqueArr+"\t dbUniqueArr;"+dbUniqueArr+"\t criterionOid;"+criterionOid);
						//User wants to clear all the email ids
						if((uniqueArr !=null && uniqueArr.size()==0) && (dbUniqueArr != null && dbUniqueArr.size()>0)) {
							String notifyOid =null;
							for(int i=0;i<dbUniqueArr.size();i++){
								//found in db but not in UI
								if(!uniqueArr.contains(dbUniqueArr.get(i))){		
								tobeDeletedIds.add(criterionOid+"_"+dbUniqueArr.get(i));
								notifyOid =userCriteriaHT.get(criterionOid+"_"+dbUniqueArr.get(i));
								deletedOids.add(notifyOid);									
								ruleDoc = new DocumentHandler();
								ruleDoc.setAttribute("/notify_rule_user_list_oid", notifyOid);	  
								ruleDoc.setAttribute("/a_criterion_oid", "0");		  
								ruleDoc.setAttribute("/user_oid", "0");
								inputDoc.addComponent("/In/NotificationRule/NotificationRuleCriterionList("
										+jLoop+")/NotifyRuleUserListList("+cnt+")", ruleDoc);
								cnt++;									

								}else {	//available in ui/db
									notifyOid =userCriteriaHT.get(criterionOid+"_"+dbUniqueArr.get(i));
									String tempUserOid = String.valueOf(dbUniqueArr.get(i));
									if(StringFunction.isBlank(tempUserOid)){
										tempUserOid="";
									}	
									ruleDoc = new DocumentHandler();
									ruleDoc.setAttribute("/notify_rule_user_list_oid", notifyOid);	  
									ruleDoc.setAttribute("/a_criterion_oid", criterionOid);		  
									ruleDoc.setAttribute("/user_oid", tempUserOid);
									inputDoc.addComponent("/In/NotificationRule/NotificationRuleCriterionList("
											+jLoop+")/NotifyRuleUserListList("+cnt+")", ruleDoc);
									cnt++;
									uniqueArr.remove(dbUniqueArr.get(i));
								}
							}
						}
						//User wants to clear some and add new email ids
						else if((uniqueArr !=null && uniqueArr.size()>0) && (dbUniqueArr != null && dbUniqueArr.size()>0)) {


							String notifyOid =null;
							for(int i=0;i<dbUniqueArr.size();i++){
								//found in db but not in UI
								if(!uniqueArr.contains(dbUniqueArr.get(i))){		
								tobeDeletedIds.add(criterionOid+"_"+dbUniqueArr.get(i));
								notifyOid =userCriteriaHT.get(criterionOid+"_"+dbUniqueArr.get(i));
								deletedOids.add(notifyOid);									
								ruleDoc = new DocumentHandler();
								ruleDoc.setAttribute("/notify_rule_user_list_oid", notifyOid);	  
								ruleDoc.setAttribute("/a_criterion_oid", "0");		  
								ruleDoc.setAttribute("/user_oid", "0");
								inputDoc.addComponent("/In/NotificationRule/NotificationRuleCriterionList("
										+jLoop+")/NotifyRuleUserListList("+cnt+")", ruleDoc);
								cnt++;									

								}else {	//available in ui/db
									notifyOid =userCriteriaHT.get(criterionOid+"_"+dbUniqueArr.get(i));
									String tempUserOid = String.valueOf(dbUniqueArr.get(i));
									if(StringFunction.isBlank(tempUserOid)){
										tempUserOid="";
									}	
									ruleDoc = new DocumentHandler();
									ruleDoc.setAttribute("/notify_rule_user_list_oid", notifyOid);	  
									ruleDoc.setAttribute("/a_criterion_oid", criterionOid);		  
									ruleDoc.setAttribute("/user_oid", tempUserOid);
									inputDoc.addComponent("/In/NotificationRule/NotificationRuleCriterionList("
											+jLoop+")/NotifyRuleUserListList("+cnt+")", ruleDoc);
									cnt++;
									uniqueArr.remove(dbUniqueArr.get(i));
								}
							}
							//new users  to add in db
							for(int i=0;i<uniqueArr.size();i++){

								if(!dbUniqueArr.contains(uniqueArr.get(i))){									

								ruleDoc = new DocumentHandler();
								ruleDoc.setAttribute("/notify_rule_user_list_oid", "");	  
								ruleDoc.setAttribute("/a_criterion_oid", criterionOid);		  
								ruleDoc.setAttribute("/user_oid", String.valueOf(uniqueArr.get(i)));
								inputDoc.addComponent("/In/NotificationRule/NotificationRuleCriterionList("
										+jLoop+")/NotifyRuleUserListList("+cnt+")", ruleDoc);
								cnt++;									
								}
							}
						}						
					}
					//New email ids adding after clearing all
					if(StringFunction.isNotBlank(criterionOid) && (uniqueArr !=null && uniqueArr.size()>0) && (dbUniqueArr != null && dbUniqueArr.size()==0)){ 
						String oid = null;
						for(int len=0;len<uniqueArr.size();len++) {
							oid = String.valueOf(uniqueArr.get(len));
							if(StringFunction.isBlank(oid)){
								oid="";
							}		
							ruleDoc = new DocumentHandler();
							ruleDoc.setAttribute("/notify_rule_user_list_oid", "");	  
							ruleDoc.setAttribute("/a_criterion_oid", criterionOid);		  
							ruleDoc.setAttribute("/user_oid", oid);
							inputDoc.addComponent("/In/NotificationRule/NotificationRuleCriterionList("
									+jLoop+")/NotifyRuleUserListList("+cnt+")", ruleDoc);
							cnt++;
						}
					}//New email ids first time saving
					else if(StringFunction.isBlank(criterionOid)) {	

							if(uniqueArr==null) {
								formattedUserIds = getUserList(userIds);
								uniqueArr = getUniqueUserIds(formattedUserIds);
							}					
							for(int len=0;len<uniqueArr.size();len++) {
								ruleDoc = new DocumentHandler();	
								ruleDoc.setAttribute("/notify_rule_user_list_oid", "");	  
								ruleDoc.setAttribute("/a_criterion_oid", "");		  
								ruleDoc.setAttribute("/user_oid", uniqueArr.get(len).toString());
								inputDoc.addComponent("/In/NotificationRule/NotificationRuleCriterionList("
										+jLoop+")/NotifyRuleUserListList("+cnt+")", ruleDoc);
								cnt++;
							}
						}

					if(StringFunction.isNotBlank(userIds)){						
						inputDoc.setAttribute("/In/NotificationRule/NotificationRuleCriterionList("+jLoop+")/notification_user_ids",formattedUserIds);
					}// if check
					else{
						inputDoc.setAttribute("/In/NotificationRule/NotificationRuleCriterionList("+jLoop+")/notification_user_ids","");
					}
				}//for loop

				//preparing deletion of notify user list

			}		// notification rule user doc setup end
		}			
		//instrument header setup start

		Vector defListVector = inputDoc.getFragments("/In/NotificationRule/DefaultList");
		int defSize = defListVector.size();


		for (iLoop=1; iLoop<=defSize; iLoop++)
		{
			String changeInstType = inputDoc.getAttribute("/In/NotificationRule/DefaultList("+iLoop+")/change_instType");
			String sendEmailSetting = inputDoc.getAttribute("/In/NotificationRule/DefaultList("+iLoop+")/send_email_setting");
			String sendNotifSetting = inputDoc.getAttribute("/In/NotificationRule/DefaultList("+iLoop+")/send_notif_setting");
			String applyToAllTran = inputDoc.getAttribute("/In/NotificationRule/DefaultList("+iLoop+")/apply_to_all_tran");
			String clearToAllTran = inputDoc.getAttribute("/In/NotificationRule/DefaultList("+iLoop+")/clear_to_all_tran");
			String updateRecipientsOnlyTran = inputDoc.getAttribute("/In/NotificationRule/DefaultList("+iLoop+")/update_recipients_only_tran");
			String updateEmailsOnlyTran = inputDoc.getAttribute("/In/NotificationRule/DefaultList("+iLoop+")/update_emails_only_tran");
			String addtionalEmailId = inputDoc.getAttribute("/In/NotificationRule/DefaultList("+iLoop+")/additional_email_addr");
			String notifcationUserIds = inputDoc.getAttribute("/In/NotificationRule/DefaultList("+iLoop+")/notificationUserIds");
			String instrumentType =  inputDoc.getAttribute("/In/NotificationRule/DefaultList("+iLoop+")/instrument_type");
			String emailFreq	 =  inputDoc.getAttribute("/In/NotificationRule/DefaultList("+iLoop+")/notify_email_freq");
			String instType = null;
			String  tempUsr = null;
			if(StringFunction.isNotBlank(notifcationUserIds)){
				tempUsr =	getUserList(notifcationUserIds);
			}
			if(StringFunction.isBlank(sendEmailSetting) && StringFunction.isBlank(sendNotifSetting) && 
					(StringFunction.isBlank(addtionalEmailId) || StringFunction.isBlank(notifcationUserIds))){
				//applyToAllTran = "";
			}
			else if(StringFunction.isNotBlank(sendEmailSetting) && StringFunction.isNotBlank(emailFreq) && 
					(StringFunction.isNotBlank(addtionalEmailId) || StringFunction.isNotBlank(notifcationUserIds))){
				//applyToAllTran = "Y";
			}
			if(clearSet.contains(instrumentType)){
				clearToAllTran = "Y";
			}
			inputDoc.setAttribute("/In/NotificationRule/send_email_setting"+iLoop,sendEmailSetting);
			inputDoc.setAttribute("/In/NotificationRule/send_notif_setting"+iLoop,sendNotifSetting);
			inputDoc.setAttribute("/In/NotificationRule/apply_to_all_tran"+iLoop,applyToAllTran);
			inputDoc.setAttribute("/In/NotificationRule/clear_to_all_tran"+iLoop,clearToAllTran);
			inputDoc.setAttribute("/In/NotificationRule/update_recipients_only_tran"+iLoop,updateRecipientsOnlyTran);
			inputDoc.setAttribute("/In/NotificationRule/update_emails_only_tran"+iLoop,updateEmailsOnlyTran);
			inputDoc.setAttribute("/In/NotificationRule/notification_user_ids"+iLoop,tempUsr);
			inputDoc.setAttribute("/In/NotificationRule/instrument_type"+iLoop,instrumentType);
			inputDoc.setAttribute("/In/NotificationRule/additional_email_addr"+iLoop,addtionalEmailId);				    	
		}										

		//instrument header setup start
		//Srinivasu D CR#927b Rel9.5 02/01/2016 - End

		//}



		String  login_oid =    inputDoc.getAttribute("/In/LoginUser/user_oid");
		System.out.println("***********************user_oid*****"+login_oid);
		String  security_rights =    inputDoc.getAttribute("/In/LoginUser/security_rights");
		System.out.println("***********************security_rights*****"+security_rights);


		//	   String  email_for_message =    inputDoc.getAttribute("/In/NotificationRule/email_for_message");
		//	   System.out.println("***********************email_for_message*****"+email_for_message);
		//   String  email_for_discrepancy =    inputDoc.getAttribute("/In/NotificationRule/email_for_discrepancy");
		//   System.out.println("***********************email_for_discrepancy*****"+email_for_discrepancy);
		//  String  email_for_ar_match_notice =    inputDoc.getAttribute("/In/NotificationRule/email_for_ar_match_notice");
		//  System.out.println("***********************email_for_ar_match_notice*****"+email_for_ar_match_notice);
		String  part_to_validate =    inputDoc.getAttribute("/In/NotificationRule/part_to_validate");
		System.out.println("***********************part_to_validate*****"+part_to_validate);
		String  ownership_type =    inputDoc.getAttribute("/In/NotificationRule/ownership_type");
		System.out.println("***********************ownership_type*****"+ownership_type);
		String  email_frequency =    inputDoc.getAttribute("/In/NotificationRule/email_frequency");
		System.out.println("***********************email_frequency*****"+email_frequency);
		String  description =    inputDoc.getAttribute("/In/NotificationRule/description");
		System.out.println("***********************description*****"+description);

		String  transactionTypeOrAction =    inputDoc.getAttribute("/In/NotificationRule/NotificationRuleCriterionList/transaction_type_or_action");
		System.out.println("***********************transactionTypeOrAction*****"+transactionTypeOrAction);
		String  instTypeOrCategory =    inputDoc.getAttribute("/In/NotificationRule/NotificationRuleCriterionList/instrument_type_or_category");
		System.out.println("***********************instrument_type_or_category*****"+instTypeOrCategory);
		String  sendNotifSetting =    inputDoc.getAttribute("/In/NotificationRule/NotificationRuleCriterionList/send_notif_setting");
		System.out.println("***********************sendNotifSetting*****"+sendNotifSetting);

		String  opt_lock =    inputDoc.getAttribute("/In/NotificationRule/opt_lock");
		System.out.println("***********************opt_lock*****>>"+opt_lock);
		System.out.println("final return to Mediator Doc:"+inputDoc);
			}

	/**
	 * This method returns unique user ids from list of oids
	 * @param oids
	 * @return
	 */

	private List getUniqueUserIds(String oids){

		String st[]= oids.split(",");
		Set hset = new HashSet();
		for(int counter=0;counter<st.length;counter++){
			if(StringFunction.isNotBlank(st[counter]) && !"0".equals(st[counter])){
				//if(!"0".equals(st[counter])) {
				hset.add(st[counter]);
			}
		}
		//System.out.println("hset;"+hset+"\t size:"+hset.size());
		List lt = new ArrayList(hset);

		return lt;
	}

	private String getUserList(String oids){

		StringBuilder res = new StringBuilder();
		if(StringFunction.isNotBlank(oids)){
			String st[]= oids.split(",");
			for(int counter=0;counter<st.length;counter++){
				if(StringFunction.isNotBlank(st[counter]) && !("0".equals(st[counter]) || "null".equals(st[counter]))) {
					res.append(st[counter]+",");
				}
			}

			if(res !=null && res.length()>1) {
				res.delete(res.length()-1, res.length());
			}
		}
		return res.toString();
	}

}
