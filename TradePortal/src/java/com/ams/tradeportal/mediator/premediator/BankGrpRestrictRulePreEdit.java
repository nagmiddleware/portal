package com.ams.tradeportal.mediator.premediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Insert the type's description here.
 *
 *     Copyright  © 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */

import com.amsinc.ecsg.web.*;

// import javax.servlet.ServletRequest; // PRUK010444171
import javax.servlet.http.*;

import java.util.*;

import com.amsinc.ecsg.util.*;
import com.amsinc.ecsg.frame.*;
import com.ams.tradeportal.common.*;
import com.ams.tradeportal.busobj.util.*;

public class BankGrpRestrictRulePreEdit implements com.amsinc.ecsg.web.WebAction
{
private static final Logger LOG = LoggerFactory.getLogger(BankGrpRestrictRulePreEdit.class);
	/**
	 * BankGrpRestrictRulePreEdit constructor comment.
	 */
	public BankGrpRestrictRulePreEdit()
	{
		super();
	}

	/**
	 *
	 */
	public void act(AmsServletInvocation reqInfo,
			BeanManager beanMgr,
			FormManager formMgr,
			ResourceManager resMgr,
			String serverLocation,
			HttpServletRequest request,
			HttpServletResponse response,
			Hashtable inputParmMap,
			DocumentHandler inputDoc) throws AmsException {

		LOG.debug("[BankGrpRestrictRulePreEdit.act()] "+"BankGrpRestrictRulePreEdit started");
		Vector bankGroups = new Vector();
		
		for(int i=0;i<inputDoc.getFragments("/In/BankGrpRestrictRule/BankGrpForRestrictRuleList").size();i++){
			bankGroups.addElement(inputDoc.getComponent("/In/BankGrpRestrictRule/BankGrpForRestrictRuleList("+i+")")) ;
		}
		int noOfItems = bankGroups.size();
		MediatorServices medService = new MediatorServices();
		for (int jLoop=0; jLoop<noOfItems; jLoop++)
		{
			DocumentHandler bnkGrpForRestictDoc = (DocumentHandler) bankGroups.elementAt(jLoop);

			String bnkOrgGrp = bnkGrpForRestictDoc.getAttribute("/bank_organization_group_oid");
			String bnkGrpForRestrictRlOid = bnkGrpForRestictDoc.getAttribute("/bank_grp_for_restrict_rls_oid");
			if(StringFunction.isBlank(bnkOrgGrp) && (StringFunction.isBlank(bnkGrpForRestrictRlOid)||bnkGrpForRestrictRlOid.equals("0"))){
				inputDoc.removeComponent("/In/BankGrpRestrictRule/BankGrpForRestrictRuleList("+jLoop+")");
			}
			else if(StringFunction.isBlank(bnkOrgGrp) && (!StringFunction.isBlank(bnkGrpForRestrictRlOid)||!bnkGrpForRestrictRlOid.equals("0"))){
				DocumentHandler deleteDoc = new DocumentHandler();
				deleteDoc.setAttribute("/Name", "BankGrpForRestrictRule");
				deleteDoc.setAttribute("/Oid", bnkGrpForRestrictRlOid);
				
				inputDoc.removeComponent("/In/BankGrpRestrictRule/BankGrpForRestrictRuleList("+jLoop+")");
				inputDoc.addComponent("/In/ComponentToDelete("+jLoop+")", deleteDoc);
			}
		}
		/*IR#T36000041349 Start*/ 
		if(inputDoc.getFragments("/In/BankGrpRestrictRule/BankGrpForRestrictRuleList").size()==0){
			medService.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,TradePortalConstants.ONE_BANK_GROUP_MUST_SELECTED);
			StringBuffer sqlQuery = new StringBuffer();
            sqlQuery.append("select BANK_GRP_FOR_RESTRICT_RLS_OID,BANK_ORGANIZATION_GROUP_OID ");
            sqlQuery.append(" from BANK_GRP_FOR_RESTRICT_RULES");
            sqlQuery.append(" where P_BANK_GRP_RESTRICT_RULES_OID = ? ");
            sqlQuery.append(" order by BANK_GRP_FOR_RESTRICT_RLS_OID");
            List params=new ArrayList();
	    	params.add(inputDoc.getAttribute("/In/BankGrpRestrictRule/bank_grp_restrict_rules_oid"));
	        DocumentHandler bankGrpList = new DocumentHandler();
            bankGrpList = DatabaseQueryBean.getXmlResultSet(sqlQuery.toString(),false,params);
            Vector bgVector = bankGrpList.getFragments("/ResultSetRecord");
            for (int iLoop=0; iLoop<bgVector.size(); iLoop++)
		      {
            	DocumentHandler bankGrpforrestricDoc = (DocumentHandler) bgVector.elementAt(iLoop);
		 	    DocumentHandler addDoc = new DocumentHandler();
		 	    addDoc.setAttribute("/bank_grp_for_restrict_rls_oid", bankGrpforrestricDoc.getAttribute("/BANK_GRP_FOR_RESTRICT_RLS_OID"));
		 	    addDoc.setAttribute("/bank_organization_group_oid", bankGrpforrestricDoc.getAttribute("/BANK_ORGANIZATION_GROUP_OID"));
		 	    inputDoc.addComponent("/In/BankGrpRestrictRule/BankGrpForRestrictRuleList("+iLoop+")", addDoc);
			  }
			
		}
		
		if(medService.getErrorManager().getErrorCount()>0){
			medService.addErrorInfo();
		  	inputDoc.setComponent ("/Error", medService.getErrorDoc ());
		  	return;
		}
		/*IR#T36000041349 End*/
		LOG.debug("[BankGrpRestrictRulePreEdit.act()] "+"BankGrpRestrictRulePreEdit Ended");



	}
}
