package com.ams.tradeportal.mediator.premediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */

import com.amsinc.ecsg.web.*;
import javax.servlet.http.*;
import java.util.*;
import com.amsinc.ecsg.util.*;
import com.amsinc.ecsg.frame.*;
import com.ams.tradeportal.common.*;
import com.ams.tradeportal.busobj.util.*;

public class LCCreationRulePreEdit implements com.amsinc.ecsg.web.WebAction {
private static final Logger LOG = LoggerFactory.getLogger(LCCreationRulePreEdit.class);


 public void act(AmsServletInvocation reqInfo, 
				BeanManager beanMgr, 
				FormManager formMgr, 
				ResourceManager resMgr, 
				String serverLocation, 
				HttpServletRequest request, 
				HttpServletResponse response, 
				Hashtable inputParmMap,
				DocumentHandler inputDoc)
			throws com.amsinc.ecsg.frame.AmsException
 {
        // Convert decimal numbers from their locale-specific entry format into
        // a standard format that can be understood by the database.
        // If the format is invalid, an error value will be placed into
        // the XML that will trigger an error later.
        // In the corresponding post mediator action, the passed-in value
        // is placed back into its original location.


        if(inputDoc.getAttribute("/In/LCCreationRule/maximum_amount") != null)
           inputDoc.setAttribute("/In/LCCreationRule/userEnteredmaximum_amount", inputDoc.getAttribute("/In/LCCreationRule/maximum_amount"));
        else
           inputDoc.setAttribute("/In/LCCreationRule/userEnteredmaximum_amount", "");

        String amount;
 
        try 
         {
            // Convert the amount from its localized form to standard (en_US) form
            amount =  NumberValidator.getNonInternationalizedValue(inputDoc.getAttribute("/In/LCCreationRule/maximum_amount"), resMgr.getResourceLocale(), true);
         }
        catch(InvalidAttributeValueException iae)
         {
            // If the format was invalid, set amount equal to something that will trigger an error
            // to be issued.
            amount = TradePortalConstants.BAD_AMOUNT;
         } 

        // Place the result of the above code into the amount location
        // This is what will be populated into the business object.
        inputDoc.setAttribute("/In/LCCreationRule/maximum_amount", amount);
 } 



 
}