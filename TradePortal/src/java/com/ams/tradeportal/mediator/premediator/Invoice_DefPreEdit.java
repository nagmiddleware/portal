package com.ams.tradeportal.mediator.premediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
*
*
*     Copyright  � 2001                         
*     American Management Systems, Incorporated 
*     All rights reserved
*/

import com.amsinc.ecsg.web.*;
import javax.servlet.http.*;
import java.util.*;
import com.amsinc.ecsg.util.*;
import com.ams.tradeportal.common.*;
import java.math.*;
import com.ams.tradeportal.busobj.webbean.*;
import com.ams.tradeportal.busobj.util.*;


public class Invoice_DefPreEdit implements com.amsinc.ecsg.web.WebAction{
private static final Logger LOG = LoggerFactory.getLogger(Invoice_DefPreEdit.class);

	/**
	 * AdminUserPreEdit constructor comment.
	 */
	public Invoice_DefPreEdit() {
		super();
   }

	/**Explicitly sets all 'missing' checkbox fields to N.
	 * (Checkbox fields which are not checked are not sent in the document,
	 * we must force an N to cause unchecked values to be save correctly.)
	 * 
	 */
	public void act(AmsServletInvocation reqInfo, 
					BeanManager beanMgr, 
					FormManager formMgr, 
					ResourceManager resMgr, 
					String serverLocation, 
					HttpServletRequest request, 
					HttpServletResponse response, 
					Hashtable inputParmMap,
					DocumentHandler inputDoc)
		throws com.amsinc.ecsg.frame.AmsException {

		String value;
		
		// get the input document 
		if (inputDoc == null) {
			LOG.info("Input document to Invoice_DefPreEdit is null");
			return;
		}
		
	    doInvoiceDefPreEdit( inputDoc );
	}

	/**
	 * Performs pre-editing of the input document that is specific to the
	 * Invoice Definition Detail JSP. Also, we need to explicitly set the
	 * default checkbox value to N when it is not already present in the document.
	 *
	 * @param doc com.amsinc.ecsg.util.DocumentHandler
	 */
	private void doInvoiceDefPreEdit(DocumentHandler inputDoc) {

		// get the input document 
		if (inputDoc == null) {
			LOG.info("Input document to Invoice_PreEdit is null");
			return;
		}

		// Checkboxes do not send values if the checkbox is unchecked.
		// Therefore, for each checkbox box, determine if we have a
		// value in the document.  If not, add an explicit N value for
		// each one that is missing.  This ensures that checkboxes that
		// are unchecked by the user get reset to N.
		
		
		// Checking Invoice Summary Deatil field required check box
		resetCheckBoxAttribute( inputDoc, "/In/InvoiceDefinition/linked_to_instrument_type_req" );
		resetCheckBoxAttribute( inputDoc, "/In/InvoiceDefinition/incoterm_req" );
		resetCheckBoxAttribute( inputDoc, "/In/InvoiceDefinition/country_of_loading_req" );
		resetCheckBoxAttribute( inputDoc, "/In/InvoiceDefinition/country_of_discharge_req" );
		resetCheckBoxAttribute( inputDoc, "/In/InvoiceDefinition/vessel_req" );
		resetCheckBoxAttribute( inputDoc, "/In/InvoiceDefinition/carrier_req" );
		resetCheckBoxAttribute( inputDoc, "/In/InvoiceDefinition/actual_ship_date_req" );
		resetCheckBoxAttribute( inputDoc, "/In/InvoiceDefinition/payment_date_req" );
		resetCheckBoxAttribute( inputDoc, "/In/InvoiceDefinition/purchase_order_id_req" );
		resetCheckBoxAttribute( inputDoc, "/In/InvoiceDefinition/goods_description_req" );
		resetCheckBoxAttribute( inputDoc, "/In/InvoiceDefinition/include_column_headers" ); // Nar IR-NNUM022937252 03/24/2012
		resetCheckBoxAttribute( inputDoc, "/In/InvoiceDefinition/seller_id_req" );
		resetCheckBoxAttribute( inputDoc, "/In/InvoiceDefinition/buyer_id_req" );
		resetCheckBoxAttribute( inputDoc, "/In/InvoiceDefinition/seller_name_req" );
		resetCheckBoxAttribute( inputDoc, "/In/InvoiceDefinition/buyer_name_req" );
		resetCheckBoxAttribute( inputDoc, "/In/InvoiceDefinition/invoice_type_req" );
		resetCheckBoxAttribute( inputDoc, "/In/InvoiceDefinition/credit_note_req" ); // DK CR-709 Rel8.2
		resetCheckBoxAttribute( inputDoc, "/In/InvoiceDefinition/pay_method_req" ); // DK CR-709 Rel8.2
		resetCheckBoxAttribute( inputDoc, "/In/InvoiceDefinition/ben_acct_num_req" ); // DK CR-709 Rel8.2
		resetCheckBoxAttribute( inputDoc, "/In/InvoiceDefinition/ben_add1_req" ); // DK CR-709 Rel8.2
		resetCheckBoxAttribute( inputDoc, "/In/InvoiceDefinition/ben_add2_req" ); // DK CR-709 Rel8.2
		resetCheckBoxAttribute( inputDoc, "/In/InvoiceDefinition/ben_add3_req" ); // DK CR-709 Rel8.2
		resetCheckBoxAttribute( inputDoc, "/In/InvoiceDefinition/ben_country_req" ); // DK CR-709 Rel8.2
		resetCheckBoxAttribute( inputDoc, "/In/InvoiceDefinition/ben_bank_name_req" ); // DK CR-709 Rel8.2
		resetCheckBoxAttribute( inputDoc, "/In/InvoiceDefinition/ben_branch_code_req" ); // DK CR-709 Rel8.2
		resetCheckBoxAttribute( inputDoc, "/In/InvoiceDefinition/ben_branch_add1_req" ); // DK CR-709 Rel8.2
		resetCheckBoxAttribute( inputDoc, "/In/InvoiceDefinition/ben_branch_add2_req" ); // DK CR-709 Rel8.2
		resetCheckBoxAttribute( inputDoc, "/In/InvoiceDefinition/ben_bank_city_req" ); // DK CR-709 Rel8.2
		resetCheckBoxAttribute( inputDoc, "/In/InvoiceDefinition/ben_bank_province_req" ); // DK CR-709 Rel8.2
		resetCheckBoxAttribute( inputDoc, "/In/InvoiceDefinition/ben_branch_country_req" ); // DK CR-709 Rel8.2
		resetCheckBoxAttribute( inputDoc, "/In/InvoiceDefinition/charges_req" ); // DK CR-709 Rel8.2
		resetCheckBoxAttribute( inputDoc, "/In/InvoiceDefinition/central_bank_rep1_req" ); // DK CR-709 Rel8.2
		resetCheckBoxAttribute( inputDoc, "/In/InvoiceDefinition/central_bank_rep2_req" ); // DK CR-709 Rel8.2
		resetCheckBoxAttribute( inputDoc, "/In/InvoiceDefinition/central_bank_rep3_req" ); // DK CR-709 Rel8.2
		resetCheckBoxAttribute( inputDoc, "/In/InvoiceDefinition/end_to_end_id_req" ); // Narayan CR914 Rel9.2 03-Nov-2014
		
		resetCheckBoxAttribute( inputDoc, "/In/InvoiceDefinition/credit_note_data_req" ); // DK CR-709 Rel8.2
		resetCheckBoxAttribute( inputDoc, "/In/InvoiceDefinition/pay_method_data_req" ); // DK CR-709 Rel8.2
		resetCheckBoxAttribute( inputDoc, "/In/InvoiceDefinition/ben_acct_num_data_req" ); // DK CR-709 Rel8.2
		resetCheckBoxAttribute( inputDoc, "/In/InvoiceDefinition/ben_add1_data_req" ); // DK CR-709 Rel8.2
		resetCheckBoxAttribute( inputDoc, "/In/InvoiceDefinition/ben_add2_data_req" ); // DK CR-709 Rel8.2
		resetCheckBoxAttribute( inputDoc, "/In/InvoiceDefinition/ben_add3_data_req" ); // DK CR-709 Rel8.2
		resetCheckBoxAttribute( inputDoc, "/In/InvoiceDefinition/ben_country_data_req" ); // DK CR-709 Rel8.2
		resetCheckBoxAttribute( inputDoc, "/In/InvoiceDefinition/ben_bank_name_data_req" ); // DK CR-709 Rel8.2
		resetCheckBoxAttribute( inputDoc, "/In/InvoiceDefinition/ben_branch_code_data_req" ); // DK CR-709 Rel8.2
		resetCheckBoxAttribute( inputDoc, "/In/InvoiceDefinition/ben_branch_add1_data_req" ); // DK CR-709 Rel8.2
		resetCheckBoxAttribute( inputDoc, "/In/InvoiceDefinition/ben_branch_add2_data_req" ); // DK CR-709 Rel8.2
		resetCheckBoxAttribute( inputDoc, "/In/InvoiceDefinition/ben_bank_city_data_req" ); // DK CR-709 Rel8.2
		resetCheckBoxAttribute( inputDoc, "/In/InvoiceDefinition/ben_bank_province_data_req" ); // DK CR-709 Rel8.2
		resetCheckBoxAttribute( inputDoc, "/In/InvoiceDefinition/ben_branch_country_data_req" ); // DK CR-709 Rel8.2
		resetCheckBoxAttribute( inputDoc, "/In/InvoiceDefinition/charges_data_req" ); // DK CR-709 Rel8.2
		resetCheckBoxAttribute( inputDoc, "/In/InvoiceDefinition/central_bank_rep1_data_req" ); // DK CR-709 Rel8.2
		resetCheckBoxAttribute( inputDoc, "/In/InvoiceDefinition/central_bank_rep2_data_req" ); // DK CR-709 Rel8.2
		resetCheckBoxAttribute( inputDoc, "/In/InvoiceDefinition/central_bank_rep3_data_req" ); // DK CR-709 Rel8.2
		resetCheckBoxAttribute( inputDoc, "/In/InvoiceDefinition/send_to_supplier_date_req" ); // Narayan CR-913 Rel9.0 30-Jan-2014
		resetCheckBoxAttribute( inputDoc, "/In/InvoiceDefinition/buyer_acct_currency_req" ); // Narayan CR-913 Rel9.0 15-Feb-2014
		resetCheckBoxAttribute( inputDoc, "/In/InvoiceDefinition/buyer_acct_num_req" ); // Narayan CR-913 Rel9.0 15-Feb-2014
		resetCheckBoxAttribute( inputDoc, "/In/InvoiceDefinition/ben_bank_sort_code_req" ); 
		resetCheckBoxAttribute( inputDoc, "/In/InvoiceDefinition/ben_bank_sort_code_data_req" );
		resetCheckBoxAttribute( inputDoc, "/In/InvoiceDefinition/end_to_end_id_data_req" ); // Narayan CR914 Rel9.2 03-Nov-2014

		for(int x=1; x<=TradePortalConstants.INV_BUYER_OR_SELLER_USER_DEF_FIELD; x++){
			resetCheckBoxAttribute( inputDoc, "/In/InvoiceDefinition/buyer_users_def" + x + "_req");
			resetCheckBoxAttribute( inputDoc, "/In/InvoiceDefinition/seller_users_def" + x + "_req");			
		}
		
		//Checking line item field required check box
		
		resetCheckBoxAttribute( inputDoc, "/In/InvoiceDefinition/line_item_id_req" );
		resetCheckBoxAttribute( inputDoc, "/In/InvoiceDefinition/unit_price_req" );
		resetCheckBoxAttribute( inputDoc, "/In/InvoiceDefinition/unit_of_measure_price_req" );
		resetCheckBoxAttribute( inputDoc, "/In/InvoiceDefinition/inv_quantity_req" );
		resetCheckBoxAttribute( inputDoc, "/In/InvoiceDefinition/unit_of_measure_quantity_req" );
		resetCheckBoxAttribute( inputDoc, "/In/InvoiceDefinition/line_item_detail_provided" );
		
		
		for(int x=1; x<=TradePortalConstants.INV_LINE_ITEM_TYPE_OR_VALUE_FIELD; x++){
			resetCheckBoxAttribute( inputDoc, "/In/InvoiceDefinition/prod_chars_ud" + x + "_type_req");		
		}
		
		resetCheckBoxAttribute( inputDoc, "/In/InvoiceDefinition/linked_to_instrument_type_data_req" );
		resetCheckBoxAttribute( inputDoc, "/In/InvoiceDefinition/incoterm_data_req" );
		resetCheckBoxAttribute( inputDoc, "/In/InvoiceDefinition/country_of_loading_data_req" );
		resetCheckBoxAttribute( inputDoc, "/In/InvoiceDefinition/country_of_discharge_data_req" );
		resetCheckBoxAttribute( inputDoc, "/In/InvoiceDefinition/vessel_data_req" );
		resetCheckBoxAttribute( inputDoc, "/In/InvoiceDefinition/carrier_data_req" );
		resetCheckBoxAttribute( inputDoc, "/In/InvoiceDefinition/actual_ship_date_data_req" );
		resetCheckBoxAttribute( inputDoc, "/In/InvoiceDefinition/payment_date_data_req" );
		resetCheckBoxAttribute( inputDoc, "/In/InvoiceDefinition/purchase_order_id_data_req" );
		resetCheckBoxAttribute( inputDoc, "/In/InvoiceDefinition/goods_description_data_req" );
		resetCheckBoxAttribute( inputDoc, "/In/InvoiceDefinition/invoice_type_data_req" );
		// Narayan CR-913 Rel9.0 30-Jan-2014 ADD- Begin
		resetCheckBoxAttribute( inputDoc, "/In/InvoiceDefinition/send_to_supplier_date_data_req" );
		resetCheckBoxAttribute( inputDoc, "/In/InvoiceDefinition/buyer_acct_currency_data_req" );
		resetCheckBoxAttribute( inputDoc, "/In/InvoiceDefinition/buyer_acct_num_data_req" );
		// Narayan CR-913 Rel9.0 30-Jan-2014 ADD- End
		for(int x=1; x<=TradePortalConstants.INV_BUYER_OR_SELLER_USER_DEF_FIELD; x++){
			resetCheckBoxAttribute( inputDoc, "/In/InvoiceDefinition/buyer_users_def" + x + "_label_data_req");
			resetCheckBoxAttribute( inputDoc, "/In/InvoiceDefinition/seller_users_def" + x + "_label_data_req");			
		}
		
		//Checking line item field required check box
		
		resetCheckBoxAttribute( inputDoc, "/In/InvoiceDefinition/line_item_id_data_req" );
		resetCheckBoxAttribute( inputDoc, "/In/InvoiceDefinition/unit_price_data_req" );
		resetCheckBoxAttribute( inputDoc, "/In/InvoiceDefinition/unit_of_measure_price_data_req" );
		resetCheckBoxAttribute( inputDoc, "/In/InvoiceDefinition/inv_quantity_data_req" );
		resetCheckBoxAttribute( inputDoc, "/In/InvoiceDefinition/unit_of_measure_quantity_data_req" );
				
		for(int x=1; x<=TradePortalConstants.INV_LINE_ITEM_TYPE_OR_VALUE_FIELD; x++){
			resetCheckBoxAttribute( inputDoc, "/In/InvoiceDefinition/prod_chars_ud" + x + "_type_data_req");		
		}
		//Rel8.2 CR-741 Discount Code checkboxes reset - start
		//
		resetCheckBoxAttribute( inputDoc, "/In/InvoiceDefinition/discount_code_req" );
		resetCheckBoxAttribute( inputDoc, "/In/InvoiceDefinition/discount_gl_code_req" );
		resetCheckBoxAttribute( inputDoc, "/In/InvoiceDefinition/discount_comments_req" );
		resetCheckBoxAttribute( inputDoc, "/In/InvoiceDefinition/discount_code_data_req" );
		resetCheckBoxAttribute( inputDoc, "/In/InvoiceDefinition/discount_gl_code_data_req" );
		resetCheckBoxAttribute( inputDoc, "/In/InvoiceDefinition/discount_comments_data_req" );	
		//
		//CR-741 - end
		
		//CR1001 Rel9.4 Start
		resetCheckBoxAttribute( inputDoc, "/In/InvoiceDefinition/reporting_code_1_req" );
		resetCheckBoxAttribute( inputDoc, "/In/InvoiceDefinition/reporting_code_1_data_req" );	
		resetCheckBoxAttribute( inputDoc, "/In/InvoiceDefinition/reporting_code_2_req" );
		resetCheckBoxAttribute( inputDoc, "/In/InvoiceDefinition/reporting_code_2_data_req" );
		
		//CR1001 Rel9.4 End
	}

	/**
	 * We check for a value at a given path in the Xml Doc, if it
	 * Does'nt exist then we set that value in the document to 'N'.  This is used for
	 * resetting a Check boxes value since deselection of a check box does NOT update the
	 * Xml Doc with a revised value.
	 *
	 * @param DocumentHandler inputDoc - the document to get and set data against.
	 * @param String xmlPath           - this the the path to use to get/set
	 *                                   attributes in the xml Doc.
	 */
	private static void resetCheckBoxAttribute( DocumentHandler inputDoc, String xmlPath) {

	    String value;

	    value = inputDoc.getAttribute( xmlPath );
	    if (value == null)
	        inputDoc.setAttribute( xmlPath, TradePortalConstants.INDICATOR_NO );
	}
}