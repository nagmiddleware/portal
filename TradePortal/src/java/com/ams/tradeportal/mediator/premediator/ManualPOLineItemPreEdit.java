package com.ams.tradeportal.mediator.premediator;
import java.util.Hashtable;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.busobj.util.POLineItemField;
import com.ams.tradeportal.busobj.webbean.POUploadDefinitionWebBean;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.ResourceManager;
import com.amsinc.ecsg.util.StringFunction;
/**
 * Premediator for Manual PO Line Items.  Removes empty PO Line Item
 * components from the input document
 *
 *     Copyright  � 2003                        
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
import com.amsinc.ecsg.web.AmsServletInvocation;
import com.amsinc.ecsg.web.BeanManager;
import com.amsinc.ecsg.web.FormManager;

public class ManualPOLineItemPreEdit implements com.amsinc.ecsg.web.WebAction
{
private static final Logger LOG = LoggerFactory.getLogger(ManualPOLineItemPreEdit.class);
   /**
    * ManualPOLineItemPreEdit constructor comment.
    */
   public ManualPOLineItemPreEdit()
   {
      super();
   }

   /**
    * 
    */
   public void act(AmsServletInvocation reqInfo, 
                   BeanManager beanMgr, 
                   FormManager formMgr, 
                   ResourceManager resMgr, 
                   String serverLocation, 
                   HttpServletRequest request, 
                   HttpServletResponse response, 
                   Hashtable inputParmMap,
                   DocumentHandler inputDoc) throws AmsException
   {
      DocumentHandler    poLineItemDoc           = null;
      String             buttonPressed           = null;
      String             selectedIndicator       = null;
      String             poLineItemOid           = null;
      int                numPOLineItems          = 0;
      int                poLineItemCount         = 0;
      int                deletedPOLineItemCount  = 0;
      Vector             poLineItemsList         = new Vector();

      // Retrieve the input document 
      if (inputDoc == null)
      {
         LOG.info("Input document to ManualPOLineItemPreEdit is null");
         return;
      }

      // If PO Line Attributes (other1, other2, etc) are not utilized then an empty component is
      // created which should be removed
      if (inputDoc.getDocumentNode("/In/ShipmentTerms/POLineItemList/po_line_item_oid") == null)
      {
        inputDoc.removeComponent("/In/ShipmentTerms/POLineItemList");
      }

      // Determine which button was pressed by the user
      buttonPressed = inputDoc.getAttribute("/In/Update/ButtonPressed");

      if (buttonPressed.equals(TradePortalConstants.BUTTON_SAVEANDCLOSE) ||
          buttonPressed.equals(TradePortalConstants.BUTTON_DELETE_SELECTED_PO_ITEMS))
      {
        // Retrieve the list of PO Line Items and determine the PO Line Item count
        poLineItemsList = inputDoc.getFragments("/In/ShipmentTerms/POLineItemList");
        numPOLineItems  = poLineItemsList.size();

        // Reset the deleted count
        inputDoc.setAttribute("/In/deleteCount", "0");

        // Remove empty components and ones selected for deletion
        for (poLineItemCount=0; poLineItemCount<numPOLineItems; poLineItemCount++)
        {
          poLineItemDoc     = (DocumentHandler) poLineItemsList.elementAt(poLineItemCount);
          poLineItemOid     = poLineItemDoc.getAttribute("/po_line_item_oid");
          selectedIndicator = poLineItemDoc.getAttribute("/selected_indicator");

          // If the user pressed the button which deletes po line items then remove all selected 
          // PO Line Items from the input document so this data is not saved and add the oid
          // to a new section in the document for processing in the mediator
          boolean deletePOLineItem = false;
          if (buttonPressed.equals(TradePortalConstants.BUTTON_DELETE_SELECTED_PO_ITEMS))
          {
            if (!StringFunction.isBlank(selectedIndicator) && 
                selectedIndicator.equals(TradePortalConstants.INDICATOR_YES))
            {
              deletePOLineItem = true;
              deletedPOLineItemCount++;
            }
          }

          // Remove the PO Line Item from the input document if it was selected for deletion or
          // if this is an empty line item
          if (((StringFunction.isBlank(poLineItemOid)) && 
              (isPOLineItemEmpty(beanMgr, inputDoc, poLineItemDoc) == true)) || (deletePOLineItem))
          {
            inputDoc.removeComponent("/In/ShipmentTerms/POLineItemList("+poLineItemCount+")");

            // If the PO Line Item contains an oid and was selected for deletion 
            // then it exists in the database and should be deleted
            if (!StringFunction.isBlank(poLineItemOid) && deletePOLineItem)
            {
              DocumentHandler deleteDoc = new DocumentHandler();
              deleteDoc.setAttribute("/Oid", poLineItemOid);
              inputDoc.addComponent("/In/ComponentToDelete("+poLineItemCount+")", deleteDoc);
            }
          }
        }
        inputDoc.setAttribute("/In/deleteCount", String.valueOf(deletedPOLineItemCount));
      }
   }

   /**
    * This method is used to determine if the user entered any data for this PO Line Item
    * 
    * @param       inputDoc DocumentHandler  
    * @param       poLineItemDoc DocumentHandler  
    * @return      boolean
    */

   private boolean isPOLineItemEmpty(BeanManager beanMgr, DocumentHandler inputDoc, DocumentHandler poLineItemDoc) 
   {
     String             definitionOid        = null;
     String             fieldType            = null;
     String             fieldValue           = null;
     int                fieldIndex           = 0;
     int                numberOfPOFields     = 0;
     Vector             poLineItemFieldList  = new Vector();
     POLineItemField    poLineItemField      = null;

     definitionOid = inputDoc.getAttribute("/In/definitionOid");
     POUploadDefinitionWebBean poDefinition  = beanMgr.createBean(POUploadDefinitionWebBean.class, "POUploadDefinition");

     poDefinition.setAttribute("po_upload_definition_oid", definitionOid);
     poDefinition.getDataFromAppServer();

     poLineItemFieldList = poDefinition.loadPOLineItemFields();
     numberOfPOFields    = poLineItemFieldList.size();
     for (fieldIndex = 0; fieldIndex < numberOfPOFields; fieldIndex++)
     {
       poLineItemField = (POLineItemField) poLineItemFieldList.elementAt(fieldIndex);
       fieldType       = poLineItemField.getFieldType();
       fieldValue      = poLineItemDoc.getAttribute("/" + fieldType);
       if (!StringFunction.isBlank(fieldValue))
       {
         return false;
       }
     }
     return true; 
   }

}


