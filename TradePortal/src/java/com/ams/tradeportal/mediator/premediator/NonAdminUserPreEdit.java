package com.ams.tradeportal.mediator.premediator;
import java.util.Hashtable;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.common.HSMEncryptDecrypt;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.ResourceManager;
import com.amsinc.ecsg.util.StringFunction;
/**
 * Insert the type's description here.
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
import com.amsinc.ecsg.web.AmsServletInvocation;
import com.amsinc.ecsg.web.BeanManager;
import com.amsinc.ecsg.web.FormManager;

public class NonAdminUserPreEdit implements com.amsinc.ecsg.web.WebAction {
private static final Logger LOG = LoggerFactory.getLogger(NonAdminUserPreEdit.class);
	/**
	 * AdminUserPreEdit constructor comment.
	 */
	public NonAdminUserPreEdit() {
		super();
	}
	/**
	 * Extracts the OwnerOrgAndLevel value and parses it into two
	 * values: an owner_org_oid and an ownership_level.  These are
	 * placed in the inputDoc under /In/User.  The OwnerOrgAndLevel
	 * is assumed to be of this form: "org/level".
	 */
	public void act(AmsServletInvocation reqInfo,
			BeanManager beanMgr,
			FormManager formMgr,
			ResourceManager resMgr,
			String serverLocation,
			HttpServletRequest request,
			HttpServletResponse response,
			Hashtable inputParmMap,
			DocumentHandler inputDoc)
	throws com.amsinc.ecsg.frame.AmsException {

			// get the input document
		if (inputDoc == null) {
			LOG.info("Input document to AdminUserPreEdit is null");
			return;
		}

		// [START] CR-482
		if(HSMEncryptDecrypt.isInstanceHSMEnabled())
		{
			// [START] PIUK012685963 - No need to decrypt when protectedNewPassword is blank or call the HSM twice to get the same value
			if(!StringFunction.isBlank(inputDoc.getAttribute("/In/User/protectedNewPassword")))
			{
				try
				{
					String decryptedPassword = HSMEncryptDecrypt.decryptForTransport(inputDoc.getAttribute("/In/User/protectedNewPassword"));
					// T36000030120-smanohar--	check password complexity at server also for HSMEnabled instances--
					String decryptedRetyped = HSMEncryptDecrypt.decryptForTransport(inputDoc.getAttribute("/In/User/encryptedRetypedNewPassword"));

					// [START] CR-543
					inputDoc.setAttribute("/In/User/newPassword", decryptedPassword);
					// T36000030120-smanohar--	check password complexity at server also for HSMEnabled instances--
					inputDoc.setAttribute("/In/User/retypePassword", decryptedRetyped);
				} catch (Exception e) {
					System.err.println("[AdminUserPreEdit] Caught exception while trying to decrypt protected password values: "+e.getMessage());
					// [END] CR-543
				}
			} 
		}
		// [END] CR-482

		// If passwords are not used for this user, there will be no password data
		if(inputDoc.getAttribute("/In/User/newPassword") != null)
			// If password is being reset, set the password change date to null
			if((!inputDoc.getAttribute("/In/User/newPassword").equals("")) ||
					(!inputDoc.getAttribute("/In/User/retypePassword").equals("")) )
			{
				inputDoc.setAttribute("/In/User/password_change_date","");
			}

		if (inputDoc.getAttribute("/In/User/use_data_while_in_sub_access") == null)
		{
			inputDoc.setAttribute("/In/User/use_data_while_in_sub_access", TradePortalConstants.INDICATOR_NO);
		}

		//-- CR-451 NShrestha 10/16/2008 Begin --
		if (inputDoc.getAttribute("/In/User/authorize_own_input_ind") == null)
		{
			inputDoc.setAttribute("/In/User/authorize_own_input_ind", TradePortalConstants.INDICATOR_NO);
		}
		
		//CR 821 Rel 8.3
		if (inputDoc.getAttribute("/In/User/sub_authorize_own_input_ind") == null)
		{
			inputDoc.setAttribute("/In/User/sub_authorize_own_input_ind", TradePortalConstants.INDICATOR_NO);
		}

		// Vshah/IAZ CR-586 [Begin]
		if (inputDoc.getAttribute("/In/User/confidential_indicator") == null)
		{
		     inputDoc.setAttribute("/In/User/confidential_indicator", TradePortalConstants.INDICATOR_NO);
		}

		if (inputDoc.getAttribute("/In/User/subsid_confidential_indicator") == null)
		{
		     inputDoc.setAttribute("/In/User/subsid_confidential_indicator", TradePortalConstants.INDICATOR_NO);
		}
		// Vshah/IAZ CR-586 [End]
		
		// DK CR-581/640 Rel7.1 Begins
		if (inputDoc.getAttribute("/In/User/live_market_rate_ind") == null)
		{
		     inputDoc.setAttribute("/In/User/live_market_rate_ind", TradePortalConstants.INDICATOR_NO);
		}

		// DK CR-581/640 Rel7.1 Ends

		// Determine whether accounts are deleted or not entered from
		Vector acctVector = inputDoc.getFragments("/In/User/UserAuthorizedAccountList");
		int numItems = acctVector.size();

		for (int iLoop=0; iLoop<numItems; iLoop++)
		{
			DocumentHandler acctDoc = (DocumentHandler) acctVector.elementAt(iLoop);

			String oid = acctDoc.getAttribute("/authorized_account_oid");
			String refAcctOid = acctDoc.getAttribute("/account_oid");
			String acctDesc = acctDoc.getAttribute("/account_description");
			// referenced accountOid, and description are blank: remove this completely blank component
			// from the input doc.
			if (StringFunction.isBlank(refAcctOid) && StringFunction.isBlank(acctDesc)) {
				inputDoc.removeComponent("/In/User/UserAuthorizedAccountList("+iLoop+")");
				if (!StringFunction.isBlank(oid)) {
					DocumentHandler deleteDoc = new DocumentHandler();
					deleteDoc.setAttribute("/Name", "UserAuthorizedAccount");
					deleteDoc.setAttribute("/Oid", oid);
					inputDoc.addComponent("/In/ComponentToDelete("+iLoop+")", deleteDoc);
				}
			}
		}
		//-- CR-451 NShrestha 10/16/2008 End --

		//CR-586 Vshah [BEGIN]
		Vector templateGroupVector = inputDoc.getFragments("/In/User/UserAuthorizedTemplateGroupList");
		int totalCount = templateGroupVector.size();

		for (int iLoop=0; iLoop<totalCount; iLoop++)
		{
			DocumentHandler templateGroupDoc = (DocumentHandler) templateGroupVector.elementAt(iLoop);

			String oid = templateGroupDoc.getAttribute("/authorized_template_group_oid");
			String refTemplateGroupOid = templateGroupDoc.getAttribute("/payment_template_group_oid");

			// referenced TemplateGroupOid is blank: remove this completely blank component
			// from the input doc.
			if (StringFunction.isBlank(refTemplateGroupOid) ) {
				inputDoc.removeComponent("/In/User/UserAuthorizedTemplateGroupList("+iLoop+")");
				if (!StringFunction.isBlank(oid)) {
					DocumentHandler deleteDoc = new DocumentHandler();
					deleteDoc.setAttribute("/Name", "UserAuthorizedTemplateGroup");
					deleteDoc.setAttribute("/Oid", oid);
					inputDoc.addComponent("/In/ComponentToDelete("+iLoop+")", deleteDoc);
				}
			}
		}
		//CR-586 Vshah [END]

		if (inputDoc.getAttribute("/In/User/allow_sso_password_ind") == null)
		{
		     inputDoc.setAttribute("/In/User/allow_sso_password_ind", TradePortalConstants.INDICATOR_NO);
		}
        }
}