package com.ams.tradeportal.mediator.premediator;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.common.NumberValidator;
import com.ams.tradeportal.common.TPCurrencyUtility;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.frame.InvalidAttributeValueException;
import com.amsinc.ecsg.frame.MediatorServices;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.DocumentNode;
import com.amsinc.ecsg.util.ResourceManager;
import com.amsinc.ecsg.util.StringFunction;
import com.amsinc.ecsg.web.AmsServletInvocation;
import com.amsinc.ecsg.web.BeanManager;
import com.amsinc.ecsg.web.FormManager;
/**
 * Insert the type's description here.
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
import java.rmi.RemoteException;

public class CorporateOrgPreEdit implements com.amsinc.ecsg.web.WebAction
{
private static final Logger LOG = LoggerFactory.getLogger(CorporateOrgPreEdit.class);
	/**
    * CorporateOrgPreEdit constructor comment.
    */
    public CorporateOrgPreEdit()
    {
		super();
	}

	/**
    *
    */
    public void act(AmsServletInvocation reqInfo,
                   BeanManager beanMgr,
                   FormManager formMgr,
                   ResourceManager resMgr,
                   String serverLocation,
                   HttpServletRequest request,
                   HttpServletResponse response,
                   Hashtable inputParmMap,
                   DocumentHandler inputDoc) throws AmsException
    {
      // Retrieve the input document
      if (inputDoc == null)
      {
         LOG.info("Input document to CorporateOrgPreEdit is null");

         return;
      }
      
      // jgadela - Rel8.3 ANZCR501 03/28/2013 START  - if user clicks APPROVE/REJECT button skip all the below validation parts.
      // IR T36000018067  - added In path 
	  String buttonPressed = inputDoc.getAttribute("/In/Update/ButtonPressed");
	  if (buttonPressed.equals(TradePortalConstants.BUTTON_APPROVE_PENDING_REF_DATA) 
	  			|| buttonPressed.equals(TradePortalConstants.BUTTON_REJECT_PENDING_REF_DATA)){
		  return;
	  }
	  //jgadela - Rel8.3 ANZCR501 03/28/2013 END.

      //CR914-Start
	  setPayableCreditNoteSettings(inputDoc);
      //CR914-end
	  
	  //Srini_D CR708 Rel8.1.1 11/10/2012 Start - resetting days before/after to null if no payment date selected
	   String bankDefInd = inputDoc.getAttribute("/In/CorporateOrganization/payment_day_allow");
	   if(TradePortalConstants.INDICATOR_NO.equals(bankDefInd)){
		   inputDoc.setAttribute("/In/CorporateOrganization/days_before", null);
		   inputDoc.setAttribute("/In/CorporateOrganization/days_after", null);
	  }
      //Srini_D CR708 Rel8.1.1 11/10/2012 end
      inputDoc = this.initializeDefaultSettings(inputDoc);
	  inputDoc = this.initializeInstrumentCapabilities(inputDoc);
      inputDoc = this.initializeAccountDetails(inputDoc);

		// Chandrakanth IR PHUI120660503 12/12/2008 Begin
		doDecimalPreMediatorAction(inputDoc, resMgr, "/In/CorporateOrganization/transfer_daily_limit_amt");
    	doDecimalPreMediatorAction(inputDoc, resMgr, "/In/CorporateOrganization/domestic_pymt_daily_limit_amt");
    	doDecimalPreMediatorAction(inputDoc, resMgr, "/In/CorporateOrganization/intl_pymt_daily_limit_amt");

	   //BSL IR BNUM050136832 05/04/2012 Rel 8.0 BEGIN
      String calculateDiscountXpath = "/In/CorporateOrganization/calculate_discount_ind";
      if (StringFunction.isBlank(inputDoc.getAttribute(calculateDiscountXpath))) {
    	  inputDoc.setAttribute(calculateDiscountXpath, null);
      }
      //BSL IR BNUM050136832 05/04/2012 Rel 8.0 END

       //Srinivasu IR-KIUL121454215 12/28/2011 Start
      String allowPOUpload = inputDoc.getAttribute("/In/CorporateOrganization/allow_purchase_order_upload");
      String poUploadFormat = inputDoc.getAttribute("/In/CorporateOrganization/po_upload_format");
      if (StringFunction.isBlank(allowPOUpload))
      {
         inputDoc.setAttribute("/In/CorporateOrganization/allow_purchase_order_upload", TradePortalConstants.INDICATOR_NO);

      } else if ( TradePortalConstants.INDICATOR_YES.equals(allowPOUpload) ){
     	  if(StringFunction.isBlank(poUploadFormat)){
     		 inputDoc.setAttribute("/In/CorporateOrganization/po_upload_format", null);
     	  }
      }

      //Suresh - Rel7100 - IR# WZUK102660022 - 09/09/2011 - Start
      String standbyLCUseBothFormsIndicator = inputDoc.getAttribute("/In/CorporateOrganization/standbys_use_either_form");
      String standbyDLCUseOnlyGuaranteeIndicator = inputDoc.getAttribute("/In/CorporateOrganization/standbys_use_guar_form_only");
      if (TradePortalConstants.INDICATOR_YES.equals(standbyLCUseBothFormsIndicator) && TradePortalConstants.INDICATOR_YES.equals(standbyDLCUseOnlyGuaranteeIndicator)) {
    	  MediatorServices medService = new MediatorServices();
		  medService.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,TradePortalConstants.SIMPLESLC_OR_SIMPLEDETAILEDSLC);
		  medService.addErrorInfo();
	      inputDoc.setComponent ("/Error", medService.getErrorDoc ());
		  return;
      }
      //Suresh - Rel7100 - IR# WZUK102660022 - 09/09/2011 - End
      
		// DK IR T36000011615 Rel8.2 03/01/2013 BEGINS
		if(TradePortalConstants.INDICATOR_YES.equals(inputDoc.getAttribute("/In/CorporateOrganization/allow_loan_request"))){
			if(TradePortalConstants.INDICATOR_NO.equals(inputDoc.getAttribute("/In/CorporateOrganization/trade_loan_type")) && TradePortalConstants.INDICATOR_NO.equals(inputDoc.getAttribute("/In/CorporateOrganization/import_loan_type")) 
					&& TradePortalConstants.INDICATOR_NO.equals(inputDoc.getAttribute("/In/CorporateOrganization/export_loan_type")) && TradePortalConstants.INDICATOR_NO.equals(inputDoc.getAttribute("/In/CorporateOrganization/invoice_financing_type"))){
				MediatorServices medService = new MediatorServices();
				  medService.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,TradePortalConstants.LOAN_TYPE_REQD);
				  medService.addErrorInfo();
			      inputDoc.setComponent ("/Error", medService.getErrorDoc ());
				  return;
			}
			if(TradePortalConstants.INDICATOR_YES.equals(inputDoc.getAttribute("/In/CorporateOrganization/trade_loan_type")) && (StringFunction.isBlank(inputDoc.getAttribute("/In/CorporateOrganization/days_before_payment_date")) ||
					StringFunction.isBlank(inputDoc.getAttribute("/In/CorporateOrganization/days_after_payment_date")) || StringFunction.isBlank(inputDoc.getAttribute("/In/CorporateOrganization/fin_days_before_loan_maturity")) ||
					StringFunction.isBlank(inputDoc.getAttribute("/In/CorporateOrganization/inv_percent_to_fin_trade_loan")))){
				MediatorServices medService = new MediatorServices();
				  medService.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,TradePortalConstants.TRADE_LOAN_RULES_REQD);
				  medService.addErrorInfo();
			      inputDoc.setComponent ("/Error", medService.getErrorDoc ());
				  return;
			}			
		}
	
		if(!StringFunction.isBlank(inputDoc.getAttribute("/In/CorporateOrganization/inv_percent_to_fin_trade_loan")) && 
				(Double.parseDouble(inputDoc.getAttribute("/In/CorporateOrganization/inv_percent_to_fin_trade_loan")) > 100.00)){
			MediatorServices medService = new MediatorServices();
			  medService.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
					   TradePortalConstants.MUST_BE_LESS_THAN_EQUAL_TO,
					   resMgr.getText( "CorpCust.PercentageInvoiceToBeFinanced",TradePortalConstants.TEXT_BUNDLE), "100.00",
                                    TradePortalConstants.TEXT_BUNDLE);
			  medService.addErrorInfo();
		      inputDoc.setComponent ("/Error", medService.getErrorDoc ());
			  return;
		}

		String [] userReportCategories = {"first_rept_categ", "second_rept_categ",
				"third_rept_categ",	"fourth_rept_categ",
				"fifth_rept_categ",	"sixth_rept_categ",
				"seventh_rept_categ","eighth_rept_categ",
				"nineth_rept_categ","tenth_rept_categ"};
		
		Vector customCategoryVector = inputDoc.getFragments("/In/CorporateOrganization/");
 		DocumentHandler customCategoryDoc = (DocumentHandler) customCategoryVector.elementAt(0);
 		
		List customCategory = new ArrayList();
   	// Check the categories.
     	for (int index = 0; index < 10; index++) {
     		String category = customCategoryDoc.getAttribute("/"+userReportCategories[index]);
     		if (category != null && category.length() > 0) {
     			customCategory.add(category);  
     		}
     	}
     	
     	String organizationOid = customCategoryDoc.getAttribute("/organization_oid");
     	if(isReportCustomCategoryReferncedByUser(organizationOid, customCategory)){
     		MediatorServices medService = new MediatorServices();
			medService.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,TradePortalConstants.REPORT_CAT_IN_USE);
			medService.addErrorInfo();
			inputDoc.setComponent ("/Error", medService.getErrorDoc ());
			return;
     	}
   
     	
     	
      //Maheswar CR-610 Begin
      //This steps loops over the request parameters
      //finds the otherAccountCheckbox fields and sets it to the
      //inputDoc which is later on parsed to the mediator to identify 
      //the accounts which is cheked.
      //This step we are putting values from request to inputDoc because
      //Jpylon doesnot support adding the Ajax called jsp to the parent jsp.
      String index = null;
       StringBuilder otherAccountOids = new StringBuilder();
      Map reqfieldMap = request.getParameterMap();
		for (Object o : reqfieldMap.keySet()) {

			String entry = (String) o;

			if (entry.startsWith(TradePortalConstants.OTHER_ACCOUNT_CHECKBOX)) {

				index = entry.substring(TradePortalConstants.OTHER_ACCOUNT_CHECKBOX.length(), entry.length());
				String acctOidKey = TradePortalConstants.OTHER_ACCOUNT_OID + index;
				if (StringFunction.isNotBlank(request.getParameter(acctOidKey))) {
					otherAccountOids.append(request.getParameter(acctOidKey));
					otherAccountOids.append(",");
				}
			}

		}
    // Determine whether accounts are deleted or not entered from
   	Vector addressVector = inputDoc.getFragments("/In/CorporateOrganization/AddressList");
    int numAddressItems = addressVector.size();
   	int addr_iLoop;
   	DocumentNode node = null;
   	String id = "";
    Map addrDeleteMap = new HashMap();
  	for (addr_iLoop=0; addr_iLoop<numAddressItems; addr_iLoop++) {
  		DocumentHandler addressDoc = (DocumentHandler) addressVector.elementAt(addr_iLoop);
  		String addr_oid = addressDoc.getAttribute("/address_oid");
  		String addr_name = addressDoc.getAttribute("/name");
        String addr_line_1 = addressDoc.getAttribute("/address_line_1");
        String addr_line_2 = addressDoc.getAttribute("/address_line_2");
        String addr_city = addressDoc.getAttribute("/city");
        String addr_state = addressDoc.getAttribute("/state_province");
        String addr_postal = addressDoc.getAttribute("/postal_code");
        String addr_seq_num = addressDoc.getAttribute("/address_seq_num");

        //cquinton 11/13/2012 - delete is only active when all fields other than oid 
        // are blank.  otherwise we want to return an error and fields are missing...
  		if (StringFunction.isBlank(addr_name) && StringFunction.isBlank(addr_line_1) &&
  				StringFunction.isBlank(addr_line_2) && StringFunction.isBlank(addr_city) &&
  				StringFunction.isBlank(addr_state) && StringFunction.isBlank(addr_postal) &&
  				StringFunction.isBlank(addr_seq_num)) {
  			if (StringFunction.isBlank(addr_oid)) {
  				// Oid is also blank: remove this completely blank component
  				// from the input doc.
  				// Chandrakanth IR AAUI120245963 12/05/2008 Begin
  				node = addressDoc.getDocumentNode ("/");
  				if(!StringFunction.isBlank(node.getIdName())) {
  					id = node.getIdName();
  				}
  				inputDoc.removeComponent("/In/CorporateOrganization/AddressList("+id+")");
  				// Chandrakanth IR AAUI120245963 12/05/2008 End.
  			} else {
  				// Oid is not blank but everything else is.  The user wants to delete this
  				// account.  Remove it from the input doc (so it doesn't get save and generate
  				// an error), then add the oid to a new section for processing in the
  				// mediator.

  				DocumentHandler deleteDoc = new DocumentHandler();
  				deleteDoc.setAttribute("/Name", "Address");
  				deleteDoc.setAttribute("/Oid", addr_oid);
  				addrDeleteMap.put(addr_iLoop+"",deleteDoc);
  	
  			}
  		}
  	}
  	addr_iLoop = 0;
		for (Object o : addrDeleteMap.keySet()) {

			String idx = (String) o;

			//jgadela -  Added the code to delete the account from Input Document account list .
			for (int jLoop = 0; jLoop < numAddressItems; jLoop++) {
				DocumentHandler acctDoc = (DocumentHandler) addressVector.elementAt(jLoop);
				DocumentHandler delDoc = (DocumentHandler) addrDeleteMap.get(idx);

				String delAcctOid = delDoc.getAttribute("/Oid");
				String acctOid = acctDoc.getAttribute("/address_oid");
				if (delAcctOid.equalsIgnoreCase(acctOid)) {
					inputDoc.removeComponent("/In/CorporateOrganization/AddressList(" + jLoop + ")");
				}
			}
		

			inputDoc.addComponent("/In/ComponentToDelete(" + addr_iLoop + ")", (DocumentHandler) addrDeleteMap.get(idx));
			addr_iLoop++;
		}
 
      
	
	// Determine whether accounts are deleted or not entered from
	Vector acctVector = inputDoc.getFragments("/In/CorporateOrganization/AccountList");
	int numItems = acctVector.size();
	int iLoop;
	DocumentNode acctNode = null;
	String acctId = "";
    Map acctDeleteMap = new HashMap();
    MediatorServices medServiceForAccounts = new MediatorServices();
	for (iLoop=0; iLoop<numItems; iLoop++)
	{
		DocumentHandler acctDoc = (DocumentHandler) acctVector.elementAt(iLoop);

		String acctOid = acctDoc.getAttribute("/account_oid");
		String acctNum = acctDoc.getAttribute("/account_number");
		String acctName = acctDoc.getAttribute("/account_name");
        String acctCurr = acctDoc.getAttribute("/currency");
        String acctType = acctDoc.getAttribute("/account_type");
        String acctBranch = acctDoc.getAttribute("/source_system_branch");

		if (StringFunction.isBlank(acctNum) && StringFunction.isBlank(acctName) &&
				StringFunction.isBlank(acctCurr) && StringFunction.isBlank(acctType) &&
				StringFunction.isBlank(acctBranch)) {
		        
			if (StringFunction.isBlank(acctOid)) {
				// main fields are all blank: remove this completely blank component
				// from the input doc.
				// Chandrakanth IR AAUI120245963 12/05/2008 Begin
				acctNode = acctDoc.getDocumentNode ("/");
				if(StringFunction.isNotBlank(acctNode.getIdName())) {
					acctId = acctNode.getIdName();
				}
				inputDoc.removeComponent("/In/CorporateOrganization/AccountList("+acctId+")");
				// Chandrakanth IR AAUI120245963 12/05/2008 End.
			} else {
				// Oid is not blank but other main filds are.  The user wants to delete this
				// account.  Remove it from the input doc (so it doesn't get save and generate
				// an error), then add the oid to a new section for processing in the
				// mediator.

				DocumentHandler deleteDoc = new DocumentHandler();
				deleteDoc.setAttribute("/Name", "Account");
				deleteDoc.setAttribute("/Oid", acctOid);
				acctDeleteMap.put(iLoop+"",deleteDoc);
	
			}
		} else {
			inputDoc.setAttribute("/In/CorporateOrganization/AccountList("+iLoop+")/owner_object_type", TradePortalConstants.CORPORATE_ORG);
			Map panelGroupMap = new HashMap();
			String value = "";
			acctNode = acctDoc.getDocumentNode ("/");
			if(StringFunction.isNotBlank(acctNode.getIdName())) {
				acctId = acctNode.getIdName();
			}
			
			int mapEntry = 0;
			for(int i=1;i<=16;i++){
				value = acctDoc.getAttribute("/panel_auth_group_oid_"+i);
				if(value != null && !value.equals("")){
					panelGroupMap.put(mapEntry+"", value);
					mapEntry++;
					inputDoc.setAttribute("/In/CorporateOrganization/AccountList("+acctId+")/panel_auth_group_oid_"+i, "");
				}
			}
			Iterator it1 = panelGroupMap.keySet().iterator();
			int count = 0;
			while (it1.hasNext()) {
			  String idx = (String)it1.next();
			  inputDoc.setAttribute("/In/CorporateOrganization/AccountList("+acctId+")/panel_auth_group_oid_"+((new Integer(idx)).intValue()+1), panelGroupMap.get(idx).toString());
		  	  count++;
			}
			
			for(int i=1; i<=count; i++) {
				String panelGroupOid = inputDoc.getAttribute("/In/CorporateOrganization/AccountList("+acctId+")/panel_auth_group_oid_"+i);
				for(int j=i+1;j<=count;j++){
					String panelGroupOidNext = inputDoc.getAttribute("/In/CorporateOrganization/AccountList("+acctId+")/panel_auth_group_oid_"+j);
					if(panelGroupOid.equals(panelGroupOidNext)){
						medServiceForAccounts.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,TradePortalConstants.DUPLICATE_PANEL_GROUPS, acctDoc.getAttribute("/account_number"), null);
					
					}
				}
			}
			List commonPmtMethodTypesList = new ArrayList();
			try{
				commonPmtMethodTypesList = validateMultiplePanelGroupsforPayments(acctDoc);
			}catch(Exception e){
				
			}
			
			for(int p=0;p<commonPmtMethodTypesList.size();p++){
				
				medServiceForAccounts.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,TradePortalConstants.MULTIPLE_PAYMENT_METHODS_ASSOC_TO_PANEL_GROUPS, commonPmtMethodTypesList.get(p).toString(), acctDoc.getAttribute("/account_number"));
			}
			
			
		}
	}
	if(medServiceForAccounts.getErrorManager().getErrorCount()>0){
		medServiceForAccounts.addErrorInfo();
	  	inputDoc.setComponent ("/Error", medServiceForAccounts.getErrorDoc ());
	  	return;
	}
	
	iLoop = 0;
	if (isAccountsReferncedByUser(acctDeleteMap)) {
		MediatorServices medService = new MediatorServices();
		medService.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,TradePortalConstants.ACTIVE_ACCOUNT);
		medService.addErrorInfo();
		inputDoc.setComponent ("/Error", medService.getErrorDoc ());
		return;
	}
	else {
		for (Object o : acctDeleteMap.keySet()) {
			String idx = (String) o;
			//jgadela - Rel8.3 ANZCR501 03/28/2013 START  - Added the code to delete the account from Input Document account list .
			for (int jLoop = 0; jLoop < numItems; jLoop++) {
				DocumentHandler acctDoc = (DocumentHandler) acctVector.elementAt(jLoop);
				DocumentHandler delDoc = (DocumentHandler) acctDeleteMap.get(idx);

				String delAcctOid = delDoc.getAttribute("/Oid");
				String acctOid = acctDoc.getAttribute("/account_oid");
				if (delAcctOid.equalsIgnoreCase(acctOid)) {
					inputDoc.removeComponent("/In/CorporateOrganization/AccountList(" + jLoop + ")");
				}
			}

			inputDoc.addComponent("/In/ComponentToDelete(" + iLoop + ")", (DocumentHandler) acctDeleteMap.get(idx));
			iLoop++;
		}
	}
	   


    	// customer can have multiple customer margin rule list, so checking for each customer margin list to validate amount.
        Vector customerMarginRuleVector = inputDoc.getFragments("/In/CorporateOrganization/CustomerMarginRuleList");
    	int totalCustomerMargin = customerMarginRuleVector.size();
    	DocumentHandler customerMarginCurrentNode = null;
    	String thresholdAmount = null;
    	for (int currentCustomerMargin = 0; currentCustomerMargin < totalCustomerMargin; currentCustomerMargin++) {
    		customerMarginCurrentNode = (DocumentHandler) customerMarginRuleVector.elementAt(currentCustomerMargin);
    		thresholdAmount = customerMarginCurrentNode.getAttribute("/threshold_amt");
    		if(StringFunction.isNotBlank(thresholdAmount)){
    			doDecimalPreMediatorAction(inputDoc, resMgr, "/In/CorporateOrganization/CustomerMarginRuleList(" + currentCustomerMargin + ")/threshold_amt");
    		}
    	}
    	
    	/*Kyriba CR 268 - External Bank 
    	 * Check if record contains corporate oid, 
    	 * if not delete that record from document handler.*/
    	
    	Vector externalBankVector = inputDoc.getFragments("/In/CorporateOrganization/ExternalBankList");
    	int numExtBank = externalBankVector.size();
    	LOG.debug("External Bank Size Before Remove : "+numExtBank);
		
    	for( int i=0; i<numExtBank; i++ ){
    		DocumentHandler extBankDoc = (DocumentHandler) externalBankVector.elementAt(i);
    		
    		if( (null == extBankDoc.getAttribute("/op_bank_org_oid")) 
    				 || StringFunction.isBlank(extBankDoc.getAttribute("/op_bank_org_oid")) ){
    			inputDoc.removeComponent("/In/CorporateOrganization/ExternalBankList("+ (i) + ")");
    			
    			if( StringFunction.isNotBlank(extBankDoc.getAttribute("/external_bank_oid")) ){
	    			DocumentHandler delExtBankDoc = new DocumentHandler();
	    			delExtBankDoc.setAttribute("/Name", "ExternalBank");
	    			delExtBankDoc.setAttribute("/Oid", extBankDoc.getAttribute("/external_bank_oid"));
					inputDoc.addComponent("/In/ComponentToDelete("+i+")", delExtBankDoc);
    			}
    		}
    	}
    	//Kyriba End
       
        // Narayan IR-NNUM031454220 04/08/2012 End
    	// Chandrakanth IR PHUI120660503 12/12/2008 End
		// DK CR-587 Rel7.1 BEGINS
		// Determine whether alias are deleted or not entered from
		Vector aliasVector = inputDoc
				.getFragments("/In/CorporateOrganization/CustomerAliasList");
		int numAlias = aliasVector.size();

		int aliasLoop;
		DocumentNode aliasDocNode = null;
		String aliasId = "";
		Map aliasDeleteMap = new HashMap();
		for (aliasLoop = 0; aliasLoop < numAlias; aliasLoop++) {
			DocumentHandler aliasDoc = (DocumentHandler) aliasVector
					.elementAt(aliasLoop);
			String alias = aliasDoc.getAttribute("/alias");
			String msgCat = aliasDoc.getAttribute("/gen_message_category_oid");
			String custAliasOid = aliasDoc.getAttribute("/customer_alias_oid");

			if (StringFunction.isBlank(alias)
					&& StringFunction.isBlank(msgCat)) {
				// Oid, alias, and msgCat are blank: remove this completely
				// blank component
				// from the input doc.
				if (StringFunction.isBlank(custAliasOid)) {
					aliasDocNode = aliasDoc.getDocumentNode("/");
					inputDoc.removeComponent("/In/CorporateOrganization/CustomerAliasList("
							+ (aliasLoop) + ")");
				} else {
					// Oid is not blank but the alias and msgCat are. The user
					// wants to delete this
					// alias. Remove it from the input doc (so it doesn't get
					// save and generate
					// an error), then add the oid to a new section for
					// processing in the
					// mediator.
					DocumentHandler deleteAliasDoc = new DocumentHandler();
					deleteAliasDoc.setAttribute("/Name", "CustomerAlias");
					deleteAliasDoc.setAttribute("/Oid", custAliasOid);
					aliasDeleteMap.put(aliasLoop+"", deleteAliasDoc);
					// DK IR DOUL081847934 Rel 7.1
					inputDoc.removeComponent("/In/CorporateOrganization/CustomerAliasList("+aliasLoop+")");
					inputDoc.addComponent("/In/ComponentToDelete("+aliasLoop+")", deleteAliasDoc);
				}
			}

		}
		// DK CR-587 Rel7.1 ENDS
		// Suresh CR-713 Rel 8.0 Begin
		// Determine whether margin rules are deleted or not entered from
                Vector marginRuleVector = inputDoc.getFragments("/In/CorporateOrganization/CustomerMarginRuleList");
		Map marginDeleteMap = new HashMap();
		for(int i=0; i<marginRuleVector.size(); i++) {
			DocumentHandler marginRuleDoc = (DocumentHandler) marginRuleVector.elementAt(i);
			String customerMarginRuleOid = marginRuleDoc.getAttribute("/customer_margin_rule_oid");
			String curr = marginRuleDoc.getAttribute("/currency");
			String instrumentType = marginRuleDoc.getAttribute("/instrument_type");
			String thresholdAmt = marginRuleDoc.getAttribute("/threshold_amt");
			String rateType = marginRuleDoc.getAttribute("/rate_type");
			String margin = marginRuleDoc.getAttribute("/margin");

			if(StringFunction.isNotBlank(margin)) {
				int length = margin.length();
                String decimalPoint = ".";
                int indexOfDecimalPoint = margin.indexOf(decimalPoint);
                int indexOfFirstChar = indexOfDecimalPoint + 1;

                // If there is a decimal point, check digits to the left and to the righ
                if(indexOfDecimalPoint >= 0)
                 {
                     if((length-indexOfFirstChar) > 6 || indexOfDecimalPoint > 3)
                      {
                    	 MediatorServices medService = new MediatorServices();
							medService.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,TradePortalConstants.MARGIN_INVALID, String.valueOf(i+1), null);
							medService.addErrorInfo();
							inputDoc.setComponent ("/Error", medService.getErrorDoc ());
							return;

                      }
                 }
                else
                 {
                     // Otherwise, only check the length against the digits to the left of the dec pt
                     if(length > 3)
                      {
                    	 MediatorServices medService = new MediatorServices();
							medService.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,TradePortalConstants.MARGIN_INVALID, String.valueOf(i+1), null);
							medService.addErrorInfo();
							inputDoc.setComponent ("/Error", medService.getErrorDoc ());
							return;
                      }
                 }
			}
			if(StringFunction.isBlank(curr) && StringFunction.isBlank(instrumentType) && StringFunction.isBlank(thresholdAmt) && StringFunction.isBlank(rateType) && StringFunction.isBlank(margin)) {
				if (StringFunction.isBlank(customerMarginRuleOid)) {
					DocumentNode marginDocNode = marginRuleDoc.getDocumentNode("/");
                                        String marginId = null;
					if(StringFunction.isNotBlank(marginDocNode.getIdName())) {
						marginId = marginDocNode.getIdName();
					}
					inputDoc.removeComponent("/In/CorporateOrganization/CustomerMarginRuleList("+ (marginId) + ")");
				} else {
					DocumentHandler deleteMarginDoc = new DocumentHandler();
					deleteMarginDoc.setAttribute("/Name", "CustomerMarginRule");
					deleteMarginDoc.setAttribute("/Oid", customerMarginRuleOid);
					marginDeleteMap.put(i+"", deleteMarginDoc);
					inputDoc.removeComponent("/In/CorporateOrganization/CustomerMarginRuleList("+i+")");
					inputDoc.addComponent("/In/ComponentToDelete("+i+")", deleteMarginDoc);
				}
			}	else {
					if(StringFunction.isNotBlank(curr) || StringFunction.isNotBlank(instrumentType) || StringFunction.isNotBlank(thresholdAmt) || StringFunction.isNotBlank(rateType)) {
						if(StringFunction.isBlank(margin)) {
							//Error message
							MediatorServices medService = new MediatorServices();
							medService.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,TradePortalConstants.MARGIN_REQUIRED, String.valueOf(i+1), null);
							medService.addErrorInfo();
							inputDoc.setComponent ("/Error", medService.getErrorDoc ());
							return;
						}
					}
					//IR 18534 - start
					if(StringFunction.isNotBlank(curr) || StringFunction.isNotBlank(instrumentType) || StringFunction.isNotBlank(thresholdAmt) || StringFunction.isNotBlank(margin)) {
						if(StringFunction.isBlank(rateType)) {
							//Error message
							MediatorServices medService = new MediatorServices();
							medService.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,TradePortalConstants.RATE_TYPE_REQUIRED, String.valueOf(i+1), null);
							medService.addErrorInfo();
							inputDoc.setComponent ("/Error", medService.getErrorDoc ());
							return;
						}
					}
					//IR 18534 - end
					if(StringFunction.isNotBlank(thresholdAmt) && StringFunction.isBlank(curr)) {
						//Error Message
						MediatorServices medService = new MediatorServices();
						medService.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,TradePortalConstants.CURRENCY_REQUIRED, String.valueOf(i+1), null);
						medService.addErrorInfo();
						inputDoc.setComponent ("/Error", medService.getErrorDoc ());
						return;
					}
				for(int j=i+1; j<marginRuleVector.size(); j++) {
				DocumentHandler marginRuleDocJ = (DocumentHandler) marginRuleVector.elementAt(j);
				String currJ = marginRuleDocJ.getAttribute("/currency");
				String instrumentTypeJ = marginRuleDocJ.getAttribute("/instrument_type");
				String thresholdAmtJ = marginRuleDocJ.getAttribute("/threshold_amt");
				String rateTypeJ = marginRuleDocJ.getAttribute("/rate_type");
				String marginJ = marginRuleDocJ.getAttribute("/margin");

				if(StringFunction.isNotBlank(currJ) || StringFunction.isNotBlank(instrumentTypeJ) || StringFunction.isNotBlank(thresholdAmtJ) || StringFunction.isNotBlank(rateTypeJ) || StringFunction.isNotBlank(marginJ)) {
					//Srinivasu_D IR# DDUM050366179 Rel8.0 05/05/2012 Start
					thresholdAmtJ = formatThreshold(thresholdAmtJ);
					thresholdAmt =  formatThreshold(thresholdAmt);
					//Srinivasu_D IR# DDUM050366179 Rel8.0 05/05/2012 End
					if(curr.equals(currJ) &&  instrumentType.equals(instrumentTypeJ) && thresholdAmt.equals(thresholdAmtJ)) {
						if(StringFunction.isNotBlank(rateType) || StringFunction.isNotBlank(margin)) {
							if(StringFunction.isNotBlank(rateTypeJ) || StringFunction.isNotBlank(marginJ)){
								//Error Message
								MediatorServices medService = new MediatorServices();
							  	medService.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,TradePortalConstants.DUPLICATE_MARGIN_RULE, String.valueOf(j+1), null);
							  	medService.addErrorInfo();
							  	inputDoc.setComponent ("/Error", medService.getErrorDoc ());
							  	return;
							}
						}
					}
				}
				}
			}
		}
		//Suresh CR-713 Rel 8.0 End
		
                // W Zhu 2/20/13 Release 8.2 CR-708B 
                Vector spMarginRuleVector = inputDoc.getFragments("/In/CorporateOrganization/SPMarginRuleList");
                int totalSPMargin = spMarginRuleVector.size();
                DocumentHandler spMarginCurrentNode = null;
                String spThresholdAmount = null;
                for (int currentSPMargin = 0; currentSPMargin < totalSPMargin; currentSPMargin++) {
                        spMarginCurrentNode = (DocumentHandler) spMarginRuleVector.elementAt(currentSPMargin);
                        spThresholdAmount = spMarginCurrentNode.getAttribute("/threshold_amt");
                        if(StringFunction.isNotBlank(spThresholdAmount)){
                                doDecimalPreMediatorAction(inputDoc, resMgr, "/In/CorporateOrganization/SPMarginRuleList(" + currentSPMargin + ")/threshold_amt");
                        }
                }
                

        		String allowSupPortal = inputDoc.getAttribute("/In/CorporateOrganization/allow_supplier_portal");
        		String spCalDiscountInd = inputDoc.getAttribute("/In/CorporateOrganization/sp_calculate_discount_ind");
        		

        		if(allowSupPortal.equalsIgnoreCase("Y") && StringFunction.isBlank(spCalDiscountInd)){
        			MediatorServices medService = new MediatorServices();
        		  	medService.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,TradePortalConstants.MISSING_SP_DIS_CALC_METHOD);
        		  	medService.addErrorInfo();
        		  	inputDoc.setComponent ("/Error", medService.getErrorDoc ());
        		  	return;
        		}
     
        		try{
        			String interestDiscRateGroup = inputDoc.getAttribute("/In/CorporateOrganization/interest_disc_rate_group");
        			DocumentHandler interestDiscRateGroupDoc = null;
   				 	
        			if(StringFunction.isNotBlank(interestDiscRateGroup)){

	   				 	String sqlQuery  = "select group_id from interest_discount_rate_group where group_id= ?";
	   					interestDiscRateGroupDoc  = DatabaseQueryBean.getXmlResultSet(sqlQuery, false, new Object[]{interestDiscRateGroup});
	   					
	   					if (interestDiscRateGroupDoc == null)
	   					{
	   						MediatorServices medService = new MediatorServices();
	   	        		  	medService.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,TradePortalConstants.INVALID_INTEREST_DISCOUNT_RATE_GROUP,
	   	        		  									String.valueOf(interestDiscRateGroup), null);
	   	        		  	medService.addErrorInfo();
	   	        		  	inputDoc.setComponent ("/Error", medService.getErrorDoc ());
	   	        		  	return;
	   					}
        			}
   			 	}catch(Exception e ){
   			 		e.printStackTrace();
   			 	}

   			 	
   			MediatorServices medServiceObj = new MediatorServices();
            String rowContents=null;
			Set<String> spMarginContents = new HashSet<String>();
			
		for(int i=0; i<totalSPMargin; i++) {
			DocumentHandler marginRuleDoc = (DocumentHandler) spMarginRuleVector.elementAt(i);
			String spMarginRuleOid = marginRuleDoc.getAttribute("/sp_margin_rule_oid");
			String curr = marginRuleDoc.getAttribute("/currency");
			String thresholdAmt = marginRuleDoc.getAttribute("/threshold_amt");
			String rateType = marginRuleDoc.getAttribute("/rate_type");
                        if ("FXD".equals(rateType)) marginRuleDoc.setAttribute("/margin", "0");
			String margin = marginRuleDoc.getAttribute("/margin");
			
			if(StringFunction.isNotBlank(margin)) {
				int length = margin.length();
                String decimalPoint = ".";
                int indexOfDecimalPoint = margin.indexOf(decimalPoint);
                int indexOfFirstChar = indexOfDecimalPoint + 1;

                // If there is a decimal point, check digits to the left and to the righ
                if(indexOfDecimalPoint >= 0)
                 {
                     if((length-indexOfFirstChar) > 6 || indexOfDecimalPoint > 3)
                      {
                    	 MediatorServices medService = new MediatorServices();
			 medService.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,TradePortalConstants.SP_MARGIN_INVALID, String.valueOf(i+1), null);
			medService.addErrorInfo();
			inputDoc.setComponent ("/Error", medService.getErrorDoc ());
			return;

                      }
                 }
                else
                 {
                     // Otherwise, only check the length against the digits to the left of the dec pt
                     if(length > 3)
                      {
                    	 MediatorServices medService = new MediatorServices();
			medService.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.SP_MARGIN_INVALID, String.valueOf(i+1), null);
			medService.addErrorInfo();
			inputDoc.setComponent ("/Error", medService.getErrorDoc ());
			return;
                      }
                 }
			}
			
			if(StringFunction.isBlank(curr) 
					&& StringFunction.isBlank(thresholdAmt) 
					&& StringFunction.isBlank(rateType) 
					&& StringFunction.isBlank(margin))
			{
				if (StringFunction.isBlank(spMarginRuleOid)) {
					DocumentNode marginDocNode = marginRuleDoc.getDocumentNode("/");
                                        String marginId = null;
					if(marginDocNode!=null && !StringFunction.isBlank(marginDocNode.getIdName())) {
						marginId = marginDocNode.getIdName();
					}
					inputDoc.removeComponent("/In/CorporateOrganization/SPMarginRuleList("+ (marginId) + ")");
				} else {
					DocumentHandler deleteMarginDoc = new DocumentHandler();
					deleteMarginDoc.setAttribute("/Name", "SPMarginRule");
					deleteMarginDoc.setAttribute("/Oid", spMarginRuleOid);
					marginDeleteMap.put(i+"", deleteMarginDoc);
					inputDoc.removeComponent("/In/CorporateOrganization/SPMarginRuleList("+i+")");
					inputDoc.addComponent("/In/ComponentToDelete("+i+")", deleteMarginDoc);
				}
			}else {

				rowContents = curr +"|"+thresholdAmt+"|";//+rateType+"|"+margin+"|";

				if (spMarginContents.contains(rowContents)){
					medServiceObj.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
										TradePortalConstants.DUPLICATE_SP_MARGIN_RULE, String.valueOf(i+1), null);
				}else{
					spMarginContents.add(rowContents);
				}
	
					if(
						(StringFunction.isNotBlank(curr) 
						|| StringFunction.isNotBlank(thresholdAmt) 
						|| StringFunction.isNotBlank(rateType))
						&& StringFunction.isBlank(margin) 
					) {
							//Error message
							medServiceObj.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
									TradePortalConstants.SP_MARGIN_REQUIRED, String.valueOf(i+1), null);
					}
					//IR 18534 - start
					if(
						(StringFunction.isNotBlank(curr) 
						|| StringFunction.isNotBlank(thresholdAmt) 
						|| StringFunction.isNotBlank(margin))
						&& StringFunction.isBlank(rateType) 
					) {
								//Error message
								medServiceObj.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
										TradePortalConstants.SP_RATE_TYPE_REQUIRED, String.valueOf(i+1), null);
						}
					//IR 18534 - end
					if(StringFunction.isNotBlank(thresholdAmt) && StringFunction.isBlank(curr)) {
						//Error Message
						medServiceObj.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
								TradePortalConstants.SP_CURRENCY_REQUIRED, String.valueOf(i+1), null);
					}
					
					if (StringFunction.isNotBlank(thresholdAmt))
	                {
						StringBuilder alias = new StringBuilder() ;
						alias.append(resMgr.getText("Cust.InvalidThresholdAmt", TradePortalConstants.TEXT_BUNDLE));
						alias.append(" '"+(i+1)+"' ");
				        // Validate that value will fit in database and that currency precision is correct
					        TPCurrencyUtility.validateAmount(curr, thresholdAmt, alias.toString(), medServiceObj.getErrorManager() );
	                }

			}
		}  
		
		medServiceObj.addErrorInfo();
		inputDoc.setComponent ("/Error", medServiceObj.getErrorDoc ());
		

		if(allowSupPortal.equalsIgnoreCase("Y") && inputDoc.getFragments("/In/CorporateOrganization/SPMarginRuleList").size()==0){
			MediatorServices medService = new MediatorServices();
		  	medService.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,TradePortalConstants.MISSING_SP_MARGIN_RULE);
		  	medService.addErrorInfo();
		  	inputDoc.setComponent ("/Error", medService.getErrorDoc ());
		  	return;
		}

		
		//AAlubala - IR#MIUM050239929 - Non-alphanumeric characters should NOT be allowed in the Interest Discount rate group field

		String interestDiscRageGroup = inputDoc.getAttribute("/In/CorporateOrganization/interest_disc_rate_group");
		if(StringFunction.isNotBlank(interestDiscRageGroup)){
			if(!interestDiscRageGroup.trim().matches("[a-zA-Z0-9]*")){
				//Error message displayed
				MediatorServices medService = new MediatorServices();
				medService.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,TradePortalConstants.ERROR_NONALPHANUMERIC_INT_GRP_RATE);
				medService.addErrorInfo();
				inputDoc.setComponent ("/Error", medService.getErrorDoc ());
				return;
			}
		}

	  
	}
    
    
   /**
	* This method is used for initializing several corporate org default
	* settings, in the event that they were not set by the user at all.
	*
	* @param       inputDoc   DocumentHandler - the current input xml doc
	* @return      DocumentHandler - the current input xml doc with initialized attributes
	* @exception   com.amsinc.ecsg.frame.AmsException
	*/
   private DocumentHandler initializeDefaultSettings(DocumentHandler inputDoc) throws AmsException
   {
      if (inputDoc.getAttribute("/In/CorporateOrganization/auth_user_different_ind") == null)
      {
         inputDoc.setAttribute("/In/CorporateOrganization/auth_user_different_ind", TradePortalConstants.INDICATOR_NO);
      }

      if (inputDoc.getAttribute("/In/CorporateOrganization/allow_create_bank_parties") == null)
      {
         inputDoc.setAttribute("/In/CorporateOrganization/allow_create_bank_parties", TradePortalConstants.INDICATOR_NO);
      }

 	  if (inputDoc.getAttribute("/In/CorporateOrganization/force_certs_for_user_maint") == null)
	  {
	     inputDoc.setAttribute("/In/CorporateOrganization/force_certs_for_user_maint", TradePortalConstants.INDICATOR_NO);
	  }

	  if (inputDoc.getAttribute("/In/CorporateOrganization/timeout_auto_save_ind") == null)
	  {
	     inputDoc.setAttribute("/In/CorporateOrganization/timeout_auto_save_ind", TradePortalConstants.INDICATOR_NO);
	  }

	  if (inputDoc.getAttribute("/In/CorporateOrganization/doc_prep_ind") == null)
	  {
	     inputDoc.setAttribute("/In/CorporateOrganization/doc_prep_ind", TradePortalConstants.INDICATOR_NO);
	  }

	  if (inputDoc.getAttribute("/In/CorporateOrganization/erp_transaction_rpt_reqd") == null)
	  {
	     inputDoc.setAttribute("/In/CorporateOrganization/erp_transaction_rpt_reqd", TradePortalConstants.INDICATOR_NO);
	  }	  
	  

	  if (inputDoc.getAttribute("/In/CorporateOrganization/air_checker_auth_ind") == null)
	  {

		  inputDoc.setAttribute("/In/CorporateOrganization/air_checker_auth_ind", TradePortalConstants.INDICATOR_NO);
	  }
	  
	  if (inputDoc.getAttribute("/In/CorporateOrganization/atp_checker_auth_ind") == null)
	  {
	     inputDoc.setAttribute("/In/CorporateOrganization/atp_checker_auth_ind", TradePortalConstants.INDICATOR_NO);
	  }
	  
	  if (inputDoc.getAttribute("/In/CorporateOrganization/exp_col_checker_auth_ind") == null)
	  {
	     inputDoc.setAttribute("/In/CorporateOrganization/exp_col_checker_auth_ind", TradePortalConstants.INDICATOR_NO);
	  }
	  
	  if (inputDoc.getAttribute("/In/CorporateOrganization/exp_dlc_checker_auth_ind") == null)
	  {
	     inputDoc.setAttribute("/In/CorporateOrganization/exp_dlc_checker_auth_ind", TradePortalConstants.INDICATOR_NO);
	  }
	  
	  if (inputDoc.getAttribute("/In/CorporateOrganization/exp_oco_checker_auth_ind") == null)
	  {
	     inputDoc.setAttribute("/In/CorporateOrganization/exp_oco_checker_auth_ind", TradePortalConstants.INDICATOR_NO);
	  }
	  //SSikhakolli - Rel-9.4 CR-818
	  if (inputDoc.getAttribute("/In/CorporateOrganization/imp_col_checker_auth_ind") == null)
	  {
	     inputDoc.setAttribute("/In/CorporateOrganization/imp_col_checker_auth_ind", TradePortalConstants.INDICATOR_NO);
	  }
	  
	  if (inputDoc.getAttribute("/In/CorporateOrganization/imp_dlc_checker_auth_ind") == null)
	  {
	     inputDoc.setAttribute("/In/CorporateOrganization/imp_dlc_checker_auth_ind", TradePortalConstants.INDICATOR_NO);
	  }
	  
	  if (inputDoc.getAttribute("/In/CorporateOrganization/lrq_checker_auth_ind") == null)
	  {
	     inputDoc.setAttribute("/In/CorporateOrganization/lrq_checker_auth_ind", TradePortalConstants.INDICATOR_NO);
	  }

	  if (inputDoc.getAttribute("/In/CorporateOrganization/gua_checker_auth_ind") == null)
	  {
	     inputDoc.setAttribute("/In/CorporateOrganization/gua_checker_auth_ind", TradePortalConstants.INDICATOR_NO);
	  }
	  
	  if (inputDoc.getAttribute("/In/CorporateOrganization/slc_checker_auth_ind") == null)
	  {
	     inputDoc.setAttribute("/In/CorporateOrganization/slc_checker_auth_ind", TradePortalConstants.INDICATOR_NO);
	  }
	  
	  if (inputDoc.getAttribute("/In/CorporateOrganization/shp_checker_auth_ind") == null)
	  {
	     inputDoc.setAttribute("/In/CorporateOrganization/shp_checker_auth_ind", TradePortalConstants.INDICATOR_NO);
	  }
	  
	  if (inputDoc.getAttribute("/In/CorporateOrganization/rqa_checker_auth_ind") == null)
	  {
	     inputDoc.setAttribute("/In/CorporateOrganization/rqa_checker_auth_ind", TradePortalConstants.INDICATOR_NO);
	  }
	  
	  if (inputDoc.getAttribute("/In/CorporateOrganization/sp_checker_auth_ind") == null)
	  {
	     inputDoc.setAttribute("/In/CorporateOrganization/sp_checker_auth_ind", TradePortalConstants.INDICATOR_NO);
	  }
	  
	  if (inputDoc.getAttribute("/In/CorporateOrganization/dcr_checker_auth_ind") == null)
	  {
	     inputDoc.setAttribute("/In/CorporateOrganization/dcr_checker_auth_ind", TradePortalConstants.INDICATOR_NO);
	  }
	  //SSikhakolli - Rel-9.4 CR-818
	  if (inputDoc.getAttribute("/In/CorporateOrganization/include_settle_instruction") == null)
	  {
	     inputDoc.setAttribute("/In/CorporateOrganization/include_settle_instruction", TradePortalConstants.INDICATOR_NO);
	  }
	  
	  if (inputDoc.getAttribute("/In/CorporateOrganization/sit_checker_auth_ind") == null)
	  {
	     inputDoc.setAttribute("/In/CorporateOrganization/sit_checker_auth_ind", TradePortalConstants.INDICATOR_NO);
	  }
	  
	  if (inputDoc.getAttribute("/In/CorporateOrganization/ftrq_checker_auth_ind") == null)
	  {
	     inputDoc.setAttribute("/In/CorporateOrganization/ftrq_checker_auth_ind", TradePortalConstants.INDICATOR_NO);
	  }
	  
	  if (inputDoc.getAttribute("/In/CorporateOrganization/ftdp_checker_auth_ind") == null)
	  {
	     inputDoc.setAttribute("/In/CorporateOrganization/ftdp_checker_auth_ind", TradePortalConstants.INDICATOR_NO);
	  }
	  
	  if (inputDoc.getAttribute("/In/CorporateOrganization/ftba_checker_auth_ind") == null)
	  {
	     inputDoc.setAttribute("/In/CorporateOrganization/ftba_checker_auth_ind", TradePortalConstants.INDICATOR_NO);
	  }
	  
	  if (inputDoc.getAttribute("/In/CorporateOrganization/mtch_apprv_checker_auth_ind") == null)
	  {
	     inputDoc.setAttribute("/In/CorporateOrganization/mtch_apprv_checker_auth_ind", TradePortalConstants.INDICATOR_NO);
	  }
	  
	  if (inputDoc.getAttribute("/In/CorporateOrganization/inv_auth_checker_auth_ind") == null)
	  {
	     inputDoc.setAttribute("/In/CorporateOrganization/inv_auth_checker_auth_ind", TradePortalConstants.INDICATOR_NO);
	  }
	  
	  if (inputDoc.getAttribute("/In/CorporateOrganization/credit_auth_checker_auth_ind") == null)
	  {
	     inputDoc.setAttribute("/In/CorporateOrganization/credit_auth_checker_auth_ind", TradePortalConstants.INDICATOR_NO);
	  }
  
	  if (inputDoc.getAttribute("/In/CorporateOrganization/pmt_bene_panel_auth_ind") == null)
	  {
	     inputDoc.setAttribute("/In/CorporateOrganization/pmt_bene_panel_auth_ind", TradePortalConstants.INDICATOR_NO);
	  }

	  if (inputDoc.getAttribute("/In/CorporateOrganization/invoice_file_upload_pay_ind") == null)
	  {
	     inputDoc.setAttribute("/In/CorporateOrganization/invoice_file_upload_pay_ind", TradePortalConstants.INDICATOR_NO);
	  }
	  if (inputDoc.getAttribute("/In/CorporateOrganization/invoice_file_upload_rec_ind") == null)
	  {
	     inputDoc.setAttribute("/In/CorporateOrganization/invoice_file_upload_rec_ind", TradePortalConstants.INDICATOR_NO);
	  }
	  if (inputDoc.getAttribute("/In/CorporateOrganization/pay_invoice_grouping_ind") == null)
	  {
	     inputDoc.setAttribute("/In/CorporateOrganization/pay_invoice_grouping_ind", TradePortalConstants.INDICATOR_NO);
	  }
	  if (inputDoc.getAttribute("/In/CorporateOrganization/pay_invoice_utilised_ind") == null)
	  {
	     inputDoc.setAttribute("/In/CorporateOrganization/pay_invoice_utilised_ind", TradePortalConstants.INDICATOR_NO);
	  }
	  if (inputDoc.getAttribute("/In/CorporateOrganization/rec_invoice_utilised_ind") == null)
	  {
	     inputDoc.setAttribute("/In/CorporateOrganization/rec_invoice_utilised_ind", TradePortalConstants.INDICATOR_NO);
	  }
	  if (inputDoc.getAttribute("/In/CorporateOrganization/pre_debit_funding_option_ind") == null)
	  {
	     inputDoc.setAttribute("/In/CorporateOrganization/pre_debit_funding_option_ind", TradePortalConstants.INDICATOR_NO);
	  }
	  if (inputDoc.getAttribute("/In/CorporateOrganization/mobile_banking_access_ind") == null)
	  {
	     inputDoc.setAttribute("/In/CorporateOrganization/mobile_banking_access_ind", TradePortalConstants.INDICATOR_NO);
	  }
	  if (inputDoc.getAttribute("/In/CorporateOrganization/allow_h2h_rec_inv_approval") == null)
	  {
		     inputDoc.setAttribute("/In/CorporateOrganization/allow_h2h_rec_inv_approval", TradePortalConstants.INDICATOR_NO);
		  }
	  if (inputDoc.getAttribute("/In/CorporateOrganization/allow_h2h_pay_inv_approval") == null)
	  {
		     inputDoc.setAttribute("/In/CorporateOrganization/allow_h2h_pay_inv_approval", TradePortalConstants.INDICATOR_NO);
		  }
	  if (inputDoc.getAttribute("/In/CorporateOrganization/allow_h2h_pay_crn_approval") == null)
	  {
		     inputDoc.setAttribute("/In/CorporateOrganization/allow_h2h_pay_crn_approval", TradePortalConstants.INDICATOR_NO);
		  }

                //SSikhakolli Rel 9.3.5 CR-1029 - Begin
	  if (inputDoc.getAttribute("/In/CorporateOrganization/cust_is_not_intg_tps") == null)
	  {
	     inputDoc.setAttribute("/In/CorporateOrganization/cust_is_not_intg_tps", TradePortalConstants.INDICATOR_NO);
	  }
	//SSikhakolli Rel 9.3.5 CR-1029 - End

 return inputDoc;
   }

   /**
	* This method is used for initializing the corporate organization's
	* instrument capabilities and authorization attributes. Specifically, it
	* sets the Import Letters of Credit, Standby Letters of Credit, Export
	* Letters of Credit, Export Collection, Guarantee, Airway Bill Release, and
	* Shipping Guarantee indicators to 'N' if the user did not check the
	* corresponding check boxes.
	*
	* @param       inputDoc   DocumentHandler - the current mediator's input xml doc
	* @return      DocumentHandler - the current input xml doc with initialized attributes
	* @exception   com.amsinc.ecsg.frame.AmsException
	*/
   private DocumentHandler initializeInstrumentCapabilities(DocumentHandler inputDoc) throws AmsException
   {
      inputDoc = this.initializeInstrumentCapabilities(inputDoc, "allow_import_DLC",        "dual_auth_import_DLC");
      inputDoc = this.initializeInstrumentCapabilities(inputDoc, "allow_SLC",               "dual_auth_SLC");
      inputDoc = this.initializeInstrumentCapabilities(inputDoc, "allow_export_LC",         "dual_auth_export_LC");
      inputDoc = this.initializeInstrumentCapabilities(inputDoc, "allow_export_collection", "dual_auth_export_coll");
      inputDoc = this.initializeInstrumentCapabilities(inputDoc, "allow_new_export_collection", "dual_auth_new_export_coll"); //Vasavi CR 524 03/31/2010 Add
      inputDoc = this.initializeInstrumentCapabilities(inputDoc, "allow_imp_col", "dual_imp_col"); //SSikhakolli - Rel-9.4 CR-818
      inputDoc = this.initializeInstrumentCapabilities(inputDoc, "allow_guarantee",         "dual_auth_guarantee");
      inputDoc = this.initializeInstrumentCapabilities(inputDoc, "allow_airway_bill",       "dual_auth_airway_bill");
      inputDoc = this.initializeInstrumentCapabilities(inputDoc, "allow_shipping_guar",     "dual_auth_shipping_guar");
      inputDoc = this.initializeInstrumentCapabilities(inputDoc, "allow_loan_request",      "dual_auth_loan_request");
      inputDoc = this.initializeInstrumentCapabilities(inputDoc, "allow_funds_transfer",    "dual_auth_funds_transfer");
      inputDoc = this.initializeInstrumentCapabilities(inputDoc, "allow_request_advise",    "dual_auth_request_advise");
      inputDoc = this.initializeInstrumentCapabilities(inputDoc, "allow_supplier_portal",    "dual_auth_supplier_portal"); //Ravindra - CR-708B

	  inputDoc = this.initializeInstrumentCapabilities(inputDoc, "allow_approval_to_pay",    "dual_auth_approval_to_pay");

	  inputDoc = this.initializeInstrumentCapabilities(inputDoc, "allow_xfer_btwn_accts",    "dual_xfer_btwn_accts");
	  inputDoc = this.initializeInstrumentCapabilities(inputDoc, "allow_domestic_payments",    "dual_domestic_payments");

	  inputDoc = this.initializeInstrumentCapabilities(inputDoc, "allow_arm",    "dual_auth_arm");

	  inputDoc = this.initializeInstrumentCapabilities(inputDoc, "allow_direct_debit",    "dual_auth_direct_debit");

	  inputDoc = this.initializeInstrumentCapabilities(inputDoc, "allow_arm_invoice",    "dual_auth_arm_invoice");

	  inputDoc = this.initializeInstrumentCapabilities(inputDoc, "allow_credit_note",    "dual_auth_credit_note");

	  inputDoc = this.initializeInstrumentCapabilities(inputDoc, "allow_payables",    "dual_auth_payables_inv"); //Rel9.0 CR913
	  inputDoc = this.initializeInstrumentCapabilities(inputDoc, "allow_payables_upl",    "dual_auth_upload_pay_inv"); //Rel9.0 CR913
	  inputDoc = this.initializeInstrumentCapabilities(inputDoc, "allow_pay_credit_note",    "dual_auth_pay_credit_note"); //Rel9.2 CR 914A


	  
	 //IR 21999--- condition is wrong - value cant be both null and empty at a time- check if value is either null or empty.

      if (StringFunction.isBlank(inputDoc.getAttribute("/In/CorporateOrganization/standbys_use_guar_form_only")))
      {
         inputDoc.setAttribute("/In/CorporateOrganization/standbys_use_guar_form_only",
                               TradePortalConstants.INDICATOR_NO);
      }


      if (StringFunction.isBlank(inputDoc.getAttribute("/In/CorporateOrganization/standbys_use_either_form")))
      {
         inputDoc.setAttribute("/In/CorporateOrganization/standbys_use_either_form",
                               TradePortalConstants.INDICATOR_NO);
      }

      if (StringFunction.isBlank(inputDoc.getAttribute("/In/CorporateOrganization/allow_auto_lc_create")))
      {
         inputDoc.setAttribute("/In/CorporateOrganization/allow_auto_lc_create",
                               TradePortalConstants.INDICATOR_NO);
      }


      if (inputDoc.getAttribute("/In/CorporateOrganization/allow_payment_file_upload") == null)
         inputDoc.setAttribute("/In/CorporateOrganization/allow_payment_file_upload", TradePortalConstants.INDICATOR_NO);



      if (inputDoc.getAttribute("/In/CorporateOrganization/straight_through_authorize") == null)
         inputDoc.setAttribute("/In/CorporateOrganization/straight_through_authorize", TradePortalConstants.INDICATOR_NO);
      

      if (inputDoc.getAttribute("/In/CorporateOrganization/allow_partial_pay_auth") == null)
         inputDoc.setAttribute("/In/CorporateOrganization/allow_partial_pay_auth", TradePortalConstants.INDICATOR_NO);


	 if (inputDoc.getAttribute("/In/CorporateOrganization/allow_auto_atp_create") == null)
     {
       inputDoc.setAttribute("/In/CorporateOrganization/allow_auto_atp_create",
                             TradePortalConstants.INDICATOR_NO);
     }

     if (inputDoc.getAttribute("/In/CorporateOrganization/allow_atp_manual_po_entry") == null)
     {
		              inputDoc.setAttribute("/In/CorporateOrganization/allow_atp_manual_po_entry",
		            		  TradePortalConstants.INDICATOR_NO);
     }




     if (inputDoc.getAttribute("/In/CorporateOrganization/allow_manual_po_entry") == null)
     {
	   inputDoc.setAttribute("/In/CorporateOrganization/allow_manual_po_entry",
							 TradePortalConstants.INDICATOR_NO);
     }

     if (inputDoc.getAttribute("/In/CorporateOrganization/enforce_daily_limit_transfer") == null)
     {
	   inputDoc.setAttribute("/In/CorporateOrganization/enforce_daily_limit_transfer",
							 TradePortalConstants.INDICATOR_NO);
     }
     if (inputDoc.getAttribute("/In/CorporateOrganization/enforce_daily_limit_dmstc_pymt") == null)
     {
	   inputDoc.setAttribute("/In/CorporateOrganization/enforce_daily_limit_dmstc_pymt",
							 TradePortalConstants.INDICATOR_NO);
     }
     if (inputDoc.getAttribute("/In/CorporateOrganization/enforce_daily_limit_intl_pymt") == null)
     {
	   inputDoc.setAttribute("/In/CorporateOrganization/enforce_daily_limit_intl_pymt",
							 TradePortalConstants.INDICATOR_NO);
     }


     if (inputDoc.getAttribute("/In/CorporateOrganization/allow_pymt_instrument_auth") == null)
     {
	   inputDoc.setAttribute("/In/CorporateOrganization/allow_pymt_instrument_auth",
							 TradePortalConstants.INDICATOR_NO);
	   inputDoc.setAttribute("/In/CorporateOrganization/allow_panel_auth_for_pymts",
				 "");
     }


     


     if (inputDoc.getAttribute("/In/CorporateOrganization/allow_non_prtl_org_instr_ind") == null)
     {
	   inputDoc.setAttribute("/In/CorporateOrganization/allow_non_prtl_org_instr_ind",
							 TradePortalConstants.INDICATOR_NO);
     }

	   if (inputDoc.getAttribute("/In/CorporateOrganization/atp_invoice_indicator") == null)
     {
       inputDoc.setAttribute("/In/CorporateOrganization/atp_invoice_indicator",
                             TradePortalConstants.INDICATOR_NO);
     }
	   if (inputDoc.getAttribute("/In/CorporateOrganization/failed_invoice_indicator") == null)
     {
       inputDoc.setAttribute("/In/CorporateOrganization/failed_invoice_indicator",
                             TradePortalConstants.INDICATOR_NO);
     }	
	    if (inputDoc.getAttribute("/In/CorporateOrganization/dual_auth_approval_to_pay") == null)
     {
       inputDoc.setAttribute("/In/CorporateOrganization/dual_auth_approval_to_pay","");
     }		 
		 
		
	    
	    if (inputDoc.getAttribute("/In/CorporateOrganization/dual_auth_loan_request") == null)
	     {
	       inputDoc.setAttribute("/In/CorporateOrganization/dual_auth_loan_request","");
	     }		
	    
		
    if (inputDoc.getAttribute("/In/CorporateOrganization/process_invoice_file_upload") == null)
     {
	   inputDoc.setAttribute("/In/CorporateOrganization/process_invoice_file_upload",
							 TradePortalConstants.INDICATOR_NO);
     }
     
     if (inputDoc.getAttribute("/In/CorporateOrganization/trade_loan_type") == null)
     {
	   inputDoc.setAttribute("/In/CorporateOrganization/trade_loan_type",
							 TradePortalConstants.INDICATOR_NO);
     }
     
     if (inputDoc.getAttribute("/In/CorporateOrganization/import_loan_type") == null)
     {
	   inputDoc.setAttribute("/In/CorporateOrganization/import_loan_type",
							 TradePortalConstants.INDICATOR_NO);
     }
     
     if (inputDoc.getAttribute("/In/CorporateOrganization/export_loan_type") == null)
     {
	   inputDoc.setAttribute("/In/CorporateOrganization/export_loan_type",
							 TradePortalConstants.INDICATOR_NO);
     }
     
     if (inputDoc.getAttribute("/In/CorporateOrganization/invoice_financing_type") == null)
     {
	   inputDoc.setAttribute("/In/CorporateOrganization/invoice_financing_type",
							 TradePortalConstants.INDICATOR_NO);
     }    
     
     if (inputDoc.getAttribute("/In/CorporateOrganization/allow_auto_loan_req_inv_upld") == null)
     {
	   inputDoc.setAttribute("/In/CorporateOrganization/allow_auto_loan_req_inv_upld",
							 TradePortalConstants.INDICATOR_NO);
     }    
     
     if (inputDoc.getAttribute("/In/CorporateOrganization/allow_auto_atp_inv_only_upld") == null)
     {
	   inputDoc.setAttribute("/In/CorporateOrganization/allow_auto_atp_inv_only_upld",
							 TradePortalConstants.INDICATOR_NO);
     }    

     //SURREWSH -Rel-9.4 CR-932 Start 
     if (inputDoc.getAttribute("/In/CorporateOrganization/enable_billing_instruments") == null)
     {
	   inputDoc.setAttribute("/In/CorporateOrganization/enable_billing_instruments",
							 TradePortalConstants.INDICATOR_NO);
     }
     //SURREWSH -Rel-9.4 CR-932 End 

      return inputDoc;
   }

   /**
	* This method is a generic method used for setting the corporate org's
	* instrument capabilities and authorization attributes. It looks for the
	* capability name in the input xml doc and sets appropriate attributes
	* accordingly.
	*
	* @param       inputDoc                DocumentHandler       - the current mediator's input xml doc
	* @param       capabilityName          String                - the name of the capability attribute to set
	* @param       authorizationName       String                - the name of the authorization attribute to set
	* @return      void
	* @exception   com.amsinc.ecsg.frame.AmsException
	*/
   private DocumentHandler initializeInstrumentCapabilities(DocumentHandler inputDoc, String capabilityName,
                                                            String authorizationName) throws AmsException
   {
      StringBuffer   authorizationPath = null;
      StringBuilder capabilityPath    = new StringBuilder("/In/CorporateOrganization/");

      capabilityPath.append(capabilityName);
      if (inputDoc.getAttribute(capabilityPath.toString()) == null)
      {
         inputDoc.setAttribute(capabilityPath.toString(), TradePortalConstants.INDICATOR_NO);
         // Chandrakanth IR VAUI121061209 12/26/2008 Begin
         authorizationPath = new StringBuffer("/In/CorporateOrganization/");
         authorizationPath.append(authorizationName);
         //Rel 9.0 IR#T36000027501 - Setting default value 'N' instead of null
         inputDoc.setAttribute(authorizationPath.toString(), TradePortalConstants.INDICATOR_NO);
         // Chandrakanth IR VAUI121061209 12/26/2008 End.
      } else {
		  // Peter Ng IR PUUJ020441802 02/09/2009 Begin
		  if ("allow_xfer_btwn_accts".equals(capabilityName) ||
		  	  "allow_domestic_payments".equals(capabilityName) ||
		  	  "allow_funds_transfer".equals(capabilityName)) {
			  authorizationPath = new StringBuffer("/In/CorporateOrganization/");
			  authorizationPath.append(authorizationName);
			  if (inputDoc.getAttribute(authorizationPath.toString()) == null) {
				  //Rel 9.0 IR#T36000027501 - Setting default value 'N' instead of null
				  inputDoc.setAttribute(authorizationPath.toString(), TradePortalConstants.INDICATOR_NO);
			  }
		  }
		  // Peter Ng IR PUUJ020441802 02/09/2009 End
	  }

      return inputDoc;
   }

    private DocumentHandler initializeAccountDetails(DocumentHandler inputDoc) throws AmsException {
   	   Vector acctVector = inputDoc.getFragments("/In/CorporateOrganization/AccountList");
   	   int numItems = acctVector.size();
   	   for(int iLoop = 0; iLoop < numItems; iLoop++) {

   		
   		   if(inputDoc.getAttribute("/In/CorporateOrganization/AccountList("+iLoop+")/available_for_direct_debit") == null)
   		   {
   			   inputDoc.setAttribute("/In/CorporateOrganization/AccountList("+iLoop+")/available_for_direct_debit", TradePortalConstants.INDICATOR_NO);
   		   }

   		   if(inputDoc.getAttribute("/In/CorporateOrganization/AccountList("+iLoop+")/available_for_loan_request") == null)
   		   {
   			   inputDoc.setAttribute("/In/CorporateOrganization/AccountList("+iLoop+")/available_for_loan_request", TradePortalConstants.INDICATOR_NO);
   		   }


   		   if(inputDoc.getAttribute("/In/CorporateOrganization/AccountList("+iLoop+")/available_for_xfer_btwn_accts") == null)
   		   {
   			   inputDoc.setAttribute("/In/CorporateOrganization/AccountList("+iLoop+")/available_for_xfer_btwn_accts", TradePortalConstants.INDICATOR_NO);
   		   }

   		   if(inputDoc.getAttribute("/In/CorporateOrganization/AccountList("+iLoop+")/available_for_internatnl_pay") == null)
   		   {
   			   inputDoc.setAttribute("/In/CorporateOrganization/AccountList("+iLoop+")/available_for_internatnl_pay", TradePortalConstants.INDICATOR_NO);
   		   }

   		   if(inputDoc.getAttribute("/In/CorporateOrganization/AccountList("+iLoop+")/available_for_domestic_pymts") == null)
		   {
			   inputDoc.setAttribute("/In/CorporateOrganization/AccountList("+iLoop+")/available_for_domestic_pymts", TradePortalConstants.INDICATOR_NO);
		   }
   		
   		   if(inputDoc.getAttribute("/In/CorporateOrganization/AccountList("+iLoop+")/available_for_settle_instr") == null)
   		   {
   			   inputDoc.setAttribute("/In/CorporateOrganization/AccountList("+iLoop+")/available_for_settle_instr", TradePortalConstants.INDICATOR_NO);
   		   }

   		   if(inputDoc.getAttribute("/In/CorporateOrganization/AccountList("+iLoop+")/deactivate_indicator") == null)
   		   {
   			   inputDoc.setAttribute("/In/CorporateOrganization/AccountList("+iLoop+")/deactivate_indicator", TradePortalConstants.INDICATOR_NO);
   		   }

   		   if(inputDoc.getAttribute("/In/CorporateOrganization/AccountList("+iLoop+")/othercorp_customer_indicator") == null)
   		   {
   			   inputDoc.setAttribute("/In/CorporateOrganization/AccountList("+iLoop+")/othercorp_customer_indicator", TradePortalConstants.INDICATOR_NO);
   		   }

   		   
   		   //cquinton 12/17/2012 ir#8895 if not an other corp account, default the account op org if necessary
   		   String otherCorp = inputDoc.getAttribute(
   		       "/In/CorporateOrganization/AccountList("+iLoop+")/othercorp_customer_indicator");
   		   if ( !TradePortalConstants.INDICATOR_YES.equals(otherCorp) ) {
               if(StringFunction.isBlank(inputDoc.getAttribute(
                      "/In/CorporateOrganization/AccountList("+iLoop+")/op_bank_org_oid"))) {
                   String firstOpBankOrg = 
                       inputDoc.getAttribute("/In/CorporateOrganization/first_op_bank_org");
                   if ( firstOpBankOrg!=null ) {
                       inputDoc.setAttribute(
                           "/In/CorporateOrganization/AccountList("+iLoop+")/op_bank_org_oid", 
                           firstOpBankOrg);                       
                   }
               }   		       
   		   }
   		   
   	   }
   	   return inputDoc;
   }
    // Chandrakanth IR AAUI120247248 12/08/2008 Begin

    /** Convert decimal number from its locale-specific entry format into
     * a standard format that can be understood by the database.
     * If the format is invalid, an error value will be placed into
     * the XML that will trigger an error later.
     * In the corresponding post mediator action, the passed-in value
     * is placed back into its original location.
     *
     * @param inputDoc DocumentHandler - the input document of the mediator
     * @param resMgr ResourceManager - used to determine locale of the user
     * @param attribute String - the decimal attribute being handled
     */
     //Narayan IR-NNUM031454220 04/08/2012 removed temp parameter as it is not required
     /*    public void doDecimalPreMediatorAction(DocumentHandler inputDoc, ResourceManager resMgr,
    		String pathToPopulate, String tempPath)*/
    public void doDecimalPreMediatorAction(DocumentHandler inputDoc, ResourceManager resMgr, String pathToPopulate)
    {
    	// get the input document
    	if (inputDoc == null)
    	{
    		LOG.info("Input document to is null");
    		return;
    	}

        


    	String amount;

    	try
    	{
    		// Convert the amount from its localized form to standard (en_US) form
    		amount =  NumberValidator.getNonInternationalizedValue(inputDoc.getAttribute(pathToPopulate), resMgr.getResourceLocale(), true);
    	}
    	catch(InvalidAttributeValueException iae)
    	{
    		// If the format was invalid, set amount equal to something that will trigger an error
    		// to be issued.
    		
    		amount = inputDoc.getAttribute(pathToPopulate);
    		
    	}

    	// Place the result of the above code into the amount location
    	// This is what will be populated into the business object.
    	inputDoc.setAttribute(pathToPopulate, amount);
    }


    protected boolean isAccountsReferncedByUser(Map acctDeleteMap) {
    	if ( acctDeleteMap.size() <1)
    		return false;


    	List<Object> sqlParams = new ArrayList();
    	StringBuilder sb = new StringBuilder("select A_ACCOUNT_OID from USER_AUTHORIZED_ACCT where A_ACCOUNT_OID in (");

    	DocumentHandler doc=null;
		for (Object o : acctDeleteMap.keySet()) {
			String key = (String) o;
			doc = (DocumentHandler) acctDeleteMap.get(key);
			sb.append("?,");
			sqlParams.add(doc.getAttribute("/Oid"));
		}
		sb.deleteCharAt(sb.length()-1).append(")");
    	try {
			DocumentHandler result = DatabaseQueryBean.getXmlResultSet(sb.toString(), true, sqlParams);
			if (result != null) {
				return true;
			}
		} catch (AmsException e) {

			e.printStackTrace();
		}
		return false;
    }
    

    protected boolean isReportCustomCategoryReferncedByUser(String organizationOid, List customCategory ) throws AmsException{
    	
    	String [] userReportCategories = {"FIRST_REPT_CATEG", "SECOND_REPT_CATEG",
				"THIRD_REPT_CATEG",	"FOURTH_REPT_CATEG",
				"FIFTH_REPT_CATEG",	"SIXTH_REPT_CATEG",
				"SEVENTH_REPT_CATEG","EIGHTH_REPT_CATEG",
				"NINETH_REPT_CATEG","TENTH_REPT_CATEG"};

    	StringBuilder sb = new StringBuilder("select unique ");
    	int index=0;
    	while( index<9) {
    		sb.append(userReportCategories[index]);
    		sb.append(", ");
    		index++;
    	}
    	sb.append(userReportCategories[index]);
    	
    	sb.append(" from users");
    	sb.append(" where p_owner_org_oid = ?");
    	
    	DocumentHandler resultXML = DatabaseQueryBean.getXmlResultSet(sb.toString(), true, new Object[]{organizationOid});
    	if(resultXML != null){   			    	   
    	     int catCount = resultXML.getFragments("/ResultSetRecord").size();
	          for (int recNum=0;recNum<catCount;recNum++)
	          	{
	        	  for ( index = 0; index < 10; index++) {
		      			String categoryValue  = resultXML.getAttribute("/ResultSetRecord(" + recNum +")/"+userReportCategories[index]);
		      			if (categoryValue != null && categoryValue.length() > 0 && !customCategory.contains(categoryValue)) {
		      				return true;
		      			}
		      		}
	          	}                                             			
           }    
    	return false;
    }
 


    /**
     * This method formats the threshold Amount
     * @param thresholdAmt
     * @return
     */

  //changed the function in order to resolve the issue "Duplicate margin rule" error
 protected String formatThreshold(String thresholdAmt){
	 StringBuilder sbuf = new StringBuilder();
		int lastZeroPos = 0; 
		if(thresholdAmt!=null && thresholdAmt.length()>0){
			if(thresholdAmt.indexOf(".")>0){
				sbuf.append(thresholdAmt);

				int indexOfDecimalPoint=thresholdAmt.indexOf(".");
				int len = sbuf.length();
				int count = 0;
				while(count==0){
					lastZeroPos = sbuf.lastIndexOf("0");
					if(lastZeroPos>indexOfDecimalPoint){
						if(lastZeroPos==(len-1)){
							sbuf.deleteCharAt(lastZeroPos);
							len = sbuf.length();
						} else
							count = 1;
					}
					else
						count = 1;
				}
			}
			else
				return thresholdAmt;

		}
		return sbuf.toString();
	}
 	
 
 private List validateMultiplePanelGroupsforPayments(DocumentHandler acctDoc) throws RemoteException, AmsException{
		
		Vector aVector = new Vector();
		String panelGroupOid;

		String panelAuthGroupSql =
     	  "select panel_auth_group_oid PAG_OID, ACH_PYMT_METHOD_IND ACH, BCHK_PYMT_METHOD_IND BCHK, BKT_PYMT_METHOD_IND BKT, CBFT_PYMT_METHOD_IND CBFT,"
			+ "CCHK_PYMT_METHOD_IND CCHK, RTGS_PYMT_METHOD_IND RTGS, FTBA_PYMT_METHOD_IND FTBA, panelgroupconfpaymentinst CONF_IND"
                 	+ " from panel_auth_group"
                  	+ " where panel_auth_group_oid in (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		  Object sqlParams[] = new Object[16];
		  sqlParams[0] = acctDoc.getAttribute("/panel_auth_group_oid_1");
		  sqlParams[1] = acctDoc.getAttribute("/panel_auth_group_oid_2");
		  sqlParams[2] = acctDoc.getAttribute("/panel_auth_group_oid_3");
		  sqlParams[3] = acctDoc.getAttribute("/panel_auth_group_oid_4");
		  sqlParams[4] = acctDoc.getAttribute("/panel_auth_group_oid_5");
		  sqlParams[5] = acctDoc.getAttribute("/panel_auth_group_oid_6");
		  sqlParams[6] = acctDoc.getAttribute("/panel_auth_group_oid_7");
		  sqlParams[7] = acctDoc.getAttribute("/panel_auth_group_oid_8");
		  sqlParams[8] = acctDoc.getAttribute("/panel_auth_group_oid_9");
		  sqlParams[9] = acctDoc.getAttribute("/panel_auth_group_oid_10");
		  sqlParams[10] = acctDoc.getAttribute("/panel_auth_group_oid_11");
		  sqlParams[11] = acctDoc.getAttribute("/panel_auth_group_oid_12");
		  sqlParams[12] = acctDoc.getAttribute("/panel_auth_group_oid_13");
		  sqlParams[13] = acctDoc.getAttribute("/panel_auth_group_oid_14");
		  sqlParams[14] = acctDoc.getAttribute("/panel_auth_group_oid_15");
		  sqlParams[15] = acctDoc.getAttribute("/panel_auth_group_oid_16");
		  
	
		DocumentHandler result = DatabaseQueryBean.getXmlResultSet(panelAuthGroupSql,false, sqlParams);
		if(result != null) aVector = result.getFragments("/ResultSetRecord");
		int vSize = aVector.size();
		ArrayList commonPmtMethodTypesList = new ArrayList();
		if(vSize > 0){
			for(int i=0;i<vSize;i++){
				String capabilities = "";
				panelGroupOid = result.getAttribute("/ResultSetRecord(" + i + ")/PAG_OID");
				capabilities+= result.getAttribute("/ResultSetRecord(" + i + ")/ACH").equals(TradePortalConstants.INDICATOR_YES)?"ACH|":" |";
				capabilities+= result.getAttribute("/ResultSetRecord(" + i + ")/BCHK").equals(TradePortalConstants.INDICATOR_YES)?"BCHK|":" |";
				capabilities+= result.getAttribute("/ResultSetRecord(" + i + ")/BKT").equals(TradePortalConstants.INDICATOR_YES)?"BKT|":" |";
				capabilities+= result.getAttribute("/ResultSetRecord(" + i + ")/CBFT").equals(TradePortalConstants.INDICATOR_YES)?"CBFT|":" |";
				capabilities+= result.getAttribute("/ResultSetRecord(" + i + ")/CCHK").equals(TradePortalConstants.INDICATOR_YES)?"CCHK|":" |";
				capabilities+= result.getAttribute("/ResultSetRecord(" + i + ")/RTGS").equals(TradePortalConstants.INDICATOR_YES)?"RTGS|":" |";
				capabilities+= result.getAttribute("/ResultSetRecord(" + i + ")/FTBA").equals(TradePortalConstants.INDICATOR_YES)?"FTBA|":" |";
				String conf_ind = result.getAttribute("/ResultSetRecord(" + i + ")/CONF_IND");
				
				for(int j=i+1;j<vSize;j++){
					
					String capabilitiesNext = "";
					capabilitiesNext+= result.getAttribute("/ResultSetRecord(" + j + ")/ACH").equals(TradePortalConstants.INDICATOR_YES)?"ACH|":" |";
					capabilitiesNext+= result.getAttribute("/ResultSetRecord(" + j + ")/BCHK").equals(TradePortalConstants.INDICATOR_YES)?"BCHK|":" |";
					capabilitiesNext+= result.getAttribute("/ResultSetRecord(" + j + ")/BKT").equals(TradePortalConstants.INDICATOR_YES)?"BKT|":" |";
					capabilitiesNext+= result.getAttribute("/ResultSetRecord(" + j + ")/CBFT").equals(TradePortalConstants.INDICATOR_YES)?"CBFT|":" |";
					capabilitiesNext+= result.getAttribute("/ResultSetRecord(" + j + ")/CCHK").equals(TradePortalConstants.INDICATOR_YES)?"CCHK|":" |";
					capabilitiesNext+= result.getAttribute("/ResultSetRecord(" + j + ")/RTGS").equals(TradePortalConstants.INDICATOR_YES)?"RTGS|":" |";
					capabilitiesNext+= result.getAttribute("/ResultSetRecord(" + j + ")/FTBA").equals(TradePortalConstants.INDICATOR_YES)?"FTBA|":" |";
					String conf_indNext = result.getAttribute("/ResultSetRecord(" + j + ")/CONF_IND");
					//PMitnala IR T36000020754 Rel 8.3 - Added another condition. 
					//If the confidential indicator is ON for one of the Panels then only check for common products
					if(conf_ind.equals(conf_indNext)){
							String[] data = capabilities.split("\\|");
							String[] dataNext = capabilitiesNext.split("\\|");
							for (int k=0;k<data.length;k++){
								
									if(data[k].equals(dataNext[k]) && !data[k].equals(" ")){
										if(!commonPmtMethodTypesList.contains(data[k])){
											commonPmtMethodTypesList.add(data[k]);
										}
									}
							}
					}
					
					
				}
			}
		}
		
		return commonPmtMethodTypesList;
	}
 
 	private void setPayableCreditNoteSettings (DocumentHandler inputDoc){
 		
 	   String uploadInd = inputDoc.getAttribute("/In/CorporateOrganization/allow_pay_credit_note_upload");
 	   
 	   String endToEnd=inputDoc.getAttribute("/In/CorporateOrganization/allow_end_to_end_id_process");
 	   String manual = inputDoc.getAttribute("/In/CorporateOrganization/allow_manual_cr_note_apply");
 	   String applicationMethod = inputDoc.getAttribute("/In/CorporateOrganization/apply_pay_credit_note");
 	   
 			   
 	   
 	   if(TradePortalConstants.INDICATOR_NO.equals(uploadInd)){
  		   inputDoc.setAttribute("/In/CorporateOrganization/pay_credit_note_available_days", null);
 		   inputDoc.setAttribute("/In/CorporateOrganization/allow_end_to_end_id_process", null);
 		   inputDoc.setAttribute("/In/CorporateOrganization/allow_manual_cr_note_apply", null);
 		   inputDoc.setAttribute("/In/CorporateOrganization/apply_pay_credit_note", null);
 		   inputDoc.setAttribute("/In/CorporateOrganization/pay_credit_sequential_type", null);
  	  }
 	   
 	  if(TradePortalConstants.INDICATOR_YES.equals(endToEnd)){
  		   inputDoc.setAttribute("/In/CorporateOrganization/allow_manual_cr_note_apply", null);
		   inputDoc.setAttribute("/In/CorporateOrganization/apply_pay_credit_note", null);
		   inputDoc.setAttribute("/In/CorporateOrganization/pay_credit_sequential_type", null);
 	  }
 	  
 	  
 	 if (TradePortalConstants.CREDIT_NOTE_APPLICATION_PROPOTIONATE.equalsIgnoreCase(applicationMethod)){
 		 
 		 inputDoc.setAttribute("/In/CorporateOrganization/pay_credit_sequential_type", null);
 	 }
 	  
 	 if (StringFunction.isBlank(manual) && StringFunction.isBlank(endToEnd)){

 		 inputDoc.setAttribute("/In/CorporateOrganization/allow_end_to_end_id_process", null);
 		   inputDoc.setAttribute("/In/CorporateOrganization/allow_manual_cr_note_apply", null);
 		 
 	 }
 	   // in update mode,database value is retained even if allow_end_to_end_id_process by user,hence make it null explicitly
 	 
 	  if (null==endToEnd){
 		 inputDoc.setAttribute("/In/CorporateOrganization/allow_end_to_end_id_process", null);
 	  }
 	  
 	  //T36000036065-start
 	  if (null == manual){
 		   inputDoc.setAttribute("/In/CorporateOrganization/apply_pay_credit_note", null);
 		   inputDoc.setAttribute("/In/CorporateOrganization/pay_credit_sequential_type", null);
 	  }
 	  //T36000036065-end
 	  

      if (inputDoc.getAttribute("/In/CorporateOrganization/enable_billing_instruments") == null)
      {
 	   inputDoc.setAttribute("/In/CorporateOrganization/enable_billing_instruments",
 							 TradePortalConstants.INDICATOR_NO);
      }
 	 
 	  
 	}
}
