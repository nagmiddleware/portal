package com.ams.tradeportal.mediator.premediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */

import com.amsinc.ecsg.web.*;
import javax.servlet.http.*;
import java.util.*;

import com.amsinc.ecsg.frame.ClientServerDataBridge;
import com.amsinc.ecsg.frame.ReferenceDataManager;
import com.amsinc.ecsg.util.*;
import com.ams.tradeportal.common.*;
import com.ams.tradeportal.busobj.util.*;

public class BankBranchRulePreEdit extends GenericTransactionPreEdit implements com.amsinc.ecsg.web.WebAction {
private static final Logger LOG = LoggerFactory.getLogger(BankBranchRulePreEdit.class);
	/**
	 * LRQPreEdit constructor comment.
	 */
	public BankBranchRulePreEdit() {
		super();
	}

	/**
	 */
	public void act(AmsServletInvocation reqInfo,
			BeanManager beanMgr,
			FormManager formMgr,
			ResourceManager resMgr,
			String serverLocation,
			HttpServletRequest request,
			HttpServletResponse response,
			Hashtable inputParmMap,
			DocumentHandler inputDoc)
	throws com.amsinc.ecsg.frame.AmsException
	{
        ClientServerDataBridge csdb = resMgr.getCSDB ();

		//IR - SEUK012878009 - 02/04/2010 - Begin

        //inputDoc.setAttribute("/In/BankBranchRule/description",
        //        ReferenceDataManager.getRefDataMgr().getDescr("COUNTRY", inputDoc.getAttribute("/In/BankBranchRule/address_country"), csdb.getLocaleName())
        //        + ", " + ReferenceDataManager.getRefDataMgr().getDescr("PAYMENT_METHOD", inputDoc.getAttribute("/In/BankBranchRule/payment_method"), csdb.getLocaleName())
        //        + ", " + inputDoc.getAttribute("/In/BankBranchRule/bank_branch_code"));

        inputDoc.setAttribute("/In/BankBranchRule/description", resMgr.getText("BankBranchRuleDetail.BankBranchRule", TradePortalConstants.TEXT_BUNDLE));

        //IR - SEUK012878009 - 02/04/2010 - End
	}

}