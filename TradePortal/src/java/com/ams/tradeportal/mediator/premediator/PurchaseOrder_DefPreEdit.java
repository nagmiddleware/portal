package com.ams.tradeportal.mediator.premediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
*
*
*     Copyright  � 2001                         
*     American Management Systems, Incorporated 
*     All rights reserved
*/

import com.amsinc.ecsg.web.*;
import javax.servlet.http.*;
import java.util.*;
import com.amsinc.ecsg.util.*;
import com.ams.tradeportal.common.*;
import java.math.*;
import com.ams.tradeportal.busobj.webbean.*;
import com.ams.tradeportal.busobj.util.*;


public class PurchaseOrder_DefPreEdit implements com.amsinc.ecsg.web.WebAction{
private static final Logger LOG = LoggerFactory.getLogger(PurchaseOrder_DefPreEdit.class);

	/**
	 * AdminUserPreEdit constructor comment.
	 */
	public PurchaseOrder_DefPreEdit() {
		super();
   }

	/**Explicitly sets all 'missing' checkbox fields to N.
	 * (Checkbox fields which are not checked are not sent in the document,
	 * we must force an N to cause unchecked values to be save correctly.)
	 * 
	 */
	public void act(AmsServletInvocation reqInfo, 
					BeanManager beanMgr, 
					FormManager formMgr, 
					ResourceManager resMgr, 
					String serverLocation, 
					HttpServletRequest request, 
					HttpServletResponse response, 
					Hashtable inputParmMap,
					DocumentHandler inputDoc)
		throws com.amsinc.ecsg.frame.AmsException {

		String value;
		
		// get the input document 
		if (inputDoc == null) {
			LOG.info("Input document to PurchaseOrder_DefPreEdit is null");
			return;
		}
		
	    doPurchaseOrderDefPreEdit( inputDoc );
	}

	/**
	 * Performs pre-editing of the input document that is specific to the
	 * Invoice Definition Detail JSP. Also, we need to explicitly set the
	 * default checkbox value to N when it is not already present in the document.
	 *
	 * @param doc com.amsinc.ecsg.util.DocumentHandler
	 */
	private void doPurchaseOrderDefPreEdit(DocumentHandler inputDoc) {

		// get the input document 
		if (inputDoc == null) {
			LOG.info("Input document to PurchaseOrder_DefPreEdit is null");
			return;
		}

		// Checkboxes do not send values if the checkbox is unchecked.
		// Therefore, for each checkbox box, determine if we have a
		// value in the document.  If not, add an explicit N value for
		// each one that is missing.  This ensures that checkboxes that
		// are unchecked by the user get reset to N.
		
		String saveAsFlag = inputDoc.getAttribute("/In/PurchaseOrderDefinition/save_as_flag");
		if("Y".equals(saveAsFlag))
		{	
			inputDoc.setAttribute("/In/PurchaseOrderDefinition/purchase_order_definition_oid","0");
		}
		
		String partToValidate = inputDoc.getAttribute("/In/PurchaseOrderDefinition/part_to_validate");

		if(partToValidate != null && TradePortalConstants.INV_GENERAL.equals(partToValidate)){
				if(inputDoc.getAttribute("/In/PurchaseOrderDefinition/default_flag")==null){
					resetCheckBoxAttribute( inputDoc, "/In/PurchaseOrderDefinition/default_flag" );
				}
				//Leelavathi CR-707 Rel-800 09/03/2012 Begin
				resetCheckBoxAttribute( inputDoc, "/In/PurchaseOrderDefinition/include_column_headers" );
				resetCheckBoxAttribute( inputDoc, "/In/PurchaseOrderDefinition/latest_shipment_date_req" );	
				//Srinivasu_D Rel8.0 IR# BRUM032645430 03/29/2012 
				resetCheckBoxAttribute( inputDoc, "/In/PurchaseOrderDefinition/line_item_detail_provided" ); 
				//Leelavathi CR-707 Rel-800 09/03/2012 End
		}
		
		// Checking Purchase Summary Deatil field required check box
		
		resetCheckBoxAttribute( inputDoc, "/In/PurchaseOrderDefinition/incoterm_req" );
		resetCheckBoxAttribute( inputDoc, "/In/PurchaseOrderDefinition/incoterm_loc_req" );
		resetCheckBoxAttribute( inputDoc, "/In/PurchaseOrderDefinition/partial_ship_allowed_req" );	
		resetCheckBoxAttribute( inputDoc, "/In/PurchaseOrderDefinition/goods_desc_req" );	
		
		
		
		for(int x=1; x<=TradePortalConstants.PO_BUYER_OR_SELLER_USER_DEF_FIELD; x++){
			resetCheckBoxAttribute( inputDoc, "/In/PurchaseOrderDefinition/buyer_users_def" + x + "_req");
			resetCheckBoxAttribute( inputDoc, "/In/PurchaseOrderDefinition/seller_users_def" + x + "_req");			
		}
		
		//Checking line item field required check box
		
		
		resetCheckBoxAttribute( inputDoc, "/In/PurchaseOrderDefinition/quantity_req" );		
		resetCheckBoxAttribute( inputDoc, "/In/PurchaseOrderDefinition/line_item_num_req" );
		resetCheckBoxAttribute( inputDoc, "/In/PurchaseOrderDefinition/unit_price_req" );
		resetCheckBoxAttribute( inputDoc, "/In/PurchaseOrderDefinition/unit_of_measure_req" );
		resetCheckBoxAttribute( inputDoc, "/In/PurchaseOrderDefinition/quantity_var_plus_req" );

		
		
		
		for(int x=1; x<=TradePortalConstants.PO_LINE_ITEM_TYPE_OR_VALUE_FIELD; x++){
			resetCheckBoxAttribute( inputDoc, "/In/PurchaseOrderDefinition/prod_chars_ud" + x + "_req");		
		}   
		
		if(TradePortalConstants.INDICATOR_YES.equals(inputDoc.getAttribute("/In/PurchaseOrderDefinition/line_item_detail_provided"))){
			inputDoc.setAttribute("/In/PurchaseOrderDefinition/line_item_num_req",TradePortalConstants.INDICATOR_NO);
			inputDoc.setAttribute("/In/PurchaseOrderDefinition/unit_price_req",TradePortalConstants.INDICATOR_NO);
			inputDoc.setAttribute("/In/PurchaseOrderDefinition/quantity_req",TradePortalConstants.INDICATOR_NO);
			inputDoc.removeComponent("/In/PurchaseOrderDefinition/po_line_item_field1");
			inputDoc.removeComponent("/In/PurchaseOrderDefinition/po_line_item_field2");
			inputDoc.removeComponent("/In/PurchaseOrderDefinition/po_line_item_field3");
			
		}
		
	}

	/**
	 * We check for a value at a given path in the Xml Doc, if it
	 * Does'nt exist then we set that value in the document to 'N'.  This is used for
	 * resetting a Check boxes value since deselection of a check box does NOT update the
	 * Xml Doc with a revised value.
	 *
	 * @param DocumentHandler inputDoc - the document to get and set data against.
	 * @param String xmlPath           - this the the path to use to get/set
	 *                                   attributes in the xml Doc.
	 */
	private static void resetCheckBoxAttribute( DocumentHandler inputDoc, String xmlPath) {

	    String value;

	    value = inputDoc.getAttribute( xmlPath );
	    if (value == null)
	        inputDoc.setAttribute( xmlPath, TradePortalConstants.INDICATOR_NO );
	}
}