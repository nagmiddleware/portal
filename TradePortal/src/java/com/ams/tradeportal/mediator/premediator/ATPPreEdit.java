package com.ams.tradeportal.mediator.premediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */

import com.amsinc.ecsg.web.*;
import javax.servlet.http.*;
import java.util.*;
import com.amsinc.ecsg.util.*;
import com.ams.tradeportal.common.*;
import com.ams.tradeportal.busobj.util.*;

public class ATPPreEdit extends GenericTransactionPreEdit implements com.amsinc.ecsg.web.WebAction {
private static final Logger LOG = LoggerFactory.getLogger(ATPPreEdit.class);
/**
 * AdminUserPreEdit constructor comment.
 */
public ATPPreEdit() {
	super();
}

/**
 * Performs different logic based on the type of transaction (ISS or AMD)
 * 
 * But basically converts the month, day, year fields for each of the 
 * dates into single date fields.  This allows populateFromXml to work.
 *
 * In addition, explicitly sets all 'missing' checkbox fields to N.
 * (Checkbox fields which are not checked are not sent in the document,
 * we must force an N to cause unchecked values to be save correctly.)
 * 
 */
 public void act(AmsServletInvocation reqInfo, 
				BeanManager beanMgr, 
				FormManager formMgr, 
				ResourceManager resMgr, 
				String serverLocation, 
				HttpServletRequest request, 
				HttpServletResponse response, 
				Hashtable inputParmMap,
				DocumentHandler inputDoc)
			throws com.amsinc.ecsg.frame.AmsException
 {
	String value;

	// get the input document 
	if (inputDoc == null)
	{
	   LOG.info("Input document to ATPPreEdit is null");
	   return;
	}

        // Convert decimal number from its locale-specific entry format into
        // a standard format that can be understood by the database.
        // If the format is invalid, an error value will be placed into
        // the XML that will trigger an error later.
        // In the corresponding post mediator action, the passed-in value
        // is placed back into its original location.
        doDecimalPreMediatorAction(inputDoc, resMgr, "amount");

	value = inputDoc.getAttribute("/In/Transaction/transaction_type_code");
	if (TransactionType.AMEND.equals(value))
	{
	   doAMDPreEdit(inputDoc, resMgr);
	}
	else
	{
	   doISSPreEdit(inputDoc, resMgr);
	}
 } 

/**
 * Handles pre-edits for the AMD type transactions.  This includes
 * converting 3part date field into single date values and then to 
 * check the increase/decrease flag and reset the amount to a negative
 * value if the flag is decrease.
 *
 * @param doc com.amsinc.ecsg.util.DocumentHandler
 */
 private void doAMDPreEdit(DocumentHandler inputDoc, ResourceManager resMgr)
 {
	String newExpDate;
	String newshipDate;
	String newInvoiceDate;
	String value;
	
	// get the input document 
	if (inputDoc == null)
	{
	   LOG.info("Input document to ATPPreEdit is null");
	   return;
	}
	
	newExpDate = inputDoc.getAttribute("/In/Terms/expiry_date");
	newExpDate = TPDateTimeUtility.convertISODateToJPylonDate(newExpDate);
	inputDoc.setAttribute("/In/Terms/expiry_date", newExpDate);

	newshipDate = inputDoc.getAttribute("/In/Terms/ShipmentTermsList/latest_shipment_date");
	newshipDate = TPDateTimeUtility.convertISODateToJPylonDate(newshipDate);
	inputDoc.setAttribute("/In/Terms/ShipmentTermsList/latest_shipment_date", newshipDate);
	
	newInvoiceDate = inputDoc.getAttribute("/In/Terms/invoice_due_date");
	//Srinivasu_D IR#T36000019036 Rel8.2 07/11/2013 - date revert to newInvoiceDate from newshipDate.
	newInvoiceDate = TPDateTimeUtility.convertISODateToJPylonDate(newInvoiceDate); 
	inputDoc.setAttribute("/In/Terms/invoice_due_date", newInvoiceDate);
	
	
	value = inputDoc.getAttribute("/In/Terms/invoice_only_ind");
	   if (value == null)
	   {
		  inputDoc.setAttribute("/In/Terms/invoice_only_ind", TradePortalConstants.INDICATOR_NO);
	   }
	
 } 

/**
 * Performs pre-editing of the input document that is specific to ISS
 * transactions.  This includes converting the three part date fields
 * into a single date values and explicitly setting checkbox values
 * to N when they are not already present in the document.
 *
 * @param doc com.amsinc.ecsg.util.DocumentHandler
 */
 private void doISSPreEdit(DocumentHandler inputDoc, ResourceManager resMgr)
 {
	String date;
	String newDate;
	String partNumber;
	String value;

	// get the input document 
	if (inputDoc == null)
	{
	   LOG.info("Input document to ATPPreEdit is null");
	   return;
	}

        // Convert decimal numbers from their locale-specific entry format into
        // a standard format that can be understood by the database.
        // If the format is invalid, an error value will be placed into
        // the XML that will trigger an error later.
        // In the corresponding post mediator action, the passed-in value
        // is placed back into its original location.
        doDecimalPreMediatorAction(inputDoc, resMgr, "fec_rate");
        doDecimalPreMediatorAction(inputDoc, resMgr, "fec_amount");
        doDecimalPreMediatorAction(inputDoc, resMgr, "insurance_endorse_value_plus");

	// get the part number of the page being looked at (if one exists)
	partNumber = inputDoc.getAttribute("/In/partNumber");

	date = inputDoc.getAttribute("/In/Terms/expiry_date");
    newDate = TPDateTimeUtility.convertISODateToJPylonDate(date);
    inputDoc.setAttribute("/In/Terms/expiry_date", newDate);
	
	// Checkboxes do not send values if the checkbox is unchecked.
	   // Therefore, for each checkbox box, determine if we have a
	   // value in the document.  If not, add an explicit N value for
	   // each one that is missing.  This ensures that checkboxes that
	   // are unchecked by the user get reset to N.
	   value = inputDoc.getAttribute("/In/Terms/comm_invoice_indicator");
	   if (value == null)
	   {
		  inputDoc.setAttribute("/In/Terms/comm_invoice_indicator", TradePortalConstants.INDICATOR_NO);
	   }
	   value = inputDoc.getAttribute("/In/Terms/packing_list_indicator");
	   if (value == null)
	   {
		  inputDoc.setAttribute("/In/Terms/packing_list_indicator", TradePortalConstants.INDICATOR_NO);
	   }
	   value = inputDoc.getAttribute("/In/Terms/cert_origin_indicator");
	   if (value == null)
	   {
		  inputDoc.setAttribute("/In/Terms/cert_origin_indicator", TradePortalConstants.INDICATOR_NO);
	   }
	   value = inputDoc.getAttribute("/In/Terms/ins_policy_indicator");
	   if (value == null)
	   {
		  inputDoc.setAttribute("/In/Terms/ins_policy_indicator", TradePortalConstants.INDICATOR_NO);
	   }

	   value = inputDoc.getAttribute("/In/Terms/other_req_doc_1_indicator");
	   if (value == null)
	   {
		  inputDoc.setAttribute("/In/Terms/other_req_doc_1_indicator", TradePortalConstants.INDICATOR_NO);
	   }
	   value = inputDoc.getAttribute("/In/Terms/other_req_doc_2_indicator");
	   if (value == null)
	   {
		  inputDoc.setAttribute("/In/Terms/other_req_doc_2_indicator", TradePortalConstants.INDICATOR_NO);
	   }
	   value = inputDoc.getAttribute("/In/Terms/other_req_doc_3_indicator");
	   if (value == null)
	   {
		  inputDoc.setAttribute("/In/Terms/other_req_doc_3_indicator", TradePortalConstants.INDICATOR_NO);
	   }
	   value = inputDoc.getAttribute("/In/Terms/other_req_doc_4_indicator");
	   if (value == null)
	   {
		  inputDoc.setAttribute("/In/Terms/other_req_doc_4_indicator", TradePortalConstants.INDICATOR_NO);
	   }

	   value = inputDoc.getAttribute("/In/Terms/addl_doc_indicator");
	   if (value == null)
	   {
		  inputDoc.setAttribute("/In/Terms/addl_doc_indicator", TradePortalConstants.INDICATOR_NO);
	   }
   
	
	   date = inputDoc.getAttribute("/In/Terms/ShipmentTermsList/latest_shipment_date");
	   newDate = TPDateTimeUtility.convertISODateToJPylonDate(date);
	   inputDoc.setAttribute("/In/Terms/ShipmentTermsList/latest_shipment_date", newDate);

	 //Leelavathi IR#T36000014569 Rel-8.2 11/03/2013 Begin
		int addDocRowCount=inputDoc.getFragments("/In/Terms/AdditionalReqDocList").size();
		//Leelavathi IR#T36000015469 CR-737 Rel-8.2 05/14/2013 Begin
		for(int iLoop=8; iLoop<(8+addDocRowCount); iLoop++){
		//Leelavathi IR#T36000015469 CR-737 Rel-8.2 05/14/2013 End	
		if( StringFunction.isBlank(inputDoc.getAttribute("/In/Terms/AdditionalReqDocList("+iLoop+")/addl_req_doc_oid"))
				&& StringFunction.isBlank(inputDoc.getAttribute("/In/Terms/AdditionalReqDocList("+iLoop+")/addl_req_doc_ind"))
				&& StringFunction.isBlank(inputDoc.getAttribute("/In/Terms/AdditionalReqDocList("+iLoop+")/addl_req_doc_name"))
				&& StringFunction.isBlank(inputDoc.getAttribute("/In/Terms/AdditionalReqDocList("+iLoop+")/addl_req_doc_originals"))
				&& StringFunction.isBlank(inputDoc.getAttribute("/In/Terms/AdditionalReqDocList("+iLoop+")/addl_req_doc_copies")) 
				&& StringFunction.isBlank(inputDoc.getAttribute("/In/Terms/AdditionalReqDocList("+iLoop+")/addl_req_doc_text"))
				
			){
			inputDoc.removeAllChildren("/In/Terms/AdditionalReqDocList("+iLoop+")");
			inputDoc.removeComponent("/In/Terms/AdditionalReqDocList("+iLoop+")");
			}
		// Leelavathi IR#T36000016410 12/05/2013 CR-737 Rel8200- Start		
		else{
			   value = inputDoc.getAttribute("/In/Terms/AdditionalReqDocList("+iLoop+")/addl_req_doc_ind");
			   if (value == null)
			   {
				  inputDoc.setAttribute("/In/Terms/AdditionalReqDocList("+iLoop+")/addl_req_doc_ind", TradePortalConstants.INDICATOR_NO);
			   }
			}
		
		}
		
	   // Checkboxes do not send values if the checkbox is unchecked.
	   // Therefore, for each checkbox box, determine if we have a
	   // value in the document.  If not, add an explicit N value for
	   // each one that is missing.  This ensures that checkboxes that
	   // are unchecked by the user get reset to N.
	   value = inputDoc.getAttribute("/In/Terms/ShipmentTermsList/trans_doc_included");
	   if (value == null)
	   {
		  inputDoc.setAttribute("/In/Terms/ShipmentTermsList/trans_doc_included", TradePortalConstants.INDICATOR_NO);
	   }
	   value = inputDoc.getAttribute("/In/Terms/ShipmentTermsList/trans_addl_doc_included");
	   if (value == null)
	   {
		  inputDoc.setAttribute("/In/Terms/ShipmentTermsList/trans_addl_doc_included", TradePortalConstants.INDICATOR_NO);
	   }

	   value = inputDoc.getAttribute("/In/Terms/ShipmentTermsList/transshipment_allowed");
	   if (value == null)
	   {
		  inputDoc.setAttribute("/In/Terms/ShipmentTermsList/transshipment_allowed", TradePortalConstants.INDICATOR_NO);
	   }

	   value = inputDoc.getAttribute("/In/Terms/partial_shipment_allowed");
	   if (value == null)
	   {
		  inputDoc.setAttribute("/In/Terms/partial_shipment_allowed", TradePortalConstants.INDICATOR_NO);
	   }
	
	   date = inputDoc.getAttribute("/In/Terms/fec_maturity_date");
	   newDate = TPDateTimeUtility.convertISODateToJPylonDate(date);
	   inputDoc.setAttribute("/In/Terms/fec_maturity_date", newDate);

	   date = inputDoc.getAttribute("/In/Terms/invoice_due_date");
	   newDate = TPDateTimeUtility.convertISODateToJPylonDate(date);
	   inputDoc.setAttribute("/In/Terms/invoice_due_date", newDate);

	   

	   // Checkboxes do not send values if the checkbox is unchecked.
	   // Therefore, for each checkbox box, determine if we have a
	   // value in the document.  If not, add an explicit N value for
	   // each one that is missing.  This ensures that checkboxes that
	   // are unchecked by the user get reset to N.
	   
	   value = inputDoc.getAttribute("/In/Terms/finance_drawing");
	   if (value == null)
	   {
		  inputDoc.setAttribute("/In/Terms/finance_drawing", TradePortalConstants.INDICATOR_NO);
	   }
	   value = inputDoc.getAttribute("/In/Terms/invoice_only_ind");
	   if (value == null)
	   {
		  inputDoc.setAttribute("/In/Terms/invoice_only_ind", TradePortalConstants.INDICATOR_NO);
	   }
	   
	
	
 }
 
}
