package com.ams.tradeportal.mediator.premediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */

import com.amsinc.ecsg.web.*;
import javax.servlet.http.*;
import java.util.*;

import com.amsinc.ecsg.util.*;
import com.ams.tradeportal.common.*;
import java.math.*;
import com.ams.tradeportal.busobj.webbean.*;
import com.ams.tradeportal.busobj.util.*;

public class POUpload_DefPreEdit implements com.amsinc.ecsg.web.WebAction {
private static final Logger LOG = LoggerFactory.getLogger(POUpload_DefPreEdit.class);
/**
 * AdminUserPreEdit constructor comment.
 */
public POUpload_DefPreEdit() {
	super();
}

/**
 * Performs different logic based on the type of transaction (ISS or AMD)
 * 
 * But basically converts the month, day, year fields for each of the 
 * dates into single date fields.  This allows populateFromXml to work.
 *
 * In addition, explicitly sets all 'missing' checkbox fields to N.
 * (Checkbox fields which are not checked are not sent in the document,
 * we must force an N to cause unchecked values to be save correctly.)
 * 
 */
public void act(AmsServletInvocation reqInfo, 
				BeanManager beanMgr, 
				FormManager formMgr, 
				ResourceManager resMgr, 
				String serverLocation, 
				HttpServletRequest request, 
				HttpServletResponse response, 
				Hashtable inputParmMap,
				DocumentHandler inputDoc)
	throws com.amsinc.ecsg.frame.AmsException {

	String value;
	
	// get the input document 
	if (inputDoc == null) {
		LOG.info("Input document to POUpload_DefPreEdit is null");
		return;
	}
	
    doPOUploadDefPreEdit( inputDoc );
}

/**
 * Performs pre-editing of the input document that is specific to the
 * PO Upload Definition DEtail JSP. Also, we need to explicitly set the
 * default checkbox value to N when it is not already present in the document.
 *
 * @param doc com.amsinc.ecsg.util.DocumentHandler
 */
private void doPOUploadDefPreEdit(DocumentHandler inputDoc) {

	// get the input document 
	if (inputDoc == null) {
		LOG.info("Input document to EXP_COLPreEdit is null");
		return;
	}

	// Checkboxes do not send values if the checkbox is unchecked.
	// Therefore, for each checkbox box, determine if we have a
	// value in the document.  If not, add an explicit N value for
	// each one that is missing.  This ensures that checkboxes that
	// are unchecked by the user get reset to N.
	//cquinton 10/21/2013 Rel 8.3 ir#21748 add back reset of indicators
	resetCheckBoxAttribute( inputDoc, "/In/POUploadDefinition/default_flag" );
	resetCheckBoxAttribute( inputDoc, "/In/POUploadDefinition/include_column_header" );
	resetCheckBoxAttribute( inputDoc, "/In/POUploadDefinition/include_footer" );
	resetCheckBoxAttribute( inputDoc, "/In/POUploadDefinition/include_po_text" );
	
	List<String> ruleList = inputDoc.getAttributes("/In/POUploadDefinition/other1_size");
	
			for(int i=0;i<ruleList.size();i++)
		{
				inputDoc.removeAllChildren("/In/POUploadDefinition/other1_size");
				inputDoc.removeComponent("/In/POUploadDefinition/other1_size");
		}
			if(inputDoc.getAttribute("/In/POUploadDefinition/other1_name")!=null){
			inputDoc.setAttribute("/In/POUploadDefinition/other1_size", inputDoc.getAttribute("/In/POUploadDefinition/other1_name"));
		    inputDoc.removeComponent("/In/POUploadDefinition/other1_name");
			}
	}
	

/**
 * We check for a value at a given path in the Xml Doc, if it
 * Does'nt exist then we set that value in the document to 'N'.  This is used for
 * resetting a Check boxes value since deselection of a check box does NOT update the
 * Xml Doc with a revised value.
 *
 * @param DocumentHandler inputDoc - the document to get and set data against.
 * @param String xmlPath           - this the the path to use to get/set
 *                                   attributes in the xml Doc.
 */
private static void resetCheckBoxAttribute( DocumentHandler inputDoc, String xmlPath) {

    String value;

    value = inputDoc.getAttribute( xmlPath );
    if (value == null)
        inputDoc.setAttribute( xmlPath, TradePortalConstants.INDICATOR_NO );
}

}
