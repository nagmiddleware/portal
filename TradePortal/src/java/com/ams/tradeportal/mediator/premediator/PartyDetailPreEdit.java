package com.ams.tradeportal.mediator.premediator;
import java.util.Hashtable;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.ResourceManager;
import com.amsinc.ecsg.util.StringFunction;
/**
 * Premediator for PartyDetail.  Handles Account List.
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
import com.amsinc.ecsg.web.AmsServletInvocation;
import com.amsinc.ecsg.web.BeanManager;
import com.amsinc.ecsg.web.FormManager;

public class PartyDetailPreEdit implements com.amsinc.ecsg.web.WebAction
{
private static final Logger LOG = LoggerFactory.getLogger(PartyDetailPreEdit.class);
   /**
    * PartyDetailPreEdit constructor comment.
    */
   public PartyDetailPreEdit()
   {
      super();
   }

   /**
    *
    */
   public void act(AmsServletInvocation reqInfo,
                   BeanManager beanMgr,
                   FormManager formMgr,
                   ResourceManager resMgr,
                   String serverLocation,
                   HttpServletRequest request,
                   HttpServletResponse response,
                   Hashtable inputParmMap,
                   DocumentHandler inputDoc) throws AmsException
   {

      // Retrieve the input document
      if (inputDoc == null)
      {
         LOG.info("Input document to PartyDetailPreEdit is null");

         return;
      }
	  //Srinivasu_D IR#T36000034559 Rel9.1 - Fishnet fix -removing extra / after decrypting from ui.
	  String desigBankOid = inputDoc.getAttribute("/In/Party/designated_bank_oid");
	 
	  if(StringFunction.isNotBlank(desigBankOid)){
		 int pos = desigBankOid.indexOf("/");
			if(pos>0){
			desigBankOid = desigBankOid.substring(0,pos);	
			inputDoc.setAttribute("/In/Party/designated_bank_oid",desigBankOid);
		   }
	  }
	  //Srinivasu_D IR#T36000034559 Rel9.1 - End
       // Determine whether accounts are deleted or not entered from
	   Vector acctVector = inputDoc.getFragments("/In/Party/AccountList");
	   int numItems = acctVector.size();
	   int iLoop;

	   for (iLoop=0; iLoop<numItems; iLoop++)
	   {
		 DocumentHandler acctDoc = (DocumentHandler) acctVector.elementAt(iLoop);

		 String acctOid = acctDoc.getAttribute("/account_oid");
		 String acctNum = acctDoc.getAttribute("/account_number");
		 String acctCurr = acctDoc.getAttribute("/currency");

		 if (StringFunction.isBlank(acctNum) && StringFunction.isBlank(acctCurr)) {
		    if (StringFunction.isBlank(acctOid)) {
			// Oid, num, and ccy are blank: remove this completely blank component
			// from the input doc.

			inputDoc.removeComponent("/In/Party/AccountList("+iLoop+")");

		    } else {
			// Oid is not blank but the num and ccy are.  The user wants to delete this
			// account.  Remove it from the input doc (so it doesn't get save and generate
			// an error), then add the oid to a new section for processing in the 
			// mediator.

			DocumentHandler deleteDoc = new DocumentHandler();
			deleteDoc.setAttribute("/Name", "Account");
			deleteDoc.setAttribute("/Oid", acctOid);

			inputDoc.removeComponent("/In/Party/AccountList("+iLoop+")");

			inputDoc.addComponent("/In/ComponentToDelete("+iLoop+")", deleteDoc);
		    }
		 } else {
			inputDoc.setAttribute("/In/Party/AccountList("+iLoop+")/owner_object_type", TradePortalConstants.PARTY);
		 }
	   }
  }

}
