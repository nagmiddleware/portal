package com.ams.tradeportal.mediator.premediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */

import com.amsinc.ecsg.web.*;
import javax.servlet.http.*;
import java.util.*;
import com.amsinc.ecsg.util.*;
import com.ams.tradeportal.common.*;
import com.ams.tradeportal.busobj.util.*;

public class FTBAPreEdit extends GenericTransactionPreEdit implements com.amsinc.ecsg.web.WebAction {
private static final Logger LOG = LoggerFactory.getLogger(FTBAPreEdit.class);
	/**
	 * FTRQPreEdit constructor comment.
	 */
	public FTBAPreEdit() {
		super();
	}
	
	/**
	 * Performs different logic based on the type of transaction (ISS or AMD)
	 * 
	 * But basically converts the month, day, year fields for each of the 
	 * dates into single date fields.  This allows populateFromXml to work.
	 *
	 * In addition, explicitly sets all 'missing' checkbox fields to N.
	 * (Checkbox fields which are not checked are not sent in the document,
	 * we must force an N to cause unchecked values to be save correctly.)
	 * 
	 * Also, perform logic to determine the account number and currency 
	 * selected for the beneficiary -- this may come from a radio button
	 * value or an enterable field.
	 */
	public void act(AmsServletInvocation reqInfo, 
			BeanManager beanMgr, 
			FormManager formMgr, 
			ResourceManager resMgr, 
			String serverLocation, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			Hashtable inputParmMap,
			DocumentHandler inputDoc)
	throws com.amsinc.ecsg.frame.AmsException
	{
		
		// get the input document 
		if (inputDoc == null)
		{
			LOG.info("Input document to FTBAPreEdit is null");
			return;
		}
		
		// Convert decimal number from its locale-specific entry format into
		// a standard format that can be understood by the database.
		// If the format is invalid, an error value will be placed into
		// the XML that will trigger an error later.
		// In the corresponding post mediator action, the passed-in value
		// is placed back into its original location.
		doDecimalPreMediatorAction(inputDoc, resMgr, "amount");
		
	    inputDoc.setAttribute("/In/Terms/reference_number", inputDoc.getAttribute("/In/Terms/transfer_description"));
		
	
		// Need to convert the release date fields into a single date value.
		
	    //String day   = inputDoc.getAttribute("/In/Transaction/transfer_day");
		//String month = inputDoc.getAttribute("/In/Transaction/transfer_month");
		//String year  = inputDoc.getAttribute("/In/Transaction/transfer_year");
		
		
		//TP REFRESH CHANGE
	    

	    String[] tempExpDate;
		String date;
		String newDate;
		
		
		date  = inputDoc.getAttribute("/In/Transaction/payment_date");
		newDate = TPDateTimeUtility.convertISODateToJPylonDate(date);
		
		
	     inputDoc.setAttribute("/In/Transaction/payment_date", newDate);
		
	   // END TPREFRESH CHANGE  
	     /*
	     
	     // DK CR-640 Rel7.1
		if (inputDoc.getAttribute("/In/Terms/request_market_rate_ind") == null)
		{
			inputDoc.setAttribute("/In/Terms/request_market_rate_ind", TradePortalConstants.INDICATOR_NO);
		}
		*/
	} 
}