package com.ams.tradeportal.mediator.premediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ams.tradeportal.common.SQLParamFilter;
import com.ams.tradeportal.common.TPDateTimeUtility;
import com.ams.tradeportal.common.TradePortalConstants;
import com.ams.tradeportal.common.TransactionType;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.frame.MediatorServices;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.ResourceManager;
import com.amsinc.ecsg.util.StringFunction;
import com.amsinc.ecsg.web.AmsServletInvocation;
import com.amsinc.ecsg.web.BeanManager;
import com.amsinc.ecsg.web.FormManager;

public class IMP_DLCPreEdit extends GenericTransactionPreEdit implements com.amsinc.ecsg.web.WebAction {
private static final Logger LOG = LoggerFactory.getLogger(IMP_DLCPreEdit.class);
/**
 * AdminUserPreEdit constructor comment.
 */
public IMP_DLCPreEdit() {
	super();
}

/**
 * Performs different logic based on the type of transaction (ISS or AMD)
 *
 * But basically converts the month, day, year fields for each of the
 * dates into single date fields.  This allows populateFromXml to work.
 *
 * In addition, explicitly sets all 'missing' checkbox fields to N.
 * (Checkbox fields which are not checked are not sent in the document,
 * we must force an N to cause unchecked values to be save correctly.)
 *
 */
 public void act(AmsServletInvocation reqInfo,
				BeanManager beanMgr,
				FormManager formMgr,
				ResourceManager resMgr,
				String serverLocation,
				HttpServletRequest request,
				HttpServletResponse response,
				Hashtable inputParmMap,
				DocumentHandler inputDoc)
			throws com.amsinc.ecsg.frame.AmsException
 {
	String value;

	// get the input document
	if (inputDoc == null)
	{
	   LOG.info("Input document to IMP_DLCPreEdit is null");
	   return;
	}

        // Convert decimal number from its locale-specific entry format into
        // a standard format that can be understood by the database.
        // If the format is invalid, an error value will be placed into
        // the XML that will trigger an error later.
        // In the corresponding post mediator action, the passed-in value
        // is placed back into its original location.
        doDecimalPreMediatorAction(inputDoc, resMgr, "amount");

	value = inputDoc.getAttribute("/In/Transaction/transaction_type_code");
	if (TransactionType.AMEND.equals(value))
	{
	   doAMDPreEdit(inputDoc, resMgr);
	}
	else
	{
	   doISSPreEdit(inputDoc, resMgr);
	}
 }

/**
 * Handles pre-edits for the AMD type transactions.  This includes
 * converting 3part date field into single date values and then to
 * check the increase/decrease flag and reset the amount to a negative
 * value if the flag is decrease.
 *
 * @param doc com.amsinc.ecsg.util.DocumentHandler
 */
 private void doAMDPreEdit(DocumentHandler inputDoc, ResourceManager resMgr)
 {

	String newExpDate;
	String newshipDate;
	// get the input document
	if (inputDoc == null)
	{
	   LOG.info("Input document to IMP_DLCPreEdit is null");
	   return;
	}

	newExpDate = inputDoc.getAttribute("/In/Terms/expiry_date");
	newExpDate = TPDateTimeUtility.convertISODateToJPylonDate(newExpDate);
	inputDoc.setAttribute("/In/Terms/expiry_date", newExpDate);

	newshipDate = inputDoc.getAttribute("/In/Terms/ShipmentTermsList/latest_shipment_date");
	newshipDate = TPDateTimeUtility.convertISODateToJPylonDate(newshipDate);
	inputDoc.setAttribute("/In/Terms/ShipmentTermsList/latest_shipment_date", newshipDate);
 }
	

/**
 * Performs pre-editing of the input document that is specific to ISS
 * transactions.  This includes converting the three part date fields
 * into a single date values and explicitly setting checkbox values
 * to N when they are not already present in the document.
 *
 * @param doc com.amsinc.ecsg.util.DocumentHandler
 */
 private void doISSPreEdit(DocumentHandler inputDoc, ResourceManager resMgr)
 {
	
	String newDate;
	String value;
	String date;

	// get the input document
	if (inputDoc == null)
	{
	   LOG.info("Input document to IMP_DLCPreEdit is null");
	   return;
	}

        // Convert decimal numbers from their locale-specific entry format into
        // a standard format that can be understood by the database.
        // If the format is invalid, an error value will be placed into
        // the XML that will trigger an error later.
        // In the corresponding post mediator action, the passed-in value
        // is placed back into its original location.
        doDecimalPreMediatorAction(inputDoc, resMgr, "fec_rate");
        doDecimalPreMediatorAction(inputDoc, resMgr, "fec_amount");
        doDecimalPreMediatorAction(inputDoc, resMgr, "insurance_endorse_value_plus");

        date = inputDoc.getAttribute("/In/Terms/expiry_date");
    	newDate = TPDateTimeUtility.convertISODateToJPylonDate(date);
    	inputDoc.setAttribute("/In/Terms/expiry_date", newDate);
	   
	   // Checkboxes do not send values if the checkbox is unchecked.
	   // Therefore, for each checkbox box, determine if we have a
	   // value in the document.  If not, add an explicit N value for
	   // each one that is missing.  This ensures that checkboxes that
	   // are unchecked by the user get reset to N.
	   value = inputDoc.getAttribute("/In/Terms/comm_invoice_indicator");
	   if (value == null)
	   {
		  inputDoc.setAttribute("/In/Terms/comm_invoice_indicator", TradePortalConstants.INDICATOR_NO);
	   }
	   value = inputDoc.getAttribute("/In/Terms/packing_list_indicator");
	   if (value == null)
	   {
		  inputDoc.setAttribute("/In/Terms/packing_list_indicator", TradePortalConstants.INDICATOR_NO);
	   }
	   value = inputDoc.getAttribute("/In/Terms/cert_origin_indicator");
	   if (value == null)
	   {
		  inputDoc.setAttribute("/In/Terms/cert_origin_indicator", TradePortalConstants.INDICATOR_NO);
	   }
	   value = inputDoc.getAttribute("/In/Terms/ins_policy_indicator");
	   if (value == null)
	   {
		  inputDoc.setAttribute("/In/Terms/ins_policy_indicator", TradePortalConstants.INDICATOR_NO);
	   }

	   value = inputDoc.getAttribute("/In/Terms/other_req_doc_1_indicator");
	   if (value == null)
	   {
		  inputDoc.setAttribute("/In/Terms/other_req_doc_1_indicator", TradePortalConstants.INDICATOR_NO);
	   }
	   value = inputDoc.getAttribute("/In/Terms/other_req_doc_2_indicator");
	   if (value == null)
	   {
		  inputDoc.setAttribute("/In/Terms/other_req_doc_2_indicator", TradePortalConstants.INDICATOR_NO);
	   }
	   value = inputDoc.getAttribute("/In/Terms/other_req_doc_3_indicator");
	   if (value == null)
	   {
		  inputDoc.setAttribute("/In/Terms/other_req_doc_3_indicator", TradePortalConstants.INDICATOR_NO);
	   }
	   value = inputDoc.getAttribute("/In/Terms/other_req_doc_4_indicator");
	   if (value == null)
	   {
		  inputDoc.setAttribute("/In/Terms/other_req_doc_4_indicator", TradePortalConstants.INDICATOR_NO);
	   }

	   value = inputDoc.getAttribute("/In/Terms/addl_doc_indicator");
	   if (value == null)
	   {
		  inputDoc.setAttribute("/In/Terms/addl_doc_indicator", TradePortalConstants.INDICATOR_NO);
	   }

	    date = inputDoc.getAttribute("/In/Terms/ShipmentTermsList/latest_shipment_date");
		newDate = TPDateTimeUtility.convertISODateToJPylonDate(date);
		inputDoc.setAttribute("/In/Terms/ShipmentTermsList/latest_shipment_date", newDate);

	
		value = inputDoc.getAttribute("/In/Terms/percent_amount_dis_ind");
		
		String paymentType=inputDoc.getAttribute("/In/Terms/payment_type");
		int pmtTermsRowCount=inputDoc.getFragmentsList("/In/Terms/PmtTermsTenorDtlList").size();
		StringBuilder pmtTermsOidStr = new StringBuilder();
		StringBuilder deleteSql = new StringBuilder();
		List<Object> sqlPrmLst = new ArrayList<Object>();
		deleteSql.append("delete from pmt_terms_tenor_dtl where p_terms_oid = ? ");
		sqlPrmLst.add(inputDoc.getAttribute("/In/Terms/terms_oid"));
		if ("SPEC".equals(paymentType)){
			for (int i = 0; i < pmtTermsRowCount; i++){
				inputDoc.removeComponent("/In/Terms/PmtTermsTenorDtlList(" + i +")");
			}
		}else if("PAYM".equals(paymentType)){
			
			for (int i = 1; i < pmtTermsRowCount; i++){
				inputDoc.removeComponent("/In/Terms/PmtTermsTenorDtlList(" + i +")");
			}
			String pmtTermsOid = inputDoc.getAttribute("/In/Terms/PmtTermsTenorDtlList("+0+")/pmt_terms_tenor_dtl_oid");
			if (StringFunction.isNotBlank(pmtTermsOid)) {
				deleteSql.append(" and pmt_terms_tenor_dtl_oid not in (?) ");
				sqlPrmLst.add(pmtTermsOid);
			}
			
			inputDoc.setAttribute("/In/Terms/PmtTermsTenorDtlList("+0+")/percent", "100");
			inputDoc.setAttribute("/In/Terms/PmtTermsTenorDtlList("+0+")/tenor_type", "SGT");
			inputDoc.setAttribute("/In/Terms/PmtTermsTenorDtlList("+0+")/amount", null);
			inputDoc.setAttribute("/In/Terms/PmtTermsTenorDtlList("+0+")/num_days_after", null);
			inputDoc.setAttribute("/In/Terms/PmtTermsTenorDtlList("+0+")/days_after_type", null);
			inputDoc.setAttribute("/In/Terms/PmtTermsTenorDtlList("+0+")/maturity_date", null);
			inputDoc.setAttribute("/In/Terms/special_tenor_text", null);
		}else{
			inputDoc.setAttribute("/In/Terms/special_tenor_text", null);
		List pmtTermsOidList = new ArrayList();
		String termsAmount=inputDoc.getAttribute("/In/Terms/amount");
		BigDecimal instrumentAmount = BigDecimal.valueOf(
											new Double(
													StringFunction.isNotBlank(termsAmount)? termsAmount:"0"));
		BigDecimal totalPmtAmount = BigDecimal.ZERO;

		for(int iLoop=0; iLoop<pmtTermsRowCount; iLoop++){
			date = inputDoc.getAttribute("/In/Terms/PmtTermsTenorDtlList("+iLoop+")/maturity_date");
			newDate = TPDateTimeUtility.convertISODateToJPylonDate(date);
			inputDoc.setAttribute("/In/Terms/PmtTermsTenorDtlList("+iLoop+")/maturity_date", newDate);
			if("A".equals(value)){
				inputDoc.setAttribute("/In/Terms/PmtTermsTenorDtlList("+iLoop+")/amount", 
						inputDoc.getAttribute("/In/Terms/PmtTermsTenorDtlList("+iLoop+")/percent"));
				inputDoc.setAttribute("/In/Terms/PmtTermsTenorDtlList("+iLoop+")/percent", null);
				 String amountStr=inputDoc.getAttribute("/In/Terms/PmtTermsTenorDtlList("+iLoop+")/amount");
                 if(StringFunction.isNotBlank(amountStr))
                                   totalPmtAmount = totalPmtAmount.add(new BigDecimal(amountStr));
			}

			else {
				inputDoc.setAttribute("/In/Terms/PmtTermsTenorDtlList("+iLoop+")/amount", null);
			}

			String percent=inputDoc.getAttribute("/In/Terms/PmtTermsTenorDtlList("+iLoop+")/percent") ;
			String amount= inputDoc.getAttribute("/In/Terms/PmtTermsTenorDtlList("+iLoop+")/amount") ;

			 if( ( StringFunction.isBlank(percent) ||
							(StringFunction.isNotBlank(percent) && "0".equals(percent.trim()) ))
					&& (StringFunction.isBlank(amount) ||
							(StringFunction.isNotBlank(amount) && "0".equals(amount.trim()) ))
					&& StringFunction.isBlank(inputDoc.getAttribute("/In/Terms/PmtTermsTenorDtlList("+iLoop+")/tenor_type"))
					&& StringFunction.isBlank(inputDoc.getAttribute("/In/Terms/PmtTermsTenorDtlList("+iLoop+")/num_days_after"))
					&& StringFunction.isBlank(inputDoc.getAttribute("/In/Terms/PmtTermsTenorDtlList("+iLoop+")/days_after_type") )
					&& StringFunction.isBlank(inputDoc.getAttribute("/In/Terms/PmtTermsTenorDtlList("+iLoop+")/maturity_date"))
				){
				inputDoc.removeAllChildren("/In/Terms/PmtTermsTenorDtlList("+iLoop+")");
				inputDoc.removeComponent("/In/Terms/PmtTermsTenorDtlList("+iLoop+")");
				}
				
				else{
					pmtTermsOidList.add(inputDoc.getAttribute("/In/Terms/PmtTermsTenorDtlList("+iLoop+")/pmt_terms_tenor_dtl_oid"));
					
					String pmtTermsOid = inputDoc.getAttribute("/In/Terms/PmtTermsTenorDtlList("+iLoop+")/pmt_terms_tenor_dtl_oid");
					if (StringFunction.isNotBlank(pmtTermsOid)) {
						pmtTermsOidStr.append(pmtTermsOid);
						pmtTermsOidStr.append(",");
					}
					
				}
				
		}
		
		if ( pmtTermsOidStr.length()>0 ) {
		 deleteSql.append(" and pmt_terms_tenor_dtl_oid not in (");
		 String placeHolderStr = SQLParamFilter.preparePlaceHolderStrForInClause(pmtTermsOidStr.substring(0,pmtTermsOidStr.length()-1), sqlPrmLst);
		 deleteSql.append(placeHolderStr);
		 deleteSql.append(" ) ");
		}
		
		 if ( "A".equals(value) &&  totalPmtAmount.compareTo(instrumentAmount)!=0){
		    //�Total of all Tenor Amounts do not equal Instrument Amount�
			 MediatorServices medService = new MediatorServices();
				try{
				  medService.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,TradePortalConstants.PMT_AMOUNT_TOT_IS_NOTEQUAL_TO_INSTRUMENT_AMOUNT);
				  medService.addErrorInfo();
			      inputDoc.setComponent ("/Error", medService.getErrorDoc ());
				  return;
				  }catch(AmsException amsEx){
					  amsEx.printStackTrace();  
				  }
		}
		
		}
		 	//Kiran IR#T36000017643 Rel-8.2 06/05/2013 Start
			//Added to fix the Issue with Available by Mixed/Negotiation Payment Terms where the Add 2 More Lines button is not working.
	 		try(Connection con = DatabaseQueryBean.connect(false);
	 			PreparedStatement pStmt = con.prepareStatement(deleteSql.toString())) {
	 			
	 			Object [] sqlPrmArray = sqlPrmLst.toArray();
	 			for (int i=0; i<sqlPrmArray.length; i++) {
					if (sqlPrmArray[i] != null) {
						pStmt.setObject(i + 1, sqlPrmArray[i]);
					} else {
						pStmt.setNull(i + 1, Types.OTHER);
					}
				}
	 			con.setAutoCommit (false);
	 			pStmt.executeUpdate();
	 			con.commit();
	 			sqlPrmArray = null;

	 		}catch (Exception ex) {
	            //because this is not vital to processing, ignore it but create a log message
	            System.err.println("Exception removing authentication log entry: " + ex.toString());
			}
	 		
	 /*KMehta - 11 Feb 2015 - Rel9.2 IR-T36000033161 - Add  - Begin*/

 		StringBuilder addlReqDocOidStr = new StringBuilder();
 		StringBuilder deleteSqlAddReqDoc = new StringBuilder();
 		sqlPrmLst = new ArrayList<Object>();
    	deleteSqlAddReqDoc.append("delete from addtional_req_docs where p_terms_oid = ? ");
    	sqlPrmLst.add(inputDoc.getAttribute("/In/Terms/terms_oid"));
		int addDocRowCount=inputDoc.getFragmentsList("/In/Terms/AdditionalReqDocList").size();
		List addReqDocOidList = new ArrayList();
		
		for(int iLoop=0; iLoop<addDocRowCount; iLoop++){
		
			if( StringFunction.isBlank(inputDoc.getAttribute("/In/Terms/AdditionalReqDocList("+iLoop+")/addl_req_doc_name"))
					&& StringFunction.isBlank(inputDoc.getAttribute("/In/Terms/AdditionalReqDocList("+iLoop+")/addl_req_doc_originals"))
					&& StringFunction.isBlank(inputDoc.getAttribute("/In/Terms/AdditionalReqDocList("+iLoop+")/addl_req_doc_copies")) 
					&& StringFunction.isBlank(inputDoc.getAttribute("/In/Terms/AdditionalReqDocList("+iLoop+")/addl_req_doc_text"))
				){    	
			    	LOG.info("AdditionalReqDocList Input document to IMP_DLCPreEdit is null" );
			    	inputDoc.removeAllChildren("/In/Terms/AdditionalReqDocList("+iLoop+")");
				    inputDoc.removeComponent("/In/Terms/AdditionalReqDocList("+iLoop+")");
			}
			else{
				addReqDocOidList.add(inputDoc.getAttribute("/In/Terms/AdditionalReqDocList("+iLoop+")/addl_req_doc_oid"));
						
						String addlReqDocOid = inputDoc.getAttribute("/In/Terms/AdditionalReqDocList("+iLoop+")/addl_req_doc_oid");
						if (StringFunction.isNotBlank(addlReqDocOid)) {
							addlReqDocOidStr.append(addlReqDocOid);
							addlReqDocOidStr.append(",");
						}
			}	
		}
		
	    if ( addlReqDocOidStr.length()>0 ) {
	    	deleteSqlAddReqDoc.append(" and addl_req_doc_oid not in (");
	    	String placeHolderStr = SQLParamFilter.preparePlaceHolderStrForInClause(addlReqDocOidStr.substring(0,addlReqDocOidStr.length()-1), sqlPrmLst);
	    	deleteSqlAddReqDoc.append(placeHolderStr);
	    	deleteSqlAddReqDoc.append(" ) ");
			}

		try(Connection conn1 = DatabaseQueryBean.connect(false);
 			PreparedStatement pStmt1 = conn1.prepareStatement(deleteSqlAddReqDoc.toString())) {
 			
 			Object [] sqlPrmArray = sqlPrmLst.toArray();
 			for (int i=0; i<sqlPrmArray.length; i++) {
				if (sqlPrmArray[i] != null) {
					pStmt1.setObject(i + 1, sqlPrmArray[i]);
				} else {
					pStmt1.setNull(i + 1, Types.OTHER);
				}
			}
 			conn1.setAutoCommit (false);
 			pStmt1.executeUpdate();
 			conn1.commit();
 			sqlPrmArray = null;

 		}catch (Exception ex) {
            //because this is not vital to processing, ignore it but create a log message
            System.err.println("Exception removing authentication log entry: " + ex.toString());
		}

		 	   
	/*KMehta - 11 Feb 2015 - Rel9.2 IR-T36000033161 - Add  - End*/
	
	   // Checkboxes do not send values if the checkbox is unchecked.
	   // Therefore, for each checkbox box, determine if we have a
	   // value in the document.  If not, add an explicit N value for
	   // each one that is missing.  This ensures that checkboxes that
	   // are unchecked by the user get reset to N.
	   value = inputDoc.getAttribute("/In/Terms/ShipmentTermsList/trans_doc_included");
	   if (value == null)
	   {
		  inputDoc.setAttribute("/In/Terms/ShipmentTermsList/trans_doc_included", TradePortalConstants.INDICATOR_NO);
	   }
	   value = inputDoc.getAttribute("/In/Terms/ShipmentTermsList/trans_addl_doc_included");
	   if (value == null)
	   {
		  inputDoc.setAttribute("/In/Terms/ShipmentTermsList/trans_addl_doc_included", TradePortalConstants.INDICATOR_NO);
	   }

	   value = inputDoc.getAttribute("/In/Terms/ShipmentTermsList/transshipment_allowed");
	   if (value == null)
	   {
		  inputDoc.setAttribute("/In/Terms/ShipmentTermsList/transshipment_allowed", TradePortalConstants.INDICATOR_NO);
	   }

	   value = inputDoc.getAttribute("/In/Terms/partial_shipment_allowed");
	   if (value == null)
	   {
		  inputDoc.setAttribute("/In/Terms/partial_shipment_allowed", TradePortalConstants.INDICATOR_NO);
	   }
	      
	    date = inputDoc.getAttribute("/In/Terms/fec_maturity_date");
		newDate = TPDateTimeUtility.convertISODateToJPylonDate(date);
		inputDoc.setAttribute("/In/Terms/fec_maturity_date", newDate);

	   

	   // Checkboxes do not send values if the checkbox is unchecked.
	   // Therefore, for each checkbox box, determine if we have a
	   // value in the document.  If not, add an explicit N value for
	   // each one that is missing.  This ensures that checkboxes that
	   // are unchecked by the user get reset to N.
	   value = inputDoc.getAttribute("/In/Terms/transferrable");
	   if (value == null)
	   {
		  inputDoc.setAttribute("/In/Terms/transferrable", TradePortalConstants.INDICATOR_NO);
	   }
	   value = inputDoc.getAttribute("/In/Terms/revolve");
	   if (value == null)
	   {
		  inputDoc.setAttribute("/In/Terms/revolve", TradePortalConstants.INDICATOR_NO);
	   }
	   value = inputDoc.getAttribute("/In/Terms/finance_drawing");
	   if (value == null)
	   {
		  inputDoc.setAttribute("/In/Terms/finance_drawing", TradePortalConstants.INDICATOR_NO);
	   }
	   value = inputDoc.getAttribute("/In/Terms/drafts_required");
	   if (value == null)
	   {
		  inputDoc.setAttribute("/In/Terms/drafts_required", TradePortalConstants.INDICATOR_NO);
	   }
	   value = inputDoc.getAttribute("/In/Terms/ucp_version_details_ind");
	   if (value == null)
	   {
		  inputDoc.setAttribute("/In/Terms/ucp_version_details_ind", TradePortalConstants.INDICATOR_NO);
	   }
	   value = inputDoc.getAttribute("/In/Terms/irrevocable");
	   if (value == null)
	   {
		  inputDoc.setAttribute("/In/Terms/irrevocable", TradePortalConstants.INDICATOR_NO);
	   }
	   value = inputDoc.getAttribute("/In/Terms/operative");
	   if (value == null)
	   {
		  inputDoc.setAttribute("/In/Terms/operative", TradePortalConstants.INDICATOR_NO);
	   }

	   //cquinton 10/16/2013 ir#21872 Rel 8.3 start
	   //when ucp version is not 'OTHER', ucp version details should be blanked out
       value = inputDoc.getAttribute("/In/Terms/ucp_version");
       if ( !TradePortalConstants.IMP_OTHER.equals(value) ) {
          inputDoc.setAttribute("/In/Terms/ucp_details", "");
       }
       //cquinton 10/16/2013 ir#21872 Rel 8.3 end
       
       //MEer CR-1026
       value = inputDoc.getAttribute("/In/Terms/tt_reimbursement_allow_ind");
	   if (value == null)
	   {
		  inputDoc.setAttribute("/In/Terms/tt_reimbursement_allow_ind", TradePortalConstants.INDICATOR_NO);
	   }
	   
 }

}