package com.ams.tradeportal.mediator.premediator;
import java.util.Hashtable;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.common.TPDateTimeUtility;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.ResourceManager;
import com.amsinc.ecsg.util.StringFunction;
/**
 *
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
import com.amsinc.ecsg.web.AmsServletInvocation;
import com.amsinc.ecsg.web.BeanManager;
import com.amsinc.ecsg.web.FormManager;

public class AnnouncementPreEdit extends GenericTransactionPreEdit implements com.amsinc.ecsg.web.WebAction {
private static final Logger LOG = LoggerFactory.getLogger(AnnouncementPreEdit.class);
	/**
	 * FTDPPreEdit constructor comment.
	 */
	
	public static final String DRAFT = "DRAFT";
	public static final String ACTIVE = "ACTIVE";
	public static final String DEACTIVE = "DEACTIVATED";
	
	public AnnouncementPreEdit() {
		super();
	}

	/**
	 * Performs different logic based on the type of transaction (ISS or AMD)
	 *
	 * But basically converts the month, day, year fields for each of the
	 * dates into single date fields.  This allows populateFromXml to work.
	 *
	 * In addition, explicitly sets all 'missing' checkbox fields to N.
	 * (Checkbox fields which are not checked are not sent in the document,
	 * we must force an N to cause unchecked values to be save correctly.)
	 *
	 * Also, perform logic to determine the account number and currency
	 * selected for the beneficiary -- this may come from a radio button
	 * value or an enterable field.
	 */
	public void act(AmsServletInvocation reqInfo,
			BeanManager beanMgr,
			FormManager formMgr,
			ResourceManager resMgr,
			String serverLocation,
			HttpServletRequest request,
			HttpServletResponse response,
			Hashtable inputParmMap,
			DocumentHandler inputDoc)
	throws com.amsinc.ecsg.frame.AmsException
	{
		// get the input document
		if (inputDoc == null)
		{
			LOG.info("Input document to AnnouncementPreEdit is null");
			return;
		}


		doPreEdit(inputDoc, resMgr);
	}

	/**
	 * Performs pre-editing of the input document that is specific to ISS
	 * transactions.  This includes converting the three part date fields
	 * into a single date values and explicitly setting checkbox values
	 * to N when they are not already present in the document.
	 *
	 * @param doc com.amsinc.ecsg.util.DocumentHandler
	 */
	private void doPreEdit(DocumentHandler inputDoc, ResourceManager resMgr)
	{

		// get the input document
		if (inputDoc == null)
		{
			LOG.info("Input document to AnnouncementPreEdit is null");
			return;
		}
		
		LOG.debug("AnnouncementPreEdit::doPreEdit: " + inputDoc.getAttribute("/In/Announcement/start_date"));
		String start_date = inputDoc.getAttribute("/In/Announcement/start_date");
		start_date = TPDateTimeUtility.convertISODateToJPylonDateWithoutTime(start_date);
		LOG.info("start_date="+start_date);
		if ((start_date == null) || (start_date.indexOf("-1") != -1))
		{
			inputDoc.setAttribute("/In/Announcement/start_date", "");
		} else {
			inputDoc.setAttribute("/In/Announcement/start_date", start_date);
		}

        
		LOG.debug("AnnouncementPreEdit::doPreEdit: " + inputDoc.getAttribute("/In/Announcement/end_date"));
		String end_date = inputDoc.getAttribute("/In/Announcement/end_date");
		end_date = TPDateTimeUtility.convertISODateToJPylonDateWithoutTime(end_date);
		LOG.info("end_date="+end_date);
		if ((end_date == null) || (end_date.indexOf("-1") != -1))
		{
			inputDoc.setAttribute("/In/Announcement/end_date", "");
		} else {
			inputDoc.setAttribute("/In/Announcement/end_date", end_date);
		}
			
		 // Determine whether accounts are deleted or not entered from
		   Vector acctVector = inputDoc.getFragments("/In/Announcement/AnnouncementBankGroupList");
		   int numItems = acctVector.size();
		   int iLoop;

		   for (iLoop=0; iLoop<numItems; iLoop++)
		   {
			 DocumentHandler acctDoc = (DocumentHandler) acctVector.elementAt(iLoop);

			 String bog_oid = acctDoc.getAttribute("/bog_oid");
			 String groupChecked = acctDoc.getAttribute("/groupChecked");
			 inputDoc.removeComponent("/In/Announcement/AnnouncementBankGroupList("+iLoop+")/groupChecked");
			 if (!StringFunction.isBlank(bog_oid)) {
			    if (StringFunction.isBlank(groupChecked)){
				// This bank group should be updated
			    } else if (!StringFunction.isBlank(groupChecked) && 
			    		groupChecked.equalsIgnoreCase("N")) {
			    }
			 }

		}  
		   LOG.info("buttonPressed ="+inputDoc.getAttribute("/In/Update/ButtonPressed"));
		  // set status depending upon button pressed  
		   String buttonPressed = inputDoc.getAttribute("/In/Update/ButtonPressed");
		   if (TradePortalConstants.BUTTON_SAVEASDRAFT.equalsIgnoreCase(buttonPressed)) {
			   inputDoc.setAttribute("/In/Announcement/announcement_status", DRAFT);   
		   } else if (TradePortalConstants.BUTTON_DEACTIVATE.equalsIgnoreCase(buttonPressed)) {
			   inputDoc.setAttribute("/In/Announcement/announcement_status", DEACTIVE);
		   } else if ((TradePortalConstants.BUTTON_SAVETRANS.equalsIgnoreCase(buttonPressed) ||
				   TradePortalConstants.BUTTON_SAVEANDCLOSE.equalsIgnoreCase(buttonPressed)) && 
				   //IR#T36000038731 pgedupudi - Rel-9.3 CR-976A 06/18/2015
				   !(inputDoc.getAttribute("/In/Announcement/announcement_status").equalsIgnoreCase(DEACTIVE))) {
			   inputDoc.setAttribute("/In/Announcement/announcement_status", ACTIVE);
		   }
		   
	}
}