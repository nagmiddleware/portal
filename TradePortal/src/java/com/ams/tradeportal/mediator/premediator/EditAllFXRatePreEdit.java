package com.ams.tradeportal.mediator.premediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */

import com.amsinc.ecsg.web.*;
import javax.servlet.http.*;
import java.util.*;
import com.amsinc.ecsg.util.*;
import com.amsinc.ecsg.frame.*;
import com.ams.tradeportal.common.*;
import com.ams.tradeportal.busobj.util.*;

public class EditAllFXRatePreEdit implements com.amsinc.ecsg.web.WebAction {
private static final Logger LOG = LoggerFactory.getLogger(EditAllFXRatePreEdit.class);

/**
 */
 public void act(AmsServletInvocation reqInfo,
				BeanManager beanMgr,
				FormManager formMgr,
				ResourceManager resMgr,
				String serverLocation,
				HttpServletRequest request,
				HttpServletResponse response,
				Hashtable inputParmMap,
				DocumentHandler inputDoc)
			throws com.amsinc.ecsg.frame.AmsException
 {
        // Convert decimal numbers from their locale-specific entry format into
        // a standard format that can be understood by the database.
        // If the format is invalid, an error value will be placed into
        // the XML that will trigger an error later.
        // In the corresponding post mediator action, the passed-in value
        // is placed back into its original location.


      Vector fxRateList = inputDoc.getFragments("/In/FXRate");

      int numberOfFxRates = fxRateList.size();

         // For each FX rate being updated, instantiate the appropriate business object and
         // save all attributes
         for (int i = 0; i < numberOfFxRates; i++)
         {
             if(inputDoc.getAttribute("/In/FXRate("+i+")/rate") != null)
                inputDoc.setAttribute("/In/FXRate("+i+")/userEnteredRate", inputDoc.getAttribute("/In/FXRate("+i+")/rate"));
             else
                inputDoc.setAttribute("/In/FXRate("+i+")/userEnteredRate", "");

             String amount;

            try
             {
              // Convert the amount from its localized form to standard (en_US) form
              amount =  NumberValidator.getNonInternationalizedValue(inputDoc.getAttribute("/In/FXRate("+i+")/rate"), resMgr.getResourceLocale(), true);
             }
            catch(InvalidAttributeValueException iae)
             {
               // If the format was invalid, set amount equal to something that will trigger an error
               // to be issued.
               amount = TradePortalConstants.BAD_AMOUNT;
             }

            // Place the result of the above code into the amount location
            // This is what will be populated into the business object.
            inputDoc.setAttribute("/In/FXRate("+i+")/rate", amount);

             //CM IAZ 10/14/08 Begin
             if(inputDoc.getAttribute("/In/FXRate("+i+")/buy_rate") != null)
                inputDoc.setAttribute("/In/FXRate("+i+")/userEnteredBuyRate", inputDoc.getAttribute("/In/FXRate("+i+")/buy_rate"));
             else
                inputDoc.setAttribute("/In/FXRate("+i+")/userEnteredBuyRate", "");

            try
             {
              // Convert the amount from its localized form to standard (en_US) form
              amount =  NumberValidator.getNonInternationalizedValue(inputDoc.getAttribute("/In/FXRate("+i+")/buy_rate"), resMgr.getResourceLocale(), true);
             }
            catch(InvalidAttributeValueException iae)
             {
               // If the format was invalid, set amount equal to something that will trigger an error
               // to be issued.
               amount = TradePortalConstants.BAD_AMOUNT;
             }

            // Place the result of the above code into the amount location
            // This is what will be populated into the business object.
            inputDoc.setAttribute("/In/FXRate("+i+")/buy_rate", amount);

             if(inputDoc.getAttribute("/In/FXRate("+i+")/sell_rate") != null)
                inputDoc.setAttribute("/In/FXRate("+i+")/userEnteredSellRate", inputDoc.getAttribute("/In/FXRate("+i+")/sell_rate"));
             else
                inputDoc.setAttribute("/In/FXRate("+i+")/userEnteredSellRate", "");

            try
             {
              // Convert the amount from its localized form to standard (en_US) form
              amount =  NumberValidator.getNonInternationalizedValue(inputDoc.getAttribute("/In/FXRate("+i+")/sell_rate"), resMgr.getResourceLocale(), true);
             }
            catch(InvalidAttributeValueException iae)
             {
               // If the format was invalid, set amount equal to something that will trigger an error
               // to be issued.
               amount = TradePortalConstants.BAD_AMOUNT;
             }

            // Place the result of the above code into the amount location
            // This is what will be populated into the business object.
            inputDoc.setAttribute("/In/FXRate("+i+")/sell_rate", amount);
            //CM IAZ 10/14/08 Begin

         }
 }

}