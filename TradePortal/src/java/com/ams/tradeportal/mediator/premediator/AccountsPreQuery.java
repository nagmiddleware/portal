package com.ams.tradeportal.mediator.premediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 *     Copyright  � 2004                        
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */

import java.util.HashMap;
import java.util.Hashtable;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ams.tradeportal.busobj.webbean.SessionWebBean;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.ResourceManager;
import com.amsinc.ecsg.web.AmsServletInvocation;
import com.amsinc.ecsg.web.BeanManager;
import com.amsinc.ecsg.web.FormManager;

public class AccountsPreQuery implements com.amsinc.ecsg.web.WebAction
{
private static final Logger LOG = LoggerFactory.getLogger(AccountsPreQuery.class);
	private static HashMap includeInParameterMap = null;
	
	static {   //parameters to include in request
		includeInParameterMap = new HashMap();
		includeInParameterMap.put("mediator", "AccountsQueryMediator");
		includeInParameterMap.put("preMedWebAction", "com.ams.tradeportal.mediator.premediator.AccountsPreQuery");
	}
	
	
	
   /**
    * AccountsPreQuery constructor comment.
    */
   public AccountsPreQuery()
   {
      super();
   }

   /**
    * 
    */
   public void act(AmsServletInvocation reqInfo, 
                   BeanManager beanMgr, 
                   FormManager formMgr, 
                   ResourceManager resMgr, 
                   String serverLocation, 
                   HttpServletRequest request, 
                   HttpServletResponse response, 
                   Hashtable inputParmMap,
                   DocumentHandler inputDoc) throws AmsException
   {
	   
	  if (request.getParameter("mediator") == null) {
		  //set mediators 
		  request.setAttribute(TradePortalConstants.INCLUDE_IN_PARAMETER, includeInParameterMap);
		  return;
	  }
	  
	  
      // Retrieve the input document 
      if (inputDoc == null)
      {
         LOG.info("Input document to AccountsPreQuery is null");
         return;
      }
      SessionWebBean sessionWebBean = (SessionWebBean)request.getSession().getAttribute("userSession");
      //set current userOid
      inputDoc.setAttribute("/In/userOid",sessionWebBean.getUserOid());
      inputDoc.setAttribute("/In/ownerOrgOid",sessionWebBean.getOwnerOrgOid());

  }
}
