package com.ams.tradeportal.mediator.premediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import com.amsinc.ecsg.util.*;
import com.amsinc.ecsg.frame.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.util.*;
import com.amsinc.ecsg.web.*;
import com.ams.tradeportal.busobj.webbean.*;
import com.ams.tradeportal.busobj.util.*;

/*
 * @(#)MapAuthorizeXml

Copies Xml from inputDoc produced by transactions detail
page to xml format acceptable by AuthorizeTransactionsMediator

 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
*/


public class MapAuthorizeXml implements WebAction
{
private static final Logger LOG = LoggerFactory.getLogger(MapAuthorizeXml.class);

  public void act(AmsServletInvocation reqInfo,
                  BeanManager beanMgr,
                  FormManager formMgr,
                  ResourceManager resMgr,
                  String serverLocation,
                  HttpServletRequest request,
                  HttpServletResponse response,
                  Hashtable inputParmMap,
		  DocumentHandler inputDoc)
       					throws AmsException
  {
    StringBuffer transactionData = new StringBuffer();

    // transform the given XML data to one that is readable by
    // PreAuthorizeTransactionMediator
    inputDoc.setAttribute("/In/User/userOid", inputDoc.getAttribute("/In/LoginUser/user_oid"));
    inputDoc.setAttribute("/In/User/securityRights", 
    	inputDoc.getAttribute("/In/LoginUser/security_rights"));
    transactionData.append(inputDoc.getAttribute("/In/Transaction/transaction_oid"));
    transactionData.append("/");
    transactionData.append(inputDoc.getAttribute("/In/Instrument/instrument_oid"));
    transactionData.append("/");
    transactionData.append(inputDoc.getAttribute("/In/Transaction/transaction_type_code"));
    inputDoc.setAttribute("/In/TransactionList/Transaction/transactionData", transactionData.toString());

  }  

}
