package com.ams.tradeportal.mediator.premediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Insert the type's description here.
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */

import com.amsinc.ecsg.web.*;

import javax.servlet.http.*;

import java.util.*;

import com.amsinc.ecsg.util.*;
import com.ams.tradeportal.busobj.util.*;
import com.ams.tradeportal.common.*;

import java.math.*;
public class PaymentMatchPreEdit implements com.amsinc.ecsg.web.WebAction {
private static final Logger LOG = LoggerFactory.getLogger(PaymentMatchPreEdit.class);
/**
 * AdminUserPreEdit constructor comment.
 */
public PaymentMatchPreEdit() {
	super();
}
	/**
	 * Extracts the individual security rights from the input document
	 * and creates a single securityRights value that is then set in
	 * the input document.
	 */
	public void act(AmsServletInvocation reqInfo,
					BeanManager beanMgr,
					FormManager formMgr,
					ResourceManager resMgr,
					String serverLocation,
					HttpServletRequest request,
					HttpServletResponse response,
					Hashtable inputParmMap,
					DocumentHandler inputDoc)
		throws com.amsinc.ecsg.frame.AmsException {
		
			// get the input document
			if (inputDoc == null) {
				LOG.info("Input document to PaymentMatchPreEdit is null");
				return;
			}
			
			inputDoc.setAttribute("/In/PayRemitList/PayRemits(0)/payRemitData", 
					inputDoc.getAttribute("/In/PayRemit/payRemitOid") + "/" + 
					inputDoc.getAttribute("/In/PayRemit/paymentInvoiceReferenceId"));
			//Srinivasu_D CR#997 Rel9.3 04/23/2015 - Added this to format the inputDoc for auto payment/discount creation
			String buttonName = inputDoc.getAttribute("/In/AutoPayment/autoMatchButton");
			
			if(TradePortalConstants.AUTO_PAY_MATCH.equals(buttonName) || TradePortalConstants.AUTO_PAY_UNMATCH.equals(buttonName) ){
				String corpOrgOid  				= inputDoc.getAttribute("/In/AutoPayment/corpOrgOid");
	    		String payRemitOid  			= inputDoc.getAttribute("/In/AutoPayment/payRemitOid");		        				
	    		String remitInd					= inputDoc.getAttribute("/In/AutoPayment/remitInd");	
	    		String remitAmount				= inputDoc.getAttribute("/In/AutoPayment/remitAmount");	
				String payInvOid				= inputDoc.getAttribute("/In/AutoPayment/payInvOid");	
				String autoPayInd				= inputDoc.getAttribute("/In/AutoPayment/autoPayInd");	
				String autoMatchButton			= inputDoc.getAttribute("/In/AutoPayment/autoMatchButton");	
				String applied_amount			= inputDoc.getAttribute("/In/AutoPayment/applied_amount");
				String invoiceID				= inputDoc.getAttribute("/In/AutoPayment/invoiceID");
				String cbName					= inputDoc.getAttribute("/In/AutoPayment/cbName");
				String erpSettings				= inputDoc.getAttribute("/In/AutoPayment/erpSettings");
				String otlInvoiceOid				= inputDoc.getAttribute("/In/AutoPayment/OTLInvoiceUoid");
				inputDoc.removeComponent("/In/AutoPayment");				
				
				
				
				int i=0;
	   	         inputDoc.setAttribute("/In/AutoPayment("+i+")/invoiceID",invoiceID);
	   	         inputDoc.setAttribute("/In/AutoPayment("+i+")/applied_amount",applied_amount);
	   	         inputDoc.setAttribute("/In/AutoPayment("+i+")/corpOrgOid",corpOrgOid);
	   	         inputDoc.setAttribute("/In/AutoPayment("+i+")/payRemitOid",payRemitOid);
	   	         inputDoc.setAttribute("/In/AutoPayment("+i+")/remitInd",remitInd);
	   	         inputDoc.setAttribute("/In/AutoPayment("+i+")/remitAmount",remitAmount);
				 inputDoc.setAttribute("/In/AutoPayment("+i+")/payInvOid",payInvOid);
				 inputDoc.setAttribute("/In/AutoPayment("+i+")/autoMatchButton",buttonName);
				 inputDoc.setAttribute("/In/AutoPayment("+i+")/autoPayInd",autoPayInd);
				 inputDoc.setAttribute("/In/AutoPayment("+i+")/cbName",cbName);
				 inputDoc.setAttribute("/In/AutoPayment("+i+")/erpSettings",erpSettings);
				 inputDoc.setAttribute("/In/AutoPayment("+i+")/OTLInvoiceUoid",otlInvoiceOid);
				}				
			
			else if(TradePortalConstants.AUTO_PAY_SEARCH.equals(buttonName)){
				
	    		String corpOrgOid  			= inputDoc.getAttribute("/In/AutoPayment/corpOrgOid");
	    		String payRemitOid  		= inputDoc.getAttribute("/In/AutoPayment/payRemitOid");		        				
	    		String remitInd				= inputDoc.getAttribute("/In/AutoPayment/remitInd");	
	    		String remitAmount			= inputDoc.getAttribute("/In/AutoPayment/remitAmount");	
				String payInvOid			= inputDoc.getAttribute("/In/AutoPayment/payInvOid");
				String autoPayInd 			= inputDoc.getAttribute("/In/AutoPayment/autoPayInd");
				String cbName				= inputDoc.getAttribute("/In/AutoPayment/cbName");
				String erpSettings			= inputDoc.getAttribute("/In/AutoPayment/erpSettings");
				inputDoc.removeComponent("/In/AutoPayment");
				Vector <DocumentHandler> newMatchItems = inputDoc.getFragments("/In/NewMatchItem");
				
				for(int i=0;i<newMatchItems.size();i++){
					 DocumentHandler currDoc = (DocumentHandler) newMatchItems.get(i);
					 String invoiceID = currDoc.getAttribute("/newMatchItemId");
		   			 String outAmount = currDoc.getAttribute("/newMatchItemInvoiceOutstandingAmount");
					 String otlInvoiceOid = currDoc.getAttribute("/OTLInvoiceUoid");
					 
		   	         BigDecimal outstandingAmt = new BigDecimal(outAmount.replace(",",""));
		   	         inputDoc.setAttribute("/In/AutoPayment("+i+")/invoiceID",invoiceID);
		   	         inputDoc.setAttribute("/In/AutoPayment("+i+")/applied_amount",String.valueOf(outstandingAmt));
		   	         inputDoc.setAttribute("/In/AutoPayment("+i+")/corpOrgOid",corpOrgOid);
		   	         inputDoc.setAttribute("/In/AutoPayment("+i+")/payRemitOid",payRemitOid);
		   	         inputDoc.setAttribute("/In/AutoPayment("+i+")/remitInd",remitInd);
		   	         inputDoc.setAttribute("/In/AutoPayment("+i+")/remitAmount",remitAmount);
					 inputDoc.setAttribute("/In/AutoPayment("+i+")/payInvOid",payInvOid);
					 inputDoc.setAttribute("/In/AutoPayment("+i+")/autoMatchButton",buttonName);
					 inputDoc.setAttribute("/In/AutoPayment("+i+")/autoPayInd",autoPayInd);
					 inputDoc.setAttribute("/In/AutoPayment("+i+")/cbName",cbName);
					 inputDoc.setAttribute("/In/AutoPayment("+i+")/erpSettings",erpSettings);
					 inputDoc.setAttribute("/In/AutoPayment("+i+")/OTLInvoiceUoid",otlInvoiceOid);
				}		
				
			 			//Srinivasu_D CR#997 Rel9.3 04/23/2015 - End
			}
			 
		}
	}	
