package com.ams.tradeportal.mediator.premediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */

import com.amsinc.ecsg.web.*;
import javax.servlet.http.*;
import java.util.*;
import com.amsinc.ecsg.util.*;
import com.amsinc.ecsg.frame.*;
import com.ams.tradeportal.common.*;
import com.ams.tradeportal.busobj.util.*;

public class ThresholdGroupPreEdit implements com.amsinc.ecsg.web.WebAction {
private static final Logger LOG = LoggerFactory.getLogger(ThresholdGroupPreEdit.class);

/**
 * Handles the conversion of the user-entered amount field for the user's
 * locale.   The JSP submits what the user entered, and therefor expects
 * to always see that in the /In section of the XML.  This pre-mediator
 * stores the user entered amount in a temporary spot, then changes
 * the actual amount data to reflect the standard numerical representation
 * (no commas, locale-specific decimal points, etc).
 * The corresponding post-mediator action places the user entered amount
 * back into the proper spot in the XML.
 *
 * This pre-mediator action must be called for all transaction forms that
 * have an amount field.
 */
 public void act(AmsServletInvocation reqInfo,
				BeanManager beanMgr,
				FormManager formMgr,
				ResourceManager resMgr,
				String serverLocation,
				HttpServletRequest request,
				HttpServletResponse response,
				Hashtable inputParmMap,
				DocumentHandler inputDoc)
			throws com.amsinc.ecsg.frame.AmsException
 {

        // Convert decimal numbers from their locale-specific entry format into
        // a standard format that can be understood by the database.
        // If the format is invalid, an error value will be placed into
        // the XML that will trigger an error later.
        // In the corresponding post mediator action, the passed-in value
        // is placed back into its original location.
       doDecimalPreMediatorAction(inputDoc, resMgr, "import_LC_issue_thold");
       doDecimalPreMediatorAction(inputDoc, resMgr, "import_LC_amend_thold");
       doDecimalPreMediatorAction(inputDoc, resMgr, "import_LC_discr_thold");
       doDecimalPreMediatorAction(inputDoc, resMgr, "standby_LC_issue_thold");
       doDecimalPreMediatorAction(inputDoc, resMgr, "standby_LC_amend_thold");
       doDecimalPreMediatorAction(inputDoc, resMgr, "standby_LC_discr_thold");
       doDecimalPreMediatorAction(inputDoc, resMgr, "export_LC_issue_thold");
       doDecimalPreMediatorAction(inputDoc, resMgr, "export_LC_amend_transfer_thold");
       doDecimalPreMediatorAction(inputDoc, resMgr, "export_LC_issue_assign_thold");
       doDecimalPreMediatorAction(inputDoc, resMgr, "export_LC_discr_thold");
       doDecimalPreMediatorAction(inputDoc, resMgr, "guarantee_issue_thold");
       doDecimalPreMediatorAction(inputDoc, resMgr, "guarantee_amend_thold");
       doDecimalPreMediatorAction(inputDoc, resMgr, "airwaybill_issue_thold");
       doDecimalPreMediatorAction(inputDoc, resMgr, "ship_guarantee_issue_thold");
       doDecimalPreMediatorAction(inputDoc, resMgr, "export_collection_issue_thold");
       doDecimalPreMediatorAction(inputDoc, resMgr, "export_collection_amend_thold");
       doDecimalPreMediatorAction(inputDoc, resMgr, "new_export_coll_issue_thold"); //Vasavi CR 524 03/31/2010 Begin
       doDecimalPreMediatorAction(inputDoc, resMgr, "new_export_coll_amend_thold");//Vasavi CR 524 03/31/2010 End
       doDecimalPreMediatorAction(inputDoc, resMgr, "inc_standby_LC_discr_thold");
       doDecimalPreMediatorAction(inputDoc, resMgr, "funds_transfer_issue_thold");
       doDecimalPreMediatorAction(inputDoc, resMgr, "loan_request_issue_thold");
       doDecimalPreMediatorAction(inputDoc, resMgr, "request_advise_issue_thold");
       doDecimalPreMediatorAction(inputDoc, resMgr, "request_advise_amend_thold");
       doDecimalPreMediatorAction(inputDoc, resMgr, "request_advise_discr_thold");
       //Krishna CR 375-D ATP 07/19/2007 Begin
       doDecimalPreMediatorAction(inputDoc, resMgr, "approval_to_pay_issue_thold");
       doDecimalPreMediatorAction(inputDoc, resMgr, "approval_to_pay_amend_thold");
       doDecimalPreMediatorAction(inputDoc, resMgr, "approval_to_pay_discr_thold");
       //Krishna CR 375-D ATP 07/19/2007 End
	//Pratiksha CR-434 ARM 10/01/2008 Begin
       doDecimalPreMediatorAction(inputDoc, resMgr, "ar_match_response_thold");
       doDecimalPreMediatorAction(inputDoc, resMgr, "ar_approve_discount_thold");
       doDecimalPreMediatorAction(inputDoc, resMgr, "ar_close_invoice_thold");
       doDecimalPreMediatorAction(inputDoc, resMgr, "ar_finance_invoice_thold");
       //Pratiksha CR-434 ARM 10/01/2008 End
       //Chandrakanth CR 451 10/21/2008 Begin
	   doDecimalPreMediatorAction(inputDoc, resMgr, "transfer_btw_accts_issue_thold");
	   doDecimalPreMediatorAction(inputDoc, resMgr, "domestic_payment_issue_thold");
	   doDecimalPreMediatorAction(inputDoc, resMgr, "ar_dispute_invoice_thold");
       //Chandrakanth CR 451 10/21/2008 End
	   doDecimalPreMediatorAction(inputDoc, resMgr, "direct_debit_thold");	   //Pratiksha CR-509 DDI 12/09/2009 
	   //Narayan CR-913 03-Feb-2014 Rel9.0 ADD- BEGIN
	   doDecimalPreMediatorAction(inputDoc, resMgr, "upload_rec_inv_thold");
	   doDecimalPreMediatorAction(inputDoc, resMgr, "upload_pay_inv_thold");
	   doDecimalPreMediatorAction(inputDoc, resMgr, "pyb_mgm_inv_thold");
	   //Narayan CR-913 03-Feb-2014 Rel9.0 ADD- End  
	   
	   //SSikhakolli - Rel-9.4 CR-818 IR# T36000042464 - Begin
	   doDecimalPreMediatorAction(inputDoc, resMgr, "imp_col_set_instr_thold");
	   doDecimalPreMediatorAction(inputDoc, resMgr, "loan_request_set_instr_thold");
	   doDecimalPreMediatorAction(inputDoc, resMgr, "imp_col_set_instr_dlimit");
	   doDecimalPreMediatorAction(inputDoc, resMgr, "loan_request_set_instr_dlimit");
	   //SSikhakolli - Rel-9.4 CR-818 IR# T36000042464 - End
	   
       doDecimalPreMediatorAction(inputDoc, resMgr, "import_LC_discr_dlimit");
       doDecimalPreMediatorAction(inputDoc, resMgr, "import_LC_issue_dlimit");
       doDecimalPreMediatorAction(inputDoc, resMgr, "import_LC_amend_dlimit");
       doDecimalPreMediatorAction(inputDoc, resMgr, "standby_LC_issue_dlimit");
       doDecimalPreMediatorAction(inputDoc, resMgr, "standby_LC_amend_dlimit");
       doDecimalPreMediatorAction(inputDoc, resMgr, "standby_LC_discr_dlimit");
       doDecimalPreMediatorAction(inputDoc, resMgr, "export_LC_issue_dlimit");
       doDecimalPreMediatorAction(inputDoc, resMgr, "export_LC_amend_transfer_dlim");
       doDecimalPreMediatorAction(inputDoc, resMgr, "export_LC_issue_assign_dlimit");
       doDecimalPreMediatorAction(inputDoc, resMgr, "export_LC_discr_dlimit");
       doDecimalPreMediatorAction(inputDoc, resMgr, "guarantee_issue_dlimit");
       doDecimalPreMediatorAction(inputDoc, resMgr, "guarantee_amend_dlimit");
       doDecimalPreMediatorAction(inputDoc, resMgr, "airwaybill_issue_dlimit");
       doDecimalPreMediatorAction(inputDoc, resMgr, "ship_guarantee_issue_dlimit");
       doDecimalPreMediatorAction(inputDoc, resMgr, "export_collection_issue_dlimit");
       doDecimalPreMediatorAction(inputDoc, resMgr, "export_collection_amend_dlimit");
       doDecimalPreMediatorAction(inputDoc, resMgr, "new_export_coll_issue_dlimit"); //Vasavi CR 524 03/31/2010 Begin
       doDecimalPreMediatorAction(inputDoc, resMgr, "new_export_coll_amend_dlimit"); //Vasavi CR 524 03/31/2010 End
       doDecimalPreMediatorAction(inputDoc, resMgr, "inc_standby_LC_discr_dlimit");
       doDecimalPreMediatorAction(inputDoc, resMgr, "funds_transfer_issue_dlimit");
       doDecimalPreMediatorAction(inputDoc, resMgr, "loan_request_issue_dlimit");
       doDecimalPreMediatorAction(inputDoc, resMgr, "request_advise_issue_dlimit");
       doDecimalPreMediatorAction(inputDoc, resMgr, "request_advise_amend_dlimit");
       doDecimalPreMediatorAction(inputDoc, resMgr, "request_advise_discr_dlimit");
       //Krishna CR 375-D ATP 07/19/2007 Begin
       doDecimalPreMediatorAction(inputDoc, resMgr, "approval_to_pay_issue_dlimit");
       doDecimalPreMediatorAction(inputDoc, resMgr, "approval_to_pay_amend_dlimit");
       doDecimalPreMediatorAction(inputDoc, resMgr, "approval_to_pay_discr_dlimit");
       //Krishna CR 375-D ATP 07/19/2007 End
	//Pratiksha CR-434 ARM 10/01/2008 Begin
       doDecimalPreMediatorAction(inputDoc, resMgr, "ar_match_response_dlimit");
       doDecimalPreMediatorAction(inputDoc, resMgr, "ar_approve_discount_dlimit");
       doDecimalPreMediatorAction(inputDoc, resMgr, "ar_close_invoice_dlimit");
       doDecimalPreMediatorAction(inputDoc, resMgr, "ar_finance_invoice_dlimit");
       //Pratiksha CR-434 ARM 10/01/2008 End
       //Chandrakanth CR 451 10/21/2008 Begin
	          doDecimalPreMediatorAction(inputDoc, resMgr, "transfer_btw_accts_issue_dlimit");
	          doDecimalPreMediatorAction(inputDoc, resMgr, "domestic_payment_issue_dlimit");
	          doDecimalPreMediatorAction(inputDoc, resMgr, "ar_dispute_invoice_dlimit");
       //Chandrakanth CR 451 10/21/2008 End
	   doDecimalPreMediatorAction(inputDoc, resMgr, "direct_debit_dlimit");  //Pratiksha CR-509 DDI 12/09/2009
	   //Narayan CR-913 03-Feb-2014 Rel9.0 ADD- BEGIN
	   doDecimalPreMediatorAction(inputDoc, resMgr, "upload_rec_inv_dlimit");
	   doDecimalPreMediatorAction(inputDoc, resMgr, "upload_pay_inv_dlimit");
	   doDecimalPreMediatorAction(inputDoc, resMgr, "pyb_mgm_inv_dlimit");
	   //Narayan CR-913 03-Feb-2014 Rel9.0 ADD- End 
	   //vdesingu CR 914A Rel 9.2 - Start
	   doDecimalPreMediatorAction(inputDoc, resMgr, "pay_credit_note_thold");
	   doDecimalPreMediatorAction(inputDoc, resMgr, "pay_credit_note_dlimit");
	   //vdesingu CR 914A Rel 9.2 - End

 }


        /** Convert decimal number from its locale-specific entry format into
         * a standard format that can be understood by the database.
         * If the format is invalid, an error value will be placed into
         * the XML that will trigger an error later.
         * In the corresponding post mediator action, the passed-in value
         * is placed back into its original location.
         *
         * @param inputDoc DocumentHandler - the input document of the mediator
         * @param resMgr ResourceManager - used to determine locale of the user
         * @param attribute String - the decimal attribute being handled
         */
 public void doDecimalPreMediatorAction(DocumentHandler inputDoc, ResourceManager resMgr, String attribute)
  {
   	// get the input document
	if (inputDoc == null)
	{
	   LOG.info("Input document to is null");
	   return;
	}

        String pathToDataUsedToPopulate = "/In/ThresholdGroup/"+attribute;
        String pathToTemporaryStorage = "/In/ThresholdGroup/userEntered"+attribute;

        // Place the user entered amount into a temporary location
        // The post-mediator action copies /userEnteredAmount back into /amount
        if(inputDoc.getAttribute(pathToDataUsedToPopulate) != null)
           inputDoc.setAttribute(pathToTemporaryStorage, inputDoc.getAttribute(pathToDataUsedToPopulate));
        else
           inputDoc.setAttribute(pathToTemporaryStorage, "");


        String amount;

        try
         {
            // Convert the amount from its localized form to standard (en_US) form
            amount =  NumberValidator.getNonInternationalizedValue(inputDoc.getAttribute(pathToDataUsedToPopulate), resMgr.getResourceLocale(), true);
         }
        catch(InvalidAttributeValueException iae)
         {
            // If the format was invalid, set amount equal to something that will trigger an error
            // to be issued.
            amount = TradePortalConstants.BAD_AMOUNT;
         }

        // Place the result of the above code into the amount location
        // This is what will be populated into the business object.
        inputDoc.setAttribute(pathToDataUsedToPopulate, amount);
  }

}