package com.ams.tradeportal.mediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.RemoteException;
import java.rmi.UnknownHostException;
import java.security.SecureRandom;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.MissingResourceException;
import java.util.PropertyResourceBundle;
import java.util.StringTokenizer;

import net.sourceforge.jradiusclient.RadiusAttribute;
import net.sourceforge.jradiusclient.RadiusAttributeValues;
import net.sourceforge.jradiusclient.RadiusClient;
import net.sourceforge.jradiusclient.RadiusPacket;
import net.sourceforge.jradiusclient.exception.InvalidParameterException;
import net.sourceforge.jradiusclient.exception.RadiusException;

import com.ams.tradeportal.busobj.ActiveUserSession;
import com.ams.tradeportal.busobj.BankOrganizationGroup;
import com.ams.tradeportal.busobj.ClientBank;
import com.ams.tradeportal.busobj.CorporateOrganization;
import com.ams.tradeportal.busobj.GlobalOrganization;
import com.ams.tradeportal.busobj.Organization;
import com.ams.tradeportal.busobj.SecurityProfile;
import com.ams.tradeportal.busobj.User;
import com.ams.tradeportal.common.ConfigSettingManager;
import com.ams.tradeportal.common.Debug;
import com.ams.tradeportal.common.HSMEncryptDecrypt;
import com.ams.tradeportal.common.Password;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.frame.MediatorBean;
import com.amsinc.ecsg.frame.MediatorServices;
import com.amsinc.ecsg.frame.ObjectIDCache;
import com.amsinc.ecsg.util.DateTimeUtility;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.GMTUtility;
import com.amsinc.ecsg.util.StringFunction;
import com.rsa.authagent.authapi.AuthAgentException;
import com.rsa.authagent.authapi.AuthSession;
import com.rsa.authagent.authapi.AuthSessionFactory;
import com.rsa.authagent.authapi.PinData;
import com.thales_esecurity.ssas.BasicResponse;
import com.thales_esecurity.ssas.DigiPassResponse;
import com.thales_esecurity.ssas.SSASError;
import com.thales_esecurity.ssas.SSASSoap;
import com.thales_esecurity.ssas.SSASSoapServiceLocator;

/*
 * Manages the Logon Transaction.
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class LogonMediatorBean extends MediatorBean
{
private static final Logger LOG = LoggerFactory.getLogger(LogonMediatorBean.class);
    static int currentServerNumber2FA = 0;
    
    // The RSA AuthSession may need to be saved from one invocation of this mediator to the next.
    private class CachedRSASession {
        String userOid;
        Date timeOfCaching;
        AuthSession session;
        public CachedRSASession(String userOid, Date timeOfCaching, AuthSession session) {
            this.userOid = userOid;
            this.timeOfCaching = timeOfCaching;
            this.session = session;
        }
    }
    static HashMap<String, CachedRSASession> cachedRSASessions = new HashMap<String, CachedRSASession>();
   /*
	* Called by the processRequest() method of the Ancestor class to process the designated
	* transaction.  The input parameters for the transaction are received in an XML document
	* (inputDoc).  The execute() method is responsible for calling one or more business objects
	* to process the transaction and populating the outputDoc XML document with the results
	* of the transaction.
	*/

    //CR-711 Rel 8.0 - Added this as a class property
    PropertyResourceBundle portalProperties = (PropertyResourceBundle)PropertyResourceBundle.getBundle("TradePortal");

   public DocumentHandler execute(DocumentHandler inputDoc, DocumentHandler outputDoc, MediatorServices mediatorServices)
								  throws RemoteException, AmsException
   {
       
	   User user                 = null;

 	  // Retrieve the input parameters passed in

	  String certificateId       = inputDoc.getAttribute("/User/certificateId");
	  String loginId             = inputDoc.getAttribute("/User/loginId");
	  String ssoId               = inputDoc.getAttribute("/User/ssoId");
	  String password            = inputDoc.getAttribute("/User/password");
      String password2FA         = inputDoc.getAttribute(("/User/password2FA"));
      String authMethod          = inputDoc.getAttribute("/auth");
      String loginOrganizationId = inputDoc.getAttribute("/organizationId");
      // [START] CR-482
      String encryptedLoginId = inputDoc.getAttribute("/User/encryptedLoginId");
      String encryptedPassword = inputDoc.getAttribute("/User/encryptedPassword");
      // [START] CR-543
      String protectedPassword = inputDoc.getAttribute("/User/protectedPassword");
      String encryptedPassword2FA = inputDoc.getAttribute("/User/encryptedPassword2FA");
      //cr498 - additional data for reauthentication
      String reAuthObjectType = inputDoc.getAttribute("/reAuthObjectType");
      //ir cnuk113043991 - handle multiple reAuthObjectOids
      String reAuthObjectOids = inputDoc.getAttribute("/reAuthObjectOids");
      String serverName = portalProperties.getString("serverName");
      outputDoc.setAttribute("/serverName", serverName);
      LOG.debug("[LogonMediatorBean.execute(DocumentHandler,DocumentHandler,MediatorServices)] encryptedLoginId = '"+encryptedLoginId+"'");
      LOG.debug("[LogonMediatorBean.execute(DocumentHandler,DocumentHandler,MediatorServices)] encryptedPassword = '"+encryptedPassword+"'");
      LOG.debug("[LogonMediatorBean.execute(DocumentHandler,DocumentHandler,MediatorServices)] protectedPassword = '"+protectedPassword+"'");
      // [END] CR-543
      LOG.debug("[LogonMediatorBean.execute(DocumentHandler,DocumentHandler,MediatorServices)] encryptedPassword2FA = '"+encryptedPassword2FA+"'");
      // Check if this TradePortal instance is configured for HSM
      // encryption / decryption.  If it is then the loginId is
      // encrypted.  Decrypt the loginId.
      boolean instanceHSMEnabled = HSMEncryptDecrypt.isInstanceHSMEnabled();
      if(instanceHSMEnabled
    		  && (encryptedLoginId != null)
    		  && (encryptedLoginId.trim().length() > 0)) {
    	  try {
			loginId = HSMEncryptDecrypt.decryptForTransport(encryptedLoginId);
			LOG.debug("[LogonMediatorBean.execute(DocumentHandler,DocumentHandler,MediatorServices)] Decrypted loginId = "
						+ loginId);
			outputDoc.setAttribute("/User/loginId", loginId);
    	  } catch (Exception e) {
			System.err.println("Exception occurred in LogonMediator:  Exception caught while trying to decrypt loginId = " + loginId + " " + e.toString());
    	  }
      } // if(instanceHSMEnabled) {
      // [END] CR-482
	  try
	  {
		 // Check to see whether the user exists for the organization
         if (authMethod.equals(TradePortalConstants.AUTH_PASSWORD)) {
		    mediatorServices.debug("validating loginid");

		    user = validateLoginId(loginId, loginOrganizationId, mediatorServices);

            if (user == null) {
                LOG.info("User is null, could not validate login id " + loginId);
                return outputDoc;
            }

			// [START] CR-482 Decrypt the password depending on if the user is currently a
            //                Legacy or Hashed password user
			if (instanceHSMEnabled) {
				if(Password.isUserPasswordLegacy(user)) {
					// Legacy password - use the encrypted password
					try {
						password = HSMEncryptDecrypt.decryptForTransport(encryptedPassword);
						LOG.debug("[LogonMediatorBean.execute(DocumentHandler,DocumentHandler,MediatorServices)] "
								+ "User has a legacy password");
					} catch (Exception e) {
						System.err.println("Exception occurred in LogonMediator while trying to decrypt a legacy password : " + e.toString());
					}
				} else {
					// [START] CR-543
					// Protected password - use the protected password
					try {
						password = HSMEncryptDecrypt.decryptForTransport(protectedPassword);
						LOG.debug("[LogonMediatorBean.execute(DocumentHandler,DocumentHandler,MediatorServices)] "
								+ "User has a protected password");
					} catch (Exception e) {
						System.err.println("Exception occurred in LogonMediator while trying to decrypt a protected password: " + e.toString());
					}
					// [END] CR-543
				}
			} // if (instanceHSMEnabled)
			// [END] CR-482

            // Validate user password
            if (!validatePassword(password, user, outputDoc, mediatorServices)) {
                return outputDoc;
            }

            // Redirect to 2FA authentication page if this user uses 2-Phase Authentication
            outputDoc = populateOutputDoc(user.getAttributeLong("owner_org_oid"),
                    user.getAttribute("ownership_level"),
                    user, outputDoc, mediatorServices);
            //Naveen- 26-Sep-12 IR-T36000005762. Commented the below line and added the new line of code to rectify 2FA login issue.
            //String orgAuthMethod = outputDoc.getAttribute("../authenticationMethod");
            String orgAuthMethod = outputDoc.getAttribute("/Session/authenticationMethod");
            String userAuthMethod = user.getAttribute("authentication_method");
            if (TradePortalConstants.AUTH_2FA.equals(orgAuthMethod)
                    || TradePortalConstants.AUTH_PERUSER.equals(orgAuthMethod) && TradePortalConstants.AUTH_2FA.equals(userAuthMethod)){
                outputDoc.setAttribute("/ForwardToEnterPassword2FA", TradePortalConstants.INDICATOR_YES);
                return outputDoc;
            }
            // CR-482 end

         }
         //BSL 08/19/11 CR-663 Begin
         else if (authMethod.equals(TradePortalConstants.AUTH_SSO_REGISTRATION)) {
		    mediatorServices.debug("Validating SSO Registration Login ID");

		    user = validateSSORegistrationLoginId(loginId, loginOrganizationId, mediatorServices);
            if (user == null) {
				LOG.debug("[LogonMediatorBean.execute(DocumentHandler,DocumentHandler,MediatorServices)] "
						+ "User is null, could not validate registration login id");
                return outputDoc;
            }

            if (!validateSSORegistrationPassword(password, user, outputDoc, mediatorServices)) {
				LOG.debug("[LogonMediatorBean.execute(DocumentHandler,DocumentHandler,MediatorServices)] "
						+ "Could not validate registration password");
                return outputDoc;
            }

            // check that the user account is not already registered
   			if (StringFunction.isNotBlank(user.getAttribute("registered_sso_id"))) {
				LOG.debug("[LogonMediatorBean.execute(DocumentHandler,DocumentHandler,MediatorServices)] "
						+ "User is already registered");
   				mediatorServices.getErrorManager().issueError(getClass().getName(),
   						TradePortalConstants.UNABLE_TO_REGISTER);
   				return outputDoc;
   			}

   			outputDoc = populateOutputDoc(user.getAttributeLong("owner_org_oid"),
                    user.getAttribute("ownership_level"),
                    user, outputDoc, mediatorServices);
         }
         else if (authMethod.equals(TradePortalConstants.AUTH_REGISTERED_SSO)){
            mediatorServices.debug("Validating Registered SSO ID ");

            user = validateRegisteredSSOId(ssoId, loginOrganizationId, mediatorServices);
         }
         //BSL 08/19/11 CR-663 End
         else if (authMethod.equals(TradePortalConstants.AUTH_CERTIFICATE)) {
		    mediatorServices.debug("validating certificateid");

		    user = validateCertificateId(certificateId, loginOrganizationId, mediatorServices);
         } else if (authMethod.equals(TradePortalConstants.AUTH_SSO)){
            String authLevel = inputDoc.getAttribute("/HTTP_CAAS_AUTHLEVEL");
        	mediatorServices.debug("LogonMediatorBean: validating SSO Id sso id = " + ssoId + " HTTP_CAAS_AUTHLEVEL = " + authLevel);

            user = validateSSOId(ssoId, loginOrganizationId, authLevel, mediatorServices);

         // CR-482 Add authentication method 2FA
         }
         //Ravindra - 12/01/2011 - CR-541 - Start
         // CMA specific: Validate type of user AUTH and retrieve user details
         else if ( authMethod.equals(TradePortalConstants.AUTH_AES256)){

            mediatorServices.debug("validating SSO Using AES Auth");

            user = validateAESAuth(ssoId, loginOrganizationId, mediatorServices);

         // CR-482 Add authentication method 2FA
         }//Ravindra - 12/01/2011 - CR-541 - End
         else if (authMethod.equals(TradePortalConstants.AUTH_2FA)){
        	 // [START] CR-482 Encryption
        	 if(instanceHSMEnabled
        			 && (encryptedPassword2FA != null)
        			 && (encryptedPassword2FA.trim().length() > 0)) {
        		 try {
        			 password2FA = HSMEncryptDecrypt.decryptForTransport(encryptedPassword2FA);
        		 } catch (Exception e) {
        			 System.err.print("Exception caught while trying to decrypt encryptedPassword2FA: "+e.getMessage());
        		 }
        	 }
        	 // [END] CR-482 Encryption
             user = validateLoginId(loginId, loginOrganizationId, mediatorServices);
             if (user == null) {
                 LOG.info("User is null, could not validate login id " + loginId);
                 return outputDoc;
             }
             String tokenId2FA = user.getAttribute("token_id_2fa");

             // If this is invoked from Recertification (TradePortalRecertification.jsp), skip loginUser() and return
             String recertificationIndicator = inputDoc.getAttribute("/RecertificationIndicator");

             //ir cnuk113043991 - when reauthenticating, always pass back user oid
             if (TradePortalConstants.INDICATOR_YES.equals(recertificationIndicator)||
                     TradePortalConstants.INDICATOR_MULTIPLE.equals(recertificationIndicator)){
                 String userOid = user.getAttribute("user_oid");
                 outputDoc.setAttribute("/userOid",userOid);
             }

             //Validate 2FA password via 2FA server

             //AAlubala - CR-711 Rel8.0 - Check to see what token type is being used
             //VASCO or RSA. If RSA, use the validatePassword2FA method as is, otherwise if
             //VASCO, use the validatePassword2FAUsingVasco method
             //- START

             if(TradePortalConstants.VASCO_TOKEN.equals(user.getAttribute("token_type"))){
            	 // The tokenApplicationID is the concatenation of the secure device id that
            	 // is defined on the Corporate Customer User Profile page and the Application ID from CAAS.
            	 // This can be configured to come from reference data or a property file. Using property file for now.
                 String tokenApplicationID = user.getAttribute("token_id_2fa");
                 //for login, it is going to be a OTP. So, obtain the application id corresponding to the online otp
                 String onlineATPApp = "";
                 try{
                	 onlineATPApp = portalProperties.getString("online.otp.app");
                 }catch (MissingResourceException e){
                	//ignore
                 } 
                 
                 if(StringFunction.isNotBlank(onlineATPApp)){
                	 tokenApplicationID = tokenApplicationID+portalProperties.getString("online.otp.app");
                 }else{
                	 tokenApplicationID = tokenApplicationID+"OTPPINH";
                 }
                 //transID is a unique number for every transaction. For the login process, using a random number
                 String transID = "";
                 SecureRandom sr = new SecureRandom();
                 long transactionID = sr.nextLong();
                 transID = String.valueOf(transactionID);
                 //password2FA - similar to RSA's one. The radio button will determine whether it is from an RSA
                 //entry window or VASCO entry window -
            	 if(!validatePassword2FAUsingVasco(transID, tokenApplicationID, password2FA,
            			 user,inputDoc, outputDoc, mediatorServices )){
	                 return outputDoc;
	             }
            	 sr=null;
             }
             // DK CR-804 Rel8.4 12/18/2013 starts
             else {
            	 String rsaAPI = ConfigSettingManager.getConfigSetting("PORTAL_APP", "PORTAL", "RSA_JRADIUS_API", "JRADIUS");
            	 String logonStatus = inputDoc.getAttribute("/LogonStatus");
            	 if ("EnterPassword2FA".equals(logonStatus) 
            			 || (TradePortalConstants.INDICATOR_YES.equals(recertificationIndicator))
            			 || "EnterPassword2FANextCode".equals(logonStatus)) {   /// DK IR T36000024234 Rel8.4 02/07/2014
	                 if ("RSA".equalsIgnoreCase(rsaAPI)) {
                             try {
		 	             if (!validatePassword2FAUsingRSA(tokenId2FA, password2FA,
			            		 recertificationIndicator, user, inputDoc, outputDoc, mediatorServices,
			            		 reAuthObjectOids, reAuthObjectType,"EnterPassword2FANextCode".equals(logonStatus) )) {
			                 return outputDoc;
			             }
                             }
                             catch (AuthAgentException e) {
                                LOG.info("[LogonMediatorBean.RSA validatePassword2FAUsingRSA] loginId="+user.getAttribute("login_id") + ";token_id_2fa=" + tokenId2FA);
                                e.printStackTrace();
                                mediatorServices.getErrorManager().issueError(getClass().getName(), TradePortalConstants.ERROR_VALIDATE_2FA, e.getMessage());
                                return outputDoc;
                             }
	                 }
	             	else{
			             if (!validatePassword2FA(tokenId2FA, password2FA,
			            		 recertificationIndicator, user, inputDoc, outputDoc, mediatorServices,
			            		 reAuthObjectOids, reAuthObjectType )) {
			                 return outputDoc;
			             }
	             	}
            	 }
            	 else if ("RSANewPin".equals(logonStatus)) {
                    try{
            		validateNewPinUsingRSA(user, inputDoc,outputDoc, mediatorServices);
                    }
                    catch (AuthAgentException e) {
                       LOG.info("[LogonMediatorBean.RSA validateNewPinUsingRSA] loginId="+user.getAttribute("login_id"));
                       e.printStackTrace();
                       mediatorServices.getErrorManager().issueError(getClass().getName(), TradePortalConstants.ERROR_VALIDATE_2FA, e.getMessage());
                    }
                    
                    // Return here to prompt the user to log in again with new PIN.
                    return outputDoc;
            	 }
                 // DK CR-804 Rel8.4 12/18/2013 ends
             }
             //END  - CR-711

             //ir cnuk113043991 - allow for multiple recertification indicator
             if (TradePortalConstants.INDICATOR_YES.equals(recertificationIndicator)||
                     TradePortalConstants.INDICATOR_MULTIPLE.equals(recertificationIndicator)){
                 outputDoc.setAttribute("/logonSuccesful","true");
                 return outputDoc;
             }
             // CR-482 end
         }

		 // If the user exists
		 if (user != null)
		 {
		    // Next, check to see whether the user is currently logged in somewhere else
            mediatorServices.debug("checking if user already logged on");

            //cquinton 2/16/2012 Rel 8.0 ppx255 start
            boolean userAlreadyLoggedIn = isUserAlreadyLoggedIn(user, mediatorServices);
            //and check to see if behavior is different because of it
            boolean allowReLogon = true;
            try {
               String allowReLogonStr = portalProperties.getString("allowReLogon");
               if ( TradePortalConstants.INDICATOR_NO.equals(allowReLogonStr) ) {
                   allowReLogon = false;
               }
            }
            catch (MissingResourceException e){} //ignore

            if ( allowReLogon || //continue with login regardless
                 (!allowReLogon && !userAlreadyLoggedIn ) ) {
			   // Log the user in
			   mediatorServices.debug("performing logon");
               outputDoc = populateOutputDoc(user.getAttributeLong("owner_org_oid"),
                  user.getAttribute("ownership_level"),
                  user, outputDoc, mediatorServices);
               outputDoc = loginUser(user, authMethod, userAlreadyLoggedIn,
                   outputDoc, mediatorServices);

			} else {
			   //user is already logged in and not configured for relogon
			   LOG.info("user is already logged on");
               mediatorServices.getErrorManager().issueError(getClass().getName(),
                  TradePortalConstants.ALREADY_LOGGED_IN,
                  user.getAttribute("user_identifier"));
			}
		    //cquinton 2/2/2012 Rel 8.0 ppx255 end
		 } else {
			LOG.info("User is null, could not validate login id");
		 }
	  }
	  catch (AmsException e)
	  {
		 LOG.info("AmsException occurred in LogonMediator: " + e.toString());
	  }
	  catch (RemoteException e)
	  {
		 LOG.info("RemoteException occurred in LogonMediator: " + e.toString());
	  }

	  // Return the results of the transaction
	  return outputDoc;
   }


   /**
	* This method is used for retrieving the user's security profile.
	*
	* @param user             User             - the current User business object
	* @param mediatorServices MediatorServices - the current mediator's mediator services object
	* @return SecurityProfile securityProfile  - the user's security profile
	* @exception java.rmi.RemoteException
	* @exception com.amsinc.ecsg.frame.AmsException
	*/
   private SecurityProfile getUserSecurityProfile(User user, MediatorServices mediatorServices)
												  throws RemoteException, AmsException
   {
	  SecurityProfile   securityProfile    = null;
	  long              securityProfileOid = 0;

	  // Retrieve the user's security profile oid
	  securityProfileOid = user.getAttributeLong("security_profile_oid");

	  // Create the security profile business object using mediator services and instantiate it
	  securityProfile = (SecurityProfile) mediatorServices.createServerEJB("SecurityProfile");
	  try
	  {
		 securityProfile.getData(securityProfileOid);
	  }
	  catch (Exception e)
	  {
                LOG.info("Exception caught while getting security profile in LogonMediator...");
                LOG.info("  Current GMT Time is: "+ GMTUtility.getGMTDateTime());
                e.printStackTrace();
                throw new AmsException("Exception caught while getting security profile in LogonMediator...");
	  }

	  return securityProfile;
   }

   /**
	* This method determines if a user has passed the maximum number of invalid
	* logon attempts.  If they have passed the limit, their user record is flagged
	* as locked and they will not be permitted to log on.  They must contact an
	* system administrator to remove the lock.
	*
	* @param user             User             - the current User business object
	* @exception java.rmi.RemoteException
	* @exception com.amsinc.ecsg.frame.AmsException
	*/
   private void checkIfUserShouldBeLockedOut(User user)
           throws AmsException, RemoteException
    {
        // Get maximum failed attempts from a hashtable
        int maxFailedAttemptsAllowed = Integer.parseInt((String)user.getSecurityLimits().get("maxFailedLogons"));
        //defect IRT#36000006113
        if(user.getAttributeInteger("invalid_login_attempt_count") >= maxFailedAttemptsAllowed)
         {
            // Lock out the user
            user.setAttribute("locked_out_ind",TradePortalConstants.INDICATOR_YES);
         }
    }

   /**
	* This method checks to see if the user's password has expired.  If it has,
	* the user is permitted to log in, but a flag is placed into the outputDoc.
	* The JSPs check for this flag, and force the user to change their password.
	*
	* @param expirationPeriodInDays - the number of days in the expiration period
	* @return true if password is expired, false if it is valid.
	*/
   private void checkPasswordExpiration(User user, DocumentHandler outputDoc)
        throws AmsException, RemoteException
    {
        //cquinton 2/15/2012 Rel 8.0 ppx 255 start
        // refactor logic to User.isPasswordExpired() so it can be used elsewhere
        boolean isExpired = user.isPasswordExpired();
        /*
        boolean isExpired;

        if(user.getAttribute("password_change_date").equals(""))
         {
            // If password change date is null, consider it expired
            isExpired = true;
         }
        else
          {
            // Determine password expiration period
            int passwordExpirationPeriodDays =
                Integer.parseInt((String)user.getSecurityLimits().get("passwordExpirationDays"));

            // Converts the expiration period (passed in as days)
            // into milliseconds
            // (number of days) * 24 hours in a day * 60 minutes in an hour *
            //     60 seconds in a minute * 1000 milliseconds in a second
            long numMillisecondsInDay = 86400000;
            long expirationPeriod = passwordExpirationPeriodDays * numMillisecondsInDay;

            // Last password change date is stored as GMT (converted to milliseconds since 1/1/1970)
            long lastPasswordChangeDate = user.getAttributeDateTime("password_change_date").getTime();

            // Get current date time in GMT (converted to milliseconds since 1/1/1970)
            long todaysDateInGMT = GMTUtility.getGMTDateTime().getTime();


            // Determine if the password is expired
            if( (lastPasswordChangeDate + expirationPeriod) < todaysDateInGMT )
                isExpired = true;
            else
                isExpired = false;
          }
        */
        //cquinton 2/15/2012 Rel 8.0 ppx 255 start

         if(isExpired)
            {
              outputDoc.setAttribute("/passwordExpire","true");
            }
    }

/**
	* This method is used for populating the output xml doc with necessary user organization
	* info.
	*
	* @param organizationOid  long             - the unique oid of the user's organization
	* @param organizationType String           - the type of organization the user belongs to
	* @param user             User             - the current User business object
	* @param outputDoc        DocumentHandler  - the current mediator's output xml doc
	* @param mediatorServices MediatorServices - the current mediator's mediator services object
	* @return SecurityProfile securityProfile  - the user's security profile
	* @exception java.rmi.RemoteException
	* @exception com.amsinc.ecsg.frame.AmsException
	*/
   private DocumentHandler populateOutputDoc(long organizationOid, String organizationType, User user,
											 DocumentHandler outputDoc, MediatorServices mediatorServices)
											 throws RemoteException, AmsException
   {
	  // If the user is a global user
	  if (organizationType.equals(TradePortalConstants.OWNER_GLOBAL))
	  {
		 outputDoc = populateGlobalUserXmlDoc(outputDoc, mediatorServices);
	  }
	  else if (organizationType.equals(TradePortalConstants.OWNER_BANK))      // Else if the user is a client bank user
	  {
		 outputDoc = populateClientBankUserXmlDoc(organizationOid, outputDoc, mediatorServices);
	  }
	  else if (organizationType.equals(TradePortalConstants.OWNER_BOG))       // Else if the user is a BOG user
	  {
		 outputDoc = populateBogUserXmlDoc(organizationOid, outputDoc, mediatorServices);
	  }
	  else if (organizationType.equals(TradePortalConstants.OWNER_CORPORATE)) // Else if the user is a corporate user
	  {
		 outputDoc = populateCorporateOrgUserXmlDoc(organizationOid, user, outputDoc, mediatorServices);
	  }
	  else
	  {
		 mediatorServices.getErrorManager().issueError(getClass().getName(), AmsConstants.INVALID_ATTRIBUTE,
													   organizationType, "organization_type");
	  }

	  String fullName = user.getAttribute("first_name");
	  if (StringFunction.isNotBlank(user.getAttribute("last_name"))) {
		  fullName += " " + user.getAttribute("last_name");
	  }
	  outputDoc.setAttribute("/User/user_full_name",fullName);

	  return outputDoc;
   }

   /**
	* This method is used for populating the mediator's output xml doc with the current
	* global user's organization info.
	*
	* @param outputDoc        DocumentHandler  - the current mediator's output xml doc
	* @return DocumentHandler outputDoc        - the output xml doc populated with organization info
	* @exception java.rmi.RemoteException
	* @exception com.amsinc.ecsg.frame.AmsException
	*/
   private DocumentHandler populateGlobalUserXmlDoc(DocumentHandler outputDoc,
		MediatorServices mediatorServices) throws RemoteException, AmsException
   {
	  GlobalOrganization   globalOrg = null;
          long globalOrgOid = -1;

	  // Create the global org business object using mediator services and instantiate it
	  globalOrg = (GlobalOrganization) mediatorServices.createServerEJB("GlobalOrganization");
	  try
 	  {
                 String sql = "select organization_oid from global_organization";
                 DocumentHandler result = DatabaseQueryBean.getXmlResultSet(sql, false, new ArrayList<Object>());
                 globalOrgOid = result.getAttributeLong("/ResultSetRecord(0)/ORGANIZATION_OID");

		 globalOrg.getData(globalOrgOid);

		 // Populate the output doc with the appropriate branding directory, org name, and org oid

		 outputDoc.setAttribute("/Session/brandingDirectory",
					globalOrg.getAttribute("branding_directory"));
		 outputDoc.setAttribute("../organizationName",
					globalOrg.getAttribute("name"));
		 outputDoc.setAttribute("../authenticationMethod",
                                        globalOrg.getAttribute("authentication_method"));
                 outputDoc.setAttribute("../globalOrgOid",
                                        globalOrg.getAttribute("organization_oid"));
                 outputDoc.setAttribute("../clientBankCode", "");

	  }
	  catch (Exception e)
	  {
          e.printStackTrace();
		 mediatorServices.getErrorManager().issueError(getClass().getName(),
						AmsConstants.INVALID_ATTRIBUTE,
						String.valueOf(globalOrgOid), "authentication_method");
	  }

	  return outputDoc;
   }

   /**
	* This method is used for populating the mediator's output xml doc with the current
	* client bank user's organization info.
	*
	* @param organizationOid  long             - the unique oid of the user's organization
	* @param outputDoc        DocumentHandler  - the current mediator's output xml doc
	* @param mediatorServices MediatorServices - the current mediator's mediator services object
	* @return DocumentHandler outputDoc        - the output xml doc populated with organization info
	* @exception java.rmi.RemoteException
	* @exception com.amsinc.ecsg.frame.AmsException
	*/
   private DocumentHandler populateClientBankUserXmlDoc(long organizationOid, DocumentHandler outputDoc,
														MediatorServices mediatorServices)
														throws RemoteException, AmsException
   {
	  ClientBank   clientBank = null;

	  // Create the client bank business object using mediator services and instantiate it
	  clientBank = (ClientBank) mediatorServices.createServerEJB("ClientBank");
	  try
	  {
		 clientBank.getData(organizationOid);

		 // Populate the output doc with the appropriate branding directory, org name, and org oid
		 outputDoc.setAttribute("/Session/brandingDirectory", clientBank.getAttribute("branding_directory"));
		 outputDoc.setAttribute("../brandButtonInd", clientBank.getAttribute("brand_button_ind"));
		 outputDoc.setAttribute("../organizationName",     clientBank.getAttribute("name"));
		 outputDoc.setAttribute("../clientBankOid",        String.valueOf(organizationOid));
         outputDoc.setAttribute("../globalOrgOid", clientBank.getAttribute("global_organization_oid"));
         outputDoc.setAttribute("../clientBankCode", clientBank.getAttribute("OTL_id"));
		 outputDoc.setAttribute("../authenticationMethod", clientBank.getAttribute("authentication_method"));
		 outputDoc.setAttribute("../enableAdminUpdateCentre", clientBank.getAttribute("enable_admin_update_centre"));

	  }
	  catch (Exception e)
	  {
          e.printStackTrace();
		 mediatorServices.getErrorManager().issueError(getClass().getName(), AmsConstants.INVALID_ATTRIBUTE,
													   String.valueOf(organizationOid), "organization_oid");
	  }

	  return outputDoc;
   }

   /**
	* This method is used for populating the mediator's output xml doc with the current
	* BOG user's organization info.
	*
	* @param organizationOid  long             - the unique oid of the user's organization
	* @param outputDoc        DocumentHandler  - the current mediator's output xml doc
	* @param mediatorServices MediatorServices - the current mediator's mediator services object
	* @return DocumentHandler outputDoc        - the output xml doc populated with organization info
	* @exception java.rmi.RemoteException
	* @exception com.amsinc.ecsg.frame.AmsException
	*/
   private DocumentHandler populateBogUserXmlDoc(long organizationOid, DocumentHandler outputDoc,
												 MediatorServices mediatorServices)
												 throws RemoteException, AmsException
   {

	  BankOrganizationGroup   bankOrganizationGroup = null;
	  ClientBank   		  clientBank = null;

	  // Create the bank organization group business object using mediator services and instantiate it
	  bankOrganizationGroup = (BankOrganizationGroup) mediatorServices.createServerEJB("BankOrganizationGroup");

	  // Create the client bank business object using mediator services and instantiate it
	  clientBank = (ClientBank) mediatorServices.createServerEJB("ClientBank");

	  try
	  {
		 bankOrganizationGroup.getData(organizationOid);

		 // Populate the output doc with the appropriate branding directory, org name, org oid, and client bank oid
		 outputDoc.setAttribute("/Session/brandingDirectory",     bankOrganizationGroup.getAttribute("branding_directory"));
		 outputDoc.setAttribute("/Session/brandButtonInd",     bankOrganizationGroup.getAttribute("brand_button_ind"));
		 outputDoc.setAttribute("../organizationName",         bankOrganizationGroup.getAttribute("name"));
		 outputDoc.setAttribute("../bankOrganizationGroupOid", String.valueOf(organizationOid));
		 String clientBankOid = bankOrganizationGroup.getAttribute("client_bank_oid");
		 outputDoc.setAttribute("../clientBankOid", clientBankOid);

		// Need to get the authentication method from the BOG's client bank.
		 clientBank.getData(Long.parseLong(clientBankOid));

		 outputDoc.setAttribute("../authenticationMethod", clientBank.getAttribute("authentication_method"));
         outputDoc.setAttribute("../globalOrgOid", clientBank.getAttribute("global_organization_oid"));
         outputDoc.setAttribute("../clientBankCode", clientBank.getAttribute("OTL_id"));
         
         if ( TradePortalConstants.INDICATOR_YES.equals(clientBank.getAttribute("enable_admin_update_centre")) && 
        		 TradePortalConstants.INDICATOR_YES.equals(bankOrganizationGroup.getAttribute("enable_admin_update_centre"))) {
        	 outputDoc.setAttribute("../enableAdminUpdateCentre", TradePortalConstants.INDICATOR_YES );
         }

	  }
	  catch (Exception e)
	  {
          e.printStackTrace();
		 mediatorServices.getErrorManager().issueError(getClass().getName(), AmsConstants.INVALID_ATTRIBUTE,
													   String.valueOf(organizationOid), "organization_oid");
	  }

	  return outputDoc;
   }

   /**
	* This method is used for populating the mediator's output xml doc with the current
	* corporate org user's organization info.
	*
	* @param organizationOid  long             - the unique oid of the user's organization
	* @param user             User             - the current User business object
	* @param outputDoc        DocumentHandler  - the current mediator's output xml doc
	* @param mediatorServices MediatorServices - the current mediator's mediator services object
	* @return DocumentHandler outputDoc        - the output xml doc populated with organization info
	* @exception java.rmi.RemoteException
	* @exception com.amsinc.ecsg.frame.AmsException
	*/
   private DocumentHandler populateCorporateOrgUserXmlDoc(long organizationOid, User user, DocumentHandler outputDoc,
														  MediatorServices mediatorServices)
														  throws RemoteException, AmsException
   {
	  CorporateOrganization   corporateOrganization    = null;
	  BankOrganizationGroup   bankOrganizationGroup    = null;
          ClientBank              clientBank               = null;
	  String                  baseCurrencyCode         = null;
	  long                    bankOrganizationGroupOid = 0;

	  // Create the corporate organization business object using mediator services and instantiate it
	  corporateOrganization = (CorporateOrganization) mediatorServices.createServerEJB("CorporateOrganization");
	  try
	  {
		 corporateOrganization.getData(organizationOid);

		 // Retrieve the oid of the user's corporate org's BOG
		 bankOrganizationGroupOid = Long.parseLong(corporateOrganization.getAttribute("bank_org_group_oid"));

		 // Create the bank organization group business object using mediator services and instantiate it
		 bankOrganizationGroup = (BankOrganizationGroup) mediatorServices.createServerEJB("BankOrganizationGroup");
		 try
		 {
			 bankOrganizationGroup.getData(bankOrganizationGroupOid);

			 // Retrieve the base currency of the user's organization
			 baseCurrencyCode  = corporateOrganization.getAttribute("base_currency_code");

			 // Populate the output doc with the appropriate branding directory, org name, base currency code, bank
			 // organization group oid, and client bank oid
			 outputDoc.setAttribute("/Session/brandingDirectory",     bankOrganizationGroup.getAttribute("branding_directory"));
			 outputDoc.setAttribute("/Session/brandButtonInd",     bankOrganizationGroup.getAttribute("brand_button_ind"));
			 outputDoc.setAttribute("../organizationName",         corporateOrganization.getAttribute("name"));
			 outputDoc.setAttribute("../baseCurrencyCode",         baseCurrencyCode);
			 outputDoc.setAttribute("../bankOrganizationGroupOid", String.valueOf(bankOrganizationGroupOid));
			 outputDoc.setAttribute("../clientBankOid",            corporateOrganization.getAttribute("client_bank_oid"));
			 outputDoc.setAttribute("../orgAutoLCCreateIndicator", corporateOrganization.getAttribute("allow_auto_lc_create"));
			 outputDoc.setAttribute("../orgManualPOIndicator", corporateOrganization.getAttribute("allow_manual_po_entry"));

			 //TAILE
			 outputDoc.setAttribute("../orgAutoATPCreateIndicator", corporateOrganization.getAttribute("allow_auto_atp_create"));
			 outputDoc.setAttribute("../orgATPManualPOIndicator", corporateOrganization.getAttribute("allow_atp_manual_po_entry"));
			 //TAILE
			 outputDoc.setAttribute("../orgATPInvoiceIndicator", corporateOrganization.getAttribute("atp_invoice_indicator"));//SHR CR-708 Rel8.1.1

			 outputDoc.setAttribute("../authenticationMethod",
					 corporateOrganization.getAttribute("authentication_method"));
			 outputDoc.setAttribute("../timeoutAutoSaveInd", corporateOrganization.getAttribute("timeout_auto_save_ind"));
			 outputDoc.setAttribute("../orgDocPrepInd", corporateOrganization.getAttribute("doc_prep_ind"));	 
			 outputDoc.setAttribute("../cust_is_not_intg_tps",   corporateOrganization.getAttribute("cust_is_not_intg_tps"));  //MEer CR-1026          
			 
		 }
		 catch (Exception e)
		 {
             e.printStackTrace();
			mediatorServices.getErrorManager().issueError(getClass().getName(), AmsConstants.INVALID_ATTRIBUTE,
														  String.valueOf(organizationOid), "organization_oid (BOG)");
		 }

          long clientBankOid = Long.parseLong(corporateOrganization.getAttribute("client_bank_oid"));

	  clientBank = (ClientBank) mediatorServices.createServerEJB("ClientBank", clientBankOid);

	  outputDoc.setAttribute("../globalOrgOid",  clientBank.getAttribute("global_organization_oid"));
	  outputDoc.setAttribute("../clientBankCode",  clientBank.getAttribute("OTL_id"));	  

	  }
	  catch (Exception e)
	  {
          e.printStackTrace();
		 mediatorServices.getErrorManager().issueError(getClass().getName(), AmsConstants.INVALID_ATTRIBUTE,
													   String.valueOf(organizationOid), "organization_oid (CORPORATE)");
	  }
	  
	  //cquinton 12/13/2012 populate corporate org user preference data
	     if ( user!= null ) {
	     String userOid = user.getAttribute("user_oid");
	     if ( !StringFunction.isBlank( userOid ) ) {
	        populateRecentInstruments(userOid, outputDoc);
	     }
	  }
	 
	  return outputDoc;
   }

   private void populateRecentInstruments(String userOid, DocumentHandler outputDoc) {
      try {
    	//jgadela R91 IR T36000026319 - SQL INJECTION FIX
         String sql = "select transaction_oid, complete_instrument_id, transaction_type_code, transaction_status "+
         				" from recent_instrument where a_user_oid = ? order by display_order"; 
         
         DocumentHandler sqlDoc = DatabaseQueryBean.getXmlResultSet(sql, false, new Object[]{userOid});
         if (sqlDoc != null) {
            List<DocumentHandler> docList = sqlDoc.getFragmentsList("/ResultSetRecord");
            for( int i=0; i<docList.size(); i++ ) {
               DocumentHandler myDoc = docList.get(i);
               String transactionOid = myDoc.getAttribute("/TRANSACTION_OID");
               String instrumentId = myDoc.getAttribute("/COMPLETE_INSTRUMENT_ID");
               String transactionType = myDoc.getAttribute("/TRANSACTION_TYPE_CODE");
               String transactionStatus = myDoc.getAttribute("/TRANSACTION_STATUS");
               
               outputDoc.setAttribute("/UserPrefs/RecentInstrument("+i+")/transactionOid", 
                  transactionOid);
               outputDoc.setAttribute("/UserPrefs/RecentInstrument("+i+")/instrumentId", 
                  instrumentId);
               outputDoc.setAttribute("/UserPrefs/RecentInstrument("+i+")/transactionType", 
                  transactionType);
               outputDoc.setAttribute("/UserPrefs/RecentInstrument("+i+")/tranactionStatus", 
                  transactionStatus);
            }
         }
      }
      catch(Exception ex) {
         //if it doesn't work we don't really care. but lets at least write to log
         System.err.println("An error occurred retrieving recent instrument data for " +
            "userOid " + userOid + ". Exception: " + ex.toString());
      }
     
   }
   

   /**
	* This method is used for determining whether or not the user is currently logged into
	* Trade Portal in another session somewhere.
	*
	* @param user             User             - the current User business object
	* @param mediatorServices MediatorServices - the current mediator's mediator services object
	* @return boolean true  - the user is currently logged into TradePortal in another session
	* @return boolean false - the user is not currently logged into TradePortal
	* @exception java.rmi.RemoteException
	* @exception com.amsinc.ecsg.frame.AmsException
	*/
   private boolean isUserAlreadyLoggedIn(User user, MediatorServices mediatorServices)
										 throws RemoteException, AmsException
   {
        // See if a row already exists for the user in the ACTIVE_USER_SESSION table
        //cquinton 2/16/2012 Rel 8.0 ppx255 start
        //ensure we don't included forced out sessions...
	   //jgadela R91 IR T36000026319 - SQL INJECTION FIX
        String whereClause = "A_USER_OID = ?  and (force_logout!='Y' or force_logout is null)";
        //cquinton 2/16/2012 Rel 8.0 ppx255 end

        int count =
            DatabaseQueryBean.getCount("ACTIVE_USER_SESSION_OID", "ACTIVE_USER_SESSION", whereClause, false, new Object[]{user.getAttribute("user_oid")});

        if(count >= 1)
            {
			    //cquinton 2/16/2012 Rel 8.0 start
				//only issue an actual error in certain cases
				// see calls to this method
		        //mediatorServices.getErrorManager().issueError(getClass().getName(),
			    //                                            TradePortalConstants.ALREADY_LOGGED_IN,
				//									        user.getAttribute("user_identifier"));
			    //cquinton 2/16/2012 Rel 8.0 end
    	        return true;
            }
        else
            {
                return false;
            }
   }


   /**
	* This method is used for validating the user's password and logging him/her in upon success.
	*
	* @exception java.rmi.RemoteException
	* @exception com.amsinc.ecsg.frame.AmsException
	*/
   //cquinton 2/16/2012 Rel 8.0 ppx255 start
   //add userAlreadyLoggedIn argument
   private DocumentHandler loginUser(User user, String authMethod, boolean userAlreadyLoggedIn,
									 DocumentHandler outputDoc, MediatorServices mediatorServices)
									 throws RemoteException, AmsException
   //cquinton 2/16/2012 Rel 8.0 ppx255 end
   {
      SecurityProfile   securityProfile = null;

      boolean saveUserFlag = false;

          // Check password expiration.  If the user's password has expired, put an indicator
          // in outputDoc, which will forward the user to Change Password page.
          // Also add a flag to indicate if the user authenticated themselves with a password
          if (authMethod.equals(TradePortalConstants.AUTH_PASSWORD)
                  || authMethod.equals(TradePortalConstants.AUTH_2FA)) {
              checkPasswordExpiration(user, outputDoc);
              outputDoc.setAttribute("/passwordValidated", "true");
          }
          else {
              outputDoc.setAttribute("/passwordValidated", "false");
          }

         // If the user is valid, but the user is locked out, do not log them in
         if ( user.getAttribute("locked_out_ind").equals(TradePortalConstants.INDICATOR_NO) )
         {
		    // Place a flag in the document returned from the mediator
		    // indicating that logon was succesful.  The JSPs will examine this flag
		    outputDoc.setAttribute("/logonSuccesful","true");

		    // Retrieve the user's security profile
		    securityProfile = getUserSecurityProfile(user, mediatorServices);

            // Populate the output xml doc with the necessary user organization data
            //outputDoc = populateOutputDoc(user.getAttributeLong("owner_org_oid"),
            //                              user.getAttribute("ownership_level"),
            //                              user, outputDoc, mediatorServices);

            // Populate the output XML document with the attributes of the user and security profile business objects
		    user.populateXmlDoc(outputDoc);

		    // If the user is logging in for the first time, set the new User flag in
		    // outputDoc and unset the new user flag in the database
		    if(user.getAttribute("new_user_ind").equals(TradePortalConstants.INDICATOR_YES))
		    {
                //cquinton 12/20/2012 security update --
                //we cannot just simply reset new user ind here, we need to wait until the
                // user actually changes their password...
                //cquinton 2/16/2012 Rel 8.0 ppx255 start
                //only reset the new user flag if we are actually logging in here
                //if (!userAlreadyLoggedIn) {
                //    user.setAttribute("new_user_ind",TradePortalConstants.INDICATOR_NO);
                //    saveUserFlag = true;
                //}
                //cquinton 2/16/2012 Rel 8.0 ppx255 start
		        outputDoc.setAttribute("/newUser","true");
		    }

		    if(!user.getAttribute("invalid_login_attempt_count").equals("0"))
		     {
				// Since they have logged in succesfully, reset the invalid
				// logon count
				user.setAttribute("invalid_login_attempt_count","0");
				saveUserFlag = true;
		     }

		    securityProfile.populateXmlDoc(outputDoc);

            // Only put row into active user session if the logon is successful
            if(outputDoc.getAttribute("/logonSuccesful").equals("true"))
            {
                //cquinton 2/16/2012 Rel 8.0 ppx255 start
                if (!userAlreadyLoggedIn) {
                    //only set the login timestamp if actually logging in here
                    //Ravindra - Rel7100 -IR KMUL042736516 - 11th Aug 2011 - Start
                    saveUserFlag = true;
                    user.setAttribute("last_login_timestamp", user.getAttribute("login_timestamp"));//CR 821 Rel 8.3
                    //user.setAttribute("login_timestamp",DateTimeUtility.getGMTDateTime());
					user.setAttribute("login_timestamp",DateTimeUtility.getGMTDateTime(true,false)); //don't use override date for this one
                    //Ravindra - Rel7100 -IR KMUL042736516 - 11th Aug 2011 - Start

                    // Place a row into the ACTIVE_USER_SESSION table representing the fact that the user is logged in
                    // ActiveUserSession also automatically sets an attribute that stores the name of the server
                    // instance on which it was created.  That way, if the server goes down, it will be known
                    // what data needs to be cleaned up
                    ActiveUserSession activeSession = (ActiveUserSession)
                        mediatorServices.createServerEJB("ActiveUserSession");
                    activeSession.newObject();
                    activeSession.setAttribute("user_oid", user.getAttribute("user_oid") );
                    activeSession.setAttribute("logon_type","initial");
//                    activeSession.setAttribute("logon_type","relogon");
                    activeSession.save();

                    //return the new active user session identifier
                    String activeUserSessionOid = activeSession.getAttribute("active_user_session_oid");
                    outputDoc.setAttribute("/activeUserSessionOid", activeUserSessionOid);
                    outputDoc.setAttribute("/lastLoginTimestamp", user.getAttribute("last_login_timestamp"));//CR 821 Rel 8.3 
                } else { //user is already logged in
                    //tell CheckLogon.jsp to forward to ReLogon page
                    //note this is only done if user is allowed to relogon
                    // so check doesn't have to reoccur in the jsp
                    outputDoc.setAttribute( "/forwardToReLogon",
                        TradePortalConstants.INDICATOR_YES);
                }

                //cquinton 2/16/2012 Rel 8.0 ppx255 end
             }

		    mediatorServices.debug("finished populating output doc");
		 }
		else
		 {
		    // Don't throw any errors, but set the flag indicating that the user is locked out
		    outputDoc.setAttribute("/lockedOut","true");
		    outputDoc.setAttribute("/lockedOutNumFailedLogons",(String)user.getSecurityLimits().get("maxFailedLogons"));
         }


	  if(saveUserFlag)
	      user.save(false);

	  return outputDoc;
   }

   /**
    *
    * @param password
    * @param user
    * @param outputDoc
    * @param mediatorServices
    * @return
    * @throws RemoteException
    * @throws AmsException
    */
   private boolean validatePassword(String password, User user, DocumentHandler outputDoc, MediatorServices mediatorServices)
   throws RemoteException, AmsException {
	   // [START] CR-543
	   String[] knownChallengeResponses = null;
	   String eMacKeyString = null;
	   boolean isInstanceHSMEnabled = HSMEncryptDecrypt.isInstanceHSMEnabled();
	   if(isInstanceHSMEnabled && !Password.isUserPasswordLegacy(user))
	   {
		   knownChallengeResponses = new String[1];
		   knownChallengeResponses[0] = user.getAttribute("password");
		   eMacKeyString = password;
	   }
	  
	   boolean ispasswordValid = false;
	   boolean isUserPasswordLegacy = Password.isUserPasswordLegacy(user);

	   if (isUserPasswordLegacy && !user.getAttribute("password").equals("") && user.getAttribute("password").equals(Password.encryptPassword(password, user.getAttribute("encryption_salt"))))
		   ispasswordValid = true;
	   else if (!isUserPasswordLegacy && isInstanceHSMEnabled && !user.getAttribute("password").equals("") && Password.validateProtectedPassword(knownChallengeResponses, eMacKeyString))
		   ispasswordValid = true;
	   else if (!isUserPasswordLegacy && !isInstanceHSMEnabled)
		   mediatorServices.getErrorManager().issueError(getClass().getName(), TradePortalConstants.USER_NOT_CONFIGURED_FOR_PASSSWORD);
	   else
	   {
		   // Issue an error indicating that the password entered by the user is invalid
		   mediatorServices.getErrorManager().issueError(getClass().getName(), TradePortalConstants.INVALID_PASSWORD);

		   // Increment the invalid login attempt count for the user
		   int currentInvalidLoginAttemptCount = user.getAttributeInteger("invalid_login_attempt_count");
		   user.setAttribute("invalid_login_attempt_count",String.valueOf(++currentInvalidLoginAttemptCount));

		   // If the user has exceeded their limit, lock them out
		   checkIfUserShouldBeLockedOut(user);
		   user.save(false);
	   }

	   return ispasswordValid;
	   // Vshah - IR#AOUL063030265 - Rel7.1 - 09/07/2011 - [END]

   }
   /**

    * @date 10/27/2011
    * @param transId - A unique identifier for each transaction/request
    * @param tokenApplicationID - a concatenation of token id that defined on corporate customer profile page
    * and application id from CAAS (defined in Tradeportal.properties file)
    * @param oneTimePassword - entry on the token typed in by the user
    * @param user - current logged in user
    * @param outputDoc
    * @param mediatorServices
    * @return
    * @throws RemoteException
    * @throws AmsException
    */
   private boolean validatePassword2FAUsingVasco(String transID, String tokenApplicationID, String oneTimePassword, User user,
		   DocumentHandler inputDoc, DocumentHandler outputDoc, MediatorServices mediatorServices )
   throws RemoteException, AmsException {


	   String serviceURL = "";
	   String auth2FALogOid="";
	   String ignoreValidation2FA = ConfigSettingManager.getConfigSetting("PORTAL_APP", "PORTAL", "2FAignoreValidation", "N");
	   try {
		   serviceURL = portalProperties.getString("VascoSSASSoapEndpointAddress");
	   }
	   catch (MissingResourceException e){
		   LOG.debug(" THE VASCO SSAS soap endpoint is missing: "+e);
		   //The default URL specified in the WSDL file will be used TODO: add code to retrieve the defaul one if necessary
	   }
	   if ("Y".equals(ignoreValidation2FA)) {
		   LOG.info("[LogonMediatorBean.validatePassword2FA Using VASCO] loginId=" + user.getAttribute("login_id") + ";2FAignoreValidation=Y");
		   return true;
	   }

	   try {
		   // Create an instance of the SOAP service.
		   SSASSoapServiceLocator soapService = new SSASSoapServiceLocator();
		   // Set the URL explicitly based on configuration
		   try{
			   soapService.setSSASSoapEndpointAddress(serviceURL);
		   }catch(Exception e){
			   LOG.debug(">> LogonMediatorBean >> obtaining soapservice: "+e);

		   }

		   SSASSoap soapCtx = soapService.getSSASSoap();
		   DigiPassResponse response = soapCtx.doDigiPassVerifyPassword(transID, null, tokenApplicationID, oneTimePassword);
		   // Check the response.
		   BasicResponse basicResponse = response.getBasicResponse();
		   // Check the status.
		   String status = basicResponse.getStatus();
		   if (status.equalsIgnoreCase("ERROR")) {
			   SSASError errors[] = basicResponse.getErrors();
 	 		   LOG.info("VASCO Debug: Error returned from VASCO server: ");
 			   for (int i=0; i<errors.length; i++) {
 				   //LOG.debug("AdditionalInfo: " + errors[i].getAdditionalInfo());
 				   //LOG.debug("   Description: " + errors[i].getDescription());
 				   //LOG.debug("          Name: " + errors[i].getName());
 	 	 		   //SOP used for VASCO debugging in order to capture output always in the out log
 				   LOG.info("VASCO Debug: \nVASCO: LogonMediator:\nVASCO: AdditionalInfo: " + errors[i].getAdditionalInfo()+" \nVASCO: Description: "+errors[i].getDescription()+" \nVASCO: Name: "+ errors[i].getName());
 				   //Display to the user Transaction signing specific error message
 				   //IR#DRUM060436621 - AAlubala - Changed the message to a generic one
 				    mediatorServices.getErrorManager().issueError(getClass().getName(),TradePortalConstants.INVALID_PASSWORD_2FA);
 			   }
			   return false;
		   }
		   //log the in the audit table.
		   //V indicates that it is a VASCO token
		   auth2FALogOid = logAuth2FARequest(
		    		user.getAttribute("login_id"), serviceURL,
		    		null, null, "V" );

	    } catch(UnknownHostException uhe){
			   removeAuth2FALog(auth2FALogOid);
			   //Display the server unknown or unreachable error to the user
//			   mediatorServices.getErrorManager().issueError(getClass().getName(), TradePortalConstants.ERROR_VALIDATE_2FA, mediatorServices.getResourceManager().getText("LogonMediatorBean.SSASSMessage",
//	                   TradePortalConstants.TEXT_BUNDLE));
				   //IR#DRUM060436621 - AAlubala - Changed the message to a generic one
			    mediatorServices.getErrorManager().issueError(getClass().getName(),TradePortalConstants.INVALID_PASSWORD_2FA);
			  //SOP used for VASCO debugging in order to capture output always in the out log
			    LOG.info("VASCO Debug: \nVASCO:UnknownHostException: LogonMediatorBean: validatepassword2fausingvasco "+uhe);
	    }catch(Exception e){
		   removeAuth2FALog(auth2FALogOid);
//		   mediatorServices.getErrorManager().issueError(getClass().getName(), TradePortalConstants.ERROR_VALIDATE_2FA, mediatorServices.getResourceManager().getText("LogonMediatorBean.SSASSMessage",
//                   TradePortalConstants.TEXT_BUNDLE));
			   //IR#DRUM060436621 - AAlubala - Changed the message to a generic one
		    mediatorServices.getErrorManager().issueError(getClass().getName(),TradePortalConstants.INVALID_PASSWORD_2FA);
		  //SOP used for VASCO debugging in order to capture output always in the out log
		    LOG.info("VASCO Debug: \nVASCO:Exception: LogonMediatorBean: validatepassword2fausingvasco "+e);
		  return false;
	   }

	   return true;
   }

/**

 * @param loginId
 * @param password2FA
 * @param recertificationIndicator
 * @param user
 * @param outputDoc
 * @param mediatorServices
 * @return
 * @throws RemoteException
 * @throws AmsException
 */
private boolean validatePassword2FA(String tokenId2FA, String password2FA,
		String recertificationIndicator, User user, DocumentHandler inputDoc, DocumentHandler outputDoc, MediatorServices mediatorServices,
		String reAuthObjectOids, String reAuthObjectType )
throws RemoteException, AmsException {

    //PropertyResourceBundle portalProperties = (PropertyResourceBundle)PropertyResourceBundle.getBundle("TradePortal");

    String host = null;
    String sharedSecret = null;
    int authenticationPort;
    int accountingPort;
    int serverNumber2FA;
    int serverTimeOut;
    String ignoreValidation2FA = ConfigSettingManager.getConfigSetting("PORTAL_APP", "PORTAL", "2FAignoreValidation", "N");
    
    if ("Y".equals(ignoreValidation2FA)) {
        LOG.info("[LogonMediatorBean.validatePassword2FA] loginId=" + user.getAttribute("login_id") + ";2FAignoreValidation=Y");
        return true;
    }

    //add ability to test call to 2fa server
    String testValidation2FA = ConfigSettingManager.getConfigSetting("PORTAL_APP", "PORTAL", "2FAtestValidation", "N");

    // Number of Radius servers.  We rotate among them in round robin.
    //Rel9.0 IR#T36000020029 - Obtain 2FAserverNumber from config_setting table
    try {
		serverNumber2FA = Integer.parseInt(ConfigSettingManager.getConfigSetting("PORTAL_APP", "PORTAL", "2FAserverNumber", "1"));
	} catch (NumberFormatException e1) {
		LOG.debug("Exception retrieving serverNumber2FA from config_setting");
		serverNumber2FA = 1;
	}

    currentServerNumber2FA = (currentServerNumber2FA + 1) % serverNumber2FA;

    try {
        if (inputDoc.getAttribute("/2FAserverName") != null) {
            // If the user enters a password and gets a response of RadiusPacket.ACCESS_CHALLENGE,
            // the user will enter the password again and the server name is stored to make sure we
            // use the same server.
            host = inputDoc.getAttribute("/2FAserverName");
        }
        else {
        	//Rel9.0 IR#T36000020029 - Obtain 2FAserverName from config_setting table
            try {
				host =  ConfigSettingManager.getConfigSetting("PORTAL_APP", "PORTAL", "2FAserverName"+currentServerNumber2FA, null);
			} catch (Exception e) {
				LOG.info("Exception retrieving the host from config_setting: "+e);
				e.printStackTrace();
			}
        }

        if (inputDoc.getAttribute("/2FAsharedSecret") != null) {
            sharedSecret = inputDoc.getAttribute("/2FAsharedSecret");
        }
        else {
            sharedSecret = portalProperties.getString("2FAsharedSecret"+currentServerNumber2FA);
        }
    }
    catch (java.util.MissingResourceException e){
        LOG.info ("Missing Resource Exception with error:  "+e.toString());
        mediatorServices.getErrorManager().issueError(getClass().getName(), TradePortalConstants.ERROR_VALIDATE_2FA, e.getMessage());
        return false;
    }

    try {
        authenticationPort = Integer.parseInt(portalProperties.getString("2FAauthencitationPort"+currentServerNumber2FA));
    }
    catch (java.util.MissingResourceException e){
        authenticationPort = 1812;
    }

    try {
        accountingPort = Integer.parseInt(portalProperties.getString("2FAaccountingPort"+currentServerNumber2FA));
    }
    catch (java.util.MissingResourceException e){
        accountingPort = 1813;
    }

    try {
        serverTimeOut = Integer.parseInt(portalProperties.getString("2FAserverTimeOut"+currentServerNumber2FA));
    }
    catch (java.util.MissingResourceException e){
        serverTimeOut = 6000;
    }

    LOG.debug("[LogonMediatorBean.validatePassword2FA] loginId="+user.getAttribute("login_id") + ";token_id_2fa=" + tokenId2FA+";host="+host+";authenticationPort="+authenticationPort+";accountingPort="+accountingPort+";serverTimeOut="+serverTimeOut);
    String auth2FALogOid = logAuth2FARequest(
    		user.getAttribute("login_id"), host,
    		reAuthObjectOids, reAuthObjectType, "R" );
    RadiusPacket accessResponse = null;
    try{
        RadiusClient rc = new RadiusClient(host, authenticationPort, accountingPort, sharedSecret, serverTimeOut);
        RadiusPacket accessRequest = new RadiusPacket(RadiusPacket.ACCESS_REQUEST);
        accessRequest.setAttribute(new RadiusAttribute(RadiusAttributeValues.USER_NAME, tokenId2FA.getBytes()));
        accessRequest.setAttribute(new RadiusAttribute(RadiusAttributeValues.USER_PASSWORD,password2FA.getBytes()));

        //ir cnuk113043991 add ability to test 2fa call
        if ("Y".equals(testValidation2FA)) {
            //testing!!!
            if ( "PASS".equalsIgnoreCase(password2FA) ) {
                accessResponse = new RadiusPacket(RadiusPacket.ACCESS_ACCEPT);
            } else if ( "AGAIN".equalsIgnoreCase(password2FA)) {
                accessResponse = new RadiusPacket(RadiusPacket.ACCESS_CHALLENGE);
            } else {
                accessResponse = new RadiusPacket(RadiusPacket.ACCESS_REJECT);
            }
        } else {
            //the real thing
            accessResponse = rc.authenticate(accessRequest);
        }
    }catch(RadiusException rex){
        LOG.info("[LogonMediatorBean.validatePassword2FA] loginId="+user.getAttribute("login_id") + ";token_id_2fa=" + tokenId2FA+";host="+host+";authenticationPort="+authenticationPort+";accountingPort="+accountingPort+";serverTimeOut="+serverTimeOut);
        rex.printStackTrace();
        mediatorServices.getErrorManager().issueError(getClass().getName(), TradePortalConstants.ERROR_VALIDATE_2FA, rex.getMessage());
        logAuth2FAError(auth2FALogOid,"ERROR",rex.toString());
        return false;
    }catch(InvalidParameterException ivpex){
        LOG.info("[LogonMediatorBean.validatePassword2FA] loginId="+user.getAttribute("login_id") + ";token_id_2fa=" + tokenId2FA+";host="+host+";authenticationPort="+authenticationPort+";accountingPort="+accountingPort+";serverTimeOut="+serverTimeOut);
        ivpex.printStackTrace();
        mediatorServices.getErrorManager().issueError(getClass().getName(), TradePortalConstants.ERROR_VALIDATE_2FA, ivpex.getMessage());
        logAuth2FAError(auth2FALogOid,"ERROR",ivpex.toString());
        return false;
    }

    LOG.debug("[LogonMediatorBean.validatePassword2FA] loginId="+user.getAttribute("login_id") + ";token_id_2fa=" + tokenId2FA+";accessResponse.getPacketType()="+accessResponse.getPacketType());
    if (accessResponse.getPacketType()== RadiusPacket.ACCESS_REJECT) {
        LOG.info("[LogonMediatorBean.validatePassword2FA] loginId="+user.getAttribute("login_id") + ";token_id_2fa=" + tokenId2FA+";host="+host+";authenticationPort="+authenticationPort+";accountingPort="+accountingPort+";serverTimeOut="+serverTimeOut+";accessResponse.getPacketType()=RadiusPacket.ACCESS_REJECT("+accessResponse.getPacketType()+")");

         // Issue an error indicating that the password entered by the user is invalid
         mediatorServices.getErrorManager().issueError(getClass().getName(), TradePortalConstants.INVALID_PASSWORD_2FA);
        logAuth2FAError(auth2FALogOid,"ACCESS_REJECT");

         // Increment the invalid login attempt count for the user
         //ir cnuk113043991 - allow for multiple recertification indicator
         if (user != null &&
                 !TradePortalConstants.INDICATOR_YES.equals(recertificationIndicator) &&
                 !TradePortalConstants.INDICATOR_MULTIPLE.equals(recertificationIndicator)){
            int currentInvalidLoginAttemptCount = user.getAttributeInteger("invalid_login_attempt_count");
             user.setAttribute("invalid_login_attempt_count",String.valueOf(++currentInvalidLoginAttemptCount));

             // If the user has exceeded their limit, lock them out
             checkIfUserShouldBeLockedOut(user);
             user.save(false);
          }
          return false;

     }
     else if (accessResponse.getPacketType() == RadiusPacket.ACCESS_CHALLENGE) {
         LOG.info("[LogonMediatorBean.validatePassword2FA] loginId="+user.getAttribute("login_id") + ";token_id_2fa=" + tokenId2FA+";host="+host+";authenticationPort="+authenticationPort+";accountingPort="+accountingPort+";serverTimeOut="+serverTimeOut+";accessResponse.getPacketType()=RadiusPacket.ACCESS_CHALLENGE("+accessResponse.getPacketType()+")");
          // Store the host name shared secret so that we will use the same server for validation again
          outputDoc.setAttribute("/2FAserverName", host);
          outputDoc.setAttribute("/2FAsharedSecret", sharedSecret);
          // Issue a warning for the user to enter a 2FA password again after the token updates.
          mediatorServices.getErrorManager().issueError(getClass().getName(), TradePortalConstants.CHALLENGE_PASSWORD_2FA);
        logAuth2FAError(auth2FALogOid,"ACCESS_CHALLENGE");
          return false;
     }

    //success - remove the log entry
    removeAuth2FALog(auth2FALogOid);

     return true;
}


/**
	* This method is used for determining whether or not the user exists for the organization
	* associated with the login Id passed in.
	*
	* @param loginId          String           - the user's login Id
        * @param loginOrganizationId  String       - the ID of the user's organization.
	* @param mediatorServices MediatorServices - the current mediator's mediator services object
	* @return User user - the user business object for the user logging in
	* @exception java.rmi.RemoteException
	* @exception com.amsinc.ecsg.frame.AmsException
	*/
   private User validateLoginId(String loginId, String loginOrganizationId, MediatorServices mediatorServices)
								throws RemoteException, AmsException
   {
       String searchCritiera = " and u.login_id = ?";
       return retrieveUserRecord(loginOrganizationId, mediatorServices, searchCritiera, TradePortalConstants.AUTH_PASSWORD, loginId);
   }

   /**
	* This method is used for determining whether or not the user exists for the organization
	* associated with the certificate Id passed in.
	*
	* @param certificateId          String     - the user's certificate Id
        * @param loginOrganizationId  String       - the ID of the user's organization.
	* @param mediatorServices MediatorServices - the current mediator's mediator services object
	* @return User user - the user business object for the user logging in
	* @exception java.rmi.RemoteException
	* @exception com.amsinc.ecsg.frame.AmsException
	*/
   private User validateCertificateId(String certificateId, String loginOrganizationId, MediatorServices mediatorServices)
								throws RemoteException, AmsException
   {
       String searchCritiera =" and u.certificate_id = ?";
       return retrieveUserRecord(loginOrganizationId, mediatorServices, searchCritiera, TradePortalConstants.AUTH_CERTIFICATE, certificateId);
   }

   //BSL 08/19/11 CR-663 Begin
   /**
    * This method is used to validate the registration password of a
    * user registering for Single Sign-On.
    *
    * @param password
    * @param user
    * @param outputDoc
    * @param mediatorServices
    * @return
    * @throws RemoteException
    * @throws AmsException
    */
   private boolean validateSSORegistrationPassword(String password, User user,
		   DocumentHandler outputDoc, MediatorServices mediatorServices)
   throws RemoteException, AmsException
   {
	   boolean isPasswordValid = false;

	   if (Password.isUserPasswordLegacy(user) &&
			   StringFunction.isNotBlank(user.getAttribute("registration_password")) &&
			   user.getAttribute("registration_password").equals(Password.encryptPassword(password, user.getAttribute("encryption_salt"))))
	   {
		   isPasswordValid = true;
	   }
	   else
	   {
		   // Issue an error indicating that the password entered by the user is invalid
		   mediatorServices.getErrorManager().issueError(getClass().getName(),
				   TradePortalConstants.INVALID_REGISTRATION_PASSWORD);

		   // Increment the invalid login attempt count for the user
		   int currentInvalidLoginAttemptCount = user.getAttributeInteger("invalid_login_attempt_count");
		   user.setAttribute("invalid_login_attempt_count",
				   String.valueOf(++currentInvalidLoginAttemptCount));

		   // If the user has exceeded their limit, lock them out
		   checkIfUserShouldBeLockedOut(user);
		   user.save(false);
	   }

	   return isPasswordValid;
   }

   /**
	* This method is used for determining whether or not the user exists for
	* the organization associated with the registration login Id passed in.
	*
	* @param loginId String - the user's registration login Id
    * @param loginOrganizationId String - the ID of the user's organization.
	* @param mediatorServices MediatorServices - the current mediator's mediator services object
	* @return User user - the user business object for the user logging in
	* @exception java.rmi.RemoteException
	* @exception com.amsinc.ecsg.frame.AmsException
	*/
   private User validateSSORegistrationLoginId(String loginId,
		   String loginOrganizationId, MediatorServices mediatorServices)
   throws RemoteException, AmsException {
      String searchCritiera = " and u.registration_login_id = ?";
      return retrieveUserRecord(loginOrganizationId, mediatorServices, searchCritiera, TradePortalConstants.AUTH_REGISTERED_SSO, loginId);
   }

   /**
	* This method is used for determining whether or not the user exists for the organization
	* associated with the registered SSO ID passed in.
	*
	* @param ssoId String - the user's registered SSO (Single Sign-On) ID
	* @param loginOrganizationId String - the ID of the user's organization.
	* @param mediatorServices MediatorServices - the current mediator's mediator services object
	* @return User user - the user business object for the user logging in
	* @exception java.rmi.RemoteException
	* @exception com.amsinc.ecsg.frame.AmsException
	*/
   private User validateRegisteredSSOId(String ssoId,
		   String loginOrganizationId, MediatorServices mediatorServices)
   throws RemoteException, AmsException
   {
      String searchCritiera = " and u.registered_sso_id = ?";
      return retrieveUserRecord(loginOrganizationId, mediatorServices, searchCritiera, TradePortalConstants.AUTH_REGISTERED_SSO, ssoId);
   }
   //BSL 08/19/11 CR-663 End

   /**
	* This method is used for determining whether or not the user exists for the organization
	* associated with the certificate Id passed in.
	*
	* @param ssoId          String     - the user's sso (Single Sign-On) Id
       * @param loginOrganizationId  String       - the ID of the user's organization.
	* @param mediatorServices MediatorServices - the current mediator's mediator services object
	* @return User user - the user business object for the user logging in
	* @exception java.rmi.RemoteException
	* @exception com.amsinc.ecsg.frame.AmsException
	*/
  private User validateSSOId(String ssoId, String loginOrganizationId, String authLevel, MediatorServices mediatorServices)
								throws RemoteException, AmsException
  {
      // W Zhu 11/30/2015 Rel 9.4.0.1 T36000044771 Handle the case that we allow the bank to configure the user as Certificate Login but actually use SSO.
      String searchCriteria;
      if ("50".equals(authLevel)) {
          searchCriteria = " and decode(u.CERTIFICATE_TYPE, 'C', u.CERTIFICATE_ID, u.sso_id) = ? ";
      }
      else {
          searchCriteria = " and u.sso_id = ? ";
      }
      User user = retrieveUserRecord(loginOrganizationId, mediatorServices, searchCriteria, TradePortalConstants.AUTH_SSO, ssoId);
      
      LOG.info("LogonMediatorBean.validateSSOId: sso_id=" + ssoId + ";logingOrganizationId=" + loginOrganizationId+";authLevel="+authLevel);
      if (user!=null && TradePortalConstants.CAAS_AUTHLEVEL_PASSWORD.equals(authLevel)) {
          String allowSSOPassword = user.getAttribute("allow_sso_password_ind");
          if ("N".equals(allowSSOPassword)) {
               LOG.info("LogonMediatorBean: user with sso id = " + ssoId + " has allow_sso_password_ind = N" );
               mediatorServices.getErrorManager().issueError(getClass().getName(), TradePortalConstants.CERTIFICATE_NOT_VALID);
              return null;
          }
           String ownershipLevel = user.getAttribute("ownership_level");
           String ownerOrgOid = user.getAttribute("owner_org_oid");
           if (ownershipLevel.equals(TradePortalConstants.OWNER_CORPORATE) && ownerOrgOid != null && ownerOrgOid.length() > 0) {
                   Organization org = (Organization) mediatorServices.createServerEJB("CorporateOrganization",
                          user.getAttributeLong("owner_org_oid") );
                   String bankOrgGroupOid = org.getAttribute("bank_org_group_oid");
                   if (bankOrgGroupOid != null && bankOrgGroupOid.length() > 0) {
                       BankOrganizationGroup bankOrgGroup = (BankOrganizationGroup) mediatorServices.createServerEJB("BankOrganizationGroup", Long.valueOf(bankOrgGroupOid));
                       if (bankOrgGroup.getAttribute("no_sso_ind").equals(TradePortalConstants.INDICATOR_YES)) {
                           LOG.info("LogonMediatorBean: user with sso id = " + ssoId + " has bank org group's no_sso_ind = Y" );
                           mediatorServices.getErrorManager().issueError(getClass().getName(), TradePortalConstants.CERTIFICATE_NOT_VALID);
                          return null;                           
                       }
                   }
           }
      }
      return user;
  }

//Ravindra - 12/01/2011 - CR-541 - Start
  private User validateAESAuth(String ssoId, String loginOrganizationId, MediatorServices mediatorServices)
	throws RemoteException, AmsException
	{
	  String searchCritiera = " and u.sso_id = ?";
	  //Validate user if profile has been set for SSO
	  return retrieveUserRecord(loginOrganizationId, mediatorServices, searchCritiera, TradePortalConstants.AUTH_AES256, ssoId);

	}
//Ravindra - 12/01/2011 - CR-541 - End
   /**
	* This method is used for determining whether or not the user exists for the organization
	* associated with the passed in search criteria (either certificate_id or login_id)
	*
        * @param loginOrganizationId  String       - the ID of the user's organization.
	* @param mediatorServices MediatorServices - the current mediator's mediator services object
	* @param searchCriteria String             - a where clause condition for searching for a user record based
	*                                            on either login_id or certificate_id
	* @param authenticationMethod String - the authentication method that
	* is used to validate this user.  Either TradePortalConstants.
	* AUTH_CERTIFICATE or TradePortalConstants.AUTH_PASSWORD. For a user of a
	* corporate organization , this should match the authentication method of
	* the corporate organization and the user.
	* @return User user - the user business object for the user logging in
	* @exception java.rmi.RemoteException
	* @exception com.amsinc.ecsg.frame.AmsException
	*/
   private User retrieveUserRecord(String loginOrganizationId, MediatorServices mediatorServices,
                                String searchCriteria, String authenticationMethod, String searchCritieraParam)
								throws RemoteException, AmsException
   {
       //jgadela R90 IR T36000026319 - SQL INJECTION FIX
       List<Object> sqlParams = new ArrayList();
       String sql = null;

       // Validate the login ID within the appropriate organization
       if(loginOrganizationId.equalsIgnoreCase(TradePortalConstants.GLOBAL_ORG_ID))
       {
          // For global org users, look for the active login ID within only global org users
    	  // Nar CR-1071 added global_organization also to get owner org 'authentication_method'.
          sql = "select u.user_oid, g.authentication_method from users u, global_organization g where u.p_owner_org_oid = g.organization_oid "
          		+ " and u.ownership_level = ? "+searchCriteria+
        	  	" and u.activation_status = ?";
          sqlParams.add(TradePortalConstants.OWNER_GLOBAL);
          sqlParams.add(searchCritieraParam);
          sqlParams.add(TradePortalConstants.ACTIVE);
       }
       else
       {
          // For all other levels, look for the active login ID within the users associated to
          // a client bank or its descendants
          //cquinton 4/4/2011 cr541 - add client bank auth method
          sql = "select u.user_oid, cb.authentication_method from users u, client_bank cb where u.a_client_bank_oid = cb.organization_oid"+
          	" and cb.otl_id = ? "+searchCriteria +" and u.activation_status = ?";
          sqlParams.add(loginOrganizationId.toUpperCase());
          sqlParams.add(searchCritieraParam);
          sqlParams.add(TradePortalConstants.ACTIVE);
       }

       User returnObject = null;

       DocumentHandler result = DatabaseQueryBean.getXmlResultSet(sql, true, sqlParams);

       if(result == null)
       {
	  LOG.info("unable to retrieve user record based on criteria: " + searchCriteria);
	  LOG.info("    and org " + loginOrganizationId);
	  //BSL 08/22/11 CR663 Rel 7.1 Begin
	  // A different error message is displayed if this is from the SSO
	  // registration login page
	  if (TradePortalConstants.AUTH_REGISTERED_SSO.equals(authenticationMethod)) {
		  mediatorServices.getErrorManager().issueError(getClass().getName(), TradePortalConstants.INVALID_REGISTRATION_PASSWORD);
	  }
	  else {
		  mediatorServices.getErrorManager().issueError(getClass().getName(), TradePortalConstants.INVALID_PASSWORD);
	  }
	  //BSL 08/22/11 CR663 Rel 7.1 End
 	  returnObject = null;
       }
       else
       {
          long userOid = result.getAttributeLong("/ResultSetRecord(0)/USER_OID");

          // Create the user business object using mediator services and instantiate it
          User user = (User) mediatorServices.createServerEJB("User");
           try {
               user.getData(userOid);
               returnObject = user;
           } catch (Exception e) {
               //LOG.info("Logon Mediator error: " + e.toString());
               e.printStackTrace();
               mediatorServices.getErrorManager().issueError(getClass().getName(), AmsConstants.INVALID_ATTRIBUTE,
                       String.valueOf(userOid), "user_oid");

               returnObject = null;
           }
           

           // Make sure the user is using the correct authentication method
           Organization org = null;
           String ownershipLevel = user.getAttribute("ownership_level");
           if (ownershipLevel.equals(TradePortalConstants.OWNER_CORPORATE)) {
               try {
                   org = (Organization) mediatorServices.createServerEJB("CorporateOrganization",
                           user.getAttributeLong("owner_org_oid"));
               } catch (Exception e) {
                   e.printStackTrace();
                   // Ignore the exception here?
               }
               if (org != null) {
                   // W Zhu 12/8/09 CR-482 Treat Password as equivalent to 2FA here since the system does not
                   String userAuthMethod; // the authentication method that the user is configured for.
                   if (!org.getAttribute("authentication_method").equals(TradePortalConstants.AUTH_PERUSER)) {
                       userAuthMethod = org.getAttribute("authentication_method");
                   }
                   else {
                       userAuthMethod = user.getAttribute("authentication_method");
                   }
                   if (userAuthMethod.equals(authenticationMethod)
                       || userAuthMethod.equals(TradePortalConstants.AUTH_2FA)
                           && authenticationMethod.equals(TradePortalConstants.AUTH_PASSWORD)
                       || userAuthMethod.equals(TradePortalConstants.AUTH_CERTIFICATE)
                           && authenticationMethod.equals(TradePortalConstants.AUTH_SSO)) {
                       // Authentication method is correct
                   } //Ravindra - 03/31/2011 - CDUL031849759 - Start
                   else if (authenticationMethod.equals(TradePortalConstants.AUTH_AES256)) {
                       if (org.getAttribute("authentication_method").equals(TradePortalConstants.AUTH_SSO)
                               || (org.getAttribute("authentication_method").equals(TradePortalConstants.AUTH_PERUSER)
                               && user.getAttribute("authentication_method").equals(TradePortalConstants.AUTH_SSO))) {
                           //Authentication method is correct
                       } else {
                           mediatorServices.getErrorManager().issueError(getClass().getName(), TradePortalConstants.INCOMPLETE_USER_PROVISIONING);
                           returnObject = null;
                       }
                   } //Ravindra - 03/31/2011 - CDUL031849759 - End
                   else if (authenticationMethod.equals(TradePortalConstants.AUTH_PASSWORD)) { //Certificate user trying to use password

                       LOG.info("user logging on with password, but certificate reqd");
                       mediatorServices.getErrorManager().issueError(getClass().getName(), TradePortalConstants.INVALID_PASSWORD);
                       returnObject = null;
                   } else { //Password user trying to use certificate
                       LOG.info("user logging on with certificate, but password reqd");
                       mediatorServices.getErrorManager().issueError(getClass().getName(), TradePortalConstants.CERTIFICATE_NOT_VALID);
                       returnObject = null;
                   }

               }
               
           } //cquinton 4/4/2011 cr541 start - return incomplete user provisioning for admin users
           else {
               // Nar CR-1071 Rel9350 09/02/2015 Add- Begin
               // modify code to validate authentication method at Admin user level also same as corporate user.
               String adminUserBankAuthMethod = result.getAttribute("/ResultSetRecord(0)/AUTHENTICATION_METHOD");
               /**
                * below is four condition to return success
                * 1. if Login authentication method is equal to Bank authentication method.
                *     valid login authentication mehtod is- PASSWORD, SSO, 2FA and CERTIFICATE
                * 2. if BANK is setup for PERUSER and Login authentication methed is equal to Admin user authentication method.
                * 3. if Login authentication methed is PASSWORD and Bank is setup for 2FA.
                * 4. if Bank is setup for PERUSER and Login authentication methed is PASSWORD and admin user setup for 2FA.
                */
               if (adminUserBankAuthMethod.equals(authenticationMethod)
                       || (adminUserBankAuthMethod.equals(TradePortalConstants.AUTH_PERUSER)
                       && user.getAttribute("authentication_method").equals(authenticationMethod))
                       || (adminUserBankAuthMethod.equals(TradePortalConstants.AUTH_2FA)
                       && authenticationMethod.equals(TradePortalConstants.AUTH_PASSWORD))
                       || (adminUserBankAuthMethod.equals(TradePortalConstants.AUTH_PERUSER)
                       && user.getAttribute("authentication_method").equals(TradePortalConstants.AUTH_2FA)
                       && authenticationMethod.equals(TradePortalConstants.AUTH_PASSWORD))) {
                   // Authentication method is correct
               } else if (TradePortalConstants.AUTH_AES256.equals(authenticationMethod)) {
                   //only do this for non-ASP
                   if (!TradePortalConstants.GLOBAL_ORG_ID.equalsIgnoreCase(loginOrganizationId)) {
                       if (TradePortalConstants.AUTH_SSO.equals(adminUserBankAuthMethod)
                               || (adminUserBankAuthMethod.equals(TradePortalConstants.AUTH_PERUSER)
                               && user.getAttribute("authentication_method").equals(TradePortalConstants.AUTH_SSO))) {
                           //Authentication method is correct
                       } else {
                           mediatorServices.getErrorManager().
                                   issueError(getClass().getName(), TradePortalConstants.INCOMPLETE_USER_PROVISIONING);
                           returnObject = null;
                       }
                   }
               } else if (authenticationMethod.equals(TradePortalConstants.AUTH_PASSWORD)) { //Certificate user trying to use password
                   LOG.info("user logging on with password, but certificate reqd");
                   mediatorServices.getErrorManager().issueError(getClass().getName(), TradePortalConstants.INVALID_PASSWORD);
                   returnObject = null;
               } else { //Password user trying to use certificate
                   LOG.info("user logging on with certificate, but password reqd");
                   mediatorServices.getErrorManager().issueError(getClass().getName(), TradePortalConstants.CERTIFICATE_NOT_VALID);
                   returnObject = null;
               }
               // Nar CR-1071 Rel9350 09/02/2015 Add- End
           }
           //cquinton 4/4/2011 cr541 end
       }

     return returnObject;
   }




	/**
	 * CR-711 - Rel 8.0 - Added a new parameter (tokenType)
     * Log the 2FA request.
     * Becuase its just logging we don't use an ejb, just straight jdbc.
     * Still use the standard OID sequence for generating a new log oid,
     * though.
     *
     * @param loginId - the user id
     * @param serverName - the radius server. could be a name or ip
     *            address depending on the config entry
     * @param reAuthObjectOids - the oids of the objects associated to the
     *            reauthentication. null for login
     * @param reAuthObjectType - the type of object being
     *            reauthenticated - free format. null for login
     * @param tokenType The Type of token, for now it is either RSA - R or VASCO - V
     * @return oid of the new log record, or null if an issue occurred
     */
    protected String logAuth2FARequest(String loginId, String serverName,
                    String reAuthObjectOids, String reAuthObjectType, String tokenType) throws AmsException {
        //ir cnuk113043991 - multiple reAuthObjectOids
        //for now just log the first one
        //IR#ABUM050271070 - start
        //If reAuthOid is null for example when coming from a detail page
        //just set it to zero "0" instead of null
        //the database column is expecting a numeric value, so, null causes a sql exception
    	//
        String reAuthOid = "0";
        //
        //ir#ABUM050271070 - end
        if ( reAuthObjectOids != null && reAuthObjectOids.length() > 0 ) {
            StringTokenizer strTok = new StringTokenizer(reAuthObjectOids,"!");
            while (strTok.hasMoreTokens()) {
                reAuthOid = strTok.nextToken();
            }
        }


        //CR711. Rel8.0 - AAlubala, 04/17/2012 - Truncate the server name if it is longer than 100 characters
        if(serverName.length()>100)serverName=serverName.substring(0, serverName.length());

        String logOid = null;
        try(Connection con = DatabaseQueryBean.connect(true);
			PreparedStatement pStmt = con.prepareStatement(
	                "insert into auth_2fa_log " +
	                "(auth_2fa_log_oid, status, request_datetime, " +
	                "login_id, server_name, " +
	                "re_auth_object_oid, re_auth_object_type, token_type ) " +
	                "values " +
	                "(?,'S',sysdate, " +
	                "?,?," +
	                "?,?,?)" )) {
            logOid = Long.toString(ObjectIDCache.getInstance(AmsConstants.OID).
                generateObjectID(false));
			
				pStmt.setString(1, logOid);
				pStmt.setString(2, loginId);
				pStmt.setString(3, serverName);
				pStmt.setString(4, reAuthOid);
				pStmt.setString(5, reAuthObjectType);
				pStmt.setString(6, tokenType);
				pStmt.executeUpdate();
				
		} catch (Exception ex) {
            //because this is not vital to processing, ignore it but create a log message
            System.err.println("Exception logging authentication request: " + ex.toString());
        } 
        return logOid;
    }

    /**
     * Log the 2FA response error.
     *
     * @param logOid - unique identifier for the log request
     * @param responseCode - either ERROR for a free format exception,
     *            ACCESS_REJECT or ACCESS_CHALLENGE
     * @param errorMessage - detailed exception info to be provided for ERROR
     *            responseCode
     */
    protected void logAuth2FAError(String logOid,
                    String responseCode, String errorMessage ) {
        if ( logOid == null || logOid.length() <= 0 ) {
            return;
        }

        try(Connection con = DatabaseQueryBean.connect(true);
			PreparedStatement pStmt = con.prepareStatement(
                "update auth_2fa_log set " +
                "status='E', response_datetime=sysdate, " +
                "response_code=?, error_message=? " +
                "where auth_2fa_log_oid=?" )) {
			
			pStmt.setString(1, responseCode);
			pStmt.setString(2, errorMessage);
			pStmt.setString(3, logOid);
			pStmt.executeUpdate();
			
		} catch (Exception ex) {
            //because this is not vital to processing, ignore it but create a log message
            System.err.println("Exception logging authentication error: " + ex.toString());
		}
    }
    private void logAuth2FAError(String logOid,
                    String responseCode ) {
    	logAuth2FAError(logOid, responseCode, null);
    }

    /**
     * Log the 2FA response error.
     *
     * @param logOid - unique identifier for the log request
     * @param responseCode - either ERROR for a free format exception,
     *            ACCESS_REJECT or ACCESS_CHALLENGE
     * @param errorMessage - detailed exception info to be provided for ERROR
     *            responseCode
     */
    protected void logAuth2FASuccess(String logOid,
                    String responseCode, String responseMessage ) {
        if ( logOid == null || logOid.length() <= 0 ) {
            return;
        }
        String stm ="update auth_2fa_log set " +
        "status='C', response_datetime=sysdate, " +
        "response_code=?, error_message=? " +
        "where auth_2fa_log_oid=?";
        try(Connection con = DatabaseQueryBean.connect(true); PreparedStatement pStmt = con.prepareStatement(
                stm )) {
                pStmt.setString(1, responseCode);
                pStmt.setString(2, responseMessage);
                pStmt.setString(3, logOid);
                int rowCount = pStmt.executeUpdate();
               

            } catch (Exception ex) {
                //because this is not vital to processing, ignore it but create a log message
                System.err.println("Exception logging authentication success: " + ex.toString());
            }
    }

    /**
     * Remove the 2FA log entry.
     *
     * @param logOid - unique identifer for the entry to remove
     */
    protected void removeAuth2FALog(String logOid) {
        if ( logOid == null || logOid.length() <= 0 ) {
            return;
        }

        try(Connection con = DatabaseQueryBean.connect(true);
			PreparedStatement pStmt = con.prepareStatement(
                "delete from auth_2fa_log " +
                "where auth_2fa_log_oid=?" )) {
			
			pStmt.setString(1, logOid);
			pStmt.executeUpdate();
			
		} catch (Exception ex) {
            //because this is not vital to processing, ignore it but create a log message
            System.err.println("Exception removing authentication log entry: " + ex.toString());
		}
    }

    /**

     * @param tokenId2FA
     * @param password2fa
     * @param recertificationIndicator
     * @param user
     * @param inputDoc
     * @param outputDoc
     * @param mediatorServices
     * @param reAuthObjectOids
     * @param reAuthObjectType
     * @return
     */
    private boolean validatePassword2FAUsingRSA(String tokenId2FA,
    		String password2fa, String recertificationIndicator, User user,
    		DocumentHandler inputDoc, DocumentHandler outputDoc,
    		MediatorServices mediatorServices, String reAuthObjectOids,
    		String reAuthObjectType, boolean isNextCode)  throws RemoteException,AmsException, AuthAgentException{

        String ignoreValidation2FA = ConfigSettingManager.getConfigSetting("PORTAL_APP", "PORTAL", "2FAignoreValidation", "N");
        
        if ("Y".equals(ignoreValidation2FA)) {
            LOG.info("[LogonMediatorBean.RSA validatePassword2FAUsingRSA] loginId=" + user.getAttribute("login_id")    + ";2FAignoreValidation=Y");
            return true;
        }

        //add ability to test call to 2fa server
        String testValidation2FA = ConfigSettingManager.getConfigSetting("PORTAL_APP", "PORTAL", "2FAtestValidation", "N");
        if ("Y".equals(ignoreValidation2FA)) {
            LOG.info("[LogonMediatorBean.RSA validatePassword2FAUsingRSA] loginId=" + user.getAttribute("login_id")    + ";2FAtestValidation=Y");
            return true;
        }

	 int authStatus=AuthSession.ACCESS_DENIED;
	 String path = null;
	 	try{
			path =  portalProperties.getString("RSA_ConfigFilePath");
	 	}catch (Exception e){
	 		//this should never happen but in case the path is not set log the error.
	 		e.printStackTrace();
	 	}
        if (StringFunction.isBlank(path)){
        	path = ConfigSettingManager.getConfigSetting("PORTAL_APP", "PORTAL", "RSA_ConfigFilePath", null);
        }

         if (path == null || "".equals(path)) {
	     mediatorServices.getErrorManager().issueError(getClass().getName(), TradePortalConstants.ERROR_VALIDATE_2FA, "CONFIG_SETTING RSA_ConfigFilePath not configured.");
             return false;
         }
        AuthSession session = null;
        PinData pinData = null; // DK IR T36000024110 Rel8.4 01/15/2014
        String userOid = user.getAttribute("user_oid");
        // get auth status
        if (!TradePortalConstants.INDICATOR_YES.equalsIgnoreCase(testValidation2FA)) {
        	//get session from cache if not then create new one        	 
             CachedRSASession cachedSession = cachedRSASessions.get(userOid);
             if (cachedSession == null) {
            	 LOG.info("[LogonMediatorBean.RSA validatePassword2FAUsingRSA] Creating new session");
            	 AuthSessionFactory api = AuthSessionFactory.getInstance(path);
                 session = api.createUserSession();
             }
             else{
            	 LOG.info("[LogonMediatorBean.RSA validatePassword2FAUsingRSA] Get cached session");
            	 session = cachedSession.session;
             }
        	if(!isNextCode){
                authStatus = session.lock(tokenId2FA);
                authStatus = session.check(tokenId2FA, password2fa);
        	}
        	else{
                /*We should have an existing RSA AuthSession in the cache.
                 */
                authStatus = session.next(password2fa);

        	}
        }
        // for dev testing!!!!!
        // Check authStatus and direct action
        else {
            if ( "CGIPIN123456".equals(password2fa) ) {
                authStatus = AuthSession.ACCESS_OK;
            } else if ( "CGIPIN123459".equals(password2fa)) {
                authStatus = AuthSession.NEXT_CODE_REQUIRED;
            }  else if ( "CGIPIN123458".equals(password2fa) || "CGIPIN123457".equals(password2fa)) {
                authStatus = AuthSession.NEW_PIN_REQUIRED;
            }
            else {
                authStatus = AuthSession.ACCESS_DENIED;
            }
        }
        LOG.info("[LogonMediatorBean.RSA validatePassword2FAUsingRSA] loginId="+user.getAttribute("login_id") + ";token_id_2fa=" + tokenId2FA+";authStatus="+authStatus);
        // depending on auth status take action

        if (authStatus==AuthSession.NEXT_CODE_REQUIRED) {
                LOG.info("[LogonMediatorBean.RSA validatePassword2FAUsingRSA] loginId="+user.getAttribute("login_id") + ";token_id_2fa=" + tokenId2FA+";authStatus"+authStatus);
            // Issue a warning for the user to enter a 2FA password again after the token updates.
            mediatorServices.getErrorManager().issueError(getClass().getName(), TradePortalConstants.CHALLENGE_PASSWORD_2FA);
            /*instead of EnterPassword2FA set EnterPassword2FANextCode as LogonStatus
             * Instead of closing the session cache it.
             */  
            //outputDoc.setAttribute("/LogonStatus", "EnterPassword2FA");
            //if (session != null) session.close();
            LOG.info("[LogonMediatorBean.RSA validatePassword2FAUsingRSA]NEXT_CODE_REQUIRED so caching the session] loginId="+user.getAttribute("login_id") + ";token_id_2fa=" + tokenId2FA);
            outputDoc.setAttribute("/LogonStatus", "EnterPassword2FANextCode");
            if (session != null && (null == cachedRSASessions.get(user.getAttribute("user_oid")))) {
            	cacheRSASession(user.getAttribute("user_oid"), session);    
            }
            return false;
        }
        else if (authStatus == AuthSession.NEW_PIN_REQUIRED) {                
                outputDoc.setAttribute("/LogonStatus", "RSANewPin");
                int failedAttempt =(StringFunction.isBlank(inputDoc.getAttribute("/FailedNewPinAttempt")))? 0 : Integer.parseInt(inputDoc.getAttribute("/FailedNewPinAttempt")); // W Zhu 6 Nov 2014 Rel 9.1 T36000033965
                outputDoc.setAttribute("/FailedNewPinAttempt", String.valueOf(failedAttempt));				 
                int pinState=-1; // will decide how new pin will be generated (by user or system generated)
                int maxPinlen;
                int minPinlen;
                boolean isPinAlphaNum;
               // DK IR T36000024146 Rel8.4 02/11/2014 starts
                if(TradePortalConstants.INDICATOR_YES.equals(recertificationIndicator)) {
                	 mediatorServices.getErrorManager().issueError(getClass().getName(), TradePortalConstants.NEW_PIN_LOGIN);
                }
               // DK IR T36000024146 Rel8.4 02/11/2014 ends
                // check if testing or real
                if (TradePortalConstants.INDICATOR_YES.equalsIgnoreCase(testValidation2FA)) {
                         // user enetered new pin criteria for testing!!!
                         maxPinlen = 8;
                         minPinlen = 6;
                         isPinAlphaNum = true;
                        if ( "CGIPIN123458".equals(password2fa)) {
                                pinState = PinData.USER_SELECTABLE;
                        }
                        else if ("CGIPIN123457".equals(password2fa)){
                                pinState = PinData.CANNOT_CHOOSE_PIN;
                        }
                        else{
                                pinState = PinData.MUST_CHOOSE_PIN;
                        }
                }
                else {
                	pinData = session.getPinData(); // DK IR T36000024110 Rel8.4 01/15/2014
                     pinState=pinData.getUserSelectable();
                     // get pin length/type and put in output doc to display in rsanewpin jsp
                     maxPinlen = pinData.getMaxPinLength();
                     minPinlen = pinData.getMinPinLength();
                     isPinAlphaNum = pinData.isAlphanumeric();
                }
                LOG.info("[LogonMediatorBean.RSA validatePassword2FAUsingRSA] pinState =  "+ pinState);
               switch (pinState) {

                 case PinData.USER_SELECTABLE: case PinData.MUST_CHOOSE_PIN:
                         outputDoc.setAttribute("/NewRSAPin", "");
                         outputDoc.setAttribute("/PinState",String.valueOf(pinState));
                         outputDoc.setAttribute("/MaxPinlen",String.valueOf(maxPinlen));
                         outputDoc.setAttribute("/MinPinlen",String.valueOf(minPinlen));
                         outputDoc.setAttribute("/IsPinAlphaNum",String.valueOf(isPinAlphaNum));
                         break;

                 case PinData.CANNOT_CHOOSE_PIN:
                         String newPin;
                        if (TradePortalConstants.INDICATOR_YES.equalsIgnoreCase(testValidation2FA)) {
                                newPin = "CGIPIN";
                        }
                        else {
                           newPin = pinData.getSystemPin().trim();
                        }
                        LOG.info("[LogonMediatorBean.RSA validatePassword2FAUsingRSA] new System pin =  "+ newPin);
                         outputDoc.setAttribute("/NewRSAPin", newPin);
                         outputDoc.setAttribute("/PinState",String.valueOf(pinState));
                         break;
                 }

                if (!TradePortalConstants.INDICATOR_YES.equalsIgnoreCase(testValidation2FA)) {
                   // Save RSA AuthSession to be used for new PIN validation during the next invocation of LogonMediator.
                   cacheRSASession(user.getAttribute("user_oid"), session);
                }
                LOG.info("[LogonMediatorBean.RSA validatePassword2FAUsingRSA] NEW_PIN_REQUIRED so caching the session loginId="+user.getAttribute("login_id") + ";token_id_2fa=" + tokenId2FA);
                return false;
        }
        else if (authStatus==AuthSession.ACCESS_OK ){
            // Return true to let the user log in.
            LOG.info("[LogonMediatorBean.RSA validatePassword2FAUsingRSA] Successfull so closing the session loginId="+user.getAttribute("login_id") + ";token_id_2fa=" + tokenId2FA); 
            if (session != null) {
                    //session.close();
                    closeRSASession(userOid,session);
            }
            return true;
        }
        else  {  // Should be either ACCESS_DENIED or NEXT_CODE_BAD
             // Issue an error indicating that the password entered by the user is invalid
             mediatorServices.getErrorManager().issueError(getClass().getName(), TradePortalConstants.INVALID_PASSWORD_2FA);
             // DK IR T36000027938 Rel8.4 BTM 05/15/2014 starts - commented below code as user should not be locked outafter 3 attempts for RSA validation
             // Increment the invalid login attempt count for the user
          
             // DK IR T36000027938 Rel8.4 BTM 05/15/2014 ends
             outputDoc.setAttribute("/LogonStatus", "EnterPassword2FA");
             LOG.info("[LogonMediatorBean.RSA validatePassword2FAUsingRSA]ACCESS_DENIED or NEXT_CODE_BAD Cache the session. loginId="+user.getAttribute("login_id") + ";token_id_2fa=" + tokenId2FA);
             if (session != null && (null == cachedRSASessions.get(user.getAttribute("user_oid")))) {
            	 LOG.info("[LogonMediatorBean.ACCESS_DENIED caching the session if it doesnt already exist for user] loginId="+user.getAttribute("login_id") + ";user_oid=" + user.getAttribute("user_oid"));
            	 cacheRSASession(user.getAttribute("user_oid"), session);
             }
              return false;
         }        
    }
    
	/**
     * close session as well as remove from cache
     * Note this method is synchronized to be thread-safe.
     * 
     * @param userOid
     * @param session 
     */
    synchronized void closeRSASession(String userOid, AuthSession session) {
    	try{
            if (session!=null) {
    		session.close();
            }
            // Optionally, check to make sure we don�t try to remove something that doesn�t exist
            if(cachedRSASessions.get(userOid) != null)
            {
                 cachedRSASessions.remove(userOid);
            }
    	}
    	catch (AuthAgentException e){
    		LOG.info("[LogonMediatorBean.RSA closeRSASession]: Exception removing while cached RSA session for user OID =  "+ userOid + ".  "  + e.getMessage() + ". Ignored");
    	}		
	}


	/**
     * Cache RSA AuthSession when it needs to be used again for new PIN validation.
     * Note this method is synchronized to be thread-safe.
     * 
     * @param userOid
     * @param session 
     */
    synchronized void  cacheRSASession(String userOid, AuthSession session) {
        // First clean up any old items in the cache.  The user may have abandoned a session and therefore left items in the cache.
        // A session is removed from the cache after 30 minutes;
        // Also remove any redundant item for the user.  We assume only one login session for the same user at the same time.
        Date currentTime = new Date();
        long currentTimeInMs = currentTime.getTime();
        Iterator<Entry<String, CachedRSASession>> it = cachedRSASessions.entrySet().iterator();
        while (it.hasNext()) {
            CachedRSASession cachedSession = it.next().getValue();
            if (cachedSession.timeOfCaching.getTime() - currentTimeInMs < - (30*60*1000)
                    || (userOid.equals(cachedSession.userOid) && session != cachedSession.session)) { // DK IR T36000027938 Rel8.4 BTM 06/06/2014
                try {
                    LOG.info("[LogonMediatorBean.RSA cacheRSASession]: close stale cached RSA session for user OID =  "+ userOid);
                    AuthSession staleSession = cachedSession.session;
                    staleSession.close();                    
                }
                catch (AuthAgentException e) {
                    LOG.info("[LogonMediatorBean.RSA cacheRSASession]: Exception removing stale cached RSA session for user OID =  "+ userOid + ".  "  + e.getMessage() + ". Ignored");
                }
                LOG.info("[LogonMediatorBean.RSA cacheRSASession]: remove stale cached RSA session for user OID =  "+ userOid);
                it.remove();
            }
        } 
        
        // cache the new session
        CachedRSASession newCachedRSASession = new CachedRSASession(userOid, currentTime, session);
        cachedRSASessions.put(userOid, newCachedRSASession);
        LOG.info("[LogonMediatorBean.RSA cacheRSASession]: add RSA session to cache for user OID =  "+ userOid);
    }

    /**

     * @param user
     * @param inputDoc
     * @param outputDoc
     * @param mediatorServices
     * @return
     * @throws RemoteException
     * @throws AmsException
     */
    private boolean validateNewPinUsingRSA( User user, DocumentHandler inputDoc, DocumentHandler outputDoc,
    		MediatorServices mediatorServices)  throws RemoteException,AmsException, AuthAgentException{

        String ignoreValidation2FA = ConfigSettingManager.getConfigSetting("PORTAL_APP", "PORTAL", "2FAignoreValidation", "N");
		 String pin2fa = inputDoc.getAttribute("/NewRSAPin");
		 String confirmPin2fa = inputDoc.getAttribute("/ConfirmNewRSAPin");

        if ("Y".equals(ignoreValidation2FA)) {
            LOG.info("[LogonMediatorBean.RSA validateNewPinUsingRSA] loginId=" + user.getAttribute("login_id")    + ";2FAignoreValidation=Y");
            return true;
        }
	// DK IR T36000025225 Rel8.4 02/18/2014 starts
	 if(((Integer.parseInt(inputDoc.getAttribute("/PinState"))!=PinData.CANNOT_CHOOSE_PIN)) && (!pin2fa.equals(confirmPin2fa))){
            int pinState =(StringFunction.isBlank(inputDoc.getAttribute("/PinState"))) ?-1:Integer.parseInt(inputDoc.getAttribute("/PinState"));
            int failedAttempt =(StringFunction.isBlank(inputDoc.getAttribute("/FailedNewPinAttempt")))?0: Integer.parseInt(inputDoc.getAttribute("/FailedNewPinAttempt")); // DK IR T36000024175 Rel8.4 01/16/2014
            outputDoc.setAttribute("/LogonStatus", "RSANewPin");
            outputDoc.setAttribute("/FailedNewPinAttempt", String.valueOf(failedAttempt));
            outputDoc.setAttribute("/NewRSAPin", "");
            outputDoc.setAttribute("/PinState",String.valueOf(pinState));
            outputDoc.setAttribute("/MaxPinlen",inputDoc.getAttribute("/MaxPinlen"));
            outputDoc.setAttribute("/MinPinlen",inputDoc.getAttribute("/MinPinlen"));
            outputDoc.setAttribute("/IsPinAlphaNum",inputDoc.getAttribute("/IsPinAlphaNum"));
            mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
            TradePortalConstants.ENTRIES_DONT_MATCH,
            mediatorServices.getResourceManager().getText("Login.NewRsaPin", TradePortalConstants.TEXT_BUNDLE),
            mediatorServices.getResourceManager().getText("Login.ConfirmNewRsaPin", TradePortalConstants.TEXT_BUNDLE));
             return false;
	 }
		 // DK IR T36000025225 Rel8.4 02/18/2014 ends

        //add ability to test call to 2fa server
        String testValidation2FA = ConfigSettingManager.getConfigSetting("PORTAL_APP", "PORTAL", "2FAtestValidation", "N");

	    	int authStatus=AuthSession.ACCESS_DENIED;
	    	// DK IR T36000024144 Rel8.4 01/15/2014 starts
	    	
	   	 String path = null;
		 	try{
				path =  portalProperties.getString("RSA_ConfigFilePath");
		 	}catch (Exception e){
		 		//this should never happen but in case the path is not set log the error.
		 		e.printStackTrace();
		 	}
	        if (StringFunction.isBlank(path)){
	        	path = ConfigSettingManager.getConfigSetting("PORTAL_APP", "PORTAL", "RSA_ConfigFilePath", null);
	        }
		 	
	         if (path == null || "".equals(path)) {
		     mediatorServices.getErrorManager().issueError(getClass().getName(), TradePortalConstants.ERROR_VALIDATE_2FA, "CONFIG_SETTING RSA_ConfigFilePath not configured.");
	             return false;
	         }
		    // DK IR T36000024144 Rel8.4 01/15/2014 ends
	    	AuthSession session = null;

                // get auth status
                if (!TradePortalConstants.INDICATOR_YES.equals(testValidation2FA)) {
                        // We should have an existing RSA AuthSession in the cache.
                        String userOid = user.getAttribute("user_oid");
                        CachedRSASession cachedSession = cachedRSASessions.get(userOid);
                        if (cachedSession == null) {
                                 mediatorServices.getErrorManager().issueError(getClass().getName(), AmsConstants.REQUIRED_ATTRIBUTE, "A Valid RSA session");
                                 return false;                                    
                        }
                        session = cachedSession.session;
                        /** rpasupulati 7-July 2014 calling isPinValid method before sending pin to RSA server
                         * IR T36000028717
                         */
                        if (isPinValid(pin2fa,inputDoc) ) {
                                authStatus = session.pin(pin2fa);
                                LOG.info("[LogonMediatorBean.RSA validateNewPinUsingRSA] pin() called. loginId="+user.getAttribute("login_id") + ";token_id_2fa=" + user.getAttribute("token_id_2fa") + ";authStatus="+authStatus);
                        
                                // validate user entered pin
                       	}else {
                                authStatus = AuthSession.PIN_REJECTED;
                                LOG.info("[LogonMediatorBean.RSA validateNewPinUsingRSA isPinValid() failed] loginId="+user.getAttribute("login_id") + ";token_id_2fa=" + user.getAttribute("token_id_2fa") + ";authStatus="+authStatus);
                        }
                             
	 
                }
                // for dev testing!!!!!
                // Check authStatus and direct action
                else {
                        if((Integer.parseInt(inputDoc.getAttribute("/PinState"))==PinData.CANNOT_CHOOSE_PIN)){
                            if ( "CGIPIN".equals(pin2fa) ) {
                                authStatus = AuthSession.PIN_ACCEPTED;
                            }
                            else {
                                authStatus = AuthSession.PIN_REJECTED;
                            }
                        }
                        // validate user entered pin
                        else {
                                if (isPinValid(pin2fa,inputDoc)  && "CGIPIN".equals(pin2fa)) { // DK IR T36000024176 Rel8.4 01/16/2014
                                authStatus = AuthSession.PIN_ACCEPTED;
                            }
                            else {
                                authStatus = AuthSession.PIN_REJECTED;
                            }
                        }
                }


	        if (authStatus==AuthSession.PIN_ACCEPTED) {
                    LOG.info("[LogonMediatorBean.RSA PIN_ACCEPTED Keep the cached session] loginId="+user.getAttribute("login_id") + ";token_id_2fa=" + user.getAttribute("token_id_2fa") + ";authStatus"+authStatus);
	            outputDoc.setAttribute("/LogonStatus", "EnterPassword2FA");
	            return true;
	        }
                else {//	        if (authStatus==AuthSession.PIN_REJECTED) {
	             // Increment the invalid new pin attempt count for the user
	            int failedAttempt =(StringFunction.isBlank(inputDoc.getAttribute("/FailedNewPinAttempt")))?0: Integer.parseInt(inputDoc.getAttribute("/FailedNewPinAttempt")); // DK IR T36000024175 Rel8.4 01/16/2014
	            int pinState =(StringFunction.isBlank(inputDoc.getAttribute("/PinState"))) ?-1:Integer.parseInt(inputDoc.getAttribute("/PinState"));
	            
	            if (failedAttempt < 2 ) { // DK IR T36000024175 Rel8.4 01/16/2014
                        // W Zhu 6 Nov 2014 Rel 9.1 T36000033965 route to Enter 2FA Password and close session after Pin rejection.
                        LOG.info("[LogonMediatorBean.RSA not PIN_ACCEPTED < 2 keep the cached session] loginId="+user.getAttribute("login_id")+ ";token_id_2fa=" + user.getAttribute("token_id_2fa")  );
                        mediatorServices.getErrorManager().issueError(getClass().getName(), TradePortalConstants.INVALID_PIN);
                        outputDoc.setAttribute("/FailedNewPinAttempt", String.valueOf(failedAttempt + 1));		        
                        outputDoc.setAttribute("/LogonStatus", "EnterPassword2FA");
	            }
	            else {
                        LOG.info("[LogonMediatorBean.RSA not PIN_ACCEPTED >= 2 close the session] loginId="+user.getAttribute("login_id")+ ";token_id_2fa=" + user.getAttribute("token_id_2fa") );
	            	outputDoc.setAttribute("/LogonStatus", "PasswordLogon");
	            	switch (pinState) {
			    	 case PinData.USER_SELECTABLE: case PinData.MUST_CHOOSE_PIN:
			    		 mediatorServices.getErrorManager().issueError(getClass().getName(), TradePortalConstants.NEW_PIN_FAIL);
			    		 break;
			    	 case PinData.CANNOT_CHOOSE_PIN:
			    		 mediatorServices.getErrorManager().issueError(getClass().getName(), TradePortalConstants.NEW_PIN_REJECT);
			    		 break;
	            	}
                        if (session != null) {
                            closeRSASession(user.getAttribute("user_oid"), session);
                        }
	            }
	              return false;
	         }

    }
    
    /**

     * @param newPin the submitted new Pin to validate
     * @param inputDoc
     * @return <code>true</code> if valid PIN; <code>false</code> otherwise
     * @throws AuthAgentException
     */
    private boolean isPinValid(String newPin, DocumentHandler inputDoc)
    {
        boolean isValid;
        int len = newPin.length();
        int maxPinlen = Integer.parseInt(inputDoc.getAttribute("/MaxPinlen")) ;
		int minPinlen = Integer.parseInt(inputDoc.getAttribute("/MinPinlen"));
		boolean isPinAlphaNum = Boolean.parseBoolean(inputDoc.getAttribute("/IsPinAlphaNum"));
		
       if (len < minPinlen || len > maxPinlen)
        {
            return false;
        }
        isValid = true;
        for (int i = 0; i < newPin.length(); i++)
        {
           if (isPinAlphaNum)
            {
                if (!Character.isLetterOrDigit(newPin.charAt(i)))
                {
                    isValid = false;
                    break;
                }
           }
            else
            {
                if (!Character.isDigit(newPin.charAt(i)))
                {
                    isValid = false;
                    break;
                }
            }
        }
        return isValid;
    }
}