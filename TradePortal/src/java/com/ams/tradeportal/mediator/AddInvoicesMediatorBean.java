package com.ams.tradeportal.mediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class is used for adding one or more Invoices to an Invoice Only ATP
 * Issue or Amend transaction. If Invoices  are being assigned to a
 * transaction that previously had no Invoices , the class will ensure
 * that all of the selected Invoices  have the same Inv upload definition
 * oid, seller name, and currency. If all selected Invoices  are
 * valid, the transaction's amount, invoice due date, currency are updated accordingly.
 *
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */

import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import com.ams.tradeportal.busobj.ArMatchingRule;
import com.ams.tradeportal.busobj.Instrument;
import com.ams.tradeportal.busobj.InvoicesSummaryData;
import com.ams.tradeportal.busobj.Terms;
import com.ams.tradeportal.busobj.TermsParty;
import com.ams.tradeportal.busobj.Transaction;
import com.ams.tradeportal.busobj.util.InvoiceUtility;
import com.ams.tradeportal.busobj.util.POLineItemUtility;
import com.ams.tradeportal.common.Debug;
import com.ams.tradeportal.common.TPDateTimeUtility;
import com.ams.tradeportal.common.TradePortalConstants;
import com.ams.tradeportal.common.TransactionType;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.frame.MediatorBean;
import com.amsinc.ecsg.frame.MediatorServices;
import com.amsinc.ecsg.util.DateTimeUtility;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.StringFunction;

public class AddInvoicesMediatorBean extends MediatorBean {
private static final Logger LOG = LoggerFactory.getLogger(AddInvoicesMediatorBean.class);
	// The following variables have been declared transient because the mediator
	// is stateless and they are initialized in only one place.
	private transient MediatorServices mediatorServices = null;
	private transient Transaction transaction = null;
	private transient Instrument instrument = null;
	private transient boolean hasInvoice = false;
	private transient String uploadDefinitionOid = null;
	private transient String beneficiaryName = null;
	private transient String currency = null;
	private transient String dueDate = null;
	private transient String formattedDueDate =null;
	private transient String payDate = null;
	private transient Vector invoiceList = null;
	private transient String invIdsSql = null;
	private transient Terms terms = null;
	private transient int numOfInvs = 0;
	private transient long termsOID = 0;

	private transient String userLocale = null;
	private transient String userOID = null;
	private transient boolean allInvoicesRemoved = false;

	/**
	 * This method performs addition of Invoices The format expected from the
	 * input doc is:
	 *
	 * <POLineItemList> <POLineItem ID="0"><poLineItemOid>1000001</POLineItem>
	 * <POLineItem ID="1"><poLineItemOid>1000002</POLineItem> <POLineItem
	 * ID="2"><poLineItemOid>1000003</POLineItem> <POLineItem
	 * ID="3"><poLineItemOid>1000004</POLineItem> <POLineItem
	 * ID="6"><poLineItemOid>1000001</POLineItem> </POLineItemList>
	 *
	 * This is a non-transactional mediator that executes transactional methods
	 * in business objects
	 *
	 * @param inputDoc
	 *            The input XML document (&lt;In&gt; section of overall Mediator
	 *            XML document)
	 * @param outputDoc
	 *            The output XML document (&lt;Out&gt; section of overall
	 *            Mediator XML document)
	 * @param newMediatorServices
	 *            Associated class that contains lots of the auxillary
	 *            functionality for a mediator.
	 */
	public DocumentHandler execute(DocumentHandler inputDoc,
			DocumentHandler outputDoc, MediatorServices newMediatorServices)
			throws RemoteException, AmsException {
		String substitutionParm = null;
		Vector invOidsList = null;
		long transactionOid = 0;

		mediatorServices = newMediatorServices;
		// This gives selected Invoice Oid List
		invoiceList = inputDoc.getFragments("/POLineItemList/POLineItem");
		transactionOid = inputDoc
				.getAttributeLong("/Transaction/transactionOid");
		hasInvoice = Boolean.valueOf(inputDoc.getAttribute("../hasInvoices"))
				.booleanValue();
		numOfInvs = invoiceList.size();
		userLocale = mediatorServices.getCSDB().getLocaleName();
		userOID = inputDoc.getAttribute("/User/userOid");

		// if the user locale not specified default to en_US
		if ((userLocale == null) || (userLocale.equals(""))) {
			userLocale = "en_US";
		}
		String buttonPressed = "";
		buttonPressed = inputDoc.getAttribute("/Update/ButtonPressed");

		if (!(TradePortalConstants.BUTTON_ADD_PO_LINE_ITEMS
				.equals(buttonPressed) || TradePortalConstants.REMOVE_PO
				.equals(buttonPressed))) {
			return outputDoc;
		}
		// If nothing was selected by the user, issue an error indicating this
		// and return
		if (numOfInvs == 0) {
			// if its addInvoices button
			if (TradePortalConstants.BUTTON_ADD_PO_LINE_ITEMS
					.equals(buttonPressed)) {
				substitutionParm = mediatorServices.getResourceManager()
						.getText("InvoiceAction.Add",
								TradePortalConstants.TEXT_BUNDLE);
			}
			// if it is removeInvoice button
			else {
				substitutionParm = mediatorServices.getResourceManager()
						.getText("InvoiceAction.Remove",
								TradePortalConstants.TEXT_BUNDLE);
			}

			mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.NO_ITEM_SELECTED, substitutionParm);
			addINV(transactionOid, outputDoc);
			return outputDoc;
		}

		// Get a list of all the Invoice oids that were selected
		String multiplestrucInvoices = TradePortalConstants.INDICATOR_NO;
		invOidsList = POLineItemUtility.getPOLineItemOids(invoiceList,
				multiplestrucInvoices, "");

		// Build the SQL to use in numerous queries throughout the mediator
		String requiredOid = "upload_invoice_oid";
		invIdsSql = POLineItemUtility.getPOLineItemOidsSql(invOidsList,
				requiredOid);

		// If Invoices don't currently exist for the transaction, make sure that all of
		// the Invoices selected by the user have the same Inv upload definition,
		// Seller and currency; if they don't, issue an error indicating this.
		//On the other hand, if the transaction *does* currently have Invoices
		// assigned to it, we don't need to validate anything since it has already been done in
		// the listview (i.e., the listview ensures that these fields match the corresponding
		// fields for the transaction).
		if (TradePortalConstants.BUTTON_ADD_PO_LINE_ITEMS.equals(buttonPressed)) {
			if (!hasInvoice) {
				if (validateInvoices(invOidsList) == false) {
					mediatorServices.getErrorManager().issueError(
							TradePortalConstants.ERR_CAT_1,
							TradePortalConstants.ADD_INVOICE_MISMATCH);										
					
					return outputDoc;
				} else {
					hasInvoice = true;
				}
			}
		}

		try {
			transaction = (Transaction) mediatorServices.createServerEJB(
					"Transaction", transactionOid);

			// Needs to instantiate instrument object for updating amendment transaction
			long instrumentOid = transaction
						.getAttributeLong("instrument_oid");
				instrument = (Instrument) mediatorServices.createServerEJB(
						"Instrument", instrumentOid);
			terms = (Terms) transaction
					.getComponentHandle("CustomerEnteredTerms");
			termsOID = terms.getAttributeLong("terms_oid");
			if (TradePortalConstants.BUTTON_ADD_PO_LINE_ITEMS
					.equals(buttonPressed)) {
				addInvoices(invOidsList, outputDoc);
			} else {
				removeInvoices(invOidsList, outputDoc);
			}

		} catch (Exception e) {
			LOG.error("Exception occured in AddInvoicesMediator: ", e);
			//e.printStackTrace();
		}
		//SHR IR T36000006752
		if (StringFunction.isNotBlank(terms.getAttribute("c_FirstTermsParty")))
		{
		TermsParty firstTermsParty = (TermsParty) terms
		.getComponentHandle("FirstTermsParty");
		beneficiaryName = firstTermsParty.getAttribute("name");
		}
		//SHR IR T36000006752

		// If the transaction previously did not have Invoices assigned to it,
		// set the Inv upload definition oid, Seller and currency in the xml
		// doc so that the jsp loads up the correct data in the listview (otherwise, the
		// user could potentially add Inv that don't have matching Inv upload definitions,
		// seller and currencies).
			outputDoc.setAttribute("/ChangePOs/name", beneficiaryName);//SHR IR 6752
			outputDoc.setAttribute("/Transaction/uploadDefinitionOid",
					uploadDefinitionOid);
			outputDoc.setAttribute("../beneficiaryName", beneficiaryName);
			outputDoc.setAttribute("../currency", currency);
			outputDoc.setAttribute("../validDate", formattedDueDate);//SHR IR T36000005856
			if (allInvoicesRemoved){
				outputDoc.setAttribute("/ChangePOs/name", "");
			}
		transaction.save(false);
		instrument.save(false);
		return outputDoc;
	}

	/**
	 * This method determines whether or not the inv upload definition oid,
	 * seller name, and currency are the same for all Invoices selected by the
	 * user.
	 *
	 * @exception com.amsinc.ecsg.frame.AmsException
	 * @exception java.rmi.RemoteException
	 * @return boolean - indicates whether or not validation was successful
	 *         (true - the inv upload definition oid, beneficiary name, and
	 *         currency are the same for all selected Invoices false - the inv
	 *         upload definition oid, seller name, and currency are not the same
	 *         for all selected Invoices )
	 */
	private boolean validateInvoices(Vector invOidsList) throws AmsException,
			RemoteException {
		StringBuilder whereClause = null;
		InvoicesSummaryData inv = null;
		long invOid = 0;
		int validationTotal = 0;
		List<Object> sqlParams = new ArrayList<Object>();

		invOid = Long.parseLong((String) invOidsList.elementAt(0));
		inv = (InvoicesSummaryData) mediatorServices.createServerEJB(
				"InvoicesSummaryData", invOid);
		// get invoice def oid

		uploadDefinitionOid = inv.getAttribute("upload_definition_oid");

		beneficiaryName = inv.getAttribute("seller_name");
		currency = inv.getAttribute("currency");
		dueDate = StringFunction.isNotBlank(inv
				.getAttribute("payment_date")) ? inv
				.getAttribute("payment_date") : inv.getAttribute("due_date");
		formattedDueDate = TPDateTimeUtility.convertToDBDate(dueDate,
				TradePortalConstants.PO_FORMATTED_US_DATE);
		whereClause = new StringBuilder();
		whereClause.append(invIdsSql);
		if (StringFunction.isBlank(uploadDefinitionOid)) {
			whereClause.append(" and a_upload_definition_oid is null");
		} else {
			whereClause.append(" and a_upload_definition_oid = ? ");
			sqlParams.add(uploadDefinitionOid);
		}

		whereClause.append(" and (due_date = NVL(?,'0') ");
		whereClause.append("or payment_date = NVL(?,'0'))");
		sqlParams.add(formattedDueDate);
		sqlParams.add(formattedDueDate);

		if (StringFunction.isBlank(beneficiaryName)) {
			whereClause.append(" and seller_name is null ");
		} else {
			whereClause.append(" and seller_name = ? ");
			sqlParams.add(beneficiaryName);
		}
		whereClause.append("and currency = ? ");
		sqlParams.add(currency);

		validationTotal = DatabaseQueryBean.getCount("upload_invoice_oid",
				"invoices_summary_data", whereClause.toString(), false, sqlParams);

		if (validationTotal == numOfInvs) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * This method keeps the previously added Invoices by the user to the
	 * transaction he/she is currently viewing when error comes up if user has
	 * not selected any Inv and still hit the add selected items button.
	 *
	 * @param long transactionOid
	 * @param DocumentHandler
	 *            - the mediator's output doc
	 * @exception com.amsinc.ecsg.frame.AmsException
	 * @exception java.rmi.RemoteException
	 * @return void
	 */
	private void addINV(long transactionOid, DocumentHandler outputDoc)
			throws AmsException, RemoteException {
		transaction = (Transaction) mediatorServices.createServerEJB(
				"Transaction", transactionOid);

		// Get the terms associated with the current transaction and its
		// shipment terms
		terms = (Terms) transaction.getComponentHandle("CustomerEnteredTerms");
		// Get data from Terms and set it in the Out section of the XML
		// That way. when the user is returned to the page of transaction, the
		// updated data will
		// appear along with other data that they entered
		outputDoc.setAttribute("/ChangePOs/amount",
				terms.getAttribute("amount"));
		outputDoc.setAttribute("/ChangePOs/amount_currency_code",
				terms.getAttribute("amount_currency_code"));
		outputDoc.setAttribute("/ChangePOs/invoice_due_date",
				terms.getAttribute("invoice_due_date"));

	}

	/**
	 * This method adds the Invoices selected by the user to the transaction
	 * that he/she is currently viewing. After all Inv have been saved, the
	 * amount, date fields are all re-derived to reflect the new item values.
	 *
	 * @param java
	 *            .util.Vector invOidsList - the list of Inv oids
	 * @param DocumentHandler
	 *            - the mediator's output doc
	 * @exception com.amsinc.ecsg.frame.AmsException
	 * @exception java.rmi.RemoteException
	 * @return void
	 */
	private void addInvoices(Vector invOidsList, DocumentHandler outputDoc)
			throws AmsException, RemoteException {
		InvoicesSummaryData inv = null;
		String transactionOid = null;
		String instrumentOid = null;
		long invOid = 0;
		Vector invList = new Vector();
		Vector invIDList = new Vector();

		transactionOid = transaction.getAttribute("transaction_oid");
		instrumentOid = transaction.getAttribute("instrument_oid");
		ArMatchingRule rule = null;
		String tpName="";
		try {
			
		
		for (int i = 0; i < numOfInvs; i++) {
			invOid = Long.parseLong((String) invOidsList.elementAt(i));
			inv = (InvoicesSummaryData) mediatorServices.createServerEJB(
			"InvoicesSummaryData", invOid);
			 
				 rule = inv.getMatchingRule();
				tpName = inv.getTpRuleName(rule);
			inv.setAttribute("tp_rule_name",tpName);
			inv.setAttribute("invoice_status", TradePortalConstants.PO_STATUS_ASSIGNED);
			inv.setAttribute("instrument_oid",instrumentOid);
			inv.setAttribute("transaction_oid",transactionOid);		
			inv.setAttribute("terms_oid",String.valueOf(termsOID));
			inv.setAttribute("action",  TradePortalConstants.PAYABLE_UPLOAD_INV_ACTION_LINK_INST);
			inv.setAttribute("user_oid",userOID);		
			 inv.save();
			
		}
	} catch (AmsException e) {
		LOG.error("Error while saving InvoicesSummaryData Object ", e);
		//e.printStackTrace();
	} 

		// Add the Invoices already associated to the Terms to invList
		// These are used in the derive process later

		  StringBuilder sb = new StringBuilder();

		  sb.append(" select invoice_id,upload_invoice_oid from invoices_summary_data where a_transaction_oid =? ");

	      DocumentHandler resultSet1 = DatabaseQueryBean.getXmlResultSet(sb.toString(), false, new Object[]{transaction.getAttribute("transaction_oid")});

		  List<DocumentHandler> rows = resultSet1.getFragmentsList("/ResultSetRecord");
			 for (DocumentHandler row:rows) {
				 String poNum = row.getAttribute("/INVOICE_ID");
				 invIDList.addElement(poNum);
				 String invOID = row.getAttribute("/UPLOAD_INVOICE_OID");
				invList.addElement(invOID);
			 }

			

			 beneficiaryName = tpName; 
		performCalculation(invList,invIDList, outputDoc);
		if(rule!=null){
		outputDoc.setAttribute("/ChangePOs/name", beneficiaryName );
		outputDoc.setAttribute("/ChangePOs/address_line_1", rule.getAttribute("address_line_1"));
		outputDoc.setAttribute("/ChangePOs/address_line_2", rule.getAttribute("address_line_2"));
		outputDoc.setAttribute("/ChangePOs/address_city", rule.getAttribute("address_city"));
		outputDoc.setAttribute("/ChangePOs/address_state_province", rule.getAttribute("address_state"));
		outputDoc.setAttribute("/ChangePOs/address_country", rule.getAttribute("address_country"));
		outputDoc.setAttribute("/ChangePOs/address_postal_code", rule.getAttribute("postalcode"));
		outputDoc.setAttribute("/ChangePOs/BenOTLCustomerId",rule.getAttribute("tps_customer_id"));
		}
		outputDoc.setAttribute("/ChangePOs/invoice_only_ind",
				TradePortalConstants.INDICATOR_YES);
		outputDoc.setAttribute("/ChangePOs/isStructuredPO",TradePortalConstants.INDICATOR_YES);

		mediatorServices.getErrorManager().issueError(
				TradePortalConstants.ERR_CAT_1,
				TradePortalConstants.ADD_INVOICE_SUCCESS,
				String.valueOf(numOfInvs));

		terms.setAttribute("invoice_only_ind",
				TradePortalConstants.INDICATOR_YES);
		TermsParty firstTermsParty = null;
		try{
			firstTermsParty = (TermsParty) terms.getComponentHandle("FirstTermsParty");
		}catch(Exception e){
			long firstTermsPartyOid = terms.newComponent("FirstTermsParty");
			firstTermsParty = (TermsParty) terms.getComponentHandle("FirstTermsParty");
		}
		firstTermsParty.setAttribute("name", beneficiaryName);
		if(rule!=null){
		firstTermsParty.setAttribute("address_line_1",rule.getAttribute("address_line_1"));
		firstTermsParty.setAttribute("address_line_2",rule.getAttribute("address_line_2"));
		firstTermsParty.setAttribute("address_city",rule.getAttribute("address_city"));
		firstTermsParty.setAttribute("address_state_province",rule.getAttribute("address_state"));
		firstTermsParty.setAttribute("address_country",rule.getAttribute("address_country"));
		firstTermsParty.setAttribute("address_postal_code",rule.getAttribute("postalcode"));
		firstTermsParty.setAttribute("OTL_customer_id",rule.getAttribute("tps_customer_id"));
		}
		allInvoicesRemoved = false;

	}

	private void removeInvoices(Vector invOidsList, DocumentHandler outputDoc)
			throws AmsException, RemoteException {
		InvoicesSummaryData inv = null;
		long invOid = 0;
		Vector associatedInvList = new Vector();
		Vector associatedInvIDList = new Vector();
		// Get a list of the Invoices already associated with this instrument
		String alreadyAssociatedSql = "select upload_invoice_oid,invoice_id from invoices_summary_data where a_terms_oid = ? ";
		DocumentHandler resultSet = DatabaseQueryBean
				.getXmlResultSet(alreadyAssociatedSql, false, new Object[]{termsOID});

		if (resultSet == null)
			resultSet = new DocumentHandler();

		List<DocumentHandler> alreadyAssociatedList = resultSet
				.getFragmentsList("/ResultSetRecord");
		for (DocumentHandler list: alreadyAssociatedList) {
			String invoiceOid = list.getAttribute("/UPLOAD_INVOICE_OID");
			associatedInvList.addElement(invoiceOid);
			associatedInvIDList.addElement(list.getAttribute("/INVOICE_ID"));
		}

		// Update the assigned to transaction indicators for each Inv
		// being added to the transaction

		InvoicesSummaryData firstInv = (InvoicesSummaryData) mediatorServices.createServerEJB(
		"InvoicesSummaryData", Long.parseLong((String) invOidsList.elementAt(0)));
		ArMatchingRule rule = null;
		String tpName="";
		try {
			for (int i = 0; i < numOfInvs; i++) {
				invOid = Long.parseLong((String) invOidsList.elementAt(i));
				inv = (InvoicesSummaryData) mediatorServices.createServerEJB(
				"InvoicesSummaryData", invOid);
					 rule = inv.getMatchingRule();
				tpName = inv.getTpRuleName(rule);
				
				inv.setAttribute("tp_rule_name",tpName);
				inv.setAttribute("invoice_status", TradePortalConstants.PO_STATUS_UNASSIGNED);
				inv.setAttribute("instrument_oid",null);
				inv.setAttribute("transaction_oid",null);
				inv.setAttribute("terms_oid",null);
				inv.setAttribute("action",  TradePortalConstants.PO_ACTION_UNLINK);
				inv.setAttribute("user_oid",userOID);	
				int res = inv.save();
				
				associatedInvList.remove(invOidsList.elementAt(i));
			}
		} catch (AmsException e) {
			LOG.error("Error while saving InvoicesSummaryData Object ", e);
		} 
		LOG.debug("associatedInvList: {} \t alreadyAssociatedList: {} ", associatedInvList, alreadyAssociatedList);
		if(associatedInvList.size()!=alreadyAssociatedList.size()){
			 dueDate =firstInv.getActualDate();
		}
		 beneficiaryName = tpName;

	      DocumentHandler resultSet1 = DatabaseQueryBean.getXmlResultSet(alreadyAssociatedSql, false, new Object[]{termsOID});
	      Vector invIDList = new Vector();
	      Vector invList = new Vector();
	      if(resultSet1!=null){
		  List<DocumentHandler> rows = resultSet1.getFragmentsList("/ResultSetRecord");
		  for (DocumentHandler row: rows) {
				 String poNum = row.getAttribute("/INVOICE_ID");
				 invIDList.addElement(poNum);
				 String invOID = row.getAttribute("/UPLOAD_INVOICE_OID");
				invList.addElement(invOID);
			 }
}
		performCalculation(invList,invIDList, outputDoc);
		if(invList!=null && invList.size()>0){
			allInvoicesRemoved = false;
			
			outputDoc.setAttribute("/ChangePOs/name", beneficiaryName );
			if(rule!=null){
			outputDoc.setAttribute("/ChangePOs/address_line_1", rule.getAttribute("address_line_1"));
			outputDoc.setAttribute("/ChangePOs/address_line_2", rule.getAttribute("address_line_2"));
			outputDoc.setAttribute("/ChangePOs/address_city", rule.getAttribute("address_city"));
			outputDoc.setAttribute("/ChangePOs/address_state_province", rule.getAttribute("address_state"));
			outputDoc.setAttribute("/ChangePOs/address_country", rule.getAttribute("address_country"));
			outputDoc.setAttribute("/ChangePOs/address_postal_code", rule.getAttribute("postalcode"));
			outputDoc.setAttribute("/ChangePOs/BenOTLCustomerId",rule.getAttribute("tps_customer_id"));
			}
			outputDoc.setAttribute("/ChangePOs/invoice_only_ind",
					TradePortalConstants.INDICATOR_YES);
			terms.setAttribute("invoice_only_ind",
					TradePortalConstants.INDICATOR_YES);
			TermsParty firstTermsParty = null;
			try{
				firstTermsParty = (TermsParty) terms.getComponentHandle("FirstTermsParty");
			}catch(Exception e){
				LOG.trace("Exception while trying to get existing FirstTermsParty so, creating new FirstTermsParty component. ");
				long firstTermsPartyOid = terms.newComponent("FirstTermsParty");
				firstTermsParty = (TermsParty) terms.getComponentHandle("FirstTermsParty");
			}
			firstTermsParty.setAttribute("name", beneficiaryName);
			if(rule!=null){
			firstTermsParty.setAttribute("address_line_1",rule.getAttribute("address_line_1"));
			firstTermsParty.setAttribute("address_line_2",rule.getAttribute("address_line_2"));
			firstTermsParty.setAttribute("address_city",rule.getAttribute("address_city"));
			firstTermsParty.setAttribute("address_state_province",rule.getAttribute("address_state"));
			firstTermsParty.setAttribute("address_country",rule.getAttribute("address_country"));
			firstTermsParty.setAttribute("address_postal_code",rule.getAttribute("postalcode"));
			firstTermsParty.setAttribute("OTL_customer_id",rule.getAttribute("tps_customer_id"));
			}
		}
		else{
			allInvoicesRemoved = true;

			outputDoc.setAttribute("/ChangePOs/name", "" );
	

			TermsParty firstTermsParty = null;
			try{
				firstTermsParty = (TermsParty) terms.getComponentHandle("FirstTermsParty");
			}catch(Exception e){
				LOG.trace("Exception while trying to get existing FirstTermsParty so, creating new FirstTermsParty component. ");
				long firstTermsPartyOid = terms.newComponent("FirstTermsParty");
				firstTermsParty = (TermsParty) terms.getComponentHandle("FirstTermsParty");
			}
			firstTermsParty.setAttribute("name", "");
			firstTermsParty.setAttribute("OTL_customer_id","");
			outputDoc.setAttribute("/ChangePOs/invoice_only_ind",
					TradePortalConstants.INDICATOR_NO);
			terms.setAttribute("invoice_only_ind",
					TradePortalConstants.INDICATOR_NO);
			terms.setAttribute("invoice_details","");
			outputDoc.setAttribute("/ChangePOs/invoice_due_date","");
		}
		// Issue an informational message indicating to the user that the
		// selected Inv have been removed successfully and save the transaction
		outputDoc.setAttribute("/ChangePOs/isStructuredPO",TradePortalConstants.INDICATOR_YES);
		mediatorServices.getErrorManager().issueError(
				TradePortalConstants.ERR_CAT_1,
				TradePortalConstants.REMOVE_INVOICE_SUCCESS,
				String.valueOf(numOfInvs));

	}

	private void performCalculation(Vector invList,Vector invIDList, DocumentHandler outputDoc)
			throws AmsException, RemoteException {
		String transactionType = transaction
				.getAttribute("transaction_type_code");
		if (transactionType.equals(TransactionType.ISSUE)) {
			InvoiceUtility.deriveTransactionCurrencyAndAmountsFromInvoice(
					transaction, terms, invList, instrument,invIDList,"");
		}
		// TODO May be any extra stuff to be added in future
		if (transactionType.equals(TransactionType.AMEND)) {
			InvoiceUtility.deriveTransactionCurrencyAndAmountsFromInvoice(
					transaction, terms, invList, instrument,invIDList,"");
		}
		outputDoc.setAttribute("/ChangePOs/amount",
				terms.getAttribute("amount"));
		outputDoc.setAttribute("/ChangePOs/amount_currency_code",
				terms.getAttribute("amount_currency_code"));
		outputDoc.setAttribute("/ChangePOs/invoice_details",
				terms.getAttribute("invoice_details"));
		SimpleDateFormat iso = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat jPylon = new SimpleDateFormat("MM/dd/yyyy");

		try {
			if (!StringFunction.isBlank(dueDate)) {
				Date date = jPylon.parse(dueDate);
				outputDoc.setAttribute("/ChangePOs/invoice_due_date",
						iso.format(date));
				SimpleDateFormat iso1 = new SimpleDateFormat(
						"yyyy-MM-dd HH:mm:ss.S");

				terms.setAttribute("invoice_due_date",
						DateTimeUtility.convertDateToDateTimeString(date));
				outputDoc.setAttribute("/ChangePOs/invoice_only_ind",
						TradePortalConstants.INDICATOR_YES);
				//IR T36000017596 -set expiry date from invoice
				outputDoc.setAttribute("/ChangePOs/expiry_date",
						iso.format(date));
				terms.setAttribute("expiry_date",
						DateTimeUtility.convertDateToDateTimeString(date));
			}
			
		} catch (Exception e) {
			LOG.error("Exception while dealing with attributes invoice_due_date/expiry_date .", e);
		}
	}

	
}
