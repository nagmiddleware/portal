package com.ams.tradeportal.mediator;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.busobj.OutgoingInterfaceQueue;
import com.ams.tradeportal.busobj.ReferenceDataPending;
import com.ams.tradeportal.busobj.SecurityProfile;
import com.ams.tradeportal.busobj.TradePortalBusinessObject;
import com.ams.tradeportal.busobj.User;
import com.ams.tradeportal.busobj.util.InstrumentServices;
import com.ams.tradeportal.busobj.util.SecurityAccess;
import com.ams.tradeportal.busobj.util.TradePortalAuditor;
import com.ams.tradeportal.common.MessageType;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.frame.ErrorManager;
import com.amsinc.ecsg.frame.InvalidObjectIdentifierException;
import com.amsinc.ecsg.frame.MediatorBean;
import com.amsinc.ecsg.frame.MediatorServices;
import com.amsinc.ecsg.util.DateTimeUtility;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.EncryptDecrypt;
import com.amsinc.ecsg.util.StringFunction;

/*
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class RefDataMediatorBean extends MediatorBean {
private static final Logger LOG = LoggerFactory.getLogger(RefDataMediatorBean.class);

	public static final String INSERT_MODE = "C";
	public static final String UPDATE_MODE = "U";
	public static final String DELETE_MODE = "D";
	public static final String APPROVE_MODE = "A";
	public static final String REJECT_MODE = "R";
	
	public static final String USER_ID = "User ID";
	public static final String CORPORATE_CUSTOMER_NAME = "Corporate Customer Name";
	public static final String PANEL_AUTH_GROUP_ID = "Panel Authorization Group Id";

	// Business methods
	public DocumentHandler execute(DocumentHandler inputDoc, DocumentHandler outputDoc, MediatorServices mediatorServices)
			throws RemoteException, AmsException {

		// Declare variables
		TradePortalBusinessObject busObj = null;
		boolean generateAuditLog = true;
		long lOid = 0;
		String busobjDesc = null;
		String ownerOrgOid = null;
		String objPath = null;

		// Get input arguments that drive the mediator.
		String objectName               = inputDoc.getAttribute("/Update/beanName");
		String oidAttributeName         = inputDoc.getAttribute("/Update/oidFieldName");
		String descAttributeName        = inputDoc.getAttribute("/Update/descFieldName");
		String ownerOrgOidAttributeName = inputDoc.getAttribute("/Update/ownerOrgOidFieldName");
		String notFoundError            = inputDoc.getAttribute("/Update/notFoundError");
		String user_oid					= inputDoc.getAttribute("/LoginUser/user_oid");
		String approverUserOid			= null ;
		//jgadela 11/07/2013 Rel 8.3  -   Corrected. (issue: When dual control is off, the when any refdata is updated, changedUserOid is null)
		String changedUserOid			= user_oid ;
		boolean isPendingRejectAction   = false;
		boolean isPendingApproveAction   = false;
		String myOrgProfileValidation = inputDoc.getAttribute("/CorporateOrganization/myorg_profile_validation");
		boolean	myOrgProfileInd = false;
		
		
		if (objectName == null)
			throw new AmsException("Need to send the objectName parameter.");

		if (oidAttributeName == null)
			throw new AmsException("Need to send the oidFieldName parameter.");

		if (descAttributeName == null)
			throw new AmsException("Need to send the descFieldName parameter.");

		if (notFoundError == null)
			notFoundError = "INF01";

		String updateMode = getUpdateMode(inputDoc, mediatorServices);
		
		if (updateMode == null)
			throw new AmsException("Unable to determine update mode.");

		objPath = "/" + objectName + "/";
		ownerOrgOid = inputDoc.getAttribute(objPath + ownerOrgOidAttributeName);
		
		String secretKeyString = mediatorServices.getCSDB().getCSDBValue("SecretKey");
		byte[] keyBytes = EncryptDecrypt.base64StringToBytes(secretKeyString);
		javax.crypto.SecretKey secretKey = new SecretKeySpec(keyBytes, "DESede/CBC/PKCS5Padding");
		boolean performPendingSave = false;
		
		// Perform update, delete, or insert.
		try {
			// Get the oid from the doc.  '0' indicates a new item so change the mode.
			lOid = inputDoc.getAttributeLong(objPath + oidAttributeName);
			// Get a handle to an instance of the object
			busObj = (TradePortalBusinessObject) mediatorServices.createServerEJB(objectName);

			busobjDesc = inputDoc.getAttribute(objPath + descAttributeName);
			

			if ( lOid == 0  || updateMode.equals(RefDataMediatorBean.INSERT_MODE)) {
					mediatorServices.debug("Mediator: Creating new " + objectName);
					lOid = busObj.newObject();
					outputDoc.setAttributeLong(objPath+oidAttributeName, lOid);
					if (!RefDataMediatorBean.APPROVE_MODE.equalsIgnoreCase(updateMode)
							&& !RefDataMediatorBean.REJECT_MODE.equalsIgnoreCase(updateMode)){
						updateMode = RefDataMediatorBean.INSERT_MODE;
					}
			} else {
					mediatorServices.debug("Mediator: Retrieving " + objectName + " " + lOid);
					try
					{
							busObj.getData(lOid);
							//Save and Save & Close Button functionality regarding OptLock by Prateep
							inputDoc.setAttribute(objPath + "opt_lock", busObj.getAttribute("opt_lock"));
					}
					catch(Exception e)
					{
						mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,TradePortalConstants.ITEM_DELETED);
					}
			}
			
			//Rel9.2 IR#T36000036363 START - For approve and reject, retrieve busObj before user access check so it validates against correct busObj being modified
			if (updateMode.equals(APPROVE_MODE) || updateMode.equals(REJECT_MODE)) {
				String pendingOidStr = getReferenceDataOid(inputDoc);
				long changedObjectOid = Long.parseLong((pendingOidStr!=null && !"".equals(pendingOidStr))?pendingOidStr:"0"); 
				///need to check pending record is still existed, If the two users are logged in same time, and the pending record is approved/rejected by one user, 
				//and second users tries to approve same record
				if(!checkForPendingDataByPendingOid(changedObjectOid)){
					mediatorServices.getErrorManager().issueError(
							TradePortalConstants.ERR_CAT_1,
							TradePortalConstants.PENDING_USER_ALREADY_APPROVED,
							objectName);
				}else{
						ReferenceDataPending pendingObj= getPendingReferenceData(user_oid,getReferenceDataOid(inputDoc),objectName,mediatorServices,TradePortalConstants.PENDING_REC_EXISTS_UPDATE);						
						constructBusinessObject( objectName,  pendingObj,  busObj);
				}
			}
			//Rel9.2 IR#T36000036363 END
			
			outputDoc.setAttribute( "oid", EncryptDecrypt.encryptStringUsingTripleDes(busObj.getAttribute(busObj.getIDAttributeName()), secretKey) );
			String loginUserRights = inputDoc.getAttribute("/LoginUser/security_rights");
			SecurityAccess.validateUserRightsForRefdata(objectName, loginUserRights, updateMode, ownerOrgOid, inputDoc, busObj);
			// Nar IR-T36000030184 Moved the below code to Security Access Delete - Begin
			//Rel9.0 IR#T36000030184 - Check that the logged in user has rights to maintain the action
			//For this IR, only User and SecurityProfile objects are checked. In future, all objects should be checked. - START
            // Nar IR-T36000030184 Moved the below code to Security Access Delete - End
			if (updateMode.equals(REJECT_MODE)) {
				isPendingRejectAction = true;
				
				String pendingOidStr = getReferenceDataOid(inputDoc);
				long changedObjectOid = Long.parseLong((pendingOidStr!=null && !"".equals(pendingOidStr))?pendingOidStr:"0");  
				
				//need to check pending record is still existed, If the two users are logged in same time, and the pending record is rejected by one user, 
				//and second users tries to approve same record
				if(!checkForPendingDataByPendingOid(changedObjectOid)){
				   
					mediatorServices.getErrorManager().issueError(
							TradePortalConstants.ERR_CAT_1,
							TradePortalConstants.PENDING_USER_ALREADY_APPROVED,
							objectName);
				}else{
					ReferenceDataPending pendingObj= getPendingReferenceData(user_oid,getReferenceDataOid(inputDoc),objectName,mediatorServices,TradePortalConstants.PENDING_REC_EXISTS_UPDATE);
					busobjDesc = pendingObj.getAttribute("bus_id");
					deletePendingReferenceData( user_oid,  getReferenceDataOid(inputDoc), objectName,  mediatorServices,  "");
					generateAuditLog = false;
				}
				
			} else if (updateMode.equals(APPROVE_MODE)) {
				isPendingApproveAction   = true;
				
				String pendingOidStr = getReferenceDataOid(inputDoc);
				long changedObjectOid = Long.parseLong((pendingOidStr!=null && !"".equals(pendingOidStr))?pendingOidStr:"0"); 
				///need to check pending record is still existed, If the two users are logged in same time, and the pending record is approved/rejected by one user, 
				//and second users tries to approve same record
				if(!checkForPendingDataByPendingOid(changedObjectOid)){
					mediatorServices.getErrorManager().issueError(
							TradePortalConstants.ERR_CAT_1,
							TradePortalConstants.PENDING_USER_ALREADY_APPROVED,
							objectName);
				}else{
						ReferenceDataPending pendingObj= getPendingReferenceData(user_oid,getReferenceDataOid(inputDoc),objectName,mediatorServices,TradePortalConstants.PENDING_REC_EXISTS_UPDATE);
						
						busobjDesc = pendingObj.getAttribute("bus_id");
				
						approverUserOid = user_oid;
						changedUserOid = pendingObj.getAttribute("change_user_oid");
						updateMode = pendingObj.getAttribute("change_type");
						if(updateMode.equals(DELETE_MODE)){
							busObj.setAttribute("opt_lock", inputDoc.getAttribute(objPath + "opt_lock"));
							deletePendingReferenceData( user_oid,  getReferenceDataOid(inputDoc), objectName,  mediatorServices,  "");
							performDelete(busObj, inputDoc, outputDoc,  mediatorServices);
						}else{
							deletePendingReferenceData( user_oid,  getReferenceDataOid(inputDoc), objectName,  mediatorServices,  "");
							// IR T36000018067- Get saved data as xml from pending refdata table and pass to performSave, then the pending refdata xml will be set to
							// business object and save.
							String xml = StringFunction.xssHtmlToChars(pendingObj.getAttribute("changed_object_data"));
							DocumentHandler tempDoc = new DocumentHandler(xml,false);
							
							//jgadela  REL 8.3 IR T36000021113  [START] - when refdata recored is in pending, if opt_lock is updated by the system(When the refdata is updated by the system),
							//We will get the error when we try to approve that, So we are getting latest opt_lock from system and set to doc.
							tempDoc.setAttribute(objPath + "opt_lock", busObj.getAttribute("opt_lock"));
							//jgadela  REL 8.3 IR T36000021113  [END]

							performSave(user_oid, ownerOrgOid, lOid, objectName, updateMode, busObj, tempDoc, outputDoc, secretKey, mediatorServices,true);
						}
				}
			} else if (updateMode.equals(DELETE_MODE)) {
				
				if (isDualControlRequired(user_oid, ownerOrgOid, objectName, inputDoc, mediatorServices)) {
					
					if(checkForPendingDataAlreadyExists(lOid, objectName)){
						mediatorServices.getErrorManager().issueError(
								TradePortalConstants.ERR_CAT_1,
								TradePortalConstants.PENDING_EXISTS);
					}else{
						performDeletePending(user_oid, ownerOrgOid, lOid, objectName, busObj, inputDoc, outputDoc,  mediatorServices);
					}
					generateAuditLog = false;
				} else {
					// Set optimistic lock to ensure that 
					// object was not changed in between this retrieve
					busObj.setAttribute("opt_lock", inputDoc.getAttribute(objPath + "opt_lock"));
					performDelete(busObj, inputDoc, outputDoc,  mediatorServices);
				}
				
			} else {  
				if (isDualControlRequired(user_oid, ownerOrgOid, objectName, inputDoc, mediatorServices)) {
					
					performPendingSave = true;
					if(updateMode.equals(UPDATE_MODE) ){
						String pendingOid = inputDoc.getAttribute("/PendingReferenceData/oid");
						if( pendingOid == null || "".equalsIgnoreCase(pendingOid)){
							
							if(checkForPendingDataAlreadyExists(lOid, objectName)){
								
								String changeUserId = DatabaseQueryBean.getObjectID("REF_DATA_PENDING","CHANGE_USER_OID","CHANGED_OBJECT_OID=? and CLASS_NAME=?", new Object[]{lOid,objectName});
								//jgadela R91 IR T36000026319 - SQL INJECTION FIX
								DocumentHandler xDoc = DatabaseQueryBean.getXmlResultSet("select FIRST_NAME || ' ' || LAST_NAME as NAME from USERS where USER_OID = ?", false, new Object[]{changeUserId});
								String changeUserName = "";
								if (xDoc != null) {
									changeUserName = xDoc.getAttribute("/ResultSetRecord(0)/NAME");
								}
								String[] params = new String[2];
								if ("User".equals(objectName)) {
									params[0] = "User";
								} else if ("CorporateOrganization".equals(objectName)) {
									params[0] = "Customer";
								}  else if ("PanelAuthorizationGroup".equals(objectName)) {
									params[0] = "Panel Authorization";
								}  
								params[1] = changeUserName;
								mediatorServices.getErrorManager().issueError(
										TradePortalConstants.ERR_CAT_1,
										TradePortalConstants.PENDING_USER_ALREADY_EXISTS ,
										params,null);
								performPendingSave = false;
							}
						}
					}
					
					if(performPendingSave){
						performSavePending(user_oid, ownerOrgOid, lOid, objectName, updateMode, busObj, inputDoc, outputDoc,  mediatorServices);
					}
					generateAuditLog = false;
				} else{
					// do not check Pending ref data duplicates
					performSave(user_oid, ownerOrgOid, lOid, objectName, updateMode, busObj, inputDoc, outputDoc, secretKey, mediatorServices,false);
				}
			}
		} catch (InvalidObjectIdentifierException ex) {

			// If the object does not exist, issue an error
			mediatorServices.getErrorManager ().issueError (
					getClass ().getName (), notFoundError, ex.getObjectId());

		} catch (Exception e) {

			//cquinton 12/6/2012 suppressing errors is bad
			//generate a server error number just use the system milliseconds
			String serverErrorNumber = "SE" + System.currentTimeMillis();	    
			LOG.info("General exception caught in RefDataMediator. " +"Server error#" + serverErrorNumber);
			e.printStackTrace();

			// we don't know exactly what this is, so just issue a generic error
			//another option is to just propogate the exception
			mediatorServices.getErrorManager ().issueError (
					getClass ().getName (), TradePortalConstants.SERVER_ERROR,
					serverErrorNumber );


		} finally {
			
			boolean errorInBean = false;
			if (busObj != null && performPendingSave) {
				/*IR#T36000019324*/
			   errorInBean = (busObj.getMaxErrorSeverity() >= ErrorManager.ERROR_SEVERITY );
			}
			
			if (isTransSuccess(mediatorServices.getErrorManager()) && !errorInBean)
			{
				if (ownerOrgOid == null)
				{
					ownerOrgOid = "";
				}
				//cquinton 2/7/2013 - remove setting of Party outputs here -
				// do as part of PostMediator on specific forms

				// Now try to create an audit record.
				if (generateAuditLog) {
					mediatorServices.debug("Mediator: Creating audit record");
					
					//Rel 8.3 CR 776	
					if(StringFunction.isNotBlank(myOrgProfileValidation)&& TradePortalConstants.MY_ORGANIZATION_PROFILE.equals(myOrgProfileValidation)){
							String className = busObj.getClass().getName();
							int lastDot = className.lastIndexOf(".");
							int beanNameEnd;
			                if(className.indexOf("Bean") >= 0)
			                     beanNameEnd = className.indexOf("Bean_");
			                else
			                     beanNameEnd = className.indexOf("_");
			                
			                className= className.substring(lastDot + 1, beanNameEnd);
			                if("CorporateOrganization".equals(className)){
			                	myOrgProfileInd = true;
			                }
					}
					TradePortalAuditor.createRefDataAudit(mediatorServices,
							busObj,
							String.valueOf(lOid),
							busobjDesc, 
							ownerOrgOid, 
							updateMode, 
							changedUserOid,
							approverUserOid,
							myOrgProfileInd);
					mediatorServices.debug("Mediator: Finished with audit record");
				}
				// Finally issue a success message based on update type.		
				if (isPendingApproveAction || !isDualControlRequired(user_oid, ownerOrgOid, objectName, inputDoc, mediatorServices)) {
					
					if (updateMode.equals(DELETE_MODE)) {
						mediatorServices.getErrorManager().issueError(
								TradePortalConstants.ERR_CAT_1,
								TradePortalConstants.DELETE_SUCCESSFUL,
								busobjDesc);
					} else if (updateMode.equals(INSERT_MODE)) {
						mediatorServices.getErrorManager().issueError(
								TradePortalConstants.ERR_CAT_1,
								TradePortalConstants.INSERT_SUCCESSFUL,
								busobjDesc);
					} else {
						if(StringFunction.isNotBlank(myOrgProfileValidation)&& TradePortalConstants.MY_ORGANIZATION_PROFILE.equals(myOrgProfileValidation)){
							busobjDesc =  mediatorServices.getResourceManager().getText("CorpCustProfileDetail.MyOrganizationProfile",
			   						   TradePortalConstants.TEXT_BUNDLE);
						}
						mediatorServices.getErrorManager().issueError(
								TradePortalConstants.ERR_CAT_1,
								TradePortalConstants.UPDATE_SUCCESSFUL,
								busobjDesc);
					}
				}else if (isPendingRejectAction) {
					mediatorServices.getErrorManager().issueError(
							TradePortalConstants.ERR_CAT_1,
							TradePortalConstants.UPDATES_REJECTED,
							busobjDesc);
				}else{
						if (updateMode.equals(DELETE_MODE)) {
							mediatorServices.getErrorManager().issueError(
									TradePortalConstants.ERR_CAT_1,
									TradePortalConstants.DELETE_SUCCESSFUL_PENDING,
									busobjDesc);
						} else if (updateMode.equals(INSERT_MODE)) {
							mediatorServices.getErrorManager().issueError(
									TradePortalConstants.ERR_CAT_1,
									TradePortalConstants.INSERT_SUCCESSFUL_PENDING ,
									busobjDesc);
						} else {
							mediatorServices.getErrorManager().issueError(
									TradePortalConstants.ERR_CAT_1,
									TradePortalConstants.UPDATE_SUCCESSFUL_PENDING,
									busobjDesc);
						}
				}
			}
		}


		return outputDoc;
	}                      



	/**
	 * @param busObj
	 * @param inputDoc
	 * @param outputDoc
	 * @param mediatorServices
	 * @throws RemoteException
	 * @throws AmsException
	 */
	private void performDelete(TradePortalBusinessObject busObj,DocumentHandler inputDoc,DocumentHandler outputDoc, MediatorServices mediatorServices) throws RemoteException, AmsException {
		mediatorServices.debug("Mediator: Deleting object");


		String deactivate = inputDoc.getAttribute("/Update/deactivate");

		if ( (deactivate == null) || (deactivate.equals(TradePortalConstants.INDICATOR_NO) ) ) {

			busObj.delete();

			mediatorServices.debug("Mediator: Completed the delete of the object");
		} else {
			// A deactivate consists of deactivating and then saving.
			busObj.deactivate();

			busObj.save();

			mediatorServices.debug("Mediator: Completed the deactivate of the object");
		}
	}

	/**
	 * @param user_oid
	 * @param ownerOrgOid
	 * @param lOid
	 * @param objectName
	 * @param updateMode
	 * @param busObj
	 * @param inputDoc
	 * @param outputDoc
	 * @param mediatorServices
	 * @return
	 * @throws RemoteException
	 * @throws AmsException
	 */
	private DocumentHandler performSave(String user_oid, String ownerOrgOid, long lOid, String objectName, String updateMode, TradePortalBusinessObject busObj,DocumentHandler inputDoc,DocumentHandler outputDoc, SecretKey secretKey,MediatorServices mediatorServices,boolean isDupCheckInPenRefData) throws RemoteException, AmsException {

			if (updateMode.equals(INSERT_MODE) && isDupCheckInPenRefData) {
				// if uniqueObjectId is existed in the ref data table, issue error message.
				if(isDuplicateInPendingRefData(inputDoc, mediatorServices, objectName,ownerOrgOid)){
					return outputDoc;
				}
			}

			// This vector is used to delete components of an object during a save.
			Vector deleteVector = inputDoc.getFragments("/ComponentToDelete");
			int deleteCount = deleteVector.size();

			// If there are components to delete, loop through and delete each one BEFORE
			// we populate the parent object.  This information is setup in a pre-mediator.
			mediatorServices.debug("Count of components to delete during save: " + deleteCount);
			if (deleteCount > 0) {
				for (int i=0; i<deleteCount; i++) {
					DocumentHandler deleteDoc = (DocumentHandler) deleteVector.elementAt(i);
					mediatorServices.debug("Component to delete " + deleteDoc.toString(true));

					deleteComponent(deleteDoc, mediatorServices);
				}
			}

		// Set object attributes equal to input arguments and save
		// adding logic to copy object from db instead of from xml when doing a save as.
		String saveAsFlag = inputDoc.getAttribute("/saveAsFlag");
		String oName = inputDoc.getAttribute("/" + objectName + "/" + inputDoc.getAttribute("/Update/descFieldName"));
		if (TradePortalConstants.INDICATOR_YES.equals(saveAsFlag)){
			String origOid = inputDoc.getAttribute("/origOid");
			if (StringFunction.isNotBlank(origOid)){
				TradePortalBusinessObject origBusObj = (TradePortalBusinessObject) mediatorServices.createServerEJB(objectName,Long.parseLong(origOid));
				
				DocumentHandler origBusObjDoc = new DocumentHandler();
				origBusObj.populateXmlDoc(origBusObjDoc);
				String newObjOid = busObj.getAttribute(inputDoc.getAttribute("/Update/oidFieldName"));
				busObj.populateFromXmlDoc(origBusObjDoc);
				busObj.setAttribute(inputDoc.getAttribute("/Update/oidFieldName"), newObjOid);
				busObj.setAttribute(inputDoc.getAttribute("/Update/descFieldName"), oName);
				if ("PaymentDefinitions".equals(objectName)){
					String objPath = "/" + objectName + "/";
					String ownershipLevel = inputDoc.getAttribute(objPath + "ownership_level");
					String ownershipType = inputDoc.getAttribute(objPath + "ownership_type");
					String description = inputDoc.getAttribute(objPath + "description");
					
					busObj.setAttribute("description",description);
					busObj.setAttribute("ownership_level", ownershipLevel);
					busObj.setAttribute("ownership_type", ownershipType);
					busObj.setAttribute("owner_org_oid", inputDoc.getAttribute("/current_owner_org_oid"));
				}
				
			}else{
				busObj.populateFromXmlDoc(inputDoc);
			}
		}else{
			/* Rel 935 IR T36000045559 - duplicate records are getting created on dual control.
			 * (dual authorize) - For Corporate, if any component is added  (eg, Account, customer alias, margin....etc) then, 
			 * duplicate of records are getting created.  
			 *  */
			if (!isDualControlRequired(user_oid, ownerOrgOid, objectName, inputDoc, mediatorServices)) {
				busObj.populateFromXmlDoc(inputDoc);
			}
		}
		
		if( objectName.equals("ArMatchingRule") || objectName.equals("CustomerErpGlCode") || objectName.equals("CustomerDiscountCode"))
		{
			createOutgoingMessage(  objectName,  lOid,  busObj,  outputDoc, secretKey, mediatorServices);
		}
		
		// W Zhu 3/19/2013 Rel 8.2 CR-708B T36000014899 move busObj.save() after oiQueue.save() since busObj.save() could rollback transaction and throw exception for oiQueue.save().
		busObj.save();

		mediatorServices.debug("Mediator: Completed the save of the object");

		return outputDoc;
	}

	/**
	 * @param user_oid
	 * @param ownerOrgOid
	 * @param lOid
	 * @param objectName
	 * @param updateMode
	 * @param busObj
	 * @param inputDoc
	 * @param outputDoc
	 * @param mediatorServices
	 * @return
	 * @throws RemoteException
	 * @throws AmsException
	 */
	private DocumentHandler performSavePending(String user_oid, String ownerOrgOid, long lOid, String objectName, String updateMode, TradePortalBusinessObject busObj,DocumentHandler inputDoc,DocumentHandler outputDoc, MediatorServices mediatorServices) throws RemoteException, AmsException {


		// Set object attributes equal to input  arguments and save
		busObj.populateFromXmlDoc(inputDoc);

		if (busObj.validate(true) == -1) {
			return outputDoc;
		}

		ReferenceDataPending pendingObj = null;
		
		String pendingRefData_oid=getReferenceDataOid(inputDoc);
		if (StringFunction.isNotBlank(pendingRefData_oid)  && !"null".equals(pendingRefData_oid)) {
			pendingObj= getPendingReferenceData(user_oid,pendingRefData_oid,objectName,mediatorServices,TradePortalConstants.PENDING_REC_EXISTS_UPDATE);
			pendingObj.setAttribute("changed_object_data",inputDoc.toString());
		} else {
			if (updateMode.equals(INSERT_MODE)) {
				// if uniqueObjectId is existed in the ref data table, issue error message.
				if(isDuplicateInPendingRefData(inputDoc,mediatorServices,objectName,ownerOrgOid)){
					return outputDoc;
				}
				pendingObj = createPendingRefData(user_oid,ownerOrgOid,lOid,objectName,updateMode,inputDoc,mediatorServices);
			}
			pendingObj = createPendingRefData(user_oid,ownerOrgOid,lOid,objectName,updateMode,inputDoc,mediatorServices);
		}
		
		populateData(pendingObj,  busObj,  objectName,  inputDoc);
		outputDoc.setAttribute( "/PendingReferenceData/oid", pendingObj.getAttribute(pendingObj.getIDAttributeName()));
		pendingObj.save();

		mediatorServices.debug("Mediator: Completed the save of the object");

		return outputDoc;
	}



	/**
	 * @param user_oid
	 * @param ownerOrgOid
	 * @param lOid
	 * @param objectName
	 * @param busObj
	 * @param inputDoc
	 * @param outputDoc
	 * @param mediatorServices
	 * @throws RemoteException
	 * @throws AmsException
	 */
	private void performDeletePending(String user_oid, String ownerOrgOid, long lOid, String objectName, TradePortalBusinessObject busObj,DocumentHandler inputDoc,DocumentHandler outputDoc, MediatorServices mediatorServices) throws RemoteException, AmsException {
		mediatorServices.debug("Mediator: Creating Pending Delete object");

		ReferenceDataPending	pendingObj = createPendingRefData(user_oid,ownerOrgOid,lOid,objectName,DELETE_MODE,inputDoc,mediatorServices);   

	    populateData(pendingObj,  busObj,  objectName,  inputDoc);
		pendingObj.save();
	}




	/**
	 * @param objectName
	 * @param pendingObj
	 * @param busObj
	 * @return
	 * @throws RemoteException
	 * @throws AmsException
	 */
	private TradePortalBusinessObject constructBusinessObject(String objectName, ReferenceDataPending pendingObj, TradePortalBusinessObject busObj) throws RemoteException, AmsException{

		String xml = StringFunction.xssHtmlToChars(pendingObj.getAttribute("changed_object_data"));

		DocumentHandler doc = new DocumentHandler(xml,false);
		busObj.populateFromXmlDoc(doc);

		return busObj;
	}

	/**
	 * @param lOid
	 * @param mediatorServices
	 * @throws RemoteException
	 * @throws AmsException
	 */
	private void createOutgoingMessage(String objectName, long lOid, TradePortalBusinessObject busObj, DocumentHandler outputDoc,SecretKey secretKey,MediatorServices mediatorServices) throws RemoteException, AmsException {
		OutgoingInterfaceQueue oiQueue = null;


		oiQueue = (OutgoingInterfaceQueue) mediatorServices.createServerEJB("OutgoingInterfaceQueue");
		oiQueue.newObject();
		if( objectName.equals("ArMatchingRule")) {
			String attributeNames[] = new String[] {"date_created", "status","msg_type", "message_id", "transaction_oid"};
			String messageId = InstrumentServices.getNewMessageID();
			String attributeValues[] = new String[] {DateTimeUtility.getGMTDateTime(),
					TradePortalConstants.OUTGOING_STATUS_STARTED,
					MessageType.RPMRULE,
					messageId,
					Long.toString(lOid)};

			oiQueue.setAttributes(attributeNames, attributeValues);
		}		
		//AAlubala - Rel8.2 CR741 - ERP Codes - BEGIN
		else if( objectName.equals("CustomerErpGlCode") )
		{
			outputDoc.setAttribute( "oid", EncryptDecrypt.encryptStringUsingTripleDes(
                                          busObj.getAttribute("customer_erp_gl_code_oid"), secretKey) );
			
			oiQueue.setAttribute("date_created",
					DateTimeUtility.getGMTDateTime());
			oiQueue.setAttribute("status",
					TradePortalConstants.OUTGOING_STATUS_STARTED);
			oiQueue.setAttribute("message_id",
					InstrumentServices.getNewMessageID());
			//IValavala CR 741. Remove hardcoded customer_erp_gl_code_oid and replace with the actual oid.
			StringBuilder processParameters = new StringBuilder("REFTABLETYPE=ERPGLCODES|REFTABLEOID=");
			
			oiQueue.setAttribute("process_parameters",
					processParameters.toString()+ busObj.getAttribute("customer_erp_gl_code_oid"));
			
			oiQueue.setAttribute("msg_type",
					TradePortalConstants.CUSREF);

		}			
		else if( objectName.equals("CustomerDiscountCode") )
		{
			outputDoc.setAttribute( "oid", EncryptDecrypt.encryptStringUsingTripleDes(
                                          busObj.getAttribute("customer_disc_code_oid"), secretKey) );
			
			oiQueue = (OutgoingInterfaceQueue) mediatorServices.createServerEJB("OutgoingInterfaceQueue");
			oiQueue.newObject();
			
			oiQueue.setAttribute("date_created",
					DateTimeUtility.getGMTDateTime());
			oiQueue.setAttribute("status",
					TradePortalConstants.OUTGOING_STATUS_STARTED);
			oiQueue.setAttribute("message_id",
					InstrumentServices.getNewMessageID());
			//IValavala CR 741. Remove hardcoded customer_erp_gl_code_oid and replace with the actual oid.
			StringBuilder processParameters = new StringBuilder("REFTABLETYPE=DISCCODES|REFTABLEOID=");
			oiQueue.setAttribute("process_parameters",
					processParameters.toString()+busObj.getAttribute("customer_disc_code_oid"));
			
			oiQueue.setAttribute("msg_type",
					TradePortalConstants.CUSREF);
	}			

			if (oiQueue.save() != 1) {
				throw new AmsException(
						"Error occurred while inserting record in Outgoing Queue for reference data type " + objectName);
			}

	}

	/*
	 * determines what update mode the mediator is in
	 */
	private String getUpdateMode(DocumentHandler inputDoc, MediatorServices mediatorServices) {
		// Now determine what update mode the mediator is in.
		String updateMode = UPDATE_MODE;

		String updateButtonValue = null;
		String deleteButtonValue = null;

		String updateButtonName = inputDoc.getAttribute("/Update/updateButtonName");    
		if (updateButtonName != null)
			updateButtonValue = inputDoc.getAttribute(updateButtonName);

		String deleteButtonName = inputDoc.getAttribute("/Update/deleteButtonName");
		if (deleteButtonName != null)
			deleteButtonValue = inputDoc.getAttribute(deleteButtonName);

		if (updateButtonValue != null)
			updateMode = UPDATE_MODE;

		if (deleteButtonValue != null)
			updateMode = DELETE_MODE;

		String buttonPressed = inputDoc.getAttribute("/Update/ButtonPressed");
		mediatorServices.debug("The button pressed is " + buttonPressed);	

		if (buttonPressed.equals(TradePortalConstants.BUTTON_SAVE))
			updateMode = UPDATE_MODE;
		if (buttonPressed.equals(TradePortalConstants.BUTTON_DELETE))
			updateMode = DELETE_MODE;
		if (buttonPressed.equals(TradePortalConstants.BUTTON_SAVEANDCLOSE))
			updateMode = UPDATE_MODE;
		if (buttonPressed.equals("SaveTrans"))
			updateMode = UPDATE_MODE;
		if (buttonPressed.equals(TradePortalConstants.BUTTON_APPROVE_PENDING_REF_DATA))
			updateMode = APPROVE_MODE;
		if (buttonPressed.equals(TradePortalConstants.BUTTON_REJECT_PENDING_REF_DATA))
			updateMode = REJECT_MODE;

		return updateMode;
	}


	/**
	 * @param user_oid
	 * @param lOid
	 * @param objectName
	 * @param mediatorServices
	 * @param errCode
	 * @throws RemoteException
	 * @throws AmsException
	 */
	private void deletePendingReferenceData(String user_oid,  String pendingRefData_oid,String objectName, MediatorServices mediatorServices, String errCode) throws RemoteException, AmsException {
		ReferenceDataPending refPending= getPendingReferenceData(user_oid,  pendingRefData_oid, objectName, mediatorServices, errCode);
			refPending.delete();
	}

	/**
	 * @param user_oid
	 * @param lOid
	 * @param objectName
	 * @param mediatorServices
	 * @param errCode
	 * @return
	 * @throws AmsException
	 * @throws RemoteException
	 */
	private ReferenceDataPending getPendingReferenceData(String user_oid, String pendingRefData_oid,String objectName, MediatorServices mediatorServices, String errCode) throws AmsException, RemoteException {

		ReferenceDataPending busObj = null;
		busObj = (ReferenceDataPending) mediatorServices.createServerEJB("ReferenceDataPending",Long.parseLong(pendingRefData_oid));
		
		//TODO: handle not found error
		
		return busObj;
	}

	/**
	 * @param pendingObj
	 * @param busObj
	 * @param className
	 * @param inputDoc
	 * @throws RemoteException
	 * @throws AmsException
	 */
	private void populateData(ReferenceDataPending pendingObj, TradePortalBusinessObject busObj, String className, DocumentHandler inputDoc) throws RemoteException, AmsException {

		if ("User".equals(className)) {
			//IR 23184 - use User ID instead of Login id
			pendingObj.setAttribute("bus_id",busObj.getAttribute("user_identifier"));
			pendingObj.setAttribute("name",busObj.getAttribute("first_name")+" "+busObj.getAttribute("last_name"));
		} else if ("CorporateOrganization".equals(className)) {
			pendingObj.setAttribute("bus_id",busObj.getAttribute("name"));
			pendingObj.setAttribute("name",busObj.getAttribute("name"));
		}  else if ("PanelAuthorizationGroup".equals(className)) {
			pendingObj.setAttribute("bus_id",busObj.getAttribute("panel_auth_group_id"));
			pendingObj.setAttribute("name",busObj.getAttribute("name"));
		}  
	}

	/**
	 * @param user_oid
	 * @param ownerOrgOid
	 * @param objectName
	 * @param inputDoc
	 * @param mediatorServices
	 * @return
	 * @throws RemoteException
	 * @throws AmsException
	 */
	private boolean isDualControlRequired(String user_oid, String ownerOrgOid, String objectName, DocumentHandler inputDoc, MediatorServices mediatorServices) throws RemoteException, AmsException {
		boolean dualControl = false;
		
		String objPath = "/" + objectName + "/";
		String isDualControlRequired = inputDoc.getAttribute(objPath + "isDual-control-required");
		if(TradePortalConstants.INDICATOR_YES.equalsIgnoreCase(isDualControlRequired)){
			dualControl = true;  
		}
		return dualControl;

	}

	/**
	 * @param user_oid
	 * @param ownerOrgOid
	 * @param lOid
	 * @param objectName
	 * @param updateMode
	 * @param inputDoc
	 * @param mediatorServices
	 * @return
	 * @throws AmsException
	 * @throws RemoteException
	 */
	private ReferenceDataPending createPendingRefData(String user_oid, String ownerOrgOid, long lOid,String objectName, String updateMode, DocumentHandler inputDoc, MediatorServices mediatorServices) throws AmsException, RemoteException {

		ReferenceDataPending pendingObj = (ReferenceDataPending) mediatorServices.createServerEJB("ReferenceDataPending");
		
		String objPath = "/" + objectName + "/";
		String ownershipLevel = inputDoc.getAttribute(objPath + "ownership_level");
		
		if ("User".equals(objectName) || "PanelAuthorizationGroup".equals(objectName)) {
			ownershipLevel = inputDoc.getAttribute(objPath + "user_type");
		}else if ("CorporateOrganization".equals(objectName)) {
			ownershipLevel = inputDoc.getAttribute(objPath + "ownership_level");
			if(ownershipLevel == null){
				ownershipLevel = "BANK";
			}
		}
		
		pendingObj.newObject();
		pendingObj.setAttribute("change_type", updateMode);
		pendingObj.setAttribute("date_time_stamp", DateTimeUtility.getGMTDateTime());
		pendingObj.setAttribute("class_name", objectName);
		pendingObj.setAttribute("change_user_oid", user_oid);
		pendingObj.setAttribute("changed_object_oid", lOid+"");
		pendingObj.setAttribute("changed_object_data", inputDoc.toString());
		pendingObj.setAttribute("owner_org_oid", ownerOrgOid);
		pendingObj.setAttribute("ownership_level", ownershipLevel);
		//jgadela 10/10/2013 - Rel 8.3 IR T36000021772  -[START] Added to identify to which user type has created the pending data. this is used while approving the pending record by particular user group
		User user = (User) mediatorServices.createServerEJB("User",Long.parseLong(user_oid));
		long secProfileOid = user.getAttributeLong("security_profile_oid");
		SecurityProfile secProfile = (SecurityProfile) user.createServerEJB("SecurityProfile", secProfileOid);
		pendingObj.setAttribute("changed_user_security_type", secProfile.getAttribute("security_profile_type"));
		//jgadela 10/10/2013 - Rel 8.3 IR T36000021772 [END]

		return pendingObj;
	}

	private String getReferenceDataOid(DocumentHandler inputDoc) {
		return inputDoc.getAttribute("/PendingReferenceData/oid");
	}


	/**
	 * This method is used to delete a component object during a save.  
	 * 
	 * @param deleteDoc DocumentHandler - a documenet containing a Name and Oid where name is
	 *                                    the object to delete and oid is its oid.
	 * @return mediatorServices com.amsinc.ecsg.frame.MediatorServices - the current mediator 
	 *                                                  services object
	 */
	private void deleteComponent(DocumentHandler deleteDoc, MediatorServices mediatorServices) throws AmsException, RemoteException
	{
		String			  objectName = deleteDoc.getAttribute("/Name");
		String			  oid = deleteDoc.getAttribute("/Oid");
		TradePortalBusinessObject busObj = null;

		if (!(StringFunction.isBlank(oid)))
		{
			mediatorServices.debug("About to delete object " + objectName + " with oid " + oid);
			busObj = (TradePortalBusinessObject) mediatorServices.createServerEJB(objectName);
			busObj.getData(new Long(oid).longValue());
			busObj.delete();
		}
	}
	
	/**
	 * This method is used to check the refdata record is existed in the pending table 
	 * 
	 * @param deleteDoc DocumentHandler - a documenet containing a Name and Oid where name is
	 *                                    the object to delete and oid is its oid.
	 * 
	 */
	private boolean checkForPendingDataAlreadyExists(long changedOid, String objectName) throws AmsException{
		
		boolean pendingFound = false;
		//jgadela R91 IR T36000026319 - SQL INJECTION FIX
		int recCount = DatabaseQueryBean.getCount("PENDING_DATA_OID", "REF_DATA_PENDING", "CHANGED_OBJECT_OID = ? and CLASS_NAME = ?", false, changedOid, objectName);
        
        if (recCount >0) {
        	pendingFound =  true;
        }
        return pendingFound;
	}
	
	/**
	 * This method is used to check the refdata record is existed in the pending table 
	 * 
	 * @param deleteDoc DocumentHandler - a documenet containing a Name and Oid where name is
	 *                                    the object to delete and oid is its oid.
	 * 
	 */
	private boolean checkForDuplicateRefdataId(String uniqueId, String objectName,String ownerOrgOid) throws AmsException{
		
		boolean pendingFound = false;
		//IR 23327 Start
		//jgadela R91 IR T36000026319 - SQL INJECTION FIX
		List<Object> sqlParams = new ArrayList();
		String whereClause = "BUS_ID = ? and CLASS_NAME = ?";
		sqlParams.add(uniqueId);
		sqlParams.add(objectName);
		if("PanelAuthorizationGroup".equals(objectName) || "User".equals(objectName)) {
			whereClause = "BUS_ID='"+uniqueId+"' and CLASS_NAME='"+objectName+"'" +" and OWNER_ORG_OID = ?";
			sqlParams.add(ownerOrgOid);
		}
		int recCount = DatabaseQueryBean.getCount("PENDING_DATA_OID", "REF_DATA_PENDING", whereClause, false, sqlParams);
		//IR 23327 End
		if (recCount >0) {
        	pendingFound =  true;
        }
        return pendingFound;
	}
	
	/**
	 * This method is used to check the refdata record is existed in the pending table 
	 * 
	 * @param deleteDoc DocumentHandler - a documenet containing a Name and Oid where name is
	 *                                    the object to delete and oid is its oid.
	 * 
	 */
	private boolean isDuplicateInPendingRefData(DocumentHandler inputDoc, MediatorServices mediatorServices, String objectName,String ownerOrgOid) throws AmsException{
		
		String uniqueObjectId = null;
		String objPath = "/" + objectName + "/";
		String errMesgSubtParam2 = "";
		
		// get object unique name and check against pending ref data table before inserting 
		if ("User".equals(objectName)) {
			//IR 23184 - use User ID instead of Login id
			//uniqueObjectId = inputDoc.getAttribute(objPath + "login_id");
			uniqueObjectId = inputDoc.getAttribute(objPath + "user_identifier");
			errMesgSubtParam2 = USER_ID;
		} else if ("CorporateOrganization".equals(objectName)) {
			uniqueObjectId = inputDoc.getAttribute(objPath + "name");
			errMesgSubtParam2 = CORPORATE_CUSTOMER_NAME ;
		}else if ("PanelAuthorizationGroup".equals(objectName)) {
			uniqueObjectId = inputDoc.getAttribute(objPath + "panel_auth_group_id");
			//jgadela Rel 8.3 IR T36000020585	- Corrected the duplicate pending insert error message.
			errMesgSubtParam2 = PANEL_AUTH_GROUP_ID ;
		}
		// if uniqueObjectId is existed in the ref data table, issue error message.
		if(checkForDuplicateRefdataId(uniqueObjectId,objectName,ownerOrgOid)){
			String[] params = new String[2];
			params[0] = uniqueObjectId;
			params[1] = errMesgSubtParam2;
			mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.DUPLICATE_PENDING_USER ,
					params,null);
			return true;
		}else{
			return false;
		}
	}
	
	
	/**
	 * This method is used to check the refdata record is existed using pending OID in the pending table 
	 * 
	 * @param refDataPendingOid long - refDataPendingOid
	 * 
	 */
	private boolean checkForPendingDataByPendingOid(long refDataPendingOid) throws AmsException{
		
		boolean pendingFound = false;
		//jgadela R91 IR T36000026319 - SQL INJECTION FIX
		int recCount = DatabaseQueryBean.getCount("PENDING_DATA_OID", "REF_DATA_PENDING", "PENDING_DATA_OID = ?", false, refDataPendingOid);
        
        if (recCount >0) {
        	pendingFound =  true;
        }
        return pendingFound;
	}
	
	// Nar IR-T36000030184 Moved the below code to Security Access Delete - Begin
	//Rel9.0 IR#T36000030184 - Used to check if the user has the proper access level to perform the action
	//NOTE: Currently implemented for User and SecurityProfile only. Will need to be updated for all objects.
	//START
	/*private BigInteger getMaintainAccess(Object busObj, boolean isAdmin) throws AmsException{
			}else{				
				accessType = SecurityAccess.MAINTAIN_USERS;
			}
		}else if(busObj instanceof SecurityProfile){
			if(isAdmin){
				accessType = SecurityAccess.MAINTAIN_ADM_SEC_PROF;
			}else{
				// KMehta - 10 Sep 2014 - Rel9.1 IR-T36000031954 - Change  
				accessType = SecurityAccess.MAINTAIN_SEC_PROF;
			}
		}else{
			accessType = SecurityAccess.NO_ACCESS;
		}
	}
	
	private boolean isAdmin(DocumentHandler inputDoc, String updateMode, Object busObj) throws RemoteException, AmsException{

		String theType="";
		if(busObj instanceof User){			
			if(INSERT_MODE.equals(updateMode)){
				theType = inputDoc.getAttribute("/User/user_type");
			}else{
				User user = (User) busObj;
				theType = user.getAttribute("ownership_level");
			}
			if (!TradePortalConstants.OWNER_CORPORATE.equals(theType)) {
				return true;
			}			
		}else if(busObj instanceof SecurityProfile){
			if(INSERT_MODE.equals(updateMode)){
				theType = inputDoc.getAttribute("/SecurityProfile/security_profile_type");
			}else{
				SecurityProfile secProfile = (SecurityProfile) busObj;
				theType = secProfile.getAttribute("security_profile_type");
			}
			if(TradePortalConstants.ADMIN.equals(theType)){
				return true;
			}			
		}		
		
		return false;
	}*/
	
//END
	// Nar IR-T36000030184 Moved the below code to Security Access Delete - End
}
