package com.ams.tradeportal.mediator;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.busobj.Instrument;
import com.ams.tradeportal.busobj.MailMessage;
import com.ams.tradeportal.busobj.Template;
import com.ams.tradeportal.busobj.util.InstrumentServices;
import com.ams.tradeportal.busobj.util.TradePortalAuditor;
import com.ams.tradeportal.common.InstrumentType;
import com.ams.tradeportal.common.TradePortalConstants;
import com.ams.tradeportal.common.TransactionType;
import com.amsinc.ecsg.frame.AmsConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.frame.InstrumentLockException;
import com.amsinc.ecsg.frame.LockingManager;
import com.amsinc.ecsg.frame.MediatorBean;
import com.amsinc.ecsg.frame.MediatorServices;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.EncryptDecrypt;
import com.amsinc.ecsg.util.StringFunction;

/*
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class CreateTransactionMediatorBean extends MediatorBean {
private static final Logger LOG = LoggerFactory.getLogger(CreateTransactionMediatorBean.class);

  private static final String CREATE_INSTR = "Create a new instrument";
  private static final String CREATE_TEMP  = "Create a new template";
  private static final String CREATE_TRANS = "Create a new transaction on an existing instrument";
  private static final String CREATE_NONE  = "Don't create nuthin. Just go to next page";

  /**
   * execute () is the main function for this mediator
   *
   * This function will call other functions and execute logic that will create
   * a new instrument if necessary.  A transaction will then be created for the
   * existing or newly created instrument.
   *
   * INPUTDOC - The input doc must contain the following
   *    /instrumentOid ---------- The existing instruments oid or "0" for a new instr.
   *    /transactionType -------- The type of the transaction being created
   *    /bankBranch ------------- The oid of the Operational Bank Org
   *    /clientBankOid ---------- The oid of the client bank that owns the corporation
   *    /userOid ---------------- The oid of the user creating the doc
   *    /securityRights --------- The security rights of the user that is creating the transaction
   *    /mode ------------------- The mode we are in, (TP CONSTANTS: NEW_INSTR, EXISTING_INSTR, TRANSFER_ELC)
   *    /copyType --------------- What we copy a new instrument from (TP CONSTANTS: FROM_INSTR, FROM_TEMPL, & FROM_BLANK)
   *    /ownerOrg ------------- The oid of the user's corporate organization
   *    /validationState -------- Tells the mediator what, if any validations to perform
   *
   * @param DocumentHandler inputDoc - described above
   * @param DocumentHandler outputDoc - passed in by framework
   * @param MediatorServices mediatorServices - passed in by framework
   * @return DocumentHandler - will contain /Transaction/oid
   */
  public DocumentHandler execute(DocumentHandler inputDoc, DocumentHandler outputDoc, MediatorServices mediatorServices)
	 throws RemoteException, AmsException
  {
    mediatorServices.debug("inputDoc follows ->\n" + inputDoc.toString());

    //whatToValidate comes from an additional parameter in the formMgr form
    //and it tells us what if any jsp we should validate
    String whatToValidate = inputDoc.getAttribute("/validationState");
    mediatorServices.debug ("Validating the following -> " + whatToValidate);
    String manualInstrumentId = inputDoc.getAttribute("/manualInstrumentOid");
	mediatorServices.debug("manulInst ID:"+manualInstrumentId);
    if (whatToValidate.equals(TradePortalConstants.VALIDATE_NEW_TRANS_STEP1))
    {
        //Validate the data on NewTransactionStep1.jsp
        if(!valdateStepOne(inputDoc, mediatorServices))
            return outputDoc;
    }
    else if (whatToValidate.equals(TradePortalConstants.VALIDATE_NEW_TEMPLATE))
    {
        //Validate the data on NewTemplate.jsp
        if(!validateNewTemplate(inputDoc, mediatorServices))
            return outputDoc;
    }
    //IR T36000013682 start- If there is a pending transaction then do not proceed further untill it is processed
    String transType = inputDoc.getAttribute("/transactionType");
    mediatorServices.debug ("transType -> " + transType);
    String messageOid = inputDoc.getAttribute("/mailMessageOid");
    mediatorServices.debug ("messageOid -> " + messageOid);
	String conversionCenterMenuInd = inputDoc.getAttribute("/conversionCenterMenuInd");
	LOG.debug("conversionCenterMenuInd ->"+conversionCenterMenuInd);
    if (TransactionType.DISCREPANCY.equals(transType) || TransactionType.APPROVAL_TO_PAY_RESPONSE.equals(transType) || 
    		TransactionType.SIR.equals(transType) )
    {
        //set the relationship between the mail message and this transaction
        MailMessage msg = (MailMessage) mediatorServices.createServerEJB ("MailMessage", Long.parseLong(messageOid));
        
        if(StringFunction.isNotBlank(msg.getAttribute("response_transaction_oid"))){
        	mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
					 TradePortalConstants.DUPL_RESP_TRAN);
        	return outputDoc;
        }
        
    }
	//Srinivasu_D CR#269 Rel8.4 09/02/2013 - start
	if(StringFunction.isNotBlank(conversionCenterMenuInd) && TradePortalConstants.INDICATOR_YES.equals(conversionCenterMenuInd)){
		if(StringFunction.isBlank(manualInstrumentId)) {
		mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
				 TradePortalConstants.REQUIRED_CC_INSTRUMENT_ID);
		return outputDoc;
	}
	else if(StringFunction.isNotBlank(manualInstrumentId)) {
		if(validateConversionInstrumentId(inputDoc,mediatorServices)){
		
		mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
				 TradePortalConstants.DUPLICATE_INSTRUMENT_ID);
		return outputDoc;
		
		}
	 }
	}
	//Srinivasu_D CR#269 Rel8.4 09/02/2013 - end
    //IR T36000013682 end
    //Now we will determine what to do next: create an instrument, create a template,
    //create a transaction, or just go to the next page
    String nextAction = determineNextAction(inputDoc, mediatorServices);
    mediatorServices.debug("The mediator's next action is -> " + nextAction);
    mediatorServices.getCSDB().setCSDBValue(TradePortalConstants.TRAN_ACTION, TradePortalConstants.TRAN_ACTION_CREATE);            	// NSX CR-574 09/21/10
    String newTransactionOid = null;
    if (nextAction.equals(CREATE_INSTR))
    {
        //Create a new instrument
        Instrument newInstr = (Instrument) mediatorServices.createServerEJB("Instrument");
        newTransactionOid = newInstr.createNew(inputDoc);
        mediatorServices.debug("New instrument created with an original transacion oid of -> " + newTransactionOid);
    }
    else if (nextAction.equals(CREATE_TEMP))
    {
        //Create a new template
        Template newTemp = (Template) mediatorServices.createServerEJB("Template");
        outputDoc = newTemp.createNew(inputDoc);

       if (this.isTransSuccess(mediatorServices.getErrorManager()))
        {
   			mediatorServices.debug("Mediator: Creating audit record");
			TradePortalAuditor.createRefDataAudit(mediatorServices,
									newTemp,
									inputDoc.getAttribute("/Template/oid"),
									inputDoc.getAttribute("/name"),
									inputDoc.getAttribute("/ownerOrg"),  //
									"C",
									inputDoc.getAttribute("/userOid")); //
			mediatorServices.debug("Mediator: Finished with audit record");
		 }

        mediatorServices.debug("New template created with a transaction oid of -> " + newTransactionOid);
    }
    else if (nextAction.equals(CREATE_TRANS))
    {
        Instrument existInstr = (Instrument) mediatorServices.createServerEJB("Instrument");
        newTransactionOid = existInstr.createTransOnExisting(inputDoc);
        mediatorServices.debug("New transaction has been created on existing instr. New transaction oid -> " + newTransactionOid);
    }

    mediatorServices.debug (inputDoc.toString());

    if (newTransactionOid != null)
    {
        outputDoc.setAttribute ("/Transaction/oid", newTransactionOid);

        //ctq portal refresh
        //set a flag on the output that indicates its a new transaction
        outputDoc.setAttribute ("/newTransaction", TradePortalConstants.INDICATOR_YES );

        //Added for discrepancies - START
        
        if (TransactionType.DISCREPANCY.equals(transType) || TransactionType.SIR.equals(transType) )
        {
            //set the relationship between the mail message and this transaction
            MailMessage msg = (MailMessage) mediatorServices.createServerEJB ("MailMessage", Long.parseLong(messageOid));
            msg.setAttribute("response_transaction_oid", newTransactionOid);
            msg.save(false); //--Unresolved Error
        }
        //Added for discrepancies - END

        //rkrishna CR 375-D ATP-Response 07/26/2007 Begin
        else if (transType!=null && transType.equals(TransactionType.APPROVAL_TO_PAY_RESPONSE))
        {
            //set the relationship between the mail message and this transaction
            MailMessage msg = (MailMessage) mediatorServices.createServerEJB ("MailMessage", Long.parseLong(messageOid));
            msg.setAttribute("response_transaction_oid", newTransactionOid);
            msg.save(false); //--Unresolved Error
        }
        //rkrishna CR 375-D ATP-Response 07/26/2007 End
    }

    return outputDoc;
  }//end execute(...)




  private boolean validateNewTemplate (DocumentHandler inputDoc, MediatorServices mediatorServices) throws RemoteException, AmsException
  {
    String copyType     = inputDoc.getAttribute("/copyType");
    String instrType    = inputDoc.getAttribute("/instrumentType");
    String name         = inputDoc.getAttribute("/name");
    String ownerOrg     = inputDoc.getAttribute("/ownerOrg");
    String alias        = "";

    boolean dataIsGood  = true;

    mediatorServices.debug("Validate based on:\n\tname: " + name + "\n\tcopyType: " + copyType + "\n\tinstrType: " + instrType);

    /************************
     * CHECK FOR ERRORS START
     ************************/
    if (InstrumentServices.isBlank(name))
    {
        mediatorServices.debug("Name is null.  Returning to NewTemplate page.");
        dataIsGood = false;

	alias = mediatorServices.getResourceManager().getText("NewInstTemplate1.TemplateName",
                                                     TradePortalConstants.TEXT_BUNDLE);
	if (alias == null) alias = "TemplateName";

       	mediatorServices.getErrorManager().issueError (TradePortalConstants.ERR_CAT_1,
					   AmsConstants.REQUIRED_ATTRIBUTE,
					   alias);
    } else {
	// Name is not blank, check for uniqueness within the org.

	String where = " name = ? and p_owner_org_oid = ?";

	mediatorServices.debug("name uniqueness test whereclause: " + where);

	int count =
                DatabaseQueryBean.getCount( "name", "template", where, false, name, ownerOrg);

	if (count >= 1) {
		alias = mediatorServices.getResourceManager().getText("NewInstTemplate1.TemplateName",
                                                     TradePortalConstants.TEXT_BUNDLE);
		if (alias == null) alias = "TemplateName";
		mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
			                          TradePortalConstants.ALREADY_EXISTS,
						  name,
						  alias);
	}
    }


    if (copyType.equals(TradePortalConstants.FROM_BLANK) && InstrumentServices.isBlank(instrType))
    {
        mediatorServices.debug("instrType is null. Returning to NewTemplate page.");
        dataIsGood = false;
        mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
						AmsConstants.REQUIRED_ATTRIBUTE,
                                                mediatorServices.getResourceManager().getText("NewInstTemplate1.InstrumentType",
                                                     TradePortalConstants.TEXT_BUNDLE));
    }

	//IAZ CR-586 08/16/10 Begin
    String isExpress = inputDoc.getAttribute("/isExpress");
    String isFixed = inputDoc.getAttribute("/isFixed");
    LOG.debug("isFixex is " + isFixed + "isExpress is " + isExpress);
	if((TradePortalConstants.INDICATOR_ON.equalsIgnoreCase(isFixed)) &&
		(TradePortalConstants.INDICATOR_ON.equalsIgnoreCase(isExpress)))

	{
	 	mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
	 			TradePortalConstants.CANT_SELECT_FIXED_AND_EXPRS);
	 	dataIsGood = false;
	}
	//IAZ CR-586 08/16/10 End
    /**********************
     * CHECK FOR ERRORS END
     **********************/
    return dataIsGood;
  }



  private boolean valdateStepOne (DocumentHandler inputDoc, MediatorServices mediatorServices) throws RemoteException, AmsException
  {
    boolean error  = false; // this boolean is set to true if any required fields are null

    String bankBranch = inputDoc.getAttribute("/bankBranch");
    String copyType   = inputDoc.getAttribute("/copyType");
    String instrType  = inputDoc.getAttribute("/instrumentType");
    String mode       = inputDoc.getAttribute("/mode");

    mediatorServices.debug("Validate based on:\n\tmode: " + mode + "\n\tcopyType: " + copyType +
          "\n\tinstrType: " + instrType + "\n\tbankBranch: " + bankBranch);

    if (InstrumentServices.isBlank(mode))
    {
        mediatorServices.debug("mode is null.  Returning to NewTransactionStep1.");
        if (mediatorServices.getErrorManager() == null)
            mediatorServices.debug("mediatorServices.getErrorManager is null");

        mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                                                TradePortalConstants.NTS1_MODE_NULL);
        error = true;
    }
    //THE FOLLOWING IF() STATEMENTS MUST BE EXECUTED IN THIS ORDER
    //OTHERWISE A NULL POINTER EXCEPTION COULD BE THROWN
    if (!error && mode.equals(TradePortalConstants.NEW_INSTR))
    {
        //New instrument
        //Check BankBranch
        if (InstrumentServices.isBlank(bankBranch))
        {
            mediatorServices.debug("bankBranch is null.  Returning to NewTransactionStep1.");
            mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                                                TradePortalConstants.NTS1_BANK_BRANCH_NULL);
            error = true;
        }

        //Check what we're copying from
        if (InstrumentServices.isBlank(copyType))
        {
            mediatorServices.debug("copyType is null.  Returning to NewTransactionStep1.");
            mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                                                    TradePortalConstants.NTS1_COPY_FROM_NULL);
            error = true;
        }
        //THE FOLLOWING IF() STATEMENT MUST BE EXECUTED IN THIS ORDER
        //OTHERWISE A NULL POINTER EXCEPTION COULD BE THROWN
        if (!error && copyType.equals(TradePortalConstants.FROM_BLANK) && InstrumentServices.isBlank(instrType))
        {
            mediatorServices.debug("instrType is null.  Returning to NewTransactionStep1.");
            mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                                                        TradePortalConstants.NTS1_INSTR_TYPE_NULL);
            error = true;
        }

    }//end new instrument

    return (!error);
  }



  /**
   * determineNextAction
   *
   * This method takes the inputDoc and figures out if we are ready to create anything, and
   * if so what we should create.  Possible return values follow:
   *    private final String CREATE_INSTR = "Create a new instrument";
   *    private final String CREATE_TEMP  = "Create a new template";
   *    private final String CREATE_TRANS = "Create a new transaction on an existing instrument";
   *    private final String CREATE_NONE  = "Don't create nuthin. Just go to next page";
   *
   * @param DocumentHandler inputDoc
   * @param MediatorServices medService
   * @return java.lang.String
   */
  String determineNextAction(DocumentHandler inputDoc, MediatorServices medService) throws RemoteException, AmsException
  {
    String mode             = inputDoc.getAttribute("/mode");
    String copyType         = inputDoc.getAttribute("/copyType");
    String instrumentOid    = InstrumentServices.parseForOid(inputDoc.getAttribute("/instrumentOid"));
    String transactionType  = inputDoc.getAttribute("/transactionType");


    medService.debug("Determine next action based on:\n\tmode: " + mode + "\n\tcopyType: " + copyType +
          "\n\tinstrumentOid: " + instrumentOid + "\n\ttransactionType: " + transactionType);

    /*
     * Create a new instrument if one of the following is true:
     *  The user selected to create a new instrument from a blank form
     *  The user selected to copy to a new instrument and the source instrument or template has been selected
     */
    if (mode.equals(TradePortalConstants.NEW_INSTR) && (copyType.equals(TradePortalConstants.FROM_BLANK) ||
                                                        (!InstrumentServices.isBlank(instrumentOid))))
    {
        medService.debug("Next Step = CREATE_INSTR");
        return CREATE_INSTR;
    }

    /*
     * Create a new instrument if one of the following is true:
     *  The user selected to transfer an export lc and an LC to transfer has been selected
     */
    if (mode.equals(TradePortalConstants.NEW_INSTR) &&
        copyType.equals(TradePortalConstants.TRANSFER_ELC) &&
        (!InstrumentServices.isBlank(instrumentOid)))
    {
        medService.debug("Next Step = CREATE_INSTR");
        return CREATE_INSTR;
    }


    /*
     * Create s new template if one of the following is true:
     *  The user selected to create a new template from a blank form.
     *  The user selected to copy to a new template and the source instrument or template has been selected
     */
    else if (mode.equals(TradePortalConstants.NEW_TEMPLATE) && (copyType.equals(TradePortalConstants.FROM_BLANK) ||
                                                             (!InstrumentServices.isBlank(instrumentOid))))
    {
        medService.debug("Next Step = CREATE_TEMP");
        return CREATE_TEMP;
    }
    /*
     * Create a new transaction on an existing instrument if the existing instrument has been selected and
     * the transaction type can be resolved or has also been selected.
     */
    else if (mode.equals(TradePortalConstants.EXISTING_INSTR) && (!InstrumentServices.isBlank(instrumentOid)))
    {
        Instrument instr = (Instrument) medService.createServerEJB("Instrument");
        try{
        	instr.getData(Long.parseLong(instrumentOid));
        }catch(NumberFormatException ne){
        	//There will be a NumberFormatException in case the instrumentOid is encrypted. And such a value is passed only when copy instrument is done from a datagrid.
        	//To handle such case, we are decrypting the Oid.
        	//Done only for Portal Refresh - Pavani Mitnala
	// W Zhu 9/11/2012 Rel 8.1 T36000004579 add key parameter.
        String secretKeyString = medService.getCSDB().getCSDBValue("SecretKey");
        byte[] keyBytes = EncryptDecrypt.base64StringToBytes(secretKeyString);
        javax.crypto.SecretKey secretKey = new javax.crypto.spec.SecretKeySpec(keyBytes, "DESede/CBC/PKCS5Padding");
        	instrumentOid = EncryptDecrypt.decryptStringUsingTripleDes(instrumentOid, secretKey);
        	instr.getData(Long.parseLong(instrumentOid));

        }
        String instrumentType = instr.getAttribute("instrument_type_code");

        //only look for the possible transaction types if this is not a discrepancy response
        //rkrishna CR 375-D ATP-Response 08/22/2007 Inserted APPROVAL_TO_PAY_RESPONSE condtion
        if (!(!InstrumentServices.isBlank(transactionType) && 
        		(TransactionType.DISCREPANCY.equals(transactionType) ||
        				TransactionType.APPROVAL_TO_PAY_RESPONSE.equals(transactionType) ||
        				TradePortalConstants.CONVERTED_TRANSACTION_PAYMENT.equals(transactionType)	
        		)))
        {
            List<String> possibleTranTypes = InstrumentServices.getPossibleTransactionTypes(instrumentType,
                                                                                  instr.getAttribute("original_transaction_oid"));
            if (possibleTranTypes.size() == 0)
            {
                //Instrument status is pending
                medService.debug ("possibleTypes.size() == 0");
                medService.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                                                        TradePortalConstants.NTM_INVALID_EXIST_INSTR_TYPE,
                                                        instrumentType);
            }
            else if (possibleTranTypes.size() == 1)
            //if there is only one type of transaction that can be created, go ahead and call the mediator
            {
                /*************************
                 * START LOCK INSTRUMENT *
                 *************************/
                long theOid = Long.parseLong(instrumentOid);
                long userOid = Long.valueOf(inputDoc.getAttribute("/userOid")).longValue();
                medService.debug("Trying to lock the instrument");
                try
                {
                    LockingManager.lockBusinessObject(theOid, userOid, false);
                    medService.debug("Instrument sucessfully locked for this user.");
                }
                catch (InstrumentLockException e)
                {
                    // instrument locked by someone else, issue an error that will show
                    // up on the transaction page
                    if (e.getUserOid() != userOid) {
                        medService.debug("Instrument locked by another user: " + e.getLastName());
                        medService.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                                                            TradePortalConstants.INSTRUMENT_LOCKED,
                                                            e.getFirstName() + " " + e.getLastName() );
                    }
                    else
                        medService.debug("Instrument is already locked by this user.");
                }
                catch (AmsException e)
                {
                    LOG.info("Exception found trying to lock instrument "
                                        + instrumentOid);
                    LOG.info("   " + e.toString());
                }
                /*************************
                 *  END LOCK INSTRUMENT  *
                *************************/
                //IR T36000013682 start- If there is a pending transaction then do not proceed further untill it is processed
                if (instr.pendingTransactionsExist() && !instrumentType.equals(InstrumentType.STANDBY_LC))
            	{
            		//ERROR
            		medService.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
            									 TradePortalConstants.NTM_OTHER_TRANS_PENDING);
            	}
                //IR T36000013682 end
                
                transactionType = possibleTranTypes.get(0);
                inputDoc.setAttribute("/transactionType", transactionType);
            }
            
        }
      /*IR T36000013682 - If there is a pending transaction then do not proceed further untill it is processed
    	if((transactionType.equals(TradePortalConstants.DISCREPANCY)||transactionType.equals(TradePortalConstants.APPROVAL_TO_PAY_RESPONSE)) 
    			&& instr.pendingResponseTransactionsExist()){
    		medService.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
					 TradePortalConstants.NTM_OTHER_TRANS_PENDING);
    		//return CREATE_NONE;
    	}*/
    	//IR T36000013682 end
    	

        if (!InstrumentServices.isBlank(transactionType))
        {
            medService.debug("Next Step = CREATE_TRANS");
            return CREATE_TRANS;
        }
    }

    return CREATE_NONE;

  }
	//Srinivasu_D CR#269 Rel8.4 09/02/2013 - start
  /**
   * This method looks for duplicate instrument is available or not
   * in db and returns true or false based on availability. 
   * @param inputDoc
   * @param mediatorServices
   * @return
   * @throws AmsException
   */
  private boolean validateConversionInstrumentId(DocumentHandler inputDoc,MediatorServices mediatorServices) throws AmsException {
	  
	  boolean duplInstId = false;
	  Vector rec  = null;
	  DocumentHandler  uploadInvoiceDoc = null;
	  String manualInstId = inputDoc.getAttribute("/manualInstrumentOid");
	  String opBankOid = inputDoc.getAttribute("/bankBranch");
	  String corpOrgOid = inputDoc.getAttribute("/ownerOrg");

	  List<Object> sqlParmsLst = new ArrayList();
	  StringBuilder sql = new StringBuilder("select complete_Instrument_id from instrument where");
		if(StringFunction.isNotBlank(opBankOid)){
			sql.append(" a_op_bank_org_oid = ? ");
			sqlParmsLst.add(opBankOid);
		}
		sql.append(" and a_corp_org_oid = ? ");
		sql.append(" and complete_Instrument_id = ?");
		sqlParmsLst.add(corpOrgOid);
		sqlParmsLst.add(manualInstId);

	  DocumentHandler resultSetDoc = DatabaseQueryBean.getXmlResultSet(sql.toString(), false, sqlParmsLst);	
	  LOG.debug("outputDoc->"+resultSetDoc);	
	  if(resultSetDoc!=null){
		  
		   rec  = resultSetDoc.getFragments("/ResultSetRecord");
		   if(rec!=null && rec.size()>0){
				 for(int pos = 0; pos<rec.size();pos++){
				 uploadInvoiceDoc = (DocumentHandler) rec.get(pos);				 
				 String instrOid = uploadInvoiceDoc.getAttribute("/COMPLETE_INSTRUMENT_ID");	

				if( StringFunction.isNotBlank(instrOid)){
					duplInstId=true;				
				}
			}
		}
	  }
	return duplInstId;

  }
  

}
