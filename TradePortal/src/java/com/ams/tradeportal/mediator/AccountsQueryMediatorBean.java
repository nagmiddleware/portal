package com.ams.tradeportal.mediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class is used for querying account balances from bank and
 * updating the accounts.
 *
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ams.tradeportal.agents.AcctBalanceQuery;
import com.ams.tradeportal.busobj.ClientBank;
import com.ams.tradeportal.busobj.CorporateOrganization;
import com.ams.tradeportal.busobj.User;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.BusinessObject;
import com.amsinc.ecsg.frame.ComponentList;
import com.amsinc.ecsg.frame.MediatorBean;
import com.amsinc.ecsg.frame.MediatorServices;
import com.amsinc.ecsg.util.DocumentHandler;

public class AccountsQueryMediatorBean extends MediatorBean
{
private static final Logger LOG = LoggerFactory.getLogger(AccountsQueryMediatorBean.class);
   // The following variables have been declared transient because the mediator
   // is stateless and they are initialized in only one place.
   private transient MediatorServices   mediatorServices    = null;
   private transient String 			userLocale 			= null;


  /**
   *
   * This is a non-transactional mediator that executes transactional
   * methods in business objects
   *
   * @param inputDoc The input XML document (&lt;In&gt; section of overall
   * Mediator XML document)
   * @param outputDoc The output XML document (&lt;Out&gt; section of overall
   * Mediator XML document)
   * @param newMediatorServices Associated class that contains lots of the
   * auxillary functionality for a mediator.
   */
   public DocumentHandler execute(DocumentHandler inputDoc, DocumentHandler outputDoc,
                                  MediatorServices newMediatorServices) throws RemoteException, AmsException
   {

	   mediatorServices    = newMediatorServices;
	   String userOid      = inputDoc.getAttribute("/userOid");
	   String corpOrgOid   = inputDoc.getAttribute("/ownerOrgOid");
	   userLocale 		  = mediatorServices.getCSDB().getLocaleName();

	   // if the user locale not specified default to en_US
	   if((userLocale == null) || (userLocale.equals("")))
	   {
		   userLocale		  = "en_US";
	   }

       int retCode = 0; //IAZ IR-TOUJ042348632 09/30/09 Add
	   try
	   {

		   List accounts = new ArrayList();
		   User	user = (User) mediatorServices.createServerEJB("User", Long.parseLong(userOid));

		   ClientBank clientBank = (ClientBank) mediatorServices.createServerEJB("ClientBank");
	       clientBank.getDataFromReferenceCache(user.getAttributeLong("client_bank_oid"));

		   CorporateOrganization corpOrg = (CorporateOrganization) mediatorServices.createServerEJB("CorporateOrganization", Long.parseLong(corpOrgOid));
		   ComponentList acctList = (ComponentList)corpOrg.getComponentHandle("AccountList");
		   int tot = acctList.getObjectCount();
		   Map accountMap = new HashMap(tot);
		   BusinessObject busObj = null;
		   for (int i = 0; i < tot; i++) {
			   acctList.scrollToObjectByIndex(i);
			   busObj = acctList.getBusinessObject();
			   accounts.add(busObj);
			   accountMap.put(busObj.getAttribute("account_oid"), busObj);
		   }
		   Map m = null;
		   try {
		      m=(new AcctBalanceQuery()).queryAcctBalance(userOid, null, clientBank, corpOrg, accounts);
		   }
		   catch (AmsException ex) {
			   if (TradePortalConstants.MISSING_REQUIRED_FIELDS.equals(ex.getMessage())){
				   
				   mediatorServices.getErrorManager().issueError(
							TradePortalConstants.ERR_CAT_1, TradePortalConstants.BAL_QUERY_MISSING_FIELD);
				   return outputDoc;
			   }
			   else {
				   throw ex;
			   }
		   }
		   int tot_errrors_returned = -1;
		   if (m!= null) {
		   corpOrg = (CorporateOrganization) mediatorServices.createServerEJB("CorporateOrganization", Long.parseLong(corpOrgOid));
		   acctList = (ComponentList)corpOrg.getComponentHandle("AccountList");
		   tot = acctList.getObjectCount();
		   for (int i = 0; i < tot; i++) {
			   acctList.scrollToObjectByIndex(i);
			   busObj = acctList.getBusinessObject();
			   String key = busObj.getAttribute("account_oid");
			   BusinessObject updBusObj = (BusinessObject)accountMap.get(key);
			   if (updBusObj !=null) {
				   busObj.setAttribute("current_balance", updBusObj.getAttribute("current_balance"));
				   busObj.setAttribute("available_funds", updBusObj.getAttribute("available_funds"));
				   busObj.setAttribute("interest_rate", updBusObj.getAttribute("interest_rate"));
				   busObj.setAttribute("last_retrieved_from_bank_date", updBusObj.getAttribute("last_retrieved_from_bank_date"));
				   busObj.setAttribute("status_code", updBusObj.getAttribute("status_code"));
			   }
		   }

           //if (m != null) {
        	 corpOrg.touch();
        	 corpOrg.setRetryOptLock(true);	//IAZ IR-TOUJ042348632 09/30/09 Add
		     retCode = corpOrg.save(false); //IAZ IR-TOUJ042348632 09/30/09 CHT
		     tot_errrors_returned = m.size();
		     LOG.debug("AccountQueryMediator: Total number of accounts that had error status code {}", tot_errrors_returned);
		   }

           //IAZ IR-TOUJ042348632 09/30/09 Begin
           if (retCode == -2)
           		LOG.debug("Optimistic Lock Exception Updating Account Balances on Refresh.");

		   //if (m==null || tot_errrors_returned > 0) {
		   if (m==null || tot_errrors_returned > 0 || retCode != 1) {
		   //IAZ IR-TOUJ042348632 09/30/09 End
			   mediatorServices.getErrorManager().initialize(); // clear 
				mediatorServices.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1, TradePortalConstants.ACCT_BAL_NOT_AVAILABLE);
		   }
		   
		   
	   }
	   catch (Exception e)
	   {
		   LOG.error("Exception occured in AccountQueryMediator: ", e);
	   }

	   return outputDoc;
   }

   public Map queryAcctBalance(String user_oid, String transactionOID, ClientBank clientBank, CorporateOrganization corpOrg, List accounts) throws RemoteException, AmsException {
	   return (new AcctBalanceQuery()).queryAcctBalance(user_oid,transactionOID,clientBank,corpOrg,accounts);
   }
}
