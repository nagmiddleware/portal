package com.ams.tradeportal.devtool;

import com.ams.tradeportal.common.Password;

/**
 * Nar IR-T36000032592 Rel9.2 10/13/2014 
 * This class is used to perform  encryption passwords.  
 */

public class PasswordHelper {

	
	 public static void main(String argc[])
     {
        if (argc.length == 0)
         {
            System.out.println("Please enter a password to encrypt...");
            return;
         }
         
        // Get the new password from the command-line arguments
        String newPassword = argc[0];     
        
        String salt;
                
        if (argc.length != 1)
         {
            salt = argc[1];
         }
        else
         {
            System.out.println("   No salt specified... Generating random 232-bit salt...");
            salt = Password.getEncryptedRandomBytes(232);
         }

        System.out.println("    Password entered: "+newPassword);
        System.out.println("    Encrypted Salt: \n"+salt);
        System.out.println("\n    Encrypting...");
            
        // Encrypt the password    
        String encryptedPassword = Password.encryptPassword(newPassword, salt);
        
        System.out.println("\n    Encrypted Password:\n"+ encryptedPassword);
    
     
     } 
     
}
