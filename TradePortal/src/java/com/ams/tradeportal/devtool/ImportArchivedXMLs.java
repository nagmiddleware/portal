import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.util.Properties;
import java.sql.*;
//import java.util.concurrent.TimeUnit;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.io.PrintWriter;
import oracle.jdbc.driver.OracleDriver;
import java.security.MessageDigest;
import java.util.zip.ZipInputStream;
import java.util.Arrays;

public class ImportArchivedXMLs {
    public static void main (String args[]) {
        ImportXMLs importXMLs = new ImportXMLs(args[0]);
        importXMLs.startImport();
    }
}

class ImportXMLs {
    // Property file name with path.
    private String propertyFileNameAndPath = "";

    public ImportXMLs (String propertyFileNameAndPath) {
        this.propertyFileNameAndPath = propertyFileNameAndPath;
    }

    public void startImport() {
	try {
            if ( propertyFileNameAndPath == null || propertyFileNameAndPath.equals("") ) {
                propertyFileNameAndPath = "D:\\ImportArchivedXMLs.properties";
            }

            File propFile = new File(propertyFileNameAndPath);
            Properties properties = new Properties();
            try(FileInputStream fileInput = new FileInputStream(propFile)){
            
            properties.load(fileInput);
            }

            /* From the Properties file:
             * Get the DataBase information; host, username and password.
             *  host - shoudl have the format - jdbc:oracle:thin:@162.70.116.15:1525:DEVLOCAL
             * Get the client bank like ANZ or BMO.
             * Get the source folder path where the xml files exist. This path should be the path where the year/month folder exists
             * so that the client bank info and the year and month can be appended for the ArchiveDir path from the config_setting table and the file can be saved
             * ex: If the XMLs are in E:\CB1\2015_01 folder.
             * The path to be provided in the properties file is E:\CB1
             * The program will read the files like 2015_01\xml file name
             * If the ArchiveDIR path in the config_setting table is C:\OTL\Archive
             * If the client bank provided in the properties file is ANZ
             * The zip files will be stored in C:\OTL\Archive\ANZ\2015_01\zip file name
             * Get the log file path.
            */
            String sourceFolderPath = properties.getProperty("SourceFolderPath");
            String dbHost = properties.getProperty("dbHost");
            String userName = properties.getProperty("UserName");
            String password = properties.getProperty("Password");
            String clientBank = properties.getProperty("ClientBank");

            //Create Log file
            DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd HHmmss");
            Date date = new Date();
            String logFileName = "ImportArchivedXMLs - " + dateFormat.format(date) + ".log";
            File file = new File(logFileName);
            try(FileOutputStream fos = new FileOutputStream(file);
            PrintWriter logFile = new PrintWriter(fos)){

            outPut("Properties File: " + propertyFileNameAndPath, logFile);
            outPut("XML Folder Path: " + sourceFolderPath, logFile);
            outPut("Database Host: " + dbHost, logFile);
            outPut("User Name: " + userName, logFile);
            outPut("Client Bank: " + clientBank, logFile);
            //System.out.println("Program will wait for a minute before proceeding further. If any of the above parameters is not correct, then terminate the program.");
            //TimeUnit.SECONDS.sleep(5);
            outPut("If all the above parameters are correct, press Y/y to continue, press any other character to terminate the program.", logFile);
            char yesOrNo = (char)System.in.read();
            if ( yesOrNo != 'Y' && yesOrNo != 'y' ) {
                return;
            }

            outPut("Started processing further ...... ", logFile);

            //Get the DB connection
            DriverManager.registerDriver(new OracleDriver());
            try(Connection conn = DriverManager.getConnection(dbHost, userName, password)){
            if ( conn == null ) {
                outPut("Error connecting to Database.", logFile);
                return;
            }

            //Get the Extended ArchiveDir path from the DB
           String archiveDirPath = "";
           try( PreparedStatement sqlStatement = conn.prepareStatement("select SETTING_VALUE from config_setting"
           		+ " where setting_name = 'EXTENDED_ARCHIVEDIR'"); ResultSet rs = sqlStatement.executeQuery()){
            
            while (rs.next()) {
                archiveDirPath = rs.getString(1);
            }
           }
            if ( archiveDirPath == null || archiveDirPath.equals("") ) {
                outPut("Extended Archive directory path not found in DB", logFile);
                return;
            }
            //StringTokenizer  stringTokenizer  = new StringTokenizer(archiveDirPath,"\\");

            //Create a file filter for XML files
            FilenameFilter xmlFileFilter = new FilenameFilter () {
                public boolean accept(File dir, String name) {
                    String lowercaseName = name.toLowerCase();
                    if (lowercaseName.endsWith(".xml")) {
                        return true;
                    } else {
                        return false;
                    }
                }
            };

            //Get the list of all the subfolders that is in the provided path. This will be the year_month folder
            File sourceFolderDir = new File(sourceFolderPath);
            File[] yearMonthDirectories = sourceFolderDir.listFiles();

             //For each year_month folder
            for(File yearMonthDir:yearMonthDirectories) {
                if (yearMonthDir.isDirectory()) {
                    String directory = yearMonthDir.getName();
                    outPut("Processing yearMonth Directory: " + directory, logFile);
                    //Get all the XML files and process each of them
                    File[] xmlFiles = yearMonthDir.listFiles(xmlFileFilter);
                    for (File xmlFile:xmlFiles) {
                        String xmlFileName = xmlFile.getName();
                        outPut("Processing XML File: " + xmlFileName, logFile);
                        String zipFilePath = archiveDirPath + "\\" + clientBank + "\\" + directory;
                        String zipFileNameAndPath = zipFilePath + "\\" +
                            xmlFileName.substring(0, xmlFileName.length() - 4) + ".zip";
                        outPut("Destination Path: " + zipFilePath, logFile);
                        outPut("Processing Zip file: " + zipFileNameAndPath, logFile);
                        //Create the zip file
                        if (zipXMLFileAndValidate( xmlFile, zipFilePath, zipFileNameAndPath, logFile, conn, clientBank)) {
                            //Determine if it is a WORKITEM or INSTRUMENT and update the DB tables accordingly.
                            if (xmlFileName.startsWith("WI_")) {
                                //This is a WORKITEM
                                String workItemNumber = xmlFileName.substring( 3, xmlFileName.indexOf("_", 3) );
                               try(PreparedStatement updateStatement = conn.prepareStatement("update archive_work_item set xml_file = ? where trim(work_item_number) = ?")){
                                updateStatement.setString(1, zipFileNameAndPath);
                                updateStatement.setString(2, workItemNumber);
                                outPut("Updating Work Item Number: " + workItemNumber, logFile);
                                int i = updateStatement.executeUpdate();
                               
                                if ( i < 1 ) {
                                    //Update to DB failed, so delete the created zip file and move on to the next file.
                                    File zipFile = new File(zipFileNameAndPath);
                                    zipFile.delete();
                                    outPut("Error updating archive_work_item DB table for xlm file " + xmlFile + " so did not copy and compress the xml file.", logFile);
                                } else {
                                    conn.commit();
                                }
                               }
                            } else {
                                //This is an INSTRUMENT
                                String instrument = "";
                                if (xmlFileName.indexOf("_") > 0) {
                                    instrument = xmlFileName.substring( 0, xmlFileName.indexOf("_") );
                                } else {
                                    instrument = xmlFileName;
                                }
                                try(PreparedStatement updateStatement = conn.prepareStatement("update archive_instrument set xml_file = ? where trim(instrument_id) = ? OR trim(parent_instrument_id) = ?")){
                                updateStatement.setString(1, zipFileNameAndPath);
                                updateStatement.setString(2, instrument);
                                updateStatement.setString(3, instrument);
                                outPut("Updating Instrument Number: " + instrument, logFile);
                                int i = updateStatement.executeUpdate();
                                if ( i < 1 ) {
                                    //Update to DB failed, so delete the created zip file and move on to the next file.
                                    File zipFile = new File(zipFileNameAndPath);
                                    zipFile.delete();
                                    outPut("Error updating archive_instrument DB table for xlm file " + xmlFile + " so did not copy and compress the xml file.", logFile);
                                } else {
                                    conn.commit();
                                }
                                }
                            }

                        }
                    }
                }
            }
            }}
	} catch (Exception e) {
            e.printStackTrace();
	}
    } // End of startImport Function

    /* Function to zip the xml file */
    protected boolean createZipFile(File sourceFile, String zipFilePath, String zipFileNameAndPath, PrintWriter logFile) {
        try {
            File directory = new File(zipFilePath);
            if (!directory.exists()) {
                directory.mkdirs();
            }

            try(FileOutputStream fos = new FileOutputStream(zipFileNameAndPath);
            ZipOutputStream zos = new ZipOutputStream(fos);
            FileInputStream fis = new FileInputStream(sourceFile)){
            byte[] buffer = new byte[ (int) fis.getChannel().size()];

            //Begin writing a new ZIP entry
            zos.putNextEntry(new ZipEntry(sourceFile.getName()));
            int length;
            while ((length = fis.read(buffer)) > 0) {
                zos.write(buffer, 0, length);
            }
            }

        } catch (Exception e) {
            e.printStackTrace();
            outPut("Error creating the zip file " + zipFileNameAndPath, logFile);
            return false;
        }
        return true;
    } // End of createZipFile function

    /* Function to write to console and log file */
    protected void outPut(String msg, PrintWriter logFile) {
        System.out.println(msg);
        logFile.println(msg);
        logFile.flush();
    } // End of outPut function

    /* Function to Generate a MD5 hash of the XML document. */
    protected byte[] getMD5Hash(String fileNameAndPath) {
        byte[] buffer; // = new byte[1024]; //What should be the size n*1024?
        byte[] MD5Hash; // = new byte[1024]; //What should be the size n*1024?
        byte[] empty = new byte[0];
        try(FileInputStream fis = new FileInputStream(fileNameAndPath)) {
            
            MessageDigest MD5Instance = MessageDigest.getInstance("MD5");
            buffer = new byte[ (int) fis.getChannel().size() ];
            MD5Hash = new byte[ (int) fis.getChannel().size() ];
            int length;
            while ((length = fis.read(buffer)) > 0) {
                MD5Instance.update(buffer, 0, length);
            }
            
            MD5Hash = MD5Instance.digest();
        } catch (Exception e) {
            e.printStackTrace();
            return empty;
        }
        return MD5Hash;
    } // End of getMD5Hash Function

    /* Function to Unzip the compressed XML document into a temporary file */
    protected String unZipFile(String sourceFileNameAndPath) {
        String tempFileNameAndPath = "";
        tempFileNameAndPath = sourceFileNameAndPath.substring(0, sourceFileNameAndPath.length() - 4);
        tempFileNameAndPath = tempFileNameAndPath.concat("_Temp.xml");
        try {
            
             File tempFile = new File(tempFileNameAndPath);
            try(FileOutputStream fos = new FileOutputStream(tempFile);
            		ZipInputStream zis = new ZipInputStream(new FileInputStream(sourceFileNameAndPath))){
            	ZipEntry ze = zis.getNextEntry();
            	File sourceFile = new File(sourceFileNameAndPath);
            	byte[] buffer = new byte[ (int) sourceFile.length() ];
            	int length;
            	while ((length = zis.read(buffer)) > 0) {
            		fos.write(buffer, 0, length);
            	}
            
            }

        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
        return tempFileNameAndPath;
    } // End of unZipFile Function

    /* Function to compress/zip the xml file, validate it and store it on file system  */
    private boolean zipXMLFileAndValidate(File sourceFile, String zipFilePath, String zipFileNameAndPath, PrintWriter logFile, Connection conn, String clientBank) {
        // Determine the DOWNLOAD_ARCHIVED_XML setting value.
        ResultSet rs = null;
        String downloadArchivedXML = "";
        String settingName = "DOWNLOAD_ARCHIVED_XML_" + clientBank;
        try(PreparedStatement preparedStatement = conn.prepareStatement("SELECT SETTING_VALUE FROM CONFIG_SETTING WHERE SETTING_NAME = ? ")) {
            
            preparedStatement.setString(1, settingName);
            rs = preparedStatement.executeQuery();
            while(rs.next()) {
                downloadArchivedXML = rs.getString(1);
            }
        } catch (SQLException ex) {
        	ex.printStackTrace();
        } 

        // If DOWNLOAD_ARCHIVED_XML is Yes, then create and store the zip file
        boolean createdZipFile = false;
        String xmlFileNameAndPath = sourceFile.getPath();

        if (downloadArchivedXML.equals("Y")) {
            int i = 0;
            boolean stopLooping = false;

            while (! stopLooping) {
                i++;
                byte[] originalFileMD5Hash = new byte[(int) sourceFile.length()];
                byte[] tempFileMD5Hash = new byte[(int) sourceFile.length()];

                /* Generate a MD5 hash of the XML document. */
                originalFileMD5Hash = getMD5Hash(xmlFileNameAndPath);

                /* Compress the XML document in zip format. */
                String tempFileNameAndPath = "";
                if (createZipFile(sourceFile, zipFilePath, zipFileNameAndPath, logFile)) {

                    /* Unzip the compressed XML document into a temporary file. */
                    tempFileNameAndPath = unZipFile(zipFileNameAndPath);
                    if (tempFileNameAndPath != null && !tempFileNameAndPath.equals("")) {

                        /* Generate a MD5 hash of the temporary file. */
                        tempFileMD5Hash = getMD5Hash(tempFileNameAndPath);

                        if (Arrays.equals(tempFileMD5Hash, originalFileMD5Hash)) {
                            //This means validation of the compressed file was successful.
                            //So delete the Temp XML File.
                            File tempXMLFile = new File(tempFileNameAndPath);
                            tempXMLFile.delete();
                            createdZipFile = true;
                            stopLooping = true;
                        } else {
                            //This means validation of the compressed file was NOT successful.
                            //So delete the created Zip file and the Temp XML File.
                            File zipFile = new File(zipFileNameAndPath);
                            zipFile.delete();
                            File tempXMLFile = new File(tempFileNameAndPath);
                            tempXMLFile.delete();
                        }

                    } else {
                        outPut("Error Uncompressing the zip file for validation. " + zipFileNameAndPath, logFile);
                        //Delete the created zip file and stop looping.
                        File zipFile = new File(zipFileNameAndPath);
                        zipFile.delete();
                        stopLooping = true;
                    }

                } else {
                    outPut("Error compressing the xml file to Zip format. " + xmlFileNameAndPath, logFile);
                    stopLooping = true;
                }

                if (i == 3) {
                    outPut("Error compressing and validating the xml file to Zip format. Reached Max Attempts. " + xmlFileNameAndPath, logFile);
                    //Reached max attempts so stop looping
                    stopLooping = true;
                }

            }
        }
        return createdZipFile;
    } // End of zipXMLFileAndValidate function
}

