package com.ams.tradeportal.devtool;

import java.io.*;



/**
 *
 *     Copyright  � 2008                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class MergeDDs
{
	private static String EJB_JAR = "ejb-jar";
	private static String EJB_JAR_ENT_BEANS = "enterprise-beans";
	private static String EJB_JAR_ENT_BEANS_SESSION = "session";
	private static String EJB_JAR_ASSEMBLY_DESCR = "assembly-descriptor";
	private static String EJB_JAR_ASSEMBLY_DESCR_CONT_TRANS = "container-transaction";

	private static String WEBLOGIC_EJB_JAR = "weblogic-ejb-jar";
	private static String WEBLOGIC_EJB_JAR_ENTPR_BEAN = "weblogic-enterprise-bean";
	private static String WEBLOGIC_EJB_TRANS_ISOLT = "transaction-isolation";


	/**
	 *  Method that is called when class is run from the command line.  Drives the merging 
	 *  process.  See class description above for information on the command line arguments
	 *  that should be passed in.
	 *
	 *  @param args java.lang.String - array of command line arguments
	 * @throws Exception 
	 */
	public static void main(String args[]) throws Exception
	{
		String filePath = "build\\merge";

		String xmlFilePathAndName = null;
		String xmlRootElement  = null;
		if(args.length == 2)
		{
			xmlFilePathAndName = args[0];
			xmlRootElement = args[1];	
		}
		File mergeFile=new File(filePath+"\\"+xmlRootElement+".xml");
		//System.out.println("MergeFile Path:"+mergeFile.getAbsolutePath());
		//If the file does not exist,let it be created and schema info be added to that.
		if(!mergeFile.exists())
		{
			//Create a big file for merge
			FileWriter writeToFile = new FileWriter(mergeFile);
			//Fetch the data of small file and put all that into big one
			String smallFileData = getCompleteDataOfToBeMergedFile(xmlFilePathAndName);   

			FileOutputStream file = new FileOutputStream(mergeFile);		
			PrintWriter writer = new PrintWriter(writeToFile);
			writer.print(smallFileData);
			writer.flush();
			writer.close();
			file.close();
		}
		else if(WEBLOGIC_EJB_JAR.equals(xmlRootElement))
		{
			try(FileReader fr = new FileReader(mergeFile)){

			// Read in characters from merge file
			char[] c = new char[(int)mergeFile.length()];
			fr.read(c,0,(int)mergeFile.length());
			String fileData = new String(c);
			String dataForMerge = getDataFromXMLToBeMergedIntoCombinedXML(xmlFilePathAndName,WEBLOGIC_EJB_JAR_ENTPR_BEAN);
			//System.out.println("dataForMerge=session:"+dataForMerge);
			//Add <session>.. before </enterprise-beans> 
			int lastSessionEleEndIndex   = fileData.lastIndexOf("</"+WEBLOGIC_EJB_JAR_ENTPR_BEAN+">")+(("</"+WEBLOGIC_EJB_JAR_ENTPR_BEAN+">").length());

			StringBuffer modData=new StringBuffer(fileData);
			modData.insert(lastSessionEleEndIndex, dataForMerge);
			//Get the <container-transaction> and add it to the Merge file data
			dataForMerge=getDataFromXMLToBeMergedIntoCombinedXML(xmlFilePathAndName,WEBLOGIC_EJB_TRANS_ISOLT);
			if(dataForMerge != null)
			{
				int lastContTransEleEndIndex   = modData.toString().lastIndexOf("</"+WEBLOGIC_EJB_TRANS_ISOLT+">")+(("</"+WEBLOGIC_EJB_TRANS_ISOLT+">").length());   
				modData.insert(lastContTransEleEndIndex,dataForMerge);
				//System.out.println("Final dataForMerge:"+modData);
			}
			// Write the merged file
			FileOutputStream file = new FileOutputStream(mergeFile);	
			PrintWriter writer = new PrintWriter(file);
			writer.print(modData.toString());
			writer.flush();
			writer.close();
			file.close();
			}
		}
		else if(EJB_JAR.equals(xmlRootElement))
		{
			try(FileReader fr = new FileReader(mergeFile)){

			// Read in characters from merge file
			char[] c = new char[(int)mergeFile.length()];
			fr.read(c,0,(int)mergeFile.length());
			String fileData = new String(c);
			String dataForMerge = getDataFromXMLToBeMergedIntoCombinedXML(xmlFilePathAndName,EJB_JAR_ENT_BEANS_SESSION);

			//Add <session>.. before </enterprise-beans> 
			int lastSessionEleIndex   = fileData.lastIndexOf("</"+EJB_JAR_ENT_BEANS+">");

			StringBuffer modData = new StringBuffer(fileData);
			modData.insert(lastSessionEleIndex, dataForMerge);
			//Get the <container-transaction> and add it to the Merge file data
			dataForMerge = getDataFromXMLToBeMergedIntoCombinedXML(xmlFilePathAndName,EJB_JAR_ASSEMBLY_DESCR_CONT_TRANS);
			if(dataForMerge != null)
			{
				int lastContTransEleIndex   = modData.toString().lastIndexOf("</"+EJB_JAR_ASSEMBLY_DESCR+">");
				if(lastContTransEleIndex!=-1)//Some XML files does not have the  <assembly-descriptor> element
					modData.insert(lastContTransEleIndex,dataForMerge);
				//System.out.println("Final dataForMerge:"+modData);
			}
			// Write the merged file
			try(FileOutputStream file = new FileOutputStream(mergeFile)){	
			PrintWriter writer = new PrintWriter(file);
			writer.print(modData.toString());
			writer.flush();
			}
			}
		}

	}


	private static String getDataFromXMLToBeMergedIntoCombinedXML(String filePathAndName,String xmlElement)
	{
		String fileData = null;
		fileData = getCompleteDataOfToBeMergedFile(filePathAndName);
		int beginIndex = fileData.indexOf("<"+xmlElement+">");
		int endIndex   = fileData.lastIndexOf("</"+xmlElement+">");//PPX-049
		int xmlElementLen=("</"+xmlElement+">").length();
		if(beginIndex == -1)
			fileData = null;
		else 
			fileData = fileData.substring(beginIndex, endIndex+xmlElementLen);
		return fileData;
	}
	private static String getCompleteDataOfToBeMergedFile(String filePathAndName)
	{
		//System.out.println("Path being passed:"+filePathAndName);
		File fileToBeMerged=new File(filePathAndName);  

		// Read in characters from file
		char[] c = null;
		try(FileReader fr = new FileReader(fileToBeMerged)) {
			
			c = new char[(int)fileToBeMerged.length()];
			fr.read(c,0,(int)fileToBeMerged.length());
		} catch (FileNotFoundException e) {
			System.out.println("File name "+fileToBeMerged.getAbsolutePath()+"could not be found.");
			System.exit(0);
		} catch (IOException e) {
			System.out.println("Data could not be read from the file named:"+filePathAndName);
			System.exit(1);
		}
		//Fetch the data from the file to be merged.The data to be fetched is the content after the schema description
		//and before the end of the root element.
		String fileData = new String(c);
		return fileData;
	}
}