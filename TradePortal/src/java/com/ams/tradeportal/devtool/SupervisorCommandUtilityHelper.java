package com.ams.tradeportal.devtool;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Vector;
import java.util.StringTokenizer;
import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;

import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.DateTimeUtility;
import com.ams.tradeportal.busobj.util.SupervisorCommandUtility;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.ams.tradeportal.busobj.SupervisorCommand;
import com.amsinc.ecsg.util.JPylonProperties;
import com.amsinc.ecsg.frame.EJBObjectFactory;

/**
 * Nar IR-T36000032592 Rel9.2 10/13/2014
 * SupervisorCommandUtility reads data from supervisor_command table
 * and build text file to add users and groups in Supervisor.
 * Supervisor is invoked in command line mode with generated text file 
 * as input.
 * 
 */

public class SupervisorCommandUtilityHelper {

	public static void main( String argc[])
    { 
        
        DocumentHandler     hierarchyDoc                = null;
        DocumentHandler     commandDoc                  = null;
        StringBuffer        sqlQuery                    = new StringBuffer();
        String              command                     = null;
        int                 totalCommands               = 0;
        long                lSupervisorCmdOid           = 0;
        Long                superOid                    = null;
        String              FileName1                   = null;
        String              FileName2                   = null;
		String 				oldFile						= null;
        String              Uname                       = null;
        String              Upass                       = null;
        String              KeyFile                     = null;
        String              SupervisorPath              = null;
        Vector              supervisorOidVector         = new Vector();
        String              dateToken                   = null;
        String              curDate                     = "";
              

        //Retrieve parameter information from file
        Hashtable parameters = SupervisorCommandUtility.getUnivParameters();
        
        FileName1 = (String)parameters.get("FileName");
		oldFile = FileName1;
        Uname = (String)parameters.get("Uname");
        Upass = (String)parameters.get("Upass");
        KeyFile = (String)parameters.get("KeyFile");
        SupervisorPath= (String)parameters.get("SupervisorPath");
        
        
        try
        {
            //rbhaduri - 28 Aug 09 - BO XI Changes - CR-451 - used different date function since the GMT one does not work
	    //curDate = DateTimeUtility.getGMTDateTime();
	    curDate = DateTimeUtility.getCurrentDateTime();
        }
        catch( Exception e)
        {
            System.out.println("Exception in SupervisorCommandUtility  " + e.getMessage());   
            
        }
        
        StringTokenizer st = new StringTokenizer(curDate,"/: ");
		        
        while (st.hasMoreTokens()) {
            dateToken = dateToken+st.nextToken();
        }
 
        		
		File f = new File(FileName1);
    
		//Check if file already exists
		if (f.exists())
		{
			try
			{
				// File (or directory) to be moved
				File newFile = new File(oldFile.concat(dateToken));
									
				// Move file to new directory
				f.renameTo(new File(newFile.getName()));
			}
			catch( Exception e)
			{
				System.out.println("Exception in SupervisorCommandUtility  while checking for exisitng file " + e.getMessage());   
            
			}
			

		}
             
        
	    try 
	    {
	        //Standard Reports
            sqlQuery.append("select command, supervisor_cmd_oid");
            sqlQuery.append(" from supervisor_command");
            sqlQuery.append(" order by creation_timestamp");
            
            hierarchyDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery.toString(), false, new ArrayList<Object>());
			
			if (hierarchyDoc != null)
			{
			    Vector commandListDoc = hierarchyDoc.getFragments("/ResultSetRecord");
    			
			    totalCommands = commandListDoc.size();

                // Write the merged file
		       try(FileOutputStream file = new FileOutputStream(FileName1);
	            PrintWriter writer = new PrintWriter(file)){
    		    
    		   //Loop through all the rows in SUPERVISOR_COMMAND
                for (int i = 0; i < totalCommands; i++)
                {
                   
                    commandDoc = (DocumentHandler) commandListDoc.get(i);
                    command = commandDoc.getAttribute("COMMAND");
                    lSupervisorCmdOid = commandDoc.getAttributeLong("../SUPERVISOR_CMD_OID");
                    superOid = new Long(lSupervisorCmdOid);
                    
                    supervisorOidVector.addElement(superOid);
    	            writer.println(command);
		            writer.flush();
    		        
                }
                       
		      }
               
       	   }	
       	    
		} catch (Exception e) 
		{
            System.out.println(e.getMessage());
        } 
		
		//Delete all the processed records in SUPERVISOR_COMMAND if there are no exceptions
        long       delSuperOid = 0;
		
		for (int j = 0; j < supervisorOidVector.size(); j++)
        {
                    
            Long        sID = (Long)supervisorOidVector.elementAt(j);
                    
            delSuperOid = sID.longValue();
            SupervisorCommand       supervisorCommand = null;	
               	        			
        			
			try
			{
			    String serverLocation = JPylonProperties.getInstance().getString("serverLocation");

                supervisorCommand = (SupervisorCommand) EJBObjectFactory.createClientEJB(serverLocation, "SupervisorCommand",delSuperOid);
                supervisorCommand.delete();
                                        
                //Delete processes rows from SUPERVISOR_COMMAND
                        
                        
                } catch (Exception e) {
		                System.out.println("error in SupervisorCommandUtility is " + e.toString());
		                e.printStackTrace();
        	    } finally {
		            try {
                            if (supervisorCommand != null) {
			                    supervisorCommand.remove();
			                    }
		                } catch (Exception e) {
			                    System.out.println("error removing supervisorCommand in SupervisorCommandUtility");
						}
				}
                     
                       
        }
        
              
    } 
}
