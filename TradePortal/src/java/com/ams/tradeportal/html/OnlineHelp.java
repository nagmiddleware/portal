package com.ams.tradeportal.html;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import com.ams.tradeportal.common.*;
import com.ams.tradeportal.busobj.webbean.SessionWebBean;
import java.net.URLEncoder;

/**
 * This utility class is used to create links on the page to the online help system
 *
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class OnlineHelp
 {
private static final Logger LOG = LoggerFactory.getLogger(OnlineHelp.class);
	/**
 	 * Creates the URL for a link to a help page.  The link calls a Javascript function 
	 * that pops up a window with the help page that was passed into this method.
	 *
	 * @param url - the URL of the help page (not including the runtime/presentation/help/{language} part
	 * @param resMgr - an instance of the resource manager (used to get locale information)
	 * @return the URL for the help page (including Javascript syntax)
	 */
	public static String getURLForHelp(String helpPage, ResourceManager resMgr)
       {
		StringBuffer buffer = new StringBuffer();
		buffer.append("javascript:openHelp('");
		buffer.append(StringFunction.escapeQuotesforJS(getHelpPath(helpPage, resMgr)));
		buffer.append("')");
		
		return buffer.toString();
	 }

	/**
 	 * Creates the full internationalized path to a specific help page.  
	 *
	 * @param helpPage - the help page desired 
	 * @param resMgr - an instance of the resource manager (used to get locale information)
	 * @return the URL for the help page 
	 */
	public static String getHelpPath(String helpPage, ResourceManager resMgr)
       {
		StringBuffer url = new StringBuffer();
		url.append("/portal/help/");
		// Get the name of the directory to look in for help - can be set by locale
		url.append(resMgr.getText("OnlineHelp.directoryName", TradePortalConstants.TEXT_BUNDLE) );
		if(!helpPage.startsWith("/"))
			url.append("/");

		url.append(helpPage);
		
		return url.toString();
	 }

	/**
 	 * Creates the full internationalized path to a specific help page with the branding directory added. 
	 *
	 * @param helpPage - the help page desired 
	 * @param resMgr - an instance of the resource manager (used to get locale information)
	 * @param brandingDirectory - name of the branding directory
	 * @return the URL for the help page 
	 */
	public static String getHelpPath(String helpPage, String brandingDirectory, ResourceManager resMgr)
       {
		StringBuffer url = new StringBuffer();
		url.append("/portal/help/");
		// Get the name of the directory to look in for help - can be set by locale
		url.append(resMgr.getText("OnlineHelp.directoryName", TradePortalConstants.TEXT_BUNDLE) );
		url.append("/");
		url.append(brandingDirectory);
		if(!helpPage.startsWith("/"))
			url.append("/");
		url.append(helpPage);
		
		return url.toString();
	 }

	/**
 	 * Creates the link to the context sensitive help page.  The link calls a Javascript function 
	 * that pops up a window with the help page that was passed into this method.
	 *
	 * @param url - the URL of the help page (not including the runtime/presentation/help/{language} part
	 * @param resMgr - an instance of the resource manager (used to get locale information)
	 * @return the HTML for the link of the help page (including Javascript syntax)
	 */
	public static String createContextSensitiveLink(String url, ResourceManager resMgr)
       {
		StringBuffer buffer = new StringBuffer();
		buffer.append("<a href=\"");
		buffer.append(getURLForHelp(url, resMgr));
		//ctq portal refresh - move image to stylesheet
		//buffer.append("\"><img src=\"/portal/images/QUESTION_15pixel.gif\" width=\"15\" height=\"15\" border=\"0\" alt=\"");
        //buffer.append(resMgr.getText("common.ContextHelp", TradePortalConstants.TEXT_BUNDLE));
        //buffer.append("\"></a>");
        buffer.append("\"></a>");
		return buffer.toString();
	 }

	/**
 	 * Creates the link to the context sensitive help page.  The link calls a Javascript function 
	 * that pops up a window with the help page that was passed into this method.
	 *
	 * @param url - the URL of the help page (not including the runtime/presentation/help/{language} part
	 * @param anchor - the anchor to go to within the page
	 * @param resMgr - an instance of the resource manager (used to get locale information)
	 * @return the HTML for the link of the help page (including Javascript syntax)
	 */
	public static String createContextSensitiveLink(String url, String anchor, ResourceManager resMgr)
       {
		return createContextSensitiveLink(url+"#"+anchor, resMgr);
	 }

	/**
 	 * Creates the link to the context sensitive help page.  The link calls a Javascript function 
	 * that pops up a window with the help page that was passed into this method.
	 *
	 * @param url - the URL of the help page (not including the runtime/presentation/help/{language} part
	 * @param resMgr - an instance of the resource manager (used to get locale information)
	 * @param brandingDirectory - branding directory
	 * @return the HTML for the link of the help page (including Javascript syntax)
	 */
	public static String createContextSensitiveLink(String url, ResourceManager resMgr, String brandingDirectory)
       {
		StringBuffer buffer = new StringBuffer();
		buffer.append("<a href=\"");
		buffer.append(getURLForHelp(url, resMgr));
        //ctq portal refresh - move image to stylesheet
        //buffer.append("\"><img src=\"/portal/images/");
        //buffer.append(brandingDirectory);
        //buffer.append("/QUESTION_15pixel.gif\" width=\"15\" height=\"15\" border=\"0\" alt=\"");
        //buffer.append(resMgr.getText("common.ContextHelp", TradePortalConstants.TEXT_BUNDLE));
        //buffer.append("\"></a>");
        buffer.append("\"></a>");
		return buffer.toString();
	 }

	/**
 	 * Creates the link to the context sensitive help page.  The link calls a Javascript function 
	 * that pops up a window with the help page that was passed into this method.
	 *
	 * @param url - the URL of the help page (not including the runtime/presentation/help/{language} part
	 * @param resMgr - an instance of the resource manager (used to get locale information)
	 * @param userSession - an instance of the SesssionWebBean (used to get branding directory information)
	 * @return the HTML for the link of the help page (including Javascript syntax)
	 */
	public static String createContextSensitiveLink(String url, ResourceManager resMgr, SessionWebBean userSession)
       {
		return createContextSensitiveLink(url, resMgr, userSession.getBrandingDirectory());
	 }

	/**
 	 * Creates the link to the context sensitive help page.  The link calls a Javascript function 
	 * that pops up a window with the help page that was passed into this method.
	 *
	 * @param url - the URL of the help page (not including the runtime/presentation/help/{language} part
	 * @param anchor - the anchor to go to within the page
	 * @param resMgr - an instance of the resource manager (used to get locale information)
	 * @param userSession - an instance of the SesssionWebBean (used to get branding directory information)
	 * @return the HTML for the link of the help page (including Javascript syntax)
	 */
	public static String createContextSensitiveLink(String url, String anchor, ResourceManager resMgr, SessionWebBean userSession)
       {
		return createContextSensitiveLink(url+"#"+anchor, resMgr, userSession);
	 }


 }