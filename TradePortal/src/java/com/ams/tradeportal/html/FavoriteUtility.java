package com.ams.tradeportal.html;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */

import java.util.HashMap;
import java.util.Map;

import com.ams.tradeportal.common.TradePortalConstants;

public class FavoriteUtility {
private static final Logger LOG = LoggerFactory.getLogger(FavoriteUtility.class);

    //keep a mapping between favorite id and menu display key
    private static Map<String, String> taskMenuKeyMap = null;

    //keep a mapping between favorite id and the text key
    //we don't map to the actual display text for i18n
    private static Map<String, String> taskDisplayKeyMap = null;
    
    /**
     * Constructor
     */
    private FavoriteUtility() {        
    }

    public static String getTaskMenuKey(String taskId) {
        
        if ( taskMenuKeyMap == null ) {
            initializeTaskMenuKeyMap();
        }
        
        String displayKey = taskMenuKeyMap.get(taskId);
        
        return displayKey;        
    }

    private static void initializeTaskMenuKeyMap() {
        Map<String, String> map = new HashMap<String, String>();
        
        map.put( TradePortalConstants.FAV_TASK__COPY_TRADE             ,
                "NewInstrumentsMenu.Trade.CopyFromExisting");
        map.put( TradePortalConstants.FAV_TASK__TRANSFER_EXPORT_LC     ,
                "NewInstrumentsMenu.Trade.TransferExportLC");
        map.put( TradePortalConstants.FAV_TASK__CREATE_AIRWAY_BILL     ,
                "NewInstrumentsMenu.Trade.AirWaybill");
        map.put( TradePortalConstants.FAV_TASK__CREATE_APPROVAL_TO_PAY ,
                "NewInstrumentsMenu.Trade.ApprovalToPay");
        map.put( TradePortalConstants.FAV_TASK__CREATE_DIRECT_SEND_COLL ,
                "NewInstrumentsMenu.Trade.DirectSendCollection");
        //cquinton 2/25/2013 add create export collection
        map.put( TradePortalConstants.FAV_TASK__CREATE_EXPORT_COLL ,
                "NewInstrumentsMenu.Trade.ExportCollection");
        map.put( TradePortalConstants.FAV_TASK__CREATE_IMPORT_LC       ,
                "NewInstrumentsMenu.Trade.ImportLC");
        map.put( TradePortalConstants.FAV_TASK__CREATE_LOAN_REQUEST    ,
                "NewInstrumentsMenu.Trade.LoanRequest");
        map.put( TradePortalConstants.FAV_TASK__CREATE_OUTGOING_GUAR   ,
                "NewInstrumentsMenu.Trade.OutgoingGuarantee");
        map.put( TradePortalConstants.FAV_TASK__CREATE_OUT_SLC_DETAILED  ,
                "NewInstrumentsMenu.Trade.OutgoingStandbyLCDetailed");
        map.put( TradePortalConstants.FAV_TASK__CREATE_OUT_SLC_SIMPLE  ,
                "NewInstrumentsMenu.Trade.OutgoingStandbyLCSimple");
        map.put( TradePortalConstants.FAV_TASK__CREATE_REQUEST_TO_ADVISE,
                "NewInstrumentsMenu.Trade.RequestToAdvise");
        map.put( TradePortalConstants.FAV_TASK__CREATE_SHIPPING_GUAR   ,
                "NewInstrumentsMenu.Trade.ShippingGuarantee");
    
        map.put( TradePortalConstants.FAV_TASK__COPY_PAYMENT           ,
                "NewInstrumentsMenu.Payment.CopyFromExisting");
        map.put( TradePortalConstants.FAV_TASK__CREATE_TRNSFR_BTN_ACCT ,
                "NewInstrumentsMenu.Payment.TransferBetweenAccounts");
        map.put( TradePortalConstants.FAV_TASK__CREATE_PMT             ,
                "NewInstrumentsMenu.Payment.Payment");
        map.put( TradePortalConstants.FAV_TASK__CREATE_INT_PMT        ,
                "NewInstrumentsMenu.Payment.InternationalPayment");
    
        map.put( TradePortalConstants.FAV_TASK__COPY_DIRECT_DEBIT      ,
                "NewInstrumentsMenu.DirectDebits.CopyFromExisting");
        map.put( TradePortalConstants.FAV_TASK__CREATE_DIRECT_DEBIT    ,
                "NewInstrumentsMenu.DirectDebits.DirectDebits");
    
        map.put( TradePortalConstants.FAV_TASK__VIEW_TRADE_PEND_TRANS  ,
                "TransactionsMenu.Trade.PendingTransactions");
        map.put( TradePortalConstants.FAV_TASK__VIEW_TRADE_AUTH_TRANS  ,
                "TransactionsMenu.Trade.AuthorizedTransactions");
        map.put( TradePortalConstants.FAV_TASK__VIEW_TRADE_HISTORY     ,
                "TransactionsMenu.Trade.Inquiries");
        map.put( TradePortalConstants.FAV_TASK__TRADE_CREATE_AMENDMENT ,
                "TransactionsMenu.Trade.CreateAmendment");
        map.put( TradePortalConstants.FAV_TASK__TRADE_CREATE_TRACER    ,
                "TransactionsMenu.Trade.CreateTracer");
        map.put( TradePortalConstants.FAV_TASK__TRADE_CREATE_ASSIGNMENT,
                "TransactionsMenu.Trade.CreateAssignment");
    
        map.put( TradePortalConstants.FAV_TASK__VIEW_PMT_PEND_TRANS ,
                "TransactionsMenu.Payment.PendingTransactions");
        map.put( TradePortalConstants.FAV_TASK__VIEW_PMT_FUTURE_TRANS  ,
                "TransactionsMenu.Payment.FutureValueTransactions");
        map.put( TradePortalConstants.FAV_TASK__VIEW_PMT_AUTH_TRANS    ,
                "TransactionsMenu.Payment.AuthorizedTransactions");
        map.put( TradePortalConstants.FAV_TASK__VIEW_PMT_HISTORY       ,
                "TransactionsMenu.Payment.Inquiries");
    
        map.put( TradePortalConstants.FAV_TASK__VIEW_DD_PEND_TRANS     ,
                "TransactionsMenu.DirectDebits.PendingTransactions");
        map.put( TradePortalConstants.FAV_TASK__VIEW_DD_HISTORY        ,
                "TransactionsMenu.DirectDebits.Inquiries");
    
        map.put( TradePortalConstants.FAV_TASK__VIEW_RCV_MATCHING      ,
                "TransactionsMenu.Receivables.Matching");
        map.put( TradePortalConstants.FAV_TASK__VIEW_RCV_INVOICES      ,
                "TransactionsMenu.Receivables.Invoices");
        map.put( TradePortalConstants.FAV_TASK__VIEW_RCV_HISTORY       ,
                "TransactionsMenu.Receivables.Inquiries");
    
        map.put( TradePortalConstants.FAV_TASK__VIEW_ACCT_BALANCES     ,
                "NavigationBar.Accounts");
    
        map.put( TradePortalConstants.FAV_TASK__VIEW_PARTIES           ,
                "RefDataMenu.Parties");
        map.put( TradePortalConstants.FAV_TASK__VIEW_PHRASES           ,
                "RefDataMenu.Phrases");
        map.put( TradePortalConstants.FAV_TASK__VIEW_USERS             ,
                "RefDataMenu.Users");
        map.put( TradePortalConstants.FAV_TASK__VIEW_THRESHOLDGROUPS   ,
                "RefDataMenu.ThresholdGroups");
        map.put( TradePortalConstants.FAV_TASK__VIEW_SECURITYPROFILES  ,
                "RefDataMenu.SecurityProfiles");
        map.put( TradePortalConstants.FAV_TASK__VIEW_PODEFINITIONS     ,
                "RefDataMenu.PODefinitions");
        //cquinton 2/25/2013 add invoice definitions
        map.put( TradePortalConstants.FAV_TASK__VIEW_INV_DEFINITIONS     ,
                "RefDataHome.InvoiceDefinitions");
        map.put( TradePortalConstants.FAV_TASK__VIEW_CREATIONRULES     ,
                "RefDataMenu.CreationRules");
        map.put( TradePortalConstants.FAV_TASK__VIEW_REC_MATCH_RULES   ,
                "RefDataMenu.ReceivablesMatchingRules");
        map.put( TradePortalConstants.FAV_TASK__VIEW_WORKGROUPS        ,
                "RefDataMenu.WorkGroups");
        map.put( TradePortalConstants.FAV_TASK__VIEW_PANEL_AUTH_GRPS   ,
                "RefDataMenu.PanelAuthorizationGroups");
        map.put( TradePortalConstants.FAV_TASK__VIEW_TEMPLATE_GRPS     ,
                "RefDataMenu.TemplateGroups");
        map.put( TradePortalConstants.FAV_TASK__VIEW_TEMPLATES         ,
                "RefDataMenu.Templates");
        map.put( TradePortalConstants.FAV_TASK__VIEW_FX_RATES          ,
                "RefDataMenu.ForeignExchangeRates");
    
        map.put( TradePortalConstants.FAV_TASK__UPLOAD_PAYMENT_FILE    ,
                "UploadCenterMenu.PaymentFileUpload");
        map.put( TradePortalConstants.FAV_TASK__UPLOAD_INVOICE_FILE    ,
                "UploadCenterMenu.InvoiceFileUpload");
        map.put( TradePortalConstants.FAV_TASK__UPLOAD_PURCHASE_ORDER  ,
                "UploadCenterMenu.PurchaseOrderUpload");
    
        taskMenuKeyMap = map;
    }

    public static String getTaskDisplayKey(String taskId) {
        
        if ( taskDisplayKeyMap == null ) {
            initializeTaskDisplayKeyMap();
        }
        
        String displayKey = taskDisplayKeyMap.get(taskId);
        
        return displayKey;        
    }
    
    private static void initializeTaskDisplayKeyMap() {
        Map<String, String> map = new HashMap<String, String>();
        
        map.put(
            TradePortalConstants.FAV_TASK__CREATE_MAIL,
            "FavoriteTask.CreateMailMessage");
        map.put(
            TradePortalConstants.FAV_TASK__VIEW_NOTIFS,
            "FavoriteTask.ViewNotification");
        map.put(
            TradePortalConstants.FAV_TASK__COPY_TRADE,
            "FavoriteTask.CopyFromExistingTrade");
        map.put(
            TradePortalConstants.FAV_TASK__TRANSFER_EXPORT_LC,
            "FavoriteTask.TransferExportLC");
        map.put(
            TradePortalConstants.FAV_TASK__CREATE_AIRWAY_BILL,
            "FavoriteTask.CreateAirWaybill");
        map.put(
            TradePortalConstants.FAV_TASK__CREATE_APPROVAL_TO_PAY,
            "FavoriteTask.CreateApprovalToPay");
        map.put(
            TradePortalConstants.FAV_TASK__CREATE_DIRECT_SEND_COLL,
            "FavoriteTask.CreateDirectSendCollection");
        //cquinton 2/25/2013 add create export collection
        map.put(
            TradePortalConstants.FAV_TASK__CREATE_EXPORT_COLL,
            "FavoriteTask.CreateExportCollection");
        map.put(
            TradePortalConstants.FAV_TASK__CREATE_IMPORT_LC,
            "FavoriteTask.CreateImportLC");
        map.put(
            TradePortalConstants.FAV_TASK__CREATE_LOAN_REQUEST,
            "FavoriteTask.CreateLoanRequest");
        map.put(
            TradePortalConstants.FAV_TASK__CREATE_OUTGOING_GUAR,
            "FavoriteTask.CreateOutgoingGuarantee");
        map.put(
            TradePortalConstants.FAV_TASK__CREATE_OUT_SLC_DETAILED,
            "FavoriteTask.CreateOutgoingStandbyLCDetailed");
        map.put(
            TradePortalConstants.FAV_TASK__CREATE_OUT_SLC_SIMPLE,
            "FavoriteTask.CreateOutgoingStandbyLCSimple");
        map.put(
            TradePortalConstants.FAV_TASK__CREATE_REQUEST_TO_ADVISE,
            "FavoriteTask.CreateRequestToAdvise");
        map.put(
            TradePortalConstants.FAV_TASK__CREATE_SHIPPING_GUAR,
            "FavoriteTask.CreateShippingGuarantee");
        map.put(
            TradePortalConstants.FAV_TASK__COPY_PAYMENT,
            "FavoriteTask.CopyFromExistingPayment");
        map.put(
            TradePortalConstants.FAV_TASK__CREATE_TRNSFR_BTN_ACCT,
            "FavoriteTask.CreateTransferBetweenAccounts");  
        map.put(
            TradePortalConstants.FAV_TASK__CREATE_PMT,
            "FavoriteTask.CreatePayment");
        map.put(
            TradePortalConstants.FAV_TASK__CREATE_INT_PMT,
            "FavoriteTask.CreateInternationalPayment");    
        map.put(
            TradePortalConstants.FAV_TASK__COPY_DIRECT_DEBIT,
            "FavoriteTask.CopyFromExistingDirectDebit");  
        map.put(
            TradePortalConstants.FAV_TASK__CREATE_DIRECT_DEBIT,
            "FavoriteTask.CreateDirectDebit");        
        map.put(
            TradePortalConstants.FAV_TASK__VIEW_TRADE_PEND_TRANS,
            "FavoriteTask.ViewPendingTradeTransactions");        
        map.put(
            TradePortalConstants.FAV_TASK__VIEW_TRADE_AUTH_TRANS,
            "FavoriteTask.ViewAuthorizedTradeTransactions");
        map.put(
            TradePortalConstants.FAV_TASK__VIEW_TRADE_HISTORY,
            "FavoriteTask.ViewTradeHistory");        
        map.put(
            TradePortalConstants.FAV_TASK__TRADE_CREATE_AMENDMENT,
            "FavoriteTask.CreateAmendment");        
        map.put(
            TradePortalConstants.FAV_TASK__TRADE_CREATE_TRACER,
            "FavoriteTask.CreateTracer");        
        map.put(
            TradePortalConstants.FAV_TASK__TRADE_CREATE_ASSIGNMENT,
            "FavoriteTask.CreateAssignment");        
        map.put(
            TradePortalConstants.FAV_TASK__VIEW_PMT_PEND_TRANS,
            "FavoriteTask.ViewPendingPaymentTransactions");        
        map.put(
            TradePortalConstants.FAV_TASK__VIEW_PMT_FUTURE_TRANS,
            "FavoriteTask.ViewFutureValuePaymentTransactions");        
        map.put(
            TradePortalConstants.FAV_TASK__VIEW_PMT_AUTH_TRANS,
            "FavoriteTask.ViewAuthorizedPaymentTransactions");        
        map.put(
            TradePortalConstants.FAV_TASK__VIEW_PMT_HISTORY,
            "FavoriteTask.ViewPaymentHistory");        
        map.put(
            TradePortalConstants.FAV_TASK__VIEW_DD_PEND_TRANS,
            "FavoriteTask.ViewPendingDirectDebitTransactions");        
        map.put(
            TradePortalConstants.FAV_TASK__VIEW_DD_HISTORY,
            "FavoriteTask.ViewDirectDebitHistory");        
        map.put(
            TradePortalConstants.FAV_TASK__VIEW_RCV_MATCHING,
            "FavoriteTask.ViewReceivablesMatching");        
        map.put(
            TradePortalConstants.FAV_TASK__VIEW_RCV_INVOICES,
            "FavoriteTask.ViewInvoices");        
        map.put(
            TradePortalConstants.FAV_TASK__VIEW_RCV_HISTORY,
            "FavoriteTask.ViewReceivablesHistory");        
        map.put(
            TradePortalConstants.FAV_TASK__VIEW_ACCT_BALANCES,
            "FavoriteTask.ViewAccountBalances");        
        map.put(
            TradePortalConstants.FAV_TASK__VIEW_PARTIES,
            "FavoriteTask.ViewParties");        
        map.put(
            TradePortalConstants.FAV_TASK__VIEW_PHRASES,
            "FavoriteTask.ViewPhrases");        
        map.put(
            TradePortalConstants.FAV_TASK__VIEW_USERS,
            "FavoriteTask.ViewUsers");        
        map.put(
            TradePortalConstants.FAV_TASK__VIEW_THRESHOLDGROUPS,
            "FavoriteTask.ViewThresholdGroups");        
        map.put(
            TradePortalConstants.FAV_TASK__VIEW_SECURITYPROFILES,
            "FavoriteTask.ViewSecurityProfiles");        
        map.put(
            TradePortalConstants.FAV_TASK__VIEW_PODEFINITIONS,
            "FavoriteTask.ViewPODefinitions");
        //cquinton 2/25/2013 add invoice definitions
        map.put(
            TradePortalConstants.FAV_TASK__VIEW_INV_DEFINITIONS,
            "FavoriteTask.ViewInvDefinitions");        
        map.put(
            TradePortalConstants.FAV_TASK__VIEW_CREATIONRULES,
            "FavoriteTask.ViewCreationRules");        
        map.put(
            TradePortalConstants.FAV_TASK__VIEW_REC_MATCH_RULES,
            "FavoriteTask.ViewReceivablesMatchingRules");        
        map.put(
            TradePortalConstants.FAV_TASK__VIEW_WORKGROUPS,
            "FavoriteTask.ViewWorkGroups");        
        map.put(
            TradePortalConstants.FAV_TASK__VIEW_PANEL_AUTH_GRPS,
            "FavoriteTask.ViewPanelAuthorizationGroups");        
        map.put(
            TradePortalConstants.FAV_TASK__VIEW_TEMPLATE_GRPS,
            "FavoriteTask.ViewTemplateGroups");        
        map.put(
            TradePortalConstants.FAV_TASK__VIEW_TEMPLATES,
            "FavoriteTask.ViewTemplates");        
        map.put(
            TradePortalConstants.FAV_TASK__VIEW_FX_RATES,
             "FavoriteTask.ViewForeignExchangeRates");        
        map.put(
            TradePortalConstants.FAV_TASK__UPLOAD_PAYMENT_FILE,
            "FavoriteTask.PaymentFileUpload");        
        map.put(
            TradePortalConstants.FAV_TASK__UPLOAD_INVOICE_FILE,
            "FavoriteTask.InvoiceFileUpload");        
        map.put(
            TradePortalConstants.FAV_TASK__UPLOAD_PURCHASE_ORDER,
            "FavoriteTask.PurchaseOrderUpload");        
        
        taskDisplayKeyMap = map;
    }
    
    public static boolean shouldDisplayTask(String taskId, ConditionalDisplay condDisplay) {
        boolean shouldDisplay = false;
        if ( TradePortalConstants.FAV_TASK__CREATE_MAIL.equals(taskId) ) {
            shouldDisplay = condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_MAIL_MESSAGES );
        }
        else if ( TradePortalConstants.FAV_TASK__VIEW_NOTIFS.equals(taskId) ) {
            shouldDisplay = condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_MAIL_MESSAGES );
        }
        else if (TradePortalConstants.FAV_TASK__COPY_TRADE.equals(taskId) ) {
            shouldDisplay = condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_NEW_TRADE_INSTRUMENTS ) ;
        }
        else if (TradePortalConstants.FAV_TASK__TRANSFER_EXPORT_LC.equals(taskId) ) {
            shouldDisplay = condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_TRANSFER_EXPORT_COLL ) ;
        }
        else if (TradePortalConstants.FAV_TASK__CREATE_AIRWAY_BILL.equals(taskId) ) {
            shouldDisplay = condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_NEW_AIR_WAYBILL );
        }
        else if (TradePortalConstants.FAV_TASK__CREATE_APPROVAL_TO_PAY.equals(taskId) ) {
            shouldDisplay = condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_NEW_APPROVAL_TO_PAY );
        }
        else if (TradePortalConstants.FAV_TASK__CREATE_DIRECT_SEND_COLL.equals(taskId) ) {
            shouldDisplay = condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_NEW_DIRECT_SEND_COLL );
        }
        else if (TradePortalConstants.FAV_TASK__CREATE_EXPORT_COLL.equals(taskId) ) {
            shouldDisplay = condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_NEW_EXPORT_COLL );
        }
        else if (TradePortalConstants.FAV_TASK__CREATE_IMPORT_LC.equals(taskId) ) {
            shouldDisplay = condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_NEW_DLC );
        }
        else if (TradePortalConstants.FAV_TASK__CREATE_LOAN_REQUEST.equals(taskId) ) {
            shouldDisplay = condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_NEW_LOAN_RQST );
        }
        else if (TradePortalConstants.FAV_TASK__CREATE_OUTGOING_GUAR.equals(taskId) ) {
            shouldDisplay = condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_NEW_GUAR );
        }
        else if (TradePortalConstants.FAV_TASK__CREATE_OUT_SLC_DETAILED.equals(taskId) ) {
            shouldDisplay = condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_NEW_SLC_DET );
        }
        else if (TradePortalConstants.FAV_TASK__CREATE_OUT_SLC_SIMPLE.equals(taskId) ) {
            shouldDisplay = condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_NEW_SLC );
        }
        else if (TradePortalConstants.FAV_TASK__CREATE_REQUEST_TO_ADVISE.equals(taskId) ) {
            shouldDisplay = condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_NEW_REQUEST_ADVISE );
        }
        else if (TradePortalConstants.FAV_TASK__CREATE_SHIPPING_GUAR.equals(taskId) ) {
            shouldDisplay = condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_NEW_SHIP_GUAR );
        }
        else if (TradePortalConstants.FAV_TASK__COPY_PAYMENT.equals(taskId) ) {
            shouldDisplay = condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_NEW_PAYMENT_INSTRUMENTS );
        }
        else if (TradePortalConstants.FAV_TASK__CREATE_TRNSFR_BTN_ACCT.equals(taskId) ) {
            shouldDisplay = condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_NEW_TRANSFER_BTW_ACCTS );
        }
        else if (TradePortalConstants.FAV_TASK__CREATE_PMT.equals(taskId) ) {
            shouldDisplay = condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_NEW_DOMESTIC_PAYMENT );
        }
        else if (TradePortalConstants.FAV_TASK__CREATE_INT_PMT.equals(taskId) ) {
            shouldDisplay = condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_NEW_INT_PAYMENT );
        }
        else if (TradePortalConstants.FAV_TASK__COPY_DIRECT_DEBIT.equals(taskId) ) {
            shouldDisplay = condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_NEW_DIRECT_DEBIT_INSTRUMENTS );
        }
        else if (TradePortalConstants.FAV_TASK__CREATE_DIRECT_DEBIT.equals(taskId) ) {
            shouldDisplay = condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_NEW_DDI );
        }
        else if (TradePortalConstants.FAV_TASK__VIEW_TRADE_PEND_TRANS.equals(taskId) ) {
            shouldDisplay = condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_TRADE_TRANSACTIONS );
        }
        else if (TradePortalConstants.FAV_TASK__VIEW_TRADE_AUTH_TRANS.equals(taskId) ) {
            shouldDisplay = condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_TRADE_TRANSACTIONS );
        }
        else if (TradePortalConstants.FAV_TASK__VIEW_TRADE_HISTORY.equals(taskId) ) {
            shouldDisplay = condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_TRADE_TRANSACTIONS );
        }
        else if (TradePortalConstants.FAV_TASK__TRADE_CREATE_AMENDMENT.equals(taskId) ) {
            shouldDisplay = condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_TRADE_TRANSACTIONS );
        }
        else if (TradePortalConstants.FAV_TASK__TRADE_CREATE_TRACER.equals(taskId) ) {
            shouldDisplay = condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_TRADE_TRANSACTIONS );
        }
        else if (TradePortalConstants.FAV_TASK__TRADE_CREATE_ASSIGNMENT.equals(taskId) ) {
            shouldDisplay = condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_TRADE_TRANSACTIONS );
        }
        else if (TradePortalConstants.FAV_TASK__VIEW_PMT_PEND_TRANS.equals(taskId) ) {
            shouldDisplay = condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_PAYMENT_TRANSACTIONS );
        }
        else if (TradePortalConstants.FAV_TASK__VIEW_PMT_FUTURE_TRANS.equals(taskId) ) {
            shouldDisplay = condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_PAYMENT_TRANSACTIONS );
        }
        else if (TradePortalConstants.FAV_TASK__VIEW_PMT_AUTH_TRANS.equals(taskId) ) {
            shouldDisplay = condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_PAYMENT_TRANSACTIONS );
        }
        else if (TradePortalConstants.FAV_TASK__VIEW_PMT_HISTORY.equals(taskId) ) {
            shouldDisplay = condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_PAYMENT_TRANSACTIONS );
        }
        else if (TradePortalConstants.FAV_TASK__VIEW_DD_PEND_TRANS.equals(taskId) ) {
            shouldDisplay = condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_DIRECT_DEBIT_TRANSACTIONS );
        }
        else if (TradePortalConstants.FAV_TASK__VIEW_DD_HISTORY.equals(taskId) ) {
            shouldDisplay = condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_DIRECT_DEBIT_TRANSACTIONS );
        }
        else if (TradePortalConstants.FAV_TASK__VIEW_RCV_MATCHING.equals(taskId) ) {
            shouldDisplay = condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_RECEIVABLE_TRANSACTIONS );
        }
        else if (TradePortalConstants.FAV_TASK__VIEW_RCV_INVOICES.equals(taskId) ) {
            shouldDisplay = condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_RECEIVABLE_TRANSACTIONS );
        }
        else if (TradePortalConstants.FAV_TASK__VIEW_RCV_HISTORY.equals(taskId) ) {
            shouldDisplay = condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_RECEIVABLE_TRANSACTIONS );
        }
        else if (TradePortalConstants.FAV_TASK__VIEW_ACCT_BALANCES.equals(taskId) ) {
            shouldDisplay = condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_ACCOUNT_BALANCES );
        }
        else if (TradePortalConstants.FAV_TASK__VIEW_PARTIES.equals(taskId) ) {
            shouldDisplay = condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_PARTY );
        }
        else if (TradePortalConstants.FAV_TASK__VIEW_PHRASES.equals(taskId) ) {
            shouldDisplay = condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_PHRASE );
        }
        else if (TradePortalConstants.FAV_TASK__VIEW_USERS.equals(taskId) ) {
            shouldDisplay = condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_USERS );
        }
        else if (TradePortalConstants.FAV_TASK__VIEW_THRESHOLDGROUPS.equals(taskId) ) {
            shouldDisplay = condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_THRESH_GRP );
        }
        else if (TradePortalConstants.FAV_TASK__VIEW_SECURITYPROFILES.equals(taskId) ) {
            shouldDisplay = condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_SEC_PROF );
        }
        else if (TradePortalConstants.FAV_TASK__VIEW_PODEFINITIONS.equals(taskId) ) {
            shouldDisplay = condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_TEXT_PURCHASE_ORDER ) ||
                condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_STRUCTURE_PURCHASE_ORDER );
        }
        //cquinton 2/25/2013 add invoice definitions
        else if (TradePortalConstants.FAV_TASK__VIEW_INV_DEFINITIONS.equals(taskId) ) {
            shouldDisplay = condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_INV_UPLOAD_DEFN );
        }
        else if (TradePortalConstants.FAV_TASK__VIEW_CREATIONRULES.equals(taskId) ) {
            shouldDisplay = condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_CREATE_RULE );
        }
        else if (TradePortalConstants.FAV_TASK__VIEW_REC_MATCH_RULES.equals(taskId) ) {
            shouldDisplay = condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_RECEIVABLES_MATCH_RULES );
        }
        else if (TradePortalConstants.FAV_TASK__VIEW_WORKGROUPS.equals(taskId) ) {
            shouldDisplay = condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_WORK_GROUPS );
        }
        else if (TradePortalConstants.FAV_TASK__VIEW_PANEL_AUTH_GRPS.equals(taskId) ) {
            shouldDisplay = condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_PANEL_AUTH_GRP );
        }
        else if (TradePortalConstants.FAV_TASK__VIEW_TEMPLATE_GRPS.equals(taskId) ) {
            shouldDisplay = condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_PAYMENT_TEMPLATE_GRP );
        }
        else if (TradePortalConstants.FAV_TASK__VIEW_TEMPLATES.equals(taskId) ) {
            shouldDisplay = condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_TEMPLATE );
        }
        else if (TradePortalConstants.FAV_TASK__VIEW_FX_RATES.equals(taskId) ) {
            shouldDisplay = condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_FX_RATE );
        }
        else if (TradePortalConstants.FAV_TASK__UPLOAD_PURCHASE_ORDER.equals(taskId) ) {
            shouldDisplay = condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_TEXT_PURCHASE_ORDER ) ||
                condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_STRUCTURE_PURCHASE_ORDER );
        }
        else if (TradePortalConstants.FAV_TASK__UPLOAD_INVOICE_FILE.equals(taskId) ) {
            shouldDisplay = condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_INVOICE_MANAGEMENT );
        }
        else if (TradePortalConstants.FAV_TASK__UPLOAD_PAYMENT_FILE.equals(taskId) ) {
            shouldDisplay = condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_PAYMENT_FILE_UPLOAD );
        }
 
        return shouldDisplay;
    }
        
}
