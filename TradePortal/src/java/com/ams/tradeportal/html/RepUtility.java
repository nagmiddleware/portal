package com.ams.tradeportal.html;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.servlet.ServletException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.busobj.webbean.CorporateOrganizationWebBean;
import com.ams.tradeportal.busobj.webbean.UserWebBean;
import com.ams.tradeportal.common.SQLParamFilter;
import com.ams.tradeportal.common.TradePortalConstants;
import com.ams.tradeportal.common.cache.Cache;
import com.ams.tradeportal.common.cache.TPCacheManager;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.StringFunction;
import com.amsinc.ecsg.web.BeanManager;
import com.crystaldecisions.sdk.exception.SDKException;
import com.crystaldecisions.sdk.framework.IEnterpriseSession;
import com.crystaldecisions.sdk.occa.infostore.IInfoObject;
import com.crystaldecisions.sdk.occa.infostore.IInfoObjects;
import com.crystaldecisions.sdk.occa.infostore.IInfoStore;
import com.crystaldecisions.sdk.occa.security.IUserInfo;
import com.crystaldecisions.sdk.plugin.CeKind;
/**
 * The RepUtility provides conveninent utility methods for creating
 * reports presentation components.
 *
 *
 *     Copyright  © 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
import java.rmi.RemoteException;

public class RepUtility {
private static final Logger LOG = LoggerFactory.getLogger(RepUtility.class);

	public final static String ALL_CATEGORIES = "All Categories"; 
	public final static String ALL_CATEGORIES_FR = "Toutes les catégories"; 

/**
 * RepUtility constructor
 */
public RepUtility() {
		
}
	
	
/**
 * This method retrieves only tradePortal report categories from Business Objects repository
 * @return DocumentHandler - Categories result set
 * @param webiSession WISession - WebIntelligence session
 * @param docType  String    - Docment type "S" Standard Reports "C" Custom Reports
 */	
public static DocumentHandler getCategories(IEnterpriseSession boSession, String docType, BeanManager beanMgr, String userOid, String reportingLang)throws RemoteException, AmsException
{
	return getCategories(boSession, docType, TradePortalConstants.REPORT_OTL_CATEGORY, beanMgr, userOid, reportingLang);
}

	
/**
 * This method retrieves all report categories from Business Objects repository except those
 * that begin with the string defined by the parameter "omit".  
 * @return DocumentHandler - Categories result set
 * @param webiSession WISession - WebIntelligence session
 * @param docType  String    - Docment type "S" Standard Reports "C" Custom Reports
 * @param omit - categories beginning with this substring will not be returned
 */	


public static DocumentHandler getCategories(IEnterpriseSession boSession, String docType, String omit, BeanManager beanMgr, String userOid, String reportingLang) throws RemoteException, AmsException
	{
		DocumentHandler     doc                       = new DocumentHandler(); 
		String[]            stWebiCategoriesList    = null;
		IInfoObjects        webiDocumentList        = null; 
		IInfoObject         webiDocument            = null; 
		IInfoStore          infoStore               = null; 
		String              strQuery                = null;
		int                 iPersonalFolId          = 0;
		
		IInfoObjects		infoObjs				= null;
		IInfoObject			infoObj					= null;
		int					iCatID					= 0;
		
		try{
			infoStore = (IInfoStore) boSession.getService("InfoStore");
		}
		catch(SDKException e)
		{
				LOG.info(e.getMessage());
		}
		
		int catgCount=0;
		StringBuffer categories = new StringBuffer();
		DocumentHandler resultXML = getCategoriesOptions(reportingLang);
		if(resultXML != null){   			    	   
		    Vector totalCatg =  resultXML.getFragments("/ResultSetRecord");   
		    catgCount = totalCatg.size();
		    for (int i = 0; i < catgCount; i++)
	        {
			   String category    = resultXML.getAttribute("/ResultSetRecord(" + i + ")/DESCR");
			   categories.append("'"+category+"'");
		         if ( i < catgCount-1) {
		        	 categories.append(", ");
		         }                              
	        }     
		}
		
		//IF report type is standard then retrieve all the categories from the corporate folder
		if (docType.equals(TradePortalConstants.STANDARD_REPORTS))
		{
			//KM - Added sort criteria
			strQuery = "SELECT si_name from CI_INFOOBJECTS WHERE SI_KIND='Category' AND SI_NAME in ("+categories.toString()+") order by si_name asc";
			LOG.debug("RepUtility.getCategories - "+ strQuery);
		}
		else
		{
			//if report type is custom reports then retrieve all the categories from the loged in user's personal category
			//get the loged in user name
			IUserInfo userInfo = null;
			String userName =  null;
			
			//get the userinfo object of the user
			try
			{
				userInfo = boSession.getUserInfo();
				userName = userInfo.getUserName();
				iPersonalFolId = userInfo.getPersonalObjectID(CeKind.PERSONALCAT);
				
				//The personal category All Categories is creaetd by Admin users during migration. Since All Categories
				//may not be owned by curent user, we need to run the following queires to retrieve correct categorie ID.
				
				strQuery = "SELECT * FROM CI_INFOOBJECTS WHERE SI_KIND='PersonalCategory' AND SI_OWNER='"+userName+"'";
				LOG.debug("RepUtility.getCategories 1st query - "+ strQuery);
				if(infoStore != null)
				{
					infoObjs = infoStore.query(strQuery);
					
					if(infoObjs.size() > 0)
					{
						infoObj = (IInfoObject) infoObjs.get(0);
						iCatID = infoObj.getID();
					}
							
					strQuery = "SELECT SI_ID,SI_NAME FROM CI_INFOOBJECTS WHERE   SI_NAME in ("+categories.toString()+") AND SI_PARENTID = "+ iCatID;
					LOG.debug("RepUtility.getCategories 2nd query - "+ strQuery);
					infoObjs = infoStore.query(strQuery);
					
					if(infoObjs.size() > 0)
					{
						infoObj = (IInfoObject) infoObjs.get(0);
						iCatID = infoObj.getID();
						String sCat = infoObj.getTitle();
																
						stWebiCategoriesList = new String[1];
						
						//We only have one personal categorey in TP - All Categories
						stWebiCategoriesList[0] = infoObj.getTitle();
						
						LOG.debug("RepUtility.getCategories iCatID = "+ iCatID);
						LOG.debug("RepUtility.getCategories sCat = "+ sCat);
								
					}
				}
			
			}
			catch(SDKException e)
			{
				//throw new Exception("Error retrieving user info");
				LOG.info(e.getMessage());
			}
			
		}
		
		if (docType.equals(TradePortalConstants.STANDARD_REPORTS))
		{
			try{
				
				if(infoStore != null)
				{

					
						webiDocumentList = infoStore.query(strQuery);
						
						
						// the query returns all the subfolder of the personal category as wel as personal category folder
						if(docType.equals(TradePortalConstants.CUSTOM_REPORTS))
						{
							stWebiCategoriesList = new String[webiDocumentList.size()-1];
						}
						else
						{
							stWebiCategoriesList = new String[webiDocumentList.size()];
						}
						
						int rowIndex = 0;
						int infoObjIndex = 0;
						while(infoObjIndex<webiDocumentList.size())
						{
							webiDocument = (IInfoObject) webiDocumentList.get(infoObjIndex);
							if(iPersonalFolId != webiDocument.getID())
							{
								stWebiCategoriesList[rowIndex] = webiDocument.getTitle();
								rowIndex++;
							}
							infoObjIndex++;
						}
				}
			}
			catch(SDKException e)
			{
				LOG.info(e.getMessage());
			}
			//Narayan- CR 603 Added the condition also to not show the custom category if new category checkbox is unchecked.			
			List customCategory = getCustomCategory(beanMgr, userOid, reportingLang);
			if(stWebiCategoriesList!=null){
			for( int rowIndex=0; rowIndex<stWebiCategoriesList.length;rowIndex++ )
			{   
				if((StringFunction.isBlank(omit) || !stWebiCategoriesList[rowIndex].startsWith(omit)))
				{
					doc.setAttribute("/ResultSetRecord("+rowIndex+")/CATEGORY",stWebiCategoriesList[rowIndex]);
				}
			}
			}
		}
		else
		{
			//Custom Reports
			
			try
			{
				doc.setAttribute("/ResultSetRecord("+0+")/CATEGORY",stWebiCategoriesList[0]);
			}
			catch(Exception e)
			{
				LOG.info(e.getMessage());
				doc.setAttribute("/ResultSetRecord("+0+")/CATEGORY", "");
			}
		}
			
		return doc;
	}


/**
 * Get the list of the categories for the selectedCategory. 
 * If selected category = "All Categories" and if the user has categories configured, return the 
 * categories that the user is authorized to. Otherwise return the selected category.
 * 
 * @param selectedCategory
 * @param beanMgr
 * @param userOid
 * @return The selected categories enclosed by single quotes, delimited by comma.
 */
/*
ANZ -> CR -603 -Report Categories  -  Admin user should see only  the category associated with the Corporate Customer.
passing isCustomerSubsidiaryAccess and selectedCustomerOid to display customer spacific categories to Admin user
*/
	public static String getUserCategorySQL (String selectedCategory, BeanManager beanMgr, String userOid,boolean isCustomerSubsidiaryAccess, String selectedCustomerOid, String  currentSecurityType) {
		String selectedCategorySQL = null;
		boolean useUserCategory = false;
   		String [] userReportCategories = new String [10];
		if (selectedCategory.equals(ALL_CATEGORIES)) {
			useUserCategory = getUserCategories(beanMgr, userOid, userReportCategories, isCustomerSubsidiaryAccess, selectedCustomerOid); 
		}
       	if (useUserCategory) {
       		if(TradePortalConstants.ADMIN.equals(currentSecurityType)){
    			selectedCategorySQL = "'" + SQLParamFilter.filter(selectedCategory) + "'";
    		}else{
	    		StringBuffer userAllCategories = new StringBuffer();
		       	for (int i = 0; i < 10; i++) {
		       		if (userReportCategories[i] != null && userReportCategories[i].length() > 0) {
		       			if (userAllCategories.length() > 0) userAllCategories.append(", ");
		       			userAllCategories.append("'").append(SQLParamFilter.filter(userReportCategories[i])).append("'");
		       		}
		       	}
		   		selectedCategorySQL = userAllCategories.toString();
    		}
	   	}
	   	else {
	   		selectedCategorySQL = "'" + SQLParamFilter.filter(selectedCategory) + "'";
	   	}		
	   	
                if ("".equals(selectedCategorySQL)) selectedCategorySQL = "''"; 
	   	return selectedCategorySQL;
	}
	//IR 20512 start - selectedCategorySQL will be like a,b,c  instead of 'a','b','c'
	public static String getUserCategorySQLWithoutQuote (String selectedCategory, BeanManager beanMgr, String userOid,boolean isCustomerSubsidiaryAccess, String selectedCustomerOid, String  currentSecurityType) {
				String selectedCategorySQL = null;
				boolean useUserCategory = false;
		   		String [] userReportCategories = new String [10];
				if (selectedCategory.equals(ALL_CATEGORIES)) {
					useUserCategory = getUserCategories(beanMgr, userOid, userReportCategories, isCustomerSubsidiaryAccess, selectedCustomerOid); 
				}
		       	if (useUserCategory) {
		       		if(TradePortalConstants.ADMIN.equals(currentSecurityType)){
		    			selectedCategorySQL = SQLParamFilter.filter(selectedCategory);
		    		}else{
			    		StringBuffer userAllCategories = new StringBuffer();
				       	for (int i = 0; i < 10; i++) {
				       		if (userReportCategories[i] != null && userReportCategories[i].length() > 0) {
				       			if (userAllCategories.length() > 0) userAllCategories.append(",");
				       			userAllCategories.append(SQLParamFilter.filter(userReportCategories[i]));
				       		}
				       	}
				   		selectedCategorySQL = userAllCategories.toString();
				   		//T36000046179 - Show all report categories if none is selected.
				   		if(StringFunction.isBlank(selectedCategorySQL)){
				   			selectedCategorySQL = ALL_CATEGORIES;
				   		}
		    		}
			   	}
			   	else {
			   		selectedCategorySQL = SQLParamFilter.filter(selectedCategory);
			   	}		
			   	
			   	return selectedCategorySQL;
			}

	/**
	 * Get the HTML of drop down options for reporting categories.  If the user has reporting categories configured,
	 * only include the categories that the user is authorized for. 
	 * @param dochdr - the categories retrieved from BO repository
	 * @param selectedCategory
	 * @param locale
	 * @param beanMgr
	 * @param userOid - Oid of the current user.
	 * @return
	 * @throws ServletException
	 */
	/*
	ANZ -> CR -603 -Report Categories  -  Admin user should see only  the category associated with the Corporate Customer.
	passing isCustomerSubsidiaryAccess and selectedCustomerOid to display customer spacific categories to Admin user
	*/
	public static String getUserCategoryOptions (DocumentHandler categoriesInBO, String selectedCategory, String locale, BeanManager beanMgr, String userOid, String includeAllCategory, boolean isCustomerSubsidiaryAccess, String selectedCustomerOid, String  currentSecurityType) 
	throws ServletException , RemoteException, AmsException{
		DropdownOptions categoryDropdownOptions = new DropdownOptions();
    	categoryDropdownOptions.setSelected(selectedCategory);	
		String reportCategoryOptions = null;
   		String [] userReportCategories = new String [10];
   		boolean allCatflag  = true;
   		
		boolean useUserCategory = getUserCategories(beanMgr, userOid, userReportCategories,isCustomerSubsidiaryAccess, selectedCustomerOid);        	
	   	
	   	if (useUserCategory) {
	   		if(TradePortalConstants.ADMIN.equals(currentSecurityType)){
    			reportCategoryOptions = Dropdown.createSortedOptions(categoriesInBO, "CATEGORY", "CATEGORY", selectedCategory, locale);
    		}else{
		       	for (int i = 0; i < 10; i++) {
		       		if (userReportCategories[i] != null && userReportCategories[i].length() > 0) {
		       			DocumentHandler cate = getCategoriesOptions(locale);
		       		 if(cate!=null){
		       			Vector repVector = cate.getFragments("/ResultSetRecord");
		       			int numItems = repVector.size();
		       		   
		       		   for (int iLoop=0;iLoop<numItems;iLoop++) {
		       		       DocumentHandler acctDoc = (DocumentHandler) repVector.elementAt(iLoop);
		       		       String code = acctDoc.getAttribute("/CODE") != null ? acctDoc.getAttribute("/CODE") : acctDoc.getAttribute("/code");
		       		       String descr = acctDoc.getAttribute("/DESCR") != null ? acctDoc.getAttribute("/DESCR") : acctDoc.getAttribute("/descr");
		       		       
		       		       if(code.equalsIgnoreCase(userReportCategories[i])){
		       		    	   categoryDropdownOptions.addOption(descr, descr);
		       		    	  //14-12-2013 Rel 8.4 CR-854 IR T36000023776 [BEGIN]- Not to add duplicate option 
		       		    	   if(ALL_CATEGORIES_FR.equalsIgnoreCase(descr) || ALL_CATEGORIES.equalsIgnoreCase(descr))
			       		       {
			       		    	   allCatflag = false;
			       		       }
		       		       }
		       		   }
		       	     }
		       		}
		       	}	
		       	 // Narayan CR603 while creating and saving report all categoty should be included only for user if category indication is off at bank level
		       	if(TradePortalConstants.INDICATOR_NO.equalsIgnoreCase(includeAllCategory) && allCatflag){
		       		if(locale.contains("fr")){
		       			categoryDropdownOptions.addOption(ALL_CATEGORIES_FR, ALL_CATEGORIES_FR);
		       		}else{
		       			categoryDropdownOptions.addOption(ALL_CATEGORIES, ALL_CATEGORIES);
		       		}
		       	}
		   		reportCategoryOptions = categoryDropdownOptions.createSortedOptionListInHtml(locale);
    		}
	   	}
	   	else {
		     reportCategoryOptions = Dropdown.createSortedOptions(categoriesInBO, "CATEGORY", "CATEGORY", selectedCategory, locale);
	   	}		
	   	
	   	return reportCategoryOptions;
	}
	
	/**
	 * Get the user's categories.  If the user has no categories, get all the categories of the parent corporate customer.
	 * 
	 * @param beanMgr
	 * @param userOid
	 * @param userReportCategories - String [] 
	 * @return - boolean whether the user's client bank uses categories.
	 */
	/*
	ANZ -> CR -603 -Report Categories  -  Admin user should see only  the category associated with the Corporate Customer.
	passing isCustomerSubsidiaryAccess and selectedCustomerOid to display customer spacific categories to Admin user
	*/
	private static boolean getUserCategories(BeanManager beanMgr, String userOid, String [] userReportCategories, boolean isCustomerSubsidiaryAccess, String selectedCustomerOid) {
		boolean hasUserCategory = false;
        UserWebBean thisUser = beanMgr.createBean(UserWebBean.class, "User");      
	    thisUser.setAttribute("user_oid", userOid);
     	thisUser.getDataFromAppServer();
     	
     	// W Zhu 6/3/2011 CR-603 IR-AIUL053156679
     	// Check the REPORT_CATEGORIES_IND on the client bank.  Check user categories only if the indicator is on.
     	String clientBankOid = thisUser.getAttribute("client_bank_oid");
     	if (clientBankOid != null && !clientBankOid.equals("")) { 
     		Cache CBCache = TPCacheManager.getInstance().getCache(TradePortalConstants.CLIENT_BANK_CACHE);
     		DocumentHandler CBCResult = (DocumentHandler)CBCache.get(clientBankOid);
     		String useCategories = CBCResult.getAttribute("/ResultSetRecord(0)/REPORT_CATEGORIES_IND");
     		if (!TradePortalConstants.INDICATOR_YES.equals(useCategories)) {
     			return false;
     		}
     	}

		String [] userReportCategoryNames = {"first_rept_categ", "second_rept_categ",
				"third_rept_categ",	"fourth_rept_categ",
				"fifth_rept_categ",	"sixth_rept_categ",
				"seventh_rept_categ","eighth_rept_categ",
				"nineth_rept_categ","tenth_rept_categ"};

     	// Check the user categories.
       	for (int i = 0; i < 10; i++) {
       		userReportCategories[i] = thisUser.getAttribute(userReportCategoryNames[i]);
       		if (userReportCategories[i] != null && userReportCategories[i].length() > 0) {
       			hasUserCategory = true;
       		}
       	}
       	
       	// W Zhu 6/3/2011 CR-603 IR-AAUL060235916 Take the corporate customer's categories into account.
       	if (!hasUserCategory && (TradePortalConstants.OWNER_CORPORATE.equals(thisUser.getAttribute("ownership_level"))|| isCustomerSubsidiaryAccess)) {
       		String corporateOrgOid = null;
       		if(isCustomerSubsidiaryAccess){
       			corporateOrgOid = selectedCustomerOid;
       		}else{
       			corporateOrgOid = thisUser.getAttribute("owner_org_oid");
       		}
       		CorporateOrganizationWebBean thisCorporateOrg = beanMgr.createBean(CorporateOrganizationWebBean.class, "CorporateOrganization");      
       		thisCorporateOrg.setAttribute("organization_oid", corporateOrgOid);
       		thisCorporateOrg.getDataFromAppServer();
           	for (int i = 0; i < 10; i++) {
           		userReportCategories[i] = thisCorporateOrg.getAttribute(userReportCategoryNames[i]);
           		if (userReportCategories[i] != null && userReportCategories[i].length() > 0) {
           			hasUserCategory = true;
           		}
           	}       		
       	}
       	
       	return true; //DVUL070864037 Return true as long as the client bank is using user categories, even though userReportCategories[] could be empty.
	}
    // Narayan CR-603 this will return the all portal custom category if check box is checked else empty.
	private static List getCustomCategory(BeanManager beanMgr, String userOid, String reportingLang) throws RemoteException, AmsException{
		List customCategory = new ArrayList();
		UserWebBean thisUser = beanMgr.createBean(UserWebBean.class, "User");      
	    thisUser.setAttribute("user_oid", userOid);
   	    thisUser.getDataFromAppServer();
   	
   	    String clientBankOid = thisUser.getAttribute("client_bank_oid");
   	    if (clientBankOid != null && !("").equals(clientBankOid)) {
   		Cache CBCache = TPCacheManager.getInstance().getCache(TradePortalConstants.CLIENT_BANK_CACHE);
   		DocumentHandler CBCResult = (DocumentHandler)CBCache.get(clientBankOid);
   		String useCategories = CBCResult.getAttribute("/ResultSetRecord(0)/REPORT_CATEGORIES_IND");
   		

			List<Object> sqlParams = new ArrayList();
			if (TradePortalConstants.INDICATOR_NO.equals(useCategories)) {
				// language option
				String sqlStatementCustomCategory = null;
				if (reportingLang != null&& reportingLang.toLowerCase().contains("fr")) {
					sqlStatementCustomCategory = " SELECT CODE,DESCR FROM REFDATA WHERE  TABLE_TYPE = ? and locale_name = ? ";
					sqlParams.add(TradePortalConstants.REPORT_CATEGORY);
					sqlParams.add("fr_CA");
				} else {
					sqlStatementCustomCategory = " SELECT CODE,DESCR FROM REFDATA WHERE   TABLE_TYPE = ? and locale_name is NULL";
					sqlParams.add(TradePortalConstants.REPORT_CATEGORY);
				}
				DocumentHandler resultXML = DatabaseQueryBean.getXmlResultSet(sqlStatementCustomCategory, false, sqlParams);
				int catgCount = 0;
				if (resultXML != null) {
					Vector totalCatg = resultXML.getFragments("/ResultSetRecord");
					catgCount = totalCatg.size();
					for (int totalCount = 0; totalCount < catgCount; totalCount++) {
						String category = resultXML.getAttribute("/ResultSetRecord(" + totalCount+ ")/DESCR");
						customCategory.add(category);
					}
				}
			}
   	   }  
	 return customCategory;
   }
	
		
	
	//jgadela Rel 8.4 CR-854 [BEGIN]- Dynamic population of ReportingCategory dropdowns based on ReportingLanguage selection
	private static DocumentHandler getCategoriesOptions(String reportingLang) throws RemoteException, AmsException{
		String sqlQryCat = null;

		if(reportingLang!= null && reportingLang.toLowerCase().contains("fr")){
			sqlQryCat = " select CODE, DESCR from refdata where table_type = ? and locale_name = 'fr_CA'";
		}else{
			sqlQryCat = " select CODE, DESCR from refdata where table_type = ? and locale_name is NULL";
		}
  
		DocumentHandler resultXML = DatabaseQueryBean.getXmlResultSet(sqlQryCat, false, new Object[]{"REPORTING_CATEGORY"});
		
	 return resultXML;
   	
	}
}
