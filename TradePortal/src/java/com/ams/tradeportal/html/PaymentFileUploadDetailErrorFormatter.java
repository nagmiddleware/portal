package com.ams.tradeportal.html;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.util.StringFunction;

/**
 * PaymentFileUploadDetailErrorFormatter writes out a carriage return prior to each "-" so that the error messages
  * are formatted to the page
 */
public class PaymentFileUploadDetailErrorFormatter extends AbstractCustomListViewColumnFormatter {
private static final Logger LOG = LoggerFactory.getLogger(PaymentFileUploadDetailErrorFormatter.class);

	/**
	 * Format data.
	 * @param obj - error message
	 * @return all '-' to '<br>-'
	 */
    public String format(Object obj) {
        String formattedData = (String) obj;

		if (StringFunction.isNotBlank(formattedData))
		{
	        formattedData = formattedData.replace("-","<br>-");
	        formattedData = formattedData.replaceFirst("<br>","");
		}

        return formattedData;
    }
}
