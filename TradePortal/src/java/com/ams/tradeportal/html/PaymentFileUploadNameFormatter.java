package com.ams.tradeportal.html;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.util.StringFunction;

/**
 * PaymentFileUploadNameFormatter truncates the 200 character payment_file_upload.payment_file_name to 70 characters for display purposes
 */
public class PaymentFileUploadNameFormatter extends AbstractCustomListViewColumnFormatter {
private static final Logger LOG = LoggerFactory.getLogger(PaymentFileUploadNameFormatter.class);

	/**
	 * Format data.
	 * @param obj - error message
	 * @return payment file name of 70 characters or less
	 */
    public String format(Object obj) {
        String formattedData = (String) obj;

		if (StringFunction.isNotBlank(formattedData))
		{
			//RKAZI IR PUL111835686 02/10/2011 START
			//reduced filename length from 70 to 40
			if (formattedData.length() > 40)
				formattedData = formattedData.substring(0,40);
			//RKAZI IR PUL111835686 02/10/2011 START
		}

        return formattedData;
    }
}
