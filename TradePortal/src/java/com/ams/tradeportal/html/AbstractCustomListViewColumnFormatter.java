package com.ams.tradeportal.html;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.busobj.webbean.SessionWebBean;
import com.amsinc.ecsg.util.ResourceManager;

/**
 * AbstractCustomListViewColumnFormatter must be implemented
 * by all custom formatters.  A custom formatter is used by the
 * ListViewHandler when a column is a 'custom' datatype.
 * The 'customFormatter' tag specifies the class name of the
 * custom formatter.
 * This mechanism allows custom formatting logic to not be
 * embedded within infrastructure common classes.
 */
public abstract class AbstractCustomListViewColumnFormatter {
private static final Logger LOG = LoggerFactory.getLogger(AbstractCustomListViewColumnFormatter.class);

    private SessionWebBean userSession;
    private ResourceManager resourceManager;

    /**
     * Set the userSession set it can be used by custom formatter.
     * This is set by the ListViewHandler, so it always available.
     */
    public void setUserSession(SessionWebBean x) {
        this.userSession = x;
    }

    /**
     * Get the userSession.
     */
    public SessionWebBean getUserSession() {
        return userSession;
    }

    /**
     * Set the resourceManager set it can be used by custom formatter.
     * This is set by the ListViewHandler, so it always available.
     */
    public void setResourceManager(ResourceManager r) {
        this.resourceManager = r;
    }

    /**
     * Get the resourceManager.
     */
    public ResourceManager getResourceManager() {
        return resourceManager;
    }

    /**
     * Get the resourceManager.
     */
    //MDB CR-564 Rel6.1 12/21/10 - default method, link will be displayed
    public boolean determineOverrideLink(String obj) {
        return false;
    }

    /**
     * Format the data values.
     * This is called for every row with a custom column so need to ensure
     * it is fast.
     */
    public abstract String format(Object obj);
}
