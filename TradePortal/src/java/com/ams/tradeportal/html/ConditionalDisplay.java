package com.ams.tradeportal.html;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Field;

import com.ams.tradeportal.busobj.util.InstrumentAuthentication;
import com.ams.tradeportal.busobj.util.SecurityAccess;
import com.ams.tradeportal.busobj.webbean.CorporateOrganizationWebBean;
import com.ams.tradeportal.busobj.webbean.BankOrganizationGroupWebBean;
import com.ams.tradeportal.busobj.webbean.ClientBankWebBean;
import com.ams.tradeportal.busobj.webbean.SessionWebBean;
import com.ams.tradeportal.common.InstrumentType;
import com.ams.tradeportal.common.TradePortalConstants;
import com.ams.tradeportal.common.cache.Cache;
import com.ams.tradeportal.common.cache.TPCacheManager;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.web.BeanManager;
import com.amsinc.ecsg.util.StringFunction;


/**
  * Encapsulates logic for determining whether components
  * on the user interface should be displayed or not.
  * This includes both SecurityAccess (i.e. security profile)
  * settings, and other ref data specific to the user, the
  * user's organizations, etc.
  *
  * Note: the security specific pieces are actually handled in
  * the SecurityAccess class, which is referenced frequently here.
  * See that class for specific security access points.
  *c
  */
public class ConditionalDisplay {
private static final Logger LOG = LoggerFactory.getLogger(ConditionalDisplay.class);

    /*
     * The display items are unique constants that identify
     * whether to display certain components in the user interface.
     * Although they may be shared (and is certainly the point
     * of having a common class to provide this functionality)
     * each different component in the system that has unique
     * display characteristics should have a different constant
     * defined below.
     *
     * The constants are utilized by the MenuFactory and GridFactory
     * classes as well - the constant is specified in the associated
     * xml and this class loads the correct set of display rights
     * to determine if the item should be displayed.
     * For those items that are dependent on others, those
     * dependencies are maintained in this class.
     *
     * Numbers here don't matter as long as they are unique.
     */

    //following set of items are for new instrument capabilities
    // from the menu
    public final static int DISPLAY_NEW_INSTRUMENTS = 1;
    public final static int DISPLAY_NEW_TRADE_INSTRUMENTS = 10;
    public final static int DISPLAY_NEW_AIR_WAYBILL_INSTRUMENT = 11;
    public final static int DISPLAY_TRANSFER_EXPORT_COLL = 12;
    public final static int DISPLAY_NEW_AIR_WAYBILL = 13;
    public final static int DISPLAY_NEW_APPROVAL_TO_PAY = 14;
    public final static int DISPLAY_NEW_DIRECT_SEND_COLL = 15;
    public final static int DISPLAY_NEW_EXPORT_COLL = 16;
    public final static int DISPLAY_NEW_DLC = 17;
    public final static int DISPLAY_NEW_LOAN_RQST = 18;
    public final static int DISPLAY_NEW_GUAR = 19;
    public final static int DISPLAY_NEW_SLC = 20;
    public final static int DISPLAY_NEW_REQUEST_ADVISE = 21;
    public final static int DISPLAY_NEW_SHIP_GUAR = 22;
    public final static int DISPLAY_NEW_PAYMENT_INSTRUMENTS = 30;
    public final static int DISPLAY_NEW_TRANSFER_BTW_ACCTS = 31;
    public final static int DISPLAY_NEW_DOMESTIC_PAYMENT = 32;
    public final static int DISPLAY_NEW_INT_PAYMENT = 33;
    public final static int DISPLAY_NEW_INT_PAYMENT_TRADE = 34;

    public final static int DISPLAY_NEW_DIRECT_DEBIT_INSTRUMENTS = 40;
    public final static int DISPLAY_NEW_DDI = 41;
    public final static int DISPLAY_NEW_SLC_DET = 42;
    //following set of items are for transaction capabilities
    // either from the menu, home page, customization pages or elsewhere
    public final static int DISPLAY_TRANSACTIONS = 100;
    public final static int DISPLAY_TRADE_TRANSACTIONS = 101;
    public final static int DISPLAY_PAYMENT_TRANSACTIONS = 102;
    public final static int DISPLAY_RECEIVABLE_TRANSACTIONS = 103;
    public final static int DISPLAY_DIRECT_DEBIT_TRANSACTIONS = 104;
    //Ravindra - CR-708B - Start
    public final static int DISPLAY_INVOICE_OFFERS = 105;
    public final static int DISPLAY_INVOICE_OFFERED = 1051;
    public final static int DISPLAY_FUTURE_VALUE_OFFERS = 1052;
    public final static int DISPLAY_INVOICE_OFFERS_HISTORY = 1053;
    public final static int DISPLAY_SP_ACCEPT_OFFER = 106;
	public final static int DISPLAY_SP_AUTHORIZE_OFFER = 107;
	public final static int DISPLAY_SP_DECLINE_OFFER = 108;
	public final static int DISPLAY_SP_ASSIGN_FVD = 109;
	public final static int DISPLAY_SP_REMOVE_FVD = 110;
	public final static int DISPLAY_SP_REMOVE_INVOICE = 111;
    public final static int DISPLAY_SP_RESET_TO_OFFERED = 112;
    public final static int DISPLAY_CREATE_SETTLEMENT_INSTRUCTION = 113;
    public final static int DISPLAY_REQUEST_ROLLOVER = 114;
	//Ravindra - CR-708B - End

    //accounts
    public final static int DISPLAY_ACCOUNTS = 200;
    public final static int DISPLAY_ACCOUNT_BALANCES = 201;

    //reports
    public final static int DISPLAY_REPORTS = 300;
    public final static int DISPLAY_STANDARD_REPORTS = 301;
    public final static int DISPLAY_CUSTOM_REPORTS = 302;

    //refdata
    public final static int DISPLAY_REFDATA = 400;
    public final static int DISPLAY_PARTY = 401;
    public final static int DISPLAY_PHRASE = 402;
    public final static int DISPLAY_THRESH_GRP = 403;
    public final static int DISPLAY_PAYMENT_TEMPLATE_GRP = 404;
    public final static int DISPLAY_TEMPLATE = 405;
    public final static int DISPLAY_USERS = 406;
    public final static int DISPLAY_SEC_PROF = 407;
    public final static int DISPLAY_FX_RATE = 408;
    public final static int DISPLAY_CREATE_RULE = 409;
    public final static int DISPLAY_PO_UPLOAD_DEFN = 410;
    public final static int DISPLAY_RECEIVABLES_MATCH_RULES = 411;
    public final static int DISPLAY_PANEL_AUTH_GRP = 412;
    public final static int DISPLAY_WORK_GROUPS = 413;
    public final static int DISPLAY_INV_UPLOAD_DEFN = 414;
    public final static int DISPLAY_INTEREST_DISCOUNT_RATE = 415;
    //AAlubala - Rel8.2 CR741
    public final static int DISPLAY_ERP_GL_CODES = 416;
    public final static int DISPLAY_DISCOUNT_CODES = 417;
    //MEerupula Rel8.3 CR 776
    public final static int DISPLAY_MYORGPROFILE = 418;
  //JGADELA - Rel8.3 CR501
    public final static int DISPLAY_APPROVE_REF_DATA = 419;

    //adminrefdata

    //upload center
    public final static int DISPLAY_UPLOAD_CENTER = 500;
    public final static int DISPLAY_PURCHASE_ORDER_LIST = 501;
    public final static int DISPLAY_PAYMENT_FILE_UPLOAD = 502;
    public final static int DISPLAY_INVOICE_GROUP = 503;
    public final static int DISPLAY_INVOICE_LIST = 504;
    public final static int DISPLAY_TEXT_PURCHASE_ORDER = 505;
    public final static int DISPLAY_STRUCTURE_PURCHASE_ORDER = 506;
    public final static int DISPLAY_INVOICE_MANAGEMENT = 507;

    //messaging capabilities
    public final static int DISPLAY_MESSAGES = 600;
    public final static int DISPLAY_MAIL_MESSAGES = 601;
    public final static int DISPLAY_NOTIFICATIONS = 602;


    //admin menu stuff
    public final static int DISPLAY_ADMIN_ORGS = 700;
    public final static int DISPLAY_CLIENT_BANKS = 701;
    public final static int DISPLAY_BANK_GROUPS = 702;
    public final static int DISPLAY_OP_ORGS = 703;
    public final static int DISPLAY_CORP_ORGS = 704;
    public final static int DISPLAY_EXTERNAL_BANKS = 705; //smanohar Kyriba PPX268
    
    public final static int DISPLAY_ADMIN_USERS_AND_SECURITY = 710;
    public final static int DISPLAY_ADMIN_USERS = 711;
    public final static int DISPLAY_ADMIN_SEC_PROF = 712;
    public final static int DISPLAY_LOCKED_OUT_USERS = 713;
    public final static int DISPLAY_ADMIN_ANNOUNCEMENTS = 714;

    public final static int DISPLAY_ADMIN_REFDATA = 800;
    public final static int DISPLAY_NOTIF_RULE_TEMPLATES = 801;//Added for Rel9.5 CR927B
    public final static int DISPLAY_GM_CAT_GRP = 802;
    public final static int DISPLAY_CROSS_RATE_RULE = 803;
    public final static int DISPLAY_BANK_BRANCH_RULES = 804;
    public final static int DISPLAY_PAYMENT_METH_VAL = 805;
    public final static int DISPLAY_CALENDAR = 806;
    public final static int DISPLAY_CURRENCY_CALENDAR_RULES = 807;
  //jgadela  Rel8.3 CR-501 04/08/2013 Start
    public final static int DISPLAY_APPROVE_REF_DATA_ADMIN = 808;
  //jgadela  Rel8.3 CR-501 04/08/2013 end


    //following set of items are specific to data grid footer actions
    public final static int DISPLAY_TRADE_AUTHORIZE_ACTION = 1000;
    public final static int DISPLAY_TRADE_DELETE_ACTION = 1001;
    public final static int DISPLAY_TRADE_ROUTE_ACTION = 1002;
    public final static int DISPLAY_PAYMENT_AUTHORIZE_ACTION = 1010;
    public final static int DISPLAY_PAYMENT_DELETE_ACTION = 1011;
    public final static int DISPLAY_EDIT_FX_RATE_ACTION = 1012;
    public final static int DISPLAY_APPROVE_FIN_ACTION = 1013;
    public final static int DISPLAY_AUTH_INV_ACTION = 1014;
    public final static int DISPLAY_REMOVE_INV_ACTION = 1015;
    public final static int DISPLAY_DELETE_INV_ACTION = 1016;
    public final static int DISPLAY_APPROVE_AND_AUTH_ACTION = 1017;
    public final static int DISPLAY_PROXY_AUTH_ACTION = 1018;
    public final static int DISPLAY_DDI_DELETE_ACTION = 1019;
    public final static int DISPLAY_DDI_ROUTE_ACTION = 1020;
    public final static int DISPLAY_PROXY_AUTH_ACTION_INVOICE = 1021;
    public final static int DISPLAY_CM_INSTRUMENT_AUTHORIZE_ACTION = 1022;

    //Srinivasu_D CR-709 Rel8.2 12/14/2012 Start
    public final static int DISPLAY_CLEAR_PAYMENT_ACTION = 1024;
    public final static int DISPLAY_ASSIGN_LOAN_TYPE_ACTION = 1025;
    public final static int DISPLAY_CREATE_LOANREQ_BYRULES_ACTION = 1026;
    //Srinivasu_D CR-709 Rel8.2 12/14/2012 End

    public final static int DISPLAY_INVOICE_UPLOAD = 1027;
	public final static int DISPLAY_INV_CREATE_RULE = 1028;
	//Srinivasu_D IR#11999 Rel8.2 start
	public final static int DISPLAY_CREATE_LOAN_REQ = 1029;
	public final static int DISPLAY_REC_APPLY_PAYMENT_DATE = 1030;
	public final static int DISPLAY_REC_ASSIGN_INSTR_TYPE = 1031;
	public final static int DISPLAY_REC_CLOSE = 1032;
	//Srinivasu_D IR#11999 Rel8.2 end
    //cquinton 2/21/2013 ir#12117
    public final static int DISPLAY_PO_CREATE_ATP_VIA_RULES = 1033;
    public final static int DISPLAY_PO_CREATE_LC_VIA_RULES = 1034;
    //SHR
    public final static int DISPLAY_DELETE_PAY_INV_ACTION = 1035;
	public final static int DISPLAY_CREATE_ATP_ACTION = 1036;
    public final static int DISPLAY_APPLY_PAYMENT_DATE_ACTION = 1037;
    public final static int DISPLAY_PAY_CLEAR_PAYMENT_ACTION = 1038;
    public final static int DISPLAY_PAY_ASSIGN_LOAN_TYPE_ACTION = 1039;
    public final static int DISPLAY_PAY_ASSIGN_INSTR_TYPE = 1040;
    public final static int DISPLAY_PAY_CREATE_LOAN_REQ = 1041;
	public final static int DISPLAY_PAY_CREATE_LOANREQ_BYRULES_ACTION = 1042;
	public final static int DISPLAY_PAY_CREATE_ATP_BYRULES_ACTION = 1043;

	//Srinivasu_D CR#269 Rel8.4 09/02/2013 - start
	public final static int DISPLAY_CC = 1044;
	public final static int DISPLAY_CC_NEW_GUAR= 1045;
	//Srinivasu_D CR#269 Rel8.4 09/02/2013 - end
	public final static int DISPLAY_AUTHORIZE_ACTION_TO_INVOICE = 1046;
	public final static int DISPLAY_OTHER_ACTION_TO_INVOICE = 1047;
	public final static int DISPLAY_OTHER_ACTION_TO_LR_INVOICE = 1048;
	public final static int DISPLAY_OTHER_ACTION_TO_CREDT_INVOICE = 1049;
	public final static int DISPLAY_OTHER_ACTION_TO_LR_PAY_INVOICE = 1050;
	public final static int DISPLAY_OTHER_ACTION_TO_PEND_PAY_INVOICE = 1054;
	
	//jgadela 08/09/2013 REL 8.3 CR-821 - T36000019559 <START>
	public final static int DISPLAY_DUAL_AUTH_APPROVAL_TO_PAY = 1100;
	public final static int DISPLAY_DUAL_AUTH_AIRWAY_BILL = 1101;
	public final static int DISPLAY_DUAL_AUTH_DISC_RESPONSE = 1102;
	public final static int DISPLAY_DUAL_AUTH_EXPORT_COLL = 1103;
	public final static int DISPLAY_DUAL_AUTH_EXPORT_LC = 1104;
	public final static int DISPLAY_DUAL_AUTH_DIRECT_DEBIT = 1105;
	public final static int DISPLAY_DUAL_AUTH_GUARANTEE = 1106;
	public final static int DISPLAY_DUAL_AUTH_IMPORT_DLC = 1107;
	public final static int DISPLAY_DUAL_AUTH_LOAN_REQUEST = 1108;
	public final static int DISPLAY_DUAL_AUTH_REQUEST_ADVISE = 1109;
	public final static int DISPLAY_DUAL_AUTH_SUPPLIER_PORTAL = 1110;
	public final static int DISPLAY_DUAL_AUTH_SHIPPING_GUAR = 1111;
	public final static int DISPLAY_DUAL_AUTH_SLC = 1112;
	public final static int DISPLAY_DUAL_AUTH_FUNDS_TRANSFER = 1113;
	public final static int DISPLAY_DUAL_DOMESTIC_PAYMENTS = 1114;
	public final static int DISPLAY_DUAL_XFER_BTWN_ACCTS = 1115;
	public final static int DISPLAY_PANEL_AUTHORISATION_TRANSACTIONS  = 1116;
	//jgadela 08/09/2013 REL 8.3 CR-821 - T36000019559 <END>
	
	//RKAZI REL 8.4 CR-599 11/20/2013 - ADD - START
    public final static int DISPLAY_PAYMENT_METH_DEFN = 1117;
    //RKAZI REL 8.4 CR-599 11/20/2013 - ADD - END
    
    //CR-913 Rel9.0 start
    
    public final static int DISPLAY_PAYABLES_TRANSACTIONS = 1118;
    public final static int DISPLAY_DEBIT_FUNDING_MAIL =1119;
    
    public final static int DISPLAY_PAY_MGMT_INV_AUTH = 1120;
    public final static int DISPLAY_PAY_MGMT_INV_OFFLINE_AUTH =1121;
    public final static int DISPLAY_PAY_MGMT_INV_APPLY_PAY_DATE =1122;
    public final static int DISPLAY_PAY_MGMT_INV_CLEAR_PAY_DATE =1123;
    public final static int DISPLAY_PAY_MGMT_INV_MODIFY_SUPP_DATE =1124;
    public final static int DISPLAY_PAY_MGMT_INV_RESET_SUPP_DATE =1125;
    public final static int DISPLAY_PAY_MGMT_INV_APPLY_EARLY_PAY_AMT =1126;
    public final static int DISPLAY_PAY_MGMT_INV_RESET_EARLY_PAY_AMT =1127;
    
    public final static int DISPLAY_MODIFY_SUPPLIER_DATE_ACTION = 1128;
    public final static int DISPLAY_RESET_SUPPLIER_DATE_ACTION = 1129;
    public final static int DISPLAY_APPLY_PAYMENT_AMOUNT_ACTION = 1130;
    public final static int DISPLAY_RESET_PAYMENT_AMOUNT_ACTION = 1131;
    public final static int DISPLAY_UPLOAD_PAY_MGM_AUTH_ACTION = 1132;
    public final static int DISPLAY_UPLOAD_PAY_MGM_OFFLINE_AUTH_ACTION = 1133;
    public final static int DISPLAY_DATES_ACTION = 1134;
    public final static int DISPLAY_AMOUNTS_ACTION = 1135;
    public final static int DISPLAY_INSTRUMENTS_ACTION = 1136;
    public final static int DISPLAY_PAY_PRGM_GROUPS_ACTION = 1137;
    public final static int DISPLAY_PAY_PRGM_GROUP_LIST_ACTION = 1138;
    public final static int DISPLAY_PAY_MGMT_AUTH_ACTION = 1139;
    public final static int DISPLAY_SP_OFFER_ACTIONS = 1140;
    public final static int DISPLAY_SP_FVD_ACTIONS = 1141;
    public final static int DISPLAY_PAY_MGMT_DATES_ACTION = 1142;
    public final static int DISPLAY_PAY_MGMT_AMOUNTS_ACTION = 1143;
    public final static int DISPLAY_REC_FINANCE_ACTION = 1144;
    public final static int DISPLAY_REC_DATE_ACTION = 1145;
    public final static int DISPLAY_REC_INSTRUMENT_ACTION = 1146;
    //IR-32102
    public final static int DISPLAY_AUTHORIZE_ACTION_TO_REC_INVOICE = 1055;
    
    
  //CR-913 Rel9.0 end
    public final static int DISPLAY_DELETE_NOTIFICATION = 1147;
    public final static int DISPLAY_PREDEBIT_ROUTE = 1148;
    public final static int DISPLAY_PREDEBIT_DELETE = 1149;
    
  // CR-914A Rel9.2
    public final static int DISPLAY_PAY_MGMT_CRT_AUTH   = 1150;
    public final static int DISPLAY_PAY_MGMT_CRT_CLOSE  =1151;
    public final static int DISPLAY_PAY_MGMT_CRT_DELETE =1152;
    public final static int DISPLAY_PAY_MGMT_CRT_APPLY =1153;
    public final static int DISPLAY_PAY_MGMT_CRT_UNAPPLY =1154;
    
    //MEer Rel 9.2 IR-37783
    public final static int DISPLAY_ATTACH_DOCUMENT_ACTION = 1155;
    public final static int DISPLAY_DELETE_DOCUMENT_ACTION = 1156;
    public final static int DISPLAY_DOCUMENT = 1157;
    
    /*pgedupudi - Rel9.3 CR976A 03/09/2015 START*/
    public final static int DISPLAY_BANK_GROUP_RESTRICT_RULES =1158;
    /*pgedupudi - Rel9.3 CR976A 03/09/2015 END*/
    
	// Srinivasu_D CR#1006 Rel9.3 05/11/2015 added decline for payables/credit/receivables
	public final static int DISPLAY_UPLOAD_PAY_MGM_INV_DECLINE_ACTION = 1160;
	public final static int DISPLAY_UPLOAD_PAY_MGM_CRN_DECLINE_ACTION = 1161;
	public final static int DISPLAY_UPLOAD_REC_MGM_INV_DECLINE_ACTION = 1162;
	// Srinivasu_D CR#1006 Rel9.3 05/11/2015 End
	
	//Rel9.3.5 CR-1028/1029 START
	public final static int  DISPLAY_UPDATE_CENTRE = 1164;
	public final static int  DISPLAY_BANK_XML_DOWNLOADS = 1165;
	public final static int  DISPLAY_BANK_TRANSACTION_UPDATES = 1166;
	public final static int  DISPLAY_BANK_MAIL_MESSAGES = 1167;
	//Rel9.3.5 CR-1028/1029 END

	public final static int  DISPLAY_PAY_MGMT_CRT_AUTH_OR_DEC = 1163;
	//rel 94 cr 932
	public final static int  DISPLAY_BILLING_TRANSACTIONS = 1168;
	
	public final static int DISPLAY_DUAL_AUTH_SETTLE_INSTRUCTION = 1169;
	
	public final static int DISPLAY_NOTIFICATION_RULES = 1170;//Added for Rel9.5 CR927B
	
//some web beans for reference data checks
    CorporateOrganizationWebBean ownerOrg = null;

    //the basic user component
    private SessionWebBean userSession;
    private BeanManager beanMgr;
    // a necessary helper for web beans
    //convenience variables
    private String securityRights;
 

    /**
     * This is a bean so must have a default constructor.
     */
    public ConditionalDisplay() {
    }

    //these properties should always be set when ConditionalDisplay is instantiated, as so:
    //<jsp:useBean id="condDisplay" class="com.ams.tradeportal.html.ConditionalDisplay" scope="request">
    //  <jsp:setProperty name="condDisplay" property="userSession" value="userSession" />
    //  <jsp:setProperty name="condDisplay" property="beanMgr" value="beanMgr" />
    //</jsp:useBean>
    public SessionWebBean getUserSession() {
        return userSession;
    }
    public BeanManager getBeanManager() {
        return beanMgr;
    }
    public void setUserSession(SessionWebBean userSession) {
        this.userSession = userSession;
        //initialize convenience variables
        securityRights = userSession.getSecurityRights();
    }
    public void setBeanMgr(BeanManager beanMgr) {
        this.beanMgr = beanMgr;
    }


    public boolean shouldDisplay(String displayItem) {
        boolean display = false;

        //use some reflection to get the int version
        try {
            Class condDisplayClass = this.getClass();
            Field displayItemField = condDisplayClass.getDeclaredField(displayItem);
            int intVal = displayItemField.getInt(this);
            display = shouldDisplay(intVal);
        }
        catch (Exception ex) {
            LOG.info("Problem getting conditional display integer value for " + displayItem);
            //and don't display
        }
        return display;
    }

    /**
     * The big kahuna - determine if a specific item should be displayed.
     *
     * Must of the standalone items are specifically implemented here,
     * more complex items with dependencies, requirements on orgs, etc
     * are calculated in sub methods.
     *
     * @param displayItem
     * @return
     */
    public boolean shouldDisplay(int displayItem) {
        switch (displayItem) {
            //new transactions
       
            case DISPLAY_NEW_INSTRUMENTS:
                return ( shouldDisplay(DISPLAY_NEW_TRADE_INSTRUMENTS) ||
                         shouldDisplay(DISPLAY_NEW_PAYMENT_INSTRUMENTS) ||
                         shouldDisplay(DISPLAY_NEW_DIRECT_DEBIT_INSTRUMENTS) );
            case DISPLAY_NEW_TRADE_INSTRUMENTS:
                return ( shouldDisplay(DISPLAY_TRANSFER_EXPORT_COLL) ||
                         shouldDisplay(DISPLAY_NEW_AIR_WAYBILL) ||
                         shouldDisplay(DISPLAY_NEW_APPROVAL_TO_PAY) ||
                         shouldDisplay(DISPLAY_NEW_DIRECT_SEND_COLL) ||
                         shouldDisplay(DISPLAY_NEW_EXPORT_COLL) ||
                         shouldDisplay(DISPLAY_NEW_LOAN_RQST) ||
                         shouldDisplay(DISPLAY_NEW_GUAR) ||
                         shouldDisplay(DISPLAY_NEW_SLC) ||
                         shouldDisplay(DISPLAY_NEW_SLC_DET) ||
                         shouldDisplay(DISPLAY_NEW_REQUEST_ADVISE) ||
                         shouldDisplay(DISPLAY_NEW_SHIP_GUAR) ||
                         shouldDisplay(DISPLAY_NEW_DLC) ||
                         //cquinton 2/25/2013 display trade if new int trade only
                         shouldDisplay(DISPLAY_NEW_INT_PAYMENT_TRADE) );
            case DISPLAY_TRANSFER_EXPORT_COLL:
                return (TradePortalConstants.INDICATOR_YES.equals(getOwnerOrg().getAttribute("allow_export_LC")) &&
                        SecurityAccess.hasRights(securityRights,SecurityAccess.EXPORT_LC_CREATE_MODIFY));
            case DISPLAY_NEW_AIR_WAYBILL:
                return (TradePortalConstants.INDICATOR_YES.equals(getOwnerOrg().getAttribute("allow_airway_bill")) &&
                        SecurityAccess.hasRights(securityRights,SecurityAccess.AIR_WAYBILL_CREATE_MODIFY));
            case DISPLAY_NEW_APPROVAL_TO_PAY:
                return (TradePortalConstants.INDICATOR_YES.equals(getOwnerOrg().getAttribute("allow_approval_to_pay")) &&
                        SecurityAccess.hasRights(securityRights,SecurityAccess.APPROVAL_TO_PAY_CREATE_MODIFY));
            //cquinton 2/25/2013 fix naming issues -- 'direct send collections' refers to export collections.
            // 'export collections' refers to new export collections
            //the term 'NEW' in the constants here means it is a new transaction!
            case DISPLAY_NEW_DIRECT_SEND_COLL:
                return (TradePortalConstants.INDICATOR_YES.equals(getOwnerOrg().getAttribute("allow_export_collection")) &&
                        SecurityAccess.hasRights(securityRights,SecurityAccess.EXPORT_COLL_CREATE_MODIFY));
            case DISPLAY_NEW_EXPORT_COLL:
                return (TradePortalConstants.INDICATOR_YES.equals(getOwnerOrg().getAttribute("allow_new_export_collection")) &&
                        SecurityAccess.hasRights(securityRights,SecurityAccess.NEW_EXPORT_COLL_CREATE_MODIFY));
            case DISPLAY_NEW_DLC:
                return (TradePortalConstants.INDICATOR_YES.equals(getOwnerOrg().getAttribute("allow_import_DLC")) &&
                        SecurityAccess.hasRights(securityRights,SecurityAccess.DLC_CREATE_MODIFY));
            case DISPLAY_NEW_LOAN_RQST:
                return (TradePortalConstants.INDICATOR_YES.equals(getOwnerOrg().getAttribute("allow_loan_request")) &&
                        SecurityAccess.hasRights(securityRights,SecurityAccess.LOAN_RQST_CREATE_MODIFY));
            case DISPLAY_NEW_GUAR:
                return (TradePortalConstants.INDICATOR_YES.equals(getOwnerOrg().getAttribute("allow_guarantee")) &&
                        SecurityAccess.hasRights(securityRights,SecurityAccess.GUAR_CREATE_MODIFY));
            case DISPLAY_NEW_SLC_DET:
                return (TradePortalConstants.INDICATOR_YES.equals(getOwnerOrg().getAttribute("allow_SLC")) &&
                        SecurityAccess.hasRights(securityRights,SecurityAccess.SLC_CREATE_MODIFY) &&
                        (TradePortalConstants.INDICATOR_YES.equals(getOwnerOrg().getAttribute("standbys_use_either_form")) ||
                        TradePortalConstants.INDICATOR_YES.equals(getOwnerOrg().getAttribute("standbys_use_guar_form_only")))
                        );
            case DISPLAY_NEW_SLC:
                return (TradePortalConstants.INDICATOR_YES.equals(getOwnerOrg().getAttribute("allow_SLC")) &&
                        SecurityAccess.hasRights(securityRights,SecurityAccess.SLC_CREATE_MODIFY) &&
                        (TradePortalConstants.INDICATOR_YES.equals(getOwnerOrg().getAttribute("standbys_use_either_form")) ||
                        !TradePortalConstants.INDICATOR_YES.equals(getOwnerOrg().getAttribute("standbys_use_guar_form_only")))
                        );
            case DISPLAY_NEW_REQUEST_ADVISE:
                return (TradePortalConstants.INDICATOR_YES.equals(getOwnerOrg().getAttribute("allow_request_advise")) &&
                            SecurityAccess.hasRights(securityRights,SecurityAccess.REQUEST_ADVISE_CREATE_MODIFY));
            case DISPLAY_NEW_SHIP_GUAR:
                return (TradePortalConstants.INDICATOR_YES.equals(getOwnerOrg().getAttribute("allow_shipping_guar")) &&
                            SecurityAccess.hasRights(securityRights,SecurityAccess.SHIP_GUAR_CREATE_MODIFY));
            //cquinton 2/25/2013 note that display of new payment instruments DOES NOT
            // include international payments.  if only international payments is
            // displayable, it should be displayed under trade.
            // see DISPLAY_NEW_INT_PAYMENT_TRADE below
            case DISPLAY_NEW_PAYMENT_INSTRUMENTS:
                return ( shouldDisplay(DISPLAY_NEW_TRANSFER_BTW_ACCTS) ||
                         shouldDisplay(DISPLAY_NEW_DOMESTIC_PAYMENT));
            case DISPLAY_NEW_TRANSFER_BTW_ACCTS:
                return ( (TradePortalConstants.INDICATOR_YES.equals(getOwnerOrg().getAttribute("allow_xfer_btwn_accts")) ||
                              TradePortalConstants.INDICATOR_YES.equals(getOwnerOrg().getAttribute("allow_xfer_btwn_accts_panel")) ) &&
                             SecurityAccess.hasRights(securityRights,SecurityAccess.TRANSFER_CREATE_MODIFY));
            case DISPLAY_NEW_DOMESTIC_PAYMENT:
                return ( (TradePortalConstants.INDICATOR_YES.equals(getOwnerOrg().getAttribute("allow_domestic_payments")) ||
                              TradePortalConstants.INDICATOR_YES.equals(getOwnerOrg().getAttribute("allow_domestic_payments_panel")) ) &&
                             SecurityAccess.hasRights(securityRights,SecurityAccess.DOMESTIC_CREATE_MODIFY));
            case DISPLAY_NEW_INT_PAYMENT:
                return ( (TradePortalConstants.INDICATOR_YES.equals(getOwnerOrg().getAttribute("allow_funds_transfer")) ||
                              TradePortalConstants.INDICATOR_YES.equals(getOwnerOrg().getAttribute("allow_funds_transfer_panel")) ) &&
                             SecurityAccess.hasRights(securityRights,SecurityAccess.FUNDS_XFER_CREATE_MODIFY) );
            //T36000010464, 01/24 - Added this to display international payments options under trade section when we do not have TBA and payments
            case DISPLAY_NEW_INT_PAYMENT_TRADE:
                return (( (TradePortalConstants.INDICATOR_YES.equals(getOwnerOrg().getAttribute("allow_funds_transfer")) ||
                              TradePortalConstants.INDICATOR_YES.equals(getOwnerOrg().getAttribute("allow_funds_transfer_panel")) ) &&
                             SecurityAccess.hasRights(securityRights,SecurityAccess.FUNDS_XFER_CREATE_MODIFY) ) &&
                             !(shouldDisplay(DISPLAY_NEW_TRANSFER_BTW_ACCTS) ||     shouldDisplay(DISPLAY_NEW_DOMESTIC_PAYMENT))
                             );

            case DISPLAY_NEW_DIRECT_DEBIT_INSTRUMENTS:
                return ( shouldDisplay(DISPLAY_NEW_DDI) );
            case DISPLAY_NEW_DDI:
                return (TradePortalConstants.INDICATOR_YES.equals(getOwnerOrg().getAttribute("allow_direct_debit")) &&
                            SecurityAccess.hasRights(securityRights,SecurityAccess.DDI_CREATE_MODIFY) );

            //transactions
            case DISPLAY_TRANSACTIONS:
                return ( shouldDisplay(DISPLAY_TRADE_TRANSACTIONS) ||
                         shouldDisplay(DISPLAY_PAYMENT_TRANSACTIONS) ||
                         shouldDisplay(DISPLAY_RECEIVABLE_TRANSACTIONS) ||
                         shouldDisplay(DISPLAY_DIRECT_DEBIT_TRANSACTIONS) ||
                         shouldDisplay(DISPLAY_INVOICE_OFFERS) ||
                         shouldDisplay(DISPLAY_PAYABLES_TRANSACTIONS)||
                         shouldDisplay(DISPLAY_BILLING_TRANSACTIONS)); //IR 28792
            case DISPLAY_TRADE_TRANSACTIONS:
                return (getOwnerOrg().allowSomeTradeTransaction() &&
                        SecurityAccess.hasRights(securityRights, SecurityAccess.ACCESS_INSTRUMENTS_AREA));
           //IR-43001  //     SecurityAccess.canViewTradeInstrument(securityRights) );
            case DISPLAY_PAYMENT_TRANSACTIONS: 
                return (getOwnerOrg().allowCashManagement() && 
                        SecurityAccess.hasRights(securityRights, SecurityAccess.ACCESS_INSTRUMENTS_AREA));
							// ER 8.1.0.6 IR# T36000015700 - start
//                		&&  SecurityAccess.canViewCashManagement(securityRights) );
            case DISPLAY_RECEIVABLE_TRANSACTIONS:
                return (getOwnerOrg().allowSomeReceivablesTransaction() &&
                        SecurityAccess.hasRights(securityRights, SecurityAccess.ACCESS_RECEIVABLES_AREA) );
            case DISPLAY_DIRECT_DEBIT_TRANSACTIONS:
                return (getOwnerOrg().allowSomeDirectDebitTransaction() &&
                        SecurityAccess.hasRights(securityRights, SecurityAccess.ACCESS_DIRECTDEBIT_AREA) );
            case DISPLAY_CREATE_SETTLEMENT_INSTRUCTION:
                return  SecurityAccess.hasRights(securityRights, SecurityAccess.SIT_CREATE_MODIFY)  ;
            case DISPLAY_REQUEST_ROLLOVER:
                return SecurityAccess.hasRights(securityRights, SecurityAccess.SIT_CREATE_MODIFY) ;           
            //Ravindra - CR-708B - Start
            case DISPLAY_INVOICE_OFFERS:
            	return ( shouldDisplay(DISPLAY_INVOICE_OFFERED) ||
                        shouldDisplay(DISPLAY_FUTURE_VALUE_OFFERS) ||
                        shouldDisplay(DISPLAY_INVOICE_OFFERS_HISTORY) );
            case DISPLAY_INVOICE_OFFERED:
                return (getOwnerOrg().allowSomeInvoiceOffersTransaction() &&
                        SecurityAccess.hasRights(securityRights, SecurityAccess.SP_INVOICE_OFFERED) );
            case DISPLAY_FUTURE_VALUE_OFFERS:
                return (getOwnerOrg().allowSomeInvoiceOffersTransaction() &&
                        SecurityAccess.hasRights(securityRights, SecurityAccess.SP_INVOICE_OFFERED) );
            case DISPLAY_INVOICE_OFFERS_HISTORY:
                return (getOwnerOrg().allowSomeInvoiceOffersTransaction() &&
                        SecurityAccess.hasRights(securityRights, SecurityAccess.SP_HISTORY) );
            case DISPLAY_SP_ACCEPT_OFFER:
				return (SecurityAccess.hasRights(securityRights, SecurityAccess.SP_ACCEPT_OFFER));
            case DISPLAY_SP_DECLINE_OFFER:
		    	return (SecurityAccess.hasRights(securityRights, SecurityAccess.SP_DECLINE_OFFER)
                                && !(userSession.hasSavedUserSession() 
                                     && userSession.getSavedUserSessionSecurityType().equals(TradePortalConstants.ADMIN)));
	    case DISPLAY_SP_AUTHORIZE_OFFER:
	        	return (SecurityAccess.hasRights(securityRights, SecurityAccess.SP_AUTHORIZE_OFFER)
                                && !(userSession.hasSavedUserSession() 
                                     && userSession.getSavedUserSessionSecurityType().equals(TradePortalConstants.ADMIN)));
            case DISPLAY_SP_ASSIGN_FVD:
            	return (SecurityAccess.hasRights(securityRights, SecurityAccess.SP_ASSIGN_FVD));
            case DISPLAY_SP_REMOVE_FVD:
               	return (SecurityAccess.hasRights(securityRights, SecurityAccess.SP_REMOVE_FVD));
            case DISPLAY_SP_REMOVE_INVOICE:
              	return (SecurityAccess.hasRights(securityRights, SecurityAccess.SP_REMOVE_INVOICE));
            case DISPLAY_SP_RESET_TO_OFFERED:
                return (SecurityAccess.hasRights(securityRights, SecurityAccess.SP_RESET_TO_OFFERED));
             //Ravindra - CR-708B - End

                //CR 913 start
            case DISPLAY_PAYABLES_TRANSACTIONS:
                return (getOwnerOrg().allowPayablesTransaction() &&
                        SecurityAccess.hasRights(securityRights, SecurityAccess.ACCESS_RECEIVABLES_AREA) );
                //CR 913 end
                //CR 912 start
            case DISPLAY_BILLING_TRANSACTIONS:
                return (TradePortalConstants.INDICATOR_YES.equals(getOwnerOrg().getAttribute("enable_billing_instruments")));
                //CR 912 end   
                

            //accounts
            case DISPLAY_ACCOUNTS:
                return ( shouldDisplay(DISPLAY_ACCOUNT_BALANCES) );
            case DISPLAY_ACCOUNT_BALANCES:
                //cquinton 2/25/2013 - use same rights as 'DISPLAY_PAYMENT_TRANSACTIONS'
                return (getOwnerOrg().allowCashManagement() && 
                        SecurityAccess.hasRights(securityRights, SecurityAccess.ACCESS_INSTRUMENTS_AREA));
							// ER 8.1.0.6 IR# T36000015700 - start
//                		&&  SecurityAccess.canViewCashManagement(securityRights) );
                
            //reports
            case DISPLAY_REPORTS:
                return ( shouldDisplay(DISPLAY_STANDARD_REPORTS) ||
                         shouldDisplay(DISPLAY_CUSTOM_REPORTS) );
            case DISPLAY_STANDARD_REPORTS:
                return ( SecurityAccess.hasRights( securityRights, SecurityAccess.ACCESS_REPORTS_AREA ) &&
                         SecurityAccess.hasRights( securityRights, SecurityAccess.VIEW_OR_MAINTAIN_STANDARD_REPORT ) );
            case DISPLAY_CUSTOM_REPORTS:
                return ( SecurityAccess.hasRights( securityRights, SecurityAccess.ACCESS_REPORTS_AREA ) &&
                         SecurityAccess.hasRights( securityRights, SecurityAccess.VIEW_OR_MAINTAIN_CUSTOM_REPORT ) );

            //refdata
            case DISPLAY_REFDATA:
                return ( 
                		//MEerupula Rel8.3 CR 776
                		 shouldDisplay(DISPLAY_MYORGPROFILE) ||
                		 shouldDisplay(DISPLAY_PARTY) ||
                         shouldDisplay(DISPLAY_PHRASE) ||
                         shouldDisplay(DISPLAY_THRESH_GRP) ||
                         shouldDisplay(DISPLAY_PAYMENT_TEMPLATE_GRP) ||
                         shouldDisplay(DISPLAY_TEMPLATE) ||
                         shouldDisplay(DISPLAY_USERS) ||
                         shouldDisplay(DISPLAY_SEC_PROF) ||
                         shouldDisplay(DISPLAY_FX_RATE) ||
                         shouldDisplay(DISPLAY_CREATE_RULE) ||
                         shouldDisplay(DISPLAY_PO_UPLOAD_DEFN) ||
                         shouldDisplay(DISPLAY_RECEIVABLES_MATCH_RULES) ||
                         shouldDisplay(DISPLAY_PANEL_AUTH_GRP) ||
                         shouldDisplay(DISPLAY_WORK_GROUPS) ||
                         shouldDisplay(DISPLAY_INV_UPLOAD_DEFN) ||
                         shouldDisplay(DISPLAY_INTEREST_DISCOUNT_RATE) ||
                         //AAlubala - Rel8.2 CR741
                         shouldDisplay(DISPLAY_ERP_GL_CODES) ||
                         shouldDisplay(DISPLAY_DISCOUNT_CODES) ||
                         //CR741 - END
                         shouldDisplay(DISPLAY_INV_CREATE_RULE) ||
                         shouldDisplay(DISPLAY_APPROVE_REF_DATA));
            case DISPLAY_MYORGPROFILE:
                return (SecurityAccess.hasRights(securityRights,SecurityAccess.VIEW_OR_MAINTAIN_ORG_PROFILE));    
            case DISPLAY_PARTY:
                return (SecurityAccess.hasRights(securityRights,SecurityAccess.VIEW_OR_MAINTAIN_PARTIES));
            case DISPLAY_PHRASE:
                return (SecurityAccess.hasRights(securityRights,SecurityAccess.VIEW_OR_MAINTAIN_PHRASE));
            case DISPLAY_THRESH_GRP:
                return (SecurityAccess.hasRights(securityRights,SecurityAccess.VIEW_OR_MAINTAIN_THRESH_GRP));
            case DISPLAY_PAYMENT_TEMPLATE_GRP:
                return (SecurityAccess.hasRights(securityRights,SecurityAccess.VIEW_OR_MAINTAIN_PAYMENT_TEMPLATE_GRP));
            case DISPLAY_TEMPLATE:
                 // Global org users cannot access templates
                return ( !userSession.getOwnershipLevel().equals(TradePortalConstants.OWNER_GLOBAL) &&
                         SecurityAccess.hasRights(securityRights,SecurityAccess.VIEW_OR_MAINTAIN_TEMPLATE));
            case DISPLAY_USERS:
                return (SecurityAccess.hasRights(securityRights,SecurityAccess.VIEW_OR_MAINTAIN_USERS));
            case DISPLAY_SEC_PROF:
                return (SecurityAccess.hasRights(securityRights,SecurityAccess.VIEW_OR_MAINTAIN_SEC_PROF));
            case DISPLAY_FX_RATE:
                return (SecurityAccess.hasRights(securityRights,SecurityAccess.VIEW_OR_MAINTAIN_FX_RATE));
            case DISPLAY_CREATE_RULE:
                return (SecurityAccess.hasRights(securityRights,SecurityAccess.VIEW_OR_MAINTAIN_LC_CREATE_RULES) ||
                        SecurityAccess.hasRights(securityRights,SecurityAccess.VIEW_OR_MAINTAIN_ATP_CREATE_RULES));
            case DISPLAY_PO_UPLOAD_DEFN:
                return (SecurityAccess.hasRights(securityRights,SecurityAccess.VIEW_OR_MAINTAIN_PO_UPLOAD_DEFN));
            case DISPLAY_INV_UPLOAD_DEFN:
                return (SecurityAccess.hasRights(securityRights,SecurityAccess.VIEW_OR_MAINTAIN_INVOICE_DEFINITION));
            case DISPLAY_RECEIVABLES_MATCH_RULES:
                return (SecurityAccess.hasRights(securityRights,SecurityAccess.VIEW_OR_MAINTAIN_RECEIVABLES_MATCH_RULES));
            case DISPLAY_PANEL_AUTH_GRP:
                return (!userSession.isUsingSubsidiaryAccess() &&
                        SecurityAccess.hasRights(securityRights,SecurityAccess.VIEW_OR_MAINTAIN_PANEL_AUTH_GRP));
            case DISPLAY_WORK_GROUPS:
                return (!userSession.isUsingSubsidiaryAccess() &&
                        SecurityAccess.hasRights(securityRights,SecurityAccess.VIEW_OR_MAINTAIN_WORK_GROUPS));
            case DISPLAY_INTEREST_DISCOUNT_RATE:
            	return (SecurityAccess.hasRights(securityRights,SecurityAccess.VIEW_OR_MAINTAIN_INTEREST_RATE));
            //AAlubala - Rel8.2 CR741
            case DISPLAY_ERP_GL_CODES:
                return (!userSession.isUsingSubsidiaryAccess() &&
                        SecurityAccess.hasRights(securityRights,SecurityAccess.VIEW_OR_MAINTAIN_ERP_GL_CODES));
            case DISPLAY_DISCOUNT_CODES:
            	return (SecurityAccess.hasRights(securityRights,SecurityAccess.VIEW_OR_MAINTAIN_DISCOUNT_CODES));
            //CR741 - END
            case DISPLAY_INV_CREATE_RULE:
                return (SecurityAccess.hasRights(securityRights,SecurityAccess.VIEW_OR_MAINTAIN_LR_CREATE_RULES) ||
                        SecurityAccess.hasRights(securityRights,SecurityAccess.VIEW_OR_MAINTAIN_ATP_INV_CREATE_RULES));
              //jgadela  Rel8.3 CR-501 04/08/2013 START
            case DISPLAY_APPROVE_REF_DATA:
            	if (userSession.hasSavedUserSession() && TradePortalConstants.ADMIN.equals(userSession.getSavedUserSessionSecurityType()) &&
            			TradePortalConstants.INDICATOR_YES.equals(getCBCResult().getAttribute("/ResultSetRecord(0)/CORP_USER_DUAL_CTRL_REQD_IND"))) {
            		return SecurityAccess.hasRights(securityRights,SecurityAccess.MAINTAIN_USERS);
            	}
            	//IR 22260 - when doing customer access it should be checking for DISPLAY_APPROVE_REF_DATA_ADMIN
            	else if (userSession.hasSavedUserSession() && TradePortalConstants.ADMIN.equals(userSession.getSavedUserSessionSecurityType())) {
            	return (((SecurityAccess.hasRights(userSession.getSavedUserSession().getSecurityRights(),SecurityAccess.MAINTAIN_ADM_USERS) ||
                    		(TradePortalConstants.OWNER_BANK.equals(userSession.getSavedUserSession().getOwnershipLevel()) &&
                    				SecurityAccess.hasRights(userSession.getSavedUserSession().getSecurityRights(),SecurityAccess.MAINTAIN_CORP_BY_CB)) ||
                            (TradePortalConstants.OWNER_BOG.equals(userSession.getSavedUserSession().getOwnershipLevel()) && 
                            		SecurityAccess.hasRights(userSession.getSavedUserSession().getSecurityRights(),SecurityAccess.MAINTAIN_CORP_BY_BG))) && 
                            		// Nar IR-T36000021754 Rel8.4 Added condition for Admin User also for Dual control check for corporate customer data access
                            		// as same added for else condition also for corporate user.
                            		(TradePortalConstants.INDICATOR_YES.equals(getOwnerOrg().getAttribute("corp_user_dual_ctrl_reqd_ind")) || 
                            		  TradePortalConstants.INDICATOR_YES.equals(getOwnerOrg().getAttribute("panel_auth_dual_ctrl_reqd_ind"))) )
                            	//do not check dual control for admin users	
                            	/*	&&
                            		(TradePortalConstants.INDICATOR_YES.equals(getCBCResult().getAttribute("/ResultSetRecord(0)/CORP_ORG_DUAL_CTRL_REQD_IND")) ||
                            		 TradePortalConstants.INDICATOR_YES.equals(getCBCResult().getAttribute("/ResultSetRecord(0)/ADMIN_USER_DUAL_CTRL_REQD_IND")) || 
                            		 TradePortalConstants.INDICATOR_YES.equals(getCBCResult().getAttribute("/ResultSetRecord(0)/CORP_USER_DUAL_CTRL_REQD_IND"))) */
                            		 );
            	}
            	else{
            	return ((SecurityAccess.hasRights(securityRights,SecurityAccess.MAINTAIN_USERS) && 
            			   TradePortalConstants.INDICATOR_YES.equals(getOwnerOrg().getAttribute("corp_user_dual_ctrl_reqd_ind"))) || 
            			(!userSession.isUsingSubsidiaryAccess() &&
                        SecurityAccess.hasRights(securityRights,SecurityAccess.MAINTAIN_PANEL_AUTH_GRP) && 
                        TradePortalConstants.INDICATOR_YES.equals(getOwnerOrg().getAttribute("panel_auth_dual_ctrl_reqd_ind"))));
            	}
            //jgadela  Rel8.3 CR-501 04/08/2013 END  

            //upload center
            case DISPLAY_UPLOAD_CENTER:
                return ( shouldDisplay(DISPLAY_PURCHASE_ORDER_LIST) ||
                         shouldDisplay(DISPLAY_PAYMENT_FILE_UPLOAD) ||
                        // shouldDisplay(DISPLAY_INVOICE_GROUP) ||
                        // shouldDisplay(DISPLAY_INVOICE_LIST) ||
                         shouldDisplay(DISPLAY_INVOICE_MANAGEMENT));

            case DISPLAY_PURCHASE_ORDER_LIST:
                //Should display *only* if
                // both the corporate org and user have the right to process purchase
                // orders OR if the corporate org has the right and the user is an admin
                // user acting on behalf of the corporate org
                boolean canProcessPOForDLC = SecurityAccess.canProcessPurchaseOrder(securityRights, InstrumentType.IMPORT_DLC);
                boolean canProcessPOForATP = SecurityAccess.canProcessPurchaseOrder(securityRights, InstrumentType.APPROVAL_TO_PAY);
                return ((TradePortalConstants.INDICATOR_YES.equals(userSession.getOrgAutoLCCreateIndicator()) &&
                        (canProcessPOForDLC ||
                         (userSession.hasSavedUserSession() &&
                          userSession.getSavedUserSessionSecurityType().equals(TradePortalConstants.ADMIN)))) ||
                        (TradePortalConstants.INDICATOR_YES.equals(userSession.getOrgAutoATPCreateIndicator()) &&
                         (canProcessPOForATP ||
                          (userSession.hasSavedUserSession() &&
                           userSession.getSavedUserSessionSecurityType().equals(TradePortalConstants.ADMIN)))));

            /*KMehta  Rel 8400 IR-T36000015989 on 4 Mar 2014 Start*/
            case DISPLAY_TEXT_PURCHASE_ORDER:
                /*return (TradePortalConstants.PO_UPLOAD_TEXT.equals(getOwnerOrg().getAttribute("po_upload_format")) &&
                                                SecurityAccess.hasRights(securityRights, SecurityAccess.VIEW_OR_MAINTAIN_PO_UPLOAD_DEFN));*/
                return (TradePortalConstants.PO_UPLOAD_TEXT.equals(getOwnerOrg().getAttribute("po_upload_format")) &&
                    	(TradePortalConstants.INDICATOR_YES.equals(getOwnerOrg().getAttribute("allow_purchase_order_upload"))) );
                
            case DISPLAY_STRUCTURE_PURCHASE_ORDER:
            /*    return (TradePortalConstants.PO_UPLOAD_STRUCTURED.equals(getOwnerOrg().getAttribute("po_upload_format")) &&
                                                SecurityAccess.hasRights(securityRights, SecurityAccess.VIEW_OR_MAINTAIN_PO_UPLOAD_DEFN));*/
                return (TradePortalConstants.PO_UPLOAD_STRUCTURED.equals(getOwnerOrg().getAttribute("po_upload_format")) &&
                    	(TradePortalConstants.INDICATOR_YES.equals(getOwnerOrg().getAttribute("allow_purchase_order_upload"))) );
                
                /*KMehta  Rel 8400 IR-T36000015989 on 4 Mar 2014 End*/            	
            	
            case DISPLAY_PAYMENT_FILE_UPLOAD:
                return ( (SecurityAccess.hasRights(securityRights, SecurityAccess.DOMESTIC_UPLOAD_BULK_FILE)) &&
                         (TradePortalConstants.INDICATOR_YES.equals(getOwnerOrg().getAttribute("allow_domestic_payments"))) &&
                         (TradePortalConstants.INDICATOR_YES.equals(getOwnerOrg().getAttribute("allow_payment_file_upload"))) &&
                         ((SecurityAccess.hasRights(securityRights, SecurityAccess.DOMESTIC_AUTHORIZE)) ||
                          (SecurityAccess.hasRights(securityRights, SecurityAccess.DOMESTIC_CREATE_MODIFY)) ||
                          (SecurityAccess.hasRights(securityRights, SecurityAccess.DOMESTIC_DELETE)) ||
                          (SecurityAccess.hasRights(securityRights, SecurityAccess.DOMESTIC_ROUTE))) );
            case DISPLAY_INVOICE_GROUP:
                return ((TradePortalConstants.INVOICES_GROUPED_TPR.equals(getOwnerOrg().getAttribute("allow_grouped_invoice_upload"))));
            
             // CR 913 -Display Groups/List page when VIEW_UPLOADED_INVOICES is allowed -->
            case DISPLAY_INVOICE_LIST:
            	 return SecurityAccess.hasRights(securityRights,
                        SecurityAccess.VIEW_UPLOADED_INVOICES) 
                        || TradePortalConstants.INDICATOR_YES.equals(getOwnerOrg().getAttribute("allow_h2h_rec_inv_approval"))
                        || TradePortalConstants.INDICATOR_YES.equals(getOwnerOrg().getAttribute("allow_h2h_pay_inv_approval"))
                        || TradePortalConstants.INDICATOR_YES.equals(getOwnerOrg().getAttribute("allow_h2h_pay_crn_approval"));
               // return ((TradePortalConstants.INVOICES_NOT_GROUPED_TPR.equals(getOwnerOrg().getAttribute("allow_h2h_rec_inv_approval"))));
            case DISPLAY_INVOICE_UPLOAD:
                return ((TradePortalConstants.INDICATOR_YES.equals(getOwnerOrg().getAttribute("allow_invoice_file_upload"))) && SecurityAccess.hasRights(securityRights, SecurityAccess.UPLOAD_INVOICES));

         /*   case DISPLAY_INVOICE_MANAGEMENT:
                return ((shouldDisplay(DISPLAY_INVOICE_GROUP) ||
                         shouldDisplay(DISPLAY_INVOICE_LIST)) && SecurityAccess.hasRights(securityRights, SecurityAccess.DOMESTIC_AUTHORIZE));*/
            case DISPLAY_INVOICE_MANAGEMENT:
                return (((SecurityAccess.hasRights(securityRights, SecurityAccess.UPLOAD_INVOICES) || SecurityAccess.hasRights(securityRights,
                        SecurityAccess.VIEW_UPLOADED_INVOICES)) && SecurityAccess.hasRights(securityRights, SecurityAccess.ACCESS_INVOICE_MGT_AREA) && TradePortalConstants.INDICATOR_YES.equals(getOwnerOrg().getAttribute("allow_invoice_file_upload")))
                        || TradePortalConstants.INDICATOR_YES.equals(getOwnerOrg().getAttribute("allow_h2h_rec_inv_approval"))
                        || TradePortalConstants.INDICATOR_YES.equals(getOwnerOrg().getAttribute("allow_h2h_pay_inv_approval"))
                        || TradePortalConstants.INDICATOR_YES.equals(getOwnerOrg().getAttribute("allow_h2h_pay_crn_approval")));

            //messages
            case DISPLAY_MESSAGES:
                return ( shouldDisplay(DISPLAY_MAIL_MESSAGES) ||
                         shouldDisplay(DISPLAY_NOTIFICATIONS) );
            case DISPLAY_MAIL_MESSAGES:
                return SecurityAccess.hasRights(securityRights, SecurityAccess.ACCESS_MESSAGES_AREA);
            case DISPLAY_NOTIFICATIONS:
                return SecurityAccess.hasRights(securityRights, SecurityAccess.ACCESS_MESSAGES_AREA);
            //CR 913 start
            case DISPLAY_DEBIT_FUNDING_MAIL:
                return SecurityAccess.hasRights(securityRights, SecurityAccess.ACCESS_MESSAGES_AREA);
            //CR 913 end   
            //admin menu stuff
            case DISPLAY_ADMIN_ORGS:
                return (SecurityAccess.hasRights( securityRights, SecurityAccess.ACCESS_ORGANIZATION_AREA) &&
                        ( shouldDisplay(DISPLAY_CLIENT_BANKS) ||
                          shouldDisplay(DISPLAY_BANK_GROUPS) ||
                          shouldDisplay(DISPLAY_OP_ORGS) ||
                          shouldDisplay(DISPLAY_CORP_ORGS)|| 
                          shouldDisplay(DISPLAY_EXTERNAL_BANKS) 
                        ) );
                
            case DISPLAY_CLIENT_BANKS:
  
                return (TradePortalConstants.OWNER_GLOBAL.equals(userSession.getOwnershipLevel()) &&
                        SecurityAccess.hasEitherRight( securityRights,
                          SecurityAccess.VIEW_CB_BY_PX,SecurityAccess.MAINTAIN_CB_BY_PX ) );
  
             	
            case DISPLAY_EXTERNAL_BANKS:
            	
                return (TradePortalConstants.OWNER_BANK.equals(userSession.getOwnershipLevel())&&
            		   SecurityAccess.hasEitherRight(securityRights, SecurityAccess.VIEW_EXTERNAL_BANK,SecurityAccess.MAINTAIN_EXTERNAL_BANK));
                		
            case DISPLAY_BANK_GROUPS:
                return (TradePortalConstants.OWNER_BANK.equals(userSession.getOwnershipLevel()) &&
                        SecurityAccess.hasEitherRight( securityRights,
                          SecurityAccess.VIEW_BG_BY_CB, SecurityAccess.MAINTAIN_BG_BY_CB) );
            case DISPLAY_OP_ORGS:
                return (TradePortalConstants.OWNER_BANK.equals(userSession.getOwnershipLevel()) &&
                        SecurityAccess.hasEitherRight( securityRights,
                          SecurityAccess.VIEW_OPBANK_BY_CB, SecurityAccess.MAINTAIN_OPBANK_BY_CB) );
            case DISPLAY_CORP_ORGS:
                return ( ( TradePortalConstants.OWNER_BANK.equals(userSession.getOwnershipLevel()) &&
                           SecurityAccess.hasEitherRight( securityRights,
                             SecurityAccess.VIEW_CORP_BY_CB, SecurityAccess.MAINTAIN_CORP_BY_CB) ) ||
                         ( TradePortalConstants.OWNER_BOG.equals(userSession.getOwnershipLevel()) &&
                           SecurityAccess.hasEitherRight( securityRights,
                             SecurityAccess.VIEW_CORP_BY_BG, SecurityAccess.MAINTAIN_CORP_BY_BG) ) );

            case DISPLAY_ADMIN_USERS_AND_SECURITY:
                return ( SecurityAccess.hasRights( securityRights,
                           SecurityAccess.ACCESS_ADM_REFDATA_AREA ) &&
                        ( shouldDisplay(DISPLAY_ADMIN_USERS) ||
                          shouldDisplay(DISPLAY_ADMIN_SEC_PROF) ||
                          shouldDisplay(DISPLAY_LOCKED_OUT_USERS) ) );
            case DISPLAY_ADMIN_USERS:
                return ( SecurityAccess.hasRights( securityRights,
                         SecurityAccess.VIEW_OR_MAINTAIN_ADM_USERS) );
            case DISPLAY_ADMIN_SEC_PROF:
                return ( SecurityAccess.hasRights( securityRights,
                         SecurityAccess.VIEW_OR_MAINTAIN_ADM_SEC_PROF ) );
            case DISPLAY_LOCKED_OUT_USERS:
                return ( SecurityAccess.hasRights( securityRights,
                         SecurityAccess.VIEW_OR_MAINTAIN_LOCKED_OUT_USERS ) );
            case DISPLAY_ADMIN_ANNOUNCEMENTS:
                return ( SecurityAccess.hasRights( securityRights,
                         SecurityAccess.VIEW_OR_MAINTAIN_ANNOUNCEMENT ) );

            case DISPLAY_ADMIN_REFDATA:
                return ( SecurityAccess.hasRights( securityRights, SecurityAccess.ACCESS_REFDATA_AREA) &&
                         ( shouldDisplay(DISPLAY_PARTY) ||
                           shouldDisplay(DISPLAY_PHRASE) ||
                           shouldDisplay(DISPLAY_PAYMENT_TEMPLATE_GRP) ||
                           shouldDisplay(DISPLAY_TEMPLATE) ||
                           shouldDisplay(DISPLAY_SEC_PROF) ||
                           shouldDisplay(DISPLAY_FX_RATE) ||
                           shouldDisplay(DISPLAY_GM_CAT_GRP) ||
                           shouldDisplay(DISPLAY_CROSS_RATE_RULE) ||
                           shouldDisplay(DISPLAY_BANK_BRANCH_RULES) ||
                           shouldDisplay(DISPLAY_PAYMENT_METH_VAL) ||
                           shouldDisplay(DISPLAY_CALENDAR) ||
                           shouldDisplay(DISPLAY_CURRENCY_CALENDAR_RULES) ||
                           shouldDisplay(DISPLAY_APPROVE_REF_DATA_ADMIN) ||
                           shouldDisplay(DISPLAY_BANK_GROUP_RESTRICT_RULES) || //pgedupudi - Rel9.3 CR976A 03/09/2015
                           shouldDisplay(DISPLAY_PAYMENT_METH_DEFN) ) ); 	//RKAZI REL 8.4 CR-599 11/20/2013 - ADD - DISPLAY_PAYMENT_METH_DEFN 
            //note: some ref data display items are shared with non-admin
            //Added for Rel9.5 CR 927B. Based on Client Bank setting, the customer may or may not be able to see Notification Rules
            case DISPLAY_NOTIFICATION_RULES:
            	if(userSession.hasSavedUserSession() && TradePortalConstants.ADMIN.equals(userSession.getSavedUserSessionSecurityType()))
            		return (SecurityAccess.hasRights( securityRights,SecurityAccess.VIEW_OR_MAINTAIN_NOTIFICATION_RULE));
            	else {
            		if(TradePortalConstants.INDICATOR_NO.equals(getCBCResult().getAttribute("/ResultSetRecord(0)/CONTROL_NOTIFRULE_BYADMIN")))
            			return (SecurityAccess.hasRights( securityRights,SecurityAccess.VIEW_OR_MAINTAIN_NOTIFICATION_RULE));
            	}
            case DISPLAY_NOTIF_RULE_TEMPLATES:
                return (SecurityAccess.hasRights( securityRights,SecurityAccess.VIEW_OR_MAINTAIN_NOTIF_RULE_TEMPLATES));

            case DISPLAY_GM_CAT_GRP:
                return (SecurityAccess.hasRights( securityRights,SecurityAccess.VIEW_OR_MAINTAIN_GM_CAT_GRP));
            case DISPLAY_CROSS_RATE_RULE:
                return (SecurityAccess.hasRights( securityRights,SecurityAccess.VIEW_OR_MAINTAIN_CROSS_RATE_RULE));
            case DISPLAY_BANK_BRANCH_RULES:
                return (SecurityAccess.hasRights( securityRights,SecurityAccess.VIEW_OR_MAINTAIN_BANK_BRANCH_RULES));
            case DISPLAY_PAYMENT_METH_VAL:
                return (SecurityAccess.hasRights( securityRights,SecurityAccess.VIEW_OR_MAINTAIN_PAYMENT_METH_VAL));
            case DISPLAY_CALENDAR:
                // Only ASP Admin user can view/maintian Calendars
                return ( TradePortalConstants.OWNER_GLOBAL.equals(userSession.getOwnershipLevel()) &&
                         SecurityAccess.hasRights( securityRights,SecurityAccess.VIEW_OR_MAINTAIN_CALENDAR));
            case DISPLAY_CURRENCY_CALENDAR_RULES:
                // Only ASP Admin user can view/maintian Calendars Rules
                return ( TradePortalConstants.OWNER_GLOBAL.equals(userSession.getOwnershipLevel()) &&
                         SecurityAccess.hasRights( securityRights,SecurityAccess.VIEW_OR_MAINTAIN_CURRENCY_CALENDAR_RULES));
                /*KMehta Rel 8.4 IR T36000021754 Start*/
                //jgadela - Rel8.3 CR501 04/08/2013 SRART
           /* case DISPLAY_APPROVE_REF_DATA_ADMIN:
                return ((SecurityAccess.hasRights(securityRights,SecurityAccess.MAINTAIN_ADM_USERS) ||
                		(TradePortalConstants.OWNER_BANK.equals(userSession.getOwnershipLevel()) &&
                				SecurityAccess.hasRights(securityRights,SecurityAccess.MAINTAIN_CORP_BY_CB)) ||
                        (TradePortalConstants.OWNER_BOG.equals(userSession.getOwnershipLevel()) && 
                        		SecurityAccess.hasRights(securityRights,SecurityAccess.MAINTAIN_CORP_BY_BG))) &&
                        		(TradePortalConstants.INDICATOR_YES.equals(getCBCResult().getAttribute("/ResultSetRecord(0)/CORP_ORG_DUAL_CTRL_REQD_IND")) ||
                        		 TradePortalConstants.INDICATOR_YES.equals(getCBCResult().getAttribute("/ResultSetRecord(0)/ADMIN_USER_DUAL_CTRL_REQD_IND")) || 
                        		 TradePortalConstants.INDICATOR_YES.equals(getCBCResult().getAttribute("/ResultSetRecord(0)/CORP_USER_DUAL_CTRL_REQD_IND"))));*/
            //jgadela - Rel8.3 CR501 04/08/2013 END 

            case DISPLAY_APPROVE_REF_DATA_ADMIN: //T36000024596 This option is not available to ASP Bank admin,
            	//hence added condition !TradePortalConstants.OWNER_GLOBAL.equals(userSession.getOwnershipLevel())
            	//Rel9.2 IR#T36000030864 - Corrected conditions
                return (!TradePortalConstants.OWNER_GLOBAL.equals(userSession.getOwnershipLevel())&&(SecurityAccess.hasRights(securityRights,SecurityAccess.MAINTAIN_ADM_USERS) &&
                		TradePortalConstants.INDICATOR_YES.equals(getCBCResult().getAttribute("/ResultSetRecord(0)/ADMIN_USER_DUAL_CTRL_REQD_IND"))) ||
						(TradePortalConstants.OWNER_BANK.equals(userSession.getOwnershipLevel()) &&
                				(SecurityAccess.hasRights(securityRights,SecurityAccess.MAINTAIN_CORP_BY_CB) &&
								TradePortalConstants.INDICATOR_YES.equals(getCBCResult().getAttribute("/ResultSetRecord(0)/CORP_ORG_DUAL_CTRL_REQD_IND")))) ||
                        (TradePortalConstants.OWNER_BOG.equals(userSession.getOwnershipLevel()) && 
                        		(SecurityAccess.hasRights(securityRights,SecurityAccess.MAINTAIN_CORP_BY_BG) &&
								TradePortalConstants.INDICATOR_YES.equals(getCBCResult().getAttribute("/ResultSetRecord(0)/CORP_USER_DUAL_CTRL_REQD_IND")))));
                /*KMehta Rel 8.4 IR T36000021754 Ends*/
                /*pgedupudi - Rel9.3 CR976A 03/09/2015 START*/
            case DISPLAY_BANK_GROUP_RESTRICT_RULES: 
            	return (!TradePortalConstants.OWNER_GLOBAL.equals(userSession.getOwnershipLevel()) && 
            			(TradePortalConstants.OWNER_BANK.equals(userSession.getOwnershipLevel()) 
            					&& SecurityAccess.hasRights(securityRights,SecurityAccess.VIEW_OR_MAINTAIN_BANK_GROUP_RESTRICT_RULES)));
            	/*pgedupudi - Rel9.3 CR976A 03/09/2015 END*/
                

            //grid footer actions
            case DISPLAY_TRADE_AUTHORIZE_ACTION:
                return SecurityAccess.hasRights(securityRights, SecurityAccess.INSTRUMENT_AUTHORIZE);
            case DISPLAY_TRADE_DELETE_ACTION:
                return SecurityAccess.hasRights(securityRights, SecurityAccess.INSTRUMENT_DELETE);
            case DISPLAY_TRADE_ROUTE_ACTION:
                return SecurityAccess.hasRights(securityRights, SecurityAccess.INSTRUMENT_ROUTE);
            case DISPLAY_DDI_DELETE_ACTION:
                return SecurityAccess.hasRights(securityRights, SecurityAccess.DDI_DELETE);
            case DISPLAY_DDI_ROUTE_ACTION:
                return SecurityAccess.hasRights(securityRights, SecurityAccess.DDI_ROUTE);
            case DISPLAY_EDIT_FX_RATE_ACTION:
                return (SecurityAccess.hasRights(securityRights, SecurityAccess.MAINTAIN_FX_RATE));
            case DISPLAY_APPROVE_FIN_ACTION:
                return (SecurityAccess.hasRights(securityRights, SecurityAccess.RECEIVABLES_APPROVE_FINANCING) &&
                (!(userSession.hasSavedUserSession() &&
                        userSession.getSavedUserSessionSecurityType().equals(TradePortalConstants.ADMIN))));
            case DISPLAY_AUTH_INV_ACTION:
                return (SecurityAccess.hasRights(securityRights, SecurityAccess.RECEIVABLES_AUTHORIZE_INVOICE) &&
                                                (!(userSession.hasSavedUserSession() &&
                                userSession.getSavedUserSessionSecurityType().equals(TradePortalConstants.ADMIN))));
            case DISPLAY_REMOVE_INV_ACTION:
                return (SecurityAccess.hasRights(securityRights, SecurityAccess.RECEIVABLES_REMOVE_INVOICE)&&
                (!(userSession.hasSavedUserSession() &&
                        userSession.getSavedUserSessionSecurityType().equals(TradePortalConstants.ADMIN))));
            case DISPLAY_DELETE_INV_ACTION:
                return (SecurityAccess.hasRights(securityRights, SecurityAccess.RECEIVABLES_DELETE_INVOICE));
            case DISPLAY_APPROVE_AND_AUTH_ACTION:
                return (SecurityAccess.hasRights(securityRights, SecurityAccess.RECEIVABLES_APPROVE_FINANCING) ||
                                                SecurityAccess.hasRights(securityRights, SecurityAccess.RECEIVABLES_AUTHORIZE_INVOICE) );
            case DISPLAY_PROXY_AUTH_ACTION:
                return (SecurityAccess.canProxyAuthorizeInstrument(securityRights, TradePortalConstants.INV_LINKED_INSTR_REC, "") &&
                                                 InstrumentAuthentication.requireTransactionAuthentication(getCBCResult().getAttribute("/ResultSetRecord(0)/REQUIRE_TRAN_AUTH"),InstrumentAuthentication.TRAN_AUTH__INVOICE_MGNT_FIN_INVOICE) &&
                                                (!(TradePortalConstants.AUTH_2FA_SUBTYPE_OTP.equals(userSession.getAuth2FASubType()))) &&
                                                shouldDisplay(DISPLAY_AUTH_INV_ACTION));
            case DISPLAY_PROXY_AUTH_ACTION_INVOICE:
                return (SecurityAccess.canProxyAuthorizeInstrument(securityRights, "RECI", "") &&
                                                (!(userSession.hasSavedUserSession() &&
                                userSession.getSavedUserSessionSecurityType().equals(TradePortalConstants.ADMIN))));
            case DISPLAY_CM_INSTRUMENT_AUTHORIZE_ACTION:
                return canAuthorizeCMInstrument();
            //cquinton 2/21/2013 ir#12117
            case DISPLAY_PO_CREATE_ATP_VIA_RULES:
                return (TradePortalConstants.INDICATOR_YES.equals(getOwnerOrg().getAttribute("allow_approval_to_pay")) &&
                        SecurityAccess.hasRights(securityRights,SecurityAccess.APPROVAL_TO_PAY_CREATE_MODIFY));
            case DISPLAY_PO_CREATE_LC_VIA_RULES:
                return (TradePortalConstants.INDICATOR_YES.equals(getOwnerOrg().getAttribute("allow_import_DLC")) &&
                        SecurityAccess.hasRights(securityRights,SecurityAccess.DLC_CREATE_MODIFY));


           //SHR PR CR708
           case DISPLAY_DELETE_PAY_INV_ACTION:
               return (SecurityAccess.hasRights(securityRights, SecurityAccess.PAYABLES_DELETE_INVOICE));
             //jgadela 03/16/2014 Rel IR 92 T36000036462 - Added security condition for corporate customer setting.
           case DISPLAY_CREATE_ATP_ACTION:
               return (SecurityAccess.hasRights(securityRights, SecurityAccess.PAYABLES_CREATE_ATP) 
            		   && SecurityAccess.canCreateModInstrument(securityRights,InstrumentType.APPROVAL_TO_PAY,"") 
            		   && TradePortalConstants.INDICATOR_YES.equals(getOwnerOrg().getAttribute("allow_approval_to_pay")) );

           case DISPLAY_APPLY_PAYMENT_DATE_ACTION:
               return (SecurityAccess.hasRights(securityRights, SecurityAccess.PAYABLES_APPLY_PAYDATE));
             //SHR PR CR708
			//Srinivasu_D CR-709 Rel8.2 12/14/2012 Start
			 case DISPLAY_CLEAR_PAYMENT_ACTION:
				  
                 return SecurityAccess.hasRights(securityRights, SecurityAccess.RECEIVABLES_CLEAR_PAYMENT_DATE);
			 case DISPLAY_ASSIGN_LOAN_TYPE_ACTION:
				
                 return SecurityAccess.hasRights(securityRights, SecurityAccess.RECEIVABLES_ASSIGN_LOAN_TYPE);
			 case DISPLAY_CREATE_LOANREQ_BYRULES_ACTION:					
                 
				 return (TradePortalConstants.INDICATOR_YES.equals(getOwnerOrg().getAttribute("allow_auto_loan_req_inv_upld"))
						 && SecurityAccess.canCreateModInstrument(securityRights,InstrumentType.LOAN_RQST,""));
			//Srinivasu_D CR-709 Rel8.2 12/14/2012 End
			//Srinivasu_D Fix of IR#11999 start
			case DISPLAY_CREATE_LOAN_REQ:
					
                 return SecurityAccess.hasRights(securityRights, SecurityAccess.RECEIVABLES_CREATE_LOAN_REQ)
                 && SecurityAccess.canCreateModInstrument(securityRights,InstrumentType.LOAN_RQST,"");
			case DISPLAY_REC_APPLY_PAYMENT_DATE	:
					
                 return SecurityAccess.hasRights(securityRights, SecurityAccess.RECEIVABLES_APPLY_PAYMENT_DATE);
			case DISPLAY_REC_ASSIGN_INSTR_TYPE	:
					
                 return SecurityAccess.hasRights(securityRights, SecurityAccess.RECEIVABLES_ASSIGN_INSTR_TYPE);
		   case DISPLAY_REC_CLOSE	:
					
                 return SecurityAccess.hasRights(securityRights, SecurityAccess.RECEIVABLES_CLOSE);

			//Srinivasu_D Fix of IR#11999 end
           //SHR CR709 Rel8.2 Start      
		    case DISPLAY_PAY_CLEAR_PAYMENT_ACTION:
				 
              return SecurityAccess.hasRights(securityRights, SecurityAccess.PAY_CLEAR_PAYMENT_DATE);
			 case DISPLAY_PAY_ASSIGN_LOAN_TYPE_ACTION:
				
              return SecurityAccess.hasRights(securityRights, SecurityAccess.PAY_ASSIGN_LOAN_TYPE);
			 case DISPLAY_PAY_ASSIGN_INSTR_TYPE	:
					
              return SecurityAccess.hasRights(securityRights, SecurityAccess.PAY_ASSIGN_INSTR_TYPE);
			 
              //jgadela 03/16/2014 Rel IR 92 T36000036462 - Added security condition for corporate customer setting.
			 case DISPLAY_PAY_CREATE_LOAN_REQ:				 
				 /* KMehta IR-T36000040714 Rel 9300 on 03-July-2015 Change - Begin*/
				 /*  return (SecurityAccess.hasRights(securityRights, SecurityAccess.PAY_CREATE_LOAN_REQ) && SecurityAccess.canCreateModInstrument(securityRights,InstrumentType.LOAN_RQST,"")
					 && TradePortalConstants.INDICATOR_YES.equals(getOwnerOrg().getAttribute("allow_loan_request")) );*/
			  
			  return (TradePortalConstants.INDICATOR_YES.equals(getOwnerOrg().getAttribute("allow_loan_request")) &&
					  SecurityAccess.hasRights(securityRights, SecurityAccess.PAY_CREATE_LOAN_REQ)
		                 && SecurityAccess.canCreateModInstrument(securityRights,InstrumentType.LOAN_RQST,""));
			  /* KMehta IR-T36000040714 Rel 9300 on 03-July-2015 Change - End*/
			
			  //SHR CR709 Rel8.2 End
			   case DISPLAY_PAY_CREATE_LOANREQ_BYRULES_ACTION:  
				 
				 return (TradePortalConstants.INDICATOR_YES.equals(getOwnerOrg().getAttribute("allow_auto_loan_req_inv_upld"))
						 && SecurityAccess.canCreateModInstrument(securityRights,InstrumentType.LOAN_RQST,""));
				//Srinivasu_D CR-708D Rel8.2 start 
			   case DISPLAY_PAY_CREATE_ATP_BYRULES_ACTION:
					
				   return (TradePortalConstants.INDICATOR_YES.equals(getOwnerOrg().getAttribute("allow_auto_atp_inv_only_upld"))
						   && SecurityAccess.canCreateModInstrument(securityRights,InstrumentType.APPROVAL_TO_PAY,""));
				//Srinivasu_D CR-708D Rel8.2 end
				
			//Srinivasu_D CR#269 Rel8.4 09/02/2013 - start
			 case DISPLAY_CC:
                return shouldDisplay(DISPLAY_CC_NEW_GUAR);

			 case DISPLAY_CC_NEW_GUAR:
                return  (TradePortalConstants.INDICATOR_YES.equals(getOwnerOrg().getAttribute("allow_guarantee")) && SecurityAccess.hasRights(securityRights,SecurityAccess.CC_GUA_CREATE_MODIFY));
           //Srinivasu_D CR#269 Rel8.4 09/02/2013 - end

				   
		   
			   //jgadela 08/09/2013 REL 8.3 CR-821 - T36000019559 <START>
			   case DISPLAY_DUAL_AUTH_APPROVAL_TO_PAY:
			  	                return (TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH.equals(getOwnerOrg().getAttribute("dual_auth_approval_to_pay")));

			   case DISPLAY_DUAL_AUTH_AIRWAY_BILL:
			  	                return (TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH.equals(getOwnerOrg().getAttribute("dual_auth_airway_bill")));

			   case DISPLAY_DUAL_AUTH_DISC_RESPONSE:
			  	                return (TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH.equals(getOwnerOrg().getAttribute("dual_auth_disc_response "))) ;

			   case DISPLAY_DUAL_AUTH_EXPORT_COLL:
			  	                return (TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH.equals(getOwnerOrg().getAttribute("dual_auth_export_coll ")));

			   case DISPLAY_DUAL_AUTH_EXPORT_LC:
			  	                return (TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH.equals(getOwnerOrg().getAttribute("dual_auth_export_LC")));

			   case DISPLAY_DUAL_AUTH_DIRECT_DEBIT:
			  	                return (TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH.equals(getOwnerOrg().getAttribute("dual_auth_direct_debit")));

			   case DISPLAY_DUAL_AUTH_GUARANTEE:
			  	                return (TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH.equals(getOwnerOrg().getAttribute("dual_auth_guarantee")));

			   case DISPLAY_DUAL_AUTH_IMPORT_DLC:
			  	                return (TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH.equals(getOwnerOrg().getAttribute("dual_auth_import_DLC")));

			   case DISPLAY_DUAL_AUTH_LOAN_REQUEST:
			  	                return (TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH.equals(getOwnerOrg().getAttribute("dual_auth_loan_request")));

			   case DISPLAY_DUAL_AUTH_REQUEST_ADVISE:
			  	                return (TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH.equals(getOwnerOrg().getAttribute("dual_auth_request_advise")));

			   case DISPLAY_DUAL_AUTH_SUPPLIER_PORTAL:
			  	                return (TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH.equals(getOwnerOrg().getAttribute("dual_auth_supplier_portal")));

			   case DISPLAY_DUAL_AUTH_SHIPPING_GUAR:
			  	                return (TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH.equals(getOwnerOrg().getAttribute("dual_auth_shipping_guar")));

			   case DISPLAY_DUAL_AUTH_SLC:
			  	                return (TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH.equals(getOwnerOrg().getAttribute("dual_auth_SLC")));

			   case DISPLAY_DUAL_DOMESTIC_PAYMENTS:
			  	                return (TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH.equals(getOwnerOrg().getAttribute("dual_domestic_payments")));

			   case DISPLAY_DUAL_XFER_BTWN_ACCTS:
			  	                return (TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH.equals(getOwnerOrg().getAttribute("dual_xfer_btwn_accts")));
			   
			   case DISPLAY_DUAL_AUTH_FUNDS_TRANSFER:
			  	                return (TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH.equals(getOwnerOrg().getAttribute("dual_auth_funds_transfer")));

			   case DISPLAY_DUAL_AUTH_SETTLE_INSTRUCTION:
 	                			return (TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH.equals(getOwnerOrg().getAttribute("dual_settle_instruction")));
 	                
			   case DISPLAY_PANEL_AUTHORISATION_TRANSACTIONS:
			                  	return ( shouldDisplay(DISPLAY_DUAL_AUTH_APPROVAL_TO_PAY) ||
			                             shouldDisplay(DISPLAY_DUAL_AUTH_AIRWAY_BILL) ||
			                             shouldDisplay(DISPLAY_DUAL_AUTH_DISC_RESPONSE) ||
			                             shouldDisplay(DISPLAY_DUAL_AUTH_EXPORT_COLL) ||
			                             shouldDisplay(DISPLAY_DUAL_AUTH_EXPORT_LC) ||
			                             shouldDisplay(DISPLAY_DUAL_AUTH_DIRECT_DEBIT) ||
			                             shouldDisplay(DISPLAY_DUAL_AUTH_GUARANTEE) ||
			                             shouldDisplay(DISPLAY_DUAL_AUTH_IMPORT_DLC) ||
			                             shouldDisplay(DISPLAY_DUAL_AUTH_LOAN_REQUEST) ||
			                             shouldDisplay(DISPLAY_DUAL_AUTH_REQUEST_ADVISE) ||
			                             shouldDisplay(DISPLAY_DUAL_AUTH_SUPPLIER_PORTAL) ||
			                             shouldDisplay(DISPLAY_DUAL_AUTH_SHIPPING_GUAR) ||
			                             shouldDisplay(DISPLAY_DUAL_AUTH_SLC) ||
			                             shouldDisplay(DISPLAY_DUAL_AUTH_FUNDS_TRANSFER) ||
			                             shouldDisplay(DISPLAY_DUAL_AUTH_SUPPLIER_PORTAL) ||
			                             shouldDisplay(DISPLAY_DUAL_XFER_BTWN_ACCTS) ||
			                             shouldDisplay(DISPLAY_DUAL_DOMESTIC_PAYMENTS) ||
			                             shouldDisplay(DISPLAY_DUAL_AUTH_SETTLE_INSTRUCTION) ); //Rel8.3 IR#T36000023004 - adding condition of Payments

			  //jgadela 08/09/2013 REL 8.3 CR-821 - T36000019559 <END>
			              	//RKAZI REL 8.4 CR-599 11/20/2013 - ADD - START				   
	            case DISPLAY_PAYMENT_METH_DEFN:
	                return (SecurityAccess.hasRights( securityRights,SecurityAccess.VIEW_OR_MAINTAIN_PAYMENT_FILE_DEF));
	            	//RKAZI REL 8.4 CR-599 11/20/2013 - ADD - END
	            
	            case DISPLAY_AUTHORIZE_ACTION_TO_INVOICE:
	            	return ( shouldDisplay(DISPLAY_UPLOAD_PAY_MGM_AUTH_ACTION) ||
	            			   shouldDisplay(DISPLAY_UPLOAD_PAY_MGM_OFFLINE_AUTH_ACTION) ||
	            			   shouldDisplay(DISPLAY_UPLOAD_PAY_MGM_INV_DECLINE_ACTION));
	           
	            case DISPLAY_OTHER_ACTION_TO_INVOICE:
	            	return ( shouldDisplay(DISPLAY_APPROVE_FIN_ACTION) ||
	            			   shouldDisplay(DISPLAY_REMOVE_INV_ACTION) || 
	            			   shouldDisplay(DISPLAY_APPROVE_AND_AUTH_ACTION));
	            	
	            case DISPLAY_OTHER_ACTION_TO_LR_INVOICE:
	            	return ( shouldDisplay(DISPLAY_REC_APPLY_PAYMENT_DATE) ||
	            			 shouldDisplay(DISPLAY_CLEAR_PAYMENT_ACTION) ||
	            			 shouldDisplay(DISPLAY_ASSIGN_LOAN_TYPE_ACTION) ||
	            			 shouldDisplay(DISPLAY_CREATE_LOAN_REQ) ||
	            			 shouldDisplay(DISPLAY_CREATE_LOANREQ_BYRULES_ACTION) ||
	            			 shouldDisplay(DISPLAY_DELETE_INV_ACTION));
	            
	            case DISPLAY_OTHER_ACTION_TO_CREDT_INVOICE:
	            	return ( shouldDisplay(DISPLAY_DELETE_INV_ACTION) ||
	            			 shouldDisplay(DISPLAY_REC_ASSIGN_INSTR_TYPE));
	            	
	            case DISPLAY_OTHER_ACTION_TO_LR_PAY_INVOICE:
	            	return ( shouldDisplay(DISPLAY_APPLY_PAYMENT_DATE_ACTION) ||
	            			 shouldDisplay(DISPLAY_PAY_CLEAR_PAYMENT_ACTION) ||
	            			 shouldDisplay(DISPLAY_PAY_ASSIGN_LOAN_TYPE_ACTION) ||
	            			 shouldDisplay(DISPLAY_PAY_CREATE_LOAN_REQ) ||
	            			 shouldDisplay(DISPLAY_PAY_CREATE_LOANREQ_BYRULES_ACTION) ||
	            			 shouldDisplay(DISPLAY_DELETE_PAY_INV_ACTION));
	            	
	            case DISPLAY_OTHER_ACTION_TO_PEND_PAY_INVOICE:
	            	return ( shouldDisplay(DISPLAY_APPLY_PAYMENT_DATE_ACTION) ||
	            			 shouldDisplay(DISPLAY_PAY_CLEAR_PAYMENT_ACTION) ||
	            			 shouldDisplay(DISPLAY_PAY_ASSIGN_INSTR_TYPE) ||
	            			 shouldDisplay(DISPLAY_CREATE_ATP_ACTION) ||
	            			 shouldDisplay(DISPLAY_PAY_CREATE_ATP_BYRULES_ACTION) ||
	            			 shouldDisplay(DISPLAY_DELETE_PAY_INV_ACTION));
	            	
	             //SURESHL T36000032102 REL 9.2 18/12/2014  <START>       
	            case DISPLAY_AUTHORIZE_ACTION_TO_REC_INVOICE:
	            	return ( shouldDisplay(DISPLAY_AUTH_INV_ACTION) ||
	            			 shouldDisplay(DISPLAY_PROXY_AUTH_ACTION_INVOICE) ||
	            			 shouldDisplay(DISPLAY_UPLOAD_REC_MGM_INV_DECLINE_ACTION));
	             //SURESHL T36000032102 REL 9.2 18/12/2014   <END> 
	            	
	            //CR 913 start 
	            case DISPLAY_PAY_MGMT_INV_AUTH:
	                return (SecurityAccess.hasRights(securityRights, SecurityAccess.PAY_MGM_AUTH) &&
	                		(!(userSession.hasSavedUserSession() &&
                            userSession.getSavedUserSessionSecurityType().equals(TradePortalConstants.ADMIN))));
	            case DISPLAY_PAY_MGMT_INV_OFFLINE_AUTH:
	                return (SecurityAccess.hasRights(securityRights, SecurityAccess.PAY_MGM_OFFLINE_AUTH) &&
	                		(!(userSession.hasSavedUserSession() &&
                            userSession.getSavedUserSessionSecurityType().equals(TradePortalConstants.ADMIN))) );
	            case DISPLAY_PAY_MGMT_INV_APPLY_PAY_DATE:
	                return (SecurityAccess.hasRights(securityRights, SecurityAccess.PAY_MGM_APPLY_PAYMENT_DATE) );
	            case DISPLAY_PAY_MGMT_INV_CLEAR_PAY_DATE:
	                return (SecurityAccess.hasRights(securityRights, SecurityAccess.PAY_MGM_CLEAR_PAYMENT_DATE) );
	            case DISPLAY_PAY_MGMT_INV_MODIFY_SUPP_DATE:
	                return (SecurityAccess.hasRights(securityRights, SecurityAccess.PAY_MGM_APPLY_SUPPLIER_DATE) );
	            case DISPLAY_PAY_MGMT_INV_RESET_SUPP_DATE:
	                return (SecurityAccess.hasRights(securityRights, SecurityAccess.PAY_MGM_RESET_SUPPLIER_DATE) );
	            case DISPLAY_PAY_MGMT_INV_APPLY_EARLY_PAY_AMT:
	                return (SecurityAccess.hasRights(securityRights, SecurityAccess.PAY_MGM_APPLY_EARLY_PAY_AMOUNT) );
	            case DISPLAY_PAY_MGMT_INV_RESET_EARLY_PAY_AMT:
	                return (SecurityAccess.hasRights(securityRights, SecurityAccess.PAY_MGM_RESET_EARLY_PAY_AMOUNT) );
	            	
	            case DISPLAY_MODIFY_SUPPLIER_DATE_ACTION :
	            	return (SecurityAccess.hasRights(securityRights, SecurityAccess.UPLOAD_PAY_MGM_APPLY_SUPPLIER_DATE));
	            case DISPLAY_RESET_SUPPLIER_DATE_ACTION :
	            	return (SecurityAccess.hasRights(securityRights, SecurityAccess.UPLOAD_PAY_MGM_RESET_SUPPLIER_DATE));
	            case DISPLAY_APPLY_PAYMENT_AMOUNT_ACTION :
	            	return (SecurityAccess.hasRights(securityRights, SecurityAccess.UPLOAD_PAY_MGM_APPLY_EARLY_PAY_AMOUNT));
	            case DISPLAY_RESET_PAYMENT_AMOUNT_ACTION :
	            	return (SecurityAccess.hasRights(securityRights, SecurityAccess.UPLOAD_PAY_MGM_RESET_EARLY_PAY_AMOUNT));
	           case DISPLAY_UPLOAD_PAY_MGM_AUTH_ACTION :
	            	return (SecurityAccess.hasRights(securityRights, SecurityAccess.UPLOAD_PAY_MGM_AUTH) &&
	                		(!(userSession.hasSavedUserSession() &&
	                                userSession.getSavedUserSessionSecurityType().equals(TradePortalConstants.ADMIN))) );
	            case DISPLAY_UPLOAD_PAY_MGM_OFFLINE_AUTH_ACTION :
	            	return (SecurityAccess.hasRights(securityRights, SecurityAccess.UPLOAD_PAY_MGM_OFFLINE_AUTH) &&
	                		(!(userSession.hasSavedUserSession() &&
	                                userSession.getSavedUserSessionSecurityType().equals(TradePortalConstants.ADMIN))) );           	
	            case DISPLAY_DATES_ACTION :
	            	return ( shouldDisplay(DISPLAY_APPLY_PAYMENT_DATE_ACTION) ||
	            			 shouldDisplay(DISPLAY_PAY_CLEAR_PAYMENT_ACTION) ||
	            			 shouldDisplay(DISPLAY_MODIFY_SUPPLIER_DATE_ACTION) ||
	            			 shouldDisplay(DISPLAY_RESET_SUPPLIER_DATE_ACTION) ||
	            			 shouldDisplay(DISPLAY_PAY_CREATE_LOANREQ_BYRULES_ACTION) ||
	            			 shouldDisplay(DISPLAY_DELETE_PAY_INV_ACTION));
	            case DISPLAY_AMOUNTS_ACTION :
	            	return ( shouldDisplay(DISPLAY_APPLY_PAYMENT_AMOUNT_ACTION) ||
	            			 shouldDisplay(DISPLAY_RESET_PAYMENT_AMOUNT_ACTION) );
	            case DISPLAY_INSTRUMENTS_ACTION :
	            	return ( shouldDisplay(DISPLAY_PAY_ASSIGN_INSTR_TYPE) ||
	            			 shouldDisplay(DISPLAY_PAY_ASSIGN_LOAN_TYPE_ACTION) || 
	            			 shouldDisplay(DISPLAY_PAY_CREATE_LOAN_REQ) ||
	            			 shouldDisplay(DISPLAY_PAY_CREATE_LOANREQ_BYRULES_ACTION)||
	            			 shouldDisplay(DISPLAY_CREATE_ATP_ACTION)||
	            			 shouldDisplay(DISPLAY_PAY_CREATE_ATP_BYRULES_ACTION) );
	            	
	            case DISPLAY_PAY_PRGM_GROUPS_ACTION :
	            	return ( shouldDisplay(DISPLAY_APPLY_PAYMENT_DATE_ACTION) ||
	            			 shouldDisplay(DISPLAY_PAY_CLEAR_PAYMENT_ACTION) ||
	            			 shouldDisplay(DISPLAY_MODIFY_SUPPLIER_DATE_ACTION) ||
	            			 shouldDisplay(DISPLAY_RESET_SUPPLIER_DATE_ACTION) ||
	            			 shouldDisplay(DISPLAY_DELETE_PAY_INV_ACTION));
	            	
	            case DISPLAY_PAY_PRGM_GROUP_LIST_ACTION :
	            	return ( shouldDisplay(DISPLAY_PAY_PRGM_GROUPS_ACTION) ||
	            			 shouldDisplay(DISPLAY_AMOUNTS_ACTION));
	            //AiA CR-913 03-24-2014
	            case DISPLAY_PAY_MGMT_AUTH_ACTION :
	            	return ( shouldDisplay(DISPLAY_PAY_MGMT_INV_AUTH) ||
	            			 shouldDisplay(DISPLAY_PAY_MGMT_INV_OFFLINE_AUTH));	
	            case DISPLAY_SP_OFFER_ACTIONS :
	            	return ( shouldDisplay(DISPLAY_SP_ACCEPT_OFFER) ||
	            			 shouldDisplay(DISPLAY_SP_DECLINE_OFFER)||
	            			 shouldDisplay(DISPLAY_SP_RESET_TO_OFFERED));
	            case DISPLAY_SP_FVD_ACTIONS :
	            	return ( shouldDisplay(DISPLAY_SP_ASSIGN_FVD) ||
	            			 shouldDisplay(DISPLAY_SP_REMOVE_FVD));	            	
	            	
	            case DISPLAY_PAY_MGMT_DATES_ACTION :
	            	return ( shouldDisplay(DISPLAY_PAY_MGMT_INV_APPLY_PAY_DATE) ||
	            			 shouldDisplay(DISPLAY_PAY_MGMT_INV_CLEAR_PAY_DATE) ||
	            			 shouldDisplay(DISPLAY_PAY_MGMT_INV_RESET_SUPP_DATE) ||
	            			 shouldDisplay(DISPLAY_PAY_MGMT_INV_MODIFY_SUPP_DATE));	
	            	
	            case DISPLAY_PAY_MGMT_AMOUNTS_ACTION :
	            	return ( shouldDisplay(DISPLAY_PAY_MGMT_INV_APPLY_EARLY_PAY_AMT) ||
	            			 shouldDisplay(DISPLAY_PAY_MGMT_INV_RESET_EARLY_PAY_AMT));	
	            	
	            case DISPLAY_REC_FINANCE_ACTION :
	            	return ( shouldDisplay(DISPLAY_REMOVE_INV_ACTION) ||
	            			 shouldDisplay(DISPLAY_APPROVE_FIN_ACTION));
	            	
	            case DISPLAY_REC_DATE_ACTION :
	            	return ( shouldDisplay(DISPLAY_REC_APPLY_PAYMENT_DATE) ||
	            			 shouldDisplay(DISPLAY_CLEAR_PAYMENT_ACTION));
	            	
	            case DISPLAY_REC_INSTRUMENT_ACTION :
	            	return ( shouldDisplay(DISPLAY_REC_ASSIGN_INSTR_TYPE) ||
	            			 shouldDisplay(DISPLAY_ASSIGN_LOAN_TYPE_ACTION) ||
	            			 shouldDisplay(DISPLAY_CREATE_LOAN_REQ)||
	            			 shouldDisplay(DISPLAY_CREATE_LOANREQ_BYRULES_ACTION));
	            	
	            case DISPLAY_DELETE_NOTIFICATION:
	            	return (SecurityAccess.hasRights(securityRights, SecurityAccess.DELETE_NOTIFICATION));
	            	
	            case DISPLAY_PREDEBIT_DELETE:
	            	return (SecurityAccess.hasRights(securityRights, SecurityAccess.DELETE_PREDEBIT_FUNDING_NOTIFICATION));
	            case DISPLAY_PREDEBIT_ROUTE:
	            	return (SecurityAccess.hasRights(securityRights, SecurityAccess.ROUTE_PREDEBIT_FUNDING_NOTIFICATION));
	            	
	            // CR914A - Rel9.2
	            //MEer IR-36222 Admin User not allowed to see Authorise button
	            case DISPLAY_PAY_MGMT_CRT_AUTH_OR_DEC:
	            	return shouldDisplay(DISPLAY_PAY_MGMT_CRT_AUTH) || shouldDisplay(DISPLAY_UPLOAD_PAY_MGM_CRN_DECLINE_ACTION);      	
	            
	            case DISPLAY_PAY_MGMT_CRT_AUTH:
	            	return ( (SecurityAccess.hasRights(securityRights, SecurityAccess.PAY_CREDIT_NOTE_AUTH) &&
	            			(TradePortalConstants.INDICATOR_NO.equals(getOwnerOrg().getAttribute("allow_manual_cr_note_apply")) ||
	            					StringFunction.isBlank(getOwnerOrg().getAttribute("allow_manual_cr_note_apply")))) ||  ( SecurityAccess.hasRights(securityRights, SecurityAccess.PAY_CREDIT_NOTE_APPROVE_AUTH) )) && 
	            					(!(userSession.hasSavedUserSession() &&
	            							userSession.getSavedUserSessionSecurityType().equals(TradePortalConstants.ADMIN))) ;      	
	            
	            case DISPLAY_PAY_MGMT_CRT_CLOSE:
	            	return (SecurityAccess.hasRights(securityRights, SecurityAccess.PAY_CREDIT_NOTE_CLOSE));
	            case DISPLAY_PAY_MGMT_CRT_DELETE:
	            	return (SecurityAccess.hasRights(securityRights, SecurityAccess.PAY_CREDIT_NOTE_DELETE));
	            	
	            case DISPLAY_PAY_MGMT_CRT_APPLY:
	            	return SecurityAccess.hasRights(securityRights, SecurityAccess.PAY_CREDIT_NOTE_MANUAL_APPLY) &&
	            			(TradePortalConstants.INDICATOR_YES.equals(getOwnerOrg().getAttribute("allow_manual_cr_note_apply")));
	            case DISPLAY_PAY_MGMT_CRT_UNAPPLY:
	            	return SecurityAccess.hasRights(securityRights, SecurityAccess.PAY_CREDIT_NOTE_MANUAL_UNAPPLY) &&
	            			(TradePortalConstants.INDICATOR_YES.equals(getOwnerOrg().getAttribute("allow_manual_cr_note_apply")));
	            //MEer Rel 9.2  IR-37783
	            case DISPLAY_ATTACH_DOCUMENT_ACTION:	  	
	                return (SecurityAccess.hasRights(securityRights, SecurityAccess.ATTACH_DOC_TRANSACTION));
	            case DISPLAY_DELETE_DOCUMENT_ACTION:
	               return (SecurityAccess.hasRights(securityRights, SecurityAccess.DELETE_DOC_TRANSACTION));	                
	            case DISPLAY_DOCUMENT:
	                return (SecurityAccess.hasRights(securityRights, SecurityAccess.ATTACH_DOC_TRANSACTION) || SecurityAccess.hasRights(securityRights, SecurityAccess.DELETE_DOC_TRANSACTION));
	              // Srinivasu_D CR#1006 Rel9.3 05/11/2015 added decline for payables/credit/receivables                               	           
	           
				 case DISPLAY_UPLOAD_PAY_MGM_INV_DECLINE_ACTION :
	            	return (SecurityAccess.hasRights(securityRights, SecurityAccess.PAY_INVOICE_DECLINE_AUTH) &&
	                		(!(userSession.hasSavedUserSession() &&
	                                userSession.getSavedUserSessionSecurityType().equals(TradePortalConstants.ADMIN))) &&
							(TradePortalConstants.INDICATOR_YES.equals(getOwnerOrg().getAttribute("allow_pay_dec_inv")) || TradePortalConstants.INDICATOR_YES.equals(getOwnerOrg().getAttribute("dual_auth_pay_dec_inv"))));	
				
				 case DISPLAY_UPLOAD_PAY_MGM_CRN_DECLINE_ACTION :
	            	return (SecurityAccess.hasRights(securityRights, SecurityAccess.PAY_CREDIT_NOTE_DECLINE_AUTH) &&
	                		(!(userSession.hasSavedUserSession() &&
	                                userSession.getSavedUserSessionSecurityType().equals(TradePortalConstants.ADMIN)))  &&
							(TradePortalConstants.INDICATOR_YES.equals(getOwnerOrg().getAttribute("allow_pay_dec_crn")) || TradePortalConstants.INDICATOR_YES.equals(getOwnerOrg().getAttribute("dual_auth_pay_dec_crn"))));	
				 
				 case DISPLAY_UPLOAD_REC_MGM_INV_DECLINE_ACTION :
	            	return (SecurityAccess.hasRights(securityRights, SecurityAccess.REC_INVOICE_DECLINE_AUTH) &&
	                		(!(userSession.hasSavedUserSession() &&
	                                userSession.getSavedUserSessionSecurityType().equals(TradePortalConstants.ADMIN)))  &&
							(TradePortalConstants.INDICATOR_YES.equals(getOwnerOrg().getAttribute("allow_rec_dec_inv")) || TradePortalConstants.INDICATOR_YES.equals(getOwnerOrg().getAttribute("dual_auth_rec_dec_inv"))));
				
				 // Srinivasu_D CR#1006 Rel9.3 05/11/2015 End
	            	
				//Rel9.3.5 CR-1028/1029 START	  
				//update center
	            case DISPLAY_UPDATE_CENTRE:
	                return ( userSession.isEnableAdminUpdateInd() &&  
            					( shouldDisplay(DISPLAY_BANK_XML_DOWNLOADS) || 
	                				shouldDisplay(DISPLAY_BANK_TRANSACTION_UPDATES) || 
	                				shouldDisplay(DISPLAY_BANK_MAIL_MESSAGES)
	                			)
	                        );

	            case DISPLAY_BANK_XML_DOWNLOADS:
	                return (SecurityAccess.hasRights(securityRights, SecurityAccess.BANK_XML_DOWNLOAD));
	            
	            case DISPLAY_BANK_TRANSACTION_UPDATES:     
					  return (( SecurityAccess.hasRights(securityRights, SecurityAccess.BANK_TRANS_APPLY_UPDATE) || 
							  SecurityAccess.hasRights(securityRights, SecurityAccess.BANK_TRANS_APPROVE) || 
							  SecurityAccess.hasRights(securityRights, SecurityAccess.BANK_TRANS_SEND_FOR_REPAIR) || 
							  SecurityAccess.hasRights(securityRights, SecurityAccess.BANK_TRANS_ATTACH_DOC) || 
							  SecurityAccess.hasRights(securityRights, SecurityAccess.BANK_TRANS_DELETE_DOC) 							
							)); 
					  
	            case DISPLAY_BANK_MAIL_MESSAGES :
					  return ((SecurityAccess.hasRights(securityRights, SecurityAccess.BANK_MAIL_DELETE) || 
							  SecurityAccess.hasRights(securityRights, SecurityAccess.BANK_MAIL_CREATE_REPLY) || 
							  SecurityAccess.hasRights(securityRights, SecurityAccess.BANK_MAIL_SEND_TO_CUST) || 
							  SecurityAccess.hasRights(securityRights, SecurityAccess.BANK_MAIL_ATTACH_DOC) || 
							  SecurityAccess.hasRights(securityRights, SecurityAccess.BANK_MAIL_DELETE_DOC)
							  )); 
	            //Rel9.3.5 CR-1028/1029 END    
	                
            default:
                return false;
        }
    }

    private boolean canAuthorizeCMInstrument() {
                boolean canAuth = false;

                if (SecurityAccess.hasRights(securityRights, SecurityAccess.INSTRUMENT_AUTHORIZE)) {
            String loginSecurityType = null;
            boolean subsidiaryAccessPanelAuth = false;
            if (userSession.hasSavedUserSession()) {
                loginSecurityType = userSession.getSavedUserSessionSecurityType();
                if (TradePortalConstants.INDICATOR_YES.equals(getOwnerOrg().getAttribute("allow_panel_auth_for_pymts"))){
                    subsidiaryAccessPanelAuth = true;
                }
            } else {
                loginSecurityType = userSession.getSecurityType();
            }
            //02/27/2014 R8.4 IR T36000023710 - Authorize button not available on Subsidiary Pending transaction
            if (loginSecurityType.equals(TradePortalConstants.NON_ADMIN) || (loginSecurityType.equals(TradePortalConstants.NON_ADMIN) && subsidiaryAccessPanelAuth)) {
                canAuth =true;
            }
         }
                return canAuth;
    }

    private CorporateOrganizationWebBean getOwnerOrg() {
        if (ownerOrg==null) {
            String ownerOrgOid = userSession.getOwnerOrgOid();
            ownerOrg = beanMgr.createBean (CorporateOrganizationWebBean.class, "CorporateOrganization");
            ownerOrg.getById(ownerOrgOid);
        }
        return ownerOrg;
    } 
    
    private DocumentHandler getCBCResult() {
                Cache reCertcache = TPCacheManager.getInstance().getCache(TradePortalConstants.CLIENT_BANK_CACHE);
        DocumentHandler CBCResults = (DocumentHandler)reCertcache.get(userSession.getClientBankOid());
        return CBCResults;
    }
}
