package com.ams.tradeportal.html;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Hashtable;

import com.ams.tradeportal.busobj.CorporateOrganizationBean;
import com.amsinc.ecsg.util.StringFunction;

/**
 * PanelLevelAliasesFormatter fetches the Panel Alias, defined at Corporate Customer Level, for Panel Levels
 */
public class PanelLevelAliasesFormatter extends AbstractCustomListViewColumnFormatter {
private static final Logger LOG = LoggerFactory.getLogger(PanelLevelAliasesFormatter.class);

	/**
	 * Format data.
	 * @param obj - error message
	 * @return panelAlias name
	 */
    public String format(Object obj) {
        String formattedData = (String) obj;

        CorporateOrganizationBean corpOrg = new CorporateOrganizationBean();
        Hashtable panelAliasesMap = new Hashtable();
        //MEer Rel 8.3 IR-21992- Updated method call with ignoreDefaultPanelValue
        boolean ignoreDefaultPanelValue = false;
		try {
			//MEer Rel 8.3 IR-19699- Updated method call with locale as parameter
			panelAliasesMap = corpOrg.getPanelLevelAliases(this.getUserSession().getOwnerOrgOid(), this.getUserSession().getUserLocale(), ignoreDefaultPanelValue);
		} catch (Exception e) {
			
			LOG.info("Exception caught in PanelLevelAliasesFormatter::"+e.toString());
		}
		if(StringFunction.isNotBlank(obj.toString())){
			formattedData = panelAliasesMap.get(obj.toString()).toString();
		}
        return formattedData;
    }
}
