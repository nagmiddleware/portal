package com.ams.tradeportal.html;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;

import com.ams.tradeportal.busobj.webbean.SessionWebBean;
import com.ams.tradeportal.common.Debug;
import com.ams.tradeportal.common.Sort;
import com.ams.tradeportal.common.TPDateTimeUtility;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.ReferenceDataManager;
import com.amsinc.ecsg.util.ResourceManager;
import com.amsinc.ecsg.util.StringFunction;

/**
 * Insert the type's description here.
 *
 * Copyright  © 2001 American Management Systems, Incorporated All rights
 * reserved
 */
public class WidgetFactory {
private static final Logger LOG = LoggerFactory.getLogger(WidgetFactory.class); 

	public WidgetFactory(ResourceManager resMgr) {
		this.resMgr = resMgr;
	}

	private ResourceManager resMgr;

	public WidgetFactory(ResourceManager resMgr, SessionWebBean userSession) {
		this.resMgr = resMgr;
		this.userSession = userSession;
	}

	private SessionWebBean userSession;

	static final String NL = System.getProperty("line.separator");
	static final String CLEAR_FIX = "<div style=\"clear:both;\"></div>";


	/* -------------------------- TEXT FIELD ------------------------------- */
	public String createTextField(String fieldName,
			String fieldLabel,
			String fieldValue,
			String fieldLength,
			boolean isReadOnly) {

		boolean isRequired = false;
		boolean isExpress = false;
		String htmlParms = "";
		String widgetParms = "";
		String divClass = "";

		return createTextField(fieldName, fieldLabel, fieldValue, fieldLength,
				isReadOnly, isRequired, isExpress, htmlParms,
				widgetParms, divClass);
	}

	public String createTextField(String fieldName,
			String fieldLabel,
			String fieldValue,
			String fieldLength,
			boolean isReadOnly,
			boolean isRequired) {

		boolean isExpress = false;
		String htmlParms = "";
		String widgetParms = "";
		String divClass = "";
		return createTextField(fieldName, fieldLabel, fieldValue, fieldLength,
				isReadOnly, isRequired, isExpress, htmlParms,
				widgetParms, divClass);
	}

	public String createTextField(String fieldName,
			String fieldLabel,
			String fieldValue,
			String fieldLength,
			boolean isReadOnly,
			boolean isRequired,
			boolean isExpress,
			String htmlProps,
			String widgetProps,
			String divClass) {


		return createTextField(fieldName, fieldLabel, fieldValue, fieldLength, isReadOnly, isRequired, isExpress, htmlProps, widgetProps, divClass, null);
	}

	public String createTextField(String fieldName,
			String fieldLabel,
			String fieldValue,
			String fieldLength,
			boolean isReadOnly,
			boolean isRequired,
			boolean isExpress,
			String htmlProps,
			String widgetProps,
			String divClass,String buttons) {
		
		   return createTextField(fieldName, fieldLabel, fieldValue, fieldLength, isReadOnly, isRequired, isExpress, htmlProps, widgetProps, divClass, buttons, false);
	    }
		public String createTextField(String fieldName,
				String fieldLabel,
				String fieldValue,
				String fieldLength,
				boolean isReadOnly,
				boolean isRequired,
				boolean isExpress,
				String htmlProps,
				String widgetProps,
				String divClass,String buttons, boolean genWrapperDivID) {

		StringBuffer html = new StringBuffer("");
		// Create the label that will identify the field
		html.append(createStackedLabel(fieldName, fieldLabel));


		// If the field is read-only, don't create a TextBox, just use plain
		// text
		if (isReadOnly) {
			html.append(createReadOnlyTextField(fieldName, fieldValue));
			wrapInDiv(html, divClass, isReadOnly, isRequired, isExpress);
			return html.toString();
		}

		// If validation criteria is specified for the field, create a special
		// validation text box. Otherwise, a plain text box will do
		if (isRequired || !widgetProps.equals("")) {
			html.append(renderWidgetId("input", "dijit.form.ValidationTextBox",
					fieldName, htmlProps, widgetProps));
		} else {
			html.append(renderWidgetId("input", "dijit.form.TextBox",
					fieldName, htmlProps, widgetProps));
		}

		// Set maximum characters that can be entered in the field
		html.append(" maxLength=\"");
		logXSSString("createTextField","fieldLength",fieldLength,StringFunction.xssCharsToHtml(fieldLength));
		//html.append(StringFunction.xssCharsToHtml(fieldLength));
		html.append(fieldLength);
		html.append("\"");

		String width=fieldLength;
        //cquinton 9/27/2012 refactor width attribute processing
        //todo: use fieldClass instead to genericize
        String attrVal=getHtmlAttributeValue(htmlProps, "width");
        if ( attrVal!=null && attrVal.length()>0 ) {
            width = attrVal;
        }

			html.append(" class=\"char");
			logXSSString("createTextField","width",width,StringFunction.xssCharsToHtml(width));
			//html.append(StringFunction.xssCharsToHtml(width));
			html.append(width);
			html.append("\"");

		// Set the value of the field if it is not empty
		if (StringFunction.isNotBlank(fieldValue)) {
			html.append(" value=\"");
			logXSSString("createTextField","fieldValue",fieldValue,StringFunction.xssCharsToHtml(fieldValue));
			//html.append(StringFunction.xssCharsToHtml(fieldValue));
			html.append(fieldValue);
			html.append("\"");
		}

		// Add in-line validation if the field is required
		if (isRequired) {
			html.append(" required=\"true\" ");
		}
//		rkazi Rel 8.3 T3600021563 10/06/2013 -Add auto complete = off to meet fishnet security requirements - Start
		if (!html.toString().toLowerCase().contains("autocomplete")){
			html.append(" AutoComplete =\"off\" ");
		}
//		rkazi Rel 8.3 T3600021563 10/06/2013 - End
		// Closing tag for the widget
		html.append(">");

		if(null!=buttons){
			html.append(buttons);
		}
		// divClass = divClass + " char" + fieldLength;

		// Put a <div> around the Label and the Field
		wrapInDiv(html, divClass, isReadOnly, isRequired, isExpress);
		
		// Add hover help
			html.append(createHoverHelp(fieldName, fieldLabel));

		return html.toString();
	}
	private String createReadOnlyTextField(String fieldName, String fieldValue) {

		StringBuilder html = new StringBuilder("");
		logXSSString("createReadOnlyTextField","fieldName",fieldName,StringFunction.xssCharsToHtml(fieldName));
		html.append("<span class=\"fieldValue\" id=\"").append(StringFunction.xssCharsToHtml(fieldName)).append("\">"); 
		
		if (StringFunction.isBlank(fieldValue)) {
			html.append("&nbsp;");
		} else {
			logXSSString("createReadOnlyTextField","fieldValue",fieldValue,StringFunction.xssCharsToHtml(fieldValue));
			//html.append(StringFunction.xssCharsToHtml(fieldValue));
			html.append(fieldValue);
		}

		html.append("</span>");
		return html.toString();
	}

	public String createSearchTextField(String fieldName, String fieldLabel, String fieldLength) {
		return createSearchTextField(fieldName, fieldLabel, fieldLength, "");
	}

    public String createSearchTextField(String fieldName, String fieldLabel, String fieldLength, String htmlProps) {
    	return createSearchTextField(fieldName, fieldLabel, fieldLength, htmlProps, "");
	}
    	
    public String createSearchTextField(String fieldName, String fieldLabel, String fieldLength, String htmlProps, String widgetProps) {

        //cquinton 9/27/2012 get width modifiers first -
        // if labelClass is specified we need to set that as well
        String width=fieldLength;
        String labelClass=null; //by default no classes

        String attrVal=getHtmlAttributeValue(htmlProps, "width");
        if ( attrVal!=null && attrVal.length()>0 ) {
            width = attrVal;
        }
        attrVal=getHtmlAttributeValue(htmlProps, "labelClass");
        if ( attrVal!=null && attrVal.length()>0 ) {
            labelClass = attrVal;
        }


		StringBuilder html = new StringBuilder("");

		html.append(NL);
		html.append("<div class=\"searchItem\" >");


		// Create the label that will identify the field
		if ( labelClass!=null ) {
		    //apply labelClass if given
		    html.append(createInlineLabel(fieldName, fieldLabel,
		                    "class=\""+labelClass+"\""));
		}
		else {
		    html.append(createInlineLabel(fieldName, fieldLabel));
		}

		html.append(renderWidgetId("input", "dijit.form.TextBox", fieldName, htmlProps, widgetProps));

		// Set maximum characters that can be entered in the field
		html.append(" maxLength=\"");
		logXSSString("createSearchTextField","fieldLength",fieldLength,StringFunction.xssCharsToHtml(fieldLength));
		//html.append(StringFunction.xssCharsToHtml(fieldLength));
		html.append(fieldLength);
		html.append("\"");

        html.append(" class=\"char");
        logXSSString("createSearchTextField","width",width,StringFunction.xssCharsToHtml(width));
        //html.append(StringFunction.xssCharsToHtml(width));
        html.append(width);
        html.append("\"");
        
//		rkazi Rel 8.3 T3600021563 10/06/2013 -Add auto complete = off to meet fishnet security requirements - Start
		if (!html.toString().toLowerCase().contains("autocomplete")){
			html.append(" AutoComplete =\"off\" ");
		}
//		rkazi Rel 8.3 T3600021563 10/06/2013 - End

		// Closing tag for the widget
		html.append(">");

		// Put a <div> around the Label and the Field
		html.append("</div>");

		// Add hover help
		html.append(createHoverHelp(fieldName, fieldLabel));

		return html.toString();
	}

	/* --------------------------- FILE FIELD ------------------------------ */
	public String createFileField(String fieldName,
			String fieldLabel,
			boolean isReadOnly) {

		if (isReadOnly) {
			return "";
		}

		StringBuffer html = new StringBuffer("");

		// Create the label that will identify the field
		html.append(createStackedLabel(fieldName, fieldLabel));


		html.append(renderWidgetId("input type=\"file\"", "dijit.form.TextBox", fieldName, "", ""));

		html.append(" class=\"fileBrowse\"");

		// Closing tag for the widget
		html.append(">");

		// Put a <div> around the Label and the Field
		boolean isRequired = false;
		boolean isExpress = false;
		String	divClass = "";
		wrapInDiv(html, divClass, isReadOnly, isRequired, isExpress);

		//Hover text should appear only for editable components
		if(!isReadOnly){
			// Add hover help
			html.append(createHoverHelp(fieldName, fieldLabel));
		}

		return html.toString();
	}
/**
 * File Field with htmlParms to specify stle elements
 */
	public String createFileField(String fieldName,
			String fieldLabel,String htmlParms,String widgetParms,
			boolean isReadOnly) {

		if (isReadOnly) {
			return "";
		}

		StringBuffer html = new StringBuffer("");

		// Create the label that will identify the field
		html.append(createStackedLabel(fieldName, fieldLabel));


		html.append(renderWidgetId("input type=\"file\"", "dijit.form.TextBox", fieldName, htmlParms, widgetParms));

		html.append(" class=\"fileBrowse\"");

		// Closing tag for the widget
		html.append(">");

		// Put a <div> around the Label and the Field
		boolean isRequired = false;
		boolean isExpress = false;
		String	divClass = "";
		wrapInDiv(html, divClass, isReadOnly, isRequired, isExpress);

		//Hover text should appear only for editable components
		if(!isReadOnly){
			// Add hover help
			html.append(createHoverHelp(fieldName, fieldLabel));
		}

		return html.toString();
	}


	/* -------------------------- PRECENT FIELD ---------------------------- */
	public String createPercentField(String fieldName, String fieldLabel,
			String fieldValue, boolean isReadOnly) {

		boolean isRequired = false;
		boolean isExpress = false;
		String htmlProps = "";
		String widgetProps = "constraints:{min:0,max:100,pattern:'###.##'}";
		String divClass = "none";

		return createPercentField(fieldName, fieldLabel, fieldValue,
				isReadOnly, isRequired, isExpress, htmlProps,
				widgetProps, divClass);
	}

	public String createPercentField(String fieldName, String fieldLabel,
			String fieldValue, boolean isReadOnly, boolean isRequired,
			boolean isExpress, String htmlProps,
			String widgetProps, String divClass) {

		StringBuffer html = new StringBuffer("");
		
		// Create the label that will identify the field
		html.append(createStackedLabel(fieldName, fieldLabel));

		// If the field is read-only, don't create a TextBox, just use plain
		// text
		if (isReadOnly) {
			html.append(createReadOnlyTextField(fieldName, fieldValue));
			wrapInDiv(html, divClass, isReadOnly, isRequired, isExpress);
			return html.toString();
		}

		// If validation criteria is specified for the field, create a special
		// validation text box. Otherwise, a plain text box will do
		/*
		 * if (isRequired || !widgetProps.equals("")){
		 * html.append(renderWidgetId("input", "dijit.form.ValidationTextBox",
		 * fieldName, htmlProps, widgetProps)); } else {
		 * html.append(renderWidgetId("input", "dijit.form.TextBox", fieldName,
		 * htmlProps, widgetProps)); }
		 */
		if(widgetProps.equals("") || widgetProps.equals(" ")){
			widgetProps = "constraints:{min:0,max:100,pattern:'###.##'}";
		}
		logXSSString("createPercentField","fieldName",fieldName,StringFunction.xssCharsToHtml(fieldName));
		html.append(renderWidgetId("input", "dijit.form.NumberTextBox",
				StringFunction.xssCharsToHtml(fieldName), htmlProps, widgetProps));

		// Set maximum characters that can be entered in the field
		// html.append(" maxLength=\"");
		// html.append(fieldLength);
		// html.append("\"");

		// If specific width for the field has not been specified, use the
		// maxLength
		if (!htmlProps.toLowerCase().contains("width")) {
			html.append(" style=\"width:");
			html.append("3");
			html.append("em\"");
		}

		// Set the value of the field if it is not empty	
		if (StringFunction.isNotBlank(fieldValue)) {
			html.append(" value=\"");
			logXSSString("createPercentField","fieldValue",fieldValue,StringFunction.xssCharsToHtml(fieldValue));
			//html.append(StringFunction.xssCharsToHtml(fieldValue));
			html.append(fieldValue);
			html.append("\"");
		}

		// Add in-line validation if the field is required
		if (isRequired) {
			html.append(" required=\"true\" ");
		}

//		rkazi Rel 8.3 T3600021563 10/06/2013 -Add auto complete = off to meet fishnet security requirements - Start
		if (!html.toString().toLowerCase().contains("autocomplete")){
			html.append(" AutoComplete =\"off\" ");
		}
//		rkazi Rel 8.3 T3600021563 10/06/2013 - End

		// Closing tag for the widget
		html.append(">");

		//html.append(createSubLabel("transaction.Percent"));

		// Put a <div> around the Label and the Field
		wrapInDiv(html, divClass, isReadOnly, isRequired, isExpress);

		// Add hover help
		html.append(createHoverHelp(fieldName, fieldLabel));

		return html.toString();
	}

	/* -------------------------- NUMBER FIELD ----------------------------- */
	public String createNumberField(String fieldName, String fieldLabel,
			String fieldValue, String fieldLength, boolean isReadOnly) {

		boolean isRequired = false;
		boolean isExpress = false;
		String htmlParms = "";
		String widgetParms = "constraints:{min:0,places:0}";
		String divClass = "";
		return createNumberField(fieldName, fieldLabel, fieldValue,
				fieldLength, isReadOnly, isRequired, isExpress,
				htmlParms, widgetParms, divClass);
	}

	public String createNumberField(String fieldName, String fieldLabel,
			String fieldValue, String fieldLength, boolean isReadOnly,
			boolean isRequired, boolean isExpress,
			String htmlProps, String widgetProps, String divClass) {

		StringBuffer html = new StringBuffer("");

		// Create the label that will identify the field
		html.append(createStackedLabel(fieldName, fieldLabel));

		// If the field is read-only, don't create a TextBox, just use plain
		// text
		if (isReadOnly) {
			html.append(createReadOnlyTextField(fieldName, fieldValue));
			wrapInDiv(html, divClass, isReadOnly, isRequired, isExpress);
			return html.toString();
		}

		// If validation criteria is specified for the field, create a special
		// validation text box. Otherwise, a plain text box will do
		// if (isRequired || !widgetProps.equals("")){
		// html.append(renderWidgetId("input", "dijit.form.ValidationTextBox",
		// fieldName, htmlProps, widgetProps));
		// } else {
		// html.append(renderWidgetId("input", "dijit.form.TextBox", fieldName,
		// htmlProps, widgetProps));
		// }
		if(widgetProps.equals("") || widgetProps.equals(" ")){
			widgetProps = "constraints:{min:0,places:0}";
		}
		logXSSString("createNumberField","fieldName",fieldName,StringFunction.xssCharsToHtml(fieldName));
		html.append(renderWidgetId("input", "dijit.form.NumberTextBox",
				StringFunction.xssCharsToHtml(fieldName), htmlProps, widgetProps));

		// Set maximum characters that can be entered in the field
		logXSSString("createNumberField","fieldLength",fieldLength,StringFunction.xssCharsToHtml(fieldLength));
		html.append(" maxLength=\"");
		//html.append(StringFunction.xssCharsToHtml(fieldLength));
		html.append(fieldLength);
		html.append("\"");

		// If specific width for the field has not been specified, use the
		// maxLength

		String width=fieldLength;
        //Komal - 14/11/2012 refactor width attribute processing
        String attrVal=getHtmlAttributeValue(htmlProps, "width");
        if ( attrVal!=null && attrVal.length()>0 ) {
            width = attrVal;
        }

			html.append(" class=\"char");
			html.append(width);
			html.append("\"");
		//Komal Commented
		/*if (!htmlProps.toLowerCase().contains("width")) {
			html.append(" style=\"width:");
			html.append(fieldLength);
			html.append("em\"");
		}*/

		// Set the value of the field if it is not empty
		if (StringFunction.isNotBlank(fieldValue)) {
			logXSSString("createNumberField","fieldValue",fieldValue,StringFunction.xssCharsToHtml(fieldValue));			
			html.append(" value=\"");
			//html.append(StringFunction.xssCharsToHtml(fieldValue));
			html.append(fieldValue);
			html.append("\"");
		}

		// Add in-line validation if the field is required
		if (isRequired) {
			html.append(" required=\"true\" ");
		}

//		rkazi Rel 8.3 T3600021563 10/06/2013 -Add auto complete = off to meet fishnet security requirements - Start
		if (!html.toString().toLowerCase().contains("autocomplete")){
			html.append(" AutoComplete =\"off\" ");
		}
//		rkazi Rel 8.3 T3600021563 10/06/2013 - End

		// Closing tag for the widget
		html.append(">");

		// Put a <div> around the Label and the Field
		wrapInDiv(html, divClass, isReadOnly, isRequired, isExpress);

		// Add hover help
		html.append(createHoverHelp(fieldName, fieldLabel));

		return html.toString();
	}

	/* -------------------------- TEXT AREA -------------------------------- */
	public String createTextArea(String fieldName,
			String fieldLabel,
			String fieldValue,
			boolean isReadOnly) {

		boolean isRequired = false;
		boolean isExpress = false;
		String htmlProps = "";
		String widgetProps = "";
		String divClass = "";
		String maxLength = "";

		return createTextArea(fieldName, fieldLabel, fieldValue, isReadOnly,
				isRequired, isExpress, htmlProps, widgetProps, divClass, maxLength);
	}

	public String createTextArea(String fieldName, String fieldLabel,
			String fieldValue, boolean isReadOnly, boolean isRequired) {

		boolean isExpress = false;
		String htmlProps = "";
		String widgetProps = "";
		String divClass = "";
		String maxLength = "";

		return createTextArea(fieldName, fieldLabel, fieldValue, isReadOnly,
				isRequired, isExpress, htmlProps, widgetProps, divClass, maxLength);
	}
	//MEerupula Rel8.3 included maxLength restriction to the textarea
	public String createTextArea(String fieldName,
			String fieldLabel,
			String fieldValue,
			boolean isReadOnly,
			boolean isRequired,
			boolean isExpress,
			String htmlProps,
			String widgetProps,
			String divClass) {

		String maxLength = "";

		return createTextArea(fieldName, fieldLabel, fieldValue, isReadOnly,
				isRequired, isExpress, htmlProps, widgetProps, divClass, maxLength);

	}


	public String createTextArea(String fieldName,
			String fieldLabel,
			String fieldValue,
			boolean isReadOnly,
			boolean isRequired,
			boolean isExpress,
			String htmlProps,
			String widgetProps,
			String divClass,
			String maxLength) {

		StringBuffer html = new StringBuffer("");
		html.append(createStackedLabel(fieldName, fieldLabel));

		//html.append(renderWidgetId("textarea", "dijit.form.Textarea",fieldName, htmlProps, widgetProps));
		/*Replaced Textarea with SimpleTextarea*/
		logXSSString("createTextArea","fieldName",fieldName,StringFunction.xssCharsToHtml(fieldName));
		html.append(renderWidgetId("textarea", "dijit.form.SimpleTextarea",StringFunction.xssCharsToHtml(fieldName), htmlProps, widgetProps));
		
		if(StringFunction.isNotBlank(maxLength)){
		// Set maximum characters that can be entered in the textarea
		html.append(" maxLength=\"");
		logXSSString("createTextArea","maxLength",maxLength,StringFunction.xssCharsToHtml(maxLength));
		html.append(StringFunction.xssCharsToHtml(maxLength));
		html.append("\"");
		}

		if (isReadOnly) {
			html.append(" readOnly");
		}
//		rkazi Rel 8.3 T3600021563 10/06/2013 -Add auto complete = off to meet fishnet security requirements - Start
		if (!html.toString().toLowerCase().contains("autocomplete")){
			html.append(" AutoComplete =\"off\" ");
		}
//		rkazi Rel 8.3 T3600021563 10/06/2013 - End

		html.append(">");

		if (StringFunction.isNotBlank(fieldValue)) {
			logXSSString("createTextArea","fieldValue",fieldValue,StringFunction.xssCharsToHtml(fieldValue));
			//html.append(StringFunction.xssCharsToHtml(fieldValue));
			html.append(fieldValue);
		}
		html.append("</textarea>");
		wrapInDiv(html, divClass, isReadOnly, isRequired, isExpress);

		//Hover text should appear only for editable components
		if(!isReadOnly){
			html.append(createHoverHelp(fieldName, fieldLabel));
		}

		return html.toString();
	}

	public String createAutoResizeTextArea(String fieldName,
			String fieldLabel,
			String fieldValue,
			boolean isReadOnly,
			boolean isRequired,
			boolean isExpress,
			String htmlProps,
			String widgetProps,
			String divClass) {

		StringBuffer html = new StringBuffer("");		
		html.append(createStackedLabel(fieldName, fieldLabel));
		logXSSString("createAutoResizeTextArea","fieldName",fieldName,StringFunction.xssCharsToHtml(fieldName));
		html.append(renderWidgetId("textarea", "dijit.form.Textarea",StringFunction.xssCharsToHtml(fieldName), htmlProps, widgetProps));
		
		if (isReadOnly) {
		html.append(" readOnly");
		}

//		rkazi Rel 8.3 T3600021563 10/06/2013 -Add auto complete = off to meet fishnet security requirements - Start
		if (!html.toString().toLowerCase().contains("autocomplete")){
			html.append(" AutoComplete =\"off\" ");
		}
//		rkazi Rel 8.3 T3600021563 10/06/2013 - End

		html.append(">");

		if (StringFunction.isNotBlank(fieldValue)) {
			logXSSString("createAutoResizeTextArea","fieldValue",fieldValue,StringFunction.xssCharsToHtml(fieldValue));
			//html.append(StringFunction.xssCharsToHtml(fieldValue));
			html.append(fieldValue);
		}
		html.append("</textarea>");
		wrapInDiv(html, divClass, true, isRequired, isExpress);

		//Hover text should appear only for editable components
		if(!isReadOnly){
			html.append(createHoverHelp(fieldName, fieldLabel));
		}

		return html.toString();
	}
	/**
	 * This method is used to create text area to display PO Line Items.
	 * Since the data needs to be displayed in tabular format, specific styles had to be written.
	 * Hence created a separate method.
	 */
	public static String createPOTextArea(String fieldName,
            String value,
            String cols,
            String rows,
            String style,
            boolean isReadOnly,
            String extraTags,
            String wrapMode,
			 boolean isScrollBar) {
		StringBuilder inputField = new StringBuilder("");
			
		if (isReadOnly && isScrollBar) {
		
			inputField.append("<textarea ");
			
			if (!fieldName.equals("")) {
				inputField.append(" name=");
				logXSSString("createPOTextArea","fieldName",fieldName,StringFunction.xssCharsToHtml(fieldName));
				inputField.append(StringFunction.xssCharsToHtml(fieldName));
			}
			if((wrapMode == null) || wrapMode.equals(""))
				inputField.append(" wrap=VIRTUAL");
			else
				inputField.append(" wrap="+wrapMode);
			
			if (!cols.equals("")) {
				inputField.append(" cols=");
				inputField.append(cols);
			}
			if (!rows.equals("")) {
				inputField.append(" rows=");
				inputField.append(rows);
			}
			if (!style.equals("")) {
				inputField.append(" class=");
				inputField.append(style);
			}
			// IR-IRUK081063995 rkazi BEGIN
			if (!extraTags.toLowerCase().contains("autocomplete")){
				extraTags = extraTags.concat(" AutoComplete =\"off\" ");
			}
			// IR-IRUK081063995 rkazi END
			
			inputField.append(" ");
			inputField.append(extraTags);
			inputField.append(" readOnly =\"true\" ");
			inputField.append(">");
			
			inputField.append(value);
			
			inputField.append("</textarea>");
		} else if (isReadOnly && !isScrollBar) {
			// Replace new lines with <br> tags
			value = formatStringWithBR(value);
			
			inputField.append("<table width=");
			
			if( (cols != null) && (!cols.equals("")) )
			{
				Integer widthVal = new Integer(cols);
			if (fieldName.equals("GoodsDescText") || fieldName.equals("POLineItems"))
			{
				widthVal = new Integer(widthVal.intValue() * 8 + 12);
			}
			else
			{
				widthVal = new Integer( widthVal.intValue() * 5 );
			}
			
				inputField.append( "\"" + widthVal.toString() + "\" border=1 ");
			}else{
				inputField.append("100% border=1 ");
			}
				inputField.append("cellspacing=0 cellpadding=3>");
				inputField.append("<tr><td>");
			if (!style.equals("")) {
				inputField.append("<p class=");
				inputField.append(style);
				inputField.append(">");
			}
			// Now output the value.  If value is "" or null, output
			// a non-breaking space.
			if ((value == null) || value.trim().equals("")) {
				inputField.append("&nbsp;");
			} else {
				inputField.append(value);
			}
			// Close all the tags
			inputField.append("</p></td></tr></table>");
		} 
		else {
		
			// [BEGIN] PPX-050-02 - hml
			// [BEGIN] PPX-050 - jkok
			//value = StringFunction.xssCharsToHtml(value);
			// [END] PPX-050 - jkok
			// [END] PPX-050-02 - hml
			inputField.append("<textarea ");
			
			if (!fieldName.equals("")) {
			inputField.append(" name=");
			inputField.append(fieldName);
			}
			if((wrapMode == null) || wrapMode.equals(""))
			inputField.append(" wrap=VIRTUAL");
			else
			inputField.append(" wrap="+wrapMode);
			
			if (!cols.equals("")) {
			inputField.append(" cols=");
			inputField.append(cols);
			}
			if (!rows.equals("")) {
			inputField.append(" rows=");
			inputField.append(rows);
			}
			if (!style.equals("")) {
			inputField.append(" class=");
			inputField.append(style);
			}
			// IR-IRUK081063995 rkazi BEGIN
			if (!extraTags.toLowerCase().contains("autocomplete")){
			extraTags = extraTags.concat(" AutoComplete =\"off\" ");
			}
			// IR-IRUK081063995 rkazi END
			
			inputField.append(" ");
			inputField.append(extraTags);
			inputField.append(" readOnly =\"true\" ");
			inputField.append(">");
			
			inputField.append(value);
			
			inputField.append("</textarea>");
		}

		return inputField.toString();
	}
	/* -------------------------- DATE FIELD ------------------------------- */
	public String createDateField(String fieldName, String fieldLabel,
			String fieldValue, boolean isReadOnly) {

		String loginLocale = userSession.getUserLocale();
		String language = loginLocale.substring(0, loginLocale.indexOf("_"));
		boolean isRequired = false;
		boolean isExpress = false;
		String htmlParms = "";
		String widgetParms = "lang:'"+language+"'";
		String divClass = "";
		String constraints = "{datePattern:'"+userSession.getDatePattern()+"'}";

		return createDateField(fieldName, fieldLabel, fieldValue, isReadOnly,
				isRequired, isExpress, htmlParms, widgetParms,
				divClass, constraints);
	}

	public String createDateField(String fieldName, String fieldLabel,
			String fieldValue, boolean isReadOnly, boolean isRequired) {

		String loginLocale = userSession.getUserLocale();
		String language = loginLocale.substring(0, loginLocale.indexOf("_"));
		boolean isExpress = false;
		String htmlParms = "";
		String widgetParms = "lang:'"+language+"'";
		String divClass = "";
		String constraints = "{datePattern:'"+userSession.getDatePattern()+"'}";

		return createDateField(fieldName, fieldLabel, fieldValue, isReadOnly,
				isRequired, isExpress, htmlParms, widgetParms,
				divClass, constraints);
	}

	public String createDateField(String fieldName, String fieldLabel,
			String fieldValue, boolean isReadOnly, boolean isRequired,
			boolean isExpress, String htmlProps,
			String widgetProps, String divClass) {

		String constraints = "{datePattern:'"+userSession.getDatePattern()+"'}";

		return createDateField(fieldName, fieldLabel, fieldValue, isReadOnly,
				isRequired, isExpress, htmlProps, widgetProps,
				divClass, constraints);

	}

public String createDateField(String fieldName, String fieldLabel,
			String fieldValue, boolean isReadOnly, boolean isRequired,
			boolean isExpress, String htmlProps,
			String widgetProps, String divClass, String constraints) {

		StringBuffer html = new StringBuffer("");

				

		html.append(createStackedLabel(fieldName, fieldLabel));

		if(!isReadOnly) {
			// Get rid of the time portion of the date
			int posOfSpace = fieldValue.indexOf(" ");
			if (posOfSpace > 0) {
				fieldValue = fieldValue.substring(0, posOfSpace);
				fieldValue = TPDateTimeUtility.convertJPylonDateToISODate(fieldValue);
			}
			else if (fieldValue.length() == 10 && posOfSpace == -1) {
				fieldValue = TPDateTimeUtility.convertJPylonDateToISODate(fieldValue);
			}
		}

		if (isReadOnly) {
			html.append(createReadOnlyDateField(fieldName, fieldValue));
			wrapInDiv(html, divClass, isReadOnly, isRequired, isExpress);
			return html.toString();
		}
		logXSSString("createDateField","fieldName",fieldName,StringFunction.xssCharsToHtml(fieldName));	
		html.append(renderWidgetId("input", "dijit.form.DateTextBox",
				StringFunction.xssCharsToHtml(fieldName), StringFunction.xssCharsToHtml(fieldName), htmlProps, widgetProps, constraints));

		if (StringFunction.isNotBlank(fieldValue)) {
			html.append(" value=\"");
			logXSSString("createDateField","fieldValue",fieldValue,StringFunction.xssCharsToHtml(fieldValue));
			html.append(fieldValue);
			html.append("\"");
		}
//		rkazi Rel 8.3 T3600021563 10/06/2013 -Add auto complete = off to meet fishnet security requirements - Start
		if (!html.toString().toLowerCase().contains("autocomplete")){
			html.append(" AutoComplete =\"off\" ");
		}
//		rkazi Rel 8.3 T3600021563 10/06/2013 - End

		html.append(">");

		wrapInDiv(html, divClass, isReadOnly, isRequired, isExpress);
		
		html.append(createHoverHelp(fieldName, fieldLabel));

		return html.toString();
	}

	private String createReadOnlyDateField(String fieldName, String fieldValue) {

		if (StringFunction.isNotBlank(fieldValue)) {
			fieldValue = TPDateTimeUtility.formatDate(fieldValue, TPDateTimeUtility.SHORT, resMgr.getResourceLocale());
		}
		
		return createReadOnlyTextField( fieldName,  fieldValue);
	}

	public String createSearchDateField(String fieldName, String fieldLabel) {

		return createSearchDateField(fieldName, fieldLabel, "");
	}

	public String createSearchDateField(String fieldName, String fieldLabel, String htmlProps) {
        //cquinton 9/27/2012 get width modifiers first -
        // if labelClass is specified we need to set that as well
		String widgetProps ="";
		return createSearchDateField(fieldName, fieldLabel, htmlProps, widgetProps);       
	}
	//Added to display default dateformat
	public String createSearchDateField(String fieldName, String fieldLabel, String htmlProps, String widgetProps) {
        //cquinton 9/27/2012 get width modifiers first -
        // if labelClass is specified we need to set that as well
        String labelClass=null; //by default just layout based on text size

        String attrVal=getHtmlAttributeValue(htmlProps, "labelClass");

		logXSSString("createSearchDateField","fieldName",fieldName,StringFunction.xssCharsToHtml(fieldName));

        if ( attrVal!=null && attrVal.length()>0 ) {
            labelClass = attrVal;
        }

		StringBuilder html = new StringBuilder("");

		html.append(NL);
		html.append("<div class=\"searchItem\" >");

        // Create the label that will identify the field
        if ( labelClass!=null ) {
            //if label width is given reuse char classes on the label
            html.append(createInlineLabel(fieldName, fieldLabel,
                            "class=\""+labelClass+"\""));
        }
        else {
            html.append(createInlineLabel(fieldName, fieldLabel));
        }
        // W Zhu Rel 8.2 T36000016357 add constraints for date pattern
		String constraints = "";
                if (userSession!=null) constraints = "{datePattern:'"+userSession.getDatePattern()+"'}";

		html.append(renderWidgetId("input", "dijit.form.DateTextBox", StringFunction.xssCharsToHtml(fieldName), StringFunction.xssCharsToHtml(fieldName), htmlProps, widgetProps, constraints));
//		rkazi Rel 8.3 T3600021563 10/06/2013 -Add auto complete = off to meet fishnet security requirements - Start
		if (!html.toString().toLowerCase().contains("autocomplete")){
			html.append(" AutoComplete =\"off\" ");
		}
//		rkazi Rel 8.3 T3600021563 10/06/2013 - End

		// Closing tag for the widget
		html.append(">");

		// Put a <div> around the Label and the Field
		html.append("</div>");

		// Add hover help
		html.append(createHoverHelp(fieldName, fieldLabel));

		return html.toString();
	}

	/* -------------------------- AMOUNT FIELD ----------------------------- */
	public String createAmountField(String fieldName, String fieldLabel,
			String fieldValue, String currencyCode, boolean isReadOnly,
			boolean isRequired) {

		boolean isExpress = false;
		String htmlParms = "";
		String widgetParms = "";
		String divClass = "";

		return createAmountField(fieldName, fieldLabel, fieldValue,
				currencyCode, isReadOnly, isRequired, isExpress,
				htmlParms, widgetParms, divClass);
	}

	public String createAmountField(String fieldName, String fieldLabel,
			String fieldValue, String currencyCode, boolean isReadOnly,
			boolean isRequired, boolean isExpress,
			String htmlProps, String widgetProps, String divClass) {

		StringBuffer html = new StringBuffer("");

		html.append(createStackedLabel(fieldName, fieldLabel));

		if (isReadOnly) {
			html.append(createReadOnlyAmountField(fieldName, fieldValue, currencyCode));
			wrapInDiv(html, divClass, isReadOnly, isRequired, isExpress);
			return html.toString();
		}

//		html.append(renderWidgetId("input", "dijit.form.CurrencyTextBox",
//				fieldName, htmlProps, widgetProps));
		logXSSString("createAmountField","fieldName",fieldName,StringFunction.xssCharsToHtml(fieldName));
		html.append(renderWidgetId("input", "dijit.form.ValidationTextBox",
				fieldName, htmlProps, widgetProps));

		html.append(" currency=\"");
		logXSSString("createAmountField","currencyCode",currencyCode,StringFunction.xssCharsToHtml(currencyCode));
		html.append(currencyCode);
		html.append("\" ");

		if (isRequired) {
			html.append(" required ");
		}

		//if (!fieldValue.equals("")) {
		if (StringFunction.isNotBlank(fieldValue)) {
			html.append(" value=\"");
			logXSSString("createAmountField","fieldValue",fieldValue,StringFunction.xssCharsToHtml(fieldValue));
			html.append(fieldValue);
			html.append("\"");
		}
//		rkazi Rel 8.3 T3600021563 10/06/2013 -Add auto complete = off to meet fishnet security requirements - Start
		if (!html.toString().toLowerCase().contains("autocomplete")){
			html.append(" AutoComplete =\"off\" ");
		}
//		rkazi Rel 8.3 T3600021563 10/06/2013 - End

		html.append(">");

		wrapInDiv(html, divClass, isReadOnly, isRequired, isExpress);

		html.append(createHoverHelp(fieldName, fieldLabel));

		return html.toString();
	}

	private String createReadOnlyAmountField(String fieldName,
			String fieldValue,
			String currencyCode) {

		return createReadOnlyTextField( fieldName,  fieldValue);
	}

	public String createSearchAmountField(String fieldName, String fieldLabel, String currencyCode) {

			return createSearchAmountField(fieldName, fieldLabel, currencyCode, "");
	}

	public String createSearchAmountField(String fieldName, String fieldLabel,
			String currencyCode, String htmlProps) {
		
		return createSearchAmountField( fieldName,  fieldLabel,
				 currencyCode,  htmlProps, "") ;
       
	}

	public String createSearchAmountField(String fieldName, String fieldLabel,
			String currencyCode, String htmlProps, String widgetProps) {
        //cquinton 9/27/2012 get width modifiers first -
        // if labelClass is specified we need to set that as well
        String labelClass=null; //by default just layout based on text size

        String attrVal=getHtmlAttributeValue(htmlProps, "labelClass");
        if ( attrVal!=null && attrVal.length()>0 ) {
            labelClass = attrVal;
        }

		StringBuilder html = new StringBuilder("");

		html.append(NL);
		html.append("<div class=\"searchItem\" >");

        // Create the label that will identify the field
        if ( labelClass!=null ) {
            //if label width is given reuse char classes on the label
            html.append(createInlineLabel(fieldName, fieldLabel,
                            "class=\""+labelClass+"\""));
        }
        else {
            html.append(createInlineLabel(fieldName, fieldLabel));
        }

//		html.append(renderWidgetId("input", "dijit.form.CurrencyTextBox",
//				fieldName, htmlProps, widgetProps));
		html.append(renderWidgetId("input", "dijit.form.ValidationTextBox",
				fieldName, htmlProps, widgetProps));

		html.append(" currency=\"");
		html.append(currencyCode);
		html.append("\" ");
//		rkazi Rel 8.3 T3600021563 10/06/2013 -Add auto complete = off to meet fishnet security requirements - Start
		if (!html.toString().toLowerCase().contains("autocomplete")){
			html.append(" AutoComplete =\"off\" ");
		}
//		rkazi Rel 8.3 T3600021563 10/06/2013 - End

		html.append(">");

		// Put a <div> around the Label and the Field
		html.append("</div>");

		html.append(createHoverHelp(fieldName, fieldLabel));

		return html.toString();
	}


	/* -------------------------- CHECK BOX -------------------------------- */
	public String createCheckboxField(String fieldName, String fieldLabel,
			boolean isChecked, boolean isReadOnly) {

		boolean isExpress = false;
		String htmlProps = "";
		String widgetProps = "";
		String divClass = "";

		return createCheckboxField(fieldName, fieldLabel,
		        isChecked, isReadOnly, isExpress,
		        htmlProps, widgetProps, divClass);
	}

	//cquinton 11/10/2012 refactored here so we don't have bunch of copied code
    public String createCheckboxField(String fieldName, String fieldLabel,
            boolean isChecked, boolean isReadOnly, boolean isExpress,
            String htmlProps, String widgetProps, String divClass) {

        String defaultValue = "Y";

        return createCheckboxField(fieldName, fieldLabel, defaultValue,
            isChecked, isReadOnly, isExpress,
            htmlProps, widgetProps, divClass);
    }

    
    public String createCheckboxField(String fieldName, String fieldLabel, String defaultValue,
			boolean isChecked, boolean isReadOnly, boolean isExpress,
			String htmlProps, String widgetProps, String divClass) {
    	
    	return createCheckboxField( fieldName, fieldName,  fieldLabel,  defaultValue,
    			 isChecked,  isReadOnly,  isExpress,
    			 htmlProps,  widgetProps,  divClass);
    }
    
    
	public String createCheckboxField(String fieldName, String fieldId, String fieldLabel, String defaultValue,
			boolean isChecked, boolean isReadOnly, boolean isExpress,
			String htmlProps, String widgetProps, String divClass) {
		StringBuffer html = new StringBuffer("");

        //cquinton 11/10/2012 get width modifiers
        // if labelClass is specified we need to set that
        String labelClass=null; //by default just layout based on text size
        String attrVal=getHtmlAttributeValue(htmlProps, "labelClass");
        if ( attrVal!=null && attrVal.length()>0 ) {
            labelClass = attrVal;
        }

		html.append(renderWidgetId("input", "dijit.form.CheckBox", fieldName,fieldId,
				htmlProps, widgetProps));

		html.append(" value=\""+defaultValue+"\" ");

		if (isReadOnly) {
			html.append(" disabled ");
		}

		if (isChecked) {
			html.append(" checked ");
		}

		html.append(">");

        // Create the label that will identify the field
        //apply checkBoxLabel class
        if ( labelClass!=null ) {
            labelClass += " checkBoxLabel";
        }
        else {
            labelClass = "checkBoxLabel";
        }
        //if label width is given reuse char classes on the label
        html.append(createInlineLabel(fieldName, fieldLabel,
            "class=\""+labelClass+"\""));

		wrapInDiv(html, divClass, isReadOnly, false, isExpress);

		// Add hover help
		html.append(createHoverHelp(fieldName, fieldLabel));

		return html.toString();
	}

	//Jyoti 26 July,2012 IR-3679
	public String createCheckboxField(String fieldName, String fieldLabel,
			boolean isChecked, boolean isReadOnly, boolean isExpress,
			String htmlProps, String widgetProps, String divClass,String searchButton) {

		StringBuffer html = new StringBuffer("");

		html.append(renderWidgetId("input", "dijit.form.CheckBox", fieldName,
				htmlProps, widgetProps));

		html.append(" value=\"Y\" ");

		if (isReadOnly) {
			html.append(" disabled ");
		}

		if (isChecked) {
			html.append(" checked ");
		}

		html.append(">");

		html.append(createInlineLabel(fieldName, fieldLabel));
		if(null!=searchButton){
			html.append(searchButton);
		}
		wrapInDiv(html, divClass, isReadOnly, false, isExpress);

		//Hover text should appear only for editable components
		if(!isReadOnly){
			// Add hover help
			html.append(createHoverHelp(fieldName, fieldLabel));
		}


		return html.toString();
	}
	//IR-3679 Ended



	public String createSearchCheckboxField(String fieldName, String fieldLabel,
			boolean isChecked) {
        //cquinton 9/27/2012 get width modifiers first -
        // if labelClass is specified we need to set that as well
        String labelClass=null; //by default just layout based on text size

         //temp comment this out as this method doesn't allow extra html properties
        //String attrVal=getHtmlAttributeValue(htmlProps, "labelClass");
        //if ( attrVal!=null && attrVal.length()>0 ) {
        //    labelClass = attrVal;
        //}

		StringBuilder html = new StringBuilder("");

		html.append("<div class=\"searchItem\" >");
		logXSSString("createSearchCheckboxField","fieldName",fieldName,StringFunction.xssCharsToHtml(fieldName));
		html.append(renderWidgetId("input", "dijit.form.CheckBox",StringFunction.xssCharsToHtml(fieldName) ,
				"", ""));

		html.append(" value=\"Y\" ");


		if (isChecked) {
			html.append(" checked ");
		}

		html.append(">");

        // Create the label that will identify the field
        if ( labelClass!=null ) {
            //if label width is given reuse char classes on the label
            html.append(createInlineLabel(fieldName, fieldLabel,
                            "class=\""+labelClass+"\""));
        }
        else {
            html.append(createInlineLabel(fieldName, fieldLabel));
        }

		html.append("</div>");

		// Add hover help
		html.append(createHoverHelp(fieldName, fieldLabel));

		return html.toString();
	}

	/* -------------------------- SELECT FIELD ----------------------------- */
	public String createSelectField(String fieldName, String fieldLabel,
			String defaultText, String options, boolean isReadOnly) {

		boolean isRequired = false;
		boolean isExpress = false;
		String htmlParms = "";
		String widgetParms = "";
		String divClass = "";

		return createSelectField(fieldName, fieldLabel, defaultText, options,
				isReadOnly, isRequired, isExpress, htmlParms,
				widgetParms, divClass);
	}

	public String createSelectField(String fieldName, String fieldLabel,
			String defaultText, String options, boolean isReadOnly,
			boolean isRequired, boolean isExpress,
			String htmlProps, String widgetProps, String divClass) {

		StringBuffer html = new StringBuffer("");		

		html.append(createStackedLabel(fieldName, fieldLabel));

		if (isReadOnly) {
			html.append(createReadOnlySelectField(fieldName, options));
			// html.append(CLOSING_DIV);
			wrapInDiv(html, divClass, isReadOnly, isRequired, isExpress);
			return html.toString();
		}

        //cquinton 11/29/2012 use custom widget that includes special common functionality
		logXSSString("createSelectField","fieldName",fieldName,StringFunction.xssCharsToHtml(fieldName));
		html.append(renderWidgetId("select", "t360.widget.FilteringSelect",
				StringFunction.xssCharsToHtml(fieldName), htmlProps, widgetProps));
			html.append(">");
		if (!defaultText.equals("")) {
			html.append("<option value=\"");
			html.append("");
			html.append("\">");
			//logXSSString("createSelectField","defaultText",defaultText,StringFunction.xssCharsToHtml(defaultText));
			//html.append(StringFunction.xssCharsToHtml(defaultText));
			html.append(defaultText);
			html.append("</option>");
		}

		html.append(options);

		html.append("</select>");

		wrapInDiv(html, divClass, isReadOnly, isRequired, isExpress);

		// Add hover help
		html.append(createHoverHelp(fieldName, fieldLabel));

		return html.toString();
	}
	
	public String createSelectFieldFromStore(String fieldName, String fieldLabel,
            String defaultText, String fieldValue, boolean isReadOnly,
            boolean isRequired, boolean isExpress,
            String htmlProps, String widgetProps, String divClass) {

     StringBuffer html = new StringBuffer("");

     html.append(createStackedLabel(fieldName, fieldLabel));

     if (isReadOnly) {
            html.append(createReadOnlySelectField(fieldName, fieldValue));
            wrapInDiv(html, divClass, isReadOnly, isRequired, isExpress);
            return html.toString();
     }


     html.append(renderWidgetId("input", "t360.widget.FilteringSelect",
                  fieldName, htmlProps, widgetProps));
	
     if (StringFunction.isNotBlank(fieldValue)) {
            html.append(" value=\"");
            logXSSString("createTextField","fieldValue",fieldValue,StringFunction.xssCharsToHtml(fieldValue));            
            //html.append(StringFunction.xssCharsToHtml(fieldValue));   
            html.append(fieldValue); 
            html.append("\"");
     }      
     
            html.append("/>");

     wrapInDiv(html, divClass, isReadOnly, isRequired, isExpress);

     // Add hover help
     html.append(createHoverHelp(fieldName, fieldLabel));

     return html.toString();
}


	private String createReadOnlySelectField(String fieldName, String options) {

		String fieldValue = getSelectedOption(options);

		return createReadOnlyTextField( fieldName,  fieldValue);
	}

	private String getSelectedOption(String options) {
		String textValue = "";

		int select = options.indexOf("selected");

		if (select > -1) {
			int start = options.indexOf(">", select);
			int end = options.indexOf("</option>", select);

			try {
				textValue = options.substring(start + 1, end);
			} catch (Exception e) {
				textValue = "";
			}
		}

		if (textValue.equals(""))
			textValue = "&nbsp;";

		return textValue;
	}
	public String createSearchSelectField(String fieldName, String fieldLabel,String defaultText, String options) {

		return createSearchSelectField(fieldName, fieldLabel, defaultText, options, "");
	}

	public String createSearchSelectField(String fieldName, String fieldLabel,
			String defaultText, String options, String htmlProps) {
        //cquinton 9/27/2012 get width modifiers first -
        // if labelClass is specified we need to set that as well
        String labelClass=null; //by default just layout based on text size

        String attrVal=getHtmlAttributeValue(htmlProps, "labelClass");
        if ( attrVal!=null && attrVal.length()>0 ) {
            labelClass = attrVal;
        }

		StringBuilder html = new StringBuilder("");
		html.append("<div class=\"searchItem\" >");

        // Create the label that will identify the field
        if ( labelClass!=null ) {
            //if label width is given reuse char classes on the label
            html.append(createInlineLabel(fieldName, fieldLabel,
                            "class=\""+labelClass+"\""));
        }
        else {
            html.append(createInlineLabel(fieldName, fieldLabel));
        }

        //cquinton 11/29/2012 use custom widget that includes special common functionality
		html.append(renderWidgetId("select", "t360.widget.FilteringSelect", fieldName, htmlProps, ""));

		html.append(">");
		if (!defaultText.equals("")) {
			html.append("<option value=\"");
			html.append("");
			html.append("\">");
			html.append(defaultText);
			html.append("</option>");
		}

		html.append(options);

		html.append("</select>");

		html.append("</div>");

		return html.toString();
	}

	public String createSearchSelectField(String fieldName, String fieldLabel,
			String defaultText, String options, boolean isReadOnly, String htmlProps) {
        //cquinton 9/27/2012 get width modifiers first -
        // if labelClass is specified we need to set that as well
        String labelClass=null; //by default just layout based on text size

        String attrVal=getHtmlAttributeValue(htmlProps, "labelClass");
        if ( attrVal!=null && attrVal.length()>0 ) {
            labelClass = attrVal;
        }

		StringBuilder html = new StringBuilder("");
		//html.append("<div class=\"searchItem\" >");
        // Create the label that will identify the field
        if ( labelClass!=null ) {
            //if label width is given reuse char classes on the label
            html.append(createInlineLabel(fieldName, fieldLabel,
                            "class=\""+labelClass+"\""));
        }
        else {
            html.append(createInlineLabel(fieldName, fieldLabel));
        }

		if (isReadOnly) {
			html.append(createReadOnlySelectField(fieldName, options));
			return html.toString();
		}

        //cquinton 11/29/2012 use custom widget that includes special common functionality
		html.append(renderWidgetId("select", "t360.widget.FilteringSelect", fieldName, htmlProps, ""));

		html.append(">");
		if (!defaultText.equals("")) {
			html.append("<option value=\"");
			html.append("");
			html.append("\">");
			logXSSString("createSearchSelectField","defaultText",defaultText,StringFunction.xssCharsToHtml(defaultText));
			html.append(StringFunction.xssCharsToHtml(defaultText));
			html.append("</option>");
		}

		html.append(options);

		html.append("</select>");

		//html.append("</div>");

		// Add hover help
		html.append(createHoverHelp(fieldName, fieldLabel));

		return html.toString();
	}

	public String createSearchSelectField(String fieldName, String fieldLabel,
			String defaultText, String defaultValue, String options, boolean isReadOnly, String htmlProps) {
        //cquinton 9/27/2012 get width modifiers first -
        // if labelClass is specified we need to set that as well
        String labelClass=null; //by default just layout based on text size

        String attrVal=getHtmlAttributeValue(htmlProps, "labelClass");
        if ( attrVal!=null && attrVal.length()>0 ) {
            labelClass = attrVal;
        }

		StringBuilder html = new StringBuilder("");
		//html.append("<div class=\"searchItem\" >");
        // Create the label that will identify the field
        if ( labelClass!=null ) {
            //if label width is given reuse char classes on the label
            html.append(createInlineLabel(fieldName, fieldLabel,
                            "class=\""+labelClass+"\""));
        }
        else {
            html.append(createInlineLabel(fieldName, fieldLabel));
        }

		if (isReadOnly) {
			html.append(createReadOnlySelectField(fieldName, options));
			return html.toString();
		}

        //cquinton 11/29/2012 use custom widget that includes special common functionality
		html.append(renderWidgetId("select", "t360.widget.FilteringSelect", fieldName, htmlProps, ""));

		html.append(">");
		if (!defaultText.equals("")) {
			html.append("<option value=\"");
			logXSSString("createSearchSelectField","defaultValue",defaultValue,StringFunction.xssCharsToHtml(defaultValue));
			html.append(defaultValue);
			html.append("\">");
			logXSSString("createSearchSelectField","defaultText",defaultText,StringFunction.xssCharsToHtml(defaultText));
			html.append(StringFunction.xssCharsToHtml(defaultText));
			html.append("</option>");
		}

		html.append(options);

		html.append("</select>");

		//html.append("</div>");

		// Add hover help
		html.append(createHoverHelp(fieldName, fieldLabel));

		return html.toString();
	}

	public String createCurrencySelectField(String fieldName, String fieldLabel,
			String options, String associatedAmount, boolean isReadOnly,
			boolean isRequired, boolean isExpress) {

		String divClass = "inline currency";
		String htmlParms = "onChange= 'setCurrencyForAmount(this.value,\"" + associatedAmount + "\");'";
		String widgetParms = "";
		String defaultText = " ";

		return createSelectField(fieldName, fieldLabel, defaultText, options,
				isReadOnly, isRequired, isExpress, htmlParms,
				widgetParms, divClass);
	}

	public String createCurrencySearchField(String fieldName, String fieldLabel, String options,
			String associatedAmount) {

		StringBuilder html = new StringBuilder("");
		html.append("<div class=\"searchItem currency\" >");
		html.append(createInlineLabel(fieldName, fieldLabel));

		String htmlParms = "onChange='setCurrency(this.value,\"" + associatedAmount + "\");'";
        //cquinton 11/29/2012 use custom widget that includes special common functionality
		html.append(renderWidgetId("select", "t360.widget.FilteringSelect", fieldName, htmlParms, ""));

		html.append(">");

		html.append(options);

		html.append("</select>");

		html.append("</div>");

		return html.toString();

	}



	/* -------------------------- RADIO BUTTON ----------------------------- */
	public String createRadioButtonField(String groupName, String fieldName,
			String fieldLabel, String valueIfSelected, boolean isChecked,
			boolean isReadOnly) {

		return createRadioButtonField(groupName, fieldName, fieldLabel,
				valueIfSelected, isChecked, isReadOnly, "", "");
	}

	public String createRadioButtonField(String groupName, String fieldName,
			String fieldLabel, String valueIfSelected, boolean isChecked,
			boolean isReadOnly, String htmlProps, String widgetProps) {

		StringBuilder html = new StringBuilder("");

        //cquinton 11/10/2012 get width modifiers
        // if labelClass is specified we need to set that
		String labelClass=null; //by default just layout based on text size
        String attrVal=getHtmlAttributeValue(htmlProps, "labelClass");
        if ( attrVal!=null && attrVal.length()>0 ) {
            labelClass = attrVal;
        }

		html.append(renderWidgetId("input", "dijit.form.RadioButton", groupName, fieldName, htmlProps, widgetProps));

		html.append(" value=\"");
		html.append(valueIfSelected);
		html.append("\" ");

		if (isReadOnly) {
			html.append(" READONLY ");
		}

		if (isChecked) {
			html.append(" CHECKED ");
		}

		html.append(">");

        // Create the label that will identify the field
        //apply radioButtonLabel class
        if ( labelClass!=null ) {
            labelClass += " radioButtonLabel";
        }
        else {
            labelClass = "radioButtonLabel";
        }
        //if label width is given reuse char classes on the label
        html.append(createInlineLabel(fieldName, fieldLabel,
            "class=\""+labelClass+"\""));

      //Hover text should appear only for editable components
		if(!isReadOnly){
			html.append(createHoverHelp(fieldName, fieldLabel));
		}

		return html.toString();
	}

	public String createSearchRadioButtonField(String groupName,
			String fieldName,
			String fieldLabel,
			String valueIfSelected,
			boolean isChecked) {
        //cquinton 9/27/2012 get width modifiers first -
        // if labelClass is specified we need to set that as well
        String labelClass=null; //by default just layout based on text size

        //ctq temp comment out as radio button does allow extra html properties
        //String attrVal=getHtmlAttributeValue(htmlProps, "labelClass");
        //if ( attrVal!=null && attrVal.length()>0 ) {
        //    labelClass = attrVal;
        //}

		StringBuilder html = new StringBuilder("");
		html.append("<div class=\"searchItem\">");
		html.append(renderWidgetId("input", "dijit.form.RadioButton", groupName, fieldName, "", ""));

		html.append(" value=\"");
		html.append(valueIfSelected);
		html.append("\" ");

		if (isChecked) {
			html.append(" checked ");
		}

		html.append(">");

        // Create the label that will identify the field
        if ( labelClass!=null ) {
            //if label width is given reuse char classes on the label
            html.append(createInlineLabel(fieldName, fieldLabel,
                            "class=\""+labelClass+"\""));
        }
        else {
            html.append(createInlineLabel(fieldName, fieldLabel));
        }

		html.append("</div>");

		html.append(createHoverHelp(fieldName, fieldLabel));

		return html.toString();
	}

	/* -------------------------- LABEL ------------------------------------ */

	public String createStackedLabel(String fieldName,
			String fieldLabel) {
		String label = createFieldLabel(fieldName, fieldLabel);
		if (label.equals("")) {
			return label;
		} else {
            return label + "<br>";
            //cquinton 9/4/2012 remove explicit style
            //style changes should be made in style sheet instead
            //this particular style however is specifically removed as it
            // added too much space between lable and value.
            //+ "<div style=\"padding-top:5px\"></div>";

		}
	}

	public String createInlineLabel(String fieldName,
			String fieldLabel) {
	    return createInlineLabel(fieldName, fieldLabel, null);
	}
    public String createInlineLabel(String fieldName,
            String fieldLabel, String htmlProps) {
		return createFieldLabel(fieldName, fieldLabel, htmlProps);
	}

    private String createFieldLabel(String fieldName,
            String fieldLabel) {
        return createFieldLabel(fieldName, fieldLabel, null);
    }
	private String createFieldLabel(String fieldName,
			String fieldLabel, String htmlProps) {

		if (fieldLabel.equals("")) {
			return "";
		}

		StringBuilder html = new StringBuilder("");

		if (fieldName.equals("")) {
			html.append("<label");
		} else {
			html.append("<label for=\"");
			logXSSString("createFieldLabel","fieldName",fieldName,StringFunction.xssCharsToHtml(fieldName));
			html.append(StringFunction.xssCharsToHtml(fieldName));
			html.append("\"");
		}
		if ( htmlProps!=null && htmlProps.length()>0 ) {
		    html.append(" ");
		    html.append(htmlProps);
		}
        html.append(">");

		html.append(resMgr.getText(fieldLabel, TradePortalConstants.TEXT_BUNDLE));

		html.append("</label>");

		return html.toString();
	}


	public String createLabel(String fieldName,
			String fieldLabel,
			boolean isReadOnly,
			boolean isRequired,
			boolean isExpress,
			String divClass) {

	    //cquinton 11/20/2012 refactor
	    return createLabel(fieldName, fieldLabel,
	        isReadOnly, isRequired, isExpress, null, divClass);
	}

	//cquinton 11/20/2012 add new version that takes labelClass
    public String createLabel(String fieldName,
            String fieldLabel,
            boolean isReadOnly,
            boolean isRequired,
            boolean isExpress,
            String htmlProps,
            String divClass) {
        String labelClass=null; //by default no classes

        String attrVal=getHtmlAttributeValue(htmlProps, "labelClass");
        if ( attrVal!=null && attrVal.length()>0 ) {
            labelClass = attrVal;
        }


        StringBuffer html = new StringBuffer("");

        // Create the label that will identify the field
        if ( labelClass!=null ) {
            //apply labelClass if given
            html.append(createFieldLabel(fieldName, fieldLabel,
                            "class=\""+labelClass+"\""));
        }
        else {
            html.append(createFieldLabel(fieldName, fieldLabel));
        }

        wrapInDiv(html, divClass, isReadOnly, isRequired, isExpress);

        return html.toString();
    }

	/* -------------------------- SUB LABEL -------------------------------- */
	//SSikhakolli - Rel-9.5 refactor
    public String createSubLabel(String fieldLabel) {
		return createSubLabel(fieldLabel, "subLabel");
	}
	
	public String createSubLabel(String fieldLabel, String divClass) {
		if (fieldLabel.equals("")) {
			return "";
		}
		StringBuilder html = new StringBuilder("");
		html.append("<span class=\""+divClass+"\">");
		html.append(resMgr.getText(fieldLabel, TradePortalConstants.TEXT_BUNDLE));
		html.append("</span>");
		return html.toString();
	}
	
	//SSikhakolli - Rel-9.4 CR-818 - Introducing new method which returns subLable in bold
	public String createBoldSubLabel(String fieldLabel) {
		if (fieldLabel.equals("")) {
			return "";
		}
		StringBuilder html = new StringBuilder("");
		html.append("<span class=\"boldSubLabel\">");
		html.append(resMgr.getText(fieldLabel, TradePortalConstants.TEXT_BUNDLE));
		html.append("</span>");
		return html.toString();
	}

	/* -------------------------- NOTE -------------------------------- */
	public String createNote(String fieldLabel) {
	    return createNote(fieldLabel, "");
	}
    public String createNote(String fieldLabel, String divClass) {

        if (fieldLabel.equals("")) {
            return "";
        }

        StringBuilder html = new StringBuilder("");
        
        String classes = "note";
        if ( !StringFunction.isBlank(divClass) ) {
            classes += " " + divClass;
        }

        html.append("<span class=\"" + classes + "\">");

        html.append(resMgr
                .getText(fieldLabel, TradePortalConstants.TEXT_BUNDLE));

        html.append("</span>");

        return html.toString();
    }

	/* -------------------------- SECTION HEADER --------------------------- */
    public String createSectionHeader(String sectionNumber, String sectionLabel) {
        return createSectionHeader(sectionNumber, sectionLabel, null);
    }
    
    //Added to include section number in the section label
    public String createSectionHeader(String sectionNumber, String sectionLabel, String divClass) {
        return createSectionHeader(sectionNumber, sectionLabel, null, false);
    }
    
	public String createSectionHeader(String sectionNumber, String sectionLabel, String divClass, boolean includeSectionNumber) {

		StringBuilder html = new StringBuilder("");
        
        //cquinton 2/1/2013 set the titlepane non-toggleable 
		// if the noToggle class is passed in
		String widgetProps = "";
        if ( divClass!=null && divClass.indexOf("noToggle")>=0 ) {
            widgetProps = "toggleable: false";
        }
		html.append(renderWidgetId("div", "dijit.TitlePane", "section" + sectionNumber, 
		    "", widgetProps));
		
		if ( divClass == null || divClass.length()<=0 ) {
		    divClass = "section";
		} else {
		    divClass = "section " + divClass;
		}
		html.append(" class=\"" + divClass + "\" ");
		html.append(" title=\"");

		if(includeSectionNumber){
		html.append(sectionNumber).append(". ");
		}
		html.append(resMgr.getText(sectionLabel, TradePortalConstants.TEXT_BUNDLE));
		html.append("\">");

		html.append(NL);
		html.append(CLEAR_FIX);

		return html.toString();

	}

	/* -------------------------- SUBSECTION HEADER ------------------------ */
	public String createSubsectionHeader(String subsectionLabel) {

		String divClass = "subsectionHeader";
		boolean isReadOnly = false;
		boolean isRequired = false;
		boolean isExpress = false;
		String searchButtonHtml = "";

		return createSubsectionHeader(subsectionLabel, divClass, isReadOnly, isRequired, isExpress, searchButtonHtml);
	}

	public String createSubsectionHeader(String subsectionLabel,
			boolean isReadOnly,
			boolean isRequired,
			boolean isExpress,
			String searchButtonHtml) {

		String divClass = "subsectionHeader";
		if (searchButtonHtml!=null && searchButtonHtml.length() > 0) {
			divClass = "subsectionShortHeader";

			if(searchButtonHtml.contains("searchButton")){
				divClass = "subsectionShortHeader";
				if((searchButtonHtml.contains("searchButton")) && (searchButtonHtml.contains("clearButton"))){
					divClass = "subsectionShortestHeader";	
				}
			}
		}

		return createSubsectionHeader(subsectionLabel, divClass, isReadOnly, isRequired, isExpress, searchButtonHtml);

	}

	public String createWideSubsectionHeader(String subsectionLabel) {

		String divClass = "subsectionWidestHeader";
		boolean isReadOnly = false;
		boolean isRequired = false;
		boolean isExpress = false;
		String searchButtonHtml = "";

		return createSubsectionHeader(subsectionLabel, divClass, isReadOnly, isRequired, isExpress, searchButtonHtml);

	}

	public String createWideSubsectionHeader(String subsectionLabel,
			boolean isReadOnly,
			boolean isRequired,
			boolean isExpress,
			String searchButtonHtml) {

		String divClass = "subsectionWidestHeader";
		if (searchButtonHtml!=null && searchButtonHtml.length() > 0) {
			divClass = "subsectionWiderHeader";
			if(searchButtonHtml.contains("searchButton")){
				divClass = "subsectionWiderHeader";
				if((searchButtonHtml.contains("searchButton")) && (searchButtonHtml.contains("clearButton"))){
					divClass = "subsectionWideHeader";	
				}
			}
		}
		return createSubsectionHeader(subsectionLabel, divClass, isReadOnly, isRequired, isExpress, searchButtonHtml);
	}


	private String createSubsectionHeader(String subsectionLabel,
			String	divClass,
			boolean isReadOnly,
			boolean isRequired,
			boolean isExpress,
			String searchButtonHtml) {

		StringBuilder html = new StringBuilder("");

		String localizedLabel = resMgr.getText(subsectionLabel,
				TradePortalConstants.TEXT_BUNDLE);
		if (localizedLabel.equals("")) {
			localizedLabel = subsectionLabel;
		}

		html.append("<div class=\"");
		html.append(divClass);

		if (isExpress) {
			html.append(" express");
		} else if (isRequired) {
			html.append(" required");
		}
		html.append("\">");

		if (isExpress) {
			html.append("<span class=\"hashMark\">#</span>");
		} else if (isRequired) {
			html.append("<span class=\"asterisk\">*</span>");
		}

		html.append("<h3>");
		//logXSSString("createSubsectionHeader","localizedLabel",localizedLabel,StringFunction.xssCharsToHtml(localizedLabel));
		//html.append(StringFunction.xssCharsToHtml(localizedLabel));
		html.append(localizedLabel);
		html.append("</h3>");

		if (!isReadOnly) {
			html.append(searchButtonHtml);
		}

		html.append("</div>");

		return html.toString();
	}

	/* -------------------------- STANDARD BUTTONS ------------------------- */
	

	//cquinton 9/19/2012 ir#4422
	//refactor createXXXButton methods to use generic createLink method
	public String createPartySearchButton(String identifierStr,
            String sectionName, boolean isReadOnly, String partyType,
            boolean displayUnicode) {
        return createSearchLink( null, //link name
        	sectionName,
            "javascript:'#'", //href
            "SearchParty('" + //onClick
              identifierStr + "','" +
              sectionName   + "','" +
              partyType     + "')" +
              ";return false;",
            isReadOnly);
    }

	public String createPartyClearButton(String buttonName,
			String functionName, boolean isReadOnly, String partyType) {
	    return createClearLink(buttonName,
	    		buttonName,
	        "javascript:void(0)", //href
		    functionName, //onClick
		    isReadOnly);
	}

	public String createBankSearchButton(String itemid,
            String section, boolean isReadOnly, String bankType) {
        return createSearchLink( "bankSearchButton",
        		section,
            "javascript:'#'", //href
            "BankSearch('" + itemid + "','" + //onClick
                section+"','" + bankType + "'); " +
            "return false;",
            isReadOnly);
   }

	//Rakesh Pasupulati Refresh adding instrument search button

	public String createInstrumentSearchButton(String identifierStr,
            String sectionName, boolean isReadOnly, String InstrumentType,
            boolean displayUnicode) {
		return createSearchLink(null,
				sectionName,
				 "javascript:'#'", //href
				 "SearchInstrument('"+//onClick
				 identifierStr+ "','" +
				 sectionName   + "','" +
				 InstrumentType     + "')" +
				 ";return false;",
		            isReadOnly);
	}

    //cquinton 9/19/2012 ir#4422 replace createRolloverButton() with a
    // set of generic create link methods.
	//note that for generic search/clear links, the class specified has "Button"
	// in the name -- this is the default style which displays as a button
    public String createClearLink(String href,
            String onClick, boolean isReadOnly) {
        return createClearLink(null, null, href, onClick, isReadOnly);
    }
    public String createClearLink(String name, String id, String href,
            String onClick, boolean isReadOnly) {
        return createLink(name, id, href, onClick, isReadOnly, "clearButton");
    }
    public String createSearchLink(String href,
            String onClick, boolean isReadOnly ) {
        return createSearchLink(null, null, href, onClick, isReadOnly);
    }
    public String createSearchLink(String name, String id, String href,
            String onClick, boolean isReadOnly ) {
        return createLink(name, id, href, onClick, isReadOnly, "searchButton");
    }
    public String createLink(String href, String onClick,
            boolean isReadOnly, String divClass ) {
        return createLink(null, null, href, onClick, isReadOnly, divClass);
    }
    public String createLink(String name, String id, String href, String onClick,
            boolean isReadOnly, String divClass ) {
        if (isReadOnly) {
            return "";
        }

        StringBuilder html = new StringBuilder("");
        html.append("<span");
        if ( divClass!=null && divClass.length()>0 ) {
            html.append(" class=\"");
            html.append(divClass);
            html.append("\"");
        }
        html.append(">");
        html.append("<a");
        if ( name!=null && name.length()>0 ) {
            html.append(" name=\"");
            html.append(name);
            html.append("\"");
        }
        if ( id!=null && id.length()>0 ) {
            html.append(" id=\"");
            html.append(id);
            html.append("\"");
        }
        html.append(" href=\"");
        html.append(href);
        html.append("\"");
        if ( onClick!=null && onClick.length()>0 ) {
            html.append(" onClick=\"");
            html.append(onClick);
            html.append("\"");
        }
        html.append(">");
        html.append("</a>");
        html.append("</span>");

        return html.toString();
    }


	public String createSidebarSubmitButton (String buttonName, String formName, String extraOnClickTags){
		boolean extraTagsSpecified = false;

		StringBuilder html = new StringBuilder();
		html.append(NL);
		html.append("<li>");
		html.append("<a href=\"javascript:document.forms['");
		html.append(formName);
		html.append("'].submit()\" ");

		html.append("name=\"");
		html.append(buttonName);
		html.append("Button\" ");

		html.append("id=\"");
		html.append(buttonName);
		html.append("Button\" ");

		if (extraOnClickTags == null || extraOnClickTags.equals("") || extraOnClickTags.equals("null")) {
		    extraTagsSpecified = false;
		}  else {
		    extraTagsSpecified = true;
		}

		html.append("onClick=\"");
		if (extraTagsSpecified) {
			html.append("setButtonPressed('");
		} else {
			html.append("return setButtonPressed('");
		}
		html.append(buttonName);
		html.append("', '");
		html.append(formName);
		html.append("'); ");
		if(extraTagsSpecified) {
			html.append(extraOnClickTags);
		}
		html.append("\">");
		html.append("<button data-dojo-type=\"dijit.form.Button\" ");
		html.append("data-dojo-props=\"iconClass:'");
		html.append(buttonName.toLowerCase());
		html.append("'\" type=\"submit\">");

		String localizedLabel = resMgr.getText("Sidebar." + buttonName, TradePortalConstants.TEXT_BUNDLE);
		html.append(localizedLabel);
		html.append("</button></a></li>");

		return html.toString();
	}


	public String createSidebarLinkButton (String buttonName, String link, String onClickTag){

		//@SuppressWarnings("deprecation")
		//String encodedLink = java.net.URLEncoder.encode(link);

		StringBuilder html = new StringBuilder();
		html.append(NL);
		html.append("<li>");
		html.append("<a href=\"");
		html.append(link);
		html.append("\" ");

		html.append("name=\"");
		html.append(buttonName);
		html.append("Button\" ");

		html.append("id=\"");
		html.append(buttonName);
		html.append("Button\" ");

		if (!(onClickTag == null || onClickTag.equals("") || onClickTag.equals("null"))) {
			html.append("onClick=\"");
			html.append(onClickTag);
			html.append("\"");
		}
		html.append("\">");

		html.append("<button data-dojo-type=\"dijit.form.Button\" ");
		html.append("data-dojo-props=\"iconClass:'");
		html.append(buttonName.toLowerCase());
		html.append("'\">");

		String localizedLabel = resMgr.getText("Sidebar." + buttonName, TradePortalConstants.TEXT_BUNDLE);
		html.append(localizedLabel);
		html.append("</button></a></li>");

		return html.toString();
	}

	/* -------------------------- HOVER HELP ------------------------------- */
	public String createHoverHelp(String fieldName, String fieldLabel) {

		String helpText = resMgr.getText(fieldLabel, "HoverHelp");

		if (helpText.equals("") || helpText.equals(fieldLabel)) {
			return "";
		}

		StringBuilder html = new StringBuilder("");

		html.append(NL);
		html.append("<span data-dojo-type=\"t360.widget.Tooltip\" data-dojo-props=\"saveConnId:'");
		html.append(fieldName);
		html.append("'\">");
		html.append(helpText);
		html.append("</span>");

		return html.toString();
	}
	/* -------------------------- HOVER HELP ------------------------------- 
	 * 12-19-2013  - Rel 8.4 CR-854 [BEGIN]- Added the to get page level language option*/
	public String createHoverHelp(String fieldName, String fieldLabel, String locale) {

		String helpText = resMgr.getText(fieldLabel, "HoverHelp", locale);

		if (helpText.equals("") || helpText.equals(fieldLabel)) {
			return "";
		}

		StringBuilder html = new StringBuilder("");

		html.append(NL);
		html.append("<span data-dojo-type=\"t360.widget.Tooltip\" data-dojo-props=\"saveConnId:'");
		html.append(fieldName);
		html.append("'\">");
		html.append(helpText);
		html.append("</span>");

		return html.toString();
	}


	/* -------------------------- COMMON FUNCTIONS ------------------------- */
	private String renderWidgetId(String tagType, String widgetType,
			String fieldName, String htmlProps, String widgetProps) {

		return renderWidgetId(tagType, widgetType, fieldName, fieldName,
				htmlProps, widgetProps);

	}

    public String renderWidgetId(String tagType, String widgetType,
            String fieldName, String fieldId, String htmlProps,
            String widgetProps) {

        StringBuilder html = new StringBuilder();

        //cquinton 12/11/2012 if widgetProps = "htmlOnly" we only render the html
        // so that the caller can dynamically create the widget themselves
        html.append(NL);
        html.append("<");
        html.append(tagType);
        if ( !"htmlOnly".equals(widgetProps) ) {
            html.append(" data-dojo-type=\"");
            html.append(widgetType);
            html.append("\"");
        }
		logXSSString("renderWidgetId","fieldName",fieldName,StringFunction.xssCharsToHtml(fieldName));
		logXSSString("renderWidgetId","fieldId",fieldId,StringFunction.xssCharsToHtml(fieldId));
	
        html.append(" name=\"");
        html.append(StringFunction.xssCharsToHtml(fieldName));
        html.append("\" id=\"");
        html.append(StringFunction.xssCharsToHtml(fieldId));
        html.append("\" ");
        html.append(htmlProps);
        html.append(" ");
        if (!widgetProps.equals("") && !"htmlOnly".equals(widgetProps)) {
            html.append(" data-dojo-props=\"");
            html.append(widgetProps);
            html.append("\" ");
        }

        return html.toString();
    }

	public String renderWidgetId(String tagType, String widgetType,
			String fieldName, String fieldId, String htmlProps,
			String widgetProps, String constraints) {

		StringBuilder html = new StringBuilder();
		logXSSString("renderWidgetId","fieldName",fieldName,StringFunction.xssCharsToHtml(fieldName));
		logXSSString("renderWidgetId","fieldId",fieldId,StringFunction.xssCharsToHtml(fieldId));
		logXSSString("renderWidgetId","tagType",tagType,StringFunction.xssCharsToHtml(tagType));
		logXSSString("renderWidgetId","widgetType",widgetType,StringFunction.xssCharsToHtml(widgetType));		
		html.append(NL);
		html.append("<");
		html.append(tagType);
		html.append(" data-dojo-type=\"");
		html.append(widgetType);
		html.append("\" name=\"");
		html.append(StringFunction.xssCharsToHtml(fieldName));
		html.append("\" id=\"");
		html.append(fieldId);
		html.append("\" ");
		html.append(htmlProps);
		html.append(" ");
		if (!constraints.equals("")) {
			html.append(" constraints=\"");
			html.append(constraints);
			html.append("\" ");
		}
		if (!widgetProps.equals("")) {
			html.append(" data-dojo-props=\"");
			html.append(widgetProps);
			html.append("\" ");
		}

		return html.toString();
	}

	public String renderDiv(String divClass, boolean isReadOnly,
			boolean isRequired, boolean isExpress) {

		StringBuilder html = new StringBuilder();
		html.append(NL);
		html.append("<div class=\"");
		html.append(divClass);

		if (isReadOnly || isExpress) {
			html.append(" readOnly");
		}

		if (isExpress) {
			html.append(" express");
		}

		if (isRequired && !isExpress) {
			html.append(" required");
		}

		html.append("\" >");
		html.append(NL);

		return html.toString();
	}

	/**
	 * Wraps a form item.  Adds the 'formItem' class to the
	 * div.  this should probably be renamed -- wrapFormItem()...
	 *
	 * @param html
	 * @param divClass
	 * @param isReadOnly
	 * @param isRequired
	 * @param isExpress
	 */
	public void wrapInDiv(StringBuffer html, String divClass,
			boolean isReadOnly, boolean isRequired, boolean isExpress) {

		if (divClass.equals("none")) {
			return;
		}

        StringBuilder classes = new StringBuilder();
        classes.append("formItem ");
        classes.append(divClass);

		if (isReadOnly || isExpress) {
			classes.append(" readOnly");
		}

		if (isExpress) {
			classes.append(" express");
		}

		if (isRequired) {
			classes.append(" required");
		}

		wrapInDiv(html, classes.toString());
	}

	/**
	 * Wraps a search item.  Adds the 'searchItem' class to the
	 * div.
	 *
	 * @param html
	 * @param divClass
	 */
    public void wrapSearchItem(StringBuffer html, String divClass) {

        if (divClass.equals("none")) {
            return;
        }

        String classes = "searchItem " + divClass;

        wrapInDiv(html, classes);
    }

    /** this just wraps the html in a div and does not assume any classes*/
    public void wrapInDiv(StringBuffer html, String divClass) {

        StringBuilder div = new StringBuilder();
        div.append(NL);
        div.append("<div class=\"");
        div.append(divClass);
        div.append("\" >");
        div.append(NL);

        html.insert(0, div);

        html.append(NL);
        html.append("</div>");
    }

    //cquinton 9/27/2012 refactor getting an attribute value from html properties
    /**
     * returns null if attribute name is not found
     */
    private String getHtmlAttributeValue(String htmlProps, String attributeName) {
        String attrVal = null;
        if (htmlProps!=null && htmlProps.length()>0 &&
                htmlProps.toLowerCase().contains(attributeName.toLowerCase()) ) {

            //simple parsing mechanism -
            //this deals with quoted attribute values
            // that have spaces
            List<String> attrList = new ArrayList<String>();
            String attr = "";
            boolean inString = false;
            for( int idx=0; idx<htmlProps.length(); idx++) {
                char c = htmlProps.charAt(idx);
                if ( !inString && ' ' == c ) {
                    //found the delimiter
                    // if found value
                    if ( attr.length()>0 ) {
                        attrList.add(attr);
                        attr = "";
                    }
                } else if ( !inString && '\"' == c) {
                    inString = true;
                } else if ( inString && '\"' == c) {
                    inString = false;
                } else {
                    attr += c;
                }
            }
            //add the last item
            if ( attr.length()>0 ) {
                attrList.add(attr);
                attr = "";
            }

            for (String aProp: attrList) {
                if (aProp.toLowerCase().startsWith(attributeName.toLowerCase())) {
                    attrVal=aProp.substring(aProp.indexOf("=")+1, aProp.length());
                    //cut off wrapping quotes if they exist
                    if (attrVal.startsWith("\"") && attrVal.endsWith("\"") &&
                          attrVal.length()>1 ) {
                        attrVal = attrVal.substring(1,attrVal.length()-1);
                    }
                }
            }
        }
        return attrVal;
    }
    
    /**
     * Given a String value, this method replaces all newline characters
     * with <br> tags.  The <br> tag will be used to display newlines
     * in html.
     *
     *
     * @param String value - the String to convert from
     * @return java.lang.String - the reformatted String
     */
    public static String formatStringWithBR( String value) {
        int startIndex = 0;
        int newlineIndex = -1;
        StringBuilder newString = new StringBuilder();

        if (value == null || value.equals("")) return value;

        newlineIndex = value.indexOf("\n", startIndex);

        while (newlineIndex > 0)
        {
    	newString.append(value.substring(startIndex, newlineIndex));
    	newString.append("<br>");
    	startIndex = ++newlineIndex;
    	newlineIndex = value.indexOf("\n", startIndex);
        }

        if (startIndex < value.length())
    	newString.append(value.substring(startIndex, value.length()));

        return newString.toString();
    }


    
    
	public String createSelectFieldInline(String fieldName, String fieldLabel,
			String defaultText, String options, boolean isReadOnly,
			boolean isRequired, boolean isExpress,
			String htmlProps, String widgetProps, String divClass) {

		StringBuffer html = new StringBuffer("");

		html.append(createFieldLabel(fieldName, fieldLabel));

		if (isReadOnly) {
			html.append(createReadOnlySelectField(fieldName, options));
			
			wrapInDiv(html, divClass, isReadOnly, isRequired, isExpress);
			return html.toString();
		}

        //cquinton 11/29/2012 use custom widget that includes special common functionality
		html.append(renderWidgetId("select", "t360.widget.FilteringSelect",
				fieldName, htmlProps, widgetProps));
			html.append(">");
		if (!defaultText.equals("")) {
			html.append("<option value=\"");
			html.append("");
			html.append("\">");
			logXSSString("createSelectFieldInline","defaultText",defaultText,StringFunction.xssCharsToHtml(defaultText));
			html.append(StringFunction.xssCharsToHtml(defaultText));
			html.append("</option>");
		}

		html.append(options);

		html.append("</select>");

		wrapInDiv(html, divClass, isReadOnly, isRequired, isExpress);

		// Add hover help
		html.append(createHoverHelp(fieldName, fieldLabel));

		return html.toString();
	}
	
	private  final String getTextResource(String key) {
		String val = resMgr.getText(key, TradePortalConstants.TEXT_BUNDLE);
		return val == null ? "" : val;
	}
	/**
	 * Generates boiler plate html code.
	 * It belongs in dojo.templates but currently tied to createHoverHelp which is here so it fits here better than the error prone/nested jsp include
	 * @param showLinks
	 * @param showTips
	 * @param showBackToTop
	 * @param _userSession - used for Show/Hide tips - but this will be used for various other settings/the constructor userSession may not be present
	 * @return html div for sidebarLinksSection
	 */
	public final String createSidebarQuickLinks(boolean showLinks, boolean showTips, boolean showBackToTop, SessionWebBean _userSession) {
		if (showLinks == false && showTips == false && showBackToTop == false) 
			return "";
		
		String text_SIdebarQuicklinkTitle = getTextResource("Sidebar.QuickLinks");
		String text_SidebarCollapseAll = getTextResource("Sidebar.CollapseAll");
		String text_SidebarExpandAll = getTextResource("Sidebar.ExpandAll");
		String text_SidebarShowTips = getTextResource("Sidebar.ShowTips");
		String text_SidebarHideTips = getTextResource("Sidebar.HideTips");
		String text_SidebarBackToTop = getTextResource("Sidebar.BackToTop");
		
		Boolean enableTips = Boolean.TRUE;//default will be shown
		Boolean savedQuickLinkValue = Boolean.TRUE;//default will be shown
		if (_userSession != null) {
			enableTips = _userSession.getShowToolTips();
			savedQuickLinkValue = _userSession.getSavedQuickLinkValue();
		}
		String hideTipsStyle = "";
		String showTipsStyle = "";
		if (enableTips.booleanValue() == true) 
			showTipsStyle = "style='display: none;'";
		else
			hideTipsStyle = "style='display: none;'";
		
		StringBuilder sb = new StringBuilder();
		//please keep the code formatted as below to be able to read the html
		sb.append("<div data-dojo-type='dijit.TitlePane' id='sidebarLinksSection' class='sidebarLinksSection' open='"+savedQuickLinkValue+"' title='"+text_SIdebarQuicklinkTitle+"'>");
		sb.append("<script type='dojo/connect' data-dojo-event='toggle' data-dojo-args='evt'>callAjaxToUpdateQuickLnk();return false;</script>");
		sb.append("  <ul class='navigationLinks' style='padding-left: 10px;'> ");
		if(showLinks){
			sb.append("	<li id='expandCollapseAllLi'>");
			sb.append("			 <div id='collapseAll'><a href='javascript:void(0);' >"+text_SidebarCollapseAll+"</a></div>");
			sb.append("			 <div id='expandAll' style='display: none;'><a href='javascript:void(0);' >"+text_SidebarExpandAll+"</a></div>");
			sb.append("	</li>");
			sb.append(this.createHoverHelp("collapseAll","ExpandCollapseHoverText"));
			sb.append(this.createHoverHelp("expandAll","ExpandCollapseHoverText"));
		}if(showTips){//not and else
			sb.append("		<li id='hideShowTipsLi'>");
			sb.append("			<div id='hideTipsDiv' "+hideTipsStyle+"><a href='javascript:hideTips();' >"+text_SidebarHideTips+"</a></div>");
			sb.append("			<div id='showTipsDiv' "+showTipsStyle+"><a href='javascript:showTips();' >"+text_SidebarShowTips+"</a></div>");
			sb.append("		</li> ");
		}if(showBackToTop){//not and else
			sb.append("		<li  id='BackToTop'><a href='#top'>"+text_SidebarBackToTop+"</a></li>");
			sb.append(this.createHoverHelp("BackToTop","BackTopHoverText"));
		} 
		sb.append("	</ul>");  
		sb.append(" <div class='sidebarDivider'></div>");
		sb.append("</div>");
		
		return sb.toString(); 
	}	
	
	/**
	 * Added for CR 821 Rel 8.3
	 * This method gets as an input Panel Aliases defined at Parent level. It then sorts the options and creates an options List
	 * @param refDataTableType
	 * @param optionsMap
	 * @param selectedOption
	 * @return
	 */
	public String createSortedOptions(String refDataTableType, Hashtable optionsMap, String selectedOption){
		StringBuilder html = new StringBuilder();
		String locale = userSession.getUserLocale();
		
		Hashtable refData = new Hashtable();
		try {
			refData = ReferenceDataManager.getRefDataMgr().getAllCodeAndDescr(refDataTableType, locale);
		} catch (AmsException e) {
			LOG.debug("WidgetFactory createSortedOptions[] - Error in fetching RefData Values"); 
		} 
		Iterator it = optionsMap.keySet().iterator(); 
		String[] values = new String[optionsMap.size()];
		int i=0;
		while (it.hasNext()) {
		  String idx = (String)it.next();
	  	  if(optionsMap.get(idx) == null || optionsMap.get(idx).equals("")){ 
	  		optionsMap.put(idx, refData.get(idx));
	  	  }
	  	  values[i] = idx+"\n\n\n"+optionsMap.get(idx);
	  	  i++;
		}
		//Sort the values
		try 
        {
           Sort.sort(values, locale);
        }
		catch(Exception e)
        {
           LOG.debug("WidgetFactory createSortedOptions[] - Error in Sorting the values."); 
        }
		
        // Loop through the Strings after being sorted
        // and build the HTML
        for(int j=0; j<values.length; j++)
         {
             // Parse out the value and description
             int indexOfDelimiter = values[j].indexOf("\n\n\n");
             String value = values[j].substring(0,indexOfDelimiter);
             String descr = values[j].substring(indexOfDelimiter+3);
     		 logXSSString("createSortedOptions","value",value,StringFunction.xssCharsToHtml(value));
    		 logXSSString("createSortedOptions","descr",descr,StringFunction.xssCharsToHtml(descr));
             // Get the HTML
             if (StringFunction.isBlank(selectedOption)) {
					if(StringFunction.isNotBlank(descr))
					html.append("<option value=\"" + value + "\">" + descr+ "</option>\n");
				} else {
					if (value.equals(selectedOption)) {
						if(StringFunction.isNotBlank(descr))
						html.append("<option selected value=\"" + value + "\">"+ descr + "</option>\n");
					} else {
						if(StringFunction.isNotBlank(descr))
						html.append("<option value=\"" + value + "\">" + descr+ "</option>\n");
					}
				}                    
         }
		return html.toString();
	}
	
	//This method is for logging xss related testing
	static void logXSSString(String methodName, String fieldName, String before, String after){
		if(StringFunction.isNotBlank(before) && StringFunction.isNotBlank(after)){
			if(!before.equals(after)){
				LOG.debug("XSS:WidgetFactory: Method: "+methodName+" FieldName: "+fieldName+" Before: ["+before+" ] After: [ "+after+" ]");
			}
		}
	}
	
}
