package com.ams.tradeportal.html;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.util.StringFunction;

/**
 * InvoiceFileUploadDetailErrorFormatter writes out a carriage return prior to each "-" so that the error messages
  * are formatted to the page
 */
public class InvoiceFileUploadDetailErrorFormatter extends AbstractCustomListViewColumnFormatter {
private static final Logger LOG = LoggerFactory.getLogger(InvoiceFileUploadDetailErrorFormatter.class);

	/**
	 * Format data.
	 * @param obj - error message
	 * @return all '-' to '<br>-'
	 */
    public String format(Object obj) {
        String formattedData = (String) obj;

		if (StringFunction.isNotBlank(formattedData))
		{
			if(!formattedData.contains("End-to-End") && !formattedData.contains("Re-upload")){
				formattedData = formattedData.replace("-","<br>-");
			}
	        formattedData = formattedData.replaceFirst("<br>","");
	        formattedData = formattedData.replace("&#39;","'");
		}

        return formattedData;
    }
}
