package com.ams.tradeportal.html;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.common.TransactionStatus;

/**
 * PaymentFileUploadTransactionLinkFormatter returns a flag to determine whether to display a link for a particular value
 */
public class PaymentFileUploadTransactionLinkFormatter extends AbstractCustomListViewColumnFormatter {
private static final Logger LOG = LoggerFactory.getLogger(PaymentFileUploadTransactionLinkFormatter.class);

	/**
	 * Format data - not used.
	 */
    public String format(Object obj) {
        String value = (String) obj;
        return value;
    }

	/**
	 * If payment upload validation status is file rejected or started, display transaction id as text and not link
	 */
    public boolean determineOverrideLink(String s) {

		if (s == null)
			return false;

        if (s.equals(TransactionStatus.FILE_UPLOAD_REJECT) || s.equals(TransactionStatus.STARTED))
        	return true;

        return false;
    }
}
