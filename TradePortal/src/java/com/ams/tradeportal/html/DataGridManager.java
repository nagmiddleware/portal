package com.ams.tradeportal.html;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.XmlConfigFileCache;
import com.amsinc.ecsg.util.DocumentHandler;

/**
 * DataGridManager
 *
 * As requests are made to load the xml file, the parameters are read in, parsed and placed in a metadata structure. This is then
 * stored in another hashtable, keyed by the xml file name. Efficiency is improved by caching the data rather than reading it again.
 * <p>
 *
 * This is sample xml to define a listview.
 *
 * <pre>
 * 
 *   <dataGrid>
 *     <rowKey>
 *        <showCheckBox>Y</showCheckBox>
 *        <showRadioButton>Y</showRadioButton>
 *     </rowKey>
 *     <resourcePrefix>CorpCustSearch</resourcePrefix>
 *     <column>
 *       <displayNameKey>bankName</displayNameKey>
 *       <justify>left</justify>
 *       <width>123</width>
 *       <linkInfo>
 *          <linkStyleInfo>
 *             <linkStyleColumn>unread_flag</linkStyleColumn>
 *             <linkStyle>
 *                <linkStyleValue>Y</linkStyleValue>
 *                <linkStyleClass>ListHeaderText</linkStyleClass>
 *             </linkStyle>
 *             <linkStyle>
 *                <linkStyleValue>N</linkStyleValue>
 *                <linkStyleClass>ListText</linkStyleClass>
 *             </linkStyle>
 *          </linkStyleInfo>
 *       </linkInfo>
 *     </column>
 *     <column>
 *       <displayNameKey>bankAssets</displayNameKey>
 *       <justify>right</justify>
 *       <width>90</width>
 *     </column>
 *     <column>
 *       <displayNameKey>bankFoundDate</displayNameKey>
 *       <justify>center</justify>
 *       <width>100</width>
 *     </column>
 *   </dataGrid>
 *
 * The following table describes each tag.
 *
 *     Tag                    Description
 * ---------------- -----------------------------------------------------------
 *
 * rowKey           put something
 *
 * showCheckBox     A Y or N value that indicates whether a checkbox should
 *                  display as the first column in the list view.  When
 *                  submitted the checkboxes have a value specified by one or
 *                  more checkBoxValue tags, which specify result set tag names
 *                  associated with the row. This defaults to N if no value is
 *                  given.
 *
 * showRadioButton  A Y or N value that indicates whether a radio button should
 *                  display as the first column in the list view.  When
 *                  submitted the radio buttons have a value specified by one or
 *                  more radioButtonValue tags, which specify result set tag names
 *                  associated with the row. This defaults to N if no value is
 *                  given.
 *
 * radioButtonValue The name of a result set xml tag from the listview SQL query
 *                  to use as the radio button value, usually an oid field but not
 *                  always. More than one radio button value tag can be specified,
 *                  with each separated by a '/' character. These tags only
 *                  apply when the showRadioButton value is set to Y.
 *
 * showFooter       A Y or N value that indicates whether or not a footer should be
 *                  displayed in the list view. This defaults to Y if no value is
 *                  given.
 *
 * column           Container tag that holds other tags that define a column.
 *                  At least one column must be specified.  Sometimes, it is
 *                  desired that different column defintions be used for admin users
 *                  versus non-admin users.  By placing the contents of the column
 *                  tag into <admin> and <nonAdmin> tags, the list
 *                  view will determine which to use.
 *
 * displayNameKey   This is a logical column name (see selectClause above).
 *                  It is a key into the text resource bundle and matches a
 *                  logical name in the selectClause.  This value is required.
 *
 * justify          This case insensitive value indicates the justification of
 *                  the data for the column.  Allowed values are 'left',
 *                  'right', and 'center'.  The default value is 'left'.
 *
 * suppressNo       Only valid for columns which are 'boolean's.  It indicates
 *                  whether or not 'No' values should be suppressed.  Allowed
 *                  values are 'Y' and 'N'.  The default is 'N'.
 *
 * width            This value indicates the width of the column in pixels.
 *                  It defaults to 10 if not given.
 *
 * hidden           Indicates if the column should be hidden. Valid values 'Y' or 'N'.
 *                  Defaults to not hidden.
 *                  
 * securityType     Indicates for which type of user the data should display for.
 *                  Valid values are admin, nonAdmin or both.  default is both.
 *
 * visibilityCondition     Specify classname that determindes the visibility of the column.  Should include the full
 *                  package name.  visibilityCondition class should implement the
 *                  AbstractVisibilityCondition interface.
 *
 * customFormatter  Specify a custom formatter classname.  Should include the full
 *                  package name.  Custom formatters should implement the
 *                  CustomListViewColumnFormatter interface.
 *
 * customFormatterValue  Specify a column value to be used by the custom formatter classname.
 *                  This value will be used by the custom formatter class to determine whether the
 *                  value will be displayed as a hyperlink.
 * </pre>
 *
 * Copyright � 2001 American Management Systems, Incorporated All rights reserved
 */

public class DataGridManager {
	private static final Logger LOG = LoggerFactory.getLogger(DataGridManager.class);

	// hash all of the data grid metadata
	private static Map<String, DataGridMetadata> allDataGridMetadata = new HashMap<>();

	/**
	 * A representation of the data grid metadata.
	 * 
	 */
	public static class DataGridMetadata {
		public boolean hasErrors = false;
		public String errors = "";
		public boolean showCheckBox = false;
		public boolean showSelectAllCheckBox = false;
		public boolean showRadioButton = false;
		public boolean showFooter = false;
		public boolean showSelectedCount = false;
		public boolean showTotalCount = false;
		public String resourcePrefix = "";
		public DataGridManager.DataGridColumn columns[] = null;
		public DataGridManager.FooterItem footerItems[] = null;
	}

	/**
	 * A 'column' consists of several pieces of data that help describe it. These are grouped together into an inner class for ease
	 * of use.
	 */
	public static class DataGridColumn {
		/**
		 * columnKey is the resource bundle key used to lookup the value to display for the column header
		 */
		public String columnKey;

		/**
		 * justifyType indicates how the data should be justified in the column. See the various constants defined in the ListViewManager
		 */
		public String justifyType;

		/**
		 * width indicates a width for the column. It is generally used when creating the HTML for the column and may represent
		 * pixel width or a percentage
		 */
		public int width = 25; // default

		/**
		 * suppressNo indicates that boolean values of false should be suppressed from display.
		 */
		public String suppressNo;

		/**
		 * currencyColumn is used with currency data types. It represents the column that holds the currency code associated with
		 * the currency value.
		 */
		public String currencyColumn;

		/**
		 * prefixCurrencyCode is a flag that determines if the currency code should be pre-pended to a currency column.
		 */
		public boolean prefixCurrencyCode;

		/**
		 * Indicates whether column is hidden.
		 */
		public boolean isHidden = false;

		/**
		 * Indicates for which type of user the data should be display.
		 */
		public String securityType = SECURITY_TYPE__BOTH;

		/**
		 * a custom formatter if data type is "custom"
		 */
		public String customFormatter;

		// Additional Column to use in logic for determining hyperlink override
		public String customFormatterValue;

		/**
		 * a visibility condition for column
		 */
		public String visibilityCondition;

		public boolean isLink = false; // default

		// CustomFormatter by Prateep
		public boolean isCustomFormatter = false;
		public String customFormatterName;
		public List<String> fields;

		// linkUrlColumn is the name of the column that should be referenced
		// to get the link info if this column is to be formatted
		// as a link
		public String linkUrlColumn;

		public String formatter;

		public String toString() {
			return ("[ColumnKey=" + columnKey
				   + " justify=" + justifyType
				   + " width=" + width
				   + " suppressNo=" + suppressNo
				   + " currencyColumn=" + currencyColumn
				   + " prefixCurrencyCode=" + prefixCurrencyCode
				   + " isHidden=" + isHidden
				   + " customFormatter=" + customFormatter
				   + " customFormatterValue=" + customFormatterValue
				   + " visibilityCondition=" + visibilityCondition
				   + "]");
		}
	}

	public static class FooterItem {
		public static final String LINK = "link";
		public static final String BUTTON = "button";
		public static final String DROPDOWNBUTTON = "dropdownbutton";
		public static final String BUSYBUTTON = "busybutton";

		/**
		 * the resource bundle key used to lookup the value to display for button or link
		 */
		public String displayNameKey;

		// link or button
		public String type = LINK;

		public String onClick = "";
		public String linkAction = "";
		public DataGridManager.LinkParameter linkParameters[] = null;

		public String conditionalDisplay;

		// should the button be disabled initially
		// if so, expectation is that code will enable when necessary
		public boolean initDisabled = false;

		public DataGridManager.DropDownItem dropDownItems[] = null; // Narayan CR-913 Rel9.0 09-Feb-2014
	}

	public static class LinkParameter {
		public String name;
		public String value;
	}

	public static class DropDownItem {

		public static final String MENUITEM = "menuitem";
		public String type = MENUITEM;
		public String displayNameKey;
		public String onClick = "";
		public String linkAction = "";
		public String conditionalDisplay;
	}

	/**
	 * Constant used to justify column data on the left. Associated with the 'justify' listview xml tag
	 */
	public final static String LEFT = "left";
	/**
	 * Constant used to justify column data on the right. Associated with the 'justify' listview xml tag
	 */
	public final static String RIGHT = "right";
	/**
	 * Constant used to justify column data in the center. Associated with the 'justify' listview xml tag
	 */
	public final static String CENTER = "center";

	/** Constants that define security types */
	public final static String SECURITY_TYPE__ADMIN = "admin";
	public final static String SECURITY_TYPE__NON_ADMIN = "nonAdmin";
	public final static String SECURITY_TYPE__BOTH = "both";

	// All data type constants are defined below

	/**
	 * Indicates the type of the column is a string suppression Associated with the 'dataType' listview xml tag. Works like a string
	 * data type except when the value is the same as that provided in the '<u>request</u> parameter 'suppressValue'. When there is
	 * an exact match, the value does not display in the listview.
	 */
	public final static java.lang.String SUPPRESS_FOR_VALUE = "suppress_for_value";

	/**
	 * Indicates custom column. If specified, the customFormatter tag should also be specified to indicate the class used to format
	 * the data.
	 */
	public final static java.lang.String CUSTOM = "custom";

	/**
	 * Default constructor
	 *
	 */
	private DataGridManager() {
	}

	/**
	 * This method returns the metadata for a given dataGrid xml file.
	 *
	 * @param listView
	 *            java.lang.String - The name of the dataGrid parameter file to return
	 * @return DataGridMetadata - The structure containing the metadata
	 */
	public static DataGridMetadata getDataGridMetadata(String dataGridName) {

		DataGridMetadata metadata = allDataGridMetadata.get(dataGridName);

		if (metadata == null) {
			loadDataGridMetadata(dataGridName);
			metadata = allDataGridMetadata.get(dataGridName);
		}

		return metadata;
	}

	/**
	 * This method reads in the XML configuration for a listView and populates the internal hashtable storage of the list view
	 * information. Each listview's hashtable is then stored in the 'global' hashtable of listviews for this class. This method is
	 * only called to initially load or refresh a listview's data. The listview xml file is described in the class description. br>
	 * Sometimes the columns need to be defined differently based on whether the user is an admin user or not. In this case, an
	 * additional tag may be inserted between the 'column' tag and its attributes. Thus, rather than using /column/displayNameKey,
	 * you can use /column/admin/displayNameKey and /column/nonAdmin/displayNameKey. The boolean is only used if both the admin and
	 * nonAdmin subdocs are included.
	 * <p>
	 * 
	 * @param myListView
	 *            String - The name of the listview parameter file to load.
	 */
	private static synchronized void loadDataGridMetadata(String dataGridName) {

		DataGridMetadata metadata = new DataGridMetadata();

		// Default the HAS_ERRORS value to N.
		metadata.hasErrors = false;

		try {
			// Create a document handler based on the passed in file.
			DocumentHandler doc = XmlConfigFileCache.readConfigFile("/datagrid/".concat(dataGridName).concat(".xml"));
			// todo: should return an error if can find the file!!!!

			// Now get each attribute from the documents and put it in the hashtable. Perform edits as necessary.

			// 'resourcePrefix' is optional.
			String value = doc.getAttribute("/resourcePrefix");
			if (value == null)
				value = "";
			metadata.resourcePrefix = value;

			// 'showCheckBox' defaults to N if not provided. Otherwise the value is taken as is. The user of this value
			// decides how to interpret/default the value.
			// Y/N is expected. If Y is provided, retrieve the column names to use for the check box's value (each separated by a '/').
			DocumentHandler rowKeyDoc = doc.getFragment("/rowKey");
			if (rowKeyDoc == null) {
				metadata.showCheckBox = false;
				metadata.showSelectAllCheckBox = false;
				metadata.showRadioButton = false;
			} else {
				value = rowKeyDoc.getAttribute("/showCheckBox");
				if (TradePortalConstants.INDICATOR_YES.equalsIgnoreCase(value)) {
					metadata.showCheckBox = true;
				} else {
					metadata.showCheckBox = false;
				}

				value = rowKeyDoc.getAttribute("/showSelectAllCheckBox");
				if (TradePortalConstants.INDICATOR_YES.equalsIgnoreCase(value)) {
					metadata.showSelectAllCheckBox = true;
				} else {
					metadata.showSelectAllCheckBox = false;
				}

				value = rowKeyDoc.getAttribute("/showRadioButton");
				if (TradePortalConstants.INDICATOR_YES.equalsIgnoreCase(value)) {
					metadata.showRadioButton = true;
				} else {
					metadata.showRadioButton = false;
				}
			}

			// 'showFooter' defaults to Y if not provided. Otherwise the value
			// is taken as is. This value indicates whether or not the listview
			// footer should be displayed. Y/N is expected.
			DocumentHandler footerDoc = doc.getFragment("/footer");
			if (footerDoc == null) {
				metadata.showFooter = false;
			} else {
				value = footerDoc.getAttribute("/showFooter");
				if (TradePortalConstants.INDICATOR_YES.equalsIgnoreCase(value)) {
					metadata.showFooter = true;
				} else {
					metadata.showFooter = false;
				}

				List<DocumentHandler> footerItems = doc.getFragmentsList("footerItem");
				int itemCount = footerItems.size();
				if (itemCount > 0) {
					metadata.footerItems = new FooterItem[itemCount];

					// Loop through each of the footer items
					for (int i = 0; i < itemCount; i++) {
						FooterItem aItem = buildFooterItemFromDoc(footerItems.get(i), metadata);
						metadata.footerItems[i] = aItem;
					} 

				}

				value = footerDoc.getAttribute("/showSelectedCount");
				if (TradePortalConstants.INDICATOR_YES.equalsIgnoreCase(value)) {
					metadata.showSelectedCount = true;
				} else {
					metadata.showSelectedCount = false;
				}

				value = footerDoc.getAttribute("/showTotalCount");
				if (TradePortalConstants.INDICATOR_YES.equalsIgnoreCase(value)) {
					metadata.showTotalCount = true;
				} else {
					metadata.showTotalCount = false;
				}
			}

			// At least one 'column' is required.
			List<DocumentHandler> columns = doc.getFragmentsList("/column");
			int columnCount = columns.size();
			if (columnCount < 1) {
				postError("No columns were specified!", metadata);
			}

			/* Customformatter by Prateep */

			Hashtable<String, String> linkStyleColumns = new Hashtable<>();
			Hashtable<String, List<String>> linkStyleClasses = new Hashtable<>();
			Hashtable<String, List<String>> linkStyleValues = new Hashtable<>();

			metadata.columns = new DataGridManager.DataGridColumn[columnCount];

			// Loop through each of the column definitions.
			for (int i = 0; i < columnCount; i++) {
				DocumentHandler columnDoc = columns.get(i);

				DataGridColumn aColumn = buildColumnFromDoc(columnDoc, metadata);

				// If the column is a link column, retrieve the link action and
				// link parameters.
				DocumentHandler linkInfoDoc = columnDoc.getFragment("/linkInfo");
				if (linkInfoDoc != null) {
					aColumn.isLink = true;

					// If a link style info section is specified in the listview xml file, there can be
					// more than one CSS style class used for links in a column. Retrieve all relevant
					// info from this section so that these classes are used appropriately.
					DocumentHandler linkStyleInfoDoc = linkInfoDoc.getFragment("/linkStyleInfo");
					if (linkStyleInfoDoc != null) {
						String linkStyleColumn = linkStyleInfoDoc.getAttribute("/linkStyleColumn");
						List<DocumentHandler> linkStyles = linkStyleInfoDoc.getFragmentsList("/linkStyle");

						List<String> linkStyleClassList = new ArrayList<>();
						List<String> linkStyleValueList = new ArrayList<>();

						for (DocumentHandler linkStyle : linkStyles) {
							DocumentHandler linkStyleDoc = linkStyle;

							linkStyleClassList.add(linkStyleDoc.getAttribute("/linkStyleClass"));
							linkStyleValueList.add(linkStyleDoc.getAttribute("/linkStyleValue"));
						}

						linkStyleColumns.put(aColumn.columnKey, linkStyleColumn);
						linkStyleClasses.put(aColumn.columnKey, linkStyleClassList);
						linkStyleValues.put(aColumn.columnKey, linkStyleValueList);
					}

					aColumn.linkUrlColumn = linkInfoDoc.getAttribute("/linkUrlColumn");
				}

				/* CustomFormatter by Prateep */
				DocumentHandler customFormatterInfo = columnDoc.getFragment("/customFormatterInfo");
				if (customFormatterInfo != null) {
					aColumn.isCustomFormatter = true;
					aColumn.customFormatterName = customFormatterInfo.getAttribute("/customFormatterName");
					List<DocumentHandler> fields = customFormatterInfo.getComponent("/fields").getFragmentsList("/field");
					aColumn.fields = new ArrayList<String>();// IR 18169
					for (DocumentHandler field1 : fields) {
						DocumentHandler field = field1;
						if (field != null) {
							// IR 18169 -this should be outside for-loop else every time it creates new list
							// aColumn.fields=new ArrayList();
							aColumn.fields.add(field.getAttribute("/"));
						}
					}

				}
				metadata.columns[i] = aColumn;

			} // end for each column loop

			// After loading all the parameters, put the metadata in
			// the 'allDataGridMetadata' hashmap, keyed by the
			// name of the datagrid.
			allDataGridMetadata.put(dataGridName, metadata);

		} catch (Exception e) {
			LOG.error("Error occured while loading Data Grid Metadata ", e);

		}
	}

	/**
	 * Appends the given error to the current list of error messages (which is stored in the hashtable under the keyword ERRORS.
	 * Messages are separated by a carriage return/line feed. Also sets the HAS_ERRORS hash value to Y.
	 *
	 * @param message
	 *            java.lang.String - The error message to post
	 * @param dataGridMetadata
	 *            Hashtable - The hashtable to post the message to
	 */
	private static void postError(String message, DataGridMetadata metadata) {
		metadata.hasErrors = true;

		if (metadata.errors == null)
			metadata.errors = "";

		metadata.errors += "\n\r" + message;
	}

	/**
	 * This method clears all the data from the global hashtable. This method can be called to refresh the listview parameters
	 * without restarting the web server.
	 *
	 */
	public synchronized void refresh() {

		try {
			allDataGridMetadata.clear();

		} catch (Exception e) {
			LOG.error("Error occured while refreshing Data Grid Metadata ", e);
		}
	}

	/**
	 * This method reads in the XML configuration for a listView and populates the internal hashtable storage of the list view
	 * information. Each listview's hashtable is then stored in the 'global' hashtable of listviews for this class. This method is
	 * only called to initially load or refresh a listview's data. The listview xml file is described in the class description. <br>
	 * Sometimes the columns need to be defined differently based on whether the user is an admin user or not. In this case, an
	 * additional tag may be inserted between the 'column' tag and its attributes. Thus, rather than using /column/displayNameKey,
	 * you can use /column/admin/displayNameKey and /column/nonAdmin/displayNameKey. The boolean is only used if both the admin and
	 * nonAdmin subdocs are included.
	 * <p>
	 * 
	 * @param myListView
	 *            String - The name of the listview parameter file to load. This file should reside in configFileDir provided in the
	 *            AmsServletConfig.properties file.
	 * @param isAdminUser
	 *            boolean - indicates if the user is an admin user
	 */
	private static synchronized DataGridColumn buildColumnFromDoc(DocumentHandler columnDoc,
			DataGridManager.DataGridMetadata dataGridMetadata) {

		DataGridColumn aColumn = new DataGridColumn();

		try {

			// 'displayNameKey' is required for each column.
			String value = columnDoc.getAttribute("/displayNameKey");

			if (value == null || value.equals("")) {
				postError("/column/displayNameKey is missing or blank", dataGridMetadata);
				value = "";
			}
			aColumn.columnKey = value;

			// Valid justify values are 'left', 'right', or 'center'. If missing, default to 'left'. If not valid, post
			// an error and default to 'left'.
			value = columnDoc.getAttribute("/justify");
			if (value == null || value.equals("")) {
				value = LEFT;
			} else {
				value = value.toLowerCase();
				if (!(value.equals(LEFT) || value.equals(CENTER) || value.equals(RIGHT))) {
					postError("/column/justify value of " + value + " is invalid; changing to 'left'", dataGridMetadata);
					value = LEFT;
				}
			}
			aColumn.justifyType = value;

			// If the suppressNo attribute is given, it must be either 'Y' or 'N'. (This test only applies if the datatype is boolean.).
			// If the test fails, it defaults to 'N'.
			value = columnDoc.getAttribute("/suppressNo");

			if (value == null || value.equals("")) {
				value = TradePortalConstants.INDICATOR_NO;
			} else if (!value.equals(TradePortalConstants.INDICATOR_YES) && !value.equals(TradePortalConstants.INDICATOR_NO)) {
				postError("/column/suppressNo is invalid - defaulting to N", dataGridMetadata);
				value = TradePortalConstants.INDICATOR_NO;
			}

			aColumn.suppressNo = value;

			// If the prefixCurrencyCode attribute is given, it must coorespond to a column that contains valid currency codes. This column
			// only applies if the data type is 'currency'
			value = columnDoc.getAttribute("/prefixCurrencyCode");
			if (value == null || value.equals("")) {
				value = "";
			}

			if (value.equals("Y")) {
				aColumn.prefixCurrencyCode = true;
			} else {
				aColumn.prefixCurrencyCode = false;
			}

			// 'width' is required for each column.
			value = columnDoc.getAttribute("/width");
			int intValue = 0;
			if (value == null || value.equals("")) {
				postError("/column/width is missing or blank", dataGridMetadata);
			} else {
				try {
					intValue = Integer.parseInt(value);
				} catch (NumberFormatException e) {
					postError("/column/width value of " + value + " is not a valid number; defaulting to 10", dataGridMetadata);
					intValue = 100;
				}
			}
			aColumn.width = intValue;

			// hidden column
			value = columnDoc.getAttribute("/hidden");
			if (TradePortalConstants.INDICATOR_YES.equalsIgnoreCase(value)) {
				aColumn.isHidden = true;
			} else {
				aColumn.isHidden = false;
			}

			// securityType column
			value = columnDoc.getAttribute("/securityType");
			if (SECURITY_TYPE__ADMIN.equalsIgnoreCase(value)) {
				aColumn.securityType = SECURITY_TYPE__ADMIN;
			} else if (SECURITY_TYPE__NON_ADMIN.equalsIgnoreCase(value)) {
				aColumn.securityType = SECURITY_TYPE__NON_ADMIN;
			} else {
				aColumn.securityType = SECURITY_TYPE__BOTH;
			}

			// custom formatter
			value = columnDoc.getAttribute("/customFormatter");
			aColumn.customFormatter = value;

			value = columnDoc.getAttribute("/customFormatterValue");
			if (value == null || value.equals(""))
				value = "";
			aColumn.customFormatterValue = value;

			// visibility condition
			value = columnDoc.getAttribute("/visibilityCondition");
			aColumn.visibilityCondition = value;

			value = columnDoc.getAttribute("/formatter");
			aColumn.formatter = value;

		} catch (Exception e) {
			LOG.error("Error occured in buildColumnFromDoc()  ", e);

		}

		return aColumn;
	}

	private static synchronized FooterItem buildFooterItemFromDoc(DocumentHandler itemDoc,
			DataGridManager.DataGridMetadata dataGridMetadata) {

		FooterItem aItem = new FooterItem();

		try {
			// 'displayNameKey' is required for each column.
			String value = itemDoc.getAttribute("/displayNameKey");

			if (value == null || value.equals("")) {
				postError("/footerItem/displayNameKey is missing or blank", dataGridMetadata);
				value = "";
			}
			aItem.displayNameKey = value;

			// Valid justify values are 'left', 'right', or 'center'. If missing, default to 'left'. If not valid, post
			// an error and default to 'left'.
			value = itemDoc.getAttribute("/type");
			if (value == null || value.equals("")) {
				value = FooterItem.LINK;
			} else {
				value = value.toLowerCase();
				if (!(value.equals(FooterItem.BUTTON) || value.equals(FooterItem.LINK) || FooterItem.DROPDOWNBUTTON.equals(value)
						|| FooterItem.BUSYBUTTON.equals(value))) {
					postError("/footerItem/type value of " + value + " is invalid; changing to 'link'", dataGridMetadata);
					value = FooterItem.LINK;
				}
			}
			aItem.type = value;

			value = itemDoc.getAttribute("/onClick");
			if (value == null || value.equals(""))
				value = "";
			aItem.onClick = value;

			value = itemDoc.getAttribute("/linkAction");
			if (value == null || value.equals(""))
				value = "";
			aItem.linkAction = value;

			List<DocumentHandler> parms = itemDoc.getFragmentsList("/linkParameter");
			int parmCount = parms.size();
			if (parmCount > 0) {
				aItem.linkParameters = new LinkParameter[parmCount];

				// Loop through each of the footer items
				for (int i = 0; i < parmCount; i++) {
					DocumentHandler parmDoc = parms.get(i);

					LinkParameter aParm = new LinkParameter();

					value = parmDoc.getAttribute("/name");
					if (value == null || value.equals(""))
						value = "";
					aParm.name = value;

					value = parmDoc.getAttribute("/value");
					if (value == null || value.equals(""))
						value = "";
					aParm.value = value;

					aItem.linkParameters[i] = aParm;

				} 

			}

			value = itemDoc.getAttribute("/conditionalDisplay");
			if (value == null || value.equals(""))
				value = "";
			aItem.conditionalDisplay = value;

			value = itemDoc.getAttribute("/initDisabled");
			if (TradePortalConstants.INDICATOR_YES.equals(value)) {
				aItem.initDisabled = true;
			}

			if (FooterItem.DROPDOWNBUTTON.equals(aItem.type)) {
				buildFooterDropDownButtonFromDoc(itemDoc, aItem);
			}

		} catch (Exception e) {
			LOG.error("Error occured in buildFooterItemFromDoc()  ", e);

		}

		return aItem;
	}

	/**
	 * This method is used to build DropDownButton in Dgrid
	 * 
	 * @param itemDoc
	 * @param footerItem
	 */

	private static synchronized void buildFooterDropDownButtonFromDoc(DocumentHandler itemDoc,
			DataGridManager.FooterItem footerItem) {

		List<DocumentHandler> dropDownMenuItems = itemDoc.getFragmentsList("/dropDownMenu/menuItem");
		int dropDownItemsCount = dropDownMenuItems.size();
		if (dropDownItemsCount > 0) {
			footerItem.dropDownItems = new DropDownItem[dropDownItemsCount];

			// Loop through each of the Menu items
			for (int i = 0; i < dropDownItemsCount; i++) {
				DocumentHandler menuItemDoc = dropDownMenuItems.get(i);
				DropDownItem dropdownItem = buildFooterMenuItemFromDoc(menuItemDoc, footerItem);
				footerItem.dropDownItems[i] = dropdownItem;
			} 
		}
	}

	/**
	 * This method is used to build DropDownButton's menu Item.
	 * 
	 * @param itemDoc
	 * @param footerItem
	 * 
	 */

	private static synchronized DataGridManager.DropDownItem buildFooterMenuItemFromDoc(DocumentHandler itemDoc,
			DataGridManager.FooterItem footerItem) {

		DataGridManager.DropDownItem dropDownItem = new DropDownItem();
		dropDownItem.displayNameKey = itemDoc.getAttribute("/displayNameKey");
		dropDownItem.type = itemDoc.getAttribute("/type");
		dropDownItem.onClick = itemDoc.getAttribute("/onClick");
		dropDownItem.conditionalDisplay = itemDoc.getAttribute("/conditionalDisplay");
		return dropDownItem;
	}

}
