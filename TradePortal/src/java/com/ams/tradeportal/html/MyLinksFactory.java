package com.ams.tradeportal.html;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Vector;


import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import com.ams.tradeportal.common.TradePortalConstants;
import com.ams.tradeportal.busobj.util.*;
import com.amsinc.ecsg.web.*;

import java.lang.reflect.Field;
import java.math.*;
import javax.servlet.http.*;
import com.ams.tradeportal.busobj.webbean.SessionWebBean;


public class MyLinksFactory {
private static final Logger LOG = LoggerFactory.getLogger(MyLinksFactory.class);
	private static final String XPATH_MENU_BAR_VAL = "/navigationMenuBar";
    private static final String XPATH_MENU_VAL = "/menuBarItem";
    private static final String XPATH_ITEM_VAL = "/menuItem";
    private static final String XPATH_CAT_VAL = "/category";
    private static final String XPATH_DISPLAY_KEY_VAL = "/displayKey";
    private static final String XPATH_COND_DISPLAY_VAL = "/conditionalDisplay";
    private static final String XPATH_LINK_ACTION_NAME = "/linkAction";
    private static final String XPATH_LINK_PARAMETER = "/linkParameter";
    private static final String XPATH_PARAMETER_NAME = "/name";
    private static final String XPATH_PARAMETER_VALUE = "/value";
    private static final String XPATH_ON_CLICK_SCRIPT = "/onClick";
    private static final String XPATH_DIVIDER_CLASS = "/dividerClass";

    private static final String NL = System.getProperty("line.separator");

    //use table layout for now - want to use display-table and float/one true layout for ie6/7, 
    // but quirks mode is causing issues with that in internet explorer
    //NOTE: when ie6, 7 are no longer supported this should be set to FALSE 
    // so we use css rather than table html elements
    private static final boolean USE_TABLE_LAYOUT = true;

    private DocumentHandler navigationMenuBarDoc = null;
    private String currentMenu = null;
    private ResourceManager resMgr = null;
    private SessionWebBean userSession = null;
    private FormManager formMgr = null;
    private HttpServletResponse response = null;

    private ConditionalDisplay condDisplay = null;
    private Hashtable myLinkItems = null;
    
    
    public MyLinksFactory(String xmlFileName, String currentMenu,
            ResourceManager resMgr, SessionWebBean userSession,
            FormManager formMgr, HttpServletResponse response,
            ConditionalDisplay condDisplay, Hashtable myLinkItems ) {
        //read the xml file from the xml file cache
        // which should help with performance
        //dev note: this requires restart of app to update
        // menu.  in prod this would be expected becuase the
        // xml file is part of the ear!
        navigationMenuBarDoc = 
            XmlConfigFileCache.readConfigFile(xmlFileName);

        this.currentMenu = currentMenu;
        this.resMgr = resMgr;
        this.userSession = userSession;
        this.formMgr = formMgr;
        this.response = response;
        this.condDisplay = condDisplay;
        this.myLinkItems = myLinkItems;
    }

    

    /** disallow default constructor*/
    private MyLinksFactory() {
    }
    
    /*
     * Create menu bar item.
     *
     * @param: String menuBarItemDisplayKey - menu bar item to display
     * @param: String securityRights
     * @param: Map displayRights - map of display rights 
     * @param: ResourceManager resMgr
     * @param: FormManager formMgr
     * @param: HttpServletResponse
     */
    public String createFavoriteBarItems(String menuBarItemDisplayKey) {

        StringBuffer output = new StringBuffer();

        DocumentHandler mainBar = navigationMenuBarDoc.getComponent(XPATH_MENU_BAR_VAL);
        Vector menus = mainBar.getFragments(XPATH_MENU_VAL);
        
        Iterator it = menus.iterator(); 

        //find the correct menu bar item
        //todo: this would be quicker if we read the doc in constructor and put menu items in a hashtable
        while (it.hasNext() ){
            DocumentHandler menu = (DocumentHandler) it.next();

                String conditionalDisplay = menu.getAttribute(XPATH_COND_DISPLAY_VAL); 
                if ( shouldDisplay( conditionalDisplay ) ) {
	                createMenuBarItem(menu, output);
                }
        }
        
        return output.toString();
    }

    private void createMenuBarItem(DocumentHandler menu, StringBuffer output) {

        String displayKey = menu.getAttribute(XPATH_DISPLAY_KEY_VAL); 

        // displayLable from property file
        String menuDisplayText = resMgr.getText(displayKey, TradePortalConstants.TEXT_BUNDLE);

        // menu names
        Vector items = menu.getFragments(XPATH_ITEM_VAL);
        Vector categories = menu.getFragments(XPATH_CAT_VAL);

        //first deal with popup menu bars
        if((items!=null && items.size()>0) || (categories!=null && categories.size()>0) ){
            if ( displayKey != null && displayKey.equals(currentMenu) ) {
            }
            
            createMegaMenuDropdown(displayKey, items, categories, output);
            
            
        } else if(isFavouriteLink(displayKey)){ //no popup -- menu bar item is a link on its own
            String linkAction = menu.getAttribute(XPATH_LINK_ACTION_NAME); 
            Vector linkParameters = menu.getFragments(XPATH_LINK_PARAMETER);
            String linkHref =  getHrefLink(linkAction, linkParameters);
            
    
            //only generate the link if it exists
            if ( linkHref!=null && linkHref.length()>0 ) {
                //generate the link if this is not currently selected
                if ( currentMenu==null || !currentMenu.equals(displayKey) ) {
                    //use onclick rather than anchor
                }
            }
            output.append("<li");
            output.append(">");
            
            output.append("<a");
            //set id if an onclick for eventhandling selection
                output.append(" id=\""+formatId(menuDisplayText)+"\"");
            //only generate the link if it exists
            if ( linkHref!=null && linkHref.length()>0 ) {
                output.append(" href=\""+linkHref+"\" ");
            }
            output.append(">" + menuDisplayText + "</a>");
            
            output.append("</li>");
            if ( displayKey != null && displayKey.equals(currentMenu) ) {
            }
            output.append(NL);
        }
    }

    private void createMegaMenuDropdown(String menuBarItemDisplayKey, 
            Vector items, Vector categories, StringBuffer output) {
        //include style display:none so the popup will initially not display
        // before dojo is ready.  dojo handles ensuring they render when 
        // menu bar items are selected.
        output.append(NL);
        output.append(NL);
        if ( USE_TABLE_LAYOUT ) {
        } else {
        }
        output.append(NL);
        
        //This if block will execute if menu is having directly menu items.
        if(items!=null && items.size()>0){
            if(categories!=null && categories.size()>0){
                if ( USE_TABLE_LAYOUT ) {
                } else {
                }
                output.append(NL);
                output.append(NL);

                Iterator ite = items.iterator(); 
                while (ite.hasNext() ){
                    DocumentHandler item = (DocumentHandler) ite.next();
                    createMenuItem(item, output);
                }

                if ( USE_TABLE_LAYOUT ) {
                } else {
                }
                output.append(NL);

                //because categories follow, add a divider
                if ( USE_TABLE_LAYOUT ) {
                } else {
                }
                output.append(NL);
                output.append(NL);
                if ( USE_TABLE_LAYOUT ) {
                } else {
                }
                output.append(NL);
            }else{
                
                Iterator ite = items.iterator(); 
                if ( USE_TABLE_LAYOUT ) {
                } else {
                }
                output.append(NL);
                output.append(NL);
                output.append(NL);
                while (ite.hasNext() ){
                    DocumentHandler item = (DocumentHandler) ite.next();
                    createMenuItem(item, output);
                }
                output.append(NL);
                output.append("      </div>");
                output.append(NL);
                output.append(NL);
            }
        }

        //This if block will execute if menu is having menu items under different categories.
        if(categories!=null && categories.size()>0){

            output.append(NL);

            Iterator cat = categories.iterator(); 
            for (int catIdx=0; cat.hasNext(); catIdx++ ){
                DocumentHandler category = (DocumentHandler) cat.next();
                createCategory(menuBarItemDisplayKey, category, cat.hasNext(), catIdx!=0, output);                
            }
            
            output.append(NL);
        }
        
        output.append(NL);
        
        output.append(NL);
        output.append(NL);
    }

    private void createCategory(String menuBarItemDisplayKey, 
            DocumentHandler category, 
            boolean catAfter, boolean catBefore, //information on category placement 
            StringBuffer output) {
        String displayKey = category.getAttribute(XPATH_DISPLAY_KEY_VAL); 
        String conditionalDisplay = category.getAttribute(XPATH_COND_DISPLAY_VAL); 

        //check displayRights
        if ( shouldDisplay( conditionalDisplay ) ) {

            //add classes to the column element to 
            // create the vertical divider
            //- this replaces a specific column for the divider below
            
            output.append(NL);

            
            output.append("       <ul>");
            output.append(NL);
            
            Vector itemsC = category.getFragments(XPATH_ITEM_VAL);
            if(itemsC!=null){
                Iterator iteC = itemsC.iterator(); 
                while (iteC.hasNext() ){
                    DocumentHandler item = (DocumentHandler) iteC.next();
                    createMenuItem(item, output);
                }
            }
            output.append("       </ul>");
            output.append(NL);
            
            output.append(NL);
        }
    }


    private void createMenuItem(DocumentHandler item, StringBuffer output) {

        String displayKey = item.getAttribute(XPATH_DISPLAY_KEY_VAL);
        String conditionalDisplay = item.getAttribute(XPATH_COND_DISPLAY_VAL); 
        String linkAction = item.getAttribute(XPATH_LINK_ACTION_NAME); 
        Vector linkParameters = item.getFragments(XPATH_LINK_PARAMETER);
        String onClick = item.getAttribute(XPATH_ON_CLICK_SCRIPT); 
        String dividerClass = item.getAttribute(XPATH_DIVIDER_CLASS);  

        //check security and displayRights
        if ( shouldDisplay( conditionalDisplay ) && isFavouriteLink(displayKey) ) {

            // displayText from property file
            String displayText = resMgr.getText(displayKey, TradePortalConstants.TEXT_BUNDLE);
        
            String linkHref =  getHrefLink(linkAction, linkParameters);
            
            output.append("<li");
            output.append(">");
            
            output.append("<a");
            //set id if an onclick for eventhandling selection
            if ( onClick != null && onClick.length()>0 ) {
                output.append(" id=\""+formatId(displayKey)+"\"");
            }
            //only generate the link if it exists
            if ( linkHref!=null && linkHref.length()>0 ) {
                output.append(" href=\""+linkHref+"\" ");
            }
            output.append(">" + displayText + "</a>");
            
            output.append("</li>");
            output.append(NL);

            if(dividerClass!=null && dividerClass.length()>0 ) {
                output.append(NL);
            }
        
        }
    }

    /**
     * Determines whether to display the item or not.
     * 
     * @param displayKey
     * @param securityKey
     * @param securityRights
     * @param displayRights
     * @return
     */
    /**
     * Determines whether to display the item or not.
     * 
     * @param conditionalDisplay
     * @return
     */
    private boolean shouldDisplay(String conditionalDisplay) {
        boolean display = true;
        //only go conditional if menu specified a value
        if ( conditionalDisplay != null && conditionalDisplay.length()>0 ) {
            //and we have a conditionalDisplay instance
            if ( condDisplay != null ) {
                display = condDisplay.shouldDisplay( conditionalDisplay );
            }
        }
        return display;
    }
    
    /**
     * Determines whether to display the item or not.
     * 
     * @param displayKey
     * @param securityKey
     * @param securityRights
     * @param displayRights
     * @return
     */
    private boolean isFavouriteLink(String displayKey) {
        boolean display = false; 
        if(myLinkItems.containsValue(displayKey)){
        	display = true;
        }
        return display;        
    }

    private String getHrefLink(String linkAction, Vector linkParameters) {
        String linkHref = "";
        String paramStr = "";

        if ( linkAction!=null && linkAction.length()>0 ) {
            //add the link parameters
            if(linkParameters!=null){
                for (Iterator iter = linkParameters.iterator(); iter.hasNext(); ) { 
                    DocumentHandler parm = (DocumentHandler) iter.next();
                    
                    String parmName = parm.getAttribute(XPATH_PARAMETER_NAME); 
                    String parmValue = parm.getAttribute(XPATH_PARAMETER_VALUE); 
                    paramStr += "&amp;" + parmName + "=" + parmValue;
                }
            }
    
            linkHref = formMgr.getLinkAsUrl(linkAction, paramStr, response);
        }
            
        return linkHref;
    }
    
    private String formatId(String value) {
        String id = "";
        if ( value != null ) {
            id = value.replace('.','_');
            id = id+"_"+"MyLink";
        }
        return id;
    }
    
    /*
     * Create menu bar item.
     *
     * @param: String menuBarItemDisplayKey - menu bar item to display
     */
    public String generateMenuBarItemEventHandlers(String menuBarItemDisplayKey) {

        StringBuffer output = new StringBuffer();

        DocumentHandler mainBar = navigationMenuBarDoc.getComponent(XPATH_MENU_BAR_VAL);
        Vector menus = mainBar.getFragments(XPATH_MENU_VAL);
        
        Iterator it = menus.iterator(); 

        //find the correct menu bar item
        //todo: this would be quicker if we read the doc in constructor and put menu items in a hashtable
        while (it.hasNext() ){
            DocumentHandler menu = (DocumentHandler) it.next();

            String displayKeyM = menu.getAttribute(XPATH_DISPLAY_KEY_VAL); 
            if ( menuBarItemDisplayKey.equals( displayKeyM ) ) {
                String conditionalDisplay = menu.getAttribute(XPATH_COND_DISPLAY_VAL); 
                if ( shouldDisplay( conditionalDisplay ) ) {
                    generateMenuBarItemEventHandlers(menu, output);
                }
            }
        }
        
        return output.toString();
    }

    private void generateMenuBarItemEventHandlers(DocumentHandler menu, StringBuffer output) {

        String displayKey = menu.getAttribute(XPATH_DISPLAY_KEY_VAL); 


        // menu names
        Vector items = menu.getFragments(XPATH_ITEM_VAL);
        Vector categories = menu.getFragments(XPATH_CAT_VAL);

        //first deal with popup menu bars
        if((items!=null && items.size()>0) || (categories!=null && categories.size()>0) ){
            generateMegaMenuEventHandlers(displayKey, items, categories, output);
        } else { //no popup -- menu bar item is a link on its own
            //todo: event handlers for menu bar item!!!
        }
    }

    private void generateMegaMenuEventHandlers(String menuBarItemDisplayKey, 
            Vector items, Vector categories, StringBuffer output) {
        
        //This if block will execute if menu is having directly menu items.
        if(items!=null && items.size()>0){
            if(categories!=null && categories.size()>0){

                //looping through the menu items
                Iterator ite = items.iterator(); 
                while (ite.hasNext() ){
                    DocumentHandler item = (DocumentHandler) ite.next();
                    generateMenuItemEventHandler(item, output);
                }

            }else{
                
                Iterator ite = items.iterator(); 
                while (ite.hasNext() ){
                    DocumentHandler item = (DocumentHandler) ite.next();
                    generateMenuItemEventHandler(item, output);
                }
            }
        }

        //This if block will execute if menu is having menu items under different categories.
        if(categories!=null && categories.size()>0){

            Iterator cat = categories.iterator(); 
            for (int catIdx=0; cat.hasNext(); catIdx++ ){
                DocumentHandler category = (DocumentHandler) cat.next();
                   
                generateCategoryEventHandlers(menuBarItemDisplayKey, category, cat.hasNext(), catIdx!=0, output);                
            }
            
        }
    }

    private void generateCategoryEventHandlers(String menuBarItemDisplayKey, 
            DocumentHandler category, 
            boolean catAfter, boolean catBefore, //information on category placement 
            StringBuffer output) {
        String conditionalDisplay = category.getAttribute(XPATH_COND_DISPLAY_VAL); 

        //check displayRights
        if ( shouldDisplay( conditionalDisplay ) ) {

            Vector itemsC = category.getFragments(XPATH_ITEM_VAL);
            if(itemsC!=null){
                Iterator iteC = itemsC.iterator(); 
                while (iteC.hasNext() ){
                    DocumentHandler item = (DocumentHandler) iteC.next();
                    generateMenuItemEventHandler(item, output);
                }
            }
        }
    }


    private void generateMenuItemEventHandler(DocumentHandler item, StringBuffer output) {

        String displayKey = item.getAttribute(XPATH_DISPLAY_KEY_VAL); 
        String conditionalDisplay = item.getAttribute(XPATH_COND_DISPLAY_VAL); 
        String onClick = item.getAttribute(XPATH_ON_CLICK_SCRIPT); 

        //check security and displayRights
        if ( shouldDisplay( conditionalDisplay ) ) {

            output.append(" query('#").append(formatId(displayKey)).append("')");
            output.append(".on('click', function() {").append(NL);
            if ( onClick != null && onClick.length()>0 ) {
                output.append("  menu.").append(onClick).append(NL);
            }
            output.append(" });").append(NL);
        }
    }

    

}
