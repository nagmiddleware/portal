    package com.ams.tradeportal.html;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
    
    import java.util.*;

import com.ams.tradeportal.common.*;
import com.amsinc.ecsg.util.StringFunction;

import javax.crypto.SecretKey;
    
    /**
     * This class is used to accumulate options for a dropdown list.  It is used by the
     * Dropdown class.
     *
     *     Copyright  � 2001                         
     *     American Management Systems, Incorporated 
     *     All rights reserved
     */
    public class DropdownOptions
     {
private static final Logger LOG = LoggerFactory.getLogger(DropdownOptions.class);
          // A list of all the options as strings.  Format is <description>\n\n\n<value>
          private Vector options;
          
          // The value which should be displayed as selected in the list
          private String selected;
          
          // A flag indicated whether or not the values should be encrypted
          private SecretKey secretKey = null;
          
          /**
           * Constructor
           */
          public DropdownOptions()
           {
               // Most dropdowns are under 30, but the ones that are above 30
               // are way above 30.  Set initial size of 30, but the growth 
               // increment is 150.
               options = new Vector(30, 150);
               selected = null;
           }
           
          /**
           * Setter for the encrypt flag.  Indicates that the values should be encrypted
           * when the option list is built as HTML.
           */           
          public void setSecretKeyForEncryption(SecretKey secretKey)
           { 
//        	 W Zhu 8/17/2012 Rel 8.1 T36000004579 add SecretKey parameter to allow different keys for each session for better security.
        	  this.secretKey = secretKey;
           }
           
          
          /**
           * Adds an option to the vector.  
           *
           * @param value - the value part of the option tag
           * @param description - the description part of the option tag
           *
           */
          public void addOption(String value, String description)
           {
               // Combine the description and value together as one string.  This way,
               // the code can sort on just this one string, then break it up later.
               // It is a way of keeping the value and description together.
               options.addElement(description+"\n\n\n"+value);
           }
           
          /**
           * Setter for which item in the list of options is to be displayed as selected
           * 
           * @param selected - the value to be selected
           */
          public void setSelected(String selected)
           {
               this.selected = StringFunction.xssHtmlToChars(selected);
           }
        
          /**
           * Creates unsorted option list in HTML format. 
           *
           * @return String - the HTML option list
           */
          public String createOptionListInHtml()
           {
              return createHtml(false, null);
           }

          /**
           * Creates sorted option list in HTML format. 
           *
           * @param String the locale of the user
           * @return String - the HTML option list
           */
          public String createSortedOptionListInHtml(String localeName)
           {
              return createHtml(true, localeName);
           }           
           
          /**
           * Creates HTML representation of the options that have been 
           * collected.  Performs sort if requested.
           *
           * @param localeName - the locale of the user
           * @param sort - flag indicating whether or not to sort
           * @return String - the HTML option list
           */           
          public String createHtml(boolean sort, String localeName)
           {
               // Pull the values out of the vector and place them into an array
               Enumeration enumer = options.elements();
               
               String[] values = new String[options.size()];
               int count=0;
               
               while(enumer.hasMoreElements())
                {
                   values[count++] = (String)enumer.nextElement();   
                }

               // If requested, run the QuickSort algorithm on the 
               // description+\n\n\n+value Strings
               if(sort)
                {
                    try 
                        {
                           Sort.sort(values, localeName);
                        }
                    catch(Exception e)
                        {
                    	LOG.error("Exception in createHtml()", e);
                        }
                }
               
               
               StringBuilder html = new StringBuilder();
               
               // Loop through the Strings after being sorted
               // and build the HTML
               for(int i=0; i<values.length; i++)
                {
                    // Parse out the value and description
                    int indexOfDelimiter = values[i].indexOf("\n\n\n");
                    String descr = values[i].substring(0,indexOfDelimiter);
                    String value = values[i].substring(indexOfDelimiter+3);

                    // Get the HTML
                    html.append(Dropdown.getOptionTag(value, descr, selected, secretKey));                    
                }
            
               // After getting the HTML, we no longer need these.  Clear them
               // out to speed up garbage collection
               options = null;
               values = null;
               selected = null;
            
               return html.toString();
           }
     }