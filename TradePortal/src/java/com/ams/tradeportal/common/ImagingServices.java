package com.ams.tradeportal.common;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
    
import java.util.*;
    
   /**
    * Contains various static methods for functionality dealing with imaging.
    *
    *     Copyright  � 2001                         
    *     American Management Systems, Incorporated 
    *     All rights reserved
    */
public class ImagingServices 
    {
private static final Logger LOG = LoggerFactory.getLogger(ImagingServices.class);
        // Store a list of imaging IDs and passwords
        private static Vector ids;
        private static Vector passwords;
            
        // The index of the next ID/password combination to be used
        private static int currentPosition;
                
        static
        {
            // Initialize the vectors
            ids = new Vector(50);
            passwords = new Vector(50);
                   
            // Set position at zero to start with
            currentPosition = 0;
                    
            // IDs and Passwords will be read from the Imaging.properties file    
            PropertyResourceBundle imagingProperties =
                    (PropertyResourceBundle)PropertyResourceBundle.getBundle ("Imaging");

            // Loop through the data from the file
            Enumeration keys = imagingProperties.getKeys();
                   
            while(keys.hasMoreElements())
                {
                    String idAndPassword = (String) keys.nextElement();
                        
                    // ID and password are delimited by a / 
                    int indexOfSlash = idAndPassword.indexOf("/");
                        
                    // Parse the string to find ID and password
                    String id = idAndPassword.substring(0, indexOfSlash);
                    String password = idAndPassword.substring(indexOfSlash+1);
                        
                    // Add the ID and password to the list
                    ids.addElement(id);
                    passwords.addElement(password);
                }
        }
         
        /**
         * This method is used to get an ID and password for use in accessing
         * the imaging server.  The ID and password are distributed in a round
         * robin fashion.  
         *
         *
         * @return IdAndPassword the ID and password to use for imaging
         */
       public static ImagingIdAndPassword getNextIdAndPassword()
        {
            // Lookup the ID and password in the list based on current position
            String id = (String) ids.elementAt(currentPosition);
            String password = (String) passwords.elementAt(currentPosition);

            // Increment the pointer
            currentPosition++; 
                 
            // If the pointer is past the end of the list, move it back to the 
            // beginning
            if(currentPosition == ids.size())
            {
                currentPosition = 0; 
            }
                 
            // Return an object containing the ID and password 
            return new ImagingIdAndPassword(id,password);
        }
    }