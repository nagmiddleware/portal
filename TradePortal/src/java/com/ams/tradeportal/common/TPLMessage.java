package com.ams.tradeportal.common;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.util.DocumentHandler;

/*
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class TPLMessage  {
private static final Logger LOG = LoggerFactory.getLogger(TPLMessage.class);


     public DocumentHandler TPLMsgDoc;
     public static final String headerPath           			= "/Proponix/Header";
     public static final String subHeaderPath      				= "/Proponix/SubHeader";
     public static final String bodyPath						= "/Proponix/Body/InstrumentTransaction";
     public static final String timestampPath           		= "/Proponix/Timestamp";
     public static final String instrumentTransactionPath		= "/Proponix/Body/InstrumentTransaction(0)";
     public static final String instrumentPath					= "/Proponix/SubHeader/InstrumentIdentification/Instrument";
     public static final String clientBankPath					= "/Proponix/SubHeader/InstrumentIdentification/ClientBank";
     public static final String transactionPath					= instrumentTransactionPath + "/Transaction";
     public static final String termsPath	       			    = instrumentTransactionPath + "/Terms";
     public static final String feePath							= instrumentTransactionPath + "/Fee";
     public static final String documentImagePath				= instrumentTransactionPath + "/DocumentImage";
     public static final String positionPath					= instrumentTransactionPath + "/Position";
     public static final String instrumentPositionPath		    = instrumentTransactionPath + "/RelatedInstrumentPosition";
     public static final String termsPartyPath					= instrumentTransactionPath + "/TermsParty";

   public TPLMessage (DocumentHandler inputMsgDoc)
   {

    this.TPLMsgDoc= inputMsgDoc;

   }

   public static DocumentHandler getUniversalMessage(DocumentHandler TPLMsgDoc)
   {
	   //Header
	   TPLMsgDoc.setAttribute(headerPath+"/DestinationID",				"");
	   TPLMsgDoc.setAttribute(headerPath+"/SenderID",					"");
	   TPLMsgDoc.setAttribute(headerPath+"/OperationOrganizationID",	"");
	   TPLMsgDoc.setAttribute(headerPath+"/MessageType",				"");
	   TPLMsgDoc.setAttribute(headerPath+"/DateSent",					"");
	   TPLMsgDoc.setAttribute(headerPath+"/TimeSent",					"");
	   TPLMsgDoc.setAttribute(headerPath+"/MessageID",					"");

       //SubHeader
       TPLMsgDoc.setAttribute(subHeaderPath+"/CustomerID","");
       TPLMsgDoc.setAttribute(subHeaderPath+"/HistoryIndicator","");
       TPLMsgDoc.setAttribute(subHeaderPath+"/TotalNumberOfEntries","1");

       //SubHeader
        //InstrumentIdentification
        TPLMsgDoc.setAttribute(instrumentPath+"/InstrumentIDNumber",  			"");
        TPLMsgDoc.setAttribute(instrumentPath+"/InstrumentIDPrefix",  			"");
        TPLMsgDoc.setAttribute(instrumentPath+"/InstrumentIDSuffix",  			"");
        TPLMsgDoc.setAttribute(instrumentPath+"/InstrumentID",  			    "");
        TPLMsgDoc.setAttribute(instrumentPath+"/InstrumentStatus",  			"");
        TPLMsgDoc.setAttribute(instrumentPath+"/InstrumentTypeCode",  			"");
        TPLMsgDoc.setAttribute(instrumentPath+"/IssueDate",  			        "");
        TPLMsgDoc.setAttribute(instrumentPath+"/RelatedInstrumentID",  			"");
        TPLMsgDoc.setAttribute(instrumentPath+"/RelatedActivityType",  			"");
        TPLMsgDoc.setAttribute(instrumentPath+"/RelatedActivitySequenceNo",     "");
        TPLMsgDoc.setAttribute(instrumentPath+"/POInvoicePortfolioUoid",  	    "");
        TPLMsgDoc.setAttribute(instrumentPath+"/OTLInstrumentUoid",  			"");

        //ClientBank
        TPLMsgDoc.setAttribute(clientBankPath+"/OTLID",  			            "");

        //Language
        TPLMsgDoc.setAttribute(subHeaderPath+"/Language",                       "");
        
        // Display Change Activity
        TPLMsgDoc.setAttribute(subHeaderPath+"/DisplayChangeActivity",                       "");


       //End of SubHeader

	  //Body
	   //Transaction
	   TPLMsgDoc.setAttribute(transactionPath+"/TransactionTypeCode", "");
	   TPLMsgDoc.setAttribute(transactionPath+"/SequenceNumber", "");
	   TPLMsgDoc.setAttribute(transactionPath+"/TransactionStatus", "");
	   TPLMsgDoc.setAttribute(transactionPath+"/TransactionStatusDate", "");
	   TPLMsgDoc.setAttribute(transactionPath+"/SequenceDate", "");
	   TPLMsgDoc.setAttribute(transactionPath+"/ReturnMessageID", "");
	   TPLMsgDoc.setAttribute(transactionPath+"/RejectReason", "");
	   TPLMsgDoc.setAttribute(transactionPath+"/ResultingPortActivityUoid", "");
	   TPLMsgDoc.setAttribute(transactionPath+"/PriorActivePortActivityUoid", "");
	   TPLMsgDoc.setAttribute(transactionPath+"/OTLActivityUoid", "");

	   //Terms
	   //TermDetails
	   TPLMsgDoc.setAttribute(termsPath+"/TermsDetails/ValueDate",		"");
	   TPLMsgDoc.setAttribute(termsPath+"/TermsDetails/ExpiryDate",		"");
	   TPLMsgDoc.setAttribute(termsPath+"/TermsDetails/ReferenceNumber",		"");
	   TPLMsgDoc.setAttribute(termsPath+"/TermsDetails/OpeningBankReferenceNumber",		"");
	   TPLMsgDoc.setAttribute(termsPath+"/TermsDetails/AmountCurrencyCode",		"");
	   TPLMsgDoc.setAttribute(termsPath+"/TermsDetails/Amount",		"");
	   TPLMsgDoc.setAttribute(termsPath+"/TermsDetails/AmountTolerancePositive",		"");
	   TPLMsgDoc.setAttribute(termsPath+"/TermsDetails/AmountToleranceNegative",		"");
	   TPLMsgDoc.setAttribute(termsPath+"/TermsDetails/RelatedInstrumentID",		"");
	   TPLMsgDoc.setAttribute(termsPath+"/TermsDetails/WorkItemNumber",		"");
       TPLMsgDoc.setAttribute(termsPath+"/TermsDetails/DiscountRate",		"");
	   TPLMsgDoc.setAttribute(termsPath+"/TermsDetails/TotalRate",		"");
	   TPLMsgDoc.setAttribute(termsPath+"/TermsDetails/InvoiceOnlyIndicator",		"");
	   TPLMsgDoc.setAttribute(termsPath+"/TermsDetails/InvoiceDueDate",		"");
	   TPLMsgDoc.setAttribute(termsPath+"/TermsDetails/FinancingAllowed",		"");
	   TPLMsgDoc.setAttribute(termsPath+"/TermsDetails/FinancePercentage",		"");
	   TPLMsgDoc.setAttribute(termsPath+"/TermsDetails/UnappliedAmount",		"0.00");
	   TPLMsgDoc.setAttribute(termsPath+"/TermsDetails/FinalExpiryDate",		"");
	   
	   //DocumentsRequired
	   TPLMsgDoc.setAttribute(termsPath+"/DocumentsRequired/TotalNumberOfEntries",		"0");
	   
	   // ShipmentTerms
	   TPLMsgDoc.setAttribute(termsPath+"/ShipmentTerms/LatestShipmentDate",		"");
	   
	   // Instructions
	   TPLMsgDoc.setAttribute(termsPath+"/Instructions/TracerSendType",		"");
	  
	   //PaymentTerms
       TPLMsgDoc.setAttribute(termsPath+"/PaymentTerms/PaymentTermsType",		"");
	   TPLMsgDoc.setAttribute(termsPath+"/PaymentTerms/PaymentTermsNumberDaysAfter",		"");
	   TPLMsgDoc.setAttribute(termsPath+"/PaymentTerms/PaymentTermsDaysAfterType",		"");
	   TPLMsgDoc.setAttribute(termsPath+"/PaymentTerms/PaymentTermsPercentInvoice",		"");
	   TPLMsgDoc.setAttribute(termsPath+"/PaymentTerms/PaymentTermsFixedMaturityDate",		"");

	   //OtherConditions
	   TPLMsgDoc.setAttribute(termsPath+"/OtherConditions/ConfirmationType",		"");

       TPLMsgDoc.setAttribute(termsPath+"/CollectionDate",		"");
       TPLMsgDoc.setAttribute(termsPath+"/UsanceMaturityDate",		"");
       TPLMsgDoc.setAttribute(termsPath+"/AdjustmentType",		"");
       TPLMsgDoc.setAttribute(termsPath+"/AccountReceivableType",		"");
       TPLMsgDoc.setAttribute(termsPath+"/OpeningBankInstrumentNumber",		"");
     //LoanTerms
       TPLMsgDoc.setAttribute(termsPath+"/LoanTerms/LoanTermsType",		"");
	   TPLMsgDoc.setAttribute(termsPath+"/LoanTerms/LoanRate",		"");
	   TPLMsgDoc.setAttribute(termsPath+"/LoanTerms/LoanTermsFixedMaturityDate",		"");
	   TPLMsgDoc.setAttribute(termsPath+"/LoanTerms/LoanTermsNumberOfDays",		"");
	   
       TPLMsgDoc.setAttribute(termsPath+"/PresentationDays",		"");

       // End Terms


       //Fee
	   TPLMsgDoc.setAttribute(feePath+"/TotalNumberOfEntries",			"0");
	   TPLMsgDoc.setAttribute(feePath+"/FullFee/SettlementCurrencyCode",			"");
	   TPLMsgDoc.setAttribute(feePath+"/FullFee/SettlementCurrencyAmount",			"");
	   TPLMsgDoc.setAttribute(feePath+"/FullFee/ChargeType",			"");
	   TPLMsgDoc.setAttribute(feePath+"/FullFee/BaseCurrencyAmount",			"");
	   TPLMsgDoc.setAttribute(feePath+"/FullFee/AccountNumber",			"");
	   TPLMsgDoc.setAttribute(feePath+"/FullFee/SettlementHowType",			"");

       //DocumentImage
	   TPLMsgDoc.setAttribute(documentImagePath+"/TotalNumberOfEntries",			"0");

       //Position
       TPLMsgDoc.setAttribute(positionPath+"/AvailableAmount",			"0.00");
 	   TPLMsgDoc.setAttribute(positionPath+"/EquivalentAmount",			"0.00");
       TPLMsgDoc.setAttribute(positionPath+"/LiabilityAmountInBaseCurrency",			"0.00");
       TPLMsgDoc.setAttribute(positionPath+"/LiabilityAmountInLimitCurrency",			"0.00");
       TPLMsgDoc.setAttribute(positionPath+"/InstrumentBaseCurrencyCode",			"");
       TPLMsgDoc.setAttribute(positionPath+"/InstrumentLimitCurrencyCode",			"");
       TPLMsgDoc.setAttribute(positionPath+"/CurrentInstrumentAmount",			"0.00");
       TPLMsgDoc.setAttribute(positionPath+"/RevalueDate",			"");
       
       // Related Instrument Position
       TPLMsgDoc.setAttribute(instrumentPositionPath+"/AvailableAmount",			"");
 	   TPLMsgDoc.setAttribute(instrumentPositionPath+"/EquivalentAmount",			"");
       TPLMsgDoc.setAttribute(instrumentPositionPath+"/LiabilityAmountInBaseCurrency",			"");
       TPLMsgDoc.setAttribute(instrumentPositionPath+"/LiabilityAmountInLimitCurrency",			"");
       TPLMsgDoc.setAttribute(instrumentPositionPath+"/InstrumentBaseCurrencyCode",			"");
       TPLMsgDoc.setAttribute(instrumentPositionPath+"/InstrumentLimitCurrencyCode",			"");
       TPLMsgDoc.setAttribute(instrumentPositionPath+"/InstrumentCurrencyCode",			"");
       TPLMsgDoc.setAttribute(instrumentPositionPath+"/CurrentInstrumentAmount",			"");

       //TermsParty
       TPLMsgDoc.setAttribute(termsPartyPath+"/TotalNumberOfEntries",			"0");
	   TPLMsgDoc.setAttribute(termsPartyPath+"/FullTermsParty/TermsPartyType",			"");
	   TPLMsgDoc.setAttribute(termsPartyPath+"/FullTermsParty/Name",			"");
	   TPLMsgDoc.setAttribute(termsPartyPath+"/FullTermsParty/AddressLine1",			"");
	   TPLMsgDoc.setAttribute(termsPartyPath+"/FullTermsParty/AddressLine2",			"");
	   TPLMsgDoc.setAttribute(termsPartyPath+"/FullTermsParty/AddressLine3",			"");
	   TPLMsgDoc.setAttribute(termsPartyPath+"/FullTermsParty/AddressCity",			"");
	   TPLMsgDoc.setAttribute(termsPartyPath+"/FullTermsParty/AddressStateProvince",			"");
	   TPLMsgDoc.setAttribute(termsPartyPath+"/FullTermsParty/AddressCountry",			"");
   	   TPLMsgDoc.setAttribute(termsPartyPath+"/FullTermsParty/AddressPostalCode",			"");
   	   TPLMsgDoc.setAttribute(termsPartyPath+"/FullTermsParty/PhoneNumber",			"");
       TPLMsgDoc.setAttribute(termsPartyPath+"/FullTermsParty/FaxNumber1",			"");
   	   TPLMsgDoc.setAttribute(termsPartyPath+"/FullTermsParty/ContactName",			"");
   	   TPLMsgDoc.setAttribute(termsPartyPath+"/FullTermsParty/OTLCustomerID",			"");
   	   TPLMsgDoc.setAttribute(termsPartyPath+"/FullTermsParty/OTLAddressSequenceNo",			"0");
   	   TPLMsgDoc.setAttribute(termsPartyPath+"/FullTermsParty/VendorID",			"");
   	   TPLMsgDoc.setAttribute(termsPartyPath+"/FullTermsParty/ReferenceNumber",			"");
   	   
   	   //Timestamp
   	   TPLMsgDoc.setAttribute(timestampPath+"/Date",			"");
 	   TPLMsgDoc.setAttribute(timestampPath+"/Time",			"");

	   LOG.debug(TPLMsgDoc.toString());

	   return TPLMsgDoc;

   }



}