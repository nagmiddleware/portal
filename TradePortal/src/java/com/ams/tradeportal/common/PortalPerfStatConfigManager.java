package com.ams.tradeportal.common;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.EJBObjectFactory;
import com.amsinc.ecsg.util.JPylonProperties;
import com.ams.tradeportal.busobj.*;

import java.rmi.RemoteException;
import java.util.MissingResourceException;
import java.util.PropertyResourceBundle;

public class PortalPerfStatConfigManager {
private static final Logger LOG = LoggerFactory.getLogger(PortalPerfStatConfigManager.class);

    /**
     * Retrieve the config_setting from the DB.  <br><br>
     *
     * if overrideConfigSettingWithPropertiesFile=Y in TradePortal.properties,
     * the value in config_setting is overridden by the value in the properties
     * file, if the entry is present.
     *
     * @param applicationName - PORTAL_APP or PORTAL_AGENT
     * @param className - PORTAL or AGENT
     * @param settingName - Setting Name
     * @param defaultValue - Default Setting Value if there is no entry in
     * config_setting
     * @return setting_value of config_setting
     * @throws AmsException
     * @throws RemoteException
     */
    public static String getPerfStatConfig(String pageName, String clientBank, String defaultValue)
            throws AmsException, RemoteException {
//        String overrideConfigSettingWithPropertiesFile = "N";
        String settingValue = null;
        try {
            PropertyResourceBundle portalProperties = (PropertyResourceBundle) PropertyResourceBundle.getBundle("TradePortal");
//            overrideConfigSettingWithPropertiesFile = portalProperties.getString("overrideConfigSettingWithPropertiesFile");
        } catch (MissingResourceException e) {
        }

        JPylonProperties jPylonProperties = JPylonProperties.getInstance();
        String serverLocation = jPylonProperties.getString("serverLocation");

        PerfStatConfig perfStatConfig = (PerfStatConfig) EJBObjectFactory.createClientEJB(serverLocation, "PerfStatConfig");
        perfStatConfig.getDataFromCache(pageName, clientBank);
        settingValue = perfStatConfig.getAttribute("active");
        if (settingValue == null || "".equals(settingValue)) {
            settingValue = defaultValue;
        }

        return settingValue;

    }

}
