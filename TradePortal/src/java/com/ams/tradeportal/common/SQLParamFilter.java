package com.ams.tradeportal.common;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.util.*;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

/**
 *
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class SQLParamFilter
 {
private static final Logger LOG = LoggerFactory.getLogger(SQLParamFilter.class);
    
    static final Pattern numberPattern = Pattern.compile("[-+\\.\\d]*|null");
  
    /**
     *
     *
     * @param sqlParam - the string to be filtered
     * @return the filtered string
     */
    public static String filter(String sqlParam)
     {
         if (sqlParam==null) return null;
        // If string contains the tick character ('), change it to double tick ('') so
        // that Oracle will interpet it as a single tick

        // Do an indexOf first so that the code doesn't go into the change() method
        // unless it needs to
        if( sqlParam.indexOf('\'') >= 0)
         {
            sqlParam = StringService.change(sqlParam, "'", "''");
         }
        
        // If string contains the escape sequence for the tick character, %27, change
        // it to double tick ('')  so that Oracle will interpet it as a single tick

        // Do an indexOf first so that the code doesn't go into the change() method
        // unless it is likely to need to do so
        if( sqlParam.indexOf('%') >= 0)
         {
            sqlParam = StringService.change(sqlParam, "%27", "''");
         }       

        return sqlParam;
     }

    public static String filterNumber(String sqlParam) {
        if (sqlParam==null) return null;
        if ( numberPattern.matcher(sqlParam.trim()).matches()) {
            return sqlParam;
        }
        else {
            LOG.info("SQLParamFilter.filterNumber: Invalid number parameter " + sqlParam + ". Substitute with -1.");
            return "-1";
        }
    }
    
    /**
     * This method will take ',' separated string value, and construct string with place holder(?,?) string for in clause, and adds the values to List
     * @param tradeInstrTypes
     * @param sqlParams
     * @return String
     */
    public static String preparePlaceHolderStrForInClause(String inParams, List<Object> sqlParams){
        
	    StringBuilder placeHolderStr = new StringBuilder("");
	    inParams = inParams.replaceAll("'", "");
    
		String[] parts = inParams.split(",");
			if(parts != null){
		     for (int i = 0; i < parts.length; i++) {
		    	 placeHolderStr.append("?");
		    		 if(parts.length-1 > i){
		    			 placeHolderStr.append(",");
			        	}
		    		 sqlParams.add(parts[i].trim());
		     }
		}
	    return placeHolderStr.toString();
    }
    
    /**
     * This method will input params and actual db details and compare
     * @param inParams
     * @param actualColumns
     * @return String  matched columns
     */
    public static String validateDBColumns(String inParams, String[] actualColumns){
        
          inParams = inParams.replaceAll("'", "");
          StringBuilder returnColumns = new StringBuilder("");
    
            String[] params = inParams.split(",");
                  if(params != null){
                 for (int i = 0; i < params.length; i++) {
                   if(Arrays.asList(actualColumns).contains(params[i])){
                         if(!returnColumns.toString().equals("")){
                               returnColumns.append(",");
                         }
                         returnColumns.append(params[i]);
                   }
                 }
            }
          return returnColumns.toString();
    }
 }