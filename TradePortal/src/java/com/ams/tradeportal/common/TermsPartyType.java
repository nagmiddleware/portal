package com.ams.tradeportal.common;

public enum TermsPartyType {
	;

	// Terms Party types
	public final static String ADVISING_BANK       = "ACB";
	public final static String AGENT               = "AGT";
	public final static String APPLICANT           = "APP";
	public final static String ASSIGNEE_01         = "A01";
	public final static String ASSIGNEE_BANK       = "ASB";
	public final static String BENEFICIARY         = "BEN";
	public final static String BENEFICIARY_BANK    = "BBK";
	public final static String BORROWER            = "OBL";
	public final static String CASE_OF_NEED        = "CON";
	public final static String COLLECTING_BANK     = "COL";
	public final static String DESIG_BANK          = "DSB"; //Not a refdata code
	public final static String DRAWEE_BUYER        = "DOP";
	public final static String DRAWER_SELLER       = "DWR";
	public final static String FREIGHT_FORWARD     = "CAR";
	public final static String NOTIFY_PARTY        = "NOT";
	public final static String OPENING_BANK        = "OPB";
	public final static String OTHER_CONSIGNEE     = "CSN";
	public final static String OVERSEAS_BANK       = "OSB"; //Note GUAR_ISSUE_OVERSEAS_BANK also exits
	public final static String RELEASE_TO_PARTY    = "RTO";
	public final static String TRANSFEREE          = "TRN";
	public final static String TRANSFEREE_NEGOT    = "TNB";
	public final static String THIRD_PARTY         = "TDP";
	public final static String APPLICANT_BANK      = "APB";
	public final static String ADVISE_THROUGH_BANK = "NAB";
	public final static String REIMBURSING_BANK    = "RMB";
	public final static String ISSUING_BANK        = "NBI";
	//  rkrishna CR 375-D ATP-ISS 07/21/2007 Begin
	public final static String INSURING_PARTY      = "INS";
	public final static String ATP_BUYER           = "BUY";
	public final static String ATP_SELLER          = "SEL";
	//  rkrishna CR 375-D ATP-ISS 07/21/2007 End
	public final static String PAYER               = "PAY";
	public final static String PAYEE               = "PAE";
	public final static String PAYEE_BANK          = "PEB";
	//IAZ CM CR-507 12/22/09 BEGIN
	public final static String ORDERING_PARTY      = "OPY";
	public final static String ORDERING_PARTY_BANK = "OPK";
	public final static String FIRST_INTERMED_BANK = "IP1";
	public final static String SECND_INTERMED_BANK = "IP2";
	//public final static String PAYER_BANK          = "PYB";	// IAZ 01/30/10 CHF
	public final static String PAYER_BANK          = "PRB";		// IAZ 01/30/10 CHT
	//IAZ CM CR-507 12/22/09 END
	//IR BKUL040547648 Rel 7000 - AAlubala 04/06/2011 - Add a new Party type
	public final static String PAYMENT_OWNER = "POW";

}
