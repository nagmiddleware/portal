package com.ams.tradeportal.common;

public enum InstrumentType {
;
	// Instrument types
	public static final String STANDBY_LC     = "SLC";
	public static final String EXPORT_DLC     = "EXP_DLC";
	public static final String GUARANTEE      = "GUA";
	public static final String EXPORT_COL     = "EXP_COL";
	public static final String NEW_EXPORT_COL = "EXP_OCO";
	public static final String AIR_WAYBILL    = "AIR";
	public static final String SHIP_GUAR      = "SHP";
	public static final String IMPORT_DLC     = "IMP_DLC";
	public static final String INCOMING_SLC   = "INC_SLC";
	public static final String LOAN_RQST      = "LRQ";
	public static final String XFER_BET_ACCTS  = "FTBA"; 
	public static final String DOMESTIC_PMT    = "FTDP"; 
	public static final String FUNDS_XFER     = "FTRQ";
	public static final String DOM_PMT     	  = "FTDP";
	public static final String APPROVAL_TO_PAY = "ATP"; 
	public static final String REQUEST_ADVISE = "RQA";
	public static final String DIRECT_DEBIT_INSTRUCTION     = "DDI";
	public static final String CLEAN_BA       = "CBA";
	public static final String DOCUMENTARY_BA = "DBA";
	public static final String REFINANCE_BA   = "RBA";
	public static final String INDEMNITY      = "LOI";
	public static final String DEFERRED_PAY   = "DFP";
	public static final String COLLECT_ACCEPT = "TAC";
	public static final String INCOMING_GUA   = "INC_GUA";
	public static final String IMPORT_COL     = "IMC";
	public static final String RECEIVABLES_PAYBLES_MANAGEMENT     = "REC";
	public static final String RECEIVABLES_FINANCE_INSTRUMENT     = "RFN";
	public static final String PAYABLE_MANAGEMENT     = "PYB";
	public static final String BILLING        = "BIL";
	public static final String IMPORT_BANKER_ACCEPTANCE = "IMP_DBA";
    public static final String EXPORT_BANKER_ACCEPTANCE = "EXP_DBA";
    public static final String IMPORT_TRADE_ACCEPTANCE  = "IMP_TAC";
    public static final String EXPORT_TRADE_ACCEPTANCE  = "EXP_TAC";
    public static final String IMPORT_DEFERRED_PAYMENT  = "IMP_DFP";
    public static final String EXPORT_DEFERRED_PAYMENT  = "EXP_DFP";
    public static final String PAYABLES_MGMT = "PYB";
    public static final String SUPPLIER_PORTAL = "SUPPLIER_PORTAL";
    public static final String MESSAGES = "MESSAGES";
    public static final String SUPP_PORT_INST_TYPE = "LRQ_INV";
    public static final String HTWOH_INV_INST_TYPE = "H2H_INV_CRN";

}
