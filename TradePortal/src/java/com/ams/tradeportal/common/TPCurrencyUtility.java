package com.ams.tradeportal.common;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.Hashtable;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.ErrorManager;
import com.amsinc.ecsg.frame.ReferenceDataManager;
import com.amsinc.ecsg.util.StringFunction;

/*
 *
 *     Copyright  © 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class TPCurrencyUtility {
private static final Logger LOG = LoggerFactory.getLogger(TPCurrencyUtility.class);

    // Contains currency code/locale specific formatters.  These
    // are added as they are created.

    private static Hashtable cachedFormatters = new Hashtable(50);

    //<Amit - IR EEUE032659500 -05/24/2005 >
    private static Hashtable cachedFormattersWithoutSeparators = new Hashtable(50);
    //</Amit - IR EEUE032659500 -05/24/2005 >

    /**
     * Create a formatter based on two conditions. First, the formatter should
     * format numbers based on the user's locale. Second, it should set the
     * number of decimal positions based on the passed in currency code. If no
     * currency code is available, the number of decimal places remains
     * unchanged.
     *
     * @return java.text.NumberFormat - resulting number format instance
     * @param currCd java.lang.String - determines number of decimals places
     * @param locale java.lang.String - determines format for number
     */
    private static NumberFormat getDecimalFormatter(int decimalPlaces, String locale) {

        NumberFormat formatter = (NumberFormat) cachedFormatters.get("DECIMAL" + decimalPlaces + locale);

        if (formatter != null) {
            return formatter;
        }

        // Create a default number formatter.
        formatter = NumberFormat.getNumberInstance(Locale.US);

        try {
		// Now, create a number format pattern.  This is of the form
            // #,##0.00 where the number of 0's following the decimal is based
            // on the number computed above.
            StringBuilder pattern = new StringBuilder("#,##0");

            for (int i = 0; i < decimalPlaces; i++) {
                if (i == 0) {
                    pattern.append(".");
                }
                pattern.append("0");
            }

		// Parse the desired locale string into a locale object.  (String
            // assumed to be of form xx_YY.
            Locale newLocale = new Locale(locale.substring(0, 2),
                    locale.substring(3, 5));

		// Get a number formatter for the desired locale.  This gets the proper
            // thousand and decimal separators.
            formatter = NumberFormat.getNumberInstance(newLocale);

            // Now apply the new pattern we just created to this number format
            ((DecimalFormat) formatter).applyPattern(pattern.toString());

		// Now, having gone through all this work to create a formatter,
            // cache it so we don't have to do the work again.
            cachedFormatters.put("DECIMAL" + decimalPlaces + locale, formatter);

        } catch (Exception e) {
		// This is likely the result of a bad locale or bad currency code.  We'll simply
            // return the default formatter (for US)
        }

        return formatter;
    }

      //<Amit - IR EEUE032659500 -05/24/2005 >
    // New method added for formatting the amount value to be displayed on the PO line items grid
    /**
     * Create a formatter based on two conditions. First, the formatter should
     * format numbers based on the user's locale without the thousand
     * separators. Second, it should set the number of decimal positions based
     * on the passed in currency code. If no currency code is available, the
     * number of decimal places remains unchanged.
     *
     * @return java.text.NumberFormat - resulting number format instance
     * @param decimalPlaces int - determines number of decimals places
     * @param locale java.lang.String - determines format for number
     */
    private static NumberFormat getDecimalFormatterWithoutThousandSeparator(int decimalPlaces, String locale) {

        NumberFormat formatter = (NumberFormat) cachedFormattersWithoutSeparators.get("DECIMAL" + decimalPlaces + locale);

        if (formatter != null) {
            return formatter;
        }

        // Create a default number formatter.
        formatter = NumberFormat.getNumberInstance(Locale.US);

        try {
	  		// Now, create a number format pattern.  This is of the form
            // #0.00 where the number of 0's following the decimal is based
            // on the number computed above.
            StringBuilder pattern = new StringBuilder("#0");

            for (int i = 0; i < decimalPlaces; i++) {
                if (i == 0) {
                    pattern.append(".");
                }
                pattern.append("0");
            }

	  		// Parse the desired locale string into a locale object.  (String
            // assumed to be of form xx_YY.
            Locale newLocale = new Locale(locale.substring(0, 2),
                    locale.substring(3, 5));

	  		// Get a number formatter for the desired locale.  This gets the proper
            // thousand and decimal separators.
            formatter = NumberFormat.getNumberInstance(newLocale);

            // Now apply the new pattern we just created to this number format
            ((DecimalFormat) formatter).applyPattern(pattern.toString());

	  		// Now, having gone through all this work to create a formatter,
            // cache it so we don't have to do the work again.
            cachedFormattersWithoutSeparators.put("DECIMAL" + decimalPlaces + locale, formatter);

        } catch (Exception e) {
	  		// This is likely the result of a bad locale or bad currency code.  We'll simply
            // return the default formatter (for US)
        }

        return formatter;
    }
	  //</Amit - IR EEUE032659500 -05/24/2005 >

    /**
     * Create a formatter based on two conditions. First, the formatter should
     * format numbers based on the user's locale. Second, it should set the
     * number of decimal positions based on the passed in currency code. If no
     * currency code is available (or is passed in as blank), the number of
     * decimal places remains unchanged.
     *
     * @return java.text.NumberFormat - resulting number format instance
     * @param currCd java.lang.String - determines number of decimals places
     * @param locale java.lang.String - determines format for number
     */
    private static NumberFormat getCurrencyFormatter(String currCd, String locale) {
        if (StringFunction.isBlank(currCd)){
            currCd = "NONE";
        }

        NumberFormat formatter = (NumberFormat) cachedFormatters.get(currCd + locale);

        if (formatter != null) {
            return formatter;
        }

        // Create a default number formatter.
        formatter = NumberFormat.getNumberInstance(Locale.US);

        try {
		// Now, create a number format pattern.  This is of the form
            // #,##0.00 where the number of 0's following the decimal is based
            // on the number computed above.
            StringBuilder pattern = new StringBuilder("#,##0");

            if (!currCd.equals("NONE")) {
                String decimalPlaces
                        = ReferenceDataManager.getRefDataMgr().getAdditionalValue(TradePortalConstants.CURRENCY_CODE, currCd);

                // if number of decimal places do not exist, set the default as 2
                if (decimalPlaces == null) {
                    decimalPlaces = "2";
                }
                int decimalCount = Integer.parseInt(decimalPlaces);

                for (int i = 0; i < decimalCount; i++) {
                    if (i == 0) {
                        pattern.append(".");
                    }
                    pattern.append("0");
                }
            } else {
                pattern.append(".########");
            }

		// Parse the desired locale string into a locale object.  (String
            // assumed to be of form xx_YY.
            Locale newLocale = new Locale(locale.substring(0, 2),
                    locale.substring(3, 5));

		// Get a number formatter for the desired locale.  This gets the proper
            // thousand and decimal separators.
            formatter = NumberFormat.getNumberInstance(newLocale);

            // Now apply the new pattern we just created to this number format
            ((DecimalFormat) formatter).applyPattern(pattern.toString());

		// Now, having gone through all this work to create a formatter,
            // cache it so we don't have to do the work again.
            cachedFormatters.put(currCd + locale, formatter);

        } catch (Exception e) {
		// This is likely the result of a bad locale or bad currency code.  We'll simply
            // return the default formatter (for US)
        }

        return formatter;
    }

// <BEGIN - DFEIGE - IR WEUH101744910 - 01/07/2008 - Add new method getCurrencyFormatterWithoutThousandSeparator>
//
// Method getCurrencyFormatterWithoutThousandSeperator is passed the currency (currCd) and locale used to determine
// the number of decimal places and decimal seperator respectively.  The pattern used by the formatter will not
// incorporate a thousand seperator.
//
// @param  | currCd    | java.lang.String       | Determines number of decimals places |
// @param  | locale    | java.lang.String       | Determines decimal seperator         |
// @return | formatter | java.text.NumberFormat | Resulting NumberFormat object        |
    private static NumberFormat getCurrencyFormatterWithoutThousandSeparator(String currCd, String locale) {

        // Attempt to retrieve cached formatter.
        NumberFormat formatter = (NumberFormat) cachedFormattersWithoutSeparators.get(currCd + locale);

        // Assuming formatter is not null the default formatter is returned.
        if (formatter != null) {
            return formatter;
        }

        // Create default formatter here.
        formatter = NumberFormat.getNumberInstance(Locale.US);

        // Using the currency (currCd) determine the number of decimal places and create appropriate pattern to be
        // used for formatting.
        try {
            StringBuilder pattern = new StringBuilder("#0");

            if (!currCd.equals("NONE")) {
                String decimalPlaces
                        = ReferenceDataManager.getRefDataMgr().getAdditionalValue(TradePortalConstants.CURRENCY_CODE, currCd);

                if (decimalPlaces == null) {
                    decimalPlaces = "2";
                }
                int decimalCount = Integer.parseInt(decimalPlaces);

                for (int i = 0; i < decimalCount; i++) {
                    if (i == 0) {
                        pattern.append(".");
                    }
                    pattern.append("0");
                }
            }

            // Create a Locale object by parsing the locale string.
            Locale newLocale = new Locale(locale.substring(0, 2), locale.substring(3, 5));

            // Retrieve a number formatter based on the new locale object.
            formatter = NumberFormat.getNumberInstance(newLocale);

            // Apply the above created pattern to the new number formatter.
            ((DecimalFormat) formatter).applyPattern(pattern.toString());

            // Store formatter in cache.
            cachedFormattersWithoutSeparators.put(currCd + locale, formatter);

        } catch (Exception e) {

        // Catch any exception presumably due to a bad currency (currCd) or bad locale.  Default formatter will be returned.
        }

        return formatter;
    }

// <END - DFEIGE - IR WEUH101744910 - 01/07/2008 - Add new method getCurrencyFormatterWithoutThousandSeparator>
    /**
     * Validates the data entered for an amount to ensure that the amount will
     * fit in the database and that the number of digits after the decimal point
     * is appropriate for the currency. Issues appropriate errors if necessary
     *
     * @param currency - the currency code that corresponds to the amount
     * @param amount - the amount being validated
     * @param alias - the alias to be used in an error message
     * @param errMgr - the error manager on which to issue the error
     */
    public static void validateAmount(String currency, String amount, String alias, ErrorManager errMgr)
            throws AmsException {
       // First, validate the number of digits to the left of the decimal point
        // It is safe to use the '.' as the decimal point
        // By the time this method will validate amounts, they would have all of
        // their localized characters stripped out
        int decimalIndex = amount.indexOf('.');

        // Check for number of digits to the left of the decimal point
        int digitsToLeftOfDecimalPoint;

        if (decimalIndex < 0) // There is no decimal point, so the length is the number of digits
        {
            digitsToLeftOfDecimalPoint = amount.length();
        } else {
            digitsToLeftOfDecimalPoint = decimalIndex;
        }

       // Amounts can only hold up to 15 digits to the left of the decimal point
        // in the database
        if (digitsToLeftOfDecimalPoint > 15) {
            errMgr.issueError(TradePortalConstants.ERR_CAT_1,
                    TradePortalConstants.DECIMAL_PLACE_LEFT,
                    alias,
                    "15");
            return;
        }

       // Second, validate that the number of digits to the right of the
        // decimal point is correct for the currency given
        // Only do this if there is both a currency and an amount
        if (!StringFunction.isBlank(amount)
                && !StringFunction.isBlank(currency)) {
            if (!checkPrecision(currency, amount)) {
                errMgr.issueError(TradePortalConstants.ERR_CAT_1,
                        TradePortalConstants.INVALID_CURRENCY_FORMAT,
                        alias);
            }
        } else {
            // If no currency was entered, we still need to validate that
            // there are no more than 3 digits to the right of the decimal point
            // because that is all that will fit in the database
            // We only need to check if there is actually a decimal point
            if (decimalIndex >= 0) {
                int digitsAfterDecimalPoint = amount.length() - decimalIndex - 1;
                if (digitsAfterDecimalPoint > 3) {
                    errMgr.issueError(TradePortalConstants.ERR_CAT_1,
                            TradePortalConstants.DECIMAL_PLACE,
                            alias,
                            "3");
                }
            }
        }
    }

    /**
     * Verify the precision of the amount, given the currency and amount. The
     * currency code is used to determine the precision (number of decimals)
     * required of the amount. If the amount is in the correct format and has an
     * equal or lower precision than what is required of the currency, this
     * method will return a true. Otherwise, it will return a false. Note that
     * this method currently only checks for precisions after the decimal point.
     * No check is made for precision before the decimal point.
     *
     * Any problems with validation will return false.
     *
     * @return boolean - true: fewer or equal number of decimals; false: greater
     * number of decimals
     * @param currency String - ISO code used to determine number of decimal
     * places
     * @param amount String - amount to format (a string value)
     */
    private static boolean checkPrecision(String currency, String amount) {
        boolean valid = true;

        // If currency and amount are blank, return true
        if (currency.equals("") && amount.equals("")) {
            return true;
        }

        // If only currency is blank, return false since precision can't be checked
        if (currency.equals("") && !amount.equals("")) {
            return false;
        }

        try {
		// get the currency formatter - currency decimals reside in the country locale
            // Get the appropriate number of decimal places from the hashtable mapping
            // of currencies to decimal places
            int decimals;
            try {
                decimals = Integer.parseInt(ReferenceDataManager.getRefDataMgr().getAdditionalValue(TradePortalConstants.CURRENCY_CODE, currency));
            } catch (NumberFormatException e) {
			// If the currency is not a valid currency, there will be no hashtable
                // entry and we get a number format exception.  Set decimals to -1;
                // this causes late logic to return false;
                decimals = -1;
            }

            // parse the amount to get the number of digits to the right side
            int decimalIndex = amount.indexOf('.');

		// check for precision only  if there is a decimal point
            // note that the substring includes the decimal point character
            if (decimalIndex >= 0) {
		   // String decimalValue = amount.substring(decimalIndex + 1);
                // String of zeros are considered ok.
                //Commenting below line as it is not validating xx.000xx as per IR T36000022556 RPasupulati.
                //decimalValue = decimalValue.replace('0', ' ').trim();
			/*IR T36000022556 RPasupulati starts*/
            	//IR T36000043804 
                BigDecimal bgDecimalValue = new BigDecimal(amount);
                String decimalValue = bgDecimalValue.toString();
                decimalValue = decimalValue.substring(decimalIndex + 1);
                /*IR T36000022556 RPasupulati Ends*/
                if (decimalValue.length() <= decimals) {
                    valid = true;
                } else {
                    valid = false;
                }
            }
        } catch (IndexOutOfBoundsException e) {
		// This is likely the result of no string values after the decimal point symbol
            // which  is acceptable for precision
            valid = true;

        } catch (Exception e) {
            e.printStackTrace();
		// This is likely the result of a bad locale.  Since we can't format
            // it properly, return invalid amount.

            valid = false;
        }

        return valid;
    }

    /**
     * Attempts to format a decimal value based on the number of decimal places
     * passed in and locale of the user. It is assumed that any data passed to
     * this method in the 'amount' parameter is formatted without any separators
     * and using a decimal point character (.). This is typically how data is
     * retrieved from the database.
     *
     * @return java.lang.String
     * @param amount java.lang.String
     * @param currencyCode java.lang.String
     * @param loginLocale java.lang.String
     */
    public static String getDecimalDisplayAmount(String amountString, int decimalPlaces, String locale) {
        String result = "";

        try {
            NumberFormat formatter = getDecimalFormatter(decimalPlaces, locale);

            double amount = Double.parseDouble(amountString);

            result = formatter.format(amount);
        } catch (NumberFormatException e) {
            result = amountString;
        } catch (Exception e) {
		// This is likely the result of a bad locale.  Since we can't format
            // it properly, allow Java to make the amount into a string.
            result = amountString;
        }

        return result;
    }

     //<Amit - IR EEUE032659500 -05/24/2005 >
    // New method added for formatting the Amount value to be displayed in the PO line items grid
    /**
     * Attempts to format a decimal value based on the number of decimal places
     * passed in and locale of the user without the thousand separators. It is
     * assumed that any data passed to this method in the 'amount' parameter is
     * formatted without any separators and using a decimal point character (.).
     * This is typically how data is retrieved from the database.
     *
     * @return java.lang.String
     * @param amountString java.lang.String
     * @param decimalPlaces java.lang.String
     * @param locale java.lang.String
     */
    public static String getDecimalAmountWithoutThousandSeparator(String amountString, int decimalPlaces, String locale) {
        String result = "";

        try {
            NumberFormat formatter = getDecimalFormatterWithoutThousandSeparator(decimalPlaces, locale);

            double amount = Double.parseDouble(amountString);

            result = formatter.format(amount);
        } catch (NumberFormatException e) {
            result = amountString;
        } catch (Exception e) {
	 			// This is likely the result of a bad locale.  Since we can't format
            // it properly, allow Java to make the amount into a string.
            result = amountString;
        }

        return result;
    }
	//</Amit - IR EEUE032659500 -05/24/2005 >

    /**
     * Attempts to format an amount based on the currency code and locale of the
     * user. The currency code is used to determine the number of decimal places
     * and the locale determines the thousands separator and the decimal point
     * character. It is assumed that any data passed to this method in the
     * 'amount' parameter is formatted without any separators and using a
     * decimal point character (.). This is typically how data is retrieved from
     * the database.
     *
     * @return java.lang.String
     * @param amount java.lang.String
     * @param currencyCode java.lang.String
     * @param loginLocale java.lang.String
     */
    public static String getDisplayAmount(String amountString, String currCd, String locale) {
        String result = "";

        NumberFormat formatter;

        try {
                // In order to use Java's formatter, the amountString must first be converted
            // to a double.  Double primitives have a mantissa of 51 bits, meaning that they
            // can fully support 15 digits of precision.  The Trade Portal supports numbers
            // up to 18 digits of precision (15 to the left of the decimal point, 3 to the right)
            // Precision is lost for large numbers when they are being formatted, so different
            // processes are used to format depending on the size of the number.

            //check for cross site scripting.
            amountString = StringFunction.xssCharsToHtml(amountString);
            currCd = StringFunction.xssCharsToHtml(currCd);

            // Amount will not lose precision.. do it the easy way
            if ((amountString.length() <= 15) || (amountString.indexOf(".") < 0)) {
                formatter = getCurrencyFormatter(currCd, locale);

                // Use normal way of formatting
                double amount = Double.parseDouble(amountString);

                result = formatter.format(amount);
            } else {
                    // Amount might lose precision... do it the hard way

                    // Get the proper formatter for the locale and determine the decimal
                // separator
                formatter = getDecimalFormatter(0, locale);

                Locale newLocale = new Locale(locale.substring(0, 2),
                        locale.substring(3, 5));

                char decimalSeparatorForLocale = new DecimalFormatSymbols(newLocale).getDecimalSeparator();

                // Split up the amount into the parts before and after the decimal point
                int decimalIndex = amountString.indexOf(".");
                String amountStringBeforeDecimal = amountString.substring(0, decimalIndex);
                String amountStringAfterDecimal = amountString.substring(decimalIndex + 1, amountString.length());

                if ((currCd != null) && (!currCd.equals(""))) {
                       // Format the section after the decimal point for the proper number of
                    // currency decimal places
                    int numberOfCurrencyPlaces = Integer.parseInt(ReferenceDataManager.getRefDataMgr().getAdditionalValue(TradePortalConstants.CURRENCY_CODE, currCd));

                    if (amountStringAfterDecimal.length() < numberOfCurrencyPlaces) {
                        for (int i = 1; i <= (numberOfCurrencyPlaces - amountStringAfterDecimal.length()); i++) {
                            amountStringAfterDecimal = amountStringAfterDecimal + "0";
                        }
                    }
                }

                    // After stripping off the part after the decimal point, the number
                // is small enough that it won't lose precision when formatted... format it using Java's formatter
                double amount = Double.parseDouble(amountStringBeforeDecimal);
                result = formatter.format(amount);

                // Stick all the pieces back together
                return result + decimalSeparatorForLocale + amountStringAfterDecimal;
            }

        } catch (Exception e) {
		// This is likely the result of a bad locale.  Since we can't format
            // it properly, allow Java to make the amount into a string.
            result = amountString;
        }

        return result;
    }

// <BEGIN - DFEIGE - IR WEUH101744910 - 01/07/2008 - Add new method getDisplayAmountWithoutCommaSeperator>
//
// Method getDisplayAmountWithoutCommaSeperator is passed the amountString, currency (currCd) and locale used to
// format the amount string based on the currency and locale and return the resulting amount  Based on the amountString
// length and number of decimal places the method will either use the method getCurrencyFormatterWithoutThousandSeparator
// to format the amount or otherwise manually process the amount by parsing the amountString, formatting and recombining
// to lessen the chance of losing precision due to double conversion.
//
// @param  | amountString | java.lang.String | Unformatted amount                   |
// @param  | currCd       | java.lang.String | Determines number of decimals places |
// @param  | locale       | java.lang.String | Determines decimal seperator         |
// @return | result       | java.lang.String | Resulting formatted amount           |
    public static String getDisplayAmountWithoutCommaSeperator(String amountString, String currCd, String locale) {
        String result = "";

        NumberFormat formatter;

        try {
            // If the amountString is not greater than the double precision limitations, create formatter using method
            // getCurrencyFormatterWithoutThousandSeparator.
            if ((amountString.length() <= 15) || (amountString.indexOf(".") < 0)) {
                formatter = getCurrencyFormatterWithoutThousandSeparator(currCd, locale);

                double amount = Double.parseDouble(amountString);

                result = formatter.format(amount);
            } // If the amountString is greater than the double precision limitations the amountString will need to be
            // manually formatted.
            else {
                // Create formatter that does not use a thousand seperator for locale.
                formatter = getDecimalFormatterWithoutThousandSeparator(0, locale);

                Locale newLocale = new Locale(locale.substring(0, 2), locale.substring(3, 5));

                // Retreive decimal seperator for locale.
                char decimalSeparatorForLocale = new DecimalFormatSymbols(newLocale).getDecimalSeparator();

                // Split amountString into value before and after the decimal.
                int decimalIndex = amountString.indexOf(".");

                String amountStringBeforeDecimal = amountString.substring(0, decimalIndex);

                String amountStringAfterDecimal = amountString.substring(decimalIndex + 1, amountString.length());

                // Based on currency (currCd) add 0's until you have the proper number of decimal places.
                if ((currCd != null) && (!currCd.equals(""))) {
                    int numberOfCurrencyPlaces = Integer.parseInt(ReferenceDataManager.getRefDataMgr().getAdditionalValue(TradePortalConstants.CURRENCY_CODE, currCd));

                    if (amountStringAfterDecimal.length() < numberOfCurrencyPlaces) {
                        for (int i = 1; i <= (numberOfCurrencyPlaces - amountStringAfterDecimal.length()); i++) {
                            amountStringAfterDecimal = amountStringAfterDecimal + "0";
                        }
                    }
                }

                // Format the amountString before the decimal place seperated previously.
                double amount = Double.parseDouble(amountStringBeforeDecimal);

                result = formatter.format(amount);

                // Recombine the results to create final formatted amount.
                return result + decimalSeparatorForLocale + amountStringAfterDecimal;
            }

        } catch (Exception e) {

            // If exception is caught return amountString.
            result = amountString;
        }

        return result;
    }

// <END - DFEIGE - IR WEUH101744910 - 01/07/2008 - Add new method getDisplayAmountWithoutCommaSeperator>
    public static int getDecimalPrecision(String currency) throws AmsException {
        return Integer.parseInt(ReferenceDataManager.getRefDataMgr().getAdditionalValue(TradePortalConstants.CURRENCY_CODE, currency));
    }

    //
    //Need a formatter for displaying any given amount without decimals
    //Rel8.0
    // @param  | amountString | java.lang.String | Unformatted amount                   |    
    public static String getDisplayAmountWithoutDecimal(String amountString) {
        String result = amountString;

        NumberFormat formatter = new DecimalFormat("##");

        try {
            if (StringFunction.isNotBlank(amountString)) {
                result = formatter.format(Double.parseDouble(amountString));
            }
        } catch (Exception e) {

            // If exception is caught return amountString.
            result = amountString;
            e.printStackTrace();
        }

        return result;
    }

    public static String getDisplayAmountWithCurrency(String amountString, String currCd, String locale) {
        String amt = getDisplayAmount(amountString, currCd, locale);
        if (StringFunction.isNotBlank(amt)) {
            return currCd + " " + amt;
        }
        return amt;
    }

}
