package com.ams.tradeportal.common;

public enum TransactionStatus {
	;

	// Transaction statuses
	public static final String AUTHORIZED           = "AUTHORIZED";
	public static final String AUTHORIZE_PENDING    = "AUTHORIZE_PENDING";
	public static final String CANCELLED_BY_BANK    = "CANCELLED_BY_BANK";
	public static final String DELETED              = "DELETED";
	public static final String AUTHORIZE_FAILED     = "AUTHORIZE_FAILED";
	public static final String PROCESSED_BY_BANK    = "PROCESSED_BY_BANK";
	public static final String PARTIALLY_AUTHORIZED = "PARTIALLY_AUTHORIZED";
	public static final String READY_TO_AUTHORIZE   = "READY_TO_AUTHORIZE";
	public static final String STARTED              = "STARTED";
	public static final String REJECTED_BY_BANK     = "REJECTED_BY_BANK";
	public static final String FVD_AUTHORIZED       = "FVD_AUTHORIZED"; //VS CR 609 11/23/10
	public static final String READY_TO_CHECK       = "READY_TO_CHECK"; // Nar CR 821 Rel 8.2.0.0 05/21/13
	public static final String REPAIR               = "REPAIR"; // Nar CR 821 Rel 8.2.0.0 05/21/13
	public static final String REPORTING_CODES_REQUIRED = "REPORTING_CODES_REQUIRED"; // CR 1001 Rel9.4 08/29/15
	//MDB CR-564 R6.1 Begin
	public static final String VERIFIED		             = "VERIFIED";
	public static final String VERIFIED_PENDING_FX          = "VERIFIED_PENDING_FX";
	public static final String FILE_UPLOAD_REJECT           = "FILE_UPLOAD_REJECTED";
	public static final String VERIFIED_AWAITING_APPROVAL   = "VERIFIED_AWAITING_APPROVAL";
	//MDB CR-564 R6.1 End
	//DK CR-640 Rel7.1 BEGINS
	public static final String FX_THRESH_EXCEEDED = "FX_THRESH_EXCEEDED";
	public static final String AUTH_PEND_MARKET_RATE = "AUTH_PEND_MARKET_RATE";
	//DK CR-640 Rel7.1 ENDS
	//IAZ 12/01/05 End
	//CR-596 11/08/2010 Begin
	public static final String TRANSACTION_PROCESSING    = "TRANSACTION_PROCESSING";
	public static final String REJECTED_PAYMENT    = "REJECTED_PAYMENT";
	public static final String PARTIALLY_REJECTED_PAYMENT    = "PARTIALLY_REJECTED_PAYMENT";
	public static final String PARTIAL_PAYMENT    = "PARTIAL_PAYMENT_"; //CR 988 Rel9.2
	//IAZ 12/01/08 Begin
	public final static String TRANS_STATUS_REQ_AUTHENTICTN    = "REQ_AUTHENTICATE";

	// Transaction statuses
	

}
