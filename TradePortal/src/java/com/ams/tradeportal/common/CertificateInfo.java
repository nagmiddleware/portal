package com.ams.tradeportal.common;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class represents a certificate and several methods for getting data from
 * it or determining whether it is valid.
 *
 */
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateParsingException;
import java.security.cert.X509Certificate;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;

import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.util.GMTUtility;

public class CertificateInfo {
private static final Logger LOG = LoggerFactory.getLogger(CertificateInfo.class);

    private X509Certificate myCertificate;
    private java.util.Properties certProperties = null;

    /**
     * Creates a new instance of this class by extracing a X.509 certificate
     * from an Http Request.
     *
     * @param request HttpServletRequest - the Http request
     * @exception java.security.cert.CertificateException Thrown if certificate
     * cannot be extracted/read
     */
    public CertificateInfo(HttpServletRequest request) throws java.security.cert.CertificateException {

        X509Certificate[] certChain = (X509Certificate[]) request.getAttribute("javax.servlet.request.X509Certificate");

        if ((certChain == null) || (certChain.length == 0)) {
            // No certificate is available in the request...
            return;
        }
        try {
            myCertificate = certChain[0];
            extractCertificateSubject(myCertificate);
        } catch (Exception e) {
            throw new CertificateException();
        }
    }

    /**
     * Extracts the subjectDN from a certifcate and then parses that value into
     * properties (private variable certProperties). Various getters allow this
     * information to be returned (e.g., getEmailAddress)
     *
     * @param certificate X509Certificate - the certificate to extract info from
     */
    private void extractCertificateSubject(X509Certificate certificate) throws IOException {

        String subject = certificate.getSubjectDN().getName();

        StringBuilder newSubject = new StringBuilder();

        // First need to convert each ',' into a carriage return.  We can pass a
        // string (actually a byte array) to the Properties class.  However it
        // assumes each property is on a separate line
        for (int i = 0; i < subject.length(); i++) {
            if (subject.charAt(i) == ',') {
                newSubject.append('\n');
            } else {
                newSubject.append(subject.charAt(i));
            }
        }

        ByteArrayInputStream baIn = new ByteArrayInputStream(newSubject.toString().getBytes());

        // Create the properties class and load from the byte array.
        certProperties = new Properties();
        certProperties.load(baIn);

    }

    /**
     * Returns the certificate associated with this CertificateInfo instance.
     *
     * @return java.security.cert.X509Certificate
     */
    public java.security.cert.X509Certificate getCertificate() {
        return myCertificate;
    }

    /**
     * Returns the a piece of data extracted from the certificate's subject with
     * the key given by propertyName. This is similar to the other
     * getOrganization, getOrgUnit, etc. methods but is more generic.
     *
     * @return java.lang.String
     * @param String propertyName - a key value included on a certificate's
     * subjectDN
     */
    public String getCertificateProperty(String propertyName) {
        return certProperties.getProperty(propertyName);
    }

    /**
     * Returns the certificate name extracted from the certificate's subject
     * (CN)
     *
     * @return java.lang.String
     */
    public String getCertName() {
        return certProperties.getProperty("CN");
    }

    /**
     * Returns the country extracted from the certificate's subject (C)
     *
     * @return java.lang.String
     */
    public String getCountry() {
        return certProperties.getProperty("C");
    }

    /**
     * Returns the email address extracted from the certificate's subject
     * (EmailAddress)
     *
     * @return java.lang.String
     */
    public String getEmailAddress() {
        return certProperties.getProperty("EMAIL");
    }

    /**
     * Returns the email address extracted from the certificate's subject
     * (EmailAddress)
     *
     * @return java.lang.String
     */
    public String[] getEmailAddressesFromSubjectAltName() throws CertificateParsingException {
        List emails = new ArrayList();
        if (myCertificate != null && myCertificate.getSubjectAlternativeNames() != null) {
            Collection altNames = myCertificate.getSubjectAlternativeNames();
            for (Iterator i = altNames.iterator(); i.hasNext();) {
                List item = (List) i.next();
                Integer type = (Integer) item.get(0);

				// Type value of 1 indicates the
                // RFC 2459 RFC 822 (email address) Subject Alt Name type.  These are all Strings.
                if (type == 1) {
                    emails.add(item.get(1));
                }
            }
        }

        String str[] = new String[emails.size()];
        emails.toArray(str);
        return str;
    }

    /**
     * Returns the organization extracted from the certificate's subject (O)
     *
     * @return java.lang.String
     */
    public String getOrganization() {
        return certProperties.getProperty("O");
    }

    /**
     * Returns the organizational unit extracted from the certificate's subject
     * (OU)
     *
     * @return java.lang.String
     */
    public String getOrgUnit() {
        return certProperties.getProperty("OU");
    }

    /**
     * Returns the userid extracted from the certificate's subject (UID)
     *
     * @return java.lang.String
     */
    public String getUserid() {
        return certProperties.getProperty("UID");
    }

    /**
     * Tests the expiry date of the certificate against the current GMT time.
     *
     *
     * @return boolean - true: certificate is expired, false: not expired
     */
    public boolean isCertExpired() {
        Date expiry = myCertificate.getNotAfter();

        Date currentTime;

        try {
            currentTime = GMTUtility.getGMTDateTime();
        } catch (AmsException e) {
            return true;
        }

        if (currentTime.after(expiry)) {
            return true;
        }

        return false;
    }

    /**
     * Compares the bank date (a string yyyy-mm-dd hh:mm:ss format) with the
     * current GMT time. If within a certain tolerance, the time is valid.
     * (Tolerance is CertificateSignonDateTolerance from security rules.
     *
     * @return boolean - true: bankDate is within the tolerance, false: bankDate
     * is NOT within the tolerance.
     * @param bankDate java.lang.String
     */
    public static boolean isTimeWithinTolerance(String stringDate) {

        LOG.debug("Time being checked that it's within tolerance: " + stringDate);

        // Convert the given string date into a date.  Use ISO format.
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date bankDate;
        Date currentTime;

        try {
            bankDate = formatter.parse(stringDate);

            // Get the current GMT time and then compute the upper and lower bounds
            currentTime = GMTUtility.getGMTDateTime();
        } catch (ParseException e) {
            LOG.debug("Could not parse date being checked that it's within time tolerance");
            // Date provided cannot be parsed, therefore,
            // it is not within the tolerance.
            return false;
        } catch (AmsException e) {
            LOG.debug("Could not get GMT date and time");
            return false;
        }

        int toleranceAmt = SecurityRules.getCertificateSignonDateTolerance();

        GregorianCalendar upperBound = new GregorianCalendar(TimeZone.getTimeZone("UTC"));
        upperBound.setTime(currentTime);
        upperBound.add(Calendar.MINUTE, toleranceAmt);

        toleranceAmt *= -1;

        GregorianCalendar lowerBound = new GregorianCalendar(TimeZone.getTimeZone("UTC"));
        lowerBound.setTime(currentTime);
        lowerBound.add(Calendar.MINUTE, toleranceAmt);

        // Finally, compare the bank's date with our allowable range to determine
        // if the date is good.
        if (bankDate.after(lowerBound.getTime())
                && bankDate.before(upperBound.getTime())) {
            return true;
        }

        return false;
    }

}
