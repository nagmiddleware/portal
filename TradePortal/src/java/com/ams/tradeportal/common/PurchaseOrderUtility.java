/**
 *
 */
package com.ams.tradeportal.common;
import java.lang.reflect.Field;
import java.rmi.RemoteException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.PropertyResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.busobj.PurchaseOrderDefinition;
import com.ams.tradeportal.busobj.util.InstrumentServices;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.MediatorServices;
import com.amsinc.ecsg.frame.ReferenceDataManager;
import com.amsinc.ecsg.util.StringFunction;

/**

 * methods to hanlde field validations etc. 
 */
public class PurchaseOrderUtility {
private static final Logger LOG = LoggerFactory.getLogger(PurchaseOrderUtility.class);

    private Map fieldPositions;

    private final MediatorServices mediatorServices;

    private PurchaseOrderDefinition purchaseOrderDefinition;

    private int currentLineNum;

    private SimpleDateFormat dateFormat;

    private String validDateFormat = null;

    private static final String AN = "AN";
    private static final String ALPHA = "ALPHA";
    private static final String REFDATA = "REFDATA";
    private static final String DATE = "DATE";
    private static final String CURRENCY = "CURRENCY";
    private static final String IND = "IND";
    private static final String DEC = "DEC";
    private static final String DEC_6_5 = "DEC_6_5";
    private static final String SWIFT = "SWIFT";

    private static final Pattern ALPHAPATTERN = Pattern.compile("[A-Za-z]+");
    private static final Pattern ALPHANUMERICPATTERN = Pattern.compile("[A-Za-z0-9_&@.$%\\-():;` ]+");
    private static final Pattern DEC_6_5_PATTERN = Pattern.compile("[-]?\\d{1,6}(\\.\\d{1,5})?");
    private static final Pattern AMOUNTPATTERN = Pattern.compile("[-]?\\d{1,15}(\\.\\d{1,3})?");
    private static final Pattern NUMPATTERN = Pattern.compile("[0-9]+");
    private static final Pattern DECPATTERN = Pattern.compile("\\d{1,3}(\\.\\d{1,3})?");

    private PropertyResourceBundle bundle = (PropertyResourceBundle) PropertyResourceBundle
            .getBundle(TradePortalConstants.TEXT_BUNDLE);

    public static final Hashtable<String, Integer> fieldSize = new Hashtable<>();

    static {
        // Static initialization of fieldSize[]
        fieldSize.put("purchase_order_num", 35);
        fieldSize.put("purchase_order_type", 3);
        fieldSize.put("issue_date", 10);
        fieldSize.put("latest_shipment_date", 10);
        fieldSize.put("currency", 3);
        fieldSize.put("amount", 19); // 18 for amount digit plus decimal
        fieldSize.put("seller_name", 35);
        fieldSize.put("incoterm", 3);
        fieldSize.put("incoterm_location", 35);
        fieldSize.put("partial_shipment_allowed", 1);
        fieldSize.put("users_def_label", 140);
        fieldSize.put("users_def_value", 140);
        fieldSize.put("line_item_num", 70);
        fieldSize.put("unit_price", 24);
        fieldSize.put("unit_of_measure", 35);
        fieldSize.put("quantity", 24);
        fieldSize.put("quantity_variance_plus", 11);
        fieldSize.put("prod_chars_ud_label", 35);
        fieldSize.put("prod_chars_ud_value", 35);
        fieldSize.put("goods_description", 70);

    }

    public static final Hashtable<String, String> fieldDataType = new Hashtable<>();

    static {
        // Static initialization of fieldSize[]
        fieldDataType.put("purchase_order_num", SWIFT);
        fieldDataType.put("purchase_order_type", "REFDATA");
        fieldDataType.put("issue_date", "DATE");
        fieldDataType.put("latest_shipment_date", "DATE");
        fieldDataType.put("currency", "REFDATA");
        fieldDataType.put("amount", "CURRENCY");
        fieldDataType.put("seller_name", SWIFT);
        fieldDataType.put("incoterm", "REFDATA");
        fieldDataType.put("incoterm_location", SWIFT);
        fieldDataType.put("partial_shipment_allowed", "IND");
        fieldDataType.put("users_def_value", SWIFT);
        fieldDataType.put("line_item_num", SWIFT);
        fieldDataType.put("unit_price", "DEC_6_5");
        fieldDataType.put("unit_of_measure", SWIFT);
        fieldDataType.put("quantity", "DEC_6_5");
        fieldDataType.put("quantity_variance_plus", "DEC");
        fieldDataType.put("prod_chars_ud_value", SWIFT);
        fieldDataType.put("goods_description", SWIFT);

    }

    public PurchaseOrderUtility(PurchaseOrderDefinition purchaseOrderDefinition, MediatorServices mediatorServices) {
        this.purchaseOrderDefinition = purchaseOrderDefinition;
        if (purchaseOrderDefinition != null) {
            try {
                String validDateFormat = purchaseOrderDefinition.getAttribute("date_format");
                dateFormat = new SimpleDateFormat(validDateFormat);
                //ShilpaR IR - RUM041166781
                dateFormat.setLenient(false);
                this.validDateFormat = validDateFormat;
            } catch (RemoteException e) {
                mediatorServices.debug("Error getting valid date format from Purchase Order Definition... "
                        + e.getMessage());
            } catch (AmsException e) {
                mediatorServices.debug("Error getting valid date format from Purchase Order Definition... "
                        + e.getMessage());
            }

        }

        this.mediatorServices = mediatorServices;
        setFieldPositions();
    }

    public SimpleDateFormat getDateFormat() {
        return dateFormat;
    }

    public int getCurrentLineNum() {
        return currentLineNum;
    }

    public void setCurrentLineNum(int currentLineNum) {
        this.currentLineNum = currentLineNum;
    }

    public Map getFieldPositions() {
        return fieldPositions;
    }

    public int getFieldPosition(String fieldName) {
        return fieldPositions.get(fieldName) != null ? (Integer) fieldPositions
                .get(fieldName) : -1;
    }

    /**
     * Sets the passed in String Array values to the passed in field names on
     * the passed Object 'obj' in the fieldNames list after validating for size
     * and format. If size or format is not as per definition then logs error
     * and set the passed Object 'obj' as invalid. Returns a Map of fields.
     *
     * @param fieldNamesList
     * @param record
     * @param lineNum
     * @param obj
     * @return
     */
    public Map<String, Object> setFieldValues(List<String> fieldNamesList, String[] record, int lineNum, Object obj) {
        Map fields = new HashMap();
        Field field = null;
        for (String fieldName : fieldNamesList) {
            try {
                int position = getFieldPosition(fieldName);
                if (position != -1) {
                    String tempFieldName = getFieldName(fieldName);
                    String fieldFormat = fieldDataType.get(tempFieldName);
                    int fieldsSize = fieldSize.get(tempFieldName);
                    if ("REFDATA".equals(fieldFormat) || "IND".equals(fieldFormat)) {
                        if (StringFunction.isNotBlank(record[position])) {
                            record[position] = record[position].toUpperCase();
                        }
                    }
                    if (validateFieldSizeAndForamt(fieldName, record[position], lineNum, fieldFormat, fieldsSize)) {
                        //Ravindra B - 18th Sep 2012 - Rel810 IR-KAUM091833385 - Start
                        //Added trim to avoid leading spaces issue
                        fields.put(fieldName, record[position].trim());
                        //Ravindra B - 18th Sep 2012 - Rel810 IR-KAUM091833385 - End
                    } else {
                        setObjectInvalid(obj);
                    }
                } else {
                    logMissingFieldError(fieldName);
                    logDebug("Error logging erorr ...");
                    setObjectInvalid(obj);
                }
            } catch (AmsException e) {
                logInvalidFieldFormatError(fieldName);
                logDebug("Error logging erorr ..." + e.getStackTrace());
                setObjectInvalid(obj);
            }
        }
        return fields;
    }

    private void setObjectInvalid(Object obj) {

        if (obj instanceof PurchaseOrderObject) {
            ((PurchaseOrderObject) obj).setValid(false);
        } else if (obj instanceof PurchaseOrderLineItemObject) {
            ((PurchaseOrderLineItemObject) obj).setValid(false);
        }

    }

    /**
     * Adds error to the mediator services. Used when only field name needs to
     * be set on the message.
     *
     * @param fieldName
     * @param errorCode
     */
    public void addError(String fieldName, String errorCode) {

        String[] substitutionValues = {"", ""};
        try {
            try {
                if (resolveUserDefinedFieldName(fieldName).equals(fieldName)) {
                    substitutionValues[0] = bundle.getString("PurchaseOrderUploadRequest." + fieldName);
                } else {
                    substitutionValues[0] = resolveUserDefinedFieldName(fieldName);
                }
            } catch (MissingResourceException e) {
                logDebug("Error logging erorr ..." + e.getStackTrace());
                substitutionValues[0] = "PurchaseOrderUploadRequest." + fieldName;
            }
            substitutionValues[1] = String.valueOf(currentLineNum);
            mediatorServices.getErrorManager().issueError(
                    TradePortalConstants.ERR_CAT_1,
                    errorCode, substitutionValues,
                    null);
        } catch (AmsException e) {
            logDebug("Error logging erorr ..." + e.getStackTrace());
        }
    }

    /**
     * Adds error to the mediator services. Used when field name and value needs
     * to be set on the message.
     *
     * @param fieldName
     * @param fieldValue
     * @param errorCode
     */
    public void addError(String fieldName, String fieldValue, String errorCode) {

        String[] substitutionValues = {"", ""};
        try {
            try {
                fieldName = bundle.getString("PurchaseOrderUploadRequest." + fieldName);
                substitutionValues[0] = fieldValue;
            } catch (MissingResourceException e) {
                logDebug("Error logging erorr ..." + e.getStackTrace());
                substitutionValues[0] = "PurchaseOrderUploadRequest." + fieldName;
            }
            substitutionValues[1] = String.valueOf(currentLineNum);
            mediatorServices.getErrorManager().issueError(
                    TradePortalConstants.ERR_CAT_1,
                    errorCode, substitutionValues,
                    null);
        } catch (AmsException e) {
            logDebug("Error logging erorr ..." + e.getStackTrace());
        }
    }

    /**
     * Adds error to the mediator services. Used when only line number needs to
     * be set on the error.
     *
     * @param errorCode
     */
    public void addError(String errorCode) {

        String[] substitutionValues = {""};
        try {
            substitutionValues[0] = String.valueOf(currentLineNum);
            mediatorServices.getErrorManager().issueError(
                    TradePortalConstants.ERR_CAT_1,
                    errorCode, substitutionValues,
                    null);
        } catch (AmsException e) {
            logDebug("Error logging erorr ..." + e.getStackTrace());
        }
    }

    private void logMissingFieldError(String fieldName) {
        addError(fieldName, TradePortalConstants.PO_FIELD_MISSING);

    }

    private void logInvalidFieldFormatError(String fieldName) {
        addError(fieldName, TradePortalConstants.INVALID_PO_FIELD_FORMAT);
    }

    private void logInvalidFieldValueSize(String fieldName) {
        String[] substitutionValues = {"", "", ""};
        try {
            try {
                if (resolveUserDefinedFieldName(fieldName).equals(fieldName)) {
                    substitutionValues[0] = bundle.getString("PurchaseOrderUploadRequest." + fieldName);
                } else {
                    substitutionValues[0] = resolveUserDefinedFieldName(fieldName);
                }
            } catch (MissingResourceException e) {
                logDebug("Error logging erorr ..." + e.getStackTrace());
                substitutionValues[0] = "PurchaseOrderUploadRequest." + fieldName;
            }
            substitutionValues[1] = String.valueOf(currentLineNum);
            substitutionValues[2] = String.valueOf(fieldSize.get(getFieldName(fieldName)));
            if ("amount".equals(fieldName)) { // if field name amount, then log error for actual amount length by removing decimal
                substitutionValues[2] = String.valueOf(Integer.parseInt(substitutionValues[2]) - 1);
            }
            mediatorServices.getErrorManager().issueError(
                    TradePortalConstants.ERR_CAT_1,
                    TradePortalConstants.ERROR_FIELD_VALUE_EXCEEDS_MAX_LEN, substitutionValues,
                    null);
        } catch (AmsException e) {
            logDebug("Error logging erorr ..." + e.getStackTrace());
        }

    }

    private String resolveUserDefinedFieldName(String fn) {

        String labelValue = fn;
        try {
            if (fn.contains("prod_chars")) {
                labelValue = purchaseOrderDefinition.getAttribute(fn);
            } else if (fn.contains("seller_user") || fn.contains("buyer_user")) {
                labelValue = purchaseOrderDefinition.getAttribute(fn.replace("user", "users"));
            }
        } catch (Exception e) {
        }

        return labelValue;

    }

    private String getFieldName(String fieldName) {
        String tempFieldName = fieldName;

        if (fieldName.startsWith("prod") && fieldName.endsWith("_label")) {
            tempFieldName = "prod_chars_ud_label";
        } else if (fieldName.startsWith("prod") && fieldName.endsWith("_value")) {
            tempFieldName = "prod_chars_ud_value";
        } else if (fieldName.endsWith("_label")) {
            tempFieldName = "users_def_label";
        } else if (fieldName.endsWith("_value")) {
            tempFieldName = "users_def_value";
        }

        return tempFieldName;
    }

    private void logDebug(String debugMessage) {
        mediatorServices.debug(debugMessage);
    }

    /**
     * Returns a list of fields defined on the PurchaseOrderDefinition.
     *
     * @param in
     * @return
     */
    public List<String> getDefinedFieldNames(String in) {

        List<String> fieldsNamesList = new ArrayList<>();
        if (purchaseOrderDefinition != null) {
            String attributeName = "po_data_field";
            if ("lineItem".equals(in)) {
                attributeName = "po_line_item_field";
            }

            String currentFieldName = "";
            boolean hasMoreFields = true;
            int index = 1;
            while (hasMoreFields) {
                try {
                    currentFieldName = purchaseOrderDefinition
                            .getAttribute(attributeName + index);
                } catch (Exception e) {
                    if ("po_data_field".equals(attributeName)) {
                        attributeName = "po_summary_field";
                        index = 1;
                        continue;
                    } else {
                        hasMoreFields = false;
                        continue;
                    }
                }

                if ((currentFieldName != null)
                        && (!currentFieldName.equals(""))) {
                    fieldsNamesList.add(currentFieldName);
                } else {
                    if ("po_line_item_field".equals(attributeName)) {
                        hasMoreFields = false;
                    }
                }
                index++;
            }
        }

        return fieldsNamesList;
    }

    /**
     * Returns a map with each fields position on the record as defined in
     * PurchaseOrderDefinition.
     *
     * @return
     */
    protected Map setFieldPositions() {
        fieldPositions = new HashMap();
        if (purchaseOrderDefinition != null) {
            boolean hasMoreFields = true;
            String currentFieldName = "";
            int index = 0;
            String attributeName = "po_data_field";

            while (hasMoreFields) {
                try {
                    currentFieldName = purchaseOrderDefinition
                            .getAttribute(attributeName + (index + 1));
                } catch (Exception e) {
                    if ("po_data_field".equals(attributeName)) {
                        attributeName = "po_summary_field";
                        index = 0;
                        continue;
                    } else if ("po_summary_field".equals(attributeName)) {
                        attributeName = "po_line_item_field";
                        index = 0;
                        continue;
                    } else if ("po_line_item_field".equals(attributeName)) {
                        hasMoreFields = false;
                        break;
                    }
                }

                if ((currentFieldName != null)
                        && (!currentFieldName.equals(""))) {
                    fieldPositions.put(currentFieldName, fieldPositions.size());
                } else {
                    if ("po_summary_field".equals(attributeName)) {
                        attributeName = "po_line_item_field";
                        index = 0;
                        continue;
                    } else if ("po_line_item_field".equals(attributeName)) {
                        hasMoreFields = false;
                    }
                }
                index++;
            }

        }
        return fieldPositions;
    }

    /**
     * Validates Size and Format of fields and creates error message if wrong
     *
     * @exception com.amsinc.ecsg.frame.AmsException
     * @return boolean - indicates whether or not a valid Invoice Data file
     * (true - valid false - invalid)
     */
    private boolean validateFieldSizeAndForamt(String fieldName,
            String value, int lineNum, String format, int fieldSize)
            throws AmsException {
        if (StringFunction.isNotBlank(value)) {
            if (value.length() > fieldSize) {
                logInvalidFieldValueSize(fieldName);
                return false;
            }
        }
        if (!StringFunction.isBlank(format)) {

            if (format.equals(AN)) {
                Matcher m = ALPHANUMERICPATTERN.matcher(value);
                if (!m.matches()) {
                    logInvalidFieldFormatError(fieldName);
                    return false;
                }
            } else if (format.equals(DEC)) {
                Matcher m = DECPATTERN.matcher(value);
                if (!m.matches()) {
                    logInvalidFieldFormatError(fieldName);
                    return false;
                }
            } else if (format.equals(ALPHA)) {
                Matcher m = ALPHAPATTERN.matcher(value);
                if (!m.matches()) {
                    logInvalidFieldFormatError(fieldName);
                    return false;
                }
            } else if (format.equals(DATE)) {
                try {
                    // just validate the format, do not save the changed format
                    dateFormat.setLenient(false);
                    Date valueDate = dateFormat.parse(value.trim());
                    value = valueDate.toString();
                } catch (ParseException e) {
                    logInvalidFieldFormatError(fieldName);
                    e.printStackTrace();
                    return false;
                }
            } else if (format.equals(CURRENCY)) {
                Matcher m = AMOUNTPATTERN.matcher(value.trim());
                if (!m.matches()) {
                    logInvalidFieldFormatError(fieldName);
                    return false;
                }
            } else if (format.equals(DEC_6_5)) {
                Matcher m = DEC_6_5_PATTERN.matcher(value.trim());
                if (!m.matches()) {
                    logInvalidFieldFormatError(fieldName);
                    return false;
                }
            } else if (format.equals(REFDATA)) {
                String tableType = "";
                if ("currency".equals(fieldName)) {
                    tableType = "CURRENCY_CODE";
                } else {
                    tableType = fieldName.toUpperCase();
                }

                if (!ReferenceDataManager.getRefDataMgr().checkCode(tableType, value, mediatorServices.getCSDB().getLocaleName())) {
                    logInvalidFieldFormatError(fieldName);
                    return false;
                }
            } else if (format.equals(IND)) {
                if (!(TradePortalConstants.INDICATOR_YES.equals(value) | TradePortalConstants.INDICATOR_NO.equals(value))) {
                    logInvalidFieldFormatError(fieldName);
                    return false;
                }
            } else if (format.equals(SWIFT)) {
                // DK IR-SAUM050359531 Rel8.1.1 09/18/2012 Begins
                if (fieldName.equals("goods_description")) {
                    boolean invGoodsDescription = validateGoodsDescriptionFormat(value);
                    if (!invGoodsDescription) {
                        logInvalidFieldFormatError(fieldName);
                        return false;
                    }
                } else {
                    String invChars = InstrumentServices.checkAutoLCForInvalidSwiftCharacters(value);
                    if (invChars != null) {
                        logInvalidFieldFormatError(fieldName);
                        return false;
                    }
                }
                // DK IR-SAUM050359531 Rel8.1.1 09/18/2012 Ends
            }
        }

        return true;
    }

 // DK IR-SAUM050359531 Rel8.1.1 09/18/2012 
    private boolean validateGoodsDescriptionFormat(String value){
    	Pattern ALPHANUMERICPATTERN = Pattern.compile("[A-Za-z0-9 /-?().,'+\n\r%��������������������������<>&]+");
		Pattern SPCHARPATTERN = Pattern.compile("<>");
		Matcher m = SPCHARPATTERN.matcher(value);    	
		if(m.find()){			
			return false;
		}
		Matcher mSWIFT = ALPHANUMERICPATTERN.matcher(value);		
		return mSWIFT.matches();
	}
}
