package com.ams.tradeportal.common;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by IntelliJ IDEA.
 * User: rizwan.kazi
 * Date: 3/6/12
 * Time: 9:50 AM
 * To change this template use File | Settings | File Templates.
 */
public class POField {
private static final Logger LOG = LoggerFactory.getLogger(POField.class);
    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getFieldValue() {
        return fieldValue;
    }

    public void setFieldValue(String fieldValue) {
        this.fieldValue = fieldValue;
    }

    public String getDataType() {
        return dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    private String fieldName;
    private String fieldValue;
    private String dataType;
    public POField(){

    }

    public POField(String fieldName, String fieldValue, String dataType){
        this.fieldName = fieldName;
        this.fieldValue = fieldValue;
        this.dataType = dataType;
    }
}
