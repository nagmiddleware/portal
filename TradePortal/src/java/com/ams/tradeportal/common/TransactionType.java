package com.ams.tradeportal.common;

public enum TransactionType {
	;

	public static final String ISSUE          = "ISS";
	public static final String AMEND_TRANSFER = "ATR";
	public static final String TRANSFER       = "TRN";
	public static final String RELEASE        = "REL";
	public static final String TRACER         = "TRC";
	public static final String DISCREPANCY    = "DCR";
	public static final String ASSIGNMENT     = "ASN";
	public static final String ADVISE         = "ADV";
	public static final String REVOLVE        = "RVV";
	public static final String EXPIRE         = "EXP";
	public static final String REACTIVATE     = "REA";
	public static final String REDUCE       = "RED";
	public static final String PAYMENT        = "PAY";
	public static final String REVALUE        = "RVL";
	public static final String SIM            = "SIM";
	public static final String SIR            = "SIR";
	public static final String TRANSFER_PAY   = "PYT";
	public static final String DEACTIVATE     = "DEA";
	public static final String PCE            = "PCE";
	public static final String PCS            = "PCS";
	public static final String NAC            = "NAC";
	public static final String CREATE_USANCE  = "CUS";
	public static final String LIQUIDATE      = "LIQ";
	public static final String EXTEND       = "EXT";
	public static final String AMEND          = "AMD";
	// Transaction types
	public static final String ADJUSTMENT     = "ADJ";
	public static final String COLLECTION     = "COL";
	public static final String DOC_EXAM       = "EXM";
	public static final String BUY        = "BUY";
	public static final String CHANGE       = "CHG";
	public static final String DEAL_CHANGE    = "DCH";
	public static final String CREATE_LOI     = "CRE";
	public static final String OPEN           = "OPN";
	public static final String PRE_ADVISE     = "PRE";
	
	public static final String APPROVAL_TO_PAY_RESPONSE = "APR";  //CR 375-D ATP-Response 07/26/2007 
	//  Pratiksha CR-434 09/24/2008 ADD BEGIN
	public static final String RECEIVABLES_MATCH_RESPONSE 	= "RMT";
	public static final String RECEIVABLES_APPROVE_DISCOUNT	= "RAD";
	public static final String RECEIVABLES_CLOSE_INVOICE	= "RCI";
	public static final String RECEIVABLES_FINANCE_INVOICE	= "RFI";
	// Pratiksha CR-434 09/24/2008 ADD END
	public static final String RECEIVABLES_DISPUTE_INVOICE	= "RDI"; // CR 451 CM 10/24/2008

}
