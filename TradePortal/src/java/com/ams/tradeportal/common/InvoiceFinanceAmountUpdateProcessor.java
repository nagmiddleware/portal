package com.ams.tradeportal.common;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.RemoveException;

import com.ams.tradeportal.busobj.CorporateOrganization;
import com.ams.tradeportal.busobj.Invoice;
import com.ams.tradeportal.busobj.InvoiceFinanceAmountQueue;
import com.ams.tradeportal.busobj.InvoiceHistory;
import com.ams.tradeportal.busobj.InvoicesSummaryData;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.ClientServerDataBridge;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.frame.EJBObjectFactory;
import com.amsinc.ecsg.frame.IssuedError;
import com.amsinc.ecsg.frame.Logger;
import com.amsinc.ecsg.util.DateTimeUtility;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.StringFunction;
import com.amsinc.ecsg.web.BeanManager;

public class InvoiceFinanceAmountUpdateProcessor {
private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(InvoiceFinanceAmountUpdateProcessor.class);

    private final String ejbServerLocation;
    private final ClientServerDataBridge csdb;
    private final long invoiceFinanceAmountQueueOid;

    private String process_id = null;
    String creditAccountBankCountryCode = null;
    String debitAccountBankCountryCode = null;
    private long interestDiscRateGrpOid = 0;

    private InvoiceFinanceAmountQueue invoiceFinanceAmountQueue = null;
    protected static final String loggerCategory = TradePortalConstants
            .getPropertyValue("AgentConfiguration", "loggerServerName",
                    "NoLogCategory");

    protected boolean _isInvoiceFile = false;

    public InvoiceFinanceAmountUpdateProcessor() {
        this.invoiceFinanceAmountQueueOid = 0;
        this.ejbServerLocation = "";
        this.csdb = new ClientServerDataBridge();
        this.process_id = "";
    }

    public InvoiceFinanceAmountUpdateProcessor(String process_id, long oidLong,
            String ejbServerLocation, ClientServerDataBridge csdb) {
        this.ejbServerLocation = ejbServerLocation;
        this.csdb = csdb;
        this.invoiceFinanceAmountQueueOid = oidLong;
        this.process_id = process_id;
    }

    /**
     * Processes uploaded Payment file saved in temporary folder.
     *
     * @return boolean true - success false = failure
     * @throws AmsException
     * @throws RemoveException
     * @throws IOException
     */
    public boolean processInterestDiscountUpdate() throws AmsException {

        DocumentHandler invoicesListDoc = null;
        InvoicesSummaryData invoiceSummaryData = null;
        String invoiceOid = null;
       
        try {
            invoiceFinanceAmountQueue = (InvoiceFinanceAmountQueue) EJBObjectFactory
                    .createClientEJB(ejbServerLocation,
                            "InvoiceFinanceAmountQueue",
                            invoiceFinanceAmountQueueOid);
            interestDiscRateGrpOid = 0;
            String interestDiscRateGrpOidStr = invoiceFinanceAmountQueue.getAttribute("interest_disc_rate_grp_oid");
            if (!StringFunction.isBlank(interestDiscRateGrpOidStr)) {
                interestDiscRateGrpOid = invoiceFinanceAmountQueue.getAttributeLong("interest_disc_rate_grp_oid");
            }

            if (StringFunction.isBlank(interestDiscRateGrpOidStr)) {
                return true;
            }

            invoiceFinanceAmountQueue.setClientServerDataBridge(csdb);
                        // W Zhu 5/7/2013 T36000016798 do not set status in processor.  Set it in agent after processing both upload invoice and supplier portal invoices.
           String sqlQuery = "select upload_invoice_oid from invoices_summary_data isd, corporate_org co, interest_discount_rate_group idrg"
                    + " where isd.a_corp_org_oid = co.organization_oid and isd.invoice_classification = ?  and co.interest_disc_rate_group = idrg.group_id"
                    + " and idrg.interest_disc_rate_group_oid = ? and isd.invoice_status in (?,?,?,?,?)";

            List<Object> sqlParams = new ArrayList<>();
            sqlParams.add(TradePortalConstants.INVOICE_TYPE_REC_MGMT);
            sqlParams.add(interestDiscRateGrpOid);
            sqlParams.add(TradePortalConstants.UPLOAD_INV_STATUS_AVL_FOR_FIN);
            sqlParams.add(TradePortalConstants.UPLOAD_INV_STATUS_AVL_FOR_AUTH);
            sqlParams.add(TradePortalConstants.UPLOAD_INV_STATUS_FIN_PAR_AUTH);
            sqlParams.add(TradePortalConstants.UPLOAD_INV_STATUS_FIN_AUTH_FAILED);
            sqlParams.add(TradePortalConstants.UPLOAD_INV_STATUS_PEND_ACT_UNABLE_TO_FIN);

            //BSL IR NCUM041858986 04/18/2012 END
            invoicesListDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery, true, sqlParams);

            // W Zhu 5/7/2013 T36000016798 do not set status to fail.  Return true.  There may be only supplier portal invoices, and no be upload invoice to process.
            if (invoicesListDoc == null) {
            	return true;
            }

            //BSL IR NLUM042739974 04/28/2012 Rel 8.0 BEGIN
            // Create a cache with corp org OID as the key and Upload System 
            // User OID as the value
            Map<String, String> systemUserCache = new HashMap<>();
            //BSL IR NLUM042739974 04/28/2012 Rel 8.0 END
            List<DocumentHandler> invoiceOidList = invoicesListDoc
                    .getFragmentsList("/ResultSetRecord");
            BeanManager beanMgr = new BeanManager();
            beanMgr.setServerLocation(ejbServerLocation);
           for (DocumentHandler list :invoiceOidList) {

                invoiceOid = list.getAttribute("UPLOAD_INVOICE_OID");
                invoiceSummaryData = (InvoicesSummaryData) EJBObjectFactory.createClientEJB(ejbServerLocation,
                        "InvoicesSummaryData", Long.parseLong(invoiceOid));
                String invoiceStatus = invoiceSummaryData.getAttribute("invoice_status");

                                // W Zhu 22 Oct 2014 T36000032768 Check invoice status again, which could have changed
                // after the list is queried since the list may contain a large number of invoices.
                if (!TradePortalConstants.UPLOAD_INV_STATUS_AVL_FOR_FIN.equals(invoiceStatus)
                        && !TradePortalConstants.UPLOAD_INV_STATUS_AVL_FOR_AUTH.equals(invoiceStatus)
                        && !TradePortalConstants.UPLOAD_INV_STATUS_FIN_PAR_AUTH.equals(invoiceStatus)
                        && !TradePortalConstants.UPLOAD_INV_STATUS_FIN_AUTH_FAILED.equals(invoiceStatus)
                        && !TradePortalConstants.UPLOAD_INV_STATUS_PEND_ACT_UNABLE_TO_FIN.equals(invoiceStatus)) {
                    invoiceSummaryData.remove();
                    continue;
                }

                CorporateOrganization corpOrg = null;
                String corpOrgOid = invoiceSummaryData.getAttribute("corp_org_oid");
                String systemUserOid = systemUserCache.get(corpOrgOid);
                if (systemUserOid == null) {
                    // Retrieve the value from the database
                    corpOrg = (CorporateOrganization) EJBObjectFactory.createClientEJB(ejbServerLocation,
                            "CorporateOrganization", Long.parseLong(corpOrgOid));
                    systemUserOid = corpOrg.getAttribute("upload_system_user_oid");
                    if (systemUserOid == null) {
                        systemUserOid = "";
                    }

                    // Store the value in the cache
                    systemUserCache.put(corpOrgOid, systemUserOid);
                }

                String newStatus;
				 try {
                    invoiceSummaryData.calculateInterestDiscount(beanMgr);
                     newStatus = TradePortalConstants.UPLOAD_INV_STATUS_AVL_FOR_FIN;
                    } catch (AmsException e) {
                    IssuedError is = e.getIssuedError();
                    if (is != null) {
                        String errorCode = is.getErrorCode();
                        if (TradePortalConstants.ERR_UPLOAD_INV_INSTR_TYPE_MISSING.equals(errorCode)) {
                             newStatus = TradePortalConstants.UPLOAD_INV_STATUS_PEND_ACT_NO_INSTR_TYPE;
                        } else if (TradePortalConstants.ERR_UPLOAD_INV_TENOR_DAYS_LESS.equals(errorCode)) {
                            logError("InvoiceFinanceAmountUpdateProcessor.processInterestDiscountUpdate: "
                                    + "Number of Tenor Days is less than number of days before due date/ payment date for invoice: "
                                    + invoiceOid);

                            newStatus = TradePortalConstants.UPLOAD_INV_STATUS_PEND_ACT_UNABLE_TO_FIN;
                        } else if (TradePortalConstants.ERR_UPLOAD_INV_DUE_PMT_DAYS_DIFF.equals(errorCode)) {
                            logError("InvoiceFinanceAmountUpdateProcessor.processInterestDiscountUpdate: "
                                    + "Number of days between due date and payment date is too large for invoice: "
                                    + invoiceOid);

                             newStatus = TradePortalConstants.UPLOAD_INV_STATUS_PEND_ACT_UNABLE_TO_FIN;
                        } 
                        else if (TradePortalConstants.ERR_UPLOAD_INV_PCT_VAL_TO_FIN_ZERO.equals(errorCode)) {
                            logError("InvoiceFinanceAmountUpdateProcessor.processInterestDiscountUpdate: "
                                    + "Invoice finance % = 0 for invoice: "
                                    + invoiceOid);

                             newStatus = TradePortalConstants.UPLOAD_INV_STATUS_PEND_ACT_UNABLE_TO_FIN;
                        } 
                        else {
                            logError("Error occured processing Interest Discount updates...", e);
                            return false;
                        }
                    } else {
                        logError("Error occured processing Interest Discount updates...", e);
                        return false;
                    }
                }
				// invoiceSummaryData.setAttribute("invoice_status", newStatus);do not change status as per IR 41042

                invoiceSummaryData.setAttribute("action",  TradePortalConstants.UPLOAD_INV_ACTION_INTREST_DIS_UPDATE);
                invoiceSummaryData.setAttribute("user_oid",systemUserOid);

                invoiceSummaryData.save();
				
                invoiceSummaryData.remove();
                if (corpOrg != null) {
                    corpOrg.remove();
                }
                
            }

        } catch (Exception e) {
            logError("Error occured processing Interest Discount updates...", e);
            throw new AmsException(e);
        }

        return true;
    }

    public boolean processSupplierPortalDiscountUpdate() throws AmsException,
            RemoveException, IOException {

        DocumentHandler invoicesListDoc = null;
        Invoice invoice = null;
        String invoiceOid = null;
        int count = 0;

        invoiceFinanceAmountQueue = (InvoiceFinanceAmountQueue) EJBObjectFactory
                .createClientEJB(ejbServerLocation,
                        "InvoiceFinanceAmountQueue",
                        invoiceFinanceAmountQueueOid);
        interestDiscRateGrpOid = 0;
        String interestDiscRateGrpOidStr = invoiceFinanceAmountQueue.getAttribute("interest_disc_rate_grp_oid");
        if (!StringFunction.isBlank(interestDiscRateGrpOidStr)) {
            interestDiscRateGrpOid = invoiceFinanceAmountQueue.getAttributeLong("interest_disc_rate_grp_oid");
        }
        long corporateOrgOid = 0;
        String corporateOrgOidStr = invoiceFinanceAmountQueue.getAttribute("corp_org_oid");
        if (!StringFunction.isBlank(corporateOrgOidStr)) {
            corporateOrgOid = invoiceFinanceAmountQueue.getAttributeLong("corp_org_oid");
        }
        String marginRuleListType = invoiceFinanceAmountQueue.getAttribute("margin_rule_list_type");
        invoiceFinanceAmountQueue.setClientServerDataBridge(csdb);
        List<Object> sqlParams = new ArrayList<>();

        StringBuilder sqlQuery = new StringBuilder();
        if (interestDiscRateGrpOid != 0) {
            sqlQuery.append("select invoice_oid from invoice isd, corporate_org co, interest_discount_rate_group idrg");
            sqlQuery.append(" where isd.a_corp_org_oid = co.organization_oid");
            sqlQuery.append(" and co.interest_disc_rate_group = idrg.group_id");
            sqlQuery.append(" and idrg.interest_disc_rate_group_oid = ? ");
            sqlParams.add(interestDiscRateGrpOid);
        } else if (corporateOrgOid != 0 && "SPMarginRuleList".equals(marginRuleListType)) {
            sqlQuery.append("select invoice_oid from invoice isd");
            sqlQuery.append(" where isd.a_corp_org_oid = ? ");
            sqlParams.add(corporateOrgOid);
        } else {
            return true;
        }
        sqlQuery.append(" and isd.supplier_portal_invoice_status in (?,?,?,?,?,?)");

        sqlParams.add(TradePortalConstants.SP_STATUS_BUYER_APPROVED);
        sqlParams.add(TradePortalConstants.SP_STATUS_REJECTED_BY_BANK);
        sqlParams.add(TradePortalConstants.SP_STATUS_FVD_ASSIGNED);
        sqlParams.add(TradePortalConstants.SP_STATUS_OFFER_ACCEPTED);
        sqlParams.add(TradePortalConstants.SP_STATUS_PARTIALLY_AUTHORIZED);
        sqlParams.add(TradePortalConstants.SP_STATUS_FVD_AUTHORIZED);

        invoicesListDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery.toString(), true, sqlParams);
        if (invoicesListDoc == null) {
                            // W Zhu 5/7/2013 T36000016798 do not set status to failed.  There may not be supplier portal invoices to process.
            //invoiceFinanceAmountQueue.setAttribute("status",
            //                        TradePortalConstants.INV_FIN_AMNT_STAT_RECALC_FAILED);
            //                invoiceFinanceAmountQueue.save();
            //return false;
            return true;
        }
        Map<String, String> systemUserCache = new HashMap<>();

        List<DocumentHandler>  invoiceOidList = invoicesListDoc
                .getFragmentsList("/ResultSetRecord");
        count = invoiceOidList.size();
        for (DocumentHandler list :invoiceOidList) {

            invoiceOid =list.getAttribute("INVOICE_OID");
            invoice = (Invoice) EJBObjectFactory.createClientEJB(ejbServerLocation,
                    "Invoice", Long.parseLong(invoiceOid));
            invoice.calculateInvoiceOfferNetAmountOffered();
            if (!StringFunction.isBlank(invoice.getAttribute("old_net_amount_offered"))) {
                invoice.updateAssociatedInvoiceOfferGroup();
            }
            String newStatus = invoice.getAttribute("supplier_portal_invoice_status");
            invoice.save();
			//IR T36000018821 Start- Create an entry in History table

            CorporateOrganization corpOrg = null;
            String corpOrgOid = invoice.getAttribute("corp_org_oid");
            String systemUserOid = systemUserCache.get(corpOrgOid);
            if (systemUserOid == null) {
                // Retrieve the value from the database
                corpOrg = (CorporateOrganization) EJBObjectFactory.createClientEJB(ejbServerLocation,
                        "CorporateOrganization", Long.parseLong(corpOrgOid));
                systemUserOid = corpOrg.getAttribute("upload_system_user_oid");
                if (systemUserOid == null) {
                    systemUserOid = "";
                }

                // Store the value in the cache
                systemUserCache.put(corpOrgOid, systemUserOid);
            }

            InvoiceHistory invHistory = (InvoiceHistory) EJBObjectFactory.createClientEJB(ejbServerLocation,
                    "InvoiceHistory");
            invHistory.newObject();
            invHistory.setAttribute("action_datetime", DateTimeUtility.getGMTDateTime(true, false));
            invHistory.setAttribute("action", TradePortalConstants.UPLOAD_INV_ACTION_INTREST_DIS_UPDATE);
            invHistory.setAttribute("invoice_status", newStatus);
            invHistory.setAttribute("invoice_status", null);
            invHistory.setAttribute("upload_invoice_oid", invoiceOid);
            invHistory.setAttribute("user_oid", systemUserOid);
            int ret = invHistory.save(false);
            if (ret != 1) {
                logError("Error occured while creating Invoice History Log...");
            }

            invHistory.remove();
            if (corpOrg != null) {
                corpOrg.remove();
            }
			//IR T36000018821 END

            invoice.remove();
        }

        return true;
    }

    /**
     * Moves orphaned entry to failed status.
     *
     * @return boolean
     * @throws AmsException
     */
    public boolean processOrphan() throws AmsException {
        try {
            invoiceFinanceAmountQueue = (InvoiceFinanceAmountQueue) EJBObjectFactory.createClientEJB(ejbServerLocation,
                    "InvoiceFinanceAmountQueue",
                    invoiceFinanceAmountQueueOid);

            invoiceFinanceAmountQueue.setClientServerDataBridge(csdb);
            invoiceFinanceAmountQueue.setAttribute("status",
                    TradePortalConstants.INV_FIN_AMNT_STAT_RECALC_FAILED);
            invoiceFinanceAmountQueue.save();
        } catch (Exception e) {
            logError("Error occured processing Interest Discount updates orphan...", e);
            throw new AmsException(e);
        }

        return true;
    }

    public void logError(String errorText) {
        String str = " : UploadFilename: " + interestDiscRateGrpOid
                + "  :: PaymentUpload_Oid: " + invoiceFinanceAmountQueueOid
                + " ";
        Logger.getLogger().log(loggerCategory, process_id,
                process_id + ": ERROR: " + errorText + str,
                Logger.ERROR_LOG_SEVERITY);

    }

    public void logError(String errorText, Exception e) {
        String str = " : UploadFilename: " + interestDiscRateGrpOid
                + "  :: PaymentUpload_Oid: " + invoiceFinanceAmountQueueOid
                + " ";
        Logger.getLogger().log(
                loggerCategory, process_id,
                process_id + ": ERROR: " + errorText + ": " + e.getMessage()
                + str, Logger.ERROR_LOG_SEVERITY);

    }

    public void logDebug(String infoText) {
        String str = " : UploadFilename: " + interestDiscRateGrpOid
                + "  :: PaymentUpload_Oid: " + invoiceFinanceAmountQueueOid
                + " ";
        Logger.getLogger().log(loggerCategory, process_id,
                process_id + ": INFO: " + infoText + str,
                Logger.DEBUG_LOG_SEVERITY);
    }

}
