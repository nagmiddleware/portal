package com.ams.tradeportal.common;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.util.*;

/**
 * Class to handle debug messages.
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class Debug
 {
private static final Logger LOG = LoggerFactory.getLogger(Debug.class);
    private static boolean showDebugMessages;
    
    static 
     {
         try
          {
            JPylonProperties jPylonProperties = JPylonProperties.getInstance();

            String debugMode = jPylonProperties.getString("debugMode");
             
            if( (debugMode == null) || 
                (debugMode.equals("0")) )
            {
                showDebugMessages = false;
            }
            else
            {
                showDebugMessages = true; 
            }
          }
         catch(Exception e)
          {
            showDebugMessages = false; 
          }
      }
      
     /**
      * Outputs the message to standard out if the debug flag is turned on.
      * (the debug flag is the jPylon "debugMode" property)
      *
      * @param msg String - the message to be output
      */
     public static void debug(String msg)
      {
         if(showDebugMessages)
          {
        	 System.out.println(msg); 
          }
      }
    
 }