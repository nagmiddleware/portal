package com.ams.tradeportal.common;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.RemoveException;

import com.ams.tradeportal.busobj.Account;
import com.ams.tradeportal.busobj.DomesticPayment;
import com.ams.tradeportal.busobj.Instrument;
import com.ams.tradeportal.busobj.InvoiceDetails;
import com.ams.tradeportal.busobj.PaymentDefinitions;
import com.ams.tradeportal.busobj.PaymentParty;
import com.ams.tradeportal.busobj.PaymentUploadErrorLog;
import com.ams.tradeportal.busobj.Terms;
import com.ams.tradeportal.busobj.TermsParty;
import com.ams.tradeportal.busobj.util.InstrumentServices;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.BusinessObject;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.frame.EJBObjectFactory;
import com.amsinc.ecsg.frame.ErrorManager;
import com.amsinc.ecsg.frame.IssuedError;
import com.amsinc.ecsg.frame.ReferenceDataManager;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.GMTUtility;
import com.amsinc.ecsg.util.StringFunction;

/**
 * This represents a payment_definitions. It is an alternative way to the
 * PaymentDefinitions EJB of the same data that is easier to use.
 *
 * A PaymentFileFormat defines the payment file format. It defines the date
 * format etc. It also consists of the definition of a number of header fields,
 * payment fields, invoice fields, trailor fields (i.e. batch record for ABA/PGN
 * format).
 *

 */
public abstract class PaymentFileFormat {
private static final Logger LOG = LoggerFactory.getLogger(PaymentFileFormat.class);

    protected String delimiterChar;
    protected String dateFormat;
    protected String paymentMethod;
    protected String formatType; //FIX, DLM, ABA, PNG
    protected boolean includeColumnHeaders;
    protected PaymentFileFieldFormat[] headerFields;
    protected PaymentFileFieldFormat[] paymentFields;
    protected PaymentFileFieldFormat[] invoiceFields;
    protected ArrayList<PaymentFileFieldFormat> headerFieldsArray;
    protected ArrayList<PaymentFileFieldFormat> paymentFieldsArray;
    protected ArrayList<PaymentFileFieldFormat> invoiceFieldsArray;
    protected PaymentFileFieldFormat[] trailerFields;
    protected HashMap<String, PaymentFileFieldFormat> fieldNameToFieldMap;
    protected String HEADER_SECTION_IDENTIFIER;
    protected String PAYMENT_SECTION_IDENTIFIER;
    protected String INVOICE_SECTION_IDENTIFIER;
    protected String TRAILER_SECTION_IDENTIFIER;
    protected String HEADER_SECTION_DESCRIPTION;
    protected String PAYMENT_SECTION_DESCRIPTION;
    protected String TRAILER_SECTION_DESCRIPTION;
    protected String execDate = null;
    protected static Map lineTypes = new HashMap();

    private static final long MAX_FILE_BENES = Long.parseLong(TradePortalConstants.getPropertyValue("TradePortal", "uploadFileMaxBenes", "10000"));
    protected static String[] ERROR_SUB = null;

    protected abstract String readHeaderLine(PaymentFileProcessor proc, boolean[] errorsFound) throws java.io.IOException, AmsException, RemoveException;

    protected abstract String[] readPaymentLine(String[] readAheadLine, boolean[] errorsFound, PaymentFileProcessor proc) throws java.io.IOException, AmsException, RemoveException;

    protected abstract String[] parseHeaderLine(String headerLine, PaymentFileProcessor proc) throws AmsException, RemoteException;

    protected abstract String[] parsePaymentLine(String[] headerFields, String[] paymentInvoiceLines, PaymentFileProcessor proc) throws AmsException, RemoteException;

    static {
        lineTypes.put(PaymentFileFieldFormat.LINE_TYPE_HEADER, "Header");
        lineTypes.put(PaymentFileFieldFormat.LINE_TYPE_PAYMENT, "Detail");
        lineTypes.put(PaymentFileFieldFormat.LINE_TYPE_TRAILER, "Batch");
    }

    /**
     * Get appropriate subclass of PaymentFileFormat.
     *
     * @param paymentFileDefinitionOid
     * @param proc
     * @return
     * @throws RemoteException
     * @throws AmsException
     */
    public static PaymentFileFormat getPaymentFileFormat(long paymentFileDefinitionOid, PaymentFileProcessor proc) throws RemoteException, AmsException {

        PaymentDefinitions paymentDefinition = (PaymentDefinitions) EJBObjectFactory.createClientEJB(proc.ejbServerLocation, "PaymentDefinitions");
        paymentDefinition.getData(paymentFileDefinitionOid);
        PaymentFileFormat paymentFileFormat;
        String fileFormatType = paymentDefinition.getAttribute("file_format_type");
        proc.paymentFileDefFormat = fileFormatType;
        if (TradePortalConstants.PAYMENT_FILE_TYPE_DELIMIT.equals(fileFormatType)) {
            paymentFileFormat = new PaymentFileDelimitedFormat(paymentDefinition);
        } else if (TradePortalConstants.PAYMENT_FILE_TYPE_FIX.equals(fileFormatType)) {
            paymentFileFormat = new PaymentFileFixedFormat(paymentDefinition);
        } else if (TradePortalConstants.PAYMENT_FILE_TYPE_ABA.equals(fileFormatType)) {
            paymentFileFormat = new PaymentFileABAFormat(paymentDefinition);
        } else {
            paymentFileFormat = new PaymentFilePNGFormat(paymentDefinition);
        }

        return paymentFileFormat;
    }

    /**
     * Constructor given the PaymentDefinitions EJB.
     *
     * @param paymentDefinition
     * @throws RemoteException
     * @throws AmsException
     */
    public PaymentFileFormat(PaymentDefinitions paymentDefinition) throws RemoteException, AmsException {
        dateFormat = paymentDefinition.getAttribute("date_format");
        paymentMethod = paymentDefinition.getAttribute("payment_method");
        includeColumnHeaders = ("Y".equals(paymentDefinition.getAttribute("include_column_headers")));
        fieldNameToFieldMap = new HashMap<String, PaymentFileFieldFormat>();
        formatType = paymentDefinition.getAttribute("file_format_type");
            // resMgr = new ResourceManager(proc.csdb);

    }

    /**
     * Read the file contents from user input stream, save to a temporary file,
     * do file-level validation. Used by PaymentUploadServlet.
     *
     * @param proc
     * @return
     * @throws java.io.IOException
     * @throws AmsException
     * @throws RemoteException
     * @throws RemoveException
     */
    public boolean uploadPaymentFile(PaymentFileProcessor proc)
            throws java.io.IOException, AmsException, RemoveException {
        String sLine;

        //ignore any column header line
        if (this.includeColumnHeaders) {
            sLine = readLine(proc.reader, proc.writer);
            proc.lineNumber++;
            proc.lineNumberForLogging++;
        }

        boolean[] errorsFound = {false};
        // Read header line.
        sLine = readHeaderLine(proc, errorsFound);
        if (errorsFound[0] == true) {
            proc.rejectFileUplaod(false);
            return false;
        }

        // Parse Header line
        String[] headerRecords = parseHeaderLine(sLine, proc);

        // Validate header line
        proc.beneName = HEADER_SECTION_DESCRIPTION;
        if (!validateHeaderLine(headerRecords, proc)) {
            return false;
        }

        // Go through each payment line 
        String[] readAheadLine = new String[1];
        readAheadLine[0] = readLine(proc.reader, proc.writer);
        proc.lineNumber++;
        // process each payment line until the end of file or the trailer line, i.e. batch records, which is only applicable for ABA and PNG.
        boolean errorFound = false;
        while (readAheadLine[0] != null
                && !(TRAILER_SECTION_IDENTIFIER != null && readAheadLine[0].startsWith(TRAILER_SECTION_IDENTIFIER))) {
            proc.lineNumberForLogging = proc.lineNumber;
            if (StringFunction.isBlank(readAheadLine[0])) {
                readAheadLine[0] = readLine(proc.reader, proc.writer);
                proc.lineNumber++;
                continue;
            }

            // read payment line(s)
            String[] payLines = readPaymentLine(readAheadLine, errorsFound, proc);

            proc.paymentCount++;
            if (proc.paymentCount > MAX_FILE_BENES) {
                logError(proc.lineNumberForLogging, "Payment Detail", "Payment File cannot exceed 10,000 beneficiaries.", ERROR_SUB, proc);

                errorFound = true;
                break;
            }

            // parse payment line(s)
            String[] paymentRecords = parsePaymentLine(headerRecords, payLines, proc);

            //Set beneName
            proc.beneName = getValue(PaymentFileFieldFormat.PAYEE_NAME, headerRecords, paymentRecords, null);
            if (proc.beneName == null) {
                proc.beneName = "UNSPECIFIED BENEFICIARY";
            }

                // Check for rogue format error.  If no error, start instrument and transaction
            // If there is any fatal error (e.g. mismatching currency), reverse all changes and exit.
            if (!validateLineForFatalError(paymentRecords, paymentFields, PaymentFileFieldFormat.LINE_TYPE_PAYMENT, proc)) {
                return false;
            }

            formatAmount(proc, paymentRecords);
            proc.successCount++;
        }

        // If the file should have trailer line, i.e. batch control record for ABA & PNG, parse and validate
        if (TRAILER_SECTION_IDENTIFIER != null) {
            proc.beneName = TRAILER_SECTION_DESCRIPTION;
            proc.lineNumberForLogging = proc.lineNumber;
            String trailerLine = readAheadLine[0];
            if (StringFunction.isBlank(trailerLine)) {
                logError(proc.lineNumberForLogging, proc.beneName, TRAILER_SECTION_DESCRIPTION + " is missing.", ERROR_SUB, proc);
                return false;
            }

            String[] trailerRecords = parseTrailerLine(trailerLine);

            if (!validateTrailerLine(trailerRecords, proc)) {
                return false;
            }

        }

        return !errorFound;

    }

    // Method to format Amount in case of ABA AND PNG Formats

    public void formatAmount(PaymentFileProcessor proc, String[] paymentRecords) {

    }

    /**
     * Validate the payment file, create instrument, transaction,
     * domestic_payments.
     *
     * @param proc
     * @return
     * @throws java.io.IOException
     * @throws AmsException
     * @throws RemoteException
     * @throws RemoveException
     */
    public boolean processPaymentFile(PaymentFileProcessor proc)
            throws java.io.IOException, AmsException, RemoveException {
        String sLine;

        //ignore any column header line
        if (this.includeColumnHeaders) {
            sLine = readLine(proc.reader, proc.writer); //ignore column headers
            proc.lineNumber++;
            proc.lineNumberForLogging++;
        }

        boolean[] errorsFound = {false};
        // Read header line.
        sLine = readHeaderLine(proc, errorsFound);
        if (errorsFound[0] == true) {
            proc.rejectFileUplaod(false);
            return false;
        }

        // Parse Header line
        String[] headerRecords = parseHeaderLine(sLine, proc);

        // Validate header line
        proc.beneName = HEADER_SECTION_DESCRIPTION;
        if (!validateHeaderLine(headerRecords, proc)) {
            proc.rejectFileUplaod(false);
            return false;
        }

        // Instantiate instrument, transaction.
        proc.init();

        // Check panel authorization.
        if (proc.corpOrg.isPanelAuthEnabled(InstrumentType.DOM_PMT)) {
            proc.validatePanelSetupData();
        }

        // Go through each payment line 
        String[] readAheadLine = new String[1];
        readAheadLine[0] = readLine(proc.reader, proc.writer);
        proc.lineNumber++;
        // process each payment line until the end of file or the trailer line, i.e. batch records, which is only applicable for ABA and PNG.
        boolean errorFound = false;
        while (readAheadLine[0] != null
                && !(TRAILER_SECTION_IDENTIFIER != null && readAheadLine[0].startsWith(TRAILER_SECTION_IDENTIFIER))) {
            proc.lineNumberForLogging = proc.lineNumber;
            if (StringFunction.isBlank(readAheadLine[0])) {
                readAheadLine[0] = readLine(proc.reader, proc.writer);
                proc.lineNumber++;
                continue;
            }

            // read payment line(s)
            String[] payLines = readPaymentLine(readAheadLine, errorsFound, proc);

            proc.paymentCount++;
            if (proc.paymentCount > 10000) {
                logError(proc.lineNumberForLogging, "Payment Detail", "Payment File cannot exceed 10,000 beneficiaries.", ERROR_SUB, proc);

                errorFound = true;
                break;
            }

            // parse payment line(s)
            String[] paymentRecords = parsePaymentLine(headerRecords, payLines, proc);

            //Set beneName
            proc.beneName = getValue(PaymentFileFieldFormat.PAYEE_NAME, headerRecords, paymentRecords, null);
            if (proc.beneName == null) {
                proc.beneName = "UNSPECIFIED BENEFICIARY";
            }

                // Check for rogue format error.  If no error, start instrument and transaction
            // If there is any fatal error (e.g. mismatching currency), reverse all changes and exit.
            if (!validateLineForFatalError(paymentRecords, paymentFields, PaymentFileFieldFormat.LINE_TYPE_PAYMENT, proc)) {
                removeInstrument(proc);
                proc.rejectFileUplaod(false);
                return false;
            }

            // Basic validation - whether any required fields are missing, any fields exceeding the data size limit
            if (!validatePaymentLine(paymentRecords, proc)) {
                errorFound = true;
                continue;
            }

            // process the payment line - create the domestic_payment and populate it.  Update fields on transaction or terms if needed.
            boolean result = processPaymentLine(headerRecords, paymentRecords, proc);
            if (!result) {
                errorFound = true;
                continue;
            }

            proc.successCount++;
        }

        // If the file should have trailer line, i.e. batch control record for ABA & PNG, parse and validate
        if (TRAILER_SECTION_IDENTIFIER != null) {
            proc.beneName = TRAILER_SECTION_DESCRIPTION;
            proc.lineNumberForLogging = proc.lineNumber;
            String trailerLine = readAheadLine[0];
            if (StringFunction.isBlank(trailerLine)) {
                removeInstrument(proc);
                logError(proc.lineNumberForLogging, proc.beneName, TRAILER_SECTION_DESCRIPTION + " is missing.", ERROR_SUB, proc);
                proc.rejectFileUplaod(false);
                return false;
            }

            String[] trailerRecords = parseTrailerLine(trailerLine);

            if (!validateTrailerLine(trailerRecords, proc)) {
                removeInstrument(proc);
                proc.rejectFileUplaod(false);
                return false;
            }

        }
        // Verify and save transaction and update payment_upload_file status
        proc.postProcess();
        proc.saveProcessingTimeData();
        return !errorFound;
    }

    protected boolean validateHeaderLine(String[] headerRecords, PaymentFileProcessor proc) throws AmsException, RemoteException, RemoveException {
        return validateLineForFatalError(headerRecords, headerFields, PaymentFileFieldFormat.LINE_TYPE_HEADER, proc)
                && validateLine(headerRecords, headerFields, PaymentFileFieldFormat.LINE_TYPE_HEADER, proc);
    }

    protected boolean validatePaymentLine(String[] paymentRecords, PaymentFileProcessor proc)
            throws AmsException, RemoteException, RemoveException {
        return validateLine(paymentRecords, paymentFields, PaymentFileFieldFormat.LINE_TYPE_PAYMENT, proc);

    }

    protected boolean validateTrailerLine(String[] trailerRecords, PaymentFileProcessor proc)
            throws AmsException, RemoteException, RemoveException {
        return validateLine(trailerRecords, trailerFields, PaymentFileFieldFormat.LINE_TYPE_TRAILER, proc);

    }

    protected boolean validateLine(String[] dataRecords, PaymentFileFieldFormat[] definitionFields, String lineType, PaymentFileProcessor proc) throws RemoveException, AmsException, RemoteException {
        if (definitionFields == null) {
            return true;
        }
        boolean errorFound = false;
        for (int i = 0; i < definitionFields.length; i++) {
            List reqFields = getMandatoryFields(this.paymentMethod);
            if ((definitionFields[i].isFieldRequired && (dataRecords.length < i + 1 || dataRecords[i] == null)) || (reqFields.contains(definitionFields[i].columnName) && StringFunction.isBlank(dataRecords[i]))) {
                logError(proc.lineNumberForLogging, proc.beneName, "Field '" + definitionFields[i].description + "' is a required value and has not been provided.", ERROR_SUB, proc);
                errorFound = true;
            } else if (definitionFields[i].isDataRequired && (dataRecords.length < i + 1 || StringFunction.isBlank(dataRecords[i]))) {
                if (PaymentFileFieldFormat.LINE_TYPE_TRAILER.equals(lineType) && "br_reserved1".equals(definitionFields[i].columnName)) {
                    logError(proc.lineNumberForLogging, proc.beneName, "The '" + definitionFields[i].description + " field ' in the batch control record is missing.", ERROR_SUB, proc);
                } else {
                    logError(proc.lineNumberForLogging, proc.beneName, "'" + definitionFields[i].description + "' is missing.", ERROR_SUB, proc);
                }
                errorFound = true;
            } else if (dataRecords.length >= i + 1 && dataRecords[i] != null && dataRecords[i].length() > (PaymentFileFieldFormat.PAYEE_INVOICE_DETAILS.equals(definitionFields[i].columnName) ? (definitionFields[i].size * 1000) : definitionFields[i].size)) {
                long fieldSize = dataRecords[i].length();
                if (dataRecords[i].length() > 1000) {
                    dataRecords[i] = dataRecords[i].substring(0, 1000) + "...";
                }
                logError(proc.lineNumberForLogging, proc.beneName, "'" + definitionFields[i].description + "' - \"" + dataRecords[i] + "\" has " + fieldSize + " characters, exceeding the size limit of "
                        + (PaymentFileFieldFormat.PAYEE_INVOICE_DETAILS.equals(definitionFields[i].columnName) ? (definitionFields[i].size * 1000) : definitionFields[i].size) + " characters.", ERROR_SUB, proc);
                errorFound = true;
            }

        }

        return !errorFound;
    }

    /**
     * Check for fatal errors that would fail the whole file.
     *
     * @param dataRecords
     * @param definitionFields
     * @param lineType
     * @param proc
     * @return
     * @throws RemoveException
     * @throws AmsException
     * @throws RemoteException
     */
    protected boolean validateLineForFatalError(String[] dataRecords, PaymentFileFieldFormat[] definitionFields, String lineType, PaymentFileProcessor proc) throws RemoveException, AmsException, RemoteException {
        // If there is one field (i.e. no delimiter) or the first field contains non-ascii, the format must be wrong.
        if (proc.paymentCount == 1 && (dataRecords.length == 1 || !dataRecords[0].matches("\\A\\p{ASCII}*\\z"))) {
            logError(proc.lineNumberForLogging, proc.beneName, "File must be in either a Fixed File Format, or the ABA or PNG File format or a Delimited (by Pipe, Comma, Semi-Colon, Tab) Format. Please correct your file format and upload again.", ERROR_SUB, proc);
            return false;
        }

        // payment method matches the one specified in the definition
        PaymentFileFieldFormat paymentMethodField = this.fieldNameToFieldMap.get(PaymentFileFieldFormat.PAYMENT_METHOD_TYPE);
        boolean errorFound = false;
        if (paymentMethodField != null && paymentMethodField.lineType.equals(lineType) && dataRecords.length > paymentMethodField.order) {
            String filePaymentMethod = dataRecords[paymentMethodField.order];
            if (!ReferenceDataManager.getRefDataMgr().checkCode("PAYMENT_METHOD", filePaymentMethod)) {
                logError(proc.lineNumberForLogging, proc.beneName, "Payment method '" + filePaymentMethod + (!(this instanceof PaymentFileDelimitedFormat) ? "' in the " + lineTypes.get(lineType) + " " : "' provided on line '" + proc.lineNumberForLogging + "' ") + "is not valid.", ERROR_SUB, proc);
                errorFound = true;
            }
            if (!filePaymentMethod.equals(this.paymentMethod)) {
                logError(proc.lineNumberForLogging, proc.beneName, "Payment method '" + filePaymentMethod + (!(this instanceof PaymentFileDelimitedFormat) ? "' in the " + lineTypes.get(lineType) + " does not match " : "' in the file is not consistent ") + "with the payment method '" + this.paymentMethod + "' in the selected Payment File Definition.", ERROR_SUB, proc);
                errorFound = true;
            }
            PaymentFileFieldFormat currencyField = this.fieldNameToFieldMap.get(PaymentFileFieldFormat.AMOUNT_CURRENCY_CODE);
            PaymentFileFieldFormat debitAccountField = this.fieldNameToFieldMap.get(PaymentFileFieldFormat.DEBIT_ACCOUNT_NO);
            String currency = dataRecords[currencyField.order];
            if (this instanceof PaymentFileABAFormat) {
                currency = null;
            }
            String debitAccountNo = dataRecords[debitAccountField.order];
            if (!(this instanceof PaymentFilePNGFormat)) {
                if (!validatePaymentCurrencyCountry(proc, filePaymentMethod, currency, debitAccountNo)) {
                    errorFound = true;
                }
            }
        }

        //Execution date is in the correct format and is not back-dated.
        PaymentFileFieldFormat execDateField = this.fieldNameToFieldMap.get(PaymentFileFieldFormat.EXECUTION_DATE);
        if (execDateField != null && execDateField.lineType.equals(lineType) && dataRecords.length > execDateField.order) {
            String fileExecDate = dataRecords[execDateField.order];
            Date execDate;
            if ((fileExecDate == null || fileExecDate.trim().equals("")) & this instanceof PaymentFileABAFormat) {
                //For ABA & PNG formats we should not set the execution date to current date. Missing Execution Date Error will be thrown at standard validation step.
            } else if (fileExecDate == null || fileExecDate.trim().equals("")) {
                // If no execution date is provided, set the execution date to the current date
                execDate = GMTUtility.getGMTDateTime(true);
                SimpleDateFormat otherFormatter = new SimpleDateFormat(this.dateFormat);
                dataRecords[execDateField.order] = otherFormatter.format(execDate);
                this.execDate = dataRecords[execDateField.order];
            } else {
                execDate = TPDateTimeUtility.convertDateStringToDate(fileExecDate.trim(), this.dateFormat);
                if (execDate == null) {
                    //jgadela 03/21/2014 - T36000026425 - corrected the error message format.
                    //logError(proc.lineNumberForLogging, proc.beneName, execDateField.description + " '" + fileExecDate + "' is in the incorrect format based on the selected file definition. Please update to '" + this.dateFormat + "' format.", ERROR_SUB, proc);
                    logError(proc.lineNumberForLogging, proc.beneName, execDateField.description + " line (" + proc.paymentCount + ") is in the incorrect format. Please update to '" + this.dateFormat + "' format.", ERROR_SUB, proc);
                    errorFound = true;
                } else {
                    java.util.Date todaysDate = GMTUtility.getGMTDateTime(true);
                    java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("yyyy/MM/dd");
                    String curDateStr = formatter.format(execDate);
                    String todaysDateStr = formatter.format(todaysDate);
                    LOG.debug("PaymentFileFormat::validateLine:: " + curDateStr + " vs. " + todaysDateStr);

                    // do not show error if Confirming Payment
                    if (curDateStr.compareTo(todaysDateStr) < 0) {
                        logError(proc.lineNumberForLogging, proc.beneName, "'" + execDateField.description + "' cannot be backdated", ERROR_SUB, proc);
                        errorFound = true;
                    } else {
                        if (StringFunction.isNotBlank(this.execDate)) {

                            if (curDateStr.compareTo(formatter.format(TPDateTimeUtility.convertDateStringToDate(this.execDate.trim(), this.dateFormat))) != 0) {
                                SimpleDateFormat otherFormatter = new SimpleDateFormat(this.dateFormat);
                                dataRecords[execDateField.order] = otherFormatter.format(execDate);
                            }
                        } else {
                            SimpleDateFormat otherFormatter = new SimpleDateFormat(this.dateFormat);
                            this.execDate = otherFormatter.format(execDate);
                        }
                    }
                }
            }
        }

        // city + province <= 31 characters.
        if (lineType.equals(PaymentFileFieldFormat.LINE_TYPE_PAYMENT)) {
            String city = getValue(PaymentFileFieldFormat.COLUMN_CITY, null, dataRecords, null);
            if (city == null) {
                city = "";
            }
            String province = getValue(PaymentFileFieldFormat.COLUMN_PROVINCE, null, dataRecords, null);
            if (province == null) {
                province = "";
            }
            if (city.length() + province.length() > 31) {
                logError(proc.lineNumberForLogging, proc.beneName, "Combination of City (" + city + ") & Province (" + province + ") cannot be more than 31 characters.", ERROR_SUB, proc);
                errorFound = true;
            }

            city = getValue(PaymentFileFieldFormat.FIRST_INTERMEDIARY_BANK_BRANCH_CITY, null, dataRecords, null);
            if (city == null) {
                city = "";
            }
            province = getValue(PaymentFileFieldFormat.FIRST_INTERMEDIARY_BANK_BRANCH_PROVINCE, null, dataRecords, null);
            if (province == null) {
                province = "";
            }
            if (city.length() + province.length() > 31) {
                logError(proc.lineNumberForLogging, proc.beneName, "Combination of City (" + city + ") & Province (" + province + ") cannot be more than 31 characters.", ERROR_SUB, proc);
                errorFound = true;
            }

            String benAccountNumber = getValue(PaymentFileFieldFormat.PAYEE_ACCOUNT_NUMBER, null, dataRecords, null);
            PaymentFileFieldFormat benAccoutnNumberField = this.fieldNameToFieldMap.get(PaymentFileFieldFormat.PAYEE_ACCOUNT_NUMBER);
            if (StringFunction.isNotBlank(benAccountNumber) && !benAccountNumber.matches("[A-Za-z0-9 -]+")) {
                logError(proc.lineNumberForLogging, proc.beneName, "Invalid value '" + benAccountNumber + "' in the " + benAccoutnNumberField.description + " field.", ERROR_SUB, proc);
                errorFound = true;
            }

        }

        //Check valid currency.
        PaymentFileFieldFormat currencyField = this.fieldNameToFieldMap.get(PaymentFileFieldFormat.AMOUNT_CURRENCY_CODE);
        if (currencyField != null && currencyField.lineType.equals(lineType) && dataRecords.length > currencyField.order) {
            String currency = dataRecords[currencyField.order];
            if (!StringFunction.isBlank(currency)) {
                // For the first time the currency appears in the file, validate that it is a valid currency.
                // For the rest of the currency fields, validate that it is the same currency.
                if (proc.currency == null) {
                    int currencyRefData = DatabaseQueryBean.getCount("code", "refdata", "table_type = 'CURRENCY_CODE' and code = ? and locale_name is null", false, new Object[]{currency});
                    if (currencyRefData <= 0) {
                        logError(proc.lineNumberForLogging, proc.beneName, "Payment Currency '" + currency + "' is not valid.", ERROR_SUB, proc);
                        errorFound = true;
                    } else {
                        proc.currency = currency;
                    }
                } else if (!proc.currency.equals(currency)) {
                    logError(proc.lineNumberForLogging, proc.beneName, "Payment Currency must be the same for each Beneficiary.", ERROR_SUB, proc);
                    errorFound = true;
                }
            }
        }

        PaymentFileFieldFormat debitAccountField = this.fieldNameToFieldMap.get(PaymentFileFieldFormat.DEBIT_ACCOUNT_NO);
        if (debitAccountField != null && debitAccountField.lineType.equals(lineType) && dataRecords.length > debitAccountField.order) {
            String fileDebitAccountNo = dataRecords[debitAccountField.order];
            if (StringFunction.isNotBlank(fileDebitAccountNo)) {
                String debitAccountOid = getDebitAccountOid(fileDebitAccountNo, proc);
                if (debitAccountOid == null) {
                    errorFound = true;
                }
            }
        }

        //Rel9.0 CR-921
        //FX Contract Number and Rate Validations
        //This will be done on only the first line
        if (proc.paymentCount < 2) {
            PaymentFileFieldFormat fxContractRate = this.fieldNameToFieldMap.get(PaymentFileFieldFormat.COLUMN_FX_CONTRACT_RATE);
            PaymentFileFieldFormat fxContractNumber = this.fieldNameToFieldMap.get(PaymentFileFieldFormat.COLUMN_FX_CONTRACT_NUMBER);
            ERROR_SUB = new String[1];//initialize
            if ((fxContractRate != null && fxContractRate.lineType.equals(lineType) && dataRecords.length > fxContractRate.order)
                    && (fxContractNumber != null && fxContractNumber.lineType.equals(lineType) && dataRecords.length > fxContractNumber.order)) {
                String fxContractNum = dataRecords[fxContractNumber.order];
                String fxRate = dataRecords[fxContractRate.order];
                //Validate FX Contract Rate and FX Contract Number are either both available or both not available
                if (StringFunction.isNotBlank(fxContractNum) && StringFunction.isBlank(fxRate)) {
                    logError(proc.lineNumberForLogging, proc.beneName, TradePortalConstants.FX_RATE_VALUE_MISSING, ERROR_SUB, proc);
                    errorFound = true;
                } else if (StringFunction.isNotBlank(fxRate) && StringFunction.isBlank(fxContractNum)) {
                    logError(proc.lineNumberForLogging, proc.beneName, TradePortalConstants.FX_CONTRACT_VALUE_MISSING, ERROR_SUB, proc);
                    errorFound = true;
                }
	    		//Rel9.0 IR#T36000028375
                //The condition where DataRequired is specified for BOTH yet one or both are missing - [START]
                //this else if condition is applicable where delimiters exists with no data
                //The condition where delimiters do not exist is handled in the outer else statement of parent if statement
                //Rel9.0 IR#T36000029135 - check each condition separately i.e using 'if' statement and not 'if else' per condition
                if (fxContractRate.isDataRequired && fxContractNumber.isDataRequired) {
                    if (StringFunction.isBlank(fxRate)) {
                        ERROR_SUB[0] = fxContractRate.description;
                        logError(proc.lineNumberForLogging, proc.beneName, TradePortalConstants.FX_REQUIRED, ERROR_SUB, proc);
                        errorFound = true;
                    }
                    if (StringFunction.isBlank(fxContractNum)) {
                        ERROR_SUB[0] = fxContractNumber.description;
                        logError(proc.lineNumberForLogging, proc.beneName, TradePortalConstants.FX_REQUIRED, ERROR_SUB, proc);
                        errorFound = true;
                    }	//-[END]
                }//Rel9.0 IR#T36000029135
                if (StringFunction.isNotBlank(fxRate) && StringFunction.isNotBlank(fxContractNum)) {

                    //T36000029918 - Validate FXRate format
                    String[] temp = fxRate.split("\\.");
                    boolean invalidFX = false;
                    if (temp.length < 3) {
                        if (temp[0].length() > 0) {
                            if (InstrumentServices.containsOnlyNumbers(temp[0])) {
                                if (temp[0].length() > 5) {
                                    invalidFX = true;
                                }
                            } else {
                                invalidFX = true;
                            }

                        }
                        if ((temp.length > 1)) {
                            if (InstrumentServices.containsOnlyNumbers(temp[1])) {
                                if (temp[1].length() > 8) {
                                    invalidFX = true;
                                }
                            } else {
                                invalidFX = true;
                            }
                        }

                    } else {
                        invalidFX = true;
                    }

	    			//both are available, check the format
                    //fx contract is alphanumeric
                    if (!StringFunction.isBlank(InstrumentServices.isAlphaNumeric(fxContractNum, false))) {
                        //fxContractNum is not alphanumeric 
                        errorFound = true;
                    }
                    //check length for fx contract number
                    if (!(fxContractNum.length() < 15)) {
                        logError(proc.lineNumberForLogging, proc.beneName, TradePortalConstants.FX_CONTRACT_SIZE_EXCEEDED, ERROR_SUB, proc);
                        errorFound = true;
                    }
                    //If any format checks failed, display the common format error
                    if (invalidFX) {
                        logError(proc.lineNumberForLogging, proc.beneName, TradePortalConstants.FX_CONTRACT_INVALID_FORMAT, ERROR_SUB, proc);
                        errorFound = true;
                    }

                }
                //Rel9.0 IR#T36000028375
            } else if ((fxContractRate != null && fxContractRate.lineType.equals(lineType) && dataRecords.length <= fxContractRate.order)
                    || (fxContractNumber != null && fxContractNumber.lineType.equals(lineType) && dataRecords.length <= fxContractNumber.order)) {
	        	//If data records are less than (or equal) to index of fx contract
                //yet is it specified as being data required, then display error - indicates data is missing
                if (fxContractNumber.isDataRequired && dataRecords.length <= fxContractNumber.order) {
                    ERROR_SUB[0] = fxContractNumber.description;
                    logError(proc.lineNumberForLogging, proc.beneName, TradePortalConstants.FX_REQUIRED, ERROR_SUB, proc);
                    errorFound = true;
                }
	        	//If data records are less than (or equal) to index of fx rate
                //yet is it specified as being data required, then display error - indicates data is missing	
                if (fxContractRate != null && fxContractRate.isDataRequired && dataRecords.length <= fxContractRate.order) {
                    ERROR_SUB[0] = fxContractRate.description;
                    logError(proc.lineNumberForLogging, proc.beneName, TradePortalConstants.FX_REQUIRED, ERROR_SUB, proc);
                    errorFound = true;
                }
            }
            ERROR_SUB = null; //release memory allocation
        }
        return !errorFound;
    }

    /**
     * If no bene is uploaded we need to remove the instrument that has been
     * created.
     *
     * @param proc
     * @throws RemoteException
     * @throws AmsException
     */
    protected void removeInstrument(PaymentFileProcessor proc) throws RemoteException, AmsException {
        String transactionOid = null;
        if (proc.trans != null) {
            transactionOid = proc.trans.getAttribute("transaction_oid");
        }
        StringBuilder deleteDPSQL = new StringBuilder();
        deleteDPSQL.append("delete from domestic_payment where p_transaction_oid = ? ");
        try {
            if (!StringFunction.isBlank(transactionOid)) {
                DatabaseQueryBean.executeUpdate(deleteDPSQL.toString(),false, new Object[]{transactionOid});
            }
        } catch (java.sql.SQLException e) {
            throw new AmsException(e);
        }

        String instrumentOid = proc.paymentUpload.getAttribute("instrument_oid");
        if (!StringFunction.isBlank(instrumentOid)) {
            Instrument instrument = (Instrument) EJBObjectFactory.createClientEJB(proc.ejbServerLocation, "Instrument", proc.csdb);
            instrument.getData(Long.valueOf(instrumentOid));
            instrument.delete();
            proc.paymentUpload.setAttribute("instrument_oid", null);
        }
    }

    /**
     * Create domestic_payment and set attributes and further validation on the
     * EJB.
     *
     * @param headerRecord
     * @param paymentRecord
     * @param proc
     * @return
     * @throws AmsException
     * @throws RemoteException
     * @throws RemoveException C
     */
    protected boolean processPaymentLine(String[] headerRecord, String[] paymentRecord, PaymentFileProcessor proc)
            throws AmsException, RemoteException, RemoveException {
        proc.domesticPayment = (DomesticPayment) EJBObjectFactory.createClientEJB(proc.ejbServerLocation, "DomesticPayment", proc.csdb);
        proc.domesticPayment.newObject();
        proc.domesticPayment.setAttribute("transaction_oid", proc.trans.getAttribute("transaction_oid"));
        proc.inputDoc.setAttribute("/Transaction/uploaded_ind", "Y");
        ArrayList<String> attributeNames = new ArrayList<String>(headerRecord.length + paymentRecord.length);
        ArrayList<String> attributeValues = new ArrayList<String>(headerRecord.length + paymentRecord.length);
        attributeNames.add("sequence_number");
        attributeValues.add(String.valueOf(proc.successCount + 1));
        boolean errorFound = false;

        for (int i = 0; i < (headerRecord.length < headerFields.length ? headerRecord.length : headerFields.length); i++) {
            if (!processField(headerRecord, paymentRecord, PaymentFileFieldFormat.LINE_TYPE_HEADER, i, attributeNames, attributeValues, proc)) {
                errorFound = true;
            }
        }
        for (int i = 0; i < (paymentRecord.length < paymentFields.length ? paymentRecord.length : paymentFields.length); i++) {
            if (!processField(headerRecord, paymentRecord, PaymentFileFieldFormat.LINE_TYPE_PAYMENT, i, attributeNames, attributeValues, proc)) {
                errorFound = true;
            }
        }
        String[] dummy = new String[0];
        proc.domesticPayment.setAttributes(attributeNames.toArray(dummy), attributeValues.toArray(dummy));
        if (errorFound) {
            proc.domesticPayment.remove();
            if (proc.invoiceDetails != null) {
                proc.invoiceDetails.remove();
            }
            return false;
        }

        if (proc.successCount == 0) {
            if (TradePortalConstants.INDICATOR_YES.equals(proc.trans.getAttribute("bene_panel_auth_ind")) && proc.panel == null) {
                proc.setTransactionPanelData(proc.domesticPayment);
            }
			// CR857 End
            // this method verify whether fx rate is present for base currency and also store multiply indicator and buy rate of
            // account and transaction currency.
            proc.verifyAndSetRateDefineForFXGroup();
            proc.debitAccountBankGroupOid = proc.getBankGroupForAccount(proc.domesticPayment.getErrorManager());//CR-1001 Rel9.4 Fetch the BankGroup of the Debit account only once for validating reporting codes
        }

        proc.creditAccountBankCountryCode = this.getBankBranchCountryForAccount(proc.inputDoc.getAttribute("/Terms/credit_account_oid"), proc);
        proc.debitAccountBankCountryCode = this.getBankBranchCountryForAccount(proc.inputDoc.getAttribute("/Terms/debit_account_oid"), proc);
        String executionDate = this.execDate;
        if (StringFunction.isBlank(executionDate) & TradePortalConstants.PAYMENT_FILE_TYPE_DELIMIT.equals(formatType)) {
            Date execDate = GMTUtility.getGMTDateTime(true);
            executionDate = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(execDate);
            proc.pmValueDates.put(this.paymentMethod, executionDate.substring(0, 10));
        }
        //Set charges to shared on UI which is "O" in DB if the payment def does not have charges as required and charges is null in uploaded file. 
        String charges = proc.domesticPayment.getAttribute("bank_charges_type");
        if (StringFunction.isBlank(charges)) {
            proc.domesticPayment.setAttribute("bank_charges_type", TradePortalConstants.CHARGE_OTHER);
        }
        // Further verification of the domestic_payment.  Save it if successful.
        String s_result = proc.domesticPayment.verifyDomesticPayment(proc.inputDoc, proc.pmValueDates, proc.creditAccountBankCountryCode, proc.debitAccountBankCountryCode, true, false);
        // Don't save the domestic payment if there is error
        if (!TradePortalConstants.DOM_PMT_VERIFY_SUCCESSFUL.equals(s_result)
                || proc.domesticPayment.getErrorManager().getMaxErrorSeverity() >= ErrorManager.ERROR_SEVERITY) {
            errorFound = true;

            logError(proc.lineNumberForLogging, proc.beneName, proc.domesticPayment, proc);
            proc.domesticPayment.remove();
            if (proc.invoiceDetails != null) {
                proc.invoiceDetails.remove();
            }
            return false;
        }else{
        	//Rel9.4 CR-1001 If no beneficiary error is found, validate Reporting codes
        	if(TradePortalConstants.PAYMENT_FILE_TYPE_DELIMIT.equals(formatType) || TradePortalConstants.PAYMENT_FILE_TYPE_FIX.equals(formatType)){
        		proc.validateReportingCodes(proc.domesticPayment);
        	}
        }

        if (proc.successCount == 0) {
            proc.inputDoc.setAttribute("/Transaction/uploaded_ind", TradePortalConstants.INDICATOR_YES);
            PaymentFileFieldFormat paymentMethodField = this.fieldNameToFieldMap.get(PaymentFileFieldFormat.PAYMENT_METHOD_TYPE);
            String paymentMethod;
            if (paymentMethodField.lineType.equals(PaymentFileFieldFormat.LINE_TYPE_HEADER)) {
                paymentMethod = headerRecord[paymentMethodField.order];
            } else {
                paymentMethod = paymentRecord[paymentMethodField.order];
            }
            proc.pmValueDates.put(paymentMethod, proc.domesticPayment.getAttribute("value_date"));
        }

        proc.totAmt = proc.totAmt.add(new BigDecimal(paymentRecord[fieldNameToFieldMap.get(PaymentFileFieldFormat.PAYMENT_AMOUNT).order]));
        proc.domesticPayment.remove();
        if (proc.invoiceDetails != null) {
            proc.invoiceDetails.save(false);
            proc.invoiceDetails.remove();
            proc.invoiceDetails = null;
        }
        return true;
    }

    private String getBankBranchCountryForAccount(String accountOid, PaymentFileProcessor proc)
            throws RemoteException, AmsException {

        String country = null;

        if (StringFunction.isNotBlank(accountOid)) {
            Account account = (Account) EJBObjectFactory.createClientEJB(proc.ejbServerLocation, "Account", proc.csdb);
            account.getData(Long.parseLong(accountOid));
            country = account.getBankBranchCountryInISO();

        }

        return country;
    }

    /**
     * Hook to process each field.
     *
     * @param headerRecord
     * @param paymentRecord
     * @param fieldType
     * @param fieldIndex
     * @param attributeNames
     * @param attributeValues
     * @param proc
     * @return
     * @throws AmsException
     * @throws RemoteException
     * @throws RemoveException
     */
    public boolean processField(String[] headerRecord, String[] paymentRecord, String fieldType, int fieldIndex, ArrayList<String> attributeNames, ArrayList<String> attributeValues, PaymentFileProcessor proc)
            throws AmsException, RemoteException, RemoveException {
        String fieldName, fieldValue;
        boolean isAttribute;
        if (fieldType.equals(PaymentFileFieldFormat.LINE_TYPE_HEADER)) {
            fieldName = headerFields[fieldIndex].name;
            fieldValue = headerRecord[fieldIndex];
            isAttribute = headerFields[fieldIndex].isAttribute;
        } else {
            fieldName = paymentFields[fieldIndex].name;
            fieldValue = paymentRecord[fieldIndex];
            isAttribute = paymentFields[fieldIndex].isAttribute;
        }

        if (fieldName.equals(PaymentFileFieldFormat.BANK_CHARGES_TYPE)) {
            if (fieldValue == null || fieldValue.trim().length() == 0) {
                fieldValue = TradePortalConstants.CHARGE_OTHER;
            } else if (fieldValue.equals(TradePortalConstants.CHARGE_UPLOAD_FW_OURS)) {
                fieldValue = TradePortalConstants.CHARGE_OUR_ACCT;
            } else if (fieldValue.equals(TradePortalConstants.CHARGE_UPLOAD_FW_BEN)) {
                fieldValue = TradePortalConstants.CHARGE_BEN_FOR_OUTSIDE;
            } else if (fieldValue.equals(TradePortalConstants.CHARGE_UPLOAD_FW_SHARE)) {
                fieldValue = TradePortalConstants.CHARGE_OTHER;
            } else {
                fieldValue = TradePortalConstants.CHARGE_BAD_TRANSLATION;
            }
        } else if (fieldName.equals(PaymentFileFieldFormat.EXECUTION_DATE)) {
            if (proc.successCount == 0) {

                Date execDate = TPDateTimeUtility.convertDateStringToDate(fieldValue.trim(), this.dateFormat);
                fieldValue = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(execDate);
                proc.trans.setAttribute("payment_date", fieldValue);
                proc.inputDoc.setAttribute("/Transaction/payment_date", fieldValue);
            }
            return true;
        } else if (proc.successCount == 0 && fieldName.equals(PaymentFileFieldFormat.AMOUNT_CURRENCY_CODE)) {
            proc.trans.setAttribute("copy_of_currency_code", fieldValue);
            Terms terms = (Terms) proc.trans.getComponentHandle("CustomerEnteredTerms");
            terms.setAttribute("amount_currency_code", fieldValue);
            proc.inputDoc.setAttribute("/Terms/amount_currency_code/", fieldValue);
        } else if (proc.successCount == 0 && fieldName.equals(PaymentFileFieldFormat.CUSTOMER_REFERENCE)) {
            Terms terms = (Terms) proc.trans.getComponentHandle("CustomerEnteredTerms");
            terms.setAttribute("reference_number", fieldValue);
        } else if (fieldName.equals(PaymentFileFieldFormat.DEBIT_ACCOUNT_NO)) {
            if (proc.successCount == 0) {
                String debitAccountOid = getDebitAccountOid(fieldValue, proc);
                if (debitAccountOid == null) {
                    return false;
                }
                Terms terms = (Terms) proc.trans.getComponentHandle("CustomerEnteredTerms");
                terms.setAttribute("debit_account_oid", debitAccountOid);
                proc.inputDoc.setAttribute("/Terms/debit_account_oid", debitAccountOid);
            }
            return true;
        } else if (proc.successCount == 0 && fieldName.equals(PaymentFileFieldFormat.PAYEE_NAME)) {
            Terms terms = (Terms) proc.trans.getComponentHandle("CustomerEnteredTerms");
            TermsParty firstTermsParty = (TermsParty) terms.getComponentHandle("FirstTermsParty");
            firstTermsParty.setAttribute("terms_party_type", TermsPartyType.PAYEE);
            String tempValue = fieldValue;
            if (fieldValue.length() > 35) {
                tempValue = fieldValue.substring(0, 35);
            }
            firstTermsParty.setAttribute("name", tempValue);
            firstTermsParty.setAttribute("contact_name", tempValue);
        } else if (proc.successCount == 0 && (fieldName.equals(PaymentFileFieldFormat.PAYEE_ADDRESS_LINE_1)
                || fieldName.equals(PaymentFileFieldFormat.PAYEE_ADDRESS_LINE_2)
                || fieldName.equals(PaymentFileFieldFormat.PAYEE_ADDRESS_LINE_3)
                || fieldName.equals(PaymentFileFieldFormat.PAYEE_COUNTRY))) {
            Terms terms = (Terms) proc.trans.getComponentHandle("CustomerEnteredTerms");
            TermsParty firstTermsParty = (TermsParty) terms.getComponentHandle("FirstTermsParty");
            String firstTermsPartyFieldName = null;
            if (fieldName.equals(PaymentFileFieldFormat.PAYEE_ADDRESS_LINE_1)) {
                firstTermsPartyFieldName = "address_line_1";
            } else if (fieldName.equals(PaymentFileFieldFormat.PAYEE_ADDRESS_LINE_2)) {
                firstTermsPartyFieldName = "address_line_2";
            } else if (fieldName.equals(PaymentFileFieldFormat.PAYEE_ADDRESS_LINE_3)) {
                firstTermsPartyFieldName = "address_line_3";
            } else if (fieldName.equals(PaymentFileFieldFormat.PAYEE_COUNTRY)) {
                firstTermsPartyFieldName = "address_country";
            }
            firstTermsParty.setAttribute(firstTermsPartyFieldName, fieldValue);
        } else if (proc.successCount == 0 && fieldName.equals(PaymentFileFieldFormat.COLUMN_CONFIDENTIAL_INDICATOR)) {
            Terms terms = (Terms) proc.trans.getComponentHandle("CustomerEnteredTerms");
            if (TradePortalConstants.INDICATOR_YES.equals(fieldValue)) {
                terms.setAttribute("confidential_indicator", TradePortalConstants.CONF_IND_FRM_TMPLT);
            } else {
                terms.setAttribute("confidential_indicator", "");
            }
            return true;
        } else if (proc.successCount == 0 && fieldName.equals(PaymentFileFieldFormat.COLUMN_INDIVIDUAL_ACCT_ENTRIES)) {
            Terms terms = (Terms) proc.trans.getComponentHandle("CustomerEnteredTerms");
            if (TradePortalConstants.INDICATOR_YES.equals(fieldValue)) {
                terms.setAttribute("individual_debit_ind", TradePortalConstants.INDICATOR_YES);
            } else {
                terms.setAttribute("individual_debit_ind", TradePortalConstants.INDICATOR_NO);
            }
            return true;
        } else if (proc.successCount == 0 && fieldName.equals(PaymentFileFieldFormat.COLUMN_CUSTOMER_FILE_REF)) {
            proc.paymentUpload.setAttribute("customer_file_ref", fieldValue);
            proc.trans.setAttribute("customer_file_ref", fieldValue);
            return true;
        } else if (fieldName.startsWith("first_intermediary_bank.")) {
            String firstIntermediaryBankOid = proc.domesticPayment.getAttribute("c_FirstIntermediaryBank");
            if (StringFunction.isBlank(firstIntermediaryBankOid)) {
                proc.domesticPayment.newComponent("FirstIntermediaryBank");
            }
            PaymentParty firstIntermediaryBank = (PaymentParty) proc.domesticPayment.getComponentHandle("FirstIntermediaryBank");

            fieldName = fieldName.substring("first_intermediary_bank.".length());
            if (fieldName.equals("city") || fieldName.equals("province")) {
                fieldName = PaymentFileFieldFormat.ADDRESS_LINE_3;
                String city = getValue(PaymentFileFieldFormat.FIRST_INTERMEDIARY_BANK_BRANCH_CITY, headerRecord, paymentRecord, null);
                if (city == null) {
                    city = "";
                }
                String province = getValue(PaymentFileFieldFormat.FIRST_INTERMEDIARY_BANK_BRANCH_PROVINCE, headerRecord, paymentRecord, null);
                if (province == null) {
                    province = "";
                }
                fieldValue = city + " " + province;
            }
            firstIntermediaryBank.setAttribute(fieldName, fieldValue);
            return true;

        } else if (fieldName.equals(PaymentFileFieldFormat.PAYEE_INVOICE_DETAILS)) {
            if (fieldValue != null && fieldValue.length() > 0) {
                proc.invoiceDetails = (InvoiceDetails) EJBObjectFactory.createClientEJB(proc.ejbServerLocation, "InvoiceDetails", proc.csdb);
                proc.invoiceDetails.newObject();
                proc.invoiceDetails.setAttribute("domestic_payment_oid", proc.domesticPayment.getAttribute("domestic_payment_oid"));
                proc.invoiceDetails.setAttribute("payee_invoice_details", fieldValue);
            }
            return true;
        } else if (fieldName.equals(PaymentFileFieldFormat.REPORTING_CODE_1) || fieldName.equals(PaymentFileFieldFormat.REPORTING_CODE_2)) {
            proc.inputDoc.setAttribute("/DomesticPayment/" + fieldName, fieldValue);
        } else if (fieldName.equals(PaymentFileFieldFormat.COLUMN_CITY) || fieldName.equals(PaymentFileFieldFormat.COLUMN_PROVINCE)) {
            if (!attributeNames.contains(PaymentFileFieldFormat.ADDRESS_LINE_3)) {
                fieldName = PaymentFileFieldFormat.ADDRESS_LINE_3;
                String city = getValue(PaymentFileFieldFormat.COLUMN_CITY, headerRecord, paymentRecord, null);
                if (city == null) {
                    city = "";
                }
                String province = getValue(PaymentFileFieldFormat.COLUMN_PROVINCE, headerRecord, paymentRecord, null);
                if (province == null) {
                    province = "";
                }
                fieldValue = city + " " + province;
                isAttribute = true;
            }
        } //
        //Rel9.0 CR-921
        //Save the fx contract number and fx contract rate to TERMS if available
        else if (proc.successCount == 0 && fieldName.equals(PaymentFileFieldFormat.COLUMN_FX_CONTRACT_NUMBER)) {
            Terms terms = (Terms) proc.trans.getComponentHandle("CustomerEnteredTerms");
            if (StringFunction.isNotBlank(fieldValue)) {
                terms.setAttribute("covered_by_fec_number", fieldValue);
            }
        } else if (proc.successCount == 0 && fieldName.equals(PaymentFileFieldFormat.COLUMN_FX_CONTRACT_RATE)) {
            Terms terms = (Terms) proc.trans.getComponentHandle("CustomerEnteredTerms");
            if (StringFunction.isNotBlank(fieldValue)) {
                terms.setAttribute("fec_rate", fieldValue);
            }
        }
            //end CR-921
        //
        // Set attribute on domestic_payment.  If a field is not an attribute on domestic payment, remove 
        // it from PaymentFileFieldFormat.columnNameToFieldNameMap 
        if (fieldValue != null && fieldValue.length() > 0 && isAttribute) {
            attributeNames.add(fieldName);
            attributeValues.add(fieldValue);
        }

        return true;
    }

    // Will be overwritten by ABA & PNG
    protected String[] parseTrailerLine(String trailerLine) {
        return null;
    }

    protected static String getDebitAccountOid(String accountNumber, PaymentFileProcessor proc)
            throws RemoteException, AmsException, RemoveException {

        StringBuilder accountSQLStr = new StringBuilder();
        accountSQLStr.append("select account_oid, available_for_domestic_pymts, deactivate_indicator, authorized_account_oid from account, user_authorized_acct where account_number = ? ")
                .append(" and p_owner_oid = ?")
                .append(" and account.account_oid = user_authorized_acct.a_account_oid (+) and user_authorized_acct.p_user_oid (+) = ? ")
                .append(" order by deactivate_indicator");
        Object sqlParams[] = new Object[3];
        sqlParams[0] = accountNumber;
        sqlParams[1] = proc.ownerOrgOid;
        sqlParams[2] = proc.userOid;
        DocumentHandler acctResultSet = DatabaseQueryBean.getXmlResultSet(accountSQLStr.toString(), false, sqlParams);
        if (acctResultSet == null || StringFunction.isBlank(acctResultSet.getAttribute("/ResultSetRecord(0)/ACCOUNT_OID"))) {
            logError(proc.lineNumberForLogging, proc.beneName, "Invalid Debit Account '" + accountNumber + "'.", ERROR_SUB, proc);
            return null;
        } else if ("Y".equals(acctResultSet.getAttribute("/ResultSetRecord(0)/DEACTIVATE_INDICATOR"))) {
            logError(proc.lineNumberForLogging, proc.beneName, "Account '" + accountNumber + "' is inactive.", ERROR_SUB, proc);
            return null;
        } else if (StringFunction.isBlank(acctResultSet.getAttribute("/ResultSetRecord(0)/AUTHORIZED_ACCOUNT_OID")) && !TradePortalConstants.ADMIN.equalsIgnoreCase(proc.isAdmin)) {
            logError(proc.lineNumberForLogging, proc.beneName, "Account '" + accountNumber + "' is not valid for debit for this user.", ERROR_SUB, proc);
            return null;
        } else if (!"Y".equals(acctResultSet.getAttribute("/ResultSetRecord(0)/AVAILABLE_FOR_DOMESTIC_PYMTS"))) {
            logError(proc.lineNumberForLogging, proc.beneName, "Account '" + accountNumber + "' is not available for Payments Transactions.", ERROR_SUB, proc);
            return null;
        }
        return acctResultSet.getAttribute("/ResultSetRecord(0)/ACCOUNT_OID");
    }

    /**
     * Log an error.
     *
     * @param lineNumber
     * @param beneInfo
     * @param errorCode
     * @param substitutionValues
     * @param proc
     * @throws RemoteException
     * @throws AmsException
     * @throws RemoveException Lo
     */
    protected static void logError(int lineNumber, String beneInfo, String errorCode, String[] substitutionValues, PaymentFileProcessor proc)
            throws RemoteException, AmsException, RemoveException {
        if (proc.paymentUpload != null) {
                // payment_file_upload entry has been created.  We are processing on Payment UPload agent.  
            // We will log error to payment_upload_error_log table.
            String errorMsg = proc.errorMgr.getIssuedError(null, errorCode, substitutionValues, null).getErrorText();
            String paymentUploadOid = proc.paymentUpload.getAttribute("payment_file_upload_oid");
            PaymentUploadErrorLog errorLog = (PaymentUploadErrorLog) EJBObjectFactory.createClientEJB(proc.ejbServerLocation, "PaymentUploadErrorLog", proc.csdb);
            errorLog.newObject();
            errorLog.setAttribute("line_number", String.valueOf(lineNumber));
            errorLog.setAttribute("payment_file_upload_oid", paymentUploadOid);
            errorLog.setAttribute("beneficiary_info", beneInfo);
            errorLog.setAttribute("error_msg", errorMsg);
            errorLog.save();
            errorLog.remove();
        } else if (proc.errorMgr != null) {
                // payment_file_upload entry has not been created.  We are still uploading the file on PaymentFileUploadServlet.//
            // We will log error to the transient error manager and display to the user.
            proc.errorMgr.issueError(TradePortalConstants.ERR_CAT_1, errorCode, substitutionValues);
        }
    }

    /**
     * Get errors from business object and log them.
     *
     * @param lineNumber
     * @param beneInfo
     * @param obj
     * @param proc
     * @throws RemoteException
     * @throws AmsException
     * @throws RemoveException
     */
    protected static void logError(int lineNumber, String beneInfo, BusinessObject obj, PaymentFileProcessor proc)
            throws RemoteException, AmsException, RemoveException {

        for (IssuedError errorObject  : obj.getIssuedErrors()) {
            logError(lineNumber, beneInfo, errorObject.getErrorText(), null, proc);
        }
    }

    /**
     * Get value for a field.
     *
     * @param fieldName
     * @param headerRecord
     * @param paymentRecord
     * @param trailerRecord
     * @return
     */
    protected String getValue(String fieldName, String[] headerRecord, String[] paymentRecord, String[] trailerRecord) {
        PaymentFileFieldFormat field = this.fieldNameToFieldMap.get(fieldName);
        String value = null;
        if (field != null && field.lineType.equals(PaymentFileFieldFormat.LINE_TYPE_HEADER) && headerRecord != null && headerRecord.length > field.order) {
            value = headerRecord[field.order];
        } else if (field != null && field.lineType.equals(PaymentFileFieldFormat.LINE_TYPE_PAYMENT) && paymentRecord != null && paymentRecord.length > field.order) {
            value = paymentRecord[field.order];
        } else if (field != null && field.lineType.equals(PaymentFileFieldFormat.LINE_TYPE_TRAILER) && trailerRecord != null && trailerRecord.length > field.order) {
            value = trailerRecord[field.order];
        }
        return value;
    }

    /**
     * Read one line from the input. If there is an output stream (in the case
     * this is called during the upload), write to the output stream. When the
     * file is upload, the input stream is the HTTP input and the output stream
     * is the temp file. When the file is validated by the agent, the input
     * stream is the temp file. There is no output stream.
     *
     * @param reader
     * @param writer
     * @return
     * @throws IOException
     */
    String readLine(BufferedReader reader, BufferedWriter writer) throws IOException {
        String sLine = reader.readLine();
        if (sLine != null && writer != null) {
            writer.write(sLine);
            writer.newLine();
        }
        return sLine;
    }

    public static boolean validatePaymentCurrencyCountry(PaymentFileProcessor proc, String paymentMethod, String paymentCurrency, String debitAccountNo) throws AmsException {
        StringBuilder whereClause = new StringBuilder();

        String debitAccountBankCountryCode = null;
        String debitAccountOid = null;

        String accountSqlStr = "select account_oid, currency from account where account_number = ?";         // NSX - 07/01/10 
        DocumentHandler acctResultSet = DatabaseQueryBean.getXmlResultSet(accountSqlStr, false, new Object[]{debitAccountNo.trim()});
        if (acctResultSet != null && StringFunction.isNotBlank(acctResultSet.getAttribute("/ResultSetRecord/ACCOUNT_OID"))) {
            debitAccountOid = acctResultSet.getAttribute("/ResultSetRecord/ACCOUNT_OID");
            if (StringFunction.isBlank(paymentCurrency)) {
                paymentCurrency = acctResultSet.getAttribute("/ResultSetRecord/CURRENCY");
            }

        } else {
            return true; // returning true to indicate invalid debit account. Error will be logged by debit account logic.
        }

        try {
            debitAccountBankCountryCode = PaymentFileWithInvoiceDetails.getBankBranchCountryForAccount(debitAccountOid);
        } catch (RemoteException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        List<Object> sqlParams = new ArrayList<Object>();
        whereClause.append(" PAYMENT_METHOD = ? AND ");
        sqlParams.add(paymentMethod);
        if (StringFunction.isNotBlank(debitAccountBankCountryCode)) {
            whereClause.append("COUNTRY = ? AND ");
            sqlParams.add(debitAccountBankCountryCode);
        }
        whereClause.append("CURRENCY = ? ");
        sqlParams.add(paymentCurrency);

        StringBuffer selectOffsetdays = (new StringBuffer("select offset_days from PAYMENT_METHOD_VALIDATION where")).append(whereClause);

        DocumentHandler paymentMethodRec = DatabaseQueryBean.getXmlResultSet(selectOffsetdays.toString(), false, sqlParams);
        boolean invalidFormat = false;
        if (paymentMethodRec == null) {
            String[] substitutionValue = {"", "", ""};
            substitutionValue[0] = paymentMethod;
            substitutionValue[1] = paymentCurrency;
            substitutionValue[2] = debitAccountBankCountryCode;
            proc.errorMgr.issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.INVALID_PAYMENT_METHOD_RULE_COMBINATION,
                    substitutionValue);
            invalidFormat = true;
        }
        //manohar - SBUL040548203 - 04/14/2011 -  end

        return !invalidFormat;
    }

    protected List getMandatoryFields(String paymentMethod) {
        List returnArrayList = new ArrayList();
        if (StringFunction.isNotBlank(paymentMethod)) {
            returnArrayList.addAll(PaymentFileFieldFormat.allReqfields);
            if (TradePortalConstants.PAYMENT_METHOD_CBFT.equals(paymentMethod)) {
                returnArrayList.addAll(PaymentFileFieldFormat.cbftReqFields);
            }
            if (!(TradePortalConstants.PAYMENT_METHOD_BCHK.equals(paymentMethod) || TradePortalConstants.PAYMENT_METHOD_CCHK.equals(paymentMethod))) {
                returnArrayList.addAll(PaymentFileFieldFormat.othrReqFields);
            }

        }
        return returnArrayList;

    }

}
