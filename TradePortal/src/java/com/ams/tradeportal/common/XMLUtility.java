package com.ams.tradeportal.common;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringReader;

import com.ams.tradeportal.agents.AgentDOMCount;

/**
 *     Copyright  © 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class XMLUtility {
private static final Logger LOG = LoggerFactory.getLogger(XMLUtility.class);

    /*
     * Reads given file and returns the contents as string 
     */
	public static String readFromFile (String fullPathfileName) {
	    StringBuilder fileText = new StringBuilder();
	    int counter = 0;
	    try(FileReader fReader = new FileReader(fullPathfileName);
	        BufferedReader bReader  = new BufferedReader(fReader)){
	        
	        String line;
	        while ((line = bReader.readLine()) != null) {
	                fileText.append(line);
	                counter = counter + 1;
	        }
	       
	    }
	    catch(IOException e){
	        e.printStackTrace();
	    }
	    return fileText.toString();

	}

	
	 /*  This method validates a string consisting of dtd and xml. This method is practical
    and can be used in real applications. Original classes from xercesSamples.jar:
    DOMCount, DOMParser, DOMParserWrapper were modified to AgentDOMCount, AgentDOMParser,
    and AgentDOMParserWrapper. These new classes are part of com.ams.tradeportal.agents
    packages. This method ultimately utilizes:
    org.apache.xerces.parsers.DOMParser.parse(InputSource inputSource)

    Additional info: "The SAX parser will use the InputSource object to determine how to read XML input.
    If there is a character stream available, the parser will read that stream directly;
    if not, the parser will use a byte stream, if available;
    if neither a character stream nor a byte stream is available,
    the parser will attempt to open a URI connection to the resource identified by the system identifier."
    */
	public static boolean validateXMLAgainstDTDFully(String dtdPlusXmlString, String agentID){

	    java.io.Reader characterStream = new StringReader(dtdPlusXmlString);
	    org.xml.sax.InputSource inputSource = new org.xml.sax.InputSource(characterStream);
	    boolean isXmlValidAgainstDtd = AgentDOMCount.count(inputSource, agentID);
	    return isXmlValidAgainstDtd;

	}
	

}
