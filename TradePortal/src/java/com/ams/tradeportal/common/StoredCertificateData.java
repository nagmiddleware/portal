package com.ams.tradeportal.common;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.security.*;

import com.amsinc.ecsg.util.*;

import java.util.*;

import org.bouncycastle.jce.provider.*;

/**
 * This class stores information about certificates that are
 * used as part of the user identification process.   When the
 * client banks create links to the Trade Portal on their web
 * sites, they encrypt information using the Trade Portal's public
 * key and they sign information using the bank's private key.  
 * The Trade Portal must use the other parts of those key-pairs
 * (Trade Portal private key, bank public key) to decipher the
 * encrypted information.  
 *
 *
 */
public class StoredCertificateData
 {
private static final Logger LOG = LoggerFactory.getLogger(StoredCertificateData.class);
    private static PropertyResourceBundle portalProperties;

    // Stores all keys - indexed by the organization ID (usually the
    // three letter organization code or "portal")
    private static Hashtable publicKeys;
    private static Hashtable privateKeys;
  
    // Constant for key to use to mark the Trade Portal's keys   
    private static String TRADE_PORTAL_CERTS = "portal";

    // The directory where the cert files are located
    private static String configFileDir;
   
    static 
     {
       try
        {        	
                // Add BouncyCastle as the primary Security Provider
               Security.addProvider(new BouncyCastleProvider());

	       publicKeys  = new Hashtable(10);
	       privateKeys = new Hashtable(2);

	       portalProperties =
	         (PropertyResourceBundle)PropertyResourceBundle.getBundle ("TradePortal");

               configFileDir = portalProperties.getString("configFileDir");
        }
       catch(Exception e)
        {
               LOG.info("Error in initializing StoredCertificateData");
        }
     }

    /**
     * Clears out the hashtables so that certs will be read again from files
     * instead of from the cached files
     */
    public static String update()
     {
        LOG.info("Updating cached Stored Certificate Data");

        synchronized(publicKeys)
         {
            publicKeys.clear();
         }

        synchronized(privateKeys)
         {
            privateKeys.clear();
         }
        return "Done";
     }

    /**
     * Returns a byte array that is the Trade Portal's public key
     *
     * @return PublicKey the public key of the Trade Portal
     */
    public static PublicKey getPortalPublicKeyFromFile()
     {
         return getPublicKeyFromFile(TRADE_PORTAL_CERTS);
     }

    /**
     * Returns a byte array that is the Trade Portal's private key
     *
     * @return PrivateKey the private key of the Trade Portal
     */
    public static PrivateKey getPortalPrivateKeyFromFile()
     {
         return getPrivateKeyFromFile(TRADE_PORTAL_CERTS);
     }

    /**
     * Returns a byte array that is public key of the organization
     * whose code is passed in.  Public keys are read from 
     * a certificate.
     *
     * @param organizationIdentifier -  three letter organization code or or TRADE_PORTAL
     * @return byte[] the private key of the Trade Portal
     */
    public static PublicKey getPublicKeyFromFile(String organizationIdentifier)
     {
        // Attempt to get the key from the already accessed keys
        PublicKey publicKey = (PublicKey) publicKeys.get(organizationIdentifier);
   
        if(publicKey != null)
         {
            // If we've already found the key, no need to look it up.
            return publicKey;
         }
        else
         {
            // Determine the location of the certificate file
            // For example, for organization XYZ, XYZ-cert.pem
            String certificateFilePath = configFileDir + "/" + organizationIdentifier+"-cert.pem";

            // Read in the certificate from the PEM file and get the public key from it
            publicKey = EncryptDecrypt.getPublicKeyFromCertificate(certificateFilePath);
         
            if(publicKey != null)
             {
                // Place it into the hashtable for retrieval later
                publicKeys.put(organizationIdentifier, publicKey);
             }

            return publicKey;
         }
     }


    /**
     * Returns a byte array that is private key of the organization
     * whose code is passed in.  Private keys are read directly from
     * a file the first time.
     *
     * @param organizationIdentifier -  three letter organization code or TRADE_PORTAL
     * @return PrivateKey the private key of the Trade Portal
     */
    public static PrivateKey getPrivateKeyFromFile(String organizationIdentifier)
     {
        // Attempt to get the key from the already accessed keys
        PrivateKey privateKey = (PrivateKey) privateKeys.get(organizationIdentifier);
   
        if(privateKey != null)
         {
            // If we've already found the key, no need to look it up
            return privateKey;
         }
        else
         {          
            // The format must be PEM with no password protection

            // First, determine the path to the private key PEM file
            // For example, for organization XYZ, XYZ-priv-key.pem
            String privateKeyFilePath = configFileDir + "/" + organizationIdentifier+"-priv-key.pem";
           
            privateKey = EncryptDecrypt.getPrivateKeyFromFile(privateKeyFilePath);
            
            if(privateKey != null)
             {
                // Place it into the hashtable for retrieval later
                privateKeys.put(organizationIdentifier, privateKey);
             }

            return privateKey;
         }
     }

 }