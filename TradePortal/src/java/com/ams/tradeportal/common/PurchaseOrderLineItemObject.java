package com.ams.tradeportal.common;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.util.StringFunction;

/**
 * User: rizwan.kazi Date: 3/9/12 Time: 12:22 PM Rel 8.0 CR-707. POJO to hold
 * Purchase Order Line Item values.
 */
public class PurchaseOrderLineItemObject {
private static final Logger LOG = LoggerFactory.getLogger(PurchaseOrderLineItemObject.class);

    private List errList = new ArrayList();
    private boolean isValid = true;
    private Map<String, Object> fields = new HashMap<>();

    public PurchaseOrderLineItemObject() {

    }

    public PurchaseOrderLineItemObject(String inLine) {
        //TODO set fields values;
    }

    public String getLine_item_num() {
        return (String) fields.get("line_item_num");
    }

    public BigDecimal getUnit_price() {
        String unitPrice = (String) fields.get("unit_price");
        return StringFunction.isBlank(unitPrice) ? null : new BigDecimal(unitPrice);
    }

    public BigDecimal getQuantity() {
        String qty = (String) fields.get("quantity");
        return StringFunction.isBlank(qty) ? null : new BigDecimal(qty);
    }

    public boolean isValid() {
        return isValid;
    }

    public void setValid(boolean valid) {
        isValid = valid;
    }

    public List getErrList() {
        return errList;
    }

    public Map getFields() {
        return fields;
    }

    public void setAttribute(String field, String value) {
        fields.put(field, value);
    }

    /**
     * This method processes a Purchase Order Line Item Object. It maps all the
     * passed in values to appropriate fields on the Object and while doing so
     * it validates the values also. If any errors found then it logs these
     * errros and marks itself invalid.
     *
     * @param liFieldNamesList
     * @param record
     * @param purchaseOrderUtility
     * @param lineNum
     */
    public void processLineItem(List<String> liFieldNamesList, String[] record, PurchaseOrderUtility purchaseOrderUtility, int lineNum) {
        purchaseOrderUtility.setCurrentLineNum(lineNum);
        fields = purchaseOrderUtility.setFieldValues(liFieldNamesList, record, lineNum, this);

    }
}
