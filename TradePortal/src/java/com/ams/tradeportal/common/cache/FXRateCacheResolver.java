/**
 *     Copyright  � 2003
 *     American Management Systems, Incorporated
 *     All rights reserved
 *
 *OSCache Copyright (c) 2001-2004 The OpenSymphony Group. All rights reserved.
 *
 *This product includes software developed by the OpenSymphony Group
 *(http://www.opensymphony.com/).
 *
 *_______________________________________________________________________________
 * */
package com.ams.tradeportal.common.cache;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.StringFunction;

/**
 * Resolver responsible for retrieving FXRate records by owner organization, currency code, and (optionally) rate group.
 */
public class FXRateCacheResolver implements CacheResolver {
	private static final Logger LOG = LoggerFactory.getLogger(FXRateCacheResolver.class);

	/**
	 * Retrieve a row from the FX_RATE table and return it as a DocumentHandler
	 *
	 * @param key
	 *            owner org oid, currency code, client bank oid (optionally), and fx rate group (optionally) as a concatenated key
	 *            delimited by pipes (|).
	 *
	 * @return Object the result of the retrieval
	 */

	@Override
	public Object get(String key) {
		DocumentHandler resultDoc = null;

		if (StringFunction.isBlank(key)) {
			// not found
			return null;
		}

		String corpOrgOid = null;
		String clientBankOid = null;
		String rateGroup = null;
		String currencyCode = null;
		String baseCurrencyCode = null;
		String[] keyParts = key.split("\\|");

		switch (keyParts.length) {
		case 4:
			clientBankOid = keyParts[2];
			rateGroup = keyParts[3];
			// fall-through
		case 2:
			corpOrgOid = keyParts[0];
			currencyCode = keyParts[1];
			break;
		default:
			// invalid key
			LOG.debug("Invalid key '" + key + "' sent to FXRateCacheResolver.");
			return null;
		}

		// BSL IR RSUL053050294 06/01/11 BEGIN
		if (StringFunction.isBlank(corpOrgOid) || StringFunction.isBlank(currencyCode)) {
			// required parameters missing
			return null;
		}

		if (StringFunction.isBlank(clientBankOid) && StringFunction.isBlank(rateGroup)) {
			String corpOrgQuery = "select A_CLIENT_BANK_OID, FX_RATE_GROUP, BASE_CURRENCY_CODE from corporate_org where organization_oid = ?";
			try {
				DocumentHandler rateGroupDoc = DatabaseQueryBean.getXmlResultSet(corpOrgQuery, true, new Object[] { corpOrgOid });
				if (rateGroupDoc != null) {
					clientBankOid = rateGroupDoc.getAttribute("/ResultSetRecord/A_CLIENT_BANK_OID");
					rateGroup = rateGroupDoc.getAttribute("/ResultSetRecord/FX_RATE_GROUP");
					baseCurrencyCode = rateGroupDoc.getAttribute("/ResultSetRecord/BASE_CURRENCY_CODE");
				}
			} catch (Exception e) {
				LOG.error("Exception from FXRate corporate org database retrieval.", e);
			}
		}

		List<Object> sqlParams = new ArrayList<Object>();
		StringBuilder selectSql = new StringBuilder("select fx_rate.* from fx_rate");
		StringBuilder whereCurrencySql = new StringBuilder(" where fx_rate.currency_code = ? ");
		sqlParams.add(currencyCode);

		// first lookup - client bank oid, fx rate group, currency
		if (StringFunction.isNotBlank(clientBankOid) && StringFunction.isNotBlank(rateGroup)) {
			StringBuilder whereRateGroupSql = new StringBuilder(whereCurrencySql);
			whereRateGroupSql.append(" and fx_rate.fx_rate_group = ? ");
			sqlParams.add(rateGroup);

			LOG.debug("Searching for FX Rates by Client Bank, FX Rate Group, and Currency.");
			resultDoc = getFXRatesResultSet(selectSql.toString(), whereRateGroupSql.toString(), sqlParams, clientBankOid);

			if (resultDoc != null && resultDoc.getFragment("/ResultSetRecord") != null) {
				return resultDoc;
			}
		}

		// second lookup - corporate organization oid, currency
		LOG.debug("Searching for FX Rates by Corporate Organization and Currency.");
		resultDoc = getFXRatesResultSet(selectSql.toString(), whereCurrencySql.toString(), sqlParams, corpOrgOid);

		if (resultDoc != null && resultDoc.getFragment("/ResultSetRecord") != null) {
			return resultDoc;
		}

		// third lookup - client bank oid, currency, base currency
		if (StringFunction.isNotBlank(clientBankOid)) {
			// add base currency to the query
			whereCurrencySql.append(" and fx_rate.base_currency_code = ");

			if (StringFunction.isNotBlank(baseCurrencyCode)) {
				// match base currency to previously retrieved value
				whereCurrencySql.append("?");
				sqlParams.add(baseCurrencyCode);
			} else {
				// dynamically match base currency to corporate_org
				selectSql.append(", corporate_org");

				whereCurrencySql.append("corporate_org.base_currency_code");
				whereCurrencySql.append(" and corporate_org.organization_oid = ? ");
				sqlParams.add(corpOrgOid);

			}

			LOG.debug("Searching for FX Rates by Client Bank, Base Currency, and Currency.");
			resultDoc = getFXRatesResultSet(selectSql.toString(), whereCurrencySql.toString(), sqlParams, clientBankOid);
		}
		return resultDoc;
	}

	private DocumentHandler getFXRatesResultSet(String selectSql, String whereSql, List<Object> paramList, String ownerOid) {
		StringBuilder sqlQuery = new StringBuilder(selectSql);
		sqlQuery.append(whereSql);
		sqlQuery.append(" and fx_rate.p_owner_org_oid = ? ");

		List<Object> sqlParams = new ArrayList<Object>();
		sqlParams.addAll(paramList);
		sqlParams.add(ownerOid);

		try {
			return DatabaseQueryBean.getXmlResultSet(sqlQuery.toString(), false, sqlParams);
		} catch (Exception e) {
			LOG.error("Exception from FXRate database retrieval.", e);
			return null;
		}
	}
}
