/**
 *     Copyright  � 2003
 *     American Management Systems, Incorporated
 *     All rights reserved
 *
 *OSCache Copyright (c) 2001-2004 The OpenSymphony Group. All rights reserved.
 *
 *This product includes software developed by the OpenSymphony Group
 *(http://www.opensymphony.com/).
 *
 *_______________________________________________________________________________
 * */

package com.ams.tradeportal.common.cache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import com.ams.tradeportal.common.Debug;
import com.opensymphony.oscache.base.Config;

public class TPCacheManager
{
private static final Logger LOG = LoggerFactory.getLogger(TPCacheManager.class);

	/**
	 * Map that holds instances of each created cache, keyed by cache name.
	 */
	protected Map cacheMap = Collections.synchronizedMap(new HashMap());

	//	type constants
	static public final String CACHE_TYPE_LRU			= "LRU";
	static public final String CACHE_TYPE_FIFO			= "FIFO";
	static public final String CACHE_TYPE_UNLIMITED		= "Unlimited";

	//other constants
	public static final int UNLIMITED_REFRESH_PERIOD	= -1;

	//OS Cache properties
	public Config osCacheConfig = new Config();

	/**
	 * The singleton instance of the TPCacheManager
	 */
	private static final TPCacheManager instance;

	/**
	 * Static initializer that will create the CacheManager instance
	 */
	static
	{
		instance = new TPCacheManager();
	}

    private TPCacheManager()
    {

    }

	/**
	 * Get the single instance of the CacheManager.
	 *
	 * @return CacheManager object
	 */
	static public TPCacheManager getInstance()
	{
		return instance;
	}

	/**
	 * Create an LRU cache.
	 * @param cacheName String name of the cache object.
	 *
	 * @return Cache new cache object
	 */
	public Cache createCache(String cacheName)
	{
		return createCache(cacheName, CACHE_TYPE_LRU);
	}
	/**
	 * Create a cache of a specified type
	 * @param cacheName String name of the cache
	 * @param cacheType type of Cache
	 *
	 * @return Cache new cache object
	 */

	public Cache createCache(String cacheName, String cacheType)
	{
		// see if the cache already exists. If not, initialize a new
		// by type
		Cache cache = (Cache)cacheMap.get(cacheName);
		if (cache == null)
		{

			// the cache does not exist - create it
			if (CACHE_TYPE_UNLIMITED.equals(cacheType))
			{
				cache = new UnlimitedCache(cacheName, osCacheConfig);
			}
			else if (CACHE_TYPE_FIFO.equals(cacheType))
			{
				cache = new FIFOCache(cacheName, osCacheConfig);
			}
			else
			{
				cache = new LRUCache(cacheName, osCacheConfig);
			}

			// make sure it hasn't been created while we were trying to create it...
			if (cacheMap.get(cacheName) == null)
			{
				cacheMap.put(cacheName, cache);
			}
			LOG.debug("added cache name = " + cacheName);
		}

		return cache;
	}

	/**
	 * Responsible for returning the reference to 
	 * a Cache identified by cacheName.  If that
	 * cache has not been initialized, this method will 
	 * invoke createCache to initialize a LRU Cache.
	 * @param cacheName String name of the cache
	 *
	 * @return Cache cache object
	 */

	public Cache getCache(String cacheName)
	{
		if (!(cacheMap.containsKey(cacheName))) {
			return createCache(cacheName);
		}
		return (Cache)cacheMap.get(cacheName);
	}
	/**
	 * Delete a cache from the manager
	 *
	 * @param cacheName
	 */
	public void deleteCache(String cacheName)
	{
		Cache cache = (Cache)cacheMap.get(cacheName);
		if (cache != null)
		{
			// remove all objects from the cache
			cache.flush();

			// and remove the cache from the manager
			cacheMap.remove(cacheName);
		}
	}
}
