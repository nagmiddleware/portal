/**
 * Copyright � 2003 American Management Systems, Incorporated All rights
 * reserved
 *
 * OSCache Copyright (c) 2001-2004 The OpenSymphony Group. All rights reserved.
 *
 * This product includes software developed by the OpenSymphony Group
 * (http://www.opensymphony.com/).
 *
 * _______________________________________________________________________________
 *
 */
package com.ams.tradeportal.common.cache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.util.StringFunction;
import com.opensymphony.oscache.base.AbstractCacheAdministrator;
import com.opensymphony.oscache.base.CacheEntry;
import com.opensymphony.oscache.base.Config;
import com.opensymphony.oscache.base.NeedsRefreshException;

abstract public class DefaultCache implements Cache {
private static final Logger LOG = LoggerFactory.getLogger(DefaultCache.class);

    private static final String DEFAULT_GROUP = "default";
    private static final String[] DEFAULT_GROUPS = {DEFAULT_GROUP};

    public static final String RESOLVER = ".resolver";
    public static final String CAPACITY = ".capacity";
    public static final String REFRESH_PERIOD = ".refreshperiod";

    private String cacheName;
    private com.opensymphony.oscache.base.Cache osCache;
    private CacheResolver resolver = null;
    private int refreshPeriod = CacheEntry.INDEFINITE_EXPIRY;

    private int capacity;

    /**
     * Instantiates osCache using passed in CacheConfiguration
     *
     * @param cacheName name of the cache
     * @param osCacheConfig cache properties
     */
    public DefaultCache(String cacheName, Config osCacheConfig) {
        setCacheName(cacheName);
        //load the appropriate cache resolver from the property file
        String resolverName = osCacheConfig.getProperty(cacheName + RESOLVER);
        if (StringFunction.isNotBlank(resolverName)) {
            setResolver(loadCacheResolver(resolverName));
        }

        //look for cache specific capacity, or use default
        String capacityValue = osCacheConfig.getProperty(cacheName + CAPACITY);
        if (StringFunction.isBlank(capacityValue)) {
            capacityValue = osCacheConfig.getProperty(AbstractCacheAdministrator.CACHE_CAPACITY_KEY);
        }

        setCapacity(Integer.parseInt(capacityValue));

        osCache = (com.opensymphony.oscache.base.Cache) createNativeCacheObject(osCacheConfig);

        //look for cache specific refresh period, or use default
        String refreshPeriodValue = osCacheConfig.getProperty(cacheName + REFRESH_PERIOD);
        if (StringFunction.isNotBlank(refreshPeriodValue)) {
            setRefreshPeriod(Integer.parseInt(refreshPeriodValue));
            LOG.debug("setting refresh period = " + getRefreshPeriod());
        }

    }

    /**
     * Create a native osCache instance.
     *
     * @param osCacheConfig cache properties
     */
    abstract Object createNativeCacheObject(Config osCacheConfig);

    /**
     * @return capacity the cache max size
     */
    public int getCapacity() {
        return capacity;
    }

    /**
     * @param key unique identifier to the cache entry
     * @param content the cached object
     */
    @Override
    public void put(String key, Object content) {
        LOG.debug("Putting object (" + content + ") into cache with key " + key);

        osCache.putInCache(key, content, DEFAULT_GROUPS);
    }

    /**
     * @param key unique identifier to the cache entry
     * @return content the cached object
     */
    @Override
    public Object get(String key) {
        return get(key, getRefreshPeriod());
    }

    /**
     * @param key unique identifier to the cache entry
     * @param refreshPeriod integer used by osCache to determine stale entries
     * @return content the cached object
     */
    @Override
    public Object get(String key, int refreshPeriod) {

        LOG.debug("Getting key " + key + " from cache.");

        if (StringFunction.isBlank(key)) {
            return null;
        }

        Object value = null;
        try {
            value = osCache.getFromCache(key, refreshPeriod);
        } catch (NeedsRefreshException e) {

            LOG.debug("Key " + key + " doesn't exist or is stale.");

            // the object either doesn't exist or is stale
            if (resolver != null) {

                LOG.debug("Getting object with key " + key + " from resolver");

                try {
                    value = resolver.get(key);
                } catch (Throwable t) {

                    //cancel the update
                    osCache.cancelUpdate(key);

                    //rethrow
                    throw new RuntimeException(t);
                }

                if (value != null) {
                    LOG.debug("Putting resolved object (" + value + ") with key " + key + " into cache.");

                    osCache.putInCache(key, value, DEFAULT_GROUPS);
                }
            }

			// the item was not found in the cache, or was stale.
            // if value is still null at this point, either we
            // don't have a resolver or it couldn't resolve the
            // item. In either case, a fresh item was never put
            // into the cache so we need to cancel the update -
            // otherwise other threads might hang.
            if (value == null) {
                osCache.cancelUpdate(key);
            }
        }

        LOG.debug("Object with key " + key + " from cache: " + value);

        return value;
    }

    /**
     * Removes the object identified by the unique identifier
     *
     * @param key unique identifier to the cache entry
     */
    @Override
    public void remove(String key) {

        LOG.debug("Flushing key " + key + " from cache.");

        osCache.flushEntry(key);
    }

    /**
     * remove all entries from this Cache
     */
    @Override
    public void flush() {
        LOG.debug("Flushing all entries from cache.");

        osCache.flushGroup(DEFAULT_GROUP);

    }

    /**
     * @return the implementation of OSCache
     */
    @Override
    public Object getImplementation() {
        return osCache;
    }

    /**
     * Sets the max capacity for this Cache
     *
     * @param size the cache max size
     */
    @Override
    public void setCapacity(int size) {
        capacity = size;
    }

    /**
     * Sets an instance of the CacheResolver class for this Cache
     *
     * @param resolver class
     */
    @Override
    public void setResolver(CacheResolver resolver) {
        this.resolver = resolver;
    }

    /**
     * gets an instance of the CacheResolver class for this Cache
     *
     * @return resolver class
     */
    @Override
    public CacheResolver getResolver() {
        return resolver;
    }

    /**
     *
     * @param refreshPeriod integer used by osCache to determine stale entries
     *
     */
    @Override
    public void setRefreshPeriod(int refreshPeriod) {
        this.refreshPeriod = refreshPeriod;
    }

    /**
     * @return cacheName the name that identifies this Cache
     */
    @Override
    public String getCacheName() {
        return cacheName;
    }

    @Override
    public void setCacheName(String name) {
        this.cacheName = name;
    }

    /**
     *
     * @return refreshPeriod integer used by osCache to determine stale entries
     *
     */
    @Override
    public int getRefreshPeriod() {
        return refreshPeriod;
    }

    /**
     * loads an instance of the CacheResolver class for this Cache, defined by
     * the resolver name
     *
     * @param resolverName string representing the resolver class name
     * @return resolver class
     */
    public CacheResolver loadCacheResolver(String resolverName) {

        CacheResolver r = null;
        try {
            // The class loader of the DefaultCache instance MUST be used
            // to instantiate an instance of the resolver.  This is because it is possible that the resolver class
            // resides withink a different class loader than the DefaultCache class.  It is assumed
            // that the resolver CAN be loaded from the class loader of the DefaultCache concrete instance.
            if (StringFunction.isNotBlank(resolverName)) {
                r = (CacheResolver) Class.forName(resolverName, true, this.getClass().getClassLoader()).newInstance();
            }
        } catch (Exception e) {
            LOG.info("Problem instantiating resolver: " + resolverName);
        }

        return r;

    }

    /**
     * returns the number of entries stored in this Cache instance. Note, stale
     * entries are stored in osCache, and the size includes those stale entries.
     *
     * @return size
     */
    @Override
    public int getSize() {
        return osCache.getSize();
    }

}
