/**
 *     Copyright  © 2003
 *     American Management Systems, Incorporated
 *     All rights reserved
 *
 *OSCache Copyright (c) 2001-2004 The OpenSymphony Group. All rights reserved.
 *
 *This product includes software developed by the OpenSymphony Group
 *(http://www.opensymphony.com/).
 *
 *_______________________________________________________________________________
 * */

package com.ams.tradeportal.common.cache;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.StringFunction;

/**
 * Resolver responsible for retrieving BankBranchRule records by a concatenated key of Bank_branch_code, payment_method and
 * address_country.
 */
public class BankBranchRuleCacheResolver implements CacheResolver {
	private static final Logger LOG = LoggerFactory.getLogger(BankBranchRuleCacheResolver.class);

	public static final String BANK_BRANCH_CODE = "BANK_BRANCH_CODE";
	public static final String PAYMENT_METHOD = "PAYMENT_METHOD";
	public static final String ADDRESS_COUNTRY = "ADDRESS_COUNTRY";

	/**
	 * Retrieve a row from the Payment_Party table and return it as a DocumentHandler
	 *
	 * @param key
	 *            concatenated string
	 *
	 * @return object the result of the retrieval
	 *
	 */
	@Override
	public Object get(String key) {

		DocumentHandler resultDoc = new DocumentHandler();

		if (StringFunction.isBlank(key)) {
			// not found
			return resultDoc;
		}

		HashMap keyValues = parseKeyForValue(key);
		List<Object> sqlParams = new ArrayList();

		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(getSQLString());
		sqlQuery.append("BANK_BRANCH_CODE = ? ");
		sqlParams.add((String) keyValues.get(BANK_BRANCH_CODE));

		if (keyValues.containsKey(PAYMENT_METHOD)) {
			sqlQuery.append(" AND PAYMENT_METHOD = ? ");
			sqlParams.add((String) keyValues.get(PAYMENT_METHOD));
		}

		if (keyValues.containsKey(ADDRESS_COUNTRY)) {
			sqlQuery.append(" AND ADDRESS_COUNTRY = ? ");
			sqlParams.add((String) keyValues.get(ADDRESS_COUNTRY));
		}

		try {

			resultDoc = (DocumentHandler) DatabaseQueryBean.getXmlResultSet(sqlQuery.toString(), false, sqlParams);
		} catch (Exception e) {
			LOG.error("Exception from BankBranchRule database retrieval", e);
		}

		return resultDoc;
	}

	/**
	 * Parse the concatenated key by the delimiter | in order to return the keyValue for each key field.
	 *
	 * @param key
	 *            concatenated field
	 *
	 * @return string value for the field name
	 *
	 */
	public HashMap parseKeyForValue(String key) {

		HashMap keyValues = new HashMap(3);

		StringTokenizer str = new StringTokenizer(key, "|");
		for (int i = 0; i < 3; i++) {
			String field = BANK_BRANCH_CODE;

			if (i == 1) {
				field = PAYMENT_METHOD;
			} else if (i == 2) {
				field = ADDRESS_COUNTRY;
			}

			if (str.hasMoreTokens()) {
				String value = str.nextToken();
				if (StringFunction.isNotBlank(value)) {
					keyValues.put(field, value);
				}
			}
		}
		return keyValues;
	}

	/**
	 * return the basic SQL String.
	 *
	 * @return string sql select string
	 *
	 */
	protected String getSQLString() {

		return "select count(BANK_BRANCH_CODE) AS BANK_BRANCH_RULE_COUNT from BANK_BRANCH_RULE where ";
	}

}
