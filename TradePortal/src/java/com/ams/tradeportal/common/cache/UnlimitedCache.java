/**
 *     Copyright  � 2003
 *     American Management Systems, Incorporated
 *     All rights reserved
 *
 *OSCache Copyright (c) 2001-2004 The OpenSymphony Group. All rights reserved.
 *
 *This product includes software developed by the OpenSymphony Group 
 *(http://www.opensymphony.com/).
 *
 *_______________________________________________________________________________
 * */

package com.ams.tradeportal.common.cache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.oscache.base.Config;
import com.opensymphony.oscache.base.AbstractCacheAdministrator;


public class UnlimitedCache extends DefaultCache
{
private static final Logger LOG = LoggerFactory.getLogger(UnlimitedCache.class);
 
	/**
     * Instantiate a new Cache instance of type Unlimited
     * 
     * @param cacheName name of the cache
     * @param osCacheConfig cache properties
     */
    public UnlimitedCache(String cacheName, Config osCacheConfig){
    	super(cacheName, osCacheConfig);
    }

    /**
     * Create a native osCache instance.
     * 
     * @param osCacheConfig cache properties
     */
    @Override
    Object createNativeCacheObject(Config osCacheConfig)
    {
		return new com.opensymphony.oscache.base.Cache(
                Boolean.parseBoolean(osCacheConfig.getProperty(AbstractCacheAdministrator.CACHE_MEMORY_KEY)),
                Boolean.parseBoolean(osCacheConfig.getProperty(AbstractCacheAdministrator.CACHE_DISK_UNLIMITED_KEY)),
				false,
                Boolean.parseBoolean(osCacheConfig.getProperty(AbstractCacheAdministrator.CACHE_BLOCKING_KEY)),
				"com.opensymphony.oscache.base.algorithm.UnlimitedCache",
		        getCapacity());
    }

}
