package com.ams.tradeportal.common;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This utility class is used to for validating email address.
 *
 * Copyright � 2001 American Management Systems, Incorporated All rights
 * reserved
 */
public class EmailValidator {
private static final Logger LOG = LoggerFactory.getLogger(EmailValidator.class);

	protected static final String DEF_EMAIL_LIST_PATTERN = "^[\\w!#$%&'*+/=?^`{}~-]+(?:\\.[\\w!#$%&'*+/=?^`{}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+(?:[A-Z]{2,4})(\\s*,\\s*[\\w!#$%&'*+/=?^`{}~-]+(?:\\.[\\w!#$%&'*+/=?^`{}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+(?:[A-Z]{2,4}))*$";
    protected static final String _EMAIL_LSIT_PATTERN = TradePortalConstants.getPropertyValue("TradePortal", "emailListPattern", DEF_EMAIL_LIST_PATTERN);
    protected static final Pattern _EMAIL_LIST_PATTERN_OBJ = Pattern.compile(_EMAIL_LSIT_PATTERN, Pattern.CASE_INSENSITIVE);

    public static void validateEmail(String email) {
        //TO-DO
    }

    /*
     * validates email list against the email list pattern.
     *
     * @param emailList
     * @return
     */
    public static boolean validateEmailList(String emailList) {
        Matcher matcher = _EMAIL_LIST_PATTERN_OBJ.matcher(emailList);
        return matcher.matches();
    }

}
