package com.ams.tradeportal.common;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.PropertyResourceBundle;

import com.ams.util.ConvenienceServices;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.util.JPylonProperties;
import com.amsinc.ecsg.util.KeyValueXmlParser;
     
/**
 * This class provides static methods to access the security rules of the
 * Trade Portal.  The security rules are stored in an XML key/value file.
 *
 *     Copyright  © 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class SecurityRules
 {
private static final Logger LOG = LoggerFactory.getLogger(SecurityRules.class);
    private static Map<String, String> securityRules;
    private static final String configFileDir;
    
    static
     {
       PropertyResourceBundle portalProperties =
	         (PropertyResourceBundle)PropertyResourceBundle.getBundle ("TradePortal");

       // Determine the directory where the security config XML file is
       configFileDir = portalProperties.getString("configFileDir");

       // Read in data from the security config XML file
       securityRules = KeyValueXmlParser.parse(configFileDir+"/TradePortalSecurityConfig.xml");           
     }

     
    /**
     * Rereads the data from the XMl file.
     */
    public static String update()
     {
        LOG.info("Updating cached security rules");

        synchronized(securityRules)
         {
            securityRules = KeyValueXmlParser.parse(configFileDir+"/TradePortalSecurityConfig.xml");  
         }
        return "Done";
     }

    /**
     * Get the number of login attempts a user can make before being 
     * redirected to another page
     * 
     * @return the value from the security properties file
     */
    public static int getLoginAttemptsBeforeBeingRedirected()
     {
        return Integer.parseInt(securityRules.get("loginAttemptsBeforeBeingRedirected"));  
     }

    //BSL CR 749 05/16/2012 Rel 8.0 BEGIN
    /**
     * Returns the max session timeout period stored in the security
     * properties file.
     * 
     * @return the value from the security properties file
     */
    public static int getMaxSessionTimeOutPeriodInSeconds()
     {
    	try {
            return Integer.parseInt(securityRules.get("maxSessionTimeoutPeriodInSeconds"));
    	}
    	catch (Exception e) {
    		// Default to 24 hours if there's a problem
    		return 86400;
    	}
     }
    //BSL CR 749 05/16/2012 Rel 8.0 END

    //RKAZI CMA SESSION SYNC REL 9.1 10/23/2014 - Start
    /**
     * Returns the auto extend key stored in the security properties file
     * 
     * @return the value from the security properties file
     */     
    public static String getAutoExtendSession()
     {
        Object autoExtendSession =securityRules.get("autoExtendSession");
        if (autoExtendSession == null){
        	autoExtendSession = "N";
        }
        
        return (String)autoExtendSession;  
         
     }
    //RKAZI CMA SESSION SYNC REL 9.1 10/23/2014 - End
    
    /**
     * Returns the timeout period stored in the security properties file
     * or the overridden value in jPylon properties
     * 
     * @return the value from the security properties file
     */     
    public static int getTimeOutPeriodInSeconds()
     {
        String timeoutPeriodOverride;

        try 
         {
  	     JPylonProperties jPylonProperties = JPylonProperties.getInstance();

  	     timeoutPeriodOverride = jPylonProperties.getString("timeoutPeriodOverride");
         }
        catch(AmsException amse)
         {
             timeoutPeriodOverride = null;
         }
 
        if((timeoutPeriodOverride == null) || (timeoutPeriodOverride.equals("")))
               return Integer.parseInt(securityRules.get("timeoutPeriodInSeconds"));  
        else
               return Integer.parseInt(timeoutPeriodOverride);  
     }
    
    /**
     * Returns the timeout warning period stored in the security properties file
     * 
     * @return the value from the security properties file
     */     
    public static int getTimeoutWarningPeriod()
     {
        String timeoutWarningPeriod = null;
 
        try
         {
            timeoutWarningPeriod = securityRules.get("timeoutWarningMinutes");
         }
        catch(Exception e)
         {
            // Default to 2 if there's a problem
            return 2;
         }

        if(timeoutWarningPeriod == null)
            return 2;

        return Integer.parseInt(timeoutWarningPeriod);
     }


    /**
     * Get the lowest value to which the "minimum password length"
     * can be set for a corporate organization user
     * 
     * @return the value from the security properties file
     */
    public static int getCorporatePasswordMinimumLengthLimit()
     {
        return Integer.parseInt(securityRules.get("Corporate.PasswordMinimumLengthLimit"));
     }
     
    /**
     * Get the highest value to which the "change password days"
     * can be set for a corporate organization user
     * 
     * @return the value from the security properties file
     */     
    public static int getCorporateChangePasswordDaysLimit()
     {        
        return Integer.parseInt(securityRules.get("Corporate.ChangePasswordDaysLimit"));
     }

    /**
     * Get the highest value to which the "number of failed logins"
     * can be set for a corporate organization user
     * 
     * @return the value from the security properties file
     */
    public static int getCorporateFailedLoginAttemptsLimit()
     {        
        return Integer.parseInt(securityRules.get("Corporate.FailedLoginAttemptsLimit"));
     }
       
	/**
	 * Get the highest value to which the password history checking
	 * can be set
	 * 
	 * @return the value from the security properties file
	 */
	public static int getCorporatePasswordHistoryCountLimit()
	 {        
		String returnValue = securityRules.get("Corporate.MaxPasswordHistory");
        
		if(!ConvenienceServices.isBlank(returnValue))
			return Integer.parseInt(returnValue);
		else
			// Default to 99
			return 99;	 	
	 }
              
       
    /**
     * Get the lowest value to which the "minimum password length"
     * can be set for a bank or bank organization group user.
     *
     * @return the value from the security properties file
     */             
    public static int getBankPasswordMinimumLengthLimit()
     {        
        return Integer.parseInt(securityRules.get("Bank.PasswordMinimumLengthLimit"));
     }

    /**
     * Get the highest value to which the "change password days"
     * can be set for a bank or bank organization group user.
     * 
     * @return the value from the security properties file
     */             
    public static int getBankChangePasswordDaysLimit()
     {        
        return Integer.parseInt(securityRules.get("Bank.ChangePasswordDaysLimit"));
     }
       
    /**
     * Get the highest value to which the "number of failed logins"
     * can be set for a bank or bank organization group user.
     * 
     * @return the value from the security properties file
     */             
    public static int getBankFailedLoginAttemptsLimit()
     {        
        return Integer.parseInt(securityRules.get("Bank.FailedLoginAttemptsLimit"));
     }

	/**
	 * Get the highest value to which the password history checking
	 * can be set
	 * 
	 * @return the value from the security properties file
	 */
	public static int getBankPasswordHistoryCountLimit()
	 {        
        String returnValue = securityRules.get("Bank.MaxPasswordHistory");
        
        if(!ConvenienceServices.isBlank(returnValue))
            return Integer.parseInt(returnValue);
        else
            // Default to 99
            return 99;
	 }

    /**
     * Get the minimum password length for a Proponix user.
     * 
     * @return the value from the security properties file
     */             
    public static int getProponixPasswordMinimumLengthLimit()
     {        
        return Integer.parseInt(securityRules.get("Proponix.PasswordMinimumLength"));
     }
       
    /**
     * Get the number of days before a Proponix user must change their password.
     * 
     * @return the value from the security properties file
     */             
    public static int getProponixChangePasswordDaysLimit()
     {        
        return Integer.parseInt(securityRules.get("Proponix.MaximumChangePasswordDays"));
     }

    /**
     * Get the number of times a Proponix user can log in before being locked out.
     * 
     * @return the value from the security properties file
     */             
    public static int getProponixFailedLoginAttemptsLimit()
     {        
        return Integer.parseInt(securityRules.get("Proponix.MaximumFailedLoginAttempts"));     
     }
    

    /**
     * Get the certificate signon date tolerance (in minutes).
     * 
     * @return the value from the security properties file
     */             
    public static int getCertificateSignonDateTolerance()
     {        
        return Integer.parseInt(securityRules.get("CertificateSignonDateTolerance"));     
     }
 
	/**
	 * Get the flag indicating whether or not stricter password
	 * rules are required for ASP level users.
	 * 
	 * @return the value from the security properties file
	 */ 
    public static String getProponixRequireUpperLowerDigit()
     {
        String returnValue = securityRules.get("Proponix.PasswordsRequireUpperLowerDigit");
        
        if(returnValue != null)
            return returnValue;
        else
            return TradePortalConstants.INDICATOR_NO;    
     }

	/**
	 * Get the number of passwords in the password history to check
	 * against.   Returns -1 if no checking should occur.
	 * 
	 * @return the value from the security properties file
	 */ 
	public static String getPasswordHistoryCount()
	 {
		String returnValue =  securityRules.get("Proponix.PasswordHistoryCount"); 
		  
		if(returnValue != null)
			return returnValue;
		else
			return "0";    
     	 
	 }
    /**
     * Get the encrypted password used for access to internal utility.
     * 
     * @return the encyrpted password
     */             
    public static String getInternalPassword()
     {        
        return securityRules.get("internalPagesPassword");     
     }


    /**
     * Get the encrypted password salt used for access to internal utility.
     * 
     * @return the encyrpted password salt
     */            
    public static String getInternalPasswordSalt()
     {        
        return securityRules.get("internalPagesPasswordSalt");     
     }    

    /**
     * Get the ServerChallengeString used as a parameter to the SmartCard applet calls.
     * 
     * @return the ServerChallengeString
     */            
    public static String getSmartCardServerChallengeString()
     {        
        return securityRules.get("smartCardServerChallengeString");     
     }    
    
    /**
     * Property indicating whether to use the default ServerChallengeString, or to generate
     * a random ServerChallengeString
     * 
     * @return the ServerChallengeString
     */            
    public static boolean useDefaultServerChallengeString()
     {        
    	String returnValue = securityRules.get("useDefaultServerChallengeString"); 
    	if(returnValue != null) {
    		if (returnValue.equalsIgnoreCase(TradePortalConstants.INDICATOR_YES)) {
    		   return true;
    		}
    	}
        return false;
     }    
 }