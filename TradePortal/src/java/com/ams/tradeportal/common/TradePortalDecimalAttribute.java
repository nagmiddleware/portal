package com.ams.tradeportal.common;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;

/**
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class TradePortalDecimalAttribute extends DecimalAttribute
 {
private static final Logger LOG = LoggerFactory.getLogger(TradePortalDecimalAttribute.class);
	public TradePortalDecimalAttribute ()
	 {
     }

	public TradePortalDecimalAttribute (String name, String physicalName)
	 {
	    super(name, physicalName);
     }

	public TradePortalDecimalAttribute (String name, String physicalName, boolean required, String alias)
	 {
	    super(name, physicalName, required, alias);
	 }
  
   
     
 }