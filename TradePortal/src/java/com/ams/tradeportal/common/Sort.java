package com.ams.tradeportal.common;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.text.*;
import com.amsinc.ecsg.util.*;

   /*
    * @(#)QSortAlgorithm.java      1.6 96/12/06
    *
    * Copyright (c) 2070, 1997 Sun Microsystems, Inc. All Rights Reserved.
    *
    * Sun grants you ("Licensee") a non-exclusive, royalty free, license to use,
    * modify and redistribute this software in source and binary code form,
    * provided that i) this copyright notice and license appear on all copies of
    * the software; and ii) Licensee does not utilize the software in a manner
    * which is disparaging to Sun.
    *
    * This software is provided "AS IS," without a warranty of any kind. ALL
    * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
    * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
    * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. SUN AND ITS LICENSORS SHALL NOT BE
    * LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING
    * OR DISTRIBUTING THE SOFTWARE OR ITS DERIVATIVES. IN NO EVENT WILL SUN OR ITS
    * LICENSORS BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR DIRECT,
    * INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES, HOWEVER
    * CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING OUT OF THE USE OF
    * OR INABILITY TO USE SOFTWARE, EVEN IF SUN HAS BEEN ADVISED OF THE
    * POSSIBILITY OF SUCH DAMAGES.
    *
    * This software is not designed or intended for use in on-line control of
    * aircraft, air traffic, aircraft navigation or aircraft communications; or in
    * the design, construction, operation or maintenance of any nuclear
    * facility. Licensee represents and warrants that it will not use or
    * redistribute the Software for such purposes.
    */
   /**
    * A quick sort algorithm
    *

    * @version     @(#)QSortAlgorithm.java 1.3, 29 Feb 1996
    */    
 public class Sort 
 {
private static final Logger LOG = LoggerFactory.getLogger(Sort.class);
   /** This is a generic version of C.A.R Hoare's Quick Sort 
    * algorithm.  This will handle arrays that are already
    * sorted, and arrays with duplicate keys.<BR>
    *
    * If you think of a one dimensional array as going from
    * the lowest index on the left to the highest index on the right
    * then the parameters to this function are lowest index or
    * left and highest index or right.  The first time you call
    * this function it will be with the parameters 0, a.length - 1.
    *
    * @param a       an integer array
    * @param lo0     left boundary of array partition
    * @param hi0     right boundary of array partition
    */
   private static void QuickSort(String a[], int lo0, int hi0, Collator collator) {
      int lo = lo0;
      int hi = hi0;
      String mid;

      if ( hi0 > lo0)
      {
         /* Arbitrarily establishing partition element as the midpoint of
          * the array.
          */
         mid = a[ ( lo0 + hi0 ) / 2 ];

         // loop through the array until indices cross
         while( lo <= hi )
         {
            /* find the first element that is greater than or equal to 
             * the partition element starting from the left Index.
             * Convert to upper case so that sorting will not be case sensitive
             * Use collator so that sorting will correct for the user's locale
             */
             while( ( lo < hi0 ) && ( collator.compare(a[lo].toUpperCase(),mid.toUpperCase()) < 0 ))
                 ++lo;

            /* find an element that is smaller than or equal to 
             * the partition element starting from the right Index.
             * Convert to upper case so that sorting will not be case sensitive
             * Use collator so that sorting will correct for the user's locale
             */
             while( ( hi > lo0 ) && ( collator.compare(a[hi].toUpperCase(),mid.toUpperCase()) > 0 ))
                 --hi;

            // if the indexes have not crossed, swap
            if( lo <= hi ) 
            {
               swap(a, lo, hi);
               ++lo;
               --hi;
            }
         }

         /* If the right index has not reached the left side of array
          * must now sort the left partition.
          */
         if( lo0 < hi )
            QuickSort( a, lo0, hi, collator );

         /* If the left index has not reached the right side of array
          * must now sort the right partition.
          */
         if( lo < hi0 )
            QuickSort( a, lo, hi0, collator );

      }
   }

   private static void swap(String a[], int i, int j)
   {
      String T;
      T = a[i]; 
      a[i] = a[j];
      a[j] = T;
   }

   /** This method sorts the given string array in ascending order
    * @param a[]     String array of values to sort.  They are sorted in 
    *                place (i.e., the passed in array contains the sorted
    *                values after this call).
    * @param localeName String that is the logical name of the user's locale
    */
   public static void sort(String a[], String localeName) {
      Collator collator; 
      try 
       {
           collator = Collator.getInstance(ResourceManager.getLocaleByLogicalName(localeName));
           // Sets how strict the collator will be when sorting.
           // The 'IDENTICAL' level of strength is the strongest, meaning all differences will be
           // considered in sorting
           collator.setStrength(Collator.IDENTICAL);
       }
      catch(Exception e)
       {
           collator = Collator.getInstance(new Locale("en", "US"));
           LOG.info("Could not use locale passed for dropdown sorting... defaulting to en_US");
       }

      QuickSort(a, 0, a.length - 1, collator);
   }
}