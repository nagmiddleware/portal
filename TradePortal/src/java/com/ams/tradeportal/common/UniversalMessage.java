package com.ams.tradeportal.common;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.util.DocumentHandler;

/*
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class UniversalMessage  {
private static final Logger LOG = LoggerFactory.getLogger(UniversalMessage.class);


     public DocumentHandler UnivMsgDoc;
     public static final String headerPath           			= "/Proponix/Header";
     public static final String subHeaderPath      				= "/Proponix/SubHeader";
     public static final String userHeaderPath      			= "/Proponix/UserHeader";
     public static final String bodyPath						= "/Proponix/Body/InstrumentTransaction";
     public static final String instrumentTransactionPath		= "/Proponix/Body/InstrumentTransaction";
     public static final String instrumentPath					= "/Proponix/SubHeader/InstrumentIdentification/Instrument";
     public static final String clientBankPath					= "/Proponix/SubHeader/InstrumentIdentification/ClientBank";
     public static final String productTypePath					= "/Proponix/SubHeader/InstrumentIdentification/ProductType";
     public static final String productPath						= "/Proponix/SubHeader/InstrumentIdentification/Product";
     public static final String productCategoryPath				= "/Proponix/SubHeader/InstrumentIdentification/ProductCategory";
     public static final String transactionPath					= "/Proponix/Body/InstrumentTransaction/Transaction";
     public static final String termsPath	       			    = "/Proponix/Body/InstrumentTransaction/Terms";
     public static final String feePath							= "/Proponix/Body/InstrumentTransaction/Fee";
     public static final String documentImagePath				= "/Proponix/Body/InstrumentTransaction/DocumentImage";
     public static final String positionPath					= "/Proponix/Body/InstrumentTransaction/Position";
     public static final String mailMessagePath					= "/Proponix/Body/InstrumentTransaction/MailMessage";
     public static final String termsPartyPath					= "/Proponix/Body/InstrumentTransaction/TermsParty";
     public static final String settlementInstructionDetailPath = "/Proponix/Body/InstrumentTransaction/SettlementInstructionDetail";

     public static final String acctInqRqPath					= "/Proponix/Body";
     public static final String acctInqRsPath					= "/Proponix/Body/AcctInqRq";
     public static final String paymentBeneficiaryPath				= "/Proponix/Body/InstrumentTransaction/Terms/LoansAndFunds/PaymentBeneficiary"; //CR-507: 1.	Change domesticPayee to PaymenBeneficiary.
     public static final String financePaymentBeneficiaryPath				= "/Proponix/Body/InstrumentTransaction/Terms/LoansAndFunds/FinancePaymentBeneficiary"; ////RKAZI CR709 Rel 8.2 01/25/2013




   public UniversalMessage (DocumentHandler inputMsgDoc)
   {

    this.UnivMsgDoc= inputMsgDoc;

   }

   public static DocumentHandler getUniversalMessage(DocumentHandler univMsgDoc)
   {
	   //Header
	   univMsgDoc.setAttribute(headerPath+"/DestinationID",				"");
	   univMsgDoc.setAttribute(headerPath+"/SenderID",					"");
	   univMsgDoc.setAttribute(headerPath+"/OperationOrganizationID",	"");
	   univMsgDoc.setAttribute(headerPath+"/MessageType",				"");
	   univMsgDoc.setAttribute(headerPath+"/DateSent",					"");
	   univMsgDoc.setAttribute(headerPath+"/TimeSent",					"");
	   univMsgDoc.setAttribute(headerPath+"/MessageID",					"");

       //SubHeader
       univMsgDoc.setAttribute(subHeaderPath+"/CustomerID","");
       univMsgDoc.setAttribute(subHeaderPath+"/TotalNumberOfEntries","1");

       //SubHeader
        //InstrumentIdentification
        univMsgDoc.setAttribute(instrumentPath+"/InstrumentIDNumber",  			"");
        univMsgDoc.setAttribute(instrumentPath+"/InstrumentIDPrefix",  			"");
        univMsgDoc.setAttribute(instrumentPath+"/InstrumentIDSuffix",  			"");
        univMsgDoc.setAttribute(instrumentPath+"/InstrumentID",  			"");
        univMsgDoc.setAttribute(instrumentPath+"/InstrumentStatus",  			"");
        univMsgDoc.setAttribute(instrumentPath+"/InstrumentTypeCode",  			"");
        univMsgDoc.setAttribute(instrumentPath+"/IssueDate",  			"");
        
        //ClientBank
        univMsgDoc.setAttribute(clientBankPath+"/OTLID",  			"");

        //Language
        univMsgDoc.setAttribute(subHeaderPath+"/Language","01");


       //End of SubHeader

        // UserHeader
        univMsgDoc.setAttribute(userHeaderPath+"/UserHeader1",  			"");
        univMsgDoc.setAttribute(userHeaderPath+"/UserHeader2",  			"");
        univMsgDoc.setAttribute(userHeaderPath+"/UserHeader3",  			"");
        univMsgDoc.setAttribute(userHeaderPath+"/UserHeader4",  			"");
        univMsgDoc.setAttribute(userHeaderPath+"/UserHeader5",  			"");
        // End of UserHeader
        
	  //Body
	   //Transaction
	   univMsgDoc.setAttribute(transactionPath+"/TransactionTypeCode", "");	
	   //ShilpaR CR707 start
	   univMsgDoc.setAttribute(transactionPath+"/GroupReferenceNumber", "");
	   univMsgDoc.setAttribute(transactionPath+"/GroupReferenceType", "");
	   //ShilpaR CR707 end
	   univMsgDoc.setAttribute(transactionPath+"/SequenceNumber", "");
	   univMsgDoc.setAttribute(transactionPath+"/TransactionStatus", "");
	   univMsgDoc.setAttribute(transactionPath+"/TransactionStatusDate", "");
	   univMsgDoc.setAttribute(transactionPath+"/StandbyUsingGuaranteeForm", "");
	   

	   //Terms
	   //TermDetails
	   univMsgDoc.setAttribute(termsPath+"/TermsDetails/BranchCode",		"");
	   univMsgDoc.setAttribute(termsPath+"/TermsDetails/AssigneeAccountNumber",		"");
	   univMsgDoc.setAttribute(termsPath+"/TermsDetails/ValueDate",		"");
	  // univMsgDoc.setAttribute(termsPath+"/TermsDetails/AccountAmountSettled",		"");
	   univMsgDoc.setAttribute(termsPath+"/TermsDetails/Irrevocable",		"");
	   univMsgDoc.setAttribute(termsPath+"/TermsDetails/Transferrable",		"");
	   univMsgDoc.setAttribute(termsPath+"/TermsDetails/Operative",		"");
	   univMsgDoc.setAttribute(termsPath+"/TermsDetails/PlaceOfExpiry",		"");
	   univMsgDoc.setAttribute(termsPath+"/TermsDetails/ExpiryDate",		"");
	   univMsgDoc.setAttribute(termsPath+"/TermsDetails/ReferenceNumber",		"");
	   univMsgDoc.setAttribute(termsPath+"/TermsDetails/AmountCurrencyCode",		"");
	   univMsgDoc.setAttribute(termsPath+"/TermsDetails/Amount",		"");
	   univMsgDoc.setAttribute(termsPath+"/TermsDetails/AmountTolerancePositive",		"");
	   univMsgDoc.setAttribute(termsPath+"/TermsDetails/AmountToleranceNegative",		"");
	   univMsgDoc.setAttribute(termsPath+"/TermsDetails/PurposeType",		"");
	   univMsgDoc.setAttribute(termsPath+"/TermsDetails/DraftsRequired",		"");
       univMsgDoc.setAttribute(termsPath+"/TermsDetails/AvailableBy",		"");
	   univMsgDoc.setAttribute(termsPath+"/TermsDetails/AvailableWithParty",		"");
	   univMsgDoc.setAttribute(termsPath+"/TermsDetails/DrawnOnParty",		"");
	   univMsgDoc.setAttribute(termsPath+"/TermsDetails/UcpIndicator",		"");
	   univMsgDoc.setAttribute(termsPath+"/TermsDetails/UcpVersion",		"");
	   univMsgDoc.setAttribute(termsPath+"/TermsDetails/UcpDetails",		"");
	   univMsgDoc.setAttribute(termsPath+"/TermsDetails/AmendmentDetails",		"");
	   univMsgDoc.setAttribute(termsPath+"/TermsDetails/BankActionRequired",		"");
	   univMsgDoc.setAttribute(termsPath+"/TermsDetails/CharacteristicType",		"");

       //TenorTerms
       univMsgDoc.setAttribute(termsPath+"/TermsDetails/TenorTerms/Tenor1Amount",		"");
       univMsgDoc.setAttribute(termsPath+"/TermsDetails/TenorTerms/Tenor2Amount",		"");
       univMsgDoc.setAttribute(termsPath+"/TermsDetails/TenorTerms/Tenor3Amount",		"");
       univMsgDoc.setAttribute(termsPath+"/TermsDetails/TenorTerms/Tenor4Amount",		"");
       univMsgDoc.setAttribute(termsPath+"/TermsDetails/TenorTerms/Tenor5Amount",		"");
       univMsgDoc.setAttribute(termsPath+"/TermsDetails/TenorTerms/Tenor6Amount",		"");
       univMsgDoc.setAttribute(termsPath+"/TermsDetails/TenorTerms/Tenor1",		"");
       univMsgDoc.setAttribute(termsPath+"/TermsDetails/TenorTerms/Tenor2",		"");
       univMsgDoc.setAttribute(termsPath+"/TermsDetails/TenorTerms/Tenor3",		"");
       univMsgDoc.setAttribute(termsPath+"/TermsDetails/TenorTerms/Tenor4",		"");
       univMsgDoc.setAttribute(termsPath+"/TermsDetails/TenorTerms/Tenor5",		"");
       univMsgDoc.setAttribute(termsPath+"/TermsDetails/TenorTerms/Tenor6",		"");
       univMsgDoc.setAttribute(termsPath+"/TermsDetails/TenorTerms/TenorDraftNumber1",		"");
       univMsgDoc.setAttribute(termsPath+"/TermsDetails/TenorTerms/TenorDraftNumber2",		"");
       univMsgDoc.setAttribute(termsPath+"/TermsDetails/TenorTerms/TenorDraftNumber3",		"");
       univMsgDoc.setAttribute(termsPath+"/TermsDetails/TenorTerms/TenorDraftNumber4",		"");
       univMsgDoc.setAttribute(termsPath+"/TermsDetails/TenorTerms/TenorDraftNumber5",		"");
       univMsgDoc.setAttribute(termsPath+"/TermsDetails/TenorTerms/TenorDraftNumber6",		"");
       //GuaTerms
       univMsgDoc.setAttribute(termsPath+"/TermsDetails/GuaTerms/GuaAcceptanceInstruction",		"");
       univMsgDoc.setAttribute(termsPath+"/TermsDetails/GuaTerms/GuaAdviseIssuance",		"");
       univMsgDoc.setAttribute(termsPath+"/TermsDetails/GuaTerms/GuaDeliverBy",		"");
       univMsgDoc.setAttribute(termsPath+"/TermsDetails/GuaTerms/GuaDeliverTo",		"");
       univMsgDoc.setAttribute(termsPath+"/TermsDetails/GuaTerms/GuaExpiryDateType",		"");
       univMsgDoc.setAttribute(termsPath+"/TermsDetails/GuaTerms/GuaValidFromDate",		"");
       univMsgDoc.setAttribute(termsPath+"/TermsDetails/GuaTerms/GuaValidFromDateType",		"");
       univMsgDoc.setAttribute(termsPath+"/TermsDetails/GuaTerms/GuaIssueType",		"");
       univMsgDoc.setAttribute(termsPath+"/TermsDetails/GuaTerms/OverseasValidityDate",		"");
	   univMsgDoc.setAttribute(termsPath+"/TermsDetails/GuaTerms/TenderOrderContractDetail",		"");
	   univMsgDoc.setAttribute(termsPath+"/TermsDetails/GuaTerms/GuaCustomerText",		"");
       univMsgDoc.setAttribute(termsPath+"/TermsDetails/GuaTerms/GuaBankStandardText",		"");

	   univMsgDoc.setAttribute(termsPath+"/TermsDetails/RelatedInstrumentID",		"");
	   univMsgDoc.setAttribute(termsPath+"/TermsDetails/WorkItemNumber",		"");
	   univMsgDoc.setAttribute(termsPath+"/TermsDetails/CaseOfNeedAuthority",		"");
	   univMsgDoc.setAttribute(termsPath+"/TermsDetails/CaseOfNeedText",		"");
	   univMsgDoc.setAttribute(termsPath+"/TermsDetails/CaseOfNeedContact",		"");
	   univMsgDoc.setAttribute(termsPath+"/TermsDetails/URCPublicationYear",		"");
	   univMsgDoc.setAttribute(termsPath+"/TermsDetails/PresentDocumentsWithinDays",		"");
	   univMsgDoc.setAttribute(termsPath+"/TermsDetails/InsuranceRiskType",		"");
	   univMsgDoc.setAttribute(termsPath+"/TermsDetails/InsuranceEndorseValuePlus",		"");
	   univMsgDoc.setAttribute(termsPath+"/TermsDetails/PartialShipmentAllowed", "");

	  // univMsgDoc.setAttribute(termsPath+"/TermsDetails/AdjustmentType",		"");

	 //Leelavathi - 10thDec2012 - Rel8200 CR-737 - Start
	   univMsgDoc.setAttribute(termsPath+"/TermsDetails/AutoExtendTerms/AutoExtendIndicator", "");
	   univMsgDoc.setAttribute(termsPath+"/TermsDetails/AutoExtendTerms/AutoExtendPeriod", "");
	   univMsgDoc.setAttribute(termsPath+"/TermsDetails/AutoExtendTerms/MaximumNo", "");
	   univMsgDoc.setAttribute(termsPath+"/TermsDetails/AutoExtendTerms/NumberOfDays", "");
	   //Leelavathi - 10thDec2012 - Rel8200 CR-737 - End
	   //Narayan - 14/Jan/2014 - Rel9.0 CR-831 - Start
	   univMsgDoc.setAttribute(termsPath+"/TermsDetails/AutoExtendTerms/BeneficiaryNotifyDays", "");
	   univMsgDoc.setAttribute(termsPath+"/TermsDetails/AutoExtendTerms/FinalExpiryDate", "");
	   //Narayan - 14/Jan/2014 - Rel9.0 CR-831 - End
	   
	   //DocumentsRequired
	   univMsgDoc.setAttribute(termsPath+"/DocumentsRequired/TotalNumberOfEntries",		"0");
	   univMsgDoc.setAttribute(termsPath+"/DocumentsRequired/FullDocumentRequired/DocumentName",		"");
	   univMsgDoc.setAttribute(termsPath+"/DocumentsRequired/FullDocumentRequired/DocumentText",		"");
       univMsgDoc.setAttribute(termsPath+"/DocumentsRequired/FullDocumentRequired/NumberOfCopies",		"");
	   univMsgDoc.setAttribute(termsPath+"/DocumentsRequired/FullDocumentRequired/NumberOfOriginals",		"");

	   //TransportDocuments
	   univMsgDoc.setAttribute(termsPath+"/TransportDocuments/TotalNumberOfEntries", "0");
           univMsgDoc.setAttribute(termsPath+"/TransportDocuments/FullTransportDocuments/TransportDocumentOriginals", "");
	   univMsgDoc.setAttribute(termsPath+"/TransportDocuments/FullTransportDocuments/TransportDocumentCopies", "");
	   univMsgDoc.setAttribute(termsPath+"/TransportDocuments/FullTransportDocuments/TransportDocumentType", "");
   	   univMsgDoc.setAttribute(termsPath+"/TransportDocuments/FullTransportDocuments/TransportDocumentConsignType", "");
   	   univMsgDoc.setAttribute(termsPath+"/TransportDocuments/FullTransportDocuments/TransportDocumentConsignOrderOfParty", "");
   	   univMsgDoc.setAttribute(termsPath+"/TransportDocuments/FullTransportDocuments/TransportDocumentConsignOrderToParty", "");
   	   univMsgDoc.setAttribute(termsPath+"/TransportDocuments/FullTransportDocuments/TransportDocumentMarkedFreight", "");
   	   univMsgDoc.setAttribute(termsPath+"/TransportDocuments/FullTransportDocuments/TransportDocumentText", "");
   	   univMsgDoc.setAttribute(termsPath+"/TransportDocuments/FullTransportDocuments/TransportAdditionalDocumentText", "");
   	   univMsgDoc.setAttribute(termsPath+"/TransportDocuments/FullTransportDocuments/TransportDocumentShipmentDescription", "");

	   //ShipmentTerms
	   univMsgDoc.setAttribute(termsPath+"/ShipmentTerms/TotalNumberOfEntries", "0");
	   univMsgDoc.setAttribute(termsPath+"/ShipmentTerms/FullShipmentTerms/TransshipmentAllowed", "");
   	   univMsgDoc.setAttribute(termsPath+"/ShipmentTerms/FullShipmentTerms/LatestShipmentDate", "");
   	   univMsgDoc.setAttribute(termsPath+"/ShipmentTerms/FullShipmentTerms/Incoterm", "");
   	   univMsgDoc.setAttribute(termsPath+"/ShipmentTerms/FullShipmentTerms/IncotermLocation", "");
   	   univMsgDoc.setAttribute(termsPath+"/ShipmentTerms/FullShipmentTerms/ShipmentFrom", "");
   	   univMsgDoc.setAttribute(termsPath+"/ShipmentTerms/FullShipmentTerms/ShipmentTo", "");
   	   univMsgDoc.setAttribute(termsPath+"/ShipmentTerms/FullShipmentTerms/GoodsDescription", "");
   	   univMsgDoc.setAttribute(termsPath+"/ShipmentTerms/FullShipmentTerms/POLineItems", "");
   	   univMsgDoc.setAttribute(termsPath+"/ShipmentTerms/FullShipmentTerms/AirWaybillNumber", "");
   	   univMsgDoc.setAttribute(termsPath+"/ShipmentTerms/FullShipmentTerms/VesselName", "");
   	   univMsgDoc.setAttribute(termsPath+"/ShipmentTerms/FullShipmentTerms/TransportTermsText", "");
   	   univMsgDoc.setAttribute(termsPath+"/ShipmentTerms/FullShipmentTerms/ContainerNumber", "");
   	   univMsgDoc.setAttribute(termsPath+"/ShipmentTerms/FullShipmentTerms/BillOfLadingNumber", "");
	   univMsgDoc.setAttribute(termsPath+"/ShipmentTerms/FullShipmentTerms/ShipFromLoading", "");
	   univMsgDoc.setAttribute(termsPath+"/ShipmentTerms/FullShipmentTerms/ShipToDischarge", "");

   	   univMsgDoc.setAttribute(termsPath+"/ShipmentTerms/FullShipmentTerms/NPBusinessName", "");
   	   univMsgDoc.setAttribute(termsPath+"/ShipmentTerms/FullShipmentTerms/NPAddressLine1", "");
   	   univMsgDoc.setAttribute(termsPath+"/ShipmentTerms/FullShipmentTerms/NPAddressLine2", "");
   	   univMsgDoc.setAttribute(termsPath+"/ShipmentTerms/FullShipmentTerms/NPAddressLine3", "");
   	   univMsgDoc.setAttribute(termsPath+"/ShipmentTerms/FullShipmentTerms/CONBusinessName", "");
   	   univMsgDoc.setAttribute(termsPath+"/ShipmentTerms/FullShipmentTerms/CONAddressLine1", "");
   	   univMsgDoc.setAttribute(termsPath+"/ShipmentTerms/FullShipmentTerms/CONAddressLine2", "");
   	   univMsgDoc.setAttribute(termsPath+"/ShipmentTerms/FullShipmentTerms/CONAddressLine3", "");

	 //Instructions
	   univMsgDoc.setAttribute(termsPath+"/Instructions/InstructionsToBank",		"");
	   univMsgDoc.setAttribute(termsPath+"/Instructions/InstructionsOther",		"");
	   univMsgDoc.setAttribute(termsPath+"/Instructions/ExportDiscrepancyInstructions",		"");
	   univMsgDoc.setAttribute(termsPath+"/Instructions/ImportDiscrepancyInstructions",		"");
	   //rkrishna CR 375-D ATP-Response 08/22/2007 Begin
	   univMsgDoc.setAttribute(termsPath+"/Instructions/ApprovaltoPayResponseInstructions",		"");
           //rkrishna CR 375-D ATP-Response 08/22/2007 End
	   univMsgDoc.setAttribute(termsPath+"/Instructions/SettlementInstructions/BranchCode",		"");
	   univMsgDoc.setAttribute(termsPath+"/Instructions/SettlementInstructions/SettlementOurAccountNumber",		"");
	   univMsgDoc.setAttribute(termsPath+"/Instructions/SettlementInstructions/SettlementForeignAccountNumber",		"");
	   univMsgDoc.setAttribute(termsPath+"/Instructions/SettlementInstructions/SettlementForeignAccountCurrency",		"");

	   //CollectionInstructions
	   univMsgDoc.setAttribute(termsPath+"/Instructions/CollectionInstructions/CollectionChargesType",		"");
	   univMsgDoc.setAttribute(termsPath+"/Instructions/CollectionInstructions/CollectionChargesIncludeText",		"");
	   univMsgDoc.setAttribute(termsPath+"/Instructions/CollectionInstructions/DoNotWaiveRefusedCharges",		"");
	   univMsgDoc.setAttribute(termsPath+"/Instructions/CollectionInstructions/DoNotWaiveRefusedInterest",		"");
	   univMsgDoc.setAttribute(termsPath+"/Instructions/CollectionInstructions/CollectInterestAtIndicator",		"");
	   univMsgDoc.setAttribute(termsPath+"/Instructions/CollectionInstructions/CollectInterestAt",		"");
	   univMsgDoc.setAttribute(termsPath+"/Instructions/CollectionInstructions/InstructAdviseAcceptByTel",		"");
	   univMsgDoc.setAttribute(termsPath+"/Instructions/CollectionInstructions/InstructAdviseNonAcceptByTel",		"");
	   univMsgDoc.setAttribute(termsPath+"/Instructions/CollectionInstructions/InstructAdvisePaymentByTel",		"");
       univMsgDoc.setAttribute(termsPath+"/Instructions/CollectionInstructions/InstructAdviseNonPaymentByTel",		"");
	   univMsgDoc.setAttribute(termsPath+"/Instructions/CollectionInstructions/InstructNoteForNonAcceptance",		"");
	   univMsgDoc.setAttribute(termsPath+"/Instructions/CollectionInstructions/InstructNoteForNonPayment",		"");
	   univMsgDoc.setAttribute(termsPath+"/Instructions/CollectionInstructions/InstructOther",		"");
	   univMsgDoc.setAttribute(termsPath+"/Instructions/CollectionInstructions/InstructProtestforNonAcceptance",		"");
	   univMsgDoc.setAttribute(termsPath+"/Instructions/CollectionInstructions/InstructProtestForNonPayment",		"");
	   univMsgDoc.setAttribute(termsPath+"/Instructions/CollectionInstructions/InstructReleaseDocumentOnAcceptance",		"");
	   univMsgDoc.setAttribute(termsPath+"/Instructions/CollectionInstructions/InstructReleaseDocumentOnPayment",		"");

	   univMsgDoc.setAttribute(termsPath+"/Instructions/CommissionAndCharges/CommissionChargesOurAccountNumber",		"");
	   univMsgDoc.setAttribute(termsPath+"/Instructions/CommissionAndCharges/CommissionChargesForeignAccountNumber",		"");
	   univMsgDoc.setAttribute(termsPath+"/Instructions/CommissionAndCharges/CommissionChargesForeignAccountCurrency",		"");
	   univMsgDoc.setAttribute(termsPath+"/Instructions/CommissionAndCharges/CommissionChargesOtherText",		"");
	   univMsgDoc.setAttribute(termsPath+"/Instructions/CommissionAndCharges/FinanceDrawing",		"");
	   univMsgDoc.setAttribute(termsPath+"/Instructions/CommissionAndCharges/FinanceDrawingNumberOfDays",		"");
       univMsgDoc.setAttribute(termsPath+"/Instructions/CommissionAndCharges/FinanceDrawingType",		"");
	   univMsgDoc.setAttribute(termsPath+"/Instructions/CommissionAndCharges/CoveredByFECNumber",		"");
	   univMsgDoc.setAttribute(termsPath+"/Instructions/CommissionAndCharges/FECAmount",		"");
	   univMsgDoc.setAttribute(termsPath+"/Instructions/CommissionAndCharges/FECRate",		"");
	   univMsgDoc.setAttribute(termsPath+"/Instructions/CommissionAndCharges/FECMaturityDate",		"");



	   univMsgDoc.setAttribute(termsPath+"/Instructions/TracerDetails",		"");
	   univMsgDoc.setAttribute(termsPath+"/Instructions/TracerSendType",		"");

	 	 //PaymentTerms
		   	univMsgDoc.setAttribute(termsPath+"/PaymentTerms/PaymentTermsType",		"");
		    univMsgDoc.setAttribute(termsPath+"/PaymentTerms/PaymentTermsNumberDaysAfter",		"");
		    univMsgDoc.setAttribute(termsPath+"/PaymentTerms/PaymentTermsDaysAfterType",		"");
		    univMsgDoc.setAttribute(termsPath+"/PaymentTerms/PaymentTermsPercentInvoice",		"");
		    univMsgDoc.setAttribute(termsPath+"/PaymentTerms/PartPaymentType",		"");
		    univMsgDoc.setAttribute(termsPath+"/PaymentTerms/PartPaymentPercent",		"");
		    univMsgDoc.setAttribute(termsPath+"/PaymentTerms/PaymentTermsFixedMaturityDate",		"");
	
		  //Leelavathi - 10thDec2012 - Rel8200 CR-737 - Start
		    univMsgDoc.setAttribute(termsPath+"/PaymentTerms/TenorSpecialText",		"");
		    univMsgDoc.setAttribute(termsPath+"/PaymentTerms/TotalNumberOfEntries",		"0");
		     
			//Leelavathi - 10thDec2012 - Rel8200 CR-737 - End

	   //BankCharges
	   univMsgDoc.setAttribute(termsPath+"/BankCharges/BankChargesType",		"");

	   //OtherConditions
	   univMsgDoc.setAttribute(termsPath+"/OtherConditions/ConfirmationType",		"");
	  // univMsgDoc.setAttribute(termsPath+"/OtherConditions/Transferrable",		"");
	   univMsgDoc.setAttribute(termsPath+"/OtherConditions/AutoExtend",		"");
	   univMsgDoc.setAttribute(termsPath+"/OtherConditions/Revolve",		"");
	   univMsgDoc.setAttribute(termsPath+"/OtherConditions/AdditionalConditions",		"");
	 

	   univMsgDoc.setAttribute(termsPath+"/FromExpressTemplate",		"");
       univMsgDoc.setAttribute(termsPath+"/CollectionDate",		"");
       univMsgDoc.setAttribute(termsPath+"/UsanceMaturityDate",		"");
       univMsgDoc.setAttribute(termsPath+"/AdjustmentType",		"");
       univMsgDoc.setAttribute(termsPath+"/AccountReceivableType",		"");
       univMsgDoc.setAttribute(termsPath+"/OpeningBankInstrumentNumber",		"");
       univMsgDoc.setAttribute(termsPath+"/AdditionalDocumentText",		"");




      //Fee
	   univMsgDoc.setAttribute(feePath+"/TotalNumberOfEntries",			"0");
	   univMsgDoc.setAttribute(feePath+"/FullFee/SettlementCurrencyCode",			"");
	   univMsgDoc.setAttribute(feePath+"/FullFee/SettlementCurrencyAmount",			"");
	   univMsgDoc.setAttribute(feePath+"/FullFee/ChargeType",			"");
	   univMsgDoc.setAttribute(feePath+"/FullFee/BaseCurrencyAmount",			"");
	   univMsgDoc.setAttribute(feePath+"/FullFee/AccountNumber",			"");
	   univMsgDoc.setAttribute(feePath+"/FullFee/SettlementHowType",			"");

       //univMsgDoc.setAttribute(feePath,	 null);




      //DocumentImage
	   univMsgDoc.setAttribute(documentImagePath+"/TotalNumberOfEntries",			"0");

      // univMsgDoc.setAttribute(documentImagePath,			null);

     //Position
       univMsgDoc.setAttribute(positionPath+"/AvailableAmount",			"");
 	   univMsgDoc.setAttribute(positionPath+"/EquivalentAmount",			"");
       univMsgDoc.setAttribute(positionPath+"/LiabilityAmountInBaseCurrency",			"");
       univMsgDoc.setAttribute(positionPath+"/LiabilityAmountInLimitCurrency",			"");

      //TermsParty
       univMsgDoc.setAttribute(termsPartyPath+"/TotalNumberOfEntries",			"0");
	   univMsgDoc.setAttribute(termsPartyPath+"/FullTermsParty/TermsPartyType",			"");
	   univMsgDoc.setAttribute(termsPartyPath+"/FullTermsParty/Name",			"");
	   univMsgDoc.setAttribute(termsPartyPath+"/FullTermsParty/AddressLine1",			"");
	   univMsgDoc.setAttribute(termsPartyPath+"/FullTermsParty/AddressLine2",			"");
	   univMsgDoc.setAttribute(termsPartyPath+"/FullTermsParty/AddressLine3",			"");
	   univMsgDoc.setAttribute(termsPartyPath+"/FullTermsParty/AddressCity",			"");
	   univMsgDoc.setAttribute(termsPartyPath+"/FullTermsParty/AddressStateProvince",			"");
	   univMsgDoc.setAttribute(termsPartyPath+"/FullTermsParty/AddressCountry",			"");
   	   univMsgDoc.setAttribute(termsPartyPath+"/FullTermsParty/AddressCity",			"");
   	   univMsgDoc.setAttribute(termsPartyPath+"/FullTermsParty/AddressPostalCode",			"");
   	   univMsgDoc.setAttribute(termsPartyPath+"/FullTermsParty/PhoneNumber",			"");
       univMsgDoc.setAttribute(termsPartyPath+"/FullTermsParty/FaxNumber1",			"");
   	   univMsgDoc.setAttribute(termsPartyPath+"/FullTermsParty/ContactName",			"");
   	   univMsgDoc.setAttribute(termsPartyPath+"/FullTermsParty/OTLCustomerID",			"");


     //  univMsgDoc.setAttribute(termsPartyPath,			null);



	   LOG.debug(univMsgDoc.toString());



	   return univMsgDoc;


   }



}