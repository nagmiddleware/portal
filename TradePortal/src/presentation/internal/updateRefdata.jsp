<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*, java.util.*" %>
<META HTTP-EQUIV= "expires" content = "Saturday, 07-Oct-00 16:00:00 GMT"></meta>

<%--
 *
 *     Copyright   2002                        
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>

  <%@ include file="fragments/internalPageAuth.frag" %>

<font face="Arial" size="2">
<a href="<%=request.getContextPath()%>/internal/utilityLauncher.jsp">Return to Launcher Page</a>
<hr>

<%
      PropertyResourceBundle portalProperties = (PropertyResourceBundle) PropertyResourceBundle.getBundle("TradePortal");
      String numServers = null;

      try
       {
          numServers = portalProperties.getString("numberOfAppServers");
       }
      catch(MissingResourceException mre)
       {  }

      int appServerCount;

      if((numServers == null) || (numServers.equals("")))
        appServerCount = 1;
      else
        appServerCount = Integer.parseInt(numServers);

      Vector serverLocations = new Vector();

      for(int i = 1; i<=appServerCount; i++)
       {
          String location = request.getParameter("serverLocation"+i);
          if((location != null) && (!location.equals("")))
           {
             serverLocations.addElement(location);
           }
       }

      Enumeration enumer = serverLocations.elements();

      // Loop through each app server location and run the utility
      while(enumer.hasMoreElements())
       {
         String serverLocation = (String) enumer.nextElement();
   
         try
          {
           out.println("Updating reference data for server location "+StringFunction.xssCharsToHtml(serverLocation)+"<br><br>"); //Rel 9.3 XSS CID-11234
  
           // Create server deploy so that the utility will execute on the correct
           // server location
           ServerDeploy sd = (ServerDeploy)EJBObjectFactory.createClientEJB(serverLocation, "ServerDeploy");  
           sd.init(serverLocation);

           sd.reloadReferenceData();
        
           out.print("Result: Success");
  
           sd.remove();
         }
        catch (Exception e)
         { 
           out.print("There was an exception <br><br>");
           e.printStackTrace();
         } 
       } 

%>