<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
 * This JSP contains the logic to present a drop down of reference
 * caches maintained by the system, and the logic to flush those caches
 * upon selection.
 *
--%>

<%@page import="java.util.List"%>
<%@ page import="com.amsinc.ecsg.web.*,com.amsinc.ecsg.util.*,
        com.amsinc.ecsg.frame.*, com.ams.tradeportal.mediator.*, com.ams.tradeportal.busobj.util.*,
	com.ams.tradeportal.common.*, com.ams.tradeportal.common.cache.*, com.ams.tradeportal.html.*,
	java.text.*, java.security.*" %>
      
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>
<jsp:useBean id="formMgr"     class="com.amsinc.ecsg.web.FormManager"            scope="session"></jsp:useBean>
<jsp:useBean id="resMgr"      class="com.amsinc.ecsg.util.ResourceManager"       scope="session"></jsp:useBean>

<%--
 * uncomment this line to enable the password check on utilityLauncher.jsp
 * <%@ include file="fragments/internalPageAuth.frag" %>
--%>
<!-- <DIV style="overflow:auto;"> -->
<table style="border : 2px solid black;   border-collapse: collapse;">
<tr style="border: 2px solid black;">
<td style="border: 2px solid black;"><b>Page Name</b></td>
<td style="border: 2px solid black;"><b>Enabled</b></td>
</tr>

<%
String cb = request.getParameter("clientBank");
cb = (StringFunction.isBlank(cb) ? "" :cb );
Debug.debug("SLAMonitoringConfigFetch.jsp : Client Bank ==>> "+ cb);
DocumentHandler doc = null; 
try {
   // Process navigation info xml configuration file
   doc = XmlConfigFileCache.readConfigFile("/Navigation.xml");
   Debug.debug("SLAMonitoringConfigFetch.jsp : NaviagtionDoc ==== >> " +doc.toString());
   java.util.Vector pageNameList = doc.getFragments("/WebPage");
   Debug.debug("SLAMonitoringConfigFetch.jsp : WebPage Vector ===> "+ pageNameList);
   Debug.debug("SLAMonitoringConfigFetch.jsp : Logical Name  ==> SLAMonitoring == isEnabled ===> " + PerfStatConfigManager.getPerfStatConfigMgr().getPerfStatConfig("SLAMonitoring", cb, "N"));
   %>
   <tr>
	<td style="border: 1px solid black;">SLAMonitoring</td>
	<td style="border: 1px solid black;"> <select id="SLAMonitoring" name="SLAMonitoring"><option value="Y" <%=(("Y".equals(PerfStatConfigManager.getPerfStatConfigMgr().getPerfStatConfig("SLAMonitoring", cb, "N"))? "selected": "")) %>>Y</option><option value="N" <%=(("N".equals(PerfStatConfigManager.getPerfStatConfigMgr().getPerfStatConfig("SLAMonitoring", cb, "N"))? "selected": "")) %>>N</option></select></td>
	</tr>
	<%
   for (int i = 0; i< pageNameList.size(); i++){
	   String logicalPageName = ((DocumentHandler)pageNameList.get(i)).getAttribute("logicalName");
	   String isEnabled = PerfStatConfigManager.getPerfStatConfigMgr().getPerfStatConfig(logicalPageName, cb, "N");
	   Debug.debug("SLAMonitoringConfigFetch.jsp : Logical Name  ==> "+logicalPageName + " == isEnabled ===> " + isEnabled);
%>
	<tr>
		<td style="border: 1px solid black;"><%=logicalPageName%></td>
		<td style="border: 1px solid black;"> <select id="<%=logicalPageName%>" name="<%=logicalPageName%>"><option value="Y" <%=(("Y".equals(isEnabled)? "selected": "")) %>>Y</option><option value="N" <%=(("N".equals(isEnabled)? "selected": "")) %>>N</option></select></td>
	</tr>
<%	   
   }
   
}catch (Exception e){
	e.printStackTrace();
}
%>
</table>
<!-- </DIV>      -->
