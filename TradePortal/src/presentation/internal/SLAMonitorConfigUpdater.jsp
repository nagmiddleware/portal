<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
 * This JSP contains the logic to present a drop down of reference
 * caches maintained by the system, and the logic to flush those caches
 * upon selection.
 *
--%>

<%@page import="java.util.List"%>
<%@ page import="com.amsinc.ecsg.web.*,com.amsinc.ecsg.util.*,
        com.amsinc.ecsg.frame.*, com.ams.tradeportal.mediator.*, com.ams.tradeportal.busobj.util.*,
	com.ams.tradeportal.common.*, com.ams.tradeportal.common.cache.*, com.ams.tradeportal.html.*,
	java.text.*, java.security.*, java.util.*" %>
      
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>
<jsp:useBean id="formMgr"     class="com.amsinc.ecsg.web.FormManager"            scope="session"></jsp:useBean>
<jsp:useBean id="resMgr"      class="com.amsinc.ecsg.util.ResourceManager"       scope="session"></jsp:useBean>

<%--
 * uncomment this line to enable the password check on utilityLauncher.jsp
 * <%@ include file="fragments/internalPageAuth.frag" %>
--%>
<%
	WidgetFactory widgetFactory = new WidgetFactory(resMgr); 
DocumentHandler cbDoc = DatabaseQueryBean.getXmlResultSet("Select OTL_ID, OTL_ID from client_bank", false, new ArrayList());
String dropdownOptions = "<option value=\"\"></options>".concat(Dropdown.createSortedOptions(cbDoc, "OTL_ID", "OTL_ID", "", resMgr.getResourceLocale()));
%>
<html>
   <head>
      <title>SLA Monitor Config Updater</title>
      <style>
 th, td {
    border: 1px solid black;
    border-collapse: collapse;
}
th, td {
    padding: 10px;
}
</style>
      <script type="text/javascript">
    //Global variable to store the XMLHttpRequest object
		
		var myRequest;
		
		//Package the request into its own function
		
		function getData(url)
		
		{
		
		//Feature-testing technique to make sure the browser
		
		//supports the XMLHttpRequest object
		
		if (window.XMLHttpRequest)
		
		{
		
		//create a new instance of the object
		
		myRequest = new XMLHttpRequest();
		
		}
		
		//else - the XMLHttpRequest object is not supported:
		
		//create an instance of ActiveXObject
		
		else
		
		{
		
		myRequest = new ActiveXObject("Microsoft.XMLHTTP");
		
		}
		
		//Use the open(retrievalMethod, "myDataFile.txt", bool) method
		
		myRequest.open("GET", url, true);
		
		//Use send() with a null argument - we have nothing to send:
		
		myRequest.send(null);
		
		//attach a function to handle onreadystatechange,
		
		//that is, the response from the server:
		
		myRequest.onreadystatechange = dataRecieved;
		
		}
		function dataRecieved (){
			//Make sure the response is at the completion stage (4):
			
			if (myRequest.readyState ===4)
			
			{
			
			//if it is, make sure the status code is 200 (success):
			
			if (myRequest.status === 200)
			
			{
			
			//if all is well, handle the response:
			
			var text = myRequest.responseText;

			document.getElementById("MonitorEnabled").innerHTML = text;
			}
			
			}
		}
      function fetchConfig(){
    	  
    	  var cb = document.getElementById('clientBank');
    	  var selectedCB = cb.options[cb.selectedIndex].value;
    	  
    	  if (selectedCB == ''){
    		  document.getElementById("MonitorEnabled").innerHTML =""
    	  }else{
    	  	getData("/portal/internal/SLAMonitorConfigFetch.jsp?clientBank="+selectedCB);
    	  }
      }
      </script>
   </head>
   <body height="300px">
     <h1>
        SLA Monitor Configuration Enable/Disable 
     </h1>	
     <form name="SLAMonitorConfigUpdater" action="SLAMonitorConfigUpdaterSave.jsp" method="POST">
     &nbsp;
     <td>
         Please select pages to start SLA Monitoring on.
     </td>
     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
     <input type ="submit" value=Save ></input>
     <table  border="0px">
       
       <tr valign="middle">
        <td style="border: 0px;">
        	<%                                               
        out.print(widgetFactory.createSearchSelectField("clientBank", "Client Bank",
        		"",
                                               dropdownOptions.toString(), "onChange=fetchConfig();"));  

%>

        </td>
       </tr>
     </table>
<br>
<DIV id="MonitorEnabled" style="overflow:auto; height: 390px; width: 450px;">
</DIV>
<BR/>

    </form>

</body>
</html>
