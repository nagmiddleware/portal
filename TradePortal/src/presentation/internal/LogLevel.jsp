<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="com.amsinc.ecsg.web.*,com.amsinc.ecsg.util.*,
        com.amsinc.ecsg.frame.*, com.ams.tradeportal.mediator.*,
	  com.ams.tradeportal.common.*, com.ams.tradeportal.html.*,
	  java.net.URL,java.util.*" %>
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session" />
<jsp:include page="/common/Header.jsp">
   <jsp:param name="minimalHeaderFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="brandingDirectory" value="<%=userSession.getBrandingDirectory()%>" />
</jsp:include>
<style>
.success {color:green;}
.error {color:red;}
</style>
	<div id="mainPageContent" class="pageContent">
	 <form id="formNode" name="formNode">
	   <label> Select Package / Type Complete Class Name </label>
	   <hr>
	   
	   <label for="Package"> Package Select: </label>
	   <select id="Package" name="Package" data-dojo-type="dijit/form/FilteringSelect"
        data-dojo-props="value: '',placeHolder: 'Select a package'">
	        <option value="com.ams.tradeportal.agents">com.ams.tradeportal.agents</option>
	        <option value="com.ams.tradeportal.busobj">com.ams.tradeportal.busobj</option>
	        <option value="com.ams.tradeportal.busobj.termsmgr">com.ams.tradeportal.busobj.termsmgr</option>
	        <option value="com.ams.tradeportal.busobj.util">com.ams.tradeportal.busobj.util</option>
	        <option value="com.ams.tradeportal.busobj.webbean">com.ams.tradeportal.busobj.webbean</option>
	        <option value="com.ams.tradeportal.common">com.ams.tradeportal.common</option>
	        <option value="com.ams.tradeportal.common.cache">com.ams.tradeportal.common.cache</option>
	        <option value="com.ams.tradeportal.conversion">com.ams.tradeportal.conversion</option>
	        <option value="com.ams.tradeportal.dataview">com.ams.tradeportal.dataview</option>
	        <option value="com.ams.tradeportal.html">com.ams.tradeportal.html</option>
	        <option value="com.ams.tradeportal.mediator">com.ams.tradeportal.mediator</option>
	        <option value="com.ams.tradeportal.mediator.postmediator">com.ams.tradeportal.mediator.postmediator</option>
	        <option value="com.ams.tradeportal.mediator.premediator">com.ams.tradeportal.mediator.premediator</option>
	        <option value="com.ams.tradeportal.mediator.util">com.ams.tradeportal.mediator.util</option>
	        <option value="com.ams.tradeportal.producer">com.ams.tradeportal.producer</option>
	        <option value="com.ams.tradeportal.servlet">com.ams.tradeportal.servlet</option>
    	</select>
	   
	   <label for="Class"> Complete Class Name: </label>
	   <input data-dojo-type="dijit/form/TextBox" id="Class" name="Package"/>
	   
	   <label for="Level"> Log Level Select: </label>
	   <select id="Level" name="Level" data-dojo-type="dijit/form/FilteringSelect"
        data-dojo-props="value: 'ALL'">
	        <option value="ALL">All</option>
	        <option value="INFO">Info</option>
	        <option value="DEBUG">Debug</option>
	        <option value="ERROR">Error</option>
	        <option value="TRACE">Trace</option>
	        <option value="WARN">Warn</option>
	        <option value="OFF">Off</option>
    	</select>
	   <hr>
	   <div>
		   <span><input type="submit" value="Submit"></span>
		   <span id="responseText"></span>
	   </div>
	 </form>
	</div>
	<script src="<%=userSession.getdojoJsPath()%>/dojoconfig.js?"></script>
	<script src='<%=userSession.getdojoJsPath()%>/dojo/dojo.js?' data-dojo-config="async: 1" ></script>
	<script>
		require(["dojo/parser", "dijit/registry", "dojo/on","dojo/dom-class","dojo/dom","dojo/request", "dojo/dom-form", "dijit/form/TextBox","dijit/form/FilteringSelect", "dojo/domReady!"],
		        function(parser, registry, on, domClass,dom, request, domForm){
		    //parse mainPageContent div as it contains dojo widgets
			parser.parse(dom.byId("mainPageContent"));
       		domClass.add(document.body, 'loaded');
		    var textBox = registry.byId("Class");
		    var packageSelection = registry.byId("Package");
		    var form = dom.byId('formNode');
		    
		    on(textBox, "change", function(){
		    	if(textBox.value.length)
		    		packageSelection.setDisabled(true);
		    	else 
		    		packageSelection.setDisabled(false);
		    		
		    });
		    
		    on(form, "submit", function(evt){
		    	// prevent the page from navigating after submit
	            evt.stopPropagation();
	            evt.preventDefault();

	            // Post the data to the server
	            request.post("/portal/internal/logLevel.jsp", {
	                // Send the username and password
	                data: domForm.toObject("formNode"),
	                // Wait 2 seconds for a response
	                timeout: 2000

	            }).then(function(response){
	            	//Clear error style before setting to style class success
	            	domClass.remove(dom.byId('responseText'), 'error');
	            	domClass.add(dom.byId('responseText'), 'success');
	                dom.byId('responseText').innerHTML = response;
	            },
				function(error){
	            	//Clear success style before setting to style class error
	            	domClass.remove(dom.byId('responseText'), 'success');
	            	domClass.add(dom.byId('responseText'), 'error');
	            	dom.byId('responseText').innerHTML = error.response.data;
				});
		    		
		    });
		});
	
	  
	</script>
</body>


</html>

