<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*, java.util.*, com.ams.tradeportal.devtool.*" %>
<%--
 *
 *    Runs the reference data copy utility based on the parameters passed in.
 *
 *
 *     Copyright  � 2002                        
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<META HTTP-EQUIV= "expires" content = "Saturday, 07-Oct-00 16:00:00 GMT"></meta>

<font face="Arial" size="2">

  <%@ include file="fragments/internalPageAuth.frag" %>

<a href="<%=request.getContextPath()%>/internal/utilityLauncher.jsp">Return to Launcher Page</a>
<hr>


<%
     String serverLocation = JPylonProperties.getInstance().getString("serverLocation");
     String queryFileLocation = request.getParameter("queryFileLocation");
     String generatedFileLocation = request.getParameter("generatedLocation");
     String highestOid = request.getParameter("highestOid");
     String targetDBinfo = request.getParameter("targetDBinfo");
     String logFilePath = request.getParameter("logFilePath");
     String reportingUserSuffix = request.getParameter("reportingUserSuffix");
    
     // Start a new thread to kick off the utility
     // That way, it can run in the background.  The user must check the log
     // to see the results.
     Thread dataCopyThread = new Thread(new RefDataCopyUtility(serverLocation, queryFileLocation, generatedFileLocation, highestOid, targetDBinfo, logFilePath, reportingUserSuffix));
     dataCopyThread.start();
%>

The reference data copy utility is now executing.   Check the log file (location specified on previous page) for results.
