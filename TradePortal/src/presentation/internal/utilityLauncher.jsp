<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*, java.util.*" %>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>

<%--
 * This page provides a way to launch Trade Portal utilities that require access 
 * to all Trade Portal classes.   All input data for the utilities are entered here,
 * and then when the button is pressed it is submitted to another JSP, which actually calls
 * the utility classes.
 *
 *     Copyright   2002                        
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>

<META HTTP-EQUIV= "expires" content = "Saturday, 07-Oct-00 16:00:00 GMT"></meta>


<font face="Arial" color="blue" size="3"><b>Trade Portal Utility Launch Page</b></font>
<br>
<font face="Arial" size="2" color="blue">This page is a data entry and launching point for several 
utility programs that must have access to all Trade Portal classes
<br>
<br>

<%
      // Determine the number of app servers
      // For some utilities, it needs to be run against all of the app servers
      PropertyResourceBundle portalProperties = (PropertyResourceBundle) PropertyResourceBundle.getBundle("TradePortal");

      String numServers = null;

      try
       {
          numServers = portalProperties.getString("numberOfAppServers");
       }
      catch(MissingResourceException mre)
       {  }

      int appServerCount;

      if((numServers == null) || (numServers.equals("")))
        appServerCount = 1;
      else
        appServerCount = Integer.parseInt(numServers);
%>

<%--  UPDATE SECURITY CONFIG UTILITY --%>

<hr>
<font face="Arial" size="2">
<b>Update Cached Security Parameters and Certificates</b>
<form name="securityConfigForm" action="<%=request.getContextPath()%>/internal/updateSecurityConfig.jsp">

<table>
<tr><td><font face='Arial' size='2'>Internal Utility Password </font></td><td><input type="password" name="password" /></td></tr>
<%
   for(int i=1; i <= appServerCount; i++)
     out.print("<tr><td><font face='Arial' size='2'>Server Location "+i+": </font></td><td><input type=text name='serverLocation"+i+"'></td></tr>");
%>
</table>
<br>
   <input type=submit value="Update Cached Security Data" />

</form>

<%--  UPDATE PERFORMANCE STATISTIC CONFIG DATA --%>

<hr>
<font face="Arial" size="2">
<b>Update Performance Statistic Configuration Data</b>
<form name="perfStatForm" action="<%=request.getContextPath()%>/internal/updatePerfStatConfig.jsp">

<table>
<tr><td><font face='Arial' size='2'>Internal Utility Password </font></td><td><input type="password" name="password" /></td></tr>
<%
   for(int i=1; i <= appServerCount; i++)
     out.print("<tr><td><font face='Arial' size='2'>Server Location "+i+": </font></td><td><input type=text name='serverLocation"+i+"'></td></tr>");
%>
</table>
<br>
   <input type=submit value="Update Performance Statistic Configuration Data" />
</form>


<%--  UPDATE REFERENCE DATA CODES UTILITY --%>

<hr>
<b>Update Cached Reference Data from Database</b>

<form name="refdataForm" action="<%=request.getContextPath()%>/internal/updateRefdata.jsp">
<table>
<tr><td><font face='Arial' size='2'>Internal Utility Password </font></td><td><input type="password" name="password" /></td></tr>
<tr><td>&nbsp;</td><td>&nbsp;</td></tr>
<%
   for(int i=1; i <= appServerCount; i++)
     out.print("<tr><td><font face='Arial' size='2'>Server Location "+i+": </font></td><td><input type=text name='serverLocation"+i+"'></td></tr>");
%>
</table>
<br>
   <input type=submit name="Update Refdata" value="Update Cached Refdata" />
</form>


<%--  CORPORATE CUSTOMER CONVERSION UTILITY --%>

<hr>
<b>Corporate Customer Conversion Utility</b>

<form name="conversionUtil" action="<%=request.getContextPath()%>/internal/runConversionUtility.jsp">
<br>
<table>
<tr><td><font face="Arial" size="2"><b>Parameter</td><td><font face="Arial" size="2"><b>Example</td><td><font face="Arial" size="2"><b>Field</td></tr>
<tr><td><font face='Arial' size='2'>Internal Utility Password </font></td><td><font face="Arial" size="2">*******</td><td><input type="password" name="password" /></td></tr>
<tr><td><font face="Arial" size="2">Client Bank ID: </td><td><font face="Arial" size="2">XYZ</td><td><input type="text" name="clientBankId" /></td></tr>
<tr><td><font face="Arial" size="2">Path to Input CSV File on App Server: </td><td><font face="Arial" size="2">D:\ccutility\input.csv</td><td><input type="text" name="inputCSVFileLocation" /></td></tr>
<tr><td><font face="Arial" size="2">Path to Mapper CSV File on App Server: </td><td><font face="Arial" size="2">D:\ccutility\mapper.csv</td><td><input type="text" name="mapperFileLocation" /></td></tr>
<tr><td><font face="Arial" size="2">File Type: </td><td><font face="Arial" size="2">PHRASE</td><td><input type="text" name="fileType" /></td></tr>
<tr><td><font face="Arial" size="2">User Identifier: </td><td><font face="Arial" size="2">JOHNDOE</td><td><input type="text" name="userIdentifier" /></td></tr>
<tr><td><font face="Arial" size="2">Max Errors: </td><td><font face="Arial" size="2">20</td><td><input type="text" name="maxErrors" /></td></tr>

</table>
<br>
   <input type=submit name="Run Conversion Utility" value="Run Conversion Utility" />
</form>


<%--  REFERENCE DATA COPY UTILITY --%>

<hr>
<b>Reference Data Copy Utility</b>

<form name="dataMigrationTool" action="<%=request.getContextPath()%>/internal/runRefDataCopyUtility.jsp">
<br>
<table>
<tr><td><font face="Arial" size="2"><b>Parameter</td><td><font face="Arial" size="2"><b>Example</td><td><font face="Arial" size="2"><b>Field</td></tr>
<tr><td><font face='Arial' size='2'>Internal Utility Password </font></td><td><font face="Arial" size="2">*******</td><td><input type="password" name="password" /></td></tr>
<tr><td><font face="Arial" size="2">Path to Query File on Source App Server: </td><td><font face="Arial" size="2">D:\copyBankData.sql</td><td><input type="text" name="queryFileLocation" /></td></tr>
<tr><td><font face="Arial" size="2">Path to Generated Files on App Server: </td><td><font face="Arial" size="2">D:\refDataCopy</td><td><input type="text" name="generatedLocation" /></td></tr>
<tr><td><font face="Arial" size="2">Highest OID in Target Database: </td><td><font face="Arial" size="2">102453</td><td><input type="text" name="highestOid" /></td></tr>
<tr><td><font face="Arial" size="2">Login String for Target Database: </td><td><font face="Arial" size="2">scott/tiger@portaldb</td><td><input type="text" name="targetDBinfo" /></td></tr>
<tr><td><font face="Arial" size="2">Log File Location on Source App Server: </td><td><font face="Arial" size="2">D:\copyUtil.log</td><td><input type="text" name="logFilePath" /></td></tr>
<tr><td><font face="Arial" size="2">Reporting User Suffix in Target DB: </td><td><font face="Arial" size="2">ANZ/</td><td><input type="text" name="reportingUserSuffix" /></td></tr>
</table>
<br>
   <input type=submit name="Run Refdata Copy Utility" value="Run Refdata Copy Utility" />
</form>

<%--  CACHE MANAGER UTILITY --%>
 <%@ include file="/internal/fragments/cacheFlusher.frag" %>
 <%
    String url = request.getContextPath() + "/internal/cacheMgr.jsp"; 
  %>

<hr>
<b>Cache Manager Utility</b>

<form name="cacheManagerUtility" action="javascript:openCacheFlusher('<%=url%>')">
<br>
<table>

<tr><td><font face='Arial' size='2'>Internal Utility Password </font></td><td><font face="Arial" size="2">*******</td><td><input type="password" name="password" /></td></tr>

</table>
<br>
   <input type=submit name="Open Cache Manager Utility" value="Open Cache Manager Utility" />
</form>




<%--  SLA Monitor Perf Stat Config MANAGER UTILITY --%>
 <%@ include file="/internal/fragments/SLAMonitorConfigUpdater.frag" %>
 <%
    String url1 = request.getContextPath() + "/internal/SLAMonitorConfigUpdater.jsp"; 
  %>

<hr>
<b>SLA Monitor Config Utility</b>

<form name="cacheManagerUtility" action="javascript:openSLAMonitorConfigUpdater('<%=url1%>')">
<br>
<table>

<tr><td><font face='Arial' size='2'>Internal Utility Password </font></td><td><font face="Arial" size="2">*******</td><td><input type="password" name="password" /></td></tr>

</table>
<br>
   <input type=submit name="Open SLA Monitor Config Updater" value="Open SLA Monitor Config Updater" />
</form>







