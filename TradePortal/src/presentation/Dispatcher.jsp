<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%-- cquinton 2/16/2012 Rel 8.0 pxx255 start
     add userSession bean --%>
<%@ page import="java.io.*,com.amsinc.ecsg.web.*,com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,java.util.*,com.ams.tradeportal.common.Debug,
				 com.ams.tradeportal.common.TradePortalConstants,com.ams.tradeportal.common.SecurityRules, com.ams.tradeportal.common.AutoExtendSession" %>
<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session" />
<%-- cquinton 2/16/2012 Rel 8.0 pxx255 end --%>



<%
	//RKAZI CMA SESSION SYNC REL 9.1 10/23/2014 - Start
	String autoSaved = request.getParameter("autoSaved");
	if (TradePortalConstants.INDICATOR_YES.equals(SecurityRules.getAutoExtendSession()) && !"Y".equals(autoSaved) ){
		HttpSession thisSession = request.getSession(false);
		thisSession.setAttribute("currentSessionId", thisSession.getId());
		request.getSession(false).setMaxInactiveInterval(SecurityRules.getTimeOutPeriodInSeconds());
		Debug.debug("Dispatcher.jsp called at : ====> " + (new java.util.Date()).toGMTString());
		Debug.debug("Session ID in Dispatcher.jsp: ====> " + thisSession.getId());
		Debug.debug("Last Accessed Time  in Dispatcher.jsp: ====> " + thisSession.getLastAccessedTime());
		Debug.debug("Current System Time in Milli seconds  in Dispatcher.jsp: ====> " + System.currentTimeMillis());
		Debug.debug("Session Max In Active Interval seconds  in Dispatcher.jsp: ====> " + thisSession.getMaxInactiveInterval());
	}
	//RKAZI CMA SESSION SYNC REL 9.1 10/23/2014 - End
	
	String  cStartTime = request.getParameter("cTime");
	if (cStartTime != null && StringFunction.isNotBlank(cStartTime)){
    	request.setAttribute("cTime",cStartTime);
    }
    String  prevPage = request.getParameter("prevPage");
	if (prevPage != null && StringFunction.isNotBlank(prevPage)){
    	request.setAttribute("prevPage",prevPage);
    }

	//RKAZI IR 47582 24-03-2016 REL 9.5 - START
	session.removeAttribute("prevSortColumnMap");
	Map<String,String> prevSortColumnMap = null;
    if(prevSortColumnMap ==null){
        prevSortColumnMap = new HashMap<String,String>();
        session.setAttribute("prevSortColumnMap",prevSortColumnMap);
    }
   //RKAZI IR 47582 24-03-2016 REL 9.5 - END
  
	String nextPhysicalPage = null;
    int debugMode;

    // Create a performance statistic object to keep track of 
    // start time, action taken, and stop time
    PerformanceStatistic perfStat = new PerformanceStatistic ();

    // Place the statistic object on the session so that other code
    // will have access to it
    session.setAttribute("performanceStatistic", perfStat);

    // Start the timer
    perfStat.startTimer();
    
    // W Zhu 5/9/2012 WNUM050768991 add logging info
    Debug.debug("Dispatcher session.getId()="+session.getId()+"; request.getParameter(\"ni\")="+request.getParameter("ni")+"; request.getCookies()="+ Arrays.toString(request.getCookies()));

    //cquinton 2/4/2012 Rel 8.0 ppx255 start
    //check the session to see if the user has been forced to logout
    boolean forceLogout = false;
    String activeUserSessionOid = userSession.getActiveUserSessionOid();
    if ( activeUserSessionOid != null ) {
        try {
            long startCheckForceLogout = System.currentTimeMillis();
            //directly check the database to avoid ejb caching
            String mySql = "select force_logout from active_user_session where active_user_session_oid=?";
            String forceLogoutInd = "N";
            DocumentHandler myResult = DatabaseQueryBean.getXmlResultSet(mySql, false, new Object[]{activeUserSessionOid});
            if( myResult!=null ) {
                forceLogoutInd = myResult.getAttribute("/ResultSetRecord(0)/FORCE_LOGOUT");
                if ( "Y".equals( forceLogoutInd ) ) {
                    forceLogout = true;
                }
                long stopCheckForceLogout = System.currentTimeMillis();
                long checkForceLogoutMillis = stopCheckForceLogout - startCheckForceLogout;
                Debug.debug("Dispatcher CheckForceLogoutMillis -> forceLogoutInd=" + forceLogoutInd + " time in milliseconds:" + checkForceLogoutMillis);
            } else {
                Debug.debug("Dispatcher CheckForceLogoutMillis -> Problem checking force logout. Got null result." );
            }
        } catch ( Exception ex ) {
            Debug.debug("Dispatcher CheckForceLogoutMillis -> Problem checking force logout: " + ex.toString() );
        }
    } else {
        Debug.debug("Dispatcher CheckForceLogoutMillis -> No activeUserSessionOid - not checking force logout");
    }
    //cquinton 2/4/2012 Rel 8.0 ppx255 end
      
    try {
        request.setAttribute("originalPage", formMgr.getOriginalPage());
        ServletContext context = this.getServletConfig().getServletContext();
        response.setContentType("text/html");
        NavigationManager nif = NavigationManager.getNavMan();
        String currPage = request.getParameter("cp");
        if (currPage != null) {
            formMgr.setCurrPage(currPage);
        }
        //cquinton 2/4/2012 Rel 8.0 ppx255 start
        //conditionally force logout
        if ( !forceLogout ) {
            RequestDispatcher reqDispatcher = context.getRequestDispatcher("/AmsServlet");
            reqDispatcher.include(request,response); 
            
            String templateAutoSaved = request.getParameter("templateAutoSaved");
            if (TradePortalConstants.INDICATOR_YES.equals(SecurityRules.getAutoExtendSession()) 
            		&& ("Y".equals(autoSaved) && templateAutoSaved != null) && !"PageNotAvailable".equals(formMgr.getCurrPage()) ){
            	Debug.debug("Dispatcher.jsp : Calling AutoExtendSession for Auto Saving template ==> " + templateAutoSaved);
            	nextPhysicalPage = new AutoExtendSession().getNextPhysicalPage(request, formMgr, templateAutoSaved, nif, formMgr.getNavigateToPage());
            }else{
            	nextPhysicalPage = nif.getPhysicalPage(formMgr.getNavigateToPage(), request);
            }
            formMgr.debug("*Dispatcher.jsp* about to forward to " + nextPhysicalPage);
        } else {
            //force logout
            nextPhysicalPage = nif.getPhysicalPage( "ForceLogout", request );
            formMgr.setCurrPage( "ForceLogout" );
            formMgr.debug("*Dispatcher.jsp* forcing session logout");
        }
        //cquinton 2/4/2012 Rel 8.0 ppx255 end
    } catch (Exception e) {
        System.out.println("exception caught in Dispatcher.jsp");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        PrintWriter writer = new PrintWriter(bytes, true);
        e.printStackTrace(writer);
        System.out.println(bytes.toString());
    }

    pageContext.forward(nextPhysicalPage); 

    // After the page that the user sees is done being
    // generated and added to the output stream, stop the
    // timer.  This also places the statistic into a cache so
    // that it will be saved.


    if (perfStat != null ){
        String context = perfStat.getContext();
        formMgr.debug("Dispatcher.jsp: context ===> " + context);
        String action = perfStat.getAction();
        formMgr.debug("Dispatcher.jsp: action ===> " + action);
    	if(("LogonMediator".equals(context) || "ReLogonMediator".equals(context)) && "TradePortalHome".equals(action)){
    		perfStat.setActionType("Login");
    		perfStat.setAction("SystemLogin");
            perfStat.setContext("TradePortalHome");
        }
    }
    formMgr.debug("Dispatcher.jsp: Perfstat ===> " + perfStat.toString());
    perfStat.stopTimer();
%>
