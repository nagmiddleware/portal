<%--
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
--%>
<%--
Ravindra - Rel8200 CR-708B - Initial Version
**********************************************************************************
                        Transactions Home History Tab

  Description:
    Contains HTML to create the History tab for the Transactions Home page

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transactions-HistoryListView.jsp" %>
*******************************************************************************
--%>

<%
      secureParms.put("UserOid",        userSession.getUserOid());
      secureParms.put("OrgOid",        userSession.getOwnerOrgOid());

      String newDropdownSearch = request.getParameter("NewDropdownSearch");
      Debug.debug("New Dropdown Search is " + newDropdownSearch);

      String searchType   = request.getParameter("SearchType");

      String selectedStatus = (String)session.getAttribute("spInvoiceHistoryStatusType");

      if (selectedStatus == null)
      {
            selectedStatus = TradePortalConstants.STATUS_ALL;
      }

      session.setAttribute("spInvoiceHistoryStatusType", selectedStatus);


      helpSensitiveLink = OnlineHelp.createContextSensitiveLink("customer/invoices.htm",  resMgr, userSession);

      String newSearchTrans = request.getParameter("NewSearch");

      loginLocale = userSession.getUserLocale();

   //rbhaduri - CR-374 - 14th Oct 08 - added options variables for invoice tab, Amount Type and Date Type dropdown
   String optionsAmountType = null;
   String optionsDateType = null;
   String invoiceID = null;

   StringBuffer statusExtraTags = new StringBuffer();
  // StringBuffer newSearchReceivableCriteria = new StringBuffer();

   
   String statusOptions = Dropdown.createSortedRefDataOptions(TradePortalConstants.SUPPLIER_PORTAL_INVOICE_STATUS, selectedStatus, loginLocale);
%>

<div class="gridSearch">
  <div class="searchHeader">
    <span class="searchHeaderCriteria">
      <%= widgetFactory.createSearchSelectField("invoiceStatusType", "InvoiceSearch.Status", "", statusOptions,"onChange='searchInvoices();'") %>
	<%
	  StringBuffer invCriteria = new StringBuffer();
	  invCriteria.append(widgetFactory.createCheckboxField("InActive", "InvoiceSearch.ShowInactive", false, false, false, "", "", "none")); //Added for Rel9.0 CR 913
	  widgetFactory.wrapSearchItem(invCriteria, "");//no extra classes
	%>
      <%=invCriteria.toString()%>

    </span>
    <span class="searchHeaderActions">
      <jsp:include page="/common/gridShowCount.jsp">
        <jsp:param name="gridId" value="<%=gridId%>" />
      </jsp:include>   
	
      <span id="invoicesOfferedRefresh" class="searchHeaderRefresh"></span>
 	  <%=widgetFactory.createHoverHelp("invoicesOfferedRefresh", "RefreshImageLinkHoverText") %>
      <span id="invoicesOfferedGridEdit" class="searchHeaderEdit"></span>
      <%=widgetFactory.createHoverHelp("invoicesOfferedGridEdit","CustomizeListImageLinkHoverText") %>  
     </span>
    <div style="clear:both;"></div>
  </div>


<%
  gridHtml = dgFactory.createDataGrid("invoicesOfferesHistoryGrid","InvoicesOfferedHistoryGrid", null);
  gridLayout = dgFactory.createGridLayout("invoicesOfferesHistoryGrid", "InvoicesOfferedHistoryGrid");
  initSearchParms="selectedStatus="+selectedStatus;
%>

<div class="searchDivider"></div>

<%@ include file="/invoiceoffers/fragments/InvoiceOffersHistorySearchFilter.frag" %>
</div>
<input type=hidden name=SearchType value="Y">
<%=gridHtml%>
