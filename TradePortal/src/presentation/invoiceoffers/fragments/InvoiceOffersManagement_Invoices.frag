<%--
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
--%>
<%--
Ravindra - Rel8200 CR-708B - Initial Version
**********************************************************************************
                        Transactions Home History Tab

  Description:
    Contains HTML to create the History tab for the Transactions Home page

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transactions-HistoryListView.jsp" %>
*******************************************************************************
--%>

<%
      secureParms.put("UserOid",        userSession.getUserOid());
      secureParms.put("OrgOid",        userSession.getOwnerOrgOid());

      String newDropdownSearch = request.getParameter("NewDropdownSearch");
      Debug.debug("New Dropdown Search is " + newDropdownSearch);

      String searchType   = request.getParameter("SearchType");


      String selectedStatus = (String)session.getAttribute("spInvoiceStatusType");

      if (selectedStatus == null)
      {
            selectedStatus = TradePortalConstants.STATUS_ALL;
      }

      session.setAttribute("spInvoiceStatusType", selectedStatus);

      helpSensitiveLink = OnlineHelp.createContextSensitiveLink("customer/invoices.htm",  resMgr, userSession);

      String newSearchTrans = request.getParameter("NewSearch");
    
      loginLocale = userSession.getUserLocale();

   //rbhaduri - CR-374 - 14th Oct 08 - added options variables for invoice tab, Amount Type and Date Type dropdown
   String optionsAmountType = null;
   String optionsDateType = null;
   String invoiceID = null;

   StringBuffer statusExtraTags = new StringBuffer();
  // StringBuffer newSearchReceivableCriteria = new StringBuffer();

   // Build the status dropdown options
   // re-selecting the most recently selected option.
  
  	  
  	  // Start Vishal Sarkary IR-27351
  	 StringBuffer invoiceStatus =null;
  	 Vector codesToInclude =null;
	 
	 codesToInclude = new Vector();
	 codesToInclude.add(TradePortalConstants.STATUS_ALL);
	 codesToInclude.add(TradePortalConstants.SP_STATUS_BUYER_APPROVED);
	 codesToInclude.add(TradePortalConstants.SP_STATUS_OFFER_ACCEPTED);
	 codesToInclude.add(TradePortalConstants.SP_STATUS_FVD_ASSIGNED);
	 codesToInclude.add(TradePortalConstants.SP_STATUS_PARTIALLY_AUTHORIZED); 
     codesToInclude.add(TradePortalConstants.SP_STATUS_REJECTED_BY_BANK);
    
     invoiceStatus = new StringBuffer(Dropdown.createSortedRefDataIncludeOptions(TradePortalConstants.SUPPLIER_PORTAL_INVOICE_STATUS,
                                             selectedStatus, resMgr,loginLocale, codesToInclude,false));
		
		
		//END Vishal Sarkary IR-27351 		  
				  

    // Get buyer info from the invoice.  This seller belongs to only one buyer so all the invoices are for the same buyer.
    StringBuilder buyerSQL = new StringBuilder().append("select min(buyer_party_identifier) buyer_party_identifier from invoice")
        .append(" where a_corp_org_oid = ? and buyer_party_identifier is not null");
    DocumentHandler buyerDoc = DatabaseQueryBean.getXmlResultSet(buyerSQL.toString(), false, new Object[]{userOrgOid} );
    String buyerID = "";
    if (buyerDoc!=null) {
        buyerID = buyerDoc.getAttribute("/ResultSetRecord(0)/BUYER_PARTY_IDENTIFIER");
    }

%>

<div class="gridSearch">
  <div class="searchHeader">
    <span class="searchHeaderCriteria">
      <%= widgetFactory.createSearchSelectField("invoiceStatusType", "InvoiceSearch.Status", "", invoiceStatus.toString(),"onChange='searchInvoices();'") %>
	<%
	  StringBuffer invCriteria = new StringBuffer();
	  widgetFactory.wrapSearchItem(invCriteria, "");//no extra classes
	%>
      <%=invCriteria.toString()%>

    </span>
    <%--IR 23181 <span class="pageSubHeader"> --%>
    <span class="formItem readOnly">
       &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=resMgr.getText("InvoicesOffered.Buyer", TradePortalConstants.TEXT_BUNDLE)%>
      <%=StringFunction.xssCharsToHtml(buyerID)%>
    </span>

    <span class="searchHeaderActions">
      <jsp:include page="/common/gridShowCount.jsp">
        <jsp:param name="gridId" value="<%=gridId%>" />
      </jsp:include>
	   <span id="invoicesOfferedRefresh" class="searchHeaderRefresh"></span>
 	  <%=widgetFactory.createHoverHelp("invoicesOfferedRefresh", "RefreshImageLinkHoverText") %>
      <span id="invoicesOfferedGridEdit" class="searchHeaderEdit"></span>
      <%=widgetFactory.createHoverHelp("invoicesOfferedGridEdit","CustomizeListImageLinkHoverText") %>      
    </span>
    <div style="clear:both;"></div>
  </div>


<%
  gridHtml = dgFactory.createDataGridMultiFooter("invoicesOfferedGrid","InvoicesOfferedDataGrid", null);
  gridLayout = dgFactory.createGridLayout("invoicesOfferedGrid", "InvoicesOfferedDataGrid");
  initSearchParms="selectedStatus="+selectedStatus;
%>

<div class="searchDivider"></div>

<%@ include file="/invoiceoffers/fragments/InvoiceOffersSearchFilter.frag" %>
</div>
<input type=hidden name=SearchType value="Y">
<%=gridHtml%>
