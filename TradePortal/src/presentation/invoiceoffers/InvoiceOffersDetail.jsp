<%--
Ravindra - Rel8200 CR-708B - Initial Version
*******************************************************************************
                        Invoice Detail

  Description:
     This page is used to display the detail for an invoice.
  Although it is modeled after the standard ref data pages, this is a readonly
  page.  It is only used to display data and can never update anything.

*******************************************************************************
--%>

<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="java.util.*,com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,com.ams.tradeportal.busobj.*,
                 com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.html.*" %>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" 
   scope="session">
</jsp:useBean>


<%-- ************** Data retrieval page setup begins here ****************  --%>

<%
  boolean isReadOnly = true;

InvoiceWebBean_Base InvoicesSummaryData = beanMgr.createBean(InvoiceWebBean_Base.class, "Invoice");
  
  DocumentHandler doc;
  String buyerInfo = null;
  String sellerInfo = null;
  String currency = null;
  String current2ndNav      = null;
  String            helpSensitiveLink      = null;
  DocumentHandler invoiceGoodsResult = null;
  //MEer Rel 8.3 T36000021992 
  String corpOrgOid = userSession.getOwnerOrgOid();
  boolean ignoreDefaultPanelValue = false;

  InvoiceWebBean invoice = beanMgr.createBean(InvoiceWebBean.class, "Invoice");
  String invoiceOid = request.getParameter("invoice_oid");
  if (invoiceOid != null) {
	  invoiceOid = EncryptDecrypt.decryptStringUsingTripleDes(invoiceOid, userSession.getSecretKey());
   }
  invoice.getById(invoiceOid);
  
                       		  
  // Get the webbeans for the login in user and his security profile.  Set the
  // readonly access appropriately.
  String loginLocale = userSession.getUserLocale();
  current2ndNav = request.getParameter("current2ndNav");
  // Get the document from the cache.  We'll use it to repopulate the screen 
  // if the user was entering data with errors.
  doc = formMgr.getFromDocCache();

  Debug.debug("doc from cache is " + doc.toString());
  helpSensitiveLink = OnlineHelp.createContextSensitiveLink("customer/invoices.htm",  resMgr, userSession);
	
 	//Sandeep Rel8.2 CR708BC IR#T36000015624 04/17/2013 - Start 
  	String linkArgs[] = {"","","","","",""};
  	String linkString = "";
  	
  	if ((!userSession.getSecurityType().equals(TradePortalConstants.ADMIN))) {
      linkArgs[0] = TradePortalConstants.PDF_INVOICE_OFFERED_DETAILS;
      linkArgs[1] = EncryptDecrypt.encryptStringUsingTripleDes(invoiceOid);
      linkArgs[2] = loginLocale;
      linkArgs[3] = userSession.getBrandingDirectory();
      linkString =  formMgr.getDocumentLinkAsURLInFrame(resMgr.getText("InvoicesOfferedDetail.PrintInvoiceDetails",
				TradePortalConstants.TEXT_BUNDLE),
				TradePortalConstants.PDF_INVOICE_OFFERED_DETAILS,
				linkArgs,
				response);
	}
  	//Sandeep Rel8.2 CR708BC IR#T36000015624 04/17/2013 - End
  
    String invoiceStatus= invoice.getAttribute("supplier_portal_invoice_status");
    if(InstrumentServices.isNotBlank(invoiceStatus)){
            invoiceStatus = ReferenceDataManager.getRefDataMgr().getDescr(
                                                          "SUPPLIER_PORTAL_INVOICE_STATUS", 
                                                          invoice.getAttribute("supplier_portal_invoice_status"), loginLocale);
    }

  %>

<%-- ********************* HTML for page begins here *********************  --%>



<%-- Body tag included as part of common header --%>
<% WidgetFactory widgetFactory = new WidgetFactory(resMgr); %>
		
<jsp:include page="/common/Header.jsp">
  <jsp:param name="includeNavigationBarFlag" 
             value="<%=TradePortalConstants.INDICATOR_YES%>" />
  <jsp:param name="includeErrorSectionFlag"  
             value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>
<%
  // Set up page navigation stack
  if ( "true".equals( request.getParameter("returning") ) ) {
    userSession.pageBack(); 
  }
  else {
    userSession.addPage("goToInvoiceOfferedDetail", request);
  }

   SessionWebBean.PageFlowRef backPage = userSession.peekPageBack();
   String returnAction = backPage.getLinkAction();
   String returnParms = backPage.getLinkParametersString();
   returnParms+="&returning=true";
%>
		
	<div class="pageMain">
  		<div class="pageContent">
    		<div class="pageSubHeader">
      			<span class="pageSubHeaderItem"> 
			        <%=resMgr.getText("InvoicesOffered.InvoiceID",TradePortalConstants.TEXT_BUNDLE)%> : 
                                <%=StringFunction.xssCharsToHtml(invoice.getAttribute("invoice_reference_id"))%> - (<%=StringFunction.xssCharsToHtml(invoiceStatus)%>)			        
      			</span>
     			<span class="pageSubHeader-right">
		      		<button data-dojo-type="dijit.form.Button" name="CloseButton" id="CloseButton" type="button" class="pageSubHeaderButton">
		          		<%=resMgr.getText("common.CloseText",TradePortalConstants.TEXT_BUNDLE)%>
		          		<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
            					document.location.href = '<%= formMgr.getLinkAsUrl(returnAction, returnParms, response) %>';
          				</script>
		        	</button> 	
		        	<%=widgetFactory.createHoverHelp("CloseButton","InvoiceDetailCloseHoverText") %>
		        	<button data-dojo-type="dijit.form.Button" name="print" id="print" type="button" class="pageSubHeaderButton">
          				<%=resMgr.getText("common.PrintText",TradePortalConstants.TEXT_BUNDLE)%>
          					<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
           						window.open( "<%=linkString%>");
		  					</script>
        			</button> 
        			<%=widgetFactory.createHoverHelp("print", "PrintHoverText") %>
      			</span>
      			<div style="clear:both;"></div>
   	 		</div>

			<form name="InvoiceDetail" method="post" id="InvoiceDetail" data-dojo-type="dijit.form.Form" action="<%=formMgr.getSubmitAction(response)%>">
 		 		<input type=hidden value="" name=buttonName>

			<%
				  // Store values such as the userid, his security rights, and his org in a secure
				  // hashtable for the form.  The mediator needs this information.
				  Hashtable secureParms = new Hashtable();
				  secureParms.put("UserOid", userSession.getUserOid());
				  currency = invoice.getAttribute("currency_code");
			%>

				<%= formMgr.getFormInstanceAsInputField("InvoicesForm", secureParms) %>
				<div class="formMainHeader">
			<%
			        String link=formMgr.getLinkAsUrl("goToReceivablesManagement", response);
					String invoiceGroupName = (String)session.getAttribute("InvoiceGroupName");
			%>
				</div>
				
				<%-- IR T36000021686 Rel 8.3  --%>
				<%@ include file="/transactions/fragments/LoadPanelLevelAliases.frag" %>
				
				<div class="formContentNoSidebar">
					<div>
						<%= widgetFactory.createTextField( "InvoicesOfferedDetail.IssueDate", "InvoicesOfferedDetail.IssueDate",  TPDateTimeUtility.formatDate(invoice.getAttribute(
															"invoice_issue_datetime"), TPDateTimeUtility.LONG,loginLocale), "30", isReadOnly, false, false,  "", "","inline" ) %>
						<%= widgetFactory.createTextField( "InvoicesOfferedDetail.DueDate", "InvoicesOfferedDetail.DueDate", TPDateTimeUtility.formatDate(invoice.getAttribute(
															"invoice_due_date"), TPDateTimeUtility.LONG,loginLocale), "30", isReadOnly,false, false,  "", "","inline" ) %>
						<%= widgetFactory.createTextField( "InvoicesOfferedDetail.PaymentDate", "InvoicesOfferedDetail.PaymentDate", TPDateTimeUtility.formatDate(invoice.getAttribute
															("invoice_payment_date"), TPDateTimeUtility.LONG,loginLocale), "30", isReadOnly, false, false,  "", "","inline" ) %>
						<%= widgetFactory.createTextField( "InvoicesOfferedDetail.Ccy", "InvoicesOfferedDetail.Ccy", invoice.getAttribute("currency_code"), "30", isReadOnly, false, false, 
															"", "","inline" ) %>
						<%= widgetFactory.createTextField( "InvoicesOfferedDetail.InvoiceAmount", "InvoicesOfferedDetail.InvoiceAmount", TPCurrencyUtility.getDisplayAmount(
															invoice.getAttribute("invoice_total_amount"), currency, loginLocale), "30", isReadOnly, false, false,  "", "","inline" ) %>
															
						<%= widgetFactory.createTextField( "InvoicesOffered.PaymentAmount", "InvoicesOffered.PaymentAmount", TPCurrencyUtility.getDisplayAmount(
															invoice.getAttribute("invoice_payment_amount"), currency, loginLocale), "30", isReadOnly, false, false,  "", "","inline" ) %>
						<%= widgetFactory.createTextField( "InvoicesOffered.CreditNoteAppliedAmt", "InvoicesOffered.CreditNoteAppliedAmt", TPCurrencyUtility.getDisplayAmount(
															invoice.getAttribute("total_credit_note_amount"), currency, loginLocale), "30", isReadOnly, false, false,  "", "","inline" ) %>
						<%= widgetFactory.createTextField( "InvoicesOffered.AmountAfterAdj", "InvoicesOffered.AmountAfterAdj", TPCurrencyUtility.getDisplayAmount(
															invoice.getAttribute("inv_amount_after_adj"), currency, loginLocale), "30", isReadOnly, false, false,  "", "","inline" ) %>									
															
						<%= widgetFactory.createTextField( "InvoicesOfferedDetail.TotalNetAmount", "InvoicesOfferedDetail.TotalNetAmount", TPCurrencyUtility.getDisplayAmount(
															invoice.getAttribute("net_amount_offered"), currency, loginLocale), "30", isReadOnly, false, false,  "", "","inline" ) %>
						<%= widgetFactory.createTextField( "InvoicesOfferedDetail.InvoiceStatus", "InvoicesOfferedDetail.InvoiceStatus",invoiceStatus , "30",isReadOnly, false, 
															false,  "", "","inline" ) %>
						<%= widgetFactory.createTextField( "InvoicesOfferedDetail.InvoiceGroup", "InvoicesOfferedDetail.InvoiceGroup", invoiceGroupName, "30", isReadOnly, false, false, 
															"", "","inline" ) %>
						<div style="clear:both;"></div>
					</div>
			  
					<%= widgetFactory.createTextField( "InvoicesOfferedDetail.RejectedInvoiceReason", "InvoicesOfferedDetail.RejectedInvoiceReason",invoice.getAttribute("rejection_reason_text") , 
												"30", isReadOnly, false, false, "", "","" ) %>
		
					<div class="formContentNoBorder">
						<div class="columnLeftNoBorder">
					 		<%=widgetFactory.createSubsectionHeader("InvoicesOfferedDetail.Buyer")%>
			   		  		<div class="formItem">
				         		<%=StringFunction.xssCharsToHtml(invoice.getAttribute("buyer_party_identifier"))%>  <br/>
				         		<% buyerInfo = invoice.getAttribute("buyer_address_line_1") + "<br>" + 
				  	      		 	invoice.getAttribute("buyer_address_line_2") + "<br>" + 
				  	      		 	invoice.getAttribute("buyer_address_country"); %>
				          		<%= buyerInfo %>  
			          		</div>                  
			        	</div>
			        	
			       	 	<div class="columnRightNoBorder">
			        		<%=widgetFactory.createSubsectionHeader("InvoicesOfferedDetail.Seller")%>
			         		<div class="formItem">
			            		<%=StringFunction.xssCharsToHtml(invoice.getAttribute("seller_party_identifier"))%><br/>
			            		<% sellerInfo = invoice.getAttribute("seller_address_line_1") + "<br>" + 
			  	      		  		invoice.getAttribute("seller_address_line_2") + "<br>" + 
			  	      		  		invoice.getAttribute("seller_address_country"); %>
			          			<%= sellerInfo %>
			           		</div>                 
			         	</div>
			         	<div style="clear: both"></div>
			       </div>  
			    <%// jgadela Rel 92 - SQL Injection fix
				    String invoiceGoodsSQL = "select * from invoice_goods where p_invoice_oid = ? ";
				    invoiceGoodsResult = DatabaseQueryBean.getXmlResultSet(invoiceGoodsSQL, false, new Object[]{invoiceOid});
			    %>		
				 	<div class="formContent">
					 	<span class="columnLeftNoBorder" style="width: 78%;">
					  		<%=widgetFactory.createWideSubsectionHeader("InvoicesOfferedDetail.InvoiceSummaryDetail") %>  
					 	
						 	<%
						 		if( invoiceGoodsResult != null ) {
						 	    	Vector listviewVector = invoiceGoodsResult.getFragments("/ResultSetRecord/");
						 	     	for(int i= 0; i<listviewVector.size(); i++) {
						 				//Sandeep Rel-8.2 CR-708BC IR# T36000015268 04/03/2013 - Start
						 	     		String purchaseOrderID = ((DocumentHandler)listviewVector.get(i)).getAttribute("/PO_REFERENCE_ID");
						 				String purchaseOrderIssueDate = ((DocumentHandler)listviewVector.get(i)).getAttribute("/PO_ISSUE_DATETIME");
                                                                         String goodsDescription = ((DocumentHandler)listviewVector.get(i)).getAttribute("/GOODS_DESCRIPTION");
						 				//Sandeep Rel-8.2 CR-708BC IR# T36000015268 04/03/2013 - Start
						 	%>
									 	<div>
										 	<%= widgetFactory.createTextField( "InvoicesOfferedDetail.PurchaseOrderID", "InvoicesOfferedDetail.PurchaseOrderID",purchaseOrderID , 
																			"35", isReadOnly, false, false, "", "","inline" ) %>
										 	<%= widgetFactory.createTextField( "InvoicesOfferedDetail.PurchaseOrderIssueDate", "InvoicesOfferedDetail.PurchaseOrderIssueDate",  TPDateTimeUtility.formatDate(purchaseOrderIssueDate
									 								, TPDateTimeUtility.LONG,loginLocale), "30", isReadOnly, false, false,  "", "","formItemWithIndent5 inline" ) %>
											<div style="clear: both"></div>
                                            <%= widgetFactory.createTextField( "InvoicesOfferedDetail.GoodsDescription", "InvoicesOfferedDetail.GoodsDescription",goodsDescription , 
															"105", isReadOnly, false, false, "", "","" ) %>
											<div style="clear: both"></div>
											<%= widgetFactory.createLabel( "InvoicesOffered.SellerDefinedFields","InvoicesOffered.SellerDefinedFields",true, false, false, "", "") %>
											<div class="formItem readOnly">
											<table>
																
											<% for(int j = 0; j < 10; j++){
												if(StringFunction.isNotBlank(((DocumentHandler)listviewVector.get(i)).getAttribute("/SELLER_INFORMATION_LABEL" + j))){%>
												<tr>
													<td><%=StringFunction.xssCharsToHtml(((DocumentHandler)listviewVector.get(i)).getAttribute("/SELLER_INFORMATION_LABEL" + j))%></td>
													<td>&nbsp;</td>
													<td>&nbsp;</td>
													<td><%=StringFunction.xssCharsToHtml(((DocumentHandler)listviewVector.get(i)).getAttribute("/SELLER_INFORMATION" +j))%></td>
												</tr>
												<%
												}
												} %>
											</table>
											</div>
											<div style="clear: both"></div>
										</div> 	
																	
						    <%
						    		} 
						 		}
						 	%>
				  		</span>
				  	</div>
  					<div style="clear: both"></div>
				<%
					DGridFactory dgFactory = new DGridFactory(resMgr, userSession, formMgr, response);
				  	String gridHtml = dgFactory.createDataGridMultiFooter("invoiceDetailLogsGridId","InvoicesOfferedLogHistoryDataGrid", null);
				  	String gridLayout = dgFactory.createGridLayout("invoiceDetailLogsGridId", "InvoicesOfferedLogHistoryDataGrid");
				%>
  					<br/>
			  		<%=widgetFactory.createWideSubsectionHeader("InvoicesOfferedDetail.InvoiceLog", false,false, false, "") %>
			  		<div style='width: 98%' class ='formItemWithIndent3'>  
			  			<%=gridHtml%>   
			  		</div> 
  				</div>
			</form>
		</div>
	</div>

	<jsp:include page="/common/Footer.jsp">
	   <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
	   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
	</jsp:include>

<script type="text/javascript" src="/portal/js/datagrid.js"></script>

<script LANGUAGE="JavaScript">
require(["t360/OnDemandGrid", "dojo/dom-construct"], function(onDemandGrid, domConstruct){

	var gridLayout = <%=gridLayout%>;
	var initParms="invoiceOid=<%=EncryptDecrypt.encryptStringUsingTripleDes(invoiceOid, userSession.getSecretKey())%>";
	var viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("InvoicesOfferedLogHistoryDataView",userSession.getSecretKey())%>";  <%--Rel9.2 IR T36000032596 --%>
	onDemandGrid.createOnDemandGrid('invoiceDetailLogsGridId', viewName,gridLayout, initParms,1);
});
	
	<%-- IR T36000021686 Rel 8.3 - For performance improvement --%>
	  function panelLevelAliasesFormatter(columnValues){
		  var panelLevel = columnValues[0];
		  var panelAlias="";
		  if(panelLevel != ""){
			  panelAlias = panelStore.get(panelLevel).name;
		  }
		  return panelAlias;
		  
	  }
</script>
<jsp:include page="/common/dGridShowCountFooter.jsp">
  <jsp:param name="gridId" value="invoiceDetailLogsGridId" />
</jsp:include>
</body>
</html>
<%
  // Finally, reset the cached document to eliminate carryover of 
  // information to the next visit of this page.
  formMgr.storeInDocCache("default.doc", new DocumentHandler());

%>
