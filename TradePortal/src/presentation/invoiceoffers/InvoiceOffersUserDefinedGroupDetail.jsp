<%--
Ravindra - Rel8200 CR-708B - Initial Version
*******************************************************************************
                        Invoice Group Detail

  Description:
     This page is used to display the invoices of an invoice group.

*******************************************************************************
--%>

<%--
 *
 *     Copyright   2001
 *     American Management Systems, Incorporated
 *     All rights reserved
--%>
<%@ page import="java.util.*,com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.html.*" %>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session">
</jsp:useBean>


<%-- ************** Data retrieval page setup begins here ****************  --%>

<%
StringBuffer      onLoad                = new StringBuffer();
String            helpSensitiveLink     = null;
String            currentTabLink        = null;
String            currentTab            = TradePortalConstants.INV_GROUP_TAB;//BSL IR NLUM040376572 04/06/2012 CHANGE
String            formName              = null;
String            tabOn                 = null;
String            userOrgOid            = null;
String            userOid               = null;
String            userSecurityRights    = null;
String            userSecurityType      = null;
StringBuffer      dynamicWhereClause           = new StringBuffer();
StringBuffer      dropdownOptions              = new StringBuffer();
int               totalOrganizations           = 0;
StringBuffer      extraTags                    = new StringBuffer();
StringBuffer      newLink                      = new StringBuffer();
StringBuffer      newUploadLink                = new StringBuffer();

String            userDefaultWipView           = null;
String            selectedWorkflow             = null;
String            selectedStatus               = "";
String            loginLocale                  = null;
Vector            codesToExclude               = null;
String            gridID                       = null;
String            gridName                     = null;
String            viewName                     = null;
String searchListViewName = "InvoicesOfferedUserDefinedGroupDetailDataView.xml";

   userSecurityRights = userSession.getSecurityRights();
   userSecurityType   = userSession.getSecurityType();
   userOrgOid         = userSession.getOwnerOrgOid();
   userOid            = userSession.getUserOid();

  // Set up page navigation stack
  if ( "true".equals( request.getParameter("returning") ) ) {
    userSession.pageBack(); 
  }
  else {
    userSession.addPage("goToInvoiceOffersUserDefinedGroupDetail", request);
  }
  
   // getting the user security type to display authorization button or not.
   boolean isAdminUser;
   if (userSession.getSavedUserSession() == null) {
      isAdminUser = TradePortalConstants.ADMIN.equals(userSession.getSecurityType());
   } else {
      isAdminUser = TradePortalConstants.ADMIN.equals(userSession.getSavedUserSessionSecurityType());
   }

   CorporateOrganizationWebBean corpOrg = beanMgr.createBean(CorporateOrganizationWebBean.class, "CorporateOrganization");
   corpOrg.getById(userOrgOid);
   UserWebBean thisUser = beanMgr.createBean(UserWebBean.class, "User");
   thisUser.getById(userOid);

   formName = "InvoiceOfferedUserDefinedGroupDetailForm";
   helpSensitiveLink = OnlineHelp.createContextSensitiveLink("customer/invoices.htm", resMgr, userSession);
   loginLocale = userSession.getUserLocale();

   Hashtable secureParms = new Hashtable();
   secureParms.put("UserOid", userSession.getUserOid());
   secureParms.put("SecurityRights",userSecurityRights); 
   secureParms.put("ownerOrg",userOrgOid);
   String invoiceGroupOid = request.getParameter("InvoiceOfferGroupOid");
   if (invoiceGroupOid != null) {
      invoiceGroupOid = EncryptDecrypt.decryptStringUsingTripleDes(invoiceGroupOid, userSession.getSecretKey());
   }
   if(InstrumentServices.isNotBlank(invoiceGroupOid)){
	   session.setAttribute("InvoiceOfferGroupOid",invoiceGroupOid);
   } 
   else{
	   invoiceGroupOid =(String) session.getAttribute("InvoiceOfferGroupOid");
   }
   secureParms.put("InvoiceOfferGroupOid", invoiceGroupOid);
   
   InvoiceOfferGroupWebBean invoiceGroup = beanMgr.createBean(InvoiceOfferGroupWebBean.class, "InvoiceOfferGroup");
   invoiceGroup.getById(invoiceGroupOid);
   session.setAttribute("documentImageFileUploadPageOriginator", "InvoiceOffersGroupDetail");
   dynamicWhereClause.append(" and i.a_corp_org_oid = ").append(userOrgOid);
      dynamicWhereClause.append(" and i.a_invoice_group_oid = ").append(invoiceGroupOid);
  
   if (!TradePortalConstants.STATUS_ALL.equals(selectedStatus)) {
      dynamicWhereClause.append(" and (i.invoice_status = '");
      dynamicWhereClause.append(selectedStatus);
      dynamicWhereClause.append("'");
      dynamicWhereClause.append(")");
   }
%>
<%
  // Store values such as the userid, his security rights, and his org in a secure
  // hashtable for the form.  The mediator needs this information.

    // Get buyer info from the invoice.  This seller belongs to only one buyer so all the invoices are for the same buyer.
    //jgadela R92 - SQL INJECTION FIX
    String buyerSQL = "select min(buyer_party_identifier) buyer_party_identifier from invoice where a_invoice_offer_group_oid = ? ";
    DocumentHandler buyerDoc = DatabaseQueryBean.getXmlResultSet(buyerSQL.toString(), false,new Object[]{invoiceGroupOid});
    String buyerID = "";
    if (buyerDoc!=null) {
        buyerID = buyerDoc.getAttribute("/ResultSetRecord(0)/BUYER_PARTY_IDENTIFIER");
    }


  StringBuilder invoicesForHeader = new StringBuilder();
  String groupName = invoiceGroup.getAttribute("name");
  session.setAttribute("InvoiceGroupName",groupName);
  if (groupName == null || groupName.length() == 0) {
      invoicesForHeader.append(resMgr.getText("InvoicesOffered.InvoicesFor", TradePortalConstants.TEXT_BUNDLE));
      invoicesForHeader.append(" ").append(resMgr.getText("UploadInvoiceDetail.Currency", TradePortalConstants.TEXT_BUNDLE));
      invoicesForHeader.append(" ").append(invoiceGroup.getAttribute("currency"));
      invoicesForHeader.append(" ").append(resMgr.getText("UploadInvoiceDetail.Slash", TradePortalConstants.TEXT_BUNDLE));
      invoicesForHeader.append(" ").append(resMgr.getText("UploadInvoiceDetail.Date", TradePortalConstants.TEXT_BUNDLE));
      invoicesForHeader.append(" ").append(TPDateTimeUtility.formatDate(invoiceGroup.getAttribute("due_payment_date"), TPDateTimeUtility.SHORT, loginLocale));
    }
   else {
      invoicesForHeader.append(resMgr.getText("InvoicesOffered.GroupedInvoices", TradePortalConstants.TEXT_BUNDLE))
                       .append(": ").append(groupName);
    }
      invoicesForHeader.append(" &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").append(resMgr.getText("InvoicesOffered.Buyer", TradePortalConstants.TEXT_BUNDLE));
      invoicesForHeader.append(" ").append(buyerID);

  
%>

<%-- ********************* HTML for page begins here *********************  --%>
<% WidgetFactory widgetFactory = new WidgetFactory(resMgr); %>


<%-- Body tag included as part of common header --%>

<jsp:include page="/common/Header.jsp">
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeErrorSectionFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="additionalOnLoad" value="<%=onLoad.toString()%>" />
</jsp:include>

<div class="pageMainNoSidebar">
  <div class="pageContent">
    
    <div class="subHeaderDivider"></div>
    <div class="pageSubHeader">
      <span class="pageSubHeaderItem">
        <%= invoicesForHeader %>
      </span>
      <span class="pageSubHeader-right">
   		<button data-dojo-type="dijit.form.Button" name="CloseButton" id="CloseButton" type="button" class="pageSubHeaderButton">
          <%=resMgr.getText("common.CloseText",TradePortalConstants.TEXT_BUNDLE)%>
          <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
            document.location.href =
              '<%= formMgr.getLinkAsUrl("goToInvoiceOffersTransactionsHome", response) %>';
          </script>
        </button>
        <%=widgetFactory.createHoverHelp("CloseButton", "CloseHoverText") %>
        <span id="invoiceGroupListGridEdit" class="searchHeaderEdit"></span>
	    <%=widgetFactory.createHoverHelp("invoiceGroupListGridEdit","CustomizeListImageLinkHoverText") %>
      </span>
		
      <div style="clear:both;">
      </div>
    </div>

 

<form name="<%=formName%>" method="POST" action="<%= formMgr.getSubmitAction(response) %>" style="margin-bottom: 0px;">
    <jsp:include page="/common/ErrorSection.jsp" />
    <div class="formContentNoSidebar">
   <input type=hidden value="" name=buttonName>

  <%
   selectedStatus = invoiceGroup.getAttribute("invoice_status");
  String resourcePrefix ="";
  gridID = "InvoicesOfferedUserDefinedGroupDetailID";
  if(TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH.equals(corpOrg.getAttribute("dual_auth_supplier_portal")) && 
		  (TradePortalConstants.SP_STATUS_OFFER_ACCEPTED.equals(invoiceGroup.getAttribute("supplier_portal_invoice_status")) ||
		  TradePortalConstants.SP_STATUS_PARTIALLY_AUTHORIZED.equals(invoiceGroup.getAttribute("supplier_portal_invoice_status")))){
    gridName = "InvoicesOfferedUserDefinedGroupDetail2DataGrid";
  }else{
	  gridName = "InvoicesOfferedUserDefinedGroupDetailDataGrid";  
  }
 viewName = "InvoicesOfferedUserDefinedGroupDetailDataView";
 resourcePrefix = "InvoicesOffered";
 
  String linkArgs[] = {"","","","","",""};
          String arg = "key~"+viewName+".xml"+ "`key~" + dynamicWhereClause+ "`key~" + resourcePrefix;


  linkArgs[0] = TradePortalConstants.PDF_LIST;
          linkArgs[1] = EncryptDecrypt.encryptStringUsingTripleDes(arg);//Use common key for encryption of parameter to doc producer
          linkArgs[2] = InstrumentServices.isNotBlank(resMgr.getResourceLocale())? resMgr.getResourceLocale():"en_US";
          //loginLocale;
          linkArgs[3] = userSession.getBrandingDirectory();
          String linkString =  formMgr.getDocumentLinkAsURLInFrame(resMgr.getText("UploadInvoiceDetail.PrintInvoiceDetails",
                  TradePortalConstants.TEXT_BUNDLE),
                  TradePortalConstants.PDF_LIST,
                  linkArgs,
                  response) ;

 %>
  <div class="gridSearch">
  <div class="searchHeader">
   <span class="searchHeaderActions">
      <jsp:include page="/common/gridShowCount.jsp">
        <jsp:param name="gridId" value="<%=gridID%>" />
      </jsp:include>
    <%-- <button data-dojo-type="dijit.form.Button" name="print" id="print" type="button" class="pageSubHeaderButton">
          <%=resMgr.getText("common.PrintText",TradePortalConstants.TEXT_BUNDLE)%>
          <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
          window.open( "<%=linkString%>");
		  </script>
        </button>  
         <%=widgetFactory.createHoverHelp("print", "PrintHoverText") %> 
 	  <span id="invoiceGroupListRefresh" class="searchHeaderRefresh"></span>
 	  <%=widgetFactory.createHoverHelp("invoiceGroupListRefresh", "RefreshImageLinkHoverText") %>
      <span id="invoiceGroupListGridEdit" class="searchHeaderEdit"></span>
      <%=widgetFactory.createHoverHelp("invoiceGroupListGridEdit","CustomizeListImageLinkHoverText") %> --%>
    </span>
    <div style="clear:both;"></div>
  </div>


 <div id="InvoiceGroupListDialogID"></div>
  <%
  DataGridFactory dgFactory = new DataGridFactory(resMgr, userSession, formMgr, beanMgr, response);
  DGridFactory dgridFactory = new DGridFactory(resMgr, userSession, formMgr, beanMgr, response);
  String gridHtml = "";
  String gridLayout = "";
  
  if("InvoicesOfferedUserDefinedGroupDetail2DataGrid".equals(gridName)){
  		gridHtml = dgridFactory.createDataGridMultiFooter(gridID,gridName,"5");
  		gridLayout = dgridFactory.createGridLayout(gridID,gridName);
  }else if("InvoicesOfferedUserDefinedGroupDetailDataGrid".equals(gridName)){
		gridHtml = dgFactory.createDataGridMultiFooter(gridID,gridName,"5");
		gridLayout = dgFactory.createGridLayout(gridID,gridName);
}
 %>
<%=gridHtml%>

<%= formMgr.getFormInstanceAsInputField(formName, secureParms) %>
</div>
<div id="InvoiceGroupDetailDialogID"></div>
</form>
</div>
</div>
              
<jsp:include page="/common/Footer.jsp">
  <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
  <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>
</body>

<script type="text/javascript" src="/portal/js/datagrid.js"></script>

<%
// IR T36000018169- use gridID
 // String gridLayout = dgFactory.createGridLayout(gridID,gridName);
%>

<script type="text/javascript">
var gridID = '<%= gridID %>';
var gridName = '<%= gridName %>';
var viewName = '<%= EncryptDecrypt.encryptStringUsingTripleDes(viewName,userSession.getSecretKey())%>';  <%--Rel9.2 IR T36000032596 --%>
var formName = '<%=formName %>';
require(["t360/OnDemandGrid", "dojo/dom-construct"], function(onDemandGrid, domConstruct){
  <%--get grid layout from the data grid factory--%>
  var gridLayout = <%= gridLayout %>;
  
  <%--set the initial search parms--%>
<%
  //encrypt selectedOrgOid for the initial, just so it is the same on subsequent
  //search - note that the value in the dropdown is encrypted
  //String encryptedOrgOid = EncryptDecrypt.encryptStringUsingTripleDes(selectedOrg, userSession.getSecretKey());
%>

  var init='invoiceGroupOid=<%=EncryptDecrypt.encryptStringUsingTripleDes(invoiceGroupOid, userSession.getSecretKey())%>';
  <%if("InvoicesOfferedUserDefinedGroupDetail2DataGrid".equals(gridName)){ %>
  		var InvoiceListId = onDemandGrid.createOnDemandGrid(gridID, viewName, gridLayout, init,1);
  <%} else if("InvoicesOfferedUserDefinedGroupDetailDataGrid".equals(gridName)){ %> 
  		var InvoiceListId = createDataGrid(gridID, viewName, gridLayout, init);
  <%}%>		
});
 function performInvoiceListAction(pressedButton){
	    <%--get array of rowkeys from the grid--%>
 var rowKeys = getSelectedGridRowKeys(gridID);

	    <%--submit the form with rowkeys
	        becuase rowKeys is an array, the parameter name for
	        each will be checkbox + an iteration number -
	        i.e. checkbox0, checkbox1, checkbox2, etc --%>
	 submitFormWithParms(formName, pressedButton, "checkbox", rowKeys);
}

 function applyRemoveFutureValueDateToInvGroupDtl(){
	 
 }
 function applyAssignFutureValueDateToInvGroupDtl(){
		require(["t360/dialog"], function(dialog) {
	 	      dialog.open("InvoiceGroupDetailDialogID", '<%=resMgr.getText("InvoicesOffered.AssignFutureValueDate", TradePortalConstants.TEXT_BUNDLE)%>',
		                      'AssignFutureValueDate.jsp',
		                      ['selectedCount'], [getSelectedGridRowKeys(gridID)], <%-- parameters --%>
		                      null, null);
		  });
	}
 function searchInvoiceGroupList() {
	    require(["dojo/dom"],
	      function(dom){
	    	var searchParms = 'invoiceGroupOid=<%=EncryptDecrypt.encryptStringUsingTripleDes(invoiceGroupOid, userSession.getSecretKey())%>';

	        console.log("SearchParms: " + searchParms);
        searchDataGrid(gridID,viewName,
                searchParms);
	      });
	  }

 require(["dojo/query", "dojo/on", "t360/datagrid", "t360/popup", "dojo/domReady!"],
	      function(query, on, t360grid, t360popup){
	    query('#invoiceGroupListRefresh').on("click", function() {
	    	searchInvoiceGroupList();
	    });
   query('#invoiceGroupListGridEdit').on("click", function() {
     var columns = t360grid.getColumnsForCustomization(gridID);
     var parmNames = [ "gridId", "gridName", "columns" ];
     var parmVals = [ gridID, gridName, columns ];
     t360popup.open(
       'invoiceGroupListGridEdit', 'gridCustomizationPopup.jsp',
       parmNames, parmVals,
       null, null);  <%-- callbacks --%>
   });
  });

</script>
<% if("InvoicesOfferedUserDefinedGroupDetail2DataGrid".equals(gridName)){ %>
<jsp:include page="/common/dGridShowCountFooter.jsp">
  <jsp:param name="gridId" value="InvoicesOfferedUserDefinedGroupDetailID" />
</jsp:include>
<%} %>
<%
  // Finally, reset the cached document to eliminate carryover of
  // information to the next visit of this page.
  formMgr.storeInDocCache("default.doc", new DocumentHandler());
%>

</html>
