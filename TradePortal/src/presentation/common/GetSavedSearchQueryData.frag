<%--
*******************************************************************************
  Cash Management Payment Inquiries Footer

  Description: Everything that should be loaded at the end of the page
               should be included here.  This includes non-visible html 
               and javascript that should execute on page load.

*******************************************************************************
--%>
<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>

<%@page import="com.amsinc.ecsg.util.StringFunction"%>
<%@page import="java.util.Iterator"%>
<%
   String comma ="," ;
%>

<script type="text/javascript">
	  <%-- Rel 9.0 - Cross-site scripting (XSS) - StringFunction.xssCharsToHtml method has been used for encoding. --%>
	  var savedSearchQueryData = {
				<% 
				
				if(savedSearchQueryData !=null && !savedSearchQueryData.isEmpty()) {
					Iterator<Map.Entry<String, String>> entries = savedSearchQueryData.entrySet().iterator();
				while (entries.hasNext()) {
					Map.Entry<String, String> entry = entries.next(); 
					%>
					'<%=StringFunction.escapeQuotesforJS(entry.getKey())%>':'<%=StringFunction.escapeQuotesforJS(entry.getValue())%>'
					<%if(entries.hasNext()) {%>
					<%=comma%>
					
				<%
					}
				}
				}%>
	}

</script>
