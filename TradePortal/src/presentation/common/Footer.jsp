<%--
*******************************************************************************
                             Footer

  Description: Common footer

  Parameters:
     None.

*******************************************************************************
--%>



<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.ams.tradeportal.common.*, com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*, com.amsinc.ecsg.frame.*,
         com.amsinc.ecsg.util.*, com.ams.tradeportal.html.*"%>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager"
  scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager"
  scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager"
  scope="session">
</jsp:useBean>

<jsp:useBean id="userSession"
  class="com.ams.tradeportal.busobj.webbean.SessionWebBean"
  scope="session">
</jsp:useBean>

<%
  String displayFooter     = request.getParameter("displayFooter");
  //String includeNavBarFlag = request.getParameter("includeNavigationBarFlag");
  String minimalHeaderFlag = request.getParameter("minimalHeaderFlag");

  // Retrieve the user's security type (i.e., ADMIN or NON_ADMIN)
  String userSecurityType = userSession.getSecurityType();
  String userAuthMethod = userSession.getAuthMethod();
  
  //01/27/2013 Prateep Gedupudi :: Retrieve the user's locale (Ex: en_US, zh_CN, ja_JP). As dojo locale supports en-us,zh-cn we are replacing _ with - and converting to lowercase.
  String loginUserLocale = "";
  if (InstrumentServices.isNotBlank(userSession.getUserLocale()))
  	 loginUserLocale = StringFunction.xssCharsToHtml(userSession.getUserLocale().replace("_", "-").toLowerCase());


  String autoSaveFlag             = null;
  String templateFlag             = null;
  String autoSaveFormNumber       = null;
  String timeoutAutoSaveInd       = null;
  String brandingDirectory		  = "";
  boolean isTimeOutDialogCritical = false;

  Boolean showToolTips = userSession.getShowToolTips() == null ? Boolean.TRUE : userSession.getShowToolTips();
  //KMehta on 9 May 2014 @ Rel 90 for IR T36000025449 Start
  autoSaveFlag            = StringFunction.xssCharsToHtml(request.getParameter("autoSaveFlag"));  // Whether to perform auto-save when time-out  
  templateFlag            = StringFunction.xssCharsToHtml(request.getParameter("templateFlag"));  // Whether to auto-save a template when time-out
  //Rel 9.2 XSS CID 11398
  if(null != request.getParameter("autoSaveFormNumber")){
	  autoSaveFormNumber      = StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(request.getParameter("autoSaveFormNumber")));  // The index number of the form being auto-saved, defaults to 0
  }
  if (autoSaveFormNumber==null) autoSaveFormNumber = "0"; 
  //KMehta on 9 May 2014 @ Rel 90 for IR T36000025449 End 
  
  // Get the version number from the TradePortalVersion.properties file.  If we
  // cannot determine the version, assign the unknown version value to it.
  String version = resMgr.getText("version", "TradePortalVersion");
  if (version.equals("version")) {
    version = resMgr.getText("Footer.UnknownVersion", 
                             TradePortalConstants.TEXT_BUNDLE);
  }
  // W Zhu 10/23/2012 Rel 8.1 T36000006807 #335 hide version number
  version = EncryptDecrypt.bytesToBase64String(version.getBytes());// RKAZI Rel 9.1 08/29/2014 Dojo Minificatio - Start - we need to use version from TradePortalVersion.properties to make caching work properly.
//RPasupulati geting first 4 char's from encripted version String IR no T36000017311 Starts.
	//version = EncryptDecrypt.encryptStringUsingTripleDes(version, userSession.getSecretKey());// using userSesion.getSecretKey() will create unique version key 
	 																							// for every user login, effectively making caching not be used at all. So commneted this out. 
  	version = version.substring(0, 4);
  //RPasupulati geting first 4 char's from encripted version String IR no T36000017311 ENDs.
  PerformanceStatistic perfStat = (PerformanceStatistic) session.getAttribute("performanceStatistic");

  if(perfStat != null) {
    perfStat.setToPage(formMgr.getNavigateToPage());
    Debug.debug("Footer.js : NAV ID ---> "+ request.getParameter("ni"));
    perfStat.setNavId(request.getParameter("ni"));
    perfStat.setaObjectOid((StringFunction.isBlank(request.getParameter("transOid"))?"":request.getParameter("transOid")));
    perfStat.setFromPage((StringFunction.isBlank(request.getParameter("prevPage"))?"":request.getParameter("prevPage")));
    perfStat.setServerName(userSession.getServerName());
    perfStat.setUserOid(userSession.getUserOid());
    perfStat.setTimeType("S");
    perfStat.setAction(formMgr.getNavigateToPage());
  }
  
          
          
%>

<div style="clear:both;"></div>
<% //although we always want the footer for final page stuff, sometimes we don't want to display it
  if ( TradePortalConstants.INDICATOR_YES.equals( displayFooter ) ) { //do not display by default
%>
<div class="pageFooter">
  <div class="pageContent">
<%
    //TODO: only display if user is logged in - for now only display if ! minimalHeader - which currently is synonymous with logged in
    if ( !TradePortalConstants.INDICATOR_YES.equals( minimalHeaderFlag ) ) {
%>
    <div class="footerContent">
      <div class="footerDivider"></div>
      <div>
        <%--this is a placeholder until we have ability to display last login info
        <span class="footerLoginInfo">last login info</span>
        --%>
      </div>
      <div style="clear:both;"></div>
      <div>
        <span class="footerCopyright">
          <%= resMgr.getText("Footer.Copyright",
                             TradePortalConstants.TEXT_BUNDLE) %>
        </span>
        <%-- W Zhu 10/23/2012 Rel 8.1 T36000006807 #335 hide version number
        <span class="footerVersion">
          <%= resMgr.getText("Footer.Version", 
                             TradePortalConstants.TEXT_BUNDLE) %>
          <%= version %>
        </span>
        --%>
        <%-- Tang Rel9.3.5 CR#1032 Session Synch -[Begin] --%>
        <%
         String sessionSynchImg = userSession.getSessionSynchImg();
    	
         if (StringFunction.isNotBlank(sessionSynchImg)) {
         		 
        	Debug.debug("Footer.jsp :  sessionSynchImg - [" + 
        	sessionSynchImg + "]");
         
			%>
		    <span class="footerSessionSynch">
				<img src="<%= sessionSynchImg %>" style="display:none">
 			</span>
			<%
 		 }
 		 %>
        <%-- Tang Rel9.3.5 CR#1032 Session Synch -[End] --%>
      </div>
      <div style="clear:both;"></div>
    </div>
<%
    } else {
%>
    <div class="footerContentMinimal"></div>
<%
    }
%>
  </div>    
</div>    
<%
  }
%>
<%-- 01/28/2013 Prateep Gedupudi Defining global variable because we need to assign text resources property to js file variable --%>
<script type="text/javascript">


var nodatafoundglobal='<%= resMgr.getText("dataGrid.noDataFound",TradePortalConstants.TEXT_BUNDLE) %>';
<%-- vdesingu CR590 8.4 - making loading message global for dgrid. --%>
var globalLoadingMessage='<%= resMgr.getText("common.Loading",TradePortalConstants.TEXT_BUNDLE) %>';
<%-- this is global - we don't have any global widgets yet for this --%>
var globalUIPref = {
        showToolTips: <%=showToolTips.booleanValue() %>,
        isShowing: <%=showToolTips.booleanValue() %>
};

</script>

<%--configure dojo. must be done before dojo loaded. in separate file to avoid loading every time--%>
<%--note our simple cache busting mechanism of including version info to the url--%>
<%-- RKAZI RELEASE 9.1 - DOJO - MINIFICATION - 08/29/2014 - Start - Added new js files as src and moved dojoparse.js to end --%>
<script src="<%=userSession.getdojoJsPath()%>/dojoconfig.js?<%= version %>"></script>
<%-- 01/27/2011 Prateep Gedupudi Load dojo with login locale from user session. Added locale:loginUserLocale --%> 
<script src='<%=userSession.getdojoJsPath()%>/dojo/dojo.js?<%= version %>' data-dojo-config="async: 1, locale: '<%=loginUserLocale %>', cacheBust: '<%=version%>'" ></script>
<script src="<%=userSession.getdojoJsPath()%>/dojo/dojox.js?<%= version %>"></script>
<script src="<%=userSession.getdojoJsPath()%>/t360/t360.js?<%= version %>"></script>
<script src="<%=userSession.getdojoJsPath()%>/t360/t360-widgets.js?<%= version %>"></script>

<%-- include common javascript libraries --%>
<%--!!!!!!include these temporarily. they are pass throughs to actual functions--%>
<script src="<%=userSession.getdojoJsPath()%>/common.js?<%= version %>"></script>
<script src="<%=userSession.getdojoJsPath()%>/dialog.js?<%= version %>"></script>
<script src="<%=userSession.getdojoJsPath()%>/datagrid.js?<%= version %>"></script>
<%-- define datagrid formatters.
     this is temporary until all grids are loaded with t360/datagrid rather than js/datagrid--%>
<script src="<%=userSession.getdojoJsPath()%>/datagridformatters.js?<%= version %>"></script>
<script src="<%=userSession.getdojoJsPath()%>/t360/partySearch.js?<%= version %>"></script>
<%-- parse the page first thing. in separate file to avoid loading every time--%>
<script src="<%=userSession.getdojoJsPath()%>/dojoparse.js?<%= version %>"></script>
<%-- RKAZI RELEASE 9.1 - DOJO - MINIFICATION - 08/29/2014 - End --%>
<%
  //for menu new transactions, we have a form that is submitted.
  // this is put in the footer so it is entirely after the other forms on the page
  //if ( TradePortalConstants.INDICATOR_YES.equals( includeNavBarFlag ) ) {
  if ( !TradePortalConstants.INDICATOR_YES.equals( minimalHeaderFlag ) &&
       !userSecurityType.equals(TradePortalConstants.ADMIN) ) {
%>

<%-- load menu dialogs --%>
<jsp:include page="/common/MenuFooter.jsp" />
<%
  }else{
%>
<jsp:include page="/common/AdminMenuFooter.jsp" />
<%
  }
%>

<%--Include common executable javascript here--%>

<%
  if ( (request.getParameter("includeTimeoutForward") == null) ||
       (!request.getParameter("includeTimeoutForward").equals("false") ) ) {
%>
<div id="timeoutWarningDialog"></div>

<%-- RKAZI CMA SESSION SYNC REL 9.1 10/23/2014 - Start --%>
<script>

var timeoutParams = {};
var logOutTimerId;
var timeoutWarningTimerId;
var autoSaveTimeoutTimerId;
var autoSaveButtonName;
var autoSaveFormNumber;
var timeOutLink;
  require(["t360/common", "t360/dialog"],
      function(common, dialog) {
	  <%
	    
	  
  	
	    // Forward the user to the timeout page after their session has timed out
	    // Subtract ten seconds from the timeout period.
	    // This way, the browser will forward to the timeout page before the session
	    // is actually killed by the server
	    int timeoutInSeconds = SecurityRules.getTimeOutPeriodInSeconds() - 15;
	    timeoutAutoSaveInd = userSession.getTimeoutAutoSaveInd();
		if ((!(TradePortalConstants.INDICATOR_YES.equals(SecurityRules.getAutoExtendSession()))) || (TradePortalConstants.INDICATOR_YES.equals(SecurityRules.getAutoExtendSession()) && !(TradePortalConstants.AUTH_SSO.equals(userAuthMethod) || TradePortalConstants.AUTH_AES256.equals(userAuthMethod)))){
		    if( (autoSaveFlag != null) && autoSaveFlag.equals(TradePortalConstants.INDICATOR_YES) &&
		        (timeoutAutoSaveInd != null) && timeoutAutoSaveInd.equals(TradePortalConstants.INDICATOR_YES)) {
		      // setTimeout causes an action to occur after a specified number of milliseconds
		      if (templateFlag!= null && templateFlag.equals(TradePortalConstants.INDICATOR_YES)) {
	%>
	    setTimeout(function () { common.autoSaveForTimeOut('<%=TradePortalConstants.BUTTON_TEMPLATETIMEOUT%>', <%=autoSaveFormNumber%>); },
	      <%=timeoutInSeconds%>000);
	<%    }
	      else {
	%>
	<%--KMehta on 9 May 2014 @ Rel 90 for IR T36000025449 Start--%>

	    setTimeout(function () { common.autoSaveForTimeOut('<%=TradePortalConstants.BUTTON_TIMEOUT%>', <%=autoSaveFormNumber%>); }, 
	      <%=timeoutInSeconds%>000);
	<%  }           
	    } else {                  
	%>
	    <%-- Don't auto save... Just forward to the timeout page --%>
	    setTimeout(function () { document.location.href='<%=formMgr.getLinkAsUrl("timeout", response)%>'; }, 
	      <%=timeoutInSeconds%>000);
	<%               
	     }
	    String timeoutWarningDialogTitle = resMgr.getText("TimeoutWarning.WindowTitle", TradePortalConstants.TEXT_BUNDLE);
	%>
	<%--KMehta on 9 May 2014 @ Rel 90 for IR T36000025449 End --%>
	    <%-- Provide a warning that the timeout will happen soon --%>
	    setTimeout(function () { dialog.open('timeoutWarningDialog','<%= timeoutWarningDialogTitle %>',
	                             'timeoutWarningDialog.jsp',null, null, <%-- no parms --%>
	                             null, null, 'warningDialog' ) },
	      <%= timeoutInSeconds - (SecurityRules.getTimeoutWarningPeriod() * 60) %>000);
	  });

<%
    
  } else {
    	
    timeoutAutoSaveInd = userSession.getTimeoutAutoSaveInd();
    String newTimeoutValue = "0";
	if (request.getAttribute("newTimeoutVal")  != null){
		newTimeoutValue = (String)request.getAttribute("newTimeoutVal") ;
		if (Integer.valueOf(newTimeoutValue).intValue() < timeoutInSeconds){
			timeoutInSeconds = Integer.valueOf(newTimeoutValue).intValue() - 15;
			
		}
	}
    if( (autoSaveFlag != null) && autoSaveFlag.equals(TradePortalConstants.INDICATOR_YES) &&
        (timeoutAutoSaveInd != null) && timeoutAutoSaveInd.equals(TradePortalConstants.INDICATOR_YES)) {
      // setTimeout causes an action to occur after a specified number of milliseconds
      if (templateFlag!= null && templateFlag.equals(TradePortalConstants.INDICATOR_YES)) {
%>
		timeoutParams["autoSaveButtonName"] = '<%=TradePortalConstants.BUTTON_SAVETRANS%>';
		timeoutParams["autoSaveFormNumber"] = <%=autoSaveFormNumber%> ;
		<% if (timeoutInSeconds - 15 <= 0) {%>
			document.forms[timeoutParams["autoSaveFormNumber"]].autoSaved.value = 'Y';
			document.forms[timeoutParams["autoSaveFormNumber"]].templateAutoSaved.value = 'Y';
			
			common.autoSaveForTimeOut(timeoutParams["autoSaveButtonName"], timeoutParams["autoSaveFormNumber"]);
		<%} else{%>
			timeoutParams["autoSaveTimeoutTimerId"] = setTimeout(function () { 
			document.forms[timeoutParams["autoSaveFormNumber"]].autoSaved.value = 'Y';
			document.forms[timeoutParams["autoSaveFormNumber"]].templateAutoSaved.value = 'Y';
			
			common.autoSaveForTimeOut(timeoutParams["autoSaveButtonName"], timeoutParams["autoSaveFormNumber"]); },
			<%=(timeoutInSeconds)%>000);
		<%}%>
<%    }
      else {
%>
<%--KMehta on 9 May 2014 @ Rel 90 for IR T36000025449 Start--%>
		timeoutParams["autoSaveButtonName"] = '<%=TradePortalConstants.BUTTON_SAVETRANS%>';
		timeoutParams["autoSaveFormNumber"] = <%=autoSaveFormNumber%> ;
		<% if (timeoutInSeconds - 15 <= 0) {%>
			document.forms[timeoutParams["autoSaveFormNumber"]].autoSaved.value = 'Y';
			document.forms[timeoutParams["autoSaveFormNumber"]].templateAutoSaved.value = 'N';
			common.autoSaveForTimeOut(timeoutParams["autoSaveButtonName"], timeoutParams["autoSaveFormNumber"]);
		<%} else {%>
			timeoutParams["autoSaveTimeoutTimerId"] = setTimeout(function () { 
			document.forms[timeoutParams["autoSaveFormNumber"]].autoSaved.value = 'Y';
			document.forms[timeoutParams["autoSaveFormNumber"]].templateAutoSaved.value = 'N';
			common.autoSaveForTimeOut(timeoutParams["autoSaveButtonName"], timeoutParams["autoSaveFormNumber"]); }, 
			<%=(timeoutInSeconds)%>000);
		<%}%>
<%  }           
    } else {                  
%>
    <%-- Don't auto save... Just forward to the timeout page --%>
    timeoutParams["timeOutLink"] = '<%=formMgr.getLinkAsUrl("timeout", response)%>';
    <%--timeoutParams["logOutTimerId"] = setTimeout(function () { document.location.href=timeoutParams["timeOutLink"]; }, 
      <%=timeoutInSeconds%>000);--%>
<%               
     }
    String timeoutWarningDialogTitle = resMgr.getText("TimeoutWarning.WindowTitle", TradePortalConstants.TEXT_BUNDLE);
%>
<%--KMehta on 9 May 2014 @ Rel 90 for IR T36000025449 End --%>
    <%-- Provide a warning that the timeout will happen soon --%>
    <%--timeoutParams["timeoutWarningTimerId"] = setTimeout(function() {checkForSessionTimeout(timeoutParams);},
      <%= timeoutInSeconds - (SecurityRules.getTimeoutWarningPeriod() * 60) %>000);--%>
  });
  
  function checkForSessionTimeout(timeoutParams){
	  require(["t360/checkSessionTimeOut", "t360/dialog"],
	          function(checkSessionTimeOut, dialog) {
		  		timeoutParams["warningTimeOutInSeconds"] = <%=(timeoutInSeconds - (SecurityRules.getTimeoutWarningPeriod() * 60))%>;
		  		timeoutParams["timeoutWarningDialogTitle"] = '<%= timeoutWarningDialogTitle %>';
		  		timeoutParams["timeoutInSecond"] = <%=timeoutInSeconds%>;
		  		checkSessionTimeOut.checkIfSessionShouldTimeout(timeoutParams);
         });
  }
 <% } %> 
 <%-- RKAZI CMA SESSION SYNC REL 9.1 10/23/2014 - End --%>
  window.onload = function (){
	  document.body.scrollTop = document.documentElement.scrollTop = 0;

	  <% if (TradePortalConstants.INDICATOR_YES.equals(userSession.getIsSLAMonitoringOn())){ 
			//XSS Rel 9.2 Coverity CID 11536, 11537, 11231, 11226, 11213 start
			String xssTransOid = null; 
			String xssLogicalPageAndInstrumentId = null;
			String xssCTime = null;
			String xssButtonName = null;
			String xssPrevPage = null;
			String xssNavId = null;
			
			xssTransOid = request.getParameter("transOid");
			/*if(null != xssTransOid){
			 xssTransOid = StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(xssTransOid));
			}*/
			xssLogicalPageAndInstrumentId = request.getParameter("logicalPageAndInstrumentId");
		/*	if(null != xssLogicalPageAndInstrumentId){
				xssLogicalPageAndInstrumentId = StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(xssLogicalPageAndInstrumentId));
			} */
			xssCTime = request.getParameter("cTime");
			/*if(null != xssCTime){
				xssCTime = StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(xssCTime));
			}*/
			xssButtonName = request.getParameter("buttonName");
			/*if(null != xssButtonName){
				xssButtonName = StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(xssButtonName));
			}*/
			xssPrevPage = request.getParameter("prevPage");
			/*if(null != xssPrevPage){
				xssPrevPage = StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(xssPrevPage));
			}*/
			xssNavId = request.getParameter("ni");
		//	if(null != xssNavId){
			//	xssNavId = StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(xssNavId));
		//	}
			//XSS Rel 9.2 end
	  %>
	  if (isActive =='Y'){
          var contextName = '<%=formMgr.getNavigateToPage()%>';
        <%-- XSS - Rel 9.2 Coverity CID 11536, 11537, 11231, 11226, 11213 start --%>
        <%--MEer Rel 9.3 XSS CID-11601, 11602, 11603, 11604 --%>
          var navId = '<%=StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(xssNavId))%>';          
          var logicalPageAndInstrumentId = '<%=(StringFunction.isBlank(xssLogicalPageAndInstrumentId)?TradePortalConstants.BLANK:StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(xssLogicalPageAndInstrumentId)))%>';
          var transOid = '<%=(StringFunction.isBlank(xssTransOid)?TradePortalConstants.BLANK:StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(xssTransOid)))%>';         
          var cStartTime = '<%=(StringFunction.isBlank(xssCTime)?TradePortalConstants.BLANK:StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(xssCTime)))%>';        
          var buttonName ='<%=(StringFunction.isBlank(xssButtonName)?TradePortalConstants.BLANK:StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(xssButtonName)))%>';
          var prevPage = '<%=(StringFunction.isBlank(xssPrevPage)?TradePortalConstants.BLANK:StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(xssPrevPage)))%>';
        <%-- XSS Rel 9.2 end --%>
          if (cStartTime == null || cStartTime == '' ){
        	  <%-- Activity 41941 RKAZI Rel 9.3 XSS Issue Fix Related to 37561  --%>
        	  cStartTime = <%=(StringFunction.isBlank(xssCTime)?null:StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(xssCTime)))%>;
          }
          if (prevPage == null){
          	  <%-- Activity 41941 RKAZI Rel 9.3 XSS Issue Fix Related to 37561  --%>
        	  prevPage = '<%=(StringFunction.isBlank(xssPrevPage)?TradePortalConstants.BLANK:StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(xssPrevPage)))%>';
          }
          var times = {};
          if (contextName in timer){
              times = timer[contextName];
          }
          times["startTime"] = timeStarted;
          times["creationTimeStamp"] = creationTimeStamp;
          times["navId"] = navId;
          times["gridsToLoad"]=gridsToLoad;
          times["gridsLoaded"]=gridsLoaded;
          if (buttonName !=''  ){
        	  times["action_type"] = buttonName;
          }else{
          	times["action_type"] = "LINK";
          }
          if (logicalPageAndInstrumentId !=''  ){
        	  times["logical_page_instrument_id"] = logicalPageAndInstrumentId;
          }
          if (transOid !=''  ){
        	  times["transOid"] = transOid;
          }
          if (buttonName =='Login'){
        	  times["action"]  ='SystemLogin';
      	  }else {
          	times["action"] = contextName;
      	  }
          times["time_type"] = "UI";
          if (cStartTime != null){
        	times["cTime"] = cStartTime;  
          }
          times["prevPage"] = prevPage;
          timer[contextName] = times;
          endTime(contextName,null);
      }
	<%}%>
  };
  <% if (TradePortalConstants.INDICATOR_YES.equals(userSession.getIsSLAMonitoringOn())){ %>
	document.body.onclick = function (evt){
        
        	evt = evt || window.event;
    
		if (isActive =='Y') {
            var cTime = (new Date()).getTime();
            var evtElement = null;
            if (evt.srcElement){
            	evtElement = evt.srcElement;
            }else if (evt.target){
            	evtElement = evt.target;
            }else if (evt.targetElement){
            	evtElement = evt.targetElement;
            }else{
            	return;
            }
            var tagName = evtElement.tagName;
            var contextName = '<%=formMgr.getNavigateToPage()%>';
            if ("A" == tagName.toUpperCase()) {
                var aFrm = evtElement.form;
                if (aFrm) {
                    var field = document.createElement('input');
                    field.type = 'text';
                    field.value = (new Date(cTime)).getTime();
                    frm.appendChild(field);
                } else {
                    var aHref = evtElement.href;
                    if (aHref != '' && (aHref.indexOf("javascript:") == -1 && aHref.indexOf("#section")==-1)) {
                        aHref = aHref + "&cTime=" + cTime +"&prevPage=" + contextName;
                        evtElement.href = aHref;
                    }
                }
            }
<%--         else if ("INPUT" == tagName.toUpperCase()){ --%>
<%-- 			if ("BUTTON" == evt.srcElement.type.toUpperCase()){ --%>
<%-- 				var frm = evt.target.form; --%>
<%-- 				if (frm) --%>
<%-- 				{ --%>
<%-- 					var field = document.getElementById("cTime"); --%>
<%-- 					if (field){ --%>
<%-- 						document.getElementById("cTime").value= (new Date(cTime)).toUTCString(); --%>
<%-- 						document.getElementById("cTime").text= (new Date(cTime)).toUTCString(); --%>
<%-- 					} --%>
<%-- 				} --%>
<%-- 			} --%>
<%-- 		} --%>
            <%-- var cTime = evt.timestamp; --%>

        }
			};
	<%}%>
	<%-- RKAZI RELEASE 9.1 - DOJO - MINIFICATION - 08/29/2014 - End --%>
</script>

<%   
  }
%>
