<%--
*******************************************************************************
  NavigationLink

  Description:

  Render a sub navigation link.
  If selected, the link is rendered as text.
  If not selected, the link will include the passed in action and 
  any additional link parameters passed in.

  Parameters:
     linkTextKey  - the resource bundle key for link text to display
     linkSelected - Y or N to indicate how the link should be rendered
     action      - the navigation manager action to use for the link
     linkParms   - optional parameters that are included in the link.  If
                   given, do no include the '&'.  Thus 'tab=2' is a valid parm.
*******************************************************************************
--%>

<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.ams.tradeportal.common.*, com.ams.tradeportal.html.*, com.amsinc.ecsg.util.StringFunction" %>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session">
</jsp:useBean>

<%
  String linkTextKey  = StringFunction.xssCharsToHtml(request.getParameter("linkTextKey"));
  String linkSelected = request.getParameter("linkSelected");
  String action       = request.getParameter("action");
  String parms        = request.getParameter("linkParms");
  String hoverText    = StringFunction.xssCharsToHtml(request.getParameter("hoverText"));
  
  WidgetFactory widgetFactory = new WidgetFactory(resMgr);
  //Validation to check Cross Site Scripting
  if(linkTextKey != null){
	  linkTextKey = StringFunction.xssCharsToHtml(linkTextKey);
  }
  if(hoverText != null){
	  hoverText = StringFunction.xssCharsToHtml(hoverText);
  }
  
  if (TradePortalConstants.INDICATOR_YES.equals(linkSelected) ) {
%>
  <div class="secondaryNavLink navLinkSelected">
     <%=resMgr.getText(linkTextKey, TradePortalConstants.TEXT_BUNDLE)%>
  </div>
<%
  } else { //not selected

    if (parms != null) {
       parms = "&" + parms;
    }

  if (hoverText != null) { 
 %>
   <div id="<%=hoverText%>" class="secondaryNavLink">
 <% } else { %> 
   <div  class="secondaryNavLink">
 <%}%>
 
    <a href="<%=formMgr.getLinkAsUrl(action, parms, response)%>">
      <%=resMgr.getText(linkTextKey, TradePortalConstants.TEXT_BUNDLE)%>
    </a>
  </div>
<%  if (hoverText != null) { %>
<%=widgetFactory.createHoverHelp(hoverText,"HoverHelp") %> 
 
<% }
  } 
%>
