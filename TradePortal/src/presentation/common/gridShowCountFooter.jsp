<%--
*******************************************************************************
  gridShowCountFooter

  Description: include this in the footer section for any page that includes
    gridShowCount.jsp

  Parameters:
     gridId - the dom id of the grid

*******************************************************************************
--%>

<%@ page import="com.ams.tradeportal.common.*, com.amsinc.ecsg.util.StringFunction"%>

<%
  String gridId = request.getParameter("gridId");

//Validation to check Cross Site Scripting
if(gridId != null){
	  gridId = StringFunction.xssCharsToHtml(gridId);
}
  //if includeRequire is set to 'N' we skip the script and require statement
  // with assumption calling has already done
  String includeRequire = request.getParameter("includeRequire");

  if (!TradePortalConstants.INDICATOR_NO.equals(includeRequire)) {
%>

<script>

  require(["dojo/query", "dojo/on", 
           "dijit/registry", "dojo/dom-class",
           "dojo/dom", "dojo/domReady!"],
      function(query, on, 
	       registry, domClass,
               dom ) {
<%
  }
%>

    <%-- use event delegation --%>
    query('#<%=StringFunction.escapeQuotesforJS(gridId)%>_showCounts').on(".gridShowCountItem:click", function(evt) {
      var clickedNodeId = evt.target.id;
      var myGrid = registry.byId("<%=StringFunction.escapeQuotesforJS(gridId)%>");
      if ( clickedNodeId.indexOf("showCount10")>=0 ) {
        if (!domClass.contains("<%=StringFunction.escapeQuotesforJS(gridId)%>_showCount10",'selected')) {
          <%-- setting rowsPerPage on LazyTreeGrid causes errors --%>
          if (myGrid instanceof dojox.grid.LazyTreeGrid) {
            <%-- do nothing --%>
          } else {
            myGrid.set('rowsPerPage',15);
          }
          myGrid.set('autoHeight',10);
          domClass.remove("<%=StringFunction.escapeQuotesforJS(gridId)%>_showCount20",'selected');
          domClass.remove("<%=StringFunction.escapeQuotesforJS(gridId)%>_showCount30",'selected');
          domClass.add("<%=StringFunction.escapeQuotesforJS(gridId)%>_showCount10",'selected');
        }
      }
      else if ( clickedNodeId.indexOf("showCount20")>=0 ) {
        if (!domClass.contains("<%=StringFunction.escapeQuotesforJS(gridId)%>_showCount20",'selected')) {
          <%-- setting rowsPerPage on LazyTreeGrid causes errors --%>
          if (myGrid instanceof dojox.grid.LazyTreeGrid) {
            <%-- do nothing --%>
          } else {
            myGrid.set('rowsPerPage',25);
          }
          myGrid.set('autoHeight',20);
          domClass.remove("<%=StringFunction.escapeQuotesforJS(gridId)%>_showCount10",'selected');
          domClass.remove("<%=StringFunction.escapeQuotesforJS(gridId)%>_showCount30",'selected');
          domClass.add("<%=StringFunction.escapeQuotesforJS(gridId)%>_showCount20",'selected');
        }
      }
      else if ( clickedNodeId.indexOf("showCount30")>=0 ) {
        if (!domClass.contains("<%=StringFunction.escapeQuotesforJS(gridId)%>_showCount30",'selected')) {
          <%-- setting rowsPerPage on LazyTreeGrid causes errors --%>
          if (myGrid instanceof dojox.grid.LazyTreeGrid) {
            <%-- do nothing --%>
          } else {
            myGrid.set('rowsPerPage',35);
          }
          myGrid.set('autoHeight',30);
          domClass.remove("<%=StringFunction.escapeQuotesforJS(gridId)%>_showCount10",'selected');
          domClass.remove("<%=StringFunction.escapeQuotesforJS(gridId)%>_showCount20",'selected');
          domClass.add("<%=StringFunction.escapeQuotesforJS(gridId)%>_showCount30",'selected');
        }
      }
    });
<%
  if (!TradePortalConstants.INDICATOR_NO.equals(includeRequire)) {
%>
  });

</script>
<%
  }
%>
