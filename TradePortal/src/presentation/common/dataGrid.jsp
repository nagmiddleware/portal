<%--
  HTML layout for a data grid.
--%>

<%@ page import="com.ams.tradeportal.common.*, com.ams.tradeportal.html.DataGridFactory, com.amsinc.ecsg.util.StringFunction"%>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session"></jsp:useBean>

<p>
NOTE::::
!!!dataGrid.jsp is DEPRECATED!!!!
!!!use DataGridFactory.createDataGrid() instead so can pull in configured footer items!!!!
</p>

<%
    String dataGridId = request.getParameter("dataGridId");
    String dataGrid = request.getParameter("dataGrid");
    
    //Validation to check Cross Site Scripting
    if(dataGridId != null){
    	dataGridId = StringFunction.xssCharsToHtml(dataGridId);
    }
    if(dataGrid != null){
    	dataGrid = StringFunction.xssCharsToHtml(dataGrid);
    }
%>

<%--add a header - not sure how this will work commonly yet--%>
<%--todo: use parameter to determine if to include footer or not--%>
<%--todo: add optional refresh, edit functionality--%>
<div class="gridHeader"></div>
<div style="clear:both;"></div>

<%--the data grid itself--%>
<div id="<%= StringFunction.xssCharsToHtml(dataGridId)%>" class="dataGrid"></div>

<%--add a footer - not sure how this will work commonly yet--%>
<%--todo: use parameter to determine if to include footer or not--%>
<%--todo: only include selection counts if data grid is selectable--%>
<div class="gridFooter">
  <div class="gridSelectedCount">
    <span id="<%=StringFunction.xssCharsToHtml(dataGridId)%>_selCount">0</span> <%--this is updated by a data grid callback. id is important--%>
    <span>&nbsp;<%=resMgr.getText("dataGrid.selected", TradePortalConstants.TEXT_BUNDLE)%></span>
  </div>
  <div class="gridTotalCount">
    <span><%=resMgr.getText("dataGrid.totalCount", TradePortalConstants.TEXT_BUNDLE)%>&nbsp;</span>
    <span id="<%=StringFunction.xssCharsToHtml(dataGridId)%>_totalCount">0</span> <%--this is updated by a data grid callback. id is important--%>
  </div>
</div>
<div style="clear:both;"></div>
