<%--
*******************************************************************************
				Part Navigation Links.jsp

  Description:  
    This includable page supports the display of "tabbed" navigation links.
  It originally supported the Transaction pages by allowing the very long
  pages to be split into multiple parts, each with a "tabbed" view at the
  top.  This allows the user to select one part or another use the "tab"
  paradigm.  It has been generalized to support other big pages as well.

  Each "tab" link is actually a submit action that sets the hidden button
  pressed field with the name of the tab (or part) selected.

  This jsp needs the following parameters defined:

  selected 	  - indicates if this link represents the tab the user is 
                    currently viewing
  textResourceKey - the text resource key for the link's label
  readOnly  	  - indicates if the page is in read only mode or not	
  partName  	  - "Part1", "Part2", "Part3", etc.  This becomes the name of 
                    the button that is submitted along with the form
     
  urlDestination  - NavigationMgr logical name for the url destination page.
                    e.g., 'selectPOUploadDefinition' or 'goToInstrumentNavigator'.
                    The default value is 'goToInstrumentNavigator'
  htmlClass 	  - 'BankColor' or 'ColorGrey'.  The default is 'ColorGrey'.
  linkDisplayType - The class to use for the displayed link value.  Default is
                    'ControlLabel'
  extraTags       - Any extra tags that should be added to the tab's link that
                    are sent when the tab is clicked.  Optional.  If included, 
                    must contain %26 before the start of each parameter 
                    (e.g., %26isTransTermsFlag=false).
  onTop           - "true" or "false" value indicating whether tabs display
                    rightside up (true) or upside down (false).  The default is
                    true.

*******************************************************************************
--%>

<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.html.*,java.math.*" %>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session" />

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session">
</jsp:useBean>


<%
     String brandingDirectory = StringFunction.xssCharsToHtml(userSession.getBrandingDirectory());
     String partName  	      = StringFunction.xssCharsToHtml(request.getParameter("partName"));
     String urlDestination    = request.getParameter("urlDestination");
     String extraTags         = request.getParameter("extraTags");
     String readOnlyTags      = request.getParameter("readOnlyTags");
     String onTop             = request.getParameter("onTop");
     String onClickTag        = "";
     String linkParameters    = "";
     String linkLabel         = "";
     String link              = "";
     String onLeft            = "";
     String onRight           = "";
     String offLeft           = "";
     String offRight          = "";
     boolean multiTabLinks    = false;
     boolean selected;
     boolean isReadOnly;

     linkLabel = request.getParameter("linkLabel");
     //Validation to check Cross Site Scripting
     if(linkLabel != null){
    	 linkLabel = StringFunction.xssCharsToHtml(linkLabel);
     }
     if(urlDestination != null){
    	 urlDestination = StringFunction.xssCharsToHtml(urlDestination);
     }
     if (InstrumentServices.isBlank(linkLabel))
     {
       linkLabel = resMgr.getText(request.getParameter("textResourceKey"), 
                                  TradePortalConstants.TEXT_BUNDLE);
     }
     String linkLabelDisplay = StringFunction.xssCharsToHtml("Shipment"+linkLabel);
     

     if (!InstrumentServices.isBlank(request.getParameter("multiTabLinks")) &&
         request.getParameter("multiTabLinks").equals("true")) 
         multiTabLinks = true;

     // Set up default values for missing parameters     
     if (InstrumentServices.isBlank( urlDestination ) ){
	urlDestination = "goToInstrumentNavigator";
     }

     // Set up the images for the left and right sides of the tabs for
     // on and off tabs (as well as orientation: rightside up, upside down)
     if (InstrumentServices.isBlank(onTop) || onTop.equals("true")) {
        onLeft   = "/portal/images/"+brandingDirectory+resMgr.getText("brandedImage.TabLeft", TradePortalConstants.TEXT_BUNDLE);
        onRight  = "/portal/images/"+brandingDirectory+resMgr.getText("brandedImage.TabRight", TradePortalConstants.TEXT_BUNDLE);
        offLeft  = "/portal/images/TabOff_left.gif";
        offRight = "/portal/images/TabOff_right.gif";
     }
     else
     {
        onLeft   = "/portal/images/"+brandingDirectory+resMgr.getText("brandedImage.TabLeftUpsideDown", TradePortalConstants.TEXT_BUNDLE);
        onRight  = "/portal/images/"+brandingDirectory+resMgr.getText("brandedImage.TabRightUpsideDown", TradePortalConstants.TEXT_BUNDLE);
        offLeft  = "/portal/images/TabOff_left_upsidedown.gif";
        offRight = "/portal/images/TabOff_right_upsidedown.gif";
     }

     if (request.getParameter("selected").equals("true")) {
         selected = true;
     } else {
         selected = false; 
     }

     if (request.getParameter("readOnly").equals("true")) {
	   isReadOnly = true;
     } else {
	   isReadOnly = false;
     }

     // Convert any %26 into ampersands.  This is because the '&' character
     // can't be passed in the parameter.
     if ((extraTags != null) && (!extraTags.equals("")))
     {    
       extraTags = StringService.change(extraTags, "%26", "&");  
     }

      if(isReadOnly)
      {
         // If in read only mode, make it a link back to the destination,
         // passing the part name and extra tags as parameters

         if (!InstrumentServices.isBlank(readOnlyTags))
         {
           linkParameters = readOnlyTags;
         }
         else
         {
           linkParameters = "&part="+partName + extraTags;
         }

         link = formMgr.getLinkAsUrl( urlDestination, 
                                      linkParameters,
                                      response);
      }
      else
      {
         // If not in read only mode, make the link submit the form
         link = "javascript:document.forms[0].submit()";
      }

      if (selected)
         {
%>
           <div class="secondaryNavLink navLinkSelected">
               <%=linkLabelDisplay%>
             </div>
<%
         }
      else
         {
           // Otherwise, the tab is set up as a link.
           //Rel 9.2 XSS CID 11321
           if (partName.equals(TradePortalConstants.SHIPMENT_TAB))
           {
             onClickTag = "setShipmentButtonPressed('" + StringFunction.escapeQuotesforJS(partName) + "', " + 0 + ",'" + StringFunction.escapeQuotesforJS(linkLabel) + "');";
           }
           else
           {
             onClickTag = "setButtonPressed('" + StringFunction.escapeQuotesforJS(partName) + "', "+ 0 + "); ";
           }

           if (multiTabLinks)
           {
%>

             <div class="secondaryNavLink">
               <a href="<%=StringFunction.xssCharsToHtml(link)%>" name="<%=partName%>"
                  onClick="<%=onClickTag%>">
                  <%=linkLabelDisplay%>
               </a>
             </div>
             
<%
           }
           else
           {
%>
             <div class="secondaryNavLink">
                 <a href="<%=StringFunction.xssCharsToHtml(link)%>" name="<%=partName%>"
                    onClick="<%=onClickTag%>">
                    <%=linkLabelDisplay%>
                 </a>
               </div>
<%	
           }

         }
%>
