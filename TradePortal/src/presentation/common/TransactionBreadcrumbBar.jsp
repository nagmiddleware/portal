<%--
*******************************************************************************
                         TransactionBreadcrumbBar
  Description:
     Creates the transaction breadcrumb bar.  Creates a one line table with
  the instrument id and instrument type as a link to the instrument summary
  page.  Folling the link is the transaction type and status.  At the far end
  of the line is a help button.

  ** NOTE ** 2 parameters have been added for the online help file:
  1:  'fileName'  **Please remember that the file needs a FORWARD '/' in the 
       file path definition.  This is the name and path to the file based on the 
       webpage defintions.  The fileName is required to be passed in, if no
       fileName is passed in, for the moment we will continue to provide the
       original functionality to maintain backwards compatibility- BUT this will
       change before we go live.
  2:  'anchor'	  This is the anchor the OnlineHelp class needs to search an 
       appropriate topic.  This is also provided for you in the webpage 
       definition.  The anchor value is optional to be passed in...

  This JSP relies on the assumption that "instrument" and "transaction" web
  beans exist in the bean manager.

*******************************************************************************
--%>

<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.web.*,com.ams.tradeportal.common.*,
                 com.ams.tradeportal.busobj.webbean.*,com.ams.tradeportal.html.*,java.net.URLEncoder" %>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager"
    scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" 
    scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" 
    scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" 
   scope="session">
</jsp:useBean>

<%
  TransactionWebBean transaction  = (TransactionWebBean)
                                      beanMgr.getBean("Transaction");
  InstrumentWebBean instrument    = (InstrumentWebBean)
                                      beanMgr.getBean("Instrument");

  StringBuffer parms = new StringBuffer();

  parms.append("&returnAction=");
  parms.append(EncryptDecrypt.encryptStringUsingTripleDes("goToInstrumentSummary", userSession.getSecretKey()));
  parms.append("&instrument_oid=");
  parms.append(EncryptDecrypt.encryptStringUsingTripleDes(instrument.getAttribute("instrument_oid"), userSession.getSecretKey()));

  String loginLocale = userSession.getUserLocale();
  String brandingDirectory = userSession.getBrandingDirectory();
  
  ReferenceDataManager refData = ReferenceDataManager.getRefDataMgr();

  StringBuffer breadCrumb = new StringBuffer();

  breadCrumb.append(instrument.getAttribute("complete_instrument_id"));
  breadCrumb.append("&nbsp;&nbsp;");
  breadCrumb.append(refData.getDescr(TradePortalConstants.INSTRUMENT_TYPE, 
                                     instrument.getAttribute("instrument_type_code"),
                                     loginLocale));
  
  String fileName = request.getParameter("fileName");
  
  String anchor   =request.getParameter("anchor");
%>

  <table width="100%" border="0" cellspacing="0" cellpadding="0" height="34">
    <tr class="BankColor">
      <td width="15" nowrap>&nbsp;</td>
      <td class="BankColor" nowrap height="34">
        <span class="Tabtext">
          <span class="Links">
            <a href=<%=formMgr.getLinkAsUrl("goToInstrumentCloseNavigator", parms.toString(),
                                            response) %>
               class="Links">
               <%=breadCrumb%>
            </a>
          </span> 
          <span class="ControlLabelWhite">
            &gt; <%=refData.getDescr(TradePortalConstants.TRANSACTION_TYPE, 
                                     transaction.getAttribute("transaction_type_code"),
                                     loginLocale)%>
            (<%=refData.getDescr(TradePortalConstants.TRANSACTION_STATUS, 
                                 transaction.getAttribute("transaction_status"),
                                 loginLocale)%>)
          </span>
        </span>
      </td>
      <td width="100%" class="BankColor" height="34">&nbsp;</td>
      <td width="15" nowrap>
<% //Adding Online help file call based on passed in info for the fileName and anchor...
   //This is backwards compatible so as not to affect everyone else that hasn't done this yet.
   if ((fileName != null) && (!fileName.equals(""))) {

	if ((anchor != null) && (!anchor.equals(""))) {
	    out.print( URLEncoder.encode(OnlineHelp.createContextSensitiveLink( fileName, StringFunction.xssCharsToHtml(anchor), resMgr, userSession)));
	}else{
	    out.print( URLEncoder.encode(OnlineHelp.createContextSensitiveLink( fileName, resMgr, userSession)) );
	}
   }else{ 
%>
        <a href="#">
          <img src="/portal/images/<%=brandingDirectory%>/QUESTION_15pixel.gif"
               border="0" width="15" height="15" 
               alt="<%=resMgr.getText("common.ContextHelp", 
                                      TradePortalConstants.TEXT_BUNDLE)%>">
        </a>
<% } %>
      </td>
      <td width="15" nowrap>
        <img src="/portal/images/Blank_15.gif" width="15" height="15">
      </td>
    </tr>
  </table>
<%
  String fromExpress = instrument.getAttribute("from_express_template");
  if (fromExpress != null && fromExpress.equals(TradePortalConstants.INDICATOR_YES))
   {
%>
  <table width="100%" border="0" cellspacing="0" cellpadding="0" height="34">
    <tr class="ColorBeige">
      <td width="15" nowrap>&nbsp;</td>
      <td width="100%" class="Express" nowrap><%=resMgr.getText("transaction.TransactionFromExpressTemplate", TradePortalConstants.TEXT_BUNDLE)%></td>
    </tr>
  </table>
<%
   }
%>
