<%--
*******************************************************************************
                                  Tab On/Off

  Description:
     Create a set of <td> tags that simulates a tab that.  The tab is either 
  selected (displays in the bank color with white text) or is unselected, 
  (displays in grey with black text).  Those tabs which are built as unselected
  are actually href links.  The link is based on the passed in action and any
  additional linkParms that should be included in the link.

  Parameters:
     action      - the navigation manager action to use for the link
     tabOn       - TradePortalConstants.INDICATOR_YES or INDICATOR_NO indicates
                   if the tab is selected.
     linkParms   - optional parameters that are included in the link.  If
                   given, do no append the '&'.  Thus 'tab=2' is a valid parm.
     linkTextKey - the resource bundle key for the text to display in the tab
*******************************************************************************
--%>

<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.ams.tradeportal.common.*, com.amsinc.ecsg.util.StringFunction" %>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session">
</jsp:useBean>

<%
  String action      = request.getParameter("action");
  String tabOn       = request.getParameter("tabOn");
  String parms       = request.getParameter("linkParms");
  String linkTextKey = request.getParameter("linkTextKey");
//MEer Rel 9.3 XSS CID-11383

  String brandingDirectory = userSession.getBrandingDirectory();

  // VERY IMPORTANT:  In the following html, the left and right sides of the tabs
  // are composed of <td><img></td>.  In order for these to render correctly in 
  // Netscape, the td, img, and /td tags must all be on the same line.

  String leftTabPath  = "/portal/images/"+brandingDirectory+resMgr.getText("brandedImage.TabLeft", TradePortalConstants.TEXT_BUNDLE);
  String rightTabPath = "/portal/images/"+brandingDirectory+resMgr.getText("brandedImage.TabRight", TradePortalConstants.TEXT_BUNDLE);

  if (tabOn.equals(TradePortalConstants.INDICATOR_YES)) {
	// class="BankColor" must be on the left <td> tag because the image is left aligned within
      // the cell.  When the page is stretched very far horizontally, a gap will appear when the 
	// cell gets larger than the image.  Having class="BankColor" there fills in the gap.
%>
    <td class="BankColor"><p class=ListText><img src="<%= leftTabPath %>" align=absbottom></p></td>
    <td nowrap align=center class="BankColor">
      <p class="TabLabel"> 
        <%=resMgr.getText(StringFunction.xssCharsToHtml(linkTextKey), TradePortalConstants.TEXT_BUNDLE)%>
      </p>
    </td>
    <td><p class=ListText><img src="<%= rightTabPath %>" align=absbottom></p></td>

<%
  } else {
    if (parms != null) {
       parms = "&" + parms;
    }
%>

    <td class=ListText><img src="/portal/images/TabOff_left.gif" align=absbottom></td>
    <td nowrap class="TabOffBackground"> 
      <p align=center class="TabLabelOff">
        <a href="<%=formMgr.getLinkAsUrl(action, parms, response)%>" 
           class="ButtonLink">
           <%=resMgr.getText(StringFunction.xssCharsToHtml(linkTextKey), TradePortalConstants.TEXT_BUNDLE)%>
        </a>
      </p>
    </td>
    <td class=ListText><img src="/portal/images/TabOff_right.gif" align=absbottom></td>
  
<%
  } 
%>
