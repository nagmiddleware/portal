<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.web.*, com.ams.tradeportal.html.*, com.amsinc.ecsg.util.*, com.ams.tradeportal.busobj.webbean.*, 
                 com.ams.tradeportal.busobj.util.*, com.ams.tradeportal.common.*" %>

<jsp:useBean id="formMgr"     class="com.amsinc.ecsg.web.FormManager" scope="session"></jsp:useBean>
<jsp:useBean id="resMgr"      class="com.amsinc.ecsg.util.ResourceManager" scope="session"></jsp:useBean>
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>
<jsp:useBean id="beanMgr"     class="com.amsinc.ecsg.web.BeanManager" scope="session"></jsp:useBean>
<jsp:useBean id="condDisplay" class="com.ams.tradeportal.html.ConditionalDisplay" scope="request">
  <jsp:setProperty name="condDisplay" property="userSession" value="<%=userSession%>" />
  <jsp:setProperty name="condDisplay" property="beanMgr" value="<%=beanMgr%>" />
</jsp:useBean>

<%
   String currentPrimaryNavigation = userSession.getCurrentPrimaryNavigation();
%>


<%-- cquinton 2/24/2012 new navigation model --%>
<%
    //instantiate the menu factory
    String menuXmlFilePath = "/AdminNavigationMenuBar.xml";
    MenuFactory menuFactory = 
        new MenuFactory(menuXmlFilePath, currentPrimaryNavigation,
			resMgr, userSession,
                        formMgr, response,
                        condDisplay);
%>
<div class="mmWrapper" >
 <div data-dojo-type="t360.widget.MenuBar" class="megaMenuBar" >
  <%--cquinton 1/29/2013 add background div for those brands that just wrap the menu itself--%>
  <div class="megaMenuBarBackground"></div>

  <%--cquinton 1/29/2013 add div for grouping standard menu bar items.
      this is just to be consistent with the non-admin version--%>
  <%-- <span class="standardMenuBarItems"> --%>

<%= menuFactory.createMenuBarItem("AdminNavigationBar.Organizations") %>

<%= menuFactory.createMenuBarItem("AdminNavigationBar.UsersAndSecurity") %>

<%= menuFactory.createMenuBarItem("AdminNavigationBar.Reports") %>

<%= menuFactory.createMenuBarItem("AdminNavigationBar.RefData") %>

<%= menuFactory.createMenuBarItem("AdminNavigationBar.UpdateCentre") %>

<%
    if (userSession.getCustomerAccessIndicator().equals(TradePortalConstants.INDICATOR_YES)) {
%>
  <%= menuFactory.createMenuBarItem("AdminNavigationBar.CustomerAccess") %>
<%
    }
%>

  <%-- </span> --%> <%--end of standardMenuBarItems--%>
  <%--cquinton 1/29/2013 clear all float and end menu--%>
  <div style="clear:both;"></div>

<%--cquinton 11/16/2012 removed my links for admin users--%>
 </div>
</div>
