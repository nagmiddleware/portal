<%
/*******************************************************************************
 * THIS JSP IS A COLLECTION OF JAVA SCRIPT FUNCTIONS THAT SHOULD BE INCLUDED
 * BEFORE THE SAVE DELETE AND CLOSE BUTTONS.  THE JAVA SCRIPT PROVIDES FOR THE
 * CONFIRMATION POPUP MESSAGE THAT IS DISPLAYED AFTER THE DELETE BUTTON IS
 * CLICKED AND THE ROLLOVER EFFECT THAT OCCURS WHEN THE MOUSE POINTER IS MOVED
 * INTO AND OUT OF THE BUTTON AREA.
 *
 * THE JSP TAKES ONE PARAMETER THAT TELLS WHEATHER OR NOT TO DISPLAY THE DELETE
 * CONFIRMATION POPUP WINDOW
 *
 * -ERSKINE
 *******************************************************************************/
%>

<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.ams.tradeportal.busobj.webbean.*,com.ams.tradeportal.common.*,
                 com.ams.tradeportal.html.*,java.util.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.frame.*" %>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session"></jsp:useBean>

    <script LANGUAGE="JavaScript">
<%
    boolean showPopup = true;

    if (request.getParameter("hideDeleteConfim") != null)
        if (request.getParameter("hideDeleteConfim").equals("true"))
            showPopup = false;

    if (showPopup) {
%>
        function confirmDelete() {
        <%--
        // Display a popup asking if the user wants to delete.  If OK is pressed,
        // true is returned.  If Cancel is pressed, false it returned.
        --%>
            var msg = "<%=resMgr.getText("confirm.delete",TradePortalConstants.TEXT_BUNDLE)%>"

            if (!confirm(msg))
             {
                formSubmitted = false;
                return false;
             }

            return true;
        }
<%
    } //end if(showPopup)
%>

</script>
