<%--
 *  Everything associatd with non-admin menus that should be placed in the footer area.
 *  This includes dialogs that are solely associated with the menu.
 *  Note that the dialogs themselves are defined
 *  at \presentation\dialog.  Behavior below is specific to use from menus.
 *  
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.web.*, com.amsinc.ecsg.util.*, com.amsinc.ecsg.frame.*, com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*, com.ams.tradeportal.common.*, com.ams.tradeportal.common.cache.*, 
                 com.ams.tradeportal.html.*, com.ams.tradeportal.busobj.*, java.util.*" %>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session"></jsp:useBean>
<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session"></jsp:useBean>
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>
<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session"></jsp:useBean>
<jsp:useBean id="condDisplay" class="com.ams.tradeportal.html.ConditionalDisplay" scope="request">
  <jsp:setProperty name="condDisplay" property="userSession" value="<%=userSession%>" />
  <jsp:setProperty name="condDisplay" property="beanMgr" value="<%=beanMgr%>" />
</jsp:useBean>

<%-- Hide dialog divs  --%>
<%--<div style="display:none;">--%>
<div>

  <div id="templateSearchDialog" ></div>
  <div id="instrumentSearchDialog" ></div>
  <div id="bankBranchSelectorDialog" ></div>
  <div id="transferExportLCDialog" ></div>
  <div id="copyDirectDebitDialog" ></div>
  <div id="copyPaymentsDialog" ></div>
  <div id="otherConditionsDialog" ></div>
  <div id="deleteAllConfirmationDialog" ></div>
  <div id="favTaskDialog" ></div>
  <div id="favReportDialog" ></div>
  <div id="corpCustAddressSearchDialog" ></div>
  <div id="routeItemDialog" ></div>

<%  //include a hidden form for new instrument submission
    // this is used for all of the new instrument types available from the menu
    Hashtable newInstrSecParms = new Hashtable();
    newInstrSecParms.put("UserOid", userSession.getUserOid());
    newInstrSecParms.put("SecurityRights", userSession.getSecurityRights());
    newInstrSecParms.put("clientBankOid", userSession.getClientBankOid());
    newInstrSecParms.put("ownerOrg", userSession.getOwnerOrgOid());
    
    
%>
  <form method="post" name="NewInstrumentForm" action="<%=formMgr.getSubmitAction(response)%>">
    <input type="hidden" name="mode" value="NEW_INSTRUMENT"/>
    <input type="hidden" name="copyType" value="Blank"/>
    <input type="hidden" name="bankBranch" />
    <input type="hidden" name="instrumentType" />
    <input type="hidden" name="transactionType" />
    <input type="hidden" name="copyInstrumentOid" />
	<input type="hidden" name="manualInstrumentOid" />	
	<input type="hidden" name="conversionCenterMenuInd" />
	<input type="hidden" name="ConvTransInd" /> 
	<input type="hidden" name="settleInstrWorkItemType" />   
<%= formMgr.getFormInstanceAsInputField("NewInstrumentForm",newInstrSecParms) %>
  </form>
 
 <%
	Hashtable newTempSecParms = new Hashtable();
	newTempSecParms.put("userOid", userSession.getUserOid());
	newTempSecParms.put("securityRights", userSession.getSecurityRights());
	newTempSecParms.put("clientBankOid", userSession.getClientBankOid());
	newTempSecParms.put("ownerOrg", userSession.getOwnerOrgOid());
	newTempSecParms.put("ownerLevel", userSession.getOwnershipLevel());
%>
	<form method="post" name="NewTemplateForm1" action="<%=formMgr.getSubmitAction(response)%>">
	<input type="hidden" name="name" />
	<input type="hidden" name="bankBranch" />
	<input type="hidden" name="transactionType"/>
	<input type="hidden" name="InstrumentType" />
	<input type="hidden" name="copyInstrumentOid" />
	<input type="hidden" name="mode"  />
	<input type="hidden" name="CopyType" />
	<input type="hidden" name="expressFlag"  />
	<input type="hidden" name="fixedFlag" />
	<input type="hidden" name="PaymentTemplGrp" />
	<input type="hidden" name="validationState" />
	<input type="hidden" name="manualInstrumentOid" />	
	<input type="hidden" name="conversionCenterMenuInd" />
	<input type="hidden" name="ConvTransInd" /> 
	<%= formMgr.getFormInstanceAsInputField("NewTemplateForm",newTempSecParms) %>
	</form> 
<%//include a hidden form for Route transaction
// this is used for route of instruments/messages
	Hashtable routeTransactionSecParms = new Hashtable();
	routeTransactionSecParms.put("UserOid", userSession.getUserOid());
	routeTransactionSecParms.put("SecurityRights", userSession.getSecurityRights());
%>

	<form method="post" name="RouteTransactionsForm" action="<%=formMgr.getSubmitAction(response)%>">
	<input type="hidden" name="RouteUserOid" />
	<input type="hidden" name="RouteCorporateOrgOid" value=""/>
	<input type="hidden" name="RouteToUser"/>
	<input type="hidden" name="RouteToOrg" />
	<input type="hidden" name="multipleOrgs" />
	<input type="hidden" name="routeAction"  />
	<input type="hidden" name="fromListView" />
	
	<%= formMgr.getFormInstanceAsInputField("RouteTransactionsForm",routeTransactionSecParms) %>
	</form>
 
 <%
	Hashtable routeMessagesSecParms = new Hashtable();
	routeMessagesSecParms.put("UserOid", userSession.getUserOid());
	routeMessagesSecParms.put("SecurityRights", userSession.getSecurityRights());
%>

	<form method="post" name="RouteMessagesForm" action="<%=formMgr.getSubmitAction(response)%>">
	<input type="hidden" name="RouteUserOid" />
	<input type="hidden" name="RouteCorporateOrgOid" />
	<input type="hidden" name="RouteToUser"/>
	<input type="hidden" name="RouteToOrg" />
	<input type="hidden" name="multipleOrgs" />
	<input type="hidden" name="routeAction"  />
	<input type="hidden" name="fromListView" />
	
	<%= formMgr.getFormInstanceAsInputField("RouteMessagesForm",routeMessagesSecParms) %>
	</form>
	
	<%
	Hashtable routePayRemitSecParms = new Hashtable();
	routePayRemitSecParms.put("UserOid", userSession.getUserOid());
	routePayRemitSecParms.put("SecurityRights", userSession.getSecurityRights());
%>

	<form method="post" name="RoutePayRemitsForm" action="<%=formMgr.getSubmitAction(response)%>">
	<input type="hidden" name="RouteUserOid" />
	<input type="hidden" name="RouteCorporateOrgOid" />
	<input type="hidden" name="RouteToUser"/>
	<input type="hidden" name="RouteToOrg" />
	<input type="hidden" name="multipleOrgs" />
	<input type="hidden" name="routeAction"  />
	<input type="hidden" name="fromListView" />
	<input type="hidden" name="buttonName" />
	
	<%= formMgr.getFormInstanceAsInputField("RoutePayRemitsForm",routePayRemitSecParms) %>
	</form>
</div>

<%
  //create some javascript variables that can be used by the menu event handlers events
  //
  //  bankBranchArray - array of operational bank org oids associated with the user's corp org
  //  templateSearchDialogTitle - localized dialog title
  //  instrumentSearchDialogTitle - localized dialog title
  //  bankBranchSelectorDialogTitle - localized dialog title
  //  transferExportLCDialogTitle - localized dialog title
  //

  String menuOwnerOrg = userSession.getOwnerOrgOid();
  CorporateOrganizationWebBean menuCorpOrg = beanMgr.createBean(CorporateOrganizationWebBean.class, "CorporateOrganization");
  menuCorpOrg.getById(menuOwnerOrg);

  String[] menuOpBankOid = {menuCorpOrg.getAttribute("first_op_bank_org"),
                            menuCorpOrg.getAttribute("second_op_bank_org"),
                            menuCorpOrg.getAttribute("third_op_bank_org"),
                            menuCorpOrg.getAttribute("fourth_op_bank_org")};
  String menuBankBranches = "";
  for (int bbaIdx=0; bbaIdx<4; bbaIdx++)
  {
      if (!InstrumentServices.isBlank(menuOpBankOid[bbaIdx])) {
          if ( menuBankBranches.length()>0 ) {
              menuBankBranches += ",";
          }
          menuBankBranches += "'" + menuOpBankOid[bbaIdx] + "'";
      }
      else {
          break; //if a blank is found there will be no more
      }
  }

	/*Kyriba CR 268
	Get all External Bank's for this particular corporate customer.
	*/
	String serverLocation = JPylonProperties.getInstance().getString("serverLocation");
	ClientServerDataBridge csdb = resMgr.getCSDB();
	long longOldCorpOrgOid = Long.parseLong(menuOwnerOrg.trim());
	
	CorporateOrganization transOriginal = (CorporateOrganization)EJBObjectFactory.createClientEJB(serverLocation,
					   "CorporateOrganization", longOldCorpOrgOid, csdb);
	ComponentList externalBankComponenet = (ComponentList) transOriginal.getComponentHandle("ExternalBankList");
	int extBankTotal = externalBankComponenet.getObjectCount();
	
	
	BusinessObject externalBankBusinessObj = null;
	String externalBankBranches = "";
	String externalBankOid = null;
	
	for(int i=0; i<extBankTotal; i++){
		
		externalBankComponenet.scrollToObjectByIndex(i);
		externalBankBusinessObj = externalBankComponenet.getBusinessObject();
		externalBankOid = externalBankBusinessObj.getAttribute("op_bank_org_oid");		
		  
		      if (!InstrumentServices.isBlank(externalBankOid)) {
		          if ( externalBankBranches.length()>0 ) {
		        	  externalBankBranches += ",";
		          }
		          externalBankBranches += "'" + externalBankOid + "'";
		      }
		      else {
		          break; //if a blank is found there will be no more
		      }		
	}


  //dialog titles - get them from text resource
  String templateSearchDialogTitle = resMgr.getTextEscapedJS("templateSearchDialog.title", TradePortalConstants.TEXT_BUNDLE);
  String instrumentSearchDialogTitle = resMgr.getTextEscapedJS("instrumentSearchDialog.title", TradePortalConstants.TEXT_BUNDLE);
  String bankBranchSelectorDialogTitle = resMgr.getTextEscapedJS("bankBranchSelectorDialog.title", TradePortalConstants.TEXT_BUNDLE);
  String transferExportLCDialogTitle = resMgr.getTextEscapedJS("transferExportLCDialog.title", TradePortalConstants.TEXT_BUNDLE); 
  String copyDirectDebitDialogTitle = resMgr.getTextEscapedJS("NewInstrumentsMenu.ExistingDirectDebitSearch.DirectDebitSearch", TradePortalConstants.TEXT_BUNDLE);
  String copyPaymentsDialogTitle = resMgr.getTextEscapedJS("NewInstrumentsMenu.ExistingPaymentSearch.PaymentSearch", TradePortalConstants.TEXT_BUNDLE);
  String otherConditionsDialogTitle = resMgr.getTextEscapedJS("common.OtherConditionsDialogTitle", TradePortalConstants.TEXT_BUNDLE);
  String deleteAllConfirmationDialogTitle = resMgr.getTextEscapedJS("Notifications.DeleteAll", TradePortalConstants.TEXT_BUNDLE);
  String corpCustAddressSearchDialogTitle = resMgr.getTextEscapedJS("AddressSearch.TabHeading", TradePortalConstants.TEXT_BUNDLE);
  String routeItemDialogTitle = resMgr.getTextEscapedJS("routeDialog.title", TradePortalConstants.TEXT_BUNDLE);
  String corpCustButtonPressed = TradePortalConstants.BUTTON_ADDRESSSEARCH;
  
  //instantiate the menu factory for generating event handlers
  String menuXmlFilePath = "/NavigationMenuBar.xml";
  MenuFactory menuFactory =
    new MenuFactory(menuXmlFilePath, null,
                    resMgr, userSession,
                    formMgr, response,
                    condDisplay);
  MyLinksFactory myLinksFactory =
      new MyLinksFactory(menuXmlFilePath, null,
                      resMgr, userSession,
                      formMgr, response,
                      condDisplay, null );
%>
<script>

  var bankBranchArray =[ <%=menuBankBranches%> ];
  var externalBankBranchArray = [ <%=externalBankBranches%> ]; <%-- Kyriba CR 268 --%>
  var templateSearchDialogTitle = '<%=templateSearchDialogTitle%>';
  var instrumentSearchDialogTitle = '<%=instrumentSearchDialogTitle%>';
  var bankBranchSelectorDialogTitle = '<%=bankBranchSelectorDialogTitle%>';
  var transferExportLCDialogTitle = '<%=transferExportLCDialogTitle%>';
  var copyDirectDebitDialogTitle = '<%=copyDirectDebitDialogTitle%>';
  var copyPaymentsDialogTitle = '<%=copyPaymentsDialogTitle%>';
  var otherConditionsDialogTitle = '<%=otherConditionsDialogTitle%>';
  var deleteAllConfirmationDialogTitle = '<%=deleteAllConfirmationDialogTitle%>';
  var otherConditionsDialogTitle = '<%=otherConditionsDialogTitle%>';
  var corpCustAddressSearchDialogTitle = '<%=corpCustAddressSearchDialogTitle%>';
  var routeItemDialogTitle = '<%=routeItemDialogTitle%>';
  var corpCustButtonPressed = '<%=corpCustButtonPressed%>';
  var copyInstrumentDialogTitle = '<%=resMgr.getTextEscapedJS("CopySelected.Title",TradePortalConstants.TEXT_BUNDLE) %>';
  var copyInstrumentDialogPrompt1 = '<%=resMgr.getTextEscapedJS("CopySelected.PromptMessage4",TradePortalConstants.TEXT_BUNDLE) %>';
  var copyInstrumentDialogPrompt2 = '<%=resMgr.getTextEscapedJS("CopySelected.PromptMessage5",TradePortalConstants.TEXT_BUNDLE) %>';

  <%--generate menu event handlers--%>

  require(["dojo/query", "dojo/on", "t360/menu", "t360/dialog", "dojo/ready"],
      function(query, on, menu, dialog, ready ) {

    ready(function() {
      <%-- favorites --%>
      query('#editFavTasks').on("click", function() {
        dialog.open('favTaskDialog', '<%=resMgr.getTextEscapedJS("FavoriteTask.AddTask", TradePortalConstants.TEXT_BUNDLE)%>',
                    'CustomFavoriteLinkDialog.jsp',
                    null,null, <%-- no parameters --%>
                    null, null); <%-- no callbacks --%>
      });
      query('#editFavReports').on("click", function() {
        dialog.open('favReportDialog', '<%=resMgr.getTextEscapedJS("FavoriteReport.AddReport", TradePortalConstants.TEXT_BUNDLE)%>',
                    'ReportFavoriteLinkDialog.jsp',
                    null,null, <%-- no parameters --%>
                    null, null); <%-- no callbacks --%>
      });
      <%= menuFactory.generateMenuBarItemEventHandlers("NavigationBar.NewInstruments") %>
      <%= menuFactory.generateMenuBarItemEventHandlers("NavigationBar.Transactions") %>
	<%-- Srinivasu_D CR#269 Rel8.4 09/02/2013  start --%>
	  <%= menuFactory.generateMenuBarItemEventHandlers("NavigationBar.ConversionCenter") %>  
	<%-- Srinivasu_D CR#269 Rel8.4 09/02/2013 - end --%>
      <%--cquinton 11/20/2012 add event handlers for my links, this avoids reparsing the menu xml--%>
      <%= menuFactory.generateMyLinksFavoriteTasksEventHandlers() %>
    });
  });

</script>
