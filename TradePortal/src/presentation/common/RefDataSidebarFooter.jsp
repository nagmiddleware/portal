<%--
*******************************************************************************
  RefDataSidebar Footer

  Description:
    Includes all javascript and event handlers associated with the refdata sidebar.

*******************************************************************************
--%>


<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page
	import="com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,com.ams.tradeportal.busobj.util.*,com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*, com.ams.tradeportal.html.*"%>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager"
	scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager"
	scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager"
	scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean"
	scope="session">
</jsp:useBean>



<script>
  require(["dijit/registry", "dojo/on", "t360/common", "dojo/ready"],
      function(registry, on, common, ready) {
    ready(function() {
    	dojo.query("input[type='button']").forEach(function(node){
			registry.getEnclosingWidget(node).on( "click", function(evt){
				
				var cTime = evt.timeStamp;
				document.getElementById("cTime").value= (new Date(cTime)).getTime();
				document.getElementById("cTime").text= (new Date(cTime)).getTime();
				document.getElementById("prevPage").value= context;
				document.getElementById("prevPage").text= context;
				});
	  	});
      <%--register event handlers--%>
      <%--todo: use event propagation to reduce registry associations--%>
      var myButton;
      myButton = registry.byId("SaveButton");
      if ( myButton ) {
        myButton.on("click", function() {
          
         var formValid = true;
		 common.setButtonPressed('SaveTrans','0'); <%--  might be overridden by parent jsp --%>
			
          if (typeof preSave === "function"){
              formValid = preSave();
           }
           
           if (formValid){
                  document.forms[0].submit();        
          }
        });
      }

           
      myButton = registry.byId("SaveCloseButton");
      if ( myButton ) {
        
        myButton.on("click", function() {
          var formValid = true;

		  common.setButtonPressed('SaveCloseTrans','0'); <%--  might be overridden by parent jsp --%>
          
          if (typeof preSaveAndClose === "function"){
             formValid = preSaveAndClose();
           }
 		 
          if (formValid){
                 document.forms[0].submit();        
          }
        });
      }
    });
  });
</script>
