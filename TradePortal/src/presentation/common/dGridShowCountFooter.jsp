<%--
*******************************************************************************
  gridShowCountFooter

  Description: include this in the footer section for any page that includes
    gridShowCount.jsp

  Parameters:
     gridId - the dom id of the grid

*******************************************************************************
--%>

<%@ page import="com.ams.tradeportal.common.*, com.amsinc.ecsg.util.StringFunction"%>

<%
  String gridId = request.getParameter("gridId");

//MEer Rel 9.3 CID-11346, 11408

  //if includeRequire is set to 'N' we skip the script and require statement
  // with assumption calling has already done
  String includeRequire = request.getParameter("includeRequire");
  
  
%>
<script>
require(["dojo/dom", "dojo/on", "dojo/dom-class", "dojo/dom-style", "dgrid/extensions/ColumnResizer", 
         "dojo/aspect","dijit/registry","dojo/ready","dojo/domReady!"], 
		function(dom, on, domClass, domStyle, ColumnResizer, aspect,registry,ready){
	ready(function(){
		var grid=registry.byId('<%=StringFunction.escapeQuotesforJS(gridId)%>');
		<%-- Use aspect approach to override resize listenner --%>
		if(grid){
			aspect.after(grid, "resize", function(){		
	
				var showCount = parseInt((dojo.query(".selected"))[0].innerHTML,10);
				setDynamicGridHeitht(null, '<%=StringFunction.escapeQuotesforJS(gridId)%>');
				if (showCount == 10){
					domClass.remove("<%=StringFunction.escapeQuotesforJS(gridId)%>_showCount20",'selected');
				    domClass.remove("<%=StringFunction.escapeQuotesforJS(gridId)%>_showCount30",'selected');
				    domClass.add("<%=StringFunction.escapeQuotesforJS(gridId)%>_showCount10",'selected');
				}else if (showCount == 20){
					domClass.remove("<%=StringFunction.escapeQuotesforJS(gridId)%>_showCount10",'selected');
				    domClass.remove("<%=StringFunction.escapeQuotesforJS(gridId)%>_showCount30",'selected');
				    domClass.add("<%=StringFunction.escapeQuotesforJS(gridId)%>_showCount20",'selected');
				}else if (showCount == 30){
					domClass.remove("<%=StringFunction.escapeQuotesforJS(gridId)%>_showCount10",'selected');
				    domClass.remove("<%=StringFunction.escapeQuotesforJS(gridId)%>_showCount20",'selected');
				    domClass.add("<%=StringFunction.escapeQuotesforJS(gridId)%>_showCount30",'selected');
				}
				
			}); <%-- end of Aspect --%>
		}

		<%-- call after dgrid refresh complete --%>
		var columnSelect = true;
	on(dom.byId('<%=StringFunction.escapeQuotesforJS(gridId)%>'), 'dgrid-refresh-complete', function (event) {

		var myGrid = dijit.byId('<%=StringFunction.escapeQuotesforJS(gridId)%>');


		function onColumnSort(){
        	columnSelect = false;
        } 
	if(columnSelect){

		setDynamicGridHeitht(null, '<%=StringFunction.escapeQuotesforJS(gridId)%>');
		domClass.remove("<%=StringFunction.escapeQuotesforJS(gridId)%>_showCount10",'selected');
	    domClass.remove("<%=StringFunction.escapeQuotesforJS(gridId)%>_showCount30",'selected');
	    domClass.add("<%=StringFunction.escapeQuotesforJS(gridId)%>_showCount20",'selected');

		<%-- hide the dgrid's default columncustomization --%>
		removeColumnHider('<%=StringFunction.escapeQuotesforJS(gridId)%>');

        }
	});

		<%-- Setting Grid height on click of (10, 20, 30) links --%>
		function setDynamicGridHeitht(event, gridId){
			var myGrid = registry.byId(gridId);
			var rowCount = parseInt("0", 10);
			if (event == null){
				rowCount = 20; <%--parseInt((dojo.query(".selected"))[0].innerHTML,10); --%>
			}else{
			 	rowCount = parseInt(event.target.innerHTML, 10);
			}
			<%--  Calculate the height of the scrollbar --%>
			<%-- var dgridScrollerNode = query('.dgrid-scroller', myGrid.domNode)[0]; --%>
			  var dgridHeaderNode = myGrid.headerNode;
			  
		    var dgridScrollerNode = myGrid.bodyNode;
		    var scrollBarHeight = dgridScrollerNode.offsetHeight - dgridScrollerNode.clientHeight;
		    
			<%-- Include column header height with the grid height --%>
			var maxHeight = (myGrid.rowHeight * (rowCount)) + (myGrid.headerNode.offsetHeight+scrollBarHeight) + 'px';
			var scrollMaxHeight = (myGrid.rowHeight * (rowCount)) + scrollBarHeight + 'px';
			
			domStyle.set(myGrid.domNode, 'maxHeight', maxHeight);
			<%-- domStyle.set(dgridScrollerNode, 'maxHeight', scrollMaxHeight); --%>
			domStyle.set(dgridScrollerNode, 'height', scrollMaxHeight);
			var recCount = myGrid._total;
			
			<%--  below code ensures grid does not show blanks rows when records count is less  --%>
			<%--  then the selected row count. e.g. when row count is 10 and record count is 8 the we --%>
			<%--  show only 8 rows. --%>
			if (recCount < rowCount){
			    var dgridContentNode = myGrid.contentNode;

			    <%-- Adding this code to to prevent IE8 crash --%>
			    if(navigator.appVersion.indexOf("MSIE 8")==-1){
			    	dgridScrollerNode.style.height = '';
			    }
			    <%-- dgridScrollerNode.style.height = ''; commented as it cause horizontal scroll in IE8 to disappear. --%>
			    dgridContentNode.style.height = 'auto';
			    if (dgridContentNode.offsetHeight < dgridScrollerNode.offsetHeight) {
			    	var dgScrlNodeOffsetHght = dgridContentNode.offsetHeight;
					dgridScrollerNode.style.height = dgScrlNodeOffsetHght + 'px';
					if (recCount == 0){
						dgridScrollerNode.style.height = '';
						dgridScrollerNode.style.height = dgScrlNodeOffsetHght + 'px';
					}else if (recCount <= rowCount){
						if (dgridScrollerNode.scrollWidth > dgridScrollerNode.offsetWidth){
			    			dgridScrollerNode.style.height = (dgridContentNode.offsetHeight + myGrid.rowHeight) + 'px';
						} else{
			    			dgridScrollerNode.style.height = (dgridContentNode.offsetHeight) + 'px';
			    		}
			    	}
			    }else{
			    	if (recCount < rowCount){
						if (dgridScrollerNode.scrollWidth > dgridScrollerNode.offsetWidth){
			    			dgridScrollerNode.style.height = (dgridContentNode.offsetHeight + myGrid.rowHeight) + 'px';
						} else{
			    			dgridScrollerNode.style.height = (dgridContentNode.offsetHeight) + 'px';
			    		}

			    	}
			    }
			    dgridContentNode.style.height = '';
		    }
		} <%-- end of setgrid --%>

		<%-- On click on 10 20 or 30 --%>
		on(dom.byId('<%=StringFunction.escapeQuotesforJS(gridId)%>_showCount10'), 'click', function (event) {
			setDynamicGridHeitht(event, '<%=StringFunction.escapeQuotesforJS(gridId)%>');
			
			domClass.remove("<%=StringFunction.escapeQuotesforJS(gridId)%>_showCount20",'selected');
		    domClass.remove("<%=StringFunction.escapeQuotesforJS(gridId)%>_showCount30",'selected');
		    domClass.add("<%=StringFunction.escapeQuotesforJS(gridId)%>_showCount10",'selected');
		});
		on(dom.byId('<%=StringFunction.escapeQuotesforJS(gridId)%>_showCount20'), 'click', function (event) {
			setDynamicGridHeitht(event, '<%=StringFunction.escapeQuotesforJS(gridId)%>');
			
			domClass.remove("<%=StringFunction.escapeQuotesforJS(gridId)%>_showCount10",'selected');
		    domClass.remove("<%=StringFunction.escapeQuotesforJS(gridId)%>_showCount30",'selected');
		    domClass.add("<%=StringFunction.escapeQuotesforJS(gridId)%>_showCount20",'selected');
		});
		on(dom.byId('<%=StringFunction.escapeQuotesforJS(gridId)%>_showCount30'), 'click', function (event) {
			<%-- Rel 9.2 XSS CID 11346 --%>
			setDynamicGridHeitht(event, '<%=StringFunction.escapeQuotesforJS(gridId)%>');
			
			domClass.remove("<%=StringFunction.escapeQuotesforJS(gridId)%>_showCount10",'selected');
		    domClass.remove("<%=StringFunction.escapeQuotesforJS(gridId)%>_showCount20",'selected');
		  <%-- Rel 9.2 XSS CID 11408 --%>
		    domClass.add("<%=StringFunction.escapeQuotesforJS(gridId)%>_showCount30",'selected');
		});
		<%-- End of click 10, 20 or 30 --%>
		
	});

	<%-- We have included Dgrid's ColumnHider exteinction to hide/show the grid columns but as we don't require the Dgrids default  --%>
	<%-- customization removing that on grid-refresh-complete event --%>
	function removeColumnHider(grid){
		if(document.getElementById("dgrid-hider-menu-"+grid) != null){
			var columnHider = document.getElementById("dgrid-hider-menu-"+grid);
			columnHider.parentNode.removeChild(columnHider);
		}
	}
});
</script>
