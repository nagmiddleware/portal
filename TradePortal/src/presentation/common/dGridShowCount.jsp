<%--
*******************************************************************************
  gridShowCount

  Description: html for grid show counts (i.e. Showing 10 20 30)

  Parameters:
     gridId - the dom id of the grid

*******************************************************************************
--%>

<%@ page import="com.amsinc.ecsg.frame.*, com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*, com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*, com.ams.tradeportal.html.*,
                 com.ams.tradeportal.busobj.webbean.*,com.businessobjects.rebean.wi.*,java.util.*, com.ams.tradeportal.common.cache.*,com.ams.tradeportal.busobj.util.SecurityAccess"%>


<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session"> </jsp:useBean>
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean"  scope="session"></jsp:useBean>
<%
  String gridId = request.getParameter("gridId");
  String defaultCount = request.getParameter("defaultCount");
//jgadela Rel 8.4 CR-854 [BEGIN]- Adding reporting language option
  String pageName = request.getParameter("page");

//jgadela R 8.4 CR-854 T36000024797   -[START] Reporting page labels. 
  String reportingPageLabelLan1 = null;
  String cbReportingLangOptionInd1 = null;
  Cache CBCache1 = (Cache)TPCacheManager.getInstance().getCache(TradePortalConstants.CLIENT_BANK_CACHE);
  DocumentHandler CBCResult1 = (DocumentHandler)CBCache1.get(userSession.getClientBankOid());
  if (!StringFunction.isBlank(userSession.getClientBankOid())){
  	   cbReportingLangOptionInd1 = CBCResult1.getAttribute("/ResultSetRecord(0)/REPORTING_LANG_OPTION_IND"); // jgadela Rel 8.4 CR-854 
  }

  if(TradePortalConstants.INDICATOR_YES.equals(cbReportingLangOptionInd1)){
  	reportingPageLabelLan1 = userSession.getReportingLanguage();
  }else{
  	reportingPageLabelLan1 = userSession.getUserLocale();
  }
  //jgadela R 8.4 CR-854 T36000024797   -[END]
  
//Validation to check Cross Site Scripting

  if(defaultCount != null){
	  defaultCount = StringFunction.xssCharsToHtml(defaultCount);
  }

  if ( !"10".equals(defaultCount) && !"30".equals(defaultCount) ) {
    defaultCount = "20";
  }
%>

<%--temp comment out
--%>
<span id="<%= StringFunction.xssCharsToHtml(gridId)%>_showCounts" class="gridShowCounts">
<%//jgadela Rel 8.4 CR-854 [BEGIN]- Adding reporting language option%>
  <span><%=("report".equalsIgnoreCase(pageName))?resMgr.getText("dataGrid.showCountPreText", TradePortalConstants.TEXT_BUNDLE, reportingPageLabelLan1):resMgr.getText("dataGrid.showCountPreText", TradePortalConstants.TEXT_BUNDLE)%></span>
  <span id="<%= StringFunction.xssCharsToHtml(gridId)%>_showCount10" class='gridShowCountItem<%= ("10".equals(defaultCount))? " selected":"" %>' >10</span>
  <span id="<%= StringFunction.xssCharsToHtml(gridId)%>_showCount20" class='gridShowCountItem<%= ("20".equals(defaultCount))? " selected":"" %>' >20</span>
  <span id="<%= StringFunction.xssCharsToHtml(gridId)%>_showCount30" class='gridShowCountItem<%= ("30".equals(defaultCount))? " selected":"" %>' >30</span>
</span>
<%--
--%>
