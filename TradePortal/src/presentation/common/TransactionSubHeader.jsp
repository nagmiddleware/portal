<%--
*******************************************************************************
  TransactionSubHeader

  The transaction sub header.
   Creates the transaction breadcrumb bar.  Creates a one line table with
  the instrument id and instrument type as a link to the instrument summary
  page.  Folling the link is the transaction type and status.  At the far end
  of the line is a help button.

  This JSP relies on the assumption that "instrument" and "transaction" web
  beans exist in the bean manager.

*******************************************************************************
--%>

<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.web.*,com.ams.tradeportal.common.*,
                 com.ams.tradeportal.busobj.webbean.*,com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.html.*" %>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager"
    scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" 
    scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" 
    scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" 
   scope="session">
</jsp:useBean>

<%
  TransactionWebBean transaction  = (TransactionWebBean)
                                      beanMgr.getBean("Transaction");
  InstrumentWebBean instrument    = (InstrumentWebBean)
                                      beanMgr.getBean("Instrument");
  
  String transactionStatus = "";
  transactionStatus = transaction.getAttribute("transaction_status");
  String bankReleasedTerms = transaction.getAttribute("c_BankReleasedTerms");
  boolean hasBankReleasedTerms = InstrumentServices.isNotBlank( bankReleasedTerms );

  ReferenceDataManager refData = ReferenceDataManager.getRefDataMgr();
  String loginLocale = userSession.getUserLocale();

  StringBuffer title = new StringBuffer();
  String item1Key = "";
  String link = "";
  if(instrument.getAttribute("instrument_type_code").equals("SLC")){
  if(transaction.getAttribute("standby_using_guarantee_form").equals(TradePortalConstants.INDICATOR_NO)){     
		  item1Key = resMgr.getText("SecondaryNavigation.Instruments.OutgoingSTandbyLCSimple", TradePortalConstants.TEXT_BUNDLE); 
  }else{
		  item1Key = resMgr.getText("SecondaryNavigation.Instruments.OutgoingStandbyLCDetailed", TradePortalConstants.TEXT_BUNDLE);
  }
  title.append(item1Key);
  }
  else
  title.append( refData.getDescr(TradePortalConstants.INSTRUMENT_TYPE, 
                 instrument.getAttribute("instrument_type_code"), 
                 loginLocale) );
  title.append( " - " );
  title.append( instrument.getAttribute("complete_instrument_id") );
  title.append( " - " );
  title.append( refData.getDescr(TradePortalConstants.TRANSACTION_TYPE, 
                 transaction.getAttribute("transaction_type_code"),
                 loginLocale) );
  title.append( " - " );
  title.append( "(" );
  title.append( refData.getDescr(TradePortalConstants.TRANSACTION_STATUS, 
                 transaction.getAttribute("transaction_status"),
                 loginLocale) );
  title.append( ")" );
  String transactionSubHeader = title.toString();
  //CR-1026 MEer 
  StringBuilder bankInstTitle = new StringBuilder();
  String bankInstIDSubHeader ="";
  String instrumentOid  = instrument.getAttribute("instrument_oid");
  String instrumentTypeCode = instrument.getAttribute("instrument_type_code");
  boolean displayBankInstrId = false;
  
  if(userSession.isCustNotIntgTPS()){
	  if(InstrumentServices.isValidInstrumentTypeForBTMU(instrumentOid, instrumentTypeCode)){
		  displayBankInstrId = true;
	  }	   
  }

  if(displayBankInstrId){
	  String bankInstrumentId = instrument.getAttribute("bank_instrument_id");	  	
	  if(StringFunction.isNotBlank(bankInstrumentId )){
		 bankInstTitle.append(resMgr.getText("TransactionSummary.BankInstrumentID",TradePortalConstants.TEXT_BUNDLE));
		 bankInstTitle.append(" "+bankInstrumentId);  
	  }
  }
 
  if(StringFunction.isNotBlank(bankInstTitle.toString())){ 
  	 bankInstIDSubHeader = bankInstTitle.toString();
  }

  //generate the return link. the InstrumentCloseNavigator.jsp
  // figures out how to return to the calling page
  StringBuffer parms = new StringBuffer();
  parms.append("&returnAction=");
  parms.append(EncryptDecrypt.encryptStringUsingTripleDes("goToInstrumentSummary", userSession.getSecretKey()
));
  parms.append("&instrument_oid=");
  parms.append(EncryptDecrypt.encryptStringUsingTripleDes(instrument.getAttribute("instrument_oid"), userSession.getSecretKey()));
  String returnLink = formMgr.getLinkAsUrl("goToInstrumentCloseNavigator", 
    parms.toString(), response);  
  link = formMgr.getLinkAsUrl("transactionClose", response);
	  //rkazi IR RSUL020973906 02/11/2011 START added 2 Transaction statuses to condition
  if ((transactionStatus.equals( TradePortalConstants.TRANS_STATUS_PROCESSED_BY_BANK ) || 
	  transactionStatus.equals( TradePortalConstants.TRANS_STATUS_REJECTED_PAYMENT ) ||
	  transactionStatus.equals( TradePortalConstants.TRANS_STATUS_PARTIALLY_REJECTED_PAYMENT )) && hasBankReleasedTerms) {
	  //rkazi IR RSUL020973906 02/11/2011 END	
	  link = formMgr.getLinkAsUrl("goToTransactionTermsDetails", response);
	  }
%>

<jsp:include page="/common/PageSubHeader.jsp">
  <jsp:param name="titleKey" value="<%= transactionSubHeader %>" />
  <jsp:param name="titleBankInstIDKey" value="<%= bankInstIDSubHeader  %>" /> 
  <jsp:param name="returnUrl" value="<%= returnLink %>" />
</jsp:include>
