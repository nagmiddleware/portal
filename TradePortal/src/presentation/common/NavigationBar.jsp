<%--
 *
 *     Copyright   2001
 *     American Management Systems, Incorporated
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.web.*, com.amsinc.ecsg.util.*, com.amsinc.ecsg.frame.*, com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*, com.ams.tradeportal.common.*, com.ams.tradeportal.common.cache.*,
                 com.ams.tradeportal.mediator.util.TradePortalSessionListener, com.ams.tradeportal.html.*,com.ams.tradeportal.busobj.util.*" %>

<jsp:useBean id="formMgr"     class="com.amsinc.ecsg.web.FormManager"            scope="session"></jsp:useBean>
<jsp:useBean id="resMgr"      class="com.amsinc.ecsg.util.ResourceManager"       scope="session"></jsp:useBean>
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>
<%--CR-433 Krishna Begin --%>
<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session"></jsp:useBean>
<%--CR-433 Krishna End --%>
<%--conditional display request bean is passed to menu factory to avoid additional db hits--%>
<jsp:useBean id="condDisplay" class="com.ams.tradeportal.html.ConditionalDisplay" scope="request">
  <jsp:setProperty name="condDisplay" property="userSession" value="<%=userSession%>" />
  <jsp:setProperty name="condDisplay" property="beanMgr" value="<%=beanMgr%>" />
</jsp:useBean>

<%
  StringBuffer   navigationBarHtml        = new StringBuffer();
  String         securityRights           = null;
  String         currentPrimaryNavigation = null;
  String         link = null;
  String                imageTextResourceKey = null;
  String              altTextResourceKey = null;
  String         brandingDirectory = userSession.getBrandingDirectory();

  securityRights = userSession.getSecurityRights();
  currentPrimaryNavigation = userSession.getCurrentPrimaryNavigation();
  //CR-433 Krishna 05/13/2008  Begin
  CorporateOrganizationWebBean org = beanMgr.createBean(CorporateOrganizationWebBean.class, "CorporateOrganization");
  //PPX-98 Trudden 2/11/2008 Begin
  /** For performance, removed references to UserWebBean to avoid the retrieval of User. Instead, just reference the userSession attribute for owner_org_oid.
   * Also, restructured the select of clientBank to get the doc_prep_url from Cache, to avoid repetitive selects of Client_Bank
   **/
  //UserWebBean user = beanMgr.createBean(UserWebBean.class,"User");
  //ClientBankWebBean clientBank = (ClientBankWebBean) beanMgr.createBean(
  //                                              "com.ams.tradeportal.busobj.webbean.ClientBankWebBean",
  //                                              "ClientBank");
  //clientBank.getById(userSession.getClientBankOid());
  //user.getById(userSession.getUserOid());
  //PPX-98 Trudden 2/11/2008 END

  //org.getById(user.getAttribute("owner_org_oid"));//PPRAKASH IR ALUJ011377704 Commented
  org.getById(userSession.getOwnerOrgOid());//PPRAKASH IR ALUJ011377704  Added

  //Fetch the Corporate Services URL set for the Corporate customer
  String corpServicesURL = org.getAttribute("doc_prep_url");
  //If no Corporate Services URL is set for the Corporate customer then fetch the same from the
  //Customer's Client Bank
  if (InstrumentServices.isBlank(corpServicesURL)) {
    //PPX-98 Trudden 2/11/2008 Begin
    //retrieve ClientBank from cache
    Cache cache = (Cache)TPCacheManager.getInstance().getCache(TradePortalConstants.CLIENT_BANK_CACHE);
    DocumentHandler result = (DocumentHandler)cache.get(userSession.getClientBankOid());
    if (result != null) {
      corpServicesURL = result.getAttribute("/ResultSetRecord(0)/doc_prep_url");
    }
  }
  //PPX-98 Trudden 2/11/2008 End
  //CR-433 Krishna 05/13/2008 End

  WidgetFactory widgetFactory = new WidgetFactory(resMgr);
%>


<%-- cquinton 2/24/2012 new navigation model --%>
<%
  //instantiate the menu factory
  String menuXmlFilePath = "/NavigationMenuBar.xml";
  MenuFactory menuFactory =
    new MenuFactory(menuXmlFilePath, currentPrimaryNavigation,
                    resMgr, userSession,
                    formMgr, response,
                    condDisplay );

  //cquinton 1/29/2013 calculate if mylinks is displayed earlier so
  // it drives additional logic
  boolean displayMyLinks = false;
  if (!(userSession.hasSavedUserSession() && userSession.getSavedUserSessionSecurityType().equals(TradePortalConstants.ADMIN))) {
    displayMyLinks = true;
  }
%>

<div class="mmWrapper" >
 <div data-dojo-type="t360.widget.MenuBar" class="megaMenuBar" >
  <%--cquinton 1/29/2013 add background div for those brands that just wrap the menu itself--%>
  <div class="megaMenuBarBackground"></div>

  <%--cquinton 1/29/2013 add div for grouping standard menu bar items.
      this allows them to wrap to 2nd row if necessary
      and not impact mylinks--%>
   <%-- <span class='standardMenuBarItems<%=displayMyLinks?" withMyLinks":""%>'>  --%>


<%= menuFactory.createMenuBarItem("NavigationBar.NewInstruments") %>

<%= menuFactory.createMenuBarItem("NavigationBar.Transactions") %>

<%= menuFactory.createMenuBarItem("NavigationBar.Accounts") %>

<%= menuFactory.createMenuBarItem("NavigationBar.Reports") %>

<%= menuFactory.createMenuBarItem("NavigationBar.RefData") %>

<%= menuFactory.createMenuBarItem("NavigationBar.UploadCenter") %>

<%-- Prateep 15/03/2014 MyLinks start: Moving to first as we are commenting above span as part of dojo 1.9.2 upgrade --%>
	<%
  //My Links logic
  if ( displayMyLinks ) {

    //favorite tasks
    String myTaskLinks = menuFactory.createMyLinksFavoriteTasks();

    //recent instruments
    //if the newRecentInstrument info is populated, add it to the list
    String newRecentTransactionOid = request.getParameter("newRecentTransactionOid");
    if ( newRecentTransactionOid != null ) {
      String newRecentInstrumentId = request.getParameter("newRecentInstrumentId");
      String newRecentTransactionType = request.getParameter("newRecentTransactionType");
      String newRecentTransactionStatus = request.getParameter("newRecentTransactionStatus");
      userSession.addRecentInstrument(
        newRecentTransactionOid, newRecentInstrumentId, 
        newRecentTransactionType, newRecentTransactionStatus);

      //set the recent instruments in the session listener so it is persisted at logout
      HttpSession theSession = request.getSession(false);
      TradePortalSessionListener listener = (TradePortalSessionListener) 
        theSession.getAttribute("TimeoutListener");
      if ( listener!= null ) {
        listener.setRecentInstrumentsQueue(userSession.getRecentInstrumentsQueue());
      }
    }
    //now get the list to display
    String myRecentTransLinks = menuFactory.createMyLinksRecentInstruments();

    //favorite reports
    String myReportLinks = menuFactory.createMyLinksFavoriteReports();

    //cquinton 1/29/2013 move logic to determine if myLinks is displayed above
    //display the My Links menu item
    //this is a custom menu item so we do it explicitly here rather than use MenuFactory
%>
  <div data-dojo-type="dijit.PopupMenuBarItem" class="myLinks">
   <span>
    <%--cquinton 2/4/2013 ir#10697 replace myLinksIcon class with background image on
        the span. this is becuase of an ie8 formatting issue.--%>
      <%= resMgr.getText("NavigationBar.MyLinks", TradePortalConstants.TEXT_BUNDLE) %>
    <span class="myLinksDropdownIcon" ></span>
   </span>
   <div data-dojo-type="dijit.layout.ContentPane" class="myLinksDropdown" style="display: none;">
    <div class="mmContent">
     <table border="0" cellpadding="0" cellspacing="0">
      <tr class="mmMainSection">
       <td>
<%
    if (userSession.corpCanAccessDocPrep() && userSession.userCanAccessDocPrep()) {
      //ctq not sure what this does
      String corpServicesLink =
        request.getContextPath() + NavigationManager.getNavMan().getPhysicalPage("DocPrepLauncher", request);
      String corpServicesTarget = ""; //by default the target is local
      if (!InstrumentServices.isBlank(corpServicesURL)) {
        corpServicesTarget = "target='DocPrep'";
      }
%>
        <div class="mmMainItems">
         <ul>
          <li><a id="corpservices" href="<%=corpServicesLink%>" <%=corpServicesTarget%>>
            <%= resMgr.getText("MyLinks.CorporateServices", TradePortalConstants.TEXT_BUNDLE) %>
          </a></li>
         </ul>
         <%=widgetFactory.createHoverHelp("corpservices", "MyLinks.CorporateServices") %>
        </div>
<%
    }

      //the my links categories are titlepanes, we add html to the title to get the gear icon
%>
        <div data-dojo-type="dijit.TitlePane"
             title='<span class="myLinksCategory"><%= resMgr.getText("MyLinks.FavoriteTasks", TradePortalConstants.TEXT_BUNDLE) %></span><span id="editFavTasks" class="myLinksEdit"></span>'>
<%
    if ( !InstrumentServices.isBlank(myTaskLinks) ) {
%>
          <div class="mmMainItems">
           <ul>
            <%= myTaskLinks %>
           </ul>
          </div>
<%
    }
%>
        </div>
        <%=widgetFactory.createHoverHelp("editFavTasks", "MyLinks.EditFavoriteTasks") %>

        <div data-dojo-type="dijit.TitlePane"
             title='<span class="myLinksCategory"><%= resMgr.getText("MyLinks.RecentInstruments", TradePortalConstants.TEXT_BUNDLE) %></span>'>
<%
    if ( !InstrumentServices.isBlank(myRecentTransLinks) ) {
%>
          <div class="mmMainItems">
           <ul>
            <%=myRecentTransLinks%>
           </ul>
          </div>
<%
    }
%>
        </div>

<%
    if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_REPORTS ) ) { 
%>
        <div data-dojo-type="dijit.TitlePane"
             title='<span class="myLinksCategory"><%= resMgr.getText("MyLinks.FavoriteReports", TradePortalConstants.TEXT_BUNDLE) %></span><span id="editFavReports" class="myLinksEdit"></span>'>
<%
      if ( !InstrumentServices.isBlank(myReportLinks) ) {
%>
          <div class="mmMainItems">
           <ul>
            <%= myReportLinks %>
           </ul>
          </div>
<%
      }
%>
        </div>
        <%=widgetFactory.createHoverHelp("editFavReports", "MyLinks.EditFavoriteReports") %>
<%
    }
%>

       </td>
      </tr>
     </table>
    </div>
   </div>
  </div>
<%
  } //if (displayMyLinks)
%>
<%-- MyLinks end --%>

<%= menuFactory.createMenuBarItem("NavigationBar.Subsidiaries") %>

<%-- Srinivasu_D CR#269 Rel8.4 09/02/2013 - start --%>
<%  if(SecurityAccess.hasRights(userSession.getSecurityRights(),SecurityAccess.ACCESS_CONVERSION_CENTER_AREA)) {
%>
<%= menuFactory.createMenuBarItem("NavigationBar.ConversionCenter") %>
<% } %> 
<%-- Srinivasu_D CR#269 Rel8.4 09/02/2013 - end --%>

<%	//System.out.println(" has rights or not:-----"+(SecurityAccess.hasRights(securityRights,SecurityAccess.CC_GUA_CREATE_MODIFY)));
  ///////////////////////////////////////////////////////////////////////////////////////////
  // Display Exit Customer Access or Subsidiary Access or Back to My Organisation button
  ////////////////////////////////////////////////////////////////////////////////////////////
  String customerAccessIndicator = userSession.getCustomerAccessIndicator();
  String userSecurityType = null;

  // Get the ownership level of the user.  If this user is within custom access or subsidiary access,
  // the true ownership level is store on the savedUserSession.
  if (userSession.hasSavedUserSession()) {
      userSecurityType = userSession.getSavedUserSessionSecurityType();
  }
  else {
      userSecurityType = userSession.getSecurityType();
  }

  // If an admin user gets to this page, it must be through customer access.
  // Build the link to exit customer access
  if (TradePortalConstants.ADMIN.equals(userSecurityType)) {
    if ((customerAccessIndicator != null) && (TradePortalConstants.INDICATOR_YES.equals(customerAccessIndicator))) {
%>
            <%= menuFactory.createMenuBarItem("NavigationBar.CustomerAccess") %>
<%
    }
  }
  // For non-admin user, check whether this user has subsidiary access or already within the
  // subsidiary access.
  else {
    // If not within subsidiary access: Display "Subsidiary Access" button if needed.
    if (!userSession.hasSavedUserSession()) {
      if ("NavigationBar.SubsidiaryAccess".equals(currentPrimaryNavigation)) {
%>
                <%= menuFactory.createMenuBarItem("NavigationBar.SubsidiaryAccess") %>
<%
      }
      // If the user has subsidiary access, display the button "Subsidiary Access"
      else if (TradePortalConstants.INDICATOR_YES.equals(customerAccessIndicator)) {
%>
                <%= menuFactory.createMenuBarItem("NavigationBar.SubsidiaryAccess") %>
<%
      }
    }
    else { // Within subsidiary access: display "Back to My Organisation" button
%>
            <%= menuFactory.createMenuBarItem("NavigationBar.BackToMyOrg") %>
<%
    }
  }
%>

<%= menuFactory.createMenuBarItem("NavigationBar.Admin") %>

   <%-- </span>  --%> <%--end of standardMenuBarItems--%>



  <%--cquinton 1/29/2013 clear all float and end menu--%>
  <div style="clear:both;"></div>

 </div>
 
</div>
