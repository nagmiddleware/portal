<%--
**********************************************************************************
                              Customer Access

  Description:
     This page is used to allow global org, client bank, and BOG users to select 
     a corporate customer organization in order to act on its behalf, when
     assistance is required.

**********************************************************************************
--%>

<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*, com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*, com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*, com.ams.tradeportal.html.*,
                 com.ams.tradeportal.busobj.webbean.*,java.util.*" %>

<jsp:useBean id="beanMgr"     class="com.amsinc.ecsg.web.BeanManager"            scope="session"></jsp:useBean>
<jsp:useBean id="formMgr"     class="com.amsinc.ecsg.web.FormManager"            scope="session"></jsp:useBean>
<jsp:useBean id="resMgr"      class="com.amsinc.ecsg.util.ResourceManager"       scope="session"></jsp:useBean>
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>

<%-- ************** Data retrieval page setup begins here ****************  --%>

<%

  Debug.debug("***START***********CUSTOMER ACCESS***************START***");

  DocumentHandler   selectBanksDoc      = null;
  DocumentHandler   xmlDoc              = null;
  QueryListView     queryListView       = null;
  StringBuffer      dynamicWhereClause  = new StringBuffer();
  StringBuffer      searchCriteria      = new StringBuffer();
  StringBuffer      sqlQuery            = null;
  Hashtable         secureParms         = new Hashtable();
  String            orgOptions          = null;
  String            ownershipLevel      = null;

  WidgetFactory widgetFactory = new WidgetFactory(resMgr);
  DataGridFactory dgFactory = new DataGridFactory(resMgr, userSession, formMgr, response);

  // If we're returning from acting on behalf of a corporate customer, go ahead and 
  // reset all the original admin user's attributes
  if ((request.getParameter("logout") != null) && 
      (request.getParameter("logout").equals(TradePortalConstants.INDICATOR_YES))) {
    userSession.restoreSession();

    // Remove all corporate org related attributes that may have been set during the
    // course of customer access by the admin user

    //cquinton 1/18/2013 remove old close action behavior
    //replace with clearing the page flow
    userSession.clearPageFlow();

    session.removeAttribute("currentMailOption");
    session.removeAttribute("currentOrgOption");
    session.removeAttribute("mailMessageOid");
    session.removeAttribute("prevStepAction");
    session.removeAttribute("currentFolder");
    session.removeAttribute("historyOrg");
    session.removeAttribute("startPage");
    session.removeAttribute("workflow");
    session.removeAttribute("refdata");
    session.removeAttribute("org");
  }


  // Set the global navigation tab indicator to the Customer Access tab
  if(userSession.getSecurityType().equals(TradePortalConstants.ADMIN)){
    userSession.setCurrentPrimaryNavigation("AdminNavigationBar.CustomerAccess");
  }
  else{
    userSession.setCurrentPrimaryNavigation("NavigationBar.CustomerAccess");
  }
  Map searchQueryMap =  userSession.getSavedSearchQueryData();
  Map savedSearchQueryData =null;
  // Determine whether any errors are being returned to this page. If there are, set the 
  // original search criteria selected/entered by the user for the listview.
  xmlDoc = formMgr.getFromDocCache();


  // Determine the type of user that is accessing this page and construct the dynamic 
  // part of the listview where clause accordingly
  ownershipLevel = userSession.getOwnershipLevel();

  Debug.debug("!!!!!!!!!!!!!!!!!ownershipLevel: " + ownershipLevel);


  // If a global org or client bank user is accessing this page, then we need to retrieve the options
  // that'll be used in the filter dropdown list. If the user is a global org user, retrieve all 
  // active client banks that belong to global org; otherwise, retrieve all active bank organization 
  // groups that belong to the client bank user's org.
  if (TradePortalConstants.OWNER_GLOBAL.equals(ownershipLevel) || 
      TradePortalConstants.OWNER_BANK.equals(ownershipLevel) ) {
    try {
      queryListView = (QueryListView) EJBObjectFactory.createClientEJB(formMgr.getServerLocation(), "QueryListView"); 

      sqlQuery = new StringBuffer();
      List<Object> sqlParams = new ArrayList<Object>();
      if (TradePortalConstants.OWNER_GLOBAL.equals(ownershipLevel)) {
        sqlQuery.append("select organization_oid, name");
        sqlQuery.append(" from client_bank");
        sqlQuery.append(" where activation_status = ? order by ");
        sqlQuery.append(resMgr.localizeOrderBy("name"));
        sqlParams.add(TradePortalConstants.ACTIVE);
      }
      else {
         
         sqlQuery.append("select organization_oid, name");
           sqlQuery.append(" from bank_organization_group");
           sqlQuery.append(" where p_client_bank_oid = ?   and activation_status = ? ");
           sqlParams.add(userSession.getClientBankOid());
           sqlParams.add(TradePortalConstants.ACTIVE);
          
            /* pgedupudi - Rel-9.3 CR-976A 03/30/2015 Start*/
            StringBuilder bnkgrps = new StringBuilder();
            if(StringFunction.isNotBlank(userSession.getBankGrpRestrictRuleOid())){
                  String query="select BANK_ORGANIZATION_GROUP_OID from BANK_GRP_FOR_RESTRICT_RULES where P_BANK_GRP_RESTRICT_RULES_OID= ? ";
                  DocumentHandler bankGrpList = new DocumentHandler();
                  List<Object> params = new ArrayList<Object>();
                  params.add(Long.parseLong(userSession.getBankGrpRestrictRuleOid()));
                  bankGrpList = DatabaseQueryBean.getXmlResultSet(query,false,params);
                  Vector bgVector = bankGrpList.getFragments("/ResultSetRecord");
                  for (int iLoop=0; iLoop<bgVector.size(); iLoop++)
                      {
                             DocumentHandler bankGrpforrestricDoc = (DocumentHandler) bgVector.elementAt(iLoop);
                             bnkgrps.append("?");
                             sqlParams.add(bankGrpforrestricDoc.getAttribute("/BANK_ORGANIZATION_GROUP_OID"));
                             if(iLoop<bgVector.size()-1){
                              bnkgrps.append(",");
                             }
                        }
             }
         if(StringFunction.isNotBlank(userSession.getBankGrpRestrictRuleOid())){
                        sqlQuery.append(" and organization_oid not in ("+ bnkgrps.toString()+")");
             }
             /* pgedupudi - Rel-9.3 CR-976A 03/30/2015 End*/
        sqlQuery.append(" order by ");
        sqlQuery.append(resMgr.localizeOrderBy("name"));
      }

      queryListView.setSQL(sqlQuery.toString(), sqlParams);
      queryListView.getRecords();

      selectBanksDoc = queryListView.getXmlResultSet();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
    finally {
      try {
        if (queryListView != null) {
          queryListView.remove();
        }
      }
      catch (Exception e) {
        System.out.println("Error removing querylistview in CustomerAccess.jsp!");
      }
    }
  }
  String searchNav = "custAccess";  
%>

<jsp:include page="/common/Header.jsp">
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>
<jsp:include page="/common/SearchCriteriaCleanup.jsp">
<jsp:param name="searchCriteriaSection" value="<%=searchNav%>"/>
</jsp:include>
<div class="pageMainNoSidebar">
  <div class="pageContent">

    <jsp:include page="/common/PageHeader.jsp">
      <jsp:param name="titleKey" value="CustomerAccess.Title" />
      <jsp:param name="helpUrl"  value="/admin/corp_cust_search.htm" />
    </jsp:include>

    <form name="CustomerAccessForm" method="POST" action="<%= formMgr.getSubmitAction(response) %>">

      <%= formMgr.getFormInstanceAsInputField("CustomerAccessForm", secureParms) %>

      <input type=hidden value="" name=buttonName>

      <jsp:include page="/common/ErrorSection.jsp" />

      <div class="formContentNoSidebar">

        <div class="searchHeader">
          <div class="searchHeaderActions">
            <%--cquinton 10/8/2012 include gridShowCounts --%>
            <jsp:include page="/common/gridShowCount.jsp">
              <jsp:param name="gridId" value="custAccessGrid" />
            </jsp:include>
          </div>
          <div style="clear:both"></div>
        </div>

        <div class="searchDivider"></div>

        <div class="searchDetail" >
          <span class="searchCriteria">

            <%=widgetFactory.createSearchTextField("CustomerName","CustomerAccess.criteria.CustomerName","35","onKeydown='Javascript: filterCorpCustomerOnEnter(\"CustomerName\");'") %>

<%
  orgOptions = ListBox.createOptionList(selectBanksDoc, 
    "ORGANIZATION_OID", "NAME", " ", userSession.getSecretKey());

  if (TradePortalConstants.OWNER_GLOBAL.equals(ownershipLevel) ) {
%>
           <%-- UAT IR T36000006212 Pavani BEGIN --%>
           <%= widgetFactory.createSearchSelectField( "BankOid", "CustomerAccess.criteria.ClientBank", " ", orgOptions, "class='char40' onChange='searchCorpCustomer();'") %>
           <%-- UAT IR T36000006212 Pavani END --%>
<%
  }
  else if (TradePortalConstants.OWNER_BANK.equals(ownershipLevel) ) {
%>
           <%-- UAT IR T36000006212 Pavani BEGIN --%>
           <%=widgetFactory.createSearchSelectField("BankOid", "CustomerAccess.criteria.BankGroup", " ", orgOptions, "class='char40' onChange='searchCorpCustomer();'") %>
           <%-- UAT IR T36000006212 Pavani END --%>
<%
  } 
  //do not display if ( TradePortalConstants.OWNER_BOG.equals(ownershipLevel) )
%>

          </span>
          <span class="searchActions for2LineLabelCriteria">
            <button id="searchButton" data-dojo-type="dijit.form.Button"
                    type="button" > <%--button type so it doesn't submit the form--%>
              <%=resMgr.getText("common.search",TradePortalConstants.TEXT_BUNDLE)%>
            </button>
            <%--cquinton 11/13/2012 add hover help--%>
            <%=widgetFactory.createHoverHelp("searchButton","SearchHoverText") %>
          </span>
          <div style="clear:both;"></div>
        </div>

<% 
if(searchQueryMap!=null && !searchQueryMap.isEmpty()){
       savedSearchQueryData = (Map)searchQueryMap.get("CustomerAccessDataView");
}
  String gridHtml ="";
  if (TradePortalConstants.OWNER_GLOBAL.equals(ownershipLevel) ||
      TradePortalConstants.OWNER_BANK.equals(ownershipLevel) ) {
    gridHtml = dgFactory.createDataGrid("custAccessGrid","CustomerAccessDataGrid",null);
  }
  else { //Bank Group user
    gridHtml = dgFactory.createDataGrid("custAccessGrid","CustomerAccessBankDataGrid",null);
  }
%>

        <%= gridHtml %>

      </div>

    </form>

  </div>
</div>

<jsp:include page="/common/Footer.jsp">
  <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
  <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>
<%@ include file="/common/GetSavedSearchQueryData.frag" %>
<script>
  function filterCorpCustomerOnEnter(fieldID){
      require(["dojo/on","dijit/registry"],function(on, registry) {
          on(registry.byId(fieldID), "keypress", function(event) {
              if (event && event.keyCode == 13) {
                  dojo.stopEvent(event);
                  searchCorpCustomer();
              }
          });
      });   <%-- end require --%>
  }<%-- end of filterCorpCustomerOnEnter() --%>

  function searchCorpCustomer() {
          require(["dojo/dom","t360/datagrid","dijit/registry"],
            function(dom,t360grid,registry){
            var searchParms="";
            <%
              if (TradePortalConstants.OWNER_GLOBAL.equals(ownershipLevel) ||
                  TradePortalConstants.OWNER_BANK.equals(ownershipLevel)) {
            %>
             var orgSelect = registry.byId("BankOid");
               var orgOid = orgSelect.get('value');
                    if ( orgOid && orgOid.length > 0 ) {
                      searchParms += "org="+orgOid;
                    }
            <%
              }
            %>
              var subName = dom.byId("CustomerName").value;
                if ( subName && subName.length > 0 ) {
                      if ( searchParms && searchParms.length > 0 ) {
                      searchParms += "&";
                 }
                      searchParms += "name="+subName;
                  }
                   
                 t360grid.searchDataGrid("custAccessGrid", "CustomerAccessDataView", searchParms);
                 console.log("searchParms="+searchParms);
                  
          });
  }


  require(["dijit/registry", "dojo/on", "t360/common", "t360/datagrid", "dojo/dom", "dojo/ready"],
      function(registry, on, common, t360grid, dom, ready) {
    
    <%--get grid layout from the data grid factory--%>
<%
  String gridLayout = null;
  if (TradePortalConstants.OWNER_GLOBAL.equals(ownershipLevel) ||
     TradePortalConstants.OWNER_BANK.equals(ownershipLevel) ) {
    gridLayout = dgFactory.createGridLayout("CustomerAccessDataGrid");
  } 
  else { //for Bank Group users, the following grid does not include Bank Group column
         //maybe not named real well...
    gridLayout = dgFactory.createGridLayout("CustomerAccessBankDataGrid");
  }
%>
    var gridLayout = <%= gridLayout %>;

    <%--no initial search parms--%>
    var initSearchParms = "";
    var custAccessGrid ="";
    <%--create the grid--%>
    var viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("CustomerAccessDataView",userSession.getSecretKey())%>";  <%--Rel9.2 IR T36000032596 --%>
   <%--cquinton 9/12/2012 Rel portal refresh ir#t36000004008 remove default sort on column 1.
       if nothing given createDataGrid will sort on first non-hidden column--%>
   custAccessGrid =t360grid.createDataGrid("custAccessGrid", viewName,
                              gridLayout, initSearchParms);
 

    ready(function() {
      <%--register event handlers--%>
      registry.byId("searchButton").on("click", function() {
        var searchParms = "";
        <%-- get the value from the org and name fields --%>
<%
  if (TradePortalConstants.OWNER_GLOBAL.equals(ownershipLevel) ||
      TradePortalConstants.OWNER_BANK.equals(ownershipLevel)) {
%>
        var orgSelect = registry.byId("BankOid");
        var orgOid = orgSelect.get('value');
        if ( orgOid && orgOid.length > 0 ) {
          searchParms += "org="+orgOid;
        }
<%
  }
%>
        var subName = dom.byId("CustomerName").value;
        if ( subName && subName.length > 0 ) {
          if ( searchParms && searchParms.length > 0 ) {
            searchParms += "&";
          }
          searchParms += "name="+subName;
        }
       
        t360grid.searchDataGrid("custAccessGrid", "CustomerAccessDataView",
          searchParms);
       
      });
      registry.byId("CustomerAccess_AccessSelectedCustomer").on("click", function() {
        <%--access the selected customer--%>
        var formName = 'CustomerAccessForm';
        var buttonName = '<%=TradePortalConstants.BUTTON_SELECT%>';

        <%--get array of rowkeys from the grid--%>
        var rowKeys = t360grid.getSelectedGridRowKeys("custAccessGrid");

        <%--submit the form with selected rowkey--%>
        common.submitFormWithParms(formName, buttonName, "selection", rowKeys[0]);
      });
    });
    
  });


</script>

<%--cquinton 10/8/2012 include gridShowCounts --%>
<jsp:include page="/common/gridShowCountFooter.jsp">
  <jsp:param name="gridId" value="custAccessGrid" />
</jsp:include>

</body>
</html>

<%
   // Finally, reset the cached document to eliminate carryover of
   // information to the next visit of this page.
   formMgr.storeInDocCache("default.doc", new DocumentHandler());
%>
