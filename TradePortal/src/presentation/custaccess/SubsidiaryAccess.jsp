<%--
**********************************************************************************
                              Subsidiary Access

  Description:
     This page is used to allow authorized Corporate Organization user to select 
     a child organization in order to act on its behalf.

**********************************************************************************
--%>

<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*, com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*, com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*, com.ams.tradeportal.html.*,
                 com.ams.tradeportal.busobj.webbean.*,java.util.*" %>

<jsp:useBean id="beanMgr"     class="com.amsinc.ecsg.web.BeanManager"            scope="session"></jsp:useBean>
<jsp:useBean id="formMgr"     class="com.amsinc.ecsg.web.FormManager"            scope="session"></jsp:useBean>
<jsp:useBean id="resMgr"      class="com.amsinc.ecsg.util.ResourceManager"       scope="session"></jsp:useBean>
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>

<%-- ************** Data retrieval page setup begins here ****************  --%>

<%

  Debug.debug("***START***********SUBSIDIARY ACCESS***************START***");

  DocumentHandler   xmlDoc              = null;
  Hashtable         secureParms         = new Hashtable();

  WidgetFactory widgetFactory = new WidgetFactory(resMgr);
  DataGridFactory dgFactory = new DataGridFactory(resMgr, userSession, formMgr, response);

   // If we're returning from acting on behalf of a corporate customer, go ahead and 
   // reset all the original admin user's attributes
   if ((request.getParameter("logout") != null) && 
       (request.getParameter("logout").equals(TradePortalConstants.INDICATOR_YES))) {
      userSession.restoreSession();

      // Remove all corporate org related attributes that may have been set during the
      // course of customer access by the admin user

      //cquinton 1/18/2013 remove close action behavior
      //instead just clear the page flow
      userSession.clearPageFlow();

      session.removeAttribute("currentMailOption");
      session.removeAttribute("currentOrgOption");
      session.removeAttribute("mailMessageOid");
      session.removeAttribute("prevStepAction");
      session.removeAttribute("currentFolder");
      session.removeAttribute("historyOrg");
      session.removeAttribute("startPage");
      session.removeAttribute("workflow");
      session.removeAttribute("refdata");
      session.removeAttribute("org");

      // Give informational message that the user is now back to his/her own organization
      MediatorServices mediatorServices = new MediatorServices();
      mediatorServices.getErrorManager().setLocaleName(resMgr.getResourceLocale());
      mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                                                    TradePortalConstants.SUBSIDIARY_ACCESS_END,
                                                    userSession.getOrganizationName());
      mediatorServices.addErrorInfo();

      xmlDoc = formMgr.getFromDocCache();
      xmlDoc.setComponent("/Error",         mediatorServices.getErrorDoc());

      formMgr.storeInDocCache("default.doc", xmlDoc);

   }


   // Set current primary navigation
   userSession.setCurrentPrimaryNavigation("NavigationBar.SubsidiaryAccess");
   Map searchQueryMap =  userSession.getSavedSearchQueryData();
   Map savedSearchQueryData =null; 
   if(searchQueryMap!=null && !searchQueryMap.isEmpty()){
		 savedSearchQueryData = (Map)searchQueryMap.get("SubsidiaryAccessDataView");
	}
   String searchNav = "subsAccess";
%>

<jsp:include page="/common/Header.jsp">
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>

<%-- Vshah - 08/02/2012 - Rel8.1 - Portal-refresh --%>
<%--  including TransactionCleanup.jsp to remove lock on instrument which has been placed by this user... --%>
<%--  this is required because (with Portal-refresh) user can navigate anywhere from MenuBar when on Instrument(detail) page. --%>
<jsp:include page="/transactions/TransactionCleanup.jsp"></jsp:include>
<jsp:include page="/common/SearchCriteriaCleanup.jsp">
<jsp:param name="searchCriteriaSection" value="<%=searchNav%>"/>
</jsp:include>
<div class="pageMainNoSidebar">
  <div class="pageContent">

    <jsp:include page="/common/PageHeader.jsp">
      <jsp:param name="titleKey" value="SubsidiaryAccess.Title" />
      <jsp:param name="helpUrl"  value="/customer/subsidiary_search_area.htm" />
    </jsp:include>

    <form name="SubsidaryAccessForm" method="POST" action="<%= formMgr.getSubmitAction(response) %>">

      <%= formMgr.getFormInstanceAsInputField("SubsidiaryAccessForm", secureParms) %>

      <input type=hidden value="" name=buttonName>

      <jsp:include page="/common/ErrorSection.jsp" />

      <div class="formContentNoSidebar">

       <div class="searchHeader">
          <div class="searchHeaderActions">
            <%--cquinton 10/8/2012 include gridShowCounts --%>
            <jsp:include page="/common/gridShowCount.jsp">
              <jsp:param name="gridId" value="subAccessGrid" />
            </jsp:include>
          </div>
          <div style="clear:both"></div>
        </div>

        <div class="searchDivider"></div>
	
        <div class="searchDetail">
          <span class="searchCriteria">
            <%=widgetFactory.createSearchTextField("SubsidiaryName","SubsidiaryAccess.criteria.SubsidiaryName","35")%>
          </span>
          <span class="searchActions">
            <button id="searchButton" data-dojo-type="dijit.form.Button"
                    type="button" > <%--button type so it doesn't submit the form--%>
              <%=resMgr.getText("common.searchButton",TradePortalConstants.TEXT_BUNDLE)%>
            </button>
            <%--cquinton 12/04/2012 add hover help--%>
            <%=widgetFactory.createHoverHelp("searchButton","SearchHoverText") %>

          </span>
          <div style="clear:both;"></div>
        </div>

<%
  String gridHtml = dgFactory.createDataGrid("subAccessGrid","SubsidiaryAccessDataGrid",null);
%>
        <%= gridHtml %>

      </div>

    </form>

  </div
</div>

<jsp:include page="/common/Footer.jsp">
  <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
  <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>
<%@ include file="/common/GetSavedSearchQueryData.frag" %>

<script type="text/javascript">

  require(["dijit/registry", "dojo/on", "t360/common", "t360/datagrid", "dojo/dom", "dojo/ready"],
      function(registry, on, common, t360grid, dom, ready) {
    
    <%--get grid layout from the data grid factory--%>
<%
  String gridLayout = dgFactory.createGridLayout("SubsidiaryAccessDataGrid");
%>
    var gridLayout = <%= gridLayout %>;

    <%--no initial search parms--%>
    var initSearchParms = "";
    var viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("SubsidiaryAccessDataView",userSession.getSecretKey())%>";  <%--Rel9.2 IR T36000032596 --%>
    <%--create the grid--%>
    var subAccessGrid = 
      t360grid.createDataGrid("subAccessGrid", viewName,
                              gridLayout, initSearchParms);

    ready(function() {
      <%--register event handlers--%>
      registry.byId("searchButton").on("click", function() {
        var searchParms = "";
        <%-- get the value of the name field --%>
        var subName = dom.byId("SubsidiaryName").value;
        if ( subName && subName.length > 0 ) {
          searchParms += "name="+subName;
        }
        t360grid.searchDataGrid("subAccessGrid", "SubsidiaryAccessDataView",
          searchParms);
      });
      registry.byId("SubsidiaryAccess_AccessSelectedSubsidiary").on("click", function() {
        <%--access the selected subsidiary--%>
        var formName = 'SubsidaryAccessForm';
        var buttonName = '<%=TradePortalConstants.BUTTON_SELECT%>';

        <%--get array of rowkeys from the grid--%>
        var rowKeys = t360grid.getSelectedGridRowKeys("subAccessGrid");

        <%--submit the form with selected rowkey--%>
        common.submitFormWithParms(formName, buttonName, "selection", rowKeys[0]);
      });
    });
  });


</script>

<%--cquinton 10/8/2012 include gridShowCounts --%>
<jsp:include page="/common/gridShowCountFooter.jsp">
  <jsp:param name="gridId" value="subAccessGrid" />
</jsp:include>

</body>
</html>

<%
   // Finally, reset the cached document to eliminate carryover of
   // information to the next visit of this page.
   formMgr.storeInDocCache("default.doc", new DocumentHandler());
%>
