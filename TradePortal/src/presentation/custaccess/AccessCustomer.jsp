<%--
 *
 *     Description: When an admin user tries to enter Customer Access or a non-admin user
 *                  tries to enter Subsidiary Access, this page sets up the new user session 
 *                  object (and save the old one) and then forward to TradePortalHome.jsp. 
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
              
<%@ page import="com.amsinc.ecsg.frame.*, com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*, com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*, com.ams.tradeportal.html.*,
                 com.ams.tradeportal.busobj.webbean.*,java.util.*,java.beans.Beans,com.businessobjects.rebean.wi.*,
				 com.crystaldecisions.sdk.framework.*,
                 com.crystaldecisions.sdk.exception.*" %>                 
                 
                 
<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" 
   scope="session">
</jsp:useBean>



<%
   MediatorServices   mediatorServices                 = null;
   DocumentHandler    securityProfilesDoc              = null;
   DocumentHandler    securityProfileDoc               = null;
   DocumentHandler    corporateOrgsDoc                 = null;
   DocumentHandler    corporateOrgDoc                  = null;
   DocumentHandler    xmlDoc                           = null;
   QueryListView      queryListView                    = null;
   Enumeration        attributeNames                   = null;
   Vector             securityProfileList              = null;
   Vector             corporateOrgList                 = null;
   String             customerAccessSecurityProfileOid = null;
   String             selectedCustomerOid              = null;
   String             baseCurrencyCode                 = null;
   String             selectedBankOid                  = null;
   String             attributeName                    = null;
   String             filterText                       = null;
   String 		    physicalPage				 = null;

   
     boolean isThereAReportingSession = userSession.getReportLoginInd();
     
     System.out.println("AccessCustomer: isThereAReportingSession = "+ isThereAReportingSession);

      // Only go in here if there is a reporting session to clean up
     if(isThereAReportingSession)
      {
			
		try
		{	
				IEnterpriseSession enterpriseSession;
			
			enterpriseSession =(IEnterpriseSession) session.getAttribute("CE_ENTERPRISESESSION");
			
			session.removeAttribute("CE_ENTERPRISESESSION");
				
			if(enterpriseSession != null)
			{
				enterpriseSession.logoff();
					enterpriseSession = null;

				System.out.println("Successfully closed BO XI Session in Access Customer");
			
			}

		}
		catch(Exception e)
		{
			System.out.println("Exception removing BO XI session - AccessCustomer");
			e.printStackTrace();

			userSession.setReportLoginInd(false);
		}




      }




   selectedCustomerOid = request.getParameter("selection");
   selectedBankOid     = request.getParameter("AccessBankOid");
   filterText          = request.getParameter("AccessFilterText");

   // 02-15-2013 jgadela, CQ T36000011752, when we do not select ant customer , selectedCustomerOid is having 'undifined', hence
   //checked with that val
   if (selectedCustomerOid == null || "".equals(selectedCustomerOid) || "undefined".equals(selectedCustomerOid) )
   {
      boolean subsidiaryAccess = userSession.getOwnershipLevel().equals(TradePortalConstants.OWNER_CORPORATE);
      xmlDoc = formMgr.getFromDocCache();

      mediatorServices = new MediatorServices();
      mediatorServices.getErrorManager().setLocaleName(resMgr.getResourceLocale());
      if (subsidiaryAccess) 
      {
         mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                                                    TradePortalConstants.NO_SUBSIDIARY_SELECTED);
      }
      else
      {
         mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                                                    TradePortalConstants.NO_CUSTOMER_SELECTED);
      }
      mediatorServices.addErrorInfo();

      xmlDoc.setComponent("/Error",         mediatorServices.getErrorDoc());
      xmlDoc.setAttribute("/In/HasErrors",  TradePortalConstants.INDICATOR_YES);
      xmlDoc.setAttribute("/In/BankOid",    selectedBankOid);
      xmlDoc.setAttribute("/In/FilterText", filterText);

      formMgr.storeInDocCache("default.doc", xmlDoc);
      if (subsidiaryAccess)
      {
         physicalPage = NavigationManager.getNavMan().getPhysicalPage("SubsidiaryAccess", request);        
      }
      else 
      {
         physicalPage = NavigationManager.getNavMan().getPhysicalPage("CustomerAccess", request);
      }

%>
      <jsp:forward page='<%= physicalPage %>' />
<%
   }
   else
   {
		//T36000015028 Start
		//Check if the logged-in user is in Customer access.
		// if so, return an error.
		boolean inCustAccess = userSession.hasSavedUserSession();
		if ( inCustAccess ) {
		 %>
			<jsp:forward page='/AutoReroute.jsp'></jsp:forward>
		<%
		}else{
		//End
      selectedCustomerOid = EncryptDecrypt.decryptStringUsingTripleDes(selectedCustomerOid,userSession.getSecretKey());

      customerAccessSecurityProfileOid = userSession.getCustomerAccessSecurityProfileOid();

      userSession.storeSavedUserSession();

      try
      {
         queryListView = (QueryListView) EJBObjectFactory.createClientEJB(formMgr.getServerLocation(), "QueryListView"); 

         // This query is used for retrieving the necessary corporate organization session info
         // for the organization in which a user will act on behalf of
         String sqlQuery = "select a.name, b.branding_directory, b.brand_button_ind, a.a_client_bank_oid, a.a_bank_org_group_oid,"
         +" a.base_currency_code, a.allow_auto_lc_create, a.allow_manual_po_entry, a.timeout_auto_save_ind, a.doc_prep_ind, a.allow_auto_atp_create, a.allow_atp_manual_po_entry, a.cust_is_not_intg_tps"
         +" from corporate_org a, bank_organization_group b"
         +" where a.organization_oid = ? and a.activation_status = ? and b.organization_oid = a.a_bank_org_group_oid";
	

         queryListView.setSQL(sqlQuery.toString(), new Object[]{selectedCustomerOid,TradePortalConstants.ACTIVE});
         queryListView.getRecords();

         corporateOrgsDoc = queryListView.getXmlResultSet();

         // This query is used to retrieve the security profile rights of the user's
         // customer/subsidiary access security profile
         sqlQuery = "select security_rights from security_profile where security_profile_oid =? ";

         queryListView.setSQL(sqlQuery.toString(), new Object[]{customerAccessSecurityProfileOid});
         queryListView.getRecords();

         securityProfilesDoc = queryListView.getXmlResultSet();
      }
      catch (Exception e)
      {
         e.printStackTrace();
      }
      finally
      {
         try
         {
            if (queryListView != null)
            {
               queryListView.remove();
            }
         }
         catch (Exception e)
         {
            System.out.println("Error removing querylistview in AccessCustomer.jsp!");
         }
      }

      // Note: There should only be one record returned from this query
      corporateOrgList    = corporateOrgsDoc.getFragments("/ResultSetRecord");
      securityProfileList = securityProfilesDoc.getFragments("/ResultSetRecord");

      corporateOrgDoc    = (DocumentHandler) corporateOrgList.elementAt(0);
      securityProfileDoc = (DocumentHandler) securityProfileList.elementAt(0);

      baseCurrencyCode  = corporateOrgDoc.getAttribute("/BASE_CURRENCY_CODE");
      userSession.setOrganizationName(corporateOrgDoc.getAttribute("/NAME"));
      userSession.setBrandingDirectory(corporateOrgDoc.getAttribute("/BRANDING_DIRECTORY"));
      userSession.setBrandButtonInd(TradePortalConstants.INDICATOR_YES.equals(corporateOrgDoc.getAttribute("/BRAND_BUTTON_IND")));
      userSession.setClientBankOid(corporateOrgDoc.getAttribute("/A_CLIENT_BANK_OID"));
      userSession.setBogOid(corporateOrgDoc.getAttribute("/A_BANK_ORG_GROUP_OID"));
      userSession.setOrgAutoLCCreateIndicator(corporateOrgDoc.getAttribute("/ALLOW_AUTO_LC_CREATE"));
      userSession.setOrgManualPOIndicator(corporateOrgDoc.getAttribute("/ALLOW_MANUAL_PO_ENTRY"));
      userSession.setBaseCurrencyCode(baseCurrencyCode);
      userSession.setOwnerOrgOid(selectedCustomerOid);
      userSession.setDefaultWipView(TradePortalConstants.WIP_VIEW_MY_ORG);
      userSession.setOwnershipLevel(TradePortalConstants.OWNER_CORPORATE);
      userSession.setCustomerAccessIndicator(TradePortalConstants.INDICATOR_YES);
      userSession.setSecurityType(TradePortalConstants.NON_ADMIN);
      userSession.setSecurityRights(securityProfileDoc.getAttribute("/SECURITY_RIGHTS"));
      userSession.setTimeoutAutoSaveInd(corporateOrgDoc.getAttribute("/TIMEOUT_AUTO_SAVE_IND"));
      userSession.setCorpCanAccessDocPrep(corporateOrgDoc.getAttribute("/DOC_PREP_IND"));
      userSession.setCustNotIntgTPS(TradePortalConstants.INDICATOR_YES.equals(corporateOrgDoc.getAttribute("/CUST_IS_NOT_INTG_TPS")));
      
      //TLE - 07/29/07 - CR-375 - Add Begin 
      userSession.setOrgAutoATPCreateIndicator(corporateOrgDoc.getAttribute("/ALLOW_AUTO_ATP_CREATE"));
      userSession.setOrgATPManualPOIndicator(corporateOrgDoc.getAttribute("/ALLOW_ATP_MANUAL_PO_ENTRY"));
      //TLE - 07/29/07 - CR-375 - Add End 

      // Remove all admin listview state objects from the session
      attributeNames = session.getAttributeNames();

      while (attributeNames.hasMoreElements())
      {
         attributeName = (String) attributeNames.nextElement();

         if (attributeName.indexOf(".xml") != -1)
         {
            session.removeAttribute(attributeName);
         }
      }

      session.removeAttribute("transStatusType");
      session.removeAttribute("workflow");
      session.removeAttribute("org");
      session.removeAttribute("historyOrg");

      //cquinton 1/18/2013 remove old close action behavior
      // replace with clearing page flow
      userSession.clearPageFlow();

      session.removeAttribute("instrStatusType");      
      session.removeAttribute("currentMailOption");
      session.removeAttribute("currentOrgOption");

      physicalPage = NavigationManager.getNavMan().getPhysicalPage("TradePortalHome", request);

      // If a non-admin user is accessing a subsidiary, give informational message that 
      // the user is acting on behalf of another organization
      if (userSession.getSavedUserSessionSecurityType().equals(TradePortalConstants.NON_ADMIN)) 
      {
         mediatorServices = new MediatorServices();
         mediatorServices.getErrorManager().setLocaleName(resMgr.getResourceLocale());
         mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                                                    TradePortalConstants.SUBSIDIARY_ACCESS_BEGIN,
                                                    corporateOrgDoc.getAttribute("/NAME"));
         mediatorServices.addErrorInfo();

         xmlDoc = formMgr.getFromDocCache();
         xmlDoc.setComponent("/Error",         mediatorServices.getErrorDoc());

         formMgr.storeInDocCache("default.doc", xmlDoc);
      }
     

%>
      <jsp:forward page='<%= physicalPage %>' />
<%
	   }
   }
%>
