<%--
 *
 *     Copyright   2003                       
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.web.*,com.amsinc.ecsg.frame.*,
		 com.amsinc.ecsg.util.*,com.ams.tradeportal.common.*, java.util.*,
		 com.ams.tradeportal.html.*" %>

<%--
 // The user is forwarded to this page when the system is attempting to load
 // a page which takes extra time to display
--%>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager"      scope="session"></jsp:useBean>
<jsp:useBean id="resMgr"  class="com.amsinc.ecsg.util.ResourceManager" scope="session"></jsp:useBean>
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>



<%     
      String version = resMgr.getText("version", "TradePortalVersion");
  if (version.equals("version")) {
    version = resMgr.getText("Footer.UnknownVersion", 
                             TradePortalConstants.TEXT_BUNDLE);
  } 
//M Eerupula 03/21/2013 Rel 8.2 T3600001495  version is encrypted consistently using Base64 encoding to fix the URL difference for cache busting; means the version is same on all included pages 
	  //version = EncryptDecrypt.bytesToBase64String(version.getBytes()); 
	//RPasupulati geting first 4 char's from encripted version String IR no T36000017311 Starts.
	version = EncryptDecrypt.encryptStringUsingTripleDes(version, userSession.getSecretKey());   
	version = version.substring(0, 4);
	 //RPasupulati geting first 4 char's from encripted version String IR no T36000017311 ENDs.
      String brandingDirectory = userSession.getBrandingDirectory();
      StringBuffer parameters = new StringBuffer();
      for (Enumeration enumer=request.getParameterNames();enumer.hasMoreElements();) 
      {
         String parmName = (String)enumer.nextElement();
         String [] parmValues = request.getParameterValues(parmName);

	 // Do not include the navigation id from the request object as a parameter;a new navigation id
	 // will be created for the next page
         for (int i=0; i<parmValues.length; i++)
         {
           if (!parmName.equals("ni"))
	   {
	     parameters.append("&");
	     parameters.append(parmName + "=");
	     parameters.append(parmValues[i]);	
           }
         }	
      }
      if (request.getAttribute(TradePortalConstants.INCLUDE_IN_PARAMETER) != null) {
    	  Map map = (Map)request.getAttribute(TradePortalConstants.INCLUDE_IN_PARAMETER);
    	  Iterator it = map.keySet().iterator();
    	  String key = null;
    	  while (it.hasNext()) {
    		 key  = (String)it.next(); 
    	     parameters.append("&").append(key).append("=").append(map.get(key));
    	  }
      }

      String nextLogicalPage = (String)request.getAttribute("nextLogicalPage");
      String nextPage = NavigationManager.getNavMan().getPhysicalPage(nextLogicalPage, request);  
      formMgr.setCurrPage(nextLogicalPage);	
%>

<jsp:include page="/common/Header.jsp">
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_NO%>" />
   <jsp:param name="includeErrorSectionFlag"  value="<%=TradePortalConstants.INDICATOR_NO%>" />
   <jsp:param name="brandingDirectory" value="<%=brandingDirectory%>" />  
   <jsp:param name="includeTimeoutForward" value="false" />
</jsp:include>

<div class="pageContent">
<div class="secondaryNav">
                    <jsp:include page="/common/PageHeader.jsp">
                    <jsp:param name="titleKey" value="IntermediatePage.Title"/>
                     </jsp:include>
</div>

<div class="formContentNoSidebar">
&nbsp;
  <br>&nbsp;<br>
 <div class="formItem">
  <br>
  <div style="margin-left:300px;">     		
  		<span id="loading" class="loadingImageClass"><%= resMgr.getText("IntermediatePage.Message1",TradePortalConstants.TEXT_BUNDLE) %></span>
  </div>
  <br>
<%
      // This will cause this page to display and then automatically 
      // forward to the next page
      String addlParms = new String(parameters);
      addlParms = addlParms.replace(' ','+');
      addlParms = addlParms.replaceAll("[\n\r]+", "");
      String link = formMgr.getUrlForLogicalPage(nextPage, addlParms, response);
      //response.setHeader("Refresh", "0;URL=".concat(link));
      session.removeAttribute("isFromTransDetailPage");
%> 
</div>
</div>
</div>
 
<script src="<%=userSession.getdojoJsPath()%>/dojoconfig.js?<%= version %>"></script>
<script src='<%=userSession.getdojoJsPath()%>/dojo/dojo.js?<%= version %>' data-dojo-config="async: 1" ></script>
<script>
  require(["dojo/domReady!"], function(){
	   <%-- location.href = "<%=link.toString()%>"; --%>
	   var newLink = '<%=link.toString()%>';
       	if (isActive =='Y') {
       		if (newLink != '' && newLink.indexOf("javascript:") == -1) {
	       		var cTime = (new Date()).getTime();
		        newLink = newLink + "&cTime=" + cTime;
		        newLink = newLink + "&prevPage=" + context;
       		}
	    }
	   window.location = newLink;
  
});
 
</script>  
</body>
</html>
