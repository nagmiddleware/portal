<%--
*******************************************************************************
                        Invoice Detail

  Description:
     This page is used to display the detail for an invoice.
  Although it is modeled after the standard ref data pages, this is a readonly
  page.  It is only used to display data and can never update anything.

*******************************************************************************
--%>

<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 
--%>
<%@ page import="java.util.*,com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.html.*,com.ams.tradeportal.busobj.*" %>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" 
   scope="session">
</jsp:useBean>


<%-- ************** Data retrieval page setup begins here ****************  --%>

<%
  boolean isReadOnly = true;

  String oid = "";

  boolean showDelete = true;
  boolean showSave   = false;

  boolean getDataFromDoc = false;
  boolean retrieveFromDB = false;

  DocumentHandler doc;

  POUtility poUtility = new POUtility();
  Hashtable fields = null;
  
  
  String invoiceNumber = null;
  String dateAndTimeReceived = null;
  String issueDate = null;
  String dueDate = null;
  String totalAmount= null;
  String netAmount = null;
  String amountPaid = null;
  String balanceRemaining = null;
  String financeStatus = null;
  String financePRCNTG = null;
  String paymentStatus = null;
  String disputed = null;
  String invoiceStatus = null;
  String buyerInfo = null;
  String billToInfo = null;
  String buyerIdentifier = null;
  String billToIdentifier = null;
  String            helpSensitiveLink      = null;
  String poID = null;
  String invoiceRefId = null;
  String poIssueDate = null;
  String totalInvoiceLineAmount = null;
  String currency = null;
  String useCurrency = null;	//IAZ 5.2 07/30/09 IR RRUJ050542624 Add
  String current2ndNav      = null;
  boolean           canViewRecMatchNotices = false;
  boolean           canViewRecInstruments  = false;
  
  DocumentHandler invoiceGoodsResult = null;

  InvoiceWebBean_Base invoice = beanMgr.createBean(InvoiceWebBean_Base.class,  "Invoice");
 
  // Get the webbeans for the login in user and his security profile.  Set the
  // readonly access appropriately.
  String loginLocale = userSession.getUserLocale();
String corpOrgOid = userSession.getOwnerOrgOid();
boolean ignoreDefaultPanelValue = false;
  current2ndNav = request.getParameter("current2ndNav");
  // Get the document from the cache.  We'll use it to repopulate the screen 
  // if the user was entering data with errors.
  doc = formMgr.getFromDocCache();

  Debug.debug("doc from cache is " + doc.toString());
  helpSensitiveLink = OnlineHelp.createContextSensitiveLink("customer/trans_recvbl_mgmt_invoice_detail.htm",  resMgr, userSession);

  if (doc.getDocumentNode("/In/Invoice") == null ) {
     // The document does not exist in the cache.  We are entering the page from
     // the listview page (meaning we're opening an existing item)

  oid =StringFunction.xssCharsToHtml(request.getParameter("invoice_oid"));
     if (oid != null) {
        oid = EncryptDecrypt.decryptStringUsingTripleDes(oid, userSession.getSecretKey());
     }
     if (oid == null) {
       oid = "0";
     } else {
       // We have a good oid so we can retrieve.
       getDataFromDoc = false;
       retrieveFromDB = true;
     }

  } else {
     // The doc does exist so check for errors.  If highest severity < WARNING,
     // its a good update.

     String maxError = doc.getAttribute("/Error/maxerrorseverity");

     if (maxError.compareTo(
                  String.valueOf(ErrorManager.WARNING_ERROR_SEVERITY)) < 0) {
        // We've returned from a delete that was successful
        // We shouldn't be here.  Forward to the ReceivablesManagementHome page.
        formMgr.setCurrPage("ReceivablesManagementHome");
        String physicalPage = NavigationManager.getNavMan().getPhysicalPage("ReceivablesManagementHome", request);

%>
        <jsp:forward page='<%=physicalPage%>' />
<%
        return;
     } else {
        // We've returned from a save/update/delete but have errors.
        // We will display data from the cache.  Determine if we're
        // in insertMode by looking for a '0' oid in the input section
        // of the doc.

        retrieveFromDB = false;
        getDataFromDoc = true;

        // Use oid from input doc.
        oid = doc.getAttribute("/In/Invoice/invoice_oid");
     }
  }

  if (retrieveFromDB) {
     // Attempt to retrieve the item.  It should always work.  Still, we'll
     // check for a blank oid -- this indicates record not found.  Display error.

     getDataFromDoc = false;

     invoice.setAttribute("invoice_oid", oid);
     invoice.getDataFromAppServer();
     
     if (invoice.getAttribute("invoice_oid").equals("")) {
       oid = "0";
     }
     
     StringBuilder invoiceGoodsSQL = new StringBuilder();
     invoiceGoodsSQL.append("select po_reference_id, po_issue_datetime, line_items_total_amount,")
	.append(" seller_information_label1, seller_information_label2, seller_information_label3, seller_information_label4, seller_information_label5, seller_information_label6, seller_information_label7, seller_information_label8, seller_information_label9, seller_information_label10,")
	.append(" seller_information1, seller_information2, seller_information3, seller_information4, seller_information5, seller_information6, seller_information7, seller_information8, seller_information9, seller_information10")
	.append(" from invoice_goods")
	.append(" where p_invoice_oid = ? ");
     invoiceGoodsResult = DatabaseQueryBean.getXmlResultSet(invoiceGoodsSQL.toString(), false, new Object[]{oid});
  } 
  
  currency = invoice.getAttribute("currency_code");
  //IAZ 5.2 07/30/09 IR RRUJ050542624 Begin
  if (!StringFunction.isBlank(currency))
  	useCurrency = currency;
  else useCurrency = "2";
  //IAZ 5.2 07/30/09 IR RRUJ050542624 End
  
  //helpSensitiveLink = OnlineHelp.createContextSensitiveLink("customer/po_line_detail.htm",  resMgr, userSession);
  //IR 20282 BEGIN
  if ( "true".equals( request.getParameter("returning") ) ) {
    userSession.pageBack(); 
  }
  else {
   //userSession.addPage("goToPaymentMatchResponse", request);
    userSession.addPage("goToInvoiceDetail", request);
  }
  
  SessionWebBean.PageFlowRef backPage = userSession.peekPageBack();
  String closeAction = backPage.getLinkAction();
  String closeParms = backPage.getLinkParametersString() + "&returning=true";
  String closeLink = formMgr.getLinkAsUrl(closeAction,closeParms,response);
  //IR 20282 END
   String fromPayables = (String) session.getAttribute("fromPayables");
%>

<%-- ********************* HTML for page begins here *********************  --%>



<%-- Body tag included as part of common header --%>
<% WidgetFactory widgetFactory = new WidgetFactory(resMgr); %>
		
<jsp:include page="/common/Header.jsp">
  <jsp:param name="includeNavigationBarFlag" 
             value="<%=TradePortalConstants.INDICATOR_YES%>" />
  <jsp:param name="includeErrorSectionFlag"  
             value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>
		
<div class="pageMain">
  <div class="pageContent">
    <div class="secondaryNav">
      <div class="secondaryNavTitle">
      <% if("Y".equals(fromPayables)){ %>
        <%=resMgr.getText("SecondaryNavigation.Payables",
             TradePortalConstants.TEXT_BUNDLE)%> 
       <%}else{ %>
        <%=resMgr.getText("SecondaryNavigation.Receivables",
             TradePortalConstants.TEXT_BUNDLE)%> 
       <%} %>
      </div>
   
<%
     String matchingSelected   = TradePortalConstants.INDICATOR_NO;
  	 String invoicesSelected = TradePortalConstants.INDICATOR_YES;
     String inquiriesSelected = TradePortalConstants.INDICATOR_NO;
    
     String navLinkParms = "";
     if(!"Y".equals(fromPayables)){
     if (TradePortalConstants.NAV__RECEIVABLE_MATCH_NOTICES.equals(current2ndNav)) {
    	 matchingSelected = TradePortalConstants.INDICATOR_YES;
     }else if (TradePortalConstants.NAV__INVOICES.equals(current2ndNav)) {
    	 invoicesSelected = TradePortalConstants.INDICATOR_YES;
     }
     else if(TradePortalConstants.NAV__INQUIRIES.equals(current2ndNav)){
       inquiriesSelected = TradePortalConstants.INDICATOR_YES;
     }
      navLinkParms = "current2ndNav=" + TradePortalConstants.NAV__RECEIVABLE_MATCH_NOTICES;
%>
      <jsp:include page="/common/NavigationLink.jsp">
        <jsp:param name="linkTextKey"  value="SecondaryNavigation.Receivables.Matching" />
        <jsp:param name="linkSelected" value="<%= matchingSelected %>" />
        <jsp:param name="action"       value="goToReceivableTransactionsHome" />
        <jsp:param name="linkParms"    value="<%= navLinkParms %>" />
        <jsp:param name="hoverText"    value="ReceivableTransaction.matchingList" />
      </jsp:include>
<%
    navLinkParms = "current2ndNav=" + TradePortalConstants.NAV__INVOICES;
%>
      <jsp:include page="/common/NavigationLink.jsp">
        <jsp:param name="linkTextKey"  value="SecondaryNavigation.Receivables.Invoices" />
        <jsp:param name="linkSelected" value="<%= invoicesSelected %>" />
        <jsp:param name="action"       value="goToReceivableTransactionsHome" />
        <jsp:param name="linkParms"    value="<%= navLinkParms %>" />
        <jsp:param name="hoverText"    value="ReceivableTransaction.invoicesList" />
      </jsp:include>
<%
  navLinkParms = "current2ndNav=" + TradePortalConstants.NAV__INQUIRIES;
%>
      <jsp:include page="/common/NavigationLink.jsp">
        <jsp:param name="linkTextKey"  value="SecondaryNavigation.Receivables.Inquiries" />
        <jsp:param name="linkSelected" value="<%= inquiriesSelected %>" />
        <jsp:param name="action"       value="goToReceivableTransactionsHome" />
        <jsp:param name="linkParms"    value="<%= navLinkParms %>" />
        <jsp:param name="hoverText"    value="ReceivableTransaction.inquiryList" />
      </jsp:include>
    
    <%}else{ 
    navLinkParms = "current2ndNav=" + TradePortalConstants.NAV__PAY_INVOICES; %>
     <jsp:include page="/common/NavigationLink.jsp">
        <jsp:param name="linkTextKey"  value="SecondaryNavigation.Payables.Invoices" />
        <jsp:param name="linkSelected" value="<%= invoicesSelected %>" />
        <jsp:param name="action"       value="goToPayableTransactionsHome" />
        <jsp:param name="linkParms"    value="<%= navLinkParms %>" />
        <jsp:param name="hoverText"    value="PayableTransaction.invoicesList" />
      </jsp:include>
    <%} %>
      <div id="secondaryNavHelp" class="secondaryNavHelp">
        <%=helpSensitiveLink %>
      </div>
	  <%=widgetFactory.createHoverHelp("secondaryNavHelp","HelpImageLinkHoverText") %>	
      <div style="clear:both;"></div>
    </div>

    <div class="subHeaderDivider"></div>
    <div class="pageSubHeader">
      <span class="pageSubHeaderItem"> 
        <span><%=resMgr.getText("InvoiceDetail.InvoiceNumber",TradePortalConstants.TEXT_BUNDLE)%></span>
        <span> 
          <% out.print("-"); %>
          <%=invoice.getAttribute("invoice_reference_id")%>
          <%out.print("-"); %> <%out.print("("); %>
<%
  invoiceStatus = invoice.getAttribute("invoice_status");
  if (invoiceStatus == null || invoiceStatus.equals("")) {
            out.print("&nbsp;");
  } 
  else {
    invoiceStatus = invoiceStatus.trim();
    StringBuffer dropdownOptions = new StringBuffer();
    dropdownOptions.append("<option value='");
    dropdownOptions.append(invoiceStatus);
    dropdownOptions.append("' selected ");
    dropdownOptions.append(">");
    dropdownOptions.append(ReferenceDataManager.getRefDataMgr().getDescr(
				      TradePortalConstants.INVOICE_STATUS, 
				       invoiceStatus, loginLocale));
    dropdownOptions.append("</option>");

    out.print(widgetFactory.createSelectField( "invoiceStatus" ,""," ", dropdownOptions.toString(), isReadOnly,false,false,"","","none"));
  }
%>
<%out.print(")"); if(TPDateTimeUtility.formatDateTime(invoice.getAttribute("activity_sequence_datetime"),TPDateTimeUtility.SHORT,loginLocale)!=null && !TPDateTimeUtility.formatDateTime(invoice.getAttribute("activity_sequence_datetime"),TPDateTimeUtility.SHORT,loginLocale).equals("")){
out.print(" - "+TPDateTimeUtility.formatDateTime(invoice.getAttribute("activity_sequence_datetime"),TPDateTimeUtility.SHORT,loginLocale));
}%>
        </span>
      </span>
      <span class="pageSubHeader-right">
        <%--cquinton 10/31/2012 ir#7015 change return link to close button when no close button present.
            note this does NOT include an icon as in the sidebar--%>
        <button data-dojo-type="dijit.form.Button" name="CloseButton" id="CloseButton" type="button" class="pageSubHeaderButton">
          <%=resMgr.getText("common.CloseText",TradePortalConstants.TEXT_BUNDLE)%>
          <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
            document.location.href =
              '<%= closeLink %>';
          </script>
        </button> 
      </span>
      <%=widgetFactory.createHoverHelp("CloseButton","InvoiceDetailCloseHoverText") %>
      <div style="clear:both;"></div>
    </div>


		<form name="InvoiceDetail" method="post" id="InvoiceDetail" data-dojo-type="dijit.form.Form"
     													 action="<%=formMgr.getSubmitAction(response)%>">
 		 <input type=hidden value="" name=buttonName>

<%
  // Store values such as the userid, his security rights, and his org in a secure
  // hashtable for the form.  The mediator needs this information.
  Hashtable secureParms = new Hashtable();
  secureParms.put("UserOid", userSession.getUserOid());

  secureParms.put("invoice_oid", oid);
%>

<%= formMgr.getFormInstanceAsInputField("InvoicesForm", secureParms) %>
<%@ include file="/transactions/fragments/LoadPanelLevelAliases.frag" %>
	<div class="formMainHeader">
  	
<%
        String link=formMgr.getLinkAsUrl("goToReceivablesManagement", response);
%>
        
	</div>
	<div class="formContentNoSidebar">
  <table>
    <tr>
      <td width="2%" nowrap>&nbsp;</td>
      <td nowrap> 
        <p> <span class="ControlLabel">
          
           <%=widgetFactory.createSubLabel("InvoiceDetail.IssueDate") %>                 
          </span>
          <br>
          <span class="ListText">
            <b><%=TPDateTimeUtility.formatDate(invoice.getAttribute("invoice_issue_datetime"),
                                          TPDateTimeUtility.SHORT,
                                          loginLocale)%></b>
          </span>
        </p>
      </td>
      <td width="2%" nowrap>&nbsp;</td>
      <td nowrap> 
        <p> <span class="ControlLabel">
        
                             <%=widgetFactory.createSubLabel("InvoiceDetail.DueDate") %> 
            </span>
          <br>
          <span class="ListText">
            <b><%=TPDateTimeUtility.formatDate(invoice.getAttribute("invoice_due_date"),
                                          TPDateTimeUtility.SHORT,
                                          loginLocale)%></b>
	      </span>
        </p>
      </td>
      <td width="2%" nowrap>&nbsp;</td>
      <td nowrap> 
        <p> <span class="ControlLabel">
        
                             <%=widgetFactory.createSubLabel("InvoiceDefinition.PaymentDate") %> 
            </span>
          <br>
          <span class="ListText">
            <b><%=TPDateTimeUtility.formatDate(invoice.getAttribute("invoice_payment_date"),
                                          TPDateTimeUtility.SHORT,
                                          loginLocale)%></b>
	      </span>
        </p>
      </td>
      <td width="2%" nowrap>&nbsp;</td>
      <td nowrap> 
        <p> <span class="ControlLabel">
        
                             <%=widgetFactory.createSubLabel("InvoiceDefinition.SupplierToSendDate") %> 
            </span>
          <br>
          <span class="ListText">
            <b><%=TPDateTimeUtility.formatDate(invoice.getAttribute("send_to_supplier_date"),
                                          TPDateTimeUtility.SHORT,
                                          loginLocale)%></b>
	      </span>
        </p>
      </td>
      <td width="2%" nowrap>&nbsp;</td>
      <td nowrap> 
        <p> <span class="ControlLabel">
          
                            <%=widgetFactory.createSubLabel("InvoiceDetail.TotalAmount") %>
          </span>
          <br>
          <span class="ListText">
            <% if (invoice.getAttribute("invoice_total_amount") != null && !invoice.getAttribute("invoice_total_amount").equals("")) { %>
             <b> <%=currency%>&nbsp;
             <%=TPCurrencyUtility.getDisplayAmount(invoice.getAttribute("invoice_total_amount"), useCurrency, loginLocale)%></b>
            <% } else { %>
              &nbsp;
            <% } %>
	  </span>
        </p>
      </td>
      <td width="2%" nowrap>&nbsp;</td>
      <td nowrap> 
        <p> <span class="ControlLabel">
          
                             <%=widgetFactory.createSubLabel("InvoiceDetail.AmountPaid") %>
          </span>
          <br>
          <span class="ListText">
            <% if (invoice.getAttribute("invoice_payment_amount") != null && !invoice.getAttribute("invoice_payment_amount").equals("")) { %>
             <b> <%=currency%>&nbsp;
              <%=TPCurrencyUtility.getDisplayAmount(invoice.getAttribute("invoice_payment_amount"), currency, loginLocale)%></b>              
            <% } else { %>
              &nbsp;
            <% } %>
	  </span>
        </p>
      </td>
      <td width="2%" nowrap>&nbsp;</td>
      <td nowrap>
        <p> <span class="ControlLabel">
        <%=widgetFactory.createSubLabel("InvoiceDetail.BalanceRemaining") %>
          </span>
          <br>
          <span class="ListText">
            <% if (invoice.getAttribute("invoice_outstanding_amount") != null && !invoice.getAttribute("invoice_outstanding_amount").equals("")) { %>
            <b> <%=currency%>&nbsp;
              <%=TPCurrencyUtility.getDisplayAmount(invoice.getAttribute("invoice_outstanding_amount"), currency, loginLocale)%></b>              
            <% } else { %>
              &nbsp;
            <% } %>
	  </span>
        </p>
      </td>
      <td width="2%" nowrap>&nbsp;</td>
      <td nowrap> 
        <p> <span class="ControlLabel">
         
                            <%=widgetFactory.createSubLabel("InvoiceDetail.FinanceStatus") %>
          </span>
          <br>
          <span class="ListText">
<%
  financeStatus = invoice.getAttribute("finance_status");
  if (financeStatus == null || financeStatus.equals("")) {
            out.print("&nbsp;");
  } else {
    financeStatus = financeStatus.trim();
    StringBuffer dropdownOptions = new StringBuffer();
    dropdownOptions.append("<option value='");
    dropdownOptions.append(financeStatus);
    dropdownOptions.append("' selected ");
    dropdownOptions.append(">");
    dropdownOptions.append(ReferenceDataManager.getRefDataMgr().getDescr(
				      TradePortalConstants.INVOICE_FINANCE_STATUS, 
				       financeStatus, loginLocale));
    dropdownOptions.append("</option>");
   %><b><%
   out.print(widgetFactory.createSelectField( "financeStatus" ,""," ", dropdownOptions.toString(), isReadOnly,false,false,"","","none")); %></b><%
  }
%>
	  </span>
        </p>
      </td>
      <td width="2%" nowrap>&nbsp;</td>
      <td nowrap>
        <p> <span class="ControlLabel">
      
                            <%=widgetFactory.createSubLabel("InvoiceDetail.FinancePRCNTG") %>
          </span>
          <br>
          <span class="ListText">
<% financePRCNTG = invoice.getAttribute("finance_percentage");
   if (financePRCNTG == null || financePRCNTG.equals("")) {
            out.print("&nbsp;");
   } else { %>
            <b><%=TPCurrencyUtility.getDisplayAmount(financePRCNTG, "2", loginLocale)%></b>
<% } %>
	  </span>
        </p>
      </td>
      <td width="2%" nowrap>&nbsp;</td>
      <td nowrap>
        <p> <span class="ControlLabel">
        
                            <%=widgetFactory.createSubLabel("InvoiceDetail.PaymentStatus") %>
          </span>
          <br>
          <span class="ListText">
<%
  paymentStatus = invoice.getAttribute("invoice_payment_status");
  if (paymentStatus == null || paymentStatus.equals("")) {
            out.print("&nbsp;");
  } else {
    paymentStatus = paymentStatus.trim();
    StringBuffer dropdownOptions = new StringBuffer();
    dropdownOptions.append("<option value='");
    dropdownOptions.append(paymentStatus);
    dropdownOptions.append("' selected ");
    dropdownOptions.append(">");
    dropdownOptions.append(ReferenceDataManager.getRefDataMgr().getDescr(
				      TradePortalConstants.INVOICE_PAYMENT_STATUS, 
				       paymentStatus, loginLocale));
    dropdownOptions.append("</option>");
    %><b>
   <% out.print(widgetFactory.createSelectField( "paymentStatus" ,""," ", dropdownOptions.toString(), isReadOnly,false,false,"","","none")); %></b><%
  }
%>
	  </span>
        </p>
      </td>
      <td width="2%" nowrap>&nbsp;</td>
      <td nowrap>
        <p> <span class="ControlLabel">
          
                            <%=widgetFactory.createSubLabel("InvoiceDetail.Disputed") %>
          </span>
          <br>
          <span class="ListText">
<b><% disputed = invoice.getAttribute("disputed_indicator");
   if (disputed == null || disputed.equals("") || disputed.equals(TradePortalConstants.INDICATOR_NO)) {
	   disputed = "NO";
	   %>
       <%= disputed %>
  <% } else { 
  	   disputed = "YES";
  %>
            <%= disputed %>
<% } %>
</b>

	  </span>
        </p>
      </td>
     <td width="2%" nowrap>&nbsp;</td>
     
    </tr>
  <%--   <tr><td>&nbsp;</td></tr> --%>
  </table>
&nbsp;
 <div class="formContentNoBorder">
					<div class="columnLeftNoBorder">
					 <%=widgetFactory.createSubsectionHeader("InvoiceDetail.BuyerInfo")%>
   
                      <div class="formItem">
         <%=invoice.getAttribute("buyer_party_identifier")%>  <br/>
           
                             <% buyerInfo = invoice.getAttribute("buyer_address_line_1") + "<br>" + 
  	      		 invoice.getAttribute("buyer_address_line_2") + "<br>" + 
  	      		 invoice.getAttribute("buyer_address_country"); %>
          <%= buyerInfo %>  
          <div style="clear: both"></div>
          </div>                  
        </div>
        <div class="columnRightNoBorder">
        <%=widgetFactory.createSubsectionHeader("InvoiceDetail.BillToInfo")%>
         <div class="formItem">
                                       <%=invoice.getAttribute("bill_party_identifier")%><br/>
                            
                            <% billToInfo = invoice.getAttribute("bill_address_line_1") + "<br>" + 
  	      		  invoice.getAttribute("bill_address_line_2") + "<br>" + 
  	      		  invoice.getAttribute("bill_address_country"); %>
          <%= billToInfo %>
           </div>                 
         </div>
         <div style="clear: both"></div>
         </div>  
          
<div class="formContentNoBorder" >                                                      
 <span class="columnLeftNoBorder" style="width: 125%;">
  <%=widgetFactory.createWideSubsectionHeader("InvoiceDetail.POInformation") %>  
  
 
  <%-- <div style="clear: both"></div> 
  <table><tr><td>&nbsp;</td></tr>
    <tr><td>&nbsp;</td></tr></table> --%>
  <div class="formItem" >     

  
<% if( invoiceGoodsResult != null ) {
     Vector listviewVector = invoiceGoodsResult.getFragments("/ResultSetRecord/");
     int size = listviewVector.size();
     for(int i= 0; i<size; i++) {
	poID = ((DocumentHandler)listviewVector.get(i)).getAttribute("/PO_REFERENCE_ID");
	poIssueDate = ((DocumentHandler)listviewVector.get(i)).getAttribute("/PO_ISSUE_DATETIME");
	totalInvoiceLineAmount = ((DocumentHandler)listviewVector.get(i)).getAttribute("/LINE_ITEMS_TOTAL_AMOUNT");
%>
<table width=70%>
    
    
    <tr width=50%>
    <td width=15%>
          <div class="spanHeader"> <%=widgetFactory.createSubLabel("InvoiceDetail.POID") %><br/></div>
         
           <b> <%=poID%></b>
          </td>
          <td width=20%>
          <div class="spanHeader"><%=widgetFactory.createSubLabel("InvoiceDetail.POIssueDate") %><br/></div>
          
        
          <b> <%=TPDateTimeUtility.formatDate(poIssueDate,
                                          TPDateTimeUtility.SHORT,
                                          loginLocale)%></b>
          </td>
          
          <td width=65%>
          <div class="spanHeader"><%=widgetFactory.createSubLabel("InvoiceDetail.TotalInvoiceLineAmount") %><br/></div>
          
            <% if (totalInvoiceLineAmount != null && !totalInvoiceLineAmount.equals("")) { %>
            <b>  <%=currency%>&nbsp;
              <%=TPCurrencyUtility.getDisplayAmount(totalInvoiceLineAmount, currency, loginLocale)%>  </b>            
            <% } %>
            </td>
            </tr>
            
          </table>
          <table width=40%>  
  <tr width=40%>
 
  <td width=15%><%=widgetFactory.createSubLabel("InvoiceDetail.Detail") %></div></td>
  </tr>
  
  
  
    
<%
	for(int j= 0; j<10; j++) {
	  String sellerLabel = ((DocumentHandler)listviewVector.get(i)).getAttribute("/SELLER_INFORMATION_LABEL" + j);
	  String sellerInformation = ((DocumentHandler)listviewVector.get(i)).getAttribute("/SELLER_INFORMATION" + j);
	  if (sellerInformation != null && !sellerInformation.equals("")) { %>
   <tr width=40%>
   
   <td width=15%>
          <b>  <%=sellerLabel%></b>&nbsp;&nbsp;&nbsp;
 </td><td width=25%>
          
            <%=sellerInformation%><br/>
        </td>  </tr>
<%	  }
	}
     }
   } %>
  
  
   
      </table>
      <br/>
  </div> 
  </span>
  </div>
  <div style="clear: both"></div> 
 
  
 <%
  DGridFactory dgFactory = new DGridFactory(resMgr, userSession, formMgr, beanMgr, response);
  String LogGridHtml = dgFactory.createDataGrid("invoiceHistoryLogGrid","InvoiceHistoryLogDataGrid",null);
  String logGridLayout = dgFactory.createGridLayout("InvoiceHistoryLogDataGrid");
  %>
   
<div class="formItem">
	<div class="GridHeader" >
	<span class="invGridHeaderTitle" style="border-top:1px solid #DBDBDB;">
	<%=resMgr.getText("UploadInvoiceSummary.TransactionLog",TradePortalConstants.TEXT_BUNDLE)%>
      </span>
	</div>
	<div style="clear: both"></div>
		 <%=LogGridHtml %>
		 </div>
	</div>
</form>
</div>


<jsp:include page="/common/Footer.jsp">
   <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>
<script type="text/javascript" src="/portal/js/datagrid.js"></script>
<script type="text/javascript">
require(["t360/OnDemandGrid", "dojo/dom-construct"], function(onDemandGrid, domConstruct){
  <%--get grid layout from the data grid factory--%>
  var logGridLayout = <%= logGridLayout %>;
  <%--set the initial search parms--%>

  <%-- MEer IR-36419 encrypt uploaded invoice oid --%>
  var init='upload_invoice_oid=<%=EncryptDecrypt.encryptStringUsingTripleDes(oid, userSession.getSecretKey())%>';

  <%--create the grid, attaching it to the div id specified in dataGrid.jsp--%>
  var viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("InvoiceHistoryLogDataView",userSession.getSecretKey())%>";  <%--Rel9.2 IR T36000032596 --%>
  var invoiceHistoryLogGrid = onDemandGrid.createOnDemandGrid("invoiceHistoryLogGrid", viewName, logGridLayout, init,1);
});
  function panelLevelAliasesFormatter(columnValues){
	  var panelLevel = columnValues[0];
	  var panelAlias="";
	  if(panelLevel != ""){
		  panelAlias = panelStore.get(panelLevel).name;
	  }
	  return panelAlias;
	  
  }

</script>
  </div> 
  <jsp:include page="/common/dGridShowCountFooter.jsp">
  <jsp:param name="gridId" value="invoiceHistoryLogGrid" />
</jsp:include>
</body>

<%
  // Finally, reset the cached document to eliminate carryover of 
  // information to the next visit of this page.
  formMgr.storeInDocCache("default.doc", new DocumentHandler());

%>

</html>
