<%--for the ajax call to format display amount--%>

<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.html.*,java.math.*,
                 java.util.ArrayList, java.util.Hashtable" %>



<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session">
</jsp:useBean>

<% 
  //parameters -
  //other variable setup
  String loginLocale = userSession.getUserLocale();
  String loginRights = userSession.getSecurityRights();
  String currCode = request.getParameter("currencyCode");
  String matchAmount = "";
  String unmatchAmount = "";
  if((null != request.getParameter("MatchAmount")) ||("" != request.getParameter("MatchAmount")) ){	 
	  matchAmount = TPCurrencyUtility.getDisplayAmount(request.getParameter("MatchAmount").toString(),request.getParameter("currencyCode"), loginLocale);
  }
  if((null != request.getParameter("UnMatchAmount")) ||("" != request.getParameter("UnMatchAmount")) ){	 
	  unmatchAmount = TPCurrencyUtility.getDisplayAmount(request.getParameter("UnMatchAmount").toString(),request.getParameter("currencyCode"), loginLocale);
	  }
  response.setContentType("text/html");
  out.println(StringFunction.xssCharsToHtml(matchAmount)+'*'+StringFunction.xssCharsToHtml(unmatchAmount));
  %>
