<%-- Begin Tab Button definitions --%>


<% 
  String brandingDirectory = userSession.getBrandingDirectory();
  String leftTabImage  = "/portal/images/"+brandingDirectory+resMgr.getText("brandedImage.TabLeft", TradePortalConstants.TEXT_BUNDLE);
  String rightTabImage = "/portal/images/"+brandingDirectory+resMgr.getText("brandedImage.TabRight", TradePortalConstants.TEXT_BUNDLE);

  String backImage = "BankColor";
  String allignInd = "";
  if (brandingDirectory.toLowerCase().indexOf("anz") != -1)
  {
  	backImage = "TabLabel";
  	allignInd = "align=absbottom";
  }
  
  for(Iterator i = dynamicTabNames.iterator(); i.hasNext(); ){
    String currentTabName = (String)i.next();
    String resource = (String)dynamicTabResourceMap.get(currentTabName);
    String resourceText = resMgr.getText(resource, TradePortalConstants.TEXT_BUNDLE); 
%>  
      
  <div id="<%=currentTabName%>Selected" style="visibility: hidden; position: absolute;">
    <span class="<%=backImage%>">
      <img src="<%= leftTabImage %>" border=0 <%= allignInd %>><span class="TabLabel" style="height: 20px; vertical-align: absbottom;">        
        <%=resourceText%>       
      </span>
      <img src="<%= rightTabImage %>" border=0 <%= allignInd %>>
    </span> 
  </div>
  <div id="<%=currentTabName%>Unselected" style="visibility: hidden; position: absolute;">
    <span class="TabOffBackground">
      <img src="/portal/images/TabOff_left.gif">
      <span class="TabLabelOff" style="height: 20px; vertical-align: center;">        
        <a href=javascript:; onclick="selectTab('<%=currentTabName%>');") class="ButtonLink"><%=resourceText%></a>        
      </span>
      <img src="/portal/images/TabOff_right.gif">
    </span>
  </div>
<%  
  }
%>  


<%-- End Tab Button definitions --%> 

<%-- Begin Display the Tab buttons --%>
  <table border="0" cellpadding="0" cellspacing="0">
    <tr>
      <td width="20">&nbsp</td>
<% 
  for(Iterator i = dynamicTabNames.iterator(); i.hasNext(); ){
    String currentTabName = (String)i.next();
%>    
      <td id="<%=currentTabName%>-TabTableData" nowrap></td>
<% } %>      
      <td width="100%">&nbsp</td>
    </tr>
  </table> 

  <script language="JavaScript">  
  
    var allTabs = new Array();
    <%
      for(int i = 0; i < dynamicTabNames.size(); i++ ){
    %>
        allTabs[<%=i%>] = "<%=(String)dynamicTabNames.get(i)%>";
    <%
      }
    %>   
    
    function selectTab(tabName){
      for(var i = 0; i < allTabs.length; i=i+1 ){
        var tab = allTabs[i];
        var dest = document.getElementById(tab+"-TabTableData");
        if(tab == tabName){
          var src = document.getElementById(tab+"Selected");
          dest.innerHTML = src.innerHTML;
          showTab(tab+"Content");
        } else {          
          var src = document.getElementById(tab+"Unselected");
          dest.innerHTML = src.innerHTML;
          hideTab(tab+"Content");
        }
      }
    }
    
    function showTab(tabName){
      var tab = document.getElementById(tabName);
      tab.style.visibility = "visible";   
    }    
    function hideTab(tabName){
      var tab = document.getElementById(tabName);
      tab.style.visibility = "hidden";      
    }   

  </script>
  <%-- End Display the Tab Buttons --%>