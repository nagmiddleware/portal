<%--
*****************************************************************************************************
                  Direct Debit Transaction History Search Advanced Filter include file

  Description:
    This is a JSP meant to be included using the <%@ include file="x.jsp" %> 
  tag.  It creates the HTML for displaying the Advanced Filter fields for the 
  Direct Debit Instrument Search.  As it is included with <%@ include %> rather than with
  the <jsp:include> directive, it is not a standalone servlet.  
 
  NOTE: Since this JSP is copied into the including page at compile time of
  the including page, changes to this page do not automatically force the
  including pages to be recompiled.  If you make changes to this page, you must
  delete the including page's servlet in order to see the changes.

********************************************************************************************************
--%>

<div>
<input type="hidden" name="NewSearch" value="Y">
<div id="advancePmtFilter" class="searchDetail">
  <div>
    <div class="searchCriteria">
      <%= widgetFactory.createSearchTextField("InstrumentIdAdv","DirectDebitInquiry.criteria.InstrumentId",
            "30", "onKeydown='Javascript: filterDDIHistoryOnEnter(\"InstrumentId\");' class='char10'") %>
      <%-- Kiran  05/13/2013  Rel8.2 IR-T36000014861  Start --%>
	  <%-- Added the Javascript code in order for enter key to work in Firefox--%>
      <%= widgetFactory.createSearchSelectField( "CreditAcctAdv", "DirectDebitInquiry.criteria.CreditAccountNo", 
            "", options.toString(), " onKeydown='filterDDIHistoryOnEnter(\"CreditAcct\");'" ) %>
      <%-- Kiran  05/13/2013  Rel8.2 IR-T36000014861  Start --%>
      <%= widgetFactory.createSearchTextField("ReferenceAdv","DirectDebitInquiry.criteria.ReferenceNo",
            "30", "onKeydown='Javascript: filterDDIHistoryOnEnter(\"Reference\");' class='char10'")%>
    </div>
    <div class="searchActions">
      <button class="gridSearchButton" id="Search" data-dojo-type="dijit.form.Button" type="button">
        <%= resMgr.getText("common.searchButton",TradePortalConstants.TEXT_BUNDLE) %>
        <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
          searchDirectDebitAdvHistory();return false;
        </script>
      </button>
      <%=widgetFactory.createHoverHelp("Search","SearchHoverText") %>
   
      <br>
      <a id="searchLinkAdvPmt" class="searchTypeToggle" href="javascript:shuffleFilterPmtSearch('Basic');">
      <%=resMgr.getText("ExistingDirectDebitSearch.Basic", TradePortalConstants.TEXT_BUNDLE)%></a>
	
      <%=widgetFactory.createHoverHelp("searchTypeToggleAdv","common.BasicSearchHypertextLink") %>
      </div>
     
    <div style="clear:both"></div>
  </div>
  <div class="searchDetail lastSearchDetail">
    <div class="searchCriteria">
      <%=widgetFactory.createSearchAmountField("AmountFrom", "DirectDebitInquiry.criteria.AmountFrom","","onKeydown='Javascript: filterDDIHistoryOnEnter(\"AmountFrom\");' class='char7'","constraints:{min:0}")%>
      <%=widgetFactory.createSearchAmountField("AmountTo", "DirectDebitInquiry.criteria.AmountTo","","onKeydown='Javascript: filterDDIHistoryOnEnter(\"AmountTo\");'class='char7'", "constraints:{min:0}")%>
      <%=widgetFactory.createSearchDateField( "FromDate", "DirectDebitInquiry.criteria.ExecutionDateFrom","class='char8'","constraints:{datePattern:'"+datePattern+"'},placeHolder:'"+datePatternDisplay+"'") %>
      <%=widgetFactory.createSearchDateField( "ToDate", "DirectDebitInquiry.criteria.ExecutionDateTo","class='char8'","constraints:{datePattern:'"+datePattern+"'},placeHolder:'"+datePatternDisplay+"'") %>
    </div>
    
  </div>
</div>
<div style="clear:both"></div>