<%
	// This boolean and MediatorServices provides error handling for invalid
	// search criteria fields.
	boolean errorsFound = false;
	MediatorServices medService = new MediatorServices();
	medService.getErrorManager().setLocaleName(
			resMgr.getResourceLocale());

	// AmountFrom is not numeric then error needs to incorporated

	String errorVariable = request.getParameter("AmountFrom");
	if (!InstrumentServices.isBlank(errorVariable)) {
		try {
			NumberValidator.getNonInternationalizedValue(errorVariable
					.trim(), loginLocale);

		} catch (InvalidAttributeValueException e) {
			medService.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.INVALID_CURRENCY_FORMAT,
					errorVariable);
			errorsFound = true;
		}
	}

	errorVariable = request.getParameter("AmountTo");
	if (!InstrumentServices.isBlank(errorVariable)) {
		try {
			NumberValidator.getNonInternationalizedValue(errorVariable
					.trim(), loginLocale);
		} catch (InvalidAttributeValueException e) {
			medService.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.INVALID_CURRENCY_FORMAT,
					errorVariable);
			errorsFound = true;
		}
	}

	if (!TPDateTimeUtility.isGoodDate(request.getParameter("YearTo"),
			request.getParameter("MonthTo"), request
					.getParameter("DayTo"))) {
		medService.getErrorManager().issueError(
				TradePortalConstants.ERR_CAT_1,
				TradePortalConstants.INVALID_TO_DATE);
		errorsFound = true;
	}

	if (!TPDateTimeUtility.isGoodDate(request.getParameter("YearFrom"),
			request.getParameter("MonthFrom"), request
					.getParameter("DayFrom"))) {
		medService.getErrorManager().issueError(
				TradePortalConstants.ERR_CAT_1,
				TradePortalConstants.INVALID_FROM_DATE);
		errorsFound = true;
	}

	if (errorsFound) {
		// Any errors found while editing the date and amount fields need to be
		// placed in the doc cache.
		DocumentHandler errDoc = formMgr.getFromDocCache();
		medService.addErrorInfo();
		errDoc.setComponent("/Error", medService.getErrorDoc());
		formMgr.storeInDocCache("default.doc", errDoc);
	}
%>