<%--
 *
 *     Copyright  � 2008                         
 *     CGI 
 *     All rights reserved
--%>
<%--
**********************************************************************************
                        Receivables Instruments Tab -  Search frag file

  Description:
    Contains HTML to create the Receivable Instruments for the Receivables Management Home page

  This is not a standalone JSP.  It MUST be included in a jsp page.
*******************************************************************************
--%> 	
	<%
	   	
	final String      ALL_ORGANIZATIONS            = resMgr.getText("AuthorizedTransactions.AllOrganizations",
                                                                   TradePortalConstants.TEXT_BUNDLE);
    
    String instrumentId = "";
  	String currency = "";
   	String amountFrom = "";
   	String amountTo = "";
   	String options="";
   
   	String selectedStatus = "";
   	String selectedOrg    = "";
    	
	  selectedOrg = request.getParameter("historyOrg");
	      if (selectedOrg == null)
	      {
	         selectedOrg = (String) session.getAttribute("historyOrg");
	         if (selectedOrg == null)
	         {
	            selectedOrg = EncryptDecrypt.encryptStringUsingTripleDes(userOrgOid, userSession.getSecretKey());
	         }
	      }

      	 session.setAttribute("historyOrg", selectedOrg);

      	 selectedOrg = EncryptDecrypt.decryptStringUsingTripleDes(selectedOrg, userSession.getSecretKey());
      	 
    	// Check if this is a new Search (based on passed in NewSearch parameter)
      	// If so, clear the listview information so it does not
      	// retain any page/sort order/ sort column/ search type information
      	// resulting from previous searches

      	// A new search can also result from the corpo org or status type dropdown
      	// being reselected. In this case, NewDropdownSearch parameter indicates a
      	// pseudo new search -- the search type is retained but the rest of the
      	// search clause is not

      	String newSearch = request.getParameter("NewARMSearch");
     
      	String newDropdownSearch = request.getParameter("NewDropdownSearch");
      	Debug.debug("New Search is " + newSearch);
      	Debug.debug("New Dropdown Search is " + newDropdownSearch);


	      if ((newSearch != null) && (newSearch.equals(TradePortalConstants.INDICATOR_YES)))
	      {
	         // Default search type for instrument status on the transactions history page is ACTIVE.
	         session.setAttribute("instrStatusType", TradePortalConstants.STATUS_ACTIVE);
	      }
         
      	
                                                                  
		selectedStatus = request.getParameter("instrStatusType");
		
	      if (selectedStatus == null)
	      {
	         selectedStatus = (String) session.getAttribute("instrStatusType");
	      }
		// Based on the statusType dropdown, build the where clause to include one or more
        // instrument statuses.
	      Vector statuses = new Vector();
	      if (TradePortalConstants.STATUS_ACTIVE.equals(selectedStatus) )
	      {
	         statuses.addElement(TradePortalConstants.INSTR_STATUS_ACTIVE);
	         statuses.addElement(TradePortalConstants.INSTR_STATUS_PENDING);
	         statuses.addElement(TradePortalConstants.INSTR_STATUS_EXPIRED);
	      }

	      else if (TradePortalConstants.STATUS_INACTIVE.equals(selectedStatus) )
	      {
	         statuses.addElement(TradePortalConstants.INSTR_STATUS_CANCELLED);
	         statuses.addElement(TradePortalConstants.INSTR_STATUS_CLOSED);
	         statuses.addElement(TradePortalConstants.INSTR_STATUS_DEACTIVATED);
	         statuses.addElement(TradePortalConstants.INSTR_STATUS_LIQUIDATE_ALL);
	         statuses.addElement(TradePortalConstants.INSTR_STATUS_LIQUIDATE_BANK_SIDE);
	         statuses.addElement(TradePortalConstants.INSTR_STATUS_LIQUIDATE_CUST_SIDE);
	      }

	      else // default is ALL (actually not all since DELeted instruments
	           // never show up (handled by the SQL in the listview XML)
	      {
	         selectedStatus = TradePortalConstants.STATUS_ALL;
	      }

	      session.setAttribute("instrStatusType", selectedStatus);
	
	// This boolean and MediatorServices provides error handling for invalid
    // search criteria fields.
  	boolean errorsFound = false;
  	MediatorServices medService = new MediatorServices();
 	medService.getErrorManager().setLocaleName(resMgr.getResourceLocale());
 	
  // Choose a character for the delimiter that will never appear in any data
  // The data must be base64ed first so that we can guarantee that the delimiter
  // will not appear
  String DELIMITER = "@";

  	instrumentId = request.getParameter("InstrumentId");

        if (instrumentId != null) {
          instrumentId = instrumentId.trim().toUpperCase();
        } else {
          instrumentId = "";
        }
    newSearchReceivableCriteria.append("InstrumentId=" + EncryptDecrypt.stringToBase64String(instrumentId) + DELIMITER);

  	currency = request.getParameter("Currency");
    	if (currency == null) {
            currency = "";
        }    
      newSearchReceivableCriteria.append("Currency=" + EncryptDecrypt.stringToBase64String(currency) + DELIMITER);
        
     amountFrom = request.getParameter("AmountFrom");
        if (amountFrom != null) {
           amountFrom = amountFrom.trim();
        
      } else {
          amountFrom = "";
      }  
      newSearchReceivableCriteria.append("AmountFrom=" + EncryptDecrypt.stringToBase64String(amountFrom) + DELIMITER);   
      
      amountTo = request.getParameter("AmountTo");
        if (amountTo != null) {
           amountTo = amountTo.trim();
     
        } else {
           amountTo = "";
        }   
       newSearchReceivableCriteria.append("AmountTo=" + EncryptDecrypt.stringToBase64String(amountTo) + DELIMITER);
  
  
   if (errorsFound) {
    // Any errors found while editing the date and amount fields need to be
    // placed in the doc cache.
    DocumentHandler errDoc = formMgr.getFromDocCache();
    medService.addErrorInfo();
    errDoc.setComponent("/Error", medService.getErrorDoc());
    formMgr.storeInDocCache("default.doc", errDoc);
  }
   
 	%>
