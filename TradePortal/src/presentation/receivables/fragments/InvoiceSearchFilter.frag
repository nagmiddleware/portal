<%--
*******************************************************************************
                  Instrument Search Advanced Filter include file

  Description:
    This is a JSP meant to be included using the <%@ include file="x.jsp" %> 
  tag.  It creates the HTML for displaying the Advanced Filter fields for the 
  Instrument Search.  As it is included with <%@ include %> rather than with
  the <jsp:include> directive, it is not a standalone servlet.  
  
  IMPORTANT NOTE: This page may currently come from the following:
  
  Create Transaction Step 1 Page: for an existing instrument or copying an
  			instrument
  Export LC Issue Transfer: Search Instrument button
  MessageHome: Search Instrument
  
  Each originating page should contain the following code:
  
    <input type="hidden" name="NewSearch" value="Y">
  
  - this will allow a new search criteria to be created.

  NOTE: Since this JSP is copied into the including page at compile time of
  the including page, changes to this page do not automatically force the
  including pages to be recompiled.  If you make changes to this page, you must
  delete the including page's servlet in order to see the changes.
  
  
  
  <%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.html.*,java.math.*" %>

*******************************************************************************
--%>
<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
  <input type="hidden" name="NewSearch" value="Y">
  <input type="hidden" name="FinanceCheck" value="<%= TradePortalConstants.INDICATOR_NO %>">
<%
  String amountFrom = "";
  String amountTo = "";
  String poNumber = "";
  String currency = "";
  String dayFrom = "";
  String monthFrom = "";
  String yearFrom = "";
  String dayTo = "";
  String monthTo = "";
  String yearTo = "";
  String otherParty = "";
  String options = "";
  String finance = "";
  String dateType = "";
  String amountType = "";
  String partyFilterText = "";
  String fromDate = "";
  String toDate = "";
  
  //Pramey 
  // Choose a character for the delimiter that will never appear in any data
  // The data must be base64ed first so that we can guarantee that the delimiter
  // will not appear
  String DELIMITER = "@";

 	invoiceID = request.getParameter("InvoiceID");	  
	if (invoiceID == null) {
	 invoiceID = "";
	}
	else {
	    invoiceID = invoiceID.trim().toUpperCase();
	}
  
  // Search by the invoice ID
  if (!invoiceID.equals("")) {
     newSearchReceivableCriteria.append("InvoiceID=" + EncryptDecrypt.stringToBase64String(invoiceID.toUpperCase()) + DELIMITER);
}  
  
 // Search by Finance
   finance = request.getParameter("FinanceCheck");
  
   if (finance != null) {
    
     newSearchReceivableCriteria.append("FinanceCheck=" + EncryptDecrypt.stringToBase64String(finance) + DELIMITER);	 
   }

  
  // Search by date type	
  dateType = request.getParameter("DateType");
  String expandedDateType = "";
  if (dateType == null ) {
    dateType = "";
  } else {
    if (dateType.equals("DDT")){
      expandedDateType = "invoice_due_date";
    }
    else if (dateType.equals("IDT")){
      dateType = "invoice_issue_datetime";
    }
    else {
      expandedDateType = "";
    }
   newSearchReceivableCriteria.append("DateType=" + EncryptDecrypt.stringToBase64String(dateType) + DELIMITER);	 
  }
  

  dayFrom   = request.getParameter("DayFrom");
  monthFrom = request.getParameter("MonthFrom");
  yearFrom  = request.getParameter("YearFrom");	
 
   dayTo   = request.getParameter("DayTo");
   monthTo = request.getParameter("MonthTo");
   yearTo  = request.getParameter("YearTo");
  if (!dateType.equals("")){
    if (TPDateTimeUtility.isGoodDate(yearFrom, monthFrom, dayFrom)) {
      fromDate = TPDateTimeUtility.formatOracleDate(yearFrom, monthFrom, dayFrom);
      if (!fromDate.equals("")) {
  	newSearchReceivableCriteria.append("DayFrom=" + EncryptDecrypt.stringToBase64String(dayFrom) + DELIMITER);
	newSearchReceivableCriteria.append("MonthFrom=" + EncryptDecrypt.stringToBase64String(monthFrom) + DELIMITER);
	newSearchReceivableCriteria.append("YearFrom=" + EncryptDecrypt.stringToBase64String(yearFrom) + DELIMITER);
      }
    } 
  
   
   if (TPDateTimeUtility.isGoodDate(yearTo, monthTo, dayTo)) {
	toDate = TPDateTimeUtility.formatOracleDate(yearTo,monthTo, dayTo);
	if (!toDate.equals("")) {
	   newSearchReceivableCriteria.append("DayTo=" + EncryptDecrypt.stringToBase64String(dayTo) + DELIMITER);
	  newSearchReceivableCriteria.append("MonthTo=" + EncryptDecrypt.stringToBase64String(monthTo) + DELIMITER);
	  newSearchReceivableCriteria.append("YearTo=" + EncryptDecrypt.stringToBase64String(yearTo) + DELIMITER);
	}
    } 
  }

  // Search by currency
  currency = request.getParameter("Currency");
  if (currency != null) {
    if (!currency.equals("")) {
    }
    newSearchReceivableCriteria.append("Currency=" + EncryptDecrypt.stringToBase64String(currency) + DELIMITER);
  } 
	


  // Search by Amount
  amountType = request.getParameter("AmountType");	
  String expandedAmountType = "";
  if (amountType == null ) {
    expandedAmountType = "";
  } else {
    if(amountType.equals("INA")){
      expandedAmountType = "invoice_total_amount";
    }
    else if (amountType.equals("DSA")){
      expandedAmountType = "invoice_discount_amount";
    }
    else if (amountType.equals("CNA")){
      expandedAmountType = "invoice_credit_note_amount";
    }
    else if (amountType.equals("FNA")){
      expandedAmountType = "finance_amount";
    }
    else { 
      expandedAmountType = "";
    }
    newSearchReceivableCriteria.append("AmountType=" + EncryptDecrypt.stringToBase64String(amountType) + DELIMITER);
  }
  
  
  
  
  amountFrom = request.getParameter("AmountFrom");
  amountTo = request.getParameter("AmountTo");
  if(!InstrumentServices.isBlank(amountType)) {
    if (amountFrom != null) {
      amountFrom = amountFrom.trim();
     
      newSearchReceivableCriteria.append("AmountFrom=" + EncryptDecrypt.stringToBase64String(amountFrom) + DELIMITER);
    } 
	
    if (amountTo != null) {
      amountTo = amountTo.trim();
     
    newSearchReceivableCriteria.append("AmountTo=" + EncryptDecrypt.stringToBase64String(amountTo) + DELIMITER);
    } 
    
  }
  
  
  partyFilterText = request.getParameter("PartyFilterText");
  if ( partyFilterText != null && !partyFilterText.equals("")) {          
    partyFilterText = partyFilterText.trim();
    newSearchReceivableCriteria.append("PartyFilterText=" + EncryptDecrypt.stringToBase64String(partyFilterText.toUpperCase()) + DELIMITER);
  } 
  
  
  poNumber = request.getParameter("PONumber");
  if (poNumber != null && !poNumber.equals("")) {
    poNumber = poNumber.toUpperCase();
    newSearchReceivableCriteria.append("PONumber=" + EncryptDecrypt.stringToBase64String(poNumber.toUpperCase()) + DELIMITER);
  }
   
%>


<div class="searchDetail">
	<span class="searchCriteria">
		<%=widgetFactory.createSearchTextField("InvoiceID", "InvoiceSearch.InvoiceID", "20","onKeydown='Javascript: filterInvoicesOnEnter(\"InvoiceID\");' class='char15'") %>
		<%=widgetFactory.createSearchTextField("BuyerIdentifier", "InvoiceSearch.BuyerId", "20","onKeydown='Javascript: filterInvoicesOnEnter(\"BuyerIdentifier\");' class='char15'") %>
		<%=widgetFactory.createSearchTextField("PONumber", "InvoiceSearch.PONumber", "20","onKeydown='Javascript: filterInvoicesOnEnter(\"PONumber\");' class='char15'") %>
	</span>
	
	<span class="searchActions">
		<button data-dojo-type="dijit.form.Button" type="button" id="searchParty"><%= resMgr.getText("common.searchButton",TradePortalConstants.TEXT_BUNDLE) %>
				<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">searchInvoices();</script>
		  	</button>
		  	<%=widgetFactory.createHoverHelp("searchParty","SearchHoverText") %>
	</span>
	<div style="clear:both"></div>
</div>

<div class="searchDetail">
	<span class="searchCriteria">
		<%options = Dropdown.createSortedRefDataOptions(TradePortalConstants.CURRENCY_CODE, currency, loginLocale);
  			out.println(widgetFactory.createSearchSelectField("Currency", "InvoiceSearch.Currency", " ", options,"class='char30'"));
  			%>

  		<%	Vector  codesToExclude   = new Vector();
  	    	codesToExclude.add("PYA");
  	    	options = Dropdown.createSortedRefDataOptions(TradePortalConstants.AMOUNT_TYPE, amountType, loginLocale,codesToExclude);
  			out.println(widgetFactory.createSearchSelectField("AmountType", "InvoiceSearch.AmountType", " ", options,"class='char20'"));
  			%>
		<%=widgetFactory.createSearchAmountField("AmountFrom", "InvoiceSearch.From","","onKeydown='Javascript: filterInvoicesOnEnter(\"AmountFrom\");' class='char10'", "") %>
		<%=widgetFactory.createSearchAmountField("AmountTo", "InvoiceSearch.To","","onKeydown='Javascript: filterInvoicesOnEnter(\"AmountTo\");' class='char10'", "") %>
	</span>
	
	
	<div style="clear:both"></div>
</div>

<div class="searchDetail">
	<span class="searchCriteria">
		<%options = Dropdown.createSortedRefDataOptions(TradePortalConstants.DATE_TYPE, dateType, loginLocale);
  			out.println(widgetFactory.createSearchSelectField("DateType", "InvoiceSearch.DateType", " ", options,"class='char10'"));
  			%>
		<%=widgetFactory.createSearchDateField("DateFrom","InvoiceSearch.From","class='char15'","constraints:{datePattern:'"+datePattern+"'},placeHolder:'"+datePatternDisplay+"'") %>
		<%=widgetFactory.createSearchDateField("DateTo","InvoiceSearch.To","class='char15'","constraints:{datePattern:'"+datePattern+"'},placeHolder:'"+datePatternDisplay+"'") %>
	</span>
	
	
	<div style="clear:both"></div>
</div>

<script language="Javascript">
function filterInvoicesOnEnter(fieldID){
	require(["dojo/on","dijit/registry"],function(on, registry) {
	    on(registry.byId(fieldID), "keypress", function(event) {
	        if (event && event.keyCode == 13) {
	        	dojo.stopEvent(event);
	        	searchInvoices();
	        }
	    });
	});	<%-- end require --%>
}<%-- end of filterInvoicesOnEnter() --%>

</script>
