<%--
*******************************************************************************
  Direct Debit Pending Transactions Footer

  Description: Everything that should be loaded at the end of the page
               should be included here.  This includes non-visible html 
               and javascript that should execute on page load.

*******************************************************************************
--%>
<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>

<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>

<%-- <%
 
  gridLayout = dgFac.createGridLayout("ddPendGrid", "DirectDebitPendingDataGrid");
	
%> --%>

<script type="text/javascript">

  <%--get grid layout from the data grid factory--%>
  var gridLayout = <%= gridLayout %>;
  var Workflow=savedSearchQueryData["Workflow"];
  var transStatusType=savedSearchQueryData["transStatusType"];
  var initSearchParms="";
  var savedSort = savedSearchQueryData["sort"];
   function initParms(){
    require(["dojo/dom","dijit/registry"],
		      function(dom,registry){
    	if("" == Workflow || null == Workflow || "undefined" == Workflow){
    		  Workflow=dijit.byId('Workflow').value;
		 	 }
		  	else{
			  registry.byId("Workflow").set('value',Workflow);
		  	}
			if("" == savedSort || null == savedSort || "undefined" == savedSort)
	  			savedSort = '0';
  			if("" == transStatusType || null == transStatusType || "undefined" == transStatusType){
  				 transStatusType=dijit.byId('transStatusType').value;
	 		}
  			else{
  				registry.byId("transStatusType").set('value',transStatusType);
  		  	}
     
  
  
    initSearchParms = "Workflow="+Workflow+"&transStatusType="+transStatusType;
 
  
 
  <%--create the grid, attaching it to the div id specified in dataGrid.jsp--%>
  var viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("DirectDebitPendingDataView",userSession.getSecretKey())%>";  <%--Rel9.2 IR T36000032596 --%>
  var DirectDebitPendingDataGridId = 
    createDataGrid("ddPendGrid", viewName,
                    gridLayout, initSearchParms,savedSort);
    }); 
   }
  
  function searchPending(){
	 
    transStatusType=dijit.byId("transStatusType").value;
    Workflow=dijit.byId("Workflow").value;
    var searchParms = "Workflow="+Workflow+"&transStatusType="+transStatusType;
    searchDataGrid("ddPendGrid", "DirectDebitPendingDataView",
                   searchParms);
 
 }
  function routePending(){
	  	openRouteDialog('routeItemDialog', routeItemDialogTitle, 
				'ddPendGrid', 
				'<%=TradePortalConstants.INDICATOR_YES%>', 
				'<%=TradePortalConstants.FROM_DDI%>','') ;
	  }
  
  function authorisePending(){
	  <% 
	  //IR T36000014931 REL ER 8.1.0.4 BEGIN
	  if (SecurityAccess.hasRights(userSecurityRights, SecurityAccess.DDI_AUTHORIZE))
	  {
	     if ( ddiRequireAuth && InstrumentServices.isNotBlank(certAuthURL)) 
	     {
	  %>
	  		openURL("<%=ddiLink%>");
	  <% }else{
	  //IR T36000014931 REL ER 8.1.0.4 END
	  %>
	    var formName = '<%=formName%>';
	    var buttonName = '<%=TradePortalConstants.BUTTON_AUTHORIZE%>';

	    <%--get array of rowkeys from the grid--%>
	    var rowKeys = getSelectedGridRowKeys("ddPendGrid");

	    <%--submit the form with rowkeys
	        becuase rowKeys is an array, the parameter name for
	        each will be checkbox + an iteration number -
	        i.e. checkbox0, checkbox1, checkbox2, etc --%>
	    submitFormWithParms(formName, buttonName, "checkbox", rowKeys);
	 <%
	 //IR T36000014931 REL ER 8.1.0.4 BEGIN
	  }
	  }
	  //IR T36000014931 REL ER 8.1.0.4 END
	  %>
  }
<%--IR T36000014931 REL ER 8.1.0.4 BEGIN --%> 
  function openURL(URL){
    if (isActive =='Y') {
	    if (URL != '' && URL.indexOf("javascript:") == -1) {
	        var cTime = (new Date()).getTime();
	        URL = URL + "&cTime=" + cTime;
	        URL = URL + "&prevPage=" + context;
        }
    }
	    document.location.href  = URL;
		return false;
}
<%--IR T36000014931 REL ER 8.1.0.4 END --%> 
  
  function deletePending(){
	  var formName = '<%=formName%>';
	    var buttonName = '<%=TradePortalConstants.BUTTON_DELETE%>';
   
		    <%--- SureshL  5/23/2014 Rel8.4  IR-T36000027671 -start  ---%>   
		    var   confirmMessage = "<%=StringFunction.asciiToUnicode(resMgr.getText("FutureValueTransactions.PopupMessage",
		    TradePortalConstants.TEXT_BUNDLE)) %>";
		     
		   
		     <%--get array of rowkeys from the grid--%>
	    var rowKeys = getSelectedGridRowKeys("ddPendGrid");
		    if(rowKeys == '' || rowKeys == "")
		    	return false;
		    
		    if (!confirm(confirmMessage)) 
			{
				return false;
			}else{
				<%--submit the form with rowkeys
		        becuase rowKeys is an array, the parameter name for
		        each will be checkbox + an iteration number -
		        i.e. checkbox0, checkbox1, checkbox2, etc --%>
		    submitFormWithParms(formName, buttonName, "checkbox", rowKeys);
			}
	  <%--- SureshL  5/23/2014 Rel8.4  IR-T36000027671 -end  ---%> 
  }

  require(["dojo/query", "dojo/on", "t360/datagrid", "t360/popup","dojo/ready", "dojo/domReady!"],
      function(query, on, t360grid, t360popup, ready ) {
          ready(function() {
        	  initParms();
    query('#ddPendRefresh').on("click", function() {
      searchPending();
    });
    query('#ddPendGridEdit').on("click", function() {
      var columns = t360grid.getColumnsForCustomization('ddPendGrid');
      var parmNames = [ "gridId", "gridName", "columns" ];
      var parmVals = [ "ddPendGrid", "DirectDebitPendingDataGrid", columns ];
      t360popup.open(
        'ddPendGridEdit', 'gridCustomizationPopup.jsp',
        parmNames, parmVals,
        null, null);  <%-- callbacks --%>
    });
          });
  });
 

</script>

<%--cquinton 10/8/2012 include gridShowCounts --%>
<jsp:include page="/common/gridShowCountFooter.jsp">
  <jsp:param name="gridId" value="ddPendGrid" />
</jsp:include>
