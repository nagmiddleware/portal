<%--cquinton 5/23/2013 Rel 8.2 ir#14780 start
    change all buttons to type=button and refactor script logic so double submits do not occur--%>
<%--cquinton 9/13/2012 Rel portal refresh start
    add titlepane to sidebar actions--%>
<%
	boolean savedSectionValue =userSession.getSavedSectionValue();
%>
<div data-dojo-type="dijit.TitlePane" class="sectionLinksSection" id="sectionLinksSection" open ='<%=savedSectionValue%>'  title='<%=resMgr.getText("Sidebar.SectionShortcuts",TradePortalConstants.TEXT_BUNDLE)%>'>
  <script type="dojo/connect" data-dojo-event="toggle" data-dojo-args="evt">callAjaxToUpdateSection();return false;</script>
  <ol id="navContainer" class="sectionLinks"></ol>
</div>    
<%
  boolean showLinks = true;
  if(request.getParameter("showLinks")!=null)
    if(request.getParameter("showLinks").equals("false"))
      showLinks = false;
					
%>    
<%= widgetFactory.createSidebarQuickLinks(showLinks, true, showLinks, userSession)  %>

<div class="formItem">
<% 
  String button ="";
  String saveTime = "";
  String errorSev = doc.getAttribute("/Error/maxerrorseverity");
  button = request.getParameter("buttonPressed");
  //out.println("error="+errorSev);
  if(errorSev !=null) {
    if(errorSev.contains("0")) {
      saveTime = TPDateTimeUtility.getCurrentTimeForRequestTimeZone(userSession.getTimeZone());
%>
  Saved <%=StringFunction.xssCharsToHtml(saveTime)%>
<%
    }
  }
%>
</div>

<%---style add to over write the styles of dojo to wrap the text for approve and decline discount---%>
<style>
.claro .dijitButtonText {											
  text-align: left;
}
</style>
<ul class="sidebarButtons">	
     
  <button data-dojo-type="dijit.form.Button"  name="SaveButton" id="SaveButton" type="button" data-dojo-props="iconClass:'save'" >
    <%=resMgr.getText("common.SaveText",TradePortalConstants.TEXT_BUNDLE)%>
  </button>
  <%=widgetFactory.createHoverHelp("SaveButton","SaveHoverText") %>   

  <button data-dojo-type="dijit.form.Button"  name="SaveCloseButton" id="SaveCloseButton" type="button" data-dojo-props="iconClass:'saveclose'" >
    <%=resMgr.getText("common.SaveCloseText",TradePortalConstants.TEXT_BUNDLE)%>
  </button>
  <%=widgetFactory.createHoverHelp("SaveCloseButton","SaveCloseHoverText") %> 
   
  <button data-dojo-type="dijit.form.Button"  name="RouteButton" id="RouteButton" type="button" data-dojo-props="iconClass:'route'" >
    <%=resMgr.getText("common.RouteText",TradePortalConstants.TEXT_BUNDLE)%>
  </button>
  <%=widgetFactory.createHoverHelp("RouteButton","RouteHoverText") %>
   
<% 
//IR#T36000016617 - 06/01/2013 
	String theOrgId = userSession.getOwnerOrgOid();
	CorporateOrganizationWebBean corpOrg = null;
	String erpOn = "";
	beanMgr.registerBean("com.ams.tradeportal.busobj.webbean.CorporateOrganizationWebBean", "CorporateOrganization");
	corpOrg = (CorporateOrganizationWebBean) beanMgr.getBean("CorporateOrganization"); 
	corpOrg.getById(theOrgId );
	erpOn = corpOrg.getAttribute("erp_transaction_rpt_reqd");
//These two buttons should not appear when ERP setting is on ( i.e No, Null, empty, etc - anything other than YES)
  if(isDiscounted && !TradePortalConstants.INDICATOR_YES.equals(erpOn)) { 

%> 
  <button data-dojo-type="dijit.form.Button"  name="ApproveDiscountButton" id="ApproveDiscountButton" type="button" data-dojo-props="iconClass:'authorize'" >
    <%=resMgr.getText("common.ApproveDiscountAuthorizeText",TradePortalConstants.TEXT_BUNDLE)%>
  </button>      
   
  <button data-dojo-type="dijit.form.Button"  name="DeclineDiscountAuthorizeButton" id="DeclineDiscountAuthorizeButton" type="button" data-dojo-props="iconClass:'authorize'">
    <%=resMgr.getText("common.DeclineDiscountAuthorizeText",TradePortalConstants.TEXT_BUNDLE)%>
  </button>
<%
  }
  else {       
%>
  <button data-dojo-type="dijit.form.Button"  name="AuthorizeButton" id="AuthorizeButton" type="button" data-dojo-props="iconClass:'authorize'" >
    <%=resMgr.getText("common.AuthorizeText",TradePortalConstants.TEXT_BUNDLE)%>
  </button>
  <%=widgetFactory.createHoverHelp("AuthorizeButton","AuthorizeHoverText") %>
<%
  }
%>

  <button data-dojo-type="dijit.form.Button" name="CloseButton" id="CloseButton" type="button" data-dojo-props="iconClass:'close'" >
    <%=resMgr.getText("common.CloseText",TradePortalConstants.TEXT_BUNDLE)%>
  </button>
  <%=widgetFactory.createHoverHelp("CloseButton","CloseHoverText") %>

</ul>
<%--cquinton 5/23/2013 Rel 8.2 ir#14780 end--%>
