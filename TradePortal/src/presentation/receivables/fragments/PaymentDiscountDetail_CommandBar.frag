<script language="Javascript">

function openURL(URL){
		document.location.href  = URL;
		return false;
	}
 
</script>  

<div data-dojo-type="dijit.TitlePane" class="sectionLinksSection" title='<%=resMgr.getText("Sidebar.SectionShortcuts",TradePortalConstants.TEXT_BUNDLE)%>'>
  <ol id="navContainer" class="sectionLinks"></ol>
</div>    
    
<div data-dojo-type="dijit.TitlePane" class="sidebarLinksSection" title='<%=resMgr.getText("Sidebar.QuickLinks",TradePortalConstants.TEXT_BUNDLE)%>'>		
  <ul class="navigationLinks" style="padding-left: 10px;">
<%
  boolean showLinks = true;
  if(request.getParameter("showLinks")!=null)
    if(request.getParameter("showLinks").equals("false"))
      showLinks = false;
					
    if(showLinks){
%>
    <li><div id="expandCollapse"><a href="javascript:expandCollapse();" >Collapse All</a></div></li>
<%  } %>
<%=widgetFactory.createHoverHelp("expandCollapse","ExpandCollapseHoverText") %>
    <li><div id="hideShowTips"><a href="javascript:hideShowTips();">Hide Tips</a></div></li>
<%  if(showLinks){ %>
    <li id="BackToTop"><a href="#top">Back To Top</a></li>
<%  } %>
  </ul>	
   <%=widgetFactory.createHoverHelp("BackToTop","BackTopHoverText") %>
  <div class="sidebarDivider"></div>
</div>

<div id="routeDialog" style="display:none;"></div>
		<div class="formItem">
		<% 
			String button ="";
			String saveTime = "";
			String errorSev = doc.getAttribute("/Error/maxerrorseverity");
			button = request.getParameter("buttonPressed");
			//out.println("error="+errorSev);
			if(errorSev !=null){
					if(errorSev.contains("0")) {
						saveTime = TPDateTimeUtility.getCurrentTimeForRequestTimeZone(userSession.getTimeZone());
		
		%>
						Saved <%=StringFunction.xssCharsToHtml(saveTime)%>
		<%
					}
			}
		%>
		</div>
		<%---style add to over write the styles of dojo to wrap the text for approve and decline discount---%>
		<style>
		.claro .dijitButtonText {											
   		 text-align: left;
   			 }
    	</style>
  <ul class="sidebarButtons">	
     <%--
      <button data-dojo-type="dijit.form.Button"  type=button name="SaveButton" id="SaveButton" data-dojo-props="iconClass:'save'" >
				<%=resMgr.getText("common.SaveText",TradePortalConstants.TEXT_BUNDLE)%>
				<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
					setButtonPressed('SaveTrans', '0');	
document.forms[0].submit();	
			 	</script>
			</button>
			
			<%=widgetFactory.createHoverHelp("SaveButton","SaveHoverText") %>   --%>
      <button data-dojo-type="dijit.form.Button"  name="SaveCloseButton" type=button id="SaveCloseButton" data-dojo-props="iconClass:'saveclose'" >
				<%=resMgr.getText("common.SaveCloseText",TradePortalConstants.TEXT_BUNDLE)%>
				<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">

					setButtonPressed('SaveAndClose', '0');
			document.forms[0].submit();
			 	</script>
			</button>
			<%=widgetFactory.createHoverHelp("SaveCloseButton","SaveCloseHoverText") %> 
   
      <%
        String link=formMgr.getLinkAsUrl("goToPaymentMatchResponse", response);
      %>

      
        <button data-dojo-type="dijit.form.Button" name="CloseButton" id="CloseButton" type="button" data-dojo-props="iconClass:'close'" >
				<%=resMgr.getText("common.CloseText",TradePortalConstants.TEXT_BUNDLE)%>
        <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
 				var r=confirm("Changes will not be saved");
				if (r==true)
 				{
						  openURL("<%=link%>"); 
 				}
                					
        </script>
			</button>
        <%=widgetFactory.createHoverHelp("CloseButton","CloseHoverText") %>
        </ul>
