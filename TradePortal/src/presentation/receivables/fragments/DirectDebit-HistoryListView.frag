<%--
 *
 *     Copyright  � 2008                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
**********************************************************************************
                        Direct Debit Transaction History Tab

  Description:
    Contains HTML to create the History tab for the Direct Debit Transaction History page

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transactions-HistoryListView.jsp" %>
*******************************************************************************
--%>

<%--cquinton 8/30/2012 add grid search header--%>
<div class="gridSearch">
  <div class="searchHeader">
    <span class="searchHeaderCriteria">

<%

  //StringBuffer statusOptions = new StringBuffer();         // NSX - PRUK080636392 - 08/16/10 -
  StringBuffer statusExtraTags = new StringBuffer();

  // NSX - PRUK080636392 - 08/16/10 - Begin
  StringBuffer statusOptions = Dropdown.createActiveInActiveStatusOptions(resMgr,selectedStatus);
  // Upon changing the status selection, automatically go back to the Transactions Home
  // page with the selected status type.  Force a new search to occur (causes cached
  // "where" clause to be reset.)

  statusExtraTags.append("onchange=\"location='");
  statusExtraTags.append(formMgr.getLinkAsUrl("goToDirectDebitTransactionsHome", response));
  statusExtraTags.append("&amp;current2ndNav=");
  statusExtraTags.append(current2ndNav);
  statusExtraTags.append("&NewDropdownSearch=Y");
  statusExtraTags.append("&amp;instrStatusType='+this.options[this.selectedIndex].value\"");

  // When the Instrument History tab is selected, display 
  // Organizations dropdown or the single organization.
  if ((SecurityAccess.hasRights(userSecurityRights, SecurityAccess.VIEW_CHILD_ORG_WORK)) 
       && (totalOrganizations > 1)) {
     orgListDropDown = true;
     dropdownOptions.append(Dropdown.getUnsortedOptions(hierarchyDoc, 
                                                        "ORGANIZATION_OID", "NAME", 
                                                        selectedOrg, userSession.getSecretKey()));
     dropdownOptions.append("<option value=\"");
          dropdownOptions.append(EncryptDecrypt.encryptStringUsingTripleDes(ALL_ORGS, userSession.getSecretKey()));
     dropdownOptions.append("\"");

     if (selectedOrg.equals(ALL_ORGS)) {
       dropdownOptions.append(" selected");
     }

     dropdownOptions.append(">");
     dropdownOptions.append(resMgr.getText(ALL_ORGS, TradePortalConstants.TEXT_BUNDLE));
     dropdownOptions.append("</option>");

     extraTags = new StringBuffer("");
     extraTags.append("onchange=\"location='");
     extraTags.append(formMgr.getLinkAsUrl("goToDirectDebitTransactionsHome", response));
     extraTags.append("&amp;current2ndNav=");
     extraTags.append(current2ndNav);
     extraTags.append("&NewDropdownSearch=Y");
     extraTags.append("&historyOrg='+this.options[this.selectedIndex].value\"");
%>
      <%= widgetFactory.createSearchSelectField("org", "DirectDebitInquiry.Show", "",
        dropdownOptions.toString(), "onChange='searchDirectDebitHistory();'") %>
<%
  }
  StringBuffer ddStatus = new StringBuffer();
  ddStatus.append(widgetFactory.createInlineLabel("", "DirectDebitInquiry.criteria.Status"));
  ddStatus.append(widgetFactory.createCheckboxField("Active", "DirectDebitInquiry.criteria.Active", true, 
    false, false,"onClick='searchDirectDebitHistory();'","","none"));
  ddStatus.append(widgetFactory.createCheckboxField("InActive", "DirectDebitInquiry.criteria.Inactive", false, 
    false,false,"onClick='searchDirectDebitHistory();'","","none"));
  widgetFactory.wrapSearchItem(ddStatus,"");
%>
      <%= ddStatus %>
    </span>
    <span class="searchHeaderActions">

<% if (SecurityAccess.hasRights(userSecurityRights,SecurityAccess.VIEW_INSTRUMENT_CREATE_OR_MODIFY)) { %>
      <span>
        <button data-dojo-type="dijit.form.Button"  name="CopySelected" id="CopySelected" type="button">
          <%=resMgr.getText("CopySelected.Button",TradePortalConstants.TEXT_BUNDLE) %>
          <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
            openCopySelectedDialogHelper(copySelectedToInstrument);       					
          </script>
        </button> 
      </span> 
      <%=widgetFactory.createHoverHelp("CopySelected","DDICopySelectedHoverText") %>
<% } %>		

      <%--cquinton 10/8/2012 include gridShowCounts --%>
      <jsp:include page="/common/gridShowCount.jsp">
        <jsp:param name="gridId" value="ddInquiryGrid" />
      </jsp:include>

      <span id="ddInquiryGridEdit" class="searchHeaderEdit"></span>
      <%=widgetFactory.createHoverHelp("ddInquiryGridEdit","CustomizeListImageLinkHoverText") %>
    </span>
    <div style="clear:both;"></div>
  </div>
  <div id="copySelectedInstrument"></div>
  
  <div class="searchDivider"></div>
  

      <%


  String linkName = resMgr.getText("common.search.basic",TradePortalConstants.TEXT_BUNDLE);
  String paramStr = "&current2ndNav=I&SearchType=";
  if (searchType == null) {
    searchType = TradePortalConstants.SIMPLE;
    paramStr=paramStr+TradePortalConstants.ADVANCED;
  } else if (searchType.equals(TradePortalConstants.ADVANCED)) {
    linkName = resMgr.getText("common.search.basic",TradePortalConstants.TEXT_BUNDLE);
    paramStr=paramStr+TradePortalConstants.SIMPLE;
  } else if (searchType.equals(TradePortalConstants.SIMPLE)) {
    linkName = resMgr.getText("common.search.advanced",TradePortalConstants.TEXT_BUNDLE);
    paramStr=paramStr+TradePortalConstants.ADVANCED;
  }

  String linkHref = formMgr.getLinkAsUrl("goToDirectDebitTransactionsHome", paramStr, response);
%>

<%
TermsPartyWebBean payerParty = beanMgr.createBean(TermsPartyWebBean.class, "TermsParty");
String corporateOrgOid = userSession.getOwnerOrgOid();
String userCheck;
boolean isBankUser = true;
String payerAcctList="";
Vector availabilityChecks = new Vector(2);
//availabilityChecks.add("!deactivate_indicator");  //list view displays the deactivated account. so diplay this in the search dropdown as well
availabilityChecks.add("available_for_direct_debit");
QueryListView queryTemplateListView = (QueryListView) EJBObjectFactory
                        .createClientEJB(formMgr.getServerLocation(),
                                                        "QueryListView");
String sqlString = "select name from client_bank, users where user_oid = "
                        + userSession.getUserOid()
                        + " and p_owner_org_oid = organization_oid";

queryTemplateListView.setSQL(sqlString);
queryTemplateListView.getRecords();
if (queryTemplateListView.getRecordCount() == 0) {
        isBankUser = false;
}

if (userSession.hasSavedUserSession()) {
        isBankUser = true;
}


if (isBankUser) {
userCheck = null;
} else {
userCheck = userSession.getUserOid();
}

payerParty.loadAcctChoices(corporateOrgOid, formMgr.getServerLocation(), resMgr, availabilityChecks,
                                        userCheck);
payerAcctList = StringFunction.xssHtmlToChars(payerParty.getAttribute("acct_choices"));

DocumentHandler acctOptions = new DocumentHandler(payerAcctList, true);

String options = Dropdown.createSortedAcctOptionsOid(acctOptions, acctOid,
                                        loginLocale);
options = "<option value=\"\"></option>"+options;
%> 
    <%@ include file="/receivables/fragments/DirectDebit-TranSearch-AdvanceFilter.frag"%>

    <%@ include file="/receivables/fragments/DirectDebit-TranSearch-BasicFilter.frag"%>
<%

  
  gridHtml = dgFac.createDataGrid("ddInquiryGrid","DirectDebitHistoryDataGrid",null);
%> 
</div>

<%= gridHtml%>

<input type=hidden name=SearchType value=<%=StringFunction.xssCharsToHtml(searchType)%>> 

