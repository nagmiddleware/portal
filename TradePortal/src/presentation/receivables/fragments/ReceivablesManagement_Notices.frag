<%--
 *
 *     Copyright  � 2008                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
**********************************************************************************
                        Transactions Home Pending Tab

  Description:
    Contains HTML to create the Receivables Match Notic tab for the Receivables Management Home page

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="ReceivablesManagement_Notices.frag" %>
*******************************************************************************
--%> 

<%-- 
**************************************************************************
    Get all the search parameters 
**************************************************************************
--%>
<%@ include file="/receivables/fragments/NoticesSearchParms.frag" %>



<%-- 
**************************************************************************
   Display the search section 
**************************************************************************
--%>

<%@ include file="/receivables/fragments/NoticesSearchFilter.frag" %>

<%
  secureParms.put("UserOid", userOid);	
  secureParms.put("SecurityRights", userSecurityRights);	
   
  gridHtml = dgFactory.createDataGrid("matchNoticesGrid","MatchNoticesDataGrid", null);
  gridLayout = dgFactory.createGridLayout("matchNoticesGrid", "MatchNoticesDataGrid");
   
  //cquinton 3/26/2013 Rel PR ir#15207 workflow is always encrypted. also remove values should not be passed
   encryptedSelectedWorkflow = EncryptDecrypt.encryptStringUsingTripleDes(selectedWorkflow, userSession.getSecretKey());      
  initSearchParms="selectedWorkflow="+encryptedSelectedWorkflow+"&pending=Pending";
%>

<%-- 
**************************************************************************
   Display the List view of the Pay Remit Notices 
**************************************************************************
--%>
<%=gridHtml %>
