<%--Srinivasu_D CR#997 Rel9.3 04/04/2015 - for the ajax call to find the Matched/UnMatched invoice--%>

<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.html.*,java.math.*,
                 java.util.*, java.util.Hashtable, com.ams.tradeportal.common.*" %>



<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session">
</jsp:useBean>

<% 
   
  String payRemitInvOidsDup = request.getParameter("payRemitInvOids");
  String payRemitOidDup = request.getParameter("payRemitOid");
  //System.out.println("payRemitOidDup:"+payRemitOidDup+"\t payRemitInvOidsDup:"+payRemitInvOidsDup);
  String tobeReturned = "";

  if(null != request.getParameter("payRemitInvOids")) {	 
	  RemittanceItemUtility utilObj = new RemittanceItemUtility();
	  tobeReturned = utilObj.getInvoiceRemittanceId(payRemitInvOidsDup,payRemitOidDup,userSession);  
	
  }
  if(StringFunction.isBlank(tobeReturned)){
	  tobeReturned = "";
  }
  //System.out.println("tobeReturned::"+tobeReturned);
  response.setContentType("text/html");
  out.println(tobeReturned);
  %>
