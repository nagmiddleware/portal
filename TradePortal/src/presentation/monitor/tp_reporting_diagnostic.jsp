<%--
 *
 *     Copyright  � 2002
 *     American Management Systems, Incorporated
 *     All rights reserved
--%>
<%--
 * This page performs a test of running a servlet (dynamically generating this page),
 * accessing an EJB (QueryListView) and reading data from the database.
 *
 *
--%>
<%@ page import="com.amsinc.ecsg.web.*,
								 com.amsinc.ecsg.util.*,
								 com.amsinc.ecsg.html.*,
								 com.amsinc.ecsg.frame.*,
								 com.bo.wibean.*,
								 com.ams.tradeportal.busobj.webbean.*,
								 com.ams.tradeportal.html.*,
								 com.ams.tradeportal.common.*,
								 java.text.*,java.util.*" %>
<%
  boolean success = false;
  boolean reportingAvailable = false;
  String serverName = "";
  String appName = "";

  try
   {
%>
	<html>
	<head>
	<title>Trade Portal Reporting Diagnostic Page</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv= "expires" content = "Saturday, 07-Oct-00 16:00:00 GMT"></meta>
	</head>

	<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
	  <%
	    formMgr.init(request.getSession(true), "AmsServlet");
	  %>
	</jsp:useBean>

	<%
    try
     {
			PropertyResourceBundle portalProperties = (PropertyResourceBundle) PropertyResourceBundle.getBundle("TradePortal");
			serverName = portalProperties.getString("serverName");

			String reportsModuleAvailable = portalProperties.getString("reportsModuleAvailable");

			if(reportsModuleAvailable.equalsIgnoreCase(TradePortalConstants.INDICATOR_YES))
				reportingAvailable = true;
     }
    catch(MissingResourceException mre)
     {	// This will happen if the reportsModuleAvailable setting is not in the config file
			reportingAvailable = false;
			success=false;
     }

     if(!reportingAvailable)
      {
			 appName = "Reporting Not Configured";
			 success=false;
      }

		// Make sure formMgr is initialized
	  formMgr.init(request.getSession(true), "AmsServlet");

		WISession webiSession = null;
		WIServerImpl webiServer = null;

		//Retrieve webiServer form Application. If webiServer is null, create new instance of webiServer.
		webiServer = (com.bo.wibean.WIServerImpl)application.getAttribute("webiServer");

		if (webiServer == null)
		{
			try
			 {
				webiServer = (com.bo.wibean.WIServerImpl)
				java.beans.Beans.instantiate(getClass().getClassLoader(), "com.bo.wibean.WIServerImpl");
        application.setAttribute("webiServer", webiServer);
        webiServer.onStartPage(request,response);

        if (webiServer.isRunning())
        {
					success = true;
				}
				else
				{	// Remove the webiServer so that portal will attempt to reconnect to the reports server next time
					application.removeAttribute("webiServer");
					success = false;
				}
     }
		 catch (Exception _beanException)
		 {  // Remove the webiServer so that portal will attempt to reconnect to the reports server next time
			 application.removeAttribute("webiServer");
			 success = false;
		 }
		}
		else
		{
			webiServer.onStartPage(request,response);

			if (webiServer.isRunning())
			{
				success = true;
			}
			else
			{ // Remove the webiServer so that portal will attempt to reconnect to the reports server next time
				application.removeAttribute("webiServer");
				success = false;
			}
		}

		if (webiServer != null)
		{
			webiServer.onEndPage();
		}

	  // Because this is just a test page, kill the session
	  request.getSession(false).invalidate();

		if(reportingAvailable)
		{
			appName = "Reporting";
		}
		else
		{
			appName = "Reporting Not Configured";
		}
	 }
   catch(Exception e)
   {
		// If there were any exceptions, set to false.
		// Remove the webiServer so that portal will attempt to reconnect to the reports server next time
		application.removeAttribute("webiServer");
		success = false;
   }
   finally
    {
		 if(success && reportingAvailable)
			 {
				 %>
					<body bgcolor="#009900">
					<table width="100%" height="100%">
						<tr>
						 <td bgcolor="#009900">
				 <%
			 }
			else
			 {
				 %>
					<body bgcolor="#FF0000">
					<table width="100%" height="100%">
						<tr>
						 <td bgcolor="#FF0000">
				 <%
			 }
    }
	%>
		<div align="center"><font color="#FFFFFF" face="Arial, Helvetica, sans-serif">
		<strong><%=serverName%> &nbsp;<%=appName%></strong></font></div>
			</td>
		</tr>
	</table>
	</body>
	</html>
