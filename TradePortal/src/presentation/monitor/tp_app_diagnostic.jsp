<%--
 *
 *     Copyright  � 2002
 *     American Management Systems, Incorporated
 *     All rights reserved
--%>
<%--
 * This page performs a test of running a servlet (dynamically generating this page),
 * accessing an EJB (QueryListView) and reading data from the database.
 *
 *
--%>
<%@ page import="com.amsinc.ecsg.web.*,
								 com.amsinc.ecsg.util.*,
        				 com.amsinc.ecsg.html.*,
        				 com.amsinc.ecsg.frame.*,
        				 com.ams.tradeportal.busobj.webbean.*,
        				 com.ams.tradeportal.html.*,
	  						 com.ams.tradeportal.common.*,
	  						 java.text.*,java.util.*" %>
<%
  boolean success = false;
  try
   {
%>
	<html>
	<head>
	<title>Trade Portal Application Diagnostic Page</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv= "expires" content = "Saturday, 07-Oct-00 16:00:00 GMT"></meta>
	</head>

	<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
	  <%
	    formMgr.init(request.getSession(true), "AmsServlet");
	  %>
	</jsp:useBean>

	<%
	  // Make sure formMgr is initialized
	  formMgr.init(request.getSession(true), "AmsServlet");

		// Access an EJB so that we can run a query
		QueryListView queryListView = (QueryListView)
		EJBObjectFactory.createClientEJB(formMgr.getServerLocation(), "QueryListView");

    // Run an arbitrary query where there will always be at least one record in the result
	  queryListView.setSQL("Select * from dual", new ArrayList());

    // Retrieve the records into XML
	  queryListView.getRecords();

	  DocumentHandler usersDoc = queryListView.getXmlResultSet();

	  queryListView.remove();

    // If data was retrieved, the test was successful.
	  if( (usersDoc != null) &&
	       (usersDoc.getFragments("/ResultSetRecord").size() > 0) )
	    {
	       success = true;
	    }

	  // Because this is just a test page, kill the session
	  request.getSession(false).invalidate();
   }
   catch(Exception e)
    {
        // If there were any exceptions, set to false
        success = false;
    }
   finally
    {
       if(success)
         {
  	       %>
						<body bgcolor="#009900">
						<table width="100%" height="100%">
							<tr>
	             <td bgcolor="#009900">
           <%
         }
        else
         {
           %>
						<body bgcolor="#FF0000">
						<table width="100%" height="100%">
							<tr>
	             <td bgcolor="#FF0000">
           <%
         }
    }
	%>
		<div align="center"><font color="#FFFFFF" face="Arial, Helvetica, sans-serif">
		<strong>
	<%
		PropertyResourceBundle portalProperties = (PropertyResourceBundle) PropertyResourceBundle.getBundle("TradePortal");
		String serverName = portalProperties.getString("serverName");
	%>
	 <%=serverName %>
	 			&nbsp; App
				</strong></font></div>
			</td>
		</tr>
	</table>
	</body>
	</html>
