<%--
 *
 *     Copyright  � 2002
 *     American Management Systems, Incorporated
 *     All rights reserved
--%>
<%--
 * This page performs a test of running a servlet (dynamically generating this page),
 * accessing an EJB (QueryListView) and reading data from the database.
 *
 *
--%>

<%@ page import="com.amsinc.ecsg.frame.*, com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*, com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*, com.ams.tradeportal.html.*,
                 com.ams.tradeportal.busobj.webbean.*,java.util.*,
                 java.beans.Beans,com.businessobjects.rebean.wi.*,
		 com.crystaldecisions.sdk.framework.*,
                 com.crystaldecisions.sdk.exception.*, java.text.*" %>


<%
  boolean	success = false;
  boolean	reportingAvailable = false;
  String	serverName = "";
  String	appName = "";
  String	cmsServer = null;
  String	secAuth = null;
  String	tpMonitorUser = null;
  String	tpMonitorPass = null;
  ReportEngines reportEngines = null;
  ReportEngine 	reportEngine =  null;
  IEnterpriseSession boSession = null;

  try
  {
%>
<html>
<head>
<title>Trade Portal Reporting Diagnostic Page</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta http-equiv= "expires" content = "Saturday, 07-Oct-00 16:00:00 GMT"></meta>
</head>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
  <%
    formMgr.init(request.getSession(true), "AmsServlet");
  %>
</jsp:useBean>

<%

    	try
     	{
		PropertyResourceBundle portalProperties = (PropertyResourceBundle) PropertyResourceBundle.getBundle("TradePortal");
		serverName = portalProperties.getString("serverName");
		cmsServer = portalProperties.getString("CMSName");
		secAuth = portalProperties.getString("Authentication");
		tpMonitorUser = portalProperties.getString("tpMonitorUser");
		tpMonitorPass = portalProperties.getString("tpMonitorPass");
		
		System.out.println(" TP Monitor - CMS Server Name = "+ cmsServer);
		System.out.println(" TP Monitor - Authentication = "+ secAuth);
		System.out.println(" TP Monitor - User Name = "+ tpMonitorUser);
		
			

		String reportsModuleAvailable = portalProperties.getString("reportsModuleAvailable");

		if(reportsModuleAvailable.equalsIgnoreCase(TradePortalConstants.INDICATOR_YES))
			reportingAvailable = true;
     	}
    	catch(MissingResourceException mre)
     	{	// This will happen if the reportsModuleAvailable setting is not in the config file
		reportingAvailable = false;
		success=false;
     	}

     	if(!reportingAvailable)
      	{
		 appName = "Reporting Not Configured";
		 success=false;
      	}

	// Make sure formMgr is initialized
	formMgr.init(request.getSession(true), "AmsServlet");
	  
	
	
	reportEngine = (ReportEngine) request.getSession().getAttribute("ReportEngine");
	
	System.out.println("reportEngine object = "+reportEngine);
	  	
	if (reportEngine != null)
	{
		System.out.println("TP_Reporting_Diagnostics - reportEngine is NULL");
		
		if (reportEngine.isReady())
		{
			success = true;
		}
		else
		{
			success = false;

		}

	}
	else
	{
		System.out.println("TP_Reporting_Diagnostics - reportEngine is not NULL");
		
		ISessionMgr sessionMgr = CrystalEnterprise.getSessionMgr();
		
		
		System.out.println("sessionMgr = "+ sessionMgr);
		boSession = sessionMgr.logon(tpMonitorUser, tpMonitorPass, cmsServer, secAuth);
		System.out.println("TP_Reporting_Diagnostics - boSession "+ boSession);
		
		reportEngines = (ReportEngines)boSession.getService("ReportEngines");
		reportEngine = reportEngines.getService(ReportEngines.ReportEngineType.WI_REPORT_ENGINE);

		if (reportEngine.isReady())
		{
			success = true;
		}
		else
		{
			success = false;
		}
	}
	 
	// Because this is just a test page, kill the session
	request.getSession(false).invalidate();

	if(reportingAvailable)
	{
		appName = "Reporting";
	}
	else
	{
		appName = "Reporting Not Configured";
	}
   
   
   }
   catch(Exception e)
   {
	// If there were any exceptions, set to false.
	// Remove the webiServer so that portal will attempt to reconnect to the reports server next time
	application.removeAttribute("webiServer");
	success = false;
   }
   finally
   {
	if(success && reportingAvailable)
	{
		 %>
			<body bgcolor="#009900">
			<table width="100%" height="100%">
				<tr>
				 <td bgcolor="#009900">
		 <%
	}
	else
	{
		 %>
			<body bgcolor="#FF0000">
			<table width="100%" height="100%">
				<tr>
				 <td bgcolor="#FF0000">
		 <%
	}
	
	//Logout the BO Session
	boSession.logoff();
	boSession = null;

    }
	
%>
<div align="center"><font color="#FFFFFF" face="Arial, Helvetica, sans-serif">
<strong><%=serverName%> &nbsp;<%=appName%></strong></font></div>
	</td>
</tr>
</table>
</body>
</html>
