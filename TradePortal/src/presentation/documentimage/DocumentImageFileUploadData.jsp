<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.html.*" %>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager"
    scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean"
   scope="session">
</jsp:useBean>


<%-- ************** Data retrieval page setup begins here ****************  --%>

<%
	WidgetFactory widgetFactory = new WidgetFactory(resMgr);
   	// Set the focus to the Purchase Order File Location field
   	String onLoad = "document.forms[0].documentName0.focus();";
   	String headerTitle="";
   	headerTitle=resMgr.getText("MailMessage.DocumentImageUpload", TradePortalConstants.TEXT_BUNDLE);
%>


<%-- ********************* HTML for page begins here *********************  --%>

			<jsp:include page="/common/Header.jsp">
			   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
			   <jsp:param name="includeErrorSectionFlag"  value="<%=TradePortalConstants.INDICATOR_YES%>" />
			   <jsp:param name="additionalOnLoad"         value="<%=onLoad%>" />
			</jsp:include>


<%--
   Note: The following form MUST have the 'enctype' attribute specified as "multipart/form-data"
   in order for the contents of the file selected by the user to be uploaded with the other
   form parameter values (i.e., hidden field values, radio button values, etc.). It also must go
   through the Purchase Order File Upload Servlet, as this servlet handles all file PO upload
   requests and forwards the user to the appropriate page afterwards.
--%>

	<div class="pageMain">
		<div class="pageContent">
			<form id="DocumentImageFileUploadDataForm" name="DocumentImageFileUploadDataForm" method="POST" data-dojo-type="dijit.form.Form"
			 	enctype="multipart/form-data" action="<%= response.encodeURL(request.getContextPath()+ "/documentimage/DocumentImageFileUploadServlet.jsp") %>">

  				<div class="formArea">
  				<jsp:include page="/common/PageHeader.jsp">
                     <jsp:param name="titleKey" value="<%=headerTitle%>" />
                 </jsp:include>
					<%-- <jsp:include page="/common/ErrorSection.jsp" /> --%>

					<div class="formContent">
  						<%= widgetFactory.createTextField("documentName0", "DocumentImageFileUpload.DocumentName", "", "20", false, true) %>
  						<%= widgetFactory.createFileField("documentFileLocation0", "DocumentImageFileUpload.File","class='char40'","", false) %>
                        <%-- IR#T36000018452 Rel 8.3.0.3 - Changing type of button from 'submit' to 'button' --%>
      	  				<div id="buttonsDiv" class="formItem">
							<button data-dojo-type="dijit.form.Button"  name=<%=TradePortalConstants.BUTTON_UPLOAD_FILE%> id="UploadButton" type="button" data-dojo-props="" >
								<%=resMgr.getText("common.UploadText", TradePortalConstants.TEXT_BUNDLE)%>
								<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
									if(document.getElementById('documentName0').value != "" && document.getElementById('documentFileLocation0').value != "") {
										setButtonPressed('<%=TradePortalConstants.BUTTON_UPLOAD_FILE%>', '0');
										hideButtons();
										document.forms[0].submit();
									}else{
										if(document.getElementById('documentName0').value == ""){
											alert('<%=StringFunction.asciiToUnicode(resMgr.getText("DocumentImageFileUpload.AlertMessage",TradePortalConstants.TEXT_BUNDLE)) %>');
										}else if(document.getElementById('documentFileLocation0').value == "") {
											alert('<%=StringFunction.asciiToUnicode(resMgr.getText("DocumentImageFileUpload.AlertMessage1",TradePortalConstants.TEXT_BUNDLE)) %>');
										}
									}
	 							</script>
							</button>
							<%=widgetFactory.createHoverHelp("UploadButton","UploadHoverText") %>
              <%
                 String pageOriginator = (String)session.getAttribute("documentImageFileUploadPageOriginator");
                 // [BEGIN] IR-YAUH032237071 - jkok - By this point a new transaction should have been saved so
                 //                                   we do not want to return to the NewTransactionDirector
                 //                                   but the InstrumentNavigator instead
                 if ((pageOriginator != null) && (pageOriginator.equals("NewTransactionDirector"))) {
                    pageOriginator = "InstrumentNavigator";
                    session.setAttribute("documentImageFileUploadPageOriginator", pageOriginator);
                 }else if(("DocumentImageFileUploadData".equals(pageOriginator))){
                     pageOriginator = "InvoiceManagement";
   	                 session.setAttribute("documentImageFileUploadPageOriginator", pageOriginator);
                    }
                 // [END] IR-YAUH032237071 - jkok
                 StringBuffer newLink = new StringBuffer();
                 newLink.append(formMgr.getLinkAsUrl("goTo"+pageOriginator, response));
                 newLink.append("&useCache=true");

                 // If we don't pass the mailMessageOid in the URL and go to MailMessageDetail
                 // the user will be brought to a new mail message page with a different oid than
                 // the existing mail message that the user was working with
                  DocumentHandler defaultDoc = null;
                 if (pageOriginator.equals("MailMessageDetail")) {
	                 defaultDoc = formMgr.getFromDocCache();
	                 //Added by dillip ANZ issue 809
	                 session.setAttribute("documentImageFileUploadOid", defaultDoc.getAttribute("/Out/message_oid"));
	                 newLink.append("&mailMessageOid="
	                    +EncryptDecrypt.encryptStringUsingTripleDes(defaultDoc.getAttribute("/Out/message_oid"), userSession.getSecretKey()));
	              } else if ( "BankMailMessageDetail".equals(pageOriginator) ) {
	            	  defaultDoc = formMgr.getFromDocCache();
	            	  session.setAttribute("documentImageFileUploadOid", defaultDoc.getAttribute("/Out/bank_mail_message_oid"));
		                 newLink.append("&mailMessageOid="
		                    +EncryptDecrypt.encryptStringUsingTripleDes(defaultDoc.getAttribute("/Out/bank_mail_message_oid"), userSession.getSecretKey()));
	              }
              %>
	              			<button data-dojo-type="dijit.form.Button"  name="CancelButton" id="CancelButton" type="button" data-dojo-props="" >
								<%=resMgr.getText("common.CancelText", TradePortalConstants.TEXT_BUNDLE)%>
								<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
								openURL("<%=newLink.toString()%>");
	 						</script>
							</button>
							<%=widgetFactory.createHoverHelp("CancelButton","common.Cancel") %>
						</div>
						<br>
						<div id="UploadMessage" style="display: none;">
						    <p >
							   <font class="availItemSubHeader">
							      <%=resMgr.getText("DocumentImageFileUpload.UploadingMessage", TradePortalConstants.TEXT_BUNDLE)%>
							   </font>
							</p>
					 	</div>
					</div> <%--closes formContent area--%>
				</div> <%--closes formArea--%>
  			</form>
		</div> <%--closes pageContent area--%>
	</div> <%--closes pageMain area--%>

	<jsp:include page="/common/Footer.jsp">
	  <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
	  <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
	</jsp:include>

	</body>
</html>

<script LANGUAGE="JavaScript">
<%-- SETUP FOR BLINKING TEXT --%>
	var bName = navigator.appName;
	var bVer = parseInt(navigator.appVersion);
	var NS4 = (bName == "Netscape" && bVer >= 4);
	var IE4 = (bName == "Microsoft Internet Explorer" && bVer >= 4);
	var NS3 = (bName == "Netscape" && bVer < 4);
	var IE3 = (bName == "Microsoft Internet Explorer" && bVer < 4);
	var blink_speed=100;
	var i=0;

 	if (NS4 || IE4) {
		if (navigator.appName == "Netscape") {
			layerStyleRef="layer.";
			layerRef="document.layers";
			styleSwitch="";
		} else {
			layerStyleRef="layer.style.";
			layerRef="document.all";
			styleSwitch=".style";
		}
	}

<%-- BLINK FUNCTION--%>
	function Blink(layerName){
		if (NS4 || IE4) {
			if(i%2==0) {
				eval(layerRef+'["'+layerName+'"]'+
				styleSwitch+'.visibility="visible"');
			} else {
 				eval(layerRef+'["'+layerName+'"]'+
				styleSwitch+'.visibility="hidden"');
			}
		}
 		if(i<1) {
			i++;
		} else {
			i--
		}
 		setTimeout("Blink('"+layerName+"')",blink_speed);
	}

<%-- HIDE BUTTONS, SHOW AND BLINK TEXT--%>
     function hideButtons() {
        <%-- if (document.getElementById) { DOM3 = IE5, NS6
			document.getElementById('CancelButton').style.visibility = 'hidden';
			document.getElementById('CancelButton').style.display = 'none';
			document.getElementById('UploadButton').style.visibility = 'hidden';
			document.getElementById('UploadButton').style.display = 'none';
			document.getElementById('UploadMessage').style.visibility = 'visible';
			document.getElementById('UploadMessage').style.display = 'block';
			Blink('UploadMessage');
		} else {
			if (document.layers) { Netscape 4
				document.CancelButton.visibility = 'hidden';
				document.CancelButton.display = 'none';
				document.UploadButton.visibility = 'hidden';
				document.UploadButton.display = 'none';
				document.UploadMessage.visibility = 'visible';
				document.UploadMessage.display = 'block';
				Blink('UploadMessage');
			} else { IE 4
				document.all.CancelButton.style.visibility = 'hidden';
				document.all.CancelButton.style.display = 'none';
				document.all.UploadButton.style.visibility = 'hidden';
				document.all.UploadButton.style.display = 'none';
				document.all.UploadMessage.style.visibility = 'visible';
				document.all.UploadMessage.style.display = 'block';
				Blink('UploadMessage');
			}
		} --%>
		document.getElementById('buttonsDiv').style.display = 'none';
		document.getElementById('UploadMessage').style.display = 'block';
	 }

function openURL(URL){
    if (isActive =='Y') {
    	if (URL != '' && URL.indexOf("javascript:") == -1) {
	        var cTime = (new Date()).getTime();
	        URL = URL + "&cTime=" + cTime;
	        URL = URL + "&prevPage=" + context;
    	}
    }
	 document.location.href  = URL;
	 return false;
}
</script>
