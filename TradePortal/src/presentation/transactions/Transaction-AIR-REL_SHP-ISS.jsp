
	<%--
	*******************************************************************************
	Air Waybill Release and Shipping Guarantee Issue Page

	Description:
	This is the main driver for the Air Waybill Release and Shipping Guarantee
	Issue page.  It handles data retrieval of terms and terms parties (or
	retrieval from the input document) and creates the html page to display all
	the data for an Air Waybill or Shipping Guarantee.
	*******************************************************************************
	--%>

	<%--
	*
	*     Copyright   2001
	*     American Management Systems, Incorporated
	*     All rights reserved
	--%>
	<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
	com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
	com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
	com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.html.*,java.math.*,java.util.*,
	com.amsinc.ecsg.util.DateTimeUtility" %>
	<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager"
	scope="session">
	</jsp:useBean>

	<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
	</jsp:useBean>

	<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
	</jsp:useBean>

	<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean"
	scope="session">
	</jsp:useBean>

	<%-- ************** Data retrieval page setup begins here ****************  --%>

	<%
		WidgetFactory widgetFactory = new WidgetFactory(resMgr);	 
		String  onLoad     = "";
		String PartySearchAddressTitle =resMgr.getTextEscapedJS("PartySearch.TabHeading",TradePortalConstants.TEXT_BUNDLE);
		String parentOrgID      = userSession.getOwnerOrgOid();
		String bogID            = userSession.getBogOid();
		String clientBankID     = userSession.getClientBankOid();
		String globalID         = userSession.getGlobalOrgOid();
		String ownershipLevel   = userSession.getOwnershipLevel();
		String  focusField = "FreightForwarderName";   // Default focus field
		boolean focusSet   = false;
		/* Naveen 11-Oct-2012 IR-T36000006190. */
		String instrumentSearchDialogTitle=resMgr.getTextEscapedJS("InstSearch.HeadingGeneric",TradePortalConstants.TEXT_BUNDLE);
		// Various oid and status info from transaction and instruments used in
		// several places.
		String instrumentOid = "";
		String transactionOid;
		String instrumentType;
		String instrumentStatus;
		String transactionType;
		String transactionStatus;
		String rejectionIndicator        = "";
		String rejectionReasonText       = "";
		
		String contactFax;
		String firstFax;
		String secondFax;
		boolean           corpOrgHasMultipleAddresses   = false;
		String            corpOrgOid                    = null;
		String links ="";
		boolean canDisplayPDFLinks = false;
		
		// Variable for the Related Instrument Id field in case we're coming back from the Instrument Search page
		String instrumentId = null;

		boolean getDataFromDoc;          // Indicates if data is retrieved from the
		   // input doc cache or from the database
		boolean phraseSelected = false;  // Indicates if a phrase was selected.

		DocumentHandler doc;

		String loginLocale = userSession.getUserLocale();
		String loginRights = userSession.getSecurityRights();
		String buttonPressed;


		// Variables used for populating ref data dropdowns.
		String options;
		String defaultText;

  //for some debugging if necessary
  //String newPartyType = "";
  //String newPartyOid = "";

		// These are the beans used on the page.

		TransactionWebBean transaction  = (TransactionWebBean)
			  beanMgr.getBean("Transaction");
		InstrumentWebBean instrument    = (InstrumentWebBean)
			  beanMgr.getBean("Instrument");
		TemplateWebBean template        = (TemplateWebBean)
			  beanMgr.getBean("Template");
		TermsWebBean terms              = null;

		TermsPartyWebBean termsPartyFreightForwarder = beanMgr.createBean(TermsPartyWebBean.class, "TermsParty");
		TermsPartyWebBean termsPartyApplicant = beanMgr.createBean(TermsPartyWebBean.class, "TermsParty");
		TermsPartyWebBean termsPartyReleaseToParty = beanMgr.createBean(TermsPartyWebBean.class, "TermsParty");
		ShipmentTermsWebBean shipmentTerms = beanMgr.createBean(ShipmentTermsWebBean.class, "ShipmentTerms");

		// Get the document from the cache.  We'll may use it to repopulate the
		// screen if returning from another page, a save, validation, or any other
		// mediator called from this page.  Otherwise, we assume an instrument oid
		// and transaction oid was passed in.

		doc = formMgr.getFromDocCache();
		buttonPressed = doc.getAttribute("/In/Update/ButtonPressed");
		Vector error = null;
		error = doc.getFragments("/Error/errorlist/error");

		//Debug.debug("doc from cache is " + doc.toString());

		/******************************************************************************
		We are either entering the page or returning from somewhere.  These are the
		conditions for how to populate the web beans.  Data comes from either the
		database or the doc cache (/In section) with some variation.

		Mode           Condition                      Populate Beans From
		-------------  ----------------------------   --------------------------------
		Enter Page     no /In/Transaction in doc      Instrument and Template web
						beans already populated, get
						data for Terms and TermsParty
						web beans from database

	 return from    /In/NewPartyFromSearchInfo/    doc cache (/In); also use
	 New Party        PartyOid exists              NewPartyFromSearchInfo to lookup, and 
	 on PartySearch                                populate a specific TermsParty
	 dialog                                        web bean

		return from    /Out/PhraseLookupInfo exists   doc cache (/In); but use Phrase
		Phrase Lookup                                 LookupInfo text (from /Out) to
						replace a specific phrase text
						in the /In document before
						populating

		return from    /In/AddressSearchInfo/Address  doc cache (/In); use /In/Address
		Address Search   SearchPartyType exists       SearchInfo to populate a specific
						TermsParty web bean

		return from    /Error/maxerrorseverity < 1    Same as Enter Page (data is
		Transaction                                   retrieved from database)
		mediator
		(no error)

		return from    /Error/maxerrorseverity > 0    doc cache (/In)
		Transaction
		mediator
		(error)
		******************************************************************************/

		// Assume we get the data from the doc.
		getDataFromDoc = true;

		String maxError = doc.getAttribute("/Error/maxerrorseverity");
		if (maxError != null && maxError.compareTo(String.valueOf(ErrorManager.WARNING_ERROR_SEVERITY)) <= 0)
		{
			// No errors, so don't get the data from doc.
			getDataFromDoc = false;
		}
		//ir cnuk113043991 - check to see if transaction needs to be refreshed
		// if so, refresh it and do not get data from doc as it is wrong
		if ( "Y".equals(doc.getAttribute("/Out/Refresh/Transaction")) ) 
		{
			transaction.getDataFromAppServer();
			getDataFromDoc = false;
		}
		if (doc.getDocumentNode("/In/NewPartyFromSearchInfo/Type") != null)
		{
			// We have returned from the party search page.  Get data from doc cache
			getDataFromDoc = true;
		}
		if (doc.getDocumentNode("/In/Transaction") == null)
		{
			// No /In/Transaction means we've never looked up the data.
			Debug.debug("No /In/Transaction section - get data from database");
			getDataFromDoc = false;
		}
		if (doc.getDocumentNode("/In/AddressSearchInfo/AddressSearchPartyType") != null) 
		{
			// We have returned from the address search page.  Get data from doc cache
			getDataFromDoc = true;
		}
		if (doc.getDocumentNode("/Out/PhraseLookupInfo") != null)
		{
			// A Looked up phrase exists.  Replace it in the /In document.
			Debug.debug("Found a looked-up phrase");
			getDataFromDoc = true;
			phraseSelected = true;

			String result = doc.getAttribute("/Out/PhraseLookupInfo/Result");
			if (result.equals(TradePortalConstants.INDICATOR_YES))
			{
				// Take the looked up and appended phrase text from the /Out section
				// and copy it to the /In section.  This allows the beans to be
				// properly populated.
				String xmlPath = doc.getAttribute("/In/PhraseLookupInfo/text_path");
				xmlPath = "/In" + xmlPath;

				doc.setAttribute(xmlPath, doc.getAttribute("/Out/PhraseLookupInfo/NewText"));

				// If we returned from the phrase lookup without errors, set the focus
				// to the correct field.  Otherwise, don't set the focus so the user can
				// see the error.
				if (maxError != null && maxError.compareTo(String.valueOf(ErrorManager.WARNING_ERROR_SEVERITY)) < 0)
				{
					focusField = doc.getAttribute("/In/PhraseLookupInfo/text_field_name");
					focusSet = true;
				}
			}
			else
			{
				// We do nothing because nothing was looked up.  We stil want to get
				// the data from the doc.
			}
		}

		//ctq - if newTransaction, set session variable so it persists
		String newTransaction = null;
		if (doc.getDocumentNode("/Out/newTransaction")!=null) 
		{
			newTransaction = doc.getAttribute("/Out/newTransaction");
			session.setAttribute("newTransaction", newTransaction);
			System.out.println("found newTransaction = "+ newTransaction);
		} 
		else
		{
			newTransaction = (String) session.getAttribute("newTransaction");
			if ( newTransaction==null ) 
			{
				newTransaction = TradePortalConstants.INDICATOR_NO;
			}
			System.out.println("used newTransaction from session = " + newTransaction);
		}

                //cquinton 1/18/2013 remove old close action behavior
	  	
		if (getDataFromDoc)
		{
			Debug.debug("Populating beans from doc cache");

			// Populate the beans from the input doc.
			try
			{
				instrument.populateFromXmlDoc(doc.getComponent("/In"));
				transaction.populateFromXmlDoc(doc.getComponent("/In"));
				template.populateFromXmlDoc(doc.getComponent("/In"));

				terms = (TermsWebBean) beanMgr.getBean("Terms");
				terms.populateFromXmlDoc(doc, "/In");

				String termsPartyOid;

				termsPartyOid = terms.getAttribute("c_FirstTermsParty");
				if (termsPartyOid != null && !termsPartyOid.equals(""))
				{
					termsPartyFreightForwarder.setAttribute("terms_party_oid", termsPartyOid);
					termsPartyFreightForwarder.getDataFromAppServer();
				}
				termsPartyOid = terms.getAttribute("c_SecondTermsParty");
				if (termsPartyOid != null && !termsPartyOid.equals(""))
				{
					termsPartyApplicant.setAttribute("terms_party_oid", termsPartyOid);
					termsPartyApplicant.getDataFromAppServer();
				}
				termsPartyOid = terms.getAttribute("c_ThirdTermsParty");
				if (termsPartyOid != null && !termsPartyOid.equals(""))
				{
					termsPartyReleaseToParty.setAttribute("terms_party_oid", termsPartyOid);
					termsPartyReleaseToParty.getDataFromAppServer();
				}

				DocumentHandler termsPartyDoc;

				termsPartyDoc = doc.getFragment("/In/Terms/FirstTermsParty");
				termsPartyFreightForwarder.loadTermsPartyFromDocTagsOnly(termsPartyDoc);

				termsPartyDoc = doc.getFragment("/In/Terms/SecondTermsParty");
				termsPartyApplicant.loadTermsPartyFromDocTagsOnly(termsPartyDoc);

				termsPartyDoc = doc.getFragment("/In/Terms/ThirdTermsParty");
				termsPartyReleaseToParty.loadTermsPartyFromDocTagsOnly(termsPartyDoc);

				// Some of the data for this transaction is stored as part of the shipment terms
				terms.populateFirstShipmentFromXml(doc);
			}
			catch (Exception e)
			{
				out.println("Contact Administrator: "
				+ "Unable to reload data after returning to page. "
				+ "Error is " + e.toString());
			}
		}
		else
		{
			Debug.debug("populating beans from database");
			// We will perform a retrieval from the database.
			// Instrument and Transaction were already retrieved.  Get
			// the rest of the data

			terms = transaction.registerCustomerEnteredTerms();

			String termsPartyOid;

			termsPartyOid = terms.getAttribute("c_FirstTermsParty");
			if (termsPartyOid != null && !termsPartyOid.equals(""))
			{
				termsPartyFreightForwarder.setAttribute("terms_party_oid", termsPartyOid);
				termsPartyFreightForwarder.getDataFromAppServer();
			}
			termsPartyOid = terms.getAttribute("c_SecondTermsParty");
			if (termsPartyOid != null && !termsPartyOid.equals(""))
			{
				termsPartyApplicant.setAttribute("terms_party_oid", termsPartyOid);
				termsPartyApplicant.getDataFromAppServer();
			}
			termsPartyOid = terms.getAttribute("c_ThirdTermsParty");
			if (termsPartyOid != null && !termsPartyOid.equals(""))
			{
				termsPartyReleaseToParty.setAttribute("terms_party_oid", termsPartyOid);
				termsPartyReleaseToParty.getDataFromAppServer();
			}

			// Some of the data for this transaction is stored as part of the shipment terms
			terms.populateFirstShipmentFromDatabase();
		}

		/*************************************************
		* Load New Party
		**************************************************/
		if (doc.getDocumentNode("/In/NewPartyFromSearchInfo/Type") != null &&
		    doc.getDocumentNode("/In/NewPartyFromSearchOutput/PartyOid") != null ) {

			// We have returned from the NewPartyDetail.  Based on the returned
			// data, RELOAD one of the terms party beans with the selected party

			String termsPartyType = doc.getAttribute("/In/NewPartyFromSearchInfo/Type");
			String partyOid = doc.getAttribute("/In/NewPartyFromSearchOutput/PartyOid");
			//newPartyType = termsPartyType;
			//newPartyOid = partyOid;

			Debug.debug("Returning from party search with " + termsPartyType + "/" + partyOid);

			// Use a Party web bean to get the party data for the selected oid.
			PartyWebBean party = beanMgr.createBean(PartyWebBean.class, "Party");
			party.setAttribute("party_oid", partyOid);
			party.getDataFromAppServer();

			DocumentHandler partyDoc = new DocumentHandler();
			party.populateXmlDoc(partyDoc);

			partyDoc = partyDoc.getFragment("/Party");

			// Based on the party type being returned (which we previously set)
			// reload one of the terms party web beans with the data from the
			// doc.
			if (termsPartyType.equals(TradePortalConstants.FREIGHT_FORWARD)) 
			{
				termsPartyFreightForwarder.loadTermsPartyFromDoc(partyDoc);

				// GGAYLE - IR EZUD102457535 - 02/13/2004
				// Set the attention_of field and then determine which fax number to return to the transaction.
				terms.setAttribute("attention_of", party.getAttribute("contact_name"));

				contactFax = party.getAttribute("contact_fax");
				firstFax = party.getAttribute("fax_1");
				secondFax = party.getAttribute("fax_2");

				if (contactFax != null && !contactFax.equals(""))
				{
					terms.setAttribute("fax_number", contactFax);
				}
				else
				{
					if (firstFax != null && !firstFax.equals(""))
					{
					terms.setAttribute("fax_number", firstFax);
					}
					else
					{
						if (secondFax != null && !secondFax.equals(""))
						{
							terms.setAttribute("fax_number", secondFax);
						}
						else
						{
							terms.setAttribute("fax_number", "");
						}
					}
				}	
			// GGAYLE - IR EZUD102457535 - 01/27/2004

				focusField = "FreightForwarderName";
				focusSet = true;
			}
			if (termsPartyType.equals(TradePortalConstants.APPLICANT)) 
			{
				termsPartyApplicant.loadTermsPartyFromDoc(partyDoc);

				//rbhaduri - 13th June 06 - IR SAUG051361872
				termsPartyApplicant.setAttribute("address_search_indicator", "N");

				focusField = "";
				focusSet = true;
				onLoad += "location.hash='#" + termsPartyType + "';";
			}
			if (termsPartyType.equals(TradePortalConstants.RELEASE_TO_PARTY)) {
			termsPartyReleaseToParty.loadTermsPartyFromDoc(partyDoc);
			focusField = "ReleaseToPartyName";
			focusSet = true;
			}
		}


		//rbhaduri - 8th August 06 - IR AOUG100368306 - Add Begin - Moved it here from below
		instrumentType = instrument.getAttribute("instrument_type_code");

		BigInteger requestedSecurityRight = null;

		// Now determine the mode for how the page operates (readonly, etc.)
		if (instrumentType.equals(InstrumentType.AIR_WAYBILL))
		{
			requestedSecurityRight = SecurityAccess.AIR_WAYBILL_CREATE_MODIFY;
		}
		else
		{
			requestedSecurityRight = SecurityAccess.SHIP_GUAR_CREATE_MODIFY;
		}
		//rbhaduri - 8th August 06 - IR AOUG100368306 - Add End

	%>
		<%-- //rbhaduri - 8th August 06 - IR AOUG100368306 - Moved it here from below --%>
	<%@ include file="fragments/Transaction-PageMode.frag" %>
	<%
		//rbhaduri - 8th August 06 - IR AOUG100368306 - Moved the code here from below
		if (isTemplate) 
			corpOrgOid = template.getAttribute("owner_org_oid");
		else
			corpOrgOid = instrument.getAttribute("corp_org_oid");
	
	 //MEer Rel 9.3.5  CR-1027
	CorporateOrganizationWebBean corporateOrg = beanMgr.createBean(CorporateOrganizationWebBean.class, "CorporateOrganization");
	corporateOrg.getById(corpOrgOid);

    String bankOrgGroupOid = corporateOrg.getAttribute("bank_org_group_oid");
    BankOrganizationGroupWebBean bankOrganizationGroup = beanMgr.createBean(BankOrganizationGroupWebBean.class,  "BankOrganizationGroup");
    bankOrganizationGroup.getById(bankOrgGroupOid);
    String selectedPDFType = "";
    if(InstrumentType.AIR_WAYBILL.equals(instrumentType)){
     	selectedPDFType =  bankOrganizationGroup.getAttribute("airway_bill_pdf_type");
    }else if (InstrumentType.SHIP_GUAR.equals(instrumentType)){
	   selectedPDFType = bankOrganizationGroup.getAttribute("shp_pdf_type");
    }
   
   if(TradePortalConstants.APPLICATION_PDF_EXPANDED_TYPE.equals(selectedPDFType)){
   	canDisplayPDFLinks = true;
   } 
   
		//jgadela  R90 IR T36000026319 - SQL FIX
		Object sqlParamsCo[] = new Object[1];
		sqlParamsCo[0] = corpOrgOid;

		if (DatabaseQueryBean.getCount("address_oid", "address", "p_corp_org_oid = ?", true, sqlParamsCo) > 0 )
		{
			corpOrgHasMultipleAddresses = true;
		}  
		/*************************************************
		* Load address search info if we come back from address search page.
		* Reload applicant terms party web beans with the data from 
		* the address
		**************************************************/
		if (doc.getDocumentNode("/In/AddressSearchInfo/AddressSearchPartyType") != null)
		{
			String termsPartyType = doc.getAttribute("/In/AddressSearchInfo/AddressSearchPartyType");
			String addressOid = doc.getAttribute("/In/AddressSearchInfo/AddressOid");

			//rbhaduri - 9th Oct 06 - IR AOUG100368306
			String primarySelected = doc.getAttribute("/In/AddressSearchInfo/ChoosePrimary");

			Debug.debug("Returning from address search with " + termsPartyType 
			+ "/" + addressOid);

			// Use a Party web bean to get the party data for the selected oid.
			AddressWebBean address = null;  
			//rbhaduri - 9th Oct 06 - IR AOUG100368306 - added PRIMARY case for primary address selection
			if ((addressOid != null||primarySelected != null) && termsPartyType.equals(TradePortalConstants.APPLICANT)) 
			{

				if (primarySelected.equals("PRIMARY")) 
				{
					termsPartyApplicant.setAttribute("name",corporateOrg.getAttribute("name"));
					termsPartyApplicant.setAttribute("address_line_1",corporateOrg.getAttribute("address_line_1"));
					termsPartyApplicant.setAttribute("address_line_2",corporateOrg.getAttribute("address_line_2"));
					termsPartyApplicant.setAttribute("address_city",corporateOrg.getAttribute("address_city"));
					termsPartyApplicant.setAttribute("address_state_province",corporateOrg.getAttribute("address_state_province"));
					termsPartyApplicant.setAttribute("address_country",corporateOrg.getAttribute("address_country"));
					termsPartyApplicant.setAttribute("address_postal_code",corporateOrg.getAttribute("address_postal_code"));
					termsPartyApplicant.setAttribute("address_seq_num","1");
					//Rel9.5 CR1132 Populate userdefinedfields from corporate
					termsPartyApplicant.setAttribute("user_defined_field_1", corporateOrg.getAttribute("user_defined_field_1"));
					termsPartyApplicant.setAttribute("user_defined_field_2", corporateOrg.getAttribute("user_defined_field_2"));
					termsPartyApplicant.setAttribute("user_defined_field_3", corporateOrg.getAttribute("user_defined_field_3"));
					termsPartyApplicant.setAttribute("user_defined_field_4", corporateOrg.getAttribute("user_defined_field_4"));
				}
				else 
				{
					address = beanMgr.createBean(AddressWebBean.class, "Address");
					address.setAttribute("address_oid", addressOid);
					address.getDataFromAppServer();
					termsPartyApplicant.setAttribute("name",address.getAttribute("name"));
					termsPartyApplicant.setAttribute("address_line_1",address.getAttribute("address_line_1"));
					termsPartyApplicant.setAttribute("address_line_2",address.getAttribute("address_line_2"));
					termsPartyApplicant.setAttribute("address_city",address.getAttribute("city"));
					termsPartyApplicant.setAttribute("address_state_province",address.getAttribute("state_province"));
					termsPartyApplicant.setAttribute("address_country",address.getAttribute("country"));
					termsPartyApplicant.setAttribute("address_postal_code",address.getAttribute("postal_code"));
					termsPartyApplicant.setAttribute("address_seq_num",address.getAttribute("address_seq_num"));
					//Rel9.5 CR1132 Populate userdefinedfields from address
					termsPartyApplicant.setAttribute("user_defined_field_1", address.getAttribute("user_defined_field_1"));
					termsPartyApplicant.setAttribute("user_defined_field_2", address.getAttribute("user_defined_field_2"));
					termsPartyApplicant.setAttribute("user_defined_field_3", address.getAttribute("user_defined_field_3"));
					termsPartyApplicant.setAttribute("user_defined_field_4", address.getAttribute("user_defined_field_4"));
				}
			}  
			focusField = "";
			focusSet = true;
			onLoad += "location.hash='#" + termsPartyType + "';";

		}


		transactionType = transaction.getAttribute("transaction_type_code");
		transactionStatus = transaction.getAttribute("transaction_status");

		// Get the transaction rejection indicator to determine whether to show the rejection reason text
		rejectionIndicator = transaction.getAttribute("rejection_indicator");
		rejectionReasonText = transaction.getAttribute("rejection_reason_text");

		instrumentType = instrument.getAttribute("instrument_type_code");
		instrumentStatus = instrument.getAttribute("instrument_status");

		Debug.debug("Instrument Type " + instrumentType);
		Debug.debug("Instrument Status " + instrumentStatus);
		Debug.debug("Transaction Type " + transactionType);
		Debug.debug("Transaction Status " + transactionStatus);

		// Now determine the mode for how the page operates (readonly, etc.)
		//rbhaduri - 8th August 06 - IR AOUG100368306 - commented out the code below and Moved above
		/* BigInteger requestedSecurityRight = null;

		// Now determine the mode for how the page operates (readonly, etc.)
		if (instrumentType.equals(InstrumentType.AIR_WAYBILL))
		{
		requestedSecurityRight = SecurityAccess.AIR_WAYBILL_CREATE_MODIFY;
		}
		else
		{
		requestedSecurityRight = SecurityAccess.SHIP_GUAR_CREATE_MODIFY;
		}
		*/
		if (doc.getDocumentNode("/In/InstrumentSearchInfo/InstrumentData") != null)
		{
			// We have returned from the instrument search page.  Based on the returned
			// data, retrieve the complete instrument ID of the selected instrument to
			// populate the Related Instrument ID in the General section
			String instrumentData = doc.getAttribute("/In/InstrumentSearchInfo/InstrumentData");
			instrumentId = InstrumentServices.parseForInstrumentId(instrumentData);
		}


	%>
	<%
		// Create documents for the phrase dropdowns. First check the cache (no need
		// to recreate if they already exist).  After creating, place in the cache.
		// (InstrumentCloseNavigator.jsp cleans these up.)

		DocumentHandler phraseLists = formMgr.getFromDocCache(TradePortalConstants.PHRASE_LIST_DOC);
		if (phraseLists == null)
		{
			phraseLists = new DocumentHandler();
		}
		DocumentHandler goodsDocList = phraseLists.getFragment("/Goods");
		if (goodsDocList == null)
		{
			goodsDocList = PhraseUtility.createPhraseList(TradePortalConstants.PHRASE_CAT_GOODS,userSession, formMgr, resMgr);
			phraseLists.addComponent("/Goods", goodsDocList);
		}

		DocumentHandler spclInstrDocList = phraseLists.getFragment("/SpclInstr");
		if (spclInstrDocList == null)
		{
			spclInstrDocList = PhraseUtility.createPhraseList(TradePortalConstants.PHRASE_CAT_SPCL_INST,userSession, formMgr, resMgr);
			phraseLists.addComponent("/SpclInstr", spclInstrDocList);
		}

		formMgr.storeInDocCache(TradePortalConstants.PHRASE_LIST_DOC, phraseLists);

%>

	<%-- ********************* HTML for page begins here ********************* --%>
	<%-- Body tag included as part of common header --%>
	<%
		// The navigation bar is only shown when editing templates.  For transactions
		// it is not shown ti minimize the chance of leaving the page without properly
		// unlocking the transaction.
		//ctq - always show the header
		String showNavBar = TradePortalConstants.INDICATOR_YES;
		// Auto save the form when time-out if not readonly.
		// (Header.jsp will check for auto-save setting of the corporate org).
		String autoSaveFlag = isReadOnly ? TradePortalConstants.INDICATOR_NO : TradePortalConstants.INDICATOR_YES;
		String templateFlag = isTemplate ? TradePortalConstants.INDICATOR_YES : TradePortalConstants.INDICATOR_NO;
	%>
	<%//cr498 begin
		//Include ReAuthentication frag in case re-authentication is required for
		//authorization of transactions for this client
			String certAuthURL = "";
			Cache reCertCache = (Cache)TPCacheManager.getInstance().getCache(TradePortalConstants.CLIENT_BANK_CACHE);
			DocumentHandler CBCResult = (DocumentHandler)reCertCache.get(userSession.getClientBankOid());	
			String requireTranAuth = CBCResult.getAttribute("/ResultSetRecord(0)/REQUIRE_TRAN_AUTH");
			boolean requireAuth = false;
		if (instrumentType.equals(InstrumentType.AIR_WAYBILL))
		{
			requireAuth = InstrumentAuthentication.requireTransactionAuthentication(
			requireTranAuth,InstrumentAuthentication.TRAN_AUTH__AIR_REL);
		} 
		else
		{
			requireAuth = InstrumentAuthentication.requireTransactionAuthentication(
			requireTranAuth,InstrumentAuthentication.TRAN_AUTH__SHP_ISS);
		}
		
	%>
	<%String pageTitleKey="";
	if ( TradePortalConstants.INDICATOR_YES.equals(newTransaction) ) {
	    pageTitleKey = "SecondaryNavigation.NewInstruments";
	    
	    if (isTemplate){
	         if(userSession.getSecurityType().equals(TradePortalConstants.ADMIN)){
	         userSession.setCurrentPrimaryNavigation("AdminNavigationBar.RefData");
	    }else{
	       userSession.setCurrentPrimaryNavigation("NavigationBar.RefData");
	    }
	    }else	         
	    	userSession.setCurrentPrimaryNavigation("NewInstrumentsMenu.Trade.AirWaybill");
	} else {
	    pageTitleKey = "SecondaryNavigation.Instruments";
	    if (isTemplate){
	         if(userSession.getSecurityType().equals(TradePortalConstants.ADMIN)){
	         userSession.setCurrentPrimaryNavigation("AdminNavigationBar.RefData");
	    }else{
	       userSession.setCurrentPrimaryNavigation("NavigationBar.RefData");
	    }
	    }else 
	         userSession.setCurrentPrimaryNavigation("NavigationBar.Transactions");
	}
	String helpUrl; 

  //cquinton 11/19/2012 pass info for recent instruments into header
  String newRecentTransactionOid = transaction.getAttribute("transaction_oid");
  String newRecentInstrumentId = instrument.getAttribute("complete_instrument_id");
  String newRecentTransactionType = transaction.getAttribute("transaction_type_code");
  String newRecentTransactionStatus = transaction.getAttribute("transaction_status");
%>

<jsp:include page="/common/Header.jsp">
  <jsp:param name="autoSaveFlag"           value="<%=autoSaveFlag%>" />
  <jsp:param name="templateFlag"           value="<%=templateFlag%>" />
  <jsp:param name="newRecentTransactionOid" value="<%=newRecentTransactionOid%>" />
  <jsp:param name="newRecentInstrumentId"  value="<%=newRecentInstrumentId%>" />
  <jsp:param name="newRecentTransactionType" value="<%=newRecentTransactionType%>" />
  <jsp:param name="newRecentTransactionStatus" value="<%=newRecentTransactionStatus%>" />
</jsp:include>

<%
if (requireAuth)
{
%>
<%@ include file="/logon/fragments/openReauthenticationWindow.frag" %>
<%
}
%>


	<div class="pageMain"><%--PageMainDiv--%>
		<div class="pageContent"><%--PageContentDiv--%>
		<%
			
			String item1Key;
			if (instrumentType.equals(InstrumentType.AIR_WAYBILL)) 
			{
				helpUrl = "customer/issue_air_waybill.htm";
				item1Key = "SecondaryNavigation.Instruments.AirWaybill";
			}
			else
			{
				helpUrl = "customer/issue_shipping_guar.htm";
				item1Key = "SecondaryNavigation.Instruments.ShippingGuarantee";
			}
		%>
		<%
		  if(isTemplate) {
		  String pageTitle;
		  String titleName;
		  String returnAction;
		  ReferenceDataManager refData = ReferenceDataManager.getRefDataMgr();		
		  pageTitle = resMgr.getText("common.template", TradePortalConstants.TEXT_BUNDLE);
		  titleName = template.getAttribute("name");
		  StringBuffer title = new StringBuffer();
		  String itemKey = "";
		  title.append(pageTitle);
		  title.append( " : " );
		  if(instrument.getAttribute("instrument_type_code").equals("SLC")){
		  if(transaction.getAttribute("standby_using_guarantee_form").equals(TradePortalConstants.INDICATOR_NO)){     
				  itemKey = resMgr.getText("SecondaryNavigation.Instruments.OutgoingSTandbyLCSimple", TradePortalConstants.TEXT_BUNDLE); 
		  }else{
				  itemKey = resMgr.getText("SecondaryNavigation.Instruments.OutgoingStandbyLCDetailed", TradePortalConstants.TEXT_BUNDLE);
		  }
		  title.append(itemKey);
		  }
		  else
		  title.append( refData.getDescr(TradePortalConstants.INSTRUMENT_TYPE, 
		                 instrument.getAttribute("instrument_type_code"), 
		                 loginLocale) );
		  title.append( " - " );
		  title.append(titleName );
		  returnAction = "goToInstrumentCloseNavigator";
		
		  String transactionSubHeader = title.toString();
  		
  		%>
		<jsp:include page="/common/PageHeader.jsp">
				<jsp:param name="titleKey" value="<%= pageTitle%>"/>
				<jsp:param name="helpUrl"  value="<%=helpUrl%>" />
  		</jsp:include>
  		
  		<jsp:include page="/common/PageSubHeader.jsp">
 				 <jsp:param name="titleKey" value="<%= transactionSubHeader%>" />
  				 <jsp:param name="returnUrl" value="<%= formMgr.getLinkAsUrl(returnAction, response) %>" />
		</jsp:include> 		
		 <%}else{ %>
			<jsp:include page="/common/PageHeader.jsp">
				<jsp:param name="titleKey" value="<%=pageTitleKey%>" />
				<jsp:param name="item1Key" value="<%=item1Key%>" />
				<jsp:param name="helpUrl"  value="<%=helpUrl%>" />
			</jsp:include>
			<jsp:include page="/common/TransactionSubHeader.jsp" />
		 <%} %>	
			<form id = "Transaction-AIR_SHP-Form" name="Transaction-AIR_SHP-Form" method="post" data-dojo-type="dijit.form.Form" action="<%=formMgr.getSubmitAction(response)%>">
				<input type=hidden value="" name="buttonName">
				<%-- error section goes above form content --%>
				<div class="formArea"><%--FormArea--%>
					<jsp:include page="/common/ErrorSection.jsp" />
					<div class="formContent"><%--FormContent--%>
					<% //cr498 begin
					if (requireAuth) 
					{
					%> 
						<input type=hidden name="reCertification" value="<%=TradePortalConstants.INDICATOR_NO%>">
						<input type=hidden name="reCertOK">
						<input type=hidden name="logonResponse">
						<input type=hidden name="logonCertificate">
					<%
					} //cr498 end
					%> 
					<%
						// Store values such as the userid, security rights, and org in a
						// secure hashtable for the form.  Also store instrument and transaction
						// data that must be secured.
						Hashtable secureParms = new Hashtable();

						secureParms.put("login_oid",             userSession.getUserOid());
						secureParms.put("owner_org_oid",         userSession.getOwnerOrgOid());
						secureParms.put("login_rights",          loginRights);

						secureParms.put("instrument_oid",        instrument.getAttribute("instrument_oid"));
						secureParms.put("instrument_type_code",  instrumentType);
						secureParms.put("instrument_status",     instrumentStatus);
						secureParms.put("corp_org_oid",  corpOrgOid);

						secureParms.put("transaction_oid",       transaction.getAttribute("transaction_oid"));
						secureParms.put("transaction_type_code", transactionType);
						secureParms.put("transaction_status",    transactionStatus);

						secureParms.put("ShipmentOid", terms.getFirstShipment().getAttribute("shipment_oid"));

						secureParms.put("transaction_instrument_info",
						transaction.getAttribute("transaction_oid") + "/" +
						instrument.getAttribute("instrument_oid") + "/" +
						transactionType);

						// If the terms record doesn't exist, set its oid to 0.
						String termsOid = terms.getAttribute("terms_oid");
						if (termsOid == null)
						{
							termsOid = "0";
						}

						secureParms.put("terms_oid", termsOid);

						if (isTemplate)
						{
							secureParms.put("template_oid", template.getAttribute("template_oid"));
							secureParms.put("opt_lock", template.getAttribute("opt_lock"));
						}
					%>
					<%-- <%@include file="fragments/MultiPartModeFormElements.frag" %>--%>
					<% 
						if (isTemplate)
						{
					%>
						<%@ include file="fragments/TemplateHeader.frag" %>    
					<%
						} 
						String extraPartTags = request.getParameter("extraPartTags");

							if (rejectionIndicator.equals(TradePortalConstants.INDICATOR_YES)
							|| rejectionIndicator.equals(TradePortalConstants.INDICATOR_X))
						{
					%>
					<%=widgetFactory.createSectionHeader("0", "Rejection Reason") %>
					<%@ include file="fragments/Transaction-RejectionReason.frag" %>
					</div>
					<%
						}
					%>
					<% // [BEGIN] IR-YVUH032343792 - jkok %>
					<%--Commented and added new frag to display attachments horizontally --%>
					<%--@ include file="fragments/Transaction-Documents.frag" --%>
					<%@ include file="fragments/Instruments-AttachDocuments.frag" %>
					<% // [END] IR-YVUH032343792 - jkok %>
					
					
					<% //CR 821 Added Repair Reason Section 
					  StringBuffer repairReasonWhereClause = new StringBuffer();
					  int  repairReasonCount = 0;
						
						/*Get all repair reason's count from transaction history table*/
						repairReasonWhereClause.append("p_transaction_oid = ?");
						repairReasonWhereClause.append(" and REPAIR_REASON is not null order by ACTION_DATETIME");
						//jgadela  R90 IR T36000026319 - SQL FIX
						Object sqlParamsRepCo[] = new Object[1];
						sqlParamsRepCo[0] =  transaction.getAttribute("transaction_oid") ;
						Debug.debug("repairReasonWhereCondition query : " + repairReasonWhereClause);
						repairReasonCount = DatabaseQueryBean.getCount("A_USER_OID", "transaction_history", repairReasonWhereClause.toString(), true, sqlParamsRepCo);
					if ( !(TradePortalConstants.TRANS_STATUS_PROCESSED_BY_BANK.equalsIgnoreCase(transactionStatus)) && repairReasonCount>0  ){%>
					
						<%=widgetFactory.createSectionHeader("", "TransactionHistory.RepairReason") %>
							<%@ include file="fragments/Transaction-RepairReason.frag" %>
						</div>
					<%} %> 
				
					<%=widgetFactory.createSectionHeader("1", "AWB_SGTEE.Terms") %>
					<%-- <div data-dojo-type="dijit.TitlePane" data-dojo-props="title: 'Pane #1'"> --%>				
					<%@ include file="fragments/Transaction-AIR-REL_SHP-ISS-General.frag" %>
						</div><%--section1 --%>
						
 					<%= widgetFactory.createSectionHeader("2", "AWB_SGTEE.ShipmentDetails") %>
					 <% 
						if (instrumentType.equals(InstrumentType.AIR_WAYBILL))
						{ 
					%>
						<%@ include file="fragments/Transaction-AIR-REL-ShipmentDetails.frag" %>					     
					<%	
						}
						else 
						{
					%>	 
						<%@ include file="fragments/Transaction-SHP-ISS-ShipmentDetails.frag" %>
					<%
						}
					%>
						</div><%--section2 --%>
						<%= widgetFactory.createSectionHeader("3", "AWB_SGTEE.InstructionsToBank") %>
						<%@ include file="fragments/Transaction-AIR-REL_SHP-ISS-BankInstructions.frag" %>
						</div><%--section3 --%>
						
						<% if (TradePortalConstants.TRANS_STATUS_PROCESSED_BY_BANK.equalsIgnoreCase(transactionStatus) && repairReasonCount>0  ){%>
						<%=widgetFactory.createSectionHeader("4", "TransactionHistory.RepairReason", null, true) %>
							<%@ include file="fragments/Transaction-RepairReason.frag" %>
						</div>
					<%} %> 
					</div><%--FormContent--%>
					</div><%--FormArea--%>
					<div class="formSidebar" data-dojo-type="widget.FormSidebar" data-dojo-props="title: '', form: 'Transaction-AIR_SHP-Form'">
						<jsp:include page="/common/Sidebar.jsp">
							<jsp:param name="links" value="<%=links%>" />
							<jsp:param name="isReadOnly" value="<%=isReadOnly%>" />
							<jsp:param name="certAuthURL" value="<%=certAuthURL%>" />
							<jsp:param name="buttonPressed" value="<%=buttonPressed%>" />
							<jsp:param name="error" value="<%= error%>" />
							<jsp:param name="formName" value="0" />
							<jsp:param name="isTemplate" value="<%=isTemplate%>"/>
							<jsp:param name="showLinks" value="true" />  
							<jsp:param name="showApplnForm" value="<%=canDisplayPDFLinks%>"/>							
							<jsp:param name="isNewTransaction" value="<%=newTransaction%>"/>
						</jsp:include>
					</div>
					<%@ include file="fragments/PhraseLookupPrep.frag" %>
					<%@ include file="fragments/PartySearchPrep.frag" %>
					<input type="hidden" name="selection" />
					<input type="hidden" name="ChoosePrimary" />
					
					<%= formMgr.getFormInstanceAsInputField("Transaction-AIR_SHP-Form", secureParms) %>
					<div id="PartySearchDialog"></div>
				</form>
				<%  //include a hidden form for new instrument submission
			//this is used for all of the new instrument types available from the menu
			Hashtable newInstrSecParms = new Hashtable();
			newInstrSecParms.put("UserOid", userSession.getUserOid());
			newInstrSecParms.put("SecurityRights", userSession.getSecurityRights());
			newInstrSecParms.put("clientBankOid", userSession.getClientBankOid());
			newInstrSecParms.put("ownerOrg", userSession.getOwnerOrgOid());
			%>
			<form method="post" name="NewInstrumentForm1" action="<%=formMgr.getSubmitAction(response)%>">
			<input type="hidden" name="bankBranch" />
			<input type="hidden" name="transactionType"/>
			<input type="hidden" name="instrumentType" />
			<input type="hidden" name="copyInstrumentOid" />
			<input type="hidden" name="mode" value="CREATE_NEW_INSTRUMENT" />
			<input type="hidden" name="copyType" value="Instr" />
			
			<%= formMgr.getFormInstanceAsInputField("NewInstrumentForm",newInstrSecParms) %>
			</form>
			<%
			Hashtable newTempSecParms = new Hashtable();
			newTempSecParms.put("userOid", userSession.getUserOid());
			newTempSecParms.put("securityRights", userSession.getSecurityRights());
			newTempSecParms.put("clientBankOid", userSession.getClientBankOid());
			newTempSecParms.put("ownerOrg", userSession.getOwnerOrgOid());
			newTempSecParms.put("ownerLevel", userSession.getOwnershipLevel());
			%>
			<form method="post" name="NewTemplateForm1" action="<%=formMgr.getSubmitAction(response)%>">
			<input type="hidden" name="name" />
			<input type="hidden" name="bankBranch" />
			<input type="hidden" name="transactionType"/>
			<input type="hidden" name="InstrumentType" />
			<input type="hidden" name="copyInstrumentOid" />
			<input type="hidden" name="mode"  />
			<input type="hidden" name="CopyType" />
			<input type="hidden" name="expressFlag"  />
			<input type="hidden" name="fixedFlag" />
			<input type="hidden" name="PaymentTemplGrp" />
			<input type="hidden" name="validationState" />
			
			<%= formMgr.getFormInstanceAsInputField("NewTemplateForm",newTempSecParms) %>
			<%-- Naveen 11-Oct-2012 IR-T36000006190. Added a empty DIV for Instrument Search Pop Up --%>
			<div id="instrumentSearchDialog"></div>
			</form>
			</div><%--PageContentDiv--%>
		</div><%--PageMainDiv--%>
		<%-- KMehta on 12 May 2014 @ Rel 90 for IR T36000025449 Start --%>
			<jsp:include page="/common/Footer.jsp">
			   <jsp:param name="autoSaveFlag"           value="<%=autoSaveFlag%>" />
			   <jsp:param name="templateFlag"           value="<%=templateFlag%>" />
			   <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
			   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
			</jsp:include>
		<%-- KMehta on 12 May 2014 @ Rel 90 for IR T36000025449 End --%>
					
			<jsp:include page="/common/SidebarFooter.jsp"></jsp:include> 
		
<%--cquinton 2/7/2013 added for debugging if necessary--%>
<%--
  out.println("getDataFromDoc="+getDataFromDoc);
  out.println(" newPartyType="+newPartyType);
  out.println(" newPartyOid="+newPartyOid);
  out.println(" xmlDoc="+StringFunction.xssCharsToHtml(doc.toString()));
--%>

	</body>
</html>

<script type="text/javascript">

  <%-- cquinton 3/3/2013 add local var --%>
  var local = {};

<%
  if (!isReadOnly && InstrumentServices.isNotBlank(focusField)) {
%>
  require(["dijit/registry", "dojo/ready"],
    function(registry, ready ) {
      ready(function() {
        var focusFieldId = '<%= focusField %>';
        if ( focusFieldId ) {
          var focusField = registry.byId(focusFieldId);
          focusField.focus();
        }       
      });
  });
  
<%
  }
	// Finally, reset the cached document to eliminate carryover of
   // information to the next visit of this page.
   formMgr.storeInDocCache("default.doc", new DocumentHandler());
%>

  var itemid;
  var section;

  function SearchParty(identifierStr, sectionName,partyType) {
			
    itemid = String(identifierStr);
    section = String(sectionName);
    partyType = String(partyType);

    require(["dojo/dom", "t360/dialog"], function(dom, dialog ) {

      <%-- cquinton 2/7/2013 --%>
      <%-- set the SearchPartyType input so it is included on new party form submit --%>
      var searchPartyTypeInput = dom.byId("SearchPartyType");
      if ( searchPartyTypeInput ) {
        searchPartyTypeInput.value=partyType;
      }

      dialog.open('PartySearchDialog', '<%=PartySearchAddressTitle%>','PartySearch.jsp',
        ['returnAction','partyType','unicodeIndicator','itemid','section'],
        ['selectTransactionParty',partyType,'<%=TradePortalConstants.INDICATOR_NO%>',itemid,section]);
    });
  }
		
		function openCopySelectedDialogHelper(callBackFunction){
		    require(["t360/dialog"], function(dialog) {
		          dialog.open('copySelectedInstrument', '<%=resMgr.getTextEscapedJS("CopySelected.Title",TradePortalConstants.TEXT_BUNDLE) %>',
		                    'copySelectedInstrumentDialog.jsp',
		                    "instrumentType", '<%=instrumentType%>', <%-- parameters --%>
		                    'select', callBackFunction);
		        	  
		      });
		  }
		function copySelectedToInstrument(bankBranchOid, copyType, templateName, templateGroup, flagExpressFixed){
			var rowKeys ;
			 require(["dijit/registry", "dojo/query", "dojo/on", "t360/dialog", "dojo/domReady!"],
				        function(registry, query, on, dialog ) {
				      <%--get array of rowkeys from the grid--%>
				      rowKeys = getSelectedGridRowKeys("instrumentsSearchGridId");
			 });
			if(copyType == 'I'){ 
				if ( document.getElementsByName('NewInstrumentForm1').length > 0 ) {
				    var theForm = document.getElementsByName('NewInstrumentForm1')[0];
				    theForm.bankBranch.value=bankBranchOid;
				    theForm.copyInstrumentOid.value=rowKeys;
				    theForm.submit();
				  }
			}	
			if(copyType == 'T'){ 
				if ( document.getElementsByName('NewTemplateForm1').length > 0 ) {
					var flag = flagExpressFixed.split('/');
					var expressFlag = flag[0];
					var fixedFlag = flag[1];
					var theForm = document.getElementsByName('NewTemplateForm1')[0];
				    theForm.name.value=templateName;
				    theForm.mode.value="<%=TradePortalConstants.NEW_TEMPLATE%>";
				    theForm.CopyType.value="<%=TradePortalConstants.FROM_INSTR%>";
				    theForm.fixedFlag.value=fixedFlag;
				    theForm.expressFlag.value=expressFlag;
				    theForm.PaymentTemplGrp.value=templateGroup;
				    theForm.copyInstrumentOid.value=rowKeys;
				    theForm.validationState.value = "";
				    theForm.submit();
				  }
			}
		}
<%-- 	Naveen 11-Oct-2012 IR-T36000006190. Added new javascript function for Instrument Search Pop Up    --%>		
		function SearchInstrument(identifierStr, sectionName , InstrumentType) {
		    <%--todo: add dialog require here, but need to walk back to event handler from the menus!!! --%> 
		    itemid = String(identifierStr);
			section = String(sectionName);
			IntType=String(InstrumentType);
			
			        
		    require(["t360/dialog"], function(dialog ) {
		      dialog.open('instrumentSearchDialog', '<%=instrumentSearchDialogTitle%>',
		                  'instrumentSearchDialogID.jsp',
		                  ['itemid','section','IntType'], [itemid,section,IntType], <%-- parameters --%>
		                  'select', null);
		     
		    });
		  }

  <%-- cquinton 3/2/2013 local phrase lookup --%>
  require(["t360/phraseLookup"],
          function( phraseLookup ) {
    local.phraseLookupAppend = function(phraseSelect, textArea, textAreaMaxLength ) {
      phraseLookup.appendPhrase( phraseSelect, textArea, textAreaMaxLength, "_errorSection" );
    };
  });

	</script>
