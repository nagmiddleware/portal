<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.web.*, com.amsinc.ecsg.frame.*, com.amsinc.ecsg.util.*, java.util.*, 
                 com.ams.tradeportal.common.*" %>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session"></jsp:useBean>

<%
	
   String nextPhysicalPage = null;

   try
   {
      ServletContext context = this.getServletConfig().getServletContext();

      response.setContentType("text/html");

      RequestDispatcher requestDispatcher = context.getRequestDispatcher("/POStructuredFileUploadServlet");
       
      NavigationManager nif = NavigationManager.getNavMan();

      nextPhysicalPage = nif.getPhysicalPage(formMgr.getCurrPage(), request);


      requestDispatcher.include(request, response);
%>
     <jsp:forward page='<%= nextPhysicalPage %>' />
<%
   }
   catch (Exception e)
   {
      System.out.println("Exception caught in POFileUploadServlet.jsp");
      e.printStackTrace();
      String link = formMgr.getUrlForLogicalPage(nextPhysicalPage, "", response);
      response.sendRedirect(response.encodeRedirectUrl (link));
   }
%>
