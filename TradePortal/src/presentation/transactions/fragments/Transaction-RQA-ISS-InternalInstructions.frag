<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
*******************************************************************************
                 Request to Advise Issue Page - Internal Instructions section

  Description:
    Contains HTML to create the Request to Advise Issue Internal Instructions section.  

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transaction-RQA-ISS-InternalInstructions.jsp" %>
*******************************************************************************
--%>
<div class = "formItem">
<%= widgetFactory.createNote("RequestAdviseIssue.IntInstrText") %>
</div>
<%= widgetFactory.createTextArea( "InternalInstructions", "", terms.getAttribute("internal_instructions"),isReadOnly, false,false, "rows='10' cols='128'","","") %>
