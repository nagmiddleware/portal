<%--
*******************************************************************************

  Description:
  A fragment for displaying terms party invAccounts.
  Fairly generic, but should be copied for specific instrument behavior.

  The following variables are passed:

    termsParty - a TermsPartyWebBean
    isReadOnly
    isTemplate
    widgetFactory

*******************************************************************************
--%>
<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.html.*,java.math.*" %>

<%
  // Logic to handle account list.  If no payee has been searched, there is
  // only an enterable account number field.  Otherwise, we display the set of invAccounts defined
  // for the party (when the party is selected, we get the set of invAccounts for that party 
  // and save it on the TermsParty record -- if the party's invAccounts change later, we do not update
  // the set on the TermsParty record.) as well as an enterable account number field.  The account 
  // number selected by the user will be one of the payee's predefined invAccounts or the 
  // entered account number.
  
  		  String inv_benAcctList = StringFunction.xssHtmlToChars(termsParty.getAttribute("acct_choices"));
		  DocumentHandler invAcctData = new DocumentHandler(inv_benAcctList, true);
		  DocumentHandler invAcct;
		  boolean invFoundAcctMatch = false; // indicates if ANY predefined invAcct num matches ben. invAcct
		  //     number from business object
		  boolean invFoundMatch = false; // indicates if a given invAcct num matches ben. invAcct
		  //     number from business object
		  int invNumAccts, i_inv;
		  Vector invAccounts = invAcctData.getFragments("/DocRoot/ResultSetRecord");
		  invNumAccts = invAccounts.size();
		  
		  String invAcctNum = null;
		  String invAcctCcy = null;

		  Debug.debug("Account Choices for Ben Party: " + inv_benAcctList);
		  Debug.debug("Number of account in list is " + invNumAccts);

		  // Set the invShowBenAccts flag that determines if a list of invAccounts with radio buttons
		  // appears.  They appear if we have a set of invAccounts (i.e., it's been preloaded into
		  // a inv_benAcctList variable).  However, in readonly mode, we do not display the list.

		  // TLE - 11/20/06 - IR#ANUG110757179 - Add Begin
		  //if (invNumAccts > 0) invShowBenAccts = true;
		  boolean invShowBenAccts = false;
		  if (invNumAccts > 0)
				invShowBenAccts = true;
		  // TLE - 11/20/06 - IR#ANUG110757179 - Add Begin
		  if (isReadOnly)
				invShowBenAccts = false;

		  Debug.debug("invShowBenAccts flag is " + invShowBenAccts);

		  // Store the set of invAccounts for the beneficiary in a hidden field.  This gets written to the
		  // TermsParty record so we always have the set of invAccounts at the time the party was selected.
		  if(null != inv_benAcctList && !"".equals(inv_benAcctList)){
			%>
			<input type=hidden value='<%=inv_benAcctList%>' name=BenAcctChoices>
			<%
		  }
		 
		  if (invShowBenAccts) {
				// This loop prints the set of invAccounts previously determined when the party was
				// selected.
				for (i_inv = 0; i_inv < invNumAccts; i_inv++) {
					  invAcct = (DocumentHandler) invAccounts.elementAt(i_inv);
		%>
		<%
		  // Display a radio button using the "account number/currency" from the
					  // account list (for whichever row we're on).  Select the correct
					  // radio button if we match on the current value of "acct_num" on the
					  // beneficiary party.
					  out.println("<td width=24 nowrap>");

					  // (Don't know why, but the first "get" from the doc must be without the /,
					  // succeeding ones must be with a /.  This is different from similar logic
					  // elsewhere.  Believed to be a symptom of flattening a DocHandler to a string
					  // and then creating another DocHandler from the string.)

					  invAcctNum = invAcct.getAttribute("ACCOUNT_NUMBER"); // no "slash"
					  invAcctCcy = invAcct.getAttribute("/CURRENCY"); // get with "slash"

					  invFoundMatch = invAcctNum.equals(termsParty.getAttribute("acct_num"));

					  if (!invFoundAcctMatch) {
							invFoundAcctMatch = invFoundAcctMatch | invFoundMatch;
					  }
	
		String radioLabel = "";
		  if (invAcctCcy.equals("")) {
			  radioLabel = invAcctNum;
	
		  } else {
			  radioLabel = invAcctNum + " (" + invAcctCcy + ")";
	
		  }
	%>
	<%=widgetFactory.createRadioButtonField("SelectedAccountRec", 
			"SelectedAccountRec"+invAcctNum, radioLabel, 
			invAcctNum+ TradePortalConstants.DELIMITER_CHAR+ invAcctCcy, 
			invFoundMatch, isReadOnly,	"onClick='document.forms[0].FinanceType[7].checked = true;'","")%>
	<div style=\"clear:both;\"></div>
	<%
		  // End of for loop
				}
		  } // end if invShowBenAccts
	%>
	<%
		  // This IF sets up invAcctNum and invAcctCcy fields with values if necessary and uses them
		  // for displaying the enterable account field.

		  if (invShowBenAccts) {
				// Display radio button for selection of the enterable account.  If we haven't found
				// an account match yet, then we might match on the enterable value.  If they match,
				// turn on the radio button.
				if (invFoundAcctMatch) {
					  invAcctNum = "";
					  invAcctCcy = "";
					  invFoundMatch = false;
				} else {
					  invAcctNum = termsParty.getAttribute("acct_num");
					  invAcctCcy = termsParty.getAttribute("acct_currency");
					  if (invAcctNum == null)
							invAcctNum = "";
							invFoundMatch = invAcctNum.equals(termsParty.getAttribute("acct_num"));
				}
	%>
	<%=widgetFactory.createRadioButtonField("SelectedAccountRec",TradePortalConstants.USER_ENTERED_ACCT,"",
			TradePortalConstants.USER_ENTERED_ACCT, invFoundMatch, isReadOnly,"onClick='document.forms[0].FinanceType[7].checked = true;'", "")%>
	<%
		  } else {
				// We're not displaying the account list, so populate the account field with the
				// account from the beneficiary party record.
				invAcctNum = termsParty.getAttribute("acct_num");
				invAcctCcy = termsParty.getAttribute("acct_currency");
				if (invAcctNum == null)
					  invAcctNum = "";
		  }
	%>
	<%-- KMEhta IR T36000022557 on 15/1/2014 changed required indicator to !isTemplate --%>
	<%=widgetFactory.createTextField("EnteredAccountRec","",
			invAcctNum,"30",isReadOnly,!isTemplate,false,"onClick='document.forms[0].FinanceType[7].checked = true;'","", "none")%>
