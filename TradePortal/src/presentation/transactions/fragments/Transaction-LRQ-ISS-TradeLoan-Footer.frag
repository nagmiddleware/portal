<%--
*******************************************************************************
  Transaction-FTDP-ISS-CreditDebit Footer

  Description: Everything that should be loaded at the end of the page
               should be included here.  This includes non-visible html 
               and javascript that should execute on page load.

*******************************************************************************
--%>
<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>



<%
  dgFactory = new DataGridFactory(resMgr, userSession, formMgr, response);
  String gridLayout = dgFactory.createDataGrid(gridId,gridName);
%>
<script type="text/javascript">

<%-- Added a delete link in grid --%>
function deleteFormatter(){
    return "<a href='javascript:deleteBeneficiaryRow();'>Delete</a>";
}



  	  	var gridLayout = 	[
						{type: "dojox.grid._RadioSelector"},[
						{name:"rowKey", field:"rowKey", hidden:"true"} ,
						{name:"Account Number", field:"AccountNumber_List", width:"100px"} ,
						{name:"Beneficiary Name", field:"PayeeName_List", width:"100px"} ,
						{name:"Currency", field:"Currency_List", width:"67px"} ,
						{name:"Amount", field:"Amount_List", width:"85px", cellClasses:"gridColumnRight"} ,
						{name:"Payment Method", field:"PaymentMethod_List", width:"100px"} ,
						{name:"Beneficiary Bank/Branch Code", field:"PayeeBankCode_List", width:"175px"} ,
						{name:"Beneficiary Bank Name", field:"PayeeBankName_List", width:"150px"} 
	<% 	if (!isReadOnly && !isMultiBenInvoicesAttached) {	%>
						,{name:"Action", field:"rowKey", formatter:deleteFormatter, width:"60px"}
	<%}%>
  	                    ]];
  	

	var gridId = '<%=gridId%>';
	var gridName = '<%=gridName%>';
  	var gridDataView = '<%=EncryptDecrypt.encryptStringUsingTripleDes(gridDataView,userSession.getSecretKey())%>';  <%--Rel9.2 IR T36000032596 --%>
  	
  	var initSearchParms = "transaction_oid=<%=transaction.getAttribute("transaction_oid")%>&benificiaryName=<%=benificiaryName%>";
  	initSearchParms += "&benificiaryBankName=<%=benificiaryBankName%>&paymentStatusSearch=<%=paymentStatusSearch%>&amount=<%=amount%>";
  	console.log("initSearchParms : "+initSearchParms);
  	if (gridId != ''){
  		createDataGrid(gridId, gridDataView,gridLayout, initSearchParms);
  	}
  	
  	
  

</script>
