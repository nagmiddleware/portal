<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
*******************************************************************************
                 Export Collection Issue Page - Documents Presented section

  Description:
    Contains HTML to create the Export COL Issue Documents Presented section.  

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transaction-EXP_COL-ISS-DocsPresented.jsp" %>
*******************************************************************************
--%>
 
  <%
  /***************************************************
  * Start of Documents Presented Section - beige bar
  ****************************************************/
  %>
   <table style="width:90%;float:left;" border="1" cellspacing="0" cellpadding="0" class="formDocumentsTable">
	<thead>
		<tr>
			<th>&nbsp;</th>
			<th class="genericCol"><%=resMgr.getText("ExportCollectionIssue.DocumentType",TradePortalConstants.TEXT_BUNDLE)%></th>
			<th class="genericCol"><%=resMgr.getText("ExportCollectionIssue.NumOfDocuments",TradePortalConstants.TEXT_BUNDLE)%></th>
		</tr>
	</thead>
	<tbody>
		 <%
  		 /*********************
  		 * Bills of Exchange
  		 *********************/
  		%>
		<tr>
			<td align="center" width=5%><%=widgetFactory.createCheckboxField("BillExchangeInd", "",terms.getAttribute("present_bills_of_exchange").
                equals(TradePortalConstants.INDICATOR_YES), isReadOnly , false, "", "", "none" ) %></td>
			<td><%= widgetFactory.createLabel( "ExportCollectionIssue.BillsOfExchangeDrafts", "DirectSendCollection.BillsOfExchangeDrafts", false, false, false, "none") %></td>
			<td align="center" width=25%><%= widgetFactory.createTextField( "BillExchangeNum", "", terms.getAttribute("present_num_bills_of_exchange"), "3", isReadOnly, false, false,"onChange=\"pickBillOfExchangeCheckbox();\"", "regExp:'[0-9/]+'","none" ) %></td>
		</tr>
		<%
  		/*********************
  		* Commercial Invoice
  		*********************/
  		%>
		<tr>
			<td align="center"><%=widgetFactory.createCheckboxField("CommInvInd", "",terms.getAttribute("present_commercial_invoice").
                    equals(TradePortalConstants.INDICATOR_YES), isReadOnly , false, "", "", "none" ) %></td>
			<td><%= widgetFactory.createLabel( "ExportCollectionIssue.CommercialInvoice", "ExportCollectionIssue.CommercialInvoice", false, false, false, "none") %></td>
			<td align="center"><%= widgetFactory.createTextField( "CommInvNum", "", terms.getAttribute("present_num_commercial_invoice"), "3", isReadOnly, false, false, "onChange=\"pickCommercialInvoiceCheckbox();\"", "regExp:'[0-9/]+'","none" ) %></td>
		</tr>
		<%
  		/*********************
  		* Bill of Lading
  		*********************/
 		 %>
 		 <tr>
 		 	<td align="center"><%=widgetFactory.createCheckboxField("BillLadingInd", "",terms.getAttribute("present_bill_of_lading").
                    equals(TradePortalConstants.INDICATOR_YES), isReadOnly , false, "", "", "none" ) %></td>
			<td><%= widgetFactory.createLabel( "ExportCollectionIssue.BillOfLading", "ExportCollectionIssue.BillOfLading", false, false, false, "none") %><%= widgetFactory.createNote( "ExportCollectionIssue.BillOfLadingContinue") %></td>
			<td align="center"><%= widgetFactory.createTextField( "BillLadingNum", "", terms.getAttribute("present_num_bill_of_lading"), "3", isReadOnly, false, false, "onChange=\"pickBillOfLadingCheckbox();\"", "regExp:'[0-9/]+'","none" ) %></td>			
        </tr>
        <%
  		/********************************
  		* Non Negotiable Bill of Lading
  		*********************************/
  		%>
		<tr>
			<td align="center"><%=widgetFactory.createCheckboxField("NonBillLadingInd", "",terms.getAttribute("present_non_neg_bill_of_lading").
                    equals(TradePortalConstants.INDICATOR_YES), isReadOnly , false, "", "", "none" ) %></td>
			<td><%= widgetFactory.createLabel( "ExportCollectionIssue.NonNegotiableBillOfLading", "ExportCollectionIssue.NonNegotiableBillOfLading", false, false, false, "none") %></td>
			<td align="center"><%= widgetFactory.createTextField( "NonBillLadingNum", "", terms.getAttribute("present_num_non_neg_bill_ladng"), "3", isReadOnly, false, false, "onChange=\"pickNonNegBillOfLadingCheckbox();\"", "regExp:'[0-9/]+'","none" ) %></td>
		</tr>
		<%
  		/**************
  		* AirWayBill
  		**************/
  		%>
  		<tr>
			<td align="center"><%=widgetFactory.createCheckboxField("AirWaybillInd", "",terms.getAttribute("present_air_waybill").
                    equals(TradePortalConstants.INDICATOR_YES), isReadOnly , false, "", "", "none" ) %></td>
			<td><%= widgetFactory.createLabel( "ExportCollectionIssue.AirWaybill", "ExportCollectionIssue.AirWaybill", false, false, false, "none") %></td>
			<td align="center"><%= widgetFactory.createTextField( "AirWaybillNum", "", terms.getAttribute("present_num_air_waybill"), "3", isReadOnly, false, false, "onChange=\"pickAirWaybillCheckbox();\"", "regExp:'[0-9/]+'","none" ) %></td>
		</tr>
		<%
  		/********************
  		* Insurance Policy
  		********************/
  		%>
  		<tr>
			<td align="center"><%=widgetFactory.createCheckboxField("InsuranceInd", "",terms.getAttribute("present_insurance").
                    equals(TradePortalConstants.INDICATOR_YES), isReadOnly , false, "", "", "none" ) %></td>
			<td><%= widgetFactory.createLabel( "ExportCollectionIssue.InsurancePolicyCertificate", "DirectSendCollection.InsurancePolicyCertificate", false, false, false, "none") %></td>
			<td align="center"><%= widgetFactory.createTextField( "InsuranceNum", "", terms.getAttribute("present_num_insurance"), "3", isReadOnly, false, false,  "onChange=\"pickInsuranceCheckbox();\"", "regExp:'[0-9/]+'","none" ) %></td>
		</tr>
		<%
 		/************************
  		* Certificate of Origin
  		************************/
  		%>
  		<tr>
			<td align="center"><%=widgetFactory.createCheckboxField("CertOfOriginInd", "",terms.getAttribute("present_cert_of_origin").
                    equals(TradePortalConstants.INDICATOR_YES), isReadOnly , false, "", "", "none" ) %></td>
			<td><%= widgetFactory.createLabel( "ExportCollectionIssue.CertificateOfOrigin", "ExportCollectionIssue.CertificateOfOrigin", false, false, false, "none") %></td>
			<td align="center"><%= widgetFactory.createTextField( "CertOfOriginNum", "", terms.getAttribute("present_num_cert_of_origin"), "3", isReadOnly, false, false, "onChange=\"pickCertificateOfOriginCheckbox();\"", "regExp:'[0-9/]+'","none" ) %></td>
		</tr>
		<%
  		/************************
  		* Packing List
  		************************/
  		%>
  		<tr>
			<td align="center"><%=widgetFactory.createCheckboxField("PackingListInd", "",terms.getAttribute("present_packing_list").
                    equals(TradePortalConstants.INDICATOR_YES), isReadOnly , false, "", "", "none" ) %></td>
			<td><%= widgetFactory.createLabel( "ExportCollectionIssue.PackingList", "ExportCollectionIssue.PackingList", false, false, false, "none") %></td>
			<td align="center"><%= widgetFactory.createTextField( "PackingListNum", "", terms.getAttribute("present_num_packing_list"), "3", isReadOnly, false, false, "onChange=\"pickPackingListCheckbox();\"", "regExp:'[0-9/]+'","none" ) %></td>
		</tr>
		<%
  		/************************
  		* Other Documents
  		************************/
  		%>
  		<tr>
  			<td valign="top"><%=widgetFactory.createCheckboxField("OtherDocsInd", "",terms.getAttribute("present_other").
                    equals(TradePortalConstants.INDICATOR_YES), isReadOnly , false, "", "", "none" ) %></td>
            <td colspan="2"><%= widgetFactory.createLabel( "ExportCollectionIssue.OtherDocuments1", "ExportCollectionIssue.OtherDocuments1", false, false, false, "none") %>           
            <%
  			/****************************
  			* Other - Phrase dropdown
  			****************************/
  			%>
            <%
        	if ( isReadOnly ) {
          	out.println("&nbsp;");
        	} else {
          options = ListBox.createOptionList(presentDocList, "PHRASE_OID", "NAME", "", userSession.getSecretKey());
          defaultText = resMgr.getText("transaction.SelectPhrase",
                                       TradePortalConstants.TEXT_BUNDLE);
          out.println(widgetFactory.createSelectField( "OtherDocsPhraseItem", "", defaultText, options, isReadOnly, false, false, "onChange=" + PhraseUtility.getPhraseChange(
                  "/Terms/present_other_text",
                  "OtherDocsText", "2000","document.forms[0].OtherDocsText"), "", ""));          
        	}		
			%>
			<div class="formItem">
			<%= widgetFactory.createTextArea( "OtherDocsText", "", terms.getAttribute("present_other_text"), isReadOnly,false, false, "onChange=\"pickOtherDocsCheckbox();\" style='width:95%;'","", "none" ) %>
            <%if(!isReadOnly){ %>
            <%= widgetFactory.createHoverHelp("OtherDocsText","OtherDocsText")%>
            <%} %>
            </div>
            </td>
            
  		</tr>
	</tbody>
 </table>
   
  
<script LANGUAGE="JavaScript">

  function pickBillOfExchangeCheckbox() {
	if ((dijit.byId("BillExchangeNum").value != '' ) &&
         (dijit.byId("BillExchangeNum").value > 0) ) {
		dijit.byId('BillExchangeInd').set('checked', true);
    }
    else if( (dijit.byId("BillExchangeNum").value == 0 ) ||
	 (dijit.byId("BillExchangeNum").value == ''))
    	dijit.byId("BillExchangeInd").set('checked', false);
  }

  function pickCommercialInvoiceCheckbox() {
    if ( (dijit.byId("CommInvNum").value > '' ) &&
         (dijit.byId("CommInvNum").value > 0) )
    	dijit.byId("CommInvInd").set('checked', true);
    else
     if( (dijit.byId("CommInvNum").value == 0 ) ||
	 (dijit.byId("CommInvNum").value == ''))
    	 dijit.byId("CommInvInd").set('checked', false);
  }

  function pickBillOfLadingCheckbox() {
    if ( (dijit.byId("BillLadingNum").value > '' ) ||
	 (dijit.byId("BillLadingNum").value > 0) )
    	dijit.byId("BillLadingInd").set('checked', true);
    else
     if( (dijit.byId("BillLadingNum").value == 0 ) || 
	 (dijit.byId("BillLadingNum").value == ''))
    	 dijit.byId("BillLadingInd").set('checked', false);
  }

  function pickNonNegBillOfLadingCheckbox() {
    if ( (dijit.byId("NonBillLadingNum").value > '' ) &&
	 (dijit.byId("NonBillLadingNum").value > 0) )
    	dijit.byId("NonBillLadingInd").set('checked', true);
    else
     if( (dijit.byId("NonBillLadingNum").value == 0 ) ||
	 (dijit.byId("NonBillLadingNum").value == ''))
    	 dijit.byId("NonBillLadingInd").set('checked', false);
  }

  function pickAirWaybillCheckbox() {
    if ( (dijit.byId("AirWaybillNum").value > '' ) &&
	 (dijit.byId("AirWaybillNum").value > 0) ) 
    	dijit.byId("AirWaybillInd").set('checked', true);
    else
     if( (dijit.byId("AirWaybillNum").value == 0 ) ||
	 (dijit.byId("AirWaybillNum").value == ''))
    	 dijit.byId("AirWaybillInd").set('checked', false);
  }

  function pickInsuranceCheckbox() {
    if ( (dijit.byId("InsuranceNum").value > '' ) &&
 	 (dijit.byId("InsuranceNum").value > 0) )
    	dijit.byId("InsuranceInd").set('checked', true);
    else
     if( (dijit.byId("InsuranceNum").value == 0 ) ||
  	 (dijit.byId("InsuranceNum").value == ''))
    	 dijit.byId("InsuranceInd").set('checked', false);
  }

  function pickCertificateOfOriginCheckbox() {
    if ( (dijit.byId("CertOfOriginNum").value > '' ) &&
       	 (dijit.byId("CertOfOriginNum").value > 0) )
    	dijit.byId("CertOfOriginInd").set('checked', true);
    else
     if( (dijit.byId("CertOfOriginNum").value == 0 )  ||
	 (dijit.byId("CertOfOriginNum").value == ''))
    	 dijit.byId("CertOfOriginInd").set('checked', false);
  }

  function pickPackingListCheckbox() {
    if ( (dijit.byId("PackingListNum").value > '' ) &&
	 (dijit.byId("PackingListNum").value > 0) )
       dijit.byId("PackingListInd").set('checked', true);
    else
     if( (dijit.byId("PackingListNum").value == 0 ) ||
	 (dijit.byId("PackingListNum").value == ''))
       dijit.byId("PackingListInd").set('checked', false);
  }

  function pickOtherDocsCheckbox() {
    if (dijit.byId("OtherDocsText").value > '')
       dijit.byId("OtherDocsInd").set('checked', true);
  }
</script>

<%
  if (phraseSelected) {
     // To ensure that the checkbox gets turned on when the user selects
     // a phrase for a previously blank text area, call these methods to
     // re-evaluate all the fields.
     // Note: this calls must be done AFTER the definition of the functions.
%>
     <script language="Javascript">
        pickBillOfExchangeCheckbox();
        pickCommercialInvoiceCheckbox();
        pickBillOfLadingCheckbox();
        pickNonNegBillOfLadingCheckbox();
        pickAirWaybillCheckbox();
	pickInsuranceCheckbox();
	pickCertificateOfOriginCheckbox();
	pickPackingListCheckbox();
	pickOtherDocsCheckbox();
     </script>
<%
  }
%>
