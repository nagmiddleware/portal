<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
*******************************************************************************
    Export Letter of Credit Issue Transfer Page - Bank Instructions section

  Description:
    Contains HTML to create the Export Letter of Credit Issue Transfer Page - 
    Bank Instructions section.

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transaction-EXP_DLC-TRN-BankInstructions.jsp" %>
*******************************************************************************
--%>          
        
        <%
           if (isReadOnly)
           {
              out.println("&nbsp;");
           }
           else
           {
              options = ListBox.createOptionList(spclInstrDocList, "PHRASE_OID", "NAME", "", userSession.getSecretKey());

              defaultText = resMgr.getText("transaction.AddAPhrase", TradePortalConstants.TEXT_BUNDLE);
             
              out.println(widgetFactory.createSelectField( "SpecialInstructionsPhraseItem", "TransferELC.SpecialInstructions",defaultText , options, isReadOnly,
		              false,false,  "onChange=" + PhraseUtility.getPhraseChange("/Terms/special_bank_instructions", 
                              "SpecialInstructionsText", "1000","document.forms[0].SpecialInstructionsText"), "", "")); 
       
           }
        %>
        
        <%= widgetFactory.createTextArea( "SpecialInstructionsText","",terms.getAttribute("special_bank_instructions"), isReadOnly) %>                            
      	
      	<%= widgetFactory.createSubsectionHeader( "TransferELC.CommCharges",isReadOnly,false,false,"") %> 
        
        <%= widgetFactory.createTextField( "BranchCode", "TransferELC.BranchCode", terms.getAttribute("branch_code"), "30", isReadOnly, false, false,  "", "","" ) %>
          
        <%= widgetFactory.createTextField( "DebitAccountNumber", "TransferELC.DebitAccount", terms.getAttribute("coms_chrgs_our_account_num"), "30", isReadOnly, false, false,  "", "","" ) %>
     
        <%= widgetFactory.createTextField( "DebitForeignAccountNumber", "TransferELC.DebitFCA", terms.getAttribute("coms_chrgs_foreign_acct_num"), "30", isReadOnly, false, false,  "", "","" ) %>
                  
            <%
             options = Dropdown.createSortedCurrencyCodeOptions(terms.getAttribute("coms_chrgs_foreign_acct_curr"), 
                                                                loginLocale);

             out.println(widgetFactory.createSelectField( "ForeignAccountCurrency", "TransferELC.ForeignCurrency"," " , options, isReadOnly,false,false, "class='char5'", "", ""));
            %>
          
           
        <%
           if (isReadOnly)
           {
              out.println("&nbsp;");
           }
           else
           {
              options = ListBox.createOptionList(spclInstrDocList, "PHRASE_OID", "NAME", "", userSession.getSecretKey());

              defaultText = resMgr.getText("transaction.AddAPhrase", TradePortalConstants.TEXT_BUNDLE);
             
              out.println(widgetFactory.createSelectField( "CandCOtherTextPhraseItem", "TransferELC.SettlementCommissionsCharges",defaultText , options, isReadOnly ,
		              false,false,  "onChange=" + PhraseUtility.getPhraseChange( "/Terms/coms_chrgs_other_text", "CandCOtherText","1000","document.forms[0].CandCOtherText"), "", ""));        
           }
        %>
        <br>
        
        <%= widgetFactory.createTextArea( "CandCOtherText","",terms.getAttribute("coms_chrgs_other_text"), isReadOnly ) %> 
