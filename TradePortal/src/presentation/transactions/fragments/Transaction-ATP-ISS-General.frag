<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
*******************************************************************************
                     Approval to Pay Issue Page - General section

  Description:
    Contains HTML to create the Approval to Pay Issue General section.  

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transaction-ATP-ISS-General.jsp" %>
*******************************************************************************
--%>

<%
  // Get some attributes for radio button fields.  We need to refer to this
  // value frequently so do getAttribute once.

 
  String bankChargesType = terms.getAttribute("bank_charges_type");

  String amount = terms.getAttribute("amount");
  
	String identifierStr ="BenName,BenAddressLine1,BenAddressLine2,BenCity,BenStateProvince,BenPostalCode,BenCountry,BenPhoneNumber,BenOTLCustomerId,BenUserDefinedField1,BenUserDefinedField2,BenUserDefinedField3,BenUserDefinedField4";
	String sectionName  = "seller";
 // Don't format the amount if we are reading data from the doc
  if(!getDataFromDoc || amountUpdatedFromPOs)
     displayAmount = TPCurrencyUtility.getDisplayAmount(amount, currency, loginLocale);
  else
     displayAmount = amount;

/*	 
  if(!isReadOnly)
     displayAmount = amount;
  else
	  displayAmount = TPCurrencyUtility.getDisplayAmount(amount.toString(), currency, loginLocale);
  */
  Vector codesToExclude;
 
%>
	
		<div class="columnLeft">  
		
		<%
		String benSearchHtml = widgetFactory.createPartySearchButton(identifierStr,sectionName,false,TradePortalConstants.ATP_SELLER,false);
		%>
		<%= widgetFactory.createSubsectionHeader( "ApprovalToPayIssue.Seller",(isReadOnly || isFromExpress), false, isExpressTemplate, benSearchHtml ) %>
		
		<%			 
		// Always send the ben terms party oid
		secureParms.put("ben_terms_party_oid", termsPartySel.getAttribute("terms_party_oid"));
		%>				
		<div style="display:none">
		<input type=hidden name='BenTermsPartyType' value=<%=TradePortalConstants.ATP_SELLER%>>
		
		<%=widgetFactory.createTextField("BenOTLCustomerId","",termsPartySel.getAttribute("OTL_customer_id"),"35",false,false,false," type=hidden ","", "")%>
		<input type=hidden name="VendorId" id="VendorId" value="<%=termsPartySel.getAttribute("vendor_id")%>">
		
		<input type=hidden name="BenUserDefinedField1"
		     value="<%=termsPartySel.getAttribute("user_defined_field_1")%>">
		<input type=hidden name="BenUserDefinedField2"
		     value="<%=termsPartySel.getAttribute("user_defined_field_2")%>">
		<input type=hidden name="BenUserDefinedField3"
		     value="<%=termsPartySel.getAttribute("user_defined_field_3")%>">
		<input type=hidden name="BenUserDefinedField4"
		     value="<%=termsPartySel.getAttribute("user_defined_field_4")%>">
		</div>
		<%
		    if (isFromExpress) {
		      // We are in a transaction created from express.  The beneficiary 
		      // fields are express fields.  This means they display as readonly 
		      // when the user is editing an instrument created from an express 
		      // template.  However, we still need to send the ben data to the 
		      // mediator.  Put the ben data in a bunch of hidden fields to 
		      // ensure this happens.
		%>
			<div style="display:none">
			<input type=hidden name="BenName" 
	        value="<%=termsPartySel.getAttribute("name")%>">
			 <input type=hidden name="BenPhoneNumber" 
			        value="<%=termsPartySel.getAttribute("phone_number")%>">
			 <input type=hidden name="BenAddressLine1"
			        value="<%=termsPartySel.getAttribute("address_line_1")%>">
			 <input type=hidden name="BenAddressLine2"
			        value="<%=termsPartySel.getAttribute("address_line_2")%>">
			 <input type=hidden name="BenCity"
			        value="<%=termsPartySel.getAttribute("address_city")%>">
			 <input type=hidden name="BenStateProvince"
			        value="<%=termsPartySel.getAttribute("address_state_province")%>">
			 <input type=hidden name="BenCountry"
			        value="<%=termsPartySel.getAttribute("address_country")%>">
			 <input type=hidden name="BenPostalCode"
			        value="<%=termsPartySel.getAttribute("address_postal_code")%>">
			 </div>
		<%
			       }
		%>
		
		<%= widgetFactory.createTextField( "BenName", "ApprovalToPayIssue.SellerName", termsPartySel.getAttribute("name"), "35", isReadOnly || isFromExpress, !isTemplate, isExpressTemplate, "onBlur='checkSellerName(\"" + 
                StringFunction.escapeQuotesforJS(termsPartySel.getAttribute("name")) + "\")'", "","" ) %>
		
		<%= widgetFactory.createTextField( "BenAddressLine1", "ApprovalToPayIssue.AddressLine1", termsPartySel.getAttribute("address_line_1"), "35", isReadOnly || isFromExpress, !isTemplate, isExpressTemplate,  "", "","" ) %>
		
		<%= widgetFactory.createTextField( "BenAddressLine2", "ApprovalToPayIssue.AddressLine2", termsPartySel.getAttribute("address_line_2"), "35", isReadOnly || isFromExpress, false, isExpressTemplate, "", "",""  ) %>
		
		<%= widgetFactory.createTextField( "BenCity", "ApprovalToPayIssue.City", termsPartySel.getAttribute("address_city"), "23", isReadOnly || isFromExpress, !isTemplate, isExpressTemplate,  "class='char35'", "",""  ) %>
		
		<div>
		<%= widgetFactory.createTextField( "BenStateProvince", "ApprovalToPayIssue.ProvinceState", termsPartySel.getAttribute("address_state_province").toString(), "8", isReadOnly || isFromExpress, false, isExpressTemplate, "class='char10'", "","inline"  ) %>
		<%= widgetFactory.createTextField( "BenPostalCode", "ApprovalToPayIssue.PostalCode", termsPartySel.getAttribute("address_postal_code").toString(), "15", isReadOnly || isFromExpress, false, isExpressTemplate, "class='char10'", "","inline" ) %>
		<div style="clear:both;"></div>
		</div>
		
		<%
		options = Dropdown.createSortedRefDataOptions(
		            TradePortalConstants.COUNTRY, 
		            termsPartySel.getAttribute("address_country"), 
		            loginLocale);
		%>
		<%= widgetFactory.createSelectField( "BenCountry", "ApprovalToPayIssue.Country", " ", options, isReadOnly || isFromExpress, !isTemplate, isExpressTemplate,  "class='char35'", "", "") %>
				     
		<%= widgetFactory.createTextField( "BenPhoneNumber", "ApprovalToPayIssue.PhoneNumber", termsPartySel.getAttribute("phone_number").toString(), "20", isReadOnly || isFromExpress, false, isExpressTemplate, "", "regExp:'[0-9]+'","") %>
	 <%-- /* Rel 8400 @ KMehta IR-T36000023613 Start*/ --%>
		<%
		
		String buyerIdentifier= "app_terms_party_oid,AppName,AppAddressLine1,AppAddressLine2,AppCity,AppStateProvince,AppCountry,AppPostalCode,Buyer,AppUserDefinedField1,AppUserDefinedField2,AppUserDefinedField3,AppUserDefinedField4";
		String buyerSectionName ="applicant";
		String buyerSearchHtml = widgetFactory.createPartySearchButton(buyerIdentifier,buyerSectionName,isReadOnly,TradePortalConstants.APPLICANT,false);
		String buyerClearHtml = "";
		if (!isReadOnly && isTemplate) {
			buyerClearHtml = widgetFactory.createPartyClearButton("ClearBuyerButton", "clearBuyer()", false,"");
		}
		%>
		<%-- <%= widgetFactory.createSubsectionHeader( "ApprovalToPayIssue.Buyer") %> --%>
		<%= widgetFactory.createSubsectionHeader( "ApprovalToPayIssue.Buyer",false, false, isExpressTemplate,(buyerSearchHtml+buyerClearHtml))%>
		<%-- /* Rel 8400 @ KMehta IR-T36000023613 End*/ --%>
		<div class="formItem">
		<%
		String addressSearchIndicator = null;
	    addressSearchIndicator = termsPartyBuy.getAttribute("address_search_indicator");
	    if (!(isReadOnly || isFromExpress) && !addressSearchIndicator.equals("N"))
	    {
	    	if(!isReadOnly && corpOrgHasMultipleAddresses)
	    	{ %>
	    	   <button data-dojo-type="dijit.form.Button"  name="AddressSearch" id="AddressSearch" type="button">
               <%=resMgr.getText("common.searchAddressButton",TradePortalConstants.TEXT_BUNDLE)%>
               <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
	               var corpOrgOid = <%=corpOrgOid%>;
					require(["t360/common", "t360/partySearch"], function(common, partySearch){
				   		partySearch.setPartySearchFields('<%=TradePortalConstants.ATP_BUYER%>');
	         			common.openCorpCustAddressSearchDialog(corpOrgOid);
	         		});
               </script>
            </button>
			<% 
	    	}
	    }
	    %>
	    </div>
		<% String buyerAddress = termsPartyBuy.buildAddress(false, resMgr); %>
		<table style="margin-left: 7px;" border='0' width="100%"><tr>
            <%if(!isTemplate){%>
            <td style="vertical-align:text-top;" bgcolor=''><span class="asterisk">*</span></td>
            <td bgcolor='' width="100%">
				<%-- Srinivasu_D  T36000019515 Rel8.4 01/23/2014 - start - changing the textarea to autoresize --%>
		<%-- <%= widgetFactory.createTextArea( "Buyer", "", buyerAddress, true, true, isExpressTemplate, "style='width:auto;' rows='4' cols='43'", "","none" ) %> --%>
				<%= widgetFactory.createAutoResizeTextArea("Buyer", "", buyerAddress, true, true,isExpressTemplate, "class='buyerAutoHeightTxtAreaQtrWidthNoSidebar' style='overflow-x: hidden;' rows='4' cols='45'", "", "")%>
				<%-- Srinivasu_D  T36000019515 Rel8.4 01/23/2014 - End --%>
			</td> 
			<%}else {%>
				<td>
				<span class="formItemWithIndent3">
			<%--	<%= widgetFactory.createTextArea( "Buyer", "", buyerAddress, true, true, isExpressTemplate, "style='width:auto;' rows='4' cols='43'", "","none" ) %> --%>

				<%= widgetFactory.createAutoResizeTextArea("Buyer", "", buyerAddress, true, true,isExpressTemplate, "class='buyerAutoHeightTxtAreaQtrWidthNoSidebar' style='overflow-x: hidden;' rows='4' cols='45'", "", "")%>

				</span>
				</td> 
			<%}%>
            </tr>
        </table>
		<%
        secureParms.put("app_terms_party_oid", 
                        termsPartyBuy.getAttribute("terms_party_oid"));
		%>
		<div style="display:none">
        <input type=hidden name="AppTermsPartyType" 
               value="<%=TradePortalConstants.ATP_BUYER%>">
        <input type=hidden name="AppName" 
               value="<%=termsPartyBuy.getAttribute("name")%>">
        <input type=hidden name="AppAddressLine1"
               value="<%=termsPartyBuy.getAttribute("address_line_1")%>">
        <input type=hidden name="AppAddressLine2"
               value="<%=termsPartyBuy.getAttribute("address_line_2")%>">
        <input type=hidden name="AppCity"
               value="<%=termsPartyBuy.getAttribute("address_city")%>">
        <input type=hidden name="AppStateProvince"
             value="<%=termsPartyBuy.getAttribute("address_state_province")%>">
        <input type=hidden name="AppCountry"
               value="<%=termsPartyBuy.getAttribute("address_country")%>">
        <input type=hidden name="AppPostalCode"
               value="<%=termsPartyBuy.getAttribute("address_postal_code")%>">
        		<%=widgetFactory.createTextField("AppOTLCustomerId","",termsPartyBuy.getAttribute("OTL_customer_id"),"35",false,false,false," type=hidden ","", "")%>
        <input type=hidden name="AppAddressSeqNum"
               value="<%=termsPartyBuy.getAttribute("address_seq_num")%>">

		<input type=hidden name="AppAddressSearchIndicator"
	               value="<%=termsPartyBuy.getAttribute("address_search_indicator")%>">
	              
	         <input type=hidden name="app_terms_party_oid"
			value="<%=termsPartyBuy.getAttribute("terms_party_oid")%>"> 
	         <input type=hidden name="AppUserDefinedField1"
			value="<%=termsPartyBuy.getAttribute("user_defined_field_1")%>">
			<input type=hidden name="AppUserDefinedField2"
			     value="<%=termsPartyBuy.getAttribute("user_defined_field_2")%>">
			<input type=hidden name="AppUserDefinedField3"
			     value="<%=termsPartyBuy.getAttribute("user_defined_field_3")%>">
			<input type=hidden name="AppUserDefinedField4"
			     value="<%=termsPartyBuy.getAttribute("user_defined_field_4")%>">
		</div>
		<%= widgetFactory.createTextField( "ApplRefNum", "transaction.BuyerRefNumber", terms.getAttribute("reference_number"), "30", isReadOnly, false, false, "class='char35'", "", ""  ) %>
		
	</div>	
	
	<div class="columnRight">
	
			<%
			String insuringPartySearchHtml = "";
			String insuringPartyClearHtml = "";
			String identifier="adv_terms_party_oid,AdvName,AdvAddressLine1,AdvAddressLine2,AdvCity,AdvStateProvince,AdvCountry,AdvPostalCode,InsuringParty,AdvOTLCustomerId,,AdvUserDefinedField1,AdvUserDefinedField2,AdvUserDefinedField3,AdvUserDefinedField4";
			String section="insured";
			if (!(isReadOnly || isFromExpress)) {
				if(!isReadOnly)
				{
					insuringPartySearchHtml = widgetFactory.createPartySearchButton(identifier,section,false,TradePortalConstants.INSURING_PARTY,false);
				}	
				if (!isTemplate) {
					insuringPartyClearHtml = widgetFactory.createPartyClearButton( "ClearAdvButton", "clearInsuringParty();",false,"");
				}	
			}   
			 %>
			 <%= widgetFactory.createSubsectionHeader( "ApprovalToPayIssue.InsuringParty",false, false, isExpressTemplate, (insuringPartySearchHtml+insuringPartyClearHtml) ) %>
			 
			 
			<% String insuringPartyAddress = termsPartyIns.buildAddress(false, resMgr); %>
			<%-- Srinivasu_D  T36000019515 Rel8.4 01/23/2014 - start - changing the textarea to autoresize --%>
			<%--
			<%= widgetFactory.createTextArea( "InsuringParty", "", insuringPartyAddress, true,!isTemplate, false, "rows='4' cols='43'", "","" ) %>
			--%>
			<%= widgetFactory.createAutoResizeTextArea("InsuringParty", "", termsPartyIns.buildAddress(false, resMgr), true, !isTemplate,false, "class='autoHeightTxtAreaQtrWidthNoSidebar' style='overflow-x: hidden;' rows='4' cols='45'", "", "")%>
			<%-- Srinivasu_D  T36000019515 Rel8.4 01/23/2014 - End --%>
				<%
			    secureParms.put("adv_terms_party_oid", 
			                    termsPartyIns.getAttribute("terms_party_oid"));
				%>
				<div style="display:none">
				<input type=hidden name="AdvTermsPartyType" 
		               value="<%=TradePortalConstants.INSURING_PARTY%>">
			    <input type=hidden name="adv_terms_party_oid" 
			           value="<%=termsPartyIns.getAttribute("terms_party_oid")%>">
			    <input type=hidden name="AdvName" 
			           value="<%=termsPartyIns.getAttribute("name")%>">
			    <input type=hidden name="AdvAddressLine1"
			           value="<%=termsPartyIns.getAttribute("address_line_1")%>">
			    <input type=hidden name="AdvAddressLine2"
			           value="<%=termsPartyIns.getAttribute("address_line_2")%>">
			    <input type=hidden name="AdvCity"
			           value="<%=termsPartyIns.getAttribute("address_city")%>">
			    <input type=hidden name="AdvStateProvince"
			         value="<%=termsPartyIns.getAttribute("address_state_province")%>">
			    <input type=hidden name="AdvCountry"
			           value="<%=termsPartyIns.getAttribute("address_country")%>">
			    <input type=hidden name="AdvPostalCode"
			           value="<%=termsPartyIns.getAttribute("address_postal_code")%>">
        		<%=widgetFactory.createTextField("AdvOTLCustomerId","",termsPartyIns.getAttribute("OTL_customer_id"),"35",false,false,false," type=hidden ","", "")%>
				
				<input type=hidden name="AdvUserDefinedField1"
				   value="<%=termsPartyIns.getAttribute("user_defined_field_1")%>">
				<input type=hidden name="AdvUserDefinedField2"
				     value="<%=termsPartyIns.getAttribute("user_defined_field_2")%>">
				<input type=hidden name="AdvUserDefinedField3"
				     value="<%=termsPartyIns.getAttribute("user_defined_field_3")%>">
				<input type=hidden name="AdvUserDefinedField4"
				     value="<%=termsPartyIns.getAttribute("user_defined_field_4")%>">
		     
				</div>	
				
				<%= widgetFactory.createSubsectionHeader( "ApprovalToPayIssue.DetailedInformation") %>
			     
			     <%
			     options = Dropdown.createSortedCurrencyCodeOptions(
		                terms.getAttribute("amount_currency_code"), loginLocale);
			     %>
			     
			     <div>
			     <%-- <%= widgetFactory.createCurrencySelectField( "TransactionCurrency", "transaction.Currency", options,"", isReadOnly, !isTemplate, false) %> --%>
			     <%=widgetFactory.createSelectField("TransactionCurrency","transaction.Currency", " ", options, isReadOnly, !isTemplate,false, "style=\"width: 50px;\"", "", "inline")%>
			     <%= widgetFactory.createAmountField( "TransactionAmount", "transaction.Amount", displayAmount, currency, isReadOnly, !isTemplate, false, "class='char12'", "readOnly:" +isReadOnly, "inline") %> 
			     <div style="clear:both;"></div>
			     </div>
			    <%
					 codesToExclude=new Vector();
			        codesToExclude.addElement(TradePortalConstants.COUNTRY_OF_BENEFICIARY);
			        codesToExclude.addElement(TradePortalConstants.AT_CONFIRMATION_BANK_COUNTERS);
					 options = Dropdown.createSortedRefDataOptions(
							  TradePortalConstants.PLACE_OF_EXPIRY_TYPE, 
							  terms.getAttribute("place_of_expiry"), 
							  loginLocale,codesToExclude);
			        codesToExclude=null;
				     %>
			<div>     
			<table>
	            <tr>
		            <td>	
			     		<%= widgetFactory.createLabel("ExpiryDate","ApprovalToPayIssue.ExpiryDate",isReadOnly, !isTemplate, false,"inline")%>
			     		<br/>
			     	</td>
				<td>
				<div class="formItem">
			     	 <%= widgetFactory.createLabel("","common.ExpiryPlace",false,false,false,"none")%>
			     	 <%= widgetFactory.createNote("ApprovalToPayIssue.ExpiryPlace1")%>
			     	 <div  style="margin-top: -7px;">
			     	 <%= widgetFactory.createNote("ApprovalToPayIssue.ExpiryPlace2")%>
				     	 <%if(!isReadOnly){ %>
				     	 <a href='javascript:openOtherConditionsDialog("AddlConditionsText","SaveTrans", "<%=resMgr.getText("ApprovalToPayIssue.OtherCondLink",TradePortalConstants.TEXT_BUNDLE)%>");'> <%=resMgr.getText("ApprovalToPayIssue.OtherCondLink",TradePortalConstants.TEXT_BUNDLE)%></a>
							 <%--<%= widgetFactory.createNote("<a href='javascript:openOtherConditionsDialog(\"AddlConditionsText\",\"SaveTrans\",\"Other Conditions\");' >Other Conditions</a>")%>--%>
							 <%}else{ %>
							 	<%= widgetFactory.createNote("RequestAdviseIssue.OtherCondLink")%>							 	
							 <%} %>
						 <%= widgetFactory.createInlineLabel("","common.ExpiryPlaceOtherEnd")%>
					</div>
				</div>
				</td>
			     </tr>
			     <tr>
			     	<td>
			     		<div  style="margin-top: -2px;"><%= widgetFactory.createDateField("ExpiryDate", "", StringFunction.xssHtmlToChars(terms.getAttribute("expiry_date")), isReadOnly, !isTemplate, false,  "class='char6'", dateWidgetOptions, "inline") %></div>					
				     </td>					 
			     	<td>
			     		<div  style="margin-top: -2px;"><%= widgetFactory.createSelectField( "PlaceOfExpiry", "", " ", options, isReadOnly, !isTemplate, false,  "class='char12'", "", "inline" ) %></div>			     	
			     	</td>	
			  </tr>
			</table>
		</div>   
			     <div class = "formItem">
			     <%= widgetFactory.createSubLabel("ApprovalToPayIssue.AmtTolerance") %>
			     </div>
			     
			     <div   style="margin-top: -10px;" class = "formItem">
					 <%= widgetFactory.createSubLabel( "transaction.Plus") %>
					<%if(!isReadOnly){ %>
					<%= widgetFactory.createPercentField( "AmountTolerancePlus", "", terms.getAttribute("amt_tolerance_pos"), isReadOnly,
												false, false, "", "", "none") %>
					
					<%}else {%>
						<b><%=terms.getAttribute("amt_tolerance_pos") %></b>
					<%} %>					
					<%= widgetFactory.createSubLabel( "transaction.Percent") %>
					&nbsp;&nbsp;
					<%= widgetFactory.createSubLabel( "transaction.Minus") %>
					<%if(!isReadOnly){ %>
					<%= widgetFactory.createPercentField( "AmountToleranceMinus", "", terms.getAttribute("amt_tolerance_neg"), isReadOnly,
												false, false, "", "", "none") %>					
					<%}else {%>
						<b><%=terms.getAttribute("amt_tolerance_neg") %></b>
					<%} %>	
					
					<%= widgetFactory.createSubLabel( "transaction.Percent") %>
					<div style="clear:both;"></div>
				 </div>
				 
				 <%= widgetFactory.createSubsectionHeader( "ApprovalToPayIssue.BankCharges",isReadOnly, !isTemplate, isExpressTemplate, "" ) %>
					<div class="formItem">
					<%= widgetFactory.createRadioButtonField( "BankChargesType", TradePortalConstants.CHARGE_OUR_ACCT, "ApprovalToPayIssue.AllOurAcct", TradePortalConstants.CHARGE_OUR_ACCT, bankChargesType.equals(TradePortalConstants.CHARGE_OUR_ACCT), isReadOnly || isFromExpress ) %>
					<br>
					
					<%= widgetFactory.createRadioButtonField( "BankChargesType", TradePortalConstants.CHARGE_BEN_FOR_OUTSIDE, "ApprovalToPayIssue.AllOutside", TradePortalConstants.CHARGE_BEN_FOR_OUTSIDE, bankChargesType.equals(TradePortalConstants.CHARGE_BEN_FOR_OUTSIDE), isReadOnly || isFromExpress ) %>
					<br>
					
					<%= widgetFactory.createRadioButtonField( "BankChargesType", TradePortalConstants.CHARGE_OTHER, "ApprovalToPayIssue.OtherDetails", TradePortalConstants.CHARGE_OTHER, bankChargesType.equals(TradePortalConstants.CHARGE_OTHER), isReadOnly || isFromExpress ) %>
					<span class="label">
					<%if(!isReadOnly){ %>
						<a href='javascript:openOtherConditionsDialog("AddlConditionsText","SaveTrans", "<%=resMgr.getText("ApprovalToPayIssue.OtherCondLink",TradePortalConstants.TEXT_BUNDLE)%>");'> <%=resMgr.getText("ApprovalToPayIssue.OtherCondLink",TradePortalConstants.TEXT_BUNDLE)%></a>
					<%}else{ %>			
						 	<%=resMgr.getText("ApprovalToPayIssue.OtherCondLink",TradePortalConstants.TEXT_BUNDLE)%>
					<%} %>						 
					</span>			         
					<%= widgetFactory.createSubLabel("ImportDLCIssue.Below") %>
					</div>
					<br>
			
</div>	
<div style="clear:both;"></div>

<% if (!isReadOnly){ %> 
		<%=widgetFactory.createHoverHelp("seller", "PartySearchIconHoverHelp") %> 
		<%=widgetFactory.createHoverHelp("PlaceOfExpiry", "PlaceOfExpiryATPHoverHelp") %>
		<%=widgetFactory.createHoverHelp("insured", "PartySearchIconHoverHelp") %>
		<%=widgetFactory.createHoverHelp("ClearAdvButton", "PartyClearIconHoverHelp") %>
		<%= widgetFactory.createHoverHelp("AmountToleranceMinus","ApprovalToPayIssue.Minus")%>
		<%= widgetFactory.createHoverHelp("AmountTolerancePlus","ApprovalToPayIssue.Plus")%>
		<%= widgetFactory.createHoverHelp("ExpiryDate","ApprovalToPayIssue.ExpiryDate")%>
<%} %>		     
 <div style="clear:both;"></div>


<script LANGUAGE="JavaScript">

  function clearInsuringParty() {
	document.TransactionATP.AdvName.value = "";
    document.TransactionATP.AdvAddressLine1.value = "";
    document.TransactionATP.AdvAddressLine2.value = "";
    document.TransactionATP.AdvCity.value = "";
    document.TransactionATP.AdvStateProvince.value = "";
    document.TransactionATP.AdvCountry.value = "";
    document.TransactionATP.AdvPostalCode.value = "";
    document.TransactionATP.AdvOTLCustomerId.value = "";
    document.TransactionATP.InsuringParty.value = "";
  }

  function checkSellerName(originalName)
  {
     if (document.forms[0].BenName.value != originalName)
     {
        <%-- document.forms[0].BenOTLCustomerId.value = ""; --%>
     }
  }

  function clearBuyer() 
  {
    document.TransactionATP.AppName.value = "";
    document.TransactionATP.AppAddressLine1.value = "";
    document.TransactionATP.AppAddressLine2.value = "";
    document.TransactionATP.AppCity.value = "";
    document.TransactionATP.AppStateProvince.value = "";
    document.TransactionATP.AppCountry.value = "";
    document.TransactionATP.AppPostalCode.value = "";
    document.TransactionATP.AppOTLCustomerId.value = "";
    document.TransactionATP.Buyer.value = "";
  }

</script>
