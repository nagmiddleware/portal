<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
*******************************************************************************
                     Funds Transfer Request Page - General section

  Description:
    Contains HTML to create the Funds Transfer Request General section.  

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transaction-FTBA.frag" %>
*******************************************************************************
--%>



<%
  String requestMarketRateVisibility = "hidden";
  DocumentHandler fxOnlineResultDoc = null; 

  if(InstrumentServices.isNotBlank(terms.getAttribute("debit_account_oid"))){
  	   fxOnlineResultDoc = DatabaseQueryBean.getXmlResultSet("SELECT g.fx_online_avail_ind FROM account a, operational_bank_org o, bank_organization_group g WHERE a.a_op_bank_org_oid = o.organization_oid AND o.a_bank_org_group_oid = g.organization_oid and a.account_oid=?", false, new Object[]{terms.getAttribute("debit_account_oid")});
   	   if (fxOnlineResultDoc != null) {
    		if (TradePortalConstants.INDICATOR_YES.equals(fxOnlineResultDoc.getAttribute("/ResultSetRecord(0)/FX_ONLINE_AVAIL_IND"))) {
    		    requestMarketRateVisibility = "visible";
		}
    	}
  }

%>

<%  
	if (!isTemplate) { 
		String confType = "Confidential TBA";
%> 
		<div class="searchDivider"></div>
		<%@ include file="TransactionConfInd.frag" %>  
<%	} %> 

<div class="columnLeft">
	<%= widgetFactory.createSubsectionHeader( "TransferBetweenAccounts.PaymentTerms",isReadOnly, false,false,"") %>    
	
	<div class="formitem">
		<% options = Dropdown.createSortedCurrencyCodeOptions(terms.getAttribute("amount_currency_code"), loginLocale);%>
	    <%=widgetFactory.createSelectField("amount_currency_code","TransferBetweenAccounts.Currency"," ",options,isReadOnly,!isTemplate,false, "class='char4'","","inline")%>
		<%//= widgetFactory.createTextField( "amount", "TransferBetweenAccounts.Amount", displayAmount, "24", isReadOnly, !isTemplate, false, "class='char16'", "", "inline") %> 
		<%= widgetFactory.createAmountField( "amount", "TransferBetweenAccounts.Amount", displayAmount, currency, isReadOnly, !isTemplate, false, "class='char16'", "", "inline") %>
		<div style="clear:both;"></div>
	</div>

	<%= widgetFactory.createSubsectionHeader( "TransferBetweenAccounts.AccountDetails",isReadOnly, false,false,"") %>
    <div class="formitem">
    <% options = ListBox.createOptionList(fromAccountDoc, "ACCOUNT_OID", "ACCOUNT",terms.getAttribute("debit_account_oid"),null); %>
    <% out.println(widgetFactory.createSelectField("from_account_oid", "TransferBetweenAccounts.FromAccount", " ", options, isReadOnly,!isTemplate,false,"onChange=\"updateCurr();reqMarketRateVis()\"","","")); %>		 
   <%--start --%>   
	<%	
	//AAlubala - IR#BEUL040548205 - 04/14/11 - Display correct account owner when readonly mode
	String ownerToDisplay="";
	if(isReadOnly){
	
			String accntOwnerSQL = "select othercorp_customer_indicator from account where account_oid= ? ";
			DocumentHandler aoSQL = DatabaseQueryBean.getXmlResultSet(accntOwnerSQL, false, new Object[]{terms.getAttribute("debit_account_oid")});
			String accInd = null;
			if ( aoSQL != null){
				accInd = aoSQL.getAttribute("ResultSetRecord/OTHERCORP_CUSTOMER_INDICATOR");
			}
			if(TradePortalConstants.INDICATOR_YES.equals(accInd)){
				String accSQL = "select name from corporate_org where organization_oid = (select other_account_owner_oid from account where account_oid=?)";										
				DocumentHandler ownerOrg = DatabaseQueryBean.getXmlResultSet(accSQL, false, new Object[]{terms.getAttribute("debit_account_oid")});     			
				if (ownerOrg != null)
    				ownerToDisplay = ownerOrg.getAttribute("ResultSetRecord/NAME");	
			}else{
				ownerToDisplay = userSession.getOrganizationName();
			}
	}		
	%>
    
    <%= widgetFactory.createTextField("accntOwnerFrom","FundsTransferRequest.AccountOwner",ownerToDisplay,"22",true)%>

   <%--end --%>
 
   
   <% options = ListBox.createOptionList(toAccountDoc, "ACCOUNT_OID", "ACCOUNT",terms.getAttribute("credit_account_oid"),null); %>
   <% out.println(widgetFactory.createSelectField("to_account_oid", "TransferBetweenAccounts.ToAccount", " ", options, isReadOnly,!isTemplate,false,"onChange=\'updateCurr();\'","","")); %>                            
    
   <%--start --%>
   
	<%	
	//AAlubala - IR#BEUL040548205 - 04/14/11 - Display correct account owner when readonly mode
	String ownerToDisplay2="";
	if(isReadOnly){
			String accntOwnerSQL = "select othercorp_customer_indicator from account where account_oid=?";
			DocumentHandler aoSQL = DatabaseQueryBean.getXmlResultSet(accntOwnerSQL, false, new Object[]{terms.getAttribute("credit_account_oid")});
			String accInd = null;
			if (aoSQL != null){
				accInd = aoSQL.getAttribute("ResultSetRecord/OTHERCORP_CUSTOMER_INDICATOR");
			}
			if(TradePortalConstants.INDICATOR_YES.equals(accInd)){
				String accSQL = "select name from corporate_org where organization_oid = (select other_account_owner_oid from account where account_oid=?)";										
				DocumentHandler ownerOrg = DatabaseQueryBean.getXmlResultSet(accSQL, false, new Object[]{terms.getAttribute("credit_account_oid")});    
				if (ownerOrg != null) 			
    				ownerToDisplay2 = ownerOrg.getAttribute("ResultSetRecord/NAME");	
			}else{
				ownerToDisplay2 = userSession.getOrganizationName();
			}
	}		
	%>
		
    <%= widgetFactory.createTextField("accntOwnerTo","FundsTransferRequest.AccountOwner",ownerToDisplay2,"22",true)%>
    <div>
        <%-- Nar CR 966 Rel 9.2 09/16/2014 modified widget to make it read-Only if it is template --%>
    	<%= widgetFactory.createDateField("payment_date", "TransferBetweenAccounts.DateOfTransfer",transaction.getAttribute("payment_date"), isReadOnly || isTemplate, !isTemplate, false,"class='char10'", "", "inline" ) %>
		<br><%= widgetFactory.createLabel("","TransferBetweenAccounts.DateOfTransferTxt",false,false,false,"","inline")%>
		<div style='clear: both;'></div>
	</div>
	<%= widgetFactory.createTextField( "transfer_description", "TransferBetweenAccounts.Description", terms.getAttribute("transfer_description"), "30", isReadOnly,!isTemplate, false, "", "", "" ) %><br>
	</div>
	</div>

<div class="columnRight">

<%= widgetFactory.createSubsectionHeader( "TransferBetweenAccounts.FEC",isReadOnly, false,false,"") %>	
	<div class="formItem">
              <%= widgetFactory.createTextField( "covered_by_fec_number", "TransferBetweenAccounts.FECCovered", terms.getAttribute("covered_by_fec_number"), "14", isReadOnly && !isFXeditable, false, false, "class='char8'", "", "inline" ) %>
      	      <%= widgetFactory.createNumberField( "fec_rate", "TransferBetweenAccounts.FECRate", terms.getAttribute("fec_rate"), "14", isReadOnly && !isFXeditable, false, false, "class='char8'", "constraints: {pattern: '##0.########', places:'0,8'}", "inline" ) %>
      	      <div style="clear:both"></div>
      </div>
      <div  class="formItem" id="requestMarketRate" >
   		
						  <%		      		 
						  if (InstrumentServices.isBlank(requestMarketRateInd)) requestMarketRateInd = TradePortalConstants.INDICATOR_NO;									   

						  out.print(widgetFactory.createCheckboxField("RequestMarketRateCheckBox",  "FundsTransferRequest.RequestMarketRate", requestMarketRateInd,requestMarketRateInd.equals(TradePortalConstants.INDICATOR_YES), isReadOnly,false, "onClick='updateReqMarketRate()'", "", "")); 

						  %>
						  
						  <%-- Vshah - IR#DEUL120562739 - 01/05/2012 - Rel7.1 - BEGIN - Added below hidden variable --%> 
						  <%-- Becuase you cannot unset this setting once it is set to 'Y'. when you un-check the box it does not send 'N' � it sends null --%> 
						  <%-- To resolve this, we have to use hidden field and javascript --%>
						  <input type="hidden" name="RequestMarketRate" value="<%=requestMarketRateInd%>" >

	  </div>		  
  
      <div>   		
   			<%= widgetFactory.createTextField( "transfer_fx_rate", "TransferBetweenAccounts.BaseToFXRate", 
   					terms.getAttribute("transfer_fx_rate"), "22", true, false, false, "style='width: 60px'", "", "inline" ) %>
	    		
    		<%= widgetFactory.createTextField( "display_fx_rate_method", "TransferBetweenAccounts.FXCalcMethod", 
    			terms.getAttribute("display_fx_rate_method"), "22", true, false, false, "style='width: 60px'", "", "inline" ) %>
    
    
    
    		
    		<%= widgetFactory.createTextField( "equivalent_exch_ccy", "TransferBetweenAccounts.EquivAmount", 
    			terms.getAttribute("equivalent_exch_amt_ccy")+"  "+displayExchAmount, "22", true, false, false, "style='width: 60px'", "", "inline" ) %>
   		
   			<div style="clear: both;"></div>
   		</div>
   		<input type="hidden" name="equivalent_exch_amount" value="<%=terms.getAttribute("equivalent_exch_amount")%>">
  <div style="clear:both;"></div>

</div>
 <div style="clear:both;"></div>
  <%-- MEer Rel 8.3 IR-19700 Repair reason section not shown for TBA when processed by bank  --%>


<%//-- CR-640/581 Rel 7.1.0 09/22/11  --Begin-- // %>
		<%
		 if ((TradePortalConstants.TRANS_STATUS_FX_THRESH_EXCEEDED.equals(transactionStatus) ||
			    TradePortalConstants.TRANS_STATUS_AUTH_PEND_MARKET_RATE.equals(transactionStatus)) && userSession.hasAccessToLiveMarketRate()) {
		
		   boolean isEditable = InstrumentServices.isEditableTransStatus(transactionStatus);
       	   boolean canAuthorize = SecurityAccess.canAuthorizeInstrument(userSession.getSecurityRights(), instrumentType, transactionType);
       	   boolean adminCustAccess = false;
	        boolean canRequestRate = false;          	   
	           //Vshah - Rel7.1 - IR#DAUL122659077 - 01/05/2012 - <BEGIN>
	           //Rel7.1 - IR#LUUM011049400 - <Begin>
	           //Comment out the below line and re-written the logic to populate "canRequestRate" 
	           //Based on the FX Online functionality of the Bank Group associated to the OP Org of the Debit Account.
	           
	           //boolean canRequestRate =  (TradePortalConstants.INDICATOR_YES.equals(terms.getAttribute("request_market_rate_ind"))) ? true : false ;
	           if (fxOnlineResultDoc != null) {
	    		    canRequestRate = (TradePortalConstants.INDICATOR_YES.equals(fxOnlineResultDoc.getAttribute("/ResultSetRecord(0)/FX_ONLINE_AVAIL_IND")))  ? true : false ;
	    	   }
	           //Rel7.1 - IR#LUUM011049400 - <End>
	           //Vshah - Rel7.1 - IR#DAUL122659077 - 01/05/2012 - <END>
       	   

       if (userSession.hasSavedUserSession())       {
			if (TradePortalConstants.ADMIN.equals(userSession.getSavedUserSessionSecurityType())) 
				adminCustAccess = true;
			else if (TradePortalConstants.INDICATOR_YES.equals((String)request.getAttribute("panelEnabled")))
				adminCustAccess = true;

 	} 
	       if (!isEditable && canAuthorize && !adminCustAccess) {
		
		%>
 
  <%--              
       <jsp:include page="/cashmanagement/fragments/MarketRate.jsp" >
		    <jsp:param name="selectedInstrument"     value='<%=selectedInstrument%>' />
		    <jsp:param name="showInfoText"     value='false' />
		</jsp:include>
 --%>		
			
 <%
   
 }	  }
	%>
	   <%//-- CR-640/581 Rel 7.1.0 09/22/11  --End-- // %>
	
	
	<div id="bankGroupNonVis" style="visibility: hidden; position: absolute;">
		<%
		String acctBankGroupSql = "select account_oid, a_bank_org_group_oid from operational_bank_org inner join account on organization_oid = a_op_bank_org_oid where deactivate_indicator != 'Y' and available_for_xfer_btwn_accts = 'Y' and p_owner_oid = ?";
		DocumentHandler acctBankGroupList = DatabaseQueryBean.getXmlResultSet(acctBankGroupSql.toString(), false, new Object[]{corpOrgOid});
		String acctBankGroupOptions = ListBox.createOptionList(acctBankGroupList, "ACCOUNT_OID", "A_BANK_ORG_GROUP_OID", options);
		out.println(InputField.createSelectField("acctBankGroupSelection", "", " ", acctBankGroupOptions, "ListText", isReadOnly));
		String bankGroupOid = "";
		// extract the bankGroupOid value from the selected option
		int abgOptSelectedIdx = acctBankGroupOptions.indexOf("selected");
		if (abgOptSelectedIdx > -1) {
			int abgOptStart = acctBankGroupOptions.indexOf(">", abgOptSelectedIdx);
			int abgOptEnd = acctBankGroupOptions.indexOf("</option>", abgOptSelectedIdx);	
			try {
				bankGroupOid = acctBankGroupOptions.substring(abgOptStart+1, abgOptEnd);
			} catch (Exception e) {
				bankGroupOid = "";
			}
	}
		%>
	


<%--AAlubala CR610  Rel 7.0.0.0 03/01/2011
IAZ 04/09/2011 - if OTHER_ACCOUNT_OWNER_OID is not empty, use it to find real owner of the account (subsidiary)
            - if it is empty, use a name of user's own corp's name
[BEGIN]
--%>

<%
//Build hidden fields containing the account name and corresponding account owner
//This will be used to display the owner of the account on the fly i.e.
//depending on the selected account. This applies to the situation where the 
//selected account belongs to another corporation
//
//FromAccount and ToAccount lists are built from the same sql, so, just loop on one of them
//but build two separate hidden fields one for each
//

String corporateOrgToDisplay = "";
Vector vector = fromAccountDoc.getFragments("/ResultSetRecord");
int numItems = vector.size();
String accntSQL = "";
DocumentHandler ownerCorpOrg;

DocumentHandler userCorpOrg = 
	DatabaseQueryBean.getXmlResultSet("select name from corporate_org where organization_oid = ?", false, new Object[]{userSession.getOwnerOrgOid()});
String userCorpName = "";
if 	(userCorpOrg != null)
 userCorpName = userCorpOrg.getAttribute("ResultSetRecord/NAME");
		
for (int i = 0; i < numItems; i++) {
	DocumentHandler doc1 = (DocumentHandler) vector.elementAt(i);
	String acct_oid1 = "";
	String accnt = "";
	String display = "";
	String accntOwnerCorpID = "";
	try {
		acct_oid1 = doc1.getAttribute("/ACCOUNT_OID");
	} catch (Exception e) {
		acct_oid1 = "";
	}
	try {
		accnt = doc1.getAttribute("/ACCOUNT");				
	} catch (Exception e) {
		accnt = "";
	}
	try {
		accntOwnerCorpID = doc1.getAttribute("/OTHER_ACCOUNT_OWNER_OID");				
	} catch (Exception e) {
		accntOwnerCorpID = "";
	}	
	display=acct_oid1;
	if(InstrumentServices.isBlank(accnt)){
		%>
		<input type=hidden name="<%=display%>" id="from<%=display%>" value="<%=corporateOrgToDisplay%>" />
		<input type=hidden name="<%=display%>" id="to<%=display%>" value="<%=corporateOrgToDisplay%>" />
		<%
	}else{
	        
		String nameToDisplay ="";
	        if (InstrumentServices.isNotBlank(accntOwnerCorpID))
	        {
			//retrieve the account owner organization's name
			accntSQL = "select name from corporate_org where organization_oid = ?"; 
			ownerCorpOrg = DatabaseQueryBean.getXmlResultSet(accntSQL, false, new Object[]{accntOwnerCorpID});
			nameToDisplay = ownerCorpOrg.getAttribute("ResultSetRecord/NAME");
		}
		else
		{
			nameToDisplay = userCorpName;
		}
		%>
		<input type=hidden name="<%=display%>" id="from<%=display%>" value="<%=nameToDisplay%>" />
		<input type=hidden name="<%=display%>" id="to<%=display%>" value="<%=nameToDisplay%>" />
		<%				
	}	
}
%>
<div id="MarketRate" ></div>
</div>  <%-- MEer Rel 8.3 IR-19700 Repair reason section not shown for TBA when processed by bank --%>
  
<%

boolean isListView = false;

%>
<%--CR610 [end] --%>
   
   <script LANGUAGE="JavaScript">
   
   function getMarketRate(){

            require(["t360/dialog"], function(dialog){
			dialog.open('MarketRate', '<%=resMgr.getText("MarketRate.dialogTitle",TradePortalConstants.TEXT_BUNDLE)%>',
			'marketRateDialog.jsp',
			  ['selectedInstrument','showInfoText'],['<%=selectedInstrument%>','false'],
			'select', null);
			});
		}
     
  
function updateReqMarketRate()
	{		
	    if (document.forms[0].RequestMarketRateCheckBox.checked == true)
	    {
	    	document.forms[0].RequestMarketRate.value = 'Y';
	    	document.forms[0].covered_by_fec_number.value='';
	    	document.forms[0].fec_rate.value=''; 
	    	
	    }
	    else
	    {
	    	document.forms[0].RequestMarketRate.value = 'N';

	    }
	    
    }
  
 

function reqMarketRateVis(){

require(["dijit/registry", "dojo/ready"],
		function(registry, ready ) {
		
	var accountOID = registry.byId("from_account_oid").value;
	var acctBankGroupSelect = document.getElementsByName("acctBankGroupSelection")[0];
	var oldBankGroupOID = null;
	var newBankGroupOID = null;
	for (var j=0; j<acctBankGroupSelect.length; j++) {
		if (acctBankGroupSelect.value == acctBankGroupSelect.options[j].value) {
			oldBankGroupOID = acctBankGroupSelect.options[j].text;
		}
		if (accountOID == acctBankGroupSelect.options[j].value) {
			newBankGroupOID = acctBankGroupSelect.options[j].text;
		}
		if (oldBankGroupOID != null && newBankGroupOID != null) {
			break;
		}
	}
	
	acctBankGroupSelect.value = accountOID;	
	if (oldBankGroupOID != newBankGroupOID) {
		if(newBankGroupOID >''){
			executeChangeBankGroup(newBankGroupOID);
		} else {
			document.getElementById('requestMarketRate').style.visibility='hidden';
			document.getElementById('requestMarketRate').style.display='none';
		}		
	}
	document.forms[0].RequestMarketRateCheckBox.checked = false;
			
});
}


function executeChangeBankGroup(bankGroupOID) {
 require(["dojo/_base/xhr"], function(xhr) {
	var remoteUrl  = "/portal/transactions/Transaction-ISS-ChangeDebitAccount.jsp?BankGroupOID=" + bankGroupOID + "&";
	xhr.get({
              url: remoteUrl,
			  handleAs: "json",
			  load: function(data) {
			  	<%-- var fxOnlineAvailableDiv = document.getElementById("fxOnlineAvailableNonVis"); --%>
				<%-- fxOnlineAvailableDiv.innerHTML = data; --%>
				<%-- var fx = document.getElementById("hidden_fxInd").value; --%>
				var fx = data.hidden_fxInd;

				if(fx=='Y'){		
					document.getElementById('requestMarketRate').style.visibility='visible';
					document.getElementById('requestMarketRate').style.display='block';
				}
				else{
					document.getElementById('requestMarketRate').style.visibility='hidden';
					document.getElementById('requestMarketRate').style.display='none';
				}
			
			},
			 error: function(error){
			     <%-- todo --%>
			}
		});
	});
}

function uncheckReqMarketRate()
	{	
		if (document.forms[0].RequestMarketRateCheckBox.checked == true)
		{
			if ((document.forms[0].covered_by_fec_number.value != "") || (document.forms[0].fec_rate.value != ""))
			{
				document.forms[0].RequestMarketRateCheckBox.checked = false;
				document.forms[0].RequestMarketRate.value = 'N';
			}	
		}
	}

</script>
