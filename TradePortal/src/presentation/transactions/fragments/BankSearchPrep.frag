<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
*******************************************************************************
                                 Bank Search Prep

  Description:
     This included JSP helps support generic bank search logic.  Basically it 
  defines a convenience method and a hidden input field.

  This JSP is used in conjunction with Bank Search Buttons defined using the
  BankSearchButton.jsp.  See that page for additional comments.

  For each search button, include code similar to the following.
        <jsp:include page="/common/PartySearchButton.jsp">
           <jsp:param name="showButton" value="<%=!isReadOnly%>" />
           <jsp:param name="name" value="SearchBenButton" />
           <jsp:param name="image" value='common.SearchBeneficiaryBankImg' />
           <jsp:param name="text" value='common.SearchText' />
           <jsp:param name="width" value="140" />
           <jsp:param name="bankType" 
                      value="<%=TradePortalConstants.BENEFICIARY%>" />
        </jsp:include>

   When the user clicks a search bank button, the search bank type hidden 
   field is given the value assigned to the button (see jsp:include above).
   This is the passed on to the party search page.

   Upon return to your page, check the /In/BankSearchInfo section for a party
   oid.  If present, it means one was selected.

*******************************************************************************
--%>

<SCRIPT LANGUAGE="JavaScript">
  function setBankSearchFields(bankType) {
    document.forms[0].SearchBankType.value = bankType;
    return true;
  }
</SCRIPT>

<input type=hidden name=SearchBankType value="">
