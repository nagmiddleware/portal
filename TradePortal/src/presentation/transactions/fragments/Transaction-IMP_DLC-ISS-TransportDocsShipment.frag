<%--
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
*******************************************************************************
        Approval to Pay Issue Page - Transport Document(s)/Shipment section

  Description:
    Contains HTML to create the Approval to Pay Issue Transport Document(s)/ Shipment 
    section.  

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transaction-ATP-ISS-TransportDocsShipment.frag" %>
*******************************************************************************
--%>
<script LANGUAGE="JavaScript">
function checkNotifyPartyName(originalName)
{
  if (document.forms[0].NotName.value != originalName)
  {
    document.forms[0].NotOTLCustomerId.value = "";
  }
}

function checkOtherConsigneePartyName(originalName)
{
  if (document.forms[0].OthName.value != originalName)
  {
    document.forms[0].OthOTLCustomerId.value = "";
  }
}

function pickTransDocCheckbox() 
{
	if (dijit.byId("TransDocType").value > ''  ||
      dijit.byId("TransDocOrigs").value > ''  ||
      dijit.byId("TransDocCopies").value > '')
    dijit.byId("TransDocInd").set('checked',true);
}

function pickConsignToOrderRadio() 
{
  if (dijit.byId("ConsignToOrder").value > '')
    dijit.byId("CONSIGN_ORDER").set('checked',true);
    if(dijit.byId("CONSIGN_ORDER").checked==true)
    dijit.byId("ConsignToParty").set('value',"");
  
}

function pickConsignToPartyRadio() 
{
  if (dijit.byId("ConsignToParty").value > '')
	  dijit.byId("CONSIGN_PARTY").set('checked',true);
	  
	  if(dijit.byId("CONSIGN_PARTY").checked==true)
	  dijit.byId("ConsignToOrder").set('value',"");
}

</script>

<%
// Variables used to display the shipment tabs
String     shipmentTabPressed     = String.valueOf(shipmentNumber);
String     shipmentTabNum	      = "0";  	
String     displayTabsOnTop       = "true";
String     shipmentTabColorTag;
String     textColorTag;
String     spacerColorTag;
boolean    isStructuredPO         = false;
boolean    hasStructuredPOs       = false;

if (numShipments > 1)
{
  shipmentTabColorTag = "class=BankColor";
  textColorTag        = "class=ControlLabelWhite";
  spacerColorTag      = "class=ControlLabelWhite";
}
else
{
  shipmentTabColorTag = "class=ColorGrey";
  textColorTag        = "class=ControlLabel";
  spacerColorTag      = "class=ColorGrey";
}

// Get some attributes for radio button fields.  We need to refer to this
// value frequently so do getAttribute once.
String consignType = shipmentTerms.getAttribute("trans_doc_consign_type");
String markedFreight = shipmentTerms.getAttribute("trans_doc_marked_freight");

corpOrgOid = instrument.getAttribute("corp_org_oid");
corporateOrg = beanMgr.createBean(CorporateOrganizationWebBean.class, "CorporateOrganization");
corporateOrg.setAttribute("organization_oid", corpOrgOid);
corporateOrg.getDataFromAppServer();

if(TradePortalConstants.PO_UPLOAD_STRUCTURED.equals(corporateOrg.getAttribute("po_upload_format")))
	  isStructuredPO = true;

%>

		
		

		<%= widgetFactory.createWideSubsectionHeader( "ImportDLCIssue.LCShipTerms") %>
				 <% if(isExpressTemplate){%>
		   <span class='hashMark'>#</span>
  	         <%}%>	
		<%= widgetFactory.createCheckboxField( "PartialAllowed", "ImportDLCIssue.PartialAllowed",  terms.getAttribute("partial_shipment_allowed").equals(TradePortalConstants.INDICATOR_YES), isReadOnly || isFromExpress, false, "", "", "none") %>
		<% 
			if(isExpressTemplate){
		%>
			<br><br>
		<%}%>
		<div class = "formItem">
		<%=resMgr.getText("ImportDLCIssue.PresentDocs",TradePortalConstants.TEXT_BUNDLE)%>
		<%---<%= widgetFactory.createSubLabel( "ImportDLCIssue.PresentDocs") %>	---%>
		<%= widgetFactory.createNumberField( "PresentDocsDays", "", terms.getAttribute("present_docs_within_days"), "3", isReadOnly, false, false,  "onChange=\"updatePresentDocsDaysUserInd();\"", "", "none" ) %> 
		<%=resMgr.getText("ImportDLCIssue.DayOfShipment",TradePortalConstants.TEXT_BUNDLE)%>
		<%---<%= widgetFactory.createSubLabel( "ImportDLCIssue.DayOfShipment") %>	---%>
		<div style="clear:both;"> </div>
		</div>
		<input type=hidden name ="PresentDocsDaysUserInd" value="<%=terms.getAttribute("present_docs_days_user_ind")%>">
		
		<div class = "formItem title-right">
		
		<% if (!(isReadOnly)) 
		{     %>	
		
			<button data-dojo-type="dijit.form.Button"  name="AddShipmentButton" id="AddShipmentButton" type="submit">
	        <%=resMgr.getText("ImportDLCIssue.AddShipment",TradePortalConstants.TEXT_BUNDLE)%>
	        <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
	             setButtonPressed('<%=TradePortalConstants.BUTTON_ADD_SHIPMENT%>', '0');
	             document.forms[0].submit();
	        </script>
	        </button>
        
	<% 
		}else{  %>
			&nbsp;
		<% 
		}
	%>
	
	<% if(numShipments >1)
	{
		if (!(isReadOnly)) 
		{     %> 
		
				<button data-dojo-type="dijit.form.Button"  name="DeleteShipmentButton" id="DeleteShipmentButton" type="submit">
		        <%=resMgr.getText("ImportDLCIssue.DeleteShipment",TradePortalConstants.TEXT_BUNDLE)%>
		        <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
		             setButtonPressed('<%=TradePortalConstants.BUTTON_DELETE_SHIPMENT%>', '0');
		             document.forms[0].submit();
		        </script>
		        </button>
		        
		<% 
		}
	}
 %>
 </div>
 </br>
 		<%
		Vector codesToExclude2= null;
		String notifySearchHtml = "";
		String consigneeSearchHtml  = "";
		String date="";
		String readDate="";
		int pane = 0;
		  
		%>
		</br>
		</br>
		<%@ include file="MultipleShipments.frag" %>
		<%@ include file="Transaction-IMP_DLC-ISS-TransportDocs.frag" %>
        <%@ include file="MultipleShipments.frag" %>
		  <input type=hidden name=shipmentNumber value="<%=shipmentTabPressed%>">

	
		
					

	
	<script LANGUAGE="JavaScript">
	
	  function updatePresentDocsDaysUserInd()
	  {
			if (document.forms[0].PresentDocsDays.value == "")
			{
				document.forms[0].PresentDocsDaysUserInd.value = "";
			}
			else
			{
				document.forms[0].PresentDocsDaysUserInd.value = "Y";
			}
	  }
	  function addShipment(tableID) {
		  require(["dojo/dom","dojo/domReady!" ], function(dom) {
		  var table = dom.byId(tableID);
												
		  <%-- get index# for field naming --%>
		  var insertRowIdx = table.rows.length;

		  <%-- add the shipment terms table row --%>
		  var row = table.insertRow(insertRowIdx);
		  row.id = "shipmentTermRow"+insertRowIdx;
		  attachAjaxResult(row.id,
		    "/portal/transactions/fragments/Transaction-IMP_DLC-ISS-ShipmentTermsRows.jsp?shipmentTermsIndex="+insertRowIdx);

		  <%-- append the transport docs section to the end of the transport docs --%>
		  var transportDocsAttachPoint = document.getElementById('transportDocs');
		  <%-- create a new element for the section --%>
		  var newTransportDocSection = document.createElement('div');
		  newTransportDocSection.id = "transportDocSection"+insertRowIdx;
		  transportDocsAttachPoint.appendChild(newTransportDocSection);

		  attachAjaxResult(newTransportDocSection.id, 
		    "/portal/transactions/fragments/Transaction-IMP_DLC-ISS-TransportDocs.jsp?pane="+insertRowIdx);
	  });
		}


function deleteRow(tableID) {
	require(["dojo/dom","dojo/domReady!" ], function(dom) {
      try {
      var table = dom.byId(tableID);
      var rowCount = table.rows.length;
		alert('rowcount is - '+rowCount);

      for(var i=0; i<rowCount; i++) {
          var row = table.rows[i];
          var chkbox = row.cells[0].childNodes[0];
			alert('chkbox'+ chkbox);
			alert('chkbox checked'+ chkbox.checked);
          if(null != chkbox && true == chkbox.checked) {
              var index = table.rowIndex;
              alert("index"+index);
              <%-- table.deleteRow(i); --%>
              <%-- rowCount--; --%>
              i--;
          }

      }
      }catch(e) {
          alert(e);
      }
});
  }

	</script>