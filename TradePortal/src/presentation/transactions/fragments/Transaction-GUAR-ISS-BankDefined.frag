<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
*******************************************************************************
                    Guarantee Issue Page - Bank Defined section

  Description:
    Contains HTML to create the Guarantee DLC Issue Bank Defined section.  

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transaction-GUAR-ISS-BankDefined.jsp" %>
*******************************************************************************
--%>
	<div class="columnLeft">
		<%= widgetFactory.createTextField("PurposeType", "GuaranteeIssue.GUARPurposeType", terms.getAttribute("purpose_type").toString(), "3", isReadOnly) %>
	</div>
	<div class="columnRight">
		<%= widgetFactory.createCheckboxField("Irrevocable", "GuaranteeIssue.Irrevocable",  terms.getAttribute("irrevocable").equals(TradePortalConstants.INDICATOR_YES), isReadOnly,false,"","","none") %><br/>
	</div>
	<div style="clear: both"></div>