<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
*******************************************************************************
                     Export Collection Amend Page - Tenors section

  Description:
    Contains HTML to display the multiple tenors for the Export Collection 
    Amend Page.  This was localized because it gets called 5 times.  This
    should help the code in the Transaction-EXP_COL-AMD-html.jsp file be a 
    little easier to read.

  This is not a standalone JSP.  It MUST be included using the following tag:

  label         = "ExportCollectionAmend.Tenor2";
  descName      = "Tenor2";
  descAttName   = "tenor_2";
  amountName    = "Tenor2Amount";
  amountAttName = "tenor_2_amount";
  draftName     = "Tenor2DraftNumber";
  draftAttName  = "tenor_2_draft_number";

    <%@ include file="Transaction-EXP_COL-AMD-Tenors.frag" %>

*******************************************************************************
--%>
<%

amount = terms.getAttribute("amount");
curcy = terms.getAttribute("amount_currency_code");
currencyCode = curcy;


amount = terms.getAttribute( "tenor_" + multiTenorIndex + "_amount" );

  if(!getDataFromDoc)
		displayAmount = TPCurrencyUtility.getDisplayAmount(amount, currencyCode, loginLocale);
  else
		displayAmount = amount;
%>
    <tr>
    	<td><%=widgetFactory.createCheckboxField("TenorInd"+multiTenorIndex, "", false, isReadOnly , false, "", "", "none" ) %> </td>
    	<td><%= multiTenorIndex%>.</td>
    	<td><%= widgetFactory.createTextField( "Tenor"+multiTenorIndex, "", terms.getAttribute("tenor_"+multiTenorIndex), "35", isReadOnly, false, false, "class='char18'", "", "none" ) %></td>    	
    	<td><%= widgetFactory.createTextField( "Tenor"+multiTenorIndex+"Amount", "", displayAmount, "22", isReadOnly, false, false, "class='char18'", "", "none" ) %></td>
    	<td><%= widgetFactory.createTextField( "Tenor"+multiTenorIndex+"DraftNumber", "", terms.getAttribute("tenor_"+multiTenorIndex+"_draft_number"), "15", isReadOnly, false, false, "class='char18'", "", "none" ) %></td>
	</tr>
