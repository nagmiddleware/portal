<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
*******************************************************************************
                     Request to Advise Issue Page - Other Conditions section

  Description:
    Contains HTML to create the Request to Advise Issue Other Conditions section.  

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transaction-RQA-ISS-OtherConditions.jsp" %>
*******************************************************************************
--%>


	<div class="columnLeft">
		<%=widgetFactory.createSubsectionHeader("RequestAdviseIssue.AdditionalTerms",false,false,isExpressTemplate,"")%>
		<%=widgetFactory.createCheckboxField("Transferable","RequestAdviseIssue.Transferable",terms.getAttribute("transferrable").equals(TradePortalConstants.INDICATOR_YES),isReadOnly || isFromExpress, isFromExpress, "", "", "")%>
		<div class="formItem">
			<%=widgetFactory.createCheckboxField("Revolve","RequestAdviseIssue.Revolve",terms.getAttribute("revolve").equals(TradePortalConstants.INDICATOR_YES),isReadOnly || isFromExpress, isFromExpress, "", "", "none")%>
			<%=widgetFactory.createNote("RequestAdviseIssue.RevolveConditions") %>
		</div>
		
	</div>
	<div class="columnRight">
		<%=widgetFactory.createSubsectionHeader("RequestAdviseIssue.Confirmation",false,false,isExpressTemplate,"")%>
		<div class="formItem">
		<%= widgetFactory.createRadioButtonField( "ConfirmationType", "TradePortalConstants.CONFIRM_NOT_REQD", "RequestAdviseIssue.ConfirmNotReqd1", TradePortalConstants.CONFIRM_NOT_REQD, confirmType.equals(TradePortalConstants.CONFIRM_NOT_REQD), isReadOnly || isFromExpress) %>
		<br>
		<%= widgetFactory.createRadioButtonField( "ConfirmationType", "TradePortalConstants.BANK_TO_ADD", "RequestAdviseIssue.BankToAdd1", TradePortalConstants.BANK_TO_ADD, confirmType.equals(TradePortalConstants.BANK_TO_ADD), isReadOnly || isFromExpress ) %>
		<br>
		<%= widgetFactory.createRadioButtonField( "ConfirmationType", "TradePortalConstants.BANK_MAY_ADD", "RequestAdviseIssue.BankMayAdd1", TradePortalConstants.BANK_MAY_ADD, confirmType.equals(TradePortalConstants.BANK_MAY_ADD), isReadOnly || isFromExpress ) %>
		<br>
		</div>
	</div>
	<div style="clear:both;"></div>
	
	<%--Sandeep - Rel-8.3 CR-752 Dev 06/12/2013 - Begin--%>
	<%-- PMitnala Rel 8.3 IR#T36000021051 Correcting the label to ICC Applicable Rules --%>
	<%= widgetFactory.createCheckboxField( "UCPVersionDetailInd", "common.ICCApplicableRules", terms.getAttribute("ucp_version_details_ind").equals(TradePortalConstants.INDICATOR_YES), isReadOnly, false, "onClick=\"clearICCPublication('UCPVersionDetailInd')\"", "", "") %>    
	
	<%
		options = Dropdown.createSortedRefDataOptions(TradePortalConstants.ICC_GUIDELINES_RQA, terms.getAttribute("ucp_version"), loginLocale);
			
		if(isReadOnly && StringFunction.isNotBlank(terms.getAttribute("ucp_version")) && 
				ReferenceDataManager.getRefDataMgr().checkCode(TradePortalConstants.ICC_GUIDELINES_RQA, terms.getAttribute("ucp_version"), loginLocale) == false){
			
			options = Dropdown.createSortedRefDataOptions(TradePortalConstants.ICC_GUIDELINES, terms.getAttribute("ucp_version"), loginLocale);
		}
		defaultText = " ";
	%>
        <% //cquinton 10/16/2013 ir#21872 Rel 8.3 start %>
	<%= widgetFactory.createSelectField( "UCPVersion", "common.UCPVersion", defaultText, options, isReadOnly, false, false, "onChange=\"changeUCPVersion()\"", "", "formItemWithIndent1" ) %> 
        <%
           //if version is something other than 'OTHER' the details field should be disabled
           //also remove setting ucp checkbox onclick - this is done in UCPVersion onchange above
           String ucpDetailsHtmlProps =  "class='char21'";
           String ucpDetailsValue = terms.getAttribute("ucp_details"); //not ideal, but some funny initial data
           if ( !TradePortalConstants.RQA_OTHER.equals(terms.getAttribute("ucp_version")) ) {
               ucpDetailsHtmlProps += " disabled=\"disabled\"";
               ucpDetailsValue = "";
           }
        %>
	<%= widgetFactory.createTextField( "UCPDetails", "common.UCPDetails", ucpDetailsValue, "30", isReadOnly,  false, false, ucpDetailsHtmlProps, "", "formItemWithIndent1" ) %>
        <% //cquinton 10/16/2013 ir#21872 Rel 8.3 end %>
	<%--Sandeep - Rel-8.3 CR-752 Dev 06/12/2013 - End--%>
	
	<%=widgetFactory.createLabel("","RequestAdviseIssue.AddlConditions",false, false,isExpressTemplate,"","")%>
	<%
	if (isReadOnly || isFromExpress) {
        out.println("&nbsp;");
      } else {
          options = ListBox.createOptionList(addlCondDocList, "PHRASE_OID", "NAME", "", userSession.getSecretKey());
        defaultText1 = resMgr.getText("transaction.SelectPhrase",
                                     TradePortalConstants.TEXT_BUNDLE);
	%>
	<%=widgetFactory.createSelectField("AddlDocsPhraseItem1","", defaultText1, options,isReadOnly, false, false,  
										"onChange=" + PhraseUtility.getPhraseChange("/Terms/additional_conditions","AddlConditionsText", "5000","document.forms[0].AddlConditionsText"), "", "")%>
	<%
      }
	if (!isReadOnly){ %> 
	<%=widgetFactory.createTextArea("AddlConditionsText","",terms.getAttribute("additional_conditions"),isReadOnly || isFromExpress,false,false, "rows='10' cols='128'","","") %>
	<%= widgetFactory.createHoverHelp("AddlConditionsText","InstrumentIssue.PlaceHolderOtherAddlConditions")%>
	<%}else{ %>
	<%=widgetFactory.createAutoResizeTextArea("AddlConditionsText","",terms.getAttribute("additional_conditions"),isReadOnly || isFromExpress,false,false, "style='width:600px;min-height:140px;' cols='128'","","") %>
	<%}%>
