<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 *  Vasavi CR 524 03/31/2010
--%>
<%--
*******************************************************************************
                 Export Collection Trace Page - BAnk Instruction Section

  Description:
    Contains HTML to create the Export Collection Trace page.  All data retrieval 
  is handled in the Transaction-EXP_OCO-TRC.jsp.

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transaction-EXP_OCO-TRC-html.jsp" %>
*******************************************************************************
--%>
<div>
	<%=widgetFactory.createStackedLabel("",
					"ExportCollectionTrace.InstsenttoBank")%>
					<br/>
	<%=widgetFactory.createRadioButtonField("TracerSendType",
					"TradePortalConstants.NO_ACTION_REQUIRED",
					"ExportCollectionTrace.CopyofTracer",
					TradePortalConstants.NO_ACTION_REQUIRED, tracerSendType
							.equals(TradePortalConstants.NO_ACTION_REQUIRED),
							isReadOnly, "onClick:showExport();", "")%>
	<br/>
	<%=widgetFactory.createRadioButtonField("TracerSendType",
					"TradePortalConstants.BANK_TO_SEND",
					"ExportCollectionTrace.SendTracer",
					TradePortalConstants.BANK_TO_SEND,
					tracerSendType.equals(TradePortalConstants.BANK_TO_SEND),
					isReadOnly, "onClick:showExport();", "")%>

	<%
		if (isReadOnly) {
			out.println("&nbsp;");
		} else {
	%>
	<%
		options = ListBox.createOptionList(spclInstrDocList,
					"PHRASE_OID", "NAME", "", userSession.getSecretKey());
			defaultText = resMgr.getText("transaction.SelectPhrase",
					TradePortalConstants.TEXT_BUNDLE);
	%><br/><br/>
	<%=widgetFactory
						.createSelectField(
								"SpecInstrPhraseItem",
								"",
								defaultText,
								options,
								isReadOnly,
								false,
								false,
								"onChange="
										+ PhraseUtility
												.getPhraseChange(
														"/Terms/special_bank_instructions",
														"SpclBankInstructions",
														"5000",
														"document.forms[0].SpclBankInstructions"),
								"", "")%>
	<%
		}
	%>
	<%=widgetFactory.createTextArea("SpclBankInstructions", "",
					terms.getAttribute("special_bank_instructions"),
					isReadOnly, false, false, "rows='10';", "", "")%>
	<%if(!isReadOnly){%>
		<%=widgetFactory.createHoverHelp("SpclBankInstructions", "DirectDebitCollection.SpecialInstToolTip") %>
	<%} %>
</div>