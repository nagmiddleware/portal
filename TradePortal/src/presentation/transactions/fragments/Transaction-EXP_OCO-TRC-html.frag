<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 *  Vasavi CR 524 03/31/2010
--%>
<%--
*******************************************************************************
                 Export Collection Trace Page - all sections

  Description:
    Contains HTML to create the Export Collection Trace page.  All data retrieval 
  is handled in the Transaction-EXP_OCO-TRC.jsp.

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transaction-EXP_OCO-TRC-html.jsp" %>
*******************************************************************************
--%>
<%
  String tracerSendType = terms.getAttribute("tracer_send_type");
%>
  <%
  /*********************************************************************************************************
  * Start of General Section - beige bar
  **********************************************************************************************************/
  %>
  <table width="100%" border="0" cellspacing="0" cellpadding="0" height="34" class="BankColor">
    <tr class="BankColor"> 
      <td width="15" class="BankColor" nowrap height="35">&nbsp;</td>
      <td class="BankColor" width="170" nowrap height="35"> 
        <p class="ControlLabelWhite">
	  <%=resMgr.getText("ExportCollectionTrace.General", 
                             TradePortalConstants.TEXT_BUNDLE)%>
	  <a name="General"></a>
	</p>
      </td>
      <td width="90%" class="BankColor" height="35" valign="top">&nbsp;</td>
      <td width="1" class="BankColor" height="35">&nbsp;</td>
      <td width="1" class="BankColor" height="35">&nbsp;</td>
      <td class="BankColor" align="right" valign="middle" width="15" height="35" nowrap>

<%    // -- Online Help button goes here -- (fileName, anchor, resMgr)
	out.print( OnlineHelp.createContextSensitiveLink( "customer/trace_exp_coll.htm", 
							  "general", resMgr, userSession) );
%>
      </td>
      <td width="20" class="BankColor" nowrap height="35">&nbsp;</td>
    </tr>
  </table>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td>&nbsp;</td>
    </tr>
  </table>

  <%
  /***********************************************************
  * Reference Number - static display of stored data
  ***********************************************************/
  %>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td width="20" nowrap rowspan="2">&nbsp;</td>
      <td nowrap> 
        <p class="ControlLabel">
	  <%=resMgr.getText("ExportCollectionTrace.YourRefNo", 
                             TradePortalConstants.TEXT_BUNDLE)%>
	</p>
      </td>
      <td rowspan="2" width="100%">&nbsp;</td>
    </tr>
    <tr>
      <td nowrap> 
        <p class="ListText">
          <%=terms.getAttribute("reference_number")%>
	</p>
      </td>
    </tr>
  </table>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td>&nbsp;</td>
    </tr>
  </table>

  <%
  /***********************************************************
  * Collection Amount Row - static display of stored data
  ***********************************************************/
  %>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td width="20" nowrap>&nbsp;</td>
      <td nowrap> 
        <p class="ControlLabel">
	  <%=resMgr.getText("ExportCollectionTrace.CollectionAmount", 
                             TradePortalConstants.TEXT_BUNDLE)%>
	</p>
      </td>
      <td width="25" nowrap>&nbsp;</td>
      <td nowrap> 
        <p class="ListText">
          <%=currencyCode%>
	</p>
      </td>
      <td width="5" nowrap>&nbsp;</td>
      <td nowrap align="right"> 
        <p class="ListText">
          <%=TPCurrencyUtility.getDisplayAmount(originalAmount.toString(), 
                                              currencyCode, loginLocale)%>
	</p>
      </td>
      <td width="25" nowrap>&nbsp;</td>
      <td>&nbsp;</td>
      <td width="100%">&nbsp;</td>
    </tr>
    <tr> 
      <td>&nbsp;</td>
      <td>&nbsp; </td>
      <td>&nbsp;</td>
      <td nowrap> 
        <p class="ListText">&nbsp;</p>
      </td>
      <td>&nbsp;</td>
      <td class="ListText" align="right">&nbsp; </td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
  </table>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td>&nbsp;</td>
    </tr>
  </table>

  <%
  /*************************************************
  * Amendment Details Text Area 
  *************************************************/
  %>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td width="20" nowrap>&nbsp;</td>
      <td nowrap class="ListText" colspan="5"> 
        <p class="ListText">
	  <%=resMgr.getText("ExportCollectionTrace.AdditionalTracertext", 
                             TradePortalConstants.TEXT_BUNDLE)%>
	</p>
      </td>
      <td width="15" nowrap>&nbsp;</td>
      <td class="ListText">&nbsp; </td>
      <td width="100%">&nbsp;</td>
    </tr>
  </table>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td width="20" nowrap>&nbsp;</td>
      <td class="ListText" align="left" valign="bottom"> 
        <%=InputField.createTextArea("TracerDetails", 
                                     terms.getAttribute("tracer_details"), 
                                     "100", "3", "ListText", isReadOnly )%>
      </td>
      <td width="100%">&nbsp;</td>
    </tr>
  </table>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td>&nbsp;</td>
    </tr>
  </table>

  <%
  /*********************************************************************************************************
  * Start of Instructions To Bank Section - beige bar
  **********************************************************************************************************/
  %>
  <table width="100%" border="0" cellspacing="0" cellpadding="0" height="34">
    <tr> 
      <td width="15" class="BankColor" nowrap height="35">&nbsp;</td>
      <td class="BankColor" width="170" nowrap height="35"> 
        <p class="ControlLabelWhite">
	   <%=resMgr.getText("ExportCollectionTrace.InsttoBank", 
                             TradePortalConstants.TEXT_BUNDLE)%>
	   <a name="InstructionstoBank"></a> 
        </p>
      </td>
      <td width="100%" class="BankColor" height="35" valign="top">&nbsp; </td>
      <td width="1" class="BankColor" height="35">&nbsp;</td>
      <td width="1" class="BankColor" height="35">&nbsp;</td>
      <td width="90" class="BankColor" height="35" nowrap> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="17">
          <tr> 
            <td align="center" valign="middle" height="17"> 
              <p class="ButtonLink">
	 	<a href="#top" class="LinksSmall">
                  <%=resMgr.getText("common.TopOfPage", 
                                    TradePortalConstants.TEXT_BUNDLE)%>
		</a>
	      </p>
            </td>
          </tr>
        </table>
      </td>
      <td width="1" class="BankColor" nowrap height="35">&nbsp;</td>
      <td class="BankColor" align="right" valign="middle" width="15" height="35" nowrap>

<%    // -- Online Help button goes here -- (fileName, anchor, resMgr)
	out.print( OnlineHelp.createContextSensitiveLink( "customer/trace_exp_coll.htm", 
							  "instr_to_bank", resMgr, userSession) );
%>
      </td>
      <td width="20" class="BankColor" nowrap height="35">&nbsp;</td>
    </tr>
  </table>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td>&nbsp;</td>
    </tr>
  </table>

  <%
  /*************************************************
  *  Action Radio buttons action is/not required
  *************************************************/
  %>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td width="20" nowrap>&nbsp;</td>
      <td nowrap>
        <p class="ListText">
	   <%=resMgr.getText("ExportCollectionTrace.InstsenttoBank", 
                             TradePortalConstants.TEXT_BUNDLE)%>
	</p>
      </td>
      <td width="100%">&nbsp;</td>
    </tr>
  </table>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td width="30" nowrap>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>
          <%=InputField.createRadioButtonField("TracerSendType", 
                     TradePortalConstants.NO_ACTION_REQUIRED, "", 
                     tracerSendType.equals(TradePortalConstants.NO_ACTION_REQUIRED),
                     "ListText", "", isReadOnly )%>
      </td>
      <td width="5" nowrap>&nbsp;</td>
      <td nowrap>
        <p class="ListText">
	   <%=resMgr.getText("ExportCollectionTrace.CopyofTracer", 
                             TradePortalConstants.TEXT_BUNDLE)%>
	</p>
      </td>
      <td width="100%">&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>
          <%=InputField.createRadioButtonField("TracerSendType", 
                     TradePortalConstants.BANK_TO_SEND, "", 
                     tracerSendType.equals(TradePortalConstants.BANK_TO_SEND),
                     "ListText", "", isReadOnly )%>
      </td>
      <td>&nbsp;</td>
      <td nowrap>
        <p class="ListText">
	   <%=resMgr.getText("ExportCollectionTrace.SendTracer", 
                             TradePortalConstants.TEXT_BUNDLE)%>
	</p>
      </td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
  </table>

  <%
  /*************************************************
  *  Special Instructions Dropdown
  *************************************************/
  %>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td width="20" nowrap>&nbsp;</td>
      <td nowrap class="ListText" colspan="5"> 
        <p class="ListText">
	   <%=resMgr.getText("ExportCollectionTrace.SpecialInstrucionsText", 
                             TradePortalConstants.TEXT_BUNDLE)%>
	</p>
      </td>
      <td width="15" nowrap>&nbsp;</td>
      <td class="ListText">&nbsp; </td>
      <td width="100%">&nbsp;</td>
    </tr>
  </table>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td width="20" nowrap>&nbsp;</td>
      <td class="ListText" align="left" valign="bottom"> 
<%
        if (isReadOnly) {
          out.println("&nbsp;");
        } else {
          options = ListBox.createOptionList(spclInstrDocList, "PHRASE_OID", "NAME", "", userSession.getSecretKey());
          defaultText = resMgr.getText("transaction.SelectPhrase",
                                       TradePortalConstants.TEXT_BUNDLE);
          out.print(InputField.createSelectField("SpecInstrPhraseItem", "", 
                   defaultText, options, "ListText", isReadOnly,
                   "onChange=" + PhraseUtility.getPhraseChange(
                                            "/Terms/special_bank_instructions",
                                            "SpclBankInstructions", "1000")));
  /*************
  * Add Button
  **************/
  %>
          <jsp:include page="/common/PhraseLookupButton.jsp">
            <jsp:param name="imageName" value='GoodsDescButton' />
          </jsp:include>
  <%
        }
  %>
	  <br>
  <%
  /*************************************************
  *  Special Instructions TextArea Box
  *************************************************/
  %>
        <%=InputField.createTextArea("SpclBankInstructions", 
                                     terms.getAttribute("special_bank_instructions"), 
                                     "100", "3", "ListText", isReadOnly)%>
      </td>
      <td width="100%">&nbsp;</td>
    </tr>
  </table>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td>&nbsp;</td>
    </tr>
  </table>
