<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%
String addEXPOCO = "N";
  if (request.getParameter("addEXPOCO") != null){
	  addEXPOCO = StringFunction.xssCharsToHtml(request.getParameter("addEXPOCO"));
  }

	  // Determine the instrument type code(s) for display
	  // and search criteria
	  // there are three options: display all instrument types,
	  // display only instrument types that can be created and
	  // display only the Export LC instrument
	  if  (searchCondition.equals(TradePortalConstants.SEARCH_ALL_INSTRUMENTS)){
            instrumentTypes = Dropdown.getTradeInstrumentTypes();
		showInstrumentDropDown = true;
	  }
	//Rel9.0 Adding instrument type 'PYB' to the instrument search dialog
	  else if  (searchCondition.equals(TradePortalConstants.SEARCH_ALL_INSTRUMENTS_FOR_MESSAGES)){
          instrumentTypes = Dropdown.getTradeInstrumentTypes();
          instrumentTypes.addElement(TradePortalConstants.PAYABLE_MANAGEMENT); //Added for Rel9.0 IR#T36000028569
          showInstrumentDropDown = true;
      }
	  else if(searchCondition.equals(TradePortalConstants.SEARCH_EXP_DLC_ACTIVE))
	  {
	        instrumentTypes = new Vector();
		instrumentTypes.addElement(  InstrumentType.EXPORT_DLC  );
	  	showInstrumentDropDown = false;
	  }

          // TLE - 10/26/06 - IR#AYUG101742823 - Add Begin
	  else if(searchCondition.equals(TradePortalConstants.ALL_EXPORT_DLC))
	  {
	        instrumentTypes = new Vector();
		instrumentTypes.addElement(  InstrumentType.EXPORT_DLC  );
	  	showInstrumentDropDown = true;
	  }
	  else if(searchCondition.equals(InstrumentType.EXPORT_COL))
	  {
	        instrumentTypes = new Vector();
		instrumentTypes.addElement(  InstrumentType.EXPORT_COL  );
		if (TradePortalConstants.INDICATOR_YES.equals(addEXPOCO)){
			instrumentTypes.addElement(  InstrumentType.NEW_EXPORT_COL  );
			}
	  	showInstrumentDropDown = true;
	  }
	  else if( searchCondition.equals(TradePortalConstants.IMPORT) )
	  {
		instrumentTypes = new Vector();
		instrumentTypes.addElement(InstrumentType.IMPORT_DLC);
		instrumentTypes.addElement(InstrumentType.SHIP_GUAR);
		instrumentTypes.addElement(InstrumentType.IMPORT_COL);
		instrumentTypes.addElement(InstrumentType.AIR_WAYBILL);
		instrumentTypes.addElement(InstrumentType.APPROVAL_TO_PAY);
                showInstrumentDropDown = true;
	  }
          // TLE - 10/26/06 - IR#AYUG101742823 - Add End
 
	  else if(searchCondition.equals(InstrumentType.LOAN_RQST))
	  {
	        instrumentTypes = new Vector();
		instrumentTypes.addElement(  InstrumentType.LOAN_RQST  );
	  	showInstrumentDropDown = false;
	  }

	  else if( searchCondition.equals(TradePortalConstants.SEARCH_FOR_COPY_INSTRUMENT) ||
	  	searchCondition.equals(TradePortalConstants.SEARCH_FOR_NEW_TRANSACTION))
	  {
	  	Long organization_oid = new Long( userSession.getOwnerOrgOid() );
	  	Long userOID = new Long(userSession.getUserOid());
	  	instrumentTypes = Dropdown.getCreateModifyInstrumentTypes( formMgr, 
						  loginRights, 
                                                  organization_oid.longValue(),
                                                  userSession.getSecurityType(),
                                                  userSession.getOwnershipLevel(),
                                                  userOID.longValue());//PPRAKASH IR PRUJ022438083
		showInstrumentDropDown = true;
	  }

	  else if( searchCondition.equals(TradePortalConstants.IMPORTS_ONLY) )
	  {
		instrumentTypes = new Vector();
		instrumentTypes.addElement(InstrumentType.IMPORT_DLC);
		/* KMehta on 9 Dec 2014 @ Rel 90 for IR T36000023101 Change - Start   
		 * Commented the following IR since Only Imports should be visible as per IR T36000023101
		 */
		//instrumentTypes.addElement(InstrumentType.STANDBY_LC);
		/* KMehta on 9 Dec 2014 @ Rel 90 for IR T36000023101 Change - End */
		instrumentTypes.addElement(InstrumentType.IMPORT_COL);
		showInstrumentDropDown = true;
	  }
	
	  else
	  {
	 //	instrumentTypes = Dropdown.getAllInstrumentTypes();
			instrumentTypes = Dropdown.getTradeInstrumentTypes(); // IR NGUJ013070808
		showInstrumentDropDown = true;	  
	  }

/* ctq comment this out for now, not sure if its necessary or not
the 'tabVisible()' method has been replaced with allSomeXXXTransaction(), so if necessary
use those methods

//PPRAKASH IR NGUJ013070808 Add Begin
String allowInternationalTrade = corpOrg.tabVisible();

if (TradePortalConstants.ONLY_INSTRUMENT_INT_TAB_VISIBLE.equals(allowInternationalTrade)) {
//Trudden IR NGUJ013070808 Add Begin
//only insert FUNDS_XFER if it does not already exist in the vector to avoid some instances 
//where the dropdown could contain dups of FUNDS_XFER
    if (!(instrumentTypes.contains(InstrumentType.FUNDS_XFER))) {
	instrumentTypes.addElement(InstrumentType.FUNDS_XFER);
    }	
}	
//Trudden IR NGUJ013070808 End
//PPRAKASH IR NGUJ013070808 Add End
//Trudden IR PYUJ031764918 Add Begin
else {
   //if international payments are allowed and Cash Management is available, then the 
   //instrument search page needs to remove international payments, domestic payments, and transfers.
   //those types will only be available in Cash Management.
   //this only applies to new and existing instruments, not new templates
   if (TradePortalConstants.INSTRUMENT_CASH_TAB_VISIBLE.equals(allowInternationalTrade)) {
   
       if ((mode != null) && 
           ((mode.equals(TradePortalConstants.EXISTING_INSTR)) ||
           (mode.equals(TradePortalConstants.NEW_INSTR)))) {
             instrumentTypes.removeElement(InstrumentType.FUNDS_XFER);
             instrumentTypes.removeElement(InstrumentType.XFER_BET_ACCTS);
             instrumentTypes.removeElement(InstrumentType.DOM_PMT);
       }    
   
   }

}
//Trudden IR PYUJ031764918 End
*/
%>
