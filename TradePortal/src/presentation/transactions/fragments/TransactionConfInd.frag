<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 
 *******************************************************************************
                               Shows Confidential Indicator
 
   Description: Displays a Check box and accepts input to save
   in the database that indicates wheather Payment/Transfer 
   Transactions is confidential (if user is allowed to see this info)
   
   Created on 08/18/10 by IAZ as CR-586
   
   //IAZ IR-RDUK091546587 09/27/10 Add Conf Indicator to Instrument itself

********************************************************************************
--%>

      <%
      if (!isTemplate)
      {  
         String confInd = "";
         boolean isDomFixedTemp = InstrumentType.DOM_PMT.equals(instrument.getAttribute("instrument_type_code")) && isFixedPayment;
         if (userSession.getSavedUserSession() == null)
             confInd = loggedUser.getAttribute("confidential_indicator");
         else
         {
             if (TradePortalConstants.ADMIN.equals(userSession.getSavedUserSession().getSecurityType()))
                 confInd = TradePortalConstants.INDICATOR_YES;
             else
                 confInd = loggedUser.getAttribute("subsid_confidential_indicator");  
         }
         if (TradePortalConstants.INDICATOR_YES.equals(confInd)) 
         {
            

         	String confSet = terms.getAttribute("confidential_indicator");
         	if (InstrumentServices.isBlank(confSet)) confSet = TradePortalConstants.INDICATOR_NO;
  
      %>
            <%//=InputField.createCheckboxField("ConfidentialIndicatorBox", confSet, confType,                          !confSet.equals(TradePortalConstants.INDICATOR_NO),                           "ListText", "", isReadOnly|| confSet.equals(TradePortalConstants.CONF_IND_FRM_TMPLT) || isStatusVerifiedPendingFX,                          "onClick=setConfidentialIndicator()")%>
                <br>            
               <%=widgetFactory.createCheckboxField(
              "ConfidentialIndicatorBox", 
              "TemplateDetail.Confidential",
              !confSet.equals(TradePortalConstants.INDICATOR_NO),
              isReadOnly || isDomFixedTemp,
              false,
              "onClick=setConfidentialIndicator()",
              "",
              ""
              )%>   
            <input type=hidden name = ConfidentialIndicator value=<%=confSet%> >
            <input type=hidden name = ConfidentialIndicatorInstrument value=<%=confSet%> >
      <%
         }
      } 

      %>

<SCRIPT LANGUAGE="JavaScript">
function setConfidentialIndicator()
{
	if (document.forms[0].ConfidentialIndicatorBox.checked)
	{
		document.forms[0].ConfidentialIndicator.value = 'Y';
		document.forms[0].ConfidentialIndicatorInstrument.value = 'Y';
	}
	else
	{

		document.forms[0].ConfidentialIndicator.value = 'N';
		document.forms[0].ConfidentialIndicatorInstrument.value = 'N';
	} 
}
<%-- IR - T36000048741--%>
if (!document.forms[0].ConfidentialIndicatorBox.checked)
{
  document.forms[0].ConfidentialIndicatorBox.disabled = false;
}

</SCRIPT>
