<%--
**********************************************************************************
// DK CR-640 Rel7.1
**********************************************************************************
--%> 
<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*, com.amsinc.ecsg.util.*, java.util.*,
                 com.amsinc.ecsg.html.*, com.amsinc.ecsg.web.*,com.ams.util.*,
                 com.ams.tradeportal.common.*, com.ams.tradeportal.html.*,
                 com.ams.tradeportal.busobj.webbean.*, com.ams.tradeportal.busobj.util.*" %>

<jsp:useBean id="beanMgr"     class="com.amsinc.ecsg.web.BeanManager"                   scope="session"></jsp:useBean>
<jsp:useBean id="formMgr"     class="com.amsinc.ecsg.web.FormManager"                   scope="session"></jsp:useBean>
<jsp:useBean id="resMgr"      class="com.amsinc.ecsg.util.ResourceManager"              scope="session"></jsp:useBean>
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>

<%

String format= request.getParameter("format");
String bankGroupOid  = request.getParameter("BankGroupOID");

if (InstrumentServices.isNotBlank(bankGroupOid)) {
	String selectClause = "select fx_online_avail_ind";
	String fromClause = " from bank_organization_group b";
	StringBuffer whereClause = new StringBuffer();
	whereClause.append(" where b.organization_oid = ?"); // jgadela 03/28/2014-  sqlinjection fix
	try{
		StringBuffer fxIndQuery = new StringBuffer(selectClause);
		fxIndQuery.append(fromClause);
		fxIndQuery.append(whereClause);
		// jgadela 03/28/2014-  sqlinjection fix
		Object sqlVars[] = new Object[1];
		sqlVars[0]= bankGroupOid;
		DocumentHandler xmlDoc = DatabaseQueryBean.getXmlResultSet(fxIndQuery.toString(), false, sqlVars);
		String fxOnlInd = xmlDoc.getAttribute("/ResultSetRecord(0)/FX_ONLINE_AVAIL_IND");
		StringBuffer fxOnlIndField = null;
		if (fxOnlInd != null) {
			if ("html".equals(format)) {
			    fxOnlIndField = new StringBuffer(InputField.createTextFieldOrLabel("hidden_fxInd", fxOnlInd,"1","1", "ListText", true,""));
			 } else {
			   fxOnlIndField = new StringBuffer("{hidden_fxInd:'");
			   fxOnlIndField.append(fxOnlInd).append("'}");
			}
			out.println(fxOnlIndField.toString());
		}
	}
	catch (Exception e) {
			e.printStackTrace();
	}	
}
%>
