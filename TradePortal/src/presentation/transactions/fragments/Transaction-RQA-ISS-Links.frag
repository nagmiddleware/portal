<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
*******************************************************************************
                     Request to Advise Issue Page - Link section

  Description:
    Contains HTML to display links to the other sections.

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transaction-RQA-ISS-General.jsp" %>
*******************************************************************************
--%>

  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td width="14" nowrap>&nbsp;</td>
      <td nowrap align="left" valign="middle"> 
        <p class="ControlLabel">
          <a href="#General">
            <%=resMgr.getText("RequestAdviseIssue.General", 
                              TradePortalConstants.TEXT_BUNDLE)%>
          </a>
        </p>
      </td>
      <td width="15" nowrap>&nbsp;</td>
      <td nowrap align="left" valign="middle"> 
        <p class="ControlLabel">
          <a href="#DocumentsRequired">
            <%=resMgr.getText("RequestAdviseIssue.DocsRequired", 
                              TradePortalConstants.TEXT_BUNDLE)%>
          </a>
        </p>
      </td>
      <td width="100%" height="30">&nbsp;</td>
    </tr>
    <tr> 
      <td width="14" nowrap>&nbsp;</td>
      <td nowrap align="left" valign="middle"> 
        <p class="ControlLabel">
          <a href="#TransportDocsShipment">
            <%=resMgr.getText("RequestAdviseIssue.TransportDocsShipment", 
                              TradePortalConstants.TEXT_BUNDLE)%>
          </a>
        </p>
      </td>
      <td width="15" nowrap>&nbsp;</td>
      <td nowrap align="left" valign="middle"> 
        <p class="ControlLabel">
          <a href="#OtherConditions">
            <%=resMgr.getText("RequestAdviseIssue.OtherConditions", 
                              TradePortalConstants.TEXT_BUNDLE)%>
          </a>
        </p>
      </td>
      <td width="100%" height="30">&nbsp;</td>
    </tr>
    <tr> 
      <td width="14" nowrap>&nbsp;</td>
      <td nowrap align="left" valign="middle"> 
        <p class="ControlLabel">
          <a href="#BankInstructions">
            <%=resMgr.getText("RequestAdviseIssue.ClientBankInstructions", 
                              TradePortalConstants.TEXT_BUNDLE)%>
          </a>
        </p>
      </td>
      <td width="15" nowrap>&nbsp;</td>
      <td nowrap align="left" valign="middle"> 
        <p class="ControlLabel">
          <a href="#InternalInstructions">
            <%=resMgr.getText("RequestAdviseIssue.InternalInstructions", 
                              TradePortalConstants.TEXT_BUNDLE)%>
          </a>
        </p>
      </td>
      <td width="100%" height="30">&nbsp;</td>
    </tr>
  <%
  // Only display the bank defined section if the user is an admin user
  if( userSession.getSecurityType().equals(TradePortalConstants.ADMIN) )
   {
  %>    
    <tr> 
      <td width="14" nowrap>&nbsp;</td>
      <td nowrap align="left" valign="middle"> 
        <p class="ControlLabel">
          <a href="#BankDefined">
            <%=resMgr.getText("RequestAdviseIssue.BankDefined", 
                              TradePortalConstants.TEXT_BUNDLE)%>
          </a>
        </p>
      </td>
      <td width="15" nowrap>&nbsp;</td>
      <td>&nbsp;</td>
      <td width="100%" height="30">&nbsp;</td>
    </tr>
   <%
    }
   %>
   
 <%
   //Add Issue Application Form PDF Link if not in admin template mode
   //and the transaction is not started and not deleted...
   if ((!userSession.getSecurityType().equals(TradePortalConstants.ADMIN)) &&
     (!transaction.getAttribute("transaction_status").equals(TradePortalConstants.TRANS_STATUS_STARTED)) &&
     (!transaction.getAttribute("transaction_status").equals(TradePortalConstants.TRANS_STATUS_DELETED)) && 
	 (!transaction.getAttribute("transaction_status").equals(TradePortalConstants.TRANS_STATUS_REJECTED_BY_BANK)) ) {
%>
    <tr> 
      <td width="15" nowrap>&nbsp;</td>
      <td nowrap align="left" valign="middle"> 
        <p class="ControlLabel">
	    <%
	      linkArgs[0] = TradePortalConstants.PDF_RQA_ISSUE_APPLICATION;
	      linkArgs[1] = EncryptDecrypt.encryptStringUsingTripleDes(transactionOid);//Use common key for encryption of parameter to doc producer
	      linkArgs[2] = "en_US";
	      //loginLocale;
	      linkArgs[3] = userSession.getBrandingDirectory(); 
	    %>
	      <%= formMgr.getDocumentLinkAsHrefInFrame(resMgr.getText("RequestAdviseIssue.IssueApplicationForm",
	      							TradePortalConstants.TEXT_BUNDLE),
	      							TradePortalConstants.PDF_RQA_ISSUE_APPLICATION,
	      							linkArgs,
	      							response) %>
	</p>
      </td>
      <td width="15" nowrap>&nbsp;</td>
      <td>&nbsp;</td>
      <td width="100%" height="30">&nbsp;</td>
    </tr>
<%
  }
%>
  </table>
