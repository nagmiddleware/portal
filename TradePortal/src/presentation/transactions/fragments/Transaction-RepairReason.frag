<%--SSikhakolli - Rel-8.3 IR# T36000021491 on 10/15/2013 - Begin--%>
<div id=repairReasonSection' style='overflow-y: auto; max-height: 150px;'>
	<div class='formItem'>
	<%
	/************************************
	 * Repair Reason : Display all repair reason for the transaction/instrument selected   *
	 ************************************/
	try {	
		UserWebBean tempUser = beanMgr.createBean(UserWebBean.class,"User"); 
		TransactionHistoryWebBean transHistoryBean = beanMgr.createBean(TransactionHistoryWebBean.class,"TransactionHistory");
		
		SimpleDateFormat formatter = new SimpleDateFormat(userSession.getDatePattern());
		String formatedDate = null;	
		StringBuffer repairReasonQry = new StringBuffer();
		Vector resSetRec = null;
		DocumentHandler tempDoc = null;
		/*Get all repair reason's from transaction history table*/
		repairReasonQry.append("select transaction_history_oid, A_USER_OID, ACTION_DATETIME, REPAIR_REASON ");
		repairReasonQry.append("from transaction_history ");
		repairReasonQry.append("where p_transaction_oid = ?");
		repairReasonQry.append(" and REPAIR_REASON is not null order by transaction_history_oid DESC");
		//jgadela  R90 IR T36000026319 - SQL FIX
		Object[] sqlParamsReReasonX = new Object[1];
		sqlParamsReReasonX[0] =  transaction.getAttribute("transaction_oid");
		
		DocumentHandler repReasonDoc = DatabaseQueryBean.getXmlResultSet(repairReasonQry.toString(), false, sqlParamsReReasonX);
		
	   if(null != repReasonDoc){
		   resSetRec = repReasonDoc.getFragments("/ResultSetRecord");
		   if(null != resSetRec){
			   for(int repResCnt=0; repResCnt<resSetRec.size(); repResCnt++){
				   tempDoc = (DocumentHandler) resSetRec.elementAt(repResCnt);
				   
				   tempUser.setAttribute("user_oid", tempDoc.getAttribute("/A_USER_OID"));
				   tempUser.getDataFromAppServer();  
				   
				   transHistoryBean.setAttribute("transaction_history_oid", tempDoc.getAttribute("/TRANSACTION_HISTORY_OID"));
				   transHistoryBean.getDataFromAppServer();
				   
				   //SSikhakolli - Rel-8.3 UAT IR#T36000021892 on 10/16/2013 - Begin
				   formatedDate = formatter.format(new Date(DateTimeUtility.convertTimestampToDateString(tempDoc.getAttribute("/ACTION_DATETIME"))));
				   //SSikhakolli - Rel-8.3 UAT IR#T36000021892 on 10/16/2013 - End
	%>
			   		<b>
			       	<%=formatedDate %> <%=resMgr.getText("TransactionHistory.User", TradePortalConstants.TEXT_BUNDLE)%>
			       	<%=tempUser.getAttribute("first_name")%> <%=tempUser.getAttribute("last_name")%>
			       	</b>
			       	</br>
			      	<%= widgetFactory.createAutoResizeTextArea("repairReason"+repResCnt, "", transHistoryBean.getAttribute("repair_reason"), true ,false, false, "style='border:0px; background-color: #ffffff !important;width: 600px;'cols='128'", "","" ) %>
	<%
			   	}
		   }
	   }
	} catch(Exception e) {
	   System.err.println("[Transaction-RepairReason.frag] Caught an exception while trying to get Repair Reason associated with transaction "
	   	+transaction.getAttribute("transaction_oid"));
	   e.printStackTrace();
	}
	%>
	</div>
</div>	
<%--SSikhakolli - Rel-8.3 IR# T36000021491 on 10/15/2013 - End--%>