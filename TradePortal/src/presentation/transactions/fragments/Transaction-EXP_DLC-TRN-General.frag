<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
*******************************************************************************
         Export Letter Of Credit Issue Transfer Page - General section

  Description:
    Contains HTML to create the Export Letter Of Credit Issue Transfer General 
    section.

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transaction-EXP_DLC-TRN-General.jsp" %>
*******************************************************************************
--%>
<div class="columnLeft"> 
<%
   String   displayAmount;
   String   amount        = terms.getAttribute("amount");
   String   currency      = terms.getAttribute("amount_currency_code");

   if(!getDataFromDoc)
      displayAmount = TPCurrencyUtility.getDisplayAmount(amount, currency, loginLocale);
   else
      displayAmount = amount;
   
	String benBankSearchHtml = "";
	//IR #14535 - Appending transferee's bank details to the transferee.
	String transfereeBankStr= ",TransfereeBankTermsOid,TransfereeBankName,TransfereeBankAddressLine1,TransfereeBankAddressLine2,TransfereeBankCity,TransfereeBankStateProvince,TransfereeBankCountry,TransfereeBankPostalCode,TransfereeBank";
	String itemid="TransfereeName,TransfereeAddressLine1,TransfereeAddressLine2,TransfereeCity,TransfereeStateProvince,TransfereePostalCode,TransfereeCountry,TransfereePhoneNumber,TransfereeContactName"+transfereeBankStr;
	String section="exportLc_transferee";
	 if (!(isReadOnly)) {
		 //Changes Begin by pmitnala - T36000014458 - PR BMO-118
	     	benBankSearchHtml = widgetFactory.createPartySearchButton(itemid,section,false,TradePortalConstants.TRANSFEREE,false);
	     	//Changed End - PR BMO-118
	 }
%>

 
  <%= widgetFactory.createTextField( "RelatedInstrumentId", "TransferELC.RelatedID", terms.getAttribute("related_instrument_id"), "35", true, false, false, "", "", "")%>
  <input type="hidden" name="SearchInstrumentType" value=<%=InstrumentType.EXPORT_DLC%>>

  <br>
  <%= widgetFactory.createSubsectionHeader( "TransferELC.Transferee",isReadOnly,false, isFromExpress,benBankSearchHtml) %> 
    
    <%

       secureParms.put("TransfereeTermsPartyOid", termsPartyTransferee.getAttribute("terms_party_oid"));
    %>
    
        <input type=hidden name="TransfereeTermsPartyType" 
               value=<%=TradePortalConstants.TRANSFEREE %>>
        <input type=hidden name="TransfereeOTLCustomerId" 
               value="<%=termsPartyTransferee.getAttribute("OTL_customer_id")%>">
        <input type=hidden name="VendorId" 
              value="<%=termsPartyTransferee.getAttribute("vendor_id")%>">
      
          <%= widgetFactory.createTextField( "TransfereeName", "TransferELC.TransfereeName", termsPartyTransferee.getAttribute("name"), "35", isReadOnly, !isTemplate, false,  "onBlur='checkTransfereeName(\"" + 
                  StringFunction.escapeQuotesforJS(termsPartyTransferee.getAttribute("name")) + "\")'", "","" ) %>
             
          <%= widgetFactory.createTextField( "TransfereeAddressLine1", "TransferELC.AddressLine1", termsPartyTransferee.getAttribute("address_line_1"), "35", isReadOnly, !isTemplate, false,  "", "","" ) %>
          
          <%= widgetFactory.createTextField( "TransfereeAddressLine2", "TransferELC.AddressLine2", termsPartyTransferee.getAttribute("address_line_2"), "35", isReadOnly, false, false,  "", "","" ) %>
          
          <%= widgetFactory.createTextField( "TransfereeCity", "TransferELC.City", termsPartyTransferee.getAttribute("address_city"), "23", isReadOnly, !isTemplate, false,  "", "","" ) %>
          <div>
          <%= widgetFactory.createTextField( "TransfereeStateProvince", "TransferELC.ProvinceState", termsPartyTransferee.getAttribute("address_state_province"), "8", isReadOnly, false, false,  "", "","inline" ) %>
       	  
       	  <%= widgetFactory.createTextField( "TransfereePostalCode", "TransferELC.PostalCode", termsPartyTransferee.getAttribute("address_postal_code"), "8", isReadOnly, false, false,  "", "","inline" ) %>
          <div style="clear:both;"></div>
         </div>          
          <%
             options = Dropdown.createSortedRefDataOptions(TradePortalConstants.COUNTRY, 
                                                           termsPartyTransferee.getAttribute("address_country"), 
                                                           loginLocale);

             out.println(widgetFactory.createSelectField( "TransfereeCountry", "TransferELC.Country"," " , options, isReadOnly,!isTemplate,false, "", "", ""));
          %>
	      <%= widgetFactory.createTextField( "TransfereePhoneNumber", "TransferELC.PhoneNumber", termsPartyTransferee.getAttribute("phone_number"), "20", isReadOnly,false, false,  "", "","" ) %>
          
          <%= widgetFactory.createTextField( "TransfereeContactName", "TransferELC.ContactName", termsPartyTransferee.getAttribute("contact_name"), "35", isReadOnly,false, false,  "", "","" ) %> 
      </div>
      <div class="columnRight">      
		<%
			String transBankSearchHtml = "";
			String transBankClearHtml = "";
			String addressSearchIndicator = "";
			String identifierStr= "TransfereeBankTermsOid,TransfereeBankName,TransfereeBankAddressLine1,TransfereeBankAddressLine2,TransfereeBankCity,TransfereeBankStateProvince,TransfereeBankCountry,TransfereeBankPostalCode,TransfereeBank";
			String sectionName="applicant";
			if (!isReadOnly) {
				transBankSearchHtml = widgetFactory.createPartySearchButton(identifierStr,sectionName,false,TradePortalConstants.TRANSFEREE_NEGOT,false);
				if (!isReadOnly) {
					transBankClearHtml = widgetFactory.createPartyClearButton( "TransfereeBankClearButton", "clearTransfereeBank()",false,"");
				}	
				
			}   
			 %>
  		  <%= widgetFactory.createSubsectionHeader( "TransferELC.TransfereesBank",isReadOnly,false, isFromExpress,(transBankSearchHtml+transBankClearHtml)) %>
          
          <% String transfereeAddress = termsPartyTransfereeBank.buildAddress(false, resMgr);%>
          <%= widgetFactory.createTextArea( "TransfereeBank", "", transfereeAddress, true,!isTemplate, false, "rows=4", "","" ) %>
    <%       

       secureParms.put("TransfereeBankTermsPartyOid", termsPartyTransfereeBank.getAttribute("terms_party_oid"));
    %>
    
        <input type=hidden name="TransfereeBankTermsOid" 
               value="<%=termsPartyTransfereeBank.getAttribute("terms_party_oid")%>">
        <input type=hidden name="TransfereeBankTermsPartyType" 
               value="<%=TradePortalConstants.TRANSFEREE_NEGOT%>">
        <input type=hidden name="TransfereeBankName" 
               value="<%=termsPartyTransfereeBank.getAttribute("name")%>">
        <input type=hidden name="TransfereeBankAddressLine1"
               value="<%=termsPartyTransfereeBank.getAttribute("address_line_1")%>">
        <input type=hidden name="TransfereeBankAddressLine2"
               value="<%=termsPartyTransfereeBank.getAttribute("address_line_2")%>">
        <input type=hidden name="TransfereeBankCity"
               value="<%=termsPartyTransfereeBank.getAttribute("address_city")%>">
        <input type=hidden name="TransfereeBankStateProvince"
             value="<%=termsPartyTransfereeBank.getAttribute("address_state_province")%>">
        <input type=hidden name="TransfereeBankCountry"
               value="<%=termsPartyTransfereeBank.getAttribute("address_country")%>">
        <input type=hidden name="TransfereeBankPostalCode"
               value="<%=termsPartyTransfereeBank.getAttribute("address_postal_code")%>">
        <input type=hidden name="TransfereeBankOTLCustomerId"
               value="<%=termsPartyTransfereeBank.getAttribute("OTL_customer_id")%>">
       


<%-- KMehta - 7th Mar 2016 - Rel9.5 IR-T36000046337 - Change  - Begin --%>
          <%= widgetFactory.createTextField( "ReferenceNumber", "TransferELC.TransfereeRefNumber", terms.getAttribute("reference_number"), "30", isReadOnly, false, false,  "", "","" ) %>                              
<%-- KMehta - 7th Mar 2016 - Rel9.5 IR-T36000046337 - Change  - End --%>               
          <%
             options = Dropdown.createSortedCurrencyCodeOptions(currency, loginLocale);

             out.println(widgetFactory.createSelectField( "TransactionCurrency", "transaction.Currency"," " , options, isReadOnly,!isTemplate,false, "class='char5'", "", ""));
          %>
	    
	      <%= widgetFactory.createTextField( "TransactionAmount", "TransferELC.Amount", displayAmount, "30", isReadOnly, !isTemplate, false,  "class='char25'", "","" ) %>
          
     	  <div class = "formItem">
		     <%= widgetFactory.createSubLabel("TransferELC.AmountTolerance") %>
		  </div>
		  <div class = "formItem">
		     <%= widgetFactory.createSubLabel( "transaction.Plus") %>
		     <%= widgetFactory.createPercentField( "TransactionAmountPlusTolerance", "", terms.getAttribute("amt_tolerance_pos"), isReadOnly) %>
		     <%= widgetFactory.createHoverHelp("TransactionAmountPlusTolerance", "transaction.Plus")%>
		     <%= widgetFactory.createSubLabel( "transaction.Percent") %>
		     <%= widgetFactory.createSubLabel( "transaction.Minus") %>
		     <%= widgetFactory.createPercentField( "TransactionAmountMinusTolerance", "", terms.getAttribute("amt_tolerance_neg"), isReadOnly) %>
		     <%= widgetFactory.createHoverHelp("TransactionAmountMinusTolerance", "transaction.Minus")%>
		     <%= widgetFactory.createSubLabel( "transaction.Percent") %>
		  <div style="clear:both;"></div>
		  </div>               
         
      	  <%= widgetFactory.createDateField("ExpiryDate", "TransferELC.ExpiryDate", StringFunction.xssHtmlToChars(terms.getAttribute("expiry_date")),isReadOnly, true, false,  "class='char6'", dateWidgetOptions, "") %>     	  
      	
      	  <%= widgetFactory.createDateField("LatestShipmentDate", "TransferELC.LatestShipDate", terms.getFirstShipment().getAttribute("latest_shipment_date"),isReadOnly, false, false,  "class='char6'", dateWidgetOptions, "" ) %>
        
      </div>

<script LANGUAGE="JavaScript">

  function clearTransfereeBank()
  {
     document.forms[0].TransfereeBankName.value          = "";
     document.forms[0].TransfereeBankAddressLine1.value  = "";
     document.forms[0].TransfereeBankAddressLine2.value  = "";
     document.forms[0].TransfereeBankCity.value          = "";
     document.forms[0].TransfereeBankStateProvince.value = "";
     document.forms[0].TransfereeBankCountry.value       = "";
     document.forms[0].TransfereeBankPostalCode.value    = "";
     document.forms[0].TransfereeBankOTLCustomerId.value = "";
     document.forms[0].TransfereeBank.value              = "";
  }

  function checkTransfereeName(originalName)
  {
     if (document.forms[0].TransfereeName.value != originalName)
     {
        document.forms[0].TransfereeOTLCustomerId.value = "";
     }
  }

</script>
