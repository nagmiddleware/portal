<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 *  Vasavi CR 524 03/31/2010
--%>
<%--
*******************************************************************************
                     Export Collection Trace Page - Link section

  Description: 
    Contains HTML to display links to the other sections.

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transaction-EXP_OCO-TRC-Links.jsp" %>
*******************************************************************************
--%>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td width="14" nowrap>&nbsp;</td>
      <td nowrap align="left" valign="middle"> 
        <p class="ControlLabel">
	  <a href="#General">
		<%=resMgr.getText("ExportCollectionTrace.General", 
                              	   TradePortalConstants.TEXT_BUNDLE)%>
	  </a>
	</p>
      </td>
      <td width="15" nowrap>&nbsp;</td>
      <td nowrap align="left" valign="middle"> 
        <p class="ControlLabel">
	  <a href="#InstructionstoBank">
		<%=resMgr.getText("ExportCollectionTrace.InsttoBank", 
                              	   TradePortalConstants.TEXT_BUNDLE)%>
	  </a>
	</p>
      </td>
      <td width="15" nowrap>&nbsp;</td>
      <td nowrap align="left" valign="middle"> 
        <p class="ControlLabel">&nbsp;</p>
      </td>
      <td nowrap height="30" width="15">&nbsp;</td>
      <td nowrap height="30"> 
        <p class="ControlLabel">&nbsp;</p>
      </td>
      <td width="100%" height="30">&nbsp;</td>
    </tr>
<%    //Determine if we need to display the PDF report links...

      if ( (!transactionStatus.equals(TradePortalConstants.TRANS_STATUS_STARTED)) &&
	   (!transactionStatus.equals(TradePortalConstants.TRANS_STATUS_DELETED)) && 
	   (!transactionStatus.equals(TradePortalConstants.TRANS_STATUS_REJECTED_BY_BANK)) ){
%>
    <tr> 
      <td width="14" nowrap>&nbsp;</td>
      <td nowrap align="left" valign="middle"> 
        <p class="ControlLabel">
<%
          // Create the "3. Export Collection" pdf link
          linkArgs[0] = TradePortalConstants.PDF_NEW_EXPORT_COLLECTION;
          linkArgs[1] = EncryptDecrypt.encryptStringUsingTripleDes(transactionOid);//Use common key for encryption of parameter to doc producer
          linkArgs[2] = loginLocale;
          linkArgs[3] = userSession.getBrandingDirectory();
%>
  	  <%= formMgr.getDocumentLinkAsHrefInFrame ( resMgr.getText("ExportCollectionTrace.ExportCollection1", 
                              	   	 	                    TradePortalConstants.TEXT_BUNDLE), 
					             "CollectionSchedule",
                                                     linkArgs,
                                                     response) %>
	</p>
      </td>
      <td width="15" nowrap>&nbsp;</td>

<%-- This was removed due to a change in the web page definition...on 5/1/01...
     ...used to display the Bill of Echanges Link...
      <td nowrap align="left" valign="middle"> 

        <p class="ControlLabel">
<%
          // Create the "5. Bill of Exchanges pdf link
          linkArgs[0] = TradePortalConstants.PDF_BILL_OF_EXCHANGES;
          linkArgs[1] = EncryptDecrypt.encryptStringUsingTripleDes(transactionOid);//Use common key for encryption of parameter to doc producer
          linkArgs[2] = loginLocale;
          linkArgs[3] = null;
%>
  	  <%= formMgr.getDocumentLinkAsHrefInFrame ( resMgr.getText("ExportCollectionTrace.BillOfExchanges1", 
                              	   	 	                    TradePortalConstants.TEXT_BUNDLE), 
					             TradePortalConstants.PDF_BILL_OF_EXCHANGES,
                                                     linkArgs,
                                                     response) %>
	</p>
      </td>
--%>
<%-- Instead this td tag was inserted... --%>
      <td nowrap align="left" valign="middle"> 
        <p class="ControlLabel">&nbsp;</p>
      </td>

      <td width="15" nowrap>&nbsp;</td>
      <td nowrap align="left" valign="middle"> 
        <p class="ControlLabel">&nbsp;</p>
      </td>
      <td height="30">&nbsp;</td>
      <td height="30">&nbsp;</td>
      <td width="100%" height="30">&nbsp;</td>
    </tr>
<%
    }
%>
  </table>
