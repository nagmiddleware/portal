<%--
 *
 *     Copyright   2001
 *     American Management Systems, Incorporated
 *     All rights reserved
--%>
<%--
*******************************************************************************
                 Export Collection Issue Page - Multiple Tenors section

  Description:
    Contains HTML to create the Export COL Issue Multiple Tenors section.

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transaction-EXP_COL-ISS-MultiTenors.frag" %>
*******************************************************************************
--%>
<%
int countTenor = 0;

for(int i=1; i<= NUMBER_OF_TENORS; i++){
	
	if( !"".equals(terms.getAttribute("tenor_"+i)) 
			|| !"".equals(terms.getAttribute("tenor_"+i+"_amount")) 
				|| !"".equals(terms.getAttribute("tenor_"+i+"_draft_number")) ){
		countTenor++;
	}
}
if(countTenor <= 2){
	countTenor = 2;
}

%>
<table style="width:70%" border="0" cellspacing="0" cellpadding="0" id="MultiTenorLabel">
      <tr>
      <td style="width:10%">&nbsp;</td>
            <td class="genericCol"><%=resMgr.getText("ExportCollectionIssue.MultipleTenorDetails",TradePortalConstants.TEXT_BUNDLE)%>&nbsp;<%=widgetFactory.createNote("ExportCollectionIssue.MultipleTenorIfNecessary")%></td>
      </tr>
  </table>
    <table  style="width:92%;float:right;" border="1" cellspacing="0" cellpadding="0" class="formDocumentsTable" id="MultTenor">
      <thead>
            <tr>
                  <th>&nbsp;</th>
                  <th>&nbsp;</th>
                  <th class="genericCol" width="28%"><%=resMgr.getText("ExportCollectionAmend.TenorColumn",TradePortalConstants.TEXT_BUNDLE)%></th>
                  <th class="genericCol" width="28%"><%=resMgr.getText("ExportCollectionIssue.MultipleTenorAmount",TradePortalConstants.TEXT_BUNDLE)%></th>
                  <th class="genericCol" width="28%"><%=resMgr.getText("ExportCollectionIssue.MultipleTenorDraftNumber",TradePortalConstants.TEXT_BUNDLE)%></th>
            </tr>
      </thead>
      <tbody>
            <% for(int multiTenorIndex = 1; multiTenorIndex <= countTenor; multiTenorIndex++) { %>
                  <input type=hidden value="<%=multiTenorIndex%>" name="multiTenorIndex" id="multiTenorIndex">
                  <%@ include file="Transaction-EXP_COL-AMD-Tenors.frag"%>
            <%    } %>
      </tbody>
 </table>
 <br/>
 <% //Rel 8.3 IR#T36000022816 - Buttons visible on in ReadOnly mode 
 if(!isReadOnly){%>
 <table width="100%" border="0" cellspacing="0" cellpadding="0" id= "tenorButtons" style="float:right">
      <tr>
      <td width="80%">&nbsp;</td>
            <td>
                  <button data-dojo-type="dijit.form.Button" type="button" name="AddTenorButton" id="AddTenorButton"><%=resMgr.getText("ExportCollection.AddTenor",TradePortalConstants.TEXT_BUNDLE)%>
                        <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
                              javascript:addTenor();
                        </script>
                  </button>
            </td>
            <td>
                  <button data-dojo-type="dijit.form.Button" type="button" id="DeleteTenorButton"><%=resMgr.getText("ExportCollection.AddDelete",TradePortalConstants.TEXT_BUNDLE)%>
                        <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
                              javascript:deleteTenor();
                        </script>
                  </button>
            </td>
      </tr>
</table>
<% }%>
<%--  Naveen 01-August-2012 IR-T36000003243. Table End tag- End   --%>

      <script language="JavaScript">

	 <%-- function to reNumber the rows during delete and Add. --%>
	function reNumber(){
	<%-- so that user can only add 2 rows --%>
		var tbl = document.getElementById('MultTenor');<%-- to identify the table in which the row will get insert --%>
		var multiTenorIndex = tbl.rows.length;
		var row;
		var cll;
		var rowNum=1;
		var totalCells;
		for( i=1;i<multiTenorIndex;i++){
			<%-- Srinivasu_D IR#T36000023942 Re.8.4 01/20/2014 - Start - modified the way of reading rows --%>
			<%-- 
			row = tbl.rows(i);
			cll = row.cells(1);
			 --%>
			totalCells = tbl.rows[i].cells;
			cll = totalCells[1];
			<%-- Srinivasu_D IR#T36000023942 Re.8.4 01/20/2014 - End --%>
				if(document.getElementById('MultTenor').rows[i].style.display==''){
					cll.innerHTML =rowNum+'.';
					rowNum++;
					}<%-- end of if  --%>
		}<%-- end of for --%>
	}<%-- end reNumber function.	 --%>
	
function addTenor(){
		var tbl = document.getElementById('MultTenor');<%-- to identify the table in which the row will get insert --%>
		var multiTenorIndex = tbl.rows.length;
		var visibleTenorTableLength=1;
		for(i=1;i<multiTenorIndex;i++){
		  if(document.getElementById('MultTenor').rows[i].style.display==''){
		  	visibleTenorTableLength++;
		  }
		}
		if(visibleTenorTableLength > '6'){
			return false;
		}
		var NewRow="";
		var isReadOnly = 'false';	 

   	 	var newRow = tbl.insertRow(multiTenorIndex);<%-- creation of new row --%>
   	 	var cell0 = newRow.insertCell(0);<%-- first  cell in the row --%>
		var cell1 = newRow.insertCell(1);<%-- second  cell in the row --%>
		var cell2 = newRow.insertCell(2);<%-- third  cell in the row --%>
		var cell3 = newRow.insertCell(3);<%-- fourth  cell in the row --%>
		var cell4 = newRow.insertCell(4);<%-- fifth  cell in the row --%>
		newRow.id = multiTenorIndex;
		
		cell1.setAttribute("id","index"+multiTenorIndex);
		
		var tenorAmt='null';
		var tenorDraft='null';
		
		
		
		cell0.innerHTML ='<input data-dojo-type=\"dijit.form.CheckBox\" name=\"TenorInd'+multiTenorIndex+'\" id=\"TenorInd'+multiTenorIndex+'\" isReadOnly>';
		cell1.innerHTML =multiTenorIndex+'.';
        cell2.innerHTML ='<input data-dojo-type=\"dijit.form.TextBox\" name=\"Tenor'+multiTenorIndex+'\" id=\"Tenor'+multiTenorIndex+'\" value="" class=\"char18\" maxLength=35>'
        cell3.innerHTML ='<input data-dojo-type=\"dijit.form.TextBox\" name=\"Tenor'+multiTenorIndex+'Amount\" id=\"Tenor'+multiTenorIndex+'Amount\" value="" class=\"char18\"  maxLength=22>'
        cell4.innerHTML ='<input data-dojo-type=\"dijit.form.TextBox\" name=\"Tenor'+multiTenorIndex+'DraftNumber\" id=\"Tenor'+multiTenorIndex+'DraftNumber\" value="" class=\"char18\" maxLength=15>'
        
        require(["dojo/parser"], function(parser) {
         	parser.parse(newRow.id);
    	 });
    	 reNumber();<%-- calling reNumber function to rearrange RowNums. --%>
}

      function deleteTenor() {
      <%-- so that user can only add 2 rows --%>
		var tbl = document.getElementById('MultTenor');<%-- to identify the table in which the row will get insert --%>
		var multiTenorIndex = tbl.rows.length;
		
		for(var i=1; i< multiTenorIndex; i++){
		
		if(document.getElementById('MultTenor').rows[i].style.display==''){
		   
			if( dijit.byId('TenorInd'+i) && dijit.byId('TenorInd'+i).getValue()=='Y'|| dijit.byId('TenorInd'+i).getValue()=='on' ){
					
					dijit.byId('TenorInd'+i).set('checked',false);
					document.getElementById('TenorInd'+i).checked = false;
					dijit.byId('Tenor'+i).set('value','');
					document.getElementById('Tenor'+i).value = "";
					dijit.byId('Tenor'+i+'Amount').set('value','');
					document.getElementById('Tenor'+i+'Amount').value = "";
					dijit.byId('Tenor'+i+'DraftNumber').set('value','');
					document.getElementById('Tenor'+i+'DraftNumber').value = "";
					
					
					dijit.byId('TenorInd'+i).destroy( true );
					dijit.byId('Tenor'+i).destroy( true );
					dijit.byId('Tenor'+i+'Amount').destroy( true );
					dijit.byId('Tenor'+i+'DraftNumber').destroy( true );
					
					document.getElementById('TenorInd'+i).removeAttribute('id'); 
					document.getElementById('Tenor'+i).removeAttribute("id"); 
					document.getElementById('Tenor'+i+'Amount').removeAttribute("id"); 
					document.getElementById('Tenor'+i+'DraftNumber').removeAttribute("id"); 
					document.getElementById('MultTenor').rows[i].style.display = 'none';
					
				}
				reNumber();<%-- calling reNumber function to rearrange RowNums. --%>
			}<%-- end of if checking style. --%>
		}
		
		return false;
		 var table = dojo.byId('MultTenor');
		 var i;
		  <%-- get index# for field naming --%>
		  var insertRowIdx = (table.rows.length);
		  for(i=insertRowIdx; i>0; i--){
			 if (dojo.byId('TenorInd'+(i-1)).checked == true) {

				 dojo.byId('MultTenor').deleteRow(i-1);
				 dijit.byId('TenorInd'+(i-1)).value="";
				 dijit.byId('TenorInd'+(i-1)).destroy( true );
				 dijit.byId('Tenor'+(i-1)).value="";
				 dijit.byId('Tenor'+(i-1)).destroy();
				 dijit.byId('Tenor'+(i-1)+'Amount').value="";
				 dijit.byId('Tenor'+(i-1)+'Amount').destroy();
				 dijit.byId('Tenor'+(i-1)+'DraftNumber').value="";
				 dijit.byId('Tenor'+(i-1)+'DraftNumber').destroy();

			 }
		 }
		 
	}
   </script>
