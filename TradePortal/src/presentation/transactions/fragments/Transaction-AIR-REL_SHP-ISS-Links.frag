<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
*******************************************************************************
     Air Waybill Release and Shipping Guarantee Issue  Page - Link section

  Description:
    Contains HTML to display links to the other sections.

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transaction-AIR-REL_SHP-ISS-Links.jsp" %>
*******************************************************************************
--%>

  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td width="14" nowrap>&nbsp;</td>
      <td nowrap align="left" valign="middle"> 
        <p class="ControlLabel">
          <a href="#General">
            <%=resMgr.getText("AWB_SGTEE.General", TradePortalConstants.TEXT_BUNDLE)%>
          </a>
        </p>
      </td>
      <td width="15" nowrap>&nbsp;</td>
      <td nowrap align="left" valign="middle"> 
        <p class="ControlLabel">
          <a href="#Value">
            <%=resMgr.getText("AWB_SGTEE.Value", TradePortalConstants.TEXT_BUNDLE)%>
          </a>
        </p>
      </td>
      <td width="15" nowrap>&nbsp;</td>
      <td nowrap align="left" valign="middle"> 
        <p class="ControlLabel">&nbsp;</p>
      </td>
      <td nowrap height="30" width="15">&nbsp;</td>
      <td nowrap height="30"> 
        <p class="ControlLabel">&nbsp;</p>
      </td>
      <td width="100%" height="30">&nbsp;</td>
    </tr>
    <tr> 
      <td width="14" nowrap>&nbsp;</td>
      <td nowrap align="left" valign="middle"> 
        <p class="ControlLabel">
          <a href="#ShipmentDetails">
            <%=resMgr.getText("AWB_SGTEE.ShipmentDetails", TradePortalConstants.TEXT_BUNDLE)%>
          </a>
        </p>
      </td>
      <td width="15" nowrap>&nbsp;</td>
      <td nowrap align="left" valign="middle"> 
        <p class="ControlLabel">
          <a href="#BankInstructions">
            <%=resMgr.getText("AWB_SGTEE.InstructionsToBank", TradePortalConstants.TEXT_BUNDLE)%>
          </a>
        </p>
      </td>
      <td width="15" nowrap>&nbsp;</td>
      <td nowrap align="left" valign="middle"> 
        <p class="ControlLabel">&nbsp;</p>
      </td>
      <td height="30">&nbsp;</td>
      <td height="30">&nbsp;</td>
      <td width="100%" height="30">&nbsp;</td>
    </tr>
  </table>
