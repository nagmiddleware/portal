<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
*******************************************************************************
                     Approval to Pay Issue Page - Link section

  Description:
    Contains HTML to display links to the other sections.

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transaction-ATP-ISS-General.jsp" %>
*******************************************************************************
--%>

  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td width="14" nowrap>&nbsp;</td>
      <td nowrap align="left" valign="middle"> 
        <p class="ControlLabel">
          <a href="#General">
            <%=resMgr.getText("ApprovalToPayIssue.General", 
                              TradePortalConstants.TEXT_BUNDLE)%>
          </a>
        </p>
      </td>
      <td width="15" nowrap>&nbsp;</td>
      <td nowrap align="left" valign="middle"> 
        <p class="ControlLabel">
          <a href="#DocumentsRequired">
            <%=resMgr.getText("ApprovalToPayIssue.DocsRequired", 
                              TradePortalConstants.TEXT_BUNDLE)%>
          </a>
        </p>
      </td>
      <td width="100%" height="30">&nbsp;</td>
    </tr>
    <tr> 
      <td width="14" nowrap>&nbsp;</td>
      <td nowrap align="left" valign="middle"> 
        <p class="ControlLabel">
          <a href="#TransportDocsShipment">
            <%=resMgr.getText("ApprovalToPayIssue.TransportDocsShipment", 
                              TradePortalConstants.TEXT_BUNDLE)%>
          </a>
        </p>
      </td>
      <td width="15" nowrap>&nbsp;</td>
      <td nowrap align="left" valign="middle"> 
        <p class="ControlLabel">
          <a href="#OtherConditions">
            <%=resMgr.getText("ApprovalToPayIssue.OtherConditions", 
                              TradePortalConstants.TEXT_BUNDLE)%>
          </a>
        </p>
      </td>
      <td width="100%" height="30">&nbsp;</td>
    </tr>
    <tr> 
      <td width="14" nowrap>&nbsp;</td>
      <td nowrap align="left" valign="middle"> 
        <p class="ControlLabel">
          <a href="#BankInstructions">
            <%=resMgr.getText("ApprovalToPayIssue.ClientBankInstructions", 
                              TradePortalConstants.TEXT_BUNDLE)%>
          </a>
        </p>
      </td>
      <td width="15" nowrap>&nbsp;</td>
      <td nowrap align="left" valign="middle"> 
        <p class="ControlLabel">
          <a href="#InternalInstructions">
            <%=resMgr.getText("ApprovalToPayIssue.InternalInstructions", 
                              TradePortalConstants.TEXT_BUNDLE)%>
          </a>
        </p>
      </td>
      <td width="100%" height="30">&nbsp;</td>
    </tr>
	
<%
//Add Issue Application Form PDF Link if not in admin template mode
//and the transaction is not started and not deleted...
  if ((!userSession.getSecurityType().equals(TradePortalConstants.ADMIN)) &&
     (!transaction.getAttribute("transaction_status").equals(TradePortalConstants.TRANS_STATUS_STARTED)) &&
     (!transaction.getAttribute("transaction_status").equals(TradePortalConstants.TRANS_STATUS_DELETED)) && 
	 (!transaction.getAttribute("transaction_status").equals(TradePortalConstants.TRANS_STATUS_REJECTED_BY_BANK)) ) {
%>
    <tr> 
      <td width="15" nowrap>&nbsp;</td>
      <td nowrap align="left" valign="middle"> 
        <p class="ControlLabel">
	    <%
	      linkArgs[0] = TradePortalConstants.PDF_ATP_ISSUE_APPLICATION;
	      linkArgs[1] = EncryptDecrypt.encryptStringUsingTripleDes(transactionOid);//Use common key for encryption of parameter to doc producer
	      linkArgs[2] = "en_US";
	      //loginLocale;
	      linkArgs[3] = userSession.getBrandingDirectory();
	    %>
	      <%= formMgr.getDocumentLinkAsHrefInFrame(resMgr.getText("ApprovalToPayIssue.IssueApplicationForm",
	      							TradePortalConstants.TEXT_BUNDLE),
	      							TradePortalConstants.PDF_ATP_ISSUE_APPLICATION,
	      							linkArgs,
	      							response) %>
	</p>
      </td>
      <td width="15" nowrap>&nbsp;</td>
      <td nowrap align="left" valign="middle">&nbsp;</td>
      <td height="30">&nbsp;</td>
      <td height="30">&nbsp;</td>
      <td width="100%" height="30">&nbsp;</td>
    </tr>
<%
  }
%>


  <%
  // Only display the bank defined section if the user is an admin user
  if( userSession.getSecurityType().equals(TradePortalConstants.ADMIN) )
   {
  %>    
    <tr> 
      <td width="14" nowrap>&nbsp;</td>
      <td nowrap align="left" valign="middle"> 
        <p class="ControlLabel">
          <a href="#BankDefined">
            <%=resMgr.getText("ApprovalToPayIssue.BankDefined", 
                              TradePortalConstants.TEXT_BUNDLE)%>
          </a>
        </p>
      </td>
      <td width="15" nowrap>&nbsp;</td>
      <td>&nbsp;</td>
      <td width="100%" height="30">&nbsp;</td>
    </tr>
   <%
    }
   %>
  </table>
