 <%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
*******************************************************************************
                 OutGoing Guarantee Issue Page - Charges Incurred section

  Description:
    Contains HTML to create the OutGoing Guarantee Issue Charges Incurred section.  

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="transaction/Transaction-GAUR-ISS-ChargesIncurred.frag" %>
*******************************************************************************
--%>

<script type="text/javascript">

function add3moreChargesIncurred1(){
	  var table = dojo.byId('chrgIncTable');
	  <%-- get index# for field naming --%>
	  var lastElement = table.rows.length-1;

		common.appendAjaxTableRows(myTable,"pmtTermsIndex",
	    		  "/portal/transactions/fragments/Add2morePaymentTermRows.jsp?pmtTermsIndex="+rowIndex+"&count="+cnt+"&TermsPmtType="+TermsPmtType); <%-- table length includes header, so subtract 1 --%>


	  for (i=lastElement;i<lastElement+3;i++) {
		  <%-- add the table row --%>
		  var newRow = table.insertRow(i);
		  newRow.id = "AddChrgInc"+i;
		  var cell0 = newRow.insertCell(0);<%-- first  cell in the row --%>
		  var cell1 = newRow.insertCell(1);<%-- second  cell in the row --%>
		  var cell2 = newRow.insertCell(2);<%-- third  cell in the row --%>
		  j = i;
		  var name = "FeeOid"+j;
		  var html="<select data-dojo-type='t360.widget.FilteringSelect' name='ChrgIncChargeType"+j+"' id='ChrgIncChargeType"+j+"' class='formItem' style=\'width: 165px;\'></select>"
		  cell0.innerHTML	= html;						
		  var html1="<select data-dojo-type='t360.widget.FilteringSelect' name='ChrgIncCurrencyCode"+j+"' id='ChrgIncCurrencyCode"+j+"' class='formItem' style=\'width: 165px;\'></select>"
		  cell1.innerHTML	= html1;	
		  cell2.innerHTML ='<input data-dojo-type=\"dijit.form.TextBox\" name=\"ChrgIncAmount'+j+'\" id=\"ChrgIncAmount'+j+'\" class=\'char12\' " maxLength=\"30\">'
		  
	      require(["dojo/parser", "dijit/registry"], function(parser, registry) {
	             parser.parse(newRow.id);
	             
	           	
		    });
	  }
	  document.getElementById("noOfChrgIncRows").value = eval(lastElement+3);
	}
	
	
</script>
	<tr><td>
      <%= widgetFactory.createSubsectionHeader("GuaranteeIssue.ChargesIncurred") %>
	</td></tr>
	<tr><td>
	&nbsp;&nbsp;&nbsp;<%= widgetFactory.createNote("GuaranteeIssue.ChargeDetails") %><br>
	</td></tr>

<input type=hidden value=<%=chrgIncRowCount %> name="numberOfMultipleObjects"   id="noOfChrgIncRows"> 
<table><tr><td>&nbsp;&nbsp;</td><td width=""><table id="chrgIncTable" class="formDocumentsTable">
<thead>
	<tr>
		<th class="genericCol">&nbsp;<%=resMgr.getText("GuaranteeIssue.ChargeType",TradePortalConstants.TEXT_BUNDLE)%></th>
		<th class="genericCol">&nbsp;<%=resMgr.getText("GuaranteeIssue.ChrgIncCurrency",TradePortalConstants.TEXT_BUNDLE)%></th>
		<th class="genericCol">&nbsp;<%=resMgr.getText("GuaranteeIssue.ChrgIncAmount",TradePortalConstants.TEXT_BUNDLE)%></th>
		<th class="genericCol">&nbsp;</th>
	</tr>
</thead>

<tbody>
					 <%--passes in pmtTermsList, isReadOnly, isFromExpress, loginLocale,
			          plus new pmtTermsIndex below--%>
					<%
					  int chrgIncIndex = 0;
					%>
			      	<%@ include file="TransactionChrgIncRows.frag" %> 
	
</tbody>
</table>
</td></tr></table>
<br>
<div>
  <% if (!(isReadOnly)) 
		{     %>	 
				&nbsp;&nbsp;&nbsp;&nbsp;<button data-dojo-type="dijit.form.Button" type="button" id="add3moreChargesIncurred">
				<%= resMgr.getText("common.Add3MoreChargesIncurred", TradePortalConstants.TEXT_BUNDLE) %>
				<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
					
				</script>
			</button>	
	<% 
		}else{  %>
			&nbsp;
		<% 
		} %>  		
  	</div>

  
  <br>
