<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
*******************************************************************************
                 Loan Request Page - Bank Defined section

  Description:
    Contains HTML to create the Loan Request Bank Defined section.  

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transaction-LRQ-ISS-BankDefined.jsp" %>
*******************************************************************************
--%>
<%
	options = Dropdown.createSortedRefDataOptions(
			TradePortalConstants.IN_ADVANCE_DISP_TYPE,
			terms.getAttribute("in_advance_disposition"), loginLocale);
%>
<%=widgetFactory.createSelectField("InAdvanceDispType",
					"LoanRequest.InAdvanceDisposition", "", options,
					isReadOnly, false, false, "", "", "inline")%>

<%
	options = Dropdown.createSortedRefDataOptions(
			TradePortalConstants.IN_ARREARS_DISP_TYPE,
			terms.getAttribute("in_arrears_disposition"), loginLocale);
%>
<%=widgetFactory.createSelectField("InArrearsDispType",
					"LoanRequest.InArrearsDisposition", "", options,
					isReadOnly, false, false, "", "", "inline")%>
<%=widgetFactory.createTextField("WorkItemPriority",
					"LoanRequest.WorkItemPriority",
					terms.getAttribute("work_item_priority"), "1", isReadOnly,
					false, false, "width", "", "inline")%>
