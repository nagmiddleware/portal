<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
*******************************************************************************
                 Approval to Pay Issue Page - Bank Defined section

  Description:
    Contains HTML to create the Approval to Pay Issue Bank Defined section.  

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transaction-ATP-ISS-BankDefined.jsp" %>
*******************************************************************************
--%>
<%= widgetFactory.createTextField( "PurposeType", "ApprovalToPayIssue.PurposeType", terms.getAttribute("purpose_type"), "3", isReadOnly) %>
