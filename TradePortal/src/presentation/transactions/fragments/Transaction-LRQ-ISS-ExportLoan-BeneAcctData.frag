<%--
*******************************************************************************

  Description:
  A fragment for displaying terms party accounts.
  Fairly generic, but should be copied for specific instrument behavior.

  The following variables are passed:

    termsParty - a TermsPartyWebBean
    isReadOnly
    isTemplate
    widgetFactory

*******************************************************************************
--%>
<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.html.*,java.math.*" %>

<%
  // Logic to handle account list.  If no payee has been searched, there is
  // only an enterable account number field.  Otherwise, we display the set of accounts defined
  // for the party (when the party is selected, we get the set of accounts for that party 
  // and save it on the TermsParty record -- if the party's accounts change later, we do not update
  // the set on the TermsParty record.) as well as an enterable account number field.  The account 
  // number selected by the user will be one of the payee's predefined accounts or the 
  // entered account number.
  
  		  String exp_benAcctList = StringFunction.xssHtmlToChars(termsParty.getAttribute("acct_choices"));
		  DocumentHandler expAcctData = new DocumentHandler(exp_benAcctList, true);
		  DocumentHandler expAcct;
		  boolean expFoundAcctMatch = false; // indicates if ANY predefined expAcct num matches ben. expAcct
		  //     number from business object
		  boolean expFoundMatch = false; // indicates if a given expAcct num matches ben. expAcct
		  //     number from business object
		  int numAccts, i;
		  Vector accounts = expAcctData.getFragments("/DocRoot/ResultSetRecord");
		  numAccts = accounts.size();
		  
		  String expAcctNum = null;
		  String expAcctCcy = null;

		  Debug.debug("Account Choices for Ben Party: " + exp_benAcctList);
		  Debug.debug("Number of account in list is " + numAccts);

		  // Set the showBenAccts1 flag that determines if a list of accounts with radio buttons
		  // appears.  They appear if we have a set of accounts (i.e., it's been preloaded into
		  // a exp_benAcctList variable).  However, in readonly mode, we do not display the list.

		  // TLE - 11/20/06 - IR#ANUG110757179 - Add Begin
		  //if (numAccts > 0) showBenAccts1 = true;
		  boolean showBenAccts1 = false;
		  if (numAccts > 0)
				showBenAccts1 = true;
		  // TLE - 11/20/06 - IR#ANUG110757179 - Add Begin
		  if (isReadOnly)
				showBenAccts1 = false;

		  Debug.debug("showBenAccts1 flag is " + showBenAccts1);

		  // Store the set of accounts for the beneficiary in a hidden field.  This gets written to the
		  // TermsParty record so we always have the set of accounts at the time the party was selected.
		  if(null != exp_benAcctList && !"".equals(exp_benAcctList)){
	%>
	<input type=hidden value='<%=exp_benAcctList%>' name=BenAcctChoices>
	<%
		  }
		  // Peform selective logic throughout that determines how this section is displayed -- as
		  // a set of radio buttons and accounts, or just a single enterable account.
		  if (showBenAccts1) {
	%>
	<%--=widgetFactory.createTextField("","LoanRequest.BeneficiaryAccountNumber", "", "30", true, false, false, "width", "", "")--%>

	<%
		  }
	%>
	<%
		  if (showBenAccts1) {
				// This loop prints the set of accounts previously determined when the party was
				// selected.
				for (i = 0; i < numAccts; i++) {
					  expAcct = (DocumentHandler) accounts.elementAt(i);
	%>
	<%
		  // Display a radio button using the "account number/currency" from the
					  // account list (for whichever row we're on).  Select the correct
					  // radio button if we match on the current value of "acct_num" on the
					  // beneficiary party.
					  out.println("<td width=24 nowrap>");

					  // (Don't know why, but the first "get" from the doc must be without the /,
					  // succeeding ones must be with a /.  This is different from similar logic
					  // elsewhere.  Believed to be a symptom of flattening a DocHandler to a string
					  // and then creating another DocHandler from the string.)

					  expAcctNum = expAcct.getAttribute("ACCOUNT_NUMBER"); // no "slash"
					  expAcctCcy = expAcct.getAttribute("/CURRENCY"); // get with "slash"

					  expFoundMatch = expAcctNum.equals(termsParty.getAttribute("acct_num"));

					  if (!expFoundAcctMatch) {
							expFoundAcctMatch = expFoundAcctMatch | expFoundMatch;
					  }
	
		String radioLabel = "";
		  if (expAcctCcy.equals("")) {
			  radioLabel = expAcctNum;
	
		  } else {
			  radioLabel = expAcctNum + " (" + expAcctCcy + ")";
	
		  }
	%>
	<%=widgetFactory.createRadioButtonField("SelectedAccountExp", 
			"SelectedAccountExp"+expAcctNum, radioLabel, 
			expAcctNum+ TradePortalConstants.DELIMITER_CHAR+ expAcctCcy, 
			expFoundMatch, isReadOnly,	"onClick='pickTheFollowingBeneficiaryRadioExp();'","")%>
	<div style=\"clear:both;\"></div>
	<%
		  // End of for loop
				}
		  } // end if showBenAccts1
	%>
	<%
		  // This IF sets up expAcctNum and expAcctCcy fields with values if necessary and uses them
		  // for displaying the enterable account field.

		  if (showBenAccts1) {
				// Display radio button for selection of the enterable account.  If we haven't found
				// an account match yet, then we might match on the enterable value.  If they match,
				// turn on the radio button.
				if (expFoundAcctMatch) {
					  expAcctNum = "";
					  expAcctCcy = "";
					  expFoundMatch = false;
				} else {
					  expAcctNum = termsParty.getAttribute("acct_num");
					  expAcctCcy = termsParty.getAttribute("acct_currency");
					  if (expAcctNum == null)
							expAcctNum = "";
							expFoundMatch = expAcctNum.equals(termsParty.getAttribute("acct_num"));
				}
	%>
	<%=widgetFactory.createRadioButtonField("SelectedAccountExp",TradePortalConstants.USER_ENTERED_ACCT,"",
			TradePortalConstants.USER_ENTERED_ACCT, expFoundMatch, isReadOnly,"onClick='pickTheFollowingBeneficiaryRadioExp();'", "")%>
	<%
		  } else {
				// We're not displaying the account list, so populate the account field with the
				// account from the beneficiary party record.
				expAcctNum = termsParty.getAttribute("acct_num");
				expAcctCcy = termsParty.getAttribute("acct_currency");
				if (expAcctNum == null)
					  expAcctNum = "";
		  }
	%>
	<%-- KMEhta IR T36000022557 on 15/1/2014 changed required indicator to !isTemplate --%>
	<%=widgetFactory.createTextField("EnteredAccountExp","",
			expAcctNum,"30",isReadOnly,!isTemplate,false,"onChange='pickTheFollowingBeneficiaryRadioExp();'","", "none")%>
