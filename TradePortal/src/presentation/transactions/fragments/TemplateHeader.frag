<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
*******************************************************************************
                       Transaction Page - Template section

  Description:
    Contains HTML to display the button bar and template section when a 
  template is being edited.

  This relies on two parameters 

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="TemplateHeader.jsp" %>
*******************************************************************************
--%>
<%
  boolean isDefault = false;
  //Naveen IR-T36000011495(ANZ- 784)- Start
  String fixedFlag = template.getAttribute("fixed_flag");
  if(fixedFlag == null || fixedFlag.equals("")) {
  	fixedFlag = TradePortalConstants.INDICATOR_NO;
  }
  //Naveen IR-T36000011495(ANZ- 784)- End
  // This is where to go to when the page is closed.
  String returnAction = "goToInstrumentCloseNavigator";

  //cquinton 1/18/2013 remove old close action behavior 
  
  String defaultFlag = template.getAttribute("default_template_flag");
  if ((defaultFlag != null) 
     && defaultFlag.equals(TradePortalConstants.INDICATOR_YES)) {
      isDefault = true;
  }

  if (!isReadOnly) {
      showTemplateSave = true;
      showTemplateDelete = true;
  }

  if (isDefault) {
      showTemplateDelete = false;
  }

  //VS CR-586 08/16/10 Begin
  DocumentHandler queryDocSet = null; //this doc will be use to get data from a query

  QueryListView queryListViewTemplate = null;

  try {
      queryListViewTemplate = (QueryListView) EJBObjectFactory.createClientEJB(
				formMgr.getServerLocation(), "QueryListView");

      // Get a list of the users for the client bank
      String sqlSet = "select PAYMENT_TEMPLATE_GROUP_OID, NAME from PAYMENT_TEMPLATE_GROUP where P_OWNER_ORG_OID in (?,? )";
      Debug.debug(sqlSet);
      
      queryListViewTemplate.setSQL(sqlSet, new Object[]{userSession.getUserOid(), userSession.getOwnerOrgOid() });
      queryListViewTemplate.getRecords();
      queryDocSet = queryListViewTemplate.getXmlResultSet();
      Debug.debug("queryDocSet -- > " + queryDocSet.toString());

    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      try {
          if (queryListViewTemplate != null) {
              queryListViewTemplate.remove();
          }
      } catch (Exception e) {
          System.out.println("error removing queryListViewTemplate in NewTemplate.jsp");
      }
    }  // try/catch/finally block
    
    
  if(isExpressTemplate && !isReadOnly)
    {
 %><span class="isExpressText">
   <%=resMgr.getText("TemplateDetail.ExpressTemplate", TradePortalConstants.TEXT_BUNDLE)%> </span>
   <br/> 
   <div class="searchDivider"></div>
 <% 
    }
  //Naveen IR-T36000011495(ANZ- 784)- Start  
  if(instrumentType.equals(InstrumentType.DOM_PMT) && fixedFlag.equals(TradePortalConstants.INDICATOR_YES))
    {
 %><span class="isFixedTemplateText">
   <%=resMgr.getText("TemplateDetail.FixedTemplate", TradePortalConstants.TEXT_BUNDLE)%> </span>
   <br/> 
   <div class="searchDivider"></div>
 <% 
    }
  //Naveen IR-T36000011495(ANZ- 784)- End
   		if(isExpressTemplate)
    	{
 		%>
        <%= widgetFactory.createTextField( "TemplateName", "TemplateDetail.ExpressName", template.getAttribute("name"), "25", isReadOnly || isDefault, true, false, "class='char31'", "", "inline" ) %>                              
       <% 
     	} else {
     	%>
     	<%= widgetFactory.createTextField( "TemplateName", "TemplateDetail.Name", template.getAttribute("name"), "25", isReadOnly || isDefault, true, false, "class='char31'", "", "inline" ) %>	
     	<% 
     	}
      
      
  	  //VS CR-586 08/16/10 Begin
      String clientBankOid = userSession.getClientBankOid();
      ClientBankWebBean clientBank = beanMgr.createBean(ClientBankWebBean.class,"ClientBank");
      clientBank.getById(clientBankOid);      

      if(TradePortalConstants.INDICATOR_YES.equals(clientBank.getAttribute("template_groups_ind"))){
      %>
       
      	<%
       		/********************************
             * START Payment Template Group  DROPDOWN
             ********************************/
            //if (!ReadOnly) 
            Debug.debug("Building Payment Template field");
            
            String optionsSet = Dropdown.createSortedOptions(queryDocSet,"PAYMENT_TEMPLATE_GROUP_OID", 
            						"NAME",template.getAttribute("payment_templ_grp_oid"), resMgr.getResourceLocale());
            
            String grpName = resMgr.getText("NewInstTemplate1.selectPayTempGrp", TradePortalConstants.TEXT_BUNDLE);
         %>
      <%=widgetFactory.createSelectField( "PaymentTemplGrp", "NewInstTemplate1.TemplateGroup", grpName, optionsSet, isReadOnly, false, false, "class='char31'", "", "formItemWithIndent5 inline") %>
	  <%
	  /********************************
       * END Payment Template Group DROPDOWN
       ********************************/
	  	}
	  //VS CR-586 08/16/10 End
      %>
      <div style="clear:both;"></div>
      <%-- //IAZ CR-586 08/18/10 Begin --%>
     <%-- Jan 07 2013 @Komal #Moved the code from TemplateHeader.frag to this Transaction-FTDP-ISS-CreditDebitDetails.frag; as per UI modifications--%>
      <%-- <%        
      if ((InstrumentType.DOM_PMT.equals(instrument.getAttribute("instrument_type_code")))||
          (InstrumentType.XFER_BET_ACCTS.equals(instrument.getAttribute("instrument_type_code")))||
          (InstrumentType.FUNDS_XFER.equals(instrument.getAttribute("instrument_type_code"))))
      {
         UserWebBean thisUser = beanMgr.createBean(UserWebBean.class, "User");
         thisUser.setAttribute("user_oid", userSession.getUserOid());
         thisUser.getDataFromAppServer();
         String confInd = "";
         if (TradePortalConstants.ADMIN.equals(userSession.getSecurityType()))
         	confInd = TradePortalConstants.INDICATOR_YES;
         else if (userSession.getSavedUserSession() == null)
            confInd = thisUser.getAttribute("confidential_indicator");
         else   
         {
            if (TradePortalConstants.ADMIN.equals(userSession.getSavedUserSession().getSecurityType()))
                confInd = TradePortalConstants.INDICATOR_YES;
            else	
                confInd = thisUser.getAttribute("subsid_confidential_indicator");  
         }       
         if (TradePortalConstants.INDICATOR_YES.equals(confInd))
         {
            //IAZ CR-586 IR-PRUK092452162 09/29/10: CONF IND for TEMPLATES is T (vs Y).
            //           This fix is for templates created from Instruments (so Y is copied over and needs to be replaced by N in Terms)
            if (TradePortalConstants.INDICATOR_YES.equals(terms.getAttribute("confidential_indicator")))
               terms.setAttribute("confidential_indicator", TradePortalConstants.INDICATOR_NO);
      %>  
        <%=widgetFactory.createCheckboxField("ConfidentialIndicatorBox", "TemplateDetail.ConfidentialPmtLabel",
        		terms.getAttribute("confidential_indicator").equals(TradePortalConstants.CONF_IND_FRM_TMPLT),
				isReadOnly,false,"onClick=setConfidentialIndicator()","","")%>
            
            <input type=hidden name = ConfidentialIndicator value=<%=terms.getAttribute("confidential_indicator")%>>
            <div id="Confidential Payment"><input type=hidden name = ConfidentialIndicatorTemplate value=<%=terms.getAttribute("confidential_indicator")%>>
            
            </div>
      <%     
         }
      }
      %> --%>
      <%-- //IAZ CR-586 08/18/10 End --%> 
  
<%-- //IAZ CR-586 08/18/10 Begin --%>
<script LANGUAGE="JavaScript">
function setConfidentialIndicator()
{
	if (document.forms[0].ConfidentialIndicatorBox.checked)
	{
		document.forms[0].ConfidentialIndicator.value = 'T';
		document.forms[0].ConfidentialIndicatorTemplate.value = 'T';
	}
	else
	{
		document.forms[0].ConfidentialIndicator.value = 'N';
		document.forms[0].ConfidentialIndicatorTemplate.value = 'N';
	}
}
</script>
<%-- //IAZ CR-586 08/18/10 End --%>
