<%--this fragment is included in both the 
    Transaction-IMP_DLC-ISS-TransportDocsShipment fragment for existing
    rows as well as in the Transaction-IMP_DLC-ISS-ShipmentTermsRows.jsp
    for adding new rows via ajax.

  Passed variables:
    shipmentTermsIndex - an integer value, zero based
    isReadOnly
    isFromExpress
--%>

<%--TODO: this should iterate through the # of existing shipment terms, or create the correct number of new rows depending on the situation--%>

  <tr>
    <td><%= widgetFactory.createCheckboxField( "check"+shipmentTermsIndex, " ", false, isReadOnly || isFromExpress, isFromExpress, "", "", "") %></td>
    <%-- index display is one based --%>
    <td><%=shipmentTermsIndex+1%></td>
    <td><%= widgetFactory.createTextField( "description"+shipmentTermsIndex, "", shipmentTerms.getAttribute("description"), "60", isReadOnly, false, false,  "", "", "none" ) %> </td>
  </tr>
