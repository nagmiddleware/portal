<%--
 *
 *     Copyright  � 2006                        
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
*******************************************************************************
                 Loan Request Issue Page - Internal Instructions section

  Description:
    Contains HTML to create the Loan Request Issue Internal Instructions section.  

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transaction-LRQ-ISS-InternalInstructions.jsp" %>
*******************************************************************************
--%>
<span class="formItem">
<%=widgetFactory.createNote("RequestAdviseIssue.IntInstrText")%></span>
<%=widgetFactory.createTextArea("InternalInstructions", "",
					terms.getAttribute("internal_instructions"),
					isReadOnly, false, false, "rows='10' cols='250'", "", "")%>