<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
/********************************************************************************
 *                    Guarantee Issue Page - General section
 *
 * Description:
 *    Contains HTML to create the Guarantee Issue General section.
 *
 * Note:
 *    This is not a standalone JSP.  It MUST be included using the following tag:
 *        <%@ include file="Transaction-GUAR-ISS-General.jsp" %>
 ********************************************************************************/
--%>
      <%
          String amount = terms.getAttribute("amount");
          String curcy = terms.getAttribute("amount_currency_code");
          String displayAmount;         
          dateWidgetOptions = "";
         // Don't format the amount if we are reading data from the doc
		if (getDataFromDoc)
			displayAmount = amount;
		else
			displayAmount = TPCurrencyUtility.getDisplayAmount(amount, curcy, loginLocale);
          /*    if(!isReadOnly)
           	     displayAmount = amount;
           	  else
           		  displayAmount = TPCurrencyUtility.getDisplayAmount(amount.toString(), curcy, loginLocale);
			*/	  
      %>
      <% if (TradePortalConstants.INDICATOR_YES.equals(transaction.getAttribute("converted_transaction_ind"))){ %>
      		 	<div class="formItem">  
                  <%= widgetFactory.createCheckboxField("ConvTransInd", "GuaranteeIssue.ConvTransInd",  TradePortalConstants.INDICATOR_YES.equals(transaction.getAttribute("converted_transaction_ind")), true,false,"","","none") %><br/>
                </div>
	<%}%>
      <div class="columnLeft">     
      		 
            <%--
               -==========================================
               -============== Beneficiary ===============
               -==========================================
            --%> 
           
                  <% 
                  String itemid ="beneficiary_name,beneficiary_address_line_1,beneficiary_address_line_2,beneficiary_address_city,beneficiary_address_state_province,beneficiary_address_postal_code,beneficiary_address_country,beneficiary_phone_number,beneficiary_contact_name,beneficiary_OTL_customer_id,BeneficiaryUserDefinedField1,BeneficiaryUserDefinedField2,BeneficiaryUserDefinedField3,BeneficiaryUserDefinedField4";
                  String section="ben_detaild";
                  %>  
            <%
                 
                  secureParms.put("first_terms_party_type", TradePortalConstants.BENEFICIARY);
                  secureParms.put("first_terms_party_oid", beneficiary.getAttribute("terms_party_oid"));
                  String benSearchHtml = widgetFactory.createPartySearchButton(itemid,section,false,TradePortalConstants.BENEFICIARY,false);
            %>
              <%= widgetFactory.createSubsectionHeader("GuaranteeIssue.Beneficiary",isReadOnly, false, false, benSearchHtml) %>
        <div style="display:none">
        	<input type=hidden name="beneficiary_OTL_customer_id" value="<%=beneficiary.getAttribute("OTL_customer_id")%>">
        	<input type=hidden name="VendorId" value="<%=beneficiary.getAttribute("vendor_id")%>">
        	 <input type=hidden name="BeneficiaryUserDefinedField1"
		     value="<%=beneficiary.getAttribute("user_defined_field_1")%>">
		<input type=hidden name="BeneficiaryUserDefinedField2"
		     value="<%=beneficiary.getAttribute("user_defined_field_2")%>">
		<input type=hidden name="BeneficiaryUserDefinedField3"
		     value="<%=beneficiary.getAttribute("user_defined_field_3")%>">
		<input type=hidden name="BeneficiaryUserDefinedField4"
		     value="<%=beneficiary.getAttribute("user_defined_field_4")%>">
		</div>
            <%= widgetFactory.createTextField("beneficiary_name", "GuaranteeIssue.BeneName", beneficiary.getAttribute("name").toString(), "35", 
                        isReadOnly, !isTemplate, isExpressTemplate, "onBlur='checkBeneficiaryName(\"" + StringFunction.escapeQuotesforJS(beneficiary.getAttribute("name")) + "\")'", "","" ) %>
          <%= widgetFactory.createTextField("beneficiary_address_line_1", "GuaranteeIssue.AddressLine1", beneficiary.getAttribute("address_line_1"), "35", 
                  isReadOnly, !isTemplate, isExpressTemplate, "", "","") %>
          <%= widgetFactory.createTextField("beneficiary_address_line_2", "GuaranteeIssue.AddressLine2", beneficiary.getAttribute("address_line_2"), "35",isReadOnly,false, false, "", "","") %>
          <%= widgetFactory.createTextField("beneficiary_address_city", "GuaranteeIssue.City", beneficiary.getAttribute("address_city"), "23", 
                  isReadOnly, !isTemplate, isExpressTemplate, "class='char35'", "","") %>
          
          <div>         
                  <%= widgetFactory.createTextField("beneficiary_address_state_province", "GuaranteeIssue.ProvinceState", beneficiary.getAttribute("address_state_province"), "8", 
                        isReadOnly, false, false, "class='char10'", "","inline") %>
                  <%= widgetFactory.createTextField("beneficiary_address_postal_code", "GuaranteeIssue.PostalCode", beneficiary.getAttribute("address_postal_code"), "8",isReadOnly, false, false, "class='char10'", "", "inline") %>
                  <div style="clear:both;"></div>
            </div>
                                                                 
            <%
                  countries = Dropdown.createSortedRefDataOptions(TradePortalConstants.COUNTRY,beneficiary.getAttribute("address_country"),loginLocale);
        %>
            <%= widgetFactory.createSelectField("beneficiary_address_country", "GuaranteeIssue.Country", " ", countries, 
                  isReadOnly, !isTemplate, isExpressTemplate, "class='char35'", "", "") %>
                  
                  <%-- START Vishal Sarkary IR-20362 --%>
                  
                  <% if(transaction.getAttribute("standby_using_guarantee_form").equals(TradePortalConstants.INDICATOR_NO)){ %> 
              		 <%= widgetFactory.createTextField("beneficiary_contact_name", "GuaranteeIssue.ContactName", 
                 		 beneficiary.getAttribute("contact_name").toString(), "35",isReadOnly,false, isExpressTemplate, "", "","") %>
				
				  <%  } %> 
                 
           
                  <%-- END Vishal Sarkary IR-20362 --%>
            
                                    
                              
           
			<%= widgetFactory.createTextField("beneficiary_phone_number", "GuaranteeIssue.PhoneNumber", beneficiary.getAttribute("phone_number"), "20", isReadOnly,false,false,"class='char25'","regExp:'[0-9]+'","") %> 
            <%--
               -==========================================
               -================ Applicant ===============
               -==========================================
            --%>         
            <%
                  String applicantSearchHtml = "";
                  String applicantClearHtml = "";
                  String addrSearchHtml = "";
                  String addressSearchIndicator = "";
                  String itemid1="applicant_terms_party_oid,applicant_name,applicant_address_line_1,applicant_address_line_2,applicant_address_city,applicant_address_country,applicant_address_state_province,applicant_address_postal_code,applicant_phone_number,applicant,applicant_OTL_customer_id,ApplicantUserDefinedField1,ApplicantUserDefinedField2,ApplicantUserDefinedField3,ApplicantUserDefinedField4";
                  String section1="applicant_guar";
            addressSearchIndicator = applicant.getAttribute("address_search_indicator");
                  
            if (!(isReadOnly)) {
                        if(!(isReadOnly || isFromExpress))
                        {
                              applicantSearchHtml = widgetFactory.createPartySearchButton(itemid1,section1,false,TradePortalConstants.APPLICANT,false);
                        }
            }
            if (!isReadOnly && isTemplate) {
                        applicantClearHtml = widgetFactory.createPartyClearButton("ClearApplicantButton", "clearApplicant()",isReadOnly,"");
                  }
          %>
            <%= widgetFactory.createSubsectionHeader("GuaranteeIssue.Applicant",isReadOnly, !isTemplate, isExpressTemplate, (applicantSearchHtml+applicantClearHtml)) %>
            <%
                  if (!(isReadOnly || isFromExpress) && !addressSearchIndicator.equals("N"))
                {
                  if(!isReadOnly && corpOrgHasMultipleAddresses)
                  { %>
                  <div class = "formItem">
                     <button data-dojo-type="dijit.form.Button"  name="AddressSearch" id="AddressSearch" type="button">
                     <%=resMgr.getText("common.searchAddressButton",TradePortalConstants.TEXT_BUNDLE)%>
                     <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
                            var corpOrgOid = <%=corpOrgOid%>;
								   require(["t360/common", "t360/partySearch"], function(common, partySearch){
										  partySearch.setPartySearchFields('<%=TradePortalConstants.APPLICANT%>');
                                          common.openCorpCustAddressSearchDialog(corpOrgOid);
                                    });
                   </script>
                   </button>
                   </div>  
                  <% 
                  }
                }
                  %>
                              
              <div style="display:none">
              <input type=hidden name="second_terms_party_type" value="<%=TradePortalConstants.APPLICANT%>">
              <input type=hidden name="applicant_OTL_customer_id" value="<%=applicant.getAttribute("OTL_customer_id")%>">
              <input type=hidden name="applicant_terms_party_oid" value="<%=applicant.getAttribute("terms_party_oid")%>">
              <input type=hidden name="applicant_name" value="<%=applicant.getAttribute("name")%>">
              <input type=hidden name="applicant_address_line_1" value="<%=applicant.getAttribute("address_line_1")%>">
              <input type=hidden name="applicant_address_line_2" value="<%=applicant.getAttribute("address_line_2")%>">
              <input type=hidden name="applicant_address_city" value="<%=applicant.getAttribute("address_city")%>">
              <input type=hidden name="applicant_address_country" value="<%=applicant.getAttribute("address_country")%>">
              <input type=hidden name="applicant_address_state_province" value="<%=applicant.getAttribute("address_state_province")%>">
              <input type=hidden name="applicant_address_postal_code" value="<%=applicant.getAttribute("address_postal_code")%>">
              <input type=hidden name="applicant_phone_number" value="<%=applicant.getAttribute("phone_number")%>">
              <input type=hidden name="applicant_address_seq_num" value="<%=applicant.getAttribute("address_seq_num")%>">
              <input type=hidden name="applicant_address_search_indicator" value="<%=applicant.getAttribute("address_search_indicator")%>">     
              
               <input type=hidden name="ApplicantUserDefinedField1"
		     value="<%=applicant.getAttribute("user_defined_field_1")%>">
		<input type=hidden name="ApplicantUserDefinedField2"
		     value="<%=applicant.getAttribute("user_defined_field_2")%>">
		<input type=hidden name="ApplicantUserDefinedField3"
		     value="<%=applicant.getAttribute("user_defined_field_3")%>">
		<input type=hidden name="ApplicantUserDefinedField4"
		     value="<%=applicant.getAttribute("user_defined_field_4")%>">
              </div>
            
			 <%-- Srinivasu_D  T36000019515 Rel8.4 01/23/2014 - start - changing the textarea to autoresize --%>
			 <%--
            <%= widgetFactory.createTextArea("applicant", "", applicant.buildAddress(false, resMgr), true,false, false, "onFocus='this.blur();' rows='4' cols='45'", "","" ) %>
			--%>

			 <%= widgetFactory.createAutoResizeTextArea("applicant", "", applicant.buildAddress(false, resMgr), true, false,false, "class='autoHeightTxtAreaQtrWidthNoSidebar' style='overflow-x: hidden;' rows='4' cols='45'", "", "")%>	

			<%-- Srinivasu_D  T36000019515 Rel8.4 01/23/2014 - End --%>

            <%= widgetFactory.createTextField("reference_number", "GuaranteeIssue.YourRefNo", terms.getAttribute("reference_number"), "30", isReadOnly, false, false, "", "", "") %>
      
            <%--
               -==========================================
               -========= Detailed Information ==========
               -==========================================
            --%>   
            <%= widgetFactory.createSubsectionHeader("GuaranteeIssue.DetailedInformation") %>
          
            <%
                  guarTypes = Dropdown.createSortedRefDataOptions("CHARACTERISTIC_TYPE",terms.getAttribute("characteristic_type"),loginLocale);
            
            if(transaction.getAttribute("standby_using_guarantee_form").equals(TradePortalConstants.INDICATOR_NO)){   %>    
			 <%= widgetFactory.createSelectField("characteristic_type", "GuaranteeIssue.PurposeType", " ", guarTypes, isReadOnly) %>	 				 
		 <% }else{ %> 
				 <%= widgetFactory.createSelectField("characteristic_type", "GuaranteeIssue.PurposeTypeLC", " ", guarTypes, isReadOnly) %>
		<%  } %> 
              
                             
            <%
                  currencies = Dropdown.createSortedCurrencyCodeOptions(terms.getAttribute("amount_currency_code"),loginLocale);
            %>
            <div>
				<%=widgetFactory.createSelectField("amount_currency_code","GuaranteeIssue.Currency", " ", currencies, isReadOnly, !isTemplate,false, "style=\"width: 50px;\"", "", "inline")%>
				<%= widgetFactory.createAmountField( "amount", "GuaranteeIssue.Amount", displayAmount, curcy, isReadOnly, !isTemplate, isExpressTemplate,"style=\"width: 136px;\" ", "readOnly:" +isReadOnly, "inline") %>
				<div style="clear: both;"></div>
			</div>
            
            <div class="formItem">        
            
            <%=resMgr.getText("common.ExpiryPlace",TradePortalConstants.TEXT_BUNDLE)%>
            <%=resMgr.getText("GuaranteeIssue.ExpiryPlaceOther",TradePortalConstants.TEXT_BUNDLE)%>
            <%if(!isReadOnly){ %>
            	<a href='javascript:openOtherConditionsDialog("special_bank_instructions","SaveTrans", "<%=resMgr.getText("GuaranteeIssue.Other2",TradePortalConstants.TEXT_BUNDLE)%>");'>
                        <%--= widgetFactory.createSubLabel("common.ExpiryPlaceInstructionsLink") --%>
                        <%=resMgr.getText("GuaranteeIssue.Other2",TradePortalConstants.TEXT_BUNDLE)%>
            	</a> 
             <%}else{ %>			
						 	<%=resMgr.getText("GuaranteeIssue.Other2",TradePortalConstants.TEXT_BUNDLE)%>
			<%} %>             
            <%=resMgr.getText("common.ExpiryPlaceOtherEnd",TradePortalConstants.TEXT_BUNDLE)%>
            
            </div>
            <%--Added by dillip for defect T36000004562 --%>
            <%
                //  options = Dropdown.createSortedRefDataOptions(TradePortalConstants.PLACE_OF_EXPIRY_TYPE, "", loginLocale);
            		options = Dropdown.createSortedRefDataOptions(
      					  TradePortalConstants.PLACE_OF_EXPIRY_TYPE, 
      					  terms.getAttribute("place_of_expiry"), 
      					  loginLocale);
            %>
            <%--Ended Here  --%>
            <%-- IR3818 dt 30-07-2012 by dillip--%>
         <%-- <span class="title-left">
         <span>
                  <%=widgetFactory.createLabel("ExpirePlace","common.ExpiryPlace",false,false,false,"") %>
            </span>           
            </span>
            IR3818 Ended Here --%>
      
            <%= widgetFactory.createSelectField("PlaceOfExpiry", "", " ", options, isReadOnly || isFromExpress, false, false, "", "", "") %>
            
            <%--
               -==========================================
               -============== Issuing Instructions ======
               -==========================================
            --%>   
            <%= widgetFactory.createSubsectionHeader("GuaranteeIssue.IssuingBank") %>
            <%
                  String issueType = terms.getAttribute("guarantee_issue_type");
        %>
            <%= widgetFactory.createLabel("", "GuaranteeIssue.GteeIssue", isReadOnly, !isTemplate, isExpressTemplate, "") %>
            
            <div class="formItem">
                  <%= widgetFactory.createRadioButtonField("guarantee_issue_type", "guarIssueApplicantsBank", 
                        "GuaranteeIssue.ApplicantsBank", TradePortalConstants.GUAR_ISSUE_APPLICANTS_BANK, issueType.equals(TradePortalConstants.GUAR_ISSUE_APPLICANTS_BANK), isReadOnly, "onclick=\"setOverSeasValidDate();\"", "") %>
                  <br/>             
                  <%= widgetFactory.createRadioButtonField("guarantee_issue_type", "guarIssueOverseasBank", 
                        "GuaranteeIssue.OverseasBank1", TradePortalConstants.GUAR_ISSUE_OVERSEAS_BANK, issueType.equals(TradePortalConstants.GUAR_ISSUE_OVERSEAS_BANK), isReadOnly, "", "") %>
                 <div style="clear:both;"></div>             
                  <div class="formItemWithIndent4" ><%= widgetFactory.createNote("GuaranteeIssue.OverseasBank2") %></div>                 
                  <div class="formItemWithIndent3">
                        <%= widgetFactory.createDateField("overseas_validity_date", "GuaranteeIssue.ValidityDateOverseasBank", StringFunction.xssHtmlToChars(terms.getAttribute("overseas_validity_date")),isReadOnly,false, false, "class='char6' onclick=\"setGuarIssueOverseasBank();\"",dateWidgetOptions,"" ) %>
            	</div>
            </div>
            <%--========= Issuing Instructions ==========--%>
            <%--Jyoti 30-07-2012 IR- 2828--%>
            <%!String searchButton ="",clearButton="";%>
            <%!String itemid2="bank_name,bank_address_line_1,bank_address_line_2,bank_address_city,bank_address_state_province,bank_address_postal_code,bank_address_country,bank_phone_number,bank_OTL_customer_id,BankUserDefinedField1,BankUserDefinedField2,BankUserDefinedField3,BankUserDefinedField4";%>
            <%String section2="Instructions_guar";%>
            <%
            if (!(isReadOnly)){
                  if(!(isReadOnly || isFromExpress))
          %>
            <%searchButton = widgetFactory.createPartySearchButton(itemid2,section2,isReadOnly,TradePortalConstants.OVERSEAS_BANK,false); %>
            <%clearButton = widgetFactory.createPartyClearButton("ClearOSBButton", "clearOverseasBankFields()",isReadOnly,""); %>
            <div style="display:none">
                  <input type=hidden name="bank_OTL_customer_id" value="<%=bank.getAttribute("OTL_customer_id")%>">
                   <input type=hidden name="BankUserDefinedField1"
		     value="<%=bank.getAttribute("user_defined_field_1")%>">
		<input type=hidden name="BankUserDefinedField2"
		     value="<%=bank.getAttribute("user_defined_field_2")%>">
		<input type=hidden name="BankUserDefinedField3"
		     value="<%=bank.getAttribute("user_defined_field_3")%>">
		<input type=hidden name="BankUserDefinedField4"
		     value="<%=bank.getAttribute("user_defined_field_4")%>">
            </div>
          <%
            } //end if
            secureParms.put("third_terms_party_type", TradePortalConstants.OVERSEAS_BANK);
            secureParms.put("bank_terms_party_oid", bank.getAttribute("terms_party_oid"));
          %>
            <%= widgetFactory.createTextField("bank_name", "GuaranteeIssue.OseasBankName", bank.getAttribute("name").toString(), "35", 
                        isReadOnly, false, false, " class='char30' onBlur='checkBankName(\"" + StringFunction.escapeQuotesforJS(bank.getAttribute("name")) + "\")'", "","",searchButton+clearButton) %>  
          

          <%= widgetFactory.createTextField("bank_address_line_1", "GuaranteeIssue.OverSeasAddressLine1", bank.getAttribute("address_line_1"), "35", 
                  isReadOnly, !isTemplate, isExpressTemplate, "class='char30'", "","") %>         
          <%= widgetFactory.createTextField("bank_address_line_2", "GuaranteeIssue.OverSeasAddressLine2", bank.getAttribute("address_line_2"), "35", isReadOnly,false, false, "class='char30'", "","") %>                                    
          <%= widgetFactory.createTextField("bank_address_city", "GuaranteeIssue.OverSeasCity", bank.getAttribute("address_city"), "23", 
                  isReadOnly, !isTemplate, isExpressTemplate, "class='char30'", "","") %>                                   
            <div>
                  <%= widgetFactory.createTextField("bank_address_state_province", "GuaranteeIssue.OverSeasProvinceState", bank.getAttribute("address_state_province"), "8", 
                        isReadOnly, false, false, "", "","inline") %>
                  <%= widgetFactory.createTextField("bank_address_postal_code", "GuaranteeIssue.OverSeasPostalCode", bank.getAttribute("address_postal_code"), "8",isReadOnly, false, false, "", "", "inline") %>
                  <div style="clear: both;"></div>
            </div>
            <%
                   countries = Dropdown.createSortedRefDataOptions(TradePortalConstants.COUNTRY, bank.getAttribute("address_country"), loginLocale);
        %>
            <%= widgetFactory.createSelectField("bank_address_country", "GuaranteeIssue.OverSeasCountry", " ", countries, isReadOnly, false, isExpressTemplate, "class='char30'", "", "") %>                                 
            <%= widgetFactory.createTextField("bank_phone_number", "GuaranteeIssue.OverSeasPhoneNumber", bank.getAttribute("phone_number"), "20", isReadOnly,false,false,"class='char20'","regExp:'[0-9]+'","") %>                
      </div>
      <%-- ==================End of Left Column Div====================== --%>

      <div class="columnRight">
            <%--
               -==========================================
               -============== Validity ==================
               -==========================================
            --%>
            <%
              String validFromType = terms.getAttribute("guar_valid_from_date_type");
              String validToType = terms.getAttribute("guar_expiry_date_type");
            %>          
            <%--========= Validity From ==========--%>
            <%-- <%= widgetFactory.createWideSubsectionHeader("GuaranteeIssue.Validity") %> --%>
            	 <%--  KMehta IR T36000023753 on 6/1/2014 CR-910 start --%> 
            <%= widgetFactory.createSubsectionHeader("GuaranteeIssue.Validity",isReadOnly, false, isExpressTemplate, "") %>
            <div style="margin-left:8px;">
            <%= widgetFactory.createLabel("", "GuaranteeIssue.ValidFrom", isReadOnly, !isTemplate, false, "") %>
            </div>
            	 <%--KMehta IR T36000023753 on 6/1/2014 CR-910 End  --%>
            	<div class="formItem">  
                  <%= widgetFactory.createRadioButtonField("guar_valid_from_date_type", "issueDate", "GuaranteeIssue.DateofIssue",TradePortalConstants.DATE_OF_ISSUE, validFromType.equals(TradePortalConstants.DATE_OF_ISSUE), isReadOnly, "onClick=\"resetDateField('guar_valid_from_date')\"", "") %>
                </div>
                <div>
					<div class="formItem inline">
						 <%= widgetFactory.createRadioButtonField("guar_valid_from_date_type", "otherDate", "GuaranteeIssue.OtherDate",TradePortalConstants.OTHER_VALID_FROM_DATE, validFromType.equals(TradePortalConstants.OTHER_VALID_FROM_DATE), isReadOnly, "", "") %>
					</div>				
					<%=widgetFactory.createDateField("guar_valid_from_date", "",StringFunction.xssHtmlToChars(terms.getAttribute("guar_valid_from_date")),isReadOnly, false, false, "onChange=\"setOtherDate()\" style=\"width: 100px;\"",dateWidgetOptions, "inline")%>			
					<div style="clear: both;"></div>
				</div> 
                  
            <%--========= Validity To ==========--%>
            <div style="margin-left:8px;">
            <%= widgetFactory.createLabel("", "GuaranteeIssue.ValidTo", isReadOnly, false, false, "") %>
             </div>                
            <div>

			<div class="formItem inline">
				 <% /* KMehta 10/18/2013 CR-910 start 
				      Added asterisk for mandatory field Expiry Date when BankGroup should be processed with Expiry Date*/%>
					<%if(TradePortalConstants.INDICATOR_YES.equals(slc_gua_indicator)){ %>
						<span class="formItemWithIndent10 asterisk">*</span>
					<%} %>
				 <% /* KMehta 10/15/2013 Rel8400 CR-910 end*/ %>
					<%= widgetFactory.createRadioButtonField("guar_expiry_date_type", "validEndDate", "GuaranteeIssue.NewValidityDate",TradePortalConstants.VALIDITY_DATE, validToType.equals(TradePortalConstants.VALIDITY_DATE), isReadOnly, "", "") %>
				</div>				
				<%=widgetFactory.createDateField("expiry_date", "",StringFunction.xssHtmlToChars(terms.getAttribute("expiry_date")),isReadOnly, false, false, "onChange=\"setValidityDate()\" style=\"width: 100px;\"",dateWidgetOptions, "inline")%>			
				<div style="clear: both;"></div>
			</div>    
                
            <div class="formItem">
                  <%= widgetFactory.createRadioButtonField("guar_expiry_date_type", "otherExpiryDate", "GuaranteeIssue.Other1", TradePortalConstants.OTHER_EXPIRY_DATE, validToType.equals(TradePortalConstants.OTHER_EXPIRY_DATE), isReadOnly, "onChange=\"resetDateField('expiry_date')\"", "") %>
                 <%if(!isReadOnly){ %>
                        <a href='javascript:openOtherConditionsDialog("special_bank_instructions","SaveTrans", "<%=resMgr.getText("GuaranteeIssue.Other2",TradePortalConstants.TEXT_BUNDLE)%>");' onclick="javascript:setOtherExpiryDate()">
                              <%--= widgetFactory.createSubLabel("GuaranteeIssue.Other2") --%>
                              <%=resMgr.getText("GuaranteeIssue.Other2",TradePortalConstants.TEXT_BUNDLE)%>
                        </a>
				 <%}else{ %>			
						 <%=resMgr.getText("GuaranteeIssue.Other2",TradePortalConstants.TEXT_BUNDLE)%>
				 <% } %>     
				     <%=resMgr.getText("common.ExpiryPlaceOtherEnd",TradePortalConstants.TEXT_BUNDLE)%>               
            </div>
            
            <%--
               -==========================================
               -========= Delivery Instructions ==========
               -==========================================
            --%>
            <%--========= Delivery To ==========--%>
          <%
             String deliverTo = terms.getAttribute("guar_deliver_to");
             String deliverBy = terms.getAttribute("guar_deliver_by");
          %>

                  <%-- <%= widgetFactory.createWideSubsectionHeader("GuaranteeIssue.DeliveryInstructions") %> --%>
                  <%= widgetFactory.createSubsectionHeader("GuaranteeIssue.DeliveryInstructions",isReadOnly, !isTemplate, isExpressTemplate, "") %>
                  <div style="margin-left:8px;">
                  <%= widgetFactory.createLabel("", "GuaranteeIssue.DeliverTo", isReadOnly, false, false, "") %>
                  </div>
                  <div class="formItem">
                        <%= widgetFactory.createRadioButtonField("guar_deliver_to", "TradePortalConstants.DELIVER_TO_APPLICANT", 
                              "GuaranteeIssue.Applicant", TradePortalConstants.DELIVER_TO_APPLICANT, deliverTo.equals(TradePortalConstants.DELIVER_TO_APPLICANT), isReadOnly) %>
                        <br/>
                        <%= widgetFactory.createRadioButtonField("guar_deliver_to", "TradePortalConstants.DELIVER_TO_BENEFICIARY", 
                              "GuaranteeIssue.Beneficiary", TradePortalConstants.DELIVER_TO_BENEFICIARY, deliverTo.equals(TradePortalConstants.DELIVER_TO_BENEFICIARY), isReadOnly) %>
                        <br/>
                        <%= widgetFactory.createRadioButtonField("guar_deliver_to", "TradePortalConstants.DELIVER_TO_AGENT", 
                              "GuaranteeIssue.Agent1", TradePortalConstants.DELIVER_TO_AGENT, deliverTo.equals(TradePortalConstants.DELIVER_TO_AGENT), isReadOnly, "", "") %>
                              <%= widgetFactory.createSubLabel("GuaranteeIssue.Agent2") %>
                        <br/>
                        <%= widgetFactory.createRadioButtonField("guar_deliver_to", "otherInDelivery", 
                              "GuaranteeIssue.Other1", TradePortalConstants.DELIVER_TO_OTHER, deliverTo.equals(TradePortalConstants.DELIVER_TO_OTHER), isReadOnly, "", "") %>
                         <%if(!isReadOnly){ %>
                              <a href='javascript:openOtherConditionsDialog("special_bank_instructions","SaveTrans", "<%=resMgr.getText("GuaranteeIssue.Other2",TradePortalConstants.TEXT_BUNDLE)%>");' onclick="javascript:setOtherInDelivery()">
                               <%=resMgr.getText("GuaranteeIssue.Other2",TradePortalConstants.TEXT_BUNDLE)%>
                              </a>
                         <%}else{ %>			
						 	<%=resMgr.getText("GuaranteeIssue.Other2",TradePortalConstants.TEXT_BUNDLE)%>
				 		 <% } %>
				 		 <%=resMgr.getText("common.ExpiryPlaceOtherEnd",TradePortalConstants.TEXT_BUNDLE)%>
                        <br/><br/>
                  </div>
                  
                  <%-- ========= Delivery By ========== --%>
                  <div style="margin-left:8px;">
                  <%= widgetFactory.createLabel("", "GuaranteeIssue.DeliverBy", isReadOnly, false, false, "") %>
            	  </div>
                  <div class="formItem">
                        <%= widgetFactory.createRadioButtonField("guar_deliver_by", "TradePortalConstants.DELIVER_BY_TELEX", 
                              "GuaranteeIssue.TelexSwift", TradePortalConstants.DELIVER_BY_TELEX, deliverBy.equals(TradePortalConstants.DELIVER_BY_TELEX), isReadOnly) %>
                        <br/>
                        <%= widgetFactory.createRadioButtonField("guar_deliver_by", "TradePortalConstants.DELIVER_BY_REG_MAIL", 
                              "GuaranteeIssue.RegMail", TradePortalConstants.DELIVER_BY_REG_MAIL, deliverBy.equals(TradePortalConstants.DELIVER_BY_REG_MAIL), isReadOnly) %>
                        <br/>
                        <%= widgetFactory.createRadioButtonField("guar_deliver_by", "TradePortalConstants.DELIVER_BY_MAIL", 
                              "GuaranteeIssue.Mail", TradePortalConstants.DELIVER_BY_MAIL, deliverBy.equals(TradePortalConstants.DELIVER_BY_MAIL), isReadOnly) %>
                        <br/>
                        <%= widgetFactory.createRadioButtonField("guar_deliver_by", "TradePortalConstants.DELIVER_BY_COURIER", 
                              "GuaranteeIssue.Courier", TradePortalConstants.DELIVER_BY_COURIER, deliverBy.equals(TradePortalConstants.DELIVER_BY_COURIER), isReadOnly) %>
                        <br/> 
                  </div>
            <%--
               -==========================================
               -============= Agent Details ==============
               -==========================================
            --%>

            <%    
                  String agentDtlSearchHtml = "";
                  String agentDtlClearHtml = "";
                  String itemid3="agent_name,agent_contact_name,agent_address_line_1,agent_address_line_2,agent_address_city,agent_address_state_province,agent_address_postal_code,agent_address_country,agent_phone_number,AgentUserDefinedField1,AgentUserDefinedField2,AgentUserDefinedField3,AgentUserDefinedField4";
                  String section3="agent_guar";
                  if (!(isReadOnly)){
                        if(!(isReadOnly || isFromExpress)){
                              agentDtlSearchHtml = widgetFactory.createPartySearchButton(itemid3,section3,false,TradePortalConstants.AGENT,false);
                        }
                        agentDtlClearHtml = widgetFactory.createPartyClearButton("ClearApplicantButton", "clearAgentFields()",isReadOnly,""); 
            %>          
            <div style="display:none">
                        <input type=hidden name="agent_OTL_customer_id" value="<%=agent.getAttribute("OTL_customer_id")%>">
                         <input type=hidden name="AgentUserDefinedField1"
		     value="<%=agent.getAttribute("user_defined_field_1")%>">
		<input type=hidden name="AgentUserDefinedField2"
		     value="<%=agent.getAttribute("user_defined_field_2")%>">
		<input type=hidden name="AgentUserDefinedField3"
		     value="<%=agent.getAttribute("user_defined_field_3")%>">
		<input type=hidden name="AgentUserDefinedField4"
		     value="<%=agent.getAttribute("user_defined_field_4")%>">
			</div>                        
          <%
              }//end if
              secureParms.put("fourth_terms_party_type", TradePortalConstants.AGENT);
              secureParms.put("agent_terms_party_oid", agent.getAttribute("terms_party_oid"));
          %>
             
            <%= widgetFactory.createSubsectionHeader("GuaranteeIssue.AgentDetails",isReadOnly, false, false, (agentDtlSearchHtml+agentDtlClearHtml)) %>

             <%--========= Agent Details ==========--%>      
            <%= widgetFactory.createTextField("agent_name", "GuaranteeIssue.AgentsName", StringFunction.escapeQuotesforJS(agent.getAttribute("name")), "35", 
                        isReadOnly, false, false, "class='char30' onBlur='checkAgentName(\"" + agent.getAttribute("name") + "\")'", "","" ) %>
            <%= widgetFactory.createTextField("agent_contact_name", "GuaranteeIssue.ContactName", agent.getAttribute("contact_name").toString(), "35",isReadOnly, false, false,"class='char30'", "", "") %>
          <%= widgetFactory.createTextField("agent_address_line_1", "GuaranteeIssue.AddressLine1", agent.getAttribute("address_line_1"), "35",isReadOnly, false, false,"class='char30'", "", "") %>
          <%= widgetFactory.createTextField("agent_address_line_2", "GuaranteeIssue.AddressLine2", agent.getAttribute("address_line_2"), "35",isReadOnly, false, false,"class='char30'", "", "") %>
          <%= widgetFactory.createTextField("agent_address_city", "GuaranteeIssue.City", agent.getAttribute("address_city"), "23", isReadOnly, false, false, "class='char30'", "", "") %>
            <div>
                  <%= widgetFactory.createTextField("agent_address_state_province", "GuaranteeIssue.ProvinceState", agent.getAttribute("address_state_province"), "8", 
                        isReadOnly, false, false, "", "","inline") %>
                  <%= widgetFactory.createTextField("agent_address_postal_code", "GuaranteeIssue.PostalCode", agent.getAttribute("address_postal_code"), "8",
                              isReadOnly, false, false, "", "", "inline") %>
                  <div style="clear: both;"></div>
            </div>
            <%
                   countries = Dropdown.createSortedRefDataOptions(TradePortalConstants.COUNTRY, agent.getAttribute("address_country"), loginLocale);
        %>
            <%= widgetFactory.createSelectField("agent_address_country", "GuaranteeIssue.Country", " ", countries, isReadOnly, false, false,"class='char30'", "", "") %>
            <%--<%= widgetFactory.createNumberField("agent_phone_number", "GuaranteeIssue.PhoneNumber", agent.getAttribute("phone_number"), "35", isReadOnly) %>--%>
            <%= widgetFactory.createTextField("agent_phone_number", "GuaranteeIssue.PhoneNumber", agent.getAttribute("phone_number"), "20", isReadOnly,false,false,"class='char25'","regExp:'[0-9]+'","") %>    
            <%= widgetFactory.createCheckboxField("guar_accept_instr", "GuaranteeIssue.AcceptInst", terms.getAttribute("guar_accept_instr").equals(TradePortalConstants.INDICATOR_YES),isReadOnly || isFromExpress) %>
            <%= widgetFactory.createCheckboxField("guar_advise_issuance", "GuaranteeIssue.AdviseIssuance", terms.getAttribute("guar_advise_issuance").equals(TradePortalConstants.INDICATOR_YES),isReadOnly || isFromExpress) %>

            <%--
               -==========================================
               -== Details of Tender / Order / Contract ==
               -==========================================
            --%>
            
            <%-- <%= widgetFactory.createWideSubsectionHeader("GuaranteeIssue.DetailsofTender") %> --%>
            <%= widgetFactory.createSubsectionHeader("GuaranteeIssue.DetailsofTender") %>
            <%
                  if (isReadOnly)
                out.println("&nbsp;");
            else 
            {
                options = ListBox.createOptionList(customerTextList, "PHRASE_OID", "NAME", "", userSession.getSecretKey());
        %>
            <%= widgetFactory.createSelectField("detailsPhraseDropDown", "", defaultText, options, isReadOnly, false, false, 
                  "onChange=" + PhraseUtility.getPhraseChange("/Terms/tender_order_contract_details", "tender_order_contract_details", "500","document.forms[0].tender_order_contract_details"), "", "") %>
        <%
            }
        %> 
        <%= widgetFactory.createTextArea("tender_order_contract_details", "", terms.getAttribute("tender_order_contract_details"), isReadOnly, false, false, "rows='5'", "","" ) %>  
            <br/>
            <% if (!isReadOnly){ %> 
	            <%=widgetFactory.createHoverHelp("Instructions_guar", "PartySearchIconHoverHelp") %>
	            <%=widgetFactory.createHoverHelp("ben_detaild", "PartySearchIconHoverHelp") %>
	            <%=widgetFactory.createHoverHelp("applicant_guar", "PartySearchIconHoverHelp") %>
	            <%=widgetFactory.createHoverHelp("agent_guar", "PartySearchIconHoverHelp") %>
	            <%=widgetFactory.createHoverHelp("ClearOSBButton", "PartyClearIconHoverHelp") %>	         
		        <%= widgetFactory.createHoverHelp("tender_order_contract_details","InstrumentIssue.PlaceHolderTendorOrderContractDetails")%>
				<%= widgetFactory.createHoverHelp("ClearApplicantButton","PartyClearIconHoverHelp")%>
		     <%} %> 
      </div>
<%-- ==================End of Right Column Div====================== --%>

<div style="clear: both"></div>
      
      <%
          Debug.debug("beneficiary oid: " + beneficiary.getAttribute("terms_party_oid"));
          Debug.debug("applicant oid: " + applicant.getAttribute("terms_party_oid"));
          Debug.debug("bank oid: " + bank.getAttribute("terms_party_oid"));
          Debug.debug("agent oid: " + agent.getAttribute("terms_party_oid"));
      
            /******************************************************************************
             *                      JavaScript Functions
             *****************************************************************************/
       %>
  <div style="clear:both;"></div>
      <script LANGUAGE="JavaScript">
      <%-- This function will reset the OTL Customer ID (to "") for the --%>
      <%-- Beneficiary Terms party if the user changes the name in the text field.      --%>
          function checkBeneficiaryName(originalName) 
          {
    	  	if(dijit.byId('beneficiary_name')){
    	  		if(dijit.byId('beneficiary_name').value != originalName){
    	  			if(null != document.getElementById('beneficiary_OTL_customer_id'))
    	  				document.getElementById('beneficiary_OTL_customer_id').value = "";
    	  		}
    	  	}
              <%-- if( document.TransactionGUAR.beneficiary_name.value != originalName )
              {
                    document.TransactionGUAR.beneficiary_OTL_customer_id.value = "";
              } --%>
          }
        
      <%-- This function will reset the OTL Customer ID (to "") for the --%>
      <%-- Overseas Bank Terms party if the user changes the name in the text field.    --%>
          function checkBankName(originalName) 
          {
        	  if(dijit.byId('bank_name')){
      	  		if(dijit.byId('bank_name').value != originalName){
      	  			if(null != document.getElementById('bank_OTL_customer_id'))
      	  				document.getElementById('bank_OTL_customer_id').value = "";
      	  		}
      	  	}
             
        	 <%--  if( document.TransactionGUAR.bank_name.value != originalName )
              {
                    document.TransactionGUAR.bank_OTL_customer_id.value = "";
              } --%>
          }
      
      <%-- This function will reset the OTL Customer ID (to "") for the --%>
      <%-- Overseas Bank Terms party if the user changes the name in the text field.    --%>
          function checkAgentName(originalName) 
          {
        	  if(dijit.byId('agent_name')){
        	  		if(dijit.byId('agent_name').value != originalName){
        	  			if(null != document.getElementById('agent_OTL_customer_id'))
        	  				document.getElementById('agent_OTL_customer_id').value = "";
        	  		}
        	  	}
        	  
              <%-- if( document.TransactionGUAR.agent_name.value != originalName )
              {
                    document.TransactionGUAR.agent_OTL_customer_id.value = "";
              } --%>
          }
      
      <%-- This function will clear out all the input fields for the Overseas Bank Terms Party       --%>
          function clearOverseasBankFields()
          {
        	  var bankCountry = dijit.byId("bank_address_country");
              document.TransactionGUAR.bank_name.value = "";
              document.TransactionGUAR.bank_address_line_1.value = "";
              document.TransactionGUAR.bank_address_line_2.value = "";
              document.TransactionGUAR.bank_address_city.value = "";
              document.TransactionGUAR.bank_address_state_province.value = "";
              document.TransactionGUAR.bank_address_country.value = "";
              bankCountry.set('value',"");
              document.TransactionGUAR.bank_address_postal_code.value = "";
              document.TransactionGUAR.bank_phone_number.value = "";
              document.TransactionGUAR.bank_OTL_customer_id.value = "";
              document.TransactionGUAR.BankUserDefinedField1.value = "";
              document.TransactionGUAR.BankUserDefinedField2.value = "";
              document.TransactionGUAR.BankUserDefinedField3.value = "";
              document.TransactionGUAR.BankUserDefinedField4.value = "";

          }    
      
      <%-- This function will clear out all the input fields for the Overseas Bank Terms Party       --%>
          function clearAgentFields()
          {
                var country = dijit.byId("agent_address_country");
            var phoneNo = dijit.byId("agent_phone_number");
            document.TransactionGUAR.agent_name.value = "";
              document.TransactionGUAR.agent_address_line_1.value = "";
              document.TransactionGUAR.agent_address_line_2.value = "";
              document.TransactionGUAR.agent_address_city.value = "";
              document.TransactionGUAR.agent_address_state_province.value = "";
              document.TransactionGUAR.agent_address_country.value = "";              
              country.set('value',"");
              phoneNo.set('value',"");
              document.TransactionGUAR.agent_address_postal_code.value = "";
              document.TransactionGUAR.agent_phone_number.value = "";
              document.TransactionGUAR.agent_contact_name.value = "";
              document.TransactionGUAR.agent_OTL_customer_id.value = "";
              document.TransactionGUAR.AgentUserDefinedField1.value = "";
              document.TransactionGUAR.AgentUserDefinedField2.value = "";
              document.TransactionGUAR.AgentUserDefinedField3.value = "";
              document.TransactionGUAR.AgentUserDefinedField4.value = "";
          }
      
        function clearApplicant() 
        {
          document.TransactionGUAR.applicant.value = "";
          document.TransactionGUAR.applicant_name.value = "";
          document.TransactionGUAR.applicant_address_line_1.value = "";
          document.TransactionGUAR.applicant_address_line_2.value = "";
          document.TransactionGUAR.applicant_address_city.value = "";
          document.TransactionGUAR.applicant_address_state_province.value = "";
          document.TransactionGUAR.applicant_address_country.value = "";
          document.TransactionGUAR.applicant_address_postal_code.value = "";
          document.TransactionGUAR.applicant_phone_number.value = "";
          document.TransactionGUAR.applicant_OTL_customer_id.value = "";
          document.TransactionGUAR.ApplicantUserDefinedField1.value = "";
          document.TransactionGUAR.ApplicantUserDefinedField2.value = "";
          document.TransactionGUAR.ApplicantUserDefinedField3.value = "";
          document.TransactionGUAR.ApplicantUserDefinedField4.value = "";
        }    
      </script>
