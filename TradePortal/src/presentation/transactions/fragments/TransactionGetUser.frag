<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 
 *******************************************************************************
                     		Transaction Get User          
 
   Description: Gets User Bean data to be used with Transactions' frags
   Created on 08/18/10 by IAZ as CR-586

*******************************************************************************
--%>

      <%   
         UserWebBean loggedUser = beanMgr.createBean(UserWebBean.class, "User");
         loggedUser.setAttribute("user_oid", userSession.getUserOid());
         loggedUser.getDataFromAppServer();
      %>
