<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
*******************************************************************************
                 Approvalto Pay Amend Page - all sections

  Description:
    Contains HTML to create the Approvalto Pay Amend page.  All data retrieval 
  is handled in the Transaction-ATP-AMD.jsp.

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transaction-ATP-ISS-BankDefined.jsp" %>
*******************************************************************************

--%>
<%
boolean isStructuredPO = false;//ShilpaR Cr707
boolean hasStructuredPOs = false;
String corpOrgOid = instrument.getAttribute("corp_org_oid");
CorporateOrganizationWebBean corporateOrg = beanMgr.createBean(CorporateOrganizationWebBean.class, "CorporateOrganization");
corporateOrg.setAttribute("organization_oid", corpOrgOid);
corporateOrg.getDataFromAppServer();
 
if(TradePortalConstants.PO_UPLOAD_STRUCTURED.equals(corporateOrg.getAttribute("po_upload_format")))
	isStructuredPO = true;
	  
%>

<%=widgetFactory.createSectionHeader("1", "ApprovalToPayIssue.Terms") %>

<div class="columnLeftNoBorder">

<%= widgetFactory.createTextField( "RefNumber", "transaction.YourRefNumber", instrument.getAttribute("copy_of_ref_num"), "30", true) %>
<%= widgetFactory.createTextField( "BuyerName", "ApprovalToPayAmend.BuyerName", applicantName, "30", true) %>
<%= widgetFactory.createDateField( "CurrentExpiryDate", "ApprovalToPayAmend.CurrentExpiryDate", currentExpiryDate, true) %>
<%= widgetFactory.createDateField( "ExpiryDate", "ApprovalToPayIssue.NewExpiryDate", StringFunction.xssHtmlToChars(terms.getAttribute("expiry_date")),
		isReadOnly, false, false, "class='char6'", dateWidgetOptions, "")%>
    

</div>
<div class="columnRight">
<%
 if(!isProcessedByBank)
  {
%>
			<%= widgetFactory.createTextField( "ATPAmount", "ApprovalToPayAmend.ATPAmount", currencyCode+ " " + TPCurrencyUtility.getDisplayAmount(originalAmount.toString(), 
                currencyCode, loginLocale), "30", true) %>
	<%
}
defaultText = "";
options = Dropdown.createIncreaseDecreaseOptions(resMgr, 
        incrDecrValue, loginLocale);
%>

&nbsp;&nbsp;&nbsp;<%=widgetFactory.createRadioButtonField(
			"IncreaseDecreaseFlag", "Increase",
			"ImportDLCAmend.IncreaseAmount",
			TradePortalConstants.INCREASE,
			incrDecrValue.equals(TradePortalConstants.INCREASE),
			isReadOnly || isFromExpress, " onChange='setNewAmount();'", "")%>
<br>
&nbsp;&nbsp;&nbsp;<%=widgetFactory.createRadioButtonField(
			"IncreaseDecreaseFlag", "Decrease",
			"ImportDLCAmend.DecreaseAmount",
			TradePortalConstants.DECREASE,
			incrDecrValue.equals(TradePortalConstants.DECREASE),
			isReadOnly || isFromExpress," onChange='setNewAmount();'", "")%>

	<%if (!isReadOnly) {%>
			<br><br>
			<div class="formItem">
				<%=widgetFactory.createInlineLabel("",currencyCode)%>
				<%-- <%= widgetFactory.createTextField( "TransactionAmount", "", displayAmount, "22", isReadOnly, false, false, " onChange='setNewAmount();'", "", "none") %>  --%>
				<%= widgetFactory.createAmountField( "TransactionAmount", "", displayAmount, currencyCode, isReadOnly, false, false, "onChange='setNewAmount();'", "", "none") %>				
			</div>
			<%}else{
				displayAmount = TPCurrencyUtility.getDisplayAmount(displayAmount, currencyCode, loginLocale);
				StringBuffer         tempDisplayAmt              = new StringBuffer();
				tempDisplayAmt.append(currencyCode);
				tempDisplayAmt.append(" ");
				tempDisplayAmt.append(displayAmount);
				%>
				<%= widgetFactory.createAmountField( "TransactionAmount", " ", tempDisplayAmt.toString(), "22", true, false, false, " onChange='setNewAmount();'", "", "") %>
				<input type="hidden" name="TransactionAmount"
			               value="<%=amountValue%>">			
			<%}%>
			<%-- Srinivasu_D IR#T36000033886 Rel9.1 10/27/2014 - Commented below duplicated variable --%>


				
<%
String newATPAmount= "";
 if(!isProcessedByBank)
  {
	 if(!badAmountFound) {
		 newATPAmount = currencyCode + " " + TPCurrencyUtility.getDisplayAmount(calculatedTotal.toString(),currencyCode, loginLocale);
	 }
%>

<%= widgetFactory.createTextField( "TransactionAmount2", "ApprovalToPayAmend.NewATPAmount", newATPAmount.toString(), "22", true, false, false, "", "", "") %> 				

<%
}
%>
<div class = "formItem">
<%= widgetFactory.createStackedLabel("","ApprovalToPayIssue.NewAmtTolerance") %>

<%= widgetFactory.createSubLabel( "transaction.Plus") %>
<%if(!isReadOnly){ %>
<%= widgetFactory.createPercentField( "AmountTolerancePlus", "", terms.getAttribute("amt_tolerance_pos"), isReadOnly,
							false, false, "", "", "none") %>
<%}else {%>
	<b><%=terms.getAttribute("amt_tolerance_pos") %></b>
<%} %>
<%= widgetFactory.createSubLabel( "transaction.Percent") %>
&nbsp;&nbsp;
<%= widgetFactory.createSubLabel( "transaction.Minus") %>
<%if(!isReadOnly){ %>
<%= widgetFactory.createPercentField( "AmountToleranceMinus", "", terms.getAttribute("amt_tolerance_neg"), isReadOnly,
							false, false, "", "", "none") %>
<%}else {%>
	<b><%=terms.getAttribute("amt_tolerance_neg") %></b>
<%} %>	

<%= widgetFactory.createSubLabel( "transaction.Percent") %>
<div style="clear:both;"></div>
</div>




<br>
</div>
</div>
<%=widgetFactory.createSectionHeader("2", "ApprovalToPayAmend.Shipment") %>

<%--
*******************************************************************************
                  Approvalto Pay Amend Page - Shipment section

*******************************************************************************
--%>

   
    <div class="columnLeftNoBorder">   
    <%= widgetFactory.createDateField( "ShipmentDate", "ApprovalToPayIssue.NewLastestShipDate", terms.getFirstShipment().getAttribute("latest_shipment_date"), isReadOnly, false, isExpressTemplate, "class='char6'", dateWidgetOptions, "")%>
    
    
	<%
    options = Dropdown.createSortedRefDataOptions( TradePortalConstants.INCOTERM,
    		terms.getFirstShipment().getAttribute("incoterm"),
                           loginLocale);
	%>
	<%= widgetFactory.createSelectField( "Incoterm", "ApprovalToPayIssue.NewShippingTerm", " ", options, isReadOnly || isFromExpress, false, isExpressTemplate,  "class='char30'", "", "" ) %>
	
	</div>
	
	<div class="columnRight">
		<%= widgetFactory.createTextField( "IncotermLocation", "ApprovalToPayIssue.NewShipTermLocation", terms.getFirstShipment().getAttribute("incoterm_location"), "30", isReadOnly, false, false,  "", "",""  ) %>
	</div>
	
	<div style="clear:both;"></div>
	   <div class="columnLeftNoBorder">
      <span class="formItem">
      <%=widgetFactory.createStackedLabel("ApprovalToPayAmend.From", "ApprovalToPayAmend.From") %>
      </span>
      <%if(!isReadOnly){ %>
            <%= widgetFactory.createTextField( "ShipFromPort", "ImportDLCAmend.NewFromPort", terms.getFirstShipment().getAttribute("shipment_from"), "65", isReadOnly, false, false,  "style='width:310px;'", "",""  ) %>
      <%}else{ %>
            <%= widgetFactory.createTextArea( "ShipFromPort", "ImportDLCAmend.NewFromPort", terms.getFirstShipment().getAttribute("shipment_from"), isReadOnly,false,false,"rows='2' cols='58'","","") %>
      <%} %>
      <%if(!isReadOnly){ %>
            <%= widgetFactory.createTextField( "ShipToPort", "ImportDLCAmend.NewToPort", terms.getFirstShipment().getAttribute("shipment_to"), "65", isReadOnly, false, false,  "style='width:310px;'", "",""  ) %>
      <%}else{ %>
            <%= widgetFactory.createTextArea( "ShipToPort", "ImportDLCAmend.NewToPort", terms.getFirstShipment().getAttribute("shipment_to"), isReadOnly,false,false,"rows='2' cols='58'","","") %>
      <%} %>      
      </div>
      <div class="columnRight">
      <span class="formItem"> 
      <%=widgetFactory.createStackedLabel("ApprovalToPayAmend.To", "ApprovalToPayAmend.To")%>      
      </span>
      <%if(!isReadOnly){ %>
      <%= widgetFactory.createTextField( "ShipToDischarge", "ImportDLCAmend.NewToDischarge", terms.getFirstShipment().getAttribute("shipment_to_discharge"), "65", isReadOnly, false, false,  "style='width:310px;'", "",""  ) %>
      <%}else{ %>
            <%= widgetFactory.createTextArea( "ShipToDischarge", "ImportDLCAmend.NewToDischarge", terms.getFirstShipment().getAttribute("shipment_to_discharge"), isReadOnly,false,false,"rows='2' cols='58'","","") %>
      <%} %>
      <%if(!isReadOnly){ %>
      <%= widgetFactory.createTextField( "ShipFromLoad", "ImportDLCAmend.NewFromLoad", terms.getFirstShipment().getAttribute("shipment_from_loading"), "65", isReadOnly, false, false,  "style='width:310px;'", "",""  ) %>
      <%}else{ %>
            <%= widgetFactory.createTextArea( "ShipFromLoad", "ImportDLCAmend.NewFromLoad", terms.getFirstShipment().getAttribute("shipment_from_loading"), isReadOnly,false,false,"rows='2' cols='58'","","") %>
      <%} %>
      </div>
      <div style="clear:both;"></div>
     
        
	<%=widgetFactory.createWideSubsectionHeader("ImportDLCAmend.NewGoodsDescription", false,false, false, "") %>
	  <div>
		<%
	    if (isReadOnly ) {
	    	 %>
	    	<div class="indent"><%=widgetFactory.createNote("common.RemindPOChanged")%></div>
	   <% } else {
              options = ListBox.createOptionList(goodsDocList, "PHRASE_OID", "NAME", "", userSession.getSecretKey());
	    	defaultText = resMgr.getText("transaction.SelectPhrase", TradePortalConstants.TEXT_BUNDLE);
		%>
		
		<%= widgetFactory.createSelectField( "GoodsPhraseItem", "", defaultText, options, isReadOnly, false, false, "onChange=" + PhraseUtility.getPhraseChange(
	            "/Terms/ShipmentTermsList/goods_description",
	            "GoodsDescText",
	            "1000","document.forms[0].GoodsDescText"), "", "inline") %>
	      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=widgetFactory.createNote("ImportDLCAmend.RemindPOChanged")%>      
		
		<%
	    }
		%>
		
		<%= widgetFactory.createTextArea( "GoodsDescText", " ", terms.getFirstShipment().getAttribute("goods_description").toString(), isReadOnly,false,false,"rows='10' cols='128'","","") %>
		<%if(!isReadOnly) {%>
		<%=widgetFactory.createHoverHelp("GoodsDescText","GoodsDescText") %>
		<%} %>
	</div>
        
    
        <%
           userOrgAutoATPCreateIndicator = userSession.getOrgAutoATPCreateIndicator();
           transactionOid                = transaction.getAttribute("transaction_oid");
           
	   
	   String userSecurityRights  = userSession.getSecurityRights();
	   boolean canProcessPOForATP = SecurityAccess.canProcessPurchaseOrder(userSecurityRights, InstrumentType.APPROVAL_TO_PAY);
	   

           if (((userOrgAutoATPCreateIndicator != null) && 
                (userOrgAutoATPCreateIndicator.equals(TradePortalConstants.INDICATOR_YES))) && 
               ((canProcessPOForATP) ||
               (userSession.hasSavedUserSession() && 
                (userSession.getSavedUserSessionSecurityType().equals(TradePortalConstants.ADMIN)))))
           {
        	   sqlQuery = new StringBuffer();
        	   if(!isStructuredPO){
              // Compose the SQL to retrieve PO Line Items that are currently assigned to 
              // this transaction
              sqlQuery.append("select a_source_upload_definition_oid, ben_name, currency");
              sqlQuery.append(" from po_line_item where a_assigned_to_trans_oid = ?");
              //jgadela  R90 IR T36000026319 - SQL FIX
			  Object sqlParams5[] = new Object[1];
			  sqlParams5[0] = transactionOid;

              // Retrieve any PO line items that satisfy the SQL query that was just constructed
              poLineItemsDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery.toString(), true, sqlParams5);

              if (poLineItemsDoc != null)
              {
                 hasPOLineItems = true;

                 poLineItems                   = poLineItemsDoc.getFragments("/ResultSetRecord");
                 poLineItemDoc                 = (DocumentHandler) poLineItems.elementAt(0);
                 poLineItemUploadDefinitionOid = poLineItemDoc.getAttribute("/A_SOURCE_UPLOAD_DEFINITION_OID");
                 poLineItemBeneficiaryName     = poLineItemDoc.getAttribute("/BEN_NAME");
                 poLineItemCurrency            = poLineItemDoc.getAttribute("/CURRENCY");
              }
        	  }
        	   else {
  				 sqlQuery.append("select a_upload_definition_oid, seller_name, currency");
                   sqlQuery.append(" from purchase_order where a_transaction_oid = ?");	 
                   //jgadela  R90 IR T36000026319 - SQL FIX
			  	   Object sqlParams6[] = new Object[1];
			  	   sqlParams6[0] = transactionOid;
                   poLineItemsDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery.toString(), true, sqlParams6);
  				 
                   if (poLineItemsDoc != null)
                     {
                  	 hasStructuredPOs = true;
                  	 poLineItems                   = poLineItemsDoc.getFragments("/ResultSetRecord");
                       poLineItemDoc                 = (DocumentHandler) poLineItems.elementAt(0);
                       poLineItemUploadDefinitionOid = poLineItemDoc.getAttribute("/A_UPLOAD_DEFINITION_OID");
                       poLineItemBeneficiaryName     = poLineItemDoc.getAttribute("/SELLER_NAME");
                       poLineItemCurrency            = poLineItemDoc.getAttribute("/CURRENCY");
                      }
          	     }
              if (!hasPOLineItems && (!(isReadOnly || isFromExpress)) &&  !isStructuredPO)
              {
        %>
		        
		   			<div class="formItem">
		   			<button data-dojo-type="dijit.form.Button"  name="AddPOLineItems" id="AddPOLineItems" type="submit">
		   		    <%=resMgr.getText("AddAtpAmdPOLineItems.Header",TradePortalConstants.TEXT_BUNDLE)%>
		   			    <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
		   			         setButtonPressed('AddUploadedPOLineItemsButton', '0');
		   			         document.forms[0].submit();
		   			    </script>
		   		    </button>
		   			</div>
                          <%
                               // Include currency code so that POs will be filtered
                               secureParms.put("addPO-currency", currencyCode);
                          %>
                    
        <%
              }
              else if (!hasStructuredPOs && (!(isReadOnly || isFromExpress)) && isStructuredPO) 
			  {
        %>
		        <div class="formItem">
		        <button data-dojo-type="dijit.form.Button"  name="AddStructuredPOButton" id="AddStructuredPOButton" onclick="AddPOStructure();" type="button">
	            <%=resMgr.getText("common.AddStructuredPOText",TradePortalConstants.TEXT_BUNDLE)%>		            
	         </button>
					</div>
				                 <%
                               // Include currency code so that POs will be filtered
                               secureParms.put("addPO-currency", currencyCode);
                          %>
            <%
              }
	    %>
        
      
  <%
     // It is possible that after a transaction has been processed by bank that it has
     // no POs assigned to it because these POs have been moved to amendments.  However,
     // we still want to display the PO information in the PO line items field.
     if(transaction.getAttribute("transaction_status").equals(TradePortalConstants.TRANS_STATUS_PROCESSED_BY_BANK) &&
           !InstrumentServices.isBlank(terms.getFirstShipment().getAttribute("po_line_items")))
               hasPOLineItems = true;

     //Krishna IR-PUUH111957767 11/30/2007 (Replaced userOrgAutoLCCreateIndicator with userOrgAutoATPCreateIndicator)
     //if (((userOrgAutoLCCreateIndicator != null) && 
     //     (userOrgAutoLCCreateIndicator.equals(TradePortalConstants.INDICATOR_YES))) && hasPOLineItems)
     if (((userOrgAutoATPCreateIndicator != null) && 
          (userOrgAutoATPCreateIndicator.equals(TradePortalConstants.INDICATOR_YES))) && hasPOLineItems)
     {
  %>
  <%if(!isReadOnly){%>
   <div class="formItem">
  <%--PR ER 8.1.0.5 -T36000015639 BEGIN. Changes done to enable right click, text highlighting, Copy options.--%>
  <%//widgetFactory.createTextArea( "POLineItems", "ApprovalToPayAmend.POLineItems", terms.getFirstShipment().getAttribute("po_line_items"), isReadOnly,false, false, "oncontextmenu='return false;' onselectstart='return false;' style='width:auto;' rows='10' cols='128' onFocus='this.blur();'", "","" ) %>
  <%=widgetFactory.createPOTextArea("POLineItems", terms.getFirstShipment().getAttribute("po_line_items"), "78", "10", 
        "POLineItems", false, "", "OFF",true)%>
  <%--PR ER 8.1.0.5 -T36000015639 END. Changes done to enable right click, text highlighting, Copy options.--%>  
  </div>  
  <%--IR T36000023557 start--%>
       <%}else{%>
        <%=widgetFactory.createAutoResizeTextArea("POLineItems","", terms.getFirstShipment().getAttribute("po_line_items"), isReadOnly, false, false, "style='width:600px;min-height:140px;' cols='100'","","")%>
        <%}%>  
   <%--IR T36000023557 end--%>
              <%
                 if ((canProcessPOForATP) && 
                     (!(isReadOnly || isFromExpress)))
                 {
                   secureParms.put("addPO-uploadDefinitionOid", poLineItemUploadDefinitionOid);
                   secureParms.put("addPO-beneficiaryName", poLineItemBeneficiaryName);
                   secureParms.put("addPO-currency", poLineItemCurrency);
                  secureParms.put("addPO-shipment_oid",  terms.getFirstShipment().getAttribute("shipment_oid"));
              %>
              <div class="formItem">
      		<button data-dojo-type="dijit.form.Button"  name="AddPOLineItems" id="AddPOLineItems" type="submit">
      	    <%=resMgr.getText("AddPOLineItems.Header",TradePortalConstants.TEXT_BUNDLE)%>
      		    <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
      		         setButtonPressed('AddPOLineItems', '0');
      		         document.forms[0].submit();
      		    </script>
      	    </button>
      	    
      	    <button data-dojo-type="dijit.form.Button"  name="RemovePOLineItemsButton" id="RemovePOLineItemsButton" type="submit">
      	    <%=resMgr.getText("common.RemovePOLineItemsText",TradePortalConstants.TEXT_BUNDLE)%>
      		    <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
      		         setButtonPressed('RemovePOLineItemsButton', '0');
      		         document.forms[0].submit();
      		    </script>
      	    </button>
      	    </div> 
              <%
                 }
              %>
  <%
     }
  %>
  <%
  // =================================== start of Structured PO Assigned=================================== 

if (TradePortalConstants.INDICATOR_YES.equals(userOrgAutoATPCreateIndicator) && isStructuredPO && hasStructuredPOs)
{
       secureParms.put("addPO-uploadDefinitionOid", poLineItemUploadDefinitionOid);
       secureParms.put("addPO-beneficiaryName", poLineItemBeneficiaryName);
       secureParms.put("addPO-currency", poLineItemCurrency);
      secureParms.put("addPO-shipment_oid",  terms.getFirstShipment().getAttribute("shipment_oid"));
      secureParms.put("addPO-hasStructuredPO", String.valueOf(hasStructuredPOs));
%>
           
		<div class = "formItem"> 
		<%-- DK IR T36000018062 Rel8.2 06/14/2013 starts --%>                    
		<button data-dojo-type="dijit.form.Button"  name="ViewStructuredPOButton" id="ViewStructuredPOButton" onclick="ViewPOStructure();" type="button">
				 <%=resMgr.getText("common.ViewStructuredPOText",TradePortalConstants.TEXT_BUNDLE)%>		 		 
		</button>
     <%
                    if ((SecurityAccess.hasRights(loginRights, SecurityAccess.DLC_PROCESS_PO)) && 
                         (!(isReadOnly || isFromExpress)))
                     {

                  %>
                  <button data-dojo-type="dijit.form.Button"  name="AddMoreStructuredPOButton" id="AddMoreStructuredPOButton" onclick="AddPOStructure();" type="button">
 		 		 <%=resMgr.getText("common.AddMoreStructuredPOText",TradePortalConstants.TEXT_BUNDLE)%>
 		 		 </button>
             
	             <button data-dojo-type="dijit.form.Button"  name="RemoveStructuredPOButton" id="RemoveStructuredPOButton" onclick="RemovePOStructure();" type="button">
	 		 		 <%=resMgr.getText("common.RemoveStructuredPOText",TradePortalConstants.TEXT_BUNDLE)%>
	 		    </button>
	 		    <%-- DK IR T36000018062 Rel8.2 06/14/2013 ends --%>  
     <%
                   }
                  %>
        </div>          

<%
}
  
}
%>
</div>
<%=widgetFactory.createSectionHeader("3", "ApprovalToPayAmend.OtherConditions") %>

<%--
*******************************************************************************
                  Approvalto Pay Amend Page - Other Conditions section

*******************************************************************************
--%>
<%-- SHR PR CR708 --%>
<%=widgetFactory.createWideSubsectionHeader("ApprovalToPayIssue.Invoices")%>
	
	<%= widgetFactory.createCheckboxField( "InvoiceOnlyInd", "ApprovalToPayIssue.AprovedInvOnly", TradePortalConstants.INDICATOR_YES.equals(terms.getAttribute("invoice_only_ind")), isReadOnly || isFromExpress, isFromExpress, "", "", ""  ) %>
	
	<%= widgetFactory.createDateField( "InvoiceDueDate", "ApprovalToPayIssue.InvoiceDueDate", StringFunction.isNotBlank(terms.getAttribute("invoice_due_date")) ? terms.getAttribute("invoice_due_date").toString() : "", isReadOnly, false, false,  "", "placeHolder:'"+datePattern+"'", "" ) %>
	
	
	<%=widgetFactory.createTextArea("InvoiceDetailsText","",invoiceDetails,true, false, false, "rows='10' cols='128'", "", "") %>
	<%
       //     userOrgATPInvoiceIndicator     = userSession.getOrgATPInvoiceIndicator();
           
      // String userSecurityRights  = userSession.getSecurityRights();
	   boolean canProcessInvoiceForATP = SecurityAccess.canProcessATPInvoice(userSecurityRights, InstrumentType.APPROVAL_TO_PAY);
           transactionOid  = transaction.getAttribute("transaction_oid");
           String            invSellerName     = "";
           String            invCurrency            = "";
           String            invOid          = "";
           String invDueDate          = "";
           boolean hasInvoiceItems =false;
           Vector invoiceItems = null;
           if ((canProcessInvoiceForATP ||
                (userSession.hasSavedUserSession() && 
                (userSession.getSavedUserSessionSecurityType().equals(TradePortalConstants.ADMIN)))))
           {
 
        	 //check if already Invs are associated
          		    StringBuffer invSqlQuery = new StringBuffer();
          		 	invSqlQuery.append("select A_UPLOAD_DEFINITION_OID, seller_name, currency,(case when payment_date is null then due_date else payment_date end) as pay_date");
          			invSqlQuery.append(" from invoices_summary_data where a_terms_oid = ?");	 
          			//jgadela  R90 IR T36000026319 - SQL FIX
			  		Object sqlParams7[] = new Object[1];
			  		sqlParams7[0] = termsOid;				
                    DocumentHandler   invDoc = DatabaseQueryBean.getXmlResultSet(invSqlQuery.toString(), true, sqlParams7);
                   if (invDoc != null)
                     {
                	   hasInvoiceItems = true;
                	   invoiceItems                  = invDoc.getFragments("/ResultSetRecord");
                	   DocumentHandler invItemDoc    = (DocumentHandler) invoiceItems.elementAt(0);
                       invOid = invItemDoc.getAttribute("/A_UPLOAD_DEFINITION_OID");
                       invSellerName     = invItemDoc.getAttribute("/SELLER_NAME");
                       invCurrency            = invItemDoc.getAttribute("/CURRENCY");
                       invDueDate            = invItemDoc.getAttribute("/PAY_DATE");
                      }
          	     }

              // Use the transaction's currency if a po line item currency does not exist
              if (InstrumentServices.isBlank(invCurrency))
                invCurrency = currencyCode;

              // Store po line item data in secure parms which will be used by the pages
              // which add/remove po line items
              secureParms.put("addInv-uploadInvDefOid", invOid);
              secureParms.put("addInv-sellerName", invSellerName);
              secureParms.put("addInv-invCurrency", invCurrency);
              secureParms.put("addInv-termsOid", terms.getAttribute("terms_oid"));
              secureParms.put("addInv-hasInvoiceItems", String.valueOf(hasInvoiceItems));
              secureParms.put("addInv-dueDate", invDueDate); //SHR IR T36000005856
              if (canProcessInvoiceForATP  && !hasInvoiceItems)
              {   
        %>
       
			<div class = "formItem">
				<button data-dojo-type="dijit.form.Button" type="submit" name="AddInvoiceButton" id="AddInvoiceButton">
		          <%=resMgr.getText("Atp.AssignInvoices",TradePortalConstants.TEXT_BUNDLE)%>	
		          <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
			      setButtonPressed('AddInvoiceButton', '0');
                  document.forms[0].submit();
				  </script>	            
		       </button>
		     </div>
		      <% } 
              if (canProcessInvoiceForATP && !isTemplate && hasInvoiceItems)
              {
		           %>
		   <div class = "formItem">                   
           		<button data-dojo-type="dijit.form.Button" type="submit" name="ViewInvoiceButton" id="ViewInvoiceButton" >
		 		 <%=resMgr.getText("Atp.ViewAssignInvoices",TradePortalConstants.TEXT_BUNDLE)%>	
		 		 <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
			      setButtonPressed('ViewInvoiceButton', '0');
                  document.forms[0].submit();
				  </script>	 	 		 
		   		</button>
		   		<%= widgetFactory.createHoverHelp("ViewInvoiceButton","Atp.ViewAssignInvoices")%>
		   		 <%
  			    if (canProcessInvoiceForATP && 
                        (!(isReadOnly || isFromExpress)))
          		{										
                               %>
           		 <button data-dojo-type="dijit.form.Button" type="submit" name="AddMoreInvoiceButton" id="AddMoreInvoiceButton" >
		 		 <%=resMgr.getText("Atp.AssignInvoices",TradePortalConstants.TEXT_BUNDLE)%>
		 		  <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
			      setButtonPressed('AddInvoiceButton', '0');
                  document.forms[0].submit();
				  </script>	
		    	</button>
		    	<%= widgetFactory.createHoverHelp("AddMoreInvoiceButton","Atp.AssignInvoices")%>
            
            	<button data-dojo-type="dijit.form.Button" type="submit" name="RemoveInvoiceButton" id="RemoveInvoiceButton" >
		 		 <%=resMgr.getText("Atp.RemoveInvoices",TradePortalConstants.TEXT_BUNDLE)%>
		 		  <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
			      setButtonPressed('RemoveInvoiceButton', '0');
                  document.forms[0].submit();
				  </script>	
		   		</button>  
		   		<%= widgetFactory.createHoverHelp("RemoveInvoiceButton","Atp.RemoveInvoices")%>             
                            
                               
          <%
          		 }
		  %>
		   </div>  
		  <%
		    }
	      %>
	<%=widgetFactory.createWideSubsectionHeader("ApprovalToPayIssue.AdditionalConditions")%>
	<%-- SHR PR CR708 --%>
  
  
<%
        if (isReadOnly) {
          out.println("&nbsp;");
        } else {
          options = ListBox.createOptionList(addlCondDocList, "PHRASE_OID", "NAME", "", userSession.getSecretKey());
          defaultText = resMgr.getText("transaction.SelectPhrase",
                                       TradePortalConstants.TEXT_BUNDLE);
 %>
          <%=widgetFactory.createSelectField("AddlDocsPhraseItem","", defaultText, options,isReadOnly , false, false,  
					"onChange=" + PhraseUtility.getPhraseChange("/Terms/additional_conditions","AddlConditionsText", "5000","document.forms[0].AddlConditionsText"), "", "")%>
<%
        }
%>
	<%=widgetFactory.createTextArea("AddlConditionsText","",terms.getAttribute("additional_conditions"),isReadOnly,false,false,"rows='10' cols='128'","","") %>
	<%if(!isReadOnly) {%>
	<%=widgetFactory.createHoverHelp("AddlConditionsText","AddlConditionsText") %>
 	<%} %>
</div>
<%=widgetFactory.createSectionHeader("4", "ApprovalToPayAmend.ClientBankInstructions") %>

<%--
*******************************************************************************
               Approvalto Pay Amend Page - Bank Instructions section

*******************************************************************************
--%>

 

 
<%
        if (isReadOnly) {
          out.println("&nbsp;");
        } else {
          options = ListBox.createOptionList(spclInstrDocList, "PHRASE_OID", "NAME", "", userSession.getSecretKey());
          defaultText = resMgr.getText("transaction.SelectPhrase",
                                       TradePortalConstants.TEXT_BUNDLE);
          %>
          <%=widgetFactory.createSelectField("CommInvPhraseItem","", defaultText, options,isReadOnly , false, false,  
					"onChange=" + PhraseUtility.getPhraseChange("/Terms/special_bank_instructions","SpclBankInstructions", "5000","document.forms[0].SpclBankInstructions"), "", "")%>
          
<%
        }
%>
<%=widgetFactory.createTextArea("SpclBankInstructions","",terms.getAttribute("special_bank_instructions"),isReadOnly,false,false,"rows='10' cols='128'","","") %>
<%if(!isReadOnly) {%>
<%=widgetFactory.createHoverHelp("SpclBankInstructions","SpclBankInstructions") %>
<%} %>        
</div>
