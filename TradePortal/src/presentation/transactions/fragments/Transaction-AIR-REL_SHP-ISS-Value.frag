<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
*******************************************************************************
    Air Waybill Release and Shipping Guarantee Issue Page - Value section

  Description:
    Contains HTML to create the Air Waybill Release and Shipping Guarantee 
    Issue Value section.

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transaction-AIR-REL_SHP-ISS-Value.jsp" %>
*******************************************************************************
--%>
