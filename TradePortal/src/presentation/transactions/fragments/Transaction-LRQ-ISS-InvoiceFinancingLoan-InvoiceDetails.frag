<script type="text/javascript" src="/portal/js/datagrid.js"></script>
<div style="clear:both;"></div>

<%=widgetFactory.createSubsectionHeader(
                              "LoanRequest.InvoiceDetails",
                              (isReadOnly || isFromExpress), (!isTemplate?true:false), false, "")%>
                              <%=widgetFactory.createHoverHelp("InvoicesDetailsTextTrd", "InvoiceDetailsHoverText")%>
          
<%-- DK IR T36000016553 Rel8.2 05/02/2013 Starts --%>
<%---RPasupulati IR T36000045586 adding condition----%>
<%
if(loanReqProcessInvoices && corOrgProcessInvoicesFileUploadInd.equals(TradePortalConstants.INDICATOR_YES)){

%>
<div class="formItem">
	<%=widgetFactory.createPOTextArea("InvoicesDetailsTextRec",terms.getAttribute("invoice_details"),
			"78","10", "POLineItems", isReadOnly || isFromExpress, 
		"", "OFF", true) %>
	</div>
          
<%-- DK IR T36000016553 Rel8.2 05/02/2013 Ends --%>                         
<% if (numberOfDomPmts == 0 && !isTemplate) { %>
<div>
    <% 
	    if (numberOfInvoicesAttached > 0 && !isTemplate) { %>	
	     <div id="view_inv_div_invf" class='formItem inline'>
	     <button data-dojo-type="dijit.form.Button"  name="ViewTransInvoicesINVF" id="ViewTransInvoicesINVF" type="button">
	        <%=resMgr.getText("common.ViewTransInvoices",TradePortalConstants.TEXT_BUNDLE)%>
	        <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
	             setButtonPressed('ViewTransInvoices', 0); 
	             document.forms[0].submit();
	        </script>
	     </button>
	     </div>
	<%}%>

	<div id="add_inv_div_invf" class='formItem inline'>
    	<button data-dojo-type="dijit.form.Button"  name="AddTransInvoicesINVF" id="AddTransInvoicesINVF"  type="button">
            <%=resMgr.getText("common.AddTransInvoices",TradePortalConstants.TEXT_BUNDLE)%>
            <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
                 setButtonPressed('AddTransInvoices', 0); 
                 document.forms[0].submit();
            </script>
        </button>
    </div>
    <% 
    	if (numberOfInvoicesAttached > 0 && !isTemplate) { %>	
			<div id="remove_inv_div_invf" class='formItem inline'>		         
		         <button data-dojo-type="dijit.form.Button"  name="RemoveTransInvoicesINVF" id="RemoveTransInvoicesINVF"  type="button">
 		            <%=resMgr.getText("common.RemoveTransInvoices",TradePortalConstants.TEXT_BUNDLE)%>
 		            <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
 		                 setButtonPressed('RemoveTransInvoices', 0); 
 		                 document.forms[0].submit();
 		            </script>
 		         </button>
	     	 </div>
	<%}%>  

</div>  
<%
}
}//IR T36000045586 Rpasupulati Condition end
%>       
<div style="clear: both;"></div>                     
<%=widgetFactory.createTextArea(
                              "FinancedInvoicesDetailsTextRec", "LoanRequest.FinancedInvoicesDetailsText",
                              terms.getAttribute("financed_invoices_details_text"),
                              isReadOnly || areInvoicesAttached, false, false,
                              "rows='10' cols='250' ", "", "")%>
                               <%=widgetFactory.createHoverHelp("FinancedInvoicesDetailsTextRec", "FinancedInvoicesHoverText")%>
