<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
*******************************************************************************
                Settlement Instructions for Rollover loan request 

  Description:
    Contains HTML to create Rollover Settlement instruction for SIM transaction.  

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transaction-LRQ-SIM.jsp" %>
*******************************************************************************
--%>
<%
	presentationAmount.append(currencyCode);
	presentationAmount.append(" ");
	presentationAmount.append(displayAmount);
	
	secureParms.put("isRolloverInstruction",  TradePortalConstants.INDICATOR_YES); // this is rollover transaction.
%>

<% 
	TermsWebBean termsForLoan        	  = null;
	TransactionWebBean originalTransaction = beanMgr.createBean(TransactionWebBean.class,
	                                                        "Transaction");
	originalTransaction.getById(instrument.getAttribute("original_transaction_oid"));
	TransactionWebBean activeTransaction = beanMgr.createBean(TransactionWebBean.class,"Transaction");
	String bankReleasedTerms	= originalTransaction.getAttribute("c_BankReleasedTerms");         
	boolean hasBankReleasedTerms  = StringFunction.isNotBlank( bankReleasedTerms );
	if( hasBankReleasedTerms ) {
		if( StringFunction.isNotBlank(instrument.getAttribute("active_transaction_oid")) ) {
              activeTransaction.getById(instrument.getAttribute("active_transaction_oid"));   
		      termsForLoan = activeTransaction.registerBankReleasedTerms();
	    }  
	    else 
	    {
 	           termsForLoan = originalTransaction.registerBankReleasedTerms();
	    }
	} else {
	    termsForLoan = originalTransaction.registerCustomerEnteredTerms(); 
	}
	
	String maturityDate = null;
	String loanTermsType = null;
	if (termsForLoan != null) {
	   maturityDate = termsForLoan.getAttribute("loan_terms_fixed_maturity_dt");
	   loanTermsType = termsForLoan.getAttribute("loan_terms_type");
	}
%>
<table width="100%">
	<tr class="formItem" width="100%">
		<td style="vertical-align: top;" width="15%">
		<%= widgetFactory.createTextField("displayAmount", "SettlementInstruction.amount",
				presentationAmount.toString(), "35", true, false, false, "", "", "") %>
		</td>
		<td style="vertical-align: top;" width="15%">
		<%= widgetFactory.createTextField("loanStartDate", "SettlementInstruction.loanStartDate",
				TPDateTimeUtility.formatDate(instrument.getAttribute("issue_date"), TPDateTimeUtility.SHORT, loginLocale), "35", true, false, false, "", "", "") %>
		</td>
		<td style="vertical-align: top;" width="15%">
			<%= widgetFactory.createDateField("MaturityDate", "SettlementInstruction.maturityDate", maturityDate, true, false, false, "", "", "")%>
		</td>
		<td style="vertical-align: top;" width="30%">
		<% 
        	if ( StringFunction.isNotBlank(loanTermsType) ) {
        		if (loanTermsType.equals(TradePortalConstants.LOAN_DAYS_AFTER)) {       
        %>
               <%= widgetFactory.createTextField("loanTerms", "SettlementInstruction.LoanTerms", termsForLoan.getAttribute("loan_terms_number_of_days")+" " + 
                    resMgr.getText("LoanRequest.DaysFrom", TradePortalConstants.TEXT_BUNDLE), "60", true, false,false, "", "", "")%>
        <%		
        		} else {
        			String loanTermsDt = termsForLoan.getAttribute("loan_terms_fixed_maturity_dt"); 
        			loanTermsDt = TPDateTimeUtility.formatDate( loanTermsDt, TPDateTimeUtility.LONG, loginLocale );
         %>
          			<%=widgetFactory.createTextField("loanTerms", "SettlementInstruction.LoanTerms", resMgr.getText("LoanRequest.AtFixedMaturityDate",TradePortalConstants.TEXT_BUNDLE)+ 
          				" " +loanTermsDt, "60", true, false,false, "", "", "")%>
         <% 	}
        	} else {
         %>
         		<%= widgetFactory.createTextField("loanTerms", "SettlementInstruction.LoanTerms", "", "60", true, false,false, "", "", "")%>
         <% } %>	
		</td>
		<td style="vertical-align: top;" width="25%">
			<%= widgetFactory.createTextField("otehrParty", "SettlementInstruction.otherParty",
				counterPartyName, "35", true, false, false, "", "", "") %>
		</td>
	</tr>
</table>
				
<div style="clear: both;"></div>
  <%@ include file="Transaction-SettlementInstruction-Common_detail.frag" %>
<div style="clear: both;"></div>

<div class = "columnLeftWithIndent1">
<%=widgetFactory.createLabel("","SettlementInstruction.useFollwingInstr", false, true, false,"","")%>
  <%
    String payinFullFinRollInd = terms.getAttribute("pay_in_full_fin_roll_ind");
    String finCurrency = terms.getAttribute("fin_roll_curr");
    String CurrOptions = Dropdown.createSortedCurrencyCodeOptions(finCurrency,loginLocale); 
    String displayPartialAmount = TPCurrencyUtility.getDisplayAmount(terms.getAttribute("fin_roll_partial_pay_amt"), finCurrency, loginLocale);
  %>
  <div class="formItem">
   <%=widgetFactory.createCheckboxField("payInFullFinRollInd", "SettlementInstruction.rolloverInFinCurr", TradePortalConstants.SETTLEMENT_REQ_ROLLOVER,
										TradePortalConstants.SETTLEMENT_REQ_ROLLOVER.equals(payinFullFinRollInd), isReadOnly, false,"","", "none")%>
		
   <%
	if(isReadOnly){
%>
	<%=widgetFactory.createBoldSubLabel(terms.getAttribute("fin_roll_curr"))%>
<%
	}else{
%>	
  <%=widgetFactory.createSelectField("finRollCurr","", " ", CurrOptions, isReadOnly, false,false, 
                  "style=\"width: 50px;\"", "", "none")%>
<%
	}
%>  
                   
   <div style="clear: both;"></div>
   <div class="formItem">
     <%@ include file="Transaction-SettlementInstruction-Roll-Finance_Detail.frag" %>
   </div>
 </div>
 </br>
</div>

<div class = "columnRightWithIndent1">
   <%@ include file="Transaction-SettlementInstruction-Debit_ACCT_Detail.frag" %>				
</div>

<div style="clear: both;"></div>
<%@ include file="Transaction-SettlementInstruction-Other_Addl_detaill.frag" %>
