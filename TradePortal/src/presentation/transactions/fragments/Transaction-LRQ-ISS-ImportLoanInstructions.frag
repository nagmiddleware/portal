<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
*******************************************************************************
                 Loan Request Page - Import Payment Instructions section

  Description:
    Contains HTML to create the Loan Request Import Payment Instructions section.  
    It shows the payment instruction when the loan type is Export (import_indicator = "N").

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transaction-LRQ-ISS-ImportLoanInstructions.frag" %>
*******************************************************************************
--%><%
int rowCount = 8;
%>
<div class="columnLeft">
      <%=widgetFactory.createSubsectionHeader("LoanRequest.LoanProceeds", (isReadOnly || isFromExpress),false, false, "")%>
      <div>
            <%
                  options = Dropdown.createSortedCurrencyCodeOptions(isImport? loanProceedsPmtCurrency: "", loginLocale);
            %>
            <%=widgetFactory.createSelectField("LoanProceedsPmtCurrImp","LoanRequest.CcyOfPayment", defaultCurrencyVal, options, isReadOnly,!isTemplate, false, "style=\"width: 50px;\"", "", "inline")%>
            <%=widgetFactory.createAmountField("LoanProceedsPmtAmountImp","LoanRequest.LoanProceedsAmount", isImport          ? displayLoanProceedsPmtAmount: "", loanProceedsPmtCurrency, isReadOnly, false, false,"style=\"width: 126px;\"", "", "inline")%>
            <div style="clear: both;"></div>
      </div>
      <%=widgetFactory.createSubsectionHeader("LoanRequest.ApplyLoanProceedsTo",(isReadOnly || isFromExpress), !isTemplate, false, "")%>
      <div class="formItem">
            <%=widgetFactory.createRadioButtonField("LoanProceedsCreditTypeImp","RelatedInst","", TradePortalConstants.CREDIT_REL_INST, (isImport? loanProceedsCreditType       : "").equals(TradePortalConstants.CREDIT_REL_INST),false, "onClick=handleBeneficiaryRequirdness('ImportLoan',false)", "")%>
           <%=resMgr.getText("LoanRequest.RelatedInstruments",TradePortalConstants.TEXT_BUNDLE)%>
            <%=widgetFactory.createNote("LoanRequest.ForASingleRelatedInstrument")%>
         <div class="formItem">
            <%
                  if (InstrumentServices.isBlank(instrumentID)) {
                        instrumentID = terms.getAttribute("related_instrument_id");
                  }
            %>
            <%    String instrSearchHtml = "";
            String itemID="RelatedInstrumentID";
            String section="InstrumentId";
                  if (!isReadOnly) {// Only display the search buttons in edit mode
                        
                        String searchValue3 = TradePortalConstants.IMPORT;
                        clickTag3 = "setButtonPressed('"+TradePortalConstants.BUTTON_INSTRUMENT_SEARCH+"','0');return setInstrumentSearchType('" + searchValue3+ "');";
                        //cquinton 9/19/2012 use createSearchLink method instead, which does not hardcode img tags
                        //instrSearchHtml = widgetFactory.createSearchLink(TradePortalConstants.BUTTON_INSTRUMENT_SEARCH, "javascript:document.forms[0].submit()", clickTag3, false);
                  instrSearchHtml =widgetFactory.createInstrumentSearchButton(itemID,section, isReadOnly,TradePortalConstants.IMPORT,false);
                  }
            %>          
            <table id="addInstrument">
            <tr>
            <td>
            <%=widgetFactory.createTextField("RelatedInstrumentID"," ",instrumentID,"16",isReadOnly,false,false,"onChange=\"pickRelatedInstImp()\"","", "none",instrSearchHtml)%>
            </td>
            </tr>
            <%String multRelatedInstr = isImport ? (terms.getAttribute("multiple_related_instruments")):"";
            String[] multRelatedInstrArr = multRelatedInstr.split(",");
            int j=0;
            if(multRelatedInstrArr.length > 1){
                  
                  for( j=0;j< multRelatedInstrArr.length ;j++){
                        String multiInstrSearchHtml =widgetFactory.createInstrumentSearchButton("multipleInstrumentID"+j,section, isReadOnly,TradePortalConstants.IMPORT,false);
                  %>
                  <tr height="10px;">
                  <td height="10px;">
                  <%=widgetFactory.createTextField("multipleInstrumentID"+j," ",multRelatedInstrArr[j],"16",isReadOnly,false,false,"onChange=\"prepareMultipleInstrString()\"","", "inline",multiInstrSearchHtml)%>
                  </td>
                  </tr>
                  <%}
            }
            %>
            </table>
             <input type=hidden value="<%=j%>" name="numberOfMultipleObjects1"      id="noOfInst">
              <input type=hidden value="<%=multRelatedInstr%>"  name="MultipleRelatedInstrumentsImp"      id="MultipleRelatedInstrumentsImp">
            <div style="clear: both;"></div>
            </div>
      </div>
      <div class="formItemWithIndent1">
            
            <button data-dojo-type="dijit.form.Button" type="button" id="add4MoreRow2" name="add4MoreRow2">
            <%= resMgr.getText("common.addmultipleinstruments", TradePortalConstants.TEXT_BUNDLE)%>
             <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
            		 pickRelatedInstImp();
                        AddMultipleInstruments();
                        </script>
                  </button>
                  </div>
            <div>
                  <div class="formItem inline">
                  <%=widgetFactory.createRadioButtonField("LoanProceedsCreditTypeImp","CreditOurAcct","LoanRequest.OurAccount", TradePortalConstants.CREDIT_OUR_ACCT, (isImport? loanProceedsCreditType		: "").equals(TradePortalConstants.CREDIT_OUR_ACCT),isReadOnly, "onClick=handleBeneficiaryRequirdness('ImportLoan',false)", "")%>
                 </div>
                  <%
                        // Using the acct_choices xml from the app party,build the dropdown and 
                        // select the one matching loan_proceeds_credit_acct
                        appAcctList = StringFunction.xssHtmlToChars(termsPartyApp.getAttribute("acct_choices"));

                        DocumentHandler acctOptions1 = new DocumentHandler(appAcctList, true);

                        options = Dropdown.createSortedAcctOptions(acctOptions1,    isImport ? StringFunction.xssHtmlToChars(terms     .getAttribute("loan_proceeds_credit_acct")) : "",loginLocale);
                  %>
                 <%=widgetFactory.createSelectField("LoanProceedsCreditAccountImp",""," ",options,isReadOnly,false,false,"onChange=\"pickCreditOurAcctTypeImp()\""+"style='width:17em'","", "inline")%> <div style="clear: both;"></div>
            </div>
            
            <div class="formItem">
           <%=widgetFactory.createRadioButtonField("LoanProceedsCreditTypeImp","CreditBenAccount","LoanRequest.TheFollowingBeneficiary", TradePortalConstants.CREDIT_BEN_ACCT, (isImport? loanProceedsCreditType: "").equals(TradePortalConstants.CREDIT_BEN_ACCT),isReadOnly, "onClick=handleBeneficiaryRequirdness('ImportLoan',true)", "")%>
            <%//=resMgr.getText("LoanRequest.TheFollowingBeneficiary",TradePortalConstants.TEXT_BUNDLE)%>
                  <%=widgetFactory.createNote("LoanRequest.TheFollowingBeneficiaryitalic")%>
           </div>
            <div class="formItem">
                 <%=widgetFactory.createRadioButtonField("LoanProceedsCreditTypeImp","CreditConsignAccount","LoanRequest.OtherRadio", TradePortalConstants.CREDIT_OTHER_ACCT, (isImport? loanProceedsCreditType: "").equals(TradePortalConstants.CREDIT_OTHER_ACCT),isReadOnly, "onClick=handleBeneficiaryRequirdness('ImportLoan',false)", "")%>
                    <%if(!isReadOnly){ %>
                  (<a href='javascript:openOtherConditionsDialog("LoanProceedsCreditOtherTextImp","SaveTrans", "<%=resMgr.getText("common.OtherDetails",TradePortalConstants.TEXT_BUNDLE)%>");' onClick="setCreditConsignAccount()"><%=resMgr.getText("common.OtherDetails",TradePortalConstants.TEXT_BUNDLE)%></a>)
                  <%}else{ %>
                  <%=resMgr.getText("common.OtherDetails",TradePortalConstants.TEXT_BUNDLE)%>
                  <%} %>
            </div>
            
                       <div style="display:none;"> 
            <%=widgetFactory.createTextArea("LoanProceedsCreditOtherTextImp", "LoanRequest.OtherRadio",isImport?(terms.getAttribute("loan_proceeds_credit_other_txt")):"",isReadOnly, false, false, "rows='10'", "", "")%>
			</div>
            
      <%
            String benSearchHtml = "";
            String benClearHtml = "";
            String importBankidentifierStr=",BbkNameImp,BbkAddressLine1Imp,BbkAddressLine2Imp,BbkCityImp,BbkStateProvinceImp,BbkPostalCodeImp,BbkCountryImp,BbkOTLCustomerIdImp";
            String identifierStr4="BenNameImp,BenAddressLine1Imp,BenAddressLine2Imp,BenCityImp,BenStateProvinceImp,BenPostalCodeImp,BenCountryImp,BenOTLCustomerIdImp"+importBankidentifierStr;
            String sectionName4="benimp";
            if (!(isReadOnly)) {
      %>
      <%
            benSearchHtml = widgetFactory.createPartySearchButton(
                              identifierStr4,sectionName4, false,
                              TradePortalConstants.BENEFICIARY, false);
                  /* extra Tags : clearBeneficiaryImp();*/
                  benClearHtml = widgetFactory.createPartyClearButton("ClearBenButton", "clearBeneficiaryImp()", false, "");
      %>
      <%
            }// Always send the ben terms party oid
            secureParms.put("ben_terms_party_oid",
                        termsPartyBen.getAttribute("terms_party_oid"));
      %>

      <%=widgetFactory.createSubsectionHeader("LoanRequest.Beneficiary", isReadOnly, false, false,
                              (benSearchHtml + benClearHtml))%>
      
      
      <input type=hidden name='BenTermsPartyTypeImp'
            value=<%=TradePortalConstants.BENEFICIARY%>> 
            <%=widgetFactory.createTextField("BenOTLCustomerIdImp","",termsPartyBen.getAttribute("OTL_customer_id"),"35",false,false,false," type=hidden ","", "")%> 
            <input
            type=hidden name="VendorIdImp"
            value="<%=termsPartyBen.getAttribute("vendor_id")%>">

      <%=widgetFactory.createTextField("BenNameImp","LoanRequest.BeneName",isImport ? (termsPartyBen.getAttribute("name")) : "","35",
                              isReadOnly,false,
                              false,"onBlur='checkBeneficiaryNameImp(\""+ StringFunction.escapeQuotesforJS(termsPartyBen.getAttribute("name"))+ "\"); pickTheFollowingBeneficiaryRadioImp();'","", "")%>

      <%=widgetFactory.createTextField("BenAddressLine1Imp","LoanRequest.BeneAddressLine1",isImport? (termsPartyBen.getAttribute("address_line_1")): "", "35", isReadOnly, false, false,"onBlur='pickTheFollowingBeneficiaryRadioImp();'", "", "")%>

      <%=widgetFactory.createTextField("BenAddressLine2Imp","LoanRequest.BeneAddressLine2",isImport? (termsPartyBen.getAttribute("address_line_2")): "", "35", isReadOnly, false, false, "", "", "")%>
 	<%--KMehta - 10 Feb 2015 - Rel9.2 IR-T36000035206 - Change  - Begin--%>
      <%=widgetFactory.createTextField("BenCityImp","LoanRequest.BeneCity",isImport? (termsPartyBen.getAttribute("address_city")): "", "23", isReadOnly, false, false,"onBlur='pickTheFollowingBeneficiaryRadioImp();'", "", "")%>
	<%--KMehta - 10 Feb 2015 - Rel9.2 IR-T36000035206 - Change  - End--%>      
      <div>
            <%=widgetFactory.createTextField("BenStateProvinceImp","LoanRequest.BeneProvinceState",isImport ? (termsPartyBen.getAttribute("address_state_province")) : "", "8",
                              isReadOnly, false, false, "class='char10'", "", "inline")%>


            <%=widgetFactory.createTextField("BenPostalCodeImp","LoanRequest.BenePostalCode",isImport ? (termsPartyBen.getAttribute("address_postal_code")) : "", "15",
                              isReadOnly, false, false, "class='char10'", "", "inline")%>
            <div style="clear: both;"></div>
      </div>
      <%
            options = Dropdown.createSortedRefDataOptions(TradePortalConstants.COUNTRY,
                        isImport? (termsPartyBen.getAttribute("address_country")): "", loginLocale);
      %>
      <%=widgetFactory.createSelectField("BenCountryImp","LoanRequest.BeneCountry", " ", options, isReadOnly, false,false, "class='char35'", "", "")%>
    <%-- Benificiary Account Number --%>
    
    <%--Beneficiary account number field can be either a text box or a set of radio buttons.
		we set the label here and put content in special div that is replaced via ajax as necessary.
		We use a partial page loading section to give a visual indicator--%>
		<div class='formItem<%=(!isTemplate) ? " required" : ""%>'><%=widgetFactory.createInlineLabel("SelectedAccount",
							"LoanRequest.BeneficiaryAccountNumber")%>
			<div style="clear: both;"></div>
				<div id="beneAccountsDataLoading" style="display: none;"><%--hide it to start--%>
				<span class='dijitInline dijitIconLoading'></span> <%=resMgr.getText("common.Loading",
									TradePortalConstants.TEXT_BUNDLE)%>
				</div>
				
				<div id="importBeneAccountsData">
				<%
					//setup the include. we pass following variables
					termsParty = termsPartyBen;
				%> <%@ include file="Transaction-LRQ-ISS-ImportLoan-BeneAcctData.frag"%>
				</div>		
		</div>
	
    
    </div>

	<div class="columnRight">
      <%
            String benBankSearchHtml = "";
            String identifierStr5="BbkNameImp,BbkAddressLine1Imp,BbkAddressLine2Imp,BbkCityImp,BbkStateProvinceImp,BbkPostalCodeImp,BbkCountryImp,BbkOTLCustomerIdImp";
            String sectionName5="bbkimp";
            if (!(isReadOnly)) {    
      benBankSearchHtml = widgetFactory.createPartySearchButton(
                  identifierStr5,sectionName5, false,
                  TradePortalConstants.BENEFICIARY_BANK, false);
            }
            // Always send the bbk terms party oid
            secureParms.put("bbk_terms_party_oid_imp",termsPartyBbk.getAttribute("terms_party_oid"));
      %>

      <%=widgetFactory.createSubsectionHeader("LoanRequest.BankBeneficiary", isReadOnly, false, false,benBankSearchHtml)%>
            <input type=hidden name='BbkTermsPartyTypeImp'
            value=<%=TradePortalConstants.BENEFICIARY_BANK%>> 
			<%=widgetFactory.createTextField("BbkOTLCustomerIdImp","",termsPartyBbk.getAttribute("OTL_customer_id"),"35",false,false,false," type=hidden ","", "")%>

      <%=widgetFactory.createTextField("BbkNameImp","LoanRequest.BeneBankName",isImport ? termsPartyBbk.getAttribute("name") : "","35",
                              isReadOnly,false,
                              false,"onBlur='checkBeneficiaryBankNameImp(\""+ termsPartyBbk.getAttribute("name") + "\")'", "","")%>

      <%=widgetFactory.createTextField("BbkAddressLine1Imp","LoanRequest.BeneBankAddressLine1", isImport? (termsPartyBbk.getAttribute("address_line_1")): "", "35", isReadOnly, false, false,"onBlur='pickTheFollowingBeneficiaryRadioImp();'", "", "")%>

      <%=widgetFactory.createTextField("BbkAddressLine2Imp","LoanRequest.BeneBankAddressLine2", isImport? (termsPartyBbk.getAttribute("address_line_2")): "", "35", isReadOnly, false, false, "", "", "")%>
	<%--KMehta - 10 Feb 2015 - Rel9.2 IR-T36000035206 - Change  - Begin--%>
      <%=widgetFactory.createTextField("BbkCityImp","LoanRequest.BeneBankCity",isImport? (termsPartyBbk.getAttribute("address_city")): "", "23", isReadOnly, false, false,"onBlur='pickTheFollowingBeneficiaryRadioImp();'", "", "")%>
	<%--KMehta - 10 Feb 2015 - Rel9.2 IR-T36000035206 - Change  - End--%> 
      <div>
            <%=widgetFactory.createTextField("BbkStateProvinceImp","LoanRequest.BeneBankProvinceState",isImport ? (termsPartyBbk.getAttribute("address_state_province")) : "", "8",
                              isReadOnly, false, false, "class='char10'", "", "inline")%>


            <%=widgetFactory.createTextField("BbkPostalCodeImp","LoanRequest.BeneBankPostalCode",isImport ? (termsPartyBbk.getAttribute("address_postal_code")) : "", "15",
                              isReadOnly, false, false, "class='char10'", "", "inline")%>
            <div style="clear: both;"></div>
      </div>
      <%
            options = Dropdown.createSortedRefDataOptions(TradePortalConstants.COUNTRY,
                        isImport? (termsPartyBbk.getAttribute("address_country")): "", loginLocale);
      %>
      <%=widgetFactory.createSelectField("BbkCountryImp","LoanRequest.BeneBankCountry", " ", options, isReadOnly,false, false, "class='char35'", "", "")%>



      <%=widgetFactory.createSubsectionHeader("LoanRequest.LoanMaturity", (isReadOnly || isFromExpress),!isTemplate, false, "")%>
      <div class="formItem">
            <%=widgetFactory.createRadioButtonField("LoanMaturityDebitTypeImp","DebitApp","LoanRequest.DebitTheBorrowersAccount", TradePortalConstants.DEBIT_APP, (isImport? loanMaturityDebitType: "").equals(TradePortalConstants.DEBIT_APP),isReadOnly,"onChange=\"clearDebitApplicantsAccount(1);\"" , "")%>
            <%
                  // Using the acct_choices xml from the app party,build the dropdown and 
                  // select the one matching loan_maturity_debit_acct
                  acctNum = StringFunction.xssHtmlToChars(terms.getAttribute("loan_maturity_debit_acct"));

                  appAcctList = StringFunction.xssHtmlToChars(termsPartyApp.getAttribute("acct_choices"));

                  //DocumentHandler acctOptions = new DocumentHandler(appAcctList, true);
                  acctOptions1 = new DocumentHandler(appAcctList, true);
                  options = Dropdown.createSortedAcctOptions(acctOptions1, isImport? acctNum
                              : "", loginLocale);
            %>
            <br/>
            <%=widgetFactory.createSelectField("LoanMaturityDebitAccountImp", "", " ", options, isReadOnly,false, false,"onChange='pickDebitApplicantsAccountRadioImp();'  class='char25'", "", "")%>
            <br/>
            <%=widgetFactory.createRadioButtonField("LoanMaturityDebitTypeImp","DebitOther","LoanRequest.OtherRadio", TradePortalConstants.DEBIT_OTHER, (isImport? loanMaturityDebitType: "").equals(TradePortalConstants.DEBIT_OTHER),isReadOnly, "onChange='clearDebitApplicantsAccount(2);'", "")%>
            <div style="clear: both;"></div>
      </div>
      <div class="formItem">
      <%=widgetFactory.createTextArea("LoanMaturityDebitOtherTextImp","",isImport? (terms.getAttribute("loan_maturity_debit_other_txt")): "", isReadOnly, !isTemplate, false,"onChange=\"pickDebitApplicantsAccountOtherRadioImp();\" style=\"min-height: 60px;\"", "", "")%>
      <%=widgetFactory.createHoverHelp("LoanMaturityDebitOtherTextImp", "LoanMaturityHoverText")%>
      </div>
</div>

<script LANGUAGE="JavaScript">

      <%-- PICK RELATED INSTRUMENT RADIO IF RELATED INSTRUMENT ID IS ENTERED --%>
      function pickRelatedInstrumentRadio() {
          
            if (document.forms[0].RelatedInstrumentID.value > '' || document.forms[0].MultipleRelatedInstrumentsImp.value > '' ) {
                  document.forms[0].LoanProceedsCreditTypeImp[0].checked = true;
            }
      }
      
      <%-- PICK OUR ACCOUNT RADIO IF AN ACCOUNT NUMBER IS SELECTED --%>
      function pickOurAccountRadioImp() {
          
            var index = document.forms[0].LoanProceedsCreditAccountImp.selectedIndex;
      var acct  = document.forms[0].LoanProceedsCreditAccountImp.options[index].value;

            if (acct > '') {
                  document.forms[0].LoanProceedsCreditTypeImp[1].checked = true;
            }
      }

      <%-- PICK THE FOLLOWING BENEFICIARY RADIO IF A BENEFICIARY IS SELECTED --%>
      function pickTheFollowingBeneficiaryRadioImp() {
          
            if (document.TransactionLRQ.BenNameImp.value > ''                 ||
                  document.TransactionLRQ.BenAddressLine1Imp.value > '' ||
                  document.TransactionLRQ.BenAddressLine2Imp.value > '' ||
                  document.TransactionLRQ.BenStateProvinceImp.value > ''      ||
                  document.TransactionLRQ.BenCityImp.value > ''               ||
                  <%-- rkrishna IR-PAUH101945718 11/01/2007 Commented Below line --%>
                  <%-- document.TransactionLRQ.BenPostalCodeImp.value > '' || --%>
                  <%-- rkrishna IR-PAUH101945718 11/01/2007 End--%>
                  document.TransactionLRQ.BenPhoneNumberImp.value > ''  ||
                  document.TransactionLRQ.BenOTLCustomerIdImp.value > ''      ||
                  document.TransactionLRQ.BenCountryImp.value > '') {
                        document.forms[0].LoanProceedsCreditTypeImp[2].checked = true;
            }
      }

      <%-- PICK OTHER (ENTER ...) RADIO IF APPLY LOAN PROCEEDS TO OTHER TEXT BOX IS FILLED OUT --%>
      function pickApplyProceedsToOtherRadioImp() {
          
            if (document.forms[0].LoanProceedsCreditOtherTextImp.value > '') {
                  document.forms[0].LoanProceedsCreditTypeImp[3].checked = true;
            }
      }
      
      function checkBeneficiaryNameImp(originalName) {
      
            if (document.forms[0].BenNameImp.value != originalName)
            {
                  document.forms[0].BenOTLCustomerIdImp.value = "";
            }
      }
      
      function checkBeneficiaryBankNameImp(originalName) {
      
            if (document.forms[0].BbkNameImp.value != originalName)
            {
                  document.forms[0].BbkOTLCustomerIdImp.value = "";
            }
      }
      <%-- rkrishna IR-PAUH101945718 11/01/2007 Add Begin --%>
      function clearBeneficiaryImp() {
      
       document.TransactionLRQ.BenNameImp.value = "";
      document.TransactionLRQ.BenAddressLine1Imp.value = "";
      document.TransactionLRQ.BenAddressLine2Imp.value = "";
      document.TransactionLRQ.BenCityImp.value = "";
      document.TransactionLRQ.BenStateProvinceImp.value = "";
      dijit.byId('BenCountryImp').set('value',"");
      document.TransactionLRQ.BenPostalCodeImp.value = "";
      document.TransactionLRQ.BenOTLCustomerIdImp.value = "";
      document.TransactionLRQ.VendorIdImp.value = ""; 
      
      }
      <%-- rkrishna IR-PAUH101945718  11/01/2007 End --%>
      
      function AddMultipleInstruments1()
      {
    	  require(["dojo/dom","dojo/domReady!" ], function(dom) {           
                  var table = dom.byId('addInstrument');
                   var insertRowIdx = table.rows.length;
                  var row = table.insertRow(insertRowIdx);
            row.id = "addInstrument"+insertRowIdx;
                  <%-- get index# for field naming --%>
                  document.getElementById("noOfInst").value = eval(insertRowIdx+1);
                  
                  var insertRowIdx = table.rows.length;
      
            attachAjaxResult( 
                
                              row.id,"/portal/transactions/AddMultipleInstrument.jsp?addInstrument="+(insertRowIdx-1)) ;
                              
      insertRowIdx = insertRowIdx+1;
      });
      }
      
      function AddMultipleInstruments() {
      <%-- so that user can only add 2 rows --%>
      var tbl = document.getElementById('addInstrument');<%-- to identify the table in which the row will get insert --%>
      var lastElement = tbl.rows.length;
            
            var newRow = tbl.insertRow(lastElement);<%-- creation of new row --%>
            var cell0 = newRow.insertCell(0);<%-- first  cell in the row --%>
            newRow.id = "addInstrRow_"+lastElement;
            j = lastElement-1;
            var html = '<input onChange = \"prepareMultipleInstrString()\" data-dojo-type=\"dijit.form.TextBox\" name=\"multipleInstrumentID'+j+'\" id=\"multipleInstrumentID'+j+'\"  class=\"char16\"  maxLength=\"16\"> <span class="searchButton"><a id=\"InstrumentId\" href=\"javascript:\'#\'\" onClick=\"SearchInstrument(\'multipleInstrumentID%count%\',\'InstrumentId\',\'IMP\');return false;\"></a></span>'
            
            html = html.replace("%count%", j);
            cell0.innerHTML = html;
              require(["dojo/parser"], function(parser) {
            parser.parse(newRow.id);
       });
            
            document.getElementById("noOfInst").value = eval(lastElement); 
      }
      function prepareMultipleInstrString() {
                  var noOfInst = document.getElementById("noOfInst").value;
                  var multInstrStr = "";
                                          
                  for(var i=0; i<noOfInst; i++){
                  multInstrStr = multInstrStr + document.getElementById("multipleInstrumentID"+i).value;
                                                
                        if(i < noOfInst-1){
                                    multInstrStr  = multInstrStr+",";
                        }
                  }
                                          
                  document.forms[0].MultipleRelatedInstrumentsImp.value = multInstrStr;
                  <%-- Pavani - ANZ Issue 783 - IR T36000011250. Changed the below assignment from false to true.  --%>
                  <%-- If not set to true, the Radio button is not retaining its value and getting unchecked after save. --%>
                  document.forms[0].LoanProceedsCreditTypeImp[0].checked=true;
      }
      
      function pickRelatedInstImp(){
    	  dijit.byId("RelatedInst").set('checked',true);
      }
      
      function pickCreditOurAcctTypeImp() {    	   
    	  if (dijit.byId("LoanProceedsCreditAccountImp").getValue() != "" ) { 
	       dijit.byId("CreditOurAcct").set('checked',true);
	    }
    }
    
    function pickDebitApplicantsAccountRadioImp() {    	   
    	  if (dijit.byId("LoanMaturityDebitAccountImp").getValue() != "" ) { 
	       dijit.byId("DebitApp").set('checked',true);
	       document.forms[0].LoanMaturityDebitOtherTextImp.value = "";
	    }
    }
    
    function pickDebitApplicantsAccountOtherRadioImp() {    	   
    	  if (dijit.byId("LoanMaturityDebitOtherTextImp").getValue() != "" ) { 
	       dijit.byId("DebitOther").set('checked',true);
	       document.forms[0].LoanMaturityDebitAccountImp.value = "";
	    }
    }
    
    function clearDebitApplicantsAccount(flag) {  
		    if(flag = 1){
		 		  document.forms[0].LoanMaturityDebitOtherTextImp.value = "";
		    }
		    
		    if(flag = 2){
		 		  document.forms[0].LoanMaturityDebitAccountImp.value = "";
		    }
    }
</script>
