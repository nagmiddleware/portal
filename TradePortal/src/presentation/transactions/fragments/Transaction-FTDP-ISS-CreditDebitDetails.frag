	<%--
 *
 *     Copyright  © 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
*******************************************************************************
                 Domestic Payment Request Page - Debit/Credit Details section

  Description:
    Contains HTML to create Domestic Payment Request Debit/Credit Details section.  

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transaction-FTDP-ISS-CreditDebit.frag" %>
  
  
  IAZ CR-586 Fixed Templates.
  	- DP Text Fileds are now drawn using new method InputField.createTextFieldOrLabel()
  	- If data in DP Dropdowns, Radio Buttons and Text Areas is fixedValue, hidden fileds
  	  are added in addition to labels.
  	- Search and Clear buttons are removed if fixedValue
  	- Transaction data is dtawn using old InputFiled methods but checks applied to
  	  figure out if value is fixed and must be protected
**************************************************************************************
--%>

<%

	// Define Variables
	DocumentHandler enteredDomPmtsDoc = null;
	DocumentHandler enteredDomPmtDoc = null;
	QueryListView queryListView = null;
	StringBuffer updateText = null;
	StringBuffer sqlQuery = new StringBuffer();
	boolean errorFlag = false;
	String corporateOrgOid = null;
	Vector enteredDomPmtList = null;
	String classAttribute = null;
	String defaultValue = null;
	String userLocale = null;
	String dropdownOptions = null;
	boolean isCBFT = false; //IAZ CM CR-507 12/19/09 Add: Cross Border FT Transaction
	boolean isCheckPM = false; //IAZ IR-SSUK012149800 01/27/10
	boolean isFixedValue = false; //IAZ CR-586 IR-VRUK091652765 10/05/10
	String canNotAddPayees = ""; //IAZ CR-586 IR-VRUK091652765 10/11/10   
	StringBuffer newSearchCriteria = new StringBuffer(); //SHILPAR CR-597
	StringBuffer dynamicWhere = new StringBuffer();
	boolean usingOtherCorpsAccnt = false; //AAlubala CR610 2.18 02/26/2011
	String accntOwnerCorporateID = ""; //AAlubala CR610 2.18 02/26/2011   
	String acctBankOrgid = "";

	// IR# PIUL103154077 Rel. 7.1.0 - Begin -
	//Vshah - IR#PUL121367992 - Rel7.1 - 12/15/2011 - <Begin>
	//Commented out the below "if" condition and re-written that.
	//if(userSession.hasAccessToLiveMarketRate() && InstrumentServices.isNotBlank(terms.getAttribute("debit_account_oid"))){
	if (InstrumentServices.isNotBlank(terms
			.getAttribute("debit_account_oid"))) {
		//Vshah - IR#PUL121367992 - Rel7.1 - 12/15/2011 - <End>   
		DocumentHandler resultsDoc = DatabaseQueryBean
				.getXmlResultSet(
						"SELECT g.fx_online_avail_ind, A_BANK_ORG_GROUP_OID  FROM account a, operational_bank_org o, bank_organization_group g WHERE a.a_op_bank_org_oid = o.organization_oid AND o.a_bank_org_group_oid = g.organization_oid and a.account_oid=?",
						false, new Object[]{terms.getAttribute("debit_account_oid")});
		if (resultsDoc != null) {
			//if (isReadOnly) { //IR T36000045461-Commenting this condition so that acctBankOrgOid is fetched under all scenarios.
				acctBankOrgid = resultsDoc
						.getAttribute("/ResultSetRecord(0)/A_BANK_ORG_GROUP_OID");
				acctBankOrgid = EncryptDecrypt
						.encryptStringUsingTripleDes(acctBankOrgid,
								userSession.getSecretKey());
			//}
			if (TradePortalConstants.INDICATOR_YES
					.equals(resultsDoc
							.getAttribute("/ResultSetRecord(0)/FX_ONLINE_AVAIL_IND"))) {
				requestMarketRateVisibility = "visible";
			}
		}

	}
	// IR# PIUL103154077 Rel. 7.1.0 - End -
	// DK CR-640 Rel7.1 Ends
	//VShah - CR564

	if (TradePortalConstants.INDICATOR_YES.equals(transaction
			.getAttribute("uploaded_ind"))) {
		//MDB Rel6.1 IR#PDUL012402000 1/31/11
		if ((TradePortalConstants.TRANS_STATUS_VERIFIED_PENDING_FX
				.equals(transaction.getAttribute("transaction_status")))
				|| (TradePortalConstants.TRANS_STATUS_REJECTED_BY_BANK
						.equals(transaction
								.getAttribute("transaction_status")))
				|| (TradePortalConstants.TRANS_STATUS_VERIFIED
						.equals(transaction
								.getAttribute("transaction_status")))) {
			isStatusVerifiedPendingFX = true;
		}

	}
	//VShah - CR564

	boolean isFXeditable = (TradePortalConstants.TRANS_STATUS_FX_THRESH_EXCEEDED
			.equals(transaction.getAttribute("transaction_status")) || TradePortalConstants.TRANS_STATUS_AUTH_PEND_MARKET_RATE
			.equals(transaction.getAttribute("transaction_status")));

	//IAZ CM-451 01/01/2009 Begin: if funding currency is not set (no account is set), the default
	//is empty string, not space
	String fundingCurrency = "";
	//String fundingCurrency = " ";
	//IAZ CM-451 01/01/2009: End
	String displayPaymentAmount = "0.00";
	String displayFundingAmount = "0.00";

	corporateOrgOid = userSession.getOwnerOrgOid();

	// IAZ: Retrive all domestic payments to be displayed as a table at the bottom 
	// of the screen

	boolean sqlPayeeError = false;

	//NSX CR-564 Rel6.1 01/12/11 Begin
	numberOfDomPmts = DatabaseQueryBean.getCount("domestic_payment_oid","domestic_payment","p_transaction_oid = ?",false, new Object[]{transaction.getAttribute("transaction_oid")});
	//Srinivasu_D IR#T36000042119 Rel9.3 07/28/2015 - Sql j issue
	List<Object> sqlParams = new ArrayList<Object>();
	//Srinivasu_D IR#T36000042119 Rel9.3 07/28/2015 - End
	if (numberOfDomPmts > 0) {
		//BSL IR# PMUL031604858 Rel7.0 03/15/11 Begin
		StringBuffer domPmtQuery = new StringBuffer();
		domPmtQuery
				.append("select d.DOMESTIC_PAYMENT_OID, payee_account_number as AccountNumber, d.SOURCE_TEMPLATE_DP_OID from domestic_payment d");
		//build where clause differently depending on whether a DP is already selected
		if (InstrumentServices.isBlank(currentDPOid)) {
			//get all DPs associated with current Transaction pick the first one in the list
			domPmtQuery
					.append(" where rownum = 1 and d.p_transaction_oid =?");
			sqlParams.add(transaction.getAttribute("transaction_oid"));
		} else {
			//use the OID of the selected DP
			domPmtQuery.append(" where d.DOMESTIC_PAYMENT_OID =?");
			sqlParams.add(currentDPOid);
		}
		domPmtQuery.append(" order by upper(AccountNumber)");
		DocumentHandler resultsDoc = DatabaseQueryBean.getXmlResultSet(
				domPmtQuery.toString(), false, sqlParams);
		//BSL IR# PMUL031604858 Rel7.0 03/15/11 End
		if (resultsDoc != null) {
			currentDPOid = resultsDoc
					.getAttribute("/ResultSetRecord(0)/DOMESTIC_PAYMENT_OID");

			if (InstrumentServices
					.isNotBlank(resultsDoc
							.getAttribute("/ResultSetRecord(0)/SOURCE_TEMPLATE_DP_OID")))
				canNotAddPayees = TradePortalConstants.INDICATOR_YES;
		}
	}

	//NSX CR-564 Rel6.1 01/12/11 End
	if (numberOfDomPmts == 1 && !getDataFromDoc) { //  NSX  - IR# PMUL012565883    Rel. 6.1.0
		try {
			queryListView = (QueryListView) EJBObjectFactory
					.createClientEJB(formMgr.getServerLocation(),
							"QueryListView");

			//userOrgOid = userSession.getOwnerOrgOid();

			sqlQuery.append("select dp.domestic_payment_oid, dp.payee_account_number as payee_account, ");
			sqlQuery.append("dp.payee_name as payee_name, dp.amount as payee_amount, ");
			sqlQuery.append("dp.amount_currency_code as currency_code, ");
			sqlQuery.append("dp.payee_bank_code, dp.payee_bank_name, dp.payee_description ");
			sqlQuery.append(", dp.payment_method_type "); // IAZ CR-483B 08/13/09 Add
			sqlQuery.append(", dp.payee_address_line_1, dp.payee_address_line_2, dp.payee_address_line_3, dp.country ");
			sqlQuery.append(", dp.value_date, customer_reference "); // IAZ CM CR-507 12/19/09, 02/03/10 Add
			sqlQuery.append(", dp.source_template_dp_oid "); // IAZ IR-VRUK091652765 10/11/10 Add      
			sqlQuery.append(", dp.payment_status, dp.sequence_number, dp.payment_system_ref, dp.error_text "); // rkazi CR-596 11/18/2010 Add
			sqlQuery.append("from domestic_payment dp ");
			sqlQuery.append("where dp.p_transaction_oid = ?");
			sqlQuery.append(" order by payee_account");

			queryListView.setSQL(sqlQuery.toString(), new Object[]{transaction.getAttribute("transaction_oid")});
			queryListView.getRecords();

			numberOfDomPmts = queryListView.getRecordCount();

			enteredDomPmtsDoc = queryListView.getXmlResultSet();

			enteredDomPmtList = enteredDomPmtsDoc
					.getFragments("/ResultSetRecord");

		} catch (Exception e) {
			sqlPayeeError = true;
			e.printStackTrace();

		}

		finally {
			try {
				if (queryListView != null) {
					queryListView.remove();
				}
			} catch (Exception e) {
				System.out
						.println("Error removing QueryListView in -CreditDebitDetails.frag!");

			}
		}
	} //NSX  - IR# PMUL012565883    Rel. 6.1.0
		//End Retrive all domestic payments list 

	//Make sure to load a Payee to a Credit section upon successfull Verify so screen does not appear empty
	if ((numberOfDomPmts >= 1)
			&& (isReadOnly || isStatusVerifiedPendingFX)) {
		if (InstrumentServices.isNotBlank(currentDPOid)) //NSX CR-564 Rel6.1 01/12/11 Begin
		{
			//enteredDomPmtDoc = (DocumentHandler) enteredDomPmtList.elementAt(0);  //NSX CR-564 Rel6.1 01/12/11 
			//	currentDPOid = enteredDomPmtDoc.getAttribute("/DOMESTIC_PAYMENT_OID");  //NSX CR-564 Rel6.1 01/12/11 
			currentDomesticPayment.setAttribute("domestic_payment_oid",
					currentDPOid);
			currentDomesticPayment.getDataFromAppServer();

		}

		//IAZ CM CR-507 12/19/09 Begin
		//Populate additional Parties, if needed. 

		if (InstrumentServices.isNotBlank(currentDomesticPayment
				.getAttribute("c_FirstIntermediaryBank"))) {
			firstIntBank.setAttribute("payment_party_oid",
					currentDomesticPayment
							.getAttribute("c_FirstIntermediaryBank"));
			//out.println(currentDomesticPayment.getAttribute("c_FirstIntermediaryBank") + currentDomesticPayment.getAttribute("domestic_payment_oid"));
			firstIntBank.getDataFromAppServer();
		}

		//	if (InstrumentServices.isNotBlank(currentDomesticPayment.getAttribute("c_SecondIntermediaryBank")))
		//	{
		//		secondIntBank.setAttribute("payment_party_oid", currentDomesticPayment.getAttribute("c_SecondIntermediaryBank"));
		//out.println(currentDomesticPayment.getAttribute("c_SecondIntermediaryBank") + currentDomesticPayment.getAttribute("domestic_payment_oid"));
		//		secondIntBank.getDataFromAppServer();
		//	}

		if (InstrumentServices.isNotBlank(terms
				.getAttribute("c_OrderingParty"))) {
			orderingParty.setAttribute("payment_party_oid",
					terms.getAttribute("c_OrderingParty"));
			orderingParty.getDataFromAppServer();
		}

		//	if (InstrumentServices.isNotBlank(terms.getAttribute("c_OrderingPartyBank")))
		//	{
		//		orderingPartyBank.setAttribute("payment_party_oid", terms.getAttribute("c_OrderingPartyBank"));
		//		orderingPartyBank.getDataFromAppServer();
		//	}
		//IAZ CM CR-507 12/19/09 End

		//BSL IR#PUL032965444 04/04/11 Begin
		// Get invoice details for current payment from DB
		//RKAZI IR#VSUL081045430 Rel 8.1 09/25/2012 - Start
		if ((currentDPOid != null) && (!currentDPOid.equals(""))) {
			//RKAZI IR#VSUL081045430 Rel 8.1 09/25/2012 - End
			StringBuffer invDetROSql = new StringBuffer();
			invDetROSql
					.append("SELECT INVOICE_DETAIL_OID FROM INVOICE_DETAILS WHERE DOMESTIC_PAYMENT_OID = ?");

			DocumentHandler invDetROOidDoc = DatabaseQueryBean
					.getXmlResultSet(invDetROSql.toString(), false, new Object[]{currentDPOid});
			if (invDetROOidDoc != null) {
				invoiceDetailOid = invDetROOidDoc
						.getAttribute("/ResultSetRecord/INVOICE_DETAIL_OID");
				if (InstrumentServices.isNotBlank(invoiceDetailOid)) {
					paymentInvoiceDetails.setAttribute(
							"invoice_detail_oid", invoiceDetailOid);
					paymentInvoiceDetails.getDataFromAppServer();
				}
			}
		} //RKAZI IR#VSUL081045430 Rel 8.1 09/25/2012 - If ends
			//BSL IR#PUL032965444 04/04/11 End
	}

	// IAZ: We retrive this amount to handle "corner cases"
	//                  such as no domestic payments are entered
	//                  However, this value will be recalculated to
	//                  be equal the sum of all dom pmt amounts

	String fundingAmount = terms.getAttribute("funding_amount");
	if ((fundingAmount == null) || (fundingAmount.equals(""))) {
		fundingAmount = "0";
	}

	// Always send the payee terms party oid
	secureParms.put("payee_terms_party_oid",
			termsPartyPayee.getAttribute("terms_party_oid"));
	// Always send the payee_bank terms party oid
	secureParms.put("payee_bank_terms_party_oid",
			termsPartyPayeeBank.getAttribute("terms_party_oid"));
	// Always send the payee_bank terms party oid
	secureParms.put("payer_terms_party_oid",
			termsPartyPayer.getAttribute("terms_party_oid"));
	//out.println("IAZDebug:: " + termsPartyPayer.getAttribute("terms_party_oid") + " " + termsPartyPayer.getAttribute("terms_party_type"));                 
	//out.println("IAZDebug:: " + termsPartyPayeeBank.getAttribute("terms_party_oid") + " " + termsPartyPayeeBank.getAttribute("terms_party_type"));
	//out.println("IAZDebug:: " + termsPartyPayee.getAttribute("terms_party_oid") + " " + termsPartyPayee.getAttribute("terms_party_type"));

	//IAZ: get payee accout number from current payee, if loaded     
	payeeAcctNum = currentDomesticPayment
			.getAttribute("payee_account_number");
	if (payeeAcctNum == null) {
		payeeAcctNum = "";
	}

	String bankChargesType = currentDomesticPayment
			.getAttribute("bank_charges_type");

	//Calculate amounts and determine accounts/currencies to be used later

	// Using the acct_choices xml from the payer party,build the dropdown and 
	// select the one matching debit_account_oid
	acctOid = StringFunction.xssHtmlToChars(terms
			.getAttribute("debit_account_oid"));

	//IAZ: For the available accounts dropdown, we only bring up those
	//     that are set to be available for domestic_pymts and for debits

	//Debug.debug("IAZDebug: Dom Pmt Page Funding accNo: " + acctNum + "<br>");        
	//Debug.debug("IAZDebug: Dom Pmt Page Account choises: " + termsPartyPayer.getAttribute("acct_choices") + "<br>");

	//IAZ CR-511 11/09/09 Begin
	Vector availabilityChecks = new Vector(2);
	/*IR 16929 - show deactivated account for instruments which are in "processed by bank" status*/
	if (!isReadOnly) {
		availabilityChecks.add("not deactivate_indicator");
	}
	availabilityChecks.add("available_for_domestic_pymts");
	//RKAZI HAUL062753546 06/27/2011 Start
	String allowPayByAnotherAccount = CBCResult
			.getAttribute("/ResultSetRecord(0)/ALLOW_PAY_BY_ANOTHER_ACCNT");
	if (TradePortalConstants.INDICATOR_NO
			.equals(allowPayByAnotherAccount)) {
		availabilityChecks.add("!othercorp_customer_indicator");
	}
	//RKAZI HAUL062753546 06/27/2011 End

	//IAZ CR-511 11/09/09 End

	String userCheck = null;
	if (isBankUser) {
		userCheck = null;
	} else {
		userCheck = userSession.getUserOid();
	}
	termsPartyPayer.loadAcctChoices(corporateOrgOid,
			formMgr.getServerLocation(), resMgr, availabilityChecks,
			userCheck);
	payerAcctList = StringFunction.xssHtmlToChars(termsPartyPayer
			.getAttribute("acct_choices"));

	//Debug.debug("IAZDebug: Dom Pmt Page Account list is " + payerAcctList + "<br>");

	DocumentHandler acctOptions = new DocumentHandler(payerAcctList,
			true);
	//options = Dropdown.createSortedAcctOptionsOid(acctOptions, acctOid, loginLocale);	//IAZ CR-483B 08/13/09 Moved 

	// IAZ: We will get Funding Currency from the Details of the Funding Account 
	// selected from the Available Account's drop-down list.
	// Selected Payee's payment amount and total amount comes from the Domestic Payment data and it
	// will be formated using transaction/payment currency which is the same 
	// for all payees (but can be different form the funding currency) 

	if (acctOid != null && !acctOid.equals("")) {
		Vector vector = acctOptions
				.getFragments("/DocRoot/ResultSetRecord");
		int numItems = vector.size();

		for (int i = 0; i < numItems; i++) {
			DocumentHandler document = (DocumentHandler) vector
					.elementAt(i);
			String oid = document.getAttribute("/ACCOUNT_OID");
			if (acctOid.equals(oid)) {
				fundingCurrency = document.getAttribute("/CURRENCY");
				break;
			}
		}
	}

	//IAZ CR-483B 08/13/09 Begin
	String transactionCCY = terms.getAttribute("amount_currency_code");
	String transactionCCYDisplay = transactionCCY;
	/*if (InstrumentServices.isBlank(transactionCCY)) {
	   if (InstrumentServices.isBlank(fundingCurrency))
	   		transactionCCYDisplay = "  ";
	   else {
	   		transactionCCY = fundingCurrency;
	   		transactionCCYDisplay = fundingCurrency;
	   }
	}*/
	String transactionAmount = terms.getAttribute("amount");
	//IAZ CR-483B 08/13/09 End 

	String paymentAmount = currentDomesticPayment
			.getAttribute("amount");
	String paymentCurrency = currentDomesticPayment
			.getAttribute("amount_currency_code");

	//IAZ CR-483B 08/13/09 Begin
	//displayPaymentAmount = TPCurrencyUtility.getDisplayAmount(paymentAmount, fundingCurrency, loginLocale); 
	displayPaymentAmount = TPCurrencyUtility.getDisplayAmount(
			paymentAmount, transactionCCY, loginLocale);
	if (getDataFromDoc
			&& maxError != null
			&& maxError.compareTo(String
					.valueOf(ErrorManager.WARNING_ERROR_SEVERITY)) > 0)
		displayPaymentAmount = paymentAmount;
	String displayTransactionAmount = TPCurrencyUtility
			.getDisplayAmount(transactionAmount, transactionCCY,
					loginLocale);
	//IAZ CR-483B 08/13/09 End
	displayFundingAmount = TPCurrencyUtility.getDisplayAmount(
			fundingAmount, fundingCurrency, loginLocale);
	//rkazi CR-596 11/18/2010 Begin

	//NSX CR-564 Rel6.1 01/12/11 Begin
	String processedAmount = "";
	// only execute if transaction is in transaction processing, Rejected Payment or Partially Rejected Payment status
	String transStatus = transaction.getAttribute("transaction_status");
	boolean showProcessedAmount = TransactionStatus.TRANSACTION_PROCESSING.equals(transStatus)
			|| TradePortalConstants.TRANS_STATUS_REJECTED_PAYMENT
					.equals(transStatus)
			|| TradePortalConstants.TRANS_STATUS_PARTIALLY_REJECTED_PAYMENT
					.equals(transStatus);
	if (showProcessedAmount) {

		/*
		DocumentHandler processedAmountDoc = null; 
		
		
		for (int j = 0; j < numberOfDomPmts; j++)
		{
		  processedAmountDoc = (DocumentHandler) enteredDomPmtList.elementAt(j);
		  BigDecimal pmtValue = new BigDecimal(processedAmountDoc.getAttribute("/PAYEE_AMOUNT"));
		  if (TradePortalConstants.BENEFICIARY_PAYMENT_STATUS_PROCESSED_BY_BANK.equals(processedAmountDoc.getAttribute("/PAYMENT_STATUS")) || 
		        TradePortalConstants.BENEFICIARY_PAYMENT_STATUS_COMPLETE.equals(processedAmountDoc.getAttribute("/PAYMENT_STATUS"))){
		        processedPmtValue = processedPmtValue.add(pmtValue);
		 }
		  
		}
		 */
		String whereClause = "p_transaction_oid = ? "
				+ " and (PAYMENT_STATUS='"
				+ TradePortalConstants.BENEFICIARY_PAYMENT_STATUS_PROCESSED_BY_BANK
				+ "' or PAYMENT_STATUS='"
				+ TradePortalConstants.BENEFICIARY_PAYMENT_STATUS_COMPLETE
				+ "')";
		double sum = DatabaseQueryBean.getSum("AMOUNT",
				"domestic_payment", whereClause, false, new Object[]{transaction.getAttribute("transaction_oid")});
		BigDecimal processedPmtValue = new BigDecimal("" + sum); //BSL Rel 6.1 IR #VEUL031445617 03/23/11 - use String constructor of BigDecimal

		//processedAmount = processedPmtValue.toString();

		//if (processedAmount != null){
		processedAmount = TPCurrencyUtility.getDisplayAmount(
				processedPmtValue.toString(), fundingCurrency,
				loginLocale);
		///}
		//else{
		//	processedAmount = "";
		//}
		/*
		String rejectedAmount = terms.getAttribute("rejected_amount");
		if (rejectedAmount != null){
			rejectedAmount = TPCurrencyUtility.getDisplayAmount(rejectedAmount, fundingCurrency, loginLocale);
		}
		else{
			rejectedAmount = "";
		}
		 */
		//NSX CR-564 Rel6.1 01/12/11 End
	}
	//rkazi CR-596 11/18/2010 End      
	String selectedInstrument = transaction
			.getAttribute("transaction_oid")
			+ "/"
			+ instrument.getAttribute("instrument_oid")
			+ "/"
			+ transactionType;
	selectedInstrument = EncryptDecrypt.encryptStringUsingTripleDes(
			selectedInstrument, userSession.getSecretKey());
%>
<%-- Srini_D IR# VSUL081045430 Rel 8.1 08/20/2012  Start --%>
<%
	// we need to retrieve invoice details for the given domestic_payment_oid
	if ((currentDPOid != null) && (!currentDPOid.equals(""))) {
		// Get invoice details from DB if cached doc does not have correct details for current payment
		String dpOid = currentDomesticPayment
				.getAttribute("domestic_payment_oid");

		StringBuffer invDetSql = new StringBuffer();
		invDetSql
				.append("SELECT INVOICE_DETAIL_OID FROM INVOICE_DETAILS WHERE DOMESTIC_PAYMENT_OID = ?");
		String tempDPOid = InstrumentServices.isNotBlank(dpOid) ? dpOid	: null;
		//RKAZI IR#VSUL081045430 Rel 8.1 09/25/2012 - used oid from current domesticpayment

		DocumentHandler invDetOidDoc = DatabaseQueryBean
				.getXmlResultSet(invDetSql.toString(), false, new Object[]{tempDPOid});
		if (invDetOidDoc != null) {
			invoiceDetailOid = invDetOidDoc
					.getAttribute("/ResultSetRecord/INVOICE_DETAIL_OID");
			if (InstrumentServices.isNotBlank(invoiceDetailOid)
					&& !invoiceDetailOid.equals(cachedInvoiceDetailOid)) {
				paymentInvoiceDetails.setAttribute(
						"invoice_detail_oid", invoiceDetailOid);
				paymentInvoiceDetails.getDataFromAppServer();

			}
		}

		//RKAZI 09/20/2012 Rel 8.1 IR RBUM091942080 - Start - Commented this code as it causes issues with adding new
		// Beneficiaries.

		//	if (InstrumentServices.isNotBlank(currentDPOid))    
		//   		{
		//   				currentDomesticPayment.setAttribute("domestic_payment_oid", currentDPOid);
		//    	    	currentDomesticPayment.getDataFromAppServer();
		//				
		//    	}   
		//RKAZI 09/20/2012 Rel 8.1 IR RBUM091942080 - End

	}
%>
<%-- Srini_D IR# VSUL081045430 Rel 8.1 08/20/2012  End --%>
<%-- // BSL CR-655 03/03/11 Begin --%>
<%
	String tranOid = transaction.getAttribute("transaction_oid");
	String[] reportingCodeNames = { "reporting_code_1",
			"reporting_code_2" };
	String[] selectedRptgCodes = new String[TradePortalConstants.MAX_REP_CODE_IDX];
	boolean[] isReadOnlyRptgCode = new boolean[TradePortalConstants.MAX_REP_CODE_IDX];
	for (int i = 0; i < reportingCodeNames.length; i++) {
		String selectedRptgCode = currentDomesticPayment
				.getAttribute(reportingCodeNames[i]);
		selectedRptgCodes[i] = InstrumentServices
				.isBlank(selectedRptgCode) ? "" : selectedRptgCode;
		isReadOnlyRptgCode[i] = isReadOnly
				|| isStatusVerifiedPendingFX
				|| currentDomesticPayment.isFixedValue(
						reportingCodeNames[i], isTemplate);
	}

	//Get list of Bank Org Group OIDs associated with each available debit account and 
	//store in a hidden <select> so it can be used with payment reporting code javascript.
	// IR-45609 
	String acctBankGroupSql = "select account_oid, a_bank_org_group_oid from operational_bank_org inner join account on organization_oid = a_op_bank_org_oid where available_for_domestic_pymts = 'Y' and  p_owner_oid = ?"; 
	// IR# PIUL103154077 Rel. 7.1.0 - 
	DocumentHandler acctBankGroupList = DatabaseQueryBean
			.getXmlResultSet(acctBankGroupSql.toString(), false, new Object[]{corporateOrgOid});
%>
<div style="display: none">
	<div id="bankGroupNonVis"
		style="visibility: hidden; position: absolute;"></div>
	<%
		String acctBankGroupOptions = ListBox.createOptionList(
				acctBankGroupList, "ACCOUNT_OID", "A_BANK_ORG_GROUP_OID",
				acctOid);
		out.println(InputField.createSelectField("acctBankGroupSelection",
				"", " ", acctBankGroupOptions, "ListText", isReadOnly));
		// out.println(widgetFactory.createSelectField("acctBankGroupSelection", "", 
		//		" ", acctBankGroupOptions,  isReadOnly, false, false, "", "", ""));

		String bankGroupOid = "";
		// extract the bankGroupOid value from the selected option
		int abgOptSelectedIdx = acctBankGroupOptions.indexOf("selected");
		if (abgOptSelectedIdx > -1) {
			int abgOptStart = acctBankGroupOptions.indexOf(">",
					abgOptSelectedIdx);
			int abgOptEnd = acctBankGroupOptions.indexOf("</option>",
					abgOptSelectedIdx);

			try {
				bankGroupOid = acctBankGroupOptions.substring(
						abgOptStart + 1, abgOptEnd);
			} catch (Exception e) {
				bankGroupOid = "";
			}
		}
	%>
</div>

<%@ include file="Transaction-FTDP-ISS-GetReportingCodes.frag"%>
<%-- // BSL CR-655 03/03/11 End --%>



<%-- IAZ: we need to preserve at least one attribute of the payer besides oid on the page
     otherise mediator will remove payer reference from the terms record 
            we also want to preserve at leats one attribute of payee which was originally 
     created form template --%>
<div style="display: none">
	<input type=hidden name=PayerName
		value='<%=termsPartyPayer.getAttribute("name")%>'>
</div>
<%
	String firstPartyOid = terms.getAttribute("c_FirstTermsParty");

	//IAZ CM-451 01/29/09 Begin
	//	In case of a single Payee, store this Payees' name into the Terms object
	//	If no payees, no Payee data should be stored with Terms
	//	If there are more then 1 Payee, store constant MULTIPLE_PAYEES_ENTERED to Terms
	//	Note: by the time of Authorize, this value will ALWAYS be set to either a single Payee name
	//	or MULTIPLE_PAYEES_ENTERED constant value because DP transaction cannot be verified
	//	if there is less then 1 Payee.  This will also ensure a creation of FirstTermsParty (Payee)
	//	record in the database
	//if ((firstPartyOid != null) && (!firstPartyOid.equals("")))
	//{
	//out.println("]"+firstPartyOid+"[");
	if (numberOfDomPmts > 1) {
%>
<div style="display: none">
	<input type=hidden name="testDebugForstPartyOid"
		value=<%=firstPartyOid%>> <input type=hidden
		name="ContactName"
		value='<%=StringFunction.escapeQuotesforJS(resMgr.getText("Common.MultBenesEntered",
						TradePortalConstants.TEXT_BUNDLE))%>'>
	<input type=hidden name="PayeeTermsPartyName"
		value='<%=StringFunction.escapeQuotesforJS(resMgr.getText("Common.MultBenesEntered",
						TradePortalConstants.TEXT_BUNDLE))%>'>
	<input type=hidden name="FirstTermsPartyAddress1" value=""> <input
		type=hidden name="FirstTermsPartyAddress2" value=""> <input
		type=hidden name="FirstTermsPartyAddress3" value=""> <input
		type=hidden name="FirstTermsPartyCountry" value="">
</div>
<%
	terms.setAttribute("reference_number", StringFunction.escapeQuotesforJS(resMgr.getText(
				"Common.MultReferences",
				TradePortalConstants.TEXT_BUNDLE)));
%>
<%
	}
	if (numberOfDomPmts == 1) {
		String payeeNameToUse = "", payeeAdd1 = "", payeeAdd2 = "", payeeAdd3 = "", payeeCountry = "", payeeRefNum = "";
		if (getDataFromDoc) {
			
			payeeNameToUse = currentDomesticPayment
					.getAttribute("payee_name");
			payeeAdd1 = currentDomesticPayment
					.getAttribute("payee_address_line_1");
			payeeAdd2 = currentDomesticPayment
					.getAttribute("payee_address_line_2");
			payeeAdd3 = currentDomesticPayment
					.getAttribute("payee_address_line_3");
			payeeCountry = currentDomesticPayment
					.getAttribute("country");
			payeeRefNum = currentDomesticPayment
					.getAttribute("customer_reference");
			terms.setAttribute("reference_number",	payeeRefNum);
		}

		else {
			
			payeeNameToUse = ((DocumentHandler) enteredDomPmtList
					.elementAt(0)).getAttribute("/PAYEE_NAME");
			payeeAdd1 = ((DocumentHandler) enteredDomPmtList
					.elementAt(0))
					.getAttribute("/PAYEE_ADDRESS_LINE_1");
			payeeAdd2 = ((DocumentHandler) enteredDomPmtList
					.elementAt(0))
					.getAttribute("/PAYEE_ADDRESS_LINE_2");
			payeeAdd3 = ((DocumentHandler) enteredDomPmtList
					.elementAt(0))
					.getAttribute("/PAYEE_ADDRESS_LINE_3");
			payeeCountry = ((DocumentHandler) enteredDomPmtList
					.elementAt(0)).getAttribute("/COUNTRY");
			payeeRefNum = ((DocumentHandler) enteredDomPmtList
					.elementAt(0)).getAttribute("/CUSTOMER_REFERENCE");
			terms.setAttribute("reference_number",
					StringFunction.xssCharsToHtml(payeeRefNum));
			
		}

		// NShrestha - 07/08/2009 - IR#RRUJ061750762 - Begin --
		//Ravindra - Rel710 - IR DEUK060962137 - 25th Aug 2011 - Start
		/* Rel 8400 @ KMehta uncommented the followingg line for fix of IR-T36000023695/13072 */
	// MEer IR-36836 escape is required when getting values from database  
		if(!getDataFromDoc){
		payeeNameToUse = StringFunction.xssCharsToHtml(payeeNameToUse);
		payeeAdd1 = StringFunction.xssCharsToHtml(payeeAdd1);
		payeeAdd2 = StringFunction.xssCharsToHtml(payeeAdd2);
		payeeAdd3 = StringFunction.xssCharsToHtml(payeeAdd3);
		payeeCountry = StringFunction.xssCharsToHtml(payeeCountry);	
		}
		
		if ((InstrumentServices.isNotBlank(payeeNameToUse))
				&& (payeeNameToUse.length() > 35))
			payeeNameToUse = payeeNameToUse.substring(0, 35);
		payeeNameToUse = StringFunction.xssCharsToHtml(payeeNameToUse);
		//Ravindra - Rel710 - IR DEUK060962137 - 25th Aug 2011 - End
		// NShrestha - 07/08/2009 - IR#RRUJ061750762 - End --
		
%>
<div style="display: none">
	<input type=hidden name="testDebugForstPartyOid"
		value=<%=firstPartyOid%>> <input type=hidden
		name="ContactName" value="<%=payeeNameToUse%>"> <input
		type=hidden name="PayeeTermsPartyName" value="<%=payeeNameToUse%>">
	<input type=hidden name="FirstTermsPartyAddress1"
		value="<%=payeeAdd1%>"> <input type=hidden
		name="FirstTermsPartyAddress2" value="<%=payeeAdd2%>"> <input
		type=hidden name="FirstTermsPartyAddress3" value="<%=payeeAdd3%>">
	<input type=hidden name="FirstTermsPartyCountry"
		value="<%=payeeCountry%>">
</div>
<%
	}
	if (numberOfDomPmts == 0) {
		String payeeNameToUse, payeeAdd1, payeeAdd2, payeeAdd3, payeeCountry;
		
		if (getDataFromDoc) {
			payeeNameToUse = currentDomesticPayment
					.getAttribute("payee_name");
			payeeAdd1 = currentDomesticPayment
					.getAttribute("payee_address_line_1");
			payeeAdd2 = currentDomesticPayment
					.getAttribute("payee_address_line_2");
			payeeAdd3 = currentDomesticPayment
					.getAttribute("payee_address_line_3");
			payeeCountry = currentDomesticPayment
					.getAttribute("country");
			// NShrestha - 07/08/2009 - IR#RRUJ061750762 - Begin --
			//Ravindra - Rel710 - IR DEUK060962137 - 25th Aug 2011 - Start
			/* Rel 8400 @ KMehta uncommented the followingg line for fix of IR-T36000023695/13072 */
			//payeeNameToUse = StringFunction
			//		.xssCharsToHtml(payeeNameToUse);
			//Ravindra - Rel710 - IR DEUK060962137 - 25th Aug 2011 - End
			// NShrestha - 07/08/2009 - IR#RRUJ061750762 - End --
			//payeeAdd1 = StringFunction.xssCharsToHtml(payeeAdd1);
		//	payeeAdd2 = StringFunction.xssCharsToHtml(payeeAdd2);
		//	payeeAdd3 = StringFunction.xssCharsToHtml(payeeAdd3);
		//	payeeCountry = StringFunction.xssCharsToHtml(payeeCountry);
			terms.setAttribute("reference_number", currentDomesticPayment
							.getAttribute("customer_reference"));

		} else {
			payeeNameToUse = "";
			payeeAdd1 = "";
			payeeAdd2 = "";
			payeeAdd3 = "";
			payeeCountry = "";
			terms.setAttribute("reference_number", "");
		}
		if ((InstrumentServices.isNotBlank(payeeNameToUse))
				&& (payeeNameToUse.length() > 35))
			payeeNameToUse = payeeNameToUse.substring(0, 35);
		//Ravindra - Rel710 - IR DEUK060962137 - 25th Aug 2011 - Start
		payeeNameToUse = StringFunction.xssCharsToHtml(payeeNameToUse);
		//Ravindra - Rel710 - IR DEUK060962137 - 25th Aug 2011 - End
%>
<div style="display: none">
	<input type=hidden name="testDebugForstPartyOid"
		value=<%=firstPartyOid%>> <input type=hidden
		name="ContactName" value="<%=payeeNameToUse%>"> <input
		type=hidden name="PayeeTermsPartyName" value="<%=payeeNameToUse%>">
	<input type=hidden name="FirstTermsPartyAddress1"
		value="<%=payeeAdd1%>"> <input type=hidden
		name="FirstTermsPartyAddress2" value="<%=payeeAdd2%>"> <input
		type=hidden name="FirstTermsPartyAddress3" value="<%=payeeAdd3%>">
	<input type=hidden name="FirstTermsPartyCountry"
		value="<%=payeeCountry%>">
</div>
<%
	}

	//}
	//IAZ CM-451 01/29/09 End
%>

<%%>
<%-- Div for Title Pane 1. Terms start --%>

<%=widgetFactory.createSectionHeader("1",
					"DomesticPaymentRequest.Terms")%>
<div class="columnLeft">
	<%-- columnLeft Start --%>

	<%=widgetFactory.createSubsectionHeader(
					"DomesticPaymentRequest.StatementTextToPayee", false,
					false, false, "")%>

	<%
		if ((!isTemplate) && (!isReadOnly)) {
			String confType = resMgr.getText(
					"TemplateDetail.ConfidentialPmtLabel",
					TradePortalConstants.TEXT_BUNDLE);

			if (!isTemplate) {
	%>
	<%@ include file="TransactionConfInd.frag"%>
	<%
		}

		} else {
	%>
	<%
		String confType = resMgr.getText(
					"TemplateDetail.ConfidentialPmtLabel",
					TradePortalConstants.TEXT_BUNDLE);

			if (!isTemplate) {
	%>
	<%@ include file="TransactionConfInd.frag"%>
	<%
		}
		}
	%>
	<%-- Jan 07 2013 @Komal #Moved the code from TemplateHEader.frag to this frag; as per UI modifications--%>
	<%
		if (isTemplate) {
	%>
	<%
		if ((InstrumentType.DOM_PMT.equals(instrument
					.getAttribute("instrument_type_code")))
					|| (InstrumentType.XFER_BET_ACCTS
							.equals(instrument
									.getAttribute("instrument_type_code")))
					|| (InstrumentType.FUNDS_XFER.equals(instrument
							.getAttribute("instrument_type_code")))) {
				UserWebBean thisUser = beanMgr.createBean(UserWebBean.class, "User");
				thisUser.setAttribute("user_oid", userSession.getUserOid());
				thisUser.getDataFromAppServer();
				String confInd = "";
				if (TradePortalConstants.ADMIN.equals(userSession
						.getSecurityType()))
					confInd = TradePortalConstants.INDICATOR_YES;
				else if (userSession.getSavedUserSession() == null)
					confInd = thisUser
							.getAttribute("confidential_indicator");
				else {
					if (TradePortalConstants.ADMIN.equals(userSession
							.getSavedUserSession().getSecurityType()))
						confInd = TradePortalConstants.INDICATOR_YES;
					else
						confInd = thisUser
								.getAttribute("subsid_confidential_indicator");
				}
				if (TradePortalConstants.INDICATOR_YES.equals(confInd)) {
					//IAZ CR-586 IR-PRUK092452162 09/29/10: CONF IND for TEMPLATES is T (vs Y).
					//           This fix is for templates created from Instruments (so Y is copied over and needs to be replaced by N in Terms)
					if (TradePortalConstants.INDICATOR_YES.equals(terms
							.getAttribute("confidential_indicator")))
						terms.setAttribute("confidential_indicator",
								TradePortalConstants.INDICATOR_NO);
	%>
	<%=widgetFactory
								.createCheckboxField(
										"ConfidentialIndicatorBox",
										"TemplateDetail.ConfidentialPmtLabel",
										terms.getAttribute(
												"confidential_indicator")
												.equals(TradePortalConstants.CONF_IND_FRM_TMPLT),
										isReadOnly, false,
										"onClick=setConfidentialIndicator()",
										"", "")%>

	<input type=hidden name=ConfidentialIndicator
		value=<%=terms.getAttribute("confidential_indicator")%>>
	<div id="Confidential Payment">
		<input type=hidden name=ConfidentialIndicatorTemplate
			value=<%=terms.getAttribute("confidential_indicator")%>>

	</div>
	<%
		}
			}
	%>
	<%
		}
	%>
	<%-- Jan 07 2013 @Komal #Moved the code from TemplateHEader.frag to this frag; as per UI modifications ENDS--%>
	<%--cquinton 8/28/2012 pass in read only flag to execution date field--%>

	<%--T36000015518 even when isStatusVerifiedPendingFX is true, the payment date should be editable, hence dont pass to widget factory --%>

<%-- KMehta @ Rel 8400 IR T36000026081 added condition for Fixed Payment Start --%>
	<%-- <%=widgetFactory.createDateField("payment_date",
					"DomesticPaymentRequest.ExecutionDate",
					transaction.getAttribute("payment_date"), isReadOnly||isFixedPayment,
					!isTemplate, false,
					"onChange='updateDMPaymentDate()'; constraints={datePattern:'"
							+ datePattern + "'}", "placeHolder:'"
							+ datePatternDisplay + "'", "")%> --%>
	<%-- Nar CR 966 Rel 9.2 09/16/2014 modified widget to make it read-Only if it is template --%> 
	<% //Rel 9.5 IR-T36000047192 
		String tempPaymentDate = transaction.getAttribute("payment_date");
		if (getDataFromDoc) {
			tempPaymentDate = doc.getAttribute("/In/Transaction/payment_date");
		}
	%>
	<%= widgetFactory.createDateField( "payment_date", "DomesticPaymentRequest.ExecutionDate", StringFunction.xssHtmlToChars(tempPaymentDate),
		isReadOnly||isTemplate, !isTemplate, false, "onChange='updateDMPaymentDate()';", dateWidgetOptions, "")%>							
<%-- KMehta @ Rel 8400 IR T36000026081 added condition for Fixed Payment End --%>
	<%
		//IR T36000019732 RPasupulati.
		String AppDebitChangeAction = "class='char30'; onChange='updatePayerAccount();'";
		/*String fixedFlag = instrument.getAttribute("fixed_payment_flag");
		if (fixedFlag == null || fixedFlag.equals("")) {
			fixedFlag = TradePortalConstants.INDICATOR_NO;
		}
		if (!fixedFlag.equals(TradePortalConstants.INDICATOR_YES)) {*/
			defaultText = resMgr.getText(
					"UserDetail.selectAccountNumberCCY",
					TradePortalConstants.TEXT_BUNDLE);
	/*	} else {*/
			
			AppDebitChangeAction = "class='char30'; onChange='updatePayerAccount();'";
	/*	} */
		isFixedValue = false;
		//BSL CR-655 02/28/11 - changed onChange func from updatePayerAccCurrency

		if (isFixedPmt || isStatusVerifiedPendingFX  ) {
			if (isStatusVerifiedPendingFX
					|| (InstrumentServices.isNotBlank(terms
							.getAttribute("debit_account_oid")) && InstrumentServices
							.isNotBlank(aSTTerms
									.getAttribute("debit_account_oid")))) {
				AppDebitChangeAction = "disabled";
				out.println("<input type=hidden name=AppDebitAcctPrincipal value=\""
						+ terms.getAttribute("debit_account_oid") + "\">");
			}
		}
		
		options = Dropdown.createSortedAcctOptionsOid(acctOptions, acctOid,
				loginLocale);
		if (!isReadOnly) {		
	%>

	<%=widgetFactory.createSelectField(
						"AppDebitAcctPrincipal",
						"DomesticPaymentRequest.DebitNumberAndCurrency",
						defaultText, options, isReadOnly
								|| isStatusVerifiedPendingFX ||isDebitAcctFixedVal, !isTemplate,
						false, AppDebitChangeAction, "", "inline")%>
						<%if(isFixedPayment){%><div style='clear:both;'></div><%}%>
	<%
		} else {

			int select = options.indexOf("selected");
			String textValue = "";

			if (select > -1) {
				int start = options.indexOf(">", select);
				int end = options.indexOf("</option>", select);

				try {
					textValue = options.substring(start + 1, end);
				} catch (Exception e) {
					textValue = "";
				}
			}

			if (textValue.equals(""))
				textValue = "&nbsp;";
	%>
	<div class=formItem>
		<label for="appDebitAcctLabel"><%=resMgr.getText(
						"DomesticPaymentRequest.DebitNumberAndCurrency",
						TradePortalConstants.TEXT_BUNDLE)%></label><br>
		<span id="AppDebitAcctPrincipal1"> <b><%=textValue%></b>
		</span>
	</div>
	<%
		}
		if(isDebitAcctFixedVal){
		out.println("<input type=hidden name=AppDebitAcctPrincipal id='AppDebitAcctPrincipalFixed' value=\""
						+ terms.getAttribute("debit_account_oid") + "\">");
		}
		
	%>

	<div>
		<%-- Currency --%>
		<%
			// IAZ CR-483B 08/13/09 Begin: Credit Currency is now separate form debit currency and is selected from a dropdwon
			options = Dropdown.createSortedCurrencyCodeOptions(
					terms.getAttribute("amount_currency_code"), loginLocale);

			isFixedValue = false;
			if (isFixedPmt) {
				if (InstrumentServices.isNotBlank(terms
						.getAttribute("amount_currency_code"))
						&& InstrumentServices.isNotBlank(aSTTerms
								.getAttribute("amount_currency_code")))
					isFixedValue = true;
			}
		%>
		<%-- MEer IR-36469 Check if field value is fixed or not for a Fixed Payment  --%>
		<%=widgetFactory.createSelectField("TransactionCurrencyDD",
					"TransferBetweenAccounts.Currency", " ", options,
					isReadOnly || isFixedValue || isStatusVerifiedPendingFX,
					!isTemplate, false,
					"class='char5'; onChange='updatePaymentCCY();'", "",
					"inline")%>

		<input type=hidden value="<%=fundingAmount%>" name=FundingAmount
			size=20 maxlength=20 class=ListText>

		<%=widgetFactory.createTextField("displayTransactionAmount",
					"TransferBetweenAccounts.Amount", displayTransactionAmount,
					"20", true, false, false, "class='char5'", "",
					"inline formItem")%>


		<%=widgetFactory.createTextField("numberOfPayees",
					"DomesticPaymentRequest.NoOfCredit", "" + numberOfDomPmts,
					"10", true, false, false, "", "", "inline formItem")%>
		<div style="clear: both;"></div>
	</div>
	<%-- IAZ CM-451 01/29/09 Add: send the total number of current Payees to the server 
              	so we can set Terms First Party data correctly --%>
	<input type=hidden name=numberOfPayees value="<%=numberOfDomPmts%>">



	<%@ include file="IndividualDebitsIndicator.frag"%>



	<%=widgetFactory.createTextField("CustUploadFileRef",
					"DomesticPaymentRequest.CustUploadFileRef",
					transaction.getAttribute("customer_file_ref"), "40",
					(isReadOnly || isStatusVerifiedPendingFX||isFixedPayment), false, false,
					"class='char30'", "", "")%>

</div>
<div class="columnRight">

	<%=widgetFactory.createSubsectionHeader(
					"DomesticPaymentRequest.OrderingParty", false, false,
					false, "")%>
	<div style="display: none">
		<%-- // VS CR 631  Begin : Moving Ordering Party and deleted Ordering Party Bank and 2nd Intermediary Bank from this file--%>
		<input type=hidden name=c_OrderingParty
			value=<%=terms.getAttribute("c_OrderingParty")%>> <input
			type=hidden name=OrderingPartyOid
			value=<%=orderingParty.getAttribute("payment_party_oid")%>> <input
			type=hidden value="<%=opAdjName%>" name='OrderingPartyName'
			id='OrderingPartyName'>
	</div>
	<%
		//String corporateOrgToDisplay = corporateOrg.getAttribute("name");
		String corporateOrgToDisplay = opAdjName;

		// if(usingOtherCorpsAccnt) corporateOrgToDisplay = accntOwnerCorporateID;
	%>


	<%=widgetFactory.createTextField("OrderingPartyNameDisplay",
					"DomesticPaymentRequest.OrderingPartyName",
					corporateOrgToDisplay, "20", true, false, false,
					"disabled", "", "")%>


	<%
		String corporateAlternateName = opAltName;
	%>
	<%=widgetFactory.createTextField("corporateAlternateName",
					"DomesticPaymentRequest.AltrOrderingPartyName",
					corporateAlternateName, "34", true, false, false,
					"disabled", "", "")%>



	<%=widgetFactory.createSubsectionHeader(
					"TransferBetweenAccounts.FEC", false, false, false, "")%>


	<%
		String equivPaymentCCY = "";
		if (InstrumentServices.isNotBlank(terms
				.getAttribute("equivalent_exch_amount")))
			equivPaymentCCY = fundingCurrency;
		if (getDataFromDoc)
			equivPaymentCCY = terms.getAttribute("equivalent_exch_amt_ccy");

		String EqDisplayAmt = TPCurrencyUtility.getDisplayAmount(
				terms.getAttribute("equivalent_exch_amount"),
				equivPaymentCCY, loginLocale);
	%>
  <%// jgadela R84 T36000019732 - readonly for fixed payemnt template %>
	<div class="formItemWithIndent3">
		<%=widgetFactory.createTextField("covered_by_fec_number",
					"TransferBetweenAccounts.FECCovered",
					terms.getAttribute("covered_by_fec_number"), "14",
					(isReadOnly || (isFixedPayment && terms.isFixedValue("covered_by_fec_number", isTemplate))) && !isFXeditable, false, false,
					"class='char10' onkeyup='uncheckReqMarketRate();'", "",
					"inline")%>
<%// jgadela R84 T36000019732 - readonly for fixed payemnt template %>
		<%=widgetFactory.createNumberField("fec_rate",
					"TransferBetweenAccounts.FECRate",
					terms.getAttribute("fec_rate"), "14", (isReadOnly || (isFixedPayment && terms.isFixedValue("fec_rate", isTemplate)))
							&& !isFXeditable, false, false,
					"class='char10' onkeyup='uncheckReqMarketRate();'",
					"constraints: {pattern: '##0.########', places:'0,8'}",
					"inline")%>

		<div style="clear: both;"></div>
	</div>


	<%
		if (InstrumentServices.isBlank(requestMarketRateInd))
			requestMarketRateInd = TradePortalConstants.INDICATOR_NO;
	%>
	<div class="formItem" id="requestMarketRate">
		<%
			out.print(widgetFactory.createCheckboxField(
					"RequestMarketRateCheckBox",
					"FundsTransferRequest.RequestMarketRate",
					requestMarketRateInd, requestMarketRateInd
							.equals(TradePortalConstants.INDICATOR_YES),
					isReadOnly, false, "onClick='updateReqMarketRate()'", "",
					""));
		%>

		<%-- Vshah - IR#DEUL120562739 - 01/05/2012 - Rel7.1 - BEGIN - Added below hidden variable --%>
		<%-- Becuase you cannot unset this setting once it is set to 'Y'. when you un-check the box it does not send 'N' �?? it sends null --%>
		<%-- To resolve this, we have to use hidden field and javascript --%>
		<input type="hidden" name="RequestMarketRate"
			value="<%=requestMarketRateInd%>">

	</div>

	<div>
		<%=widgetFactory.createTextField("transfer_fx_rate",
					"TransferBetweenAccounts.BaseToFXRate",
					terms.getAttribute("transfer_fx_rate"), "22", true, false,
					false, "class='char5'", "", "inline")%>

		<%=widgetFactory.createTextField("display_fx_rate_method",
					"TransferBetweenAccounts.FXCalcMethod",
					terms.getAttribute("display_fx_rate_method"), "22", true,
					false, false, "class='char5'", "", "inline")%>

		<%=widgetFactory.createTextField("equivalent_exch_ccy",
					"TransferBetweenAccounts.EquivAmount",
					terms.getAttribute("equivalent_exch_amt_ccy") + "  "
							+ EqDisplayAmt, "22", true, false, false,
					"class='char5'", "", "inline")%>

		<div style="clear: both;"></div>
	</div>
	<div style="display: none">
		<input type="hidden" name="equivalent_exch_amount"
			value=<%=terms.getAttribute("equivalent_exch_amount")%>>
	</div>

</div>
<%-- End  div class = "columnRight" --%>

</div>
<%-- End Div for Title Pane 1. Terms Title Pane --%>

<%-- Div for Title Pane Beneficiary Start --%>
<%=widgetFactory.createSectionHeader("2",
					"DomesticPaymentRequest.Beneficiaries")%>
<div id="add_ben_div">

	<%
		String totalPaymentsAmountStr = "0.00";


		BigDecimal totalPaymentsAmount = new BigDecimal(""
				+ DatabaseQueryBean.getSum(
						"AMOUNT",
						"domestic_payment",
						"p_transaction_oid =? ",false, new Object[]{transaction.getAttribute("transaction_oid")}));
		totalPaymentsAmountStr = TPCurrencyUtility.getDisplayAmount(
				totalPaymentsAmount.toPlainString(), transactionCCY,
				loginLocale);
		Debug.debug("totalPaymentsAmountStr is " + totalPaymentsAmountStr);
		DocumentHandler checkError = doc.getComponent("/Error");
		if (checkError != null) {
			String checkErrorStr = checkError.toString();
			if ((checkErrorStr != null)
					&& (checkErrorStr
							.indexOf(TradePortalConstants.INVALID_CURRENCY_FORMAT)) != -1) {
				//totalPaymentsAmountStr = totalPaymentsAmount.toString();
				totalPaymentsAmountStr = totalPaymentsAmount
						.toPlainString();
			}
		}
		//BSL Rel 6.1 IR #VEUL031445617 03/22/11 End

		StringBuffer dynamicWhereClause = new StringBuffer();
		dynamicWhereClause.append("dp.p_transaction_oid = ");
		dynamicWhereClause.append(transaction
				.getAttribute("transaction_oid"));

		currentDPOid = currentDomesticPayment
				.getAttribute("domestic_payment_oid");
	%>

	<%@ include file="DomesticPayment-InvoiceDetail-SearchParms.frag"%>
	<%@ include file="DomesticPayment-InvoiceDetailSearch.frag"%>

	<%
		DataGridFactory dgFactory = new DataGridFactory(resMgr,
				userSession, formMgr, response);
		String gridHtml = dgFactory.createDataGrid(
				"TransactionFTDPISSDataGridId",
				"TransactionFTDPISSDataGrid", null);
	%>
	<%=gridHtml%>

</div>
<%=widgetFactory.createWideSubsectionHeader(
					"TransactionTerms.PaymentInstructions", false, false,
					false, "")%>
<div class="columnLeft">

	<%
		String defaultPM = currentDomesticPayment
				.getAttribute("payment_method_type");
		if (InstrumentServices.isBlank(defaultPM))
			defaultPM = "";

		List<Object> sqlPrmsLst = new ArrayList<Object>();
		String pLocaleSQL = null;
		
		pLocaleSQL = " locale_name = ?";
		sqlPrmsLst.add(loginLocale);
		if (pLocaleSQL.indexOf("fr") == -1){
			pLocaleSQL = " locale_name is null";
			sqlPrmsLst = new ArrayList<Object>();
		}
		
		String pSql = "select CODE, DESCR from refdata where table_type = 'PAYMENT_METHOD' and "
				+ pLocaleSQL
				+ " and addl_value in ('ELECTRONIC', 'CHEQUE', 'INTERNATIONAL', 'Electronic', 'Cheque', 'International')";
		DocumentHandler pList = DatabaseQueryBean.getXmlResultSet(pSql,
				false, sqlPrmsLst);
		
		String pOptions = ListBox.createOptionList(pList, "CODE", "DESCR",
				defaultPM, null);

		isFixedValue = currentDomesticPayment.isFixedValue(
				"payment_method_type", isTemplate);
		if (isFixedValue || isStatusVerifiedPendingFX)
			out.println("<input type=hidden name=PaymentMethodType value=\""
					+ defaultPM + "\">");
	%>
	<%=widgetFactory.createSelectField("PaymentMethodType",
					"DomesticPaymentRequest.PaymentMethod", " ", pOptions,
					(isReadOnly || isFixedValue || isStatusVerifiedPendingFX),
					!isTemplate, false, "onChange='pmDynamicUpdates();'", "",
					"")%>
	<%
		if (TradePortalConstants.CROSS_BORDER_FIN_TRANSFER
				.equals(defaultPM))
			isCBFT = true;
		if ((TradePortalConstants.PAYMENT_METHOD_BCHK.equals(defaultPM)) || // IAZ IR-SSUK012149800 01/27/10
				(TradePortalConstants.PAYMENT_METHOD_CCHK.equals(defaultPM))) // IAZ IR-SSUK012149800 01/27/10
			isCheckPM = true; // IAZ IR-SSUK012149800 01/27/10
	%>


	<%-- Charges Radio button --%>

	<%=widgetFactory.createLabel("",
					"DomesticPaymentRequest.BankCharges", false, !isTemplate,
					false, "")%>

	<div class="formItem">
		<%=widgetFactory.createRadioButtonField("BankChargesType",
					"TradePortalConstants.CHARGE_OUR_ACCT",
					"DomesticPaymentRequest.AllOurAcct",
					TradePortalConstants.CHARGE_OUR_ACCT, bankChargesType
							.equals(TradePortalConstants.CHARGE_OUR_ACCT),
					isReadOnly || isFixedValue || isStatusVerifiedPendingFX,
					"", "")%>
		<br>
		<%=widgetFactory
					.createRadioButtonField(
							"BankChargesType",
							"TradePortalConstants.CHARGE_BEN_FOR_OUTSIDE",
							"DomesticPaymentRequest.PayeeAccount",
							TradePortalConstants.CHARGE_BEN_FOR_OUTSIDE,
							bankChargesType
									.equals(TradePortalConstants.CHARGE_BEN_FOR_OUTSIDE),
							isReadOnly || isFixedValue
									|| isStatusVerifiedPendingFX, "", "")%>
		<br>
		<%=widgetFactory.createRadioButtonField(
					"BankChargesType",
					"TradePortalConstants.CHARGE_OTHER",
					"DomesticPaymentRequest.Share",
					TradePortalConstants.CHARGE_OTHER,
					bankChargesType == null
							|| bankChargesType.equals("")
							|| bankChargesType
									.equals(TradePortalConstants.CHARGE_OTHER),
					isReadOnly || isFixedValue || isStatusVerifiedPendingFX,
					"", "")%>

	</div>
	<div style="clear: both;"></div>
</div>
<%-- End  div class = "columnLeft" --%>

<div class="columnRight">
	<%--Debit Party Div columnRight start --%>

	<%-- Currency/Amount/Value Date --%>
	<%=widgetFactory.createTextField(
					"TransactionCurrencyDisplay", "FundsTransferRequest.CCY",
					transactionCCYDisplay, "5", true, false, false,
					"style='width: 60px'; disabled", "", "inline formItem")%>

	<%--<%=widgetFactory.createTextField("DomesticPaymentAmount","FundsTransferRequest.Amount",
			displayPaymentAmount ,"22",(isReadOnly||currentDomesticPayment.isFixedValue("amount", isTemplate)||isStatusVerifiedPendingFX),
			!isTemplate,false, "class='char6'", "", "inline formItem") %> --%>

	<%=widgetFactory.createAmountField(
					"DomesticPaymentAmount",
					"FundsTransferRequest.Amount",
					displayPaymentAmount,
					transactionCCY,
					(isReadOnly
							|| currentDomesticPayment.isFixedValue("amount",
									isTemplate) || isStatusVerifiedPendingFX),
					!isTemplate, false, "class='char6'", "readOnly:"
							+ isReadOnly, "inline formItem")%>
	<%-- Nar CR 966 Rel 9.2 09/17/2014 Add - Begin 
	   Display Allow Amount Modification check box only in Payment Fixed template --%>
	<% if(isFixedTemplate){ %>
	<%=widgetFactory.createCheckboxField("AllowPayAmtModification", "TemplateDetail.AllowModification",
										TradePortalConstants.INDICATOR_YES.equals(currentDomesticPayment.getAttribute("allow_pay_amt_modification")),
										isReadOnly, false, "", "", "")%>
	
    <% }
	// Added if condition to not display value date text for template 
    if(!isTemplate){ 
    %>
    <%-- Nar CR 966 Rel 9.2 09/17/2014 Add - End --%>
	<%=widgetFactory.createTextField("ValueDate",
					"DomesticPaymentRequest.ValueDate", TPDateTimeUtility
							.formatDate(currentDomesticPayment
									.getAttribute("value_date"),
									TPDateTimeUtility.LONG, userLocale), "5",
					true, false, false, "style='width: 60px'", "",
					"inline formItem")%>
    <%} %>


	<div style="clear: both"></div>


	<%=widgetFactory.createTextField(
					"PayerDescription",
					"DomesticPaymentRequest.CustomerReference",
					currentDomesticPayment.getAttribute("customer_reference"),
					"20",
					(isReadOnly
							|| currentDomesticPayment.isFixedValue(
									"customer_reference", isTemplate) || isStatusVerifiedPendingFX),
					false, false, "onChange='updateTermsDescription();'", "",
					"")%>
</div>
<%--Debit Party Div columnRight End --%>
<div style="clear: both"></div>
<%
	String payeeMessageText = currentDomesticPayment
			.getAttribute("payee_description");
	if (!InstrumentServices.isBlank(payeeMessageText)) {
		if (payeeMessageText.equals("null"))
			payeeMessageText = "";
	}

	isFixedValue = currentDomesticPayment.isFixedValue(
			"payee_description", isTemplate);
	if (isFixedValue)
		out.println("<input type=hidden name=PayeeMessage value=\""
				+ StringFunction.xssCharsToHtml(payeeMessageText) + "\">");
%>
   <%-- Nar CR 966 Rel 9.2 09/17/2014 Add - Begin --%>
   <%=widgetFactory.createLabel("",
					"DomesticPaymentRequest.StatementTextToPayee", false, isReadOnly || isFixedValue
					|| isStatusVerifiedPendingFX, false, "inline")%>
   <%-- Display Allow Payment Detail Modification check box only in Payment Fixed template --%>
   <% if(isFixedTemplate){ %>

     <%=widgetFactory.createCheckboxField("AllowPayDetailModification", "TemplateDetail.AllowModification",
										TradePortalConstants.INDICATOR_YES.equals(currentDomesticPayment.getAttribute("allow_pay_detail_modification")),
										isReadOnly, false, "", "", "")%>
   <%}%>
   <%-- Nar CR 966 Rel 9.2 09/17/2014 Add - End --%>
   <%=widgetFactory.createTextArea("PayeeMessage",
					"",
		   StringFunction.xssCharsToHtml(payeeMessageText), isReadOnly || isFixedValue
							|| isStatusVerifiedPendingFX, false, false,
					"rows='5' cols='128' maxlength='140'",
					"", "")%>



<%-- Beneficiary  --%>

<div class="columnLeft">
	<%
		String benSearchHtml = "";
		String clearBenSearchHtml = "";
		String identifierStr = "PayeeName,PayeeAddrL1,PayeeAddrL2,PayeeAddrL3,PayeeAddrL4,Country,payeeFaxNumber,payeeEmail,beneficiaryPartyOid";
		String sectionName = "payben";

		if (!currentDomesticPayment.isFixedValue("payee_name", isTemplate)
				&& !isStatusVerifiedPendingFX) {
			benSearchHtml = widgetFactory.createPartySearchButton(
					identifierStr, sectionName, false,
					TradePortalConstants.PAYEE, true);
			clearBenSearchHtml = widgetFactory.createPartyClearButton(
					"ClearBenSearchButton", "clearBenSearch()", false, "");
		}
	%>

	
	<%=widgetFactory.createSubsectionHeader("FTDP.FirstParty",
					(isReadOnly || isFromExpress), false, isExpressTemplate,
					(benSearchHtml + clearBenSearchHtml))%>




	<%
		String clazz = "formItem";
		if (isReadOnly
				|| currentDomesticPayment.isFixedValue(
						"payee_account_number", isTemplate)
				|| isStatusVerifiedPendingFX) {
			clazz += " readOnly";
		}
	%>
	<%--Naveen. IR-T36000011495(ANZ 784). Mandatory symbol not required for Account Number label. Hence commented below if block.
      if(isTemplate){
	  	 clazz+=" required";
      }	  
 --%>


	<div id='enterAccountLabelDiv' class='<%=clazz%>'>
		<label for="enterAccountLabel"><%=resMgr.getText("DomesticPaymentRequest.BenAccountNumber",
					TradePortalConstants.TEXT_BUNDLE)%></label><br>
		<div style="clear: both;"></div>

		<div id="beneAccountsDataLoading" style="display: none;">
			<%--hide it to start--%>
			<span class='dijitInline dijitIconLoading'></span>
			<%=resMgr.getText("common.Loading",
					TradePortalConstants.TEXT_BUNDLE)%>
		</div>
		<div id="beneAccountsData">
			<%
				boolean isFXReadOnly = isReadOnly || isStatusVerifiedPendingFX;
			%>
			<%@ include file="Transaction-FTDP-ISS-BeneAcctData.frag"%>

		</div>
	</div>
	<%--KMehta - 27 Feb 2015 - Rel9.3 IR-T36000036737 - Change  - Begin--%>
	<%=widgetFactory.createTextField(
					"PayeeName",
					"DomesticPaymentRequest.PayeeName",
					currentDomesticPayment.getAttribute("payee_name"),
					"140",
					(isReadOnly
							|| (currentDomesticPayment.isFixedValue(
									"payee_name", isTemplate)) || isStatusVerifiedPendingFX),
					!isTemplate, false,
					"class='char35'; onChange='updatePayeePartyName();'", "",
					"")%>
	<%--KMehta - 27 Feb 2015 - Rel9.3 IR-T36000036737 - Change  - End --%>

	<%=widgetFactory.createTextField(
					"PayeeAddrL1",
					"DomesticPaymentRequest.BeneficiaryAddress",
					currentDomesticPayment.getAttribute("payee_address_line_1"),
					"35",
					(isReadOnly
							|| (currentDomesticPayment.isFixedValue(
									"payee_address_line_1", isTemplate)) || isStatusVerifiedPendingFX),
					!isTemplate, false,
					"class='char35'; onChange='updatePayeeAddressLine();'", "",
					"")%>



	<%=widgetFactory.createTextField(
					"PayeeAddrL2",
					"",
					currentDomesticPayment.getAttribute("payee_address_line_2"),
					"35",
					(isReadOnly
							|| (currentDomesticPayment.isFixedValue(
									"payee_address_line_2", isTemplate)) || isStatusVerifiedPendingFX),
					!isTemplate, false,
					"class='char35'; onChange='updatePayeeAddressLine();'", "",
					"")%>



	<%=widgetFactory.createTextField(
					"PayeeAddrL3",
					"",
					currentDomesticPayment.getAttribute("payee_address_line_3"),
					"35",
					(isReadOnly
							|| (currentDomesticPayment.isFixedValue(
									"payee_address_line_3", isTemplate)) || isStatusVerifiedPendingFX),
					!isTemplate, false,
					"class='char35'; onChange='updatePayeeAddressLine();'", "",
					"")%>

	<%
		// IAZ CM CR-507 12/19/09: Line 4 is only displayed when Payment Method selected is Cross Boarder FT
		String l4Visibility = "visibility: visible;";
		if (isCBFT)
			l4Visibility = "hidden;";
	%>
	<div id="payeeAddrLine4">


		<%=widgetFactory.createTextField(
					"PayeeAddrL4",
					"",
					currentDomesticPayment.getAttribute("payee_address_line_4"),
					"35",
					(isReadOnly
							|| (currentDomesticPayment.isFixedValue(
									"payee_address_line_4", isTemplate)) || isStatusVerifiedPendingFX),
					!isTemplate, false,
					"class='char35'; onChange='updatePayeeAddressLine();'", "",
					"")%>
	</div>


	<%
		String defaultCntry = currentDomesticPayment
				.getAttribute("country");
		if (InstrumentServices.isBlank(defaultCntry))
			defaultCntry = " ";
		options = Dropdown.createSortedRefDataOptions("COUNTRY",
				defaultCntry, loginLocale);

		isFixedValue = currentDomesticPayment.isFixedValue("country",
				isTemplate);
		if (isFixedValue)
			out.println("<input type=hidden name=Country value=\""
					+ defaultCntry + "\">");
	%>

	<%
		if (isReadOnly || isFixedValue || isStatusVerifiedPendingFX) {
	%>
	<div id="domesticCountryLabel" class="formItem readOnly">
		<label for="domCountryLab"><%=resMgr.getText("DomesticPaymentRequest.Country",
						TradePortalConstants.TEXT_BUNDLE)%></label><br>
		<span id="Country" class="fieldValue"> <%=(defaultCntry == null || defaultCntry.trim().equals(
						"")) ? "&nbsp;" : ReferenceDataManager.getRefDataMgr()
						.getDescr(TradePortalConstants.COUNTRY, defaultCntry)%>
		</span>
	</div>
	<%
		} else {
	%>
	<div id="domesticCountryLabel" class="formItem">
		<label for="domCountryLab"><%=resMgr.getText("DomesticPaymentRequest.Country",
						TradePortalConstants.TEXT_BUNDLE)%></label><br>
		<select name="Country" id="Country"
			data-dojo-type="dijit.form.FilteringSelect" class='char35'
			; value="<%=defaultCntry%>" data-dojo-props="trim:true">
			<%=options%>
		</select>
	</div>
	<%
		}
	%>
	<div id="payeeFaxNumberId">
		<%=widgetFactory.createTextField(
					"payeeFaxNumber",
					"DomesticPaymentRequest.BeneficiaryFaxNum",
					currentDomesticPayment.getAttribute("payee_fax_number"),
					"20",
					(isReadOnly
							|| (currentDomesticPayment.isFixedValue(
									"payee_fax_number", isTemplate)) || isStatusVerifiedPendingFX),
					false, false, "", "maxLength:'15'", "")%>
	</div>

	<%
		if (isReadOnly
				|| isStatusVerifiedPendingFX
				|| (currentDomesticPayment.isFixedValue("payee_email",
						isTemplate))) {
			//SSikhakolli - Rel-8.3 ANZ IR# T36000018971 on 10/28/2013 - Removing 'payeeEmailId div' related statements form code.
	%>
	<div class="formItem readOnly">
		<label for="emailLabel"><%=resMgr.getText(
						"DomesticPaymentRequest.BenEmailAddress",
						TradePortalConstants.TEXT_BUNDLE)%></label><br>
		<span id="payeeEmail" class="fieldValueWordWrap"> <%=(null == currentDomesticPayment
						.getAttribute("payee_email") || currentDomesticPayment
						.getAttribute("payee_email").trim().equals("")) ? "&nbsp;"
						: currentDomesticPayment.getAttribute("payee_email")%>
		</span>
	</div>
	<%
		} else {
	%>
	<%=widgetFactory.createTextArea("payeeEmail",
						"DomesticPaymentRequest.BenEmailAddress",
						currentDomesticPayment.getAttribute("payee_email"),
						false, false, false, "maxlength='255'",
						"rows: '5', cols: '20'", "")%>
	<%
		}
	%>
	<div id="instructionNumber">
		<%=widgetFactory.createTextField(
					"payeeInstructionNumber",
					"DomesticPaymentRequest.BenInstructionNumber",
					currentDomesticPayment
							.getAttribute("payee_instruction_number"),
					"10",
					(isReadOnly
							|| (currentDomesticPayment.isFixedValue(
									"payee_instruction_number", isTemplate)) || isStatusVerifiedPendingFX),
					false, false, "class='char10';", "", "")%>
	</div>



	<%
		String benBankSearchHtml = "";
		String clearBenBankSearchHtml = "";

		if (TradePortalConstants.NON_ADMIN.equals(userSession
				.getSecurityType())) {
			if (!currentDomesticPayment.isFixedValue("payee_bank_code",
					isTemplate) && !isStatusVerifiedPendingFX) {
				String itemid = "PayeeBankName,PayeeBankAddressLine1,PayeeBankAddressLine2,PayeeBankAddressLine3,PayeeBankCountry,PayeeBankBranchName,PayeeBankCode,PayeeBankData";
				String section = "payments_banksearch";
				benBankSearchHtml = widgetFactory.createBankSearchButton(
						itemid, section, false,
						TradePortalConstants.BENEFICIARY_BANK);
				clearBenBankSearchHtml = widgetFactory
						.createPartyClearButton("clearBenBankSearchButton",
								"clearBenBankSearch()", false, "");
			}
		}
	%>

	<div id="benBankDivID">
		<%-- Ben bank Div Start --%>
		<%=widgetFactory.createSubsectionHeader(
					"DomesticPaymentRequest.BeneficiaryBank",
					(isReadOnly || isFromExpress), false, isExpressTemplate,
					(benBankSearchHtml + clearBenBankSearchHtml))%>

		<%
			//IAZ CR 573 06/01/10 Begin
			//    Make Bank Brach Codes editable for Transactions. Keep protected for Templates. 
			if (isTemplate) {
		%>

		<%=widgetFactory.createTextField("PayeeBankCode",
						"DomesticPaymentRequest.BenBankBranchCode",
						currentDomesticPayment.getAttribute("payee_bank_code"),
						"20", (isReadOnly || isStatusVerifiedPendingFX)
								|| isStatusVerifiedPendingFX, !isTemplate,
						false, "readonly", "", "")%>

		<div id="benBankCodeLabel" class="formItem readOnly"
			style="display: none"></div>
		<%
			} else {

				if (isReadOnly
						|| currentDomesticPayment.isFixedValue(
								"payee_bank_code", isTemplate)
						|| isStatusVerifiedPendingFX) {
		%>
		<div id="benBankCodeLabel" class="formItem readOnly">
			<label for="payeeBankCodeLabel"><%=resMgr.getText(
							"DomesticPaymentRequest.BenBankBranchCode",
							TradePortalConstants.TEXT_BUNDLE)%></label><br>
			<span id="PayeeBankCode" class="fieldValue"> <%=(currentDomesticPayment
							.getAttribute("payee_bank_code") == null || currentDomesticPayment
							.getAttribute("payee_bank_code").trim().equals("")) ? "&nbsp;"
							: currentDomesticPayment
									.getAttribute("payee_bank_code")%>
			</span>
		</div>
		<%
			} else {
		%>
		<div id='benBankCodeLabel' class="formItem  required">
			<label for="payeeBankCodeLabel"><%=resMgr.getText(
							"DomesticPaymentRequest.BenBankBranchCode",
							TradePortalConstants.TEXT_BUNDLE)%></label><br>
			<input type="text" name="PayeeBankCode" id="PayeeBankCode"
				value="<%=currentDomesticPayment
							.getAttribute("payee_bank_code")%>"
				class='char35' ; data-dojo-type="dijit.form.ValidationTextBox"
				maxlength=20 onChange="updateBankBranchCode('PRB')"
				data-dojo-props="trim:true">
		</div>
		<%
			}

			}
			//IAZ CR 573 06/01/10 End
		%>



		<%-- // IAZ CM CR-507 12/19/09: Bank Name and address now protected TextArea --%>
		<%
			String countryCode = "";
			String countryDesc = "";
			/* IAZ IR-SEUK012660805 02/10/20 Rework Begin: For Release 5.2 we do not enable Country Field for OPY 
			   IAZ IR-SLUK021033331 04/28/10 For Release 6.0, we now enable Country Field for OPY */
			String extraTag = "";
			String clearReqExtraTag = ""; //IAZ 02/02/10 Add

			countryCode = "";
			countryDesc = "";
			try {
				countryCode = currentDomesticPayment
						.getAttribute("payee_bank_country");
				if (InstrumentServices.isNotBlank(countryCode))
					countryDesc = ReferenceDataManager.getRefDataMgr()
							.getDescr(TradePortalConstants.COUNTRY,
									countryCode,
									resMgr.getCSDB().getLocaleName());
				if (InstrumentServices.isBlank(countryDesc)) {
					countryDesc = countryCode;
					countryCode = "";
				}
			} catch (Exception e) {
				countryCode = "";
				countryDesc = "";
			}

			String paymentBankDisplayData = "";
			paymentBankDisplayData = buildPaymentBankDisplayData(
					currentDomesticPayment.getAttribute("payee_bank_name"),
					currentDomesticPayment.getAttribute("payee_branch_name"),
					currentDomesticPayment.getAttribute("address_line_1"),
					currentDomesticPayment.getAttribute("address_line_2"),
					currentDomesticPayment.getAttribute("address_line_3"),
					countryDesc, isReadOnly || isStatusVerifiedPendingFX);
		%>


		<%--cquinton 2/8/2013 added PayeeBankData and PayeeBankName back - use PayeeBankData for display--%>

		<%-- Srinivasu_D  T36000019515 Rel8.4 01/23/2014 - start - changing the textarea to autoresize --%>
		<%--
        <%=widgetFactory.createTextArea("PayeeBankData","DomesticPaymentRequest.PayeeBankNameAddress",
             paymentBankDisplayData , true, false, false, 
             "", "rows: '5', cols: '20'", "") %>
		--%>
		<%=widgetFactory
					.createAutoResizeTextArea(
							"PayeeBankData",
							"DomesticPaymentRequest.PayeeBankNameAddress",
							paymentBankDisplayData,
							true,
							false,
							false,
							"class='autoHeightTxtAreaQtrWidthNoSidebar' style='overflow-x: hidden;' rows='4' cols='45'",
							"", "")%>

		<%-- Srinivasu_D  T36000019515 Rel8.4 01/23/2014 - End --%>

		<input type=hidden id="PayeeBankName" name="PayeeBankName"
			value="<%=currentDomesticPayment.getAttribute("payee_bank_name")%>">

		<input type=hidden id="PayeeBankAddressLine1"
			name="PayeeBankAddressLine1"
			value="<%=currentDomesticPayment.getAttribute("address_line_1")%>">
		<input type=hidden id="PayeeBankAddressLine2"
			name="PayeeBankAddressLine2"
			value="<%=currentDomesticPayment.getAttribute("address_line_2")%>">
		<input type=hidden id="PayeeBankAddressLine3"
			name="PayeeBankAddressLine3"
			value="<%=currentDomesticPayment.getAttribute("address_line_3")%>">
		<input type=hidden id="PayeeBankCountry" name="PayeeBankCountry"
			value="<%=countryCode%>"> <input type=hidden
			id="PayeeBankBranchName" name="PayeeBankBranchName"
			value="<%=currentDomesticPayment.getAttribute("payee_branch_name")%>">
	</div>
	<%-- Ben bank Div End --%>
</div>
<%-- End Div for  columnLeft (Beneficiary)--%>

<div class="columnRight">
	<%--First 	Intermediary Bank --%>

	<%
		String interBankSearchHtml = "";
		String interBankClearHtml = "";
		isFixedValue = currentDomesticPayment.isFixedValue(
				"c_FirstIntermediaryBank", isTemplate);
		if (TradePortalConstants.NON_ADMIN.equals(userSession
				.getSecurityType())) {
			if (!isFixedValue && !isStatusVerifiedPendingFX) {
				/* KMehta IR-T36000031030 Rel 9300 on 22-Jun-2015 Change - Begin*/
				String itemid1 = "FirstIntBankName,FirstIntBankAddressLine1,FirstIntBankAddressLine2,FirstIntBankAddressLine3,FirstIntBankCountry,FirstIntBankBranchName,FirstIntCode,FirstIntBankData,FirstIntBankBranchCode,c_FirstIntermediaryBank";
				/* KMehta IR-T36000031030 Rel 9300 on 22-Jun-2015 Change - End*/
				String section1 = "payments_firstintermediarybank";
				interBankSearchHtml = widgetFactory.createBankSearchButton(
						itemid1, section1, false,
						TradePortalConstants.INTERMEDIARY_PARTY_BANK);
				interBankClearHtml = widgetFactory.createPartyClearButton(
						"ClearInterButton", "clearInterBankSearch()",
						false, "");
			}

		}
	%>
	<div id="firstIntBankDivID">
		<%-- First Inter bank Div Start --%>
		<%=widgetFactory.createSubsectionHeader(
					"DomesticPaymentRequest.FirstInterBank",
					(isReadOnly || isFromExpress), false, isExpressTemplate,
					(interBankSearchHtml + interBankClearHtml))%>


		<%
			//IAZ CR 573 06/01/10 Begin
			//    Make Bank Brach Codes editable for Transactions. Keep protected for Templates. \          
			if (isTemplate) {
		%>

		<%=widgetFactory.createTextField("FirstIntCode",
						"BankSearch.BankBranchCode",
						firstIntBank.getAttribute("bank_branch_code"), "20",
						(isReadOnly || isStatusVerifiedPendingFX), false,
						false, "disabled", "", "")%>
		<%=widgetFactory.createTextField(
						"FirstIntBankBranchCode", "",
						firstIntBank.getAttribute("bank_branch_code"), "20",
						(isReadOnly || isStatusVerifiedPendingFX), false,
						false, " type=hidden ", "", "")%>

		<%
			} else {
		%>
		<%=widgetFactory.createTextField("FirstIntCode", "",
						firstIntBank.getAttribute("bank_branch_code"), "20",
						(isReadOnly || isStatusVerifiedPendingFX), false,
						false, " disabled type=hidden ", "", "")%>

		<%=widgetFactory.createTextField(
						"FirstIntBankBranchCode",
						"BankSearch.BankBranchCode",
						firstIntBank.getAttribute("bank_branch_code"),
						"20",
						(isReadOnly || isFixedValue || isStatusVerifiedPendingFX),
						false,
						false,
						"class='char35'; onChange=\"updateBankBranchCode('IP1');\"",
						"", "")%>


		<%
			}
			//IAZ CR 573 06/01/10 End
		%>



		<%
			countryCode = "";
			countryDesc = "";
			try {
				countryCode = firstIntBank.getAttribute("country");
				if (InstrumentServices.isNotBlank(countryCode))
					countryDesc = ReferenceDataManager.getRefDataMgr()
							.getDescr(TradePortalConstants.COUNTRY,
									countryCode,
									resMgr.getCSDB().getLocaleName());
				if (InstrumentServices.isBlank(countryDesc)) {
					countryDesc = countryCode;
					countryCode = "";
				}

			} catch (Exception e) {
				countryCode = "";
				countryDesc = "";
			}

			paymentBankDisplayData = "";
			paymentBankDisplayData = buildPaymentBankDisplayData(
					firstIntBank.getAttribute("bank_name"),
					firstIntBank.getAttribute("branch_name"),
					firstIntBank.getAttribute("address_line_1"),
					firstIntBank.getAttribute("address_line_2"),
					firstIntBank.getAttribute("address_line_3"), countryDesc,
					isReadOnly || isStatusVerifiedPendingFX);
		%>

		<%-- Srinivasu_D  T36000019515 Rel8.4 01/23/2014 - start - changing the textarea to autoresize --%>
		<%--  			
         <%=widgetFactory.createTextArea("FirstIntBankData","DomesticPaymentRequest.FirstIntBankName",
        paymentBankDisplayData , true, false, false, 
        "", "rows: '5', cols: '20'", "") %>
       --%>
		<%=widgetFactory
					.createAutoResizeTextArea(
							"FirstIntBankData",
							"DomesticPaymentRequest.FirstIntBankName",
							paymentBankDisplayData,
							true,
							false,
							false,
							"class='autoHeightTxtAreaQtrWidthNoSidebar' style='overflow-x: hidden;' rows='4' cols='45'",
							"", "")%>

		<%-- Srinivasu_D  T36000019515 Rel8.4 01/23/2014 - End --%>


		<input type=hidden name="FirstIntBankName" id="FirstIntBankName"
			value="<%=firstIntBank.getAttribute("bank_name")%>"> <input
			type=hidden name="FirstIntBankAddressLine1"
			id="FirstIntBankAddressLine1"
			value="<%=firstIntBank.getAttribute("address_line_1")%>"> <input
			type=hidden name="FirstIntBankAddressLine2"
			id="FirstIntBankAddressLine2"
			value="<%=firstIntBank.getAttribute("address_line_2")%>"> <input
			type=hidden name="FirstIntBankAddressLine3"
			id="FirstIntBankAddressLine3"
			value="<%=firstIntBank.getAttribute("address_line_3")%>"> <input
			type=hidden name="FirstIntBankBranchName" id="FirstIntBankBranchName"
			value="<%=firstIntBank.getAttribute("branch_name")%>"> <input
			type=hidden name="FirstIntBankType"
			value="<%=TradePortalConstants.INTERMEDIARY_PARTY_BANK%>"> <input
			type=hidden name="FirstIntBankCountry" id="FirstIntBankCountry"
			value="<%=countryCode%>">
	</div>
	<%-- First Inter bank Div End --%>

	<div id="regulationReportingDivID">
		<%-- Regulation Report Div Start --%>

		<%=widgetFactory.createSubsectionHeader(
					"DomesticPaymentRequest.CentraBankReportingLabel", false,
					false, false, "")%>


		<%=widgetFactory.createTextField(
					"central_bank_reporting_1",
					"DomesticPaymentRequest.CentraBankReporting",
					currentDomesticPayment
							.getAttribute("central_bank_reporting_1"),
					"35",
					(isReadOnly
							|| currentDomesticPayment.isFixedValue(
									"central_bank_reporting_1", isTemplate) || isStatusVerifiedPendingFX),
					false, false, "", "", "")%>


		<%=widgetFactory.createTextField(
					"central_bank_reporting_2",
					"",
					currentDomesticPayment
							.getAttribute("central_bank_reporting_2"),
					"35",
					(isReadOnly
							|| (currentDomesticPayment.isFixedValue(
									"central_bank_reporting_2", isTemplate)) || isStatusVerifiedPendingFX),
					false, false, "", "", "")%>

		<%=widgetFactory.createTextField(
					"central_bank_reporting_3",
					"",
					currentDomesticPayment
							.getAttribute("central_bank_reporting_3"),
					"35",
					(isReadOnly
							|| (currentDomesticPayment.isFixedValue(
									"central_bank_reporting_3", isTemplate)) || isStatusVerifiedPendingFX),
					false, false, "", "", "")%>


		<%-- // BSL CR-655 02/22/11 Begin --%>
		<%
			for (int i = 0; i < isReadOnlyRptgCode.length; i++) {
		%>
		<%
			if (isReadOnlyRptgCode[i]) {
		%>
		<%=widgetFactory.createSelectField(
							reportingCodeNames[i],
							"DomesticPaymentRequest.ReportingCode" + (i + 1),
							"", "", true, false, false, "", "", "")%>
		<%
			} else {
		%>
		<%=widgetFactory.createSelectField(
							reportingCodeNames[i],
							"DomesticPaymentRequest.ReportingCode" + (i + 1),
							"", "", false, false, false, "", "", "")%>
		<%
			}
		%>
		<%
			}
		%>
		<%-- // BSL CR-655 02/22/11 End --%>

	</div>
	<%-- Regulation Report Div End --%>

	<div id="checkDeliveryDetailDivID">
		<%-- Check Delivery Detail Div Start --%>

		<%=widgetFactory.createSubsectionHeader(
					"DomesticPaymentRequest.BenCheckDelDetails", false, false,
					false, "")%>

		<%
			String defaultDeliveryMethod = currentDomesticPayment
					.getAttribute("delivery_method");
			if (InstrumentServices.isBlank(defaultDeliveryMethod))
				defaultDeliveryMethod = " ";
			options = Dropdown.createSortedRefDataOptions(
					"DELIVERY_METHOD_DELIVER_TO", defaultDeliveryMethod,
					loginLocale);
			isFixedValue = currentDomesticPayment.isFixedValue(
					"delivery_method", isTemplate);
			if (isFixedValue)
				out.println("<input type=hidden name=DeliveryMethod value=\""
						+ defaultDeliveryMethod + "\">");
			//out.println(InputField.createSelectField("DeliveryMethod", "",
			//          " ", options, "ListText", 
			//        isReadOnly||isFixedValue||isStatusVerifiedPendingFX));

			if (isReadOnly || isFixedValue || isStatusVerifiedPendingFX) {
		%>
		<div id="deliveryMethodID" class="formItem readOnly">
			<label for="domMethodLab"><%=resMgr.getText("DomesticPaymentRequest.SelectDeliveryMethod",
						TradePortalConstants.TEXT_BUNDLE)%></label><br>
			<span id="DeliveryMethod" class="fieldValue"> <%=(defaultDeliveryMethod == null || defaultDeliveryMethod
						.trim().equals("")) ? "&nbsp;" : ReferenceDataManager
						.getRefDataMgr().getDescr("DELIVERY_METHOD_DELIVER_TO",
								defaultDeliveryMethod)%>
			</span>
		</div>
		<%
			} else {
		%>
		<div id="deliveryMethodID" class="formItem">
			<label for="domMethodLab"><%=resMgr.getText("DomesticPaymentRequest.SelectDeliveryMethod",
						TradePortalConstants.TEXT_BUNDLE)%></label><br>
			<select name="DeliveryMethod" id="DeliveryMethod"
				data-dojo-type="dijit.form.FilteringSelect" class='char35'
				; value="<%=defaultDeliveryMethod%>" data-dojo-props="trim:true">
				<%=options%>
			</select>
		</div>
		<%
			}
		%>



		<%=widgetFactory.createTextField(
					"PayableLocation",
					"DomesticPaymentRequest.PayableLocation",
					currentDomesticPayment.getAttribute("payable_location"),
					"20",
					isReadOnly
							|| currentDomesticPayment.isFixedValue(
									"payable_location", isTemplate)
							|| isStatusVerifiedPendingFX, false, false,
					"class='char35';", "", "")%>


		<%=widgetFactory.createTextField(
					"PrintLocation",
					"DomesticPaymentRequest.PrintLocation",
					currentDomesticPayment.getAttribute("print_location"),
					"20",
					isReadOnly
							|| currentDomesticPayment.isFixedValue(
									"print_location", isTemplate)
							|| isStatusVerifiedPendingFX, false, false,
					"class='char35';", "", "")%>


		<%=widgetFactory.createTextField(
					"mailing_address_line_1",
					"DomesticPaymentRequest.MailingAdress",
					currentDomesticPayment
							.getAttribute("mailing_address_line_1"),
					"35",
					isReadOnly
							|| currentDomesticPayment.isFixedValue(
									"mailing_address_line_1", isTemplate)
							|| isStatusVerifiedPendingFX, false, false,
					"class='char35';", "", "")%>

		<%=widgetFactory.createTextField(
					"mailing_address_line_2",
					"",
					currentDomesticPayment
							.getAttribute("mailing_address_line_2"),
					"35",
					isReadOnly
							|| currentDomesticPayment.isFixedValue(
									"mailing_address_line_2", isTemplate)
							|| isStatusVerifiedPendingFX, false, false,
					"class='char35';", "", "")%>

		<%=widgetFactory.createTextField(
					"mailing_address_line_3",
					"",
					currentDomesticPayment
							.getAttribute("mailing_address_line_3"),
					"35",
					isReadOnly
							|| currentDomesticPayment.isFixedValue(
									"mailing_address_line_3", isTemplate)
							|| isStatusVerifiedPendingFX, false, false,
					"class='char35';", "", "")%>

		<%=widgetFactory.createTextField(
					"mailing_address_line_4",
					"",
					currentDomesticPayment
							.getAttribute("mailing_address_line_4"),
					"35",
					isReadOnly
							|| currentDomesticPayment.isFixedValue(
									"mailing_address_line_4", isTemplate)
							|| isStatusVerifiedPendingFX, false, false,
					"class='char35';", "", "")%>
	</div>
	<%-- Check Delivery Detail Div End --%>
</div>
<%-- End Div for  columnright--%>
<div style="clear: both"></div>
<%=widgetFactory.createWideSubsectionHeader(
					"DomesticPaymentRequest.BeneInvoiceDetails", false, false,
					false, "")%>

<%
	String invoiceDetails = StringFunction
			.xssHtmlToChars(paymentInvoiceDetails
					.getAttribute("payee_invoice_details"));
	if (isTemplate) {		
		if (isFixedTemplate) {
%>
 <%-- Nar CR 966 Rel 9.2 09/17/2014
	   Display Allow Payment Detail Modification check box only in Payment Fixed template --%>
 <%=widgetFactory.createCheckboxField("AllowInvDetailModification", "TemplateDetail.AllowModification",
										TradePortalConstants.INDICATOR_YES.equals(currentDomesticPayment.getAttribute("allow_inv_detail_modification")),
									isReadOnly, false, "", "", "")%>
       <%}%>	
       <!-- /* KMehta IR-T36000043824 Rel 9500 on 27-Nov-2015 Add - Begin */ -->
<%=widgetFactory.createTextArea(
						"payeeInvoiceDetails",
						"",
						invoiceDetails,
						isReadOnly
								|| currentDomesticPayment.isFixedValue(
										"payee_invoice_details", isTemplate)
								|| isStatusVerifiedPendingFX,
						false,
						false,
						"maxlength=81000  rows='10' cols='122'",
						"", "")%>

<%
	} else {
%>

<%=widgetFactory.createTextArea(
						"payeeInvoiceDetails",
						"",
						invoiceDetails,
						isReadOnly
								|| currentDomesticPayment.isFixedValue(
										"payee_invoice_details", isTemplate)
								|| isStatusVerifiedPendingFX,
						false,
						false,
						"maxlength=81000   rows='10' cols='122' onChange='updatePayeeInvoiceDetail();'",
						"", "")%>
<%
	}
%>
<!-- /* KMehta IR-T36000043824 Rel 9500 on 27-Nov-2015 Add - End */ -->
<%=widgetFactory
					.createWideSubsectionHeader("DomesticPaymentRequest.PaymentProcessingDetails")%>

<div class="columnLeft">
	<%-- IR POUL011148837 rkazi 01/18/2011 Begin --%>
	<%
		String benSeqId = (InstrumentServices
				.isBlank(currentDomesticPayment
						.getAttribute("sequence_number")) || isTemplate) ? ""
				: instrument.getAttribute("complete_instrument_id")
						+ "-"
						+ currentDomesticPayment
								.getAttribute("sequence_number");
	%>

	<%=widgetFactory.createTextField("BeneficiarySequenceId",
					"DomesticPaymentRequest.BeneficiarySequenceId", benSeqId,
					"20", true, false, false, "", "", "")%>
	<%-- IR POUL011148837 rkazi 01/18/2011 End --%>

	<%-- IR POUL011148837 rkazi 01/18/2011 Begin --%>
	<%
		String paymentStatus = currentDomesticPayment
				.getAttribute("payment_status");
	%>

	<%=widgetFactory.createTextField(
					"PaymentStatus",
					"DomesticPaymentRequest.PaymentStatus",
					(InstrumentServices.isBlank(paymentStatus) | isTemplate) ? ""
							: ReferenceDataManager
									.getRefDataMgr()
									.getDescr(
											TradePortalConstants.BENEFICIARY_PAYMENT_STATUS,
											currentDomesticPayment
													.getAttribute("payment_status")),
					"20", true, false, false, "", "", "")%>


	<input type=hidden name="PaymentStatus"
		value="<%=(InstrumentServices.isBlank(paymentStatus) | isTemplate) ? ""
					: paymentStatus%>">
	<%-- cquinton 2/22/2011 Rel 6.1.0 ir#rvul020969252 end --%>


	<%=widgetFactory.createTextField("PaymentSystemReference",
					"DomesticPaymentRequest.PaymentSystemReference",
					currentDomesticPayment.getAttribute("payment_system_ref"),
					"20", true, false, false, "", "", "")%>

</div>
<%-- columnLeft End --%>

<div class="columnRight">



	<%=widgetFactory.createTextArea("ErrorText",
					"DomesticPaymentRequest.ErrorText",
					currentDomesticPayment.getAttribute("error_text"), true,
					false, false, "", "rows: '5', cols: '20'", "")%>




	<%
		if (!(isReadOnly || isStatusVerifiedPendingFX || TradePortalConstants.INDICATOR_YES
				.equals(transaction.getAttribute("uploaded_ind")))) {
	%>
	<div style="float: right;">
		<div id="save_ben_button_div" class='formItem inline'>
			<button data-dojo-type="dijit.form.Button" type="button"
				name="saveBenButton" id="saveBenButton" class="SearchButton">
				<%=resMgr.getText(
						"DomesticPaymentRequest.SaveBeneficiary",
						TradePortalConstants.TEXT_BUNDLE)%>
				<script type="dojo/method" data-dojo-event="onClick"
					data-dojo-args="evt">
					dijit.byId("DomesticPaymentOID").set("value",null);		        	
					dijit.byId('DomesticPaymentOID').set('checked', false);
           <%--- Suresh_L Rel8.4 Fix for IR-T36000013975  - start---%> 

		    if(setButtonPressed('<%=TradePortalConstants.BUTTON_SAVETRANS%>', '0')==true){
                     dijit.byId("saveBenButton").set("disabled",true);
                     document.forms[0].submit();
              }else{
                     dijit.byId("saveBenButton").set("disabled",false);
                 }
	       <%--- Suresh_L Rel8.4 Fix for IR-T36000013975  -  end ---%> 				
				</script>
			</button>
		</div>
		<div id="update_ben_button_div" class='formItem inline'>
			<button data-dojo-type="dijit.form.Button" type="button"
				name="updateBenButton" id="updateBenButton" class="SearchButton">
				<%=resMgr.getText("common.UpdateBeneficiaryText",
						TradePortalConstants.TEXT_BUNDLE)%>
				<script type="dojo/method" data-dojo-event="onClick"
					data-dojo-args="evt">
		        	//dijit.byId("DomesticPaymentOID").set("value",'<%//=EncryptDecrypt.encryptStringUsingTripleDes(currentDPOid, userSession.getSecretKey())%>');
		  			setButtonPressed('<%=TradePortalConstants.BUTTON_SAVETRANS%>', '0');
					document.forms[0].submit();
				</script>
			</button>
		</div>
		<div id="cancel_ben_button_div" class='formItem inline'>
			<button data-dojo-type="dijit.form.Button" type="button"
				name="cancelButton" id="cancelButton" class="SearchButton">
				<%=resMgr.getText("common.cancel",
						TradePortalConstants.TEXT_BUNDLE)%>
				<script type="dojo/method" data-dojo-event="onClick"
					data-dojo-args="evt">
		  			reloadBeneficiaryFormDetails();
				</script>
			</button>
		</div>
	</div>
	<%
		}
	%>
</div>
<%-- columnRight End --%>
<div style="clear: both"></div>
</div>
<%-- 2.Beneficiary End --%>
<%--AAlubala Rel 7.0.0.0 CR610 Need hidden fields to hold accnt owners for purposes of displaying to the user
       when a different option field is selected - triggered by the onchnage javascript event handler
       IAZ 04/09/11 Use OTHER_ACCOUNT_OWNER_OID to retrieve account owner's name (if needed).
       BEGIN
       --%>
<%
	DocumentHandler allAccnts = new DocumentHandler(payerAcctList, true);
	//if (acctOid !=null && !acctOid.equals("")) {
	Vector vector = allAccnts.getFragments("/DocRoot/ResultSetRecord");
	int numItems = vector.size();
	String accntSQL = "";
	DocumentHandler ownerCorpOrg;
	for (int i = 0; i < numItems; i++) {

		DocumentHandler doc1 = (DocumentHandler) vector.elementAt(i);
		String acct_oid1 = doc1.getAttribute("/ACCOUNT_OID");
		String acct_num1 = doc1.getAttribute("/ACCOUNT_NUMBER");
		String acct_desc1 = doc1.getAttribute("/ACCOUNT_DESCRIPTION");
		String acct_currency1 = doc1.getAttribute("/CURRENCY");
		String accntOwnerCorpID = doc1
				.getAttribute("/OTHER_ACCOUNT_OWNER_OID");
		//check to ensure description is not null
		StringBuffer sbDisplay = new StringBuffer();
		sbDisplay.append(acct_num1);
		if (InstrumentServices.isNotBlank(acct_desc1)) {
			sbDisplay.append("-");
			sbDisplay.append(acct_desc1);
		}
		if (InstrumentServices.isNotBlank(acct_currency1)) {
			sbDisplay.append(" (");
			sbDisplay.append(acct_currency1);
			sbDisplay.append(")");
		}
		String display = sbDisplay.toString();
		String nameToDisplay;
		if (InstrumentServices.isBlank(accntOwnerCorpID)) {
			//nameToDisplay = corporateOrgToDisplay;
			nameToDisplay = corporateOrg.getAttribute("name");
		} else {
			//retrieve the account owner organization's name
			accntSQL = "select name from corporate_org where organization_oid = ?" ;
			ownerCorpOrg = DatabaseQueryBean.getXmlResultSet(accntSQL, false, new Object[]{accntOwnerCorpID});
			nameToDisplay = ownerCorpOrg
					.getAttribute("ResultSetRecord/NAME");
		}
%>
<input type=hidden name="<%=display%>" id="<%=display%>"
	value="<%=nameToDisplay%>" />
<%
	}
	// }
%>
<%--CR610 END --%>
<%
	isFixedValue = currentDomesticPayment.isFixedValue(
			"bank_charges_type", isTemplate);
	if (isFixedValue || isStatusVerifiedPendingFX)
		out.println("<input type=hidden name=BankChargesType value=\""
				+ bankChargesType + "\">");
%>

<%
	Date date_1 = null;
	SimpleDateFormat sdf_1 = null;
	SimpleDateFormat iso_1 = null;
	//payment_date is a ISO Date format i.e.M/dd/yyyy HH:mm:ss.S(the format returned
	// from the database). Convert it to yyyy-MM-dd HH:mm:ss.S  
	String result_1 = transaction.getAttribute("payment_date");

	//sdf_1 = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
	//iso_1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 

	sdf_1 = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
	iso_1 = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");

	try {
		date_1 = iso_1.parse(result_1);
		result_1 = sdf_1.format(date_1);
	} catch (Exception e) {
		System.out.println("Date Format Exception  : " + e);
	}
%>

<script LANGUAGE="JavaScript">


<%if (!isTemplate) {

				out.println("function confirmDelete()");
				out.println("{ var msg = 'Are you sure you want to delete this item?'");
				out.println("if(!confirm(msg)) {formSubmitted = false; return false;}");
				out.println("return true;}");

			}%>
</script>
<script LANGUAGE="JavaScript">

<%-- var localDate = dojo.byId("payment_date1").value + " 00:00:00"; --%>
 
   function updateDMPaymentDate() {
	
	<%-- window.alert(document.forms[0].payment_date.value); --%>
	<%-- Change date format as required in DateTimeAttribute.java file --%>
	<%-- var x = new Date(dojo.byId("payment_date1").value); --%>
	<%-- x = dojo.date.locale.format(x, {datePattern: "MM/dd/yyyy HH:mm:ss", selector: "date"}); --%>
	
	
	<%-- document.forms[0].payment_date.value = x; --%>

	document.getElementById("ValueDate").innerHTML = ""; 
  }
	
  function updatePaymentCCY()
  {
 
  
  document.getElementById('TransactionCurrencyDisplay').innerHTML = document.getElementById('TransactionCurrencyDD').value;
   <%--  document.forms[0].TransactionCurrencyDisplay.value = document.getElementById('TransactionCurrencyDD').value; --%>
    document.getElementById('TransactionCurrency').value = document.getElementById('TransactionCurrencyDD').value;
    document.getElementById("ValueDate").innerHTML = ""; 
  
  }
  
<%-- BSL CR-655 02/28/11 Begin --%>
var xmlhttp = null;



function getXmlHttpRequest() {
	var xmlHttpObj;
	if (window.XMLHttpRequest) {
		xmlHttpObj = new XMLHttpRequest();
	}
	else {
		try {
			xmlHttpObj = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
			try {
				xmlHttpObj = new ActiveXObject("Microsoft.XMLHTTP");
			} catch (e) {
				xmlHttpObj = false;
			}
		}
	}
	return xmlHttpObj;
}

function executeChangeBankGroup(bankGroupOID) {
	var getstr  = "BankGroupOID=" + bankGroupOID + "&"
	getstr = getstr + "TranOID=" + <%=tranOid%> + "&";
<%for (int i = 0; i < TradePortalConstants.MAX_REP_CODE_IDX; i++) {%>
	getstr = getstr + "IsReadOnlyRptgCode<%=i + 1%>=<%=isReadOnlyRptgCode[i]%>&";
<%}%>
	if (xmlhttp == null) {
		xmlhttp = getXmlHttpRequest();
	}
	xmlhttp.open("GET", "/portal/transactions/Transaction-FTDP-ISS-ChangeDebitAccount.jsp?" + getstr, true);
	xmlhttp.onreadystatechange = updateNewDebitAccount;
	xmlhttp.send(null);
}

function updateNewDebitAccount() {
	if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
		var rptgDiv = document.getElementById("rptgCodesNonVis");
		rptgDiv.innerHTML = xmlhttp.responseText;
		
		updateReportingCodes();
		<%--  DK CR-640 Rel7.1 BEGINS --%>
		<%-- var fxOnlineAvailableDiv = document.getElementById("fxOnlineAvailableNonVis"); --%>
		<%-- fxOnlineAvailableDiv.innerHTML = xmlhttp.responseText; --%>
		<%-- Vshah - IR#PUL121367992 - Rel7.1 - Comented out below line... --%>
		<%-- var liveMarketRateInd = '<%=userSession.hasAccessToLiveMarketRate()%>'; //DK IR-PIUL103154077 Rel7.1 --%>
		var fx = document.getElementsByName("hidden_fxInd")[0].value;
		<%-- Vshah - IR#PUL121367992 - Rel7.1 - Comented out below "if" condition and re-written that...		 --%>
		<%-- if(fx=='Y' && liveMarketRateInd=='true') --%>
		if(fx=='Y'){		
			document.getElementById('requestMarketRate').style.visibility='visible';
			document.getElementById('requestMarketRate').style.display='block';
		}else {
			document.getElementById('requestMarketRate').style.visibility='hidden';
			document.getElementById('requestMarketRate').style.display='none';
			document.forms[0].covered_by_fec_number.disabled=false; <%-- IR#DEUL120562739 - Rel7.1 - 12/09/2011 --%>
			document.forms[0].fec_rate.disabled=false; <%-- IR#DEUL120562739 - Rel7.1 - 12/09/2011 --%>
		}	
			
		<%--  DK CR-640 Rel7.1 ENDS --%>
	}
}

function updateReportingCodes() {
<%for (int i = 0; i < reportingCodeNames.length; i++) {%>
	updateReportingCode("<%=reportingCodeNames[i]%>","<%=selectedRptgCodes[i]%>");
<%}%>
}


function updateReportingCode(reportingCodeName, selectedRptgCode) {

	
	require(["dijit/registry", "dojo/ready"],
		function(registry, ready ) {
           var rptgCode = document.getElementsByName("hidden_" + reportingCodeName)[0];
		   var rpt = dijit.byId(reportingCodeName);
			
			if (rptgCode) {
				rpt.store.fetch({
					query: {name: '*'},
					onComplete: function (items) {
						dojo.forEach(items, function (item, i) {
							rpt.store.remove(item.id);
						});
					}
				}); 
				
				var opt= null;

				for (i=0;i<rptgCode.length;i++) {
					opt= rptgCode.options[i];
					rpt.store.add({disabled:false,id:opt.value,name:opt.text,label:opt.text,selected:false,value:opt.value});
				}
				rpt.reset();
				rpt.store.fetch({
							query: {name: '*'},
							onComplete: function (items) {
								  rpt.set('value', selectedRptgCode);
							}
						});
			}

		});
	
	
}

function updatePayerAccount() {
	
	var accountOID = dijit.byId("AppDebitAcctPrincipal").value;
	var acctBankGroupSelect = document.getElementsByName("acctBankGroupSelection")[0];
	var oldBankGroupOID = null;
	var newBankGroupOID = null;
	
	
	for (var j=0; j<acctBankGroupSelect.length; j++) {
		if (acctBankGroupSelect.value == acctBankGroupSelect.options[j].value) {
			oldBankGroupOID = acctBankGroupSelect.options[j].text;
		}
		if (accountOID == acctBankGroupSelect.options[j].value) {
			newBankGroupOID = acctBankGroupSelect.options[j].text;
		}
		if (oldBankGroupOID != null && newBankGroupOID != null) {
			break;
		}
	}
	
	acctBankGroupSelect.value = accountOID;	
	if (oldBankGroupOID != newBankGroupOID) {
		if(newBankGroupOID >''){
			executeChangeBankGroup(newBankGroupOID);
			}
		else {
			document.getElementById('requestMarketRate').style.visibility='hidden';
			document.getElementById('requestMarketRate').style.display='none';
			document.forms[0].covered_by_fec_number.disabled=false; <%-- IR#DEUL120562739 - Rel7.1 - 12/09/2011 --%>
			document.forms[0].fec_rate.disabled=false; <%-- IR#DEUL120562739 - Rel7.1 - 12/09/2011 --%>

			}		
	}
	document.forms[0].RequestMarketRate.checked = false;
	document.forms[0].covered_by_fec_number.disabled=false; <%-- IR#DEUL120562739 - Rel7.1 - 12/09/2011 --%>
	document.forms[0].fec_rate.disabled=false; <%-- IR#DEUL120562739 - Rel7.1 - 12/09/2011 --%>
	
	updatePayerAccCurrency();
}
<%-- BSL CR-655 02/28/11 End --%>
    
  function updatePayerAccCurrency() {

    var account = dijit.byId('AppDebitAcctPrincipal').attr('displayedValue');
    var accountVal = dijit.byId('AppDebitAcctPrincipal').value;

    <%-- AAlubala Rel 7.0.0.0 CR 610 02/28/2011 --%>
    <%-- var orderingParty = document.getElementById("126854USD00013_Dont use (USD)").value;     --%>
    var orderingParty;
    	    if(accountVal > ''){
    	    	orderingParty = document.getElementById(account).value;
	    	}
    document.getElementById("OrderingPartyNameDisplay").innerHTML = orderingParty;
    document.getElementById('OrderingPartyName').value=orderingParty;
    document.getElementById("corporateAlternateName").innerHTML="";
    var i = 0;
    var currency ="";

    var aindex = account.indexOf("(");

    if (aindex != -1)
    	currency = account.substring(aindex+1, aindex+4);    
    var aTC = dijit.byId('TransactionCurrencyDD');
   
  <%--  if (aTC.value == "") { --%>
    	document.getElementById('TransactionCurrencyDisplay').innerHTML = currency;
    	document.getElementById('TransactionCurrency').value = currency;
    	
    	aTC.set('value', currency);    	
  <%--  } --%>
    document.getElementById("ValueDate").innerHTML = ""; 
  }    
    

  
  function updateTotalAmount() {
  
      var displayTranAmtfromDB = document.forms[0].displayTransactionAmount.value;
      var calculatedTranAmt = document.forms[0].DisplayTotalPaymentsAmount.value;
      <%-- window.alert(displayTranAmtfromDB + " " + calculatedTranAmt) --%>
      if (displayTranAmtfromDB != calculatedTranAmt)
      {
      	document.forms[0].TransactionAmount.value = calculatedTranAmt;
      	document.forms[0].displayTransactionAmount.value = calculatedTranAmt;
      	document.forms[0].FundingAmount.value = calculatedTranAmt;
      }      
  }
    
  function updateTermsDescription() {
  
    <%-- document.forms[0].UseOtherText.value = --%>
    <%-- 	  document.forms[0].PayerDescription.value; --%>
    document.forms[0].PayerRefNum.value =    	  
    	  document.forms[0].PayerDescription.value;
  }

function updatePayeePartyName()
{
		
	document.forms[0].PayeeTermsPartyName.value = 
          document.forms[0].PayeeName.value.substring(0,35);
        document.forms[0].ContactName.value = 
          document.forms[0].PayeeName.value.substring(0,35);
			
}

function updatePayeeAddressLine()
{
	document.forms[0].FirstTermsPartyAddress1.value = 
          document.forms[0].PayeeAddrL1.value;
    document.forms[0].FirstTermsPartyAddress2.value = 
          document.forms[0].PayeeAddrL2.value;
    document.forms[0].FirstTermsPartyAddress3.value = 
          document.forms[0].PayeeAddrL3.value;
    if(dijit.byId("Country")){
    	document.forms[0].FirstTermsPartyCountry.value = 
    		dijit.byId("Country").value;
    }else
    	document.forms[0].FirstTermsPartyCountry.value = "";
   
}

<%--  IAZ CM CR-507 12/19/09 Begin. --%>
function selectOtherCharges()
{
  var size = document.forms[0].BankChargesType.length;
  var counter = size - 1
  while(counter >=0)
  {
  	if (document.forms[0].BankChargesType[counter].value == '<%=TradePortalConstants.CHARGE_OTHER%>')
  	{
  		document.forms[0].BankChargesType[counter].checked = true;
  	    break;
  	}
  	counter--;	
  }
}

<%--  IAZ IR-SSUK012149800 01/27/10 and IR-PDUL052658738 06/14/11 Begin --%>
function pmDynamicUpdates()
{
	require(["dojo/dom-class"], function(domClass){
		   <%-- Srinivasu_D IR#T36000043520 Rel9.3 ER9.3.0.1 09/09/2015 --%>
		if(dijit.byId("PaymentMethodType")){
			if( dijit.byId("PaymentMethodType").value!="<%=TradePortalConstants.CROSS_BORDER_FIN_TRANSFER%>" ){
				dijit.byId("PayeeName").setAttribute("maxLength",140);
				
			}
			<%-- Srinivasu_D IR#T36000043520 Rel9.3 ER9.3.0.1 09/09/2015 -End --%>
			if( dijit.byId("PaymentMethodType").value=="<%=TradePortalConstants.CROSS_BORDER_FIN_TRANSFER%>" ){
				domClass.add('domesticCountryLabel', 'required');
				
				document.getElementById("payeeAddrLine4").style.display = "none";
				document.getElementById("instructionNumber").style.display = "block";
				document.getElementById("benBankDivID").style.display = "block";
				document.getElementById("firstIntBankDivID").style.display = "block";
				document.getElementById("regulationReportingDivID").style.display = "block";
				document.getElementById("checkDeliveryDetailDivID").style.display = "none";
				domClass.remove('deliveryMethodID', 'required');
				domClass.add('benBankCodeLabel', 'required');	
				domClass.add('enterAccountLabelDiv', 'required');
				document.getElementById("payeeFaxNumberId").style.display = "block";				
				dijit.byId("PayeeName").setAttribute("maxLength",35);
				document.forms[0].PayeeTermsPartyName.value = document.forms[0].PayeeName.value.substring(0,35);
			    document.forms[0].ContactName.value = document.forms[0].PayeeName.value.substring(0,35);
			}else if( (dijit.byId("PaymentMethodType").value=="<%=TradePortalConstants.PAYMENT_METHOD_BCHK%>") 
					|| (dijit.byId("PaymentMethodType").value=="<%=TradePortalConstants.PAYMENT_METHOD_CCHK%>") ){
				domClass.add('deliveryMethodID', 'required');
				
				domClass.remove('domesticCountryLabel', 'required');	
				domClass.remove('benBankCodeLabel', 'required');
				domClass.remove('enterAccountLabelDiv', 'required');
				document.getElementById("payeeAddrLine4").style.display = "block";
				document.getElementById("instructionNumber").style.display = "none";
				document.getElementById("benBankDivID").style.display = "none";
				document.getElementById("firstIntBankDivID").style.display = "none";
				document.getElementById("regulationReportingDivID").style.display = "none";
				document.getElementById("checkDeliveryDetailDivID").style.display = "block";
				<%--  Sandeep IR-T36000010513 01/23/2013 Start --%>
					document.getElementById("payeeFaxNumberId").style.display = "none";
				<%--  Sandeep IR-T36000010513 01/23/2013 End --%>
			}else if( (dijit.byId("PaymentMethodType").value=="<%=TradePortalConstants.PAYMENT_METHOD_RTGS%>") 
					|| (dijit.byId("PaymentMethodType").value=="<%=TradePortalConstants.PAYMENT_METHOD_ACH_GIRO%>") 
					|| (dijit.byId("PaymentMethodType").value=="<%=TradePortalConstants.PAYMENT_METHOD_BKT%>") ){
				
				document.getElementById("firstIntBankDivID").style.display = "none";
				document.getElementById("checkDeliveryDetailDivID").style.display = "none";
				document.getElementById("regulationReportingDivID").style.display = "block";
				document.getElementById("instructionNumber").style.display = "block";
				document.getElementById("benBankDivID").style.display = "block";
				document.getElementById("payeeFaxNumberId").style.display = "block";
				domClass.add('enterAccountLabelDiv', 'required');
				
			}else{
				if(domClass.contains('domesticCountryLabel', 'required')){			
					domClass.remove('domesticCountryLabel', 'required');	
					document.getElementById("payeeAddrLine4").style.display = "block";
				}
				if(domClass.contains('deliveryMethodID', 'required')){
					domClass.remove('deliveryMethodID', 'required');	
				}
				if(!domClass.contains('benBankCodeLabel', 'required')){
					domClass.add('benBankCodeLabel', 'required');	
				}
				if(!domClass.contains('enterAccountLabelDiv', 'required')){
					domClass.add('enterAccountLabelDiv', 'required');	
				}
				document.getElementById("instructionNumber").style.display = "block";
				document.getElementById("benBankDivID").style.display = "block";
				document.getElementById("firstIntBankDivID").style.display = "block";
				document.getElementById("regulationReportingDivID").style.display = "block";
				document.getElementById("checkDeliveryDetailDivID").style.display = "block";
				document.getElementById("payeeFaxNumberId").style.display = "block";
			}
		}
	});
	
	
    
}
<%--  IAZ IR-SSUK012149800 01/27/10 and IR-PDUL052658738 06/14/11 End --%>
<%--  IAZ CM CR-507 12/19/09 End --%>

<%-- IAZ CR-573 06/08/10 Begin --%>
function updateBankBranchCode(partyIndicator)
{
    if (partyIndicator == '<%=TradePortalConstants.PAYER_BANK%>')
		document.forms[0].PRB_BBCode_Updated.value = 'Y';
    if (partyIndicator == '<%=TradePortalConstants.ORDERING_PARTY_BANK%>')
		document.forms[0].OPK_BBCode_Updated.value = 'Y';
    if (partyIndicator == '<%=TradePortalConstants.FIRST_INTERMED_BANK%>')
		document.forms[0].IP1_BBCode_Updated.value = 'Y';
    if (partyIndicator == '<%=TradePortalConstants.SECND_INTERMED_BANK%>')
		document.forms[0].IP2_BBCode_Updated.value = 'Y';
		
	
}
<%-- IAZ CR-573 06/08/10 End --%>

<%-- SHILPA R CR-597 start --%>
function updatePayeeInvoiceDetail()
{
	<%-- BSL IR#PUL032965444 04/04/11 --%>
	<%-- document.forms[0].PaymentInvoiceDetailChanged.value = TradePortalConstants.INDICATOR_YES; --%>
	document.forms[0].PaymentInvoiceDetailChanged.value = "<%=TradePortalConstants.INDICATOR_YES%>";
	<%-- MEer Rel 8.3 IR-23112 comment out as NOT working/required --%>
	<%-- var size = document.forms[0].payeeInvoiceDetails.length;
    if(size>8000){
	document.forms[0].payeeInvoiceDetails.value =  document.forms[0].payeeInvoiceDetails.value.substring(0,8000);
} --%>

}
<%-- SHILPA R CR-597 end --%>

<%-- BSL IR#SBUL040440564 04/05/11 Begin --%>
<%-- Enable form submission by pressing the enter key. --%>
<%--  event.which works for Netscape and event.keyCode works for IE. --%>
function enterSubmit(event, myform) {
	if (event && (event.which == 13 || event.keyCode == 13)) {
		<%-- setButtonPressed('null', 0);//BSL IR# SBUL040440564 04/27/11 - change 'null' to 'ModifyPayee' --%>
		setButtonPressed('ModifyPayee', 0);
		myform.submit();
	}
}
<%-- BSL IR#SBUL040440564 04/05/11 Begin --%>

<%--  DK CR-640 Rel7.1 BEGINS --%>
 function getMarketRate(){

            require(["t360/dialog"], function(dialog){
			dialog.open('MarketRate', '<%=resMgr.getText("MarketRate.dialogTitle",
					TradePortalConstants.TEXT_BUNDLE)%>',
			'marketRateDialog.jsp',
			  ['selectedInstrument','showInfoText'],['<%=selectedInstrument%>','false'],
			'select', null);
			});
		}

</script>

<%-- Hidden fields start --%>
<%-- Nar IR-T36000020735 include Execution date only when it is read only mode--%>
<%-- KMehta @ Rel 8400 IR T36000026081 added condition for Fixed Payment Start--%>
<%
	if (isReadOnly || isFixedPayment) {
%>
<input type=hidden name="payment_date" value="<%=displayDPDate%>">
<%
	}
%>
<%-- KMehta @ Rel 8400 IR T36000026081 added condition for Fixed Payment End--%>

<input type=hidden name=equivalent_exch_amt_ccy
	value=<%=equivPaymentCCY%>>
<input type=hidden name='PayeeBankTermsPartyType'
	value=<%=TradePortalConstants.PAYEE_BANK%>>
<input type=hidden name="PayeeBankOTLCustomerId"
	value="<%=termsPartyPayeeBank.getAttribute("OTL_customer_id")%>">
<input type=hidden name='PayeeTermsPartyType'
	value=<%=TradePortalConstants.PAYEE%>>
<input type=hidden name="PayeeOTLCustomerId"
	value="<%=termsPartyPayee.getAttribute("OTL_customer_id")%>">
<input type=hidden name="VendorId"
	value="<%=termsPartyPayee.getAttribute("vendor_id")%>">

<input type=hidden name=PayeeAccount id=PayeeAccount
	value=<%=currentDomesticPayment
					.getAttribute("payee_account_number")%>>
<input type=hidden name="beneficiaryPartyOid" id="beneficiaryPartyOid"
	value=<%=EncryptDecrypt.encryptStringUsingTripleDes(
					currentDomesticPayment
							.getAttribute("beneficiary_party_oid"), userSession
							.getSecretKey())%>>
<input type=hidden name=c_FirstIntermediaryBank
	id="c_FirstIntermediaryBank"
	value=<%=EncryptDecrypt.encryptStringUsingTripleDes(
					currentDomesticPayment
							.getAttribute("c_FirstIntermediaryBank"),
					userSession.getSecretKey())%>>
<input type=hidden name=FisrtIntermediaryBankOid
	id=FisrtIntermediaryBankOid
	value=<%=firstIntBank.getAttribute("payment_party_oid")%>>
<input type=hidden name="InvoiceDetailOID" id="InvoiceDetailOID"
	value="<%=paymentInvoiceDetails.getAttribute("invoice_detail_oid")%>">
<input type=hidden name="PaymentInvoiceDetailChanged"
	value="<%=invDetailsChanged%>">
<%-- // Don't saveoid thorugh uoid. it is saved when new instrument is created. this fild  will have only  
     // when beneficiary is copied from fixed template
<input type=hidden name=source_template_dp_oid
	value=<%=currentDomesticPayment
					.getAttribute("source_template_dp_oid")%>>
--%>
<input type=hidden name=source_template_trans_oid
	value=<%=terms.getAttribute("source_template_trans_oid")%>>
<input type=hidden name=fixedPmtInd value=<%=canNotAddPayees%>>

<input type=hidden name=DisplayTotalPaymentsAmount
	value="<%=totalPaymentsAmountStr%>">
<%-- BSL Rel 6.1 IR #VEUL031445617 03/23/11 - use BigDecimal.toPlainString() --%>
<input type=hidden name=TotalPaymentsAmount
	value="<%=totalPaymentsAmount.toPlainString()%>">


<input type=hidden name=PayerRefNum
	value="<%=terms.getAttribute("reference_number")%>">
<input type=hidden name=TransactionCurrency id="TransactionCurrency"
	value="<%=transactionCCY%>">
<input type=hidden name=TransactionAmount
	value="<%=totalPaymentsAmount.toPlainString()%>">

<%-- IAZ CR-573 06/08/10 Begin: these indicate whether data for corresponding bank parties must be retrieved form DB on Save() --%>
<input type=hidden name="PRB_BBCode_Updated"
	value="<%=TradePortalConstants.INDICATOR_NO%>">
<input type=hidden name="OPK_BBCode_Updated"
	value="<%=TradePortalConstants.INDICATOR_NO%>">
<input type=hidden name="IP1_BBCode_Updated"
	value="<%=TradePortalConstants.INDICATOR_NO%>">
<input type=hidden name="IP2_BBCode_Updated"
	value="<%=TradePortalConstants.INDICATOR_NO%>">
<input type=hidden name="ins_c_id" id="ins_c_id"
	value="<%=instrument.getAttribute("complete_instrument_id") + "-"%>">
<input type=hidden name="acctBankOrgid" id="acctBankOrgid"
	value="<%=acctBankOrgid%>">

<%-- Hidden fields end --%>
