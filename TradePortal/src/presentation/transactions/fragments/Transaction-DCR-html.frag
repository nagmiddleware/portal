<%--
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
--%>
<%--
*******************************************************************************
      Discrepancy Response Page - all sections

  Description:
     This page is used by the Transaction-INC_SLC-DCR-EXP_DLC-DCR.jsp and
	 Transaction-SLC-DCR-IMP_DLC-DCR.jsp to display discrepancy transaction
	 HTML.  All data retrieval
	 is handled in the Transaction-INC_SLC-DCR-EXP_DLC-DCR.jsp. or
	 Transaction-SLC-DCR-IMP_DLC-DCR.jsp

	 Special assumptions are that the following fields are declared and populated:

	 String discrepancyInstructions
	 String instrumentType

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transaction-DCR.jsp" %>
*******************************************************************************
--%>


<%--
*******************************************************************************
     Discrepancy Response Transaction Page - General section
*******************************************************************************
--%>
		<div><br/>
			<%
				presentationAmount.append(currencyCode);
			    presentationAmount.append(" ");
			    presentationAmount.append(displayAmount);
			%>
			<%= widgetFactory.createTextField("PresentationAmount", "DiscrepancyResponse.PresentationAmount",
				presentationAmount.toString(),
				"35", true, false, false, "", "", "inline") %>

			<%= widgetFactory.createTextField("PresentationNumber", "DiscrepancyResponse.PresentationNumber", terms.getAttribute("drawing_number"),
				"35", true, false, false, "", "", "inline") %>

			<%
			 	String presentationDateSql = "select presentation_date from mail_message where  response_transaction_oid= ?";
			  	DocumentHandler results = DatabaseQueryBean.getXmlResultSet(presentationDateSql, false, new Object[]{transaction.getAttribute("transaction_oid")});

			  if(results != null)
			   {
			      presentationDate = results.getAttribute("/ResultSetRecord(0)/PRESENTATION_DATE");
			     //The retrieved presentationDate value would be in the format of "2007-10-25 00:00:00.0"
			   }
			    //As "PresentationDate" is a newly added attribute empty check is being done here to avoid exceptions
			    // for existing transactions
			    if(InstrumentServices.isNotBlank(presentationDate))
			    {
			%>
			<%= widgetFactory.createTextField("PresentationNumber", "DiscrepancyResponse.PresentationDate",
				TPDateTimeUtility.formatDate(presentationDate, TPDateTimeUtility.SHORT, loginLocale),
				"35", true, false, false, "", "", "inline") %>
			<%
				}
			   else
				{
			%>
				<%= widgetFactory.createTextField("PresentationNumber", "DiscrepancyResponse.PresentationDate", "", "35", true, false, false, "", "", "inline") %>
			<%}%>

			<%= widgetFactory.createTextField("OtherParty", "MailMessage.OtherParty", counterPartyName, "35", true, false, false, "", "", "inline") %>
			<div style="clear: both;"></div>
		</div>

		<%@ include file="Transaction-Documents.frag" %>
		<%= widgetFactory.createLabel("", "DiscrepancyResponse.Notice", false, true, false, "") %>

 			<div class="formItem">
				<%
				    // if instrument type is export LC or incoming standby LC,
					// display the four radio buttons for export discrepancy instruction options,
					// otherwise display the three buttons for import discrepancy instructions.

				    if (instrumentType.equals(InstrumentType.EXPORT_DLC) ||
						instrumentType.equals(InstrumentType.INCOMING_SLC))
				    {
				%>

				<%= widgetFactory.createRadioButtonField("DiscrepancyInstructions", "TradePortalConstants.EXP_SEND_APPR", "DiscrepancyResponse.SendDocsForApproval",
					TradePortalConstants.EXP_SEND_APPR, discrepancyInstructions.equals(TradePortalConstants.EXP_SEND_APPR), isReadOnly, "", "") %>
				<%if(!isReadOnly) {%>
					<%=widgetFactory.createHoverHelp("TradePortalConstants.EXP_SEND_APPR","DiscrepancyResponse.SendDocsForApproval") %>
				<%}%>				
				<br/>
				<%= widgetFactory.createRadioButtonField("DiscrepancyInstructions", "TradePortalConstants.EXP_SWIFT_AND_HOLD", "DiscrepancyResponse.ApprovalHoldDocuments",
					TradePortalConstants.EXP_SWIFT_AND_HOLD, discrepancyInstructions.equals(TradePortalConstants.EXP_SWIFT_AND_HOLD), isReadOnly) %>
				<%if(!isReadOnly) {%>
					<%=widgetFactory.createHoverHelp("TradePortalConstants.EXP_SWIFT_AND_HOLD","DiscrepancyResponse.ApprovalHoldDocuments") %>
				<%}%>					
				<br/>
				<%= widgetFactory.createRadioButtonField("DiscrepancyInstructions", "TradePortalConstants.EXP_SWIFT_AND_SEND", "DiscrepancyResponse.ApprovalSendDocuments",
					TradePortalConstants.EXP_SWIFT_AND_SEND, discrepancyInstructions.equals(TradePortalConstants.EXP_SWIFT_AND_SEND), isReadOnly) %>
				<%if(!isReadOnly) {%>
					<%=widgetFactory.createHoverHelp("TradePortalConstants.EXP_SWIFT_AND_SEND","DiscrepancyResponse.ApprovalSendDocuments") %>
				<%}%>					
				<br/>
				<%= widgetFactory.createRadioButtonField("DiscrepancyInstructions", "TradePortalConstants.EXP_OTHER", "DiscrepancyResponse.Other",
					TradePortalConstants.EXP_OTHER, discrepancyInstructions.equals(TradePortalConstants.EXP_OTHER), isReadOnly) %>
				<%if(!isReadOnly) {%>
					<%=widgetFactory.createHoverHelp("TradePortalConstants.EXP_OTHER","DiscrepancyResponse.Other") %>
				<%}%>						
				<br/>

				<%
					}
					else
					{
					  // display import discrepancy instruction options for Standby LC and Import DLC
				%>

				<%= widgetFactory.createRadioButtonField("DiscrepancyInstructions", "TradePortalConstants.IMP_ACC_NO_DCR", "DiscrepancyResponse.Accepted",
					TradePortalConstants.IMP_ACC_NO_DCR, discrepancyInstructions.equals(TradePortalConstants.IMP_ACC_NO_DCR), isReadOnly) %>
				<%if(!isReadOnly) {%>
					<%=widgetFactory.createHoverHelp("TradePortalConstants.IMP_ACC_NO_DCR","DiscrepancyResponse.Accepted") %>
				<%}%>					
				<br/>
				<%= widgetFactory.createRadioButtonField("DiscrepancyInstructions", "TradePortalConstants.IMP_REJ", "DiscrepancyResponse.Rejected",
					TradePortalConstants.IMP_REJ, discrepancyInstructions.equals(TradePortalConstants.IMP_REJ), isReadOnly) %>
				<%if(!isReadOnly) {%>
					<%=widgetFactory.createHoverHelp("TradePortalConstants.IMP_REJ","DiscrepancyResponse.Rejected") %>
				<%}%>						
				<br/>
				<%= widgetFactory.createRadioButtonField("DiscrepancyInstructions", "TradePortalConstants.IMP_OTHER", "DiscrepancyResponse.Other",
					TradePortalConstants.IMP_OTHER, discrepancyInstructions.equals(TradePortalConstants.IMP_OTHER), isReadOnly) %>
				<%if(!isReadOnly) {%>
					<%=widgetFactory.createHoverHelp("TradePortalConstants.IMP_OTHER","DiscrepancyResponse.ImportOther") %>
				<%}%>					
				<br/>

				 <%
				 	}
				 %>
			</div>
		<%--	<p><%= widgetFactory.createLabel("", "DiscrepancyResponse.AdditionalInstructions", false, false, false, "") %></p><br> --%>
  			<%
			    options = ListBox.createOptionList(discrInstrDocList, "PHRASE_OID", "NAME", "", userSession.getSecretKey());
			%>
			<% String phrase = resMgr.getText("transaction.SelectPhrase", TradePortalConstants.TEXT_BUNDLE);
			if (transactionStatus.equalsIgnoreCase("STARTED")) {	%>
			<%= widgetFactory.createSelectField("DiscrDocsPhraseItem", "DiscrepancyResponse.AdditionalInstructions", phrase, options, isReadOnly, false, false,
				"onChange=" + PhraseUtility.getPhraseChange("/Terms/instr_other",
                                                  "OtherInstructionsText", "1000","document.forms[0].OtherInstructionsText"), "", "")%>
			<% } %>


			<%if(!transactionStatus.equalsIgnoreCase("STARTED")) {	%>
			<%= widgetFactory.createLabel("", "DiscrepancyResponse.AdditionalInstructions", false, false, false, "") %>
			<% } %>
			<%= widgetFactory.createTextArea("OtherInstructionsText", "", terms.getAttribute("instr_other"), isReadOnly, false, false, "maxlength=1000 rows=10", "", "") %>			
			<%if(!isReadOnly) {	%>
			<%=widgetFactory.createHoverHelp("OtherInstructionsText","OtherInstructionsHoverText") %>
			<% } %>
<%--CR-419 Krishna Begin--%>
<script language="JavaScript">


  <%--
  // This function is used to display a popup message confirming that the user
  // wishes to authorize the Discrepancy and at the same time delete the associated Notice.
  --%>
  function confirmAuthorizeDelete() {
    var   confirmMessage = "<%=resMgr.getText("Response.AuthorizeDeletePopupMessage",
                                              TradePortalConstants.TEXT_BUNDLE) %>";

            if (!confirm(confirmMessage))
             {
                formSubmitted = false;
                return false;
             } else {
       return true;
    }
  }



</script>
<%--CR-419 Krishna End--%>
