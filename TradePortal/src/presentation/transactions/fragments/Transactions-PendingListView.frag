<%--
**********************************************************************************
                        Transactions Home Pending Tab

  Description:
    Contains HTML to create the Pending tab for the Transactions Home page

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transactions-PendingListView.jsp" %>
*******************************************************************************
--%>

<%--cquinton 8/30/2012 add grid search header--%>
<div class="gridSearch">
  <div class="searchHeader">
    <span class="searchHeaderCriteria">

	<%
		StringBuffer prependText = new StringBuffer();
		prependText.append(resMgr.getText("PendingTransactions.WorkFor", TradePortalConstants.TEXT_BUNDLE));
		prependText.append(" ");
	
	 	String defaultValue = "";
	    String defaultText  = "";
	
	    if(!userSession.hasSavedUserSession())
	     {
	       defaultValue = EncryptDecrypt.encryptStringUsingTripleDes(MY_WORK, userSession.getSecretKey());
	       defaultText = MY_WORK;
	       
	       dropdownOptions.append("<option value=\"");
	       dropdownOptions.append(defaultValue);
	       dropdownOptions.append("\"");
	       
	       if (selectedWorkflow.equals(defaultText))
	       {
	          dropdownOptions.append(" selected");
	       }
	
	       dropdownOptions.append(">");
	       dropdownOptions.append(resMgr.getText(defaultText, TradePortalConstants.TEXT_BUNDLE));
	       dropdownOptions.append("</option>");
	       
	     }

        // If the user has rights to view child organization work, retrieve the 
        // list of dropdown options from the query listview results doc (containing
        // an option for each child org) otherwise, simply use the user's org 
        // option to the dropdown list (in addition to the default 'My Work' option).

        if (SecurityAccess.hasRights(userSecurityRights, 
                                     SecurityAccess.VIEW_CHILD_ORG_WORK))
        {
           dropdownOptions.append(Dropdown.getUnsortedOptions(hierarchyDoc, 
                                                               "ORGANIZATION_OID", "NAME", 
                                                               prependText.toString(),
                                                               selectedWorkflow, userSession.getSecretKey()));

           // Only display the 'All Work' option if the user's org has child organizations
           if (totalOrganizations > 1)
           {
              dropdownOptions.append("<option value=\"");
              dropdownOptions.append(EncryptDecrypt.encryptStringUsingTripleDes(ALL_WORK, userSession.getSecretKey()));
              dropdownOptions.append("\"");

              if (selectedWorkflow.equals(ALL_WORK))
              {
                 dropdownOptions.append(" selected");
              }

              dropdownOptions.append(">");
              dropdownOptions.append(resMgr.getText(ALL_WORK, TradePortalConstants.TEXT_BUNDLE));
              dropdownOptions.append("</option>");
           }
        }
        else
        {
           dropdownOptions.append("<option value=\"");
           dropdownOptions.append(EncryptDecrypt.encryptStringUsingTripleDes(userOrgOid, userSession.getSecretKey()));
           dropdownOptions.append("\"");

           if (selectedWorkflow.equals(userOrgOid))
           {
              dropdownOptions.append(" selected");
           }

           dropdownOptions.append(">");
           dropdownOptions.append(prependText.toString());
           dropdownOptions.append(userSession.getOrganizationName());
           dropdownOptions.append("</option>");
        }

        
%>
<%= widgetFactory.createSearchSelectField("Workflow","PendingTransactions.Show","", 
		dropdownOptions.toString(),"onChange='searchPendTrans();'" )%> 
<%=widgetFactory.createHoverHelp("Workflow","InstPendingTrans.Show") %> 
<%
        dropdownOptions = Dropdown.createTransStatusOptions(resMgr,selectedStatus);	       
%>

<%= widgetFactory.createSearchSelectField("transStatusType","instrumentSearchDialog.criteria.status","", dropdownOptions.toString(),"onChange='searchPendTrans();'" )%>
<%=widgetFactory.createHoverHelp("transStatusType","InstPendingTrans.Status") %>
    </span>
    <span class="searchHeaderActions">

<%		 newLink = new StringBuffer();
         newLink.append(formMgr.getLinkAsUrl("goToTransactionsHome", response));
         newLink.append("&current2nvNav=");
         newLink.append(TradePortalConstants.NAV__PENDING_TRANSACTIONS);
         newLink.append("&workflow=");
         newLink.append(EncryptDecrypt.encryptStringUsingTripleDes(selectedWorkflow, userSession.getSecretKey()));
%>  


		<% if (SecurityAccess.hasRights(userSecurityRights,SecurityAccess.VIEW_INSTRUMENT_CREATE_OR_MODIFY)) { %>
      <span> 
			<button data-dojo-type="dijit.form.Button"  name="CopySelected" id="CopySelected" type="button">
				<%=resMgr.getText("CopySelected.Button",TradePortalConstants.TEXT_BUNDLE) %>
				<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
					<%-- Method added to get the grid selected row details --%>
					getSelectedGridRow(); 
					openCopySelectedDialogHelper(copySelectedToInstrument);        					
				</script>
			</button>
			<%=widgetFactory.createHoverHelp("CopySelected","InstPendingTrans.CopySelected") %>
      </span>	
		<% }%>

      <%--cquinton 10/8/2012 include gridShowCounts --%>
      <jsp:include page="/common/gridShowCount.jsp">
        <jsp:param name="gridId" value="tradePendGrid" />
      </jsp:include>

      <span id="tradePendRefresh" class="searchHeaderRefresh"></span>
      <%=widgetFactory.createHoverHelp("tradePendRefresh","RefreshImageLinkHoverText") %>
      <span id="tradePendGridEdit" class="searchHeaderEdit"></span>
      <%=widgetFactory.createHoverHelp("tradePendGridEdit","CustomizeListImageLinkHoverText") %>
    </span>
    <div style="clear:both;"></div>
  </div>
</div>

<%-- IR T36000014931 REL ER 8.1.0.4 - Code removed here related to Recertification. --%>

<%
  dgFactory = new DataGridFactory(resMgr,userSession,formMgr,beanMgr,response,condDisplay);
  gridHtml = dgFactory.createDataGrid("tradePendGrid","InstrumentsPendingTransactionsDataGrid");
%>
  
<%= gridHtml %>

<%--cquinton 8/30/2012 - moved all scripts to Transactions-PendingListViewFooter.frag--%>
