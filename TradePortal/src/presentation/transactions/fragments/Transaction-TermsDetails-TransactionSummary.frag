<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
*******************************************************************************
            Transaction Terms Detail Page - Terms Summary section

  Description:   
    This jsp fragment displays the Transaction Summary section for the Terms
  Details page.

  This jsp must be included by the following include:
  	<%@ include file="Transaction-TermsDetails-TransactionSummary.jsp" %>
  Because it is called this way, it is assumed that various variables are 
  defined.  
*******************************************************************************
--%>


<%
  Debug.debug("****** Start of Transaction Summary Section ******");


  String transAmount;
  transAmount = TPCurrencyUtility.getDisplayAmount(
                             transaction.getAttribute("copy_of_amount"), 
                             transaction.getAttribute("copy_of_currency_code"),
                             loginLocale);
  
  
  String loanRate;
  loanRate = terms.getAttribute("loan_rate");
  //Narayan IR-RAUJ041840519 3/26/2010 Begin
  String discountRate = terms.getAttribute("discount_rate");
  if(loanRate==null || loanRate.equals(""))
  {
   loanRate= discountRate;
  }
  //Narayan IR-RAUJ041840519 3/26/2010 End
  loanRate = TPCurrencyUtility.getDisplayAmount(loanRate, "", resMgr.getResourceLocale());
  

%>
<% // 08/10/2015 --- R 94 CR 932.  Added new condition for Billing instrument transactions
if(isBILInstrument){%>
	<%= widgetFactory.createDateField("TransStatusDate", "TransactionTerms.TransStatusDate", transaction.getAttribute("transaction_status_date"), true, false, false, "", "", "inline")%>
	<% //MEer Rel 9.4 CR-932 Retrieved Billing Reason from BankRefdata
	 String purposeType = terms.getAttribute("purpose_type");
	 List<Object> sqlParams = new ArrayList();
	 String billingReason="";
		if(StringFunction.isNotBlank(purposeType)){
			String	billingReasonDescrSql = "select descr as BILLING_DESCRIPTION from bankrefdata where table_type ='BIL_PURPOSE_TYPE' and code = ? and client_bank_id = ? ";
   		    sqlParams.add(purposeType);
       	    sqlParams.add(userSession.getLoginOrganizationId());
       		DocumentHandler result = DatabaseQueryBean.getXmlResultSet(billingReasonDescrSql,false,sqlParams);
       		if(result !=null)
       			 billingReason = result.getAttribute("/ResultSetRecord(0)/BILLING_DESCRIPTION");
		}
	%>
	<%= widgetFactory.createDateField("TransStatusDate", "Billing.BillingReason", billingReason, true, false, false, "", "", "inline")%>
<%}else{%>

	<%= widgetFactory.createDateField("TransStatusDate", "TransactionTerms.TransStatusDate", transaction.getAttribute("transaction_status_date"), true, false, false, "", "", "inline")%>
	<%= widgetFactory.createTextField("Currency", "TransactionTerms.Currency", transaction.getAttribute("copy_of_currency_code"), "10", true, false,false, "", "", "inline")%>
	<%= widgetFactory.createTextField("Amount", "TransactionTerms.Amount", transAmount, "25", true, false,false, "", "", "inline")%>          
<%}%>

<%
 if( hasBankReleasedTerms )
  {
 if ((transactionType.equals(TransactionType.DOC_EXAM)) ||
             (transactionType.equals(TransactionType.PAYMENT)) ||
             (transactionType.equals(TradePortalConstants.PAYABLES_TRANSACTION_TYPE))) 
     {
 %>
 <%= widgetFactory.createTextField("YourReferenceNumber", "TransactionTerms.YourReferenceNumber", terms.getAttribute("reference_number"), "25", true, false,false, "", "", "inline")%> 
	  
<%
	}
  }
%>
<%
      if (isLoanRqst || isRFInstrument) {
%>
	<%= widgetFactory.createTextField("Rate", "TransactionTerms.Rate", loanRate, "25", true, false,false, "", "", "inline")%>
<%
      }
%>
      

<%
  Debug.debug("****** END of Transaction Summary Section ******");
%>
