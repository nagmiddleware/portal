<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
*******************************************************************************
         Export Letter Of Credit Assignment Page - General section

  Description:
    Contains HTML to create the Export Letter Of Credit Assignment General 
    section.

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transaction-EXP_DLC-ASN-General.jsp" %>
*******************************************************************************
--%>

<div class="columnLeft"> 
 <%  
 	String benBankSearchHtml = "";
 	String benBankSearchClearHtml = "";
 	//IR #14533 - Appending assignee's bank details to the assignee.
 	String assigneeBankStr=",AssigneeBankName,AssigneeBankAddressLine1,AssigneeBankAddressLine2,AssigneeBankCity,AssigneeBankStateProvince,AssigneeBankPostalCode,AssigneeBankCountry";
 	String itemid="AssigneeName,AssigneeAddressLine1,AssigneeAddressLine2,AssigneeCity,AssigneeStateProvince,AssigneePostalCode,AssigneeCountry,AssigneePhoneNumber,AssigneeContactName"+assigneeBankStr;
 	String section="exportLc_assigne";
	 if (!(isReadOnly)) {
	     	benBankSearchHtml = widgetFactory.createPartySearchButton(itemid,section,false,TradePortalConstants.ASSIGNEE_01,false);
	     	benBankSearchClearHtml = widgetFactory.createPartyClearButton( "ClearAssigneeButton", "clearAssigneeSearch()",false,"");
	 }
 %>
	<%=widgetFactory.createHoverHelp("exportLc_assigne", "PartySearchIconHoverHelp") %>
	<%=widgetFactory.createHoverHelp("ClearAssigneeButton", "PartyClearIconHoverHelp") %>
	
 <%= widgetFactory.createSubsectionHeader( "Assignment.Assignee",isReadOnly,false, isFromExpress,(benBankSearchHtml+benBankSearchClearHtml)) %> 
 <% secureParms.put("AssigneeTermsPartyOid", termsPartyAssignee.getAttribute("terms_party_oid")); %>
 
        <input type=hidden name="AssigneeTermsPartyType" value=<%=TradePortalConstants.ASSIGNEE_01 %>>
        <input type=hidden name="AssigneeOTLCustomerId" value="<%=termsPartyAssignee.getAttribute("OTL_customer_id")%>">
     
        <%= widgetFactory.createTextField( "AssigneeName", "Assignment.AssigneeName", termsPartyAssignee.getAttribute("name"), "35", isReadOnly, !isTemplate, false,  "onBlur='checkAssigneeName(\"" +termsPartyAssignee.getAttribute("name") + "\")' class='char31'", "","" ) %>                          
    
         <%= widgetFactory.createTextField( "AssigneeAddressLine1", "Assignment.AddressLine1", termsPartyAssignee.getAttribute("address_line_1"), "35", isReadOnly, !isTemplate, false,  "class='char31'", "","" ) %>                          
   
      <%= widgetFactory.createTextField( "AssigneeAddressLine2", "Assignment.AddressLine2", termsPartyAssignee.getAttribute("address_line_2"), "35", isReadOnly,false, false,  "class='char31'", "","" ) %>                          
       
         <%= widgetFactory.createTextField( "AssigneeCity", "Assignment.City", termsPartyAssignee.getAttribute("address_city"), "23", isReadOnly, !isTemplate, false,  "class='char31'", "","" ) %>                          
      <div>
        <%= widgetFactory.createTextField( "AssigneeStateProvince", "Assignment.ProvinceState", termsPartyAssignee.getAttribute("address_state_province"), "8", isReadOnly, false, false,  "", "","inline" ) %>                          
        <%= widgetFactory.createTextField( "AssigneePostalCode", "Assignment.PostalCode", termsPartyAssignee.getAttribute("address_postal_code"), "8", isReadOnly, false, false,  "", "","inline" ) %>
      <div style="clear:both;"></div>
      </div>
          <%
             options = Dropdown.createSortedRefDataOptions(TradePortalConstants.COUNTRY, termsPartyAssignee.getAttribute("address_country"), loginLocale);
          %>
           <%=widgetFactory.createSelectField( "AssigneeCountry", "Assignment.Country"," " , options, isReadOnly,!isTemplate,false, "class='char31'", "", "")%>
             
           <%= widgetFactory.createTextField( "AssigneePhoneNumber", "Assignment.PhoneNumber", termsPartyAssignee.getAttribute("phone_number"), "20", isReadOnly,false, false,  "class='char31'", "regExp:'[0-9]+'","" ) %>    
                                
      		<%= widgetFactory.createTextField( "AssigneeContactName", "Assignment.ContactName", termsPartyAssignee.getAttribute("contact_name"), "35", isReadOnly,false, false,  "class='char31'", "","" ) %>                          
   </div>          
<div class="columnRight"> 


<%  String assBankSearchHtml = "";	
	String itemid2="AssigneeBankName,AssigneeBankAddressLine1,AssigneeBankAddressLine2,AssigneeBankCity,AssigneeBankStateProvince,AssigneeBankPostalCode,AssigneeBankCountry";
	String section2="exportLc_assigneeBank";
	 if (!(isReadOnly)) {
	     	assBankSearchHtml = widgetFactory.createPartySearchButton(itemid2,section2,false,TradePortalConstants.ASSIGNEE_BANK,false);
	     	 
	 }
 %>
 <%=widgetFactory.createHoverHelp("exportLc_assigneeBank", "PartySearchIconHoverHelp") %>
 <%= widgetFactory.createSubsectionHeader( "Assignment.AssigneesBank",isReadOnly,false, isFromExpress,assBankSearchHtml) %> 
 
    <%
       secureParms.put("AssigneeBankTermsPartyOid", termsPartyAssigneeBank.getAttribute("terms_party_oid"));
    %>
        <input type=hidden name="AssigneeBankTermsPartyType"  value=<%=TradePortalConstants.ASSIGNEE_BANK %>>
        <input type=hidden name="AssigneeBankOTLCustomerId" value="<%=termsPartyAssigneeBank.getAttribute("OTL_customer_id")%>">

        <%= widgetFactory.createTextField( "AssigneeBankName", "Assignment.AssigneeBankName", termsPartyAssigneeBank.getAttribute("name"), "35", isReadOnly, !isTemplate, false,  "class='char31' onBlur='checkAssigneeBankName(\"" +termsPartyAssigneeBank.getAttribute("name") + "\")'", "","" ) %>                          
                                   
  	  <%= widgetFactory.createTextField( "AssigneeBankAddressLine1", "Assignment.ABAddressLine1", termsPartyAssigneeBank.getAttribute("address_line_1"), "35", isReadOnly, !isTemplate, false,  "class='char31'", "","" ) %>                          
   
       <%= widgetFactory.createTextField( "AssigneeBankAddressLine2", "Assignment.ABAddressLine2", termsPartyAssigneeBank.getAttribute("address_line_2"), "35", isReadOnly,false, false,  "class='char31'", "","" ) %>                          
   
   
      <%= widgetFactory.createTextField( "AssigneeBankCity", "Assignment.ABCity", termsPartyAssigneeBank.getAttribute("address_city"), "23", isReadOnly, !isTemplate, false,  "class='char31'", "","" ) %>                          
   
      <div>  
      <%= widgetFactory.createTextField( "AssigneeBankStateProvince", "Assignment.ABProvinceState", termsPartyAssigneeBank.getAttribute("address_state_province"), "8", isReadOnly, false, false,  "", "","inline" ) %>
      <%= widgetFactory.createTextField( "AssigneeBankPostalCode", "Assignment.ABPostalCode", termsPartyAssigneeBank.getAttribute("address_postal_code"), "8", isReadOnly, false, false,  "", "","inline" ) %>
      <div style="clear:both;"></div>
      </div>
   
          <%
             options = Dropdown.createSortedRefDataOptions(TradePortalConstants.COUNTRY,   termsPartyAssigneeBank.getAttribute("address_country"), loginLocale);
           %>  
             <%=widgetFactory.createSelectField( "AssigneeBankCountry", "Assignment.ABCountry"," " , options, isReadOnly,!isTemplate,false, "class='char31'", "", "")%>
         
    
                                
   
     <%= widgetFactory.createTextField( "AssigneeAccountNumber", "Assignment.AccountNumber", terms.getAttribute("assignees_account_number"), "35", isReadOnly, false, false,  "class='char31'", "","" ) %>                          
   </div>
   <div style="clear:both;"></div>
     
   <%= widgetFactory.createWideSubsectionHeader( "Assignment.AssignmentAmountDetails",isReadOnly,false, isFromExpress,"") %> 

	 <div style="width:350px; float:left;" class=formItem>
	 	 <%= widgetFactory.createLabel("LCAmount","Assignment.LCAmount",isReadOnly,false,false,"")%>
	          
	      <div class="formItem"><span><%= currencyCode %></span><span class='formItemWithIndent5'><%= originalAmount %></span></div>
	           </div>
	          <div style="width:300px; float:left;">
	       <%= widgetFactory.createLabel("AssignementAmount","Assignment.AssignmentAmount",isReadOnly,true,false,"")%>
	          <div class="formItem">   
	        <span><%= currencyCode %></span><span class='formItem'><%= widgetFactory.createAmountField( "TransactionAmount", "", assignmentAmount, currencyCode, isReadOnly, !isTemplate, false,  "maxlength=20 class='char10'", "","none" ) %></span>                          
	     </div>
	      </div>
	<div style="clear:both;"></div>
   
    <div class="formItemWithIndent4 subsectionWiderHeader required h3"><span class="asterisk">*</span><h3>Partial Payments</h3></div>
   
          <%
           partialPaymentsType = terms.getAttribute("part_payments_type");

           if (partialPaymentsType.equals(""))
           {
              defaultRadioButtonValue1 = false;
              defaultRadioButtonValue2 = false;
           }
           else if (partialPaymentsType.equals(TradePortalConstants.PARTIAL_PAYMENT_TYPE_FULL_AMOUNT))
           {
              defaultRadioButtonValue1 = true;
              defaultRadioButtonValue2 = false;
           }
           else
           {
              defaultRadioButtonValue1 = false;
              defaultRadioButtonValue2 = true;
           }

        %> 
        <div class="formItemWithIndent1 formItemWithIndent2">   
         <%=widgetFactory.createRadioButtonField("PartialPaymentsType",	"PARTIAL_PAYMENT_TYPE_FULL_AMOUNT", "Assignment.FullAmount", TradePortalConstants.PARTIAL_PAYMENT_TYPE_FULL_AMOUNT,	defaultRadioButtonValue1,	isReadOnly, "onClick=\"clearPartialPaymentsPercentage()\" labelClass=\"formWidth\"", "")%>
         <br><br>
         <div>
             <%=widgetFactory.createRadioButtonField("PartialPaymentsType",	"PARTIAL_PAYMENT_TYPE_PERCENTAGE", "", TradePortalConstants.PARTIAL_PAYMENT_TYPE_PERCENTAGE,	defaultRadioButtonValue2,	isReadOnly)%>
             <%= widgetFactory.createTextField( "PartialPaymentsPercentage", "", terms.getAttribute("part_payments_percent"), "", isReadOnly, !isTemplate, false,  "maxlength=3 class='char1' onClick=\"selectPartialPaymentsPercentage()\"", "regExp:'[0-9]+'","none" ) %>
             <%=resMgr.getText("Assignment.Percentage", TradePortalConstants.TEXT_BUNDLE)%>                       
     	</div>
     <br>
          <%= widgetFactory.createNote("Assignment.Note")%>
          </div>




<script LANGUAGE="JavaScript">
	function clearPartialPaymentsPercentage(){
		require(["dijit/registry"], function(registry) {
			registry.byId("PartialPaymentsPercentage").set("value","");
		});
	}
	
	function selectPartialPaymentsPercentage(){
		require(["dijit/registry"], function(registry) {
			registry.byId("PARTIAL_PAYMENT_TYPE_PERCENTAGE").set('checked', true);
		});
	}
	
  	function clearAssigneeSearch(){
	  	require(["dijit/registry"], function(registry) {
	  		registry.byId("AssigneeName").set("value","");
	  		registry.byId("AssigneeAddressLine1").set("value","");
	  		registry.byId("AssigneeAddressLine2").set("value","");
	  		registry.byId("AssigneeCity").set("value","");
	  		registry.byId("AssigneeStateProvince").set("value","");
	  		registry.byId("AssigneeCountry").set("value","");
	  		registry.byId("AssigneePostalCode").set("value","");
	  		registry.byId("AssigneeContactName").set("value","");
	  		registry.byId("AssigneePhoneNumber").set("value","");
	  		registry.byId("AssigneeOTLCustomerId").set("value","");
  		});
  	}

  	function checkAssigneeName(originalName){
  		require(["dijit/registry"], function(registry) {
	    	if (registry.byId("AssigneeName").value != originalName){
	    		registry.byId("AssigneeOTLCustomerId").set("value","");
	    	}
  		});
  	}

  	function checkAssigneeBankName(originalName){
  		require(["dijit/registry"], function(registry) {
     		if (registry.byId("AssigneeBankName").value != originalName){
     			registry.byId("AssigneeBankOTLCustomerId").set("value","");
     		}
  		});
  	}
</script>
