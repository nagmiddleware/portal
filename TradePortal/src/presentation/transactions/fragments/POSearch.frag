<%--
*******************************************************************************
               Purchase Order (PO) Line Items Search include file

  Description:
  This is a JSP meant to be included using the <%@ include file="x.jsp" %> 
  tag for PO Line Item search.  It creates the HTML for displaying PO Line Items
  Search fields (PO Number, Line Item Number, and Beneficiary Name) for the 
  Search. The file also handles building the extended WHERE Clause that will
  retrieve PO entries based on the search criteria that was entered by the 
  user.

  Because this file is used by including with <%@ include %> rather than with
  the <jsp:include> directive, it is not a standalone servlet.  

*******************************************************************************
--%>

  <input type="hidden" name="NewSearch" value="Y">

<%
  poNumber       = request.getParameter("PONumber");
  lineItemNumber = request.getParameter("LineItemNumber");
  beneName       = request.getParameter("BeneName");

  if (poNumber == null) 
     poNumber = "";
  
  if (lineItemNumber == null) 
       lineItemNumber = "";
  
  if (beneName == null) 
       beneName = "";
%>
	<div class="gridSearch">
	<div class="searchHeader">
	<span class="searchHeaderCriteria">
		<%=widgetFactory.createSearchTextField("PONumber","AddPOLineItems.PONumber","14", "onKeydown='Javascript: filterPOSearchOnEnter(\"PONumber\");' 14")%>
		<%=widgetFactory.createSearchTextField("LineItemNumber","AddPOLineItems.LineItemNumber","14","onKeydown='Javascript: filterPOSearchOnEnter(\"LineItemNumber\");' 14")%>
		<%=widgetFactory.createSearchTextField("BeneName","AddPOLineItems.BeneficiaryName","35", "onKeydown='Javascript: filterPOSearchOnEnter(\"BeneName\");' class=\"char20\"")%>
	</span>						
	<span class="searchHeaderActions">
		<button data-dojo-type="dijit.form.Button" type="button" id="SearchButton">
			<%=resMgr.getText("common.FilterText", TradePortalConstants.TEXT_BUNDLE)%>
		    <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
	              filterPOs();return false;
	  		</script>
		</button>
		<%=widgetFactory.createHoverHelp("SearchButton", "SearchHoverText")%>
	</span>
	<div style="clear: both;"></div>
	</div>
	</div>
	
<script type="text/javascript">

function filterPOSearchOnEnter(fieldID){
	require(["dojo/on","dijit/registry"],function(on, registry) {
	    on(registry.byId(fieldID), "keypress", function(event) {
	        if (event && event.keyCode == 13) {
	        	dojo.stopEvent(event);
	        	filterPOs();
	        }
	    });
	});	<%-- end require --%>
}<%-- end of filterPOSearchOnEnter() --%>
</script>
