<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
*******************************************************************************
                 Loan Request Page - Payment Instructions section

  Description:
    Contains HTML to create the Loan Request Payment Instructions section.  

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transaction-LRQ-ISS-PaymentInstructions.jsp" %>
*******************************************************************************
--%>
<div class="columnLeft">

	<%
		/* KMehta IR-T36000024809 Rel 9300 on 25-May-2015 Change - Start */
	Vector availabilityChecks = new Vector(2);
	if (!isReadOnly) {
		availabilityChecks.add("not deactivate_indicator");
	}
	availabilityChecks.add("available_for_loan_request");

	String allowPayByAnotherAccount = CBCResult
			.getAttribute("/ResultSetRecord(0)/ALLOW_PAY_BY_ANOTHER_ACCNT");
	if (TradePortalConstants.INDICATOR_NO
			.equals(allowPayByAnotherAccount)) {
		availabilityChecks.add("!othercorp_customer_indicator");
	}
	//RKAZI HAUL062753546 06/27/2011 End

	//IAZ CR-511 11/09/09 End

	String userCheck = null;
	if (isBankUser) {
		userCheck = null;
	} else {
		userCheck = userSession.getUserOid();
	}
	termsPartyApp.loadAcctChoices(corpOrgOid,
			formMgr.getServerLocation(), resMgr, availabilityChecks,
			userCheck);

	    
		//Using the acct_choices xml from the app party,build the dropdown and 
		// select the one matching coms_chrgs_our_account_num
		acctNum = StringFunction.xssHtmlToChars(terms
				.getAttribute("coms_chrgs_our_account_num"));

		appAcctList = StringFunction.xssHtmlToChars(termsPartyApp
				.getAttribute("acct_choices"));

		//DocumentHandler acctOptions = new DocumentHandler(appAcctList, true);
		acctOptions = new DocumentHandler(appAcctList, true);
		options = Dropdown.createSortedAcctOptions(acctOptions, acctNum,
				loginLocale); 
	%>	
	<%=widgetFactory.createSelectField(
					"ComsChargesOurAccountNum",
					"LoanRequest.ApplicantsDebitAccountForCharges", " ",
					options, isReadOnly, !isTemplate, false, "class='char25' onChange='pickChrgDebitAccountRadio();'", "", "")%>
   					
</div>
<div class="columnRight">
	<%
		// Using the acct_choices xml from the app party,build the dropdown and 
		// select the one matching interest_debit_acct_num
		acctNum = StringFunction.xssHtmlToChars(terms
				.getAttribute("interest_debit_acct_num"));

		 appAcctList = StringFunction.xssHtmlToChars(termsPartyApp
				.getAttribute("acct_choices"));

		//DocumentHandler acctOptions = new DocumentHandler(appAcctList, true);
		acctOptions = new DocumentHandler(appAcctList, true);
		options = Dropdown.createSortedAcctOptions(acctOptions, acctNum,
				loginLocale); 
	%>
	<%-- KMehta IR-T36000024809 Rel 9300 on 25-May-2015 Change - End  --%>
	<%=widgetFactory.createSelectField(
					"InterestDebitAccountNum",
					"LoanRequest.ApplicantsDebitAccountForInterest", " ",
					options, isReadOnly, !isTemplate, false, "class='char25' onChange='pickChrgDebitAccountRadio();'", "", "")%>
</div>

<input type=hidden value='<%=appAcctList%>' name=AppAcctChoices>

<script LANGUAGE="JavaScript">
	function pickChrgDebitAccountRadio(){
	        if (dijit.byId("ComsChargesOurAccountNum").getValue() != null || dijit.byId("InterestDebitAccountNum").getValue() != null ) { 
	             dijit.byId("DebitAccounts").set('checked',true);
	             
	          }
	}
</script>