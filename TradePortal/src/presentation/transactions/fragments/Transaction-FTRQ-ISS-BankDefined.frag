<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
*******************************************************************************
                 Funds Transfer Request Page - Bank Defined section

  Description:
    Contains HTML to create the Funds Transfer Request Bank Defined section.  

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transaction-FTRQ-ISS-BankDefined.jsp" %>
*******************************************************************************
--%>

  <table width="100%" border="0" cellspacing="0" cellpadding="0" height="34">
    <tr> 
      <td width="15" class="BankColor" nowrap height="35">&nbsp;</td>
      <td class="BankColor" width="170" nowrap height="35"> 
        <p class="ControlLabelWhite">
          <span class="ControlLabelWhite">
            <%=resMgr.getText("FundsTransferRequest.BankDefined", 
                              TradePortalConstants.TEXT_BUNDLE)%>
          </span>
          <a name="BankDefined"></a>
        </p>
      </td>
      <td width="100%" class="BankColor" height="35" valign="top">&nbsp;</td>
      <td width="1" class="BankColor" height="35">&nbsp;</td>
      <td width="1" class="BankColor" height="35">&nbsp;</td>
      <td width="90" class="BankColor" height="35" nowrap> 
        <table width=100% border=0 cellspacing=0 cellpadding=0 height=17>
          <tr> 
            <td align="center" valign="middle" height="17"> 
              <p class="ButtonLink">
                <a href="#top" class="LinksSmall">
                  <%=resMgr.getText("common.TopOfPage", 
                                    TradePortalConstants.TEXT_BUNDLE)%>
                </a>
              </p>
            </td>
          </tr>
        </table>
      </td>
      <td width="1" class="BankColor" nowrap height="35">&nbsp;</td>
      <td class=BankColor align=right valign=middle width=15 height=35 nowrap>
        <%= OnlineHelp.createContextSensitiveLink("customer/issue_funds_trans_request.htm", "bankdefined", resMgr, userSession) %>
      </td>
      <td width="20" class="BankColor" nowrap height="35">&nbsp;</td>
    </tr>
  </table>
  
  

  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td>&nbsp;</td>
    </tr>
  </table>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td width="20" nowrap>&nbsp;</td>
      <td nowrap width="284">
	    <p class="ControlLabel">
          <%=resMgr.getText("FundsTransferRequest.PurposeType", 
                            TradePortalConstants.TEXT_BUNDLE)%>
          <br>

          <%=InputField.createTextField("PurposeType",
                                        terms.getAttribute("purpose_type"),
                                        "3", "3", "ListText", isReadOnly)%>
      </td>
      <td width="693">
	    
      </td>
    </tr>
	<tr> 
      <td width="20" nowrap>&nbsp;</td>
      <td colspan="2" nowrap>&nbsp;</td>
    </tr>
	<tr> 
      <td width="20" nowrap>&nbsp;</td>
      <td colspan="2" nowrap>
	     <p class="ListText">
		  <%=resMgr.getText("FundsTransferRequest.ApplicableOnly", 
                              TradePortalConstants.TEXT_BUNDLE)%>
		 </p>					  
	  </td>
    </tr>
    <tr> 
      <td width="20" nowrap>&nbsp;</td>
      <td colspan="2" nowrap>&nbsp;</td>
    </tr>
    <tr> 
      <td width="20" nowrap>&nbsp;</td>
      <td nowrap width="284">
	    <p class="ControlLabel">
		  <%=resMgr.getText("FundsTransferRequest.InAdvanceDisposition", 
                              TradePortalConstants.TEXT_BUNDLE)%>
          <br>
<%
              options = Dropdown.createSortedRefDataOptions(
                      TradePortalConstants.IN_ADVANCE_DISP_TYPE, 
                      terms.getAttribute("in_advance_disposition"), 
                      loginLocale);					  
               out.println(InputField.createSelectField("InAdvanceDispType", "",
                      " ", options, "ListText", isReadOnly));
%>
        </p>
      </td>
      <td width="693">
	    <p class="ControlLabel">
	  				  <%=resMgr.getText("FundsTransferRequest.InArrearsDisposition", 
                              TradePortalConstants.TEXT_BUNDLE)%>
	    <br>
<%
               options = Dropdown.createSortedRefDataOptions(
                      TradePortalConstants.IN_ARREARS_DISP_TYPE, 
                      terms.getAttribute("in_arrears_disposition"), 
                      loginLocale);
               out.println(InputField.createSelectField("InArrearsDispType", "",
                      " ", options, "ListText", isReadOnly));
%>
         </p>
      </td>
    </tr>	
  </table>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td>&nbsp;</td>
    </tr>
  </table>