<%--
**********************************************************************************
// BSL CR-655 02/22/11 
**********************************************************************************
--%> 
<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<div id="rptgCodesNonVis" style="visibility: hidden; position: absolute;">
<%
if (InstrumentServices.isNotBlank(bankGroupOid)) {
	String selectClause = "select p.code, p.description";
	String[] fromClause = {" from payment_reporting_code_1 p", " from payment_reporting_code_2 p"}; 
	StringBuffer whereClause = new StringBuffer();
	whereClause.append(" where p.p_bank_group_oid = ? ");

	try {
		for (int i=0; i < fromClause.length; i++) {
			StringBuffer rptgCodeQuery = new StringBuffer();
			rptgCodeQuery.append(selectClause);
			rptgCodeQuery.append(fromClause[i]);
			rptgCodeQuery.append(whereClause);

			DocumentHandler xmlDoc = DatabaseQueryBean.getXmlResultSet(rptgCodeQuery.toString(), false, new Object[]{bankGroupOid});

			if (xmlDoc != null) {
				String listBoxParmName = "\"hidden_reporting_code_" + (i+1) + "\"";
				String codeOptions = ListBox.createOptionList(xmlDoc, "CODE", "DESCRIPTION", selectedRptgCodes[i]);
				String defaultRptgCodeText = "";
				if (InstrumentServices.isBlank(selectedRptgCodes[i])) {
					// if a rptg code is not already selected, include blank option in dropdown
					defaultRptgCodeText = "                              ";
				}
				StringBuffer rptgCodeOutField = new StringBuffer(InputField.createSelectField(listBoxParmName, "", defaultRptgCodeText, codeOptions, "ListText", isReadOnlyRptgCode[i]));
				if (rptgCodeOutField.substring(0,5).equalsIgnoreCase("<span")) {
					// add ID attribute to facilitate getElementById
					rptgCodeOutField.insert(5, " id=" + listBoxParmName + " ");
				}
				out.println(rptgCodeOutField.toString());
			}
		}
	}
	catch (Exception e) {
		e.printStackTrace();
	}
}
%>
</div>