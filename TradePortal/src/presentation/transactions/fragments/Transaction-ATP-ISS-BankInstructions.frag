<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
*******************************************************************************
                 Approval to Pay Issue Page - Bank Instructions section

  Description:
    Contains HTML to create the Approval to Pay Issue Bank Instructions section.  

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transaction-ATP-ISS-BankInstructions.jsp" %>
*******************************************************************************
--%>
<script LANGUAGE="JavaScript">
function enableFinanceDrawing(){
		var FinanceDrawingNumDays= dijit.byId("FinanceDrawingNumDays").value;
		if((isNaN(FinanceDrawingNumDays)==false) ){
		dijit.byId("FinanceDrawing").set('checked',true);	
				}
}

</script>
<%
// Get the formatted FEC amount and Rate 

String displayFECAmount;
String displayFECRate;
  
  if(!getDataFromDoc)
   {
     displayFECAmount = TPCurrencyUtility.getDisplayAmount(terms.getAttribute("fec_amount"), 
                                                terms.getAttribute("amount_currency_code"), loginLocale);
     displayFECRate   = TPCurrencyUtility.getDisplayAmount(terms.getAttribute("fec_rate"), 
                                                "", loginLocale);
   }
  else
   {
     displayFECAmount = terms.getAttribute("fec_amount");
     displayFECRate   = terms.getAttribute("fec_rate");
   }
  
  
  String regExpRange = "";
  regExpRange = "[0-9]{0,5}([.][0-9]{0,8})?";
  regExpRange = "regExp:'" + regExpRange + "'"; 
%>


	 
		
		<%
		options = Dropdown.createSortedRefDataOptions(
              TradePortalConstants.INSTRUMENT_LANGUAGE, 
              instrument.getAttribute("language"), 
              loginLocale);
		%>

		<%= widgetFactory.createSelectField( "InstrumentLanguage", "ApprovalToPayIssue.IssueInstrumentIn", "", options, isReadOnly, true, false, "", "", "") %>
		
		<%
      if (isReadOnly || isFromExpress) {
      	out.println("&nbsp;");
      } else {
          options = ListBox.createOptionList(spclInstrDocList, "PHRASE_OID", "NAME", "", userSession.getSecretKey());
      	defaultText1 = resMgr.getText("transaction.SelectPhrase", TradePortalConstants.TEXT_BUNDLE);
		%>

		<%= widgetFactory.createSelectField( "CommInvPhraseItem1", "ApprovalToPayIssue.InstructionsText", defaultText1, options, isReadOnly, false, isExpressTemplate, "onChange=" + PhraseUtility.getPhraseChange(
                "/Terms/special_bank_instructions",
                "SpclBankInstructions",
                "1000","document.forms[0].SpclBankInstructions"), "", "") %>
		
		<%
      }if(!isReadOnly){
		%>
		<%= widgetFactory.createTextArea( "SpclBankInstructions", "", terms.getAttribute("special_bank_instructions").toString(), isReadOnly || isFromExpress,false,false, "rows='10' cols='100'","","") %>
		<%= widgetFactory.createHoverHelp("SpclBankInstructions","InstrumentIssue.PlaceHolderSpecialBankInstr2")%>
		<%}else{%>
		<%=widgetFactory.createAutoResizeTextArea("SpclBankInstructions", "", terms.getAttribute("special_bank_instructions").toString(), isReadOnly || isFromExpress,false,false, "style='width:600px;min-height:140px;' cols='100'","","") %>
		<%}%>
		<div class="columnLeft"> 
		
				<%= widgetFactory.createSubsectionHeader( "ApprovalToPayIssue.SettleInstructions",true, false, false,"" ) %>
			
				<%= widgetFactory.createTextField( "SettleOurAcct", "ApprovalToPayIssue.DebitOurAcct", terms.getAttribute("settlement_our_account_num").toString(), "30", isReadOnly ) %>
				
				<%= widgetFactory.createTextField( "BranchCode", "ApprovalToPayIssue.BranchCode", terms.getAttribute("branch_code").toString(), "30", isReadOnly) %>
				
				<%= widgetFactory.createTextField( "SettleForeignAcct", "ApprovalToPayIssue.DebitForeignAcct", terms.getAttribute("settlement_foreign_acct_num").toString(), "30", isReadOnly) %>
				
				<% options = Dropdown.createSortedCurrencyCodeOptions(terms.getAttribute("settlement_foreign_acct_curr"), loginLocale);
				%>
  				<%=widgetFactory.createSelectField("SettleForeignCcy","ApprovalToPayIssue.AcctCcy", " ", options, isReadOnly, false,false, "style=\"width: 50px;\"", "", "inline")%>		
  				<div style="clear:both;"></div>	
				
   
		</div>
		
		<div class="columnRight"> 
		
				<%= widgetFactory.createSubsectionHeader( "ApprovalToPayIssue.CommAndChrg",true, false, false, "" ) %>
				
				<%= widgetFactory.createTextField( "CandCOurAcct", "ApprovalToPayIssue.DebitOurAcct", terms.getAttribute("coms_chrgs_our_account_num").toString(), "30", isReadOnly) %>
				
				<%= widgetFactory.createTextField( "CandCForeignAcct", "ApprovalToPayIssue.DebitForeignAcct", terms.getAttribute("coms_chrgs_foreign_acct_num").toString(), "30", isReadOnly ) %>
				
				<% options = Dropdown.createSortedCurrencyCodeOptions(terms.getAttribute("coms_chrgs_foreign_acct_curr"), loginLocale);
				%>
				<%=widgetFactory.createSelectField("CandCForeignCcy","ApprovalToPayIssue.AcctCcy", " ", options, isReadOnly, false,false, "style=\"width: 50px;\"", "", "inline")%>		  				
  				<div style="clear:both;"></div>
				
				
		</div>
		
		<div style="clear:both;"></div>
		<div class="columnLeft">
		<%= widgetFactory.createSubsectionHeader( "ApprovalToPayIssue.FEC",true, false, false, "" ) %>
				
				<div class="inline">
				<%= widgetFactory.createTextField( "FECNumber", "ApprovalToPayIssue.FECCovered", terms.getAttribute("covered_by_fec_number").toString(), "14", isReadOnly, false, false, "class='char15'", "", "inline"  ) %>
				<%= widgetFactory.createTextField( "FECRate", "ApprovalToPayIssue.FECRate", displayFECRate, "14", isReadOnly, false, false,  "class='char10'", regExpRange, "inline"  ) %>
				<%-- <%= widgetFactory.createNumberField( "FECRate", "ApprovalToPayIssue.FECRate", displayFECRate, "14", isReadOnly, false, false,  "class='char10'", "", "inline"  ) %> --%>
				<div style="clear:both;"></div>
			     </div>
				
			     <div>
				<%= widgetFactory.createAmountField( "FECAmount", "ApprovalToPayIssue.FECAmt", displayFECAmount, terms.getAttribute("amount_currency_code"), isReadOnly, false, false, "class='char15'", "readOnly:"+isReadOnly, "inline") %> 
				<%= widgetFactory.createDateField( "FECMaturityDate", "ApprovalToPayIssue.FECMaturity",  StringFunction.xssHtmlToChars(terms.getAttribute("fec_maturity_date")).toString(), isReadOnly, false, false,  "class='char10'", dateWidgetOptions, "inline" ) %>
				<div style="clear:both;"></div>
			     </div>
		 </div>
		<div class="columnRight">
		<%= widgetFactory.createSubsectionHeader( "ApprovalToPayIssue.FinancingInstructions",true, false, false,"" ) %>
				
				<div class  = "formItem">
				<%= widgetFactory.createCheckboxField( "FinanceDrawing", "ApprovalToPayIssue.FinanceDrawing", terms.getAttribute("finance_drawing").equals(TradePortalConstants.INDICATOR_YES), isReadOnly, false, "", "", "none"  ) %>
				<%= widgetFactory.createNumberField( "FinanceDrawingNumDays", "", terms.getAttribute("finance_drawing_num_days"), "3", isReadOnly, false, true,  "onChange=\"enableFinanceDrawing();\"", "", "none" ) %>
				<%= widgetFactory.createSubLabel( "ApprovalToPayIssue.DaysIn") %>	
				</div>
				<br/>
				<% options =Dropdown.createSortedRefDataOptions(TradePortalConstants.DRAWING_TYPE,
                      terms.getAttribute("finance_drawing_type"),
                      loginLocale);
				%>
				
					<div style="margin-left: 50px; margin-top:-7px">
					    <%= widgetFactory.createSubLabel( "ApprovalToPayIssue.In") %>
						<%= widgetFactory.createSelectField( "FinanceDrawingType", "", "", options, isReadOnly, false, false,  "", "", "none" ) %>				
						<%= widgetFactory.createSubLabel( "ApprovalToPayIssue.DrawingAt") %>	
					</div>
				
		 </div>
		 <div style="clear:both;"></div>
		<% if (isReadOnly || isFromExpress) {
	          out.println("&nbsp;");
	        } else {
          options = ListBox.createOptionList(spclInstrDocList, "PHRASE_OID", "NAME", "", userSession.getSecretKey());
	          defaultText1 = resMgr.getText("transaction.SelectPhrase", TradePortalConstants.TEXT_BUNDLE); 
	     %>
		
	     
		<%= widgetFactory.createSelectField( "CommInvPhraseItem2", "ApprovalToPayIssue.InstructionsText", defaultText1, options, isReadOnly, false, false,  "onChange=" + PhraseUtility.getPhraseChange(
	             "/Terms/coms_chrgs_other_text","CandCOtherText", "1000","document.forms[0].CandCOtherText"), "", "") %>
		<% 
	        }
	        if(!isReadOnly){
		%>
		<%= widgetFactory.createTextArea( "CandCOtherText", "", terms.getAttribute("coms_chrgs_other_text").toString(), isReadOnly,false,false, "rows='10' cols='100'","","") %>
		<%= widgetFactory.createHoverHelp("CandCOtherText","InstrumentIssue.PlaceHolderCommissionsChargesInstr")%>		
		<%}else{%>
		<%=widgetFactory.createAutoResizeTextArea("CandCOtherText", "", terms.getAttribute("coms_chrgs_other_text").toString(), isReadOnly,false,false, "style='width:600px;min-height:140px;' cols='100'","","") %>
		<%}%>
