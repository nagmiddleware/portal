<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
*******************************************************************************
                 Import DLC Amend Page - Other Conditions section

  Description:
    Contains HTML to create the Import DLC Amend page.  All data retrieval 
  is handled in the Transaction-IMP_DLC-AMD.jsp.

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transaction-IMP_DLC-AMD-OtherConditions.frag" %>
*******************************************************************************

--%>
<%
	if (isReadOnly) {
		out.println("&nbsp;");
	} else {
%>
<%
	options = ListBox.createOptionList(addlCondDocList,
				"PHRASE_OID", "NAME", "", userSession.getSecretKey());
		defaultText = resMgr.getText("transaction.SelectPhrase",
				TradePortalConstants.TEXT_BUNDLE);
%>
<%=widgetFactory
						.createSelectField(
								"AddlDocsPhraseItem",
								"",
								defaultText,
								options,
								isReadOnly,
								false,
								false,
								"onChange="
										+ PhraseUtility
												.getPhraseChange(
														"/Terms/additional_conditions",
														"AddlConditionsText",
														textAreaMaxlen,
														"document.forms[0].AddlConditionsText"),
								"", "")%>
<%
	}
%>
<%=widgetFactory.createTextArea("AddlConditionsText", "",
					terms.getAttribute("additional_conditions"), isReadOnly,
					false, false, "rows='10' cols='128'", "", "","")%>
<%if(!isReadOnly) {%>
<%=widgetFactory.createHoverHelp("AddlConditionsText","AddlConditionsText") %>
<% }%>