<%--
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
*******************************************************************************
    Loan Request Page - Bank Instructions section

  Description:
    Contains HTML to create the Loan Request Bank Instructions section.

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transaction-LQR-ISS-BankInstructions.jsp" %>
*******************************************************************************
--%>
<%
	options = Dropdown.createSortedRefDataOptions(
			TradePortalConstants.INSTRUMENT_LANGUAGE,
			instrument.getAttribute("language"), loginLocale);
%>
<%=widgetFactory.createSelectField("InstrumentLanguage",
					"LoanRequest.IssueInstrumentIn", "", options, isReadOnly,
					true, false, "", "", "")%>
<%
	if (isReadOnly) {
		out.println("&nbsp;");
	} else {
%>
<%
	options = ListBox.createOptionList(specialInstructionsList,
				"PHRASE_OID", "NAME", "", userSession.getSecretKey());
		defaultText = resMgr.getText("transaction.SelectPhrase",
				TradePortalConstants.TEXT_BUNDLE);
%>
<%=widgetFactory.createSelectField(
						"bankInstructionsPhraseDropDown",
						"LoanRequest.AdditionalInst",
						defaultText,
						options,
						isReadOnly,
						false,
						false,
						"onChange="
								+ PhraseUtility
										.getPhraseChange(
												"/Terms/special_bank_instructions",
												"SpclBankInstructions", "5000",
												"document.forms[0].SpclBankInstructions"),
						"", "")%>
<%
	}
%>
<%=widgetFactory.createTextArea("SpclBankInstructions", "",
					terms.getAttribute("special_bank_instructions"),
					isReadOnly, false, false, "rows='10' cols='250'", "", "")%>
					<%=widgetFactory.createHoverHelp("SpclBankInstructions", "SpclBankInstrHoverText")%>
