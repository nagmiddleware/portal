<%--
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
*******************************************************************************
        Multiple Shipments

  Description:
    Contains HTML to create tabs for multiple shipments.  

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="MultipleShipments.frag" %>
*******************************************************************************
--%>

<% 
  if (numShipments > 1)
  {
%>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
<%
    if (displayTabsOnTop.equals("true"))
    {
%>
      <tr> 
        <td nowrap>&nbsp;</td>
      </tr>
<%
    }
%>
      <tr> 
        <td width="35" class=ListText nowrap>&nbsp;</td>
<%
        for (iLoop=1; iLoop<=numShipments; iLoop++)
        {
          shipmentTabNum = String.valueOf(iLoop);
          
          // Set the shipment tab number as parameter in the tab's link
          linkParameters = new StringBuffer();
          linkParameters.append("&shipmentTabNumber=");
          linkParameters.append(shipmentTabNum);

          // Wrap shipment tabs every 15 shipments
          if (iLoop % 16 == 0)
          {
%>
                <td width="100%">&nbsp;</td>
                <td>&nbsp;</td>
                <td width="20" nowrap>&nbsp;</td>
              </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td width="35" class=ListText nowrap>&nbsp;</td>
<%
          }
%>
          <jsp:include page="/common/PartNavigationLink.jsp">
            <jsp:param name="partName"	      value="ShipmentTab" />			    
            <jsp:param name="selected" 	      value="<%=shipmentTabPressed.equals(shipmentTabNum)%>" />
            <jsp:param name="linkLabel"       value="<%=shipmentTabNum%>" />
            <jsp:param name="readOnly"	      value="<%=isReadOnly%>" />
	    <jsp:param name="urlDestination"  value="goToInstrumentNavigator" />
            <jsp:param name="shipmentTabNum"  value="<%=shipmentTabNum%>" />
            <jsp:param name="onTop"           value="<%=displayTabsOnTop%>" />
            <jsp:param name="extraTags"       value="<%=linkParameters.toString()%>" />
            <jsp:param name="readOnlyTags"    value="<%=linkParameters.toString()%>" />
            <jsp:param name="multiTabLinks"   value="true" />
	  </jsp:include>       

<%
        }
%>
        <td width="100%">&nbsp;</td>
        <td>&nbsp;</td>
        <td width="20" nowrap>&nbsp;</td>
      </tr>

<%
    if (displayTabsOnTop.equals("false"))
    {
%>
      <tr> 
        <td nowrap>&nbsp;</td>
      </tr>
<%
    }
%>
    </table>
<%
  }
%>
