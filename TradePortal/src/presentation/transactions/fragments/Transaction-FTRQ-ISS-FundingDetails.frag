<%--
*
*     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
*******************************************************************************
                 Funds Transfer Request Page - Funding Details section

  Description: Contains HTML to create the Funds Transfer Request Funding Details section.  

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transaction-FTRQ-ISS-FundingDetails.jsp" %>
*******************************************************************************
--%>

 <%
  // Get the formatted FEC amount and Rate 

  String displayFECAmount;
  String displayFECRate;
  String requestMarketRateVisibility = "hidden";

  if(userSession.hasAccessToLiveMarketRate() && InstrumentServices.isNotBlank(terms.getAttribute("debit_account_oid"))){
	   DocumentHandler resultsDoc = DatabaseQueryBean.getXmlResultSet("SELECT g.fx_online_avail_ind FROM account a, operational_bank_org o, bank_organization_group g WHERE a.a_op_bank_org_oid = o.organization_oid AND o.a_bank_org_group_oid = g.organization_oid and a.account_oid=?" ,false, new Object[]{terms.getAttribute("debit_account_oid")});
   	   if (resultsDoc != null) {
    		if (TradePortalConstants.INDICATOR_YES.equals(resultsDoc.getAttribute("/ResultSetRecord(0)/FX_ONLINE_AVAIL_IND"))) {
    		    requestMarketRateVisibility = "visible";
			}
    	}
	}


  if(!getDataFromDoc)
   {
	 displayFECAmount = TPCurrencyUtility.getDisplayAmount(terms.getAttribute("fec_amount"), terms.getAttribute("amount_currency_code"), loginLocale);
	 displayFECRate   = TPCurrencyUtility.getDisplayAmount(terms.getAttribute("fec_rate"),  "", loginLocale);
   }
  else
   {
	 displayFECAmount = terms.getAttribute("fec_amount");
	 displayFECRate   = terms.getAttribute("fec_rate");
   }
%>

<%// Take care of retrieving funding amount
  String fundingAmount = terms.getAttribute("funding_amount");
  String displayFundingAmount;

  // Don't format the amount if we are reading data from the doc
  if(!getDataFromDoc)
     displayFundingAmount = TPCurrencyUtility.getDisplayAmount(fundingAmount, currency, loginLocale);
  else
     displayFundingAmount = fundingAmount;
	 
%>

  
<div class="columnLeft"> 
		
  <%= widgetFactory.createSubsectionHeader( "FundsTransferRequest.SettlementDetails",isReadOnly,!isTemplate, isFromExpress,"" ) %>
  <%-- PHILC START HERE
       Replace Related Instrument FXFR Settle Type and use proceeds from with CreateFinancingToFundPayment (CFP)
       Remove RelatedLoanInstID, text box, and search button.
  --%>
			
  <%= widgetFactory.createRadioButtonField("FundsXferSettleType", TradePortalConstants.XFER_SETTLE_DEBIT_PAY_ACCT,
        "FundsTransferRequest.DebitPayersAccount", TradePortalConstants.XFER_SETTLE_DEBIT_PAY_ACCT,
        settleXferType.equals(TradePortalConstants.XFER_SETTLE_DEBIT_PAY_ACCT),	isReadOnly)%>

<%
  // Using the acct_choices xml from the payer party,build the dropdown and 
  // select the one matching debit_account_oid
  String acctOid = StringFunction.xssHtmlToChars(terms.getAttribute("debit_account_oid"));

  //If this is Not a Corp Customer or a Subsidiary access, bring accounts for this user only.
  //Make sure those accounts are available for Debit
        
  String sqlString = "select name from client_bank, users where user_oid = ? and p_owner_org_oid = organization_oid";      
  DocumentHandler bankUser = DatabaseQueryBean.getXmlResultSet(sqlString, false, new Object[]{userSession.getUserOid()});
  String userCheck = null;
	
  //Subsidiary Access: Display all subsidiary accounts
  boolean isSubsUser = false;
  if (userSession.hasSavedUserSession())  isSubsUser = true;

        if ((bankUser == null) && (!isSubsUser))       
        {
        	userCheck = userSession.getUserOid();	
        }
        

        Vector availabilityChecks = new Vector(2);
	/*Rel 8.3 IR T36000022821 - show deactivated account for instruments which are in "processed by bank" status*/
        if(!isReadOnly){
        	availabilityChecks.add("!deactivate_indicator");
        }
		availabilityChecks.add("available_for_internatnl_pay");		
		
        //RKAZI HAUL062753546 06/27/2011 Start
        String allowPayByAnotherAccount = CBCResult.getAttribute("/ResultSetRecord(0)/ALLOW_PAY_BY_ANOTHER_ACCNT");
        if (TradePortalConstants.INDICATOR_NO.equals(allowPayByAnotherAccount)){
        	availabilityChecks.add("!othercorp_customer_indicator");
        }
        //RKAZI HAUL062753546 06/27/2011 End
		
	    
	    termsPartyPayer.loadAcctChoices(corpOrgOid, formMgr.getServerLocation(), resMgr, availabilityChecks, userCheck);
        payerAcctList = StringFunction.xssHtmlToChars(termsPartyPayer.getAttribute("acct_choices"));

        DocumentHandler acctOptions = new DocumentHandler(payerAcctList, true);
        options = Dropdown.createSortedAcctOptionsOid(acctOptions, acctOid, loginLocale);
        out.println(widgetFactory.createSelectField( "AppDebitAcctPrincipal", "FundsTransferRequest.AccountNumber"," ",
                options, isReadOnly, false, false, "onChange='pickDebitPayerRadio();'", "", "")  );                                        
      

        //AAlubala CR610 
        //Build hidden fields containing the account name and corresponding account owner
        //This will be used to display the owner of the account on the fly i.e.
        //depending on the selected account. This applies to the situation where the 
        //selected account belongs to another corporation
        //[START]
        String corporateOrgToDisplay = termsPartyPayer.getAttribute("name"); 
       	DocumentHandler allAccnts = new DocumentHandler(payerAcctList, true);
        Vector vector = allAccnts.getFragments("/DocRoot/ResultSetRecord");
		int numItems = vector.size();
		String accntSQL = "";
		DocumentHandler ownerCorpOrg;
		for (int ii = 0; ii < numItems; ii++) {
			DocumentHandler doc1 = (DocumentHandler) vector.elementAt(ii); 
			String acct_oid1 = doc1.getAttribute("/ACCOUNT_OID");
			//AAlubala - IR#BEUL040548205 - 04/11/11 
			//Make use of the other_account_owner_oid
			String accntOwnerCorpID = doc1.getAttribute("/OTHER_ACCOUNT_OWNER_OID");
			String display = acct_oid1;
			String nameToDisplay;
			if(InstrumentServices.isBlank(accntOwnerCorpID)){
				nameToDisplay = corporateOrgToDisplay;
			}else{
				//retrieve the account owner organization's name
				accntSQL = "select name from corporate_org where organization_oid = ?"; 
				ownerCorpOrg = DatabaseQueryBean.getXmlResultSet(accntSQL, false, new Object[]{accntOwnerCorpID});     			
     			nameToDisplay = ownerCorpOrg.getAttribute("ResultSetRecord/NAME");
			}
			%>
			<input type=hidden name="<%=display%>" id="<%=display%>" value="<%=nameToDisplay%>" />
			<%			
		  }           
           
        //[END]        
%>

  <div id="bankGroupNonVis" style="visibility: hidden; position: absolute;">
<%
	String acctBankGroupSql = "select account_oid, a_bank_org_group_oid from operational_bank_org inner join account on organization_oid = a_op_bank_org_oid where deactivate_indicator != 'Y' and available_for_internatnl_pay = 'Y' and  p_owner_oid = ?";
	DocumentHandler acctBankGroupList = DatabaseQueryBean.getXmlResultSet(acctBankGroupSql.toString(), false, new Object[]{corpOrgOid});
	String acctBankGroupOptions = ListBox.createOptionList(acctBankGroupList, "ACCOUNT_OID", "A_BANK_ORG_GROUP_OID", acctOid);
	out.println(InputField.createSelectField("acctBankGroupSelection", "", " ", acctBankGroupOptions, "ListText", isReadOnly));
	String bankGroupOid = "";
	// extract the bankGroupOid value from the selected option
	int abgOptSelectedIdx = acctBankGroupOptions.indexOf("selected");	
	if (abgOptSelectedIdx > -1) {
		int abgOptStart = acctBankGroupOptions.indexOf(">", abgOptSelectedIdx);
		int abgOptEnd = acctBankGroupOptions.indexOf("</option>", abgOptSelectedIdx);	
		try {
			bankGroupOid = acctBankGroupOptions.substring(abgOptStart+1, abgOptEnd);
		} catch (Exception e) {
			bankGroupOid = "";
		}
	}
%>
  </div>
   
	 
<%	
			//AAlubala - IR#BEUL040548205 - 04/14/11 - Display correct account owner when readonly mode
			String ownerToDisplay="";
			if(isReadOnly && !TradePortalConstants.XFER_SETTLE_OTHER.equals(terms.getAttribute("funds_xfer_settle_type"))){
					
					String accntOwnerSQL = "select othercorp_customer_indicator from account where account_oid=?";
					DocumentHandler aoSQL = DatabaseQueryBean.getXmlResultSet(accntOwnerSQL, false, new Object[]{acctOid});
					String accInd = null;
					if (aoSQL != null ){
						accInd = aoSQL.getAttribute("ResultSetRecord/OTHERCORP_CUSTOMER_INDICATOR");
					}
					if(TradePortalConstants.INDICATOR_YES.equals(accInd)){
						String accSQL = "select name from corporate_org where organization_oid = (select other_account_owner_oid from account where account_oid=?)";										
						DocumentHandler ownerOrg = DatabaseQueryBean.getXmlResultSet(accSQL, false, new Object[]{acctOid});    
						if (ownerOrg != null)						 			
							ownerToDisplay = ownerOrg.getAttribute("ResultSetRecord/NAME");	
					}else{
						ownerToDisplay = termsPartyPayer.getAttribute("name");
					}
			}
%>	

  <%= widgetFactory.createTextField("accntOwner","FundsTransferRequest.AccountOwner",ownerToDisplay,"22",true)%>			
  <br>   

	 
  <%= widgetFactory.createRadioButtonField("FundsXferSettleType", TradePortalConstants.XFER_SETTLE_OTHER,
        "FundsTransferRequest.SettleOther", TradePortalConstants.XFER_SETTLE_OTHER,
        settleXferType.equals(TradePortalConstants.XFER_SETTLE_OTHER),	isReadOnly)%>
  <%= widgetFactory.createTextArea("UseOtherText","",terms.getAttribute("finance_type_text"),	isReadOnly,false,false,"onChange='pickOtherRadio();'","","") %>		
  <%--CR 610 [END] --%>         

	
	
  <%= widgetFactory.createSubsectionHeader( "FundsTransferRequest.ChargesPaymentDetails",isReadOnly, false, isFromExpress,"" ) %>
       
<%
  // Using the acct_choices xml from the app party (reuse from above dropdown --
  // they both use the same set of accounts), build the dropdown and 
  // select the one matching payment_charges_account_oid
  acctOid = StringFunction.xssHtmlToChars(terms.getAttribute("payment_charges_account_oid"));
  options = Dropdown.createSortedAcctOptionsOid(acctOptions, acctOid, loginLocale);
  out.println(widgetFactory.createSelectField( "AppDebitAcctCharges", "FundsTransferRequest.AccountNumber"," ", options, isReadOnly, !isTemplate, isFromExpress, "onChange='displayAccountOwner();'", "", "")  );   
      
  //AAlubala - IR#BEUL040548205 - 04/14/11 - Display correct account owner when readonly mode
  if(isReadOnly){
    String accntOwnerSQL = "select othercorp_customer_indicator from account where account_oid=?";
    DocumentHandler aoSQL = DatabaseQueryBean.getXmlResultSet(accntOwnerSQL, false, new Object[]{acctOid});
    String accInd = null;
    if (aoSQL != null) {
    	accInd = aoSQL.getAttribute("ResultSetRecord/OTHERCORP_CUSTOMER_INDICATOR");
    }
    if(TradePortalConstants.INDICATOR_YES.equals(accInd)){
      String accSQL = "select name from corporate_org where organization_oid = (select other_account_owner_oid from account where account_oid=?)";										
      DocumentHandler ownerOrg = DatabaseQueryBean.getXmlResultSet(accSQL, false, new Object[]{acctOid});   
      if (ownerOrg != null)  			
      	ownerToDisplay = ownerOrg.getAttribute("ResultSetRecord/NAME");	
    }else{
      ownerToDisplay = termsPartyPayer.getAttribute("name");
    }
  }
%>
             
  <%= widgetFactory.createTextField("accntOwner2","FundsTransferRequest.AccountOwner",ownerToDisplay,"22",true)%>		

</div>  <%-- END GENERAL CONTENT LEFT --%>	
		
		

<div class="columnRight">  <%-- START GENERAL CONTENT RIGHT --%> 
		
  <%= widgetFactory.createSubsectionHeader( "FundsTransferRequest.ExchangeRateDetails",isReadOnly, false, isFromExpress,"" ) %>

    <div>    
		<%= widgetFactory.createTextField( "CoveredByFECNumber", "TransferBetweenAccounts.FECCovered", terms.getAttribute("covered_by_fec_number"), "14", isReadOnly && !isFXeditable, false, false, "onChange='pickLoanProceedsFECCheckbox();'", "","inline") %>                                      
		<%= widgetFactory.createTextField( "FECRate", "TransferBetweenAccounts.BaseToFXRate", displayFECRate, "14", isReadOnly && !isFXeditable, false, false, "onChange='pickLoanProceedsFECCheckbox();'", "","inline") %>                          
		<div style="clear:both;"></div>
	</div> 

	<div  class="formItem" id="requestMarketRate" >

		  <%		      		 
		  if (InstrumentServices.isBlank(requestMarketRateInd)) requestMarketRateInd = TradePortalConstants.INDICATOR_NO;									   

		  out.print(widgetFactory.createCheckboxField("RequestMarketRateCheckBox",  "FundsTransferRequest.RequestMarketRate", requestMarketRateInd,requestMarketRateInd.equals(TradePortalConstants.INDICATOR_YES), isReadOnly,false, "onClick='updateReqMarketRate()'", "", "")); 

		  %>
		  
		  <%-- Vshah - IR#DEUL120562739 - 01/05/2012 - Rel7.1 - BEGIN - Added below hidden variable --%> 
		  <%-- Becuase you cannot unset this setting once it is set to 'Y'. when you un-check the box it does not send 'N' � it sends null --%> 
		  <%-- To resolve this, we have to use hidden field and javascript --%>
		  <input type="hidden" name="RequestMarketRate" value="<%=requestMarketRateInd%>" >

	  </div>	

 	<div>
		
		   <%= widgetFactory.createTextField( "transfer_fx_rate", "TransferBetweenAccounts.BaseToFXRate", 
   					terms.getAttribute("transfer_fx_rate"), "22", true, false, false, "style='width: 60px'", "", "inline" ) %>
 		   
		
		   <%= widgetFactory.createTextField( "display_fx_rate_method", "TransferBetweenAccounts.FXCalcMethod", 
    			terms.getAttribute("display_fx_rate_method"), "22", true, false, false, "style='width: 60px'", "", "inline" ) %>
		  
		   <%= widgetFactory.createTextField( "equivalent_exch_ccy", "TransferBetweenAccounts.EquivAmount", 
    			terms.getAttribute("equivalent_exch_amt_ccy")+"  "+displayExchAmount, "22", true, false, false, "style='width: 60px'", "", "inline" ) %>
		   <div style="clear: both;"></div>
   </div>
  
    <%
		 if ((TradePortalConstants.TRANS_STATUS_FX_THRESH_EXCEEDED.equals(transactionStatus) ||
				TradePortalConstants.TRANS_STATUS_AUTH_PEND_MARKET_RATE.equals(transactionStatus)) && userSession.hasAccessToLiveMarketRate()) {
	
			boolean isEditable = InstrumentServices.isEditableTransStatus(transactionStatus);
			boolean canAuthorize = SecurityAccess.canAuthorizeInstrument(userSession.getSecurityRights(), instrumentType, transactionType);
			boolean adminCustAccess = false;

			 if (userSession.hasSavedUserSession())       {
				if (TradePortalConstants.ADMIN.equals(userSession.getSavedUserSessionSecurityType())) 
				adminCustAccess = true;
			 else if (TradePortalConstants.INDICATOR_YES.equals((String)request.getAttribute("panelEnabled")))
				adminCustAccess = true;
			}
		 
			 if (!isEditable && canAuthorize && !adminCustAccess) {
	%>
		
			 <script LANGUAGE="JavaScript">
   
   function getMarketRate(){

            require(["t360/dialog"], function(dialog){
			var errSec = document.getElementById('_errorSection');
			errSec.innerHTML = "";
			dialog.open('MarketRate', '<%=resMgr.getText("MarketRate.dialogTitle",TradePortalConstants.TEXT_BUNDLE)%>',
			'marketRateDialog.jsp',
			  ['selectedInstrument','showInfoText'],['<%=selectedInstrument%>','false'],
			'select', null);
			});
		}
     
	 </script>
    <%
			}
		}	  
	
		if (useMarketRate.equals(TradePortalConstants.INDICATOR_YES)){
                defaultCheckBoxValue = true;
        } else{
                defaultCheckBoxValue = false;
         }
    	 out.print(widgetFactory.createCheckboxField("UseMarketRate",	"FundsTransferRequest.BankToBookExchangeRate", defaultCheckBoxValue,isReadOnly,false,"onClick='updateReqMarketRate()'","",""));  
     
     
		if (useFEC.equals(TradePortalConstants.INDICATOR_YES)) {
                defaultCheckBoxValue = true;
		}else{
                defaultCheckBoxValue = false;
        }
		out.print(widgetFactory.createCheckboxField("UseFEC",	"FundsTransferRequest.FEC", defaultCheckBoxValue,isReadOnly,false,"onClick='updateFECVals()'","",""));  
													  
		if (fecuseOther.equals(TradePortalConstants.INDICATOR_YES)) {
                defaultCheckBoxValue = true;
		}else{
                defaultCheckBoxValue = false;
        }
	 %>
	
  <%=widgetFactory.createCheckboxField("FECUseOther","FundsTransferRequest.Other2",defaultCheckBoxValue,isReadOnly,false,"","","")%> 
  <%= widgetFactory.createTextArea("FECUseOtherText","",terms.getAttribute("use_other_text"), isReadOnly,false,false,"onChange='pickFECOtherCheckBox();'","","") %>	                            

</div> <%-- END GENERAL CONTENT RIGHT --%> 


<div id="MarketRate" ></div>
<div style="clear:both;"></div> 

<script LANGUAGE="JavaScript">

  function pickCreateFinanceRadio() {
       document.forms[0].FundsXferSettleType[0].checked = true;
	   updateFundingCurrency();
  }
  
   function pickAtFixedMaturityDateRadio() {
	
		var index = document.forms[0].LoanTermsFixedMaturityDay.selectedIndex;
		var day = document.forms[0].LoanTermsFixedMaturityDay.options[index].value;
	
		index = document.forms[0].LoanTermsFixedMaturityMonth.selectedIndex;
		var month = document.forms[0].LoanTermsFixedMaturityMonth.options[index].value;
	
		index = document.forms[0].LoanTermsFixedMaturityYear.selectedIndex;
		var year = document.forms[0].LoanTermsFixedMaturityYear.options[index].value;
		    
		if (day != '-1' || month != '-1' || year != '-1') {
			document.forms[0].LoanTermsType[0].checked = true;
			pickCreateFinanceRadio();
		}
	}
	
  function pickDaysFromLoanStartRadio() {    
    if (document.forms[0].DaysFrom.value > '') {
	  document.forms[0].LoanTermsType[1].checked = true;
	  pickCreateFinanceRadio();
	  }
  }

  function pickOtherRadio() {
    if (document.forms[0].UseOtherText.value > '')
       document.forms[0].FundsXferSettleType[1].checked = true;
  }
  function pickDebitPayerRadio() {
	    <%--
	    // Examine all the fields associated with this radio button.  If any
	    // have data, select the radio button.  Otherwise, don't do anything.
	    --%>

	require(["dijit/registry", "dojo/ready"],
    function(registry, ready ) {
	    var orderingParty="";      
	    var acct  = registry.byId("AppDebitAcctPrincipal").value;
	    if (acct > '') {
		      registry.byId("<%=TradePortalConstants.XFER_SETTLE_DEBIT_PAY_ACCT%>").set('checked', true);	
		      orderingParty = document.getElementById(acct).value;			  
		}
	    <%--
	    //AAlubala CR610 Rel 7.0.0.0 02/28/2011
	    //change the account owner depending on the selected account
	    --%>
	    var accntOwner = document.getElementById("accntOwner");	    

	    if(accntOwner != null){ 
	  		if(orderingParty != null){
	  			accntOwner.innerHTML = orderingParty;
	  		}else
	       		accntOwner.innerHTML = '<%=corporateOrgToDisplay%>';
	    }

	<%-- var accountOID = document.forms[0].AppDebitAcctPrincipal.value; --%>
	var acctBankGroupSelect = document.getElementsByName("acctBankGroupSelection")[0];
	var oldBankGroupOID = null;
	var newBankGroupOID = null;
	for (var j=0; j<acctBankGroupSelect.length; j++) {
		if (acctBankGroupSelect.value == acctBankGroupSelect.options[j].value) {
			oldBankGroupOID = acctBankGroupSelect.options[j].text;
		}
		if (acct == acctBankGroupSelect.options[j].value) {
			newBankGroupOID = acctBankGroupSelect.options[j].text;
		}
		if (oldBankGroupOID != null && newBankGroupOID != null) {
			break;
		}
	}
	
	acctBankGroupSelect.value = acct;	
	if (oldBankGroupOID != newBankGroupOID) {
		if(newBankGroupOID >''){
			executeChangeBankGroup(newBankGroupOID);
			}
		else {
			document.getElementById('requestMarketRate').style.visibility='hidden';
			document.getElementById('requestMarketRate').style.display='none';
			}		
	}
	document.forms[0].RequestMarketRateCheckBox.checked = false;

	});
		
	 

 }




 function executeChangeBankGroup(bankGroupOID) {
	require(["dojo/_base/xhr"], function(xhr) {
	var remoteUrl  = "/portal/transactions/Transaction-ISS-ChangeDebitAccount.jsp?BankGroupOID=" + bankGroupOID + "&";
	xhr.get({
              url: remoteUrl,
			  handleAs: "json",
			  load: function(data) {

				var fx = data.hidden_fxInd;

				if(fx=='Y'){		
					document.getElementById('requestMarketRate').style.visibility='visible';
					document.getElementById('requestMarketRate').style.display='block';
				}
				else{
					document.getElementById('requestMarketRate').style.visibility='hidden';
					document.getElementById('requestMarketRate').style.display='none';
				}
			
			},
			 error: function(error){
			     <%-- todo --%>
			}
		});
	});
}

  
  function displayAccountOwner() {
	    require(["dijit/registry", "dojo/ready"],
		function(registry, ready ) {

		var acct = dijit.byId('AppDebitAcctCharges').value;

	    <%-- 	    //change the account owner depending on the selected account 	    --%>
	    var defaultOwner = '<%=corporateOrgToDisplay%>';
	    var accntOwner = document.getElementById("accntOwner2");
	    var orderingParty = "";
		if (acct) {
		   orderingParty = document.getElementById(acct).value;
		}
	    if(accntOwner != null){ 
	  		if(orderingParty != null){
	  			accntOwner.innerHTML = orderingParty;
	  		}else
	       		accntOwner.innerHTML = defaultOwner;
	    }
		});
	  }
  
  <%-- 
  //AAlubala IR#BEUL040548205 - 04/12/11 - Added a function to display the AccountOwner's upon reloading the page
  --%>
  function displayAccountsOnLoad(){
	<%-- try{
		var idx1 = document.forms[0].AppDebitAcctPrincipal.selectedIndex;
		var idx2 = document.forms[0].AppDebitAcctCharges.selectedIndex;
		var accnt1 = document.forms[0].AppDebitAcctPrincipal.options[idx1].value;
		var accnt2 = document.forms[0].AppDebitAcctCharges.options[idx2].value;
	}catch(err1){
	
	}--%>
	try{
		document.getElementById("accntOwner").innerHTML = document.getElementById(accnt1).value;
		document.getElementById("accntOwner2").innerHTML = document.getElementById(accnt2).value;
	}catch(err){
		<%-- do nothing --%>
	}
 
    pickDebitPayerRadio(); 
	displayAccountOwner();
  }

	<%-- CHECK LOAN PROCEEDS FEC CHECKBOX IF NUMBER, AMOUNT, RATE, OR DATE IS ENTERED --%>
	function pickLoanProceedsFECCheckbox() {
		
	
			if (document.forms[0].CoveredByFECNumber.value > ''		||
				document.forms[0].FECRate.value > '') {
					document.forms[0].UseFEC.checked = true;
			}
	}
	
	<%-- PICK LOAN PROCEEDS FEC OTHER CHECKBOX IF APPLY LOAN PROCEEDS FEC OTHER TEXT BOX IS FILLED OUT --%>
	function pickLoanProceedsFECOtherCheckBox() {
	    
		if (document.forms[0].UseOtherText.value > '') {
			document.forms[0].ProceedsUseOther.checked = true;
		}
	}
	

	<%-- PICK DEBIT THE APPLICANTS ACCOUNT RADIO IF AN ACCOUNT NUMBER IS SELECTED --%>
	function pickDebitApplicantsAccountRadio() {
	    
		var index = document.forms[0].LoanMaturityDebitAccountImp.selectedIndex;
    	var acct  = document.forms[0].LoanMaturityDebitAccountImp.options[index].value;

		if (acct > '') {
			document.forms[0].LoanMaturityDebitTypeImp[0].checked = true;
		}
	}

   <%--
   // TLE - 11/03/06 IR#MHUG101470192 - Add Begin
	//// PICK DEBIT OTHER RADIO IF TEXT IS ENTERED INTO THE OTHER (ADVISE ...) TEXT BOX
	//function pickDebitOtherRadio() {
	//    
	//	if (document.forms[0].LoanMaturityDebitOtherText.value > '') {
	//		document.forms[0].LoanMaturityDebitType[1].checked = true;
	//	}
	//}
	--%>
	function pickDebitOtherRadio(value) {
                if (value == 1) {
		   if (document.forms[0].LoanMaturityDebitOtherTextExp.value > '') {
			document.forms[0].LoanMaturityDebitTypeExp[1].checked = true;
                   }
                } 
                else if (value == 2) {
		   if (document.forms[0].LoanMaturityDebitOtherTextImp.value > '') {
			document.forms[0].LoanMaturityDebitTypeImp[1].checked = true;
                   }
                }
                else {
		   if (document.forms[0].LoanMaturityDebitOtherText.value > '') {
			document.forms[0].LoanMaturityDebitType[1].checked = true;
                   }
                }
	}
   

	
	<%-- PICK MATURITY FEC OTHER CHECKBOX IF APPLY MATURITY FEC OTHER TEXT BOX IS FILLED OUT --%>
	function pickFECOtherCheckBox() {
	    
		if (document.forms[0].FECUseOtherText.value > '') {
			document.forms[0].FECUseOther.checked = true;
		}
	}
	
	function clearFundingAmount() {    
	     <%--  document.forms[0].FundingAmount.value = ''; --%>
    }
	
	function updateFundingAmount() {    
     <%--  if (document.forms[0].TransactionAmount.value > '' && document.forms[0].FundsXferSettleType[0].checked) {
      	
	  	if (document.forms[0].FundingAmount!=null && (document.forms[0].FundingAmount.value.length==0) || (document.forms[0].FundingAmount.value==null)) {
	      document.forms[0].FundingAmount.value = document.forms[0].TransactionAmount.value;
		}
	  }
	   --%>
    }

    function updateFECVals()
    {
        
    
    	if (dijit.byId("UseFEC").checked)
    	{
	    	dijit.byId("RequestMarketRateCheckBox").set('checked',false);
			document.forms[0].RequestMarketRate.value = 'N';
	    } else {
			document.forms[0].CoveredByFECNumber.value = '';
			document.forms[0].FECRate.value = ''; 
		}
    	
    }
    
    function updateUseMarketRate()
	{
		
	   	if (document.forms[0].UseMarketRate.checked == true)
	    {
	    	document.forms[0].UseMarketRate.value = 'Y';
	    }
	    else
	    {
	    	document.forms[0].UseMarketRate.value = 'N';
	    }
	    
    }



function updateReqMarketRate()
	{
	if (dijit.byId("RequestMarketRateCheckBox").checked){
		dijit.byId("UseMarketRate").setDisabled(true);
	}else{
		dijit.byId("UseMarketRate").setDisabled(false);
	}
	    if (dijit.byId("RequestMarketRateCheckBox").checked)
	    {
	    	document.forms[0].RequestMarketRate.value = 'Y';
	    	<%-- document.forms[0].CoveredByFECNumber.disabled=true; --%>
	    	document.forms[0].CoveredByFECNumber.value='';
		<%-- document.forms[0].FECRate.disabled=true; //IR#DEUL120562739 - Rel7.1 - 12/09/2011 --%>
	    	document.forms[0].FECRate.value=''; <%-- IR#DEUL120562739 - Rel7.1 - 12/09/2011 --%>
	    	<%-- IR#DEUL120562739 - Rel7.1 - 01/10/2012 - BEGIN --%>
	    	if (dijit.byId("UseFEC").checked)
	    	{
				<%-- dojo.byId("UseFEC").value='N'; --%>
				
	    		dijit.byId("UseFEC").set('checked',false);
				<%-- dijit.byId("UseFEC").set('value','N'); --%>
	    	}
	    	<%-- IR#DEUL120562739 - Rel7.1 - 01/10/2012 - END --%>
	    }
	    else
	    {
	    	document.forms[0].RequestMarketRate.value = 'N';
	    	<%-- document.forms[0].CoveredByFECNumber.disabled=false; --%>
	    	<%-- document.forms[0].FECRate.disabled=false; //IR#DEUL120562739 - Rel7.1 - 12/09/2011	    	 --%>
	    }
    }


function unCheckBankToBookXchnageRate() {
    dijit.byId("UseMarketRate").set('checked',false);
    dijit.byId("UseMarketRate").disabled = true;
    <%--  dijit.byId("UseMarketRate").setAttribute("disabled",true));  --%>
  }


function enableBankToBookXchnageRate(){
	dijit.byId("UseMarketRate").disabled = false;
}
</script>
