<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
*******************************************************************************
         Export Letter Of Credit Issue Transfer Page - General section

  Description:
    Contains HTML to create the Export Letter Of Credit Issue Transfer General 
    section.

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transaction-EXP_DLC-TRN-General.jsp" %>
*******************************************************************************
--%>

<div class="columnLeftNoBorder">

	<%=widgetFactory.createTextField("refnum",
					"transaction.YourRefNumber",
					terms.getAttribute("reference_number"), "30", true,
					false, false, "", "", "")%>
	<%=widgetFactory.createTextField("appname",
					"TransferELCAmend.ApplicantName", applicantName, "30", true,
					false, false, "", "", "")%>
	<%=widgetFactory.createDateField("",
					"ImportDLCAmend.CurrentExpiryDate", currentExpiryDate,
					true, false, false, "class='char6'", dateWidgetOptions, "")%>
	
	
	<%=widgetFactory.createDateField("ExpiryDate",
						"TransferELCAmend.NewExpiryDate", 
								StringFunction.xssHtmlToChars(terms.getAttribute("expiry_date")),
						isReadOnly, false, isExpressTemplate, "class='char6'", dateWidgetOptions, "")%>
	
	<%=widgetFactory.createDateField("LatestShipmentDate",
			"TransferELCAmend.NewLatestShipDate", 
			terms.getFirstShipment().getAttribute("latest_shipment_date"),
			isReadOnly, false, isExpressTemplate, "class='char6'", dateWidgetOptions, "")%>

</div>
<%-- Left Column Ends.... General Section --%>
<div class="columnRight">
	<%  if(!isProcessedByBank)
    {
	%>
		<%= widgetFactory.createTextField( "TransactionAmount", "TransferELCAmend.TransferAmount", TransferAmount.toString(), "22", true, false, false, "", "", "") %>
	<%
    }
	%>
	
		&nbsp;&nbsp;&nbsp;<%=widgetFactory.createRadioButtonField(
					"IncreaseDecreaseFlag", "Increase",
					"ImportDLCAmend.IncreaseAmount",
					TradePortalConstants.INCREASE,
					incrDecrValue.equals(TradePortalConstants.INCREASE),
					isReadOnly || isFromExpress, " onChange='setNewAmount();'", "")%>
		<br>
		&nbsp;&nbsp;&nbsp;<%=widgetFactory.createRadioButtonField(
					"IncreaseDecreaseFlag", "Decrease",
					"ImportDLCAmend.DecreaseAmount",
					TradePortalConstants.DECREASE,
					incrDecrValue.equals(TradePortalConstants.DECREASE),
					isReadOnly || isFromExpress," onChange='setNewAmount();'", "")%>
	<%if(!isReadOnly){ %>
	<br><br>
	<div class="formItem">
	<%=widgetFactory.createInlineLabel("",currencyCode)%>
	<%= widgetFactory.createAmountField( "TransactionAmount", "", displayAmount, currencyCode, isReadOnly, false, false, "onChange='setNewAmount();'", "", "none") %> 				
	</div>
	<%}else{
		displayAmount = TPCurrencyUtility.getDisplayAmount(displayAmount, currencyCode, loginLocale);		
		StringBuffer         tempDisplayAmt              = new StringBuffer();
		tempDisplayAmt.append(currencyCode);
		tempDisplayAmt.append(" ");
		tempDisplayAmt.append(displayAmount);
		%>
				<%= widgetFactory.createAmountField( "TransactionAmount", " ", tempDisplayAmt.toString(), "", true, false, false, "onChange='setNewAmount();'", "", "") %>
				<input type="hidden" name="TransactionAmount"
			               value="<%=amountValue%>">			
			<%}%>
			
	<%
		if (!isProcessedByBank) {
	%>
	<%
		if (!badAmountFound) {
	%>
	
	<%= widgetFactory.createTextField( "TransactionAmount2", "TransferELCAmend.NewTransferAmount", newTransferAmount.toString(), "22", true, false, false, "", "", "") %>				
	
	<%
		}
		}
	%>
	<div class="formItem">
		<%=widgetFactory.createStackedLabel("",
					"TransferELCAmend.NewAmountTolerance")%>
		
		<%= widgetFactory.createSubLabel( "transaction.Plus") %>
		<%if(!isReadOnly){ %>
		<%= widgetFactory.createPercentField( "TransactionAmountPlusTolerance", "", terms.getAttribute("amt_tolerance_pos"), isReadOnly,
									false, false, "", "", "none") %>
		<%}else {%>
			<b><%=terms.getAttribute("amt_tolerance_pos") %></b>
		<%} %>							
		
		
		<%= widgetFactory.createSubLabel( "transaction.Percent") %>
		&nbsp;&nbsp;
		<%= widgetFactory.createSubLabel( "transaction.Minus") %>
		<%if(!isReadOnly){ %>
		<%= widgetFactory.createPercentField( "TransactionAmountMinusTolerance", "", terms.getAttribute("amt_tolerance_neg"), isReadOnly,
									false, false, "", "", "none") %>
		<%}else {%>
			<b><%=terms.getAttribute("amt_tolerance_neg") %></b>
		<%} %>	
		
		<%= widgetFactory.createSubLabel( "transaction.Percent") %>
		<div style="clear:both;"></div>
</div>
</div>
<%-- Right Column Ends.... General Section --%>
