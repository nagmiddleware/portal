<%--
*******************************************************************************
              Invoice Search include file

  Description:
  This is a JSP meant to be included using the <%@ include file="x.jsp" %> 
  tag for Invoice search.  It creates the HTML for displaying Invoice 
  Search fields (Invoice ID, currency ,amount, and Seller Name) for the 
  Search. The file also handles building the extended WHERE Clause that will
  retrieve Invoice entries based on the search criteria that was entered by the 
  user.

  Because this file is used by including with <%@ include %> rather than with
  the <jsp:include> directive, it is not a standalone servlet.  

*******************************************************************************
--%>
<%--
 *     Copyright  � 2006, CGI-AMS - All rights reserved
--%>

<input type="hidden" name="NewSearch" value="Y">

<%
	String loginLocale=userSession.getUserLocale();
    String DELIMITER = "@";
    StringBuffer newSearchCriteria = new StringBuffer();
	String invID=null, beneficiaryName=null, amountFrom=null, amountTo=null, invCurrency=null;
	String fromDate = null;
	String toDate = null;
	String options = "";
	invID = request.getParameter("InvID") != null ? request.getParameter("InvID").toUpperCase() : "";
	beneficiaryName = request.getParameter("BeneName") != null ? request.getParameter("BeneName").toUpperCase() : "";	
	amountFrom = request.getParameter("AmountFrom") != null ? request.getParameter("AmountFrom") : "";	
	amountTo = request.getParameter("AmountTo") != null ? request.getParameter("AmountTo") : "";	
	invCurrency = request.getParameter("Currency") != null ? request.getParameter("Currency").toUpperCase() : "";
	
	  
	  amountFrom = request.getParameter("AmountFrom") != null ? request.getParameter("AmountFrom") : "";
		
		amountTo = request.getParameter("AmountTo") != null ? request.getParameter("AmountTo") : "";
		
		invCurrency = request.getParameter("Currency") != null ? request.getParameter("Currency").toUpperCase() : "";
		
		
		
		//get DateFormat from usersession
		   
		   
		   if(dPattern == null){
		 	   dPattern = "";
		   }else if("dd/mm/yyyy".equalsIgnoreCase(dPattern) || "dd/mm/yy".equalsIgnoreCase(dPattern)){
		 	   dPattern = "dd/MM/yyyy";
		   }else if("dd-mm-yyyy".equalsIgnoreCase(dPattern) || "dd-mm-yy".equalsIgnoreCase(dPattern)){
		 	   dPattern = "dd-MM-yyyy";
		   }else if("mm/dd/yyyy".equalsIgnoreCase(dPattern) || "mm/dd/yy".equalsIgnoreCase(dPattern)){
		 	   dPattern = "MM/dd/yyyy";
		   }else if("mm-dd-yyyy".equalsIgnoreCase(dPattern) || "mm-dd-yy".equalsIgnoreCase(dPattern)){
		 	   dPattern = "MM-dd-yyyy";
		   }else if("yyyy/dd/mm".equalsIgnoreCase(dPattern) || "yy/dd/mm".equalsIgnoreCase(dPattern)){
		 	   dPattern = "yyyy/dd/MM";
		   }else if("yyyy/mm/dd".equalsIgnoreCase(dPattern) || "yy/mm/dd".equalsIgnoreCase(dPattern)){
		 	   dPattern = "yyyy/MM/dd";
		   }else if("yyyy-mm-dd".equalsIgnoreCase(dPattern) || "yy-mm-dd".equalsIgnoreCase(dPattern)){
		 	   dPattern = "yyyy-MM-dd";
		   }else if("yyyy-dd-mm".equalsIgnoreCase(dPattern) || "yy-dd-mm".equalsIgnoreCase(dPattern)){
		 	   dPattern = "yyyy-dd-MM";
		   }
		
%>

 <%-- JavaScript to enable form submission by enter.  --%>
      <script LANGUAGE="JavaScript">
      
         function enterSubmit(event, myform) {
            if (event && event.which == 13) {
               setButtonPressed('null', 0);
               myform.submit();
            }
            else if (event && event.keyCode == 13) {
               setButtonPressed('null', 0);
               myform.submit();
            }
            else {
               return true;
            }
         }
  
     
      </script>

 <div class="searchDetail">
    <span class="searchCriteria">
			<%=widgetFactory.createLabel("","InvoiceSearch.InvoiceID",false, false, false, "inline searchLabel")%>
			<%=widgetFactory.createTextField("InvID","", invID, "14", false, false, false, "class='char6'",  "", "inline searchItem")%>
		<%=widgetFactory.createLabel("","InstSearch.AmountFrom",false, false, false, "inline searchLabel")%>
			<%=widgetFactory.createTextField("AmountFrom","",   StringFunction.xssCharsToHtml(amountFrom), "14", false, false, false, "class='char6'",  "", "inline searchItem")%>
		<%=widgetFactory.createLabel("","InstSearch.To",false, false, false, "inline searchLabel")%>
			<%=widgetFactory.createTextField("AmountTo","", StringFunction.xssCharsToHtml(amountTo), "14", false, false, false, "class='char6'",  "", "inline searchItem")%>
<%
			if (fromAddInvoice) {
		%>	
		<%options = Dropdown.createSortedRefDataOptions(TradePortalConstants.CURRENCY_CODE, invCurrency, loginLocale);
  			out.println(widgetFactory.createSearchSelectField("Currency", "InvoiceSearch.Currency", " ", options,"class='char10'"));
  			%>
  			<%} %>
   </span>
    <span class="searchActions">						
		
			<button data-dojo-type="dijit.form.Button" type="button" id="PurchaseOrderSearch" name="PurchaseOrderSearch">
				<%=resMgr.getText("common.FilterText", TradePortalConstants.TEXT_BUNDLE)%>
			    <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
	                  filterPOs();return false;
	      		</script>
			</button>
   </span>
   <div style="clear: both;"></div>
</div>


<%
String currValue=(String)request.getHeader("User-Agent"); 
String browser = "";
String version = "";
boolean isIE7 = false;
if(currValue != null ){
if(currValue.indexOf("MSIE") >0){
   browser = "IE";
   String tempStr = currValue.substring(currValue.indexOf("MSIE"),currValue.length());
   version = (tempStr.substring(4,tempStr.indexOf(";"))).trim();
}
}
if(("IE").equals(browser) && ("7.0").equals(version)) {
      isIE7 = true;
}
if(isIE7) {
%>
<br>
<%}%>


<div class="searchDetail">
	<span class="searchCriteria">
		<%
			if (fromAddInvoice) {
		%>							
			<%=widgetFactory.createLabel("","InvoiceSearchAtp.seller_name",false, false, false, "inline searchLabel")%>									
			<%=widgetFactory.createTextField("BeneName","",beneficiaryName, "35", false, false, false, "class='char6'",  "", "inline searchItem")%>
			<%} %>
			<%=widgetFactory.createLabel("","AtpInvoiceSearch.DateRange",false, false, false, "inline searchLabel")%>	
			<%-- DK IR T36000016621 Rel8.2 05/02/2013 Starts --%>								
			<%=widgetFactory.createSearchDateField("DateFrom","InvoiceSearch.From","class='char15'","constraints:{datePattern:'"+dPattern+"'},placeHolder:'"+dPattern+"'") %>
		<%=widgetFactory.createSearchDateField("DateTo","InvoiceSearch.To","class='char15'","constraints:{datePattern:'"+dPattern+"'},placeHolder:'"+dPattern+"'") %>
		<%-- DK IR T36000016621 Rel8.2 05/02/2013 Starts --%>	
    </span>
     <div style="clear: both;"></div>
</div>

<%= widgetFactory.createHoverHelp("PurchaseOrderSearch", "SearchHoverText")%>	
