<%--
*******************************************************************************
  Transaction-FTRQ-ISS-BeneAcctData.frag

  Description:
  A fragment for displaying terms party accounts.
  Fairly generic, but should be copied for specific instrument behavior.

  The following variables are passed:

    termsParty - a TermsPartyWebBean
    isReadOnly
    isTemplate
    widgetFactory

*******************************************************************************
--%>
<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.html.*,java.math.*" %>

<%
  // Logic to handle account list.  If no payee has been searched, there is
  // only an enterable account number field.  Otherwise, we display the set of accounts defined
  // for the party (when the party is selected, we get the set of accounts for that party 
  // and save it on the TermsParty record -- if the party's accounts change later, we do not update
  // the set on the TermsParty record.) as well as an enterable account number field.  The account 
  // number selected by the user will be one of the payee's predefined accounts or the 
  // entered account number.

  String acctList = StringFunction.xssHtmlToChars(termsParty.getAttribute("acct_choices"));
  DocumentHandler acctData = new DocumentHandler(acctList, true);
  DocumentHandler acct;
  boolean foundAcctMatch = false;	// indicates if ANY predefined acct num matches ben. acct  number from business object
  boolean foundMatch = false;		// indicates if a given acct num matches ben. acct number from business object
  Vector accounts = acctData.getFragments("/DocRoot/ResultSetRecord");
  int numAccts = accounts.size();
  String termsPartyAcctNum = null;
  String termsPartyAcctCcy = null;

  Debug.debug("Account Choices for Party: " + acctList);
  Debug.debug("Number of account in list is " + numAccts);

  // Set the showAccts flag that determines if a list of accounts with radio buttons
  // appears.  They appear if we have a set of accounts (i.e., it's been preloaded into
  // a acctList variable).  However, in readonly mode, we do not display the list.

  boolean showAccts = false;
  if (numAccts > 0) showAccts = true;
  if (isReadOnly) showAccts = false;

  // Store the set of acc<%--ounts in a hidden field.  This gets written to the
  // TermsParty record so we always have the set of accounts at the time the party was selected.
%>
  <input type="hidden" name="PayeeAcctChoices" value='<%=acctList%>'>

<%
  if (showAccts) {
    // This loop prints the set of accounts previously determined when the party was selected.
    for (int i = 0; i<numAccts; i++) {
      acct = (DocumentHandler) accounts.elementAt(i);
		
      // Display a radio button using the "account number/currency" from the
      // account list (for whichever row we're on).  Select the correct
      // radio button if we match on the current value of "acct_num" on the
      // beneficiary party.
		
      // (Don't know why, but the first "get" from the doc must be without the /,
      // succeeding ones must be with a /.  This is different from similar logic
      // elsewhere.  Believed to be a symptom of flattening a DocHandler to a string
      // and then creating another DocHandler from the string.)
		
      termsPartyAcctNum = acct.getAttribute("ACCOUNT_NUMBER");       // no "slash"
      termsPartyAcctCcy = acct.getAttribute("/CURRENCY");            // get with "slash"
		
      foundMatch = termsPartyAcctNum.equals(termsParty.getAttribute("acct_num"));
		
      if (!foundAcctMatch) {
        foundAcctMatch = foundAcctMatch | foundMatch;
      }

      String radioLabel;
      if (termsPartyAcctCcy.equals("")) {
        radioLabel = termsPartyAcctNum;
      } else {
        radioLabel = termsPartyAcctNum + " (" + termsPartyAcctCcy + ")";
      }
      out.println(widgetFactory.createRadioButtonField( "SelectedAccount", 
        "SelectedAccount" + termsPartyAcctNum, radioLabel, //id, label
        termsPartyAcctNum + TradePortalConstants.DELIMITER_CHAR + termsPartyAcctCcy,  //value if selected
        foundMatch, isReadOnly, "", ""));
      out.println("<div style=\"clear:both;\"></div>");
    }  // End of for loop

    // This IF sets up acctNum and acctCcy fields with values if necessary and uses them
    // for displaying the enterable account field.
		
    // Display radio button for selection of the enterable account.  If we haven't found
    // an account match yet, then we might match on the enterable value.  If they match,
    // turn on the radio button.
    if (foundAcctMatch) {
      termsPartyAcctNum = "";
      termsPartyAcctCcy = "";
      foundMatch = false;
    } else {
      termsPartyAcctNum = termsParty.getAttribute("acct_num");
      termsPartyAcctCcy = termsParty.getAttribute("acct_currency");
      if (termsPartyAcctNum == null) termsPartyAcctNum = "";
      foundMatch = termsPartyAcctNum.equals(termsParty.getAttribute("acct_num"));
    }
    //now generate the text field, but without label as that is above
%>
    <div>
      <%= widgetFactory.createRadioButtonField( "SelectedAccount", 
            "SelectedAccount" + TradePortalConstants.USER_ENTERED_ACCT, "", //id, label
            TradePortalConstants.USER_ENTERED_ACCT, //value
            foundMatch, isReadOnly, "inline", "") %>
      <%= widgetFactory.createTextField( "EnteredAccount", "", termsPartyAcctNum, 
            "34", isReadOnly, false, false,  "class='char30'", "","none" ) %>
    </div>
<%
  } else {
    // We're not displaying the account list, so populate the account field with the
    // account from the Payee party record.
    termsPartyAcctNum = termsParty.getAttribute("acct_num");
    termsPartyAcctCcy = termsParty.getAttribute("acct_currency");
    if (termsPartyAcctNum == null) termsPartyAcctNum = "";

    //note no label on the below
%>
    <%= widgetFactory.createTextField( "EnteredAccount", "", termsPartyAcctNum, 
          "34", isReadOnly, !isTemplate, false,  "class='char32'", "","none" ) %>   
<%
  }
%>
