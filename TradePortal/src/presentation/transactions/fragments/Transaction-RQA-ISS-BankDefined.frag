<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
*******************************************************************************
                 Request to Advise Issue Page - Bank Defined section

  Description:
    Contains HTML to create the Request to Advise Issue Bank Defined section.  

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transaction-RQA-ISS-BankDefined.jsp" %>
*******************************************************************************
--%>

<div class = "columnLeft"> 
		<%= widgetFactory.createTextField( "PurposeType", "RequestAdviseIssue.PurposeType", terms.getAttribute("purpose_type"), "3", isReadOnly ) %>
		
</div>
<div class = "columnRight">
		<%= widgetFactory.createCheckboxField( "Irrevocable", "RequestAdviseIssue.Irrevocable", terms.getAttribute("irrevocable").equals(TradePortalConstants.INDICATOR_YES), isReadOnly) %>
		<%= widgetFactory.createCheckboxField( "Operative", "RequestAdviseIssue.Operative", terms.getAttribute("operative").equals(TradePortalConstants.INDICATOR_YES), isReadOnly) %> 
</div>		