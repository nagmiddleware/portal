<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
*******************************************************************************
                 Import DLC Issue Page - Internal Instructions section

  Description:
    Contains HTML to create the Import DLC Issue Internal Instructions section.  

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transaction-IMP_DLC-ISS-InternalInstructions.jsp" %>
*******************************************************************************
--%>
<div class = "formItem">
<%= widgetFactory.createNote("ImportDLCIssue.IntInstrText") %>
</div>
<%if(!isReadOnly){%>
<%= widgetFactory.createTextArea( "InternalInstructions", "", terms.getAttribute("internal_instructions"), isReadOnly, false,false, "rows='10' cols='100'","","") %>
 <%}else{%> 
<%= widgetFactory.createAutoResizeTextArea( "InternalInstructions", "", terms.getAttribute("internal_instructions"), isReadOnly, false,false, "style='width:600px;min-height:140px;' cols='128'","","") %>
<%}%>