<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
*******************************************************************************
                     Loan Request Page - Link section

  Description:
    Contains HTML to display links to the other sections.

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transaction-LRQ-ISS-General.jsp" %>
*******************************************************************************
--%>

  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td width="14" nowrap>&nbsp;</td>
      <td nowrap align="left" valign="middle"> 
        <p class="ControlLabel">
          <a href="#General">
            <%=resMgr.getText("LoanRequest.General", 
                              TradePortalConstants.TEXT_BUNDLE)%>
          </a>
        </p>
      </td>
      <td width="15" nowrap>&nbsp;</td>
      <td nowrap align="left" valign="middle"> 
        <p class="ControlLabel">
          <a href="#LoanRequestDetails">
            <%=resMgr.getText("LoanRequest.LoanRequestDetails", 
                              TradePortalConstants.TEXT_BUNDLE)%>
          </a>
        </p>
      </td>
      <td width="100%" height="30">&nbsp;</td>
    </tr>
    <tr> 
      <td width="14" nowrap>&nbsp;</td>
      <td nowrap align="left" valign="middle"> 
        <div id="ExportLoanLink" style="display: block;">
		  <p class="ControlLabel">
		  	<a href="#ExportInstructions">
            	<%=resMgr.getText("LoanRequest.ExportInstructions", 
                              	TradePortalConstants.TEXT_BUNDLE)%>
			</a>
	      </p>
		</div>
		<div id="ImportLoanLink" style="display: block;">
		  <p class="ControlLabel">
            <a href="#ImportInstructions">
              <%=resMgr.getText("LoanRequest.ImportInstructions", 
                              TradePortalConstants.TEXT_BUNDLE)%>
			</a>
		  </p>
		</div>
		<div id="InvoiceFinancingLink" style="display: block;">
		  <p class="ControlLabel">
            <a href="#InvoiceInstructions">
              <%=resMgr.getText("LoanRequest.InvoiceInstructions", 
                              TradePortalConstants.TEXT_BUNDLE)%>
			</a>
		  </p>
		</div>
		<div id="LoanLink" style="display: block;">
		  <p class="ControlLabel">
            <a href="#ExportInstructions">
              <%=resMgr.getText("LoanRequest.LoanInstructions", 
                              TradePortalConstants.TEXT_BUNDLE)%>
            </a>
		  </p>
		</div>
      </td>
	  <td width="15" nowrap>&nbsp;</td>
      <td nowrap align="left" valign="middle"> 
        <p class="ControlLabel">
          <a href="#ChargesAndInterest">
            <%=resMgr.getText("LoanRequest.ChargesAndInterest", TradePortalConstants.TEXT_BUNDLE)%>
          </a>
        </p>
      </td>
	  <td width="100%" height="30">&nbsp;</td>
	</tr> 
    <tr> 
      <td width="14" nowrap>&nbsp;</td>
      <td nowrap align="left" valign="middle"> 
        <p class="ControlLabel">
          <a href="#ExchangeRate">
            <%=resMgr.getText("LoanRequest.ExchangeRate", TradePortalConstants.TEXT_BUNDLE)%>
          </a>
        </p>
      </td>
	  <td width="15" nowrap>&nbsp;</td>
      <td nowrap align="left" valign="middle"> 
        <p class="ControlLabel">
          <a href="#BankInstructions">
            <%=resMgr.getText("LoanRequest.InstructionstoBank", 
                              TradePortalConstants.TEXT_BUNDLE)%>
          </a>
        </p>
      </td>
	  <td width="100%" height="30">&nbsp;</td>
	</tr> 
    <tr> 
      <td width="14" nowrap>&nbsp;</td>
      <td nowrap align="left" valign="middle"> 
        <p class="ControlLabel">
          <a href="#InternalInstructions">
            <%=resMgr.getText("LoanRequest.InternalInstructions", 
                              TradePortalConstants.TEXT_BUNDLE)%>
          </a>
        </p>
      </td>
	  <td width="15" nowrap>&nbsp;</td>
 <% 
  // Only display the bank defined section if the user is an admin user
  if( userSession.getSecurityType().equals(TradePortalConstants.ADMIN) )
   {
  %>	  
      <td nowrap align="left" valign="middle"> 
        <p class="ControlLabel">
          <a href="#BankDefined">
            <%=resMgr.getText("LoanRequest.BankDefined", TradePortalConstants.TEXT_BUNDLE)%>
          </a>
        </p>
      </td>

  <%
    } else {
   %>
      <td width="15" nowrap>&nbsp;</td>
      <td>&nbsp;</td> 
  <%
    }
   %>   
	<td width="100%" height="30">&nbsp;</td>
	</tr> 
  </table>
