<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
/********************************************************************************
 *                    Guarantee Issue Page - Internal Instructions section
 *
 * Description:
 *    Contains HTML to create the Guarantee Issue Internal Instructions section.
 *
 * Note:
 *    This is not a standalone JSP.  It MUST be included using the following tag:
 *        <%@ include file="Transaction-GUAR-ISS-InternalInstructions.jsp" %>
 ********************************************************************************/
--%>

	
	 	
	 <span class="formItem">
	 <%= widgetFactory.createNote("GuaranteeIssue.InternalInstructionsText") %></span>
	 <%if(!isReadOnly){%>
	 	<%=widgetFactory.createTextArea("internal_instructions","",
                        terms.getAttribute("internal_instructions"), 
                        isReadOnly, false, false, "rows='10' cols='128'", "", "") %>
                        <%}else{%>
      	<%=widgetFactory.createAutoResizeTextArea("internal_instructions","",
                        terms.getAttribute("internal_instructions"), 
                        isReadOnly, false, false, "style='width:600px;min-height:140px;'cols='128'", "", "") %>
                        <%}%>
