<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
*******************************************************************************
                 Export Collection Issue Page - Bank Instructions section

  Description:
    Contains HTML to create the Export COL Issue Bank Instructions section.  

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transaction-EXP_COL-ISS-BankInstructions.jsp" %>
*******************************************************************************
--%>

<%
//Get the formatted FEC amount (based on the currency of the general section)

  String displayFECAmount;  
  String displayFECRate;

  if(!getDataFromDoc)
   {
	 displayFECAmount = TPCurrencyUtility.getDisplayAmount(terms.getAttribute("fec_amount"), 
              terms.getAttribute("amount_currency_code"), loginLocale);
     displayFECRate   = TPCurrencyUtility.getDisplayAmount(terms.getAttribute("fec_rate"), 
                                                "", loginLocale);
   }
  else
   {
	 displayFECAmount = terms.getAttribute("fec_amount");
     displayFECRate   = terms.getAttribute("fec_rate");
   }
String regExpRange = "";
    regExpRange = "[0-9]{0,5}([.][0-9]{0,8})?";
    regExpRange = "regExp:'" + regExpRange + "'"; 
%>

  
  <%
  /*********************************************************
  * Start of Instructions to Bank Section - beige bar
  **********************************************************/
  %>
    
  <%
  /********************************
  * Special Instructinos DropDown
  ********************************/
  %>
    <%= widgetFactory.createLabel( "ExportCollectionIssue.EnterAnyAdditionalInstHere", "ExportCollectionIssue.EnterAnyAdditionalInstHere", false, false, false, "") %>
    

<%
        if (isReadOnly) {
          out.println("&nbsp;");
        } else {
          options = ListBox.createOptionList(SpclInstrDocList, "PHRASE_OID", "NAME", "", userSession.getSecretKey());
          defaultText = resMgr.getText("transaction.SelectPhrase",
                                       TradePortalConstants.TEXT_BUNDLE);
          
          out.println(widgetFactory.createSelectField( "CommInvPhraseItem", "", defaultText, options, isReadOnly, false, false, "onChange=" + PhraseUtility.getPhraseChange(
                  "/Terms/special_bank_instructions",
                  "SpclBankInstructions", "1000","document.forms[0].SpclBankInstructions"), "", "inline"));
        }
				%>
	   &nbsp;			
	   <%= widgetFactory.createNote("ExportCollectionAmend.InstsenttoBank") %>

	
	
  <%
  /********************************
  * Special Instructinos TextArea
  ********************************/
  %>
        <%if(!isReadOnly){ %>
         <%= widgetFactory.createTextArea( "SpclBankInstructions", "", terms.getAttribute("special_bank_instructions"), isReadOnly,false, false, "rows='10'","", "" ) %>                            
         <%= widgetFactory.createHoverHelp("SpclBankInstructions","SpclBankInstructions")%>
         <%}else{ %>
         <%=widgetFactory.createAutoResizeTextArea("SpclBankInstructions", "", terms.getAttribute("special_bank_instructions"), isReadOnly,false, false, "style='width:600px;;min-height:140px;'","", "" ) %>
         <%}%>
  
  <div class="columnLeft">
  <%
  /********************************
  * Settlement Instructions
  ********************************/
  %>
    
       <%=widgetFactory.createSubsectionHeader("ExportCollectionIssue.SettlementInstructions",isReadOnly,false,false,"")%>                                 
  <%
  /******************************
  * Credit - Our Account number
  ******************************/
  %>        
           <%= widgetFactory.createTextField( "CreditOurAcct", "ExportCollectionIssue.CreditOurAccountNumber", terms.getAttribute("settlement_our_account_num"), "30", isReadOnly, false, false, "", "", "" ) %> 
  <%
  /*********************
  * Branch Code
  *********************/
  %>
   
          <%= widgetFactory.createTextField( "BranchCode", "ExportCollectionIssue.BranchCode", terms.getAttribute("branch_code"), "30", isReadOnly, false, false, "", "", "" ) %>                              
                                  
      
  <%
  /*************************************
  * Credit - Foreign Ccy Account number
  *************************************/
  %>
          <%= widgetFactory.createTextField( "CreditForeignAcct", "ExportCollectionIssue.CreditForeignCurrencyAccountNumber", terms.getAttribute("settlement_foreign_acct_num"), "30", isReadOnly, false, false, "", "", "" ) %>                              
      </td>
  <%
  /******************************
  * Currency of Account
  ******************************/
  %>
      
<%
          options = Dropdown.createSortedCurrencyCodeOptions(
                      terms.getAttribute("settlement_foreign_acct_curr"), loginLocale);
          
          out.println(widgetFactory.createSelectField( "CreditForeignCcy", "ExportCollectionIssue.CcyofAccount", " ", options, isReadOnly, false, false, "class='char5'", "",""));
          //out.println(widgetFactory.createCurrencySelectField("CreditForeignCcy","ExportCollectionIssue.CcyofAccount",options,"",isReadOnly,false,false));
%>
<div style="clear: both;"></div>

</div>
<div class="columnRight"> 
  
  <%
  /*********************************
  * FEC - Forward Exchange Contract
  *********************************/
  %>
    
           <%=widgetFactory.createSubsectionHeader("ExportCollectionIssue.FEC",isReadOnly,false,false,"")%>                 
	
  <%
  /*********************************
  * Covered By FEC Number
  *********************************/
  %>
 
          <%= widgetFactory.createTextField( "FECNumber", "ExportCollectionIssue.CoveredByFECNumber", terms.getAttribute("covered_by_fec_number"), "14", isReadOnly, false, false, "", "", "inline" ) %>                              
  
  <%
  /********************
  * Rate of FEC
  ********************/
  %>
    
          <%= widgetFactory.createTextField( "FECRate", "ExportCollectionIssue.RateofFEC", displayFECRate.replace(",",""), "14", isReadOnly, false, false, "class='char10'",regExpRange, "inline" ) %> 
          <div style="clear: both;"></div>     
  <%
  /********************
  * Amount of FEC
  ********************/
  %>
      
<%-- Uses Predefined Variable 'displayFECAmount' for the data to be displayed.  --%>
<%-- This variable is defined at the top of this JSP. 				--%>
       <div>  
       <%--<%= widgetFactory.createTextField( "FECAmount", "ExportCollectionIssue.AmountofFEC", displayFECAmount, "30", isReadOnly, false, false, "class='char15'", "regExp:'[0-9]+'", "inline") %>--%>
       <%= widgetFactory.createAmountField( "FECAmount", "ExportCollectionIssue.AmountofFEC", displayFECAmount, currencyCode, isReadOnly, false, false, "class='char15'", "readOnly:" +isReadOnly, "inline") %>                       
      
  <%
  /*************************
  * Maturity Date of FEC
  *************************/
  %>
               
	  <%= widgetFactory.createDateField("FECMaturityDate", "ExportCollectionIssue.MaturityDateofFEC",  StringFunction.xssHtmlToChars(terms.getAttribute("fec_maturity_date")), isReadOnly, false, false, "class='char10'","placeHolder:'"+datePatternDisplay+"'", "inline") %>
	  </div>
</div>
<div style="clear: both;"></div>
 <%
  /*************************************
  * Additional Settlement Instructions
  *************************************/
  %>
     <%= widgetFactory.createLabel( "ExportCollectionIssue.EnterAnyAdditionalInstHere", "ExportCollectionIssue.EnterAnyAdditionalInstHere", false, false, false, "") %>                                                    
  <%
  /*************************************
  * Settlement Phrase Dropdown
  *************************************/
  %>
<%
        if (isReadOnly) {
          out.println("&nbsp;");
        } else {
          options = ListBox.createOptionList(SettlementInstrDocList, "PHRASE_OID", "NAME", "", userSession.getSecretKey());
          defaultText = resMgr.getText("transaction.SelectPhrase",
                                       TradePortalConstants.TEXT_BUNDLE);
         
          out.println(widgetFactory.createSelectField( "AddSettlementPhraseItem", "", defaultText, options, isReadOnly, false, false, "onChange=" + PhraseUtility.getPhraseChange(
                  "/Terms/other_settlement_instructions",
                  "AdditionalSettlementText", "4000","document.forms[0].AdditionalSettlementText"), "", ""));
%>
       
<%
        }
%>
  <%
  /*************************
  * Settlement TextArea
  **************************/
  %>
	
        <%if(!isReadOnly) {%>                             
       <%= widgetFactory.createTextArea( "AdditionalSettlementText", "", terms.getAttribute("other_settlement_instructions"), isReadOnly,false, false, "rows='10',cols='100'","", "" ) %>
       <%= widgetFactory.createHoverHelp("AdditionalSettlementText","AdditionalSettlementHoverText")%>
       <%}else{%>
       <%=widgetFactory.createAutoResizeTextArea( "AdditionalSettlementText", "", terms.getAttribute("other_settlement_instructions"), isReadOnly,false, false, "style='width:600px;;min-height:140px;'","", "" ) %>
       <%}%>
