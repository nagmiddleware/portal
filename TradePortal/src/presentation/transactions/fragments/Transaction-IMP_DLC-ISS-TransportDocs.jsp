<%--for the ajax call to add shipments--%>

<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.html.*,java.math.*,
                 java.util.*" %>



<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session">
</jsp:useBean>

<% 
  //parameters -
  String parmValue = "";
  boolean isReadOnly = false; //request.getParameter("isReadOnly");
  boolean isFromExpress = false; //request.getParameter("isFromExpress");
  boolean isExpressTemplate = false; //request.getParameter("isExpressTemplate");
  boolean isTemplate = false; //request.getParameter("isExpressTemplate");
  parmValue = request.getParameter("pane");
  int pane = 0;
  if ( parmValue != null ) {
    try {
      pane = (new Integer(parmValue)).intValue();
    } catch (Exception ex ) {
    }
  }

  //other variable setup
  String loginLocale = userSession.getUserLocale();
  String loginRights = userSession.getSecurityRights();
  WidgetFactory widgetFactory = new WidgetFactory(resMgr);
  String options;
  String defaultText;
  String defaultText1 = resMgr.getText("transaction.SelectPhrase",TradePortalConstants.TEXT_BUNDLE);
  String consignType = "";
  Hashtable secureParms = new Hashtable();
  Vector codesToExclude2= new Vector(); 
  codesToExclude2.addElement(TradePortalConstants.CONSIGN_BUYER);
  codesToExclude2.addElement(TradePortalConstants.CONSIGN_BUYERS_BANK);
  String notifySearchHtml = "";
  String consigneeSearchHtml = "";
  String poLineItemSourceType = "";

  DocumentHandler transportDocList = PhraseUtility.createPhraseList(
                                    TradePortalConstants.PHRASE_CAT_TRANSPORT,
                                    userSession, formMgr, resMgr);
  DocumentHandler goodsDocList = PhraseUtility.createPhraseList(
                                    TradePortalConstants.PHRASE_CAT_GOODS,
				    userSession, formMgr, resMgr);
    boolean           hasPOLineItems                = false;
    boolean           hasStructuredPOs              = false;
    boolean           userCanUploadPOs              = false;
    boolean           userCanAddManualPOs           = false;

  //when included per ajax, the business objects will be blank
  TransactionWebBean transaction  = (TransactionWebBean) beanMgr.getBean("Transaction");
  ShipmentTermsWebBean shipmentTerms   = beanMgr.createBean(ShipmentTermsWebBean.class, "ShipmentTerms");
  TermsPartyWebBean termsPartyNot    = beanMgr.createBean(TermsPartyWebBean.class, "TermsParty");
  TermsPartyWebBean termsPartyOth    = beanMgr.createBean(TermsPartyWebBean.class, "TermsParty");
  
  String markedFreight = shipmentTerms.getAttribute("trans_doc_marked_freight");
%>
