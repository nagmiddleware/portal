<%--
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
--%>
<%--
**********************************************************************************
                        Authorized Transactions Page

  Description:
    Contains HTML to create the Authorized tab for the Transactions Home page

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transactions-AuthorizedListView.jsp" %>
*******************************************************************************
--%>

<%--cquinton 8/30/2012 add grid search header--%>
<div class="gridSearch">
  <div class="searchHeader">
    <span class="searchHeaderCriteria">

<%
  //cquinton 3/11/2013 use constant instead of translated value for ALL
  final String ALL_ORGS = "AuthorizedTransactions.AllOrganizations";

  // Display Organizations dropdown or the single organization.

  if ((SecurityAccess.hasRights(userSecurityRights,
                                SecurityAccess.VIEW_CHILD_ORG_WORK))
      && (totalOrganizations > 1)) {
    dropdownOptions.append(Dropdown.getUnsortedOptions(hierarchyDoc,
                                                       "ORGANIZATION_OID", "NAME",
                                                       selectedOrg, userSession.getSecretKey()));
    dropdownOptions.append("<option value=\"");
    dropdownOptions.append(EncryptDecrypt.encryptStringUsingTripleDes(ALL_ORGS, userSession.getSecretKey()));
    dropdownOptions.append("\"");

    if (selectedOrg.equals(ALL_ORGS)) {
      dropdownOptions.append(" selected");
    }

    dropdownOptions.append(">");
    dropdownOptions.append(resMgr.getText(ALL_ORGS, TradePortalConstants.TEXT_BUNDLE));
    dropdownOptions.append("</option>");
%>
      <%-- Added code for defect 4372 by dillip --%>
      <%= widgetFactory.createSearchSelectField("Organization","AuthorizedTransactions.Show","", dropdownOptions.toString(), "onChange='local.filterAuthListView();' class='inline'" )%>
      <%= widgetFactory.createSubLabel("AuthorizedTransactions.ItemsAuthorized")%>
      <%--Ended Here --%>

<% 
  }
  else { 
%>
      <%= widgetFactory.createSubLabel("AuthorizedTransactions.ItemsAuthorized")%>
<%  
  } 
%>
    </span>
    <span class="searchHeaderActions">

      <%--cquinton 10/8/2012 include gridShowCounts --%>
      <jsp:include page="/common/dGridShowCount.jsp">
        <jsp:param name="gridId" value="tradeAuthGrid" />
      </jsp:include>

      <span id="tradeAuthRefresh" class="searchHeaderRefresh"></span>
      <%=widgetFactory.createHoverHelp("tradeAuthRefresh","RefreshImageLinkHoverText") %>
      <span id="tradeAuthGridEdit" class="searchHeaderEdit"></span>
      <%=widgetFactory.createHoverHelp("tradeAuthGridEdit","CustomizeListImageLinkHoverText") %>
    </span>
    <div style="clear:both;"></div>
  </div>
</div>

<%
  DGridFactory dgridFactory = new DGridFactory(resMgr, userSession, formMgr, response);
  gridHtml = dgFactory.createDataGrid("tradeAuthGrid","InstrumentsAuthorizedTransactionsDataGrid",null);
%>
<%= gridHtml %>
