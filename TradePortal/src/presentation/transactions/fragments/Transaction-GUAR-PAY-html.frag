<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
*******************************************************************************
                     Guarantee Payment Page - all sections

  Description:
    Contains HTML to create the Guarantee Amend page.  All data retrieval 
  is handled in the Transaction-GUAR-PAY.jsp.

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transaction-GUAR-PAY-html.jsp" %>
*******************************************************************************
--%>
<%--
*******************************************************************************
               Guarantee Payment Page - General section

*******************************************************************************
--%>
  <input type=hidden name=IncreaseDecreaseFlag id=decrease value="Decrease" />
  		  <%= widgetFactory.createSectionHeader("1", "GuaranteePayment.General") %>
  		  <%--
  		  	RKAZI PPX-269 REL 8.4 Kyriba 09/06/2013 - Add - START
  		  --%>
  		  <% if (TradePortalConstants.INDICATOR_YES.equals(transaction.getAttribute("converted_transaction_ind"))){ %>
          	 	<div class="formItem">  
                  <%= widgetFactory.createCheckboxField("ConvTransInd", "GuaranteePayment.ConvTransInd",  TradePortalConstants.INDICATOR_YES.equals(transaction.getAttribute("converted_transaction_ind")), true,false,"","","none") %><br/>
                </div>
		  <%}%>                
  		  <%--
  		  	RKAZI PPX-269 REL 8.4 Kyriba 09/06/2013 - Add - END
  		  --%>
          <div class="columnLeft">
    <%
     if(!isProcessedByBank)
      {
    	 String gteeAmt = currencyCode +"  "+ TPCurrencyUtility.getDisplayAmount(originalAmount.toString(), currencyCode, loginLocale);
   %>
    <%= widgetFactory.createTextField("AvailableAmount", "Guarantee.CurrentAvailableAmount", availableAmount.toString(), "25", true, true,false, "", "", "")%>
    
<%
	}
%>       
<%= widgetFactory.createSubsectionHeader("GuaranteePayment.PaymentAmount") %>  
	<%if(!isReadOnly){ %>

	     
	
	<div class="formItem">
	<%=widgetFactory.createInlineLabel("",currencyCode)%>
	<%= widgetFactory.createAmountField( "amount", "", displayAmount, currencyCode, isReadOnly, false, false, "onChange='setNewAmount();'", "", "none") %> 				
	</div>

	<%}else{
				displayAmount = TPCurrencyUtility.getDisplayAmount(displayAmount, currencyCode, loginLocale);
				String tempDisplayAmt = currencyCode +"  "+ displayAmount;
				%>
				<%= widgetFactory.createAmountField( "amount", " ", tempDisplayAmt, "22", true, false, false, "onChange='setNewAmount();'", "", "") %>			
			<%}%>
	<%
	StringBuffer newGteeAmt= new StringBuffer();
	 if(!isProcessedByBank)
	  {
		 if(!badAmountFound) {
			 newGteeAmt.append(currencyCode);
			 newGteeAmt.append(" ");  
			 newGteeAmt.append(TPCurrencyUtility.getDisplayAmount(calculatedTotal.toString(),currencyCode, loginLocale));			 
		 }
	%>
	
	<%= widgetFactory.createTextField( "TransactionAmount2", "GuaranteePayment.NewGteePaymentAmount", newGteeAmt.toString(), "22", true, false, false, "", "", "") %> 				
	

	<%
	}
	%>
	      </div>

  <div class="columnRight">
          	 	<div class="formItem">  
                  <%= widgetFactory.createCheckboxField("CloseAndDeActivate", "GuaranteePayment.CloseAndDeAct",  TradePortalConstants.INDICATOR_YES.equals(terms.getAttribute("close_and_deactivate")), isReadOnly,false,"","","none") %><br/>
                </div>
	
  </div>
<div style="clear: both;"></div>
</div>
<%
						 if (TradePortalConstants.INDICATOR_YES.equals(transaction.getAttribute("converted_transaction_ind"))){ 

%>
	<%= widgetFactory.createSectionHeader("2", "GuaranteePayment.ConversionDetails") %>
		<tr><td>						

      <%= widgetFactory.createSubsectionHeader("GuaranteeIssue.ChargesIncurred") %>
</td></tr>
	<tr><td>
	&nbsp;&nbsp;&nbsp;<%= widgetFactory.createNote("GuaranteeIssue.ChargeDetails") %><br>
	</td></tr>
<input type=hidden value=<%=chrgIncRowCount %> name="numberOfMultipleObjects"   id="noOfChrgIncRows"> 
<table><tr><td>&nbsp;&nbsp;</td><td width=""><table id="chrgIncTable" class="formDocumentsTable">
<thead>
	<tr>
		<th class="genericCol"><%=resMgr.getText("GuaranteeIssue.ChargeType",TradePortalConstants.TEXT_BUNDLE)%></th>
		<th class="genericCol"><%=resMgr.getText("GuaranteeIssue.ChrgIncCurrency",TradePortalConstants.TEXT_BUNDLE)%></th>
		<th class="genericCol"><%=resMgr.getText("GuaranteeIssue.ChrgIncAmount",TradePortalConstants.TEXT_BUNDLE)%></th>
		<th class="genericCol">&nbsp;</th>
	</tr>
</thead>

<tbody>
					 <%--passes in pmtTermsList, isReadOnly, isFromExpress, loginLocale,
			          plus new pmtTermsIndex below--%>
					<%
					  int chrgIncIndex = 0;
					%>
			      	<%@ include file="TransactionChrgIncRows.frag" %> 
	
</tbody>
</table>
</td></tr></table>
<br>
<div>
  <% if (!(isReadOnly)) 
		{     %>	 
				&nbsp;&nbsp;&nbsp;&nbsp;<button data-dojo-type="dijit.form.Button" type="button" id="add3moreChargesIncurred">
				<%= resMgr.getText("common.Add3MoreChargesIncurred", TradePortalConstants.TEXT_BUNDLE) %>
				<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
					
				</script>
			</button>	
	<% 
		}else{  %>
			&nbsp;
		<% 
		} %>  		
  	</div>

  
  <br>

	</div>	
	<%}%>