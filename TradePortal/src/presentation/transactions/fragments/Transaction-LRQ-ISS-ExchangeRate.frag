<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
*******************************************************************************
                 Loan Request Page - Exchange Rate section

  Description:
    Contains HTML to create the Loan Request Payment Instructions section.  

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transaction-LRQ-ISS-ExchangeRate.frag" %>
*******************************************************************************
--%>
<%
  String regExpRange1 = "";
  regExpRange1 = "[0-9]{0,5}([.][0-9]{0,8})?";
  regExpRange1 = "regExp:'" + regExpRange1 + "'"; 
 %>
<div class="columnLeft">
	<%=widgetFactory.createSubsectionHeader(
					"LoanRequest.LoanProceeds", (isReadOnly || isFromExpress),
					false, false, "")%>
	<div class="formItem">
	<%=widgetFactory.createStackedLabel("",
					"LoanRequest.ExchangeRateDetails")%>
	</div>
	<%
		if (proceedsBankToBook.equals(TradePortalConstants.INDICATOR_YES)) {
			defaultCheckBoxValue = true;
		} else {
			defaultCheckBoxValue = false;
		}
	%>
	<div class="formItemButtoninline">
	<%=widgetFactory.createCheckboxField("ProceedsBankToBook",
					"LoanRequest.BankToBook", defaultCheckBoxValue, isReadOnly,
					false, "", "", "none")%>
					</div>
	<%
		if (proceedsUseFEC.equals(TradePortalConstants.INDICATOR_YES)) {
			defaultCheckBoxValue = true;
		} else {
			defaultCheckBoxValue = false;
		}
	%>
	<div class="formItemButtoninline">
	<%=widgetFactory.createCheckboxField("ProceedsUseFEC",
					"LoanRequest.FEC", defaultCheckBoxValue, isReadOnly, false,
					"", "", "none")%></div>
					
	<div>
			<%=widgetFactory.createTextField(
					"ProceedsCoveredByFECNumber", "LoanRequest.FECCovered",
					terms.getAttribute("covered_by_fec_number"), "14",
					isReadOnly, false, false,
					"onChange='pickLoanProceedsFECCheckbox();'class='char15'", "", "inline")%>
		<%--<%=widgetFactory.createTextField("SettlementFECRate",
					"LoanRequest.FECRate", displaySettlementFECRate, "13",
					isReadOnly, false, false,
					"onChange='pickLoanProceedsFECCheckbox();'class='char10'", "", "inline")%>--%>
					 
			<%--SureshL Rel9.0  IR-23940 03/25/2014  [Start]--%>	 
		<%= widgetFactory.createTextField( "SettlementFECRate", "LoanRequest.FECRate", displaySettlementFECRate.replace(",",""), "14", isReadOnly, false, false, "onChange='pickLoanProceedsFECCheckbox();' class='char10'", regExpRange1, "inline" ) %>
		     <%--  SureshL  Rel9.0 IR-23940 03/25/2014  [End]--%>
		<div style="clear: both;"></div>
	</div>
	<div>		
		<%= widgetFactory.createTextField( "SettlementFECAmount", "LoanRequest.FECAmount", displaySettlementFECAmount, "22", isReadOnly, false, false, "onChange='pickLoanProceedsFECCheckbox();' class='char15'", "", "inline") %>			
		<%
			if (isReadOnly) {
		%>
		<%=widgetFactory.createDateField("FECMaturityDate",
						"LoanRequest.FECMaturity",
						 StringFunction.xssHtmlToChars(terms.getAttribute("fec_maturity_date")), isReadOnly,
						false, false, "class='char10'", dateWidgetOptions, "inline")%>
		<%
			} else {
		%>
		<%=widgetFactory.createDateField("FECMaturityDate",
						"LoanRequest.FECMaturity", StringFunction.xssHtmlToChars(terms
										.getAttribute("fec_maturity_date")),
						isReadOnly, false, false,
						"onChange='pickLoanProceedsFECCheckbox();' class='char10'", dateWidgetOptions,
						"inline")%>
		<%
			}
		%>
		<div style="clear: both;"></div>
	</div>
	<%
		if (proceedsUseOther.equals(TradePortalConstants.INDICATOR_YES)) {
			defaultCheckBoxValue = true;
		} else {
			defaultCheckBoxValue = false;
		}
	%>
	<%=widgetFactory.createCheckboxField("ProceedsUseOther",
					"LoanRequest.OtherRadio", defaultCheckBoxValue, isReadOnly, false,
					"", "", "")%>
	
	<%=widgetFactory.createTextArea("UseOtherText", "",
					terms.getAttribute("use_other_text"), isReadOnly,
					!isTemplate, false,
					"style=\"min-height: 60px;\"", "", "")%>
	<%=widgetFactory.createHoverHelp("UseOtherText", "UseOtherHoverText")%>
</div>
<%-- END LC --%>
<div class="columnRight">
	<%=widgetFactory.createSubsectionHeader(
					"LoanRequest.LoanMaturity", (isReadOnly || isFromExpress),
					false, false, "")%>
	<div class="formItem">
		<%=widgetFactory.createStackedLabel("",
					"LoanRequest.ExchangeRateDetails")%>
	</div>
	<%
		if (maturityBankToBook.equals(TradePortalConstants.INDICATOR_YES)) {
			defaultCheckBoxValue = true;
		} else {
			defaultCheckBoxValue = false;
		}
	%>
	<div class="formItemButtoninline">
	<%=widgetFactory.createCheckboxField("MaturityBankToBook",
					"LoanRequest.BankToBook", defaultCheckBoxValue, isReadOnly,
					false, "", "", "none")%></div>
	<%
		if (maturityUseFEC.equals(TradePortalConstants.INDICATOR_YES)) {
			defaultCheckBoxValue = true;
		} else {
			defaultCheckBoxValue = false;
		}
	%><div class="formItemButtoninline">
	<%=widgetFactory.createCheckboxField("MaturityUseFEC",
					"LoanRequest.FEC", defaultCheckBoxValue, isReadOnly, false,
					"", "", "none")%></div>
	<div>
		<%=widgetFactory.createTextField(
					"MaturityCoveredByFECNumber", "LoanRequest.FECCovered",
					terms.getAttribute("maturity_covered_by_fec_number"), "14",
					isReadOnly, false, false,
					"onChange='pickMaturityFECCheckbox();' class='char15'", "", "inline")%>
		<%--<%=widgetFactory.createTextField("MaturityFECRate",
					"LoanRequest.FECRate", displayMaturityFECRate, "13",
					isReadOnly, false, false,
					"onChange='pickMaturityFECCheckbox();' class='char10'", "", "inline")%>--%>
		<%= widgetFactory.createTextField( "MaturityFECRate", "LoanRequest.FECRate", displayMaturityFECRate.replace(",",""), "14", isReadOnly, false, false, "onChange='pickMaturityFECCheckbox();' class='char10'", regExpRange1, "inline" ) %>
		<div style="clear: both;"></div>
	</div>
	<div>		
		<%= widgetFactory.createTextField( "LoanFECAmount", "LoanRequest.FECAmount", displayLoanFECAmount, "22", isReadOnly, false, false, "onChange='pickMaturityFECCheckbox();' class='char15'", "", "inline") %>			
		<%
			if (isReadOnly) {
		%>
		<%=widgetFactory.createDateField(
						"MaturityFECMaturityDate", "LoanRequest.FECMaturity",
						StringFunction.xssHtmlToChars(terms.getAttribute("maturity_fec_maturity_date")),
						isReadOnly, false, false, "class='char10'", dateWidgetOptions, "inline")%>
		<%
			} else {
		%>
		<%=widgetFactory.createDateField(
						"MaturityFECMaturityDate", "LoanRequest.FECMaturity",
						StringFunction.xssHtmlToChars(terms
								.getAttribute("maturity_fec_maturity_date")),
						isReadOnly, false, false,
						"onChange='pickMaturityFECCheckbox();'class='char10'",dateWidgetOptions, "inline")%>
		<%
			}
		%>
		<div style="clear: both;"></div>
	</div>
	<%
		if (maturityUseOther.equals(TradePortalConstants.INDICATOR_YES)) {
			defaultCheckBoxValue = true;
		} else {
			defaultCheckBoxValue = false;
		}
	%>
	<%=widgetFactory.createCheckboxField("MaturityUseOther",
					"LoanRequest.OtherRadio", defaultCheckBoxValue, isReadOnly, false,
					"", "", "")%>				
	<%=widgetFactory.createTextArea("MaturityUseOtherText", "",
					terms.getAttribute("maturity_use_other_text"), isReadOnly,
					!isTemplate, false,
					"style=\"min-height: 60px;\"", "", "")%>
					<%=widgetFactory.createHoverHelp("MaturityUseOtherText", "UseOtherHoverText")%>
</div>


<script LANGUAGE="JavaScript">
	function clearBeneficiaryBank() {
		document.TransactionLRQ.BbkName.value = "";
		document.TransactionLRQ.BbkAddressLine1.value = "";
		document.TransactionLRQ.BbkAddressLine2.value = "";
		document.TransactionLRQ.BbkCity.value = "";
		document.TransactionLRQ.BbkStateProvince.value = "";
		document.TransactionLRQ.BbkCountry.value = "";
		document.TransactionLRQ.BbkPostalCode.value = "";
		document.TransactionLRQ.BbkPhoneNumber.value = "";
		document.TransactionLRQ.BbkOTLCustomerId.value = "";
	}

	function clearBeneficiary() {
		document.TransactionLRQ.BenName.value = "";
		document.TransactionLRQ.BenAddressLine1.value = "";
		document.TransactionLRQ.BenAddressLine2.value = "";
		document.TransactionLRQ.BenCity.value = "";
		document.TransactionLRQ.BenStateProvince.value = "";
		document.TransactionLRQ.BenCountry.value = "";
		document.TransactionLRQ.BenPostalCode.value = "";
		document.TransactionLRQ.BenPhoneNumber.value = "";
		document.TransactionLRQ.BenOTLCustomerId.value = "";
	}
<%-- CHECK LOAN PROCEEDS FEC CHECKBOX IF NUMBER, AMOUNT, RATE, OR DATE IS ENTERED --%>
	function pickLoanProceedsFECCheckbox() {

	}
<%-- PICK LOAN PROCEEDS FEC OTHER CHECKBOX IF APPLY LOAN PROCEEDS FEC OTHER TEXT BOX IS FILLED OUT --%>
	function pickLoanProceedsFECOtherCheckBox() {

	dijit.byId('ProceedsUseOther').set('isChecked','true');
	}
<%-- PICK DEBIT THE APPLICANTS ACCOUNT RADIO IF AN ACCOUNT NUMBER IS SELECTED --%>
	function pickDebitApplicantsAccountRadio() {

		var index = document.forms[0].LoanMaturityDebitAccountImp.selectedIndex;
		var acct = document.forms[0].LoanMaturityDebitAccountImp.options[index].value;

		if (acct > '') {
			document.forms[0].LoanMaturityDebitTypeImp[0].checked = true;
		}
	}
<%--
   // TLE - 11/03/06 IR#MHUG101470192 - Add Begin
	//// PICK DEBIT OTHER RADIO IF TEXT IS ENTERED INTO THE OTHER (ADVISE ...) TEXT BOX
	//function pickDebitOtherRadio() {
	//    
	//	if (document.forms[0].LoanMaturityDebitOtherText.value > '') {
	//		document.forms[0].LoanMaturityDebitType[1].checked = true;
	//	}
	//}
	--%>
	function pickDebitOtherRadio(value) {
		if (value == 1) {
			if (document.forms[0].LoanMaturityDebitOtherTextExp.value > '') {
				document.forms[0].LoanMaturityDebitTypeExp[1].checked = true;
			}
		} else if (value == 2) {
			if (document.forms[0].LoanMaturityDebitOtherTextImp.value > '') {
				document.forms[0].LoanMaturityDebitTypeImp[1].checked = true;
			}
		} else if (value == 3) {
			if (document.forms[0].LoanMaturityDebitOtherTextRec.value > '') {
				document.forms[0].LoanMaturityDebitTypeRec[1].checked = true;
			}
		} else {
			if (document.forms[0].LoanMaturityDebitOtherText.value > '') {
				document.forms[0].LoanMaturityDebitType[1].checked = true;
			}
		}
	}
<%-- TLE - 11/03/06 IR#MHUG101470192 - Add End --%>
	
<%-- CHECK LOAN MATURITY FEC CHECKBOX IF NUMBER, AMOUNT, RATE, OR DATE IS ENTERED --%>
	function pickMaturityFECCheckbox() {


	}
<%-- PICK MATURITY FEC OTHER CHECKBOX IF APPLY MATURITY FEC OTHER TEXT BOX IS FILLED OUT --%>
	function pickMaturityFECOtherCheckBox() {

		if (document.forms[0].MaturityUseOtherText.value > '') {
			document.forms[0].MaturityUseOther.checked = true;
		}
	}

</script>
