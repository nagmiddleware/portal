<%--
*******************************************************************************
                    Domestic Payment Invoice Detail Filter include file

  Description:
    This is a JSP meant to be included using the <%@ include file="x.jsp" %> 
  tag.  It creates the HTML for displaying the Basic Filter fields for the  
  Domestic Payment Invoice Detail Search.  As it is included with <%@ include %> rather than with
  the <jsp:include> directive, it is not a standalone servlet.  

  NOTE: Since this JSP is copied into the including page at compile time of
  the including page, changes to this page do not automatically force the
  including pages to be recompiled.  If you make changes to this page, you must
  delete the including page's servlet in order to see the changes.

*******************************************************************************
--%>
<%--SHILPAR CR-597  start - Beneficiary search functionality --%>
<input type="hidden" name="NewSearch" value="Y">
<% if (!(isReadOnly || isStatusVerifiedPendingFX || TradePortalConstants.INDICATOR_YES.equals(transaction.getAttribute("uploaded_ind")))) {%>
<div>

	<div style="float:right;">
		
		<button data-dojo-type="dijit.form.Button" id='addBeneficaryButton' type="button" class="SearchButton">
            <%=resMgr.getText("common.AddBeneficiaryText", TradePortalConstants.TEXT_BUNDLE)%>
         	<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
					clearAllBeneficiaryFormDetails();
					return false;
	      		</script>	
        </button>
		
	</div>
</div>
<div><br><br><br></div>

<% 
  }
   String label1 = resMgr.getText("DomesticPaymentRequest.All", TradePortalConstants.TEXT_BUNDLE);
   options = Dropdown.createSortedRefDataOptions("BENEFICIARY_PAYMENT_STATUS", paymentStatusSearch, loginLocale); //BSL IR# SBUL040440564 - change paymentStatus to paymentStatusSearch
   //BSL IR LMUL052437851 05/31/11 BEGIN
   // Remove option REJECTED_UNPAID because it is synonymous with REJECTED
   int rejUnpStartIdx = options.indexOf("<option value=\"REJECTED_UNPAID\">");
   if (rejUnpStartIdx > -1) {
 	  String optionEndTag = "</option>";
 	  int rejUnpEndIdx = options.indexOf(optionEndTag, rejUnpStartIdx) + optionEndTag.length();
 	  options = options.replace(options.substring(rejUnpStartIdx, rejUnpEndIdx), "");
   }
   
%>

<table>
<tr>
<td style="width:110px; text-align:right;"><label for="domesticpaymentstatus"><%=resMgr.getText("DomesticPaymentRequest.PaymentStatus", TradePortalConstants.TEXT_BUNDLE)%> : </label></td>
<td style="width:100px">
	<select name="PaymentStatusSearch" id="PaymentStatusSearch" data-dojo-type="dijit.form.FilteringSelect" 
		value=" " data-dojo-props="trim:true" style="width: 120px;">
			<%=options%>
	</select>
</td>
<td style="width:30px; "></td>

<td style="width:120px; text-align:right;"><label for="domesticpayeename"><%=resMgr.getText("DomesticPaymentRequest.PayeeName", TradePortalConstants.TEXT_BUNDLE)%> : </label></td>
<td >
	<input type="text" name="BenificiaryName" id="BenificiaryName"  
		data-dojo-type="dijit.form.TextBox" style="width: 150px;" data-dojo-props="trim:true">
</td>
<td style="width:30px"></td>

<td >
<button data-dojo-type="dijit.form.Button" type="button" class="SearchButton">
       	<%=resMgr.getText("common.SearchGreyText", TradePortalConstants.TEXT_BUNDLE)%>
    		<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
	                  filterPayments("ModifyPayee");return false;
	      	</script>
</button>
</td>
<td>
<%-- CR 857 Prateep Start--%>
<span id="pmtBeneGridEdit" class="gridHeaderEdit"></span>
<%=widgetFactory.createHoverHelp("pmtBeneGridEdit", "CustomizeListImageLinkHoverText") %>
<%-- CR 857 Prateep End--%>
</td>

</tr>

<tr>
<td style="text-align:right"><label for="domesticbenbankname"><%=resMgr.getText("DomesticPaymentRequest.PayeeBankName", TradePortalConstants.TEXT_BUNDLE)%> : </label></td>
<td >
	<input type="text" name="BenificiaryBankName" id="BenificiaryBankName"  
		data-dojo-type="dijit.form.TextBox" style="width: 140px;" data-dojo-props="trim:true">
</td>
<td style="width:30px"></td>

<td style="text-align:right"><label for="domesticbenamt"><%=resMgr.getText("DomesticPaymentRequest.Amount_List", TradePortalConstants.TEXT_BUNDLE)%> : </label></td>
<td >
	<input type="text" name="Amount" id="Amount"  
		data-dojo-type="dijit.form.TextBox" style="width: 100px;" data-dojo-props="trim:true">
</td>
<td ></td>

<td ></td>

</tr>

</table>


<div style="clear:both;"></div>
