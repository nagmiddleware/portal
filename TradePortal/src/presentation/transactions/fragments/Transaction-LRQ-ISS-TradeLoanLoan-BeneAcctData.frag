<%--
*******************************************************************************
  
  Description:
  A fragment for displaying terms party trd_accounts.
  Fairly generic, but should be copied for specific instrument behavior.

  The following variables are passed:

    termsParty - a TermsPartyWebBean
    isReadOnly
    isTemplate
    widgetFactory

*******************************************************************************
--%>
<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.html.*,java.math.*" %>

<%
  // Logic to handle account list.  If no payee has been searched, there is
  // only an enterable account number field.  Otherwise, we display the set of trd_accounts defined
  // for the party (when the party is selected, we get the set of trd_accounts for that party 
  // and save it on the TermsParty record -- if the party's trd_accounts change later, we do not update
  // the set on the TermsParty record.) as well as an enterable account number field.  The account 
  // number selected by the user will be one of the payee's predefined trd_accounts or the 
  // entered account number.
  
  		  String trd_benAcctList = StringFunction.xssHtmlToChars(termsParty.getAttribute("acct_choices"));
		  DocumentHandler trd_AcctData = new DocumentHandler(trd_benAcctList, true);
		  DocumentHandler trd_acct;
		  boolean trd_FoundAcctMatch = false; // indicates if ANY predefined trd_acct num matches ben. trd_acct
		  //     number from business object
		  boolean trd_foundMatch = false; // indicates if a given trd_acct num matches ben. trd_acct
		  //     number from business object
		  int trdNumAccts, tl;
		  Vector trd_accounts = trd_AcctData.getFragments("/DocRoot/ResultSetRecord");
		  trdNumAccts = trd_accounts.size();
		  
		  String trd_AcctNum = null;
		  String trd_AcctCcy = null;

		  Debug.debug("Account Choices for Ben Party: " + trd_benAcctList);
		  Debug.debug("Number of account in list is " + trdNumAccts);

		  // Set the trd_showBenAccts flag that determines if a list of trd_accounts with radio buttons
		  // appears.  They appear if we have a set of trd_accounts (i.e., it's been preloaded into
		  // a trd_benAcctList variable).  However, in readonly mode, we do not display the list.

		  // TLE - 11/20/06 - IR#ANUG110757179 - Add Begin
		  //if (trdNumAccts > 0) trd_showBenAccts = true;
		  boolean trd_showBenAccts = false;
		  if (trdNumAccts > 0)
				trd_showBenAccts = true;
		  // TLE - 11/20/06 - IR#ANUG110757179 - Add Begin
		  if (isReadOnly)
				trd_showBenAccts = false;

		  Debug.debug("trd_showBenAccts flag is " + trd_showBenAccts);

		  // Store the set of trd_accounts for the beneficiary in a hidden field.  This gets written to the
		  // TermsParty record so we always have the set of trd_accounts at the time the party was selected.
		  if(null != trd_benAcctList && !"".equals(trd_benAcctList)){
			%>
			<input type=hidden value='<%=trd_benAcctList%>' name=BenAcctChoicesTrd>
			<%
		  }
		  // Peform selective logic throughout that determines how this section is displayed -- as
		  // a set of radio buttons and trd_accounts, or just a single enterable account.
		  if (trd_showBenAccts) {
	%>
	<%--=widgetFactory.createTextField("","LoanRequest.BeneficiaryAccountNumber", "", "30", true, false, false, "width", "", "")--%>

	<%
		  }
	%>
	<%
		  if (trd_showBenAccts) {
				// This loop prints the set of trd_accounts previously determined when the party was
				// selected.
				for (tl = 0; tl < trdNumAccts; tl++) {
					  trd_acct = (DocumentHandler) trd_accounts.elementAt(tl);
	%>
	<%
		  // Display a radio button using the "account number/currency" from the
					  // account list (for whichever row we're on).  Select the correct
					  // radio button if we match on the current value of "acct_num" on the
					  // beneficiary party.
					  out.println("<td width=24 nowrap>");

					  // (Don't know why, but the first "get" from the doc must be without the /,
					  // succeeding ones must be with a /.  This is different from similar logic
					  // elsewhere.  Believed to be a symptom of flattening a DocHandler to a string
					  // and then creating another DocHandler from the string.)

					  trd_AcctNum = trd_acct.getAttribute("ACCOUNT_NUMBER"); // no "slash"
					  trd_AcctCcy = trd_acct.getAttribute("/CURRENCY"); // get with "slash"

					  trd_foundMatch = trd_AcctNum.equals(termsParty.getAttribute("acct_num"));

					  if (!trd_FoundAcctMatch) {
							trd_FoundAcctMatch = trd_FoundAcctMatch | trd_foundMatch;
					  }
	
		String radioLabel = "";
		  if (trd_AcctCcy.equals("")) {
			  radioLabel = trd_AcctNum;
	
		  } else {
			  radioLabel = trd_AcctNum + " (" + trd_AcctCcy + ")";
	
		  }
	%>
	<%=widgetFactory.createRadioButtonField("SelectedAccounttrd", 
			"BenAccountNum"+trd_AcctNum+"Trd", radioLabel, 
			trd_AcctNum+ TradePortalConstants.DELIMITER_CHAR+ trd_AcctCcy, 
			trd_foundMatch, isReadOnly,	"onClick='pickTheFollowingBeneficiaryRadioTrd();'","")%>
	<div style=\"clear:both;\"></div>
	<%
		  // End of for loop
				}
		  } // end if trd_showBenAccts
	%>
	<%
		  // This IF sets up trd_AcctNum and trd_AcctCcy fields with values if necessary and uses them
		  // for displaying the enterable account field.

		  if (trd_showBenAccts) {
				// Display radio button for selection of the enterable account.  If we haven't found
				// an account match yet, then we might match on the enterable value.  If they match,
				// turn on the radio button.
				if (trd_FoundAcctMatch) {
					  trd_AcctNum = "";
					  trd_AcctCcy = "";
					  trd_foundMatch = false;
				} else {
					  trd_AcctNum = termsParty.getAttribute("acct_num");
					  trd_AcctCcy = termsParty.getAttribute("acct_currency");
					  if (trd_AcctNum == null)
							trd_AcctNum = "";
							trd_foundMatch = trd_AcctNum.equals(termsParty.getAttribute("acct_num"));
				}
	%>
	<%=widgetFactory.createRadioButtonField("SelectedAccounttrd","BenAccountNumTrd","",
			TradePortalConstants.USER_ENTERED_ACCT, trd_foundMatch, isReadOnly,"onClick='pickTheFollowingBeneficiaryRadioTrd();'", "")%>
	<%
		  } else {
				// We're not displaying the account list, so populate the account field with the
				// account from the beneficiary party record.
				trd_AcctNum = termsParty.getAttribute("acct_num");
				trd_AcctCcy = termsParty.getAttribute("acct_currency");
				if (trd_AcctNum == null)
					  trd_AcctNum = benAcctNumTrd;
		  }
		  
		  // if (StringFunction.isBlank(trd_AcctNum) && !isMultiBenInvoicesAttached)
		  //	trd_AcctNum = benAcctNumTrd;
	%>
	<%-- KMEhta IR T36000022557 on 15/1/2014 changed required indicator to !isTemplate --%>
	<%=widgetFactory.createTextField("EnteredAccountTrd","",trd_AcctNum,"30",isReadOnly,!isTemplate,false,"","", "none")%>
