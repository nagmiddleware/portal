<%= widgetFactory.createSectionHeader("transportdoc0", "ImportDLCIssue.TransportDocs") %>

<table class="formDocumentsTable">
		<thead>
			<tr>
				<th>&nbsp;</th>
				<th><%= widgetFactory.createLabel( "ImportDLCIssue.DocType", "ImportDLCIssue.DocType", false,false, isExpressTemplate, "") %>	</th>
				<th><%= widgetFactory.createLabel( "ImportDLCIssue.Originals", "ImportDLCIssue.Originals", false,false, isExpressTemplate, "") %></th>
				<th><%= widgetFactory.createLabel( "ImportDLCIssue.Copies", "ImportDLCIssue.Copies", false,false, isExpressTemplate, "") %></th>
				<th><%= widgetFactory.createLabel( "ImportDLCIssue.Description", "ImportDLCIssue.Description", false,false, isExpressTemplate, "") %></th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td><%= widgetFactory.createCheckboxField( "TransDocInd0", "", shipmentTerms.getAttribute("trans_doc_included").equals(TradePortalConstants.INDICATOR_YES), isReadOnly || isFromExpress, isFromExpress, "", "", "none") %></td>
				<%
		          options = Dropdown.createSortedRefDataOptions(
		                      TradePortalConstants.TRANS_DOC_TYPE, 
		                      shipmentTerms.getAttribute("trans_doc_type"), 
		                      loginLocale);
				%>
				<td><%= widgetFactory.createSelectField( "TransDocType0", "", " ", options, isReadOnly || isFromExpress, false, false,  "onChange=\"pickTransDocCheckbox();\"", "", "none") %></td>
				<%
		          options = Dropdown.createSortedRefDataOptions(
		                      TradePortalConstants.TRANS_DOC_ORIGINALS_TYPE, 
		                      shipmentTerms.getAttribute("trans_doc_originals"), 
		                      loginLocale);
				%>
				<td><%= widgetFactory.createSelectField( "TransDocOrigs0", "", " ", options, isReadOnly || isFromExpress, false, false,  "onChange=\"pickTransDocCheckbox();\"", "", "none") %> </td>
				<td><%= widgetFactory.createNumberField( "TransDocCopies0", "", shipmentTerms.getAttribute("trans_doc_copies"), "3", isReadOnly || isFromExpress, false, false,  "onChange=\"pickTransDocCheckbox();\"", "", "none" ) %> </td>
				<td>
				<%
					if (!(isReadOnly || isFromExpress)) {
						options = ListBox.createOptionList(transportDocList, "PHRASE_OID", "NAME", "", userSession.getSecretKey());
						defaultText1 = resMgr.getText("transaction.SelectPhrase",
				                                   TradePortalConstants.TEXT_BUNDLE);
				%> 
			    	<%= widgetFactory.createSelectField( "TransDocItem0", "", defaultText1, options, isReadOnly || isFromExpress, false, false, "onChange=" + PhraseUtility.getPhraseChange(
                            "/Terms/ShipmentTermsList/trans_doc_text","TransDocText0", textAreaMaxlength2,"document.forms[0].TransDocText0"), "", "none") %>
			    	    
				<% }
			    %>
			    <br>
			    <br>
			    <%= widgetFactory.createTextArea( "TransDocText0", "", shipmentTerms.getAttribute("trans_doc_text"), isReadOnly || isFromExpress,false, false, "", "","none",textAreaMaxlength2 ) %>
				</td>
			</tr>
			
			<tr>
				<td><%=widgetFactory.createCheckboxField("TransAddlDocInd0", "",shipmentTerms.getAttribute("trans_addl_doc_included").equals(TradePortalConstants.INDICATOR_YES), isReadOnly || isFromExpress, isFromExpress, "", "", "none" ) %></td>
				<td><%=resMgr.getText("ImportDLCIssue.AddlTransportDocs",TradePortalConstants.TEXT_BUNDLE)%></td>
				<td colspan="3">
				<%
					if (isReadOnly || isFromExpress) {
				      out.println("&nbsp;");
				    } else {
				      options = ListBox.createOptionList(transportDocList, "PHRASE_OID", "NAME", "", userSession.getSecretKey());
				      defaultText1 = resMgr.getText("transaction.SelectPhrase",
				                                   TradePortalConstants.TEXT_BUNDLE);
				    }  
			    %>  
			    <%= widgetFactory.createSelectField( "TransAddlDocItem0", "", defaultText1, options, false, false, false, "onChange=" + PhraseUtility.getPhraseChange(
                        "/Terms/ShipmentTermsList/trans_addl_doc_text","TransAddlDocText",textAreaMaxlen,"document.forms[0].TransAddlDocText"), "", "none") %>
			    <%= widgetFactory.createSubLabel("ImportDLCIssue.SpecifyNumber")%>
			    <br>
			    <br>
			    <%= widgetFactory.createTextArea( "TransAddlDocText0", "", shipmentTerms.getAttribute("trans_addl_doc_text"), isReadOnly || isFromExpress,false, false, "", "","none", textAreaMaxlen ) %>
				</td>
			</tr>
		</tbody>	
		</table>
		
		<br>
		<br>
		<br>
		
		<div class="columnLeft">
		
		<%=widgetFactory.createSubsectionHeader("ImportDLCIssue.Consignment",isReadOnly, false, isExpressTemplate, "" ) %>
		<div>
			<%=widgetFactory.createRadioButtonField( "ConsignmentType", "TradePortalConstants.CONSIGN_ORDER0", "ImportDLCIssue.ConsignedToOrderOf", TradePortalConstants.CONSIGN_ORDER, consignType.equals(TradePortalConstants.CONSIGN_ORDER), isReadOnly || isFromExpress) %>
			<%
			codesToExclude2 = new Vector(); 
	        codesToExclude2.addElement(TradePortalConstants.CONSIGN_BUYER);
	        codesToExclude2.addElement(TradePortalConstants.CONSIGN_BUYERS_BANK);
	        options = Dropdown.createSortedRefDataOptions( TradePortalConstants.CONSIGNMENT_PARTY_TYPE,
	                                 shipmentTerms.getAttribute("trans_doc_consign_order_of_pty"),
	                                 loginLocale,codesToExclude2);  
	        %>
			<%= widgetFactory.createSelectField( "ConsignToOrder0", "", " ", options, isReadOnly || isFromExpress, false, false,  "onChange=\"pickConsignToOrderRadio();\"", "", "none" ) %> 
		<div style="clear:both;"></div>
		</div>
		
		<div>
			<%=widgetFactory.createRadioButtonField( "ConsignmentType", "TradePortalConstants.CONSIGN_PARTY0", "ImportDLCIssue.ConsignedTo", TradePortalConstants.CONSIGN_PARTY, consignType.equals(TradePortalConstants.CONSIGN_PARTY), isReadOnly || isFromExpress) %>
			<%
			options = Dropdown.createSortedRefDataOptions(TradePortalConstants.CONSIGNMENT_PARTY_TYPE,
                    shipmentTerms.getAttribute("trans_doc_consign_to_pty"),
                    loginLocale,codesToExclude2);
                    codesToExclude2=null;   
	        %>
			<%= widgetFactory.createSelectField( "ConsignToParty0", "", " ", options, isReadOnly || isFromExpress, false, false, "onChange=\"pickConsignToPartyRadio();\"", "", "none" ) %> 
		<div style="clear:both;"></div>
		</div>
		
		<br>
		<%
		 notifySearchHtml = widgetFactory.createPartySearchButton(TradePortalConstants.BUTTON_PARTYSEARCH,"",isReadOnly,TradePortalConstants.NOTIFY_PARTY,false);
		 %>
		<%=widgetFactory.createSubsectionHeader("ImportDLCIssue.NotifyParty",isReadOnly, false, isExpressTemplate, notifySearchHtml ) %>
		
		<%secureParms.put("not_terms_party_oid", termsPartyNot.getAttribute("terms_party_oid")); %>
		<input type=hidden name='NotTermsPartyType' value=<%=TradePortalConstants.NOTIFY_PARTY%>>
		<input type=hidden name="NotOTLCustomerId"  value="<%=termsPartyNot.getAttribute("OTL_customer_id")%>">
		<%
		    if (isFromExpress) {
		      // We are in a transaction created from express.  The notify party  
		      // fields are express fields.  This means they display as readonly 
		      // when the user is editing an instrument created from an express 
		      // template.  However, we still need to send the notify data to the 
		      // mediator.  Put the notify data in a bunch of hidden fields to 
		      // ensure this happens.
		%>
		      <input type=hidden name="NotName" 
		             value="<%=termsPartyNot.getAttribute("name")%>">
		      <input type=hidden name="NotAddressLine1"
		             value="<%=termsPartyNot.getAttribute("address_line_1")%>">
		      <input type=hidden name="NotAddressLine2"
		             value="<%=termsPartyNot.getAttribute("address_line_2")%>">
		      <input type=hidden name="NotAddressLine3"
		             value="<%=termsPartyNot.getAttribute("address_line_3")%>">
		<%
		  }
		%>
		
		<%= widgetFactory.createTextField( "NotName0", "", termsPartyNot.getAttribute("name"), "35", isReadOnly || isFromExpress, false, isExpressTemplate,  "onBlur='checkNotifyPartyName(\"" + 
                StringFunction.escapeQuotesforJS(termsPartyNot.getAttribute("name")) + "\")'", "",""  ) %>
		<%= widgetFactory.createTextField( "NotAddressLine10", "", termsPartyNot.getAttribute("address_line_1"), "35", isReadOnly || isFromExpress, false, isExpressTemplate,  "", "",""  ) %>
		<%= widgetFactory.createTextField( "NotAddressLine20", "", termsPartyNot.getAttribute("address_line_2"), "35", isReadOnly || isFromExpress, false, isExpressTemplate,  "", "",""  ) %>
		<%= widgetFactory.createTextField( "NotAddressLine30", "", termsPartyNot.getAttribute("address_line_3"), "35", isReadOnly || isFromExpress, false, isExpressTemplate,  "", "",""  ) %>
		</div>
		
		<div class="columnRight">
		
		<%=widgetFactory.createSubsectionHeader("ImportDLCIssue.MarkedFreight",isReadOnly, false, isExpressTemplate, "" ) %>
		
		<%=widgetFactory.createRadioButtonField( "MarkedFreightType", "TradePortalConstants.PREPAID0", "ImportDLCIssue.Prepaid", TradePortalConstants.PREPAID, consignType.equals(TradePortalConstants.PREPAID), isReadOnly || isFromExpress) %>
		<br>
		<%=widgetFactory.createRadioButtonField( "MarkedFreightType", "TradePortalConstants.COLLECT0", "ImportDLCIssue.Collect", TradePortalConstants.COLLECT, consignType.equals(TradePortalConstants.COLLECT), isReadOnly || isFromExpress) %>
		
		<br>
		
		<br>
		<%
		 consigneeSearchHtml = widgetFactory.createPartySearchButton(TradePortalConstants.BUTTON_PARTYSEARCH,"",isReadOnly,TradePortalConstants.OTHER_CONSIGNEE,false);
		 %>
		<%=widgetFactory.createSubsectionHeader("ImportDLCIssue.OtherConsignee",isReadOnly, false, isExpressTemplate, consigneeSearchHtml ) %>
		
		<%secureParms.put("oth_terms_party_oid", 
                termsPartyOth.getAttribute("terms_party_oid"));
		%>
		    <input type=hidden name='OthTermsPartyType' 
		           value=<%=TradePortalConstants.OTHER_CONSIGNEE%>>
		    <input type=hidden name="OthOTLCustomerId"
		           value="<%=termsPartyOth.getAttribute("OTL_customer_id")%>">
		<%
		    if (isFromExpress) {
		      // We are in a transaction created from express.  The other party  
		      // fields are express fields.  This means they display as readonly 
		      // when the user is editing an instrument created from an express 
		      // template.  However, we still need to send the otherdata to the 
		      // mediator.  Put the other data in a bunch of hidden fields to 
		      // ensure this happens.
		%>
		      <input type=hidden name="OthName" 
		             value="<%=termsPartyOth.getAttribute("name")%>">
		      <input type=hidden name="OthAddressLine1"
		             value="<%=termsPartyOth.getAttribute("address_line_1")%>">
		      <input type=hidden name="OthAddressLine2"
		             value="<%=termsPartyOth.getAttribute("address_line_2")%>">
		      <input type=hidden name="OthAddressLine3"
		             value="<%=termsPartyOth.getAttribute("address_line_3")%>">
		<%
		  }
		%>
		
		<%= widgetFactory.createTextField( "OthName0", "", termsPartyOth.getAttribute("name"), "35", isReadOnly || isFromExpress, false, isExpressTemplate,  "onBlur='checkOtherConsigneePartyName(\"" + 
                StringFunction.escapeQuotesforJS(termsPartyOth.getAttribute("name")) + "\")'", "",""  ) %>
		<%= widgetFactory.createTextField( "OthAddressLine10", "", termsPartyOth.getAttribute("address_line_1"), "35", isReadOnly || isFromExpress, false, isExpressTemplate,  "", "",""  ) %>
		<%= widgetFactory.createTextField( "OthAddressLine20", "", termsPartyOth.getAttribute("address_line_2"), "35", isReadOnly || isFromExpress, false, isExpressTemplate,  "", "",""  ) %>
		<%= widgetFactory.createTextField( "OthAddressLine30", "", termsPartyOth.getAttribute("address_line_3"), "35", isReadOnly || isFromExpress, false, isExpressTemplate,  "", "",""  ) %>
		</div>
		
		<div style = "clear:both;"></div>
		
		<%=widgetFactory.createWideSubsectionHeader("ImportDLCIssue.Shipment") %>
		
		<div>
		
        <%= widgetFactory.createDateField( "LastestShipDate0", "ImportDLCIssue.LastestShipDate", StringFunction.xssHtmlToChars(shipmentTerms.getAttribute("latest_shipment_date")), isReadOnly, !isTemplate, isExpressTemplate,  "", dateWidgetOptions, "inline" ) %>
        <%=widgetFactory.createCheckboxField("TranshipAllowed0", "ImportDLCIssue.TranshipAllowed",shipmentTerms.getAttribute("transshipment_allowed").equals(TradePortalConstants.INDICATOR_YES), isReadOnly || isFromExpress, isFromExpress, "", "", "inline" ) %>
		<div style = "clear:both;"></div>
		</div>
		
		<div>
		<%
        options = Dropdown.createSortedRefDataOptions( TradePortalConstants.INCOTERM,
                               shipmentTerms.getAttribute("incoterm"),
                               loginLocale);
		%>
		<%= widgetFactory.createSelectField( "Incoterm0", "ImportDLCIssue.ShippingTerm", " ", options, isReadOnly || isFromExpress, false, isExpressTemplate,  "", "", "inline" ) %>
		<%= widgetFactory.createTextField( "IncotermLocation0", "ImportDLCIssue.ShipTermLocation", shipmentTerms.getAttribute("incoterm_location"), "30", isReadOnly || isFromExpress, false, isExpressTemplate,  "", "","inline"  ) %>
		<div style="clear:both;"></div>
		</div>
        
		<div class="columnLeft">
		<span class="formItem">
		<%=widgetFactory.createStackedLabel("From", "ImportDLCIssue.From") %>
		</span>
		<%if(!(isReadOnly || isFromExpress)){ %>
			<%= widgetFactory.createTextField( "ShipFromPort0", "ApprovalToPayIssue.FromPort",shipmentTerms.getAttribute("shipment_from"), "65", isReadOnly || isFromExpress, false, false,  "class='char35'", "",""  ) %>
		<%}else{ %>
			<%= widgetFactory.createTextArea( "ShipFromPort0", "ApprovalToPayIssue.FromPort",shipmentTerms.getAttribute("shipment_from"), isReadOnly,false,false,"rows='2' cols='44'","","") %>
		<%} %>
		<%if(!(isReadOnly || isFromExpress)){ %>
			<%= widgetFactory.createTextField( "ShipFromLoad0", "ApprovalToPayIssue.FromLoad",shipmentTerms.getAttribute("shipment_from_loading"), "65", isReadOnly || isFromExpress, false, false,  "class='char35'", "",""  ) %>
		<%}else{ %>
			<%= widgetFactory.createTextArea( "ShipFromLoad0", "ApprovalToPayIssue.FromLoad",shipmentTerms.getAttribute("shipment_from_loading"), isReadOnly,false,false,"rows='2' cols='44'","","") %>
		<%} %>	
		</div>
		
		<div class="columnRight">
		<span class="formItem">
			<%=widgetFactory.createStackedLabel("To", "ImportDLCIssue.To") %>
		</span>			
		<%if(!(isReadOnly || isFromExpress)){ %>
			<%= widgetFactory.createTextField( "ShipToDischarge0", "ImportDLCIssue.ToDischarge",shipmentTerms.getAttribute("shipment_to_discharge"), "65", isReadOnly || isFromExpress, false, false,  "class='char35'", "",""  ) %>
		<%}else{ %>
			<%= widgetFactory.createTextArea( "ShipToDischarge0", "ImportDLCIssue.ToDischarge",shipmentTerms.getAttribute("shipment_to_discharge"), isReadOnly,false,false,"rows='2' cols='44'","","") %>
		<%} %>
		<%if(!(isReadOnly || isFromExpress)){ %>
			<%= widgetFactory.createTextField( "ShipToPort0", "ImportDLCIssue.ToPort",shipmentTerms.getAttribute("shipment_to"), "65", isReadOnly || isFromExpress, false, false,  "class='char35'", "",""  ) %>
		<%}else{ %>
			<%= widgetFactory.createTextArea( "ShipToPort0", "ImportDLCIssue.ToPort",shipmentTerms.getAttribute("shipment_to"), isReadOnly,false,false,"rows='2' cols='44'","","") %>
		<%} %>
		</div>
		
		<div style="clear:both;"></div>
		
		<%=widgetFactory.createWideSubsectionHeader("ImportDLCIssue.GoodsDesc", false,!isTemplate, false, "") %>
		
		<%
        if (isReadOnly ) {
        	out.println("&nbsp;");
        } else {
        	options = ListBox.createOptionList(goodsDocList, "PHRASE_OID", "NAME", "", userSession.getSecretKey());
        	defaultText1 = resMgr.getText("transaction.AddAPhrase", TradePortalConstants.TEXT_BUNDLE);
		%>

		<%= widgetFactory.createSelectField( "GoodsPhraseItem0", "", defaultText1, options, isReadOnly, false, false, "onChange=" + PhraseUtility.getPhraseChange(
                "/Terms/ShipmentTermsList/goods_description",
                "GoodsDescText",
                textAreaMaxlen,"document.forms[0].GoodsDescText"), "", "") %>
		
		<%
        }
		%>
		
		<%-- <%= widgetFactory.createTextArea( "GoodsDescText0", " ", shipmentTerms.getAttribute("goods_description").toString(), isReadOnly) %> --%>
		<%= widgetFactory.createTextArea( "GoodsDescText0", "", shipmentTerms.getAttribute("goods_description").toString(), isReadOnly,false, false, "", "","none", "" ) %>
