<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
*******************************************************************************
                 Domestic Payment Request Page - Debit/Credit Details section

  Description:
    Contains HTML to create Domestic Payment Request Debit/Credit Details section.  

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transaction-FTDP-ISS-CreditDebit.frag" %>
*******************************************************************************
--%>

<%
	// Define Variables
	DocumentHandler enteredDomPmtDoc = null;
	QueryListView queryListView = null;
	StringBuffer updateText = null;
	StringBuffer sqlQuery = new StringBuffer();
	boolean errorFlag = false;
	String corporateOrgOid = null;
	Vector enteredDomPmtList = null;
	String classAttribute = null;
	String defaultValue = null;
	String userLocale = null;
	int numberOfDomPmts = 0;
	String dropdownOptions = null;
	String fundingCurrency = "";
	String displayPaymentAmount = "0.00";
	String displayFundingAmount = "0.00";

	corporateOrgOid = userSession.getOwnerOrgOid();

	// IAZ: Retrive all domestic payments to be displayed as a table at the bottom 
	// of the screen

	boolean sqlPayeeError = false;
	try {
		queryListView = (QueryListView) EJBObjectFactory.createClientEJB(formMgr.getServerLocation(),"QueryListView");

		sqlQuery.append("select dp.domestic_payment_oid, dp.payee_account_number as payee_account, ");
		sqlQuery.append("dp.payee_name as payee_name, dp.amount as payee_amount, ");
		sqlQuery.append("dp.amount_currency_code as currency_code, ");
		sqlQuery.append("dp.payee_bank_code, dp.payee_bank_name, dp.payee_description ");
		sqlQuery.append(", dp.payment_method_type ");
		sqlQuery.append(", dp.payee_address_line_1, dp.payee_address_line_2, dp.payee_address_line_3, dp.payee_address_line_4, dp.country ");
		sqlQuery.append(", dp.value_date, customer_reference ");											//IAZ CR-509 01/18/10, 02/03/10 Add
		sqlQuery.append("from domestic_payment dp ");
		sqlQuery.append("where dp.p_transaction_oid = ?");
		sqlQuery.append(" order by payee_account");

		queryListView.setSQL(sqlQuery.toString(), new Object[]{transaction.getAttribute("transaction_oid")});
		queryListView.getRecords();
		numberOfDomPmts = queryListView.getRecordCount();
		DocumentHandler enteredDomPmtsDoc = queryListView.getXmlResultSet();
		enteredDomPmtList = enteredDomPmtsDoc.getFragments("/ResultSetRecord");

	} catch (Exception e) {
		sqlPayeeError = true;
		e.printStackTrace();
	}
	finally {
		try {
			if (queryListView != null) {
				queryListView.remove();
			}
		} catch (Exception e) {
			System.out.println("Error removing QueryListView in -CreditDebitDetails.frag!");

		}
	}
	//End Retrive all domestic payments list 

	//make sure to load a Payee to a Credit section upon successfull Verify so screen 
	//does not appear empty
	if ((numberOfDomPmts >= 1) && (isReadOnly)) {
		if (InstrumentServices.isBlank(currentDPOid)) {
			enteredDomPmtDoc = (DocumentHandler) enteredDomPmtList
					.elementAt(0);
			currentDPOid = enteredDomPmtDoc
					.getAttribute("/DOMESTIC_PAYMENT_OID");
			currentDomesticPayment.setAttribute("domestic_payment_oid",
					currentDPOid);
			currentDomesticPayment.getDataFromAppServer();
		}
	}

	// IAZ: We retrive this amount to handle "corner cases"
	//                  such as no domestic payments are entered
	//                  However, this value will be recalculated to
	//                  be equal the sum of all dom pmt amounts

	String fundingAmount = terms.getAttribute("funding_amount");
	if ((fundingAmount == null) || (fundingAmount.equals(""))) {
		fundingAmount = "0";
	}

	// Always send the terms party oids
	secureParms.put("PayerParty_terms_party_oid", payerParty
			.getAttribute("terms_party_oid"));
	secureParms.put("ApplicantParty_terms_party_oid", applicantParty
			.getAttribute("terms_party_oid"));

	//IAZ: get payee accout number from current payee, if loaded     
	payeeAcctNum = currentDomesticPayment
			.getAttribute("payee_account_number");
	if (payeeAcctNum == null) {
		payeeAcctNum = "";
	}

	String bankChargesType = currentDomesticPayment
			.getAttribute("bank_charges_type");

	//Calculate amounts and determine accounts/currencies to be used later

	// Using the acct_choices xml from the payer party,build the dropdown and 
	// select the one matching debit_account_oid
	
	acctOid = StringFunction.xssHtmlToChars(terms.getAttribute("credit_account_oid"));

	Vector availabilityChecks = new Vector(2);
	availabilityChecks.add("!deactivate_indicator");
	availabilityChecks.add("available_for_direct_debit");

	String userCheck = null;
	if (isBankUser) {
		userCheck = null;
	} else {
		userCheck = userSession.getUserOid();
	}
	payerParty.loadAcctChoices(corporateOrgOid, formMgr
					.getServerLocation(), resMgr, availabilityChecks,
					userCheck);
	payerAcctList = StringFunction.xssHtmlToChars(payerParty.getAttribute("acct_choices"));

	DocumentHandler acctOptions = new DocumentHandler(payerAcctList, true);

	// IAZ: We will get Funding Currency from the Details of the Funding Account 
	// selected from the Available Account's drop-down list.
	// Selected Payee's payment amount and total amount comes from the Domestic Payment data and it
	// will be formated using transaction/payment currency which is the same 
	// for all payees (but can be different form the funding currency) 

	if (acctOid != null && !acctOid.equals("")) {
		Vector vector = acctOptions
				.getFragments("/DocRoot/ResultSetRecord");
		int numItems = vector.size();

		for (int i = 0; i < numItems; i++) {
			DocumentHandler document = (DocumentHandler) vector
					.elementAt(i);
			String oid = document.getAttribute("/ACCOUNT_OID");
			if (acctOid.equals(oid)) {
				fundingCurrency = document.getAttribute("/CURRENCY");
				break;
			}
		}
	}

	String transactionCCY = terms.getAttribute("amount_currency_code");
	String transactionCCYDisplay = transactionCCY;
	String transactionAmount = terms.getAttribute("amount");
	String paymentAmount = currentDomesticPayment
			.getAttribute("amount");
	String paymentCurrency = currentDomesticPayment
			.getAttribute("amount_currency_code");

	displayPaymentAmount = TPCurrencyUtility.getDisplayAmount(
			paymentAmount, transactionCCY, loginLocale);
	if (getDataFromDoc
			&& maxError != null
			&& maxError.compareTo(String
					.valueOf(ErrorManager.WARNING_ERROR_SEVERITY)) > 0)
		displayPaymentAmount = paymentAmount;
	//out.println("transactionAmount"+transactionAmount);
	//out.println("transactionCCY"+transactionCCY);
	String displayTransactionAmount = TPCurrencyUtility
			.getDisplayAmount(transactionAmount, transactionCCY,
					loginLocale);
	//out.println("displayTransactionAmount"+displayTransactionAmount);
	displayFundingAmount = TPCurrencyUtility.getDisplayAmount(
			fundingAmount, fundingCurrency, loginLocale);
	//out.println("displayFundingAmount"+displayFundingAmount);
%>

<%-- Header line --%>

<%-- IAZ: we need to preserve at least one attribute of the payee besides oid on the page
     otherise mediator will remove payer reference from the terms record 
            we also want to preserve at leats one attribute of payer which was originally 
     created form template --%>

<input type=hidden name=ApplicantName value='<%=applicantParty.getAttribute("name")%>'>
<input type=hidden name=PayerPartyType value=<%=TradePortalConstants.PAYER %>>

<%
	String firstPartyOid = terms.getAttribute("c_FirstTermsParty");

	//	In case of a single Payer, store this Payers' name into the Terms object
	//	If no payers, no Payer data should be stored with Terms (case of an error when first payee
	//  is being entered is also covered)
	//	If there are more then 1 Payer, store constant MULTIPLE_PAYERS_ENTERED to Terms
	
	//	Note: by the time of Authorize, this value will ALWAYS be set to either a single Payer name
	//	or MULTIPLE_PAYERS_ENTERED constant value because DDI transaction cannot be verified
	//	if there is less then 1 Payer.  This will also ensure a creation of FirstTermsParty (Payer)
	//	record in the database

	if (numberOfDomPmts > 1) {
%>
<input type="hidden" name="testDebugForstPartyOid"
	value="<%=firstPartyOid%>">
<input type="hidden" name="PayerPartyContactName"
	value='<%=resMgr.getText("Common.MultPayersEntered", TradePortalConstants.TEXT_BUNDLE)%>'>
<input type="hidden" name="PayeeTermsPartyName"
	value='<%=resMgr.getText("Common.MultPayersEntered", TradePortalConstants.TEXT_BUNDLE)%>'>	
<input type="hidden" name="FirstTermsPartyAddress1" value="">
<input type="hidden" name="FirstTermsPartyAddress2" value="">
<input type="hidden" name="FirstTermsPartyAddress3" value="">
<input type="hidden" name="FirstTermsPartyAddress4" value="">
<input type="hidden" name="FirstTermsPartyCountry" value="">
<% terms.setAttribute("reference_number", resMgr.getText("Common.MultReferences",
														TradePortalConstants.TEXT_BUNDLE)); %>

<%
	}
	
	if (numberOfDomPmts == 1) {
		String payeeNameToUse, payeeAdd1, payeeAdd2, payeeAdd3, payeeAdd4, payeeCountry, payeeRefNum;
		if (getDataFromDoc) {
			payeeNameToUse = currentDomesticPayment
					.getAttribute("payee_name");
			payeeAdd1 = currentDomesticPayment
					.getAttribute("payee_address_line_1");
			payeeAdd2 = currentDomesticPayment
					.getAttribute("payee_address_line_2");
			payeeAdd3 = currentDomesticPayment
					.getAttribute("payee_address_line_3");
			payeeAdd4 = currentDomesticPayment
					.getAttribute("payee_address_line_4");
			payeeCountry = currentDomesticPayment
			        .getAttribute("country");
			payeeRefNum = currentDomesticPayment.getAttribute("customer_reference");        
		} else {
			payeeNameToUse = ((DocumentHandler) enteredDomPmtList
					.elementAt(0)).getAttribute("/PAYEE_NAME");
			payeeAdd1 = ((DocumentHandler) enteredDomPmtList
					.elementAt(0))
					.getAttribute("/PAYEE_ADDRESS_LINE_1");
			payeeAdd2 = ((DocumentHandler) enteredDomPmtList
					.elementAt(0))
					.getAttribute("/PAYEE_ADDRESS_LINE_2");
			payeeAdd3 = ((DocumentHandler) enteredDomPmtList
					.elementAt(0))
					.getAttribute("/PAYEE_ADDRESS_LINE_3");
			payeeAdd4 = ((DocumentHandler) enteredDomPmtList
					.elementAt(0))
					.getAttribute("/PAYEE_ADDRESS_LINE_4");
			payeeCountry = ((DocumentHandler) enteredDomPmtList
					.elementAt(0))
					.getAttribute("/COUNTRY");
			payeeRefNum = ((DocumentHandler) enteredDomPmtList
					.elementAt(0))
					.getAttribute("/CUSTOMER_REFERENCE");
		}

		payeeNameToUse = StringFunction.xssCharsToHtml(payeeNameToUse);
		if ((InstrumentServices.isNotBlank(payeeNameToUse))&&(payeeNameToUse.length() > 35))
            payeeNameToUse = payeeNameToUse.substring(0,35);		
		
		payeeAdd1 = StringFunction.xssCharsToHtml(payeeAdd1);
		payeeAdd2 = StringFunction.xssCharsToHtml(payeeAdd2);
		payeeAdd3 = StringFunction.xssCharsToHtml(payeeAdd3);
		payeeAdd4 = StringFunction.xssCharsToHtml(payeeAdd4);
		payeeCountry = StringFunction.xssCharsToHtml(payeeCountry);
		terms.setAttribute("reference_number", StringFunction.xssCharsToHtml(payeeRefNum));        
		
%>
<input type="hidden" name="testDebugForstPartyOid"
	value="<%=firstPartyOid%>">
<input type="hidden" name="PayerPartyContactName" value="<%=payeeNameToUse%>">
<input type="hidden" name="PayeeTermsPartyName"
	value="<%=payeeNameToUse%>">
<input type="hidden" name="FirstTermsPartyAddress1" value="<%=payeeAdd1%>">
<input type="hidden" name="FirstTermsPartyAddress2" value="<%=payeeAdd2%>">
<input type="hidden" name="FirstTermsPartyAddress3" value="<%=payeeAdd3%>">
<input type="hidden" name="FirstTermsPartyAddress4" value="<%=payeeAdd4%>">
<input type="hidden" name="FirstTermsPartyCountry" value="<%=payeeCountry%>">
<%
	}
	
	if (numberOfDomPmts == 0) {
		String payeeNameToUse, payeeAdd1, payeeAdd2, payeeAdd3, payeeAdd4, payeeCountry;
		if (getDataFromDoc) {
			payeeNameToUse = currentDomesticPayment
					.getAttribute("payee_name");
			payeeAdd1 = currentDomesticPayment
					.getAttribute("payee_address_line_1");
			payeeAdd2 = currentDomesticPayment
					.getAttribute("payee_address_line_2");
			payeeAdd3 = currentDomesticPayment
					.getAttribute("payee_address_line_3");
			payeeAdd4 = currentDomesticPayment
			     .getAttribute("payee_address_line_4");
			payeeCountry = currentDomesticPayment
 		         .getAttribute("country");
			payeeNameToUse = StringFunction
					.xssCharsToHtml(payeeNameToUse);
			payeeAdd1 = StringFunction.xssCharsToHtml(payeeAdd1);
			payeeAdd2 = StringFunction.xssCharsToHtml(payeeAdd2);
			payeeAdd3 = StringFunction.xssCharsToHtml(payeeAdd3);
			payeeAdd4 = StringFunction.xssCharsToHtml(payeeAdd4);
			payeeCountry = StringFunction.xssCharsToHtml(payeeCountry);
			terms.setAttribute("reference_number", StringFunction.xssCharsToHtml(currentDomesticPayment.getAttribute("customer_reference"))); 
		} else {
			payeeNameToUse = "";
			payeeAdd1 = "";
			payeeAdd2 = "";
			payeeAdd3 = "";
			payeeAdd4 = "";
			payeeCountry = "";
			terms.setAttribute("reference_number", "");
		}
        if ((InstrumentServices.isNotBlank(payeeNameToUse))&&(payeeNameToUse.length() > 35))
           	payeeNameToUse = payeeNameToUse.substring(0,35); 		
%>
<input type="hidden" name="testDebugForstPartyOid"
	value="<%=firstPartyOid%>">
<input type="hidden" name="PayerPartyContactName" value="<%=payeeNameToUse%>">
<input type="hidden" name="PayeeTermsPartyName"
	value="<%=payeeNameToUse%>">
<input type="hidden" name="FirstTermsPartyAddress1" value="<%=payeeAdd1%>">
<input type="hidden" name="FirstTermsPartyAddress2" value="<%=payeeAdd2%>">
<input type="hidden" name="FirstTermsPartyAddress3" value="<%=payeeAdd3%>">
<input type="hidden" name="FirstTermsPartyAddress4" value="<%=payeeAdd4%>">
<input type="hidden" name="FirstTermsPartyCountry" value="<%=payeeCountry%>">
<%
	}
%>
<div class = "columnLeft">
<%= widgetFactory.createSubsectionHeader( "DirectDebitInstruction.CreditDetailsText") %>
<%
			if (isReadOnly) {
				String dateToFormat;
				if ((transaction.getAttribute("payment_date").indexOf("/") != -1)
						&& (InstrumentServices.isNotBlank(formatableDate))) {
					dateToFormat = formatableDate;
					Debug.debug("IAZDEbug::formatedDate in readOnly:: "
							+ dateToFormat);
				} else {
					dateToFormat = transaction.getAttribute("payment_date");

				}
			
				out.println(widgetFactory.createDateField( "payment_date", "DirectDebitInstruction.ExecutionDate", 
			transaction.getAttribute("payment_date"),isReadOnly || isFromExpress, !isTemplate, false, "", dateWidgetOptions, ""));
			} else {
			
			out.println(widgetFactory.createDateField( "payment_date", "DirectDebitInstruction.ExecutionDate", 
			transaction.getAttribute("payment_date"),isReadOnly || isFromExpress, !isTemplate, false, "", dateWidgetOptions, ""));
		 
		
			}
%>
		<%

			defaultText = resMgr.getText("UserDetail.selectAccountNumberCCY",
					TradePortalConstants.TEXT_BUNDLE);

			options = Dropdown.createSortedAcctOptionsOid(acctOptions, acctOid,
					loginLocale);
			
		%>
		<%--	Naveen 13-August-2012 IR-T36000004481. Modified the width of the Dropdown using the style attribute	--%>
		<%= widgetFactory.createSelectField( "CreditAccount", "DirectDebitInstruction.CreditToAccount", 
		defaultText, options, isReadOnly || isFromExpress, !isTemplate, isExpressTemplate, "onchange=\"javascript:updatePayerAccCurrency();\" class='char30'","","" ) %>


<%= widgetFactory.createSubsectionHeader( "DirectDebitInstruction.DebitInformation") %>
<div>
<%-- IAZ CR-483B 08/13/09 Begin: Various sections on DP Transaction and Template screens are moved per CR-483 design --%>
<%-- Template DropDown and Payment Amount Summary Table --%>

		<%
			options = Dropdown.createSortedCurrencyCodeOptions(terms
					.getAttribute("amount_currency_code"), loginLocale);
			out.println(widgetFactory.createSelectField("TransactionCurrencyDD", "transaction.Currency", " ", options,
			    isReadOnly || isFromExpress, !isTemplate, isExpressTemplate, "onChange='updatePaymentCCY();' class='char3'","","inline"));
		%>				
		<%= widgetFactory.createTextField( "displayTransactionAmount", "TransferBetweenAccounts.Amount", displayTransactionAmount, "22", false, !isTemplate, isExpressTemplate, " disabled class='char15'", "", "inline") %> 				
		
		<input type="hidden" value="<%=fundingAmount%>" name="FundingAmount"
			size="20" maxlength="20" class="ListText"> 



	<%= widgetFactory.createTextField( "NoOfDebits", "DirectDebitInstruction.NoOfDebits",
				new Integer(numberOfDomPmts).toString(), "10",  true, isTemplate, isExpressTemplate, "","","inline")%>
			<%-- IAZ CM-451 01/29/09 Add: send the total number of current Payees to the server 
          	so we can set Terms First Party data correctly --%> 
          	<input
			type="hidden" name="numberOfPayees" value="<%=numberOfDomPmts%>">
		
<div style="clear:both"> </div>
</div>		<%-- close of Debit Information --%>

		 <%-- Vshah CR509 Begin --%>
		 <input type=hidden name=c_OrderingParty value="<%=terms.getAttribute("c_OrderingParty")%>">
         <input type=hidden name=OrderingPartyOid value="<%=orderingParty.getAttribute("payment_party_oid")%>">        
         <input type=hidden name=c_OrderingPartyBank value="<%=terms.getAttribute("c_OrderingPartyBank")%>">
         <input type=hidden name=OrderingPartyBankOid value="<%=orderingPartyBank.getAttribute("payment_party_oid")%>">
         <%-- Vshah CR509 End --%> 
		
<%
			 String identifierStr = "OrderingPartyName,OrderingPartyAddress1,OrderingPartyAddress2,OrderingPartyAddress3,OrderingPartyAddress4,OrderingPartyCountry";
		     String sectionName = "orderPar";	
			 String orderingPartySearchHtml = widgetFactory.createPartySearchButton(identifierStr,sectionName,
			 false,TradePortalConstants.ORDERING_PARTY,false);
			
			 %>
                <%= widgetFactory.createSubsectionHeader( "DirectDebitInstruction.OrderingParty",
                isReadOnly || isFromExpress, false, isExpressTemplate, orderingPartySearchHtml)%>
            <%--  <div class="subsectionShortHeader"><h3>Ordering Party</h3><a name="PartySearch" onClick="SearchParty('<%=identifierStr%>', '<%=sectionName%>');"><img class="searchButton" src="/portal/themes/default/images/search_icon.png"></a></div>---%>

<%--	Naveen 13-August-2012 IR-T36000004481. Modified the width of below five TextFields	--%>
<%= widgetFactory.createTextField( "OrderingPartyName", "DomesticPaymentRequest.OrderingPartyName", 
orderingParty.getAttribute("bank_name").toString(), "140", isReadOnly || isFromExpress, false, isExpressTemplate, "style=\"width:22em;\"", "","" ) %>

<%= widgetFactory.createTextField( "OrderingPartyAddress1", "DirectDebitInstruction.OrderPartyAddress", 
orderingParty.getAttribute("address_line_1").toString(), "30", isReadOnly || isFromExpress, false, isExpressTemplate, "", "","" ) %>
<%= widgetFactory.createTextField( "OrderingPartyAddress2", "", 
orderingParty.getAttribute("address_line_2").toString(), "30", isReadOnly || isFromExpress, false, isExpressTemplate, "", "","" ) %>
<%= widgetFactory.createTextField( "OrderingPartyAddress3", "", 
orderingParty.getAttribute("address_line_3").toString(), "30", isReadOnly || isFromExpress, false, isExpressTemplate, "", "","" ) %>
<%= widgetFactory.createTextField( "OrderingPartyAddress4", "", 
orderingParty.getAttribute("address_line_4").toString(), "30", isReadOnly || isFromExpress, false, isExpressTemplate, "", "","" ) %>

<%
        String countryCode = "";
        String countryDesc = "";
        /* IAZ IR-SEUK012660805 Rework Begin: For Release 5.2 we do not enable Country Fireld for OPY
           Vshah IR-SLUK021033331 04/28/10 For Release 6.0, we now enable Country Field for OPY */ 
        try
        {
        	countryCode = orderingParty.getAttribute("country");
        	if (InstrumentServices.isNotBlank(countryCode))
        		countryDesc = ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.COUNTRY, countryCode, 
        															resMgr.getCSDB().getLocaleName());
        	if (InstrumentServices.isBlank(countryDesc))
        	{
        		countryDesc = countryCode;
        		countryCode = "";
        	}
        }
        catch (Exception e)
        {
        	countryCode = "";
        	countryDesc = "";
        }
        
        if (InstrumentServices.isBlank(countryCode))
        	countryCode = " ";
		options = Dropdown.createSortedRefDataOptions(
		                        "COUNTRY",
		                        countryCode,
		                        loginLocale);
		                        
		out.println(widgetFactory.createSelectField( "OrderingPartyCountry", "DirectDebitInstruction.Country", 
" ", options, isReadOnly || isFromExpress, false, isExpressTemplate, "class='char30'", "","" ));
		                        
		/* IAZ IR-SEUK012660805 Rework End
		   Vshah IR-SLUK021033331 End  */                           
		  %>
<%--	Naveen 13-August-2012 IR-T36000004481. Modified the width of the above select field using style attribute		--%>		  
</div> <%--close left div --%>
<div class="columnRight">
<%
			identifierStr = "OrderingPartyBankName,OrderingPartyBankAddressLine1,OrderingPartyBankAddressLine2,OrderingPartyBankAddressLine3,OrderingPartyBankCountry,OrderingPartyBankCode,PayeeBankData";
		    sectionName = "orderPartyBank";
			
			 String orderingPartyBankSearchHtml = widgetFactory.createPartySearchButton(identifierStr,sectionName,
			 false,TradePortalConstants.ORDERING_PARTY_BANK,false);
			 
			 String clearFunction = "setButtonPressed('Clear', 0); return setBankPartyToClear('OPK');";
			 String clearOrderingPartyBankSearchHtml = widgetFactory.createPartyClearButton("ClearOrderingPartyBankButton",
			 clearFunction, false, TradePortalConstants.ORDERING_PARTY_BANK);
			 clearOrderingPartyBankSearchHtml = widgetFactory.createPartyClearButton( "ClearOrderingPartyBankButton", "clearOrderingPartyBank();",false,"");
			 
			 %>
			 <%= widgetFactory.createSubsectionHeader( "DirectDebitInstruction.OrderPartyBank",
								isReadOnly || isFromExpress, false, isExpressTemplate, orderingPartyBankSearchHtml+clearOrderingPartyBankSearchHtml) %>
	
        <%
        countryCode = "";
        countryDesc = "";
        try
        {
        	countryCode = orderingPartyBank.getAttribute("country");
        	if (InstrumentServices.isNotBlank(countryCode))
        		countryDesc = ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.COUNTRY, countryCode, 
        															resMgr.getCSDB().getLocaleName());
        	if (InstrumentServices.isBlank(countryDesc))
        	{
        		countryDesc = countryCode;
        		countryCode = "";
        	}
        }
        catch (Exception e)
        {
        	countryCode = "";
        	countryDesc = "";
        }
        
		%>        
<%--	Naveen 13-August-2012 IR-T36000004481. Modified the width of the TextField	  --%>
        <%= widgetFactory.createTextField( "OrderingPartyBankCode", "DomesticPaymentRequest.OrderingPartyBankBranchCode", 
orderingPartyBank.getAttribute("bank_branch_code").toString(), "30", isReadOnly || isFromExpress, false, isExpressTemplate, "class='char32'", "","" ) %>			

		<input type="hidden" name='OrderingPartyType' value="<%=TradePortalConstants.ORDERING_PARTY%>">
		<input type="hidden" name='OrderingPartyBankPartyType' value="<%=TradePortalConstants.ORDERING_PARTY_BANK%>">
		<input type="hidden" name='ApplicantPartyType' value="<%=TradePortalConstants.APPLICANT%>">
		<input type="hidden" name="PayerPartyOTLCustomerId" value='<%=payerParty.getAttribute("OTL_customer_id")%>'>
		<input type=hidden name="OrderingPartyType" value="<%=TradePortalConstants.ORDERING_PARTY%>"></p>  
		
<%--	Naveen 21-August-2012 IR-T36000004481/T36000004555. Modified the height of the TextArea using style attribute	--%>		
		<%= widgetFactory.createTextArea(
			"PayeeBankData",
			"DirectDebitInstruction.OrderPartyBankNameAddress",
			buildPaymentBankDisplayData(
					orderingPartyBank.getAttribute("bank_name"),
					orderingPartyBank.getAttribute("branch_name"),
					orderingPartyBank.getAttribute("address_line_1"),
 					orderingPartyBank.getAttribute("address_line_2"),
					orderingPartyBank.getAttribute("address_line_3"),
					countryDesc, isReadOnly),
			true, false, isExpressTemplate,
			"style=\"min-height: 80px;\"",
			"",
			"") %>
		
		
				<%-- Vshah ClearParty End --%>
		  <%-- Vshah CR509 Begin --%>							
		  <input type=hidden name="OrderingPartyBankName" value="<%=orderingPartyBank.getAttribute("bank_name")%>">
          <input type=hidden name="OrderingPartyBankAddressLine1" value="<%=orderingPartyBank.getAttribute("address_line_1")%>">
          <input type=hidden name="OrderingPartyBankAddressLine2" value="<%=orderingPartyBank.getAttribute("address_line_2")%>">
          <input type=hidden name="OrderingPartyBankAddressLine3" value="<%=orderingPartyBank.getAttribute("address_line_3")%>">
          <input type=hidden name="OrderingPartyBankBranchName" value="<%=orderingPartyBank.getAttribute("branch_name")%>">
          <input type=hidden name="OrderingPartyBankType" value="<%=TradePortalConstants.ORDERING_PARTY_BANK%>">
          <input type=hidden name="OrderingPartyBankCountry" value="<%=countryCode%>">  
          <%-- Vshah CR509 End --%>
    	</div> <%-- closes rightColumn --%>
    	<% if (!isReadOnly){ %> 
				<%=widgetFactory.createHoverHelp("orderPar", "PartySearchIconHoverHelp") %>
				<%=widgetFactory.createHoverHelp("orderPartyBank", "PartySearchIconHoverHelp") %>
				<%= widgetFactory.createHoverHelp("ClearOrderingPartyBankButton","PartyClearIconHoverHelp")%>			
			<%} %>
