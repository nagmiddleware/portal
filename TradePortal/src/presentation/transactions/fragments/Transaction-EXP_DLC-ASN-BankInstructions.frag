<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
*******************************************************************************
    Export Letter of Credit Assignment Page - Bank Instructions section

  Description:
    Contains HTML to create the Export Letter of Credit Assignment Page - 
    Bank Instructions section.

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transaction-EXP_DLC-ASN-BankInstructions.jsp" %>
*******************************************************************************
--%>

                
        
        <%
           if (isReadOnly)
           {
              out.println("&nbsp;");
           }
           else
           {
              options = ListBox.createOptionList(spclInstrDocList, "PHRASE_OID", "NAME", "", userSession.getSecretKey());

              defaultText = resMgr.getText("transaction.AddAPhrase", TradePortalConstants.TEXT_BUNDLE);

              out.println(widgetFactory.createSelectField( "SpecialInstructionsPhraseItem", "",defaultText , options, isReadOnly || isFromExpress,
					              !isTemplate,false,  "onChange=" + PhraseUtility.getPhraseChange( "/Terms/special_bank_instructions", "SpecialInstructionsText","1000","document.forms[0].SpecialInstructionsText"), "", "")); 
      
      }
        %>
       
<%= widgetFactory.createTextArea( "SpecialInstructionsText","",terms.getAttribute("special_bank_instructions"), isReadOnly || isFromExpress, false, false, "maxlength=1000 rows=10 cols='128'", "", "") %>                       
<%if(!isReadOnly){%>
    	<%=widgetFactory.createHoverHelp("SpecialInstructionsText", "FundsTransferRequest.InstructionsText") %>  
	<%}%>

<%= widgetFactory.createWideSubsectionHeader( "Assignment.CommCharges",isReadOnly,false, isFromExpress,"") %> 
	<%-- Kiran Rel 8.3 IR#T36000015075 07/09/2013 Start--%>
     <%= widgetFactory.createTextField( "BranchCode", "Assignment.BranchCode", terms.getAttribute("branch_code"), "30", isReadOnly, false, false,  "class='char31'", "","" ) %>                          
    <%-- Kiran Rel 8.3 IR#T36000015075 07/09/2013 End--%>
      <%= widgetFactory.createTextField( "DebitAccountNumber", "Assignment.DebitAccount", terms.getAttribute("coms_chrgs_our_account_num"), "30", isReadOnly, false, false,  "class='char19'", "","" ) %>                          
   
     <%= widgetFactory.createTextField( "DebitForeignAccountNumber", "Assignment.DebitFCA", terms.getAttribute("coms_chrgs_foreign_acct_num"), "30", isReadOnly, false, false,  "class='char19'", "","" ) %>                          
    
          <%
             options = Dropdown.createSortedCurrencyCodeOptions(terms.getAttribute("coms_chrgs_foreign_acct_curr"), loginLocale);
             //out.println(InputField.createSelectField("ForeignAccountCurrency", "", " ", options, "ListText", isReadOnly));
              out.println(widgetFactory.createSelectField( "ForeignAccountCurrency", "Assignment.ForeignCurrency"," " , options, isReadOnly,false,false, "class='char1'", "", ""));
          %>

         <%=widgetFactory.createLabel("", "ImportDLCIssue.InstructionsText", false, false, false, "")%>
        <%
           if (!isReadOnly){
              options = ListBox.createOptionList(spclInstrDocList, "PHRASE_OID", "NAME", "", userSession.getSecretKey());
              defaultText = resMgr.getText("transaction.AddAPhrase", TradePortalConstants.TEXT_BUNDLE);
        %>
        <%=widgetFactory.createSelectField( "CandCOtherTextPhraseItem", "",defaultText , options, isReadOnly || isFromExpress,
	              false,false,  "class='char11' onChange=" + PhraseUtility.getPhraseChange( "/Terms/coms_chrgs_other_text", "CandCOtherText","1000","document.forms[0].CandCOtherText"), "", "")%> 
        <%
           }
        %>
   			<%= widgetFactory.createTextArea( "CandCOtherText","",terms.getAttribute("coms_chrgs_other_text"), isReadOnly || isFromExpress, false, false, "maxlength=1000 rows=10 cols='128'", "", "") %>                       
    <%if(!isReadOnly){%>
    	<%=widgetFactory.createHoverHelp("CandCOtherText", "CommissionHoverHelp") %>
	<%}%>
