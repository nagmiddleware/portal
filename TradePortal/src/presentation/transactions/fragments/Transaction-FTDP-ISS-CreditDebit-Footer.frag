<%--
*******************************************************************************
  Transaction-FTDP-ISS-CreditDebit Footer

  Description: Everything that should be loaded at the end of the page
               should be included here.  This includes non-visible html 
               and javascript that should execute on page load.

*******************************************************************************
--%>
<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>



<%
  DataGridFactory dgFactory = new DataGridFactory(resMgr, userSession, formMgr,beanMgr, response);
  String gridLayout = dgFactory.createGridLayout("TransactionFTDPISSDataGridId", "TransactionFTDPISSDataGrid");
  Map searchQueryMap =  userSession.getSavedSearchQueryData();
  Map savedSearchQueryData =null;
  savedSearchQueryData = (Map)searchQueryMap.get("TransactionFTDPISSDataView");
%>
<%@ include file="/common/GetSavedSearchQueryData.frag" %> 
<script type="text/javascript">

<%-- Added a delete link in grid --%>
function deleteFormatter(){
	<% if(!isReadOnly){%>
    return "<a href='javascript:deleteBeneficiaryRow();'><%=StringFunction.escapeQuotesforJS(resMgr.getText("DomesticPaymentRequest.Delete", TradePortalConstants.TEXT_BUNDLE))%></a>";
    <%}else{%>
    	return "";
    <%}%>
}



  	var gridLayout = <%= gridLayout %>;
  	
  	
	
  	var initSearchParms = "transaction_oid=<%=transaction.getAttribute("transaction_oid")%>&benificiaryName=<%=benificiaryName%>";
  	initSearchParms += "&benificiaryBankName=<%=benificiaryBankName%>&paymentStatusSearch=<%=paymentStatusSearch%>&amount=<%=amount%>";
  	console.log("initSearchParms : "+initSearchParms);
  	<%-- createDataGrid("TransactionFTDPISSDataGridId", "TransactionFTDPISSDataView",gridLayout, initSearchParms); --%>
    <%--  DK CR-886 Rel8.4 10/15/2013 starts --%>
  	var savedSort = savedSearchQueryData["sort"];
  	var pmt_bene_sort_default = '<%=pmt_bene_sort_default%>'  ;
  	if ('O'==pmt_bene_sort_default)
  		savedSort = 'SEQUENCE_NUMBER';
  	else if('A'==pmt_bene_sort_default)
  		savedSort = 'AccountNumber_List';
  	var viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("TransactionFTDPISSDataView",userSession.getSecretKey())%>";  <%--Rel9.2 IR T36000032596 --%>
  	createDataGrid("TransactionFTDPISSDataGridId", viewName,gridLayout, initSearchParms,savedSort);
 	<%--  DK CR-886 Rel8.4 10/15/2013 ends --%>
  	
  	<%--  CR 857 Prateep Start  --%>
  		require(["dojo/query", "dojo/on", "t360/datagrid", "t360/popup", "dojo/domReady!"],
		      function(query, on, t360grid, t360popup){
		    query('#pmtBeneGridEdit').on("click", function() {
		      var columns = t360grid.getColumnsForCustomization('TransactionFTDPISSDataGridId');
		      var parmNames = [ "gridId", "gridName", "columns" ];
		      var parmVals = [ "TransactionFTDPISSDataGridId", "TransactionFTDPISSDataGrid", columns ];
		      t360popup.open(
		        'pmtBeneGridEdit', 'gridCustomizationPopup.jsp',
		        parmNames, parmVals);
		    });
		  });
		  		
  	<%--  CR 857 Prateep End  --%>
  	
  

</script>
