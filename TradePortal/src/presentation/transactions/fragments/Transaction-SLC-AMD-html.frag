<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
*******************************************************************************
                 Standby LC Amend Page - all sections

  Description:
    Contains HTML to create the Standby LC Amend page.  All data retrieval 
  is handled in the Transaction-IMP_DLC-AMD.jsp.

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transaction-SLC-ISS-BankDefined.jsp" %>
*******************************************************************************
--%>

<%--
*******************************************************************************
                     StandbyLC Issue Page - General section
*******************************************************************************
--%>

	<%= widgetFactory.createSectionHeader("1", "ImportDLCAmend.General") %>
		<div class="columnLeftNoBorder">	
			<%= widgetFactory.createTextField("transaction.YourRefNumber","transaction.YourRefNumber",terms.getAttribute("reference_number"),"10",true) %>
			<%= widgetFactory.createTextField("ApprovalToPayAmend.BuyerName","ApprovalToPayAmend.BuyerName",applicantName,"10",true) %>
			<%= widgetFactory.createDateField( "CurrentExpiryDate", "ApprovalToPayAmend.CurrentExpiryDate", currentExpiryDate, true) %>
			<%= widgetFactory.createDateField( "ExpiryDate", "SLCAmend.NewExpiryDate", StringFunction.xssHtmlToChars(terms.getAttribute("expiry_date")),
					isReadOnly, false, isExpressTemplate, "class='char6'", dateWidgetOptions, "")%>
							
		</div>		
		
		<div class="columnRight">
			<%
     			if(!isProcessedByBank)
      			{
     				String lcAmt = currencyCode +"  "+ TPCurrencyUtility.getDisplayAmount(originalAmount.toString(), currencyCode, loginLocale);
     		%>
   			
   				<%= widgetFactory.createTextField("SLCAmend.LCAmount","SLCAmend.LCAmount",lcAmt,"30",true) %>
   				<%--PR CMA Issue No 5 # IR T36000010961 --%>
   				<%-- CR-1026 MEer --%>
   				<%if (!userSession.isCustNotIntgTPS()){%>
   					<%= widgetFactory.createTextField("AvailableAmount", "ImportDLCAmend.CurrentAvailableAmount", availableAmount.toString(), "25", true, false,false, "", "", "")%>	
			<%
					}
				}
			%>
						
			&nbsp;&nbsp;&nbsp;<%= widgetFactory.createRadioButtonField("IncreaseDecreaseFlag", "Increase", 
					"SLCAmend.IncreaseAmount", TradePortalConstants.INCREASE, incrDecrValue.equals(TradePortalConstants.INCREASE), isReadOnly, " onChange='setNewAmount();'", "") %>
				<br/>
			&nbsp;&nbsp;&nbsp;<%= widgetFactory.createRadioButtonField("IncreaseDecreaseFlag", "Decrease", 
					"SLCAmend.DecreaseAmount", TradePortalConstants.DECREASE, incrDecrValue.equals(TradePortalConstants.DECREASE), isReadOnly, " onChange='setNewAmount();'", "") %>
			<%if (!isReadOnly) {%>
			<br/><br/>	
			<div class="formItem">
				<%=widgetFactory.createInlineLabel("",currencyCode)%>
				<%= widgetFactory.createAmountField( "TransactionAmount", "", displayAmount, currencyCode, isReadOnly, false, false, " onChange='setNewAmount();'", "", "none") %> 				
			</div>
			<%}else{
				displayAmount = TPCurrencyUtility.getDisplayAmount(displayAmount, currencyCode, loginLocale);
				StringBuffer         tempDisplayAmt              = new StringBuffer();
				tempDisplayAmt.append(currencyCode);
				tempDisplayAmt.append(" ");
				tempDisplayAmt.append(displayAmount);
				%>
				<%= widgetFactory.createAmountField( "TransactionAmount", " ", tempDisplayAmt.toString(), "", true, false, false, " onChange='setNewAmount();'", "", "") %>
				<input type="hidden" name="TransactionAmount"
			               value="<%=amountValue%>">	
			<%}%>
			<%-- Srinivasu_D IR#T36000033886 Rel9.1 10/27/2014 - Commented below duplicated variable --%>
			
					
			
			<% 
			
			StringBuffer         newLCAmount              = new StringBuffer();
			 if(!isProcessedByBank)
			  {
				 if(!badAmountFound) {					 
					  newLCAmount.append(currencyCode);
					  newLCAmount.append(" ");  
					  newLCAmount.append(TPCurrencyUtility.getDisplayAmount(calculatedTotal.toString(),currencyCode, loginLocale));
					  
				 }
			%>
			<%= widgetFactory.createAmountField( "TransactionAmount2", "SLCAmend.NewLCAmount", newLCAmount.toString(), "", true, false, false, "", "", "") %>
						

			<%
			}
			%>
			
			
			<div class="formItem">
			<%= widgetFactory.createStackedLabel("", "SLCAmend.NewAmountTolerance") %>
			<%= widgetFactory.createSubLabel( "transaction.Plus") %>
			<%if(!isReadOnly){ %>
			<%= widgetFactory.createPercentField( "AmountTolerancePlus", "", terms.getAttribute("amt_tolerance_pos"), isReadOnly,
										false, false, "", "", "none") %>
			<%}else {%>
				<b><%=terms.getAttribute("amt_tolerance_pos") %></b>
			<%} %>							
			
			
			<%= widgetFactory.createSubLabel( "transaction.Percent") %>
			&nbsp;&nbsp;
			<%= widgetFactory.createSubLabel( "transaction.Minus") %>
			<%if(!isReadOnly){ %>
			<%= widgetFactory.createPercentField( "AmountToleranceMinus", "", terms.getAttribute("amt_tolerance_neg"), isReadOnly,
										false, false, "", "", "none") %>
			<%}else {%>
				<b><%=terms.getAttribute("amt_tolerance_neg") %></b>
			<%} %>	
			
			<%= widgetFactory.createSubLabel( "transaction.Percent") %>
			<div style="clear:both;"></div>
			</div>	
		</div>
		
		<div style="clear: both"></div>
		
		<%-- <%= widgetFactory.createWideSubsectionHeader("SLCAmend.AmendmentDetailsText") %> --%>

 		<%
        	if (!isReadOnly) {
          		options = ListBox.createOptionList(addlCondDocList, "PHRASE_OID", "NAME", "", userSession.getSecretKey());
          		defaultText = resMgr.getText("transaction.AddAPhrase",TradePortalConstants.TEXT_BUNDLE);
        %>

		<%= widgetFactory.createSelectField("AddlDocsPhraseItem", "", defaultText, options, isReadOnly, false, false, 
	 		"onChange=" + PhraseUtility.getPhraseChange("/Terms/amendment_details","AmendmentDetailsText", textAreaMaxlen,"document.forms[0].AmendmentDetailsText"), "", "") %>
		<%
		 	}
		%>
		<%-- KMehta Rel 8400 IR T36000026451 on 25 Mar 2014  --%>
		<%-- KMehta Rel 9400 IR T36000042634 on 27 Aug 2015  --%>
		<%= widgetFactory.createTextArea("AmendmentDetailsText", "", terms.getAttribute("amendment_details"), isReadOnly,false,false, "rows='10';","","","") %>
		<%if(!isReadOnly) {%>
		<%= widgetFactory.createHoverHelp("AmendmentDetailsText","DirectDebitCollection.AdditionalToolTip")%>
		<%} %> 		
	</div>
	
	<%= widgetFactory.createSectionHeader("2", "SLCAmend.InstructionsToBank") %>
		
		<%-- <%= widgetFactory.createWideSubsectionHeader("SLCIssue.SpecialInstructionsText") %> --%>

		<%
	        if (!isReadOnly) {
          		options = ListBox.createOptionList(spclInstrDocList, "PHRASE_OID", "NAME", "", userSession.getSecretKey());
          		defaultText = resMgr.getText("transaction.AddAPhrase",TradePortalConstants.TEXT_BUNDLE);
		%>
		
		<%= widgetFactory.createSelectField("CommInvPhraseItem", "", defaultText, options, isReadOnly, false, false, 
	 		"onChange=" + PhraseUtility.getPhraseChange("/Terms/special_bank_instructions","SpclBankInstructions",textAreaMaxlen,"document.forms[0].SpclBankInstructions"), "", "") %>
          
        <%
        	}
		%>
		<%-- KMehta Rel 8400 IR T36000026451 on 25 Mar 2014  --%>
		<%-- KMehta Rel 9400 IR T36000042634 on 27 Aug 2015 Change Start--%>
		<%= widgetFactory.createTextArea("SpclBankInstructions", "", terms.getAttribute("special_bank_instructions"), isReadOnly,false,false, "rows='10';","","","") %>
		<%if(!isReadOnly) {%>
		<%= widgetFactory.createHoverHelp("SpclBankInstructions","InstrumentIssue.PlaceHolderSpecialBankInstr")%>
		<% } %>
 	</div>
