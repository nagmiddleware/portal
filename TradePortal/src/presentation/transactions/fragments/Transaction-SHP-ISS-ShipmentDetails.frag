<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
*******************************************************************************
            Shipment Guarantee Issue Page - Shipment Details section

  Description:
    Contains HTML to create the Shipment Guarantee Issue Shipment Details 
    section.

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transaction-SHP-ISS-ShipmentDetails.jsp" %>
*******************************************************************************
--%>


	<div class="columnLeft">
     
         <%= widgetFactory.createTextField( "VesselNameVoyageNumber", "AWB_SGTEE.VesselNameVoyage", 
        		 terms.getFirstShipment().getAttribute("vessel_name_voyage_num"), "40", isReadOnly, false, false, "class='char35';", "", "") %>   
        		                                                              
                   
         <%= widgetFactory.createTextField( "BillOfLadingNumber", "AWB_SGTEE.BillOfLadingNumber", 
        		 terms.getFirstShipment().getAttribute("bill_of_lading_num"), "40", isReadOnly, false, false, "class='char35';", "", "") %>
          
         <%= widgetFactory.createTextField( "ContainerNumber", "AWB_SGTEE.ContainerNumber", 
        		 terms.getFirstShipment().getAttribute("container_number"), "40", isReadOnly, false, false, "class='char35';", "", "") %>                 
   </div>
   <div class="columnRight">
     	  <%= widgetFactory.createTextField( "PortOfLoading", "AWB_SGTEE.PortOfLoading", 
     			  terms.getFirstShipment().getAttribute("shipment_from"), "40", isReadOnly , false, false, "class='char35';", "", "") %>
          
          <%= widgetFactory.createTextField( "PortOfDischarge", "AWB_SGTEE.PortOfDischarge", 
        		  terms.getFirstShipment().getAttribute("shipment_to"), "40", isReadOnly, false, false, "class='char35';", "", "") %>
          <br>             
   </div>                      
   <div style="clear: both;"></div>
   <%=widgetFactory.createWideSubsectionHeader("AWB_SGTEE.GoodsDescription",isReadOnly,false,false,"")%>
        <%
          
              options = ListBox.createOptionList(goodsDocList, "PHRASE_OID", "NAME", "", userSession.getSecretKey());

              defaultText = resMgr.getText("transaction.AddAPhrase", TradePortalConstants.TEXT_BUNDLE);

              out.println(widgetFactory.createSelectField( "GoodsPhraseItem", "", defaultText, options, isReadOnly, true, false, "onChange=" + PhraseUtility.getPhraseChange("/Terms/ShipmentTermsList/goods_description", "GoodsDescText", 
              "6500","document.forms[0].GoodsDescText"), "", ""));
        %>                     
	<%if(!isReadOnly){ %>	
	<%=widgetFactory.createTextArea("GoodsDescText", "", terms.getFirstShipment().getAttribute("goods_description"),
					isReadOnly, false, false, "rows='10' cols='128'", "", "")%>
	
		<%=widgetFactory.createHoverHelp("GoodsDescText", "AirwayBill.GoodsDescToolTip") %>	
	<%}else{%>
	<%=widgetFactory.createAutoResizeTextArea("GoodsDescText", "", terms.getFirstShipment().getAttribute("goods_description"),
					isReadOnly, false, false, "maxlength=6500 style='width:600px;min-height:140px;' cols='128'", "", "")%>
	<%}%>