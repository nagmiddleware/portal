<%--
*******************************************************************************
  Transaction-FTRQ-ISS-BeneAcctData.frag

  Description:
  A fragment for displaying terms party accounts.
  Fairly generic, but should be copied for specific instrument behavior.

  The following variables are passed:

    termsParty - a TermsPartyWebBean
    isReadOnly
    isTemplate
    widgetFactory

*******************************************************************************
--%>
<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.html.*,java.math.*" %>

<%
  // Logic to handle account list.  If no payee has been searched, there is
  // only an enterable account number field.  Otherwise, we display the set of accounts defined
  // for the party (when the party is selected, we get the set of accounts for that party 
  // and save it on the TermsParty record -- if the party's accounts change later, we do not update
  // the set on the TermsParty record.) as well as an enterable account number field.  The account 
  // number selected by the user will be one of the payee's predefined accounts or the 
  // entered account number.
  
  
  
  TermsPartyWebBean termsParty = beanMgr.createBean(TermsPartyWebBean.class, "TermsParty");

  /*
  // Use a Party web bean to get the party data for the selected oid.
  PartyWebBean party = beanMgr.createBean(PartyWebBean.class, "Party");
  //todo: what if partyOid is not populated
  party.setAttribute("party_oid", bene_party_oid);
  party.getDataFromAppServer();
 
  DocumentHandler partyDoc = new DocumentHandler();
  party.populateXmlDoc(partyDoc);
  partyDoc = partyDoc.getFragment("/Party");
*/
  String partyType = TradePortalConstants.PAYEE;
  //load the terms party web bean
  // not entirely sure why we do differently per party type, but this was taken from int payment
  if (InstrumentServices.isNotBlank(bene_party_oid)) {
  if (TradePortalConstants.PAYEE.equals(partyType)) {
    // use the "FromDocTagsOnly" so we don't overwrite the acct_currency
    //termsParty.loadTermsPartyFromDocTagsOnly(partyDoc);

    termsParty.loadAcctChoices(bene_party_oid, formMgr.getServerLocation(), resMgr);
  }
  else if (TradePortalConstants.PAYER.equals(partyType)) {
    //termsParty.loadTermsPartyFromDoc(partyDoc);
    termsParty.setAttribute("address_search_indicator", "N");

    termsParty.loadAcctChoices(bene_party_oid, formMgr.getServerLocation(), resMgr);
  }
  else {
    //do nothing
  }
}
  
  
  
  
  

  String acctList = StringFunction.xssHtmlToChars(termsParty.getAttribute("acct_choices"));
  DocumentHandler acctData = new DocumentHandler(acctList, true);
  String termsPartyAcctNum = null;
  String termsPartyAcctCcy = null;

  Vector accounts = acctData.getFragments("/DocRoot/ResultSetRecord");
  int numAccts = accounts.size();



  Debug.debug("Account Choices for Party: " + acctList);
  Debug.debug("Number of account in list is " + numAccts);

  // Set the showAccts flag that determines if a list of accounts with radio buttons
  // appears.  They appear if we have a set of accounts (i.e., it's been preloaded into
  // a acctList variable).  However, in readonly mode, we do not display the list.


   boolean displayMultipleAccounts = false;
  if (numAccts > 0) displayMultipleAccounts = true;
  if (isReadOnly) displayMultipleAccounts = false;

  // Store the set of acc<%--ounts in a hidden field.  This gets written to the
  // TermsParty record so we always have the set of accounts at the time the party was selected.
%>
  <input type="hidden" name="PayeeAcctChoices" value='<%=acctList%>'>

<%
  if (displayMultipleAccounts && !currentDomesticPayment.isFixedValue("payee_account_number", isTemplate)) {
             /* defaultText = resMgr.getText("DomesticPaymentRequest.selectAccountNumber",  TradePortalConstants.TEXT_BUNDLE);
        	  out.println(InputField.createRadioButtonField("AccountSelectType", "NOT"+TradePortalConstants.USER_ENTERED_ACCT, "",
				  accountSet.contains(payeeAcctNum), "ListText", "", isReadOnly||isStatusVerifiedPendingFX,"onClick=\"javascript:setAccount('SelectedAccount');\"\""));
        	  acctNo = (accountSet.contains(payeeAcctNum))? payeeAcctNum : "";
              options = ListBox.createOptionList(accountDoc, "ACCOUNT_NUMBER", "ACCOUNT_NUMBER",acctNo,null);
		      out.println(InputField.createSelectField("SelectedAccount", "", defaultText, options, "ListText", isReadOnly||isStatusVerifiedPendingFX,"onChange=\"javascript:setAccountSelectType('SelectedAccount','1');\"\""));
			  */ 
			 
			  DocumentHandler acct;
			  boolean foundAcctMatch = false;	// indicates if ANY predefined acct num matches ben. acct  number from business object
			  boolean foundMatch = false;		// indicates if a given acct num matches ben. acct number from business object
    for (int i = 0; i<numAccts; i++) {
      acct = (DocumentHandler) accounts.elementAt(i);
		
		
      termsPartyAcctNum = acct.getAttribute("ACCOUNT_NUMBER");       // no "slash"
      termsPartyAcctCcy = acct.getAttribute("/CURRENCY");            // get with "slash"
		
		
				  foundMatch = termsPartyAcctNum.equals(payeeAcctNum);
			  
		
      if (!foundAcctMatch) {
        foundAcctMatch = foundAcctMatch | foundMatch;
      }

      String radioLabel;
      if (termsPartyAcctCcy.equals("")) {
        radioLabel = termsPartyAcctNum;
      } else {
        radioLabel = termsPartyAcctNum + " (" + termsPartyAcctCcy + ")";
      }
      out.println(widgetFactory.createRadioButtonField( "SelectedAccount", 
        "SelectedAccount" + termsPartyAcctNum, radioLabel, //id, label
        termsPartyAcctNum ,  //value if selected
        foundMatch, isReadOnly||isStatusVerifiedPendingFX, "onClick=\"javascript:setAccount(this.id);\"", ""));
      out.println("<div style=\"clear:both;\"></div>");
    }  // End of for loop

    // This IF sets up acctNum and acctCcy fields with values if necessary and uses them
    // for displaying the enterable account field.
		
    // Display radio button for selection of the enterable account.  If we haven't found
    // an account match yet, then we might match on the enterable value.  If they match,
    // turn on the radio button.
    if (foundAcctMatch) {
      termsPartyAcctNum = "";
     // termsPartyAcctCcy = "";
      foundMatch = false;
    } else {
       termsPartyAcctNum = payeeAcctNum;
      //termsPartyAcctCcy = termsParty.getAttribute("acct_currency");
      if (termsPartyAcctNum == null) termsPartyAcctNum = "";
          foundMatch = termsPartyAcctNum.equals(payeeAcctNum);
    }
    //now generate the text field, but without label as that is above
%>
    <div>
      <%= widgetFactory.createRadioButtonField( "SelectedAccount", 
            "EnteredAccountRDO", "", //id, label
            TradePortalConstants.USER_ENTERED_ACCT, //value
            foundMatch, isReadOnly, "inline onClick=\"javascript:setAccount('EnteredAccount');\"", "") %>
		<%= widgetFactory.createTextField( "EnteredAccount", "", termsPartyAcctNum, 
            "34", isReadOnly, false, false,  "class='char30' onChange=\"javascript:setAccount(this.id,true);\"", "","none" ) %>
    </div>
<%
  } else {
    // We're not displaying the account list, so populate the account field with the
    // account from the Payee party record.
    termsPartyAcctNum = payeeAcctNum;
    //termsPartyAcctCcy = termsParty.getAttribute("acct_currency");
    if (termsPartyAcctNum == null) termsPartyAcctNum = "";

    //note no label on the below
%>
    <%= widgetFactory.createTextField( "EnteredAccount", "", termsPartyAcctNum, 
          "34", isReadOnly, !isTemplate, false,  "class='char32' onChange=\"javascript:setAccount(this.id,false);\"", "","none" ) %>   
<%
  }
%>
