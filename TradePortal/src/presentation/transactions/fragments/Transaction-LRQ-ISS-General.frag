<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
*******************************************************************************
                     Loan Request Page - General section

  Description:
    Contains HTML to create the Loan Request General section.  

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transaction-LRQ-ISS-General.jsp" %> 
*******************************************************************************
--%>
<div>
<%
	String applicantSearchHtml = "";
	String applicantClearHtml = "";
	String addressSearchIndicator = "";
	String identifierStr1="app_terms_party_oid,AppName,AppAddressLine1,AppAddressLine2,AppCity,AppStateProvince,AppCountry,AppPostalCode,Applicant,AppUserDefinedField1,AppUserDefinedField2,AppUserDefinedField3,AppUserDefinedField4";
	String sectionName1="borrower";
	if (!(isReadOnly || isFromExpress)) {
		addressSearchIndicator = termsPartyApp.getAttribute("address_search_indicator");
		applicantSearchHtml = widgetFactory.createPartySearchButton(
				identifierStr1, sectionName1, false,
				TradePortalConstants.APPLICANT, false);
		if (!isReadOnly && isTemplate) {
			applicantClearHtml = widgetFactory.createPartyClearButton(
					"ClearApplicantButton", "clearApplicant()", false,
					"");
		}
	}
%>

<%=widgetFactory.createWideSubsectionHeader(
					"LoanRequest.Borrower", false, !isTemplate,
					isExpressTemplate,
					(applicantSearchHtml + applicantClearHtml))%>


<div class=columnLeft>
<%
if (!(isReadOnly || isFromExpress) && !addressSearchIndicator.equals("N"))
{
	if(!isReadOnly && corpOrgHasMultipleAddresses)
	{ %>
	<div class = "formItem">
	   <button data-dojo-type="dijit.form.Button"  name="AddressSearch" id="AddressSearch" type="button">
       <%=resMgr.getText("common.searchAddressButton",TradePortalConstants.TEXT_BUNDLE)%>
       <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
	       var corpOrgOid = <%=corpOrgOid%>;
			require(["t360/common", "t360/partySearch"], function(common, partySearch){
				 partySearch.setPartySearchFields('<%=TradePortalConstants.APPLICANT%>');
	 			common.openCorpCustAddressSearchDialog(corpOrgOid);
	 		});
       </script>
       </button>

	 </div>  
	<% 
	}
}
%>
	<%
		String applicantAddress = termsPartyApp.buildAddress(false, resMgr);
	%>

	 <%-- Srinivasu_D  T36000019515 Rel8.4 01/23/2014 - start - changing the textarea to autoresize --%>	
	 <%--
	<%=widgetFactory.createTextArea("Applicant", "",
					applicantAddress, true, false, false,
					"onFocus='this.blur();' rows='4'", "", "")%>
	 --%>
	 
  	 <%= widgetFactory.createAutoResizeTextArea("Applicant", "", applicantAddress, true, false,false, "class='autoHeightTxtAreaQtrWidthNoSidebar' style='overflow-x: hidden;' rows='4' cols='45'", "", "")%>	
		<%-- Srinivasu_D  T36000019515 Rel8.4 01/23/2014 - End --%>
	<%
	//	secureParms.put("app_terms_party_oid",termsPartyApp.getAttribute("terms_party_oid"));
	%>
	<input type=hidden name="app_terms_party_oid"
		value="<%=termsPartyApp.getAttribute("terms_party_oid")%>">
	<input type=hidden name="AppTermsPartyType" 
        value="<%=TradePortalConstants.APPLICANT%>"><input
		type=hidden name="AppName"
		value="<%=termsPartyApp.getAttribute("name")%>"> <input
		type=hidden name="AppAddressLine1"
		value="<%=termsPartyApp.getAttribute("address_line_1")%>"> <input
		type=hidden name="AppAddressLine2"
		value="<%=termsPartyApp.getAttribute("address_line_2")%>"> <input
		type=hidden name="AppCity"
		value="<%=termsPartyApp.getAttribute("address_city")%>"> <input
		type=hidden name="AppStateProvince"
		value="<%=termsPartyApp.getAttribute("address_state_province")%>">
	<input type=hidden name="AppCountry"
		value="<%=termsPartyApp.getAttribute("address_country")%>"> <input
		type=hidden name="AppPostalCode"
		value="<%=termsPartyApp.getAttribute("address_postal_code")%>">
	<input type=hidden name="AppOTLCustomerId"
		value="<%=termsPartyApp.getAttribute("OTL_customer_id")%>"> <input
		type=hidden name="AppAddressSeqNum"
		value="<%=termsPartyApp.getAttribute("address_seq_num")%>">
	<%--rbhaduri - 14th Jun 06 - IR SAUG051361872--%>
	<input type=hidden name="AppAddressSearchIndicator"
		value="<%=termsPartyApp.getAttribute("address_search_indicator")%>">
	<input type=hidden name="AppUserDefinedField1"
	value="<%=termsPartyApp.getAttribute("user_defined_field_1")%>">
	<input type=hidden name="AppUserDefinedField2"
	value="<%=termsPartyApp.getAttribute("user_defined_field_2")%>">
	<input type=hidden name="AppUserDefinedField3"
	value="<%=termsPartyApp.getAttribute("user_defined_field_3")%>">
	<input type=hidden name="AppUserDefinedField4"
	value="<%=termsPartyApp.getAttribute("user_defined_field_4")%>">
</div>
<div class='columnRight'>

	<%=widgetFactory.createTextField("ApplRefNum",
					"LoanRequest.BorrowerRefNum",
					terms.getAttribute("reference_number"), "30", isReadOnly,
					false, false, "", "", "")%>
</div>

</div>
<script LANGUAGE="JavaScript">
	function clearApplicant() {
		document.TransactionLRQ.AppName.value = "";
		document.TransactionLRQ.AppAddressLine1.value = "";
		document.TransactionLRQ.AppAddressLine2.value = "";
		document.TransactionLRQ.AppCity.value = "";
		document.TransactionLRQ.AppStateProvince.value = "";
		document.TransactionLRQ.AppCountry.value = "";
		document.TransactionLRQ.AppPostalCode.value = "";
		document.TransactionLRQ.AppOTLCustomerId.value = "";
		document.TransactionLRQ.Applicant.value = "";
	  document.TransactionLRQ.AppUserDefinedField1.value = "";
	  document.TransactionLRQ.AppUserDefinedField2.value = "";
	  document.TransactionLRQ.AppUserDefinedField3.value = "";
	  document.TransactionLRQ.AppUserDefinedField4.value = "";
	}
</script>
