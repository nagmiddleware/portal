<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
*******************************************************************************
            Transaction Terms Detail Page - Terms Summary section

  Description:   
    This jsp is only designed to display the Terms Summary Section.  This
  jsp is called by possibly 2 different jsp's : The Transaction-TermsDetails.jsp. 
  This is designed to be called by:
  	<%@ include file="Transaction-Terms-Summary.jsp" %>
  Because it is called this way, it is assumed that various variables are 
  defined.  Most noteably the isFromCurrentTerms and instrumentType are
  critical to bringing up this page with out error.  Also important are the 
  setup of the following webBeans: Instrument, Terms, TermsParty (1 and 2),
  and the transaction.
  Please Note that the Download LC Terms button is only meant to be displayed 
  if we're coming from the Current Terms.jsp and the instrumentType is 
  an Export LC.  The actual execution of the Download button is still to be 
  determined, right now it will just log the user off since it goes to a 
  place that's not defined in the NavMgr file.
*******************************************************************************
--%>


<%
     /***************************************************************************************************
      * Start of Terms Summary Section 
      *********************************/           Debug.debug("****** Start of Terms Summary Section ******");

 

  String paymentAppliedAmount = TPCurrencyUtility.getDisplayAmount(
                             transaction.getAttribute("copy_of_amount"), 
                             transaction.getAttribute("copy_of_currency_code"),
                             loginLocale);
                             
  String paymentSettledAmount  = TPCurrencyUtility.getDisplayAmount(
                               terms.getAttribute("payment_settled_amount"),// IR VRUJ021860876
                               transaction.getAttribute("copy_of_currency_code"),
                             loginLocale);  
                             
  String referenceNumber = terms.getAttribute("reference_number")==null?"":terms.getAttribute("reference_number");
 %>

 <%= widgetFactory.createTextField("PaymentAppliedAmount", "TransactionTerms.PaymentAppliedAmount", paymentAppliedAmount, "30", true, false, false, "", "", "inline")%>
 <%= widgetFactory.createTextField("PaymentSettledAmount", "TransactionTerms.PaymentSettledAmount", paymentSettledAmount, "30", true, false, false, "", "", "inline")%>
 <%= widgetFactory.createTextField("YourReferenceNumber", "TransactionTerms.YourReferenceNumber", referenceNumber, "30", true, false, false, "", "", "inline")%>
