<%=widgetFactory.createSubsectionHeader("SettlementInstruction.paymentDetails", isReadOnly,false, false, "")%>
   <div class="formItemWithIndent3">
   <%= widgetFactory.createRadioButtonField("accountRemitInd", "Account", "SettlementInstruction.account",
					TradePortalConstants.SETTLEMENT_ACCOUNT_REMIT_TYPE_ACCOUNT, TradePortalConstants.SETTLEMENT_ACCOUNT_REMIT_TYPE_ACCOUNT.equals(terms.getAttribute("account_remit_ind")), isReadOnly, "", "") %>
   </div>
   <% 
        String userCheck = null;
        Vector availability = null;
        if ( InstrumentType.LOAN_RQST.equals(instrument.getAttribute("instrument_type_code")) ) {
           availability = new Vector();
           availability.add("AVAILABLE_FOR_LOAN_REQUEST");
        }
        termsPartyApp.loadAcctChoices(userSession.getOwnerOrgOid(), formMgr.getServerLocation(), resMgr, availability, userCheck, true);
        String settleAcctList = StringFunction.xssHtmlToChars(termsPartyApp.getAttribute("acct_choices"));

        DocumentHandler settleAcctOptions = new DocumentHandler(settleAcctList, true);
        String DebitPrincipalAcctoptions = Dropdown.createSortedAcctOptionsOid(settleAcctOptions, terms.getAttribute("principal_account_oid"), loginLocale);
        String DebitChargesAcctoptions   = Dropdown.createSortedAcctOptionsOid(settleAcctOptions, terms.getAttribute("charges_account_oid"), loginLocale);
        String DebitInterestAcctoptions  = Dropdown.createSortedAcctOptionsOid(settleAcctOptions, terms.getAttribute("interest_account_oid"), loginLocale);
        
   %>
   <div id="principalAcctID">
   <%= widgetFactory.createSelectField( "principalAccountOid", "SettlementInstruction.debitAcctForPrincipal"," ",
                DebitPrincipalAcctoptions, isReadOnly, true, false, "", "", "formItemWithIndent1") %>
    </div>       
    <% if ( InstrumentType.LOAN_RQST.equals(instrument.getAttribute("instrument_type_code")) ) { %>
       <%= widgetFactory.createSelectField( "interestAccountOid", "SettlementInstruction.debitAccountForInterest"," ",
                DebitInterestAcctoptions, isReadOnly, false, false, "", "", "formItemWithIndent1") %>
    <% } %>
                  
   <%= widgetFactory.createSelectField( "chargesAccountOid", "SettlementInstruction.debitAcctForCharges"," ",
                DebitChargesAcctoptions, isReadOnly, true, false, "", "", "formItemWithIndent1") %>  
                
   </br>             
   <div class="formItemWithIndent3">					
        <%= widgetFactory.createRadioButtonField("accountRemitInd", "Remitted", "SettlementInstruction.remittedToYourBranch",
					TradePortalConstants.SETTLEMENT_ACCOUNT_REMIT_TYPE_REMITTED, TradePortalConstants.SETTLEMENT_ACCOUNT_REMIT_TYPE_REMITTED.equals(terms.getAttribute("account_remit_ind")), isReadOnly, "", "") %>
   </div>