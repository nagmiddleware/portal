<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
  <%
       // This JSP must be included into any transaction page that supports
       // multi-part pages.  It should be included using the <@include tag
       // instead of the <jsp:include tag

       // It must be included after the "secureParms" variable is instantiated
       // in the calling JSP, but before the call to formMgr.getFormInstanceAsInputField()

       // These are the errors from the user's last action, if any
       // They must be sent as part of the request so that they can be displayed
       // if all the user did was change tabs
       secureParms.put("lastActionErrors", errorsToStoreInSecureParms);

       // The name of the part that the user is currently on
       secureParms.put("partNumber", whichPart);
  %>
