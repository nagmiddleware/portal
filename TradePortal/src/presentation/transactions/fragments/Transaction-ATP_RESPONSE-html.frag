<%--
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
--%>
<%--
*******************************************************************************
      Approval to Pay Response Page - all sections

  Description:
     This page is used by the Transaction-ATP-APR.jsp to display ApprovalToPay transaction
	 HTML.  All data retrieval
	 is handled in the Transaction-ATP-APR.jsp.

	 Special assumptions are that the following fields are declared and populated:

	 String ApprovalToPayInstructions
	 String instrumentType

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transaction-ATP_RESPONSE-html.frag" %>
*******************************************************************************
--%>


<%--
*******************************************************************************
    Approval to Pay Response Transaction Page - General section
*******************************************************************************
--%>
	<br>
	<div>
		<%--
		    ApprovalToPayResponse.DrawingAmount,ApprovalToPayResponse.DrawingNumber are replaced with
		    ApprovalToPayResponse.PresentationAmount,ApprovalToPayResponse.PresentationNumber
		    and the new feilds Presentation Date & Other Party
		--%>
		<%= widgetFactory.createTextField("PresentationAmount", "ApprovalToPayResponse.PresentationAmount",
			terms.getAttribute("amount_currency_code")+" "+TPCurrencyUtility.getDisplayAmount(terms.getAttribute("amount"), terms.getAttribute("amount_currency_code"), loginLocale),
			"35", true, false, false, "", "", "inline") %>

		<%= widgetFactory.createTextField("PresentationNumber", "ApprovalToPayResponse.PresentationNumber", terms.getAttribute("drawing_number"),
				"35", true, false, false, "", "", "inline") %>

      	<%
	  		String presentationDateSql = "select presentation_date from mail_message where  response_transaction_oid= ?";
      		
      		//jgadela  R90 IR T36000026319 - SQL FIX
			Object sqlParamsTr1[] = new Object[1];
			sqlParamsTr1[0] = transaction.getAttribute("transaction_oid");
      		DocumentHandler results = DatabaseQueryBean.getXmlResultSet(presentationDateSql, false, sqlParamsTr1);

      		if(results != null){
          		presentationDate = results.getAttribute("/ResultSetRecord(0)/PRESENTATION_DATE");
         		//The retrieved presentationDate value would be in the format of "2007-10-25 00:00:00.0"
       		}

	    	//As "PresentationDate" is a newly added attribute empty check is being done here to avoid exceptions
	    	// for existing transactions
       		if(InstrumentServices.isNotBlank(presentationDate))
      	%>
        	<%= widgetFactory.createTextField("PresentationNumber", "ApprovalToPayResponse.PresentationDate",
				TPDateTimeUtility.formatDate(presentationDate, TPDateTimeUtility.SHORT, loginLocale),
				"35", true, false, false, "", "", "inline") %>

		<%= widgetFactory.createTextField("OtherParty", "ApprovalToPayResponse.OtherParty", counterPartyName, "35", true, false, false, "", "", "inline") %>
		<div style="clear: both;"></div>
	</div>
	<br>
	<%@ include file="Transaction-Documents.frag" %>
	<% if(instrumentType.equals(InstrumentType.APPROVAL_TO_PAY)){ %>
  		<%= widgetFactory.createLabel("", "ApprovalToPayResponse.Notice", false, true, false, "") %>

 	<%
 		}
    	// if instrument type is Approval to Pay,
		// display the three radio buttons for  ApprovalToPay instruction options
		if(instrumentType.equals(InstrumentType.APPROVAL_TO_PAY)){
	%>
		<div class="formItem">
			<%= widgetFactory.createRadioButtonField("DiscrepancyInstructions", "TradePortalConstants.ATP_INVOICE_APPROVED", "",
				TradePortalConstants.ATP_INVOICE_APPROVED, discrepancyInstructions.equals(TradePortalConstants.ATP_INVOICE_APPROVED), isReadOnly, "", "") %>
		<span class="radioButtonLongLabelInline"><%= resMgr.getText("ApprovalToPayResponse.Approved",TradePortalConstants.TEXT_BUNDLE)%></span>
		<%if(!isReadOnly) {%>
			<%=widgetFactory.createHoverHelp("TradePortalConstants.ATP_INVOICE_APPROVED","ApprovalToPayResponse.Approved") %>
		<%}%>
			<br/>
			<%= widgetFactory.createRadioButtonField("DiscrepancyInstructions", "TradePortalConstants.ATP_INVOICE_REFUSED", "",
				TradePortalConstants.ATP_INVOICE_REFUSED, discrepancyInstructions.equals(TradePortalConstants.ATP_INVOICE_REFUSED), isReadOnly, "", "") %>
		<span class="radioButtonLongLabelInline"><%= resMgr.getText("ApprovalToPayResponse.Refused",TradePortalConstants.TEXT_BUNDLE)%></span>
		<%if(!isReadOnly) {%>
			<%=widgetFactory.createHoverHelp("TradePortalConstants.ATP_INVOICE_REFUSED","ApprovalToPayResponse.Refused") %>
		<%}%>		
			<br/>
			<%= widgetFactory.createRadioButtonField("DiscrepancyInstructions", "TradePortalConstants.ATP_OTHER", "",
				TradePortalConstants.ATP_OTHER, discrepancyInstructions.equals(TradePortalConstants.ATP_OTHER), isReadOnly, "", "") %>
		<span class="radioButtonLongLabelInline"><%= resMgr.getText("ApprovalToPayResponse.Other",TradePortalConstants.TEXT_BUNDLE)%></span>
		<%if(!isReadOnly) {%>
			<%=widgetFactory.createHoverHelp("TradePortalConstants.ATP_OTHER","ApprovalToPayResponse.Other") %>
		<%}%>		
			<br/>
		</div>
	<%}%>

    <br>

 	<%
        if (!isReadOnly) {
          options = ListBox.createOptionList(discrInstrDocList, "PHRASE_OID", "NAME", "", userSession.getSecretKey());
          defaultText = resMgr.getText("transaction.AddAPhrase", TradePortalConstants.TEXT_BUNDLE);
	%>
		<%= widgetFactory.createSelectField("DiscrDocsPhraseItem", "ApprovalToPayIssue.AdditionalInstructions", defaultText, options, isReadOnly, false, false,
				"onChange=" + PhraseUtility.getPhraseChange("/Terms/instr_other","OtherInstructionsText", "1000","document.forms[0].OtherInstructionsText"), "", "") %>
	<%}%>

 <%if (isReadOnly){ %>
 <span class="formItem">
 <%=resMgr.getText("ApprovalToPayIssue.AdditionalInstructions", TradePortalConstants.TEXT_BUNDLE)%>
</span>
<%} %>

    <%--<%= widgetFactory.createTextArea("OtherInstructionsText", "", terms.getAttribute("instr_other"), isReadOnly) %>--%>
			<%= widgetFactory.createTextArea("OtherInstructionsText", "", terms.getAttribute("instr_other"), isReadOnly, false, false, "maxlength=1000 rows=10", "", "") %>			
			<%if(!isReadOnly) {	%>
			
			<%=widgetFactory.createHoverHelp("OtherInstructionsText","OtherInstructionsHoverText") %>
			<% } %> 
  	<br>