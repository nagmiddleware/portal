<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
*******************************************************************************
                               Phrase Lookup Prep

  Description:
     This included JSP helps support phrase lookup logic.  Basically it defines
  a convenience method and several hidden input fields.

  For each phrase dropdown, define it in a manner similar to the following.  The
  key to this is the onChange parameter.  It calls a method that sets the 
  hidden fields with several values: the oid of the selected item, the xml path
  of the text to append to and the name of the field on the page containing the
  text.  (Note: because the page is initially created with no values in the
  hidden fields, this is what the user sees even after they've been set with 
  values.  Thus, the user can never see the oid.)

  InputField.createSelectField("CommInvPhraseItem", "", defaultText
                   options, "ListText", isReadOnly,
                   "onChange=" + getPhraseChange("/Terms/comm_invoice_text",
                                                 "CommInvText"))

   CommInvPhraseItem is the name of the field.  It is not relevant.
   defaultText is a string for the first item in the dropdown, generally "".
   options is a set of HTML options.  Can be created with 
       ListBox.createOptionList.

   For the getPhraseChange method, the second parameter is the field name in
   the JSP containing the text.  The first parameter is the path name given
   in the mapfile for that field name.

   When the user selects a phrase in a dropdown, the three hidden fields are
   given values.  When the user click the associated phrase lookup button, the
   form is submitted to the PhraseLookupMediator which gets the text for the
   given oid, gets the text to append it to (from the input document), appends
   it, and puts the resulting text in the /Out document.

   Upon return to your page, retrieve the /Out text and place it in the 
   correct field.  The fieldname in the /In/PhraseLookupInfo section can be
   used to set the focus.

*******************************************************************************
--%>

<SCRIPT LANGUAGE="JavaScript">
  <%-- todo: get rid of this --%>
  function setPhraseLookupFields(oid, textpath, fieldName, maxLength) {
    document.forms[0].SelectedPhrase.value = oid;
    document.forms[0].TextPath.value = textpath; 
    document.forms[0].TextFieldName.value = fieldName;
    document.forms[0].TextMaxlength.value = maxLength;
  }

  var xhReq1;
  function setPhraseLookupFields(phraseSelect, textpath, fieldName, textAreaMaxLength, textArea) {

    <%-- cquinton 3/2/2013 call local function that pulls in dojo functions --%>
    <%--  todo: remove this and modify the phrase dropdowns to do this on change --%>
    local.phraseLookupAppend(phraseSelect, textArea, textAreaMaxLength);

  }
 
  <%-- todo: this is wrongly named!!!! it is Replace not append! fix naming --%>
  function setPhraseLookupFieldsAppend(phraseSelect, textpath, fieldName, textAreaMaxLength, textArea) {
    local.phraseLookupReplace(phraseSelect, textArea, textAreaMaxLength);
  }
   
  <%-- todo: get rid of this --%>
  function setPhraseParmFields(textpath, fieldName, maxLength) {
    <%--
    // Similar to setPhraseLookupFields but has no oid.  This method
    // added to support fill-in phrases
    --%>
    document.forms[0].SelectedPhrase.value = "";
    document.forms[0].TextPath.value = textpath; 
    document.forms[0].TextFieldName.value = fieldName;
    document.forms[0].TextMaxlength.value = maxLength;
  }

</SCRIPT>

<input type=hidden name=SelectedPhrase value="">
<input type=hidden name=TextPath value="">
<input type=hidden name=TextFieldName value="">
<input type=hidden name=TextMaxlength value="">
