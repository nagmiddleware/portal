<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
*******************************************************************************
    Air Waybill Release and Shipping Guarantee Issue  Page - Bank Instructions 
    section

  Description:
    Contains HTML to create the Air Waybill Release and Shipping Guarantee 
    Issue Bank Instructions section.

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transaction-AIR-REL_SHP-ISS-BankInstructions.jsp" %>
*******************************************************************************
--%>

	
      <%=widgetFactory.createLabel("","AWB_SGTEE.IssueInstrumentIn",isReadOnly,true,false,"") %>
        
      <%
      defaultText = resMgr.getText("transaction.SelectLanguage", TradePortalConstants.TEXT_BUNDLE);
          options = Dropdown.createSortedRefDataOptions(
                      TradePortalConstants.INSTRUMENT_LANGUAGE, 
                      instrument.getAttribute("language"), 
                      loginLocale);
         
          
          out.println(widgetFactory.createSelectField( "InstrumentLanguage", "", defaultText, options, isReadOnly, true, false, "", "", ""));
      %>
                                                              
      <%=widgetFactory.createWideSubsectionHeader("AWB_SGTEE.CommissionsCharges",isReadOnly,false,false,"")%>
          
      <%= widgetFactory.createTextField( "DebitOurAccountNumber", "AWB_SGTEE.DebitAccount", terms.getAttribute("coms_chrgs_our_account_num"), "30", isReadOnly) %>
        
      <%= widgetFactory.createTextField( "DebitForeignAccountNumber", "AWB_SGTEE.DebitFCA", terms.getAttribute("coms_chrgs_foreign_acct_num"), "30", isReadOnly) %>
                   
            
      <%
             options = Dropdown.createSortedCurrencyCodeOptions(terms.getAttribute("coms_chrgs_foreign_acct_curr"), 
                                                                loginLocale);

      %>
      <%= widgetFactory.createSelectField("DebitForeignCurrency", "AWB_SGTEE.FCACurrency", 
				" ", options, isReadOnly, false, false, "class='char5';", "", "") %>
		
		
      <%
              options = ListBox.createOptionList(spclInstrDocList, "PHRASE_OID", "NAME", "", userSession.getSecretKey());

              defaultText = resMgr.getText("transaction.AddAPhrase", TradePortalConstants.TEXT_BUNDLE);
                           
        %>   
        <span class="formItem"><%=widgetFactory.createSubLabel("AWB_SGTEE.AdditionalInstructions")%></span><br>
        <%=widgetFactory.createSelectField( "SpecialInstructionsPhraseItem", "", defaultText, options, isReadOnly, false, false, "onChange=" + 
                PhraseUtility.getPhraseChange("/Terms/special_bank_instructions",
                        "SpclBankInstructions", "1000","document.forms[0].SpclBankInstructions"), "", "") %>
      <%if(!isReadOnly){ %>                     
       <%=widgetFactory.createTextArea("SpclBankInstructions", "",
					terms.getAttribute("special_bank_instructions"),
					isReadOnly, false, false, "rows='10' cols='128'", "", "")%>
			<%=widgetFactory.createHoverHelp("SpclBankInstructions", "AirwayBill.AddInsToolTip") %>
	<%}else{ %>
	<%=widgetFactory.createAutoResizeTextArea("SpclBankInstructions", "",
					terms.getAttribute("special_bank_instructions"),
					isReadOnly, false, false, "maxlength=1000 style='width: 600px;min-height:140px;' cols='128'", "", "")%>
	<%}%>
