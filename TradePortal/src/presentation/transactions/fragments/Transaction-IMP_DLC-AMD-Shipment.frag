<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
*******************************************************************************
                 Import DLC Amend Page - Shipment section

  Description:
    Contains HTML to create the Import DLC Amend page.  All data retrieval 
  is handled in the Transaction-IMP_DLC-AMD.jsp.

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transaction-IMP_DLC-AMD-Shipment.frag" %>
*******************************************************************************

--%>
<%
boolean isStructuredPO = false;//ShilpaR Cr707
boolean hasStructuredPOs = false;
 
if(TradePortalConstants.PO_UPLOAD_STRUCTURED.equals(corporateOrg.getAttribute("po_upload_format")))
	isStructuredPO = true;
	  
%>
<div>
	<%=widgetFactory
					.createWideSubsectionHeader("ImportDLCAmend.ShipmentDetails")%>

	<div class="columnLeftNoBorder">
		<%-- Left Column Starts.... Shipment Details Sub Section --%>
		<%			
			if (isReadOnly) {				
		%>

		<%=widgetFactory.createDateField("ShipmentDate",
						"ImportDLCAmend.NewLastestShipDate", StringFunction.xssHtmlToChars(terms.getFirstShipment()
					.getAttribute("latest_shipment_date")),
						isReadOnly, false, isExpressTemplate, "class='char6'", dateWidgetOptions, "")%>
		<%
			} else {
		%>
		<%=widgetFactory.createDateField("ShipmentDate",
						"ImportDLCAmend.NewLastestShipDate",StringFunction.xssHtmlToChars(terms.getFirstShipment()
										.getAttribute("latest_shipment_date")),
						isReadOnly, false, isExpressTemplate, "class='char6'", dateWidgetOptions, "")%>
		<%
			}
		%>

		<%
			options = Dropdown.createSortedRefDataOptions(
					TradePortalConstants.INCOTERM, terms.getFirstShipment()
							.getAttribute("incoterm"), loginLocale);
			defaultText = " ";
		%>
		<%=widgetFactory.createSelectField("Incoterm",
					"ImportDLCAmend.NewShippingTerm", defaultText, options,
					isReadOnly, false, false, "class='char30'", "", "")%>
	</div>
	<%-- Left Column Ends.... Shipment Details Sub Section --%>
	<div class="columnRight">
		<%-- Right Column Starts.... Shipment Details Sub Section --%>
		<%=widgetFactory.createTextField("IncotermLocation",
					"ImportDLCAmend.NewShipTermLocation",
					terms.getFirstShipment().getAttribute("incoterm_location"),
					"30", isReadOnly, false, false, "", "", "")%>
	</div>
	<div style="clear:both;"></div>
	<%-- Right Column Ends.... Shipment Details Sub Section --%>
	<%--VishalSarkary Rel9.0 IR-23940 03/25/2014 [Start] --%>


	     <div class="columnLeftNoBorder">
	<span class="formItem">
	<%=widgetFactory.createStackedLabel("From", "ImportDLCIssue.From") %>
	</span>
	<%if(!isReadOnly){ %>
		<%=widgetFactory.createTextField("ShipFromPort",
					"ImportDLCAmend.NewFromPort", terms.getFirstShipment()
							.getAttribute("shipment_from"), "65", isReadOnly,
					false, false, "style='width:310px;'", "", "")%>
		<%}else{ %>
			<%= widgetFactory.createTextArea( "ShipFromPort", "ImportDLCAmend.NewFromPort", terms.getFirstShipment().getAttribute("shipment_from"), isReadOnly,false,false,"rows='2' cols='58'","","") %>
		<%} %>
		<%if(!isReadOnly){ %>
		<%=widgetFactory.createTextField("ShipFromLoad",
				"ImportDLCAmend.NewFromLoad", terms.getFirstShipment()
						.getAttribute("shipment_from_loading"), "65", isReadOnly,
				false, false, "style='width:310px;'", "", "")%>			
		<%}else{ %>
			<%= widgetFactory.createTextArea( "ShipFromLoad", "ImportDLCAmend.NewFromLoad", terms.getFirstShipment().getAttribute("shipment_from_loading"), isReadOnly,false,false,"rows='2' cols='58'","","") %>
		<%} %>
	</div>
	<div class="columnRight">
		<%-- Right Column Starts.... Shipment Details Sub Section --%>
		<span class="formItem">
		<%=widgetFactory.createStackedLabel("To", "ImportDLCIssue.To") %>
		</span>	
		<%if(!isReadOnly){ %>		
		<%=widgetFactory.createTextField("ShipToPort",
				"ImportDLCAmend.NewToPort", terms.getFirstShipment()
						.getAttribute("shipment_to"), "65",
				isReadOnly, false, false, "style='width:310px;'", "", "")%>
		<%}else{ %>
		<%= widgetFactory.createTextArea( "ShipToPort", "ImportDLCAmend.NewToPort", terms.getFirstShipment().getAttribute("shipment_to"), isReadOnly,false,false,"rows='2' cols='58'","","") %>
		<%} %>	
		<%if(!isReadOnly){ %>			
		<%=widgetFactory.createTextField("ShipToDischarge",
				"ImportDLCAmend.NewToDischarge", terms.getFirstShipment()
						.getAttribute("shipment_to_discharge"), "65",
				isReadOnly, false, false, "style='width:310px;'", "", "")%>
		<%}else{ %>
			<%= widgetFactory.createTextArea( "ShipToDischarge", "ImportDLCAmend.NewToDischarge", terms.getFirstShipment().getAttribute("shipment_to_discharge"), isReadOnly,false,false,"rows='2' cols='58'","","") %>
		<%} %>		
	</div>
  <div style="clear:both;"></div>
     
       <%-- VishalSarkary Rel9.0 IR-25411 03/25/2014 [End] --%>
	
</div>
<div>
	<%=widgetFactory
					.createWideSubsectionHeader("ImportDLCAmend.NewGoodsDescription")%>
	<div>
		<%
			if (isReadOnly) { %>
				<div class="indent"><%=widgetFactory.createNote("common.RemindPOChanged")%></div>
			<%} else {
		%>
		<%
			options = ListBox.createOptionList(goodsDocList, "PHRASE_OID",
						"NAME", "", userSession.getSecretKey());
				defaultText = resMgr.getText("transaction.SelectPhrase",
						TradePortalConstants.TEXT_BUNDLE);
		%>
		<div class="inline">
		<%=widgetFactory.createSelectField(
						"GoodsPhraseItem",
						"",
						defaultText,
						options,
						isReadOnly,
						false,
						false,
						"onChange="
								+ PhraseUtility
										.getPhraseChange(
												"/Terms/ShipmentTermsList/goods_description",
												"GoodsDescText", textAreaMaxlen,
												"document.forms[0].GoodsDescText"),
						"", "inline")%>
		</div>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=widgetFactory.createNote("ImportDLCAmend.RemindPOChanged")%>
		<%
			}
		%>
		
		<%if(!isReadOnly){ %>
		<%=widgetFactory.createTextArea("GoodsDescText", "", terms
					.getFirstShipment().getAttribute("goods_description").toString(),
					isReadOnly, false, false, "rows='10' cols='128'", "", "","")%>
		<%=widgetFactory.createHoverHelp("GoodsDescText","GoodsDescText") %>		
		<%}else{%>
				<%= widgetFactory.createAutoResizeTextArea( "GoodsDescText", "", terms
						.getFirstShipment().getAttribute("goods_description").toString(), isReadOnly,false,false, "style='width:600px;min-height:140px;' cols='100'","",textAreaMaxlen) %>
		<%}%>
		
		<%
			userOrgAutoLCCreateIndicator = userSession
					.getOrgAutoLCCreateIndicator();
			transactionOid = transaction.getAttribute("transaction_oid");

			if (((userOrgAutoLCCreateIndicator != null) && (userOrgAutoLCCreateIndicator
					.equals(TradePortalConstants.INDICATOR_YES)))
					&& ((SecurityAccess.hasRights(loginRights,
							SecurityAccess.DLC_PROCESS_PO)) || (userSession
							.hasSavedUserSession() && (userSession
							.getSavedUserSessionSecurityType()
							.equals(TradePortalConstants.ADMIN))))) {
				// Compose the SQL to retrieve PO Line Items that are currently assigned to 
				// this transaction
				sqlQuery = new StringBuffer();
				if(!isStructuredPO){
				sqlQuery.append("select a_source_upload_definition_oid, ben_name, currency");
				sqlQuery.append(" from po_line_item where a_assigned_to_trans_oid = ?");
				//jgadela  R90 IR T36000026319 - SQL FIX
				// Retrieve any PO line items that satisfy the SQL query that was just constructed
				poLineItemsDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery.toString(), false, new Object[]{transactionOid});

				if (poLineItemsDoc != null) {
					hasPOLineItems = true;

					poLineItems = poLineItemsDoc
							.getFragments("/ResultSetRecord");
					poLineItemDoc = (DocumentHandler) poLineItems.elementAt(0);
					poLineItemUploadDefinitionOid = poLineItemDoc
							.getAttribute("/A_SOURCE_UPLOAD_DEFINITION_OID");
					poLineItemBeneficiaryName = poLineItemDoc
							.getAttribute("/BEN_NAME");
					poLineItemCurrency = poLineItemDoc
							.getAttribute("/CURRENCY");
				}
				}else {
	  				   sqlQuery.append("select a_upload_definition_oid, seller_name, currency");
	                   sqlQuery.append(" from purchase_order where a_transaction_oid = ?");	 
	                   //jgadela  R90 IR T36000026319 - SQL FIX
	                   poLineItemsDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery.toString(), false, new Object[]{transactionOid});
	  				 
	                   if (poLineItemsDoc != null)
	                     {
	                  	 hasStructuredPOs = true;
	                  	 poLineItems                   = poLineItemsDoc.getFragments("/ResultSetRecord");
	                       poLineItemDoc                 = (DocumentHandler) poLineItems.elementAt(0);
	                       poLineItemUploadDefinitionOid = poLineItemDoc.getAttribute("/A_UPLOAD_DEFINITION_OID");
	                       poLineItemBeneficiaryName     = poLineItemDoc.getAttribute("/SELLER_NAME");
	                       poLineItemCurrency            = poLineItemDoc.getAttribute("/CURRENCY");
	                      }
	          	     }
				if (!hasPOLineItems && (!(isReadOnly || isFromExpress)) &&  !isStructuredPO) {
		%>

		<div class="formItem">
		<button data-dojo-type="dijit.form.Button"  name="AddPOLineItems" id="AddPOLineItems" type="submit">
	    <%=resMgr.getText("AddImpAmdPOLineItems.Header",TradePortalConstants.TEXT_BUNDLE)%>
		    <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
		         setButtonPressed('AddPOLineItems', '0');
		         document.forms[0].submit();
		    </script>
	    </button>
		</div>
		<%
			// Include currency code so that POs will be filtered
					secureParms.put("addPO-currency", currencyCode);
		%>

		<%
			}
				else if (!hasStructuredPOs && (!(isReadOnly || isFromExpress)) && isStructuredPO) 
				  {
	        %>
			        <div class="formItem">
			        <button data-dojo-type="dijit.form.Button"  name="AddStructuredPOButton" type="button" id="AddStructuredPOButton" onclick="AddPOStructure();">
		            <%=resMgr.getText("common.AddStructuredPOText",TradePortalConstants.TEXT_BUNDLE)%>		            
		         </button>
						</div>
					                 <%
	                               // Include currency code so that POs will be filtered
	                               secureParms.put("addPO-currency", currencyCode);
	                          %>
	            <%
	              }
			}
		%>

		<%
			// It is possible that after a transaction has been processed by bank that it has
			// no POs assigned to it because these POs have been moved to amendments.  However,
			// we still want to display the PO information in the PO line items field.
			if (transaction.getAttribute("transaction_status").equals(
					TradePortalConstants.TRANS_STATUS_PROCESSED_BY_BANK)
					&& !InstrumentServices.isBlank(terms.getFirstShipment()
							.getAttribute("po_line_items")))
				hasPOLineItems = true;

			if (((userOrgAutoLCCreateIndicator != null) && (userOrgAutoLCCreateIndicator
					.equals(TradePortalConstants.INDICATOR_YES)))
					&& hasPOLineItems
					|| (userSession.isEnableAdminUpdateInd() && userSession.getSecurityType().equals(TradePortalConstants.ADMIN))) //Added condition of ADMIN so that Admin can view POs from Update Centre - Rel9.3.5 IR#45181
					{
		%>
		<%-- <%=widgetFactory
						.createTextArea(
								"POLineItems",
								"ImportDLCAmend.POLineItems",
								terms.getFirstShipment().getAttribute(
										"po_line_items"),
								isReadOnly,
								false,
								false,
								"style=\"'width:auto;' rows='10' cols='110'\" onFocus='this.blur();' oncontextmenu='return false;' onselectstart='return false;'",
								"", "")%> --%>
<%if(!isReadOnly){%>
		<div class="formItem">
		<%--PR ER-T36000015639 BEGIN. Changes done to enable right click, text highlighting, Copy options.--%>
			<%//widgetFactory.createPOTextArea("POLineItems", terms.getFirstShipment().getAttribute("po_line_items"), "78", "10","POLineItems", isReadOnly, "onFocus='this.blur();' oncontextmenu='return false;' onselectstart='return false;'", "OFF",true)%>
			<%=widgetFactory.createPOTextArea("POLineItems", terms.getFirstShipment().getAttribute(
				"po_line_items"), "78", "10", 
	        "POLineItems", isReadOnly, "", "OFF",true)%>
		<%--PR ER-T36000015639 END. Changes done to enable right click, text highlighting, Copy options.--%>	
		</div>
		<%--IR T36000023557 start--%>
<%}else{%>
        <%=widgetFactory.createAutoResizeTextArea("POLineItems","", terms.getFirstShipment().getAttribute("po_line_items"), isReadOnly, false, false, "style='width:600px;min-height:140px;' cols='100'","","")%>
        <%}%> 
        <%--IR T36000023557 end--%>
		<%--=InputField.createTextArea("POLineItems", terms.getFirstShipment().getAttribute("po_line_items"), "65", "6", 
                             "FixedSize", isReadOnly, true, "onFocus='this.blur();' oncontextmenu='return false;' onselectstart='return false;'", "OFF")--%>

		<%
			if ((SecurityAccess.hasRights(loginRights,
						SecurityAccess.DLC_PROCESS_PO))
						&& (!(isReadOnly || isFromExpress))) {
					secureParms.put("addPO-uploadDefinitionOid",
							poLineItemUploadDefinitionOid);
					secureParms.put("addPO-beneficiaryName",
							poLineItemBeneficiaryName);
					secureParms.put("addPO-currency", poLineItemCurrency);
					secureParms.put("addPO-shipment_oid", terms
							.getFirstShipment().getAttribute("shipment_oid"));
		%>



		<div class="formItem">
		<button data-dojo-type="dijit.form.Button"  name="AddPOLineItems" id="AddPOLineItems" type="submit">
	    <%=resMgr.getText("AddImpAmdPOLineItems.Header",TradePortalConstants.TEXT_BUNDLE)%>
		    <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
		         setButtonPressed('AddPOLineItems', '0');
		         document.forms[0].submit();
		    </script>
	    </button>
	    
	    <button data-dojo-type="dijit.form.Button"  name="RemovePOLineItemsButton" id="RemovePOLineItemsButton" type="submit">
	    <%=resMgr.getText("common.RemovePOLineItemsText",TradePortalConstants.TEXT_BUNDLE)%>
		    <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
		         setButtonPressed('RemovePOLineItemsButton', '0');
		         document.forms[0].submit();
		    </script>
	    </button>
	    </div>
    
		<%
			}
		%>
		<%
			}
		%>
		<%
		  // =================================== start of Structured PO Assigned=================================== 

		if (TradePortalConstants.INDICATOR_YES.equals(userOrgAutoLCCreateIndicator) && isStructuredPO && hasStructuredPOs)
		{
		       secureParms.put("addPO-uploadDefinitionOid", poLineItemUploadDefinitionOid);
		       secureParms.put("addPO-beneficiaryName", poLineItemBeneficiaryName);
		       secureParms.put("addPO-currency", poLineItemCurrency);
		      secureParms.put("addPO-shipment_oid",  terms.getFirstShipment().getAttribute("shipment_oid"));
		      secureParms.put("addPO-hasStructuredPO", String.valueOf(hasStructuredPOs));
		%>
		         
				<div class = "formItem">    
				<%-- DK IR T36000018062 Rel8.2 06/14/2013 starts --%>                 
				<button data-dojo-type="dijit.form.Button"  name="ViewStructuredPOButton" id="ViewStructuredPOButton" onclick="ViewPOStructure();" type="button">
						 <%=resMgr.getText("common.ViewStructuredPOText",TradePortalConstants.TEXT_BUNDLE)%>		 		 
				</button>
		     <%
		                    if ((SecurityAccess.hasRights(loginRights, SecurityAccess.DLC_PROCESS_PO)) && 
		                         (!(isReadOnly || isFromExpress)))
		                     {

		                  %>
		                  <button data-dojo-type="dijit.form.Button"  name="AddMoreStructuredPOButton" id="AddMoreStructuredPOButton" onclick="AddPOStructure();"  type="button">
		 		 		 <%=resMgr.getText("common.AddMoreStructuredPOText",TradePortalConstants.TEXT_BUNDLE)%>
		 		 		 </button>
		             
			             <button data-dojo-type="dijit.form.Button"  name="RemoveStructuredPOButton" id="RemoveStructuredPOButton" onclick="RemovePOStructure();" type="button">
			 		 		 <%=resMgr.getText("common.RemoveStructuredPOText",TradePortalConstants.TEXT_BUNDLE)%>
			 		    </button>
			 		    <%-- DK IR T36000018062 Rel8.2 06/14/2013 ends --%>  
		     <%
		                   }
		                  %>
		        </div>          

		<%
		}
		%>
	</div>
</div>
