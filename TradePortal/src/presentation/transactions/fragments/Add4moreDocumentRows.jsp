<%-- Leelavathi - 10thDec2012 - Rel8200 CR-737 -Initial Version --%>
<%--for the ajax include--%>

<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.html.*" %>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager"
   scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager"
   scope="session">
</jsp:useBean>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager"
   scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean"
   scope="session">
</jsp:useBean>


<% 
//Initialize WidgetFactory.
WidgetFactory widgetFactory = new WidgetFactory(resMgr); 

int tableIndex = 0;

String parmValue = request.getParameter("additionalReqDocIndex");
boolean isReadOnly = Boolean.parseBoolean(request.getParameter("isReadOnly"));
boolean isFromExpress = Boolean.parseBoolean(request.getParameter("isFromExpress"));
String defaultText1 = "";
String options="";
DocumentHandler reqdDocList=null;
String loginLocale = userSession.getUserLocale();
if ( parmValue != null ) {
	try {
		tableIndex = Integer.parseInt(parmValue);
	}
	catch (Exception ex ) {
	}
}

//when included per ajax, the business objects will be blank
 AdditionalReqDocWebBean additionalReqDoc=beanMgr.createBean(AdditionalReqDocWebBean.class,"AdditionalReqDoc");
 
%>
<tr>
	<td><%=widgetFactory.createCheckboxField("AdditionalReqDocInd"+tableIndex, "",TradePortalConstants.INDICATOR_YES.equals(additionalReqDoc.getAttribute("addl_req_doc_ind")), isReadOnly || isFromExpress, isFromExpress, "", "", "none" ) %></td>
	<td><%= widgetFactory.createTextField( "AdditionalReqDocName"+tableIndex, "", additionalReqDoc.getAttribute("addl_req_doc_name"), "30", isReadOnly || isFromExpress, false, false,  "", "","none" ) %></td>
	<td><%= widgetFactory.createNumberField( "AdditionalReqDocOriginals"+tableIndex, "", additionalReqDoc.getAttribute("addl_req_doc_originals"), "3", isReadOnly || isFromExpress, false, false,  "", "","none" ) %></td>
	<td><%= widgetFactory.createNumberField( "AdditionalReqDocCopies"+tableIndex, "",additionalReqDoc.getAttribute("addl_req_doc_copies"), "3", isReadOnly || isFromExpress, false, false,  "", "","none" ) %></td>
	<td>
	<%
		if (isReadOnly || isFromExpress) {
	      out.println("&nbsp;");
	    } else {
	    	DocumentHandler phraseLists = formMgr.getFromDocCache(TradePortalConstants.PHRASE_LIST_DOC);
			if (phraseLists == null) phraseLists = new DocumentHandler();

		  reqdDocList = phraseLists.getFragment("/Required");
	      options = ListBox.createOptionList(reqdDocList, "PHRASE_OID", "NAME", "");
	      defaultText1 = resMgr.getText("transaction.SelectPhrase",loginLocale);
	    }  
    %> 
	<%= widgetFactory.createSelectField( "AdditionalReqDocPhraseItem"+tableIndex, "", defaultText1, options, false, false, false, "", "", "") %>
    <%= widgetFactory.createTextArea( "AdditionalReqDocText"+tableIndex, "", additionalReqDoc.getAttribute("addl_req_doc_text"), false,false, false, "", "","" ) %>
	</td>
	</tr>
	<%
  			String additionalReqDocOid = additionalReqDoc.getAttribute("addl_req_doc_oid");
			String tabIndexEncode = StringFunction.xssCharsToHtml(Integer.toString(tableIndex));
  			out.print("<INPUT TYPE=HIDDEN NAME='AdditionalReqDocOid"+tabIndexEncode+"' VALUE='" +additionalReqDocOid+ "'>"); 
		%>