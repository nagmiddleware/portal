<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 *  Vasavi CR 524 03/31/2010
--%>
<%--
*******************************************************************************
                 Export Collection Trace Page - all sections

  Description:
    Contains HTML to create the Export Collection Trace page.  All data retrieval 
  is handled in the Transaction-EXP_OCO-TRC.jsp.

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transaction-EXP_OCO-TRC-html.jsp" %>
*******************************************************************************
--%>
<div>
	
	<div class="inline formItem readOnly" >
		<label for="exportRefNoLabel"><%=resMgr.getText("ExportCollectionAmend.YourRefNo", TradePortalConstants.TEXT_BUNDLE)%></label><br/>
		<span class="fieldValue"><%= (terms.getAttribute("reference_number")==null
				||terms.getAttribute("reference_number").trim().equals(""))?"&nbsp;":terms.getAttribute("reference_number") %></span>
	</div>
	<div class="inline formItem readOnly" >
		<label for="exportDrawNameLabel"><%=resMgr.getText("ExportCollectionAmend.DrawerName", TradePortalConstants.TEXT_BUNDLE)%></label><br/>
		<span class="fieldValue"><%= (drawerName==null||drawerName.trim().equals(""))?"&nbsp;":drawerName %></span>
	</div>	
	<div style="clear: both;"></div>
	<%
            if (!isProcessedByBank) {
      %>
  
	<%String tempCollAmt = TPCurrencyUtility.getDisplayAmount(originalAmount.toString(), currencyCode,loginLocale); %>
	<div class="formItem inline readOnly">
		<label for="exportColAmtLabel"><%=resMgr.getText("ExportCollectionAmend.CollectionAmount", TradePortalConstants.TEXT_BUNDLE)%></label><br>
			<%=currencyCode%>
    	<span class="fieldValue"><%= (tempCollAmt==null||tempCollAmt.trim().equals(""))?"&nbsp;":tempCollAmt %></span>
    </div>
	 
		<div style="clear: both;"></div>
      <%
            }
      %>
      
      <div class="formItem readOnly">
      	<div class="inline">
      		<label for="exportNewAmtLabel"><%=resMgr.getText("ExportCollectionAmend.NewAmount", TradePortalConstants.TEXT_BUNDLE)%></label><br>
      			<%=currencyCode%>
	      <%= widgetFactory.createTextField( "TransactionAmount", "", displayAmount, "30", isReadOnly, false,false, "class='char8'", "", "none") %>                      
	    </div>
	   </div>
     <br/>
      <%=widgetFactory.createTextArea("AmendmentDetails", "",
        		terms.getAttribute("amendment_details"), isReadOnly, false, false, "rows='10'", "", "")%> 
      <%if(!isReadOnly) {%>
      	<%=widgetFactory.createHoverHelp("AmendmentDetails", "DirectDebitCollection.AdditionalToolTip") %>
      <%} %>
</div>