<%--
*******************************************************************************
                  Instrument Search Advanced Filter include file

  Description:
    This is a JSP meant to be included using the <%@ include file="x.jsp" %> 
  tag.  It creates the HTML for displaying the Advanced Filter fields for the 
  Instrument Search.  As it is included with <%@ include %> rather than with
  the <jsp:include> directive, it is not a standalone servlet.  
  
  IMPORTANT NOTE: This page may currently come from the following:
  
  Create Transaction Step 1 Page: for an existing instrument or copying an
  			instrument
  Export LC Issue Transfer: Search Instrument button
  MessageHome: Search Instrument
  
  Each originating page should contain the following code:
  
    <input type="hidden" name="NewSearch" value="Y">
  
  - this will allow a new search criteria to be created.

  NOTE: Since this JSP is copied into the including page at compile time of
  the including page, changes to this page do not automatically force the
  including pages to be recompiled.  If you make changes to this page, you must
  delete the including page's servlet in order to see the changes.

*******************************************************************************
--%>
<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<div id="advancedInstrumentFilter" style="display:none;"> <%--default advanced to non-display--%>
  <div class="searchDetail">
    <span class="searchCriteria">

<%
		if (showInstrumentDropDown)
		{
			// Build the option tags for the dropdown box.
			options = Dropdown.getInstrumentList( instrumentType,
		                                          loginLocale, instrumentTypes );
		%>
		<%-- Kiran  05/07/2013  Rel8.2 IR-T36000014861  Start --%>
		<%-- changed event to window.event to all the fields in order for enter key to work in Firefox --%>
		<%=widgetFactory.createSearchSelectField("InstrumentTypeAdvance","InstSearch.InstrumentType"," ", options, 
				" class='char11' onKeydown='filterOnEnter(window.event, \"InstrumentTypeAdvance\",\"Advanced\");'")%>
		<%-- Kiran  05/07/2013  Rel8.2 IR-T36000014861  End --%>
		
		<%	
		}
		else
		{%>
			<%=widgetFactory.createSubLabel("InstSearch.InstrumentType")%>
			 <b> <%=resMgr.getText("CorpCust.ExportLC", TradePortalConstants.TEXT_BUNDLE)%> </b>
		 
		    <input type ="hidden" name="InstrumentType" value="<%=InstrumentType.EXPORT_DLC%>" >
		<%	}	%>
		
	    <% options = Dropdown.createSortedRefDataOptions(TradePortalConstants.CURRENCY_CODE, currency, loginLocale); %>
	    <%-- Kiran  05/07/2013  Rel8.2 IR-T36000014861  Start --%>
		<%=widgetFactory.createSearchSelectField("Currency","InstSearch.Currency"," ", options, " class='char22em' onKeydown='filterOnEnter(window.event, \"Currency\",\"Advanced\");'")%>		
		<%=widgetFactory.createSearchAmountField("AmountFrom","InstSearch.AmountFrom","", " class='char7em' onKeydown='filterOnEnter(window.event, \"AmountFrom\",\"Advanced\");'")%>
		<%=widgetFactory.createSearchAmountField("AmountTo","InstSearch.To","", " class='char7em' onKeydown='filterOnEnter(window.event, \"AmountTo\",\"Advanced\");'")%>
		<%-- Kiran  05/07/2013  Rel8.2 IR-T36000014861  End --%>
    </span>
    <span class="searchActions">
      <button id="advanceSearchId" class="gridSearchButton" data-dojo-type="dijit.form.Button" type="button" >
        <%= resMgr.getText("common.searchButton",TradePortalConstants.TEXT_BUNDLE) %>
        <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
          local.searchInstruments("Advanced");return false;
        </script>
      </button>

      <%-- <a class="searchTypeToggle" href="<%=linkHref%>" ><%=linkName%></a>--%>
      <div id="basic"><a class="searchTypeToggle" href="javascript:local.shuffleFilter('Basic');"><%=resMgr.getText("ExistingDirectDebitSearch.Basic", TradePortalConstants.TEXT_BUNDLE)%></a></div>
    </span>
     <%=widgetFactory.createHoverHelp("advanceSearchId","SearchHoverText") %>
     <%=widgetFactory.createHoverHelp("basic","BasicSearchHypertextLinkHoverText") %>
    <div style="clear:both"></div>
   																													
		<%=widgetFactory.createSearchDateField("DateFrom","InstSearch.ExpiryDateFrom", " class='char8'", "constraints:{datePattern:'"+datePattern+"'},placeHolder:'"+datePatternDisplay+"'")%>
		<%=widgetFactory.createSearchDateField("DateTo","InstSearch.To", " class='char8' ", "constraints:{datePattern:'"+datePattern+"'},placeHolder:'"+datePatternDisplay+"'")%>
		<%-- Kiran  05/07/2013  Rel8.2 IR-T36000014861  Start --%>
		<%=widgetFactory.createSearchTextField("OtherParty","InstSearch.Party","30", " class='char15' onKeydown='filterOnEnter(window.event, \"OtherParty\",\"Advanced\");'")%>
		<%=widgetFactory.createSearchTextField("VendorIdAdvance","InstSearch.VendorId","15", " class='char9' onKeydown='filterOnEnter(window.event, \"VendorIdAdvance\",\"Advanced\");'")%>						
    	<%-- Kiran  05/07/2013  Rel8.2 IR-T36000014861  Start --%>
    	<%-- DK CR-886 Rel8.4 10/15/2013 starts --%>
    	<%=widgetFactory.createSearchDateField("StartDateFrom","InstSearch.StartDateFrom", " class='char8'", "constraints:{datePattern:'"+datePattern+"'},placeHolder:'"+datePatternDisplay+"'")%>
    	<%=widgetFactory.createSearchDateField("StartDateTo","InstSearch.To", " class='char8'", "constraints:{datePattern:'"+datePattern+"'},placeHolder:'"+datePatternDisplay+"'")%>
    	<%-- DK CR-886 Rel8.4 10/15/2013 ends --%>
    	<%-- Kyriba CR 268 --%> 
    	<div style="clear:both"></div>
    <%	
    CorporateOrganization corpOrgObj = (CorporateOrganization)EJBObjectFactory.createClientEJB(formMgr.getServerLocation(), "CorporateOrganization");
    corpOrgObj.getData(Long.valueOf(userOrgOid).longValue());
    ComponentList externalBankComponenet = (ComponentList) corpOrgObj.getComponentHandle("ExternalBankList");
    int extBankTotal = externalBankComponenet.getObjectCount();

    BusinessObject externalBankBusinessObj = null;
    String externalBankBranches = "";
    String externalBankOid = null;
    DocumentHandler extBankDoc = null;
	//jgadela  R90 IR T36000026319 - SQL FIX
    java.util.List<Object> sqlParamsLst = new java.util.ArrayList();
    String externalBankBranchesWildCard = "";

    for(int i=0; i<extBankTotal; i++){
    	
    	externalBankComponenet.scrollToObjectByIndex(i);
    	externalBankBusinessObj = externalBankComponenet.getBusinessObject();
    	externalBankOid = externalBankBusinessObj.getAttribute("op_bank_org_oid");		
    	  
    	      if (!InstrumentServices.isBlank(externalBankOid)) {
    	          if ( externalBankBranchesWildCard.length()>0 ) {
    	        	  //externalBankBranches += ",";
    	        	  externalBankBranchesWildCard += ",";
    	          }
    	          //externalBankBranches += "'" + externalBankOid + "'";
    	          externalBankBranchesWildCard += "?";
    	          sqlParamsLst.add(externalBankOid);
    	          
    	      }
    	      else {
    	          break; //if a blank is found there will be no more
    	      }		
    }
    //Build query to get all external bank's 
    try {
        StringBuffer extBanksql = new StringBuffer();
        extBanksql.append("select ORGANIZATION_OID, NAME");
        extBanksql.append(" from OPERATIONAL_BANK_ORG");
        extBanksql.append(" where ORGANIZATION_OID in (").append(externalBankBranchesWildCard).append(")");
        extBanksql.append(" order by name");

       //Srinivasu_D Rel9.2 12/01/2014 - sql issue fix ,found in 91 UAT logs
	if(StringFunction.isNotBlank(externalBankBranchesWildCard)){
        extBankDoc = DatabaseQueryBean.getXmlResultSet(extBanksql.toString(), true, sqlParamsLst);
        }
        

      } catch (AmsException e) {
        //todo: should throw an error
        Debug.debug("InstSearch-Advance:Exception in Getting External Bank");
        e.printStackTrace();
      }

    
    options = ListBox.createOptionList(extBankDoc, "ORGANIZATION_OID", "NAME", "", null);
    // Suresh_L  05/23/2014 Rel8.4  IR-T36000027647 -start 
  out.print(widgetFactory.createSearchSelectField( "ExternalBankDropDownId", "InstSearch.BankName", " ", options, ""));
    // Suresh_L  05/23/2014 Rel8.4  IR-T36000027647  -end 
  
  %>
    	<div style="clear:both"></div>
</div>
</div>
