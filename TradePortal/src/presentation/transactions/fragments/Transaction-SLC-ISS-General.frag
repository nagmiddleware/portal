<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
*******************************************************************************
                     Standby LC Issue Page - General section

  Description:
    Contains HTML to create the Standby LC Issue General section.  

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transaction-SLC-ISS-General.jsp" %>
*******************************************************************************
--%>

		<%
		  // Get some attributes for radio button fields.  We need to refer to this
		  // value frequently so do getAttribute once.
		
		  String pmtTermsType = terms.getAttribute("pmt_terms_type");
		  String bankChargesType = terms.getAttribute("bank_charges_type");
		  
		  String deliverBy = terms.getAttribute("guar_deliver_by");
		  String deliverTo = terms.getAttribute("guar_deliver_to");
		
		  String amount = terms.getAttribute("amount");
		  String currency = terms.getAttribute("amount_currency_code");
		  String displayAmount;
		  
		  String issueType = terms.getAttribute("guarantee_issue_type");//IR-PAUK040245609 SWIFT Upgrade
		
		  if(!getDataFromDoc)
		     displayAmount = TPCurrencyUtility.getDisplayAmount(amount, currency, loginLocale);
		  else 
		     displayAmount = amount;
		    /* if(!isReadOnly)
		         displayAmount = amount;
		      else
		    	  displayAmount = TPCurrencyUtility.getDisplayAmount(amount.toString(), currency, loginLocale);
		    */	  
		%>
		
		<div class="columnLeft">	
		<%--
		   -==========================================
		   -============== Beneficiary ===============
		   -==========================================
		--%>
			<div style="display:none">				
		        <input type=hidden name='BenTermsPartyType' value=<%=TradePortalConstants.BENEFICIARY%>>
		        <input type=hidden name="BenOTLCustomerId" value="<%=termsPartyBen.getAttribute("OTL_customer_id")%>">
		        <input type=hidden name="VendorId" value="<%=termsPartyBen.getAttribute("vendor_id")%>">
		        <input type=hidden name="BenAddressLine3" value="<%=termsPartyBen.getAttribute("address_line_3")%>">
		        </div>
			<%
		        if (isFromExpress) {
		          // We are in a transaction created from express.  The beneficiary 
		          // fields are express fields.  This means they display as readonly 
		          // when the user is editing an instrument created from an express 
		          // template.  However, we still need to send the ben data to the 
		          // mediator.  Put the ben data in a bunch of hidden fields to 
		          // ensure this happens.
			%>
				  <div style="display:none">
		          <input type=hidden name="BenName" value="<%=termsPartyBen.getAttribute("name")%>">
		          <input type=hidden name="BenPhoneNumber" value="<%=termsPartyBen.getAttribute("phone_number")%>">
		          <input type=hidden name="BenAddressLine1" value="<%=termsPartyBen.getAttribute("address_line_1")%>">
		          <input type=hidden name="BenAddressLine2" value="<%=termsPartyBen.getAttribute("address_line_2")%>">
		          <input type=hidden name="BenCity" value="<%=termsPartyBen.getAttribute("address_city")%>">
		          <input type=hidden name="BenStateProvince" value="<%=termsPartyBen.getAttribute("address_state_province")%>">
		          <input type=hidden name="BenCountry" value="<%=termsPartyBen.getAttribute("address_country")%>">
		          <input type=hidden name="BenPostalCode" value="<%=termsPartyBen.getAttribute("address_postal_code")%>">
		          </div>
			<%
			      }
			%>
			
			<% 
			//KMehta - 04 Aug 2014 - Rel9.1 IR-T36000029273 - Change  - Begin
			String itemid1="BenName,BenAddressLine1,BenAddressLine2,BenCity,BenStateProvince,BenPostalCode,BenCountry,BenPhoneNumber,BenOTLCustomerId,cor_terms_party_oid,CorName,CorAddressLine1,CorAddressLine2,CorAddressLine3,CorCity,CorStateProvince,CorCountry,CorPostalCode,CorrespondentBank,CorOTLCustomerId";
			String section1="ben_slc_iss";
			//KMehta - 04 Aug 2014 - Rel9.1 IR-T36000029273 - Change  - End
			 %>
		    <%
			    			
				// populate the secure parms and hidden fields that are
				// necessary for the save and verify process, since these
				// information is not populated in any displayable input field
				// and is not set anywhere
				
			    secureParms.put("ben_terms_party_oid", termsPartyBen.getAttribute("terms_party_oid"));
			    String benSearchHtml = widgetFactory.createPartySearchButton(itemid1,section1,false,TradePortalConstants.BENEFICIARY,false);
			%>
			<%= widgetFactory.createSubsectionHeader("SLCIssue.Beneficiary",isReadOnly, false, false, benSearchHtml) %>
			
			<%= widgetFactory.createTextField("BenName", "SLCIssue.BeneficiaryName", termsPartyBen.getAttribute("name").toString(), "35", 
				isReadOnly || isFromExpress, !isTemplate, isExpressTemplate, "onBlur='checkBeneficiaryName(\"" + StringFunction.escapeQuotesforJS(termsPartyBen.getAttribute("name")) + "\")'", "","" ) %>
	     	<%= widgetFactory.createTextField("BenAddressLine1", "PartyDetail.AddressLine1", termsPartyBen.getAttribute("address_line_1"), "35", 
	     		isReadOnly || isFromExpress, !isTemplate, isExpressTemplate, "", "","") %>
	     	<%= widgetFactory.createTextField("BenAddressLine2", "PartyDetail.AddressLine2", termsPartyBen.getAttribute("address_line_2"), "35", 
	     		isReadOnly || isFromExpress, false, isExpressTemplate, "", "","") %>
	     	<%= widgetFactory.createTextField("BenCity", "PartyDetail.City", termsPartyBen.getAttribute("address_city"), "23", 
	     		isReadOnly || isFromExpress, !isTemplate, isExpressTemplate, "class='char35'", "","") %>
	     			    	
	    	<div>
		    	<%= widgetFactory.createTextField("BenStateProvince", "PartyDetail.ProvinceState", termsPartyBen.getAttribute("address_state_province"), "8", 
		     		isReadOnly || isFromExpress, false, isExpressTemplate, "", "","inline") %>
			    <%= widgetFactory.createTextField("BenPostalCode", "PartyDetail.PostalCode", termsPartyBen.getAttribute("address_postal_code"), "15", 
			    	isReadOnly || isFromExpress, false, isExpressTemplate, "", "","inline") %>
			    <div style="clear: both;"></div>
			</div>
			<%
      			options = Dropdown.createSortedRefDataOptions(TradePortalConstants.COUNTRY, termsPartyBen.getAttribute("address_country"), loginLocale);
            %>
		    <%= widgetFactory.createSelectField("BenCountry", "PartyDetail.Country", " ", options, isReadOnly || isFromExpress, !isTemplate, isExpressTemplate, "class='char35'", "", "") %>
		    
		    <%= widgetFactory.createTextField("BenPhoneNumber", "SLCIssue.PhoneNumber", termsPartyBen.getAttribute("phone_number"), "20", 
		     	isReadOnly || isFromExpress, false, isExpressTemplate, "","regExp:'[0-9]+'","") %> 

		<%--
		   -==========================================
		   -================ Applicant ===============
		   -==========================================
		--%>		
			<%
				String applicantSearchHtml = "";
				String applicantClearHtml = "";
				String itemid2="app_terms_party_oid,AppName,AppAddressLine1,AppAddressLine2,AppCity,AppStateProvince,AppCountry,AppPostalCode,AppPhoneNumber,Applicant,AppOTLCustomerId";
				String section2="applicant_slc";
				if (!(isReadOnly || isFromExpress)) {
					applicantSearchHtml = widgetFactory.createPartySearchButton(itemid2,section2,isReadOnly,TradePortalConstants.APPLICANT,false);
					
					if (!isReadOnly && isTemplate) {
						applicantClearHtml = widgetFactory.createPartyClearButton("ClearApplicantButton", "clearApplicant()",isReadOnly,"");
					}	
				}
				%>
				 <%= widgetFactory.createSubsectionHeader("SLCIssue.Applicant",isReadOnly, !isTemplate, false, (applicantSearchHtml+applicantClearHtml)) %>
				 
				 <%
				 	String addressSearchIndicator = null;
				    addressSearchIndicator = termsPartyApp.getAttribute("address_search_indicator");
					if (!(isReadOnly || isFromExpress) && !addressSearchIndicator.equals("N"))
				    {
				    	if(!isReadOnly && corpOrgHasMultipleAddresses)
				    	{ %>
				    	<div class = "formItem">
				    	   <button data-dojo-type="dijit.form.Button"  name="AddressSearch" id="AddressSearch" type="button">
		                   <%=resMgr.getText("common.searchAddressButton",TradePortalConstants.TEXT_BUNDLE)%>
		                   <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
			                    var corpOrgOid = <%=corpOrgOid%>;
								require(["t360/common", "t360/partySearch"], function(common, partySearch){
									partySearch.setPartySearchFields('<%=TradePortalConstants.APPLICANT%>');
				       	  			common.openCorpCustAddressSearchDialog(corpOrgOid);
				       	  		});
		                   </script>
		                   </button>

						 </div>  
				    	<% 
				    	}
				    }
					%>
				
			
			 <% String applicantAddress = termsPartyApp.buildAddress(false, resMgr); %>
			  <%-- Srinivasu_D  T36000019515 Rel8.4 01/23/2014 - start - changing the textarea to autoresize --%>
			 <%--
			 <%= widgetFactory.createTextArea("Applicant", "", applicantAddress, true,false, isExpressTemplate, "onFocus='this.blur();' rows='4' cols='45'", "","" ) %>
			--%>

			 <%= widgetFactory.createAutoResizeTextArea("Applicant", "", applicantAddress, true, false,false, "class='autoHeightTxtAreaQtrWidthNoSidebar' style='overflow-x: hidden;' rows='4' cols='45'", "", "")%>	

			<%-- Srinivasu_D  T36000019515 Rel8.4 01/23/2014 - End --%>
			 <%
			   // secureParms.put("app_terms_party_oid",termsPartyApp.getAttribute("terms_party_oid"));
			 //style='min-height:60px;'
			 %>
			 	<div style="display:none">			 	
		        <input type=hidden name="app_terms_party_oid" value="<%=termsPartyApp.getAttribute("terms_party_oid")%>">
		        <input type=hidden name="AppTermsPartyType" value="<%=TradePortalConstants.APPLICANT%>">
		        <input type=hidden name="AppName" value="<%=termsPartyApp.getAttribute("name")%>">
		        <input type=hidden name="AppAddressLine1" value="<%=termsPartyApp.getAttribute("address_line_1")%>">
		        <input type=hidden name="AppAddressLine2" value="<%=termsPartyApp.getAttribute("address_line_2")%>">
		        <input type=hidden name="AppAddressLine3" value="<%=termsPartyApp.getAttribute("address_line_3")%>">
		        <input type=hidden name="AppCity" value="<%=termsPartyApp.getAttribute("address_city")%>">
		        <input type=hidden name="AppStateProvince" value="<%=termsPartyApp.getAttribute("address_state_province")%>">
		        <input type=hidden name="AppCountry" value="<%=termsPartyApp.getAttribute("address_country")%>">
		        <input type=hidden name="AppPostalCode" value="<%=termsPartyApp.getAttribute("address_postal_code")%>">
		        <input type=hidden name="AppPhoneNumber" value="<%=termsPartyApp.getAttribute("phone_number")%>">
		        <input type=hidden name="AppOTLCustomerId" value="<%=termsPartyApp.getAttribute("OTL_customer_id")%>">
		        <input type=hidden name="AppAddressSeqNum" value="<%=termsPartyApp.getAttribute("address_seq_num")%>">
				<input type=hidden name="AppAddressSearchIndicator" value="<%=termsPartyApp.getAttribute("address_search_indicator")%>">	
				</div>
			<%= widgetFactory.createTextField("ApplRefNum", "transaction.ApplRefNumber", terms.getAttribute("reference_number").toString(), "30", 
				isReadOnly || isFromExpress, false, isExpressTemplate, "class='char30'", "", "") %>		 		
				
		<%--
		   -==========================================
		   -=========== Corresponding Bank ===========
		   -==========================================
		--%>
			<%
				String corrBankSearchHtml = "";
				String corrBankClearHtml = "";
				String itemid3="cor_terms_party_oid,CorName,CorAddressLine1,CorAddressLine2,CorCity,CorStateProvince,CorCountry,CorPostalCode,CorrespondentBank,CorOTLCustomerId";
				String section3="corres_slc";
				if (!(isReadOnly || isFromExpress)) {
					corrBankSearchHtml = widgetFactory.createPartySearchButton(itemid3,section3,isReadOnly,TradePortalConstants.ADVISING_BANK,false);
					//Commented as on 10th DEc as per IR
					//if (!isReadOnly && isTemplate) {
						corrBankClearHtml = widgetFactory.createPartyClearButton("ClearCorButton", "clearCorrespondentBank()",isReadOnly,"");
					//}
					
				 }	
			 %>
			 <%= widgetFactory.createSubsectionHeader("SLCIssue.CorrespondentBank",isReadOnly, false, false, (corrBankSearchHtml+corrBankClearHtml) ) %>
			 <br/>
			  <%-- Srinivasu_D  T36000019515 Rel8.4 01/23/2014 - start - changing the textarea to autoresize --%>
			 <%--
			 <%= widgetFactory.createTextArea("CorrespondentBank", "", termsPartyCor.buildAddress(false, resMgr), true,false, isExpressTemplate, "onFocus='this.blur();'  rows='4' cols='45'", "","" ) %>			
			 --%>

			 <%= widgetFactory.createAutoResizeTextArea("CorrespondentBank", "", termsPartyCor.buildAddress(false, resMgr), true, false,isExpressTemplate, "class='autoHeightTxtAreaQtrWidthNoSidebar' style='overflow-x: hidden;' rows='4' cols='45'", "", "")%>	

			<%-- Srinivasu_D  T36000019515 Rel8.4 01/23/2014 - End --%>
			 <%
			 //	secureParms.put("cor_terms_party_oid", termsPartyCor.getAttribute("terms_party_oid"));
			 %>
			 	<div style="display:none">
		        <input type=hidden name="cor_terms_party_oid" value="<%=termsPartyCor.getAttribute("terms_party_oid")%>">
		        <input type=hidden name="CorTermsPartyType" value="<%=TradePortalConstants.ADVISING_BANK%>">
		        <input type=hidden name="CorName" value="<%=termsPartyCor.getAttribute("name")%>">
		        <input type=hidden name="CorAddressLine1" value="<%=termsPartyCor.getAttribute("address_line_1")%>">
		        <input type=hidden name="CorAddressLine2" value="<%=termsPartyCor.getAttribute("address_line_2")%>">
		        <input type=hidden name="CorCity" value="<%=termsPartyCor.getAttribute("address_city")%>">
		        <input type=hidden name="CorStateProvince" value="<%=termsPartyCor.getAttribute("address_state_province")%>">
		        <input type=hidden name="CorCountry" value="<%=termsPartyCor.getAttribute("address_country")%>">
		        <input type=hidden name="CorPostalCode" value="<%=termsPartyCor.getAttribute("address_postal_code")%>">
		        <input type=hidden name="CorOTLCustomerId" value="<%=termsPartyCor.getAttribute("OTL_customer_id")%>">
		        </div>				
		</div>
		
		<div class="columnRight">
		<%--
		   -==========================================
		   -============== Issuing Instructions ======
		   -==========================================
		--%>		
			<%-- <%= widgetFactory.createSubsectionHeader("SLCIssue.IssuanceInstructions",isReadOnly,false,false, "") %>
			<%= widgetFactory.createLabel("", "SLCIssue.GteeIssue", isReadOnly, !isTemplate, isExpressTemplate, "") %>
			
			<div class="formItem">	
				<%= widgetFactory.createRadioButtonField("guarantee_issue_type", "guarIssueApplicantsBank", 
					"SLCIssue.ApplicantsBank", TradePortalConstants.GUAR_ISSUE_APPLICANTS_BANK, issueType.equals(TradePortalConstants.GUAR_ISSUE_APPLICANTS_BANK), isReadOnly, "onclick=\"setOverSeasValidDate();\"", "") %>
				<br/>
				<%= widgetFactory.createRadioButtonField("guarantee_issue_type", "guarIssueOverseasBank", 
					"SLCIssue.OverseasBank1", TradePortalConstants.GUAR_ISSUE_OVERSEAS_BANK, issueType.equals(TradePortalConstants.GUAR_ISSUE_OVERSEAS_BANK), isReadOnly, "", "") %>
				<div style="clear:both;"></div>
             
                  <div class="formItemWithIndent4" > <%= widgetFactory.createNote("SLCIssue.OverseasBank2") %></div>
				
			</div>
			<div class="formItemWithIndent4" >
			
			<%= widgetFactory.createDateField( "overseas_validity_date", "SLCIssue.ValidityDateOverseasBank", StringFunction.xssHtmlToChars(terms.getAttribute("overseas_validity_date")),isReadOnly,false, false, "class='char6' onclick=\"setGuarIssueOverseasBank();\"",dateWidgetOptions,"") %>
			</div>	 --%>
		<%-- KMehta - 05 Feb 2016 - Rel9.5 IR-T36000045824 - Code Commented for Simple Standby LC  - End --%>
		<%--
		   -==========================================
		   -============== Payment Terms =============
		   -==========================================
		--%>		
		<%-- Leelavathi - 10thDec2012 - Rel8200 CR-737 - Start --%>
		<%-- 	
				<%= widgetFactory.createSubsectionHeader("SLCIssue.PaymentTerms",isReadOnly, true, false, "") %>
			
			<div class="formItem">	
			<%= widgetFactory.createRadioButtonField("PmtTermsType", "pmtSight", 
					"SLCIssue.Sight", TradePortalConstants.PMT_SIGHT, pmtTermsType.equals(TradePortalConstants.PMT_SIGHT), isReadOnly || isFromExpress, "", "") %>
				<br/>
				<%= widgetFactory.createRadioButtonField("PmtTermsType", "TradePortalConstants.PMT_OTHER", 
					"SLCIssue.PaymentTermsOther", TradePortalConstants.PMT_OTHER, pmtTermsType.equals(TradePortalConstants.PMT_OTHER), isReadOnly || isFromExpress) %>
				
					<%
						if(!isReadOnly){
					%>
					<a href='javascript:openOtherConditionsDialog("AddlConditionsText", "SaveTrans","<%=resMgr.getText("common.ExpiryPlaceOtherLink",TradePortalConstants.TEXT_BUNDLE)%>");' onclick="setPmtSight()">
	          			<%= widgetFactory.createSubLabel("SLCIssue.PaymentTermsOtherCond") %>
	          		</a>
	          		<% 
						}else{
	          		%>
	          			<%= widgetFactory.createSubLabel("SLCIssue.PaymentTermsOtherCond") %>
	          		<%
						}
	          		%>
	          		
				<%= widgetFactory.createSubLabel("SLCIssue.PaymentTermsOtherBelow") %>
			</div>
			 --%>
			<%-- Leelavathi - 10thDec2012 - Rel8200 CR-737 - End --%>
		<%--
		   -==========================================
		   -============== Bank Charges ==============
		   -==========================================
		--%>
			<%= widgetFactory.createSubsectionHeader("SLCIssue.BankCharges",isReadOnly, !isTemplate, isExpressTemplate, "") %>
			
			<div class="formItem">	
				<%= widgetFactory.createRadioButtonField("BankChargesType", "TradePortalConstants.CHARGE_OUR_ACCT", 
					"SLCIssue.AllOurAccount", TradePortalConstants.CHARGE_OUR_ACCT, bankChargesType.equals(TradePortalConstants.CHARGE_OUR_ACCT), isReadOnly || isFromExpress) %>
				<br/>
				<%= widgetFactory.createRadioButtonField("BankChargesType", "TradePortalConstants.CHARGE_BEN_FOR_OUTSIDE", 
					"SLCIssue.AllOutside", TradePortalConstants.CHARGE_BEN_FOR_OUTSIDE, bankChargesType.equals(TradePortalConstants.CHARGE_BEN_FOR_OUTSIDE), isReadOnly || isFromExpress) %>
				<br/>
				<%= widgetFactory.createRadioButtonField("BankChargesType", "bankChargeOther", 
					"SLCIssue.BankChargesOther", TradePortalConstants.CHARGE_OTHER, bankChargesType.equals(TradePortalConstants.CHARGE_OTHER), isReadOnly || isFromExpress, "", "") %>
				    <%
						if(!isReadOnly){
					%>				    
				    <a href='javascript:openOtherConditionsDialog("AddlConditionsText", "SaveTrans","<%=resMgr.getText("common.ExpiryPlaceOtherLink",TradePortalConstants.TEXT_BUNDLE)%>");' onclick="setBankChargeOther()">
				    	<%= widgetFactory.createSubLabel("SLCIsuse.BankChargesOtherLink") %>
				    </a>
				    <%
						}else{
				    %>
				    	<%= widgetFactory.createSubLabel("SLCIsuse.BankChargesOtherLink") %>
				    <%
						}
				    %>
	          		<%--<%= widgetFactory.createSubLabel("SLCIsuse.BankChargesOtherBelow") %>--%>
			</div>

		<%--
		   -==========================================
		   -========= Delivery Instructions ==========
		   -==========================================
		--%>
			<%--========= Delivery To ==========--%>
			<br/>
			<%= widgetFactory.createSubsectionHeader("SLCIssue.DeliveryInstructions",isReadOnly, !isTemplate, false, "") %>
			<div class="formItem">
			<% if(isExpressTemplate) {%>
				<span class="hashMark">#</span>
			<%}%>			
			<%= widgetFactory.createStackedLabel("", "GuaranteeIssue.DeliverTo")%>
			
			
				<%= widgetFactory.createRadioButtonField("guar_deliver_to", "TradePortalConstants.DELIVER_TO_APPLICANT", 
					"SLCIssue.Applicant", TradePortalConstants.DELIVER_TO_APPLICANT, deliverTo.equals(TradePortalConstants.DELIVER_TO_APPLICANT), isReadOnly) %>
				<br/>
				<%= widgetFactory.createRadioButtonField("guar_deliver_to", "TradePortalConstants.DELIVER_TO_BENEFICIARY", 
					"SLCIssue.Beneficiary", TradePortalConstants.DELIVER_TO_BENEFICIARY, deliverTo.equals(TradePortalConstants.DELIVER_TO_BENEFICIARY), isReadOnly) %>
				<br/>
				<%= widgetFactory.createRadioButtonField("guar_deliver_to", "deliveredToAgent", 
					"SLCIssue.Agent1", TradePortalConstants.DELIVER_TO_AGENT, deliverTo.equals(TradePortalConstants.DELIVER_TO_AGENT), isReadOnly, "", "") %>
					<%
						if(!isReadOnly){
					%>	
						<a href='javascript:openOtherConditionsDialog("SpclBankInstructions", "SaveTrans", "<%=resMgr.getText("common.OtherDetails",TradePortalConstants.TEXT_BUNDLE)%>");' onclick="setDeliveredToAgent()">
							<%= widgetFactory.createSubLabel("SLCIssue.Agent2") %>
						</a>
					<%
						}else{
					%>
						<%= widgetFactory.createSubLabel("SLCIssue.Agent2") %>
					<%
						}
					%>
				<br/>
				<%= widgetFactory.createRadioButtonField("guar_deliver_to", "deliveredToOther", 
					"SLCIssue.Other1", TradePortalConstants.DELIVER_TO_OTHER, deliverTo.equals(TradePortalConstants.DELIVER_TO_OTHER), isReadOnly, "", "") %>
					<%
						if(!isReadOnly){
					%>
					<a href='javascript:openOtherConditionsDialog("SpclBankInstructions", "SaveTrans", "<%=resMgr.getText("common.OtherDetails",TradePortalConstants.TEXT_BUNDLE)%>");' onclick="setDeliveredToOther()">
						<%= widgetFactory.createSubLabel("SLCIssue.Other2") %>
					</a>
					<%
						}else{
					%>
						<%= widgetFactory.createSubLabel("SLCIssue.Other2") %>
					<%
						}
					%>
				<br/><br/>
			</div>
			
			<%--========= Delivery By ==========--%>
			<div class="formItem">
			<% if(isExpressTemplate) {%>
				<span class="hashMark">#</span>
			<%}%>				
			<%= widgetFactory.createStackedLabel("", "GuaranteeIssue.DeliverBy") %>

			
				<%= widgetFactory.createRadioButtonField("guar_deliver_by", "TradePortalConstants.DELIVER_BY_TELEX", 
					"SLCIssue.TelexSwift", TradePortalConstants.DELIVER_BY_TELEX, deliverBy.equals(TradePortalConstants.DELIVER_BY_TELEX), isReadOnly) %>
				<br/>
				<%= widgetFactory.createRadioButtonField("guar_deliver_by", "TradePortalConstants.DELIVER_BY_REG_MAIL", 
					"SLCIssue.RegMail", TradePortalConstants.DELIVER_BY_REG_MAIL, deliverBy.equals(TradePortalConstants.DELIVER_BY_REG_MAIL), isReadOnly) %>
				<br/>
				<%= widgetFactory.createRadioButtonField("guar_deliver_by", "TradePortalConstants.DELIVER_BY_MAIL", 
					"SLCIssue.Mail", TradePortalConstants.DELIVER_BY_MAIL, deliverBy.equals(TradePortalConstants.DELIVER_BY_MAIL), isReadOnly) %>
				<br/>
				<%= widgetFactory.createRadioButtonField("guar_deliver_by", "TradePortalConstants.DELIVER_BY_COURIER", 
					"SLCIssue.Courier", TradePortalConstants.DELIVER_BY_COURIER, deliverBy.equals(TradePortalConstants.DELIVER_BY_COURIER), isReadOnly) %>
				<br/>	
			</div>	
		</div>
		<div style="clear: both"></div>
<%-- Leelavathi 10thDec2012 - Rel8200 CR-737 - Start --%>
		<%--
		   -==========================================
		   -========= Payment Tenor Terms  ==========
		   -==========================================
		--%>	
		
		<%=widgetFactory.createWideSubsectionHeader("OutgoingSLCIssue.PmtTerms", false,!isTemplate, false, "") %>
		<div class = "formItem">
			<%-- Leelavathi IR#T36000015097 Rel-8.2 03/22/2013 Start --%>
		<div style="clear:both;"></div>
		<%--<%= widgetFactory.createLabel( "ImportDLCIssue.PmtType","ImportDLCIssue.PmtType",isReadOnly,false,isFromExpress,"inline")
			<%
			options = Dropdown.createSortedRefDataOptions( TradePortalConstants.AVAILABLE_BY_TYPE, terms.getAttribute("payment_type"), loginLocale);
			%>
			<%=widgetFactory.createSelectField("TermsPmtType","", " ", options,isReadOnly, false, false, "", "", "")%> --%>
			<div>
			<%=resMgr.getText("OutgoingSLCIssue.PmtType",TradePortalConstants.TEXT_BUNDLE)%>
			&nbsp;&nbsp;
			<%options = Dropdown.createSortedRefDataOptions( TradePortalConstants.AVAILABLE_BY_TYPE, terms.getAttribute("payment_type"), loginLocale);%>
			<b><%=widgetFactory.createSelectField("TermsPmtType","", " ", options,isReadOnly||isFromExpress, false, false, "", "", "none")%></b>
			<div style='clear: both;'></div>
		</div>
		<%-- Leelavathi IR#T36000015097 Rel-8.2 03/22/2013 End --%>
			<br>
			<% String displayStyle = "display: none;";
			String displayStyle1 = "display: block;";
			// Kiran IR#T36000015047 Rel-8.2 06/03/2013 Start 
			// Added a new string in order to hide the Add 2 more button  
					String disAdd2MrLnsStl = "display: block;";
			 // Kiran IR#T36000015047 Rel-8.2 06/03/2013 End 
			
			//Leelavathi IR#T36000011655,T36000011742 Rel-8.2 02/20/2013 Begin 
			String accpDefpDisabled ="";
			if("ACCP".equals(terms.getAttribute("payment_type")) || "DEFP".equals(terms.getAttribute("payment_type"))){
				//Leelavathi IR#T36000014888 CR-737 Rel-8.2 11/04/2013 Begin
				//Leelavathi IR#T36000016659 CR-737 Rel-8.2 06/05/2013 Begin
				accpDefpDisabled = " READONLY ";
				// Kiran IR#T36000015047 Rel-8.2 06/03/2013 Start 
			// Used that new string in order to hide the Add 2 more button 
				disAdd2MrLnsStl= "display: none;";
			// Kiran IR#T36000015047 Rel-8.2 06/03/2013 End
				//accpDefpDisabled = " DISABLED ";
				//Leelavathi IR#T36000016659 CR-737 Rel-8.2 06/05/2013 End
				//Leelavathi IR#T36000014888 CR-737 Rel-8.2 11/04/2013 End
			}
			//Leelavathi IR#T36000011655,T36000011742 Rel-8.2 02/20/2013 End 
			if("SPEC".equals(terms.getAttribute("payment_type")) ){ 
					displayStyle = "display: block;";	
					displayStyle1 = "display: none;";
					// Kiran IR#T36000015047 Rel-8.2 06/03/2013 Start 
			// Used that new string in order to hide the Add 2 more button 
				disAdd2MrLnsStl= "display: none;";
			// Kiran IR#T36000015047 Rel-8.2 06/03/2013 End
			}
			//Leelavathi IR#T36000011303 Rel-8.2 02/20/2013 Begin
			if("PAYM".equals(terms.getAttribute("payment_type")) ){ 
				displayStyle1 = "display: none;";
				// Kiran IR#T36000015047 Rel-8.2 06/03/2013 Start 
			// Used that new string in order to hide the Add 2 more button 
				disAdd2MrLnsStl= "display: none;";
			// Kiran IR#T36000015047 Rel-8.2 06/03/2013 End
		}
			//Leelavathi IR#T36000011303 Rel-8.2 02/20/2013 End 
			%> 
			
			<div id='PmtTermsSpecialTenorTextDiv' style='<%=displayStyle%>'>
			<%= widgetFactory.createTextArea( "PmtTermsSpecialTenorText", "OutgoingSLCIssue.SpecialTenorText", terms.getAttribute("special_tenor_text"), isReadOnly || isFromExpress, false, true, "", "", "none" ) %>
			</div>
			<br>
			<div id='PmtTermsInPercentAmountIndDiv' style='<%=displayStyle1%>'>	
			<%--Leelavathi IR#T36000011742 Rel-8.2 02/20/2013 End	 --%>
			<%--<%= widgetFactory.createRadioButtonField( "PmtTermsInPercentAmountInd", "OutgoingSLCIssue.Percentage","OutgoingSLCIssue.Percentage",TradePortalConstants.PMT_IN_PERCENT,TradePortalConstants.PMT_IN_PERCENT.equals(terms.getAttribute("percent_amount_dis_ind")), isReadOnly || isFromExpress ) 
			<%= widgetFactory.createRadioButtonField( "PmtTermsInPercentAmountInd", "OutgoingSLCIssue.Amount","OutgoingSLCIssue.Amount",TradePortalConstants.PMT_IN_AMOUNT,  TradePortalConstants.PMT_IN_AMOUNT.equals(terms.getAttribute("percent_amount_dis_ind")), isReadOnly || isFromExpress ) %>--%>
			<%= widgetFactory.createRadioButtonField( "PmtTermsInPercentAmountInd", "OutgoingSLCIssue.Percentage","OutgoingSLCIssue.Percentage",TradePortalConstants.PMT_IN_PERCENT,TradePortalConstants.PMT_IN_PERCENT.equals(terms.getAttribute("percent_amount_dis_ind")), isReadOnly || isFromExpress,""+accpDefpDisabled,"" ) %>
			<%= widgetFactory.createRadioButtonField( "PmtTermsInPercentAmountInd", "OutgoingSLCIssue.Amount", "OutgoingSLCIssue.Amount",TradePortalConstants.PMT_IN_AMOUNT,  TradePortalConstants.PMT_IN_AMOUNT.equals(terms.getAttribute("percent_amount_dis_ind")), isReadOnly || isFromExpress ,""+accpDefpDisabled,"") %>
			 <%--//Leelavathi IR#T36000011742 Rel-8.2 02/20/2013 End  --%>
			</div>

			<input type=hidden value=<%=pmtTermsRowCount %> name="numberOfMultipleObjects1"   id="noOfPmtTermsRows"> 
			<div id='pymtTermsDiv' style='<%=displayStyle1%>'>
			<table id="pymtTerms" class="formDocumentsTable" width="100%" border="1" cellspacing="0" cellpadding="0">
				<thead>
					<tr>
					<%if(TradePortalConstants.PMT_IN_AMOUNT.equals(terms.getAttribute("percent_amount_dis_ind"))){%>
					<%-- Jyothikumari.G 16/05/2013 Rel8.2 IR-T36000016903 Start --%>
					<%-- I added the below class to move the column header to the left --%>
						<th width="8%" class="genericCol"><%=resMgr.getText("OutgoingSLCIssue.PaymentInAmount",TradePortalConstants.TEXT_BUNDLE)%></th>
					<%}else{%>
						<th width="8%" class="genericCol"><%=resMgr.getText("OutgoingSLCIssue.PaymentPercent",TradePortalConstants.TEXT_BUNDLE)%></th>	
					<%}%>
						<th width="18%" class="genericCol"><%=resMgr.getText("OutgoingSLCIssue.TenorType",TradePortalConstants.TEXT_BUNDLE)%></th>
						<th width="45%" class="genericCol"><%=resMgr.getText("OutgoingSLCIssue.TenorDetails",TradePortalConstants.TEXT_BUNDLE)%></th>
						<th width="28%" class="genericCol"><%=resMgr.getText("OutgoingSLCIssue.MaturityDate",TradePortalConstants.TEXT_BUNDLE)%></th>
						<%-- Jyothikumari.G 16/05/2013 Rel8.2 IR-T36000016903 End --%>
					</tr>
				</thead>
				<tbody>
				 <%--passes in pmtTermsList, isReadOnly, isFromExpress, loginLocale,
		          plus new pmtTermsIndex below--%>
				<%
				  int pmtTermsIndex = 0;
				  int iLoop=0;
				  String percentOrAmountInd = terms.getAttribute("percent_amount_dis_ind");
				%>
		      <%@ include file="TransactionPaymentTermRows.frag" %> 
				</tbody>
			</table>
		</div>
		<div style="clear:both;"></div>

<%-- Kiran IR#T36000015047 Rel-8.2 06/03/2013 Start --%>
<%-- Added a new style and replaced  displayStyle1 with disAdd2MrLnsStl --%>
<div id='add2MoreLinesDiv' style='<%=disAdd2MrLnsStl%>'>
<%-- Kiran IR#T36000015047 Rel-8.2 05/31/2013 End --%>
	<% if (!(isReadOnly))
	{     %>
    <button data-dojo-type="dijit.form.Button" type="button" id="add2MoreLines" name="add2MoreLines">
    	<%= resMgr.getText("common.Add2MoreLines", TradePortalConstants.TEXT_BUNDLE) %>
    	<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
				//add2MoreLinesTab(2);
		</script>
	</button>
			<%
		}else{  %>
			&nbsp;
		<%
		} %>
		<%-- Leelavathi IR#T36000011669 Rel-8.2 13/02/2013 End --%>
</div>
			
		<div style="clear:both;"></div>
			</div>
			<%-- Leelavathi 10thDec2012 - Rel8200 CR-737 - End --%>
		<%--
		   -==========================================
		   -========= Detailed Information ==========
		   -==========================================
		--%>	
		<%= widgetFactory.createWideSubsectionHeader("SLCIssue.DetailedInformation",isReadOnly, false, false, "") %>

		<div class="columnLeft">	
			<%
				options = Dropdown.createSortedRefDataOptions(TradePortalConstants.CHARACTERISTIC_TYPE, terms.getAttribute("characteristic_type"),loginLocale);
			%>	
			<%= widgetFactory.createSelectField("characteristic_type", "SLCIssue.PurposeType", " ", options, isReadOnly) %>
			<div class="formItem">
				<%= widgetFactory.createSubLabel("SLCIssue.PurposeType1") %>
				<%
						if(!isReadOnly){
				%>
				<a href='javascript:openOtherConditionsDialog("AddlConditionsText", "SaveTrans","<%=resMgr.getText("common.ExpiryPlaceOtherLink",TradePortalConstants.TEXT_BUNDLE)%>");'>
					<%= widgetFactory.createSubLabel("common.ExpiryPlaceOtherLink") %>
				</a>
				<%
						}else{
				%>
					<%= widgetFactory.createSubLabel("common.ExpiryPlaceOtherLink") %>
				<%
						}
				%>
				<%= widgetFactory.createSubLabel("common.ExpiryPlaceOtherEnd") %>
			</div>					
			<%
				options = Dropdown.createSortedCurrencyCodeOptions(terms.getAttribute("amount_currency_code"), loginLocale);
			%>
			<div>
				<%= widgetFactory.createCurrencySelectField("TransactionCurrency", "transaction.Currency", options, "",
			    	isReadOnly, !isTemplate, false) %>						
				<%= widgetFactory.createAmountField( "TransactionAmount", "transaction.Amount", displayAmount, currency, isReadOnly, 
						!isTemplate, false, "class='char15'", "readOnly:" +isReadOnly, "inline") %>		
				<div style="clear: both;"></div>
			</div>	    	
		</div>
		
		<div class="columnRight">			
			<div class="formItem">
			<%= widgetFactory.createStackedLabel("", "SLCIssue.AmountTolerance") %>
			<%= widgetFactory.createSubLabel( "transaction.Plus") %>
			<%if(!isReadOnly){ %>
			<%= widgetFactory.createPercentField( "AmountTolerancePlus", "", terms.getAttribute("amt_tolerance_pos"), isReadOnly,
										false, false, "", "", "none") %>										
			<%}else {%>
				<b><%=terms.getAttribute("amt_tolerance_pos") %></b>
			<%} %>							
			
			
			<%= widgetFactory.createSubLabel( "transaction.Percent") %>
			&nbsp;&nbsp;
			<%= widgetFactory.createSubLabel( "transaction.Minus") %>
			<%if(!isReadOnly){ %>
			<%= widgetFactory.createPercentField( "AmountToleranceMinus", "", terms.getAttribute("amt_tolerance_neg"), isReadOnly,
										false, false, "", "", "none") %>													
			<%}else {%>
				<b><%=terms.getAttribute("amt_tolerance_neg") %></b>
			<%} %>	
			
			<%= widgetFactory.createSubLabel( "transaction.Percent") %>
			<div style="clear:both;"></div>		
			</div>	
			<%
					 Vector codesToExclude=new Vector();
			         codesToExclude.addElement(TradePortalConstants.COUNTRY_OF_SELLER);
					 options = Dropdown.createSortedRefDataOptions(
		 					  TradePortalConstants.PLACE_OF_EXPIRY_TYPE, 
		 					  terms.getAttribute("place_of_expiry"), 
		 					  loginLocale,codesToExclude);
		             codesToExclude=null;
		   %>
			<div>
				<table>
				<%//CQ IR T36000014381, BMO 110, jgadela -02/28/2013 - Provided the width details for the table columns%>
	            <tr>
	            <% /* KMehta 10/18/2013 CR-910 start 
				      Added asterisk for mandatory field Expiry Date when BankGroup should be processed with Expiry Date*/%>
						<%if(TradePortalConstants.INDICATOR_YES.equals(slc_gua_indicator)){ %>
			            <td width = "45%" align="left">	
				     		<%= widgetFactory.createLabel("ExpiryDate","SLCIssue.ExpiryDate",isReadOnly, !isTemplate, false,"inline")%>
				     		<br/>
				     	</td>
			     	<% } else {%>
			     	<td width = "45%" align="left">	
				     		<%= widgetFactory.createLabel("ExpiryDate","SLCIssue.ExpiryDate",isReadOnly, isTemplate, false,"inline")%>
				     		<br/>
				     	</td>
				     <%} %>
			     	<%/* KMehta 10/15/2013 Rel8400 CR-910 End*/ %>
			     	
			     	<td width = "55%"><div class= "formItem">
			     	 <%= widgetFactory.createInlineLabel("","common.ExpiryPlace")%>
			     	 <%= widgetFactory.createInlineLabel("","common.ExpiryPlaceOther")%>
			     	 <%if(!isReadOnly){ %>
			     	 <a href='javascript:openOtherConditionsDialog("AddlConditionsText","SaveTrans","<%=resMgr.getText("common.ExpiryPlaceOtherLink",TradePortalConstants.TEXT_BUNDLE)%>");'>
						<%= widgetFactory.createSubLabel("common.ExpiryPlaceOtherCondLink") %>
					 </a> 
					 <%}else{ %>			
					 	<%= widgetFactory.createSubLabel("common.ExpiryPlaceOtherCondLink") %>
					 	<%} %>
						</div>
			     	 </td>
			     </tr>
			     <tr>
			     	<td width = "45%" align="left"
			     		<%= widgetFactory.createDateField( "ExpiryDate", "", StringFunction.xssHtmlToChars(terms.getAttribute("expiry_date")), isReadOnly, !isTemplate, false,  "class='char6'", dateWidgetOptions, "inline") %>					
				     </td>
				     <td width = "55%"><div class="align:left" class= "formItem">
				     <%= widgetFactory.createSelectField( "PlaceOfExpiry", "", " ", options, isReadOnly, !isTemplate, false,  "class='char12'", "", "inline" ) %>
				     	</div>
				    </td>
				 </tr>
			</table><div style="clear: both;"></div>
			</div>
		</div>
		
			<% if (!isReadOnly){ %> 
				<%=widgetFactory.createHoverHelp("ben_iss", "PartySearchIconHoverHelp") %>
				<%=widgetFactory.createHoverHelp("corres_slc", "PartySearchIconHoverHelp") %>
			 	<%=widgetFactory.createHoverHelp("ClearCorButton", "PartyClearIconHoverHelp") %>
				<%=widgetFactory.createHoverHelp("applicant_slc", "PartySearchIconHoverHelp") %>
				<%=widgetFactory.createHoverHelp("ClearApplicantButton", "PartyClearIconHoverHelp") %>
				<%= widgetFactory.createHoverHelp("AmountToleranceMinus", "ImportDLCIssue.Minus")%>			
				<%= widgetFactory.createHoverHelp("AmountTolerancePlus", "ImportDLCIssue.Plus")%>				
			<%} %>	
<div style="clear:both;"></div>
<script LANGUAGE="JavaScript">
  function clearCorrespondentBank() {
    document.TransactionSLC.CorName.value = "";
    document.TransactionSLC.CorAddressLine1.value = "";
    document.TransactionSLC.CorAddressLine2.value = "";
    document.TransactionSLC.CorCity.value = "";
    document.TransactionSLC.CorStateProvince.value = "";
    document.TransactionSLC.CorCountry.value = "";
    document.TransactionSLC.CorPostalCode.value = "";
    document.TransactionSLC.CorOTLCustomerId.value = "";
    document.TransactionSLC.CorrespondentBank.value = "";

  }  
  
  function checkBeneficiaryName(originalName)
  {
	  if(dijit.byId('BenName')){
    	  		if(dijit.byId('BenName').value != originalName){
    	  			if(null != document.getElementById('BenOTLCustomerId'))
    	  				document.getElementById('BenOTLCustomerId').value = "";
    	  		}
    	  	}
	<%-- 
     if (document.forms[0].BenName.value != originalName)
     {
        document.forms[0].BenOTLCustomerId.value = "";
     }
	  --%>
  }

  function clearApplicant() 
  {
    document.TransactionSLC.AppName.value = "";
    document.TransactionSLC.AppAddressLine1.value = "";
    document.TransactionSLC.AppAddressLine2.value = "";
    document.TransactionSLC.AppAddressLine3.value = "";
    document.TransactionSLC.AppCity.value = "";
    document.TransactionSLC.AppStateProvince.value = "";
    document.TransactionSLC.AppCountry.value = "";
    document.TransactionSLC.AppPostalCode.value = "";
    document.TransactionSLC.AppPhoneNumber.value = "";
    document.TransactionSLC.AppOTLCustomerId.value = "";
    document.TransactionSLC.Applicant.value = "";
  }
</script>
