<%--
*******************************************************************************
              Structured Purchase Order Search include file

  Description:
  This is a JSP meant to be included using the <%@ include file="x.jsp" %> 
  tag for PO Line Item search.  It creates the HTML for displaying PO 
  Search fields (PO Number, currency ,amount, and Beneficiary Name) for the 
  Search. The file also handles building the extended WHERE Clause that will
  retrieve PO entries based on the search criteria that was entered by the 
  user.

  Because this file is used by including with <%@ include %> rather than with
  the <jsp:include> directive, it is not a standalone servlet.  

*******************************************************************************
--%>
<%--
 *     Copyright  � 2006, CGI-AMS - All rights reserved
--%>

<input type="hidden" name="NewSearch" value="Y">

<%
	String loginLocale=userSession.getUserLocale();
    String DELIMITER = "@";
    StringBuffer newSearchCriteria = new StringBuffer();
	String poNum=null, beneficiaryName=null, amountFrom=null, amountTo=null, poCurrency=null;

	poNum = request.getParameter("PONum") != null ? request.getParameter("PONum").toUpperCase() : "";		
	beneficiaryName = request.getParameter("BeneName") != null ? request.getParameter("BeneName").toUpperCase() : "";	
	amountFrom = request.getParameter("AmountFrom") != null ? request.getParameter("AmountFrom") : "";	
	amountTo = request.getParameter("AmountTo") != null ? request.getParameter("AmountTo") : "";	
	poCurrency = request.getParameter("Currency") != null ? request.getParameter("Currency").toUpperCase() : "";
	
%>

 <%-- JavaScript to enable form submission by enter.  --%>
      <script LANGUAGE="JavaScript">
      
         function enterSubmit(event, myform) {
            if (event && event.which == 13) {
               setButtonPressed('null', 0);
               myform.submit();
            }
            else if (event && event.keyCode == 13) {
               setButtonPressed('null', 0);
               myform.submit();
            }
            else {
               return true;
            }
         }
  
      
      </script>

 <div class="searchDetail">
    <span class="searchCriteria">
   
			<%=widgetFactory.createLabel("","AddPOLineItems.PONumber",false, false, false, "inline searchLabel")%>
			<%=widgetFactory.createTextField("PONum","", StringFunction.xssCharsToHtml(poNum), "14", false, false, false, "class='char6'",  "", "inline searchItem")%>
									
			<%=widgetFactory.createLabel("","AddPOLineItems.ben_name",false, false, false, "inline searchLabel")%>									
			<%=widgetFactory.createTextField("BeneName","",StringFunction.xssCharsToHtml(beneficiaryName), "35", false, false, false, "class='char6'",  "", "inline searchItem")%>
   </span>
    <span class="searchActions">						
		
			<button data-dojo-type="dijit.form.Button" type="button" id="PurchaseOrderSearch" name="PurchaseOrderSearch">
				<%=resMgr.getText("common.FilterText", TradePortalConstants.TEXT_BUNDLE)%>
			    <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
	                  filterPOs();return false;
	      		</script>
			</button>
   </span>
   <div style="clear: both;"></div>
</div>
<%
String currValue=(String)request.getHeader("User-Agent"); 
String browser = "";
String version = "";
boolean isIE7 = false;
if(currValue != null ){
if(currValue.indexOf("MSIE") >0){
   browser = "IE";
   String tempStr = currValue.substring(currValue.indexOf("MSIE"),currValue.length());
   version = (tempStr.substring(4,tempStr.indexOf(";"))).trim();
}
}
if(("IE").equals(browser) && ("7.0").equals(version)) {
      isIE7 = true;
}
if(isIE7) {
%>
<br>
<%}%>

	<% if (!fromAddStrcuPO) {%>
<div class="searchDetail">
    <span class="searchCriteria">

			<%
			String options = Dropdown.createSortedRefDataOptions(
						TradePortalConstants.CURRENCY_CODE, poCurrency,
						loginLocale);
			%>
			<span class="searchHeaderCriteria">
				<%=widgetFactory.createSearchSelectField("Currency","InstSearch.Currency"," ", options, "class='inline'")%>		
			</span>		
			 <%-- MEer Rel 9.3 XSS CID 11359 --%>			
			<%=widgetFactory.createLabel("","InstSearch.From",false, false, false, "inline searchLabel")%>									
			<%=widgetFactory.createTextField("AmountFrom","",StringFunction.xssCharsToHtml(amountFrom), "27", false, false, false, "class='char6'",  "", "inline searchItem")%>
			
			<%=widgetFactory.createLabel("","InstSearch.To",false, false, false, "inline searchLabel")%>									
			<%=widgetFactory.createTextField("AmountTo","",StringFunction.xssCharsToHtml(amountTo), "27", false, false, false, "class='char6'",  "", "inline searchItem")%>						
		</span>						
		
		<div style="clear: both;"></div>
  </div>
	<% }%>
<%= widgetFactory.createHoverHelp("PurchaseOrderSearch", "SearchHoverText")%>	
