<%--
*******************************************************************************
                              Load Panel Level Aliases

  Description: 
  
  	Added as part of CR 821 of Rel8.3
    This is common logic used by Jsps, where Panel Aliases need to be displayed in DataGrids.
	It loads the Panel Aliases for the particular corporate customer and is stored in a Dojo Store.
	
	Earlier customFormatter class was being used to format for every row of the datagrid.
	This caused performance issues. Hence the panelAliases are fetched only once in the jsp 
	and a js formatter is used to format data for each row.
	
	This is not a standalone JSP.
*******************************************************************************

--%>

<%
	CorporateOrganizationBean corpOrgBean = new CorporateOrganizationBean();
	Hashtable corporgpanelAliases = new Hashtable();
	//MEer Rel 8.3 T36000021992 
	if(StringFunction.isNotBlank(corpOrgOid)){
		corporgpanelAliases = corpOrgBean.getPanelLevelAliases(corpOrgOid, userSession.getUserLocale(), ignoreDefaultPanelValue);
	}
	
	List keyaliasList=new ArrayList(corporgpanelAliases.keySet());
	StringBuffer panelaliases=new StringBuffer();
	panelaliases.append("{id: '', name: '',value:''},");
	Collections.sort(keyaliasList);
	for(Iterator i=keyaliasList.iterator();i.hasNext();){
	String key=(String)i.next();
	panelaliases.append("{");
	panelaliases.append("id:'").append(key).append("'");
	panelaliases.append(", ");
	panelaliases.append("name:'").append(StringFunction.escapeQuotesforJS(corporgpanelAliases.get(key).toString())).append("'");
	panelaliases.append(", ");
	panelaliases.append("value:'").append(key).append("'");
	panelaliases.append("} ");
		if(i.hasNext()){
			panelaliases.append(" , ");	
		}
	}
%>
<div data-dojo-type="dojo.store.Memory"
    data-dojo-id="panelStore"
    data-dojo-props="data:[<%= StringFunction.xssCharsToHtml(panelaliases.toString())%>]">
</div>