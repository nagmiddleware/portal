<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
*******************************************************************************
                     Standby LC Issue Page - Other Conditions section

  Description:
    Contains HTML to create the Standby LC Issue Other Conditions section.  

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transaction-SLC-ISS-OtherConditions.jsp" %>
*******************************************************************************
--%>
		
<div class="columnLeft">
		<%=widgetFactory.createSubsectionHeader("OutgoingSLCIssue.AutoExtensionTerms")%>
		<%-- Leelavathi IR#T36000010273 25th Jan 2013 added onClick event Start --%>
		<% 
		String disabled=" disabled=\"disabled\"";
		String maxAutoHtmlPros =" disabled=\"disabled\"";
		String finalExpiryDateHtmlProps =" disabled=\"disabled\"";
		if(TradePortalConstants.INDICATOR_YES.equals(terms.getAttribute("auto_extension_ind"))){
			disabled="";
			if(StringFunction.isBlank(terms.getAttribute("final_expiry_date")) && StringFunction.isBlank(terms.getAttribute("max_auto_extension_allowed"))){
				maxAutoHtmlPros ="";
				finalExpiryDateHtmlProps = "";
			}else if(StringFunction.isNotBlank(terms.getAttribute("final_expiry_date"))){
				maxAutoHtmlPros =" disabled=\"disabled\"";
				finalExpiryDateHtmlProps = "";
			}else{
				finalExpiryDateHtmlProps =" disabled=\"disabled\"";
				maxAutoHtmlPros ="";
			}
		}
		
 	   String isNumDaysReadOnly = "";
 	   if(isFromExpress || !"DAY".equals(terms.getAttribute("auto_extension_period"))){
 		  isNumDaysReadOnly =" disabled=\"disabled\"";
 	   }
      %>
   			<div>	
				<%=widgetFactory.createCheckboxField("AutoExtensionIndicator","GuaranteeIssue.AutoExtension",terms.getAttribute("auto_extension_ind").equals(TradePortalConstants.INDICATOR_YES), isReadOnly, isFromExpress, "onClick='maxNumDisable();'", "",  "inline")%>
			    <div style='clear: both;'></div>
			</div>
			<div>	
			    <%=widgetFactory.createDateField("FinalExpiryDate", "TermsBeanAlias.final_expiry_date",StringFunction.xssHtmlToChars(terms.getAttribute("final_expiry_date")),isReadOnly || isFromExpress, false, isFromExpress, "onChange='extensionType(\"FinalExpiryDate\");'"+finalExpiryDateHtmlProps, dateWidgetOptions, "inline")%>
				<%= widgetFactory.createNumberField( "MaxAutoExtensionAllowed", "GuaranteeIssue.MaxNumber", terms.getAttribute("max_auto_extension_allowed"), "3", isReadOnly || isFromExpress, false, isFromExpress, "onChange='extensionType(\"MaxAutoExtensionAllowed\");'"+maxAutoHtmlPros, "", "inline" ) %>
				<%-- Leelavathi IR#T36000010273 25th Jan 2013 End --%>
				<div style='clear: both;'></div>
			</div>
			<div>
				<%options = Dropdown.createSortedRefDataOptions(TradePortalConstants.AUTO_EXTEND_PERIOD_TYPE, terms.getAttribute("auto_extension_period"), loginLocale);%>
				<%-- Leelavathi IR#T36000014949,IR#T36000016435 CR-737 Rel-8.2 03/04/2013 added onChange function Begin --%>
		        <%=widgetFactory.createSelectField("AutoExtensionPeriod","GuaranteeIssue.ExtensionPeriod", " ", options,isReadOnly || isFromExpress, false, isFromExpress, "onChange='numDaysDisable();'"+disabled, "", "inline")%>
		        <%-- Leelavathi IR#T36000014949,IR#T36000016435 CR-737 Rel-8.2 03/04/2013 End --%>
		        <%= widgetFactory.createNumberField( "AutoExtensionDays", "GuaranteeIssue.NumOfDays", terms.getAttribute("auto_extension_days"), "3", isReadOnly || isFromExpress, false, isFromExpress, isNumDaysReadOnly, "", "inline" ) %>
				<div style='clear: both;'></div>
			</div>			
			<div>
				<%= widgetFactory.createNumberField( "NotifyBeneDays", "TermsBeanAlias.notify_bene_days", terms.getAttribute("notify_bene_days"), "4", isReadOnly || isFromExpress, false, isFromExpress, disabled, "", "inline" ) %>
				<div style='clear: both;'></div>
			</div>  
		
</div>
<div class="columnRight">
	<%
	  String confirmType = terms.getAttribute("confirmation_type");
	%>

	<%= widgetFactory.createWideSubsectionHeader("SLCIssue.Confirmations", false, false, isExpressTemplate, "") %>
 	 
 	<div class="formItem">	 	
		<%= widgetFactory.createRadioButtonField("ConfirmationType", "TradePortalConstants.CONFIRM_NOT_REQD", 
		"", TradePortalConstants.CONFIRM_NOT_REQD, confirmType.equals(TradePortalConstants.CONFIRM_NOT_REQD), isReadOnly || isFromExpress,"","") %>
		<span class="formWidth"><%= resMgr.getText("SLCIssue.CorrBankConfirmationNotRequired",TradePortalConstants.TEXT_BUNDLE)%></span>
	<br/>
	
		<%= widgetFactory.createRadioButtonField("ConfirmationType", "TradePortalConstants.BANK_TO_ADD", 
			"", TradePortalConstants.BANK_TO_ADD, confirmType.equals(TradePortalConstants.BANK_TO_ADD), isReadOnly || isFromExpress) %>
		<span class="formWidth"><%= resMgr.getText("SLCIssue.CorrBankAddConfirmation",TradePortalConstants.TEXT_BUNDLE)%></span>
		<br/>
			
		<%= widgetFactory.createRadioButtonField("ConfirmationType", "TradePortalConstants.BANK_MAY_ADD", 
			"", TradePortalConstants.BANK_MAY_ADD, confirmType.equals(TradePortalConstants.BANK_MAY_ADD), isReadOnly || isFromExpress) %>			
		<span class="formWidth"><%= resMgr.getText("SLCIssue.CorrBankMayAddConfirmation",TradePortalConstants.TEXT_BUNDLE)%></span>
		<br/>
	</div>
</div>
	<div style="clear:both;"></div>
	
	<%--Sandeep - Rel-8.3 CR-752 Dev 06/12/2013 - Begin--%>
	<%-- PMitnala Rel 8.3 IR#T36000021051 Correcting the label to ICC Applicable Rules --%>
	<%= widgetFactory.createCheckboxField("UCPVersionDetailInd", "common.ICCApplicableRules", terms.getAttribute("ucp_version_details_ind").equals(TradePortalConstants.INDICATOR_YES), isReadOnly, false, "onClick=\"clearICCPublication('UCPVersionDetailInd')\"", "", "") %>
	<%
		options = Dropdown.createSortedRefDataOptions(TradePortalConstants.ICC_GUIDELINES_SLC, terms.getAttribute("ucp_version"),loginLocale);
	
		if(isReadOnly && StringFunction.isNotBlank(terms.getAttribute("ucp_version")) && 
				ReferenceDataManager.getRefDataMgr().checkCode(TradePortalConstants.ICC_GUIDELINES_SLC, terms.getAttribute("ucp_version"), loginLocale) == false){
			
			options = Dropdown.createSortedRefDataOptions(TradePortalConstants.ICC_GUIDELINES, terms.getAttribute("ucp_version"), loginLocale);
		}
	%>
                <% //cquinton 10/16/2013 ir#21872 Rel 8.3 start %>
		<%= widgetFactory.createSelectField("UCPVersion", "common.UCPVersion", " ", options, isReadOnly, false, false, "class='char20' onChange=\"changeUCPVersion()\"", "", "formItemWithIndent1") %>
                <%
                   //if version is something other than 'OTHER' the details field should be disabled
                   //also remove setting ucp checkbox onclick - this is done in UCPVersion onchange above
                   String ucpDetailsHtmlProps = "class='char25'";
                   if ( !TradePortalConstants.SLC_OTHER.equals(terms.getAttribute("ucp_version")) ) {
                       ucpDetailsHtmlProps += " disabled=\"disabled\"";
                   }
                %>
		<%= widgetFactory.createTextField("UCPDetails", "common.UCPDetails", terms.getAttribute("ucp_details").toString(), "30", isReadOnly, false, false, ucpDetailsHtmlProps, "", "formItemWithIndent1") %>
                <% //cquinton 10/16/2013 ir#21872 Rel 8.3 end %>
	<%--Sandeep - Rel-8.3 CR-752 Dev 06/12/2013 - End--%>
	<%
        if (isReadOnly || isFromExpress) {
          out.println("");
        } else {
          options = ListBox.createOptionList(addlCondDocList, "PHRASE_OID", "NAME", "", userSession.getSecretKey());
	        
	    defaultText = resMgr.getText("transaction.AddAPhrase", TradePortalConstants.TEXT_BUNDLE);
	%>	
	<%= widgetFactory.createSelectField("AddlDocsPhraseItem", "SLCIssue.AdditionalConditions", defaultText, options, isReadOnly || isFromExpress,false, isExpressTemplate, 
 		"onChange=" + PhraseUtility.getPhraseChange("/Terms/additional_conditions", "AddlConditionsText", "6500","document.forms[0].AddlConditionsText"), "", "") %>
	<%
		}
	%>
	
	<%= widgetFactory.createTextArea("AddlConditionsText", "", terms.getAttribute("additional_conditions"), isReadOnly || isFromExpress, false, false, "onFocus=defaultAddlConditionsText(); rows='10' cols='128'", "placeHolder:'Enter Other Conditions'", "","") %>
	<%if(!isReadOnly) {%>
		<%=widgetFactory.createHoverHelp("AddlConditionsText","InstrumentIssue.PlaceHolderOtherAddlConditions") %>
		<%} %>
  <br/>
