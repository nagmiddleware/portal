<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
*******************************************************************************
                 Standby LC Issue Page - Internal Instructions section

  Description:
    Contains HTML to create the Standby LC Issue Internal Instructions section.  

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transaction-SLC-ISS-InternalInstructions.jsp" %>
*******************************************************************************
--%>
<span class="formItem">
	 <%= widgetFactory.createNote("SLCIssue.InternalInstructionsText") %></span>
	 	
	 	<%= widgetFactory.createTextArea("InternalInstructions", "", terms.getAttribute("internal_instructions"), isReadOnly, false, false, " rows='10' cols='128'", "", "")%>
