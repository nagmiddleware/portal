<div class="columnLeftWithIndent1NoBorder">
<span class='formItem inline'>
	<%=resMgr.getText("SettlementInstruction.ourReference", TradePortalConstants.TEXT_BUNDLE)%>
<%
	if(isReadOnly){
%>	
	<%=widgetFactory.createBoldSubLabel(terms.getAttribute("our_reference"))%>
<%
	}else{
%>	
	<%=widgetFactory.createTextField("ourReference","", terms.getAttribute("our_reference"), "30", isReadOnly, false, false, "class='char20'", "", "none")%>
<%	}%>
</span>
</div>
<div class="columnRightWithIndent1NoBorder">
<%
     String datePattern = userSession.getDatePattern();
     String datePatternDisplay = ReferenceDataManager.getRefDataMgr().getDescr("DATEPATTERN", datePattern, loginLocale);
	 String dateWidgetOptions = "placeHolder:'" + datePatternDisplay + "'";
%>
<span class='asterisk'>*</span>
<span>
	<%=resMgr.getText("SettlementInstruction.applyPaymentOn", TradePortalConstants.TEXT_BUNDLE)%>
<%
	if(isReadOnly){
%>	
	<%=widgetFactory.createBoldSubLabel(TPDateTimeUtility.formatDate(StringFunction.xssHtmlToChars(terms.getAttribute("apply_payment_on_date")), TPDateTimeUtility.SHORT, loginLocale))%>
<%
	}else{
%>	
	<%=widgetFactory.createDateField("applyPaymentOnDate","", StringFunction.xssHtmlToChars(terms.getAttribute("apply_payment_on_date")) ,isReadOnly, true, false, "class='char6'", dateWidgetOptions, "none")%>
<%	}%>
</span>
</div>