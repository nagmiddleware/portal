<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
*******************************************************************************
                              Load Designated Party

  Description:
    This convenience method uses a designated party oid to lookup a party
  and populate a termsParty webbean.  If not found, the webbean is unaffected.
*******************************************************************************
--%>

<%!
   //cquinton 2/13/2013 - change the lookup to take the designated_bank_oid from the passed in partyDoc
   public void loadDesignatedParty(DocumentHandler partyDoc,
                                   TermsPartyWebBean termsParty,
                                   BeanManager beanMgr) {
      // This method attempts to look up a party based on the designated party oid
      // that may be present in the document.  If found, the party data is loaded
      // to the passed in termsParty webbean.

      String designatedBankOid = partyDoc.getAttribute("/designated_bank_oid");

      if (designatedBankOid != null && !designatedBankOid.equals("")) {
        // Use a Party web bean to get the party data for the oid.
        PartyWebBean party = 
              beanMgr.createBean(PartyWebBean.class, "Party");
        party.setAttribute("party_oid", designatedBankOid);
        party.getDataFromAppServer();

        DocumentHandler designatedPartyDoc = new DocumentHandler();
        party.populateXmlDoc(designatedPartyDoc);

        designatedPartyDoc = designatedPartyDoc.getFragment("/Party");

        termsParty.loadTermsPartyFromDoc(designatedPartyDoc);

      }
   }
%>