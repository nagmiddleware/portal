<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
*******************************************************************************
                     Funds Transfer Request Page - Link section

  Description:
    Contains HTML to display links to the other sections.

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transaction-FTRQ-ISS-General.jsp" %>
*******************************************************************************
--%>

  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td width="1" nowrap>&nbsp;</td>
      <td width="115" align="left" valign="middle" nowrap> 
        <p class="ControlLabel">
          <a href="#General">
            <%=resMgr.getText("FundsTransferRequest.General", TradePortalConstants.TEXT_BUNDLE)%>
          </a>
        </p>
      </td>
      <td width="256" nowrap>&nbsp;</td>
      <td width="161" align="left" valign="middle" nowrap> 
        <p class="ControlLabel">
          <a href="#PaymentTerms">
            <%=resMgr.getText("FundsTransferRequest.PaymentTerms", TradePortalConstants.TEXT_BUNDLE)%>
          </a>
        </p>
      </td>
      <td width="622" height="30">&nbsp;</td>
    </tr>
    <tr> 
      <td width="1" nowrap>&nbsp;</td>
      <td nowrap align="left" valign="middle"> 
        <p class="ControlLabel">
          <a href="#FundingDetails">
            <%=resMgr.getText("FundsTransferRequest.FundingDetails", TradePortalConstants.TEXT_BUNDLE)%>
          </a>
        </p>
      </td>
	  <td width="256" nowrap>&nbsp;</td>
      <td nowrap align="left" valign="middle"> 
        <p class="ControlLabel">
          <a href="#OtherConditions">
            <%=resMgr.getText("FundsTransferRequest.OtherConditions", TradePortalConstants.TEXT_BUNDLE)%>
          </a>
        </p>
      </td>
	  <td width="622" height="30">&nbsp;</td>
	</tr>
	
	<td width="1" nowrap>&nbsp;</td>
      <td nowrap align="left" valign="middle"> 
        <p class="ControlLabel">
          <a href="#BankInstructions">
            <%=resMgr.getText("FundsTransferRequest.InstructionstoBank", TradePortalConstants.TEXT_BUNDLE)%>
          </a>
        </p>
      </td>
	  <td width="256" height="30">&nbsp;</td>
	</tr>

  </table>
