<%-- Leelavathi - 10thDec2012 - Rel8200 CR-737 -Initial Version --%>
<%--for the ajax include--%>

<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.html.*,java.util.*" %>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager"
   scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager"
   scope="session">
</jsp:useBean>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager"
   scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean"
   scope="session">
</jsp:useBean>


<% 
//Initialize WidgetFactory.
WidgetFactory widgetFactory = new WidgetFactory(resMgr,userSession); 
String loginLocale = userSession.getUserLocale();
String currencies = "";
int chrgIncIndex = 0;
boolean isTemplate = "true".equals(request.getParameter("isTemplate"));
boolean isExpressTemplate = "true".equals(request.getParameter("isExpressTemplate"));

String parmValue = request.getParameter("chrgIncIndex");

boolean isReadOnly = false;
boolean isFromExpress = false;
int newRecCount=0;
String defaultText1 = "";
String options="";
//Leelavathi IR#T36000015041 CR-737 Rel-8.2 06/05/2013 Begin
String currency="";
//Leelavathi IR#T36000015041 CR-737 Rel-8.2 06/05/2013 End
//Leelavathi IR#T36000011655 Rel-8.2 02/20/2013 Begin

DocumentHandler chrgincList=null;
if ( parmValue != null ) {
	try {
		chrgIncIndex = Integer.parseInt(parmValue);
		
		newRecCount = Integer.parseInt(request.getParameter("count"));
	}
	catch (Exception ex ) {
	}
}


List<FeeWebBean> chrgIncList = new ArrayList<FeeWebBean>();
FeeWebBean fee = null;
for(int i=0; i<newRecCount ; i++){
	//when included per ajax, the business objects will be blank      
	 fee=beanMgr.createBean(FeeWebBean.class,"Fee");

	 chrgIncList.add(fee);
	
}

%>
<%-- Leelavathi 30thJan2013 - Rel8200 CR-737 - start --%>
<%@ include file="TransactionChrgIncRows.frag" %>
<%-- Leelavathi 30thJan2013 - Rel8200 CR-737 - End --%>
