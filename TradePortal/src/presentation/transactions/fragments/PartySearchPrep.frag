<%--
*******************************************************************************
  Party Search Prep

  Description:
    Includes necessary pieces for party search.
    This is used by the PartySearchDialog when user selects to 
    create a new party.
    The field must be set in the SearchParty() javascript
    before the PartySearch dialog is opened.
    This causes the SearchPartyType field to be inluded on form submission.

   Upon return to your page, check the /In/NewPartyFromSearchInfo section for a party
   oid.  If present, it means one was selected.

*******************************************************************************
--%>
<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>

<input type="hidden" id="SearchPartyType" name="SearchPartyType" value="">
