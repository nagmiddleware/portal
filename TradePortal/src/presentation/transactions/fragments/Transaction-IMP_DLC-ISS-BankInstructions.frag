 <%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
*******************************************************************************
                 Import DLC Issue Page - Bank Instructions section

  Description:
    Contains HTML to create the Import DLC Issue Bank Instructions section.  

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transaction-IMP_DLC-ISS-BankInstructions.jsp" %>
*******************************************************************************
--%>
<script LANGUAGE="JavaScript">
<%-- Jyoti 24-08-2012 IR2914 Starts --%>
function enableFinanceDrawing(){
		var FinanceDrawingNumDays= dijit.byId("FinanceDrawingNumDays").value;
		if((isNaN(FinanceDrawingNumDays)==false) ){
		dijit.byId("FinanceDrawing").set('checked',true);	
				}
}
<%-- IR2914 Ends --%>
</script>
<%
  // Get the formatted FEC amount and Rate 

  String displayFECAmount;
  String displayFECRate;

  
if(!getDataFromDoc)
   {
     displayFECAmount = TPCurrencyUtility.getDisplayAmount(terms.getAttribute("fec_amount"), 
                                                terms.getAttribute("amount_currency_code"), loginLocale);
     displayFECRate   = TPCurrencyUtility.getDisplayAmount(terms.getAttribute("fec_rate"), 
                                                "", loginLocale);
   }
  else
   {
     displayFECAmount = terms.getAttribute("fec_amount");
     displayFECRate   = terms.getAttribute("fec_rate");
   }

	  
  	  String regExpRange = "";
	  regExpRange = "[0-9]{0,5}([.][0-9]{0,8})?";
	  regExpRange = "regExp:'" + regExpRange + "'";  
%>


	 
		
		<%
		options = Dropdown.createSortedRefDataOptions(
                TradePortalConstants.INSTRUMENT_LANGUAGE, 
                instrument.getAttribute("language"), 
                loginLocale);
		%>

		<%= widgetFactory.createSelectField( "InstrumentLanguage", "ImportDLCIssue.IssueInstrumentIn", "", options, isReadOnly, true, false, "", "", "") %>
		
		<%
        if (isReadOnly || isFromExpress) {
        	out.println("&nbsp;");
        } else {
          options = ListBox.createOptionList(spclInstrDocList, "PHRASE_OID", "NAME", "", userSession.getSecretKey());
        	defaultText1 = resMgr.getText("transaction.SelectPhrase", TradePortalConstants.TEXT_BUNDLE);
		%>

		<%= widgetFactory.createSelectField( "CommInvPhraseItem1", "ImportDLCIssue.InstructionsText", defaultText1, options, isReadOnly, false, isExpressTemplate, "onChange=" + PhraseUtility.getPhraseChange(
                "/Terms/special_bank_instructions",
                "SpclBankInstructions",
                textAreaMaxlen,"document.forms[0].SpclBankInstructions"), "", "") %>		
		<%
        }
		if(!isReadOnly){ %>
		<%= widgetFactory.createTextArea( "SpclBankInstructions", "", terms.getAttribute("special_bank_instructions").toString(), isReadOnly,false,false, "rows='10' cols='128'","","", textAreaMaxlen) %>
		<%= widgetFactory.createHoverHelp("SpclBankInstructions","InstrumentIssue.PlaceHolderSpecialBankInstr2")%>
		<%}else{ %>
		<%= widgetFactory.createAutoResizeTextArea( "SpclBankInstructions", "", terms.getAttribute("special_bank_instructions").toString(), isReadOnly,false,false, "style='width:600px;min-height:140px;' cols='128'","","") %>
		<%}%>
		<div class="columnLeft"> 
		
				<%= widgetFactory.createSubsectionHeader( "ImportDLCIssue.SettleInstructions",true, false, false,"" ) %>
			
				<%= widgetFactory.createTextField( "SettleOurAcct", "ImportDLCIssue.DebitOurAcct", terms.getAttribute("settlement_our_account_num").toString(), "30", isReadOnly ) %>
				
				<%= widgetFactory.createTextField( "BranchCode", "ImportDLCIssue.BranchCode", terms.getAttribute("branch_code").toString(), "30", isReadOnly) %>
				
				<%= widgetFactory.createTextField( "SettleForeignAcct", "ImportDLCIssue.DebitForeignAcct", terms.getAttribute("settlement_foreign_acct_num").toString(), "30", isReadOnly) %>
				
				<% options = Dropdown.createSortedCurrencyCodeOptions(terms.getAttribute("settlement_foreign_acct_curr"), loginLocale);
				%>
      
		  		<%=widgetFactory.createSelectField("SettleForeignCcy","ImportDLCIssue.AcctCcy", " ", options, isReadOnly, false,false, "style=\"width: 50px;\"", "", "inline")%>		
  				<div style="clear:both;"></div>
		</div>
		
		<div class="columnRight"> 
		
				<%= widgetFactory.createSubsectionHeader( "ImportDLCIssue.CommAndChrg",true, false, false, "" ) %>
				
				<%= widgetFactory.createTextField( "CandCOurAcct", "ImportDLCIssue.DebitOurAcctCommChrg", terms.getAttribute("coms_chrgs_our_account_num").toString(), "30", isReadOnly) %>
				
				<%= widgetFactory.createTextField( "CandCForeignAcct", "ImportDLCIssue.DebitForeignAcctCommChrg", terms.getAttribute("coms_chrgs_foreign_acct_num").toString(), "30", isReadOnly ) %>
				
				<% options = Dropdown.createSortedCurrencyCodeOptions(terms.getAttribute("coms_chrgs_foreign_acct_curr"), loginLocale);
				%>		

  				<%=widgetFactory.createSelectField("CandCForeignCcy","ImportDLCIssue.AcctCcy", " ", options, isReadOnly, false,false, "style=\"width: 50px;\"", "", "inline")%>		
			  				<div style="clear:both;"></div>
		</div>
		<div style="clear:both;"></div>
			 <div class="columnLeft">
			 <%= widgetFactory.createSubsectionHeader( "ImportDLCIssue.FEC",true, false, false, "" ) %>
				
				<div>
				<%= widgetFactory.createTextField( "FECNumber", "ImportDLCIssue.FECCovered", terms.getAttribute("covered_by_fec_number").toString(), "14", isReadOnly, false, false,  "class='char15'", "", "inline"  ) %>
				<%= widgetFactory.createTextField( "FECRate", "ImportDLCIssue.FECRate", displayFECRate, "14", isReadOnly, false, false,  "class='char10'", regExpRange, "inline"  ) %>				
				<%-- <%= widgetFactory.createAmountField( "FECRate", "ImportDLCIssue.FECRate", displayFECRate,"", isReadOnly, false, false, "class='char10'", "readOnly:" +isReadOnly, "inline") %> --%>
				<div style="clear:both;"></div>
			     </div>
		
			     <div>
				<%= widgetFactory.createAmountField( "FECAmount", "ImportDLCIssue.FECAmt", displayFECAmount,currency, isReadOnly, false, false, "class='char15'", "readOnly:" +isReadOnly, "inline") %>
				<%= widgetFactory.createDateField( "FECMaturityDate", "ImportDLCIssue.FECMaturity",  StringFunction.xssHtmlToChars(terms.getAttribute("fec_maturity_date")).toString(), isReadOnly, false, false,  "class='char10'", dateWidgetOptions, "inline" ) %>
				<div style="clear:both;"></div>
			     </div>
     
	
			 </div>		
			 <div class="columnRight">   	
				<%= widgetFactory.createSubsectionHeader( "ImportDLCIssue.FinancingInstructions",true, false, false,"" ) %>
				
				<div class  = "formItem">
				<%= widgetFactory.createCheckboxField( "FinanceDrawing", "ImportDLCIssue.FinanceDrawing", terms.getAttribute("finance_drawing").equals(TradePortalConstants.INDICATOR_YES), isReadOnly, false, "", "", "none"  ) %>
				<%= widgetFactory.createNumberField( "FinanceDrawingNumDays", "", terms.getAttribute("finance_drawing_num_days"), "3", isReadOnly, false, true,  "onChange=\"enableFinanceDrawing();\"", "", "none" ) %>
				<%= widgetFactory.createSubLabel( "ImportDLCIssue.DaysIn") %>	
				</div>
				
				<% options =Dropdown.createSortedRefDataOptions(TradePortalConstants.DRAWING_TYPE,
                        terms.getAttribute("finance_drawing_type"),
                        loginLocale);
				%>
				<div style="margin-left: 50px; margin-top:-7px">
						<%= widgetFactory.createSubLabel( "ImportDLCIssue.In") %>
						<%= widgetFactory.createSelectField( "FinanceDrawingType", "", "", options, isReadOnly, false, false,  "", "", "none" ) %>
						<%= widgetFactory.createSubLabel( "ImportDLCIssue.DrawingAt") %>	
					
				</div>
			</div>
		
		
		<div style="clear:both;"></div>
		
		<% if (isReadOnly || isFromExpress) {
	          out.println("&nbsp;");
	        } else {
          options = ListBox.createOptionList(spclInstrDocList, "PHRASE_OID", "NAME", "", userSession.getSecretKey());
	          defaultText1 = resMgr.getText("transaction.SelectPhrase", TradePortalConstants.TEXT_BUNDLE); 
	     %>
		
		<%= widgetFactory.createSelectField( "CommInvPhraseItem2", "ImportDLCIssue.InstructionsText", defaultText1, options, isReadOnly, false, false, "onChange=" + PhraseUtility.getPhraseChange(
                "/Terms/coms_chrgs_other_text",
                "CandCOtherText",
                textAreaMaxlen,"document.forms[0].CandCOtherText"), "", "") %>
		<% 
	        }
		
		if(!isReadOnly){%>
		<%= widgetFactory.createTextArea( "CandCOtherText", "", terms.getAttribute("coms_chrgs_other_text").toString(), isReadOnly,false,false, "rows='10' cols='128'","","", textAreaMaxlen) %>
		<%= widgetFactory.createHoverHelp("CandCOtherText","InstrumentIssue.PlaceHolderCommissionsChargesInstr")%>		
		<%}else{%>
		<%= widgetFactory.createAutoResizeTextArea( "CandCOtherText", "", terms.getAttribute("coms_chrgs_other_text").toString(), isReadOnly,false,false, "style='width:600px;min-height:140px;' cols='128'","","") %>
		<%}%>