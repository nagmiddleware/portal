<script type="text/javascript" src="/portal/js/datagrid.js"></script>
<div style="clear:both;"></div>

<%=widgetFactory.createSubsectionHeader(
                              "LoanRequest.InvoiceDetails",
                              (isReadOnly || isFromExpress), (!isTemplate?true:false), false, "")%>
<%=widgetFactory.createHoverHelp("InvoicesDetailsTextTrd", "InvoiceDetailsHoverText")%>

<%-- DK IR T36000016553 Rel8.2 05/02/2013 Starts --%>
	<div class="formItem">
		<%=widgetFactory.createPOTextArea("InvoicesDetailsTextTrd",terms.getAttribute("invoice_details"),
				"78","10", "POLineItems", isReadOnly || isFromExpress, 
			"", "OFF", true) %>
			</div>

<%-- %=widgetFactory.createTextArea(
                              "InvoicesDetailsTextTrd", "LoanRequest.InvoiceDetails",
                              terms.getAttribute("invoice_details"),
                              true, false, false,
                              "rows='10' cols='250' ", "", "")%> --%>
<%-- DK IR T36000016553 Rel8.2 05/02/2013 Ends --%>
<% if (numberOfDomPmts == 0 && !isTemplate) { %>
<div>
    <% 
	    if (numberOfInvoicesAttached > 0 && !isTemplate) { %>	
	     <div id="view_inv_div_tl" class='formItem inline'>
	     <button data-dojo-type="dijit.form.Button"  name="ViewTransInvoicesTL" id="ViewTransInvoicesTL"  type="button">
	        <%=resMgr.getText("common.ViewTransInvoices",TradePortalConstants.TEXT_BUNDLE)%>
	        <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
             	setButtonPressed('ViewTransInvoices', 0); 
             	document.forms[0].submit();
	        </script>
	     </button>
	     </div>
	<%}%>

	<div id="add_inv_div_tl" class='formItem inline'>
    	<button data-dojo-type="dijit.form.Button"  name="AddTransInvoicesTL" id="AddTransInvoicesTL"  type="button">
            <%=resMgr.getText("common.AddTransInvoices",TradePortalConstants.TEXT_BUNDLE)%>
            <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
	        	if (checkFieldRequiredness()){
	                 setButtonPressed('AddTransInvoices', 0); 
	                 document.forms[0].submit();
                 }
                 
		       function checkFieldRequiredness(){
		             var retVal = false;
					require(["dojo/aspect","dijit/registry","dojo/ready"], function(aspect,registry,ready){
						ready(function(){
						    var loanProceedsPmtCurrTrd = dijit.getEnclosingWidget(document.getElementById('LoanProceedsPmtCurrTrd'))	;
						    if (loanProceedsPmtCurrTrd && loanProceedsPmtCurrTrd.value == ''){
						    	alert("Please select Loan Proceeds Currency. ");
							    retVal = false;
							    return retVal;
						    }
							var loanProceedsOA = dijit.getEnclosingWidget(document.getElementById('CreditOurAcctTrd'));
							var loanProceedsBen = dijit.getEnclosingWidget(document.getElementById('CreditBenAccountTrd'));
							var loanProceedsMultiBen = dijit.getEnclosingWidget(document.getElementById('CreditMultiBenAccountTrd'));
							if (loanProceedsOA && loanProceedsOA.checked ){
								retVal = true;
								return retVal;
							}
							if (loanProceedsBen && loanProceedsBen.checked ){
								retVal = true;
							}
							if (loanProceedsMultiBen && loanProceedsMultiBen.checked ){
								retVal = true;
							}
							if (!retVal){
								alert("Please select Apply Loan Proceed To. ");
								return retVal;
							}
							var payMethod = dijit.getEnclosingWidget(document.getElementById('PaymentMethodType'));
							<%-- IR 29707--%>
							var isMultiBenInvoices =<%=isMultiBenInvoicesAttached%>;
							var isSingleBenInvoices =<%=isSingleBenInvoicesAttached%>;

							if (payMethod && payMethod.value =='' && !(isMultiBenInvoices || isSingleBenInvoices)){
								alert("Please select Payment Method. ");
								retVal = false;
								return retVal;
							}else{
						    	retVal = true;
						    }
						    if (payMethod && payMethod.value ==''){
			dijit.byId("PaymentMethodType").set("value","<%=invPayMethod%>");
			}
							
						});
					});            	
					return retVal;
		          }
	                 
            </script>
        </button>
    </div>
    <% 
    	if (numberOfInvoicesAttached > 0 && !isTemplate) { %>	
			<div id="remove_inv_div_tl" class='formItem inline'>		         
		         <button data-dojo-type="dijit.form.Button"  name="RemoveTransInvoicesTL" id="RemoveTransInvoicesTL"  type="button">
 		            <%=resMgr.getText("common.RemoveTransInvoices",TradePortalConstants.TEXT_BUNDLE)%>
 		            <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
 		                 setButtonPressed('RemoveTransInvoices', 0); 
 		                 document.forms[0].submit();
 		            </script>
 		         </button>
	     	 </div>
	<%}%>  

</div>  
<%}%>     
<div style="clear: both;"></div> 
<%=widgetFactory.createTextArea(
                              "FinancedInvoicesDetailsTextTrd", "LoanRequest.FinancedInvoicesDetailsText",
                              terms.getAttribute("financed_invoices_details_text"),
                              isReadOnly || areInvoicesAttached, false, false,
                              "rows='10' cols='250' ", "", "")%>                  
 <%=widgetFactory.createHoverHelp("FinancedInvoicesDetailsTextTrd", "FinancedInvoicesHoverText")%>
