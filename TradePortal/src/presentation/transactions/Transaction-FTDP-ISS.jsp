  
<%--
*******************************************************************************
                        Funds Transfer Request Page

  Description:    
    This is the main driver for the Domestic Payment Request page.  It handles 
  data retrieval of terms and parties (or retrieval from the input document)
  and creates the html page to display all the data for a Domestic Payment Request.
*******************************************************************************
--%>
      
<%-- 
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>

<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.html.*,java.math.*,java.util.*,com.amsinc.ecsg.util.DateTimeUtility" %>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager"    scope="session"> </jsp:useBean>
<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session"> </jsp:useBean> 
<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session"> </jsp:useBean>
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean"     scope="session"> </jsp:useBean>
   

<%-- ************** Data retrieval page setup begins here ****************  --%>

<%
  WidgetFactory widgetFactory = new WidgetFactory(resMgr, userSession);
  //cquinton 2/12/2013 add refdata manager for datepattern lookup
  ReferenceDataManager refData = ReferenceDataManager.getRefDataMgr();

  String onLoad = "";
  String focusField = "payment_date";			// IAZ CR-586 10/09/10 Chg: This is now the Default focus field
  boolean focusSet = true;
  boolean radioSelectSection = false;
  boolean payeeAutoReload = true;
  
  boolean isFixedPmt = false;	
  
  String PartySearchAddressTitle =resMgr.getTextEscapedJS("PartySearch.TabHeading",TradePortalConstants.TEXT_BUNDLE);
  // Various oid and status info from transaction and instruments used in several places.
  String instrumentOid = "";
  String transactionOid;
  String instrumentType;
  String instrumentStatus;
  String transactionType; 
  String transactionStatus;
  String rejectionIndicator = "";
  String rejectionReasonText = "";
  String prevTransStatusFromDoc = null;				
  boolean           corpOrgHasMultipleAddresses   = false;
  String            corpOrgOid                    = null;
 
  String invoiceDetailOid = "";
  String cachedInvoiceDetailOid = "";
// Srini_D IR# VSUL081045430 Rel 8.1 08/20/2012  Start 
 // String invDetailsChanged = TradePortalConstants.INDICATOR_NO;
  String invDetailsChanged = TradePortalConstants.INDICATOR_YES;
  // Srini_D IR# VSUL081045430 Rel 8.1 08/20/2012  End 
  String requestMarketRateVisibility = "hidden";   
  
  DocumentHandler accountDoc = null;
  
  //Set accountSet = null;
  //String acctNo = "";
  StringBuffer accountSql = new StringBuffer();
  String buttonClicked = request.getParameter("buttonName");

  boolean getDataFromDoc;              // Indicates if data is retrieved from the input doc cache or from the database
  boolean instrumentSelected = false;  // Indicates if an instrument was selected.
  boolean isStatusVerifiedPendingFX 	= false;	
  String BankSearchAddressTitle =resMgr.getText("RefDataHome.BankBranchRules.Search",TradePortalConstants.TEXT_BUNDLE);	
  DocumentHandler doc;

  String loginLocale = userSession.getUserLocale();
  String loginRights = userSession.getSecurityRights();

  //cquinton 2/12/2013 lookup locale specific datePattern description from refData manager
  String datePattern = userSession.getDatePattern();
  String datePatternDisplay = refData.getDescr("DATEPATTERN", datePattern, loginLocale);
  String dateWidgetOptions = "placeHolder:'" + datePatternDisplay + "'";

  // Variables used for dealing with a terms party's accounts.
  boolean showPayeeAccts = false;
  String payeeAcctList = "";
  String payerAcctList = "";
  String acctOid = "";
  String acctNum = "";
  String acctCcy = "";
  String payeeAcctNum = "";
  String payeeAcctCcy = "";  

  // Dates are stored in the database as one field but displayed on the
  // page as 3 fields.  These variables hold the pieces for each date.
  String paymentDateDay = null;
  String paymentDateMonth = null;
  String paymentDateYear = null;          
  String displayDPDate = null;
  String formatableDate = null;  

  // Variables used for populating ref data dropdowns.
  String options;
  String defaultText;
  
  String			 benificiaryName 	   = "";					
  String			 benificiaryBankName   = "";					
  String	         paymentStatusSearch   = "";					
  String            amount                = "";						
  int               numberOfDomPmts       = 0;
  String bButtonPressed = "";
  // These are the beans used on the page.
  TransactionWebBean transaction  = (TransactionWebBean)beanMgr.getBean("Transaction");
  InstrumentWebBean instrument    = (InstrumentWebBean)beanMgr.getBean("Instrument");
  TemplateWebBean template        = (TemplateWebBean)beanMgr.getBean("Template");
  TermsWebBean terms              = null;
  

  TermsPartyWebBean termsPartyPayee = beanMgr.createBean(TermsPartyWebBean.class, "TermsParty");
  TermsPartyWebBean termsPartyPayer = beanMgr.createBean(TermsPartyWebBean.class, "TermsParty");
  TermsPartyWebBean termsPartyPayeeBank = beanMgr.createBean(TermsPartyWebBean.class, "TermsParty");
  
  DomesticPaymentWebBean currentDomesticPayment = beanMgr.createBean(DomesticPaymentWebBean.class, "DomesticPayment");  
  InvoiceDetailsWebBean paymentInvoiceDetails = beanMgr.createBean(InvoiceDetailsWebBean.class, "InvoiceDetails");  
                               

  // New Parties to the Payment Transactions: First and Second Intermediary Banks, Ordering Party and its Bank.
  PaymentPartyWebBean firstIntBank = beanMgr.createBean(PaymentPartyWebBean.class, "PaymentParty");
  PaymentPartyWebBean orderingParty = beanMgr.createBean(PaymentPartyWebBean.class, "PaymentParty");

  TransactionWebBean aSourceTemplateTransaction		= null;	//IAZ CR-586 10/09/10 Add Source Template Transactuion and 
  TermsWebBean aSTTerms           					= null;	//                    Terms to be used with Fixed payments
  

  // Get the document from the cache.  We'll may use it to repopulate the 
  // screen if returning from another page, a save, validation, or any other
  // mediator called from this page.  Otherwise, we assume an instrument oid
  // and transaction oid was passed in.

  doc = formMgr.getFromDocCache();
  bButtonPressed = doc.getAttribute("/In/Update/ButtonPressed");
  
  Vector error = null;
  error = doc.getFragments("/Error/errorlist/error");
   
    // DK CR-886 Rel8.4 10/15/2013 starts
   CorporateOrganizationWebBean corporateOrg = beanMgr.createBean(CorporateOrganizationWebBean.class, "CorporateOrganization");
   corporateOrg.setAttribute("organization_oid", userSession.getOwnerOrgOid());
   corporateOrg.getDataFromAppServer();
   String pmt_bene_sort_default = corporateOrg.getAttribute("pmt_bene_sort_default");
	// DK CR-886 Rel8.4 10/15/2013 ends

 //PMitnala Rel 8.3 IR#T36000021096 Moved the code from top of the page and adding an additional check to see if payment has multiple beneficiaries
 //Sandeep - Rel 8.3 System Test IR# T36000020739 09/16/2013 - Begin
   //boolean pmtBenPanelAuthInd = false;
  // if (TradePortalConstants.INDICATOR_YES.equals((String)transaction.getAttribute("bene_panel_auth_ind")))
	 //  pmtBenPanelAuthInd = true;
 //Sandeep - Rel 8.3 System Test IR# T36000020739 09/16/2013 - End
 
   String links= "";
   Vector errorList = null;
   
   String bankReleasedTerms	= transaction.getAttribute("c_BankReleasedTerms");
   boolean hasBankReleasedTerms  = InstrumentServices.isNotBlank( bankReleasedTerms );
  
  String currentDPOid = null;	//Payment transaction contains multiple individual payments (payees). Information for
  								//	one (selected) payement only displayed on the page at a time.  This variable contains
  								//	oid of the correspondign record or none if no paymenst are selected

  //When page is reload because an individual payment was selected from the Payee List at the bottom 
  //	(as apposed to because an error was detected), we must retrieve individaul payment information from the database
  //	even though the general tarnsaction data is reloaded from cache.
  //	Code below retrieves oid of the last selected paument to retrieve it from db
  if ((doc != null) && (doc.getAttribute("/In/Update/ButtonPressed") == null))
  {     
  	currentDPOid = doc.getAttribute("/In/DomesticPayment/domestic_payment_oid"); 
  }
  
  Debug.debug("doc from cache is " + doc.toString());

/******************************************************************************
  We are either entering the page or returning from somewhere.  These are the 
  conditions for how to populate the web beans.  Data comes from either the
  database or the doc cache (/In section) with some variation.

  Mode           Condition                      Populate Beans From
  -------------  ----------------------------   --------------------------------
  Enter Page     no /In/Transaction in doc      Instrument and Template web
                                                beans already populated, get
                                                data for Terms and TermsParty
                                                web beans from database

  return from    /Error/maxerrorseverity < 1    Same as Enter Page (data is
  Transaction                                   retrieved from database)
    mediator
    (no error)

  return from    /Error/maxerrorseverity > 0    doc cache (/In)
  Transaction    
    mediator
    (error)

******************************************************************************/
	
  // Assume we get the data from the doc.
  getDataFromDoc = true;
  
  String maxError = doc.getAttribute("/Error/maxerrorseverity");
  if (maxError != null && maxError.compareTo(String.valueOf(ErrorManager.WARNING_ERROR_SEVERITY)) <= 0) {
     // No errors, so don't get the data from doc.
     getDataFromDoc = false;
  }
  //In the case where reCertification/Authentication req'd on authorized, the page is reloaded with error
  //but the status of the transaction actually changed.
  else
  {
        String errorSectionText = null;
        if (doc.getComponent("/Error") != null)
  	{
  		errorSectionText = doc.getComponent("/Error").toString();
  	}
  	if (errorSectionText != null)
  	{
  		//jgadela ANZ Ticket # 106962(CQ- T36000013794) - added condition to check whether the page is coming from attachment action.
  		if ((errorSectionText.indexOf(TradePortalConstants.CERTIFICATE_AUTH_REQ_ERR) != -1 ) 
	 	|| (errorSectionText.indexOf(TradePortalConstants.COMMIT_TO_IMAGE_DB_FAILED) != -1 )
  		    //||(errorSectionText.indexOf(TradePortalConstants.DP_OID_NOT_PROVIDED_FOR_DELETE) != -1)
  		    ) 
  		{
  			prevTransStatusFromDoc = doc.getAttribute("/In/Transaction/transaction_status"); //IAZ CM-451 03/24/09 Add
  			getDataFromDoc = false;
  			transaction.getDataFromAppServer();
  		}
  	}
  }
  
  //ir cnuk113043991 - check to see if transaction needs to be refreshed
  // if so, refresh it and do not get data from doc as it is wrong
  if ( "Y".equals(doc.getAttribute("/Out/Refresh/Transaction")) ) {
     transaction.getDataFromAppServer();
     getDataFromDoc = false;
  }
  
  if (doc.getDocumentNode("/In/Transaction") == null) {
     // No /In/Transaction means we've never looked up the data.
     Debug.debug("No /In/Transaction section - get data from database");
     getDataFromDoc = false;
  }
  
  
  if (doc.getDocumentNode("/In/NewPartyFromSearchInfo/Type") != null) {
     // We have returned from the party search page.  Get data from doc cache
     getDataFromDoc = true;
  }
  
  if (doc.getDocumentNode("/In/BankSearchInfo/Type") != null) {
     // We have returned from the Bank search page.  Get data from doc cache
     getDataFromDoc = true;
  }
  
    
  
  if (getDataFromDoc) {
     Debug.debug("Populating beans from doc cache");

     // Populate the beans from the input doc.
     try {
        instrument.populateFromXmlDoc(doc.getComponent("/In"));
        transaction.populateFromXmlDoc(doc.getComponent("/In"));
        template.populateFromXmlDoc(doc.getComponent("/In"));

        terms = (TermsWebBean) beanMgr.getBean("Terms");
        terms.populateFromXmlDoc(doc, "/In");

        //BSL IR#PUL032965444 04/04/11 Begin
        paymentInvoiceDetails.populateFromXmlDoc(doc.getComponent("/In"));
    	cachedInvoiceDetailOid = paymentInvoiceDetails.getAttribute("invoice_detail_oid");
		/*  Srini_D IR# VSUL081045430 Rel 8.1.1 08/20/2012  Start 
        if (TradePortalConstants.INDICATOR_YES.equals(doc.getAttribute("/In/InvoiceDetails/changed"))) 
        {
            invDetailsChanged = TradePortalConstants.INDICATOR_YES;
        }
		//Srini_D IR# VSUL081045430 Rel 8.1.1 08/20/2012  End
		*/
        //BSL IR#PUL032965444 04/04/11 End

        String termsPartyOid, paymentPartyOid; //Vshah CR507
        

        termsPartyOid = terms.getAttribute("c_FirstTermsParty");
        if (termsPartyOid != null && !termsPartyOid.equals(""))
        {
           termsPartyPayee.setAttribute("terms_party_oid", termsPartyOid);
           termsPartyPayee.getDataFromAppServer();
        }

        termsPartyOid = terms.getAttribute("c_SecondTermsParty");
        if (termsPartyOid != null && !termsPartyOid.equals(""))
        {
           termsPartyPayer.setAttribute("terms_party_oid", termsPartyOid);
           termsPartyPayer.getDataFromAppServer();
        }

        termsPartyOid = terms.getAttribute("c_ThirdTermsParty");
        if (termsPartyOid != null && !termsPartyOid.equals(""))
        {
           termsPartyPayeeBank.setAttribute("terms_party_oid", termsPartyOid);
           termsPartyPayeeBank.getDataFromAppServer();
        }
        
        //Vshah CR507 Begin
		paymentPartyOid = terms.getAttribute("c_OrderingParty");
        if (paymentPartyOid != null && !paymentPartyOid.equals(""))
        {
           orderingParty.setAttribute("payment_party_oid", paymentPartyOid);
           orderingParty.getDataFromAppServer();
        }
        
		//Vshah CR507 End

        DocumentHandler termsPartyDoc, paymentPartyDoc; //Vshah CR507

        termsPartyDoc = doc.getFragment("/In/Terms/FirstTermsParty");
        termsPartyPayee.loadTermsPartyFromDocTagsOnly(termsPartyDoc);
 
        termsPartyDoc = doc.getFragment("/In/Terms/SecondTermsParty");
        termsPartyPayer.loadTermsPartyFromDocTagsOnly(termsPartyDoc);

        termsPartyDoc = doc.getFragment("/In/Terms/ThirdTermsParty");
        termsPartyPayeeBank.loadTermsPartyFromDocTagsOnly(termsPartyDoc);     
        
        paymentPartyDoc = doc.getFragment("/In/DomesticPayment/OrderingParty");
        orderingParty.loadPaymentPartyFromDocTagsOnly(paymentPartyDoc);  
        
      
        //IAZ CM-451 11/25/08: If there was an error AND domestic payment info was entered/selected 
        //                  (e.g., AddPayee or Save or Verify while Payee data is loaded/entered to the detaile section), 
        //					redisplay dom pmt data on the screen.
        //IAZ CM-451 12/14/08: If there was an error AND domestic payment info was NOT entered/selected
        //					(e.g., Save or Verify while No Payee data loaded/entered in the dtaile section)
        //                  check if an a payee-in-error's oid is provided in the error section
        //					
                

        //if (doc.getAttribute("/Out/DomesticPayment/domestic_payment_oid") == null 
        //		|| "".equalsIgnoreCase(doc.getAttribute("/Out/DomesticPayment/domestic_payment_oid")))
		if ((doc.getAttribute("/Out/DomesticPayment/domestic_payment_oid") == null )	 || 
           (maxError != null && maxError.compareTo(String.valueOf(ErrorManager.ERROR_SEVERITY)) >= 0) )		
        {
        	currentDomesticPayment.populateFromXmlDoc(doc.getComponent("/In"));
        	String curCharges = curCharges=doc.getAttribute("/In/Terms/bank_charges_type");
        	if (curCharges != null)
        	{
        		currentDomesticPayment.setAttribute("bank_charges_type", curCharges);
        	}

			// If page is reload due to an error (and not because user selects new Payment entry form the list),
			//    get Intermediary banks data from cache
        	if (InstrumentServices.isBlank(currentDPOid))
        	{
        		populatePaymentParty(firstIntBank, doc.getFragment("/In"), "FirstPaymentParty");
        		
        	}

        }
        else
        {
        	currentDomesticPayment.setAttribute("domestic_payment_oid", doc.getAttribute("/Out/DomesticPayment/domestic_payment_oid"));
        	currentDomesticPayment.getDataFromAppServer();
        	
   			if (InstrumentServices.isNotBlank(currentDomesticPayment.getAttribute("c_FirstIntermediaryBank")))
   			{
   				firstIntBank.setAttribute("payment_party_oid", currentDomesticPayment.getAttribute("c_FirstIntermediaryBank"));
   				//out.println(currentDomesticPayment.getAttribute("c_FirstIntermediaryBank") + currentDomesticPayment.getAttribute("domestic_payment_oid"));
   				firstIntBank.getDataFromAppServer();
   			}

        }


        populatePaymentParty(orderingParty, doc.getFragment("/In"), "OrderingParty");			
       
        //Get and process Date as needed so it can be displayed correctly
        displayDPDate = transaction.getAttribute("payment_date");
		
		
        if ((displayDPDate != null)&& (!displayDPDate.equals(""))) 
        {

        	if ((displayDPDate.indexOf("//") != -1) || (displayDPDate.indexOf("-1") != -1))
        	{
        		paymentDateDay = paymentDateMonth = paymentDateYear = "-1";	
        	}
        	else
        	{
        		paymentDateDay = displayDPDate.substring(3,5);
        		paymentDateMonth = displayDPDate.substring(0,2);
        		paymentDateYear = displayDPDate.substring(6,10);
        	}
        }
        else
        {
        		paymentDateDay = "-1";
        		paymentDateMonth = "-1";
        		paymentDateYear = "-1";
				displayDPDate = paymentDateMonth + "/" + paymentDateDay + "/" + paymentDateYear + " 00:00:00";       		
        }
        formatableDate = paymentDateYear + '-' + paymentDateMonth + '-' + paymentDateDay + " 00:00:00.0";
        //End of Date section
        
     } catch (Exception e) {
        out.println("Contact Administrator: Unable to reload data after returning to page. Error is " + e.toString());
     }
  } else {
     Debug.debug("populating beans from database");	
	 
	 // Srini_D IR# MNUM121350532 Rel 8.1 12/28/2012  Start 
     if(TradePortalConstants.BUTTON_VERIFY.equals(doc.getAttribute("/In/Update/ButtonPressed")) ||  TradePortalConstants.BUTTON_EDIT.equals(doc.getAttribute("/In/Update/ButtonPressed")) || TradePortalConstants.BUTTON_SAVETRANS.equals(doc.getAttribute("/In/Update/ButtonPressed"))){

		 if(InstrumentServices.isNotBlank(doc.getAttribute("/In/DomesticPayment/domestic_payment_oid"))){
			
			currentDPOid = doc.getAttribute("/In/DomesticPayment/domestic_payment_oid");
			
		 }
		 if(InstrumentServices.isNotBlank(doc.getAttribute("/Out/DomesticPayment/domestic_payment_oid"))){
			
			currentDPOid = doc.getAttribute("/Out/DomesticPayment/domestic_payment_oid");
			
		 }
	 }
	 
	 // Srini_D IR# MNUM121350532 Rel 8.1 12/28/2012  End

     // We will perform a retrieval from the database.
     // Instrument and Transaction were already retrieved.  Get the rest of the data
     if (terms == null)
     {
     	terms = transaction.registerCustomerEnteredTerms();
     }
     if (terms == null)
     {
    	 terms = transaction.registerBankReleasedTerms();
     }


     String termsPartyOid;

     termsPartyOid = terms.getAttribute("c_FirstTermsParty");
     if (termsPartyOid != null && !termsPartyOid.equals("")) {
        termsPartyPayee.setAttribute("terms_party_oid", termsPartyOid);
        termsPartyPayee.getDataFromAppServer();
     }
     
     termsPartyOid = terms.getAttribute("c_SecondTermsParty");
     if (termsPartyOid != null && !termsPartyOid.equals("")) {
        termsPartyPayer.setAttribute("terms_party_oid", termsPartyOid);
        termsPartyPayer.getDataFromAppServer();
     }
           
     termsPartyOid = terms.getAttribute("c_ThirdTermsParty");
     if (termsPartyOid != null && !termsPartyOid.equals("")) {
        termsPartyPayeeBank.setAttribute("terms_party_oid", termsPartyOid);
        termsPartyPayeeBank.getDataFromAppServer();
     }
     
     //Get and process Date as needed so it can be displayed correctly
     String dpDate = transaction.getAttribute("payment_date");
     
     if ((dpDate == null ) || (dpDate.equals(""))) //||(dpDate.indexOf("/") != -1)) //|| (dpDate.indexOf("-1") != -1))
     {
        paymentDateDay = "-1";
        paymentDateMonth = "-1";
        paymentDateYear = "-1";
     }
     else
     {
     
        if (dpDate.indexOf("/") != -1)
        {
          	transaction.getDataFromAppServer();
			dpDate = transaction.getAttribute("payment_date");          	
     	}
     	
     	paymentDateDay = TPDateTimeUtility.parseDayFromDate(dpDate);
     	paymentDateMonth = TPDateTimeUtility.parseMonthFromDate(dpDate);
     	paymentDateYear = TPDateTimeUtility.parseYearFromDate(dpDate);     
     }
     displayDPDate = paymentDateMonth + '/' + paymentDateDay + '/' + paymentDateYear + " 00:00:00";

     //End of Date section
     
   	 if (InstrumentServices.isNotBlank(terms.getAttribute("c_OrderingParty")))
   	 {
   		orderingParty.setAttribute("payment_party_oid", terms.getAttribute("c_OrderingParty"));
   		orderingParty.getDataFromAppServer();
   	 }

  }
    

  boolean returnFromSearch = false;
  boolean returnFromFIBSearch = false;
  boolean returnFromSIBSearch = false;

  /*************************************************
  * Load New Party
  **************************************************/
  if (doc.getDocumentNode("/In/NewPartyFromSearchInfo/Type") != null &&
      doc.getDocumentNode("/In/NewPartyFromSearchOutput/PartyOid") != null ) {
 
     returnFromSearch = true;
     // We have returned from the party search page.  Based on the returned 
     // data, RELOAD one of the payment party beans with the selected party

     String paymentPartyType = doc.getAttribute("/In/NewPartyFromSearchInfo/Type");
     String partyOid = doc.getAttribute("/In/NewPartyFromSearchOutput/PartyOid");

     Debug.debug("Returning from party search with " + paymentPartyType + "/" + partyOid);

     // Use a Party web bean to get the party data for the selected oid.
     PartyWebBean party = beanMgr.createBean(PartyWebBean.class, "Party");
     party.setAttribute("party_oid", partyOid);
     party.getDataFromAppServer();

     DocumentHandler partyDoc = new DocumentHandler();
     party.populateXmlDoc(partyDoc);

     partyDoc = partyDoc.getFragment("/Party");

     // Based on the party type being returned (which we previously set), reload one of the payment party web beans with the data from the doc.
     if (paymentPartyType.equals(TradePortalConstants.PAYEE)) {
     	if (partyDoc != null) {
		    currentDomesticPayment.loadPaymentPartyFromDoc(partyDoc,paymentPartyType); 
		}		
		 focusField = "PayeeName";
         focusSet = true;
     }
     
     if (paymentPartyType.equals(TradePortalConstants.ORDERING_PARTY)) {
	     if (partyDoc != null) {
    		orderingParty.loadPaymentPartyFromDoc(partyDoc);
    	 }	 	
     	 focusField = "OrderingPartyName";
         focusSet = true;
     }

  }
  
  
  if (doc.getDocumentNode("/In/BankSearchInfo/BankOid") != null) {
  	 returnFromSearch = true;
  	 String bankType = doc.getAttribute("/In/BankSearchInfo/Type");
     String bankOid = doc.getAttribute("/In/BankSearchInfo/BankOid");
     
     BankBranchRuleWebBean bankBranchAddr = beanMgr.createBean(BankBranchRuleWebBean.class, "BankBranchRule");
     bankBranchAddr.setAttribute("bank_branch_rule_oid", bankOid);
     bankBranchAddr.getDataFromAppServer();

     DocumentHandler bankBranchAddrDoc = new DocumentHandler();
     bankBranchAddr.populateXmlDoc(bankBranchAddrDoc);

     bankBranchAddrDoc = bankBranchAddrDoc.getFragment("/BankBranchRule");

	 if (bankType.equals(TradePortalConstants.BENEFICIARY_BANK)) {
		     currentDomesticPayment.loadPaymentPartyBankFromDoc(bankBranchAddrDoc);
		}	
		
	 if (bankType.equals(TradePortalConstants.FIRST_INTERMED_BANK)) {
	 		 returnFromFIBSearch = true;
		     firstIntBank.loadPaymentPartyBankFromDoc(bankBranchAddrDoc);
		}	
     
  }
  
  
  // Vshah ClearBank Party Start
  if (doc.getDocumentNode("/In/BankToClearInfo/ClearBank") != null) {
  
     //out.println("cleaning... " + doc.getAttribute("/In/BankToClearInfo/Type"));
     returnFromSearch = true;
    
	 String bankType = doc.getAttribute("/In/BankToClearInfo/Type"); 
	  
		
	 if (bankType.equals(TradePortalConstants.BENEFICIARY_BANK)) {
		     currentDomesticPayment.clearPaymentPartyBank();
		}	
		
	 if (bankType.equals(TradePortalConstants.FIRST_INTERMED_BANK)) {
		     firstIntBank.clearPaymentPartyBank();
		     returnFromFIBSearch = true;
		}	

  }  
 // Vshah ClearBank Party End
 
 // Srini_D IR# MNUM121350532 Rel 8.1 12/28/2012  Start 
  if ((currentDPOid != null) && (!currentDPOid.equals(""))){
		currentDomesticPayment.setAttribute("domestic_payment_oid", currentDPOid);
        currentDomesticPayment.getDataFromAppServer();
		 
	}	
  // Srini_D IR# MNUM121350532 Rel 8.1 12/28/2012  End 

  //IAZ: When user picks a Beneficiary form the Bene List, data must reload to the "Details" (upper) part fo the screen. 
  //     We must handle this differently from loading data from db or on-error document's reload.
  if ((currentDPOid != null) && (!currentDPOid.equals("")))
  {
	//BSL IR#PUL032965444 04/04/11 Begin
	// Get invoice details from DB if cached doc does not have correct details for current payment
	StringBuffer invDetSql = new StringBuffer();
	invDetSql.append("SELECT INVOICE_DETAIL_OID FROM INVOICE_DETAILS WHERE DOMESTIC_PAYMENT_OID = ?");

	DocumentHandler invDetOidDoc = DatabaseQueryBean.getXmlResultSet(invDetSql.toString(), false, new Object[]{currentDPOid});
	if (invDetOidDoc != null) {
		invoiceDetailOid = invDetOidDoc.getAttribute("/ResultSetRecord/INVOICE_DETAIL_OID");
		if (InstrumentServices.isNotBlank(invoiceDetailOid) && 
				!invoiceDetailOid.equals(cachedInvoiceDetailOid)) {
			paymentInvoiceDetails.setAttribute("invoice_detail_oid", invoiceDetailOid);
			paymentInvoiceDetails.getDataFromAppServer();
		}
	}
	//BSL IR#PUL032965444 04/04/11 End

    //Vshah/IAZ CR-507 Change
    //   If page is reloaded because user selects a Bene from the List, load data for this beneficiary form the database
    //   including Intermediary Banks data if they have previously been saved for this Bene.
    //   However, if screen reloads because it is returned from the a Party/Bank Search, do not reload Beneficiary data form db --
    //   it has already been loaded from doc cache or searhc result. Intermediary Bank has not been loaded form cache, so unless it has been
    //   loaded from the search results, load it from cache doc.
    
	if (TradePortalConstants.BUTTON_MODIFY_PAYEE.equals(buttonClicked)) {  

  		currentDomesticPayment.setAttribute("domestic_payment_oid", currentDPOid);
		currentDomesticPayment.getDataFromAppServer();

   		if (InstrumentServices.isNotBlank(currentDomesticPayment.getAttribute("c_FirstIntermediaryBank")))
   		{
   			firstIntBank.setAttribute("payment_party_oid", currentDomesticPayment.getAttribute("c_FirstIntermediaryBank"));
   			firstIntBank.getDataFromAppServer();
   		}

   		
   	 }
   	 else
   	 {
        if (!returnFromFIBSearch)
   		{
        	populatePaymentParty(firstIntBank, doc.getFragment("/In"), "FirstPaymentParty");      		
   		}   	
   		
     }		

   }
  
 
  if (InstrumentServices.isNotBlank(terms.getAttribute("source_template_trans_oid")))
  {
  	aSourceTemplateTransaction = beanMgr.createBean(TransactionWebBean.class, "Transaction");                           
  	aSourceTemplateTransaction.setAttribute("transaction_oid", terms.getAttribute("source_template_trans_oid"));
  	aSourceTemplateTransaction.getDataFromAppServer();

  	if (aSourceTemplateTransaction != null)	
  	{
  		aSTTerms = aSourceTemplateTransaction.registerCustomerEnteredTerms();
  		if (aSTTerms != null){
  			 isFixedPmt = true;
  			 terms.sourceTermsWebBean = aSTTerms;
  		}
  			
  		if (InstrumentServices.isNotBlank(aSTTerms.getAttribute("c_OrderingParty")))
  		{
  			orderingParty.sourcePartyWebBean = beanMgr.createBean(PaymentPartyWebBean.class,  "PaymentParty");  
  			orderingParty.sourcePartyWebBean.setAttribute("payment_party_oid", aSTTerms.getAttribute("c_OrderingParty"));
  			orderingParty.sourcePartyWebBean.getDataFromAppServer();
  		}
  	}
  }

 
  BigInteger requestedSecurityRight = SecurityAccess.DOMESTIC_CREATE_MODIFY;

  String requestMarketRateInd = terms.getAttribute("request_market_rate_ind"); // DK CR-640 Rel7.1
  
  String bene_party_oid = currentDomesticPayment.getAttribute("beneficiary_party_oid");
%>



<%@ include file="fragments/Transaction-PageMode.frag" %>
<%
  // Nar IR-T36000029027 Rel9.0.0.0 17-June-2014 Begin
  boolean isDebitAcctFixedVal = false;
  if(isFixedPayment && StringFunction.isNotBlank(terms.getAttribute("debit_account_oid"))){
	isDebitAcctFixedVal = true;
  }
 //Nar IR-T36000029027 Rel9.0.0.0 17-June-2014 Begin
 
//if newTransaction, set session variable so it persists
  String newTransaction = null;
  if (doc.getDocumentNode("/Out/newTransaction")!=null) {
    newTransaction = doc.getAttribute("/Out/newTransaction");
    session.setAttribute("newTransaction", newTransaction);
    System.out.println("found newTransaction = "+ newTransaction);
  } 
  else {
    newTransaction = (String) session.getAttribute("newTransaction");
    if ( newTransaction==null ) {
      newTransaction = TradePortalConstants.INDICATOR_NO;
    }
    System.out.println("used newTransaction from session = " + newTransaction);
  }

  //cquinton 1/18/2013 remove old close action behavior
%>

<%
    
    if (isTemplate) 
   	corpOrgOid = template.getAttribute("owner_org_oid");
    else
   	corpOrgOid = instrument.getAttribute("corp_org_oid");
   	
   	if (isTemplate)
   		if (InstrumentServices.isNotBlank(focusField)&&focusField.equals("templateSelection"))
   			focusField = "TemplateName";

     if (DatabaseQueryBean.getCount("address_oid", "address", "p_corp_org_oid = ?", false, new Object[]{corpOrgOid}) > 0 )
     {
   	corpOrgHasMultipleAddresses = true;
     }   

  transactionType = transaction.getAttribute("transaction_type_code");
  transactionStatus = transaction.getAttribute("transaction_status");

  rejectionIndicator = transaction.getAttribute("rejection_indicator");
  rejectionReasonText = transaction.getAttribute("rejection_reason_text");

  instrumentType = instrument.getAttribute("instrument_type_code");
  instrumentStatus = instrument.getAttribute("instrument_status");

  Debug.debug("Instrument Type " + instrumentType);
  Debug.debug("Instrument Status " + instrumentStatus);
  Debug.debug("Transaction Type " + transactionType);
  Debug.debug("Transaction Status " + transactionStatus);  

  
  //Template Dropdown Data Retrive
  %>
  <%@include file="fragments/TransactionGetUser.frag" %>
  <%
  
  String confidentialClause = "";
  if (userSession.getSavedUserSession() == null)
  {
      if (TradePortalConstants.INDICATOR_NO.equals(loggedUser.getAttribute("confidential_indicator")))
          confidentialClause = "and t.confidential_indicator = 'N' ";
  }   	
  else
  {
      if (!TradePortalConstants.ADMIN.equals(userSession.getSavedUserSession().getSecurityType()))	
         if (TradePortalConstants.INDICATOR_NO.equals(loggedUser.getAttribute("subsid_confidential_indicator")))
            confidentialClause = "and t.confidential_indicator = 'N' ";
  }	  

  	  
   QueryListView queryTemplateListView = null;
   
   Vector templateList = null;
   int numberOfTemplates = 0;
   boolean isBankUser = true;
   try
   {
      
      queryTemplateListView = (QueryListView) EJBObjectFactory.createClientEJB(formMgr.getServerLocation(), "QueryListView"); 

      boolean useTemplateGroup = false;
      if (userSession.getSavedUserSession() == null)
      {      
      	String checkTemplateGroupSql = "select tg.authorized_template_group_oid from USER_AUTHORIZED_TEMPLATE_GROUP tg where tg.p_user_oid = ? ";
        queryTemplateListView.setSQL(checkTemplateGroupSql, new Object[]{userSession.getUserOid()});
        queryTemplateListView.getRecords();

        if (queryTemplateListView.getRecordCount() != 0)
        	useTemplateGroup = true;
      }
      String sqlString;
      List<Object> sqlPrms = new ArrayList<Object>();
      if (useTemplateGroup)
      {
      	sqlString = 
      "select i.original_transaction_oid, i.original_transaction_oid || '/' || t.template_oid as TransactionOid, t.name as TemplateName, t.ownership_level as ownershipLevel, t.ownership_type as ownershipType, i.instrument_type_code as instrumentType, p.name as partyName from template t, instrument i, terms_party p, USER_AUTHORIZED_TEMPLATE_GROUP tg where t.c_template_instr_oid = i.instrument_oid and t.default_template_flag = 'N' and i.a_counter_party_oid = p.terms_party_oid (+)  and i.instrument_type_code = 'FTDP' and t.payment_templ_grp_oid = tg.A_TEMPLATE_GROUP_OID and tg.p_user_oid = ? "
      + confidentialClause + " order by upper(TemplateName)";
      	sqlPrms.add(userSession.getUserOid());
      }
      else
      {
      	sqlString = "select i.original_transaction_oid, i.original_transaction_oid || '/' || t.template_oid as TransactionOid, t.name as TemplateName, t.ownership_level as ownershipLevel, t.ownership_type as ownershipType, i.instrument_type_code as instrumentType, p.name as partyName from template t, instrument i, terms_party p where t.c_template_instr_oid = i.instrument_oid and t.default_template_flag = 'N' and i.a_counter_party_oid = p.terms_party_oid (+)  and i.instrument_type_code = 'FTDP' "
      			+ "and p_owner_org_oid in (?,?,?,?) " + confidentialClause + " order by upper(TemplateName)";
      	sqlPrms.add(userSession.getOwnerOrgOid());
      	sqlPrms.add(userSession.getClientBankOid());
      	sqlPrms.add(userSession.getGlobalOrgOid());
      	sqlPrms.add(userSession.getBogOid());
      }
      queryTemplateListView.setSQL(sqlString, sqlPrms);
      queryTemplateListView.getRecords();

      DocumentHandler templatesDocHandler = queryTemplateListView.getXmlResultSet();
      numberOfTemplates = queryTemplateListView.getRecordCount();
      templateList = templatesDocHandler.getFragments("/ResultSetRecord");
      
      sqlString = "select name from client_bank, users where user_oid = ? and p_owner_org_oid = organization_oid";

      queryTemplateListView.setSQL(sqlString, new Object[]{userSession.getUserOid()});
      queryTemplateListView.getRecords();      
	  if (queryTemplateListView.getRecordCount() == 0)
	  {
	  	isBankUser = false;
	  }


      if (userSession.hasSavedUserSession())
      {
      	isBankUser=true;
      }
      
      
   }
   catch (Exception e)
   {
      e.printStackTrace();
   }
   finally
   {
      try
      {
         if (queryTemplateListView != null)
         {
            queryTemplateListView.remove();
         }
      }
      catch (Exception e)
      {
         System.out.println("Error removing QueryListView in -CreditDebitDetails.frag!");
         
      }
   }   
  


String opAdjName = null;
String opAltName = null;
if (InstrumentServices.isNotBlank(terms.getAttribute("debit_account_oid")))
{
	StringBuffer acctCorpSql = new StringBuffer ("select name, ALTERNATE_NAME from corporate_org co, account a"); 
	acctCorpSql.append(" where a.OTHERCORP_CUSTOMER_INDICATOR = 'Y' and a.OTHER_ACCOUNT_OWNER_OID = co.organization_oid");
	acctCorpSql.append(" and a.account_oid = ?");

	try
	{
		QueryListView aCqueryListView = (QueryListView) EJBObjectFactory.createClientEJB(formMgr.getServerLocation(), "QueryListView");
		aCqueryListView.setSQL(acctCorpSql.toString(), new Object[]{terms.getAttribute("debit_account_oid")});
		aCqueryListView.getRecords();

		if (aCqueryListView.getRecordCount() > 0)
		{	
			DocumentHandler acResult = aCqueryListView.getXmlResultSet();
			Vector acRecs = acResult.getFragments("/ResultSetRecord");
			if (acRecs != null)
			{
				DocumentHandler acRecord = (DocumentHandler) acRecs.elementAt(0);
				if (acRecord != null)
				{
					opAdjName = acRecord.getAttribute("/NAME");
					opAltName = acRecord.getAttribute("/ALTERNATE_NAME");
				}
			}
		}
	}	
	catch(Exception exc1)
	{
		//nothing here
	}
}
if (InstrumentServices.isBlank(opAdjName))
{
	opAdjName = corporateOrg.getAttribute("name");
	opAltName = corporateOrg.getAttribute("alternate_name");	
}



%>
  <%-- rbhaduri - 8th August 06 - IR AOUG100368306 - Moved above
		<%@ include file="fragments/Transaction-PageMode.frag" %>
	--%>


<%-- ********************* HTML for page begins here ********************* --%>


  <%@include file="fragments/MultiPartMode.frag" %>

<%
  if (!isReadOnly) 
  {
	 //onLoad += "updateFundingAmount();";	 //IAZ CR-483B 08/13/09 CHF
	 onLoad += "updateTotalAmount();";	 //IAZ CR-483B 08/13/09 CHT
  }
  
%>

<%
  // The navigation bar is only shown when editing templates.  For transactions
  // it is not shown ti minimize the chance of leaving the page without properly
  // unlocking the transaction.
  String showNavBar = TradePortalConstants.INDICATOR_NO;
  if (isTemplate)
  {
     showNavBar = TradePortalConstants.INDICATOR_YES;
  }

  // Auto save the form when time-out if not readonly.  
  // (Header.jsp will check for auto-save setting of the corporate org).
  String autoSaveFlag = isReadOnly ? TradePortalConstants.INDICATOR_NO : TradePortalConstants.INDICATOR_YES;
  String templateFlag = isTemplate ? TradePortalConstants.INDICATOR_YES : TradePortalConstants.INDICATOR_NO;
%>

<% //IAZ 01/03/09 and 05/15/09 Begin
   //Include ReAuthentication frag in case re-authentication is required for complete
   //authorization of transactions for this client
   //cr498 change certNewLink for certAuthURL
   String certAuthURL = "";
   //if (TradePortalConstants.TRANS_STATUS_REQ_AUTHENTICTN.equals(transaction.getAttribute("transaction_status")))
   Cache reCertCache = (Cache)TPCacheManager.getInstance().getCache(TradePortalConstants.CLIENT_BANK_CACHE);
   DocumentHandler CBCResult = (DocumentHandler)reCertCache.get(userSession.getClientBankOid());	
   //cr498 - any instrument/transaction could require reauthentication
   String requireTranAuth = CBCResult.getAttribute("/ResultSetRecord(0)/REQUIRE_TRAN_AUTH");
 


//VShah - CR564 - 03/01/2011 - BEGIN
if((TradePortalConstants.TRANS_STATUS_VERIFIED_AWAITING_APPROVAL.equals(transaction.getAttribute("transaction_status")) ||
		TradePortalConstants.TRANS_STATUS_REPORTING_CODES_REQUIRED.equals(transaction.getAttribute("transaction_status"))) &&
   TradePortalConstants.INDICATOR_YES.equals(transaction.getAttribute("uploaded_ind")))
{
	MediatorServices medService = new MediatorServices();
  	medService.getErrorManager().setLocaleName(resMgr.getResourceLocale());
  	medService.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.FILE_MUST_BE_CONFIRMED);
  	medService.addErrorInfo();
	DocumentHandler errDoc = formMgr.getFromDocCache();
	errDoc.setComponent("/Error", medService.getErrorDoc());
}
//VShah - CR564 - 03/01/2011 - END
//manohar - PSUL052447378 - 06/28/2011 - BEGIN
if(TradePortalConstants.TRANS_STATUS_VERIFIED_PENDING_FX.equals(transaction.getAttribute("transaction_status")) &&
   TradePortalConstants.INDICATOR_YES.equals(transaction.getAttribute("uploaded_ind")))
{
	Date date     = null;
 	SimpleDateFormat sdf      = null;
  	SimpleDateFormat iso      = null;
  	//payment_date is a ISO Date format i.e.M/dd/yyyy HH:mm:ss.S(the format returned
   // from the database). Convert it to yyyy-MM-dd HH:mm:ss.S  
   	String result   = transaction.getAttribute("payment_date");
   
   	sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss.S");
   	iso = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");   
   		try{
   	  		date = iso.parse(result);   
   	  		result = sdf.format(date);
      	}
      	catch (Exception e)
      	{   
   		}
		   
	if(TPDateTimeUtility.isFutureDate(result)){
		MediatorServices medService = new MediatorServices();
  		medService.getErrorManager().setLocaleName(resMgr.getResourceLocale());
  		medService.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.INFO_DOM_FUTURE_DEB_CRED_DIFF);
  		medService.addErrorInfo();
		DocumentHandler errDoc = formMgr.getFromDocCache();
		//MDB POUM010364494 Rel7.1 1/5/12 - Begin
		int errorListSize = errDoc.getFragments("/Error/errorlist/error").size();
		errDoc.addComponent("/Error/errorlist/error(" + (errorListSize+1) + ")", (DocumentHandler) medService.getErrorDoc().getFragments("/errorlist/error").elementAt(0));
		//MDB POUM010364494 Rel7.1 1/5/12 - End
	}else{
		//MDB PRUL112347061 Rel7.1 12/15/11 - Begin
		MediatorServices medService = new MediatorServices();
		medService.getErrorManager().setLocaleName(resMgr.getResourceLocale());
		medService.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, transaction.getAttribute("authorization_errors"));
		medService.addErrorInfo();
		DocumentHandler errDoc = formMgr.getFromDocCache();
		//MDB DSUM010948585 Rel7.1 1/9/12 - Begin
		//MDB LHUM011262371 Rel7.1 1/13/12 - Begin
		//int errorListSize = errDoc.getFragments("/Error/errorlist/error").size();
		errorList = errDoc.getFragments("/Error/errorlist/error");
		int errorListSize = errorList.size();
		boolean addValue = true;
		for (int i = 0; i < errorListSize; i++)
		{
			DocumentHandler errorDoc = (DocumentHandler)errorList.elementAt(i);
			if (errorDoc.getAttribute("/code").equalsIgnoreCase(transaction.getAttribute("authorization_errors")))
			{
				addValue = false;
				break;
			}
		}
				
		if (addValue)
			errDoc.addComponent("/Error/errorlist/error(" + (errorListSize+1) + ")", (DocumentHandler) medService.getErrorDoc().getFragments("/errorlist/error").elementAt(0));
		//MDB LHUM011262371 Rel7.1 1/13/12 - End
		//MDB DSUM010948585 Rel7.1 1/9/12 - End

		//MDB PRUL112347061 Rel7.1 12/15/11 - End
	}
}

 
   boolean requireAuth = InstrumentAuthentication.requireTransactionAuthentication(requireTranAuth,InstrumentAuthentication.TRAN_AUTH__FTDP_ISS);
   if (requireAuth) {
   
      // If Transaction just got successfully authroized (so Tran status just got changed to REQ_AUTHENTICTN 
      // from somnething else, open the authentication window once Transaction Page reloads.
      if (((InstrumentServices.isNotBlank(prevTransStatusFromDoc)))&&(!TradePortalConstants.TRANS_STATUS_REQ_AUTHENTICTN.equals(prevTransStatusFromDoc)))
      {
      		onLoad = onLoad + "openReauthenticationWindow('" + certAuthURL + "')";
      }
      
   }
 String pageTitleKey="";

  if ( TradePortalConstants.INDICATOR_YES.equals(newTransaction) ) {
   pageTitleKey = "SecondaryNavigation.NewInstruments";

   if (isTemplate){
       if(userSession.getSecurityType().equals(TradePortalConstants.ADMIN)){
          userSession.setCurrentPrimaryNavigation("AdminNavigationBar.RefData");
       }else{
          userSession.setCurrentPrimaryNavigation("NavigationBar.RefData");
       }
   }else
        userSession.setCurrentPrimaryNavigation("NavigationBar.NewInstruments");
} else {
   pageTitleKey = "SecondaryNavigation.Instruments";

   if (isTemplate){
       if(userSession.getSecurityType().equals(TradePortalConstants.ADMIN)){
          userSession.setCurrentPrimaryNavigation("AdminNavigationBar.RefData");
       }else{
          userSession.setCurrentPrimaryNavigation("NavigationBar.RefData");
       }
   }else 
        userSession.setCurrentPrimaryNavigation("NavigationBar.Transactions");
}



  String helpUrl = "customer/issue_direct_send_coll.htm";

  //cquinton 11/19/2012 pass info for recent instruments into header
  String newRecentTransactionOid = transaction.getAttribute("transaction_oid");
  String newRecentInstrumentId = instrument.getAttribute("complete_instrument_id");
  String newRecentTransactionType = transaction.getAttribute("transaction_type_code");
  String newRecentTransactionStatus = transaction.getAttribute("transaction_status");
%>

<jsp:include page="/common/Header.jsp">
  <jsp:param name="autoSaveFlag"           value="<%=autoSaveFlag%>" />
  <jsp:param name="templateFlag"           value="<%=templateFlag%>" />
  <jsp:param name="newRecentTransactionOid" value="<%=newRecentTransactionOid%>" />
  <jsp:param name="newRecentInstrumentId"  value="<%=newRecentInstrumentId%>" />
  <jsp:param name="newRecentTransactionType" value="<%=newRecentTransactionType%>" />
  <jsp:param name="newRecentTransactionStatus" value="<%=newRecentTransactionStatus%>" />
</jsp:include>

<div class="pageMain">		<%-- Page Main Start --%>
<div class="pageContent">	<%--  Page Content Start --%>

<%

   if (requireAuth) {
%>   
      <%@ include file="/logon/fragments/openReauthenticationWindow.frag" %>
<%

   }
   
%> 

<form id="TransactionFTDP" name="TransactionFTDP" method="post" data-dojo-type="dijit.form.Form"  action="<%=formMgr.getSubmitAction(response)%>">

  <input type=hidden value="" name=buttonName>
<%
	if(isTemplate) {
		  String pageTitle = resMgr.getText("common.template", TradePortalConstants.TEXT_BUNDLE);
		  StringBuffer title = new StringBuffer();
		  title.append(pageTitle);
		  title.append( " : " );
		  title.append( refData.getDescr(TradePortalConstants.INSTRUMENT_TYPE, instrument.getAttribute("instrument_type_code"), loginLocale) );
		  title.append( " - " );
		  title.append(template.getAttribute("name") );
  		
  		%>
`
		<jsp:include page="/common/PageHeader.jsp">
				<jsp:param name="titleKey" value="<%= pageTitle%>"/>
				<jsp:param name="helpUrl"  value="customer/issue_dom_payments.htm" />
  		</jsp:include>
  		
  		<jsp:include page="/common/PageSubHeader.jsp">
 				 <jsp:param name="titleKey" value="<%= title.toString()%>" />
  				
		</jsp:include> 		
	<%} else { %>

		<jsp:include page="/common/PageHeader.jsp">
            <jsp:param name="titleKey" value="<%=pageTitleKey%>" />
            <jsp:param name="item1Key" value="SecondaryNavigation.Instruments.Payment" />
			<jsp:param name="helpUrl" value="customer/issue_dom_payments.htm"/>
		</jsp:include>
		
		<jsp:include page="/common/TransactionSubHeader.jsp" />

<%} %>
 <div class="formArea">
          <jsp:include page="/common/ErrorSection.jsp" />
	
				  <div class="formContent">

  
 <%

  if (requireAuth) {
%> 

  <input type="hidden" name="reCertification" value="<%=TradePortalConstants.INDICATOR_NO%>">
  <input type="hidden" name="reCertOK">
  <input type="hidden" name="logonResponse">
  <input type="hidden" name="logonCertificate">
  <%--AAlubala CR711 Rel 8.0 01/27/2012
  Add proxy value but do not set a value to it.  The value will be set on the fly if the transaction requires to be signed by 2FA token --%>
  <input type="hidden" name="proxy">  

<%
  }

  // Store values such as the userid, security rights, and org in a
  // secure hashtable for the form.  Also store instrument and transaction
  // data that must be secured.
  Hashtable secureParms = new Hashtable();
  secureParms.put("login_oid", userSession.getUserOid());
  secureParms.put("owner_org_oid", userSession.getOwnerOrgOid());
  secureParms.put("login_rights", loginRights);

  secureParms.put("instrument_oid",        instrument.getAttribute("instrument_oid"));
  secureParms.put("instrument_type_code",  instrumentType);
  secureParms.put("instrument_status",     instrumentStatus);
  secureParms.put("corp_org_oid",  corpOrgOid);

  secureParms.put("transaction_oid",       transaction.getAttribute("transaction_oid"));
  secureParms.put("transaction_type_code", transactionType);
  secureParms.put("transaction_status",    transactionStatus);
  
  secureParms.put("transaction_instrument_info", transaction.getAttribute("transaction_oid") + "/" + instrument.getAttribute("instrument_oid") + "/" + 	transactionType);

  secureParms.put("userOid",userSession.getUserOid());
  secureParms.put("securityRights",userSession.getSecurityRights()); 
  secureParms.put("clientBankOid",userSession.getClientBankOid()); 
  secureParms.put("ownerOrg",userSession.getOwnerOrgOid());
    
  // If the terms record doesn't exist, set its oid to 0.
  String termsOid = terms.getAttribute("terms_oid");

  if (termsOid == null)
  {
     termsOid = "0";
  }
  secureParms.put("terms_oid", termsOid);

  if (isTemplate)
  {
     secureParms.put("template_oid", template.getAttribute("template_oid"));
     secureParms.put("opt_lock",     template.getAttribute("opt_lock"));
  }
%>

  <%@include file="fragments/MultiPartModeFormElements.frag" %>

<%
  if (isTemplate)
  {
%>
    <%@ include file="fragments/TemplateHeader.frag" %>
<%
  }
  
  String extraPartTags = request.getParameter("extraPartTags");
%>

  
 <%@ include file="fragments/Instruments-AttachDocuments.frag" %>

<%

  if (rejectionIndicator.equals(TradePortalConstants.INDICATOR_YES) || rejectionIndicator.equals(TradePortalConstants.INDICATOR_X))
  {
%>
   <%=widgetFactory.createSectionHeader("0", "Rejection Reason") %> 
    <%@ include file="fragments/Transaction-RejectionReason.frag" %>
    </div>
<%
  }
  
    
  if (!isTemplate)
  {
%>
     <%-- %@ include file="fragments/Transaction-TemplateDropDown.frag" % --%>	
<%
  }

if (!multiPartMode || whichPart.equals(TradePortalConstants.PART_TWO))
  {
%>

<% //CR 821 Added Repair Reason Section 
StringBuffer repairReasonWhereClause = new StringBuffer();
int  repairReasonCount = 0;
	
	/*Get all repair reason's count from transaction history table*/
	repairReasonWhereClause.append("p_transaction_oid = ?"  );
	repairReasonWhereClause.append(" and REPAIR_REASON is not null order by ACTION_DATETIME");
	
	String transOid = transaction.getAttribute("transaction_oid");
	Long intOb = (transOid !=null)?Long.parseLong(transOid):Long.parseLong("0");

	Debug.debug("repairReasonWhereCondition query : " + repairReasonWhereClause);
	repairReasonCount = DatabaseQueryBean.getCount("A_USER_OID", "transaction_history", repairReasonWhereClause.toString(), false, new Object[]{intOb});
			
if ( !(TradePortalConstants.TRANS_STATUS_PROCESSED_BY_BANK.equalsIgnoreCase(transactionStatus)) && repairReasonCount>0  ){%>
	<%=widgetFactory.createSectionHeader("", "TransactionHistory.RepairReason") %>
		<%@ include file="fragments/Transaction-RepairReason.frag" %>
	</div>
<%} %> 

	
     <%@ include file="fragments/Transaction-FTDP-ISS-CreditDebitDetails.frag" %>
     
     <% if (TradePortalConstants.TRANS_STATUS_PROCESSED_BY_BANK.equalsIgnoreCase(transactionStatus) && repairReasonCount>0  ){%>
		<%=widgetFactory.createSectionHeader("3", "TransactionHistory.RepairReason", null, true) %>
		<%@ include file="fragments/Transaction-RepairReason.frag" %>
		</div>
    <%} %> 
     
<%
  }
%>

  <jsp:include page="/transactions/PartNavigationBar.jsp">
     <jsp:param name="multiPartMode" value="<%=false%>" />
     <jsp:param name="whichPart"     value="<%=whichPart%>" />
     <jsp:param name="isHeader"      value="false" />
     <jsp:param name="part1TextKey"  value="FundsTransferRequest.Part1" />
     <jsp:param name="part2TextKey"  value="FundsTransferRequest.Part2" />
     <jsp:param name="readOnly"      value="<%=isReadOnly%>" />
     <jsp:param name="extraTags"     value="<%=extraPartTags%>" />
  </jsp:include>

  <%
//PMitnala Rel 8.3 IR#T36000021096 Moved the code from top of the page and adding an additional check to see if payment has multiple beneficiaries
//Sandeep - Rel 8.3 System Test IR# T36000020739 09/16/2013 - Begin
  boolean pmtBenPanelAuthInd = false;
  if (TradePortalConstants.INDICATOR_YES.equals((String)transaction.getAttribute("bene_panel_auth_ind")) && numberOfDomPmts>1)
	   pmtBenPanelAuthInd = true;
//Sandeep - Rel 8.3 System Test IR# T36000020739 09/16/2013 - End
  if (!isReadOnly)
  {
  	radioSelectSection = true;   
  %>
	
  <%
  }

   // Only display the bank defined section if the user is an admin user
    // IR-42689 REL 9.4 SURREWSH Start
   if (userSession.getSecurityType().equals(TradePortalConstants.ADMIN) || userSession.hasSavedUserSession())
   {
%>
     
<%
   }
%>   

 
  <%@ include file="fragments/PartySearchPrep.frag" %>
  <%@ include file="fragments/BankSearchPrep.frag" %>
 
  <%@ include file="fragments/BankClearPrep.frag" %>
 
<%= formMgr.getFormInstanceAsInputField("Transaction-FTDPForm", secureParms) %>


</div> 	<%-- Form Content End --%>
</div>	<%--  Form Area End --%>


<div class="formSidebar" data-dojo-type="widget.FormSidebar" data-dojo-props="title: 'My Tools', form: 'TransactionFTDP'">
	<jsp:include page="/common/Sidebar.jsp">
		<jsp:param name="links" value="<%=links%>" />
		<jsp:param name="isReadOnly" value="<%=isReadOnly%>" />
		<jsp:param value="<%=certAuthURL%>" name="certAuthURL" />
		<jsp:param name="buttonPressed" value="<%=bButtonPressed%>" />
		<jsp:param name="error" value="<%=error%>" />  
		<jsp:param name="formName" value="0" />
		<jsp:param name="isTemplate" value="<%=isTemplate%>"/>
		<jsp:param name="showLinks" value="true" />  
		<jsp:param name="isNewTransaction" value="<%=newTransaction%>"/>
		<jsp:param name="pmtBenPanelAuthInd" value="<%=pmtBenPanelAuthInd%>"/>
		</jsp:include>
</div>

<%
if( currentDPOid != null && !"".equalsIgnoreCase(currentDPOid) ){
	currentDPOid = EncryptDecrypt.encryptStringUsingTripleDes(currentDPOid, userSession.getSecretKey());
}else{
	currentDPOid = "";
}
%>
<input dojoType="dijit.form.TextBox" type="hidden" name="DomesticPaymentOID" id="DomesticPaymentOID" value="<%=currentDPOid %>"/>
<div id="PartySearchDialog"></div>
<div id="BankSearchDialog"></div>
<input type="hidden" name="selection" />
<input type="hidden" name="ChoosePrimary" />
<div id="MarketRate" ></div>
</form> 



</div>		<%--  Page Content End --%> 
</div>		<%--  Page Main End --%>

<div class="pageFooter">	<%--  Page Footer Start --%>
<div class="pageContent">	<%--  Page Content Start --%>
<%-- KMehta on 12 May 2014 @ Rel 90 for IR T36000025449 Start --%>
<jsp:include page="/common/Footer.jsp">
   <jsp:param name="autoSaveFlag"           value="<%=autoSaveFlag%>" />
   <jsp:param name="templateFlag"           value="<%=templateFlag%>" />
   <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>
<%-- KMehta on 12 May 2014 @ Rel 90 for IR T36000025449 End --%>
</div>		<%-- Page Content End --%>
</div>		<%-- Page Footer End --%>

<jsp:include page="/common/SidebarFooter.jsp"/>

<%
  if (!isReadOnly && InstrumentServices.isNotBlank(focusField)) {
%>
<script>
  require(["dijit/registry", "dojo/ready"],
    function(registry, ready ) {
      ready(function() {
    	  
    	var focusFieldId = '<%= focusField %>';
        if ( focusFieldId ) {
          var focusField = registry.byId(focusFieldId);
          if ( focusField ) {
          focusField.focus();
          }
        }
		<% 
		  //03/18/2014 R84 - T36000019732 - Populating reportingCodes
		  //if (!(isReadOnly || isStatusVerifiedPendingFX || isFixedPayment)) {
		  if (!(isReadOnly || isStatusVerifiedPendingFX )) {
		%>
		
		if (dijit.byId("AppDebitAcctPrincipal") && dijit.byId("AppDebitAcctPrincipal").attr('value')) {
		    updateReportingCodes();
		}
		
		<%if(isFixedTemplate){%>
		  setAllowModificationCheck();
		<%
		 }
		}
		%>
      });
  });
  

</script>
<%
  }
%>

<%
if (!multiPartMode || whichPart.equals(TradePortalConstants.PART_TWO))
  {
%>
     <%@ include file="/transactions/fragments/Transaction-FTDP-ISS-CreditDebit-Footer.frag" %>
<%
  }
%>


<script type="text/javascript">


function setAccount(id) {
	var arr = document.getElementsByName(id);
	if (arr != null) {
     	document.forms[0].PayeeAccount.value = arr[0].value;
    }
}



	
function setAccountSelectType(id,idx) {
	var arr = document.getElementsByName(id);
	if (arr != null) {
		var val = arr[0].value;
		if (val.length > 0) {
			document.forms[0].AccountSelectType[idx].checked='true';
			document.forms[0].PayeeAccount.value = val; 
	    }
	}
}
  
function filterPayments(buttonId) {
	
		
		
    require(["dojo/dom"],
      function(dom){
        
        var searchParms="transaction_oid=<%=transaction.getAttribute("transaction_oid")%>";
       
        var benificiaryName = dom.byId("BenificiaryName").value;
        var benificiaryBankName = dom.byId("BenificiaryBankName").value;;
        var paymentStatusSearch = dijit.byId("PaymentStatusSearch").attr('value');
        var amount = dom.byId("Amount").value;
        
            if(buttonId=="ModifyPayee"){
		       	searchParms+="&benificiaryName="+benificiaryName.toUpperCase();
		       	searchParms+="&benificiaryBankName="+benificiaryBankName.toUpperCase();
		       	searchParms+="&paymentStatusSearch="+paymentStatusSearch.toUpperCase();
		       	searchParms+="&amount="+amount;
	        }
	    

        

		searchDataGrid("TransactionFTDPISSDataGridId", "TransactionFTDPISSDataView", searchParms);
		
		
	});
}


</script> 

</body>
</html>

<%
   // Finally, reset the cached document to eliminate carryover of
   // information to the next visit of this page.
   formMgr.storeInDocCache("default.doc", new DocumentHandler());
%>

<%!
   // Various methods are declared here (in alphabetical order).


public String getExpressIndicator(boolean showIndicator) {
      if (showIndicator) {
         return "<span class=Express>#</span>";
      } else {
         return "";
      }
   }

   public String getRequiredIndicator(boolean showIndicator) {
      if (showIndicator) {
         return "<span class=Asterix>*</span>";
      } else {
         return "";
      }
   }
   
   // IAZ CM CR-507 12/19/09 Begin
   // Method to build protected TextArea for Bank Parties
   public String buildPaymentBankDisplayData(String bankName, String branchName, String addr1, String addr2, String addr3, boolean readOnly)
   {
		return buildPaymentBankDisplayData(bankName, branchName, addr1, addr2, addr3, null, readOnly);
   }
   
   public String buildPaymentBankDisplayData(String bankName, String branchName, String addr1, String addr2, String addr3, String country, boolean readOnly)
   {
    //if (InstrumentServices.isBlank(bankName))
    //	return "";
    String eol = "\n";
   	StringBuffer bankDData = new StringBuffer();
   	appendPaymentBankDisplayData(bankDData, eol, bankName);
   	appendPaymentBankDisplayData(bankDData, eol, branchName);
   	appendPaymentBankDisplayData(bankDData, eol, addr1);
   	appendPaymentBankDisplayData(bankDData, eol, addr2);
   	appendPaymentBankDisplayData(bankDData, eol, addr3);
   	appendPaymentBankDisplayData(bankDData, " ", country);
   	return bankDData.toString();	
   }
   
   private void appendPaymentBankDisplayData(StringBuffer bankDData, String separator, String data) {
      	if (InstrumentServices.isNotBlank(data)) {
          	if (bankDData.length()>0) bankDData.append(separator);
          	bankDData.append(data);
      	}
   }
   
   //Method to populate Payment Party/Banks from Doc Cache
   public void populatePaymentParty(PaymentPartyWebBean interBank, DocumentHandler intBankDoc, String partyPointer)
   {
       	interBank.setAttribute("bank_name", intBankDoc.getAttribute("/DomesticPayment/" + partyPointer + "/bank_name"));
       	interBank.setAttribute("bank_branch_code", intBankDoc.getAttribute("/DomesticPayment/" + partyPointer + "/bank_branch_code"));
       	interBank.setAttribute("address_line_1", intBankDoc.getAttribute("/DomesticPayment/" + partyPointer + "/address_line_1"));
       	interBank.setAttribute("address_line_2", intBankDoc.getAttribute("/DomesticPayment/" + partyPointer + "/address_line_2"));
       	interBank.setAttribute("address_line_3", intBankDoc.getAttribute("/DomesticPayment/" + partyPointer + "/address_line_3"));
       	interBank.setAttribute("address_line_4", intBankDoc.getAttribute("/DomesticPayment/" + partyPointer + "/address_line_4"));       	
       	interBank.setAttribute("branch_name", intBankDoc.getAttribute("/DomesticPayment/" + partyPointer + "/branch_name"));
       	interBank.setAttribute("country", intBankDoc.getAttribute("/DomesticPayment/" + partyPointer + "/country"));
   }
   // IAZ CM CR-507 12/19/09 End
%>

<script>

function setAccount(id, multi) {          
	require(["dojo/dom","dijit/registry","dojo/domReady!" ], function(dom,registry) {
	<%--   
	var arr = document.getElementsByName(id);
	if (arr != null) {
     	document.forms[0].PayeeAccount.value = arr[0].value;
    }
	 --%>
	var ele=dom.byId(id);
	if (ele) {
		dom.byId("PayeeAccount").value = ele.value;
	}
	
	if (multi && id == "EnteredAccount") {
	   dijit.byId(id+"RDO").set('checked', true);
	}
	
	
	});
}

<%-- 

function setUserSelAccount() {          
	setAccount("EnteredAccount");
}



function setUserEnteredAccount() {
	var ele=dojo.byId("EnteredAccount");
	if (ele && ele.value.lenght > 0) {
		dojo.byId("PayeeAccount").value = ele.value;
	    }
	}
}

 --%>


<% 	if (!isReadOnly) {	%>
<%-- Delete Beneficiary details --%>
function deleteBeneficiaryRow(){

         <%--  SureshL IR-T36000027758 05/22/2014 Start --%>
	 var   confirmMessage = "<%=StringFunction.asciiToUnicode(resMgr.getText("FutureValueTransactions.PopupMessage",TradePortalConstants.TEXT_BUNDLE)) %>"; 
         <%--  SureshL IR-T36000027758  05/22/2014 Start --%>
 <%-- jgadela 09/05/2013 REL 8.3 IR T36000020527 - Removed the return statement when confirm returns false. --%>
     var confirmDelete = confirm(confirmMessage);
	 if (confirmDelete == true) 
	 {
				var delRow = getSelectedGridRowKeys("TransactionFTDPISSDataGridId");
				dijit.byId("DomesticPaymentOID").set("value",delRow);
				setButtonPressed('<%=TradePortalConstants.BUTTON_DELETE_PAYEE%>', 0); 
				document.forms[0].submit(); 
	 }	
	<%-- jgadela 09/05/2013 REL 8.3 IR T36000020527 - [END] --%>
}

<%}%>

var myGrid = "";
var local = {};

<%--  jgadela R8.4 IR 19732 - Made the changes for payment instrument which is selected from fixed payment template. --%>
function updateBeneAccount(param, fixedTemplate) {
	
	var readonly = <%=(isReadOnly || isStatusVerifiedPendingFX)%>
	readonly = readonly || fixedTemplate;
 require(["dojo/_base/xhr","t360/common","dojo/_base/array","dijit/registry","dojo/dom-style","dojo/dom-construct","dojo/dom","dojo/domReady!"],
      function(xhr, common, arrayUtil, registry,domStyle,domConstruct,dom) {
	
	<%-- make an ajax call to get the account info  --%>
	var beneAccounts = dom.byId("beneAccountsData");
      var beneAccountsWidgets = registry.findWidgets(beneAccounts);
      arrayUtil.forEach(beneAccountsWidgets, function(entry, i) {
        entry.destroyRecursive(true);
      });
      domConstruct.empty(beneAccounts);
	  domStyle.set("beneAccountsDataLoading", 'display', 'inline-block');
      common.attachAjaxResult('beneAccountsData',
        "/portal/transactions/Transaction-FTDP-ISS-BeneAcctData.jsp?partyType=PAE&isTemplate=<%=isTemplate%>"+"&isReadOnly="+readonly +""+ param,
        local.beneAccountsDataLoadedCallback);
	});
}
                                
<%-- Used to clear beneficiary details --%>
function clearAllBeneficiaryFormDetails(){ 
	require(["dojo/dom", "dijit/registry","dojo/domReady!"], function(dom, registry) {	
	<%--  jgadela R8.4 IR 19732 - Made the changes for instrument which is selected from fixed payment template. --%>
	updateBeneAccount("&clear=Y",false);

	
<%-- AiA --%>
<%-- IR T36000018376 - 06/26/2013 --%>
<%-- There was a run time error sometimes happening in this method --%>
<%-- when some of the objects are null. So, added a try catch so that --%>
<%-- it fails gracefully but the method executes to the end - unlike before when it was  --%>
<%-- simply bailing out - START --%>

<%-- 04/02/2014 R 84 IR T36000019732 [Start]  -  In the case of Fixed template --%>
try{
	   <%--  Inthe case of Fixed template --%>
	   if(document.getElementById("PayeeName"))
		registry.byId("PayeeName").set('readOnly',false);
       if(document.getElementById("EnteredAccount"))
		registry.byId("EnteredAccount").set('readOnly',false);
       if(document.getElementById("PayeeAddrL1"))
	   		registry.byId("PayeeAddrL1").set('readOnly',false); 
	   if(document.getElementById("PayeeAddrL2"))
	   		registry.byId("PayeeAddrL2").set('readOnly',false); 
	   if(document.getElementById("PayeeAddrL3"))
	   	registry.byId("PayeeAddrL3").set('readOnly',false);  

	   if(document.getElementById("PayeeAddrL4"))
	   	registry.byId("PayeeAddrL4").set('readOnly',false);  		
	   if(document.getElementById("Country"))
	   	registry.byId("Country").set('readOnly',false);  			
	   if(document.getElementById("payeeFaxNumber"))
	   	registry.byId("payeeFaxNumber").set('readOnly',false);  		
	   if(document.getElementById("payeeEmail"))
	   	registry.byId("payeeEmail").set('readOnly',false);  
	   if(document.getElementById("payeeInstructionNumber"))
	   	registry.byId("payeeInstructionNumber").set('readOnly',false);  		
	   if(document.getElementById("PayeeBankCode"))
	   	registry.byId("PayeeBankCode").set('readOnly',false);  				
	   if(document.getElementById("FirstIntBankBranchCode"))
	   	registry.byId("FirstIntBankBranchCode").set('readOnly',false);  		
	   if(document.getElementById("central_bank_reporting_1"))
	   	registry.byId("central_bank_reporting_1").set('readOnly',false);  	
	   if(document.getElementById("central_bank_reporting_2"))
	   	registry.byId("central_bank_reporting_2").set('readOnly',false);  	
	   if(document.getElementById("central_bank_reporting_3"))
	   	registry.byId("central_bank_reporting_3").set('readOnly',false);  	
	   if(document.getElementById("reporting_code_1"))
	   	registry.byId("reporting_code_1").set('readOnly',false); 			
	   
	   if(document.getElementById("reporting_code_2"))
		registry.byId("reporting_code_2").set('readOnly',false);    			
	   
	   if(document.getElementById("PayableLocation"))
	   	registry.byId("PayableLocation").set('readOnly',false);    			
	   if(document.getElementById("PrintLocation"))
	   	registry.byId("PrintLocation").set('readOnly',false);   				
	   if(document.getElementById("mailing_address_line_1"))
	   	registry.byId("mailing_address_line_1").set('readOnly',false);   	
	   if(document.getElementById("mailing_address_line_2"))
	   	registry.byId("mailing_address_line_2").set('readOnly',false);   	
	   if(document.getElementById("mailing_address_line_3"))
	   	registry.byId("mailing_address_line_3").set('readOnly',false);   	
	   if(document.getElementById("mailing_address_line_4"))
	   	registry.byId("mailing_address_line_4").set('readOnly',false);   	
	   if(document.getElementById("payeeInvoiceDetails"))
	   	registry.byId("payeeInvoiceDetails").set('readOnly',false);   		
	   if(document.getElementById("PaymentMethodType"))
		   registry.byId("PaymentMethodType").set('readOnly',false);
	   
	   if(document.getElementById("DomesticPaymentAmount"))
		  registry.byId("DomesticPaymentAmount").set('readOnly',false);
	    
	   if(document.getElementsByName("BankChargesType")){
		   registry.byId("TradePortalConstants.CHARGE_OUR_ACCT").set('readOnly',false);
		   registry.byId("TradePortalConstants.CHARGE_BEN_FOR_OUTSIDE").set('readOnly',false);
		   registry.byId("TradePortalConstants.CHARGE_OTHER").set('readOnly',false);
	   }
	   
	   if(document.getElementById("PayerDescription"))
		   registry.byId("PayerDescription").set('readOnly',false);	
       if(document.getElementById("PayeeMessage"))
    	   registry.byId("PayeeMessage").set('readOnly',false);		
}catch(err){

}
<%-- 04/02/2014 R 84 IR T36000019732 [Start] --%>

try{
	
	<%-- 03/18/2014 R84 - T36000019732 - null check for objects  --%>
	if(dijit.byId("PaymentMethodType"))
		dijit.byId("PaymentMethodType").set("value","");
	
	if(dijit.byId("TradePortalConstants.CHARGE_OUR_ACCT"))
		dijit.byId("TradePortalConstants.CHARGE_OUR_ACCT").set('checked', false);
	
	if(dijit.byId("radePortalConstants.CHARGE_BEN_FOR_OUTSIDE"))
		dijit.byId("radePortalConstants.CHARGE_BEN_FOR_OUTSIDE").set('checked', false);
	
	if(dijit.byId("radePortalConstants.CHARGE_OTHER"))
		dijit.byId("radePortalConstants.CHARGE_OTHER").set('checked', false);

	<%-- dijit.byId("TransactionCurrencyDisplay").set("value",""); --%>
	if(dijit.byId("DomesticPaymentAmount"))
		dijit.byId("DomesticPaymentAmount").set("value","");
	
	if(dijit.byId("ValueDate"))
		document.getElementById("ValueDate").innerHTML = "";
	
	if(dijit.byId("PayerDescription"))
		dijit.byId("PayerDescription").set("value","");	
	
	if(dijit.byId("PayeeMessage"))
		dijit.byId("PayeeMessage").set("value","");
	<%-- dijit.byId("EnteredAccount").set("value",""); --%>
	
	if(dijit.byId("PayeeAccount"))
		dom.byId("PayeeAccount").value = "";
	
	if(dijit.byId("PayeeName"))
		dijit.byId("PayeeName").set("value","");
	
	if(dijit.byId("PayeeAddrL1"))
		dijit.byId("PayeeAddrL1").set("value","");
	
	if(dijit.byId("PayeeAddrL2"))
		dijit.byId("PayeeAddrL2").set("value","");
	
	if(dijit.byId("PayeeAddrL3"))
		dijit.byId("PayeeAddrL3").set("value","");
	
	if(dijit.byId("PayeeAddrL4"))
		dijit.byId("PayeeAddrL4").set("value","");
	
	if(dijit.byId("Country"))
		dijit.byId("Country").set("value","");
	
	if(dijit.byId("payeeFaxNumber"))
		dijit.byId("payeeFaxNumber").set("value","");
	
	if(dijit.byId("payeeEmail"))
		dijit.byId("payeeEmail").set("value","");
	
	if(dijit.byId("payeeInstructionNumber"))
		dijit.byId("payeeInstructionNumber").set("value","");
	
	if(dijit.byId("PayeeBankCode"))
		dijit.byId("PayeeBankCode").set("value","");	
	<%-- dijit.byId("PayeeBankName").set("value",""); --%>
	
	if(dijit.byId("PayeeBankData"))
		dijit.byId("PayeeBankData").set("value","");
	
	if(dijit.byId("FirstIntBankBranchCode"))
		dijit.byId("FirstIntBankBranchCode").set("value","");
	
	if(dijit.byId("FirstIntCode"))
		dijit.byId("FirstIntCode").set("value","");
	
	if(dijit.byId("FirstIntBankData"))
		dijit.byId("FirstIntBankData").set("value","");
	
	if(dijit.byId("FirstIntBankName"))
		dijit.byId("FirstIntBankName").set("value","");
	
	if(dijit.byId("FirstIntBankBranchName"))
		dijit.byId("FirstIntBankBranchName").set("value","");
	
	if(dijit.byId("FirstIntBankAddressLine1"))
		dijit.byId("FirstIntBankAddressLine1").set("value","");
	
	if(dijit.byId("FirstIntBankAddressLine2"))
		dijit.byId("FirstIntBankAddressLine2").set("value","");
	
	if(dijit.byId("FirstIntBankAddressLine3"))
		dijit.byId("FirstIntBankAddressLine3").set("value","");
	
	if(dijit.byId("FirstIntBankCountry"))
		dijit.byId("FirstIntBankCountry").set("value","");

	dijit.byId("central_bank_reporting_1").set("value","");
	dijit.byId("central_bank_reporting_2").set("value","");
	dijit.byId("central_bank_reporting_3").set("value","");
	
	if(dijit.byId("DeliveryMethod"))
		dijit.byId("DeliveryMethod").set("value","");
	dijit.byId("reporting_code_1").set("value","");
	dijit.byId("reporting_code_2").set("value","");
	
	if(dijit.byId("FirstIntBankData"))
		dijit.byId("FirstIntBankData").set("value","");
	
	if(dijit.byId("PayableLocation"))
		dijit.byId("PayableLocation").set("value","");
	
	if(dijit.byId("PrintLocation"))
		dijit.byId("PrintLocation").set("value","");
	
	dijit.byId("mailing_address_line_1").set("value","");	
	dijit.byId("mailing_address_line_2").set("value","");
	dijit.byId("mailing_address_line_3").set("value","");
	dijit.byId("mailing_address_line_4").set("value","");
	
	if(dijit.byId("payeeInvoiceDetails"))
		dijit.byId("payeeInvoiceDetails").set("value","");
	if(dijit.byId("InvoiceDetailOID"))
		dom.byId("InvoiceDetailOID").value = "";
	if(dijit.byId("ErrorText"))
		dijit.byId("ErrorText").set("value","");
	if(dijit.byId("DomesticPaymentOID"))
		dijit.byId("DomesticPaymentOID").set("value","");
   	
}catch(err){}
	<%-- 03/18/2014 R 84 IR T36000019732 [Start] --%>
		try{
	    document.getElementById("payben").style.display = "block";
		document.getElementById("ClearBenSearchButton").style.display = "block";
		document.getElementById("payments_banksearch").style.display = "block";
		document.getElementById("clearBenBankSearchButton").style.display = "block";
		document.getElementById("ClearInterButton").style.display = "block";
		document.getElementById("payments_firstintermediarybank").style.display = "block";
	}catch(err){}
	<%--  03/18/2014 R 84 IR T36000019732 [END] --%>

<%-- IR T36000018376 - END --%>
	dijit.byId("TransactionFTDPISSDataGridId").selection.clear();
	
	<% if (!(isReadOnly || isStatusVerifiedPendingFX || TradePortalConstants.INDICATOR_YES.equals(transaction.getAttribute("uploaded_ind")))) {%>
		document.getElementById("update_ben_button_div").style.display = "none";
		document.getElementById("save_ben_button_div").style.display = "block";
	    dijit.byId("addBeneficaryButton").setAttribute('disabled', true);
	<%}%>
   	
	
});
}

function reloadBeneficiaryFormDetails(){
	
	myGrid = dijit.byId("TransactionFTDPISSDataGridId");

	if( dijit.byId("DomesticPaymentOID") && "" != dijit.byId("DomesticPaymentOID").value ){
		 myGrid.selection.addToSelection(dijit.byId("DomesticPaymentOID").value); 
    	 
	}else{
		clearAllBeneficiaryFormDetails();
	}
}

<%-- Clears Beneficiary informations --%>
function clearBenSearch() {

	dijit.byId("EnteredAccount").set("value","");
	dijit.byId("PayeeName").set("value","");
	
	dijit.byId("PayeeAddrL1").set("value","");
	dijit.byId("PayeeAddrL2").set("value","");
	dijit.byId("PayeeAddrL3").set("value","");	
	dijit.byId("PayeeAddrL4").set("value","");
	
	dijit.byId("Country").set("value","");
	dijit.byId("payeeFaxNumber").set("value","");
	dijit.byId("payeeEmail").set("value","");
	dijit.byId("payeeInstructionNumber").set("value","");
  
  
}

  <%-- Clears Beneficiary Bank informations  --%>
  function clearBenBankSearch() {
    <%-- cquinton 2/8/2013 fix clear issue --%>
    require(["dojo/dom", "dijit/registry"], function(dom, registry) {

	if(registry.byId("PayeeBankBranchCode")) {
		registry.byId("PayeeBankBranchCode").set("value","");
	}
	if(registry.byId("PayeeBankCode")) {
		registry.byId("PayeeBankCode").set("value","");
	}
	if(registry.byId("PayeeBankData")) {
		registry.byId("PayeeBankData").set("value","");
	}
	if(dom.byId("PayeeBankName")) {
          dom.byId("PayeeBankName").value = '';
	}
	if(dom.byId("PayeeBankAddressLine1")) {
          dom.byId("PayeeBankAddressLine1").value = '';
	}
	if(dom.byId("PayeeBankAddressLine2")) {
          dom.byId("PayeeBankAddressLine2").value = '';
	}
	if(dom.byId("PayeeBankAddressLine3")) {
          dom.byId("PayeeBankAddressLine3").value = '';
	}
	if(dom.byId("PayeeBankCountry")) {
          dom.byId("PayeeBankCountry").value = '';
	}
	if(dom.byId("PayeeBankBranchName")) {
          dom.byId("PayeeBankBranchName").value = '';
	}
    });
  }

<%-- Clears Inter Bank informations  --%>
function clearInterBankSearch() {

	if(dijit.byId("FirstIntCode"))
		dijit.byId("FirstIntCode").set("value","");
	if(dijit.byId("FirstIntBankBranchCode"))
		dijit.byId("FirstIntBankBranchCode").set("value","");
	if(dijit.byId("FirstIntBankData"))
		dijit.byId("FirstIntBankData").set("value","");
}

<%-- populate Grid Data to form --%>
require(["dojo/ready","dijit/registry","dojo/_base/array", "dojo/dom-attr"], function(ready,registry,baseArray,domAttr) {
	
	 ready(function(){ 
		firstrun=true;
	
			myGrid = registry.byId("TransactionFTDPISSDataGridId");
			
			
		    myGrid.on("SelectionChanged", function(){
		        <%--   this will be an array of dojo/data items --%>
		        var items = this.selection.getSelected();
		       
		     
		        <%--   get the data of each column of selected grid and set the data to fields in the form --%>
		        baseArray.forEach(items, function(item){		        	   
		        	
		        	    
		        	    
		        	    
		        	    
		        	    dijit.byId("DomesticPaymentOID").set("value",this.store.getValue(item, "rowKey"));
		        	    dijit.byId('DomesticPaymentOID').set('checked', true);
		        	  
		        	   setBeneficiaryDetails(myGrid, item);
		        	   
		        }, this);
		       
		        }, true);
		    
		    <%-- Used to fetch the grid's selected row. --%>
		    dojo.connect(myGrid,"_onFetchComplete",function(items){ 
		    	
		    	if("" != dijit.byId("DomesticPaymentOID").value){
			    	 dojo.forEach(items, function(i){
	                     if(dijit.byId("DomesticPaymentOID").value == myGrid.store.getValue(i, "rowKey")){	                    	
							if (firstrun <%=((getDataFromDoc) ? " && true" : " && false")%> ) {
							     var inIndex = myGrid.getItemIndex(i);
								 myGrid.selection.selectedIndex = inIndex;
								var rowNode = myGrid.getRowNode(inIndex);
								if(rowNode){
									domAttr.set(rowNode, "aria-selected", "true");
								}
								<%-- myGrid.selection._beginUpdate(); --%>
								myGrid.selection.selected[inIndex] = true;
								<%-- this.grid.onSelected(inIndex); --%>
								<%-- this.onSelected(inIndex); --%>
								<%-- this.onSetSelected(inIndex, true); --%>
								<%-- myGrid.selection._endUpdate(); --%>
								firstrun = false;
                               							 
						     } else {								 
	                    	   myGrid.selection.addToSelection(myGrid.getItemIndex(i)); 
	                    	   dijit.byId("DomesticPaymentOID").set("value",myGrid.store.getValue(i, "rowKey"));
	                    	}
	                     }
	                 });
		    	}
	         }); 
		    
		    if(<%=numberOfDomPmts%> > 1){
		    	document.getElementById("add_ben_div").style.display = "block";		    	
		    }else if(<%=numberOfDomPmts%> == 0){
		    	document.getElementById("add_ben_div").style.display = "none";		    	
		    }
			
			dojo.style("requestMarketRate", "visibility", "<%=requestMarketRateVisibility%>");
			var display;
			if('<%=requestMarketRateVisibility%>' == 'hidden') display='none';
			if('<%=requestMarketRateVisibility%>' == 'visible') display='block';
			dojo.style("requestMarketRate", "display", display);
			
		    require(["dojo/dom-class"], function(domClass){
				<% if(TradePortalConstants.CROSS_BORDER_FIN_TRANSFER.equalsIgnoreCase(currentDomesticPayment.getAttribute("payment_method_type")) ){ %>
					dijit.byId("PayeeName").setAttribute("maxLength",35);
					
					domClass.add('domesticCountryLabel', 'required');
					domClass.add('enterAccountLabelDiv', 'required');
					document.getElementById("payeeAddrLine4").style.display = "none";
					document.getElementById("checkDeliveryDetailDivID").style.display = "none";
				<% }else if( TradePortalConstants.PAYMENT_METHOD_BCHK.equalsIgnoreCase(currentDomesticPayment.getAttribute("payment_method_type"))
					|| TradePortalConstants.PAYMENT_METHOD_CCHK.equalsIgnoreCase(currentDomesticPayment.getAttribute("payment_method_type")) ){ %>
						domClass.add('deliveryMethodID', 'required');
						domClass.remove('benBankCodeLabel', 'required');
						domClass.remove('enterAccountLabelDiv', 'required');
						document.getElementById("instructionNumber").style.display = "none";
						if(document.getElementById("payeeEmailId")){
						   document.getElementById("payeeEmailId").style.display = "none";
						}
						document.getElementById("benBankDivID").style.display = "none";
						document.getElementById("firstIntBankDivID").style.display = "none";
						document.getElementById("regulationReportingDivID").style.display = "none";
				<% } else if( TradePortalConstants.PAYMENT_METHOD_RTGS.equalsIgnoreCase(currentDomesticPayment.getAttribute("payment_method_type")) 
					|| TradePortalConstants.PAYMENT_METHOD_ACH_GIRO.equalsIgnoreCase(currentDomesticPayment.getAttribute("payment_method_type")) 
					|| TradePortalConstants.PAYMENT_METHOD_BKT.equalsIgnoreCase(currentDomesticPayment.getAttribute("payment_method_type")) ){ %>
						document.getElementById("firstIntBankDivID").style.display = "none";
						document.getElementById("checkDeliveryDetailDivID").style.display = "none";		
						domClass.add('enterAccountLabelDiv', 'required');
				<%}else{%>
					domClass.add('enterAccountLabelDiv', 'required');
				<%}%>
		    	
		    });
			
			
<% 
    if (!(isReadOnly || isStatusVerifiedPendingFX || TradePortalConstants.INDICATOR_YES.equals(transaction.getAttribute("uploaded_ind")))) {
%>
		
		    
		    if("" == dijit.byId("DomesticPaymentOID").value){	    
		    	document.getElementById("update_ben_button_div").style.display = "none";
        	   	document.getElementById("save_ben_button_div").style.display = "block";
        	   	dijit.byId("addBeneficaryButton").setAttribute('disabled', true);
		    }else{
		    	document.getElementById("update_ben_button_div").style.display = "block";
        	   	document.getElementById("save_ben_button_div").style.display = "none";
        	   	dijit.byId("addBeneficaryButton").setAttribute('disabled', false);
		    }
			
<%}%>			
		    
	 });
     
   });
 



  require(["dojo/_base/array", "dojo/_base/xhr", "dojo/dom", "dojo/dom-construct", "dojo/dom-style", "dijit/registry", "t360/common"],
      function(arrayUtil, xhr, dom, domConstruct, domStyle, registry, common) {


    <%-- cquinton 1/4/2013 after populating the party fields we also  --%>
    <%--  need to get party account information for population here --%>
    local.chooseBeneficiary = function(selectedRowKey) {
    <%--  jgadela R8.4 IR 19732 - Made the changes for instrument which is selected from fixed payment template. --%>
	updateBeneAccount("&partyRowKey="+selectedRowKey, <%=isFixedPayment%>);

      <%-- also load the beneficiary bank data --%>
      <%-- this is done by getting the designated party data from the selected party --%>
      <%--  and then putting the output into the beneficiary bank fields --%>
      <%-- todo: should use servlet and json for this! --%>
      <%-- todo: better xml parsing... (if no json) --%>
      function getDataValue(data, attributeName) {
        var openTag = '<'+attributeName+'>';
        var closeTag = '</'+attributeName+'>';
        var myValue = "";
        var openTagIdx = data.indexOf(openTag);
        if ( openTagIdx >= 0 ) {
          myValue = data.substring( openTagIdx+openTag.length); 
          var closeTagIdx = myValue.indexOf(closeTag);
          if ( closeTagIdx>=0 ) {
            myValue = myValue.substring( 0, myValue.indexOf(closeTag) );
          }
        }
        return myValue;
      }
      function setWidgetValue(widgetId, data, attributeName) {
        var myValue = getDataValue(data, attributeName);
        var myWidget = registry.byId(widgetId);
        if ( myWidget ) {
          myWidget.set('value',myValue);
        }
      }
      function setInputValue(domId, data, attributeName) {
        var myValue = getDataValue(data, attributeName);
        var myInput = dom.byId(domId);
        if ( myInput ) {
          myInput.value = myValue;
        }
      }      
    }

    <%-- after loading account data, remove the loading message --%>
    local.beneAccountsDataLoadedCallback = function() {
      domStyle.set("beneAccountsDataLoading", 'display', 'none');
    }

  });

   
  
  
function updateReqMarketRate()
	{
	    if (dijit.byId("RequestMarketRateCheckBox").checked) {
	    	document.forms[0].RequestMarketRate.value = 'Y';
		    dijit.byId("covered_by_fec_number").set('value','');
			dijit.byId("fec_rate").set('value','');
 	    } else  {
	    	document.forms[0].RequestMarketRate.value = 'N';
	    }
    }

	
	function uncheckReqMarketRate() {	
		if (dijit.byId("RequestMarketRateCheckBox").checked) {
			if ((dijit.byId("covered_by_fec_number").value) || (dijit.byId("fec_rate").value)) 	{
				dijit.byId("RequestMarketRateCheckBox").set('checked',false);
				document.forms[0].RequestMarketRate.value = 'N';
			}
		}
	}


  var itemid;
  var section;

  function SearchParty(identifierStr, sectionName , partyType){
	
    itemid = String(identifierStr);
    section = String(sectionName);
    partyType= String(partyType);
	
    require(["dojo/dom", "t360/dialog"], function(dom, dialog ) {

      <%-- set the SearchPartyType input so it is included on new party form submit --%>
      var searchPartyTypeInput = dom.byId("SearchPartyType");
      if ( searchPartyTypeInput ) {
        searchPartyTypeInput.value=partyType;
      }

      dialog.open('PartySearchDialog', '<%=PartySearchAddressTitle%>',
        'PartySearch.jsp',
        ['returnAction','filterText','partyType','unicodeIndicator','itemid','section'],
        ['selectTransactionParty','',partyType,'<%=TradePortalConstants.INDICATOR_NO%>',itemid,section], <%-- parameters --%>
        'select',  local.chooseBeneficiary);
    });
  }

function BankSearch(identifierStr, sectionName,bankType){
	itemid = String(identifierStr);
	section = String(sectionName);
	bankType= String(bankType);
	<%-- Srinivasu_D IR#T36000018356 Rel8.2 06/19/2013 - BankBranch Code parms attached to Search --%>
	var bankBranchCode = dijit.byId('PayeeBankCode').value;
	var paymentMethodCode = dijit.byId('PaymentMethodType').value;
	<%if(!isDebitAcctFixedVal){%>
	var debit_account_oid = dijit.byId('AppDebitAcctPrincipal').value;
	<%}else{%>
	var debit_account_oid = document.getElementById('AppDebitAcctPrincipalFixed').value;
	<%}%>
	if (debit_account_oid == '') {
		alert('Debit Account Number and Currency is empty');
		return;
	}

	require(["t360/dialog"], function(dialog ) {
	dialog.open('BankSearchDialog', '<%=BankSearchAddressTitle%>',
	'BankSearch.jsp',
	['returnAction','bankBranchCode','paymentMethodCode','bankType','itemid','section','account_oid'],
	['selectTransactionParty',bankBranchCode,paymentMethodCode,bankType,itemid,section,debit_account_oid], <%-- parameters --%>
	'select', null);
	});	
}

function openCopySelectedDialogHelper(callBackFunction){
    require(["t360/dialog"], function(dialog) {
     
    	  dialog.open('copySelectedInstrument', '<%=resMgr.getTextEscapedJS("CopySelected.Title",TradePortalConstants.TEXT_BUNDLE) %>',
                  'copySelectedInstrumentDialog.jsp',
                  "instrumentType", "FTDP", <%-- parameters --%>
                  'select', callBackFunction);
      });	
}

function copySelectedToInstrument(bankBranchOid, copyType, templateName, templateGroup, flagExpressFixed){
	var rowKeys ;
	 require(["dijit/registry", "dojo/query", "dojo/on", "t360/dialog", "dojo/domReady!"],
		        function(registry, query, on, dialog ) {
		      <%--get array of rowkeys from the grid--%>
		      rowKeys = getSelectedGridRowKeys("paymentInquiriesID");
	 });
	if(copyType == 'I'){ 
		if ( document.getElementsByName('NewInstrumentForm1').length > 0 ) {
		    var theForm = document.getElementsByName('NewInstrumentForm1')[0];
		    theForm.bankBranch.value=bankBranchOid;
		    theForm.copyInstrumentOid.value=rowKeys;
		    theForm.submit();
		  }
	}	
	if(copyType == 'T'){ 
		if ( document.getElementsByName('NewTemplateForm1').length > 0 ) {
			var flag = flagExpressFixed.split('/');
			var expressFlag = flag[0];
			var fixedFlag = flag[1];
			var theForm = document.getElementsByName('NewTemplateForm1')[0];
		    theForm.name.value=templateName;
		    theForm.mode.value="<%=TradePortalConstants.NEW_TEMPLATE%>";
		    theForm.CopyType.value="<%=TradePortalConstants.FROM_INSTR%>";
		    theForm.fixedFlag.value=fixedFlag;
		    theForm.expressFlag.value=expressFlag;
		    theForm.PaymentTemplGrp.value=templateGroup;
		    theForm.copyInstrumentOid.value=rowKeys;
		    theForm.validationState.value = "";
		    theForm.submit();
		  }
	}
}

function setBeneficiaryDetails(grid, benJsObj){

	require(["dojo/_base/xhr","t360/common","dojo/_base/array","dijit/registry","dojo/dom-construct","dojo/dom","dojo/domReady!" ], function(xhr, common, arrayUtil, registry,domConstruct,dom) {
  var rowKey=grid.store.getValue(benJsObj, "rowKey");
  var acctBankOrgid =dom.byId("acctBankOrgid").value;

  var remoteUrl  = "/portal/transactions/Transaction-FTDP-GetBeneficiaryDetail.jsp?rowKey=" + rowKey + "&readOnly=" + <%=isReadOnly%> + "&bankgOid=" + acctBankOrgid+ "&";
	xhr.get({
              url: remoteUrl,
			  handleAs: "json",
			  load: function(data) {

				populateBenDetails(grid, benJsObj, data);
			
			},
			 error: function(error){
			     <%-- todo --%>
			}
		});
		
	
<% 
  if (!isReadOnly) {
%>
	var selectedRowKey = grid.store.getValue(benJsObj, "bene_party_id");
	<%--  jgadela R8.4 IR 19732 - Made the changes for instrument which is selected from fixed payment template. --%>
	updateBeneAccount("&partyRowKey="+selectedRowKey+"&domId="+rowKey, <%=isFixedPayment%>);	
<%}%>
	});
}

<%--  Rel 9.2 - IR T36000036836 - Added encoding for all readonly fields of Beneficiary details --%>
function populateBenDetails(grid, benJsObj, data){
  require(["dojo/dom", "dijit/registry", "t360/textFormatter", "dojox/html/entities", "dojo/domReady!"],
      function( dom, registry, txtFmt, entities ) {
    var seq_num = "";
    var rowKey=grid.store.getValue(benJsObj, "rowKey");
 
<% 
  if (isReadOnly || isStatusVerifiedPendingFX) {
%>
	
    seq_num =   dom.byId("ins_c_id").value + entities.encode(grid.store.getValue(benJsObj, "SEQUENCE_NUMBER"));	
    if (dom.byId("PaymentMethodType")) dom.byId("PaymentMethodType").innerHTML=entities.encode(grid.store.getValue(benJsObj, "PaymentMethod_List"));
		
	if(grid.store.getValue(benJsObj, "BANK_CHARGES_TYPE")=='<%=TradePortalConstants.CHARGE_OUR_ACCT%>'){
		registry.byId('TradePortalConstants.CHARGE_OUR_ACCT').set('checked', true);
	}else if(grid.store.getValue(benJsObj, "BANK_CHARGES_TYPE")=='<%=TradePortalConstants.CHARGE_BEN_FOR_OUTSIDE%>'){
		registry.byId('TradePortalConstants.CHARGE_BEN_FOR_OUTSIDE').set('checked', true);
	}else{
		registry.byId('TradePortalConstants.CHARGE_OTHER').set('checked', true);
	}
	dom.byId("DomesticPaymentAmount").innerHTML=entities.encode(grid.store.getValue(benJsObj, "Amount_List"));
	var valueDate = dom.byId("ValueDate");
	if(valueDate){
		valueDate.innerHTML = entities.encode(grid.store.getValue(benJsObj, "ValueDate_List"));
	}
	dom.byId("PayerDescription").innerHTML=entities.encode(grid.store.getValue(benJsObj, "CUSTOMER_REFERENCE"));
	dom.byId("PayeeMessage").innerHTML=entities.encode(grid.store.getValue(benJsObj, "PAYEE_DESCRIPTION"));
	
	if (dom.byId("EnteredAccount")) dom.byId("EnteredAccount").innerHTML=entities.encode(grid.store.getValue(benJsObj, "AccountNumber_List"));
	if (dom.byId("PayeeName")) dom.byId("PayeeName").innerHTML=entities.encode(grid.store.getValue(benJsObj, "PayeeName_List"));
	if (dom.byId("PayeeAddrL1")) dom.byId("PayeeAddrL1").innerHTML=entities.encode(grid.store.getValue(benJsObj, "PAYEE_ADDRESS_LINE_1"));
	if (dom.byId("PayeeAddrL2")) dom.byId("PayeeAddrL2").innerHTML=entities.encode(grid.store.getValue(benJsObj, "PAYEE_ADDRESS_LINE_2"));
	if (dom.byId("PayeeAddrL3")) dom.byId("PayeeAddrL3").innerHTML=entities.encode(grid.store.getValue(benJsObj, "PAYEE_ADDRESS_LINE_3"));
	if (dom.byId("PayeeAddrL4")) dom.byId("PayeeAddrL4").innerHTML=entities.encode(grid.store.getValue(benJsObj, "PAYEE_ADDRESS_LINE_4"));
	if (dom.byId("Country")) dom.byId("Country").innerHTML=entities.encode(getVal(data,"Country"));
	if (dom.byId("payeeFaxNumber")) dom.byId("payeeFaxNumber").innerHTML=entities.encode(grid.store.getValue(benJsObj, "PAYEE_FAX_NUMBER"));
	if (dom.byId("payeeEmail")) dom.byId("payeeEmail").innerHTML=entities.encode(grid.store.getValue(benJsObj, "PAYEE_EMAIL"));
	if (dom.byId("payeeInstructionNumber")) dom.byId("payeeInstructionNumber").innerHTML=entities.encode(grid.store.getValue(benJsObj, "PAYEE_INSTRUCTION_NUMBER"));
	
	if (dom.byId("PayeeBankCode")) dom.byId("PayeeBankCode").innerHTML=entities.encode(grid.store.getValue(benJsObj, "PayeeBankCode_List"));

	var benBank_name = entities.encode(grid.store.getValue(benJsObj, "PayeeBankName_List"));
	var benBank_adl1 = entities.encode(grid.store.getValue(benJsObj, "ADDRESS_LINE_1"));
	var benBank_adl2 = entities.encode(grid.store.getValue(benJsObj, "ADDRESS_LINE_2"));
	var benBank_adl3 = entities.encode(grid.store.getValue(benJsObj, "ADDRESS_LINE_3"));
	var benBank_country = entities.encode(grid.store.getValue(benJsObj, "PAYEE_BANK_COUNTRY"));
	var benBank_branch_name = entities.encode(grid.store.getValue(benJsObj, "PAYEE_BRANCH_NAME"));
	
        <%-- cquinton 2/8/2013 format address using common module --%>
	var benBankData = txtFmt.formatBankAddress(benBank_name, benBank_branch_name, 
          benBank_adl1, benBank_adl2, benBank_adl3, benBank_country);
	
	<%-- cquinton 2/8/2013 set PayeeBankData with the build blob, plus all the hidden fields for next save --%>
	<%-- 03/11/2014 R 8.4 IR T36000025638 - .innsrHtml is not worknig for some browers. --%>
	<%-- dom.byId("PayeeBankData").innerHTML=benBankData; --%>
	if(dom.byId("PayeeBankData")) dom.byId("PayeeBankData").value = benBankData;
	if(dom.byId("PayeeBankName")) dom.byId("PayeeBankName").value = benBank_name;
	if(dom.byId("PayeeBankAddressLine1"))  dom.byId("PayeeBankAddressLine1").value = benBank_adl1;
	if(dom.byId("PayeeBankAddressLine2"))  dom.byId("PayeeBankAddressLine2").value = benBank_adl2;
	if(dom.byId("PayeeBankAddressLine3"))  dom.byId("PayeeBankAddressLine3").value = benBank_adl3;
	if(dom.byId("PayeeBankCountry"))  dom.byId("PayeeBankCountry").value = benBank_country;
	if(dom.byId("PayeeBankBranchName"))  dom.byId("PayeeBankBranchName").value = benBank_branch_name;
              
	if(dom.byId("central_bank_reporting_1")) dom.byId("central_bank_reporting_1").innerHTML=entities.encode(grid.store.getValue(benJsObj, "CENTRAL_BANK_REPORTING_1"));
	if(dom.byId("central_bank_reporting_2")) dom.byId("central_bank_reporting_2").innerHTML=entities.encode(grid.store.getValue(benJsObj, "CENTRAL_BANK_REPORTING_2"));
	if(dom.byId("central_bank_reporting_3")) dom.byId("central_bank_reporting_3").innerHTML=entities.encode(grid.store.getValue(benJsObj, "CENTRAL_BANK_REPORTING_3"));
	
	if(dom.byId("reporting_code_1")) dom.byId("reporting_code_1").innerHTML=entities.encode(getVal(data,"REPORTING_CODE_1"));
	if(dom.byId("reporting_code_2")) dom.byId("reporting_code_2").innerHTML=entities.encode(getVal(data,"REPORTING_CODE_2"));
	
	if (dom.byId("DeliveryMethod")) dom.byId("DeliveryMethod").innerHTML=entities.encode(getVal(data,"DeliveryMethod"));
	
	if (dom.byId("PayableLocation")) dom.byId("PayableLocation").innerHTML=entities.encode(grid.store.getValue(benJsObj, "PAYABLE_LOCATION"));
	if (dom.byId("PrintLocation")) dom.byId("PrintLocation").innerHTML=entities.encode(grid.store.getValue(benJsObj, "PRINT_LOCATION"));
	if (dom.byId("mailing_address_line_1")) dom.byId("mailing_address_line_1").innerHTML=entities.encode(grid.store.getValue(benJsObj, "MAILING_ADDRESS_LINE_1"));
	if (dom.byId("mailing_address_line_2")) dom.byId("mailing_address_line_2").innerHTML=entities.encode(grid.store.getValue(benJsObj, "MAILING_ADDRESS_LINE_2"));
	if (dom.byId("mailing_address_line_3")) dom.byId("mailing_address_line_3").innerHTML=entities.encode(grid.store.getValue(benJsObj, "MAILING_ADDRESS_LINE_3"));
	if (dom.byId("mailing_address_line_4")) dom.byId("mailing_address_line_4").innerHTML=entities.encode(grid.store.getValue(benJsObj, "MAILING_ADDRESS_LINE_4"));
	
	var input = entities.encode(getVal(data,"payeeInvoiceDetails"));   
 	input = input.replace(/&#40;/g,"(");
 	input = input.replace(/&#41;/g,")");
 	input=  input.replace(/&#60;/g,"<");
 	input=  input.replace(/&#62;/g,">");
 	input=  input.replace(/&#38;/g,"&");
	input=  input.replace(/&#39;/g,"'");
   	input=  input.replace(/&#47;/g,"/");
	input=  input.replace(/&amp;#47;/g,"/");
 	if (dom.byId("payeeInvoiceDetails")) dom.byId("payeeInvoiceDetails").value=input;
	
 	if (dom.byId("ErrorText")) dom.byId("ErrorText").value=entities.encode(grid.store.getValue(benJsObj, "ERROR_TEXT"));
 	if (dom.byId("PaymentStatus")) dom.byId("PaymentStatus").innerHTML=entities.encode(grid.store.getValue(benJsObj, "PaymentStatus_List"));
 	if (dom.byId("PaymentSystemReference")) dom.byId("PaymentSystemReference").innerHTML=entities.encode(grid.store.getValue(benJsObj, "PAYMENT_SYSTEM_REF"));
 	if (dom.byId("BeneficiarySequenceId")) dom.byId("BeneficiarySequenceId").innerHTML=seq_num;

	<%-- IR T36000020215 Rel 8.3 START --%>
	if(registry.byId("DomesticPaymentOID")) registry.byId("DomesticPaymentOID").set("value",grid.store.getValue(benJsObj, "rowKey"));
	<%-- IR T36000020215 Rel 8.3 END --%>
	if (dom.byId("FirstIntBankBranchCode")) dom.byId("FirstIntBankBranchCode").innerHTML=entities.encode(getVal(data,"FirstIntBankBranchCode"));
	if (dom.byId("FirstIntCode")) dom.byId("FirstIntCode").innerHTML=entities.encode(getVal(data,"FirstIntBankBranchCode"));
	<%--  KMehta IR-T36000031030 Rel 9300 on 07-July-2015 Change - Begin --%>
	if(dom.byId("FirstIntBankData")) dom.byId("FirstIntBankData").value=getVal(data,"FirstIntBankData");	
   	if(dom.byId("FirstIntBankName")) dom.byId("FirstIntBankName").value=getVal(data,"FirstIntBankName");
   	if(dom.byId("FirstIntBankBranchName")) dom.byId("FirstIntBankBranchName").value=getVal(data,"FirstIntBankBranchName");
   	if(dom.byId("FirstIntBankAddressLine1")) dom.byId("FirstIntBankAddressLine1").value=getVal(data,"FirstIntBankAddressLine1");
   	if(dom.byId("FirstIntBankAddressLine2")) dom.byId("FirstIntBankAddressLine2").value=getVal(data,"FirstIntBankAddressLine2");
   	if(dom.byId("FirstIntBankAddressLine3")) dom.byId("FirstIntBankAddressLine3").value=getVal(data,"FirstIntBankAddressLine3");
   	if(dom.byId("FirstIntBankCountry")) dom.byId("FirstIntBankCountry").value=getVal(data,"FirstIntBankCountry");
   	<%--  KMehta IR-T36000031030 Rel 9300 on 07-July-2015 Change - End --%>
	
<% } else { %>
	
    if(registry.byId("PaymentMethodType")) registry.byId("PaymentMethodType").set("value",grid.store.getValue(benJsObj, "PaymentMethod_List"));
		
	if(grid.store.getValue(benJsObj, "BANK_CHARGES_TYPE")=='<%=TradePortalConstants.CHARGE_OUR_ACCT%>'){
		registry.byId('TradePortalConstants.CHARGE_OUR_ACCT').set('checked', true);
	}else if(grid.store.getValue(benJsObj, "BANK_CHARGES_TYPE")=='<%=TradePortalConstants.CHARGE_BEN_FOR_OUTSIDE%>'){
		registry.byId('TradePortalConstants.CHARGE_BEN_FOR_OUTSIDE').set('checked', true);
	}else{
		registry.byId('TradePortalConstants.CHARGE_OTHER').set('checked', true);
	}
	
	if(registry.byId("DomesticPaymentAmount")) registry.byId("DomesticPaymentAmount").set("value",grid.store.getValue(benJsObj, "Amount_List"));
	if(dom.byId("ValueDate")) dom.byId("ValueDate").innerHTML = grid.store.getValue(benJsObj, "ValueDate_List");
	if(registry.byId("PayerDescription")) registry.byId("PayerDescription").set("value",grid.store.getValue(benJsObj, "CUSTOMER_REFERENCE"));
	if(registry.byId("PayeeMessage")) registry.byId("PayeeMessage").set("value",grid.store.getValue(benJsObj, "PAYEE_DESCRIPTION"));	
	if(registry.byId("PayeeName")) registry.byId("PayeeName").set("value",grid.store.getValue(benJsObj, "PayeeName_List"));
	if(registry.byId("PayeeAddrL1")) registry.byId("PayeeAddrL1").set("value",grid.store.getValue(benJsObj, "PAYEE_ADDRESS_LINE_1"));
	if(registry.byId("PayeeAddrL2")) registry.byId("PayeeAddrL2").set("value",grid.store.getValue(benJsObj, "PAYEE_ADDRESS_LINE_2"));
	if(registry.byId("PayeeAddrL3")) registry.byId("PayeeAddrL3").set("value",grid.store.getValue(benJsObj, "PAYEE_ADDRESS_LINE_3"));
	if(registry.byId("PayeeAddrL4")) registry.byId("PayeeAddrL4").set("value",grid.store.getValue(benJsObj, "PAYEE_ADDRESS_LINE_4"));
	if(registry.byId("Country")) registry.byId("Country").set("value",grid.store.getValue(benJsObj, "COUNTRY"));
	if(registry.byId("payeeFaxNumber")) registry.byId("payeeFaxNumber").set("value",grid.store.getValue(benJsObj, "PAYEE_FAX_NUMBER"));
	
	<%--  Sandeep IR-T36000010513 01/23/2013 Start --%>
	if( registry.byId("PaymentMethodType") && (registry.byId("PaymentMethodType").value!="<%=TradePortalConstants.PAYMENT_METHOD_BCHK%>") 
			&& (registry.byId("PaymentMethodType").value!="<%=TradePortalConstants.PAYMENT_METHOD_CCHK%>") ){
		if(document.getElementById("payeeEmailId")){
		  document.getElementById("payeeEmailId").style.display = "block";	
		}
		if(registry.byId("payeeEmail") != undefined){
		 registry.byId("payeeEmail").set("value",grid.store.getValue(benJsObj, "PAYEE_EMAIL"));
		}
	}else{
	   if(registry.byId("payeeEmail") != undefined){
		registry.byId("payeeEmail").set("value",grid.store.getValue(benJsObj, "PAYEE_EMAIL"));
	   }
	}
	<%--  Sandeep IR-T36000010513 01/23/2013 End --%>
	
	if (registry.byId("payeeInstructionNumber")) registry.byId("payeeInstructionNumber").set("value",grid.store.getValue(benJsObj, "PAYEE_INSTRUCTION_NUMBER"));
	if (registry.byId("PayeeBankCode")) registry.byId("PayeeBankCode").set("value",grid.store.getValue(benJsObj, "PayeeBankCode_List"));

	var benBank_name = grid.store.getValue(benJsObj, "PayeeBankName_List");
	var benBank_adl1 = grid.store.getValue(benJsObj, "ADDRESS_LINE_1");
	var benBank_adl2 = grid.store.getValue(benJsObj, "ADDRESS_LINE_2");
	var benBank_adl3 = grid.store.getValue(benJsObj, "ADDRESS_LINE_3");
	var benBank_country = grid.store.getValue(benJsObj, "PAYEE_BANK_COUNTRY");
	var benBank_branch_name = grid.store.getValue(benJsObj, "PAYEE_BRANCH_NAME");

        <%-- cquinton 2/8/2013 format address using common module --%>
	var benBankData = txtFmt.formatBankAddress(benBank_name, benBank_branch_name, 
          benBank_adl1, benBank_adl2, benBank_adl3, benBank_country);
	
	<%-- cquinton 2/8/2013 set PayeeBankData with the build blob --%>
	<%-- 03/11/2014 R 8.4 IR T36000025638 - .innsrHtml is not worknig for some browers. --%>
	<%-- dom.byId("PayeeBankData").innerHTML=benBankData; --%>
	if(dom.byId("PayeeBankData")) dom.byId("PayeeBankData").value = benBankData;
	if(dom.byId("PayeeBankName")) dom.byId("PayeeBankName").value = benBank_name;
	if(dom.byId("PayeeBankAddressLine1")) dom.byId("PayeeBankAddressLine1").value = benBank_adl1;
	if(dom.byId("PayeeBankAddressLine2")) dom.byId("PayeeBankAddressLine2").value = benBank_adl2;
	if(dom.byId("PayeeBankAddressLine3")) dom.byId("PayeeBankAddressLine3").value = benBank_adl3;
	if(dom.byId("PayeeBankCountry")) dom.byId("PayeeBankCountry").value = benBank_country;
	if(dom.byId("PayeeBankBranchName")) dom.byId("PayeeBankBranchName").value = benBank_branch_name;
	
    if (registry.byId("central_bank_reporting_1")) registry.byId("central_bank_reporting_1").set("value",grid.store.getValue(benJsObj, "CENTRAL_BANK_REPORTING_1"));
    if (registry.byId("central_bank_reporting_2")) registry.byId("central_bank_reporting_2").set("value",grid.store.getValue(benJsObj, "CENTRAL_BANK_REPORTING_2"));
    if (registry.byId("central_bank_reporting_3")) registry.byId("central_bank_reporting_3").set("value",grid.store.getValue(benJsObj, "CENTRAL_BANK_REPORTING_3"));
	
	
    if (registry.byId("reporting_code_1")) registry.byId("reporting_code_1").set("value",grid.store.getValue(benJsObj, "REPORTING_CODE_1"));
    if (registry.byId("reporting_code_2")) registry.byId("reporting_code_2").set("value",grid.store.getValue(benJsObj, "REPORTING_CODE_2"));
	
    if (registry.byId("DeliveryMethod")) registry.byId("DeliveryMethod").set("value",grid.store.getValue(benJsObj, "DELIVERY_METHOD"));
    if (registry.byId("PayableLocation")) registry.byId("PayableLocation").set("value",grid.store.getValue(benJsObj, "PAYABLE_LOCATION"));
    if (registry.byId("PrintLocation")) registry.byId("PrintLocation").set("value",grid.store.getValue(benJsObj, "PRINT_LOCATION"));
    if (registry.byId("mailing_address_line_1")) registry.byId("mailing_address_line_1").set("value",grid.store.getValue(benJsObj, "MAILING_ADDRESS_LINE_1"));
    if (registry.byId("mailing_address_line_2")) registry.byId("mailing_address_line_2").set("value",grid.store.getValue(benJsObj, "MAILING_ADDRESS_LINE_2"));
    if (registry.byId("mailing_address_line_3")) registry.byId("mailing_address_line_3").set("value",grid.store.getValue(benJsObj, "MAILING_ADDRESS_LINE_3"));
    if (registry.byId("mailing_address_line_4")) registry.byId("mailing_address_line_4").set("value",grid.store.getValue(benJsObj, "MAILING_ADDRESS_LINE_4"));
 	 
 	var input = getVal(data,"payeeInvoiceDetails");
   
     	input = input.replace(/&#40;/g,"(");
     	input = input.replace(/&#41;/g,")");
     	input=  input.replace(/&#60;/g,"<");
     	input=  input.replace(/&#62;/g,">");
     	input=  input.replace(/&#38;/g,"&");
     	<%--  KMehta IR-T36000043824 Rel 9500 on 27-Nov-2015 Add - Begin  --%>
     	input=  input.replace(/&#39;/g,"'");
     	input=  input.replace(/&#47;/g,"/");
     	<%--  KMehta IR-T36000043824 Rel 9500 on 27-Nov-2015 Add - End  --%>
    if (registry.byId("payeeInvoiceDetails")) registry.byId("payeeInvoiceDetails").set("value",input);
	
	if (registry.byId("ErrorText")) registry.byId("ErrorText").set("value",grid.store.getValue(benJsObj, "ERROR_TEXT"));
	if(dom.byId("PaymentStatus")) dom.byId("PaymentStatus").innerHTML=grid.store.getValue(benJsObj, "PaymentStatus_List");
	if(dom.byId("PaymentSystemReference")) dom.byId("PaymentSystemReference").innerHTML=grid.store.getValue(benJsObj, "PAYMENT_SYSTEM_REF");
	if(dom.byId("BeneficiarySequenceId")) dom.byId("BeneficiarySequenceId").innerHTML=seq_num;
	if(dom.byId("InvoiceDetailOID")) dom.byId("InvoiceDetailOID").value=getVal(data,"InvoiceDetailOID");
	if(dom.byId("beneficiaryPartyOid")) dom.byId("beneficiaryPartyOid").value = grid.store.getValue(benJsObj, "bene_party_id");
	if(dom.byId("PayeeAccount")) dom.byId("PayeeAccount").value = grid.store.getValue(benJsObj, "AccountNumber_List");
	
	if(registry.byId("DomesticPaymentOID")) registry.byId("DomesticPaymentOID").set("value",grid.store.getValue(benJsObj, "rowKey"));
	
	if(dom.byId("update_ben_button_div")) dom.byId("update_ben_button_div").style.display = "block";
	if(dom.byId("save_ben_button_div")) dom.byId("save_ben_button_div").style.display = "none";
	if(registry.byId("addBeneficaryButton")) registry.byId("addBeneficaryButton").setAttribute('disabled', false);
   	if(dom.byId("FirstIntBankBranchCode")) dom.byId("FirstIntBankBranchCode").value=getVal(data,"FirstIntBankBranchCode");
   	if(dom.byId("FirstIntCode")) dom.byId("FirstIntCode").value=getVal(data,"FirstIntBankBranchCode");
   	if(dom.byId("FirstIntBankData")) dom.byId("FirstIntBankData").value=getVal(data,"FirstIntBankData");	
   	if(dom.byId("FirstIntBankName")) dom.byId("FirstIntBankName").value=getVal(data,"FirstIntBankName");
   	if(dom.byId("FirstIntBankBranchName")) dom.byId("FirstIntBankBranchName").value=getVal(data,"FirstIntBankBranchName");
   	if(dom.byId("FirstIntBankAddressLine1")) dom.byId("FirstIntBankAddressLine1").value=getVal(data,"FirstIntBankAddressLine1");
   	if(dom.byId("FirstIntBankAddressLine2")) dom.byId("FirstIntBankAddressLine2").value=getVal(data,"FirstIntBankAddressLine2");
   	if(dom.byId("FirstIntBankAddressLine3")) dom.byId("FirstIntBankAddressLine3").value=getVal(data,"FirstIntBankAddressLine3");
   	if(dom.byId("FirstIntBankCountry")) dom.byId("FirstIntBankCountry").value=getVal(data,"FirstIntBankCountry");
	<% } %>	
	
	<%-- jgadela 04/02/2014 R8.4 IR T36000019732 Rework- refactored the below block code  --%>
	<%-- IR T36000019732 Fix Rpasupulati Start --%>
	<%if(isFixedPayment){%>

       checkFixedFieldValue(grid, benJsObj, "PayeeName_List", "t_payee_name", "PayeeName");   
       checkFixedFieldValue(grid, benJsObj, "PAYEE_ADDRESS_LINE_1", "t_payee_address_line_1", "PayeeAddrL1");
       checkFixedFieldValue(grid, benJsObj, "PAYEE_ADDRESS_LINE_2", "t_payee_address_line_2", "PayeeAddrL2");
       checkFixedFieldValue(grid, benJsObj, "PAYEE_ADDRESS_LINE_3", "t_payee_address_line_3", "PayeeAddrL3");
       checkFixedFieldValue(grid, benJsObj, "PAYEE_ADDRESS_LINE_4", "t_payee_address_line_4", "PayeeAddrL4");
       checkFixedFieldValue(grid, benJsObj, "COUNTRY", "t_country", "Country");
       checkFixedFieldValue(grid, benJsObj, "PAYEE_FAX_NUMBER", "t_payee_fax_number", "payeeFaxNumber");
       checkFixedFieldValue(grid, benJsObj, "PAYEE_EMAIL", "t_payee_email", "payeeEmail");
       checkFixedFieldValue(grid, benJsObj, "PAYEE_INSTRUCTION_NUMBER", "t_payee_instruction_number", "payeeInstructionNumber");
       <%-- MEer IR-36226 Bank Data readOnly and Search icon issue  --%>
       <%--checkFixedFieldValue(grid, benJsObj, "PAYEE_BRANCH_CODE", "t_payee_branch_code", "PayeeBankCode");--%>
       checkFixedFieldValue(grid, benJsObj, "PayeeBankCode_List", "t_payee_bank_code", "PayeeBankCode");
       checkFixedFieldValue(grid, benJsObj, "C_FIRST_INTERMEDIARY_BANK", "t_c_first_intermediary_bank", "FirstIntBankBranchCode");
       checkFixedFieldValue(grid, benJsObj, "DELIVERY_METHOD", "t_delivery_method", "DeliveryMethod");
       checkFixedFieldValue(grid, benJsObj, "CENTRAL_BANK_REPORTING_1", "t_central_bank_reporting_1", "central_bank_reporting_1");
       checkFixedFieldValue(grid, benJsObj, "CENTRAL_BANK_REPORTING_2", "t_central_bank_reporting_2", "central_bank_reporting_2");
       checkFixedFieldValue(grid, benJsObj, "CENTRAL_BANK_REPORTING_3", "t_central_bank_reporting_3", "central_bank_reporting_3");
       checkFixedFieldValue(grid, benJsObj, "REPORTING_CODE_1", "t_reporting_code_1", "reporting_code_1");
       checkFixedFieldValue(grid, benJsObj, "REPORTING_CODE_2", "t_reporting_code_2", "reporting_code_2");
       
       checkFixedFieldValue(grid, benJsObj, "PAYABLE_LOCATION", "t_payable_location", "PayableLocation");
       checkFixedFieldValue(grid, benJsObj, "PRINT_LOCATION", "t_print_location", "PrintLocation");
       checkFixedFieldValue(grid, benJsObj, "MAILING_ADDRESS_LINE_1", "t_mailing_address_line_1", "mailing_address_line_1");
       checkFixedFieldValue(grid, benJsObj, "MAILING_ADDRESS_LINE_2", "t_mailing_address_line_2", "mailing_address_line_2");
       checkFixedFieldValue(grid, benJsObj, "MAILING_ADDRESS_LINE_3", "t_mailing_address_line_3", "mailing_address_line_3");
       checkFixedFieldValue(grid, benJsObj, "MAILING_ADDRESS_LINE_4", "t_mailing_address_line_4", "mailing_address_line_4");
       checkFixedFieldValue(grid, benJsObj, "PaymentMethod_List", "t_payment_method_type", "PaymentMethodType");
       checkFixedFieldValue(grid, benJsObj, "CUSTOMER_REFERENCE", "t_customer_reference", "PayerDescription");
	   
	   if(grid.store.getValue(benJsObj, "t_payee_invoice_details")!="" && 
			   grid.store.getValue(benJsObj, "allow_inv_detail_modification")=='<%=TradePortalConstants.INDICATOR_NO%>'){
		   if(registry.byId("payeeInvoiceDetails")) registry.byId("payeeInvoiceDetails").set('readOnly',true);  
	   }
	   <%-- 04/03/2014 R 8.4 - IR 19732 - Amount issue --%>
	    if(grid.store.getValue(benJsObj, "Amount_List")!="" && grid.store.getValue(benJsObj, "t_amount")!="" && 
	    		dom.byId("DomesticPaymentAmount").value!="0.00" && 
	    		grid.store.getValue(benJsObj, "allow_pay_amt_modification")=='<%=TradePortalConstants.INDICATOR_NO%>' ){
		  if(registry.byId("DomesticPaymentAmount")) registry.byId("DomesticPaymentAmount").set('readOnly',true);
	    }
	   		   
	   if( grid.store.getValue(benJsObj, "PAYEE_DESCRIPTION")!="" && grid.store.getValue(benJsObj, "t_payee_description")!="" && 
			   grid.store.getValue(benJsObj, "allow_pay_detail_modification")=='<%=TradePortalConstants.INDICATOR_NO%>'){
		   if(registry.byId("PayeeMessage")) registry.byId("PayeeMessage").set('readOnly',true);
	   }
	
	   if(grid.store.getValue(benJsObj, "BANK_CHARGES_TYPE")!="" && grid.store.getValue(benJsObj, "t_bank_charges_type")!=""){
		   registry.byId("TradePortalConstants.CHARGE_OUR_ACCT").set('readOnly',true);
		   registry.byId("TradePortalConstants.CHARGE_BEN_FOR_OUTSIDE").set('readOnly',true);
		   registry.byId("TradePortalConstants.CHARGE_OTHER").set('readOnly',true);
	   }
	   
	    <%-- 03/18/2014 - T36000019732  - disable Ben Search buttons in the case of fixed template --%>
	    if(dom.byId("payben")) dom.byId("payben").style.display = "none";
	    if(dom.byId("ClearBenSearchButton")) dom.byId("ClearBenSearchButton").style.display = "none";
	    if(dom.byId("ClearInterButton")) dom.byId("ClearInterButton").style.display = "none";
	    if(dom.byId("payments_firstintermediarybank")) dom.byId("payments_firstintermediarybank").style.display = "none";
	   	
	   	if(grid.store.getValue(benJsObj, "PayeeBankCode_List")!="" && grid.store.getValue(benJsObj, "t_payee_bank_code")!=""){	
	   		if(dom.byId("payments_banksearch")) dom.byId("payments_banksearch").style.display = "none";
	   		if(dom.byId("clearBenBankSearchButton")) dom.byId("clearBenBankSearchButton").style.display = "none";
	   	}
	   	
		<%}%>
		<%-- IR 19732 end --%>
		<%-- Nar CR 966 Rel 9.2 09/17/2014 Add - Begin --%>
		<%if(isFixedTemplate){%>
		  setAllowModificationCheck();
		  if(grid.store.getValue(benJsObj, "allow_pay_detail_modification")=='<%=TradePortalConstants.INDICATOR_YES%>'){
			  if(registry.byId("AllowPayDetailModification")) registry.byId('AllowPayDetailModification').set('checked', true);
		  }
		  if(grid.store.getValue(benJsObj, "allow_pay_amt_modification")=='<%=TradePortalConstants.INDICATOR_YES%>'){
			  if(registry.byId("AllowPayAmtModification")) registry.byId('AllowPayAmtModification').set('checked', true);
		  }
		  if(grid.store.getValue(benJsObj, "allow_inv_detail_modification")=='<%=TradePortalConstants.INDICATOR_YES%>'){
			  if(registry.byId("AllowInvDetailModification")) registry.byId('AllowInvDetailModification').set('checked', true);
		  }		
		<%}%>
		<%-- Nar CR 966 Rel 9.2 09/17/2014 Add - End --%>
  });
}

function checkFixedFieldValue(grid, benJsObj, currrentField, sourceField, Id){
	require(["dojo/dom", "dijit/registry", "dojo/domReady!"],
		      function( dom, registry) {
		var fieldId = registry.byId(Id);
		if(fieldId){
			if(grid.store.getValue(benJsObj, currrentField)!="" && 
					 grid.store.getValue(benJsObj, sourceField)!=""){
				fieldId.set('readOnly',true);
			}else{
				fieldId.set('readOnly',false);
			}
		}		
	});
}

function getVal(data, code) {
   var val = '';
  if (data && data[code] ) {
     val = data[code];
   }
   return val;
}

<%-- Nar CR 966 Rel 9.2 09/17/2014 Add - Start --%>
function setAllowModificationCheck(){
	checkAllowModificationVal("PayeeMessage", "AllowPayDetailModification");
	checkAllowModificationVal("DomesticPaymentAmount", "AllowPayAmtModification");
	checkAllowModificationVal("payeeInvoiceDetails", "AllowInvDetailModification");
}

function checkAllowModificationVal(allowModificationTextID, allowCheckBoxID){
	  require(["dijit/registry", "dojo/domReady!"],
 function(registry) {
	   var allowText  = registry.byId(allowModificationTextID);
	   var allowcheck = registry.byId(allowCheckBoxID);
			if(allowText && allowcheck){
				if( allowText.get('value') ==  ""){
					allowcheck.set('readOnly',true);
				}else{
					allowcheck.set('readOnly',false);
				}
			}   
	 });
}

require(["dojo/on", "dijit/registry", "dojo/ready"],
	      function( on, registry, ready ) {
  ready(function() {  
		var domesticPaymentAmount = registry.byId("DomesticPaymentAmount");
		if (domesticPaymentAmount) {
			domesticPaymentAmount.on("change", function() {
		    var allowPayAmtModification = registry.byId("AllowPayAmtModification");
		     if (allowPayAmtModification) {
				     if ( this.value == '' ) {
				    	 allowPayAmtModification.set('checked', false);
				    	 allowPayAmtModification.set("readOnly", true);
		             } else {
		            	 allowPayAmtModification.set("readOnly", false); 
				     }
		     }
		     });	
		   }
	   
	   var payeeMessage = registry.byId("PayeeMessage");
	   if (payeeMessage) {
	  	 payeeMessage.on("change", function() {
	     var allowPayDetailModification = registry.byId("AllowPayDetailModification");
	     if (allowPayDetailModification) {
			     if ( this.value == '' ) {
			    	 allowPayDetailModification.set('checked', false);
			    	 allowPayDetailModification.set("readOnly", true);
	             } else {
	            	 allowPayDetailModification.set("readOnly", false); 
			     }
	     }
	     });	
	   }
	   
	   var payeeInvoiceDetails = registry.byId("payeeInvoiceDetails");
	   if (payeeInvoiceDetails) {
		 payeeInvoiceDetails.on("change", function() {
	     var allowInvDetailModification = registry.byId("AllowInvDetailModification");
	     if (allowInvDetailModification) {
			     if ( this.value == '' ) {
			    	 allowInvDetailModification.set('checked', false);
			    	 allowInvDetailModification.set("readOnly", true);
	             } else {
	            	 allowInvDetailModification.set("readOnly", false); 
			     }
	     }
	     });	
	   }   
    });
});
<%-- Nar CR 966 Rel 9.2 09/17/2014 Add - End --%>

</script>
