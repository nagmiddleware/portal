<%--
*******************************************************************************
               Export Letter of Credit Issue Transfer Page

  Description:   
  This is the main driver for the Export Letter of Credit Issue Transfer page.  
  It handles data retrieval of terms and terms parties (or retrieval from the 
  input document) and creates the html page to display all the data for an 
  Export Letter of Credit Issue Transfer.
*******************************************************************************
--%>

<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,java.util.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.html.*,java.math.*,
                 com.amsinc.ecsg.util.DateTimeUtility" %>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager"
    scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" 
   scope="session">
</jsp:useBean>


<%-- ************** Data retrieval page setup begins here ****************  --%>

<%
WidgetFactory widgetFactory = new WidgetFactory(resMgr, userSession);
  String  onLoad     = "";
  String  focusField = "TransfereeName";   // Default focus field
  boolean focusSet   = false;

  // Various oid and status info from transaction and instruments used in
  // several places.
  String instrumentOid = "";         
  String transactionOid;
  String instrumentType;
  String instrumentStatus;
  String transactionType; 
  String transactionStatus;
  String rejectionIndicator        = "";
  String rejectionReasonText       = "";
  String buttonPressed;
  String links					   = "";
  
  boolean getDataFromDoc;          // Indicates if data is retrieved from the
                                   // input doc cache or from the database
  boolean phraseSelected = false;  // Indicates if a phrase was selected.

  DocumentHandler doc;

  String loginLocale = userSession.getUserLocale();
  String loginRights = userSession.getSecurityRights();

  // Dates are stored in the database as one field but displayed on the
  // page as 3.  These variables hold the pieces for each date.
  String expiryDay           = "";
  String expiryMonth         = "";
  String expiryYear          = "";
  String latestShipmentDay   = "";
  String latestShipmentMonth = "";
  String latestShipmentYear  = "";

  // Variable for the Related Instrument Id field in case we're coming back from the Instrument Search page
  String instrumentId = null;

  // Variables used for populating ref data dropdowns.
  String options;
  String defaultText;
  ReferenceDataManager refData1 = ReferenceDataManager.getRefDataMgr();
  String datePattern = userSession.getDatePattern();
  String datePatternDisplay = refData1.getDescr("DATEPATTERN", datePattern, loginLocale);
  String dateWidgetOptions = "placeHolder:'" + datePatternDisplay + "'";

  // These are the beans used on the page.

  TransactionWebBean transaction  = (TransactionWebBean)
                                      beanMgr.getBean("Transaction");
  InstrumentWebBean instrument    = (InstrumentWebBean)
                                      beanMgr.getBean("Instrument");
  TemplateWebBean template        = (TemplateWebBean)
                                      beanMgr.getBean("Template");
  TermsWebBean terms              = null;

  TermsPartyWebBean termsPartyTransferee = beanMgr.createBean(TermsPartyWebBean.class, "TermsParty");
  TermsPartyWebBean termsPartyTransfereeBank = beanMgr.createBean(TermsPartyWebBean.class, "TermsParty");

  ShipmentTermsWebBean shipmentTerms = beanMgr.createBean(ShipmentTermsWebBean.class, "ShipmentTerms");

  // Get the document from the cache.  We'll may use it to repopulate the 
  // screen if returning from another page, a save, validation, or any other
  // mediator called from this page.  Otherwise, we assume an instrument oid
  // and transaction oid was passed in.

  doc = formMgr.getFromDocCache();
  buttonPressed = doc.getAttribute("/In/Update/ButtonPressed");
  Vector error = null;
  error = doc.getFragments("/Error/errorlist/error");
  String PartySearchAddressTitle =resMgr.getTextEscapedJS("PartySearch.TabHeading",TradePortalConstants.TEXT_BUNDLE);

/******************************************************************************
  We are either entering the page or returning from somewhere.  These are the 
  conditions for how to populate the web beans.  Data comes from either the
  database or the doc cache (/In section) with some variation.

  Mode           Condition                                 Populate Beans From
  -------------  ---------------------------------------   --------------------------------
  Enter Page     no /In/Transaction in doc                 Instrument and Template web
                                                           beans already populated, get
                                                           data for Terms and TermsParty
                                                           web beans from database

  return from    /In/NewPartyFromSearchInfo/PartyOid              doc cache (/In); also use Party
  Party Search     exists                                  SearchInfo to lookup, and 
                                                           populate a specific TermsParty
                                                           web bean

  return from    /In/InstrumentSearchInfo/InstrumentData   doc cache (/In)
  Instrument       exists
  Search

  return from    /Out/PhraseLookupInfo exists              doc cache (/In); but use Phrase
  Phrase Lookup                                            LookupInfo text (from /Out) to
                                                           replace a specific phrase text
                                                           in the /In document before 
                                                           populating

  return from    /Error/maxerrorseverity < 1               Same as Enter Page (data is
  Transaction                                              retrieved from database)
    mediator
    (no error)

  return from    /Error/maxerrorseverity > 0               doc cache (/In)
  Transaction    
    mediator
    (error)
******************************************************************************/

  // Assume we get the data from the doc.
  getDataFromDoc = true;

  String maxError = doc.getAttribute("/Error/maxerrorseverity");
  if (maxError != null && maxError.compareTo(String.valueOf(ErrorManager.WARNING_ERROR_SEVERITY)) <= 0)
  {
     // No errors, so don't get the data from doc.
     getDataFromDoc = false;
  }
  //ir cnuk113043991 - check to see if transaction needs to be refreshed
  // if so, refresh it and do not get data from doc as it is wrong
  if ( "Y".equals(doc.getAttribute("/Out/Refresh/Transaction")) ) {
     transaction.getDataFromAppServer();
     getDataFromDoc = false;
  }
  if (doc.getDocumentNode("/In/NewPartyFromSearchInfo/Type") != null) {
     // We have returned from the party search page.  Get data from doc cache
     getDataFromDoc = true;
  }
  if (doc.getDocumentNode("/In/Transaction") == null)
  {
     // No /In/Transaction means we've never looked up the data.
     Debug.debug("No /In/Transaction section - get data from database");
     getDataFromDoc = false;
  }
  if (doc.getDocumentNode("/Out/PhraseLookupInfo") != null)
  {
     // A Looked up phrase exists.  Replace it in the /In document.
     Debug.debug("Found a looked-up phrase");
     getDataFromDoc = true;
     phraseSelected = true;

     String result = doc.getAttribute("/Out/PhraseLookupInfo/Result");
     if (result.equals(TradePortalConstants.INDICATOR_YES))
     {
        // Take the looked up and appended phrase text from the /Out section
        // and copy it to the /In section.  This allows the beans to be 
        // properly populated.
        String xmlPath = doc.getAttribute("/In/PhraseLookupInfo/text_path");
        xmlPath = "/In" + xmlPath;

        doc.setAttribute(xmlPath, doc.getAttribute("/Out/PhraseLookupInfo/NewText"));

        // If we returned from the phrase lookup without errors, set the focus
        // to the correct field.  Otherwise, don't set the focus so the user can
        // see the error.
        if (maxError != null && maxError.compareTo(String.valueOf(ErrorManager.WARNING_ERROR_SEVERITY)) < 0)
        {
           focusField = doc.getAttribute("/In/PhraseLookupInfo/text_field_name");
           focusSet = true;
        }
     }
     else
     {
        // We do nothing because nothing was looked up.  We stil want to get
        // the data from the doc.
     }
  }

  if (getDataFromDoc)
  {
     Debug.debug("Populating beans from doc cache");

     // Populate the beans from the input doc.
     try
     {
        instrument.populateFromXmlDoc(doc.getComponent("/In"));
        transaction.populateFromXmlDoc(doc.getComponent("/In"));
        template.populateFromXmlDoc(doc.getComponent("/In"));

        terms = (TermsWebBean) beanMgr.getBean("Terms");
        terms.populateFromXmlDoc(doc, "/In");

        String termsPartyOid;

        termsPartyOid = terms.getAttribute("c_FirstTermsParty");
        if (termsPartyOid != null && !termsPartyOid.equals(""))
        {
           termsPartyTransferee.setAttribute("terms_party_oid", termsPartyOid);
           termsPartyTransferee.getDataFromAppServer();
        }
        termsPartyOid = terms.getAttribute("c_SecondTermsParty");
        if (termsPartyOid != null && !termsPartyOid.equals(""))
        {
           termsPartyTransfereeBank.setAttribute("terms_party_oid", termsPartyOid);
           termsPartyTransfereeBank.getDataFromAppServer();
        }

        DocumentHandler termsPartyDoc;

        termsPartyDoc = doc.getFragment("/In/Terms/FirstTermsParty");
        termsPartyTransferee.loadTermsPartyFromDocTagsOnly(termsPartyDoc);

        termsPartyDoc = doc.getFragment("/In/Terms/SecondTermsParty");
        termsPartyTransfereeBank.loadTermsPartyFromDocTagsOnly(termsPartyDoc);

        // Some of the data for this transaction is stored as part of the shipment terms
        terms.populateFirstShipmentFromXml(doc);

        expiryDay   = doc.getAttribute("/In/Terms/expiry_day");
        expiryMonth = doc.getAttribute("/In/Terms/expiry_month");
        expiryYear  = doc.getAttribute("/In/Terms/expiry_year");

        latestShipmentDay   = 
             doc.getAttribute("/In/Terms/ShipmentTermsList/latest_shipment_day");
        latestShipmentMonth = 
             doc.getAttribute("/In/Terms/ShipmentTermsList/latest_shipment_month");
        latestShipmentYear  = 
             doc.getAttribute("/In/Terms/ShipmentTermsList/latest_shipment_year");
     }
     catch (Exception e)
     {
        out.println("Contact Administrator: "
              + "Unable to reload data after returning to page. "
              + "Error is " + e.toString());
     }
  }
  else
  {
     Debug.debug("populating beans from database");
     // We will perform a retrieval from the database.
     // Instrument and Transaction were already retrieved.  Get
     // the rest of the data

     terms = transaction.registerCustomerEnteredTerms();

     String termsPartyOid;

     termsPartyOid = terms.getAttribute("c_FirstTermsParty");
     if (termsPartyOid != null && !termsPartyOid.equals(""))
     {
        termsPartyTransferee.setAttribute("terms_party_oid", termsPartyOid);
        termsPartyTransferee.getDataFromAppServer();
     }
     termsPartyOid = terms.getAttribute("c_SecondTermsParty");
     if (termsPartyOid != null && !termsPartyOid.equals(""))
     {
        termsPartyTransfereeBank.setAttribute("terms_party_oid", termsPartyOid);
        termsPartyTransfereeBank.getDataFromAppServer();
     }

     // Some of the data for this transaction is stored as part of the shipment terms
     terms.populateFirstShipmentFromDatabase();

     // Because date fields are represented as three fields on the page,
     // we need to parse out the date values into three fields.  Pre-mediator
     // logic puts these three fields back together for the terms bean.
     String date = terms.getAttribute("expiry_date");
     expiryDay   = TPDateTimeUtility.parseDayFromDate(date);
     expiryMonth = TPDateTimeUtility.parseMonthFromDate(date);
     expiryYear  = TPDateTimeUtility.parseYearFromDate(date);

     date                = terms.getFirstShipment().getAttribute("latest_shipment_date");
     latestShipmentDay   = TPDateTimeUtility.parseDayFromDate(date);
     latestShipmentMonth = TPDateTimeUtility.parseMonthFromDate(date);
     latestShipmentYear  = TPDateTimeUtility.parseYearFromDate(date);
  }

  /*************************************************
  * Load New Party
  **************************************************/
  if (doc.getDocumentNode("/In/NewPartyFromSearchInfo/Type") != null &&
      doc.getDocumentNode("/In/NewPartyFromSearchOutput/PartyOid") != null ) {

     // We have returned from the PartyDetailNew page.  Based on the returned 
     // data, RELOAD one of the terms party beans with the new party

     String termsPartyType = doc.getAttribute("/In/NewPartyFromSearchInfo/Type");
     String partyOid = doc.getAttribute("/In/NewPartyFromSearchOutput/PartyOid");

     Debug.debug("Returning from party search with " + termsPartyType + "/" + partyOid);

     // Use a Party web bean to get the party data for the selected oid.
     PartyWebBean party = beanMgr.createBean(PartyWebBean.class, "Party");
     party.setAttribute("party_oid", partyOid);
     party.getDataFromAppServer();

     DocumentHandler partyDoc = new DocumentHandler();
     party.populateXmlDoc(partyDoc);

     partyDoc = partyDoc.getFragment("/Party");

     // Based on the party type being returned (which we previously set)
     // reload one of the terms party web beans with the data from the
     // doc.
     if (termsPartyType.equals(TradePortalConstants.TRANSFEREE)) {
        termsPartyTransferee.loadTermsPartyFromDoc(partyDoc);
        // For a transferee selection, the transferee's bank (transferee_negot) 
        // should be populated if there is a designated bank.
        //cquinton 2/7/2013 pass designated party as part of partyDoc
        loadDesignatedParty(partyDoc, termsPartyTransfereeBank, beanMgr);
        focusField = "TransfereeName";
        focusSet = true;
     }
     if (termsPartyType.equals(TradePortalConstants.TRANSFEREE_NEGOT)) {
        termsPartyTransfereeBank.loadTermsPartyFromDoc(partyDoc);
        focusField = "";
        focusSet = true;
        onLoad += "location.hash='#" + termsPartyType + "';";
     }
  }

  if (doc.getDocumentNode("/In/InstrumentSearchInfo/InstrumentData") != null)
  {
     // We have returned from the instrument search page.  Based on the returned 
     // data, retrieve the complete instrument ID of the selected instrument to 
     // populate the Related Instrument ID in the General section
     String instrumentData = doc.getAttribute("/In/InstrumentSearchInfo/InstrumentData");

     instrumentId = InstrumentServices.parseForInstrumentId(instrumentData);
  }

  transactionType = transaction.getAttribute("transaction_type_code");
  transactionStatus = transaction.getAttribute("transaction_status");

  // Get the transaction rejection indicator to determine whether to show the rejection reason text
  rejectionIndicator = transaction.getAttribute("rejection_indicator");
  rejectionReasonText = transaction.getAttribute("rejection_reason_text");
  
  instrumentType = instrument.getAttribute("instrument_type_code");
  instrumentStatus = instrument.getAttribute("instrument_status");

  Debug.debug("Instrument Type " + instrumentType);
  Debug.debug("Instrument Status " + instrumentStatus);
  Debug.debug("Transaction Type " + transactionType);
  Debug.debug("Transaction Status " + transactionStatus);

  BigInteger requestedSecurityRight = null;

  requestedSecurityRight = SecurityAccess.EXPORT_LC_CREATE_MODIFY;
%>
  <%@ include file="fragments/Transaction-PageMode.frag" %>


<%
  // Create documents for the phrase dropdowns. First check the cache (no need
  // to recreate if they already exist).  After creating, place in the cache.
  // (InstrumentCloseNavigator.jsp cleans these up.)

  DocumentHandler phraseLists = formMgr.getFromDocCache(TradePortalConstants.PHRASE_LIST_DOC);
  if (phraseLists == null) phraseLists = new DocumentHandler();

  DocumentHandler goodsDocList = phraseLists.getFragment("/Goods");
  if (goodsDocList == null)
  {
     goodsDocList = PhraseUtility.createPhraseList(
                                    TradePortalConstants.PHRASE_CAT_GOODS, 
                                    userSession, formMgr, resMgr);
     phraseLists.addComponent("/Goods", goodsDocList);
  }

  DocumentHandler addlCondDocList = phraseLists.getFragment("/AddlCond");
  if (addlCondDocList == null)
  {
     addlCondDocList = PhraseUtility.createPhraseList(
                                    TradePortalConstants.PHRASE_CAT_ADDL_COND, 
                                    userSession, formMgr, resMgr);
     phraseLists.addComponent("/AddlCond", addlCondDocList);
  }

  DocumentHandler spclInstrDocList = phraseLists.getFragment("/SpclInstr");
  if (spclInstrDocList == null)
  {
     spclInstrDocList = PhraseUtility.createPhraseList(
                                    TradePortalConstants.PHRASE_CAT_SPCL_INST, 
                                    userSession, formMgr, resMgr);
     phraseLists.addComponent("/SpclInstr", spclInstrDocList);
  }

  formMgr.storeInDocCache(TradePortalConstants.PHRASE_LIST_DOC, phraseLists);
%>


<%-- ********************* HTML for page begins here ********************* --%>

<%
  if (isTemplate) {
%>
    <jsp:include page="/common/ButtonPrep.jsp" />
<%
  }
%>

<%-- Body tag included as part of common header --%>
<%
  // The navigation bar is only shown when editing templates.  For transactions
  // it is not shown ti minimize the chance of leaving the page without properly
  // unlocking the transaction.
  String showNavBar = TradePortalConstants.INDICATOR_NO;
  if (isTemplate)
  {
    showNavBar = TradePortalConstants.INDICATOR_YES;
  }
%>

  
<%
  // If we're in multi part mode, set the focusField based on the current part.
  // The focus was defaulted to the first field on the first page.  Check if the
  // focus still has that value.  If not it's because we've made a phrase 
  // selection and reset focus.  Do not reset it again.
  //if (multiPartMode && !focusSet) {
  //  if (whichPart.equals(TradePortalConstants.PART_ONE)) {
  //    focusField = "TransfereeName";
  //   }
  //  if (whichPart.equals(TradePortalConstants.PART_TWO)) {
  //     focusField = "GoodsDescText";
  //  }
   //}

  // Auto save the form when time-out if not readonly.  
  // (Header.jsp will check for auto-save setting of the corporate org).
  String autoSaveFlag = isReadOnly ? TradePortalConstants.INDICATOR_NO : TradePortalConstants.INDICATOR_YES;
  String templateFlag = isTemplate ? TradePortalConstants.INDICATOR_YES : TradePortalConstants.INDICATOR_NO;

  //cquinton 11/19/2012 pass info for recent instruments into header
  String newRecentTransactionOid = transaction.getAttribute("transaction_oid");
  String newRecentInstrumentId = instrument.getAttribute("complete_instrument_id");
  String newRecentTransactionType = transaction.getAttribute("transaction_type_code");
  String newRecentTransactionStatus = transaction.getAttribute("transaction_status");
%>

<jsp:include page="/common/Header.jsp">
  <jsp:param name="autoSaveFlag"           value="<%=autoSaveFlag%>" />
  <jsp:param name="templateFlag"           value="<%=templateFlag%>" />
  <jsp:param name="newRecentTransactionOid" value="<%=newRecentTransactionOid%>" />
  <jsp:param name="newRecentInstrumentId"  value="<%=newRecentInstrumentId%>" />
  <jsp:param name="newRecentTransactionType" value="<%=newRecentTransactionType%>" />
  <jsp:param name="newRecentTransactionStatus" value="<%=newRecentTransactionStatus%>" />
</jsp:include>

<% //cr498 begin
   //Include ReAuthentication frag in case re-authentication is required for
   //authorization of transactions for this client
   String certAuthURL = "";
   Cache reCertCache = (Cache)TPCacheManager.getInstance().getCache(TradePortalConstants.CLIENT_BANK_CACHE);
   DocumentHandler CBCResult = (DocumentHandler)reCertCache.get(userSession.getClientBankOid());	
   String requireTranAuth = CBCResult.getAttribute("/ResultSetRecord(0)/REQUIRE_TRAN_AUTH");
   boolean requireAuth = InstrumentAuthentication.requireTransactionAuthentication(
       requireTranAuth,InstrumentAuthentication.TRAN_AUTH__EXP_DLC_TRN);
   if (requireAuth) {
%>   
      <%@ include file="/logon/fragments/openReauthenticationWindow.frag" %>
<%
   }
   //cr498 end
%>
<%
  if (isTemplate) {
%>
    <jsp:include page="/common/ButtonPrep.jsp" />
<%
  }
%>
<div class="pageMain">
<div class="pageContent">
<%
  
    
  String helpUrl = "customer/issue_transfer_exp_lc.htm";
%>
<%
		  if(isTemplate) {
		  String pageTitle;
		  String titleName;
		  String returnAction;
		  ReferenceDataManager refData = ReferenceDataManager.getRefDataMgr();		
		  pageTitle = "Template";
		  titleName = template.getAttribute("name");
		  StringBuffer title = new StringBuffer();
		  String itemKey = "";
		  title.append(pageTitle);
		  title.append( " : " );
		  if(instrument.getAttribute("instrument_type_code").equals("SLC")){
		  if(transaction.getAttribute("standby_using_guarantee_form").equals(TradePortalConstants.INDICATOR_NO)){     
				  itemKey = "Outgoing Standby LC: Simple"; 
		  }else{
				  itemKey = "Outgoing Standby LC: Detailed";
		  }
		  title.append(itemKey);
		  }
		  else
		  title.append( refData.getDescr(TradePortalConstants.INSTRUMENT_TYPE, 
		                 instrument.getAttribute("instrument_type_code"), 
		                 loginLocale) );
		  title.append( " - " );
		  title.append(titleName );
		  returnAction = "goToInstrumentCloseNavigator";
		
		  String transactionSubHeader = title.toString();
  		
  		%>
		<jsp:include page="/common/PageHeader.jsp">
				<jsp:param name="titleKey" value="<%= pageTitle%>"/>
				<jsp:param name="helpUrl"  value="<%=helpUrl%>" />
  		</jsp:include>
  		
  		<jsp:include page="/common/PageSubHeader.jsp">
 				 <jsp:param name="titleKey" value="<%= transactionSubHeader%>" />
  				 <jsp:param name="returnUrl" value="<%= formMgr.getLinkAsUrl(returnAction, response) %>" />
		</jsp:include> 		
		 <%}else{ %>
<jsp:include page="/common/PageHeader.jsp">
   <jsp:param name="titleKey" value="SecondaryNavigation.ExportLCTransfer" />
   <jsp:param name="helpUrl"  value="<%=helpUrl%>" />
</jsp:include>

<jsp:include page="/common/TransactionSubHeader.jsp" />

	<%} %>	
<form name="Transaction-EXP_DLC-Form" id="Transaction-EXP_DLC-Form" method="post" data-dojo-type="dijit.form.Form" action="<%=formMgr.getSubmitAction(response)%>">

  <input type=hidden value="" name="buttonName">
<div class="formArea">
<jsp:include page="/common/ErrorSection.jsp" />

<div class="formContent">
<% //cr498 begin
  if (requireAuth) {
%> 

  <input type=hidden name="reCertification" value="<%=TradePortalConstants.INDICATOR_NO%>">
  <input type=hidden name="reCertOK">
  <input type=hidden name="logonResponse">
  <input type=hidden name="logonCertificate">

<%
  } //cr498 end
%> 

<%
  // Store values such as the userid, security rights, and org in a
  // secure hashtable for the form.  Also store instrument and transaction
  // data that must be secured.
  Hashtable secureParms = new Hashtable();

  secureParms.put("login_oid",             userSession.getUserOid());
  secureParms.put("owner_org_oid",         userSession.getOwnerOrgOid());
  secureParms.put("login_rights",          loginRights);

  secureParms.put("instrument_oid",        instrument.getAttribute("instrument_oid"));
  secureParms.put("instrument_type_code",  instrumentType);
  secureParms.put("instrument_status",     instrumentStatus);

  secureParms.put("transaction_oid",       transaction.getAttribute("transaction_oid"));
  secureParms.put("transaction_type_code", transactionType);
  secureParms.put("transaction_status",    transactionStatus);

  secureParms.put("ShipmentOid", terms.getFirstShipment().getAttribute("shipment_oid"));

  secureParms.put("transaction_instrument_info", 
  	transaction.getAttribute("transaction_oid") + "/" + 
	instrument.getAttribute("instrument_oid") + "/" + 
	transactionType);

  // If the terms record doesn't exist, set its oid to 0.
  String termsOid = terms.getAttribute("terms_oid");
  if (termsOid == null)
  {
     termsOid = "0";
  }

  secureParms.put("terms_oid", termsOid);

  if (isTemplate) {
    secureParms.put("template_oid", template.getAttribute("template_oid"));
    secureParms.put("opt_lock", template.getAttribute("opt_lock"));
 
%>
    <%@ include file="fragments/TemplateHeader.frag" %>

<%
    }


  String extraPartTags = request.getParameter("extraPartTags");
%>

 
<%
  if (rejectionIndicator.equals(TradePortalConstants.INDICATOR_YES)
  	|| rejectionIndicator.equals(TradePortalConstants.INDICATOR_X))
  {
%>
<%=widgetFactory.createSectionHeader("0", "Rejection Reason") %>
     <%@ include file="fragments/Transaction-RejectionReason.frag" %>
     </div>
<%
  }
%>
<% // [BEGIN] IR-YVUH032343792 - jkok %>
  <%@ include file="fragments/Transaction-Documents.frag" %>
<% // [END] IR-YVUH032343792 - jkok %>

<script>
		<%-- dojo.require("dijit/TitlePane") --%>
</script>
<% //CR 821 Added Repair Reason Section 
StringBuffer repairReasonWhereClause = new StringBuffer();
int  repairReasonCount = 0;
	
	/*Get all repair reason's count from transaction history table*/
	repairReasonWhereClause.append("p_transaction_oid = ?");
	repairReasonWhereClause.append(" and REPAIR_REASON is not null order by ACTION_DATETIME");
	//jgadela  R90 IR T36000026319 - SQL FIX
	Object sqlParamsRepCnt2[] = new Object[1];
	sqlParamsRepCnt2[0] =  transaction.getAttribute("transaction_oid") ;

	Debug.debug("repairReasonWhereCondition query : " + repairReasonWhereClause);
	repairReasonCount = DatabaseQueryBean.getCount("A_USER_OID", "transaction_history", repairReasonWhereClause.toString(), true, sqlParamsRepCnt2);
			
if ( !(TradePortalConstants.TRANS_STATUS_PROCESSED_BY_BANK.equalsIgnoreCase(transactionStatus)) && repairReasonCount>0  ){%>
	<%=widgetFactory.createSectionHeader("", "TransactionHistory.RepairReason") %>
		<%@ include file="fragments/Transaction-RepairReason.frag" %>
	</div>
<%} %> 

<%= widgetFactory.createSectionHeader("1", "TransferELC.Terms") %>

    <%@ include file="fragments/Transaction-EXP_DLC-TRN-General.frag" %>
</div>
<%= widgetFactory.createSectionHeader("2", "TransferELC.OtherConditionsOfTransfer") %>
    <%@ include file="fragments/Transaction-EXP_DLC-TRN-OtherConditions.frag" %>
</div>
<%= widgetFactory.createSectionHeader("3", "TransferELC.InstructionsToBank") %>
    <%@ include file="fragments/Transaction-EXP_DLC-TRN-BankInstructions.frag" %>
</div>

<% if (TradePortalConstants.TRANS_STATUS_PROCESSED_BY_BANK.equalsIgnoreCase(transactionStatus) && repairReasonCount>0  ){%>
		<%=widgetFactory.createSectionHeader("4", "TransactionHistory.RepairReason", null, true) %>
		<%@ include file="fragments/Transaction-RepairReason.frag" %>
		</div>
<%} %> 


</div><%--formContent--%>
</div><%--formArea--%>
  
<div class="formSidebar" data-dojo-type="widget.FormSidebar"
                                    data-dojo-props="form: 'Transaction-EXP_DLC-Form'">
          <jsp:include page="/common/Sidebar.jsp">
            <jsp:param name="links" value="<%=links%>" />
        	<jsp:param name="isReadOnly" value="<%=isReadOnly%>" />
			<jsp:param name="certAuthURL" value="<%=certAuthURL%>" />
			<jsp:param name="buttonPressed" value="<%=buttonPressed%>" />
            <jsp:param name="error" value="<%= error%>" />
            <jsp:param name="formName" value="0" />
            <jsp:param name="isTemplate" value="<%=isTemplate%>"/>
            <jsp:param name="showLinks" value="true"/>
            </jsp:include>
   </div>
  

  <%@ include file="fragments/PhraseLookupPrep.frag" %>
  <%@ include file="fragments/PartySearchPrep.frag" %>

  

  <%= formMgr.getFormInstanceAsInputField("Transaction-EXP_DLC-Form", secureParms) %>
<input type="hidden" name="selection" />
<input type="hidden" name="ChoosePrimary" />
<div id="PartySearchDialog"></div>
</form>

<%  //include a hidden form for new instrument submission
			//this is used for all of the new instrument types available from the menu
			Hashtable newInstrSecParms = new Hashtable();
			newInstrSecParms.put("UserOid", userSession.getUserOid());
			newInstrSecParms.put("SecurityRights", userSession.getSecurityRights());
			newInstrSecParms.put("clientBankOid", userSession.getClientBankOid());
			newInstrSecParms.put("ownerOrg", userSession.getOwnerOrgOid());
			%>
			<form method="post" name="NewInstrumentForm1" action="<%=formMgr.getSubmitAction(response)%>">
			<input type="hidden" name="bankBranch" />
			<input type="hidden" name="transactionType"/>
			<input type="hidden" name="instrumentType" />
			<input type="hidden" name="copyInstrumentOid" />
			<input type="hidden" name="mode" value="CREATE_NEW_INSTRUMENT" />
			<input type="hidden" name="copyType" value="Instr" />
			
			<%= formMgr.getFormInstanceAsInputField("NewInstrumentForm",newInstrSecParms) %>
			</form>
			<%
			Hashtable newTempSecParms = new Hashtable();
			newTempSecParms.put("userOid", userSession.getUserOid());
			newTempSecParms.put("securityRights", userSession.getSecurityRights());
			newTempSecParms.put("clientBankOid", userSession.getClientBankOid());
			newTempSecParms.put("ownerOrg", userSession.getOwnerOrgOid());
			newTempSecParms.put("ownerLevel", userSession.getOwnershipLevel());
			%>
			<form method="post" name="NewTemplateForm1" action="<%=formMgr.getSubmitAction(response)%>">
			<input type="hidden" name="name" />
			<input type="hidden" name="bankBranch" />
			<input type="hidden" name="transactionType"/>
			<input type="hidden" name="InstrumentType" />
			<input type="hidden" name="copyInstrumentOid" />
			<input type="hidden" name="mode"  />
			<input type="hidden" name="CopyType" />
			<input type="hidden" name="expressFlag"  />
			<input type="hidden" name="fixedFlag" />
			<input type="hidden" name="PaymentTemplGrp" />
			<input type="hidden" name="validationState" />
			
			<%= formMgr.getFormInstanceAsInputField("NewTemplateForm",newTempSecParms) %>
			</form>
</div>
</div>
<jsp:include page="/common/Footer.jsp">
   <jsp:param name="autoSaveFlag"           value="<%=autoSaveFlag%>" />
   <jsp:param name="templateFlag"           value="<%=templateFlag%>" />
   <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>
<jsp:include page="/common/SidebarFooter.jsp"/>

<script>

  <%-- cquinton 3/3/2013 add local var --%>
  var local = {};

<%
  if (!isReadOnly && InstrumentServices.isNotBlank(focusField)) {
%>

  require(["dijit/registry", "dojo/ready"],
    function(registry, ready ) {
      ready(function() {
        var focusFieldId = '<%= focusField %>';
        if ( focusFieldId ) {
          var focusField = registry.byId(focusFieldId);
          focusField.focus();
        }
      });
  });
<%
  }
%>
</script>

</body>
</html>

<%
   // Finally, reset the cached document to eliminate carryover of
   // information to the next visit of this page.
   doc.removeAllChildren("/");

   formMgr.storeInDocCache("default.doc", doc);
%>

<%!
   // Various methods are declared here (in alphabetical order).



   public String getRequiredIndicator(boolean showIndicator)
   {
      if (showIndicator)
      {
         return "<span class=Asterix>*</span>";
      }
      else
      {
         return "";
      }
   }
%> 

<%@include file="fragments/DesignatedPartyLookup.frag" %>
<script type="text/javascript">
  var itemid;
  var section;

  function SearchParty(identifierStr, sectionName,partyType){
	
    itemid = String(identifierStr);
    section = String(sectionName);
    partyType = String(partyType);

    require(["dojo/dom", "t360/dialog"], function(dom, dialog ) {

      <%-- cquinton 2/7/2013 --%>
      <%-- set the SearchPartyType input so it is included on new party form submit --%>
      var searchPartyTypeInput = dom.byId("SearchPartyType");
      if ( searchPartyTypeInput ) {
        searchPartyTypeInput.value=partyType;
      }

      dialog.open('PartySearchDialog', '<%=PartySearchAddressTitle%>',
        'PartySearch.jsp',
        ['returnAction','filterText','partyType','unicodeIndicator','itemid','section'],
        ['selectTransactionParty','',partyType,'<%=TradePortalConstants.INDICATOR_NO%>',itemid,section]); <%-- parameters --%>
    });
  }

  <%-- cquinton 3/2/2013 local phrase lookup --%>
  require(["t360/phraseLookup"],
          function( phraseLookup ) {
    local.phraseLookupAppend = function(phraseSelect, textArea, textAreaMaxLength ) {
      phraseLookup.appendPhrase( phraseSelect, textArea, textAreaMaxLength, "_errorSection" );
    };
  });

</script>
