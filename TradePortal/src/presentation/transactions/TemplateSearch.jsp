<%--
*******************************************************************************
                                    Template Search

  Description:
    This page contains two forms.  The first is the upper portion of the page
  and supports the filtering of templates by instrument type.  The second is
  the selection of a template and continuation to the next step in the template
  creation process.

*******************************************************************************
--%>

<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.html.*,java.util.*" %>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager"
    scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean"
   scope="session">
</jsp:useBean>


<%-- ************** Data retrieval page setup begins here ****************  --%>
<%//read in parameters
	String templateSearchDialogId = StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(request.getParameter("dialogId")));
	//think this is what is necessary to limit initially
	//String[] bankBranchSelectorDialogBankBranchArray = request.getParameter("bankBranchArray");
	WidgetFactory widgetFactory = new WidgetFactory(resMgr);
	
	  String       gridLayout      = "";
	  DataGridFactory dgFactory = new DataGridFactory(resMgr, userSession, formMgr, response);
	//DataGridFactory dgFactory = new DataGridFactory(resMgr);
	String gridHtml = "";
	Debug.debug("***START****************TEMPLATE SEARCH***************START***");
	boolean newTemplate = true;     // indicates what page we came from (i.e.,
	                                // doing new template or new transaction)
	StringBuffer dynamicWhere  = new StringBuffer();;
	String link = "";
	String temp = ""; 
	int dbCount			  = 0; 
	String OwnerOrgOid 	  = null; 
	String parentOrgID    = userSession.getOwnerOrgOid();
	String bogID          = userSession.getBogOid();
	String clientBankID   = userSession.getClientBankOid();
	String globalID       = userSession.getGlobalOrgOid();
	String ownershipLevel = userSession.getOwnershipLevel();
	String pUserOid 	  = null;
	String loginLocale = userSession.getUserLocale();
	String loginRights = userSession.getSecurityRights();
	String standbys_use_guar_form_only = null;
	String standbys_use_either_form = null;
	String sqlTemp = null;
	// Parameters that drive this page.
	String cancelAction;
	String prevStepAction;
	String nextStepForm;
	String copyType;
	String titleKey;
	String bankBranch;
	String isFixed = null;	
	
	DocumentHandler doc = formMgr.getFromDocCache();
	// First read in the parameters that drive how the page operates.
   cancelAction =  request.getParameter("CancelAction");
  
  if (cancelAction == null)
     Debug.debug("***Warning: CancelAction parameter not specified");

  prevStepAction = request.getParameter("PrevStepAction");

  nextStepForm = request.getParameter("NextStepForm");
  if (nextStepForm == null)
     Debug.debug("***Warning: NextStepForm parameter not specified");

  copyType =  StringFunction.xssCharsToHtml(request.getParameter("CopyType"));//MEer REl 9.3 CID-11440
  if (copyType == null) copyType = "";

  if (copyType.equals(TradePortalConstants.FROM_TEMPL)) newTemplate = true;
  else newTemplate = false;

  bankBranch =request.getParameter("BankBranch");  //MEer REl 9.3 CID-11409

  titleKey = request.getParameter("TitleKey");

  /************************
   * ERSKINE'S CODE START *
   ************************/

  //If we are in the transaction area the parameters will be passed in via this document
  //If we are in the transaction area the parameters will be passed in via this document
  HttpSession theSession = request.getSession(false);
  String inTransactionArea = (String) theSession.getAttribute("inNewTransactionArea");

  if (inTransactionArea != null && inTransactionArea.equals(TradePortalConstants.INDICATOR_YES))
  {
    Debug.debug("Getting data from doc cache ->\n" + doc.toString());

    String mode    = doc.getAttribute("/In/mode"); 
    cancelAction   = TradePortalConstants.TRANSACTION_CANCEL_ACTION;
    titleKey       = "InstSearch.HeadingNewTransStep2";
    copyType       = doc.getAttribute("/In/copyType");
    bankBranch     = doc.getAttribute("/In/bankBranch");
    isFixed        = doc.getAttribute("/In/isFixed"); 	 	//IAZ CR-586 08/16/10 Add
        

    if (mode != null) {
      // The next step and prev step button actions change based on the 
      // mode we're currently doing.  They indicate what the previous page
      // is and the form for the next step (which indicate a mediator).
     // Debug.debug("the mode is " + mode);

       if (mode.equals(TradePortalConstants.EXISTING_INSTR)) {
         nextStepForm = "NewTransInstrSearchForm2";
         prevStepAction = "newTransactionStep1";
      } else if (mode.equals(TradePortalConstants.NEW_TEMPLATE)) {
         nextStepForm = "NewTransTemplateSearchForm";
         prevStepAction = "newTemplate";
         titleKey = "TemplateSearch.HeadingNewTemplateStep2";
         cancelAction = "goToRefDataHome";
      } else {
         nextStepForm = "NewTransInstrSearchForm";
         prevStepAction = "newTransactionStep1";
      } 
    }
  }

  OwnerOrgOid = userSession.getOwnerOrgOid();
  pUserOid = userSession.getUserOid();
  String checkTemplateGroup = " p_user_oid = ? ";
//jgadela  R90 IR T36000026319 - SQL FIX
  Object sqlParams2[] = new Object[1];
  sqlParams2[0] = userSession.getUserOid();
  dbCount = DatabaseQueryBean.getCount("AUTHORIZED_TEMPLATE_GROUP_OID", "USER_AUTHORIZED_TEMPLATE_GROUP", checkTemplateGroup, false, sqlParams2);
 
  // Now read in the search parameters to redisplay to the user.
/* TODO KOMAL templateName pass param to Datview java*/
  String templateName = request.getParameter("TemplateName");
  if (templateName != null) {
    templateName = templateName.trim().toUpperCase();
    if (!templateName.equals("")) {
    }
  } else {
    templateName = "";
  }   
  /* TODO ends */
  boolean OrgDataUnderSubAccess = userSession.showOrgDataUnderSubAccess();
  Long organization_oid = new Long( userSession.getOwnerOrgOid() );
  Vector instrumentTypes = Dropdown.getActualEditableInstrumentTypes(formMgr, organization_oid.longValue(), loginRights, userSession.getSecurityType(), ownershipLevel);
  
//TODO Commented by KOMAL
   /* if (doc.getAttribute("/In/mode").equals(TradePortalConstants.NEW_INSTR)) {
     //Vshah - 10/12/2010 - IR#VIUK101152531 - [BEGIN]
     //Commented below line & add new ones beneath it.
     //dynamicWhere.append(" and i.instrument_type_code not in ('FTDP','FTBA')");		
    
     if (instrumentTypes.contains(InstrumentType.DOM_PMT)) {
     	instrumentTypes.removeElement(InstrumentType.DOM_PMT);
     }
     if (instrumentTypes.contains(InstrumentType.XFER_BET_ACCTS)) {
     	instrumentTypes.removeElement(InstrumentType.XFER_BET_ACCTS);
     }
  	 
     sqlTemp = StringFunction.toSQLString(instrumentTypes);
     
  }
    
  if (doc.getAttribute("/In/mode").equals(TradePortalConstants.NEW_TEMPLATE)) {
  	 if(TradePortalConstants.INDICATOR_ON.equalsIgnoreCase(isFixed))
  	 	sqlTemp = null;
  }   */
//TODO Commented by KOMAL End
	  UserWebBean loggedUser = beanMgr.createBean(UserWebBean.class, "User");
	  loggedUser.setAttribute("user_oid", userSession.getUserOid());
	  loggedUser.getDataFromAppServer();
	  String confInd = "";
	  if (TradePortalConstants.ADMIN.equals(userSession.getSecurityType()))
	      confInd = TradePortalConstants.INDICATOR_YES;
	  else if (userSession.getSavedUserSession() == null)
	      confInd = loggedUser.getAttribute("confidential_indicator");
	  else   
	  {
	      if (TradePortalConstants.ADMIN.equals(userSession.getSavedUserSession().getSecurityType()))
	         confInd = TradePortalConstants.INDICATOR_YES;
	      else	
	         confInd = loggedUser.getAttribute("subsid_confidential_indicator");  
	  }
//IAZ CR-586 08/16/10 and 08/18/10 End
//Suresh Penke Rel7100 IR-WZUK102660022 09/23/2011 Begin
CorporateOrganizationWebBean corporateOrg = beanMgr.createBean(CorporateOrganizationWebBean.class,"CorporateOrganization");
corporateOrg.setAttribute("organization_oid", userSession.getOwnerOrgOid());
corporateOrg.getDataFromAppServer();
standbys_use_guar_form_only = corporateOrg.getAttribute("standbys_use_guar_form_only");
standbys_use_either_form = corporateOrg.getAttribute("standbys_use_either_form");

 String prevParms = "&CopyType=" + copyType
                 + "&BankBranch=" + bankBranch; 
%>

<%-- ********************* HTML for page begins here *********************  --%>

<script LANGUAGE="JavaScript">

  function checkSelection() {
    <%--
    // Before going to the next step, the user must have selected one of the
    // radio buttons.  If not, give an error.  Since the selection radio button
    // is dynamic, it is possible the field doesn't even exist.  First check
    // for the existence of the field and then check if a selection was made.
    --%>

    numElements = document.TemplateSearch.elements.length;
    i = 0;
    foundField = false;

    <%-- First determine if the field even exists. --%>
    for (i=0; i < numElements; i++) {
       if (document.TemplateSearch.elements[i].name == "selection") {
          foundField = true;
       }
    }

    if (foundField) {
        var size=document.TemplateSearch.selection.length;
        <%-- Handle the case where there are two or more radio buttons --%>
        for (i=0; i<size; i++)
        {
            if (document.TemplateSearch.selection[i].checked)
            {
                return true;
            }
        }
        <%-- Handle the case where there is only one radio button --%>
        if (document.TemplateSearch.selection.checked)
        {
            return true;
        }
    }
    <%-- Handle the case where no button are checked --%>
    var msg = "<%=resMgr.getText("InstSearch.SelectOne",
                                  TradePortalConstants.TEXT_BUNDLE)%>"
    alert(msg);
    return false;

  }

</script>



<%-- Body tag included as part of common header --%>
<jsp:include page="/common/Header.jsp">
   <jsp:param name="includeNavigationBarFlag"
              value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeErrorSectionFlag"
              value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>

<%
  // Store values such as the userid, his security rights, and his org in a
  // secure hashtable for the form.  The mediator needs this information.
  Hashtable secureParms = new Hashtable();
  secureParms.put("login_oid", userSession.getUserOid());
  secureParms.put("owner_org_oid", userSession.getOwnerOrgOid());
  secureParms.put("login_rights", loginRights);

%>

<%-- ********************** First form begins here ***********************  --%>
<%-- Supports processing of Filter button.                                  --%>

<form name="TemplateFilter" method="post"
      action="<%=formMgr.getSubmitAction(response)%>">

  <%=formMgr.getFormInstanceAsInputField("TemplateFilterForm", secureParms) %>
<div>  
<%-- MEer Rel 9.3 CID 11435, 11409 --%>
  <input type="hidden" name="CancelAction" value="<%=StringFunction.xssCharsToHtml(cancelAction)%>">
  <input type="hidden" name="PrevStepAction" value="<%=StringFunction.xssCharsToHtml(prevStepAction)%>">
  <input type="hidden" name="NextStepForm" value="<%=StringFunction.xssCharsToHtml(nextStepForm)%>">
  <input type="hidden" name="CopyType" value="<%=copyType%>">
  <input type="hidden" name="TitleKey" value="<%=StringFunction.xssCharsToHtml(titleKey)%>">
  <input type="hidden" name="BankBranch" value="<%=StringFunction.xssCharsToHtml(bankBranch)%>">
<%= widgetFactory.createSearchTextField("TN","TemplateSearch.TemplateSearch","30")%>
 


<%  
  gridHtml = dgFactory.createDataGrid("instruSearchGrid","InstrumentTemplateSearchDataGrid",null);
  gridLayout = dgFactory.createGridLayout("InstrumentTemplateSearchDataGrid");
%>
<%= gridHtml %>

</div>

</form>
</body>
</html>
<jsp:include page="/common/Footer.jsp">
   <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>

<script type='text/javascript'>
  var gridLayout = <%= gridLayout %>;
  <%--  <div id="templateSearchGrid"></div>  --%>
   <%--create a local memory grid--%> 
   <%-- <% createMemoryDataGrid("instruSearchGrid", gridData, gridLayout); %> --%>
    var initSearchParms = "standbys_use_guar_form_only=<%=standbys_use_guar_form_only%>&standbys_use_either_form=<%=standbys_use_either_form%>&OrgDataUnderSubAccess=<%=OrgDataUnderSubAccess%>&dbcount=<%=dbCount%>&confInd=<%=confInd%>"; 	
  <%--create the grid, attaching it to the div id specified in dataGrid.jsp--%>
  var viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("InstrumentTemplateSearchDataView",userSession.getSecretKey())%>";  <%--Rel9.2 IR T36000032596 --%>
  createDataGrid("instruSearchGrid", viewName,gridLayout, initSearchParms);
 
  <%-- on select perform execute the select callback function --%>
  function selectTS() {

    <%--get array of rowkeys from the grid--%>
    var rowKeys = getSelectedGridRowKeys("instruSearchGrid");
    if (rowKeys && rowKeys.length == 1 ) {
      bankBranchSelectorDialogSelect(rowKeys[0]);
    }
  }

  function closeTSDialog() {
    hideDialog('<%=templateSearchDialogId%>');
  }
 <%--  <button onclick="instrumentSearchDialogSelect('736561','4706448');">Select</button>
  <a onclick="hideDialog('<%=instrumentSearchDialogId%>');">Cancel</a> --%>


</script>

<%
  // Unlike other pages, we do not reset the cached document with a blank document.
  // We will get rid of any errors and pass the document on.  This allows the filter
  // button to "remember" the doc parameters (required for creating the template)
  // from the prior page invocation.  The TemplateFilterForm must have
  // addToInputCache = yes

  /* if (doc!=null) doc.removeAllChildren("/Error");

  formMgr.storeInDocCache("default.doc", doc); */

  Debug.debug("End TemplateSearch.jsp");
%>