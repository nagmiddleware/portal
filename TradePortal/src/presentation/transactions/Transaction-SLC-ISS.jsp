<%--
*******************************************************************************
                              Standby Issue Page

  Description: 
    This is the main driver for the Standby Issue page.  It handles data
  retrieval of terms and terms parties (or retrieval from the input document)
  and creates the html page to display all the data for an Import DLC.
*******************************************************************************
--%>

<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>


<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,java.util.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.html.*,java.math.*,
                 com.amsinc.ecsg.util.DateTimeUtility" %>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager"
    scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" 
   scope="session">
</jsp:useBean>


<%-- ************** Data retrieval page setup begins here ****************  --%>

<%
  WidgetFactory widgetFactory = new WidgetFactory(resMgr, userSession);	
  ReferenceDataManager refData = ReferenceDataManager.getRefDataMgr();		
	
  String links = "";
  //links="SLCIssue.Part1,SLCIssue.Part2,SLCIssue.Part3,SLCIssue.Part4,SLCIssue.Part5";

  String  onLoad = "";
  String  focusField = "BenName";   // Default focus field
  boolean focusSet = false;

  // Various oid and status info from transaction and instruments used in
  // several places.
  String instrumentOid = "";         
  String transactionOid;
  String instrumentType;
  String instrumentStatus;
  String transactionType; 
  String transactionStatus;
  String rejectionIndicator            = "";
  String rejectionReasonText           = "";
  String PartySearchAddressTitle =resMgr.getTextEscapedJS("PartySearch.TabHeading",TradePortalConstants.TEXT_BUNDLE);
  
  boolean getDataFromDoc;             // Indicates if data is retrieved from the
                                      // input doc cache or from the database
  boolean phraseSelected = false;     // Indicates if a phrase was selected.
  boolean displayBankDefined = false; // Indicates if Bank Defined section 
  									  // should be displayed
  boolean           corpOrgHasMultipleAddresses   = false;
  String            corpOrgOid                    = null;
  boolean canDisplayPDFLinks = false;

  DocumentHandler doc;
   
  String loginLocale = userSession.getUserLocale();
  String loginRights = userSession.getSecurityRights();
  String  clientBankTemp = userSession.getClientBankOid();
  String tempOrgName = userSession.getOwnerOrgOid();
  
 
  //cquinton 2/12/2013 lookup locale specific datePattern description from refData manager
  String datePattern = userSession.getDatePattern();
  String datePatternDisplay = refData.getDescr("DATEPATTERN", datePattern, loginLocale);
  String dateWidgetOptions = "placeHolder:'" + datePatternDisplay + "'";

  // Variables used for populating ref data dropdowns.
  String options;
  String defaultText;

  // Variable for help sensitive link
  String helpSensitiveLink = 
  	"customer/issue_standby_lc.htm";

  // These are the beans used on the page.

  TransactionWebBean transaction  = (TransactionWebBean)
                                      beanMgr.getBean("Transaction");
  InstrumentWebBean instrument    = (InstrumentWebBean)
                                      beanMgr.getBean("Instrument");
  TemplateWebBean template        = (TemplateWebBean)
                                      beanMgr.getBean("Template");
  BankOrganizationGroupWebBean bankGroup;
  beanMgr.registerBean("com.ams.tradeportal.busobj.webbean.BankOrganizationGroupWebBean",
          "BankOrganizationGroup");
  bankGroup = (BankOrganizationGroupWebBean) beanMgr.getBean("BankOrganizationGroup");

  TermsWebBean terms              = null;

  TermsPartyWebBean termsPartyBen = beanMgr.createBean(TermsPartyWebBean.class, "TermsParty");
  TermsPartyWebBean termsPartyCor = beanMgr.createBean(TermsPartyWebBean.class, "TermsParty");
  TermsPartyWebBean termsPartyApp = beanMgr.createBean(TermsPartyWebBean.class, "TermsParty");

  // Get the document from the cache.  We'll may use it to repopulate the 
  // screen if returning from another page, a save, validation, or any other
  // mediator called from this page.  Otherwise, we assume an instrument oid
  // and transaction oid was passed in.

  doc = formMgr.getFromDocCache();
//KMehta Rel 9400 IR T36000042634 on 23 Sep 2015 Add-Begin 
  String textAreaMaxlen = "2147483646";
  //KMehta Rel 9400 IR T36000042634 on 23 Sep 2015 Add-End
  Vector error = null;
  error = doc.getFragments("/Error/errorlist/error");
  
  String buttonClicked = request.getParameter("buttonName");
  
  Debug.debug("doc from cache is " + doc.toString());
//Leelavathi - 10thDec2012 - Rel8200 CR-737 - Start
  int PMT_TERMS_DEFAULT_ROW_COUNT = 2;
  List pmtTermsList = new ArrayList(PMT_TERMS_DEFAULT_ROW_COUNT);
  PmtTermsTenorDtlWebBean pmtTerms=null;
  Vector vVector;
  int pmtTermsRowCount=0;
  if(request.getParameter("numberOfMultipleObjects1")!=null)
	  pmtTermsRowCount = Integer.parseInt(request.getParameter("numberOfMultipleObjects1"));
  else
	  pmtTermsRowCount = PMT_TERMS_DEFAULT_ROW_COUNT;
//Leelavathi - 10thDec2012 - Rel8200 CR-737 - End

/******************************************************************************
  We are either entering the page or returning from somewhere.  These are the 
  conditions for how to populate the web beans.  Data comes from either the
  database or the doc cache (/In section) with some variation.

  Mode           Condition                      Populate Beans From
  -------------  ----------------------------   --------------------------------
  Enter Page     no /In/Transaction in doc      Instrument and Template web
                                                beans already populated, get
                                                data for Terms and TermsParty
                                                web beans from database

  return from    /In/NewPartyFromSearchInfo/PartyOid   doc cache (/In); also use Party
  Party Search     exists                       SearchInfo to lookup, and 
                                                populate a specific TermsParty
                                                web bean

  return from    /In/AddressSearchInfo/Address  doc cache (/In); use /In/Address
  Address Search   SearchPartyType exists       SearchInfo to populate a specific
                                                TermsParty web bean

  return from    /Out/PhraseLookupInfo exists   doc cache (/In); but use Phrase
  Phrase Lookup                                 LookupInfo text (from /Out) to
                                                replace a specific phrase text
                                                in the /In document before 
                                                populating

  return from    /Error/maxerrorseverity < 1    Same as Enter Page (data is
  Transaction                                   retrieved from database)
    mediator
    (no error)

 return from    /Error/maxerrorseverity > 0    doc cache (/In)
  Transaction, 
  Authorize, Route,
  Delete Transaction  
    mediator
    (error)
	
******************************************************************************/

  // Assume we get the data from the doc.
  getDataFromDoc = true;

  String maxError = doc.getAttribute("/Error/maxerrorseverity");
  if (maxError != null && maxError.compareTo(
              String.valueOf(ErrorManager.WARNING_ERROR_SEVERITY)) <= 0) {
     // No errors, so don't get the data from doc.
     getDataFromDoc = false;
  }
  //ir cnuk113043991 - check to see if transaction needs to be refreshed
  // if so, refresh it and do not get data from doc as it is wrong
  if ( "Y".equals(doc.getAttribute("/Out/Refresh/Transaction")) ) {
     transaction.getDataFromAppServer();
     getDataFromDoc = false;
  }
  if (doc.getDocumentNode("/In/NewPartyFromSearchInfo/Type") != null) {
     // We have returned from the party search page.  Get data from doc cache
     getDataFromDoc = true;
  }
  if (doc.getDocumentNode("/In/AddressSearchInfo/AddressSearchPartyType") != null) {
     // We have returned from the address search page.  Get data from doc cache
     getDataFromDoc = true;
  }
  if (doc.getDocumentNode("/In/Transaction") == null) {
     // No /In/Transaction means we've never looked up the data.
     Debug.debug("No /In/Transaction section - get data from database");
     getDataFromDoc = false;
  }
  if (doc.getDocumentNode("/Out/PhraseLookupInfo") != null) {
     // A Looked up phrase exists.  Replace it in the /In document.
     Debug.debug("Found a looked-up phrase");
     getDataFromDoc = true;
     phraseSelected = true;

     String result = doc.getAttribute("/Out/PhraseLookupInfo/Result");
     if (result.equals(TradePortalConstants.INDICATOR_YES)) {
        // Take the looked up and appended phrase text from the /Out section
        // and copy it to the /In section.  This allows the beans to be 
        // properly populated.
        String xmlPath = doc.getAttribute("/In/PhraseLookupInfo/text_path");
        xmlPath = "/In" + xmlPath;

        doc.setAttribute(xmlPath, 
                      doc.getAttribute("/Out/PhraseLookupInfo/NewText"));

        // If we returned from the phrase lookup without errors, set the focus
        // to the correct field.  Otherwise, don't set the focus so the user can
        // see the error.
        if (maxError != null && maxError.compareTo(
              String.valueOf(ErrorManager.WARNING_ERROR_SEVERITY)) < 0) {
           focusField = doc.getAttribute("/In/PhraseLookupInfo/text_field_name");
           focusSet = true;
        }
     } else {
        // We do nothing because nothing was looked up.  We stil want to get
        // the data from the doc.
     }
 }

  //ctq - if newTransaction, set session variable so it persists
  String newTransaction = null;
  if (doc.getDocumentNode("/Out/newTransaction")!=null) {
    newTransaction = doc.getAttribute("/Out/newTransaction");
    session.setAttribute("newTransaction", newTransaction);
    System.out.println("found newTransaction = "+ newTransaction);
  } 
  else {
    newTransaction = (String) session.getAttribute("newTransaction");
    if ( newTransaction==null ) {
      newTransaction = TradePortalConstants.INDICATOR_NO;
    }
    System.out.println("used newTransaction from session = " + newTransaction);
  }

  //cquinton 1/18/2013 remove old close action behavior
	 	
  if (getDataFromDoc) {
     Debug.debug("Populating beans from doc cache");

     // Populate the beans from the input doc.
     try {
        instrument.populateFromXmlDoc(doc.getComponent("/In"));
        transaction.populateFromXmlDoc(doc.getComponent("/In"));
        template.populateFromXmlDoc(doc.getComponent("/In"));
        bankGroup.populateFromXmlDoc(doc.getFragment("/In"));
        
        terms = (TermsWebBean) beanMgr.getBean("Terms");
        terms.populateFromXmlDoc(doc, "/In");

        String termsPartyOid;

        termsPartyOid = terms.getAttribute("c_FirstTermsParty");
        if (termsPartyOid != null && !termsPartyOid.equals(""))
        {
           termsPartyBen.setAttribute("terms_party_oid", termsPartyOid);
           termsPartyBen.getDataFromAppServer();
        }

        termsPartyOid = terms.getAttribute("c_SecondTermsParty");
        if (termsPartyOid != null && !termsPartyOid.equals(""))
        {
           termsPartyApp.setAttribute("terms_party_oid", termsPartyOid);
           termsPartyApp.getDataFromAppServer();
        }

        termsPartyOid = terms.getAttribute("c_ThirdTermsParty");
        if (termsPartyOid != null && !termsPartyOid.equals(""))
        {
           termsPartyCor.setAttribute("terms_party_oid", termsPartyOid);
           termsPartyCor.getDataFromAppServer();
        }

        DocumentHandler termsPartyDoc;

        termsPartyDoc = doc.getFragment("/In/Terms/FirstTermsParty");
        termsPartyBen.loadTermsPartyFromDocTagsOnly(termsPartyDoc);

        termsPartyDoc = doc.getFragment("/In/Terms/SecondTermsParty");
        termsPartyApp.loadTermsPartyFromDocTagsOnly(termsPartyDoc);

        termsPartyDoc = doc.getFragment("/In/Terms/ThirdTermsParty");
        termsPartyCor.loadTermsPartyFromDocTagsOnly(termsPartyDoc);

      //Leelavathi - 10thDec2012 - Rel8200 CR-737 - Start
 		vVector = doc.getFragments("/In/Terms/PmtTermsTenorDtlList");
 		for (int iLoop=0; iLoop<vVector.size(); iLoop++)
		   {
			pmtTerms= beanMgr.createBean(PmtTermsTenorDtlWebBean.class,"PmtTermsTenorDtl");
		    DocumentHandler pmtTermsTenorDtlDoc = (DocumentHandler) vVector.elementAt(iLoop);
		    pmtTerms.populateFromXmlDoc(pmtTermsTenorDtlDoc,"/");
			 pmtTermsList.add(pmtTerms);
		   } 		 
 		
 		//SSikhakolli - Rel-8.2 UAT IR#T36000018999 Fixed for Rel-8.3 on 09/24/2013 - Begin
 		pmtTermsRowCount=vVector.size() > PMT_TERMS_DEFAULT_ROW_COUNT ?vVector.size():PMT_TERMS_DEFAULT_ROW_COUNT;
 		//SSikhakolli - Rel-8.2 UAT IR#T36000018999 Fixed for Rel-8.3 on 09/24/2013 - End
 		
 		//Leelavathi IR#T36000011623,IR#T36000012166 Rel-8.2 02/20/2013 Begin
 		//if(vVector.size() < PMT_TERMS_DEFAULT_ROW_COUNT) {
		if(vVector.size() < PMT_TERMS_DEFAULT_ROW_COUNT && InstrumentServices.isBlank(terms.getAttribute("payment_type"))) {
			//Leelavathi IR#T36000011623,IR#T36000012166 Rel-8.2 02/20/2013 End
		  for(int iLoop = vVector.size(); iLoop < PMT_TERMS_DEFAULT_ROW_COUNT; iLoop++) {
			  pmtTermsList.add(beanMgr.createBean(PmtTermsTenorDtlWebBean.class,"PmtTermsTenorDtl"));
			  }
		}
		//Leelavathi - 10thDec2012 - Rel8200 CR-737 - End

     } catch (Exception e) {
        out.println("Contact Administrator: "
              + "Unable to reload data after returning to page. "
              + "Error is " + e.toString());
     }
  } else {
     Debug.debug("populating beans from database");
     // We will perform a retrieval from the database.
     // Instrument and Transaction were already retrieved.  Get
     // the rest of the data

     terms = transaction.registerCustomerEnteredTerms();
     Debug.debug("Received terms -- getting terms parties");
     String termsPartyOid;

     termsPartyOid = terms.getAttribute("c_FirstTermsParty");
     if (termsPartyOid != null && !termsPartyOid.equals("")) {
        termsPartyBen.setAttribute("terms_party_oid", termsPartyOid);
        termsPartyBen.getDataFromAppServer();
     }
     termsPartyOid = terms.getAttribute("c_SecondTermsParty");
     if (termsPartyOid != null && !termsPartyOid.equals("")) {
        termsPartyApp.setAttribute("terms_party_oid", termsPartyOid);
        termsPartyApp.getDataFromAppServer();
     }
     termsPartyOid = terms.getAttribute("c_ThirdTermsParty");
     if (termsPartyOid != null && !termsPartyOid.equals("")) {
        termsPartyCor.setAttribute("terms_party_oid", termsPartyOid);
        termsPartyCor.getDataFromAppServer();
     }
   //Leelavathi - 10thDec2012 - Rel8200 CR-737 - Start
	 vVector=null;
	 QueryListView queryListView = null;
	try{
			StringBuffer sql = new StringBuffer();
			queryListView = (QueryListView) EJBObjectFactory.createClientEJB(formMgr.getServerLocation(), "QueryListView");
            sql.append("select pmt_terms_tenor_dtl_oid, percent, amount, tenor_type, num_days_after, days_after_type, maturity_date, p_terms_oid  ");
            sql.append(" from PMT_TERMS_TENOR_DTL ");
            sql.append(" where p_terms_oid = ?");
            //Leelavathi IR#T36000011139 Rel-8.2 08/03/2013 Begin
            sql.append(" order by ");
            sql.append(resMgr.localizeOrderBy("pmt_terms_tenor_dtl_oid"));
          //Leelavathi IR#T36000011139 Rel-8.2 08/03/2013 End
            queryListView.setSQL(sql.toString(), new Object[]{terms.getAttribute("terms_oid")});
            queryListView.getRecords();

            DocumentHandler pmtTermsOidList = new DocumentHandler();
            pmtTermsOidList = queryListView.getXmlResultSet();
          	vVector = pmtTermsOidList.getFragments("/ResultSetRecord");
			} catch (Exception e) {
	            e.printStackTrace();
	      } finally {
	            try {
	                  if (queryListView != null) {
	                        queryListView.remove();
	                  }
	            } catch (Exception e) {
	                  System.out.println("Error removing querylistview in BankGroupDetail.jsp!");
	            }
	      }
        for (int iLoop=0; iLoop<vVector.size(); iLoop++)
		   {
        	pmtTerms= beanMgr.createBean(PmtTermsTenorDtlWebBean.class,"PmtTermsTenorDtl");
        	DocumentHandler pmtTermsTenorDtlDoc = (DocumentHandler) vVector.elementAt(iLoop);
        	pmtTerms.setAttribute("pmt_terms_tenor_dtl_oid", pmtTermsTenorDtlDoc.getAttribute("/PMT_TERMS_TENOR_DTL_OID"));
        	pmtTerms.setAttribute("percent", pmtTermsTenorDtlDoc.getAttribute("/PERCENT"));
        	pmtTerms.setAttribute("amount", pmtTermsTenorDtlDoc.getAttribute("/AMOUNT"));
        	pmtTerms.setAttribute("tenor_type", pmtTermsTenorDtlDoc.getAttribute("/TENOR_TYPE"));
        	pmtTerms.setAttribute("num_days_after", pmtTermsTenorDtlDoc.getAttribute("/NUM_DAYS_AFTER"));
        	pmtTerms.setAttribute("days_after_type", pmtTermsTenorDtlDoc.getAttribute("/DAYS_AFTER_TYPE"));
        	pmtTerms.setAttribute("maturity_date", pmtTermsTenorDtlDoc.getAttribute("/MATURITY_DATE"));
        	pmtTerms.setAttribute("terms_oid", pmtTermsTenorDtlDoc.getAttribute("/TERMS_OID"));
        	pmtTermsList.add(pmtTerms);
		   } 		 
        
      	//SSikhakolli - Rel-8.2 UAT IR#T36000018999 Fixed for Rel-8.3 on 09/24/2013 - Begin
      	pmtTermsRowCount=vVector.size() > PMT_TERMS_DEFAULT_ROW_COUNT ?vVector.size():PMT_TERMS_DEFAULT_ROW_COUNT;
    	//SSikhakolli - Rel-8.2 UAT IR#T36000018999 Fixed for Rel-8.3 on 09/24/2013 - End
    
      //Leelavathi IR#T36000011623,IR#T36000012166 Rel-8.2 02/20/2013 Begin
 		//if(vVector.size() < PMT_TERMS_DEFAULT_ROW_COUNT) {
		if(vVector.size() < PMT_TERMS_DEFAULT_ROW_COUNT && InstrumentServices.isBlank(terms.getAttribute("payment_type"))) {
			//Leelavathi IR#T36000011623,IR#T36000012166 Rel-8.2 02/20/2013 End
		  for(int iLoop = vVector.size(); iLoop < PMT_TERMS_DEFAULT_ROW_COUNT; iLoop++) {
			  pmtTermsList.add(beanMgr.createBean(PmtTermsTenorDtlWebBean.class,"PmtTermsTenorDtl"));
			  }
		}
		//Leelavathi - 10thDec2012 - Rel8200 CR-737 - End
     Debug.debug("Complete terms parties");
     
  }

  /*************************************************
  * Load New Party
  **************************************************/
  if (doc.getDocumentNode("/In/NewPartyFromSearchInfo/Type") != null &&
      doc.getDocumentNode("/In/NewPartyFromSearchOutput/PartyOid") != null ) {

     // We have returned from the PartyDetailNew page.  Based on the returned 
     // data, RELOAD one of the terms party beans with the new party

     String termsPartyType = doc.getAttribute("/In/NewPartyFromSearchInfo/Type");
     String partyOid = doc.getAttribute("/In/NewPartyFromSearchOutput/PartyOid");

     Debug.debug("Returning from party search with " + termsPartyType 
           + "/" + partyOid);

     // Use a Party web bean to get the party data for the selected oid.
     PartyWebBean party = beanMgr.createBean(PartyWebBean.class,"Party");
     party.setAttribute("party_oid", partyOid);
     party.getDataFromAppServer();

     DocumentHandler partyDoc = new DocumentHandler();
     party.populateXmlDoc(partyDoc);

     partyDoc = partyDoc.getFragment("/Party");

     // Based on the party type being returned (which we previously set)
     // reload one of the terms party web beans with the data from the
     // doc.
     if (termsPartyType.equals(TradePortalConstants.BENEFICIARY)) {
        termsPartyBen.loadTermsPartyFromDoc(partyDoc);
        //cquinton 2/7/2013 pass designated party as part of partyDoc
        loadDesignatedParty(partyDoc, termsPartyCor, beanMgr);
        focusField = "BenName";
        focusSet = true;
     }
     if (termsPartyType.equals(TradePortalConstants.ADVISING_BANK)) {
        termsPartyCor.loadTermsPartyFromDoc(partyDoc);
        focusField = "";
        focusSet = true;
        onLoad += "location.hash='#" + termsPartyType + "';";
     }
     if (termsPartyType.equals(TradePortalConstants.APPLICANT)) {
        termsPartyApp.loadTermsPartyFromDoc(partyDoc);
	//rbhaduri - 13th June 06 - IR SAUG051361872
	termsPartyApp.setAttribute("address_search_indicator", "N");

        focusField = "";
        focusSet = true;
        onLoad += "location.hash='#" + termsPartyType + "';";
     }
  }
  
  // Now determine the mode for how the page operates (readonly, etc.)
  //rbhaduri - 8th August 06 - IR AOUG100368306 - Moved it here from below
  BigInteger requestedSecurityRight = SecurityAccess.SLC_CREATE_MODIFY;
  
  %>

<%@ include file="fragments/Transaction-PageMode.frag" %>

<%

    //rbhaduri - 8th August 06 - IR AOUG100368306 - Moved the code here from below
    if (isTemplate) 
   	corpOrgOid = template.getAttribute("owner_org_oid");
    else
   	corpOrgOid = instrument.getAttribute("corp_org_oid");

	//MEer Rel 9.3.5  CR-1027
	CorporateOrganizationWebBean corporateOrg = beanMgr.createBean(CorporateOrganizationWebBean.class, "CorporateOrganization");
	corporateOrg.getById(corpOrgOid);
	String bankOrgGroupOid = corporateOrg.getAttribute("bank_org_group_oid");   
    BankOrganizationGroupWebBean bankOrganizationGroup = beanMgr.createBean(BankOrganizationGroupWebBean.class,   "BankOrganizationGroup");
    bankOrganizationGroup.getById(bankOrgGroupOid);
  
     String selectedPDFType =  bankOrganizationGroup.getAttribute("slc_pdf_type"); 
     if(TradePortalConstants.APPLICATION_PDF_BASIC_TYPE.equals(selectedPDFType)  || TradePortalConstants.APPLICATION_PDF_EXPANDED_TYPE.equals(selectedPDFType)){
  			canDisplayPDFLinks = true;
     }


	//jgadela  R90 IR T36000026319 - SQL FIX
	Object[] sqlParamsAddCnt = new Object[1];
	sqlParamsAddCnt[0] =   corpOrgOid;

     if (DatabaseQueryBean.getCount("address_oid", "address", "p_corp_org_oid = ? ",true, sqlParamsAddCnt) > 0 )
     {
   		corpOrgHasMultipleAddresses = true;
     }            

  /*************************************************
  * Load address search info if we come back from address search page.
  * Reload applicant terms party web beans with the data from 
  * the address
  **************************************************/
  if (doc.getDocumentNode("/In/AddressSearchInfo/AddressSearchPartyType") != null) {
     String termsPartyType = doc.getAttribute("/In/AddressSearchInfo/AddressSearchPartyType");
     String addressOid = doc.getAttribute("/In/AddressSearchInfo/AddressOid");
     //rbhaduri - 9th Oct 06 - IR AOUG100368306
     String primarySelected = doc.getAttribute("/In/AddressSearchInfo/ChoosePrimary");
     Debug.debug("Returning from address search with " + termsPartyType 
           + "/" + addressOid);

     // Use a Party web bean to get the party data for the selected oid.
     AddressWebBean address = null;  
      //rbhaduri - 9th Oct 06 - IR AOUG100368306 - added PRIMARY case for primary address selection
     if ((addressOid != null||primarySelected != null) && termsPartyType.equals(TradePortalConstants.APPLICANT)) {
        if (primarySelected.equals("PRIMARY")) {
		
			termsPartyApp.setAttribute("name",corporateOrg.getAttribute("name"));
           	termsPartyApp.setAttribute("address_line_1",corporateOrg.getAttribute("address_line_1"));
           	termsPartyApp.setAttribute("address_line_2",corporateOrg.getAttribute("address_line_2"));
     	   	termsPartyApp.setAttribute("address_city",corporateOrg.getAttribute("address_city"));
       	   	termsPartyApp.setAttribute("address_state_province",corporateOrg.getAttribute("address_state_province"));
       	   	termsPartyApp.setAttribute("address_country",corporateOrg.getAttribute("address_country"));
       	   	termsPartyApp.setAttribute("address_postal_code",corporateOrg.getAttribute("address_postal_code"));
       	   	termsPartyApp.setAttribute("address_seq_num","1");
       	//Rel9.5 CR1132 Populate userdefinedfields from corporate
			termsPartyApp.setAttribute("user_defined_field_1", corporateOrg.getAttribute("user_defined_field_1"));
			termsPartyApp.setAttribute("user_defined_field_2", corporateOrg.getAttribute("user_defined_field_2"));
			termsPartyApp.setAttribute("user_defined_field_3", corporateOrg.getAttribute("user_defined_field_3"));
			termsPartyApp.setAttribute("user_defined_field_4", corporateOrg.getAttribute("user_defined_field_4"));
	}
	else {address = beanMgr.createBean(AddressWebBean.class, "Address");
        address.setAttribute("address_oid", addressOid);
        address.getDataFromAppServer();
        termsPartyApp.setAttribute("name",address.getAttribute("name"));
        termsPartyApp.setAttribute("address_line_1",address.getAttribute("address_line_1"));
        termsPartyApp.setAttribute("address_line_2",address.getAttribute("address_line_2"));
     	termsPartyApp.setAttribute("address_city",address.getAttribute("city"));
       	termsPartyApp.setAttribute("address_state_province",address.getAttribute("state_province"));
       	termsPartyApp.setAttribute("address_country",address.getAttribute("country"));
       	termsPartyApp.setAttribute("address_postal_code",address.getAttribute("postal_code"));
       	termsPartyApp.setAttribute("address_seq_num",address.getAttribute("address_seq_num"));
      //Rel9.5 CR1132 Populate userdefinedfields from address
      		termsPartyApp.setAttribute("user_defined_field_1", address.getAttribute("user_defined_field_1"));
      		termsPartyApp.setAttribute("user_defined_field_2", address.getAttribute("user_defined_field_2"));
      		termsPartyApp.setAttribute("user_defined_field_3", address.getAttribute("user_defined_field_3"));
      		termsPartyApp.setAttribute("user_defined_field_4", address.getAttribute("user_defined_field_4"));
       	}
     }  
     focusField = "";
     focusSet = true;
     onLoad += "location.hash='#" + termsPartyType + "';";
     
  }


  transactionType = transaction.getAttribute("transaction_type_code");
  transactionStatus = transaction.getAttribute("transaction_status");
  transactionOid = transaction.getAttribute("transaction_oid");

  // Get the transaction rejection indicator to determine whether to show the rejection reason text
  rejectionIndicator = transaction.getAttribute("rejection_indicator");
  rejectionReasonText = transaction.getAttribute("rejection_reason_text");
  
  instrumentType = instrument.getAttribute("instrument_type_code");
  instrumentStatus = instrument.getAttribute("instrument_status");
 
  /* KMehta 10/15/2013 Rel8400 CR-910  start
     Code added for getting SLC-GUA indicator on Bank group for the specific Operational Bank Org
  */
  
  String opBankOrgOid = instrument.getAttribute("op_bank_org_oid");
  QueryListView queryListView = null;
  String slc_gua_indicator = null;
  
	try{
			
			StringBuffer sql = new StringBuffer();
			queryListView = (QueryListView) EJBObjectFactory.createClientEJB(formMgr.getServerLocation(), "QueryListView");		    
		    sql.append("select slc_gua_expiry_date_required ");
		    sql.append("from bank_organization_group bog, operational_bank_org obg " );
	        sql.append("where bog.organization_oid =  obg.a_bank_org_group_oid ");                 
	        sql.append("and obg.organization_oid = ?");
	            
			Debug.debug("SLC_GUA indicator : " + sql.toString());
			queryListView.setSQL(sql.toString(), new Object[]{opBankOrgOid});
		    queryListView.getRecords();
    	    DocumentHandler result = queryListView.getXmlResultSet();
    	    slc_gua_indicator = result.getAttribute("/ResultSetRecord(0)/SLC_GUA_EXPIRY_DATE_REQUIRED");
    	    Debug.debug("slc_gua_indicator: " +slc_gua_indicator);
	}	catch (Exception e)
	   {
	      e.printStackTrace();
	   }
	   finally
	   {
	      try
	      {
	         if (queryListView != null)
	         {
	            queryListView.remove();
	         }
	      }
	      catch (Exception e)
	      {
	         System.out.println("Error removing querylistview in Transaction-SLC-ISS.jsp!");
	      }
	   }
	/* KMehta 10/15/2013 Rel8400 CR-910 End */
	
  Debug.debug("Instrument Type " + instrumentType);
  Debug.debug("Instrument Status " + instrumentStatus);
  Debug.debug("Transaction Type " + transactionType);
  Debug.debug("Transaction Status " + transactionStatus);
	
  
  //rbhaduri - 8th August 06 - IR AOUG100368306 - Moved above
  // Now determine the mode for how the page operates (readonly, etc.)
  //BigInteger requestedSecurityRight = SecurityAccess.SLC_CREATE_MODIFY;
%>


<%
  DocumentHandler phraseLists = formMgr.getFromDocCache(
                                             TradePortalConstants.PHRASE_LIST_DOC);
  if (phraseLists == null) phraseLists = new DocumentHandler();

  DocumentHandler reqdDocList = phraseLists.getFragment("/Required");
  if (reqdDocList == null) {
     reqdDocList = PhraseUtility.createPhraseList(
                                    TradePortalConstants.PHRASE_CAT_DOCREQD, 
                                    userSession, formMgr, resMgr);
     phraseLists.addComponent("/Required", reqdDocList);
  }

  DocumentHandler addlCondDocList = phraseLists.getFragment("/AddlCond");
  if (addlCondDocList == null) {
     addlCondDocList = PhraseUtility.createPhraseList(
                                    TradePortalConstants.PHRASE_CAT_ADDL_COND, 
                                    userSession, formMgr, resMgr);
     phraseLists.addComponent("/AddlCond", addlCondDocList);
  }

  DocumentHandler spclInstrDocList = phraseLists.getFragment("/SpclInstr");
  if (spclInstrDocList == null) {
     spclInstrDocList = PhraseUtility.createPhraseList(
                                    TradePortalConstants.PHRASE_CAT_SPCL_INST, 
                                    userSession, formMgr, resMgr);
     phraseLists.addComponent("/SpclInstr", spclInstrDocList);
  }

  formMgr.storeInDocCache(TradePortalConstants.PHRASE_LIST_DOC, phraseLists);
%>


<%-- ********************* HTML for page begins here ********************* --%>

<%
  if (isTemplate) {
%>
 <%--   <jsp:include page="/common/ButtonPrep.jsp" />  --%>
<%
  }
%>

<%
  // The navigation bar is only shown when editing templates.  For transactions
  // it is not shown ti minimize the chance of leaving the page without properly
  // unlocking the transaction.
  String showNavBar = TradePortalConstants.INDICATOR_NO;
  if (isTemplate) {
    showNavBar = TradePortalConstants.INDICATOR_YES;
  }
%>



<%
  // This section determines where to position the cursor depending upon:
  //	a. readony mode
  //	b. express mode
  //	c. multipart mode
  // Some of the retrieval logic above may have set a focus field.  Otherwise,
  // we'll use the initial value for focus.

  if (isFromExpress) {
     // For transactions created from express templates, the BenName field
     // is not enterable.  If the focus field is still set to that field, 
     // reset it to the first enterable field.
     if (focusField.equals("BenName")) focusField = "ApplRefNum";
  }

  
  
  if (!isReadOnly) {
     // One last condition to change the focus field if not already set.
     // In non-multi-part mode for express templates, focus field is
     // Applicant's Ref Num since Bennie fields are readonly.
     if (!focusSet && isFromExpress) {
        focusField = "ApplRefNum";
     }
  }
   

  // Auto save the form when time-out if not readonly.  
  // (Header.jsp will check for auto-save setting of the corporate org).
  String autoSaveFlag = isReadOnly ? TradePortalConstants.INDICATOR_NO : TradePortalConstants.INDICATOR_YES;
  String templateFlag = isTemplate ? TradePortalConstants.INDICATOR_YES : TradePortalConstants.INDICATOR_NO;
  
%>

<%
  String pageTitleKey;
  if ( TradePortalConstants.INDICATOR_YES.equals(newTransaction) ) {
    pageTitleKey = "SecondaryNavigation.NewInstruments";
			    
    if (isTemplate){
      if(userSession.getSecurityType().equals(TradePortalConstants.ADMIN)){
        userSession.setCurrentPrimaryNavigation("AdminNavigationBar.RefData");
      }else{
        userSession.setCurrentPrimaryNavigation("NavigationBar.RefData");
      }
    }else
      userSession.setCurrentPrimaryNavigation("NavigationBar.NewInstruments");
  } else {
    pageTitleKey = "SecondaryNavigation.Instruments";
    if (isTemplate){
      if(userSession.getSecurityType().equals(TradePortalConstants.ADMIN)){
        userSession.setCurrentPrimaryNavigation("AdminNavigationBar.RefData");
      }else{
        userSession.setCurrentPrimaryNavigation("NavigationBar.RefData");
      }
    }else 
      userSession.setCurrentPrimaryNavigation("NavigationBar.Transactions");
  }
  String helpUrl = "customer/issue_standby_lc.htm";

  //cquinton 11/19/2012 pass info for recent instruments into header
  String newRecentTransactionOid = transaction.getAttribute("transaction_oid");
  String newRecentInstrumentId = instrument.getAttribute("complete_instrument_id");
  String newRecentTransactionType = transaction.getAttribute("transaction_type_code");
  String newRecentTransactionStatus = transaction.getAttribute("transaction_status");
%>

<jsp:include page="/common/Header.jsp">
  <jsp:param name="autoSaveFlag"           value="<%=autoSaveFlag%>" />
  <jsp:param name="templateFlag"           value="<%=templateFlag%>" />
  <jsp:param name="newRecentTransactionOid" value="<%=newRecentTransactionOid%>" />
  <jsp:param name="newRecentInstrumentId"  value="<%=newRecentInstrumentId%>" />
  <jsp:param name="newRecentTransactionType" value="<%=newRecentTransactionType%>" />
  <jsp:param name="newRecentTransactionStatus" value="<%=newRecentTransactionStatus%>" />
</jsp:include>

	<div class="pageMain">
		<div class="pageContent">						
		
		<% //cr498 begin
		   //Include ReAuthentication frag in case re-authentication is required for
		   //authorization of transactions for this client
		   String certAuthURL = "";
		   Cache reCertCache = (Cache)TPCacheManager.getInstance().getCache(TradePortalConstants.CLIENT_BANK_CACHE);
		   DocumentHandler CBCResult = (DocumentHandler)reCertCache.get(userSession.getClientBankOid());	
		// KMehta Rel 9400 IR T36000042634 on 22 Sep 2015
		   String swiftLengthInd = "Y"; 
		   swiftLengthInd = CBCResult.getAttribute("/ResultSetRecord(0)/OVERRIDE_SWIFT_LENGTH_IND");	   
		   if(TradePortalConstants.INDICATOR_NO.equals(swiftLengthInd)){
			   textAreaMaxlen = "1000";
		   }	   
		   // KMehta Rel 9400 IR T36000042634 on 22 Sep 2015
		   String requireTranAuth = CBCResult.getAttribute("/ResultSetRecord(0)/REQUIRE_TRAN_AUTH");
		   boolean requireAuth = InstrumentAuthentication.requireTransactionAuthentication(
		       requireTranAuth,InstrumentAuthentication.TRAN_AUTH__SLC_ISS);
		   if (requireAuth) {
		%>   
		      <%@ include file="/logon/fragments/openReauthenticationWindow.frag" %>
		<%
		   }
		   //cr498 end
		%>	
				
				<%
		  if(isTemplate) {
		  String pageTitle;
		  String titleName;
		  String returnAction;
		  pageTitle = resMgr.getText("common.template", TradePortalConstants.TEXT_BUNDLE);
		  titleName = template.getAttribute("name");
		  StringBuffer title = new StringBuffer();
		  String itemKey = "";
		  title.append(pageTitle);
		  title.append( " : " );
		  if(instrument.getAttribute("instrument_type_code").equals("SLC")){
		  if(transaction.getAttribute("standby_using_guarantee_form").equals(TradePortalConstants.INDICATOR_NO)){     
				  itemKey = resMgr.getText("SecondaryNavigation.Instruments.OutgoingSTandbyLCSimple", TradePortalConstants.TEXT_BUNDLE); 
		  }else{
				  itemKey = resMgr.getText("SecondaryNavigation.Instruments.OutgoingStandbyLCDetailed", TradePortalConstants.TEXT_BUNDLE);
		  }
		  title.append(itemKey);
		  }
		  else
		  title.append( refData.getDescr(TradePortalConstants.INSTRUMENT_TYPE, 
		                 instrument.getAttribute("instrument_type_code"), 
		                 loginLocale) );
		  title.append( " - " );
		  title.append(titleName );
		  returnAction = "goToInstrumentCloseNavigator";
		
		  String transactionSubHeader = title.toString();
  		
  		%>
		<jsp:include page="/common/PageHeader.jsp">
				<jsp:param name="titleKey" value="<%= pageTitle%>"/>
				<jsp:param name="helpUrl"  value="<%= helpUrl%>" />
  		</jsp:include>
  		
  		<jsp:include page="/common/PageSubHeader.jsp">
 				 <jsp:param name="titleKey" value="<%= transactionSubHeader%>" />
  				 <jsp:param name="returnUrl" value="<%= formMgr.getLinkAsUrl(returnAction, response) %>" />
		</jsp:include> 		
		 <%}else{ %>
				<jsp:include page="/common/PageHeader.jsp">
				   <jsp:param name="titleKey" value="<%=pageTitleKey%>" />
				   <jsp:param name="item1Key" value="SecondaryNavigation.Instruments.OutgoingSTandbyLCSimple" />
				   <jsp:param name="helpUrl"  value="<%=helpUrl%>" />
				</jsp:include>
				
				<jsp:include page="/common/TransactionSubHeader.jsp" />
				
		<%} %>		
				
				
		<form id="TransactionSLC" name="TransactionSLC" method="post" data-dojo-type="dijit.form.Form" action="<%=formMgr.getSubmitAction(response)%>">
			  <input type=hidden value="" name=buttonName>		
        <%-- error section goes above form content --%>
        <div class="formArea">
        <jsp:include page="/common/ErrorSection.jsp" />
	
				<div class="formContent">
				  <% //cr498 begin
			  if (requireAuth) {
			%> 
			  <div style="display:none">
			  <input type=hidden name="reCertification" value="<%=TradePortalConstants.INDICATOR_NO%>">
			  <input type=hidden name="reCertOK">
			  <input type=hidden name="logonResponse">
			  <input type=hidden name="logonCertificate">
			  </div>
			<%
			  } //cr498 end
			%> 
			
			<%
			  // Store values such as the userid, security rights, and org in a
			  // secure hashtable for the form.  Also store instrument and transaction
			  // data that must be secured.
			  Hashtable secureParms = new Hashtable();
			  secureParms.put("login_oid", userSession.getUserOid());
			  secureParms.put("owner_org_oid", userSession.getOwnerOrgOid());
			  secureParms.put("login_rights", loginRights);
			
			  secureParms.put("instrument_oid", instrument.getAttribute("instrument_oid"));
			  secureParms.put("instrument_type_code", instrumentType);
			  secureParms.put("instrument_status", instrumentStatus);
			  secureParms.put("corp_org_oid",  corpOrgOid);
			
			  secureParms.put("transaction_oid", transaction.getAttribute("transaction_oid"));
			  secureParms.put("transaction_type_code", transactionType);
			  secureParms.put("transaction_status", transactionStatus);
			
			  secureParms.put("transaction_instrument_info", 
			  	transaction.getAttribute("transaction_oid") + "/" + 
				instrument.getAttribute("instrument_oid") + "/" + 
				transactionType);
			
			  // If the terms record doesn't exist, set its oid to 0.
			  String termsOid = terms.getAttribute("terms_oid");
			  if (termsOid == null) termsOid = "0";
			  secureParms.put("terms_oid", termsOid);
			
			  if (isTemplate) {
			    secureParms.put("template_oid", template.getAttribute("template_oid"));
			    secureParms.put("opt_lock", template.getAttribute("opt_lock"));
			  }
			
			  if (isTemplate) {
			%>
				<%@ include file="fragments/TemplateHeader.frag" %>
			<%	
			  }
			  System.out.println("clientBank: " +clientBankTemp);
			  
			  System.out.println("tempOrgName: " +tempOrgName);
			  
			%>
			
					<% // [BEGIN] IR-YVUH032343792 - jkok %>
					  <%@ include file="fragments/Instruments-AttachDocuments.frag" %>
					<% // [END] IR-YVUH032343792 - jkok %>
					
					 <%	if (rejectionIndicator.equals(TradePortalConstants.INDICATOR_YES)
					    || rejectionIndicator.equals(TradePortalConstants.INDICATOR_X))
					  {
					%>
						<%=widgetFactory.createSectionHeader("0", "Rejection Reason") %>
					     <%@ include file="fragments/Transaction-RejectionReason.frag" %>
					     </div>
					<%
					  }
					%>
<% //CR 821 Added Repair Reason Section 
StringBuffer repairReasonWhereClause = new StringBuffer();
int  repairReasonCount = 0;
	
	/*Get all repair reason's count from transaction history table*/
	repairReasonWhereClause.append("p_transaction_oid = ?");
	repairReasonWhereClause.append(" and REPAIR_REASON is not null order by ACTION_DATETIME");
	//jgadela  R90 IR T36000026319 - SQL FIX
	Object[] sqlParamsRCnt = new Object[1];
	sqlParamsRCnt[0] =   transaction.getAttribute("transaction_oid");
	Debug.debug("repairReasonWhereCondition query : " + repairReasonWhereClause);
	repairReasonCount = DatabaseQueryBean.getCount("A_USER_OID", "transaction_history", repairReasonWhereClause.toString(), true, sqlParamsRCnt);
			
if ( !(TradePortalConstants.TRANS_STATUS_PROCESSED_BY_BANK.equalsIgnoreCase(transactionStatus)) && repairReasonCount>0  ){%>
	<%=widgetFactory.createSectionHeader("", "TransactionHistory.RepairReason") %>
		<%@ include file="fragments/Transaction-RepairReason.frag" %>
	</div>
<%} %> 					
					<%= widgetFactory.createSectionHeader("1", "SLCIssue.Part1") %>
						<%@ include file="fragments/Transaction-SLC-ISS-General.frag" %>
					</div>
	
					<%= widgetFactory.createSectionHeader("2", "SLCIssue.Part2") %>
						<%@ include file="fragments/Transaction-SLC-ISS-DocsReqd.frag" %>
					</div>

					<%= widgetFactory.createSectionHeader("3", "SLCIssue.Part3") %>
						<%@ include file="fragments/Transaction-SLC-ISS-OtherConditions.frag" %>
					</div>
					
					<%= widgetFactory.createSectionHeader("4", "SLCIssue.Part4") %>
						<%@ include file="fragments/Transaction-SLC-ISS-BankInstructions.frag" %>
					</div>
					
					<%= widgetFactory.createSectionHeader("5", "SLCIssue.Part5") %>
						<%@ include file="fragments/Transaction-SLC-ISS-InternalInstructions.frag" %>
					</div>
	
					<%
					     // determine if Bank Defined should be displayed.  If we don't display
					     // it, the checkbox fields must be sent as secure parms (otherwise they
					     // might E reset)
					     // IR-42689 REL 9.4 SURREWSH Start
					     if (isTemplate && userSession.getSecurityType().equals(TradePortalConstants.ADMIN) || userSession.hasSavedUserSession())
					     {
					    	 //links=links+",SLCIssue.Part6";
					%>
					
					<%= widgetFactory.createSectionHeader("6", "SLCIssue.Part6") %>
						<%@ include file="fragments/Transaction-SLC-ISS-BankDefined.frag" %>
					</div>
					
					<%
					     } else {
					       secureParms.put("DraftsRequired", terms.getAttribute("drafts_required"));
					       secureParms.put("Irrevocable", terms.getAttribute("irrevocable"));
					       secureParms.put("Operative", terms.getAttribute("operative"));
                                               //cquinton 10/16/2013 Rel 8.3 ir#21872 do not setup ucpversiondetailind here
					       //secureParms.put("UCPVersionDetailInd", 
					       //                terms.getAttribute("ucp_version_details_ind"));
					     }
					  
					%>
					
					<% if (TradePortalConstants.TRANS_STATUS_PROCESSED_BY_BANK.equalsIgnoreCase(transactionStatus) && repairReasonCount>0  ){
						 if (TradePortalConstants.ADMIN.equals(userSession.getSecurityType()))   {%>
  								<%=widgetFactory.createSectionHeader("7", "TransactionHistory.RepairReason", null,true) %>
								<%@ include file="fragments/Transaction-RepairReason.frag" %>
							</div>
						<%}else{%>
							<%=widgetFactory.createSectionHeader("6", "TransactionHistory.RepairReason", null, true) %>
							<%@ include file="fragments/Transaction-RepairReason.frag" %>
							</div>
						<% 	}	 
					 }%> 
				</div> <%-- formContent Area closes here --%>
				</div><%--formArea--%>
				
					<div class="formSidebar" data-dojo-type="widget.FormSidebar" data-dojo-props="form: 'TransactionSLC'">
					    <jsp:include page="/common/Sidebar.jsp">
								<jsp:param name="links" value="<%=links%>" />
								<jsp:param name="isReadOnly" value="<%=isReadOnly%>" />
								<jsp:param name="certAuthURL" value="<%=certAuthURL%>" />
								<jsp:param name="buttonPressed" value="<%=buttonClicked%>" />
								<jsp:param name="error" value="<%= error%>" />
								<jsp:param name="formName" value="0" />
								<jsp:param name="isTemplate" value="<%=isTemplate%>"/>
								 <jsp:param name="showLinks" value="true" />  
								 <jsp:param name="showApplnForm" value="<%=canDisplayPDFLinks%>"/>
								 <jsp:param name="isNewTransaction" value="<%=newTransaction%>"/>
							</jsp:include>
					</div> <%--closes sidebar area--%>
	 
				  <%@ include file="fragments/PhraseLookupPrep.frag" %>
				  <%@ include file="fragments/PartySearchPrep.frag" %> 
<input type="hidden" name="selection" />
<input type="hidden" name="ChoosePrimary" />
			  
				<%= formMgr.getFormInstanceAsInputField("Transaction-SLCForm", secureParms) %>
				<div id="PartySearchDialog"></div>
			</form>
		</div> <%-- pageContentt Area closes here --%>
	</div>
<div style="clear:both;"></div>
<%-- KMehta on 12 May 2014 @ Rel 90 for IR T36000025449 Start --%>
<jsp:include page="/common/Footer.jsp">
   <jsp:param name="autoSaveFlag"           value="<%=autoSaveFlag%>" />
   <jsp:param name="templateFlag"           value="<%=templateFlag%>" />
   <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>
<%-- KMehta on 12 May 2014 @ Rel 90 for IR T36000025449 End --%>
<jsp:include page="/common/SidebarFooter.jsp"/>

<script>

  <%-- cquinton 3/3/2013 add local var --%>
  var local = {};

<%
  if (!isReadOnly && InstrumentServices.isNotBlank(focusField)) {
%>

function setPmtSight() {
	dijit.getEnclosingWidget(document.forms[0].pmtSight).set('checked',true);

}
function setBankChargeOther(){
	dijit.getEnclosingWidget(document.forms[0].bankChargeOther).set('checked',true);
}

function setDeliveredToAgent(){
	dijit.getEnclosingWidget(document.forms[0].deliveredToAgent).set('checked',true);	
}
function setDeliveredToOther(){
	dijit.getEnclosingWidget(document.forms[0].deliveredToOther).set('checked',true);	
}

function setGuarIssueOverseasBank() {
	dijit.getEnclosingWidget(document.forms[0].guarIssueOverseasBank).set('checked',true);
}

function setOverSeasValidDate(){
	 dijit.byId("overseas_validity_date").reset();
}
  require(["dijit/registry", "dojo/ready"],
    function(registry, ready ) {
      ready(function() {
    	<%-- KMehta Rel 9400 IR T36000042634 on 23 Sep 2015 Add-Begin  --%>
    	  <% if(TradePortalConstants.INDICATOR_NO.equals(swiftLengthInd)){ %>
	    	  var textAreaMaxLength = 1000;	    	  
	    	  var spclBankInst = '<%=terms.getAttribute("special_bank_instructions")%>';
	    	  spclBankInst = spclBankInst.substring(0,textAreaMaxLength);
	    	  dom.byId("SpclBankInstructions").value = spclBankInst;
	    	  var CandCOtherText = '<%=terms.getAttribute("coms_chrgs_other_text")%>';
	    	  CandCOtherText = spclBankInst.substring(0,textAreaMaxLength);
	    	  dom.byId("CandCOtherText").value = CandCOtherText;
	    	 
	    	  
	    	 
    	  <%}%>
    	<%-- KMehta Rel 9400 IR T36000042634 on 23 Sep 2015 Add-Begin --%>
        var focusFieldId = '<%= focusField %>';
        if ( focusFieldId ) {
          var focusField = registry.byId(focusFieldId);
          focusField.focus();
        }
      });
  });
<%
  }
%>
</script>

</body>
</html>
<%
   // Finally, reset the cached document to eliminate carryover of
   // information to the next visit of this page.
   formMgr.storeInDocCache("default.doc", new DocumentHandler());
%>

<%@ include file="fragments/DesignatedPartyLookup.frag" %>
<script type="text/javascript">

  var itemid;
  var section;

  function SearchParty(identifierStr, sectionName ,partyType){
	
    itemid = String(identifierStr);
    section = String(sectionName);
    partyType=String(partyType);
    <%-- itemid = 'OrderingPartyName,OrderingPartyAddress1,OrderingPartyAddress2,OrderingPartyAddress3,OrderingPartyAddress4,OrderingPartyCountry'; --%>

    require(["dijit/registry", "dojo/ready"],
    function(registry, ready ) {
      ready(function() {
        var focusField = registry.byId("BenName");
        if (partyType == "APP") {
            focusField = registry.byId("ApplRefNum");        
        }
        if (partyType == "COR") {
            focusField = registry.byId("overseas_validity_date");        
        }
          focusField.focus();
      });
    });

    require(["dojo/dom", "t360/dialog"], function(dom, dialog ) {

      <%-- cquinton 2/7/2013 --%>
      <%-- set the SearchPartyType input so it is included on new party form submit --%>
      var searchPartyTypeInput = dom.byId("SearchPartyType");
      if ( searchPartyTypeInput ) {
        searchPartyTypeInput.value=partyType;
      }

      dialog.open('PartySearchDialog', '<%=PartySearchAddressTitle%>',
        'PartySearch.jsp',
        ['returnAction','filterText','partyType','unicodeIndicator','itemid','section'],
        ['selectTransactionParty','',partyType,'<%=TradePortalConstants.INDICATOR_NO%>',itemid,section]); <%-- parameters --%>
    });
  }

function defaultAddlConditionsText(){	
	<%--  var temp = dijit.byId("AddlConditionsText").value;
	 if( temp = 'Enter other Conditions'){ 
		document.getElementById("AddlConditionsText").value = "";
	}  --%>
}

function defaultSpclBankInstructions(){
	<%--  var temp1 = dijit.byId("SpclBankInstructions").value;
	 if( temp1 = 'Enter any special instructions for your bank'){ 
		document.getElementById("SpclBankInstructions").value = "";
	}  --%>
}

function defaultCandCOtherText(){
	<%--  var temp11 = dijit.byId("CandCOtherText").value;
	 if( temp11 = 'Enter any additional Settlement or Commissions & Charges instructions'){ 
		document.getElementById("CandCOtherText").value = "";
	}  --%>
}

var isReadOnly = '<%=isReadOnly%>';

function enableField(){
	if(isReadOnly && dijit.byId('DraftsRequired') != undefined){
		if(dijit.byId('DraftsRequired').checked == true){
			dijit.byId("DrawnOnParty").setDisabled(false);
		}else{
			dijit.byId("DrawnOnParty").setDisabled(true);
		}
	}
}
<%-- Jyoti IR#T36000011687,IR#T36000011402,IR#T36000011401,IR#T36000011688 CR737 Rel-8.2 27th Feb,2013  Start--%>
function addConstraint(){	
	require(["dojo/dom","dojo/domReady!" ], function(dom) {
	 var amount=dijit.byId("OutgoingSLCIssue.Amount");
	 var percentage =dijit.byId("OutgoingSLCIssue.Percentage");
     var amountChkd=amount.checked;
     var percentageChkd=percentage.checked;
     var table = dom.byId('pymtTerms');     
	  var rowCount = table.rows.length;
     if(amountChkd){
 		for (var i=0; i<(rowCount-1);i++){
 			var f=dijit.byId("PmtTermPercent"+i);
 			<%--  Kiran IR#T36000015041 CR-737 Rel-8.2 06/21/2013 Begin  --%>
 			<%-- Added the regular expression inorder to display zero after decimal in amount field  --%>
 			f.set('regExp','[0-9]{0,15}([.][0-9]{0,15})?');
 			<%-- Kiran IR#T36000015041 CR-737 Rel-8.2 06/21/2013 End --%>
 			f.set('style',"width: 5em");
 		}
     }else if(percentageChkd){
    	 for (var i=0; i<(rowCount-1);i++){
  			var f=dijit.byId("PmtTermPercent"+i);
  			<%-- Kiran IR#T36000015041 CR-737 Rel-8.2 06/21/2013 Begin --%>
  			<%-- Added the regular expression in order to match percentage field criteria --%>
  			f.set('regExp','^([0-9]{1,2}([.][0-9]{0,2})?$|100([.][0]{0,2})?)$');
  			<%-- Kiran IR#T36000015041 CR-737 Rel-8.2 06/21/2013 End --%>
  			f.set('style',"width: 3em");
  		} 
     }
});
}	
<%-- Jyoti IR#T36000011687,IR#T36000011402,IR#T36000011401,IR#T36000011688 CR737 Rel-8.2 27th Feb,2013 End--%>
require(["dijit/registry","dojo/ready"], function(registry,ready){
	ready(function(){
		if(isReadOnly && dijit.byId('DraftsRequired') != undefined){
			if(dijit.byId('DraftsRequired').checked == true){
				dijit.byId("DrawnOnParty").setDisabled(false);
			}else{
				dijit.byId("DrawnOnParty").setDisabled(true);
			}
		}
		
		<%-- cquinton 10/16/2013 Rel 8.3 ir#21872 move disable of ucp fields to widget creation --%>
	});
});

  <%-- cquinton 3/2/2013 local phrase lookup --%>
  require(["t360/phraseLookup"],
          function( phraseLookup ) {
    local.phraseLookupAppend = function(phraseSelect, textArea, textAreaMaxLength ) {
      phraseLookup.appendPhrase( phraseSelect, textArea, textAreaMaxLength, "_errorSection" );
    };
  });

<%-- Leelavathi 10thDec2012 - Rel8200 CR-737 - Start --%>
require(["dojo/ready","dijit/registry", "dojo/on","dojo/_base/array","t360/common","dojo/dom","dojo/domReady!"], function(ready,registry, on,arrayUtil,common,dom){
	ready(function(){
		var PmtTermsInPercentAmountIndDiv=document.getElementById("PmtTermsInPercentAmountIndDiv");
		var PmtTermsInPercentAmountIndDivDisable=function(){
			PmtTermsInPercentAmountIndDivWs=registry.findWidgets(PmtTermsInPercentAmountIndDiv);
			arrayUtil.forEach(PmtTermsInPercentAmountIndDivWs, function(entry, i) {
		        entry.set('disabled',true);
		        });
			document.getElementById("PmtTermsInPercentAmountIndDiv").style.display = "none";
		};
		var PmtTermsInPercentAmountIndDivEnable=function(){
			PmtTermsInPercentAmountIndDivWs=registry.findWidgets(PmtTermsInPercentAmountIndDiv);
			arrayUtil.forEach(PmtTermsInPercentAmountIndDivWs, function(entry, i) {
				entry.set('disabled',false);
		        entry.set('readOnly',false);
		        });
			document.getElementById("PmtTermsInPercentAmountIndDiv").style.display = "block";
		};
		addConstraint();<%--  Jyoti IR#T36000011687,IR#T36000011402,IR#T36000011401,IR#T36000011688 CR737 Rel-8.2 27th Feb,2013 added --%>
		var pymtTermsDiv=document.getElementById("pymtTermsDiv");
		var pymtTermsDivDisable=function(){
			pymtTermsDivWs=registry.findWidgets(pymtTermsDiv);
			arrayUtil.forEach(pymtTermsDivWs, function(entry, i) {
		        entry.set('disabled',true);
		        });
			document.getElementById("pymtTermsDiv").style.display = "none";
		};
		var pymtTermsDivEnable=function(){
			pymtTermsDivWs=registry.findWidgets(pymtTermsDiv);
			arrayUtil.forEach(pymtTermsDivWs, function(entry, i) {
		        entry.set('disabled',false);
		        });
			document.getElementById("pymtTermsDiv").style.display = "block";
		};
		
		var PmtTermsSpecialTenorTextDiv = document.getElementById("PmtTermsSpecialTenorTextDiv");
		var PmtTermsSpecialTenorTextDivDisable=function(){
			PmtTermsSpecialTenorTextDivWs=registry.findWidgets(PmtTermsSpecialTenorTextDiv);
			arrayUtil.forEach(PmtTermsSpecialTenorTextDivWs, function(entry, i) {
		        entry.set('disabled',true);
		        });
			document.getElementById("PmtTermsSpecialTenorTextDiv").style.display = "none";
		};
		var PmtTermsSpecialTenorTextDivEnable=function(){
			PmtTermsSpecialTenorTextDivWs=registry.findWidgets(PmtTermsSpecialTenorTextDiv);
			arrayUtil.forEach(PmtTermsSpecialTenorTextDivWs, function(entry, i) {
		        entry.set('disabled',false);
		        });
					document.getElementById("PmtTermsSpecialTenorTextDiv").style.display = "block";
		};
		
		var add2MoreLinesDiv=document.getElementById("add2MoreLinesDiv");
		var add2MoreLinesDivDisable=function(){
			add2MoreLinesDivWs=registry.findWidgets(add2MoreLinesDiv);
			arrayUtil.forEach(add2MoreLinesDivWs, function(entry, i) {
		        entry.set('disabled',true);
		        });
			document.getElementById("add2MoreLinesDiv").style.display = "none";
		};
		var add2MoreLinesDivEnable=function(){
			add2MoreLinesDivWs=registry.findWidgets(add2MoreLinesDiv);
			arrayUtil.forEach(add2MoreLinesDivWs, function(entry, i) {
		        entry.set('disabled',false);
		        });
			document.getElementById("add2MoreLinesDiv").style.display = "block";
		};
		var deleteRowsFrompymtTerms=function(){
			var table = dom.byId('pymtTerms');
            var rowCount = table.rows.length;
            for(var i=(rowCount-1); i>0; i--) {
                 table.deleteRow(i);       
                 dijit.byId('PmtTermPercent'+(i-1)).value="";
				 dijit.byId('PmtTermPercent'+(i-1)).destroy( true );
				 dijit.byId('PmtTermTenorType'+(i-1)).value="";
				 dijit.byId('PmtTermTenorType'+(i-1)).destroy( true );
				 dijit.byId('PmtTermNumDaysAfter'+(i-1)).value="";
				 dijit.byId('PmtTermNumDaysAfter'+(i-1)).destroy( true );
				 dijit.byId('PmtTermDaysAfterType'+(i-1)).value="";
				 dijit.byId('PmtTermDaysAfterType'+(i-1)).destroy( true );
				 dijit.byId('PmtTermMaturityDate'+(i-1)).value="";
				 dijit.byId('PmtTermMaturityDate'+(i-1)).destroy( true );
            }
		};
		var PmtTermsInPercentAmountIndReadOnly=function(){
			<%-- Leelavathi IR#T36000011742 Rel-8.2 02/20/2013 Begin --%>
			<%-- dijit.byId("OutgoingSLCIssue.Percentage").set('disabled',true); --%>
			<%-- dijit.byId("OutgoingSLCIssue.Amount").set('disabled',true); --%>
			dijit.byId("OutgoingSLCIssue.Percentage").set('readOnly',true);
			<%-- Leelavathi IR#T36000014888 CR-737 Rel-8.2 11/04/2013 Begin --%>
			<%-- dijit.byId("OutgoingSLCIssue.Amount").set('readOnly',true); --%>
			dijit.byId("OutgoingSLCIssue.Amount").set('disabled',true);
			<%-- Leelavathi IR#T36000014888 CR-737 Rel-8.2 11/04/2013 End --%>

			<%-- Leelavathi IR#T36000011742 Rel-8.2 02/20/2013 End --%>
		};
		
		var add2MoreLinesTab = function(cnt){
			var myTable = dom.byId('pymtTerms');
			<%-- Leelavathi IR#T36000011878 Rel-8.2 18/2/2013 Begin --%>
			var rowIndex = myTable.rows.length - 1;
			common.appendAjaxTableRows(myTable,"pmtTermsIndex",
		    		  "/portal/transactions/fragments/Add2morePaymentTermRows.jsp?pmtTermsIndex="+rowIndex+"&count="+cnt+"&TermsPmtType="+TermsPmtType); <%-- table length includes header, so subtract 1 --%>
		    		  
		      document.getElementById("noOfPmtTermsRows").value = eval(rowIndex+cnt);   		  
		};
		<%-- Leelavathi IR#T36000011878 Rel-8.2 18/2/2013 End --%>
		
		var TermsPmtType=registry.byId("TermsPmtType");
		<%-- Kiran IR#T36000015047 Rel-8.2 04/03/2013 Start --%>
		<%-- Kiran IR#T36000015047 Rel-8.2 06/03/2013 Start --%>
		<%--  Removed the conditions in order for the Add 2 more buttons to work --%>
		<% if (!(isReadOnly) ) 
				{     %>
				
			on(registry.byId("add2MoreLines"),"click",function(){ 
													add2MoreLinesTab(2) } );
		<%--  Kiran IR#T36000015047 Rel-8.2 06/03/2013 End --%>
		<%-- Leelavathi IR#T36000015730 CR-737 Rel-8.2 19/04/2013 Begin --%>
		<% }else{ %>
				add2MoreLinesDivDisable();
			<%}%>
			<%-- Leelavathi IR#T36000015730 CR-737 Rel-8.2 19/04/2013 End --%>
		<%-- Kiran IR#T36000015047 Rel-8.2 04/03/2013 End --%>
		var pymtTerms=document.getElementById("pymtTerms");
		on(registry.byId("OutgoingSLCIssue.Percentage"),"change",function(isChecked){ 
							if(isChecked){
								<%-- Jyoti IR#T36000011687,IR#T36000011402,IR#T36000011401,IR#T36000011688 CR737 Rel-8.2 27th Feb,2013  Start  --%>
								var table = dom.byId('pymtTerms');
								var rowCount = table.rows.length;
								for (var i=0; i<(rowCount-1);i++){
									var f=dijit.byId("PmtTermPercent"+i);
									<%-- Kiran IR#T36000015041 CR-737 Rel-8.2 06/21/2013 Begin --%>
						  			<%-- Added the regular expression in order to match percentage field criteria --%>
									f.set('regExp','^([0-9]{1,2}([.][0-9]{0,2})?$|100([.][0]{0,2})?)$');
									<%-- Kiran IR#T36000015041 CR-737 Rel-8.2 06/21/2013 End --%>
									f.set('style',"width: 3em");
									f.setValue(0);
								}
								<%-- Jyoti IR#T36000011687,IR#T36000011402,IR#T36000011401,IR#T36000011688 CR737 Rel-8.2 27th Feb,2013 End --%>
							
									pymtTerms.rows[0].cells[0].innerHTML='<%=resMgr.getText("OutgoingSLCIssue.PaymentInPercent",TradePortalConstants.TEXT_BUNDLE)%>';
								}
							},true );
		on(registry.byId("OutgoingSLCIssue.Amount"),"change",function(isChecked){ 
							if(isChecked){
								<%-- Jyoti IR#T36000011687,IR#T36000011402,IR#T36000011401,IR#T36000011688 CR737 Rel-8.2 27th Feb,2013  Start  --%>
								var table = dom.byId('pymtTerms');
								var rowCount = table.rows.length;
								for (var i=0; i<(rowCount-1);i++){
									var f=dijit.byId("PmtTermPercent"+i);
									<%--  Kiran IR#T36000015041 CR-737 Rel-8.2 06/21/2013 Begin  --%>
						 			<%-- Added the regular expression inorder to display zero after decimal in amount field  --%>
									f.set('regExp','[0-9]{0,15}([.][0-9]{0,15})?');
									<%-- Kiran IR#T36000015041 CR-737 Rel-8.2 06/21/2013 End --%>
									f.set('style',"width: 5em");
									f.setValue(0);
								}
								<%-- Jyoti IR#T36000011687,IR#T36000011402,IR#T36000011401,IR#T36000011688 CR737 Rel-8.2 27th Feb,2013  End 	 --%>
							
									pymtTerms.rows[0].cells[0].innerHTML='<%=resMgr.getText("OutgoingSLCIssue.PaymentInAmount",TradePortalConstants.TEXT_BUNDLE)%>';
								}	
							},true );
		
		on(TermsPmtType, "change", function(TermsPmtType){
			if(TermsPmtType=="PAYM"){
				PmtTermsInPercentAmountIndDivDisable();
				PmtTermsSpecialTenorTextDivDisable();
				pymtTermsDivDisable();
				add2MoreLinesDivDisable();
			}
			else if(TermsPmtType=="ACCP" || TermsPmtType=="DEFP"){
				pymtTermsDivEnable();
				PmtTermsInPercentAmountIndDivEnable();
				registry.byId("OutgoingSLCIssue.Percentage").set('checked',true);
				PmtTermsSpecialTenorTextDivDisable();
				add2MoreLinesDivEnable();
				deleteRowsFrompymtTerms();
				PmtTermsInPercentAmountIndReadOnly();
				add2MoreLinesTab(1);
				add2MoreLinesDivDisable();
			}
			else if(TermsPmtType=="MIXP" || TermsPmtType=="NEGO"){
				pymtTermsDivEnable();
				PmtTermsInPercentAmountIndDivEnable();
				registry.byId("OutgoingSLCIssue.Percentage").set('checked',true);
				PmtTermsSpecialTenorTextDivDisable();
				deleteRowsFrompymtTerms();
				add2MoreLinesDivEnable();
				add2MoreLinesTab(2);
				<%-- deleteColsFromPymtTerms(); --%>
				<%-- var table = dojo.byId('pymtTerms'); --%>
				<%-- var insertRowIdx = table.rows.length; --%>
				<%-- if(insertRowIdx<3){ --%>
				<%-- 	var rowCount= insertRowIdx-3; --%>
				<%-- 	add2MoreLinesTab(rowCount); --%>
				<%-- } --%>
			}
			else if(TermsPmtType=="SPEC"){
				PmtTermsInPercentAmountIndDivDisable();
				pymtTermsDivDisable();
				add2MoreLinesDivDisable();
				PmtTermsSpecialTenorTextDivEnable();
			}
			
		},true);
		
	})});
<%-- Leelavathi IR#T36000011665,IR#T36000011628,IR#T36000011882,IR#T36000011880 Rel-8.2 02/18/2013 Begin  --%>
function DaysAfterAndDaysAfterTypeReadOnly(index){
	if(dijit.byId("PmtTermMaturityDate"+index) != ''){
		dijit.byId("PmtTermNumDaysAfter"+index).set("value","");
		dijit.byId("PmtTermNumDaysAfter"+index).set('readOnly',true);
		dijit.byId("PmtTermDaysAfterType"+index).set("value","");
		dijit.byId("PmtTermDaysAfterType"+index).set('readOnly',true);
	}
	<%-- Kiran IR#T36000015488 Rel-8.2 04/03/2013 Start --%>
	<%-- Leelavathi IR#T36000015047 05/14/2013 CR-737 Rel-8.2 Begin --%>
	else if ( dijit.byId("PmtTermTenorType"+index) != 'SGT') {
		<%-- Leelavathi IR#T36000015047 05/14/2013 CR-737 Rel-8.2 End --%>
		dijit.byId("PmtTermNumDaysAfter"+index).set('readOnly',false);
		dijit.byId("PmtTermDaysAfterType"+index).set('readOnly',false);
	}
	<%-- Kiran IR#T36000015488 Rel-8.2 04/03/2013 End --%>
}
function MaturityDateReadOnly(index){
	if( dijit.byId("PmtTermNumDaysAfter"+index) != '' 
		&& dijit.byId("PmtTermDaysAfterType"+index) != ''
		){
		dijit.byId("PmtTermMaturityDate"+index).set('readOnly',true);
	}
	<%-- Kiran IR#T36000015488 Rel-8.2 04/03/2013 Start --%>
	<%-- Leelavathi IR#T36000015047 05/14/2013 CR-737 Rel-8.2 Begin --%>
	else if ( dijit.byId("PmtTermTenorType"+index) != 'SGT') {
		<%-- Leelavathi IR#T36000015047 05/14/2013 CR-737 Rel-8.2 End --%>
		dijit.byId("PmtTermMaturityDate"+index).set('readOnly',false);
	}
	<%-- Kiran IR#T36000015488 Rel-8.2 04/03/2013 End --%>
}
<%--  Leelavathi IR#T36000011665,IR#T36000011628,IR#T36000011882,IR#T36000011880 Rel-8.2 02/18/2013 End  --%>
<%-- Leelavathi IR#T36000011745 Rel-8.2 02/25/2013 Begin  --%>
function TenorDetailsAndMaturityDateReadOnly(index){
	dijit.byId("PmtTermNumDaysAfter"+index).set("value","");
	dijit.byId("PmtTermDaysAfterType"+index).set("value","");
	dijit.byId("PmtTermMaturityDate"+index).set("value",{});
	if(dijit.byId("PmtTermTenorType"+index) == 'SGT' ){
		<%-- Leelavathi IR#T36000016890 CR_737 12/05/2013 Rel-8.2 Begin --%>
		
			
		<%-- Leelavathi IR#T36000016890 CR_737 12/05/2013 Rel-8.2 End --%>
		dijit.byId("PmtTermNumDaysAfter"+index).set('readOnly',true);
		dijit.byId("PmtTermDaysAfterType"+index).set('readOnly',true);
		dijit.byId("PmtTermMaturityDate"+index).set('readOnly',true);
	}	
	<%-- Leelavathi IR#T36000016179 CR-737 Rel-8.2 19/04/2013 Begin --%>
	else{
		dijit.byId("PmtTermNumDaysAfter"+index).set('readOnly',false);
		dijit.byId("PmtTermDaysAfterType"+index).set('readOnly',false);
		dijit.byId("PmtTermMaturityDate"+index).set('readOnly',false);
	}
	<%-- Leelavathi IR#T36000016179 CR-737 Rel-8.2 19/04/2013 End --%>
}
<%-- Leelavathi IR#T36000011745 Rel-8.2 02/25/2013 End --%>

<%-- Leelavathi 10thDec2012 - Rel8200 CR-737 - End --%>


  <%-- cquinton 3/2/2013 local phrase lookup --%>
  require(["t360/phraseLookup"],
          function( phraseLookup ) {
    local.phraseLookupAppend = function(phraseSelect, textArea, textAreaMaxLength ) {
      phraseLookup.appendPhrase( phraseSelect, textArea, textAreaMaxLength, "_errorSection" );
    };
  });

	<%-- Sandeep - Rel-8.3 CR-752 Dev 06/12/2013 - Begin --%>
<%-- cquinton 10/16/2013 ir#21872 Rel 8.3 move select ucp checkbox function to part of changeUCPVersion below --%>
  	
  	<%-- When ICC Publication check box has been un-selected then clere the version and details fields under it --%>
  	function clearICCPublication(selectField) {
	   	require(["dijit/registry"], function(registry) {
	   		if(!registry.byId(selectField).checked){
	   			registry.byId("UCPVersion").set("value",""); <%-- this invokes changeUCPVersion below too! --%>
		   		registry.byId("UCPDetails").set("value","");
	   		}
	    });
	}
	
<%-- cquinton 10/16/2013 ir#21872 Rel 8.3 start - 
     changeUCPVersion both sets the ucp checkbox field and disables/enables details --%>
  	function changeUCPVersion(){
  		require(["dijit/registry"], function(registry) {
	  		var ucpVersion = registry.byId('UCPVersion').value;

                        if ( ucpVersion && ucpVersion!="" ) { <%-- if called by clearICCPublication above --%>
                            registry.byId('UCPVersionDetailInd').set('checked', true);
			}
			
			if(ucpVersion == '<%=TradePortalConstants.SLC_OTHER%>') {
				registry.byId('UCPDetails').setDisabled(false);
			}else{
				registry.byId('UCPDetails').setDisabled(true);
				registry.byId('UCPDetails').set('value', '');
			}
  		});	
  	}
<%-- cquinton 10/16/2013 ir#21872 Rel 8.3 end --%>
  	<%-- Sandeep - Rel-8.3 CR-752 Dev 06/12/2013 - End --%>
<%-- moved from frag to jsp.	 --%>
 function maxNumDisable(){
 require(["dijit/registry"], function(registry) {
  var autoExtensionIndicator = registry.byId("AutoExtensionIndicator");
  if(autoExtensionIndicator){
 	 autoExtensionIndicator.on("change", function(checkValue) {
	          if ( checkValue ) {
	        	  registry.byId("MaxAutoExtensionAllowed").setDisabled(false);	  
	        	  registry.byId("AutoExtensionPeriod").setDisabled(false);
	        	  registry.byId("FinalExpiryDate").setDisabled(false);
	        	  registry.byId("NotifyBeneDays").setDisabled(false);
	        	  
	          } else {
	        	  registry.byId('MaxAutoExtensionAllowed').set('value','');
	        	  registry.byId('AutoExtensionPeriod').set('value','');
	        	  registry.byId('AutoExtensionDays').set('value','');
	        	  registry.byId('FinalExpiryDate').set('DisplayedValue','');
	        	  registry.byId('NotifyBeneDays').set('value','');
	        	  registry.byId("MaxAutoExtensionAllowed").setDisabled(true);	  
	        	  registry.byId("AutoExtensionPeriod").setDisabled(true);
	        	  registry.byId("AutoExtensionDays").setDisabled(true);
	        	  registry.byId("FinalExpiryDate").setDisabled(true);
	        	  registry.byId("NotifyBeneDays").setDisabled(true);
	          }
	        });
  }    
 });

}

 function numDaysDisable(){
	  require(["dijit/registry"], function(registry) {
		     var autoExtendPeriod = registry.byId("AutoExtensionPeriod");
		     if(autoExtendPeriod){
		       if(autoExtendPeriod.value == 'DAY'){
		    	  registry.byId("AutoExtensionDays").setDisabled(false);
		       } else {
		    	 registry.byId('AutoExtensionDays').set('value','');
		    	 registry.byId("AutoExtensionDays").setDisabled(true); 
		       }
		     }
  });
 }

 function extensionType(type){
	require(["dijit/registry"], function(registry) {
	if(registry.byId("AutoExtensionIndicator").checked){
	 if(type == 'FinalExpiryDate') {
		 var finalExpiryDate = registry.byId('FinalExpiryDate').value;
		 if (finalExpiryDate=="" ||  finalExpiryDate==null) { 
			 registry.byId("MaxAutoExtensionAllowed").setDisabled(false);
       }else{
      	 registry.byId("MaxAutoExtensionAllowed").setDisabled(true);
       }  
	 }else if(type == 'MaxAutoExtensionAllowed'){
		 var maxAutoExtensionAllowed = registry.byId('MaxAutoExtensionAllowed').value;
		 if (isNaN(maxAutoExtensionAllowed)) { 
			 registry.byId("FinalExpiryDate").setDisabled(false);
       }else{
       	 registry.byId("FinalExpiryDate").setDisabled(true);
       } 
	 }
	}
	});
 }

</script>
