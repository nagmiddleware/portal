<%--
*******************************************************************************
                              Guarantee Amend Page

  Description: 
    This is the main driver for the Guarantee Amend page.  It handles data
  retrieval of terms (or retrieval from the input document) and creates the 
  html page to display all amendment data for an Amendment Guarantee.
*******************************************************************************
--%>

<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,java.util.*,
                 com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.html.*,java.math.*,com.amsinc.ecsg.util.DateTimeUtility" %>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session"></jsp:useBean>
<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session"></jsp:useBean>
<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session"></jsp:useBean>
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>

<%-- ************** Data retrieval and page setup logic begins here ****************  --%>

<%
  WidgetFactory widgetFactory = new WidgetFactory(resMgr, userSession);
  //cquinton 2/12/2013 add refdata manager for datepattern lookup
  ReferenceDataManager refData = ReferenceDataManager.getRefDataMgr();

  String focusField           = "amount";   // Default focus field

  String instrumentOid        = null;
  String instrumentType       = null;
  String instrumentStatus     = null;

  String transactionOid       = null;
  String transactionType      = null;
  String transactionStatus    = null;

  String rejectionIndicator   = "";
  String rejectionReasonText  = "";

  String termsPartyOid        = null;

  boolean getDataFromDoc      = true; //== true if data is retrieved from the doc cache
  boolean phraseSelected      = false; // Indicates if a phrase was selected.

  DocumentHandler doc         = null; //default.doc

  String loginLocale          = userSession.getUserLocale();
  String loginRights          = userSession.getSecurityRights();

  //cquinton 2/12/2013 lookup locale specific datePattern description from refData manager
  String datePattern = userSession.getDatePattern();
  String datePatternDisplay = refData.getDescr("DATEPATTERN", datePattern, loginLocale);
  String dateWidgetOptions = "placeHolder:'" + datePatternDisplay + "'";

    boolean isProcessedByBank	  = false;

    // Fields of the current terms to be displayed as read-only.
    String currentValidToDate   = null;
    String applicantName        = "";

    // Dates are stored in the database as one field but displayed on the
    // page as 3.  These variables hold the pieces for each date.
    String validFromDay         = null;
    String validFromMonth       = null;
    String validFromYear        = null;
    String validToDay           = null;
    String validToMonth         = null;
    String validToYear          = null;
    String links				= "";
    
    //PR CMA Issue No 5 # IR T36000010961
    StringBuffer         availableAmount          = new StringBuffer();

    // Variables used for populating ref data dropdowns.
    String defaultText = resMgr.getText("transaction.SelectPhrase", TradePortalConstants.TEXT_BUNDLE);
    String options;

    String bButtonPressed = "";
    // These are the beans used on the page.
 String currencies = "";
 Vector vVector = new Vector();
	int CHRG_INC_DEFAULT_ROW_COUNT = 3;
	List chrgIncList = new ArrayList(CHRG_INC_DEFAULT_ROW_COUNT);
	AdditionalReqDocWebBean chrgInc=null;
	int chrgIncRowCount=0;
	if(request.getParameter("numberOfMultipleObjects")!=null)
	  chrgIncRowCount = Integer.parseInt(request.getParameter("numberOfMultipleObjects"));
	else
	  chrgIncRowCount = CHRG_INC_DEFAULT_ROW_COUNT;
   
    TransactionWebBean transaction  = (TransactionWebBean)
                                       beanMgr.getBean("Transaction");
    InstrumentWebBean instrument    = (InstrumentWebBean)
                                       beanMgr.getBean("Instrument");
    TemplateWebBean template        = (TemplateWebBean)
                                       beanMgr.getBean("Template");
    TermsWebBean terms              = null;
    
    FeeWebBean feeWebBean = null;

    // We need the amount of the active transaction to display.  Create a webbean
    // to hold this data.
    TransactionWebBean activeTransaction = beanMgr.createBean(TransactionWebBean.class, "Transaction");


    // Get the document from the cache.  We may use it to repopulate the
    // screen if returning from another page, a save, validation, or any other
    // mediator called from this page.  Otherwise, we assume an instrument oid
    // and transaction oid was passed in.

    doc = formMgr.getFromDocCache();
    bButtonPressed = doc.getAttribute("/In/Update/ButtonPressed");
    
    Vector error = null;
    error = doc.getFragments("/Error/errorlist/error");
     
    Debug.debug("doc from cache follows below: \n" + doc.toString());

    /******************************************************************************
    We are either entering the page or returning from somewhere.  These are the
    conditions for how to populate the web beans.  Data comes from either the
    database or the doc cache (/In section) with some variation.

    Mode           Condition                      Populate Beans From
    -------------  -----------------------------  --------------------------------
    Enter Page     /In/Transaction doesn't exist  Instrument and Template web
                                                  beans already populated, get
                                                  data for Terms and TermsParty
                                                  web beans from database

    return from    /In/NewPartyFromSearchInfo/PartyOid   doc cache (/In); also use Party
    Party Search     exists                       SearchInfo to lookup, and
                                                  populate a specific TermsParty
                                                  web bean

    return from    /Out/PhraseLookupInfo exists   doc cache (/In); but use Phrase
    Phrase Lookup                                 LookupInfo text (from /Out) to
                                                  replace a specific phrase text
                                                  in the /In document before
                                                  populating

    return from    /Error/maxerrorseverity < 4    Same as Enter Page (data is
    Transaction                                   retrieved from database)
    mediator
    no error)

    return from    /Error/maxerrorseverity > 3    doc cache (/In)
    Transaction
    mediator
    (error)
    ******************************************************************************/

    // Assume we get the data from the doc.
    getDataFromDoc = true;

  //if newTransaction, set session variable so it persists
    String newTransaction = null;
    if (doc.getDocumentNode("/Out/newTransaction")!=null) {
      newTransaction = doc.getAttribute("/Out/newTransaction");
      session.setAttribute("newTransaction", newTransaction);
      System.out.println("found newTransaction = "+ newTransaction);
    } 
    else {
      newTransaction = (String) session.getAttribute("newTransaction");
      if ( newTransaction==null ) {
        newTransaction = TradePortalConstants.INDICATOR_NO;
      }
      System.out.println("used newTransaction from session = " + newTransaction);
    }

    String maxError = doc.getAttribute("/Error/maxerrorseverity");

    if (maxError != null && maxError.compareTo(String.valueOf(ErrorManager.WARNING_ERROR_SEVERITY)) <= 0)
        getDataFromDoc = false; // No errors, so don't get the data from doc.
    //ir cnuk113043991 - check to see if transaction needs to be refreshed
    // if so, refresh it and do not get data from doc as it is wrong
    if ( "Y".equals(doc.getAttribute("/Out/Refresh/Transaction")) ) {
       transaction.getDataFromAppServer();
       getDataFromDoc = false;
    }
    if (doc.getDocumentNode("/In/NewPartyFromSearchInfo/Type") != null) {
       // We have returned from the party search page.  Get data from doc cache
       getDataFromDoc = true;
    }

    if (doc.getDocumentNode("/In/Transaction") == null)
    {
        // No /In/Transaction means we've never looked up the data.
        Debug.debug("No /In/Transaction section - get data from database");
        getDataFromDoc = false;
    }
    if (doc.getDocumentNode("/Out/PhraseLookupInfo") != null)
    {
        // A Looked up phrase exists.  Replace it in the /In document.
        Debug.debug("Found a looked-up phrase");
        getDataFromDoc = true;
        phraseSelected = true;

        String result = doc.getAttribute("/Out/PhraseLookupInfo/Result");
        if (result.equals(TradePortalConstants.INDICATOR_YES))
        {
            // Take the looked up and appended phrase text from the /Out section
            // and copy it to the /In section.  This allows the beans to be
            // properly populated.
            String xmlPath = doc.getAttribute("/In/PhraseLookupInfo/text_path");
            xmlPath = "/In" + xmlPath;

            doc.setAttribute(xmlPath, doc.getAttribute("/Out/PhraseLookupInfo/NewText"));

            // If we returned from the phrase lookup without errors, set the focus
            // to the correct field.  Otherwise, don't set the focus so the user can
            // see the error.
            if (maxError != null && maxError.compareTo(String.valueOf(ErrorManager.WARNING_ERROR_SEVERITY)) < 0)
                focusField = doc.getAttribute("/In/PhraseLookupInfo/text_field_name");
        }
    }

    if (getDataFromDoc) {
       Debug.debug("Populating beans from doc cache");

       // Populate the beans from the input doc.
       try {
          instrument.populateFromXmlDoc(doc, "/In");
          transaction.populateFromXmlDoc(doc, "/In");

          terms = (TermsWebBean) beanMgr.getBean("Terms");
          terms.populateFromXmlDoc(doc, "/In");

          validFromDay = doc.getAttribute("/In/Terms/guar_valid_from_date_day");
          validFromMonth = doc.getAttribute("/In/Terms/guar_valid_from_date_month");
          validFromYear = doc.getAttribute("/In/Terms/guar_valid_from_date_year");

          validToDay = doc.getAttribute("/In/Terms/expiry_date_day");
          validToMonth = doc.getAttribute("/In/Terms/expiry_date_month");
          validToYear = doc.getAttribute("/In/Terms/expiry_date_year");

          String activeTransOid = instrument.getAttribute("active_transaction_oid");
          if (!InstrumentServices.isBlank(activeTransOid)) {
             activeTransaction.setAttribute("transaction_oid", activeTransOid);
             activeTransaction.getDataFromAppServer();
          }
          vVector = doc.getFragments("/In/Transaction/FeeList");
	 		for (int iLoop=0; iLoop<vVector.size(); iLoop++)
			   {
	 			feeWebBean= beanMgr.createBean(FeeWebBean.class,"Fee");
			    DocumentHandler feeDoc = (DocumentHandler) vVector.elementAt(iLoop);
			    feeWebBean.populateFromXmlDoc(feeDoc,"/");
			    chrgIncList.add(feeWebBean);
			   } 		
	 		
			if(vVector.size() < CHRG_INC_DEFAULT_ROW_COUNT) {
			  for(int iLoop = vVector.size(); iLoop < CHRG_INC_DEFAULT_ROW_COUNT; iLoop++) {
				  chrgIncList.add(beanMgr.createBean(FeeWebBean.class,"Fee"));
				  }
			}
          

       } catch (Exception e) {
          out.println("Contact Administrator: " +
                      "Unable to reload data after returning to page. " +
                      "Error is " + e.toString());
       }
    } else {
       Debug.debug("populating beans from database");
       // We will perform a retrieval from the database.
       // Instrument and Transaction were already retrieved.  Get
       // the rest of the data

       terms = transaction.registerCustomerEnteredTerms();

       String activeTransOid = instrument.getAttribute("active_transaction_oid");
       if (!InstrumentServices.isBlank(activeTransOid)) {
             activeTransaction.setAttribute("transaction_oid", activeTransOid);
             activeTransaction.getDataFromAppServer();
        }

       // Because date fields are represented as three fields on the page,
       // we need to parse out the date values into three fields.  Pre-mediator
       // logic puts these three fields back together for the terms bean.
       String date = terms.getAttribute("guar_valid_from_date");
       validFromDay = TPDateTimeUtility.parseDayFromDate(date);
       validFromMonth = TPDateTimeUtility.parseMonthFromDate(date);
       validFromYear = TPDateTimeUtility.parseYearFromDate(date);

       date = terms.getAttribute("expiry_date");
       validToDay = TPDateTimeUtility.parseDayFromDate(date);
       validToMonth = TPDateTimeUtility.parseMonthFromDate(date);
       validToYear = TPDateTimeUtility.parseYearFromDate(date);
     	 vVector=null;
        	QueryListView  queryListView = null;
     	try{
     			StringBuffer sql = new StringBuffer();
     			queryListView = (QueryListView) EJBObjectFactory.createClientEJB(formMgr.getServerLocation(), "QueryListView");
                sql.append("select fee_oid, SETTLEMENT_CURR_CODE, SETTLEMENT_CURR_AMOUNT, CHARGE_TYPE, P_TRANSACTION_OID  ");
                sql.append(" from FEE ");
                sql.append(" where P_TRANSACTION_OID = ?");
                sql.append(" order by ");
                sql.append(resMgr.localizeOrderBy("fee_oid"));
                queryListView.setSQL(sql.toString(), new Object[]{transaction.getAttribute("transaction_oid")});
                queryListView.getRecords();

                DocumentHandler feeOidList = new DocumentHandler();
                feeOidList = queryListView.getXmlResultSet();
              	vVector = feeOidList.getFragments("/ResultSetRecord");
     			} catch (Exception e) {
     	            e.printStackTrace();
     	      } finally {
     	            try {
     	                  if (queryListView != null) {
     	                        queryListView.remove();
     	                  }
     	            } catch (Exception e) {
     	                  System.out.println("Error removing querylistview in Transaction-GUAR-ISS.jsp!");
     	            }
     	      }
            for (int iLoop=0; iLoop<vVector.size(); iLoop++)
     		   {
            	feeWebBean= beanMgr.createBean(FeeWebBean.class,"Fee");
            	DocumentHandler addReqDocDoc = (DocumentHandler) vVector.elementAt(iLoop);
            	feeWebBean.setAttribute("fee_oid", addReqDocDoc.getAttribute("/FEE_OID"));
            	feeWebBean.setAttribute("settlement_curr_code", addReqDocDoc.getAttribute("/SETTLEMENT_CURR_CODE"));
            	feeWebBean.setAttribute("settlement_curr_amount", addReqDocDoc.getAttribute("/SETTLEMENT_CURR_AMOUNT"));
            	feeWebBean.setAttribute("charge_type", addReqDocDoc.getAttribute("/CHARGE_TYPE"));
            	feeWebBean.setAttribute("p_transaction_oid", addReqDocDoc.getAttribute("/P_TRANSACTION_OID"));
            	chrgIncList.add(feeWebBean);
     		   } 		                                                                                                                                                                                            
     		    
     		if(vVector.size() < CHRG_INC_DEFAULT_ROW_COUNT) {
     		  for(int iLoop = vVector.size(); iLoop < CHRG_INC_DEFAULT_ROW_COUNT; iLoop++) {
     			  chrgIncList.add(beanMgr.createBean(FeeWebBean.class,"Fee"));
     			  }
     		}

    }


    // Now determine the mode for how the page operates (readonly, etc.)
    //BigInteger requestedSecurityRight = SecurityAccess.GUAR_CREATE_MODIFY;
	BigInteger requestedSecurityRight = null;
	String convertedTran = transaction!=null? transaction.getAttribute("converted_transaction_ind"):"";
	Debug.debug("convertedTran:"+convertedTran);
	if(TradePortalConstants.INDICATOR_YES.equals(convertedTran)){
		requestedSecurityRight = SecurityAccess.CC_GUA_CREATE_MODIFY;
	}else{
		requestedSecurityRight = SecurityAccess.GUAR_CREATE_MODIFY;
	}

%>
    <%@ include file="fragments/Transaction-PageMode.frag" %>
<%

  // Now we need to calculate the computed new lc amount total.  This involves 
  // using the active transaction amount, the amount entered by the user,
  // performing some validation and then doing the calculation.

  MediatorServices medService = new MediatorServices();
  medService.getErrorManager().setLocaleName(resMgr.getResourceLocale());
  BigDecimal originalAmount = BigDecimal.ZERO;
  BigDecimal offsetAmount = BigDecimal.ZERO;
  BigDecimal calculatedTotal = BigDecimal.ZERO;
  String amountValue = null;
  String displayAmount;
  String incrDecrValue = TradePortalConstants.INCREASE;
  String currencyCode = activeTransaction.getAttribute("copy_of_currency_code");
  boolean badAmountFound = false;

  // Convert the active transaction amount to a double
  try {
	 if (TransactionType.PAYMENT.equals(activeTransaction.getAttribute("transaction_type_code"))){
		 amountValue = activeTransaction.getAttribute("available_amount");
	 }else{
     	amountValue = activeTransaction.getAttribute("instrument_amount");
	 }
     originalAmount = new BigDecimal(amountValue);;
  } catch (Exception e) {
     originalAmount = BigDecimal.ZERO;
  }

//PR CMA Issue No 5 # IR T36000010961
  if((currencyCode != null)
	      && (currencyCode.trim().length() > 0)
	      && (activeTransaction.getAttribute("available_amount") != null)
	      && (activeTransaction.getAttribute("available_amount").trim().length() > 0))
	   {
	      availableAmount.append(currencyCode);
	      availableAmount.append(" ");
	      availableAmount.append(TPCurrencyUtility.getDisplayAmount(activeTransaction.getAttribute("available_amount"),
	    		  currencyCode, loginLocale));
	   }
  // DK IR T36000027615 Rel9.1 09/12/2014
  if (doc.getAttribute("/In/Terms/amount") != null) {
     // Convert the amount for this terms record to a double.  If we're getting
     // data from the doc, use the value in the document and attempt to convert
     // to a double.  It may fail due to invalid number.  If so, we'll post 
     // an error and set a flag to prevent the calculation from being 
     // displayed.
     //
     // Furthermore, since the displayed terms amount is always displayed as a 
     // positive number (the Increase/Decrease dropdown represents the sign).
     // Therefore, we may need to set the double amount negative.

     amountValue = doc.getAttribute("/In/Terms/amount");
     incrDecrValue = doc.getAttribute("/In/Terms/IncreaseDecreaseFlag");
     Debug.debug("amountvalue " + amountValue);

     try {
        amountValue = NumberValidator.getNonInternationalizedValue(amountValue, 
                                                          loginLocale, false);
        try {
           offsetAmount = new BigDecimal(amountValue);;
        } catch (Exception e) {
           offsetAmount = BigDecimal.ZERO;
        }
        if (incrDecrValue.equals(TradePortalConstants.DECREASE)) {
           offsetAmount = offsetAmount.negate();
        }

        // Don't format the number because we only came from the doc
        displayAmount = doc.getAttribute("/In/Terms/amount");

     } catch (Exception e) {
        // Unable to unformat number so the display value is the save value
        // the user typed in.
        displayAmount = amountValue;
        badAmountFound = true;
        medService.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                                  TradePortalConstants.INVALID_CURRENCY_FORMAT,
                                  amountValue);
        medService.addErrorInfo();
        doc.addComponent("/Error", medService.getErrorDoc());
        formMgr.storeInDocCache("default.doc", doc);
     }
  } else {
     // We're getting the amount from the database so convert to a positive
     // number if necessary and set the Increase/Decrease dropdown accordingly.
     try {
        amountValue = terms.getAttribute("amount");
        offsetAmount = new BigDecimal(amountValue);
     } catch (Exception e) {
        offsetAmount = BigDecimal.ZERO;
     }
     if (offsetAmount.doubleValue() < 0) incrDecrValue = TradePortalConstants.DECREASE;
     if( InstrumentServices.isBlank( amountValue )  && !isReadOnly) {
   	    displayAmount = "";
     }
     else{
       displayAmount = TPCurrencyUtility.getDisplayAmount(
                            offsetAmount.abs().toString(), currencyCode, loginLocale);
     }
	 //Srinivasu_D IR#T36000035537 ER9.1.0.2 01/01/2015 - handling minus
	 if(StringFunction.isNotBlank(amountValue)){
	 char amtChar[] = amountValue.toCharArray();
		if(amtChar[0] == '-'){
			amountValue = amountValue.substring(1,amountValue.length());
		}
	 }
	 //Srinivasu_D IR#T36000035537 ER9.1.0.2 01/01/2015 - End
  }

  Debug.debug("display amoutn "+displayAmount);
  calculatedTotal = originalAmount.add(offsetAmount);


    transactionType = transaction.getAttribute("transaction_type_code");
    transactionStatus = transaction.getAttribute("transaction_status");

    // Get the transaction rejection indicator to determine whether to show the rejection reason text
    rejectionIndicator = transaction.getAttribute("rejection_indicator");
    rejectionReasonText = transaction.getAttribute("rejection_reason_text");

    instrumentType = instrument.getAttribute("instrument_type_code");
    instrumentStatus = instrument.getAttribute("instrument_status");

    Debug.debug("Instrument Type " + instrumentType);
    Debug.debug("Instrument Status " + instrumentStatus);
    Debug.debug("Transaction Type " + transactionType);
    Debug.debug("Transaction Status " + transactionStatus);


    // Create documents for the phrase dropdowns. First check the cache (no need
    // to recreate if they already exist).  After creating, place in the cache.
    // (InstrumentCloseNavigator.jsp cleans these up.)

    DocumentHandler phraseLists = formMgr.getFromDocCache(TradePortalConstants.PHRASE_LIST_DOC);
    if (phraseLists == null) phraseLists = new DocumentHandler();

    // This list of phrases is used in the General Section
    DocumentHandler customerTextList = phraseLists.getFragment("/CustomerText");
    if (customerTextList == null)
    {
        customerTextList = PhraseUtility.createPhraseList(TradePortalConstants.PHRASE_CAT_GUAR_CUST,
                                                      userSession, formMgr, resMgr);
        phraseLists.addComponent("/CustomerText", customerTextList);
    }

    //this list of phrases is used in the bank instructions section
    DocumentHandler specialInstructionsList = phraseLists.getFragment("/SpecialInstructions");
    if (specialInstructionsList == null)
    {
        specialInstructionsList = PhraseUtility.createPhraseList(TradePortalConstants.PHRASE_CAT_SPCL_INST,
                                                                 userSession, formMgr, resMgr);
        phraseLists.addComponent("/SpecialInstructions", specialInstructionsList);
    }

  formMgr.storeInDocCache(TradePortalConstants.PHRASE_LIST_DOC, phraseLists);

  ////////////////////////////////////////////////////////////////////////
  // Retrieve the latest information of applicatnt name and expiry date.  
  // They are displayed in general section.
  ////////////////////////////////////////////////////////////////////////

  // Retrieve the latest information of expiry date from instrument.
  currentValidToDate = instrument.getAttribute("copy_of_expiry_date");
  // Retrieve the applicant name from the bank release terms of the latest issuance or amendment transaction.
  //    (The latest transaction is the one that has the latest transaction status date and in case several transaction
  //     have the same transaction status date, the largest transaction_oid.)
  String instrument_oid = instrument.getAttribute("instrument_oid");
  StringBuffer issueTransactionSQL = new StringBuffer();
  issueTransactionSQL.append("select p.name")
                     .append(" from terms_party p, terms t, transaction tr")
                     .append(" where tr.transaction_oid =")
                     .append("       (select max(transaction_oid)")
                     .append("          from transaction")
                     .append("         where transaction_status_date = (select max(transaction_status_date)")
                     .append("                                            from transaction")
                     .append("                                           where p_instrument_oid = ?")
                     .append("                                             and (transaction_type_code = ? or transaction_type_code = ? or transaction_type_code = ?)")
                     .append("                                             and transaction_status = ?)")
                     .append("           and p_instrument_oid = ?")
                     .append("           and (transaction_type_code = ? or transaction_type_code = ? or transaction_type_code = ?)")
                     .append("           and transaction_status = ?)")
                     .append(" and tr.c_bank_release_terms_oid = t.terms_oid")
                     .append(" and (p.terms_party_oid = t.c_first_terms_party_oid")
                     .append("      or p.terms_party_oid = t.c_second_terms_party_oid")
                     .append("      or p.terms_party_oid = t.c_third_terms_party_oid")
                     .append("      or p.terms_party_oid = t.c_fourth_terms_party_oid")
                     .append("      or p.terms_party_oid = t.c_fifth_terms_party_oid)")
                     .append(" and p.terms_party_type = ?");
  //jgadela  R90 IR T36000026319 - SQL FIX
  Object[] sqlParamsGuarA = new Object[11];
  sqlParamsGuarA[0] =  instrument_oid;
  sqlParamsGuarA[1] =  TransactionType.ISSUE;
  sqlParamsGuarA[2] =  TransactionType.AMEND;
  sqlParamsGuarA[3] =  TransactionType.CHANGE;
  sqlParamsGuarA[4] =  TradePortalConstants.TRANS_STATUS_PROCESSED_BY_BANK;
  sqlParamsGuarA[5] =  instrument_oid;
  sqlParamsGuarA[6] =  TransactionType.ISSUE;
  sqlParamsGuarA[7] =  TransactionType.AMEND;
  sqlParamsGuarA[8] =  TransactionType.CHANGE;
  sqlParamsGuarA[9] =  TradePortalConstants.TRANS_STATUS_PROCESSED_BY_BANK;
  sqlParamsGuarA[10] =  TradePortalConstants.APPLICANT;
  
  DocumentHandler issueTransactionResult = DatabaseQueryBean.getXmlResultSet(issueTransactionSQL.toString(), false, sqlParamsGuarA);
  if (issueTransactionResult!=null) {
     applicantName = issueTransactionResult.getAttribute("/ResultSetRecord(0)/NAME");
  }

%>


<%-- ********************* HTML for page begins here ********************* --%>


<%
  // Some of the retrieval logic above may have set a focus field.  Otherwise,
  // we'll use the initial value for focus.
  String onLoad = "";

  // Auto save the form when time-out if not readonly.  
  // (Header.jsp will check for auto-save setting of the corporate org).
  String autoSaveFlag = isReadOnly ? TradePortalConstants.INDICATOR_NO : TradePortalConstants.INDICATOR_YES;
  String templateFlag = isTemplate ? TradePortalConstants.INDICATOR_YES : TradePortalConstants.INDICATOR_NO;
  

%>
<%

    if(transaction.getAttribute("transaction_status").equals(TradePortalConstants.TRANS_STATUS_PROCESSED_BY_BANK))
	  isProcessedByBank = true;

    // Store values such as the userid, security rights, and org in a
    // secure hashtable for the form.  Also store instrument and transaction
    // data that must be secured.
    Hashtable secureParms = new Hashtable();
    secureParms.put("login_oid", userSession.getUserOid());
    secureParms.put("owner_org_oid", userSession.getOwnerOrgOid());
    secureParms.put("login_rights", loginRights);

    secureParms.put("instrument_oid", instrument.getAttribute("instrument_oid"));
    secureParms.put("instrument_type_code", instrumentType);
    secureParms.put("instrument_status", instrumentStatus);

    secureParms.put("transaction_oid", transaction.getAttribute("transaction_oid"));
    secureParms.put("transaction_type_code", transactionType);
    secureParms.put("transaction_status", transactionStatus);

    secureParms.put("transaction_instrument_info", 
  	transaction.getAttribute("transaction_oid") + "/" + 
	instrument.getAttribute("instrument_oid") + "/" + 
	transactionType);

    // If the terms record doesn't exist, set its oid to 0.
    String termsOid = terms.getAttribute("terms_oid");
    if (termsOid == null)
        termsOid = "0";
    secureParms.put("terms_oid", termsOid);
    secureParms.put("TransactionCurrency", currencyCode);
    //IR - PAUK042248441 [BEGIN]
    //secureParms.put("ApplRefNum", terms.getAttribute("reference_number"));
    secureParms.put("ApplRefNum", StringFunction.xssHtmlToChars(terms.getAttribute("reference_number"))); 
    //IR - PAUK042248441 [BEGIN]

%>
<%
  String pageTitleKey;
  if ( TradePortalConstants.INDICATOR_YES.equals(newTransaction) && !(TransactionType.AMEND).equals(transaction.getAttribute("transaction_type_code"))) {
    pageTitleKey = "SecondaryNavigation.NewInstruments";
    userSession.setCurrentPrimaryNavigation("NavigationBar.NewInstruments");
  } else {
    pageTitleKey = "SecondaryNavigation.Instruments";
    userSession.setCurrentPrimaryNavigation("NavigationBar.Transactions");
  }
  String item1Key = resMgr.getText("SecondaryNavigation.Instruments.OutgoingGuarantee",TradePortalConstants.TEXT_BUNDLE) + ": "+resMgr.getText("common.amendButton",TradePortalConstants.TEXT_BUNDLE);
  String helpUrl;
  if(transaction.getAttribute("standby_using_guarantee_form").
			equals(TradePortalConstants.INDICATOR_NO) )
    helpUrl = "customer/amend_guarantee.htm";
  else
    helpUrl = "customer/amend_detailed_standby_lc.htm";

  //cquinton 11/19/2012 pass info for recent instruments into header
  String newRecentTransactionOid = transaction.getAttribute("transaction_oid");
  String newRecentInstrumentId = instrument.getAttribute("complete_instrument_id");
  String newRecentTransactionType = transaction.getAttribute("transaction_type_code");
  String newRecentTransactionStatus = transaction.getAttribute("transaction_status");
%>

<jsp:include page="/common/Header.jsp">
  <jsp:param name="autoSaveFlag"           value="<%=autoSaveFlag%>" />
  <jsp:param name="templateFlag"           value="<%=templateFlag%>" />
  <jsp:param name="newRecentTransactionOid" value="<%=newRecentTransactionOid%>" />
  <jsp:param name="newRecentInstrumentId"  value="<%=newRecentInstrumentId%>" />
  <jsp:param name="newRecentTransactionType" value="<%=newRecentTransactionType%>" />
  <jsp:param name="newRecentTransactionStatus" value="<%=newRecentTransactionStatus%>" />
</jsp:include>

<div class="pageMain">
<div class="pageContent">

<jsp:include page="/common/PageHeader.jsp">
   <jsp:param name="titleKey" value="<%=pageTitleKey%>" />
   <jsp:param name="item1Key" value="<%=item1Key %>" />
   <jsp:param name="helpUrl"  value="<%=helpUrl%>" />
</jsp:include>

<jsp:include page="/common/TransactionSubHeader.jsp"/>



<% //cr498 begin
   //Include ReAuthentication frag in case re-authentication is required for
   //authorization of transactions for this client
   String certAuthURL = "";
   Cache reCertCache = (Cache)TPCacheManager.getInstance().getCache(TradePortalConstants.CLIENT_BANK_CACHE);
   DocumentHandler CBCResult = (DocumentHandler)reCertCache.get(userSession.getClientBankOid());	
   String requireTranAuth = CBCResult.getAttribute("/ResultSetRecord(0)/REQUIRE_TRAN_AUTH");
   boolean requireAuth = InstrumentAuthentication.requireTransactionAuthentication(
       requireTranAuth,InstrumentAuthentication.TRAN_AUTH__GUAR_AMD);
   if (requireAuth) {
%>   
      <%@ include file="/logon/fragments/openReauthenticationWindow.frag" %>
<%
   }
   //cr498 end
%>

<form name="Transaction-GUAR-Form" id="Transaction-GUAR-Form" method="post" data-dojo-type="dijit.form.Form" action="<%=formMgr.getSubmitAction(response)%>">
   <input type=hidden value="" name=buttonName>

<% //cr498 begin
  if (requireAuth) {
%> 

  <input type=hidden name="reCertification" value="<%=TradePortalConstants.INDICATOR_NO%>">
  <input type=hidden name="reCertOK">
  <input type=hidden name="logonResponse">
  <input type=hidden name="logonCertificate">

<%
  } //cr498 end
%> 


<%-- error section goes above form content --%>
<div class="formArea">
<jsp:include page="/common/ErrorSection.jsp" />
<%@ include file="fragments/Instruments-AttachDocuments.frag" %>

<div class="formContent">
	<%	if (rejectionIndicator.equals(TradePortalConstants.INDICATOR_YES)
    || rejectionIndicator.equals(TradePortalConstants.INDICATOR_X))
	  {
	%>
		<%=widgetFactory.createSectionHeader("0", "Rejection Reason") %>
	     <%@ include file="fragments/Transaction-RejectionReason.frag" %>
	     </div>
	<%
	  }
	%>
	
	<% //CR 821 Added Repair Reason Section 
	  StringBuffer repairReasonWhereClause = new StringBuffer();
	  int  repairReasonCount = 0;
		
		/*Get all repair reason's count from transaction history table*/
		repairReasonWhereClause.append("p_transaction_oid = ?");
		repairReasonWhereClause.append(" and REPAIR_REASON is not null order by ACTION_DATETIME");
		//jgadela  R90 IR T36000026319 - SQL FIX
		Object[] sqlParamsReaCo = new Object[1];
		sqlParamsReaCo[0] =  transaction.getAttribute("transaction_oid");
		Debug.debug("repairReasonWhereCondition query : " + repairReasonWhereClause);
		repairReasonCount = DatabaseQueryBean.getCount("A_USER_OID", "transaction_history", repairReasonWhereClause.toString(), true, sqlParamsReaCo);
				
	if ( !(TradePortalConstants.TRANS_STATUS_PROCESSED_BY_BANK.equalsIgnoreCase(transactionStatus)) && repairReasonCount>0  ){%>
		<%=widgetFactory.createSectionHeader("", "TransactionHistory.RepairReason") %>
			<%@ include file="fragments/Transaction-RepairReason.frag" %>
		</div>
	<%} %> 
	
  <%@ include file="fragments/Transaction-GUAR-AMD-html.frag" %>
  
 <% if (TradePortalConstants.TRANS_STATUS_PROCESSED_BY_BANK.equalsIgnoreCase(transactionStatus) && repairReasonCount>0  ){%>
		<%=widgetFactory.createSectionHeader("4", "TransactionHistory.RepairReason", null, true) %>
		<%@ include file="fragments/Transaction-RepairReason.frag" %>
		</div>
<%} %> 
  
  
  
</div><%--formContent--%>
</div><%--formArea--%>

<div class="formSidebar" data-dojo-type="widget.FormSidebar"
                                    data-dojo-props="form: 'Transaction-GUAR-Form'">
    <jsp:include page="/common/Sidebar.jsp">
		<jsp:param name="links" value="<%=links%>" />
		<jsp:param name="isReadOnly" value="<%=isReadOnly%>" />
		<jsp:param name="certAuthURL" value="<%=certAuthURL%>" />
		<jsp:param name="buttonPressed" value="<%=bButtonPressed%>" />
		<jsp:param name="error" value="<%= error%>" />
		<jsp:param name="formName" value="0" />
		<jsp:param name="isTemplate" value="<%=isTemplate%>"/>
		 <jsp:param name="showLinks" value="true" />  
		 <jsp:param name="isNewTransaction" value="<%=newTransaction%>"/>
	</jsp:include>
    	  
</div>
<%@ include file="fragments/PhraseLookupPrep.frag" %>

<%= formMgr.getFormInstanceAsInputField("Transaction-GUAR-Form", secureParms) %>
<input type=hidden name=amount_currency_code value=<%=currencyCode%>>
</form>
</div>
</div>

<%-- KMehta on 12 May 2014 @ Rel 90 for IR T36000025449 Start --%>
<jsp:include page="/common/Footer.jsp">
   <jsp:param name="autoSaveFlag"           value="<%=autoSaveFlag%>" />
   <jsp:param name="templateFlag"           value="<%=templateFlag%>" />
   <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>
<%-- KMehta on 12 May 2014 @ Rel 90 for IR T36000025449 End --%>
<jsp:include page="/common/SidebarFooter.jsp" />


<script>

  <%-- cquinton 3/3/2013 add local var --%>
  var local = {};
  function resetFeeOid(row){
	  require(["dijit/registry", "dojo/ready"],
			    function(registry, ready ) {
			      ready(function() {

	var chargeType = registry.byId(('ChrgIncChargeType'+(row))).get('value');
	var currCode = registry.byId(('ChrgIncCurrencyCode'+(row))).get('value');
	var amt = registry.byId(('ChrgIncAmount'+(row))).get('value');
	if(chargeType == "" & currCode =="" & amt == ""){
		registry.byId(('FeeOid'+(row))).set('value',"");
	}
			      });
	  });
	 
}

require(["dojo/_base/array", "dojo/dom",
         "dojo/dom-construct", "dojo/dom-class", 
         "dojo/dom-style", "dojo/query",
         "dijit/registry", "dojo/on", 
         "t360/common",
         "dojo/ready", "dojo/NodeList-traverse"], function(arrayUtil, dom,
                 domConstruct, domClass,
                 domStyle, query,
                 registry, on, 
                 common, 
                 ready){
		ready(function(){
			var add3moreChargesIncurred = function(cnt){
				var myTable = dojo.byId('chrgIncTable');
				<%-- Leelavathi IR#T36000011878 Rel-8.2 18/2/2013 Begin --%>
				var rowIndex = myTable.rows.length - 1;
				common.appendAjaxTableRows(myTable,"chrgIncIndex",
			    		  "/portal/transactions/fragments/Add3moreChrgIncRows.jsp?chrgIncIndex="+rowIndex+"&count="+cnt+"&isTemplate=<%=isTemplate%>&isExpressTemplate=<%=isExpressTemplate%>"); <%-- table length includes header, so subtract 1 --%>
			    		  
			      document.getElementById("noOfChrgIncRows").value = eval(rowIndex+cnt);   		  
			};
			
			<% if (!(isReadOnly)) 
			{     %>
	on(registry.byId("add3moreChargesIncurred"),"click",function(){ 
		add3moreChargesIncurred(3) } );


		var deleteChrgIncRows = function (rowId){

		}
	    query('#chrgIncTable').on(".deleteSelectedItem:click", function(evt) {
		      thisPage.deleteEventHandler(this, document.getElementById("chrgIncTable"));

		    });
		<%}%>	    
      var thisPage = {
     		 deleteEventHandler: function(deleteButton, tableID) {
		        <%-- uncheck the available item --%>
		        <%-- this automatically does the onchange mechanism there --%>
		        <%-- including removing this item and decreasing the total count --%>
		       <%--  var deleteSelectItem = deleteButton.parentNode; --%>
		       
		        var deleteSelectItemId = deleteButton.id;
		       var rowIndex = deleteSelectItemId.substring(13,deleteSelectItemId.length);
		        var row = document.getElementById('chrgIncIndex' + rowIndex);
		        
		        deleteOrderField(row, tableID);	
		      }

      }

	  function deleteOrderField(deleteRow, table){
		  if(deleteRow){
		var tabSize = (table.rows.length)- (2); <%--  two row for header --%>
		var replaceRow = deleteRow.rowIndex;
		<%-- for(var row= replaceRow; row <= tabSize; row++){ --%>
		<%-- 	table.rows[ row ].id = table.rows[ row + 1 ].id; --%>
		<%-- 	table.rows[ row ].cells[0].firstElementChild.value = table.rows[ row+1 ].cells[0].firstElementChild.value; --%>
		<%-- 	table.rows[ row ].cells[1].innerHtml = table.rows[ row+1 ].cells[1].innerHtml; --%>
		<%-- 	table.rows[ row ].cells[1].firstElementChild.innerText = table.rows[ row+1 ].cells[1].firstElementChild.innerText; --%>
		<%-- 	table.rows[ row ].cells[1].childNodes[3].id = table.rows[ row+1 ].cells[1].childNodes[3].id; --%>
		<%-- } --%>
		for(var row= replaceRow; row <= tabSize; row++){
 			document.getElementById(('ChrgIncChargeType'+(row-1))).value=document.getElementById(('ChrgIncChargeType'+(row))).value;
 			document.getElementById(('ChrgIncCurrencyCode'+(row-1))).value=document.getElementById(('ChrgIncCurrencyCode'+(row))).value;
 			document.getElementById(('ChrgIncAmount'+(row-1))).value=document.getElementById(('ChrgIncAmount'+(row))).value;
 			document.getElementById(('FeeOid'+(row-1))).value=document.getElementById(('FeeOid'+(row))).value;
   			registry.byId(('ChrgIncChargeType'+(row-1))).set('value',registry.byId(('ChrgIncChargeType'+(row))).get('value'));
   			registry.byId(('ChrgIncCurrencyCode'+(row-1))).set('value',registry.byId(('ChrgIncCurrencyCode'+(row))).get('value'));
   			registry.byId(('ChrgIncAmount'+(row-1))).set('value',registry.byId(('ChrgIncAmount'+(row))).get('value'));
   			registry.byId(('FeeOid'+(row-1))).set('value',registry.byId(('FeeOid'+(row))).get('value'));

		}
			table.deleteRow(tabSize+1);
  	 		var idNumberToDestroy = (table.rows.length) - (1);
  	  		
  	  		
  	  		var chargeType = dijit.byId('ChrgIncChargeType' + idNumberToDestroy);
  	  		var currCode = dijit.byId('ChrgIncCurrencyCode' + idNumberToDestroy);
  	  		var amt = dijit.byId('ChrgIncAmount' + idNumberToDestroy);	
  	  		var feeOid = dijit.byId('FeeOid' + idNumberToDestroy);
  	  		if (feeOid){
  	  			dijit.byId('FeeOid' + idNumberToDestroy).destroyRecursive();
  	  		}else{
  	  			document.getElementById(('FeeOid'+(table.rows.length - 1))).value="";
  	  		}
  	  		dijit.byId('ChrgIncChargeType' + idNumberToDestroy).destroyRecursive();
  	  		dijit.byId('ChrgIncCurrencyCode' + idNumberToDestroy).destroyRecursive();
  	  		dijit.byId('ChrgIncAmount' + idNumberToDestroy).destroyRecursive();
  			if (table.rows.length ==1){
  				add3moreChargesIncurred(3);
  			}
		
		
		
		  }
	  }

});
});	

<%
  if (!isReadOnly && InstrumentServices.isNotBlank(focusField)) {
%>
function setOtherDate() {
	dijit.getEnclosingWidget(document.forms[0].guar_valid_from_date_type[2]).set('checked',true);

}
function setValidityDate() {
	dijit.getEnclosingWidget(document.forms[0].guar_expiry_date_type[1]).set('checked',true);

}
  require(["dijit/registry", "dojo/ready"],
    function(registry, ready ) {
      ready(function() {
        var focusFieldId = '<%= focusField %>';
        if ( focusFieldId ) {
          var focusField = registry.byId(focusFieldId);
          focusField.focus();
        }
        <%-- registry.byId("TransactionAmount2").attr('disabled', 'true'); --%>
      });
  });
  
  var xhReq;
  function setNewAmount(){
	  require(["dijit/registry"],
			    function(registry ) {
		  			var existingLCAmount = <%=originalAmount%>;
	  				var ccyCode = '<%=currencyCode%>';
	  				var flag;
	  				if(registry.byId("Increase").checked){
	  					flag='Increase';
	  				}else{
	  					flag='Decrease';
	  				}
	  				var incDecValue = registry.byId("amount").value;
	  				var newLCAmount = 0;	  				
	  				
		  				if(flag == 'Increase'){
		  					newLCAmount = Number(existingLCAmount) + Number(incDecValue);
		  				}else if(flag == 'Decrease'){
		  					newLCAmount = Number(existingLCAmount) - Number(incDecValue);
		  				}		  				
		  				if(isNaN(newLCAmount)){
		  					newLCAmount = existingLCAmount;
		  				}
	  				if (!xhReq)
	  					xhReq = getXmlHttpRequest();
	  				var req = "Amount=" + newLCAmount + "&currencyCode="+ccyCode;
	  				xhReq.open("GET", '/portal/transactions/AmountFormatter.jsp?'+req, true);
	  				xhReq.onreadystatechange = updateAmount;
	  				xhReq.send(null);
	  			  });}

  function getXmlHttpRequest()
  {
    var xmlHttpObj;
    if (window.XMLHttpRequest) {
      xmlHttpObj = new XMLHttpRequest();
    }
    else {
      try {
        xmlHttpObj = new ActiveXObject("Msxml2.XMLHTTP");
      } catch (e) {
        try {
          xmlHttpObj = new ActiveXObject("Microsoft.XMLHTTP");
        } catch (e) {
          xmlHttpObj = false;
        }
      }
    }
    console.log(xmlHttpObj);
    return xmlHttpObj;
  }
  
	  			function updateAmount(){
	  				require(["dijit/registry","dojo/dom","dojo/domReady!"], function(registry,dom){
	  				 if (xhReq.readyState == 4 && xhReq.status == 200) {	 
	  					var ccyCode = '<%=currencyCode%>';
	  					 var formattedAmt = ccyCode + " " + xhReq.responseText;	  					
	  					dom.byId("TransactionAmount2").innerHTML = formattedAmt;
	  			    	<%-- registry.byId("TransactionAmount2").set('value',xhReq.responseText); --%>
	  			      } 
	  				});
	  			  }
 
  <%-- cquinton 3/2/2013 local phrase lookup --%>
  require(["t360/phraseLookup"],
          function( phraseLookup ) {
    local.phraseLookupAppend = function(phraseSelect, textArea, textAreaMaxLength ) {
      phraseLookup.appendPhrase( phraseSelect, textArea, textAreaMaxLength, "_errorSection" );
    };
  });

<%
  }
%>
</script>

</body>
</html>

<%
   // Finally, reset the cached document to eliminate carryover of
   // information to the next visit of this page.
   formMgr.storeInDocCache("default.doc", new DocumentHandler());
%>
