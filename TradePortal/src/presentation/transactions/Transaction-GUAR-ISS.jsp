
<%--
*******************************************************************************
                              Guarantee Issue Page

  Description: 
    This is the main driver for the Guarantee Issue page.  It handles data
  retrieval of terms and terms parties (or retrieval from the input document)
  and creates the html page to display all the data for an Import DLC.
*******************************************************************************
--%>

<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>

<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,java.util.*,
                 com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.html.*,java.math.*,
                 com.amsinc.ecsg.util.DateTimeUtility" %>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session"></jsp:useBean>
<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session"></jsp:useBean>
<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session"></jsp:useBean>
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>

<%-- ************** Data retrieval and page setup logic begins here ****************  --%>


<%
  Debug.debug("***************************** START GUARANTEE ISSUE PAGE ************************");

  WidgetFactory widgetFactory = new WidgetFactory(resMgr, userSession);	
  //cquinton 2/12/2013 add refdata manager for datepattern lookup
  ReferenceDataManager refData = ReferenceDataManager.getRefDataMgr();

  String links="";
  //links="GuaranteeIssue.Part1,GuaranteeIssue.Part2,GuaranteeIssue.Part3,GuaranteeIssue.Part4";
			
  String  onLoad              = "";
  String  focusField          = "beneficiary_name"; // Default focus field  
  boolean focusSet            = false;

  String instrumentOid        = null;
  String instrumentType       = null;
  String instrumentStatus     = null;
  String PartySearchAddressTitle =resMgr.getTextEscapedJS("PartySearch.TabHeading",TradePortalConstants.TEXT_BUNDLE);
  String transactionOid       = null;
  String transactionType      = null;
  String transactionStatus    = null;

  String rejectionIndicator   = "";
  String rejectionReasonText  = "";
	 
  String termsPartyOid        = null;
  String newTransaction = null;
    
  boolean getDataFromDoc      = true; //== true if data is retrieved from the doc cache
  boolean phraseSelected      = false; // Indicates if a phrase was selected.
  String  phraseType          = "";
  boolean doingFillInText     = false;
  boolean           corpOrgHasMultipleAddresses   = false;
  String            corpOrgOid                    = null;

  DocumentHandler doc         = null; //default.doc
  String buttonPressed ="";
  Vector error = null;
  	
  String loginLocale          = userSession.getUserLocale();
  String loginRights          = userSession.getSecurityRights();

  //cquinton 2/12/2013 lookup locale specific datePattern description from refData manager
  String datePattern = userSession.getDatePattern();
  String datePatternDisplay = refData.getDescr("DATEPATTERN", datePattern, loginLocale);
  String dateWidgetOptions = "placeHolder:'" + datePatternDisplay + "'";

    // Dates are stored in the database as one field but displayed on the
    // page as 3.  These variables hold the pieces for each date.
    String overseasDay              = null;
    String overseasMonth            = null;
    String overseasYear             = null;
    String validFromDay         = null;
    String validFromMonth       = null;
    String validFromYear        = null;
    String validToDay           = null;
    String validToMonth         = null;
    String validToYear          = null;

    // This array of string is used when creating the SLC Issue Application Detail Form links.
    String linkArgs[] = {"","","",""};

    // Variables used for populating ref data dropdowns.
    String countries = "";
    String currencies = "";
    String guarTypes = "";
    String defaultText = resMgr.getText("transaction.SelectPhrase", TradePortalConstants.TEXT_BUNDLE);
    String options = "";
    String sortXsl = "/common/xsl/SortByOption.xsl";
    boolean canDisplayPDFLinks = false;

    // These are the beans used on the page.
    Vector vVector = new Vector();
	int CHRG_INC_DEFAULT_ROW_COUNT = 3;
	List chrgIncList = new ArrayList(CHRG_INC_DEFAULT_ROW_COUNT);
	AdditionalReqDocWebBean chrgInc=null;
	int chrgIncRowCount=0;
	if(request.getParameter("numberOfMultipleObjects")!=null)
	  chrgIncRowCount = Integer.parseInt(request.getParameter("numberOfMultipleObjects"));
	else
	  chrgIncRowCount = CHRG_INC_DEFAULT_ROW_COUNT;

    TransactionWebBean transaction  = (TransactionWebBean)
                                       beanMgr.getBean("Transaction");
    InstrumentWebBean instrument    = (InstrumentWebBean)
                                       beanMgr.getBean("Instrument");
    TemplateWebBean template        = (TemplateWebBean)
                                       beanMgr.getBean("Template");
    TermsWebBean terms              = null;

    TermsPartyWebBean beneficiary = beanMgr.createBean(TermsPartyWebBean.class, "TermsParty");
    TermsPartyWebBean applicant   = beanMgr.createBean(TermsPartyWebBean.class, "TermsParty");
    TermsPartyWebBean bank        = beanMgr.createBean(TermsPartyWebBean.class, "TermsParty");
    TermsPartyWebBean agent       = beanMgr.createBean(TermsPartyWebBean.class, "TermsParty");

    FeeWebBean feeWebBean = null;
    // Get the document from the cache.  We may use it to repopulate the
    // screen if returning from another page, a save, validation, or any other
    // mediator called from this page.  Otherwise, we assume an instrument oid
    // and transaction oid was passed in.

    doc = formMgr.getFromDocCache();
 
	buttonPressed = doc.getAttribute("/In/Update/ButtonPressed");
	error = doc.getFragments("/Error/errorlist/error");

    String buttonClicked = request.getParameter("buttonName");
    
    Debug.debug("doc from cache follows below: \n" + doc.toString());

    /******************************************************************************
    We are either entering the page or returning from somewhere.  These are the
    conditions for how to populate the web beans.  Data comes from either the
    database or the doc cache (/In section) with some variation.

    Mode           Condition                      Populate Beans From
    -------------  -----------------------------  --------------------------------
    Enter Page     /In/Transaction doesn't exist  Instrument and Template web
                                                  beans already populated, get
                                                  data for Terms and TermsParty
                                                  web beans from database

    return from    /In/NewPartyFromSearchInfo/PartyOid   doc cache (/In); also use Party
    Party Search     exists                       SearchInfo to lookup, and
                                                  populate a specific TermsParty
                                                  web bean

  return from    /In/AddressSearchInfo/Address  doc cache (/In); use /In/Address
  Address Search   SearchPartyType exists       SearchInfo to populate a specific
                                                TermsParty web bean

    return from    /Out/PhraseLookupInfo exists   doc cache (/In); but use Phrase
    Phrase Lookup                                 LookupInfo text (from /Out) to
                                                  replace a specific phrase text
                                                  in the /In document before
                                                  populating

    return from    /Error/maxerrorseverity < 4    Same as Enter Page (data is
    Transaction                                   retrieved from database)
    mediator
    no error)

    return from    /Error/maxerrorseverity > 3    doc cache (/In)
    Transaction
    mediator
    (error)
    ******************************************************************************/

    String maxError = doc.getAttribute("/Error/maxerrorseverity");

    if (maxError != null && maxError.compareTo(String.valueOf(ErrorManager.WARNING_ERROR_SEVERITY)) <= 0)
        getDataFromDoc = false; // No errors, so don't get the data from doc.
    //ir cnuk113043991 - check to see if transaction needs to be refreshed
    // if so, refresh it and do not get data from doc as it is wrong
    if ( "Y".equals(doc.getAttribute("/Out/Refresh/Transaction")) ) {
       transaction.getDataFromAppServer();
       getDataFromDoc = false;
    }
    if (doc.getDocumentNode("/In/NewPartyFromSearchInfo/Type") != null) 
    {
       // We have returned from the party search page.  Get data from doc cache
       getDataFromDoc = true;
    }
    if (doc.getDocumentNode("/In/AddressSearchInfo/AddressSearchPartyType") != null) {
       // We have returned from the address search page.  Get data from doc cache
       getDataFromDoc = true;
    }
    if (doc.getDocumentNode("/In/Transaction") == null)
    {
        // No /In/Transaction means we've never looked up the data.
        Debug.debug("No /In/Transaction section - get data from database");
        getDataFromDoc = false;
    }

    // We have browser/platform specific logic following.  For the Macintosh
    // under IE, when returning from filling in a partially modifiable phrase,
    // in multi-part mode, IE seems to cause another submit to occur sending 
    // the page back to part 1 and not saving the data.  Very strange.  For
    // this condition we'll suppress the setting of the focus to the anchor
    // (this seems to correct the problem).

    String userAgent = request.getHeader("User-Agent");
    boolean isMacIE = false;

    if ( (userAgent.indexOf("Mac") > -1) && (userAgent.indexOf("MSIE") > -1) ) {
        isMacIE = true;
    }

    if (doc.getDocumentNode("/Out/PhraseLookupInfo") != null) {
        // A Looked up phrase exists.  Replace it in the /In document.
        Debug.debug("Found a looked-up phrase");
        getDataFromDoc = true;
        phraseSelected = true;
        phraseType = doc.getAttribute("/Out/PhraseLookupInfo/PhraseType");
        if (phraseType == null) phraseType = "";

        String textField = doc.getAttribute("/In/PhraseLookupInfo/text_field_name");
        String result = doc.getAttribute("/Out/PhraseLookupInfo/Result");
        if (result.equals(TradePortalConstants.INDICATOR_YES)) {
            // Take the looked up and appended phrase text from the /Out section
            // and copy it to the /In section.  This allows the beans to be
            // properly populated.
            String xmlPath = doc.getAttribute("/In/PhraseLookupInfo/text_path");
            xmlPath = "/In" + xmlPath;

            doc.setAttribute(xmlPath, doc.getAttribute("/Out/PhraseLookupInfo/NewText"));

            // If we returned from the phrase lookup without errors, set the focus
            // to the correct field.  Otherwise, don't set the focus so the user can
            // see the error.  The focus field varies based on the phrase type 
            // selected
            if (maxError != null 
               && maxError.compareTo(String.valueOf(ErrorManager.WARNING_ERROR_SEVERITY)) < 0) {

                // We're dealing with the fill-in-the-blank field.  Special logic to
                // decide the focus field is required since this is ALWAYS a readonly
                // field and we can't put focus on the text area.
                if (xmlPath.equals("/In/Terms/guar_bank_standard_text")) {
                   if (phraseType.equals(TradePortalConstants.PHRASE_PARTLY_MODIFIABLE)) {
                      focusField = "PhraseField0";
                      focusSet = true;
                   } else if (phraseType.equals(TradePortalConstants.PHRASE_PARTLY_MODIF_FILLED)) {
                     // No place to put focus after the phrase has been filled in.  Also
                     // since FILLED is not a valid phrase type, reset the type to a 
                     // valid one.

                     if (isMacIE) {
                       // for the mac ie environment, don't bother setting the
                       // focus to the anchor -- it doesn't work.
                       focusField = "";
                       focusSet = true; 
                     } else {
                       onLoad += "location.hash='#" + "BSW" + "';";
                       focusSet = true;
                     }
                     phraseType = TradePortalConstants.PHRASE_PARTLY_MODIFIABLE;

                   } else if (InstrumentServices.isBlank(phraseType)) {
                     // This occurs when the phrase has been cleared.  In this case, put
                     // focus on the dropdown field.
                     focusField = "bankStandardTextPhraseDropDown";
                     focusSet = true;

                   } else {
                     // Type is static or modifiable (which it shouldn't be).  Can't
                     // put focus on dropdown (doesn't exist) or textarea (readonly).
                     // Use the Bank Standard Wording anchor as the "focus"
                     if (isMacIE) {
                       // for the mac ie environment, don't bother setting the
                       // focus to the anchor -- it doesn't work.
                       focusField = "";
                       focusSet = true; 
                     } else {
                       onLoad += "location.hash='#" + "BSW" + "';";
                       focusSet = true;
                     }
                   }

                   // Lastly, set the guar_bank_std_phrase_type in the input doc
                   // so the terms bean is properly populated.
                   doc.setAttribute("/In/Terms/guar_bank_std_phrase_type", phraseType);

               } else {
                   // else it's just a regular text field so set the focus.
                   focusField = textField;
                   focusSet = true;
               }
            }
 
        }

    }
	
    if (doc.getDocumentNode("/Out/newTransaction")!=null) {
        newTransaction = doc.getAttribute("/Out/newTransaction");
        session.setAttribute("newTransaction", newTransaction);
        System.out.println("found newTransaction = "+ newTransaction);
      } 
      else {
        newTransaction = (String) session.getAttribute("newTransaction");
        if ( newTransaction==null ) {
          newTransaction = TradePortalConstants.INDICATOR_NO;
        }
        System.out.println("used newTransaction from session = " + newTransaction);
      }
   
  //cquinton 1/18/2013 remove old close action behavior
 
    if (getDataFromDoc)
    {
        Debug.debug("Populating beans from doc cache");

        // Populate the beans from the input doc.
        try
        {
            terms = (TermsWebBean) beanMgr.getBean("Terms");

            terms.populateFromXmlDoc(doc, "/In");
            instrument.populateFromXmlDoc(doc, "/In");
            transaction.populateFromXmlDoc(doc, "/In");
            template.populateFromXmlDoc(doc, "/In");

            termsPartyOid = terms.getAttribute("c_FirstTermsParty");
            if (termsPartyOid != null && !termsPartyOid.equals(""))
            {
                beneficiary.setAttribute("terms_party_oid", termsPartyOid);
                beneficiary.getDataFromAppServer();
            }
            termsPartyOid = terms.getAttribute("c_SecondTermsParty");
            if (termsPartyOid != null && !termsPartyOid.equals(""))
            {
                applicant.setAttribute("terms_party_oid", termsPartyOid);
                applicant.getDataFromAppServer();
            }
            termsPartyOid = terms.getAttribute("c_ThirdTermsParty");
            if (termsPartyOid != null && !termsPartyOid.equals(""))
            {
                bank.setAttribute("terms_party_oid", termsPartyOid);
                bank.getDataFromAppServer();
            }
            termsPartyOid = terms.getAttribute("c_FourthTermsParty");
            if (termsPartyOid != null && !termsPartyOid.equals(""))
            {
                agent.setAttribute("terms_party_oid", termsPartyOid);
                agent.getDataFromAppServer();
            }

            DocumentHandler termsPartyDoc = null;

            termsPartyDoc = doc.getFragment("/In/Terms/FirstTermsParty");
            beneficiary.loadTermsPartyFromDocTagsOnly(termsPartyDoc);

            termsPartyDoc = doc.getFragment("/In/Terms/SecondTermsParty");
            applicant.loadTermsPartyFromDocTagsOnly(termsPartyDoc);

            termsPartyDoc = doc.getFragment("/In/Terms/ThirdTermsParty");
            bank.loadTermsPartyFromDocTagsOnly(termsPartyDoc);

            termsPartyDoc = doc.getFragment("/In/Terms/FourthTermsParty");
            agent.loadTermsPartyFromDocTagsOnly(termsPartyDoc);

            /*
             * Parse out dates
             */
            String date = terms.getAttribute("overseas_validity_date");
            Debug.debug("date -> " + date);
            overseasDay = doc.getAttribute("/In/Terms/overseas_validity_date_day");
            overseasMonth = doc.getAttribute("/In/Terms/overseas_validity_date_month");
            overseasYear = doc.getAttribute("/In/Terms/overseas_validity_date_year");

            date = terms.getAttribute("guar_valid_from_date");
            validFromDay = doc.getAttribute("/In/Terms/guar_valid_from_date_day");
            validFromMonth = doc.getAttribute("/In/Terms/guar_valid_from_date_month");
            validFromYear = doc.getAttribute("/In/Terms/guar_valid_from_date_year");

            date = terms.getAttribute("expiry_date");
            validToDay = doc.getAttribute("/In/Terms/expiry_date_day");
            validToMonth = doc.getAttribute("/In/Terms/expiry_date_month");
            validToYear = doc.getAttribute("/In/Terms/expiry_date_year");
	 		
            vVector = doc.getFragments("/In/Transaction/FeeList");
	 		for (int iLoop=0; iLoop<vVector.size(); iLoop++)
			   {
	 			feeWebBean= beanMgr.createBean(FeeWebBean.class,"Fee");
			    DocumentHandler feeDoc = (DocumentHandler) vVector.elementAt(iLoop);
			    feeWebBean.populateFromXmlDoc(feeDoc,"/");
			    chrgIncList.add(feeWebBean);
			   } 		
	 		
			if(vVector.size() < CHRG_INC_DEFAULT_ROW_COUNT) {
			  for(int iLoop = vVector.size(); iLoop < CHRG_INC_DEFAULT_ROW_COUNT; iLoop++) {
				  chrgIncList.add(beanMgr.createBean(FeeWebBean.class,"Fee"));
				  }
			}

        }
        catch (Exception e)
        {
            out.println("Contact Administrator: " +
                        "Unable to reload data after returning to page. " +
                        "Error is " + e.toString());
        }
    }
    else
    {
        Debug.debug("populating beans from database");
        // We will perform a retrieval from the database.
        // Instrument and Transaction were already retrieved.  Get
        // the rest of the data

        terms = transaction.registerCustomerEnteredTerms();

        DocumentHandler myDoc = new DocumentHandler();
        terms.populateXmlDoc( myDoc );
        Debug.debug("The terms object is: " + myDoc.toString() );

        termsPartyOid = terms.getAttribute("c_FirstTermsParty");
        if (termsPartyOid != null && !termsPartyOid.equals(""))
        {
            beneficiary.setAttribute("terms_party_oid", termsPartyOid);
            beneficiary.getDataFromAppServer();
        }

        termsPartyOid = terms.getAttribute("c_SecondTermsParty");
        if (termsPartyOid != null && !termsPartyOid.equals(""))
        {
            applicant.setAttribute("terms_party_oid", termsPartyOid);
            applicant.getDataFromAppServer();
        }

        termsPartyOid = terms.getAttribute("c_ThirdTermsParty");
        if (termsPartyOid != null && !termsPartyOid.equals(""))
        {
            bank.setAttribute("terms_party_oid", termsPartyOid);
            bank.getDataFromAppServer();
        }

        termsPartyOid = terms.getAttribute("c_FourthTermsParty");
        if (termsPartyOid != null && !termsPartyOid.equals(""))
        {
            agent.setAttribute("terms_party_oid", termsPartyOid);
            agent.getDataFromAppServer();
        }

        // Because date fields are represented as three fields on the page,
        // we need to parse out the date values into three fields.  Pre-mediator
        // logic puts these three fields back together for the terms bean.

        /*
         * Parse out dates
         */
        String date = terms.getAttribute("overseas_validity_date");
        Debug.debug("****************************************");
        Debug.debug("date -> " + date);
        overseasDay = TPDateTimeUtility.parseDayFromDate(date);
        overseasMonth = TPDateTimeUtility.parseMonthFromDate(date);
        overseasYear = TPDateTimeUtility.parseYearFromDate(date);
        Debug.debug("overseasMonth -> " + overseasMonth);

        date = terms.getAttribute("guar_valid_from_date");
        validFromDay = TPDateTimeUtility.parseDayFromDate(date);
        validFromMonth = TPDateTimeUtility.parseMonthFromDate(date);
        validFromYear = TPDateTimeUtility.parseYearFromDate(date);

        date = terms.getAttribute("expiry_date");
        validToDay = TPDateTimeUtility.parseDayFromDate(date);
        validToMonth = TPDateTimeUtility.parseMonthFromDate(date);
        validToYear = TPDateTimeUtility.parseYearFromDate(date);

   	 vVector=null;
   	QueryListView  queryListView = null;
	try{
			StringBuffer sql = new StringBuffer();
			queryListView = (QueryListView) EJBObjectFactory.createClientEJB(formMgr.getServerLocation(), "QueryListView");
           sql.append("select fee_oid, SETTLEMENT_CURR_CODE, SETTLEMENT_CURR_AMOUNT, CHARGE_TYPE, P_TRANSACTION_OID  ");
           sql.append(" from FEE ");
           sql.append(" where P_TRANSACTION_OID = ?");
           sql.append(" order by ");
           sql.append(resMgr.localizeOrderBy("fee_oid"));
           queryListView.setSQL(sql.toString(), new Object[]{transaction.getAttribute("transaction_oid")});
           queryListView.getRecords();

           DocumentHandler feeOidList = new DocumentHandler();
           feeOidList = queryListView.getXmlResultSet();
         	vVector = feeOidList.getFragments("/ResultSetRecord");
			} catch (Exception e) {
	            e.printStackTrace();
	      } finally {
	            try {
	                  if (queryListView != null) {
	                        queryListView.remove();
	                  }
	            } catch (Exception e) {
	                  System.out.println("Error removing querylistview in Transaction-GUAR-ISS.jsp!");
	            }
	      }
       for (int iLoop=0; iLoop<vVector.size(); iLoop++)
		   {
       	feeWebBean= beanMgr.createBean(FeeWebBean.class,"Fee");
       	DocumentHandler addReqDocDoc = (DocumentHandler) vVector.elementAt(iLoop);
       	feeWebBean.setAttribute("fee_oid", addReqDocDoc.getAttribute("/FEE_OID"));
       	feeWebBean.setAttribute("settlement_curr_code", addReqDocDoc.getAttribute("/SETTLEMENT_CURR_CODE"));
       	feeWebBean.setAttribute("settlement_curr_amount", addReqDocDoc.getAttribute("/SETTLEMENT_CURR_AMOUNT"));
       	feeWebBean.setAttribute("charge_type", addReqDocDoc.getAttribute("/CHARGE_TYPE"));
       	feeWebBean.setAttribute("p_transaction_oid", addReqDocDoc.getAttribute("/P_TRANSACTION_OID"));
       	chrgIncList.add(feeWebBean);
		   } 		                                                                                                                                                                                            
		    
		if(vVector.size() < CHRG_INC_DEFAULT_ROW_COUNT) {
		  for(int iLoop = vVector.size(); iLoop < CHRG_INC_DEFAULT_ROW_COUNT; iLoop++) {
			  chrgIncList.add(beanMgr.createBean(FeeWebBean.class,"Fee"));
			  }
		}

        
    }
	
    /*************************************************
    * Load New Party
    **************************************************/
    if (doc.getDocumentNode("/In/NewPartyFromSearchInfo/Type") != null &&
        doc.getDocumentNode("/In/NewPartyFromSearchOutput/PartyOid") != null ) {

        // We have returned from the PartyDetailNew page.  Based on the returned
        // data, RELOAD one of the terms party beans with the new party

        String termsPartyType = doc.getAttribute("/In/NewPartyFromSearchInfo/Type");
        String partyOid = doc.getAttribute("/In/NewPartyFromSearchOutput/PartyOid");

        Debug.debug("Returning from party search with " + termsPartyType + "/" + partyOid);

        // Use a Party web bean to get the party data for the selected oid.
        PartyWebBean party = beanMgr.createBean(PartyWebBean.class, "Party");

        party.setAttribute("party_oid", partyOid);
        party.getDataFromAppServer();

        DocumentHandler partyDoc = new DocumentHandler();
        party.populateXmlDoc(partyDoc);

        partyDoc = partyDoc.getFragment("/Party");

        // Based on the party type being returned (which we previously set)
        // reload one of the terms party web beans with the data from the
        // doc.
        if (termsPartyType.equals(TradePortalConstants.BENEFICIARY)) {
            beneficiary.loadTermsPartyFromDoc(partyDoc);
            focusField = "beneficiary_name";
            focusSet = true;
        }
        if (termsPartyType.equals(TradePortalConstants.APPLICANT)) {
            applicant.loadTermsPartyFromDoc(partyDoc);
	    //rbhaduri - 13th June 06 - IR SAUG051361872
	    applicant.setAttribute("address_search_indicator", "N");

            focusField = "";
            focusSet = true;
            onLoad += "location.hash='#" + termsPartyType + "';";
        }
        if (termsPartyType.equals(TradePortalConstants.OVERSEAS_BANK)) {
            bank.loadTermsPartyFromDoc(partyDoc);
            focusField = "bank_name";
            focusSet = true;
        }
        if (termsPartyType.equals(TradePortalConstants.AGENT)) {
            agent.loadTermsPartyFromDoc(partyDoc);
            focusField = "agent_name";
            focusSet = true;
        }
    }

    
//rbhaduri - 8th August 06 - IR AOUG100368306 - Moved it here from below
BigInteger requestedSecurityRight = null;
String convertedTran = transaction!=null? transaction.getAttribute("converted_transaction_ind"):"";
Debug.debug("convertedTran:"+convertedTran);
if(TradePortalConstants.INDICATOR_YES.equals(convertedTran)){
	requestedSecurityRight = SecurityAccess.CC_GUA_CREATE_MODIFY;
}else{
	requestedSecurityRight = SecurityAccess.GUAR_CREATE_MODIFY;
}

%>


<%@ include file="fragments/Transaction-PageMode.frag" %>

<%

    //rbhaduri - 8th August 06 - IR AOUG100368306 - Moved the code here from below
    if (isTemplate) 
   	corpOrgOid = template.getAttribute("owner_org_oid");
    else
   	corpOrgOid = instrument.getAttribute("corp_org_oid");
	//jgadela  R90 IR T36000026319 - SQL FIX
	Object[] sqlParamsCnt = new Object[1];
	sqlParamsCnt[0] =  corpOrgOid;
    if (DatabaseQueryBean.getCount("address_oid", "address", "p_corp_org_oid = ?", true, sqlParamsCnt) > 0 )
    {
   		corpOrgHasMultipleAddresses = true;
    }  
    //MEer CR-1027
    CorporateOrganizationWebBean corporateOrg = beanMgr.createBean(CorporateOrganizationWebBean.class, "CorporateOrganization");
	corporateOrg.getById(corpOrgOid);
   	String bankOrgGroupOid = corporateOrg.getAttribute("bank_org_group_oid");
    
    BankOrganizationGroupWebBean bankOrganizationGroup = beanMgr.createBean(BankOrganizationGroupWebBean.class,    "BankOrganizationGroup");
    bankOrganizationGroup.getById(bankOrgGroupOid);
    
    String selectedPDFType =  bankOrganizationGroup.getAttribute("gua_pdf_type");
    if(TradePortalConstants.APPLICATION_PDF_BASIC_TYPE.equals(selectedPDFType)  || TradePortalConstants.APPLICATION_PDF_EXPANDED_TYPE.equals(selectedPDFType)){
 			canDisplayPDFLinks = true;
    }
    

  /*************************************************
  * Load address search info if we come back from address search page.
  * Reload applicant terms party web beans with the data from 
  * the address
  **************************************************/
  if (doc.getDocumentNode("/In/AddressSearchInfo/AddressSearchPartyType") != null) {
     String termsPartyType = doc.getAttribute("/In/AddressSearchInfo/AddressSearchPartyType");
     String addressOid = doc.getAttribute("/In/AddressSearchInfo/AddressOid");
     
     //rbhaduri - 9th Oct 06 - IR AOUG100368306
     String primarySelected = doc.getAttribute("/In/AddressSearchInfo/ChoosePrimary");	
    
    Debug.debug("Returning from address search with " + termsPartyType 
           + "/" + addressOid);

     // Use a Party web bean to get the party data for the selected oid.
     AddressWebBean address = null;  
     if ((addressOid != null||primarySelected != null) && termsPartyType.equals(TradePortalConstants.APPLICANT)) {
        
        if (primarySelected.equals("PRIMARY")) {
		applicant.setAttribute("name", corporateOrg.getAttribute("name"));
        	applicant.setAttribute("address_line_1", corporateOrg.getAttribute("address_line_1"));
        	applicant.setAttribute("address_line_2", corporateOrg.getAttribute("address_line_2"));
     		applicant.setAttribute("address_city", corporateOrg.getAttribute("address_city"));
       		applicant.setAttribute("address_state_province", corporateOrg.getAttribute("address_state_province"));
       		applicant.setAttribute("address_country", corporateOrg.getAttribute("address_country"));
       		applicant.setAttribute("address_postal_code", corporateOrg.getAttribute("address_postal_code"));
       		applicant.setAttribute("address_seq_num", "1");
       		//Rel9.5 CR1132 Populate userdefinedfields from corporate
			applicant.setAttribute("user_defined_field_1", corporateOrg.getAttribute("user_defined_field_1"));
			applicant.setAttribute("user_defined_field_2", corporateOrg.getAttribute("user_defined_field_2"));
			applicant.setAttribute("user_defined_field_3", corporateOrg.getAttribute("user_defined_field_3"));
			applicant.setAttribute("user_defined_field_4", corporateOrg.getAttribute("user_defined_field_4"));
		}
	else {address = beanMgr.createBean(AddressWebBean.class, "Address");
        	address.setAttribute("address_oid", addressOid);
        	address.getDataFromAppServer();
        	applicant.setAttribute("name",address.getAttribute("name"));
        	applicant.setAttribute("address_line_1",address.getAttribute("address_line_1"));
        	applicant.setAttribute("address_line_2",address.getAttribute("address_line_2"));
     		applicant.setAttribute("address_city",address.getAttribute("city"));
       		applicant.setAttribute("address_state_province",address.getAttribute("state_province"));
       		applicant.setAttribute("address_country",address.getAttribute("country"));
       		applicant.setAttribute("address_postal_code",address.getAttribute("postal_code"));
       		applicant.setAttribute("address_seq_num",address.getAttribute("address_seq_num"));
       	 //Rel9.5 CR1132 Populate userdefinedfields from address
      		applicant.setAttribute("user_defined_field_1", address.getAttribute("user_defined_field_1"));
      		applicant.setAttribute("user_defined_field_2", address.getAttribute("user_defined_field_2"));
      		applicant.setAttribute("user_defined_field_3", address.getAttribute("user_defined_field_3"));
      		applicant.setAttribute("user_defined_field_4", address.getAttribute("user_defined_field_4"));
	}
     }  
     focusField = "";
     focusSet = true;
     onLoad += "location.hash='#" + termsPartyType + "';";
     
  }


    transactionType = transaction.getAttribute("transaction_type_code");
    transactionStatus = transaction.getAttribute("transaction_status");
    transactionOid = transaction.getAttribute("transaction_oid");

    // Get the transaction rejection indicator to determine whether to show the rejection reason text
    rejectionIndicator = transaction.getAttribute("rejection_indicator");
    rejectionReasonText = transaction.getAttribute("rejection_reason_text");
  
    instrumentType = instrument.getAttribute("instrument_type_code");
    instrumentStatus = instrument.getAttribute("instrument_status");

    /* KMehta 10/15/2013 Rel8400 CR-910  start
       Code added for getting SLC-GUA indicator on Bank group for the specific Operational Bank Org
    */
    
    String opBankOrgOid = instrument.getAttribute("op_bank_org_oid");
    QueryListView queryListView = null;
    String slc_gua_indicator = null;
    
  	try{
  			
  			StringBuffer sql = new StringBuffer();
  			queryListView = (QueryListView) EJBObjectFactory.createClientEJB(formMgr.getServerLocation(), "QueryListView");		    
  		    sql.append("select slc_gua_expiry_date_required ");
  		    sql.append("from bank_organization_group bog, operational_bank_org obg " );
  	        sql.append("where bog.organization_oid =  obg.a_bank_org_group_oid ");                 
  	        sql.append("and obg.organization_oid = ?");
  	            
  			Debug.debug("SLC_GUA indicator : " + sql.toString());
  			queryListView.setSQL(sql.toString(), new Object[]{opBankOrgOid});
  		    queryListView.getRecords();
      	    DocumentHandler result = queryListView.getXmlResultSet();
      	    slc_gua_indicator = result.getAttribute("/ResultSetRecord(0)/SLC_GUA_EXPIRY_DATE_REQUIRED");
      	    Debug.debug("slc_gua_indicator: " +slc_gua_indicator);
  	}	catch (Exception e)
  	   {
  	      e.printStackTrace();
  	   }
  	   finally
  	   {
  	      try
  	      {
  	         if (queryListView != null)
  	         {
  	            queryListView.remove();
  	         }
  	      }
  	      catch (Exception e)
  	      {
  	         System.out.println("Error removing querylistview in Transaction-SLC-ISS.jsp!");
  	      }
  	   }
  	/* KMehta 10/15/2013 Rel8400 CR-910 End */
  	
    Debug.debug("Instrument Type " + instrumentType);
    Debug.debug("Instrument Status " + instrumentStatus);
    Debug.debug("Transaction Type " + transactionType);
    Debug.debug("Transaction Status " + transactionStatus);

    // Now determine the mode for how the page operates (readonly, etc.)
    //rbhaduri - 8th August 06 - IR AOUG100368306 - Moved it above
    //BigInteger requestedSecurityRight = SecurityAccess.GUAR_CREATE_MODIFY;

    %>
    
	<%--  //rbhaduri - 8th August 06 - IR AOUG100368306 - Moved it above
	<%@ include file="fragments/Transaction-PageMode.frag" %>
	--%>

    <%
      

    DocumentHandler phraseLists = formMgr.getFromDocCache(TradePortalConstants.PHRASE_LIST_DOC);
    if (phraseLists == null) phraseLists = new DocumentHandler();

    // This list of phrases is used in the General Section
    DocumentHandler customerTextList = phraseLists.getFragment("/CustomerText");
    if (customerTextList == null)
    {
        customerTextList = PhraseUtility.createPhraseList(TradePortalConstants.PHRASE_CAT_GUAR_CUST,
                                                      userSession, formMgr, resMgr);
        phraseLists.addComponent("/CustomerText", customerTextList);
    }

    //this list of phrases is used in the bank instructions section
    DocumentHandler specialInstructionsList = phraseLists.getFragment("/SpecialInstructions");
    if (specialInstructionsList == null)
    {
        specialInstructionsList = PhraseUtility.createPhraseList(TradePortalConstants.PHRASE_CAT_SPCL_INST,
                                                                 userSession, formMgr, resMgr);
        phraseLists.addComponent("/SpecialInstructions", specialInstructionsList);
    }

    //this list of phrases is used in the bank instructions section
    DocumentHandler bankStandardTextList = phraseLists.getFragment("/BankStandardText");
    if (bankStandardTextList == null)
    {
        bankStandardTextList = PhraseUtility.createPhraseList(TradePortalConstants.PHRASE_CAT_GUAR_BANK,
                                                                 userSession, formMgr, resMgr);
        phraseLists.addComponent("/BankStandardText", bankStandardTextList);
    }

  formMgr.storeInDocCache(TradePortalConstants.PHRASE_LIST_DOC, phraseLists);
%>


<%-- ********************* HTML for page begins here ********************* --%>

<%
  if (isTemplate) {
%>
    <%--   <jsp:include page="/common/ButtonPrep.jsp" />  --%>
<%
  }
%>

<%-- Body tag included as part of common header --%>
<%
    // The navigation bar is only shown when editing templates.  For transactions
    // it is not shown ti minimize the chance of leaving the page without properly
    // unlocking the transaction.
    String showNavBar = TradePortalConstants.INDICATOR_NO;
    if (isTemplate)
        showNavBar = TradePortalConstants.INDICATOR_YES;
%>

<%			
  // Auto save the form when time-out if not readonly.  
  // (Header.jsp will check for auto-save setting of the corporate org).
  String autoSaveFlag = isReadOnly ? TradePortalConstants.INDICATOR_NO : TradePortalConstants.INDICATOR_YES;
  String templateFlag = isTemplate ? TradePortalConstants.INDICATOR_YES : TradePortalConstants.INDICATOR_NO;
%>

<%
  String pageTitleKey;
 if ( TradePortalConstants.INDICATOR_YES.equals(newTransaction) ) {
    pageTitleKey = "SecondaryNavigation.NewInstruments";
    if (isTemplate){
    	if(userSession.getSecurityType().equals(TradePortalConstants.ADMIN)){
    		userSession.setCurrentPrimaryNavigation("AdminNavigationBar.RefData");
    	}else{
    	   userSession.setCurrentPrimaryNavigation("NavigationBar.RefData");
    	}
    }else
    	userSession.setCurrentPrimaryNavigation("NavigationBar.NewInstruments");
  } else {
    pageTitleKey = "SecondaryNavigation.Instruments";
    if (isTemplate){
    	if(userSession.getSecurityType().equals(TradePortalConstants.ADMIN)){
    		userSession.setCurrentPrimaryNavigation("AdminNavigationBar.RefData");
    	}else{
    	   userSession.setCurrentPrimaryNavigation("NavigationBar.RefData");
    	}
    }else
    	userSession.setCurrentPrimaryNavigation("NavigationBar.Transactions");
  }
    
  String helpFileName;
  String item1Key;
	
  // Determine if this is a real guarantee or a standby using guarantee form
  if(transaction.getAttribute("standby_using_guarantee_form").equals(TradePortalConstants.INDICATOR_NO)){
              helpFileName = "customer/issue_guarantee.htm";
  			  item1Key = "SecondaryNavigation.Instruments.OutgoingGuarantee"; 
  }else{
              helpFileName = "customer/issue_detailed_standby_lc.htm";
  			  item1Key = "SecondaryNavigation.Instruments.OutgoingStandbyLCDetailed";
  }

  //cquinton 11/19/2012 pass info for recent instruments into header
  String newRecentTransactionOid = transaction.getAttribute("transaction_oid");
  String newRecentInstrumentId = instrument.getAttribute("complete_instrument_id");
  String newRecentTransactionType = transaction.getAttribute("transaction_type_code");
  String newRecentTransactionStatus = transaction.getAttribute("transaction_status");
%>

<jsp:include page="/common/Header.jsp">
  <jsp:param name="autoSaveFlag"           value="<%=autoSaveFlag%>" />
  <jsp:param name="templateFlag"           value="<%=templateFlag%>" />
  <jsp:param name="newRecentTransactionOid" value="<%=newRecentTransactionOid%>" />
  <jsp:param name="newRecentInstrumentId"  value="<%=newRecentInstrumentId%>" />
  <jsp:param name="newRecentTransactionType" value="<%=newRecentTransactionType%>" />
  <jsp:param name="newRecentTransactionStatus" value="<%=newRecentTransactionStatus%>" />
</jsp:include>

<div class="pageMain">
<div class="pageContent">


<% //cr498 begin
   //Include ReAuthentication frag in case re-authentication is required for
   //authorization of transactions for this client
   String certAuthURL = "";
   Cache reCertCache = (Cache)TPCacheManager.getInstance().getCache(TradePortalConstants.CLIENT_BANK_CACHE);
   DocumentHandler CBCResult = (DocumentHandler)reCertCache.get(userSession.getClientBankOid());	
   String requireTranAuth = CBCResult.getAttribute("/ResultSetRecord(0)/REQUIRE_TRAN_AUTH");
   boolean requireAuth = InstrumentAuthentication.requireTransactionAuthentication(
       requireTranAuth,InstrumentAuthentication.TRAN_AUTH__GUAR_ISS);
   if (requireAuth) {
%>   
      <%@ include file="/logon/fragments/openReauthenticationWindow.frag" %>
<%
   }
   //cr498 end
%>
<%
		  if(isTemplate) {
		  String pageTitle;
		  String titleName;
		  String returnAction;
		  pageTitle = resMgr.getText("common.template", TradePortalConstants.TEXT_BUNDLE);
		  titleName = template.getAttribute("name");
		  StringBuffer title = new StringBuffer();
		  String itemKey = "";
		  title.append(pageTitle);
		  title.append( " : " );
		  if(instrument.getAttribute("instrument_type_code").equals("SLC")){
		  if(transaction.getAttribute("standby_using_guarantee_form").equals(TradePortalConstants.INDICATOR_NO)){     
				  itemKey = resMgr.getText("SecondaryNavigation.Instruments.OutgoingSTandbyLCSimple", TradePortalConstants.TEXT_BUNDLE); 
		  }else{
				  itemKey = resMgr.getText("SecondaryNavigation.Instruments.OutgoingStandbyLCDetailed", TradePortalConstants.TEXT_BUNDLE);
		  }
		  title.append(itemKey);
		  }
		  else
		  title.append( refData.getDescr(TradePortalConstants.INSTRUMENT_TYPE, 
		                 instrument.getAttribute("instrument_type_code"), 
		                 loginLocale) );
		  title.append( " - " );
		  title.append(titleName );
		  returnAction = "goToInstrumentCloseNavigator";
		
		  String transactionSubHeader = title.toString();
  		
  		%>
		<jsp:include page="/common/PageHeader.jsp">
				<jsp:param name="titleKey" value="<%= pageTitle%>"/>
				<jsp:param name="helpUrl"  value="<%=helpFileName%>" />
  		</jsp:include>
  		
  		<jsp:include page="/common/PageSubHeader.jsp">
 				 <jsp:param name="titleKey" value="<%= transactionSubHeader%>" />
  				 <jsp:param name="returnUrl" value="<%= formMgr.getLinkAsUrl(returnAction, response) %>" />
		</jsp:include> 		
		 <%}else{ %>
<jsp:include page="/common/PageHeader.jsp">
   <jsp:param name="titleKey" value="<%=pageTitleKey%>" />
   <jsp:param name="item1Key" value="<%=item1Key%>" />
   <jsp:param name="helpUrl"  value="<%=helpFileName%>" />
</jsp:include>

<jsp:include page="/common/TransactionSubHeader.jsp" />

	<%} %>		
			<form id="TransactionGUAR" name="TransactionGUAR" method="post" data-dojo-type="dijit.form.Form" action="<%=formMgr.getSubmitAction(response)%>">
			   <input type=hidden value="" name=buttonName>

    <%-- error section goes above form content --%>
    <div class="formArea">
    <jsp:include page="/common/ErrorSection.jsp" />

	<div class="formContent">	
	
			<% //cr498 begin
			  if (requireAuth) {
			%> 
			
			  <input type=hidden name="reCertification" value="<%=TradePortalConstants.INDICATOR_NO%>">
			  <input type=hidden name="reCertOK">
			  <input type=hidden name="logonResponse">
			  <input type=hidden name="logonCertificate">
			
			<%
			  } //cr498 end
			%> 
			
			<%
			    // Store values such as the userid, security rights, and org in a
			    // secure hashtable for the form.  Also store instrument and transaction
			    // data that must be secured.
			    Hashtable secureParms = new Hashtable();
			    secureParms.put("login_oid", userSession.getUserOid());
			    secureParms.put("owner_org_oid", userSession.getOwnerOrgOid());
			    secureParms.put("login_rights", loginRights);
			
			    secureParms.put("instrument_oid", instrument.getAttribute("instrument_oid"));
			    secureParms.put("instrument_type_code", instrumentType);
			    secureParms.put("instrument_status", instrumentStatus);
			    secureParms.put("corp_org_oid",  corpOrgOid);
			
			    secureParms.put("transaction_oid", transaction.getAttribute("transaction_oid"));
			    secureParms.put("transaction_type_code", transactionType);
			    secureParms.put("transaction_status", transactionStatus);
			
			    // If the terms record doesn't exist, set its oid to 0.
			    String termsOid = terms.getAttribute("terms_oid");
			    if (termsOid == null)
			        termsOid = "0";
			    secureParms.put("terms_oid", termsOid);
			
			    // To support the Route Trans/Authorize Trans buttons - this secureParm is required.
			    secureParms.put("transaction_instrument_info", transaction.getAttribute("transaction_oid") + "/" +
					            instrument.getAttribute("instrument_oid") + "/" + transactionType);
			
			   
			
			    if (isTemplate)
			    {
			        secureParms.put("template_oid", template.getAttribute("template_oid"));
			        secureParms.put("opt_lock", template.getAttribute("opt_lock"));
			    }
			  if (isTemplate) {
			%>
			    <%@ include file="fragments/TemplateHeader.frag" %>
			<%
			  } 
			%>		  			
					<% // [BEGIN] IR-YVUH032343792 - jkok %>
					  <%@ include file="fragments/Instruments-AttachDocuments.frag" %>
					<% // [END] IR-YVUH032343792 - jkok %>
					
					 <%	if (rejectionIndicator.equals(TradePortalConstants.INDICATOR_YES)
					    || rejectionIndicator.equals(TradePortalConstants.INDICATOR_X))
					  {
					%>
						<%=widgetFactory.createSectionHeader("0", "Rejection Reason") %>
					     <%@ include file="fragments/Transaction-RejectionReason.frag" %>
					     </div>
					<%
					  }
					%>
<% //CR 821 Added Repair Reason Section 
StringBuffer repairReasonWhereClause = new StringBuffer();
int  repairReasonCount = 0;
	
	/*Get all repair reason's count from transaction history table*/
	repairReasonWhereClause.append("p_transaction_oid = ?");
	repairReasonWhereClause.append(" and REPAIR_REASON is not null order by ACTION_DATETIME");
	//jgadela  R90 IR T36000026319 - SQL FIX
    Object[] sqlParamsReasonCount = new Object[1];
    sqlParamsReasonCount[0] =  transaction.getAttribute("transaction_oid");
    
	Debug.debug("repairReasonWhereCondition query : " + repairReasonWhereClause);
	repairReasonCount = DatabaseQueryBean.getCount("A_USER_OID", "transaction_history", repairReasonWhereClause.toString(), true, sqlParamsReasonCount);

			
if ( !(TradePortalConstants.TRANS_STATUS_PROCESSED_BY_BANK.equalsIgnoreCase(transactionStatus)) && repairReasonCount>0  ){%>
	<%=widgetFactory.createSectionHeader("", "TransactionHistory.RepairReason") %>
		<%@ include file="fragments/Transaction-RepairReason.frag" %>
	</div>
<%} %> 
				
					<% // [END] IR-YVUH032343792 - jkok %>	

						<%= widgetFactory.createSectionHeader("1", "GuaranteeIssue.Terms") %>
							<%@ include file="fragments/Transaction-GUAR-ISS-General.frag" %>
						</div>					    			        				    
				
						<%-- Leelavathi - CR-737 Rel8200 10th Dec 2012- Start --%>
						<%= widgetFactory.createSectionHeader("2", "GuaranteeIssue.OtherConditions") %>
							<%@ include file="fragments/Transaction-GUAR-ISS-OtherConditions.frag" %>
						</div>	
						<%= widgetFactory.createSectionHeader("3", "GuaranteeIssue.AdditionalInf") %>
							<%@ include file="fragments/Transaction-GUAR-ISS-Terms.frag" %>
							<%@ include file="fragments/Transaction-GUAR-ISS-BankStandardWording.frag" %>
						</div>												        				    
				
						<%= widgetFactory.createSectionHeader("4", "GuaranteeIssue.InstructionstoBank") %>
							<%@ include file="fragments/Transaction-GUAR-ISS-BankInstructions.frag" %>
						</div>
						
						<%= widgetFactory.createSectionHeader("5", "GuaranteeIssue.InternalInstructions") %>
							<%@ include file="fragments/Transaction-GUAR-ISS-InternalInstructions.frag" %>
						</div>												        
						<%
							int sectionNumber = 6;
							String convDetailsLabel = "GuaranteeIssue.ConversionDetails";
						        // Only display the bank defined section if the user is an admin user
						        // and this is a template.
						         // IR-42689 REL 9.4 SURREWSH Start
						         if (isTemplate && userSession.getSecurityType().equals(TradePortalConstants.ADMIN) || userSession.hasSavedUserSession()) {
						        	sectionNumber++;							        	
						%>
						
						<%= widgetFactory.createSectionHeader("6", "GuaranteeIssue.BankDefined") %>
							<%@ include file="fragments/Transaction-GUAR-ISS-BankDefined.frag" %>
						</div>
						<%-- Leelavathi - CR-737 Rel8200 10th Dec 2012- End --%>
		
            			<%
							} else {
							   secureParms.put("Irrevocable", terms.getAttribute("irrevocable"));
                                                           //cquinton 10/16/2013 Rel 8.3 ir#21872 do not setup ucpversiondetailind here
							   //secureParms.put("UCPVersionDetailInd", 
							   //                terms.getAttribute("ucp_version_details_ind"));	
							}
						%>
						
						<% if (TradePortalConstants.TRANS_STATUS_PROCESSED_BY_BANK.equalsIgnoreCase(transactionStatus) && repairReasonCount>0  ){
								 if (TradePortalConstants.ADMIN.equals(userSession.getSecurityType()))   {%>
 										<%=widgetFactory.createSectionHeader("7", "TransactionHistory.RepairReason", null,true) %>
										<%@ include file="fragments/Transaction-RepairReason.frag" %>
									</div>
								<%}else{%>
										<%=widgetFactory.createSectionHeader("6", "TransactionHistory.RepairReason", null, true) %>
										<%@ include file="fragments/Transaction-RepairReason.frag" %>
									</div>
							 <% 	}
						}%>

						<% if (TradePortalConstants.INDICATOR_YES.equals(transaction.getAttribute("converted_transaction_ind"))){ %>
							<%= widgetFactory.createSectionHeader(String.valueOf(sectionNumber), convDetailsLabel+String.valueOf(sectionNumber)) %>
								<%@ include file="fragments/Transaction-GUAR-ISS-ChargesIncurred.frag" %>
							</div>		
							
						<% 	sectionNumber++; 
							}
						%>
													<%   
							boolean isExternalBankUsed = getExternalBankUsed(instrument.getAttribute("op_bank_org_oid"));
								
								if (isExternalBankUsed){ %>
								
							<%= widgetFactory.createSectionHeader(String.valueOf(sectionNumber + 1), "GuaranteeIssue.ExternalBank"+String.valueOf(sectionNumber)) %>
							<%@ include file="fragments/Transaction-GUAR-ISS-ExternalBank.frag" %>
							</div>
						
						<%
							} 
						%>										        
					 <%!
					 	boolean getExternalBankUsed(String op_bank_org_oid){
				        	
				         	try{
				         		   //jgadela  R90 IR T36000026319 - SQL FIX
				         	        Object[] sqlParamsCnt1 = new Object[2];
				         	        sqlParamsCnt1[0] = "Y";
				         	        sqlParamsCnt1[1] = op_bank_org_oid;
				                    return DatabaseQueryBean.getCount("organization_oid","operational_bank_org"," external_bank_ind = ? and organization_oid = ?", true, sqlParamsCnt1) != 0;


				         			} catch (Exception e) {
				         	            e.printStackTrace();
				         	      } finally {
				         	      }

						 	return false;
					 	}
					 %>
					</div> <%-- formContentt Area closes here --%>
					</div><%--formArea--%>
				    
					<div class="formSidebar" data-dojo-type="widget.FormSidebar" data-dojo-props="form: 'TransactionGUAR'">
						    <jsp:include page="/common/Sidebar.jsp">
								<jsp:param name="links" value="<%=links%>" />
								<jsp:param name="isReadOnly" value="<%=isReadOnly%>" />
								<jsp:param name="certAuthURL" value="<%=certAuthURL%>" />
								<jsp:param name="buttonPressed" value="<%=buttonPressed%>" />
								<jsp:param name="error" value="<%= error%>" />
								<jsp:param name="formName" value="0" />
								<jsp:param name="isTemplate" value="<%=isTemplate%>"/>
								<jsp:param name="showLinks" value="true" />  
							    <jsp:param name="showApplnForm" value="<%=canDisplayPDFLinks%>"/>
								<jsp:param name="isNewTransaction" value="<%=newTransaction%>"/>
							</jsp:include>
					</div>  <%--closes sidebar area--%>						
				     <%--Commenting below code as it is repeting IR T36000016662 RPasupulati   				    
					<%
					  if (rejectionIndicator.equals(TradePortalConstants.INDICATOR_YES)
					  	|| rejectionIndicator.equals(TradePortalConstants.INDICATOR_X))
					  {
					%>
					     <%@ include file="fragments/Transaction-RejectionReason.frag" %>
					<%
					  }
					---%>
					
				    <%//@ include file="fragments/Transaction-Footer.frag" %>
				
				    <%@ include file="fragments/PhraseLookupPrep.frag" %>
				    <%@ include file="fragments/PartySearchPrep.frag" %>

					<%
						Debug.debug("*********************** IN THE ISSUE FILE ****************************");
						Debug.debug("*** This is the Terms_Oid: " + terms.getAttribute("terms_oid"));
						Debug.debug("*** This is the Transaction_oid: " + transaction.getAttribute("transaction_oid"));
						Debug.debug("*** This is the Terms Party 1 oid: " + terms.getAttribute("c_FirstTermsParty"));
						Debug.debug("*** This is the Terms Party 2 oid: " + terms.getAttribute("c_SecondTermsParty"));
						Debug.debug("*** This is the Terms Party 3 oid: " + terms.getAttribute("c_ThirdTermsParty"));
						Debug.debug("*** This is the Terms Party 4 oid: " + terms.getAttribute("c_FourthTermsParty"));
					    Debug.debug("***************************** END GUARANTEE ISSUE PAGE ************************");
					%>
<input type="hidden" name="selection" />
<input type="hidden" name="ChoosePrimary" />

					<%= formMgr.getFormInstanceAsInputField("Transaction-GUAR-Form", secureParms) %>
					<div id="PartySearchDialog"></div>
			</form>
			<%  //include a hidden form for new instrument submission
			//this is used for all of the new instrument types available from the menu
			Hashtable newInstrSecParms = new Hashtable();
			newInstrSecParms.put("UserOid", userSession.getUserOid());
			newInstrSecParms.put("SecurityRights", userSession.getSecurityRights());
			newInstrSecParms.put("clientBankOid", userSession.getClientBankOid());
			newInstrSecParms.put("ownerOrg", userSession.getOwnerOrgOid());
			%>
			<form method="post" name="NewInstrumentForm1" action="<%=formMgr.getSubmitAction(response)%>">
			<input type="hidden" name="bankBranch" />
			<input type="hidden" name="transactionType"/>
			<input type="hidden" name="instrumentType" />
			<input type="hidden" name="copyInstrumentOid" />
			<input type="hidden" name="mode" value="CREATE_NEW_INSTRUMENT" />
			<input type="hidden" name="copyType" value="Instr" />
			
			<%= formMgr.getFormInstanceAsInputField("NewInstrumentForm",newInstrSecParms) %>
			</form>
			<%
			Hashtable newTempSecParms = new Hashtable();
			newTempSecParms.put("userOid", userSession.getUserOid());
			newTempSecParms.put("securityRights", userSession.getSecurityRights());
			newTempSecParms.put("clientBankOid", userSession.getClientBankOid());
			newTempSecParms.put("ownerOrg", userSession.getOwnerOrgOid());
			newTempSecParms.put("ownerLevel", userSession.getOwnershipLevel());
			%>
			<form method="post" name="NewTemplateForm1" action="<%=formMgr.getSubmitAction(response)%>">
			<input type="hidden" name="name" />
			<input type="hidden" name="bankBranch" />
			<input type="hidden" name="transactionType"/>
			<input type="hidden" name="InstrumentType" />
			<input type="hidden" name="copyInstrumentOid" />
			<input type="hidden" name="mode"  />
			<input type="hidden" name="CopyType" />
			<input type="hidden" name="expressFlag"  />
			<input type="hidden" name="fixedFlag" />
			<input type="hidden" name="PaymentTemplGrp" />
			<input type="hidden" name="validationState" />
			
			<%= formMgr.getFormInstanceAsInputField("NewTemplateForm",newTempSecParms) %>
			</form>
		</div> <%-- pageContentt Area closes here --%>
	</div>

<jsp:include page="/common/Footer.jsp">
   <jsp:param name="autoSaveFlag"           value="<%=autoSaveFlag%>" />
   <jsp:param name="templateFlag"           value="<%=templateFlag%>" />
   <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>
<jsp:include page="/common/SidebarFooter.jsp"/>

<script>

  <%-- cquinton 3/3/2013 add local var --%>
  var local = {};

<%
  if (!isReadOnly && InstrumentServices.isNotBlank(focusField)) {
%>
	
function setOtherDate() {
	dijit.getEnclosingWidget(document.forms[0].otherDate).set('checked',true);

}
function setValidityDate() {
	dijit.getEnclosingWidget(document.forms[0].validEndDate).set('checked',true);

}
function setOtherExpiryDate(){
	dijit.byId("expiry_date").reset();
	dijit.getEnclosingWidget(document.forms[0].otherExpiryDate).set('checked',true);	
}
function setOtherInDelivery(){
	dijit.getEnclosingWidget(document.forms[0].otherInDelivery).set('checked',true);	
}

function setGuarIssueOverseasBank() {
	dijit.getEnclosingWidget(document.forms[0].guarIssueOverseasBank).set('checked',true);
}

function resetFeeOid(row){
	  require(["dijit/registry", "dojo/ready"],
			    function(registry, ready ) {
			      ready(function() {

	var chargeType = registry.byId(('ChrgIncChargeType'+(row))).get('value');
	var currCode = registry.byId(('ChrgIncCurrencyCode'+(row))).get('value');
	var amt = registry.byId(('ChrgIncAmount'+(row))).get('value');
	if(chargeType == "" & currCode =="" & amt == ""){
		registry.byId(('FeeOid'+(row))).set('value',"");
	}
			      });
	  });
	 
}
function setOverSeasValidDate(){
	 dijit.byId("overseas_validity_date").reset();
}
  require(["dijit/registry", "dojo/ready"],
    function(registry, ready ) {
      ready(function() {
        var focusFieldId = '<%= focusField %>';
        if ( focusFieldId ) {
          var focusField = registry.byId(focusFieldId);
          focusField.focus();
        }
      });
  });
  
  <%--  require(["dojo/ready"], function(ready){
	    ready(function(){
	    	SetMsg(document.getElementById('tender_order_contract_details', false));
	    });
	});  --%>
<%
  }
%>
</script>

</body>
</html>

<%
   // Finally, reset the cached document to eliminate carryover of
   // information to the next visit of this page.
   doc.removeAllChildren("/");
   formMgr.storeInDocCache("default.doc", doc);
%>
<script type="text/javascript">

  var itemid;
  var section;
  require(["dojo/_base/array", "dojo/dom",
           "dojo/dom-construct", "dojo/dom-class", 
           "dojo/dom-style", "dojo/query",
           "dijit/registry", "dojo/on", 
           "t360/common",
           "dojo/ready", "dojo/NodeList-traverse"], function(arrayUtil, dom,
                   domConstruct, domClass,
                   domStyle, query,
                   registry, on, 
                   common, 
                   ready){
		ready(function(){
			var add3moreChargesIncurred = function(cnt){
				var myTable = dojo.byId('chrgIncTable');
				<%-- Leelavathi IR#T36000011878 Rel-8.2 18/2/2013 Begin --%>
				var rowIndex = myTable.rows.length - 1;
				common.appendAjaxTableRows(myTable,"chrgIncIndex",
			    		  "/portal/transactions/fragments/Add3moreChrgIncRows.jsp?chrgIncIndex="+rowIndex+"&count="+cnt+"&isTemplate=<%=isTemplate%>&isExpressTemplate=<%=isExpressTemplate%>"); <%-- table length includes header, so subtract 1 --%>
			    		  
			      document.getElementById("noOfChrgIncRows").value = eval(rowIndex+cnt);   		  
			};
			
			<% if (!(isReadOnly)) 
			{     %>
	on(registry.byId("add3moreChargesIncurred"),"click",function(){ 
		add3moreChargesIncurred(3); } );


		var deleteChrgIncRows = function (rowId){

		}
	    query('#chrgIncTable').on(".deleteSelectedItem:click", function(evt) {
		      thisPage.deleteEventHandler(this, document.getElementById("chrgIncTable"));

		    });
		<%}%>	    
        var thisPage = {
       		 deleteEventHandler: function(deleteButton, tableID) {
 		        <%-- uncheck the available item --%>
 		        <%-- this automatically does the onchange mechanism there --%>
 		        <%-- including removing this item and decreasing the total count --%>
 		       <%--  var deleteSelectItem = deleteButton.parentNode; --%>
 		       
 		        var deleteSelectItemId = deleteButton.id;
 		       var rowIndex = deleteSelectItemId.substring(13,deleteSelectItemId.length);
 		        var row = document.getElementById('chrgIncIndex' + rowIndex);
 		        
 		        deleteOrderField(row, tableID);	
 		      }

        }

  	  function deleteOrderField(deleteRow, table){
  		  if(deleteRow){
  		var tabSize = (table.rows.length)- (2); <%--  two row for header --%>
  		var replaceRow = deleteRow.rowIndex;
  		<%-- for(var row= replaceRow; row <= tabSize; row++){ --%>
  		<%-- 	table.rows[ row ].id = table.rows[ row + 1 ].id; --%>
  		<%-- 	table.rows[ row ].cells[0].firstElementChild.value = table.rows[ row+1 ].cells[0].firstElementChild.value; --%>
  		<%-- 	table.rows[ row ].cells[1].innerHtml = table.rows[ row+1 ].cells[1].innerHtml; --%>
  		<%-- 	table.rows[ row ].cells[1].firstElementChild.innerText = table.rows[ row+1 ].cells[1].firstElementChild.innerText; --%>
  		<%-- 	table.rows[ row ].cells[1].childNodes[3].id = table.rows[ row+1 ].cells[1].childNodes[3].id; --%>
  		<%-- } --%>
  		for(var row= replaceRow; row <= tabSize; row++){
   			document.getElementById(('ChrgIncChargeType'+(row-1))).value=document.getElementById(('ChrgIncChargeType'+(row))).value;
   			document.getElementById(('ChrgIncCurrencyCode'+(row-1))).value=document.getElementById(('ChrgIncCurrencyCode'+(row))).value;
   			document.getElementById(('ChrgIncAmount'+(row-1))).value=document.getElementById(('ChrgIncAmount'+(row))).value;
   			document.getElementById(('FeeOid'+(row-1))).value=document.getElementById(('FeeOid'+(row))).value;

   			registry.byId(('ChrgIncChargeType'+(row-1))).set('value',registry.byId(('ChrgIncChargeType'+(row))).get('value'));
   			registry.byId(('ChrgIncCurrencyCode'+(row-1))).set('value',registry.byId(('ChrgIncCurrencyCode'+(row))).get('value'));
   			registry.byId(('ChrgIncAmount'+(row-1))).set('value',registry.byId(('ChrgIncAmount'+(row))).get('value'));
   			registry.byId(('FeeOid'+(row-1))).set('value',registry.byId(('FeeOid'+(row))).get('value'));

  		}

  			table.deleteRow(tabSize+1);
  	 		var idNumberToDestroy = (table.rows.length) - (1);
  	  		
  	  		
  	  		var chargeType = dijit.byId('ChrgIncChargeType' + idNumberToDestroy);
  	  		var currCode = dijit.byId('ChrgIncCurrencyCode' + idNumberToDestroy);
  	  		var amt = dijit.byId('ChrgIncAmount' + idNumberToDestroy);	
  	  		var feeOid = dijit.byId('FeeOid' + idNumberToDestroy);
  	  		if (feeOid){
  	  			dijit.byId('FeeOid' + idNumberToDestroy).destroyRecursive();
  	  		}else{
  	  			document.getElementById(('FeeOid'+(table.rows.length - 1))).value="";
  	  		}
  	  		dijit.byId('ChrgIncChargeType' + idNumberToDestroy).destroyRecursive();
  	  		dijit.byId('ChrgIncCurrencyCode' + idNumberToDestroy).destroyRecursive();
  	  		dijit.byId('ChrgIncAmount' + idNumberToDestroy).destroyRecursive();
  			if (table.rows.length ==1){
  				add3moreChargesIncurred(3);
  			}
  		
  		
  		
  		  }
  	  }

  });
  });	
  

  function SearchParty(identifierStr, sectionName ,partyType){
	
    itemid = String(identifierStr);
    section = String(sectionName);
    partyType=String(partyType);
    <%-- itemid = 'OrderingPartyName,OrderingPartyAddress1,OrderingPartyAddress2,OrderingPartyAddress3,OrderingPartyAddress4,OrderingPartyCountry'; --%>

    require(["dijit/registry", "dojo/ready"],
        function(registry, ready ) {

      ready(function() {
        var focusField = registry.byId("beneficiary_name");
        if (partyType == "APP") {
            focusField = registry.byId("reference_number");        
        }
        if (partyType == "AGT") {
            focusField = registry.byId("agent_name");        
        }
        if (partyType == "OSB") {
            focusField = registry.byId("bank_name");        
        }
        focusField.focus();
        
	<%-- cquinton 10/16/2013 Rel 8.3 ir#21872 - move disabling of ucp fields to widget creation --%>
      });
    });

    require(["dojo/dom", "t360/dialog"], function(dom, dialog ) {

      <%-- cquinton 2/7/2013 --%>
      <%-- set the SearchPartyType input so it is included on new party form submit --%>
      var searchPartyTypeInput = dom.byId("SearchPartyType");
      if ( searchPartyTypeInput ) {
        searchPartyTypeInput.value=partyType;
      }

      dialog.open('PartySearchDialog', '<%=PartySearchAddressTitle%>',
        'PartySearch.jsp',
        ['returnAction','filterText','partyType','unicodeIndicator','itemid','section'],
        ['selectTransactionParty','',partyType,'<%=TradePortalConstants.INDICATOR_NO%>',itemid,section]); <%-- parameters --%>
    });
  }

var TextMessage = 'Enter details of the Tender / Order / Contract ';
function SetMsg (txt, active) {
    if (txt == null) return;
    
    if (active) {
        if (txt.value == TextMessage) txt.value = '';
    } else {
        if (txt.value == '') txt.value = TextMessage;
    }
}

function clearText() {
	document.getElementById("guar_bank_standard_text").value = "";
	var opt=dijit.byId("bankStandardTextPhraseDropDown");
	opt.reset();
	opt.setDisplayedValue("<Select a Phrase>");
}

var TextMessage = 'Enter details of the Tender / Order / Contract ';
function SetMsg (txt, active) {
if (txt == null) return;

if (active) {
    if (txt.value == TextMessage) txt.value = '';
} else {
    if (txt.value == '') txt.value = TextMessage;
}
}
function openCopySelectedDialogHelper(callBackFunction){
    require(["t360/dialog"], function(dialog) {
          dialog.open('copySelectedInstrument', '<%=resMgr.getTextEscapedJS("CopySelected.Title",TradePortalConstants.TEXT_BUNDLE) %>',
                    'copySelectedInstrumentDialog.jsp',
                    "instrumentType", '<%=instrumentType%>', <%-- parameters --%>
                    'select', callBackFunction);
        	  
      });
  }
function copySelectedToInstrument(bankBranchOid, copyType, templateName, templateGroup, flagExpressFixed){
	var rowKeys ;
	 require(["dijit/registry", "dojo/query", "dojo/on", "t360/dialog", "dojo/domReady!"],
		        function(registry, query, on, dialog ) {
		      <%--get array of rowkeys from the grid--%>
		      rowKeys = getSelectedGridRowKeys("instrumentsSearchGridId");
	 });
	if(copyType == 'I'){ 
		if ( document.getElementsByName('NewInstrumentForm1').length > 0 ) {
		    var theForm = document.getElementsByName('NewInstrumentForm1')[0];
		    theForm.bankBranch.value=bankBranchOid;
		    theForm.copyInstrumentOid.value=rowKeys;
		    theForm.submit();
		  }
	}	
	if(copyType == 'T'){ 
		if ( document.getElementsByName('NewTemplateForm1').length > 0 ) {
			var flag = flagExpressFixed.split('/');
			var expressFlag = flag[0];
			var fixedFlag = flag[1];
			var theForm = document.getElementsByName('NewTemplateForm1')[0];
		    theForm.name.value=templateName;
		    theForm.mode.value="<%=TradePortalConstants.NEW_TEMPLATE%>";
		    theForm.CopyType.value="<%=TradePortalConstants.FROM_INSTR%>";
		    theForm.fixedFlag.value=fixedFlag;
		    theForm.expressFlag.value=expressFlag;
		    theForm.PaymentTemplGrp.value=templateGroup;
		    theForm.copyInstrumentOid.value=rowKeys;
		    theForm.validationState.value = "";
		    theForm.submit();
		  }
	}
}

  <%-- cquinton 3/2/2013 local phrase lookup --%>
  require(["t360/phraseLookup"],
          function( phraseLookup ) {
    local.phraseLookupAppend = function(phraseSelect, textArea, textAreaMaxLength ) {
      phraseLookup.appendPhrase( phraseSelect, textArea, textAreaMaxLength, "_errorSection" );
    };
    local.phraseLookupReplace = function(phraseSelect, textArea, textAreaMaxLength ) {
      phraseLookup.replacePhrase( phraseSelect, textArea, textAreaMaxLength, "_errorSection" );
    };
  });

	<%-- Sandeep - Rel-8.3 CR-752 Dev 06/12/2013 - Begin --%>
<%-- cquinton 10/16/2013 ir#21872 Rel 8.3 move select ucp checkbox function to part of changeUCPVersion below --%>
  	
  	<%-- When ICC Publication check box has been un-selected then clere the version and details fields under it --%>
  	function clearICCPublication(selectField) {
	   	require(["dijit/registry"], function(registry) {
	   		if(!registry.byId(selectField).checked){
	   			registry.byId("UCPVersion").set("value",""); <%-- this invokes changeUCPVersion below too! --%>
		   		registry.byId("UCPDetails").set("value","");
	   		}
	    });
	}
  	
<%-- cquinton 10/16/2013 ir#21872 Rel 8.3 start - 
     changeUCPVersion both sets the ucp checkbox field and disables/enables details --%>
  	function changeUCPVersion(){
  		require(["dijit/registry"], function(registry) {
	  		var ucpVersion = registry.byId('UCPVersion').value;

                        if ( ucpVersion && ucpVersion!="" ) { <%-- if called by clearICCPublication above --%>
                            registry.byId('UCPVersionDetailInd').set('checked', true);
			}
			
			if(ucpVersion == '<%=TradePortalConstants.GUA_OTHER%>') {
				registry.byId('UCPDetails').setDisabled(false);
			}else{
				registry.byId('UCPDetails').setDisabled(true);
				registry.byId('UCPDetails').set('value', '');
			}
  		});	
  	}
<%-- cquinton 10/16/2013 ir#21872 Rel 8.3 end --%>
	<%-- Sandeep - Rel-8.3 CR-752 Dev 06/12/2013 - End  --%>
	
 <%-- moved from frag to jsp.	 --%>
 function maxNumDisable(){
 require(["dijit/registry"], function(registry) {
  var autoExtensionIndicator = registry.byId("AutoExtensionIndicator");
  if(autoExtensionIndicator){
 	 autoExtensionIndicator.on("change", function(checkValue) {
	          if ( checkValue ) {
	        	  registry.byId("MaxAutoExtensionAllowed").setDisabled(false);	  
	        	  registry.byId("AutoExtensionPeriod").setDisabled(false);
	        	  registry.byId("FinalExpiryDate").setDisabled(false);
	        	  registry.byId("NotifyBeneDays").setDisabled(false);
	        	  
	          } else {
	        	  registry.byId('MaxAutoExtensionAllowed').set('value','');
	        	  registry.byId('AutoExtensionPeriod').set('value','');
	        	  registry.byId('AutoExtensionDays').set('value','');
	        	  registry.byId('FinalExpiryDate').set('DisplayedValue','');
	        	  registry.byId('NotifyBeneDays').set('value','');
	        	  registry.byId("MaxAutoExtensionAllowed").setDisabled(true);	  
	        	  registry.byId("AutoExtensionPeriod").setDisabled(true);
	        	  registry.byId("AutoExtensionDays").setDisabled(true);
	        	  registry.byId("FinalExpiryDate").setDisabled(true);
	        	  registry.byId("NotifyBeneDays").setDisabled(true);
	          }
	        });
  }    
 });

}

 function numDaysDisable(){
	  require(["dijit/registry"], function(registry) {
		     var autoExtendPeriod = registry.byId("AutoExtensionPeriod");
		     if(autoExtendPeriod){
		       if(autoExtendPeriod.value == 'DAY'){
		    	  registry.byId("AutoExtensionDays").setDisabled(false);
		       } else {
		    	 registry.byId('AutoExtensionDays').set('value','');
		    	 registry.byId("AutoExtensionDays").setDisabled(true); 
		       }
		     }
  });
 }

 function extensionType(type){
	require(["dijit/registry"], function(registry) {
	if(registry.byId("AutoExtensionIndicator").checked){
	 if(type == 'FinalExpiryDate') {
		 var finalExpiryDate = registry.byId('FinalExpiryDate').value;
		 if (finalExpiryDate=="" ||  finalExpiryDate==null) { 
			 registry.byId("MaxAutoExtensionAllowed").setDisabled(false);
       }else{
      	 registry.byId("MaxAutoExtensionAllowed").setDisabled(true);
       }  
	 }else if(type == 'MaxAutoExtensionAllowed'){
		 var maxAutoExtensionAllowed = registry.byId('MaxAutoExtensionAllowed').value;
		 if (isNaN(maxAutoExtensionAllowed)) { 
			 registry.byId("FinalExpiryDate").setDisabled(false);
       }else{
       	 registry.byId("FinalExpiryDate").setDisabled(true);
       } 
	 }
	}
	});
 }
</script>
