<%--
*******************************************************************************
                              Approvalto Pay Amend Page

  Description:
*******************************************************************************
--%>
    
<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.html.*,java.math.*,
                 com.amsinc.ecsg.util.DateTimeUtility,java.util.Vector, java.util.Hashtable, java.util.Date" %>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager"
    scope="session">
</jsp:useBean>   
  
<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" 
   scope="session">
</jsp:useBean>


<%-- ************** Data retrieval page setup begins here ****************  --%>

<%
  WidgetFactory widgetFactory = new WidgetFactory(resMgr,userSession);
  //cquinton 2/12/2013 add refdata manager for datepattern lookup
  ReferenceDataManager refData = ReferenceDataManager.getRefDataMgr();

  String focusField = "TransactionAmount";   // Default focus field

  // Various oid and status info from transaction and instruments used in
  // several places.
  String instrumentOid = "";         
  String transactionOid;
  String instrumentType;
  String instrumentStatus;
  String transactionType; 
  String transactionStatus;
  String invoiceDueDay = "";
  String invoiceDueMonth = "";
  String invoiceDueYear = "";
  String bButtonPressed = ""; 
  String links = "";
	  

  boolean           amountUpdatedFromPOs          = false;

  boolean getDataFromDoc;          // Indicates if data is retrieved from the
                                   // input doc cache or from the database

  DocumentHandler doc;

  String loginLocale = userSession.getUserLocale();
  String loginRights = userSession.getSecurityRights();

  //cquinton 2/12/2013 lookup locale specific datePattern description from refData manager
  String datePattern = userSession.getDatePattern();
  String datePatternDisplay = refData.getDescr("DATEPATTERN", datePattern, loginLocale);
  String dateWidgetOptions = "placeHolder:'" + datePatternDisplay + "'";

  // Applicant name and expiry date from the issue transaction.  Displayed in General section
  String applicantName = "";
  String currentExpiryDate = null;

  // These are local variables used in the Shipment section for the PO line item
  // buttons and text area box
  DocumentHandler   poLineItemsDoc                = null;
  DocumentHandler   poLineItemDoc                 = null;
  StringBuffer      sqlQuery                      = null;
  StringBuffer      link                          = null;
  boolean           hasPOLineItems                = false;
  String            poLineItemUploadDefinitionOid = "";
  String            userOrgAutoLCCreateIndicator  = null;
  String            userCustomerAccessIndicator   = null;
  String            poLineItemBeneficiaryName     = "";
  String            poLineItemCurrency            = "";
  boolean 		  isProcessedByBank		  = false;
  Vector            poLineItems                   = null;
  String			rejectionIndicator            = "";
  String			rejectionReasonText	          = "";
  
  //TAILE
  String            userOrgAutoATPCreateIndicator  = null;
  //TAILE

  // Dates are stored in the database as one field but displayed on the
  // page as 3.  These variables hold the pieces for each date.
  String expiryDay = "";
  String expiryMonth = "";
  String expiryYear = "";
  String shipDay = "";
  String shipMonth = "";
  String shipYear = "";
  String invoiceDetails ="";//SHR PR CR708 
  String invoiceOnlyInd ="";//SHR PR CR708
  String invoiceDueDate ="";//SHR PR CR708
  // Variables used for populating ref data dropdowns.
  String options;
  String defaultText;

  // This array of string is used when creating the LC Amendment Application Form links.
  String linkArgs[] = {"","","",""};

  // These are the beans used on the page.

  TransactionWebBean transaction  = (TransactionWebBean)
                                      beanMgr.getBean("Transaction");
  InstrumentWebBean instrument    = (InstrumentWebBean)
                                      beanMgr.getBean("Instrument");
  TemplateWebBean template        = (TemplateWebBean)
                                      beanMgr.getBean("Template");
  TermsWebBean terms              = null;

  ShipmentTermsWebBean shipmentTerms = beanMgr.createBean(ShipmentTermsWebBean.class, "ShipmentTerms");

  // We need the amount of the original transaction to display.  Create a webbean
  // to hold this data.
  TransactionWebBean activeTransaction = beanMgr.createBean(TransactionWebBean.class, "Transaction");

  // Get the document from the cache.  We'll may use it to repopulate the 
  // screen if returning from another page, a save, validation, or any other
  // mediator called from this page.  Otherwise, we assume an instrument oid
  // and transaction oid was passed in.

  doc = formMgr.getFromDocCache();
  bButtonPressed = doc.getAttribute("/In/Update/ButtonPressed");
  
  Vector error = null;
  error = doc.getFragments("/Error/errorlist/error");
  
//if newTransaction, set session variable so it persists
  String newTransaction = null;
  if (doc.getDocumentNode("/Out/newTransaction")!=null) {
    newTransaction = doc.getAttribute("/Out/newTransaction");
    session.setAttribute("newTransaction", newTransaction);
    
  } 
  else {
    newTransaction = (String) session.getAttribute("newTransaction");
    if ( newTransaction==null ) {
      newTransaction = TradePortalConstants.INDICATOR_NO;
    }
    
  }


  //Debug.debug("doc from cache is " + doc.toString());

/******************************************************************************
  We are either entering the page or returning from somewhere.  These are the 
  conditions for how to populate the web beans.  Data comes from either the
  database or the doc cache (/In section) with some variation.

  Mode           Condition                      Populate Beans From
  -------------  ----------------------------   --------------------------------
  Enter Page     no /In/Transaction in doc      Instrument and Template web
                                                beans already populated, get
                                                data for Terms and TermsParty
                                                web beans from database

  return from    /In/NewPartyFromSearchInfo/PartyOid   doc cache (/In); also use Party
  Party Search     exists                       SearchInfo to lookup, and 
                                                populate a specific TermsParty
                                                web bean

  return from    /Out/PhraseLookupInfo exists   doc cache (/In); but use Phrase
  Phrase Lookup                                 LookupInfo text (from /Out) to
                                                replace a specific phrase text
                                                in the /In document before 
                                                populating

  return from    /Error/maxerrorseverity < 1    Same as Enter Page (data is
  Transaction                                   retrieved from database)
    mediator
    (no error)

  return from    /Error/maxerrorseverity > 0    doc cache (/In)
  Transaction    
    mediator
    (error)
******************************************************************************/

  // Assume we get the data from the doc.
  
  // IR-RSUH083142989 Krishna Begin 10/03/2007
  // IREGUH071657459 of IMP_DLC & FUH011755336 are taken care.Reverse the dealut value
  // TLE - 01/18/07 IR-FUH011755336 - Add Begin
     getDataFromDoc = true;  //Uncommented for IR-RSUH083142989
  // getDataFromDoc = false; //commented for IR-RSUH083142989
  // TLE - 01/18/07 IR-FUH011755336 - Add End
  // IR-RSUH083142989 Krishna End 10/03/2007

  String maxError = doc.getAttribute("/Error/maxerrorseverity");
  if (maxError != null && maxError.compareTo(
              String.valueOf(ErrorManager.WARNING_ERROR_SEVERITY)) < 0) {
     // No errors, so don't get the data from doc.
     getDataFromDoc = false;
  }
  //ir cnuk113043991 - check to see if transaction needs to be refreshed
  // if so, refresh it and do not get data from doc as it is wrong
  if ( "Y".equals(doc.getAttribute("/Out/Refresh/Transaction")) ) {
     transaction.getDataFromAppServer();
     getDataFromDoc = false;
  }
  if (doc.getDocumentNode("/In/NewPartyFromSearchInfo/Type") != null) {
     // We have returned from the party search page.  Get data from doc cache
     getDataFromDoc = true;
  }
  if (doc.getDocumentNode("/Out/ChangePOs") != null) {
      
	    invoiceDueDay = doc.getAttribute("/In/Terms/invoice_due_day");
        invoiceDueMonth = doc.getAttribute("/In/Terms/invoice_due_month");
        invoiceDueYear = doc.getAttribute("/In/Terms/invoice_due_year");
	  
     // Some data was updated by the add/remove POs process.  
     // Update the data in the XML so that the page will relect the changes   
     doc.setAttribute("/In/Terms/ShipmentTermsList/po_line_items",doc.getAttribute("/Out/ChangePOs/po_line_items"));
     doc.setAttribute("/In/Terms/amount", doc.getAttribute("/Out/ChangePOs/amount"));
     doc.setAttribute("/In/Terms/ShipmentTermsList/goods_description", doc.getAttribute("/Out/ChangePOs/goods_description"));

     //pmitnala 3/05/2013 Changes Begin for IR-T36000014541, PR BMO Issue-123 - Expiry Date or the Latest Ship Date were not derived from uploaded PO
     doc.setAttribute("/In/Terms/expiry_date", doc.getAttribute("/Out/ChangePOs/expiry_date"));
     doc.setAttribute("/In/Terms/ShipmentTermsList/latest_shipment_date", doc.getAttribute("/Out/ChangePOs/latest_shipment_date"));
     doc.setAttribute("/In/Terms/invoice_due_date", doc.getAttribute("/Out/ChangePOs/invoice_due_date"));
     //pmitnala 3/05/2013 Changes End for IR-T36000014541	 
	 
     doc.setAttribute("/In/Terms/invoice_due_date",doc.getAttribute("/Out/ChangePOs/invoice_due_date"));
     doc.setAttribute("/In/Terms/invoice_details",doc.getAttribute("/Out/ChangePOs/invoice_details"));//SHR PR CR708 
     doc.setAttribute("/In/Terms/FirstTermsParty/name",doc.getAttribute("/Out/ChangePOs/name"));//SHR PR CR708 
     doc.setAttribute("/In/Terms/invoice_only_ind",doc.getAttribute("/Out/ChangePOs/invoice_only_ind"));//SHR PR CR708 
     getDataFromDoc = true;
     amountUpdatedFromPOs = true;
   }
  if (doc.getDocumentNode("/In/Transaction") == null) {
     // No /In/Transaction means we've never looked up the data.
     Debug.debug("No /In/Transaction section - get data from database");
     getDataFromDoc = false;
  }
  if (doc.getDocumentNode("/Out/PhraseLookupInfo") != null) {
     // A Looked up phrase exists.  Replace it in the /In document.
     Debug.debug("Found a looked-up phrase");
     getDataFromDoc = true;

     // Take the looked up and appended phrase text from the /Out section
     // and copy it to the /In section.  This allows the beans to be 
     // properly populated.
     String xmlPath = doc.getAttribute("/In/PhraseLookupInfo/text_path");
     xmlPath = "/In" + xmlPath;

     doc.setAttribute(xmlPath, 
                      doc.getAttribute("/Out/PhraseLookupInfo/NewText"));

     // If we returned from the phrase lookup without errors, set the focus
     // to the correct field.  Otherwise, don't set the focus so the user can
     // see the error.
     if (maxError != null && maxError.compareTo(
              String.valueOf(ErrorManager.WARNING_ERROR_SEVERITY)) < 0) {
       focusField = doc.getAttribute("/In/PhraseLookupInfo/text_field_name");
     }
  }

  if (getDataFromDoc) {
     Debug.debug("Populating beans from doc cache");

     // Populate the beans from the input doc.
     try {
        instrument.populateFromXmlDoc(doc.getComponent("/In"));
        transaction.populateFromXmlDoc(doc.getComponent("/In"));
        template.populateFromXmlDoc(doc.getComponent("/In"));

        terms = (TermsWebBean) beanMgr.getBean("Terms");
        terms.populateFromXmlDoc(doc, "/In");

        expiryDay = doc.getAttribute("/In/Terms/expiry_day");
        expiryMonth = doc.getAttribute("/In/Terms/expiry_month");
        expiryYear = doc.getAttribute("/In/Terms/expiry_year");

        shipDay = doc.getAttribute("/In/Terms/shipment_day");
        shipMonth = doc.getAttribute("/In/Terms/shipment_month");
        shipYear = doc.getAttribute("/In/Terms/shipment_year");
		
		invoiceDueDay = doc.getAttribute("/In/Terms/invoice_due_day");
        invoiceDueMonth = doc.getAttribute("/In/Terms/invoice_due_month");
        invoiceDueYear = doc.getAttribute("/In/Terms/invoice_due_year");
      	//SHR PR CR708 Begin
        invoiceDetails = doc.getAttribute("/In/Terms/invoice_details");
        invoiceOnlyInd = doc.getAttribute("/In/Terms/invoice_only_ind");
        invoiceDueDate = doc.getAttribute("/In/Terms/invoice_due_date");
        //SHR PR CR708 End

        String activeTransOid = instrument.getAttribute("active_transaction_oid");
        if (!InstrumentServices.isBlank(activeTransOid)) {
           activeTransaction.setAttribute("transaction_oid", activeTransOid);
           activeTransaction.getDataFromAppServer();
        }

        // Some of the data for this transaction is stored as part of the shipment terms
        terms.populateFirstShipmentFromXml(doc);
     } catch (Exception e) {
        out.println("Contact Administrator: "
              + "Unable to reload data after returning to page. "
              + "Error is " + e.toString());
     }
  } else {
     Debug.debug("populating beans from database");
     // We will perform a retrieval from the database.
     // Instrument and Transaction were already retrieved.  Get
     // the rest of the data

     terms = transaction.registerCustomerEnteredTerms();

        String activeTransOid = instrument.getAttribute("active_transaction_oid");
        if (!InstrumentServices.isBlank(activeTransOid)) {
           activeTransaction.setAttribute("transaction_oid", activeTransOid);
           activeTransaction.getDataFromAppServer();
        }


     // Because date fields are represented as three fields on the page,
     // we need to parse out the date values into three fields.  Pre-mediator
     // logic puts these three fields back together for the terms bean.
     String date = terms.getAttribute("expiry_date");
     expiryDay = TPDateTimeUtility.parseDayFromDate(date);
     expiryMonth = TPDateTimeUtility.parseMonthFromDate(date);
     expiryYear = TPDateTimeUtility.parseYearFromDate(date);

     // Some of the data for this transaction is stored as part of the shipment terms
     terms.populateFirstShipmentFromDatabase();

     date = terms.getFirstShipment().getAttribute("latest_shipment_date");
     shipDay = TPDateTimeUtility.parseDayFromDate(date);
     shipMonth = TPDateTimeUtility.parseMonthFromDate(date);
     shipYear = TPDateTimeUtility.parseYearFromDate(date);
	 
	 date = terms.getAttribute("invoice_due_date");
     invoiceDueDay = TPDateTimeUtility.parseDayFromDate(date);
     invoiceDueMonth = TPDateTimeUtility.parseMonthFromDate(date);
     invoiceDueYear = TPDateTimeUtility.parseYearFromDate(date);
     invoiceDetails = terms.getAttribute("invoice_details");//SHR CR708 Rel8.1.1
     invoiceOnlyInd = terms.getAttribute("invoice_only_ind");//SHR CR708 Rel8.1.1
     invoiceDueDate = terms.getAttribute("invoice_due_date");
  }

  // Now determine the mode for how the page operates (readonly, etc.)
  BigInteger requestedSecurityRight = SecurityAccess.APPROVAL_TO_PAY_CREATE_MODIFY;
%>
<%@ include file="fragments/Transaction-PageMode.frag" %>  
<%
  // Now we need to calculate the computed new lc amount total.  This involves 
  // using the instument amount of the active transaction, the amount entered by the user,
  // performing some validation and then doing the calculation.

  MediatorServices medService = new MediatorServices();
  medService.getErrorManager().setLocaleName(resMgr.getResourceLocale());
  BigDecimal originalAmount = BigDecimal.ZERO;
  BigDecimal offsetAmount = BigDecimal.ZERO;
  BigDecimal calculatedTotal = BigDecimal.ZERO;
  String amountValue = null;
  String displayAmount;
  String incrDecrValue = TradePortalConstants.INCREASE;
  String currencyCode = activeTransaction.getAttribute("copy_of_currency_code");
  boolean badAmountFound = false;
  String newAmountValue = null;  // DK IR-RAUM051653353 Rel8.1.1 09/25/2012
  BigDecimal newAmount = BigDecimal.ZERO;  // DK IR-RAUM051653353 Rel8.1.1 09/25/2012
  // Convert the original transaction amount to a double
  try {
     amountValue = activeTransaction.getAttribute("instrument_amount");
     originalAmount = new BigDecimal(amountValue);
     newAmountValue = transaction.getAttribute("instrument_amount");  // DK IR-RAUM051653353 Rel8.1.1 09/25/2012
     if(InstrumentServices.isNotBlank(newAmountValue)){
     	newAmount = new BigDecimal(newAmountValue);  // DK IR-RAUM051653353 Rel8.1.1 09/25/2012
     }
  	} catch (Exception e) {
     originalAmount = BigDecimal.ZERO;
     newAmount = BigDecimal.ZERO;
  	}
  	
  	boolean invAssigned = false;
    if(InstrumentServices.isNotBlank(transaction.getAttribute("transaction_oid")) && !"0".equals(transaction.getAttribute("transaction_oid"))){
   	  StringBuffer       whereClause              = new StringBuffer();
   	  whereClause.append("a_transaction_oid = ?");
      //whereClause.append(transaction.getAttribute("transaction_oid"));
    //jgadela  R90 IR T36000026319 - SQL FIX
	Object sqlParams[] = new Object[1];
	sqlParams[0] = transaction.getAttribute("transaction_oid");

     if (DatabaseQueryBean.getCount("upload_invoice_oid", "invoices_summary_data", whereClause.toString(), false, sqlParams) != 0)
     {
   	  	Debug.debug("There is an invoice associated to transaction");
   	  	invAssigned= true;
     }
    }
  
  //TLE - 01/29/07 - IR#FUH011755336 - Add Begin 
  //if (getDataFromDoc && doc.getAttribute("/In/Terms/amount") != null) {
  
  boolean hasEnteredAmt = false;
  if (doc.getAttribute("/In/Terms/amount") != null) {
     String enteredVal = doc.getAttribute("/In/Terms/amount");
     // [BEGIN] IR-NNUH061363633 - jkok - Added try/catch to handle NumberFormatException
     try {
     	if ((enteredVal != null) && (enteredVal.trim().length() > 0)) {
     		BigDecimal enteredAmount = new BigDecimal(enteredVal);
     		if (enteredAmount.doubleValue() != 0){
        		hasEnteredAmt = true;
     		}
     	} // if (enteredVal.trim().length() > 0) {
     } catch (NumberFormatException e) {
     	System.err.println("Transaction-ATP-AMD.jsp "
     		+"Caught NumberFormatException while trying to create BigDecimal from '"+enteredVal+"'");
        //IR-NOUH121449540(Weblogic Logs).Commented printStackTrace() method call to avoid verbose printing to logs.
     	//e.printStackTrace();
     }// [END] IR-NNUH061363633 - jkok
     
  }
     
  
  if (hasEnteredAmt && !invAssigned)  {
  //TLE - 01/29/07 - IR#FUH011755336 - Add End 

     //
     // Furthermore, since the displayed terms amount is always displayed as a 
     // positive number (the Increase/Decrease dropdown represents the sign).
     // Therefore, we may need to set the double amount negative.

     amountValue = doc.getAttribute("/In/Terms/amount");
     incrDecrValue = doc.getAttribute("/In/Terms/IncreaseDecreaseFlag");

     try {
        //<Papia - IR No. NNUF021050622 - 06/03/05>
        amountValue = TPCurrencyUtility.getDisplayAmount(amountValue, 
                                                   currencyCode, loginLocale);
        //</ Papia - IR No. NNUF021050622 - 06/03/05>
        amountValue = NumberValidator.getNonInternationalizedValue(amountValue, 
                                                          loginLocale, false);
        try {
           offsetAmount = new BigDecimal(amountValue);
        } catch (Exception e) {
           offsetAmount = BigDecimal.ZERO;
        }
       // if (incrDecrValue.equals(TradePortalConstants.DECREASE)) {
       //    offsetAmount = offsetAmount.negate();
        //}
        //RavindraB - Rel800- IR# RAUM051653353 - 15thJun2012 - Start
        if (TradePortalConstants.DECREASE.equals(incrDecrValue)) {
         //RavindraB - Rel800- IR# RAUM051653353 - 15thJun2012 - End
           offsetAmount = offsetAmount.negate();
           displayAmount = TPCurrencyUtility.getDisplayAmount(
                   offsetAmount.abs().toString(), currencyCode, loginLocale);
        }
        else {
            displayAmount = doc.getAttribute("/In/Terms/amount");

         }

        if(amountUpdatedFromPOs) {
           displayAmount = terms.getAttribute("amount"); 
           if( InstrumentServices.isBlank(displayAmount) && !isReadOnly ) {
              displayAmount = "";
           }
           /* Komal M commented the following code for BMO 75
            * TPCurrencyUtility class converts currency to a comma separated values / any locale specific formatting of the currency.
            * This value will not be accepted by Dojo CurrencyTextBox (Widget internally used in createAmountField) and results in BLANK amount field.
            */

          /*  else {
              displayAmount = TPCurrencyUtility.getDisplayAmount(displayAmount, currencyCode, loginLocale);
           } */
        }
        

     } catch (Exception e) {
        // Unable to unformat number so the display value is the save value
        // the user typed in.
        displayAmount = amountValue;
        badAmountFound = true;
        medService.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                                  TradePortalConstants.INVALID_CURRENCY_FORMAT,
                                  amountValue);
        medService.addErrorInfo();
        doc.addComponent("/Error", medService.getErrorDoc());
        formMgr.storeInDocCache("default.doc", doc);
     }
  } else {
	  
	  Debug.debug("have POs/Invs");
     // We're getting the amount from the database so convert to a positive
     // number if necessary and set the Increase/Decrease dropdown accordingly.
     try {
        amountValue = terms.getAttribute("amount");
     // DK IR-RAUM051653353 Rel8.1.1 10/16/2012 Begin
        amountValue = amountValue.replaceAll(",","");
     //SHR instead of double use string value
		offsetAmount = new BigDecimal(amountValue);
		// DK IR-RAUM051653353 Rel8.1.1 10/16/2012 End
     } catch (Exception e) {
        offsetAmount = BigDecimal.ZERO;
     }
     if(getDataFromDoc){
    	 if("Y".equals(doc.getAttribute("/Out/ChangePOs/fromStrucPO"))){
    		 if (offsetAmount.doubleValue() < 0) incrDecrValue = TradePortalConstants.DECREASE;
         }
    	 else{
    		 incrDecrValue = doc.getAttribute("/In/Terms/IncreaseDecreaseFlag");
    		 if(TradePortalConstants.DECREASE.equals(incrDecrValue)){
        		 offsetAmount = offsetAmount.negate();
        	 } 
    	}
    }
     else{
      if (offsetAmount.doubleValue() < 0) incrDecrValue = TradePortalConstants.DECREASE;
  	  }
     if( InstrumentServices.isBlank( amountValue )  && !isReadOnly) {
   	    displayAmount = "";
     }
     else{
    	 
    	 /* Komal M commented the following code for BMO 75
          * TPCurrencyUtility class converts currency to a comma separated values / any locale specific formatting of the currency.
          * This value will not be accepted by Dojo CurrencyTextBox (Widget internally used in createAmountField) and results in BLANK amount field.
          */
         displayAmount = offsetAmount.abs().toString();
         /*  displayAmount = TPCurrencyUtility.getDisplayAmount(
                            offsetAmount.abs().toString(), currencyCode, loginLocale);*/
     }
  }
  
  /*else {
     // We're getting the amount from the database so convert to a positive
     // number if necessary and set the Increase/Decrease dropdown accordingly.
     try {
        amountValue = terms.getAttribute("amount");
        offsetAmount = new BigDecimal(amountValue);
     } catch (Exception e) {
        offsetAmount = BigDecimal.ZERO;
     }
     if (offsetAmount.doubleValue() < 0) incrDecrValue = TradePortalConstants.DECREASE;
     if( InstrumentServices.isBlank( amountValue )  && !isReadOnly) {
   	    displayAmount = "";
     }
     else{
         /* Komal M commented the following code for BMO 75
          * TPCurrencyUtility class converts currency to a comma separated values / any locale specific formatting of the currency.
          * This value will not be accepted by Dojo CurrencyTextBox (Widget internally used in createAmountField) and results in BLANK amount field.
          */
        /*  displayAmount = offsetAmount.abs().toString();
         displayAmount = TPCurrencyUtility.getDisplayAmount(
                            offsetAmount.abs().toString(), currencyCode, loginLocale); 
     }
  }*/
//Srinivasu_D IR#T36000035537 ER9.1.0.2 01/01/2015 - handling minus
	 if(StringFunction.isNotBlank(amountValue)){
	 char amtChar[] = amountValue.toCharArray();
		if(amtChar[0] == '-'){
			amountValue = amountValue.substring(1,amountValue.length());
		}
	 }
	 //Srinivasu_D IR#T36000035537 ER9.1.0.2 01/01/2015 - End 
  calculatedTotal = originalAmount.add(offsetAmount);


  transactionType = transaction.getAttribute("transaction_type_code");
  transactionStatus = transaction.getAttribute("transaction_status");
  transactionOid = transaction.getAttribute("transaction_oid");
  
  // Get the transaction rejection indicator to determine whether to show the rejection reason text
  rejectionIndicator = transaction.getAttribute("rejection_indicator");
  rejectionReasonText = transaction.getAttribute("rejection_reason_text");

  instrumentType = instrument.getAttribute("instrument_type_code");
  instrumentStatus = instrument.getAttribute("instrument_status");

  Debug.debug("Instrument Type " + instrumentType);
  Debug.debug("Instrument Status " + instrumentStatus);
  Debug.debug("Transaction Type " + transactionType);
  Debug.debug("Transaction Status " + transactionStatus);

  // Create documents for the phrase dropdowns. First check the cache (no need
  // to recreate if they already exist).  After creating, place in the cache.
  // (InstrumentCloseNavigator.jsp cleans these up.)

  DocumentHandler phraseLists = formMgr.getFromDocCache("PhraseLists");
  if (phraseLists == null) phraseLists = new DocumentHandler();

  DocumentHandler goodsDocList = phraseLists.getFragment("/Goods");
  if (goodsDocList == null) {
     goodsDocList = PhraseUtility.createPhraseList(
                                    TradePortalConstants.PHRASE_CAT_GOODS, 
                                    userSession, formMgr, resMgr);
     phraseLists.addComponent("/Goods", goodsDocList);
  }

  DocumentHandler addlCondDocList = phraseLists.getFragment("/AddlCond");
  if (addlCondDocList == null) {
     addlCondDocList = PhraseUtility.createPhraseList(
                                    TradePortalConstants.PHRASE_CAT_ADDL_COND, 
                                    userSession, formMgr, resMgr);
     phraseLists.addComponent("/AddlCond", addlCondDocList);
  }

  DocumentHandler spclInstrDocList = phraseLists.getFragment("/SpclInstr");
  if (spclInstrDocList == null) {
     spclInstrDocList = PhraseUtility.createPhraseList(
                                    TradePortalConstants.PHRASE_CAT_SPCL_INST, 
                                    userSession, formMgr, resMgr);
     phraseLists.addComponent("/SpclInstr", spclInstrDocList);
  }

  formMgr.storeInDocCache("PhraseLists", phraseLists);

  ////////////////////////////////////////////////////////////////////////
  // Retrieve the latest information of applicatnt name and expiry date.  
  // They are displayed in general section.
  ////////////////////////////////////////////////////////////////////////

  // Retrieve the latest information of expiry date from instrument.
  currentExpiryDate = instrument.getAttribute("copy_of_expiry_date");

  // Retrieve the applicant name from the bank release terms of the latest issuance or amendment transaction.
  //    (The latest transaction is the one that has the latest transaction status date and in case several transaction
  //     have the same transaction status date, the largest transaction_oid.)
  String instrument_oid = instrument.getAttribute("instrument_oid");
  String issueTransactionSQL = "select p.name"
                     +" from terms_party p, terms t, transaction tr"
                     +" where tr.transaction_oid ="
                     +"       (select max(transaction_oid)"
                     +"        from transaction"
                     +"        where transaction_status_date = (select max(transaction_status_date)"
                     +"                                             from transaction"
                     +"                                             where p_instrument_oid = ?"
                     +"                                             and (transaction_type_code = ? or transaction_type_code = ? or transaction_type_code = ?) "
                     +"                                             and transaction_status = ?)" 
                     +"           and p_instrument_oid = ?"
                     +"           and (transaction_type_code = ? or transaction_type_code = ? or transaction_type_code = ?)"
                     +"           and transaction_status = ?)"
                     +" and tr.c_bank_release_terms_oid = t.terms_oid"
                     +" and (p.terms_party_oid = t.c_first_terms_party_oid"
                     +"      or p.terms_party_oid = t.c_second_terms_party_oid"
                     +"      or p.terms_party_oid = t.c_third_terms_party_oid"
                     +"      or p.terms_party_oid = t.c_fourth_terms_party_oid"
                     +"      or p.terms_party_oid = t.c_fifth_terms_party_oid)"
                     +" and p.terms_party_type = ?";
  
	//jgadela  R90 IR T36000026319 - SQL FIX
    Object sqlParams1[] = new Object[11];
	sqlParams1[0] = instrument_oid;
	sqlParams1[1] = TransactionType.ISSUE;
	sqlParams1[2] = TransactionType.AMEND;
	sqlParams1[3] = TransactionType.CHANGE;
	sqlParams1[4] = TradePortalConstants.TRANS_STATUS_PROCESSED_BY_BANK;
	
	sqlParams1[5] = instrument_oid;
	sqlParams1[6] = TransactionType.ISSUE;
	sqlParams1[7] = TransactionType.AMEND;
	sqlParams1[8] = TransactionType.CHANGE;
	sqlParams1[9] = TradePortalConstants.TRANS_STATUS_PROCESSED_BY_BANK;
	sqlParams1[10] = TradePortalConstants.ATP_BUYER;
	
                     
  DocumentHandler issueTransactionResult = DatabaseQueryBean.getXmlResultSet(issueTransactionSQL, false, sqlParams1);
  if (issueTransactionResult!=null) {
     applicantName = issueTransactionResult.getAttribute("/ResultSetRecord(0)/NAME");
  }
%>


<%-- ********************* HTML for page begins here ********************* --%>

<%
  // Some of the retrieval logic above may have set a focus field.  Otherwise,
  // we'll use the initial value for focus.
  String onLoad = "";

  // Auto save the form when time-out if not readonly.  
  // (Header.jsp will check for auto-save setting of the corporate org).
  String autoSaveFlag = isReadOnly ? TradePortalConstants.INDICATOR_NO : TradePortalConstants.INDICATOR_YES;
  String templateFlag = isTemplate ? TradePortalConstants.INDICATOR_YES : TradePortalConstants.INDICATOR_NO;
  
  String showNavBar = TradePortalConstants.INDICATOR_NO;
  if (isTemplate)
  {
     showNavBar = TradePortalConstants.INDICATOR_YES;
  }

%>


<%
  String pageTitleKey;
	if ( TradePortalConstants.INDICATOR_YES.equals(newTransaction) && !(TransactionType.AMEND).equals(transaction.getAttribute("transaction_type_code"))) {
    pageTitleKey = "SecondaryNavigation.NewInstruments";
    userSession.setCurrentPrimaryNavigation("NavigationBar.NewInstruments");
  } else {
    pageTitleKey = "SecondaryNavigation.Instruments";
    userSession.setCurrentPrimaryNavigation("NavigationBar.Transactions");
  }
  String helpUrl = "customer/amend_approval_to_pay.htm";

  //cquinton 11/19/2012 pass info for recent instruments into header
  String newRecentTransactionOid = transaction.getAttribute("transaction_oid");
  String newRecentInstrumentId = instrument.getAttribute("complete_instrument_id");
  String newRecentTransactionType = transaction.getAttribute("transaction_type_code");
  String newRecentTransactionStatus = transaction.getAttribute("transaction_status");
%>

<jsp:include page="/common/Header.jsp">
  <jsp:param name="autoSaveFlag"           value="<%=autoSaveFlag%>" />
  <jsp:param name="templateFlag"           value="<%=templateFlag%>" />
  <jsp:param name="newRecentTransactionOid" value="<%=newRecentTransactionOid%>" />
  <jsp:param name="newRecentInstrumentId"  value="<%=newRecentInstrumentId%>" />
  <jsp:param name="newRecentTransactionType" value="<%=newRecentTransactionType%>" />
  <jsp:param name="newRecentTransactionStatus" value="<%=newRecentTransactionStatus%>" />
</jsp:include>

<div class="pageMain">
<div class="pageContent">

<% //cr498 begin
   //Include ReAuthentication frag in case re-authentication is required for
   //authorization of transactions for this client
   String certAuthURL = "";
   Cache reCertCache = (Cache)TPCacheManager.getInstance().getCache(TradePortalConstants.CLIENT_BANK_CACHE);
   DocumentHandler CBCResult = (DocumentHandler)reCertCache.get(userSession.getClientBankOid());	
   String requireTranAuth = CBCResult.getAttribute("/ResultSetRecord(0)/REQUIRE_TRAN_AUTH");
   boolean requireAuth = InstrumentAuthentication.requireTransactionAuthentication(
       requireTranAuth,InstrumentAuthentication.TRAN_AUTH__ATP_AMD);
   if (requireAuth) {
%>   
      <%@ include file="/logon/fragments/openReauthenticationWindow.frag" %>
<%
   }
   //cr498 end
%>
<form id="TransactionATP" name="TransactionATP" method="post" data-dojo-type="dijit.form.Form" action="<%=formMgr.getSubmitAction(response)%>">
  <input type=hidden value="" name=buttonName>
  
  

<% //cr498 begin
  if (requireAuth) {
%> 

  <input type=hidden name="reCertification" value="<%=TradePortalConstants.INDICATOR_NO%>">
  <input type=hidden name="reCertOK">
  <input type=hidden name="logonResponse">
  <input type=hidden name="logonCertificate">

<%
  } //cr498 end
%> 

<%
  if(transaction.getAttribute("transaction_status").equals(TradePortalConstants.TRANS_STATUS_PROCESSED_BY_BANK))
	  isProcessedByBank = true;

  // Store values such as the userid, security rights, and org in a
  // secure hashtable for the form.  Also store instrument and transaction
  // data that must be secured.
  Hashtable secureParms = new Hashtable();
  secureParms.put("login_oid", userSession.getUserOid());
  secureParms.put("owner_org_oid", userSession.getOwnerOrgOid());
  secureParms.put("login_rights", loginRights);

  secureParms.put("instrument_oid", instrument.getAttribute("instrument_oid"));
  secureParms.put("instrument_type_code", instrumentType);
  secureParms.put("instrument_status", instrumentStatus);

  secureParms.put("transaction_oid", transaction.getAttribute("transaction_oid"));
  secureParms.put("transaction_type_code", transactionType);
  secureParms.put("transaction_status", transactionStatus);

  secureParms.put("shipment_oid", terms.getFirstShipment().getAttribute("shipment_oid"));
  secureParms.put("shipmentOid", terms.getFirstShipment().getAttribute("shipment_oid"));
  
  secureParms.put("transaction_instrument_info", 
  	transaction.getAttribute("transaction_oid") + "/" + 
	instrument.getAttribute("instrument_oid") + "/" + 
	transactionType);

  // If the terms record doesn't exist, set its oid to 0.
  String termsOid = terms.getAttribute("terms_oid");
  if (termsOid == null) termsOid = "0";
  secureParms.put("terms_oid", termsOid);
  secureParms.put("TransactionCurrency", currencyCode);
  //IR - PAUK042248441 [BEGIN]
  //secureParms.put("ApplRefNum", terms.getAttribute("reference_number"));
  secureParms.put("ApplRefNum", StringFunction.xssHtmlToChars(terms.getAttribute("reference_number")));
  //IR - PAUK042248441 [END]
%>

     

<jsp:include page="/common/PageHeader.jsp">
   <jsp:param name="titleKey" value="<%=pageTitleKey%>" />
   <jsp:param name="item1Key" value="SecondaryNavigation.Instruments.ApprovalToPay" />
   <jsp:param name="helpUrl"  value="<%=helpUrl%>" />
</jsp:include>

<jsp:include page="/common/TransactionSubHeader.jsp" />

	<%-- error section goes above form content --%>
		        <div class="formArea">
		          <jsp:include page="/common/ErrorSection.jsp" />
		          <%-- DK IR T36000023963 Rel8.4 01/17/2014 --%>
		          <div class="formContent">
<% // [BEGIN] IR-YVUH032343792 - jkok %>
  <%@ include file="fragments/Instruments-AttachDocuments.frag" %>
<% // [END] IR-YVUH032343792 - jkok %>

				  
 <%	if (rejectionIndicator.equals(TradePortalConstants.INDICATOR_YES)
    || rejectionIndicator.equals(TradePortalConstants.INDICATOR_X))
  {
%>
	<%=widgetFactory.createSectionHeader("0", "Rejection Reason") %>
     <%@ include file="fragments/Transaction-RejectionReason.frag" %>
     </div>
<%
  }
%>
	<% //CR 821 Added Repair Reason Section 
	  StringBuffer repairReasonWhereClause = new StringBuffer();
	  int  repairReasonCount = 0;
		
		/*Get all repair reason's count from transaction history table*/
		repairReasonWhereClause.append("p_transaction_oid = ? " );
		repairReasonWhereClause.append(" and REPAIR_REASON is not null order by ACTION_DATETIME");

		Debug.debug("repairReasonWhereCondition query : " + repairReasonWhereClause);
		//jgadela  R90 IR T36000026319 - SQL FIX
	    Object sqlParams2[] = new Object[1];
		sqlParams2[0] = transaction.getAttribute("transaction_oid");
		
		repairReasonCount = DatabaseQueryBean.getCount("A_USER_OID", "transaction_history", repairReasonWhereClause.toString(), true, sqlParams2);
				
	if ( !(TradePortalConstants.TRANS_STATUS_PROCESSED_BY_BANK.equalsIgnoreCase(transactionStatus)) && repairReasonCount>0  ){%>
		<%=widgetFactory.createSectionHeader("", "TransactionHistory.RepairReason") %>
			<%@ include file="fragments/Transaction-RepairReason.frag" %>
		</div>
	<%} %> 
	

  <%@ include file="fragments/Transaction-ATP-AMD-html.frag" %>
  
  	<% if (TradePortalConstants.TRANS_STATUS_PROCESSED_BY_BANK.equalsIgnoreCase(transactionStatus) && repairReasonCount>0  ){%>
			<%=widgetFactory.createSectionHeader("5", "TransactionHistory.RepairReason", null, true) %>
			<%@ include file="fragments/Transaction-RepairReason.frag" %>
			</div>
	<%} %> 

  </div><%--formContent--%>
</div><%--formArea--%>

<div class="formSidebar" data-dojo-type="widget.FormSidebar" data-dojo-props="form: 'TransactionATP'">
    <jsp:include page="/common/Sidebar.jsp">
		<jsp:param name="links" value="<%=links%>" />
		<jsp:param name="isReadOnly" value="<%=isReadOnly%>" />
		<jsp:param name="certAuthURL" value="<%=certAuthURL%>" />
		<jsp:param name="buttonPressed" value="<%=bButtonPressed%>" />
		<jsp:param name="error" value="<%= error%>" />
		<jsp:param name="formName" value="0" />
		<jsp:param name="isTemplate" value="<%=isTemplate%>"/>
		 <jsp:param name="showLinks" value="true" />  
		 <jsp:param name="isNewTransaction" value="<%=newTransaction%>"/>
	</jsp:include>
</div> <%--closes sidebar area--%>
 



  <%@ include file="fragments/PhraseLookupPrep.frag" %>
  

<%= formMgr.getFormInstanceAsInputField("Transaction-ATPForm", secureParms) %>

</form>  
</div> 
</div> 
<div id="AddStructurePODialog"></div>
<div id="RemoveStructurePODialog"></div>
<div id="ViewStructurePODialog"></div>
<%-- KMehta on 12 May 2014 @ Rel 90 for IR T36000025449 Start --%>
<jsp:include page="/common/Footer.jsp">
   <jsp:param name="autoSaveFlag"           value="<%=autoSaveFlag%>" />
   <jsp:param name="templateFlag"           value="<%=templateFlag%>" />
   <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>
<%-- KMehta on 12 May 2014 @ Rel 90 for IR T36000025449 End --%>
<jsp:include page="/common/SidebarFooter.jsp" />


<script type="text/javascript">

  <%-- cquinton 3/3/2013 add local var --%>
  var local = {};
<%--  IR T36000019155- AddPOStructure/RemovePOStructure/ViewPOStructure() moved before the isReadOnly condition below. --%>
<%-- IR T36000017820 -modified to pass currency/BeneName and uploadDefOid to the AddStructuredPO.jsp --%>
	function AddPOStructure(){
	  require(["dojo/dom","t360/dialog"], function(dom,dialog) {

	    destroyPurchaseOrderSearchId();
	    
		var dialogName = '<%=resMgr.getText("PurchaseOrder.SearchStructuredPO", TradePortalConstants.TEXT_BUNDLE)%>';	 
		var parmNames = [ "ins_type", "poLineItemCurrency", "poLineItemBeneficiaryName", "poLineItemUploadDefinitionOid" ];
		<%-- PMitnala Rel 8.3 IR#T36000021934 - Passing poLineItemBeneficiaryName to the method in StringFucntion, to replace special characters --%>
	    	var parmVals = [ "ATP", '<%= currencyCode%>', '<%= StringFunction.escapeQuotesforJS(poLineItemBeneficiaryName)%>', '<%= poLineItemUploadDefinitionOid%>' ];
		
			 dialog.open('AddStructurePODialog', dialogName, 'AddStructuredPO.jsp', parmNames,parmVals, <%-- parameters --%>
	                 'select', null);
	  });
	}

	function ViewPOStructure(){	
	 require(["t360/dialog"], function(dialog) {
		
		 destroyPurchaseOrderSearchId();
			
		 var dialogName = '<%=resMgr.getText("PurchaseOrder.AssignedStructuredPO", TradePortalConstants.TEXT_BUNDLE)%>';	 
		 dialog.open('ViewStructurePODialog', dialogName, 'ViewStructuredPO.jsp', ['shipmentOid'],['<%=terms.getFirstShipment().getAttribute("shipment_oid")%>'], <%-- parameters --%>
	                'select', null);
	  });
	}

	function RemovePOStructure(){
	  require(["t360/dialog"], function(dialog) {
			
		destroyPurchaseOrderSearchId();
		  
		var dialogName = '<%=resMgr.getText("PurchaseOrder.AssignedStructuredPO", TradePortalConstants.TEXT_BUNDLE)%>';	 
		dialog.open('RemoveStructurePODialog', dialogName, 'RemoveStructuredPO.jsp', ['shipmentOid','ins_type'],['<%=terms.getFirstShipment().getAttribute("shipment_oid")%>','ATP'], <%-- parameters --%>
	                 'select', null);
	 });
	}

	function destroyPurchaseOrderSearchId(){
		
		require(["dijit/registry"], function(registry) {
			var PONum = registry.byId("PONum");
			if(PONum) PONum.destroy();
			
			var BeneName = registry.byId("BeneName");
			if(BeneName) BeneName.destroy();
			
			var AmountFrom = registry.byId("AmountFrom");
			if(AmountFrom) AmountFrom.destroy();
			
			var AmountTo = registry.byId("AmountTo");
			if(AmountTo) AmountTo.destroy();
			
			var Currency = registry.byId("Currency");
			if(Currency) Currency.destroy();
			
			var Search = registry.byId("PurchaseOrderSearch");
			if(Search) Search.destroy();
			
			var Close = registry.byId("AddStructurePODialogCloseButton");
			if(Close) Close.destroy();
		});		
	
	}

<%
  if (!isReadOnly && InstrumentServices.isNotBlank(focusField)) {
%>

  require(["dijit/registry", "dojo/ready"],
    function(registry, ready ) {
      ready(function() {
        var focusFieldId = '<%= focusField %>';
        if ( focusFieldId ) {
          var focusField = registry.byId(focusFieldId);
          focusField.focus();
        }
       <%--  registry.byId("TransactionAmount2").attr('disabled', 'true'); --%>
      });
  });
  
  var xhReq;
  function setNewAmount(){
	  require(["dijit/registry"],
			    function(registry ) {
		  
		  			var existingLCAmount = <%=originalAmount%>;
	  				var ccyCode = '<%=currencyCode%>';
	  				var flag;
	  				
	  				if(registry.byId("Increase").checked){
	  					flag='Increase';
	  				}else{
	  					flag='Decrease';
	  				}
	  				var incDecValue = registry.byId("TransactionAmount").value;
	  				var newLCAmount = 0;	  				
	  				
		  				if(flag == 'Increase'){
		  					newLCAmount = Number(existingLCAmount) + Number(incDecValue);
		  				}else if(flag == 'Decrease'){
		  					newLCAmount = Number(existingLCAmount) - Number(incDecValue);
		  				}		  				
		  				if(isNaN(newLCAmount)){
		  					newLCAmount = existingLCAmount;
		  				}
	  				if (!xhReq)
	  					xhReq = getXmlHttpRequest();
	  				var req = "Amount=" + newLCAmount + "&currencyCode="+ccyCode;
	  				xhReq.open("GET", '/portal/transactions/AmountFormatter.jsp?'+req, true);	  				
	  				xhReq.onreadystatechange = updateAmount;
	  				xhReq.send(null);
	  			  });}

  function getXmlHttpRequest()
  {
    var xmlHttpObj;
    if (window.XMLHttpRequest) {
      xmlHttpObj = new XMLHttpRequest();
    }
    else {
      try {
        xmlHttpObj = new ActiveXObject("Msxml2.XMLHTTP");
      } catch (e) {
        try {
          xmlHttpObj = new ActiveXObject("Microsoft.XMLHTTP");
        } catch (e) {
          xmlHttpObj = false;
        }
      }
    }
    console.log(xmlHttpObj);
    return xmlHttpObj;
  }
  
	  			function updateAmount(){
	  				require(["dijit/registry","dojo/dom","dojo/domReady!"], function(registry,dom){
	  				 if (xhReq.readyState == 4 && xhReq.status == 200) {	 
	  					var ccyCode = '<%=currencyCode%>';
	  					 var formattedAmt = ccyCode + " " + xhReq.responseText;	  					
	  					dom.byId("TransactionAmount2").innerHTML = formattedAmt;
	  			    	<%-- registry.byId("TransactionAmount2").set('value',xhReq.responseText); --%>
	  			      } 
	  				});
	  			  }
 
	  		
		<%-- cquinton 3/2/2013 local phrase lookup --%>
  require(["t360/phraseLookup"],
          function( phraseLookup ) {
    local.phraseLookupAppend = function(phraseSelect, textArea, textAreaMaxLength ) {
      phraseLookup.appendPhrase( phraseSelect, textArea, textAreaMaxLength, "_errorSection" );
    };
  });	

<%
  }
%>
</script>

</body>
</html>

<%
   // Finally, reset the cached document to eliminate carryover of
   // information to the next visit of this page.
   formMgr.storeInDocCache("default.doc", new DocumentHandler());
%>
