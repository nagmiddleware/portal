<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.web.*, com.amsinc.ecsg.frame.*, com.amsinc.ecsg.util.*, java.util.*, 
                 java.io.ByteArrayOutputStream,java.io.PrintWriter,com.ams.tradeportal.common.*" %>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session"></jsp:useBean>

<%

   String nextPhysicalPage = null;

   try
   {
      ServletContext context = this.getServletConfig().getServletContext();

      response.setContentType("text/html");

      RequestDispatcher requestDispatcher = context.getRequestDispatcher("/DirectDebitFileUploadServlet");

      requestDispatcher.include(request, response);

      NavigationManager nif = NavigationManager.getNavMan();

      nextPhysicalPage = nif.getPhysicalPage(formMgr.getCurrPage(), request);
   }
   catch (Exception e)
   {
      System.out.println("Exception caught in DirectDebitFileUploadServlet.jsp");
      ByteArrayOutputStream bytes = new ByteArrayOutputStream();
      PrintWriter writer = new PrintWriter(bytes, true);
      e.printStackTrace(writer);
      System.out.println(bytes.toString());
   }
%>

<jsp:forward page='<%= nextPhysicalPage %>' />
