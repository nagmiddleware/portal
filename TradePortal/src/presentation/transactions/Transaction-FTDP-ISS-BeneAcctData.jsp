<%--
*******************************************************************************
  International Payment Beneficiary Account Data section

  Description:
  This jsp is called via ajax and returns a snippet of html associated with
  a new terms party for the accounts associated with a beneficiary.  
  The user can then select an existing account or enter a new one.

  This is dependent on the common Transaction-FTRQ-ISS-BeneAcctData.frag
  which generically handles the terms party account section, either from here 
  or from the instrument page itself.

  Note: this assumes non readonly mode.
  
*******************************************************************************
--%>
<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.html.*,java.math.*,java.util.*" %>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session"> </jsp:useBean>
<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session"> </jsp:useBean>
<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session"> </jsp:useBean>
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"> </jsp:useBean>
   

<%


  //String partyType = request.getParameter("partyType");
  String encryptedPartyRowKey = request.getParameter("partyRowKey");
  String enteredAcct = request.getParameter("acctNo");
  String domId = request.getParameter("domId");
  //String clean = request.getParameter("clear");
  
  domId = EncryptDecrypt.decryptStringUsingTripleDes(domId, userSession.getSecretKey());

  
  String bene_party_oid = ""; 
  String partyRowKey = EncryptDecrypt.decryptStringUsingTripleDes(encryptedPartyRowKey, userSession.getSecretKey());
  
if (InstrumentServices.isNotBlank(partyRowKey)) { 
	  StringTokenizer strTok = new StringTokenizer(partyRowKey,"/");
	  if (strTok.hasMoreTokens()) {
		bene_party_oid = strTok.nextToken();
	  }
	  else {
		bene_party_oid =partyRowKey;
	  }
  }
  
  
  boolean isStatusVerifiedPendingFX = false; 
  boolean isReadOnly = false;
  boolean isTemplate = false;
  String isTemplateStr = request.getParameter("isTemplate");
  if ( "true".equals(isTemplateStr) ) {
    isTemplate = true; 
  } 
  
  String isisReadOnly = request.getParameter("isReadOnly");
  if ( "true".equals(isisReadOnly) ) {
    isReadOnly = true;
  }
  
  
  WidgetFactory widgetFactory = new WidgetFactory(resMgr, userSession);

  
  DomesticPaymentWebBean currentDomesticPayment = beanMgr.createBean(DomesticPaymentWebBean.class, "DomesticPayment");  
  
  if (InstrumentServices.isNotBlank(domId)) {
	currentDomesticPayment.setAttribute("domestic_payment_oid", domId);
	currentDomesticPayment.getDataFromAppServer();
  }
  
  if (InstrumentServices.isNotBlank(currentDomesticPayment.getAttribute("source_template_dp_oid")))
  {  	
  	currentDomesticPayment.sourceDomesticPayment = beanMgr.createBean(DomesticPaymentWebBean.class, "DomesticPayment");  
  	currentDomesticPayment.sourceDomesticPayment.setAttribute("domestic_payment_oid", currentDomesticPayment.getAttribute("source_template_dp_oid"));
  	currentDomesticPayment.sourceDomesticPayment.getDataFromAppServer();
  }
  

  
  String payeeAcctNum = currentDomesticPayment.getAttribute("payee_account_number");
  
  
  
  //termsParty is now populated with account data
%>

 <%@ include file="fragments/Transaction-FTDP-ISS-BeneAcctData.frag" %>
