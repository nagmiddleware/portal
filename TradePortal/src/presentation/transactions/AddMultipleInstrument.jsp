<%--for the ajax include--%>

<%@ page
	import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,com.ams.tradeportal.common.*,com.ams.tradeportal.html.*,com.ams.tradeportal.busobj.*,com.ams.tradeportal.busobj.webbean.*,com.ams.tradeportal.busobj.util.*,java.util.*,com.ams.tradeportal.common.cache.*"%>
				 
<jsp:useBean  id="resMgr" class="com.amsinc.ecsg.util.ResourceManager"
	scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" 
             scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" 
             scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" 
   scope="session">
</jsp:useBean>

<% 
	String parmValue = "";
	WidgetFactory widgetFactory = new WidgetFactory(resMgr); 
	parmValue = request.getParameter("addInstrument");
    String loginLocale = userSession.getUserLocale();
    String loginRights = userSession.getSecurityRights();
    int addInstrument = 0;
    if ( parmValue != null ) {
      try {
    	  addInstrument = (new Integer(parmValue)).intValue();
      } 
      catch (Exception ex ) {
      }
    }
  
  TermsPartyWebBean terms = beanMgr.createBean(TermsPartyWebBean.class, "TermsParty");
 %>
 

<%@ include file="fragments/AddMultipleInstrument.frag" %>