<%--
*******************************************************************************
                            PO Upload Log Detail Page

  Description:  The PO Upload Log Detail page is used to provide detail
                information for a specified PO upload file.
                
  Note:         Consult userSession pageFlow to determine where to return.

*******************************************************************************
--%>

<%--
 *
 *     Copyright   2001
 *     American Management Systems, Incorporated
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.html.*" %>
<%@ page import="java.util.*" %>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager"
    scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean"
   scope="session">
</jsp:useBean>


<%-- ************** Data retrieval page setup begins here ****************  --%>

<%
	PurchaseOrderFileUploadWebBean fileUpload = null;
	UserWebBean thisUser = null;
	DocumentHandler xmlDoc = null;
	StringBuffer errorListWhere = new StringBuffer();
	StringBuffer dynamicWhereClause = null;
	StringBuffer label = new StringBuffer();
	Hashtable secureParms = new Hashtable();
	String userSecurityRights = null;
	String userSecurityType = null;
	String instrumentOid = null;
	String uploadOid = null;
	String userTimeZone = null;
	String userLocale = null;
	String userOrgOid = null;
	String newLink = null;
	String userOid = null;
	String validationStatus = null;
	String instrumentTypeCode = null;
	boolean isReadOnly = true;
	int uploadNumber = 0;
	String uploadNumberLabel = "";
	String initSearchParms = "";
	DataGridFactory dgFactory = new DataGridFactory(resMgr,
			userSession, formMgr, response);
	String gridHtml = "";
	String gridLayout = "";
	boolean displayErrorGrid = false;
	String gridLogHtml = "";
	String gridLogLayout = "";
	boolean displayLogGrid = false;
	DGridFactory dGridFactory = new DGridFactory(resMgr, userSession, formMgr, beanMgr, response);
	
   //cquinton 1/18/2013 add page to page flow
   userSession.addPage("goToPOUploadFileDetail", request);

	// Get the user's security rights, security type, locale, organization, oid, and client bank oid
	userSecurityRights = userSession.getSecurityRights();
	userSecurityType = userSession.getSecurityType();
	userTimeZone = userSession.getTimeZone();
	userLocale = userSession.getUserLocale();
	userOrgOid = userSession.getOwnerOrgOid();
	userOid = userSession.getUserOid();

	fileUpload = beanMgr.createBean(PurchaseOrderFileUploadWebBean.class, "PurchaseOrderFileUpload");

	if (request.getParameter("UploadOid") != null) {
		uploadOid = EncryptDecrypt.decryptStringUsingTripleDes(
				request.getParameter("UploadOid"),
				userSession.getSecretKey());
		session.setAttribute("UploadOid", EncryptDecrypt
				.encryptStringUsingTripleDes(uploadOid,
						userSession.getSecretKey()));
	} else
		uploadOid = EncryptDecrypt.decryptStringUsingTripleDes(
				(String) session.getAttribute("UploadOid"),
				userSession.getSecretKey());

	fileUpload.setAttribute("po_file_upload_oid", uploadOid);
	fileUpload.getDataFromAppServer();
	validationStatus = fileUpload.getAttribute("validation_status");

	thisUser = beanMgr.createBean(UserWebBean.class, "User");
	thisUser.setAttribute("user_oid",
			fileUpload.getAttribute("user_oid"));
	thisUser.getDataFromAppServer();

	label.append(resMgr.getText("POUploadLogDetail.PurchaseOrderFile",
			TradePortalConstants.TEXT_BUNDLE));
	label.append("&nbsp;  - &nbsp;");
	label.append(fileUpload.getAttribute("po_file_name"));
	label.append("&nbsp;- &nbsp;");

	errorListWhere.append(" and p_po_file_upload_oid = ");
	errorListWhere
			.append(fileUpload.getAttribute("po_file_upload_oid"));

	int invUploaded = 0;
	if (InstrumentServices.isNotBlank(fileUpload
			.getAttribute("number_of_po_uploaded")))
		invUploaded = Integer.parseInt(fileUpload
				.getAttribute("number_of_po_uploaded"));

	int invFailed = 0;
	if (InstrumentServices.isNotBlank(fileUpload
			.getAttribute("number_of_po_failed")))
		invFailed = Integer.parseInt(fileUpload
				.getAttribute("number_of_po_failed"));

	uploadNumber = invUploaded - invFailed;

	if (!validationStatus
			.equals(TradePortalConstants.PYMT_UPLOAD_VALIDATION_SUCCESSFUL)) {
		if ((validationStatus
				.equals(TradePortalConstants.PYMT_UPLOAD_VALIDATION_FAILED))
				&& (InstrumentServices.isNotBlank(fileUpload
						.getAttribute("error_msg")))) {
			uploadNumber = 0;
		}
	}

	String xmlStr = fileUpload.getAttribute("po_result_parameters");
	xmlStr = com.amsinc.ecsg.util.StringFunction.xssHtmlToChars(xmlStr);	
%>

<%-- ********************* HTML for page begins here *********************  --%>

<%
	WidgetFactory widgetFactory = new WidgetFactory(resMgr);
	
%>
<%  String titleKeyValue = "PaymentUploadLogDetail.UploadLogText";	
	if (!validationStatus
			.equals(TradePortalConstants.PYMT_UPLOAD_VALIDATION_SUCCESSFUL)) {
		if ((validationStatus
				.equals(TradePortalConstants.PYMT_UPLOAD_VALIDATION_FAILED))
				&& (InstrumentServices.isNotBlank(fileUpload
						.getAttribute("error_msg")))) {
			titleKeyValue = "PaymentUploadLogDetail.UploadLogText";
			label.append(resMgr.getText(
					"PaymentUploadLogDetail.UploadLogText",
					TradePortalConstants.TEXT_BUNDLE));
		}else{
			titleKeyValue = "PoUploadLogDetail.ErrorLogText";
			label.append(resMgr.getText("PoUploadLogDetail.ErrorLogText",
					TradePortalConstants.TEXT_BUNDLE));
		}
	}else{
		label.append(resMgr.getText(
				"PaymentUploadLogDetail.UploadLogText",
				TradePortalConstants.TEXT_BUNDLE));
	}
%>  
<jsp:include page="/common/Header.jsp">
	<jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
	<jsp:param name="includeErrorSectionFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>

<div class="pageMainNoSidebar">
  <div class="pageContent">
    <jsp:include page="/common/PageHeader.jsp">
      <jsp:param name="titleKey" value="SecondaryNavigation.PurchaseOrders" />
      <jsp:param name="item1Key" value='<%=titleKeyValue%>'/>
      <jsp:param name="helpUrl" value="customer/po_file_upload_log.htm" />
    </jsp:include>

    <div class="subHeaderDivider"></div>
    <div class="pageSubHeader">
    	<span class="pageSubHeaderItem">
        	<%= label.toString() %>
      	</span>
      <span class="pageSubHeader-right">
        <%--cquinton 10/31/2012 ir#7015 change return link to close button when no close button present.
            note this does NOT include an icon as in the sidebar--%>
<%
  //cquinton 1/18/2013 replace old close action behavior with page flow logic
  SessionWebBean.PageFlowRef backPage = userSession.peekPageBack();
  String returnAction = backPage.getLinkAction();
  String returnParms = backPage.getLinkParametersString() + "&returning=true";
  String closeLink = formMgr.getLinkAsUrl(returnAction,returnParms,response);
%>
        <button data-dojo-type="dijit.form.Button" name="CloseButton" id="CloseButton" type="button" class="pageSubHeaderButton">
          <%=resMgr.getText("common.CloseText",
					TradePortalConstants.TEXT_BUNDLE)%>
          <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
            document.location.href = "<%= closeLink %>";
          </script>
        </button>
        <%=widgetFactory.createHoverHelp("CloseButton","CloseHoverText") %> 
      </span>
      <div style="clear:both;"></div>
    </div>

<form name="POUploadLogDetailForm" method="POST" action="<%=formMgr.getSubmitAction(response)%>">

  <input type=hidden value="" name=buttonName>
  <div class="formContentNoSidebar">


  <%
  	// Store this parameter so it can be passed back into the page from somewhere else
  	secureParms.put("po_file_upload_oid", uploadOid);
  	secureParms.put("user_oid", thisUser.getAttribute("user_oid"));
  %>

  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
		<td nowrap>		  
          <%=widgetFactory.createTextField("User",
					"PaymentUploadLogDetail.User",
					thisUser.getAttribute("user_identifier"), "", isReadOnly,
					false, false, "", "", "")%>
		</td>
	</tr>
	<tr>		
		<td>&nbsp;</td>
	</tr>
    <tr>
      <td nowrap>
          <%=widgetFactory.createTextField(
					"ValidationStatus",
					"PaymentUploadLogDetail.ValidationStatus",
					ReferenceDataManager.getRefDataMgr().getDescr(
							TradePortalConstants.VALIDATION_STATUS,
							validationStatus), "", isReadOnly, false, false,
					"", "", "")%>
      </td>
    </tr>
    <tr>		
		<td>&nbsp;</td>
	</tr>
    <tr>      
      <td nowrap>   
		  <%=widgetFactory.createTextField("CompletionDate",
					"PaymentUploadLogDetail.CompletionDate",
					TPDateTimeUtility.convertGMTDateTimeForTimezoneAndFormat(
							fileUpload.getAttribute("completion_timestamp"),
							TPDateTimeUtility.SHORT,
							resMgr.getResourceLocale(), userTimeZone), "",
					isReadOnly, false, false, "", "", "")%>
      </td>
    </tr>
    <tr>		
		<td>&nbsp;</td>
	</tr>
<%
	if (!validationStatus
			.equals(TradePortalConstants.PYMT_UPLOAD_VALIDATION_SUCCESSFUL)) {
		if ((validationStatus
				.equals(TradePortalConstants.PYMT_UPLOAD_VALIDATION_FAILED))
				&& (InstrumentServices.isNotBlank(fileUpload
						.getAttribute("error_msg")))) {
%>   
	    <tr>
	      <td nowrap>
	          <%=widgetFactory.createTextField("Errors",
							"PaymentUploadLogDetail.Errors",
							fileUpload.getAttribute("error_msg"), "",
							isReadOnly, false, false, "", "", "")%>
          </td>
	    </tr>
<%
	} else {
%>   
	    <tr>	      
	      <td nowrap>
	      	  <%=widgetFactory.createTextField("UploadedPOs",
							"POUploadLogDetail.UploadedPOs",
							Integer.toString(uploadNumber), "", isReadOnly,
							false, false, "", "", "")%>
          </td>
	    </tr>
	    <tr>
	      <td nowrap>
	      	<%=widgetFactory.createTextField("FailedPOs",
							"POUploadLogDetail.FailedPOs",
							Integer.toString(invFailed), "", isReadOnly, false,
							false, "", "", "")%>
          </td>
	    </tr>
<%
	}
	}
%>   
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
  </table>
  <div class="formItem">
  <%
  	if (InstrumentServices.isNotBlank(xmlStr)) {
  %>
  <%=widgetFactory.createSubLabel("POUploadLogDetail.InstrumentList")%>
	        
   <%
   	displayLogGrid = true;
   		gridLogHtml = dGridFactory.createDataGrid(
   				"POUploadLogDetailDataGridID",
   				"POUploadLogDetailDataGrid", null);
   		gridLogLayout = dGridFactory
   				.createGridLayout("POUploadLogDetailDataGrid");
   %>
   <%=gridLogHtml%>
 <%
 	}
 %> 
  
<%
   	if (!validationStatus
   			.equals(TradePortalConstants.PYMT_UPLOAD_VALIDATION_SUCCESSFUL)) {
   		if ((validationStatus
   				.equals(TradePortalConstants.PYMT_UPLOAD_VALIDATION_FAILED))
   				&& (InstrumentServices.isNotBlank(fileUpload
   						.getAttribute("error_msg")))) {
   		} else {
   %>   
	  <%=widgetFactory.createSubLabel("POUploadLogDetail.ValidationWithErrorText")%>
	         
<%
	displayErrorGrid = true;
			gridHtml = dGridFactory.createDataGrid(
					"POFileUploadErrorsDataGridID",
					"POFileUploadErrorsDataGrid", null);
			gridLayout = dGridFactory
					.createGridLayout("POFileUploadErrorsDataGrid");
%>   
	  <%=gridHtml%> 
<%
 	}
 	}
 %>   
  
<%=formMgr.getFormInstanceAsInputField(
					"POUploadLogDetailForm", secureParms)%>
</div>
</div>
</form>

</div>
</div>

<jsp:include page="/common/Footer.jsp">
  <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
  <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>

<script type="text/javascript">			
require(["dojo/dom", "t360/OnDemandGrid", "dojo/dom-construct", "dojo/aspect", "dojo/on", "dojo/dom-style", "dijit/registry","dojo/ready", "t360/datagrid"], 
		function(dom, onDemandGrid, domConstruct, aspect, on, domStyle, registry,ready,t360grid){
	ready(function(){
<%if (displayErrorGrid) {%> 
    var gridLayout = <%=gridLayout%>;
    var initSearchParms="po_file_upload_oid=<%=StringFunction.escapeQuotesforJS(uploadOid)%>";
    var viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("POFileUploadErrorsDataView",userSession.getSecretKey())%>";  <%--Rel9.2 IR T36000032596 --%>
    onDemandGrid.createOnDemandGrid("POFileUploadErrorsDataGridID",viewName,gridLayout, initSearchParms, '0');
<%}
			if (displayLogGrid) {%>			
    var gridLogLayout = <%=gridLogLayout%>;
    var initLogSearchParms='xmlStr=<%=StringFunction.escapeQuotesforJS(xmlStr)%>';
    var viewName1 = "<%=EncryptDecrypt.encryptStringUsingTripleDes("POUploadLogDetailDataView",userSession.getSecretKey())%>";  <%--Rel9.2 IR T36000032596 --%>
    onDemandGrid.createOnDemandGrid("POUploadLogDetailDataGridID",viewName1, gridLogLayout, initLogSearchParms, '0');
<%}%>	
	});
	
	if (dom.byId('POFileUploadErrorsDataGridID')){
	    on(dom.byId('POFileUploadErrorsDataGridID'), 'dgrid-refresh-complete', function (event) {
	    	setDynamicGridHeitht(null, 'POFileUploadErrorsDataGridID');
	    });
    }
	
	if (dom.byId('POUploadLogDetailDataGridID')){
	    on(dom.byId('POUploadLogDetailDataGridID'), 'dgrid-refresh-complete', function (event) {
	    	setDynamicGridHeitht(null, 'POUploadLogDetailDataGridID');
	    });
    }
	
	function setDynamicGridHeitht(event, gridId){
		var myGrid = registry.byId(gridId);
		var rowCount = 5;
		<%--  Calculate the height of the scrollbar --%>
		<%-- var dgridScrollerNode = query('.dgrid-scroller', myGrid.domNode)[0]; --%>
		  var dgridHeaderNode = myGrid.headerNode;
		  
	    var dgridScrollerNode = myGrid.bodyNode;
	    var scrollBarHeight = dgridScrollerNode.offsetHeight - dgridScrollerNode.clientHeight;
	    
		<%-- Include column header height with the grid height --%>
		var maxHeight = (myGrid.rowHeight * (rowCount)) + (myGrid.headerNode.offsetHeight+scrollBarHeight) + 'px';
		var scrollMaxHeight = (myGrid.rowHeight * (rowCount)) + scrollBarHeight + 'px';
		
		domStyle.set(myGrid.domNode, 'maxHeight', maxHeight);
		<%-- domStyle.set(dgridScrollerNode, 'maxHeight', scrollMaxHeight); --%>
		domStyle.set(dgridScrollerNode, 'height', scrollMaxHeight);
		var recCount = myGrid._total;
		
		<%--  below code ensures grid does not show blanks rows when records count is less  --%>
		<%--  then the selected row count. e.g. when row count is 10 and record count is 8 the we --%>
		<%--  show only 8 rows. --%>
		if (recCount < rowCount){
		    var dgridContentNode = myGrid.contentNode;

		    <%-- Adding this code to to prevent IE8 crash --%>
		    if(navigator.appVersion.indexOf("MSIE 8")==-1){
		    	dgridScrollerNode.style.height = '';
		    }
		    <%-- dgridScrollerNode.style.height = ''; commented as it cause horizontal scroll in IE8 to disappear. --%>
		    dgridContentNode.style.height = 'auto';
		    if (dgridContentNode.offsetHeight < dgridScrollerNode.offsetHeight) {
		    	var dgScrlNodeOffsetHght = dgridContentNode.offsetHeight;
				dgridScrollerNode.style.height = dgScrlNodeOffsetHght + 'px';
				if (recCount == 0){
					dgridScrollerNode.style.height = '';
					dgridScrollerNode.style.height = (dgScrlNodeOffsetHght+15) + 'px';
				}else if (recCount <= rowCount){
					if (dgridScrollerNode.scrollWidth > dgridScrollerNode.offsetWidth){
		    			dgridScrollerNode.style.height = (dgridContentNode.offsetHeight + myGrid.rowHeight) + 'px';
					} else{
		    			dgridScrollerNode.style.height = (dgridContentNode.offsetHeight) + 'px';
		    		}
		    	}
		    }else{
		    	if (recCount < rowCount){
					if (dgridScrollerNode.scrollWidth > dgridScrollerNode.offsetWidth){
		    			dgridScrollerNode.style.height = (dgridContentNode.offsetHeight + myGrid.rowHeight) + 'px';
					} else{
		    			dgridScrollerNode.style.height = (dgridContentNode.offsetHeight) + 'px';
		    		}

		    	}
		    }
		    dgridContentNode.style.height = '';
	    }
	} <%-- end of setgrid --%>
});
</script> 
  
</body>
</html>

<%
	// Finally, reset the cached document to eliminate carryover of
	// information to the next visit of this page.
	xmlDoc = formMgr.getFromDocCache();

	xmlDoc.removeAllChildren("/");

	formMgr.storeInDocCache("default.doc", xmlDoc);
%>
