              
<%--
*******************************************************************************
                        Funds Transfer Request Page

  Description:    
    This is the main driver for the Funds Transfer Request page.  It handles 
  data retrieval of terms and terms parties (or retrieval from the input document)
  and creates the html page to display all the data for a Funds Transfer Request.
*******************************************************************************
--%>
      
<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.html.*,java.math.*,com.amsinc.ecsg.util.DateTimeUtility, java.util.*" %>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager"     scope="session"> </jsp:useBean>
<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session"> </jsp:useBean>
<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session"> </jsp:useBean>
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean"     scope="session"> </jsp:useBean>
   

<%-- ************** Data retrieval page setup begins here ****************  --%>

<%
  WidgetFactory widgetFactory = new WidgetFactory(resMgr, userSession);
  //cquinton 2/12/2013 add refdata manager for datepattern lookup
  ReferenceDataManager refData = ReferenceDataManager.getRefDataMgr();

  String onLoad = "";
  String focusField = "PayeeName";   // Default focus field
  boolean focusSet = false;

  // Various oid and status info from transaction and instruments used in
  // several places.
  String instrumentOid = "";         
  String transactionOid;
  String instrumentType;
  String instrumentStatus;
  String transactionType; 
  String transactionStatus;
  String rejectionIndicator = "";
  String rejectionReasonText = "";
  String prevTransStatusFromDoc = null;	//IAZ CM-451 03/24/09 Add 
  boolean           corpOrgHasMultipleAddresses   = false;
  String            corpOrgOid                    = null;

  boolean getDataFromDoc;              // Indicates if data is retrieved from the
                                       // input doc cache or from the database
  boolean instrumentSelected = false;  // Indicates if an instrument was selected.
  boolean phraseSelected = false;  // Indicates if a phrase was selected.
  boolean isStatusVerifiedPendingFX 	= false;	//Vshah - CR564

  DocumentHandler doc;

  String loginLocale = userSession.getUserLocale();
  String loginRights = userSession.getSecurityRights();

  //cquinton 2/12/2013 lookup locale specific datePattern description from refData manager
  String datePattern = userSession.getDatePattern();
  String datePatternDisplay = refData.getDescr("DATEPATTERN", datePattern, loginLocale);
  String dateWidgetOptions = "placeHolder:'" + datePatternDisplay + "'";

  // Dates are stored in the database as one field but displayed on the
  // page as 3 fields.  These variables hold the pieces for each date.
  String releaseDay = "";
  String releaseMonth = "";
  String releaseYear = "";
  String date = "";
  String maturityDay = "";
  String maturityMonth = "";
  String maturityYear = "";
    // page as 3 fields.  These variables hold the pieces for each date.
  String loanTermsFixedMaturityDay = "";
  String loanTermsFixedMaturityMonth = "";
  String loanTermsFixedMaturityYear = "";
  String PartySearchAddressTitle =resMgr.getTextEscapedJS("PartySearch.TabHeading",TradePortalConstants.TEXT_BUNDLE);
	String parentOrgID      = userSession.getOwnerOrgOid();
	String bogID            = userSession.getBogOid();
	String clientBankID     = userSession.getClientBankOid();
	String globalID         = userSession.getGlobalOrgOid();
	String ownershipLevel   = userSession.getOwnershipLevel();
 
 // FEC Date Variables Written by Edozie 
  String fecMaturityDay = "";
  String fecMaturityMonth = "";
  String fecMaturityYear = "";
 
  // Variables used for dealing with a terms party's accounts.
  boolean showPayeeAccts = false;
  String payeeAcctList = "";
  String payerAcctList = "";
  String acctNum = "";
  String acctCcy = "";
  
  String links = ""; //Added for Sidebar
  // Variables used for populating ref data dropdowns.
  String options;
  String defaultText;

  // These are the beans used on the page.
  TransactionWebBean transaction  = (TransactionWebBean) beanMgr.getBean("Transaction");
  InstrumentWebBean instrument    = (InstrumentWebBean) beanMgr.getBean("Instrument");
  TemplateWebBean template        = (TemplateWebBean) beanMgr.getBean("Template");
  TermsWebBean terms              = null;

  TermsPartyWebBean termsPartyPayee = beanMgr.createBean(TermsPartyWebBean.class, "TermsParty");
  TermsPartyWebBean termsPartyPayer = beanMgr.createBean(TermsPartyWebBean.class, "TermsParty");
  TermsPartyWebBean termsPartyPayeeBank = beanMgr.createBean(TermsPartyWebBean.class, "TermsParty");
   
  // Get the document from the cache.  We'll may use it to repopulate the 
  // screen if returning from another page, a save, validation, or any other
  // mediator called from this page.  Otherwise, we assume an instrument oid
  // and transaction oid was passed in.

  doc = formMgr.getFromDocCache();

  Debug.debug("doc from cache is " + doc.toString());

/******************************************************************************
  We are either entering the page or returning from somewhere.  These are the 
  conditions for how to populate the web beans.  Data comes from either the
  database or the doc cache (/In section) with some variation.

  Mode           Condition                      Populate Beans From
  -------------  ----------------------------   --------------------------------
  Enter Page     no /In/Transaction in doc      Instrument and Template web
                                                beans already populated, get
                                                data for Terms and TermsParty
                                                web beans from database

  return from    /In/NewPartyFromSearchInfo/PartyOid   doc cache (/In); also use Party
  Party Search     exists                       SearchInfo to lookup, and 
                                                populate a specific TermsParty
                                                web bean

  return from    /In/AddressSearchInfo/Address  doc cache (/In); use /In/Address
  Address Search   SearchPartyType exists       SearchInfo to populate a specific
                                                TermsParty web bean

  return from    /Error/maxerrorseverity < 1    Same as Enter Page (data is
  Transaction                                   retrieved from database)
    mediator
    (no error)

  return from    /Error/maxerrorseverity > 0    doc cache (/In)
  Transaction    
    mediator
    (error)

 ------------------------------------------------------------------------------
  Clear          /ClearBeneficary exists        Populate as above, also clear 
  Beneficiary                                   beneficiary fields.

  Instrument     /In/InstrumentSearchInfo/      Populate as above, also set
  Search           InstrumentData exists        related instrument id field.
******************************************************************************/
   
  // Assume we get the data from the doc.
  getDataFromDoc = true;


  String maxError = doc.getAttribute("/Error/maxerrorseverity");
  if (maxError != null && maxError.compareTo(
              String.valueOf(ErrorManager.WARNING_ERROR_SEVERITY)) <= 0) {
     // No errors, so don't get the data from doc.
     getDataFromDoc = false;
  }
  
  
  //In the case where reCertification/Authentication req'd on authorized, the page is reloaded with error
  //but the status of the transaction actually chnaged.
  else
  {
        String errorSectionText = null;
        if (doc.getComponent("/Error") != null)
      	{
      		errorSectionText = doc.getComponent("/Error").toString();
      	}
      	if (errorSectionText != null)
      	{
      		if (errorSectionText.indexOf(TradePortalConstants.CERTIFICATE_AUTH_REQ_ERR) != -1)
      		{
   				prevTransStatusFromDoc = doc.getAttribute("/In/Transaction/transaction_status"); 
      			getDataFromDoc = false;
      			transaction.getDataFromAppServer();
      		}
      	}
  }
  
  //ir cnuk113043991 - check to see if transaction needs to be refreshed
  // if so, refresh it and do not get data from doc as it is wrong
  if ( "Y".equals(doc.getAttribute("/Out/Refresh/Transaction")) ) {
     transaction.getDataFromAppServer();
     getDataFromDoc = false;
  }
  
  if (doc.getDocumentNode("/In/NewPartyFromSearchInfo/Type") != null) {
     // We have returned from the party search page.  Get data from doc cache
     getDataFromDoc = true;
  }
  if (doc.getDocumentNode("/In/AddressSearchInfo/AddressSearchPartyType") != null) {
     // We have returned from the party search page.  Get data from doc cache
     getDataFromDoc = true;
  }
  if (doc.getDocumentNode("/In/Transaction") == null) {
     // No /In/Transaction means we've never looked up the data.
     Debug.debug("No /In/Transaction section - get data from database");
     getDataFromDoc = false;
  }
  
  if (doc.getDocumentNode("/Out/PhraseLookupInfo") != null) {
     // A Looked up phrase exists.  Replace it in the /In document.
     Debug.debug("Found a looked-up phrase");
     getDataFromDoc = true;
     phraseSelected = true;

     String result = doc.getAttribute("/Out/PhraseLookupInfo/Result");
     if (result.equals(TradePortalConstants.INDICATOR_YES)) {
        // Take the looked up and appended phrase text from the /Out section
        // and copy it to the /In section.  This allows the beans to be
        // properly populated.
        String xmlPath = doc.getAttribute("/In/PhraseLookupInfo/text_path");
        xmlPath = "/In" + xmlPath;

        doc.setAttribute(xmlPath, doc.getAttribute("/Out/PhraseLookupInfo/NewText"));

        // If we returned from the phrase lookup without errors, set the focus
        // to the correct field.  Otherwise, don't set the focus so the user can
        // see the error.
        if (maxError != null && maxError.compareTo(String.valueOf(ErrorManager.WARNING_ERROR_SEVERITY)) < 0) {
           focusField = doc.getAttribute("/In/PhraseLookupInfo/text_field_name");
           focusSet = true;
        }
     } else {
        // We do nothing because nothing was looked up.  We stil want to get
        // the data from the doc.
     }
  }

  if (getDataFromDoc) {
     Debug.debug("Populating beans from doc cache");

     // Populate the beans from the input doc.
     try {
        instrument.populateFromXmlDoc(doc.getComponent("/In"));
        transaction.populateFromXmlDoc(doc.getComponent("/In"));
        template.populateFromXmlDoc(doc.getComponent("/In"));

        terms = (TermsWebBean) beanMgr.getBean("Terms");
        terms.populateFromXmlDoc(doc, "/In");

        String termsPartyOid;

        termsPartyOid = terms.getAttribute("c_FirstTermsParty");
        if (termsPartyOid != null && !termsPartyOid.equals(""))
        {
           termsPartyPayee.setAttribute("terms_party_oid", termsPartyOid);
           termsPartyPayee.getDataFromAppServer();
        }

        termsPartyOid = terms.getAttribute("c_SecondTermsParty");
        if (termsPartyOid != null && !termsPartyOid.equals(""))
        {
           termsPartyPayer.setAttribute("terms_party_oid", termsPartyOid);
           termsPartyPayer.getDataFromAppServer();
        }

        termsPartyOid = terms.getAttribute("c_ThirdTermsParty");
        if (termsPartyOid != null && !termsPartyOid.equals(""))
        {
           termsPartyPayeeBank.setAttribute("terms_party_oid", termsPartyOid);
           termsPartyPayeeBank.getDataFromAppServer();
        }

        DocumentHandler termsPartyDoc;

        termsPartyDoc = doc.getFragment("/In/Terms/FirstTermsParty");
        termsPartyPayee.loadTermsPartyFromDocTagsOnly(termsPartyDoc);
 
        termsPartyDoc = doc.getFragment("/In/Terms/SecondTermsParty");
        termsPartyPayer.loadTermsPartyFromDocTagsOnly(termsPartyDoc);

        termsPartyDoc = doc.getFragment("/In/Terms/ThirdTermsParty");
        termsPartyPayeeBank.loadTermsPartyFromDocTagsOnly(termsPartyDoc);

        releaseDay = doc.getAttribute("/In/Terms/release_day");
        releaseMonth = doc.getAttribute("/In/Terms/release_month");
        releaseYear = doc.getAttribute("/In/Terms/release_year");
        date = doc.getAttribute("/In/Terms/release_by_bank_on");;

        loanTermsFixedMaturityDay = doc.getAttribute("/In/Terms/loan_terms_fixed_maturity_day");
        loanTermsFixedMaturityMonth = doc.getAttribute("/In/Terms/loan_terms_fixed_maturity_month");
        loanTermsFixedMaturityYear = doc.getAttribute("/In/Terms/loan_terms_fixed_maturity_year");

     } catch (Exception e) {
        out.println("Contact Administrator: "
              + "Unable to reload data after returning to page. "
              + "Error is " + e.toString());
     }
  } else {
     Debug.debug("populating beans from database");
     // We will perform a retrieval from the database.
     // Instrument and Transaction were already retrieved.  Get
     // the rest of the data

     terms = transaction.registerCustomerEnteredTerms();

     String termsPartyOid;

     termsPartyOid = terms.getAttribute("c_FirstTermsParty");
     if (termsPartyOid != null && !termsPartyOid.equals("")) {
        termsPartyPayee.setAttribute("terms_party_oid", termsPartyOid);
        termsPartyPayee.getDataFromAppServer();
     }

     termsPartyOid = terms.getAttribute("c_SecondTermsParty");
     if (termsPartyOid != null && !termsPartyOid.equals("")) {
        termsPartyPayer.setAttribute("terms_party_oid", termsPartyOid);
        termsPartyPayer.getDataFromAppServer();
     }
     termsPartyOid = terms.getAttribute("c_ThirdTermsParty");
     if (termsPartyOid != null && !termsPartyOid.equals("")) {
        termsPartyPayeeBank.setAttribute("terms_party_oid", termsPartyOid);
        termsPartyPayeeBank.getDataFromAppServer();
     }

     // Because date fields are represented as three fields on the page,
     // we need to parse out the date values into three fields.  Pre-mediator
     // logic puts these three fields back together for the terms bean.
     
     date = transaction.getAttribute("payment_date");

     releaseDay = TPDateTimeUtility.parseDayFromDate(date);
     releaseMonth = TPDateTimeUtility.parseMonthFromDate(date);
     releaseYear = TPDateTimeUtility.parseYearFromDate(date);

     
  }
  
  
  String importIndicator = instrument.getAttribute("import_indicator");
  boolean isImport = importIndicator.equals(TradePortalConstants.INDICATOR_YES);
  boolean isExport = importIndicator.equals(TradePortalConstants.INDICATOR_NO);
  
  boolean defaultCheckBoxValue = false;
  
  // Don't format the amount if we are reading data from the doc
  String amount = terms.getAttribute("amount");
  String currency = terms.getAttribute("amount_currency_code");
  String displayAmount;
  
  String loanProceedsPmtAmount = terms.getAttribute("loan_proceeds_pmt_amount");
  String loanProceedsPmtCurrency = terms.getAttribute("loan_proceeds_pmt_curr");
  String displayLoanProceedsPmtAmount;
  
  String settlementFECAmount = terms.getAttribute("fec_amount");
  String settlementFECCurrency = loanProceedsPmtCurrency;
  String displaySettlementFECAmount;

  String loanFECAmount = terms.getAttribute("maturity_fec_amount");
  String loanFECCurrency = currency;
  String displayLoanFECAmount;

  String displaySettlementFECRate;
  String displayMaturityFECRate;
  String displayExchAmount; // DK CR-640 Rel7.1
  
  
  if(!getDataFromDoc) {
    displayAmount = TPCurrencyUtility.getDisplayAmount(amount, currency, loginLocale);
    displayLoanProceedsPmtAmount = TPCurrencyUtility.getDisplayAmount(loanProceedsPmtAmount, loanProceedsPmtCurrency, loginLocale);
    displaySettlementFECAmount = TPCurrencyUtility.getDisplayAmount(settlementFECAmount, settlementFECCurrency, loginLocale);
    displayLoanFECAmount = TPCurrencyUtility.getDisplayAmount(loanFECAmount, loanFECCurrency, loginLocale);
    displaySettlementFECRate = TPCurrencyUtility.getDisplayAmount(terms.getAttribute("fec_rate"), "", loginLocale);
    displayMaturityFECRate = TPCurrencyUtility.getDisplayAmount(terms.getAttribute("maturity_fec_rate"), "", loginLocale);
    displayExchAmount= TPCurrencyUtility.getDisplayAmount(terms.getAttribute("equivalent_exch_amount"), terms.getAttribute("equivalent_exch_amt_ccy"), loginLocale); // DK CR-640 Rel7.1
  }
  else {
    displayAmount = amount;
    displayLoanProceedsPmtAmount = loanProceedsPmtAmount;
    displaySettlementFECAmount = settlementFECAmount;
    displayLoanFECAmount = loanFECAmount;
    displaySettlementFECRate = terms.getAttribute("fec_rate");
    displayMaturityFECRate = terms.getAttribute("maturity_fec_rate");
    displayExchAmount= terms.getAttribute("equivalent_exch_amount"); 
  }

  String useMarketRate = terms.getAttribute("use_mkt_rate");
  String useFEC = terms.getAttribute("use_fec");
  String fecuseOther = terms.getAttribute("use_other");
  String requestMarketRateInd = terms.getAttribute("request_market_rate_ind"); 
  
  
  //cquinton 2/7/2013 added NewParty logic
  /*******************************************************
   * Load New Party
   *******************************************************/
  if (doc.getDocumentNode("/In/NewPartyFromSearchInfo/Type") != null &&
      doc.getDocumentNode("/In/NewPartyFromSearchOutput/PartyOid") != null) {

     // We have returned from the PartyDetailNew page.  Based on the returned 
     // data, RELOAD one of the terms party beans with the new party

     String termsPartyType = doc.getAttribute("/In/NewPartyFromSearchInfo/Type");
     String partyOid = doc.getAttribute("/In/NewPartyFromSearchOutput/PartyOid");

     Debug.debug("Returning from party search with " + termsPartyType 
           + "/" + partyOid);

     // Use a Party web bean to get the party data for the selected oid.
     PartyWebBean party = beanMgr.createBean(PartyWebBean.class,  "Party");
     party.setAttribute("party_oid", partyOid);
     party.getDataFromAppServer();

     DocumentHandler partyDoc = new DocumentHandler();
     party.populateXmlDoc(partyDoc);

     partyDoc = partyDoc.getFragment("/Party");

     // Based on the party type being returned (which we previously set)
     // reload one of the terms party web beans with the data from the
     // doc.
     if (termsPartyType.equals(TradePortalConstants.PAYEE)) {
        // use the "FromDocTagsOnly" so we don't overwrite the acct_currency
        termsPartyPayee.loadTermsPartyFromDocTagsOnly(partyDoc);

        // For a beneficary selection, the beneficiary bank should be populated
        // if there is a designated bank.
        loadDesignatedParty(partyDoc, termsPartyPayeeBank, beanMgr);
        focusField = "PayeeName";
        focusSet = true;

        // Now get the account data for this party and load it to the acct_choices field.  The
        // value is XML similar to a query result (i.e., /ResultSetRecord).

        termsPartyPayee.loadAcctChoices(partyOid, formMgr.getServerLocation(), resMgr);
     }
     if (termsPartyType.equals(TradePortalConstants.PAYER)) {
        termsPartyPayer.loadTermsPartyFromDoc(partyDoc);
	//rbhaduri - 13th June 06 - IR SAUG051361872
	termsPartyPayer.setAttribute("address_search_indicator", "N");

        focusField = "";
        focusSet = true;
        onLoad += "location.hash='#" + termsPartyType + "';";

        // Now get the account data for this party and load it to the acct_choices field.  The
        // value is XML similar to a query result (i.e., /ResultSetRecord).

        termsPartyPayer.loadAcctChoices(partyOid, formMgr.getServerLocation(), resMgr);
     }
     if (termsPartyType.equals(TradePortalConstants.PAYEE_BANK)) {
        termsPartyPayeeBank.loadTermsPartyFromDoc(partyDoc);
        focusField = "PayeeBankName";
        focusSet = true;
        onLoad += "location.hash='#" + termsPartyType + "';";
     }

  }
  
  
  BigInteger requestedSecurityRight = SecurityAccess.FUNDS_XFER_CREATE_MODIFY;
%>

<%@ include file="fragments/Transaction-PageMode.frag" %>

<%
    if (isTemplate) 
		corpOrgOid = template.getAttribute("owner_org_oid");
    else
		corpOrgOid = instrument.getAttribute("corp_org_oid");

    if (DatabaseQueryBean.getCount("address_oid", "address", "p_corp_org_oid = ?", false, new Object[]{corpOrgOid}) > 0 )
    {
		corpOrgHasMultipleAddresses = true;
    }   
  /*************************************************
  * Load address search info if we come back from address search page.
  * Reload applicant terms party web beans with the data from 
  * the address
  **************************************************/
  if (doc.getDocumentNode("/In/AddressSearchInfo/AddressSearchPartyType") != null) {
     String termsPartyType = doc.getAttribute("/In/AddressSearchInfo/AddressSearchPartyType");
     String addressOid = doc.getAttribute("/In/AddressSearchInfo/AddressOid");
     String primarySelected = doc.getAttribute("/In/AddressSearchInfo/ChoosePrimary");
     
     Debug.debug("Returning from address search with " + termsPartyType  + "/" + addressOid);

     // Use a Party web bean to get the party data for the selected oid.
     AddressWebBean address = null;  
    //rbhaduri - 9th Oct 06 - IR AOUG100368306 - added PRIMARY case for primary address selection
     if ((addressOid != null||primarySelected != null) && termsPartyType.equals(TradePortalConstants.PAYER)) {
        
        if (primarySelected.equals("PRIMARY")) {
		
 		CorporateOrganizationWebBean corporateOrg = beanMgr.createBean(CorporateOrganizationWebBean.class, "CorporateOrganization");
	   	corporateOrg.setAttribute("organization_oid", corpOrgOid);
	   	corporateOrg.getDataFromAppServer();
		termsPartyPayer.setAttribute("name",corporateOrg.getAttribute("name"));
           	termsPartyPayer.setAttribute("address_line_1",corporateOrg.getAttribute("address_line_1"));
           	termsPartyPayer.setAttribute("address_line_2",corporateOrg.getAttribute("address_line_2"));
     	   	termsPartyPayer.setAttribute("address_city",corporateOrg.getAttribute("address_city"));
       	   	termsPartyPayer.setAttribute("address_state_province",corporateOrg.getAttribute("address_state_province"));
       	   	termsPartyPayer.setAttribute("address_country",corporateOrg.getAttribute("address_country"));
       	   	termsPartyPayer.setAttribute("address_postal_code",corporateOrg.getAttribute("address_postal_code"));
       	   	termsPartyPayer.setAttribute("address_seq_num","1");
	}
	else {
        
        address = beanMgr.createBean(AddressWebBean.class, "Address");
        address.setAttribute("address_oid", addressOid);
        address.getDataFromAppServer();
        termsPartyPayer.setAttribute("name",address.getAttribute("name"));
        termsPartyPayer.setAttribute("address_line_1",address.getAttribute("address_line_1"));
        termsPartyPayer.setAttribute("address_line_2",address.getAttribute("address_line_2"));
     	termsPartyPayer.setAttribute("address_city",address.getAttribute("city"));
       	termsPartyPayer.setAttribute("address_state_province",address.getAttribute("state_province"));
       	termsPartyPayer.setAttribute("address_country",address.getAttribute("country"));
       	termsPartyPayer.setAttribute("address_postal_code",address.getAttribute("postal_code"));
       	termsPartyPayer.setAttribute("address_seq_num",address.getAttribute("address_seq_num"));
       	}
     }  
     focusField = "";
     focusSet = true;
     onLoad += "location.hash='#" + termsPartyType + "';";
     
  }


  if (doc.getDocumentNode("/In/ClearBeneficiary") != null) {
     // The user clicked the Clear Beneficiary button.  All the data has been reloaded
     // from the doc.  Now clear out the beneficiary fields.  (Unlike other "Clear"
     // actions handled by JavaScript, we also need to remove the account radio 
     // buttons from the HTML.)

     termsPartyPayee.setAttribute("name", "");
     termsPartyPayee.setAttribute("address_line_1", "");
     termsPartyPayee.setAttribute("address_line_2", "");
     termsPartyPayee.setAttribute("address_city", "");
     termsPartyPayee.setAttribute("address_state_province", "");
     termsPartyPayee.setAttribute("address_country", "");
     termsPartyPayee.setAttribute("address_postal_code", "");
     termsPartyPayee.setAttribute("OTL_customer_id", "");

     // By setting acct_choices to blank, the account radio buttons do not get
     // created.
     termsPartyPayee.setAttribute("acct_choices", "");
     termsPartyPayee.setAttribute("acct_num", "");
     termsPartyPayee.setAttribute("acct_currency", "");

  }
  
//if newTransaction, set session variable so it persists
  String newTransaction = null;
  if (doc.getDocumentNode("/Out/newTransaction")!=null) {
    newTransaction = doc.getAttribute("/Out/newTransaction");
    session.setAttribute("newTransaction", newTransaction);
    System.out.println("found newTransaction = "+ newTransaction);
  } 
  else {
    newTransaction = (String) session.getAttribute("newTransaction");
    if ( newTransaction==null ) {
      newTransaction = TradePortalConstants.INDICATOR_NO;
    }
    System.out.println("used newTransaction from session = " + newTransaction);
  }

  //cquinton 1/18/2013 remove old close action behavior
	 	
  String instrumentID = "";

  if (doc.getDocumentNode("/In/InstrumentSearchInfo/InstrumentData") != null)
  {
     // We have returned from the instrument search page.  Based on the returned 
     // data, retrieve the complete instrument ID of the selected instrument to 
     // populate the Related Instrument ID 
     String instrumentData = doc.getAttribute("/In/InstrumentSearchInfo/InstrumentData");

     instrumentID = InstrumentServices.parseForInstrumentId(instrumentData);
     focusField = "RelatedInstrumentId";
     focusSet = true;

     instrumentSelected = true;
  }


  transactionType = transaction.getAttribute("transaction_type_code");
  transactionStatus = transaction.getAttribute("transaction_status");
  
  rejectionIndicator = transaction.getAttribute("rejection_indicator");
  rejectionReasonText = transaction.getAttribute("rejection_reason_text");

  instrumentType = instrument.getAttribute("instrument_type_code");
  instrumentStatus = instrument.getAttribute("instrument_status");

  Debug.debug("Instrument Type " + instrumentType);
  Debug.debug("Instrument Status " + instrumentStatus);
  Debug.debug("Transaction Type " + transactionType);
  Debug.debug("Transaction Status " + transactionStatus);

  String interestToBePaid = terms.getAttribute("interest_to_be_paid");
  String loanTermsType = terms.getAttribute("loan_terms_type");
  String settleXferType = terms.getAttribute("funds_xfer_settle_type");
  //PPRAKASH IR POUI121941869 12/08/08 Add Begin
  //Template Dropdown Data Retrive
  
  %>
  <%@include file="fragments/TransactionGetUser.frag" %>
  <%
  //IAZ CR-586 09/01/10 Begin
  String confidentialClause = "";
  if (userSession.getSavedUserSession() == null)
  {
      if (TradePortalConstants.INDICATOR_NO.equals(loggedUser.getAttribute("confidential_indicator")))
          confidentialClause = "and t.confidential_indicator = 'N' ";
  }   	
  else
  {
      if (!TradePortalConstants.ADMIN.equals(userSession.getSavedUserSession().getSecurityType()))	
         if (TradePortalConstants.INDICATOR_NO.equals(loggedUser.getAttribute("subsid_confidential_indicator")))
            confidentialClause = "and t.confidential_indicator = 'N' ";
  }	  
  //IAZ CR-586 09/01/10 End	
  	  
   QueryListView queryTemplateListView = null;
   
   Vector templateList = null;
   int numberOfTemplates = 0;
   boolean isBankUser = true;
   try
   {
      
      //IAZ CR-586 09/01/10 and IR-VIUK101252424 10/16/10 Begin
      queryTemplateListView = (QueryListView) EJBObjectFactory.createClientEJB(formMgr.getServerLocation(), "QueryListView"); 

      boolean useTemplateGroup = false;
      if (userSession.getSavedUserSession() == null)
      {      
      	String checkTemplateGroupSql = "select tg.authorized_template_group_oid from USER_AUTHORIZED_TEMPLATE_GROUP tg where tg.p_user_oid = ? ";
        queryTemplateListView.setSQL(checkTemplateGroupSql, new Object[]{userSession.getUserOid()});
        queryTemplateListView.getRecords();
    
        if (queryTemplateListView.getRecordCount() != 0)
        	useTemplateGroup = true;
      }

      String sqlString;
      java.util.List<Object> sqlParamsLstt = new java.util.ArrayList();
      if (useTemplateGroup)
      {
      	sqlString = 
      "select i.original_transaction_oid, i.original_transaction_oid || '/' || t.template_oid as TransactionOid, t.name as TemplateName, t.ownership_level as ownershipLevel, t.ownership_type as ownershipType, i.instrument_type_code as instrumentType, p.name as partyName from template t, instrument i, terms_party p, USER_AUTHORIZED_TEMPLATE_GROUP tg where t.c_template_instr_oid = i.instrument_oid and t.default_template_flag = 'N' and i.a_counter_party_oid = p.terms_party_oid (+)  and i.instrument_type_code = 'FTRQ' and t.payment_templ_grp_oid = tg.A_TEMPLATE_GROUP_OID and tg.p_user_oid = ? "
      + confidentialClause + " order by upper(TemplateName)";
      	sqlParamsLstt.add(userSession.getUserOid());
      }
      else
      {
      	sqlString = "select i.original_transaction_oid, i.original_transaction_oid || '/' || t.template_oid as TransactionOid, t.name as TemplateName, t.ownership_level as ownershipLevel, t.ownership_type as ownershipType, i.instrument_type_code as instrumentType, p.name as partyName from template t, instrument i, terms_party p where t.c_template_instr_oid = i.instrument_oid and t.default_template_flag = 'N' and i.a_counter_party_oid = p.terms_party_oid (+)  and i.instrument_type_code = 'FTRQ' and p_owner_org_oid in (" 
      	+  "?,?,?,?) " + confidentialClause + " order by upper(TemplateName)"; 
      	sqlParamsLstt.add(userSession.getOwnerOrgOid());
      	sqlParamsLstt.add(userSession.getClientBankOid());
      	sqlParamsLstt.add(userSession.getGlobalOrgOid());
      	sqlParamsLstt.add(userSession.getBogOid());
      }
      queryTemplateListView.setSQL(sqlString, sqlParamsLstt);
      queryTemplateListView.getRecords();      
      //IAZ CR-586 09/01/10 and IR-VIUK101252424 10/16/10 End

      DocumentHandler templatesDocHandler = queryTemplateListView.getXmlResultSet();
      numberOfTemplates = queryTemplateListView.getRecordCount();
      templateList = templatesDocHandler.getFragments("/ResultSetRecord");

      sqlString = "select name from client_bank, users where user_oid = ? and p_owner_org_oid = organization_oid";
      queryTemplateListView.setSQL(sqlString, new Object[]{userSession.getUserOid()});
      queryTemplateListView.getRecords();
	  if (queryTemplateListView.getRecordCount() == 0)
	  {
	  	isBankUser = false;
	  }

   }
   catch (Exception e)
   {
      e.printStackTrace();

   }
   finally
   {
      try
      {
         if (queryTemplateListView != null)
         {
            queryTemplateListView.remove();
         }
      }
      catch (Exception e)
      {
		Debug.debug("Exception while querying the  " + e.toString());

      }
   }
   //PPRAKASH IR POUI121941869 12/08/08 Add End

  // Now determine the mode for how the page operates (readonly, etc.)
  //rbhaduri - 8th August 06 - IR AOUG100368306 - Moved above
  //BigInteger requestedSecurityRight = SecurityAccess.FUNDS_XFER_CREATE_MODIFY;
%>
  <%-- rbhaduri - 8th August 06 - IR AOUG100368306 - Moved above
		<%@ include file="fragments/Transaction-PageMode.frag" %>
	--%>

<%
 
  // Determine whether the corporate org customer
  // has multiple addresses.  If it does we display "Search For An Address" button.
  
DocumentHandler phraseLists = formMgr.getFromDocCache(TradePortalConstants.PHRASE_LIST_DOC);
  if (phraseLists == null) phraseLists = new DocumentHandler();

DocumentHandler spclInstrDocList = phraseLists.getFragment("/SpclInstr");
  if (spclInstrDocList == null) {
     spclInstrDocList = PhraseUtility.createPhraseList(
                                    TradePortalConstants.PHRASE_CAT_SPCL_INST,
                                    userSession, formMgr, resMgr);
     phraseLists.addComponent("/SpclInstr", spclInstrDocList);
  }

// Edozie's Code - CR375 8-30-07 Begins
DocumentHandler addlCondDocList = phraseLists.getFragment("/AddlCond");
  if (addlCondDocList == null) {
     addlCondDocList = PhraseUtility.createPhraseList(
                                    TradePortalConstants.PHRASE_CAT_ADDL_COND,
                                    userSession, formMgr, resMgr);
     phraseLists.addComponent("/AddlCond", addlCondDocList);
  }
  formMgr.storeInDocCache(TradePortalConstants.PHRASE_LIST_DOC, phraseLists);
  //Edozie's code - CR375 8-30-07 Ends
  
 // Retrieve the currency code 
  //String currency    = terms.getAttribute("amount_currency_code");
  	String selectedInstrument = transaction.getAttribute("transaction_oid") + "/" +  instrument.getAttribute("instrument_oid") + "/" + transactionType;
	selectedInstrument=EncryptDecrypt.encryptStringUsingTripleDes(selectedInstrument, userSession.getSecretKey());
	 boolean isFXeditable = (TradePortalConstants.TRANS_STATUS_FX_THRESH_EXCEEDED.equals(transaction.getAttribute("transaction_status")) || TradePortalConstants.TRANS_STATUS_AUTH_PEND_MARKET_RATE.equals(transaction.getAttribute("transaction_status")));

%>

<%-- ********************* HTML for page begins here ********************* --%>
<%String buttonClicked = request.getParameter("buttonName");
Vector error = null;  error = doc.getFragments("/Error/errorlist/error");%>



  <%@include file="fragments/MultiPartMode.frag" %>

<% 
  // Some of the retrieval logic above may have set a focus field.  Otherwise,
  // we'll use the initial value for focus.

  if (multiPartMode && !focusSet)
  {
     if (whichPart.equals("") || whichPart.equals(TradePortalConstants.PART_ONE))
     {
        focusField = "PayeeName";
     }
     else if (whichPart.equals(TradePortalConstants.PART_TWO))
     {
        focusField = "TransactionCurrency";
     }
  }

  if (!isReadOnly) {
	 //onLoad += "updateFundingAmount();";
  }
  
%>

<%-- Body tag included as part of common header --%>
<%
  // The navigation bar is only shown when editing templates.  For transactions
  // it is not shown ti minimize the chance of leaving the page without properly
  // unlocking the transaction.
  String showNavBar = TradePortalConstants.INDICATOR_NO;
  if (isTemplate)
  {
     showNavBar = TradePortalConstants.INDICATOR_YES;
  }

  // Auto save the form when time-out if not readonly.  
  // (Header.jsp will check for auto-save setting of the corporate org).
  String autoSaveFlag = isReadOnly ? TradePortalConstants.INDICATOR_NO : TradePortalConstants.INDICATOR_YES;
  String templateFlag = isTemplate ? TradePortalConstants.INDICATOR_YES : TradePortalConstants.INDICATOR_NO;
%>

<% 
   //Include ReAuthentication frag in case re-authentication is required for complete
   //authorization of transactions for this client
   //cr498 change certNewLINk for certAuthURL
   String certAuthURL = "";
   //if (TradePortalConstants.TRANS_STATUS_REQ_AUTHENTICTN.equals(transaction.getAttribute("transaction_status")))
   Cache reCertCache = (Cache)TPCacheManager.getInstance().getCache(TradePortalConstants.CLIENT_BANK_CACHE);
   DocumentHandler CBCResult = (DocumentHandler)reCertCache.get(userSession.getClientBankOid());	
   //cr498 - any instrument/transaction could require reauthentication
   String requireTranAuth = CBCResult.getAttribute("/ResultSetRecord(0)/REQUIRE_TRAN_AUTH");
   boolean requireAuth = InstrumentAuthentication.requireTransactionAuthentication(requireTranAuth,InstrumentAuthentication.TRAN_AUTH__FTRQ_ISS);
   if (requireAuth) {
      // If Transaction just got successfully authroized (so Tran status just got changed to REQ_AUTHENTICTN 
      // from somnething else, open the authentication window once Transaction Page reloads.
      //if (TradePortalConstants.TRANS_STATUS_READY_TO_AUTHORIZE.equals(prevTransStatusFromDoc))
      if (((InstrumentServices.isNotBlank(prevTransStatusFromDoc)))&&(!TradePortalConstants.TRANS_STATUS_REQ_AUTHENTICTN.equals(prevTransStatusFromDoc)))
      {
      		onLoad = onLoad + "openReauthenticationWindow('" + certAuthURL + "')";
      }
      
   }


   //load account owner information when the page loads
   //AAlubala - IR#BEUL040548205 - 04/12/11
   onLoad = onLoad + " displayAccountsOnLoad();";
%>
<%
  String pageTitleKey="";
if ( TradePortalConstants.INDICATOR_YES.equals(newTransaction) ) {
    pageTitleKey = "SecondaryNavigation.NewInstruments";
    
    if (isTemplate){
         if(userSession.getSecurityType().equals(TradePortalConstants.ADMIN)){
         userSession.setCurrentPrimaryNavigation("AdminNavigationBar.RefData");
    }else{
       userSession.setCurrentPrimaryNavigation("NavigationBar.RefData");
    }
    }else
    userSession.setCurrentPrimaryNavigation("NavigationBar.NewInstruments");
  } else {
    pageTitleKey = "SecondaryNavigation.Instruments";
    if (isTemplate){
         if(userSession.getSecurityType().equals(TradePortalConstants.ADMIN)){
         userSession.setCurrentPrimaryNavigation("AdminNavigationBar.RefData");
    }else{
       userSession.setCurrentPrimaryNavigation("NavigationBar.RefData");
    }
    }else 
    userSession.setCurrentPrimaryNavigation("NavigationBar.Transactions");
  }
  String helpUrl = "customer/issue_intl_payments.htm";

  //cquinton 11/19/2012 pass info for recent instruments into header
  String newRecentTransactionOid = transaction.getAttribute("transaction_oid");
  String newRecentInstrumentId = instrument.getAttribute("complete_instrument_id");
  String newRecentTransactionType = transaction.getAttribute("transaction_type_code");
  String newRecentTransactionStatus = transaction.getAttribute("transaction_status");
%>

<jsp:include page="/common/Header.jsp">
  <jsp:param name="autoSaveFlag"           value="<%=autoSaveFlag%>" />
  <jsp:param name="templateFlag"           value="<%=templateFlag%>" />
  <jsp:param name="newRecentTransactionOid" value="<%=newRecentTransactionOid%>" />
  <jsp:param name="newRecentInstrumentId"  value="<%=newRecentInstrumentId%>" />
  <jsp:param name="newRecentTransactionType" value="<%=newRecentTransactionType%>" />
  <jsp:param name="newRecentTransactionStatus" value="<%=newRecentTransactionStatus%>" />
</jsp:include>

<%
if (requireAuth) {
%>   
      <%@ include file="/logon/fragments/openReauthenticationWindow.frag" %>
<%
}	  
%>

<div class="pageMain">
<div class="pageContent">

<%
  if(isTemplate) {
		  String pageTitle = resMgr.getText("common.template", TradePortalConstants.TEXT_BUNDLE);
		  StringBuffer title = new StringBuffer();
		  title.append(pageTitle);
		  title.append( " : " );
		  title.append( refData.getDescr(TradePortalConstants.INSTRUMENT_TYPE,  instrument.getAttribute("instrument_type_code"),  loginLocale) );
		  title.append( " - " );
		  title.append(template.getAttribute("name") );
		
		  String transactionSubHeader = title.toString();
		  String returnUrl=	formMgr.getLinkAsUrl("goToInstrumentCloseNavigator", response);
  		
  		%>
		<jsp:include page="/common/PageHeader.jsp">
				<jsp:param name="titleKey" value="<%= pageTitle%>"/>
                                <jsp:param name="helpUrl"  value="customer/issue_intl_payments.htm" />
  		</jsp:include>
  		
    <jsp:include page="/common/PageSubHeader.jsp">
      <jsp:param name="title" value="<%= transactionSubHeader %>" />
    </jsp:include>
	 <%}else{ %>
	<jsp:include page="/common/PageHeader.jsp">
	   <jsp:param name="titleKey" value="<%=pageTitleKey%>" />
	   <jsp:param name="item1Key" value="SecondaryNavigation.Instruments.InternationalPayment" />
	   <jsp:param name="helpUrl"  value="<%=helpUrl%>" />
	</jsp:include>


	<jsp:include page="/common/TransactionSubHeader.jsp" />

<%} %>		
<form id="TransactionFTRQ" name="TransactionFTRQ" method="post" data-dojo-type="dijit.form.Form" action="<%=formMgr.getSubmitAction(response)%>">


<%

  if (requireAuth) {
%> 

 <input type=hidden name="reCertification" value="<%=TradePortalConstants.INDICATOR_NO%>">
 <input type=hidden name="reCertOK">
 <input type=hidden name="logonResponse">
 <input type=hidden name="logonCertificate">
   <%--AAlubala CR711 Rel 8.0 01/27/2012
  Add proxy value but do not set a value to it.
  The value will be set on the fly if the transaction requires to be 
  signed by 2FA token --%>
   <input type=hidden name="proxy">

<%
 }
%>    
 <%-- IAZ 02/18/09 and 05/15/09 End --%>
 
  <input type=hidden value="" name=buttonName>
<%-- error section goes above form content --%>
<div class="formArea">
<jsp:include page="/common/ErrorSection.jsp" />

<div class="formContent">
<%
  // Store values such as the userid, security rights, and org in a
  // secure hashtable for the form.  Also store instrument and transaction
  // data that must be secured.
  Hashtable secureParms = new Hashtable();
  secureParms.put("login_oid", userSession.getUserOid());
  secureParms.put("owner_org_oid", userSession.getOwnerOrgOid());
  secureParms.put("login_rights", loginRights);

  secureParms.put("instrument_oid",        instrument.getAttribute("instrument_oid"));
  secureParms.put("instrument_type_code",  instrumentType);
  secureParms.put("instrument_status",     instrumentStatus);
  secureParms.put("corp_org_oid",  corpOrgOid);

  secureParms.put("transaction_oid",       transaction.getAttribute("transaction_oid"));
  secureParms.put("transaction_type_code", transactionType);
  secureParms.put("transaction_status",    transactionStatus);
  
  secureParms.put("transaction_instrument_info", 
  	transaction.getAttribute("transaction_oid") + "/" + 
	instrument.getAttribute("instrument_oid") + "/" + 
	transactionType);

    //secureParms.put("bankBranch",corpOrg.getAttribute("first_op_bank_org")); 
    secureParms.put("userOid",userSession.getUserOid());
    secureParms.put("securityRights",userSession.getSecurityRights()); 
    secureParms.put("clientBankOid",userSession.getClientBankOid()); 
    secureParms.put("ownerOrg",userSession.getOwnerOrgOid());
    
  // If the terms record doesn't exist, set its oid to 0.
  String termsOid = terms.getAttribute("terms_oid");

  if (termsOid == null)
  {
     termsOid = "0";
  }
  secureParms.put("terms_oid", termsOid);

  if (isTemplate)
  {
     secureParms.put("template_oid", template.getAttribute("template_oid"));
     secureParms.put("opt_lock",     template.getAttribute("opt_lock"));
  }
%>

  <%@include file="fragments/MultiPartModeFormElements.frag" %>

<%
  if (isTemplate)
  {
%>
    <%@ include file="fragments/TemplateHeader.frag" %>
<%
    if (!multiPartMode)
    {
%>
       <%--  >%@ include file="fragments/Transaction-FTRQ-ISS-Links.frag" % --%>
<%
    }
  }
  else
  {
%>

 <% if (!multiPartMode){
%>
       <%--  >%@ include file="fragments/Transaction-FTRQ-ISS-Links.frag" % --%>
<%
    }
%>
<% }
%>

 <%@ include file="fragments/Instruments-AttachDocuments.frag" %>

	
<%

  if (rejectionIndicator.equals(TradePortalConstants.INDICATOR_YES)
  	|| rejectionIndicator.equals(TradePortalConstants.INDICATOR_X))
  {
%>
<%=widgetFactory.createSectionHeader("0", "Rejection Reason") %>
     <%@ include file="fragments/Transaction-RejectionReason.frag" %>
     </div>
<%
  }
String extraPartTags = request.getParameter("extraPartTags");
%> 

<% //CR 821 Added Repair Reason Section  
StringBuffer repairReasonWhereClause = new StringBuffer();
int  repairReasonCount = 0;
	
	/*Get all repair reason's count from transaction history table*/
	repairReasonWhereClause.append("p_transaction_oid = ? ");
	repairReasonWhereClause.append(" and REPAIR_REASON is not null order by ACTION_DATETIME");

	Debug.debug("repairReasonWhereCondition query : " + repairReasonWhereClause);
	repairReasonCount = DatabaseQueryBean.getCount("A_USER_OID", "transaction_history", repairReasonWhereClause.toString(), false, new Object[]{transaction.getAttribute("transaction_oid")});
			
if ( !(TradePortalConstants.TRANS_STATUS_PROCESSED_BY_BANK.equalsIgnoreCase(transactionStatus)) && repairReasonCount>0  ){%>
	<%=widgetFactory.createSectionHeader("", "TransactionHistory.RepairReason") %>
		<%@ include file="fragments/Transaction-RepairReason.frag" %>
	</div>
<%} %> 
	
	
	<%= widgetFactory.createSectionHeader( "1","FundsTransferRequest.General") %> 
	      <%@ include file="fragments/Transaction-FTRQ-ISS-General.frag" %>
	</div>

	
	<%= widgetFactory.createSectionHeader( "2","FundsTransferRequest.PaymentTerms") %> 	
     <%@ include file="fragments/Transaction-FTRQ-ISS-PaymentTerms.frag" %>
     </div>
     
     
     <%= widgetFactory.createSectionHeader( "3","FundsTransferRequest.FundingDetails") %> 
     <%@ include file="fragments/Transaction-FTRQ-ISS-FundingDetails.frag" %>
     </div>
     
      <%= widgetFactory.createSectionHeader( "4","FundsTransferRequest.OtherConditions") %> 
	 <%@ include file="fragments/Transaction-FTRQ-ISS-OtherConditions.frag" %>
	 </div>

	  <%= widgetFactory.createSectionHeader( "5","FundsTransferRequest.InstructionstoBank") %> 
		 <%@ include file="fragments/Transaction-FTRQ-ISS-BankInstructions.frag" %>
	 </div>
<%
     // Only display the bank defined section if the user is an admin user
     //rbhaduri - 1st Dec 08 - CR-434 - Do not dispaly it now. Placed the inculde inside this block
     if (userSession.getSecurityType().equals(TradePortalConstants.ADMIN))
     {
     }
%>


 <% if (TradePortalConstants.TRANS_STATUS_PROCESSED_BY_BANK.equalsIgnoreCase(transactionStatus) && repairReasonCount>0  ){%>
		<%=widgetFactory.createSectionHeader("6", "TransactionHistory.RepairReason", null, true) %>
		<%@ include file="fragments/Transaction-RepairReason.frag" %>
		</div>
    <%} %> 
     


  <%@ include file="fragments/PhraseLookupPrep.frag" %>
  <%@ include file="fragments/PartySearchPrep.frag" %>
</div> <%-- formContentt Area closes here --%>
</div> <%--formArea--%>
<%-- side bar --%>
				<%-- <%boolean showLinks = false; %> --%> 
				<div class="formSidebar" data-dojo-type="widget.FormSidebar" data-dojo-props="title: ' ', form: 'TransactionFTRQ'">
					<jsp:include page="/common/Sidebar.jsp">
						<jsp:param value="<%=links %>" name="links"/>
				        <jsp:param name="isReadOnly" value="<%=isReadOnly%>" />
						<jsp:param name="certAuthURL" value="<%=certAuthURL%>" />
						<jsp:param name="buttonPressed" value="<%=buttonClicked%>" />
						<jsp:param name="error" value="<%=error%>" />
						<jsp:param name="formName" value="0" />
						<jsp:param name="showLinks" value="true" />
						<jsp:param name="isTemplate" value="<%=isTemplate%>"/>
						<jsp:param name="isNewTransaction" value="<%=newTransaction%>"/>
				    </jsp:include>
				</div> <%--closes sidebar area--%>
<%= formMgr.getFormInstanceAsInputField("Transaction-FTRQForm", secureParms) %>
<input type="hidden" name="selection" />
<input type="hidden" name="ChoosePrimary" />
</form>
</div><%--closes pageContent area--%>
</div><%--closes pageMain area--%>

<jsp:include page="/common/Footer.jsp">
   <jsp:param name="autoSaveFlag"           value="<%=autoSaveFlag%>" />
   <jsp:param name="templateFlag"           value="<%=templateFlag%>" />
   <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>
<jsp:include page="/common/SidebarFooter.jsp"/>

<div id="PartySearchDialog"></div>

<script type="text/javascript">

  var local = {};

  function setChargeOther(){
	  dijit.getEnclosingWidget(document.forms[0].chargeOther).set('checked',true);
  } 

  require(["dojo/_base/array", "dojo/_base/xhr", "dojo/dom", "dojo/dom-construct", "dojo/dom-style", "dijit/registry", "t360/common"],
      function(arrayUtil, xhr, dom, domConstruct, domStyle, registry, common) {
<%
  if (!isReadOnly && InstrumentServices.isNotBlank(focusField)) {
%>
  require(["dojo/ready"],
    function( ready ) {
      ready(function() {
    	  var display;
    	  if('<%=requestMarketRateVisibility%>'=='hidden') display='none';
    	  if('<%=requestMarketRateVisibility%>'=='visible') display='block';
    	  
    	  dojo.style("requestMarketRate", "visibility", "<%=requestMarketRateVisibility%>");
    	  dojo.style("requestMarketRate", "display", display);
    	  
        var focusFieldId = '<%= focusField %>';
        if ( focusFieldId ) {
          var focusField = registry.byId(focusFieldId);
          focusField.focus();
        }
		displayAccountsOnLoad();
      });
  });
<%
  }
%>

    <%-- cquinton 1/4/2013 after populating the party fields we also  --%>
    <%--  need to get party account information for population here --%>
    local.chooseBeneficiary = function(selectedRowKey) {	
      <%-- first remove the existing beneficiary account number field and  --%>
      <%--  replace with a loading... message --%>
      var beneAccounts = dom.byId("beneAccountsData");
      var beneAccountsWidgets = registry.findWidgets(beneAccounts);
      arrayUtil.forEach(beneAccountsWidgets, function(entry, i) {
        entry.destroyRecursive(true);
      });
      domConstruct.empty(beneAccounts);
      domStyle.set("beneAccountsDataLoading", 'display', 'inline-block');
      <%-- make an ajax call to get the account info --%>
      common.attachAjaxResult('beneAccountsData',
        "/portal/transactions/Transaction-FTRQ-ISS-BeneAcctData.jsp?partyType=PAE&partyRowKey="+selectedRowKey+"&isTemplate=<%=isTemplate%>",
        local.beneAccountsDataLoadedCallback);

      <%-- also load the beneficiary bank data --%>
      <%-- this is done by getting the designated party data from the selected party --%>
      <%--  and then putting the output into the beneficiary bank fields --%>
      <%-- todo: should use servlet and json for this! --%>
      <%-- todo: better xml parsing... (if no json) --%>
      function getDataValue(data, attributeName) {
        var openTag = '<'+attributeName+'>';
        var closeTag = '</'+attributeName+'>';
        var myValue = "";
        var openTagIdx = data.indexOf(openTag);
        if ( openTagIdx >= 0 ) {
          myValue = data.substring( openTagIdx+openTag.length);
          var closeTagIdx = myValue.indexOf(closeTag);
          if ( closeTagIdx>=0 ) {
            myValue = myValue.substring( 0, myValue.indexOf(closeTag) );
          }
        }
        return myValue;
      }
      function setWidgetValue(widgetId, data, attributeName) {
        var myValue = getDataValue(data, attributeName);
        var myWidget = registry.byId(widgetId);
        if ( myWidget ) {
          myWidget.set('value',myValue);
        }
      }
      function setInputValue(domId, data, attributeName) {
        var myValue = getDataValue(data, attributeName);
        var myInput = dom.byId(domId);
		 document.getElementsByName(domId)[0].value=myValue;
		<%-- 
        if ( myInput ) {
          myInput.value = myValue;

        } --%>
      }
      xhr.get({
        url: "/portal/transactions/GetSelectedDesignatedParty.jsp?partyRowKey="+selectedRowKey,
        load: function(data) {
          if ( data.indexOf("SUCCESS")>=0 ) {
            <%-- parse the data and set the fields --%>
            setWidgetValue("PayeeBankName", data, "name");
            setWidgetValue("PayeeBankAddressLine1", data, "address_line_1");
            setWidgetValue("PayeeBankAddressLine2", data, "address_line_2");
            setWidgetValue("PayeeBankCity", data, "address_city");
            setWidgetValue("PayeeBankStateProvince", data, "address_state_province");
            setWidgetValue("PayeeBankCountry", data, "address_country");
            setWidgetValue("PayeeBankPostalCode", data, "address_postal_code");
            setInputValue("PayeeBankOTLCustomerId", data, "OTL_customer_id");

          }
          else if ( data.indexOf("NOTFOUND")>=0 ) {
            <%-- do nothing --%>
          }
          else {
            var errMsg = "Unknown error getting setting bank beneficiary.";
            if ( data.indexOf("<errorMessage>")>=0 ) {
              errMsg = data.substring( data.indexOf('<errorMessage>')+14 );
              errMsg = errMsg.substring( 0, errMsg.indexOf('</errorMessage>') );
            }
            alert(errMsg);
          }
        },
        error: function(err) {
          <%-- todo: make a custom dialog for errors --%>
          <%-- would be nice to display this in a nice fashion,  --%>
          <%--  probably also allow the user to avoid the error in future --%>
          <%--  an alternative would be to have some kind of error display at bottom of the page??? --%>
          alert('Error retrieving remote content:'+err);
        }
      });

    }

    <%-- after loading account data, remove the loading message --%>
    local.beneAccountsDataLoadedCallback = function() {
      domStyle.set("beneAccountsDataLoading", 'display', 'none');
    }

  });

  var itemid;
  var section;
  function SearchParty(identifierStr, sectionName,partyType){
	  
    itemid = String(identifierStr);
    section = String(sectionName);
    partyType = String(partyType);
    require(["dojo/dom", "t360/dialog"], function(dom, dialog ) {

      <%-- cquinton 2/7/2013 --%>
      <%-- set the SearchPartyType input so it is included on new party form submit --%>
      var searchPartyTypeInput = dom.byId("SearchPartyType");
      if ( searchPartyTypeInput ) {
        searchPartyTypeInput.value=partyType;
      }

      if ( partyType == "PAE" ) {
        <%-- when the beneficiary do extra callback --%>
        dialog.open('PartySearchDialog', '<%=PartySearchAddressTitle%>',
                    'PartySearch.jsp',
                    ['returnAction','filterText','partyType','unicodeIndicator','itemid','section'],
                    ['selectTransactionParty','',partyType,'<%=TradePortalConstants.INDICATOR_NO%>',itemid,section], <%-- parameters --%>
                    'select', local.chooseBeneficiary);
      }
      else {
        dialog.open('PartySearchDialog', '<%=PartySearchAddressTitle%>',
                    'PartySearch.jsp',
                    ['returnAction','filterText','partyType','unicodeIndicator','itemid','section'],
                    ['selectTransactionParty','',partyType,'<%=TradePortalConstants.INDICATOR_NO%>',itemid,section]); <%-- parameters --%>
      }
    });
  }

  <%-- cquinton 3/2/2013 local phrase lookup --%>
  require(["t360/phraseLookup"],
          function( phraseLookup ) {
    local.phraseLookupAppend = function(phraseSelect, textArea, textAreaMaxLength ) {
      phraseLookup.appendPhrase( phraseSelect, textArea, textAreaMaxLength, "_errorSection" );
    };
  });

</script>

</body>
</html>

<%
   // Finally, reset the cached document to eliminate carryover of
   // information to the next visit of this page.
   formMgr.storeInDocCache("default.doc", new DocumentHandler());
%>

<%--cquinton 2/7/2013 add back DesignatedPartyLookup--%>
<%@include file="fragments/DesignatedPartyLookup.frag"%>
