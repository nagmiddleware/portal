<%--
*******************************************************************************
               Export Letter of Credit Assignment Page

  Description:
  This is the main driver for the Export Letter of Credit Assignment page.  
  It handles data retrieval of terms and terms parties (or retrieval from the 
  input document) and creates the html page to display all the data for an 
  Export Letter of Credit Assignment.
*******************************************************************************
--%>

<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,java.util.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.html.*,java.math.*,
                 com.amsinc.ecsg.util.DateTimeUtility" %>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager"
    scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" 
   scope="session">
</jsp:useBean>


<%-- ************** Data retrieval page setup begins here ****************  --%>

<%
WidgetFactory widgetFactory = new WidgetFactory(resMgr);
  String  onLoad     = "";
  String  focusField = "AssigneeName";   // Default focus field
  boolean focusSet   = false;

  // Various oid and status info from transaction and instruments used in
  // several places.
  String instrumentOid = "";         
  String transactionOid;
  String instrumentType;
  String instrumentStatus;
  String transactionType; 
  String transactionStatus;
  String rejectionIndicator        = "";
  String rejectionReasonText       = "";
  String links = ""; //Added for Sidebar
  
  boolean getDataFromDoc;          // Indicates if data is retrieved from the
                                   // input doc cache or from the database
  boolean phraseSelected = false;  // Indicates if a phrase was selected.
  String PartySearchAddressTitle =resMgr.getTextEscapedJS("PartySearch.TabHeading",TradePortalConstants.TEXT_BUNDLE);
  DocumentHandler doc;

  String loginLocale = userSession.getUserLocale();
  String loginRights = userSession.getSecurityRights();

  String origTransOid = null;

  // Variables for the LC Amount and Assignment Amount fields
  String   assignmentAmount = null;
  String   originalAmount   = null;
  String   currencyCode     = null;
  String   amount           = null;

  // Variables for the Partial Payments section
  boolean defaultRadioButtonValue1 = false;
  boolean defaultRadioButtonValue2 = false;
  String  partialPaymentsType      = null;

  // Variables used for populating ref data dropdowns.
  String options;
  String defaultText;

  // These are the beans used on the page.

  TransactionWebBean transaction  = (TransactionWebBean)
                                      beanMgr.getBean("Transaction");
  InstrumentWebBean instrument    = (InstrumentWebBean)
                                      beanMgr.getBean("Instrument");
  TemplateWebBean template        = (TemplateWebBean)
                                      beanMgr.getBean("Template");
  TermsWebBean terms              = null;

  TermsPartyWebBean termsPartyAssignee = beanMgr.createBean(TermsPartyWebBean.class, "TermsParty");
  TermsPartyWebBean termsPartyAssigneeBank = beanMgr.createBean(TermsPartyWebBean.class, "TermsParty");

  // We need the amount of the original transaction to display in the LC Amount 
  // fields.  Create a webbean to hold this data.
  TransactionWebBean originalTransaction = beanMgr.createBean(TransactionWebBean.class, "Transaction");

  // Get the document from the cache.  We'll may use it to repopulate the 
  // screen if returning from another page, a save, validation, or any other
  // mediator called from this page.  Otherwise, we assume an instrument oid
  // and transaction oid was passed in.

  doc = formMgr.getFromDocCache();

/******************************************************************************
  We are either entering the page or returning from somewhere.  These are the 
  conditions for how to populate the web beans.  Data comes from either the
  database or the doc cache (/In section) with some variation.

  Mode           Condition                      Populate Beans From
  -------------  ----------------------------   --------------------------------
  Enter Page     no /In/Transaction in doc      Instrument and Template web
                                                beans already populated, get
                                                data for Terms and TermsParty
                                                web beans from database

  return from    /In/NewPartyFromSearchInfo/PartyOid   doc cache (/In); also use Party
  Party Search     exists                       SearchInfo to lookup, and 
                                                populate a specific TermsParty
                                                web bean

  return from    /Out/PhraseLookupInfo exists   doc cache (/In); but use Phrase
  Phrase Lookup                                 LookupInfo text (from /Out) to
                                                replace a specific phrase text
                                                in the /In document before 
                                                populating

  return from    /Error/maxerrorseverity < 1    Same as Enter Page (data is
  Transaction                                   retrieved from database)
    mediator
    (no error)

  return from    /Error/maxerrorseverity > 0    doc cache (/In)
  Transaction    
    mediator
    (error)
******************************************************************************/

  // Assume we get the data from the doc.
  getDataFromDoc = true;
  
  Vector error = null;
  error = doc.getFragments("/Error/errorlist/error");
  Debug.debug("error - "+error);
  
  String bButtonPressed = doc.getAttribute("/In/Update/ButtonPressed");

  String maxError = doc.getAttribute("/Error/maxerrorseverity");
  if (maxError != null && maxError.compareTo(String.valueOf(ErrorManager.WARNING_ERROR_SEVERITY)) <= 0)
  {
     // No errors, so don't get the data from doc.
     getDataFromDoc = false;
  }
  //ir cnuk113043991 - check to see if transaction needs to be refreshed
  // if so, refresh it and do not get data from doc as it is wrong
  if ( "Y".equals(doc.getAttribute("/Out/Refresh/Transaction")) ) {
     transaction.getDataFromAppServer();
     getDataFromDoc = false;
  }
  if (doc.getDocumentNode("/In/NewPartyFromSearchInfo/Type") != null) {
     // We have returned from the party search page.  Get data from doc cache
     getDataFromDoc = true;
  }
  if (doc.getDocumentNode("/In/Transaction") == null)
  {
     // No /In/Transaction means we've never looked up the data.
     Debug.debug("No /In/Transaction section - get data from database");
     getDataFromDoc = false;
  }
  if (doc.getDocumentNode("/Out/PhraseLookupInfo") != null)
  {
     // A Looked up phrase exists.  Replace it in the /In document.
     Debug.debug("Found a looked-up phrase");
     getDataFromDoc = true;
     phraseSelected = true;

     String result = doc.getAttribute("/Out/PhraseLookupInfo/Result");
     if (result.equals(TradePortalConstants.INDICATOR_YES))
     {
        // Take the looked up and appended phrase text from the /Out section
        // and copy it to the /In section.  This allows the beans to be 
        // properly populated.
        String xmlPath = doc.getAttribute("/In/PhraseLookupInfo/text_path");
        xmlPath = "/In" + xmlPath;

        doc.setAttribute(xmlPath, doc.getAttribute("/Out/PhraseLookupInfo/NewText"));

        // If we returned from the phrase lookup without errors, set the focus
        // to the correct field.  Otherwise, don't set the focus so the user can
        // see the error.
        if (maxError != null && maxError.compareTo(String.valueOf(ErrorManager.WARNING_ERROR_SEVERITY)) < 0)
        {
           focusField = doc.getAttribute("/In/PhraseLookupInfo/text_field_name");
           focusSet = true;
        }
     }
     else
     {
        // We do nothing because nothing was looked up.  We stil want to get
        // the data from the doc.
     }
  }

  if (getDataFromDoc)
  {
     Debug.debug("Populating beans from doc cache");

     // Populate the beans from the input doc.
     try
     {
        instrument.populateFromXmlDoc(doc.getComponent("/In"));
        transaction.populateFromXmlDoc(doc.getComponent("/In"));
        template.populateFromXmlDoc(doc.getComponent("/In"));

        terms = (TermsWebBean) beanMgr.getBean("Terms");
        terms.populateFromXmlDoc(doc, "/In");

        String termsPartyOid;

        termsPartyOid = terms.getAttribute("c_FirstTermsParty");
        if (termsPartyOid != null && !termsPartyOid.equals(""))
        {
           termsPartyAssignee.setAttribute("terms_party_oid", termsPartyOid);
           termsPartyAssignee.getDataFromAppServer();
        }
        termsPartyOid = terms.getAttribute("c_SecondTermsParty");
        if (termsPartyOid != null && !termsPartyOid.equals(""))
        {
           termsPartyAssigneeBank.setAttribute("terms_party_oid", termsPartyOid);
           termsPartyAssigneeBank.getDataFromAppServer();
        }

        DocumentHandler termsPartyDoc;

        termsPartyDoc = doc.getFragment("/In/Terms/FirstTermsParty");
        termsPartyAssignee.loadTermsPartyFromDocTagsOnly(termsPartyDoc);

        termsPartyDoc = doc.getFragment("/In/Terms/SecondTermsParty");
        termsPartyAssigneeBank.loadTermsPartyFromDocTagsOnly(termsPartyDoc);

        origTransOid = instrument.getAttribute("original_transaction_oid");
        if (!InstrumentServices.isBlank(origTransOid))
        {
           originalTransaction.getById(origTransOid);
        }
     }
     catch (Exception e)
     {
        out.println("Contact Administrator: "
              + "Unable to reload data after returning to page. "
              + "Error is " + e.toString());
     }
  }
  else
  {
     Debug.debug("populating beans from database");
     // We will perform a retrieval from the database.
     // Instrument and Transaction were already retrieved.  Get
     // the rest of the data

     terms = transaction.registerCustomerEnteredTerms();

     String termsPartyOid;

     termsPartyOid = terms.getAttribute("c_FirstTermsParty");
     if (termsPartyOid != null && !termsPartyOid.equals(""))
     {
        termsPartyAssignee.setAttribute("terms_party_oid", termsPartyOid);
        termsPartyAssignee.getDataFromAppServer();
     }
     termsPartyOid = terms.getAttribute("c_SecondTermsParty");
     if (termsPartyOid != null && !termsPartyOid.equals(""))
     {
        termsPartyAssigneeBank.setAttribute("terms_party_oid", termsPartyOid);
        termsPartyAssigneeBank.getDataFromAppServer();
     }

     origTransOid = instrument.getAttribute("original_transaction_oid");
     if (!InstrumentServices.isBlank(origTransOid))
     {
        originalTransaction.getById(origTransOid);
     }
  }

  /*************************************************
  * Load New Party
  **************************************************/
  if (doc.getDocumentNode("/In/NewPartyFromSearchInfo/Type") != null &&
      doc.getDocumentNode("/In/NewPartyFromSearchOutput/PartyOid") != null ) {

     // We have returned from the PartyDetailNew page.  Based on the returned 
     // data, RELOAD one of the terms party beans with the new party

     String termsPartyType = doc.getAttribute("/In/NewPartyFromSearchInfo/Type");
     String partyOid = doc.getAttribute("/In/NewPartyFromSearchOutput/PartyOid");

     Debug.debug("Returning from party search with " + termsPartyType + "/" + partyOid);

     // Use a Party web bean to get the party data for the selected oid.
     PartyWebBean party = beanMgr.createBean(PartyWebBean.class, "Party");
     party.setAttribute("party_oid", partyOid);
     party.getDataFromAppServer();

     DocumentHandler partyDoc = new DocumentHandler();
     party.populateXmlDoc(partyDoc);

     partyDoc = partyDoc.getFragment("/Party");

     // Based on the party type being returned (which we previously set)
     // reload one of the terms party web beans with the data from the
     // doc.
     if (termsPartyType.equals(TradePortalConstants.ASSIGNEE_01)) {
        termsPartyAssignee.loadTermsPartyFromDoc(partyDoc);
        // For a assignee selection, the assignee bank bank should be populated
        // if there is a designated bank.
        //cquinton 2/7/2013 pass designated party as part of partyDoc
        loadDesignatedParty(partyDoc, termsPartyAssigneeBank, beanMgr);
        focusField = "AssigneeName";
        focusSet = true;
     }
     if (termsPartyType.equals(TradePortalConstants.ASSIGNEE_BANK)) {
        termsPartyAssigneeBank.loadTermsPartyFromDoc(partyDoc);
        focusField = "AssigneeBankName";
        focusSet = true;
     }
  }

  // Retrieve and format all amount fields for the page
  currencyCode     = originalTransaction.getAttribute("copy_of_currency_code");
  //amount         = originalTransaction.getAttribute("copy_of_amount");
  amount           = instrument.getAttribute("copy_of_instrument_amount");
  originalAmount   = TPCurrencyUtility.getDisplayAmount(amount, currencyCode, loginLocale);
  amount           = terms.getAttribute("amount");
  assignmentAmount = TPCurrencyUtility.getDisplayAmount(amount, currencyCode, loginLocale);

  transactionType = transaction.getAttribute("transaction_type_code");
  transactionStatus = transaction.getAttribute("transaction_status");

  // Get the transaction rejection indicator to determine whether to show the rejection reason text
  rejectionIndicator = transaction.getAttribute("rejection_indicator");
  rejectionReasonText = transaction.getAttribute("rejection_reason_text");
  
  instrumentType = instrument.getAttribute("instrument_type_code");
  instrumentStatus = instrument.getAttribute("instrument_status");

//if newTransaction, set session variable so it persists
  String newTransaction = null;
  if (doc.getDocumentNode("/Out/newTransaction")!=null) {
    newTransaction = doc.getAttribute("/Out/newTransaction");
    session.setAttribute("newTransaction", newTransaction);
    System.out.println("found newTransaction = "+ newTransaction);
  } 
  else {
    newTransaction = (String) session.getAttribute("newTransaction");
    if ( newTransaction==null ) {
      newTransaction = TradePortalConstants.INDICATOR_NO;
    }
    System.out.println("used newTransaction from session = " + newTransaction);
  }
  
  //cquinton 1/18/2013 remove old close action behavior

  Debug.debug("Instrument Type " + instrumentType);
  Debug.debug("Instrument Status " + instrumentStatus);
  Debug.debug("Transaction Type " + transactionType);
  Debug.debug("Transaction Status " + transactionStatus);

  BigInteger requestedSecurityRight = null;

  requestedSecurityRight = SecurityAccess.EXPORT_LC_CREATE_MODIFY;
%>
  <%@ include file="fragments/Transaction-PageMode.frag" %>


<%
  // Create documents for the phrase dropdowns. First check the cache (no need
  // to recreate if they already exist).  After creating, place in the cache.
  // (InstrumentCloseNavigator.jsp cleans these up.)

  DocumentHandler phraseLists = formMgr.getFromDocCache(TradePortalConstants.PHRASE_LIST_DOC);
  if (phraseLists == null) phraseLists = new DocumentHandler();

  DocumentHandler addlCondDocList = phraseLists.getFragment("/AddlCond");
  if (addlCondDocList == null)
  {
     addlCondDocList = PhraseUtility.createPhraseList(
                                    TradePortalConstants.PHRASE_CAT_ADDL_COND, 
                                    userSession, formMgr, resMgr);
     phraseLists.addComponent("/AddlCond", addlCondDocList);
  }

  DocumentHandler spclInstrDocList = phraseLists.getFragment("/SpclInstr");
  if (spclInstrDocList == null)
  {
     spclInstrDocList = PhraseUtility.createPhraseList(
                                    TradePortalConstants.PHRASE_CAT_SPCL_INST, 
                                    userSession, formMgr, resMgr);
     phraseLists.addComponent("/SpclInstr", spclInstrDocList);
  }

  formMgr.storeInDocCache(TradePortalConstants.PHRASE_LIST_DOC, phraseLists);
  
  userSession.setCurrentPrimaryNavigation("NavigationBar.Transactions");
%>


<%-- ********************* HTML for page begins here ********************* --%>

<%
  if (isTemplate) {
%>
    <jsp:include page="/common/ButtonPrep.jsp" />
<%
  }
%>

<%
  // The navigation bar is only shown when editing templates.  For transactions
  // it is not shown ti minimize the chance of leaving the page without properly
  // unlocking the transaction.
  String showNavBar = TradePortalConstants.INDICATOR_NO;
  if (isTemplate)
  {
    showNavBar = TradePortalConstants.INDICATOR_YES;
  }
%>

 
<%
 
  // Auto save the form when time-out if not readonly.  
  // (Header.jsp will check for auto-save setting of the corporate org).
  String autoSaveFlag = isReadOnly ? TradePortalConstants.INDICATOR_NO : TradePortalConstants.INDICATOR_YES;
  String templateFlag = isTemplate ? TradePortalConstants.INDICATOR_YES : TradePortalConstants.INDICATOR_NO;

  //cquinton 11/19/2012 pass info for recent instruments into header
  String newRecentTransactionOid = transaction.getAttribute("transaction_oid");
  String newRecentInstrumentId = instrument.getAttribute("complete_instrument_id");
  String newRecentTransactionType = transaction.getAttribute("transaction_type_code");
  String newRecentTransactionStatus = transaction.getAttribute("transaction_status");
%>

<jsp:include page="/common/Header.jsp">
  <jsp:param name="autoSaveFlag"           value="<%=autoSaveFlag%>" />
  <jsp:param name="templateFlag"           value="<%=templateFlag%>" />
  <jsp:param name="newRecentTransactionOid" value="<%=newRecentTransactionOid%>" />
  <jsp:param name="newRecentInstrumentId"  value="<%=newRecentInstrumentId%>" />
  <jsp:param name="newRecentTransactionType" value="<%=newRecentTransactionType%>" />
  <jsp:param name="newRecentTransactionStatus" value="<%=newRecentTransactionStatus%>" />
</jsp:include>
 
<% //cr498 begin
   //Include ReAuthentication frag in case re-authentication is required for
   //authorization of transactions for this client
   String certAuthURL = "";
   Cache reCertCache = (Cache)TPCacheManager.getInstance().getCache(TradePortalConstants.CLIENT_BANK_CACHE);
   DocumentHandler CBCResult = (DocumentHandler)reCertCache.get(userSession.getClientBankOid());	
   String requireTranAuth = CBCResult.getAttribute("/ResultSetRecord(0)/REQUIRE_TRAN_AUTH");
   boolean requireAuth = InstrumentAuthentication.requireTransactionAuthentication(
       requireTranAuth,InstrumentAuthentication.TRAN_AUTH__EXP_DLC_ASN);
   if (requireAuth) {
%>   
      <%@ include file="/logon/fragments/openReauthenticationWindow.frag" %>
<%
   }
   //cr498 end
%>
<div class="pageMain">
<div class="pageContent">

<%

  String helpUrl = "customer/assign_exp_lc.htm";
%>
<jsp:include page="/common/PageHeader.jsp">
  <jsp:param name="titleKey" value="SecondaryNavigation.ExportLCAssignmentOfProceeds" />   
   <jsp:param name="helpUrl"  value="<%=helpUrl%>" />
</jsp:include>

<jsp:include page="/common/TransactionSubHeader.jsp" />


<form id="Transaction-EXP_DLC-Form" name="Transaction-EXP_DLC-Form" method="post" data-dojo-type="dijit.form.Form" action="<%=formMgr.getSubmitAction(response)%>">


  <input type=hidden value="" name="buttonName">
  
  <%-- error section goes above form content --%>
<div class="formArea">
<jsp:include page="/common/ErrorSection.jsp" />

<div class="formContent">

<% //cr498 begin
  if (requireAuth) {
%> 

  <input type=hidden name="reCertification" value="<%=TradePortalConstants.INDICATOR_NO%>">
  <input type=hidden name="reCertOK">
  <input type=hidden name="logonResponse">
  <input type=hidden name="logonCertificate">

<%
  } //cr498 end
%> 

<%
  // Store values such as the userid, security rights, and org in a
  // secure hashtable for the form.  Also store instrument and transaction
  // data that must be secured.
  Hashtable secureParms = new Hashtable();

  secureParms.put("login_oid",             userSession.getUserOid());
  secureParms.put("owner_org_oid",         userSession.getOwnerOrgOid());
  secureParms.put("login_rights",          loginRights);

  secureParms.put("instrument_oid",        instrument.getAttribute("instrument_oid"));
  secureParms.put("instrument_type_code",  instrumentType);
  secureParms.put("instrument_status",     instrumentStatus);

  secureParms.put("transaction_oid",       transaction.getAttribute("transaction_oid"));
  secureParms.put("transaction_type_code", transactionType);
  secureParms.put("transaction_status",    transactionStatus);

  secureParms.put("transaction_instrument_info", 
  	transaction.getAttribute("transaction_oid") + "/" + 
	instrument.getAttribute("instrument_oid") + "/" + 
	transactionType);

  // If the terms record doesn't exist, set its oid to 0.
  String termsOid = terms.getAttribute("terms_oid");
  if (termsOid == null)
  {
     termsOid = "0";
  }

  secureParms.put("terms_oid", termsOid);
  secureParms.put("TransactionCurrency", currencyCode);

  if (isTemplate)
  {
    secureParms.put("template_oid", template.getAttribute("template_oid"));
    secureParms.put("opt_lock", template.getAttribute("opt_lock"));
  }

  if (isTemplate) 
  {
%>
    <%@ include file="fragments/TemplateHeader.frag" %>
<%} %>


  
  
<%
  if (rejectionIndicator.equals(TradePortalConstants.INDICATOR_YES)
  	|| rejectionIndicator.equals(TradePortalConstants.INDICATOR_X))
  {
%>
<%=widgetFactory.createSectionHeader("0", "Rejection Reason") %>
     <%@ include file="fragments/Transaction-RejectionReason.frag" %>
     </div>
<%
  }
%>

  <%@ include file="fragments/Transaction-Documents.frag" %>
  <% //CR 821 Added Repair Reason Section 
  StringBuffer repairReasonWhereClause = new StringBuffer();
  int  repairReasonCount = 0;
	
	/*Get all repair reason's count from transaction history table*/
	repairReasonWhereClause.append("p_transaction_oid = ? ");
	repairReasonWhereClause.append(" and REPAIR_REASON is not null order by ACTION_DATETIME");
	//jgadela  R90 IR T36000026319 - SQL FIX
	Object sqlParamsRepCnt[] = new Object[1];
	sqlParamsRepCnt[0] =  transaction.getAttribute("transaction_oid") ;
	Debug.debug("repairReasonWhereCondition query : " + repairReasonWhereClause);
	repairReasonCount = DatabaseQueryBean.getCount("A_USER_OID", "transaction_history", repairReasonWhereClause.toString(), true, sqlParamsRepCnt);
			
if ( !(TradePortalConstants.TRANS_STATUS_PROCESSED_BY_BANK.equalsIgnoreCase(transactionStatus)) && repairReasonCount>0  ){%>
	<%=widgetFactory.createSectionHeader("", "TransactionHistory.RepairReason") %>
		<%@ include file="fragments/Transaction-RepairReason.frag" %>
	</div>
<%} %> 

	
  <%= widgetFactory.createSectionHeader("1", "Assignment.Terms") %>
    <%@ include file="fragments/Transaction-EXP_DLC-ASN-General.frag" %>
  </div>

  <%= widgetFactory.createSectionHeader("2", "Assignment.OtherConditionsOfTransfer") %>
    <%@ include file="fragments/Transaction-EXP_DLC-ASN-OtherConditions.frag" %>
  </div>
  
  <%= widgetFactory.createSectionHeader("3", "Assignment.InstructionsToBank") %>
    <%@ include file="fragments/Transaction-EXP_DLC-ASN-BankInstructions.frag" %>
 </div>
 
 <% if (TradePortalConstants.TRANS_STATUS_PROCESSED_BY_BANK.equalsIgnoreCase(transactionStatus) && repairReasonCount>0  ){%>
		<%=widgetFactory.createSectionHeader("4", "TransactionHistory.RepairReason", null, true) %>
		<%@ include file="fragments/Transaction-RepairReason.frag" %>
		</div>
<%} %> 

</div><%--formContent--%>
</div><%--formArea--%>  


<div class="formSidebar" data-dojo-type="widget.FormSidebar" data-dojo-props="form: 'Transaction-EXP_DLC-Form'">
	<jsp:include page="/common/Sidebar.jsp">
		<jsp:param value="<%=links %>" name="links"/>
        <jsp:param name="isReadOnly" value="<%=isReadOnly%>" />
		<jsp:param name="certAuthURL" value="<%=certAuthURL%>" />
		<jsp:param name="buttonPressed" value="<%=bButtonPressed%>" />
        <jsp:param name="error" value="<%=error%>" />            	
		<jsp:param name="formName" value="0" />
		<jsp:param name="showLinks" value="true" />
		<jsp:param name="isNewTransaction" value="<%=newTransaction%>"/>
    </jsp:include>
</div> <%--closes sidebar area--%>

 
  <%@ include file="fragments/PhraseLookupPrep.frag" %>
  <%@ include file="fragments/PartySearchPrep.frag" %>
<%= formMgr.getFormInstanceAsInputField("Transaction-EXP_DLC-Form", secureParms) %>
 <div id="PartySearchDialog"></div> 
</form>
</div>
</div>

<jsp:include page="/common/Footer.jsp">
   <jsp:param name="autoSaveFlag"           value="<%=autoSaveFlag%>" />
   <jsp:param name="templateFlag"           value="<%=templateFlag%>" />
   <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>
<jsp:include page="/common/SidebarFooter.jsp"/>

<script>

  <%-- cquinton 3/3/2013 add local var --%>
  var local = {};

<%
  if (!isReadOnly && InstrumentServices.isNotBlank(focusField)) {
%>
  require(["dijit/registry", "dojo/ready"],
    function(registry, ready ) {
      ready(function() {
        var focusFieldId = '<%= focusField %>';
        if ( focusFieldId ) {
          var focusField = registry.byId(focusFieldId);
          focusField.focus();
        }
      });
  });
<%
  }
%>
</script>

</body>
</html>

<%
   // Finally, reset the cached document to eliminate carryover of
   // information to the next visit of this page.
   doc.removeAllChildren("/");

   formMgr.storeInDocCache("default.doc", doc);
%>

<%!
   // Various methods are declared here (in alphabetical order).



   public String getRequiredIndicator(boolean showIndicator)
   {
      if (showIndicator)
      {
         return "<span class=Asterix>*</span>";
      }
      else
      {
         return "";
      }
   }
%> 

<%@include file="fragments/DesignatedPartyLookup.frag" %>
<script type="text/javascript">

  var itemid;
  var section;

  function SearchParty(identifierStr, sectionName,partyType){
	
    itemid = String(identifierStr);
    section = String(sectionName);
    partyType = String(partyType);

    require(["dojo/dom", "t360/dialog"], function(dom, dialog ) {

      <%-- cquinton 2/7/2013 --%>
      <%-- set the SearchPartyType input so it is included on new party form submit --%>
      var searchPartyTypeInput = dom.byId("SearchPartyType");
      if ( searchPartyTypeInput ) {
        searchPartyTypeInput.value=partyType;
      }

      dialog.open('PartySearchDialog', '<%=PartySearchAddressTitle%>',
        'PartySearch.jsp',
        ['returnAction','filterText','partyType','unicodeIndicator','itemid','section'],
        ['selectTransactionParty','',partyType,'<%=TradePortalConstants.INDICATOR_NO%>',itemid,section]); <%-- parameters --%>
    });
  }

  <%-- cquinton 3/2/2013 local phrase lookup --%>
  require(["t360/phraseLookup"],
          function( phraseLookup ) {
    local.phraseLookupAppend = function(phraseSelect, textArea, textAreaMaxLength ) {
      phraseLookup.appendPhrase( phraseSelect, textArea, textAreaMaxLength, "_errorSection" );
    };
  });

</script>
