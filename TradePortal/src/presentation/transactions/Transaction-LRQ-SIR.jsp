<%--
*******************************************************************************
                        Loan Request Page

  Description:    
    This is the main driver for the Loan Request page.  It handles 
  data retrieval of terms and terms parties (or retrieval from the input document)
  and creates the html page to display all the data for a Loan Request.
*******************************************************************************
--%>
      
<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,java.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.html.*,java.math.*,
                 com.amsinc.ecsg.util.DateTimeUtility, java.util.HashMap, java.util.Map" %>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager"
    scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" 
   scope="session">
</jsp:useBean>
   
<%-- ************** Data retrieval page setup begins here ****************  --%>



<%
  WidgetFactory widgetFactory = new WidgetFactory(resMgr, userSession);
  ReferenceDataManager refData = ReferenceDataManager.getRefDataMgr();
		  
  // Various oid and status info from transaction and instruments used in
  // several places.
 String instrumentOid = "";         
  String transactionOid;
  String instrumentType;
  String instrumentStatus;
  String transactionType; 
  String transactionStatus;
  String rejectionIndicator            = "";
  String rejectionReasonText	          = "";
  TermsPartyWebBean    counterParty  = null;
  String    counterPartyOid   = null;
  String    counterPartyName  = null;
  String    presentationDate  = null;
  String    maturityDate = null;
  String newTransaction = null;
  String links = ""; 
  String buttonPressed;
  StringBuilder presentationAmount = new StringBuilder();
  boolean 		  isProcessedByBank		  = false;
  boolean canDisplayPDFLinks = false;

  boolean getDataFromDoc;          // Indicates if data is retrieved from the
                                   // input doc cache or from the database

  DocumentHandler doc;
  String loginLocale = userSession.getUserLocale();
  String loginRights = userSession.getSecurityRights();

  // These are the beans used on the page.
  TransactionWebBean transaction  = (TransactionWebBean) beanMgr.getBean("Transaction");
  InstrumentWebBean instrument    = (InstrumentWebBean) beanMgr.getBean("Instrument");
  TemplateWebBean template        = null;
  TermsWebBean terms              = null;
  
  TermsPartyWebBean termsPartyApp = beanMgr.createBean(TermsPartyWebBean.class, "TermsParty");

  // Get the document from the cache.  We'll may use it to repopulate the 
  // screen if returning from another page, a save, validation, or any other
  // mediator called from this page.  Otherwise, we assume an instrument oid
  // and transaction oid was passed in.

  doc = formMgr.getFromDocCache();
  buttonPressed = doc.getAttribute("/In/Update/ButtonPressed");
  TermsWebBean originalTerms              = null;
  Vector error = null;
  error = doc.getFragments("/Error/errorlist/error");
  String buttonClicked = request.getParameter("buttonName");
  
  if (doc.getDocumentNode("/Out/newTransaction")!=null) {
	    newTransaction = doc.getAttribute("/Out/newTransaction");
	    session.setAttribute("newTransaction", newTransaction);
   } else {
	    newTransaction = (String) session.getAttribute("newTransaction");
	    if ( newTransaction==null ) {
	      newTransaction = TradePortalConstants.INDICATOR_NO;
	    }
}

/******************************************************************************
  We are either entering the page or returning from somewhere.  These are the 
  conditions for how to populate the web beans.  Data comes from either the
  database or the doc cache (/In section) with some variation.

  Mode           Condition                      Populate Beans From
  -------------  ----------------------------   --------------------------------
  Enter Page     no /In/Transaction in doc      Instrument and Template web
                                                beans already populated, get
                                                data for Terms and TermsParty
                                                web beans from database

  return from    /Out/PhraseLookupInfo exists   doc cache (/In); but use Phrase
  Phrase Lookup                                 LookupInfo text (from /Out) to
                                                replace a specific phrase text
                                                in the /In document before 
                                                populating

  return from    /Error/maxerrorseverity < 1    Same as Enter Page (data is
  Transaction                                   retrieved from database)
    mediator
    (no error)

  return from    /Error/maxerrorseverity > 0    doc cache (/In)
  Transaction    
    mediator
    (error)
******************************************************************************/

  // Assume we get the data from the doc.
  getDataFromDoc = true;

  String maxError = doc.getAttribute("/Error/maxerrorseverity");
  if (maxError != null && maxError.compareTo(
              String.valueOf(ErrorManager.WARNING_ERROR_SEVERITY)) < 0) {
     // No errors, so don't get the data from doc.
     getDataFromDoc = false;
  }
  // check to see if transaction needs to be refreshed
  // if so, refresh it and do not get data from doc as it is wrong
  if ( "Y".equals(doc.getAttribute("/Out/Refresh/Transaction")) ) {
     transaction.getDataFromAppServer();
     getDataFromDoc = false;
  }

  if (doc.getDocumentNode("/In/Transaction") == null) {
     // No /In/Transaction means we've never looked up the data.
     Debug.debug("No /In/Transaction section - get data from database");
     getDataFromDoc = false;
  }

  if (getDataFromDoc) {
     Debug.debug("Populating beans from doc cache");

     // Populate the beans from the input doc.
     try {
        instrument.populateFromXmlDoc(doc.getComponent("/In"));
        transaction.populateFromXmlDoc(doc.getComponent("/In"));

        terms = (TermsWebBean) beanMgr.getBean("Terms");
        terms.populateFromXmlDoc(doc, "/In");
        
        DocumentHandler termsPartyDoc = doc.getFragment("/In/Terms/SecondTermsParty");
        termsPartyApp.loadTermsPartyFromDocTagsOnly(termsPartyDoc);
        
        originalTerms = transaction.registerCustomerEnteredTerms();

     } catch (Exception e) {
        out.println("Contact Administrator: "
              + "Unable to reload data after returning to page. "
              + "Error is " + e.toString());
     }
  } else {
     Debug.debug("populating beans from database");
     // We will perform a retrieval from the database.
     // Instrument and Transaction were already retrieved.  Get
     // the rest of the data

     terms = transaction.registerCustomerEnteredTerms();
     originalTerms = terms;
     String termsPartyOid = terms.getAttribute("c_SecondTermsParty");
     if (termsPartyOid != null && !termsPartyOid.equals("")) {
        termsPartyApp.setAttribute("terms_party_oid", termsPartyOid);
        termsPartyApp.getDataFromAppServer();
     }

  }
  
  // Get the amount and currency code for display from terms

  String currencyCode = originalTerms.getAttribute("amount_currency_code");
  String displayAmount = TPCurrencyUtility.getDisplayAmount(
		  originalTerms.getAttribute("amount"), currencyCode, 
						 loginLocale);
		  
  transactionType = transaction.getAttribute("transaction_type_code");
  transactionStatus = transaction.getAttribute("transaction_status");
  transactionOid = transaction.getAttribute("transaction_oid");

  // Get the transaction rejection indicator to determine whether to show the rejection reason text
  rejectionIndicator = transaction.getAttribute("rejection_indicator");
  rejectionReasonText = transaction.getAttribute("rejection_reason_text");

  instrumentType = instrument.getAttribute("instrument_type_code");
  instrumentStatus = instrument.getAttribute("instrument_status");

   // Retrieve the name of the counter party associated with the instrument
   counterPartyOid = instrument.getAttribute("a_counter_party_oid");
   if (!StringFunction.isBlank(counterPartyOid))
   {
      counterParty = beanMgr.createBean(TermsPartyWebBean.class, "TermsParty");
      counterParty.getById(counterPartyOid);
      counterPartyName = counterParty.getAttribute("name");
   }
   else
   {
      counterPartyName = "";
   }
   BigInteger requestedSecurityRight = SecurityAccess.SIT_CREATE_MODIFY;	 
 %>
   <%@ include file="fragments/Transaction-PageMode.frag" %>

<%-- ********************* HTML for page begins here ********************* --%>

 <%
    String onLoad = "";
	//The navigation bar is only shown when editing templates.  For transactions
	 // it is not shown ti minimize the chance of leaving the page without properly
	 // unlocking the transaction.
	String showNavBar = TradePortalConstants.INDICATOR_NO;
   String autoSaveFlag = isReadOnly ? TradePortalConstants.INDICATOR_NO : TradePortalConstants.INDICATOR_YES;
   String pageTitleKey = "SecondaryNavigation.Instruments";
   userSession.setCurrentPrimaryNavigation("NavigationBar.Transactions");
   String helpUrl = "customer/Settlement_Instruction_Response.htm";
   String item1Key = resMgr.getText("SecondaryNavigation.Instruments.SettlementInstructionResponse",TradePortalConstants.TEXT_BUNDLE);

   
   String newRecentTransactionOid = transaction.getAttribute("transaction_oid");
   String newRecentInstrumentId = instrument.getAttribute("complete_instrument_id");
   String newRecentTransactionType = transaction.getAttribute("transaction_type_code");
   String newRecentTransactionStatus = transaction.getAttribute("transaction_status");
   String corpOrgOid = instrument.getAttribute("corp_org_oid");
		 
 %>

   <jsp:include page="/common/Header.jsp">
     <jsp:param name="autoSaveFlag"           value="<%=autoSaveFlag%>" />
     <jsp:param name="newRecentTransactionOid" value="<%=newRecentTransactionOid%>" />
     <jsp:param name="newRecentInstrumentId"  value="<%=newRecentInstrumentId%>" />
     <jsp:param name="newRecentTransactionType" value="<%=newRecentTransactionType%>" />
     <jsp:param name="newRecentTransactionStatus" value="<%=newRecentTransactionStatus%>" />
   </jsp:include>


<div class="pageMain">
<div class="pageContent">

<% 
   //Include ReAuthentication frag in case re-authentication is required for
   //authorization of transactions for this client
   String certAuthURL = "";
   Cache reCertCache = (Cache)TPCacheManager.getInstance().getCache(TradePortalConstants.CLIENT_BANK_CACHE);
   DocumentHandler CBCResult = (DocumentHandler)reCertCache.get(userSession.getClientBankOid());	
   String requireTranAuth = CBCResult.getAttribute("/ResultSetRecord(0)/REQUIRE_TRAN_AUTH");
   boolean requireAuth = InstrumentAuthentication.requireTransactionAuthentication(
       requireTranAuth,InstrumentAuthentication.TRAN_AUTH__IMP_DLC_AMD);
   if (requireAuth) {
%>   
      <%@ include file="/logon/fragments/openReauthenticationWindow.frag" %>
<%
   }
   //cr498 end
    //MEer Rel 9.3.5  CR-1027  
    corpOrgOid = instrument.getAttribute("corp_org_oid");
    CorporateOrganizationWebBean corporateOrg = beanMgr.createBean(CorporateOrganizationWebBean.class, "CorporateOrganization");
    corporateOrg.getById(corpOrgOid);	
    String bankOrgGroupOid = corporateOrg.getAttribute("bank_org_group_oid");
    
    BankOrganizationGroupWebBean bankOrganizationGroup = beanMgr.createBean(BankOrganizationGroupWebBean.class, "BankOrganizationGroup");
     bankOrganizationGroup.getById(bankOrgGroupOid);
  
     String selectedPDFType =  bankOrganizationGroup.getAttribute("imp_dlc_pdf_type");
     
     if(TradePortalConstants.APPLICATION_PDF_BASIC_TYPE.equals(selectedPDFType)  || TradePortalConstants.APPLICATION_PDF_EXPANDED_TYPE.equals(selectedPDFType)){
  			canDisplayPDFLinks = true;
     }
%>

<jsp:include page="/common/PageHeader.jsp">
   <jsp:param name="titleKey" value="<%=pageTitleKey%>" />
   <jsp:param name="item1Key" value="<%=item1Key%>" />
   <jsp:param name="helpUrl"  value="<%=helpUrl%>" />
</jsp:include>

<jsp:include page="/common/TransactionSubHeader.jsp" />

<form id="TransactionLRQ" name="TransactionLRQ"
	method="post" data-dojo-type="dijit.form.Form" action="<%=formMgr.getSubmitAction(response)%>">
	<input type=hidden value="" name=buttonName>
	<div class="formArea">
    <jsp:include page="/common/ErrorSection.jsp" />

    <div class="formContent">
    
<% //cr498 begin
  if (requireAuth) {
%> 

  <input type=hidden name="reCertification" value="<%=TradePortalConstants.INDICATOR_NO%>">
  <input type=hidden name="reCertOK">
  <input type=hidden name="logonResponse">
  <input type=hidden name="logonCertificate">

<%
  } //cr498 end
%> 
    
<%
  if(transaction.getAttribute("transaction_status").equals(TradePortalConstants.TRANS_STATUS_PROCESSED_BY_BANK))
	 isProcessedByBank = true;
  // Store values such as the userid, security rights, and org in a
  // secure hashtable for the form.  Also store instrument and transaction
  // data that must be secured.
  Hashtable secureParms = new Hashtable();
  secureParms.put("login_oid", userSession.getUserOid());
  secureParms.put("owner_org_oid", userSession.getOwnerOrgOid());
  secureParms.put("login_rights", loginRights);

  secureParms.put("instrument_oid",        instrument.getAttribute("instrument_oid"));
  secureParms.put("instrument_type_code",  instrumentType);
  secureParms.put("instrument_status",     instrumentStatus);
  secureParms.put("corp_org_oid",  corpOrgOid);

  secureParms.put("transaction_oid",       transaction.getAttribute("transaction_oid"));
  secureParms.put("transaction_type_code", transactionType);
  secureParms.put("transaction_status",    transactionStatus);
  
  secureParms.put("transaction_instrument_info", 
  	transaction.getAttribute("transaction_oid") + "/" + 
	instrument.getAttribute("instrument_oid") + "/" + 
	transactionType);

  // If the terms record doesn't exist, set its oid to 0.
  String termsOid = terms.getAttribute("terms_oid");

  if (termsOid == null)
  {
     termsOid = "0";
  }
  secureParms.put("terms_oid", termsOid);
%>
    
  <%@ include file="fragments/Instruments-AttachDocuments.frag" %>

  <%	if (rejectionIndicator.equals(TradePortalConstants.INDICATOR_YES)
    || rejectionIndicator.equals(TradePortalConstants.INDICATOR_X))
  {
  %>
	<%=widgetFactory.createSectionHeader("0", "common.RejectionReason") %>
     <%@ include file="fragments/Transaction-RejectionReason.frag" %>
     </div>
  <%
  }
  %>

	<% //CR 821 Added Repair Reason Section
	  StringBuffer repairReasonWhereClause = new StringBuffer();
	  int  repairReasonCount = 0;

		/*Get all repair reason's count from transaction history table*/
		repairReasonWhereClause.append("p_transaction_oid = ?");
		repairReasonWhereClause.append(" and REPAIR_REASON is not null order by ACTION_DATETIME");
		Object sqlParams1[] = new Object[1];
		sqlParams1[0] = transaction.getAttribute("transaction_oid");
		repairReasonCount = DatabaseQueryBean.getCount("A_USER_OID", "transaction_history", repairReasonWhereClause.toString(), true, sqlParams1);

	if ( !(TradePortalConstants.TRANS_STATUS_PROCESSED_BY_BANK.equalsIgnoreCase(transactionStatus)) && repairReasonCount>0  ){%>
		<%=widgetFactory.createSectionHeader("", "TransactionHistory.RepairReason") %>
			<%@ include file="fragments/Transaction-RepairReason.frag" %>
		</div>
	<%} %>
    
    <%= widgetFactory.createSectionHeader( "1","SettlementInstruction.1SettlementInstruction") %>
		   <%@ include file="fragments/Transaction-LRQ-SIR-SettlementInstruction.frag" %>
	</div>
	<%= widgetFactory.createSectionHeader( "2","SettlementInstruction.2FXRateDetails") %>
		   <%@ include file="fragments/Transaction-SettlementInstruction-FX_Detail.frag" %>
     </div>
    
    <% if (TradePortalConstants.TRANS_STATUS_PROCESSED_BY_BANK.equalsIgnoreCase(transactionStatus) && repairReasonCount>0  ){ %>
		 <%=widgetFactory.createSectionHeader("3", "TransactionHistory.RepairReason", null, true) %>
			  <%@ include file="fragments/Transaction-RepairReason.frag" %>
		 </div>
	<% }%>
    
    </div> <%-- Form Content End Here --%>
   </div><%--formArea--%>
   

    
  <div class="formSidebar" data-dojo-type="widget.FormSidebar" data-dojo-props="form: 'TransactionLRQ'">
	<jsp:include page="/common/Sidebar.jsp">
		<jsp:param name="links" value="<%=links%>" />
		<jsp:param name="isReadOnly" value="<%=isReadOnly%>" />
		<jsp:param name="certAuthURL" value="<%=certAuthURL%>" />
		<jsp:param name="buttonPressed" value="<%=buttonPressed%>" />
		<jsp:param name="error" value="<%= error%>" />
		<jsp:param name="formName" value="0" />
		 <jsp:param name="showLinks" value="true" />  
		 <jsp:param name="isNewTransaction" value="<%=newTransaction%>"/>
		 <jsp:param name="showApplnForm" value="<%=canDisplayPDFLinks%>"/>
	</jsp:include>
</div>
  
 <%= formMgr.getFormInstanceAsInputField("Transaction-SettlementInstructionForm", secureParms) %>  
 </form>
 </div>
 </div>
  <%--Rpasupulati IR T36000044993 adding phraseLookUpPrep frag  --%>
 <%@ include file="fragments/PhraseLookupPrep.frag" %>
 <jsp:include page="/common/Footer.jsp">
   <jsp:param name="autoSaveFlag"           value="<%=autoSaveFlag%>" />
   <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>

<jsp:include page="/common/SidebarFooter.jsp"/>
<%--Nar CR 818 Rel9400 08/12/2015--%>
<script type="text/javascript" src="/portal/js/page/settlementDetail.js"></script>
</body>
</html>

<%
   // Finally, reset the cached document to eliminate carryover of
   // information to the next visit of this page.
   formMgr.storeInDocCache("default.doc", new DocumentHandler());
%>
<%--Rpasupulati IR T36000044993 Start--%>
<script type = "text/javascript">
var local = {};
require(["t360/phraseLookup"],
          function( phraseLookup ) {
    local.phraseLookupAppend = function(phraseSelect, textArea, textAreaMaxLength ) {
      phraseLookup.appendPhrase( phraseSelect, textArea, textAreaMaxLength, "_errorSection" );
    };
  });	
  
function confirmAuthorizeDelete() {
    var  confirmMessage = "<%=resMgr.getText("Response.AuthorizeDeletePopupMessage",TradePortalConstants.TEXT_BUNDLE) %>";
    if (!confirm(confirmMessage)) {
          formSubmitted = false;
          return false;
    } else {
          return true;
    }
}
</script>
<%--Rpasupulati IR T36000044993 End--%>
