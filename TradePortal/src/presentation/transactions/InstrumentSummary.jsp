<%--
*******************************************************************************
                            Instrument Summary Page

  Description:  The Instrument Summary page is used to provide summary
                information for a specified instrument. This information
                includes the instrument's current terms, transactions, and
                mail messages. For users with the correct security rights,
                this page also provides links to create a mail message,
                create a transaction, and update a transaction. Links to
                current term details, related instruments, and existing
                mail messages are provided in the page as well.

  Note:         The userSession pageFlow is used to determine what page to return to.

*******************************************************************************
--%>

<%--
*
*     Copyright   2001
*     American Management Systems, Incorporated
*     All rights reserved
--%>
<%@page import="java.util.Vector"%>
<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,com.ams.tradeportal.busobj.*,
                 com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.html.*,java.util.*,org.slf4j.Logger,
org.slf4j.LoggerFactory" %>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager"
    scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean"
   scope="session">
</jsp:useBean>



<%-- ************** Data retrieval page setup begins here ****************  --%>

<%

   WidgetFactory widgetFactory = new WidgetFactory(resMgr,userSession);
   TransactionWebBean   originalTransaction             = null;
   TransactionWebBean   activeTransaction               = null;
   TermsPartyWebBean    counterParty                    = null;
   InstrumentWebBean    relatedInstrument               = null;
   InstrumentWebBean    instrument                      = null;
   DocumentHandler      xmlDoc                          = null;
   StringBuffer         preConstructedSqlQuery          = new StringBuffer();
   StringBuffer         transactionListWhere            = new StringBuffer();
   StringBuffer         dynamicWhereClause              = null;
   StringBuffer         instrumentAmount                = new StringBuffer();
   StringBuffer         equivalentAmount                = new StringBuffer();
   StringBuffer         availableAmount                 = new StringBuffer();
   StringBuffer         instrumentLabel                 = new StringBuffer();
   StringBuffer         additionalParms                 = new StringBuffer();
   StringBuffer         downloadLink                    = null;
   Hashtable            secureParms                     = new Hashtable();
   boolean              displayDownloadAdviceTerms      = false;
   boolean              hasMailMessagesAccess           = true;
   boolean              hasRelatedInstrument            = false;
   boolean              displayCurrentTerms             = false;
   boolean              fromCashMgmt                              = false;          // NSX CR-574 09/21/10
   String               activeTransactionCurrencyCode   = null;
   String               originalTransactionCurrencyCode = null;
   String               baseCurrencyCode                = null;
   String               instrumentOwnerOrgOid           = null;
   String               relatedInstrumentOid            = null;
   String               relatedInstrumentId             = null;
   String               instrumentTypeCode              = null;
   String               userSecurityRights              = null;
   String               userSecurityType                = null;
   String               counterPartyName                = null;
   String               counterPartyOid                 = null;
   String               referenceNumber                 = null;
   //String               listViewToSort                  = null;
   String               clientBankOid                   = null;
   String               userTimeZone                    = null;
   String               userLocale                      = null;
   String               userOrgOid                      = null;
   String               bankBranch                      = null;
   StringBuffer         newLink                         = null;
   String               userOid                         = null;
   String                     bankReferenceNumber                       = null;
   StringBuffer               processedAmount                     = null;
   StringBuffer               rejectedAmount                            = null;
   String                     termsOid                                  = null;
   boolean                    isTPSOriginatedPayment              = false;

   String label = "";
   boolean maturityFlag = false;
   String date 	       = "";
   String fromPage		= null;
   String amount;
   String tolerancePos;
   String toleranceNeg;
   String currencyCode;
   String pmtTerms;
   String daysAfterType;
   String daysAfterNum;
   String fixedMatDate;
   String loginLocale = userSession.getUserLocale(); 
   TermsWebBean terms              	  = null;
   TermsWebBean termsForLoan        	  = null;

   //MEer Rel 8.3 T36000021992 
   String corpOrgOid = userSession.getOwnerOrgOid();
   boolean ignoreDefaultPanelValue = false;
   boolean isSummaryPage = true;
   boolean doNotDisplayAmountSections = false;

   //cquinton 1/18/2013 remove old close action logic

   // Get the user's security rights, security type, locale, organization, oid, and client bank oid
   userSecurityRights = userSession.getSecurityRights();
   userSecurityType   = userSession.getSecurityType();
   userTimeZone       = userSession.getTimeZone();
   userLocale         = userSession.getUserLocale();
   userOrgOid         = userSession.getOwnerOrgOid();
   userOid            = userSession.getUserOid();
   clientBankOid      = userSession.getClientBankOid();
 
   //Naveen IR-T36000011007(ANZ- 721) 02/02/2013- Added for Route button navigation
   session.setAttribute("fromPage", TradePortalConstants.INSTRUMENT_SUMMARY);
   //IR T36000026794 Rel9.0 07/24/2014 starts
   session.setAttribute("startHomePage", "InstrumentSummary");
   session.removeAttribute("fromTPHomePage");
   //IR T36000026794 Rel9.0 07/24/2014 End
   //cquinton 1/18/2013 set return page
   //this assumes encrypted instrument oid
   String encryptedInstrumentOid = null;
   String instrumentOid = null;
   String encryptedtranOid = null;
   String tranOid = null;
   if ( "true".equals( request.getParameter("returning") ) ) {
      SessionWebBean.PageFlowRef thisPage = userSession.pageBack(); //to keep it correct
      encryptedInstrumentOid = thisPage.getParameterValue("instrument_oid");
      instrumentOid = EncryptDecrypt.decryptStringUsingTripleDes(encryptedInstrumentOid, userSession.getSecretKey());
   }
   else {
      encryptedInstrumentOid = request.getParameter("instrument_oid");
      instrumentOid = EncryptDecrypt.decryptStringUsingTripleDes(encryptedInstrumentOid, userSession.getSecretKey());
      SessionWebBean.PageFlowParameter parm = userSession.new PageFlowParameter("instrument_oid", encryptedInstrumentOid);
      userSession.addPage("goToInstrumentSummary", parm);
   }
   //cquinton 2/11/2013 remove code for mailMessageOid

   SessionWebBean.PageFlowRef backPage = userSession.peekPageBack();
   String returnAction = "";
   String returnParms = "";
   if(backPage == null){	
	System.out.println(" NPE Handled...");	  
	  returnAction = userSession.getCurrentPage().getLinkAction();
	  returnParms  = userSession.getCurrentPage().getLinkParametersString();
   }
   else{
    returnAction = backPage.getLinkAction();
    returnParms = backPage.getLinkParametersString();
   }
	System.out.println(" returnAction..."+returnAction);
   //cquinton 2/11/2013 remove code for mailMessageOid
   returnParms+="&returning=true";
		   
   boolean isFromBankTransUpdatePage = false;
	if(null != request.getParameter("bank_transaction_update_oid")){
		isFromBankTransUpdatePage = true;
	}
	
	 DGridFactory dGridFactory = new DGridFactory(resMgr, userSession, formMgr, beanMgr, response);
	
   
   /*   IR T36000010939 Made changes since instrument_oid is null in few cases
        @ Komal M Start 31/03/2013
   */
   /////////////////////////////////////////////////////////////////////////////////////////
   // WARNING!!!! cquinton 3/27/2013
   // below logic to retrieve instrument oid from the request and save/retrieve from session does not
   // seem correct due to above logic to get instrument oid from the page flow when returning from somewhere.
   // in addition note that we now use the encrypted oid derived above to populate the lists on the page 
   // via javascript - if instrumentoid is null wouldn't encrypted oid also be null?
   // believe solution is to ensure any access to this page either includes instrument oid as a request parameter
   // or if a returning action, uses the page flow functionality to correctly return to this page.
   // am not modifying now to limit risk.
   // hopefully this note will help maintenance on this page in the future!
   ////////////////////////////////////////////////////////////////////////////////////////
   instrument = beanMgr.createBean(InstrumentWebBean.class, "Instrument");

	   if (request.getParameter("instrument_oid") != null)
	   {
	      instrumentOid = EncryptDecrypt.decryptStringUsingTripleDes(request.getParameter("instrument_oid"), userSession.getSecretKey());
	   }

	   if (InstrumentServices.isBlank(instrumentOid))
	   {
	      instrumentOid = (String) session.getAttribute(TradePortalConstants.INSTRUMENT_SUMMARY_OID);
	   }
	   else
	   {
	      session.setAttribute(TradePortalConstants.INSTRUMENT_SUMMARY_OID, instrumentOid);
	   }
   /*   IR T36000010939 Made changes since instrument_oid is null in few cases
       @ Komal M Ends 31/03/2013
  */

   // Get the listview to sort if the user clicked on a listview header link; we only want to sort the listview the
   // user clicked on, NOT both listviews on the page
   //listViewToSort = request.getParameter("listView");

   // Create an instrument web bean and retrieve data from the database for the instrument oid passed into the page.
   // If we aren't passed an oid (i.e., we are coming BACK to the Instrument Summary page from a page we accessed
   // earlier FROM the Instrument Summary page, such as Create a Mail Message, Show Term Details, etc.), get it from
   // the session; otherwise, store it in the session for later use
   instrument = beanMgr.createBean(InstrumentWebBean.class, "Instrument");
   instrument.getById(instrumentOid);
		   
		   System.out.println("instrumentOid: "+ instrumentOid);

   // Get the oid of the organization that owns the instrument
   instrumentOwnerOrgOid = instrument.getAttribute("corp_org_oid");
   //Rel 9.3.5 IR-43406
   CorporateOrganizationWebBean corpOrg = null;
   corpOrg = beanMgr.createBean(CorporateOrganizationWebBean.class,	"CorporateOrganization");
   corpOrg.getById(instrumentOwnerOrgOid);
   String bankOrgGroupOid = corpOrg.getAttribute("bank_org_group_oid");

   // Get the instrument type code and bank branch (i.e., operational bank org oid) so we can use them
   // later for the New Transaction form parameters
   instrumentTypeCode = instrument.getAttribute("instrument_type_code");
   bankBranch         = instrument.getAttribute("op_bank_org_oid");
   String confirmationInd = instrument.getAttribute("copy_of_confirmation_ind");
   
   boolean isFundsXfer = false;
   boolean isLoanRqst = false;
   boolean isRequestAdvise = false;
   boolean isARMInstrument = false; //Pramey - 10/22/2008 CR-434 ARM
   boolean isRFInstrument = false; //IR - RIUJ021980931 Vshah
   boolean isPPYInstrument = false;
   boolean isSFNInstrument = false;
   boolean isBILInstrument = false;
   if (instrumentTypeCode.equals(InstrumentType.DOMESTIC_PMT) ||
	          instrumentTypeCode.equals(InstrumentType.FUNDS_XFER) ||
	          instrumentTypeCode.equals(InstrumentType.XFER_BET_ACCTS)) {
	   fromCashMgmt = true;
	}

   if( instrumentTypeCode.equals(InstrumentType.DOCUMENTARY_BA )) {

		maturityFlag = true;
	  }

   // Certain fields display based on instrument types of funds transfer or loan request.
   if (instrumentTypeCode.equals(InstrumentType.FUNDS_XFER))
       isFundsXfer = true;
   if (instrumentTypeCode.equals(InstrumentType.LOAN_RQST))
       isLoanRqst = true;
   if (instrumentTypeCode.equals(InstrumentType.REQUEST_ADVISE))
       isRequestAdvise = true;

   //Pramey - 10/22/2008 CR-434 ARM Add Begin
     if (instrumentTypeCode.equals(InstrumentType.RECEIVABLES_PAYBLES_MANAGEMENT))
        isARMInstrument = true;
   //Pramey - 10/22/2008 CR-434 ARM Add End

   //IR - RIUJ021980931 Vshah
     if (instrumentTypeCode.equals(InstrumentType.RECEIVABLES_FINANCE_INSTRUMENT))
      isRFInstrument = true;
   //CR 913 start
    if (instrumentTypeCode.equals(TradePortalConstants.INV_LINKED_INSTR_PYB))
      isPPYInstrument = true;
    if (instrumentTypeCode.equals(TradePortalConstants.SELLER_FINANCE_INSTRUMENT_TYPE))
        isSFNInstrument = true;
    //CR 913 end 
    
    // 08/05/2015 Rel 94 CR 932 start
    if (instrumentTypeCode.equals(TradePortalConstants.BILLING))
      isBILInstrument = true;
    // 08/05/2015 Rel 94 CR 932 start
    
   // Build the instrument label to use at the top of the page
   instrumentLabel.append(instrument.getAttribute("complete_instrument_id"));
   instrumentLabel.append("&nbsp;&nbsp;");
   instrumentLabel.append(ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.INSTRUMENT_TYPE,
                                                                        instrumentTypeCode, userLocale));
   instrumentLabel.append(" (");
   instrumentLabel.append(ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.INSTRUMENT_STATUS,
                                                                        instrument.getAttribute("instrument_status"),
                                                                        userLocale));
   instrumentLabel.append(")");

   // Retrieve both original transaction and instrument base currency codes, and format the instrument amount,
   // available amount, and equivalent amount fields accordingly
   originalTransaction = beanMgr.createBean(TransactionWebBean.class, "Transaction");

   originalTransaction.getById(instrument.getAttribute("original_transaction_oid"));


   activeTransaction = beanMgr.createBean(TransactionWebBean.class, "Transaction");

   TermsPartyWebBean termsPartyApplicant   = beanMgr.createBean(TermsPartyWebBean.class, "TermsParty");
   TermsPartyWebBean termsPartyBeneficiary = beanMgr.createBean(TermsPartyWebBean.class, "TermsParty");
   // R94 - CR 932
   TermsPartyWebBean termsBillCustomer = beanMgr.createBean(TermsPartyWebBean.class, "TermsParty");
   
   String termsPartyOid;

   String bankReleasedTerms	= originalTransaction.getAttribute("c_BankReleasedTerms");
   boolean hasBankReleasedTerms  = InstrumentServices.isNotBlank( bankReleasedTerms );
   System.out.println("hasBankReleasedTerms---"+hasBankReleasedTerms);
   if( hasBankReleasedTerms )
   {
	 //Srinivasu_D fix of IR#T36000019063 Rel8.2 07/14/2013 - added below cond to display latest shipment date
	  if(!InstrumentServices.isBlank(instrument.getAttribute("active_transaction_oid")))
    {
       activeTransaction.getById(instrument.getAttribute("active_transaction_oid"));
    
		terms = activeTransaction.registerBankReleasedTerms();
		termsForLoan = terms; 

	}  //Srinivasu_D fix of IR#T36000019063 Rel8.2 07/14/2013 - end
	else {
 	terms = originalTransaction.registerBankReleasedTerms();
 	termsForLoan = terms; //PUUH062751147
	}
   }else{
		terms = (TermsWebBean) originalTransaction.registerCustomerEnteredTerms();   
   }
   
// Jaya Mamidala CR-49930 Start
                 
      encryptedtranOid = request.getParameter("notifTransactionOid");
      if( StringFunction.isNotBlank(encryptedtranOid) )
      {
	  tranOid  = EncryptDecrypt.decryptStringUsingTripleDes(encryptedtranOid, userSession.getSecretKey());
	  long ltransOid = Long.parseLong(tranOid.trim());
	    	
	 Transaction notifTransaction = (Transaction)EJBObjectFactory.createClientEJB(formMgr.getServerLocation(), 
   		      "Transaction", ltransOid, resMgr.getCSDB());

      		if(!TradePortalConstants.INDICATOR_NO.equals(notifTransaction.getAttribute("unread_flag")) ) 
      		{
				notifTransaction.setAttribute("unread_flag",TradePortalConstants.INDICATOR_NO);
				notifTransaction.save();                                     
		    }   
		 }
   
   
 // Jaya Mamidala CR-49930 End
   %>
   <%@ include file="fragments/LoadTermsDetailsParties.frag" %>
   <%

   if(!InstrumentServices.isBlank(instrument.getAttribute("active_transaction_oid")))
    {
       activeTransaction.getById(instrument.getAttribute("active_transaction_oid"));
    }

   activeTransactionCurrencyCode = activeTransaction.getAttribute("copy_of_currency_code");
   originalTransactionCurrencyCode = originalTransaction.getAttribute("copy_of_currency_code");
   baseCurrencyCode      = activeTransaction.getAttribute("base_currency_code");

   if((originalTransactionCurrencyCode != null)
      && (originalTransactionCurrencyCode.trim().length() > 0)
      && (instrument.getAttribute("copy_of_instrument_amount") != null)
      && (instrument.getAttribute("copy_of_instrument_amount").trim().length() > 0))
   {
      instrumentAmount.append(originalTransactionCurrencyCode);
      instrumentAmount.append(" ");
      instrumentAmount.append(TPCurrencyUtility.getDisplayAmount(instrument.getAttribute("copy_of_instrument_amount"),
                                                              originalTransactionCurrencyCode, userLocale));
   }

   if((activeTransactionCurrencyCode != null)
      && (activeTransactionCurrencyCode.trim().length() > 0)
      && (activeTransaction.getAttribute("available_amount") != null)
      && (activeTransaction.getAttribute("available_amount").trim().length() > 0))
   {
      availableAmount.append(activeTransactionCurrencyCode);
      availableAmount.append(" ");
      availableAmount.append(TPCurrencyUtility.getDisplayAmount(activeTransaction.getAttribute("available_amount"),
                                                             activeTransactionCurrencyCode, userLocale));
   }

   if((baseCurrencyCode != null)
      && (baseCurrencyCode.trim().length() > 0)
      && (activeTransaction.getAttribute("equivalent_amount") != null)
      && (activeTransaction.getAttribute("equivalent_amount").trim().length() > 0))
   {
      equivalentAmount.append(baseCurrencyCode);
      equivalentAmount.append(" ");
      equivalentAmount.append(TPCurrencyUtility.getDisplayAmount(activeTransaction.getAttribute("equivalent_amount"),
                                                              baseCurrencyCode, userLocale));
   }

   // Retrieve the name of the counter party associated with the instrument
   counterPartyOid = instrument.getAttribute("a_counter_party_oid");
   if (!InstrumentServices.isBlank(counterPartyOid))
   {
      counterParty = beanMgr.createBean(TermsPartyWebBean.class, "TermsParty");

      counterParty.getById(counterPartyOid);

      counterPartyName = counterParty.getAttribute("name");
   }
   else
   {
      counterPartyName = "";
   }

   // Retrieve the instrument's reference number
   referenceNumber = instrument.getAttribute("copy_of_ref_num");
   // Retrieve the Bank Reference Number
   bankReferenceNumber = instrument.getAttribute("opening_bank_ref_num");
   
   
   
   // Retrieve the instrument that is related to the instrument we are currently viewing if one exists
   relatedInstrumentOid = instrument.getAttribute("related_instrument_oid");
   if (!InstrumentServices.isBlank(relatedInstrumentOid))
   {
      relatedInstrument = beanMgr.createBean(InstrumentWebBean.class,  "Instrument");

      relatedInstrument.getById(relatedInstrumentOid);

      additionalParms.append("&instrument_oid=");
      additionalParms.append(EncryptDecrypt.encryptStringUsingTripleDes(relatedInstrumentOid, userSession.getSecretKey()));

      relatedInstrumentId = formMgr.getLinkAsHref("goToInstrumentSummary",
                                                  relatedInstrument.getAttribute("complete_instrument_id"),
                                                  additionalParms.toString(), response);

      hasRelatedInstrument = true;
   }

   // This is the dynamic where clause for the transaction list.  We select those
   // transactions that are for the displayed instrument as well as the original
   // transaction for related instruments.
   transactionListWhere.append(" AND a.transaction_oid in (select original_transaction_oid as transaction_oid ");
   transactionListWhere.append("from instrument where a_related_instrument_oid = ");
   transactionListWhere.append(instrumentOid);
   transactionListWhere.append(" union ");
   transactionListWhere.append("select transaction_oid from transaction ");
   transactionListWhere.append("where p_instrument_oid = ");
   transactionListWhere.append(instrumentOid);
   transactionListWhere.append(")");
   
   // Determine whether or not the the user has access to the Instrument Mail listview and Create
   // a Mail Message button.  If the instrument isn't owned by the user's organization and the
   // user doesn't have the security right for viewing child org mail messages, the Create a Mail
   // Message button and Instrument Mail listview won't appear on the page.
  if ((instrumentOwnerOrgOid.equals(userOrgOid)) ||
       (SecurityAccess.hasRights(userSecurityRights, SecurityAccess.VIEW_CHILD_ORG_MESSAGES)) ||(userOrgOid.equals(bankOrgGroupOid)) )
   {
      // Construct the dynamic part of the SQL query used for generating the Instrument Mail listview
      dynamicWhereClause = new StringBuffer();
      dynamicWhereClause.append("and a.a_related_instrument_oid = ");
      dynamicWhereClause.append(instrumentOid);
   }
   else
   {
      hasMailMessagesAccess = false;
   }



   // Determine whether or not the user will have access to view the instrument's current terms; the
   // user will only have access for Import Letters of Credit, Export Letters of Credit, Incoming
   // Standby Letters of Credit, and Incoming Guarantees.
   if ((!InstrumentServices.isBlank(instrument.getAttribute("active_transaction_oid"))) &&
       ((instrumentTypeCode.equals(InstrumentType.IMPORT_DLC)) ||
        (instrumentTypeCode.equals(InstrumentType.EXPORT_DLC)) ||
        (instrumentTypeCode.equals(InstrumentType.STANDBY_LC)) ||
        (instrumentTypeCode.equals(InstrumentType.GUARANTEE)) ||
        (instrumentTypeCode.equals(InstrumentType.INCOMING_SLC)) ||
        (instrumentTypeCode.equals(InstrumentType.INCOMING_GUA)) ||
        (instrumentTypeCode.equals(InstrumentType.FUNDS_XFER)) ||
      (instrumentTypeCode.equals(InstrumentType.LOAN_RQST)) ||
      (instrumentTypeCode.equals(InstrumentType.RECEIVABLES_FINANCE_INSTRUMENT))  || //IR - RIUJ021980931 VShah
        (instrumentTypeCode.equals(InstrumentType.REQUEST_ADVISE))  ||
        (instrumentTypeCode.equals(InstrumentType.APPROVAL_TO_PAY)) ||
        (instrumentTypeCode.equals(InstrumentType.XFER_BET_ACCTS)) || // Peter Ng PSUJ011957828 2/27/2009
        (instrumentTypeCode.equals(InstrumentType.DOMESTIC_PMT))) )//rkrishna CR 375-D ATP 08/22/2007

   {
      displayCurrentTerms = true;
   }

   // Determine whether or not the user will have access to download the instrument's advice terms;
   // the user will only have access for Export Letter of Credit - Advise transactions
   if ((instrumentTypeCode.equals(InstrumentType.EXPORT_DLC)) &&
       (originalTransaction.getAttribute("transaction_type_code").equals(TransactionType.ADVISE)) &&
       (!InstrumentServices.isBlank(originalTransaction.getAttribute("transaction_as_text"))))
   {
      displayDownloadAdviceTerms = true;
   }

   // Set all necessary secure parms for the Create a Transaction button; these will be sent
   // to the Create Transaction Mediator, which will determine the next page to go to.
   secureParms.put("mode",            TradePortalConstants.EXISTING_INSTR);
   secureParms.put("clientBankOid",   clientBankOid);
   secureParms.put("userOid",         userOid);
   secureParms.put("bankBranch",      bankBranch);
   secureParms.put("validationState", TradePortalConstants.VALIDATE_NOTHING);
   secureParms.put("instrumentType",  instrumentTypeCode);
   secureParms.put("ownerOrgOid",     userOrgOid);
   secureParms.put("securityRights",  userSecurityRights);
   secureParms.put("instrumentData",  instrumentOid + "/");
   secureParms.put("startPage",       "InstrumentSummary");

      //rkazi CR-596 11/18/2010 Add

   //End Retrive all domestic payments list
   if (instrumentTypeCode.equals(InstrumentType.DOMESTIC_PMT)){
       //terms = (TermsWebBean) originalTransaction.registerCustomerEnteredTerms();
    DocumentHandler   enteredDomPmtsDoc     = null;
   DocumentHandler   enteredDomPmtDoc      = null;
   QueryListView     queryListView         = null;
   StringBuffer      updateText            = null;
   StringBuffer      sqlQuery              = new StringBuffer();
   int               numberOfDomPmts       = 0;
   Vector            enteredDomPmtList     = null;
   boolean sqlPayeeError = false;
      try
   {
      queryListView = (QueryListView) EJBObjectFactory.createClientEJB(formMgr.getServerLocation(), "QueryListView");

      //userOrgOid = userSession.getOwnerOrgOid();

      sqlQuery.append("select sum(amount) as processed_amount from domestic_payment where (payment_status='COMPLETE' or payment_status='PROCESSED_BY_BANK') and p_transaction_oid=?");

      queryListView.setSQL(sqlQuery.toString(), new Object[]{instrument.getAttribute("original_transaction_oid")});
      queryListView.getRecords();

      numberOfDomPmts = queryListView.getRecordCount();

      enteredDomPmtsDoc = queryListView.getXmlResultSet();

      enteredDomPmtList = enteredDomPmtsDoc.getFragments("/ResultSetRecord");

   }
   catch (Exception e)
   {
      sqlPayeeError = true;
      e.printStackTrace();

   }

   finally
   {
      try
      {
         if (queryListView != null)
         {
            queryListView.remove();
         }
      }
      catch (Exception e)
      {
         System.out.println("Error removing QueryListView in -CreditDebitDetails.frag!");

      }
   }

      processedAmount = new StringBuffer();
      rejectedAmount = new StringBuffer();
      // terms.getById(termsOid);
        String processAmount = "";

       if (numberOfDomPmts > 0){
            enteredDomPmtDoc = (DocumentHandler) enteredDomPmtList.elementAt(0);
            processAmount = enteredDomPmtDoc.getAttribute("/PROCESSED_AMOUNT");
       }

        if((originalTransactionCurrencyCode != null)
                  && (originalTransactionCurrencyCode.trim().length() > 0)
                  && (processAmount != null)
                  && (processAmount.trim().length() > 0))
               {
                        processedAmount.append(originalTransactionCurrencyCode);
                        processedAmount.append(" ");
                        processedAmount.append(TPCurrencyUtility.getDisplayAmount(processAmount,
                              originalTransactionCurrencyCode, userLocale));
               }
        else{
                  processedAmount.append(originalTransactionCurrencyCode);
                  processedAmount.append(" ");
                  processedAmount.append(TPCurrencyUtility.getDisplayAmount("0.00",
                        originalTransactionCurrencyCode, userLocale));

        }
//        if((originalTransactionCurrencyCode != null)
//                && (originalTransactionCurrencyCode.trim().length() > 0)
//                && (terms.getAttribute("rejected_amount") != null)
//                && (terms.getAttribute("rejected_amount").trim().length() > 0))
//             {
//           rejectedAmount.append(originalTransactionCurrencyCode);
//           rejectedAmount.append(" ");
//           rejectedAmount.append(TPCurrencyUtility.getDisplayAmount(terms.getAttribute("rejected_amount"),
//                      originalTransactionCurrencyCode, userLocale));
//             }
   }
//rkazi CR-596 11/18/2010 End
//rkazi IR RAUL020976454 02/11/2011 START
     if (originalTransaction != null){
           if ( originalTransaction.registerCustomerEnteredTerms() == null )
           {
                 isTPSOriginatedPayment = true;
           }
     }
     else if (activeTransaction != null){
           if ( activeTransaction.registerCustomerEnteredTerms() == null )
           {
                 isTPSOriginatedPayment = true;
           }
     }
     //rkazi IR RAUL020976454 02/11/2011 END
     
     //CR-1026 MEer
     String isTDBankInstrument = ConfigSettingManager.getConfigSetting("PORTAL_APP", "PORTAL", "TDB_APP", "");
     if(userSession.isCustNotIntgTPS() && !"Y".equals(isTDBankInstrument)){
    	 if(InstrumentServices.isValidInstrumentTypeForBTMU(instrumentOid, instrumentTypeCode))
    			 doNotDisplayAmountSections = true;
     }
   	
%>

<%-- ********************* HTML for page begins here *********************  --%>

<jsp:include page="/common/Header.jsp">
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeErrorSectionFlag"  value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="transOid"  value="<%=instrumentOid%>" />
</jsp:include>

<script type="text/javascript" src="/portal/js/datagrid.js"></script>

<div class="pageMainNoSidebar">
<div class="pageContent">
<%
  String pageTitleKey="";
  String helpUrl = "";
  if(isARMInstrument || isRFInstrument){
	pageTitleKey=resMgr.getText("SecondaryNavigation.Receivables",TradePortalConstants.TEXT_BUNDLE)+" "+ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.INSTRUMENT_TYPE,instrumentTypeCode, userLocale)+": "+resMgr.getText("InstrumentSummary.CurrentTerms",TradePortalConstants.TEXT_BUNDLE);
	helpUrl="customer/receivable_instrument_detail.htm";
  }
  else if(fromCashMgmt){
	pageTitleKey=resMgr.getText("SecondaryNavigation.Payments",TradePortalConstants.TEXT_BUNDLE)+" "+ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.INSTRUMENT_TYPE,instrumentTypeCode, userLocale)+": "+resMgr.getText("InstrumentSummary.CurrentTerms",TradePortalConstants.TEXT_BUNDLE);
	helpUrl = "customer/instr_summary.htm";
  }else if(isPPYInstrument){
	pageTitleKey=resMgr.getText("SecondaryNavigation.Payables",TradePortalConstants.TEXT_BUNDLE)+" "+ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.INSTRUMENT_TYPE,instrumentTypeCode, userLocale)+": "+resMgr.getText("InstrumentSummary.CurrentTerms",TradePortalConstants.TEXT_BUNDLE);
	helpUrl = "customer/instr_summary.htm";  
  }	  
  else{
	pageTitleKey=resMgr.getText("SecondaryNavigation.Instruments",TradePortalConstants.TEXT_BUNDLE)+" "+ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.INSTRUMENT_TYPE,instrumentTypeCode, userLocale)+": "+resMgr.getText("InstrumentSummary.CurrentTerms",TradePortalConstants.TEXT_BUNDLE);
	helpUrl = "customer/current_terms.htm";
  }
%>
<%-- IR T36000021686 Rel 8.3  --%>
<%@ include file="fragments/LoadPanelLevelAliases.frag" %>


<jsp:include page="/common/PageHeader.jsp">
   <jsp:param name="titleKey" value="<%=pageTitleKey%>" />
   <jsp:param name="item1Key" value="" />
   <jsp:param name="helpUrl"  value="<%=helpUrl%>" />
</jsp:include>

<form name="InstrumentSummaryForm" method="POST"  onSubmit="return false;">

  <input type=hidden value="" name=buttonName>

  <%= formMgr.getFormInstanceAsInputField("InstrumentSummaryForm", secureParms) %>

  <%--cquinton 9/13/2012 rearrange layout to more conform with style guide--%>
<%
  // Store this parameter so it can be passed back into the page from somewhere else
  secureParms = new Hashtable();
  secureParms.put("instrument_oid", instrument.getAttribute("instrument_oid"));

  StringBuffer subTitle = new StringBuffer();
  subTitle.append(ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.INSTRUMENT_TYPE,instrumentTypeCode, userLocale));
  subTitle.append(" - ");
  subTitle.append(instrument.getAttribute("complete_instrument_id"));
  subTitle.append(" - (");
  subTitle.append(ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.INSTRUMENT_STATUS,instrument.getAttribute("instrument_status"),userLocale));
  subTitle.append(")");
  StringBuilder bankInstTitle = new StringBuilder();
 
  if(doNotDisplayAmountSections){	  
  	String bankInstrumentId = instrument.getAttribute("bank_instrument_id");
  	if(StringFunction.isNotBlank(bankInstrumentId )){
	  	bankInstTitle.append(resMgr.getText("TermsSummary.BankInstrumentID",TradePortalConstants.TEXT_BUNDLE));
	  	bankInstTitle.append(" "+bankInstrumentId);
  	}
  }

  
  // IR T36000017531 - Start - Determine the button(Amend/Assign) to be displayed based on the 1st transaction type.
  String buttonTranName = "";
  if(instrumentTypeCode.equals(InstrumentType.EXPORT_DLC)){
	  List<String> possibleTypes = InstrumentServices.getPossibleTransactionTypes(instrumentTypeCode,
			  instrument.getAttribute("original_transaction_oid"));
	  buttonTranName = possibleTypes.size() == 1?  possibleTypes.get(0):"";
  }
	//IR T36000017531 - End
%>

  <div class="subHeaderDivider"></div>
  <div class="pageSubHeader">
    <span class="pageSubHeaderItem">
      <%= StringFunction.xssCharsToHtml(subTitle.toString())%>
    </span>
    <span class="pageSubHeaderIndent" >
    <%if(StringFunction.isNotBlank(bankInstTitle.toString())){ %>
          <%= StringFunction.xssCharsToHtml(bankInstTitle.toString())%>
    <%} %>
    </span>
    <span class="pageSubHeader-right">  
    
<%
  if ( SecurityAccess.canCreateModSettlementMsgAndResponse(userSecurityRights) ) {
	  if ( SettlementInstrUtility.isCreateSettlmentAvailableForInstr(instrumentTypeCode) ) {
%>		  
	<button data-dojo-type="dijit.form.Button"  name="CreateSettlementInstruction" id="CreateSettlementInstruction" type="submit" class="pageSubHeaderButton">
        <%=resMgr.getText("common.createSettlementInstruction",TradePortalConstants.TEXT_BUNDLE)%>
        <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
          createSettlementInstructions();
        </script>
      </button>
		  
<%		  
	  }
	  if ( SettlementInstrUtility.isRolloverAvailableForInstr(instrumentTypeCode) ) {
%>		  
      <button data-dojo-type="dijit.form.Button"  name="RequestRollover" id="RequestRollover" type="submit" class="pageSubHeaderButton">
        <%=resMgr.getText("common.RequestRollover",TradePortalConstants.TEXT_BUNDLE)%>
        <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
          requestRollover();
        </script>
      </button>
<%
	  }
  }
%>
       
<%
  if (hasMailMessagesAccess && (SecurityAccess.hasRights(userSecurityRights, SecurityAccess.CREATE_MESSAGE))){
	  newLink = new StringBuffer();
      newLink.append(formMgr.getLinkAsUrl("goToMailMessageDetail", response));
%>
      <button data-dojo-type="dijit.form.Button" name="CreateNewMailMessageButton" id="CreateNewMailMessageButton" type="button" class="pageSubHeaderButton">
        <%=resMgr.getText("common.CreateNewMailMessageText",TradePortalConstants.TEXT_BUNDLE)%>
        <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
          //createMessage();
			openURL("<%=newLink.toString()%>");
        </script>
      </button>
	  <%=widgetFactory.createHoverHelp("CreateNewMailMessageButton","CreateNewMailMessage") %>
<%
  }

  if(instrumentTypeCode.equals(InstrumentType.IMPORT_DLC) || instrumentTypeCode.equals(InstrumentType.GUARANTEE) ||
     instrumentTypeCode.equals(InstrumentType.APPROVAL_TO_PAY) || instrumentTypeCode.equals(InstrumentType.EXPORT_COL) || TransactionType.AMEND_TRANSFER.equals(buttonTranName) ||
     instrumentTypeCode.equals(InstrumentType.NEW_EXPORT_COL) || instrumentTypeCode.equals(InstrumentType.STANDBY_LC) ||
     instrumentTypeCode.equals(InstrumentType.REQUEST_ADVISE)) {
    if ((SecurityAccess.canCreateModConvertInstrument(userSecurityRights, instrumentTypeCode, "") ||(SecurityAccess.canCreateModInstrument(userSecurityRights, instrumentTypeCode, "")))
    		&& (userOrgOid.equals(instrumentOwnerOrgOid)) && !isARMInstrument && !isRFInstrument && !isPPYInstrument && !instrumentTypeCode.equals(InstrumentType.FUNDS_XFER) && !instrumentTypeCode.equals(InstrumentType.XFER_BET_ACCTS) && !isBILInstrument){
    	if (TradePortalConstants.INDICATOR_YES.equals(instrument.getAttribute("converted_transaction_ind"))){
%>
      <button data-dojo-type="dijit.form.Button"  name="CreateAmendTransaction" id="CreateAmendTransaction" type="submit" class="pageSubHeaderButton">
        <%=resMgr.getText("common.amendButton",TradePortalConstants.TEXT_BUNDLE)%>
        <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
          createConversionAmendment('<%=instrumentOid%>');
        </script>
      </button>
      <%=widgetFactory.createHoverHelp("CreateAmendTransaction","AmendTransactionHoverText") %>
      <button data-dojo-type="dijit.form.Button"  name="CreatePaymentTransaction" id="CreatePaymentTransaction" type="submit" class="pageSubHeaderButton">
        <%=resMgr.getText("common.paymentButton",TradePortalConstants.TEXT_BUNDLE)%>
        <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
          createConversionPayment('<%=instrumentOid%>');
        </script>
      </button>
      <%=widgetFactory.createHoverHelp("CreatePaymentTransaction","PaymentTransactionHoverText") %>

<%}else{ %>
      <button data-dojo-type="dijit.form.Button"  name="CreateAmendTransaction" id="CreateAmendTransaction" type="submit" class="pageSubHeaderButton">
        <%=resMgr.getText("common.amendButton",TradePortalConstants.TEXT_BUNDLE)%>
        <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
          createAmendment();
        </script>
      </button>
      <%=widgetFactory.createHoverHelp("CreateAmendTransaction","AmendTransactionHoverText") %>
<%}
    }
  }

  if(instrumentTypeCode.equals(InstrumentType.NEW_EXPORT_COL) || instrumentTypeCode.equals(InstrumentType.EXPORT_COL)) {
    if ((SecurityAccess.canCreateModInstrument(userSecurityRights, instrumentTypeCode, "")) && (userOrgOid.equals(instrumentOwnerOrgOid)) && !isARMInstrument && !isRFInstrument && !isPPYInstrument && !instrumentTypeCode.equals(InstrumentType.DOMESTIC_PMT) && !instrumentTypeCode.equals(InstrumentType.FUNDS_XFER) && !instrumentTypeCode.equals(InstrumentType.XFER_BET_ACCTS)){
%>
      <button data-dojo-type="dijit.form.Button"  name="CreateTracerTransaction" id="CreateTracerTransaction" type="submit" class="pageSubHeaderButton">
        <%=resMgr.getText("common.traceButton",TradePortalConstants.TEXT_BUNDLE)%>
        <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
          createTracer();
        </script>
      </button>
      <%=widgetFactory.createHoverHelp("CreateTracerTransaction","TracerTransactionHoverText") %>
<%
    }
  }

  if(instrumentTypeCode.equals(InstrumentType.EXPORT_DLC) && TransactionType.ASSIGNMENT.equals(buttonTranName)) {
    if ((SecurityAccess.canCreateModInstrument(userSecurityRights, instrumentTypeCode, "")) && (userOrgOid.equals(instrumentOwnerOrgOid)) && !isARMInstrument && !isRFInstrument && !isPPYInstrument && !instrumentTypeCode.equals(InstrumentType.DOMESTIC_PMT) && !instrumentTypeCode.equals(InstrumentType.FUNDS_XFER) && !instrumentTypeCode.equals(InstrumentType.XFER_BET_ACCTS)){
%>
      <button data-dojo-type="dijit.form.Button"  name="CreateAssignmentTransaction" id="CreateAssignmentTransaction" type="submit" class="pageSubHeaderButton">
        <%=resMgr.getText("common.AssignButton",TradePortalConstants.TEXT_BUNDLE)%>
        <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
          createAssignment();
        </script>
      </button>
<%
    }
  }


  if (displayDownloadAdviceTerms && !isARMInstrument && !isRFInstrument && !isPPYInstrument){
    downloadLink = new StringBuffer();

      downloadLink.append(request.getContextPath());

         // W Zhu 10/25/12 Rel 8.1 KJUM101652104 T36000006922 remove /.jsp
      downloadLink.append("/common/TradePortalDownloadServlet?");
      downloadLink.append(TradePortalConstants.DOWNLOAD_REQUEST_TYPE);
      downloadLink.append("=");
      downloadLink.append(TradePortalConstants.DOWNLOAD_ADVICE_TERMS_DATA);
%>
      <button data-dojo-type="dijit.form.Button"  name="DownloadAdviceTermsButton" id="DownloadAdviceTermsButton" type="button" data-dojo-props="iconClass:'download'"  class="pageSubHeaderButton">
        <%=resMgr.getText("common.DownLoadAdviceTermsText",TradePortalConstants.TEXT_BUNDLE)%>
        <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
          openURL("<%=downloadLink.toString()%>");
        </script>
      </button>
<%
  }

%>
      <%--cquinton 10/31/2012 ir#7015 change return link to close button when no close button present.
          note this does NOT include an icon as in the sidebar--%>
         <%-- KMehta IR-T36000020050 Rel 9400 on 14-Aug-2015 Add - Begin  --%>
       <%  if(hasRelatedInstrument){
    	   returnAction = "goToInstrumentSummary";
    	   returnParms = relatedInstrument.getAttribute("complete_instrument_id");
    	   returnParms = additionalParms.toString();
       }
      %>
      <%-- KMehta IR-T36000020050 Rel 9400 on 14-Aug-2015 Add - End  --%>
      <button data-dojo-type="dijit.form.Button" name="CloseButton" id="CloseButton" type="button" class="pageSubHeaderButton">
        <%=resMgr.getText("common.CloseText",TradePortalConstants.TEXT_BUNDLE)%>
        <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
          openURL("<%=formMgr.getLinkAsUrl(returnAction, returnParms, response)%>");
        </script>
      </button>
      <%=widgetFactory.createHoverHelp("CloseButton","ReturnPreviousHoverText") %>
    </span>
    <div style="clear:both;"></div>
  </div>

  <jsp:include page="/common/ErrorSection.jsp" />

  <div  class="formContentNoSidebar">


 <%-- <table width="100%" border="0" cellspacing="0" cellpadding="0"> --%>
 <table class="currentTermsContent">
  <tr>
      <td colspan=9>&nbsp;</td>
    </tr>

    <tr>

<% int cnt=0; %>


<%
// R 94 CR 932
if(isBILInstrument) {
		String purposeType = toleranceNeg = terms.getAttribute("purpose_type");
		//MEer Rel 9.4 CR-932 Retrieved Billing Reason from BankRefdata
		 List<Object> sqlParams = new ArrayList();
		 String billingReason="";
		 if(StringFunction.isNotBlank(purposeType)){
			String	billingReasonDescrSql = "select descr as BILLING_DESCRIPTION from bankrefdata where table_type ='BIL_PURPOSE_TYPE' and code = ? and client_bank_id = ? ";
   		    sqlParams.add(purposeType);
       	    sqlParams.add(userSession.getLoginOrganizationId());
       		DocumentHandler result = DatabaseQueryBean.getXmlResultSet(billingReasonDescrSql,false,sqlParams);
  		 	if(result !=null && result.getFragments("/ResultSetRecord").size()>0)
            	 billingReason = result.getAttribute("/ResultSetRecord(0)/BILLING_DESCRIPTION");	
           			
		}
		%>
		
		<tr>
		<td>
		<%= widgetFactory.createTextField("BillingReason", "Billing.BillingReason", billingReason, "50", true, false,false, "", "", "")%>
		</td>
	</tr>
<%}%>


<%if(!isARMInstrument && !isPPYInstrument && !isBILInstrument) { %>
<td>
<%
      //amount = originalTransaction.getAttribute("instrument_amount");
	  amount = instrument.getAttribute("copy_of_instrument_amount");
	  currencyCode = originalTransaction.getAttribute("copy_of_currency_code");
	  amount = TPCurrencyUtility.getDisplayAmount(amount, currencyCode, loginLocale);
	  if(instrumentTypeCode.equals(InstrumentType.DOMESTIC_PMT)) {
		  label = "InstrumentSummary.TransactionAmount";
	  }else{
		  label = "TransactionTerms.InstAmount";
	  }
	  if (!InstrumentServices.isBlank(amount) ) { %>
      		<%= widgetFactory.createTextField("InstAmount", label, currencyCode + "   " + amount, "25", true, false,false, "", "", "inline")%>
      <% } else { %>
      		<%= widgetFactory.createTextField("InstAmount", label, "", "25", true, false,false, "", "", "inline")%>
      <% } %>

      <%cnt = cnt+1; %>
</td>
<%if(cnt%4 == 0){ %>
</tr> <tr>
<%} %>
<%

   if (instrumentTypeCode.equals(InstrumentType.DOMESTIC_PMT) ||
       instrumentTypeCode.equals(InstrumentType.FUNDS_XFER) ||
       instrumentTypeCode.equals(InstrumentType.XFER_BET_ACCTS)) { %>
      <td>
            <%= widgetFactory.createTextField("RefNumber", "InstrumentSummary.YourReferenceNumber", referenceNumber, "25", true, false,false, "", "", "")%>
      <%cnt=cnt+1; %>
      </td>
   <%} else{
	   //RPasupulati IR T36000046145 not displying AvailableAmount when it is null
	   if (!InstrumentServices.isBlank(availableAmount.toString()) ) { %>
       <td>
      <%
      if(!doNotDisplayAmountSections){
      	 %>
				<%= widgetFactory.createTextField("AvailableAmount", "TransactionTerms.AvailableAmount", availableAmount.toString(), "25", true, false,false, "", "", "inline")%>
    	  <% } %>
      <%-- else { %>
      			<%= widgetFactory.createTextField("AvailableAmount", "TransactionTerms.AvailableAmount", "", "25", true, false,false, "", "", "inline")%>
     	 
      }%> --%>
      <%cnt=cnt+1; %>
      </td>
       <% }
	 } %>
<%if(cnt%4 == 0){ %>
</tr> <tr>
<%} %>
<%
   if (instrumentTypeCode.equals(InstrumentType.DOMESTIC_PMT) ||
       instrumentTypeCode.equals(InstrumentType.FUNDS_XFER) ||
       instrumentTypeCode.equals(InstrumentType.XFER_BET_ACCTS)) { %>
      <td>
        <%= widgetFactory.createTextField("OtherParty", "InstrumentSummary.OtherParty", counterPartyName, "25", true, false,false, "", "", "")%>
      <% cnt = cnt+1; %>
      </td>
   <%}else{  //RPasupulati IR T36000046145 not displying EquivalentAmount when it is null
	   if (!InstrumentServices.isBlank(amount) ) { %>
	      <td>
		 <%  if(!doNotDisplayAmountSections){%>
		 		
					<%= widgetFactory.createTextField("EquivalentAmount", "TransactionTerms.EquivalentAmount", equivalentAmount.toString(), "25", true, false,false, "", "", "inline")%>
	     	 	<%-- <% } else { %>
	      			<%= widgetFactory.createTextField("EquivalentAmount", "TransactionTerms.EquivalentAmount", "", "25", true, false,false, "", "", "inline")%>
	     		 <% } --%>
	     	<%  }%>
		 <% cnt = cnt+1; %>
	      </td>
	      <% }
	} %>
<%if(cnt%4 == 0){ %>
</tr> <tr>
<%} %>
		<%
          if (instrumentTypeCode.equals(InstrumentType.DOMESTIC_PMT) ||
	          instrumentTypeCode.equals(InstrumentType.FUNDS_XFER) ||
	          instrumentTypeCode.equals(InstrumentType.XFER_BET_ACCTS)) {
	       //   out.print("&nbsp;");
	      } else {%>
	      <td>
               <% if(!(isLoanRqst || isFundsXfer || isRFInstrument)) {%>
                	 <%= widgetFactory.createTextField("OtherParty", "InstrumentSummary.OtherParty", counterPartyName, "25", true, false,false, "", "", "")%>
               <%}else{ %>
                    <%= widgetFactory.createTextField("RefNumber", "InstrumentSummary.YourReferenceNumber", referenceNumber, "25", true, false,false, "", "", "")%>
               <%} %>
          <%cnt=cnt+1; %>
          </td>
          <%}%>
     <%if(cnt%4 == 0){ %>
</tr>
<tr>
    <td colspan=9>&nbsp;</td>
</tr>
<tr>
<%} %>
	<%
          if (instrumentTypeCode.equals(InstrumentType.DOMESTIC_PMT) ||
	          instrumentTypeCode.equals(InstrumentType.FUNDS_XFER) ||
	          instrumentTypeCode.equals(InstrumentType.XFER_BET_ACCTS)) {
	       //   out.print("&nbsp;");
	      } else {%>

               <% if(!(isLoanRqst || isFundsXfer || isRFInstrument)){%>
               <td>
                	 <%= widgetFactory.createTextField("RefNumber", "InstrumentSummary.YourReferenceNumber", referenceNumber, "25", true, false,false, "", "", "")%>
               <%cnt=cnt+1; %>
          		</td>
               <%} %>

          <%}%>
<%if(cnt%4 == 0){ %>
</tr> <tr>
<%} %>
           <%
           if( instrumentTypeCode.equals(InstrumentType.EXPORT_DLC) ||
                       instrumentTypeCode.equals(InstrumentType.INCOMING_SLC) ||
                       instrumentTypeCode.equals(InstrumentType.INCOMING_GUA) )
           {%>
          <td>
                 <%= widgetFactory.createTextField("OpeningBankReferenceNumber", "InstrumentSummary.OpeningBankReferenceNumber", bankReferenceNumber, "30", true, false, false, "", "", "inline")%>
           <%cnt=cnt+1; %>
           </td>
          <% }
           else if( instrumentTypeCode.equals(InstrumentType.IMPORT_COL) )
           {%>
           <td>
         	  <%= widgetFactory.createTextField("RemittingBankReferenceNumber", "InstrumentSummary.RemittingBankReferenceNumber", bankReferenceNumber, "30", true, false, false, "", "", "inline")%>
           <%cnt=cnt+1; %>
           </td>
           <%}
           else if( hasRelatedInstrument )
       {%>
       <td>
          <%= widgetFactory.createTextField("RelatedInstrument", "InstrumentSummary.RelatedInstrument", relatedInstrumentId, "30", true, false, false, "", "", "inline")%>
       <%cnt=cnt+1; %>
       </td>
       <%}
   %>

 	<%if(cnt%4 == 0){ %>
</tr> <tr>
<%} %>
   <%
           if( (hasRelatedInstrument) &&
                ( instrumentTypeCode.equals(InstrumentType.EXPORT_DLC) ||
                 instrumentTypeCode.equals(InstrumentType.INCOMING_SLC) ||
                 instrumentTypeCode.equals(InstrumentType.INCOMING_GUA) ||
                 instrumentTypeCode.equals(InstrumentType.IMPORT_COL)) )
       {%>
       <td>
          <%= widgetFactory.createTextField("RelatedInstrument", "InstrumentSummary.RelatedInstrument", relatedInstrumentId, "30", true, false, false, "", "", "inline")%>
        <%cnt=cnt+1; %>
        </td>
      <% }
           else
           {
                 //out.print("&nbsp;");
           }
   %>

   <%if(cnt%4 == 0){ %>
</tr> <tr>
<%} %>

   <% if (instrumentTypeCode.equals(InstrumentType.DOMESTIC_PMT) & !isTPSOriginatedPayment)
   		{
      		if (instrumentTypeCode.equals(InstrumentType.DOMESTIC_PMT)) { %>
      		<td>
      			<%if(null != processedAmount){ %>
          			<%= widgetFactory.createTextField("ProcessedTransactionAmount", "InstrumentSummary.ProcessedTransactionAmount", processedAmount.toString(), "30", true)%>
          		<%} else if(null == processedAmount){ %>
          			<%= widgetFactory.createTextField("ProcessedTransactionAmount", "InstrumentSummary.ProcessedTransactionAmount", "", "30", true)%>
          		<%} %>
          	<%cnt=cnt+1; %>
           </td>
            <% }
      	}
   } %>
    <%if(cnt%4 == 0){ %>
</tr> <tr>
<%} %>

<%
     // Don't show these rows if the instrument is a funds transfer
    if (!isFundsXfer)
    {
 %>
<%
if (displayCurrentTerms && !isARMInstrument && !isPPYInstrument && 
		  !instrumentTypeCode.equals(InstrumentType.DOMESTIC_PMT) &&
		  !instrumentTypeCode.equals(InstrumentType.FUNDS_XFER) &&
		  !instrumentTypeCode.equals(InstrumentType.XFER_BET_ACCTS)) {
        boolean displayAboutPercent;

	    try
         {
	      tolerancePos = terms.getAttribute("amt_tolerance_pos");
	      toleranceNeg = terms.getAttribute("amt_tolerance_neg");
	     }
            catch(Exception e)
             {
	       tolerancePos = "";
	       toleranceNeg = "";
	     }

	    if ((!InstrumentServices.isBlank(tolerancePos)) &&
	        (!InstrumentServices.isBlank(toleranceNeg)))
                 displayAboutPercent = true;
            else
                 displayAboutPercent = false;

              if(displayAboutPercent && !isFundsXfer && !instrumentTypeCode.equals(InstrumentType.DOMESTIC_PMT) && !instrumentTypeCode.equals(InstrumentType.XFER_BET_ACCTS)) {
            	  String percentValue = resMgr.getText("transaction.Plus",
                          TradePortalConstants.TEXT_BUNDLE)
				         + tolerancePos
				         + resMgr.getText("transaction.Percent",
				                          TradePortalConstants.TEXT_BUNDLE)
				         + " "
				         + resMgr.getText("transaction.Minus",
				                          TradePortalConstants.TEXT_BUNDLE)
				         + toleranceNeg
				         + resMgr.getText("transaction.Percent",
				                          TradePortalConstants.TEXT_BUNDLE);
				%>
				<td>
              <%= widgetFactory.createTextField("AboutPercent", "TransactionTerms.About", percentValue, "25", true, false,false, "", "", "inline")%>
              <%cnt=cnt+1; %>
              </td>
              <% } } %>

<%if(cnt%4 == 0){ %>
</tr> <tr>
<%} %>
<%
if(isBILInstrument){%>
          <td>
    	  	<%= widgetFactory.createTextField("RefNumber", "InstrumentSummary.YourReferenceNumber", referenceNumber, "25", true, false,false, "", "", "")%>
    	  	<%cnt=cnt+1; %>
    	  </td>
<%}%>

<% if (!isFundsXfer && !instrumentTypeCode.equals(InstrumentType.DOMESTIC_PMT)) { // only print top two rows if not a Funds Transfer %>
    <td>
        <%
          if (isLoanRqst || isRFInstrument) {
             label ="TransactionTerms.LoanStartDate";
		  }else if (isRequestAdvise) {
 			  label = "InstrumentSummary.AdvisedDate";
 		  } else if (isARMInstrument || isPPYInstrument) {
 			 label = "InstrumentSummary.CreateDate" ;
 		  } else if (isBILInstrument) {
  			 label = "Billing.StartDate" ;
  		  }else {
             label = "TransactionTerms.IssueDate";
          }
%>
          <%= widgetFactory.createDateField("Date1", label, instrument.getAttribute("issue_date"), true, false, false, "", "", "inline")%>

     <%cnt=cnt+1; %>
     </td>
    <%if(cnt%4 == 0){ %>
</tr>

<%
if ( !instrumentTypeCode.equals(InstrumentType.XFER_BET_ACCTS)) {
%>
<tr>
    <td colspan=9>&nbsp;</td>
</tr>
<%}%>
<tr>

<%} %>
<%
          String dateLabel = "";
		if( instrumentTypeCode.equals(InstrumentType.CLEAN_BA) ||
		    instrumentTypeCode.equals(InstrumentType.DEFERRED_PAY)||
		    instrumentTypeCode.equals(InstrumentType.DOCUMENTARY_BA) ||
		    instrumentTypeCode.equals(InstrumentType.REFINANCE_BA) ||
		    instrumentTypeCode.equals(InstrumentType.COLLECT_ACCEPT) ||
         isLoanRqst || isRFInstrument){
             dateLabel = resMgr.getText("TransactionTerms.MaturityDate",
                                        TradePortalConstants.TEXT_BUNDLE);
          }
		 else if( instrumentTypeCode.equals(InstrumentType.GUARANTEE) ){
             dateLabel = resMgr.getText("InstrumentSummary.ValidityDate",
                                        TradePortalConstants.TEXT_BUNDLE);
          }else if(  isARMInstrument || isPPYInstrument){%>
          <td>
    	  	<%= widgetFactory.createTextField("Currency", "transaction.Currency", originalTransactionCurrencyCode.toString(), "25", true, false,false, "", "", "inline")%>
    	  	<%cnt=cnt+1; %>
    	  </td>
     	<%}
          else if (isBILInstrument) {
             dateLabel = resMgr.getText("Billing.CloseDate",
                                        TradePortalConstants.TEXT_BUNDLE);
          }else if (!instrumentTypeCode.equals(InstrumentType.DOMESTIC_PMT) && !instrumentTypeCode.equals(InstrumentType.XFER_BET_ACCTS)) {
              dateLabel = resMgr.getText("TransactionTerms.ExpiryDate",
                                         TradePortalConstants.TEXT_BUNDLE);
           }
		  if (instrumentTypeCode.equals(InstrumentType.XFER_BET_ACCTS)) {
    	      date = "";
    	  }else if(isLoanRqst){
    		  date =  terms.getAttribute("loan_terms_fixed_maturity_dt");
    	  }else {
    		date = instrument.getAttribute("copy_of_expiry_date");
    	  }
%>
 <%if(cnt%4 == 0){ %>
</tr> <tr>
<%} %>
<%if(dateLabel !="") {%>
<td>
          <%= widgetFactory.createDateField("Date2", dateLabel, date, true, false, false, "", "", "inline")%>
</td>
<% cnt = cnt+1;
}%>
   <%if(cnt%4 == 0){ %>
</tr>

<%
if ( !instrumentTypeCode.equals(InstrumentType.XFER_BET_ACCTS)) {
%>
<tr>
    <td colspan=9>&nbsp;</td>
</tr>
<%}%>
<tr>
<%} %>

<%
if (displayCurrentTerms && !isARMInstrument && !isPPYInstrument &&
		  !instrumentTypeCode.equals(InstrumentType.DOMESTIC_PMT) &&
		  !instrumentTypeCode.equals(InstrumentType.FUNDS_XFER) &&
		  !instrumentTypeCode.equals(InstrumentType.XFER_BET_ACCTS)) {
						if (isLoanRqst || isRFInstrument || instrumentTypeCode.equals(InstrumentType.XFER_BET_ACCTS)) {

			             label = "";
			          }
			          else {
			             label = resMgr.getText("TransactionTerms.LatestShipDate",
			                                    TradePortalConstants.TEXT_BUNDLE);
			          }

		  int shipmentCount = -1;

			try{
			     if (isLoanRqst || isRFInstrument || instrumentTypeCode.equals(InstrumentType.XFER_BET_ACCTS)) {
			         date = "";
			     }
			     else {
			    	//IR 21814 Start-Get the active transaction terms only for latest shipment date				  
			    	 date =InstrumentServices.getLatestShipmentDate(instrument.getAttribute("instrument_oid"));
				     }
			   //IR 21814 end
			}catch(Exception e){
			   date = "";
			 }
		  if(label!=""){%>
		  <td>
          <%if (!InstrumentServices.isBlank(date) && (shipmentCount <= 1)) { %>
	          <%= widgetFactory.createDateField("Date3", label, date, true, false, false, "", "", "inline")%>
		  <% } else if (shipmentCount > 1) { %>
		       <%=resMgr.getText("TransactionTerms.MultipleShipments", TradePortalConstants.TEXT_BUNDLE)%>
		  <% } else if(InstrumentServices.isBlank(date)) {%>
				<%= widgetFactory.createDateField("Date3", label, "", true, false, false, "", "", "inline")%>
		  <% } %>
		  </td>
		  <%cnt = cnt+1;
		  }
	}	%>
		  <%if(cnt%4 == 0){ %>
</tr> <tr>
<%} %>
      <%
      if((instrumentTypeCode != null)
      	&& (confirmationInd != null)
      	&& (instrumentTypeCode.equals(InstrumentType.INCOMING_SLC)
      		|| instrumentTypeCode.equals(InstrumentType.EXPORT_DLC)
      		|| instrumentTypeCode.equals(InstrumentType.INCOMING_GUA)))
      { %>
      <td>
    	 <% if(instrument.getAttribute("copy_of_confirmation_ind").equals(TradePortalConstants.INDICATOR_YES))
        	{ %>
        		<%= widgetFactory.createTextField("ConfirmedText", "TransactionTerms.Confirmed", resMgr.getText("common.Yes", TradePortalConstants.TEXT_BUNDLE), "30", true, false,false, "", "", "inline")%>
        	<%}
        	else
        	{%>
    			<%= widgetFactory.createTextField("ConfirmedText", "TransactionTerms.Confirmed", resMgr.getText("common.No", TradePortalConstants.TEXT_BUNDLE), "30", true, false,false, "", "", "inline")%>
    		<%}

      	%>
      	<%cnt = cnt+1; %>
      	</td>

      	<%
      }%>

<%if(cnt%4 == 0){ %>
</tr> <tr>
<%} %>

<% } %>

<%if (displayCurrentTerms && !isARMInstrument && !isPPYInstrument &&
		  !instrumentTypeCode.equals(InstrumentType.DOMESTIC_PMT) &&
		  !instrumentTypeCode.equals(InstrumentType.FUNDS_XFER) &&
		  !instrumentTypeCode.equals(InstrumentType.XFER_BET_ACCTS)) {%>

	    <%try{
	    //Because the Payment Terms may not be registered, it was put into this try/catch block
  	    //to prevent an exception from creating problems.  Also, since the Payment Terms Type
 	    //has types in it that require multiple pieces of data, an if block was the only way to
	    //prep the data before sending out to display.

	    pmtTerms = terms.getAttribute("pmt_terms_type");

            if (isLoanRqst || isRFInstrument) {
	       if( pmtTerms.equals( TradePortalConstants.LOAN_DAYS_AFTER ) ) {
	   	  daysAfterNum  = terms.getAttribute("loan_terms_number_of_days");

		  //this will prevent an empty terms.pmt_terms_days_after_type from throwing
		  //an exception thus blanking out the built string.
		  try{
		    daysAfterType = ReferenceDataManager.getRefDataMgr().getDescr(
                                                      TradePortalConstants.LOAN_TERMS_TYPE,
						      pmtTerms);
		  }catch(Exception e){
		 	daysAfterType = "";
		  }
		  pmtTerms = daysAfterNum + "  "
                           + resMgr.getText("TransactionTerms.Days", TradePortalConstants.TEXT_BUNDLE)
                           + " " + daysAfterType;

	       }else if( pmtTerms.equals( TradePortalConstants.LOAN_FIXED_MATURITY ) ) {
		  pmtTerms = ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.LOAN_TERMS_TYPE,
						                  	 pmtTerms);
		  date = terms.getAttribute("loan_terms_fixed_maturity_dt");
		  date = TPDateTimeUtility.formatDate( date, TPDateTimeUtility.LONG, loginLocale );
		  pmtTerms = pmtTerms + "  " + date;

	       }else{
		  pmtTerms = "";
	       }
            } else {
	       if( (pmtTerms.equals( TradePortalConstants.PMT_SIGHT ))        ||
	           (pmtTerms.equals( TradePortalConstants.PMT_CASH_AGAINST )) ||
	           (pmtTerms.equals( TradePortalConstants.PMT_OTHER )) ) {

   	 	     pmtTerms = ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.PMT_TERMS_TYPE,
						                  	 pmtTerms);

	       }else if( pmtTerms.equals( TradePortalConstants.PMT_DAYS_AFTER ) ) {
	   	  daysAfterNum  = terms.getAttribute("pmt_terms_num_days_after");
	 	  daysAfterType = terms.getAttribute("pmt_terms_days_after_type");

		  //this will prevent an empty terms.pmt_terms_days_after_type from throwing
		  //an exception thus blanking out the built string.
		  try{
		    daysAfterType = ReferenceDataManager.getRefDataMgr().getDescr(
                                                      TradePortalConstants.PMT_TERMS_DAYS_AFTER,
						      daysAfterType);
		  }catch(Exception e){
		 	daysAfterType = "";
		  }
		  pmtTerms = resMgr.getText("TransactionTerms.DaysAfter", TradePortalConstants.TEXT_BUNDLE);
		  pmtTerms = daysAfterNum + "  " + pmtTerms + " " + daysAfterType;

	       }else if( pmtTerms.equals( TradePortalConstants.PMT_FIXED_MATURITY ) ) {
		  pmtTerms = ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.PMT_TERMS_TYPE,
						                  	 pmtTerms);
		  date = terms.getAttribute("pmt_terms_fixed_maturity_date");
		  date = TPDateTimeUtility.formatDate( date, TPDateTimeUtility.LONG, loginLocale );
		  pmtTerms = pmtTerms + "  " + date;

	       }else{
		  pmtTerms = "";
	       }
            }

	    }catch(Exception e){
	        pmtTerms = "";
	    }

            if (isLoanRqst || isRFInstrument) {%>
            	<%
            	String loanTermsType = null;
    			if (termsForLoan != null)
    			{
    			    loanTermsType = termsForLoan.getAttribute("loan_terms_type");
    			}
    			if (loanTermsType != null)
    			{
    			  if (loanTermsType.equals(TradePortalConstants.LOAN_DAYS_AFTER)) {
    			%>
    			<td>
                    <%= widgetFactory.createTextField("PmtTerms", "TransactionTerms.LoanTerms", termsForLoan.getAttribute("loan_terms_number_of_days")+" " + resMgr.getText("LoanRequest.DaysFrom", TradePortalConstants.TEXT_BUNDLE), "60", true, false,false, "", "", "inline")%>
                <%cnt=cnt+1; %>
                </td>
                <%if(cnt%4 == 0){ %>
					</tr> <tr>
					<%} %>
                  <%} else {
    				date = termsForLoan.getAttribute("loan_terms_fixed_maturity_dt"); //PUUH062751147
    				date = TPDateTimeUtility.formatDate( date, TPDateTimeUtility.LONG, loginLocale );
    				%>
    			<td>
                    <%=widgetFactory.createTextField("PmtTerms", "TransactionTerms.LoanTerms", resMgr.getText("LoanRequest.AtFixedMaturityDate",TradePortalConstants.TEXT_BUNDLE)+ " " +date, "60", true, false,false, "", "", "inline")%>
    			<%cnt=cnt+1; %>
                </td>
                <%if(cnt%4 == 0){ %>
</tr> <tr>
<%} %>
    			<%
    			  }

    			%>

            <%} else if (isFundsXfer || instrumentTypeCode.equals(InstrumentType.DOMESTIC_PMT) || instrumentTypeCode.equals(InstrumentType.XFER_BET_ACCTS)) {
            if(referenceNumber == null) referenceNumber = "";
			%>
			<% } else if(!pmtTerms.equals("")) { %>
			<td>
               <%= widgetFactory.createTextField("PmtTerms", "TransactionTerms.PaymentTerms", pmtTerms, "60", true, false,false, "", "", "inline")%>
            <%cnt=cnt+1; %>
            </td>
            <% } else{%>
            <td>
            	<%= widgetFactory.createTextField("PmtTerms", "TransactionTerms.PaymentTerms", "", "60", true, false,false, "", "", "inline")%>
            <%cnt=cnt+1; %>
            </td>
           <% }
            %>
            <%if(cnt%4 == 0){ %>
</tr> <tr>
<%} %>

<% } %>
 		<%
 		if (instrumentTypeCode.equals(InstrumentType.GUARANTEE) || instrumentTypeCode.equals(InstrumentType.STANDBY_LC) ||
 			instrumentTypeCode.equals(InstrumentType.INCOMING_SLC) || instrumentTypeCode.equals(InstrumentType.REQUEST_ADVISE)) {
 			if(!pmtTerms.equals("")) {%>
          	<td>
             <%= widgetFactory.createTextField("PmtTerms", "TransactionTerms.PaymentTerms", pmtTerms, "60", true, false,false, "", "", "inline")%>
            <%cnt=cnt+1; %>
            </td>
      	<% } } %>
           <% //if(cnt%4 == 0){  %>
			</tr>
			</table><table class="currentTermsContent" border='0'>
			<tr>
			<% //} %>

  			<%if(instrumentTypeCode.equals(InstrumentType.APPROVAL_TO_PAY)) { %>
  			<td>
<%-- cquinton 10/21/2013 Rel 8.3 ir#21819 start
     fixed an issue with flickering auto resize text area in IE8.  to resolve made
     a static width -- see new autoHeightTxtAreaQtrWidthNoSidebar class.
     also had to add style of overflow-x: hidden here - this would not take
     in the stylesheet - as the dojo TextArea takes over.  but it does work
     when explicitly set inline here.  note that this is just a precaution
     becuase of an apparent miscalculation in dojo for ie8 - when just a bit
     too long, we get a horizontal scrollbar, but a bit longer and the
     text wraps --%>
	 <%-- Srinivasu_D IR#T36000019515 Rel8.4 01/23/2014 Start Textarea altered --%>
             	<%= widgetFactory.createAutoResizeTextArea("Applicant", "TransactionTerms.Buyer", termsPartyApplicant.buildAddress(false, false, resMgr), true, false,false, "class='autoHeightTxtAreaQtrWidthNoSidebar' style='overflow-x: hidden;' rows='4' cols='45'", "", "")%>
            <%cnt=cnt+1; %>
            </td>
             <%} else if (instrumentTypeCode.equals(InstrumentType.FUNDS_XFER)) {%>
            <td>
	             <%= widgetFactory.createAutoResizeTextArea("Applicant", "TransactionTerms.Applicant", termsPartyApplicant.buildAddress(false, true, resMgr), true, false,false, "class='autoHeightTxtAreaQtrWidthNoSidebar' style='overflow-x: hidden;' rows='4' cols='45'", "", "")%>
	         <%cnt=cnt+1; %>
             </td>
	     	 <%} else if(instrumentTypeCode.equals(InstrumentType.LOAN_RQST)) {%>
	     	 <td>
	             <%= widgetFactory.createAutoResizeTextArea("Applicant", "TransactionTerms.Borrower", termsPartyApplicant.buildAddress(false, true, resMgr), true, false,false, "class='autoHeightTxtAreaQtrWidthNoSidebar' style='overflow-x: hidden;' rows='4' cols='45'", "", "")%>
	         <%cnt=cnt+1; %>
            </td>
             <%} else if(instrumentTypeCode.equals(InstrumentType.DOMESTIC_PMT)) { %>
            <td>
	             <%= widgetFactory.createAutoResizeTextArea("Applicant", "TransactionTerms.DebitParty", termsPartyApplicant.buildAddress(false, true, resMgr), true, false,false, "class='autoHeightTxtAreaQtrWidthNoSidebar' style='overflow-x: hidden;' rows='4' cols='45'", "", "")%>
	         <%cnt=cnt+1; %>
            </td>
             <%} else{ %>
             <td>
	             <%= widgetFactory.createAutoResizeTextArea("Applicant", "TransactionTerms.Applicant", termsPartyApplicant.buildAddress(false, true, resMgr), true, false,false, "class='autoHeightTxtAreaQtrWidthNoSidebar' style='overflow-x: hidden;' rows='4' cols='45'", "", "")%>
				 <%-- Srinivasu_D IR#T36000019515 Rel8.4 01/23/2014 End --%>
<%-- cquinton 10/21/2013 Rel 8.3 ir#21819 end --%>
	         <%cnt=cnt+1; %>
             </td>
             <%}%>
  			<%if(cnt%4 == 0){ %>
  			<%--  Srinivasu_D IR#T36000025989 Rel8.4 03/11/2014 - commented below <tr> for buyer/seller to be in the same line--%>
  		<%-- 	</tr> <tr>
				--%>
  			<%} %>



 		<%if (isLoanRqst || isRFInstrument) {%>

      	<% } else {
          	if(instrumentTypeCode.equals(InstrumentType.APPROVAL_TO_PAY)) { %>
          	<td>
<%-- cquinton 10/21/2013 Rel 8.3 ir#21819 start
     fixed an issue with flickering auto resize text area in IE8.  to resolve made
     a static width -- see new autoHeightTxtAreaQtrWidthNoSidebar class.
     also had to add style of overflow-x: hidden here - this would not take
     in the stylesheet - as the dojo TextArea takes over.  but it does work
     when explicitly set inline here.  note that this is just a precaution
     becuase of an apparent miscalculation in dojo for ie8 - when just a bit
     too long, we get a horizontal scrollbar, but a bit longer and the
     text wraps --%>
             <%= widgetFactory.createAutoResizeTextArea("Beneficiary", "TransactionTerms.Seller", termsPartyBeneficiary.buildAddress(false, true, resMgr), true, false,false, "class='autoHeightTxtAreaQtrWidthNoSidebar' style='overflow-x: hidden;' rows='4' cols='45'", "", "")%>
            <%cnt=cnt+1; %>
            </td>
           <%}else if(instrumentTypeCode.equals(InstrumentType.DOMESTIC_PMT)) {%>
           <td>
             <%= widgetFactory.createAutoResizeTextArea("Beneficiary", "TransactionTerms.Payee", termsPartyBeneficiary.buildAddress(false, true, resMgr), true, false,false, "class='autoHeightTxtAreaQtrWidthNoSidebar' style='overflow-x: hidden;' rows='4' cols='45'", "", "")%>
           <%cnt=cnt+1; %>
            </td>
           <%} else { %>
           <td>
             <%= widgetFactory.createAutoResizeTextArea("Beneficiary", "TransactionTerms.Beneficiary", termsPartyBeneficiary.buildAddress(false, true, resMgr), true, false,false, "class='autoHeightTxtAreaQtrWidthNoSidebar' style='overflow-x: hidden;' rows='4' cols='45'", "", "")%>
<%-- cquinton 10/21/2013 Rel 8.3 ir#21819 end --%>
           <%cnt=cnt+1; %>
            </td>
           <%}%>
           <%if(cnt%4 == 0){ %>
</tr> <tr>
<%} %>
          <%}
    } } %>
<%
if(isBILInstrument) { %>
        <tr>             <td>
             <%= widgetFactory.createAutoResizeTextArea("Applicant", "Billing.BillCustomerDetails", termsBillCustomer.buildAddress(false, true, resMgr), true, false,false, "class='autoHeightTxtAreaQtrWidthNoSidebar' style='overflow-x: hidden;' rows='4' cols='45'", "", "")%>
         <%cnt=cnt+1; %>
        </td> </tr>  
         <%}%> 

 </table>
    <br/>


  <%-----------------------------------------------  LIST VIEWS START HERE-------------------------------------------- --%>

    <%--cquinton 9/13/2013 format like other grid headers--%>
    <div class="gridHeader">
      <span class="gridHeaderTitle">
        <%=resMgr.getText("NavigationBar.Transactions",TradePortalConstants.TEXT_BUNDLE)%>
     </span>      
    <%-- DK IR T36000016489 Rel8.3 07/31/2013 starts --%>        
            <span class="gridHeader-right">
          <span class="gridHeaderShowCounts">
            <span><%=resMgr.getText("Home.ShowCountPreText", TradePortalConstants.TEXT_BUNDLE)%></span>
<%
int gridShowCount = 30;
      String show5Class = "gridHeaderShowCountItem";
      String show10Class = "gridHeaderShowCountItem";
      String show20Class = "gridHeaderShowCountItem";
      if ( gridShowCount == 10 ) {
        show5Class += " selected";
      }
      else if ( gridShowCount == 20 ) {
        show10Class += " selected";
      }
      else if ( gridShowCount == 30 ) {
        show20Class += " selected";
      }
%>
            <span id="transactionShowCount10" class="<%= show5Class %>" >10</span>
            <span id="transactionShowCount20" class="<%= show10Class %>" >20</span>
            <span id="transactionShowCount30" class="<%= show20Class %>" >30</span>
          </span>          
        </span>
        <%-- DK IR T36000016489 Rel8.3 07/31/2013 ends --%>    
    </div>


<%
boolean isFromBankMailMessage = false;

if(StringFunction.isNotBlank(request.getParameter("bankMailMessageOid"))||returnAction.equals("goToBankMessagesHome")){
	isFromBankMailMessage = true;
	//hasMailMessagesAccess = false; //IR-45045 Rel94 10/26/2015
}


  DataGridFactory dgFactory1 = new DataGridFactory(resMgr, userSession, formMgr, beanMgr, response);
  String mailGridHtml = "";
  String mailGridLayout = "";
  String LogGridHtml = "";
  String logGridLayout = "";
  String logGridViewName = "";
  String logGridName = "";

  String gridHtml = dgFactory1. createDataGrid("instrumentTransGrid","InstrumentTransactionsDataGrid",null);
  String gridLayout = dgFactory1.createGridLayout("InstrumentTransactionsDataGrid");
  
  // R 94 - CR 932
  String gridHtmlBillTran = dgFactory1. createDataGrid("billingTransGrid","InstrumentTransactionsDataGrid",null);
  String gridLayouBillTran = dgFactory1.createGridLayout("BillingTransactionsDataGrid");

 if (isBILInstrument) {
   out.println(gridHtmlBillTran);
  }else{
	  out.println(gridHtml);
  }%>

    <br/>
<%
if (isFromBankTransUpdatePage){
	logGridViewName = "BankTransactionHistoryLogDataView";
	logGridName = "BankTransactionHistoryLogGrid";
}else{
	logGridViewName = "TransactionHistoryLogDataView";
	logGridName = "TransactionHistoryLogGrid";
}

  DataGridFactory dgFactory3 = new DataGridFactory(resMgr, userSession, formMgr, beanMgr, response);
  LogGridHtml = dgFactory3.createDataGrid("transactionHistoryLogGrid",logGridName,null);
  logGridLayout = dgFactory3.createGridLayout(logGridName);
%>

    <div class="gridHeader">
      <span class="gridHeaderTitle">
        <%=resMgr.getText("InstrumentSummary.TransactionLog",TradePortalConstants.TEXT_BUNDLE)%>
      </span>
    </div>

    <%=LogGridHtml %>

    <br/>
    
<%  DataGridFactory dgFactory2 = new DataGridFactory(resMgr, userSession, formMgr, beanMgr, response);
if (hasMailMessagesAccess) { %>
    <div class="gridHeader">
      <span class="gridHeaderTitle">
        <%=resMgr.getText("SecondaryNavigation.Messages.Mail",TradePortalConstants.TEXT_BUNDLE)%>
      </span>
    </div>

<%
  
  mailGridHtml = dGridFactory.createDataGrid("instrumentMailGrid","InstrumentMailDataGrid",null);
  mailGridLayout = dGridFactory.createGridLayout("InstrumentMailDataGrid");
	 
%>
    <%=mailGridHtml %>
<%
  }
%>



 <%-----------------------------------------------  LIST VIEWS ARE END HERE-------------------------------------------- --%>


  </div>
  <div id="mailMessageDialog"></div>
</form>

</div><%--closes pageContent area--%>
</div><%--closes pageMain area--%>
</div>
<jsp:include page="/common/Footer.jsp">
   <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="minimalHeaderFlag"  value="<%=TradePortalConstants.INDICATOR_NO%>" />
   <jsp:param name="transOid"  value="<%=instrumentOid%>" />
</jsp:include>
</body>
</html>

<%
  //include a hidden form for instrument submission
  // this is used for Amend/Trace/Assign of instruments
  Hashtable newInstrSecParms = new Hashtable();
  newInstrSecParms.put("UserOid", userSession.getUserOid());
  newInstrSecParms.put("SecurityRights", userSession.getSecurityRights());
  newInstrSecParms.put("clientBankOid", userSession.getClientBankOid());
  newInstrSecParms.put("ownerOrg", userSession.getOwnerOrgOid());
%>

  <form method="post" name="NewInstrumentForm" action="<%=formMgr.getSubmitAction(response)%>">
    <input type="hidden" name="mode" value="NEW_INSTRUMENT"/>
    <input type="hidden" name="copyType" value="Blank"/>
    <input type="hidden" name="bankBranch" />
    <input type="hidden" name="instrumentType" />
    <input type="hidden" name="transactionType" />
    <input type="hidden" name="copyInstrumentOid" />
    <%= formMgr.getFormInstanceAsInputField("NewInstrumentForm",newInstrSecParms) %>
  </form>

<%  //include a hidden form for new mail message submission
  Hashtable newMailSecParms = new Hashtable();
  newMailSecParms.put("user_oid", userSession.getUserOid());
  newMailSecParms.put("UserOid", userSession.getUserOid());
  newMailSecParms.put("security_rights", userSession.getSecurityRights());
  newMailSecParms.put("SecurityRights", userSession.getSecurityRights());
  newMailSecParms.put("clientBankOid", userSession.getClientBankOid());
  newMailSecParms.put("ownerOrg", userSession.getOwnerOrgOid());
  newMailSecParms.put("timeZone", userSession.getTimeZone());
%>

  <form method="post" name="MessagesDetailPopupForm" action="<%=formMgr.getSubmitAction(response)%>">
    <input type="hidden" name="MessageMode" value="<%=TradePortalConstants.MESSAGE_CREATE%>" />
    <input type="hidden" name="complete_instrument_id" />
    <input type="hidden" name="message_oid" />
    <input type="hidden" name="message_text" />
    <input type="hidden" name="message_subject"/>
    <input type="hidden" name="is_reply"/>
    <input type="hidden" name="buttonName"/>
    <%= formMgr.getFormInstanceAsInputField("MessagesDetailPopupForm",newMailSecParms) %>
  </form>

<script type="text/javascript">

  <%-- cquinton 10/17/2013 Rel 8.3 ir#21776 
       add variable to determine if user clicked transaction link --%>
  var transLinkClicked = false;
  var lastATranOid;

  <%-- IR T36000003554 PR BEGIN --%>
   var gridLayout = [
{type: "dojox.grid._RadioSelector"},[
{name:"rowKey", field:"rowKey", hidden:"true"} ,
{name:"<%=resMgr.getText("InstrumentSummary.TransStatusDate",TradePortalConstants.TEXT_BUNDLE)%>", field:"TransStatusDate", width:"150px"} ,
{name:"<%=resMgr.getText("InstrumentSummary.Transaction",TradePortalConstants.TEXT_BUNDLE)%>", field:"Transaction", fields:["Transaction","Sequence","Transaction_linkUrl"], formatter:t360gridFormatters.statusSeqFormatter, width:"150px"} ,
{name:"<%=resMgr.getText("InstrumentSummary.Currency",TradePortalConstants.TEXT_BUNDLE)%>", field:"Currency", width:"30px"} ,
{name:"<%=resMgr.getText("InstrumentSummary.Amount",TradePortalConstants.TEXT_BUNDLE)%>", field:"Amount", width:"125px", cellClasses:"gridColumnRight"} ,
{name:"<%=resMgr.getText("InstrumentSummary.Status",TradePortalConstants.TEXT_BUNDLE)%>", field:"Status", width:"150px"} ,
{name:"<%=resMgr.getText("InstrumentSummary.Reference",TradePortalConstants.TEXT_BUNDLE)%>", field:"Reference", fields:["Reference","Reference_linkUrl"], formatter:t360gridFormatters.formatGridLink, width:"180px"} ]];

   <%-- IR T36000003554 PR END --%>
   
   <%--  R94 CR 932 start --%>
    var gridLayoutBilTran = [
{type: "dojox.grid._RadioSelector"},[
{name:"rowKey", field:"rowKey", hidden:"true"} ,
{name:"<%=resMgr.getText("InstrumentSummary.TransStatusDate",TradePortalConstants.TEXT_BUNDLE)%>", field:"TransStatusDate", width:"150px"} ,
{name:"<%=resMgr.getText("InstrumentSummary.Transaction",TradePortalConstants.TEXT_BUNDLE)%>", field:"Transaction", fields:["Transaction","Sequence","Transaction_linkUrl"], formatter:t360gridFormatters.statusSeqFormatter, width:"150px"} ,
{name:"<%=resMgr.getText("InstrumentSummary.Currency",TradePortalConstants.TEXT_BUNDLE)%>", field:"Currency", width:"125px"} ,
{name:"<%=resMgr.getText("BillingHistory.ChargeAmount",TradePortalConstants.TEXT_BUNDLE)%>", field:"Amount", width:"125px", cellClasses:"gridColumnRight"} ,
{name:"<%=resMgr.getText("InstrumentSummary.Status",TradePortalConstants.TEXT_BUNDLE)%>", field:"Status", width:"170px"} ]];
 <%--  R94 CR 932 END --%>

  <%-- cquinton 3/26/2013 Rel PR ir#15207 encrypt the instrument oid --%>

<%// jgadela R 93 - IR T36000040137 %>
  var initSearchParms = "InstrumentOid=<%=EncryptDecrypt.encryptStringUsingTripleDes(instrumentOid, userSession.getSecretKey())%>&suppressValue=<%= instrument.getAttribute("complete_instrument_id") %>";
 
  var viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("InstrumentTransactionsDataView",userSession.getSecretKey())%>";  <%--Rel9.2 IR T36000032596 --%>
 <% if (isBILInstrument) {%>
 	  viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("BillingTransactionsDataView",userSession.getSecretKey())%>";  <%--Rel9.2 IR T36000032596 --%>
  	  createDataGrid("billingTransGrid", viewName, gridLayoutBilTran, initSearchParms,'-1');
<%}else{%>
	  createDataGrid("instrumentTransGrid", viewName, gridLayout, initSearchParms,'-1');
<%}%>
<%if( StringFunction.isNotBlank(mailGridLayout) && (!isFromBankMailMessage || hasMailMessagesAccess) ) {// IR-45045 Rel94%>
	require(["t360/OnDemandGrid", "dijit/registry", "t360/datagrid", "dojo/dom-construct", "dojo/domReady!"],
		      function( onDemandGrid, registry, t360grid, domConstruct ) {
var mailGridLayout = <%= mailGridLayout %>;
<%-- cquinton 3/26/2013 Rel PR ir#15207 encrypt the instrument oid --%>
var mailInitSearchParms = "InstrumentOid=<%=EncryptDecrypt.encryptStringUsingTripleDes(instrumentOid, userSession.getSecretKey())%>";
viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("InstrumentMailDataView",userSession.getSecretKey())%>";  <%--Rel9.2 IR T36000032596 --%>
onDemandGrid.createOnDemandGrid("instrumentMailGrid", viewName,mailGridLayout, mailInitSearchParms,'-1');
	 });
<%}%>
  <%--  DK IR T36000016489 Rel8.3 07/31/2013 starts --%>
  require(["dojo/_base/array", "dojo/dom-attr", "dijit/registry", "dojo/query", "dojo/on", "dojo/dom-class", "t360/dialog",
           "dojo/dom", "dijit/layout/ContentPane", "dijit/TooltipDialog", "dijit/popup",
           "t360/common", "t360/popup", "t360/datagrid","dojo/aspect","dojo/dom-style",
           "dojo/ready", "dojo/domReady!", "dojo/_base/array"],
    function(baseArray, domAttr, registry, query, on, domClass, dialog,
             dom, ContentPane, TooltipDialog, popup,
             common, t360popup, t360grid, aspect, domStyle,
             ready ) {
      query('#transactionShowCount10').on("click", function() {
	        var myGrid = registry.byId("instrumentTransGrid");
	        <% // R 94 CR 932 - If the instrument is Billing instrument , get billing instrument grin id name 
			if (isBILInstrument) {%>
			myGrid = registry.byId("billingTransGrid");
			<%}%>
	        myGrid.set('autoHeight',10);
	        domClass.remove("transactionShowCount20",'selected');
	        domClass.remove("transactionShowCount30",'selected');
	        domClass.add("transactionShowCount10",'selected');
	      });
      query('#transactionShowCount20').on("click", function() {
    	  
	        var myGrid = registry.byId("instrumentTransGrid");
	        <% // R 94 CR 932 - If the instrument is Billing instrument , get billing instrument grin id name 
			if (isBILInstrument) {%>
			myGrid = registry.byId("billingTransGrid");
			<%}%>
	        myGrid.set('autoHeight',20);
	        domClass.remove("transactionShowCount10",'selected');
	        domClass.remove("transactionShowCount30",'selected');
	        domClass.add("transactionShowCount20",'selected');
	      });
      query('#transactionShowCount30').on("click", function() {
	        var myGrid = registry.byId("instrumentTransGrid");
	        <% // R 94 CR 932 - If the instrument is Billing instrument , get billing instrument grin id name 
			if (isBILInstrument) {%>
			myGrid = registry.byId("billingTransGrid");
			<%}%>
	        myGrid.set('autoHeight',30);
	        domClass.remove("transactionShowCount10",'selected');
	        domClass.remove("transactionShowCount20",'selected');
	        domClass.add("transactionShowCount30",'selected');
	      });
      
      
      
      <%--  DK IR T36000016489 Rel8.3 07/31/2013 ends --%>
      ready(function(){
	var myGrid1 = registry.byId("instrumentTransGrid");
        <%--  DK IR T36000016489 Rel8.3 07/31/2013 - remove autoheight --%>
        myGrid1.set('autoHeight',30);
			
        if(registry.byId("instrumentMailGrid") != undefined){
          var myGrid2 = registry.byId("instrumentMailGrid");
          myGrid2.set('autoHeight',5);
        }
	        
        if(registry.byId("transactionHistoryLogGrid") != undefined){
          var myGrid3 = registry.byId("transactionHistoryLogGrid");
          myGrid3.set('autoHeight',5);
        }
      });
      
      if (dom.byId('instrumentMailGrid')){
		    on(dom.byId('instrumentMailGrid'), 'dgrid-refresh-complete', function (event) {
		    	setDynamicGridHeitht(null, 'instrumentMailGrid');
		    });
	    }
      
      function setDynamicGridHeitht(event, gridId){
			var myGrid = registry.byId(gridId);
			var rowCount =5;
			<%--  Calculate the height of the scrollbar --%>
			<%-- var dgridScrollerNode = query('.dgrid-scroller', myGrid.domNode)[0]; --%>
			  var dgridHeaderNode = myGrid.headerNode;
			  
		    var dgridScrollerNode = myGrid.bodyNode;
		    var scrollBarHeight = dgridScrollerNode.offsetHeight - dgridScrollerNode.clientHeight;
		    
			<%-- Include column header height with the grid height --%>
			var maxHeight = (myGrid.rowHeight * (rowCount)) + (myGrid.headerNode.offsetHeight+scrollBarHeight) + 'px';
			var scrollMaxHeight = (myGrid.rowHeight * (rowCount)) + scrollBarHeight + 'px';
			
			domStyle.set(myGrid.domNode, 'maxHeight', maxHeight);
			<%-- domStyle.set(dgridScrollerNode, 'maxHeight', scrollMaxHeight); --%>
			domStyle.set(dgridScrollerNode, 'height', scrollMaxHeight);
			var recCount = myGrid._total;
			
			<%--  below code ensures grid does not show blanks rows when records count is less  --%>
			<%--  then the selected row count. e.g. when row count is 10 and record count is 8 the we --%>
			<%--  show only 8 rows. --%>
			if (recCount < rowCount){
			    var dgridContentNode = myGrid.contentNode;

			    <%-- Adding this code to to prevent IE8 crash --%>
			    if(navigator.appVersion.indexOf("MSIE 8")==-1){
			    	dgridScrollerNode.style.height = '';
			    }
			    <%-- dgridScrollerNode.style.height = ''; commented as it cause horizontal scroll in IE8 to disappear. --%>
			    dgridContentNode.style.height = 'auto';
			    if (dgridContentNode.offsetHeight < dgridScrollerNode.offsetHeight) {
			    	var dgScrlNodeOffsetHght = dgridContentNode.offsetHeight;
					dgridScrollerNode.style.height = dgScrlNodeOffsetHght + 'px';
					if (recCount == 0){
						dgridScrollerNode.style.height = '';
						dgridScrollerNode.style.height = (dgScrlNodeOffsetHght+15) + 'px';
					}else if (recCount <= rowCount){
						if (dgridScrollerNode.scrollWidth > dgridScrollerNode.offsetWidth){
			    			dgridScrollerNode.style.height = (dgridContentNode.offsetHeight + myGrid.rowHeight) + 'px';
						} else{
			    			dgridScrollerNode.style.height = (dgridContentNode.offsetHeight) + 'px';
			    		}
			    	}
			    }else{
			    	if (recCount < rowCount){
						if (dgridScrollerNode.scrollWidth > dgridScrollerNode.offsetWidth){
			    			dgridScrollerNode.style.height = (dgridContentNode.offsetHeight + myGrid.rowHeight) + 'px';
						} else{
			    			dgridScrollerNode.style.height = (dgridContentNode.offsetHeight) + 'px';
			    		}

			    	}
			    }
			    dgridContentNode.style.height = '';
		    }
		} <%-- end of setgrid --%>
      
      
      
	  
      <%--SSikhakolli - Rel-8.3 SysTest IR#T36000021048 on 10/04/2013 - Begin--%>
      <%-- cquinton 10/17/2013 Rel 8.3 ir#21776 start
        //after the transaction grid fetches data, create the
        // transaction history grid and register its event handlers --%>
      var myGrid = registry.byId("instrumentTransGrid");
      <% // R 94 CR 932 - If the instrument is Billing instrument , get billing instrument grin id name 
		if (isBILInstrument) {%>
		myGrid = registry.byId("billingTransGrid");
		<%}%>
      
      dojo.connect(myGrid,"_onFetchComplete",function(items){
        myGrid.selection.setSelected(0, true);
 
        var origTranOid;
        var objs = myGrid.selection.getSelected();
        baseArray.forEach(objs, function(item){		        	   
          origTranOid = this.store.getValue(item, "rowKey");
        }, this);

        var logGridLayout = <%= logGridLayout %>;
        var logInitSearchParms="origTranOid="+origTranOid;
        var viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes(logGridViewName,userSession.getSecretKey())%>";  <%--Rel9.2 IR T36000032596 --%>
        t360grid.createDataGrid("transactionHistoryLogGrid", viewName,logGridLayout, logInitSearchParms,'-1');

        <%-- there is an issue with refreshing transaction history on selecting the transaction link.
             lets make this simple and set a flag if the user clicks a link in a grid and then avoid the update.
             note: this event fires first becuase its lower level. --%>
        query('.dojoxGridCell').on('a:click', function(evt) {
          transLinkClicked = true;
        });

        myGrid.on("SelectionChanged", function(){
          <%--   this will be an array of dojo/data items --%>
          var items = this.selection.getSelected();
          <%-- update history only when it wasn't the 
               transaction link being clicked --%>
          if ( !transLinkClicked) {
            var aTranOid = ''; <%-- returns not found if no item selected --%>
            if ( items && items.length>0 ) {
              <%--   get the data of each column of selected grid and set the data to fields in the form --%>
              baseArray.forEach(items, function(item){		        	   
                aTranOid = this.store.getValue(item, "rowKey");
              }, this);
              <%-- MEerupula 10/29/2013 Rel 8.3 IR-22468 for radio button selection--%>
           <%--  } --%>

            <%-- last of all check if the transaction actually changed before refetching --%>
            if(aTranOid != undefined && aTranOid != "undefined" && aTranOid != lastATranOid ){
              lastATranOid = aTranOid;
              var searchParms="origTranOid="+aTranOid;
              var logGridName = "<%=logGridName%>"
              t360grid.searchDataGrid("transactionHistoryLogGrid", logGridName, searchParms);
              <%-- redo the event handler after search --%>
              query('.dojoxGridCell').on('a:click', function(evt) {
                transLinkClicked = true;
              });
            }
          } <%-- MEerupula 10/29/2013 Rel 8.3 IR-22468 --%>
          }
        }, true);
      }, true); 
      <%-- cquinton 10/17/2013 Rel 8.3 ir#21776 end --%>
      <%--SSikhakolli - Rel-8.3 SysTest IR#T36000021048 on 10/04/2013 - End--%>
    });
	
  function openURL(URL){
	    if (isActive =='Y') {
	    	if (URL != '' && URL.indexOf("javascript:") == -1) {
		    	var cTime = (new Date()).getTime();
		        URL = URL + "&cTime=" + cTime;
		        URL = URL + "&prevPage=" + context;
	    	}
	    }
		 document.location.href  = URL;
		 return false;
  }

  function createAmendment() {
	if ( document.getElementsByName('NewInstrumentForm').length > 0 ) {
		var theForm = document.getElementsByName('NewInstrumentForm')[0];
        theForm.mode.value='USE_EXISTING_INSTR';
        theForm.copyInstrumentOid.value=<%=instrumentOid%>;
        theForm.transactionType.value='AMD';
        theForm.submit();
        return false;
	}
  }
  
  function createSettlementInstructions() {
     	if ( document.getElementsByName('NewInstrumentForm').length > 0 ) {
     		var theForm = document.getElementsByName('NewInstrumentForm')[0];
             theForm.mode.value='USE_EXISTING_INSTR';
             theForm.copyInstrumentOid.value=<%=instrumentOid%>;
             theForm.transactionType.value='SIM';
             theForm.submit();
             return false;
     	}
  }
  
  function requestRollover() {
   	if ( document.getElementsByName('NewInstrumentForm').length > 0 ) {
   		var theForm = document.getElementsByName('NewInstrumentForm')[0];
           theForm.mode.value='USE_EXISTING_INSTR';
           theForm.copyInstrumentOid.value=<%=instrumentOid%>;
           theForm.transactionType.value='SIM';
           theForm.settleInstrWorkItemType.value='WRO';
           theForm.submit();
           return false;
   	}
}
  
  function createConversionPayment(instrumentOid) {
   	if ( document.getElementsByName('NewInstrumentForm').length > 0 ) {
   		var theForm = document.getElementsByName('NewInstrumentForm')[0];
           theForm.mode.value='USE_EXISTING_INSTR';
           theForm.copyInstrumentOid.value=instrumentOid;
           theForm.transactionType.value='PAY';
           theForm.ConvTransInd.value='Y';
           theForm.submit();
           return false;
   	}
  }

  function createConversionAmendment(instrumentOid) {
  	if ( document.getElementsByName('NewInstrumentForm').length > 0 ) {
  		var theForm = document.getElementsByName('NewInstrumentForm')[0];
          theForm.mode.value='USE_EXISTING_INSTR';
          theForm.copyInstrumentOid.value=instrumentOid;
          theForm.transactionType.value='AMD';
          theForm.ConvTransInd.value='Y';
          theForm.submit();
          return false;
  	}
  }  
  function createTracer() {
	if ( document.getElementsByName('NewInstrumentForm').length > 0 ) {
		var theForm = document.getElementsByName('NewInstrumentForm')[0];
        theForm.mode.value='USE_EXISTING_INSTR';
        theForm.copyInstrumentOid.value=<%=instrumentOid%>;
        theForm.transactionType.value='TRC';
        theForm.submit();
        return false;
	}
  }

  function createAssignment() {
	if ( document.getElementsByName('NewInstrumentForm').length > 0 ) {
		var theForm = document.getElementsByName('NewInstrumentForm')[0];
		theForm.mode.value='USE_EXISTING_INSTR';
        theForm.copyType.value='Instr';
        theForm.copyInstrumentOid.value=<%=instrumentOid%>;
        theForm.submit();
        return false;
	}
  }

  function createMessage(){
	 require(["t360/dialog"], function(dialog) {
	      dialog.open('mailMessageDialog', "<%=resMgr.getText("Message.NewMailMessageHeader",TradePortalConstants.TEXT_BUNDLE)%>",
	                      'NewMailMessagePopup.jsp',
	                      null, null, <%-- parameters --%>
	                      'select', this.createNewMailMessage);
	  });
  }

  function createNewMailMessage(buttonName,instrumentID,messageSubject,messageText){
	require(["t360/common"], function(common) {
		if ( document.getElementsByName('MessagesDetailPopupForm').length > 0 ) {
			var theForm = document.getElementsByName('MessagesDetailPopupForm')[0];
			theForm.message_subject.value = messageSubject;
			theForm.message_text.value = messageText;
			theForm.complete_instrument_id.value = instrumentID;
			theForm.buttonName.value = buttonName;
			theForm.is_reply.value='N';
			common.setButtonPressed(buttonName,0);
			theForm.submit();
		}
	});
  }
  
  <%-- IR T36000021686 Rel 8.3 - For performance improvement --%>
  function panelLevelAliasesFormatter(columnValues){
	  var panelLevel = columnValues[0];
	  var panelAlias="";
	  if(panelLevel != ""){
		  panelAlias = panelStore.get(panelLevel).name;
	  }
	  return panelAlias;
	  
  }
</script>
<%
   // Finally, reset the cached document to eliminate carryover of
   // information to the next visit of this page.
   xmlDoc = formMgr.getFromDocCache();

   xmlDoc.removeAllChildren("/");

   formMgr.storeInDocCache("default.doc", xmlDoc);
%>
