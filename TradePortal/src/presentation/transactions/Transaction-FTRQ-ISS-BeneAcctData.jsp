<%--
*******************************************************************************
  International Payment Beneficiary Account Data section

  Description:
  This jsp is called via ajax and returns a snippet of html associated with
  a new terms party for the accounts associated with a beneficiary.  
  The user can then select an existing account or enter a new one.

  This is dependent on the common Transaction-FTRQ-ISS-BeneAcctData.frag
  which generically handles the terms party account section, either from here 
  or from the instrument page itself.

  Note: this assumes non readonly mode.
  
*******************************************************************************
--%>
<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.html.*,java.math.*,java.util.*" %>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session"> </jsp:useBean>
<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session"> </jsp:useBean>
<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session"> </jsp:useBean>
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"> </jsp:useBean>
   

<%
  String partyType = request.getParameter("partyType");
  String encryptedPartyRowKey = request.getParameter("partyRowKey");

  String partyOid = "";
  String partyRowKey = EncryptDecrypt.decryptStringUsingTripleDes(encryptedPartyRowKey, userSession.getSecretKey());
  StringTokenizer strTok = new StringTokenizer(partyRowKey,"/");
  if (strTok.hasMoreTokens()) {
    partyOid = strTok.nextToken();
  }

  boolean isReadOnly = false;
  boolean isTemplate = false;
  String isTemplateStr = request.getParameter("isTemplate");
  if ( "true".equals(isTemplateStr) ) {
    isTemplate = true;
  }
  WidgetFactory widgetFactory = new WidgetFactory(resMgr, userSession);

  TermsPartyWebBean termsParty = beanMgr.createBean(TermsPartyWebBean.class, "TermsParty");

  // Use a Party web bean to get the party data for the selected oid.
  PartyWebBean party = beanMgr.createBean(PartyWebBean.class, "Party");
  //todo: what if partyOid is not populated
  party.setAttribute("party_oid", partyOid);
  party.getDataFromAppServer();

  DocumentHandler partyDoc = new DocumentHandler();
  party.populateXmlDoc(partyDoc);
  partyDoc = partyDoc.getFragment("/Party");

  //load the terms party web bean
  // not entirely sure why we do differently per party type, but this was taken from int payment
  if (TradePortalConstants.PAYEE.equals(partyType)) {
    // use the "FromDocTagsOnly" so we don't overwrite the acct_currency
    termsParty.loadTermsPartyFromDocTagsOnly(partyDoc);

    // For a beneficary selection, the beneficiary bank should be populated
    // if there is a designated bank.
    //???: loadDesignatedParty(doc, termsPartyPayeeBank, beanMgr);

    // Now get the account data for this party and load it to the acct_choices field.  The
    // value is XML similar to a query result (i.e., /ResultSetRecord).

    termsParty.loadAcctChoices(partyOid, formMgr.getServerLocation(), resMgr);
  }
  else if (TradePortalConstants.PAYER.equals(partyType)) {
    termsParty.loadTermsPartyFromDoc(partyDoc);
    termsParty.setAttribute("address_search_indicator", "N");

    // Now get the account data for this party and load it to the acct_choices field.  The
    // value is XML similar to a query result (i.e., /ResultSetRecord).

    termsParty.loadAcctChoices(partyOid, formMgr.getServerLocation(), resMgr);
  }
  else {
    //do nothing
  }

  //termsParty is now populated with account data
%>

 <%@ include file="fragments/Transaction-FTRQ-ISS-BeneAcctData.frag" %>
