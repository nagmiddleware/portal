<%--
*******************************************************************************
                              Request to Advise Amend Page

  Description:
*******************************************************************************
--%>
    
<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
				 com.ams.tradeportal.busobj.*, java.lang.*,
                 com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.html.*,java.math.*,com.amsinc.ecsg.util.DateTimeUtility,java.util.*" %>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager"
    scope="session">
</jsp:useBean>   
  
<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" 
   scope="session">
</jsp:useBean>


<%-- ************** Data retrieval page setup begins here ****************  --%>

<%
  WidgetFactory widgetFactory = new WidgetFactory(resMgr, userSession);
  //cquinton 2/12/2013 add refdata manager for datepattern lookup
  ReferenceDataManager refData = ReferenceDataManager.getRefDataMgr();

  String links="";
  String focusField = "TransactionAmount";   // Default focus field

  // Various oid and status info from transaction and instruments used in
  // several places.
  String instrumentOid = "";         
  String transactionOid;
  String instrumentType;
  String instrumentStatus;
  String transactionType; 
  String transactionStatus;

  boolean           amountUpdatedFromPOs          = false;

  boolean getDataFromDoc;          // Indicates if data is retrieved from the
                                   // input doc cache or from the database

  DocumentHandler doc;

  String loginLocale = userSession.getUserLocale();
  String loginRights = userSession.getSecurityRights();

  //cquinton 2/12/2013 lookup locale specific datePattern description from refData manager
  String datePattern = userSession.getDatePattern();
  String datePatternDisplay = refData.getDescr("DATEPATTERN", datePattern, loginLocale);
  String dateWidgetOptions = "placeHolder:'" + datePatternDisplay + "'";

  // Applicant name and expiry date from the issue transaction.  Displayed in General section
  String applicantName = "";
  String currentExpiryDate = null;

  // These are local variables used in the Shipment section for the PO line item
  // buttons and text area box
  DocumentHandler   poLineItemsDoc                = null;
  DocumentHandler   poLineItemDoc                 = null;
  StringBuffer      sqlQuery                      = null;
  StringBuffer      link                          = null;
  boolean           hasPOLineItems                = false;
  String            poLineItemUploadDefinitionOid = null;
  String            userOrgAutoLCCreateIndicator  = null;
  String            userCustomerAccessIndicator   = null;
  String            poLineItemBeneficiaryName     = null;
  String            poLineItemCurrency            = null;
  boolean 		  isProcessedByBank		  = false;
  Vector            poLineItems                   = null;
  String			rejectionIndicator            = "";
  String			rejectionReasonText	          = "";

  // Dates are stored in the database as one field but displayed on the
  // page as 3.  These variables hold the pieces for each date.
  String expiryDay = "";
  String expiryMonth = "";
  String expiryYear = "";
  String shipDay = "";
  String shipMonth = "";
  String shipYear = "";

  // Variables used for populating ref data dropdowns.
  String options;
  String defaultText;

  // This array of string is used when creating the RQA Amend Application Form links.
  String linkArgs[] = {"","","",""};
 
  // These are the beans used on the page.

  TransactionWebBean transaction  = (TransactionWebBean)
                                      beanMgr.getBean("Transaction");
  InstrumentWebBean instrument    = (InstrumentWebBean)
                                      beanMgr.getBean("Instrument");
  TemplateWebBean template        = (TemplateWebBean)
                                      beanMgr.getBean("Template");
  TermsWebBean terms              = null;

  ShipmentTermsWebBean shipmentTerms = beanMgr.createBean(ShipmentTermsWebBean.class, "ShipmentTerms");

  // We need the amount of the original transaction to display.  Create a webbean
  // to hold this data.
  TransactionWebBean activeTransaction = beanMgr.createBean(TransactionWebBean.class, "Transaction");

  // Get the document from the cache.  We'll may use it to repopulate the 
  // screen if returning from another page, a save, validation, or any other
  // mediator called from this page.  Otherwise, we assume an instrument oid
  // and transaction oid was passed in.

  doc = formMgr.getFromDocCache();
  Vector error = null;
  error = doc.getFragments("/Error/errorlist/error");
  
  String buttonClicked = request.getParameter("buttonName");
  //Debug.debug("doc from cache is " + doc.toString());

/******************************************************************************
  We are either entering the page or returning from somewhere.  These are the 
  conditions for how to populate the web beans.  Data comes from either the
  database or the doc cache (/In section) with some variation.

  Mode           Condition                      Populate Beans From
  -------------  ----------------------------   --------------------------------
  Enter Page     no /In/Transaction in doc      Instrument and Template web
                                                beans already populated, get
                                                data for Terms and TermsParty
                                                web beans from database

  return from    /In/NewPartyFromSearchInfo/PartyOid   doc cache (/In); also use Party
  Party Search     exists                       SearchInfo to lookup, and 
                                                populate a specific TermsParty
                                                web bean

  return from    /Out/PhraseLookupInfo exists   doc cache (/In); but use Phrase
  Phrase Lookup                                 LookupInfo text (from /Out) to
                                                replace a specific phrase text
                                                in the /In document before 
                                                populating

  return from    /Error/maxerrorseverity < 1    Same as Enter Page (data is
  Transaction                                   retrieved from database)
    mediator
    (no error)

  return from    /Error/maxerrorseverity > 0    doc cache (/In)
  Transaction    
    mediator
    (error)
******************************************************************************/

  // Assume we get the data from the doc.
  getDataFromDoc = true;

  String maxError = doc.getAttribute("/Error/maxerrorseverity");
  if (maxError != null && maxError.compareTo(
              String.valueOf(ErrorManager.WARNING_ERROR_SEVERITY)) < 0) {
     // No errors, so don't get the data from doc.
     getDataFromDoc = false;
  }
  //ir cnuk113043991 - check to see if transaction needs to be refreshed
  // if so, refresh it and do not get data from doc as it is wrong
  if ( "Y".equals(doc.getAttribute("/Out/Refresh/Transaction")) ) {
     transaction.getDataFromAppServer();
     getDataFromDoc = false;
  }
  
  String newTransaction = null;
  if (doc.getDocumentNode("/Out/newTransaction")!=null) {
    newTransaction = doc.getAttribute("/Out/newTransaction");
    session.setAttribute("newTransaction", newTransaction);
    System.out.println("found newTransaction = "+ newTransaction);
  } 
  else {
    newTransaction = (String) session.getAttribute("newTransaction");
    if ( newTransaction==null ) {
      newTransaction = TradePortalConstants.INDICATOR_NO;
    }
    System.out.println("used newTransaction from session = " + newTransaction);
  }
  if (doc.getDocumentNode("/In/NewPartyFromSearchInfo/Type") != null) {
     // We have returned from the party search page.  Get data from doc cache
     getDataFromDoc = true;
  }
  if (doc.getDocumentNode("/Out/ChangePOs") != null) {

     // Some data was updated by the add/remove POs process.  
     // Update the data in the XML so that the page will relect the changes   
     doc.setAttribute("/In/Terms/ShipmentTermsList/po_line_items",doc.getAttribute("/Out/ChangePOs/po_line_items"));
     doc.setAttribute("/In/Terms/amount", doc.getAttribute("/Out/ChangePOs/amount"));
     doc.setAttribute("/In/Terms/ShipmentTermsList/goods_description", doc.getAttribute("/Out/ChangePOs/goods_description"));

     //pmitnala 3/05/2013 Changes Begin for IR-T36000014541, PR BMO Issue-123 - Expiry Date or the Latest Ship Date were not derived from uploaded PO
     doc.setAttribute("/In/Terms/expiry_date", doc.getAttribute("/Out/ChangePOs/expiry_date"));
     doc.setAttribute("/In/Terms/ShipmentTermsList/latest_shipment_date", doc.getAttribute("/Out/ChangePOs/latest_shipment_date"));
     //pmitnala 3/05/2013 Changes End for IR-T36000014541	
     getDataFromDoc = true;
     amountUpdatedFromPOs = true;
   }
  if (doc.getDocumentNode("/In/Transaction") == null) {
     // No /In/Transaction means we've never looked up the data.
     Debug.debug("No /In/Transaction section - get data from database");
     getDataFromDoc = false;
  }
  if (doc.getDocumentNode("/Out/PhraseLookupInfo") != null) {
     // A Looked up phrase exists.  Replace it in the /In document.
     Debug.debug("Found a looked-up phrase");
     getDataFromDoc = true;

     // Take the looked up and appended phrase text from the /Out section
     // and copy it to the /In section.  This allows the beans to be 
     // properly populated.
     String xmlPath = doc.getAttribute("/In/PhraseLookupInfo/text_path");
     xmlPath = "/In" + xmlPath;

     doc.setAttribute(xmlPath, 
                      doc.getAttribute("/Out/PhraseLookupInfo/NewText"));

     // If we returned from the phrase lookup without errors, set the focus
     // to the correct field.  Otherwise, don't set the focus so the user can
     // see the error.
     if (maxError != null && maxError.compareTo(
              String.valueOf(ErrorManager.WARNING_ERROR_SEVERITY)) < 0) {
       focusField = doc.getAttribute("/In/PhraseLookupInfo/text_field_name");
     }
  }

  if (getDataFromDoc) {
     Debug.debug("Populating beans from doc cache");

     // Populate the beans from the input doc.
     try {
        instrument.populateFromXmlDoc(doc.getComponent("/In"));
        transaction.populateFromXmlDoc(doc.getComponent("/In"));
        template.populateFromXmlDoc(doc.getComponent("/In"));

        terms = (TermsWebBean) beanMgr.getBean("Terms");
        terms.populateFromXmlDoc(doc, "/In");

        expiryDay = doc.getAttribute("/In/Terms/expiry_day");
        expiryMonth = doc.getAttribute("/In/Terms/expiry_month");
        expiryYear = doc.getAttribute("/In/Terms/expiry_year");

        shipDay = doc.getAttribute("/In/Terms/shipment_day");
        shipMonth = doc.getAttribute("/In/Terms/shipment_month");
        shipYear = doc.getAttribute("/In/Terms/shipment_year");

        String activeTransOid = instrument.getAttribute("active_transaction_oid");
        if (!InstrumentServices.isBlank(activeTransOid)) {
           activeTransaction.setAttribute("transaction_oid", activeTransOid);
           activeTransaction.getDataFromAppServer();
        }

        // Some of the data for this transaction is stored as part of the shipment terms
        terms.populateFirstShipmentFromXml(doc);
     } catch (Exception e) {
        out.println("Contact Administrator: "
              + "Unable to reload data after returning to page. "
              + "Error is " + e.toString());
     }
  } else {
     Debug.debug("populating beans from database");
     // We will perform a retrieval from the database.
     // Instrument and Transaction were already retrieved.  Get
     // the rest of the data

     terms = transaction.registerCustomerEnteredTerms();

        String activeTransOid = instrument.getAttribute("active_transaction_oid");
        if (!InstrumentServices.isBlank(activeTransOid)) {
           activeTransaction.setAttribute("transaction_oid", activeTransOid);
           activeTransaction.getDataFromAppServer();
        }


     // Because date fields are represented as three fields on the page,
     // we need to parse out the date values into three fields.  Pre-mediator
     // logic puts these three fields back together for the terms bean.
     String date = terms.getAttribute("expiry_date");
     expiryDay = TPDateTimeUtility.parseDayFromDate(date);
     expiryMonth = TPDateTimeUtility.parseMonthFromDate(date);
     expiryYear = TPDateTimeUtility.parseYearFromDate(date);

     // Some of the data for this transaction is stored as part of the shipment terms
     terms.populateFirstShipmentFromDatabase();

     date = terms.getFirstShipment().getAttribute("latest_shipment_date");
     shipDay = TPDateTimeUtility.parseDayFromDate(date);
     shipMonth = TPDateTimeUtility.parseMonthFromDate(date);
     shipYear = TPDateTimeUtility.parseYearFromDate(date);
  }

  // Setup Amount Tolerance and Maximum Credit Amount radio buttons.
  String amountToleranceType;
  String maximumCreditAmount = terms.getAttribute("maximum_credit_amount");
  String amountTolerancePlus = terms.getAttribute("amt_tolerance_pos");
  String amountToleranceMinus = terms.getAttribute("amt_tolerance_neg");
  if (!InstrumentServices.isBlank(maximumCreditAmount)) {
  	amountToleranceType = TradePortalConstants.NOT_EXCEEDING;
  } else if (!InstrumentServices.isBlank(amountTolerancePlus) ||
  	!InstrumentServices.isBlank(amountToleranceMinus)) {
	amountToleranceType = TradePortalConstants.AMOUNT_TOLERANCE;
  } else { // niether radio button should be checked
  	amountToleranceType = "";
  }

  // Now determine the mode for how the page operates (readonly, etc.)
  BigInteger requestedSecurityRight = SecurityAccess.DLC_CREATE_MODIFY;
%>
  <%@ include file="fragments/Transaction-PageMode.frag" %>


<%
  // Now we need to calculate the computed new lc amount total.  This involves 
  // using the instument amount of the active transaction, the amount entered by the user,
  // performing some validation and then doing the calculation.

  MediatorServices medService = new MediatorServices();
  medService.getErrorManager().setLocaleName(resMgr.getResourceLocale());
  BigDecimal originalAmount = BigDecimal.ZERO;
  BigDecimal offsetAmount = BigDecimal.ZERO;
  BigDecimal calculatedTotal = BigDecimal.ZERO;
  String amountValue = null;
  String displayAmount;
  String incrDecrValue = TradePortalConstants.INCREASE;
  String currencyCode = activeTransaction.getAttribute("copy_of_currency_code");
  boolean badAmountFound = false;

  // Convert the original transaction amount to a double
  try {
     amountValue = activeTransaction.getAttribute("instrument_amount");
     originalAmount = new BigDecimal(amountValue);
  } catch (Exception e) {
     originalAmount = BigDecimal.ZERO;
  }

  if (getDataFromDoc && doc.getAttribute("/In/Terms/amount") != null) {
     //
     // Furthermore, since the displayed terms amount is always displayed as a 
     // positive number (the Increase/Decrease dropdown represents the sign).
     // Therefore, we may need to set the double amount negative.

     amountValue = doc.getAttribute("/In/Terms/amount");
     incrDecrValue = doc.getAttribute("/In/Terms/IncreaseDecreaseFlag");

     try {
        //<Papia - IR No. NNUF021050622 - 06/03/05>
        amountValue = TPCurrencyUtility.getDisplayAmount(amountValue, 
                                                   currencyCode, loginLocale);
        //</ Papia - IR No. NNUF021050622 - 06/03/05>
        amountValue = NumberValidator.getNonInternationalizedValue(amountValue, 
                                                          loginLocale, false);
        try {
           offsetAmount = new BigDecimal(amountValue);
        } catch (Exception e) {
           offsetAmount = BigDecimal.ZERO;
        }
        if (incrDecrValue.equals(TradePortalConstants.DECREASE)) {
           offsetAmount = offsetAmount.negate();
        }

        if(amountUpdatedFromPOs) {
           displayAmount = terms.getAttribute("amount"); 
           if( InstrumentServices.isBlank(displayAmount) && !isReadOnly ) {
              displayAmount = "";
           }
           else {
              displayAmount = TPCurrencyUtility.getDisplayAmount(displayAmount, currencyCode, loginLocale);
           }
        }
        else {
           displayAmount = doc.getAttribute("/In/Terms/amount");
        }

     } catch (Exception e) {
        // Unable to unformat number so the display value is the save value
        // the user typed in.
        displayAmount = amountValue;
        badAmountFound = true;
        medService.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                                  TradePortalConstants.INVALID_CURRENCY_FORMAT,
                                  amountValue);
        medService.addErrorInfo();
        doc.addComponent("/Error", medService.getErrorDoc());
        formMgr.storeInDocCache("default.doc", doc);
     }
  } else {
     // We're getting the amount from the database so convert to a positive
     // number if necessary and set the Increase/Decrease dropdown accordingly.
     try {
        amountValue = terms.getAttribute("amount");
        offsetAmount = new BigDecimal(amountValue);
     } catch (Exception e) {
        offsetAmount = BigDecimal.ZERO;
     }
     if (offsetAmount.doubleValue() < 0) incrDecrValue = TradePortalConstants.DECREASE;
     if( InstrumentServices.isBlank( amountValue )  && !isReadOnly) {
   	    displayAmount = "";
     }
     else{
        displayAmount = TPCurrencyUtility.getDisplayAmount(
                            offsetAmount.abs().toString(), currencyCode, loginLocale);
     }
  }
  
  calculatedTotal = originalAmount.add(offsetAmount);

  transactionType = transaction.getAttribute("transaction_type_code");
  transactionStatus = transaction.getAttribute("transaction_status");
  transactionOid = transaction.getAttribute("transaction_oid");
  
  // Get the transaction rejection indicator to determine whether to show the rejection reason text
  rejectionIndicator = transaction.getAttribute("rejection_indicator");
  rejectionReasonText = transaction.getAttribute("rejection_reason_text");

  instrumentType = instrument.getAttribute("instrument_type_code");
  instrumentStatus = instrument.getAttribute("instrument_status");

  Debug.debug("Instrument Type " + instrumentType);
  Debug.debug("Instrument Status " + instrumentStatus);
  Debug.debug("Transaction Type " + transactionType);
  Debug.debug("Transaction Status " + transactionStatus);

  // Create documents for the phrase dropdowns. First check the cache (no need
  // to recreate if they already exist).  After creating, place in the cache.
  // (InstrumentCloseNavigator.jsp cleans these up.)

  DocumentHandler phraseLists = formMgr.getFromDocCache("PhraseLists");
  if (phraseLists == null) phraseLists = new DocumentHandler();

  DocumentHandler goodsDocList = phraseLists.getFragment("/Goods");
  if (goodsDocList == null) {
     goodsDocList = PhraseUtility.createPhraseList(
                                    TradePortalConstants.PHRASE_CAT_GOODS, 
                                    userSession, formMgr, resMgr);
     phraseLists.addComponent("/Goods", goodsDocList);
  }

  DocumentHandler addlCondDocList = phraseLists.getFragment("/AddlCond");
  if (addlCondDocList == null) {
     addlCondDocList = PhraseUtility.createPhraseList(
                                    TradePortalConstants.PHRASE_CAT_ADDL_COND, 
                                    userSession, formMgr, resMgr);
     phraseLists.addComponent("/AddlCond", addlCondDocList);
  }

  DocumentHandler spclInstrDocList = phraseLists.getFragment("/SpclInstr");
  if (spclInstrDocList == null) {
     spclInstrDocList = PhraseUtility.createPhraseList(
                                    TradePortalConstants.PHRASE_CAT_SPCL_INST, 
                                    userSession, formMgr, resMgr);
     phraseLists.addComponent("/SpclInstr", spclInstrDocList);
  }

  formMgr.storeInDocCache("PhraseLists", phraseLists);

   // Get Issuer Reference number from original Transaction terms
   String serverLocation = JPylonProperties.getInstance().getString("serverLocation");
   ClientServerDataBridge csdb = resMgr.getCSDB();
   String oldTransactionOid = instrument.getAttribute("original_transaction_oid");
   long longOldTransactionOid = Long.parseLong(oldTransactionOid.trim());
   Transaction transOriginal = (Transaction)EJBObjectFactory.createClientEJB(serverLocation,
						   "Transaction", longOldTransactionOid, csdb);
	
   Terms termsOriginal = (Terms) transOriginal.getComponentHandle("CustomerEnteredTerms");
				
   String issuerRefNum =  termsOriginal.getAttribute("issuer_ref_num");

   //rbhaduri - 12th Sep 06 - IR ALUG062061742 - get the applicant/opening bank ref number as well
   String applicantRefNum = termsOriginal.getAttribute("reference_number");
   
   transOriginal.remove();
   // finished getting Issuer Referencen number from original transaction terms

 
  ////////////////////////////////////////////////////////////////////////
  // Retrieve the latest information of applicatnt name and expiry date.  
  // They are displayed in general section.
  ////////////////////////////////////////////////////////////////////////

  // Retrieve the latest information of expiry date from instrument.
  currentExpiryDate = instrument.getAttribute("copy_of_expiry_date");

  // Retrieve the applicant name from the bank release terms of the latest issuance or amendment transaction.
  //    (The latest transaction is the one that has the latest transaction status date and in case several transaction
  //     have the same transaction status date, the largest transaction_oid.)
  String instrument_oid = instrument.getAttribute("instrument_oid");
  StringBuffer issueTransactionSQL = new StringBuffer();
  issueTransactionSQL.append("select p.name")
                     .append(" from terms_party p, terms t, transaction tr")
                     .append(" where tr.transaction_oid =")
                     .append("       (select max(transaction_oid)")
                     .append("          from transaction")
                     .append("         where transaction_status_date = (select max(transaction_status_date)")
                     .append("                                            from transaction")
                     .append("                                           where p_instrument_oid = ?")
                     .append("                                             and (transaction_type_code = ? or transaction_type_code = ? or transaction_type_code = ?) ")
                     .append("                                             and transaction_status = ?)")
                     .append("           and p_instrument_oid = ?")
                     .append("           and (transaction_type_code = ? or transaction_type_code = ? or transaction_type_code = ?)")
                     .append("           and transaction_status = ?)")
                     .append(" and tr.c_bank_release_terms_oid = t.terms_oid")
                     .append(" and (p.terms_party_oid = t.c_first_terms_party_oid")
                     .append("      or p.terms_party_oid = t.c_second_terms_party_oid")
                     .append("      or p.terms_party_oid = t.c_third_terms_party_oid")
                     .append("      or p.terms_party_oid = t.c_fourth_terms_party_oid")
                     .append("      or p.terms_party_oid = t.c_fifth_terms_party_oid)")
                     .append(" and p.terms_party_type = ?");
  //jgadela  R90 IR T36000026319 - SQL FIX
  Object[] sqlParamsTranR = new Object[11];
  sqlParamsTranR[0] =  instrument_oid;
  sqlParamsTranR[1] =  TransactionType.ISSUE;
  sqlParamsTranR[2] =  TransactionType.AMEND;
  sqlParamsTranR[3] =  TransactionType.CHANGE;
  sqlParamsTranR[4] =  TradePortalConstants.TRANS_STATUS_PROCESSED_BY_BANK;
  sqlParamsTranR[5] =  instrument_oid;
  sqlParamsTranR[6] =  TransactionType.ISSUE;
  sqlParamsTranR[7] =  TransactionType.AMEND;
  sqlParamsTranR[8] =  TransactionType.CHANGE;
  sqlParamsTranR[9] =  TradePortalConstants.TRANS_STATUS_PROCESSED_BY_BANK;
  sqlParamsTranR[10] =  TradePortalConstants.APPLICANT;
  
  DocumentHandler issueTransactionResult = DatabaseQueryBean.getXmlResultSet(issueTransactionSQL.toString(), false, sqlParamsTranR);
  if (issueTransactionResult!=null) {
     applicantName = issueTransactionResult.getAttribute("/ResultSetRecord(0)/NAME");
	 
  }
  
  
  //PHILC out.println("Applicant Name = " + applicantName + " [" + issueTransactionSQL.toString() + "]");
%>




<%
				  String pageTitleKey;
				  if ( TradePortalConstants.INDICATOR_YES.equals(newTransaction) && !(TransactionType.AMEND).equals(transaction.getAttribute("transaction_type_code"))) {
					    pageTitleKey = "SecondaryNavigation.NewInstruments";
					    userSession.setCurrentPrimaryNavigation("NavigationBar.NewInstruments");
					  } else {
					    pageTitleKey = "SecondaryNavigation.Instruments";
					    userSession.setCurrentPrimaryNavigation("NavigationBar.Transactions");
					  }
				  String item1Key = resMgr.getText("SecondaryNavigation.Instruments.RequestToAdvise",TradePortalConstants.TEXT_BUNDLE) + ": "+resMgr.getText("common.amendButton",TradePortalConstants.TEXT_BUNDLE);
				  String helpUrl = "customer/amend_request_to_advise.htm";
				%>
				

<%
  // Some of the retrieval logic above may have set a focus field.  Otherwise,
  // we'll use the initial value for focus.
  String onLoad = "";

  // Auto save the form when time-out if not readonly.  
  // (Header.jsp will check for auto-save setting of the corporate org).
  String autoSaveFlag = isReadOnly ? TradePortalConstants.INDICATOR_NO : TradePortalConstants.INDICATOR_YES;
  String templateFlag = isTemplate ? TradePortalConstants.INDICATOR_YES : TradePortalConstants.INDICATOR_NO;

  //cquinton 11/19/2012 pass info for recent instruments into header
  String newRecentTransactionOid = transaction.getAttribute("transaction_oid");
  String newRecentInstrumentId = instrument.getAttribute("complete_instrument_id");
  String newRecentTransactionType = transaction.getAttribute("transaction_type_code");
  String newRecentTransactionStatus = transaction.getAttribute("transaction_status");
%>

<jsp:include page="/common/Header.jsp">
  <jsp:param name="autoSaveFlag"           value="<%=autoSaveFlag%>" />
  <jsp:param name="templateFlag"           value="<%=templateFlag%>" />
  <jsp:param name="newRecentTransactionOid" value="<%=newRecentTransactionOid%>" />
  <jsp:param name="newRecentInstrumentId"  value="<%=newRecentInstrumentId%>" />
  <jsp:param name="newRecentTransactionType" value="<%=newRecentTransactionType%>" />
  <jsp:param name="newRecentTransactionStatus" value="<%=newRecentTransactionStatus%>" />
</jsp:include>

<div class="pageMain">
	<div class="pageContent">

<%-- ********************* HTML for page begins here ********************* --%>

<% //cr498 begin
   //Include ReAuthentication frag in case re-authentication is required for
   //authorization of transactions for this client
   String certAuthURL = "";
   Cache reCertCache = (Cache)TPCacheManager.getInstance().getCache(TradePortalConstants.CLIENT_BANK_CACHE);
   DocumentHandler CBCResult = (DocumentHandler)reCertCache.get(userSession.getClientBankOid());	
   String requireTranAuth = CBCResult.getAttribute("/ResultSetRecord(0)/REQUIRE_TRAN_AUTH");
   boolean requireAuth = InstrumentAuthentication.requireTransactionAuthentication(
       requireTranAuth,InstrumentAuthentication.TRAN_AUTH__RQA_AMD);
   if (requireAuth) {
%>   
      <%@ include file="/logon/fragments/openReauthenticationWindow.frag" %>
<%
   }
   //cr498 end
%>
<form id="TransactionRQA" name="TransactionRQA"	method="post" data-dojo-type="dijit.form.Form" action="<%=formMgr.getSubmitAction(response)%>">
	  <input type=hidden value="" name=buttonName>

<% //cr498 begin
  if (requireAuth) {
%> 

  <input type=hidden name="reCertification" value="<%=TradePortalConstants.INDICATOR_NO%>">
  <input type=hidden name="reCertOK">
  <input type=hidden name="logonResponse">
  <input type=hidden name="logonCertificate">

<%
  } //cr498 end
%> 

<%
  if(transaction.getAttribute("transaction_status").equals(TradePortalConstants.TRANS_STATUS_PROCESSED_BY_BANK))
	  isProcessedByBank = true;

  // Store values such as the userid, security rights, and org in a
  // secure hashtable for the form.  Also store instrument and transaction
  // data that must be secured.
  Hashtable secureParms = new Hashtable();
  secureParms.put("login_oid", userSession.getUserOid());
  secureParms.put("owner_org_oid", userSession.getOwnerOrgOid());
  secureParms.put("login_rights", loginRights);

  secureParms.put("instrument_oid", instrument.getAttribute("instrument_oid"));
  secureParms.put("instrument_type_code", instrumentType);
  secureParms.put("instrument_status", instrumentStatus);

  secureParms.put("transaction_oid", transaction.getAttribute("transaction_oid"));
  secureParms.put("transaction_type_code", transactionType);
  secureParms.put("transaction_status", transactionStatus);

  secureParms.put("shipment_oid", terms.getFirstShipment().getAttribute("shipment_oid"));
  secureParms.put("shipmentOid", terms.getFirstShipment().getAttribute("shipment_oid"));
  
  secureParms.put("transaction_instrument_info", 
  	transaction.getAttribute("transaction_oid") + "/" + 
	instrument.getAttribute("instrument_oid") + "/" + 
	transactionType);

  // If the terms record doesn't exist, set its oid to 0.
  String termsOid = terms.getAttribute("terms_oid");
  if (termsOid == null) termsOid = "0";
  secureParms.put("terms_oid", termsOid);
  secureParms.put("TransactionCurrency", currencyCode);
  //IR - PAUK042248441 [BEGIN]
  //secureParms.put("ApplRefNum", terms.getAttribute("reference_number"));
  secureParms.put("ApplRefNum", StringFunction.xssHtmlToChars(terms.getAttribute("reference_number")));
  //IR - PAUK042248441 [END]
%>

  
				<jsp:include page="/common/PageHeader.jsp">
				   <jsp:param name="titleKey" value="<%=pageTitleKey%>" />
				   <jsp:param name="item1Key" value="<%=item1Key %>" />
				   <jsp:param name="helpUrl"  value="<%=helpUrl%>" />
				</jsp:include>
				
				<jsp:include page="/common/TransactionSubHeader.jsp" />
	
    
				<%-- error section goes above form content --%>
		        <div class="formArea">
		          <jsp:include page="/common/ErrorSection.jsp" />
				  <% // [BEGIN] IR-YVUH032343792 - jkok %>
					  <%@ include file="fragments/Instruments-AttachDocuments.frag" %>
  				  <% // [END] IR-YVUH032343792 - jkok %>

				  
				  <div class="formContent">
				  <%	if (rejectionIndicator.equals(TradePortalConstants.INDICATOR_YES)
					    || rejectionIndicator.equals(TradePortalConstants.INDICATOR_X))
					  {
					%>
						<%=widgetFactory.createSectionHeader("0", "Rejection Reason") %>
					     <%@ include file="fragments/Transaction-RejectionReason.frag" %>
					     </div>
					<%
					  }
					%>
<% //CR 821 Added Repair Reason Section 
StringBuffer repairReasonWhereClause = new StringBuffer();
int  repairReasonCount = 0;
	
	/*Get all repair reason's count from transaction history table*/
	repairReasonWhereClause.append("p_transaction_oid = ?");
	repairReasonWhereClause.append(" and REPAIR_REASON is not null order by ACTION_DATETIME");
	//jgadela  R90 IR T36000026319 - SQL FIX
	Object[] sqlParamm = new Object[1];
	sqlParamm[0] =  transaction.getAttribute("transaction_oid");
	Debug.debug("repairReasonWhereCondition query : " + repairReasonWhereClause);
	repairReasonCount = DatabaseQueryBean.getCount("A_USER_OID", "transaction_history", repairReasonWhereClause.toString(), true, sqlParamm);
			
if ( !(TradePortalConstants.TRANS_STATUS_PROCESSED_BY_BANK.equalsIgnoreCase(transactionStatus)) && repairReasonCount>0  ){%>
	<%=widgetFactory.createSectionHeader("", "TransactionHistory.RepairReason") %>
		<%@ include file="fragments/Transaction-RepairReason.frag" %>
	</div>
<%} %> 				
				  	<%@ include file="fragments/Transaction-RQA-AMD-html.frag" %>
				  	<% if (TradePortalConstants.TRANS_STATUS_PROCESSED_BY_BANK.equalsIgnoreCase(transactionStatus) && repairReasonCount>0  ){%>
						<%=widgetFactory.createSectionHeader("5", "TransactionHistory.RepairReason", null, true) %>
						<%@ include file="fragments/Transaction-RepairReason.frag" %>
						</div><%-- Repair Reason Section Ends Here --%>
					<%} %> 
				  </div><%--formContent--%>
				</div><%--formArea--%>
				
				<div class="formSidebar" data-dojo-type="widget.FormSidebar" data-dojo-props="form: 'TransactionRQA'">							
						  <jsp:include page="/common/Sidebar.jsp">
								<jsp:param name="links" value="<%=links%>" />
								<jsp:param name="isReadOnly" value="<%=isReadOnly%>" />
								<jsp:param name="certAuthURL" value="<%=certAuthURL%>" />
								<jsp:param name="buttonPressed" value="<%=buttonClicked%>" />
								<jsp:param name="error" value="<%= error%>" />
								<jsp:param name="formName" value="0" />
								<jsp:param name="isTemplate" value="<%=isTemplate%>"/>
								 <jsp:param name="showLinks" value="true" />  
								 <jsp:param name="isNewTransaction" value="<%=newTransaction%>"/>
							</jsp:include>
				</div> <%--closes sidebar area--%>	
  <%@ include file="fragments/PhraseLookupPrep.frag" %>

<%= formMgr.getFormInstanceAsInputField("Transaction-RQAForm", secureParms) %>

</form>

</div>
	</div>

<%-- KMehta on 12 May 2014 @ Rel 90 for IR T36000025449 Start --%>
<jsp:include page="/common/Footer.jsp">
   <jsp:param name="autoSaveFlag"           value="<%=autoSaveFlag%>" />
   <jsp:param name="templateFlag"           value="<%=templateFlag%>" />
   <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>
<%-- KMehta on 12 May 2014 @ Rel 90 for IR T36000025449 End --%>
<jsp:include page="/common/SidebarFooter.jsp"/>

<script type="text/javascript">

  <%-- cquinton 3/3/2013 add local var --%>
  var local = {};

<%
  if (!isReadOnly && InstrumentServices.isNotBlank(focusField)) {
%>
  require(["dijit/registry", "dojo/ready"],
    function(registry, ready ) {
      ready(function() {
        var focusFieldId = '<%= focusField %>';
        if ( focusFieldId ) {
          var focusField = registry.byId(focusFieldId);
          focusField.focus();
        }
       <%--  registry.byId("TransactionAmount2").attr('disabled', 'true'); --%>
      });
  });
  function setNewAmtTol() {
		dijit.getEnclosingWidget(document.forms[0].newAmtTol).set('checked',true);

	}
  function setAmtTolExceed(){
		dijit.getEnclosingWidget(document.forms[0].amtTolExceed).set('checked',true);
	}

  var xhReq;
  function setNewAmount(){
	  require(["dijit/registry"],
			    function(registry ) {
		  			var existingLCAmount = <%=originalAmount%>;
	  				var ccyCode = '<%=currencyCode%>';
	  				var flag;
	  				if(registry.byId("Increase").checked){
	  					flag='Increase';
	  				}else{
	  					flag='Decrease';
	  				}
	  				var incDecValue = registry.byId("TransactionAmount").value;
	  				var newLCAmount = 0;	  				
	  				
		  				if(flag == 'Increase'){
		  					newLCAmount = Number(existingLCAmount) + Number(incDecValue);
		  				}else if(flag == 'Decrease'){
		  					newLCAmount = Number(existingLCAmount) - Number(incDecValue);
		  				}		  				
		  				if(isNaN(newLCAmount)){
		  					newLCAmount = existingLCAmount;
		  				}
	  				if (!xhReq)
	  					xhReq = getXmlHttpRequest();
	  				var req = "Amount=" + newLCAmount + "&currencyCode="+ccyCode;
	  				xhReq.open("GET", '/portal/transactions/AmountFormatter.jsp?'+req, true);
	  				xhReq.onreadystatechange = updateAmount;
	  				xhReq.send(null);
	  			  });}

  function getXmlHttpRequest()
  {
    var xmlHttpObj;
    if (window.XMLHttpRequest) {
      xmlHttpObj = new XMLHttpRequest();
    }
    else {
      try {
        xmlHttpObj = new ActiveXObject("Msxml2.XMLHTTP");
      } catch (e) {
        try {
          xmlHttpObj = new ActiveXObject("Microsoft.XMLHTTP");
        } catch (e) {
          xmlHttpObj = false;
        }
      }
    }
    console.log(xmlHttpObj);
    return xmlHttpObj;
  }
  
	  			function updateAmount(){
	  				require(["dijit/registry","dojo/dom","dojo/domReady!"], function(registry,dom){
	  				 if (xhReq.readyState == 4 && xhReq.status == 200) {	 
	  					var ccyCode = '<%=currencyCode%>';
	  					 var formattedAmt = ccyCode + " " + xhReq.responseText;	  					
	  					dom.byId("TransactionAmount2").innerHTML = formattedAmt;
	  			    	<%-- registry.byId("TransactionAmount2").set('value',xhReq.responseText); --%>
	  			      } 
	  				});
	  			  }
  
  <%-- cquinton 3/2/2013 local phrase lookup --%>
  require(["t360/phraseLookup"],
          function( phraseLookup ) {
    local.phraseLookupAppend = function(phraseSelect, textArea, textAreaMaxLength ) {
      phraseLookup.appendPhrase( phraseSelect, textArea, textAreaMaxLength, "_errorSection" );
    };
  });

<%
  }
%>
</script>

</body>
</html>

<%
   // Finally, reset the cached document to eliminate carryover of
   // information to the next visit of this page.
   formMgr.storeInDocCache("default.doc", new DocumentHandler());
%>
