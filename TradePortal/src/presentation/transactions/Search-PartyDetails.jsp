<%@ page import="com.ams.tradeportal.busobj.webbean.*,com.ams.tradeportal.common.*, com.amsinc.ecsg.html.*,
                 com.ams.tradeportal.html.*,java.util.*,com.amsinc.ecsg.util.*, com.amsinc.ecsg.frame.*,
                 com.ams.tradeportal.busobj.util.*" %>
<%

	String partyoid = request.getParameter("partyoid");
	StringBuffer sqlBuf = null;
	StringBuffer desiBank = null;
	String jsonFmt = "";
	
	if(partyoid!=null){	
		
	    sqlBuf = new StringBuffer();
	    
		sqlBuf.append("select name as SearchPartyName, address_line_1 as AddressLine1,address_line_2 as AddressLine2,");
		sqlBuf.append("address_state_province as AddressLine3,address_postal_code as AddressLine4, address_city as City, address_country as Country,");
		sqlBuf.append("branch_code as BranchCode, party_oid as PartyOID, otl_customer_id as OTLCustomerId, ");
		sqlBuf.append("user_defined_field_1 as UserDefinedField1, user_defined_field_2 as UserDefinedField2, user_defined_field_3 as UserDefinedField3, user_defined_field_4 as UserDefinedField4 ");
		sqlBuf.append("from party where party_oid=?");
		//jgadela  R90 IR T36000026319 - SQL FIX
		 Object sqlParamsSrh[] = new Object[1];
		 sqlParamsSrh[0] = partyoid;
		
		DocumentHandler resultDoc = DatabaseQueryBean.getXmlResultSet(sqlBuf.toString(), false, sqlParamsSrh);
		//out.println("resultDoc  : "+resultDoc);
		if(resultDoc != null){
			desiBank = new StringBuffer();
			desiBank.append("{");
			if(null != resultDoc.getAttribute("/ResultSetRecord(0)/SEARCHPARTYNAME")){
				desiBank.append("\"name\":\""+resultDoc.getAttribute("/ResultSetRecord(0)/SEARCHPARTYNAME")+"\",");
			}
			if(null != resultDoc.getAttribute("/ResultSetRecord(0)/ADDRESSLINE1")){
				desiBank.append("\"address1\":\""+resultDoc.getAttribute("/ResultSetRecord(0)/ADDRESSLINE1")+"\",");
			}
			if(null != resultDoc.getAttribute("/ResultSetRecord(0)/ADDRESSLINE2")){
				desiBank.append("\"address2\":\""+resultDoc.getAttribute("/ResultSetRecord(0)/ADDRESSLINE2")+"\",");
			}
			if(null != resultDoc.getAttribute("/ResultSetRecord(0)/ADDRESSLINE4")){
				desiBank.append("\"postalcode\":\""+resultDoc.getAttribute("/ResultSetRecord(0)/ADDRESSLINE4")+"\",");
			}
			if(null != resultDoc.getAttribute("/ResultSetRecord(0)/CITY")){
				desiBank.append("\"city\":\""+resultDoc.getAttribute("/ResultSetRecord(0)/CITY")+"\",");
			}
			if(null != resultDoc.getAttribute("/ResultSetRecord(0)/ADDRESSLINE3")){
				desiBank.append("\"address3\":\""+resultDoc.getAttribute("/ResultSetRecord(0)/ADDRESSLINE3")+"\",");
			}
			if(null != resultDoc.getAttribute("/ResultSetRecord(0)/COUNTRY")){
				desiBank.append("\"countrycode\":\""+resultDoc.getAttribute("/ResultSetRecord(0)/COUNTRY")+"\",");
			}
			if(null != resultDoc.getAttribute("/ResultSetRecord(0)/COUNTRY")){
				desiBank.append("\"country\":\""+ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.COUNTRY, resultDoc.getAttribute("/ResultSetRecord(0)/COUNTRY"))+"\",");
			}
			if(null != resultDoc.getAttribute("/ResultSetRecord(0)/BRANCHCODE")){
				desiBank.append("\"branchcode\":\""+resultDoc.getAttribute("/ResultSetRecord(0)/BRANCHCODE")+"\",");
			}
			if(null != resultDoc.getAttribute("/ResultSetRecord(0)/OTLCUSTOMERID")){
				desiBank.append("\"otlcustomerid\":\""+resultDoc.getAttribute("/ResultSetRecord(0)/OTLCUSTOMERID")+"\",");
			}	
			//Rel9.5 CR1132 - populate userdefined data from party
			if(null != resultDoc.getAttribute("/ResultSetRecord(0)/USERDEFINEDFIELD1")){
				desiBank.append("\"userdefinedfield1\":\""+resultDoc.getAttribute("/ResultSetRecord(0)/USERDEFINEDFIELD1")+"\",");
			}
			if(null != resultDoc.getAttribute("/ResultSetRecord(0)/USERDEFINEDFIELD2")){
				desiBank.append("\"userdefinedfield2\":\""+resultDoc.getAttribute("/ResultSetRecord(0)/USERDEFINEDFIELD2")+"\",");
			}
			if(null != resultDoc.getAttribute("/ResultSetRecord(0)/USERDEFINEDFIELD3")){
				desiBank.append("\"userdefinedfield3\":\""+resultDoc.getAttribute("/ResultSetRecord(0)/USERDEFINEDFIELD3")+"\",");
			}
			if(null != resultDoc.getAttribute("/ResultSetRecord(0)/USERDEFINEDFIELD4")){
				desiBank.append("\"userdefinedfield4\":\""+resultDoc.getAttribute("/ResultSetRecord(0)/USERDEFINEDFIELD4")+"\"");
			}
			if(desiBank.substring(desiBank.length()-1).indexOf(",") != -1){
				jsonFmt = desiBank.substring(0, desiBank.length()-1);
			}else{
				jsonFmt = desiBank.substring(0, desiBank.length());
			}
			jsonFmt = jsonFmt+"}";
			
		}
		
	}
	
	response.setContentType("application/json");
	//out.println(desiBank.toString()); 
	if(null != desiBank){
		out.print(jsonFmt);
	}else{
		out.print("");
	}
	out.flush();
%>
