<%--
*******************************************************************************
				Part Navigation Bar.jsp

  Description:  
    This includable page creates HTML that displays a set of tabs that support 
  breaking up of larger pages into smaller, more managable "tabbed" pages.
  Up to 4 tabs may be defined.

  Each "tab" link is actually a submit action that sets the hidden button
  pressed field with the name of the tab (or part) selected.

  This jsp needs the following parameters defined:

  multiPartMode - if true, user wants to view page in multi-part mode (required)
  whichPart     - the part that should be displayed as currently selected (required)
  isHeader      - indicates whether this is the header's part navigation bar or 
                  the footer's
  readOnly      - indicates if the page is in read only mode
  part1TextKey  - the text resource key for the "Part 1" link label. (optional)
  part2TextKey  - the text resource key for the "Part 2" link label. (optional)
  part3TextKey  - the text resource key for the "Part 3" link label. (optional)
  part4TextKey  - the text resource key for the "Part 4" link label. (optional)
  extraTags     - Any extra tags that should be added to the tab's link that
                  are sent when the tab is clicked.  Optional.  If included, 
                  must contain %26 before the start of each parameter 
                  (e.g., %26isTransTermsFlag=false).

*******************************************************************************
--%>

<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.html.*,java.math.*" %>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session" />

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

  <%
   // The purpose of this JSP is to create the HTML for the area of the page
   // that the users use to navigate between the different "parts" of a transaction
   // page

   // The following parameters are read by this JSP:
   //      multiPartMode - if true, user wants to view transactions in multi-part mode (required)
   //      whichPart     - the part that should be displayed as currently selected (required)
   //      isHeader      - indicates whether this is the header's part navigation bar or the footer's
   //      readOnly      - indicates if the page is in read only mode
   //      part1TextKey  - the text resource key for the "Part 1" link label. (optional)
   //      part2TextKey  - the text resource key for the "Part 2" link label. (optional)
   //      part3TextKey  - the text resource key for the "Part 3" link label. (optional)
   //      part4TextKey  - the text resource key for the "Part 4" link label. (optional)

   // If we're not in multi-part mode, there is nothing to do
   if(request.getParameter("multiPartMode").equals("false"))
    {
     return;
    }

   boolean isExportCollection 	= false;
   boolean hasPDFLinks 		= false;
   
   // Determine which part should be highlighted as selected
   // This will be a string starting with the word 'Part' (of course, there are constants)
   String whichPart = request.getParameter("whichPart");

   // Get the text resource keys for the link labels from the parameters passed in
   // Inclusion of these parameters indicates whether or not the part link
   // should be displayed
   String part1TextResourceKey = request.getParameter("part1TextKey");
   String part2TextResourceKey = request.getParameter("part2TextKey");
   String part3TextResourceKey = request.getParameter("part3TextKey");
   String part4TextResourceKey = request.getParameter("part4TextKey");

   String extraTags 	= request.getParameter("extraTags");
   boolean isHeader     = request.getParameter("isHeader").equals("true");

   String onTop 	= isHeader ? "true": "false";

   if( InstrumentServices.isNotBlank( request.getParameter("exportColFlag") ) &&
       request.getParameter("exportColFlag").equals("true") )
   {	
	isExportCollection = true;
   }

   if( InstrumentServices.isNotBlank( request.getParameter("hasPDFLinks") ) &&
       request.getParameter("hasPDFLinks").equals("true") )
   {
	hasPDFLinks = true;
   }

   // The number of parts must be displayed
   // Count up the number of text resource keys passed in
   int numberOfParts = 0;
   
   if(part1TextResourceKey != null)
    {
	 numberOfParts++;
    }
   if(part2TextResourceKey != null)
    {
	 numberOfParts++;
    }
   if(part3TextResourceKey != null)
    {
	 numberOfParts++;
    }
   if(part4TextResourceKey != null)
    {
	 numberOfParts++;
    }

   if (isHeader)
     {

      if( !isExportCollection || ( isExportCollection && !hasPDFLinks ) ||
	 ( isExportCollection && hasPDFLinks && whichPart.equals(TradePortalConstants.PART_ONE)) )
	{
    %>
	  <table width="100%" border="0" cellspacing="0" cellpadding="0">
	    <tr> 
	      <td class=ListText width="20" nowrap>&nbsp;</td>
	      <td class=ListText width="100%" wrap align="left" valign="middle"> 

<%
	if( whichPart.equals(TradePortalConstants.PART_ONE) ) {
%>
               <table border="0" cellspacing="0" cellpadding="0">
                 <tr>
                    <td><img src="/portal/images/Blank_4x4.gif" width="4" height="4"></td>
                 </tr>
                 <tr>  
                    <td class=ListText>               
                       <%=resMgr.getText("transaction.MultiPartNavBar1", 
                                      TradePortalConstants.TEXT_BUNDLE) %>
		       <%=numberOfParts%>
                       <%=resMgr.getText("transaction.MultiPartNavBar2", 
                                      TradePortalConstants.TEXT_BUNDLE) %>
                    </td>
                 </tr>
                 <tr>
                    <td><img src="/portal/images/Blank_4x4.gif" width="4" height="4"></td>
                 </tr>
               </table>
<%	}else{
%>              &nbsp;
<%	}      
%>	      </td>
	      <td class=ListText width="100%" height="17">&nbsp;</td>
	    </tr>
	  </table>
    <%
	}
     }
    else
     {
        // "Footer" tabs get a separator bar
%>
       <table width=100% border=0 cellspacing=0 cellpadding=0 class=BankColor>
         <tr>
           <td class=BankColor height=10><p class=ListText>&nbsp;</p></td>
         </tr>
       </table>
<%
     }
    %>

  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
    <td class=ListText width=20 nowrap>&nbsp;</td>
    <%
     // Display each of the part links,
     // based on whether the text resource key was passed in or not.

     String TRUE = "true";
     String FALSE = "false";

     String isReadOnly = request.getParameter("readOnly");
 
     if(part1TextResourceKey != null)
      {
     %>
		<jsp:include page="/common/PartNavigationLink.jsp">
		   <jsp:param name="selected"        value="<%= (whichPart.equals(TradePortalConstants.PART_ONE)) ? TRUE : FALSE %>" />
		   <jsp:param name="textResourceKey" value="<%= part1TextResourceKey %>" />
		   <jsp:param name="partName"        value="<%= TradePortalConstants.PART_ONE %>" />
		   <jsp:param name="readOnly"		 value="<%= isReadOnly %>" />
		   <jsp:param name="extraTags"       value="<%= extraTags %>" />
	      	   <jsp:param name="onTop"           value="<%=onTop%>" />
      	</jsp:include>
     <%
	}

     if(part2TextResourceKey != null) 
      {    
     %>
		<jsp:include page="/common/PartNavigationLink.jsp">
		   <jsp:param name="selected"        value="<%= (whichPart.equals(TradePortalConstants.PART_TWO)) ? TRUE : FALSE %>" />
		   <jsp:param name="textResourceKey" value="<%= part2TextResourceKey %>" />
		   <jsp:param name="partName"        value="<%= TradePortalConstants.PART_TWO %>" />
		   <jsp:param name="readOnly"		 value="<%= isReadOnly %>" />
		   <jsp:param name="extraTags"       value="<%= extraTags %>" />
	      	   <jsp:param name="onTop"           value="<%=onTop%>" />
      	</jsp:include>
     <%
	}

     if(part3TextResourceKey != null) 
      {    
     %>
		<jsp:include page="/common/PartNavigationLink.jsp">
		   <jsp:param name="selected"        value="<%= (whichPart.equals(TradePortalConstants.PART_THREE)) ? TRUE : FALSE %>" />
		   <jsp:param name="textResourceKey" value="<%= part3TextResourceKey %>" />
		   <jsp:param name="partName"        value="<%= TradePortalConstants.PART_THREE %>" />
		   <jsp:param name="readOnly"		 value="<%= isReadOnly %>" />
		   <jsp:param name="extraTags"       value="<%= extraTags %>" />
	      	   <jsp:param name="onTop"           value="<%=onTop%>" />
      	</jsp:include>
     <%
	}

     if(part4TextResourceKey != null) 
      {    
     %>
		<jsp:include page="/common/PartNavigationLink.jsp">
		   <jsp:param name="selected"        value="<%= (whichPart.equals(TradePortalConstants.PART_FOUR)) ? TRUE : FALSE %>" />
		   <jsp:param name="textResourceKey" value="<%= part4TextResourceKey %>" />
		   <jsp:param name="partName"        value="<%= TradePortalConstants.PART_FOUR %>" />
		   <jsp:param name="readOnly"		 value="<%= isReadOnly %>" />
		   <jsp:param name="extraTags"       value="<%= extraTags %>" />
	      	   <jsp:param name="onTop"           value="<%=onTop%>" />
      	</jsp:include>
     <%
	}
     %>
    <td class=ListText width=100%>&nbsp;</td>
    </tr>
  </table>
<%  
   if (!isHeader) {
%>
      <br>
<%
   }
%>
