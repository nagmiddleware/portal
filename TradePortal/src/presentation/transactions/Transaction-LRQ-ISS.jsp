
<%--
*******************************************************************************
                        Loan Request Page

  Description:    
    This is the main driver for the Loan Request page.  It handles 
  data retrieval of terms and terms parties (or retrieval from the input document)
  and creates the html page to display all the data for a Loan Request.
*******************************************************************************
--%>
      
<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,java.util.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.html.*,java.math.*,
                 com.amsinc.ecsg.util.DateTimeUtility, java.util.HashMap, java.util.Map" %>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager"
    scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" 
   scope="session">
</jsp:useBean>
   
<%-- ************** Data retrieval page setup begins here ****************  --%>

<%
  WidgetFactory widgetFactory = new WidgetFactory(resMgr, userSession);
  //cquinton 2/12/2013 add refdata manager for datepattern lookup
  ReferenceDataManager refData = ReferenceDataManager.getRefDataMgr();

  String defaultCurrencyVal = " ";
  String links = "";
  String links2 = "";
  String onLoad = "";
  String focusField = "ApplRefNum";   // Default focus field
  boolean focusSet = false;

  // Various oid and status info from transaction and instruments used in
  // several places.
  String instrumentOid = "";         
  String transactionOid;
  String instrumentType;
  String instrumentStatus;
  String transactionType; 
  String transactionStatus;
  String rejectionIndicator            = "";
  String rejectionReasonText	          = "";
  String PartySearchAddressTitle =resMgr.getTextEscapedJS("PartySearch.TabHeading",TradePortalConstants.TEXT_BUNDLE);
  String instrumentSearchDialogTitle=resMgr.getTextEscapedJS("InstSearch.HeadingGeneric",TradePortalConstants.TEXT_BUNDLE);
  String BankSearchAddressTitle =resMgr.getText("RefDataHome.BankBranchRules.Search",TradePortalConstants.TEXT_BUNDLE);
	String parentOrgID      = userSession.getOwnerOrgOid();
	String bogID            = userSession.getBogOid();
	String clientBankID     = userSession.getClientBankOid();
	String globalID         = userSession.getGlobalOrgOid();
 
  boolean getDataFromDoc;              // Indicates if data is retrieved from the
                                       // input doc cache or from the database
  boolean instrumentSelected = false;  // Indicates if an instrument was selected.

  DocumentHandler doc;
  String buttonPressed1 ="";
  Vector error = null;
  String newTransaction = null;
  boolean           corpOrgHasMultipleAddresses   = false;
  String            corpOrgOid                    = null;
  boolean  canDisplayPDFLinks   = false;

  String loginLocale = userSession.getUserLocale();
  String loginRights = userSession.getSecurityRights();

  /* KMehta IR-T36000024809 Rel 9300 on 14-May-2015 Add - Begin */
  DocumentHandler accountDoc = new DocumentHandler();  
  DocumentHandler currencyIDDoc = new DocumentHandler();
  DocumentHandler templateList = new DocumentHandler();
  DocumentHandler CBCResult = null;
  Cache reCertCache = null;
  boolean isBankUser = true;
  /* KMehta IR-T36000024809 Rel 9300 on 14-May-2015 Add - End */
  // Dates are stored in the database as one field but displayed on the
  // page as 3 fields.  These variables hold the pieces for each date.
  String loanTermsFixedMaturityDay = "";
  String loanTermsFixedMaturityMonth = "";
  String loanTermsFixedMaturityYear = "";
  
  String loanStartDay = "";
  String loanStartMonth = "";
  String loanStartYear = "";

  String fecMaturityDay = "";
  String fecMaturityMonth = "";
  String fecMaturityYear = "";
  
  String maturityFECMaturityDay = "";
  String maturityFECMaturityMonth = "";
  String maturityFECMaturityYear = "";
 
  // Variables used for dealing with a terms party's accounts.
  boolean showBenAccts = false;
  String benAcctList = "";
  String appAcctList = "";
  String acctNum = "";
  String acctCcy = "";

  //cquinton 2/12/2013 lookup locale specific datePattern description from refData manager
  String datePattern = userSession.getDatePattern();
  String datePatternDisplay = refData.getDescr("DATEPATTERN", datePattern, loginLocale);
  String dateWidgetOptions = "placeHolder:'" + datePatternDisplay + "'";

  /* KMehta Rel 8400 IR-T36000022478 on 11/11/2013 */
  String timeZone = userSession.getTimeZone();
  
  // Variables used for populating ref data dropdowns.
  String options;
  String defaultText;
    String bankChargesType ="";
  // Prase Selected Flag
  boolean phraseSelected = false;
  boolean areInvoicesAttached  = false;
  //debugging vars
  String newPartyType = "";
  String newPartyOid = "";
  String financingBackedByBuyerVal = "N";
  // These are the beans used on the page.
  TransactionWebBean transaction  = (TransactionWebBean)
                                      beanMgr.getBean("Transaction");
  InstrumentWebBean instrument    = (InstrumentWebBean)
                                      beanMgr.getBean("Instrument");
  TemplateWebBean template        = (TemplateWebBean)
                                      beanMgr.getBean("Template");
  TermsWebBean terms              = null;
  
  TermsPartyWebBean termsPartyApp = beanMgr.createBean(TermsPartyWebBean.class, "TermsParty");

  TermsPartyWebBean termsPartyBen = beanMgr.createBean(TermsPartyWebBean.class, "TermsParty");
							 
  TermsPartyWebBean termsPartyBbk = beanMgr.createBean(TermsPartyWebBean.class, "TermsParty");

  ShipmentTermsWebBean shipmentTerms = beanMgr.createBean(ShipmentTermsWebBean.class, "ShipmentTerms");
    InvoicePaymentInstructionsWebBean invoicePaymentInstructions = beanMgr.createBean(InvoicePaymentInstructionsWebBean.class, "InvoicePaymentInstructions");

  // Get the document from the cache.  We'll may use it to repopulate the 
  // screen if returning from another page, a save, validation, or any other
  // mediator called from this page.  Otherwise, we assume an instrument oid
  // and transaction oid was passed in.

  	doc = formMgr.getFromDocCache();
  	if (doc.getDocumentNode("/In/Update/ButtonPressed") != null) 
    {
	buttonPressed1 = doc.getAttribute("/In/Update/ButtonPressed"); 	
 	error = doc.getFragments("/Error/errorlist/error");
    }
  Debug.debug("doc from cache is " + doc.toString());

  if (doc.getDocumentNode("/Out/newTransaction")!=null) {
	    newTransaction = doc.getAttribute("/Out/newTransaction");
	    session.setAttribute("newTransaction", newTransaction);
	    System.out.println("found newTransaction = "+ newTransaction);
	  } 
	  else {
	    newTransaction = (String) session.getAttribute("newTransaction");
	    if ( newTransaction==null ) {
	      newTransaction = TradePortalConstants.INDICATOR_NO;
	    }
	    System.out.println("used newTransaction from session = " + newTransaction);
	  }

  //cquinton 1/18/2013 remove old close action behavior
	 	
/******************************************************************************
  We are either entering the page or returning from somewhere.  These are the 
  conditions for how to populate the web beans.  Data comes from either the
  database or the doc cache (/In section) with some variation.

  Mode           Condition                      Populate Beans From
  -------------  ----------------------------   --------------------------------
  Enter Page     no /In/Transaction in doc      Instrument and Template web
                                                beans already populated, get
                                                data for Terms and TermsParty
                                                web beans from database

  return from    /In/NewPartyFromSearchInfo/PartyOid   doc cache (/In); also use Party
  Party Search     exists                       SearchInfo to lookup, and 
                                                populate a specific TermsParty
                                                web bean

  return from    /In/AddressSearchInfo/Address  doc cache (/In); use /In/Address
  Address Search   SearchPartyType exists       SearchInfo to populate a specific
                                                TermsParty web bean

  return from    /Error/maxerrorseverity < 1    Same as Enter Page (data is
  Transaction                                   retrieved from database)
    mediator
    (no error)

  return from    /Error/maxerrorseverity > 0    doc cache (/In)
  Transaction    
    mediator
    (error)

 ------------------------------------------------------------------------------
  Clear          /ClearBeneficary exists        Populate as above, also clear 
  Beneficiary                                   beneficiary fields.

  Instrument     /In/InstrumentSearchInfo/      Populate as above, also set
  Search           InstrumentData exists        related instrument id field.
******************************************************************************/
   
  // Assume we get the data from the doc.
  getDataFromDoc = true;


  String maxError = doc.getAttribute("/Error/maxerrorseverity");
  if (maxError != null && maxError.compareTo(
              String.valueOf(ErrorManager.WARNING_ERROR_SEVERITY)) <= 0) {
     // No errors, so don't get the data from doc.
     getDataFromDoc = false;
  }
  //ir cnuk113043991 - check to see if transaction needs to be refreshed
  // if so, refresh it and do not get data from doc as it is wrong
  if ( "Y".equals(doc.getAttribute("/Out/Refresh/Transaction")) ) {
     transaction.getDataFromAppServer();
     getDataFromDoc = false;
  }
  if (doc.getDocumentNode("/In/NewPartyFromSearchInfo/Type") != null) {
     // We have returned from the party search page.  Get data from doc cache
     getDataFromDoc = true;
  }
  if (doc.getDocumentNode("/In/AddressSearchInfo/AddressSearchPartyType") != null) {
     // We have returned from the address search page.  Get data from doc cache
     getDataFromDoc = true;
  }

  if (doc.getDocumentNode("/In/Transaction") == null) {
     // No /In/Transaction means we've never looked up the data.
     Debug.debug("No /In/Transaction section - get data from database");
     getDataFromDoc = false;
  }
  
  DocumentHandler phraseLists = formMgr.getFromDocCache(
                                             TradePortalConstants.PHRASE_LIST_DOC);
  if (phraseLists == null) phraseLists = new DocumentHandler();

  DocumentHandler goodsDocList = phraseLists.getFragment("/Goods");
  if (goodsDocList == null) {
     goodsDocList = PhraseUtility.createPhraseList(
                                    TradePortalConstants.PHRASE_CAT_GOODS, 
                                    userSession, formMgr, resMgr);
     phraseLists.addComponent("/Goods", goodsDocList);
  }

  if (doc.getDocumentNode("/Out/PhraseLookupInfo") != null) {
     // A Looked up phrase exists.  Replace it in the /In document.
     Debug.debug("Found a looked-up phrase");
     getDataFromDoc = true;
     phraseSelected = true;

     String result = doc.getAttribute("/Out/PhraseLookupInfo/Result");
     if (result.equals(TradePortalConstants.INDICATOR_YES)) {
        // Take the looked up and appended phrase text from the /Out section
        // and copy it to the /In section.  This allows the beans to be 
        // properly populated.
        String xmlPath = doc.getAttribute("/In/PhraseLookupInfo/text_path");
        xmlPath = "/In" + xmlPath;

        doc.setAttribute(xmlPath, 
                      doc.getAttribute("/Out/PhraseLookupInfo/NewText"));

        // If we returned from the phrase lookup without errors, set the focus
        // to the correct field.  Otherwise, don't set the focus so the user can
        // see the error.
        if (maxError != null && maxError.compareTo(
              String.valueOf(ErrorManager.WARNING_ERROR_SEVERITY)) < 0) {
           focusField = doc.getAttribute("/In/PhraseLookupInfo/text_field_name");
           focusSet = true;
        }
     } else {
        // We do nothing because nothing was looked up.  We stil want to get
        // the data from the doc.
     }
  }


  if (getDataFromDoc) {
     Debug.debug("Populating beans from doc cache");

     // Populate the beans from the input doc.
     try {
        instrument.populateFromXmlDoc(doc.getComponent("/In"));
        transaction.populateFromXmlDoc(doc.getComponent("/In"));
        template.populateFromXmlDoc(doc.getComponent("/In"));

        terms = (TermsWebBean) beanMgr.getBean("Terms");
        terms.populateFromXmlDoc(doc, "/In");

        String termsPartyOid;

        termsPartyOid = terms.getAttribute("c_FirstTermsParty");
        if (termsPartyOid != null && !termsPartyOid.equals(""))
        {
           termsPartyBen.setAttribute("terms_party_oid", termsPartyOid);
           termsPartyBen.getDataFromAppServer();
        }

        termsPartyOid = terms.getAttribute("c_SecondTermsParty");
        if (termsPartyOid != null && !termsPartyOid.equals(""))
        {
           termsPartyApp.setAttribute("terms_party_oid", termsPartyOid);
           termsPartyApp.getDataFromAppServer();
        }

        termsPartyOid = terms.getAttribute("c_ThirdTermsParty");
        if (termsPartyOid != null && !termsPartyOid.equals(""))
        {
           termsPartyBbk.setAttribute("terms_party_oid", termsPartyOid);
           termsPartyBbk.getDataFromAppServer();
        }

        DocumentHandler termsPartyDoc;

        termsPartyDoc = doc.getFragment("/In/Terms/FirstTermsParty");
        termsPartyBen.loadTermsPartyFromDocTagsOnly(termsPartyDoc);

        termsPartyDoc = doc.getFragment("/In/Terms/SecondTermsParty");
        termsPartyApp.loadTermsPartyFromDocTagsOnly(termsPartyDoc);

        termsPartyDoc = doc.getFragment("/In/Terms/ThirdTermsParty");
        termsPartyBbk.loadTermsPartyFromDocTagsOnly(termsPartyDoc);

        // Some of the data for this transaction is stored as part of the shipment terms 
        terms.populateFirstShipmentFromXml(doc);

        loanStartDay = doc.getAttribute("/In/Terms/loan_start_day");
        loanStartMonth = doc.getAttribute("/In/Terms/loan_start_month");
        loanStartYear = doc.getAttribute("/In/Terms/loan_start_year");

        loanTermsFixedMaturityDay = doc.getAttribute("/In/Terms/loan_terms_fixed_maturity_day");
        loanTermsFixedMaturityMonth = doc.getAttribute("/In/Terms/loan_terms_fixed_maturity_month");
        loanTermsFixedMaturityYear = doc.getAttribute("/In/Terms/loan_terms_fixed_maturity_year");
		
        fecMaturityDay = doc.getAttribute("/In/Terms/fec_maturity_day");
        fecMaturityMonth = doc.getAttribute("/In/Terms/fec_maturity_month");
        fecMaturityYear = doc.getAttribute("/In/Terms/fec_maturity_year");

        maturityFECMaturityDay = doc.getAttribute("/In/Terms/maturity_fec_maturity_day");
        maturityFECMaturityMonth = doc.getAttribute("/In/Terms/maturity_fec_maturity_month");
        maturityFECMaturityYear = doc.getAttribute("/In/Terms/maturity_fec_maturity_year");

     } catch (Exception e) {
        out.println("Contact Administrator: "
              + "Unable to reload data after returning to page. "
              + "Error is " + e.toString());
     }
  } else {
     Debug.debug("populating beans from database");
     // We will perform a retrieval from the database.
     // Instrument and Transaction were already retrieved.  Get
     // the rest of the data

     terms = transaction.registerCustomerEnteredTerms();

     String termsPartyOid;

     termsPartyOid = terms.getAttribute("c_FirstTermsParty");
     if (termsPartyOid != null && !termsPartyOid.equals("")) {
        termsPartyBen.setAttribute("terms_party_oid", termsPartyOid);
        termsPartyBen.getDataFromAppServer();
     }

     termsPartyOid = terms.getAttribute("c_SecondTermsParty");
     if (termsPartyOid != null && !termsPartyOid.equals("")) {
        termsPartyApp.setAttribute("terms_party_oid", termsPartyOid);
        termsPartyApp.getDataFromAppServer();
     }
	 
     termsPartyOid = terms.getAttribute("c_ThirdTermsParty");
     if (termsPartyOid != null && !termsPartyOid.equals("")) {
        termsPartyBbk.setAttribute("terms_party_oid", termsPartyOid);
        termsPartyBbk.getDataFromAppServer();
     }

     // Some of the data for this transaction is stored as part of the shipment terms
     terms.populateFirstShipmentFromDatabase();
	 
     // Because date fields are represented as three fields on the page,
     // we need to parse out the date values into three fields.  Pre-mediator
     // logic puts these three fields back together for the terms bean.
     String date = "";
	 
	 date = terms.getAttribute("loan_start_date");
     loanStartDay = TPDateTimeUtility.parseDayFromDate(date);
     loanStartMonth = TPDateTimeUtility.parseMonthFromDate(date);
     loanStartYear = TPDateTimeUtility.parseYearFromDate(date);

     date = terms.getAttribute("loan_terms_fixed_maturity_dt");
     loanTermsFixedMaturityDay = TPDateTimeUtility.parseDayFromDate(date);
     loanTermsFixedMaturityMonth = TPDateTimeUtility.parseMonthFromDate(date);
     loanTermsFixedMaturityYear = TPDateTimeUtility.parseYearFromDate(date);
	 
	 date = terms.getAttribute("fec_maturity_date");
     fecMaturityDay = TPDateTimeUtility.parseDayFromDate(date);
     fecMaturityMonth = TPDateTimeUtility.parseMonthFromDate(date);
     fecMaturityYear = TPDateTimeUtility.parseYearFromDate(date);

     date = terms.getAttribute("maturity_fec_maturity_date");
     maturityFECMaturityDay = TPDateTimeUtility.parseDayFromDate(date);
     maturityFECMaturityMonth = TPDateTimeUtility.parseMonthFromDate(date);
     maturityFECMaturityYear = TPDateTimeUtility.parseYearFromDate(date);

  }

  String importIndicator = instrument.getAttribute("import_indicator");
  String bankOpOrgOid = instrument.getAttribute("op_bank_org_oid");
  String opOrgCountry = InvoiceUtility.fetchOpOrgCountryForOpOrg(bankOpOrgOid);
  opOrgCountry = StringFunction.isNotBlank(opOrgCountry) ? opOrgCountry : "";
  boolean isImport = TradePortalConstants.INDICATOR_YES.equals(importIndicator);
  boolean isExport = TradePortalConstants.INDICATOR_NO.equals(importIndicator);
  boolean isReceivables = TradePortalConstants.INDICATOR_X.equals(importIndicator);
  boolean isTradeLoan = TradePortalConstants.INDICATOR_T.equals(importIndicator);
  String tradeLoanType =   terms.getAttribute("finance_type");
  boolean isTradeReceivables = TradePortalConstants.TRADE_LOAN_REC.equals(terms.getAttribute("finance_type"));
  boolean isTradePayables = TradePortalConstants.TRADE_LOAN_PAY.equals(terms.getAttribute("finance_type"));
  boolean uploadIndicator = TradePortalConstants.INDICATOR_YES.equals(transaction.getAttribute("upload_indicator"));
  boolean isLoanTypeSet = (isImport || isExport || isReceivables || isTradeLoan);
  
  
  
  int               numberOfDomPmts       = 0;
  int 				numberOfInvoicesAttached = 0;
  Object[] sqlParams = new Object[1];
  sqlParams[0] =  transaction.getAttribute("transaction_oid");
  numberOfDomPmts = DatabaseQueryBean.getCount("inv_pay_inst_oid", "invoice_payment_instructions", "p_transaction_oid = ?", false, sqlParams);
  numberOfInvoicesAttached = DatabaseQueryBean.getCount("upload_invoice_oid", "invoices_summary_data", "a_transaction_oid = ?" , false, sqlParams);
  areInvoicesAttached = numberOfInvoicesAttached > 0;
  boolean areMultiBeneficiariesAdded = numberOfDomPmts > 0;
  String invClassification = null;
  String invPayMethod = null;
  if (areInvoicesAttached){
	  StringBuffer sql = new StringBuffer("SELECT DISTINCT INVOICE_CLASSIFICATION,PAY_METHOD FROM INVOICES_SUMMARY_DATA WHERE A_TRANSACTION_OID = ?") ;
	  DocumentHandler  invClassificationDoc = DatabaseQueryBean.getXmlResultSet(sql.toString(), false, sqlParams);
	  if (invClassificationDoc != null){
		  invClassification = invClassificationDoc.getAttribute("/ResultSetRecord(0)/INVOICE_CLASSIFICATION");
		  invPayMethod = invClassificationDoc.getAttribute("/ResultSetRecord(0)/PAY_METHOD");
	  }
	  
  }
  
  boolean isMultiBenInvoicesAttached = false;
  boolean isSingleBenInvoicesAttached = false;
  boolean isSingleBenInvoicesPaymentMthdBlank = false;
  
  if (areInvoicesAttached && isTradeLoan && 
			TradePortalConstants.INVOICE_TYPE_PAY_MGMT.equals(invClassification)){
	
	  int singleBeneCount = InvoiceUtility.isSingleBeneficairyInvoice(" A_TRANSACTION_OID = " +transaction.getAttribute("transaction_oid") +" AND PAY_METHOD IS NOT NULL ");  
	  if (singleBeneCount > 1){
		  isMultiBenInvoicesAttached = true;
		}else if (singleBeneCount == 1){
			isSingleBenInvoicesAttached = true;
		}
  }


  boolean isUpldInvsAttchdLoanType = uploadIndicator ||  areInvoicesAttached || areMultiBeneficiariesAdded;

  boolean defaultCheckBoxValue = false;
  
  // Don't format the amount if we are reading data from the doc
  String amount = terms.getAttribute("amount");
  String currency = terms.getAttribute("amount_currency_code");
  String displayAmount;
  
  String loanProceedsPmtAmount = terms.getAttribute("loan_proceeds_pmt_amount");
  String loanProceedsPmtCurrency = terms.getAttribute("loan_proceeds_pmt_curr");
  String displayLoanProceedsPmtAmount;
  
  if ((isTradeLoan || isReceivables || isExport) && !(maxError != null && maxError.compareTo(
          String.valueOf(ErrorManager.ERROR_SEVERITY)) == 0)){
	  loanProceedsPmtAmount = amount;
	  loanProceedsPmtCurrency = currency;
  }
  String settlementFECAmount = terms.getAttribute("fec_amount");
  String settlementFECCurrency = loanProceedsPmtCurrency;
  String displaySettlementFECAmount;

  String loanFECAmount = terms.getAttribute("maturity_fec_amount");
  String loanFECCurrency = currency;
  String displayLoanFECAmount;

  String displaySettlementFECRate;
  String displayMaturityFECRate;
  
  
  if(!getDataFromDoc) {
     displayAmount = TPCurrencyUtility.getDisplayAmount(amount, currency, loginLocale);
     displayLoanProceedsPmtAmount = TPCurrencyUtility.getDisplayAmount(loanProceedsPmtAmount, loanProceedsPmtCurrency, loginLocale);
     displaySettlementFECAmount = TPCurrencyUtility.getDisplayAmount(settlementFECAmount, settlementFECCurrency, loginLocale);
     displayLoanFECAmount = TPCurrencyUtility.getDisplayAmount(loanFECAmount, loanFECCurrency, loginLocale);
	 displaySettlementFECRate = TPCurrencyUtility.getDisplayAmount(terms.getAttribute("fec_rate"), "", loginLocale);
	 displayMaturityFECRate = TPCurrencyUtility.getDisplayAmount(terms.getAttribute("maturity_fec_rate"), "", loginLocale);
	 }
  else {
     displayAmount = amount;
     displayLoanProceedsPmtAmount = loanProceedsPmtAmount;
     displaySettlementFECAmount = settlementFECAmount;
     displayLoanFECAmount = loanFECAmount;
	 displaySettlementFECRate = terms.getAttribute("fec_rate");
	 displayMaturityFECRate = terms.getAttribute("maturity_fec_rate");
	 }
 
  String financeTypeText = terms.getAttribute("finance_type_text");
 
  String loanProceedsCreditType = terms.getAttribute("loan_proceeds_credit_type");
  String financeType = terms.getAttribute("finance_type");
  String proceedsBankToBook = terms.getAttribute("use_mkt_rate");
  String proceedsUseFEC = terms.getAttribute("use_fec");
  String proceedsUseOther = terms.getAttribute("use_other");
  String loanMaturityDebitType = terms.getAttribute("loan_maturity_debit_type");
  String maturityBankToBook = terms.getAttribute("maturity_use_mkt_rate");
  String maturityUseFEC = terms.getAttribute("maturity_use_fec");
  String maturityUseOther = terms.getAttribute("maturity_use_other");
  String financingBackedByBuyer = terms.getAttribute("financing_backed_by_buyer_ind");
  String searchInstrType = "";
  String clickTag1 = "";
  String clickTag2 = "";
  String clickTag3 = "";
  
  String			  interestToBePaid = terms.getAttribute("interest_to_be_paid");
  String			  loanTermsType = terms.getAttribute("loan_terms_type");

  /*************************************************
  * Load New Party
  **************************************************/

  if (doc.getDocumentNode("/In/NewPartyFromSearchInfo/Type") != null &&
      doc.getDocumentNode("/In/NewPartyFromSearchOutput/PartyOid") != null ) {
	
	  // We have returned from the PartyDetailNew page.  Based on the returned 
     // data, RELOAD one of the terms party beans with the new party

     String termsPartyType = doc.getAttribute("/In/NewPartyFromSearchInfo/Type");
     String partyOid = doc.getAttribute("/In/NewPartyFromSearchOutput/PartyOid");
     newPartyType = termsPartyType;
     newPartyOid = partyOid;

     Debug.debug("Returning from party search with " + termsPartyType 
           + "/" + partyOid);

     // Use a Party web bean to get the party data for the selected oid.
     PartyWebBean party = beanMgr.createBean(PartyWebBean.class,  "Party");
     party.setAttribute("party_oid", partyOid);
     party.getDataFromAppServer();

     DocumentHandler partyDoc = new DocumentHandler();
     party.populateXmlDoc(partyDoc);

     partyDoc = partyDoc.getFragment("/Party");

     // Based on the party type being returned (which we previously set)
     // reload one of the terms party web beans with the data from the
     // doc.
     if (termsPartyType.equals(TradePortalConstants.BENEFICIARY)) {
        // use the "FromDocTagsOnly" so we don't overwrite the acct_currency
        termsPartyBen.loadTermsPartyFromDocTagsOnly(partyDoc);

        // For a beneficary selection, the beneficiary bank should be populated
        // if there is a designated bank.
        //cquinton 2/7/2013 pass designated party as part of partyDoc
        loadDesignatedParty(partyDoc, termsPartyBbk, beanMgr);
		
		// Now get the account data for this party and load it to the acct_choices field.  The
        // value is XML similar to a query result (i.e., /ResultSetRecord).
        termsPartyBen.loadAcctChoices(partyOid, formMgr.getServerLocation(), resMgr);

        // TLE - 11/20/06 - IR#ANUG110757179 - Add Begin
        if (isImport) {
           focusField = "BenNameImp";
        } else if (isExport) {
           focusField = "BenNameExp";
		} else if (isReceivables) {
		   focusField = "BenNameRec";
        } else if (isTradeLoan) {
            focusField = "BenNameTrd";
        }
        // TLE - 11/20/06 - IR#ANUG110757179 - Add End

        focusSet = true;		
		onLoad += "location.hash='#" + termsPartyType + "';";
     }
     if (termsPartyType.equals(TradePortalConstants.APPLICANT)) {
        termsPartyApp.loadTermsPartyFromDoc(partyDoc);
	//rbhaduri - 13th June 06 - IR SAUG051361872
	termsPartyApp.setAttribute("address_search_indicator", "N");

        focusField = "";
        focusSet = true;
        onLoad += "location.hash='#" + termsPartyType + "';";

        // Now get the account data for this party and load it to the acct_choices field.  The
        // value is XML similar to a query result (i.e., /ResultSetRecord).

        termsPartyApp.loadAcctChoices(partyOid, formMgr.getServerLocation(), resMgr);
		
     }
     if (termsPartyType.equals(TradePortalConstants.BENEFICIARY_BANK)) {
        termsPartyBbk.loadTermsPartyFromDoc(partyDoc);

        // TLE - 11/20/06 - IR#ANUG110757179 - Add Begin
         if (isImport) {
           focusField = "BbkNameImp";
        } else if (isExport) {
           focusField = "BbkNameExp";
		} else if (isReceivables) {
            focusField = "BbkNameRec";
        } else if (isTradeLoan) {
            focusField = "BbkNameTrd";
        }
        // TLE - 11/20/06 - IR#ANUG110757179 - Add End

        focusSet = true;
        onLoad += "location.hash='#" + termsPartyType + "';";
     }//Rpasupulati loading Account Choices IR T36000032852 start
  }else{
	 
	  String userCheck = null;
		
			userCheck = userSession.getUserOid();
		
			String corpOrgOid1 = userSession.getOwnerOrgOid();
	    String termsPartyBenOid = instrument.getAttribute("c_FirstTermsParty");
        
	      if (StringFunction.isNotBlank(termsPartyBenOid)){
	      termsPartyBen.loadAcctChoices(corpOrgOid1, formMgr.getServerLocation(), resMgr,null,userCheck);
	      }
	      String termsPartyAppOid = terms.getAttribute("c_SecondTermsParty");
	        
	      if (StringFunction.isNotBlank(termsPartyAppOid)){
	      termsPartyApp.loadAcctChoices(corpOrgOid1, formMgr.getServerLocation(), resMgr,null,null);
	      }
	      

  }//Rpasupulati loading Account Choices IR T36000032852 end


 

  if (doc.getDocumentNode("/In/ClearBeneficiary") != null) {
     // The user clicked the Clear Beneficiary button.  All the data has been reloaded
     // from the doc.  Now clear out the beneficiary fields.  (Unlike other "Clear"
     // actions handled by JavaScript, we also need to remove the account radio 
     // buttons from the HTML.)

     termsPartyBen.setAttribute("name", "");
     termsPartyBen.setAttribute("address_line_1", "");
     termsPartyBen.setAttribute("address_line_2", "");
     termsPartyBen.setAttribute("address_city", "");
     termsPartyBen.setAttribute("address_state_province", "");
     termsPartyBen.setAttribute("address_country", "");
     termsPartyBen.setAttribute("address_postal_code", "");
     termsPartyBen.setAttribute("OTL_customer_id", "");

     // By setting acct_choices to blank, the account radio buttons do not get
     // created.
     termsPartyBen.setAttribute("acct_choices", "");
     termsPartyBen.setAttribute("acct_num", "");
     termsPartyBen.setAttribute("acct_currency", "");

  }
  
  // P Cutrone - IR-KHUG121933479 - Instrument Search focus Field issue [BEGIN]
  String searchInstrumentType = "";
  String instrumentID = "";
  String linkedInstrumentID = "";
  String instrumentData = "";
  if (doc.getDocumentNode("/In/InstrumentSearchInfo") != null) {
     searchInstrumentType = doc.getAttribute("/In/InstrumentSearchInfo/SearchInstrumentType");
	 if (searchInstrumentType.equals(InstrumentType.EXPORT_COL)) { // EXPORT
		focusField = "LinkedCOLInstrumentID";
		focusSet = true;
		if (doc.getDocumentNode("/In/InstrumentSearchInfo/InstrumentData") != null) {
			instrumentData = doc.getAttribute("/In/InstrumentSearchInfo/InstrumentData");
			instrumentID = InstrumentServices.parseForInstrumentId(instrumentData);
     		linkedInstrumentID = instrumentID;
		}			
	 } else if (searchInstrumentType.equals(TradePortalConstants.ALL_EXPORT_DLC)) { //EXPORT
		focusField = "LinkedDLCInstrumentID";
		focusSet = true;
		if (doc.getDocumentNode("/In/InstrumentSearchInfo/InstrumentData") != null) {
			instrumentData = doc.getAttribute("/In/InstrumentSearchInfo/InstrumentData");
			instrumentID = InstrumentServices.parseForInstrumentId(instrumentData);
     		linkedInstrumentID = instrumentID;
		}			
	 } else if (searchInstrumentType.equals(TradePortalConstants.IMPORT)) { // IMPORT	
		focusField = "RelatedInstrumentID";
		focusSet = true;
		if (doc.getDocumentNode("/In/InstrumentSearchInfo/InstrumentData") != null) {
			instrumentData = doc.getAttribute("/In/InstrumentSearchInfo/InstrumentData");
			instrumentID = InstrumentServices.parseForInstrumentId(instrumentData);
     		linkedInstrumentID = instrumentID;
			loanProceedsCreditType = TradePortalConstants.CREDIT_REL_INST;
		}			
	 }  
  }
  // P Cutrone - IR-KHUG121933479 - Instrument Search focus Field issue [END]

  session.removeAttribute("InstrumentSearchListView.xml");

  transactionType = transaction.getAttribute("transaction_type_code");
  transactionStatus = transaction.getAttribute("transaction_status");

  // Get the transaction rejection indicator to determine whether to show the rejection reason text
  rejectionIndicator = transaction.getAttribute("rejection_indicator");
  rejectionReasonText = transaction.getAttribute("rejection_reason_text");
  
  instrumentType = instrument.getAttribute("instrument_type_code");
  instrumentStatus = instrument.getAttribute("instrument_status");

  Debug.debug("Instrument Type " + instrumentType);
  Debug.debug("Instrument Status " + instrumentStatus);
  Debug.debug("Transaction Type " + transactionType);
  Debug.debug("Transaction Status " + transactionStatus);

    DocumentHandler phraseList = formMgr.getFromDocCache(TradePortalConstants.PHRASE_LIST_DOC);
    if (phraseList == null) phraseList = new DocumentHandler();

    // This list of phrases is used in the General Section
    DocumentHandler customerTextList = phraseList.getFragment("/CustomerText");
    if (customerTextList == null)
    {
        customerTextList = PhraseUtility.createPhraseList(TradePortalConstants.PHRASE_CAT_GUAR_CUST,
                                                      userSession, formMgr, resMgr);
        phraseList.addComponent("/CustomerText", customerTextList);
    }

    //this list of phrases is used in the bank instructions section
    DocumentHandler specialInstructionsList = phraseList.getFragment("/SpecialInstructions");
    if (specialInstructionsList == null)
    {
        specialInstructionsList = PhraseUtility.createPhraseList(TradePortalConstants.PHRASE_CAT_SPCL_INST,
                                                                 userSession, formMgr, resMgr);
        phraseLists.addComponent("/SpecialInstructions", specialInstructionsList);
    }

    formMgr.storeInDocCache(TradePortalConstants.PHRASE_LIST_DOC, phraseList);




  // Now determine the mode for how the page operates (readonly, etc.)
  BigInteger requestedSecurityRight = SecurityAccess.LOAN_RQST_CREATE_MODIFY;
		  
  %>
  <%@ include file="fragments/Transaction-PageMode.frag" %>

<%-- ********************* HTML for page begins here ********************* --%>

  <%@include file="fragments/MultiPartMode.frag" %>

<% 
  // Determine whether the corporate org customer
  // has multiple addresses.  If it does we display "Search For An Address" button.
   
  if (isTemplate) // rbhaduri - 8th August 06 - IR CGUG071768901 - get the org oid from template for template instrm
     corpOrgOid = template.getAttribute("owner_org_oid");
  else
     corpOrgOid = instrument.getAttribute("corp_org_oid"); 

//MEer Rel 9.3.5  CR-1027  
  CorporateOrganizationWebBean corporateOrg = beanMgr.createBean(CorporateOrganizationWebBean.class, "CorporateOrganization");
  corporateOrg.getById(corpOrgOid);
  //Only for Invoice Financing
   String bankOrgGroupOid = corporateOrg.getAttribute("bank_org_group_oid");
  if(isReceivables || TradePortalConstants.INDICATOR_YES.equals(corporateOrg.getAttribute("invoice_financing_type"))){
	 
  	BankOrganizationGroupWebBean bankOrganizationGroup = beanMgr.createBean(BankOrganizationGroupWebBean.class, "BankOrganizationGroup");
  	bankOrganizationGroup.getById(bankOrgGroupOid);
 
  	String selectedPDFType =  bankOrganizationGroup.getAttribute("lrq_pdf_type");
 	 if(TradePortalConstants.APPLICATION_PDF_EXPANDED_TYPE.equals(selectedPDFType)){
			canDisplayPDFLinks = true;
 	 }
  }
  
  beanMgr.registerBean ("com.ams.tradeportal.busobj.webbean.OperationalBankOrganizationWebBean", "OperationalBankOrganization");
  OperationalBankOrganizationWebBean opBankOrg = (OperationalBankOrganizationWebBean)beanMgr.getBean("OperationalBankOrganization");

  String opOrgOid = instrument.getAttribute("op_bank_org_oid");
	opBankOrg.getById(opOrgOid);
	
	String bankGroupOid = opBankOrg.getAttribute("bank_org_group_oid");
  Object[] sqlParamsCnt = new Object[1];
  sqlParamsCnt[0] =  corpOrgOid;
  if (DatabaseQueryBean.getCount("address_oid", "address", "p_corp_org_oid = ?", true, sqlParamsCnt) > 0 )
  {
     corpOrgHasMultipleAddresses = true;
  }            


  /*************************************************
  * Load address search info if we come back from address search page.
  * Reload applicant terms party web beans with the data from 
  * the address
  **************************************************/
  if (doc.getDocumentNode("/In/AddressSearchInfo/AddressSearchPartyType") != null) {
     String termsPartyType = doc.getAttribute("/In/AddressSearchInfo/AddressSearchPartyType");
     String addressOid = doc.getAttribute("/In/AddressSearchInfo/AddressOid");
     String primarySelected = doc.getAttribute("/In/AddressSearchInfo/ChoosePrimary");

     Debug.debug("Returning from address search with " + termsPartyType
           + "/" + addressOid + ":" + primarySelected );

     
     // Use a Party web bean to get the party data for the selected oid.
     AddressWebBean address = null;  
     if ((addressOid != null || primarySelected != null) && termsPartyType.equals(TradePortalConstants.APPLICANT)) {
    	 if (primarySelected.equals("PRIMARY")) {
    		    //Moved these lines up-to get CorporateOrg bean
    		      termsPartyApp.setAttribute("name",corporateOrg.getAttribute("name"));
    		      termsPartyApp.setAttribute("address_line_1",corporateOrg.getAttribute("address_line_1"));
    		      termsPartyApp.setAttribute("address_line_2",corporateOrg.getAttribute("address_line_2"));
    		      termsPartyApp.setAttribute("address_city",corporateOrg.getAttribute("address_city"));
    		      termsPartyApp.setAttribute("address_state_province",corporateOrg.getAttribute("address_state_province"));
    		      termsPartyApp.setAttribute("address_country",corporateOrg.getAttribute("address_country"));
    		      termsPartyApp.setAttribute("address_postal_code",corporateOrg.getAttribute("address_postal_code"));
    		      termsPartyApp.setAttribute("address_seq_num","1");
    		      //Rel9.5 CR1132 Populate userdefinedfields from corporate
    				termsPartyApp.setAttribute("user_defined_field_1", corporateOrg.getAttribute("user_defined_field_1"));
    				termsPartyApp.setAttribute("user_defined_field_2", corporateOrg.getAttribute("user_defined_field_2"));
    				termsPartyApp.setAttribute("user_defined_field_3", corporateOrg.getAttribute("user_defined_field_3"));
    				termsPartyApp.setAttribute("user_defined_field_4", corporateOrg.getAttribute("user_defined_field_4"));
    		  }else{
    		 
        address = beanMgr.createBean(AddressWebBean.class, "Address");
        address.setAttribute("address_oid", addressOid);
        address.getDataFromAppServer();
        termsPartyApp.setAttribute("name",address.getAttribute("name"));
        termsPartyApp.setAttribute("address_line_1",address.getAttribute("address_line_1"));
        termsPartyApp.setAttribute("address_line_2",address.getAttribute("address_line_2"));
     	termsPartyApp.setAttribute("address_city",address.getAttribute("city"));
       	termsPartyApp.setAttribute("address_state_province",address.getAttribute("state_province"));
       	termsPartyApp.setAttribute("address_country",address.getAttribute("country"));
       	termsPartyApp.setAttribute("address_postal_code",address.getAttribute("postal_code"));
       	termsPartyApp.setAttribute("address_seq_num",address.getAttribute("address_seq_num"));
      //Rel9.5 CR1132 Populate userdefinedfields from address
		termsPartyApp.setAttribute("user_defined_field_1", address.getAttribute("user_defined_field_1"));
		termsPartyApp.setAttribute("user_defined_field_2", address.getAttribute("user_defined_field_2"));
		termsPartyApp.setAttribute("user_defined_field_3", address.getAttribute("user_defined_field_3"));
		termsPartyApp.setAttribute("user_defined_field_4", address.getAttribute("user_defined_field_4"));
    		  }
     }  
     focusField = "";
     focusSet = true;
     onLoad += "location.hash='#" + termsPartyType + "';";
     
  }
  //TLE - 03/30/07 - IR-AYUH021051356 - Add Begin
  //We need to display some fields in read-only mode for Loan Request when
  //a transaction is rejected from bank. For that reason, we cannot use/pass the is_ReadOnly
  //variable when creating objects/fields. Therefore, the variable ib_rejectedByBankReadOnly
  //is used for that purpose. This new variable will set = true if the isReadOnly = true. But if 
  //the isReadOnly = false but the transaction is rejected from bank, this variable will
  //be set = true so that fields can be created in readonly mode.
  boolean ib_rejectedByBankReadOnly = false;

  if (isReadOnly) {
     ib_rejectedByBankReadOnly = true;
  } 

  if ((!ib_rejectedByBankReadOnly) && (transactionStatus.equals(TradePortalConstants.TRANS_STATUS_REJECTED_BY_BANK)) || (!rejectionReasonText.equals(""))) {
     ib_rejectedByBankReadOnly = true;
  }
  //TLE - 03/30/07 - IR-AYUH021051356 - Add End

 // Some of the retrieval logic above may have set a focus field.  Otherwise,
  // we'll use the initial value for focus.
	isUpldInvsAttchdLoanType = isUpldInvsAttchdLoanType || ib_rejectedByBankReadOnly;
  

  if (multiPartMode && !focusSet)
  {
     if (whichPart.equals("") || whichPart.equals(TradePortalConstants.PART_ONE))
     {
        focusField = "ApplRefNum";
     }
     else if (whichPart.equals(TradePortalConstants.PART_TWO))
     {
        focusField = "TransactionAmount";
     }
  }

  //TLE - 03/30/07 - IR-AYUH021051356 - Add Begin
  //Pass transactionStatus
  //onLoad += "setupImportExport();";
  if (ib_rejectedByBankReadOnly) {
     if (isImport) {
        onLoad += "setupImportExport('REJECTED_BY_BANK_IMPORT');";
     } else if (isExport) {
        onLoad += "setupImportExport('REJECTED_BY_BANK_EXPORT');";
	 } else if (isReceivables) { 
        onLoad += "setupImportExport('REJECTED_BY_BANK_RECEIVABLES');";
     } else if (isTradePayables | isTradeReceivables) { 
        onLoad += "setupImportExport('REJECTED_BY_BANK_TRADELOAN');";
     }
  } else {
     onLoad += "setupImportExport('');";
  }	 
  //TLE - 03/30/07 - IR-AYUH021051356 - Add End
%>

<%

    CorporateOrganizationWebBean corporateOrgBean = beanMgr.createBean(CorporateOrganizationWebBean.class, "CorporateOrganization");
    corporateOrgBean.setAttribute("organization_oid", corpOrgOid);
    corporateOrgBean.getDataFromAppServer();
    String orgTradeLoanType = corporateOrgBean.getAttribute("trade_loan_type");
    String importLoanType = corporateOrgBean.getAttribute("import_loan_type"); 
    String exportLoanType = corporateOrgBean.getAttribute("export_loan_type");
    String invoiceFinancingType = corporateOrgBean.getAttribute("invoice_financing_type");
    String userSecurityRights = userSession.getSecurityRights();
    String corOrgProcessInvoicesFileUploadInd = corporateOrgBean.getAttribute("process_invoice_file_upload");
    boolean loanReqProcessInvoices = SecurityAccess.canProcessLRQInvoice(userSecurityRights, InstrumentType.LOAN_RQST);
    // New design change added to IR-41755
    Map<String, String> selectedLoanTypes = new HashMap<String, String>();
    if(TradePortalConstants.INDICATOR_YES.equals(orgTradeLoanType))
    	selectedLoanTypes.put("TradeLoanType",orgTradeLoanType );
    if(TradePortalConstants.INDICATOR_YES.equals(importLoanType))
    	selectedLoanTypes.put("ImportLoanType",importLoanType);
    if(TradePortalConstants.INDICATOR_YES.equals(exportLoanType))
    	selectedLoanTypes.put("ExportLoanType",exportLoanType );
    if(TradePortalConstants.INDICATOR_YES.equals(invoiceFinancingType))
    	selectedLoanTypes.put("InvoiceFinancingType",invoiceFinancingType);
    
    
    
%>

<%
  // The navigation bar is only shown when editing templates.  For transactions
  // it is not shown ti minimize the chance of leaving the page without properly
  // unlocking the transaction.
  String showNavBar = TradePortalConstants.INDICATOR_NO;
  if (isTemplate)
  {
     showNavBar = TradePortalConstants.INDICATOR_YES;
  }

  // Auto save the form when time-out if not readonly.  
  // (Header.jsp will check for auto-save setting of the corporate org).
  String autoSaveFlag = isReadOnly ? TradePortalConstants.INDICATOR_NO : TradePortalConstants.INDICATOR_YES;
  String templateFlag = isTemplate ? TradePortalConstants.INDICATOR_YES : TradePortalConstants.INDICATOR_NO;
%>
<%
  String pageTitleKey="";
if ( TradePortalConstants.INDICATOR_YES.equals(newTransaction) ) {
    pageTitleKey = "SecondaryNavigation.NewInstruments";
    
    if (isTemplate){
         if(userSession.getSecurityType().equals(TradePortalConstants.ADMIN)){
         userSession.setCurrentPrimaryNavigation("AdminNavigationBar.RefData");
    }else{
       userSession.setCurrentPrimaryNavigation("NavigationBar.RefData");
    }
    }else
         userSession.setCurrentPrimaryNavigation("NavigationBar.NewInstruments");
} else {
    pageTitleKey = "SecondaryNavigation.Instruments";
    if (isTemplate){
         if(userSession.getSecurityType().equals(TradePortalConstants.ADMIN)){
         userSession.setCurrentPrimaryNavigation("AdminNavigationBar.RefData");
    }else{
       userSession.setCurrentPrimaryNavigation("NavigationBar.RefData");
    }
    }else 
         userSession.setCurrentPrimaryNavigation("NavigationBar.Transactions");
}

    
  String onlineHelpFileName = "customer/issue_loan_request.htm";

  //cquinton 11/19/2012 pass info for recent instruments into header
  String newRecentTransactionOid = transaction.getAttribute("transaction_oid");
  String newRecentInstrumentId = instrument.getAttribute("complete_instrument_id");
  String newRecentTransactionType = transaction.getAttribute("transaction_type_code");
  String newRecentTransactionStatus = transaction.getAttribute("transaction_status");
%>

<jsp:include page="/common/Header.jsp">
  <jsp:param name="autoSaveFlag"           value="<%=autoSaveFlag%>" />
  <jsp:param name="templateFlag"           value="<%=templateFlag%>" />
  <jsp:param name="newRecentTransactionOid" value="<%=newRecentTransactionOid%>" />
  <jsp:param name="newRecentInstrumentId"  value="<%=newRecentInstrumentId%>" />
  <jsp:param name="newRecentTransactionType" value="<%=newRecentTransactionType%>" />
  <jsp:param name="newRecentTransactionStatus" value="<%=newRecentTransactionStatus%>" />
</jsp:include>

<% //cr498 begin
   //Include ReAuthentication frag in case re-authentication is required for
   //authorization of transactions for this client
   String certAuthURL = "";
   reCertCache = (Cache)TPCacheManager.getInstance().getCache(TradePortalConstants.CLIENT_BANK_CACHE);
   CBCResult = (DocumentHandler)reCertCache.get(userSession.getClientBankOid());	
   String requireTranAuth = CBCResult.getAttribute("/ResultSetRecord(0)/REQUIRE_TRAN_AUTH");
   boolean requireAuth = InstrumentAuthentication.requireTransactionAuthentication(
       requireTranAuth,InstrumentAuthentication.TRAN_AUTH__LRQ_ISS);
   if (requireAuth) {
%>   
      <%@ include file="/logon/fragments/openReauthenticationWindow.frag" %>
<%
   }
   //cr498 end
%>




<div class="pageMain">
<div class="pageContent">

<%
		  if(isTemplate) {
		  String pageTitle;
		  String titleName;
		  String returnAction;
		  pageTitle = resMgr.getText("common.template", TradePortalConstants.TEXT_BUNDLE);
		  titleName = template.getAttribute("name");
		  StringBuffer title = new StringBuffer();
		  String itemKey = "";
		  title.append(pageTitle);
		  title.append( " : " );
		  if(instrument.getAttribute("instrument_type_code").equals("SLC")){
		  if(transaction.getAttribute("standby_using_guarantee_form").equals(TradePortalConstants.INDICATOR_NO)){     
				  itemKey = resMgr.getText("SecondaryNavigation.Instruments.OutgoingSTandbyLCSimple", TradePortalConstants.TEXT_BUNDLE); 
		  }else{
				  itemKey = resMgr.getText("SecondaryNavigation.Instruments.OutgoingStandbyLCDetailed", TradePortalConstants.TEXT_BUNDLE);
		  }
		  title.append(itemKey);
		  }
		  else
		  title.append( refData.getDescr(TradePortalConstants.INSTRUMENT_TYPE, 
		                 instrument.getAttribute("instrument_type_code"), 
		                 loginLocale) );
		  title.append( " - " );
		  title.append(titleName );
		  returnAction = "goToInstrumentCloseNavigator";
		
		  String transactionSubHeader = title.toString();
  		
  		%>
		<jsp:include page="/common/PageHeader.jsp">
				<jsp:param name="titleKey" value="<%= pageTitle%>"/>
				<jsp:param name="helpUrl"  value="<%=onlineHelpFileName%>" />
  		</jsp:include>
  		
  		<jsp:include page="/common/PageSubHeader.jsp">
 				 <jsp:param name="titleKey" value="<%= transactionSubHeader%>" />
  				 <jsp:param name="returnUrl" value="<%= formMgr.getLinkAsUrl(returnAction, response) %>" />
		</jsp:include> 		
		 <%}else{ %>
<jsp:include page="/common/PageHeader.jsp">
   <jsp:param name="titleKey" value="<%=pageTitleKey%>" />
   <jsp:param name="item1Key" value="SecondaryNavigation.Instruments.LoanRequest" />
   <jsp:param name="helpUrl"  value="<%=onlineHelpFileName%>" />
</jsp:include>

<jsp:include page="/common/TransactionSubHeader.jsp" />

<%} %>
<form id="TransactionLRQ" name="TransactionLRQ"
	method="post" data-dojo-type="dijit.form.Form" action="<%=formMgr.getSubmitAction(response)%>">
	
  <input type=hidden value="" name=buttonName>
  <input type=hidden value=<%=(areInvoicesAttached)?"Y":"N" %> name=areInvoicesAttached>	
<%-- error section goes above form content --%>
<div class="formArea">
<jsp:include page="/common/ErrorSection.jsp" />

<div class="formContent">
<% //cr498 begin
  if (requireAuth) {
%> 

  <input type=hidden name="reCertification" value="<%=TradePortalConstants.INDICATOR_NO%>">
  <input type=hidden name="reCertOK">
  <input type=hidden name="logonResponse">
  <input type=hidden name="logonCertificate">

<%
  } //cr498 end
%> 

<%
  // Store values such as the userid, security rights, and org in a
  // secure hashtable for the form.  Also store instrument and transaction
  // data that must be secured.
  Hashtable secureParms = new Hashtable();
  secureParms.put("login_oid", userSession.getUserOid());
  secureParms.put("owner_org_oid", userSession.getOwnerOrgOid());
  secureParms.put("login_rights", loginRights);

  secureParms.put("instrument_oid",        instrument.getAttribute("instrument_oid"));
  secureParms.put("instrument_type_code",  instrumentType);
  secureParms.put("instrument_status",     instrumentStatus);
  secureParms.put("corp_org_oid",  corpOrgOid);

  secureParms.put("transaction_oid",       transaction.getAttribute("transaction_oid"));
  secureParms.put("transaction_type_code", transactionType);
  secureParms.put("transaction_status",    transactionStatus);

  secureParms.put("ShipmentOid", terms.getFirstShipment().getAttribute("shipment_oid"));
  
  secureParms.put("transaction_instrument_info", 
  	transaction.getAttribute("transaction_oid") + "/" + 
	instrument.getAttribute("instrument_oid") + "/" + 
	transactionType);

  // If the terms record doesn't exist, set its oid to 0.
  String termsOid = terms.getAttribute("terms_oid");

  if (termsOid == null)
  {
     termsOid = "0";
  }
  secureParms.put("terms_oid", termsOid);

  if (isTemplate)
  {
     secureParms.put("template_oid", template.getAttribute("template_oid"));
     secureParms.put("opt_lock",     template.getAttribute("opt_lock"));
  }
%>

  <%@include file="fragments/MultiPartModeFormElements.frag" %>

<%
    String extraPartTags = request.getParameter("extraPartTags");
%>

  
<%
  if (rejectionIndicator.equals(TradePortalConstants.INDICATOR_YES)
  	|| rejectionIndicator.equals(TradePortalConstants.INDICATOR_X))
  {
%>
<%=widgetFactory.createSectionHeader("0", "Rejection Reason") %>
     <%@ include file="fragments/Transaction-RejectionReason.frag" %>
     </div>
<%
  }
if (isTemplate)
{
%>
  <%@ include file="fragments/TemplateHeader.frag" %>
<%
} else {
%>


<%
  }
%>
<%@ include file="fragments/Instruments-AttachDocuments.frag" %>

<% //CR 821 Added Repair Reason Section 
StringBuffer repairReasonWhereClause = new StringBuffer();
int  repairReasonCount = 0;
	
	/*Get all repair reason's count from transaction history table*/
	repairReasonWhereClause.append("p_transaction_oid = ?");
	repairReasonWhereClause.append(" and REPAIR_REASON is not null order by ACTION_DATETIME");
	//jgadela  R90 IR T36000026319 - SQL FIX
	Object[] sqlParamsTran = new Object[1];
	sqlParamsTran[0] = transaction.getAttribute("transaction_oid");
	Debug.debug("repairReasonWhereCondition query : " + repairReasonWhereClause);
	repairReasonCount = DatabaseQueryBean.getCount("A_USER_OID", "transaction_history", repairReasonWhereClause.toString(), true, sqlParamsTran);
			
if ( !(TradePortalConstants.TRANS_STATUS_PROCESSED_BY_BANK.equalsIgnoreCase(transactionStatus)) && repairReasonCount>0  ){%>
	    <%=widgetFactory.createSectionHeader("", "TransactionHistory.RepairReason") %>
		<%@ include file="fragments/Transaction-RepairReason.frag" %>
	</div>
<%} %> 

	
<% // [BEGIN] IR-YVUH032343792 - jkok %>
		
			<%-- Form Content Area starts here --%>
			<%=widgetFactory.createSectionHeader("1",
						"LoanRequest.General")%>
				<%@ include file="fragments/Transaction-LRQ-ISS-General.frag"%>
			</div>
	
			<%=widgetFactory.createSectionHeader("2",
						"LoanRequest.LoanRequestDetails")%>
			<%@ include
				file="fragments/Transaction-LRQ-ISS-LoanRequestDetails.frag"%>
			</div>
			
			<%=widgetFactory.createSectionHeader("3",
							"LoanRequest.LoanInstructions")%>
				<div class="formContent2">
					<%-- Form Content Area starts here --%>
					<%=widgetFactory.createSectionHeader("3.1",
									"LoanRequest.ExportInstructions")%>
					<%@ include
						file="fragments/Transaction-LRQ-ISS-ExportLoanInstructions.frag"%>
					</div>
				
					<%=widgetFactory.createSectionHeader("3.2",
									"LoanRequest.ImportInstructions")%>
					<%@ include
						file="fragments/Transaction-LRQ-ISS-ImportLoanInstructions.frag"%>
					</div>
					<%=widgetFactory.createSectionHeader("3.3",
									"LoanRequest.InvoiceInstructions")%>
					<%@ include
						file="fragments/Transaction-LRQ-ISS-InvoiceFinancingInstructions.frag"%>
					</div>

                    <%=widgetFactory.createSectionHeader("3.4",
                            "LoanRequest.TradeLoanInstructions")%>
                    <%@ include
                            file="fragments/Transaction-LRQ-ISS-TradeLoanInstructions.frag"%>
                    </div>
				</div>
			</div>
		
			<%=widgetFactory.createSectionHeader("4",
							"LoanRequest.ChargesAndInterest")%>
<div class="formItem">
    <%=widgetFactory.createRadioButtonField("ChargesAndInterestType","DebitAccounts", "LoanRequest.DebitAccounts",TradePortalConstants.DEBIT_OUR_ACCOUNTS, TradePortalConstants.DEBIT_OUR_ACCOUNTS.equals(terms.getAttribute("coms_chrgs_type")) ,ib_rejectedByBankReadOnly, "", "")%>
    <br>
			<%@ include file="fragments/Transaction-LRQ-ISS-ChargesInterest.frag"%>
    <%=widgetFactory.createRadioButtonField("ChargesAndInterestType","DebitLoanProceeds", "LoanRequest.DebitChargesFromLoanProceeds",TradePortalConstants.DEBIT_LOAN_PROCEEDS, TradePortalConstants.DEBIT_LOAN_PROCEEDS.equals(terms.getAttribute("coms_chrgs_type")) ,ib_rejectedByBankReadOnly, "", "")%>
    <br>
    <%=widgetFactory.createRadioButtonField("ChargesAndInterestType","DebitAcctReceivables", "LoanRequest.DebitChargesFromAccountsReceivables",TradePortalConstants.DEBIT_ACCT_REC, TradePortalConstants.DEBIT_ACCT_REC.equals(terms.getAttribute("coms_chrgs_type")) ,ib_rejectedByBankReadOnly, "", "")%>
    </div>
			</div> 
		
			<%=widgetFactory.createSectionHeader("5",
							"LoanRequest.ExchangeRate")%>
			<%@ include file="fragments/Transaction-LRQ-ISS-ExchangeRate.frag"%>
			</div>
		
			<%=widgetFactory.createSectionHeader("6",
							"LoanRequest.InstructionstoBank")%>
			<%@ include file="fragments/Transaction-LRQ-ISS-BankInstructions.frag"%>
			</div>
			<%=widgetFactory.createSectionHeader("7",
							"LoanRequest.InternalInstructions")%>
			<%@ include
				file="fragments/Transaction-LRQ-ISS-InternalInstructions.frag"%>
			</div>
			<%-- // Only display the bank defined section if the user is an admin user --%>
		
			<% // Only display the bank defined section if the user is an admin user
			  // IR-42689 REL 9.4 SURREWSH Start

     if (userSession.getSecurityType().equals(TradePortalConstants.ADMIN) || userSession.hasSavedUserSession())
  {
%>
			<%=widgetFactory.createSectionHeader("8",
								"LoanRequest.BankDefined")%>
			<%@ include file="fragments/Transaction-LRQ-ISS-BankDefined.frag"%>
			</div>
			<%
  }
%>
			
<% if (TradePortalConstants.TRANS_STATUS_PROCESSED_BY_BANK.equalsIgnoreCase(transactionStatus) && repairReasonCount>0  ){
		 if (TradePortalConstants.ADMIN.equals(userSession.getSecurityType()))   {%>
						<%=widgetFactory.createSectionHeader("9", "TransactionHistory.RepairReason", null,true) %>
							<%@ include file="fragments/Transaction-RepairReason.frag" %>
						</div>
					<%}else{%>
						<%=widgetFactory.createSectionHeader("8", "TransactionHistory.RepairReason", null, true) %>
						<%@ include file="fragments/Transaction-RepairReason.frag" %>
					</div>
				<% 	}	 
	 }%> 

		</div><%--formContent--%>
		<%-- Form Content End Here --%>
		</div><%--formArea--%>

		<% if (!isReadOnly){ %> 
                  <%=widgetFactory.createHoverHelp("borrower", "PartySearchIconHoverHelp") %>
                  <%=widgetFactory.createHoverHelp("InstrumentId", "InstrumentHoverText") %>
                   <%=widgetFactory.createHoverHelp("InstrumentId_1", "InstrumentHoverText") %>
                   <%=widgetFactory.createHoverHelp("InstrumentId_2", "InstrumentHoverText") %>
                  <%=widgetFactory.createHoverHelp("bbkexp", "PartySearchIconHoverHelp") %>
                  <%=widgetFactory.createHoverHelp("benexp", "PartySearchIconHoverHelp") %>
                  <%=widgetFactory.createHoverHelp("bbkimp", "PartySearchIconHoverHelp") %>
                  <%=widgetFactory.createHoverHelp("benimp", "PartySearchIconHoverHelp") %>
                   <%=widgetFactory.createHoverHelp("releasetopartySection", "PartySearchIconHoverHelp") %>
                  <%=widgetFactory.createHoverHelp("sellerdetails", "PartySearchIconHoverHelp") %>
                  <%= widgetFactory.createHoverHelp("ClearApplicantButton","PartyClearIconHoverHelp")%>
                  <%= widgetFactory.createHoverHelp("ClearBenButton","PartyClearIconHoverHelp")%>
                  <%= widgetFactory.createHoverHelp("ClearSellerButton","PartyClearIconHoverHelp")%>
                   <%= widgetFactory.createHoverHelp("ClearBenButton_1","PartyClearIconHoverHelp")%>
                  <%= widgetFactory.createHoverHelp("FinancedPOsDetailsText","LoanRequest.StandalonePreshipmentFinanced")%>
                  <%= widgetFactory.createHoverHelp("PRESHIPMENT_FINANCING","PRESHIPMENT_FINANCINGHelpText")%>
                  <%= widgetFactory.createHoverHelp("EXP_DLC","EXP_DLC")%>
                  <%= widgetFactory.createHoverHelp("EXP_COL","EXP_COL")%>
                  
                  
      <%} %>
		<%=widgetFactory.createHoverHelp("FinanceTypeTextExp","LoanRequest.FinanceTypeTextExp") %>
		<%=widgetFactory.createHoverHelp("GoodsDescription","EnterGoodDec") %>
		
<div class="formSidebar" data-dojo-type="widget.FormSidebar"
	data-dojo-props="form: 'TransactionLRQ'">

	<jsp:include page="/common/Sidebar.jsp">
		<jsp:param name="links" value="<%=links%>" />
		<jsp:param name="isReadOnly" value="<%=isReadOnly%>" />
		<jsp:param name="certAuthURL" value="<%=certAuthURL%>" />
		<jsp:param name="buttonPressed" value="<%=buttonPressed1%>" />
        <jsp:param name="error" value="<%= error%>" />
        <jsp:param name="formName" value="0" />
        <jsp:param name="isTemplate" value="<%=isTemplate%>"/>
        <jsp:param name="showLinks" value="true" /> 
        <jsp:param name="showApplnForm" value="<%=canDisplayPDFLinks%>"/>
        <jsp:param name="isNewTransaction" value="<%=newTransaction%>"/>
	</jsp:include>
</div>


<%@ include file="fragments/PhraseLookupPrep.frag"%>
<%@ include file="fragments/PartySearchPrep.frag"%>
<input type="hidden" name="selection" />
<input type="hidden" name="ChoosePrimary" />
<%= formMgr.getFormInstanceAsInputField("Transaction-LRQForm", secureParms) %>
<div id="PartySearchDialog"></div>
<div id="BankSearchDialog"></div>
<div id="instrumentSearchDialog"></div>
</form>
</div>
</div>

<%-- KMehta on 12 May 2014 @ Rel 90 for IR T36000025449 Start --%>
<jsp:include page="/common/Footer.jsp">
   <jsp:param name="autoSaveFlag"           value="<%=autoSaveFlag%>" />
   <jsp:param name="templateFlag"           value="<%=templateFlag%>" />
   <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>
<%-- KMehta on 12 May 2014 @ Rel 90 for IR T36000025449 End --%>
<jsp:include page="/common/SidebarFooter.jsp"/>

<%@ include file="/transactions/fragments/Transaction-LRQ-ISS-TradeLoan-Footer.frag" %>

<script>

  <%-- cquinton 3/3/2013 add local var --%>
  <%-- var local = {}; --%>

<%
  if (!isReadOnly && InstrumentServices.isNotBlank(focusField)) {
%>
  require(["dijit/registry", "dojo/ready"],
    function(registry, ready ) {
      ready(function() {
        var focusFieldId = '<%= focusField %>';
        if ( focusFieldId ) {
          var focusField = registry.byId(focusFieldId);
        <%--  focusField.focus(); --%>
        }
      });
  });
<%
  }
%>

		function setCreditOtherAcct() {
			dijit.getEnclosingWidget(document.forms[0].creditOtherAcct).set('checked',true);
		
		}
		function setCreditConsignAccount(){
			dijit.getEnclosingWidget(document.forms[0].CreditConsignAccount).set('checked',true);
		}
<%-- This function sets search instrument type for the search --%>
     function setInstrumentSearchType(searchInstrumentType) {
        document.forms[0].SearchInstrumentType.value = searchInstrumentType;
		if (searchInstrumentType == "ALL_EXP_DLC") {
			document.forms[0].FinanceType[2].checked = true;
		} else if (searchInstrumentType == "EXP_COL") {
			document.forms[0].FinanceType[3].checked = true;
		} else {
			document.forms[0].LoanProceedsCreditTypeImp[0].checked = true;
		}
        return true;
     }
	 
	 

	 function setupImportExport(transactionStatus) {
	 	if ((document.forms[0].ImportIndicator[0] != null && document.forms[0].ImportIndicator[1].checked == true) || (transactionStatus == "REJECTED_BY_BANK_EXPORT")) { <%--  Export --%>
			showExport();
		} else if ((document.forms[0].ImportIndicator[0] != null && document.forms[0].ImportIndicator[2].checked == true) || (transactionStatus == "REJECTED_BY_BANK_IMPORT")) { <%--  Import --%>
			showImport();
		} else if ((document.forms[0].ImportIndicator[0] != null && document.forms[0].ImportIndicator[3].checked == true) || (transactionStatus == "REJECTED_BY_BANK_RECEIVABLES")) { <%--  Receivables --%>
			showReceivables();
		} else if ((document.forms[0].ImportIndicator[0] != null && document.forms[0].ImportIndicator[0].checked == true) || (transactionStatus == "REJECTED_BY_BANK_TRADELOAN")) { <%--  Payables --%>
			showTadeLoan();
		} else { <%-- Probably in readonly so just show both --%>
			hideExportImportReceivables();
		}
	 } 

</script>

<%--cquinton 2/7/2013 added for debugging if necessary--%>
<%--
  out.println("getDataFromDoc="+getDataFromDoc);
  out.println(" newPartyType="+newPartyType);
  out.println(" newPartyOid="+newPartyOid);
  out.println(" xmlDoc="+StringFunction.xssCharsToHtml(doc.toString()));
--%>

</body>
</html>

<%
   // Finally, reset the cached document to eliminate carryover of
   // information to the next visit of this page.
   formMgr.storeInDocCache("default.doc", new DocumentHandler());
%>

<%!
   // Various methods are declared here (in alphabetical order).


   public String getRequiredIndicator(boolean showIndicator) {
      if (showIndicator) {
         return "<span class=Asterix>*</span>";
      } else {
         return "";
      }
   }

%> 

<%@include file="fragments/DesignatedPartyLookup.frag" %>
<script type="text/javascript">

  var itemid;
  var section;
  
var local = {};
  
  require(["dojo/_base/array", "dojo/_base/xhr", "dojo/dom", "dojo/dom-construct", "dojo/dom-style", "dijit/registry", "t360/common"],
	function(arrayUtil, xhr, dom, domConstruct, domStyle, registry, common) {
	  
		var accountTypeDiv = "exportBeneAccountsData";
		var beneAccounts = "";
		var beneAccountsWidgets = "";
		
	    <%-- cquinton 1/4/2013 after populating the party fields we also  --%>
	    <%--  need to get party account information for population here --%>
	    local.chooseBeneficiary = function(selectedRowKey) {
	    	<%-- Srinivasu_D IR#T36000018962 Rel8.2 07/10/2013 start - checking for corp settings  --%>
			<%-- to avoid javacript error in case of unselection of loan types.  --%>
			var expLoan = '<%=exportLoanType%>';
			var impLoan = '<%=importLoanType%>';
			var finType = '<%=invoiceFinancingType%>';
			var tradeType = '<%=orgTradeLoanType%>';
			var yes = '<%=TradePortalConstants.INDICATOR_YES%>';
	    	
	    	<%-- first remove the existing beneficiary account number field and  --%>
			<%--  replace with a loading... message --%>
			beneAccounts = dom.byId("exportBeneAccountsData");
			beneAccountsWidgets = registry.findWidgets(beneAccounts);
			arrayUtil.forEach(beneAccountsWidgets, function(entry, i) {
			  entry.destroyRecursive(true);
			});
			domConstruct.empty(beneAccounts);
			
			beneAccounts = dom.byId("importBeneAccountsData");
			beneAccountsWidgets = registry.findWidgets(beneAccounts);
			arrayUtil.forEach(beneAccountsWidgets, function(entry, i) {
			  entry.destroyRecursive(true);
			});
			domConstruct.empty(beneAccounts);
			
			<%-- Nar IR-T36000031438 11/06/2014 Rel9.2 Add- Begin --%>
			beneAccounts = dom.byId("tradeLoanBeneAccountsData");
			if(beneAccounts){
			   beneAccountsWidgets = registry.findWidgets(beneAccounts);
			   arrayUtil.forEach(beneAccountsWidgets, function(entry, i) {
			      entry.destroyRecursive(true);
			   });
			   domConstruct.empty(beneAccounts);
			}
			<%-- Nar IR-T36000031438 11/06/2014 Rel9.2 Add- End --%>
	    	
	    	<%-- Check for the Loan type before populating data .   --%>
	    	<%-- Also set the div, where to populate the account number. --%>
	    	if(yes == expLoan && registry.byId("ExportLoanRadio").checked == true){								
				
				accountTypeDiv = "exportBeneAccountsData";
				
	    	}else if(yes == impLoan && registry.byId("ImportLoanRadio").checked == true){
	    						
				accountTypeDiv = "importBeneAccountsData";
			<%-- Nar IR-T36000031438 11/06/2014 Rel9.2 Add- Begin --%>	
	    	}else if(yes == tradeType && registry.byId("TradeLoanRadio").checked == true){
	    						
				accountTypeDiv = "tradeLoanBeneAccountsData";
				<%-- Nar IR-T36000031438 11/06/2014 Rel9.2 Add- End --%>	
	    	}else if(yes == finType && registry.byId("InvoiceFinancingRadio").checked == true){
	    		<%-- todo --%>
	    	}else{
	    		<%-- if user try's to search with out any loan type :-  --%>
	    		<%-- todo --%>
	    	} 
	    	
	     
	      domStyle.set("beneAccountsDataLoading", 'display', 'inline-block');
	      <%-- make an ajax call to get the account info --%>
	      common.attachAjaxResult(accountTypeDiv,
	        "/portal/transactions/Transaction-LRQ-ISS-BeneAcctData.jsp?partyType=BEN&partyRowKey="+selectedRowKey+"&isTemplate=<%=isTemplate%>&accountTypeDiv="+accountTypeDiv,
	        local.beneAccountsDataLoadedCallback);
	      
	    }
	    
	  <%-- after loading account data, remove the loading message --%>
	    local.beneAccountsDataLoadedCallback = function() {
	      domStyle.set("beneAccountsDataLoading", 'display', 'none');
	    }
  });
  
  function BankSearch(identifierStr, sectionName,bankType){
		
		itemid = String(identifierStr);
		section = String(sectionName);
		bankType= String(bankType);
		var paymentMethodCode = dijit.byId('PaymentMethodType').value;
		var account_oid = "";
		var beneCountry = '<%=opOrgCountry%>';
		
		if (dijit.byId('TradeLoanRadio').checked) {		
			if (dijit.byId('CreditOurAcctTrd').checked) {
				account_oid = dijit.byId('LoanProceedsCreditAccountTrd').value;
				if (account_oid == '') {
					alert('Debit Account Number and Currency is empty');
					return;
				}
			}
		}
		
		if (paymentMethodCode == '') {
			alert('Please select a Payment Method for this Beneficiary.');
			return;
		}
		
		require(["t360/dialog"], function(dialog ) {
		dialog.open('BankSearchDialog', '<%=BankSearchAddressTitle%>',
		'BankSearch.jsp',
		['returnAction','paymentMethodCode','bankType','itemid','section','account_oid','beneCountry'],
		['selectTransactionParty',paymentMethodCode,bankType,itemid,section,account_oid,beneCountry], <%-- parameters --%>
		'select', null);
		});	
	
	}


  function SearchParty(identifierStr, sectionName , partyType){

    itemid = String(identifierStr);
    section = String(sectionName);
    partyType=String(partyType);
  	
    require(["dojo/dom", "t360/dialog"], function(dom, dialog ) {

      <%-- cquinton 2/7/2013 --%>
      <%-- set the SearchPartyType input so it is included on new party form submit --%>
      var searchPartyTypeInput = dom.byId("SearchPartyType");
      if ( searchPartyTypeInput ) {
        searchPartyTypeInput.value=partyType;
      }

      dialog.open('PartySearchDialog', '<%=PartySearchAddressTitle%>',
        'PartySearch.jsp',
        ['returnAction','filterText','partyType','unicodeIndicator','itemid','section'],
        ['selectTransactionParty','',partyType,'<%=TradePortalConstants.INDICATOR_NO%>',itemid,section],
        'select', local.chooseBeneficiary);
    });
  }

function SearchInstrument(identifierStr, sectionName , InstrumentType) {
    <%--todo: add dialog require here, but need to walk back to event handler from the menus!!! --%> 
    itemid = String(identifierStr);
	section = String(sectionName);
	IntType=String(InstrumentType);
	addEXPOCO='N';
	if (IntType == 'EXP_COL'){
		addEXPOCO = 'Y';
	}
	        
    require(["t360/dialog"], function(dialog ) {
      dialog.open('instrumentSearchDialog', '<%=instrumentSearchDialogTitle%>',
                  'instrumentSearchDialogID.jsp',
                  ['itemid','section','IntType','addEXPOCO'], [itemid,section,IntType,addEXPOCO], <%-- parameters --%>
                  'select', null);
     
    });
  }

</script>
<script>

<%-- Start IR#T36000031463 Vsarkary Rel 9.2 - Validating Charges Label for 
mandatory when payables is selected and non-mandatory when receivables is selected dynamically--%>
function validateMandatoryChargesField() {
	require(["dojo/dom"], function(dom) {
    	    	 if (document.forms[0].FinanceType[0].checked == true){
    	    		 dojo.byId("chargesDivId").setAttribute("class", "formItem");
    	    	 }
    	    	else if (document.forms[0].FinanceType[1].checked == true){
    	    		 dojo.byId("chargesDivId").setAttribute("class", "formItem required");
    	    }
    	    });
	
}

function showHideDivs(radName) {
var invClassification ='<%=invClassification%>';
var noofInvsAttchd=<%=numberOfInvoicesAttached%>;
var isRec=<%=isTradeReceivables%>;
var isPay=<%=isTradePayables%>;
var isReadOnly =<%=isUpldInvsAttchdLoanType%>;
var pageReadOnly =<%=isReadOnly%>;

	if(radName == 'TradeLoanRadio'){
		require(["dojo/dom","dojo/dom-style"], function(dom, domStyle) {
              		  domStyle.set(dom.byId("section3.4"),'display', 'block');
			  domStyle.set(dom.byId("section3.3"),'display', 'none');
			  domStyle.set(dom.byId("section3.2"),'display', 'none');
			  domStyle.set(dom.byId("section3.1"),'display', 'none');
			  if (noofInvsAttchd > 0){
			  	domStyle.set(dom.byId("section2.2"),'display', 'none');
			  }
			  if ('R' == invClassification || isRec){
				  validateMandatoryChargesField();
					   if (document.getElementById('payablesBenRadioBtns')){
						   document.getElementById("payablesBenRadioBtns").style.display = "none";
						   
					   }
					   if (dijit.getEnclosingWidget(document.getElementById('CreditOurAcctTrd'))){
						   dijit.getEnclosingWidget(document.getElementById('CreditOurAcctTrd')).set('checked', true);
					   }
					   var loanProceedsPmtCurrTrd = dijit.getEnclosingWidget(document.getElementById('LoanProceedsPmtCurrTrd'))	;
					   if (loanProceedsPmtCurrTrd){
						   loanProceedsPmtCurrTrd.set("readOnly",isReadOnly);
					   }
					   var loanProceedsPmtAmountTrd = dijit.getEnclosingWidget(document.getElementById('LoanProceedsPmtAmountTrd'))	;
					   if (loanProceedsPmtAmountTrd){
						   loanProceedsPmtAmountTrd.set("readOnly",isReadOnly);
					   }
					   hideBeneficiaryDetails(true);
 
			  } else if ('P' == invClassification || isPay){
				  validateMandatoryChargesField();
				   if (document.getElementById('payablesBenRadioBtns')){
					   document.getElementById("payablesBenRadioBtns").style.display = "block";
				   }
				   var areMultiBeneficiariesAdded =<%=areMultiBeneficiariesAdded%>;
				   var loanProceedsPmtCurrTrd = dijit.getEnclosingWidget(document.getElementById('LoanProceedsPmtCurrTrd'))	;
				   if (loanProceedsPmtCurrTrd && !areMultiBeneficiariesAdded){
					   loanProceedsPmtCurrTrd.set("readOnly",isReadOnly);
				   }
				   var loanProceedsPmtAmountTrd = dijit.getEnclosingWidget(document.getElementById('LoanProceedsPmtAmountTrd'))	;
				   if (loanProceedsPmtAmountTrd && !areMultiBeneficiariesAdded){
					   loanProceedsPmtAmountTrd.set("readOnly",isReadOnly);
				   }
				   hideBeneficiaryDetails(false);
		  	  }
			});
	} else if(radName == 'ExportLoanRadio'){
		require(["dojo/dom","dojo/dom-style"], function(dom, domStyle) {
			  domStyle.set(dom.byId("section3.3"),'display', 'none');
			  domStyle.set(dom.byId("section3.2"),'display', 'none');
			  domStyle.set(dom.byId("section3.1"),'display', 'block');
			  domStyle.set(dom.byId("section2.2"),'display', 'block');
              		  domStyle.set(dom.byId("section3.4"),'display', 'none');
			});
		  setMaturityDateReadOnly(noofInvsAttchd);
		  if ('R' == invClassification || isRec){
			  validateMandatoryChargesField();
				   var loanProceedsPmtCurrExp = dijit.getEnclosingWidget(document.getElementById('LoanProceedsPmtCurrExp'))	;
				   if (loanProceedsPmtCurrExp){
					   loanProceedsPmtCurrExp.set("readOnly",isReadOnly);
				   }
				   var loanProceedsPmtAmountExp = dijit.getEnclosingWidget(document.getElementById('LoanProceedsPmtAmountExp'))	;
				   if (loanProceedsPmtAmountExp){
					   loanProceedsPmtAmountExp.set("readOnly",isReadOnly);
				   }
				   hideBeneficiaryDetails(true);

		  } else if ('P' == invClassification || isPay){
			  validateMandatoryChargesField();
			   var loanProceedsPmtCurrExp = dijit.getEnclosingWidget(document.getElementById('LoanProceedsPmtCurrExp'))	;
			   if (loanProceedsPmtCurrExp){
				   loanProceedsPmtCurrExp.set("readOnly",isReadOnly);
			   }
			   var loanProceedsPmtAmountExp = dijit.getEnclosingWidget(document.getElementById('LoanProceedsPmtAmountExp'))	;
			   if (loanProceedsPmtAmountExp){
				   loanProceedsPmtAmountExp.set("readOnly",isReadOnly);
			   }
			   hideBeneficiaryDetails(false);
	  	  }
	    	if (dijit.getEnclosingWidget(document.getElementById('PRESHIPMENT_FINANCING')) && dijit.getEnclosingWidget
	    			(document.getElementById('PRESHIPMENT_FINANCING')).get('checked')||
	     			dijit.getEnclosingWidget(document.getElementById('LoanRequestOTHER')) && dijit.getEnclosingWidget
	    			(document.getElementById('LoanRequestOTHER')).get('checked') ){
	    		
	    				dijit.getEnclosingWidget(document.getElementById('AddTransInvoicesExp')).set("disabled", true);
	    				console.log ('adjusted add invoices button disabled button');
	    				
	    	}else if (dijit.getEnclosingWidget(document.getElementById('AddTransInvoicesExp'))){
	     				dijit.getEnclosingWidget(document.getElementById('AddTransInvoicesExp')).set("disabled", false);
	     				console.log ('adjusted add invoices button enabled button');
	    	}
	    	
	    	 	  
	      	if (dijit.getEnclosingWidget(document.getElementById('InvoicesDetailsTextExp')))
	      		dijit.getEnclosingWidget(document.getElementById('InvoicesDetailsTextExp')).set("readOnly", true);

	} else if(radName == 'ImportLoanRadio'){
		require(["dojo/dom","dojo/dom-style"], function(dom, domStyle) {
			  domStyle.set(dom.byId("section3.3"),'display', 'none');
			  domStyle.set(dom.byId("section3.1"),'display', 'none');
			  domStyle.set(dom.byId("section3.2"),'display', 'block');
			  domStyle.set(dom.byId("section2.2"),'display', 'block');
              		  domStyle.set(dom.byId("section3.4"),'display', 'none');
			});
		}
	else if(radName == 'InvoiceFinancingRadio'){
		require(["dojo/dom","dojo/dom-style", "dijit/registry"], function(dom, domStyle, registry) {
			  domStyle.set(dom.byId("section3.1"),'display', 'none');
			  domStyle.set(dom.byId("section3.2"),'display', 'none');
			  domStyle.set(dom.byId("section3.3"),'display', 'block');
			  domStyle.set(dom.byId("section2.2"),'display', 'block');
              domStyle.set(dom.byId("section3.4"),'display', 'none');
              if(registry.byId("InvoiceFinancingRadio").checked == true){
                  <%if(userSession.isCustNotIntgTPS()){%>
                  		if (registry.getEnclosingWidget(document.getElementById('ReceivablesFinancing'))){
                  			registry.getEnclosingWidget(document.getElementById('ReceivablesFinancing')).set('checked',true);
                  		}  
                  	 	domStyle.set(dom.byId("BuyerFinance"),'display', 'none');
                  		domStyle.set(dom.byId("SellerDetails"),'display', 'none');
                  		domStyle.set(dom.byId("loanProceedSection"),'display', 'none');
                  <%}%>                  
              }
			});
			setMaturityDateReadOnly(noofInvsAttchd);
		  if ('R' == invClassification || isRec){
			  validateMandatoryChargesField();
				   var loanProceedsPmtCurrRec = dijit.getEnclosingWidget(document.getElementById('LoanProceedsPmtCurrRec'))	;
				   if (loanProceedsPmtCurrRec){
					   loanProceedsPmtCurrRec.set("readOnly",isReadOnly);
				   }
				   var loanProceedsPmtAmountRec= dijit.getEnclosingWidget(document.getElementById('LoanProceedsPmtAmountRec'))	;
				   if (loanProceedsPmtAmountRec){
					   loanProceedsPmtAmountRec.set("readOnly",isReadOnly);
				   }
				   var receivablesFinancing = dijit.getEnclosingWidget(document.getElementById('ReceivablesFinancing'))	;
				   if (receivablesFinancing){
					   receivablesFinancing.set("readOnly",isReadOnly);
				   }
			   var buyerRequestedFinancing = dijit.getEnclosingWidget(document.getElementById('BuyerRequestedFinancing'))	;
			   if (buyerRequestedFinancing){
				   buyerRequestedFinancing.set("readOnly",isReadOnly);
			   }
			   var financingBackedByBuyer = dijit.getEnclosingWidget(document.getElementById('FinancingBackedByBuyer'))	;
			   if (financingBackedByBuyer){
				   financingBackedByBuyer.set("readOnly",isReadOnly);
			   }
			   
				   hideBeneficiaryDetails(true);

		  } else if ('P' == invClassification || isPay){
			  validateMandatoryChargesField();
			  var areMultiBeneficiariesAdded =<%=areMultiBeneficiariesAdded%>;
			   var loanProceedsPmtCurrRec = dijit.getEnclosingWidget(document.getElementById('LoanProceedsPmtCurrRec'))	;
			   if (loanProceedsPmtCurrRec && !areMultiBeneficiariesAdded){
				   loanProceedsPmtCurrRec.set("readOnly",isReadOnly);
			   }
			   var loanProceedsPmtAmountRec= dijit.getEnclosingWidget(document.getElementById('LoanProceedsPmtAmountRec'))	;
			   if (loanProceedsPmtAmountRec && !areMultiBeneficiariesAdded){
				   loanProceedsPmtAmountRec.set("readOnly",isReadOnly);
			   }
			   var receivablesFinancing = dijit.getEnclosingWidget(document.getElementById('ReceivablesFinancing'))	;
			   if (receivablesFinancing){
				   receivablesFinancing.set("readOnly",isReadOnly);
			   }
			   var buyerRequestedFinancing = dijit.getEnclosingWidget(document.getElementById('BuyerRequestedFinancing'))	;
			   if (buyerRequestedFinancing){
				   buyerRequestedFinancing.set("readOnly",isReadOnly);
			   }
			   var financingBackedByBuyer = dijit.getEnclosingWidget(document.getElementById('FinancingBackedByBuyer'))	;
			   if (financingBackedByBuyer){
				   financingBackedByBuyer.set("readOnly",isReadOnly);
			   }
			   hideBeneficiaryDetails(false);
	  	  }
		}
   	if (dijit.getEnclosingWidget(document.getElementById('TradeLoanReceivablesRadio'))){
		if (dijit.getEnclosingWidget(document.getElementById('TradeLoanReceivablesRadio')).checked){
			checkTradeLoan(radName);
			<%--  DK IR T36000017608 Rel8.2 06/21/2013 - remove requiredness asterisk from shipment detail fields --%>
		   	<%-- handleRequirdness(radName,noofInvsAttchd);  --%>
		}
	}
   	if (dijit.getEnclosingWidget(document.getElementById('TradeLoanPaybalesRadio'))){
		if (dijit.getEnclosingWidget(document.getElementById('TradeLoanPaybalesRadio')).checked){
			checkTradeLoan(radName);
		}
	}
   	if (dijit.getEnclosingWidget(document.getElementById('ExportLoanRadio'))){
		if (dijit.getEnclosingWidget(document.getElementById('ExportLoanRadio')).checked){
			checkTradeLoan(radName);
		}
	}   	
   	if (dijit.getEnclosingWidget(document.getElementById('InvoiceFinancingRadio'))){
		if (dijit.getEnclosingWidget(document.getElementById('InvoiceFinancingRadio')).checked){
			checkTradeLoan(radName);
		}
	}   	
   	if (dijit.getEnclosingWidget(document.getElementById('ImportLoanRadio'))){
		if (dijit.getEnclosingWidget(document.getElementById('ImportLoanRadio')).checked){
			checkTradeLoan(radName);
		  }
	}   	

   	if (pageReadOnly){
		   if (dijit.getEnclosingWidget(document.getElementById('AddTransInvoicesExp')))
			   dijit.getEnclosingWidget(document.getElementById('AddTransInvoicesExp')).set("disabled", true);
		   if (dijit.getEnclosingWidget(document.getElementById('RemoveTransInvoicesExp')))
			   dijit.getEnclosingWidget(document.getElementById('RemoveTransInvoicesExp')).set("disabled", true);
		   if (dijit.getEnclosingWidget(document.getElementById('AddTransInvoicesINVF')))
			   dijit.getEnclosingWidget(document.getElementById('AddTransInvoicesINVF')).set("disabled", true);
		   if (dijit.getEnclosingWidget(document.getElementById('RemoveTransInvoicesINVF')))
			   dijit.getEnclosingWidget(document.getElementById('RemoveTransInvoicesINVF')).set("disabled", true);
		   if (dijit.getEnclosingWidget(document.getElementById('AddTransInvoicesTL')))
			   dijit.getEnclosingWidget(document.getElementById('AddTransInvoicesTL')).set("disabled", true);
		   if (dijit.getEnclosingWidget(document.getElementById('RemoveTransInvoicesTL')))
			   dijit.getEnclosingWidget(document.getElementById('RemoveTransInvoicesTL')).set("disabled", true);
		   
		   if (dijit.getEnclosingWidget(document.getElementById('CalcDistAndIntrst')))
			   dijit.getEnclosingWidget(document.getElementById('CalcDistAndIntrst')).set("disabled", true);

		   if (dijit.getEnclosingWidget(document.getElementById('saveBenButton')))
			   dijit.getEnclosingWidget(document.getElementById('saveBenButton')).set("disabled", true);

		   if (dijit.getEnclosingWidget(document.getElementById('updateBenButton')))
			   dijit.getEnclosingWidget(document.getElementById('updateBenButton')).set("disabled", true);

		   if (dijit.getEnclosingWidget(document.getElementById('addBeneficaryButton')))
			   dijit.getEnclosingWidget(document.getElementById('addBeneficaryButton')).set("disabled", true);


   	}
	console.log("Inside showideDivs()");
	return false;

    }
    function setMaturityDateReadOnly(noofInvsAttchd){
		  var maturityDate = dijit.getEnclosingWidget(document.getElementById('LoanTermsFixedMaturityDate'))	;
		  var loanTermsType = '<%=loanTermsType%>';
		  if (noofInvsAttchd > 0 && (maturityDate && maturityDate.value != '')){
			  maturityDate.set("readOnly",true);
			   var loanTermsFixedMaturity = dijit.getEnclosingWidget(document.getElementById('TradePortalConstants.LOAN_FIXED_MATURITY'))	;
			   if (loanTermsFixedMaturity){
				   loanTermsFixedMaturity.set("checked",true);
				   loanTermsFixedMaturity.set("readOnly",true);
				} 
			   var loanTermsDaysAfter = dijit.getEnclosingWidget(document.getElementById('TradePortalConstants.LOAN_DAYS_AFTER'))	;
			   if (loanTermsDaysAfter){
				   loanTermsDaysAfter.set("checked",false);
				   loanTermsDaysAfter.set("readOnly",true);
				}  
			   var loanTermsDaysFrom = dijit.getEnclosingWidget(document.getElementById('DaysFrom'))	;
			   if (loanTermsDaysFrom){
				   loanTermsDaysFrom.set("value","");
				   loanTermsDaysFrom.set("readOnly",true);
				}  
			   
		  }else{
			   if (loanTermsType == ''){	
				   maturityDate.set("readOnly",false);
				   var loanTermsFixedMaturity = dijit.getEnclosingWidget(document.getElementById('TradePortalConstants.LOAN_FIXED_MATURITY'))	;
				   if (loanTermsFixedMaturity){
					   loanTermsFixedMaturity.set("checked",false);
					   loanTermsFixedMaturity.set("readOnly",false);
					} 
				   var loanTermsDaysAfter = dijit.getEnclosingWidget(document.getElementById('TradePortalConstants.LOAN_DAYS_AFTER'))	;
				   if (loanTermsDaysAfter){
					   loanTermsDaysAfter.set("checked",false);
					   loanTermsDaysAfter.set("readOnly",false);
					}  
				   var loanTermsDaysFrom = dijit.getEnclosingWidget(document.getElementById('DaysFrom'))	;
				   if (loanTermsDaysFrom){
					   loanTermsDaysFrom.set("value","");
					   loanTermsDaysFrom.set("readOnly",false);
					}  
			   }
		  }

    }
    function handleAddInvoiceButton(state){
    	
    	if (dijit.getEnclosingWidget(document.getElementById('PRESHIPMENT_FINANCING')))
 			console.log (dijit.getEnclosingWidget(document.getElementById('PRESHIPMENT_FINANCING')).get('checked'));
 		
 		
 	   if (state == 'disable' && dijit.getEnclosingWidget(document.getElementById('AddTransInvoicesExp'))){
 		   document.getElementById("add_inv_div_exp").style.display = "block";
		   dijit.getEnclosingWidget(document.getElementById('AddTransInvoicesExp')).set("disabled", true);
 	   }

 	   if (state == 'enable' && dijit.getEnclosingWidget(document.getElementById('AddTransInvoicesExp'))){
 		   document.getElementById("add_inv_div_exp").style.display = "block";
		   dijit.getEnclosingWidget(document.getElementById('AddTransInvoicesExp')).set("disabled", false);
 	   }
 	   
 	    <%--IR 21233- hide the Add Invoice button --%>
 	   if (state == 'hide' && dijit.getEnclosingWidget(document.getElementById('add_inv_div_exp')))
 		  document.getElementById("add_inv_div_exp").style.display = "none";
 	   
 	  
  		if (dijit.getEnclosingWidget(document.getElementById('InvoicesDetailsTextExp')))
  			dijit.getEnclosingWidget(document.getElementById('InvoicesDetailsTextExp')).set("readOnly", true);
    	
  		console.log('handlestate of issue jsp' + state);
    	
    }
	function setBenFieldsReadOnly(isSinglBeneReadOnly){
	   	
		if (isSinglBeneReadOnly){
			if (dijit.getEnclosingWidget(document.getElementById('PaymentMethodType')))
				dijit.getEnclosingWidget(document.getElementById('PaymentMethodType')).set("readOnly",isSinglBeneReadOnly);
			var creditOurAcctTrd = dijit.getEnclosingWidget(document.getElementById('CreditOurAcctTrd'));
			if  (creditOurAcctTrd){
				if (!creditOurAcctTrd.checked){
							
					if (dijit.getEnclosingWidget(document.getElementById('TradePortalConstants.CHARGE_UPLOAD_FW_OURS')))  
						dijit.getEnclosingWidget(document.getElementById('TradePortalConstants.CHARGE_UPLOAD_FW_OURS')).set("readOnly",isSinglBeneReadOnly);
					if (dijit.getEnclosingWidget(document.getElementById('TradePortalConstants.CHARGE_UPLOAD_FW_BEN')))  
						dijit.getEnclosingWidget(document.getElementById('TradePortalConstants.CHARGE_UPLOAD_FW_BEN')).set("readOnly",isSinglBeneReadOnly);
					if (dijit.getEnclosingWidget(document.getElementById('TradePortalConstants.CHARGE_UPLOAD_FW_SHARE')))  
						dijit.getEnclosingWidget(document.getElementById('TradePortalConstants.CHARGE_UPLOAD_FW_SHARE')).set("readOnly",isSinglBeneReadOnly);
				}
			}
	    	
			if (dijit.getEnclosingWidget(document.getElementById('LoanProceedsPmtCurrTrd')))
				dijit.getEnclosingWidget(document.getElementById('LoanProceedsPmtCurrTrd')).set("readOnly",isSinglBeneReadOnly);
	    	
			if (dijit.getEnclosingWidget(document.getElementById('LoanProceedsPmtAmountTrd')))
				dijit.getEnclosingWidget(document.getElementById('LoanProceedsPmtAmountTrd')).set("readOnly",isSinglBeneReadOnly);
	    	
			if (dijit.getEnclosingWidget(document.getElementById('BenNameAmountCCYTrd')))
				dijit.getEnclosingWidget(document.getElementById('BenNameAmountCCYTrd')).set("readOnly",isSinglBeneReadOnly);

			if (dijit.getEnclosingWidget(document.getElementById('BenAmountTrd')))
				dijit.getEnclosingWidget(document.getElementById('BenAmountTrd')).set("readOnly",isSinglBeneReadOnly);

			if (dijit.getEnclosingWidget(document.getElementById('EnteredAccountTrd')))
				dijit.getEnclosingWidget(document.getElementById('EnteredAccountTrd')).set("readOnly",isSinglBeneReadOnly);
	    	
			if (dijit.getEnclosingWidget(document.getElementById('BenNameTrd')))
				dijit.getEnclosingWidget(document.getElementById('BenNameTrd')).set("readOnly",isSinglBeneReadOnly);
	    	
	    	
			if (dijit.getEnclosingWidget(document.getElementById('BenAddressLine1Trd')))
				dijit.getEnclosingWidget(document.getElementById('BenAddressLine1Trd')).set("readOnly",isSinglBeneReadOnly);
	    	
			if (dijit.getEnclosingWidget(document.getElementById('BenAddressLine2Trd')))
				dijit.getEnclosingWidget(document.getElementById('BenAddressLine2Trd')).set("readOnly",isSinglBeneReadOnly);
	    	
			if (dijit.getEnclosingWidget(document.getElementById('BenAddressLine3Trd')))
				dijit.getEnclosingWidget(document.getElementById('BenAddressLine3Trd')).set("readOnly",isSinglBeneReadOnly);
	    	
	    	<%-- dijit.byId("PayeeAddrL4").set("value",""); --%>
	    	
			if (dijit.getEnclosingWidget(document.getElementById('BenCountryTrd')))
				dijit.getEnclosingWidget(document.getElementById('BenCountryTrd')).set("readOnly",isSinglBeneReadOnly);
	    	
			if (dijit.getEnclosingWidget(document.getElementById('BbkNameTrd')))
				dijit.getEnclosingWidget(document.getElementById('BbkNameTrd')).set("readOnly",isSinglBeneReadOnly);
	    	
			if (dijit.getEnclosingWidget(document.getElementById('BbkBranchCodeTrd')))
				dijit.getEnclosingWidget(document.getElementById('BbkBranchCodeTrd')).set("readOnly",isSinglBeneReadOnly);
	    	
			if (dijit.getEnclosingWidget(document.getElementById('BbkAddressLine1Trd')))
				dijit.getEnclosingWidget(document.getElementById('BbkAddressLine1Trd')).set("readOnly",isSinglBeneReadOnly);
	    	
			if (dijit.getEnclosingWidget(document.getElementById('BbkAddressLine2Trd')))
				dijit.getEnclosingWidget(document.getElementById('BbkAddressLine2Trd')).set("readOnly",isSinglBeneReadOnly);
	    	
			if (dijit.getEnclosingWidget(document.getElementById('BbkCityTrd')))
				dijit.getEnclosingWidget(document.getElementById('BbkCityTrd')).set("readOnly",isSinglBeneReadOnly);
	    	
			if (dijit.getEnclosingWidget(document.getElementById('BbkStateProvinceTrd')))
				dijit.getEnclosingWidget(document.getElementById('BbkStateProvinceTrd')).set("readOnly",isSinglBeneReadOnly);
			
			if (dijit.getEnclosingWidget(document.getElementById('BbkSortCodeTrd')))
				dijit.getEnclosingWidget(document.getElementById('BbkSortCodeTrd')).set("readOnly",isSinglBeneReadOnly);
	    	
			if (dijit.getEnclosingWidget(document.getElementById('BbkCountryTrd')))
				dijit.getEnclosingWidget(document.getElementById('BbkCountryTrd')).set("readOnly",isSinglBeneReadOnly);
	    	
			if (dijit.getEnclosingWidget(document.getElementById('BenReferenceTrd')))
				dijit.getEnclosingWidget(document.getElementById('BenReferenceTrd')).set("readOnly",isSinglBeneReadOnly);
	    	
	    	
			if (dijit.getEnclosingWidget(document.getElementById('central_bank_reporting_1')))
				dijit.getEnclosingWidget(document.getElementById('central_bank_reporting_1')).set("readOnly",isSinglBeneReadOnly);
	    	
			if (dijit.getEnclosingWidget(document.getElementById('central_bank_reporting_2')))
				dijit.getEnclosingWidget(document.getElementById('central_bank_reporting_2')).set("readOnly",isSinglBeneReadOnly);
	    	
			if (dijit.getEnclosingWidget(document.getElementById('central_bank_reporting_3')))
				dijit.getEnclosingWidget(document.getElementById('central_bank_reporting_3')).set("readOnly",isSinglBeneReadOnly);
	    	
			if (dijit.getEnclosingWidget(document.getElementById('ReportingCode1')))
				dijit.getEnclosingWidget(document.getElementById('ReportingCode1')).set("readOnly",isSinglBeneReadOnly);
			
			if (dijit.getEnclosingWidget(document.getElementById('ReportingCode2')))
				dijit.getEnclosingWidget(document.getElementById('ReportingCode2')).set("readOnly",isSinglBeneReadOnly);
			
			if (dijit.getEnclosingWidget(document.getElementById('LoanProceedsPmtCurrTrd')))
				dijit.getEnclosingWidget(document.getElementById('LoanProceedsPmtCurrTrd')).set("readOnly",isSinglBeneReadOnly);
	    	
			if (dijit.getEnclosingWidget(document.getElementById('LoanProceedsPmtAmountTrd')))
				dijit.getEnclosingWidget(document.getElementById('LoanProceedsPmtAmountTrd')).set("readOnly",isSinglBeneReadOnly);
	    	

	  		
		}
	}	
 	function handleRequirdness(radio,noofInvsAttchd){
   		require(["dojo/dom","dojo/dom-class","dijit/registry"], function(dom, domClass,registry) {
   
   			var option = false;
   	 		if(radio != 'ImportLoanRadio'&& noofInvsAttchd == 0){ 
   	 			option = true;
  	 		}
    	 	
   	 	if (!isTemplate){
   	 		
  	 		markWidgetMandtory(dijit.getEnclosingWidget(document.getElementById('ShipmentFrom')),domClass,option);
  			markWidgetMandtory(dijit.getEnclosingWidget(document.getElementById('ShipmentFromLoading')),domClass,option);
  			markWidgetMandtory(dijit.getEnclosingWidget(document.getElementById('VesselName')),domClass,option);
  			markWidgetMandtory(dijit.getEnclosingWidget(document.getElementById('ShipmentToDischarge')),domClass,option);
  			markWidgetMandtory(dijit.getEnclosingWidget(document.getElementById('ShipmentTo')),domClass,option);
  			markWidgetMandtory(dijit.getEnclosingWidget(document.getElementById('AirWaybill')),domClass,option);	
			markWidgetMandtory(dijit.getEnclosingWidget(document.getElementById('GoodsDescription')),domClass,option);	   	 		
   	 	}
				

 		});
 	}
 	
 	function handleBeneficiaryRequirdness(loanType,option,loanProceed){
   
 		require(["dojo/dom","dojo/dom-class","dijit/registry"], function(dom, domClass,registry) {
 	   	 	
    	 		
 			if (!isTemplate){
   	  			if (loanType == 'TradeLoan'){
   	 				markWidgetMandtory(dijit.getEnclosingWidget(document.getElementById('BenNameTrd')),domClass,option);
  	  				markWidgetMandtory(dijit.getEnclosingWidget(document.getElementById('BenAddressLine1Trd')),domClass,option);
  	 				markWidgetMandtory(dijit.getEnclosingWidget(document.getElementById('BenCountryTrd')),domClass,option);
  	 				<%-- IR 22749 start --%>
  	 				if(loanProceed=='A'){
  	 				document.TransactionLRQ.BenNameTrd.value="";
  	 				document.TransactionLRQ.EnteredAccountTrd.value="";
  	 				document.TransactionLRQ.BenAddressLine1Trd.value ="";
  	                document.TransactionLRQ.BenAddressLine2Trd.value ="";
  	                document.TransactionLRQ.BenAddressLine3Trd.value ="";
  	                document.TransactionLRQ.BenOTLCustomerIdTrd.value ="";
  	                document.TransactionLRQ.BenReferenceTrd.value ="";
  	                dijit.getEnclosingWidget(document.getElementById('BenCountryTrd')).set("value","");
  	             	document.TransactionLRQ.BbkNameTrd.value ="";
  	             	document.TransactionLRQ.BbkBranchCodeTrd.value ="";
  	            	document.TransactionLRQ.BbkAddressLine1Trd.value ="";
  	         	 	document.TransactionLRQ.BbkAddressLine2Trd.value ="";
  	         	    document.TransactionLRQ.BbkCityTrdTemp.value ="";
  	         		document.TransactionLRQ.BbkCityTrd.value ="";
  	        		dijit.getEnclosingWidget(document.getElementById('BbkCountryTrd')).set("value","");
  	    			document.TransactionLRQ.BbkStateProvinceTrd.value ="";
  	    			document.TransactionLRQ.BbkSortCodeTrd.value ="";
  	    	              
  	  				document.TransactionLRQ.central_bank_reporting_1.value ="";
  	 				document.TransactionLRQ.central_bank_reporting_2.value ="";
  	 				document.TransactionLRQ.central_bank_reporting_3.value ="";
  	 				<%-- Rpasupulati Cr1001 Start --%>
  	 				document.TransactionLRQ.ReportingCode1.value ="";
  	 				document.TransactionLRQ.ReportingCode2.value ="";
  	 				<%-- Rpasupulati Cr1001 End --%>
  	    		
  	 				}
  	 				else if(loanProceed !='A'){
  	 					dijit.getEnclosingWidget(document.getElementById('LoanProceedsCreditAccountTrd')).set("value","");
  	 				}
  	 			    <%-- IR 22749 end --%>
   	  			} 
  	 
  	 			if (loanType == 'ExportLoan'){
    	 			markWidgetMandtory(dijit.getEnclosingWidget(document.getElementById('BenNameExp')),domClass,option);
  	  				markWidgetMandtory(dijit.getEnclosingWidget(document.getElementById('BenAddressLine1Exp')),domClass,option);
  	 				markWidgetMandtory(dijit.getEnclosingWidget(document.getElementById('BenCountryExp')),domClass,option);
   	  			} 

  	 			if (loanType == 'ImportLoan'){
  	 				
    	 			markWidgetMandtory(dijit.getEnclosingWidget(document.getElementById('BenNameExp')),domClass,option);
  	  				markWidgetMandtory(dijit.getEnclosingWidget(document.getElementById('BenAddressLine1Exp')),domClass,option);
  	 				markWidgetMandtory(dijit.getEnclosingWidget(document.getElementById('BenCountryExp')),domClass,option);		
   	  			} 
  	 			
  				if (loanType == 'InvoiceFinance'){
  					
    	 			markWidgetMandtory(dijit.getEnclosingWidget(document.getElementById('BenNameRec')),domClass,option);
  	  				markWidgetMandtory(dijit.getEnclosingWidget(document.getElementById('BenAddressLine1Rec')),domClass,option);
  	 				markWidgetMandtory(dijit.getEnclosingWidget(document.getElementById('BenCountryRec')),domClass,option);
    	  				
  	  			} 
 	   	 	}
   		});
 	}  

 	function markWidgetMandtory(widget,domClass,option){
   		
 		if (widget){
  			if (option){
		         var parentNode = widget.domNode.parentNode;
		         widget.required = true;
		         domClass.add(parentNode, "required");
 	 		}else{
		       var parentNode = widget.domNode.parentNode;
 	           widget.required = false;
 	           domClass.remove(parentNode, "required");
 	 		}
  		}
 	}
 	
 	
 	function pickOurAccountRadioTrd() {
 	    var acct  = dijit.getEnclosingWidget(document.getElementById('LoanProceedsCreditAccountTrd')).value;
	
	    if (acct > '') {
	    	dijit.getEnclosingWidget(document.getElementById('CreditOurAcctTrd')).set("checked", true);
	    }else if(acct > '') {
	    	dijit.getEnclosingWidget(document.getElementById('CreditOurAcctTrd')).set("checked", false);
	    }
	}
    <%-- PICK THE FOLLOWING BENEFICIARY RADIO IF A BENEFICIARY IS SELECTED --%>
    function pickTheFollowingBeneficiaryRadioTrd() {
		var areMultiBeneficiariesAdded =<%=areMultiBeneficiariesAdded%>;
        if ((document.TransactionLRQ.BenNameTrd.value > ''			||
                document.TransactionLRQ.BenAddressLine1Trd.value > ''	||
                document.TransactionLRQ.BenAddressLine2Trd.value > ''	||
                document.TransactionLRQ.BenAddressLine3Trd.value > ''	||
                document.TransactionLRQ.BenOTLCustomerIdTrd.value > ''	||
                document.TransactionLRQ.BenCountryTrd.value > '') && !areMultiBeneficiariesAdded &&
                !document.forms[0].LoanProceedsCreditTypeTrd[2].checked) {
        	
            document.forms[0].LoanProceedsCreditTypeTrd[1].checked = true;
        }
    }
	function setbbkCityTrd(){
		if ((document.TransactionLRQ.BbkCityTrdTemp.value > ''	)){
			var prov = document.TransactionLRQ.BbkStateProvinceTrd.value;
			var city = document.TransactionLRQ.BbkCityTrdTemp.value;
			city = city.replace (" " +prov,"");
			dijit.getEnclosingWidget(document.getElementById('BbkCityTrd')).set("value", city);
			if (document.TransactionLRQ.BbkCityTrdTemp.value != city + "" + prov)
				dijit.getEnclosingWidget(document.getElementById('BbkCityTrdTemp')).set("value", city + " " + prov);
		}
	}
   
    function checkTradeLoan(radName){
    	var isReadOnly =<%=isUpldInvsAttchdLoanType%>;
    	var isRec=<%=isTradeReceivables%>;
    	var isPay=<%=isTradePayables%>;
        if (radName == 'InvoiceFinancingRadio' || radName == 'ImportLoanRadio' || radName == 'ExportLoanRadio')   {
        	if (dijit.getEnclosingWidget(document.getElementById('TradeLoanRadio'))){
        		dijit.getEnclosingWidget(document.getElementById('TradeLoanRadio')).set('checked',false);
        		dijit.getEnclosingWidget(document.getElementById('TradeLoanRadio')).set('readOnly',isReadOnly);
        	}
        	if (dijit.getEnclosingWidget(document.getElementById('TradeLoanReceivablesRadio'))){
        		dijit.getEnclosingWidget(document.getElementById('TradeLoanReceivablesRadio')).set('checked',false);
                dijit.getEnclosingWidget(document.getElementById('TradeLoanReceivablesRadio')).set('readOnly',isReadOnly);
        	}
        	if (dijit.getEnclosingWidget(document.getElementById('TradeLoanPaybalesRadio'))){
        		dijit.getEnclosingWidget(document.getElementById('TradeLoanPaybalesRadio')).set('checked',false);
                dijit.getEnclosingWidget(document.getElementById('TradeLoanPaybalesRadio')).set('readOnly',isReadOnly);        		
        	}
           	if (dijit.getEnclosingWidget(document.getElementById('TradeLoanRadio'))){
        		dijit.getEnclosingWidget(document.getElementById('TradeLoanRadio')).set('readOnly',isReadOnly);
        	}
           	if (dijit.getEnclosingWidget(document.getElementById('ImportLoanRadio'))){
        		dijit.getEnclosingWidget(document.getElementById('ImportLoanRadio')).set('readOnly',isReadOnly);
        	}
           	if (dijit.getEnclosingWidget(document.getElementById('ExportLoanRadio'))){
        		dijit.getEnclosingWidget(document.getElementById('ExportLoanRadio')).set('readOnly',isReadOnly);
        	}
           	if (dijit.getEnclosingWidget(document.getElementById('InvoiceFinancingRadio'))){
        		dijit.getEnclosingWidget(document.getElementById('InvoiceFinancingRadio')).set('readOnly',isReadOnly);
        	}

        } else  if (radName == 'TradeLoanRadio' ) {
        	if (dijit.getEnclosingWidget(document.getElementById('TradeLoanRadio'))){
        		dijit.getEnclosingWidget(document.getElementById('TradeLoanRadio')).set('checked',true);
        		dijit.getEnclosingWidget(document.getElementById('TradeLoanRadio')).set('readOnly',isReadOnly);
        		dijit.getEnclosingWidget(document.getElementById('TradeLoanReceivablesRadio')).set('readOnly',isReadOnly);
        		dijit.getEnclosingWidget(document.getElementById('TradeLoanPaybalesRadio')).set('readOnly',isReadOnly);
        	}
        	if (dijit.getEnclosingWidget(document.getElementById('ExportLoanRadio'))){
        		dijit.getEnclosingWidget(document.getElementById('ExportLoanRadio')).set('checked',false);
        		dijit.getEnclosingWidget(document.getElementById('ExportLoanRadio')).set('readOnly',isReadOnly);
        	}
        	if (dijit.getEnclosingWidget(document.getElementById('ImportLoanRadio'))){
        		dijit.getEnclosingWidget(document.getElementById('ImportLoanRadio')).set('checked',false);
        		dijit.getEnclosingWidget(document.getElementById('ImportLoanRadio')).set('readOnly',isReadOnly);
        	}
        	if (dijit.getEnclosingWidget(document.getElementById('InvoiceFinancingRadio'))){
        		dijit.getEnclosingWidget(document.getElementById('InvoiceFinancingRadio')).set('checked',false);
        		dijit.getEnclosingWidget(document.getElementById('InvoiceFinancingRadio')).set('readOnly',isReadOnly);
        	}
        	
        	
        }
    	
    	
		   var transactionCurr = dijit.getEnclosingWidget(document.getElementById('TransactionCurrency'))	;
		   if (transactionCurr){
			   transactionCurr.set("readOnly",isReadOnly);
		   }
		   var transactionAmount = dijit.getEnclosingWidget(document.getElementById('TransactionAmount'))	;
		   if (transactionAmount){
			   transactionAmount.set("readOnly",isReadOnly);
		   }

    }
    
    function hideBeneficiaryDetails(show){
    		if (show){
				   if (document.getElementById('ben_details')){
					   document.getElementById("ben_details").style.display = "none";
					   
				   }
				   if (document.getElementById('benBank_details')){
					   document.getElementById("benBank_details").style.display = "none";
					   
				   }
				   if (document.getElementById('central_bank_reporting_div')){
					   document.getElementById("central_bank_reporting_div").style.display = "none";
					   
				   }	
				   if (document.getElementById('reporting_code_div')){
					   document.getElementById("reporting_code_div").style.display = "none";
					   
				   }	
				   if (document.getElementById('addBeneficaryButton'))
					   document.getElementById('addBeneficaryButton').disabled = true;
					   if (document.getElementById('update_ben_button_div'))
						   document.getElementById("update_ben_button_div").style.display = "none";
					   if (document.getElementById('save_ben_button_div'))
						   document.getElementById("save_ben_button_div").style.display = "none";	
					   if (document.getElementById('cancel_ben_link_div'))
						   document.getElementById("cancel_ben_link_div").style.display = "none"; 


   		}
			else {
				   if (document.getElementById('ben_details')){
					   document.getElementById("ben_details").style.display = "block";
					   
				   }
				   if (document.getElementById('benBank_details')){
					   document.getElementById("benBank_details").style.display = "block";
					   
				   }
				   if (document.getElementById('central_bank_reporting_div')){
					   document.getElementById("central_bank_reporting_div").style.display = "block";
					   
				   }
				   if (document.getElementById('reporting_code_div')){
					   document.getElementById("reporting_code_div").style.display = "block";
					   
				   }

				   var  multiBen = document.getElementById("CreditMultiBenAccountTrd");
				   var isMultiBenInvoicesAttached =<%=isMultiBenInvoicesAttached%>;
				   
				   if ((multiBen && multiBen.checked) && !isMultiBenInvoicesAttached){
					   if (document.getElementById('addBeneficaryButton'))
						   document.getElementById('addBeneficaryButton').disabled = false;
					   if (document.getElementById('update_ben_button_div'))
						   document.getElementById("update_ben_button_div").style.display = "none";
					   if (document.getElementById('save_ben_button_div'))
						   document.getElementById("save_ben_button_div").style.display = "block";	
					   if (document.getElementById('cancel_ben_link_div'))
						   document.getElementById("cancel_ben_link_div").style.display = "block"; 
				   } else {
					   if (isMultiBenInvoicesAttached){
						   setBenFieldsReadOnly(true);
						   if (dijit.getEnclosingWidget(document.getElementById('CreditBenAccountTrd')))
							   	dijit.getEnclosingWidget(document.getElementById('CreditBenAccountTrd')).set("readOnly",true);
						   if (dijit.getEnclosingWidget(document.getElementById('CreditMultiBenAccountTrd')))
							   	dijit.getEnclosingWidget(document.getElementById('CreditMultiBenAccountTrd')).set("readOnly",true);
					   }
					   if (document.getElementById('addBeneficaryButton'))
						   document.getElementById('addBeneficaryButton').disabled = true;
					   if (document.getElementById('update_ben_button_div'))
						   document.getElementById("update_ben_button_div").style.display = "none";
					   if (document.getElementById('save_ben_button_div'))
						   document.getElementById("save_ben_button_div").style.display = "none";	
					   if (document.getElementById('cancel_ben_link_div'))
						   document.getElementById("cancel_ben_link_div").style.display = "none"; 
					   
				   }
				   var  singleBen = document.getElementById("CreditBenAccountTrd");
				   var isSingleBenInvoicesAttached =<%=isSingleBenInvoicesAttached%>;
				   if ((singleBen && singleBen.checked) && isSingleBenInvoicesAttached){
					   
					   if (!isAnyFieldBlank()){
							setBenFieldsReadOnly(false); 
					   }else{ 
					   		setBenFieldsReadOnly(true);
					   }
					   if (dijit.getEnclosingWidget(document.getElementById('CreditBenAccountTrd')))
					   	dijit.getEnclosingWidget(document.getElementById('CreditBenAccountTrd')).set("readOnly",true);
					   if (dijit.getEnclosingWidget(document.getElementById('CreditMultiBenAccountTrd')))
						dijit.getEnclosingWidget(document.getElementById('CreditMultiBenAccountTrd')).set("readOnly",true);


				   }
				   var  creditOurAcct = document.getElementById("CreditOurAcctTrd");
				   
				   if ((creditOurAcct && creditOurAcct.checked) ){
					   	var isReadOnly =<%=isUpldInvsAttchdLoanType%>;
				    	var isPay=<%=isTradePayables%>;
					   if (isReadOnly && isPay){
						   if (dijit.getEnclosingWidget(document.getElementById('CreditBenAccountTrd')))
							   	dijit.getEnclosingWidget(document.getElementById('CreditBenAccountTrd')).set("readOnly",!isSingleBenInvoicesAttached);
						   if (dijit.getEnclosingWidget(document.getElementById('CreditMultiBenAccountTrd')))
							dijit.getEnclosingWidget(document.getElementById('CreditMultiBenAccountTrd')).set("readOnly",true);
						   
					   }						   
					   if (document.getElementById('ben_details')){
						   document.getElementById("ben_details").style.display = "none";
						   
					   }
					   if (document.getElementById('benBank_details')){
						   document.getElementById("benBank_details").style.display = "none";
						   
					   }
					   if (document.getElementById('central_bank_reporting_div')){
						   document.getElementById("central_bank_reporting_div").style.display = "none";
						   
					   }
					   if (document.getElementById('reporting_code_div')){
						   document.getElementById("reporting_code_div").style.display = "none";
						   
					   }
					   
					   if (document.getElementById('addBeneficaryButton'))
						   document.getElementById('addBeneficaryButton').disabled = true;
					   if (document.getElementById('update_ben_button_div'))
						   document.getElementById("update_ben_button_div").style.display = "none";
					   if (document.getElementById('save_ben_button_div'))
						   document.getElementById("save_ben_button_div").style.display = "none";	
					   if (document.getElementById('cancel_ben_link_div'))
						   document.getElementById("cancel_ben_link_div").style.display = "none"; 


				   }

			}
     }
  function isAnyFieldBlank(){
	 var  retVal = true;
		var   benField = document.getElementById("PaymentMethodType");
		  if (benField && benField.value == ''){
		   retVal = false;
		  }
		benField = document.getElementById("BenNameTrd");
		  if (benField && benField.value == ''){
		   retVal = false;
		  }
		benField = document.getElementById("BenAddressLine1Trd");
		  if (benField && benField.value == ''){
		   retVal = false;
		  }
		benField = document.getElementById("BenAddressLine2Trd");
		  if (benField && benField.value == ''){
		   retVal = false;
		  }
		benField = document.getElementById("BenAddressLine3Trd");
		  if (benField && benField.value == ''){
		   retVal = false;
		  }
		benField = document.getElementById("BenCountryTrd");
		  if (benField && benField.value == ''){
		   retVal = false;
		  }
		benField = document.getElementById("BenReferenceTrd");
		  if (benField && benField.value == ''){
		   retVal = false;
		  }
		benField = document.getElementById("BbkNameTrd");
		  if (benField && benField.value == ''){
		   retVal = false;
		  }
		benField = document.getElementById("BbkAddressLine1Trd");
		  if (benField && benField.value == ''){
		   retVal = false;
		  }
		benField = document.getElementById("BbkAddressLine2Trd");
		  if (benField && benField.value == ''){
		   retVal = false;
		  }
		benField = document.getElementById("BbkCityTrd");
		  if (benField && benField.value == ''){
		   retVal = false;
		  }
		benField = document.getElementById("BbkCountryTrd");
		  if (benField && benField.value == ''){
		   retVal = false;
		  }
		benField = document.getElementById("central_bank_reporting_1");
		  if (benField && benField.value == ''){
		   retVal = false;
		  }
		benField = document.getElementById("central_bank_reporting_2");
		  if (benField && benField.value == ''){
		   retVal = false;
		  }
		benField = document.getElementById("central_bank_reporting_3");
		  if (benField && benField.value == ''){
		   retVal = false;
		  }
		benField = document.getElementById("ReportingCode1");
		  if (benField && benField.value == ''){
		   retVal = false;
		  }
		benField = document.getElementById("ReportingCode2");
		  if (benField && benField.value == ''){
		   retVal = false;
		  }
		  return retVal ;
  }

</script>
<script>
var isReadOnly = <%=isReadOnly%>;<%-- Checks done in case of readOnly mode, so that the On functions are not trigerred which are leading to js errors --%>
var tradeLoanAllowed = '<%=orgTradeLoanType%>';
var importLoanAllowed = '<%=importLoanType%>';
var exportLoanAllowed = '<%=exportLoanType%>';
var invoiceFinancingAllowed = '<%=invoiceFinancingType%>';
var isTemplate = <%=isTemplate%>;
var y = "Y";
  require(["dijit/registry", "dojo/on", "dojo/ready"],
      function(registry, on, ready) {
    ready( function(dom){
    	   
		updateReportingCodes();
    	if(dijit.byId("ExportLoanRadio") && (y == exportLoanAllowed ) && dijit.byId("ExportLoanRadio").checked == true){
    		showHideDivs('ExportLoanRadio');
    	}else if(dijit.byId("ImportLoanRadio") && (y == importLoanAllowed) && dijit.byId("ImportLoanRadio").checked == true){
    		showHideDivs('ImportLoanRadio');
    	}else if(dijit.byId("InvoiceFinancingRadio") && (y == invoiceFinancingAllowed ) && dijit.byId("InvoiceFinancingRadio").checked == true){
    		showHideDivs('InvoiceFinancingRadio');
        }else if(dijit.byId("TradeLoanRadio")  && (y == tradeLoanAllowed) && dijit.byId("TradeLoanRadio").checked == true){
            showHideDivs('TradeLoanRadio');
    	} else {
    		if ((y == exportLoanAllowed )){
    			showHideDivs('ExportLoanRadio');
    		}
    		if ((y == importLoanAllowed)){
    			showHideDivs('ImportLoanRadio');
    		}
    		if ((y == invoiceFinancingAllowed )){
    			showHideDivs('InvoiceFinancingRadio');
    		}
    		if ((y == tradeLoanAllowed)){
    			showHideDivs('TradeLoanRadio');
    		}
    	}    	
    });
  });
  
  require(["dijit/registry", "dojo/on", "dojo/ready"],
	      function(registry, on, ready) {
	  	ready(function(){
	  		var LoanMaturityDebitOtherTextExp=registry.byId("LoanMaturityDebitOtherTextExp");
			var OAD=registry.byId("OAD");
			if(!isReadOnly){ <%-- This check is to make sure that in readOnly mode, the On functions are not trigerred which are leading to js errors --%>
				on(LoanMaturityDebitOtherTextExp, "change", function(){
					OAD.set("checked",true);
				},true);
			}
	  	});
	  
  });
  require(["dijit/registry", "dojo/on", "dojo/ready"],
	      function(registry, on, ready) {
	  	ready(function(){
	  		var UseOtherText= registry.byId("UseOtherText");
			var ProceedsUseOther=registry.byId("ProceedsUseOther");
			if(!isReadOnly){
				on(UseOtherText, "change", function(){
					ProceedsUseOther.set("checked",true);
				},true);
			}
	  	});
		
	  
  });
  
  
    require(["dijit/registry", "dojo/on", "dojo/ready"],
	      function(registry, on, ready) {
	  	ready(function(){
	  		var MaturityUseOtherText= dijit.byId("MaturityUseOtherText");
			var MaturityUseOther=dijit.byId("MaturityUseOther");
			if(!isReadOnly){
				on(MaturityUseOtherText, "change", function(){
					MaturityUseOther.set("checked",true);
				},true);
			}
	  	});
	  
  });
    require(["dijit/registry", "dojo/on", "dojo/ready"],
  	      function(registry, on, ready) {
  	  	ready(function(){
  	  		var LoanMaturityDebitAccountRec= registry.byId("LoanMaturityDebitAccountRec");
  			var DebitApp1=registry.byId("DebitApp1");
  			if(!isReadOnly){
	  			on(LoanMaturityDebitAccountRec, "change", function(){
	  				DebitApp1.set("checked",true);
	  				
	  			},true);
  			}
  	  	});
  		
  	  
    });
    
    
      require(["dijit/registry", "dojo/on", "dojo/ready"],
  	      function(registry, on, ready) {
  	  	ready(function(){
  	  		var LoanMaturityDebitOtherTextRec= registry.byId("LoanMaturityDebitOtherTextRec");
  			var DebitOther1=registry.byId("DebitOther1");
  			if(!isReadOnly){
	  			on(LoanMaturityDebitOtherTextRec, "change", function(){
	  				DebitOther1.set("checked",true);
	  				
	  			},true);
  			}
  	  	});
  	  
    });
      
      require(["dijit/registry", "dojo/on", "dojo/ready"],
    	      function(registry, on, ready) {
    	  	ready(function(){
    	  		var FinancingBackedByBuyer= registry.byId("FinancingBackedByBuyer");
    			var BuyerRequestedFinancing=registry.byId("BuyerRequestedFinancing");
    			if(!isReadOnly){
	    			on(FinancingBackedByBuyer, "change", function(){
	    				BuyerRequestedFinancing.set("checked",true);
	    			},true);
    			}
    	  	});
    	  
      });
      require(["dijit/registry", "dojo/on", "dojo/ready"],
    	      function(registry, on, ready) {
    	  	ready(function(){
    	  		var LoanProceedsCreditAccountExp= registry.byId("LoanProceedsCreditAccountExp");
    			var CAY=registry.byId("CAY");
    			if(!isReadOnly){
	    			on(LoanProceedsCreditAccountExp, "change", function(){
	    				CAY.set("checked",true);
	    			},true);
    			}
    	  	});
    	  
      });
      require(["dijit/registry", "dojo/on", "dojo/ready"],
    	      function(registry, on, ready) {
    	  	ready(function(){
    	  		var LoanTermsFixedMaturityDate= registry.byId("LoanTermsFixedMaturityDate");
    			var LOAN_FIXED_MATURITY=registry.byId("TradePortalConstants.LOAN_FIXED_MATURITY");
    			if(!isReadOnly){
	    			on(LoanTermsFixedMaturityDate, "change", function(){
	    				LOAN_FIXED_MATURITY.set("checked",true);
	    			},true);
    			}
    	  	});
    	  
      });
      
      require(["dijit/registry", "dojo/on", "dojo/ready"],
    	      function(registry, on, ready) {
    	  	ready(function(){
    	  		var DaysFrom= registry.byId("DaysFrom");
    			var LOAN_DAYS_AFTER=registry.byId("TradePortalConstants.LOAN_DAYS_AFTER");
    			if(!isReadOnly){
	    			on(DaysFrom, "change", function(){
	    				if (!isNaN(DaysFrom.value))
	    					LOAN_DAYS_AFTER.set("checked",true);
	    				else
	    					LOAN_DAYS_AFTER.set("checked",false);
	    			},true);
    			}
    	  	});
            });
  <%-- populate Grid Data to form --%>
    require(["dojo/ready","dijit/registry","dojo/_base/array"], function(ready,registry,baseArray) {
    	
    	 ready(function(){ 
    		 	 if (registry.byId("TransactionLRQTradeLoanISSDataGridId")) {
    			myGrid = registry.byId("TransactionLRQTradeLoanISSDataGridId");
    			
    		  var clearBen = false;
    		    myGrid.on("SelectionChanged", function(){
    		        <%--   this will be an array of dojo/data items --%>
    		        var items = this.selection.getSelected();
    		       console.log("items : "+items);

    		        <%--   get the data of each column of selected grid and set the data to fields in the form --%>
    		        baseArray.forEach(items, function(item){		        	   
    		        	
    		        	    console.log("rowKey : "+this.store.getValue(item, "rowKey"));
    		        	    console.log("PayeeName_List : "+this.store.getValue(item, "PayeeName_List"));
    		        	    console.log("hidden :"+dijit.byId("InvoicePaymentInstructionsOID").value);
    		        	    console.log(item);
    		        	    dijit.byId("InvoicePaymentInstructionsOID").set("value",this.store.getValue(item, "rowKey"));
    		        	    dijit.byId('InvoicePaymentInstructionsOID').set('checked', true);
    		        	  
    		        	   setBeneficiaryDetails(myGrid, item);
    		        	  
    		        }, this);
    		     	if (items.length == 0 && dijit.byId("InvoicePaymentInstructionsOID").value  != ""){
    		     		clearAllBeneficiaryFormDetails();
    		     	}
    		     		
    		    }, true);
    		 	 if (clearBen){
     		     	
    		     		
    		 	 }
    		    <%-- Used to fetch the grid's selected row. --%>
    		    dojo.connect(myGrid,"_onFetchComplete",function(items){ 

    		    	if("" != dijit.byId("InvoicePaymentInstructionsOID").value){
    			    	 dojo.forEach(items, function(i){
    	                     if(dijit.byId("InvoicePaymentInstructionsOID").value == myGrid.store.getValue(i, "rowKey")){	                    	
    	                     
    	                    	 myGrid.selection.addToSelection(myGrid.getItemIndex(i)); 
    	                    	 dijit.byId("InvoicePaymentInstructionsOID").set("value",myGrid.store.getValue(i, "rowKey"));
    	                     }
    	                 });
    		    	}
    	         }); 
    		    
    		    if(<%=numberOfDomPmts%> >= 1){
    		    	document.getElementById("add_ben_div").style.display = "block";		    	
    		    }else if(<%=numberOfDomPmts%> == 0){
    		    	document.getElementById("add_ben_div").style.display = "none";		    	
    		    }
    			

    		    if("" == dijit.byId("InvoicePaymentInstructionsOID").value){	    
    		    	document.getElementById("update_ben_button_div").style.display = "none";
            	   	document.getElementById("save_ben_button_div").style.display = "block";
            	   	dijit.byId("addBeneficaryButton").setAttribute('disabled', true);
    		    }else{
    		    	document.getElementById("update_ben_button_div").style.display = "block";
            	   	document.getElementById("save_ben_button_div").style.display = "none";
            	   	dijit.byId("addBeneficaryButton").setAttribute('disabled', false);
    		    }
    		 	 } 
    		 	 if (registry.byId("TransactionLRQTradeLoanISSMultiBenINVsDataGridId")) {
    	    			myGrid = registry.byId("TransactionLRQTradeLoanISSMultiBenINVsDataGridId");
    	    			
    	    		
    	    		    myGrid.on("SelectionChanged", function(){
    	    		        <%--   this will be an array of dojo/data items --%>
    	    		        var items = this.selection.getSelected();
    	    		       console.log("items : "+items);
    	    		     
    	    		        <%--   get the data of each column of selected grid and set the data to fields in the form --%>
    	    		        baseArray.forEach(items, function(item){		        	   
    	    		        	
    	    		        	    console.log("rowKey : "+this.store.getValue(item, "rowKey"));
    	    		        	    console.log("PayeeName_List : "+this.store.getValue(item, "PayeeName_List"));
    	    		        	    console.log("hidden :"+dijit.byId("InvoicePaymentInstructionsOID").value);
    	    		        	    console.log(item);
      	    		        	   setBeneficiaryDetails(myGrid, item);
    	    		        	  
    	    		        }, this);
    	    		    }, true);
    	    		 	 
    	    		    <%-- Used to fetch the grid's selected row. --%>
    	    		    dojo.connect(myGrid,"_onFetchComplete",function(items){ 

    	    		    	if("" != dijit.byId("InvoicePaymentInstructionsOID").value){
    	    			    	 dojo.forEach(items, function(i){
    	    	                     if(dijit.byId("InvoicePaymentInstructionsOID").value == myGrid.store.getValue(i, "rowKey")){	                    	
    	    	                     
    	    	                    	 myGrid.selection.addToSelection(myGrid.getItemIndex(i)); 
    	    	                    	
    	    	                     }
    	    	                 });
    	    		    	}
    	    	         }); 
    	    		    
    	      	  	 	 } 
      });
         
       });
  
    function filterPayments(buttonId) {
    	
		console.log("Inside filterPayments()");
		
    require(["dojo/dom"],
      function(dom){
        
        var searchParms="transaction_oid=<%=transaction.getAttribute("transaction_oid")%>";
       
        var benificiaryName = dom.byId("BenificiaryName").value;
        var benificiaryBankName = dom.byId("BenificiaryBankName").value;;
        var amount = dom.byId("BeneficiaryAmount").value;
        
            if(buttonId=="ModifyPayee"){
		       	searchParms+="&benificiaryName="+benificiaryName.toUpperCase();
		       	searchParms+="&benificiaryBankName="+benificiaryBankName.toUpperCase();
		       	searchParms+="&beneficiaryAmount="+amount;
	        }
	    

        console.log("searchParms="+searchParms);
	 	 if (dom.byId("TransactionLRQTradeLoanISSDataGridId")) {

			searchDataGrid("TransactionLRQTradeLoanISSDataGridId", "TransactionLRQTradeLoanISSDataView", searchParms);
	 	 }else if (dom.byId("TransactionLRQTradeLoanISSMultiBenINVsDataGridId")) {
	 		searchDataGrid("TransactionLRQTradeLoanISSMultiBenINVsDataGridId", "TransactionLRQTradeLoanISSMultiBenINVsDataView", searchParms);
	 	 }
		
		console.log("Query fired");	        
	});
}
    
  <%-- Delete Beneficiary details --%>
    function deleteBeneficiaryRow(){
    	
    	var   confirmMessage = "<%=resMgr.getText("FutureValueTransactions.PopupMessage", 
    	TradePortalConstants.TEXT_BUNDLE) %>";
    		if (!confirm(confirmMessage)) 
    		{
    			return false;
    		}		
    	var delRow = getSelectedGridRowKeys("TransactionLRQTradeLoanISSDataGridId");
    	 
    	dijit.byId("InvoicePaymentInstructionsOID").set("value",delRow);
    	setButtonPressed('<%=TradePortalConstants.BUTTON_DELETE_INV_PAYEE%>', 0); 
    	document.forms[0].submit(); 
    	
    }


    var myGrid = "";
                                    
    <%-- Used to clear beneficiary details --%>
    function clearAllBeneficiaryFormDetails(){ 
  	  require(["dojo/dom", "dijit/registry", "t360/textFormatter"],
    	      function( dom, registry, txtFmt ) {
   	
    	<% 
  	  if (isReadOnly ) {
  	%>
  		dom.byId("PaymentMethodType").innerHTML="";
  		

      		registry.byId('TradePortalConstants.CHARGE_UPLOAD_FW_OURS').set('checked', false);

      		registry.byId('TradePortalConstants.CHARGE_UPLOAD_FW_BEN').set('checked', false);
      		registry.byId('TradePortalConstants.CHARGE_UPLOAD_FW_SHARE').set('checked', false);
 		
  		dom.byId("LoanProceedsPmtAmountTrd").innerHTML="";
  		document.getElementById("LoanProceedsPmtCurrTrd").innerHTML = "";
  		dom.byId("BenReferenceTrd").innerHTML="";
 		
  		if (dom.byId("EnteredAccountTrd")) dom.byId("EnteredAccountTrd").innerHTML="";
  		dom.byId("BenNameTrd").innerHTML="";
  		dom.byId("BenAddressLine1Trd").innerHTML="";
  		dom.byId("BenAddressLine2Trd").innerHTML="";
  		dom.byId("BenAddressLine3Trd").innerHTML="";
  		if (dom.byId("BenCountryTrd")) dom.byId("BenCountryTrd").innerHTML="";
  		
  		if (dom.byId("BbkBranchCodeTrd")) dom.byId("BbkBranchCodeTrd").innerHTML="";

  		var benBank_name = "";
  		var benBank_adl1 = "";
  		var benBank_adl2 = "";
  		var benBank_city = "";
  		var benBank_country = "";
  		var benBank_branch_StPrvnc = "";
	    	dom.byId("BbkNameTrd").innerHTML = benBank_name;
	        dom.byId("BbkAddressLine1Trd").innerHTML = benBank_adl1;
        	dom.byId("BbkAddressLine2Trd").innerHTML = benBank_adl2;
	        dom.byId("BbkCityTrd").innerHTML = benBank_city;
	        dom.byId("BbkCountryTrd").innerHTML = benBank_country;
        	dom.byId("BbkStateProvinceTrd").innerHTML = benBank_branch_StPrvnc;
        	dom.byId("BbkSortCodeTrd").innerHTML = "";
  	              
  		dom.byId("central_bank_reporting_1").innerHTML="";
  		dom.byId("central_bank_reporting_2").innerHTML="";
  		dom.byId("central_bank_reporting_3").innerHTML="";
  		dom.byId("ReportingCode1").innerHTML="";
  		dom.byId("ReportingCode2").innerHTML="";
  		
  		
  		dom.byId("InvoicePaymentInstructionsOID").innerHTML="";
  		
  		
  	<% } else { %>
    	dijit.byId("PaymentMethodType").set("value","");
    	
    	dijit.byId('TradePortalConstants.CHARGE_UPLOAD_FW_OURS').set('checked', false);
    	dijit.byId('TradePortalConstants.CHARGE_UPLOAD_FW_BEN').set('checked', false);
    	dijit.byId('TradePortalConstants.CHARGE_UPLOAD_FW_SHARE').set('checked', false);
    	
    	dijit.byId("LoanProceedsPmtCurrTrd").set("value","");
    	dijit.byId("LoanProceedsPmtAmountTrd").set("value","");
    	dijit.byId("BenNameAmountCCYTrd").set("value","");
    	dijit.byId("BenAmountTrd").set("value","");

    	dijit.byId("EnteredAccountTrd").set("value","");
    	dijit.byId("BenNameTrd").set("value","");
    	
    	dijit.byId("BenAddressLine1Trd").set("value","");
    	dijit.byId("BenAddressLine2Trd").set("value","");
    	dijit.byId("BenAddressLine3Trd").set("value","");	
    	dijit.byId("BenReferenceTrd").set("value","");
    	
    	dijit.byId("BenCountryTrd").set("value","");
    	dijit.byId("BbkNameTrd").set("value","");
    	dijit.byId("BbkBranchCodeTrd").set("value","");
    	dijit.byId("BbkAddressLine1Trd").set("value","");
    	dijit.byId("BbkAddressLine2Trd").set("value","");
    	dijit.byId("BbkCityTrd").set("value","");	
    	dijit.byId("BbkStateProvinceTrd").set("value","");
    	dijit.byId("BbkSortCodeTrd").set("value","");
    	dijit.byId("BbkCountryTrd").set("value","");
    	
    	dijit.byId("central_bank_reporting_1").set("value","");
    	dijit.byId("central_bank_reporting_2").set("value","");
    	dijit.byId("central_bank_reporting_3").set("value","");
    	dijit.byId("ReportingCode1").set("value","");
    	dijit.byId("ReportingCode2").set("value","");
    	dijit.byId("BenReferenceTrd").set("value","");
    	dijit.byId("central_bank_reporting_3").set("value","");
    	
    	dijit.byId("InvoicePaymentInstructionsOID").set("value","");
    	
    	dijit.byId("TransactionLRQTradeLoanISSDataGridId").selection.clear();
    	dijit.byId("LoanProceedsPmtCurrTrd").set("readOnly",false);
    	dijit.byId("LoanProceedsPmtAmountTrd").set("readOnly",false);

    	document.getElementById("update_ben_button_div").style.display = "none";
       	document.getElementById("save_ben_button_div").style.display = "block";
    	dijit.byId("addBeneficaryButton").setAttribute('disabled', true);
       	
		<% } %>
	  });    	
    }
    
    function setBeneficiaryDetails(grid, benJsObj){
    	


    var rptgCodes1 =grid.store.getValue(benJsObj, "ReportingCode1");
    var rptgCodes2 =grid.store.getValue(benJsObj, "ReportingCode2");
      

    	 require(["dojo/_base/xhr","t360/common","dojo/_base/array","dijit/registry","dojo/dom-construct" ], function(xhr, common, arrayUtil, registry,domConstruct) {
    	 var remoteUrl  = "/portal/transactions/Transaction-LRQ-RepCodeDetail.jsp?rptgCodes1=" + rptgCodes1 + "&rptgCodes2=" + rptgCodes2 + "&bankgOid=" + <%=bankGroupOid%>+ "&";
      	 xhr.get({
    	              url: remoteUrl,
    				  handleAs: "json",
    				  load: function(data) {

    				populateBenDetails(grid, benJsObj, data);
    			},
    			 error: function(error){
    			    <%-- todo --%>
    			 }
    		});
    			
    		
    	
    		
    		});
    }
 <%--  Rel 9.2 - IR T36000036836 - Added encoding for all readonly fields of Beneficiary details --%>
    function populateBenDetails(grid, benJsObj,data){
    	  require(["dojo/dom", "dijit/registry", "t360/textFormatter", "dojox/html/entities"],
    	      function( dom, registry, txtFmt, entities ) {

    	    var seq_num = "";
    	   
    	    var rowKey=grid.store.getValue(benJsObj, "rowKey");
    	 
    	<% 
    	  if (isReadOnly ) {
    	%>
    		dom.byId("PaymentMethodType").innerHTML=entities.encode(grid.store.getValue(benJsObj, "PaymentMethod_List"));
    		
        	if(grid.store.getValue(benJsObj, "BANK_CHARGES_TYPE")=='<%=TradePortalConstants.CHARGE_UPLOAD_FW_OURS%>'){
        		registry.byId('TradePortalConstants.CHARGE_UPLOAD_FW_OURS').set('checked', true);
        	}else if(grid.store.getValue(benJsObj, "BANK_CHARGES_TYPE")=='<%=TradePortalConstants.CHARGE_UPLOAD_FW_BEN%>'){
        		registry.byId('TradePortalConstants.CHARGE_UPLOAD_FW_BEN').set('checked', true);
        	}else if(grid.store.getValue(benJsObj, "BANK_CHARGES_TYPE")=='<%=TradePortalConstants.CHARGE_UPLOAD_FW_SHARE%>'){
        		registry.byId('TradePortalConstants.CHARGE_UPLOAD_FW_SHARE').set('checked', true);
        	}
   		
    		dom.byId("LoanProceedsPmtAmountTrd").innerHTML=entities.encode(grid.store.getValue(benJsObj, "Amount_List"));
    		document.getElementById("LoanProceedsPmtCurrTrd").innerHTML = entities.encode(grid.store.getValue(benJsObj, "Currency_List"));
    		dom.byId("BenReferenceTrd").innerHTML=entities.encode(grid.store.getValue(benJsObj, "CUSTOMER_REFERENCE"));
     		if (dom.byId("EnteredAccountTrd")) dom.byId("EnteredAccountTrd").innerHTML=entities.encode(grid.store.getValue(benJsObj, "AccountNumber_List"));
    		dom.byId("BenNameTrd").innerHTML=entities.encode(grid.store.getValue(benJsObj, "PayeeName_List"));
    		dom.byId("BenAddressLine1Trd").innerHTML=entities.encode(grid.store.getValue(benJsObj, "BenAddressOne"));
    		dom.byId("BenAddressLine2Trd").innerHTML=entities.encode(grid.store.getValue(benJsObj, "BenAddressTwo"));
    		dom.byId("BenAddressLine3Trd").innerHTML=entities.encode(grid.store.getValue(benJsObj, "BenAddressThree"));
    		if (dom.byId("BenCountryTrd")) dom.byId("BenCountryTrd").innerHTML=entities.encode(grid.store.getValue(benJsObj, "BenCountry"));
    		
    		if (dom.byId("BbkBranchCodeTrd")) dom.byId("BbkBranchCodeTrd").innerHTML=entities.encode(grid.store.getValue(benJsObj, "PayeeBankCode_List"));

    		var benBank_name = grid.store.getValue(benJsObj, "PayeeBankName_List");
    		var benBank_adl1 = grid.store.getValue(benJsObj, "BenBranchAddress1");
    		var benBank_adl2 = grid.store.getValue(benJsObj, "BenBranchAddress2");
    		var benBank_city = grid.store.getValue(benJsObj, "BenBankCity");
    		var benBank_country = grid.store.getValue(benJsObj, "BenBranchCountry");
    		var benBank_branch_StPrvnc = grid.store.getValue(benJsObj, "BenBankProvince");
    		var benBank_sort_code = grid.store.getValue(benJsObj, "BenBankSortCode");
   	        dom.byId("BbkNameTrd").innerHTML = entities.encode(benBank_name);
   	        dom.byId("BbkAddressLine1Trd").innerHTML = entities.encode(benBank_adl1);
   	        dom.byId("BbkAddressLine2Trd").innerHTML = entities.encode(benBank_adl2);
   	        dom.byId("BbkCityTrd").innerHTML = entities.encode(benBank_city);
   	        dom.byId("BbkCountryTrd").innerHTML = entities.encode(benBank_country);
   	        dom.byId("BbkStateProvinceTrd").innerHTML = entities.encode(benBank_branch_StPrvnc);
   	        dom.byId("BbkSortCodeTrd").innerHTML = entities.encode(benBank_sort_code);
    	              
    		dom.byId("central_bank_reporting_1").innerHTML=entities.encode(grid.store.getValue(benJsObj, "CENTRAL_BANK_REPORTING_1"));
    		dom.byId("central_bank_reporting_2").innerHTML=entities.encode(grid.store.getValue(benJsObj, "CENTRAL_BANK_REPORTING_2"));
    		dom.byId("central_bank_reporting_3").innerHTML=entities.encode(grid.store.getValue(benJsObj, "CENTRAL_BANK_REPORTING_3"));
    		
    		dom.byId("ReportingCode1").innerHTML= entities.encode(getVal(data,"ReportingCode1"));
    		dom.byId("ReportingCode2").innerHTML= entities.encode(getVal(data,"ReportingCode2"));
    		
    		dom.byId("InvoicePaymentInstructionsOID").innerHTML=entities.encode(grid.store.getValue(benJsObj, "rowKey"));
    		
    		
    	<% } else { %>

    	dijit.byId("PaymentMethodType").set("value",grid.store.getValue(benJsObj, "PaymentMethod_List"));
    	
    	
    	if(grid.store.getValue(benJsObj, "BANK_CHARGES_TYPE")=='<%=TradePortalConstants.CHARGE_UPLOAD_FW_OURS%>'){
    		dijit.byId('TradePortalConstants.CHARGE_UPLOAD_FW_OURS').set('checked', true);
    	}else if(grid.store.getValue(benJsObj, "BANK_CHARGES_TYPE")=='<%=TradePortalConstants.CHARGE_UPLOAD_FW_BEN%>'){
    		dijit.byId('TradePortalConstants.CHARGE_UPLOAD_FW_BEN').set('checked', true);
    	}else if(grid.store.getValue(benJsObj, "BANK_CHARGES_TYPE")=='<%=TradePortalConstants.CHARGE_UPLOAD_FW_SHARE%>'){
    		dijit.byId('TradePortalConstants.CHARGE_UPLOAD_FW_SHARE').set('checked', true);
    	}
    	var isMultiBenInvoicesAttached =<%=isMultiBenInvoicesAttached%>;
    	if (isMultiBenInvoicesAttached){
    		dijit.getEnclosingWidget(document.getElementById('TradePortalConstants.CHARGE_UPLOAD_FW_OURS')).set("readOnly",true);
    		dijit.getEnclosingWidget(document.getElementById('TradePortalConstants.CHARGE_UPLOAD_FW_BEN')).set("readOnly",true);
    		dijit.getEnclosingWidget(document.getElementById('TradePortalConstants.CHARGE_UPLOAD_FW_SHARE')).set("readOnly",true);
    	}
    	dijit.byId("LoanProceedsPmtCurrTrd").set("value",grid.store.getValue(benJsObj, "Currency_List"));
    	dijit.byId("LoanProceedsPmtAmountTrd").set("value",grid.store.getValue(benJsObj, "Amount_List"));
    	dijit.byId("BenNameAmountCCYTrd").set("value",grid.store.getValue(benJsObj, "Currency_List"));
    	dijit.byId("BenAmountTrd").set("value",grid.store.getValue(benJsObj, "Amount_List"));


    	dijit.byId("EnteredAccountTrd").set("value",grid.store.getValue(benJsObj, "AccountNumber_List"));
    	dijit.byId("BenNameTrd").set("value",grid.store.getValue(benJsObj, "PayeeName_List"));
    	dijit.byId("BenAddressLine1Trd").set("value",grid.store.getValue(benJsObj, "BenAddressOne"));
    	dijit.byId("BenAddressLine2Trd").set("value",grid.store.getValue(benJsObj, "BenAddressTwo"));
    	dijit.byId("BenAddressLine3Trd").set("value",grid.store.getValue(benJsObj, "BenAddressThree"));
    	dijit.byId("BenCountryTrd").set("value",grid.store.getValue(benJsObj, "BenCountry"));
    	dijit.byId("BbkNameTrd").set("value",grid.store.getValue(benJsObj, "PayeeBankName_List"));
    	dijit.byId("BbkBranchCodeTrd").set("value",grid.store.getValue(benJsObj, "PayeeBankCode_List"));
    	dijit.byId("BbkAddressLine1Trd").set("value",grid.store.getValue(benJsObj, "BenBranchAddress1"));
    	dijit.byId("BbkAddressLine2Trd").set("value",grid.store.getValue(benJsObj, "BenBranchAddress2"));
    	
    	dijit.byId("BbkCityTrd").set("value",grid.store.getValue(benJsObj, "BenBankCity"));
    	
    	dijit.byId("central_bank_reporting_1").set("value",grid.store.getValue(benJsObj, "CENTRAL_BANK_REPORTING_1"));
    	dijit.byId("central_bank_reporting_2").set("value",grid.store.getValue(benJsObj, "CENTRAL_BANK_REPORTING_2"));
    	dijit.byId("central_bank_reporting_3").set("value",grid.store.getValue(benJsObj, "CENTRAL_BANK_REPORTING_3"));
    	dijit.byId("ReportingCode1").set("value",grid.store.getValue(benJsObj, "ReportingCode1"));
    	dijit.byId("ReportingCode2").set("value",grid.store.getValue(benJsObj, "ReportingCode2"));
    	dijit.byId("BbkStateProvinceTrd").set("value",grid.store.getValue(benJsObj, "BenBankProvince"));
    	dijit.byId("BbkSortCodeTrd").set("value",grid.store.getValue(benJsObj, "BenBankSortCode"));
    	dijit.byId("BbkCountryTrd").set("value",grid.store.getValue(benJsObj, "BenBranchCountry"));
    	dijit.byId("BenReferenceTrd").set("value",grid.store.getValue(benJsObj, "CUSTOMER_REFERENCE"));
    	
    	dijit.byId("InvoicePaymentInstructionsOID").set("value",grid.store.getValue(benJsObj, "rowKey"));
    	var isMultiBenInvoicesAttached =<%=isMultiBenInvoicesAttached%>;
		if (isMultiBenInvoicesAttached){
			document.getElementById("update_ben_button_div").style.display = "none";
			dijit.byId("addBeneficaryButton").setAttribute('disabled', true);
			dijit.getEnclosingWidget(document.getElementById('addBeneficaryButton')).set("disabled", true);
		}else{

    		document.getElementById("update_ben_button_div").style.display = "block";
    		document.getElementById("save_ben_button_div").style.display = "none";
        	dijit.byId("LoanProceedsPmtCurrTrd").set("readOnly",false);
        	dijit.byId("LoanProceedsPmtAmountTrd").set("readOnly",false);
    		dijit.byId("addBeneficaryButton").setAttribute('disabled', false);
    		dijit.getEnclosingWidget(document.getElementById('addBeneficaryButton')).set("disabled", false);
		}
       	
		<% } %>
    	  });    	
    	
    }
    function reloadBeneficiaryFormDetails(){
    	
    	myGrid = dijit.byId("TransactionLRQTradeLoanISSDataGridId");

    	if( dijit.byId("InvoicePaymentInstructionsOID") && "" != dijit.byId("InvoicePaymentInstructionsOID").value ){
    	
    		 myGrid.selection.addToSelection(dijit.byId("InvoicePaymentInstructionsOID").value); 
        	 
    	}else{
    		clearAllBeneficiaryFormDetails();
    	}
    }
      
     
    <%-- Prateep Gedupudi coded for adding subsection shortcuts for LoanRequest page Starts  --%>
      <%-- Implementing Subsection shortcuts at FormSidebar.js for the Loan Request page is confusing. So adding following script at page level.  --%>
      require(["dojo/_base/array", "dijit/registry", "dijit/TitlePane","dojo/query","dojo/dom-construct","dojo/ready"], function(arrayUtil, registry, TitlePane,query,domConstruct,ready){
      	ready(function(){
			var updateBenButton  =  registry.byId("update_ben_button_div");
			var isMultiBenInvoicesAttached =<%=isMultiBenInvoicesAttached%>;
			if (updateBenButton){
				if (isMultiBenInvoicesAttached){
					document.getElementById("update_ben_button_div").style.display = "none";
					dijit.getEnclosingWidget(document.getElementById('addBeneficaryButton')).set("disabled", true);
				}
				
			}
       	 var isError = <%=isError%>;
   	  if (isError){
   	 	dijit.byId("LoanProceedsPmtCurrTrd").set("readOnly", false);
   	 	dijit.byId("LoanProceedsPmtAmountTrd").set("readOnly", false);
   	  }

    		var singleBen =  registry.byId("CreditBenAccountTrd");
    		
      		if (singleBen){
      			singleBen.on("change", function(checkValue) {
    				   if ( checkValue ) {
    					   var transactionCurr = dijit.getEnclosingWidget(document.getElementById('TransactionCurrency'))	;
    					   if (transactionCurr){
    						   if (transactionCurr.value != ''){
    							   dijit.byId("LoanProceedsPmtCurrTrd").set("value", transactionCurr.value);
    							   dijit.byId("BenNameAmountCCYTrd").set("value", transactionCurr.value);
    						   }
    					   }
    					   var transactionAmount = dijit.getEnclosingWidget(document.getElementById('TransactionAmount'))	;
    					   if (transactionAmount){
    						   if (transactionAmount.value != ''){
    							   dijit.byId("LoanProceedsPmtAmountTrd").set("value", transactionAmount.value);
    							   dijit.byId("BenAmountTrd").set("value", transactionAmount.value);
    						   }

    					   }

    					   if (document.getElementById('TradeLoanPaybalesRadio'))
    						   dijit.getEnclosingWidget(document.getElementById("TradeLoanPaybalesRadio")).set("checked", true);
    					   hideBeneficiaryDetails(false);
    					   if (document.getElementById('addBeneficaryButton'))
    					   	   document.getElementById('addBeneficaryButton').disabled = true;
    					   if (document.getElementById('update_ben_button_div'))
    						   document.getElementById("update_ben_button_div").style.display = "none";
    					   if (document.getElementById('save_ben_button_div'))
    						   document.getElementById("save_ben_button_div").style.display = "none";	
    					   if (document.getElementById('cancel_ben_link_div'))
    						   document.getElementById("cancel_ben_link_div").style.display = "none"; 
    					   if (document.getElementById('add_ben_div'))
    					   		document.getElementById('add_ben_div').style.display = "none"	;
						   if (document.getElementById('add_inv_div'))
							   document.getElementById('add_inv_div').style.display = "block";	 
						   if (document.getElementById('remove_inv_div'))
							   document.getElementById('remove_inv_div').style.display = "block";	 
						   if (document.getElementById('view_inv_div'))
							   document.getElementById('view_inv_div').style.display = "block";	 
    				   
    	            	   	
    				   }
    		        });
      		}
      		var ourAcct = registry.byId("CreditOurAcctTrd");
      		if (ourAcct){
      			ourAcct.on("change", function(checkValue) {
    				   if ( checkValue ) {
    					   if (document.getElementById('TradeLoanReceivablesRadio'))
    						  
    						   hideBeneficiaryDetails(true);
    					   var isSingleBenInvoicesAttached =<%=isSingleBenInvoicesAttached%>;
    					   if (isSingleBenInvoicesAttached){
    						   if (dijit.getEnclosingWidget(document.getElementById('CreditBenAccountTrd')))
    							   	dijit.getEnclosingWidget(document.getElementById('CreditBenAccountTrd')).set("readOnly",false);

 
    					   }
    					   var isMultiBenInvoicesAttached =<%=isMultiBenInvoicesAttached%>;
   					   		if (isMultiBenInvoicesAttached){
 							   if (dijit.getEnclosingWidget(document.getElementById('CreditMultiBenAccountTrd')))
   								dijit.getEnclosingWidget(document.getElementById('CreditMultiBenAccountTrd')).set("readOnly",false);   						   
    					   }
    						   
    					   if (document.getElementById('addBeneficaryButton'))
        					   document.getElementById('addBeneficaryButton').disabled = true;
        					   if (document.getElementById('update_ben_button_div'))
        						   document.getElementById("update_ben_button_div").style.display = "none";
        					   if (document.getElementById('save_ben_button_div'))
        						   document.getElementById("save_ben_button_div").style.display = "none";	
        					   if (document.getElementById('cancel_ben_link_div'))
        						   document.getElementById("cancel_ben_link_div").style.display = "none"; 
        					   if (document.getElementById('add_ben_div'))
       					   		document.getElementById('add_ben_div').style.display = "none";	
    						   if (document.getElementById('add_inv_div'))
    							   document.getElementById('add_inv_div').style.display = "block";	 
    						   if (document.getElementById('remove_inv_div'))
    							   document.getElementById('remove_inv_div').style.display = "block";	 
    						   if (document.getElementById('view_inv_div'))
    							   document.getElementById('view_inv_div').style.display = "block";	 
        				  
         	    		    	
   				   }
    		        });
      		}
    
      		var  multiBen = registry.byId("CreditMultiBenAccountTrd");
      		if (multiBen){
      			multiBen.on("change", function(checkValue) {
    				   if ( checkValue ) {
    					   if (document.getElementById('TradeLoanPaybalesRadio'))
    						   dijit.getEnclosingWidget(document.getElementById("TradeLoanPaybalesRadio")).set("checked", true);
    					   hideBeneficiaryDetails(false);
    					   var isMultiBenInvoicesAttached =<%=isMultiBenInvoicesAttached%>;
    					   if (!isMultiBenInvoicesAttached){
    						   setBenFieldsReadOnly(false);
	    					   if (document.getElementById('addBeneficaryButton'))
	    					   	document.getElementById('addBeneficaryButton').disabled = false;
	    					   if (document.getElementById('update_ben_button_div'))
	    						   document.getElementById("update_ben_button_div").style.display = "none";
	    					   if (document.getElementById('save_ben_button_div'))
	    						   document.getElementById("save_ben_button_div").style.display = "block";	
	    					   if (document.getElementById('cancel_ben_link_div'))
	    						   document.getElementById("cancel_ben_link_div").style.display = "block"; 
	    					   if (document.getElementById('add_ben_div'))
	   					   			document.getElementById('add_ben_div').style.display = "block";	
							   if (document.getElementById('add_inv_div'))
								   document.getElementById('add_inv_div').style.display = "none";	 
							   if (document.getElementById('remove_inv_div'))
								   document.getElementById('remove_inv_div').style.display = "none";	 
							   if (document.getElementById('view_inv_div'))
								   document.getElementById('view_inv_div').style.display = "none";	
	    					 } else {
	    						 setBenFieldsReadOnly(true);
	    						   if (document.getElementById('addBeneficaryButton'))
	    							   document.getElementById('addBeneficaryButton').disabled = true;
	    						   if (document.getElementById('update_ben_button_div'))
	    							   document.getElementById("update_ben_button_div").style.display = "none";
	    						   if (document.getElementById('save_ben_button_div'))
	    							   document.getElementById("save_ben_button_div").style.display = "none";	
	    						   if (document.getElementById('cancel_ben_link_div'))
	    							   document.getElementById("cancel_ben_link_div").style.display = "none"; 
		    					   if (document.getElementById('add_ben_div'))
		   					   			document.getElementById('add_ben_div').style.display = "block";	

	    					   }
    				   }
    		        });
      		}
      		
      		var  trdLoanRecRadio = registry.byId("TradeLoanReceivablesRadio");
      		if (trdLoanRecRadio){
      			trdLoanRecRadio.on("change", function(checkValue) {
    				   if ( checkValue ) {
				       	var isReadOnly =<%=isUpldInvsAttchdLoanType%>;
    					   showHideDivs('TradeLoanRadio');
  
    					   if (document.getElementById('payablesBenRadioBtns'))
    						   document.getElementById("payablesBenRadioBtns").style.display = "none";
    					   var loanProceedsPmtCurrTrd = dijit.getEnclosingWidget(document.getElementById('LoanProceedsPmtCurrTrd'))	;
    					   if (loanProceedsPmtCurrTrd){
    						   loanProceedsPmtCurrTrd.set("readOnly",isReadOnly);
    					   }
    					   var loanProceedsPmtAmountTrd = dijit.getEnclosingWidget(document.getElementById('LoanProceedsPmtAmountTrd'))	;
    					   if (loanProceedsPmtAmountTrd){
    						   loanProceedsPmtAmountTrd.set("readOnly",isReadOnly);
    					   }

    					   if (document.getElementById('ben_details')){
    						   document.getElementById("ben_details").style.display = "none";
    						   
    					   }
    					   if (document.getElementById('benBank_details')){
    						   document.getElementById("benBank_details").style.display = "none";
    						   
    					   }
    					   if (document.getElementById('central_bank_reporting_div')){
    						   document.getElementById("central_bank_reporting_div").style.display = "none";
    						   
    					   }
    					   if (document.getElementById('reporting_code_div')){
    						   document.getElementById("reporting_code_div").style.display = "none";
    						   
    					   }
    					   
						   if (document.getElementById('addBeneficaryButton'))
							   document.getElementById('addBeneficaryButton').disabled = true;
						   if (document.getElementById('update_ben_button_div'))
							   document.getElementById("update_ben_button_div").style.display = "none";
						   if (document.getElementById('save_ben_button_div'))
							   document.getElementById("save_ben_button_div").style.display = "none";	
						   if (document.getElementById('cancel_ben_link_div'))
							   document.getElementById("cancel_ben_link_div").style.display = "none"; 
   					   
    		   
    				   }
    		        });
      		}
      		var  trdLoanPayRadio = registry.byId("TradeLoanPaybalesRadio");
      		if (trdLoanPayRadio){
      			trdLoanPayRadio.on("change", function(checkValue) {
    				   if ( checkValue ) {
    					   showHideDivs('TradeLoanRadio');
    					   if (document.getElementById('payablesBenRadioBtns'))
    						   document.getElementById("payablesBenRadioBtns").style.display = "block";
    					   var  creditOurAcct = document.getElementById("CreditOurAcctTrd");
    					   var  singleBenAcct = document.getElementById("CreditBenAccountTrd");
    					   var  multiBenAcct = document.getElementById("CreditMultiBenAccountTrd");
    					   if ((creditOurAcct && !creditOurAcct.checked) &&
    							   (singleBenAcct && !singleBenAcct.checked) && 
    							   (multiBenAcct && !multiBenAcct.checked) ){
			    		    	if (document.getElementById('update_ben_button_div'))
			    		    		document.getElementById("update_ben_button_div").style.display = "none";
			    		    	if (document.getElementById('save_ben_button_div'))
			            	   		document.getElementById("save_ben_button_div").style.display = "none";
			    		    	if (document.getElementById('addBeneficaryButton'))
			            	   		dijit.byId("addBeneficaryButton").setAttribute('disabled', true);
    						    if (document.getElementById('cancel_ben_link_div'))
    							    document.getElementById("cancel_ben_link_div").style.display = "none"; 
    						   
    					   }
    					   if ((creditOurAcct && !creditOurAcct.checked) ){

	    					   if (document.getElementById('ben_details')){
	    						   document.getElementById("ben_details").style.display = "block";
	    						   
	    					   }
	    					   if (document.getElementById('benBank_details')){
	    						   document.getElementById("benBank_details").style.display = "block";
	    						   
	    					   }
	    					   if (document.getElementById('central_bank_reporting_div')){
	    						   document.getElementById("central_bank_reporting_div").style.display = "block";
	    						   
	    					   }
	    					   if (document.getElementById('reporting_code_div')){
	    						   document.getElementById("reporting_code_div").style.display = "block";
	    						   
	    					   }
    					   }
    					   
    					   if (multiBenAcct && multiBenAcct.checked){
    						   
    							   
    			    		    if("" == dijit.byId("InvoicePaymentInstructionsOID").value){
    			    		    	if (document.getElementById('update_ben_button_div'))
    			    		    		document.getElementById("update_ben_button_div").style.display = "none";
    			    		    	if (document.getElementById('save_ben_button_div'))
    			            	   		document.getElementById("save_ben_button_div").style.display = "block";
    			    		    	if (document.getElementById('addBeneficaryButton'))
    			            	   		dijit.byId("addBeneficaryButton").setAttribute('disabled', true);
    			    		    }else{
    			    		    	if (document.getElementById('update_ben_button_div'))
    			    		    		document.getElementById("update_ben_button_div").style.display = "block";
    			    		    	if (document.getElementById('save_ben_button_div'))
    			    		    		document.getElementById("save_ben_button_div").style.display = "none";
    			    		    	if (document.getElementById('addBeneficaryButton'))
    			    		    		dijit.byId("addBeneficaryButton").setAttribute('disabled', false);
    			    		    }
    						   
    						   if (document.getElementById('cancel_ben_link_div'))
    							   document.getElementById("cancel_ben_link_div").style.display = "block"; 
     
    					   }
    				   }
    				   
    		        });
      		}
 
      		
        	if (dijit.getEnclosingWidget(document.forms[0].FinanceType[0])){
        		if (dijit.getEnclosingWidget(document.forms[0].FinanceType[0]).checked){
        			require(["dojo/dom","dojo/dom-style"], function(dom, domStyle) {
        				  domStyle.set(dom.byId("payablesBenRadioBtns"),'display', 'none');
        				});

        		} 
         		
        	}
        	if (dijit.getEnclosingWidget(document.forms[0].FinanceType[1])){
        		if (dijit.getEnclosingWidget(document.forms[0].FinanceType[1]).checked){
        			require(["dojo/dom","dojo/dom-style"], function(dom, domStyle) {
        				  domStyle.set(dom.byId("payablesBenRadioBtns"),'display', 'block');
        				});

        		}
        	}
      		<%-- Getting all the children for section 3 TitlePane --%>
      	var section3Chidren = registry.byId("section3").getChildren();
      	<%-- Getting dom node for specific section --%>
      	var section3NavCon= query("#navContainer > li")[2];
      	
      	<%-- As we are calling this on change of LoanType Radios, we need to destroy the li's before painted. --%>
      	if(query("#navContainer > li > li").length){
      	  query("#navContainer > li > li").forEach(domConstruct.destroy);
      	}
      	
     	
      	 });
      });

      <%-- Prateep Gedupudi coded for adding subsection shortcuts for LoanRequest page Ends --%>

  <%-- cquinton 3/2/2013 local phrase lookup --%>
  require(["t360/phraseLookup"],
          function( phraseLookup ) {
    local.phraseLookupAppend = function(phraseSelect, textArea, textAreaMaxLength ) {
      phraseLookup.appendPhrase( phraseSelect, textArea, textAreaMaxLength, "_errorSection" );
    };
  });

 if(!isReadOnly && !isTemplate){ <%--  should not display calculate section if it is template or read only mode --%>
 require(["dijit/registry", "dojo/on", "dojo/ready"],
    	      function(registry, on, ready) {
    	  	ready(function(){
    	  		var interestDiscountSection= document.getElementById("interestDiscountSection"); 
    	  		var tradeLoanReceivablesRadio = registry.byId("TradeLoanReceivablesRadio");
    	  		var tradeLoanPaybalesRadio = registry.byId("TradeLoanPaybalesRadio");
     	  	    var tradeLoanRadio = registry.byId("TradeLoanRadio");

  			   if (tradeLoanReceivablesRadio) {
  				 tradeLoanReceivablesRadio.on("change", function(checkValue) {
  					   if ( this.checked ) {
  						 interestDiscountSection.style.visibility = 'visible';
  			          }
  			        });  				   				 
  			   }  
  			   	    	  		    	  	   
  			   if (tradeLoanPaybalesRadio) {
  				 tradeLoanPaybalesRadio.on("change", function(checkValue) {
  					   if ( this.checked ) {
  						 interestDiscountSection.style.visibility = 'visible';
  			          }  			          
  			        });
  			   }

  			   if (tradeLoanRadio) {
  				 tradeLoanRadio.on("change", function(checkValue) {
  					   if ( this.checked == false) {
  						 interestDiscountSection.style.visibility = 'hidden';
  			          }  			          
  			        });
  				 
  				 if(tradeLoanReceivablesRadio.checked || tradeLoanPaybalesRadio.checked){
  					interestDiscountSection.style.visibility = 'visible';
  				 }else{
  					interestDiscountSection.style.visibility = 'hidden';
  				 }
  			   }    	  		   	  		
    	  	});    	  

      });
  }else if(isReadOnly && !isTemplate){ <%--  should not display calculate section if it is template or read only mode --%>
	 require(["dijit/registry", "dojo/on", "dojo/ready"],
	    	      function(registry, on, ready) {
	    	  	ready(function(){
	    	  		var interestDiscountSection= document.getElementById("interestDiscountSection"); 
	    	  		var tradeLoanReceivablesRadio = registry.byId("TradeLoanReceivablesRadio");
	    	  		var tradeLoanPaybalesRadio = registry.byId("TradeLoanPaybalesRadio");
	     	  	    var tradeLoanRadio = registry.byId("TradeLoanRadio");

	  			   if (tradeLoanReceivablesRadio) {
	  				 tradeLoanReceivablesRadio.on("change", function(checkValue) {
	  					   if ( this.checked ) {
	  						 interestDiscountSection.style.visibility = 'visible';
	  			          }
	  			        });  				   				 
	  			   }  
	  			   	    	  		    	  	   
	  			   if (tradeLoanPaybalesRadio) {
	  				 tradeLoanPaybalesRadio.on("change", function(checkValue) {
	  					   if ( this.checked ) {
	  						 interestDiscountSection.style.visibility = 'visible';
	  			          }  			          
	  			        });
	  			   }

	  			   if (tradeLoanRadio) {
	  				 tradeLoanRadio.on("change", function(checkValue) {
	  					   if ( this.checked == false) {
	  						 interestDiscountSection.style.visibility = 'hidden';
	  			          }  			          
	  			        });
	  				 
	  				 if(tradeLoanReceivablesRadio.checked || tradeLoanPaybalesRadio.checked){
	  					interestDiscountSection.style.visibility = 'visible';
	  					interestDiscountButtonSection.style.visibility = 'hidden';
	  				 }else{
	  					interestDiscountSection.style.visibility = 'hidden';
	  				 }
	  			   }    	  		   	  		
	    	  	});    	  

	      });
	  }
 
<%--  if InAdvance option is selected, then make charge and account section as readonly and select loan proceed account option. --%>
 
 require(["dijit/registry", "dojo/on", "dojo/ready", "dojo/dom", "dojo/query"],
	      function(registry, on, ready, dom, query) {
	 ready(function(){
		 
     <%--  during page load if InAdvance option is selected, then default select loan proceed account option and make read only whole section. --%>
     if( document.forms[0].InAdvance.checked){
    	 accountSction(true) 
     }
     
     <%--  manual action if InAdvance option is selected, then make section as readonly, else not. --%>
	 var inAdvance =  registry.byId("InAdvance");
	   if (inAdvance) {
		   inAdvance.on("change", function(checkValue) {
			   if ( this.checked ) {
				   accountSction(true);
			   }else{
				   accountSction(false);
	          }
		   });	   
	   } 
	 });
 });
 
 function accountSction(isSelected){
	 if(isSelected){
		 if (dijit.byId("ComsChargesOurAccountNum"))
		 	dijit.byId("ComsChargesOurAccountNum").set("value","");
		 if (dijit.byId("InterestDebitAccountNum"))
			 	dijit.byId("InterestDebitAccountNum").set("value","");
		 dijit.getEnclosingWidget(document.getElementById('DebitLoanProceeds')).set('checked',true);
	 }
	 dijit.getEnclosingWidget(document.getElementById('DebitLoanProceeds')).set('readOnly',isSelected);
	 dijit.getEnclosingWidget(document.getElementById('DebitAcctReceivables')).set('readOnly',isSelected);
	 dijit.getEnclosingWidget(document.getElementById('DebitAccounts')).set('readOnly',isSelected);
	 dijit.getEnclosingWidget(document.getElementById('ComsChargesOurAccountNum')).set('readOnly',isSelected);
	 dijit.getEnclosingWidget(document.getElementById('InterestDebitAccountNum')).set('readOnly',isSelected);
	 	 
 }
 
 function getVal(data, code) {
	   var val = '';
	  if (data && data[code] ) {
	     val = data[code];
	   }
	   return val;
	}
 
 function updateReportingCodes() {
		require(["dijit/registry", "dojo/ready"],
			function(registry, ready ) {
			<%--CR-1001 Rel9.4 - If the Instrument is created from Invoices using a different OpOrg(not first one), 
	    	   then clear the options and set the Reporting code value as given in the invoice--%>
	    		var repCode1Field = document.getElementById("ReportingCode1");
				  if (repCode1Field && repCode1Field.value == ''){
					  var selectedRptgCodeDesc1 = '<%=selectedRptgCodeDesc1%>';
					  var selectedRptgCodes1 = '<%=selectedRptgCodes1%>';
					  if(selectedRptgCodeDesc1!=''){
						  var rpt1 = dijit.byId("ReportingCode1");
						  rpt1.store.fetch({
								query: {name: '*'},
								onComplete: function (items) {
									dojo.forEach(items, function (item, i) {
										rpt1.store.remove(item.id);
									});
								}
							});
						  rpt1.store.add({disabled:true,id:selectedRptgCodes1,name:selectedRptgCodeDesc1,label:selectedRptgCodeDesc1,value:selectedRptgCodeDesc1, selected: true})
						  rpt1.reset();
						  rpt1.store.fetch({
								query: {name: '*'},
								onComplete: function (items) {
									  rpt1.set('value', selectedRptgCodes1);
								}
							});
						  
						  
					  }
				  }
			  var repCode2Field = document.getElementById("ReportingCode2");
			  if (repCode2Field && repCode2Field.value == ''){
				  var selectedRptgCodeDesc2 = '<%=selectedRptgCodeDesc2%>';
				  var selectedRptgCodes2 = '<%=selectedRptgCodes2%>';
				  if(selectedRptgCodeDesc2!=''){
					  var rpt2 = dijit.byId("ReportingCode2");
					  rpt2.store.fetch({
							query: {name: '*'},
							onComplete: function (items) {
								dojo.forEach(items, function (item, i) {
									rpt2.store.remove(item.id);
								});
							}
						});
					  rpt2.store.add({disabled:true,id:selectedRptgCodes2,name:selectedRptgCodeDesc2,label:selectedRptgCodeDesc2,value:selectedRptgCodeDesc2, selected: true});
					  rpt2.reset();
					  rpt2.store.fetch({
							query: {name: '*'},
							onComplete: function (items) {
								  rpt2.set('value', selectedRptgCodes2);
							}
						});
				  }
			  }

			});
		
		
	}
</script>
