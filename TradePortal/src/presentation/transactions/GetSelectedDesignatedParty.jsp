<%--
*******************************************************************************
  Get Selected Designated Party

  Description:
  This jsp is called via ajax and returns xml representing a party to the 
  caller.  The input is a rowKey from a PartySearch selection.
  The rowKey consists of a partyOid followed by the designated party oid.
  This jsp returns the party data for that designated party.

  Note: this is very similar to DesignatedPartyLookup previously used for
  page entry from PartySearch.jsp - but now in ajax form.

*******************************************************************************
--%>
<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.html.*,java.math.*, java.util.StringTokenizer" %>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session"> </jsp:useBean>
<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session"> </jsp:useBean>
<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session"> </jsp:useBean>
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"> </jsp:useBean>
   

<%
  String output = "<response>";
  boolean errors = false;
  String errorMessage = "";

  boolean found = false;

  String encryptedPartyRowKey = request.getParameter("partyRowKey");
  String designatedPartyOid = "";
  String partyRowKey = EncryptDecrypt.decryptStringUsingTripleDes(encryptedPartyRowKey, userSession.getSecretKey());
  StringTokenizer strTok = new StringTokenizer(partyRowKey,"/");
  if (strTok.hasMoreTokens()) {
    strTok.nextToken();
  }
  if (strTok.hasMoreTokens()) {
    designatedPartyOid = strTok.nextToken();
    found = true;
  }

  String partyOutput = "";
  if ( found && designatedPartyOid != null && !designatedPartyOid.equals("")) {
    // Use a Party web bean to get the party data for the oid.
    PartyWebBean party = beanMgr.createBean(PartyWebBean.class, "Party");
    party.setAttribute("party_oid", designatedPartyOid);
    party.getDataFromAppServer();

    DocumentHandler partyDoc = new DocumentHandler();
    party.populateXmlDoc(partyDoc);

    partyDoc = partyDoc.getFragment("/Party");

    partyOutput = partyDoc.toString();
  }

  //todo: check for no errors!
  if ( !errors ) {
    if ( found ) {
      output += "<responseCode>SUCCESS</responseCode>";
      output += partyOutput;
    }
    else {
      output += "<responseCode>NOTFOUND</responseCode>";
    }
  }
  else {
    output += "<responseCode>ERROR</responseCode>";
    output += "<errorMessage>" + errorMessage + "</errorMessage>";
  }
  output += "</response>";

  out.println(output);
%>
