<%--
*******************************************************************************
                              Export Collection Amend Page

  Description:
    This is the main driver for the Export Collection Amendment page.  It handles 
  data retrieval of terms and terms parties (or retrieval from the input document)
  and creates the html page to display all the data for an Export Collection.
 
 Vasavi CR 524 03/31/2010 

*******************************************************************************
--%>

<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page
	import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,java.util.*,
	com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.html.*,java.math.*,
	com.amsinc.ecsg.util.DateTimeUtility"%>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager"
	scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager"
	scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager"
	scope="session">
</jsp:useBean>

<jsp:useBean id="userSession"
	class="com.ams.tradeportal.busobj.webbean.SessionWebBean"
	scope="session">
</jsp:useBean>


<%-- ************** Data retrieval page setup begins here ****************  --%>

<%
	WidgetFactory widgetFactory = new WidgetFactory(resMgr);
	String links="";
	String focusField = "TracerDetails"; // Default focus field

	// Various oid and status info from transaction and instruments used in
	// several places.
	String instrumentOid = "";
	String transactionOid;
	String instrumentType;
	String instrumentStatus;
	String transactionType;
	String transactionStatus;
	String rejectionIndicator = "";
	String rejectionReasonText = "";
	// This array of string is used when creating the Collection Schedule
	// and Bill of Exchanges pdf links.
	String linkArgs[] = { "", "", "", "" };

	boolean getDataFromDoc; // Indicates if data is retrieved from the
							// input doc cache or from the database

	DocumentHandler doc;
	String buttonPressed ="";
  	Vector error = null;
	String loginLocale = userSession.getUserLocale();
	String loginRights = userSession.getSecurityRights();

	// Variables used for populating ref data dropdowns.
	String options;
	String defaultText;

	// Drawer name displayed on the amendment page
	String drawerName = "";

	//These are the values to be passed to set up the Tenor section (3)
	//It was easier and less expensive to use variables rather than create a ResMgr object
	//in the calling jsp:include for just one getText call.
	String label;
	String newTransaction = null;
	String descName;
	String descAttName;
	String amountName;
	String amountAttName;
	String draftName;
	String draftAttName;
	boolean isProcessedByBank = false;
	int NUMBER_OF_TENORS = 6;
	// These are the beans used on the page.

	TransactionWebBean transaction = (TransactionWebBean) beanMgr
			.getBean("Transaction");
	InstrumentWebBean instrument = (InstrumentWebBean) beanMgr
			.getBean("Instrument");
	TemplateWebBean template = (TemplateWebBean) beanMgr
			.getBean("Template");
	TermsWebBean terms = null;

	// We need the amount of the active transaction to display.  Create a webbean
	// to hold this data.
	TransactionWebBean activeTransaction = beanMgr.createBean(TransactionWebBean.class, "Transaction");

	// Get the document from the cache.  We'll may use it to repopulate the 
	// screen if returning from another page, a save, validation, or any other
	// mediator called from this page.  Otherwise, we assume an instrument oid
	// and transaction oid was passed in.

	
	doc = formMgr.getFromDocCache();
	 if (doc.getDocumentNode("/In/Update/ButtonPressed") != null) 
	  {
	buttonPressed = doc.getAttribute("/In/Update/ButtonPressed");
  	error = doc.getFragments("/Error/errorlist/error");
	  }
	 
	 if (doc.getDocumentNode("/Out/newTransaction")!=null) {
		    newTransaction = doc.getAttribute("/Out/newTransaction");
		    session.setAttribute("newTransaction", newTransaction);
		    System.out.println("found newTransaction = "+ newTransaction);
		  } 
		  else {
		    newTransaction = (String) session.getAttribute("newTransaction");
		    if ( newTransaction==null ) {
		      newTransaction = TradePortalConstants.INDICATOR_NO;
		    }
		    System.out.println("used newTransaction from session = " + newTransaction);
		  }

	//Debug.debug("doc from cache is " + doc.toString());

	/******************************************************************************
	 We are either entering the page or returning from somewhere.  These are the 
	 conditions for how to populate the web beans.  Data comes from either the
	 database or the doc cache (/In section) with some variation.

	 Mode           Condition                      Populate Beans From
	 -------------  ----------------------------   --------------------------------
	 Enter Page     no /In/Transaction in doc      Instrument and Template web
	 beans already populated, get
	 data for Terms and TermsParty
	 web beans from database

	 return from    /In/NewPartyFromSearchInfo/PartyOid   doc cache (/In); also use Party
	 Party Search     exists                       SearchInfo to lookup, and 
	 populate a specific TermsParty
	 web bean

	 return from    /Out/PhraseLookupInfo exists   doc cache (/In); but use Phrase
	 Phrase Lookup                                 LookupInfo text (from /Out) to
	 replace a specific phrase text
	 in the /In document before 
	 populating

	 return from    /Error/maxerrorseverity < 1    Same as Enter Page (data is
	 Transaction                                   retrieved from database)
	 mediator
	 (no error)

	 return from    /Error/maxerrorseverity > 0    doc cache (/In)
	 Transaction    
	 mediator
	 (error)
	 ******************************************************************************/

	// Assume we get the data from the doc.
	getDataFromDoc = true;

	String maxError = doc.getAttribute("/Error/maxerrorseverity");
	if (maxError != null
			&& maxError.compareTo(String
					.valueOf(ErrorManager.WARNING_ERROR_SEVERITY)) < 0) {
		// No errors, so don't get the data from doc.
		getDataFromDoc = false;
	}
	//ir cnuk113043991 - check to see if transaction needs to be refreshed
	// if so, refresh it and do not get data from doc as it is wrong
	if ("Y".equals(doc.getAttribute("/Out/Refresh/Transaction"))) {
		transaction.getDataFromAppServer();
		getDataFromDoc = false;
	}
	if (doc.getDocumentNode("/In/NewPartyFromSearchInfo/Type") != null) {
		// We have returned from the party search page.  Get data from doc cache
		getDataFromDoc = true;
	}
	if (doc.getDocumentNode("/In/Transaction") == null) {
		// No /In/Transaction means we've never looked up the data.
		Debug.debug("No /In/Transaction section - get data from database");
		getDataFromDoc = false;
	}
	if (doc.getDocumentNode("/Out/PhraseLookupInfo") != null) {
		// A Looked up phrase exists.  Replace it in the /In document.
		Debug.debug("Found a looked-up phrase");
		getDataFromDoc = true;

		// Take the looked up and appended phrase text from the /Out section
		// and copy it to the /In section.  This allows the beans to be 
		// properly populated.
		String xmlPath = doc
				.getAttribute("/In/PhraseLookupInfo/text_path");
		xmlPath = "/In" + xmlPath;

		doc.setAttribute(xmlPath,
				doc.getAttribute("/Out/PhraseLookupInfo/NewText"));

		// If we returned from the phrase lookup without errors, set the focus
		// to the correct field.  Otherwise, don't set the focus so the user can
		// see the error.
		if (maxError != null
				&& maxError.compareTo(String
						.valueOf(ErrorManager.WARNING_ERROR_SEVERITY)) < 0) {
			focusField = doc
					.getAttribute("/In/PhraseLookupInfo/text_field_name");
		}
	}

	if (getDataFromDoc) {
		Debug.debug("Populating beans from doc cache");

		// Populate the beans from the input doc.
		try {
			instrument.populateFromXmlDoc(doc.getComponent("/In"));
			transaction.populateFromXmlDoc(doc.getComponent("/In"));
			template.populateFromXmlDoc(doc.getComponent("/In"));

			terms = (TermsWebBean) beanMgr.getBean("Terms");
			terms.populateFromXmlDoc(doc, "/In");

			String activeTransOid = instrument
					.getAttribute("active_transaction_oid");
			if (!InstrumentServices.isBlank(activeTransOid)) {
				activeTransaction.setAttribute("transaction_oid",
						activeTransOid);
				activeTransaction.getDataFromAppServer();
			}

		} catch (Exception e) {
			out.println("Contact Administrator: "
					+ "Unable to reload data after returning to page. "
					+ "Error is " + e.toString());
		}
	} else {
		Debug.debug("populating beans from database");
		// We will perform a retrieval from the database.
		// Instrument and Transaction were already retrieved.  Get
		// the rest of the data

		terms = transaction.registerCustomerEnteredTerms();

		String activeTransOid = instrument
				.getAttribute("active_transaction_oid");
		if (!InstrumentServices.isBlank(activeTransOid)) {
			activeTransaction.setAttribute("transaction_oid",
					activeTransOid);
			activeTransaction.getDataFromAppServer();
		}

	}

	// Now determine the mode for how the page operates (readonly, etc.)
	BigInteger requestedSecurityRight = SecurityAccess.NEW_EXPORT_COLL_CREATE_MODIFY;
%>
<%@ include file="fragments/Transaction-PageMode.frag"%>


<%
	// Now we need to calculate the computed new lc amount total.  This involves 
	// using the instrument amount of the active transaction, the amount entered by the user,
	// performing some validation and then doing the calculation.

	BigDecimal originalAmount = BigDecimal.ZERO;
	BigDecimal offsetAmount = BigDecimal.ZERO;
	String amountValue = null;
	String displayAmount;
	String currencyCode = activeTransaction
			.getAttribute("copy_of_currency_code");
	terms.setAttribute("reference_number",
			instrument.getAttribute("copy_of_ref_num"));
	boolean badAmountFound = false;

	// Convert the active transaction amount to a double
	try {
		amountValue = activeTransaction
				.getAttribute("instrument_amount");
		originalAmount = new BigDecimal(amountValue);
	} catch (Exception e) {
		originalAmount = BigDecimal.ZERO;
	}

	if (getDataFromDoc && doc.getAttribute("/In/Terms/amount") != null) {
		// Convert the amount for this terms record to a double.  If we're getting
		// data from the doc, use the value in the document and attempt to convert
		// to a double.  It may fail due to invalid number.  If so, we'll post 
		// an error and set a flag to prevent the calculation from being 
		// displayed.
		//

		amountValue = doc.getAttribute("/In/Terms/amount");
		displayAmount = amountValue;

	} else {
		try {
			amountValue = terms.getAttribute("amount");
			offsetAmount = new BigDecimal(amountValue);
		} catch (Exception e) {
			offsetAmount = BigDecimal.ZERO;
		}
		Debug.debug("TermsOid = " + terms.getAttribute("terms_oid")
				+ "  Terms Amount == '" + terms.getAttribute("amount")
				+ "'");
		if (InstrumentServices.isBlank(amountValue) && !isReadOnly) {
			displayAmount = "";
		} else {
			displayAmount = TPCurrencyUtility.getDisplayAmount(
					offsetAmount.toString(), currencyCode, loginLocale);
		}
	}

	transactionType = transaction.getAttribute("transaction_type_code");
	transactionStatus = transaction.getAttribute("transaction_status");
	transactionOid = transaction.getAttribute("transaction_oid");

	// Get the transaction rejection indicator to determine whether to show the rejection reason text
	rejectionIndicator = transaction
			.getAttribute("rejection_indicator");
	rejectionReasonText = transaction
			.getAttribute("rejection_reason_text");

	instrumentType = instrument.getAttribute("instrument_type_code");
	instrumentStatus = instrument.getAttribute("instrument_status");

	Debug.debug("Instrument Type " + instrumentType);
	Debug.debug("Instrument Status " + instrumentStatus);
	Debug.debug("Transaction Type " + transactionType);
	Debug.debug("Transaction Status " + transactionStatus);

	// Create documents for the phrase dropdowns. First check the cache (no need
	// to recreate if they already exist).  After creating, place in the cache.
	// (InstrumentCloseNavigator.jsp cleans these up.)

	DocumentHandler phraseLists = formMgr
			.getFromDocCache(TradePortalConstants.PHRASE_LIST_DOC);
	if (phraseLists == null)
		phraseLists = new DocumentHandler();

	DocumentHandler spclInstrDocList = phraseLists
			.getFragment("/SpclInstr");
	if (spclInstrDocList == null) {
		spclInstrDocList = PhraseUtility.createPhraseList(
				TradePortalConstants.PHRASE_CAT_SPCL_INST, userSession,
				formMgr, resMgr);
		phraseLists.addComponent("/SpclInstr", spclInstrDocList);
	}

 formMgr.storeInDocCache(TradePortalConstants.PHRASE_LIST_DOC, phraseLists);

	// Retrieve the drawer name from the bank release terms of the latest issuance or amendment transaction.
	//    (The latest transaction is the one that has the latest transaction status date and in case several transaction
	//     have the same transaction status date, the largest transaction_oid.)
	if (transactionType.equals(TransactionType.AMEND)) {
		String instrument_oid = instrument
				.getAttribute("instrument_oid");
		StringBuffer issueTransactionSQL = new StringBuffer();
		issueTransactionSQL
				.append("select p.name")
				.append(" from terms_party p, terms t, transaction tr")
				.append(" where tr.transaction_oid =")
				.append("       (select max(transaction_oid)")
				.append("          from transaction")
				.append("         where transaction_status_date = (select max(transaction_status_date)")
				.append("                                            from transaction")
				.append("                                           where p_instrument_oid = ?")
				.append("                                           and (transaction_type_code = ? or transaction_type_code = ?  or transaction_type_code = ?)")
				.append("                                             and transaction_status = ?)")
				.append("           and p_instrument_oid = ?")
				.append("           and (transaction_type_code = ? or transaction_type_code = ? or transaction_type_code = ?)")
				.append("           and transaction_status = ?)")
				.append(" and tr.c_bank_release_terms_oid = t.terms_oid")
				.append(" and (p.terms_party_oid = t.c_first_terms_party_oid")
				.append("      or p.terms_party_oid = t.c_second_terms_party_oid")
				.append("      or p.terms_party_oid = t.c_third_terms_party_oid")
				.append("      or p.terms_party_oid = t.c_fourth_terms_party_oid")
				.append("      or p.terms_party_oid = t.c_fifth_terms_party_oid)")
				.append(" and p.terms_party_type = ?");
		//jgadela  R90 IR T36000026319 - SQL FIX
		Object sqlParamsTransResu[] = new Object[11];
		sqlParamsTransResu[0] =  instrument_oid ;
		sqlParamsTransResu[1] =  TransactionType.ISSUE;
		sqlParamsTransResu[2] =  TransactionType.AMEND;
		sqlParamsTransResu[3] =  TransactionType.CHANGE;
		sqlParamsTransResu[4] =  TradePortalConstants.TRANS_STATUS_PROCESSED_BY_BANK;
		sqlParamsTransResu[5] =  instrument_oid ;
		sqlParamsTransResu[6] =  TransactionType.ISSUE;
		sqlParamsTransResu[7] =  TransactionType.AMEND;
		sqlParamsTransResu[8] =  TransactionType.CHANGE;
		sqlParamsTransResu[9] =  TradePortalConstants.TRANS_STATUS_PROCESSED_BY_BANK;
		sqlParamsTransResu[10] = TradePortalConstants.DRAWER_SELLER;
		
		DocumentHandler issueTransactionResult = DatabaseQueryBean.getXmlResultSet(issueTransactionSQL.toString(), false, sqlParamsTransResu);
		if (issueTransactionResult != null) {drawerName = issueTransactionResult.getAttribute("/ResultSetRecord(0)/NAME");
		}
	}
%>


<%-- ********************* HTML for page begins here ********************* --%>

<%
	if (transactionType.equals(TransactionType.AMEND)) {

		focusField = "TransactionAmount";

	}

	// Some of the retrieval logic above may have set a focus field.  Otherwise,
	// we'll use the initial value for focus.
	String onLoad = "";

	// The navigation bar is only shown when editing templates.  For transactions
		// it is not shown ti minimize the chance of leaving the page without properly
		// unlocking the transaction.
		String showNavBar = TradePortalConstants.INDICATOR_NO;
		if (isTemplate) {
			showNavBar = TradePortalConstants.INDICATOR_YES;
		}
		
	// Auto save the form when time-out if not readonly.  
	// (Header.jsp will check for auto-save setting of the corporate org).
	String autoSaveFlag = isReadOnly ? TradePortalConstants.INDICATOR_NO
			: TradePortalConstants.INDICATOR_YES;
	String templateFlag = isTemplate ? TradePortalConstants.INDICATOR_YES
			: TradePortalConstants.INDICATOR_NO;
%>

<%
	//cr498 begin
	//Include ReAuthentication frag in case re-authentication is required for
	//authorization of transactions for this client
	String certAuthURL = "";
	Cache reCertCache = (Cache) TPCacheManager.getInstance().getCache(
			TradePortalConstants.CLIENT_BANK_CACHE);
	DocumentHandler CBCResult = (DocumentHandler) reCertCache
			.get(userSession.getClientBankOid());
	String requireTranAuth = CBCResult
			.getAttribute("/ResultSetRecord(0)/REQUIRE_TRAN_AUTH");
	boolean requireAuth = false;
	if (transactionType.equals(TransactionType.AMEND)) {
		requireAuth = InstrumentAuthentication
				.requireTransactionAuthentication(requireTranAuth,
						InstrumentAuthentication.TRAN_AUTH__EXP_OCO_AMD);
	}
	if (requireAuth) {
%>
<%@ include file="/logon/fragments/openReauthenticationWindow.frag"%>
<%
	}
	//cr498 end
%>

<%
  String pageTitleKey;
  if ( TradePortalConstants.INDICATOR_YES.equals(newTransaction) ) {
    pageTitleKey = "SecondaryNavigation.Instruments";
    userSession.setCurrentPrimaryNavigation("NavigationBar.NewInstruments");
  } else {
    pageTitleKey = "SecondaryNavigation.Instruments";
    userSession.setCurrentPrimaryNavigation("NavigationBar.Transactions");
  }

  //cquinton 11/19/2012 pass info for recent instruments into header
  String newRecentTransactionOid = transaction.getAttribute("transaction_oid");
  String newRecentInstrumentId = instrument.getAttribute("complete_instrument_id");
  String newRecentTransactionType = transaction.getAttribute("transaction_type_code");
  String newRecentTransactionStatus = transaction.getAttribute("transaction_status");
%>

<jsp:include page="/common/Header.jsp">
  <jsp:param name="autoSaveFlag"           value="<%=autoSaveFlag%>" />
  <jsp:param name="templateFlag"           value="<%=templateFlag%>" />
  <jsp:param name="newRecentTransactionOid" value="<%=newRecentTransactionOid%>" />
  <jsp:param name="newRecentInstrumentId"  value="<%=newRecentInstrumentId%>" />
  <jsp:param name="newRecentTransactionType" value="<%=newRecentTransactionType%>" />
  <jsp:param name="newRecentTransactionStatus" value="<%=newRecentTransactionStatus%>" />
</jsp:include>

<div class="pageMain">
<div class="pageContent">
<%
    
  String onlineHelpFileName;
  ReferenceDataManager refData = ReferenceDataManager.getRefDataMgr();
  String itemKey;
  itemKey = refData.getDescr(TradePortalConstants.INSTRUMENT_TYPE, 
          instrument.getAttribute("instrument_type_code"), 
          loginLocale);
	if (transactionType.equals(TransactionType.AMEND))
		onlineHelpFileName = "customer/amend_exp_coll.htm";
	else
		onlineHelpFileName = "customer/export_collection_tracer.htm";
	//Added to show :Amend/Tracer
	if (transactionType.equals(TransactionType.AMEND)){
		itemKey+=": "+resMgr.getText("ExportCollectionAmend.Amend", TradePortalConstants.TEXT_BUNDLE);
	}else if (transactionType.equals(TransactionType.TRACER)){
		itemKey+=": "+resMgr.getText("ExportCollectionTracer.Tracer", TradePortalConstants.TEXT_BUNDLE);
	}
%>
<jsp:include page="/common/PageHeader.jsp">
   <jsp:param name="titleKey" value="<%=pageTitleKey%>" />
   <jsp:param name="item1Key" value="<%=itemKey%>" />
   <jsp:param name="helpUrl"  value="<%=onlineHelpFileName%>" />
</jsp:include>

<jsp:include page="/common/TransactionSubHeader.jsp"/>
<form id="TransactionEXP_OCO" name="TransactionEXP_OCO"
	method="post" data-dojo-type="dijit.form.Form" action="<%=formMgr.getSubmitAction(response)%>">

	<input type=hidden value="" name=buttonName>

	<%
		//cr498 begin
		if (requireAuth) {
	%>

	<input type=hidden name="reCertification" value="<%=TradePortalConstants.INDICATOR_NO%>"> 
	<input type=hidden name="reCertOK"> 
	<input type=hidden name="logonResponse"> 
	<input type=hidden name="logonCertificate">

	<%
		} //cr498 end
	%>

	<%
		if (transaction.getAttribute("transaction_status").equals(
				TradePortalConstants.TRANS_STATUS_PROCESSED_BY_BANK))
			isProcessedByBank = true;

		// Store values such as the userid, security rights, and org in a
		// secure hashtable for the form.  Also store instrument and transaction
		// data that must be secured.
		Hashtable secureParms = new Hashtable();
		secureParms.put("login_oid", userSession.getUserOid());
		secureParms.put("owner_org_oid", userSession.getOwnerOrgOid());
		secureParms.put("login_rights", loginRights);

		secureParms.put("instrument_oid",
				instrument.getAttribute("instrument_oid"));
		secureParms.put("instrument_type_code", instrumentType);
		secureParms.put("instrument_status", instrumentStatus);

		secureParms.put("transaction_oid",
				transaction.getAttribute("transaction_oid"));
		secureParms.put("transaction_type_code", transactionType);
		secureParms.put("transaction_status", transactionStatus);

		secureParms.put("transaction_instrument_info",
				transaction.getAttribute("transaction_oid") + "/"
						+ instrument.getAttribute("instrument_oid") + "/"
						+ transactionType);

		// If the terms record doesn't exist, set its oid to 0.
		String termsOid = terms.getAttribute("terms_oid");
		if (termsOid == null)
			termsOid = "0";
		secureParms.put("terms_oid", termsOid);
		secureParms.put("TransactionCurrency", currencyCode);
		//IR - PAUK042248441 [BEGIN]
		//secureParms.put("RefNum", terms.getAttribute("reference_number"));
		secureParms.put("RefNum", StringFunction.xssHtmlToChars(terms
				.getAttribute("reference_number")));
		//IR - PAUK042248441 [END]
	%>
		<%-- Main Content Area starts here --%>
    <%-- error section goes above form content --%>
    <div class="formArea">
    <jsp:include page="/common/ErrorSection.jsp" />
    <%--Added to get attached documents as horizontal align --%>
	<%@ include file="fragments/Instruments-AttachDocuments.frag" %>
	<%-- Moved this frag here as its common for Tracer/Amend  --%>
	<%@ include file="fragments/PhraseLookupPrep.frag"%> 
		<div class="formContent">
			<%-- Form Content Area starts here --%>
			
	<% //CR 821 Added Repair Reason Section 
			  StringBuffer repairReasonWhereClause = new StringBuffer();
			  int  repairReasonCount = 0;
				
				/*Get all repair reason's count from transaction history table*/
				repairReasonWhereClause.append("p_transaction_oid = ?");
				repairReasonWhereClause.append(" and REPAIR_REASON is not null order by ACTION_DATETIME");
				//jgadela  R90 IR T36000026319 - SQL FIX
				Object sqlParamsRepCount[] = new Object[1];
				sqlParamsRepCount[0] =  transaction.getAttribute("transaction_oid") ;

				Debug.debug("repairReasonWhereCondition query : " + repairReasonWhereClause);
				repairReasonCount = DatabaseQueryBean.getCount("A_USER_OID", "transaction_history", repairReasonWhereClause.toString(), true, sqlParamsRepCount);
					
			if ( !(TradePortalConstants.TRANS_STATUS_PROCESSED_BY_BANK.equalsIgnoreCase(transactionStatus)) && repairReasonCount>0  ){%>
				<%=widgetFactory.createSectionHeader("", "TransactionHistory.RepairReason") %>
					<%@ include file="fragments/Transaction-RepairReason.frag" %>
				</div>
			<%} %> 

			<%
				//If we're in Amend mode do this...

				if (transactionType.equals(TransactionType.AMEND)) {
			%>
			<%=widgetFactory.createSectionHeader("1",
						"ExportCollectionAmend.General")%>
				<%@ include file="fragments/Transaction-EXP_OCO-AMD_General.frag"%>
			</div>
	
			<%=widgetFactory.createSectionHeader("2",
							"ExportCollectionAmend.InsttoBank")%>
				<%@ include file="fragments/Transaction-EXP_OCO-AMD_BankInstructions.frag"%>
			</div>
			<%=widgetFactory.createSectionHeader("3",
							"ExportCollectionAmend.NewPaymentTerms")%>
				<%@ include file="fragments/Transaction-EXP_OCO-AMD_NewPaymentTerms.frag"%>
			</div>
			<% if (TradePortalConstants.TRANS_STATUS_PROCESSED_BY_BANK.equalsIgnoreCase(transactionStatus) && repairReasonCount>0  ){%>
				<%=widgetFactory.createSectionHeader("4", "TransactionHistory.RepairReason", null, true) %>
				<%@ include file="fragments/Transaction-RepairReason.frag" %>
				</div>
		<%}%> 
					
			
			<%
				} else { //if we're in Tracer mode do this...
			%>
			<%=widgetFactory.createSectionHeader("1",
						"ExportCollectionAmend.General")%>
				<%@ include file="fragments/Transaction-EXP_OCO-TRC_General.frag"%>
			</div>
	
			<%=widgetFactory.createSectionHeader("2",
							"ExportCollectionTrace.InsttoBank")%>
				<%@ include file="fragments/Transaction-EXP_OCO-TRC_BankInstructions.frag"%>
			</div>
			<% if (TradePortalConstants.TRANS_STATUS_PROCESSED_BY_BANK.equalsIgnoreCase(transactionStatus) && repairReasonCount>0  ){%>
				<%=widgetFactory.createSectionHeader("3", "TransactionHistory.RepairReason", null, true) %>
				<%@ include file="fragments/Transaction-RepairReason.frag" %>
				</div>
			<%}%> 
			
			
	<%
		}
	%>
	
	
	</div><%--formContent--%>
	</div><%--formArea--%>
	
	<div class="formSidebar" data-dojo-type="widget.FormSidebar"
	data-dojo-props="title: '', form: 'TransactionEXP_OCO'">

	<jsp:include page="/common/Sidebar.jsp">		
		<jsp:param name="isReadOnly" value="<%=isReadOnly%>" />
		<jsp:param name="certAuthURL" value="<%=certAuthURL%>" />
		<jsp:param name="buttonPressed" value="<%=buttonPressed%>" />
        <jsp:param name="error" value="<%= error%>" />
        <jsp:param name="formName" value="0" />
        <jsp:param name="showLinks" value="true" /> 
	</jsp:include>
</div>

	<%--Sidebar ENDS here --%>
	<%-- Page Content Area ENDS here --%>
	<%=formMgr.getFormInstanceAsInputField(
					"Transaction-EXP_OCOForm", secureParms)%>

</form>
</div>
</div>

<%-- KMehta on 12 May 2014 @ Rel 90 for IR T36000025449 Start --%>
<jsp:include page="/common/Footer.jsp">
   <jsp:param name="autoSaveFlag"           value="<%=autoSaveFlag%>" />
   <jsp:param name="templateFlag"           value="<%=templateFlag%>" />
   <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>
<%-- KMehta on 12 May 2014 @ Rel 90 for IR T36000025449 End --%>
<jsp:include page="/common/SidebarFooter.jsp"/>

<script>

  <%-- cquinton 3/3/2013 add local var --%>
  var local = {};

<%
  if (!isReadOnly && InstrumentServices.isNotBlank(focusField)) {
%>
  require(["dijit/registry", "dojo/ready"],
    function(registry, ready ) {
      ready(function() {
        var focusFieldId = '<%= focusField %>';
        if ( focusFieldId ) {
          var focusField = registry.byId(focusFieldId);
          focusField.focus();
        }
      });
  });
<%
  }
%>

  <%-- cquinton 3/2/2013 local phrase lookup --%>
  require(["t360/phraseLookup"],
          function( phraseLookup ) {
    local.phraseLookupAppend = function(phraseSelect, textArea, textAreaMaxLength ) {
      phraseLookup.appendPhrase( phraseSelect, textArea, textAreaMaxLength, "_errorSection" );
    };
  });
</script>

</body>
</html>

<%
	// Finally, reset the cached document to eliminate carryover of
	// information to the next visit of this page.
	formMgr.storeInDocCache("default.doc", new DocumentHandler());
%>

<%!// Various methods are declared here (in alphabetical order).

	public String getExpressIndicator(boolean showIndicator) {
		if (showIndicator) {
			return "<span class=Express>#</span>";
		} else {
			return "";
		}
	}

	public String getRequiredIndicator(boolean showIndicator) {
		if (showIndicator) {
			return "<span class=Asterix>*</span>";
		} else {
			return "";
		}
	}%>
