<%--  
*******************************************************************************
                        LRQ - Reporting Codes

*******************************************************************************
--%>
      
<%-- 
 *
 *     RPasupulati                        
 *     CGI, Incorporated 
 *     All rights reserved
--%>

<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*, net.sf.json.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.html.*,java.math.*,java.util.*" %>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager"    scope="session"> </jsp:useBean>
<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session"> </jsp:useBean> 
<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session"> </jsp:useBean>
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean"     scope="session"> </jsp:useBean>
   
<%

response.setContentType("application/json");
response.setCharacterEncoding("UTF-8");



JSONObject repMap = new JSONObject();

 
String          userSecurityRights   = userSession.getSecurityRights();
String          userOid              = userSession.getUserOid();
String          rptgCodes1         	 = request.getParameter("rptgCodes1");
String          rptgCodes2         	 = request.getParameter("rptgCodes2");
String          bankGroupOid         	 = request.getParameter("bankgOid");


   	   //RPasupulati IR T36000032489
       repMap.put("ReportingCode1",getReportingCode(rptgCodes1,bankGroupOid,"payment_reporting_code_1"));
	   repMap.put("ReportingCode2",getReportingCode(rptgCodes2,bankGroupOid,"payment_reporting_code_2"));


//out.println(getResults(rMap));
out.println(repMap.toString());

%>

<%!

private String getReportingCode(String code,String bankGroupOid, String table ) {
	String val="";
	String codeText = "";
	String descText = "";

	if (InstrumentServices.isBlank(bankGroupOid) || InstrumentServices.isBlank(code)) return val;
	
	StringBuffer sql = new StringBuffer("select CODE, DESCRIPTION");
	sql.append(" from ").append(table);
	sql.append(" where p_bank_group_oid = ? and code = ?");
	
	try {
			DocumentHandler xmlDoc = DatabaseQueryBean.getXmlResultSet(sql.toString(), false, bankGroupOid, code);
			
			if (xmlDoc != null) {
				codeText =  xmlDoc.getAttribute("/ResultSetRecord/CODE");
				descText = xmlDoc.getAttribute("/ResultSetRecord/DESCRIPTION");
				val = codeText+"-"+descText;
				
			}
	}
	catch (Exception e) {
		e.printStackTrace();
	}
	return val; 

}


%>