<%--
*******************************************************************************
                              New Transaction Director Page

  Description:
    This is where the Create Transaction Mediator returns to after performing
  validation or creating a new transaction.  Several parameters are examined
  to determine what the next page should be.  Based on these parms, we will
  forward to the InstrumentSearch, the Template Search, NewTransactionStep3,
  or to the InstrumentNavigator page.

*******************************************************************************
--%>

<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.ams.tradeportal.busobj.webbean.*,com.ams.tradeportal.common.*,
                 com.ams.tradeportal.html.*,java.util.*,
                 com.amsinc.ecsg.util.*,com.amsinc.ecsg.frame.*,
                 com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.mediator.*" %>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session"></jsp:useBean>
<%
    Debug.debug("***START*************NEW TRANSACTION DIRECTOR*************START***");

    //GET DOC
    DocumentHandler doc = formMgr.getFromDocCache();
    Debug.debug("doc -> \n" + doc.toString());

    HttpSession theSession    = request.getSession(false);
    theSession.setAttribute("inNewTransactionArea", TradePortalConstants.INDICATOR_YES);
    //theSession.setAttribute("startPage", startPage);

    String copyType     = doc.getAttribute("/In/copyType");
    String mode         = doc.getAttribute("/In/mode");

    String logicalPage = "";
    String physicalPage = null;
    
    //Added for discrepancies - START 
    String startPage    = doc.getAttribute("/In/startPage");
    if (!InstrumentServices.isBlank(startPage))
    {
        theSession.setAttribute("startPage", startPage);
        
        String msgOid = doc.getAttribute("/In/mailMessageOid");
        if (!InstrumentServices.isBlank(msgOid))
            theSession.setAttribute("mailMessageOid", msgOid);
    }
    //Added for discrepancies - END


    if ( mode.equals(TradePortalConstants.EXISTING_INSTR) ) {
        if (InstrumentServices.isBlank(doc.getAttribute("/Out/Transaction/oid")))
        //No new transaction has been created
        {
            if (InstrumentServices.isBlank(doc.getAttribute("/In/instrumentOid")))
            {
                logicalPage = "InstrumentSearch";
            }
            else
            {
                logicalPage = "InstrumentNavigator";
            }
        }
        else
        //The new transaction has been created so we must go to Instrument Navigator
        {
            logicalPage = "InstrumentNavigator";
        }
    } 
    else if ( (copyType != null) && copyType.equals(TradePortalConstants.TRANSFER_ELC) )
    {
        logicalPage = "InstrumentSearch";
	    doc.setAttribute("/In/InstrumentSearchInfo/SearchInstrumentType", InstrumentType.EXPORT_DLC);
        formMgr.storeInDocCache("default.doc", doc);
    }
    else if (!InstrumentServices.isBlank(doc.getAttribute("/Out/Transaction/oid")))
    {
        logicalPage = "InstrumentNavigator";
    }
    else if (InstrumentServices.isBlank(copyType))
    {
        if (InstrumentServices.isBlank(doc.getAttribute("/In/instrumentOid")))
            logicalPage = "InstrumentSearch";
        else
            logicalPage = "NewTransactionStep3";
    }
    else if ((copyType != null) && copyType.equals(TradePortalConstants.FROM_INSTR))
    {
        logicalPage = "InstrumentSearch";
    }
    else if ((copyType != null) && copyType.equals(TradePortalConstants.FROM_TEMPL))
    {
        logicalPage = "TemplateSearch";
    }

    Debug.debug("Next Logical page is " + logicalPage);

    // Now, if the logical page is not blank, forward to the physical page for the
    // logical page.
    if (!logicalPage.equals("")) {
      physicalPage = NavigationManager.getNavMan().getPhysicalPage(logicalPage, request);
      Debug.debug("...and the physical page is " + physicalPage);
%>
      <jsp:forward page='<%=physicalPage%>' />
<%
    }
    // else we've fallen through to an unknown type, what should we do?
%>
