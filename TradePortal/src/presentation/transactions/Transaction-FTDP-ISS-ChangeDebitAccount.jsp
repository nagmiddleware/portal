<%--
**********************************************************************************
// BSL CR-655 02/22/11 
**********************************************************************************
--%> 

<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*, com.amsinc.ecsg.util.*, java.util.*,
                 com.amsinc.ecsg.html.*, com.amsinc.ecsg.web.*,com.ams.util.*,
                 com.ams.tradeportal.common.*, com.ams.tradeportal.html.*,
                 com.ams.tradeportal.busobj.webbean.*, com.ams.tradeportal.busobj.util.*" %>

<jsp:useBean id="beanMgr"     class="com.amsinc.ecsg.web.BeanManager"                   scope="session"></jsp:useBean>
<jsp:useBean id="formMgr"     class="com.amsinc.ecsg.web.FormManager"                   scope="session"></jsp:useBean>
<jsp:useBean id="resMgr"      class="com.amsinc.ecsg.util.ResourceManager"              scope="session"></jsp:useBean>
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>

<% 
String          userSecurityRights   = userSession.getSecurityRights();
String          userOid              = userSession.getUserOid();
String          bankGroupOid         = request.getParameter("BankGroupOID");
String          tranOid              = request.getParameter("TranOID");
String          isReadOnlyRptgCode1  = request.getParameter("IsReadOnlyRptgCode1");
String          isReadOnlyRptgCode2  = request.getParameter("IsReadOnlyRptgCode2");
String[]        selectedRptgCodes    = {"",""};
boolean[]       isReadOnlyRptgCode   = {"true".equalsIgnoreCase(isReadOnlyRptgCode1), 
										"true".equalsIgnoreCase(isReadOnlyRptgCode2)};


if (InstrumentServices.isBlank(userOid)) {
%>
	<jsp:forward page='<%= NavigationManager.getNavMan().getPhysicalPage("PageNotAvailable", request) %>'></jsp:forward>
<%
}
%>

<%@ include file="fragments/Transaction-FTDP-ISS-GetReportingCodes.frag" %>

<jsp:include page="fragments/Transaction-ISS-GetFXOnlineAvailable.jsp">
    <jsp:param name="BankGroupOID" value="<%=bankGroupOid%>" />
	<jsp:param name="format" value="html" />
</jsp:include>

<%
if (InstrumentServices.isNotBlank(tranOid)) {
	String dpTableName = "domestic_payment";
	String dpIdField = "domestic_payment_oid";

	StringBuffer dpWhereClause = new StringBuffer();
	dpWhereClause.append(" p_transaction_oid = ?" );
	
	StringBuffer updateStatement = new StringBuffer();
	updateStatement.append("update ").append(dpTableName);
	updateStatement.append(" set reporting_code_1 = null, reporting_code_2 = null");
	updateStatement.append(" where ").append(dpWhereClause);
	
	try {
		Long intOb = (tranOid !=null)?Long.parseLong(tranOid):null;
		
		Debug.debug("Executing statement: " + updateStatement.toString());
		Vector rowIDs = DatabaseQueryBean.selectRowsForUpdate(dpTableName,  
				dpIdField, dpWhereClause.toString(), updateStatement.toString(), false, new Object[]{intOb}, new Object[]{intOb});
		Debug.debug("Updated " + rowIDs.size() + " rows in " + dpTableName);
	}
	catch (Exception e) {
		e.printStackTrace();
	}
}
%>
