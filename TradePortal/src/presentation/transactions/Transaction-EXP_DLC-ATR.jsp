<%--
*******************************************************************************
               Export Letter of Credit Amend Transfer Page

  Description:
  This is the main driver for the Export Letter of Credit Amend Transfer page.  
  It handles data retrieval of terms and terms parties (or retrieval from the 
  input document) and creates the html page to display all the data for an 
  Export Letter of Credit Amend Transfer.
*******************************************************************************
--%>

<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,java.util.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.html.*,java.math.*,
                 com.amsinc.ecsg.util.DateTimeUtility" %>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager"
    scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" 
   scope="session">
</jsp:useBean>


<%-- ************** Data retrieval page setup begins here ****************  --%>

<%
  WidgetFactory widgetFactory = new WidgetFactory(resMgr, userSession);
  //cquinton 2/12/2013 add refdata manager for datepattern lookup
  ReferenceDataManager refData = ReferenceDataManager.getRefDataMgr();

  //PMitnala Rel8.3 IR T36000022009 - Incorrect ID for focusfield is causing javascript error on the page. 
  //Due to this error, buttons did not respond in Firefox browser. Hence correcting the ID.
  String focusField = "Increase";   // Default focus field

  // Various oid and status info from transaction and instruments used in
  // several places.
  String instrumentOid = "";         
  String transactionOid;
  String instrumentType;
  String instrumentStatus;
  String transactionType; 
  String transactionStatus;
  String rejectionIndicator        = "";
  String rejectionReasonText       = "";

  boolean isProcessedByBank = false;

  boolean getDataFromDoc;          // Indicates if data is retrieved from the
                                   // input doc cache or from the database
  boolean phraseSelected = false;  // Indicates if a phrase was selected.

  DocumentHandler doc;

  String loginLocale = userSession.getUserLocale();
  String loginRights = userSession.getSecurityRights();

  //cquinton 2/12/2013 lookup locale specific datePattern description from refData manager
  String datePattern = userSession.getDatePattern();
  String datePatternDisplay = refData.getDescr("DATEPATTERN", datePattern, loginLocale);
  String dateWidgetOptions = "placeHolder:'" + datePatternDisplay + "'";

  // Dates are stored in the database as one field but displayed on the
  // page as 3.  These variables hold the pieces for each date.
  String expiryDay           = "";
  String expiryMonth         = "";
  String expiryYear          = "";
  String latestShipmentDay   = "";   
  String latestShipmentMonth = "";
  String latestShipmentYear  = "";
  StringBuffer TransferAmount = new StringBuffer();
  StringBuffer newTransferAmount = new StringBuffer();

  // Applicant name and expiry date from the issue transaction.  Displayed in General section
  String applicantName = "";
  String currentExpiryDate = null;

  String activeTransOid = null;

  // Variables used for populating ref data dropdowns.
  String options;
  String defaultText;

  String bButtonPressed = ""; 
  String links = "";
  // These are the beans used on the page.

  TransactionWebBean transaction  = (TransactionWebBean)
                                      beanMgr.getBean("Transaction");
  InstrumentWebBean instrument    = (InstrumentWebBean)
                                      beanMgr.getBean("Instrument");
  TemplateWebBean template        = (TemplateWebBean)
                                      beanMgr.getBean("Template");
  TermsWebBean terms              = null;

  ShipmentTermsWebBean shipmentTerms = beanMgr.createBean(ShipmentTermsWebBean.class, "ShipmentTerms");

  // We need the amount of the active transaction to display.  Create a webbean
  // to hold this data.
  TransactionWebBean activeTransaction = beanMgr.createBean(TransactionWebBean.class, "Transaction");

  // Get the document from the cache.  We'll may use it to repopulate the 
  // screen if returning from another page, a save, validation, or any other
  // mediator called from this page.  Otherwise, we assume an instrument oid
  // and transaction oid was passed in.

  doc = formMgr.getFromDocCache();

 bButtonPressed = doc.getAttribute("/In/Update/ButtonPressed");
  
  Vector error = null;
  error = doc.getFragments("/Error/errorlist/error");
  
//if newTransaction, set session variable so it persists
  String newTransaction = null;
  if (doc.getDocumentNode("/Out/newTransaction")!=null) {
    newTransaction = doc.getAttribute("/Out/newTransaction");
    session.setAttribute("newTransaction", newTransaction);
    System.out.println("found newTransaction = "+ newTransaction);
  } 
  else {
    newTransaction = (String) session.getAttribute("newTransaction");
    if ( newTransaction==null ) {
      newTransaction = TradePortalConstants.INDICATOR_NO;
    }
    System.out.println("used newTransaction from session = " + newTransaction);
  }


/******************************************************************************
  We are either entering the page or returning from somewhere.  These are the 
  conditions for how to populate the web beans.  Data comes from either the
  database or the doc cache (/In section) with some variation.

  Mode           Condition                      Populate Beans From
  -------------  ----------------------------   --------------------------------
  Enter Page     no /In/Transaction in doc      Instrument and Template web
                                                beans already populated, get
                                                data for Terms and TermsParty
                                                web beans from database

  return from    /Out/PhraseLookupInfo exists   doc cache (/In); but use Phrase
  Phrase Lookup                                 LookupInfo text (from /Out) to
                                                replace a specific phrase text
                                                in the /In document before 
                                                populating

  return from    /Error/maxerrorseverity < 1    Same as Enter Page (data is
  Transaction                                   retrieved from database)
    mediator
    (no error)

  return from    /Error/maxerrorseverity > 0    doc cache (/In)
  Transaction    
    mediator
    (error)
******************************************************************************/

  // Assume we get the data from the doc.
  getDataFromDoc = true;

  String maxError = doc.getAttribute("/Error/maxerrorseverity");
  if (maxError != null && maxError.compareTo(String.valueOf(ErrorManager.WARNING_ERROR_SEVERITY)) <= 0)
  {
     // No errors, so don't get the data from doc.
     getDataFromDoc = false;
  }
  //ir cnuk113043991 - check to see if transaction needs to be refreshed
  // if so, refresh it and do not get data from doc as it is wrong
  if ( "Y".equals(doc.getAttribute("/Out/Refresh/Transaction")) ) {
     transaction.getDataFromAppServer();
     getDataFromDoc = false;
  }
  if (doc.getDocumentNode("/In/Transaction") == null)
  {
     // No /In/Transaction means we've never looked up the data.
     Debug.debug("No /In/Transaction section - get data from database");
     getDataFromDoc = false;
  }
  if (doc.getDocumentNode("/Out/PhraseLookupInfo") != null)
  {
     // A Looked up phrase exists.  Replace it in the /In document.
     Debug.debug("Found a looked-up phrase");
     getDataFromDoc = true;
     phraseSelected = true;

     String result = doc.getAttribute("/Out/PhraseLookupInfo/Result");
     if (result.equals(TradePortalConstants.INDICATOR_YES))
     {
        // Take the looked up and appended phrase text from the /Out section
        // and copy it to the /In section.  This allows the beans to be 
        // properly populated.
        String xmlPath = doc.getAttribute("/In/PhraseLookupInfo/text_path");
        xmlPath = "/In" + xmlPath;

        doc.setAttribute(xmlPath, doc.getAttribute("/Out/PhraseLookupInfo/NewText"));

        // If we returned from the phrase lookup without errors, set the focus
        // to the correct field.  Otherwise, don't set the focus so the user can
        // see the error.
        if (maxError != null && maxError.compareTo(String.valueOf(ErrorManager.WARNING_ERROR_SEVERITY)) < 0)
        {
           focusField = doc.getAttribute("/In/PhraseLookupInfo/text_field_name");
        }
     }
     else
     {
        // We do nothing because nothing was looked up.  We stil want to get
        // the data from the doc.
     }
  }

  if (getDataFromDoc)
  {
     Debug.debug("Populating beans from doc cache");

     // Populate the beans from the input doc.
     try
     {
        instrument.populateFromXmlDoc(doc.getComponent("/In"));
        transaction.populateFromXmlDoc(doc.getComponent("/In"));
        template.populateFromXmlDoc(doc.getComponent("/In"));

        terms = (TermsWebBean) beanMgr.getBean("Terms");
        terms.populateFromXmlDoc(doc, "/In");

        // Some of the data for this transaction is stored as part of the shipment terms
        terms.populateFirstShipmentFromXml(doc);

        expiryDay   = doc.getAttribute("/In/Terms/expiry_day");
        expiryMonth = doc.getAttribute("/In/Terms/expiry_month");
        expiryYear  = doc.getAttribute("/In/Terms/expiry_year");

        latestShipmentDay   = 
               doc.getAttribute("/In/Terms/ShipmentTermsList/latest_shipment_day");
        latestShipmentMonth = 
               doc.getAttribute("/In/Terms/ShipmentTermsList/latest_shipment_month");
        latestShipmentYear  = 
               doc.getAttribute("/In/Terms/ShipmentTermsList/latest_shipment_year");

        activeTransOid = instrument.getAttribute("active_transaction_oid");
        if (!InstrumentServices.isBlank(activeTransOid))
        {
           activeTransaction.getById(activeTransOid);
        }
     }
     catch (Exception e)
     {
        out.println("Contact Administrator: "
              + "Unable to reload data after returning to page. "
              + "Error is " + e.toString());
     }
  }
  else
  {
     Debug.debug("populating beans from database");
     // We will perform a retrieval from the database.
     // Instrument and Transaction were already retrieved.  Get
     // the rest of the data

     terms = transaction.registerCustomerEnteredTerms();

     activeTransOid = instrument.getAttribute("active_transaction_oid");
     if (!InstrumentServices.isBlank(activeTransOid))
     {
        activeTransaction.getById(activeTransOid);
     }

     // Some of the data for this transaction is stored as part of the shipment terms
     terms.populateFirstShipmentFromDatabase();

     // Because date fields are represented as three fields on the page,
     // we need to parse out the date values into three fields.  Pre-mediator
     // logic puts these three fields back together for the terms bean.
     String date = terms.getAttribute("expiry_date");
     expiryDay   = TPDateTimeUtility.parseDayFromDate(date);
     expiryMonth = TPDateTimeUtility.parseMonthFromDate(date);
     expiryYear  = TPDateTimeUtility.parseYearFromDate(date);

     date                = terms.getFirstShipment().getAttribute("latest_shipment_date");
     latestShipmentDay   = TPDateTimeUtility.parseDayFromDate(date);
     latestShipmentMonth = TPDateTimeUtility.parseMonthFromDate(date);
     latestShipmentYear  = TPDateTimeUtility.parseYearFromDate(date);
  }

  BigInteger requestedSecurityRight = SecurityAccess.EXPORT_LC_CREATE_MODIFY;
%>
  <%@ include file="fragments/Transaction-PageMode.frag" %>


<%

  // Now we need to calculate the computed new lc amount total.  This involves 
  // using the active transaction's instrument amount, the amount entered by the user,
  // performing some validation and then doing the calculation.
  MediatorServices   mediatorServices = new MediatorServices();
  mediatorServices.getErrorManager().setLocaleName(resMgr.getResourceLocale());
  boolean            badAmountFound   = false;
  String             amountValue      = null;
  String             displayAmount    = null;
  String             incrDecrValue    = TradePortalConstants.INCREASE;
  String             currencyCode     = activeTransaction.getAttribute("copy_of_currency_code");
  BigDecimal         originalAmount   = BigDecimal.ZERO;
  BigDecimal         offsetAmount     = BigDecimal.ZERO;
  BigDecimal         calculatedTotal  = BigDecimal.ZERO;

  // Convert the active transaction amount to a double
  try
  {
     amountValue    = activeTransaction.getAttribute("instrument_amount");
     originalAmount = new BigDecimal(amountValue);
  }
  catch (Exception e)
  {
     originalAmount = BigDecimal.ZERO;
  }

  if (getDataFromDoc && doc.getAttribute("/In/Terms/amount") != null) 
  {
     // Convert the amount for this terms record to a double.  If we're getting
     // data from the doc, use the value in the document and attempt to convert
     // to a double.  It may fail due to invalid number.  If so, we'll post 
     // an error and set a flag to prevent the calculation from being 
     // displayed.
     //
     // Furthermore, since the displayed terms amount is always displayed as a 
     // positive number (the Increase/Decrease dropdown represents the sign).
     // Therefore, we may need to set the double amount negative.

     amountValue   = doc.getAttribute("/In/Terms/amount");
     incrDecrValue = doc.getAttribute("/In/Terms/IncreaseDecreaseFlag");

     try
     {
        amountValue = NumberValidator.getNonInternationalizedValue(amountValue, loginLocale, false);

        try
        {
           offsetAmount = new BigDecimal(amountValue);
        }
        catch (Exception e)
        {
           offsetAmount = BigDecimal.ZERO;
        }

        if (incrDecrValue.equals(TradePortalConstants.DECREASE))
        {
           offsetAmount = offsetAmount.negate();
        }

        // Don't format the number because we only came from the doc
        displayAmount = doc.getAttribute("/In/Terms/amount");
     }
     catch (Exception e)
     {
        // Unable to unformat number so the display value is the same value
        // the user typed in.
        displayAmount  = amountValue;
        badAmountFound = true;

        mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, 
                                                      TradePortalConstants.INVALID_CURRENCY_FORMAT, amountValue);
        mediatorServices.addErrorInfo();

        doc.addComponent("/Error", mediatorServices.getErrorDoc());

        formMgr.storeInDocCache("default.doc", doc);
     }
  }
  else
  {
     // We're getting the amount from the database so convert to a positive
     // number if necessary and set the Increase/Decrease dropdown accordingly.
     try
     {
        amountValue  = terms.getAttribute("amount");
        offsetAmount = new BigDecimal(amountValue);
     }
     catch (Exception e)
     {
        offsetAmount = BigDecimal.ZERO;
     }

     if (offsetAmount.doubleValue() < 0)
     {
        incrDecrValue = TradePortalConstants.DECREASE;
     }

     if( InstrumentServices.isBlank( amountValue )  && !isReadOnly) {
   	    displayAmount = "";
     }
     else{
        displayAmount = TPCurrencyUtility.getDisplayAmount(offsetAmount.abs().toString(), currencyCode, loginLocale);
     }
  }
//Srinivasu_D IR#T36000035537 ER9.1.0.2 01/01/2015 - handling minus
	 if(StringFunction.isNotBlank(amountValue)){
	 char amtChar[] = amountValue.toCharArray();
		if(amtChar[0] == '-'){
			amountValue = amountValue.substring(1,amountValue.length());
		}
	 }
	 //Srinivasu_D IR#T36000035537 ER9.1.0.2 01/01/2015 - End 

  calculatedTotal = originalAmount.add(offsetAmount);

  TransferAmount.append(currencyCode);
  TransferAmount.append(" ");
  TransferAmount.append(TPCurrencyUtility.getDisplayAmount(originalAmount.toString(), currencyCode, loginLocale));

  newTransferAmount.append(currencyCode);
  newTransferAmount.append(" ");  
  newTransferAmount.append(TPCurrencyUtility.getDisplayAmount(calculatedTotal.toString(), currencyCode, loginLocale));
  
  transactionType   = transaction.getAttribute("transaction_type_code");
  transactionStatus = transaction.getAttribute("transaction_status");
  
  // Get the transaction rejection indicator to determine whether to show the rejection reason text
  rejectionIndicator = transaction.getAttribute("rejection_indicator");
  rejectionReasonText = transaction.getAttribute("rejection_reason_text");
   
  instrumentType    = instrument.getAttribute("instrument_type_code");
  instrumentStatus  = instrument.getAttribute("instrument_status");

  Debug.debug("Instrument Type " + instrumentType);
  Debug.debug("Instrument Status " + instrumentStatus);
  Debug.debug("Transaction Type " + transactionType);
  Debug.debug("Transaction Status " + transactionStatus);


  // Create documents for the phrase dropdowns. First check the cache (no need
  // to recreate if they already exist).  After creating, place in the cache.
  // (InstrumentCloseNavigator.jsp cleans these up.)

  DocumentHandler phraseLists = formMgr.getFromDocCache(TradePortalConstants.PHRASE_LIST_DOC);
  if (phraseLists == null) phraseLists = new DocumentHandler();

  DocumentHandler addlCondDocList = phraseLists.getFragment("/AddlCond");
  if (addlCondDocList == null)
  {
     addlCondDocList = PhraseUtility.createPhraseList(
                                    TradePortalConstants.PHRASE_CAT_ADDL_COND, 
                                    userSession, formMgr, resMgr);
     phraseLists.addComponent("/AddlCond", addlCondDocList);
  }

  DocumentHandler spclInstrDocList = phraseLists.getFragment("/SpclInstr");
  if (spclInstrDocList == null)
  {
     spclInstrDocList = PhraseUtility.createPhraseList(
                                    TradePortalConstants.PHRASE_CAT_SPCL_INST, 
                                    userSession, formMgr, resMgr);
     phraseLists.addComponent("/SpclInstr", spclInstrDocList);
  }

  formMgr.storeInDocCache(TradePortalConstants.PHRASE_LIST_DOC, phraseLists);

  ////////////////////////////////////////////////////////////////////////
  // Retrieve the latest information of applicatnt name and expiry date.  
  // They are displayed in general section.
  ////////////////////////////////////////////////////////////////////////

  // Retrieve the latest information of expiry date from instrument.
  currentExpiryDate = instrument.getAttribute("copy_of_expiry_date");

  // Retrieve the applicant name from the bank release terms of the latest advise or amendment transaction on the original instrument.
  StringBuffer issueTransactionSQL = new StringBuffer();
  issueTransactionSQL.append("select orig_p.name")
                     .append(" from instrument orig_i, transaction orig_tr, terms orig_t, terms_party orig_p")
                     .append(" where orig_i.instrument_oid = ? ")
                     .append("   and orig_tr.transaction_oid =")
                     .append("       (select max(transaction_oid)")
                     .append("          from transaction")
                     .append("         where transaction_status_date = (select max(transaction_status_date)")
                     .append("                                            from transaction")
                     .append("                                           where p_instrument_oid = orig_i.instrument_oid")
                     .append("                                             and (transaction_type_code = ? or transaction_type_code = ? or transaction_type_code = ?)")
                     .append("                                             and transaction_status = ?)")
                     .append("           and p_instrument_oid = orig_i.instrument_oid")
                     .append("           and (transaction_type_code = ? or transaction_type_code = ? or transaction_type_code = ?)")
                     .append("           and transaction_status = ?)")
                     .append("   and orig_tr.c_bank_release_terms_oid = orig_t.terms_oid")
                     .append("   and (orig_p.terms_party_oid = orig_t.c_first_terms_party_oid")
                     .append("       or orig_p.terms_party_oid = orig_t.c_second_terms_party_oid")
                     .append("       or orig_p.terms_party_oid = orig_t.c_third_terms_party_oid")
                     .append("       or orig_p.terms_party_oid = orig_t.c_fourth_terms_party_oid")
                     .append("       or orig_p.terms_party_oid = orig_t.c_fifth_terms_party_oid)")
                     .append("   and orig_p.terms_party_type = ?");
  Object sqlParamsTranRel[] = new Object[10];
  sqlParamsTranRel[0] =  instrument.getAttribute("related_instrument_oid");
  sqlParamsTranRel[1] =  TransactionType.ADVISE;
  sqlParamsTranRel[2] =  TransactionType.AMEND;
  sqlParamsTranRel[3] =  TransactionType.CHANGE;
  
  sqlParamsTranRel[4] =  TradePortalConstants.TRANS_STATUS_PROCESSED_BY_BANK;
  sqlParamsTranRel[5] =  TransactionType.ADVISE;
  sqlParamsTranRel[6] =  TransactionType.AMEND;
  sqlParamsTranRel[7] =  TransactionType.CHANGE;
  sqlParamsTranRel[8] =  TradePortalConstants.TRANS_STATUS_PROCESSED_BY_BANK;
  sqlParamsTranRel[9] =  TradePortalConstants.APPLICANT;
  
  DocumentHandler issueTransactionResult = DatabaseQueryBean.getXmlResultSet(issueTransactionSQL.toString(), false, sqlParamsTranRel);
  if (issueTransactionResult!=null) {
     applicantName = issueTransactionResult.getAttribute("/ResultSetRecord(0)/NAME");
  }

%>


<%-- ********************* HTML for page begins here ********************* --%>

<%
  if (isTemplate)
  {
%>
    <jsp:include page="/common/ButtonPrep.jsp" />
<%
  }

  // Some of the retrieval logic above may have set a focus field.  Otherwise,
  // we'll use the initial value for focus.
  String onLoad = "";
%>

<%-- Body tag included as part of common header --%>
<%
  // The navigation bar is only shown when editing templates.  For transactions
  // it is not shown ti minimize the chance of leaving the page without properly
  // unlocking the transaction.
  String showNavBar = TradePortalConstants.INDICATOR_NO;
  if (isTemplate)
  {
    showNavBar = TradePortalConstants.INDICATOR_YES;
  }

  String pageTitleKey;
	if ( TradePortalConstants.INDICATOR_YES.equals(newTransaction) && !(TransactionType.AMEND).equals(transaction.getAttribute("transaction_type_code"))) {
	  pageTitleKey = "SecondaryNavigation.NewInstruments";
	  userSession.setCurrentPrimaryNavigation("NavigationBar.NewInstruments");
	} else {
	  pageTitleKey = "SecondaryNavigation.Instruments";
	  userSession.setCurrentPrimaryNavigation("NavigationBar.Transactions");
	}
	String helpUrl = "customer/amend_transfer_exp_lc.htm";

  // Auto save the form when time-out if not readonly.  
  // (Header.jsp will check for auto-save setting of the corporate org).
  String autoSaveFlag = isReadOnly ? TradePortalConstants.INDICATOR_NO : TradePortalConstants.INDICATOR_YES;
  String templateFlag = isTemplate ? TradePortalConstants.INDICATOR_YES : TradePortalConstants.INDICATOR_NO;

  //cquinton 11/19/2012 pass info for recent instruments into header
  String newRecentTransactionOid = transaction.getAttribute("transaction_oid");
  String newRecentInstrumentId = instrument.getAttribute("complete_instrument_id");
  String newRecentTransactionType = transaction.getAttribute("transaction_type_code");
  String newRecentTransactionStatus = transaction.getAttribute("transaction_status");
%>

<jsp:include page="/common/Header.jsp">
  <jsp:param name="autoSaveFlag"           value="<%=autoSaveFlag%>" />
  <jsp:param name="templateFlag"           value="<%=templateFlag%>" />
  <jsp:param name="newRecentTransactionOid" value="<%=newRecentTransactionOid%>" />
  <jsp:param name="newRecentInstrumentId"  value="<%=newRecentInstrumentId%>" />
  <jsp:param name="newRecentTransactionType" value="<%=newRecentTransactionType%>" />
  <jsp:param name="newRecentTransactionStatus" value="<%=newRecentTransactionStatus%>" />
</jsp:include>

<div class="pageMain">
<div class="pageContent">
<% //cr498 begin
   //Include ReAuthentication frag in case re-authentication is required for
   //authorization of transactions for this client
   String certAuthURL = "";
   Cache reCertCache = (Cache)TPCacheManager.getInstance().getCache(TradePortalConstants.CLIENT_BANK_CACHE);
   DocumentHandler CBCResult = (DocumentHandler)reCertCache.get(userSession.getClientBankOid());	
   String requireTranAuth = CBCResult.getAttribute("/ResultSetRecord(0)/REQUIRE_TRAN_AUTH");
   boolean requireAuth = InstrumentAuthentication.requireTransactionAuthentication(
       requireTranAuth,InstrumentAuthentication.TRAN_AUTH__EXP_DLC_ATR);
   if (requireAuth) {
%>   
      <%@ include file="/logon/fragments/openReauthenticationWindow.frag" %>
<%
   }
   //cr498 end
%>
<form id="Transaction-EXP_DLC-Form" name="Transaction-EXP_DLC-Form" method="post" data-dojo-type="dijit.form.Form" action="<%=formMgr.getSubmitAction(response)%>">

  <input type=hidden value="" name="buttonName">

<% //cr498 begin
  if (requireAuth) {
%> 

  <input type=hidden name="reCertification" value="<%=TradePortalConstants.INDICATOR_NO%>">
  <input type=hidden name="reCertOK">
  <input type=hidden name="logonResponse">
  <input type=hidden name="logonCertificate">

<%
  } //cr498 end
%> 

<%
  if(transaction.getAttribute("transaction_status").equals(TradePortalConstants.TRANS_STATUS_PROCESSED_BY_BANK))
	  isProcessedByBank = true;

  // Store values such as the userid, security rights, and org in a
  // secure hashtable for the form.  Also store instrument and transaction
  // data that must be secured.
  Hashtable secureParms = new Hashtable();

  secureParms.put("login_oid",             userSession.getUserOid());
  secureParms.put("owner_org_oid",         userSession.getOwnerOrgOid());
  secureParms.put("login_rights",          loginRights);

  secureParms.put("instrument_oid",        instrument.getAttribute("instrument_oid"));
  secureParms.put("instrument_type_code",  instrumentType);
  secureParms.put("instrument_status",     instrumentStatus);

  secureParms.put("transaction_oid",       transaction.getAttribute("transaction_oid"));
  secureParms.put("transaction_type_code", transactionType);
  secureParms.put("transaction_status",    transactionStatus);

  secureParms.put("ShipmentOid", terms.getFirstShipment().getAttribute("shipment_oid"));

  secureParms.put("transaction_instrument_info", 
  	transaction.getAttribute("transaction_oid") + "/" + 
	instrument.getAttribute("instrument_oid") + "/" + 
	transactionType);

  // If the terms record doesn't exist, set its oid to 0.
  String termsOid = terms.getAttribute("terms_oid");
  if (termsOid == null)
  {
     termsOid = "0";
  }

  secureParms.put("terms_oid", termsOid);
  secureParms.put("TransactionCurrency", currencyCode);

  if (isTemplate)
  {
    secureParms.put("template_oid", template.getAttribute("template_oid"));
    secureParms.put("opt_lock", template.getAttribute("opt_lock"));
  }

  if (isTemplate) 
  {
%>
    <%@ include file="fragments/TemplateHeader.frag" %>

    <%@ include file="fragments/Transaction-EXP_DLC-ATR-Links.frag" %>
<%
  } 
  else
  {

  }
%>
 
<jsp:include page="/common/PageHeader.jsp">
   <jsp:param name="titleKey" value="<%=pageTitleKey%>" />
   <jsp:param name="item1Key" value="SecondaryNavigation.ExportLCAmendTransfer" />
   <jsp:param name="helpUrl"  value="<%=helpUrl%>" />
</jsp:include>

<jsp:include page="/common/TransactionSubHeader.jsp" />


	<%-- error section goes above form content --%>
		        <div class="formArea">
		          <jsp:include page="/common/ErrorSection.jsp" />
<% // [BEGIN] IR-YVUH032343792 - jkok %>
  <%@ include file="fragments/Instruments-AttachDocuments.frag" %>
<% // [END] IR-YVUH032343792 - jkok %>

				  <div class="formContent">
 <%	if (rejectionIndicator.equals(TradePortalConstants.INDICATOR_YES)
    || rejectionIndicator.equals(TradePortalConstants.INDICATOR_X))
  {
%>
	<%=widgetFactory.createSectionHeader("0", "Rejection Reason") %>
     <%@ include file="fragments/Transaction-RejectionReason.frag" %>
     </div>
<%
  }
%>
<% //CR 821 Added Repair Reason Section 
StringBuffer repairReasonWhereClause = new StringBuffer();
int  repairReasonCount = 0;
	
	/*Get all repair reason's count from transaction history table*/
	repairReasonWhereClause.append("p_transaction_oid = ?");
	repairReasonWhereClause.append(" and REPAIR_REASON is not null order by ACTION_DATETIME");
	//jgadela  R90 IR T36000026319 - SQL FIX
	Object sqlParamsRepCnt1[] = new Object[1];
	sqlParamsRepCnt1[0] =  transaction.getAttribute("transaction_oid") ;
	Debug.debug("repairReasonWhereCondition query : " + repairReasonWhereClause);
	repairReasonCount = DatabaseQueryBean.getCount("A_USER_OID", "transaction_history", repairReasonWhereClause.toString(), true, sqlParamsRepCnt1);
			
if ( !(TradePortalConstants.TRANS_STATUS_PROCESSED_BY_BANK.equalsIgnoreCase(transactionStatus)) && repairReasonCount>0  ){%>
	<%=widgetFactory.createSectionHeader("", "TransactionHistory.RepairReason") %>
		<%@ include file="fragments/Transaction-RepairReason.frag" %>
	</div>
<%} %> 
	
	<%= widgetFactory.createSectionHeader("1", "TransferELCAmend.General") %>
         <%@ include file="fragments/Transaction-EXP_DLC-ATR-General.frag"%> 			
	</div> <%-- General Tab Ends Here--%>
  
  	<%= widgetFactory.createSectionHeader("2", "TransferELCAmend.OtherConditionsOfTransfer") %>
		<%@ include  file="fragments/Transaction-EXP_DLC-ATR-OtherConditions.frag"%>
	</div> <%-- Other Conditions Tab Ends Here --%>
	
	<%= widgetFactory.createSectionHeader("3", "TransferELCAmend.InstructionsToBank") %>
		<%@ include file="fragments/Transaction-EXP_DLC-ATR-BankInstructions.frag"%>
	</div> <%-- Bank Instructions Tab Ends Here --%>
  
  <% if (TradePortalConstants.TRANS_STATUS_PROCESSED_BY_BANK.equalsIgnoreCase(transactionStatus) && repairReasonCount>0 ){%>
		<%=widgetFactory.createSectionHeader("4", "TransactionHistory.RepairReason", null, true) %>
		<%@ include file="fragments/Transaction-RepairReason.frag" %>
		</div>
 <%} %> 
  

 </div><%--formContent--%>
</div><%--formArea--%>

<div class="formSidebar" data-dojo-type="widget.FormSidebar" data-dojo-props="form: 'Transaction-EXP_DLC-Form'">
    <jsp:include page="/common/Sidebar.jsp">
		<jsp:param name="links" value="<%=links%>" />
		<jsp:param name="isReadOnly" value="<%=isReadOnly%>" />
		<jsp:param name="certAuthURL" value="<%=certAuthURL%>" />
		<jsp:param name="buttonPressed" value="<%=bButtonPressed%>" />
		<jsp:param name="error" value="<%= error%>" />
		<jsp:param name="formName" value="0" />
		<jsp:param name="isTemplate" value="<%=isTemplate%>"/>
		 <jsp:param name="showLinks" value="true" />  
		 <jsp:param name="isNewTransaction" value="<%=newTransaction%>"/>
	</jsp:include>
</div> <%--closes sidebar area--%>
 
 
 
  <%@ include file="fragments/PhraseLookupPrep.frag" %>

  <%= formMgr.getFormInstanceAsInputField("Transaction-EXP_DLC-Form", secureParms) %>

</form>  
</div> 
</div> 


<jsp:include page="/common/Footer.jsp">
   <jsp:param name="autoSaveFlag"           value="<%=autoSaveFlag%>" />
   <jsp:param name="templateFlag"           value="<%=templateFlag%>" />
   <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>

<jsp:include page="/common/SidebarFooter.jsp"/>

<script>

  <%-- cquinton 3/3/2013 add local var --%>
  var local = {};

<%
  if (!isReadOnly && InstrumentServices.isNotBlank(focusField)) {
%>
  require(["dijit/registry", "dojo/ready"],
    function(registry, ready ) {
      ready(function() {
        var focusFieldId = '<%= focusField %>';
        if ( focusFieldId ) {
          var focusField = registry.byId(focusFieldId);
          focusField.focus();
        }
      });
  });
  
  var xhReq;
  function setNewAmount(){
	  require(["dijit/registry"],
			    function(registry ) {
		  			var existingLCAmount = <%=originalAmount%>;
	  				var ccyCode = '<%=currencyCode%>';
	  				var flag;
	  				if(registry.byId("Increase").checked){
	  					flag='Increase';
	  				}else{
	  					flag='Decrease';
	  				}
	  				var incDecValue = registry.byId("TransactionAmount").value;
	  				var newLCAmount = 0;	  				
	  				
		  				if(flag == 'Increase'){
		  					newLCAmount = Number(existingLCAmount) + Number(incDecValue);
		  				}else if(flag == 'Decrease'){
		  					newLCAmount = Number(existingLCAmount) - Number(incDecValue);
		  				}		  				
		  				if(isNaN(newLCAmount)){
		  					newLCAmount = existingLCAmount;
		  				}
	  				if (!xhReq)
	  					xhReq = getXmlHttpRequest();
	  				var req = "Amount=" + newLCAmount + "&currencyCode="+ccyCode;
	  				xhReq.open("GET", '/portal/transactions/AmountFormatter.jsp?'+req, true);
	  				xhReq.onreadystatechange = updateAmount;
	  				xhReq.send(null);
	  			  });}

  function getXmlHttpRequest()
  {
    var xmlHttpObj;
    if (window.XMLHttpRequest) {
      xmlHttpObj = new XMLHttpRequest();
    }
    else {
      try {
        xmlHttpObj = new ActiveXObject("Msxml2.XMLHTTP");
      } catch (e) {
        try {
          xmlHttpObj = new ActiveXObject("Microsoft.XMLHTTP");
        } catch (e) {
          xmlHttpObj = false;
        }
      }
    }
    console.log(xmlHttpObj);
    return xmlHttpObj;
  }
  
	  			function updateAmount(){
	  				require(["dijit/registry","dojo/dom","dojo/domReady!"], function(registry,dom){
	  				 if (xhReq.readyState == 4 && xhReq.status == 200) {	 
	  					var ccyCode = '<%=currencyCode%>';
	  					 var formattedAmt = ccyCode + " " + xhReq.responseText;	  					
	  					dom.byId("TransactionAmount2").innerHTML = formattedAmt;
	  			    	<%-- registry.byId("TransactionAmount2").set('value',xhReq.responseText); --%>
	  			      } 
	  				});
	  			  }
 
  
<%
  }
%>

  <%-- cquinton 3/2/2013 local phrase lookup --%>
  require(["t360/phraseLookup"],
          function( phraseLookup ) {
    local.phraseLookupAppend = function(phraseSelect, textArea, textAreaMaxLength ) {
      phraseLookup.appendPhrase( phraseSelect, textArea, textAreaMaxLength, "_errorSection" );
    };
  });
</script>

</body>
</html>

<%
   // Finally, reset the cached document to eliminate carryover of
   // information to the next visit of this page.
   doc.removeAllChildren("/");

   formMgr.storeInDocCache("default.doc", doc);
%>
