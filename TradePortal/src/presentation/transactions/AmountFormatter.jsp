<%--for the ajax call to format display amount--%>

<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.html.*,java.math.*,
                 java.util.ArrayList, java.util.Hashtable" %>



<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session">
</jsp:useBean>

<% 
  //parameters -
  //other variable setup
  String loginLocale = userSession.getUserLocale();
  String loginRights = userSession.getSecurityRights();
  String currCode = request.getParameter("currencyCode");
  String decimalPlaces = request.getParameter("curDecimalPlaces");
  int curDecimal = 0;
  String amount = "";
  
  if(StringFunction.isNotBlank(request.getParameter("Amount"))){	
	  /* KMehta Rel 8400 IR-T36000016434 on 26/12/2013 */
//	  amount = TPCurrencyUtility.getDisplayAmount(request.getParameter("Amount").toString(),request.getParameter("currencyCode"), loginLocale);
  	amount = TPCurrencyUtility.getDecimalDisplayAmount(request.getParameter("Amount").toString(),2, loginLocale);
  	/* KMehta Rel 8400 IR-T36000016434 on 26/12/2013 */
  }  
  /* Vsarkary Rel 9100 IR-T36000031287 */ 
  if(StringFunction.isNotBlank(request.getParameter("AppliedAmount"))){	
	  if(StringFunction.isNotBlank(decimalPlaces)){
  		curDecimal = Integer.parseInt(decimalPlaces);
  		amount = TPCurrencyUtility.getDecimalDisplayAmount(request.getParameter("AppliedAmount").toString(),curDecimal, loginLocale);
	  }else{
		  amount = TPCurrencyUtility.getDecimalDisplayAmount(request.getParameter("AppliedAmount").toString(),2, loginLocale);
	  }
  	/* Vsarkary Rel 9100 IR-T36000031287 */
  }  
  response.setContentType("text/html");
  out.println(amount);
  
  
  %>
