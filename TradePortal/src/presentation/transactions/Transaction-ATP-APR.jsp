<%--
*******************************************************************************
                  Approval to Pay Response Page

  Description:
*******************************************************************************
--%>

<%--
 *
 *     Copyright   2007                         
 *     CGI 
 *     All rights reserved
--%>
<%--CR-419 Krishna java.text.* is added--%>
<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.html.*,com.ams.tradeportal.busobj.util.*,java.math.*,java.text.*, java.util.Vector, java.util.Hashtable, java.util.Date,
                 java.util.List, java.util.ArrayList" %>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager"
    scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" 
   scope="session">  
</jsp:useBean>


<%-- ************** Data retrieval page setup begins here ****************  --%>

<%
	WidgetFactory widgetFactory = new WidgetFactory(resMgr);
//  String focusField = "TransactionAmount";   // Default focus field
  String focusField = null;

  // Various oid and status info from transaction and instruments used in
  // several places.
  String instrumentOid = "";
  String transactionOid;
  String instrumentType;
  String instrumentStatus;
  String transactionType; 
  String transactionStatus;
  //CR-419 Krishna Begin
  TermsPartyWebBean    counterParty  = null;
  String    counterPartyOid   = null;
  String    counterPartyName  = null;
  String    presentationDate  = null;
  //CR-419 Krishna End

  boolean getDataFromDoc;          // Indicates if data is retrieved from the
                                   // input doc cache or from the database

  DocumentHandler doc;

  String loginLocale = userSession.getUserLocale();
  String loginRights = userSession.getSecurityRights();

  // Variables used for populating ref data dropdowns.
  String options;
  String defaultText;

  // These are the beans used on the page.

  TransactionWebBean transaction  = (TransactionWebBean)
                                      beanMgr.getBean("Transaction");
  InstrumentWebBean instrument    = (InstrumentWebBean)
                                      beanMgr.getBean("Instrument");
  TemplateWebBean template        = null;
  TermsWebBean terms              = null;

  // Get the document from the cache.  We'll may use it to repopulate the 
  // screen if returning from another page, a save, validation, or any other
  // mediator called from this page.  Otherwise, we assume an instrument oid
  // and transaction oid was passed in.

  doc = formMgr.getFromDocCache();
  
  Vector error = null;
  error = doc.getFragments("/Error/errorlist/error");
  
  String buttonClicked = request.getParameter("buttonName");

  // Debug.debug("doc from cache is " + doc.toString());

/******************************************************************************
  We are either entering the page or returning from somewhere.  These are the 
  conditions for how to populate the web beans.  Data comes from either the
  database or the doc cache (/In section) with some variation.

  Mode           Condition                      Populate Beans From
  -------------  ----------------------------   --------------------------------
  Enter Page     no /In/Transaction in doc      Instrument and Template web
                                                beans already populated, get
                                                data for Terms and TermsParty
                                                web beans from database

  return from    /Out/PhraseLookupInfo exists   doc cache (/In); but use Phrase
  Phrase Lookup                                 LookupInfo text (from /Out) to
                                                replace a specific phrase text
                                                in the /In document before 
                                                populating

  return from    /Error/maxerrorseverity < 1    Same as Enter Page (data is
  Transaction                                   retrieved from database)
    mediator
    (no error)

  return from    /Error/maxerrorseverity > 0    doc cache (/In)
  Transaction    
    mediator
    (error)
******************************************************************************/

  // Assume we get the data from the doc.
  getDataFromDoc = true;

  String maxError = doc.getAttribute("/Error/maxerrorseverity");
  if (maxError != null && maxError.compareTo(
              String.valueOf(ErrorManager.WARNING_ERROR_SEVERITY)) < 0) {
     // No errors, so don't get the data from doc.
     getDataFromDoc = false;
  }
  //ir cnuk113043991 - check to see if transaction needs to be refreshed
  // if so, refresh it and do not get data from doc as it is wrong
  if ( "Y".equals(doc.getAttribute("/Out/Refresh/Transaction")) ) {
     transaction.getDataFromAppServer();
     getDataFromDoc = false;
  }

  if (doc.getDocumentNode("/In/Transaction") == null) {
     // No /In/Transaction means we've never looked up the data.
     Debug.debug("No /In/Transaction section - get data from database");
     getDataFromDoc = false;
  }

  if (doc.getDocumentNode("/Out/PhraseLookupInfo") != null) {
     // A Looked up phrase exists.  Replace it in the /In document.
     Debug.debug("Found a looked-up phrase");
     getDataFromDoc = true;

     // Take the looked up and appended phrase text from the /Out section
     // and copy it to the /In section.  This allows the beans to be 
     // properly populated.
     String xmlPath = doc.getAttribute("/In/PhraseLookupInfo/text_path");
     xmlPath = "/In" + xmlPath;

     doc.setAttribute(xmlPath, 
                      doc.getAttribute("/Out/PhraseLookupInfo/NewText"));

     // If we returned from the phrase lookup without errors, set the focus
     // to the correct field.  Otherwise, don't set the focus so the user can
     // see the error.
     if (maxError != null && maxError.compareTo(
              String.valueOf(ErrorManager.WARNING_ERROR_SEVERITY)) < 0) {
       focusField = doc.getAttribute("/In/PhraseLookupInfo/text_field_name");
     }
  }

  if (getDataFromDoc) {
     Debug.debug("Populating beans from doc cache");

     // Populate the beans from the input doc.
     try {
        instrument.populateFromXmlDoc(doc.getComponent("/In"));
        transaction.populateFromXmlDoc(doc.getComponent("/In"));

        terms = (TermsWebBean) beanMgr.getBean("Terms");
        terms.populateFromXmlDoc(doc, "/In");

     } catch (Exception e) {
        out.println("Contact Administrator: "
              + "Unable to reload data after returning to page. "
              + "Error is " + e.toString());
     }
  } else {
     Debug.debug("populating beans from database");
     // We will perform a retrieval from the database.
     // Instrument and Transaction were already retrieved.  Get
     // the rest of the data

     terms = transaction.registerCustomerEnteredTerms();

  }
  
  // Get the amount and currency code for display from terms

  String currencyCode = terms.getAttribute("amount_currency_code");
  String displayAmount = TPCurrencyUtility.getDisplayAmount(
                         terms.getAttribute("amount"), currencyCode, 
						 loginLocale);
   //CR-419 Krishna Begin
   // Retrieve the name of the counter party associated with the instrument
   counterPartyOid = instrument.getAttribute("a_counter_party_oid");
   if (!InstrumentServices.isBlank(counterPartyOid))
   {
      counterParty = beanMgr.createBean(TermsPartyWebBean.class, "TermsParty");

      counterParty.getById(counterPartyOid);

      counterPartyName = counterParty.getAttribute("name");
   }
   else
   {
      counterPartyName = "";
   }
   //CR-419 Krishna End

  String discrepancyInstructions = 
  		terms.getAttribute("import_discrepancy_instr");     

  if (discrepancyInstructions == null)
  	 discrepancyInstructions = "";

						 
  transactionType = transaction.getAttribute("transaction_type_code");
  transactionStatus = transaction.getAttribute("transaction_status");

  instrumentType = instrument.getAttribute("instrument_type_code");
  instrumentStatus = instrument.getAttribute("instrument_status");

  Debug.debug("Instrument Type " + instrumentType);
  Debug.debug("Instrument Status " + instrumentStatus);
  Debug.debug("Transaction Type " + transactionType);
  Debug.debug("Transaction Status " + transactionStatus);

  // Now determine the mode for how the page operates (readonly, etc.)  
  //Groy - 02/22/2010 - IR LAUJ081255999 - Add Begin
 // Commented the line below to send DISCREPANCY_CREATE_MODIFY instead of APPROVAL_TO_PAY_CREATE_MODIFY
 //BigInteger requestedSecurityRight = SecurityAccess.APPROVAL_TO_PAY_CREATE_MODIFY;

    BigInteger requestedSecurityRight = SecurityAccess.DISCREPANCY_CREATE_MODIFY;

 //Groy - 02/22/2010 - IR LAUJ081255999 - Add End
%>

  <%@ include file="fragments/Transaction-PageMode.frag" %>

<%
  // Create documents for the phrase dropdowns. First check the cache (no need
  // to recreate if they already exist).  After creating, place in the cache.
  // (InstrumentCloseNavigator.jsp cleans these up.)

  DocumentHandler phraseLists = formMgr.getFromDocCache("PhraseLists");
  if (phraseLists == null) phraseLists = new DocumentHandler();

  DocumentHandler discrInstrDocList = phraseLists.getFragment("/DiscrInstr");
  if (discrInstrDocList == null) {
     discrInstrDocList = PhraseUtility.createPhraseList(
                                    TradePortalConstants.PHRASE_CAT_DCR_INST, 
                                    userSession, formMgr, resMgr);
     phraseLists.addComponent("/DiscrInstr", discrInstrDocList);
  }
  
  formMgr.storeInDocCache("PhraseLists", phraseLists);
  
  // if not returning from Error, determine the focus field
  // if there was an error, focusField would have been populated
  
  if (focusField == null || focusField.equals(""))
  {
	  if (discrepancyInstructions.equals(TradePortalConstants.ATP_INVOICE_APPROVED))
    	       focusField  = "TradePortalConstants.ATP_INVOICE_APPROVED";
	  else if (discrepancyInstructions.equals(TradePortalConstants.ATP_INVOICE_REFUSED))
    	       focusField  = "TradePortalConstants.ATP_INVOICE_REFUSED";
	  else if (discrepancyInstructions.equals(TradePortalConstants.ATP_OTHER))
    	focusField = "TradePortalConstants.ATP_OTHER";
  	  else focusField = "TradePortalConstants.ATP_INVOICE_APPROVED";
  }
  
%>


<%-- ********************* HTML for page begins here ********************* --%>

<%
  // Some of the retrieval logic above may have set a focus field.  Otherwise,
  // we'll use the initial value for focus.
  String onLoad = ""; 

  // Auto save the form when time-out if not readonly.  
  // (Header.jsp will check for auto-save setting of the corporate org).
  String autoSaveFlag = isReadOnly ? TradePortalConstants.INDICATOR_NO : TradePortalConstants.INDICATOR_YES;
  String templateFlag = isTemplate ? TradePortalConstants.INDICATOR_YES : TradePortalConstants.INDICATOR_NO;
%>

<% //cr498 begin
   //Include ReAuthentication frag in case re-authentication is required for
   //authorization of transactions for this client
   String certAuthURL = "";
   Cache reCertCache = (Cache)TPCacheManager.getInstance().getCache(TradePortalConstants.CLIENT_BANK_CACHE);
   DocumentHandler CBCResult = (DocumentHandler)reCertCache.get(userSession.getClientBankOid());	
   String requireTranAuth = CBCResult.getAttribute("/ResultSetRecord(0)/REQUIRE_TRAN_AUTH");
   boolean requireAuth = InstrumentAuthentication.requireTransactionAuthentication(
       requireTranAuth,InstrumentAuthentication.TRAN_AUTH__ATP_APR);
   if (requireAuth) {
%>   
      <%@ include file="/logon/fragments/openReauthenticationWindow.frag" %>
<%
   }
   //cr498 end

  //cquinton 11/19/2012 pass info for recent instruments into header
  String newRecentTransactionOid = transaction.getAttribute("transaction_oid");
  String newRecentInstrumentId = instrument.getAttribute("complete_instrument_id");
  String newRecentTransactionType = transaction.getAttribute("transaction_type_code");
  String newRecentTransactionStatus = transaction.getAttribute("transaction_status");
  
 

%> 

<jsp:include page="/common/Header.jsp">
  <jsp:param name="autoSaveFlag"           value="<%=autoSaveFlag%>" />
  <jsp:param name="templateFlag"           value="<%=templateFlag%>" />
  <jsp:param name="newRecentTransactionOid" value="<%=newRecentTransactionOid%>" />
  <jsp:param name="newRecentInstrumentId"  value="<%=newRecentInstrumentId%>" />
  <jsp:param name="newRecentTransactionType" value="<%=newRecentTransactionType%>" />
  <jsp:param name="newRecentTransactionStatus" value="<%=newRecentTransactionStatus%>" />
</jsp:include>

	<div class="pageMain">
		<div class="pageContent">
			<form id="TransactionAPR" name="TransactionAPR" method="post" data-dojo-type="dijit.form.Form" action="<%=formMgr.getSubmitAction(response)%>">
  				<input type=hidden value="" name=buttonName>

				<% if (requireAuth) { %> 
				  <input type=hidden name="reCertification" value="<%=TradePortalConstants.INDICATOR_NO%>">
				  <input type=hidden name="reCertOK">
				  <input type=hidden name="logonResponse">
				  <input type=hidden name="logonCertificate">
				<% } %>

				<%
					// Store values such as the userid, security rights, and org in a
					// secure hashtable for the form.  Also store instrument and transaction
					// data that must be secured.
					Hashtable secureParms = new Hashtable();
					secureParms.put("login_oid", userSession.getUserOid());
					secureParms.put("owner_org_oid", userSession.getOwnerOrgOid());
					secureParms.put("login_rights", loginRights);
					secureParms.put("instrument_oid", instrument.getAttribute("instrument_oid"));
					secureParms.put("instrument_type_code", instrumentType);
					secureParms.put("instrument_status", instrumentStatus);
					secureParms.put("transaction_oid", transaction.getAttribute("transaction_oid"));
					secureParms.put("transaction_type_code", transactionType);
					secureParms.put("transaction_status", transactionStatus);
					secureParms.put("transaction_instrument_info", 
					 	transaction.getAttribute("transaction_oid") + "/" + 
						instrument.getAttribute("instrument_oid") + "/" + 
						transactionType);
					
					// If the terms record doesn't exist, set its oid to 0.
					String termsOid = terms.getAttribute("terms_oid");
					if (termsOid == null) termsOid = "0";
					
					secureParms.put("terms_oid", termsOid);
					String helpSensitiveLink; 
					helpSensitiveLink = "customer/approval_to_pay_response.htm";
				%>
				
				<jsp:include page="/common/PageHeader.jsp">
				   <jsp:param name="titleKey" value="Approval to Pay Response" />
				   <jsp:param name="helpUrl"  value="customer/approval_to_pay_response.htm" />
				</jsp:include>
				<%--MEer Rel 8.3 IR-22540 --%>
					<jsp:include page="/common/TransactionSubHeader.jsp" />
				

  				<%@ include file="fragments/PhraseLookupPrep.frag" %>
<%--	Below IF condition is not necessary. Hence commenting the code.  --%>  				
<%--   				<%	  					
				if(!transaction.getAttribute("transaction_status").equals("AUTHORIZED")){ 		
  				%> --%>
            <%-- error section goes above form content --%>
            <div class="formArea">
             	<jsp:include page="/common/ErrorSection.jsp" />
  				<div class="formContent">
  				
 <% //MEer Rel 8.3 IR-19746 Added Repair Reason Section  based on  CR 821 
StringBuffer repairReasonWhereClause = new StringBuffer();
int  repairReasonCount = 0;
	
	/*Get all repair reason's count from transaction history table*/
	repairReasonWhereClause.append("p_transaction_oid = ?");
	repairReasonWhereClause.append(" and REPAIR_REASON is not null order by ACTION_DATETIME");

	Debug.debug("repairReasonWhereCondition query : " + repairReasonWhereClause);
	//jgadela  R90 IR T36000026319 - SQL FIX
	Object sqlParams[] = new Object[1];
	sqlParams[0] = transaction.getAttribute("transaction_oid");
	repairReasonCount = DatabaseQueryBean.getCount("A_USER_OID", "transaction_history", repairReasonWhereClause.toString(), true, sqlParams);
	
if ( !(TradePortalConstants.TRANS_STATUS_PROCESSED_BY_BANK.equalsIgnoreCase(transactionStatus)) && repairReasonCount>0  ){%>
	<%=widgetFactory.createSectionHeader("", "TransactionHistory.RepairReason") %>
		<%@ include file="fragments/Transaction-RepairReason.frag" %>
		</div>
<%} %> 
  	
  					<%@ include file="fragments/Transaction-ATP_RESPONSE-html.frag" %>
<%-- Naveen- Commented the below frag and added it to the above frag- Transaction-ATP_RESPONSE-html.frag as Attachments need to be aligned above the Conditions --%>  					
					<%-- <%@ include file="fragments/Transaction-Documents.frag" %> --%>
					
			<% if (TradePortalConstants.TRANS_STATUS_PROCESSED_BY_BANK.equalsIgnoreCase(transactionStatus) && repairReasonCount>0  ){%>
		<%=widgetFactory.createSectionHeader("", "TransactionHistory.RepairReason") %>
		<%@ include file="fragments/Transaction-RepairReason.frag" %>
		</div>
<%} %> 
		<%-- MEerupula IR T36000021153 Removed the code changes made for adding transaction log as it is not needed here --%>
		
			</div>
			</div>
			
				
				<% String links = ""; %>
				<div class="formSidebar" data-dojo-type="widget.FormSidebar" data-dojo-props="form: 'TransactionAPR'">
					<jsp:include page="/common/Sidebar.jsp">
						<jsp:param value="<%=links %>" name="links"/>
				        <jsp:param name="isReadOnly" value="<%=isReadOnly%>" />
						<jsp:param name="certAuthURL" value="<%=certAuthURL%>" />
						<jsp:param name="showLinks" value="false" />
						<jsp:param name="buttonPressed" value="<%=buttonClicked%>" />
						<jsp:param name="error" value="<%=error %>" />
					</jsp:include>
				</div> <%--closes sidebar area--%>
<%--	Below ESLE condition is not necessary. Hence commenting the code.  --%> 
<%--   				<%
  					}else{
  				%>
  				<div class="formContentNoSidebar">
  					<%@ include file="fragments/Transaction-ATP_RESPONSE-html.frag" %>
					<%@ include file="fragments/Transaction-Documents.frag" %>
				</div>
  				<%
  					}//else end
  				%> --%>						
				<%= formMgr.getFormInstanceAsInputField("Transaction-ATP_APR-Form", secureParms) %>
			</form>
		</div>
	</div>

<%-- KMehta on 12 May 2014 @ Rel 90 for IR T36000025449 Start --%>
<jsp:include page="/common/Footer.jsp">
   <jsp:param name="autoSaveFlag"           value="<%=autoSaveFlag%>" />
   <jsp:param name="templateFlag"           value="<%=templateFlag%>" />
   <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>
<%-- KMehta on 12 May 2014 @ Rel 90 for IR T36000025449 End --%>
<jsp:include page="/common/SidebarFooter.jsp" />

<script language="JavaScript">


 
<%-- cquinton 3/3/2013 add local var --%>
  var local = {};


  require(["dijit/registry", "dojo/ready"],
    function(registry, ready ) {
      ready(function() {
    	 <%
    	  if (!isReadOnly && InstrumentServices.isNotBlank(focusField)) {
    	%>
        var focusFieldId = '<%=focusField%>';
          var focusField = registry.byId(focusFieldId);
          if ( focusField ) {
          	focusField.focus();
          }
          <%
    	  }
    	 %>
          
        
        
          
        <%--  MEerupula IR T36000021153 Removed the code changes made for adding transaction log as it is not needed here  --%>
         
        
      });
  });

  <%-- cquinton 3/2/2013 local phrase lookup --%>
  require(["t360/phraseLookup"],
          function( phraseLookup ) {
    local.phraseLookupAppend = function(phraseSelect, textArea, textAreaMaxLength ) {
      phraseLookup.appendPhrase( phraseSelect, textArea, textAreaMaxLength, "_errorSection" );
    };
  });
  

</script>

</body>
</html>

<%
   // Finally, reset the cached document to eliminate carryover of
   // information to the next visit of this page.
   formMgr.storeInDocCache("default.doc", new DocumentHandler());
%>

<script language="JavaScript">


  <%--
  // This function is used to display a popup message confirming that the user
  // wishes to authorize the ATP response and at the same time delete the associated Notice.
  --%>
  function confirmAuthorizeDelete() {
    var   confirmMessage = "<%=resMgr.getText("Response.AuthorizeDeletePopupMessage", 
                                              TradePortalConstants.TEXT_BUNDLE) %>";

            if (!confirm(confirmMessage))
             {
                formSubmitted = false;
                return false;
             } else {
       return true;
    }
  }



</script>
