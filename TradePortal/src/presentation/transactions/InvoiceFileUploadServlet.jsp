<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.web.*, com.amsinc.ecsg.frame.*, com.amsinc.ecsg.util.*, java.util.*, 
                 com.ams.tradeportal.common.*" %>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session"></jsp:useBean>

<%

   String nextPhysicalPage = null;

   try
   {
      ServletContext context = this.getServletConfig().getServletContext();

      /* KMehta - 13 Mar 2015 - Rel9.3 IR-T36000029753 - Change  - Begin */
      response.setContentType("text/html; charset=UTF-8 ");
      /* KMehta - 13 Mar 2015 - Rel9.3 IR-T36000029753 - Change  - End */

      RequestDispatcher requestDispatcher = context.getRequestDispatcher("/InvoiceFileUploadServlet");
       
      NavigationManager nif = NavigationManager.getNavMan();

      nextPhysicalPage = nif.getPhysicalPage(formMgr.getCurrPage(), request);


      requestDispatcher.include(request, response);
%>
     <jsp:forward page='<%= nextPhysicalPage %>' />
<%
   }
   catch (Exception e)
   {
      System.out.println("Exception caught in InvoiceFileUploadServlet.jsp");
      e.printStackTrace();
      //ByteArrayOutputStream bytes = new ByteArrayOutputStream();
      //PrintWriter writer = new PrintWriter(bytes, true);
      //e.printStackTrace(writer);
      //System.out.println(bytes.toString());
      String link = formMgr.getUrlForLogicalPage(nextPhysicalPage, "", response);
      response.sendRedirect(response.encodeRedirectUrl (link));
   }
%>
