
<%--
**********************************************************************************
  Trade Transactions Home

  Description:  
     This page is used as the main Transactions page for trade instruments.

**********************************************************************************
--%> 

<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*, com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*, com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*, com.ams.tradeportal.html.*,com.ams.tradeportal.busobj.*,com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.ams.tradeportal.busobj.webbean.*, com.ams.tradeportal.busobj.util.*" %>

<jsp:useBean id="beanMgr"     class="com.amsinc.ecsg.web.BeanManager"            scope="session"></jsp:useBean>
<jsp:useBean id="formMgr"     class="com.amsinc.ecsg.web.FormManager"            scope="session"></jsp:useBean>
<jsp:useBean id="resMgr"      class="com.amsinc.ecsg.util.ResourceManager"       scope="session"></jsp:useBean>
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>


 
<%

	MailMessage message                     = null;
	MediatorServices mediatorServices		= new MediatorServices();
	long messageOid                         = 0;
	Long oid                                = null;
	String jPylonDate                       = DateTimeUtility.getGMTDateTime();		//Find out what today's date is...
	
	String subject = request.getParameter("subject");
	String text = request.getParameter("text");
	String complete_instrument_id = request.getParameter("complete_instrument_id");
	String encryptedMailMessageOid;
	
	String    userOid                 = userSession.getUserOid();
	String    corpOrgOid        	  = userSession.getOwnerOrgOid();
	String    loginRights             = userSession.getSecurityRights();
	int ifInsert = 0; // Flag to check whether insert is successful. SUCCESS = 0, FAIL = 1
	
	message = (MailMessage)EJBObjectFactory.createClientEJB(formMgr.getServerLocation(), "MailMessage");
	message.newObject();
	
	messageOid = message.getAttributeLong("message_oid");
	oid = new Long(messageOid);
	message.setAttribute("message_oid", oid.toString());  
	message.setAttribute("message_subject", subject);
	message.setAttribute("message_text", text);
	message.setAttribute("is_reply", "N");
	message.setAttribute("assigned_to_corp_org_oid",corpOrgOid);
	message.setRelatedInstrument(complete_instrument_id, corpOrgOid);
	message.setAttribute("message_source_type", TradePortalConstants.PORTAL);
	message.setAttribute("last_entry_user_oid", userOid);
	message.setAttribute("last_update_date", jPylonDate );
	message.setAttribute("discrepancy_flag", TradePortalConstants.INDICATOR_NO);
	message.setAttribute("atp_notice_flag", TradePortalConstants.INDICATOR_NO);
	message.setAttribute("message_status", TradePortalConstants.DRAFT);
	message.setAttribute("unread_flag", TradePortalConstants.INDICATOR_NO);
	message.setAttribute("assigned_to_user_oid", userOid);

	try {
	    message.save(true);
	} catch (Exception e) {
		ifInsert = 1;
	    System.out.println("General exception caught in erformSave - " + e.toString());
	    e.printStackTrace();
	} finally {
	    Debug.debug("Finally block of message Save");
	}

	encryptedMailMessageOid = EncryptDecrypt.encryptStringUsingTripleDes( oid.toString(), userSession.getSecretKey() );
	out.println("{\"flag\":\"" + ifInsert + "\",\"oid\":\"" + encryptedMailMessageOid.trim()+"\"}");

%>
