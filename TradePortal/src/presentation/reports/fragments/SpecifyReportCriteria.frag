<div id="SpecifyReportCriteriaDIV">

  <%--cquinton 2/18/2013 fix issues with ie7 formatting - also remove table tags--%>
  <div class="instruction">
    <%= resMgr.getText("Reports.Prompt",TradePortalConstants.TEXT_BUNDLE) %>
  </div>
  <br/>

  <input type=hidden name="noOfControls" id="noOfControls" value="<%=iPrompts%>">

  <%--cquinton 2/18/2013 use a table to ensure all field labels line up vertically--%>
  <table>
<%
  isRequiredMsg= resMgr.getText("ATT02","ErrorMessages");
  for (int i=0;i<iPrompts;i++ ) {
    //for each prompt
    webiPrompt = widPromptsList.getItem(i);  
%>

    <tr>
      <td valign="top">
        <input type=hidden name="<%="Prompt"+i %>" id="<%="Prompt"+i %>" value="<%=webiPrompt.getName()%>">

        <div>
          <%=widgetFactory.createInlineLabel( "", webiPrompt.getName() )%>
	</div>
      </td>
      <td>

<%
 //jgadela 01/16/2014 Rel 8.4 IR T36000023438 [START]- Modified logic to take date format as per selected user date format
  	String datePattern = userSession.getDatePattern();
  	String dateWidgetOptions = "placeHolder:'" + datePattern + "'"; 
  
    if (webiPrompt.getType() == PromptType.Mono) {
        String[] pValue = null;
	  if (webiPrompt.getObjectType() == ObjectType.DATE) {
      	//previousValue = "dd/mm/yyyy";
      	out.println(widgetFactory.createSearchDateField("ListOfValues" + i,"","class='char10'", dateWidgetOptions));
      }
	   else{
	   
		  pValue = webiPrompt.getPreviousValues();
	      if (pValue.length > 0) {
			 previousValue = pValue [0];
		  }  else {
			//MEer  05/09/2014 IR-15640 Prompt fields defaults to null value instead of blank
			// previousValue = ""; 
			   previousValue = "null";
		  }
		  out.println(widgetFactory.createTextField( "ListOfValues" + i, "",previousValue, "25", false, false, false, "", "", "none" ));
	 }
	 //jgadela 01/16/2014 Rel 8.4 IR T36000023438 [END]
%>
        <input type=hidden value="" id=refr name=refr>

<%   
    }
    else {
      listOfValues = (String [])webiPrompt.getCurrentValues();
      Lov widLov = webiPrompt.getLOV();
      int iLov = widLov.getMaxColumns();
      Values widValues = widLov.getAllValues();
      int iCount = widValues.getCount(); 
%>

      <%--2/18/2013 todo: this should be a dojo widget, but as we don't have a multiple select type
          finalized yet, leave as is--%>
      <select id="<%="ListOfValues" + i%>" name="<%="ListOfValues"+i %>"

<%
      if ( webiPrompt.getType() == PromptType.Multi ) { 
%>
                multiple size=4>
<%  
      }
      else { 
%>        
                size=1>
<% 
      }
      int index;
      String sSelected;
      index = 1; 
%>
          <option value="" selected><%="Please select a prompt value "%></option>
<%
      for( int j = 0; j < iCount ; j++) {
        sSelected = "";   
%>
	<%-- VishalS Cross Site Scripting encoding for widValues --%>
          <option value="<%=StringFunction.xssCharsToHtml(widValues.getValue(j))%>" <%=sSelected%>><%=StringFunction.xssCharsToHtml(widValues.getValue(j))%></option>
<%
        index++;
      } 
%>
        </select>
<%   
    }
%>
      </td>
    </tr>
<%
  } 
%>
  </table>
  <br/>
  
  <div>
    <button data-dojo-type="dijit.form.Button"  name="SpecifyReportButton" id="SpecifyReportButton" type="button" >
      <%=resMgr.getText("SpecifyReport.ShowReportText",TradePortalConstants.TEXT_BUNDLE)%>
      <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">	
        viewReports('SpecifyReportButton');
      </script>	
    </button>
<%
  newLink = new StringBuffer();
  newLink.append(formMgr.getLinkAsUrl("goToReportsHome", response));
%>
    <button data-dojo-type="dijit.form.Button"  name="CancelReportSave" id="CancelReportSave" type="button">
      <%=resMgr.getText("common.CancelText",TradePortalConstants.TEXT_BUNDLE)%>
      <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">				
        openButtonURL("<%=newLink.toString()%>");       					
      </script>	
    </button> 
  </div>

</div>

