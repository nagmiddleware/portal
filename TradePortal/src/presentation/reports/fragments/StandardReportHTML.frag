<%--cquinton 2/14/2013 ir#11445 fix formatting of page subheaders so they can be expanded/collapsed--%>
<div id="divider2" class="subHeaderDivider"></div>

<div class="pageSubHeader">

<%
  if(pageMode.equalsIgnoreCase(TradePortalConstants.INDICATOR_YES))
  {
	//Retrieve the reports in HTML format and print the body   
	//Setup the page navigation 
	if(pgn.getCurrent()==1)
	{
		prevPage = 1;
	}
	else
	{
		prevPage = pgn.getCurrent()-1;
	}
	if(pgn.getCurrent() == totalNoPages)
	{
		nextPage = totalNoPages ;
	}
	else
	{
		nextPage = pgn.getCurrent()+1;
	}
	if( totalNoPages != 1)
	{		
		
        for(int iLoop=0; iLoop < totalNoPages; iLoop++){
        dropdownOptions.append("<option value=\"");
        dropdownOptions.append(iLoop+1);
        dropdownOptions.append("\"");
        if ((Integer.valueOf(iLoop).compareTo(Integer.valueOf(pgn.getCurrent()-1))) == 0)	
        {
       	 dropdownOptions.append("selected");
        }
        dropdownOptions.append(">");
        dropdownOptions.append(iLoop+1);    
        dropdownOptions.append("</option>");
        
        }
        
%>

  <input type=hidden value='<%=sToken%>' name="sToken" id="sToken">

	<Table align="left" style="margin-top: -4px; margin-bottom: -6px;" >
			<TR align="center">
			<%//01-14-2014 Rel 8.4 CR-854 T36000023777 [BEGIN]- label translations
			String pageLabel =  resMgr.getText("listview.page",TradePortalConstants.TEXT_BUNDLE, reportingPageLabelLan);
			String ofLabel =  resMgr.getText("listview.of",TradePortalConstants.TEXT_BUNDLE, reportingPageLabelLan);%>
			
				<TD><%=widgetFactory.createSubLabel(pageLabel)%></TD>					
				<TD><%=widgetFactory.createSelectField("PaginationDropdown","", "", dropdownOptions.toString(), false,false, false, "onChange='filterPages();' style=\"width: 50px;\"", "", "none")%></TD>
				<TD><%=widgetFactory.createSubLabel(ofLabel)%>	 
					<%=widgetFactory.createSubLabel(Integer.toString(totalNoPages))%>	
						
				</TD>
				<%-- Komal ANZ Issue No.359 BEGINS --%>
				
				<%if(pgn.getCurrent() != 1){%>
					<TD>
					<span class="paginationFirst">
					<a id="firstPageLink" href="/portal/reports/StandardReportsPage.jsp?goToPage=1&pageCall=Y&sToken=<%=sToken%>&oid=<%=StringFunction.xssCharsToHtml(request.getParameter("oid"))%>&expandedPage=<%=StringFunction.xssCharsToHtml(expandedPage)%>"></a>
					</span>
					</TD>
					<TD>
					<span class="paginationPrev">
					<a id="prevPageLink" href="/portal/reports/StandardReportsPage.jsp?goToPage=<%=prevPage%>&pageCall=Y&sToken=<%=sToken%>&oid=<%=StringFunction.xssCharsToHtml(request.getParameter("oid"))%>&expandedPage=<%=StringFunction.xssCharsToHtml(expandedPage)%>"></a>
					</span>
					</TD>
				<%} else {%>
					<TD>
					<span class="paginationFirstDisabled">
					<a href="#"  onclick="return false;"></a>
					</span>
					</TD>
					<TD>
					<span class="paginationPrevDisabled">
					<a href="#" onclick="return false;"></a>
					</span>
					</TD>
				<%}%>
			
			
				<%if(pgn.getCurrent() != totalNoPages){%>
					<TD>
					<span class="paginationNext">
					<a id="nextPageLink" href="/portal/reports/StandardReportsPage.jsp?goToPage=<%=nextPage%>&pageCall=Y&sToken=<%=sToken%>&oid=<%=StringFunction.xssCharsToHtml(request.getParameter("oid"))%>&expandedPage=<%=StringFunction.xssCharsToHtml(expandedPage)%>"></a>
					</span>
					</TD>
					<TD>
					<span class="paginationLast">
					<a id="lastPageLink" href="/portal/reports/StandardReportsPage.jsp?goToPage=<%=totalNoPages%>&pageCall=Y&sToken=<%=sToken%>&oid=<%=StringFunction.xssCharsToHtml(request.getParameter("oid"))%>&expandedPage=<%=StringFunction.xssCharsToHtml(expandedPage)%>"></a>
					</span>
					</TD>
				<%} else {%>
					<TD>
					<span class="paginationNextDisabled">
					<a href="#"  onclick="return false;"></a>
					</span>
					</TD>
					<TD>
					<span class="paginationLastDisabled">
					<a href="#" onclick="return false;"></a>
					</span>
					</TD>
				<%}%>
				
				<%-- Komal ANZ Issue No.359 ENDS --%>
			</TR>
		</TABLE>

<%
	}
%>

  <jsp:include page="/common/ReportsTypesSubHeaderBar.jsp">
    <jsp:param name="sToken" value="<%=sToken%>"/> 	
    <jsp:param name="divID" value="standardReportsPDFId"/>	
  </jsp:include>

  <div style="clear:both;"></div>

</div>

<%
  //cquinton 2/14/2013 ir#11445 on first entry to page, set explicit height of the report viewport.
  // also add ability to expand/collapse page.  on subsequent entry if page if expanded
  // ensure we set the expanded styles correctly.  see expandPage() function on main page for
  // more details
  String reportSectionStyle = "";
  if ("Y".equals(expandedPage)) {
    reportSectionStyle="position: relative; width:auto; height: auto; overflow: visible; *zoom: 1;";
  }
  else {
    reportSectionStyle="position: relative; width:100%; height: 375px; overflow: auto; *zoom: 1;";
  }
%>
<div id="reportSection" style="<%=reportSectionStyle%>">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td width="20" nowrap>&nbsp;</td>
				<td width="100%" class="ListText"> 
					<%repView = (HTMLView) widReport.getView(OutputFormatType.HTML); 
						if(repView!=null)
						{
							String strHeight = "height:50%";
							String strWidth = "width:100%";
							String strDivStart = "<DIV id = WebiContent style=\"postion:relative";
							String strDivEnd = "\">";
							String strHead = repView.getStringPart("head",false);
							out.println(strHead);
							String strBody = repView.getStringPart("body",false);
							int iDiv1 = strBody.indexOf("<div");
							int iDiv2 = strBody.indexOf("<div",iDiv1+1);
							String strDiv = strBody.substring(iDiv1,iDiv2);
							StringTokenizer stk = new StringTokenizer(strDiv,";");
							while(stk.hasMoreTokens())
							{
								String strToken = stk.nextToken();
								if(strToken.startsWith("width"))
								{
									strWidth = strToken;
								}
								else if(strToken.startsWith("height"))
								{
									strHeight = strToken;
								}
							}

							while(strBody.indexOf("overflow:hidden;")>-1)
							{
								int istrBodylength = strBody.length();
								int istrBodybeginOverFlow = strBody.indexOf("overflow:hidden;");
								String strBodyPart1 = strBody.substring(0,istrBodybeginOverFlow-1);
								String strBodyPart2 = strBody.substring(istrBodybeginOverFlow+16 ,istrBodylength);
								strBody = strBodyPart1 + strBodyPart2;
							}

							while(strBody.indexOf("overflow: hidden;")>-1)
							{
								int istrBodylength = strBody.length();
								int istrBodybeginOverFlow = strBody.indexOf("overflow: hidden;");
								String strBodyPart1 = strBody.substring(0,istrBodybeginOverFlow-1);
								String strBodyPart2 = strBody.substring(istrBodybeginOverFlow+17 ,istrBodylength);
								strBody = strBodyPart1 + strBodyPart2;
							}
							
							
							while(strBody.indexOf("ro:style")>-1)
					        {
					              strBody = strBody.replaceAll("ro:style","style");
					        }
					         if(strBody.indexOf("<div class=\"s1 ovh\" style=\"position: absolute;left:76px;top:124px;")> -1){                 
					              strBody = strBody.replace("<div class=\"s1 ovh\" style=\"position: absolute;left:76px;top:124px;","<div class=\"s1 ovh\" style=\"position: absolute;left:76px;top:48px;");
					        }
					        
					        //06/26/2015 R 93 T36000040649     - Corrected alignment.
        					if(strBody.indexOf("<div class=\"s4 \" style=\"position: absolute;left:0px;top:0px;width:1280px;height:146pxpadding:0px 0px 17px 0px;\">")> -1){                 
		        					 strBody = strBody.replace("<div class=\"s4 \" style=\"position: absolute;left:0px;top:0px;width:1280px;height:146pxpadding:0px 0px 17px 0px;\">","<div class=\"s4 \" style=\"position: absolute;left:0px;top:45px;width:1280px;height:146pxpadding:0px 0px 17px 0px;\">");
							}
							if(strBody.indexOf("<table class=\"s11 dt bc\" cellspacing=\"0\" style=\"position: absolute;left:80px;top:51px;\" cols=\"5\">")> -1){  
					        	 strBody = strBody.replace("<table class=\"s11 dt bc\" cellspacing=\"0\" style=\"position: absolute;left:80px;top:51px;\" cols=\"5\">","<table class=\"s11 dt bc\" cellspacing=\"0\" style=\"position: absolute;left:80px;top:37px;\" cols=\"5\">");
							}
					        
					        //02/05/2014 R 8.4 T36000013921 - Corrected image display.
					        if(strHead.indexOf("ap{position: absolute;}")>-1){
								strBody = strBody.replaceAll("class=\"ap\"","");
							}
							
							%><DIV ID="webIContent" style=" position:relative;<%=strWidth%>;<%=strHeight%>"><%out.println(strBody);%></DIV>
							<%out.flush();	
						}	
					%>
			</td>
			<td width="20" nowrap>&nbsp;</td>
	    </tr>
	   
	  </table>
<%	
}
else
{
	for(int pgcount = 1;pgcount<=totalNoPages;pgcount++)
	{
 		pgn.setTo(pgcount);
 		repView = (HTMLView) widReport.getView(OutputFormatType.HTML); 
 		String strHeight = "height:50%";
 		String strWidth = "width:100%";
 		String strDivStart = "<DIV id = WebiContent style=\"postion:relative";
 		String strDivEnd = "\">";
 		String strHead = repView.getStringPart("head",false);
 		out.println(strHead);
 		String strBody = repView.getStringPart("body",false);
 		int iDiv1 = strBody.indexOf("<div");
 		int iDiv2 = strBody.indexOf("<div",iDiv1+1);
 		String strDiv = strBody.substring(iDiv1,iDiv2);
 		StringTokenizer stk = new StringTokenizer(strDiv,";");
 	    
		while(stk.hasMoreTokens())
 		{
 			String strToken = stk.nextToken();
 			if(strToken.startsWith("width"))
 			{
 				strWidth = strToken;
 			}
 			else if(strToken.startsWith("height"))
 			{
 				strHeight = strToken;
 			}
 		}

		while(strBody.indexOf("overflow:hidden;")>-1)
		{
			int istrBodylength = strBody.length();
			int istrBodybeginOverFlow = strBody.indexOf("overflow:hidden;");
			String strBodyPart1 = strBody.substring(0,istrBodybeginOverFlow-1);
			String strBodyPart2 = strBody.substring(istrBodybeginOverFlow+16 ,istrBodylength);
			strBody = strBodyPart1 + strBodyPart2;
 		}
 		
		while(strBody.indexOf("overflow: hidden;")>-1)
		{
			int istrBodylength = strBody.length();
			int istrBodybeginOverFlow = strBody.indexOf("overflow: hidden;");
			String strBodyPart1 = strBody.substring(0,istrBodybeginOverFlow-1);
			String strBodyPart2 = strBody.substring(istrBodybeginOverFlow+17 ,istrBodylength);
			strBody = strBodyPart1 + strBodyPart2;
 		}
 	

		while(strBody.indexOf("ro:style")>-1)
        {
              strBody = strBody.replaceAll("ro:style","style");
        }
        if(strBody.indexOf("<div class=\"s1 ovh\" style=\"position: absolute;left:76px;top:124px;")> -1){                 
              strBody = strBody.replace("<div class=\"s1 ovh\" style=\"position: absolute;left:76px;top:124px;","<div class=\"s1 ovh\" style=\"position: absolute;left:76px;top:48px;");
        }
        
        //06/26/2015 R 93 T36000040649     - Corrected alignment.
        if(strBody.indexOf("<div class=\"s4 \" style=\"position: absolute;left:0px;top:0px;width:1280px;height:146pxpadding:0px 0px 17px 0px;\">")> -1){                 
		         strBody = strBody.replace("<div class=\"s4 \" style=\"position: absolute;left:0px;top:20px;width:1280px;height:146pxpadding:0px 0px 17px 0px;\">","<div class=\"s4 \" style=\"position: absolute;left:0px;top:45px;width:1280px;height:146pxpadding:0px 0px 17px 0px;\">");
		}
		if(strBody.indexOf("<table class=\"s11 dt bc\" cellspacing=\"0\" style=\"position: absolute;left:80px;top:51px;\" cols=\"5\">")> -1){  
					        	 strBody = strBody.replace("<table class=\"s11 dt bc\" cellspacing=\"0\" style=\"position: absolute;left:80px;top:51px;\" cols=\"5\">","<table class=\"s11 dt bc\" cellspacing=\"0\" style=\"position: absolute;left:80px;top:37px;\" cols=\"5\">");
							}
        
        //02/05/2014 R 8.4 T36000013921 - Corrected image display.
		if(strHead.indexOf("ap{position: absolute;}")>-1){
			strBody = strBody.replaceAll("class=\"ap\"","");
		}
        
		%><DIV ID="webIContent" style=" position:relative;<%=strWidth%>;<%=strHeight%>"><%out.println(strBody);%></DIV>
		<%out.flush();
 	}
}	
%>

</div>

 <script  type="text/javascript">
 var oida = '<%=StringFunction.escapeQuotesforJS(request.getParameter("oid"))%>'
	function filterPages() {
  		console.log("Inside filterPages()");
	    require(["dojo/dom"],
	      function(dom){
			var sToken = document.getElementById("sToken").value;
	    	var pagingSelect = dijit.byId("PaginationDropdown").attr('value');
	    	location.href="/portal/reports/StandardReportsPage.jsp?goToPage="+pagingSelect+"&pageCall=Y&sToken="+sToken+"&oid="+oida+"&expandedPage="+expandedPage;     
		});
	}  
  
  </script>
