<div>

  <%--cquinton 2/18/2013 fix issues with ie7 formatting - also remove table tags--%>
  <div class="instruction">
    <% //jgadela 12-19-2013 Rel8.4 CR-854 [BEGIN]- Added parameter for ReportingLanguage %>
    <%= resMgr.getText("Reports.Prompt",TradePortalConstants.TEXT_BUNDLE, reportingPageLabelLan) %>
  </div>
  <br/>
  
<%
  isRequiredMsg= resMgr.getText("ATT02","ErrorMessages");
  int pCount = 0;
  pCount = webiPromptList.getCount();
%>
  <input type=hidden name="noOfControls" id="noOfControls" value="<%=pCount%>">
  
  <%--cquinton 2/18/2013 use a table to ensure all field labels line up vertically--%>
  <table>
<%
  for (int i=0;i<pCount;i++ ) {
    // for each prompt
    //Retrieve the prompt details
    webiPrompt = webiPromptList.getItem(i);
    String promptName = webiPrompt.getName(); 
%>
    <tr>
      <td valign="top">
        <input type=hidden name="<%="Prompt"+i %>" id="<%="Prompt"+i %>" value="<%=promptName%>">
    
        <div class="requiredAstrickWithOutFormItem">
          <%=widgetFactory.createInlineLabel( "", promptName )%>
	</div>
      </td>
      <td>
<%
  //jgadela 01/16/2014 Rel 8.4 IR T36000023438 [START]- Modified logic to take date format as per selected user date format
  String datePattern = userSession.getDatePattern();
  String dateWidgetOptions = "placeHolder:'" + datePattern + "'"; 

if (webiPrompt.getType() == PromptType.Mono){
      String[] pmtValue = webiPrompt.getCurrentValues();
      //IR T36000017537 - start check for object type
      if (webiPrompt.getObjectType() == ObjectType.DATE) {
      	//previousValue = "dd/mm/yyyy";
      	//out.println(widgetFactory.createTextField( "ListOfValues" + i, "",previousValue, "25", false, false, false, "", "", "none" ));
      	out.println(widgetFactory.createSearchDateField("ListOfValues" + i,"","class='char10'", dateWidgetOptions));
      }
      else{
		  String[] promptValue = webiPrompt.getPreviousValues();
		  if ( promptValue.length > 0) {
			previousValue = promptValue[0];
		  } else {
			//MEer  05/09/2014 IR-15640 Prompt fields defaults to null value instead of blank
			//	previousValue = "";
		  	previousValue = "null";	
		  }
		  out.println(widgetFactory.createTextField( "ListOfValues" + i, "", previousValue, "25", false, false, false, "", "", "none" ));
	} 	
	//jgadela 01/16/2014 Rel 8.4 IR T36000023438 [END]  
%>
        <input type=hidden value="" id=refr name=refr>
<%
    }
    else {
      listOfValues = (String [])webiPrompt.getCurrentValues();
      Lov widLov = webiPrompt.getLOV();
      int iLov = widLov.getMaxColumns();
      Values widValues = widLov.getAllValues();
      int iCount = widValues.getCount();
%>
		                    
      <select id="<%="ListOfValues" + i%>" name="<%="ListOfValues"+i %>" 

<%
      if (webiPrompt.getType() == PromptType.Multi) {

%>
                multiple size=4>
<%
      }
      else {
%>
                size=1>
<%
      }   //jgadela 12-19-2013 Rel8.4 CR-854 [BEGIN]- Modified to get the selectOption label using locale.
		  String selectOption = resMgr.getText("Report.Prompt.Select",TradePortalConstants.TEXT_BUNDLE, reportingPageLabelLan); %>
          <option value="" selected><%=selectOption%></option>
<%
      for( int j = 0; j < iCount ; j++)	{
        String sSelected = ""; 
%>
          <option value="<%=widValues.getValue(j)%>" <%=sSelected%>><%=StringFunction.xssCharsToHtml(widValues.getValue(j))%></option>
<%
      }
%>		                                
        </select>
<%
    }
%>
      </td>
    </tr>
<%
  }
%>            
  </table>
  <br/>
  
  <div>
    <button data-dojo-type="dijit.form.Button"  name="SpecifyReportButton" id="SpecifyReportButton" type="button" >
      <%=resMgr.getText("SpecifyReport.ShowReportText",TradePortalConstants.TEXT_BUNDLE, reportingPageLabelLan)%>
      <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">	
        viewReports('SpecifyReportButton');
      </script>	
    </button>
<%
  newLink = new StringBuffer();
  newLink.append(formMgr.getLinkAsUrl("goToReportsHome", response));
%>
  </div>
		
</div>