<%--
*******************************************************************************
  Reports Secondary Navigation

  Description:  Common place to render the secondary navigation for
                all pages associated with Reports.

  Variables:
    current2ndNav - Standard or Custom (TradePortalConstants)
        

*******************************************************************************
--%>
 <%--
 *	   Dev By Sandeep
 *     Copyright  � 2012                         
 *     CGI, Incorporated 
 *     All rights reserved
--%>

<div class="secondaryNav">
<%-- Komal ANZ Issue No.359 BEGINS --%>
  <div class="secondaryNavTitleCamel">
  <% //12-19-2013 Rel 8.4 CR-854 - changed the code to get labels using page locale%>
    <%=resMgr.getText("SecondaryNavigation.Reports",TradePortalConstants.TEXT_BUNDLE, reportingPageLabelLan).toUpperCase()%>
  </div>
<%-- Komal ANZ Issue No.359 ENDS --%>

  <%
     String standardLinkSelected = TradePortalConstants.INDICATOR_NO,
     		customLinkSelected = TradePortalConstants.INDICATOR_NO;
     		//helpLink = "";
     
     if (TradePortalConstants.STANDARD_REPORTS.equals(current2ndNav)){
       standardLinkSelected = TradePortalConstants.INDICATOR_YES;
       if(helpLink == null){                                    
      	 	helpLink = OnlineHelp.createContextSensitiveLink("customer/standard_reports.htm",resMgr,userSession);
       }
     }

     if (TradePortalConstants.CUSTOM_REPORTS.equals(current2ndNav)){
       customLinkSelected = TradePortalConstants.INDICATOR_YES;
       if(helpLink == null){
      	 helpLink = OnlineHelp.createContextSensitiveLink("customer/custom_reports.htm",resMgr,userSession);
      	 }
     }
     
     

     String linkParms = "current2ndNav=" + TradePortalConstants.STANDARD_REPORTS;
      //12-19-2013 Rel 8.4 CR-854 - changed the code to get labels using page locale
     String strdReportsNavText = resMgr.getText("SecondaryNavigation.Reports.StandardReports",TradePortalConstants.TEXT_BUNDLE, reportingPageLabelLan);
   %>
	   <jsp:include page="/common/NavigationLink.jsp">
	     <jsp:param name="linkTextKey"  value="<%=strdReportsNavText%>" />
	     <jsp:param name="linkSelected" value="<%= standardLinkSelected %>" />
	     <jsp:param name="action"       value="goToReportsHome" />
	     <jsp:param name="linkParms"    value="<%= linkParms %>" />
	   </jsp:include>
   <%
     linkParms = "current2ndNav=" + TradePortalConstants.CUSTOM_REPORTS;
     //Rel 8.4 CR-854 [BEGIN]- Reporting language option.
     String custReportsNavText = resMgr.getText("SecondaryNavigation.Reports.CustomReports",TradePortalConstants.TEXT_BUNDLE, reportingPageLabelLan);
   %>
	   <jsp:include page="/common/NavigationLink.jsp">
	     <jsp:param name="linkTextKey"  value="<%=custReportsNavText%>" />
	     <jsp:param name="linkSelected" value="<%= customLinkSelected %>" />
	     <jsp:param name="action"       value="goToReportsHome" />
	     <jsp:param name="linkParms"    value="<%= linkParms %>" />
	   </jsp:include>
<%=widgetFactory.createHoverHelp("secondaryNavHelp","HelpImageLinkHoverText", reportingPageLabelLan) %>

	<div class="secondaryNavHelp" id="secondaryNavHelp">
    	<%=helpLink%>
	</div>
  <div style="clear:both;"></div>
</div>
