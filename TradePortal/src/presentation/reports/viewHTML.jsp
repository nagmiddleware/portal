<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*, com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*, com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*, com.ams.tradeportal.html.*,
                 com.ams.tradeportal.busobj.webbean.*,com.businessobjects.rebean.wi.*,java.io.*" %>



<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" 
   scope="session">
</jsp:useBean>


<%
Debug.debug("***START********************VIEW*PDF*PAGE**************************START***");

DocumentInstance 	widDoc = null;
Reports 		widReports = null;
Report 			widReport = null;
HTMLView 		docHTMLView = null;

%>
<%@ include file="fragments/wistartpage.frag" %>
<%

String storageToken = request.getParameter("storageToken");
response.reset();
//NARAYAN-IR-PSUL011859834  Begin
//commented the below code because while downloading excel file this is preventing to chahce on local system and system doesn't open the PDF file
//response.setHeader("Cache-Control","no-cache"); //HTTP 1.1
//response.setHeader("Pragma","no-cache"); //HTTP 1.0   
//NARAYAN-IR-PSUL011859834  End 
response.setDateHeader("expires", 0);
request.setCharacterEncoding("UTF-8");

try
{
	
	
	
	//Request coming for the first time
	widDoc = reportEngine.getDocumentFromStorageToken(storageToken);
	widReports = widDoc.getReports();
	widReport = widReports.getItem(0);
	
	//Set the page navigation mode to Listing  
	widReport.setPaginationMode(PaginationMode.Listing);
	docHTMLView = (HTMLView) widReport.getView(OutputFormatType.HTML);
	if(docHTMLView!=null)
	{
	/* int iLength = docHTMLView.getContentLength();	
	response.setContentLength(iLength); */
	/* response.setHeader("Content-Type", "application/html");
	PrintWriter pwOut = new PrintWriter();

	//outputStream.write(abyBinaryContent);
	 docHTMLView.getContent(pwOut, "", "");
	 ServletOutputStream outputStream = response.getOutputStream();
	 outputStream.write(pwOut); */
	 
	 
	 PrintWriter writer = response.getWriter();
	 docHTMLView.getContent(writer, "", "");
	 writer.close();
	}
	 
}catch (REException rex)
{
	//Catch all exception
    System.out.println("EXCEPTION in VIEW PDF Page ######################"+rex.getErrorCode()+" "+rex.getMessage());
}

//Garbage Collection
widDoc = null;
widReports = null;
widReport = null;
docHTMLView = null;
  	  	  	  
%>

<%
Debug.debug("***END********************VIEW*PDF*PAGE**************************END***");
%>