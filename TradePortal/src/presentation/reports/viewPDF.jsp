<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*, com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*, com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*, com.ams.tradeportal.html.*,
                 com.ams.tradeportal.busobj.webbean.*,com.businessobjects.rebean.wi.*,java.io.*,java.util.*" %>



<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" 
   scope="session">
</jsp:useBean>


<%
Debug.debug("***START********************VIEW*PDF*PAGE**************************START***");
String                  reportName          = null;
StringBuffer            defaultFileName     = null;
DocumentInstance 	widDoc = null;
Reports 		widReports = null;
Report 			widReportPDF = null;
BinaryView 		docBinaryView = null;
//DK IR T36000017385 Rel8.2 06/25/2013 starts	
Prompts         webiPromptList = null;
int             iPromptCount  = 0;
Prompt webiPrompt = null;
PromptType        widPromptType           = null;
String                        locale                              = null;// T36000019689 Rel 8.2 08/07/2013 - Added
String                        formString                    = null;
String []               LOV                           = null;

//DK IR T36000017385 Rel8.2 06/25/2013 ends

%>
<%@ include file="fragments/wistartpage.frag" %>
<%
PropertyResourceBundle portalProperties = (PropertyResourceBundle) PropertyResourceBundle.getBundle("TradePortal");
String storageToken = request.getParameter("storageToken");
response.reset();
response.setHeader("Cache-Control","no-store"); 
response.setHeader("Pragma","no-cache");
response.setDateHeader("expires", 0);
request.setCharacterEncoding("UTF-8");
ServletOutputStream outputStream = null;
try
{
	//Locale to set the date prompt
	locale = portalProperties.getString("locale");// T36000019689 Rel 8.2 08/07/2013 - Added
	//Request coming for the first time
	widDoc = reportEngine.getDocumentFromStorageToken(storageToken);
	widDoc.refresh();
	
	// DK IR T36000017385 Rel8.2 06/25/2013 starts	
	webiPromptList = widDoc.getPrompts();
	iPromptCount = webiPromptList.getCount();
	for (int i=0;i<iPromptCount;i++)
	{
		webiPrompt = webiPromptList.getItem(i);
		formString = "ListOfValues"+i;
		widPromptType = webiPrompt.getType();
		if (webiPrompt.getType() == PromptType.Mono)
		{	
			String repPrompt = 	(String)session.getAttribute("formString"+i);
			//RKAZI T36000019902 08/16/2013 Rel 8.2 Start
			if (StringFunction.isBlank(repPrompt)){
				continue;
			}
			//RKAZI T36000019902 08/16/2013 Rel 8.2 End
			StringTokenizer strTokenizer = new StringTokenizer(repPrompt, ";");
			int iValueCount = strTokenizer.countTokens();
			String[] astrValues = new String[iValueCount];
			PromptType pt = webiPrompt.getType();
			
			int iValueIndex = 0;
			for (iValueIndex = 0; iValueIndex < iValueCount; iValueIndex++)
			{
				astrValues[iValueIndex] = strTokenizer.nextToken();
				// T36000019689 Rel 8.2 08/07/2013 - Start
				//Date parsing logic				
				if (webiPrompt.getObjectType() == ObjectType.DATE)
				{
					//jgadela 01/16/2014 Rel 8.4 IR T36000023438 -[START] Modified to take date format as per selected user date format
					String  retDt = astrValues[iValueIndex];
					int intFirstVal = retDt.indexOf("-");
					String dtFirsrVal = retDt.substring(0,intFirstVal);
					int intSecVal = retDt.lastIndexOf("-");
					String dtSecVal = retDt.substring(intFirstVal+1,intSecVal);
					int dtlen = retDt.length();
					String dtThirdVal = retDt.substring(intSecVal+1,dtlen);
					
					if(locale.equals("en_GB"))
					{ 
						if((intSecVal == 7 ) && (intFirstVal == 4) )
						{
							astrValues[iValueIndex] =  dtSecVal + "/"+  dtThirdVal + "/" + dtFirsrVal;
						}	
					}else{
						if((intSecVal == 7 ) && (intFirstVal == 4) )
						{
							astrValues[iValueIndex] =   dtThirdVal + "/"+   dtSecVal+ "/" + dtFirsrVal;
						}	
					}
					//jgadela 01/16/2014 Rel 8.4 IR T36000023438 -[END]
				}
				// T36000019689 Rel 8.2 08/07/2013 - End
				webiPrompt.enterValues(astrValues);
			}
		}
		//RKAZI T36000019902 08/16/2013 Rel 8.2 Start
		else
		{
			LOV = request.getParameterValues(formString);
			if (LOV == null){
				continue;
			}
			webiPrompt.enterValues(LOV);
		}
		//RKAZI T36000019902 08/16/2013 Rel 8.2 End
	}	
	widDoc.setPrompts();
	widDoc.applyFormat();
// DK IR T36000017385 Rel8.2 06/25/2013 ends
	widReports = widDoc.getReports();
	widReportPDF = widReports.getItem(0);	
	//Set the page navigation mode to Listing  
	widReportPDF.setPaginationMode(PaginationMode.Listing);
	docBinaryView = (BinaryView) widReportPDF.getView(OutputFormatType.PDF);
	byte[] abyBinaryContent = docBinaryView.getContent();
	int iLength = docBinaryView.getContentLength();
	defaultFileName = new StringBuffer();

	//Retrieve the report name from the report
	reportName = widDoc.getProperties().getProperty(PropertiesType.NAME, "");

	defaultFileName.append((reportName+".pdf"));
	response.setContentLength(iLength);
	response.setHeader("Content-Type", "application/pdf");
	String disposition = " fileName="+defaultFileName;
	response.setHeader("Content-Disposition", disposition);
	outputStream = response.getOutputStream();
	outputStream.write(abyBinaryContent);
	
}catch (REException rex)
{
	//Catch all exception
    System.out.println("EXCEPTION in VIEW PDF Page ######################"+rex.getErrorCode()+" "+rex.getMessage());
}

//Garbage Collection
widDoc = null;
widReports = null;
widReportPDF = null;
docBinaryView = null;
outputStream = null;
%>

<%
Debug.debug("***END********************VIEW*PDF*PAGE**************************END***");
%>