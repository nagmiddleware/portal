<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*, com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*, com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*, com.ams.tradeportal.html.*, com.ams.tradeportal.common.cache.*,
                 com.ams.tradeportal.busobj.webbean.*, com.ams.tradeportal.busobj.util.*,com.businessobjects.rebean.wi.*,
                                                                 com.crystaldecisions.sdk.plugin.CeKind,com.crystaldecisions.sdk.occa.infostore.*,java.util.*" %>


<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session">
</jsp:useBean>


<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" 
   scope="session">
</jsp:useBean>



<%
Debug.debug("***START*********************EDIT*REPORT*STEP1***************************START***"); 

//commented by KajalBhatt
//WIDocument      webiDocument            = null;
DocumentInstance webiDocument           = null;
String          universeName            = null;
String          universeId              = null;
String          universeCUId            = null;
String          domainId                = null;
String          bRefresh                = "";
String          bRefrLOV                = "";
String          bViewSQL                = "";
String          bAccessWP               = "";
String          bCreateDoc              = "";
String          bDrill                                = "N";
StringBuffer    newLink                 = null;
String          resultsetPage           = null;
String          resultPage           = null;
String          storageToken            = null;
String          report                  = null;
String          reportMode              = null;
MediatorServices    mediatorServices        = null;
DocumentHandler     xmlDoc                  = null;
String              physicalPage            = null;
String           reportType             = null;

IInfoStore       infoStore              = null;
IInfoObjects     webiUniverses          = null;
IInfoObject      webiUniverse           = null;
PropertyResourceBundle portalProperties     = null;
String repPort              = null;
String repProtocol          = null;

WidgetFactory widgetFactory = new WidgetFactory (resMgr);
%>

<%@ include file="fragments/wistartpage.frag" %>

<%

storageToken = request.getParameter("storageToken"); 
report = request.getParameter("report"); 
reportMode = request.getParameter("reportMode"); 
// Rel 9.3 XSS CID 11301, 11305
Debug.debug("REPORT MODE IN EDIT REPORT STEP 1######"+ reportMode);

//If storageToken is null then display new create report applet
System.out.println("storageToken in editReport 1 = "+storageToken);

// Narayan PPX-122 Rel 7.1 Begin 
/*** Here getting URL port and protocol parameter 'repPort' and 'repProtocol' value from TradePortal.properties                      
 * if it is not defined, then it will use default value 'https' for protocol and port value from request.
 * repProtocol and repPort are optional property.
 */

 portalProperties = (PropertyResourceBundle) PropertyResourceBundle.getBundle("TradePortal");
 try{
      repPort = portalProperties.getString("repPort");      
     }
 catch(MissingResourceException e){
       repPort = String.valueOf(request.getServerPort()); 
           }
                     
  try{
       repProtocol = portalProperties.getString("repProtocol");   
       }
  catch(MissingResourceException e){
       repProtocol  = "https"; 
       }
//Narayan PPX-122 Rel 7.1 End

if (storageToken == null) 
{
                //commented by 
                // there is no such method in XIR1 to create a blank document, there is one method in ReportEngine called newDocument
                //but we have to pass universe id
    //webiDocument = webiSession.newDocument();
                
    Hashtable unvAttbr = SupervisorCommandUtility.getUnivParameters();
  //jgadela Rel 8.4 CR-854 [BEGIN]- Modified code for french reporting language.
    String reportingLanguage = userSession.getReportingLanguage();
    if(reportingLanguage != null && reportingLanguage.contains("fr")){
    	reportingLanguage = "_fr_CA";
    }else{
    	reportingLanguage = "";
    }
    universeName = (String)unvAttbr.get("universeName"+reportingLanguage);
    universeId = (String)unvAttbr.get("universeId"+reportingLanguage);
  //jgadela Rel 8.4 CR-854 [END]- Modified code for french reporting language.
                
     
      
      // Get the universe selected by the user
      String sQuery = "SELECT SI_CUID, SI_NAME FROM CI_APPOBJECTS WHERE SI_ID = " + universeId + " AND SI_KIND = '" + CeKind.UNIVERSE + "'" ;
      infoStore = (IInfoStore) boSession.getService("InfoStore");
      webiUniverses = (IInfoObjects) infoStore.query(sQuery);
      
      if(webiUniverses.getResultSize() > 0)
      {
                      try{
                                                       webiUniverse = (IInfoObject) webiUniverses.get(0);
                                                       universeCUId = webiUniverse.getCUID();
                      }
                      catch(IndexOutOfBoundsException ex)
                      {
                                                      System.out.println("Univers Not found");
                      }
      }
                
    if (report.equals(TradePortalConstants.NEW_STANDARD_REPORTS))
    {
                                 reportType = "Y";
         resultsetPage = request.getContextPath() + NavigationManager.getNavMan().getPhysicalPage("StandardNewReport", request);
    }
    
    if (report.equals(TradePortalConstants.NEW_CUSTOM_REPORTS))
    {
                                reportType = "N";
        resultsetPage = request.getContextPath() + NavigationManager.getNavMan().getPhysicalPage("CustomNewReport", request);
    }   

                
}
else 
{

    System.out.println("storageToken is NOT NULL ");        
    //Open existinf report in the web panel when user clicks on Previous Step
                //replaced by 
    webiDocument = reportEngine.getDocumentFromStorageToken(storageToken);
    
                //Method to retrieve universe parameters from text file
    report = reportMode;
    universeName = "";
    universeId   = "";
    //domainId     = ""; //commented by  domain id not require
    
    if (reportMode.equals(TradePortalConstants.NEW_STANDARD_REPORTS))
    {
                                reportType = "Y";
         resultsetPage = request.getContextPath() + NavigationManager.getNavMan().getPhysicalPage("StandardNewReport", request);
    }
     
    if (reportMode.equals(TradePortalConstants.NEW_CUSTOM_REPORTS))
    {
                                reportType = "N";
         resultsetPage = request.getContextPath() + NavigationManager.getNavMan().getPhysicalPage("CustomNewReport", request);
    } 
     
       
    if (reportMode.equals(TradePortalConstants.REPORT_EDIT_CUSTOM ))
    {
        //CUSTOM EDIT REPORT
         resultsetPage = request.getContextPath() + NavigationManager.getNavMan().getPhysicalPage("CustomEditReport", request);
        reportType = "N";
    }
        
    if (reportMode.equals(TradePortalConstants.REPORT_EDIT_STANDARD ))
    {
        //STANDARD EDIT REPORT   
                                reportType = "Y";
         resultsetPage = request.getContextPath() + NavigationManager.getNavMan().getPhysicalPage("StandardEditReport", request);
    }
    
}
String sUserAgent = request.getHeader("User-Agent");
int iPos = sUserAgent.indexOf("MSIE");
String sBrowser = "";

if (iPos > -1) {
                sBrowser = "IE";
}
else
{
    sBrowser = "NETSCAPE";
}
Debug.debug("Browser type ######"+ sBrowser);


//Include Header frag
String          current2ndNav=null;
String              reportTab= null;
String   securityRights     = null;
boolean isStdMaintainRights = false;
boolean isCusMaintainRights = false; 
boolean isStdViewRights     = false;
boolean isCusViewRights     = false;

isStdMaintainRights   = SecurityAccess.hasRights(securityRights, SecurityAccess.MAINTAIN_STANDARD_REPORT);
isStdViewRights       = SecurityAccess.hasRights(securityRights, SecurityAccess.VIEW_STANDARD_REPORT);
isCusMaintainRights   = SecurityAccess.hasRights(securityRights, SecurityAccess.MAINTAIN_CUSTOM_REPORT);
isCusViewRights       = SecurityAccess.hasRights(securityRights, SecurityAccess.VIEW_CUSTOM_REPORT);    

reportTab = request.getParameter("reportTab");

                if (reportTab == null)
{
                reportTab = (String) session.getAttribute("reportTab");

                //If reportTab is null set the default reports tab to Standatd Reports
                if (reportTab == null){        
                                if(isStdMaintainRights || isStdViewRights){
                                  reportTab = TradePortalConstants.STANDARD_REPORTS;
                                 }
                                else {
                                  reportTab = TradePortalConstants.CUSTOM_REPORTS;
                                }
                } 
}

current2ndNav = request.getParameter("current2ndNav");

if (current2ndNav == null){
                if(isStdMaintainRights || isStdViewRights){
    current2ndNav = TradePortalConstants.STANDARD_REPORTS;
                }else {
                                current2ndNav = TradePortalConstants.CUSTOM_REPORTS;
                }
}

Debug.debug("*** <<reportTab>>: "+reportTab); 
Debug.debug("*** <<current2ndNav>>: "+current2ndNav); 


//jgadela R 8.4 CR-854 T36000024797   -[START] Reporting page labels. 
String reportingPageLabelLan = null;
String cbReportingLangOptionInd = null;
Cache CBCache = (Cache)TPCacheManager.getInstance().getCache(TradePortalConstants.CLIENT_BANK_CACHE);
DocumentHandler CBCResult = (DocumentHandler)CBCache.get(userSession.getClientBankOid());
if (!StringFunction.isBlank(userSession.getClientBankOid())){
	   cbReportingLangOptionInd = CBCResult.getAttribute("/ResultSetRecord(0)/REPORTING_LANG_OPTION_IND"); // jgadela Rel 8.4 CR-854 
}

if(TradePortalConstants.INDICATOR_YES.equals(cbReportingLangOptionInd)){
	reportingPageLabelLan = userSession.getReportingLanguage();
}else{
	reportingPageLabelLan = userSession.getUserLocale();
}
//jgadela R 8.4 CR-854 T36000024797   -[END]



%>

<%--*******************************HTML for page begins here **************--%>


<%
   // Since there is an applet included with this page, the timeout forward cannot
   // be included.   If the timeout forward is included, the user can be forwarded to 
   // the timeout page even though there has been activity within the applet.
%>

<jsp:include page="/common/Header.jsp">
  <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
  <jsp:param name="includeErrorSectionFlag"  value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>

<div class="pageMain">               
  <div class="pageContent">         
                
    <div class="secondaryNav">
	
      <div class="secondaryNavTitle">
      <% //12-19-2013 Rel 8.4 CR-854 - changed the code to get labels using page locale%>
        <%=resMgr.getText("SecondaryNavigation.Reports",TradePortalConstants.TEXT_BUNDLE, reportingPageLabelLan).toUpperCase()%>
      </div>

<%
     String standardLinkSelected = TradePortalConstants.INDICATOR_NO,
                                customLinkSelected = TradePortalConstants.INDICATOR_NO,
                                helpLink = "";
     
     if (TradePortalConstants.STANDARD_REPORTS.equals(current2ndNav)){
       standardLinkSelected = TradePortalConstants.INDICATOR_YES;
      helpLink = OnlineHelp.createContextSensitiveLink("customer/report_step1.htm",resMgr,userSession);
       }

     if (TradePortalConstants.CUSTOM_REPORTS.equals(current2ndNav)){
       customLinkSelected = TradePortalConstants.INDICATOR_YES;
       helpLink = OnlineHelp.createContextSensitiveLink("customer/report_step1.htm",resMgr,userSession);
     }

     String linkParms = "current2ndNav=" + TradePortalConstants.STANDARD_REPORTS;
     //12-19-2013 Rel 8.4 CR-854 - changed the code to get labels using page locale
     String strdReportsNavText = resMgr.getText("SecondaryNavigation.Reports.StandardReports",TradePortalConstants.TEXT_BUNDLE, reportingPageLabelLan);
     String custReportsNavText = resMgr.getText("SecondaryNavigation.Reports.CustomReports",TradePortalConstants.TEXT_BUNDLE, reportingPageLabelLan);
%>
      <jsp:include page="/common/NavigationLink.jsp">
        <jsp:param name="linkTextKey"  value="<%= strdReportsNavText%>" />
        <jsp:param name="linkSelected" value="<%= standardLinkSelected %>" />
        <jsp:param name="action"       value="goToReportsHome" />
        <jsp:param name="linkParms"    value="<%= linkParms %>" />
      </jsp:include>
<%
     linkParms = "current2ndNav=" + TradePortalConstants.CUSTOM_REPORTS;
%>
      <jsp:include page="/common/NavigationLink.jsp">
        <jsp:param name="linkTextKey"  value="<%=custReportsNavText %>" />
        <jsp:param name="linkSelected" value="<%= customLinkSelected %>" />
        <jsp:param name="action"       value="goToReportsHome" />
        <jsp:param name="linkParms"    value="<%= linkParms %>" />
      </jsp:include>

      <div class="secondaryNavHelp"  id="secondaryNavHelp">
        <%=helpLink%>
      </div>
      <% //12-19-2013 Rel 8.4 CR-854 - getting ables%>
      <%= widgetFactory.createHoverHelp("secondaryNavHelp","HelpImageLinkHoverText", reportingPageLabelLan)%>
                
      <div style="clear:both;"></div>
    </div>

    <div class="subHeaderDivider"></div>

    <%--cquinton 2/14/2013 add expandNode - everthing within this expands to the page size--%>
    <div id="expandNode">

    <div class="pageSubHeader">
                                                       
      <span class="pageSubHeaderItem">
<%
      if (report.equals(TradePortalConstants.NEW_STANDARD_REPORTS) ||
          report.equals(TradePortalConstants.NEW_CUSTOM_REPORTS))
      {
%>
		<% //12-19-2013 Rel 8.4 CR-854 - getting ables%>
        <%=resMgr.getText("Reports.NewReport", TradePortalConstants.TEXT_BUNDLE, reportingPageLabelLan) %>
<%
      }
      
      if (report.equals(TradePortalConstants.REPORT_EDIT_CUSTOM ) ||
          report.equals(TradePortalConstants.REPORT_EDIT_STANDARD ))
      {
		%>
		<% //12-19-2013 Rel 8.4 CR-854 - getting ables%>
        <%=resMgr.getText("Reports.EditStep1Text", TradePortalConstants.TEXT_BUNDLE, reportingPageLabelLan) %>
        <span>
        <% //12-19-2013 Rel 8.4 CR-854 - getting ables%>
          <%=resMgr.getText("Reports.Step1Heading", TradePortalConstants.TEXT_BUNDLE, reportingPageLabelLan) %>
        </span>
<%
      }
%>
      </span>
      
      <span class="pageSubHeader-right">

          <button data-dojo-type="dijit.form.Button" name="CollapseButton" id="CollapseButton" type="button" 
                  class="pageSubHeaderButton" style="display:none;"> <%--starts out hidden--%>
                  <% //12-19-2013 Rel 8.4 CR-854 - getting ables%>
            <%=resMgr.getText("Reports.Collapse",TradePortalConstants.TEXT_BUNDLE, reportingPageLabelLan)%>
            <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
              local.collapsePage();
            </script>
          </button>
          <% //12-19-2013 Rel 8.4 CR-854 - getting ables%>
          <%= widgetFactory.createHoverHelp("CollapseButton","Reports.Collapse", reportingPageLabelLan)%>

          <button data-dojo-type="dijit.form.Button" name="ExpandButton" id="ExpandButton" type="button" 
                  class="pageSubHeaderButton" >
                  <% //12-19-2013 Rel 8.4 CR-854 - getting ables%>
            <%=resMgr.getText("Reports.Expand",TradePortalConstants.TEXT_BUNDLE, reportingPageLabelLan)%>
            <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
              local.expandPage();
            </script>
          </button>
          <%= widgetFactory.createHoverHelp("ExpandButton","Reports.Expand")%>

          <button data-dojo-type="dijit.form.Button" name="CloseButton" id="CloseButton" type="button" 
                  class="pageSubHeaderButton" >
                 <% //12-19-2013 Rel 8.4 CR-854 - getting ables%>
            <%=resMgr.getText("common.CloseText",TradePortalConstants.TEXT_BUNDLE, reportingPageLabelLan)%>
            <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
              openURL("<%=formMgr.getLinkAsUrl("goToReportsHome", "&current2ndNav="+current2ndNav, response)%>");
            </script>
          </button>
          <% //12-19-2013 Rel 8.4 CR-854 - getting ables%>
          <%= widgetFactory.createHoverHelp("CloseButton","CloseHoverText", reportingPageLabelLan)%>
                                    
      </span>
      <div style="clear:both;"></div>
                                                
    </div>   
                                                
    <form method="post" action="../FullHTML/Replace"100%" border="0" cellspacing="0" cellpadding="0" height="34" data-dojo-type="dijit.form.Form" >
      <div class="formContentNoSidebar">
		<% //12-19-2013 Rel 8.4 CR-854 - getting ables%>
        <div class="formItem"><%=resMgr.getText("Reports.Step1", TradePortalConstants.TEXT_BUNDLE, reportingPageLabelLan) %></div>

        <%--cquinton 2/13/2013 note: javascript here is not ideal, but to limit risk of modifying the 
            applet am leaving this here.  when applet approach is rewritten, please consider moving javascript to
            bottom of page--%>
        <script language="javascript">
          var _info = navigator.userAgent;
          <%-- var _ns = false; --%>
          var _ie = (_info.indexOf("MSIE") > 0 && _info.indexOf("Win") > 0 && _info.indexOf("Windows 3.1") < 0);
          <%-- var _ns = (navigator.appName.indexOf("Netscape") >= 0 && ((_info.indexOf("Win") > 0 && _info.indexOf("Win16") < 0 && java.lang.System.getProperty("os.version").indexOf("3.5") < 0) || (_info.indexOf("Sun") > 0) || (_info.indexOf("Linux") > 0))); --%>
          var _dom = (document.getElementById != null) ? true : false;
          var _appVer = navigator.appVersion.toLowerCase();
          var _mac = (_appVer.indexOf('macintosh') >= 0) || (_appVer.indexOf('macos') >= 0);
          var _moz = _dom && !_ie;
          var _saf = _moz && _mac;
          var JRE_URL = "JavaPlugin/Win32/jre-6u3-windows-i586-p.exe";
          <%-- if(_ns) --%>
          <%--                 JRE_URL = "http://www.oracle.com/technetwork/java/javasebusiness/downloads/java-archive-downloads-javase6-419409.html"; --%>
          if(_saf)
                          JRE_URL='http://www.apple.com/downloads/macosx/apple/java.html';
          if(_info.indexOf("Linux") > 0)
                          JRE_URL='http://java.sun.com/products/plugin/index.html#download';

          <%--  Call the function saveDocument in the Applet --%>
          function saveDocumentCall( title, desc, keyword, folderID, corpCat, persCat, refreshOnOpen )
          {
	  		  <%-- T36000020322 RKAZI REL 8.2 08/28/2013 - ADD START --%>
        	  <%-- need to add code specific for chrome which does not return applets array. --%>
        	  if (navigator.appName=="Microsoft Internet Explorer")
              {
                  document.applets[0].saveDocument( title, desc, keyword, folderID, corpCat, persCat, refreshOnOpen,_saf );
              }else{
            	  document.getElementById("Java_Report_PanelID").saveDocument( title, desc, keyword, folderID, corpCat, persCat, refreshOnOpen,_saf );
              }
              <%-- T36000020322 RKAZI REL 8.2 08/28/2013 - ADD END        --%>
          }
        </script>
                                
<%
  java.util.Date date = new java.util.Date();

  String strSourceID = "RepoId="+domainId+";UnivId="+universeId+";UnivName="+universeName;
  String sToken = null;
  String repotype = null;
  String oId = null;
  String repName = null;
  String docType = null;
  String repDesc = null;
  int ioId = 0;

  if (boSession != null)
  {
    if (storageToken != null)
    {
      sToken = storageToken;
      //added by  to get the dcument type in bo xir2
      docType = webiDocument.getProperties().getProperty(PropertiesType.DOCUMENT_TYPE);
      oId = webiDocument.getProperties().getProperty(PropertiesType.DOCUMENT_ID);
      repName = webiDocument.getProperties().getProperty(PropertiesType.NAME);
      repDesc = webiDocument.getProperties().getProperty(PropertiesType.DESCRIPTION);
    }

    //commented by 
    //String sVersionApplet = webiSession.getServerDictionary("webi").getProperty("APPLET_VERSION");                //get something like 6.x.y.z
    //String sVersionAppletIELib = sVersionApplet.replace('.', ',');     //used for cab versionning (=> "6,x,y,z" instead of "6.x.y.z")
    //String sVersionAppletSUNLib = sVersionApplet.replace(',', '.');               //used for jar versionning (=> "6.x.y.z" instead of "6,x,y,z")

    //added by  for to run the java applet in bo xir2
    String instanceID = reportEngine.createServerInstance();
    String strWISession = instanceID.substring(0,instanceID.indexOf(","));
//     final String BO_CMS_NAME  = portalProperties.getString("CMSName");
//     final String BO_AUTH_TYPE = portalProperties.getString("Authentication");
//     final String BO_USERNAME  = request.getParameter("bo_username");
//     final String BO_PASSWORD  = request.getParameter("bo_password");
//     String infoviewURL = "http://xlpropx03:8080/InfoViewApp/logon/start.do?ivsLogonSession="+ java.net.URLEncoder.encode(boSession.getSerializedSession());
		//System.out.println("bo Properties ===> " + boSession.getService("WebiReportEngine"));
	   //String infoviewURL ="http://162.70.10.24:8080/OpenDocument/opendoc/openDocument.jsp?sType=wid&sIDType="+docType+"&iDocID="+oId+"&sViewer=html&sOutputFormat=I&serSes=" + java.net.URLEncoder.encode(boSession.getSerializedSession());
// String token = boSession.getLogonTokenMgr().createLogonToken("",120,100);
// String tokenEncode = URLEncoder.encode(token, "UTF-8")
// 	   String infoviewURL ="http://xlpropx03:8080/AnalyticalReporting/viewers/cdz_adv/viewDocument.jsp?id=225149&actId=99679&pvl=en_US&loc=en&appKind=InfoView&cafWebSesInit=true&bypassLatestInstance=true&ctx=standalone&service=%2FInfoViewApp%2Fcommon%2FappService.do&objIds=225149&containerId=2993&pref=maxOpageU%3D15%3BmaxOpageUt%3D200%3BmaxOpageC%3D10%3Btz%3DAmerica%2FNew_York%3BmUnit%3Dinch%3BshowFilters%3Dtrue%3BsmtpFrom%3Dtrue%3BpromptForUnsavedData%3Dtrue%3B&kind=Webi&iventrystore=widtoken&ViewType=H&entSession=CE_ENTERPRISESESSION&lang=en&ivsLogonSession="+ java.net.URLEncoder.encode(boSession.getSerializedSession());
%>
<!-- <div> -->
<%-- 	<iframe align="middle" name="reportFrame" src="<%=infoviewURL%>" seamless="seamless" height="600" width =1024" target="reportFrame"> --%>
<!-- 	<base target="_self"></base> -->
<!-- 	</iframe> -->
<!-- </div> -->

        <script language="javascript">
        var text1 = "<%=resMgr.getText("Reports.Text1",TradePortalConstants.TEXT_BUNDLE, reportingPageLabelLan)%>";
        var text2 = "<%=resMgr.getText("Reports.Text2",TradePortalConstants.TEXT_BUNDLE, reportingPageLabelLan)%>";
        var text3 = "<%=resMgr.getText("Reports.Text3",TradePortalConstants.TEXT_BUNDLE, reportingPageLabelLan)%>";
     
        

          <%--cquinton 2/13/2013 note: this code to detect IE vs. Netscape is no longer valid
              but as it continues to work, not addressing at this time...--%>
          if (navigator.appName=="Microsoft Internet Explorer")
          {  <%-- jgadela 04/06/2014 R8.4 IR 26404 - modified the class id --%>
            var appletParams = '<div><OBJECT id= "Java_Report_PanelID" name="Java_Report_Panel" classid="clsid:CAFEEFAC-0017-0000-FFFF-ABCDEFFEDCBA" WIDTH="100%" HEIGHT="680px" codebase="bodistribution/jre-7u80-windows-i586.exe#Version=1,7,0,80">' +
              '  <param name="java_type" value="application/x-java-applet;version=1.7">' +
              '  <param name="java_code" value="com.businessobjects.wp.tc.TCMain">' + 
              '  <param name="java_codebase" VALUE="<%= request.getContextPath() %>/webiApplet/">' +
              '  <param name="NAME" value="Java_Report_Panel">' +
              '  <param name="scriptable" value="true">' +
              '  <param name="cache_archive" value="ThinCadenza.jar,BOExtPoints.jar"></param>' +
              '  <param name="cache_version" value="12.6.4.2204"></param>' +   
              '  <param name="Server" value="<%= request.getServerName() %>"></param>' +
              '  <param name="Isapi" value="<%= request.getContextPath() %>/servlet/CadenzaServlet"></param>' +
              '  <param name="Protocol" value="<%=repProtocol%>"></param>' +
              '  <param name="Port" value="<%=repPort%>"></param>' +
              '  <param name="WebiSession" value="<%= strWISession %>"></param>' + 
              '  <param name="CdzSession" value="<%= reportEngine.createServerInstance() %>"></param>';
<%
    if(oId != null)
    {
%>
            appletParams = appletParams +'  <param name="DocumentID" value="<%= oId %>"></param>' ;
<%  
    }
%>
            appletParams = appletParams +'  <param name="DocumentType" value="wid"></param>' +
              '  <param name="Universe" value="<%=universeName%>"></param>' +
              '  <param name="UniverseID" value="UnivCUID=<%=universeCUId%>"></param>' +
              '  <param name="SaveAs" value="<%= request.getContextPath() %>/dialog/SaveReport.jsp?StandardReport=<%=reportType%>&storageToken=<%=StringFunction.escapeQuotesforJS(storageToken)%>&reportMode=<%=StringFunction.escapeQuotesforJS(reportMode)%>&unvId=<%=universeId%>&reportNamePassed=<%=repName%>&reportDescPassed=<%=repDesc%>"></param>' +
              '  <param name="bRobot" value="false"></param>' +
              '  <param name="bTraceInLogFile" value="false"></param>' +
              '  <param name="HelpRoot" value="<%= request.getContextPath().substring(1) %>"></param>' +
              '  <param name="Lang" value="en"></param>' +
              '  <param name="SplashScreen" value="true"></param>' +
              '  <param name="SplashScreenVersionString" value="12.6.4.2204"></param>' +
              '  <param name="progressbar" value="true"></param>' +
              '  <param name="boxmessage" value="Please wait, downloading Trade Portal Java Report Panel..."></param>' +
              '  <param name="boxbgcolor" value="#FFFFFF"></param>' +
              '  <param name="progresscolor" value="#00008F"></param>' +
               text1 + '<a href="bodistribution/jre-7u80-windows-i586.exe">' +text2 + '</a> <span></span>' +text3 +
              ' </OBJECT></div>';
            document.writeln(appletParams);
          }
          else
          {
            <%--cquinton 2/13/2013 ir#11446 explicitly set height here too
                comment says its for netscape - netscape is no longer supported, but we do use in all browsers other than IE --%>
            <%-- Netscape --%>
            <%-- Rel 9.2 XSS - CID - 11349, sToken has been escaped--%>
            var appletParams = '<EMBED id="Java_Report_PanelID" type="application/x-java-applet;version=1.7" WIDTH="100%" HEIGHT="680px" pluginspage="bodistribution/jre-7u80-windows-i586.exe#Version=1,7,0,80" ' + 
              'CODE="com.businessobjects.wp.tc.TCMain" '+
              'java_codebase="<%= request.getContextPath() %>/webiApplet/"'+
              'type="application/x-java-applet;version=1.7"' +
              'name="Java_Report_Panel" ' +
              'scriptable="true" ' +
              'cache_archive="ThinCadenza.jar,BOExtPoints.jar" ' +
              'cache_version="12.6.4.2204" ' + 
              'archive="ThinCadenza.jar,BOExtPoints.jar" ' +
              'Server="<%= request.getServerName() %>" ' +
              'Isapi="<%= request.getContextPath() %>/servlet/CadenzaServlet" ' +
              'Protocol="<%= repProtocol %>" ' +
              'Port="<%= repPort %>" ' +
              'WebiSession="<%= strWISession %>" ' +
              'CdzSession="<%= reportEngine.createServerInstance() %>" ' +
              'Token="<%= StringFunction.escapeQuotesforJS(sToken) %>" ' ;
<%
    if(oId != null)
    {
%>
            appletParams = appletParams + 'DocumentID="<%= oId %>" ';
<%
    }
%>
            appletParams = appletParams + 'DocumentType="wid" '+
              'Universe="<%=universeName%>" ' +                   
              'UniverseID="UnivCUID=<%=universeCUId%>" ' +
              'bRobot="false" ' +
              'bTraceInLogFile="false" ' +
              'HelpRoot="<%= request.getContextPath().substring(1) %>" ' +
              'Lang="en" ' +
              'SaveAs="<%= request.getContextPath() %>/dialog/SaveReport.jsp?StandardReport=<%=reportType%>&storageToken=<%=StringFunction.escapeQuotesforJS(storageToken)%>&reportMode=<%=StringFunction.escapeQuotesforJS(reportMode)%>&unvId=<%=universeId%>" ' +
              'SplashScreen="true" ' +
              'SplashScreenVersionString="" ' +
              'progressbar="true" ' +
              'boxmessage="Please wait, downloading WebIntelligence Java Report Panel..." ' +
              'boxbgcolor="#FFFFFF" ' +
              'progresscolor="#00008F" ' +
              '<NOEMBED>' +
               text1 + '<a href="bodistribution/jre-7u80-windows-i586.exe">' +text2 + '</a> <span></span>' +text3 +
              '</NOEMBED>' +
              '</EMBED>';
            document.writeln(appletParams);
          }
          function openURL(URL){
        	    if (isActive =='Y') {
        	    	if (URL != '' && URL.indexOf("javascript:") == -1) {
	        	    	var cTime = (new Date()).getTime();
	        	        URL = URL + "&cTime=" + cTime;
	        	        URL = URL + "&prevPage=" + context;
        	    	}
        	    }
            document.location.href  = URL;
            return false;
          }

        </script>

<% 
  }
  else
  {
    Debug.debug("The boSession is null in EditReport_Step1");
  }
%>
 
      </div>

    </form>

    </div> <%--end of expandNode--%>

  </div>
</div>
<jsp:include page="/common/Footer.jsp">
   <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>

<%--cquinton 2/14/2013 add expand/collapse functionality--%>
<script>
  var local = {};

  require(["dojo/dom", "dojo/dom-style", "dojo/dom-class", "dojo/query", "dijit/registry"],
      function( dom, domStyle, domClass, query, registry ) {

    local.expandPage = function() {
      var expandNode = dom.byId("expandNode");
      if ( expandNode ) {
        <%-- hide everything else --%>
        query(".pageHeader").forEach( function(entry, i) {
          domStyle.set(entry, "display", "none");
        });
        query(".pageHeaderNavigation").forEach( function(entry, i) {
          domStyle.set(entry, "display", "none");
        });
        query(".secondaryNav").forEach( function(entry, i) {
          domStyle.set(entry, "display", "none");
        });
        query(".subHeaderDivider").forEach( function(entry, i) {
          domStyle.set(entry, "display", "none");
        });
        query(".pageFooter").forEach( function(entry, i) {
          domStyle.set(entry, "display", "none");
        });
        domClass.add(expandNode, "expandedPage");

        var expandButton = registry.byId("ExpandButton");
        var collapseButton = registry.byId("CollapseButton");
        if (expandButton && collapseButton) {
          domStyle.set(expandButton.domNode, "display", "none");
          domStyle.set(collapseButton.domNode, "display", "inline-block");
        }
      }
    }

    local.collapsePage = function() {
      var expandNode = dom.byId("expandNode");
      if ( expandNode ) {
        <%-- and remove the expand class --%>
        domClass.remove(expandNode, "expandedPage");
        <%-- show everything that was hidden above --%>
        query(".pageHeader").forEach( function(entry, i) {
          domStyle.set(entry, "display", "block");
        });
        query(".pageHeaderNavigation").forEach( function(entry, i) {
          domStyle.set(entry, "display", "block");
        });
        query(".secondaryNav").forEach( function(entry, i) {
          domStyle.set(entry, "display", "block");
        });
        query(".subHeaderDivider").forEach( function(entry, i) {
          domStyle.set(entry, "display", "block");
        });
        query(".pageFooter").forEach( function(entry, i) {
          domStyle.set(entry, "display", "block");
        });

        var expandButton = registry.byId("ExpandButton");
        var collapseButton = registry.byId("CollapseButton");
        if (expandButton && collapseButton) {
          domStyle.set(collapseButton.domNode, "display", "none");
          domStyle.set(expandButton.domNode, "display", "inline-block");
        }
      }
    }

  });

</script>

</body>
</html>
<%
  Debug.debug("***END*********************EDIT*REPORT*STEP1***************************END***"); 
%>
