<%--
 *
 *     Copyright  � 2012                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*, com.amsinc.ecsg.util.*, com.ams.tradeportal.common.cache.*,
                 com.amsinc.ecsg.html.*, com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*, com.ams.tradeportal.html.*,
                 com.ams.tradeportal.busobj.webbean.*,com.businessobjects.rebean.wi.*,java.util.*,com.ams.tradeportal.busobj.util.SecurityAccess" %>



<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
</jsp:useBean>


<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" 
   scope="session">
</jsp:useBean>
<%@ include file="fragments/reportVariables.frag" %>
<%
Debug.debug("***START*********************CUSTOM*REPORTS*PROMPT*PAGE1***************************START***"); 
// first try to retrieve the session by using the WebIntelligence Cookie
// if not switch back to default page 
//T36000023438
WidgetFactory widgetFactory = new WidgetFactory(resMgr, userSession);
String viewURLLink = "";
StringBuffer      dropdownOptions              = new StringBuffer();
String options = ""; 
String				buttonPressed			= "";
String 				divID					= ""; 
String 				reportdivID				= "";
String              redirectURI             = null;
String              storageToken            = null;
String []           listOfValues            = null;
String              previousValue           = null;
boolean             promptDoc               = false;
MediatorServices    mediatorServices        = null;
DocumentHandler     xmlDoc                  = null;
String 		        physicalPage			= null;
String              cell                    = null;
String              sAction                 = null;
DocumentInstance widDoc = null;
Prompts  			webiPromptList 			= null;
Prompts widPromptsList = null;
Prompt  webiPrompt = null;
Reports widReports = null;
PaginationMode pgMode = null;
int iPrompts = 0;
int 				totalNoPages 			= 0;
PageNavigation 		pgn 					= null;
String 				pageToken 				= null;
String 				formString				= null;
String [] 			LOV 					= null;
boolean 			end;
int 				iPromptCount 			= 0;
HTMLView 			repView 				= null;
Report 				widReport 				= null;
String 				pageMode 				= null;
int 				prevPage 				= 0;
int 				nextPage 				= 0;
String 				pageCall 	 			= "";
String 				pageNum 				= null;
int 				goToPage 				= 0;
String 				locale					= null;
String 				isRequiredMsg 			= null;

//cquinton 2/14/2013 add a variable to see if on entry page is expanded or not

//Rel 9.3 MEer XSS CID-11389
String expandedPage=request.getParameter("expandedPage");
if ( expandedPage == null ) {
  expandedPage = "N";
}

%>
<%@ include file="fragments/wistartpage.frag" %>
<%
// VishalS Cross Site Scripting encoding for expandedPage
storageToken = StringFunction.xssCharsToHtml(request.getParameter("storageToken"));
try
{
     widDoc = reportEngine.getDocumentFromStorageToken(storageToken);
}catch (REException rex)
{
     //Catch all exception
     System.out.println("EXCEPTION in Custom Reports Prompt Page1 ######################"+rex.getErrorCode()+" "+rex.getMessage());
     xmlDoc = formMgr.getFromDocCache();
     mediatorServices = new MediatorServices();
     mediatorServices.getErrorManager().setLocaleName(resMgr.getResourceLocale());
     mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.WEBI_SESSION_ERROR);
     mediatorServices.addErrorInfo();
     xmlDoc.setComponent("/Error",         mediatorServices.getErrorDoc());
     xmlDoc.setAttribute("/In/HasErrors",  TradePortalConstants.INDICATOR_YES);
     formMgr.storeInDocCache("default.doc", xmlDoc);
     physicalPage = NavigationManager.getNavMan().getPhysicalPage("ReportsErrors", request);
                
     %>
     <jsp:forward page='<%= physicalPage %>' />
     <%
}

//the next command will create the prompt list
try
{  
   widPromptsList = widDoc.getPrompts();
   iPrompts = widPromptsList.getCount();

} catch (REException wie)
{
   throw wie;
}
repName = widDoc.getProperties().getProperty(PropertiesType.NAME);
//VishalS Cross Site Scripting encoding for buttonPressed, storageToken,pageToken
buttonPressed = StringFunction.xssCharsToHtml(request.getParameter("buttonPressed"));
storageToken = StringFunction.xssCharsToHtml(request.getParameter("storageToken"));
if(request.getParameter("pageToken")!=null)
{
	pageToken = StringFunction.xssCharsToHtml(request.getParameter("pageToken"));
	
}
PropertyResourceBundle portalProperties = (PropertyResourceBundle) PropertyResourceBundle.getBundle("TradePortal");
pageMode = portalProperties.getString("pageMode");
String securityRights = userSession.getSecurityRights();
if( pageMode == null)
{
    pageMode = "n";

}
//Locale to parse the report date paramater
locale = portalProperties.getString("locale");
if(pageMode.equalsIgnoreCase(TradePortalConstants.INDICATOR_YES))
{
	if (request.getParameter("pageCall")!=null)
	{
		pageCall = request.getParameter("pageCall");
        
	}
	else
	{
		pageCall ="N";
	}


	if (request.getParameter("goToPage")!=null) 
	{
   		pageNum = request.getParameter("goToPage");
	   	goToPage = (new Integer(pageNum).intValue());

	}
	else 
	{
   		goToPage = 1;
   
	}

}
else
{

  pageCall ="N";

}
try
{
	if (pageCall.equals("Y"))
	{
		//Request coming from the same page
		widDoc = reportEngine.getDocumentFromStorageToken(pageToken);
	}
	else
	{

		//Request coming for the first time
		widDoc = reportEngine.getDocumentFromStorageToken(storageToken);
		widDoc.refresh();
	}	
}catch (REException wie)
{
     //Catch all exception
     System.out.println("EXCEPTION in Custom Reports Prompt Page3 ######################"+wie.getErrorCode()+" "+wie.getMessage());
                        
     xmlDoc = formMgr.getFromDocCache();
     mediatorServices = new MediatorServices();
     mediatorServices.getErrorManager().setLocaleName(resMgr.getResourceLocale());
     mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.WEBI_SESSION_ERROR);
     mediatorServices.addErrorInfo();
     xmlDoc.setComponent("/Error",         mediatorServices.getErrorDoc());
     xmlDoc.setAttribute("/In/HasErrors",  TradePortalConstants.INDICATOR_YES);
     formMgr.storeInDocCache("default.doc", xmlDoc);
     physicalPage = NavigationManager.getNavMan().getPhysicalPage("ReportsErrors", request);
     %>
     <jsp:forward page='<%= physicalPage %>' />
     <%
}

//T36000016123 - R 8.1.0.6 ER   jgadela  -   handling validation exceptions  [START]
Vector            errorList          = null;
int               severityLevel      = 0;
int               errorListSize      = 0;
boolean fromException = false;
xmlDoc             = formMgr.getFromDocCache();
errorList     = xmlDoc.getFragments("/Error/errorlist/error");
if(errorList != null){
	errorListSize = errorList.size();
}
if(errorListSize>0){
	fromException = true;
}
xmlDoc = null;

//T36000016123 - R 8.1.0.6 ER   jgadela  -   handling validation exceptions [END]

repName = widDoc.getProperties().getProperty(PropertiesType.NAME);
//T36000016123 - R 8.1.0.6 ER   jgadela - added condition fromException
if(("SpecifyReportButton").equals(buttonPressed) && !pageCall.equalsIgnoreCase("Y") && fromException != true) 
{ 
	try
	{
		webiPromptList = widDoc.getPrompts();
		iPromptCount = webiPromptList.getCount();
		for (int i=0;i< iPromptCount;i++)
		{
			webiPrompt = webiPromptList.getItem(i);
			// the Request.Form contains the values selected in previous page
			// it can contained multiple values for a single variable in a string separated by commas
			formString = "ListOfValues"+i;

			if (webiPrompt.getType() == PromptType.Mono)
			{
				// case for an input text
				String  testPrompt = request.getParameter(formString);

				String[] testPrompt2 = new String[2];
				testPrompt2[0] = testPrompt;

				if( testPrompt2.length > 0)
					LOV = testPrompt2;

				//webiPrompt.enterValue(request.getParameter(formString));
				StringTokenizer strTokenizer = new StringTokenizer(testPrompt, ";");
				int iValueCount = strTokenizer.countTokens();
				String[] astrValues = new String[iValueCount];

				PromptType pt = webiPrompt.getType();

				int iValueIndex = 0;
				for (iValueIndex = 0; iValueIndex < iValueCount; iValueIndex++)
				{
					
					astrValues[iValueIndex] = strTokenizer.nextToken();
					
						
					if (webiPrompt.getObjectType() == ObjectType.DATE){
					
						//jgadela 01/16/2014 Rel 8.4 IR T36000023438 -[START] Modified to take date format as per selected user date format
						String  retDt = astrValues[iValueIndex];
						int intFirstVal = retDt.indexOf("-");
						String dtFirsrVal = retDt.substring(0,intFirstVal);
						int intSecVal = retDt.lastIndexOf("-");
						String dtSecVal = retDt.substring(intFirstVal+1,intSecVal);
						int dtlen = retDt.length();
						String dtThirdVal = retDt.substring(intSecVal+1,dtlen);
						
						if(locale.equals("en_GB"))
						{ 
							if((intSecVal == 7 ) && (intFirstVal == 4) )
							{
								astrValues[iValueIndex] =  dtSecVal + "/"+  dtThirdVal + "/" + dtFirsrVal;
							}	
						}else{
							if((intSecVal == 7 ) && (intFirstVal == 4) )
							{
								astrValues[iValueIndex] =   dtThirdVal + "/"+   dtSecVal+ "/" + dtFirsrVal;
							}	
						}
						//jgadela 01/16/2014 Rel 8.4 IR T36000023438 -[END]
						}
					}
					webiPrompt.enterValues(astrValues);
					//String [] testValue = webiPrompt.getCurrentValues();
			}
			else
			{
				//Lov listOfValues = webiPrompt.getLOV();
				LOV = request.getParameterValues(formString);
				//for (int j=0;j<LOV.length;j++)
				webiPrompt.enterValues(LOV);
			}
		}
		// when all values has been set the values will be stored by calling the GetHTMLView
		// function
		widDoc.setPrompts();
		widDoc.applyFormat();
		//Set the storage token to download reports
		storageToken = StringFunction.xssCharsToHtml(widDoc.getStorageToken());
		pageToken = StringFunction.xssCharsToHtml(widDoc.getStorageToken());
	}
	catch (REException rex) // have changed the REException class
	{
		 //Catch all exception
		 System.out.println("EXCEPTION in Standard Reports Prompt Page1 ######################"+rex.getErrorCode()+" "+rex.getMessage());
		 xmlDoc = formMgr.getFromDocCache();
		 mediatorServices = new MediatorServices();
		 mediatorServices.getErrorManager().setLocaleName(resMgr.getResourceLocale());
		 mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.WEBI_INVALID_DATE_ERROR);
		 mediatorServices.addErrorInfo();
		 xmlDoc.setComponent("/Error",         mediatorServices.getErrorDoc());
		 xmlDoc.setAttribute("/In/HasErrors",  TradePortalConstants.INDICATOR_YES);
		 formMgr.storeInDocCache("default.doc", xmlDoc);
		 physicalPage = NavigationManager.getNavMan().getPhysicalPage("CustomReportPrompt1", request);
		 %>
		 <jsp:forward page='<%= physicalPage %>' />
		 <%
	}
	catch (IndexOutOfBoundsException ex) // Check for null dates
	{
		 //Catch all exception
		 System.out.println("EXCEPTION in Custom Reports Prompt Page3 ######################");
		 xmlDoc = formMgr.getFromDocCache();
		 mediatorServices = new MediatorServices();
		 mediatorServices.getErrorManager().setLocaleName(resMgr.getResourceLocale());
		 mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.WEBI_INVALID_DATE_ERROR);
		 mediatorServices.addErrorInfo();
		 xmlDoc.setComponent("/Error",         mediatorServices.getErrorDoc());
		 xmlDoc.setAttribute("/In/HasErrors",  TradePortalConstants.INDICATOR_YES);
		 formMgr.storeInDocCache("default.doc", xmlDoc);
		 physicalPage = NavigationManager.getNavMan().getPhysicalPage("CustomReportPrompt1", request);
		 %>
		 <jsp:forward page='<%= physicalPage %>' />
		 <%
	}	

}


/* Issue HRUM102332850
   Removed Code for Date report Issues - Moved to Custom Report 3  */               
            
widReports = widDoc.getReports();
widReport = widReports.getItem(0);
widReport.setPaginationMode(PaginationMode.QuickDisplay);
pgn = widReport.getPageNavigation();
          
pgn.last();
      
totalNoPages = pgn.getCurrent();

if(pageMode.equalsIgnoreCase(TradePortalConstants.INDICATOR_YES))
{
    pgn.setTo(goToPage);
    repView = (HTMLView) widReport.getView(OutputFormatType.HTML); 

} 

//jgadela R 8.4 CR-854 T36000024797   -[START] Reporting page labels. 
String reportingPageLabelLan = null;
String cbReportingLangOptionInd = null;

Cache CBCache = (Cache)TPCacheManager.getInstance().getCache(TradePortalConstants.CLIENT_BANK_CACHE);
DocumentHandler CBCResult = (DocumentHandler)CBCache.get(userSession.getClientBankOid());
if (!StringFunction.isBlank(userSession.getClientBankOid())){
   cbReportingLangOptionInd = CBCResult.getAttribute("/ResultSetRecord(0)/REPORTING_LANG_OPTION_IND"); // jgadela Rel 8.4 CR-854 
}

if(TradePortalConstants.INDICATOR_YES.equals(cbReportingLangOptionInd)){
	reportingPageLabelLan = userSession.getReportingLanguage();
}else{
	reportingPageLabelLan = userSession.getUserLocale();
}
//jgadela R 8.4 CR-854 T36000024797   -[END]
%>
<%--*******************************HTML for page begins here **************--%>
<jsp:include page="/common/Header.jsp">
  <jsp:param name="includeNavigationBarFlag" 
             value="<%=TradePortalConstants.INDICATOR_YES%>" />
  <jsp:param name="includeErrorSectionFlag" 
             value="<%=TradePortalConstants.INDICATOR_YES%>" />
  <jsp:param name="expandedPage" value="<%=StringFunction.xssCharsToHtml(expandedPage)%>" />
</jsp:include>

<div class="pageMain">
  <div class="pageContent">
<%  
  String pageTitleKey;
  String current2ndNav ="C";
  String helpLink = OnlineHelp.createContextSensitiveLink("customer/report.htm",resMgr,userSession); 
%>
    <%@ include file="/reports/fragments/Reports-SecondaryNavigation.frag" %>
    <input type=hidden value='<%=current2ndNav%>' name="current2ndNav" id="current2ndNav">
<%		
  pageTitleKey = repName;
  // figures out how to return to the calling page
  StringBuffer parms = new StringBuffer();
  parms.append("&returnAction=");
  String selectedCategory = request.getParameter("selectedCategory");
  parms.append(EncryptDecrypt.encryptStringUsingTripleDes("goToReportsHome", userSession.getSecretKey()));
  String returnLink = formMgr.getLinkAsUrl("goToReportsHome", "&reportTab=C&current2ndNav=C&selectedCategory="+selectedCategory, response);
  showSaveAsButton = false;
  
//T36000016123 - R 8.1.0.6 ER   jgadela - added condition fromException
  if((("SpecifyReportButton").equals(buttonPressed)||"Y".equals(pageCall)) && fromException != true) {
 
    if (SecurityAccess.hasRights(securityRights, SecurityAccess.MAINTAIN_CUSTOM_REPORT)){ 
      //Edit reports design method
      showEditButton = true;
      editReportMode = TradePortalConstants.REPORT_EDIT_CUSTOM; 
      editNavPage = formMgr.getLinkAsUrl("goToNewReportStep1", response);
			  	  
      //Reports delete button
      showDeleteButton = true;
      reportTab = TradePortalConstants.CUSTOM_REPORTS;
      deleteNavPage = formMgr.getLinkAsUrl("goToReportsDelete", response);
      
      // Reports Save as Button
      showSaveAsButton = true;
      saveAsNavPage = formMgr.getLinkAsUrl("goToEditReportStep3", response);  
    }
  }else{
    showSaveAsButton = false;
  }
%>      
    <%--cquinton 2/14/2013 do not display divider if already expanded--%>
    <div id="divider1" class="subHeaderDivider"<%=("Y".equals(expandedPage))?" style=\"display:none;\"":""%>></div>
 
    <%--cquinton 2/14/2013 add expandNode - everthing within this expands to the page size--%>
    <div id="expandNode"<%=("Y".equals(expandedPage))?" class=\"expandedPage\"":""%> >
   
      <jsp:include page="/common/ReportsSubHeaderBar.jsp">
        <jsp:param name="returnLink" value="<%=returnLink%>" />
        <jsp:param name="pageTitleKey" value="<%=pageTitleKey%>" />
        <jsp:param name="showExpandCollapse" value="true" />
        <jsp:param name="showEditButton" value="<%=showEditButton%>"/>
        <jsp:param name="showSaveAsButton" value="<%=showSaveAsButton%>"/>
        <jsp:param name="showDeleteButton" value="<%=showDeleteButton%>"/>	
        <jsp:param name="sToken" value="<%=storageToken%>"/>
        <jsp:param name="repName" value="<%=repName%>"/>
        <jsp:param name="reportDesc" value="<%=reportDesc%>"/>
        <jsp:param name="reportTab" value="<%=reportTab%>"/>
        <jsp:param name="editReportMode" value="<%=editReportMode%>"/>
        <jsp:param name="editNavPage" value="<%=editNavPage%>"/> 
        <jsp:param name="saveAsNavPage" value="<%=saveAsNavPage%>"/> 
        <jsp:param name="deleteNavPage" value="<%=deleteNavPage%>"/>
        <jsp:param name="expandedPage"   value="<%=StringFunction.xssCharsToHtml(expandedPage)%>" />
      </jsp:include>

<%
  //View PDF
  newLink = new StringBuffer();
  newLink.append("/portal/reports/viewHTML.jsp?");
  newLink.append("&storageToken=" + storageToken);
  viewURLLink = newLink.toString();
  
  if (iPrompts > 0) {
    reportdivID = "customReportPromptPDFId";
    divID = "SpecifyReportCriteria";
%>
    
      <form id="customReportPrompt" name="customReportPrompt" method="post" data-dojo-type="dijit.form.Form" 
            action="<%=formMgr.getSubmitAction(response)%>">
        <input type=hidden value="<%=buttonPressed%>" name="buttonPressed" id="buttonPressed"/>						
        <jsp:include page="/common/ErrorSection.jsp" />		
        <div class="errorsAndWarnings" id="errorsAndWarnings1"
             style="display: none; width:900px;">
          <div class="errorText" id="errorText1"></div>
        </div>

        <div id="SpecifyReportCriteria">
          <%=widgetFactory.createSectionHeader("","SpecifyReport.SpecifyReportCriteria")%>
            <%@ include file="fragments/SpecifyReportCriteria.frag"%>
          </div>
        </div>
        <div id="customReportPromptPDFId">
        <% if("Y".equals(pageCall)) {%>
        <input type=hidden value="Y" name="pageCall" id="pageCall" /> 
		<%}else{ %>
        <input type=hidden value="N" name="pageCall" id="pageCall" /> 
     <% }
//T36000016123 - R 8.1.0.6 ER   jgadela -hidden variables to validate expand/collapse of criteria toggle pane  
if(fromException == true) {%>
        <input type=hidden value="Y" name="fromExceptionH" id="fromExceptionH" /> 
<%}else{ %>
        <input type=hidden value="N" name="fromExceptionH" id="fromExceptionH" /> 
<%}%>   
        
<% 
 //T36000016123 - R 8.1.0.6 ER   jgadela - added condition fromException
    if((("SpecifyReportButton").equals(buttonPressed) || pageCall.equalsIgnoreCase("Y")) && fromException != true) {
%>
          <%@ include file="fragments/CustomReportPromptHTML.frag" %>
<% 
    } 
%>
        </div>
        <div style="clear: both;"></div>
					
        <%= formMgr.getFormInstanceAsInputField("customReportPrompt") %>
        <input type=hidden value="<%=StringFunction.xssCharsToHtml(storageToken)%>" name=storageToken> 
      </form>
<%
  }
%>

    </div> <%--end expandNode--%>

  </div>
</div>

<form name="saveReport" method="POST" action="<%= formMgr.getSubmitAction(response) %>">
	<input type=hidden value="" name=buttonName value="<%=TradePortalConstants.CUSTOM_REPORTS%>" /> 
	<input type="hidden" name="ReportName" /> <input type="hidden" name="ReportDesc" /> 
	<input type="hidden" name="StandardReport" id="StandardReport" /> 
	<input type="hidden" name="StandardCategory" />
	<input type="hidden" name="CustomCategory" /> 
	<input type="hidden" name="storageToken" value="<%=StringFunction.xssCharsToHtml(storageToken)%>" />
	<input type="hidden" name="reportMode" value="<%=TradePortalConstants.REPORT_COPY_CUSTOM%>" /> 
	<input type=hidden value='<%=current2ndNav%>' name="current2ndNav" id="current2ndNav1">
	<input type="hidden" name="selectedCategory" /> 
	<%= formMgr.getFormInstanceAsInputField("saveReport") %>
</form>
<%--cquinton 3/16/2012 Portal Refresh - start--%>
<jsp:include page="/common/Footer.jsp">
   <jsp:param name="displayFooter" 
              value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeNavigationBarFlag" 
              value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>
<%--cquinton 3/16/2012 Portal Refresh - end--%>

<script>

  var expandedPage = "<%=StringFunction.escapeQuotesforJS(expandedPage)%>";

  var local = {};

  function viewReports(buttonName){
    var isRequiredMsg = '<%=isRequiredMsg%>'
    var returnFlag = true;
    var noOfControls = document.getElementById('noOfControls').value;
    var errorMsgs = "";
    
          for(var i = 0; i<noOfControls; i++){
                var val = document.getElementById('ListOfValues'+i).value;
                if(val == "" || val == "dd/mm/yyyy"){
                      var name = document.getElementById('Prompt'+i).value;
                      name = "'"+name+"'";
                      errTxt = isRequiredMsg.replace('{0}',name)
                      
                      errorMsgs += errTxt+"<br>";
                      returnFlag = false;
                }
          }
          
          console.log(errorMsgs);
          if(!returnFlag){
                var errordiv=document.getElementById('errorsAndWarnings1').style.display='block';
                var errorTextDiv = document.getElementById('errorText1');
                errorTextDiv.innerHTML = "";
                errorTextDiv.innerHTML = errorMsgs;
                return false;
          }
          
          var buttonClicked = document.getElementById('buttonPressed');
          buttonClicked.value = buttonName;
          document.forms[0].submit();
  }
    
  require(["dijit/registry", "dojo/on", "dojo/ready"],
          function(registry, on, ready) {
          ready( function(){
          	
          var buttonClicked = document.getElementById('buttonPressed');
          var pageCall = document.getElementById('pageCall');
          var fromException = document.getElementById('fromExceptionH');
       
          if((buttonClicked.value == 'SpecifyReportButton' || pageCall.value == 'Y') && fromException.value!='Y' ){
          var section=registry.byId("section");
          section.set("open",false);
         
          }
  });
});
  require(["dojo/dom", "dojo/dom-style", "dojo/dom-class", "dojo/query", "dijit/registry"],
      function( dom, domStyle, domClass, query, registry ) {

    local.expandPage = function() {
      var expandNode = dom.byId("expandNode");
      if ( expandNode ) {
        <%-- hide everything else --%>
        query(".pageHeader").forEach( function(entry, i) {
          domStyle.set(entry, "display", "none");
        });
        query(".pageHeaderNavigation").forEach( function(entry, i) {
          domStyle.set(entry, "display", "none");
        });
        query(".secondaryNav").forEach( function(entry, i) {
          domStyle.set(entry, "display", "none");
        });
        query("#divider1").forEach( function(entry, i) {
          domStyle.set(entry, "display", "none");
        });
        query(".pageFooter").forEach( function(entry, i) {
          domStyle.set(entry, "display", "none");
        });
        query("#reportSection").forEach( function(entry, i) {
          domStyle.set(entry, "width", "auto");
          domStyle.set(entry, "height", "auto");
          domStyle.set(entry, "overflow", "visible");
        });
        domClass.add(expandNode, "expandedPage");

        var expandButton = registry.byId("ExpandButton");
        var collapseButton = registry.byId("CollapseButton");
        if (expandButton && collapseButton) {
          domStyle.set(expandButton.domNode, "display", "none");
          domStyle.set(collapseButton.domNode, "display", "inline-block");
        }

        expandedPage = "Y";
        local.setPageLinkExpandedPageParmVal( "firstPageLink", "Y" );
        local.setPageLinkExpandedPageParmVal( "prefPageLink", "Y" );
        local.setPageLinkExpandedPageParmVal( "nextPageLink", "Y" );
        local.setPageLinkExpandedPageParmVal( "lastPageLink", "Y" );
      }
    }

    local.collapsePage = function() {
      var expandNode = dom.byId("expandNode");
      if ( expandNode ) {
        <%-- and remove the expand class --%>
        domClass.remove(expandNode, "expandedPage");
        query("#reportSection").forEach( function(entry, i) {
          domStyle.set(entry, "width", "100%");
          domStyle.set(entry, "height", "375px");
          domStyle.set(entry, "overflow", "auto");
        });
        <%-- show everything that was hidden above --%>
        query(".pageHeader").forEach( function(entry, i) {
          domStyle.set(entry, "display", "block");
        });
        query(".pageHeaderNavigation").forEach( function(entry, i) {
          domStyle.set(entry, "display", "block");
        });
        query(".secondaryNav").forEach( function(entry, i) {
          domStyle.set(entry, "display", "block");
        });
        query("#divider1").forEach( function(entry, i) {
          domStyle.set(entry, "display", "block");
        });
        query(".pageFooter").forEach( function(entry, i) {
          domStyle.set(entry, "display", "block");
        });

        var expandButton = registry.byId("ExpandButton");
        var collapseButton = registry.byId("CollapseButton");
        if (expandButton && collapseButton) {
          domStyle.set(collapseButton.domNode, "display", "none");
          domStyle.set(expandButton.domNode, "display", "inline-block");
        }

        expandedPage = "N";
        local.setPageLinkExpandedPageParmVal( "firstPageLink", "N" );
        local.setPageLinkExpandedPageParmVal( "prefPageLink", "N" );
        local.setPageLinkExpandedPageParmVal( "nextPageLink", "N" );
        local.setPageLinkExpandedPageParmVal( "lastPageLink", "N" );
      }
    },

    <%-- this assumes expandedPage parm exists and is the last in the href url --%>
    local.setPageLinkExpandedPageParmVal = function( pageLinkId, parmVal ) {
      var pageLink = dom.byId(pageLinkId);
      if ( pageLink ) {
        var myHref = pageLink.href;
        var parmIdx = myHref.indexOf("expandedPage=");
        if ( parmIdx >= 0 ) {
          parmIdx = parmIdx + 13;
          myHref = myHref.substring(0, parmIdx) + parmVal;
        }
        pageLink.href = myHref;
      }
    }
  });
</script>

</body>
</html>

<%
// Finally, reset the cached document to eliminate carryover of
// information to the next visit of this page.

formMgr.storeInDocCache("default.doc", new DocumentHandler());

Debug.debug("***END*********************CUSTOM*REPORTS*PROMPT*PAGE1***************************END***"); 
%>
