<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*, com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*, com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*, com.ams.tradeportal.html.*,
                 com.ams.tradeportal.busobj.webbean.*,com.businessobjects.rebean.wi.*,java.io.*,java.math.*,java.text.*"%>



<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" 
   scope="session">
</jsp:useBean>


<%
Debug.debug("***START********************VIEW*CSV*PAGE**************************START***");
String                  storageToken        = null;
String                  reportName          = null;
Object                  name                = null;
StringBuffer            defaultFileName     = null;
DocumentInstance		widDoc				= null;
CSVView 		docCSVView = null;
BinaryView 		docBinaryView = null;
int                     iNbRows             = 0;
int                     iNbCols             = 0;
Float                   fColumn             = new Float(0.0) ;
double                  dColumn             = 0;
BigDecimal              fDecimal;

//E6.5 changes
ReportEngine			cdzReportEngine     = null;
boolean					blnStartTransfer    = false;
String eol = "\n";      //End of line
String vd = "\"";       //Value delimiter
String cs = ",";        //Column separator
ServletOutputStream outputStream = null; 

%>
<%@ include file="fragments/wistartpage.frag" %>
<%
response.reset();
response.setHeader("Cache-Control","no-store"); 
response.setHeader("Pragma","no-cache");
response.setDateHeader("expires", 0);
request.setCharacterEncoding("UTF-8");

try
{
	
	
	storageToken = request.getParameter("storageToken");
	//Request coming for the first time
	widDoc = reportEngine.getDocumentFromStorageToken(storageToken);
	// Set the default file name to use for the report data file to be downloaded
	defaultFileName = new StringBuffer();

	//Retrieve the report name from the report
	reportName = widDoc.getProperties().getProperty(PropertiesType.NAME, "");

	defaultFileName.append((reportName+".csv").replace('/', '_'));

	DataProviders arrDataProviders = widDoc.getDataProviders();
	DataProvider objectDP = arrDataProviders.getItem(0);
   // docCSVView = (CSVView)arrDataProviders.getView(OutputFormatType.CSV);
    docCSVView = (CSVView)objectDP.getResultAsCSV(0);
	
	//response.setHeader("Content-Type", "application/vnd.ms-csv");
    String disposition = "attachment; fileName="+defaultFileName;
	String CSVFile = docCSVView.getContent();	
	
	
	//T36000010324	PR BMO Issue 51 - after running a report, and selecting the CSV button, the data does not map into columns automatically.	
	//Replaced ';' with ','  ***This is temporary fix, we need to do like old portal later
	CSVFile = CSVFile.replaceAll(";", ",");
	// 	out.println(CSVFile);
	// IR T36000038917 Rel9.5 04/26/2016
	response.setContentType("application/csv,charset=UTF-8");   
	//response.setContentType("application/unknown"); //this also works   
	response.setCharacterEncoding("UTF-8");
	response.setHeader("Content-Disposition",disposition); // set the file name to whatever required..
	outputStream = response.getOutputStream();
	// IR T36000038917 Rel9.5 05/09/2016 - requires cp1252 encoding to open correctly in msexcel
	outputStream.write(CSVFile.getBytes("cp1252"));
	/* New Addi */
}catch (REException rex)
{
	//Catch all exception
    System.out.println("EXCEPTION in VIEW PDF Page ######################"+rex.getErrorCode()+" "+rex.getMessage());
 
    // SureshL IR-T36000032903 10/30/2014 Start 
}
finally{

// response.getOutputStream().flush();
// response.getOutputStream().close();

}
// SureshL IR-T36000032903 10/30/2014 End 
//Garbage Collection
widDoc = null;
docCSVView = null;
outputStream = null;
%>

<%
Debug.debug("***END********************VIEW*PDF*PAGE**************************END***");
%>