<%--
 *
 *     Copyright  � 2012                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*, com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*, com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*, com.ams.tradeportal.html.*,
                 com.ams.tradeportal.busobj.webbean.*,com.businessobjects.rebean.wi.*,java.util.*,com.ams.tradeportal.busobj.util.SecurityAccess" %>



<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" 
   scope="session">
</jsp:useBean>
<%@ include file="fragments/reportVariables.frag" %>
<%
Debug.debug("***START*********************CUSTOM*REPORTS*PROMPT*PAGE3***************************START***"); 
//first try to retrieve the session by using the WebIntelligence Cookie
//if not switch back to default page
String            	redirectURI             = null;
String              cell                    = null;
String              sAction                 = null;
DocumentInstance    webiDocument            = null;
MediatorServices   	mediatorServices        = null;
DocumentHandler     xmlDoc                  = null;
String              physicalPage            = null;
String 		    	storageToken 	    	= null;
//BOXIR2 Changes
DocumentInstance 	widDoc 					= null;
Prompts  			webiPromptList 			= null;
Prompt  			webiPrompt 				= null;
Reports 			widReports 				= null;
PaginationMode 		pgMode 					= null;
int 				iPrompts 				= 0;
int 				totalNoPages 			= 0;
PageNavigation 		pgn 					= null;
String 				pageToken 				= null;
String 				formString				= null;
String [] 			LOV 					= null;
boolean 			end;
String [] 			listOfValues			= null;
int 				iPromptCount 			= 0;
HTMLView 			repView 				= null;
Report 				widReport 				= null;
String 				pageMode 				= null;
int 				prevPage 				= 0;
int 				nextPage 				= 0;
String 				pageCall 	 			= null;
String 				pageNum 				= null;
int 				goToPage 				= 0;
String 				locale					= null;

%>

<%@ include file="fragments/wistartpage.frag" %>

<%
storageToken = request.getParameter("storageToken");
if(request.getParameter("pageToken")!=null)
{
	pageToken = request.getParameter("pageToken");
	
}
PropertyResourceBundle portalProperties = (PropertyResourceBundle) PropertyResourceBundle.getBundle("TradePortal");
pageMode = portalProperties.getString("pageMode");
String securityRights = userSession.getSecurityRights();
if( pageMode == null)
{
    pageMode = "n";

}
//Locale to parse the report date paramater
locale = portalProperties.getString("locale");
if(pageMode.equalsIgnoreCase(TradePortalConstants.INDICATOR_YES))
{
	if (request.getParameter("pageCall")!=null)
	{
		pageCall = request.getParameter("pageCall");
        
	}
	else
	{
		pageCall ="N";
	}


	if (request.getParameter("goToPage")!=null) 
	{
   		pageNum = request.getParameter("goToPage");
	   	goToPage = (new Integer(pageNum).intValue());

	}
	else 
	{
   		goToPage = 1;
   
	}

}
else
{

  pageCall ="N";

}
try
{
	if (pageCall.equals("Y"))
	{
		//Request coming from the same page
		widDoc = reportEngine.getDocumentFromStorageToken(pageToken);
	}
	else
	{

		//Request coming for the first time
		widDoc = reportEngine.getDocumentFromStorageToken(storageToken);
		widDoc.refresh();
	}	
}catch (REException wie)
{
     //Catch all exception
     System.out.println("EXCEPTION in Custom Reports Prompt Page3 ######################"+wie.getErrorCode()+" "+wie.getMessage());
                        
     xmlDoc = formMgr.getFromDocCache();
     mediatorServices = new MediatorServices();
     mediatorServices.getErrorManager().setLocaleName(resMgr.getResourceLocale());
     mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.WEBI_SESSION_ERROR);
     mediatorServices.addErrorInfo();
     xmlDoc.setComponent("/Error",         mediatorServices.getErrorDoc());
     xmlDoc.setAttribute("/In/HasErrors",  TradePortalConstants.INDICATOR_YES);
     formMgr.storeInDocCache("default.doc", xmlDoc);
     physicalPage = NavigationManager.getNavMan().getPhysicalPage("ReportsErrors", request);
     %>
     <jsp:forward page='<%= physicalPage %>' />
     <%
}


repName = widDoc.getProperties().getProperty(PropertiesType.NAME);
if (pageCall.equals("N"))
{ 
	
	try
	{
		webiPromptList = widDoc.getPrompts();
		iPromptCount = webiPromptList.getCount();
		for (int i=0;i< iPromptCount;i++)
		{
			webiPrompt = webiPromptList.getItem(i);
			// the Request.Form contains the values selected in previous page
			// it can contained multiple values for a single variable in a string separated by commas
			formString = "ListOfValues"+i;

			if (webiPrompt.getType() == PromptType.Mono)
			{
				// case for an input text
				String  testPrompt = request.getParameter(formString);

				String[] testPrompt2 = new String[2];
				testPrompt2[0] = testPrompt;

				if( testPrompt2.length > 0)
					LOV = testPrompt2;

					//webiPrompt.enterValue(request.getParameter(formString));

				StringTokenizer strTokenizer = new StringTokenizer(testPrompt, ";");
				int iValueCount = strTokenizer.countTokens();
				String[] astrValues = new String[iValueCount];

				PromptType pt = webiPrompt.getType();

				int iValueIndex = 0;
				for (iValueIndex = 0; iValueIndex < iValueCount; iValueIndex++)
				{
					
					astrValues[iValueIndex] = strTokenizer.nextToken();
					
						
					if (webiPrompt.getObjectType() == ObjectType.DATE){
					
						if(locale.equals("en_GB")){ 

							String  retDt = astrValues[iValueIndex];


							int intFirstVal = retDt.indexOf("/");

							String dtFirsrVal = retDt.substring(0,intFirstVal);

							int intSecVal = retDt.lastIndexOf("/");

							String dtSecVal = retDt.substring(intFirstVal+1,intSecVal);

							int dtlen = retDt.length();

							String dtThirdVal = retDt.substring(intSecVal+1,dtlen);

							if((intSecVal == 5 ) && (intFirstVal == 2) ){

							astrValues[iValueIndex] = dtSecVal + "/"+ dtFirsrVal + "/" + dtThirdVal;

							}
							

						}
				
					
					}
					
									
					webiPrompt.enterValues(astrValues);
					//String [] testValue = webiPrompt.getCurrentValues();
				
				
				}

			}
			else
			{
				//Lov listOfValues = webiPrompt.getLOV();
				LOV = request.getParameterValues(formString);

				//for (int j=0;j<LOV.length;j++)
				webiPrompt.enterValues(LOV);

			}


		}

		// when all values has been set the values will be stored by calling the GetHTMLView
		// function
			

		widDoc.setPrompts();
		widDoc.applyFormat();


		//Set the storage token to download reports
		storageToken = widDoc.getStorageToken();
		pageToken = widDoc.getStorageToken();
		
	}
	catch (REException rex) // have changed the REException class
	{
		 //Catch all exception
		 System.out.println("EXCEPTION in Standard Reports Prompt Page1 ######################"+rex.getErrorCode()+" "+rex.getMessage());
		 xmlDoc = formMgr.getFromDocCache();
		 mediatorServices = new MediatorServices();
		 mediatorServices.getErrorManager().setLocaleName(resMgr.getResourceLocale());
		 mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.WEBI_INVALID_DATE_ERROR);
		 mediatorServices.addErrorInfo();
		 xmlDoc.setComponent("/Error",         mediatorServices.getErrorDoc());
		 xmlDoc.setAttribute("/In/HasErrors",  TradePortalConstants.INDICATOR_YES);
		 formMgr.storeInDocCache("default.doc", xmlDoc);
		 physicalPage = NavigationManager.getNavMan().getPhysicalPage("CustomReportPrompt1", request);
		 %>
		 <jsp:forward page='<%= physicalPage %>' />
		 <%
	}
	catch (IndexOutOfBoundsException ex) // Check for null dates
	{
		 //Catch all exception
		 System.out.println("EXCEPTION in Custom Reports Prompt Page3 ######################");
		 xmlDoc = formMgr.getFromDocCache();
		 mediatorServices = new MediatorServices();
		 mediatorServices.getErrorManager().setLocaleName(resMgr.getResourceLocale());
		 mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.WEBI_INVALID_DATE_ERROR);
		 mediatorServices.addErrorInfo();
		 xmlDoc.setComponent("/Error",         mediatorServices.getErrorDoc());
		 xmlDoc.setAttribute("/In/HasErrors",  TradePortalConstants.INDICATOR_YES);
		 formMgr.storeInDocCache("default.doc", xmlDoc);
		 physicalPage = NavigationManager.getNavMan().getPhysicalPage("CustomReportPrompt1", request);
		 %>
		 <jsp:forward page='<%= physicalPage %>' />
		 <%
	}	

}
               
            
widReports = widDoc.getReports();
widReport = widReports.getItem(0);
widReport.setPaginationMode(PaginationMode.QuickDisplay);
pgn = widReport.getPageNavigation();
          
pgn.last();
      
totalNoPages = pgn.getCurrent();

if(pageMode.equalsIgnoreCase(TradePortalConstants.INDICATOR_YES))
{
    pgn.setTo(goToPage);
    repView = (HTMLView) widReport.getView(OutputFormatType.HTML); 

}
	
 
%>


<%--*******************************HTML for page begins here **************--%>

<%-- Begin HTML header decleration --%>    

		<jsp:include page="/common/Header.jsp">
			<jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
			<jsp:param name="includeErrorSectionFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
		</jsp:include>

<div class="pageMain">
	<div class="pageContent">
		 <%
  String pageTitleKey;
    pageTitleKey = "Reports.ReportName";
%>
<jsp:include page="/common/PageHeader.jsp">
   <jsp:param name="titleKey" value="<%=pageTitleKey%>" />
   <jsp:param name="item1Key" value="Common.Reports" />
</jsp:include>
<%		
  	pageTitleKey = repName;
	// figures out how to return to the calling page
	StringBuffer parms = new StringBuffer();
	parms.append("&returnAction=");
	parms.append(EncryptDecrypt.encryptStringUsingTripleDes("goToReportsHome", userSession.getSecretKey()));
	String returnLink = formMgr.getLinkAsUrl("goToReportsHome", parms.toString(), response);
%>
 <%
	//Edit reports design method
  editReportMode = TradePortalConstants.REPORT_EDIT_CUSTOM; 
  editNavPage = formMgr.getLinkAsUrl("goToNewReportStep1", response);
  %>
  <%  //Save as Standard Reports button
  saveAsNavPage = formMgr.getLinkAsUrl("goToEditReportStep3", response);      
  %>
  <%    //Reports delete button
  reportTab = TradePortalConstants.CUSTOM_REPORTS;
  deleteNavPage = formMgr.getLinkAsUrl("goToReportsDelete", response);
  %>      
<jsp:include page="/common/ReportsSubHeaderBar.jsp">
	<jsp:param name="returnLink" value="<%=returnLink%>" />
	<jsp:param name="pageTitleKey" value="<%=pageTitleKey%>" />
	<jsp:param name="showEditButton" value="<%=showEditButton%>"/>
	<jsp:param name="showSaveAsButton" value="<%=showSaveAsButton%>"/>
	<jsp:param name="showDeleteButton" value="<%=showDeleteButton%>"/>	
	<jsp:param name="sToken" value="<%=pageToken%>"/>
	<jsp:param name="repName" value="<%=repName%>"/>
	<jsp:param name="reportTab" value="<%=reportTab%>"/>
	<jsp:param name="editReportMode" value="<%=editReportMode%>"/>
	<jsp:param name="editNavPage" value="<%=editNavPage%>"/> 
	<jsp:param name="saveAsNavPage" value="<%=saveAsNavPage%>"/> 
	<jsp:param name="deleteNavPage" value="<%=deleteNavPage%>"/>  
</jsp:include>
<jsp:include page="/common/ReportsTypesSubHeaderBar.jsp">
	<jsp:param name="sToken" value="<%=pageToken%>"/>
	<jsp:param name="divID" value="PDFId"/>	 
</jsp:include>
<%-- <form method="post" action="../FullHTML/Replace"100%" border="0" cellspacing="0" cellpadding="0" height="34"> --%>
<form id="customReportsPage3" name="customReportsPage3" method="post" data-dojo-type="dijit.form.Form" action="<%=formMgr.getSubmitAction(response)%>">
			<input type=hidden value="" name=buttonName>			
			<div class="formArea">
				<jsp:include page="/common/ErrorSection.jsp" />
				<%-- error section goes above form content --%>
				<div class="formContent">
					
				
					<%-- <%= widgetFactory.createSectionHeader("", "SpecifyReport.SpecifyReportCriteria") %>
					<%@ include file="CustomReportPrompt1.jsp"%> --%>
				
					</div>
				</div>
				<%-- Form Content End Here --%>
			</div>
			<%--formArea--%>
			<%= formMgr.getFormInstanceAsInputField("saveReport") %>

<%-- End HTML header decleration --%>    
<%-- 
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="" bgcolor="#FFFFFF">
<form method="post" action="../FullHTML/Replace"100%" border="0" cellspacing="0" cellpadding="0" height="34">
  

<%

	if(pageMode.equalsIgnoreCase(TradePortalConstants.INDICATOR_YES))
	{


				
		//Retrieve the reports in HTML format and print the body

	        if(pgn.getCurrent()==1)
			{
			prevPage = 1;
			}
			else
			{
			prevPage = pgn.getCurrent()-1;
			}

			
			if(pgn.getCurrent() == totalNoPages)
			{
			  nextPage = totalNoPages ;
			}
			else
			{
			  nextPage = pgn.getCurrent()+1;
			}
		if (totalNoPages != 1)
		{
%>

			<Table align="center" >

			<TR align="center">
			<TD><a class="ListHeaderText" href="/portal/reports/StandardReportPrompt3.jsp?goToPage=1&pageCall=Y&pageToken=<%=pageToken%>&storageToken=<%=storageToken%>">First Page</a></TD>

			<TD><a class="ListHeaderText" href="/portal/reports/StandardReportPrompt3.jsp?goToPage=<%=prevPage%>&pageCall=Y&pageToken=<%=pageToken%>&storageToken=<%=storageToken%>">Previous Page</a></TD>


			<TD><a class="ListHeaderText" href="/portal/reports/StandardReportPrompt3.jsp?goToPage=<%=nextPage%>&pageCall=Y&pageToken=<%=pageToken%>&storageToken=<%=storageToken%>">Next Page</a></TD>


			<TD><a class="ListHeaderText" href="/portal/reports/StandardReportPrompt3.jsp?goToPage=<%=totalNoPages%>&pageCall=Y&pageToken=<%=pageToken%>&storageToken=<%=storageToken%>">Last Page</a></TD>
			</TR>

			</Table>

<%


		}

		

%>

	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	    <tr>
	      <td width="20" nowrap>&nbsp;</td>
	      <td width="100%" class="ListText"> 
	      </td>
	      <td width="20" nowrap>&nbsp;</td>
	    </tr>
	  </table>

	<%
	//out.println(((HTMLView) repView).getStringPart("head",false));
	//out.println(((HTMLView) repView).getStringPart("body",false));
	
	repView = (HTMLView) widReport.getView(OutputFormatType.HTML); 
	
	if(repView!=null)
	{
		
		 		
		String strHeight = "height:50%";
		String strWidth = "width:100%";
		String strDivStart = "<DIV id = WebiContent style=\"postion:relative";
		String strDivEnd = "\">";
		String strHead = repView.getStringPart("head",false);
		out.println(strHead);
		String strBody = repView.getStringPart("body",false);
		int iDiv1 = strBody.indexOf("<div");
		int iDiv2 = strBody.indexOf("<div",iDiv1+1);
		String strDiv = strBody.substring(iDiv1,iDiv2);
		StringTokenizer stk = new StringTokenizer(strDiv,";");

		while(stk.hasMoreTokens())
		{
			String strToken = stk.nextToken();

			if(strToken.startsWith("width"))
			{
				strWidth = strToken;
			}
			else if(strToken.startsWith("height"))
			{
				strHeight = strToken;
			}
		}

		while(strBody.indexOf("overflow:hidden;")>-1){
			int istrBodylength = strBody.length();
			int istrBodybeginOverFlow = strBody.indexOf("overflow:hidden;");
			String strBodyPart1 = strBody.substring(0,istrBodybeginOverFlow-1);
			String strBodyPart2 = strBody.substring(istrBodybeginOverFlow+16 ,istrBodylength);
			strBody = strBodyPart1 + strBodyPart2;

		}
		

		while(strBody.indexOf("overflow: hidden;")>-1){
			int istrBodylength = strBody.length();
			int istrBodybeginOverFlow = strBody.indexOf("overflow: hidden;");
			String strBodyPart1 = strBody.substring(0,istrBodybeginOverFlow-1);
			String strBodyPart2 = strBody.substring(istrBodybeginOverFlow+17 ,istrBodylength);
			strBody = strBodyPart1 + strBodyPart2;

		}

		while(strBody.indexOf("ro:style")>-1)
		{
			strBody = strBody.replaceAll("ro:style","style");
		}
                if(strBody.indexOf("<div class=\"s1 ovh\" style=\"position: absolute;left:76px;top:124px;")> -1){			
			strBody = strBody.replace("<div class=\"s1 ovh\" style=\"position: absolute;left:76px;top:124px;","<div class=\"s1 ovh\" style=\"position: absolute;left:76px;top:48px;");
		}
		

		%>
		<DIV ID="webIContent" style=" position:relative;<%=strWidth%>;<%=strHeight%>">  

		<%

		out.println(strBody);

		%>

		</DIV>

		<%

		out.flush();

	
	}

	%>


	  <table width="100%" border="0" cellspacing="0" cellpadding="0">
	    <tr>
	      <td>&nbsp;</td>
	    </tr>
	  </table>


	<%
	}
	else
	{

		 	
	 	for(int pgcount = 1;pgcount<=totalNoPages;pgcount++){
	 		pgn.setTo(pgcount);
	 		repView = (HTMLView) widReport.getView(OutputFormatType.HTML); 
	 		
	 		String strHeight = "height:50%";
	 		String strWidth = "width:100%";
	 		String strDivStart = "<DIV id = WebiContent style=\"postion:relative";
	 		String strDivEnd = "\">";
	 		String strHead = repView.getStringPart("head",false);
	 		out.println(strHead);
	 		String strBody = repView.getStringPart("body",false);
	 		int iDiv1 = strBody.indexOf("<div");
	 		int iDiv2 = strBody.indexOf("<div",iDiv1+1);
	 		String strDiv = strBody.substring(iDiv1,iDiv2);
	 		StringTokenizer stk = new StringTokenizer(strDiv,";");
	 	
	 		while(stk.hasMoreTokens())
	 		{
	 			String strToken = stk.nextToken();
	 			
	 			if(strToken.startsWith("width"))
	 			{
	 				strWidth = strToken;
	 			}
	 			else if(strToken.startsWith("height"))
	 			{
	 				strHeight = strToken;
	 			}
	 		}

			while(strBody.indexOf("overflow:hidden;")>-1){
		 						 		
				int istrBodylength = strBody.length();
				int istrBodybeginOverFlow = strBody.indexOf("overflow:hidden;");

				String strBodyPart1 = strBody.substring(0,istrBodybeginOverFlow-1);

				String strBodyPart2 = strBody.substring(istrBodybeginOverFlow+16 ,istrBodylength);

				strBody = strBodyPart1 + strBodyPart2;

					
		
 			}


			while(strBody.indexOf("overflow: hidden;")>-1){
		 						 		
				int istrBodylength = strBody.length();
				int istrBodybeginOverFlow = strBody.indexOf("overflow: hidden;");

				String strBodyPart1 = strBody.substring(0,istrBodybeginOverFlow-1);

				String strBodyPart2 = strBody.substring(istrBodybeginOverFlow+17 ,istrBodylength);

				strBody = strBodyPart1 + strBodyPart2;

					
		
 			}

			while(strBody.indexOf("ro:style")>-1)
			{
				strBody = strBody.replaceAll("ro:style","style");
			}
			
 	               if(strBody.indexOf("<div class=\"s1 ovh\" style=\"position: absolute;left:76px;top:124px;")> -1){			
	                        strBody = strBody.replace("<div class=\"s1 ovh\" style=\"position: absolute;left:76px;top:124px;","<div class=\"s1 ovh\" style=\"position: absolute;left:76px;top:48px;");
		        }

 				 		
	 		//out.println(strDivStart + ";" + strWidth + ";" + strHeight + strDivEnd);
	                %>
	                <DIV ID="webIContent" style=" position:relative;<%=strWidth%>;<%=strHeight%>">  
	 	         
	 
	                
	                <%
	 
	 		out.println(strBody);
	 		
	 		
	 		//out.println("</DIV>");
	 		
	 		%>
	 		
	 		</DIV>
	 		
	 		<%
	 			
	 		out.flush();
	 		
	 	
	 	}
	
	}		
 
	
%>


  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td></td>
    </tr>
  </table>
  

  <p>&nbsp;</p>
 --%>
 </form>
	</div>
</div>
<jsp:include page="/common/Footer.jsp">
	<jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
	<jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>
<jsp:include page="/common/SidebarFooter.jsp" />


</body>
</html>

<%
//Garbage collection
webiDocument            = null;
//commented by kajal bhatt
//webiSession             = null;
widDoc 		= null;
webiPromptList 	= null;
webiPrompt 	= null;
widReports 	= null;
pgMode 		= null;
pgn 		= null;
repView = null;
widReport = null;


%>

<%
Debug.debug("***END*********************CUSTOM*REPORTS*PROMPT*PAGE3***************************END***"); 
%>