<%--
*******************************************************************************
  Reports Home Page

  Description:  Common place to render the secondary navigation for
                all pages associated with Reports.

  Variables:
    current2ndNav - Standard or Custom (TradePortalConstants)
        

*******************************************************************************
--%>
 <%--
 *	   Dev By Sandeep
 *     Copyright  � 2012                         
 *     CGI, Incorporated 
 *     All rights reserved
--%>
	<%@ page import="com.amsinc.ecsg.frame.*, com.amsinc.ecsg.util.*, com.ams.tradeportal.common.cache.*,
                 com.amsinc.ecsg.html.*, com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*, com.ams.tradeportal.html.*,
                 com.ams.tradeportal.busobj.webbean.*,java.util.*,java.beans.Beans,com.businessobjects.rebean.wi.*,
				 com.crystaldecisions.sdk.framework.*,
                 com.crystaldecisions.sdk.exception.*,com.ams.tradeportal.busobj.util.SecurityAccess, 
                 java.text.*, com.ams.tradeportal.busobj.util.*, com.crystaldecisions.sdk.occa.infostore.IInfoStore, net.sf.json.*, net.sf.json.xml.*
                 " %>

	<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session">
	</jsp:useBean>
	
	<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
	</jsp:useBean>
	
	<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
	</jsp:useBean>
	
	<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" 
	   scope="session">
	</jsp:useBean>
<%
Debug.debug(" *** Inside: ");

DGridFactory dGridFactory = new DGridFactory(resMgr, userSession, formMgr, beanMgr, response);

WidgetFactory widgetFactory = new WidgetFactory(resMgr);
String gridHtml = "";
String gridLayout = "";
session.setAttribute("fromPage", "ReportsHome");
//IR T36000026794 Rel9.0 07/24/2014 starts
session.setAttribute("startHomePage", "ReportsHome");
session.removeAttribute("fromTPHomePage");
//IR T36000026794 Rel9.0 07/24/2014 End
Map searchQueryMap =  userSession.getSavedSearchQueryData();
Map savedSearchQueryData =null;
if(searchQueryMap!=null && !searchQueryMap.isEmpty()){
	savedSearchQueryData = (Map)searchQueryMap.get("ReportsDataView");
}
  // THIS SECTION DETERMINES IF REPORTING IS AVAILABLE IN THIS ENVIRONMENT
  // IF IT IS NOT, THEN JUST DISPLAY A MESSAGE AND NOTHING ELSE
   boolean reportingAvailable = false;

	PropertyResourceBundle portalProperties = null; 
	// Narayan IR -AHUL061457053  06/18/2011 Begin
	String   securityRights     = null;
	boolean isStdMaintainRights = false;
	boolean isCusMaintainRights = false; 
	boolean isStdViewRights     = false;
	boolean isCusViewRights     = false; 
	
	IEnterpriseSession boSessionLocal = (IEnterpriseSession) request.getSession().getAttribute("CE_ENTERPRISESESSION");
	String current2ndNav = null;
	   
	securityRights = userSession.getSecurityRights();
	// Narayan IR -AHUL061457053  06/18/2011 End 
	Debug.debug(" In Reports home ");

try
{
	portalProperties = (PropertyResourceBundle) PropertyResourceBundle.getBundle("TradePortal");
	String reportsModuleAvailable = portalProperties.getString("reportsModuleAvailable");
	
	if(reportsModuleAvailable.equalsIgnoreCase(TradePortalConstants.INDICATOR_YES))
	reportingAvailable = true;
}
catch(MissingResourceException mre)
{
	  // This will happen if the reportsModuleAvailable setting is not in the config file
	  reportingAvailable = false;
}

if(!reportingAvailable)
{
%>
	<jsp:include page="/common/Header.jsp">
	   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
	   <jsp:param name="includeErrorSectionFlag"  value="<%=TradePortalConstants.INDICATOR_YES%>" />
	</jsp:include>  
<%
	out.println("<br><br><font color='red'>&nbsp;&nbsp;&nbsp;The reporting module is not available in this environment</font>");
	return;

}
Debug.debug(" Reporting is available ");
%>
<%
Debug.debug("***START********************REPORTS*HOME**************************START***");

String              reportTab                   = null;
StringBuffer        reportTabLink               = null;
StringBuffer        dropdownOptions             = new StringBuffer();
StringBuffer        extraTags                   = new StringBuffer();
String              value1                      = null;
String              value2                      = null;
String              selectedCategory            = null;
String              selectedCategorySQL         = null;
DocumentHandler     dochdr                      = null;
String              reportType                  = null;
String              userName                    = null;
String              userPass                    = null;
String 		        physicalPage				= null;
String              report                      = null;
MediatorServices    mediatorServices            = null; 
DocumentHandler     xmlDoc                      = null; 
boolean		        reportLoginInd              = false;
StringBuffer        newLink                     = new StringBuffer();
IEnterpriseSession  boSession                   = null;  //added by kajal bhatt on 25/06/07
String 		cmsName 				= null;
String 		cmsAuth 				= null;	


try 
{ 
	if(userSession.getSecurityType().equals(TradePortalConstants.ADMIN)){
		userSession.setCurrentPrimaryNavigation("AdminNavigationBar.Reports");
	}else{
		userSession.setCurrentPrimaryNavigation("NavigationBar.Reports"); 
	}
	

	//added by kajal bhatt on 25/06/07
	//starts here
	try
	{
		boSession = (IEnterpriseSession) request.getSession().getAttribute("CE_ENTERPRISESESSION");
	}
	catch (Exception e)
	{
		Debug.debug("*** <<Exception at the beginnning - printing stacktrace>>: ");
		e.printStackTrace();
	}
	//ends here
	// Determine the suffix to place in front of the organization OID
	
	String reportingUserSuffix = userSession.getReportingUserSuffix();
 
	if(reportingUserSuffix == null)
	 {
	   //rbhaduri - 30th Nov 09 - IR KAUJ112336153 Modify Begin - M And T Bank Change - get reporting 
       	   //suffix based on the level of the user logged in. For ASP level user get the
           //get the suffix from global organization table. But for all other levels, get
	   //the suffix from the client bank table
	   
	   String userOwnerShipLevel = null;
	   userOwnerShipLevel = userSession.getOwnershipLevel();
		
	   if (userOwnerShipLevel.equals(TradePortalConstants.OWNER_GLOBAL))
		{ 
		  		// Get the reporting user suffix from the Global Org to which this org belongs

				//jgadela R90 IR T36000026319 - SQL INJECTION FIX
		  		String reportingUserSuffixSql = "select reporting_user_suffix" 
		                     + " from global_organization"
		                     + " where organization_oid = ? ";
	
		  		DocumentHandler result = DatabaseQueryBean.getXmlResultSet(reportingUserSuffixSql, false, new Object[]{userSession.getGlobalOrgOid()});
		
		  		reportingUserSuffix = result.getAttribute("/ResultSetRecord(0)/REPORTING_USER_SUFFIX");    
		  
		  		// Put it on the session so that later accesses of this page don't have to look it up  
		  		userSession.setReportingUserSuffix(reportingUserSuffix);
		}
	    else
	    	{	    	  
		  		String clientBankOID = null;
		  		clientBankOID = userSession.getClientBankOid();
	    	  
		  		// Get the reporting user suffix from the Client Bank of the logged in user
		  		//jgadela R90 IR T36000026319 - SQL INJECTION FIX
		  		String reportingUserSuffixSql = "select reporting_user_suffix" 
		                     + " from client_bank"
		                     + " where organization_oid = ? ";
	
		  		DocumentHandler result = DatabaseQueryBean.getXmlResultSet(reportingUserSuffixSql, false, new Object[]{clientBankOID});
		
		  		reportingUserSuffix = result.getAttribute("/ResultSetRecord(0)/REPORTING_USER_SUFFIX");
		  
		  		// Put it on the session so that later accesses of this page don't have to look it up
	      		userSession.setReportingUserSuffix(reportingUserSuffix);

	    	}  //rbhaduri - 30th Nov 09 - IR KAUJ112336153 Modify End - M And T Bank Change 
	 }
	
	//rbhaduri - 30th Nov 09 - IR KAUJ112336153 - added system out
	Debug.debug("*** <<Reporting Suffix>>:" + reportingUserSuffix );
	// Place the reporting user suffix on to the beginning of the OID
	// The suffix is used to distinguish between organizations being processed
	// by different ASPs that might be on the same reports server.  
	userName = userSession.getOwnerOrgOid()+reportingUserSuffix;
	Debug.debug("*** <<User Name>>: " + userName );

	// get password from the property file
	userPass = userSession.getOwnerOrgOid() + reportingUserSuffix.toLowerCase(); 
	//initialize config file
    //ConfigFileReader.init(application.getRealPath("WEB-INF"));
	Debug.debug("*** <<User Name in Reports Home is>>: "+userName);
	try //try-2
	{
		Debug.debug("*** Inside webiServer is running loop in ReportsHome"); 
		Debug.debug("\n*** webiServer is running - checking from ReportsHome");

		//Retrieve report login indicator
		reportLoginInd = userSession.getReportLoginInd(); 
	
		//modified by Kajal Bhatt on 25/06/07
		//starts here
		if((boSession == null) || (reportLoginInd == false)) //if-3
		{
			// Attempt logon. Create an Enterprise session manager object.
			ISessionMgr sessionMgr = CrystalEnterprise.getSessionMgr();
			
			// Log on to BusinessObjects Enterprise
                  cmsName = portalProperties.getString("CMSName");
	            cmsAuth = portalProperties.getString("Authentication");	

			Debug.debug("*** <<CMS Name>>: "+ cmsName);
			Debug.debug("*** <<CMS Auth>>: "+ cmsAuth );

			Debug.debug(" user Credentials " + userName + userPass);
			boSession = sessionMgr.logon(userName, userPass, cmsName, cmsAuth);

                  Debug.debug("*** <<boSession>>: " + boSession );
			
			//put the enterprisesession in to session
			request.getSession().setAttribute("CE_ENTERPRISESESSION", boSession);
			
			//get the reportengines object and store it into session
			ReportEngines reportEngines = null;
			reportEngines = (ReportEngines)boSession.getService("ReportEngines");
			request.getSession().setAttribute("ReportEngines", reportEngines);
			//ends here
			
			//get the default storage token and store it into session
			String strToken = boSession.getLogonTokenMgr().getDefaultToken();
			userSession.setReportLoginInd(true); // Nar IR-T36000031433 Rel 9.2.0.0
		    Debug.debug("*** Successfully created webiSession");       
		}//end of if-3
	} //end of try-2
	catch (REException rex) //catch-2
	{
		rex.printStackTrace();
		//Catch all exceptions
		Debug.debug("*** REException in Reports Home>>: "+rex.getErrorCode()+" "+rex.getMessage());
		
		xmlDoc = formMgr.getFromDocCache();
		mediatorServices = new MediatorServices();
		mediatorServices.getErrorManager().setLocaleName(resMgr.getResourceLocale());
		mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.WEBI_SESSION_ERROR);
		mediatorServices.addErrorInfo();
		xmlDoc.setComponent("/Error", mediatorServices.getErrorDoc());
		xmlDoc.setAttribute("/In/HasErrors", TradePortalConstants.INDICATOR_YES);
		formMgr.storeInDocCache("default.doc", xmlDoc);
		physicalPage = NavigationManager.getNavMan().getPhysicalPage("ReportsErrors", request);
		%>
		<jsp:forward page='<%= physicalPage %>'/>
		<%
	}//end of catch-2
	catch(SDKException sdk)
	{
		sdk.printStackTrace();
				StringTokenizer stk = new StringTokenizer(sdk.toString(),"OCA_Abuse");
				Debug.debug("*** <<SDKException in Reports Home>>: " + sdk.getMessage());
		
		//BO User is not setup in CMS
		if( stk.hasMoreTokens())
		{
			Debug.debug("*** <<BO user is not serup in CMS>>: " + userName);
		   
			xmlDoc = formMgr.getFromDocCache();
			mediatorServices = new MediatorServices();
			mediatorServices.getErrorManager().setLocaleName(resMgr.getResourceLocale());
			mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.REPORT_USER_NOT_SETUP);
			mediatorServices.addErrorInfo();
			xmlDoc.setComponent("/Error", mediatorServices.getErrorDoc());
			xmlDoc.setAttribute("/In/HasErrors", TradePortalConstants.INDICATOR_YES);
			formMgr.storeInDocCache("default.doc", xmlDoc);
			physicalPage = NavigationManager.getNavMan().getPhysicalPage("ReportsErrors", request);
			%>
			<jsp:forward page='<%= physicalPage %>'/>
			<% 
				}
				else
				{
		        //Catch all exceptions
			xmlDoc = formMgr.getFromDocCache();
			mediatorServices = new MediatorServices();
			mediatorServices.getErrorManager().setLocaleName(resMgr.getResourceLocale());
			mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.WEBI_SESSION_ERROR);
			mediatorServices.addErrorInfo();
			xmlDoc.setComponent("/Error", mediatorServices.getErrorDoc());
			xmlDoc.setAttribute("/In/HasErrors", TradePortalConstants.INDICATOR_YES);
			formMgr.storeInDocCache("default.doc", xmlDoc);
			physicalPage = NavigationManager.getNavMan().getPhysicalPage("ReportsErrors", request);
			%>
			<jsp:forward page='<%= physicalPage %>'/>
			<%
		}
	}
}
catch (Exception e) 
{ 
	// In this block, catch exceptions from anywhere during the process of attempting to connect
	// to the reports server
	
	Debug.debug("*** Exception occured while in ReportsHome.jsp while attempting to connect to reports server: "); 
	e.printStackTrace();
	xmlDoc = formMgr.getFromDocCache();
	mediatorServices = new MediatorServices();
	mediatorServices.getErrorManager().setLocaleName(resMgr.getResourceLocale());        
	mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, 	TradePortalConstants.WEBI_SERVER_ERROR);
	mediatorServices.addErrorInfo();
	xmlDoc.setComponent("/Error", mediatorServices.getErrorDoc());
	xmlDoc.setAttribute("/In/HasErrors", TradePortalConstants.INDICATOR_YES);
	formMgr.storeInDocCache("default.doc", xmlDoc);
	physicalPage = NavigationManager.getNavMan().getPhysicalPage("ReportsErrors", request); 
	%>
		<jsp:forward page='<%= physicalPage %>' />
	<%
		} 
	
		isStdMaintainRights   = SecurityAccess.hasRights(securityRights, SecurityAccess.MAINTAIN_STANDARD_REPORT);
		isStdViewRights       = SecurityAccess.hasRights(securityRights, SecurityAccess.VIEW_STANDARD_REPORT);
		isCusMaintainRights   = SecurityAccess.hasRights(securityRights, SecurityAccess.MAINTAIN_CUSTOM_REPORT);
		isCusViewRights       = SecurityAccess.hasRights(securityRights, SecurityAccess.VIEW_CUSTOM_REPORT);          	  
	
	 	reportTab = request.getParameter("reportTab");
		
	 	if (reportTab == null)
		{
			reportTab = (String) session.getAttribute("reportTab");
		
			//If reportTab is null set the default reports tab to Standatd Reports
			if (reportTab == null){	    
				if(isStdMaintainRights || isStdViewRights){
				  reportTab = TradePortalConstants.STANDARD_REPORTS;
				 }
				else {
				  reportTab = TradePortalConstants.CUSTOM_REPORTS;
				}
			} 
		}
	 	
	    current2ndNav = request.getParameter("current2ndNav");
	   
	    Debug.debug("*** <<reportTab>>: "+reportTab); 
	    Debug.debug("*** <<current2ndNav>>: "+current2ndNav); 
         
	    // 02-09-2013 CQ T36000011329 - checking the object with all null and non value 
	    if (current2ndNav == null || ("".equals(current2ndNav)) || "null".equals(current2ndNav)){
	    	if(isStdMaintainRights || isStdViewRights){
	          current2ndNav = TradePortalConstants.STANDARD_REPORTS;
	       	}else {
	       		current2ndNav = TradePortalConstants.CUSTOM_REPORTS;
	       	}
	    }
	    
	    Debug.debug("*** <<reportTab>>: "+reportTab); 
	    Debug.debug("*** <<current2ndNav>>: "+current2ndNav); 
	   //Set report type to "S" Standard Reports or "C" to Custom Reports 
	   //based on tab selected
		if(TradePortalConstants.STANDARD_REPORTS.equals(current2ndNav)){
			reportType = TradePortalConstants.STANDARD_REPORTS;  //Standard Reports
			//Retrieve report categories for standard reports
			selectedCategory = request.getParameter("selectedCategory");
			if(selectedCategory == null){
				selectedCategory = request.getParameter("StandardCategory");
			}
			if(StringFunction.isNotBlank(selectedCategory))
			selectedCategory = EncryptDecrypt.base64StringToString(selectedCategory);
		}
		
		if (TradePortalConstants.CUSTOM_REPORTS.equals(current2ndNav)){
			reportType = TradePortalConstants.CUSTOM_REPORTS; //Custom Reports
			//Retrieve report categories for custom reports
			selectedCategory = request.getParameter("selectedCategory");  
			
			if(selectedCategory == null){
				selectedCategory = (String) session.getAttribute("CustomCategory");
			}
			
			if(selectedCategory == null){
				selectedCategory = request.getParameter("CustomCategory");
			}
			if(StringFunction.isNotBlank(selectedCategory))
			selectedCategory = EncryptDecrypt.base64StringToString(selectedCategory);
		}   
		//Default categories to "All Categories"
		if (selectedCategory == null || (selectedCategory != null && (selectedCategory.trim().equals("")|| selectedCategory.trim().equals("null")))){
			selectedCategory = TradePortalConstants.REPORT_ALL_CAT;
		}
			
		if (TradePortalConstants.STANDARD_REPORTS.equals(current2ndNav)){
			session.setAttribute("standardCategory", selectedCategory);
			report = TradePortalConstants.NEW_STANDARD_REPORTS;
		}
		
		if(TradePortalConstants.CUSTOM_REPORTS.equals(current2ndNav)){
			session.setAttribute("customCategory", selectedCategory);
			report = TradePortalConstants.NEW_CUSTOM_REPORTS;
		}
	
	//Take into consideration of the user categories.  Note this is only for standard reports.
		//IR 20512 -comment
	    //selectedCategorySQL = RepUtility.getUserCategorySQL(selectedCategory, beanMgr, userSession.getUserOid(),userSession.hasSavedUserSession(),userSession.getOwnerOrgOid(),userSession.getSecurityType());

		selectedCategorySQL = RepUtility.getUserCategorySQLWithoutQuote(RepUtility.ALL_CATEGORIES, beanMgr, userSession.getUserOid(),userSession.hasSavedUserSession(),userSession.getOwnerOrgOid(),userSession.getSecurityType());
	
	//Method to get reports category
		if (TradePortalConstants.STANDARD_REPORTS.equals(current2ndNav) || TradePortalConstants.CUSTOM_REPORTS.equals(current2ndNav)){
			//getCategory returns list of categories in DocHandler
			//jgadela Rel 8.4 CR-854 [BEGIN]- Added parameter for ReportingLanguage 
			dochdr = RepUtility.getCategories(boSession, reportType, beanMgr, userSession.getUserOid(),userSession.getReportingLanguage());
		}
	
		Debug.debug("*** <<selectedCategorySQL>>: "+selectedCategorySQL);
		Debug.debug("*** <<reportType>>: "+reportType);
		Debug.debug("*** <<Categories>>: "+dochdr);
		Debug.debug("*** After get categories");
		String searchNav = "reportHome."+current2ndNav;  
		
		// jgadela R 8.4 CR-854 T36000024797   -[START] Reporting page labels. 
		String reportingPageLabelLan = null;
		String cbReportingLangOptionInd = null;

		Cache CBCache = (Cache)TPCacheManager.getInstance().getCache(TradePortalConstants.CLIENT_BANK_CACHE);
		DocumentHandler CBCResult = (DocumentHandler)CBCache.get(userSession.getClientBankOid());
		if (!StringFunction.isBlank(userSession.getClientBankOid())){
			   cbReportingLangOptionInd = CBCResult.getAttribute("/ResultSetRecord(0)/REPORTING_LANG_OPTION_IND"); // jgadela Rel 8.4 CR-854 
		}

		if(TradePortalConstants.INDICATOR_YES.equals(cbReportingLangOptionInd)){
			reportingPageLabelLan = userSession.getReportingLanguage();
		}else{
			reportingPageLabelLan = userSession.getUserLocale();
		}
		//jgadela R 8.4 CR-854 T36000024797   -[END]
	%>

	<jsp:include page="/common/Header.jsp">
	   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
	   <jsp:param name="includeErrorSectionFlag"  value="<%=TradePortalConstants.INDICATOR_YES%>" />
	</jsp:include>
	
	<%-- Vshah - 08/02/2012 - Rel8.1 - Portal-refresh --%>
	<%--  including TransactionCleanup.jsp to remove lock on instrument which has been placed by this user... --%>
	<%--  this is required because (with Portal-refresh) user can navigate anywhere from MenuBar when on Instrument(detail) page. --%>
	<jsp:include page="/transactions/TransactionCleanup.jsp"></jsp:include>
	
<jsp:include page="/common/SearchCriteriaCleanup.jsp">
<jsp:param name="searchCriteriaSection" value="<%=searchNav%>"/>
</jsp:include>
	<div class="pageMainNoSidebar">	
		<div class="pageContent">	
		<% // this variable is being used in the  Reports-SecondaryNavigation.frag%>
		<%String helpLink = null; %>
			<%@ include file="/reports/fragments/Reports-SecondaryNavigation.frag" %>
			
			<form id="ReportsListForm" name="ReportsListForm" method="post" data-dojo-type="dijit.form.Form" action="<%= formMgr.getSubmitAction(response) %>">
				<jsp:include page="/common/ErrorSection.jsp" />
				
				<div class="formContentNoSidebar">

                                  <%--cquinton 10/8/2012 include gridShowCounts --%>
                                  <div class="gridSearch">
                                    <div class="searchHeader">
                                
                                
                                 <span class="searchHeaderCriteria">
                                      
 
					<%
						String  urlParam = "&report="+report;
					
						extraTags.append("onchange=\"location='");
			            extraTags.append(formMgr.getLinkAsUrl("goToReportsHome", response));

			   
			   if (TradePortalConstants.STANDARD_REPORTS.equals(current2ndNav)) {
			           		urlParam = urlParam + "&reportMode="+TradePortalConstants.NEW_STANDARD_REPORTS+"&current2ndNav="+TradePortalConstants.STANDARD_REPORTS;
			           	   //12-15-2013 Rel 8.4 CR-854 [BEGIN]- Modified the locale param with  for ReportingLanguage parameter as locale
			        	   	dropdownOptions.append(RepUtility.getUserCategoryOptions(dochdr, selectedCategory, userSession.getReportingLanguage(), beanMgr, userSession.getUserOid(),TradePortalConstants.INDICATOR_NO,userSession.hasSavedUserSession(),userSession.getOwnerOrgOid(),userSession.getSecurityType()));
			        	   	
			        	   	extraTags.append("&amp;reportTab=" + TradePortalConstants.STANDARD_REPORTS + "&amp;standardCategory='+this.options[this.selectedIndex].value\"");
			        	  //12-19-2013 Rel 8.4 CR-854 [BEGIN]- getting show lable
			        	   	String showLable =  resMgr.getText("PendingTransactions.Show",TradePortalConstants.TEXT_BUNDLE, reportingPageLabelLan); 
			        
			        %>
			        	<%= widgetFactory.createSearchSelectField("Category", showLable, "",dropdownOptions.toString(),"onChange='filterStandardReports();'") %>   	
			        
<%
  }
			   if(TradePortalConstants.CUSTOM_REPORTS.equals(current2ndNav)){
							urlParam = urlParam + "&reportMode="+TradePortalConstants.NEW_CUSTOM_REPORTS+"&current2ndNav="+TradePortalConstants.CUSTOM_REPORTS;
			        	   	dropdownOptions.append(Dropdown.createSortedOptions(dochdr, "CATEGORY", "CATEGORY", selectedCategory, resMgr.getResourceLocale()));
			        	   	extraTags.append("&amp;reportTab=" + TradePortalConstants.CUSTOM_REPORTS + "&amp;customCategory='+this.options[this.selectedIndex].value\"");
			        %>
                     <%}%>
                     </span>
			   
			   <span class="searchHeaderActions"> 
			   <%
			   if (TradePortalConstants.STANDARD_REPORTS.equals(current2ndNav)) {
%>
                                        <jsp:include page="/common/gridShowCount.jsp">
                                          <jsp:param name="gridId" value="standardReportDataGridId" />
                                           <jsp:param name="page" value="report" />
                                        </jsp:include>
<%
  }
           else if (TradePortalConstants.CUSTOM_REPORTS.equals(current2ndNav)) {
%>
                                        <jsp:include page="/common/gridShowCount.jsp">
                                          <jsp:param name="gridId" value="customReportDataGridId" />
                                          <jsp:param name="page" value="report" />
                                        </jsp:include>
<%
  }
%>
							
										
					<%
					newLink = new StringBuffer();
				        String  nextPage = "goToNewReportStep1";
				        newLink.append(formMgr.getLinkAsUrl(nextPage,urlParam, response));
				        
				        if(((TradePortalConstants.STANDARD_REPORTS.equals(current2ndNav)) && isStdMaintainRights) || (TradePortalConstants.CUSTOM_REPORTS.equals(current2ndNav) && isCusMaintainRights )){
		           	%> 
		           	<span>				       
		           	 <button data-dojo-type="dijit.form.Button" id="newButton" type="button">
		           	 <%//jgadela 12-19-2013 Rel8.4 CR-854 [BEGIN]- Modified to get the label using locale from page.%>
				          <%=resMgr.getText("Reports.CreateNewReportText",TradePortalConstants.TEXT_BUNDLE,reportingPageLabelLan)%>
				          <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
				            openURL("<%=newLink.toString()%>");        					
	 			          </script>
				        </button> 
				        
				        
					<%
				        }
					%>
					
					</span>	
					
				
                      </span>
                   <%//jgadela 12-19-2013 Rel8.4 CR-854 [BEGIN]- Modified to get the label using locale from page.%>
                   <%=widgetFactory.createHoverHelp("newButton","NewHoverText",reportingPageLabelLan) %>
                     <div style="clear:both;">
                     </div>                
                                  </div>
                     <%-- NAR Added Search button Begin --%>             
                     <div class="searchDivider"></div>
                     
                     <div class="searchDetail lastSearchDetail">
                         <span class="searchCriteria">
                           <% if(TradePortalConstants.STANDARD_REPORTS.equals(current2ndNav)){ %>
                             <%=widgetFactory.createSearchTextField("sReportFilterText", "ReportSearch.ReportsName", "50","onKeydown='Javascript: filterStandardOnEnter(\"sReportFilterText\");'") %>
                           <%} else if (TradePortalConstants.CUSTOM_REPORTS.equals(current2ndNav)) { %>
                              <%=widgetFactory.createSearchTextField("cReportFilterText", "ReportSearch.ReportsName", "50","onKeydown='Javascript: filterCustomOnEnter(\"cReportFilterText\");'") %>
                           <% } %>
                        </span>
                        <span class="searchActions">
                              <button data-dojo-type="dijit.form.Button" type="button" id="searchButton" >
                                 <%= resMgr.getText("ReportSearch.Search", TradePortalConstants.TEXT_BUNDLE) %>
                                 <% if(TradePortalConstants.STANDARD_REPORTS.equals(current2ndNav)){ %>
                                      <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">filterStandardReports();</script>
                                  <%} else if (TradePortalConstants.CUSTOM_REPORTS.equals(current2ndNav)) { %>
                                      <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">filterCustomReports();</script>
                                  <% } %>
                               </button>
                         <%=widgetFactory.createHoverHelp("searchButton","SearchHoverText") %>
                       </span>
                      <div style="clear:both"></div>
                   </div>
                   <%-- NARAYAN Added Search button End --%>      
 		     
		            <%	
		            	String where = " ";
		            
		            	if((isStdMaintainRights || isStdViewRights) && (TradePortalConstants.STANDARD_REPORTS.equals(current2ndNav))){   // By Sandeep		           
		            
							Debug.debug("*** Before Including Listview"); 
							Debug.debug("*** <<reportFlag>>: "+TradePortalConstants.REPORTFLAG);
							Debug.debug("*** <<categories>>: "+selectedCategorySQL);
							Debug.debug("*** <<reportType>>: "+reportType);
					
					    	gridHtml = dGridFactory.createDataGrid("standardReportDataGridId","StandardReportDataGrid",null);
					    	gridLayout = dGridFactory.createGridLayout("standardReportDataGridId", "StandardReportDataGrid");
					%>
				  	<%= gridHtml %>
		            <% 
		            	}
		            	if((isCusMaintainRights || isCusViewRights) && (TradePortalConstants.CUSTOM_REPORTS.equals(current2ndNav))) {     // By Sandeep
		            		gridHtml = dGridFactory.createDataGrid("customReportDataGridId","CustomReportDataGrid",null);
		            		gridLayout = dGridFactory.createGridLayout("customReportDataGridId", "CustomReportDataGrid");
					%>
				  	<%= gridHtml %>
		            <% 
		              	} 
		            %>   
		            </div> 
  				</div><%-- end of formContentNoSidebar div --%>
			</form>
		</div><%-- end of pageContent div --%>
	</div><%-- end of pageMainNoSidebar div --%>

	<jsp:include page="/common/Footer.jsp">
	   <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
	   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
	</jsp:include>
<%@ include file="/common/GetSavedSearchQueryData.frag" %>
	<%
		if((isStdMaintainRights || isStdViewRights) && (TradePortalConstants.STANDARD_REPORTS.equals(current2ndNav))){
	%>
		<script type='text/javascript'>
		require(["t360/OnDemandGrid", "dojo/dom-construct","dojo/dom","dijit/registry","dojo/ready"],
	   	        function(onDemandGrid, domConstruct, dom, registry, ready ) {	
		  ready(function() {
			var gridLayout = <%= gridLayout %>;	
			
			var initSearchParms="reportType=<%=reportType%>";
			var savedSort = savedSearchQueryData["sort"];  
		   	if("" == savedSort || null == savedSort || "undefined" == savedSort){
		        savedSort = '0';
		     }
		   	var categories = savedSearchQueryData["categories"]; 
		    var selectedCategorySQL = '<%=StringFunction.escapeQuotesforJS(selectedCategorySQL)%>';
		    var reportingPageLabelLan = '<%=reportingPageLabelLan%>';
           	if("" == categories || null == categories || "undefined" == categories)
           		categories=dom.byId("Category").value;
      		 else{
      			 if (selectedCategorySQL == categories){
      				 if (reportingPageLabelLan.indexOf("fr") == -1){
      					registry.byId("Category").set("value", '<%=RepUtility.ALL_CATEGORIES%>');
      				 }else{
      					registry.byId("Category").set("value", '<%=RepUtility.ALL_CATEGORIES_FR%>');
      				 }
      				 
      			 }else{
      				registry.byId("Category").value = categories;
      			 }
      		 }

           	if(categories=="All Categories"){
           		initSearchParms+="&categories=<%=StringFunction.escapeQuotesforJS(selectedCategorySQL)%>";
	    	}
           	else{
           		initSearchParms+="&categories="+categories+"&selectedCategory="+categories;
	    	}
           	
        	var filterSReportname = savedSearchQueryData["filterSReportname"];
           	if("" == filterSReportname || null == filterSReportname || "undefined" == filterSReportname) {
           		if ( dom.byId("sReportFilterText") ) {
           			filterSReportname = dom.byId("sReportFilterText").value;
           		}else {
           			filterSReportname="";
           		}
           	} else {
           		if ( registry.byId("sReportFilterText") ) {
           			registry.byId("sReportFilterText").set('value',filterSReportname);
           		} else {
           			filterSReportname ="";
           		}          		
           	}          	
           	initSearchParms+="&filterSReportname="+filterSReportname;
		   				
			console.log("initSearchParms="+initSearchParms);
			var viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("ReportsDataView",userSession.getSecretKey())%>";  <%--Rel9.2 IR T36000032596 --%>
			onDemandGrid.createOnDemandGrid("standardReportDataGridId", viewName,gridLayout, initSearchParms,savedSort);
		    });
		    });
			function filterStandardReports(){
				console.log("inside filterStandardReports()");
				
				require(["dijit/registry", "t360/OnDemandGrid", "dojo/dom-construct"],function(registry, onDemandGrid,domConstruct){
			    	var categories = registry.byId("Category").value;
			    	
			    	var searchParms="reportType=<%=reportType%>";
			    	
			    	if(categories=="All Categories"){
			    		searchParms+="&categories=<%=StringFunction.escapeQuotesforJS(selectedCategorySQL)%>";
			    	}
			    	else{
			    		searchParms+="&categories="+categories+"&selectedCategory="+categories;
			    	}
			    	var filterSReportname = registry.byId("sReportFilterText").value;
			    	searchParms+="&filterSReportname="+filterSReportname;
			        console.log("searchParms="+searchParms);
			        onDemandGrid.searchDataGrid("standardReportDataGridId", "ReportsDataView", searchParms);
				});
			}
		</script> 
    <%--cquinton 10/8/2012 include gridShowCounts --%>
    <%-- <jsp:include page="/common/gridShowCountFooter.jsp"> --%>
    <jsp:include page="/common/dGridShowCountFooter.jsp">
      <jsp:param name="gridId" value="standardReportDataGridId" />
    </jsp:include>
    <%
    	}
	
		if((isCusMaintainRights || isCusViewRights) && (TradePortalConstants.CUSTOM_REPORTS.equals(current2ndNav))) {
    %>
    	<script type='text/javascript'>
    	require(["t360/OnDemandGrid", "dojo/dom-construct","dojo/dom","dijit/registry","dojo/ready"],
	   	        function(onDemandGrid, domConstruct, dom, registry, ready ) {
    	 ready(function() {
    		var gridLayout = <%= gridLayout %>;	
    		
    		var initSearchParms="reportType=<%=reportType%>";

    		var filterCReportname = savedSearchQueryData["filterCReportname"];
           	if("" == filterCReportname || null == filterCReportname || "undefined" == filterCReportname) {
           		if ( dom.byId("cReportFilterText") ) {
           			filterCReportname = dom.byId("cReportFilterText").value;
           		}else {
           			filterCReportname="";
           		}
           	} else {
           		if ( registry.byId("cReportFilterText") ) {
           			registry.byId("cReportFilterText").set('value',filterCReportname);
           		} else {
           			filterCReportname ="";
           		}          		
           	}          	
           	initSearchParms+="&filterCReportname="+filterCReportname;
		   				    		
    		console.log("initSearchParms="+initSearchParms);
    		
    		var savedSort = savedSearchQueryData["sort"];  
		   	if("" == savedSort || null == savedSort || "undefined" == savedSort){
		        savedSort = '0';
		     }
		   	
    		var viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("ReportsDataView",userSession.getSecretKey())%>";  <%--Rel9.2 IR T36000032596 --%>
    		onDemandGrid.createOnDemandGrid("customReportDataGridId", viewName,gridLayout, initSearchParms,savedSort);
    	});
    	});
    		function filterCustomReports(){
				console.log("inside filterStandardReports()");
				
				require(["t360/OnDemandGrid", "dojo/dom-construct","dijit/registry"],function(onDemandGrid, domConstruct,registry){
			    	
			    	var searchParms="reportType=<%=reportType%>";			    	
			    	var filterCReportname = registry.byId("cReportFilterText").value;
			    	searchParms+="&filterCReportname="+filterCReportname;
					onDemandGrid.searchDataGrid("customReportDataGridId", "ReportsDataView", searchParms);
				});
			}
    	
    	</script>    
    <%--cquinton 10/8/2012 include gridShowCounts --%>
    <%-- <jsp:include page="/common/gridShowCountFooter.jsp"> --%>
    <jsp:include page="/common/dGridShowCountFooter.jsp">
      <jsp:param name="gridId" value="customReportDataGridId" />
    </jsp:include>
    <%
   		}
    %>
 
</body>
</html>
	<script type="text/javascript">
		 function openURL(URL){
			    if (isActive =='Y') {
			    	if (URL != '' && URL.indexOf("javascript:") == -1) {    
			    		var cTime = (new Date()).getTime();	
				        URL = URL + "&cTime=" + cTime;
				        URL = URL + "&prevPage=" + context;
			    	}
			    }
			 document.location.href  = URL;
			 return false;
		 }
		 
		 function filterStandardOnEnter(fieldID){
				require(["dojo/on","dijit/registry"],function(on, registry) {
				    on(registry.byId(fieldID), "keypress", function(event) {
				        if (event && event.keyCode == 13) {
				        	dojo.stopEvent(event);
				        	filterStandardReports();
				        }
				    });
				});	
		 }
			
		 function filterCustomOnEnter(fieldID){
				require(["dojo/on","dijit/registry"],function(on, registry) {
				    on(registry.byId(fieldID), "keypress", function(event) {
				        if (event && event.keyCode == 13) {
				        	dojo.stopEvent(event);
				        	filterCustomReports();
				        }
				    });
				});	
		 }
	</script>
	
	<%
	   	session.setAttribute("reportTab", reportTab);
		session.setAttribute("report2ndNav", current2ndNav);
		//Rel 9.5 IR T36000047671
		session.setAttribute("selectedCategory", selectedCategory);

   		Debug.debug("*** <<END of REPORTS HOME>>");

		// Finally, reset the cached document to eliminate carryover of
		// information to the next visit of this page.
		formMgr.storeInDocCache("default.doc", new DocumentHandler());
	%>
