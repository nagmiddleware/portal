<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*, com.amsinc.ecsg.util.*, com.ams.tradeportal.common.cache.*,
                 com.amsinc.ecsg.html.*, com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*, com.ams.tradeportal.html.*,
                 com.ams.tradeportal.busobj.webbean.*,com.businessobjects.rebean.wi.*,
		         com.crystaldecisions.sdk.framework.*,com.crystaldecisions.sdk.occa.infostore.*,java.util.*,com.ams.tradeportal.busobj.util.SecurityAccess" %>



<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" 
   scope="session">
</jsp:useBean>
<%@ include file="fragments/reportVariables.frag" %>
<%
Debug.debug("***START********************STANDARD*REPORTS*PAGE**************************START***");
WidgetFactory widgetFactory = new WidgetFactory(resMgr);
StringBuffer      dropdownOptions              = new StringBuffer();
String options = ""; 
StringBuffer        widBuffer               = null;
String              oID                     = null;
String              physicalPage            = null;
String              userName                = null;
String              cell                    = null;
int                 intOID                  = 0;
int                 iRepoType               = 0;
boolean             gooddoc                 = false;
MediatorServices    mediatorServices        = null;
DocumentHandler     xmlDoc                  = null;
 	
//XIR2 changes
DocumentInstance	widDoc               	= null;
Prompts 			widPrompts              = null;
Reports 			widReports             	= null;
PaginationMode 		pgMode 		 		 	= null;
Report 				widReport 				= null;
HTMLView			repView 				= null;
PageNavigation 		pgn 					= null;
int 				totalNoPages 			= 0;
int 				prevPage 				= 0;
int 				nextPage 				= 0;
int 				goToPage 				= 0;
String 				pageMode 				= null;
String 				pageCall 				= null; 
String 				pageNum 				= null;
int 				iPrompts				= 0;
String 				repOwn 					= null;
IInfoObjects 		oInfoObjects			= null;
IInfoObject 		oInfoObject				= null;
String 				query					= null;
Object 				author 					= null;
PropertyResourceBundle portalProperties = null;
portalProperties = (PropertyResourceBundle) PropertyResourceBundle.getBundle("TradePortal");


//cquinton 2/14/2013 add a variable to see if on entry page is expanded or not
String expandedPage=request.getParameter("expandedPage");
if ( expandedPage == null ) {
  expandedPage = "N";
}

pageMode = portalProperties.getString("pageMode");
String securityRights = userSession.getSecurityRights();

if( pageMode == null)
{
    pageMode = "n";

}

if (request.getParameter("sToken")!=null)
{
	sToken = request.getParameter("sToken");
	
}

if(pageMode.equalsIgnoreCase(TradePortalConstants.INDICATOR_YES))
{
	//The pageCall variable determines if the request is coming from
	//the same page of a different page
	if (request.getParameter("pageCall")!=null)
	{

		pageCall = request.getParameter("pageCall");
	}
	else
	{
		pageCall ="N";
	}

	if (request.getParameter("goToPage")!=null) 
	{
		pageNum = request.getParameter("goToPage");
		goToPage = (new Integer(pageNum).intValue());
	}
	else 
	{
  	 	goToPage = 1;
	}
}
else
{
	pageCall ="N";
}

if(request.getParameter("Name")!=null)
{
	//repName = EncryptDecrypt.decryptStringUsingTripleDes(request.getParameter("Name"), userSession.getSecretKey());
	repName = request.getParameter("Name");
}

if(request.getParameter("oid")!=null)
{
	//oID = EncryptDecrypt.decryptStringUsingTripleDes(request.getParameter("oid"), userSession.getSecretKey());
	oID = request.getParameter("oid");
	intOID = (new Integer(oID).intValue());
}

if(request.getParameter("ReportType")!=null)
{
	//reportType = EncryptDecrypt.decryptStringUsingTripleDes(request.getParameter("ReportType"), userSession.getSecretKey());
	reportType = request.getParameter("ReportType");
	iRepoType = (new Integer(reportType).intValue());
}   

try
{
	query = "Select * From CI_INFOOBJECTS Where SI_ID = "+SQLParamFilter.filterNumber(oID);
	IEnterpriseSession boSessionLocal = (IEnterpriseSession) request.getSession().getAttribute("CE_ENTERPRISESESSION");
	IInfoStore iStore = (IInfoStore)boSessionLocal.getService("","InfoStore");

    oInfoObjects =  (IInfoObjects)iStore.query( query );

	oInfoObject = (IInfoObject)oInfoObjects.get(0);
		
    author = oInfoObject.properties().getProperty("SI_OWNER").getValue();
			
	repOwn = author.toString();
		
}                
catch(SDKException e)
{
		//Catch all exception
     System.out.println("EXCEPTION in Standard Reports Page ");
                        
     xmlDoc = formMgr.getFromDocCache();
     mediatorServices = new MediatorServices();
     mediatorServices.getErrorManager().setLocaleName(resMgr.getResourceLocale());
     mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.WEBI_SESSION_ERROR);
     mediatorServices.addErrorInfo();
     xmlDoc.setComponent("/Error",         mediatorServices.getErrorDoc());
     xmlDoc.setAttribute("/In/HasErrors",  TradePortalConstants.INDICATOR_YES);  
     formMgr.storeInDocCache("default.doc", xmlDoc);
     physicalPage = NavigationManager.getNavMan().getPhysicalPage("ReportsErrors", request);
         
     %>
     <jsp:forward page='<%= physicalPage %>' />
     <%
}%>
<%@ include file="fragments/wistartpage.frag" %>
<%
try
{
	if (reportEngine.isReady())
    {    
		if (pageCall.equals("Y"))
		{
		      //If the request is coming from the current page then open the
		      //page from the storage token
			  //modified by Kajal Bhatt on 25/06/07
		      widDoc = reportEngine.getDocumentFromStorageToken(sToken);
		}
		else
		{
			//Open the report using the report details for the first time
			widDoc = reportEngine.openDocument(intOID);

			//This call will refresh the report widDoc.refresh();
			widDoc.refresh();
		} 
		//get reports name
		repName = widDoc.getProperties().getProperty(PropertiesType.NAME);
		sToken = widDoc.getStorageToken();
		widReports = widDoc.getReports();
        
    	widReport = widReports.getItem(0);
        	
        //Set the page navigation mode to page  
		widReport.setPaginationMode(PaginationMode.QuickDisplay);
		pgn = widReport.getPageNavigation();

	    //Set the page navigation to last page for computing 
		//the total number of pages	
        pgn.last();
		totalNoPages = pgn.getCurrent();
		
		if(pageMode.equalsIgnoreCase(TradePortalConstants.INDICATOR_YES))
		{
			//Set the page number
			pgn.setTo(goToPage);

			//modified by KajalBhatt on 25/06/07
			repView = (HTMLView) widReport.getView(OutputFormatType.HTML);
		}
    }
    else
    {
		System.out.println("XIR2 Reports Engine is not ready ######################");
		
		//Catch all exception
		xmlDoc = formMgr.getFromDocCache();
		mediatorServices = new MediatorServices();
		mediatorServices.getErrorManager().setLocaleName(resMgr.getResourceLocale());
		mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.WEBI_SESSION_ERROR);
		mediatorServices.addErrorInfo();
		xmlDoc.setComponent("/Error", mediatorServices.getErrorDoc());
		xmlDoc.setAttribute("/In/HasErrors",TradePortalConstants.INDICATOR_YES);
		formMgr.storeInDocCache("default.doc", xmlDoc);
		physicalPage = NavigationManager.getNavMan().getPhysicalPage("ReportsErrors", request);
		%>
		<jsp:forward page='<%= physicalPage %>' />
		<%
    }
}
catch (REException rex)
{
     //Catch all exception
     System.out.println("EXCEPTION in Standard Reports Page "+ repName +"--"+ rex.getCode()+" "+rex.getMessage());
                        
     xmlDoc = formMgr.getFromDocCache();
     mediatorServices = new MediatorServices();
     mediatorServices.getErrorManager().setLocaleName(resMgr.getResourceLocale());
     mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.WEBI_SESSION_ERROR);
     mediatorServices.addErrorInfo();
     xmlDoc.setComponent("/Error",         mediatorServices.getErrorDoc());
     xmlDoc.setAttribute("/In/HasErrors",  TradePortalConstants.INDICATOR_YES);  
     formMgr.storeInDocCache("default.doc", xmlDoc);
     physicalPage = NavigationManager.getNavMan().getPhysicalPage("ReportsErrors", request);
         
     %>
     <jsp:forward page='<%= physicalPage %>' />
     <%
}
reportAuthor = widDoc.getProperties().getProperty(PropertiesType.AUTHOR);

// Place the reporting user suffix on to the end of the OID
// The suffix is used to distinguish between organizations being processed
// by different ASPs that might be on the same reports server.  

userName = userSession.getOwnerOrgOid()+userSession.getReportingUserSuffix();
 
try
{
	link = new StringBuffer();
	
	//Retrieve the report prompts
	widPrompts = widDoc.getPrompts();
	iPrompts = widPrompts.getCount();
       
	//If the prompt count is greater than zero then forward the request to the prompts page
	if (iPrompts > 0)
	{
		link = new StringBuffer();
		
		//Navigation logic to prompt page
		link.append(NavigationManager.getNavMan().getPhysicalPage("StandardReportPrompt1", request));
		link.append("?storageToken=");
		link.append(sToken);
		link.append("&repOwn=" + repOwn);	
		session.setAttribute("repName",repName);
		%>
		<jsp:forward page='<%=link.toString()%>' />
		<%
	}
}catch (REException rex)
{
	System.out.println( "Excepton in Standard Reports Page"+rex.getMessage());    
	    
	physicalPage = NavigationManager.getNavMan().getPhysicalPage("ReportsErrors", request);  
	%>
		<jsp:forward page='<%= physicalPage %>' />
	<%
}

//jgadela R 8.4 CR-854 T36000024797   -[START] Reporting page labels. 
String reportingPageLabelLan = null;
String cbReportingLangOptionInd = null;

Cache CBCache = (Cache)TPCacheManager.getInstance().getCache(TradePortalConstants.CLIENT_BANK_CACHE);
DocumentHandler CBCResult = (DocumentHandler)CBCache.get(userSession.getClientBankOid());
if (!StringFunction.isBlank(userSession.getClientBankOid())){
	   cbReportingLangOptionInd = CBCResult.getAttribute("/ResultSetRecord(0)/REPORTING_LANG_OPTION_IND"); // jgadela Rel 8.4 CR-854 
}

if(TradePortalConstants.INDICATOR_YES.equals(cbReportingLangOptionInd)){
	reportingPageLabelLan = userSession.getReportingLanguage();
}else{
	reportingPageLabelLan = userSession.getUserLocale();
}
//jgadela R 8.4 CR-854 T36000024797   -[END]
%>
<%--*******************************HTML for page begins here **************--%>


<jsp:include page="/common/Header.jsp">
  <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
  <jsp:param name="includeErrorSectionFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
  <jsp:param name="expandedPage" value="<%=expandedPage%>" />
</jsp:include>
	
<div class="pageMain">
	<div class="pageContent">
	<%  String pageTitleKey;%>
	<% String current2ndNav ="S"; %>
	<%String helpLink = OnlineHelp.createContextSensitiveLink("customer/report.htm",resMgr,userSession); %>
	<%@ include file="/reports/fragments/Reports-SecondaryNavigation.frag" %>
	<input type=hidden value='<%=current2ndNav%>' name="current2ndNav" id="current2ndNav">
<%		
	String selectedCategory = request.getParameter("selectedCategory");
	//Rel 9.5 IR T36000047671
	if(StringFunction.isBlank(selectedCategory) || "null".equals(selectedCategory)){
		selectedCategory = (String)session.getAttribute("selectedCategory");
	}
	// IR T36000048590 Rel 9.5
	if ("N".equals(pageCall) && ((StringFunction.isBlank(selectedCategory))))
	selectedCategory = "null".equals(selectedCategory) ? "" : EncryptDecrypt.base64StringToString(selectedCategory);
    pageTitleKey = repName;
	// figures out how to return to the calling page
	StringBuffer parms = new StringBuffer();
	parms.append("&returnAction=");
	parms.append(EncryptDecrypt.encryptStringUsingTripleDes("goToReportsHome", userSession.getSecretKey()));
	String returnLink = formMgr.getLinkAsUrl("goToReportsHome", "&reportTab=S&current2ndNav=S&selectedCategory="+EncryptDecrypt.stringToBase64String(selectedCategory), response);
%>
      <%
      showDeleteButton = false;
      showEditButton = false;
      showSaveAsButton = false; // Nar IR-T36000014687
    // User has maintain rights then only 'Delete' and 'Edit Report Design' Button should be display
    // DK IR T36000026070 Rel8.4 - display Save As button if user has view rights
    if (SecurityAccess.hasRights(securityRights, SecurityAccess.VIEW_OR_MAINTAIN_STANDARD_REPORT)){
  	  showSaveAsButton = true;
    }
     if (SecurityAccess.hasRights(securityRights, SecurityAccess.MAINTAIN_STANDARD_REPORT)){	
    	  // Nar IR-T36000014687 show Save as Standard Reports button if user has maintain rights
          //As per above comment show Save as should be true under maintain rights. so making it as true 
          saveAsNavPage = formMgr.getLinkAsUrl("goToEditReportStep3", response);
		  if (userName.equals(repOwn))
	      {    
			//Edit reports design method
			 editReportMode = TradePortalConstants.REPORT_EDIT_STANDARD; 
			 editNavPage = formMgr.getLinkAsUrl("goToNewReportStep1", response); 
			 reportTab = TradePortalConstants.STANDARD_REPORTS;
		     deleteNavPage = formMgr.getLinkAsUrl("goToReportsDelete", response);
		     showDeleteButton = true;
		     showEditButton = true;
	       }
     } 
     
    /* if (iPrompts > 0)
 	 {
    	 showDeleteButton = false;
	     showEditButton = false;
	     showSaveAsButton = false;
 	 }*/
     
%>
  
<%--cquinton 2/14/2013 do not display divider if already expanded--%>
<div id="divider1" class="subHeaderDivider"<%=("Y".equals(expandedPage))?" style=\"display:none;\"":""%>></div>
 
<%--cquinton 2/14/2013 add expandNode - everthing within this expands to the page size--%>
<div id="expandNode"<%=("Y".equals(expandedPage))?" class=\"expandedPage\"":""%> >
   
  <jsp:include page="/common/ReportsSubHeaderBar.jsp">
	<jsp:param name="returnLink" value="<%=returnLink%>" />
	<jsp:param name="pageTitleKey" value="<%=pageTitleKey%>" />
	<jsp:param name="showExpandCollapse" value="true"/>
	<jsp:param name="showEditButton" value="<%=showEditButton%>"/>
	<jsp:param name="showSaveAsButton" value="<%=showSaveAsButton%>"/>
	<jsp:param name="showDeleteButton" value="<%=showDeleteButton%>"/>	
	<jsp:param name="sToken" value="<%=sToken%>"/>
	<jsp:param name="repName" value="<%=repName%>"/>
	<jsp:param name="reportTab" value="<%=reportTab%>"/>
	<jsp:param name="editReportMode" value="<%=editReportMode%>"/>
	<jsp:param name="editNavPage" value="<%=editNavPage%>"/> 
	<jsp:param name="saveAsNavPage" value="<%=saveAsNavPage%>"/> 
	<jsp:param name="deleteNavPage" value="<%=deleteNavPage%>"/>
	<jsp:param name="reportType"   value="<%=TradePortalConstants.STANDARD_REPORTS%>" />
	<jsp:param name="current2ndNav"   value="<%=current2ndNav%>" />
	<jsp:param name="expandedPage"   value="<%=expandedPage%>" />
  </jsp:include>

  <form id="standardReportsPage" name="standardReportsPage" method="post" data-dojo-type="dijit.form.Form" action="<%=formMgr.getSubmitAction(response)%>">
    <input type=hidden value="" name="buttonPressed" id="buttonPressed"/>			

    <%--cquinton 2/14/2013 what does error section do here?--%>
    <jsp:include page="/common/ErrorSection.jsp" />

    <%--cquinton 2/14/2013 move ReportsTypeSubHeaderBar.jsp to customReportHTML.frag--%>			
    <div id="standardReportsPDFId">
      <%@ include file="fragments/StandardReportHTML.frag" %>
    </div>
			
    <%= formMgr.getFormInstanceAsInputField("saveReport") %>
			
  </form>

</div>


  </div>
</div>
 <form name="saveReport" method="POST" action="<%= formMgr.getSubmitAction(response) %>">
<input type=hidden value="" name=buttonName value="<%=TradePortalConstants.STANDARD_REPORTS%>" />
<input type="hidden" name="ReportName" />
<input type="hidden" name="ReportDesc" />
<input type="hidden" name="StandardReport"  id="StandardReport" />
<input type="hidden" name="StandardCategory" />
<input type="hidden" name="CustomCategory" />
<input type="hidden" name="storageToken" value="<%= sToken %>" />
<input type="hidden" name="reportMode" value="<%=TradePortalConstants.REPORT_COPY_STANDARD%>" />
<input type=hidden value='<%=current2ndNav%>' name="current2ndNav" id="current2ndNav1">
<input type="hidden" name="selectedCategory" /> 
<%= formMgr.getFormInstanceAsInputField("saveReport") %>
</form>

<jsp:include page="/common/Footer.jsp">
	<jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
	<jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>

<%--cquinton 2/14/2013 add expand/collapse functionality--%>
<script>
  var expandedPage = "<%=StringFunction.escapeQuotesforJS(expandedPage)%>";

  var local = {};

  require(["dojo/dom", "dojo/dom-style", "dojo/dom-class", "dojo/query", "dijit/registry"],
      function( dom, domStyle, domClass, query, registry ) {

    local.expandPage = function() {
      var expandNode = dom.byId("expandNode");
      if ( expandNode ) {
        <%-- hide everything else --%>
        query(".pageHeader").forEach( function(entry, i) {
          domStyle.set(entry, "display", "none");
        });
        query(".pageHeaderNavigation").forEach( function(entry, i) {
          domStyle.set(entry, "display", "none");
        });
        query(".secondaryNav").forEach( function(entry, i) {
          domStyle.set(entry, "display", "none");
        });
        query("#divider1").forEach( function(entry, i) {
          domStyle.set(entry, "display", "none");
        });
        query(".pageFooter").forEach( function(entry, i) {
          domStyle.set(entry, "display", "none");
        });
        query("#reportSection").forEach( function(entry, i) {
          domStyle.set(entry, "width", "auto");
          domStyle.set(entry, "height", "auto");
          domStyle.set(entry, "overflow", "visible");
        });
        domClass.add(expandNode, "expandedPage");

        var expandButton = registry.byId("ExpandButton");
        var collapseButton = registry.byId("CollapseButton");
        if (expandButton && collapseButton) {
          domStyle.set(expandButton.domNode, "display", "none");
          domStyle.set(collapseButton.domNode, "display", "inline-block");
        }

        expandedPage = "Y";
        local.setPageLinkExpandedPageParmVal( "firstPageLink", "Y" );
        local.setPageLinkExpandedPageParmVal( "prefPageLink", "Y" );
        local.setPageLinkExpandedPageParmVal( "nextPageLink", "Y" );
        local.setPageLinkExpandedPageParmVal( "lastPageLink", "Y" );
      }
    }

    local.collapsePage = function() {
      var expandNode = dom.byId("expandNode");
      if ( expandNode ) {
        <%-- and remove the expand class --%>
        domClass.remove(expandNode, "expandedPage");
        query("#reportSection").forEach( function(entry, i) {
          domStyle.set(entry, "width", "100%");
          domStyle.set(entry, "height", "375px");
          domStyle.set(entry, "overflow", "auto");
        });
        <%-- show everything that was hidden above --%>
        query(".pageHeader").forEach( function(entry, i) {
          domStyle.set(entry, "display", "block");
        });
        query(".pageHeaderNavigation").forEach( function(entry, i) {
          domStyle.set(entry, "display", "block");
        });
        query(".secondaryNav").forEach( function(entry, i) {
          domStyle.set(entry, "display", "block");
        });
        query("#divider1").forEach( function(entry, i) {
          domStyle.set(entry, "display", "block");
        });
        query(".pageFooter").forEach( function(entry, i) {
          domStyle.set(entry, "display", "block");
        });

        var expandButton = registry.byId("ExpandButton");
        var collapseButton = registry.byId("CollapseButton");
        if (expandButton && collapseButton) {
          domStyle.set(collapseButton.domNode, "display", "none");
          domStyle.set(expandButton.domNode, "display", "inline-block");
        }

        expandedPage = "N";
        local.setPageLinkExpandedPageParmVal( "firstPageLink", "N" );
        local.setPageLinkExpandedPageParmVal( "prefPageLink", "N" );
        local.setPageLinkExpandedPageParmVal( "nextPageLink", "N" );
        local.setPageLinkExpandedPageParmVal( "lastPageLink", "N" );
      }
    },

    <%-- this assumes expandedPage parm exists and is the last in the href url --%>
    local.setPageLinkExpandedPageParmVal = function( pageLinkId, parmVal ) {
      var pageLink = dom.byId(pageLinkId);
      if ( pageLink ) {
        var myHref = pageLink.href;
        var parmIdx = myHref.indexOf("expandedPage=");
        if ( parmIdx >= 0 ) {
          parmIdx = parmIdx + 13;
          myHref = myHref.substring(0, parmIdx) + parmVal;
        }
        pageLink.href = myHref;
      }
    }

  });

</script>

</body>
</html>

<%
//Garbage Collection
session.setAttribute("test",null); 

widDoc 		= null;
widPrompts 	= null;
widReports 	= null;
pgMode 		= null;
widReport 	= null;
repView 	= null;
pgn 		= null;
%>

<%
Debug.debug("***END********************STANDARD*REPORTS*PAGE**************************END***");
%>
