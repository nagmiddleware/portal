<%--
*******************************************************************************
  Transactions Inquiries Footer

  Description: Everything that should be loaded at the end of the page
               should be included here.  This includes non-visible html
               and javascript that should execute on page load.

*******************************************************************************
--%>
<%--
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
--%>

<%
  dgFactory = new DataGridFactory(resMgr, userSession, formMgr, response);
  //gridLayout is not used for now, its hardcoded below
%>

<script type="text/javascript">
  var local = {
    searchType: "S" <%-- i.e. basic --%>
  };
	var custNotIntgWithTPS = false;
	<% if(userSession.isCustNotIntgTPS()){ %>
     		custNotIntgWithTPS = true;
     <% } %>
  var selectedOrg="";
  var instrType; <%-- used by copy --%>
  var userOrgOid = '<%=EncryptDecrypt.encryptStringUsingTripleDes(userOrgOid, userSession.getSecretKey())%>';
  
  var quickViewFormatter=function(columnValues, idx, level) {
    var gridLink="";

    if(level == 0){
      if ( columnValues.length >= 2 ) {
        gridLink = "<a href='" + columnValues[1] + "'>" + columnValues[0] + "</a>";
      }
      else if ( columnValues.length == 1 ) {
        <%-- no url parameters, just return the display --%>
        gridLink = columnValues[0];
      }
      return gridLink;

    }
    else if(level ==1){
      console.log(columnValues[3]);
      if(columnValues[3]=='PROCESSED_BY_BANK'){
        if(columnValues[2]){
          return "<a href='javascript:dialogQuickView("+columnValues[2]+");'>Quick View</a> "+columnValues[0]+" ";
        }
        else{
          return columnValues[0];
        }
      }
      else{
        return columnValues[0];
      }
    }
  }

  var formatGridLinkChild=function(columnValues, idx, level){
    var gridLink="";

    if(level == 0){
      return columnValues[0];
    } else if(level ==1){
      if ( columnValues.length >= 2 ) {
        gridLink = "<a href='" + columnValues[1] + "'>" + columnValues[0] + "</a>";
       } else if ( columnValues.length == 1 ) {
        <%-- no url parameters, just return the display --%>
        gridLink = columnValues[0];
      }
      return gridLink;
    }
  }

  <%-- problems with lazy tree grid resizing on search  --%>
  <%--  as a temporary fix, recreate the grid on search --%>
  var gridLayout = [
    {name:"<%=resMgr.getText("InstrumentHistory.InstrumentId",TradePortalConstants.TEXT_BUNDLE) %>", field:"InstrumentId", fields:["InstrumentId","InstrumentId_linkUrl","transaction_oid","STATUSFORQVIEW","InstrumentType"], formatter:quickViewFormatter, width:"150px"} ,
    {name:"<%=resMgr.getText("InstrumentHistory.InstrumentType",TradePortalConstants.TEXT_BUNDLE) %>", field:"InstrumentType", fields:["InstrumentType","InstrumentType_linkUrl"], formatter:formatGridLinkChild, width:"150px"},
    {name:"<%=resMgr.getText("BillingHistory.ChargeAmount",TradePortalConstants.TEXT_BUNDLE) %>", field:"ChargeAmount", width:"80px", cellClasses:"gridColumnRight"} ,
    {name:"<%=resMgr.getText("InstrumentHistory.STATUS",TradePortalConstants.TEXT_BUNDLE) %>", field:"Status", width:"100px"} ,
    {name:"<%=resMgr.getText("InstrumentHistory.Party",TradePortalConstants.TEXT_BUNDLE) %>", field:"Party", width:"150px"},
    {name:"<%=resMgr.getText("InstrumentHistory.ApplicantsRefNo",TradePortalConstants.TEXT_BUNDLE) %>", field:"ApplicantsRefNo", width:"160px"}
  ];

  var instrumentId = "";
  var refNum = "";
  var startDateFrom = "";
  var startDateTo = "";
  var closeDateFrom = "";
  var closeDateTo = "";
  var otherParty = "";
  var init='loginLocale=<%=loginLocale%>';
  var savedSort = savedSearchQueryData["sort"];
   searchType = savedSearchQueryData["searchType"];
   instrStatusType = savedSearchQueryData["selectedStatus"];
   selectedOrg = savedSearchQueryData["selectedOrg"];
  
   var bankInstrumentId = "";
  
   require(["dojo/dom","dojo/query", "dojo/on", "t360/datagrid", "t360/popup", "dojo/ready","dijit/registry", "dojox/html/entities","dojo/domReady!"],
		      function(dom,query, on, t360grid, t360popup, ready, registry, entities){
			  ready(function(){
	  if("" == selectedOrg || null == selectedOrg || "undefined" == selectedOrg){
		  selectedOrg = ( registry.byId('Organization') == null ?"":registry.byId('Organization').attr('value'));	
	  } 
	  else{
		  if(registry.byId('Organization')){
			  registry.byId("Organization").set('value',selectedOrg);
	  }}
	    if("" == selectedOrg || null == selectedOrg){
	      selectedOrg = userOrgOid;
	    }
	    if("" == instrStatusType || null == instrStatusType || "undefined" == instrStatusType){
	    	if(dom.byId("InstrStatusTypeActive").checked && dom.byId("InstrStatusTypeInactive").checked){
	    			instrStatusType="<%=TradePortalConstants.STATUS_ALL%>";
	          }
	          else if(dom.byId("InstrStatusTypeActive").checked){
	        	    instrStatusType="<%=TradePortalConstants.STATUS_ACTIVE%>";
	          }
	          else if(dom.byId("InstrStatusTypeInactive").checked){
	        	    instrStatusType="<%=TradePortalConstants.STATUS_INACTIVE%>"
	          }
	          else{
	        	    instrStatusType="<%=TradePortalConstants.STATUS_ACTIVE%>";
	          }	
  		}else{
	    	if(instrStatusType=="<%=TradePortalConstants.STATUS_ACTIVE%>"){
	    	   registry.byId("InstrStatusTypeActive").set('checked',true);
	    	   registry.byId("InstrStatusTypeInactive").set('checked',false);
	    	}
	    		
	    	if(instrStatusType=="<%=TradePortalConstants.STATUS_INACTIVE%>"){
	    		 registry.byId("InstrStatusTypeInactive").set('checked',true);
	    		 registry.byId("InstrStatusTypeActive").set('checked',false);
	    	}
	    	if(instrStatusType=="<%=TradePortalConstants.STATUS_ALL%>"){
	    		  registry.byId("InstrStatusTypeInactive").set('checked',true);
	    	      registry.byId("InstrStatusTypeActive").set('checked',true);
	    	}
	    }	
	    
	    if("" == savedSort || null == savedSort || "undefined" == savedSort)
	    		  savedSort = '0';
	    
	    if("" == searchType || null == searchType || "undefined" == searchType){
	        	searchType = registry.byId('SearchType');
        }else{
	    	   if('A'==searchType){
	    	   document.getElementById("advancedInstrumentFilter").style.display='block';
	           document.getElementById("basicInstrumentFilter").style.display='none';
	    	   }
	    	   else{
	    		   document.getElementById("advancedInstrumentFilter").style.display='none';
		           document.getElementById("basicInstrumentFilter").style.display='block';
	    	   }
	    	  
	     }
	    
	     if('A'==searchType){
	      		 startDateFrom = savedSearchQueryData["startDateFrom"];
	        	 startDateTo = savedSearchQueryData["startDateTo"];
	    	   	 closeDateFrom = savedSearchQueryData["closeDateFrom"];
	        	 closeDateTo = savedSearchQueryData["closeDateTo"];
	        	 
	        	 if(startDateFrom != undefined) startDateFrom  = startDateFrom.replace(/&#47;/g,'/');
			     if(startDateTo != undefined) startDateTo  = startDateTo.replace(/&#47;/g,'/');
				 if(closeDateFrom != undefined) closeDateFrom  = closeDateFrom.replace(/&#47;/g,'/');
				 if(closeDateTo != undefined) closeDateTo  = closeDateTo.replace(/&#47;/g,'/');
	
	         	if("" == startDateFrom || null == startDateFrom || "undefined" == startDateFrom)
	        		startDateFrom = dom.byId("startDateFrom").value;
	        	else{
	        		registry.byId("startDateFrom").set('displayedValue',startDateFrom);
	        	}
	        	if("" == startDateTo || null == startDateTo || "undefined" == startDateTo)
	        		startDateTo = dom.byId("startDateTo").value;
	        	else{
	        		registry.byId("startDateTo").set('displayedValue',startDateTo);
	        	}
	        	
	        	if("" == closeDateFrom || null == closeDateFrom || "undefined" == closeDateFrom){
	        		 	closeDateFrom = dom.byId("closeDateFrom").value;
	        	}
	        	else{
	        		registry.byId("closeDateFrom").set('displayedValue',closeDateFrom);
	        	}
	        	
	        	if("" == closeDateTo || null == closeDateTo || "undefined" == closeDateTo)
	        		 closeDateTo = dom.byId("closeDateTo").value;
	        	else{
	        		registry.byId("closeDateTo").set('displayedValue',closeDateTo);
	        	}
	        	
	        	 var tempDatePattern = encodeURI('<%=datePattern%>');
       			
	        	init = init+"&closeDateTo="+closeDateTo+"&closeDateFrom="+closeDateFrom+"&startDateTo="+startDateTo+"&startDateFrom="+startDateFrom+"&dPattern="+tempDatePattern;
	        }
	        else{
	        	 refNum = savedSearchQueryData["refNum"];	
	        	 instrumentId = savedSearchQueryData["instrumentId"];
	        	 otherParty = savedSearchQueryData["otherParty"];
	        	 
	        	 if(custNotIntgWithTPS){
       				 bankInstrumentId = savedSearchQueryData["bankInstrumentId"];
       			 }
       			 
	        	if("" == instrumentId || null == instrumentId || "undefined" == instrumentId)
	        		instrumentId = checkString( dijit.byId("InstrumentId") );
	        	else
	        		dom.byId("InstrumentId").value=instrumentId;
	        		
	        	if("" == otherParty || null == otherParty || "undefined" == otherParty)
	        		 otherParty = checkString( dijit.byId("OtherParty") );
	        	else
	        		dom.byId("OtherParty").value=otherParty;
	        	
	        	 if("" == refNum || null == refNum || "undefined" == refNum)
	        		 refNum = checkString( dijit.byId("RefNum") );
	        	 else
	        		 dom.byId("RefNum").value=refNum;
	            
	        	 if(custNotIntgWithTPS){	 
	        		 if("" == bankInstrumentId || null == bankInstrumentId || "undefined" == bankInstrumentId)
	        				bankInstrumentId = checkString( dijit.byId("BankInstrumentId") );
	        			else	 
	        				dom.byId("BankInstrumentId").value=bankInstrumentId;
	             		
	             	init = init+"&instrumentId="+instrumentId+"&refNum="+refNum+"&bankInstrumentId="+bankInstrumentId+"&otherParty="+otherParty;
	             }else{
	             	init = init+"&instrumentId="+instrumentId+"&refNum="+refNum+"&otherParty="+otherParty;
	             }

	        } 
	           
	  var initSearchParms = "selectedOrg="+selectedOrg+"&selectedStatus="+instrStatusType+"&searchType="+searchType+"&searchCondition=<%=searchCondition%>&"+init;
      console.log("initSearchParms--"+initSearchParms);
      var viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("BillingInstrHistoryDataView",userSession.getSecretKey())%>"; 
      <%--create the grid, attaching it to the div id specified in dataGrid.jsp--%>
      local.theGrid =
      t360grid.createLazyTreeDataGrid("billingInstrSearchGridId", viewName, gridLayout, initSearchParms, savedSort);
	  });
    
    
    
    
      local.searchInstruments = function(buttonId) {

      var mySearchType = "";
      if ( buttonId && "Basic" == buttonId ) {
        mySearchType = "S";
      }
      else if ( buttonId && "Advanced" == buttonId ) {
        mySearchType = "A";
      }
      else {
        mySearchType = local.searchType; <%-- use page variable --%>
      }

      var organization="";
      var gridId = registry.byId("billingInstrSearchGridId");
      if (gridId.selection.getSelectedCount() != 0){
        var selectedCount = dom.byId("billingInstrSearchGridId_selCount");
        selectedCount.innerHTML = 0;
      }
      if(registry.byId("Organization") != undefined){
        organization = registry.byId("Organization").attr('value');
      }
      else{
        organization="<%=EncryptDecrypt.encryptStringUsingTripleDes(userOrgOid, userSession.getSecretKey())%>";
      }
      <%-- Ende Here --%>
      var status = "";
	  if(registry.byId("InstrStatusTypeActive").attr('checked') && registry.byId("InstrStatusTypeInactive").attr('checked')){
        status="<%=TradePortalConstants.STATUS_ALL%>";
      }
      else if(registry.byId("InstrStatusTypeActive").attr('checked')){
        status="<%=TradePortalConstants.STATUS_ACTIVE%>";
      }
      else if(registry.byId("InstrStatusTypeInactive").attr('checked')){
        status="<%=TradePortalConstants.STATUS_INACTIVE%>";
      }
      else{
        status="''";
      }		 
      console.log("status-"+status+" organization-"+organization);
    	 
      var searchParms = "selectedOrg="+organization+"&selectedStatus="+status+ "&searchCondition=<%=searchCondition%>";
 		
      var tempDatePattern = encodeURI('<%=datePattern%>');
      searchParms=searchParms+"&dPattern="+tempDatePattern;
 		
      var instrumentId = "";
      var refNum = "";
     
      var startDateFrom = "";
      var startDateTo = "";
      var closeDateFrom = "";
      var closeDateTo = "";
      var otherParty = "";
    
        
      if("A" == mySearchType){       	
     	startDateFrom = dom.byId("startDateFrom").value; 
        startDateTo = dom.byId("startDateTo").value; 
        closeDateFrom = dom.byId("closeDateFrom").value;
        closeDateTo = dom.byId("closeDateTo").value;
        
        searchParms=searchParms+"&startDateFrom="+startDateFrom;
        searchParms=searchParms+"&startDateTo="+startDateTo;
        searchParms=searchParms+"&closeDateFrom="+closeDateFrom;
        searchParms=searchParms+"&closeDateTo="+closeDateTo;
        searchParms=searchParms+"&searchType="+"A";
      }
      else { <%-- basic --%>
        instrumentId = dom.byId("InstrumentId").value;
        refNum = dom.byId("RefNum").value;
        otherParty = dom.byId("OtherParty").value;
        
        searchParms=searchParms+"&instrumentId="+instrumentId;
        searchParms=searchParms+"&refNum="+refNum;
        searchParms=searchParms+"&otherParty="+otherParty;
      
        searchParms=searchParms+"&searchType="+"S";
      }

      if (searchParms != "") {
        var strLen = searchParms.length;
        searchParms = searchParms.slice(0,strLen);
      }
 		console.log("searchParams= "+searchParms);
      var viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("BillingInstrHistoryDataView",userSession.getSecretKey())%>"; 
      <%--  problems with lazy tree grid not resizing on search --%>
      <%--  as a temporary fix, recreate the grid on search --%>
      local.theGrid =
        t360grid.searchLazyTreeDataGrid("billingInstrSearchGridId", viewName, gridLayout, searchParms);
    };

    local.shuffleFilter = function(linkValue){
      var hd = dom.byId("secondaryNavHelp");
      var a = hd.children[0];
      if(linkValue=='Advanced'){
        a.setAttribute('href',"javascript:openHelp('/portal/help/en/customer/inst_hist.htm#advanced')");
        document.getElementById("advancedInstrumentFilter").style.display='block';
        document.getElementById("basicInstrumentFilter").style.display='none';
        local.searchType = "A";

        <%-- clearing Basic Filter --%>
        registry.byId("InstrumentId").value='';
        registry.byId("RefNum").value='';
        registry.byId("OtherParty").value='';
      			
        <%-- Search call,  --%>
        local.searchInstruments("Advanced");
      }
      if(linkValue=='Basic'){
        a.setAttribute('href',"javascript:openHelp('/portal/help/en/customer/inst_hist.htm#basic')");
        document.getElementById("advancedInstrumentFilter").style.display='none';
        document.getElementById("basicInstrumentFilter").style.display='block';
        local.searchType = "S";
    				
        <%-- clearing Advance Filter --%>
        
        registry.byId("CloseDateFrom").set("value",0);
        registry.byId("CloseDateTo").set("value",0);
       			
        <%-- Search call --%>
        local.searchInstruments("Basic");
      }
    };

  });

  <%-- Method will be called when user press enter key on any search field	  --%>
  <%-- Modified the function in order for enter key to work in Firefox --%>  
  function filterOnEnter(evt, fieldID, buttonId){
  	require(["dojo/on","dijit/registry"],function(on, registry) {
	    on(registry.byId(fieldID), "keypress", function(evt) {
      if(evt && evt.keyCode==13){
      local.searchInstruments(buttonId);
    }
  });
 });  
 }<%-- end of filterOnEnter() --%>
 
  function dialogQuickView(rowKey){
	 var dialogDiv = document.createElement("div");
	 dialogDiv.setAttribute('id',"quickviewdialogdivid");
	 document.forms[0].appendChild(dialogDiv);

	 var title="";

	require(["t360/dialog"], function(dialog ) {
		
       dialog.open('quickviewdialogdivid',title ,
                   'TransactionQuickViewDialog.jsp',
                   ['rowKey','DailogId'],[rowKey,'quickviewdialogdivid'], <%-- parameters --%>
                   'select', quickViewCallBack);
     });
  }


  <%-- function to set the title after ajax call of dialog value[2]- dialogId, value[0]-complete instrument id, value[1]-transaction type --%>
  function quickViewCallBack(key,value){
	dijit.byId(value[2]).set('title','Transaction Quick View - '+value[0]+' - '+value[1]);
  }
  function checkString(inquiryStr){
		if(null == inquiryStr)
			return "";
		else
			return inquiryStr.attr('value');
	}

</script>

<%-- include gridShowCounts --%>
<jsp:include page="/common/gridShowCountFooter.jsp">
  <jsp:param name="gridId" value="billingInstrSearchGridId" />
</jsp:include>
