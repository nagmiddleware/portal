<%--
*******************************************************************************
                    Billing Search Basic Filter include file

  Description:
    This is a JSP meant to be included using the <%@ include file="x.jsp" %> 
  tag.  It creates the HTML for displaying the Basic Filter fields for the 
  Instrument Search.  As it is included with <%@ include %> rather than with
  the <jsp:include> directive, it is not a standalone servlet.  

  NOTE: Since this JSP is copied into the including page at compile time of
  the including page, changes to this page do not automatically force the
  including pages to be recompiled.  If you make changes to this page, you must
  delete the including page's servlet in order to see the changes.

*******************************************************************************
--%>
<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>

<div id="basicInstrumentFilter" style="padding-top: 5px;"> 
  <input type="hidden" name="NewSearch" value="Y">

  <div class="searchDetail">
    <span class="searchCriteria">
	
	
	<%-- changed event to window.event to all the fields in order for enter key to work in Firefox --%>
      <%=widgetFactory.createSearchTextField("InstrumentId","InstSearch.InstrumentID","16", " class='char15' onKeydown=' filterOnEnter(window.event, \"InstrumentId\", \"Basic\");'")%>
  	  <%=widgetFactory.createSearchTextField("OtherParty","InstSearch.Party","30", " class='char15' onKeydown='filterOnEnter(window.event, \"OtherParty\",\"Advanced\");'")%>
	  <%=widgetFactory.createSearchTextField("RefNum","InstSearch.ApplRefNumber","30", " width=\"25\" onKeydown='filterOnEnter(window.event, \"RefNum\", \"Basic\");'")%>
    </span>
   
    <span class="searchActions">
      <button id="basicSearchId" class="gridSearchButton" data-dojo-type="dijit.form.Button" type="button" >
       <%= resMgr.getText("common.searchButton",TradePortalConstants.TEXT_BUNDLE) %>
        <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
          local.searchInstruments("Basic");return false;
        </script>
      </button>
      <a id="advanceSearchType" class="searchTypeToggle" href="javascript:local.shuffleFilter('Advanced');"><%=resMgr.getText("ExistingDirectDebitSearch.Advanced", TradePortalConstants.TEXT_BUNDLE)%></a>      
    </span>
     <%=widgetFactory.createHoverHelp("advanceSearchType","AdvancedSearchHypertextLinkHoverText") %>
    <%=widgetFactory.createHoverHelp("basicSearchId","SearchHoverText") %>
    <div style="clear:both"></div>
  </div>
<div>
&nbsp;
</div>  
   
 </div>