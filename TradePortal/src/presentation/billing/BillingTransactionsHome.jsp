<%--
**********************************************************************************
  Billing Transactions Home

  Description:  
     This page is used as the main Transactions page for trade instruments.

**********************************************************************************
--%> 

<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>

<%@ page import="com.amsinc.ecsg.frame.*, com.amsinc.ecsg.util.*,java.util.Map,
                 com.amsinc.ecsg.html.*, com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*, com.ams.tradeportal.html.*,
                 com.ams.tradeportal.busobj.webbean.*, com.ams.tradeportal.busobj.util.*, com.ams.tradeportal.busobj.*,java.util.*" %>

<jsp:useBean id="formMgr"     class="com.amsinc.ecsg.web.FormManager"            scope="session"></jsp:useBean>
<jsp:useBean id="resMgr"      class="com.amsinc.ecsg.util.ResourceManager"       scope="session"></jsp:useBean>
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>
 
<%
  WidgetFactory widgetFactory = new WidgetFactory(resMgr);
  //cquinton 2/12/2013 add refdata manager for datepattern lookup
  ReferenceDataManager refData = ReferenceDataManager.getRefDataMgr();

  final String ALL_WORK = "common.allWork";
  final String MY_WORK = "common.myWork";
   StringBuffer      dropdownOptions              = new StringBuffer();
   String            current2ndNav                = null;
   DocumentHandler   hierarchyDoc                 = null;
   StringBuffer      onLoad                       = new StringBuffer();
   Hashtable         secureParms                  = new Hashtable();
   String 			 userOwnerShipLevel			  = null;
   String            helpSensitiveLink            = null;
   String            selectedWorkflow             = null;
   String            selectedStatus               = "";
   String            selectedOrg                  = null;
   String            searchType                   = null;
   String            userOrgOid                   = null;
   String            userOid                      = null;
   String            userSecurityRights           = userSession.getSecurityRights();
    
   String            formName                     = "TransactionListForm";
   int               totalOrganizations           = 0;
   DataGridFactory dgFactory = new DataGridFactory(resMgr, userSession, formMgr, response);
   String gridHtml = "";

   // Create a link to the basic or advanced instrument search
   String linkParms   = null;
   String linkText    = null;
   String link        = null;
   String loginLocale = userSession.getUserLocale();
   // These variables are used for Instrument History for the 
   // Advanced and Basic Search logic
   String       options;
   
   StringBuffer orgList = new StringBuffer();

   String instrumentId = "";
   String refNum = "";
   String instrumentType = "";
   String otherParty = "";
   String dayFrom = "";
   String monthFrom = "";
   String yearFrom = "";
   String dayTo = "";
   String monthTo = "";
   String yearTo = "";
   String searchCondition 	= TradePortalConstants.SEARCH_ALL_INSTRUMENTS;  
   
   userSession.setCurrentPrimaryNavigation("NavigationBar.Transactions");

   //cquinton 1/18/2013 set return page
   if ( "true".equals( request.getParameter("returning") ) ) {
     userSession.pageBack(); //to keep it correct
   }
   else {
     userSession.addPage("goToBillingTransactionsHome");
   }
   session.setAttribute("startHomePage", "billingTransactionHome");
   session.removeAttribute("fromTPHomePage");
  
   Map searchQueryMap =  userSession.getSavedSearchQueryData();
   String sort ="";
   String instrStatusType ="";
   Map savedSearchQueryData =null;
    
   userOrgOid         = userSession.getOwnerOrgOid();
   userOid            = userSession.getUserOid();
   String datePattern = userSession.getDatePattern();
  String datePatternDisplay = refData.getDescr("DATEPATTERN", datePattern, loginLocale);
  String dateWidgetOptions = "placeHolder:'" + datePatternDisplay + "'";
   
   current2ndNav = request.getParameter("current2ndNav");
   if (current2ndNav == null)
   {
      current2ndNav = (String) session.getAttribute("billingTransactions2ndNav");
   }

   // This is the query used for populating the workflow drop down list; it retrieves
   // all active child organizations that belong to the user's current org.
   StringBuffer sqlQuery = new StringBuffer();
   sqlQuery.append("select organization_oid, name");
   sqlQuery.append(" from corporate_org");
   sqlQuery.append(" where activation_status = ? start with organization_oid = ? ");
   sqlQuery.append(" connect by prior organization_oid = p_parent_corp_org_oid");
   sqlQuery.append(" order by ");
   sqlQuery.append(resMgr.localizeOrderBy("name"));
	
   Object sqlParamsHierac[] = new Object[2];
   sqlParamsHierac[0] = TradePortalConstants.ACTIVE;
   sqlParamsHierac[1] = userOrgOid;
   hierarchyDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery.toString(), false, sqlParamsHierac);
	  
   Vector orgListDoc = hierarchyDoc.getFragments("/ResultSetRecord");
   totalOrganizations = orgListDoc.size();

   // Now perform data setup for whichever is the current tab.  Each block of
   // code sets tab specific parameters (for data retrieval or dropdowns) as
   // well as setting common things like the appropriate help link and formName


   // Set the performance statistic object to have the current tab as its context
   PerformanceStatistic perfStat = (PerformanceStatistic) session.getAttribute("performanceStatistic");

   if((perfStat != null) && (perfStat.isLinkAction()))
           perfStat.setContext(TradePortalConstants.NAV__BILLING_INSTR_HISTORY_INQUIRIES);

   // ***********************************************************************
   // Data setup for the History  tab
   // ***********************************************************************
      formName     = "TransactionListForm";

      // Determine the organization to select in the dropdown
      selectedOrg = request.getParameter("historyOrg");
      if (selectedOrg == null)
      {
         selectedOrg = (String) session.getAttribute("historyOrg");
         if (selectedOrg == null)
         {
            selectedOrg = EncryptDecrypt.encryptStringUsingTripleDes(userOrgOid, userSession.getSecretKey());
         }
      }

      session.setAttribute("historyOrg", selectedOrg);
      selectedOrg = EncryptDecrypt.decryptStringUsingTripleDes(selectedOrg, userSession.getSecretKey());

      // Check if this is a new Search (based on passed in NewSearch parameter)
      // If so, clear the listview information so it does not
      // retain any page/sort order/ sort column/ search type information 
      // resulting from previous searches
	
      // A new search can also result from the corpo org or status type dropdown 
      // being reselected. In this case, NewDropdownSearch parameter indicates a 
      // pseudo new search -- the search type is retained but the rest of the 
      // search clause is not

      String newSearch = request.getParameter("NewSearch");
      String newDropdownSearch = request.getParameter("NewDropdownSearch");
      Debug.debug("New Search is " + newSearch);
      Debug.debug("New Dropdown Search is " + newDropdownSearch);

      if ((newSearch != null) && (newSearch.equals(TradePortalConstants.INDICATOR_YES)))
      {
        
         // Default search type for instrument status on the transactions history page is ACTIVE.
         session.setAttribute("instrStatusType", TradePortalConstants.STATUS_ACTIVE);
      }

      searchType   = request.getParameter("SearchType");

      if (searchType == null) 
      {
            searchType = TradePortalConstants.SIMPLE;
        
          }
     

      // depending on whether the search type is Advanced or Basic, display
      // the link, link parameters, link text and help links.
      linkParms = "";
      linkText  = "";

      if (searchType.equals(TradePortalConstants.ADVANCED))
      {
         linkParms += "&SearchType=" + TradePortalConstants.SIMPLE;
         link       = formMgr.getLinkAsUrl("goToBillingTransactionsHome", linkParms, response);
         linkText   = resMgr.getText("InstSearch.BasicSearch", TradePortalConstants.TEXT_BUNDLE);

         helpSensitiveLink = OnlineHelp.createContextSensitiveLink("customer/inst_hist.htm", "advanced", resMgr, userSession);
      }
      else
      {
         linkParms += "&SearchType=" + TradePortalConstants.ADVANCED;
         link       = formMgr.getLinkAsUrl("goToBillingTransactionsHome", linkParms, response);
         linkText   = resMgr.getText("InstSearch.AdvancedSearch", TradePortalConstants.TEXT_BUNDLE);

         helpSensitiveLink = OnlineHelp.createContextSensitiveLink("customer/inst_hist.htm", "basic", resMgr, userSession);
      }
      session.setAttribute("instrStatusType", selectedStatus);
      // End data setup for History Tab
%>
     
<%
   
String searchNav = "billingTranHome."+TradePortalConstants.NAV__BILLING_INSTR_HISTORY_INQUIRIES;
%>

<%-- ********************* HTML for page begins here *********************  --%>

<jsp:include page="/common/Header.jsp">
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="additionalOnLoad"         value="<%=onLoad.toString()%>" />
</jsp:include>
 <%--  including TransactionCleanup.jsp to remove lock on instrument which has been placed by this user... --%>
<%--  this is required because (with Portal-refresh) user can navigate anywhere from MenuBar when on Instrument(detail) page. --%>
<jsp:include page="/transactions/TransactionCleanup.jsp"></jsp:include>

<jsp:include page="/common/SearchCriteriaCleanup.jsp">
<jsp:param name="searchCriteriaSection" value="<%=searchNav%>"/>
</jsp:include>

   <%String certAuthURL = ""; //needed for the frag %>
        
<div class="pageMain">
<div class="pageContent">

<div class="secondaryNav">

  <div class="secondaryNavTitle">
    <%=resMgr.getText("TransactionsMenu.Billing", TradePortalConstants.TEXT_BUNDLE)%>
  </div>

   <%
    String navLinkParms = "current2ndNav=" + TradePortalConstants.NAV__BILLING_INSTR_HISTORY_INQUIRIES; 
  %>
  <jsp:include page="/common/NavigationLink.jsp">
    <jsp:param name="linkTextKey"  value="SecondaryNavigation.Instruments.Inquiries" />
    <jsp:param name="linkSelected" value="<%= TradePortalConstants.INDICATOR_YES %>" />
    <jsp:param name="action"       value="goToTradeTransactionsHome" />
    <jsp:param name="linkParms"    value="<%= navLinkParms %>" />
    <jsp:param name="hoverText"       value="HistoryHover" />
  </jsp:include>

  <div id="secondaryNavHelp" class="secondaryNavHelp">
    <%=helpSensitiveLink%>
  </div>
<%=widgetFactory.createHoverHelp("secondaryNavHelp","HelpImageLinkHoverText") %> 
  <div style="clear:both;"></div>

</div>
<%  //include a hidden form for new instrument submission
//this is used for all of the new instrument types available from the menu
Hashtable newInstrSecParms = new Hashtable();
newInstrSecParms.put("UserOid", userSession.getUserOid());
newInstrSecParms.put("SecurityRights", userSession.getSecurityRights());
newInstrSecParms.put("clientBankOid", userSession.getClientBankOid());
newInstrSecParms.put("ownerOrg", userSession.getOwnerOrgOid());
%>



<form name="<%=formName%>" method="POST" 
      action="<%= formMgr.getSubmitAction(response) %>">
  <input type=hidden value="" name=buttonName>
 <jsp:include page="/common/ErrorSection.jsp" />
 
 
  <div class="formContentNoSidebar">
<%  if(searchQueryMap!=null && !searchQueryMap.isEmpty()){
	   savedSearchQueryData = (Map)searchQueryMap.get("BillingInstrHistoryDataView");
	 } 
%>
    <%@ include file="/billing/fragments/Billing-HistoryListView.frag" %>
  </div>
<%= formMgr.getFormInstanceAsInputField(formName, secureParms) %>
</form>

<%
Hashtable newTempSecParms = new Hashtable();
newTempSecParms.put("userOid", userSession.getUserOid());
newTempSecParms.put("securityRights", userSession.getSecurityRights());
newTempSecParms.put("clientBankOid", userSession.getClientBankOid());
newTempSecParms.put("ownerOrg", userSession.getOwnerOrgOid());
newTempSecParms.put("ownerLevel", userSession.getOwnershipLevel());
%>

</div>
</div>

<jsp:include page="/common/Footer.jsp">
  <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
  <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>

<%@ include file="/common/GetSavedSearchQueryData.frag" %> 
<%@ include file="/billing/fragments/Billing-HistoryListViewFooter.frag" %>

 <div id="copySelectedInstrument"></div>
<script type="text/javascript">

<%-- Method added to get the grid selected row details --%>
function getSelectedGridRow(){ 
	require(["dijit/registry","t360/dialog","dojo/on", "t360/datagrid", "dojo/ready","dojo/_base/array"], 
			function(registry,dialog,on,datagrid, ready, baseArray) {
			
			var myGrid1 = registry.byId("billingInstrSearchGridId");
			
			console.log(myGrid1+"-"+myGrid2);
			if(myGrid1){				
				var items = myGrid1.selection.getSelected();
				baseArray.forEach(items, function(item){
					instrType = item.i.InstrumentTypeCode;
					console.log(instrType);
				});
			}
	});
}


require(["dijit/registry","t360/dialog","dojo/on", "t360/datagrid", "dojo/ready","dojo/_base/array"], 
		function(registry,dialog,on,datagrid, ready, baseArray) {
		ready(function(){
		var myGrid1 = registry.byId("billingInstrSearchGridId");
		console.log(myGrid1);
		if(myGrid1 != undefined){
			myGrid1.on("SelectionChanged", function(){
			var items = this.selection.getSelected();
			baseArray.forEach(items, function(item){
				instrType = item.i.InstrumentTypeCode;
				console.log(instrType);
			});
			});
		}
		});	
});

</script>

<%-- **************** JavaScript for Pending tab only *********************  --%>
	   <%-- JavaScript for Instrument History tab to enable form submission by enter.
           event.which works for NetScape and event.keyCode works for IE  --%>
      <script LANGUAGE="JavaScript">
     
         function enterSubmit(event, myform) {
            if (event && event.which == 13) {
               setButtonPressed('null', 0);
               myform.submit();
            }
            else if (event && event.keyCode == 13) {
               setButtonPressed('null', 0);
               myform.submit();
            }
            else {
               return true;
            }
         }
      </script>
</body>
</html>

<%
   formMgr.storeInDocCache("default.doc", new DocumentHandler());
   session.setAttribute("billingTransactions2ndNav", TradePortalConstants.NAV__BILLING_INSTR_HISTORY_INQUIRIES);
%>
