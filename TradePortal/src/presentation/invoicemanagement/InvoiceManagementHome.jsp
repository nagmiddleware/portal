<%--
**********************************************************************************
                               Invoice Management Home

  Description:  
     This page is used as the main Invoice Management page. It displays  
     three tabs: Invoice uploads, Invoice Groups, Receivables Instruments.

**********************************************************************************
--%>

<%--
 *
 *     Copyright  © 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page
	import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*, com.ams.tradeportal.html.*,
                  java.util.*"%>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager"
	scope="session"></jsp:useBean>
<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager"
	scope="session"></jsp:useBean>
<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager"
	scope="session"></jsp:useBean>
<jsp:useBean id="userSession"
	class="com.ams.tradeportal.busobj.webbean.SessionWebBean"
	scope="session"></jsp:useBean>

<%
StringBuffer      onLoad                = new StringBuffer();
Hashtable         secureParms           = new Hashtable();
String            helpSensitiveLink     = null;
String            currentTabLink        = null;
String            currentTab            = null;
String            formName              = null;
String            tabOn                 = null;
String            userOrgOid            = null;
String            userOid               = null;
String            userSecurityRights    = null;
String            userSecurityType      = null;
String            invoiceGroupOid       = null;
DocumentHandler   hierarchyDoc                 = null;
StringBuffer      dynamicWhereClause           = new StringBuffer();
StringBuffer      dropdownOptions              = new StringBuffer();
int               totalOrganizations           = 0;
StringBuffer      extraTags                    = new StringBuffer();
StringBuffer      newLink                      = new StringBuffer();
StringBuffer      newUploadLink                = new StringBuffer();
StringBuilder     invoicesForHeader            = new StringBuilder();//BSL IR NLUM040376572 04/06/2012 ADD
String            userDefaultWipView           = null; 
String            selectedWorkflow             = null;
String            selectedStatus               = "";
String 	          loginLocale                  = null;
Vector            codesToExclude               = null;
String gridID = null;
String gridName = null;
String searchID = null;
String onSearch = null;
String dataView = null;
String dataRefreshID = null;
String dataEditID = null;

boolean groupingEnabled = false;
String usePaymentDate = "";
String  timeZone						   		= null;
//InvoiceGroupWebBean invoiceGroup = null;
final String      ALL_ORGANIZATIONS            = resMgr.getText("AuthorizedTransactions.AllOrganizations", 
        TradePortalConstants.TEXT_BUNDLE);
final String      ALL_WORK                     = resMgr.getText("PendingTransactions.AllWork", 
        TradePortalConstants.TEXT_BUNDLE);
final String      MY_WORK                      = resMgr.getText("PendingTransactions.MyWork",  
        TradePortalConstants.TEXT_BUNDLE);
   
   StringBuffer newSearchReceivableCriteria = new StringBuffer();                          

   //cquinton 1/18/2013 set return page
   if ( "true".equals( request.getParameter("returning") ) ) {
     userSession.pageBack(); //to keep it correct
   }
   else {
     userSession.addPage("goToInvoiceManagement");
   }
	// adding primary navigation Rpasupulati
   userSession.setCurrentPrimaryNavigation("NavigationBar.UploadCenter");
   
   //cquinton 1/18/2013 remove old close action behavior

  Map searchQueryMap =  userSession.getSavedSearchQueryData();
  Map savedSearchQueryDataLR =null;
  Map savedSearchQueryDataCR =null;
  Map savedSearchQueryDataPen =null;
  Map savedSearchQueryDataPayPrgm =null;
  Map savedSearchQueryData =null; 
  Map savedSearchQueryDataL =null;
  Map savedSearchQueryDataG =null;
  Map savedSearchQueryDataC =null; 
  String statusRefData = "UPLOAD_INVOICE_STATUS";
   userSecurityRights = userSession.getSecurityRights();
   userSecurityType   = userSession.getSecurityType();
	
   userOrgOid         = StringFunction.xssCharsToHtml(userSession.getOwnerOrgOid());
   userOid            = userSession.getUserOid();
   timeZone			  =  StringFunction.xssCharsToHtml(userSession.getTimeZone());   
   secureParms.put("timeZone",   userSession.getTimeZone());
   secureParms.put("clientBankOid",   userSession.getClientBankOid());
   secureParms.put("baseCurrencyCode", StringFunction.xssCharsToHtml(userSession.getBaseCurrencyCode()));   

   boolean hasViewUploadInvoiceRights    = SecurityAccess.hasRights(userSecurityRights, SecurityAccess.VIEW_UPLOADED_INVOICES);
   boolean hasUploadInvoicesRights       = SecurityAccess.hasRights(userSecurityRights, SecurityAccess.UPLOAD_INVOICES);
   boolean hasFailedInvoiceRemoveRights  = SecurityAccess.hasRights(userSecurityRights, SecurityAccess.REMOVE_UPLOADED_FILES);
   
   // getting the user security type to display authorization button or not.   
   boolean isAdminUser;
   if (userSession.getSavedUserSession() == null) {
      isAdminUser = TradePortalConstants.ADMIN.equals(userSession.getSecurityType());
   } else {
      isAdminUser = TradePortalConstants.ADMIN.equals(userSession.getSavedUserSessionSecurityType());
   }

   CorporateOrganizationWebBean corpOrg = beanMgr.createBean(CorporateOrganizationWebBean.class, "CorporateOrganization");
   corpOrg.getById(userOrgOid);
   UserWebBean thisUser = beanMgr.createBean(UserWebBean.class, "User");
   thisUser.setAttribute("user_oid", userOid);
   thisUser.getDataFromAppServer();
   usePaymentDate = corpOrg.getAttribute("payment_day_allow");  
   String corporateOrgInvFileUploadPayInd = corpOrg.getAttribute("invoice_file_upload_pay_ind");  //Rel9.0 CR 913 
   String corporateOrgInvFileUploadRecInd = corpOrg.getAttribute("invoice_file_upload_rec_ind");  //Rel9.0 CR 913
	//#RKAZI CR1006 04-24-2015 - BEGIN
   String corporateOrgRecInvAllowH2HApprovalInd = corpOrg.getAttribute("allow_h2h_rec_inv_approval");
   String corporateOrgPayInvAllowH2HApprovalInd = corpOrg.getAttribute("allow_h2h_pay_inv_approval");
   String corporateOrgPayCrnAllowH2HApprovalInd = corpOrg.getAttribute("allow_h2h_pay_crn_approval");
   //#RKAZI CR1006 04-24-2015 - END 
   String yes = "yes";
  
   secureParms.put("usePaymentDate",usePaymentDate);   
   boolean isPayMgmGroupingEnabled = InvoiceUtility.isPayablesPrgmInvoiceGroupEnable(corpOrg); //CR 913
   boolean isIntegratedPayPrgmEnabled = InvoiceUtility.isIntegratedPayPrgmEnabled(corpOrg); //CR 913
   boolean recGroupingEnabled = TradePortalConstants.INVOICES_GROUPED_TPR.equals(corpOrg.getAttribute("allow_grouped_invoice_upload"));
   boolean recGroupingNotEnabled = TradePortalConstants.INVOICES_NOT_GROUPED_TPR.equals(corpOrg.getAttribute("allow_grouped_invoice_upload"));
   
   boolean isLRQEnabled = TradePortalConstants.INDICATOR_YES.equals(corpOrg.getAttribute("allow_loan_request"));
   boolean isCreditNoteEnabled = TradePortalConstants.INDICATOR_YES.equals(corpOrg.getAttribute("allow_pay_credit_note_upload"));
   boolean isRecPrgmEnabled = TradePortalConstants.INDICATOR_YES.equals(corpOrg.getAttribute("rec_invoice_utilised_ind"));
   
   String confInd = "";
   if (userSession.getSavedUserSession() == null)
       confInd = thisUser.getAttribute("confidential_indicator");
   else
   {
       if (TradePortalConstants.ADMIN.equals(userSession.getSavedUserSession().getSecurityType()))
           confInd = TradePortalConstants.INDICATOR_YES;
       else
           confInd = thisUser.getAttribute("subsid_confidential_indicator");  
   }
   if (InstrumentServices.isBlank(confInd))
      confInd = TradePortalConstants.INDICATOR_NO;
   
   currentTab = request.getParameter("current2ndNav");
   if (currentTab == null)
   {
      currentTab = (String) session.getAttribute("currentInvoiceMgmtTab");
      if (currentTab == null)
      {
         currentTab = TradePortalConstants.INVOICE_LIST_TAB;;
      }
   }
         
   // This is the query used for populating the workflow drop down list; it retrieves
   // all active child organizations that belong to the user's current org.
   
   String sqlQuery = "select organization_oid, name  from corporate_org where activation_status = ?  start with organization_oid = ? connect by prior "
		   +" organization_oid = p_parent_corp_org_oid order by "+resMgr.localizeOrderBy("name");

   hierarchyDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery, false, TradePortalConstants.ACTIVE, userOrgOid);
	  
   Vector orgListDoc = hierarchyDoc.getFragments("/ResultSetRecord");
   totalOrganizations = orgListDoc.size();
 
   String currentFolder = request.getParameter("currentFolder");
       if (currentFolder == null)
       {
          currentFolder = (String) session.getAttribute("currentFolder");
          if (currentFolder == null)
          {
        	  if((TradePortalConstants.INDICATOR_YES.equals(corporateOrgInvFileUploadPayInd) || TradePortalConstants.INDICATOR_YES.equals(corporateOrgPayInvAllowH2HApprovalInd)) && isPayMgmGroupingEnabled){ //#RKAZI CR1006 04-24-2015  - Add H2H Approval condition
             	 currentFolder = TradePortalConstants.PAYABLE_INV_GROUP_TAB;
              }
              else if((TradePortalConstants.INDICATOR_YES.equals(corporateOrgInvFileUploadPayInd) || TradePortalConstants.INDICATOR_YES.equals(corporateOrgPayInvAllowH2HApprovalInd)) && !isPayMgmGroupingEnabled){//#RKAZI CR1006 04-24-2015  - Add H2H Approval condition
             	 currentFolder = TradePortalConstants.PAYABLE_INV_LIST_TAB;
              }
              else if((TradePortalConstants.INDICATOR_YES.equals(corporateOrgInvFileUploadRecInd) || TradePortalConstants.INDICATOR_YES.equals(corporateOrgRecInvAllowH2HApprovalInd)) && recGroupingEnabled){ //#RKAZI CR1006 04-24-2015  - Add H2H Approval condition
            	 currentFolder = TradePortalConstants.INV_GROUP_TAB;
             }
             else if((TradePortalConstants.INDICATOR_YES.equals(corporateOrgInvFileUploadRecInd) || TradePortalConstants.INDICATOR_YES.equals(corporateOrgRecInvAllowH2HApprovalInd)) && recGroupingNotEnabled){ //#RKAZI CR1006 04-24-2015  - Add H2H Approval condition
            	 currentFolder = TradePortalConstants.INVOICE_LIST_TAB;
             }
          }
       }
       newLink = new StringBuffer();

       newLink.append(formMgr.getLinkAsUrl("goToInvoiceManagement", response));
   String label="";   
   String titleKey = "NavigationBar.InvoiceManagement";
        //SHR PR 708
   // Now perform data setup for whichever is the current tab.  Each block of
   // code sets tab specific parameters (for data retrieval or dropdowns) as
   // well as setting common things like the appropriate help link and formName

   // Set the performance statistic object to have the current tab as its context
   PerformanceStatistic perfStat = (PerformanceStatistic) session.getAttribute("performanceStatistic");

   if((perfStat != null) && (perfStat.isLinkAction()))
           perfStat.setContext(currentTab);

   // ***********************************************************************
   // Data setup for the Invoice Uploads tab
   // ***********************************************************************
   formName = "UploadInvoiceListForm";
   String helpText = "customer/ungrouped_invoice_list.htm";
   
   if (TradePortalConstants.INVOICE_LIST_TAB.equals(currentTab)) 
      {
      formName     = "UploadInvoiceListForm";
      helpSensitiveLink = OnlineHelp.createContextSensitiveLink("customer/invoices.htm",  resMgr, userSession);
      loginLocale=userSession.getUserLocale();
      if(TradePortalConstants.PAYABLE_INV_GROUP_TAB.equals(currentFolder)){
    	  formName     = "PayableInvoiceGroupListForm";
      }
      
   
   	else if (TradePortalConstants.INV_GROUP_TAB.equals(currentFolder)) {
      formName = "InvoiceGroupListForm";
      helpText = "customer/receivable_grouped_invoice_management.htm";
   
  }}
  
   Debug.debug("form " + formName); 
   
   newLink.append("&current2ndNav=" + currentTab);
   String searchNav = "invManHome."+currentTab+currentFolder;  
   if (userSession.getSavedUserSession() == null) {
       secureParms.put("AuthorizeAtParentViewOrSubs", TradePortalConstants.INDICATOR_YES);
 }else{
	  secureParms.put("AuthorizeAtParentViewOrSubs", TradePortalConstants.INDICATOR_NO);
 }
   
   //IR 29690 start
   String[] opBankOid = {corpOrg.getAttribute("first_op_bank_org"),
                        corpOrg.getAttribute("second_op_bank_org"),
                        corpOrg.getAttribute("third_op_bank_org"),
                        corpOrg.getAttribute("fourth_op_bank_org")};
   
   //do a local query to get names for operational bank orgs
   //could do this as dataview, but this is simpler - we don't expect this to  
   //change on this dialog via search.
   DocumentHandler oboListDoc = null;
 
   List<Object> sqlParams = new ArrayList();
   StringBuilder in = new StringBuilder("?");
   sqlParams.add(opBankOid[0]);
   for (int oboCount=1; oboCount<4; oboCount++) {
 	  
     if (StringFunction.isBlank(opBankOid[oboCount])) {
       break;
     }
     else {
         in.append(", ? ");
         sqlParams.add(opBankOid[oboCount]);
     }
   }
   try {
 	    StringBuilder sql = new StringBuilder();
 	    sql.append("select ORGANIZATION_OID, NAME");
 	    sql.append(" from OPERATIONAL_BANK_ORG");
 	    sql.append(" where ORGANIZATION_OID in (").append(in).append(")");
 	    sql.append(" order by name");
 		System.out.println("sql.toString():"+sql.toString());
 	    oboListDoc = DatabaseQueryBean.getXmlResultSet(sql.toString(), true, sqlParams);

 	  } catch (AmsException e) { 
 	    //todo: should throw an error
 	    Debug.debug("queryListView threw an exception"); 
 	    e.printStackTrace();
   }
 	 String orgOid ="";
 	 int bankOrgList =0; 
   StringBuilder bbGridData = new StringBuilder();
   if ( oboListDoc != null ) {
     Vector oboList = oboListDoc.getFragments("/ResultSetRecord");
     bankOrgList =oboList.size(); 
     for ( int oboIdx = 0; oboIdx<oboList.size(); oboIdx++ ) {
       DocumentHandler oboDoc = (DocumentHandler) oboList.get(oboIdx);
        orgOid = oboDoc.getAttribute("/ORGANIZATION_OID");
       String orgName = oboDoc.getAttribute("/NAME");
 			bbGridData.append("<option value='").append(orgOid).append("'>");
             bbGridData.append(StringFunction.xssCharsToHtml(orgName));
             bbGridData.append("</option>");
     }
   }
   
 //IR 29690 end
%>

<%-- ********************* HTML for page begins here *********************  --%>
<% WidgetFactory widgetFactory = new WidgetFactory(resMgr); %>

<jsp:include page="/common/Header.jsp">
	<jsp:param name="includeNavigationBarFlag"
		value="<%=TradePortalConstants.INDICATOR_YES%>" />
	<jsp:param name="includeErrorSectionFlag"
		value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>
<jsp:include page="/common/SearchCriteriaCleanup.jsp">
<jsp:param name="searchCriteriaSection" value="<%=searchNav%>"/>
</jsp:include>

<div class="pageMainNoSidebar">
  <div id="InvoiceMgtDivID" class="pageContent gridInvoiceFooterDivHidden">


<div class="secondaryNav">

  <span class="secondaryNavTitle">
    <%=resMgr.getText("InvoiceMgmt.InvoiceManagement",
                      TradePortalConstants.TEXT_BUNDLE)%>
  </span>

<%  String standardLinkSelected = TradePortalConstants.INDICATOR_NO,
	customLinkSelected = TradePortalConstants.INDICATOR_NO;
	String linkParms = null;     
     
     if(TradePortalConstants.PAYABLE_INV_GROUP_TAB.equals(currentFolder)){
       standardLinkSelected = TradePortalConstants.INDICATOR_YES;	  
     }
	
      if(TradePortalConstants.INV_GROUP_TAB.equals(currentFolder)){
       customLinkSelected = TradePortalConstants.INDICATOR_YES;	  
     }	
      if(TradePortalConstants.INVOICE_LIST_TAB.equals(currentFolder)){
          customLinkSelected = TradePortalConstants.INDICATOR_YES;	  
        }
      if(TradePortalConstants.PAYABLE_INV_LIST_TAB.equals(currentFolder)){
    	  standardLinkSelected = TradePortalConstants.INDICATOR_YES;	  
        }
  %>
  <%if(TradePortalConstants.INDICATOR_YES.equals(corporateOrgInvFileUploadPayInd) || TradePortalConstants.INDICATOR_YES.equals(corporateOrgPayInvAllowH2HApprovalInd)) { //#RKAZI CR1006 04-24-2015  - Add H2H Approval condition
	  if(isPayMgmGroupingEnabled)
    linkParms = "currentFolder=" + TradePortalConstants.PAYABLE_INV_GROUP_TAB;
	  else
	linkParms = "currentFolder=" + TradePortalConstants.PAYABLE_INV_LIST_TAB;  
  %>
	   <jsp:include page="/common/NavigationLink.jsp">
	     <jsp:param name="linkTextKey"  value="InvoiceMgmt.PayInv" /> 
	     <jsp:param name="linkSelected" value="<%= standardLinkSelected %>" />
	     <jsp:param name="action"       value="goToInvoiceManagement" />
	     <jsp:param name="linkParms"    value="<%= linkParms %>" />
	   </jsp:include>
   <%}
    if(TradePortalConstants.INDICATOR_YES.equals(corporateOrgInvFileUploadRecInd) || TradePortalConstants.INDICATOR_YES.equals(corporateOrgRecInvAllowH2HApprovalInd)) { //#RKAZI CR1006 04-24-2015  - Add H2H Approval condition
		%>
   <%   if(recGroupingEnabled)
   linkParms = "currentFolder=" + TradePortalConstants.INV_GROUP_TAB; 
    else
	linkParms = "currentFolder=" + TradePortalConstants.INVOICE_LIST_TAB;  
	%>

	   <jsp:include page="/common/NavigationLink.jsp">
	     <jsp:param name="linkTextKey"  value="InvoiceMgmt.RecInv" /> 
	     <jsp:param name="linkSelected" value="<%= customLinkSelected %>" />
	     <jsp:param name="action"       value="goToInvoiceManagement" />
	     <jsp:param name="linkParms"    value="<%= linkParms %>" />
	   </jsp:include>
    <%} %>

  <span class="secondaryNav-right">
<%
      String helpSensitiveLink1 =
        OnlineHelp.createContextSensitiveLink(helpText,  resMgr, userSession);
%>
    <span class="secondaryNavHelp" id="secondaryNavHelp">
      <%=helpSensitiveLink1%>
    </span>
    <%=widgetFactory.createHoverHelp("secondaryNavHelp","HelpImageLinkHoverText") %> 


  </span>
  <div style="clear:both;"></div>

</div>
 

<%if (TradePortalConstants.INV_GROUP_TAB.equals(currentFolder) || TradePortalConstants.INVOICE_LIST_TAB.equals(currentFolder)) { %>
    
    <jsp:include page="/common/PageSubHeader.jsp">
      <jsp:param name="titleKey" value="InvoiceMgmt.RecInv" /> 
    </jsp:include>
<%} %>
<%if (TradePortalConstants.PAYABLE_INV_GROUP_TAB.equals(currentFolder) || TradePortalConstants.PAYABLE_INV_LIST_TAB.equals(currentFolder)) { %>
    
    <jsp:include page="/common/PageSubHeader.jsp">
      <jsp:param name="titleKey" value="InvoiceMgmt.PayInv" />
    </jsp:include>
<%} %>

  <% //AAlubala - 04/23/2012 - reauthenticate if necessary
    String certAuthURL = ""; //needed for openReauth frag
    Cache reCertCache = (Cache)TPCacheManager.getInstance().getCache(TradePortalConstants.CLIENT_BANK_CACHE);
    DocumentHandler CBCResult = (DocumentHandler)reCertCache.get(userSession.getClientBankOid());	
    String requireTranAuth = CBCResult.getAttribute("/ResultSetRecord(0)/REQUIRE_TRAN_AUTH");
    boolean InvoiceRequireAuth = InstrumentAuthentication.requireTransactionAuthentication(
            requireTranAuth,InstrumentAuthentication.TRAN_AUTH__INVOICE_MGNT_FIN_INVOICE);
    boolean PayInvoiceRequireAuth = InstrumentAuthentication.requireTransactionAuthentication(
            requireTranAuth,InstrumentAuthentication.TRAN_AUTH__PAYABLES_UPLOAD_INV_PROCESS);
    boolean offlineAuthoriseRequireAuth = SecurityAccess.hasRights(userSecurityRights,  SecurityAccess.UPLOAD_PAY_MGM_OFFLINE_AUTH);
    String authorizeLink = "";

 
    String proxyAuthLink = "";
%>

<form id="<%=formName%>" name="<%=formName%>" method="POST" action="<%= formMgr.getSubmitAction(response) %>" data-dojo-type="dijit.form.Form" style="margin-bottom: 0px;">
<jsp:include page="/common/ErrorSection.jsp" />
 <div class="formContentNoSidebar">

 <input type="hidden" name="paymentDate" />
 <input type="hidden" name="loanType" />
 <input type="hidden" name="instrumentType" /> 
 <input type="hidden" name="bankBranch" />
<input type=hidden value="" name=buttonName>
<input type=hidden value="yes" name="fromPayGroup">
<input type=hidden  name="timeZone" value= "<%=timeZone%>">
<input type=hidden  name="baseCurrencyCode" value = "<%=userSession.getBaseCurrencyCode()%>">

<input type=hidden value="yes" name="fromRecGroup">
<input type="hidden" name="supplierDate" /> 
<input type="hidden" name="paymentAmount" />

<%  //AAlubala VASCO reauthentication hidden fields
    if ( InvoiceRequireAuth || PayInvoiceRequireAuth || offlineAuthoriseRequireAuth) {
%> 

      <input type=hidden name="reCertification" value="<%=TradePortalConstants.INDICATOR_NO%>">
      <input type=hidden name="reCertOK">
      <input type=hidden name="logonResponse">
      <input type=hidden name="logonCertificate">

<%
    }
   
    if (InvoiceRequireAuth || PayInvoiceRequireAuth || offlineAuthoriseRequireAuth) {
    	
  %>   
  <%@ include file="/logon/fragments/openReauthenticationWindow.frag" %>
  <%
        //NOTE: although there are lists on the screen, this is specific to the pay remit
        authorizeLink = "javascript:openReauthenticationWindowForList('" + StringFunction.xssCharsToHtml(certAuthURL) + "'," +
            "'" + formName + "','" + TradePortalConstants.BUTTON_INV_AUTHORIZE_INVOICES + "'," +
            "'Invoice')"; //IR#MUM061409797 Rel8.0 - 06/20/12- AAlubala - Corrected the name of the invoice Auth Button

       proxyAuthLink = "javascript:openReauthenticationWindowForList('" + StringFunction.xssCharsToHtml(certAuthURL) + "'," +
	       "'" + formName + "','" + TradePortalConstants.BUTTON_PROXY_AUTHORIZE + "'," +
           "'Invoice')";
    }
    //vasco end
  %> 

<%--VASCO END --%>
 
 <% if (currentTab.equals(TradePortalConstants.INVOICE_LIST_TAB)){
	  session.setAttribute("documentImageFileUploadPageOriginator", formMgr.getCurrPage());
     if(TradePortalConstants.PAYABLE_INV_GROUP_TAB.equals(currentFolder)){
    	 if(searchQueryMap!=null && !searchQueryMap.isEmpty()){
			 savedSearchQueryDataLR = (Map)searchQueryMap.get("InvoicePayableLRGroupListDataView");
			 savedSearchQueryDataPen = (Map)searchQueryMap.get("InvoicePayablePendingGroupListDataView"); 
			 savedSearchQueryDataPayPrgm = (Map)searchQueryMap.get("PayablesPrgmInvoiceGroupListDataView"); 
			 savedSearchQueryDataCR = (Map)searchQueryMap.get("PayablesCreditNoteListDataView");
		}%>
  <%@ include file="/invoicemanagement/fragments/InvoiceManagement_PayableInvoiceGroups.frag"%>
 <% }else if(TradePortalConstants.INVOICE_LIST_TAB.equals(currentFolder) ){
	  if(searchQueryMap!=null && !searchQueryMap.isEmpty()){
			 savedSearchQueryData = (Map)searchQueryMap.get("InvoiceUploadListDataView");
		}%>
        <%@ include file="/invoicemanagement/fragments/InvoiceManagement_Invoices.frag"%>
     <% }
     else if(TradePortalConstants.INV_GROUP_TAB.equals(currentFolder)){
    	  if(searchQueryMap!=null && !searchQueryMap.isEmpty()){
			 savedSearchQueryDataG = (Map)searchQueryMap.get("InvoiceGroupDataView");
			 savedSearchQueryDataL = (Map)searchQueryMap.get("InvoiceLoanRequestDataView");
			 savedSearchQueryDataC = (Map)searchQueryMap.get("InvoicePendingActionDataView");
		}%>

      <%@ include file="/invoicemanagement/fragments/InvoiceManagement_InvoiceGroups.frag"%>
            <% }else if(TradePortalConstants.PAYABLE_INV_LIST_TAB.equals(currentFolder) ){ 
            if(searchQueryMap!=null && !searchQueryMap.isEmpty()){
			savedSearchQueryData = (Map)searchQueryMap.get("PayablesPrgmInvoiceListDataView");
			savedSearchQueryDataCR = (Map)searchQueryMap.get("PayablesCreditNoteListDataView");//IR 36527
		}%>   
            <%@ include file="/invoicemanagement/fragments/InvoiceManagement_PayableInvoices.frag"%>   
            <%}%> 
    </div> 
 
<%}

%>

<%= formMgr.getFormInstanceAsInputField(formName, secureParms) %>
</form>

<form method="post" id="InvoiceGroup-NotifyPanelUserForm" name="InvoiceGroup-NotifyPanelUserForm" action="<%=formMgr.getSubmitAction(response)%>">
<input type=hidden name="buttonName" value="">
<input type=hidden name="invoiceGroupOid" value=""> 
<%= formMgr.getFormInstanceAsInputField("InvoiceGroup-NotifyPanelUserForm", secureParms) %>
 </form>
 <form method="post" id="InvoiceSummary-NotifyPanelUserForm" name="InvoiceSummary-NotifyPanelUserForm" action="<%=formMgr.getSubmitAction(response)%>">
<input type=hidden name="buttonName" value="">
<input type=hidden name="invoiceSummaryOid" value=""> 
<%= formMgr.getFormInstanceAsInputField("InvoiceSummary-NotifyPanelUserForm", secureParms) %>
 </form>



<jsp:include page="/common/Footer.jsp">
  <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
  <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>
<%@ include file="/common/GetSavedSearchQueryData.frag" %>

	
<script type="text/javascript" src="/portal/js/datagrid.js"></script>

     <%if(TradePortalConstants.INVOICE_LIST_TAB.equals(currentTab)){ %>
      <div id="InvoiceGroupListDialogID" ></div>
       <div id="PayablesPrgmInvoiceListDialogID" ></div>
       <div id="creditNoteApplySearchDialog" ></div>
       <div id="creditNoteUnApplySearchDialog" ></div>
     <%if(TradePortalConstants.PAYABLE_INV_GROUP_TAB.equals(currentFolder)){  %>
  <%@ include file="/invoicemanagement/fragments/InvoiceManagement_PayableInvoiceGroupFooter.frag"%>
 <% }else if(TradePortalConstants.INVOICE_LIST_TAB.equals(currentFolder)){	%>
      <%@ include file="/invoicemanagement/fragments/InvoiceManagement_InvoiceListFooter.frag" %>

	<% } 
 else if(TradePortalConstants.PAYABLE_INV_LIST_TAB.equals(currentFolder) ){   %>
  <%@ include file="/invoicemanagement/fragments/InvoiceManagement_PayableInvoiceListFooter.frag"%>
 <% }else if(TradePortalConstants.INV_GROUP_TAB.equals(currentFolder)){  %>
	 <%@ include file="/invoicemanagement/fragments/InvoiceManagement_InvoiceGroupFooter.frag" %>
	<jsp:include page="/common/dGridShowCountFooter.jsp">
  <jsp:param name="gridId" value="InvoiceGroupSummaryId" />
</jsp:include>
	<%} }%>

</div>
</div>

<%if(isCreditNoteEnabled || TradePortalConstants.INDICATOR_YES.equals(corporateOrgPayCrnAllowH2HApprovalInd)){ //#RKAZI CR1006 04-24-2015  - Add H2H Approval condition
%>
<jsp:include page="/common/gridShowCountFooter.jsp">
  <jsp:param name="gridId" value="PayablesCreditNoteListId" />
</jsp:include>
<% }%>
<jsp:include page="/common/gridShowCountFooter.jsp">
  <jsp:param name="gridId" value="InvoiceUploadListId" />
</jsp:include>
</body>
</html>

<%
   formMgr.storeInDocCache("default.doc", new DocumentHandler());
   session.setAttribute("currentInvoiceMgmtTab", currentTab);
   session.setAttribute("currentFolder",currentFolder);
%>
