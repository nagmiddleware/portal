<%--
*******************************************************************************
                        Payable Invoice Group Detail

  Description:
     This page is used to display the Payable invoices of an invoice group.

*******************************************************************************
--%>

<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="java.util.*,com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.html.*" %>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session">
</jsp:useBean>


<%-- ************** Data retrieval page setup begins here ****************  --%>

<%
StringBuffer      onLoad                = new StringBuffer();
String            helpSensitiveLink     = null;
String            currentTabLink        = null;
String            currentTab            = TradePortalConstants.PAYABLE_INV_GROUP_TAB;
String            formName              = null;
String            tabOn                 = null;
String            userOrgOid            = null;
String            userOid               = null;
String            userSecurityRights    = null;
String            userSecurityType      = null;
StringBuffer      dynamicWhereClause           = new StringBuffer();
StringBuffer      dropdownOptions              = new StringBuffer();
int               totalOrganizations           = 0;
StringBuffer      extraTags                    = new StringBuffer();
StringBuffer      newLink                      = new StringBuffer();
StringBuffer      newUploadLink                = new StringBuffer();
String  timeZone						   		= null;
String            userDefaultWipView           = null; 
String            selectedWorkflow             = null;
String            selectedStatus               = "";
String            loginLocale                  = null;
Vector            codesToExclude               = null;
String searchListViewName = "PayableInvoiceGroupDetailListView.xml";

userSession.setCurrentPrimaryNavigation(resMgr.getText("NavigationBar.InvoiceManagement",
        TradePortalConstants.TEXT_BUNDLE));

//cquinton 1/18/2013 set return page
if ( "true".equals( request.getParameter("returning") ) ) {
  userSession.pageBack(); //to keep it correct
}
else {
  userSession.addPage("goToInvoiceManagement");
}

//cquinton 1/18/2013 remove old close action stuff
   userSecurityRights = userSession.getSecurityRights();
   userSecurityType   = userSession.getSecurityType();
   userOrgOid         = StringFunction.xssCharsToHtml(userSession.getOwnerOrgOid());
   userOid            = StringFunction.xssCharsToHtml(userSession.getUserOid());
   timeZone			  = StringFunction.xssCharsToHtml(userSession.getTimeZone());		
   session.setAttribute("documentImageFileUploadPageOriginator", formMgr.getCurrPage());
   // getting the user security type to display authorization button or not.
   boolean isAdminUser;
   if (userSession.getSavedUserSession() == null) {
      isAdminUser = TradePortalConstants.ADMIN.equals(userSession.getSecurityType());
   } else {
      isAdminUser = TradePortalConstants.ADMIN.equals(userSession.getSavedUserSessionSecurityType());
   }

   CorporateOrganizationWebBean corpOrg = beanMgr.createBean(CorporateOrganizationWebBean.class, "CorporateOrganization");
   corpOrg.getById(userOrgOid);
   UserWebBean thisUser = beanMgr.createBean(UserWebBean.class, "User");
   thisUser.getById(userOid);
   
   boolean groupingEnabled = true; //TradePortalConstants.INVOICES_GROUPED_TPR.equals(corpOrg.getAttribute("allow_grouped_invoice_upload"));
   //if (groupingEnabled) {
	   session.removeAttribute("fromInvoiceGroupDetail");
   //}

   formName = "UploadInvoiceListForm";
   helpSensitiveLink = OnlineHelp.createContextSensitiveLink("customer/invoices.htm", resMgr, userSession);
   loginLocale = userSession.getUserLocale();

   String invoiceGroupOid = StringFunction.xssCharsToHtml(request.getParameter("invoice_group_oid"));
   if(InstrumentServices.isNotBlank(invoiceGroupOid)){
	   session.setAttribute("invGrpOid",invoiceGroupOid);
	}
   else{
	   invoiceGroupOid =(String) session.getAttribute("invGrpOid");
   }
   Hashtable secureParms = new Hashtable();
   secureParms.put("UserOid", userSession.getUserOid());
   secureParms.put("SecurityRights",   userSecurityRights);
   secureParms.put("ownerOrg",   userOrgOid);
   secureParms.put("clientBankOid", userSession.getClientBankOid());
   if(InstrumentServices.isNotBlank(invoiceGroupOid))
   secureParms.put("invoice_group_oid", invoiceGroupOid);
   if (invoiceGroupOid != null) {
	   // Srini_D IR# T36000005211 Rel 8.1.1 09/06 /2012    Start
     // invoiceGroupOid = EncryptDecrypt.decryptAlphaNumericString(invoiceGroupOid);
	  invoiceGroupOid = EncryptDecrypt.decryptStringUsingTripleDes(invoiceGroupOid, userSession.getSecretKey()); 
	   // Srini_D IR# T36000005211 Rel 8.1.1 09/06 /2012    End
   }

   InvoiceGroupWebBean invoiceGroup = beanMgr.createBean(InvoiceGroupWebBean.class, "InvoiceGroup");
   invoiceGroup.getById(invoiceGroupOid);
   
   // Store values such as the userid, his security rights, and his org in a secure
   // hashtable for the form.  The mediator needs this information.
  //jgadela  R92 - SQL Injection FIX
   String groupDueDatesql = " SELECT due_date FROM invoice_group_view WHERE invoice_group_oid = ? ";
   DocumentHandler invoiceDueDateDoc = DatabaseQueryBean.getXmlResultSet(groupDueDatesql.toString(), false, new Object[]{invoiceGroupOid});
   String dueDate = "";
   String paymentDate = "";
   if(invoiceDueDateDoc !=null){
	   dueDate = (invoiceDueDateDoc.getFragment("/ResultSetRecord")).getAttribute("DUE_DATE");
   }  
   if(StringFunction.isBlank(dueDate)){
	   dueDate = invoiceGroup.getAttribute("payment_date");
   }
   
   StringBuilder invoicesForHeader = new StringBuilder(resMgr.getText("UploadInvoiceDetail.InvoicesFor", TradePortalConstants.TEXT_BUNDLE));
   invoicesForHeader.append(" - ").append(invoiceGroup.getAttribute("trading_partner_name"));
   invoicesForHeader.append(" - ").append(invoiceGroup.getAttribute("currency"));
   invoicesForHeader.append(" - ").append(resMgr.getText("UploadInvoiceDetail.Date", TradePortalConstants.TEXT_BUNDLE));
   invoicesForHeader.append(": ").append(TPDateTimeUtility.formatDate(dueDate, TPDateTimeUtility.SHORT, loginLocale));
%>


<% //AAlubala - 04/23/2012 - reauthenticate if necessary
    String certAuthURL = ""; //needed for openReauth frag
    Cache reCertCache = (Cache)TPCacheManager.getInstance().getCache(TradePortalConstants.CLIENT_BANK_CACHE);
    DocumentHandler CBCResult = (DocumentHandler)reCertCache.get(userSession.getClientBankOid());	
    String requireTranAuth = CBCResult.getAttribute("/ResultSetRecord(0)/REQUIRE_TRAN_AUTH");
    boolean InvoiceRequireAuth = InstrumentAuthentication.requireTransactionAuthentication(
        requireTranAuth,InstrumentAuthentication.TRAN_AUTH__INVOICE_MGNT_FIN_INVOICE);

    String authorizeLink = "";

    //CR711 - 04/18/2012
    String proxyAuthLink = "";
%>
<%-- ********************* HTML for page begins here *********************  --%>
<% WidgetFactory widgetFactory = new WidgetFactory(resMgr); %>


<%-- Body tag included as part of common header --%>

<jsp:include page="/common/Header.jsp">
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeErrorSectionFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="additionalOnLoad" value="<%=onLoad.toString()%>" />
</jsp:include>

<div class="pageMainNoSidebar">
  <%-- IR T36000030360: (gridInvoiceFooterDivHidden) Hide page content, untill widgets get parsed. --%>
  <div id="InvoiceMgtDivID" class="pageContent gridInvoiceFooterDivHidden">

    <jsp:include page="/common/PageHeader.jsp">
      <jsp:param name="titleKey" value="InvoiceMgmt.PayInvPrgmInvList" />
      <jsp:param name="helpUrl" value="customer/receivables_grouped_invoice_list.htm" />
    </jsp:include>

    <%--cquinton 11/1/2012 ir#7015 replace return link with close button--%>
    <div class="subHeaderDivider"></div>
    <div class="pageSubHeader">
      <span class="pageSubHeaderItem">
        <%= invoicesForHeader %>
      </span>
      <span class="pageSubHeader-right">
        <%--cquinton 10/31/2012 ir#7015 change return link to close button when no close button present.
            note this does NOT include an icon as in the sidebar--%>
        <button data-dojo-type="dijit.form.Button" name="CloseButton" id="CloseButton" type="button" class="pageSubHeaderButton">
          <%=resMgr.getText("common.CloseText",TradePortalConstants.TEXT_BUNDLE)%>
          <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
            document.location.href =
              '<%= formMgr.getLinkAsUrl("goToInvoiceManagement", response) %>';
          </script>
        </button>
        <%=widgetFactory.createHoverHelp("CloseButton", "CloseHoverText") %>
      </span>
      <div style="clear:both;"></div>
    </div>

<form name="<%=formName%>" method="POST" action="<%= formMgr.getSubmitAction(response) %>" style="margin-bottom: 0px;">
  <jsp:include page="/common/ErrorSection.jsp" />
    <div class="formContentNoSidebar">
   <input type=hidden value="" name=buttonName>
	<input type=hidden name="payment_date">
	<input type="hidden" name="loanType" />
	<input type="hidden" name="instrumentType" /> 
	<input type="hidden" name="bankBranch" />
	<input type="hidden" value="yes" name="fromPayDetail">
	<input type=hidden  name="timeZone" value= "<%=timeZone%>">
	<input type=hidden  name="baseCurrencyCode" value = "<%=StringFunction.xssCharsToHtml(userSession.getBaseCurrencyCode())%>">
<%  //AAlubala VASCO reauthentication hidden fields
    if ( InvoiceRequireAuth) {
%> 

      <input type=hidden name="reCertification" value="<%=TradePortalConstants.INDICATOR_NO%>">
      <input type=hidden name="reCertOK">
      <input type=hidden name="logonResponse">
      <input type=hidden name="logonCertificate">

<%
    }
%>

<%
  
  String gridID = "PayableInvoiceGroupDetailID";
	String gridName = "PayableInvoiceGroupDetailDataGrid";
	String viewName = "PayableInvoiceGroupDetailDataView";
	String resourcePrefix = "UploadInvoices";
	
	//SHR print button start
	  String linkArgs[] = {"","","","","",""};
	          String arg = "key~"+viewName+ "`key~" + resourcePrefix;

	  linkArgs[0] = TradePortalConstants.PDF_LIST;
	          linkArgs[1] = EncryptDecrypt.encryptStringUsingTripleDes(arg);//Use common key for encryption of parameter to doc producer
	          linkArgs[2] = InstrumentServices.isNotBlank(resMgr.getResourceLocale())? resMgr.getResourceLocale():"en_US";
	          //loginLocale;
	          linkArgs[3] = userSession.getBrandingDirectory();
	          String linkString =  formMgr.getDocumentLinkAsURLInFrame(resMgr.getText("UploadInvoiceDetail.PrintInvoiceDetails",
	                  TradePortalConstants.TEXT_BUNDLE),
	                  TradePortalConstants.PDF_LIST,
	                  linkArgs,
	                  response) ;

	          //SHR end
%>

<%
    if (InvoiceRequireAuth) {
%>   
  <%@ include file="/logon/fragments/openReauthenticationWindow.frag" %>
  <%
        //NOTE: although there are lists on the screen, this is specific to the pay remit
        authorizeLink = "javascript:openReauthenticationWindowForList('" + certAuthURL + "'," +
            "'" + formName + "','" + TradePortalConstants.BUTTON_AUTHORIZE + "'," +
            "'Invoice')";

        proxyAuthLink = "javascript:openReauthenticationWindowForList('" + certAuthURL + "'," +
	    "'" + formName + "','" + TradePortalConstants.BUTTON_PROXY_AUTHORIZE + "'," +
            "'Invoice')";
    }
    //vasco end
  %> 
<%-- SHR --%>
 <div class="gridSearch">
  <div class="searchHeader">
   <span class="searchHeaderCriteria">
	<%=resMgr.getText("InvoiceList.Invoices",TradePortalConstants.TEXT_BUNDLE)%>

	</span>
 	<span class="searchHeaderActions">
 	<jsp:include page="/common/gridShowCount.jsp">
        <jsp:param name="gridId" value='<%=gridID%>'/>
      </jsp:include>
    <button data-dojo-type="dijit.form.Button" name="print" id="print" type="button" class="pageSubHeaderButton">
          <%=resMgr.getText("common.PrintText",TradePortalConstants.TEXT_BUNDLE)%>
          <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
          window.open( "<%=linkString%>");
		  </script>
        </button>
         <%=widgetFactory.createHoverHelp("print", "PrintHoverText") %>
 	  <span id="payInvoiceGroupListRefresh" class="searchHeaderRefresh"></span>
 	  <%=widgetFactory.createHoverHelp("payInvoiceGroupListRefresh", "RefreshImageLinkHoverText") %>
      <span id="payInvoiceGroupListGridEdit" class="searchHeaderEdit"></span>
      <%=widgetFactory.createHoverHelp("payInvoiceGroupListGridEdit","CustomizeListImageLinkHoverText") %>
    </span>
    <div style="clear:both;"></div>

  <%
  DataGridFactory dgFactory = new DataGridFactory(resMgr, userSession, formMgr,beanMgr, response);
  String gridHtml = dgFactory.createDataGridMultiFooter(gridID,gridName,"5");
 %>
 
 </div>
<%=gridHtml%>

<%= formMgr.getFormInstanceAsInputField(formName, secureParms) %>


</div>
</div>

</form>
</div>
</div>

<%-- <%@ include file="/invoicemanagement/fragments/InvoiceManagement_PayableInvoices.frag"%>--%>
<jsp:include page="/common/Footer.jsp">
  <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
  <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>

<script type="text/javascript" src="/portal/js/datagrid.js"></script>
 <div id="InvoiceGroupListDialogID" ></div>
<%
  String gridLayout = dgFactory.createGridLayout(gridID,gridName);
%>

<script type="text/javascript">

  <%--get grid layout from the data grid factory--%>
  var gridLayout = <%= gridLayout %>;
  
  var gridID = '<%= gridID %>';
  var gridName = '<%= gridName %>';
  var viewName = '<%=EncryptDecrypt.encryptStringUsingTripleDes(viewName,userSession.getSecretKey())%>';  <%--Rel9.2 IR T36000032596 --%>
  var formName = "UploadInvoiceListForm";
  <%--set the initial search parms--%>
<%
  //encrypt selectedOrgOid for the initial, just so it is the same on subsequent
  //search - note that the value in the dropdown is encrypted
  //String encryptedOrgOid = EncryptDecrypt.encryptStringUsingTripleDes(selectedOrg, userSession.getSecretKey());
%>
  var init='userOid=<%=userOid%>&userOrgOid=<%=userOrgOid%>&invoiceGroupOid=<%=StringFunction.xssCharsToHtml(invoiceGroupOid)%>';
  var InvoiceListId = createDataGrid(gridID, viewName, gridLayout, init);

 function performAction(pressedButton){
	    <%--get array of rowkeys from the grid--%>
 var rowKeys = getSelectedGridRowKeys(gridID);
 	    <%--submit the form with rowkeys
	        becuase rowKeys is an array, the parameter name for
	        each will be checkbox + an iteration number -
	        i.e. checkbox0, checkbox1, checkbox2, etc --%>
	 submitFormWithParms(formName, pressedButton, "checkbox", rowKeys);
}
 
 function createApprovalToPay(bankBranchArray){

		<%--get array of rowkeys from the grid--%>
		rowKeys = getSelectedGridRowKeys(gridID);
		var items=getSelectedGridItems(gridID);

		if(rowKeys == ""){
			alert("Please pick a record and click 'Select'");
			return;
		}
		if ( bankBranchArray.length == 1 ) {
	    	createPayableATP(bankBranchArray[0]);
		} else {
			require(["t360/dialog"], function(dialog ) {
		      dialog.open('bankBranchSelectorDialog', '<%=resMgr.getText("bankBranchSelectorDialog.title", TradePortalConstants.TEXT_BUNDLE)%>',
		                  'bankBranchSelectorDialog.jsp',
		                  null, null, //parameters
		                  'select', this.createPayableATP);
		    });
		 }
		}

		function createPayableATP(bankBranchOid) {
			submitInvFormWithParms(bankBranchOid);
		}

		function closePaymentDialog(){
			require(["t360/dialog"], function(dialog) {
				dialog.hide('copyPaymentsDialog');
			});
		}
		
		function submitInvFormWithParms(bankBranchOid) {
			buttonName='InvCreateApprovalToPay';
			parmName="checkbox";
			parmValue = rowKeys;
		      //verify arguments at development time
		      if ( formName == null ) {
		        alert('Problem submitting form: formName required');
		      }
		      if ( buttonName == null ) {
		        alert('Problem submitting form: buttonName required');
		      }
		      if ( parmName == null ) {
		        alert('Problem submitting form: parmName required');
		      }
		  
		      //get the form and submit it
		      if ( document.getElementsByName(formName).length > 0 ) {
		        var myForm = document.getElementsByName(formName)[0];
		        //generate url parameters for the rowKeys and add to 
		        //form as hidden fields
		        if (parmValue instanceof Array) {
		          for (var myParmIdx in parmValue) {
		            var myParmValue = parmValue[myParmIdx];
		            var myParmName = parmName + myParmIdx;
		            addParmToForm(myForm, myParmName, myParmValue);
		          }
		        }
		        else { //assume a single
		        	 addParmToForm(myForm, parmName, parmValue);
		          
		        }
		        myForm.bankBranch.value=bankBranchOid; // Nar IR-T36000015190 Rel 8.2
		        //addParmToForm(myForm, 'bankBranch', bankBranchOid);
		        var formNumber = getFormNumber(formName);
		        setButtonPressed(buttonName, formNumber); 
		        myForm.submit();
		      } else {
		        alert('Problem submitting form: form ' + formName + ' not found');
		      }
		    }

 function applyPaymentDateToInvGroupList(){
 	require(["t360/dialog"], function(dialog) {
 		
 		      dialog.open('InvoiceGroupListDialogID', '<%=resMgr.getText("UploadInvoiceAction.UploadInvoiceApplyPaymentDateText", TradePortalConstants.TEXT_BUNDLE)%>',
		                      'AssignPaymentDate.jsp',
		                      ['selectedCount'], [getSelectedGridRowKeys("PayableInvoiceGroupDetailID").length], //parameters
		                      'select', this.createNewApplyPaymentValue);
		  });
}
 function createNewApplyPaymentValue(payDate) {
	 var theForm = document.getElementsByName(formName)[0];
   	theForm.payment_date.value=payDate;
   	var rowKeys = getSelectedGridRowKeys("PayableInvoiceGroupDetailID");
    submitFormWithParms("UploadInvoiceListForm","InvApplyPaymentDate", "checkbox",rowKeys);
 }

  function assignInstrumentTypeToInvList(){
	 
	    	require(["t360/dialog"], function(dialog) {
		 	      dialog.open('InvoiceGroupListDialogID', '<%=resMgr.getText("UploadInvoiceAction.AssignInstrType", TradePortalConstants.TEXT_BUNDLE)%>',
			                      'AssignInstrumentType.jsp',
			                      ['selectedCount','fromPage'], [getSelectedGridRowKeys("PayableInvoiceGroupDetailID").length,'PL'], //parameters
			                      'select', this.createNewInstrumentTypeDetailValue);
			  }); 
			
 }

function createNewInstrumentTypeDetailValue(instType,loanType) {

	 var theForm = document.getElementsByName(formName)[0];
	theForm.instrumentType.value=instType;
	theForm.loanType.value=loanType; 	
	var rowKeys = getSelectedGridRowKeys("PayableInvoiceGroupDetailID");
	submitFormWithParms("UploadInvoiceListForm","InvAssignInstrType", "checkbox",rowKeys);
}

 function searchInvoiceGroupList() {
	    require(["dojo/dom"],
	      function(dom){
	    	var searchParms = "userOrgOid="+<%=userOrgOid%>+"&invoiceGroupOid="+<%=invoiceGroupOid%>;

	        console.log("SearchParms: " + searchParms);
        searchDataGrid(gridID,viewName,
                searchParms);
	      });
	  }

 require(["dojo/query", "dojo/on", "t360/datagrid", "t360/popup", "dojo/domReady!"],
	      function(query, on, t360grid, t360popup){
	    query('#payInvoiceGroupListRefresh').on("click", function() {
	    	searchInvoiceGroupList();
	    });
   query('#payInvoiceGroupListGridEdit').on("click", function() {
     var columns = t360grid.getColumnsForCustomization(gridID);
     var parmNames = [ "gridId", "gridName", "columns" ];
     var parmVals = [ gridID, gridName, columns ];
     t360popup.open(
       'payInvoiceGroupListGridEdit', 'gridCustomizationPopup.jsp',
       parmNames, parmVals,
       null, null);  //callbacks
   });
  });

//KMehta - 13 Oct 2014 - Rel9.1 IR-T36000033383 - Added domClass 
require(["dojo/query", "dojo/on", "dojo/dom-class","t360/datagrid", "t360/popup", "dijit/registry", "dojo/ready", "dojo/domReady!"],
function(query, on, domClass, t360grid, t360popup, registry,ready) {
	 ready(function() {
		//IR T36000030360: Hide page content, untill widgets get parsed.
			domClass.add("InvoiceMgtDivID", "gridInvoiceFooterDivVisible");
			domClass.remove("InvoiceMgtDivID", "gridInvoiceFooterDivHidden");

		  if(registry.byId("PayableInvoiceGroupDetailID")){
			  var invGrid = registry.byId("PayableInvoiceGroupDetailID");			   
			  on(invGrid,"selectionChanged", function() {
				  if (registry.byId('UploadInvoices_Actions')){
					  t360grid.enableFooterItemOnSelectionGreaterThan(invGrid,"UploadInvoices_Actions",0);
				  }
				  if (registry.byId('UploadInvoices_Document')){
					  t360grid.enableFooterItemOnSelectionGreaterThan(invGrid,"UploadInvoices_Document",0);
				  }
			 });
		 } 
	 });
}); 
 
</script>

<jsp:include page="/common/gridShowCountFooter.jsp">
  <jsp:param name="gridId" value='<%=gridID%>'/>
</jsp:include>

</body>

<%
  // Finally, reset the cached document to eliminate carryover of
  // information to the next visit of this page.
  formMgr.storeInDocCache("default.doc", new DocumentHandler());
%>

</html>