<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved.
--%>
<%--
*******************************************************************************
                        Invoice Management Invoice Groups Tab

  Description:
    Contains HTML to display invoices which are uploaded through upload tab

  This is not a standalone frag.  It MUST be included using the following tag:
  <%@ include file="InvoiceManagement_InvoiceGroups.frag" %>
*******************************************************************************
--%> 

<%
      String searchType        = request.getParameter("SearchType");
      String newDropdownSearch = request.getParameter("NewDropdownSearch");
      StringBuffer statusExtraTags = null;
      String searchListViewName = null;
      String statusSearchParm = null;
      String defaultSearchStatus = null;
      String statusSearchLabel = null;
      String sectionHeader = null;
      boolean isAvailableForFinancing = true;
      String transactionType ="";
      boolean isVascoOTP = false; 
      StringBuffer      sqlQueryStr   = new StringBuffer(); //Vshah - IR#DWUM043037276     
      
      secureParms.put("UserOid", userOid);
      secureParms.put("ownerOrg", userOrgOid);
      secureParms.put("SecurityRights", userSecurityRights);
      session.setAttribute("fromInvoiceGroupDetail", "GL");
      boolean hasApproveRights   = SecurityAccess.hasRights(userSecurityRights, SecurityAccess.RECEIVABLES_APPROVE_FINANCING);
      boolean hasAuthRights      = SecurityAccess.hasRights(userSecurityRights, SecurityAccess.RECEIVABLES_AUTHORIZE_INVOICE);
      boolean hasRemoveInvRights = SecurityAccess.hasRights(userSecurityRights, SecurityAccess.RECEIVABLES_REMOVE_INVOICE);      
      //
      //AAlubala - Proxy Authorize / VASCO
      //
      boolean canProxyAuthorize =SecurityAccess.canProxyAuthorizeInstrument(userSecurityRights, TradePortalConstants.INV_LINKED_INSTR_REC, transactionType);

      helpSensitiveLink = OnlineHelp.createContextSensitiveLink("customer/invoices.htm",  resMgr, userSession);      
%>
<%
////AAlubala - IR#DNUM062245488 -  This will check if a user is  set to use OTP Vasco type - START
//
QueryListView queryListViewUsers = (QueryListView) EJBObjectFactory.createClientEJB(formMgr.getServerLocation(), "QueryListView"); 
sqlQueryStr.append("select auth2fa_subtype from users where user_oid = ?");
queryListViewUsers.setSQL(sqlQueryStr.toString(),new Object[]{userSession.getUserOid()});
queryListViewUsers.getRecords();   
DocumentHandler result = queryListViewUsers.getXmlResultSet();
//String tokenType = result.getAttribute("/ResultSetRecord(0)/TOKEN_TYPE");
String authSubtype = result.getAttribute("/ResultSetRecord(0)/AUTH2FA_SUBTYPE");
if (TradePortalConstants.AUTH_2FA_SUBTYPE_OTP.equals(authSubtype)) isVascoOTP = true;
// 
//AAlubala - IR#DNUM062245488 -   - END

DataGridFactory dgFactory = new DataGridFactory(resMgr, userSession, formMgr,beanMgr, response);
String gridHtmlSummary = "";
%>

<%
      newLink.append(formMgr.getLinkAsUrl("goToInvoiceManagement", response));
      newLink.append("&currentTab=").append(currentTab);
%>


    <%-- Rkazi CR-710 Rel 8.0 02/09/2012 START - Added to call the Summary group listview. --%>
<%
      searchListViewName = "InvoiceGroupSummaryListView.xml";
      statusSearchParm = "invoiceGroupStatusType";
      defaultSearchStatus = TradePortalConstants.STATUS_ALL;
      statusSearchLabel = "InvoiceSearch.GroupedInvoices";
      sectionHeader = "InvoiceGroups.Section1Header";
      

      
%>
<%if(isRecPrgmEnabled){ %>
<%=widgetFactory.createSectionHeader("1", "InvoiceGroups.Section1Header") %>
<%@ include file="/invoicemanagement/fragments/InvoiceManagement_InvoiceGroupsSummaryList.frag"%>
</div>
<%} %>


<%
      searchListViewName = "InvoiceGroupListView.xml";
      statusSearchParm = "invoiceGroupStatusType";
      defaultSearchStatus = TradePortalConstants.STATUS_ALL;
      statusSearchLabel = "InvoiceSearch.GroupedInvoices";
      sectionHeader = "InvoiceGroups.Section2GPHeader";
      isAvailableForFinancing = true;
      codesToExclude = new Vector();
      codesToExclude.add(TradePortalConstants.UPLOAD_INV_STATUS_AVL_FOR_AUTH);
      codesToExclude.add(TradePortalConstants.UPLOAD_INV_STATUS_AVL_FOR_FIN);
      codesToExclude.add(TradePortalConstants.UPLOAD_INV_STATUS_AUTH_FAILED);
      codesToExclude.add(TradePortalConstants.UPLOAD_INV_STATUS_PAR_AUTH);
      codesToExclude.add(TradePortalConstants.UPLOAD_INV_STATUS_AUTH);
      codesToExclude.add(TradePortalConstants.UPLOAD_INV_STATUS_DBC);
      codesToExclude.add(TradePortalConstants.UPLOAD_INV_STATUS_RAA);
      
      gridID = "InvoiceGroupId";
      gridName = "InvoiceGroupDataGrid";
      searchID = "InvoiceGroupSearchID";
      onSearch = "onChange='searchInvoicePending();'";
      dataView = "InvoiceGroupDataView";
      dataRefreshID="invoiceGroupRefresh";
      dataEditID="invoiceGroupEdit";
      String buttonDisplay ="6";
	  if(isRecPrgmEnabled){
%>
		<%=widgetFactory.createSectionHeader("2", "InvoiceGroups.Section2GPHeader") %>
		<%@ include file="/invoicemanagement/fragments/InvoiceManagement_InvoiceGroupsList.frag"%>
		</div>
	<%}%>	



<%-- DK CR709 Rel8.2 Start --%>

<%
if(isLRQEnabled){
	searchListViewName = "InvoiceLoanRequestListView.xml";
	statusSearchParm = "invoiceGroupStatusType";
	defaultSearchStatus = TradePortalConstants.STATUS_ALL;
	statusSearchLabel = "InvoiceSearch.GroupedInvoices";
	sectionHeader = "InvoiceGroups.Section3Header";
	isAvailableForFinancing = true;
	codesToExclude = new Vector();
	codesToExclude.add(TradePortalConstants.UPLOAD_INV_STATUS_AVL_FOR_AUTH);
	//codesToExclude.add(TradePortalConstants.UPLOAD_INV_STATUS_AVL_FOR_FIN);
	codesToExclude.add(TradePortalConstants.UPLOAD_INV_STATUS_AUTH_FAILED);
	codesToExclude.add(TradePortalConstants.UPLOAD_INV_STATUS_PAR_AUTH);
	codesToExclude.add(TradePortalConstants.PO_STATUS_AUTHORISED);
	//codesToExclude.add(TradePortalConstants.UPLOAD_INV_STATUS_PEND_ACT_NO_INSTR_TYPE);
	codesToExclude.add(TradePortalConstants.PAYABLE_UPLOAD_INV_STATUS_ASSIGNED);
	codesToExclude.add(TradePortalConstants.UPLOAD_INV_STATUS_DELETED); 
	codesToExclude.add(TradePortalConstants.PAYABLE_UPLOAD_INV_STATUS_PENDING_ACTION); 
	//codesToExclude.add(TradePortalConstants.PAYABLE_UPLOAD_INV_STATUS_PROCESSED_BY_BANK); //Srinivasu_D IR#T36000017756 Rel8.2 06/04/2013
	codesToExclude.add(TradePortalConstants.PAYABLE_UPLOAD_INV_STATUS_CANCELLED);
	codesToExclude.add(TradePortalConstants.PAYABLE_UPLOAD_INV_STATUS_UNASSIGNED);
	
	gridID = "InvoiceLoanRequestId";
	gridName = "InvoiceLoanRequestDataGrid";
	searchID = "InvoiceLoanRequestSearchID";
	onSearch = "onChange='searchInvoiceLoanRequest();'";
	dataView = "InvoiceLoanRequestDataView";
	dataRefreshID="invoiceLoanRequestRefresh";
	dataEditID="invoiceLoanRequestEdit";
	buttonDisplay ="6";
	if(isRecPrgmEnabled){ 
	%>
		<%=widgetFactory.createSectionHeader("3", "InvoiceGroups.Section3Header") %>
	<%}else{ %>
		<%=widgetFactory.createSectionHeader("1", "InvoiceGroups.SectionLRHeader") %>
	<% } %>	
<%@ include file="/invoicemanagement/fragments/InvoiceManagement_InvoiceGroupsList.frag"%>
</div>

<%-- DK CR709 Rel8.2 End  --%>

<% }
      searchListViewName = "InvoicePendingActionListView.xml";
      statusSearchParm = "invoicePendingActionStatusType";
      defaultSearchStatus = TradePortalConstants.STATUS_ALL;
      statusSearchLabel = "InvoiceSearch.Invoices";
      sectionHeader = "InvoiceGroups.Section4Header";
      isAvailableForFinancing = false;
      codesToExclude = new Vector();
      codesToExclude.add(TradePortalConstants.UPLOAD_INV_STATUS_PEND_ACT_CREDIT_NOTE);
      codesToExclude.add(TradePortalConstants.UPLOAD_INV_STATUS_PEND_ACT_NO_INSTR_TYPE);
      codesToExclude.add(TradePortalConstants.UPLOAD_INV_STATUS_PEND_ACT_UNABLE_TO_FIN);
      codesToExclude.add(TradePortalConstants.UPLOAD_INV_STATUS_AUTH_FAILED);
      codesToExclude.add(TradePortalConstants.UPLOAD_INV_STATUS_PAR_AUTH);
      codesToExclude.add(TradePortalConstants.UPLOAD_INV_STATUS_AUTH);
      codesToExclude.add(TradePortalConstants.UPLOAD_INV_STATUS_DELETED); 
	  codesToExclude.add(TradePortalConstants.PAYABLE_UPLOAD_INV_STATUS_PENDING_ACTION); 
      
      gridID = "InvoicePendingActionId";
      gridName = "InvoicePendingActionDataGrid";
      searchID = "CreditGroupSearchID";
      onSearch = "onChange='searchCreditPending();'";
      dataView = "InvoicePendingActionDataView";
      dataRefreshID= "invCreditGroupRefresh";
      dataEditID= "invCreditGroupEdit";
      buttonDisplay ="6";
      
      if(isRecPrgmEnabled && isLRQEnabled){ %>
<%=widgetFactory.createSectionHeader("4", "InvoiceGroups.Section4Header")%>
<% } else if(isRecPrgmEnabled && !isLRQEnabled){%>
<%=widgetFactory.createSectionHeader("3", "InvoiceGroups.Section3PHeader")%>
<% } else if(!isRecPrgmEnabled && isLRQEnabled){%>
<%=widgetFactory.createSectionHeader("2", "InvoiceGroups.Section2PHeader")%>
<%} else {%>
<%=widgetFactory.createSectionHeader("1", "InvoiceGroups.Section1PHeader")%>
<%} %>
<%@ include file="/invoicemanagement/fragments/InvoiceManagement_InvoiceGroupsList.frag"%>
</div>
