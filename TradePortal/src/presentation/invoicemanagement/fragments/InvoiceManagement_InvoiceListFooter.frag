<%--
*******************************************************************************
  Payment Pending Transactions Footer

  Description: Everything that should be loaded at the end of the page
               should be included here.  This includes non-visible html 
               and javascript that should execute on page load.

*******************************************************************************
--%>
<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
*******************************************************************************
  Payment Pending Transactions Footer

  Description: Everything that should be loaded at the end of the page
               should be included here.  This includes non-visible html 
               and javascript that should execute on page load.

*******************************************************************************
--%>
<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>

<%
  DataGridFactory dgFactory = new DataGridFactory(resMgr, userSession, formMgr,beanMgr, response);
 

//T36000014760 - changed method used to create GridLayout and also removed grid layout that was hardcoded at the page level
	
	
  String gridLayout = dgFactory.createGridLayout("InvoiceUploadListId","InvoiceUploadListDataGrid");	
//String gridLayout = dgFactory.createGridLayout("InvoiceUploadListDataGrid");	
  String liiin =formMgr.getLinkAsHref("goToViewPDF", resMgr.getText("AutoReroute.TextPart4", TradePortalConstants.TEXT_BUNDLE), response);
  DocumentHandler docc =   formMgr.getFromDocCache("ViewPDFDOC");
%>

<script type="text/javascript">

  <%--get grid layout from the data grid factory--%>
  <%-- Following code has been commented as it moved to xml. --%>
  var gridLayout=<%=gridLayout%>;
  <%-- 
var gridLayout = [
		{type: "dojox.grid._CheckBoxSelector"},[
		{name:"rowKey", field:"rowKey", hidden:"true"} ,
		{name:"Trading Partner", field:"TradingPartner", fields:["TradingPartner","TradingPartner_linkUrl"], formatter:t360gridFormatters.formatGridLink, width:"225px"} ,
		{name:"Invoice ID", field:"InvoiceID", width:"75px"} ,
		{name:"CCY", field:"Ccy", width:"50px"} ,
		{name:"Due Date", field:"DueDate", width:"80px"} ,
		{name:"Payment Date", field:"PaymentDate", width:"80px"} ,
		{name:"Credit Note", field:"CreditNote", width:"50px"} ,
		{name:"Invoice Amount", field:"InvoiceAmount", width:"100px", cellClasses:"gridColumnRight"} ,
		{name:"Financed Amount", field:"FinancedAmount", width:"100px", cellClasses:"gridColumnRight"} ,
		{name:"Estimated Interest", field:"EstimatedInterest", width:"100px", cellClasses:"gridColumnRight"} ,
		{name:"Net Finance Amount", field:"NetFinanceAmount", width:"100px", cellClasses:"gridColumnRight"} ,
		{name:"Linked to Instrument", field:"LinkedInstrType", width:"80px"} ,
		{name:"Loan Type", field:"LoanType", width:"50px"} ,
		{name:"Status", field:"Status", field:"Status", width:"140px"} ,
		<%if (TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH.equals(corpOrg.getAttribute("dual_auth_arm_invoice"))) {%>
		{name:"Panel Status", fields:["PanelStatus","TradingPartner","StatusCode","PanelStatus_linkUrl"], formatter:statusFormatter, width:"150px"} ,
		<%}%>
		{name:"View PDF", field:"DocName", fields:["DocName","DocName_linkUrl"], formatter:t360gridFormatters.formatGridLink, width:"100px"} ]];
 --%>
  <%--set the initial search parms--%>
<% 
  //encrypt selectedOrgOid for the initial, just so it is the same on subsequent
  //search - note that the value in the dropdown is encrypted
  //String encryptedOrgOid = EncryptDecrypt.encryptAlphaNumericString(selectedOrg);
%>
  var init='userOid=<%=StringFunction.xssCharsToHtml(userOid)%>&userOrgOid=<%=StringFunction.xssCharsToHtml(userOrgOid)%>';
  var initSearchParms = "";
  var invoiceStatusType =savedSearchQueryData["invoiceStatusType"];
  var formName = "UploadInvoiceListForm";
  var savedSort= savedSearchQueryData["sort"];
  var gridID = "InvoiceUploadListId";
 
 function initParms(){
	 require(["dojo/dom","dijit/registry"],
		      function(dom,registry){	
		 if("" == invoiceStatusType || null == invoiceStatusType || "undefined" == invoiceStatusType)
			 invoiceStatusType=registry.byId('invoiceStatusType').value;	
		 else if("All" !=invoiceStatusType)
			 registry.byId("invoiceStatusType").set('value',invoiceStatusType);
		
		 if("" == invoiceStatusType){
			 invoiceStatusType = 'All';
			}
		 if("" == savedSort || null == savedSort || "undefined" == savedSort){
			 savedSort = 'TradingPartner';
    	 }
		 }); 
	  
	 initSearchParms = init+"&invoiceStatusType="+invoiceStatusType;
	 
 console.log(initSearchParms);
 var viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("InvoiceUploadListDataView",userSession.getSecretKey())%>";  <%--Rel9.2 IR T36000032596 --%>
  <%--create the grid, attaching it to the div id specified in dataGrid.jsp--%>
  var InvoiceUploadListId = createDataGrid("InvoiceUploadListId", viewName, gridLayout, initSearchParms,savedSort);
 }
  function statusFormatter(columnValues){
  	  var gridLink = "";
	  <%--Nar 01 Sep 2013 Rel 8.3.0.0 IR-T36000020252 ADD BEGIN--%>
  	  <%--Modifed IF condtion to add failed status as well and used Constants instead hardcoded value--%>
  	<%--Rel8.3 IR#T36000022756 Removed the status 'Available for Authorisation' --%>
  	  if(columnValues[2] == '<%=TradePortalConstants.UPLOAD_INV_STATUS_FIN_PAR_AUTH%>' ||
    			columnValues[2] == '<%=TradePortalConstants.UPLOAD_INV_STATUS_FIN_AUTH_FAILED%>' || columnValues[2] == '<%=TradePortalConstants.UPLOAD_INV_STATUS_PAR_AUTH%>' ||
	  			columnValues[2] == '<%=TradePortalConstants.UPLOAD_INV_STATUS_AUTH_FAILED%>'){
      <%--Nar 01 Sep 2013 Rel 8.3.0.0 IR-T36000020252 ADD END--%>
  			 var title = "<%=resMgr.getText("Home.PanelAuthTransactions.ViewAuthorisationsDialog.Title", TradePortalConstants.TEXT_BUNDLE)%>"+columnValues[1];
  			 gridLink =  "<a href='javascript:opendialogViewPanelAuths(\""+title+"\",\"InvoiceUploadListId\",\"INV_SUMMARY\");'>"+columnValues[0]+"</a>";
  	  		 return gridLink;
  		}else{
  			return columnValues[0];
  		}
  	}
  
  function searchPending(){
		 invoiceStatusType=dijit.byId("invoiceStatusType").value;
		 if("" == invoiceStatusType){
			 invoiceStatusType = 'All';
			}
		  var searchParms = init+"&invoiceStatusType="+invoiceStatusType;
		  console.log('searchParms='+searchParms); 
	      searchDataGrid("InvoiceUploadListId", "InvoiceUploadListDataView",
	                     searchParms);
	  }
 

  function performAction(pressedButton){
	    var formName = '<%=formName%>';

	    <%--get array of rowkeys from the grid--%>
	    var rowKeys = getSelectedGridRowKeys("InvoiceUploadListId");

	    <%--submit the form with rowkeys
	        becuase rowKeys is an array, the parameter name for
	        each will be checkbox + an iteration number -
	        i.e. checkbox0, checkbox1, checkbox2, etc --%>
	    submitFormWithParms(formName, pressedButton, "checkbox", rowKeys);
  }
  
  function performAuthorization(){
	    var formName = '<%=formName%>';
	    var certAuthURL = '<%=certAuthURL%>';
	    
	    <%if(InvoiceRequireAuth){%>   
	        <%-- IR T36000014931 REL ER 8.1.0.4 BEGIN 	       --%>
	    	openReauthenticationWindowForList(certAuthURL, formName, "ListAuthorizeInvoices" , "Invoice","InvoiceUploadListId");	     
		<%-- IR T36000014931 REL ER 8.1.0.4 END --%>
	    <%}else{%>
	    
	    <%--get array of rowkeys from the grid--%>
	    var rowKeys = getSelectedGridRowKeys("InvoiceUploadListId");

	    <%--submit the form with rowkeys
	        becuase rowKeys is an array, the parameter name for
	        each will be checkbox + an iteration number -
	        i.e. checkbox0, checkbox1, checkbox2, etc --%>
	    submitFormWithParms(formName, "ListAuthorizeInvoices", "checkbox", rowKeys);
       <%}%>
  }
  <%--  #  KMEhta Rel8400 Fix for IR T36000023304 on 02/01/2014 Start  --%>
  function performInvoiceOfflineAuthorizationAction(){
	  openReauthenticationWindowForList(certAuthURL, formName, "ProxyAuthorizeInvoices", "Invoice","InvoiceUploadListId");
  }
  <%--  #  KMEhta Rel8400 Fix for IR T36000023304 on 02/01/2014 End  --%>
  
  function applyPaymentDateToInvList(){
	 
	    	require(["t360/dialog"], function(dialog) {
		 	      dialog.open('InvoiceGroupListDialogID', '<%=resMgr.getText("UploadInvoiceAction.UploadInvoiceApplyPaymentDateText", TradePortalConstants.TEXT_BUNDLE)%>',
			                      'AssignPaymentDate.jsp',
			                      ['selectedCount'], [getSelectedGridRowKeys("InvoiceUploadListId").length], 
			                      'select', this.createNewApplyPaymentValue);
			  }); 
 }
  function createNewApplyPaymentValue(payDate) {
		var theForm = document.getElementsByName(formName)[0];
		theForm.payment_date.value=payDate;
		var rowKeys = getSelectedGridRowKeys("InvoiceUploadListId");
		submitFormWithParms(formName,"ApplyPaymentDate", "checkbox",rowKeys);		
	}
  
  function searchInvoices() {
	    require(["dojo/dom"],
	      function(dom){

	    	var invoiceStatus = dijit.byId("invoiceStatusType").attr('value');
	 
	        <%-- var filterText=filterTextCS.toUpperCase(); --%>
	        var searchParms = init+"&invoiceStatusType="+invoiceStatusType;
	        console.log("SearchParms: " + searchParms);
	        searchDataGrid("InvoiceUploadListId", "InvoiceUploadListDataView",
	                       searchParms);
	      });
	  }

	require(["dojo/query", "dojo/on", "t360/datagrid", "t360/popup", "dojo/ready", "dojo/domReady!"],
			 function(query, on, t360grid, t360popup, ready ) {
		 ready(function() {
		
		initParms();
		    query('#invoiceUploadListRefresh').on("click", function() {
		    	searchInvoices();
		    });
	  query('#invoiceUploadListGridEdit').on("click", function() {
	    var columns = t360grid.getColumnsForCustomization('InvoiceUploadListId');
	    var parmNames = [ "gridId", "gridName", "columns" ];
	    var parmVals = [ "InvoiceUploadListId", "InvoiceUploadListDataGrid", columns ];
	    t360popup.open(
	      'invoiceUploadListGridEdit', 'gridCustomizationPopup.jsp',
	      parmNames, parmVals,
	      null, null);  <%-- callbacks --%>
	  });
		 });
	 });

 function assignInstrumentTypeToInvList(){
	 
	    	require(["t360/dialog"], function(dialog) {
		 	      dialog.open('InvoiceGroupListDialogID', '<%=resMgr.getText("UploadInvoiceAction.AssignInstrType", TradePortalConstants.TEXT_BUNDLE)%>',
			                      'AssignInstrumentType.jsp',
			                      ['selectedCount'], [getSelectedGridRowKeys("InvoiceUploadListId").length], 
			                      'select', this.createNewInstrumentTypeValue);
			  }); 
 }
 <%-- Callback method for Instrument Type - for Detail invoices --%>
function createNewInstrumentTypeValue(instType,loanType) {

	 var theForm = document.getElementsByName(formName)[0];
   	theForm.instrumentType.value=instType;
	theForm.loanType.value=loanType;
   	var rowKeys = getSelectedGridRowKeys("InvoiceUploadListId");
    submitFormWithParms("UploadInvoiceListForm","ListAssignInstrType", "checkbox",rowKeys);
 }

   <%-- LoanType Action  --%>
 function assignLoanTypeToInvList(){		 
  	require(["t360/dialog"], function(dialog) {
	 	      dialog.open('InvoiceGroupListDialogID', '<%=resMgr.getText("UploadInvoiceAction.AssignLoanType", TradePortalConstants.TEXT_BUNDLE)%>',
		                      'AssignLoanType.jsp',
		                      ['selectedCount'], [getSelectedGridRowKeys("InvoiceUploadListId").length], 
		                      'select', this.createNewLoanTypeValueToLoanReqInvGroup);
		  }); 
 }
   <%-- Loan Req - LoanType action CallBack method --%>
 function createNewLoanTypeValueToLoanReqInvGroup(userLoanType) {	
	
	var theForm = document.getElementsByName(formName)[0];	
	theForm.loanType.value=userLoanType;	
	var rowKeys = getSelectedGridRowKeys("InvoiceUploadListId");
	submitFormWithParms("ApplyPaymentDateToSelectionForm","AssignLoanType", "checkbox",rowKeys);
	
 }

 function createLoanReqByRulesInvList(){
	 
	    	require(["t360/dialog"], function(dialog) {
		 	      dialog.open('InvoiceGroupListDialogID', '<%=resMgr.getText("UploadInvoices.CreateLoanReqByRules", TradePortalConstants.TEXT_BUNDLE)%>',
			                      'CreateLoanRequestByRulesDialog.jsp',
			                      ['selectedCount'], [getSelectedGridRowKeys("InvoiceUploadListId").length], 
			                      null, null);
			  }); 
 } 

function createInvLRCreateLRQ(bankBranchArray){	
	  rowKeys = getSelectedGridRowKeys(gridID);
		var items=getSelectedGridItems(gridID);

		if(rowKeys == ""){
			alert("Please pick a record and click 'Select'");
			return;
		}
		if ( bankBranchArray.length == 1 ) {
			createNewLoanRequestInstValue(bankBranchArray[0]);
		} else {
			require(["t360/dialog"], function(dialog ) {
		      dialog.open('InvoiceGroupListDialogID', '<%=resMgr.getText("bankBranchSelectorDialog.title", TradePortalConstants.TEXT_BUNDLE)%>',
		                  'bankBranchSelectorDialog.jsp',
		                  null, null, <%-- parameters --%>
		                  'select', this.createNewLoanRequestInstValue);
		    });
		 }
}
 function submitInvFormWithParms(bankBranchOid) {
		buttonName='ListCreateLRQ';
		parmName="checkbox";
		parmValue = rowKeys;
	      <%-- verify arguments at development time --%>
	      if ( formName == null ) {
	        alert('Problem submitting form: formName required');
	      }
	      if ( buttonName == null ) {
	        alert('Problem submitting form: buttonName required');
	      }
	      if ( parmName == null ) {
	        alert('Problem submitting form: parmName required');
	      }
	  
	      <%-- get the form and submit it --%>
	      if ( document.getElementsByName(formName).length > 0 ) {
	        var myForm = document.getElementsByName(formName)[0];
	        <%-- generate url parameters for the rowKeys and add to  --%>
	        <%-- form as hidden fields --%>
	        if (parmValue instanceof Array) {
	          for (var myParmIdx in parmValue) {
	            var myParmValue = parmValue[myParmIdx];
	            var myParmName = parmName + myParmIdx;
	            addParmToForm(myForm, myParmName, myParmValue);
	          }
	        }
	        else { <%-- assume a single --%>
	        	 addParmToForm(myForm, parmName, parmValue);
	          
	        }
	        myForm.bankBranch.value=bankBranchOid; 
	        var formNumber = getFormNumber(formName);
	        setButtonPressed(buttonName, formNumber); 
	        myForm.submit();
	      } else {
	        alert('Problem submitting form: form ' + formName + ' not found');
	      }
	    }
  
function createNewLoanRequestInstValue(bankBranchOid) {	
	
	submitInvFormWithParms(bankBranchOid);

}

 <%-- Srinivasu_D CR-709 Rel 8.2 02/02/2013 End --%>

	require(["dojo/dom-class", "dojo/query", "dojo/on", "t360/datagrid", "t360/popup", "dijit/registry", "dojo/ready", "dojo/domReady!"],
			 function(domClass, query, on, t360grid, t360popup, registry,ready) {
		 ready(function() {
			 
			 	<%-- IR T36000030360: Hide page content, untill widgets get parsed. --%>
				domClass.add("InvoiceMgtDivID", "gridInvoiceFooterDivVisible");
				domClass.remove("InvoiceMgtDivID", "gridInvoiceFooterDivHidden");
				
			if(registry.byId("InvoiceUploadListId")){
				  var invGrid = registry.byId("InvoiceUploadListId");			   
				  on(invGrid,"selectionChanged", function() {
					  if (registry.byId('UploadInvoices_Instruments')){
				          t360grid.enableFooterItemOnSelectionGreaterThan(invGrid,"UploadInvoices_Instruments",0);
					  }
					  if (registry.byId('UploadInvoices_Dates')){
						  t360grid.enableFooterItemOnSelectionGreaterThan(invGrid,"UploadInvoices_Dates",0);
					  }
					  if (registry.byId('UploadInvoices_Financing')){
						  t360grid.enableFooterItemOnSelectionGreaterThan(invGrid,"UploadInvoices_Financing",0);
					  }			  
					  if (registry.byId('UploadInvoices_Document')){
				          t360grid.enableFooterItemOnSelectionGreaterThan(invGrid,"UploadInvoices_Document",0);
					  }
					  if (registry.byId('UploadInvoices_Other')){
						  t360grid.enableFooterItemOnSelectionGreaterThan(invGrid,"UploadInvoices_Other",0);
					  }
					  if (registry.byId('UploadInvoices_Authorize')){
						  t360grid.enableFooterItemOnSelectionGreaterThan(invGrid,"UploadInvoices_Authorize",0);
					  }				  
				 });
			}
		 });
	 }); 
</script>  

</script>
