<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
**********************************************************************************
                        Invoice Management List Invoice Tab

  Description:
    Contains HTML to display incoices which are uploaded through upload tab

  This is not a standalone frag.  It MUST be included using the following tag:
  <%@ include file="InvoiceManagement_invoices.frag" %>
*******************************************************************************
--%> 

<%
      String searchType                   = StringFunction.xssCharsToHtml(request.getParameter("SearchType"));
      boolean hasApproveFinancingRights   = false;
      boolean hasAuthInvRights            = false;
      boolean hasRemoveInvRights          = false;
      boolean hasDeleteInvRights          = false;
      StringBuffer statusExtraTags        = new StringBuffer();
      String defaultSearchStatus          = TradePortalConstants.STATUS_ALL;
      String linkArgs[] = {"","","","","",""};
      
      secureParms.put("UserOid", userOid); 
      secureParms.put("ownerOrg", userOrgOid); 
      secureParms.put("SecurityRights", userSecurityRights);
     
      String newDropdownSearch  = request.getParameter("NewDropdownSearch");      
      hasApproveFinancingRights = SecurityAccess.hasRights(userSecurityRights, SecurityAccess.RECEIVABLES_APPROVE_FINANCING);
      hasAuthInvRights          = SecurityAccess.hasRights(userSecurityRights, SecurityAccess.RECEIVABLES_AUTHORIZE_INVOICE);
      hasRemoveInvRights        = SecurityAccess.hasRights(userSecurityRights, SecurityAccess.RECEIVABLES_REMOVE_INVOICE);
      hasDeleteInvRights        = SecurityAccess.hasRights(userSecurityRights, SecurityAccess.RECEIVABLES_DELETE_INVOICE);      
      
      codesToExclude = new Vector();
	 codesToExclude.add(TradePortalConstants.UPLOAD_INV_STATUS_AVL_FOR_AUTH);
	 codesToExclude.add(TradePortalConstants.UPLOAD_INV_STATUS_AVL_FOR_FIN);
	 codesToExclude.add(TradePortalConstants.UPLOAD_INV_STATUS_AUTH_FAILED);
	 codesToExclude.add(TradePortalConstants.UPLOAD_INV_STATUS_PAR_AUTH);
	 codesToExclude.add(TradePortalConstants.UPLOAD_INV_STATUS_AUTH); 
     codesToExclude.add(TradePortalConstants.PAYABLE_UPLOAD_INV_STATUS_ASSIGNED);
	 codesToExclude.add(TradePortalConstants.UPLOAD_INV_STATUS_PEND_ACT_CREDIT_NOTE); 
	 codesToExclude.add(TradePortalConstants.UPLOAD_INV_STATUS_PEND_ACT_NO_INSTR_TYPE); 
	 codesToExclude.add(TradePortalConstants.UPLOAD_INV_STATUS_PEND_ACT_UNABLE_TO_FIN);
	 codesToExclude.add(TradePortalConstants.PAYABLE_UPLOAD_INV_STATUS_PENDING_ACTION);
	// codesToExclude.add(TradePortalConstants.PAYABLE_UPLOAD_INV_STATUS_PROCESSED_BY_BANK); //Srinivasu_D IR#T36000017756 Rel8.2 06/04/2013
	 codesToExclude.add(TradePortalConstants.PAYABLE_UPLOAD_INV_STATUS_CANCELLED);
	// codesToExclude.add(TradePortalConstants.PAYABLE_UPLOAD_INV_STATUS_REL_BY_BANK);
	 codesToExclude.add(TradePortalConstants.UPLOAD_INV_STATUS_DELETED);
	 codesToExclude.add(TradePortalConstants.UPLOAD_INV_STATUS_DBC);
	 codesToExclude.add(TradePortalConstants.UPLOAD_INV_STATUS_RAA);
     codesToExclude.add(TradePortalConstants.PAYABLE_UPLOAD_INV_STATUS_UNASSIGNED);
	
	 session.removeAttribute("invoiceStatusType");
	     selectedStatus = request.getParameter("invoiceStatusType");
      if (selectedStatus == null) 
      {
      	 if (null == session.getAttribute("invoiceStatusType") || ("").equals(session.getAttribute("invoiceStatusType"))) {
            selectedStatus = defaultSearchStatus;
         } else {
            selectedStatus = (String) session.getAttribute("invoiceStatusType");
         }
      }
      
      session.setAttribute("invoiceStatusType", selectedStatus);
      
       dynamicWhereClause.append(" and i.a_corp_org_oid = ").append(userOrgOid);
   /*   if (groupingEnabled) {
         dynamicWhereClause.append(" and i.a_invoice_group_oid = ").append(invoiceGroupOid);
         session.setAttribute("fromInvoiceGroupDetail", TradePortalConstants.INDICATOR_YES);
      }*/
      session.setAttribute("fromInvoiceGroupDetail", "NGL");
      
      if (!TradePortalConstants.STATUS_ALL.equals(selectedStatus)) {
         dynamicWhereClause.append(" and (i.invoice_status = '");
         dynamicWhereClause.append(selectedStatus);
         dynamicWhereClause.append("'");
        
         dynamicWhereClause.append(")");
      } 
                     
   // Build the status dropdown options 
   // re-selecting the most recently selected option.
   
   dropdownOptions = new StringBuffer(Dropdown.createSortedRefDataIncludeOptions("UPLOAD_INVOICE_STATUS",
                                             selectedStatus, resMgr,loginLocale, codesToExclude,false));
        
   //SHR print button start
	String viewName = "InvoiceUploadListDataView";
	String resourcePrefix = "UploadInvoices";
    String arg = "key~"+viewName+ "`key~" + resourcePrefix;
       
		linkArgs[0] = TradePortalConstants.PDF_LIST;
        linkArgs[1] = EncryptDecrypt.encryptStringUsingTripleDes(arg);//Use common key for encryption of parameter to doc producer
        linkArgs[2] = InstrumentServices.isNotBlank(resMgr.getResourceLocale())? resMgr.getResourceLocale():"en_US";
        //loginLocale;
        linkArgs[3] = userSession.getBrandingDirectory();
        String linkString =  formMgr.getDocumentLinkAsURLInFrame(resMgr.getText("UploadInvoiceDetail.PrintInvoiceDetails",
                TradePortalConstants.TEXT_BUNDLE),
                TradePortalConstants.PDF_LIST,
                linkArgs,
                response) ;
        
        //SHR end
%>
   
 <div class="gridSearch">
  <div class="searchHeader">  
   <span class="searchHeaderCriteria">
 	
<%                                               
        out.print(widgetFactory.createSearchSelectField("invoiceStatusType", "InvoiceSearch.Show",
        		resMgr.getText("common.StatusAll",TradePortalConstants.TEXT_BUNDLE),
                                               dropdownOptions.toString(), "onChange='searchPending();'"));  

%>
   </span>
	<span class="searchHeaderActions">
	  <jsp:include page="/common/gridShowCount.jsp">
        <jsp:param name="gridId" value="InvoiceUploadListId" />
      </jsp:include>
      <button data-dojo-type="dijit.form.Button" name="print" id="print" type="button" class="pageSubHeaderButton">
          <%=resMgr.getText("common.PrintText",TradePortalConstants.TEXT_BUNDLE)%>
           <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
          window.open( "<%=linkString%>");
		  </script>
       </button> 
      <%=widgetFactory.createHoverHelp("print", "PrintHoverText") %> 
 	  <span id="invoiceUploadListRefresh" class="searchHeaderRefresh"></span>
 	  <%=widgetFactory.createHoverHelp("invoiceUploadListRefresh", "RefreshImageLinkHoverText") %>
      <span id="invoiceUploadListGridEdit" class="searchHeaderEdit"></span>
      <%=widgetFactory.createHoverHelp("invoiceUploadListGridEdit","CustomizeListImageLinkHoverText") %> 
      
    </span>
    <div style="clear:both;"></div>

  <input type=hidden name=SearchType value="<%= searchType %>">
  <input type=hidden name="payment_date">
  
  <%-- BSL IR NLUM040376572 04/06/2012 BEGIN - Add title and customButton parameters --%>
   <%
   DataGridFactory dgFactory = new DataGridFactory(resMgr, userSession, formMgr,beanMgr, response);
  String gridHtml = dgFactory.createDataGridMultiFooter("InvoiceUploadListId","InvoiceUploadListDataGrid","7");
  String gridLayout = dgFactory.createGridLayout("InvoiceUploadListId", "InvoiceUploadListDataGrid");

%>
 </div>

<%=gridHtml%>
</div>
