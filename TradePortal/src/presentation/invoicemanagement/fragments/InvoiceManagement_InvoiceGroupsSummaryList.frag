<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
  <%--  Rkazi CR-710 Rel 8.0 02/09/2012 START - Added to call the Summary group listview. --%>
<%--

*******************************************************************************
                        Invoice Management Invoice Groups List

  Description:
    Contains HTML to the list views that appear on the Invoice Groups tab

  This is not a standalone frag.  It MUST be included using the following tag:
  <%@ include file="InvoiceManagement_InvoiceGroupsSummaryList.frag" %>
*******************************************************************************
--%> 
<%
      dynamicWhereClause = new StringBuffer();
      dynamicWhereClause.append(" ").append(userOrgOid);
      dynamicWhereClause.append(" ) GROUP BY Ccy ");

%>


  		 <%--  <jsp:include page="/common/TwoColumnListView.jsp">
		      <jsp:param name="listView"       value='<%= searchListViewName %>' />       
		      <jsp:param name="userType"       value='<%= userSecurityType %>' />
		      <jsp:param name="whereClause2"   value='<%= dynamicWhereClause.toString() %>' />
		      <jsp:param name="maxScrollHeight" value="112"/>
		      <jsp:param name="sortColumn" value="1"/>
		      
		  </jsp:include>--%>
		  
<%
   gridHtmlSummary = dgFactory.createDataGrid("InvoiceGroupSummaryId","InvoiceGroupSummaryDataGrid",null);
%>

<%=gridHtmlSummary%>
