<%--
*******************************************************************************
  Payment Pending Transactions Footer

  Description: Everything that should be loaded at the end of the page
               should be included here.  This includes non-visible html 
               and javascript that should execute on page load.

*******************************************************************************
--%>
<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
*******************************************************************************
  Payment Pending Transactions Footer

  Description: Everything that should be loaded at the end of the page
               should be included here.  This includes non-visible html 
               and javascript that should execute on page load.

*******************************************************************************
--%>
<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>

<%
  DataGridFactory dgFactory = new DataGridFactory(resMgr, userSession, formMgr,beanMgr, response);
  DGridFactory dgridFactory = new DGridFactory(resMgr, userSession, formMgr,beanMgr, response);
  String gridInvoiceGroupSummaryLayout = dgridFactory.createGridLayout("InvoiceGroupSummaryId","InvoiceGroupSummaryDataGrid");	
  String gridInvoiceGroupLayout = dgFactory.createGridLayout("InvoiceGroupId","InvoiceGroupDataGrid");
  String gridInvoicePendingActionLayout = dgFactory.createGridLayout("InvoicePendingActionId","InvoicePendingActionDataGrid");
  // DK CR709
  String gridInvoiceLoanRequestLayout = dgFactory.createGridLayout("InvoiceLoanRequestId","InvoiceLoanRequestDataGrid");
%>

<script type="text/javascript">
<%-- IR 16481 start --%>
var savedSearchQueryDataG = {
				<% 
				if(savedSearchQueryDataG !=null && !savedSearchQueryDataG.isEmpty()) {
					Iterator<Map.Entry<String, String>> entries = savedSearchQueryDataG.entrySet().iterator();
				while (entries.hasNext()) {
					Map.Entry<String, String> entry = entries.next(); 
					%>
				'<%=entry.getKey()%>':'<%=entry.getValue()%>'
					<%if(entries.hasNext()) {%>
					<%=comma%>
					
				<%
					}
				}
				}%>
	}
var savedSearchQueryDataL = {
		<% 
		if(savedSearchQueryDataL !=null && !savedSearchQueryDataL.isEmpty()) {
			Iterator<Map.Entry<String, String>> entries = savedSearchQueryDataL.entrySet().iterator();
		while (entries.hasNext()) {
			Map.Entry<String, String> entry = entries.next(); 
			%>
		'<%=entry.getKey()%>':'<%=entry.getValue()%>'
			<%if(entries.hasNext()) {%>
			<%=comma%>
			
		<%
			}
		}
		}%>
}
var savedSearchQueryDataC = {
		<% 
		if(savedSearchQueryDataC !=null && !savedSearchQueryDataC.isEmpty()) {
			Iterator<Map.Entry<String, String>> entries = savedSearchQueryDataC.entrySet().iterator();
		while (entries.hasNext()) {
			Map.Entry<String, String> entry = entries.next(); 
			%>
		'<%=entry.getKey()%>':'<%=entry.getValue()%>'
			<%if(entries.hasNext()) {%>
			<%=comma%>
			
		<%
			}
		}
		}%>
}
<%-- IR 16481 end --%>

<% 
  //encrypt selectedOrgOid for the initial, just so it is the same on subsequent
  //search - note that the value in the dropdown is encrypted
  //String encryptedOrgOid = EncryptDecrypt.encryptAlphaNumericString(selectedOrg);
%>
  var init='userOrgOid=<%=userOrgOid%>';
  var formName = "InvoiceGroupListForm";
  var initInvoiceSearchParms = "";
  var initInvoiceSearchParmsL = "";
  var invoiceStatusType = savedSearchQueryDataG["selectedStatus"];
  var initCreditSearchParms = "";
  var invoiceStatusTypeL = savedSearchQueryDataL["selectedStatus"]; 
  var creditStatusType = savedSearchQueryDataC["selectedStatus"];
  var certAuthURL = '<%=certAuthURL%>';
  var invoiceRequireAuth = '<%=InvoiceRequireAuth%>';
  var savedSortL = savedSearchQueryDataL["sort"];
  var savedSortC = savedSearchQueryDataC["sort"];
  var savedSortG = savedSearchQueryDataG["sort"];
  function initParms(){
	  require(["dojo/dom","dijit/registry"],
		      function(dom,registry){	
		 <%if(isRecPrgmEnabled){%> 
		 if("" == invoiceStatusType || null == invoiceStatusType || "undefined" == invoiceStatusType)
			 invoiceStatusType=registry.byId("InvoiceGroupSearchID").value;	
		 else{
			 if("All" !=invoiceStatusType)
	 		 registry.byId("InvoiceGroupSearchID").set('value',invoiceStatusType);
		 }
		 if("" == invoiceStatusType){
			 invoiceStatusType = 'All';
			}
		 <%}%>
		 
		 if("" == creditStatusType || null == creditStatusType || "undefined" == creditStatusType)
			 creditStatusType=registry.byId('CreditGroupSearchID').value;
		 else{
			 if("All" !=creditStatusType)
	 		 registry.byId("CreditGroupSearchID").set('value',creditStatusType);
		 }
		 if("" == creditStatusType){
			 creditStatusType = 'All';
			}
		 <%if(isLRQEnabled){%> 
		 if("" == invoiceStatusTypeL || null == invoiceStatusTypeL || "undefined" == invoiceStatusTypeL)
			 invoiceStatusTypeL=registry.byId('InvoiceLoanRequestSearchID').value;
		 else{
			 if("All" !=invoiceStatusTypeL)
	 		 registry.byId("InvoiceLoanRequestSearchID").set('value',invoiceStatusTypeL);
		 }
		 if("" == invoiceStatusTypeL){
			 invoiceStatusTypeL = 'All';
			}
		 <%}%>
		 }); 
	  
	 
	 if("" == savedSortL || null == savedSortL || "undefined" == savedSortL){
		 savedSortL = 'TradingPartner';
	 }
	 
	 if("" == savedSortG || null == savedSortG || "undefined" == savedSortG){
		 savedSortG = 'TradingPartner';
	 }
	 if("" == savedSortC || null == savedSortC || "undefined" == savedSortC){
		 savedSortC = 'TradingPartner';
	 }
	 initInvoiceSearchParms = init+"&selectedStatus="+invoiceStatusType;
	 initCreditSearchParms = init+"&selectedStatus="+creditStatusType;
	 initInvoiceSearchParmsL =init+"&selectedStatus="+invoiceStatusTypeL;
  
  <%--create the grid, attaching it to the div id specified in dataGrid.jsp--%>
  require(["t360/OnDemandGrid", "dojo/dom-construct"], function(onDemandGrid, domConstruct){ 
	  <%--get grid layout from the data grid factory--%>
	  var gridInvoiceGroupSummaryLayout = <%= gridInvoiceGroupSummaryLayout %>;
	  var gridInvoiceGroupLayout;
	  <%if(isRecPrgmEnabled) { %> 
	  	gridInvoiceGroupSummaryLayout = <%= gridInvoiceGroupSummaryLayout %>;
	  	gridInvoiceGroupLayout =<%= gridInvoiceGroupLayout %>;
	  <%} %>	
	    <%-- DK CR709 --%>
	     var gridInvoiceLoanRequestLayout;
	     <%if(isLRQEnabled){%>
	   gridInvoiceLoanRequestLayout = <%= gridInvoiceLoanRequestLayout %>;
	  <%--set the initial search parms--%>
	  <%}%>
	  <%-- CR 821 Rel 8.3 START--%>
		var gridInvoicePendingActionLayout= <%=gridInvoicePendingActionLayout%>;
	 
	  
	  <%-- CR 821 Rel 8.3 END--%>
  var viewName = "";
  viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("InvoiceGroupSummaryDataView",userSession.getSecretKey())%>";  <%--Rel9.2 IR T36000032596 --%>
  require(["t360/OnDemandGrid", "dojo/dom-construct"], function(onDemandGrid, domConstruct){
  	var InvoiceGroupSummaryId =  onDemandGrid.createOnDemandGrid("InvoiceGroupSummaryId", viewName, 
  			gridInvoiceGroupSummaryLayout, init,'0'); 
  });
 
  <%if(isRecPrgmEnabled){ %>
  		viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("InvoiceGroupDataView",userSession.getSecretKey())%>";  <%--Rel9.2 IR T36000032596 --%>
  		var InvoiceGroupId = createDataGrid("InvoiceGroupId", viewName, gridInvoiceGroupLayout, initInvoiceSearchParms,savedSortG);
  <%} %>		
  
  viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("InvoicePendingActionDataView",userSession.getSecretKey())%>";  <%--Rel9.2 IR T36000032596 --%>
  var InvoicePendingActionId = createDataGrid("InvoicePendingActionId", viewName, gridInvoicePendingActionLayout, initCreditSearchParms,savedSortC);
  <%if(isLRQEnabled){%>
  viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("InvoiceLoanRequestDataView",userSession.getSecretKey())%>";  <%--Rel9.2 IR T36000032596 --%>
  var InvoiceLoanRequestId = createDataGrid("InvoiceLoanRequestId", viewName, gridInvoiceLoanRequestLayout, initInvoiceSearchParmsL,savedSortL);
  <%}%>
  
  });
  }
  
  function statusFormatter(columnValues){
  	  var gridLink = "";
  	  <%--Nar 01 Sep 2013 Rel 8.3.0.0 IR-T36000020252 ADD BEGIN--%>
  	  <%--Modifed IF condtion to add failed status as well and used constants--%>
  	  <%--Rel8.3 IR#T36000022756 Removed the status 'Available for Authorisation' --%>
  	  if(columnValues[2] == '<%=TradePortalConstants.UPLOAD_INV_STATUS_FIN_PAR_AUTH%>' ||
  			columnValues[2] == '<%=TradePortalConstants.UPLOAD_INV_STATUS_FIN_AUTH_FAILED%>'){
  	  <%--Nar 01 Sep 2013 Rel 8.3.0.0 IR-T36000020252 ADD END--%>
  			 var title = "<%=resMgr.getText("Home.PanelAuthTransactions.ViewAuthorisationsDialog.Title", TradePortalConstants.TEXT_BUNDLE)%>"+columnValues[1];
  			 gridLink =  "<a href='javascript:opendialogViewPanelAuths(\""+title+"\",\"InvoiceGroupId\",\"INV_GROUP\");'>"+columnValues[0]+"</a>";
  	  		 return gridLink;
  		}else{
  			return columnValues[0];
  		}
  	}
    
    function creditStatusFormatter(columnValues){
    	  var gridLink = "";
    	  <%--Nar 01 Sep 2013 Rel 8.3.0.0 IR-T36000020252 ADD BEGIN--%>
      	  <%--Modifed IF condtion to add failed status as well and used Constants instead hardcoded value--%>
      	  <%--Rel8.3 IR#T36000022756 Removed the status 'Available for Authorisation' --%>
    		if(columnValues[2] == '<%=TradePortalConstants.UPLOAD_INV_STATUS_PAR_AUTH%>' ||
    	  			columnValues[2] == '<%=TradePortalConstants.UPLOAD_INV_STATUS_AUTH_FAILED%>'){
    	  <%--Nar 01 Sep 2013 Rel 8.3.0.0 IR-T36000020252 ADD END--%>
    			 var title = "<%=resMgr.getText("Home.PanelAuthTransactions.ViewAuthorisationsDialog.Title", TradePortalConstants.TEXT_BUNDLE)%>"+columnValues[1];
    			 gridLink =  "<a href='javascript:opendialogViewPanelAuths(\""+title+"\",\"InvoicePendingActionId\",\"INV_GROUP\");'>"+columnValues[0]+"</a>";
    	  		 return gridLink;
    		}else{
    			return columnValues[0];
    		}
    	}
  function searchInvoicePending(){
	  require(["dojo/dom"],
		      function(dom){

		 invoiceStatusType=dijit.byId("InvoiceGroupSearchID").attr('value'); 
		 if("" == invoiceStatusType){
			 invoiceStatusType = 'All';
			}
		  var searchParms = init+"&selectedStatus="+invoiceStatusType;
		  console.log('searchParms='+searchParms); 
	      searchDataGrid("InvoiceGroupId", "InvoiceGroupDataView",
	                     searchParms);
	  });
	 }
  
  function searchCreditPending(){
	     creditStatusType=dijit.byId("CreditGroupSearchID").value;
		 if("" == creditStatusType){
			 creditStatusType = 'All';
			}
		  var searchParms = init+"&selectedStatus="+creditStatusType;
		  console.log('searchParms='+searchParms); 
	      searchDataGrid("InvoicePendingActionId", "InvoicePendingActionDataView",
	                     searchParms);
	 }
  
  <%--  DK CR709 Start --%>
  
  function searchInvoiceLoanRequest(){
	  require(["dojo/dom"],
		      function(dom){
		  
	  invoiceLRStatusType=dijit.byId("InvoiceLoanRequestSearchID").value;
		 if("" == invoiceLRStatusType){
			 invoiceLRStatusType = 'All';
			}
		  var searchParms = init+"&selectedStatus="+invoiceLRStatusType;
		  console.log('searchParms='+searchParms); 
	      searchDataGrid("InvoiceLoanRequestId", "InvoiceLoanRequestDataView",
	                     searchParms);
	  });
	 }
  
  <%--  DK CR709 End --%>
  
  function performInvoiceGroupAction(pressedButton){
	 
	    <%--get array of rowkeys from the grid--%>
  var rowKeys = getSelectedGridRowKeys("InvoiceGroupId");
	if("AttachDocument"==(pressedButton) || "DeleteDocument"==(pressedButton)){
		if("" == rowKeys){
		rowKeys = getSelectedGridRowKeys("InvoicePendingActionId");
		}
	}
	if("ClearPaymentDate"==(pressedButton)){
		if("" == rowKeys){
		rowKeys = getSelectedGridRowKeys("InvoiceLoanRequestId");
		}
	}
	
	    <%--submit the form with rowkeys
	        becuase rowKeys is an array, the parameter name for
	        each will be checkbox + an iteration number -
	        i.e. checkbox0, checkbox1, checkbox2, etc --%>
	 submitFormWithParms(formName, pressedButton, "checkbox", rowKeys);
}
  
  function performCreditGroupAction(pressedButton){
	    <%--get array of rowkeys from the grid--%>
   var rowKeys = getSelectedGridRowKeys("InvoicePendingActionId");

	    <%--submit the form with rowkeys
	        becuase rowKeys is an array, the parameter name for
	        each will be checkbox + an iteration number -
	        i.e. checkbox0, checkbox1, checkbox2, etc --%>
	 submitFormWithParms(formName, pressedButton, "checkbox", rowKeys);
 }
  
  <%--  DK Cr709 Start --%>
  
    function performLoanRequestActionToInvGroup(pressedButton){  
	  var rowKeys = getSelectedGridRowKeys("InvoiceLoanRequestId");
	    <%--submit the form with rowkeys
	        becuase rowKeys is an array, the parameter name for
	        each will be checkbox + an iteration number -
	        i.e. checkbox0, checkbox1, checkbox2, etc --%>
	 submitFormWithParms(formName, pressedButton, "checkbox", rowKeys);
 }

<%--  DK Cr709 End --%>
 
 <%-- Srinivasu_D Fix of IR#IR#11999 Rel8.2 02/25/2013 start --%>
   <%-- LoanType Action  --%>
 function applyLoanTypeToLoanReqInvGroup(){		 
  	require(["t360/dialog"], function(dialog) {
	 	      dialog.open('InvoiceGroupListDialogID', '<%=resMgr.getText("UploadInvoiceAction.AssignLoanType", TradePortalConstants.TEXT_BUNDLE)%>',
		                      'AssignLoanType.jsp',
		                      ['selectedCount'], [getSelectedGridRowKeys("InvoiceLoanRequestId").length], 
		                      'select', this.createNewLoanTypeValueToLoanReqInvGroup);
		  }); 
 }
   <%-- Loan Req - LoanType action CallBack method --%>
 function createNewLoanTypeValueToLoanReqInvGroup(userLoanType) {	
	
	var theForm = document.getElementsByName(formName)[0];	
	theForm.loanType.value=userLoanType;	
	var rowKeys = getSelectedGridRowKeys("InvoiceLoanRequestId");
	submitFormWithParms("InvoiceGroupListForm","AssignLoanType", "checkbox",rowKeys);
	
 }
  
  function createLoanReqInsToLoanReqInvGroup(bankBranchArray){	
	  rowKeys = getSelectedGridRowKeys("InvoiceLoanRequestId");
		var items=getSelectedGridItems("InvoiceLoanRequestId");

		if(rowKeys == ""){
			alert("Please pick a record and click 'Select'");
			return;
		}
		if ( bankBranchArray.length == 1 ) {
			createNewLoanRequestInstValue(bankBranchArray[0]);
		} else {
			require(["t360/dialog"], function(dialog ) {
		      dialog.open('InvoiceGroupListDialogID', '<%=resMgr.getText("bankBranchSelectorDialog.title", TradePortalConstants.TEXT_BUNDLE)%>',
		                  'bankBranchSelectorDialog.jsp',
		                  null, null, <%-- parameters --%>
		                  'select', this.createNewLoanRequestInstValue);
		    });
		 }
}
 function submitInvFormWithParms(bankBranchOid) {
		buttonName='CreateLRQ';
		parmName="checkbox";
		parmValue = rowKeys;
	      <%-- verify arguments at development time --%>
	      if ( formName == null ) {
	        alert('Problem submitting form: formName required');
	      }
	      if ( buttonName == null ) {
	        alert('Problem submitting form: buttonName required');
	      }
	      if ( parmName == null ) {
	        alert('Problem submitting form: parmName required');
	      }
	  
	      <%-- get the form and submit it --%>
	      if ( document.getElementsByName(formName).length > 0 ) {
	        var myForm = document.getElementsByName(formName)[0];
	        <%-- generate url parameters for the rowKeys and add to  --%>
	        <%-- form as hidden fields --%>
	        if (parmValue instanceof Array) {
	          for (var myParmIdx in parmValue) {
	            var myParmValue = parmValue[myParmIdx];
	            var myParmName = parmName + myParmIdx;
	            addParmToForm(myForm, myParmName, myParmValue);
	          }
	        }
	        else { <%-- assume a single --%>
	        	 addParmToForm(myForm, parmName, parmValue);
	          
	        }
	        myForm.bankBranch.value=bankBranchOid; 
	        var formNumber = getFormNumber(formName);
	        setButtonPressed(buttonName, formNumber); 
	        myForm.submit();
	      } else {
	        alert('Problem submitting form: form ' + formName + ' not found');
	      }
	    }
  
function createNewLoanRequestInstValue(bankBranchOid) {	
	
	submitInvFormWithParms(bankBranchOid);

}
 
 
  <%-- Loan Req - Apply Paymentdate Action  --%>
 function applyPaymentDateToLoanReqInvGroup(){
	 
	    	require(["t360/dialog"], function(dialog) {
		 	      dialog.open('InvoiceGroupListDialogID', '<%=resMgr.getText("UploadInvoiceAction.UploadInvoiceApplyPaymentDateText", TradePortalConstants.TEXT_BUNDLE)%>',
			                      'AssignPaymentDate.jsp',
			                      ['selectedCount'], [getSelectedGridRowKeys("InvoiceLoanRequestId").length], 
			                      'select', this.createNewApplyPaymentValueToLoanReqInvGroup);
			  }); 
 }
	<%-- Loan Req - Apply Paymentdate Action Callback method --%>
 function createNewApplyPaymentValueToLoanReqInvGroup(payDate) {

	var theForm = document.getElementsByName(formName)[0];
   	theForm.paymentDate.value=payDate;
   	var rowKeys = getSelectedGridRowKeys("InvoiceLoanRequestId");
    submitFormWithParms("InvoiceGroupListForm","ApplyPaymentDate", "checkbox",rowKeys);

 }
 <%-- Srinivasu_D Fix of IR#IR#11999 Rel8.2 02/25/2013 end --%>

 function performInvoiceGroupAuthorizationAction(){

	 <%if(InvoiceRequireAuth){ %>
	 <%-- IR T36000014931 REL ER 8.1.0.4 BEGIN --%>
	     openReauthenticationWindowForList(certAuthURL, formName, "AuthorizeInvoices" , "InvoiceG","InvoiceGroupId");	     
	 <%-- IR T36000014931 REL ER 8.1.0.4 END --%>
	 <%}else{ %>
	     
	    <%--get array of rowkeys from the grid--%>
      var rowKeys = getSelectedGridRowKeys("InvoiceGroupId");
 
	    <%--submit the form with rowkeys
	        becuase rowKeys is an array, the parameter name for
	        each will be checkbox + an iteration number -
	        i.e. checkbox0, checkbox1, checkbox2, etc --%>
	  submitFormWithParms(formName, "AuthorizeInvoices", "checkbox", rowKeys);
   <%}%>
 }
 
 function performInvoiceGroupOfflineAuthorizationAction(){
 	<%-- IR T36000014931 REL ER 8.1.0.4 BEGIN --%>
	 openReauthenticationWindowForList(certAuthURL, formName, "ProxyAuthorizeInvoices", "InvoiceG","InvoiceGroupId");      
	 <%-- IR T36000014931 REL ER 8.1.0.4 END --%>
 }
 
 function performAuthCreditGroupAction(){
	 
	 <%if(InvoiceRequireAuth){ %>
	     <%-- IR T36000014931 REL ER 8.1.0.4 BEGIN	    	       --%>
	     openReauthenticationWindowForList(certAuthURL, formName, "AuthorizeCreditNote" , "InvoiceG","InvoicePendingActionId");	     
	     <%-- IR T36000014931 REL ER 8.1.0.4 END --%>
	     <%}else{ %>	     
	    <%--get array of rowkeys from the grid--%>
         var rowKeys = getSelectedGridRowKeys("InvoicePendingActionId");

	    <%--submit the form with rowkeys
	        becuase rowKeys is an array, the parameter name for
	        each will be checkbox + an iteration number -
	        i.e. checkbox0, checkbox1, checkbox2, etc --%>
	    submitFormWithParms(formName, "AuthorizeCreditNote", "checkbox", rowKeys);
	 <%}%>
 }
 
 function performProxyAuthCreditGroupAction(){
 	<%-- IR T36000014931 REL ER 8.1.0.4 BEGIN --%>
	 openReauthenticationWindowForList(certAuthURL, formName, "ProxyAuthorizeCreditNote", "InvoiceG","InvoicePendingActionId");      
	<%-- IR T36000014931 REL ER 8.1.0.4 END  --%>
 }
 <%--  
  function applyPaymentDateToInvGroup(){		 
  	require(["t360/dialog"], function(dialog) {
	 	      dialog.open('InvoiceGroupListDialogID', '<%=resMgr.getText("UploadInvoiceAction.UploadInvoiceApplyPaymentDateText", TradePortalConstants.TEXT_BUNDLE)%>',
		                      'AssignPaymentDate.jsp',
		                      ['selectedCount'], [getSelectedGridRowKeys("InvoiceGroupId").length], 
		                      null, null);
		  }); 
 }
 --%>
   <%-- Invoice Group-  Apply Paymentdate Action  --%>
 function applyPaymentDateToInvGroup(){
	 
	    	require(["t360/dialog"], function(dialog) {
		 	      dialog.open('InvoiceGroupListDialogID', '<%=resMgr.getText("UploadInvoiceAction.UploadInvoiceApplyPaymentDateText", TradePortalConstants.TEXT_BUNDLE)%>',
			                      'AssignPaymentDate.jsp',
			                      ['selectedCount'], [getSelectedGridRowKeys("InvoiceGroupId").length], 
			                      'select', this.createNewApplyPaymentValueToInvGroup);
			  }); 
 }
	<%-- Invoice Group - Apply Paymentdate Action Callback method --%>
 function createNewApplyPaymentValueToInvGroup(payDate) {

	var theForm = document.getElementsByName(formName)[0];
   	theForm.paymentDate.value=payDate;
   	var rowKeys = getSelectedGridRowKeys("InvoiceGroupId");
    submitFormWithParms("InvoiceGroupListForm","ApplyPaymentDate", "checkbox",rowKeys);

 }

<%-- Rel 8.3 IR T36000020251 START- Calling initParms() only once, thus avoiding script errors.	 --%>
 require(["dojo/query", "dojo/on", "t360/datagrid", "t360/popup", "dojo/ready", "dojo/domReady!"],
		  function(query, on, t360grid, t360popup, ready ) {
		 ready(function() {
	  
	 initParms();
		 });
 });
<%-- Rel 8.3 IR T36000020251 END --%>

  <%--  DK CR709 End --%>
 
  require(["dojo/query", "dojo/on", "t360/datagrid", "t360/popup", "dojo/ready", "dojo/domReady!"],
		  function(query, on, t360grid, t360popup, ready ) {
		 ready(function() {
	  
	     query('#invoiceGroupRefresh').on("click", function() {
	    	searchInvoicePending();
	    });
  query('#invoiceGroupEdit').on("click", function() {
    var columns = t360grid.getColumnsForCustomization("InvoiceGroupId");
    var parmNames = [ "gridId", "gridName", "columns" ];
    var parmVals = ["InvoiceGroupId", "InvoiceGroupDataGrid", columns ];
    t360popup.open(
      'invoiceGroupEdit', 'gridCustomizationPopup.jsp',
      parmNames, parmVals,
      null, null);  <%-- callbacks --%>
  });
		 });
 });
  
  
  require(["dojo/query", "dojo/on", "t360/datagrid", "t360/popup", "dojo/ready", "dojo/domReady!"],
		  function(query, on, t360grid, t360popup, ready ) {
		 ready(function() {
	    query('#invCreditGroupRefresh').on("click", function() {
	    	searchCreditPending();
	    });
  query('#invCreditGroupEdit').on("click", function() {
    var columns = t360grid.getColumnsForCustomization("InvoicePendingActionId");
    var parmNames = [ "gridId", "gridName", "columns" ];
    var parmVals = ["InvoicePendingActionId", "InvoicePendingActionDataGrid", columns ];
    t360popup.open(
      'invCreditGroupEdit', 'gridCustomizationPopup.jsp',
      parmNames, parmVals,
      null, null);  <%-- callbacks --%>
  });
		 });
 });

<%--  DK CR709 --%>
  
    require(["dojo/query", "dojo/on", "t360/datagrid", "t360/popup","dojo/ready", "dojo/domReady!"],
    		 function(query, on, t360grid, t360popup, ready ) {
		 ready(function() {
    	query('#invoiceLoanRequestRefresh').on("click", function() {
	    	searchInvoiceLoanRequest();
	    });
  query('#invoiceLoanRequestEdit').on("click", function() {
    var columns = t360grid.getColumnsForCustomization("InvoiceLoanRequestId");
    var parmNames = [ "gridId", "gridName", "columns" ];
    var parmVals = ["InvoiceLoanRequestId", "InvoiceLoanRequestDataGrid", columns ];
    t360popup.open(
      'invoiceLoanRequestEdit', 'gridCustomizationPopup.jsp',
      parmNames, parmVals,
      null, null);  <%-- callbacks --%>
  });
		 });
 });
 

function assignInstrumentTypeToInvGroup(){
	 
	    	require(["t360/dialog"], function(dialog) {
		 	      dialog.open('InvoiceGroupListDialogID', '<%=resMgr.getText("UploadInvoiceAction.AssignInstrType", TradePortalConstants.TEXT_BUNDLE)%>',
			                      'AssignInstrumentType.jsp',
			                      ['selectedCount'], [getSelectedGridRowKeys("InvoicePendingActionId").length], 
			                      'select', this.createNewInstrumentTypeGroupValue);
			  }); 
 }

<%-- Callback fro Instrument Type - for Group invoices --%>
function createNewInstrumentTypeGroupValue(instType,loanType) {

	 var theForm = document.getElementsByName(formName)[0];
   	theForm.instrumentType.value=instType;
	theForm.loanType.value=loanType;
   	var rowKeys = getSelectedGridRowKeys("InvoicePendingActionId");
    submitFormWithParms("InvoiceGroupListForm","AssignInstrType", "checkbox",rowKeys);
 }

	require(["dojo/dom-class", "dojo/query", "dojo/on", "t360/datagrid", "t360/popup", "dijit/registry", "dojo/ready", "dojo/domReady!"],
			 function(domClass, query, on, t360grid, t360popup, registry,ready) {
		 ready(function() {
			  <%-- Grouped Invoices Available for Financing --%>
			  
			  <%-- IR T36000030360: Hide page content, untill widgets get parsed. --%>
				domClass.add("InvoiceMgtDivID", "gridInvoiceFooterDivVisible");
				domClass.remove("InvoiceMgtDivID", "gridInvoiceFooterDivHidden");
				
			  if(registry.byId("InvoiceGroupId")){
				  var invGrid = registry.byId("InvoiceGroupId");			   
				  on(invGrid,"selectionChanged", function() {
					  if (registry.byId('UploadInvoices_Authorize')){
				          t360grid.enableFooterItemOnSelectionGreaterThan(invGrid,"UploadInvoices_Authorize",0);
					  }
					  if (registry.byId('UploadInvoices_Actions')){
						  t360grid.enableFooterItemOnSelectionGreaterThan(invGrid,"UploadInvoices_Actions",0);
					  }
					  if (registry.byId('UploadInvoices_Document')){
						  t360grid.enableFooterItemOnSelectionGreaterThan(invGrid,"UploadInvoices_Document",0);
					  }
					  if (registry.byId('UploadInvoices_Financing')){
						  t360grid.enableFooterItemOnSelectionGreaterThan(invGrid,"UploadInvoices_Financing",0);
					  }					  
				  });
			  }
			  <%-- invoices for loan requests --%>
			  if(registry.byId("InvoiceLoanRequestId")){
				  var invLRGrid = registry.byId("InvoiceLoanRequestId");			   
				  on(invLRGrid,"selectionChanged", function() {
					  if (registry.byId('UploadInvoices_LActions')){
						  t360grid.enableFooterItemOnSelectionGreaterThan(invLRGrid,"UploadInvoices_LActions",0);
					  }
					  if (registry.byId('UploadInvoices_LDocument')){
						  t360grid.enableFooterItemOnSelectionGreaterThan(invLRGrid,"UploadInvoices_LDocument",0);
					  }
					 				  
				  });
			  }
			  <%-- invoices for pending action --%>
			  if(registry.byId("InvoicePendingActionId")){
				  var invPGrid = registry.byId("InvoicePendingActionId");			   
				  on(invPGrid,"selectionChanged", function() {
					  if (registry.byId('UploadInvoices_PActions')){
						  t360grid.enableFooterItemOnSelectionGreaterThan(invPGrid,"UploadInvoices_PActions",0);
					  }
					  if (registry.byId('UploadInvoices_PDocument')){
						  t360grid.enableFooterItemOnSelectionGreaterThan(invPGrid,"UploadInvoices_PDocument",0);
					  }	
					  if (registry.byId('UploadInvoices_PAuthorize')){
				          t360grid.enableFooterItemOnSelectionGreaterThan(invPGrid,"UploadInvoices_PAuthorize",0);
					  }			  
				 });			  
			 }
		 });
	 });
</script>
