<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
*******************************************************************************
                        Invoice Management Invoice Groups List

  Description:
    Contains HTML to the list views that appear on the Invoice Groups tab

  This is not a standalone frag.  It MUST be included using the following tag:
  <%@ include file="InvoiceManagement_InvoiceGroupsList.frag" %>
*******************************************************************************
--%> 
<%
      dynamicWhereClause = new StringBuffer();
      statusExtraTags = new StringBuffer();
      searchType = request.getParameter("SearchType");

      selectedStatus = request.getParameter(statusSearchParm);
      session.removeAttribute(statusSearchParm);
      if (selectedStatus == null) {
      	 if (InstrumentServices.isBlank((String)session.getAttribute(statusSearchParm))) {
            selectedStatus = defaultSearchStatus;
         } else {
            selectedStatus = (String) session.getAttribute(statusSearchParm);
         }
      }

      session.setAttribute(statusSearchParm, selectedStatus);

      if (!TradePortalConstants.STATUS_ALL.equals(selectedStatus)) {
         dynamicWhereClause.append(" and i.invoice_status = '");
         dynamicWhereClause.append(selectedStatus).append("' ");
      }
      dynamicWhereClause.append(" and i.a_corp_org_oid = ").append(userOrgOid);

      // Build the status dropdown options 
      // re-selecting the most recently selected option.
      dropdownOptions = new StringBuffer(Dropdown.createSortedRefDataIncludeOptions(statusRefData,
                                             selectedStatus,resMgr, loginLocale, codesToExclude,false));

      // Upon changing the status selection, automatically go back to the
      // Receivable Home page with the selected status type.  Force a new
      // search to occur (causes cached "where" clause to be reset.)
      statusExtraTags.append("onchange=\"location='");
      statusExtraTags.append(formMgr.getLinkAsUrl("goToInvoiceManagement", response));
      statusExtraTags.append("&currentTab=").append(currentTab);
      statusExtraTags.append("&NewDropdownSearch=Y");
      statusExtraTags.append("&").append(statusSearchParm);
      statusExtraTags.append("='+this.options[this.selectedIndex].value\"");
%>


<div class="gridSearch">
  <div class="searchHeader">  
   <span class="searchHeaderCriteria">
 	
<%                 

out.print(widgetFactory.createSearchSelectField(searchID, "InvoiceSearch.Show",
		TradePortalConstants.STATUS_ALL,
        dropdownOptions.toString(),onSearch));     
 

%>
   </span>
	<span class="searchHeaderActions">
	<%if("CreditUploadListEdit".equals(dataEditID)){%>
		<jsp:include page="/common/gridShowCount.jsp">
	    	<jsp:param name="gridId" value="PayablesCreditNoteListId" />
	    </jsp:include>
	<%}%>    
 	  <span id="<%=dataRefreshID%>" class="searchHeaderRefresh"></span>
 	  <%=widgetFactory.createHoverHelp(dataRefreshID, "RefreshImageLinkHoverText") %>
 	  
 	  <%if("invoiceGroupEdit".equals(dataEditID) || "invoiceLoanRequestEdit".equals(dataEditID) || 
 			  "invCreditGroupEdit".equals(dataEditID) || "InvoiceLRListEdit".equals(dataEditID) || "InvoiceUploadListEdit".equals(dataEditID) || 
 			 "payablesPrgmInvListEdit".equals(dataEditID)  || "CreditUploadListEdit".equals(dataEditID)) {   %>
      <span id="<%=dataEditID%>" class="searchHeaderEdit"></span>
      <%=widgetFactory.createHoverHelp(dataEditID,"CustomizeListImageLinkHoverText") %> 
       <%} %>
    </span>
   
    <div style="clear:both;"></div>

  <input type=hidden name=SearchType value="<%= searchType %>">
  <input type=hidden name="payment_date">
  
 <%
   //DataGridFactory dgFactory = new DataGridFactory(resMgr, userSession, formMgr, response);
    gridHtmlSummary = dgFactory.createDataGridMultiFooter(gridID, gridName, buttonDisplay);
%>
</div>

<%=gridHtmlSummary%>
</div>
