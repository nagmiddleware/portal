
<%--
*******************************************************************************
  Payment Pending Transactions Footer

  Description: Everything that should be loaded at the end of the page
               should be included here.  This includes non-visible html 
               and javascript that should execute on page load.

*******************************************************************************
--%>
<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
*******************************************************************************
  Payment Pending Transactions Footer

  Description: Everything that should be loaded at the end of the page
               should be included here.  This includes non-visible html 
               and javascript that should execute on page load.

*******************************************************************************
--%>
<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>

<%
DataGridFactory dgFactory = new DataGridFactory(resMgr, userSession, formMgr,beanMgr, response);
String gridLayout = dgFactory.createGridLayout("InvoiceUploadListId", "PayablesPrgmInvoiceListDataGrid");
String PayCRTgridLayout ="";
if(isCreditNoteEnabled || TradePortalConstants.INDICATOR_YES.equals(corporateOrgPayCrnAllowH2HApprovalInd)){
   PayCRTgridLayout = dgFactory.createGridLayout("PayablesCreditNoteListId","PayablesCreditNoteListDataGrid");
}
 %>

<script type="text/javascript">

var formatGridLinkChild=function(columnValues, idx, level){

    var gridLink="";
  
    if(level == 0){ 
      gridLink = "<a href='" + columnValues[1] + "'>" + columnValues[0] + "</a>";
      return gridLink;
      
    }else if(level ==1){
      gridLink = "<a href='" + columnValues[2] + "'>" + columnValues[0] + "</a>";
      return gridLink;
    }
  }
  <%--IR 36527 : missing JS var added--%>
var savedSearchQueryDataCR = {
		<% 
		if(savedSearchQueryDataCR !=null && !savedSearchQueryDataCR.isEmpty()) {
			Iterator<Map.Entry<String, String>> entries = savedSearchQueryDataCR.entrySet().iterator();
		while (entries.hasNext()) {
			Map.Entry<String, String> entry = entries.next(); 
			%>
		'<%=entry.getKey()%>':'<%=entry.getValue()%>'
			<%if(entries.hasNext()) {%>
			<%=comma%>
			
		<%
			}
		}
		}%>
}
  
  <%--get grid layout from the data grid factory--%>
  var gridLayout=<%=gridLayout%>;
  var gridCRTLayout;
  <%if(isCreditNoteEnabled || TradePortalConstants.INDICATOR_YES.equals(corporateOrgPayCrnAllowH2HApprovalInd)){%>
  gridCRTLayout = <%=PayCRTgridLayout%>;
    <%}%>
  var gridID = "InvoiceUploadListId";
  var gridName = "PayablesPrgmInvoiceListDataGrid";
  var init='userOid=<%=StringFunction.xssCharsToHtml(userOid)%>&userOrgOid=<%=StringFunction.xssCharsToHtml(userOrgOid)%>';
  var initSearchParms = "";
  var invoiceStatusType =savedSearchQueryData["invoiceStatusType"];
  var formName = "UploadInvoiceListForm";
  var savedSort= savedSearchQueryData["sort"];
  var creditStatusType =savedSearchQueryDataCR["invoiceStatusType"];<%-- IR 36527 --%>
  var savedCRTSort= savedSearchQueryDataCR["sort"];<%-- IR 36527 --%>
  var initCRSearchParms = "";
  var viewCRName ="";
 function initParms(){
	 require(["dojo/dom","dijit/registry"],
		      function(dom,registry){	
		 if("" == invoiceStatusType || null == invoiceStatusType || "undefined" == invoiceStatusType)
			 invoiceStatusType=registry.byId('invoiceStatusType').value;	
		 else if("All" !=invoiceStatusType)
			 registry.byId("invoiceStatusType").set('value',invoiceStatusType);
		
		 if("" == invoiceStatusType){
			 invoiceStatusType = 'All';
			}
		 if("" == savedSort || null == savedSort || "undefined" == savedSort){
			 savedSort = 'TradingPartner';
    	 }
    	 
    	 <%if(isCreditNoteEnabled || TradePortalConstants.INDICATOR_YES.equals(corporateOrgPayCrnAllowH2HApprovalInd)){%>
		 if("" == creditStatusType || null == creditStatusType || "undefined" == creditStatusType)
			 creditStatusType=registry.byId('creditStatusType').value;	
		 else if("All" !=creditStatusType)
			 registry.byId("creditStatusType").set('value',creditStatusType);
		
		 if("" == creditStatusType){
			 creditStatusType = 'All';
			}
		 if("" == savedSort || null == savedSort || "undefined" == savedSort){
			 savedCRTSort = 'TradingPartner';
    	 }
		 <%}%>
    	 
		 }); 
	  
	  initSearchParms = init+"&invoiceStatusType="+invoiceStatusType;
	  initCRSearchParms = init+"&invoiceStatusType="+creditStatusType;
      console.log(initSearchParms);
 
      <%--create the grid, attaching it to the div id specified in dataGrid.jsp--%>
      var viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("PayablesPrgmInvoiceListDataView",userSession.getSecretKey())%>";  <%--Rel9.2 IR T36000032596 --%>
      var InvoiceUploadListId = createDataGrid(gridID, viewName, gridLayout, initSearchParms,savedSort);
      <%if(isCreditNoteEnabled || TradePortalConstants.INDICATOR_YES.equals(corporateOrgPayCrnAllowH2HApprovalInd)){%>
        viewCRName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("PayablesCreditNoteListDataView",userSession.getSecretKey())%>"; 
        var PayablesCreditNoteListId = createMultiSelectLazyTreeDataGrid("PayablesCreditNoteListId", viewCRName, gridCRTLayout, init,'TradingPartner');
      <%}%>
 }
 
 <%--get grid layout from the data grid factory--%>
 
  function payablesPanelStatusFormatter(columnValues){
  	  var gridLink = "";
  	if(columnValues[2] == '<%=TradePortalConstants.UPLOAD_INV_STATUS_PAR_AUTH%>' ||
	  		columnValues[2] == '<%=TradePortalConstants.UPLOAD_INV_STATUS_AUTH_FAILED%>'){
      		 var title = "<%=resMgr.getText("Home.PanelAuthTransactions.ViewAuthorisationsDialog.Title", TradePortalConstants.TEXT_BUNDLE)%>"+columnValues[1];
  			 gridLink =  "<a href='javascript:opendialogViewPanelAuths(\""+title+"\",\"InvoiceUploadListId\",\"PAY_INV_SUMMARY\");'>"+columnValues[0]+"</a>";
  	  		 return gridLink;
  		}else{
  			return columnValues[0];
  		}
  	}
  
  function creditNotesPanelStatusFormatter(columnValues){
  	  var gridLink = "";
		 	if(columnValues[2] == '<%=TradePortalConstants.CREDIT_NOTE_STATUS_PAUT%>' ||
			  		columnValues[2] == '<%=TradePortalConstants.CREDIT_NOTE_STATUS_FAUT%>'){
				 var title = "<%=resMgr.getText("Home.PanelAuthTransactions.ViewAuthorisationsDialog.Title", TradePortalConstants.TEXT_BUNDLE)%>"+columnValues[1];
				 gridLink =  "<a href='javascript:opendialogViewPanelAuths(\""+title+"\",\"PayablesCreditNoteListId\",\"CREDIT_NOTES\");'>"+columnValues[0]+"</a>";
		  		 return gridLink;
			}else{
				return "";
			}
  }
  function searchPending(){
		 invoiceStatusType=dijit.byId("invoiceStatusType").value;
		 if("" == invoiceStatusType){
			 invoiceStatusType = 'All';
			}
		  var searchParms = init+"&invoiceStatusType="+invoiceStatusType;
		  console.log('searchParms='+searchParms); 
	      searchDataGrid(gridID, "PayablesPrgmInvoiceListDataView",
	                     searchParms);
	  }
 
  function performInvPayPrgmAction(pressedButton){
	    var rowKeys = getSelectedGridRowKeys(gridID);

	    submitFormWithParms(formName, pressedButton, "checkbox", rowKeys);
  }

  function performInvoiceOfflineAuthorizationAction(){
	  openReauthenticationWindowForList(certAuthURL, formName, "ProxyAuthorizeInvoices", "Invoice",gridID);
  }
  
<%--Added for CR 1006 --%>
	 function performInvoiceDecline(pressedButton){
	 var formName = '<%=formName%>';
	 require(["dojo/_base/array"], 
        	function(array) {
			    <%--get array of rowkeys from the grid--%>
			    var rowKeys = getSelectedGridRowKeys(gridID);
				var items = getSelectedGridItems(gridID);
				var endToEndID = "";
			 	array.forEach(items, function(item){
		      		endToEndID = endToEndID + item.i.EndToEndID;
		      	}); 
		      	var payInvAllowH2HApprovalInd = <%=TradePortalConstants.INDICATOR_YES.equals(corporateOrgPayInvAllowH2HApprovalInd)%>;
			 	if(payInvAllowH2HApprovalInd && endToEndID !=""){
			 		if(confirm("<%= resMgr.getText("UploadInvoices.DeclineE2EInvoicesAlert", TradePortalConstants.TEXT_BUNDLE)%>")){
			 			submitFormWithParms(formName, pressedButton, "checkbox", rowKeys);
			 		}
			 		else{
			 			return false;
			 		}
			 	}else{
			 		submitFormWithParms(formName, pressedButton, "checkbox", rowKeys);
			 	}
		});	 	
	 }
 function authoriseInvoices(){
 require(["dojo/_base/array"], 
        	function(array) {
			    <%--get array of rowkeys from the grid--%>
			    var items = getSelectedGridItems(gridID);
			    var rowKeys = getSelectedGridRowKeys(gridID);
				var endToEndID = "";
				var apprInd = "";
			 	array.forEach(items, function(item){
		      		endToEndID = item.i.EndToEndID;
		      		apprInd = item.i.ApprovalInd;
		      	});
		      	var payCRAllowH2HApprovalInd = <%=TradePortalConstants.INDICATOR_YES.equals(corporateOrgPayInvAllowH2HApprovalInd)%>;
		      	if(endToEndID!= ""){
					if(!confirm("<%=resMgr.getText("UploadInvoices.AuthoriseEndToEndIDConfirmMsg",TradePortalConstants.TEXT_BUNDLE) %>")){
						return false;
					}
					else if(apprInd!= "N" && payCRAllowH2HApprovalInd){
				    	if(confirm("<%= resMgr.getText("UploadInvoices.DeclineE2EInvoicesAlert", TradePortalConstants.TEXT_BUNDLE)%>")){
			 				performAuthorizeInvoices();
			 			}
			 			else{
			 				return false;
			 			}
					}
					else{
						performAuthorizeInvoices();
					} 
				}
				else{
					performAuthorizeInvoices();
				} 
		});
 }
 
 function performAuthorizeInvoices(){
 var buttonName = '<%=TradePortalConstants.PAY_PGM_BUTTON_AUTHORIZ%>';
 var rowKeys = getSelectedGridRowKeys(gridID);
  <%if (PayInvoiceRequireAuth && StringFunction.isNotBlank(certAuthURL) &&  StringFunction.isNotBlank(authorizeLink) ) {%>
	  openReauthenticationWindowForList(certAuthURL, formName, buttonName, "Invoice",gridID);
	  <%} else {%>		
	 	submitFormWithParms(formName, buttonName, "checkbox", rowKeys);
	    <%}%>
 }
 
 function OfflineAuthInvoices(){
	  var rowKeys = getSelectedGridRowKeys(gridID);
	  var buttonName = '<%=TradePortalConstants.PAY_PGM_BUTTON_AUTHORIZ%>';
	  var certAuthURL = '<%=certAuthURL%>';
		
	  <%if (offlineAuthoriseRequireAuth && StringFunction.isNotBlank(certAuthURL)) {%>
	  openReauthenticationWindowForList(certAuthURL, formName, buttonName, "Invoice",gridID);
	
	    <%}%>
 }
  
  function applyPaymentDateToInvList(){
	 
	    	require(["t360/dialog"], function(dialog) {
		 	      dialog.open('InvoiceGroupListDialogID', '<%=resMgr.getText("UploadInvoiceAction.UploadInvoiceApplyPaymentDateText", TradePortalConstants.TEXT_BUNDLE)%>',
			                      'AssignPaymentDate.jsp',
			                      ['selectedCount'], [getSelectedGridRowKeys(gridID).length], 
			                      'select', this.createNewApplyPaymentValue);
			  }); 
 }
  function createNewApplyPaymentValue(payDate) {
		var theForm = document.getElementsByName(formName)[0];
		theForm.payment_date.value=payDate;
		var rowKeys = getSelectedGridRowKeys(gridID);
		submitFormWithParms(formName,"ApplyPaymentDate", "checkbox",rowKeys);		
	}
  
  function searchInvoices() {
	    require(["dojo/dom"],
	      function(dom){

	    	var invoiceStatus = dijit.byId("invoiceStatusType").attr('value');
	 
	        var searchParms = init+"&invoiceStatusType="+invoiceStatusType;
	        console.log("SearchParms: " + searchParms);
	        searchDataGrid(gridID, "PayablesPrgmInvoiceListDataView",
	                       searchParms);
	      });
	  }
	  
  function searchCreditNote(){
	  require(["t360/datagrid","dijit/registry","dojo/on", "dojo/ready", "dojo/domReady!"], function(t360grid,registry,on,ready){
			 invoiceStatusType=dijit.byId("creditStatusType").value;
			 if("" == invoiceStatusType){
				 invoiceStatusType = 'All';
				}
			  var searchParms = init+"&invoiceStatusType="+invoiceStatusType;
			  var newGridLayout = registry.byId('PayablesCreditNoteListId').get('structure');
			  t360grid.searchMultiSelectLazyTreeDataGrid("PayablesCreditNoteListId", viewCRName, newGridLayout, searchParms);
			  ready(function() {
		 			 <%-- SSikhakolli - Rel-9.2 CR-914A 02/17/2015 - Addinng below code to protect child rows from mouse selection. --%>
			          	var myGrid = registry.byId("PayablesCreditNoteListId");
			          myGrid.onCanSelect=function(idx){
							var selectedItem = this.getItem(idx).i;
							var isChild = (selectedItem.CHILDREN != "true");
						   	return (!isChild);
						};
		 		});
			  <%--  Rel 9.2 - IR 36527 Start  --%>
			
			  if(registry.byId("PayablesCreditNoteListId")){
				  
				  var crtPGrid = registry.byId("PayablesCreditNoteListId");			   
				  on(crtPGrid,"selectionChanged", function() {
					  if (registry.byId('UploadInvoices_CRActions')){
						  t360grid.enableFooterItemOnSelectionGreaterThan(crtPGrid,"UploadInvoices_CRActions",0);
					  }
					  if (registry.byId('UploadInvoices_CRDocument')){
						  t360grid.enableFooterItemOnSelectionGreaterThan(crtPGrid,"UploadInvoices_CRDocument",0);
					  }	
					  if (registry.byId('UploadInvoices_CRAuthorize')){
						  t360grid.enableFooterItemOnSelectionGreaterThan(crtPGrid,"UploadInvoices_CRAuthorize",0);
					  }						  
				 });			  
			 }
			  <%-- Rel 9.2 - IR 36527 End   --%>
		    });
		  
	}

	require(["dojo/dom-class", "dojo/query", "dojo/on", "t360/datagrid", "t360/popup", "dijit/registry", "dojo/ready", "dojo/domReady!"],
			 function(domClass, query, on, t360grid, t360popup, registry,ready) {
		 ready(function() {
		
		initParms();
		<%-- IR T36000030360: Hide page content, untill widgets get parsed. --%>
		domClass.add("InvoiceMgtDivID", "gridInvoiceFooterDivVisible");
		domClass.remove("InvoiceMgtDivID", "gridInvoiceFooterDivHidden");
			 if(registry.byId(gridID)){
				  var invGrid = registry.byId(gridID);
				  on(invGrid,"selectionChanged", function() {
					  if (registry.byId('UploadInvoices_Authorize')){
				          t360grid.enableFooterItemOnSelectionGreaterThan(invGrid,"UploadInvoices_Authorize",0);
					  }
					  if (registry.byId('UploadInvoices_Actions')){
					  	t360grid.enableFooterItemOnSelectionGreaterThan(invGrid,"UploadInvoices_Actions",0);
					  }
					  if (registry.byId('UploadInvoices_Instruments')){
					  	t360grid.enableFooterItemOnSelectionGreaterThan(invGrid,"UploadInvoices_Instruments",0);
					  }
					  if (registry.byId('UploadInvoices_Document')){
					  	t360grid.enableFooterItemOnSelectionGreaterThan(invGrid,"UploadInvoices_Document",0);
					  }
					  if (registry.byId('UploadInvoices_Dates')){
					  	t360grid.enableFooterItemOnSelectionGreaterThan(invGrid,"UploadInvoices_Dates",0);
					  }
					  if (registry.byId('UploadInvoices_Amounts')){
					  	t360grid.enableFooterItemOnSelectionGreaterThan(invGrid,"UploadInvoices_Amounts",0);
			  		  }
					  if (registry.byId('UploadInvoices_More')){
					  	t360grid.enableFooterItemOnSelectionGreaterThan(invGrid,"UploadInvoices_More",0);
				  	  }
		  		 });
			 }
			 
			  <%--  Payable Credit Note --%>
			  if(registry.byId("PayablesCreditNoteListId")){
				  var crtPGrid = registry.byId("PayablesCreditNoteListId");			   
				  on(crtPGrid,"selectionChanged", function() {
					  if (registry.byId('UploadInvoices_CRActions')){
						  t360grid.enableFooterItemOnSelectionGreaterThan(crtPGrid,"UploadInvoices_CRActions",0);
					  }
					  if (registry.byId('UploadInvoices_CRDocument')){
						  t360grid.enableFooterItemOnSelectionGreaterThan(crtPGrid,"UploadInvoices_CRDocument",0);
					  }	
					  if (registry.byId('UploadInvoices_CRAuthorize')){
						  t360grid.enableFooterItemOnSelectionGreaterThan(crtPGrid,"UploadInvoices_CRAuthorize",0);
					  }						  
				 });			  
			 }
			 
		  query('#invoiceUploadListRefresh').on("click", function() {
		  	searchInvoices();
	      });
	      
	     query('#invoiceUploadListGridEdit').on("click", function() {
	      var columns = t360grid.getColumnsForCustomization(gridID);
	      var parmNames = [ "gridId", "gridName", "columns" ];
	      var parmVals = [ gridID, "PayablesPrgmInvoiceListDataGrid", columns ];
	      t360popup.open(
	       'invoiceUploadListGridEdit', 'gridCustomizationPopup.jsp',
	       parmNames, parmVals,
	       null, null);  <%-- callbacks --%>
	    });
	  
	   query('#CreditUploadListEdit').on("click", function() {
		    var columns = t360grid.getColumnsForCustomization('PayablesCreditNoteListId');
		    var parmNames = [ "gridId", "gridName", "columns" ];
		    var parmVals = [ "PayablesCreditNoteListId", "PayablesCreditNoteListDataGrid", columns ];
		    t360popup.open(
		      'CreditUploadListEdit', 'gridCustomizationPopup.jsp',
		      parmNames, parmVals,
		      null, null);  <%-- callbacks --%>
	   });
	  
	   query('#creditUploadListRefresh').on("click", function() {
		    	 searchCreditNote();
	  });
	  
	   <%-- SSikhakolli - Rel-9.2 CR-914A 02/17/2015 - Addinng below code to protect child rows from mouse selection. --%>
     	var myGrid = registry.byId("PayablesCreditNoteListId");
     myGrid.onCanSelect=function(idx){
			var selectedItem = this.getItem(idx).i;
			var isChild = (selectedItem.CHILDREN != "true");
		   	return (!isChild);
		};
		
		<%-- MEer Rel 9.2 T36000035885 - Change font style to Italic for Child nodes in the CreditNotes Grid --%>
		dojo.connect(myGrid, 'onStyleRow', this, function (row) {
			  var trNode = row.node.childNodes[0].childNodes[0].childNodes[0];
			  if(trNode.className=="dojoxGridNoChildren"){
				  dojo.addClass(trNode, "treeGridChildren");
			  }			 
		});
		
	});
   });

	
 function assignInstrumentTypeToInvList(){
	 
	    	require(["t360/dialog"], function(dialog) {
		 	      dialog.open('InvoiceGroupListDialogID', '<%=resMgr.getText("UploadInvoiceAction.AssignInstrType", TradePortalConstants.TEXT_BUNDLE)%>',
			                      'AssignInstrumentType.jsp',
			                      ['selectedCount','fromPage'], [getSelectedGridRowKeys(gridID).length,'PL'], 
			                      'select', this.createNewInstrumentTypeValue);
			  }); 
 }
 function createNewInstrumentTypeValue(instType,loanType) {

	 var theForm = document.getElementsByName(formName)[0];
   	theForm.instrumentType.value=instType;
	theForm.loanType.value=loanType;
   	var rowKeys = getSelectedGridRowKeys(gridID);
    submitFormWithParms(formName,"ListAssignInstrType", "checkbox",rowKeys);
 }

 function assignLoanTypeToInvList(){		 
  	require(["t360/dialog"], function(dialog) {
	 	      dialog.open('InvoiceGroupListDialogID', '<%=resMgr.getText("UploadInvoiceAction.AssignLoanType", TradePortalConstants.TEXT_BUNDLE)%>',
		                      'AssignLoanType.jsp',
		                      ['selectedCount','fromPage'], [getSelectedGridRowKeys(gridID).length,'PLRG'], 
		                      'select', this.createNewLoanTypeValueToLoanReqInvGroup);
		  }); 
 }
 function createNewLoanTypeValueToLoanReqInvGroup(userLoanType) {	
	
	var theForm = document.getElementsByName(formName)[0];	
	theForm.loanType.value=userLoanType;	
	var rowKeys = getSelectedGridRowKeys(gridID);
	submitFormWithParms(formName,"AssignLoanType", "checkbox",rowKeys);
	
 }

 function createLoanReqByRulesInvList(){
	 
	    	require(["t360/dialog"], function(dialog) {
		 	      dialog.open('InvoiceGroupListDialogID', '<%=resMgr.getText("UploadInvoices.CreateLoanReqByRules", TradePortalConstants.TEXT_BUNDLE)%>',
			                      'CreateLoanRequestByRulesDialog.jsp',
			                      ['selectedCount'], [getSelectedGridRowKeys(gridID).length], 
			                      null, null);
			  }); 
 } 

 <%--   function createInvLRCreateLRQ(){	
	 	  var bankList=  '<%= bankOrgList%>';
		   if(bankList>1){
  	require(["t360/dialog"], function(dialog) {
	 	      dialog.open('InvoiceGroupListDialogID', '<%=resMgr.getText("UploadInvoices.InvLRCreateLRQ", TradePortalConstants.TEXT_BUNDLE)%>',
		                      'CreateLoanRequestDialog.jsp',
		                      ['selectedCount','orglist'], [getSelectedGridRowKeys(gridID).length, "<%=bbGridData.toString()%>"], 
		                      'select', this.createNewLoanRequestInstValue);
		  }); 
		   }
		   else{
			   createNewLoanRequestInstValue('<%=orgOid%>');
		   }
 }
  
function createNewLoanRequestInstValue(userBankBranch) {	
	
	var theForm = document.getElementsByName(formName)[0];	
	theForm.bankBranch.value=userBankBranch;	
	var rowKeys = getSelectedGridRowKeys(gridID);
	submitFormWithParms(formName,"ListCreateLRQ", "checkbox",rowKeys);
	
} --%>
function createInvLRCreateLRQ(bankBranchArray){	
	  rowKeys = getSelectedGridRowKeys(gridID);
		var items=getSelectedGridItems(gridID);

		if(rowKeys == ""){
			alert("Please pick a record and click 'Select'");
			return;
		}
		if ( bankBranchArray.length == 1 ) {
			createNewLoanRequestInstValue(bankBranchArray[0]);
		} else {
			require(["t360/dialog"], function(dialog ) {
		      dialog.open('InvoiceGroupListDialogID', '<%=resMgr.getText("bankBranchSelectorDialog.title", TradePortalConstants.TEXT_BUNDLE)%>',
		                  'bankBranchSelectorDialog.jsp',
		                  ['branchGridId'], ['InvoiceBankBranchSelectorDataGrid'], <%-- parameters --%>
		                  'select', this.createNewLoanRequestInstValue);
		    });
		 }
}
 function submitLRInvFormWithParms(bankBranchOid) {
		buttonName='ListCreateLRQ';
		parmName="checkbox";
		parmValue = rowKeys;
	      <%-- verify arguments at development time --%>
	      if ( formName == null ) {
	        alert('Problem submitting form: formName required');
	      }
	      if ( buttonName == null ) {
	        alert('Problem submitting form: buttonName required');
	      }
	      if ( parmName == null ) {
	        alert('Problem submitting form: parmName required');
	      }
	  
	      <%-- get the form and submit it --%>
	      if ( document.getElementsByName(formName).length > 0 ) {
	        var myForm = document.getElementsByName(formName)[0];
	        <%-- generate url parameters for the rowKeys and add to  --%>
	        <%-- form as hidden fields --%>
	        if (parmValue instanceof Array) {
	          for (var myParmIdx in parmValue) {
	            var myParmValue = parmValue[myParmIdx];
	            var myParmName = parmName + myParmIdx;
	            addParmToForm(myForm, myParmName, myParmValue);
	          }
	        }
	        else { <%-- assume a single --%>
	        	 addParmToForm(myForm, parmName, parmValue);
	          
	        }
	        myForm.bankBranch.value=bankBranchOid; 
	        var formNumber = getFormNumber(formName);
	        setButtonPressed(buttonName, formNumber); 
	        myForm.submit();
	      } else {
	        alert('Problem submitting form: form ' + formName + ' not found');
	      }
	    }
  
function createNewLoanRequestInstValue(bankBranchOid) {	
	
	submitLRInvFormWithParms(bankBranchOid);

}
function createApprovalToPay(bankBranchArray){
	var rowKeys = getSelectedGridRowKeys(gridID);
	
	if ( bankBranchArray.length == 1 ) {
    	createPayableATP(bankBranchArray[0]);
	} else {
		require(["t360/dialog"], function(dialog ) {
	      dialog.open('bankBranchSelectorDialog', '<%=resMgr.getText("bankBranchSelectorDialog.title", TradePortalConstants.TEXT_BUNDLE)%>',
	                  'bankBranchSelectorDialog.jsp',
	                  null, null, <%-- parameters --%>
	                  'select', this.createPayableATP);
	    });
	 }
	}

	function createPayableATP(bankBranchOid) {
		submitInvFormWithParms(bankBranchOid);
		
	}

	function submitInvFormWithParms(bankBranchOid) {
		buttonName='PAYCreateApprovalToPay';
		parmName="checkbox";
		parmValue =  getSelectedGridRowKeys(gridID);
	      <%-- verify arguments at development time --%>
	      if ( formName == null ) {
	        alert('Problem submitting form: formName required');
	      }
	      if ( buttonName == null ) {
	        alert('Problem submitting form: buttonName required');
	      }
	      if ( parmName == null ) {
	        alert('Problem submitting form: parmName required');
	      }
	  
	      <%-- get the form and submit it --%>
	      if ( document.getElementsByName(formName).length > 0 ) {
	        var myForm = document.getElementsByName(formName)[0];
	        <%-- generate url parameters for the rowKeys and add to  --%>
	        <%-- form as hidden fields --%>
	        if (parmValue instanceof Array) {
	          for (var myParmIdx in parmValue) {
	            var myParmValue = parmValue[myParmIdx];
	            var myParmName = parmName + myParmIdx;
	            addParmToForm(myForm, myParmName, myParmValue);
	          }
	        }
	        else { <%-- assume a single --%>
	        	 addParmToForm(myForm, parmName, parmValue);
	          
	        }
			 var theForm = document.getElementsByName(formName)[0];
			 
				theForm.bankBranch.value=bankBranchOid;
	        <%-- addParmToForm(myForm, 'bankBranch', bankBranchOid); --%>
	        var formNumber = getFormNumber(formName);
	        setButtonPressed(buttonName, formNumber); 
	        myForm.submit();
	      } else {
	        alert('Problem submitting form: form ' + formName + ' not found');
	      }
	    }
	function ApplyPayAmtToPayPrgmGroupList(){  
		var rowKeys = getSelectedGridRowKeys(gridID);
		 require(["t360/dialog"], function(dialog) {
	          dialog.open('PayablesPrgmInvoiceListDialogID', '<%=resMgr.getText("UploadInvoices.ApplyEarlyPaymentAmount", TradePortalConstants.TEXT_BUNDLE)%>',
	                          'ApplyEarlyPaymentAmt.jsp',
	                          ['selectedCount'], [rowKeys.length], 
	                          'select', this.applyPaymentAmtValue);
	      }); 

	}
	function applyPaymentAmtValue(paymentAmt) {
	    var theForm = document.getElementsByName(formName)[0];
	    theForm.paymentAmount.value=paymentAmt;
	    var rowKeys = getSelectedGridRowKeys(gridID);
	  submitFormWithParms(formName,"ApplyEarlyPayAmt", "checkbox", rowKeys);
	}
	
	function modifySuppDateToPayPrgmGroupList(){	 
		 var rowKeys = getSelectedGridRowKeys(gridID);
			require(["t360/dialog"], function(dialog) {
				dialog.open('InvoiceGroupListDialogID', '<%=resMgr.getText("UploadInvoices.InvPayModifySuppDate", TradePortalConstants.TEXT_BUNDLE)%>',
			                      'ModifySupplierDate.jsp',
			                      ['selectedCount'], [rowKeys.length], 
			                      'select', this.modifySuppToDateValueToPatPrgm);
			  }); 

    }
	function modifySuppToDateValueToPatPrgm(suppDate) {
		var theForm = document.getElementsByName(formName)[0];
		theForm.supplierDate.value=suppDate;
		var rowKeys = getSelectedGridRowKeys(gridID);
	 submitFormWithParms(formName,"ModifySupplierDate", "checkbox", rowKeys);
	}
	
    function performPayPrgmCreditNoteAction(pressedButton){
	    var formName = '<%=formName%>';
		 require(["dojo/_base/array"], 
        	function(array) {
			    <%--get array of rowkeys from the grid--%>
			    var rowKeys = getSelectedGridRowKeys("PayablesCreditNoteListId");
				var items = getSelectedGridItems("PayablesCreditNoteListId");
				var endToEndID = "";
			 	array.forEach(items, function(item){
		      		endToEndID = item.i.EndToEndId;
		      	});
				if(pressedButton == "AuthorizePayCreditNote"){
					if(endToEndID!= ""){
						if(!confirm("<%=resMgr.getText("UploadInvoices.AuthoriseEndToEndIDConfirmMsg",TradePortalConstants.TEXT_BUNDLE) %>")){
							return false;
						}else{
							submitFormWithParms(formName, pressedButton, "checkbox", rowKeys);
						}
					}
					else{
						submitFormWithParms(formName, pressedButton, "checkbox", rowKeys);
					} 
			   }else{
				   submitFormWithParms(formName, pressedButton, "checkbox", rowKeys);
			   }
				
		});
 }
 function applyCreditNote() {
	    require(["dijit/registry","t360/dialog"], function(registry,dialog ) {
	      	var rowKeys = getSelectedGridRowKeys("PayablesCreditNoteListId");
	       if (rowKeys && rowKeys.length != 1 ) {
	       alert("Please select single credit Note");
	       return false;
	       }
	       if(rowKeys && rowKeys.length ==1){
	       		var items = getSelectedGridItems("PayablesCreditNoteListId");
	       		var endToEndID = items[0].i.EndToEndId;
	       		if(endToEndID !=""){
	       			alert('<%=resMgr.getText("UploadInvoices.ApplyCreditNoteErrMsg", TradePortalConstants.TEXT_BUNDLE)%>');
	       			return false;
	       		}
	       		
	       		<%-- SSikhakolli - Rel-9.2 CR-914A STRT IR# T36000035893 02/04/2015 - Adding alert message and return the acction if Applied CN Status is "Applied(FAP)".  --%>
	       		var appliedCNStatus = items[0].i.AppliedStatus;
	       		if(appliedCNStatus == "Applied"){
	       			alert('<%=resMgr.getText("UploadInvoices.FullyApplyCreditNoteErrMsg", TradePortalConstants.TEXT_BUNDLE)%>');
	       			return false;
	       		}
	       }
		  dialog.open('creditNoteApplySearchDialog', '<%=resMgr.getText("InvoicesForCreditNoteApply.Apply", TradePortalConstants.TEXT_BUNDLE)%>',
	          'CreditNoteApplySearchDialog.jsp',
	          ['userOrgOid','loginLocale','creditNoteOid'],['<%=userOrgOid%>','<%=loginLocale%>',rowKeys[0]], 
	          'select', this.addSelectedInvoices);
	          
	          
	      
	    });
	  }

function UnApplyCreditNote() {
	    require(["dijit/registry","t360/dialog"], function(registry,dialog ) {
	      	var rowKeys = getSelectedGridRowKeys("PayablesCreditNoteListId");
	       if (rowKeys && rowKeys.length != 1 ) {
	       alert("Please select single credit Note");
	       return false;
	       }
		  dialog.open('creditNoteUnApplySearchDialog', '<%=resMgr.getText("UploadInvoices.UnApplyCreditNote", TradePortalConstants.TEXT_BUNDLE)%>',
	          'creditNoteUnApplySearchDialog.jsp',
	          ['userOrgOid','loginLocale','creditNoteOid'],['<%=userOrgOid%>','<%=loginLocale%>',rowKeys[0]], 
	          'select', this.removeSelectedInvoices);
	          
	          
	      
	    });
	  }
	  
function addSelectedInvoices(invLists){
 	var theForm = document.getElementsByName(formName)[0];
	   	var rowKeys = getSelectedGridRowKeys("PayablesCreditNoteListId");
	   	var parmName1="checkbox";
			var parmValue1 = rowKeys;
	   	var parmName ="selectInv";
	   	var myForm = document.getElementsByName("UploadInvoiceListForm")[0];
	   	if (invLists instanceof Array) {
	   	 
		          for (var myParmIdx in invLists) {
		            var myParmValue = invLists[myParmIdx];
		            var myParmName = parmName + myParmIdx;
		            addParmToForm(myForm, myParmName, myParmValue);
		          }
		        }
		        else { <%-- assume a single --%>
		        	 addParmToForm(myForm, parmName, invLists);
		          
		        }
		        
		        if (parmValue1 instanceof Array) {
		        
		          for (var myParmIdx in parmValue1) {
		            var myParmValue = parmValue1[myParmIdx];
		            var myParmName = parmName1 + myParmIdx;
		            addParmToForm(myForm, myParmName, myParmValue);
		          }
		        }
		        else { <%-- assume a single --%>
		        	 addParmToForm(myForm, parmName1, parmValue1);
		          
		        }
		        var formNumber = getFormNumber("UploadInvoiceListForm");
		        setButtonPressed("ApplyCreditNote", formNumber); 
		        myForm.submit(); 
	   <%--  submitFormWithParms("UploadInvoiceListForm","ApplyCreditNote", "checkbox", rowKeys); --%>
 	}	  
 	
 	function removeSelectedInvoices(invLists){
 	var theForm = document.getElementsByName(formName)[0];
	   	var rowKeys = getSelectedGridRowKeys("PayablesCreditNoteListId");
	   	var parmName1="checkbox";
			var parmValue1 = rowKeys;
	  	   	var parmName ="selectInv";
	   	var myForm = document.getElementsByName("UploadInvoiceListForm")[0];
	   	if (invLists instanceof Array) {
	   	
		          for (var myParmIdx in invLists) {
		            var myParmValue = invLists[myParmIdx];
		            var myParmName = parmName + myParmIdx;
		            addParmToForm(myForm, myParmName, myParmValue);
		          }
		        }
		        else { <%-- assume a single --%>
		        	 addParmToForm(myForm, parmName, invLists);
		          
		        }
		        
		        if (parmValue1 instanceof Array) {
		       
		          for (var myParmIdx in parmValue1) {
		            var myParmValue = parmValue1[myParmIdx];
		            var myParmName = parmName1 + myParmIdx;
		            addParmToForm(myForm, myParmName, myParmValue);
		          }
		        }
		        else { <%-- assume a single --%>
		        	 addParmToForm(myForm, parmName1, parmValue1);
		          
		        }
		        var formNumber = getFormNumber("UploadInvoiceListForm");
		        setButtonPressed("UnApplyCreditNote", formNumber); 
		        myForm.submit(); 
	   <%--  submitFormWithParms("UploadInvoiceListForm","ApplyCreditNote", "checkbox", rowKeys); --%>
 	}
 	

</script>