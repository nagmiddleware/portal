<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
**********************************************************************************
                        Invoice Management List Invoice Tab

  Description:
    Contains HTML to display incoices which are uploaded through upload tab

  This is not a standalone frag.  It MUST be included using the following tag:
  <%@ include file="InvoiceManagement_invoices.frag" %>
*******************************************************************************
--%> 

<%
      String searchType                   = request.getParameter("SearchType");
      boolean hasApproveFinancingRights   = false;
      boolean hasAuthInvRights            = false;
      boolean hasRemoveInvRights          = false;
      boolean hasDeleteInvRights          = false;
      StringBuffer statusExtraTags        = new StringBuffer();
      String defaultSearchStatus          = TradePortalConstants.STATUS_ALL;
      String linkArgs[] = {"","","","","",""};
      String gridHtmlSummary = "";
      String buttonDisplay = "";
      
      secureParms.put("UserOid", userOid); 
      secureParms.put("ownerOrg", userOrgOid); 
      secureParms.put("SecurityRights", userSecurityRights);
     
      String newDropdownSearch  = request.getParameter("NewDropdownSearch");      
      hasApproveFinancingRights = SecurityAccess.hasRights(userSecurityRights, SecurityAccess.RECEIVABLES_APPROVE_FINANCING);
      hasAuthInvRights          = SecurityAccess.hasRights(userSecurityRights, SecurityAccess.RECEIVABLES_AUTHORIZE_INVOICE);
      hasRemoveInvRights        = SecurityAccess.hasRights(userSecurityRights, SecurityAccess.RECEIVABLES_REMOVE_INVOICE);
      hasDeleteInvRights        = SecurityAccess.hasRights(userSecurityRights, SecurityAccess.RECEIVABLES_DELETE_INVOICE);    
        
      
      codesToExclude = new Vector();
	 codesToExclude.add(TradePortalConstants.UPLOAD_INV_STATUS_AVL_FOR_AUTH);
	 codesToExclude.add(TradePortalConstants.UPLOAD_INV_STATUS_AUTH_FAILED);
	 codesToExclude.add(TradePortalConstants.UPLOAD_INV_STATUS_PAR_AUTH);
	 codesToExclude.add(TradePortalConstants.UPLOAD_INV_STATUS_AUTH); 
     codesToExclude.add(TradePortalConstants.PAYABLE_UPLOAD_INV_STATUS_ASSIGNED);
	 codesToExclude.add(TradePortalConstants.UPLOAD_INV_STATUS_PEND_ACT_NO_INSTR_TYPE); 
	 codesToExclude.add(TradePortalConstants.PAYABLE_UPLOAD_INV_STATUS_PENDING_ACTION);
	 codesToExclude.add(TradePortalConstants.PAYABLE_UPLOAD_INV_STATUS_CANCELLED);
	 codesToExclude.add(TradePortalConstants.UPLOAD_INV_STATUS_DELETED);
	 codesToExclude.add(TradePortalConstants.UPLOAD_INV_STATUS_DBC);
	 codesToExclude.add(TradePortalConstants.UPLOAD_INV_STATUS_RAA);
    
	     selectedStatus = request.getParameter("invoiceStatusType");
      if (selectedStatus == null) 
      {
      	    selectedStatus = defaultSearchStatus;
      }
      
    session.setAttribute("fromInvoiceGroupDetail", "NGL");
      
    dropdownOptions = new StringBuffer(Dropdown.createSortedRefDataIncludeOptions("UPLOAD_INVOICE_STATUS",
                                             selectedStatus, resMgr,loginLocale, codesToExclude,false));
        
   //SHR print button start
	String viewName = "PayablesPrgmInvoiceListDataView";
	String resourcePrefix = "UploadInvoices";
    String arg = "key~"+viewName+ "`key~" + resourcePrefix;
       
		linkArgs[0] = TradePortalConstants.PDF_LIST;
        linkArgs[1] = EncryptDecrypt.encryptStringUsingTripleDes(arg);//Use common key for encryption of parameter to doc producer
        linkArgs[2] = InstrumentServices.isNotBlank(resMgr.getResourceLocale())? resMgr.getResourceLocale():"en_US";
        //loginLocale;
        linkArgs[3] = userSession.getBrandingDirectory();
        String linkString =  formMgr.getDocumentLinkAsURLInFrame(resMgr.getText("UploadInvoiceDetail.PrintInvoiceDetails",
                TradePortalConstants.TEXT_BUNDLE),
                TradePortalConstants.PDF_LIST,
                linkArgs,
                response) ;

%>
<%=widgetFactory.createSectionHeader("1", "InvoiceList.Section1Invoices")%>    
 <div class="gridSearch">
  <div class="searchHeader">  
   <span class="searchHeaderCriteria">
 	
<%                                               
        out.print(widgetFactory.createSearchSelectField("invoiceStatusType", "InvoiceSearch.Show",
        		resMgr.getText("common.StatusAll",TradePortalConstants.TEXT_BUNDLE),
                                               dropdownOptions.toString(), "onChange='searchPending();'"));  

%>
   </span>
	<span class="searchHeaderActions">
	  <jsp:include page="/common/gridShowCount.jsp">
        <jsp:param name="gridId" value="InvoiceUploadListId" />
      </jsp:include>
      <button data-dojo-type="dijit.form.Button" name="print" id="print" type="button" class="pageSubHeaderButton">
          <%=resMgr.getText("common.PrintText",TradePortalConstants.TEXT_BUNDLE)%>
           <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
          window.open( "<%=linkString%>");
		  </script>
       </button> 
      <%=widgetFactory.createHoverHelp("print", "PrintHoverText") %> 
 	  <span id="invoiceUploadListRefresh" class="searchHeaderRefresh"></span>
 	  <%=widgetFactory.createHoverHelp("invoiceUploadListRefresh", "RefreshImageLinkHoverText") %>
      <span id="invoiceUploadListGridEdit" class="searchHeaderEdit"></span>
      <%=widgetFactory.createHoverHelp("invoiceUploadListGridEdit","CustomizeListImageLinkHoverText") %> 
      
    </span>
    <div style="clear:both;"></div>

  <input type=hidden name=SearchType value="<%= searchType %>">
  <input type=hidden name="payment_date">
  <input type="hidden" value="yes" name="fromPayDetail">
  
   <%
   DataGridFactory dgFactory = new DataGridFactory(resMgr, userSession, formMgr,beanMgr, response);
  String gridHtml = dgFactory.createDataGridMultiFooter("InvoiceUploadListId","PayablesPrgmInvoiceListDataGrid","5");
 
%>
 </div>

<%=gridHtml%>
</div>
</div>
<% if(isCreditNoteEnabled || TradePortalConstants.INDICATOR_YES.equals(corporateOrgPayCrnAllowH2HApprovalInd)){ 
// MEer  IR-35813 Retrieve based on Credit Note Status
//  statusRefData="CREDIT_NOTE_APPLIED_STATUS";
   statusRefData = "CREDIT_NOTE_STATUS";
 	String statusSearchParm = "invoiceStatusType";//IR 36527
 	defaultSearchStatus = resMgr.getText("common.StatusAll",
 			TradePortalConstants.TEXT_BUNDLE);
	codesToExclude = new Vector();
 	
 	codesToExclude.add(TradePortalConstants.CREDIT_NOTE_STATUS_AVA);
 	codesToExclude.add(TradePortalConstants.CREDIT_NOTE_STATUS_STB);
 	codesToExclude.add(TradePortalConstants.CREDIT_NOTE_STATUS_AUT);
 	codesToExclude.add(TradePortalConstants.CREDIT_NOTE_STATUS_PAUT);
 	codesToExclude.add(TradePortalConstants.CREDIT_NOTE_STATUS_FAUT);
 	codesToExclude.add(TradePortalConstants.CREDIT_NOTE_STATUS_CLO);
 	codesToExclude.add(TradePortalConstants.CREDIT_NOTE_STATUS_PBB_BAL);
 	codesToExclude.add(TradePortalConstants.UPLOAD_INV_STATUS_DBC);
 	codesToExclude.add(TradePortalConstants.UPLOAD_INV_STATUS_RAA);

 	gridID = "PayablesCreditNoteListId";
 	gridName = "PayablesCreditNoteListDataGrid";
 	searchID = "creditStatusType";
 	onSearch = "onChange='searchCreditNote();'";
 	dataView = "PayablesCreditNoteListDataView";
 	dataRefreshID = "creditUploadListRefresh";
 	dataEditID = "CreditUploadListEdit"; 
 	buttonDisplay = "6";
 	dgFactory = new DataGridFactory(resMgr, userSession, formMgr, beanMgr, response);
%>

<%=widgetFactory.createSectionHeader("2", "InvoiceList.Section2CRInvoiceID")%>

<%@ include file="/invoicemanagement/fragments/InvoiceManagement_PayableCreditNoteList.frag"%>
</div>
<%}%>
