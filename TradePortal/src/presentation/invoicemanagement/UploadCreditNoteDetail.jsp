
<%--
**********************************************************************************
  Upload Credit Note Detail jsp

  
**********************************************************************************
--%>

<%--
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
--%>
<%@ page import="java.util.*,com.amsinc.ecsg.frame.*, com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*, com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*, com.ams.tradeportal.html.*,
                 com.ams.tradeportal.busobj.webbean.*, com.ams.tradeportal.busobj.util.*, com.ams.tradeportal.busobj.*,java.math.BigDecimal" %>

<jsp:useBean id="beanMgr"     class="com.amsinc.ecsg.web.BeanManager"            scope="session"></jsp:useBean>
<jsp:useBean id="formMgr"     class="com.amsinc.ecsg.web.FormManager"            scope="session"></jsp:useBean>
<jsp:useBean id="resMgr"      class="com.amsinc.ecsg.util.ResourceManager"       scope="session"></jsp:useBean>
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>

<%

  	String fromPayablesFlag = null;
	String closeLink = "goToInvoiceManagement"; 
	fromPayablesFlag = request.getParameter("fromPayables");
	if(fromPayablesFlag!=null && fromPayablesFlag.equals("Y")){
		closeLink = "goToPayableTransactionsHome";
	}
%>
<%@ include file="/invoicemanagement/fragments/UploadCreditNoteDetail.frag"%>
