<%--
*******************************************************************************
                            Invoice Upload Log Detail Page

  Description:  The Invoice Upload Log Detail page is used to provide detail
                information for a specified invoice upload file.
                
  Note:         Consult userSession page flow to determine page to return to.

*******************************************************************************
--%>

<%--
 *
 *     Copyright   2001
 *     American Management Systems, Incorporated
 *     All rights reserved
--%>
<%@ page
	import="java.util.*,com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.html.*"%>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager"
	scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager"
	scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager"
	scope="session">
</jsp:useBean>

<jsp:useBean id="userSession"
	class="com.ams.tradeportal.busobj.webbean.SessionWebBean"
	scope="session">
</jsp:useBean>


<%-- ************** Data retrieval page setup begins here ****************  --%>

<%
   InvoiceFileUploadWebBean    fileUpload               = null;
   UserWebBean 		thisUser			= null;
   DocumentHandler      xmlDoc                          = null;
   StringBuffer         errorListWhere   	        = new StringBuffer();
   StringBuffer         dynamicWhereClause              = null;
   StringBuffer         instrumentLabel                 = new StringBuffer();
   Hashtable            secureParms                     = new Hashtable();
   String               userSecurityRights              = null;
   String               userSecurityType                = null;
   String               instrumentOid                   = null;
   String               uploadOid       	        = null;
   String               userTimeZone                    = null;
   String               userLocale                      = null;
   String               userOrgOid                      = null;
   String               newLink                         = null;
   String               userOid                         = null;
   String		validationStatus		= null;
   String               instrumentTypeCode              = null;
   int 			uploadNumber			= 0;
   String initSearchParms="";
   DGridFactory dgFactory = new DGridFactory(resMgr, userSession, formMgr, response);
   String gridHtml = "";
   String gridLayout = "";
   boolean displayErrorGrid = false;
	String gridLogHtml = "";
	String gridLogLayout = "";
	boolean displayLogGrid = false;
	String gridGroupLogHtml = "";
	String gridGroupLogLayout = "";
	boolean displayGroupLogGrid = false;

   userSession.setCurrentPrimaryNavigation("NavigationBar.UploadCenter");

   //cquinton 1/18/2013 add page to page flow
   userSession.addPage("goToInvoiceUploadLogDetail", request);

   // Get the user's security rights, security type, locale, organization, oid, and client bank oid
   userSecurityRights = userSession.getSecurityRights();
   userSecurityType   = userSession.getSecurityType();
   userTimeZone       = userSession.getTimeZone();
   userLocale         = userSession.getUserLocale();
   userOrgOid         = userSession.getOwnerOrgOid();
   userOid            = userSession.getUserOid();

   fileUpload = beanMgr.createBean(InvoiceFileUploadWebBean.class, "InvoiceFileUpload");
   if (request.getParameter("UploadOid") != null)
   {
      uploadOid = EncryptDecrypt.decryptStringUsingTripleDes(request.getParameter("UploadOid"), userSession.getSecretKey());
      session.setAttribute("UploadOid", EncryptDecrypt.encryptStringUsingTripleDes(uploadOid, userSession.getSecretKey()));
   }
   else
      uploadOid = EncryptDecrypt.decryptStringUsingTripleDes((String) session.getAttribute("UploadOid"), userSession.getSecretKey());

   fileUpload.setAttribute("invoice_file_upload_oid", uploadOid);
   fileUpload.getDataFromAppServer();
   validationStatus = fileUpload.getAttribute("validation_status");

   thisUser = beanMgr.createBean(UserWebBean.class, "User");
   thisUser.setAttribute("user_oid", fileUpload.getAttribute("user_oid"));
   thisUser.getDataFromAppServer();

 
   errorListWhere.append(" and p_invoice_file_upload_uoid = ");
   errorListWhere.append(fileUpload.getAttribute("invoice_file_upload_oid"));

   int invUploaded = 0;
   if (InstrumentServices.isNotBlank(fileUpload.getAttribute("number_of_invoices_uploaded")))
	   invUploaded = Integer.parseInt(fileUpload.getAttribute("number_of_invoices_uploaded"));

   int invFailed = 0;
   if (InstrumentServices.isNotBlank(fileUpload.getAttribute("number_of_invoices_failed")))
	   invFailed = Integer.parseInt(fileUpload.getAttribute("number_of_invoices_failed"));
	 //IR 17776 start     
  		uploadNumber = invUploaded - invFailed;
		//uploadNumber = invUploaded;// Srinivasu_D IR#T36000018079  Rel8.2 06/25/2013 - added
	 //IR 17776 end
	 if (uploadNumber<0){
    	
   		uploadNumber=0;
   	}

   //BSL IR NNUM040229447 04/02/2012 BEGIN
//   if (!validationStatus.equals(TradePortalConstants.PAYBACK_STATUS_SUCCESS))
//   {
//   	if (validationStatus.equals(TradePortalConstants.INVOICE_STATUS_FAILED))
//    	{
//   		uploadNumber=0;
//   	}
//   }
	// Srinivasu_D IR#T36000018079  Rel8.2 06/25/2013 - added
	/*
   if (TradePortalConstants.PYMT_UPLOAD_VALIDATION_FAILED.equals(validationStatus)) {
	   uploadNumber=0;
   }
   */
   //BSL IR NNUM040229447 04/02/2012 END
   	String xmlStr = fileUpload.getAttribute("inv_result_parameters");
	xmlStr = com.amsinc.ecsg.util.StringFunction.xssHtmlToChars(xmlStr);
	//String xmlStr = "<DocRoot><ResultSetRecord ID=\"0\"><INS_NUM>ILC176USO1P</INS_NUM><INS_OID>895681</INS_OID><TRAN_OID>864635</TRAN_OID></ResultSetRecord><ResultSetRecord ID=\"1\"><INS_NUM>ILC176USO1P</INS_NUM><INS_OID>895681</INS_OID><TRAN_OID>864635</TRAN_OID></ResultSetRecord></DocRoot>";

%>

<%-- ********************* HTML for page begins here *********************  --%>

<% WidgetFactory widgetFactory = new WidgetFactory(resMgr); %>
<jsp:include page="/common/Header.jsp">
	<jsp:param name="includeNavigationBarFlag"
		value="<%=TradePortalConstants.INDICATOR_YES%>" />
	<jsp:param name="includeErrorSectionFlag"
		value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>

<div class="pageMainNoSidebar">
  <div class="pageContent">

    <jsp:include page="/common/PageHeader.jsp">
      <jsp:param name="titleKey" value="InvoiceUploadLogDetail.UploadLogText" />
      <jsp:param name="helpUrl" value="customer/Invoice_File_Upload_Log.htm" />
    </jsp:include>

    <div class="subHeaderDivider"></div>
    
    <div class="pageSubHeader">
      <span class="pageSubHeaderItem">
<% 
  String logFileStr = (String)fileUpload.getAttribute("invoice_file_name");
  String [] logFileName = logFileStr.split(".txt");
	
  if (validationStatus.equals(TradePortalConstants.PYMT_UPLOAD_VALIDATED_WITH_ERRORS)) {
    out.print(resMgr.getText("InvoiceDetail.InvoiceNumber",
            TradePortalConstants.TEXT_BUNDLE)+" " +logFileName[0]+" - "+resMgr.getText("PoUploadLogDetail.ErrorLogText",
                    TradePortalConstants.TEXT_BUNDLE)); 
  }
  else {
    out.print(resMgr.getText("InvoiceDetail.InvoiceNumber",
            TradePortalConstants.TEXT_BUNDLE)+" " +logFileName[0]+" - "+resMgr.getText("InvoiceMgmtPaymentFileUploadList.UploadLog",
                    TradePortalConstants.TEXT_BUNDLE));
  }
%>
      </span>
      <span class="pageSubHeader-right">

        <%--cquinton 10/31/2012 ir#7015 change return link to close button when no close button present.
            note this does NOT include an icon as in the sidebar--%>
<%
  //cquinton 1/18/2013 replace old close action behavior with page flow logic
  SessionWebBean.PageFlowRef backPage = userSession.peekPageBack();
  String returnAction = backPage.getLinkAction();
  String returnParms = backPage.getLinkParametersString() + "&returning=true";
  String closeLink = formMgr.getLinkAsUrl(returnAction,returnParms,response);
%>
        <button data-dojo-type="dijit.form.Button" name="CloseButton" id="CloseButton" type="button" class="pageSubHeaderButton">
          <%=resMgr.getText("common.CloseText",TradePortalConstants.TEXT_BUNDLE)%>
          <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
            document.location.href = "<%= closeLink %>";
          </script>
        </button> 
        <%=widgetFactory.createHoverHelp("CloseButton", "CloseHoverText") %>
      </span>
      <div style="clear:both;"></div>
    </div>

<form name="InvoiceUploadLogDetailForm" method="POST" action="<%= formMgr.getSubmitAction(response) %>">
<input type=hidden value="" name=buttonName>
<div class="formContentNoSidebar">
<%
     // Store this parameter so it can be passed back into the page from somewhere else
     // secureParms.put("instrument_oid", instrument.getAttribute("instrument_oid"));
     secureParms.put("invoice_file_upload_oid", uploadOid);
     secureParms.put("user_oid", thisUser.getAttribute("user_oid"));
     //secureParms.put("instrument_type_code", instrument.getAttribute("instrument_type_code"));
 %>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
	
	 <tr>
		<td nowrap>		  
          <%=widgetFactory.createTextField("User",
					"PaymentUploadLogDetail.User",
					thisUser.getAttribute("user_identifier"), "", true,
					false, false, "", "", "")%>
		</td>
	</tr>
	<tr>		
		<td>&nbsp;</td>
	</tr>
	
	<tr>
      <td nowrap>
          <%=widgetFactory.createTextField(
					"ValidationStatus",
					"PaymentUploadLogDetail.ValidationStatus",
					ReferenceDataManager.getRefDataMgr().getDescr(
							TradePortalConstants.VALIDATION_STATUS,
							validationStatus), "", true, false, false,
					"", "", "")%>
      </td>
    </tr>
	<tr>		
		<td>&nbsp;</td>
	</tr>
	
	<tr>      
      <td nowrap>   
		  <%=widgetFactory.createTextField("CompletionDate",
					"PaymentUploadLogDetail.CompletionDate",
					TPDateTimeUtility.convertGMTDateTimeForTimezoneAndFormat(
							fileUpload.getAttribute("completion_timestamp") == null ? "" : fileUpload.getAttribute("completion_timestamp") ,
							TPDateTimeUtility.SHORT,
							resMgr.getResourceLocale(), userTimeZone), "",
					true, false, false, "", "", "")%>
      </td>
    </tr>
	<tr>		
		<td>&nbsp;</td>
	</tr>
	
<% if (!TradePortalConstants.PYMT_UPLOAD_VALIDATION_SUCCESSFUL.equals(validationStatus)) {
      if (TradePortalConstants.PYMT_UPLOAD_VALIDATION_FAILED.equals(validationStatus) &&
          InstrumentServices.isNotBlank(fileUpload.getAttribute("error_msg"))) {
         //BSL IR NNUM040229447 04/02/2012 END
%>

	 <tr>
	      <td nowrap>
	          <%=widgetFactory.createTextField("Errors",
							"PaymentUploadLogDetail.Errors",
							fileUpload.getAttribute("error_msg"), "",
							true, false, false, "", "", "")%>
          </td>
	  </tr>
<%
	}
	else {
%>
	
	<tr>	      
	      <td nowrap>
	      	  <%=widgetFactory.createTextField("UploadedPOs",
							"InvoiceUploadLogDetail.UploadedInvoices",
							Integer.toString(uploadNumber), "", true,
							false, false, "", "", "")%>
          </td>
	    </tr>
	<tr>		
		<td>&nbsp;</td>
	</tr>
	<tr>
	      <td nowrap>
	      	<%=widgetFactory.createTextField("FailedPOs",
							"InvoiceUploadLogDetail.FailedInvoices",
							Integer.toString(invFailed), "", true, false,
							false, "", "", "")%>
          </td>
	    </tr>
	
<% 	}
   }
%>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
</table>
<div class="formItem">
  <%
  	if (InstrumentServices.isNotBlank(xmlStr)) {
  %>
  <%=widgetFactory.createSubLabel("POUploadLogDetail.InstrumentList")%>
	        
   <%
   	displayLogGrid = true;
   		gridLogHtml = dgFactory.createDataGrid(
   				"INVUploadLogDetailDataGridID",
   				"INVUploadLogDetailDataGrid", null);
   		gridLogLayout = dgFactory
   				.createGridLayout("INVUploadLogDetailDataGrid");
   %>
   <%=gridLogHtml%>
 <%
 	}
  //IR IR 15470 start
 String pSql = " (NVL(displayable,?) != ? ) and p_invoice_file_upload_uoid = ?";
  List<Object> sqlParamsInvUp = new ArrayList();
  sqlParamsInvUp.add(TradePortalConstants.INDICATOR_YES);
  sqlParamsInvUp.add(TradePortalConstants.INDICATOR_NO);
  sqlParamsInvUp.add(uploadOid);
  
int errCount = DatabaseQueryBean.getCount("invoice_id", "INVOICE_UPLOAD_ERROR_LOG", pSql,false,sqlParamsInvUp);

 //if (errCount != 0){
 %> 
 <%-- 
<%=widgetFactory.createSubLabel("InvoiceUploadLogDetail.ValidationWithErrorText")%>
	        
   <%
   	displayGroupLogGrid = true;
   		gridGroupLogHtml = dgFactory.createDataGrid(
   				"INVUploadLogGroupDetailDataGridID",
   				"INVUploadLogGroupDetailDataGrid", null);
   		gridGroupLogLayout = dgFactory
   				.createGridLayout("INVUploadLogGroupDetailDataGrid");
   %>
   <%=gridGroupLogHtml%>
   --%>
<%
// }
//IR IR 15470 end

//BSL IR NNUM040229447 04/02/2012 BEGIN
//if (!validationStatus.equals(TradePortalConstants.PAYBACK_STATUS_SUCCESS))
//   {
//   	if ( (validationStatus.equals(TradePortalConstants.INVOICE_STATUS_FAILED)) &&
// 	     (InstrumentServices.isNotBlank(fileUpload.getAttribute("error_msg"))) )
// 	{}
// 	else
// 	{
	//IR 15470 -start
if(errCount>0 || TradePortalConstants.PYMT_UPLOAD_VALIDATION_FAILED.equals(validationStatus)){
//if (!TradePortalConstants.PYMT_UPLOAD_VALIDATION_SUCCESSFUL.equals(validationStatus)) {

	//if (!TradePortalConstants.PYMT_UPLOAD_VALIDATION_FAILED.equals(validationStatus) ||
	//		InstrumentServices.isBlank(fileUpload.getAttribute("error_msg"))) {
	//IR 15470 -end
//BSL IR NNUM040229447 04/02/2012 END
%>

<span style="font-style:italic;">
<%=widgetFactory.createSubLabel("InvoiceUploadLogDetail.ValidationWithErrorText")%>

</span>
<% if (request.getParameter("UploadOid") != null)
	//session.removeAttribute("InvoiceMgmtFileUploadErrorsListView.xml");


      displayErrorGrid = true;
      gridHtml = dgFactory.createDataGrid("InvoiceMgmtFileUploadErrorsDataGridId","InvoiceMgmtFileUploadErrorsDataGrid", null);
      gridLayout = dgFactory.createGridLayout("InvoiceMgmtFileUploadErrorsDataGrid");

%> 

         <%=gridHtml%> 
	 
 <%	 }
 // }
%>

      <%= formMgr.getFormInstanceAsInputField("InvoiceUploadLogDetailForm", secureParms) %>
      </div>
      </div>
    </form>

  </div>
</div>

<%--cquinton Portal Refresh - start--%>
<jsp:include page="/common/Footer.jsp">
  <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
  <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>
<%--cquinton Portal Refresh - end--%>

 <script type="text/javascript">
 require(["t360/OnDemandGrid", "dojo/dom-construct"], function(onDemandGrid, domConstruct){
<% 
if(errCount>0 || TradePortalConstants.PYMT_UPLOAD_VALIDATION_FAILED.equals(validationStatus)){
  //if (displayErrorGrid) {
%>
<%--IR 29862 -Vertical scrollbar to appear when longer error messages exist and are wrapped.  --%>
require(["t360/OnDemandGrid", "dojo/dom-construct"],
	      function(onDemandGrid, domConstruct) {
 
		
    var gridLayout = <%=gridLayout%>;
    var initSearchParms="uploadOid=<%=StringFunction.xssCharsToHtml(uploadOid)%>";
    var viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("InvoiceMgmtFileUploadErrorsDataView",userSession.getSecretKey())%>";  <%--Rel9.2 IR T36000032596 --%>
    var InvMgmFileUpload =  onDemandGrid.createOnDemandGrid("InvoiceMgmtFileUploadErrorsDataGridId", viewName,gridLayout, initSearchParms,'-1'); 
  	<% if(errCount==20){%>
    InvMgmFileUpload.set('autoHeight',19);
    <%}%>  
   
});
<% 
  } if (displayLogGrid) {
%>			
  var gridLogLayout = <%=gridLogLayout%>;
  var initLogSearchParms='xmlStr=<%=xmlStr%>';
  var viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("INVUploadLogDetailDataView",userSession.getSecretKey())%>";  <%--Rel9.2 IR T36000032596 --%>
  onDemandGrid.createOnDemandGrid("INVUploadLogDetailDataGridID", viewName,gridLogLayout, initLogSearchParms,'-1'); 
  <%}
  %>  
}); 
<%--IR 15470 -comment InvoiceMgmtFileUploadErrorsDataView can be used instead
<%}if (displayGroupLogGrid) {
%>			
	var gridGroupLogLayout = <%=gridGroupLogLayout%>;
	var initGroupLogSearchParms="uploadOid=<%=uploadOid%>";
	createDataGrid("INVUploadLogGroupDetailDataGridID","INVUploadLogGroupDetailDataView", gridGroupLogLayout, initGroupLogSearchParms);
--%>


</script> 
<% 
if(errCount>0 || TradePortalConstants.PYMT_UPLOAD_VALIDATION_FAILED.equals(validationStatus)){
%>
<jsp:include page="/common/dGridShowCountFooter.jsp">
  <jsp:param name="gridId" value="InvoiceMgmtFileUploadErrorsDataGridId" />
</jsp:include>
<% 
  } if (displayLogGrid) {
%>
<jsp:include page="/common/dGridShowCountFooter.jsp">
  <jsp:param name="gridId" value="INVUploadLogDetailDataGridID" />
</jsp:include>
 <%
 }
 %> 
</body>
</html>

<%
   // Finally, reset the cached document to eliminate carryover of
   // information to the next visit of this page.
   xmlDoc = formMgr.getFromDocCache();

   xmlDoc.removeAllChildren("/");

   formMgr.storeInDocCache("default.doc", xmlDoc);
%>
