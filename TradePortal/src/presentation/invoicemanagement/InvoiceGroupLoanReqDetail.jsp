<%--
*******************************************************************************
                        Invoice Group Detail

  Description:
     This page is used to display the invoices of an invoice group.

*******************************************************************************
--%>

<%--
 *
 *     Copyright   2001
 *     American Management Systems, Incorporated
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.html.*, java.util.*" %>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session">
</jsp:useBean>


<%-- ************** Data retrieval page setup begins here ****************  --%>

<%
StringBuffer      onLoad                = new StringBuffer();
String            helpSensitiveLink     = null;
String            currentTabLink        = null;
String            currentTab            = TradePortalConstants.INV_GROUP_TAB;//BSL IR NLUM040376572 04/06/2012 CHANGE
String            formName              = null;
String            tabOn                 = null;
String            userOrgOid            = null;
String            userOid               = null;
String            userSecurityRights    = null;
String            userSecurityType      = null;
StringBuffer      dynamicWhereClause           = new StringBuffer();
StringBuffer      dropdownOptions              = new StringBuffer();
int               totalOrganizations           = 0;
StringBuffer      extraTags                    = new StringBuffer();
StringBuffer      newLink                      = new StringBuffer();
StringBuffer      newUploadLink                = new StringBuffer();

String            userDefaultWipView           = null;
String            selectedWorkflow             = null;
String            selectedStatus               = "";
String            loginLocale                  = null;
Vector            codesToExclude               = null;
String            gridID                       = null;
String            gridName                     = null;
String            viewName                     = null;
Hashtable         secureParms					 = new Hashtable();
	System.out.println("******************** Invoice GroupLoan Request Details.jsp.......................");
   userSession.setCurrentPrimaryNavigation(resMgr.getText("NavigationBar.InvoiceManagement",
                                         TradePortalConstants.TEXT_BUNDLE));

   //cquinton 1/18/2013 set return page
   if ( "true".equals( request.getParameter("returning") ) ) {
     userSession.pageBack(); //to keep it correct
   }
   else {
     userSession.addPage("goToInvoiceManagement");
   }

   //cquinton 1/18/2013 remove old close action stuff

   userSecurityRights = userSession.getSecurityRights();
   userSecurityType   = userSession.getSecurityType();
   userOrgOid         = StringFunction.xssCharsToHtml(userSession.getOwnerOrgOid());
   userOid            = StringFunction.xssCharsToHtml(userSession.getUserOid());
   secureParms.put("timeZone",   userSession.getTimeZone());
   secureParms.put("clientBankOid",   userSession.getClientBankOid());
   secureParms.put("baseCurrencyCode", userSession.getBaseCurrencyCode()); 
   session.setAttribute("documentImageFileUploadPageOriginator", formMgr.getCurrPage());
   // getting the user security type to display authorization button or not.
   boolean isAdminUser;
   if (userSession.getSavedUserSession() == null) {
      isAdminUser = TradePortalConstants.ADMIN.equals(userSession.getSecurityType());
   } else {
      isAdminUser = TradePortalConstants.ADMIN.equals(userSession.getSavedUserSessionSecurityType());
   }

   CorporateOrganizationWebBean corpOrg = beanMgr.createBean(CorporateOrganizationWebBean.class, "CorporateOrganization");
   corpOrg.getById(userOrgOid);
   UserWebBean thisUser = beanMgr.createBean(UserWebBean.class, "User");
   thisUser.getById(userOid);

   boolean groupingEnabled = TradePortalConstants.INVOICES_GROUPED_TPR.equals(corpOrg.getAttribute("allow_grouped_invoice_upload"));

   if (groupingEnabled) {
	   session.removeAttribute("fromInvoiceGroupDetail");
   }

   formName = "UploadInvoiceListForm";
   helpSensitiveLink = OnlineHelp.createContextSensitiveLink("customer/invoices.htm", resMgr, userSession);
   loginLocale = userSession.getUserLocale();
	//SHR RM-15 Invoice Groups- Invoice List Issues
   String invoiceGroupOid = request.getParameter("invoice_group_oid");
   if(InstrumentServices.isNotBlank(invoiceGroupOid)){
	   session.setAttribute("invGrpOid",invoiceGroupOid);
	}
   else{
	   invoiceGroupOid =(String) session.getAttribute("invGrpOid");
   }

   secureParms.put("UserOid", userSession.getUserOid());
   secureParms.put("SecurityRights",   userSecurityRights);
   secureParms.put("ownerOrg",   userOrgOid);
   if(InstrumentServices.isNotBlank(invoiceGroupOid))
   secureParms.put("invoice_group_oid", invoiceGroupOid);
 	//SHR RM-15 Invoice Groups- Invoice List Issues
   if (invoiceGroupOid != null) {
      invoiceGroupOid = EncryptDecrypt.decryptStringUsingTripleDes(invoiceGroupOid, userSession.getSecretKey());
   }

   InvoiceGroupWebBean invoiceGroup = beanMgr.createBean(InvoiceGroupWebBean.class, "InvoiceGroup");
   invoiceGroup.getById(invoiceGroupOid);
   session.setAttribute("fromInvoiceGroupDetail", "GD");
   dynamicWhereClause.append(" and i.a_corp_org_oid = ").append(userOrgOid);
   if (groupingEnabled) {
      dynamicWhereClause.append(" and i.a_invoice_group_oid = ").append(invoiceGroupOid);
   }

   else if (!TradePortalConstants.STATUS_ALL.equals(selectedStatus)) {
      dynamicWhereClause.append(" and (i.invoice_status = '");
      dynamicWhereClause.append(selectedStatus);
      dynamicWhereClause.append("'");
      if (TradePortalConstants.UPLOAD_INV_STATUS_AUTH.equals(selectedStatus)) {
          dynamicWhereClause.append(" or i.invoice_status = '");
          dynamicWhereClause.append(TradePortalConstants.UPLOAD_INV_STATUS_FIN_AUTH);
          dynamicWhereClause.append("'");
      }
      else if (TradePortalConstants.UPLOAD_INV_STATUS_PAR_AUTH.equals(selectedStatus)) {
          dynamicWhereClause.append(" or i.invoice_status = '");
          dynamicWhereClause.append(TradePortalConstants.UPLOAD_INV_STATUS_FIN_PAR_AUTH);
          dynamicWhereClause.append("'");
      }
      else if (TradePortalConstants.UPLOAD_INV_STATUS_AUTH_FAILED.equals(selectedStatus)) {
          dynamicWhereClause.append(" or i.invoice_status = '");
          dynamicWhereClause.append(TradePortalConstants.UPLOAD_INV_STATUS_FIN_AUTH_FAILED);
          dynamicWhereClause.append("'");
      }
      dynamicWhereClause.append(")");
   }


%>
<%
  // Store values such as the userid, his security rights, and his org in a secure
  // hashtable for the form.  The mediator needs this information.
//jgadela R92 - SQL INJECTION FIX
   String groupDueDatesql = "SELECT due_date FROM invoice_group_view WHERE invoice_group_oid = ? ";
   DocumentHandler invoiceDueDateDoc = DatabaseQueryBean.getXmlResultSet(groupDueDatesql.toString(), false, new Object[]{invoiceGroupOid});
   String dueDate = "";
   String paymentDate = "";
   if(invoiceDueDateDoc !=null){
	   dueDate = (invoiceDueDateDoc.getFragment("/ResultSetRecord")).getAttribute("DUE_DATE");
   }
   if(StringFunction.isBlank(dueDate)){
	   dueDate = invoiceGroup.getAttribute("payment_date");
   }
   
  StringBuilder invoicesForHeader = new StringBuilder(resMgr.getText("UploadInvoiceDetail.InvoicesFor", TradePortalConstants.TEXT_BUNDLE));
  invoicesForHeader.append(" ").append(" - ");
  invoicesForHeader.append(" ").append(invoiceGroup.getAttribute("trading_partner_name"));
  invoicesForHeader.append(" ").append(" - ");
  invoicesForHeader.append(" ").append(invoiceGroup.getAttribute("currency"));
  invoicesForHeader.append(" ").append(" - ");
  invoicesForHeader.append(" ").append(resMgr.getText("InvoiceDefinition.DueDate", TradePortalConstants.TEXT_BUNDLE)+":");
  invoicesForHeader.append(" ").append(TPDateTimeUtility.formatDate(dueDate, TPDateTimeUtility.SHORT, loginLocale));
  
  selectedStatus = invoiceGroup.getAttribute("invoice_status");
  System.out.println("selectedStatus:"+selectedStatus);
  String resourcePrefix ="";
     gridID = "InvoiceLoanRequestId";
	 gridName = "InvoiceLoanRequestListDataGrid";
	 viewName = "InvoiceLoanRequestListDataView";
	 resourcePrefix = "UploadInvoices";

 System.out.println("gridName:"+gridName+"\t viewName:"+viewName);
 
//IR 29690 start
 String[] opBankOid = {corpOrg.getAttribute("first_op_bank_org"),
                      corpOrg.getAttribute("second_op_bank_org"),
                      corpOrg.getAttribute("third_op_bank_org"),
                      corpOrg.getAttribute("fourth_op_bank_org")};
 
 //do a local query to get names for operational bank orgs
 //could do this as dataview, but this is simpler - we don't expect this to 
 //change on this dialog via search.
 DocumentHandler oboListDoc = null;
//jgadela R92 - SQL INJECTION FIX
 List<Object> sqlParams = new ArrayList();
 StringBuffer in = new StringBuffer("?");
 sqlParams.add(opBankOid[0]);


 for (int oboCount=1; oboCount<4; oboCount++) {
	  
   if (StringFunction.isBlank(opBankOid[oboCount])) {
     break;
   }
   else {
       in.append(", ?");
       sqlParams.add(opBankOid[oboCount]);
   }
 }
 try {
	    StringBuilder sql = new StringBuilder();
	    sql.append("select ORGANIZATION_OID, NAME");
	    sql.append(" from OPERATIONAL_BANK_ORG");
	    sql.append(" where ORGANIZATION_OID in (").append(in).append(")");
	    sql.append(" order by name");
		System.out.println("sql.toString():"+sql.toString());
	    oboListDoc = DatabaseQueryBean.getXmlResultSet(sql.toString(), true, sqlParams);

	  } catch (AmsException e) {
	    //todo: should throw an error
	    Debug.debug("queryListView threw an exception");
	    e.printStackTrace();
 }
	 String orgOid =""; 
	 int bankOrgList =0; 
 StringBuffer bbGridData = new StringBuffer();
 if ( oboListDoc != null ) {
   Vector oboList = oboListDoc.getFragments("/ResultSetRecord");
   bankOrgList =oboList.size(); 
   for ( int oboIdx = 0; oboIdx<oboList.size(); oboIdx++ ) {
     DocumentHandler oboDoc = (DocumentHandler) oboList.get(oboIdx);
      orgOid = oboDoc.getAttribute("/ORGANIZATION_OID");
     String orgName = oboDoc.getAttribute("/NAME");
			bbGridData.append("<option value='").append(orgOid).append("'>");
           bbGridData.append(StringFunction.xssCharsToHtml(orgName));
           bbGridData.append("</option>");
   }
 }
 
//IR 29690 end
%>

<%-- ********************* HTML for page begins here *********************  --%>
<% WidgetFactory widgetFactory = new WidgetFactory(resMgr); %>


<%-- Body tag included as part of common header --%>

<jsp:include page="/common/Header.jsp">
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeErrorSectionFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="additionalOnLoad" value="<%=onLoad.toString()%>" />
</jsp:include>

<div class="pageMainNoSidebar">
  <%-- IR T36000030360: (gridInvoiceFooterDivHidden) Hide page content, untill widgets get parsed. --%>
  <div id="InvoiceMgtDivID" class="pageContent gridInvoiceFooterDivHidden">
    <jsp:include page="/common/PageHeader.jsp">
      <jsp:param name="titleKey" value="InvoiceMgmt.RecInvGroupInvList" />
      <jsp:param name="helpUrl" value="customer/receivables_grouped_invoice_list.htm" />
    </jsp:include>

    <%--cquinton 11/1/2012 ir#7015 replace return link with close button--%>
    <div class="subHeaderDivider"></div>
    <div class="pageSubHeader">
      <span class="pageSubHeaderItem">
        <%= invoicesForHeader %>
      </span>
      <span class="pageSubHeader-right">
        <%--cquinton 10/31/2012 ir#7015 change return link to close button when no close button present.
            note this does NOT include an icon as in the sidebar--%>
        <button data-dojo-type="dijit.form.Button" name="CloseButton" id="CloseButton" type="button" class="pageSubHeaderButton">
          <%=resMgr.getText("common.CloseText",TradePortalConstants.TEXT_BUNDLE)%>
          <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
            document.location.href =
              '<%= formMgr.getLinkAsUrl("goToInvoiceManagement", response) %>';
          </script>
        </button>
        <%=widgetFactory.createHoverHelp("CloseButton", "CloseHoverText") %>
      </span>
      <div style="clear:both;"></div>
    </div>

<% //AAlubala - 04/23/2012 - reauthenticate if necessary
    String certAuthURL = ""; //needed for openReauth frag
    Cache reCertCache = (Cache)TPCacheManager.getInstance().getCache(TradePortalConstants.CLIENT_BANK_CACHE);
    DocumentHandler CBCResult = (DocumentHandler)reCertCache.get(userSession.getClientBankOid());
    String requireTranAuth = CBCResult.getAttribute("/ResultSetRecord(0)/REQUIRE_TRAN_AUTH");
    boolean InvoiceRequireAuth = InstrumentAuthentication.requireTransactionAuthentication(
        requireTranAuth,InstrumentAuthentication.TRAN_AUTH__INVOICE_MGNT_FIN_INVOICE);

    String authorizeLink = "";

    //CR711 - 04/18/2012
    String proxyAuthLink = "";
%>


<form name="<%=formName%>" method="POST" action="<%= formMgr.getSubmitAction(response) %>" style="margin-bottom: 0px;">
    <jsp:include page="/common/ErrorSection.jsp" />
    <div class="formContentNoSidebar">
   <input type=hidden value="" name=buttonName>
   <input type="hidden" name="payment_date" />
   <input type="hidden" name="loanType" />
   <input type="hidden" name="bankBranch" />
   <input type="hidden" value="yes" name="fromRecDetail">
<%--   <input type="hidden" value="yes" name="fromPayDetail">--%>

<%  //AAlubala VASCO reauthentication hidden fields
    if ( InvoiceRequireAuth) {
%>

      <input type=hidden name="reCertification" value="<%=TradePortalConstants.INDICATOR_NO%>">
      <input type=hidden name="reCertOK">
      <input type=hidden name="logonResponse">
      <input type=hidden name="logonCertificate">

<%
    }
%>

<%
    if (InvoiceRequireAuth) {
%>
  <%@ include file="/logon/fragments/openReauthenticationWindow.frag" %>
  <%
        //NOTE: although there are lists on the screen, this is specific to the pay remit
        authorizeLink = "javascript:openReauthenticationWindowForList('" + certAuthURL + "'," +
            "'" + formName + "','" + TradePortalConstants.BUTTON_AUTHORIZE + "'," +
            "'Invoice')";

        proxyAuthLink = "javascript:openReauthenticationWindowForList('" + certAuthURL + "'," +
	    "'" + formName + "','" + TradePortalConstants.BUTTON_PROXY_AUTHORIZE + "'," +
            "'Invoice')";
    }
    //vasco end
  %>
  <%
  
  //SHR print button start
  String linkArgs[] = {"","","","","",""};
          String arg = "key~"+viewName+ "`key~" + resourcePrefix;

  linkArgs[0] = TradePortalConstants.PDF_LIST;
          linkArgs[1] = EncryptDecrypt.encryptStringUsingTripleDes(arg);//Use common key for encryption of parameter to doc producer
          linkArgs[2] = InstrumentServices.isNotBlank(resMgr.getResourceLocale())? resMgr.getResourceLocale():"en_US";
          //loginLocale;
          linkArgs[3] = userSession.getBrandingDirectory();
          String linkString =  formMgr.getDocumentLinkAsURLInFrame(resMgr.getText("UploadInvoiceDetail.PrintInvoiceDetails",
                  TradePortalConstants.TEXT_BUNDLE),
                  TradePortalConstants.PDF_LIST,
                  linkArgs,
                  response) ;

          //SHR end
 %>
 <%-- SHR --%>
 <div class="gridSearch">
  <div class="searchHeader">
   <span class="searchHeaderCriteria">
	<%=resMgr.getText("InvoiceList.Invoices",TradePortalConstants.TEXT_BUNDLE)%>

	</span>
 	<span class="searchHeaderActions">
 	<jsp:include page="/common/gridShowCount.jsp">
        <jsp:param name="gridId" value='<%=gridID%>'/>
      </jsp:include>
    <button data-dojo-type="dijit.form.Button" name="print" id="print" type="button" class="pageSubHeaderButton">
          <%=resMgr.getText("common.PrintText",TradePortalConstants.TEXT_BUNDLE)%>
          <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
          window.open( "<%=linkString%>");
		  </script>
        </button>
         <%=widgetFactory.createHoverHelp("print", "PrintHoverText") %>
 	  <span id="invoiceGroupListRefresh" class="searchHeaderRefresh"></span>
 	  <%=widgetFactory.createHoverHelp("invoiceGroupListRefresh", "RefreshImageLinkHoverText") %>
      <span id="invoiceGroupListGridEdit" class="searchHeaderEdit"></span>
      <%=widgetFactory.createHoverHelp("invoiceGroupListGridEdit","CustomizeListImageLinkHoverText") %>
    </span>
    <div style="clear:both;"></div>

  <%
  DataGridFactory dgFactory = new DataGridFactory(resMgr, userSession, formMgr,beanMgr, response);
  String gridHtml = dgFactory.createDataGridMultiFooter(gridID,gridName,"5");
 %>
 
 </div>
<%=gridHtml%>

<%-- <%@ include file="/invoicemanagement/fragments/InvoiceManagement_Invoices.frag"%>--%>
<%  System.out.println(" formName:"+formName+"\t secureParms:"+secureParms); %>
<%= formMgr.getFormInstanceAsInputField(formName, secureParms) %>

</div>
</div>
</form>
</div>
</div>
<%--cquinton Portal Refresh - start--%>
<jsp:include page="/common/Footer.jsp">
  <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
  <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>

<script type="text/javascript" src="/portal/js/datagrid.js"></script>
<div id="InvoiceGroupListDialogID" ></div>
<%
  String gridLayout = dgFactory.createGridLayout(gridID,gridName);
%>

<script type="text/javascript">

  <%--get grid layout from the data grid factory--%>
  var gridLayout = <%= gridLayout %>;
  
 var gridID = '<%= gridID %>';
  var gridName = '<%= gridName %>';
  var viewName = '<%=EncryptDecrypt.encryptStringUsingTripleDes(viewName,userSession.getSecretKey())%>';  <%--Rel9.2 IR T36000032596 --%>
  var formName = "UploadInvoiceListForm";
  <%--set the initial search parms--%>
  var init='userOid=<%=userOid%>&userOrgOid=<%=userOrgOid%>&invoiceGroupOid=<%=StringFunction.xssCharsToHtml(invoiceGroupOid)%>&selectedStatus=<%=selectedStatus%>';
  var InvoiceListId = createDataGrid(gridID, viewName, gridLayout, init);

 function assignLoanTypeToInv(){		 
  	require(["t360/dialog"], function(dialog) {
	 	      dialog.open('InvoiceGroupListDialogID', '<%=resMgr.getText("UploadInvoiceAction.AssignLoanType", TradePortalConstants.TEXT_BUNDLE)%>',
		                      'AssignLoanType.jsp',
		                      ['selectedCount'], [getSelectedGridRowKeys("InvoiceLoanRequestId").length], 
		                      'select', this.createNewLoanTypeValue);
		  }); 
 }
  
function createNewLoanTypeValue(userLoanType) {	
	
	var theForm = document.getElementsByName(formName)[0];	
	theForm.loanType.value=userLoanType;	
	var rowKeys = getSelectedGridRowKeys("InvoiceLoanRequestId");
	submitFormWithParms("ApplyPaymentDateToSelectionForm","AssignLoanType", "checkbox",rowKeys);
	
}

 function performLoanRequestAction(pressedButton){  
	  var rowKeys = getSelectedGridRowKeys("InvoiceLoanRequestId");
	    <%--submit the form with rowkeys
	        becuase rowKeys is an array, the parameter name for
	        each will be checkbox + an iteration number -
	        i.e. checkbox0, checkbox1, checkbox2, etc --%>
			
	 submitFormWithParms(formName, pressedButton, "checkbox", rowKeys);
 }

 function createInvLRCreateLRQ(bankBranchArray){	
	  rowKeys = getSelectedGridRowKeys(gridID);
		var items=getSelectedGridItems(gridID);

		if(rowKeys == ""){
			alert("Please pick a record and click 'Select'");
			return;
		}
		if ( bankBranchArray.length == 1 ) {
			createNewLoanRequestInstValue(bankBranchArray[0]);
		} else {
			require(["t360/dialog"], function(dialog ) {
		      dialog.open('InvoiceGroupListDialogID', '<%=resMgr.getText("bankBranchSelectorDialog.title", TradePortalConstants.TEXT_BUNDLE)%>',
		                  'bankBranchSelectorDialog.jsp',
		                  null, null, <%-- parameters --%>
		                  'select', this.createNewLoanRequestInstValue);
		    });
		 }
}
 function submitInvFormWithParms(bankBranchOid) {
		buttonName='ListCreateLRQ';
		parmName="checkbox";
		parmValue = rowKeys;
	      <%-- verify arguments at development time --%>
	      if ( formName == null ) {
	        alert('Problem submitting form: formName required');
	      }
	      if ( buttonName == null ) {
	        alert('Problem submitting form: buttonName required');
	      }
	      if ( parmName == null ) {
	        alert('Problem submitting form: parmName required');
	      }
	  
	      <%-- get the form and submit it --%>
	      if ( document.getElementsByName(formName).length > 0 ) {
	        var myForm = document.getElementsByName(formName)[0];
	        <%-- generate url parameters for the rowKeys and add to  --%>
	        <%-- form as hidden fields --%>
	        if (parmValue instanceof Array) {
	          for (var myParmIdx in parmValue) {
	            var myParmValue = parmValue[myParmIdx];
	            var myParmName = parmName + myParmIdx;
	            addParmToForm(myForm, myParmName, myParmValue);
	          }
	        }
	        else { <%-- assume a single --%>
	        	 addParmToForm(myForm, parmName, parmValue);
	          
	        }
	        myForm.bankBranch.value=bankBranchOid; 
	        var formNumber = getFormNumber(formName);
	        setButtonPressed(buttonName, formNumber); 
	        myForm.submit();
	      } else {
	        alert('Problem submitting form: form ' + formName + ' not found');
	      }
	    }
  
function createNewLoanRequestInstValue(bankBranchOid) {	
	
	submitInvFormWithParms(bankBranchOid);

}

 function applyPaymentDateToInvLR(){
	 
	    	require(["t360/dialog"], function(dialog) {
		 	      dialog.open('InvoiceGroupListDialogID', '<%=resMgr.getText("UploadInvoiceAction.UploadInvoiceApplyPaymentDateText", TradePortalConstants.TEXT_BUNDLE)%>',
			                      'AssignPaymentDate.jsp',
			                      ['selectedCount'], [getSelectedGridRowKeys("InvoiceLoanRequestId").length], 
			                      'select', this.createNewApplyPaymentValue);
			  }); 
 }

function createNewApplyPaymentValue(payDate) {
	 var theForm = document.getElementsByName(formName)[0];
   	theForm.payment_date.value=payDate;
   	var rowKeys = getSelectedGridRowKeys(gridID);
    submitFormWithParms(formName,"ApplyPaymentDate", "checkbox",rowKeys);
 }

 function searchInvoiceGroupList() {
	    require(["dojo/dom"],
	      function(dom){
	    	var searchParms = "userOrgOid="+<%=userOrgOid%>+"&invoiceGroupOid="+<%=StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(invoiceGroupOid))%>;

	        console.log("SearchParms: " + searchParms);
        searchDataGrid(gridID,viewName,
                searchParms);
	      });
	  }

 require(["dojo/query", "dojo/on", "t360/datagrid", "t360/popup", "dojo/domReady!"],
	      function(query, on, t360grid, t360popup){
	    query('#invoiceGroupListRefresh').on("click", function() {
	    	searchInvoiceGroupList();
	    });
   query('#invoiceGroupListGridEdit').on("click", function() {
     var columns = t360grid.getColumnsForCustomization(gridID);
     var parmNames = [ "gridId", "gridName", "columns" ];
     var parmVals = [ gridID, gridName, columns ];
     t360popup.open(
       'invoiceGroupListGridEdit', 'gridCustomizationPopup.jsp',
       parmNames, parmVals,
       null, null);  <%-- callbacks --%>
   });
  });

require(["dojo/dom-class", "dojo/query", "dojo/on", "t360/datagrid", "t360/popup", "dijit/registry", "dojo/ready", "dojo/domReady!"],
function(domClass, query, on, t360grid, t360popup, registry,ready) {
	 ready(function() {
		<%-- IR T36000030360: Hide page content, untill widgets get parsed. --%>
			domClass.add("InvoiceMgtDivID", "gridInvoiceFooterDivVisible");
			domClass.remove("InvoiceMgtDivID", "gridInvoiceFooterDivHidden");
		  if(registry.byId("InvoiceLoanRequestId")){
			  var invGrid = registry.byId("InvoiceLoanRequestId");			   
			  on(invGrid,"selectionChanged", function() {
				  if (registry.byId('UploadInvoices_Actions')){
					  t360grid.enableFooterItemOnSelectionGreaterThan(invGrid,"UploadInvoices_Actions",0);
				  }
				  if (registry.byId('UploadInvoices_Document')){
					  t360grid.enableFooterItemOnSelectionGreaterThan(invGrid,"UploadInvoices_Document",0);
				  }
			 });
		 } 
	 });
 });


</script>
<jsp:include page="/common/gridShowCountFooter.jsp">
  <jsp:param name="gridId" value='<%=gridID%>'/>
</jsp:include>

</body>

<%
  // Finally, reset the cached document to eliminate carryover of
  // information to the next visit of this page.
  formMgr.storeInDocCache("default.doc", new DocumentHandler());
%>

</html>
