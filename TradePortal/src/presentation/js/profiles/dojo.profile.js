dependencies = {
    stripConsole    : "normal",
    selectorEngine  : "acme",
    optimize        : "closure",
    layerOptimize   : "closure",
    cssOptimize     : "comments.keepLines",
    mini            : true,
    internStrings   : true,
    localeList      : "en-us",
    releaseName     : "dojo.custom",
    action          : "release",
    optimize        : "shrinksafe",
    layerOptimize   : "shrinksafe",
    
    
    layers : [
        {
            name         : "dojo.js",
            dependencies : [
		"dijit.Dialog",
		"dijit.Tooltip",
		"dojox.grid.LazyTreeGrid",
		"dojox.grid._CheckBoxSelector",
		"dojox.grid._RadioSelector",
		"dojox.data.QueryReadStore",
		"dijit.form.Button",
		"dijit.Tooltip",
		"dijit.PopupMenuBarItem",
		"dijit.MenuBarItem",
		"dijit.layout.ContentPane",
		"dijit.form.ValidationTextBox",
		"dijit.form.TextBox",
		"dijit.form.NumberTextBox",
		"dijit.form.Textarea",
		"dijit.form.SimpleTextarea",
		"dijit.form.DateTextBox",
		"dijit.form.CurrencyTextBox",
		"dijit.form.CheckBox",
		"dijit.form.RadioButton",
		"dijit.TitlePane",
		"dijit.form.Form",
		"dijit.form.NumberSpinner",
		"dijit.form.FilteringSelect",
		"dijit.MenuBar",
		"dojox.layout.FloatingPane",
		"dojo.data.ItemFileReadStore",
		"dojo.data.ItemFileWriteStore",
		"dojo.store.Memory",
		"dojo.data.ObjectStore",
		"dojox.layout.ResizeHandle",
		"dojox.layout.Dock",
		"dijit.form.nls.ComboBox",
		"dijit.TooltipDialog"
		
		
		
            ]
        }


    ],
    prefixes: [ [ "dijit", "../dijit" ], [ "dojox", "../dojox" ] ]
}