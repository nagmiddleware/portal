
  require(["dijit/registry", "dojo/ready", "dojo/dom"], function(registry, ready, dom ) {
	  
	var settlemnt = {
			
		// this function is used to clear 'FX Contract' data if Use FX contract radio button is un-selected.  
		clearFXContractDetail: function() {
				  var settleCoveredByFecNum = registry.byId("settleCoveredByFecNum");
				   if (settleCoveredByFecNum) {
					   settleCoveredByFecNum.set('value', '');
				   }
				   var settleFecRate = registry.byId("settleFecRate");
				   if (settleFecRate) {
					   settleFecRate.set('value', '');
				   }
				   var settleFecCurrency = registry.byId("settleFecCurrency");
				   if (settleFecCurrency) {
					   settleFecCurrency.set('value', '');
				   }
	    },
		
		// this function is used to clear 'Other FX Instructions' area text if 'Other FX instructions' option
		// is un-selected. 
		clearOtherFXInstructionText: function() {
				  var settleOtherFecText = registry.byId("settleOtherFecText");
				   if (settleOtherFecText) {
					   settleOtherFecText.set('value', '');
				   }				   
	    },
	    
	    // this function is used to clear all debit account detail i.e Principle, Charges and Interest. depend on Instrument.
		clearAllDebitAccountDetail: function() {
				   var principalAccountOid = registry.byId("principalAccountOid");
				   if (principalAccountOid) {
					   principalAccountOid.set('value', '');
				   }				   
				   var chargesAccountOid = registry.byId("chargesAccountOid");
				   if (chargesAccountOid) {
					   chargesAccountOid.set('value', '');
				   }
				   var interestAccountOid = registry.byId("interestAccountOid");
				   if (interestAccountOid) {
					   interestAccountOid.set('value', '');
				   }
	    },
	    
	    clearPaymentDetail: function() {
	    	     var account = registry.byId("Account");
			     if (account) {
			    	 account.set("checked", false);  
			     }
			     var remitted = registry.byId("Remitted");
			     if (remitted) {
			    	 remitted.set("checked", false);  
			     }
        },
        
        setSettlementInstrReadOnly: function() {
           registry.byId('ourReference').setDisabled(true);
 		   registry.byId('applyPaymentOnDate').setDisabled(true);
     	   registry.byId('PayInFull').setDisabled(true);
     	   registry.byId('Finance').setDisabled(true);
     	   registry.byId('finRollCurr').setDisabled(true);
     	   registry.byId('ForFull').setDisabled(true);
     	   registry.byId('ForPartial').setDisabled(true);
     	   registry.byId('finRollPartialPayAmt').setDisabled(true);
     	   registry.byId('FinForDays').setDisabled(true);
     	   registry.byId('finRollNumberOfDays').setDisabled(true);
     	   registry.byId('FinAtMaturityDate').setDisabled(true);
     	   registry.byId('finRollMaturityDate').setDisabled(true);
     	   registry.byId('Account').setDisabled(true);
     	   registry.byId('principalAccountOid').setDisabled(true);
     	   registry.byId('chargesAccountOid').setDisabled(true);
     	   registry.byId('Remitted').setDisabled(true);
     	   registry.byId('otherAddlInstructionsInd').setDisabled(true);
     	   registry.byId('descrSettleOtherInstrDropDown').setDisabled(true);
     	   registry.byId('otherAddlInstructions').setDisabled(true);
     	   registry.byId('DailyRate').setDisabled(true);
    	   registry.byId('UseFXContract').setDisabled(true);
    	   registry.byId('settleCoveredByFecNum').setDisabled(true);
    	   registry.byId('settleFecRate').setDisabled(true);
    	   registry.byId('settleFecCurrency').setDisabled(true);
    	   registry.byId('OtherFXDetail').setDisabled(true);
    	   registry.byId('settlementFXPhraseDropDown').setDisabled(true);
    	   registry.byId('settleOtherFecText').setDisabled(true);
    	   
    	   settlemnt.clearSettlementDetail();
        },
	    
        setSettlementInstrEditable: function() {
           registry.byId('ourReference').setDisabled(false);
 		   registry.byId('applyPaymentOnDate').setDisabled(false);
     	   registry.byId('PayInFull').setDisabled(false);
     	   registry.byId('Finance').setDisabled(false);
     	   registry.byId('finRollCurr').setDisabled(false);
     	   registry.byId('ForFull').setDisabled(false);
     	   registry.byId('ForPartial').setDisabled(false);
     	   registry.byId('finRollPartialPayAmt').setDisabled(false);
     	   registry.byId('FinForDays').setDisabled(false);
     	   registry.byId('finRollNumberOfDays').setDisabled(false);
     	   registry.byId('FinAtMaturityDate').setDisabled(false);
     	   registry.byId('finRollMaturityDate').setDisabled(false);
     	   //registry.byId('Account').setDisabled(false);
     	   //registry.byId('principalAccountOid').setDisabled(false);
     	   //registry.byId('chargesAccountOid').setDisabled(false);
     	   registry.byId('Remitted').setDisabled(false);
     	   registry.byId('otherAddlInstructionsInd').setDisabled(false);
     	   registry.byId('descrSettleOtherInstrDropDown').setDisabled(false);
     	   registry.byId('otherAddlInstructions').setDisabled(false);
     	   registry.byId('DailyRate').setDisabled(false);
     	   registry.byId('UseFXContract').setDisabled(false);
     	   registry.byId('settleCoveredByFecNum').setDisabled(false);
     	   registry.byId('settleFecRate').setDisabled(false);
     	   registry.byId('settleFecCurrency').setDisabled(false);
     	   registry.byId('OtherFXDetail').setDisabled(false);
     	   registry.byId('settlementFXPhraseDropDown').setDisabled(false);
     	   registry.byId('settleOtherFecText').setDisabled(false);
         },
         
         setFinanceOrRollover: function() {
        	 var FinOrRollInd = registry.byId('Finance');
        	 if ( FinOrRollInd ) {
        		 FinOrRollInd.set("checked", true); 
        	 }
         },
         
         clearFinanceOrRolloverDetail: function() {
        	 registry.byId("ForFull").set("checked", false);
        	 registry.byId("ForPartial").set("checked", false);
        	 registry.byId("FinForDays").set("checked", false);
        	 registry.byId("FinAtMaturityDate").set("checked", false);
        	 registry.byId("finRollCurr").set('value', '');
         },
                  
         clearFXDetails: function() {
        	 registry.byId('DailyRate').set("checked", false);
        	 registry.byId('UseFXContract').set("checked", false);
        	 registry.byId('OtherFXDetail').set("checked", false);
         },
         
         clearSettlementDetail: function() {
        	 registry.byId('ourReference').set('value', '');
   		     registry.byId('applyPaymentOnDate').set('value', null);
   		     registry.byId('PayInFull').set("checked", false);
    	     registry.byId('Finance').set("checked", false);
    	     registry.byId('otherAddlInstructionsInd').set("checked", false);
    	     settlemnt.clearPaymentDetail();
   		     settlemnt.clearFXDetails();
         },
         
         accountDetailsReadOnly: function() {
      	   registry.byId('interestAccountOid').setDisabled(true);
      	   registry.byId('Account').setDisabled(true);
      	   registry.byId('principalAccountOid').setDisabled(true);
      	   registry.byId('chargesAccountOid').setDisabled(true);
      	   registry.byId('Remitted').setDisabled(true);     	   
      	   
      	   settlemnt.clearAllDebitAccountDetail();
         },	
         
         accountDetailsEditable: function() {
        	   registry.byId('interestAccountOid').setDisabled(false);
        	   registry.byId('Account').setDisabled(false);
        	   registry.byId('principalAccountOid').setDisabled(false);
        	   registry.byId('chargesAccountOid').setDisabled(false);
        	   registry.byId('Remitted').setDisabled(false); 
           }         
			  
	 };
	  
      ready(function() {
    	  
    	  var useFXContract = registry.byId("UseFXContract");
		   if (useFXContract) {
			   useFXContract.on("change", function(checkValue) {
				   // if 'Use FX Contract' is unselected, clear all related data for this option
				   if ( !checkValue ) {
    	             settlemnt.clearFXContractDetail();
				   }
			   });      
		   } 
		   
		   var settleCoveredByFecNum = registry.byId("settleCoveredByFecNum");
		   if (settleCoveredByFecNum) {
			   settleCoveredByFecNum.on("change", function(checkValue) {
				 // if Data is entered in text, then select the 'Use FX Contract' option
				 if ( checkValue.length > 0 ) {
					 registry.byId("UseFXContract").set("checked", true);
				  }
			   });      
		    }
		   
		   var settleFecRate = registry.byId("settleFecRate");
		   if (settleFecRate) {
			   settleFecRate.on("change", function(checkValue) {
				 // if Data is entered in text, then select the 'Use FX Contract' option
				 if ( !isNaN(checkValue) && checkValue != "" ) {
					 registry.byId("UseFXContract").set("checked", true);
				  }
			   });      
		    }
		   
		   var settleFecCurrency = registry.byId("settleFecCurrency");
		   if (settleFecCurrency) {
			   settleFecCurrency.on("change", function(checkValue) {
				 // if Data is entered in text, then select the 'Use FX Contract' option
				 if ( checkValue.length > 0 ) {
					 registry.byId("UseFXContract").set("checked", true);
				  }
			   });      
		    }
		   
		   
		   
		   var otherFXDetail = registry.byId("OtherFXDetail");
		   if (otherFXDetail) {
			   otherFXDetail.on("change", function(checkValue) {
				   // if 'Other FX instructions' is unselected, clear text area.
				   if ( !checkValue ) {
    	             settlemnt.clearOtherFXInstructionText();
				   }
			   });      
		   }
		   
		   var settleOtherFecText = registry.byId("settleOtherFecText");
		   if (settleOtherFecText) {
			   settleOtherFecText.on("change", function(checkValue) {
				 // if Data is entered in text, then select the 'Other FX instructions' option
				 if ( checkValue.length > 0 ) {
					 registry.byId("OtherFXDetail").set("checked", true);
				  }
			   });      
		    } 
		   
		   var otherAddlInstructions = registry.byId("otherAddlInstructions");
		   if (otherAddlInstructions) {
			   otherAddlInstructions.on("change", function(checkValue) {
				 // if Data is entered in 'Additional Instructions' text, then select the 'Other( Enter additional Instruction below)' option
				 if ( checkValue.length > 0 ) {
					 registry.byId("otherAddlInstructionsInd").set("checked", true);
				  }
			   });      
		    }
		   
		   var otherAddlInstructionsInd = registry.byId("otherAddlInstructionsInd");
		   if (otherAddlInstructionsInd) {
			   otherAddlInstructionsInd.on("change", function(checkValue) {
				 // if 'Other( Enter additional Instruction below)' option is un-selected, then clear additional text.
				 if ( !checkValue ) {
					 registry.byId("otherAddlInstructions").set('value', '');
				  }
			   });      
		    } 
    	             
		   var account = registry.byId("Account");
		   if (account) {
			   account.on("change", function(checkValue) {
				   // if Account option is unselected, then clear all debit account.
				   if ( !checkValue ) {
    	             settlemnt.clearAllDebitAccountDetail();
				   }
			   });      
		   } 
		   
		   var principalAccountOid = registry.byId("principalAccountOid");
		   if (principalAccountOid) {
			   principalAccountOid.on("change", function(checkValue) {
				 // if Principal account is selected, then select the Account option.
				 if ( checkValue.length > 0 ) {
					 registry.byId("Account").set("checked", true);
					 if ( registry.byId("chargesAccountOid") ) {
						 registry.byId("chargesAccountOid").set("value", checkValue);
					 }
					 if ( registry.byId("interestAccountOid") ) {
						 registry.byId("interestAccountOid").set("value", checkValue);
					 }					 
				  }
			   });      
		    } else { // if Priciple account not present, then charges account same as interest account.
		    	var interestAccountOid = registry.byId("interestAccountOid");
		    	if ( interestAccountOid ) {
		    		interestAccountOid.on("change", function(checkValue) {
						 if ( checkValue.length > 0 ) {
							 if ( registry.byId("chargesAccountOid") ) {
								 registry.byId("chargesAccountOid").set("value", checkValue);
							 }					 
						  }
					   });
		    	}
		    }
		   
		   var chargesAccountOid = registry.byId("chargesAccountOid");
		   if (chargesAccountOid) {
			   chargesAccountOid.on("change", function(checkValue) {
				 // if Principal account is selected, then select the Account option.
				 if ( checkValue.length > 0 ) {
					 registry.byId("Account").set("checked", true);
				  }
			   });      
		    }
		   
		   var interestAccountOid = registry.byId("interestAccountOid");
		   if (interestAccountOid) {
			   interestAccountOid.on("change", function(checkValue) {
				 // if Principal account is selected, then select the Account option.
				 if ( checkValue.length > 0 ) {
					 registry.byId("Account").set("checked", true);
				  }
			   });      
		    }
		   
		   var forFull = registry.byId("ForFull");
		   if (forFull) {
			   forFull.on("change", function(checkValue) {
				   // if 'For Full Amount' option is selected, then clear all payment detail and select Rollover or Finance in finance.
				   if ( checkValue ) {

					   
    	             settlemnt.setFinanceOrRollover();
    	             settlemnt.accountDetailsReadOnly();
				   }
			   }); 
			   if ( forFull.checked ) {
				   settlemnt.accountDetailsReadOnly(); 
			   }			   
		   }
		   var payInFull = registry.byId("PayInFull");
		   if (payInFull) {
			   payInFull.on("change", function(checkValue) {
				   // if 'For Pay In Full Amount' option is selected, enable Payment.
				   if ( checkValue ) {
					   settlemnt.accountDetailsEditable();
				   }
			   }); 			   
		   }		   
		   
		   var forPartial = registry.byId("ForPartial");
		   if (forPartial) {
			   forPartial.on("change", function(checkValue) {
				   // if 'For Partial Amount' option is selected, then select Rollover or Finance in finance..
				   if ( checkValue ) {
					   //enable accounts
					 settlemnt.accountDetailsEditable();
    	             settlemnt.setFinanceOrRollover();
				   }
			   });      
		   }
		   
		   var finForDays = registry.byId("FinForDays");
		   if (finForDays) {
			   finForDays.on("change", function(checkValue) {
				   // if 'Rollover or Finance for days' option is selected, then select Rollover or Finance in finance..
				   if ( checkValue ) {
    	             settlemnt.setFinanceOrRollover();
				   }
			   });      
		   }
		   
		   var finAtMaturityDate = registry.byId("FinAtMaturityDate");
		   if (finAtMaturityDate) {
			   finAtMaturityDate.on("change", function(checkValue) {
				   // if 'Rollover or Finance Maturity date' option is selected, then select Rollover or Finance in finance..
				   if ( checkValue ) {
    	             settlemnt.setFinanceOrRollover();
				   }
			   });      
		   }
		   
		   var finRollNumberOfDays = registry.byId("finRollNumberOfDays");
		   if (finRollNumberOfDays) {
			   finRollNumberOfDays.on("change", function(checkValue) {
				 // if Finance number of days entered, then select the corresponding option.
				 if ( !isNaN(checkValue) && checkValue != "" ) {
					 registry.byId("FinForDays").set("checked", true);
				  }
			   });      
		    }
		   
		   var finForDays = registry.byId("FinForDays");
		   if (finForDays) {
			   finForDays.on("change", function(checkValue) {
				   // if number of Finance days option is unselected, then clear number of days data.
				   if ( !checkValue ) {
					   registry.byId("finRollNumberOfDays").set('value', '');
				   }
			   });      
		   } 
		   
		   var finRollMaturityDate = registry.byId("finRollMaturityDate");
		   if (finRollMaturityDate) {
			   finRollMaturityDate.on("change", function(checkValue) {
				 // if Maturity date is selected, then select the corresponding option.
				 if ( finRollMaturityDate != '' )  {
					 registry.byId("FinAtMaturityDate").set("checked", true);
				  }
			   });      
		    }
		   
		   var finAtMaturityDate = registry.byId("FinAtMaturityDate");
		   if (finAtMaturityDate) {
			   finAtMaturityDate.on("change", function(checkValue) {
				   // if Maturity date option is unselected, then clear Maturity date.
				   if ( !checkValue ) {
					   registry.byId("finRollMaturityDate").set('value', null);
				   }
			   });      
		   }
		   
		   var finRollPartialPayAmt = registry.byId("finRollPartialPayAmt");
		   if (finRollPartialPayAmt) {
			   finRollPartialPayAmt.on("change", function(checkValue) {
				 // if payment amount is entered, then select the corresponding Pay Partial Amount option.
				 if ( !isNaN(checkValue) && checkValue != "" ) {
					 if ( registry.byId("ForPartial") ) {
					   registry.byId("ForPartial").set("checked", true);
					 }
				  }
			   });      
		    }
		   
		   var forPartial = registry.byId("ForPartial");
		   if (forPartial) {
			   forPartial.on("change", function(checkValue) {
				 // if Pay Partial Amount option is un-selected, then clear the payment amount data.
				   if ( !checkValue ) {
					   registry.byId("finRollPartialPayAmt").set('value', null);
				   }
			   });      
		    }
		   
		   var rejDoc =  registry.byId('TradePortalConstants.IMP_REJ');
		   var ourRef =  registry.byId('ourReference');
		   if ( rejDoc && ourRef) {
			   rejDoc.on("change", function(checkValue) {
				   if (checkValue) {
					   settlemnt.setSettlementInstrReadOnly();
				   } else {
					   settlemnt.setSettlementInstrEditable();
				   }
				   
			   });
			   // page load time.
			   if ( rejDoc.checked ) {
				   settlemnt.setSettlementInstrReadOnly(); 
			   }
		   }
		   
		   var finance = registry.byId("Finance");
		   if (finance) {
			   finance.on("change", function(checkValue) {
				   // if Finance or Rollover in Finance option is un-selected, the clear detail data.
				   if ( !checkValue ) {
					   settlemnt.clearFinanceOrRolloverDetail();
				   }else{
					   //only set it editable if For Full amount is not selected					  
					   var rollFullA =  registry.byId('ForFull');
					   var setAccEditable = true;
					   if (rollFullA) {
						   if(rollFullA.checked) setAccEditable = false;
					   }					   
					   if(setAccEditable)
					   settlemnt.accountDetailsEditable();
				   }
			   });      
		   }
    	                	             
    	  
      });
  });