//Invoice Offers Transactions Home START //
 function initializeDataGrid(){
	
   	
	//create the grid, attaching it to the div id specified created in dataGridDataGridFactory.createDataGrid.jsp()//
        require(["t360/datagrid", "dojo/dom", "dijit/registry"],
        function( t360grid,dom, registry) {
        	  //   if(dataViewName=='InvoicesOfferedDataView' || dataViewName=='InvoicesFvdOfferedDataView'){ //
        	var savedSort = savedSearchQueryData["sort"];  
               	var currency = savedSearchQueryData["currency"]; 
             	var amountFrom = savedSearchQueryData["amountFrom"]; 
             	var amountTo = savedSearchQueryData["amountTo"]; 
             	var dateFrom = savedSearchQueryData["dateFrom"]; 
             	var dateTo = savedSearchQueryData["dateTo"]; 
             	var fvDateFrom = savedSearchQueryData["fvDateFrom"]; 
            	var fvDateTo = savedSearchQueryData["fvDateTo"]; 
            	var invoiceStatus = savedSearchQueryData["selectedStatus"];
            	
               	if("" == currency || null == currency || "undefined" == currency)
               		currency=dom.byId("Currency").value;
          		 else
          			 registry.byId("Currency").set('value',currency);
            	if("" == amountFrom || null == amountFrom || "undefined" == amountFrom)
               		amountFrom=dom.byId("AmountFrom").value;
          		 else
          			dom.byId("AmountFrom").value = amountFrom;
 
               	if("" == amountTo || null == amountTo || "undefined" == amountTo)
               		amountTo=dom.byId("AmountTo").value;
          		 else
          			dom.byId("AmountTo").value = amountTo;
            	
	        	if("" == dateFrom || null == dateFrom || "undefined" == dateFrom)
	        		dateFrom=dom.byId("DateFrom").value;
	      		 else
	      			 registry.byId("DateFrom").set('displayedValue',dateFrom);
	        	
		    	if("" == dateTo || null == dateTo || "undefined" == dateTo)
		    		dateTo=dom.byId("DateTo").value;
		  		 else
		  			registry.byId("DateTo").set('displayedValue',dateTo);
		    	
		    	if("" == fvDateFrom || null == fvDateFrom || "undefined" == fvDateFrom)
		    		fvDateFrom=dom.byId("FvDateFrom").value;
		  		 else
		  			 registry.byId("FvDateFrom").set('displayedValue',fvDateFrom);
		    	
		    	if("" == fvDateTo || null == fvDateTo || "undefined" == fvDateTo)
		    		fvDateTo=dom.byId("FvDateTo").value;
		  		 else
		  			 registry.byId("FvDateTo").set('displayedValue',fvDateTo);
		    	  
		    	
		    	if(dijit.byId("invoiceStatusType")){
		     	if("" == invoiceStatus || null == invoiceStatus || "undefined" == invoiceStatus)
		    		invoiceStatus=dijit.byId("invoiceStatusType").attr('value');
		  		 else
		  			 registry.byId("invoiceStatusType").set('value',invoiceStatus);
		    	}
		    	initSearchParms = initSearchParms+"&selectedStatus="+invoiceStatus+"&currency="+currency+"&amountFrom="+amountFrom
		        +"&amountTo="+amountTo+"&dateFrom="+dateFrom+"&dateTo="+dateTo+"&fvDateFrom="+fvDateFrom+"&fvDateTo="+fvDateTo;
        	 //  } //
		  // IR 20302 BEGIN //
        	    if("" == savedSort || null == savedSort || "undefined" == savedSort){
            		t360grid.createDataGrid(gridId, dataViewName,gridLayout, initSearchParms);
        	    }
        	    else{
        	    	 t360grid.createDataGrid(gridId, dataViewName,gridLayout, initSearchParms,savedSort);
        	    }
		    // IR 20302 END //

	});
        }
  

 function initializeTranDataGrid(){
	  //create the grid, attaching it to the div id specified created in dataGridDataGridFactory.createDataGrid.jsp()//
	 require(["t360/datagrid", "dijit/registry", "dojo/dom"],
		        function( t360grid,dom, registry) {
		 var savedSort = savedSearchQueryData["sort"];  
		   	if("" == savedSort || null == savedSort || "undefined" == savedSort){
		        savedSort = '0';
		     }
        	var currency = savedSearchQueryData["currency"]; 
         	var amountFrom = savedSearchQueryData["amountFrom"]; 
         	var amountTo = savedSearchQueryData["amountTo"]; 
         	var dateFrom = savedSearchQueryData["dateFrom"]; 
         	var dateTo = savedSearchQueryData["dateTo"]; 
         	var invoiceID = savedSearchQueryData["invoiceID"]; 
        	var relatedInstrumentID = savedSearchQueryData["relatedInstrumentID"]; 
        	var invoiceStatus = savedSearchQueryData["selectedStatus"]; 
        	var showInactive = savedSearchQueryData["showInactive"]; // Rel 9.0 CR 913  //
           	if("" == currency || null == currency || "undefined" == currency)
           		currency=dom.byId("Currency").value;
      		 else
      			 dijit.byId("Currency").set('value',currency);
         
           	if("" == amountFrom || null == amountFrom || "undefined" == amountFrom)
           		amountFrom=dom.byId("AmountFrom").value;
      		 else
      			dijit.byId("AmountFrom").set('value',amountFrom);

           	if("" == amountTo || null == amountTo || "undefined" == amountTo)
           		amountTo=dom.byId("AmountTo").value;
      		 else
      			dijit.byId("AmountTo").set('value',amountTo);
        	
        	if("" == dateFrom || null == dateFrom || "undefined" == dateFrom)
        		dateFrom=registry.byId("DateFrom").value;
      		 else
      			dijit.byId("DateFrom").set('displayedValue',dateFrom);
        	
	    	if("" == dateTo || null == dateTo || "undefined" == dateTo)
	    		dateTo=registry.byId("DateTo").value;
	  		 else
	  			dijit.byId("DateTo").set('displayedValue',dateTo);
	    	
	    	if("" == invoiceID || null == invoiceID || "undefined" == invoiceID)
	    		invoiceID=dom.byId("InvoiceID").value;
	  		 else
	  			dijit.byId("InvoiceID").set('value',invoiceID);
	    	
	    	if("" == relatedInstrumentID || null == relatedInstrumentID || "undefined" == relatedInstrumentID)
	    		relatedInstrumentID=dom.byId("RelatedInstrumentID").value;
	  		 else
	  			dom.byId("RelatedInstrumentID").value = relatedInstrumentID;
	    	
	    	if("" == invoiceStatus || null == invoiceStatus || "undefined" == invoiceStatus)
	    		invoiceStatus=dijit.byId("invoiceStatusType").attr('value');
	  		 else
	  			dijit.byId("invoiceStatusType").set('value',invoiceStatus);
	    	// Rel 9.0 CR 913 START //
	    	if("" == showInactive || null == showInactive || "undefined" == showInactive){
                    if(dom.byId("InActive").checked){
                        showInactive="Y";
                    }else{
                    	showInactive="N";
                    }
            }else{
                if(showInactive=="Y")
                    dijit.byId("InActive").set('checked',true);
                      else
                        dijit.byId("InActive").set('checked',false);
                 
             }
           		// Rel 9.0 CR 913 Ends //
	    	initSearchParms = initSearchParms+"&selectedStatus="+invoiceStatus+"&invoiceID="+invoiceID+"&currency="+currency+"&amountFrom="+amountFrom
		        +"&amountTo="+amountTo+"&dateFrom="+dateFrom+"&dateTo="+dateTo+"&relatedInstrumentID="+relatedInstrumentID+"&showInactive="+showInactive;
            	t360grid.createDataGrid(gridId, dataViewName,gridLayout, initSearchParms,savedSort);

});
    }
 function dialogQuickView(rowKey){
	 var dialogDiv = document.createElement("div");
	 dialogDiv.setAttribute('id',"quickviewdialogdivid");
	 document.forms[0].appendChild(dialogDiv);
	 
	 var title="";

	require(["t360/dialog"], function(dialog ) {
		
        dialog.open('quickviewdialogdivid',title ,
                    'TransactionQuickViewDialog.jsp',
                    ['rowKey','DailogId'],[rowKey,'quickviewdialogdivid'], // parameters //
                    'select', quickViewCallBack);
      });
}
 var quickViewFormatter=function(columnValues, idx, level) {
		var gridLink="";

		if(level == 0){

			if ( columnValues.length >= 2 ) {
			      gridLink = "<a href='" + columnValues[1] + "'>" + columnValues[0] + "</a>";
			    }
			    else if ( columnValues.length == 1 ) {
			      // no url parameters, just return the display //
			      gridLink = columnValues[0];
			    }
			    return gridLink;

	  }else if(level ==1){
		  	console.log(columnValues[3]);
		  	if(columnValues[3]=='PROCESSED_BY_BANK'){
		  		if(columnValues[2]){
		  			return "<a href='javascript:dialogQuickView("+columnValues[2]+");'>Quick View</a> "+columnValues[0]+ " ";
		  		}else{
		  			return columnValues[0];	
		  		}

		  	}else{
					return columnValues[0];
			  	}

	  }
	}
 var formatGridLinkChild=function(columnValues, idx, level){
		var gridLink="";

			if(level == 0){
				return columnValues[0];

		  }else if(level ==1){
			  if ( columnValues.length >= 2 ) {
			      gridLink = "<a href='" + columnValues[1] + "'>" + columnValues[0] + "</a>";
			    }
			    else if ( columnValues.length == 1 ) {
			      // no url parameters, just return the display //
			      gridLink = columnValues[0];
			    }
			    return gridLink;
			    }
		}

	// function to set the title after ajax call of dialog value[2]- dialogId, value[0]-complete instrument id, value[1]-transaction type //
 function quickViewCallBack(key,value){
		dijit.byId(value[2]).set('title','Transaction Quick View - '+value[0]+' - '+value[1]);
		dijit.byId(value[2]).set('style','width:600px');
		
	}
	
	function booleanFormat(item){
		if(item=='true')
			return true;
		else
			return false;
	}
	

//Invoice Offers Transactions Home END //