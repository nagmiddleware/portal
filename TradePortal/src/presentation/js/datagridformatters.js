  //create a global variable that holds formatters
  // this is not ideal, but removes need for grid repaints to figure out
  // the variable reference of the t360/datagrid returned object

  //todo: check to see if the variable exists, if so don't recreate

  //!!!!!!!!!ADD ALL COMMON GRID FORMATTERS HERE!!!!!!!!!!
  t360gridFormatters = {
    formatGridLink: function(columnValues) {
      var gridLink = "";
      if ( columnValues.length >= 2 ) {
        gridLink = "<a href='" + columnValues[1] + "'>" + t360gridFormatters.encodeHTML(columnValues[0]) + "</a>";
      }
      else if ( columnValues.length == 1 ) {
        //no url parameters, just return the display
        gridLink = t360gridFormatters.encodeHTML(columnValues[0]);
      }
      return gridLink;
    },
    
    statusSeqFormatter: function(columnValues) {
      var gridLink = "";
      if ( columnValues.length >= 3 ) {
    	  if(columnValues[1] != ""){
    		  gridLink = "<a href='" + columnValues[2] + "'>" + (columnValues[0]+" - "+columnValues[1]) + "</a>";
    	  }else if(columnValues[1] == ""){
    		  gridLink = "<a href='" + columnValues[2] + "'>" + columnValues[0] + "</a>";
    	  }
      }
      else if ( columnValues.length == 2 ) {
    	//no url parameters, just return the display
        if(columnValues[1] !=""){
        	gridLink = columnValues[0]+" - "+columnValues[1];
        }else if(columnValues[1] == ""){
        	gridLink = columnValues[0];
        }
      }
      return gridLink;
    },
    
	encodeHTML: function(val) {
	   if (val) {
	 		 return String(val).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
 	   }
	   return val;
	},
	errorMsgFormatter: function(fields) {
	      var formattedErrMsg = "";
	      formattedErrMsg += fields[0]; 
	      formattedErrMsg = formattedErrMsg.replace(/-/g,"<br>-");
	      formattedErrMsg = formattedErrMsg.replace("<br>","");
	      return formattedErrMsg;
	},
    addressFormatter: function(fields) {
      var formattedAddress = "";
      if ( fields.length == 6 ) {
        formattedAddress += fields[0]; //address line1
        if ( fields[1]!=null && fields[1].length>0 ) {
          if ( formattedAddress!=null && formattedAddress.length > 0 ) {
            formattedAddress += "<br/>"; 
          }
          formattedAddress += fields[1]; //address line2
        }
        var cityStateZip = "";
        cityStateZip += fields[2]; //city
        if ( fields[3]!=null && fields[3].length>0 ) {
          if ( cityStateZip!=null && cityStateZip.length > 0 ) {
            cityStateZip += ", "; 
          }
          cityStateZip += fields[3]; //state/province
        }
        if ( fields[4]!=null && fields[4].length>0 ) {
          if ( cityStateZip!=null && cityStateZip.length > 0 ) {
            cityStateZip += ", "; 
          }
          cityStateZip += fields[4]; //country
        }
        if ( fields[5]!=null && fields[5].length>0 ) {
          if ( cityStateZip!=null && cityStateZip.length > 0 ) {
            cityStateZip += ", "; 
          }
          cityStateZip += fields[5]; //zip
        }
        //put them together
        if ( cityStateZip!=null && cityStateZip.length > 0 ) {
          if ( formattedAddress!=null && formattedAddress.length > 0 &&
               cityStateZip!=null && cityStateZip.length > 0 ) {
            formattedAddress += "<br/>"; 
          }
          formattedAddress += cityStateZip;
        }
      }
      else {
        var formattedAddress1 = "";
        var formattedAddress2 = "";
        if ( fields.length >= 2 ) {
          for (i=0; i<fields.length; i++) {
            if(i<=1 && !formattedAddress1=="")
              formattedAddress1=formattedAddress1+","+fields[i];
            else if(i<=1 &&formattedAddress1=="")
              formattedAddress1=fields[i];
        		
            if(i>1 && !formattedAddress2=="")
              formattedAddress2=formattedAddress2+","+fields[i];
            else if(i>1 && formattedAddress2=="")
              formattedAddress2=fields[i];
          }
          formattedAddress=formattedAddress1+"\n"+formattedAddress2;
        }
        else if ( fields.length == 1 ) {
          
            formattedAddress = fields[0];
        }
      }
      return formattedAddress;
    }
    ,
    invPDFFormatter: function(columnValues) {
        var gridLink = "";
        if ( columnValues.length >= 2 ) {
      	  	  gridLink = "<a href='" + columnValues[1] + "' target ='_blank'>" + columnValues[0] + "</a>";
      	 
        }
        else if ( columnValues.length == 2 ) {
      	//no url parameters, just return the display
          if(columnValues[1] !=""){
          	gridLink = columnValues[0]+" - "+columnValues[1];
          }else if(columnValues[1] == ""){
          	gridLink = columnValues[0];
          }
        }
        return gridLink;
      }
      ,
      openLinkInNewWindow: function(columnValues) {
      var gridLink = "";
      if ( columnValues.length >= 2 ) {
        gridLink = "<a href='" + columnValues[1] + "' target ='_blank'>" + columnValues[0] + "</a>";
      }
      else if ( columnValues.length == 1 ) {
        //no url parameters, just return the display
        gridLink = columnValues[0];
      }
      return gridLink;
    }

  };