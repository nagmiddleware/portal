 /*----------------------------------------------------------------------------*/
 // Copyright (c) 2009 Australia and New Zealand Banking Group Limited
 // 
 // THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 // WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 // MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 // ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 // WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 // ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 // OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
/*----------------------------------------------------------------------------*/
/*
*  TP Protect client encryption library that can be used to encrypt
*  any string to used with the server using an asymmetric algorithm.
*
*  Depends on pidCrypt (pidcrypt.js, pidcrypt_util.js), AES (aes_core.js, aes_cbc.js)
*  MD5 (md5.js), ASN1 (asn1.js), BigInteger (jsbn.js), Random (rng.js, prng.js),
*  and RSA (rsa.js)
/*----------------------------------------------------------------------------*/

if(typeof(pidCrypt) != 'undefined' &&
   typeof(pidCrypt.AES) != 'undefined' &&
   typeof(pidCrypt.MD5) != 'undefined' &&
   typeof(pidCrypt.SHA256) != 'undefined' &&
   typeof(BigInteger) != 'undefined' &&
   typeof(SecureRandom) != 'undefined' &&
   typeof(Arcfour) != 'undefined' &&
   typeof(pidCrypt.RSA) != 'undefined')
{
    
    TPProtect = function() {
    }
    
    /**
    * protect the data provided using the key provided
    * @param  options {
    *           publicKey: base64 
    *           usePem: true | false indicates if public key is PEM encoded
    *           iv: initialiation Vector in Base64
    *           bits: bits of AES key
    *           data: data to encode (must be a string)
    *         }
    */
    TPProtect.prototype.Encrypt = function (options) {
    
        // Load the RSA Public Key
        var publicKey = "MIGJAoGBALOqBQtpyyzLLs3bN43RZkAlPyPSWwkSy3rJsQbDKmfIxhrnrxxHCrG7TBeAMMhR+VyVqQrQB7uNw+hpjHRMRuBQNffyH5WRP/gf43VJcnk7zCG4R9hqkaNve46YPQkeX1Gm0wRqTOc4HstElz5UM+HSz6JNWvNP4LCcJTRSb0YhAgMBAAE=";
        if (options.publicKey)
            if (options.usePem) 
                publicKey = this.pemParser(options.publicKey).b64;
            else
                publicKey = options.publicKey;
        var key = publicKey.decodeBase64();
        var rsa = new pidCrypt.RSA();
        var asn = pidCrypt.ASN1.decode(key.toByteArray());
        var tree = asn.toHexTree();
        rsa.setPublicKeyFromASN(tree);

        // Generate a symmetric key
        var aes = new pidCrypt.AES.CBC();
        var rng = new SecureRandom();
        var bits = 128;
        if (options.bits)
            bits = options.bits;
        var keyBytes = new Array();
        keyBytes[(bits >> 3) - 1] = 0;
        rng.nextBytes(keyBytes);
        var key = byteArray2String(keyBytes).convertToHex();

        // Prepare the IV provided
        var iv = "";  // dummy sample
        if (options.iv)
            iv = options.iv;
        else {
            var ivBytes = new Array();
            ivBytes[16 - 1] = 0;
            rng.nextBytes(ivBytes);
            iv = byteArray2String(ivBytes).convertToHex();
        }
        // trim the IV
        iv = (iv.decodeBase64().convertToHex() + "00000000000000000000000000000000" ).substring(0,32);

        // Encrypt the data
        var utf8Data = options.data.encodeUTF8();
        aes.initByValues('', key, iv, {nBits:bits,A0_PAD:false,UTF8:true});
        var encryptedData = aes.encryptRaw(utf8Data.toByteArray());

        // Encrypt the key
        var encryptedKey = rsa.encryptRaw(key.convertFromHex());

        // Format the response data
        var encodedOutput = "ENC|" + iv.convertFromHex().encodeBase64() + "|" + encryptedKey.encodeBase64() + "|" + encryptedData.encodeBase64();
        
        return encodedOutput;
    }

    /** 
    * Load the Base64, PEM wrapped blob into a binary string
    *
    * @param pem: PEM encoded, unencrypted blob
    */
    TPProtect.prototype.pemParser = function (pem) {
        var lines = pem.split('\n');
        var read = false;
        var b64 = false;
        var end = false;
        var flag = '';
        var retObj = {};
        retObj.info = '';
        retObj.salt = '';
        retObj.iv;
        retObj.b64 = '';
        retObj.aes = false;
        retObj.mode = '';
        retObj.bits = 0;
        for(var i=0; i < lines.length; i++) {
            flag = lines[i].substr(0,9);
            if(i==1 && flag.indexOf('M') == 0)  // ASN1 Sequence as first element
                b64 = true;
            switch(flag) {
                case '-----BEGI':
                    read = true;
                    break;
                case '':
                    if(read) b64 = true;
                    break;
                case '-----END ':
                    if(read) {
                        b64 = false;
                        read = false;
                    }
                    break;
                default:
                    if(read && b64)
                        retObj.b64 += lines[i].stripLineFeeds();
            }
        }
        return retObj;
    }

    // [START] CR-543
    /**
    * MAC the challenge using the password (or userID|password pair)
    * @param  options {
    *           publicKey: base64 
    *           usePem: true | false indicates if public key is PEM encoded
    *           iv: initialiation Vector in Base64
    *           bits: bits of AES key
    *           password: data to encode (must be a string)
    *		     challenge: base64 encoded challenge from server (a browser challenge will be prepended)
    *         }
    * returns base64encoded( browser challenge + server challenge + padding to block size ) 
    *         + '|' + MAC + ' ' + publicKey encrypted SHA256 Hash(password)
    */
    TPProtect.prototype.SignMAC = function (options) {

    	// Load the RSA Public Key
    	var publicKey = "MIGJAoGBALOqBQtpyyzLLs3bN43RZkAlPyPSWwkSy3rJsQbDKmfIxhrnrxxHCrG7TBeAMMhR+VyVqQrQB7uNw+hpjHRMRuBQNffyH5WRP/gf43VJcnk7zCG4R9hqkaNve46YPQkeX1Gm0wRqTOc4HstElz5UM+HSz6JNWvNP4LCcJTRSb0YhAgMBAAE=";
    	if (options.publicKey)
    		if (options.usePem) 
    			publicKey = this.pemParser(options.publicKey).b64;
    		else
    			publicKey = options.publicKey;
    	var key = publicKey.decodeBase64();
    	var rsa = new pidCrypt.RSA();
    	var asn = pidCrypt.ASN1.decode(key.toByteArray());
    	var tree = asn.toHexTree();
    	rsa.setPublicKeyFromASN(tree);

    	// Generate a symmetric key
    	var aes = new pidCrypt.AES.CBC();
    	var rng = new SecureRandom();
    	var blockSize = 16;
    	var bits = 128;
    	if (options.bits)
    		bits = options.bits;
    	var key = (pidCrypt.SHA256(options.password) + "00000000000000000000000000000000" ).substring(0,(bits >> 2));

    	// Prepare the IV provided
    	var iv = "";  // dummy sample
    	if (options.iv)
    		iv = options.iv;
    	else {
    		var ivBytes = new Array();
    		ivBytes[16 - 1] = 0;
    		rng.nextBytes(ivBytes);
    		iv = byteArray2String(ivBytes).convertToHex();
    	}
    	// trim the IV
    	iv = (iv.decodeBase64().convertToHex() + "00000000000000000000000000000000" ).substring(0,32);

    	// Prepare the IV provided
    	var challenge = "";  // dummy sample
    	if (options.challenge)
    		challenge = options.challenge;
    	else {
    		challenge = iv;
    	}

    	iv = ("000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000" ).substring(0,blockSize << 1);

    	// Generate a browser challenge and combine with the provided server challenge
    	var browserChallenge = new Array();
    	browserChallenge[(bits >> 3) - 1] = 0;
    	rng.nextBytes(browserChallenge);

    	// Generate challenge
    	var combinedChallenge = byteArray2String(browserChallenge).convertToHex() + challenge.decodeBase64().convertToHex();

    	// Round the length to an even block size
    	if ((combinedChallenge.length % blockSize) != 0)
    		combinedChallenge = ( combinedChallenge + "00000000000000000000000000000000" ).substring(0, (combinedChallenge.length + blockSize - (combinedChallenge.length % blockSize)));

    	// Encrypt the data
    	var utf8Data = combinedChallenge.convertFromHex();
    	aes.initByValues("", key, iv, {nBits:bits,A0_PAD:false,UTF8:false});
    	var encryptedData = aes.encryptRaw(utf8Data.toByteArray());

    	var mac = encryptedData.substring(encryptedData.length - blockSize - blockSize, encryptedData.length - blockSize);

    	// Encrypt the key
    	var encryptedKey = rsa.encryptRaw(key.convertFromHex());

    	// Format the response data
    	var encodedOutput = utf8Data.encodeBase64() + "|" + mac.encodeBase64() + " " + encryptedKey.encodeBase64();

    	return encodedOutput;
    } // TPProtect.prototype.SignMAC = function (options) {
    // [END] CR-543
    
    /**
    * <desc>
    * @param  options {
    *           errorCode: errorCode from TradePortalConstants
    *           newPassword: new password entered by user
    *           retypePassword: retyped new password entered by user
    *         }
    */
    TPProtect.prototype.MatchingEntries = function (options) {
    	if(options.newPassword != options.retypePassword) {
    		return options.errorCode;
    	}
    	return "";
    } // TPProtect.prototype.MatchEntries = function (options)
    
    /**
    * <desc>
    * @param  options {
    *           errorCode: errorCode from TradePortalConstants
    *           newPassword: new password entered by user
    *           sameAsString: retyped new password entered by user
    *         }
    */
    TPProtect.prototype.IsSameAs = function (options) {
    	var regExp = new RegExp(options.sameAsString, "i");    	
    	if(regExp.test(options.newPassword)) {
    		return options.errorCode + options.sameAsString;
    	}
    	return "";
    } // TPProtect.prototype.IsSameAs = function (options)

    /**
    * <desc>
    * @param  options {
    *           errorCode: errorCode from TradePortalConstants
    *           newPassword: new password entered by user
    *         }
    */
    TPProtect.prototype.StartEndWithSpace = function (options) {
    	var regExp = new RegExp("^\ .*");    	
    	if(regExp.test(options.newPassword)) {
    		return options.errorCode;
    	}
    	
    	regExp = new RegExp(".*\ $");
    	    	if(regExp.test(options.newPassword)) {
			    		return options.errorCode;
    	}
    	
    	return "";
    } // TPProtect.prototype.StartEndWithSpace = function (options)

    /**
    * <desc>
    * @param  options {
    *           errorCode: errorCode from TradePortalConstants
    *           newPassword: new password entered by user
    *           reqLength: required length of password
    *         }
    */
    TPProtect.prototype.IsRequiredLength = function (options) {
    	if((options.newPassword.length > 0)
    		&& (options.newPassword.length < options.reqLength)) {
    		return options.errorCode;
    	}    	
    	return "";
    } // TPProtect.prototype.IsRequiredLength = function (options)

    /**
    * <desc>
    * @param  options {
    *           errorCode: errorCode from TradePortalConstants
    *           newPassword: new password entered by user
    *         }
    */
    TPProtect.prototype.CheckConsecutiveChars = function (options) {
    	if(/([\S\s])\1\1/.test(options.newPassword)) {
    		return options.errorCode;
    	}
    	return "";
    } // TPProtect.prototype.CheckConsecutiveChars = function (options)

    /**
    * <desc>
    * @param  options {
    *           errorCode: errorCode from TradePortalConstants
    *           newPassword: new password entered by user
    *         }
    */
    TPProtect.prototype.CheckPasswordComplexity = function (options) {
    	if((/[a-z]/.test(options.newPassword))
    		&& (/[A-Z]/.test(options.newPassword))
    		&& (/[0-9]/.test(options.newPassword)))
    	{
    		return "";
    	}
    	
    	return options.errorCode;
    } // TPProtect.prototype.CheckPasswordComplexity = function (options)
}

