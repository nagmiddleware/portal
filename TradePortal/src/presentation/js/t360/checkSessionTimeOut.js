define("t360/checkSessionTimeOut", [
                                    "dijit/registry", 
                                    "dojo/on", 
                                    "t360/common", 
                                    "t360/dialog", 
                                    "dojo/_base/xhr",
                                    "dojo/parser"
], function(registry, on, common, dialog, xhr, parser) {
  var timeoutParams = {};
  var checkSessionTimeOut = {
		  
		  checkIfSessionShouldTimeout: function (timeoutParams){
			  require(["t360/common","dojo/_base/xhr","dojo/parser"],
			          function(common) {
				  this.timeoutParams = timeoutParams;
				  
				        xhr.get({
				          url: "/portal/checkSessionTimeout?TypeOfOperation=CHECKFORSESSIONTIMEOUT",
				          load: function(data) {
				        	  extendTimeOut(data); 
				          },
				          error: function(err) {
				            //todo: make a custom dialog for errors
				            //would be nice to display this in a nice fashion, 
				            // probably also allow the user to avoid the error in future
				            // an alternative would be to have some kind of error display at bottom of the page???
				            console.log('Error retrieving remote content:'+err);
				          }
				        });
				      });
			  }
			  
  };
   function extendTimeOut(data){
	  require(["dijit/registry", "dojo/on", "t360/common", "t360/dialog", "t360/checkSessionTimeOut"],
	          function(registry, on, common, dialog, checkSessionTimeOut) {
		  clearTimeout(this.timeoutParams["logOutTimerId"]);
		  clearTimeout(this.timeoutParams["autoSaveTimeoutTimerId"]);
		  clearTimeout(this.timeoutParams["timeoutWarningTimerId"]);
			var newTimeOut = data;
			//var timeOutInSeconds = <%=(timeoutInSeconds - (SecurityRules.getTimeoutWarningPeriod() * 60))%>;
			if (newTimeOut != undefined && newTimeOut != ''){
				var timeoutVal = Number(newTimeOut) - 15; //substract 15 to avoid server session already timed out.
				console.log('timeout Val -- > ' + timeoutVal);
				var warningTimeOutInSeconds = Number(this.timeoutParams["timeoutInSecond"]) - Number(this.timeoutParams["warningTimeOutInSeconds"]);
				if (timeoutVal <= 15){
					dialog.hide('timeoutWarningDialog');
					if (this.timeoutParams["autoSaveTimeoutTimerId"]){
						  common.autoSaveForTimeOut(this.timeoutParams["autoSaveButtonName"], this.timeoutParams["autoSaveFormNumber"]); 
					  }else{
						  document.location.href=this.timeoutParams["timeOutLink"];
					  }
				}
//				else if (  timeoutVal <= warningTimeOutInSeconds ){
//						dialog.hide('timeoutWarningDialog');
//					  dialog.open('timeoutWarningDialog',this.timeoutParams["timeoutWarningDialogTitle"],
//				              'timeoutWarningDialog.jsp',null, null, //no parms
//				              null, null, 'warningDialog' ) ;
//					  if (this.timeoutParams["autoSaveTimeoutTimerId"]){
//						  this.timeoutParams["autoSaveTimeoutTimerId"] = setTimeout(function () { common.autoSaveForTimeOut(this.timeoutParams["autoSaveButtonName"], this.timeoutParams["autoSaveFormNumber"]); },
//							  (timeoutVal-20) +'000');
//					  }
//					  this.timeoutParams["logOutTimerId"] = setTimeout(function () { document.location.href=this.timeoutParams["timeOutLink"]; }, 
//							  (timeoutVal) +'000');
//					  this.timeoutParams["timeoutWarningTimerId"] = setTimeout(function () { checkSessionTimeOut.checkIfSessionShouldTimeout(this.timeoutParams);  },
//				              (timeoutVal - 15) +'000');
//				}
				else{
					dialog.hide('timeoutWarningDialog');
					var newWarningTimeOutInSeconds = Number(timeoutVal) - Number(warningTimeOutInSeconds);
					if (newWarningTimeOutInSeconds < 1){
						newWarningTimeOutInSeconds = Number(timeoutVal) - 15;
						if (newWarningTimeOutInSeconds < 1){
							newWarningTimeOutInSeconds = Number(timeoutVal);
						}
					}
					if (this.timeoutParams["autoSaveTimeoutTimerId"]){
						this.timeoutParams["autoSaveTimeoutTimerId"] = setTimeout(function () { checkForSessionTimeout(this.timeoutParams); },
							  (Number(timeoutVal)-15) +'000');
					}else{
						this.timeoutParams["logOutTimerId"] = setTimeout(function () { checkForSessionTimeout(this.timeoutParams); }, 
							  (Number(timeoutVal)-15) +'000');
					}
//					this.timeoutParams["timeoutWarningTimerId"] = setTimeout(function () { checkSessionTimeOut.checkIfSessionShouldTimeout(this.timeoutParams); },
//			                (newWarningTimeOutInSeconds)+'000');					
				}		
			}
	  });
  }
  return checkSessionTimeOut;
});

