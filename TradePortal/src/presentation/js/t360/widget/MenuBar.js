define([
    "dojo/_base/declare", "dijit/MenuBar",
    "dojo/dom-style", "dojo/_base/sniff", "t360/common" 
], function(declare, MenuBar, style, sniff, common) {
    return declare("t360.widget.MenuBar", MenuBar, {
        postCreate: function(){
            // perform any logic from superclasses
            this.inherited(arguments);
            // override _orient to instruct child popups to
            // display centered under their parent
            this._orient = ["below-centered"];
        },
        
        // summary:
    	//		_openPopup has been moved to _MenuBase.js between 1.7 to 1.9.   
    	// description:
    	//		But all the functionality with respective to the method is still available under _openItemPopup with little bit change of calling wrapper.
    	//		wrapper =this.currentPopupItem.popup._popupWrapper;
        _openItemPopup: function() {
            // opens pop-up menu, ensuring that its x-bounds don"t 
            // extend outside the width of the menu
            this.inherited(arguments);
            //get the popup dom wrapper
            var wrapper = this.currentPopupItem.popup._popupWrapper;
            var left = style.get(wrapper, "left");
            //standard offset logic
            var leftOffset = left - this.domNode.offsetLeft,
            rightOffset = (this.domNode.offsetLeft + this.domNode.offsetWidth) - (left + wrapper.offsetWidth);
            if (leftOffset < 0) {
                var leftAdjust = left - leftOffset;
                //account for standard -1 margin on dojo menu bar items
                leftAdjust = leftAdjust - 1;
                style.set(wrapper, "left", leftAdjust + "px");
            } else if (rightOffset < 0) {
                style.set(wrapper, "left", left + rightOffset + "px");
            }
        }
    });
});