define("t360/common", [
  "dojo/_base/array",
  "dojo/dom",
  "dojo/dom-construct",
  "dojo/dom-style",
  "dojo/dom-class",
  "dojo/query",
  "dijit/registry",
  "t360/widget/Tooltip",
  "t360/widget/Dialog"

], function(array, dom, domConstruct, domStyle, domClass, query, registry, tooltip, T360Dialog) {
  var otherCondsFieldId = "";
  var otherCondsDialogTitle = "";
  var rowKeyValue;
  var fromListViewGlobal;
  var common = {
	  	resetDateField: function(dateFieldId){
	  		var fieldId = dateFieldId.split(",");
			require(["dijit/registry"], function(registry) { 
				for(i=0; i<fieldId.length; i++){
					registry.byId(fieldId[i]).reset();
				}
			});
	  	},
	      
      //Called by the hyperlink which opens the Other Conditions Dailog       
      openOtherConditionsDialog: function(fieldId){
            openOtherConditionsDialog(fieldId,"SaveTrans"); 
      },
      
       openOtherConditionsDialog: function(fieldId,buttonName){
	       openOtherConditionsDialog(fieldId,buttonName,"Other Conditions");
       },  
       
      openOtherConditionsDialog: function(fieldId,buttonName,otherConditionsDialogTitle){
            otherCondsFieldId = fieldId; 
            otherCondsDialogTitle = otherConditionsDialogTitle;
            require(["dijit/registry","t360/dialog"], function(registry,dialog) {
                 var OCData = registry.byId(otherCondsFieldId).value;
            dialog.open('otherConditionsDialog', otherConditionsDialogTitle,
                     'otherConditionsDialog.jsp', 'otherConditionsFieldData', OCData, buttonName, this.otherConditionsCallback);
         });
      },                
      
      otherConditionsCallback: function(otherConditionsData){
    	  require(["dijit/registry","t360/common"],function(registry,common) {
    		  registry.byId('otherConditionsDialog').set('title',otherCondsDialogTitle);
    		  registry.byId(otherCondsFieldId).set('value',otherConditionsData); 			
    		  common.setButtonPressed('SaveTrans','0');
    		  document.forms[0].submit();
    	  });
  	  },
      
      //Called by the Delete button Click to conform the Delete Action        
      conformDeleteAction: function(buttonName, itemName){
            var confirmDelete = confirm("You are about to delete "+itemName+". Please press OK to confirm.");
            
            if(confirmDelete==true){                        
                  setButtonPressed(buttonName, '0');
                  document.forms[0].submit();
            }
      },
    //removed expandCollapse and related functions as they are now taken care by widget.FormSidebar in FormSidebar.js
    //Called by the hyperlink which shuffle the Hide Tips & Show Tips -- Added by Sandeep
    //removed hideShowTips() - simplification
      
    //-- Added by Sandeep
    hideTips: function(){
       //globalUIPref is defined in Footer.jsp as a global variable
       if(globalUIPref.isShowing == true){//hide only when it was already showing
            this.toggleToolTips();//called for show tips and hide tips functionality by Paateep
            globalUIPref.isShowing=false;
            var showTipsDiv = dom.byId("showTipsDiv");
            var hideTipsDiv = dom.byId("hideTipsDiv");            
            if (showTipsDiv && hideTipsDiv) {
                     domStyle.set(showTipsDiv, "display", "");
                     domStyle.set(hideTipsDiv, "display", "none");
            }
       }
    },
  
    //-- Added by Sandeep
    showTips: function(){

       if(globalUIPref.isShowing == false) {//show only when it was already hiding
            this.toggleToolTips();//called for show tips and hide tips functionality by Paateep
            globalUIPref.isShowing=true;
            var showTipsDiv = dom.byId("showTipsDiv");
            var hideTipsDiv = dom.byId("hideTipsDiv");            
            if (showTipsDiv && hideTipsDiv) {
                    domStyle.set(showTipsDiv, "display", "none");
                    domStyle.set(hideTipsDiv, "display","");    
            }    
       }
    },
    
    toggleToolTips:function(){
        require(["dojo/_base/array",
                 "t360/widget/Tooltip",
                 "dijit/registry"], 
        function(array,tooltip,registry){

              var widgetsInPage=registry.toArray();

              array.forEach(widgetsInPage, function(tip){
                  if(tip instanceof tooltip){
                       if(tip.connectId.length>0){
                          tip.removeTarget(tip.saveConnId);
                       } else {
                          tip.addTarget(tip.saveConnId);
                       }
                  }
               });

        });
    },
    //removed tipsCall
    
   
    //Added by Prateep
    setCurrencyForAmount: function(value,field){
      currencybox=dijit.byId(field);
      currencybox._setConstraintsAttr({currency:value});
    },
    
    callAjaxToUpdate: function(nodeId,remoteUrl,callbackFunction) {
        require(["dojo/_base/xhr","dojo/parser"], function(xhr,parser) {
          xhr.get({
            url: remoteUrl,
            load: function(data) {
            	 var attachDomNode = document.getElementById(nodeId);
            	 if(attachDomNode){
                 attachDomNode.innerHTML = data;
                 //dojoize
                 parser.parse(nodeId);
            	 }
               if ( callbackFunction ) {
                callbackFunction();
               }
            },
            error: function(err) {
               alert('Error retrieving remote content:'+err);
            }
          });
        });
      },
  
    //make an ajax call to the given url
    //and replace the node with the given id
    //with the ajax result.
    //this also parses the result to ensure
    //dojo widgets are instantiated
    //callbackFunction is optional
    attachAjaxResult: function(nodeId,remoteUrl,callbackFunction) {
      require(["dojo/_base/xhr","dojo/parser"], function(xhr,parser) {
        xhr.get({
          url: remoteUrl,
          load: function(data) {
            var attachDomNode = document.getElementById(nodeId);
            attachDomNode.innerHTML = data;
            //dojoize
            parser.parse(nodeId);
            //if populated call callbackFunction
            if ( callbackFunction ) {
              callbackFunction();
            }
          },
          error: function(err) {
            //todo: make a custom dialog for errors
            //would be nice to display this in a nice fashion, 
            // probably also allow the user to avoid the error in future
            // an alternative would be to have some kind of error display at bottom of the page???
            alert('Error retrieving remote content:'+err);
          }
        });
      });
    },

    //make an ajax call to the given url
    // and append the result (which must be a set of table rows)
    // to the table
    appendAjaxTableRows: function(tableNode,rowIdPrefix,remoteUrl) {
      require(["dojo/dom-construct", "dojo/_base/xhr", "dojo/parser", "dojo/NodeList-traverse"], 
          function(domConstruct, xhr, parser) {
        xhr.get({
          url: remoteUrl,
          load: function(data) {
            
            var insertRowIdx = tableNode.rows.length-1; //table length includes header, so subtract 1
            var myTbody = tableNode.getElementsByTagName("tbody")[0];
            //todo: validate data?
            domConstruct.place(data, myTbody);
            //dojoize each new row
            for(var i=insertRowIdx; i<(tableNode.rows.length-1); i++) {
               var rowId = rowIdPrefix+i;
               parser.parse(rowId);
            }
          },
          error: function(err) {
            //todo: make a custom dialog for errors
            //would be nice to display this in a nice fashion, 
            // probably also allow the user to avoid the error in future
            // an alternative would be to have some kind of error display at bottom of the page???
            alert('Error retrieving remote content:'+err);
          }
        });
      });
    },
  //make an ajax call to the given url
    // and append the result (which must be a set of table rows)
    // to the tables with out having table header row. This function has added for CR920B for Notificaiton rule.
    appendAjaxTableRowsWithOutHeader: function(tableNode,rowIdPrefix,remoteUrl) {
      require(["dojo/dom-construct", "dojo/_base/xhr", "dojo/parser", "dojo/NodeList-traverse"], 
          function(domConstruct, xhr, parser) {
        xhr.get({
          url: remoteUrl,
          load: function(data) {
            
            var insertRowIdx = tableNode.rows.length; //table length does not includes table header.
            var myTbody = tableNode.getElementsByTagName("tbody")[0];
            domConstruct.place(data, myTbody);
            //parse each new row to dojo widgets.
            for(var i=insertRowIdx; i<(tableNode.rows.length); i++) {
               var rowId = rowIdPrefix+"_"+i;
            	parser.parse(rowId);
            }
          },
          error: function(err) {
            //todo: make a custom dialog for errors
            //would be nice to display this in a nice fashion, 
            // probably also allow the user to avoid the error in future
            // an alternative would be to have some kind of error display at bottom of the page???
            alert('Error retrieving remote content:'+err);
          }
        });
      });
    },
    
    appendAjaxInvTableRows: function(tableNode,rowIdPrefix,remoteUrl) {
        require(["dojo/dom-construct", "dojo/_base/xhr", "dojo/parser", "dojo/NodeList-traverse"], 
            function(domConstruct, xhr, parser) {
          xhr.get({
            url: remoteUrl,
            load: function(data) {
              var insertRowIdx = tableNode.rows.length-1; //table length includes header, so subtract 1
              var myTbody = tableNode.getElementsByTagName("tbody")[0];
              //todo: validate data?
              domConstruct.place(data, myTbody);
              //dojoize each new row
                var rowId = rowIdPrefix;
                parser.parse(rowId);
            },
            error: function(err) {
              //todo: make a custom dialog for errors
              //would be nice to display this in a nice fashion, 
              // probably also allow the user to avoid the error in future
              // an alternative would be to have some kind of error display at bottom of the page???
              alert('Error retrieving remote content:'+err);
            }
          });
        });
  },
    //for a given form name, figure out the form index #
    getFormNumber: function(formName) {
      //get the form number
      var formNumber = 0; //default
      for (var i=0; i<document.forms.length; i++) {
        var myForm = document.forms[i];
        if ( myForm != null && myForm.name == formName ) {
          formNumber = i;
          break;
        }
      }
      return formNumber;
    },
  
    //add a parameter to submit action for a form
    // by creating a hidden field on the form
    addParmToForm: function(myForm, parmName, parmValue) {
      var hiddenField = document.createElement("input");
      hiddenField.setAttribute("type", "hidden");
      hiddenField.setAttribute("name", parmName);
      hiddenField.setAttribute("value", parmValue);
      myForm.appendChild(hiddenField);
    },
  
    //submit the form
    submitForm: function(formName, buttonName) {
      //verify arguments at development time
      if ( formName == null ) {
        alert('Problem submitting form: formName required');
      }
      if ( buttonName == null ) {
        alert('Problem submitting form: buttonName required');
      }
  
      //get the form and submit it
      if ( document.getElementsByName(formName).length > 0 ) {
        var myForm = document.getElementsByName(formName)[0];

        var formNumber = getFormNumber(formName);
        setButtonPressed(buttonName, formNumber); 
        myForm.submit();
      } else {
        alert('Problem submitting form: form ' + formName + ' not found');
      }
    },
 
    //submit the form and add repeating parms to it
    //this currently supports a single parm name with multiple parm values
    //it iterates over the parm values and create a unique numbered
    //parm name
    //TODO: as necessary this function could be updated
    //to also take an array of parm name and values
    //then each pair could be evaluated to see if its a single
    //parm or a set of multiples.  will leave this for future exercise as necessary
    submitFormWithParms: function(formName, buttonName, parmName, parmValue) {
      //verify arguments at development time
      if ( formName == null ) {
        alert('Problem submitting form: formName required');
      }
      if ( buttonName == null ) {
        alert('Problem submitting form: buttonName required');
      }
      if ( parmName == null ) {
        alert('Problem submitting form: parmName required');
      }
  
      //get the form and submit it
      if ( document.getElementsByName(formName).length > 0 ) {
        var myForm = document.getElementsByName(formName)[0];

        //generate url parameters for the rowKeys and add to 
        //form as hidden fields
        if (parmValue instanceof Array) {
          for (var myParmIdx in parmValue) {
            var myParmValue = parmValue[myParmIdx];
            var myParmName = parmName + myParmIdx;
            addParmToForm(myForm, myParmName, myParmValue);
          }
        }
        else { //assume a single
          addParmToForm(myForm, parmName, parmValue);
        }
    
        var formNumber = getFormNumber(formName);
        setButtonPressed(buttonName, formNumber); 
        myForm.submit();
      } else {
        alert('Problem submitting form: form ' + formName + ' not found');
      }
    },
    
    
    //Variable to keep track of whether or not page was submitted
    formSubmitted: false,
    
    setButtonPressed: function(button, formNum) {
      // Don't let users submit the form twice 
      if(this.formSubmitted) {
        return false;
      }
    
      // Sets a hidden field with the 'button' that was pressed.  This 
      // value is passed along to the mediator to determine what 
      // action should be done.  formNum identifies which form to use 
      if(document.forms[formNum].buttonName != null) {
        document.forms[formNum].buttonName.value = button;
        // Flag the form as being submitted 
        this.formSubmitted=true;
      }
         
      // Continue with submit 
      return true;
    },
   
    //todo: this is NOT common - move this to shipment page specific 
    setShipmentButtonPressed: function(button, formNum, shipmentNumber) {
      // Don't let users submit the form twice 
      if(this.formSubmitted)
        return false;
    
      // Set the value of any additional hidden fields 
      if (document.forms[formNum].shipmentNumber != null) {
        document.forms[formNum].shipmentNumber.value = shipmentNumber;
      }
    
      // Sets a hidden field with the 'button' that was pressed.  This 
      // value is passed along to the mediator to determine what 
      // action should be done.  formNum identifies which form to use 
      if(document.forms[formNum].buttonName != null) {
        document.forms[formNum].buttonName.value = button;
        // Flag the form as being submitted 
        this.formSubmitted=true;
      }
         
      // Continue with submit 
      return true;
    },
    
    autoSaveForTimeOut: function(button, formNum) {
      if(document.forms.length > formNum) {
        if(setButtonPressed(button, formNum)) {
          document.forms[formNum].submit();
        }
      }
      return;
    },
      
    
    
    
    //help window functionality ported from header
    
    
    // this will place help information into a popup window
    openHelp: function(url) {
      var widgets;
      var moveWindow;
      var helpWin;
      if(helpWin && helpWin.open && !helpWin.closed) {
        widgets = 'toolbar=yes,location=no,directories=no,status=no,menubar=yes,scrollbars=yes,resizable=yes';
        moveWindow = false;
      }
      else {
        widgets = 'toolbar=yes,location=no,directories=no,status=no,menubar=yes,scrollbars=yes,resizable=yes,width=750,height=425';
        moveWindow = true;
      }
    
      helpWin = window.open(url, 'TradePortalHelp', widgets);
      if(moveWindow == true) {
        helpWin.moveTo(0,0);
      }
      helpWin.opener.top.name = "opener";
      helpWin.focus();
    },
    
    openCorpCustAddressSearchDialog : function(corpOrgOid){
    	require(["t360/dialog","t360/common"], function(dialog, common ) {
		dialog.open('corpCustAddressSearchDialog', corpCustAddressSearchDialogTitle,'CorpCustAddressSearchDialog.jsp','corp_org_oid',corpOrgOid,'setAddress', common.setAddress);
		});
    },
    setAddress: function (addressOid, addressType){
    	require(["t360/common"], function(common ) {
	    	document.forms[0].selection.value=addressOid;
	    	if((addressType != null || addressType !="") && addressType == "PRIMARY"){
	    		document.forms[0].ChoosePrimary.value=addressType;
	    	}
	    	common.setButtonPressed(corpCustButtonPressed, 0); 
	    	document.forms[0].submit();
    	});
    	
    },
    openCopySelectedDialogHelper: function (selectedGrid, fromListView, rowKey, instrType){
		
        require(["t360/dialog","dijit/registry"], function(dialog,registry) {
        	var selectedRow;
        	fromListViewGlobal = fromListView;
        	if(fromListView == 'Y' ){
	        	  if(registry.byId(selectedGrid) != undefined){
	          		 selectedRow = getSelectedGridRowKeys(selectedGrid);
	        	  }
		          if(selectedRow.length == 0){
		        	  alert(copyInstrumentDialogPrompt1);
		          }else if(selectedRow.length >1){
		        	  alert(copyInstrumentDialogPrompt2);
		          }else {
		        	  dialog.open('copySelectedInstrument', copyInstrumentDialogTitle,
		                      'copySelectedInstrumentDialog.jsp',
		                      "instrumentType", instrType, //parameters
		                      'select', common.copySelectedToInstrument);
		          }	
        	}else{
        		rowKeyValue = rowKey;
        		dialog.open('copySelectedInstrument', copyInstrumentDialogTitle,
	                      'copySelectedInstrumentDialog.jsp',
	                      "instrumentType", instrType, //parameters
	                      'select', common.copySelectedToInstrument);
        	}    
        });
    },
    copySelectedToInstrument: function(bankBranchOid, copyType, templateName, templateGroup, flagExpressFixed){
			
    	var rowKeys ;
    	 require(["dijit/registry", "dojo/on", "t360/dialog", "dojo/domReady!"],
    		        function(registry, on, dialog ) {
    		      if(fromListViewGlobal == 'Y'){
    		    	  if(registry.byId("instrumentsSearchGridId") != undefined){
    		       	  	rowKeys = getSelectedGridRowKeys("instrumentsSearchGridId");
	    		      }else if(registry.byId("tradePendGrid") != undefined){
	    		    		rowKeys = getSelectedGridRowKeys("tradePendGrid");
	    		      }
    		      }
    	 });
    	 rowKeys = rowKeyValue;
    	if(copyType == 'I'){ 
    		if ( document.getElementsByName('NewInstrumentForm').length > 0 ) {
    		    var theForm = document.getElementsByName('NewInstrumentForm')[0];
    		    theForm.mode.value="CREATE_NEW_INSTRUMENT";
    		    theForm.copyType.value="Instr";
    		    theForm.bankBranch.value=bankBranchOid;
    		    theForm.copyInstrumentOid.value=rowKeys;
    		    theForm.submit();
    		  }
    	}	
    	if(copyType == 'T'){ 
    		if ( document.getElementsByName('NewTemplateForm1').length > 0 ) {
    			var flag = flagExpressFixed.split('/');
    			var expressFlag = flag[0];
    			var fixedFlag = flag[1];
    			var theForm = document.getElementsByName('NewTemplateForm1')[0];
    		    theForm.name.value=templateName;
    		    theForm.mode.value="CREATE_NEW_TEMPLATE";
    		    theForm.CopyType.value="Instr";
    		    theForm.fixedFlag.value=fixedFlag;
    		    theForm.expressFlag.value=expressFlag;
    		    theForm.PaymentTemplGrp.value=templateGroup;
    		    theForm.copyInstrumentOid.value=rowKeys;
    		    theForm.validationState.value = "";
    		    theForm.submit();
    		  }
    	}
    },
	//Srinivasu_D Rel8.4 Kyriba 10/04/2013 - start
	 openCopyConvertedSelectedDialogHelper: function (selectedGrid, fromListView, rowKey, instrType){
		
        require(["t360/dialog","dijit/registry"], function(dialog,registry) {
        	var selectedRow;
        	fromListViewGlobal = fromListView;
        	if(fromListView == 'Y' ){
	        	  if(registry.byId(selectedGrid) != undefined){
	          		 selectedRow = getSelectedGridRowKeys(selectedGrid);
	        	  }
		          if(selectedRow.length == 0){
		        	  alert(copyInstrumentDialogPrompt1);
		          }else if(selectedRow.length >1){
		        	  alert(copyInstrumentDialogPrompt2);
		          }else {
		        	  dialog.open('copySelectedInstrument', copyInstrumentDialogTitle,
		                      'copySelectedConversionInstrumentDialog.jsp',
		                      "instrumentType", instrType, //parameters
		                      'select', common.copySelectedConversionToInstrument);
		          }	
        	}else{
        		rowKeyValue = rowKey;
        		dialog.open('copySelectedInstrument', copyInstrumentDialogTitle,
	                      'copySelectedConversionInstrumentDialog.jsp',
	                      "instrumentType", instrType, //parameters
	                      'select', common.copySelectedConversionToInstrument);
        	}    
        });
    },
    copySelectedConversionToInstrument: function(bankBranchOid, copyType, templateName, templateGroup, flagExpressFixed){
		
    	var rowKeys ;
    	 require(["dijit/registry", "dojo/on", "t360/dialog", "dojo/domReady!"],
    		        function(registry, on, dialog ) {
    		      if(fromListViewGlobal == 'Y'){
    		    	  if(registry.byId("instrumentsSearchGridId") != undefined){
    		       	  	rowKeys = getSelectedGridRowKeys("instrumentsSearchGridId");
	    		      }else if(registry.byId("tradePendGrid") != undefined){
	    		    		rowKeys = getSelectedGridRowKeys("tradePendGrid");
	    		      }
    		      }
    	 });
    	 rowKeys = rowKeyValue;
		 var arry = flagExpressFixed.split('/');
		 var manualInstId = arry[2];
		
    	if(copyType == 'I'){ 
    		if ( document.getElementsByName('NewInstrumentForm').length > 0 ) {
    		    var theForm = document.getElementsByName('NewInstrumentForm')[0];
    		    theForm.mode.value="CREATE_NEW_INSTRUMENT";
    		    theForm.copyType.value="Instr";
    		    theForm.bankBranch.value=bankBranchOid;
    		    theForm.copyInstrumentOid.value=rowKeys;
				theForm.manualInstrumentOid.value = manualInstId;
				theForm.conversionCenterMenuInd.value = "Y";
				theForm.ConvTransInd.value = "Y";
    		    theForm.submit();
    		  }
    	}	
    	if(copyType == 'T'){ 
    		if ( document.getElementsByName('NewTemplateForm1').length > 0 ) {				
				
				
    			var flag = flagExpressFixed.split('/');
    			var expressFlag = flag[0];
    			var fixedFlag = flag[1];
				
    			var theForm = document.getElementsByName('NewTemplateForm1')[0];
    		    theForm.name.value=templateName;
    		    theForm.mode.value="CREATE_NEW_TEMPLATE";
    		    theForm.CopyType.value="Instr";
    		    theForm.fixedFlag.value=fixedFlag;
    		    theForm.expressFlag.value=expressFlag;
    		    theForm.PaymentTemplGrp.value=templateGroup;
    		    theForm.copyInstrumentOid.value=rowKeys;
    		    theForm.validationState.value = "";	
    		    theForm.submit();
    		  }
    	}
    },
		//Srinivasu_D Rel8.4 Kyriba 10/04/2013 - end
    //cquinton 9/17/2012
    //get the absolute x position of the object within the page
    // see: http://www.quirksmode.org/js/findpos.html
    getXPos: function(obj) {
      var curleft = 0;
      if (obj.offsetParent) {
        do {
          curleft += obj.offsetLeft;
        } while (obj = obj.offsetParent);
      }
      return curleft;
    },
    getYPos: function(obj) {
      var curtop = 0;
      if (obj.offsetParent) {
        do {
          curtop += obj.offsetTop;
        } while (obj = obj.offsetParent);
      }
      return curtop;
    },

    //cquinton 1/28/2013 new function to help with javascript error section management
    //add errors in standard error xml format to a given error section
    //return count of number errors added
    addXmlErrorsToErrorSection: function(errorSectionId, errorXml, showDuplicates) {
      var errorCount = 0;
      var myErrorXml = errorXml;
      //by default show duplicates, but if showDuplicates=false filter them out
      var showDups = false;
      if ( typeof showDuplicates === 'undefined' ) {
         showDups = true; //default
      }

      var errSection = dom.byId(errorSectionId);
      //safeguard to ensure error section actually exists
      if ( errSection ) {
        var foundAnError = true;
        while (foundAnError) {
          foundAnError = false; //prime it
          var startErrorIdx=myErrorXml.indexOf("<error ID");
          var endErrorIdx=myErrorXml.indexOf("</error>");
          if ( startErrorIdx>=0 && endErrorIdx>=0 ) {
            var anErrorXml = myErrorXml.substring(startErrorIdx, endErrorIdx);
            var startMsgIdx=anErrorXml.indexOf("<message>");
            var endMsgIdx=anErrorXml.indexOf("</message>");
            if ( startMsgIdx>=0 && endMsgIdx>=0 ) {
              var errorMsg = anErrorXml.substring(startMsgIdx+9, endMsgIdx);
              foundAnError=true;

              //create the errWarn section if necessary
              var hasErrWarnSection = false;
              var errWarnDiv;
              //cquinton 3/3/2013 fix issue with errorsAndWarning not existing
              var nl = query(".errorsAndWarnings", errSection);
              if ( nl && nl.length>0 ) {
                hasErrWarnSection = true;
                errWarnDiv = nl[0];
              }
              if ( !hasErrWarnSection ) {
                errWarnDiv = domConstruct.create("div");
                domClass.add(errWarnDiv, "errorsAndWarnings");
                domConstruct.place(errWarnDiv, errSection);
              }

              //see if the error message already exists
              var errorExists = false;
              if ( !showDups ) {
                query(".errorText").forEach( function(entry, i) {
                  var existingMsg = entry.innerHTML;
                  if ( existingMsg && existingMsg == errorMsg ) {
                    errorExists = true;
                  }
                });
              }

              if ( !errorExists ) {
                var errDiv = domConstruct.create("div", { innerHTML: errorMsg });
                domClass.add(errDiv, "errorText");
                domConstruct.place(errDiv, errWarnDiv);
                errorCount += 1;
              }
            }
          }
          myErrorXml = myErrorXml.substring(endErrorIdx+8);
        }
      }
      return errorCount;
    },
    
    //vdesingu Rel8.4 CR-590, Add Errors to the error section 
    addErrorsToErrorSection: function(errorSectionId, errorJson, showDuplicates) {
    	//loop through errors and add to errorSection as specified.
    	//if showDuplicates==true, check the error section for the error before adding
    	//  and if pre-existing do not add

    	//create the errWarn section if necessary
    	var errorCount = 0;
    	var errSection = dom.byId(errorSectionId);

    	//by default show duplicates, but if showDuplicates=false filter them out
    	var showDups = false;
    	if ( typeof showDuplicates === 'undefined' ) {
    		showDups = true; //default
    	}

    	if(errSection){
    		
    		var hasErrWarnSection = false;
    		var errWarnDiv;
    		//cquinton 3/3/2013 fix issue with errorsAndWarning not existing
    		var nl = query(".errorsAndWarnings", errSection);
    		if ( nl && nl.length>0 ) {
    			hasErrWarnSection = true;
    			errWarnDiv = nl[0];
    		}

    		if ( !hasErrWarnSection ) {
    			errWarnDiv = domConstruct.create("div");
    			domClass.add(errWarnDiv, "errorsAndWarnings");
    			//domConstruct.place(errWarnDiv, errSection);
    		}

    		//see if the error message already exists
    		var errorArray = new Array();
    		var warnArray = new Array();
    		var index=0;
    		
    		
    		for (var i = 0; i < errorJson.length; i++) {
    			if ( !showDups ) {
    				query(".errorText").forEach( function(entry, i) {
    					var existingMsg = entry.innerHTML;
    					if ( existingMsg && existingMsg != errorJson[i].ErrorText ) {
    						errorArray[index++] = errorJson[i].ErrorText;
    					}        				
    				});

    			}
    		}
    		index=0;
    		for (var i = 0; i < errorJson.length; i++) {
    			if ( !showDups ) {
    				query(".warnText").forEach( function(entry, i) {
    					var existingMsg = entry.innerHTML;
    					if ( existingMsg && (existingMsg != errorJson[i].ErrorText) ) {
    						warnArray[index++] = errorJson[i].ErrorText;
    					}        				
    				});

    			}
    		}
    		if(errorArray.length > 0 || warnArray.length > 0 || errorJson.length > 0){
    			domConstruct.place(errWarnDiv, errSection);
    		}
    		var errCode = null;
    		var warCode = null;
    		if( errorArray.length > 0 ){
    			for( var i=0; i<errorArray.length; i++ ){
    				var errDiv = domConstruct.create("div", { innerHTML: errorArray[i] });
    				domClass.add(errDiv, "errorText");
    				domConstruct.place(errDiv, errWarnDiv);
    			}
    		}else{
    			for( var i=0; i<errorJson.length; i++ ){
    				errCode = errorJson[i].ErrorCode;
    				if(errCode){
    					if( 'E'==errCode.charAt(0) ){
    						var errDiv = domConstruct.create("div", { innerHTML: errorJson[i].ErrorText });
    						domClass.add(errDiv, "errorText");
    						domConstruct.place(errDiv, errWarnDiv);
    					}

    				}
    			}
    		}

    		if( warnArray.length > 0 ){
    			for( var i=0; i<warnArray.length; i++ ){
    				var warnDiv = domConstruct.create("div", { innerHTML: warnArray[i] });
    				domClass.add(warnDiv, "warnText");
    				domConstruct.place(warnDiv, errWarnDiv);
    			}
    		}else{
    			for( var i=0; i<errorJson.length; i++ ){
    				warCode = errorJson[i].ErrorCode;
    				if(warCode){
    					if( 'W'==warCode.charAt(0) ){
    						var warnDiv = domConstruct.create("div", { innerHTML: errorJson[i].ErrorText });
    						domClass.add(warnDiv, "warnText");
    						domConstruct.place(warnDiv, errWarnDiv);
    					}

    				}
    			}
    		}
    	}
    },
    
    /*vdesingu Rel8.4 CR-590, remove all errors, warning, info messages from the errorSection
     * */
    clearErrorSection: function(errorSectionId) {
    	  
    	var errSection = dom.byId(errorSectionId);
    	
    	//var beneAccounts = dojo.byId("beneAccountsData");
    	require(["dojo/_base/array","dijit/registry"], function(arrayUtil, registry){
    		if (errSection){
		        var errSectionWidgets = registry.findWidgets(errSection);
		        arrayUtil.forEach(errSectionWidgets, function(entry, i) {
		          entry.destroyRecursive(true);
		        });
		        domConstruct.empty(errSection);
    		}
    	});  
    	
    }

  };

  return common;
});
