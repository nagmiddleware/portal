define([
	"dojo/_base/array",
	"dojo/_base/declare",
	"dojo/_base/lang",
	"dojo/_base/xhr",
	"dojo/store/Memory",
	"dojo/store/util/QueryResults"
], function(arrayUtil, declare, lang, xhr, Memory, QueryResults){
	//	summary:
	//		This module defines a store that conforms to the dojo/store API,
	//		where all operations write operations are performed in memory. Any
	//		queries for data will reach out to a server.
	return declare(Memory, {
		//	idProperty: String
		//		Indicates the property to use as the identity property. The values of this
		//		property should be unique.
		idProperty: "rowKey",

		//	target: String
		//		The target base URL to use for all requests to the server. This string will be
		//		prepended to the id to generate the URL (relative or absolute) for requests
		//		sent to the server
		target: "",

		query: function(query, options){
			//	summary:
			//		Queries the store for objects. This will trigger a GET request to the server, with the
			//		query added as a query string.
			//	query: Object
			//		The query to use for retrieving objects from the store.
			//	options: dojo.store.api.Store.QueryOptions?
			//		The optional arguments to apply to the resultset.
			//	returns: dojo.store.api.Store.QueryResults
			//		The results of the query, extended with iterative methods.
			var querystring = "", self = this;

			if (query && typeof query == "object"){
				querystring = xhr.objectToQuery(query);
				querystring = querystring ? "?" + query : "&";
			}

			if (options.start >= 0 || options.count >= 0){
				querystring += (querystring ? "&" : "?");
				querystring += "start=" + (options.start ? options.start : "0");
				querystring += "&count=" + (options.count ? options.count : "0");
			}

			var results = xhr.get({
				url: this.target + querystring,
				handleAs: "json"
			});
			
			var items = results.then(function(data){
				// Iterate over the items we got back, making sure
				// that they get put into the store
				arrayUtil.forEach(data.items, function(item){
					var storeItem = self.get(self.getIdentity(item));

					if (!storeItem) {
						Memory.prototype.put.call(self, item);
					}
				});

				// Override the items with a localized query
				return self.queryEngine(query, options)(self.data);
			});

			// Set up some delegation on the promise, as it is frozen
			items = lang.delegate(items);

			// Set up what the total actually is
			items.total = results.then(function(data){
				return Math.max(data.numRows, self.data.length);
			});

			return QueryResults(items);
		}
	});
});
