//configure dojo - this is executed before dojo is loaded to get the
// modules defined correctly.

var dojoConfig = {     
  baseUrl: "/portal/js/",
  packages: [
    // register packages with identical name and location
    // More verbose declaration, for reference: { name: 'app', location: 'app', packageMap: {} }
    "dojo",
    "dijit",
    "dojox",
    "t360",
    "widget",
    //commented out additionals will want later
    'xstyle',
    'put-selector',
    'dgrid',
    'dojo-smore'
  ],
  //cacheBust:"1.9.2",//Cache all above packages with dojo version. RKAZI 08/29/2014 - commneted this as it is set now from Footer.jsp using the encrypted version value. 
  xdWaitSeconds: 5
};
