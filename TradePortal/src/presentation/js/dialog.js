////ATTENTION!!!!!!!!! THIS HAS BEEN REPlACED WITH t360/dialog.js !!!!!!!!!!!!!!
///please do not add any additional javascript here!!!!!!!!!!!
/// use dialog.open() instead

/*
 * dialog.js - functions to create specific dialogs and handling of callbacks.
 *
 * Please do not place any code specific to calling page functionality in this
 * library.  The code here is meant to be generic to allow for dialogs to
 * be used in multiple ways.
 * Typically what should be included is:
 *  1 - openXxxDialog() function to open the dialog, pass in any specific
 *      data from the calling page for dialog behavior plus any callback function
 *      references.
 *  2 - Callback functions that the dialog to callback the specific page function
 *      given dialog actions.
 *  3 - Any other behavior that should occur on the dialog itself 
 *      (not on the calling page).
 *
 */



function openSaveAsDialog(saveAsDialogId, dialogTitle, pageName) {
	
  require(["dijit/registry", "dojo/dom-class", "dijit/Dialog"], function(registry, domClass) {

    var dialogUrl =  "/portal/dialog/"+pageName;
    
    //only instantiate if it doesn't exist
    if (!registry.byId(saveAsDialogId)) {
      //use a simple global javascript variable for the dialog
      // this assumes a single dialog of this type open at a time
      saveAsDialogOb = new dijit.Dialog({
          title: dialogTitle,
	  //load the dialog from url
          href: dialogUrl
	}, saveAsDialogId );

      //add a class for styling
        domClass.add(saveAsDialogOb.domNode, "normalDialog");
    } else { 
      //just reset the href on re-entry
    	saveAsDialogOb.set("href",dialogUrl);
    }
    saveAsDialogOb.closeButtonNode.setAttribute("id",saveAsDialogId+"Id");
    //open it
    showDialog(saveAsDialogOb);
  });
}


////////////////////////////////////////////////////////////////
//Route Messages
//Arguments:
// 1 - dialogId - used to identify the dialog div for hiding, etc.
// 2 - dialogTitle - its a text resource needs to be rendered in a jsp
// 3 - selectedGrid - Grid Id
// 4 - fromListView - Y/N on whether dialog box is opened from a listview page
// 5 - fromWhere - navigation to open dialog
// 6 - rowKey 
////////////////////////////////////////////////////////////////
var fromListViewRoute;
var fromWhereGlobal;
function openRouteDialog(dialogId, dialogTitle, selectedGrid, fromListView, fromWhere, rowKey, gridDisplayValue1, gridDisplayValue2, gridDisplayValue3) {
	fromWhereGlobal = fromWhere;
	require(["t360/dialog"], function(dialog) {
		if(fromListView == 'Y'){
			fromListViewRoute = 'Y';
			dialog.open(dialogId, dialogTitle,
	                      'routeDialog.jsp',
	                      ['selectedCount','fromListView','fromWhere', 'rowKey'], [getSelectedGridItems(selectedGrid).length,fromListView,fromWhere,getSelectedGridRowKeys(selectedGrid)], //parameters
	                      'select', this.routeSelectedItems);
		}else{
			fromListViewRoute = 'N';
			dialog.open(dialogId, dialogTitle,
                    'routeDialog.jsp',
                    ['fromListView','fromWhere', 'rowKey','gridDisplayValue1','gridDisplayValue2', 'gridDisplayValue3'], [fromListView,fromWhere,rowKey, gridDisplayValue1,gridDisplayValue2,gridDisplayValue3], //parameters
                    'select', this.routeSelectedItems);
		}
	  }); 
}
function routeSelectedItems(routeToUser, routeUserOid, routeCorporateOrgOid, multipleOrgs, rowKey){
	if(fromWhereGlobal == 'M'){
			if ( document.getElementsByName('RouteMessagesForm').length > 0 ) {
		        var theForm = document.getElementsByName('RouteMessagesForm')[0];
		        theForm.RouteUserOid.value=routeUserOid;
		        if(routeCorporateOrgOid != ""){
		        	theForm.RouteCorporateOrgOid.value=routeCorporateOrgOid;
		        }
		        theForm.RouteToUser.value=routeToUser;
		        theForm.multipleOrgs.value=multipleOrgs;
		        theForm.routeAction.value='Y';
		        theForm.fromListView.value='N';
		        var buttonName = 'Route';
	
		        submitFormWithParms("RouteMessagesForm", buttonName, "checkbox", rowKey);
	
		 }
	}else if(fromWhereGlobal == 'P'){
		if ( document.getElementsByName('RoutePayRemitsForm').length > 0 ) {
	        var theForm = document.getElementsByName('RoutePayRemitsForm')[0];
	        theForm.RouteUserOid.value=routeUserOid;
	        if(routeCorporateOrgOid != ""){
	        	theForm.RouteCorporateOrgOid.value=routeCorporateOrgOid;
	        }
	        theForm.RouteToUser.value=routeToUser;
	        theForm.multipleOrgs.value=multipleOrgs;
	        theForm.routeAction.value='Y';
	        theForm.fromListView.value='N';
	        theForm.buttonName.value='Route';
	        var buttonName = 'Route';

	        if(fromListViewRoute == 'Y'){
	        	submitFormWithParms("RoutePayRemitsForm", buttonName, "checkbox", rowKey);
	        }else{
	   	    	submitFormWithParms("RoutePayRemitsForm", buttonName, "receivable_remittance_info", rowKey);
	   	    }	 

	 }
	}else{
			if ( document.getElementsByName('RouteTransactionsForm').length > 0 ) {
		        var theForm = document.getElementsByName('RouteTransactionsForm')[0];
		        theForm.RouteUserOid.value=routeUserOid;
		        if(routeCorporateOrgOid != ""){
		        	theForm.RouteCorporateOrgOid.value=routeCorporateOrgOid;
		        }
		        theForm.RouteToUser.value=routeToUser;
		        theForm.multipleOrgs.value=multipleOrgs;
		        theForm.routeAction.value='N';
		        theForm.fromListView.value= fromListViewRoute;
		        var buttonName = 'Route';
		        var checkbox = 'checkbox';		       
		        if(rowKey=='' || rowKey==""){
		        	checkbox = "";
	    		}
		   	    if(fromListViewRoute == 'Y'){
		   	    	submitFormWithParms("RouteTransactionsForm", buttonName, checkbox, rowKey);
		   	    }else{
		   	    	submitFormWithParms("RouteTransactionsForm", buttonName, "transaction_instrument_info", rowKey);
		   	    }	   	    	        
			}
	}
} 
//Pavani CR 821 Rel8.3
function opendialogViewPanelAuths(title,gridId, fromPage){
	 var dialogDiv = document.createElement("div");
	 dialogDiv.setAttribute('id',"viewPanelAuthsdialogdivid");
	 document.forms[0].appendChild(dialogDiv);
	 // DK IR T36000025093 Rel8.4 02/21/2014 starts
	require(["t360/dialog","dijit/registry","t360/datagrid","t360/OnDemandGrid"], function(dialog,registry,t360grid,OnDemandGrid ) {
		var rowKeys;
		if(gridId=='homePanelAuthTranGrid' || gridId == 'homeRcvTranGrid' || gridId == 'homeInvoicesOfferedGrid'){ // Nar IR-T36000026217 Rel 8.4
			rowKeys = OnDemandGrid.getSelectedGridRowKeys(gridId);
		}			
		else {
			rowKeys = t360grid.getSelectedGridRowKeys(gridId);	
		}
	// DK IR T36000025093 Rel8.4 02/21/2014 ends
     dialog.open('viewPanelAuthsdialogdivid',title ,
                 'ViewPanelAuthorisationsDialog.jsp',
                 ['DailogId','rowkey','insType'],['quickviewdialogdivid',rowKeys,fromPage], //parameters
                 'select', null);
   });
}

function opendialogViewPanelAuthsFromSidebar(title,rowKeys, fromPage, isEncrypted){
	 var dialogDiv = document.createElement("div");
	 dialogDiv.setAttribute('id',"viewPanelAuthsdialogdivid");
	 document.forms[0].appendChild(dialogDiv);

	require(["t360/dialog","dijit/registry","t360/datagrid"], function(dialog,registry,t360grid ) {
	dialog.open('viewPanelAuthsdialogdivid',title ,
                'ViewPanelAuthorisationsDialog.jsp',
                ['DailogId','rowkey','insType','isEncrypted'],['quickviewdialogdivid',rowKeys,fromPage,isEncrypted], //parameters
                'select', null);
  });
}
function openNotify(panelLevel,paramOid1, paramOid2, fromPage, title){
	var dialogDiv = document.createElement("div");
	 dialogDiv.setAttribute('id',"NotifyPendingPanelAuthEmailDialog");
	 document.forms[0].appendChild(dialogDiv);
	 
	require(["t360/dialog"], function(dialog) {		 
	       dialog.open('NotifyPendingPanelAuthEmailDialog', title, 'NotifyPendingPanelAuthEmailDialog.jsp', 
	    		   ['panelLevel','paramOid1','paramOid2','fromPage'],[panelLevel,paramOid1, paramOid2, fromPage],
          null, null);
});
}

function openViewBene(panelAuthRangeOid,transactionOid, title){
	var dialogDiv = document.createElement("div");
	 dialogDiv.setAttribute('id',"PendingPanelAuthViewBeneficiaryDialog");
	 document.forms[0].appendChild(dialogDiv);
	 
	require(["t360/dialog"], function(dialog) {		 
	       dialog.open('PendingPanelAuthViewBeneficiaryDialog', title, 'PendingPanelAuthViewBeneficiaryDialog.jsp', 
	    		   ['panelAuthRangeOid','transactionOid'],[panelAuthRangeOid,transactionOid], 
          null, null);
});
}