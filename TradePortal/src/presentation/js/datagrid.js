//common datagrid functions

//!!!!!!!!! these functions are deprecated!!!!!
// call t360grid.function() directly instead rather than these pass throughs!!!!!

//create a standard datagrid from a dataview
function createDataGrid(dataGridId, dataViewName, gridLayout, searchParms,sortIndex) {
  require(["t360/datagrid"], function(t360grid){
    t360grid.createDataGrid(dataGridId, dataViewName, gridLayout, searchParms,sortIndex);
  });
}
function createLazyTreeDataGrid(dataGridId, dataViewName, gridLayout, searchParms,sortIndex) {
	  require(["t360/datagrid"], function(t360grid){
	    t360grid.createLazyTreeDataGrid(dataGridId, dataViewName, gridLayout, searchParms,sortIndex);
	  });
	}

//T36000034958 - Rel-9.2 CR-914A 12/02/2014 - Adding createMultiSelectLazyTreeDataGrid()
function createMultiSelectLazyTreeDataGrid(dataGridId, dataViewName, gridLayout, searchParms,sortIndex) {
	  require(["t360/datagrid"], function(t360grid){
	    t360grid.createMultiSelectLazyTreeDataGrid(dataGridId, dataViewName, gridLayout, searchParms,sortIndex);
	  });
	}
function searchDataGrid(dataGridId, dataViewName, searchParms, gridLoadingMessage){
  require(["t360/datagrid"], function(t360grid){ 
    t360grid.searchDataGrid(dataGridId, dataViewName, searchParms, gridLoadingMessage);
  });
}


//create a datagrid from in-memory data
function createMemoryDataGrid(dataGridId, gridData, gridLayout) {
  require(["t360/datagrid"], function(t360grid){ 
    t360grid.createMemoryDataGrid(dataGridId, gridData, gridLayout);
  });
}

//start a datagrid from existing memory store parameter
function startMemoryDataGrid(dataGridId, myStore, gridLayout) {
  require(["t360/datagrid"], function(t360grid){ 
    t360grid.startMemoryDataGrid(dataGridId, myStore, gridLayout);
  });
}

//get an array of selected row key values from the given grid
function getSelectedGridRowKeys(dataGridId){
  var rowKeys = null;
  require(["t360/datagrid"], function(t360grid){ 
    rowKeys = t360grid.getSelectedGridRowKeys(dataGridId);
  });
  return rowKeys;
}

//get an array of selected items from the given grid
function getSelectedGridItems(dataGridId){
  var items = null;
  require(["t360/datagrid"], function(t360grid){ 
    items = t360grid.getSelectedGridItems(dataGridId);
  });
  return items;
}

//use this to format links in the grid
function formatGridLink(columnValues) {
  var gridLink = "";
  gridLink = t360gridFormatters.formatGridLink(columnValues);
  return gridLink;
}

//use this to format address in the grid
//cquinton 11/10/2012 - this is unused. commenting out
//use t360gridFormatters.addressFormatter instead
//function addressFormatter(fields) {
//  var formattedAddress = "";
//  formattedAddress = t360gridFormatters.addressFormatter(fields);
//  return formattedAddress;
//}

