<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.html.*" %>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager"
    scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean"
   scope="session">
</jsp:useBean>


<%-- ************** Data retrieval page setup begins here ****************  --%>

<%
	WidgetFactory widgetFactory = new WidgetFactory(resMgr);
   	
   	StringBuffer downloadLink = new StringBuffer();
    downloadLink.append(request.getContextPath());
    downloadLink.append("/common/TradePortalDownloadServlet?");
    downloadLink.append(TradePortalConstants.DOWNLOAD_REQUEST_TYPE);
    downloadLink.append("=");
    downloadLink.append(TradePortalConstants.DOWNLOAD_AUTHORISED_TRANSACTION_XML);
    
    String parms = request.getParameter("selectedData");
%>


<%-- ********************* HTML for page begins here *********************  --%>

	<jsp:include page="/common/Header.jsp">
  <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_NO%>" />
  <jsp:param name="includeErrorSectionFlag"  value="<%=TradePortalConstants.INDICATOR_NO%>" />
  <jsp:param name="includeTimeoutForward" value="false" />
  <jsp:param name="checkChangePassword" value="<%= TradePortalConstants.INDICATOR_NO %>" />
</jsp:include>
<div Class="pageContent">
<div class="secondaryNav">
                    
</div>

<form name="XMLDownloadForm" method="POST" action="<%=downloadLink.toString()%>">     
<input type=hidden name="buttonName"     value="">
<div class="pageSubHeader">
    <span class="pageSubHeaderItem">
    	<%= resMgr.getText("AuthorizedXMLTransactions.DownloadPageHeading", TradePortalConstants.TEXT_BUNDLE) %>
    </span>
    <span class="pageSubHeader-right">
    <button data-dojo-type="dijit.form.Button" name="CloseButton" id="CloseButton" type="button" class="pageSubHeaderButton">
        <%=resMgr.getText("common.CloseText",TradePortalConstants.TEXT_BUNDLE)%>
        <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
          openURL("<%=formMgr.getLinkAsUrl("goToXMLDownloads",response)%>");
        </script>
      </button>
      <%=widgetFactory.createHoverHelp("CloseButton","ReturnPreviousHoverText") %>
    </span>
    <div style="clear:both;"></div>
  </div>
<div Class="formContentNoSidebar">
<br>&nbsp;<br>
		<div Class="formItem">
       <%= resMgr.getText("AuthorizedXMLTransactions.DownloadMessage1", TradePortalConstants.TEXT_BUNDLE) %><br>&nbsp;<br>
       <%= resMgr.getText("AuthorizedXMLTransactions.DownloadMessage2", TradePortalConstants.TEXT_BUNDLE) %><br>&nbsp;<br>
		</div>
  </div>
</div>
</form>

<jsp:include page="/common/Footer.jsp">
  <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>

 
	</body>
</html>

<script LANGUAGE="JavaScript">

require(["dojo/dom", "dojo/ready"],
	      function(dom, ready) {
		  ready(function() {
			  	var parms ="<%=parms%>";
			    var rowKeys = parms.split(",");
				submitFormWithParms("XMLDownloadForm","Download", "transactionOid", rowKeys);
				return false;
		  });
		  
});

function openURL(URL){
    if (isActive =='Y') {
    	if (URL != '' && URL.indexOf("javascript:") == -1) {
	        var cTime = (new Date()).getTime();
	        URL = URL + "&cTime=" + cTime;
	        URL = URL + "&prevPage=" + context;
    	}
    }
		document.location.href  = URL;
		return false;
	}

</script>
