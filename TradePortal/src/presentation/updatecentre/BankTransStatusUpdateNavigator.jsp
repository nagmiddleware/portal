<%--
*******************************************************************************
                              Bank Transaction Update Navigator Page
                              
This page is an common page used to determine which instrument/transaction page
to be displayed.  This navigator page is triggered from BankTransactionUpdateDataView.xml.

*******************************************************************************
--%>

<%@ page
	import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.html.*,
                 com.ams.tradeportal.busobj.*"%>
<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager"
	scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager"
	scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager"
	scope="session">
</jsp:useBean>

<jsp:useBean id="userSession"
	class="com.ams.tradeportal.busobj.webbean.SessionWebBean"
	scope="session">
</jsp:useBean>


<%-- ************** Data retrieval page setup begins here ****************  --%>

<%
	boolean getFromDB = false;
	boolean multiPartTabPressed = false;

	String instrumentOid = null;
	String transactionOid;
	String templateOid = "";

	String instrumentType;
	String instrumentStatus;
	String transactionType;
	String transactionStatus;
	boolean isTemplate = false;
	boolean isFromTransTermsFlag = false;
	String transTermsFlag;
	String newTransaction;

	TransactionWebBean transaction;
	InstrumentWebBean instrument;
	TemplateWebBean template;
	BankTransactionUpdateWebBean btuWebBean;

	HttpSession theSession = request.getSession(false);
	DocumentHandler doc = formMgr.getFromDocCache();

	transaction = (TransactionWebBean) beanMgr.getBean("Transaction");
	instrument = (InstrumentWebBean) beanMgr.getBean("Instrument");
	btuWebBean = (BankTransactionUpdateWebBean) beanMgr
			.getBean("BankTransactionUpdate");

	if (transaction == null) {
		Debug.debug("No transaction bean found, creating");
		beanMgr.registerBean(
				"com.ams.tradeportal.busobj.webbean.TransactionWebBean",
				"Transaction");
		transaction = (TransactionWebBean) beanMgr
				.getBean("Transaction");
		getFromDB = true;
	}

	if (instrument == null) {
		Debug.debug("No instrument bean found, creating");
		beanMgr.registerBean(
				"com.ams.tradeportal.busobj.webbean.InstrumentWebBean",
				"Instrument");
		instrument = (InstrumentWebBean) beanMgr.getBean("Instrument");
		getFromDB = true;
	}

	if (btuWebBean == null) {
		Debug.debug("No BankTransactionUpdateWebBean bean found, creating");
		beanMgr.registerBean(
				"com.ams.tradeportal.busobj.webbean.BankTransactionUpdateWebBean",
				"BankTransactionUpdate");
		btuWebBean = (BankTransactionUpdateWebBean) beanMgr
				.getBean("BankTransactionUpdate");
		getFromDB = true;
	}

	String bankTransUpdateOID = null;
	//Check for bank_transaction_update_oid in request
	if (null != request.getParameter("bank_trans_update_oid")) {
		bankTransUpdateOID = EncryptDecrypt
				.decryptStringUsingTripleDes(
						request.getParameter("bank_trans_update_oid"),
						userSession.getSecretKey());
	}

	/* 	If bank_transaction_update_oid is not found in the request, 
	 then it means returning from mediator.  Get it from doc Cache. */
	if (null == bankTransUpdateOID) {
		if (null != doc) {
			if (null != doc
					.getAttribute("/In/BankTransactionUpdate/bank_transaction_update_oid")) {
				bankTransUpdateOID = doc
						.getAttribute("/In/BankTransactionUpdate/bank_transaction_update_oid");
			}
		}
	}

	String transactionOID = null;
	//Check for transaction_oid in request
	if (null != request.getParameter("transaction_oid")) {
		transactionOID = EncryptDecrypt.decryptStringUsingTripleDes(
				request.getParameter("transaction_oid"),
				userSession.getSecretKey());
	}

	/* If transaction_oid is not found in the request, 
	then it means returning from mediator.  Get it from doc Cache. */
	if (null == transactionOID) {
		if (null != doc) {
			if (null != doc
					.getAttribute("/In/Transaction/transaction_oid")) {
				transactionOID = doc
						.getAttribute("/In/Transaction/transaction_oid");
			}
		}
	}
	Debug.debug("Bank Transaction Oid is " + bankTransUpdateOID);
	Debug.debug("transactionOID is " + transactionOID);
	
	//initialize  BankTransactionUpdate bean
	if(StringFunction.isNotBlank(bankTransUpdateOID)){
		btuWebBean.setAttribute("bank_transaction_update_oid",
				bankTransUpdateOID);
		btuWebBean.getDataFromAppServer();
		
	}
	

	if (null != transactionOID) {
		Debug.debug("Populating data for instrument/transaction ");

		transaction.setAttribute("transaction_oid", transactionOID);
		transaction.getDataFromAppServer();

		instrumentOid = transaction.getAttribute("instrument_oid");
		instrument.setAttribute("instrument_oid", instrumentOid);
		instrument.getDataFromAppServer();
	}

	transactionType = transaction.getAttribute("transaction_type_code");
	transactionStatus = transaction.getAttribute("transaction_status");

	instrumentType = instrument.getAttribute("instrument_type_code");
	instrumentStatus = instrument.getAttribute("instrument_status");
	String bankTranStatus = btuWebBean.getAttribute("bank_update_status");

	Debug.debug("Instrument Type " + instrumentType);
	Debug.debug("Instrument Status " + instrumentStatus);
	Debug.debug("Transaction Type " + transactionType);
	Debug.debug("Transaction Status " + transactionStatus);
	Debug.debug("Bank Transaction Status : "+bankTranStatus);	
	
	
	// Now we need to lock the instrument.
	    if (StringFunction.isNotBlank(bankTranStatus)) {
	       // The instrument is not a template, attempt to lock it.
	       if (InstrumentServices.isEditableBankTransStatus(bankTranStatus)) {
	          Debug.debug("Transaction is editable, attempting to lock...");
	          instrumentOid = instrument.getAttribute("instrument_oid");
	          long theOid = Long.valueOf(instrumentOid).longValue();
	          long userOid = Long.valueOf(userSession.getUserOid()).longValue();
	          try {
	             LockingManager.lockBusinessObject(theOid, userOid, false);
	             userSession.setInstrumentLock(true);
	             Debug.debug("Instrument sucessfully locked for this user.");
	          } catch (InstrumentLockException e) {
	             // instrument locked by someone else, issue an error that will show
	             // up on the transaction page
	             if (e.getUserOid() != userOid) {
	               Debug.debug("Instrument locked by another user: " + e.getLastName());
	               MediatorServices medService = new MediatorServices();
	               medService.getErrorManager().setLocaleName(resMgr.getResourceLocale());
	               medService.getErrorManager().issueError(
	                                      TradePortalConstants.ERR_CAT_1,
	                                      TradePortalConstants.INSTRUMENT_LOCKED,
	                                      e.getFirstName() + " " + e.getLastName() );
	               medService.addErrorInfo();
	               doc.setComponent("/Error", medService.getErrorDoc());
	               formMgr.storeInDocCache("default.doc", doc);
	             } else {
	               Debug.debug("Instrument is already locked by this user.");
	             }
	          } catch (AmsException e) {
	             System.out.println("Exception found trying to lock instrument "
	                                + instrumentOid);
	             System.out.println("   " + e.toString());
	          }
	       }
	    }

	String logicalPage = "";

	/* Assign the logical page based upon instrument/transaction type */
	if (instrumentType.equals(InstrumentType.IMPORT_DLC)
			|| instrumentType.equals(InstrumentType.STANDBY_LC)
			|| instrumentType.equals(InstrumentType.GUARANTEE)
			|| instrumentType.equals(InstrumentType.LOAN_RQST)
			|| instrumentType.equals(InstrumentType.SHIP_GUAR)
			|| instrumentType.equals(InstrumentType.AIR_WAYBILL)) {
		if (transactionType.equals(TransactionType.ISSUE)) {
			logicalPage = "BankTransStatusUpdateDetails-ISS";
		} else if (transactionType.equals(TransactionType.AMEND)) {
			logicalPage = "BankTransStatusUpdateDetails-AMD";
		} else if (transactionType.equals(TransactionType.RELEASE)) {
			logicalPage = "BankTransStatusUpdateDetails-REL";
		}

	}
	Debug.debug("Next Logical page is " + logicalPage);

	// Now, if the logical page is not blank, forward to the physical page for the
	// logical page.
	if (!logicalPage.equals("")) {
		String physicalPage = NavigationManager.getNavMan()
				.getPhysicalPage(logicalPage, request);
		Debug.debug("...and the physical page is " + physicalPage);
		String logicalPageAndInstrumentId = logicalPage;
		// Add context so that we will know which page the user is actually looking at
		PerformanceStatistic perfStat = (PerformanceStatistic) session
				.getAttribute("performanceStatistic");

		if ((perfStat != null) && (perfStat.isLinkAction()))
			perfStat.setContext(logicalPage);

		theSession.removeAttribute("fromTPHomePage");
		theSession.removeAttribute("startHomePage");
		theSession.removeAttribute("newTransaction");
		theSession.removeAttribute("isFromTransDetailPage");
		theSession.removeAttribute("TradePortalHome");
		userSession.addPage("BankTransactionUpdatesGrid");
		theSession.setAttribute("fromPage",
				"BankTransactionUpdatesGrid");
%>

<jsp:forward page='<%=physicalPage%>'>
	<jsp:param name="bankTransOid" value="<%=bankTransUpdateOID%>" />
	<jsp:param name="transOid" value="<%=transactionOID%>" />
</jsp:forward>
<%
	}
%>

</body>
</html>