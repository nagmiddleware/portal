<%--
 *
 *     Copyright  � 2008                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
**********************************************************************************
                        Cash Management Transaction History Tab

  Description:
    Contains HTML to create the History tab for the Cash Management Transaction page

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transactions-HistoryListView.jsp" %>
*******************************************************************************
--%> 

<%--cquinton 8/30/2012 add grid search header--%>
<div class="gridSearch">
  <div class="searchHeader">
    <div class="searchHeaderCriteria">

<%
   //StringBuffer statusOptions = new StringBuffer();           // NSX - PRUK080636392 - 08/16/10 
   StringBuffer statusExtraTags = new StringBuffer();

    // NSX - PRUK080636392 - 08/16/10 - Begin
   StringBuffer statusOptions = Dropdown.createActiveInActiveStatusOptions(resMgr,selectedStatus);
   



if ((SecurityAccess.hasRights(userSecurityRights, SecurityAccess.VIEW_CHILD_ORG_WORK)) 
           && (totalOrganizations > 1))
{
	dropdownOptions.append(Dropdown.getUnsortedOptions(hierarchyDoc, 
	            "ORGANIZATION_OID", "NAME", 
	            selectedOrg, userSession.getSecretKey()));
	dropdownOptions.append("<option value=\"");
          dropdownOptions.append(EncryptDecrypt.encryptStringUsingTripleDes(ALL_ORGANIZATIONS, userSession.getSecretKey()));
	dropdownOptions.append("\"");
	
	if (selectedOrg.equals(ALL_ORGANIZATIONS)) {
	dropdownOptions.append(" selected");
	}
	
	dropdownOptions.append(">");
	dropdownOptions.append(ALL_ORGANIZATIONS);
	dropdownOptions.append("</option>");
	
	extraTags = new StringBuffer("");
	extraTags.append("onchange=\"location='");
	extraTags.append(formMgr.getLinkAsUrl("goToPaymentTransactionsHome", response));
	extraTags.append("&amp;current2ndNav=");
	extraTags.append(current2ndNav);
	extraTags.append("&NewDropdownSearch=Y");
	extraTags.append("&historyOrg='+this.options[this.selectedIndex].value\"");
	      
	%>
      <div class="searchItem">
		<label for="Organization"><%=resMgr.getText("InstrumentHistory.Show", TradePortalConstants.TEXT_BUNDLE)%></label>
		<select name="Organization" id="Organization" data-dojo-type="dijit.form.FilteringSelect" 
		data-dojo-props="trim:true" onchange="local.paymentInquirySearch();">
			<%=dropdownOptions%>
	   	</select>  
      </div>
<%} else { %>
<%=widgetFactory.createTextField("Organization","",EncryptDecrypt.encryptStringUsingTripleDes(selectedOrg, userSession.getSecretKey()),"35",false,false,false," type=hidden ","", "")%>
	
<%} %>

      <div class="searchItem">
		<label for="instrStatusType"><%=resMgr.getText("InstrumentHistory.Status", TradePortalConstants.TEXT_BUNDLE)%></label>
		<select name="instrStatusType" id="instrStatusType" data-dojo-type="dijit.form.FilteringSelect" 
		data-dojo-props="trim:true" onchange="local.paymentInquirySearch();">
			<%=statusOptions%>
	   	</select>  
      </div>
    </div>
    <span class="searchHeaderActions">
	
<% if (SecurityAccess.hasRights(userSecurityRights,SecurityAccess.VIEW_INSTRUMENT_CREATE_OR_MODIFY)) { %>
		<button data-dojo-type="dijit.form.Button"  name="CopySelected" id="CopySelected" type="button">
		<%=resMgr.getText("CopySelected.Button",TradePortalConstants.TEXT_BUNDLE) %>
		<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
			openCopySelectedDialogHelper(copySelectedToInstrument);       					
		</script>
		</button>  
		<%=widgetFactory.createHoverHelp("CopySelected","common.copy") %>
<% }%>		

      <%--cquinton 10/8/2012 include gridShowCounts --%>
      <jsp:include page="/common/gridShowCount.jsp">
        <jsp:param name="gridId" value="paymentInquiriesID" />
      </jsp:include>

    </span>
    <div style="clear:both;"></div>
  </div>
  <div id="copySelectedInstrument"></div>
  
  <div class="searchDivider"></div>

<%
statusExtraTags.append("onchange=\"location='");
statusExtraTags.append(formMgr.getLinkAsUrl("goToPaymentTransactionsHome", response));
statusExtraTags.append("&amp;current2ndNav=");
statusExtraTags.append(current2ndNav);
statusExtraTags.append("&NewDropdownSearch=Y");
statusExtraTags.append("&amp;instrStatusType='+this.options[this.selectedIndex].value\"");
%>



    <%@ include file="/cashManagement/fragments/CashMgmt-TranSearch-AdvanceFilter.frag" %>

    <%@ include file="/cashManagement/fragments/CashMgmt-TranSearch-BasicFilter.frag" %>

</div>

<input type=hidden name=SearchType value=<%=searchType%>>

<%
  DataGridFactory dgFactory = new DataGridFactory(resMgr, userSession, formMgr, response);
  String gridHtml = dgFactory.createDataGrid("paymentInquiriesID","PaymentInquiriesDataGrid",null);
%>
  <%= gridHtml %>
