<%--
*******************************************************************************
  Payment Authorized Transactions

  Description: Main content for the payment authorized transactions data grid.

*******************************************************************************
--%>
<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>

<%--cquinton 8/30/2012 add grid search header--%>
<div class="gridSearch">
  <div class="searchHeader">
    <span class="searchHeaderCriteria">

<%
  // Display Organizations dropdown or the single organization.

  if ((SecurityAccess.hasRights(userSecurityRights,
                                SecurityAccess.VIEW_CHILD_ORG_WORK)) 
       && (totalOrganizations > 1)) {
	   orgListDropDown = true;
    dropdownOptions.append(Dropdown.getUnsortedOptions(hierarchyDoc, 
                                                       "ORGANIZATION_OID", "NAME", 
                                                       selectedOrg, userSession.getSecretKey()));
    dropdownOptions.append("<option value=\"");
    dropdownOptions.append(EncryptDecrypt.encryptStringUsingTripleDes(ALL_ORGANIZATIONS, userSession.getSecretKey()));
    dropdownOptions.append("\"");

    if (selectedOrg.equals(ALL_ORGANIZATIONS)) {
      dropdownOptions.append(" selected");
    }

    dropdownOptions.append(">");
    dropdownOptions.append(ALL_ORGANIZATIONS);
    dropdownOptions.append("</option>");
%>
      <%= widgetFactory.createSearchSelectField( "Organization", "CashMgmtAuthorizedTransactions.Show", "" , 
                    dropdownOptions.toString(), "onchange=\"searchAuthTrans();\"") %>
<%
  }
%>

    </span>
    <span class="searchHeaderActions">
      <%--cquinton 10/8/2012 include gridShowCounts --%>
      <jsp:include page="/common/gridShowCount.jsp">
        <jsp:param name="gridId" value="pmtAuthGrid" />
      </jsp:include>

      <span id="pmtAuthRefresh" class="searchHeaderRefresh"></span>
      <span id="pmtAuthGridEdit" class="searchHeaderEdit"></span>
	  <%=widgetFactory.createHoverHelp("pmtAuthRefresh","RefreshImageLinkHoverText") %> 
	  <%=widgetFactory.createHoverHelp("pmtAuthGridEdit","CustomizeListImageLinkHoverText") %>  
    </span>
    <div style="clear:both;"></div>
  </div>
</div>

 <%--ctq portal refresh add grid html start--%>

<%
  DGridFactory dgFactory = new DGridFactory(resMgr, userSession, formMgr, response);

  //TODO: build header fields including standard refresh and edit buttons
%>

<%
  //TODO: build the displayRights map for the footer items and evaluate within
  String gridHtml = dgFactory.createDataGrid("pmtAuthGrid","PaymentAuthorizedTransactionsDataGrid",null);
%>
<%= gridHtml %>
<%--ctq portal refresh add grid html end--%>
