<%--
*********************************************************************************************
                  Cash Management Transaction Search - Search Parms include file

  Description:
    This is a JSP meant to be included using the <%@ include file="x.jsp" %>
  tag.  It extracts search criteria from the request and builds a  where clause
  suitable for use in a listview (use InstrumentSearchListView as a model).
  As it is included with <%@ include %> rather than with the <jsp:include>
  directive, it is not a standalone servlet.  This JSP was created to allow
  a degree of reusability in multiple pages.

  This JSP assumes that searchType, dynamicWhere, searchListViewName and
  newSearchCriteria has previously been declared and assigned a
  value.

  NOTE: Since this JSP is copied into the including page at compile time of
  the including page, changes to this page do not automatically force the
  including pages to be recompiled.  If you make changes to this page, you must
  delete the including page's servlet in order to see the changes.

*********************************************************************************************
--%>
<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%

  // This boolean and MediatorServices provides error handling for invalid
  // search criteria fields.
  boolean errorsFound = false;
  MediatorServices medService = new MediatorServices();
  medService.getErrorManager().setLocaleName(resMgr.getResourceLocale());

  // Choose a character for the delimiter that will never appear in any data
  // The data must be base64ed first so that we can guarantee that the delimiter
  // will not appear
  String DELIMITER = "@";

  // determine if this is a redisplay of the listview coming from either
  // the a page link (page number or prev, next link) or the sort column
  // link
 
  
     // Determine the possible instrument type combinations to search on
     // and any additional search conditions based on where the previous
     // page's actions.  The conditions are as follows:
     // for Export DLC searches coming from Export DLC Transfer, only search
     // on instruments where the transaction type where the original transaction
     // is Advise
     // otherwise, only search on instrument types that are available
     // to the user, based on user's rights

     if (searchCondition.equals(TradePortalConstants.SEARCH_EXP_DLC_ACTIVE))
     {
        instrumentType = InstrumentType.EXPORT_DLC;
        dynamicWhere.append(" and i.instrument_type_code='");
        dynamicWhere.append(InstrumentType.EXPORT_DLC);
        dynamicWhere.append("' and i.instrument_oid in ");
        dynamicWhere.append("(select p_instrument_oid from transaction where ");
        dynamicWhere.append("p_instrument_oid = i.instrument_oid and ");
        dynamicWhere.append("transaction_type_code = '");
        dynamicWhere.append(TransactionType.ADVISE);
        dynamicWhere.append("')");
     }
     else if (!searchCondition.equals(TradePortalConstants.SEARCH_ALL_INSTRUMENTS))
     {
        // search on instruments that can be seen in the dropdown
        StringBuffer instrumentTypeSql = new StringBuffer();
        int vectorSize = instrumentTypes.size();

        for (int i = 0; i < vectorSize; i++)
        {
           instrumentTypeSql.append("'");
           instrumentTypeSql.append((String) instrumentTypes.elementAt(i));
           instrumentTypeSql.append("'");
         
           if (i < vectorSize - 1)
             instrumentTypeSql.append(", ");
        }
       
        dynamicWhere.append(" and i.instrument_type_code in (");
        dynamicWhere.append(instrumentTypeSql.toString());
        dynamicWhere.append(")");       
        instrumentType = "";

     }
     
     // if to create new transaction for an existing instrument, 
     // instrument must have a status of Active
     if (searchCondition.equals(TradePortalConstants.SEARCH_FOR_NEW_TRANSACTION))
     {
        dynamicWhere.append(" and i.instrument_status <> '");
        dynamicWhere.append(TradePortalConstants.INSTR_STATUS_PENDING);
        dynamicWhere.append("' and i.instrument_status <> '");
        dynamicWhere.append(TradePortalConstants.INSTR_STATUS_DELETED);
        dynamicWhere.append("' and i.instrument_status <> '");
        dynamicWhere.append(TradePortalConstants.INSTR_STATUS_CANCELLED);
        dynamicWhere.append("'");
     }

     if (searchCondition.equals(TradePortalConstants.IMPORTS_ONLY))
     {
        dynamicWhere.append(" and i.instrument_type_code in ('");
        dynamicWhere.append(InstrumentType.IMPORT_DLC);
        dynamicWhere.append("','");
        dynamicWhere.append(InstrumentType.STANDBY_LC);
        dynamicWhere.append("','");
        dynamicWhere.append(InstrumentType.IMPORT_COL);
        dynamicWhere.append("') ");
     }

     if (searchType.equals(TradePortalConstants.ADVANCED)) {

        instrumentType = request.getParameter("InstrumentType");

        if (instrumentType != null && !instrumentType.trim().equals("")) 
        {
           dynamicWhere.append(" and i.instrument_type_code='");
           dynamicWhere.append(instrumentType);
           dynamicWhere.append("'");
           newSearchCriteria.append("InstrumentType=" + EncryptDecrypt.stringToBase64String(instrumentType) + DELIMITER);
        }

        otherParty = request.getParameter("OtherParty");
        if (otherParty != null) {
           newSearchCriteria.append("OtherParty=" + EncryptDecrypt.stringToBase64String(otherParty.trim()) + DELIMITER);
           otherParty = otherParty.trim().toUpperCase();
           if (!otherParty.equals("")) {
              dynamicWhere.append(" and upper(p.name) like '");
              dynamicWhere.append(SQLParamFilter.filter(otherParty));
              dynamicWhere.append("%'");
           }
        } else {
           otherParty = "";
        }

        currency = request.getParameter("Currency");
        if (currency != null) {
           if (!currency.equals("")) {
              dynamicWhere.append(" and o.copy_of_currency_code='" + currency + "'");
           }
        } else {
           currency = "";
        }
        newSearchCriteria.append("Currency=" + EncryptDecrypt.stringToBase64String(currency) + DELIMITER);

        amountFrom = request.getParameter("AmountFrom");
        if (amountFrom != null) {
           amountFrom = amountFrom.trim();
           try {
              String amount =
                 NumberValidator.getNonInternationalizedValue(amountFrom,
                                                              loginLocale);
              if (!amount.equals("")) {
                 dynamicWhere.append(" and i.copy_of_instrument_amount >= ");
                 dynamicWhere.append(amount);
              }
           } catch (InvalidAttributeValueException e) {
              medService.getErrorManager().issueError(
                                 TradePortalConstants.ERR_CAT_1,
                                 TradePortalConstants.INVALID_CURRENCY_FORMAT,
                                 amountFrom);
              errorsFound = true;
           }
        } else {
          amountFrom = "";
        }
        newSearchCriteria.append("AmountFrom=" + EncryptDecrypt.stringToBase64String(amountFrom) + DELIMITER);

        amountTo = request.getParameter("AmountTo");
        if (amountTo != null) {
           amountTo = amountTo.trim();
           try {
              String amount =
                 NumberValidator.getNonInternationalizedValue(amountTo,
                                                              loginLocale);
              if (!amount.equals("")) {
                 dynamicWhere.append(" and i.copy_of_instrument_amount <= ");
                 dynamicWhere.append(amount);
              }
           } catch (InvalidAttributeValueException e) {
              medService.getErrorManager().issueError(
                                   TradePortalConstants.ERR_CAT_1,
                                   TradePortalConstants.INVALID_CURRENCY_FORMAT,
                                   amountTo);
              errorsFound = true;
           }
        } else {
           amountTo = "";
        }
        newSearchCriteria.append("AmountTo=" + EncryptDecrypt.stringToBase64String(amountTo) + DELIMITER);

        } else {

        // Unlike the ADVANCED search, the SIMPLE search values are ORed together.
        // To handle this we'll add an 'AND (simple conditions ored together)'
        // clause.  This is only added if at least one of the search fields has a
        // value.

        instrumentId = request.getParameter("InstrumentId");

        if (instrumentId != null) {
          instrumentId = instrumentId.trim().toUpperCase();
        } else {
          instrumentId = "";
        }

        newSearchCriteria.append("InstrumentId=" + EncryptDecrypt.stringToBase64String(instrumentId) + DELIMITER);

        refNum = request.getParameter("RefNum");
        if (refNum != null) {
           refNum = refNum.trim().toUpperCase();
        } else {
           refNum = "";
        }
        newSearchCriteria.append("RefNum=" + EncryptDecrypt.stringToBase64String(refNum) + DELIMITER);

        instrumentType = request.getParameter("InstrumentType");
        if (instrumentType != null) {
           instrumentType = instrumentType.trim();
        } else {
           instrumentType = "";
        }
        newSearchCriteria.append("InstrumentType=" + EncryptDecrypt.stringToBase64String(instrumentType));

        if ( !instrumentId.equals("") || !instrumentType.equals("") || !refNum.equals("") ) {   
          // At least one search field has a value.

          boolean addOr = false;
          dynamicWhere.append(" and (");

          if (!instrumentId.equals("")) {
             //dynamicWhere.append("upper(i.complete_instrument_id) like '%25"); // Wildcard "%" is URLEncoded to "%25" when it could be followed by alphabets or digits
             dynamicWhere.append("upper(i.complete_instrument_id) like '%"); // WebLogic 6.1 SP6 does not treat jsp:param as URLEncoded any more.
             dynamicWhere.append(SQLParamFilter.filter(instrumentId));
             dynamicWhere.append("%'"); //"%'" is safe for URLdecoding.
             addOr = true;
          }
          if (!refNum.equals("")) {
             if (addOr) dynamicWhere.append(" or ");
             dynamicWhere.append("upper(i.copy_of_ref_num) like '");
             dynamicWhere.append(SQLParamFilter.filter(refNum));
             dynamicWhere.append("%'");
             addOr = true;
          }       
          if (!instrumentType.equals("")) {
             if (addOr) dynamicWhere.append(" or ");
             // if this search originates from Export DLC transaction page,
             // ALWAYS filter based on instrument type Export LC that originate from
             // advise
             dynamicWhere.append("i.instrument_type_code='");
             dynamicWhere.append(instrumentType);
             dynamicWhere.append("'");
          }
         
          dynamicWhere.append(")");

       }  // end if-else (ADVANCED vs BASIC)
  }

  Debug.debug("The search criteria is " + newSearchCriteria.toString());
  Debug.debug("The search criteria is " + dynamicWhere);

  if (errorsFound) {
    // Any errors found while editing the date and amount fields need to be
    // placed in the doc cache.
    DocumentHandler errDoc = formMgr.getFromDocCache();
    medService.addErrorInfo();
    errDoc.setComponent("/Error", medService.getErrorDoc());
    formMgr.storeInDocCache("default.doc", errDoc);
  }
%>
