<%--
*******************************************************************************
  Cash Management Future value Footer

  Description: Everything that should be loaded at the end of the page
               should be included here.  This includes non-visible html 
               and javascript that should execute on page load.

*******************************************************************************
--%>
<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>

<%
  DataGridFactory dgFactory = new DataGridFactory(resMgr, userSession, formMgr, response);
  String gridLayout = dgFactory.createGridLayout("pmtFutureGrid", "CashMgmtFutureValueDataGrid");
  String encryptedOrgOid = EncryptDecrypt.encryptStringUsingTripleDes(selectedWorkflow, userSession.getSecretKey());
%>

<script type="text/javascript">

  var gridLayout = <%= gridLayout %>;
  var workflow="";
  var init='userOid=<%=userOid%>&userOrgOid=<%=userOrgOid%>&confInd=<%=confInd%>';
  var initSearchParms = "";

  <%--set the initial search parms--%>
  
 <%--  initParms(); --%>
  console.log(initSearchParms);
<%-- IR 16481 start  --%>
  <%--function initParms(){
 
 require(["dojo/dom"],
	      function(dom){
	 workflow=dom.byId('Workflow').value;
	 console.log("Inside initParms"+workflow);
	
	      }); 
  
 initSearchParms = init+"&workflow="+workflow;
 }--%>
 
var savedSort = savedSearchQueryData["sort"];
var selectedOrg = savedSearchQueryData["workflow"];

function initializeFutDataGrid(){
require(["dojo/dom", "dijit/registry", "t360/datagrid"], 
	      function(dom, registry, t360grid) {
		  if("" == selectedOrg || null == selectedOrg || "undefined" == selectedOrg){
			  selectedOrg= '<%=encryptedOrgOid%>';
		  }
		  else{
			  registry.byId("Workflow").set('value',selectedOrg);
		  }
		  if("" == savedSort || null == savedSort || "undefined" == savedSort)
			  savedSort = 'InstrumentID';
	    if("" == selectedOrg || null == selectedOrg){
	      selectedOrg = '<%=userOrgOid%>';
	    }
	    initSearchParms = init+"&workflow="+selectedOrg;
  <%--create the grid, attaching it to the div id specified in dataGrid.jsp--%>
  var viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("CashMgmtFutureValueDataView",userSession.getSecretKey())%>";  <%--Rel9.2 IR T36000032596 --%>
  var cashMgmtFutureValueId = 
    createDataGrid("pmtFutureGrid",viewName,
                    gridLayout, initSearchParms,savedSort);
}); 
}
	
<%-- moved below under ready()
require(["dojo/query", "dojo/on", "t360/datagrid", "dijit/registry","dojo/domReady!"],
      function(query, on, t360grid,  registry){					
	  
	  
	   var f =  registry.byId('CashMgmtFutureValue_Route');
	   if (f) f.set('disabled',true);
	   
	   f = registry.byId('CashMgmtFutureValue_Delete');
	   if (f) f.set('disabled',true);
					
	var cashMgmtFutureValueId = registry.byId('pmtFutureGrid');
	 on(cashMgmtFutureValueId,"selectionChanged", function() {
	   if (registry.byId('CashMgmtFutureValue_Route')){
	  t360grid.enableFooterItemOnSelectionGreaterThan(cashMgmtFutureValueId,"CashMgmtFutureValue_Route",0);
	  }
	  
	    if (registry.byId('CashMgmtFutureValue_Delete')){
	  t360grid.enableFooterItemOnSelectionGreaterThan(cashMgmtFutureValueId,"CashMgmtFutureValue_Delete",0);
	  }
      });				
});
		--%>			
		<%-- IR 16481 end 					 --%>
  function searchFutureTrans(){
		 
		 workflow=dijit.byId("Workflow").value;
		  
		  var searchParms = init+"&workflow="+workflow;
		  console.log('searchParms='+searchParms); 
	      searchDataGrid("pmtFutureGrid", "CashMgmtFutureValueDataView",
	                     searchParms);
	  }

  
	  function routePending(){
	  	openRouteDialog('routeItemDialog', routeItemDialogTitle, 
				'pmtFutureGrid', 
				'<%=TradePortalConstants.INDICATOR_YES%>', 
				'<%=TradePortalConstants.FROM_CM_FVD%>','') ;
	  }
	  
	  function deletePending(){
	 
		  var formName = '<%=formName%>';
		    var buttonName = '<%=TradePortalConstants.BUTTON_DELETE%>';
		    
		    <%--- Suresh_L  5/23/2014 Rel8.4  IR-T36000027671 -start  ---%>   
		    var   confirmMessage = "<%=StringFunction.asciiToUnicode(resMgr.getText("FutureValueTransactions.PopupMessage",
		    TradePortalConstants.TEXT_BUNDLE)) %>";
		   <%--- Suresh_L  5/23/2014 Rel8.4  IR-T36000027671 -end  ---%>   
		   
		    <%--get array of rowkeys from the grid--%>
		    var rowKeys = getSelectedGridRowKeys("pmtFutureGrid");
		    if(rowKeys == '' || rowKeys == "")
		    	return false;
		    
		    if (!confirm(confirmMessage)) 
			{
				return false;
			}else{
				<%--submit the form with rowkeys
		        becuase rowKeys is an array, the parameter name for
		        each will be checkbox + an iteration number -
		        i.e. checkbox0, checkbox1, checkbox2, etc --%>
		    submitFormWithParms(formName, buttonName, "checkbox", rowKeys);
			}
		    
}

  require(["dojo/query", "dojo/on", "t360/datagrid","dijit/registry", "t360/popup", "dojo/ready", "dojo/domReady!"],
      function(query, on, t360grid,registry, t360popup,ready){
	  ready(function(){
		  initializeFutDataGrid();
		  

		   var f =  registry.byId('CashMgmtFutureValue_Route');
		   if (f) f.set('disabled',true);
		   
		   f = registry.byId('CashMgmtFutureValue_Delete');
		   if (f) f.set('disabled',true);
						
		var cashMgmtFutureValueId = registry.byId('pmtFutureGrid');
		 on(cashMgmtFutureValueId,"selectionChanged", function() {
		   if (registry.byId('CashMgmtFutureValue_Route')){
		  t360grid.enableFooterItemOnSelectionGreaterThan(cashMgmtFutureValueId,"CashMgmtFutureValue_Route",0);
		  }
		  
		    if (registry.byId('CashMgmtFutureValue_Delete')){
		  t360grid.enableFooterItemOnSelectionGreaterThan(cashMgmtFutureValueId,"CashMgmtFutureValue_Delete",0);
		  }
	      });				
		  
    query('#pmtFutureRefresh').on("click", function() {
      searchFutureTrans();
    });
    query('#pmtFutureGridEdit').on("click", function() {
      var columns = t360grid.getColumnsForCustomization('pmtFutureGrid');
      var parmNames = [ "gridId", "gridName", "columns" ];
      var parmVals = [ "pmtFutureGrid", "CashMgmtFutureValueDataGrid", columns ];
      t360popup.open(
        'pmtFutureGridEdit', 'gridCustomizationPopup.jsp',
        parmNames, parmVals,
        null, null);  <%-- callbacks --%>
    });
	  });
  });


</script>

<%--cquinton 10/8/2012 include gridShowCounts --%>
<jsp:include page="/common/gridShowCountFooter.jsp">
  <jsp:param name="gridId" value="pmtFutureGrid" />
</jsp:include>
