<%--
*******************************************************************************
  Payment Authorized Transactions Footer

  Description: Everything that should be loaded at the end of the page
               should be included here.  This includes non-visible html 
               and javascript that should execute on page load.

*******************************************************************************
--%>
<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>

<%
  DGridFactory dgFactory = new DGridFactory(resMgr, userSession, formMgr, response);
  String gridLayout = dgFactory.createGridLayout("pmtAuthGrid", "PaymentAuthorizedTransactionsDataGrid");
%>

<script type="text/javascript">
require(["t360/OnDemandGrid", "dojo/dom-construct"], function(onDemandGrid, domConstruct){
  <%--get grid layout from the data grid factory--%>
  var gridLayout = <%= gridLayout %>;

  <%--set the initial search parms--%>
<% 
  //encrypt selectedOrgOid for the initial, just so it is the same on subsequent
  //search - note that the value in the dropdown is encrypted
  String encryptedOrgOid = EncryptDecrypt.encryptStringUsingTripleDes(selectedOrg, userSession.getSecretKey());
%>
<%-- IR 16481 start  --%>
var savedSort = savedSearchQueryData["sort"];
var selectedOrg = savedSearchQueryData["selectedOrgOid"];
function initializeAuthDataGrid(){
require(["dojo/dom", "dijit/registry", "t360/OnDemandGrid", "dojo/dom-construct","dojo/domReady!"], 
	      function(dom, registry, onDemandGrid, domConstruct) {
		  if("" == selectedOrg || null == selectedOrg || "undefined" == selectedOrg){
	   <%--  selectedOrg = checkString( registry.byId('Organization') );	 --%>
				selectedOrg= '<%=encryptedOrgOid%>';
		  }
		  else if(registry.byId("Organization")){
			  registry.byId("Organization").set('value',selectedOrg);
		  }
		  if("" == savedSort || null == savedSort || "undefined" == savedSort)
			  savedSort = '0';
	    if("" == selectedOrg || null == selectedOrg){
	      selectedOrg = '<%=userOrgOid%>';
	    }
  var initSearchParms = "selectedOrgOid="+selectedOrg+"&userOrgOid=<%=userOrgOid%>&cInd=<%=confInd%>";

  <%--create the grid, attaching it to the div id specified in dataGrid.jsp--%>
  var viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("PaymentAuthorizedTransactionsDataView",userSession.getSecretKey())%>";  <%--Rel9.2 IR T36000032596 --%>
  var pmtAuthGrid = onDemandGrid.createOnDemandGrid("pmtAuthGrid", viewName,gridLayout, initSearchParms,savedSort); 
});                   
});
}
<%-- IR 16481 end  --%>
  <%--provide a search function that collects the necessary search parameters--%>
  function searchAuthTrans() {
  
    require(["dojo/dom", "t360/OnDemandGrid", "dojo/dom-construct"],
      function(dom, onDemandGrid, domConstruct){
	  var orgOidValue = '';
	     if (<%=orgListDropDown%>)
				orgOidValue = dijit.byId('Organization').attr('value');
			else
				orgOidValue = "<%=encryptedOrgOid%>";
       
        var searchParms = "userOrgOid=<%=userOrgOid%>&selectedOrgOid=" + orgOidValue + "&cInd=<%=confInd%>"; 
        onDemandGrid.searchDataGrid("pmtAuthGrid", "PaymentAuthorizedTransactionsDataView",
                       searchParms);
      });
  }

  require(["dojo/query", "dojo/on", "t360/OnDemandGrid", "dojo/dom-construct", "t360/popup","dojo/ready","dojo/domReady!"],
      function(query, on, onDemandGrid, domConstruct, t360popup,ready){
	  ready(function(){
	  initializeAuthDataGrid();
    query('#pmtAuthRefresh').on("click", function() {
      searchAuthTrans();
    });
    query('#pmtAuthGridEdit').on("click", function() {
      var columns = onDemandGrid.getColumnsForCustomization('pmtAuthGrid');
      var parmNames = [ "gridId", "gridName", "columns" ];
      var parmVals = [ "pmtAuthGrid", "PaymentAuthorizedTransactionsDataGrid", columns ];
      t360popup.open(
        'pmtAuthGridEdit', 'gridCustomizationPopup.jsp',
        parmNames, parmVals,
        null, null);  <%-- callbacks --%>
    });
	  });
  });

</script>

<%--cquinton 10/8/2012 include gridShowCounts --%>
<jsp:include page="/common/dGridShowCountFooter.jsp">
  <jsp:param name="gridId" value="pmtAuthGrid" />
</jsp:include>
