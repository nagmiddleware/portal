<%--
*******************************************************************************
                    Cash Management Transaction Search Basic Filter include file

  Description:
    This is a JSP meant to be included using the <%@ include file="x.jsp" %> 
  tag.  It creates the HTML for displaying the Basic Filter fields for the  
  Cash Management Instrument Search.  As it is included with <%@ include %> rather than with
  the <jsp:include> directive, it is not a standalone servlet.  

  NOTE: Since this JSP is copied into the including page at compile time of
  the including page, changes to this page do not automatically force the
  including pages to be recompiled.  If you make changes to this page, you must
  delete the including page's servlet in order to see the changes.

*******************************************************************************
--%>

<%
  options = Dropdown.getInstrumentList(instrumentType, loginLocale, instrumentTypes );
%>

<input type="hidden" name="NewSearch" value="Y">
  
<div id="basicPmtFilter" class="searchDetail lastSearchDetail">

  <span class="searchCriteria">
	<%-- Kiran  05/07/2013  Rel8.2 IR-T36000014861  Start --%>
	<%-- changed event to window.event to all the fields in order for enter key to work in Firefox --%>
    <%=widgetFactory.createSearchTextField("InstrumentId","CashMgmtTransactionSearch.TransactionID","20", "class='char15' onKeydown='filterpaymentInquiryOnEnter(window.event, \"InstrumentId\", \"Basic\");'")%>
    <%=widgetFactory.createSearchSelectField("InstrumentType","CashMgmtTransactionSearch.TransactionType"," ", options, " class='char20'  onKeydown='filterpaymentInquiryOnEnter(window.event, \"InstrumentType\", \"Basic\");' ")%>
    <%=widgetFactory.createSearchTextField("RefNum","CashMgmtTransactionSearch.PrimaryRefNum","30", "class='char15' onKeydown='filterpaymentInquiryOnEnter(window.event, \"RefNum\", \"Basic\");'")%>
	<%-- Kiran  05/07/2013  Rel8.2 IR-T36000014861  End --%>
	
  </span> 

  <span class="searchActions">

    <button id="searchButtonBasicPmt" data-dojo-type="dijit.form.Button" type="button" class="SearchButton">
      <%= resMgr.getText("common.searchButton",TradePortalConstants.TEXT_BUNDLE) %>
      <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
        local.paymentInquiryBasicSearch();return false;
      </script>
    </button>
    <%=widgetFactory.createHoverHelp("searchButtonBasicPmt","SearchHoverText") %>
    <br>
    <span id="searchLinkAdvPMT" class="searchTypeToggle"><a href="javascript:local.shuffleFilterPmtSearch('Advance');"><%=resMgr.getText("ExistingDirectDebitSearch.Advanced", TradePortalConstants.TEXT_BUNDLE)%></a>	</span>
      <%=widgetFactory.createHoverHelp("searchLinkAdvPMT","common.AdvancedSearchHypertextLink") %>		
    </span>

  <div style="clear:both"></div>
</div>
