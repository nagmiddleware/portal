<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
**********************************************************************************
                        Cash Mgmt Transactions Home Future Value Tab

  Description:
    Contains HTML to create the Future Value tab for the Cash Mgmt Transactions Home page
    VS CR 609 11/23/10

*******************************************************************************
--%> 

<%--cquinton 8/30/2012 add grid search header--%>
<div class="gridSearch">
  <div class="searchHeader">
    <span class="searchHeaderCriteria">

<%
        StringBuffer prependText = new StringBuffer();
        prependText.append(resMgr.getText("FutureValueTransactions.WorkFor", 
                                           TradePortalConstants.TEXT_BUNDLE));
        prependText.append(" ");

        // If the user has rights to view child organization work, retrieve the 
        // list of dropdown options from the query listview results doc (containing
        // an option for each child org) otherwise, simply use the user's org 
        // option to the dropdown list (in addition to the default 'My Work' option).

        if (SecurityAccess.hasRights(userSecurityRights, 
                                     SecurityAccess.VIEW_CHILD_ORG_WORK))
        {
           dropdownOptions.append(Dropdown.getUnsortedOptions(hierarchyDoc, 
                                                               "ORGANIZATION_OID", "NAME", 
                                                               prependText.toString(),
                                                               selectedWorkflow, userSession.getSecretKey()));

           // Only display the 'All Work' option if the user's org has child organizations
           if (totalOrganizations > 1)
           {
              dropdownOptions.append("<option value=\"");
              dropdownOptions.append(EncryptDecrypt.encryptStringUsingTripleDes(ALL_WORK, userSession.getSecretKey()));
              dropdownOptions.append("\"");

              if (selectedWorkflow.equals(ALL_WORK))
              {
                 dropdownOptions.append(" selected");
              }

              dropdownOptions.append(">");
	      //IR T36000014928 ER Rel 8.1.0.4 BEGIN
              dropdownOptions.append(resMgr.getText(ALL_WORK, TradePortalConstants.TEXT_BUNDLE));
	      //IR T36000014928 ER Rel 8.1.0.4 END
              dropdownOptions.append("</option>");
           }
        }
        else
        {
           dropdownOptions.append("<option value=\"");
           dropdownOptions.append(EncryptDecrypt.encryptStringUsingTripleDes(userOrgOid, userSession.getSecretKey()));
           dropdownOptions.append("\"");

           if (selectedWorkflow.equals(userOrgOid))
           {
              dropdownOptions.append(" selected");
           }

           dropdownOptions.append(">");
           dropdownOptions.append(prependText.toString());
           dropdownOptions.append(userSession.getOrganizationName());
           dropdownOptions.append("</option>");
        }

        extraTags.append("onchange=\"location='");
        extraTags.append(formMgr.getLinkAsUrl("goToPaymentTransactionsHome", response));
        extraTags.append("&amp;current2ndNav=");
        extraTags.append(TradePortalConstants.NAV__FUTURE_TRANSACTIONS);
        extraTags.append("&amp;workflow='+this.options[this.selectedIndex].value\"");

        String defaultValue = "";
        String defaultText  = "";

        if(!userSession.hasSavedUserSession())
         {
           defaultValue = EncryptDecrypt.encryptStringUsingTripleDes(MY_WORK, userSession.getSecretKey());
           defaultText = MY_WORK;
	
	//IR T36000014928 ER Rel 8.1.0.4 BEGIN	   
	   StringBuffer sb = new StringBuffer();
	   sb.append("<option value=\"");
           sb.append(defaultValue);
           sb.append("\">");
           sb.append(resMgr.getText(defaultText, TradePortalConstants.TEXT_BUNDLE));
           sb.append("</option>");
		   dropdownOptions.insert(0,sb.toString());
	//IR T36000014928 ER Rel 8.1.0.4 END	   
         }
        out.print(widgetFactory.createSearchSelectField("Workflow", "PendingTransactions.Show", "", 
				dropdownOptions.toString(), "onChange='searchFutureTrans();'"));

%>
    </span>
    <span class="searchHeaderActions">
      <%--cquinton 10/8/2012 include gridShowCounts --%>
      <jsp:include page="/common/gridShowCount.jsp">
        <jsp:param name="gridId" value="pmtFutureGrid" />
      </jsp:include>

      <span id="pmtFutureRefresh" class="searchHeaderRefresh"></span>
      <span id="pmtFutureGridEdit" class="searchHeaderEdit"></span>
	  <%=widgetFactory.createHoverHelp("pmtFutureRefresh","RefreshImageLinkHoverText") %> 
	  <%=widgetFactory.createHoverHelp("pmtFutureGridEdit","CustomizeListImageLinkHoverText") %>  
    </span>
    <div style="clear:both;"></div>
  </div>
</div>
 
<%
  DataGridFactory dgFactory = new DataGridFactory(resMgr, userSession, formMgr, response);
  String gridHtml = dgFactory.createDataGrid("pmtFutureGrid","CashMgmtFutureValueDataGrid",null);
%>
  <%= gridHtml %>
