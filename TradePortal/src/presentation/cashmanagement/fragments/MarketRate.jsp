<%--/*
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
*********************************************************************************
  Description:
    Contains HTML to create the Pending tab for the Cash Mgmt Transactions Home page
*******************************************************************************
*/--%>   
<%@ page import="com.ams.tradeportal.common.*, com.ams.tradeportal.busobj.util.*,
         com.amsinc.ecsg.util.*" %>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session"></jsp:useBean>
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>

<%
boolean isListView = true;

String selectedInstrument = request.getParameter("selectedInstrument");

//Validation to check Cross Site Scripting
if(selectedInstrument != null){
	selectedInstrument = StringFunction.xssCharsToHtml(selectedInstrument);
}

String value = request.getParameter("showInfoText");
if ((value != null) && value.equals("false")) {
   isListView = false;
   //selectedInstrument = transactionOid +"/" +  instrumentOid +"/" transactionType;
   selectedInstrument=EncryptDecrypt.encryptStringUsingTripleDes(selectedInstrument, userSession.getSecretKey());
}

%>
  <style type="text/css">
     
   #inner {
        position:absolute;
           top:45%;
           left:35%;
           width:650px;
   }
   
   .disablingDiv
         {
             display: none;   /* Do not display it on entry */
             position: absolute;
             top: 0%;
             left: 0%;
             width: 100%;
             height: 100%;
             z-index:1000;
             background-color: white;
             opacity:.60;
             filter: alpha(opacity=60);
      }
  
   .popupCon
   {
    display: none;
   }
   
   .web_dialog_overlay
   {
      position: fixed;
      top: 0;
      right: 0;
      bottom: 0;
      left: 0;
      height: 100%;
      width: 100%;
      margin: 0;
      padding: 0;
      background: #000000;
      opacity: .15;
      filter: alpha(opacity=15);
      -moz-opacity: .15;
      z-index: 950;
      display: none;
   }

   .web_dialog
   {
       display: none;
      position: absolute;
      width: 420px;
      height: 300px;
      top: 45%;
      left: 50%;
      margin-left: -150px;
      margin-top: -100px;
      background-color: #ffffff;
      border: 1px solid #666;
      padding: 0px;
      z-index: 1007;
      font-family: Arial;
      font-size: 12px;
      opacity: 1.0;
      filter: alpha(opacity=100);
   }

   .web_dialog_title
   {
      border-bottom: solid 1px #369;
      background-color:silver;
      padding: 1px solid #666666;
      color: black;
      font-weight:bold;
       z-index: 1003;
   }

   .web_dialog_title a
   {
      color: black;
      text-decoration: none;
   }
   .align_right
   {
      text-align: right;
   }
 </style>
      
       
	   
   <script LANGUAGE="JavaScript">
       
       var xmlhttp;
	   var i=0;
       var timeout;
       
       function startCountDown(counter, delay, func) {
       
			<%--  make reference to div --%>
			var countDownObj = document.getElementById("countDown");
            if (countDownObj == null) return;
       
			countDownObj.count = function(counter) {
				countDownObj.innerHTML = counter;
				if (counter == 0) {
					func();  <%--  execute function --%>
					return;
				}
				timeout = setTimeout(function() {
					      countDownObj.count(counter - 1);}
					      , delay);
			};
       
			countDownObj.count(counter);
       }

       function getXmlHttpRequest()
       {
             var xmlHttpObj;
             if (window.XMLHttpRequest) {
               xmlHttpObj = new XMLHttpRequest();
             }
             else {
               try {
                 xmlHttpObj = new ActiveXObject("Msxml2.XMLHTTP");
               } catch (e) {
                 try {
                   xmlHttpObj = new ActiveXObject("Microsoft.XMLHTTP");
                 } catch (e) {
                   xmlHttpObj = false;
                 }
               }
             }
           return xmlHttpObj;
      }
  
      function changePopUp(layer) {
        document.getElementById("retrieving").style.display='none';
        document.getElementById("gotRate").style.display='none';
        document.getElementById("confirming").style.display='none';
        document.getElementById(layer).style.display='block';
        return true;
      }
	  
      function showPopUp(modal) {
           if (modal) {
               blockPage(true, false);
           }
           createDiv();

           document.getElementById('disablingDiv1').style.display='block';
           document.getElementById('dialog').style.display='block';
           changePopUp('retrieving');
           return true;
     }
	 
	 function blockPage(block, msg) {
	 	     var divTag = document.getElementById('disablingDiv');
	 	     if (block) {
	 	           if (divTag ==null) {
	 	               divTag = document.createElement("div");
	 	               divTag.id = "disablingDiv";
	 	               divTag.className = "disablingDiv";
	 	               document.body.appendChild(divTag);
	 	           }
	 	           
	 	           document.getElementById('disablingDiv').style.display='block';
	 	      } else {
	 	           document.getElementById('disablingDiv').style.display='none';
	 	           divTag.innerHTML = "";
	 	      }
	   }
	             
	   function Blink(layerName){
	  	  if(i%2==0) {
	  				eval("document.all"+'["'+layerName+'"]'+'.style.visibility="visible"');
  		  } else {
	   			eval("document.all"+'["'+layerName+'"]'+'.style.visibility="hidden"');
	  	  }
	  		 
	   		if(i<1) {
	  			i++;
	  		} else {
	  			i--;
	  		}
	   		setTimeout("Blink('"+layerName+"')",200);
	   }

     function dclose() {
         if (timeout != null) {
            clearTimeout(timeout);
         }
         document.getElementById('disablingDiv1').style.display='none';
		 document.getElementById('dialog').style.display='none';
	     blockPage(false);
         return true;
     }
              
     function createDiv()  {
		if (document.getElementById('disablingDiv1') ==null) {
	        var divTag = document.createElement("div");
	        divTag.id = "disablingDiv1";
	        divTag.style.zIndex = 1;
            divTag.appendChild(document.getElementById("dialog"));
            document.body.appendChild(divTag);
	     }
	 }
	 
     function rateExpired() {
		 var errResp = document.getElementById('timedOutResp');
		 updateErr(errResp.innerHTML);
		 dclose();
     }
	 
	
	 function getSelectedCount() {
	   <% if (isListView) {
	  %>
		var formNum = 0; 
		var counter = 0;
		var numElements = document.forms[formNum].elements.length;

		var i = 0;
		for (i = 0; i < numElements; i++) {
			if (document.forms[formNum].elements[i].type == 'checkbox') {
			    if (document.forms[formNum].elements[i].checked) {
				    counter++;
			    }
           } 
		}
		
		<% } else {%>
		  var counter =1;
		<%}%>
        return counter;
	 }
	 
	 
	 
	 
     function getSelected() {
	  <% if (isListView) {
	  %>
		var formNum = 0; 
		var numElements = document.forms[formNum].elements.length;

		<%-- var inputs = document.getElementsByTagName(�input�); --%>
		var i = 0;
		for (i = 0; i < numElements; i++) {
			if (document.forms[formNum].elements[i].type == 'checkbox') {
			    if (document.forms[formNum].elements[i].checked) {
				    return document.forms[formNum].elements[i].value;
			    }
           } 
		}
        return null;
		<% } else {%>
		
		return '<%=StringFunction.escapeQuotesforJS(selectedInstrument)%>';
		<%}%>
	 }
	 
     function makeRequest() {
	 <% if (isListView) {
	  %>
	      var totSelected = getSelectedCount();
		  if (totSelected ==0) {
		  
		  }
		  else if (totSelected > 1) {
		  
		  }
	 <%}%>
        if (!xmlhttp) xmlhttp = getXmlHttpRequest();
	    xmlhttp.open('GET', '/portal/transactions/RequestMarketRate.jsp?'  + 'selected='+getSelected()+ '&selectedCount='+getSelectedCount(), true);
	    xmlhttp.onreadystatechange = onGotRate;
        xmlhttp.send(null);
  	 }
       	    
	    function onGotRate(){
	        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			    var resp =  xmlhttp.responseText;
				var container = document.getElementById('container');
				container.innerHTML = resp;
			   	var errorOccured = document.getElementById('errorSectionResp');
				
				if (errorOccured == null) {
				    var gotRate = document.getElementById('gotRate');
				    gotRate.innerHTML = resp;
					container.innerHTML = "";
	                changePopUp("gotRate");
	                startCountDown(<%=TradePortalConstants.getPropertyValue("TradePortal", "FX_ONLINE_QUOTE_TIMEOUT", "20")%>,1000,rateExpired);
					
			     } else {
				   updateErr(errorOccured.innerHTML);
				   container.innerHTML = "";
				   dclose();
                 }				 
	        }    
        }

		function updateErr(update) {
		var errSec = document.getElementById('errorSection');
		if (errSec == null) return;
		if (update == "") update="<br>";
		errSec.innerHTML = update;
		}
		
		function makeRequestPost() {
        if (!xmlhttp) xmlhttp = getXmlHttpRequest();
	    xmlhttp.open('POST', '/portal/Dispatcher.jsp', true);
	    xmlhttp.onreadystatechange = onGotRate;
        xmlhttp.send(null);
  	    }
		
		function getRate() {
		   
		     updateErr("");
             showPopUp(true);
             makeRequest();
             return true;
        }
		
	 function fxConfirm() {
		 clearTimeout(timeout);
         changePopUp("confirming");
         
		 setButtonPressed('ConfirmFX', 0); 
		 var _form=document.forms[0];
		 _form.appendChild(createField("selected",getSelected()));
		 _form.submit();
     }
		
   function createField(name, value){
    var _field = document.createElement("input");
    _field.setAttribute("type","hidden");
    _field.setAttribute("name",name);

    if (value == null) value = "";
    _field.setAttribute("value",value);

    return _field;
    }
		
   </script>
        
   <table>
          <tr> 
		  <%
            if (isListView) {

           %>
           <td width ="40%">
		   <span class="ListHeaderText"> <%= resMgr.getText("MarketRate.getRateInfo",TradePortalConstants.TEXT_BUNDLE) %> </span>
           </td>
		   <% }
		   %>
           
            </tr>
            </table>
			
<div id="container" style="display: none">
</div> 			
<div id="dialog" class="web_dialog"> 
      <table style="width: 100%; border: 0px;" cellpadding="3" cellspacing="0">
      <tr>
         <td class="web_dialog_title"><span class="ListHeaderTextWhite"><%= resMgr.getText("MarketRate.dialogTitle",TradePortalConstants.TEXT_BUNDLE) %></span></td>
         <td class="web_dialog_title align_right">
            <a href="#" onclick="return dclose();" id="btnClose"><span class="ListHeaderTextWhite"><%= resMgr.getText("common.CloseText",TradePortalConstants.TEXT_BUNDLE) %></span></a>
         </td>
      </tr> 
      </table>
      
      <div id="retrieving" class="popupCon">
      <table style="width: 100%; border: 0px;" cellpadding="3" cellspacing="0">
         <tr height="100">
                     <td>&nbsp;</td>
            </tr>
            <tr>
			<td width="55">&nbsp;</td>       		<%-- MDB PKUL120231929 Rel 7.1 12/6/11 --%>
                     <td>
					 <span class="ListHeaderText"> <%= resMgr.getText("MarketRate.retrievingMsg",TradePortalConstants.TEXT_BUNDLE) %> </span> </td>
                     <td>&nbsp;</td>
            </tr>
            <tr>
                     <td>&nbsp;</td>
            </tr>
             </table>
      </div>
      
      <div id="gotRate" class="popupCon">
	  </div>
      
      <div id="confirming" class="popupCon">
      <table style="width: 100%; border: 0px;" cellpadding="3" cellspacing="0">
            <tr height="100">
	                         <td>&nbsp;</td>
	                </tr>

	                <tr>
					 <td width="40">&nbsp;</td>       		<%-- MDB PKUL120231929 Rel 7.1 12/6/11 --%>
					 
	                         <td>
							 <span class="ListHeaderText"> <%= resMgr.getText("MarketRate.confirmingMsg",TradePortalConstants.TEXT_BUNDLE) %> </span> </td>
	                         <td>&nbsp;</td>
	                </tr>
	                <tr>
	                         <td>&nbsp;</td>
            </tr>
             </table>
      </div>
      
      
   </div>