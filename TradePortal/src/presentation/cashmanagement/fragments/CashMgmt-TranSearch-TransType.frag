<%--
 *
 *     Copyright  � 2008                       
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%
	  
	  // Determine the instrument type code(s) for display
	  // and search criteria
	  //PPRAKASH IR NGUJ013070808 Add Begin
	  //instrumentTypes = Dropdown.getAllInstrumentTypes();
		Long organization_oid = new Long( userSession.getOwnerOrgOid() );
	  	Long userOID = new Long(userSession.getUserOid()); 
	  	instrumentTypes = Dropdown.getCashManagementInstrumentTypes( formMgr, 
						  loginRights, 
                                                  organization_oid.longValue(),
                                                  userSession.getSecurityType(),
                                                  userSession.getOwnershipLevel(),
                                                  userOID.longValue());
	 //PPRAKASH IR NGUJ013070808 Add End

%>