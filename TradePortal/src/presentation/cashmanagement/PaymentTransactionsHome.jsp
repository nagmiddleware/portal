<%--
*******************************************************************************
  Payment Transactions Home Page

  Description:  
     This page is used as the payment transactions main page.
   
*******************************************************************************
--%>

<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>


<%@ page import="java.util.*,com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,java.util.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*, com.ams.tradeportal.html.*, com.ams.tradeportal.busobj.*" %>

<jsp:useBean id="beanMgr"     class="com.amsinc.ecsg.web.BeanManager"            scope="session"></jsp:useBean>
<jsp:useBean id="formMgr"     class="com.amsinc.ecsg.web.FormManager"            scope="session"></jsp:useBean>
<jsp:useBean id="resMgr"      class="com.amsinc.ecsg.util.ResourceManager"       scope="session"></jsp:useBean>
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>
 

<%-- ********************* JavaScript for page begins here *********************  --%>



<%-- ************** Data retrieval page setup begins here ****************  --%>

<%
     
   StringBuffer      onLoad                = new StringBuffer();
   Hashtable         secureParms           = new Hashtable();
   String            helpSensitiveLink     = null;
   String            current2ndNav      = null;
   String            formName              = null;

   String            userOrgOid            = null;
   String            userOid               = null;
   String            userSecurityRights    = null;
   String            userSecurityType      = null;
   StringBuffer orgList = new StringBuffer();
   boolean orgListDropDown = false;  

   
   // Widget Factory  
   WidgetFactory widgetFactory = new WidgetFactory(resMgr);
   String refreshLink = formMgr.getLinkAsUrl("goToPaymentTransactionsHome", response);
   
   // Set the current primary navigation
   userSession.setCurrentPrimaryNavigation("NavigationBar.CashManagement");

   //cquinton 1/18/2013 set return page
   if ( "true".equals( request.getParameter("returning") ) ) {
     userSession.pageBack(); //to keep it correct
   }
   else {
     userSession.addPage("goToPaymentTransactionsHome");
   }
   
   //IR T36000026794 Rel9.0 07/24/2014 starts
   session.setAttribute("startHomePage", "PaymentTransactionsHome");
   session.removeAttribute("fromTPHomePage");
   //IR T36000026794 Rel9.0 07/24/2014 End
  
   Map searchQueryMap =  userSession.getSavedSearchQueryData();
   Map savedSearchQueryData =null;
  
   //cquinton 1/18/2013 remove old close action stuff

   // Retrieve the user's security rights, security type, organization oid, and user oid
   userSecurityRights = userSession.getSecurityRights();
   userSecurityType   = userSession.getSecurityType();
   userOrgOid         = StringFunction.xssCharsToHtml(userSession.getOwnerOrgOid());
   userOid            = StringFunction.xssCharsToHtml(userSession.getUserOid());
   
   //DK CR-886 Rel8.4 10/15/2013 starts
   ReferenceDataManager refData = ReferenceDataManager.getRefDataMgr();
   String loginLocale = userSession.getUserLocale();
   String datePattern = userSession.getDatePattern();
   String datePatternDisplay = refData.getDescr("DATEPATTERN", datePattern, loginLocale);
   String dateWidgetOptions = "placeHolder:'" + datePatternDisplay + "'";
   //DK CR-886 Rel8.4 10/15/2013 ends
   
   //CR-586 Vshah [BEGIN]
   UserWebBean thisUser = beanMgr.createBean(UserWebBean.class, "User");
   thisUser.setAttribute("user_oid", userOid);
   thisUser.getDataFromAppServer();
   
   String confInd = "";
   if (userSession.getSavedUserSession() == null)
       confInd = StringFunction.xssCharsToHtml(thisUser.getAttribute("confidential_indicator"));
   else
   {
       if (TradePortalConstants.ADMIN.equals(userSession.getSavedUserSession().getSecurityType()))
           confInd = TradePortalConstants.INDICATOR_YES;
       else
           confInd = StringFunction.xssCharsToHtml(thisUser.getAttribute("subsid_confidential_indicator"));  
   }
   if (InstrumentServices.isBlank(confInd))
      confInd = TradePortalConstants.INDICATOR_NO;
   //CR-586 Vshah [END]

   current2ndNav = request.getParameter("current2ndNav");
   if (current2ndNav == null) {
      current2ndNav = (String) session.getAttribute("paymentTransactions2ndNav");

      if (current2ndNav == null) {
         current2ndNav = TradePortalConstants.NAV__PENDING_TRANSACTIONS;
      }
   }

   // This is the query used for populating the workflow drop down list; it retrieves
   // all active child organizations that belong to the user's current org.
   
   
   // Now perform data setup for whichever is the current secondary navigation.  Each block of
   // code sets page specific parameters (for data retrieval or dropdowns) as
   // well as setting common things like the appropriate help link and formName


   // Set the performance statistic object to have the current page as its context
    PerformanceStatistic perfStat = (PerformanceStatistic) session.getAttribute("performanceStatistic");

   if((perfStat != null) && (perfStat.isLinkAction()))
           perfStat.setContext(current2ndNav);

   // ***********************************************************************
   // Data setup for the Cash Mgmt. Pending Transactions page
   // ***********************************************************************
   // Chandrakanth CR 451 12/01/2008 
   if (TradePortalConstants.NAV__PENDING_TRANSACTIONS.equals(current2ndNav)||
		   TradePortalConstants.NAV__FUTURE_TRANSACTIONS.equals(current2ndNav) ||
		   TradePortalConstants.NAV__AUTHORIZED_TRANSACTIONS.equals(current2ndNav) ||
		   TradePortalConstants.NAV__INQUIRIES.equals(current2ndNav))
   {
      formName = "CashMgmt-TransactionForm";
      if (TradePortalConstants.NAV__AUTHORIZED_TRANSACTIONS.equals(current2ndNav) )
      {
         helpSensitiveLink = OnlineHelp.createContextSensitiveLink("customer/auth_trans_cash_mgmt.htm",  resMgr, userSession);
      }
      else if (TradePortalConstants.NAV__INQUIRIES.equals(current2ndNav))
      {
         helpSensitiveLink = OnlineHelp.createContextSensitiveLink("customer/payment_trans_history.htm",  resMgr, userSession);
      }
      else if (TradePortalConstants.NAV__PENDING_TRANSACTIONS.equals(current2ndNav))
      {
         helpSensitiveLink = OnlineHelp.createContextSensitiveLink("customer/pend_trans.htm",  resMgr, userSession);
      }
      else if (TradePortalConstants.NAV__FUTURE_TRANSACTIONS.equals(current2ndNav)) //VS C 609 11/23/10
      {
               helpSensitiveLink = OnlineHelp.createContextSensitiveLink("customer/future_value_transactions.htm",  resMgr, userSession);
      }
   }
   
   Debug.debug("form " + formName);
   String searchNav = "payTranHome."+current2ndNav;
%>

<%-- ********************* HTML for page begins here *********************  --%>

<jsp:include page="/common/Header.jsp">
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="additionalOnLoad"         value="<%=onLoad.toString()%>" />
</jsp:include>

<%-- Vshah - 08/02/2012 - Rel8.1 - Portal-refresh --%>
<%--  including TransactionCleanup.jsp to remove lock on instrument which has been placed by this user... --%>
<%--  this is required because (with Portal-refresh) user can navigate anywhere from MenuBar when on Instrument(detail) page. --%>
<jsp:include page="/transactions/TransactionCleanup.jsp"></jsp:include>

<jsp:include page="/common/SearchCriteriaCleanup.jsp">
<jsp:param name="searchCriteriaSection" value="<%=searchNav%>"/>
</jsp:include>

<div class="pageMain">
  <div class="pageContent">

<%
//IR T36000014931 REL ER 8.1.0.4 BEGIN
String certAuthURL = ""; //needed for the frag 
boolean anyRequireAuth = false;
if (SecurityAccess.hasRights(userSecurityRights, SecurityAccess.INSTRUMENT_AUTHORIZE))
{
	        //cr498 setup openReauthenticationWindow.frag
            //cquinton 1/18/2011 Rel 6.1.0 ir#stul011761874 start
            //only include reauthentication stuff if there is some transaction type that requires it
            Cache reCertCache = (Cache)TPCacheManager.getInstance().getCache(TradePortalConstants.CLIENT_BANK_CACHE);
            DocumentHandler CBCResult = (DocumentHandler)reCertCache.get(userSession.getClientBankOid());	
            String requireTranAuth = CBCResult.getAttribute("/ResultSetRecord(0)/REQUIRE_TRAN_AUTH");
            anyRequireAuth = InstrumentAuthentication.requireTransactionAuthentication(
                requireTranAuth, InstrumentAuthentication.ANY_TRAN_AUTH);
            if (anyRequireAuth) { 
                
%>
              <%@ include file="/logon/fragments/openReauthenticationWindow.frag" %>
         
		   <%
		   } 
}
//IR T36000014931 REL ER 8.1.0.4 END
		   %>
        
<div class="secondaryNav">

  <div class="secondaryNavTitle">
    <%=resMgr.getText("SecondaryNavigation.Payments",  TradePortalConstants.TEXT_BUNDLE)%>
  </div>

  <%
    String pendingSelected    = TradePortalConstants.INDICATOR_NO;
    String futureSelected     = TradePortalConstants.INDICATOR_NO;
    String authorizedSelected = TradePortalConstants.INDICATOR_NO;
    String inquiriesSelected  = TradePortalConstants.INDICATOR_NO;
    if (TradePortalConstants.NAV__PENDING_TRANSACTIONS.equals(current2ndNav))
    {
      pendingSelected = TradePortalConstants.INDICATOR_YES;
    }
    else if (TradePortalConstants.NAV__FUTURE_TRANSACTIONS.equals(current2ndNav))
    {
      futureSelected = TradePortalConstants.INDICATOR_YES;
    }
    else if (TradePortalConstants.NAV__AUTHORIZED_TRANSACTIONS.equals(current2ndNav))
    {
      authorizedSelected = TradePortalConstants.INDICATOR_YES;
    }
    else
    {
      inquiriesSelected = TradePortalConstants.INDICATOR_YES;
    }

    String navLinkParms = "current2ndNav=" + TradePortalConstants.NAV__PENDING_TRANSACTIONS;
  %>
  <jsp:include page="/common/NavigationLink.jsp">
    <jsp:param name="linkTextKey"  value="SecondaryNavigation.Payments.PendingTransactions" />
    <jsp:param name="linkSelected" value="<%= pendingSelected %>" />
    <jsp:param name="action"       value="goToPaymentTransactionsHome" />
    <jsp:param name="linkParms"    value="<%= navLinkParms %>" />
	<jsp:param name="hoverText"    value="PaymentTransaction.pendingList" />
  </jsp:include>
  <%
    navLinkParms = "current2ndNav=" + TradePortalConstants.NAV__FUTURE_TRANSACTIONS;
  %>
  <jsp:include page="/common/NavigationLink.jsp">
    <jsp:param name="linkTextKey"  value="SecondaryNavigation.Payments.FutureTransactions" />
    <jsp:param name="linkSelected" value="<%= futureSelected %>" />
    <jsp:param name="action"       value="goToPaymentTransactionsHome" />
    <jsp:param name="linkParms"    value="<%= navLinkParms %>" />
	<jsp:param name="hoverText"    value="PaymentTransaction.futureValueList" />
  </jsp:include>
  <%
    navLinkParms = "current2ndNav=" + TradePortalConstants.NAV__AUTHORIZED_TRANSACTIONS;
  %>
  <jsp:include page="/common/NavigationLink.jsp">
    <jsp:param name="linkTextKey"  value="SecondaryNavigation.Payments.AuthorizedTransactions" />
    <jsp:param name="linkSelected" value="<%= authorizedSelected %>" />
    <jsp:param name="action"       value="goToPaymentTransactionsHome" />
    <jsp:param name="linkParms"    value="<%= navLinkParms %>" />
	<jsp:param name="hoverText"    value="PaymentTransaction.authorizedList" />	
  </jsp:include>
  <%
    navLinkParms = "current2ndNav=" + TradePortalConstants.NAV__INQUIRIES;
  %>
  <jsp:include page="/common/NavigationLink.jsp">
    <jsp:param name="linkTextKey"  value="SecondaryNavigation.Payments.Inquiries" />
    <jsp:param name="linkSelected" value="<%= inquiriesSelected %>" />
    <jsp:param name="action"       value="goToPaymentTransactionsHome" />
    <jsp:param name="linkParms"    value="<%= navLinkParms %>" />
	<jsp:param name="hoverText"    value="PaymentTransaction.inquiryList" />	
  </jsp:include>

  <span id="secondaryNavHelp" class="secondaryNavHelp">
    <%=helpSensitiveLink%>
  </span>
  <%=widgetFactory.createHoverHelp("secondaryNavHelp","HelpImageLinkHoverText") %> 
  <div style="clear:both;"></div>

</div>

<%  //include a hidden form for new instrument submission
//this is used for all of the new instrument types available from the menu
Hashtable newInstrSecParms = new Hashtable();
newInstrSecParms.put("UserOid", userSession.getUserOid());
newInstrSecParms.put("SecurityRights", userSession.getSecurityRights());
newInstrSecParms.put("clientBankOid", userSession.getClientBankOid());
newInstrSecParms.put("ownerOrg", userSession.getOwnerOrgOid());
%>

<form name="<%=formName%>" method="POST"  action="<%= formMgr.getSubmitAction(response) %>">
  <input type=hidden value="" name=buttonName>

<% //cr498 begin
  if (anyRequireAuth) {
%> 

  <input type=hidden name="reCertification" value="<%=TradePortalConstants.INDICATOR_NO%>">
  <input type=hidden name="reCertOK">
  <input type=hidden name="logonResponse">
  <input type=hidden name="logonCertificate">

<%
  } //cr498 end
%>
  <jsp:include page="/common/ErrorSection.jsp" />

  <div class="formContentNoSidebar">

<%
  // Based on the current selected, display the appropriate HTML 

//ctq - todo - this should be in the navigation to the page, not here!!!!  
//   if (TradePortalConstants.NAV__PENDING_TRANSACTIONS.equals(current2ndNav) ||
//         	   TradePortalConstants.NAV__FUTURE_TRANSACTIONS.equals(current2ndNav) ||
//		   TradePortalConstants.NAV__AUTHORIZED_TRANSACTIONS.equals(current2ndNav) ||
//		   TradePortalConstants.NAV__INQUIRIES.equals(current2ndNav)) {
%>

<%-- ************** Data retrieval page setup begins here ****************  --%>

<%
   final String      ALL_ORGANIZATIONS            = resMgr.getText("AuthorizedTransactions.AllOrganizations", TradePortalConstants.TEXT_BUNDLE);
   //final String      ALL_WORK                     = resMgr.getText("PendingTransactions.AllWork",  TradePortalConstants.TEXT_BUNDLE);
   //final String      MY_WORK                      = resMgr.getText("PendingTransactions.MyWork",  TradePortalConstants.TEXT_BUNDLE);
   final String ALL_WORK = "common.allWork";
  final String MY_WORK = "common.myWork";
  

    //Vshah - 12/08/2008 - Cash Management History Page - Begin

   String			 instrumentId 		   = "";
   String			 refNum			   = "";
   String			 amountFrom     	   = "";
   String			 amountTo                  = "";
   String 			 currency                  = "";
   String			 searchCondition 	   = TradePortalConstants.SEARCH_ALL_INSTRUMENTS; 
   String			 searchType		   = null;   
   String 			 instrumentType		   = null;
   Vector            		 instrumentTypes           = null;
   String 			 options                   = "";
   String 			 otherParty                = "";
   String 			 link 			   = null;
   String 			 linkText 		   = null;   
   StringBuffer 		 newSearchCriteria         = new StringBuffer();                                                          
   //Vshah - 12/08/2008 - Cash Management History Page - End

   DocumentHandler   hierarchyDoc                 = null;
   StringBuffer      dynamicWhereClause           = new StringBuffer();
   StringBuffer      dropdownOptions              = new StringBuffer();
   int               totalOrganizations           = 0;
   StringBuffer      extraTags                    = new StringBuffer();
   StringBuffer      newLink                      = new StringBuffer();
   StringBuffer      newUploadLink                = new StringBuffer();
         
   String            userDefaultWipView           = null; 
   String            selectedWorkflow             = null;
   String            selectedStatus               = "";
   
   String linkParms   = null;   
   StringBuffer dynamicWhere = dynamicWhereClause;
   String            selectedOrg                  = null;
   String loginRights = userSecurityRights; 

   userDefaultWipView           = userSession.getDefaultWipView();
   
   String sqlQuery = "select organization_oid, name from corporate_org where activation_status =? start with organization_oid = ? "
	+" connect by prior organization_oid = p_parent_corp_org_oid  order by "+resMgr.localizeOrderBy("name");

   hierarchyDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery.toString(), false, new Object[]{TradePortalConstants.ACTIVE,userOrgOid});
	  
   Vector orgListDoc = hierarchyDoc.getFragments("/ResultSetRecord");
   totalOrganizations = orgListDoc.size();
   
   // Now perform data setup for whichever is the current secondary navigation.  Each block of
   // code sets specific parameters (for data retrieval or dropdowns) as
   // well as setting common things like the appropriate help link and formName


   // Set the performance statistic object to have the current secondary navigation as its context
    

   if((perfStat != null) && (perfStat.isLinkAction()))
           perfStat.setContext(current2ndNav);

   // ***********************************************************************
   // Data setup for the Pending Transactions
   // ***********************************************************************
   if (TradePortalConstants.NAV__PENDING_TRANSACTIONS.equals(current2ndNav))
   {
      formName     = "CashMgmt-TransactionForm";

      // Based on the statusType dropdown, build the where clause to include one or more
      // statuses.
      Vector statuses = new Vector();
      int numStatuses = 0;

      selectedStatus = request.getParameter("transStatusType");

      if (selectedStatus == null) 
      {
         selectedStatus = (String) session.getAttribute("transStatusType");
      }

      if (TradePortalConstants.STATUS_READY.equals(selectedStatus) )
      {
         statuses.addElement(TradePortalConstants.TRANS_STATUS_READY_TO_AUTHORIZE);
         statuses.addElement(TradePortalConstants.TRANS_STATUS_AUTHORIZE_PENDING);
      }
      else if (TradePortalConstants.STATUS_PARTIAL.equals(selectedStatus) )
      {
         statuses.addElement(TradePortalConstants.TRANS_STATUS_PARTIALLY_AUTHORIZED);
      }
      else if (TradePortalConstants.STATUS_AUTH_FAILED.equals(selectedStatus) )
      {
         statuses.addElement(TradePortalConstants.TRANS_STATUS_AUTHORIZE_FAILED);
      }
      else if (TradePortalConstants.STATUS_STARTED.equals(selectedStatus) )
      {
         statuses.addElement(TradePortalConstants.TRANS_STATUS_STARTED);
      }
      else if (TradePortalConstants.STATUS_REJECTED_BY_BANK.equals(selectedStatus) )
      {
         statuses.addElement(TradePortalConstants.TRANS_STATUS_REJECTED_BY_BANK);
      }    
      else if (TradePortalConstants.TRANS_STATUS_VERIFIED.equals(selectedStatus) )
      {
         statuses.addElement(TradePortalConstants.TRANS_STATUS_VERIFIED);
      }    
      else if (TradePortalConstants.TRANS_STATUS_VERIFIED_PENDING_FX.equals(selectedStatus) )
      {
         statuses.addElement(TradePortalConstants.TRANS_STATUS_VERIFIED_PENDING_FX);
      }    
      else if (TradePortalConstants.TRANS_STATUS_FX_THRESH_EXCEEDED.equals(selectedStatus) )
      {
         statuses.addElement(TradePortalConstants.TRANS_STATUS_FX_THRESH_EXCEEDED);
      } 
      else if (TradePortalConstants.TRANS_STATUS_AUTH_PEND_MARKET_RATE.equals(selectedStatus) )
      {
         statuses.addElement(TradePortalConstants.TRANS_STATUS_AUTH_PEND_MARKET_RATE);
      }  
    //Sandeep Rel-8.3 CR-821 Dev 05/28/2013 - Start
      else if (TradePortalConstants.STATUS_READY_TO_CHECK.equals(selectedStatus) )
      {
         statuses.addElement(TradePortalConstants.TRANS_STATUS_READY_TO_CHECK);
      } 
      else if (TradePortalConstants.STATUS_REPAIR.equals(selectedStatus) )
      {
         statuses.addElement(TradePortalConstants.TRANS_STATUS_REPAIR);
      } 
    //Sandeep Rel-8.3 CR-821 Dev 05/28/2013 - Start
      else // default is ALL
      {
         statuses.addElement(TradePortalConstants.TRANS_STATUS_STARTED);
         statuses.addElement(TradePortalConstants.TRANS_STATUS_AUTHORIZE_FAILED);
         statuses.addElement(TradePortalConstants.TRANS_STATUS_AUTHORIZE_PENDING);
         statuses.addElement(TradePortalConstants.TRANS_STATUS_PARTIALLY_AUTHORIZED);
         statuses.addElement(TradePortalConstants.TRANS_STATUS_READY_TO_AUTHORIZE);
         statuses.addElement(TradePortalConstants.TRANS_STATUS_REQ_AUTHENTICTN);   //IAZ CM-451 01/22/09         
         statuses.addElement(TradePortalConstants.TRANS_STATUS_REJECTED_BY_BANK);         
         statuses.addElement(TradePortalConstants.TRANS_STATUS_VERIFIED);         
         statuses.addElement(TradePortalConstants.TRANS_STATUS_VERIFIED_PENDING_FX);         
		 statuses.addElement(TradePortalConstants.TRANS_STATUS_FX_THRESH_EXCEEDED); 
		 statuses.addElement(TradePortalConstants.TRANS_STATUS_AUTH_PEND_MARKET_RATE);
		//Sandeep Rel-8.3 CR-821 Dev 05/28/2013 - Start
		 statuses.addElement(TradePortalConstants.TRANS_STATUS_READY_TO_CHECK); 
		 statuses.addElement(TradePortalConstants.TRANS_STATUS_REPAIR);
		//Sandeep Rel-8.3 CR-821 Dev 05/28/2013 - End
         selectedStatus = TradePortalConstants.STATUS_ALL;
      }

      session.setAttribute("transStatusType", selectedStatus);

      // Build a where clause using the vector of statuses we just set up.

      dynamicWhereClause.append(" and a.transaction_status in (");

      numStatuses = statuses.size();

      for (int i=0; i<numStatuses; i++) {
          dynamicWhereClause.append("'");
          dynamicWhereClause.append( (String) statuses.elementAt(i) );
          dynamicWhereClause.append("'");

          if (i < numStatuses - 1) {
            dynamicWhereClause.append(", ");
          }
      }

      dynamicWhereClause.append(") ");


      // Based on the workflow dropdown, build the where clause to include transactions at
      // various levels of the organization.
      selectedWorkflow = request.getParameter("Workflow");

      if (selectedWorkflow == null)
      {
         if (userDefaultWipView.equals(TradePortalConstants.WIP_VIEW_MY_ORG))
         {
            selectedWorkflow = userOrgOid;
         }
         else if (userDefaultWipView.equals(TradePortalConstants.WIP_VIEW_MY_ORG_CHILDREN) && totalOrganizations > 1)
         {
            selectedWorkflow = ALL_WORK;
         }
         else
         {
            selectedWorkflow = MY_WORK;
         }
         selectedWorkflow = EncryptDecrypt.encryptStringUsingTripleDes(selectedWorkflow, userSession.getSecretKey());
      }

      session.setAttribute("workflow", selectedWorkflow);

      selectedWorkflow = EncryptDecrypt.decryptStringUsingTripleDes(selectedWorkflow, userSession.getSecretKey());

      if (selectedWorkflow.equals(MY_WORK))
      {
         dynamicWhereClause.append("and a.a_assigned_to_user_oid = ");
         dynamicWhereClause.append(userOid);
      }
      else if (selectedWorkflow.equals(ALL_WORK))
      {
         dynamicWhereClause.append("and b.a_corp_org_oid in (");
         dynamicWhereClause.append(" select organization_oid");
         dynamicWhereClause.append(" from corporate_org");
         dynamicWhereClause.append(" where activation_status = '");
         dynamicWhereClause.append(TradePortalConstants.ACTIVE);
         dynamicWhereClause.append("' start with organization_oid = ");
         dynamicWhereClause.append(userOrgOid);
         dynamicWhereClause.append(" connect by prior organization_oid = p_parent_corp_org_oid)");
      }
      else
      {
         dynamicWhereClause.append("and b.a_corp_org_oid = ");
         dynamicWhereClause.append(selectedWorkflow);
      }
      
      //CR-586 Vshah [BEGIN]
      //For Users where the new User Profile level "Access to Confidential Payment Instruments/Templates" Indicator is not selected (set to No), 
      //on the Cash Management/Payment Transactions listviews, no transactions will appear that are designated as Confidential Payments
      if (TradePortalConstants.INDICATOR_NO.equals(confInd))
      {
       	 dynamicWhereClause.append(" and ( t.confidential_indicator = 'N' OR ");
       	 dynamicWhereClause.append(" t.confidential_indicator is null) ");
      }
      //CR-586 Vshah [END]

      helpSensitiveLink = OnlineHelp.createContextSensitiveLink("customer/pend_trans.htm",  resMgr, userSession);

      onLoad.append("document.TransactionListForm.Workflow.focus();");
%>

      <%-- **************** JavaScript for Pending Transactions only *********************  --%>

      <script language="JavaScript">


         <%--
         // This function is used to display a popup message confirming
         // that the user wishes to delete the selected items in the
         // listview.
         --%>
         function confirmDelete()
         {
           var   confirmMessage = "<%=StringFunction.asciiToUnicode(resMgr.getText("PendingTransactions.PopupMessage", TradePortalConstants.TEXT_BUNDLE)) %>";

            if (!confirm(confirmMessage)) <%--  Modified by LOGANATHAN - 10/10/2005 - IR LNUF092749769 --%>
             {
                formSubmitted = false;
                return false;
             }
           else
           {
              return true;
           }
        }

     

      </script>

<%
   } // End data setup for Pending Transactions
   
   // ****************************************************************************
   // Data setup for Future Value Date Transactions
   // *********************************************************************************
   else if (TradePortalConstants.NAV__FUTURE_TRANSACTIONS.equals(current2ndNav))
   {
      formName     = "CashMgmt-TransactionForm";

      // Based on the statusType dropdown, build the where clause to include one or more
      // statuses.
      Vector statuses = new Vector();
      int numStatuses = 0;


      // Based on the workflow dropdown, build the where clause to include transactions at
      // various levels of the organization.
      selectedWorkflow = request.getParameter("workflow");

      if (selectedWorkflow == null)
      {
         selectedWorkflow = (String) session.getAttribute("workflow");

         if (selectedWorkflow == null)
         {
            if (userDefaultWipView.equals(TradePortalConstants.WIP_VIEW_MY_ORG))
            {
               selectedWorkflow = userOrgOid;
            }
            else if (userDefaultWipView.equals(TradePortalConstants.WIP_VIEW_MY_ORG_CHILDREN))
            {
               selectedWorkflow = ALL_WORK;
            }
            else
            {
               selectedWorkflow = MY_WORK;
            }  
            selectedWorkflow = EncryptDecrypt.encryptStringUsingTripleDes(selectedWorkflow, userSession.getSecretKey());
         }
      }

      session.setAttribute("workflow", selectedWorkflow);

      selectedWorkflow = EncryptDecrypt.decryptStringUsingTripleDes(selectedWorkflow, userSession.getSecretKey());

      if (selectedWorkflow.equals(MY_WORK))
      {
         dynamicWhereClause.append("and a.a_assigned_to_user_oid = ");
         dynamicWhereClause.append(userOid);
      }
      else if (selectedWorkflow.equals(ALL_WORK))
      {
         dynamicWhereClause.append("and b.a_corp_org_oid in (");
         dynamicWhereClause.append(" select organization_oid");
         dynamicWhereClause.append(" from corporate_org");
         dynamicWhereClause.append(" where activation_status = '");
         dynamicWhereClause.append(TradePortalConstants.ACTIVE);
         dynamicWhereClause.append("' start with organization_oid = ");
         dynamicWhereClause.append(userOrgOid);
         dynamicWhereClause.append(" connect by prior organization_oid = p_parent_corp_org_oid)");
      }
      else
      {
         dynamicWhereClause.append("and b.a_corp_org_oid = ");
         dynamicWhereClause.append(selectedWorkflow);
      }
      
      //CR-586 Vshah [BEGIN]
      //For Users where the new User Profile level "Access to Confidential Payment Instruments/Templates" Indicator is not selected (set to No), 
      //on the Cash Management/Payment Transactions listviews, no transactions will appear that are designated as Confidential Payments
      if (TradePortalConstants.INDICATOR_NO.equals(confInd))
      {
       	 dynamicWhereClause.append(" and ( t.confidential_indicator = 'N' OR ");
       	 dynamicWhereClause.append(" t.confidential_indicator is null) ");
      }
      //CR-586 Vshah [END]

      helpSensitiveLink = OnlineHelp.createContextSensitiveLink("customer/futureValueTransaction.htm",  resMgr, userSession);

      onLoad.append("document.TransactionListForm.Workflow.focus();");
%>

      <%-- **************** JavaScript for Future Transactions only *********************  --%>

      <script language="JavaScript">


         <%--
         // This function is used to display a popup message confirming
         // that the user wishes to delete the selected items in the
         // listview.
         --%>
         function confirmDelete()
         {
           var   confirmMessage = "<%=resMgr.getText("FutureValueTransactions.PopupMessage", TradePortalConstants.TEXT_BUNDLE) %>";

            if (!confirm(confirmMessage)) <%--  Modified by LOGANATHAN - 10/10/2005 - IR LNUF092749769 --%>
             {
                formSubmitted = false;
                return false;
             }
           else
           {
              return true;
           }
        }

      

      </script>

<%
   } // End data setup for Future Value Transactions
   //VS CR 609 11/23/10 End
   
   else if (TradePortalConstants.NAV__AUTHORIZED_TRANSACTIONS.equals(current2ndNav))
   {
      formName = "CashMgmt-TransactionForm";
      helpSensitiveLink = OnlineHelp.createContextSensitiveLink("customer/cashManagement.htm",  resMgr, userSession);
      // The organization dropdown displays dynamically based on security and the
      // number of orgs.  If it will display, set focus to that field.
      if ((SecurityAccess.hasRights(userSecurityRights, SecurityAccess.VIEW_CHILD_ORG_WORK)) 
           && (totalOrganizations > 1)) {
        onLoad.append("document.TransactionListForm.Organization.focus();");
      }

      selectedOrg = request.getParameter("org");

      if (selectedOrg == null)
      {
         selectedOrg = (String) session.getAttribute("org");

         if (selectedOrg == null)
         {
            selectedOrg = EncryptDecrypt.encryptStringUsingTripleDes(userOrgOid, userSession.getSecretKey());
         }
      }

      session.setAttribute("org", selectedOrg);

      selectedOrg = EncryptDecrypt.decryptStringUsingTripleDes(selectedOrg, userSession.getSecretKey());

      //ctq portal refresh
      //this is done in the dataview
      //todo: comment out when ready to remove listview!!!
      if (selectedOrg.equals(ALL_ORGANIZATIONS))
      {
         dynamicWhereClause.append("and b.a_corp_org_oid in (");
         dynamicWhereClause.append(" select organization_oid");
         dynamicWhereClause.append(" from corporate_org");
         dynamicWhereClause.append(" where activation_status = '");
         dynamicWhereClause.append(TradePortalConstants.ACTIVE);
         dynamicWhereClause.append("' start with organization_oid = ");
         dynamicWhereClause.append(userOrgOid);
         dynamicWhereClause.append(" connect by prior organization_oid = p_parent_corp_org_oid)");
      }
      else
      {
         dynamicWhereClause.append("and b.a_corp_org_oid = ");
         dynamicWhereClause.append(selectedOrg);
      }
      
      //CR-586 Vshah [BEGIN]--- 
      //For Users where the new User Profile level "Access to Confidential Payment Instruments/Templates" Indicator is not selected (set to No), 
      //on the Cash Management/Payment Transactions listviews, no transactions will appear that are designated as Confidential Payments
      //ctq portal refresh
      //this is done in the dataview
      //todo: comment out when ready to remove listview!!!
      if (TradePortalConstants.INDICATOR_NO.equals(confInd))
      {
         dynamicWhereClause.append(" and ( t.confidential_indicator = 'N' OR ");
         dynamicWhereClause.append(" t.confidential_indicator is null) ");
      } 	
      //CR-586 Vshah [END]
	  
      helpSensitiveLink = OnlineHelp.createContextSensitiveLink("customer/auth_trans.htm",  resMgr, userSession);
   }
   //Vshah - 12/08/2008 - Cash Management History Page - Begin
   else if (TradePortalConstants.NAV__INQUIRIES.equals(current2ndNav))
   {
       
       formName     = "CashMgmt-TransactionForm";
       helpSensitiveLink = OnlineHelp.createContextSensitiveLink("customer/cashManagement.htm",  resMgr, userSession);
       
       
       // Determine the organization to select in the dropdown
       selectedOrg = request.getParameter("historyOrg");
       if (selectedOrg == null)
       {
       	  selectedOrg = (String) session.getAttribute("historyOrg");
       	  if (selectedOrg == null)
          {
            selectedOrg = EncryptDecrypt.encryptStringUsingTripleDes(userOrgOid, userSession.getSecretKey());
            
          }
        }     

      	session.setAttribute("historyOrg", selectedOrg);
	selectedOrg = EncryptDecrypt.decryptStringUsingTripleDes(selectedOrg, userSession.getSecretKey()); 
       
   	String newSearch = request.getParameter("NewSearch");
	String newDropdownSearch = request.getParameter("NewDropdownSearch");
    	Debug.debug("New Search is " + newSearch);
      	Debug.debug("New Dropdown Search is " + newDropdownSearch);
   	searchType   = StringFunction.xssCharsToHtml(request.getParameter("SearchType"));
   		
   		if ((newSearch != null) && (newSearch.equals(TradePortalConstants.INDICATOR_YES)))
      	{
         	         	// Default search type for instrument status on the transactions history page is ACTIVE.
         	session.setAttribute("instrStatusType", TradePortalConstants.STATUS_ACTIVE);
     	}
   		
   		if (searchType == null) 
	    {
   				  searchType = TradePortalConstants.SIMPLE;
        		
      	}	
      	
        
        linkParms = "";
      	linkText  = "";
        
         if (searchType.equals(TradePortalConstants.ADVANCED))
      	{
         	linkParms += "&SearchType=" + TradePortalConstants.SIMPLE;
         	link       = formMgr.getLinkAsUrl("goToPaymentTransactionsHome", linkParms, response);
         	linkText   = resMgr.getText("common.search.basic", TradePortalConstants.TEXT_BUNDLE);

         	helpSensitiveLink = OnlineHelp.createContextSensitiveLink("customer/inst_hist.htm", "advanced", resMgr, userSession);
      	}
      	else
      	{
        	linkParms += "&SearchType=" + TradePortalConstants.ADVANCED;
         	link       = formMgr.getLinkAsUrl("goToPaymentTransactionsHome", linkParms, response);
         	linkText   = resMgr.getText("common.search.advanced", TradePortalConstants.TEXT_BUNDLE);

         	helpSensitiveLink = OnlineHelp.createContextSensitiveLink("customer/inst_hist.htm", "basic", resMgr, userSession);
     	 }

	 // Determine the organizations to select for listview
	 
	 // Build a comma separated list of the orgs
		 DocumentHandler orgDoc = null;
    
	         for (int i = 0; i < totalOrganizations; i++)
	         {
	    	        orgDoc = (DocumentHandler) orgListDoc.get(i);
        		orgList.append(orgDoc.getAttribute("ORGANIZATION_OID"));
            		if (i < (totalOrganizations - 1) )
            		{
	             		 orgList.append(", ");
    	       		}
        	 }  

	         
     	/* /This call is no more as we have moved to ajax call for grid refresh.
     	
     	if (selectedOrg.equals(ALL_ORGANIZATIONS))
     	 {
        	 // Build a comma separated list of the orgs
		 DocumentHandler orgDoc = null;
    
	         for (int i = 0; i < totalOrganizations; i++)
	         {
	    	        orgDoc = (DocumentHandler) orgListDoc.get(i);
        		orgList.append(orgDoc.getAttribute("ORGANIZATION_OID"));
            		if (i < (totalOrganizations - 1) )
            		{
	             		 orgList.append(", ");
    	       		}
        	 }  

         	 dynamicWhere.append(" and i.a_corp_org_oid in (" + orgList.toString() + ")");
      	 }
      	 else
     	 {
        	 dynamicWhere.append(" and i.a_corp_org_oid = "+  selectedOrg);
      	 }*/
     	 
     	 // Based on the statusType dropdown, build the where clause to include one or more
     	 // instrument statuses.
     	 Vector statuses = new Vector();
     	 int numStatuses = 0;

      	selectedStatus = request.getParameter("instrStatusType");

     	 if (selectedStatus == null) 
     	 {
       		  selectedStatus = (String) session.getAttribute("instrStatusType");
     	 }

      	if (TradePortalConstants.STATUS_ACTIVE.equals(selectedStatus) )
      	{
        	 statuses.addElement(TradePortalConstants.INSTR_STATUS_ACTIVE);
        	 statuses.addElement(TradePortalConstants.INSTR_STATUS_PENDING);
        	 statuses.addElement(TradePortalConstants.INSTR_STATUS_EXPIRED);
      	}

      	else if (TradePortalConstants.STATUS_INACTIVE.equals(selectedStatus) )
      	{
       		 statuses.addElement(TradePortalConstants.INSTR_STATUS_CANCELLED);
        	 statuses.addElement(TradePortalConstants.INSTR_STATUS_CLOSED);
        	 statuses.addElement(TradePortalConstants.INSTR_STATUS_DEACTIVATED);
        	 statuses.addElement(TradePortalConstants.INSTR_STATUS_LIQUIDATE_ALL);
        	 statuses.addElement(TradePortalConstants.INSTR_STATUS_LIQUIDATE_BANK_SIDE);
         	 statuses.addElement(TradePortalConstants.INSTR_STATUS_LIQUIDATE_CUST_SIDE);
      	}

      	else // default is ALL (actually not all since DELeted instruments
           // never show up (handled by the SQL in the listview XML)
      	{
        	 selectedStatus = TradePortalConstants.STATUS_ALL;
      	}

     	session.setAttribute("instrStatusType", selectedStatus);
     	
     	
     	if (!TradePortalConstants.STATUS_ALL.equals(selectedStatus))
     	{
        	 dynamicWhere.append(" and i.instrument_status in (");

	         numStatuses = statuses.size();

    	     for (int i=0; i<numStatuses; i++)
    	     {
        	     dynamicWhere.append("'");
            	 dynamicWhere.append( (String) statuses.elementAt(i) );
             	 dynamicWhere.append("'");

	             if (i < numStatuses - 1)
	             {
               		dynamicWhere.append(", ");
             	 }
         	 }

         	 dynamicWhere.append(") ");
      	}
      	
      	//CR-586 Vshah [BEGIN]---
      	//For Users where the new User Profile level "Access to Confidential Payment Instruments/Templates" Indicator is not selected (set to No), 
	//on the Cash Management/Payment Transactions listviews, no transactions will appear that are designated as Confidential Payments
	if (TradePortalConstants.INDICATOR_NO.equals(confInd))
	{
	    //IAZ IR-RDUK091546587 Use INSTRUMENTS table with Instrument History View for Conf Indicator Check
		dynamicWhereClause.append(" and ( i.confidential_indicator = 'N' OR ");
		dynamicWhereClause.append(" i.confidential_indicator is null) ");
	}
  	//CR-586 Vshah [END]
%>
		<%@ include file="/cashManagement/fragments/CashMgmt-TranSearch-TransType.frag" %>
		<%@ include file="/cashManagement/fragments/CashMgmt-TranSearch-SearchParms.frag" %>
		
		<%-- JavaScript for Instrument History to enable form submission by enter.
           event.which works for NetScape and event.keyCode works for IE  --%>
      <script LANGUAGE="JavaScript">
      
         function enterSubmit(event, myform) {
            if (event && event.which == 13) {
               setButtonPressed('null', 0);
               myform.submit();
            }
            else if (event && event.keyCode == 13) {
               setButtonPressed('null', 0);
               myform.submit();
            }
            else {
               return true;
            }
         }
  

      </script>

<%      	
     }
     
   Debug.debug("form " + formName);
   
   //Vshah - 12/08/2008 - Cash Management History Page - End   
%>

<%-- ********************* HTML for page begins here *********************  --%>


  
  <%
   if ( (TradePortalConstants.NAV__PENDING_TRANSACTIONS.equals(current2ndNav)) ||
        (TradePortalConstants.NAV__FUTURE_TRANSACTIONS.equals(current2ndNav)) )
   {
      // Store the user's oid and security rights in a secure hashtable for the form
      secureParms.put("SecurityRights", userSecurityRights);
      secureParms.put("UserOid",        userOid);
   }

   CorporateOrganizationWebBean corpOrg  = beanMgr.createBean(CorporateOrganizationWebBean.class, "CorporateOrganization");
   corpOrg.getById(userOrgOid);
  %>

<%
  // Based on the current secondary navigation, display the appropriate HTML
  if (TradePortalConstants.NAV__PENDING_TRANSACTIONS.equals(current2ndNav)) {   
	  if(searchQueryMap!=null && !searchQueryMap.isEmpty()){
		   savedSearchQueryData = (Map)searchQueryMap.get("PaymentPendingTransactionsDataView");
	   
	   }  
%>
    <%@ include file="/cashManagement/fragments/PaymentPendingTransactions.frag" %> 
<%
  }
  else if (TradePortalConstants.NAV__FUTURE_TRANSACTIONS.equals(current2ndNav)) {
	  if(searchQueryMap!=null && !searchQueryMap.isEmpty()){
		   savedSearchQueryData = (Map)searchQueryMap.get("CashMgmtFutureValueDataView");
	   
	   } 
%>
    <%@ include file="/cashManagement/fragments/CashMgmt-FutureValue-ListView.frag" %>
<%
  } 
  else if (TradePortalConstants.NAV__AUTHORIZED_TRANSACTIONS.equals(current2ndNav)) {
	  if(searchQueryMap!=null && !searchQueryMap.isEmpty()){
		   savedSearchQueryData = (Map)searchQueryMap.get("PaymentAuthorizedTransactionsDataView");
	   
	   }  
%>
    <%@ include file="/cashManagement/fragments/PaymentAuthorizedTransactions.frag" %>
<%
  }
  else if (TradePortalConstants.NAV__INQUIRIES.equals(current2ndNav)) {
	  if(searchQueryMap!=null && !searchQueryMap.isEmpty()){
		   savedSearchQueryData = (Map)searchQueryMap.get("PaymentInquiriesDataView");
	   
	   } 
%>
    <%@ include file="/cashManagement/fragments/CashMgmt-HistoryListView.frag" %>
<%
  } 
%>
        


<%
//   }
%>

<%
      //IAZ CM-451 12/27/08 Begin: add secure params for the CM Page navigation
      //secureParms.put("bankBranch",corpOrg.getAttribute("first_op_bank_org")); 
      secureParms.put("userOid",userSession.getUserOid());
      secureParms.put("securityRights",userSession.getSecurityRights()); 
      secureParms.put("clientBankOid",userSession.getClientBankOid()); 
      secureParms.put("ownerOrg",userSession.getOwnerOrgOid());
      //IAZ 12/27/08 End 
      //VS RVU062382912 Adding New parameter value for transactions
      if (userSession.getSavedUserSession() == null) {
        secureParms.put("AuthorizeAtParentViewOrSubs", TradePortalConstants.INDICATOR_YES); 
      }
%>
<%= formMgr.getFormInstanceAsInputField(formName, secureParms) %>

  </div>

</form>
                      
  </div>
</div>

<%-- IR T36000014931 REL ER 8.1.0.4 BEGIN  --- ALWAYS PLACE THESE FORMS AFTER THE PRIMARY FORM. DO NOT CHANGE THE ORDER OF THESE FORMS AS OTHER FUNCTIONALITY WILL BE AFFECTED. --%>
<form method="post" name="NewInstrumentForm1" action="<%=formMgr.getSubmitAction(response)%>">
<input type="hidden" name="bankBranch" />
<input type="hidden" name="transactionType"/>
<input type="hidden" name="instrumentType" />
<input type="hidden" name="copyInstrumentOid" />
<input type="hidden" name="mode" value="CREATE_NEW_INSTRUMENT" />
<input type="hidden" name="copyType" value="Instr" />
 
<%= formMgr.getFormInstanceAsInputField("NewInstrumentForm",newInstrSecParms) %>
</form>
<%
Hashtable newTempSecParms = new Hashtable();
newTempSecParms.put("userOid", userSession.getUserOid());
newTempSecParms.put("securityRights", userSession.getSecurityRights());
newTempSecParms.put("clientBankOid", userSession.getClientBankOid());
newTempSecParms.put("ownerOrg", userSession.getOwnerOrgOid());
newTempSecParms.put("ownerLevel", userSession.getOwnershipLevel());
%>
<form method="post" name="NewTemplateForm1" action="<%=formMgr.getSubmitAction(response)%>">
<input type="hidden" name="name" />
<input type="hidden" name="bankBranch" />
<input type="hidden" name="transactionType"/>
<input type="hidden" name="InstrumentType" />
<input type="hidden" name="copyInstrumentOid" />
<input type="hidden" name="mode"  />
<input type="hidden" name="CopyType" />
<input type="hidden" name="expressFlag"  />
<input type="hidden" name="fixedFlag" />
<input type="hidden" name="PaymentTemplGrp" />
<input type="hidden" name="validationState" />

<%= formMgr.getFormInstanceAsInputField("NewTemplateForm",newTempSecParms) %>
</form>


<%
Hashtable routeTransactionSecParms = new Hashtable();
routeTransactionSecParms.put("UserOid", userSession.getUserOid());
routeTransactionSecParms.put("SecurityRights", userSession.getSecurityRights());
%>

<form method="post" name="RouteTransactionsForm" action="<%=formMgr.getSubmitAction(response)%>">
<input type="hidden" name="RouteUserOid" />
<input type="hidden" name="RouteCorporateOrgOid" />
<input type="hidden" name="RouteToUser"/>
<input type="hidden" name="RouteToOrg" />
<input type="hidden" name="multipleOrgs" />
<input type="hidden" name="routeAction"  />
<input type="hidden" name="fromListView" />

<%= formMgr.getFormInstanceAsInputField("RouteTransactionsForm",routeTransactionSecParms) %>
</form>

<%-- IR T36000014931 REL ER 8.1.0.4 END --%>
<%--cquinton Portal Refresh - start--%>
<jsp:include page="/common/Footer.jsp">
  <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
  <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>
<%@ include file="/common/GetSavedSearchQueryData.frag" %> 
<%
  // load the footer fragments - this is where individual page javascript should be placed
  if (TradePortalConstants.NAV__PENDING_TRANSACTIONS.equals(current2ndNav)) {
%>
    <%@ include file="/cashManagement/fragments/PaymentPendingTransactionsFooter.frag" %>
<%
  }else if (TradePortalConstants.NAV__FUTURE_TRANSACTIONS.equals(current2ndNav)) {
	  %>
	    <%@ include file="/cashManagement/fragments/CashMgmt-FutureValue-Footer.frag" %>
<%
  }	
  else if (TradePortalConstants.NAV__AUTHORIZED_TRANSACTIONS.equals(current2ndNav)) {
%>
    <%@ include file="/cashManagement/fragments/PaymentAuthorizedTransactionsFooter.frag" %>
<%
  }
else if (TradePortalConstants.NAV__INQUIRIES.equals(current2ndNav)) {
%>
    <%@ include file="/cashManagement/fragments/PaymentInquiries-Footer.frag" %>
<%       
  } 
%>
<%--cquinton 3/16/2012 Portal Refresh - end--%>
 
</body>
</html>

<%
   formMgr.storeInDocCache("default.doc", new DocumentHandler());

   // Store the current secondary navigation in the session.
   session.setAttribute("paymentTransactions2ndNav", current2ndNav);
%>
