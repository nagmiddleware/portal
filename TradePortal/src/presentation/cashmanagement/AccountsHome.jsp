<%--
*******************************************************************************
  Accounts Home Page

  Description:  
     This page is used as the Accounts main page.
   
*******************************************************************************
--%>

<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>


<%@ page import="java.util.*,com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*, com.ams.tradeportal.html.*" %>

<jsp:useBean id="beanMgr"     class="com.amsinc.ecsg.web.BeanManager"            scope="session"></jsp:useBean>
<jsp:useBean id="formMgr"     class="com.amsinc.ecsg.web.FormManager"            scope="session"></jsp:useBean>
<jsp:useBean id="resMgr"      class="com.amsinc.ecsg.util.ResourceManager"       scope="session"></jsp:useBean>
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>
 

<%-- ********************* JavaScript for page begins here *********************  --%>



<%-- ************** Data retrieval page setup begins here ****************  --%>

<%

   StringBuffer      onLoad             = new StringBuffer();
   Hashtable         secureParms        = new Hashtable();
   String            helpSensitiveLink  = null;
   String            current2ndNav      = null;
   String            formName           = null;

   String            userOrgOid         = null;
   String            userOid            = null;
   String            userSecurityRights = null;
   String            userSecurityType   = null;

   // Set the current primary navigation item
   userSession.setCurrentPrimaryNavigation("NavigationBar.Accounts");

   //cquinton 1/18/2013 set return page
   if ( "true".equals( request.getParameter("returning") ) ) {
     userSession.pageBack(); //to keep it correct
   }
   else {
     userSession.addPage("goToAccountsHome");
   }
   
   //IR T36000026794 Rel9.0 07/24/2014 starts
   session.setAttribute("startHomePage", "AccountsHome");
   session.removeAttribute("fromTPHomePage");
   //IR T36000026794 Rel9.0 07/24/2014 End
   
   Map searchQueryMap =  userSession.getSavedSearchQueryData();
   Map savedSearchQueryData =null;
  
   WidgetFactory widgetFactory = new WidgetFactory(resMgr);

   //cquinton 1/18/2013 remove old close action behavior

   // Retrieve the user's security rights, security type, organization oid, and user oid
   userSecurityRights = userSession.getSecurityRights();
   userSecurityType   = userSession.getSecurityType();
   userOrgOid         = userSession.getOwnerOrgOid();
   userOid            = userSession.getUserOid();
   
   //CR-586 Vshah [BEGIN]
   UserWebBean thisUser = beanMgr.createBean(UserWebBean.class, "User");
   thisUser.setAttribute("user_oid", userOid);
   thisUser.getDataFromAppServer();
   
   String confInd = "";
   if (userSession.getSavedUserSession() == null)
       confInd = thisUser.getAttribute("confidential_indicator");
   else
   {
       if (TradePortalConstants.ADMIN.equals(userSession.getSavedUserSession().getSecurityType()))
           confInd = TradePortalConstants.INDICATOR_YES;
       else
           confInd = thisUser.getAttribute("subsid_confidential_indicator");  
   }
   if (InstrumentServices.isBlank(confInd))
      confInd = TradePortalConstants.INDICATOR_NO;
   //CR-586 Vshah [END]

   current2ndNav = request.getParameter("current2ndNav");
   if (current2ndNav == null) {
      current2ndNav = (String) session.getAttribute("accounts2ndNav");

      if (current2ndNav == null) {
         current2ndNav = TradePortalConstants.NAV__ACCT_BALANCE;
      }
   }
   
   // Now perform data setup for whichever is the current secondary navigation.  Each block of
   // code sets tab specific parameters (for data retrieval or dropdowns) as
   // well as setting common things like the appropriate help link and formName


   // Set the performance statistic object to have the current tab as its context
    PerformanceStatistic perfStat = (PerformanceStatistic) session.getAttribute("performanceStatistic");

   if((perfStat != null) && (perfStat.isLinkAction()))
           perfStat.setContext(current2ndNav);

   // ***********************************************************************
   // Data setup for the Account Balances tab
   // ***********************************************************************
   formName = "AcctBalanceForm";
   helpSensitiveLink = OnlineHelp.createContextSensitiveLink("customer/account_balance_screen.htm",  resMgr, userSession);
  
   Debug.debug("form " + formName);
%>

<%
 String            cashSelection                   = null;
 StringBuffer      newLink                          = new StringBuffer();
 String selectedOrg    = ""; 
 String searchListViewName = "";
 StringBuffer dynamicWhereClause = new StringBuffer();
 
	final String      ALL_ORGANIZATIONS            = resMgr.getText("AuthorizedTransactions.AllOrganizations",
                                                                   TradePortalConstants.TEXT_BUNDLE);
 if (current2ndNav.equals(TradePortalConstants.NAV__ACCT_BALANCE))
   {
      cashSelection = request.getParameter("cashSelection");

      if (cashSelection == null)
      {
         cashSelection = (String) session.getAttribute("cashSelection");
	      if (cashSelection == null)
         {
            cashSelection = TradePortalConstants.NAV__ACCT_BALANCE;
         }
      }
   }   
   
   session.setAttribute("cashSelection",     cashSelection);
  //IAZ CM-451 12/26/08 Begin 
  //check if this is a bank user: if so, use CustAccountListView to display all accounts
  //for this customer.  if not, use AccountListView to display accounts for this user only 
  
  //IAZ CM-451 02/25/09 Begin
  //Use userSession.hasSavedUserSession() to determine if bank/subsidiary user

  //cquinton 12/20/2012 move allowPayByAnotherAcct processing to the dataview 
   
  //IAZ CM-451 12/26/08 and 02/25/09 End   
    selectedOrg = request.getParameter("historyOrg");
	      if (selectedOrg == null)
	      {
	         selectedOrg = (String) session.getAttribute("historyOrg");
	         if (selectedOrg == null)
	         {
	            selectedOrg = EncryptDecrypt.encryptStringUsingTripleDes(userOrgOid, userSession.getSecretKey());
	         }
	      }
     	 session.setAttribute("historyOrg", selectedOrg);
      	 selectedOrg = EncryptDecrypt.decryptStringUsingTripleDes(selectedOrg, userSession.getSecretKey());
      	 
      	String searchNav = "acctHome."+current2ndNav; 
%>

<%-- ********************* HTML for page begins here *********************  --%>

<jsp:include page="/common/Header.jsp">
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="additionalOnLoad"         value="<%=onLoad.toString()%>" />
</jsp:include>

<%-- Vshah - 08/02/2012 - Rel8.1 - Portal-refresh --%>
<%--  including TransactionCleanup.jsp to remove lock on instrument which has been placed by this user... --%>
<%--  this is required because (with Portal-refresh) user can navigate anywhere from MenuBar when on Instrument(detail) page. --%>
<jsp:include page="/transactions/TransactionCleanup.jsp"></jsp:include>

<jsp:include page="/common/SearchCriteriaCleanup.jsp">
<jsp:param name="searchCriteriaSection" value="<%=searchNav%>"/>
</jsp:include>

<div class="pageMain">
<div class="pageContent">

<div class="secondaryNav">
  <div class="secondaryNavTitle">
    <%=resMgr.getText("SecondaryNavigation.AccountBalances", TradePortalConstants.TEXT_BUNDLE)%>
  </div>
  <div class="secondaryNavHelp" id="secondaryNavHelp">
    <%=helpSensitiveLink%>
  </div>
  <%=widgetFactory.createHoverHelp("secondaryNavHelp","HelpImageLinkHoverText") %> 
  <div style="clear:both;"></div>
</div>



<form name="<%=formName%>" method="POST"  action="<%= formMgr.getSubmitAction(response) %>">
<input type=hidden value="" name=buttonName>

<jsp:include page="/common/ErrorSection.jsp" />

<div  class="formContentNoSidebar">
 
<%
  String gridName = null;
  String viewName = null;
   
  if (userSession.hasSavedUserSession())
  {
    gridName = "CustAccountDataGrid";
    viewName = "CustAccountDataView";
    if(searchQueryMap!=null && !searchQueryMap.isEmpty()){
       	savedSearchQueryData = (Map)searchQueryMap.get("CustAccountDataView");
       }
  }
  else 
  {
    gridName = "AccountDataGrid";
    viewName = "AccountDataView";
    if(searchQueryMap!=null && !searchQueryMap.isEmpty()){
       	savedSearchQueryData = (Map)searchQueryMap.get("AccountDataView");
       }
  }
 
 // DataGridFactory dGridFactory = new DataGridFactory(resMgr, userSession, formMgr, response);
//CR 590
  DGridFactory dGridFactory = new DGridFactory(resMgr, userSession, formMgr, beanMgr, response);
  String gridHtml = dGridFactory. createDataGrid("accountDataGrid",gridName,null);
  String gridLayout = dGridFactory.createGridLayout(gridName);
  
  newLink.append(formMgr.getFormInstanceAsURL("cashManagementViaIntermediatePageForm"));
%>

  <%--cquinton 10/8/2012 include gridShowCounts --%>
  <div class="gridSearch">
    <div class="searchHeader">
      <span class="searchHeaderActions">
        <jsp:include page="/common/dGridShowCount.jsp">
          <jsp:param name="gridId" value="accountDataGrid" />
        </jsp:include>
		<%-- vdesingu Rel8.4 CR-590: Removed Anchor tag, now refresh with only reload the grid  --%>
        <span id="acctRefresh" class="searchHeaderRefresh"></span>
      </span>
		<%=widgetFactory.createHoverHelp("acctRefresh","RefreshImageLinkHoverText") %> 
      <div style="clear:both;"></div>
    </div>
  </div>

  <%=gridHtml %>
 
<%
      //IAZ CM-451 12/27/08 Begin: add secure params for the CM Tab navigation
      //secureParms.put("bankBranch",corpOrg.getAttribute("first_op_bank_org")); 
      secureParms.put("userOid",userSession.getUserOid());
      secureParms.put("securityRights",userSession.getSecurityRights()); 
      secureParms.put("clientBankOid",userSession.getClientBankOid()); 
      secureParms.put("ownerOrg",userSession.getOwnerOrgOid());
      //IAZ 12/27/08 End 
      //VS RVU062382912 Adding New parameter value for transactions
      if (userSession.getSavedUserSession() == null) {
        secureParms.put("AuthorizeAtParentViewOrSubs", TradePortalConstants.INDICATOR_YES); 
      }
%>
<%= formMgr.getFormInstanceAsInputField(formName, secureParms) %>

</div>
</form>

</div>
</div>

<%--cquinton 3/16/2012 Portal Refresh - start--%>
<jsp:include page="/common/Footer.jsp">
   <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>
<%@ include file="/common/GetSavedSearchQueryData.frag" %>
<script type="text/javascript">
  <%--get grid layout from the data grid factory--%>
  require(["t360/OnDemandGrid", "dojo/dom-construct"], function(onDemandGrid, domConstruct){
  var gridLayout = <%= gridLayout %>;
  <%-- cquinton 12/20/2012 move user/org parms to dataview for security --%>
  var initSearchParms = "";
  var savedSort = savedSearchQueryData["sort"];  
 	if("" == savedSort || null == savedSort || "undefined" == savedSort){
      savedSort = '0';
   }
 	var accountDataGrid = "";
  <%--create the grid, attaching it to the div id specified in dataGrid.jsp--%>
  require(["t360/OnDemandGrid", "dojo/dom-construct"], function(onDemandGrid, domConstruct){
  	accountDataGrid =  onDemandGrid.createOnDemandGrid("accountDataGrid", "<%=EncryptDecrypt.encryptStringUsingTripleDes(viewName,userSession.getSecretKey())%>", 
  			gridLayout, initSearchParms,savedSort); <%--Rel9.2 IR T36000032596 --%>
  });
  });
 <%-- vdesingu Rel8.4 CR-590 : refresh button will refresh only the grid not the page --%>
 require(["dojo/query", "dojo/on", "dojo/ready"],
		 function(query, on, ready){
		 	ready(function() {
 				query('#acctRefresh').on("click", function() {
     			refreshAccountBalance();
   				});
		 	});
 });
	<%-- Pass custom grid message	 	 --%>
 function refreshAccountBalance(){
	 require(["t360/OnDemandGrid"], function(onDemandGridSearch){
		 var searchParms = "refresh=true"; 
		 var gridLoadingMessage = "<%=resMgr.getText("Home.AccountBalanceLoadingMessage", TradePortalConstants.TEXT_BUNDLE) %>";
		 onDemandGridSearch.searchDataGrid("accountDataGrid", "<%=viewName%>",
	                    searchParms, gridLoadingMessage);
	 });
 }
<%-- vdesingu Rel8.4 CR-590 end --%>
	  </script>

<%--cquinton 10/8/2012 include gridShowCounts --%>
<jsp:include page="/common/dGridShowCountFooter.jsp">
  <jsp:param name="gridId" value="accountDataGrid" />
</jsp:include>
<%--cquinton 3/16/2012 Portal Refresh - end--%>

</body>
</html>

<%
   formMgr.storeInDocCache("default.doc", new DocumentHandler());
   // Store the current secondary navigation in the session.
   session.setAttribute("accounts2ndNav", current2ndNav);
%>
