<%--cquinton 10/31/2012 removing redundant doctype
<!DOCTYPE HTML>
--%>
<%--
*******************************************************************************
                            Payment Upload Log Detail Page

  Description:  The Payment Upload Log Detail page is used to provide detail
                information for a specified payment upload file.
                
                
                This information
                includes the instrument's current terms, transactions, and
                mail messages. For users with the correct security rights,
                this page also provides links to create a mail message,
                create a transaction, and update a transaction. Links to
                current term details, related instruments, and existing
                mail messages are provided in the page as well.

  Note:         consult userSession pageFlow to determine return page.

*******************************************************************************
--%>

<%--
 *
 *     Copyright   2001
 *     American Management Systems, Incorporated
 *     All rights reserved
--%>
<%@ page import="java.util.*,com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.html.*" %>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager"
    scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean"
   scope="session">
</jsp:useBean>


<%-- ************** Data retrieval page setup begins here ****************  --%>

<%
   InstrumentWebBean    instrument                      = null;
   PaymentFileUploadWebBean    fileUpload               = null;
   TransactionWebBean	transaction						= null;
   OperationalBankOrganizationWebBean opBankOrg			= null;
   AccountWebBean debitAccount							= null;
   UserWebBean 		thisUser			= null;
   DocumentHandler      xmlDoc                          = null;
   StringBuffer         errorListWhere   	        = new StringBuffer();
   StringBuffer         dynamicWhereClause              = null;
   StringBuffer         instrumentLabel                 = new StringBuffer();
   Hashtable            secureParms                     = new Hashtable();
   String               userSecurityRights              = null;
   String               userSecurityType                = null;
   String               instrumentOid                   = null;
   String               uploadOid       	        = null;
   String               userTimeZone                    = null;
   String               userLocale                      = null;
   String               userOrgOid                      = null;
   String               newLink                         = null;
   String               userOid                         = null;
   String		validationStatus		= null;
   String               instrumentTypeCode              = null;
   int 			uploadNumber			= 0;
   String initSearchParms="";
   DGridFactory dgridFactory = new DGridFactory(resMgr, userSession, formMgr, response);
   DataGridFactory dgFactory = new DataGridFactory(resMgr, userSession, formMgr, response);
   String gridHtml = "";
   String gridLayout = "";
   String repGridHtml = "";
   String repGridLayout = "";
   boolean displayErrorGrid = false;
   String codeOptions1 = "";
   String codeOptions2 = "";

   userSession.setCurrentPrimaryNavigation("NavigationBar.UploadCenter");

   //cquinton 1/18/2013 add page to page flow
   userSession.addPage("goToPaymentUploadLogDetail", request);

   // Get the user's security rights, security type, locale, organization, oid, and client bank oid
   userSecurityRights = userSession.getSecurityRights();
   userSecurityType   = userSession.getSecurityType();
   userTimeZone       = userSession.getTimeZone();
   userLocale         = userSession.getUserLocale();
   userOrgOid         = userSession.getOwnerOrgOid();
   userOid            = userSession.getUserOid();

   fileUpload = beanMgr.createBean(PaymentFileUploadWebBean.class, "PaymentFileUpload");

   if (request.getParameter("UploadOid") != null)
   {
      uploadOid = EncryptDecrypt.decryptStringUsingTripleDes(request.getParameter("UploadOid"), userSession.getSecretKey());
      session.setAttribute("UploadOid", EncryptDecrypt.encryptStringUsingTripleDes(uploadOid, userSession.getSecretKey()));
   }
   else
      uploadOid = EncryptDecrypt.decryptStringUsingTripleDes((String) session.getAttribute("UploadOid"), userSession.getSecretKey());

   fileUpload.setAttribute("payment_file_upload_oid", uploadOid);
   fileUpload.getDataFromAppServer();
   validationStatus = fileUpload.getAttribute("validation_status");

   thisUser = beanMgr.createBean(UserWebBean.class, "User");
   thisUser.setAttribute("user_oid", fileUpload.getAttribute("user_oid"));
   thisUser.getDataFromAppServer();

   if (InstrumentServices.isNotBlank(fileUpload.getAttribute("instrument_oid"))) {
		instrument = beanMgr.createBean(InstrumentWebBean.class, "Instrument");
		instrument.getById(fileUpload.getAttribute("instrument_oid"));

		instrumentTypeCode = instrument.getAttribute("instrument_type_code");

		// Build the instrument label to use at the top of the page
		instrumentLabel.append(ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.INSTRUMENT_TYPE,
																			instrumentTypeCode, userLocale));
		instrumentLabel.append("&nbsp;&nbsp;");
		instrumentLabel.append(instrument.getAttribute("complete_instrument_id"));
		instrumentLabel.append(" - ");
   }
   errorListWhere.append(" where p_payment_file_upload_oid = ");
   errorListWhere.append(fileUpload.getAttribute("payment_file_upload_oid"));

   int beneUploaded = 0;
   if (InstrumentServices.isNotBlank(fileUpload.getAttribute("number_of_bene_uploaded")))
	beneUploaded = Integer.parseInt(fileUpload.getAttribute("number_of_bene_uploaded"));

   int beneFailed = 0;
   if (InstrumentServices.isNotBlank(fileUpload.getAttribute("number_of_bene_failed")))
	beneFailed = Integer.parseInt(fileUpload.getAttribute("number_of_bene_failed"));

   uploadNumber = beneUploaded - beneFailed;
   String strUploadNumber = Integer.toString(uploadNumber);
   
   if (InstrumentServices.isNotBlank(fileUpload.getAttribute("instrument_oid"))) {
   		StringBuffer strQuery = new StringBuffer();
   		String transactionOid = null;
   		strQuery.append("select transaction_oid from transaction where p_instrument_oid=?");
   		DocumentHandler xmlResult1 = DatabaseQueryBean.getXmlResultSet(strQuery.toString(), false, new Object[]{fileUpload.getAttribute("instrument_oid")});
   		if(xmlResult1!=null){
   			transactionOid = xmlResult1.getAttribute("/ResultSetRecord(0)/TRANSACTION_OID").toString();
   		}
   		strQuery = new StringBuffer();
   		String termsOid = null;
   		strQuery.append("select c_cust_enter_terms_oid from transaction where transaction_oid=?");
   		xmlResult1 = new DocumentHandler();
   		xmlResult1 = DatabaseQueryBean.getXmlResultSet(strQuery.toString(), false, new Object[]{transactionOid});
   		if(xmlResult1!=null){
   			termsOid = xmlResult1.getAttribute("/ResultSetRecord(0)/C_CUST_ENTER_TERMS_OID").toString();
   		}
   		strQuery = new StringBuffer();
   		String debitAccountOid = null;
   		strQuery.append("select debit_account_oid from terms where terms_oid=?");
   		xmlResult1 = new DocumentHandler();
   		xmlResult1 = DatabaseQueryBean.getXmlResultSet(strQuery.toString(), false, new Object[]{termsOid});
   		if(xmlResult1!=null){
   			debitAccountOid = xmlResult1.getAttribute("/ResultSetRecord(0)/DEBIT_ACCOUNT_OID").toString();
   		}
   		debitAccount = beanMgr.createBean(AccountWebBean.class, "Account");
	    debitAccount.getById(debitAccountOid);
	    
	    opBankOrg = beanMgr.createBean(OperationalBankOrganizationWebBean.class, "OperationalBankOrganization");
	    opBankOrg.getById(debitAccount.getAttribute("op_bank_org_oid"));
		
		String bankGroupOid = opBankOrg.getAttribute("bank_org_group_oid");
		StringBuffer reportingCode1 = new StringBuffer();
		StringBuffer reportingCode2 = new StringBuffer();
		reportingCode1.append("select code, description from payment_reporting_code_1");
		reportingCode2.append("select code, description from payment_reporting_code_2");
		reportingCode1.append(" where p_bank_group_oid = ? ");
		reportingCode2.append(" where p_bank_group_oid = ? ");
		
		DocumentHandler xmlDocRep1 = DatabaseQueryBean.getXmlResultSet(reportingCode1.toString(), false, new Object[]{bankGroupOid});
		DocumentHandler xmlDocRep2 = DatabaseQueryBean.getXmlResultSet(reportingCode2.toString(), false, new Object[]{bankGroupOid});
		
		if (xmlDocRep1 != null) {
			codeOptions1 = Dropdown.createMultiTextOptions(xmlDocRep1,"CODE", "CODE,DESCRIPTION", "",userLocale);
		}
	
		if (xmlDocRep2 != null) {
			codeOptions2 = Dropdown.createMultiTextOptions(xmlDocRep2,"CODE", "CODE,DESCRIPTION", "",userLocale);
		}
   }
   
   if(validationStatus.equals(TradePortalConstants.PYMT_UPLOAD_VALIDATED_AWAITING_REPAIR))
	{
		MediatorServices medService = new MediatorServices();
	  	medService.getErrorManager().setLocaleName(resMgr.getResourceLocale());
	  	medService.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.FILE_REPORTING_ERRORS_MUST_BE_CONFIRMED);
	  	medService.addErrorInfo();
		DocumentHandler errDoc = formMgr.getFromDocCache();
		errDoc.setComponent("/Error", medService.getErrorDoc());
	}

%>

<%-- ********************* HTML for page begins here *********************  --%>

<% WidgetFactory widgetFactory = new WidgetFactory(resMgr); %>

<jsp:include page="/common/Header.jsp">
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeErrorSectionFlag"  value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>

<div class="pageMainNoSidebar">
  <div class="pageContent">

    <jsp:include page="/common/PageHeader.jsp">
      <jsp:param name="titleKey" value="PaymentUploadLogDetail.UploadLogText"/>
      <jsp:param name="helpUrl" value="customer/payment_file_upload_log.htm"/>
    </jsp:include>

    <div class="subHeaderDivider"></div>

    <div class="pageSubHeader">
      <span class="pageSubHeaderItem">
        <%= instrumentLabel.toString() %>
<% if (validationStatus.equals(TradePortalConstants.PYMT_UPLOAD_VALIDATED_WITH_ERRORS) || validationStatus.equals(TradePortalConstants.PYMT_UPLOAD_VALIDATION_FAILED)
		|| validationStatus.equals(TradePortalConstants.PYMT_UPLOAD_VALIDATED_AWAITING_REPAIR)) {
      out.print("Error Log"); 
   }
   else {
      out.print("Upload Log");
   }

int reportingErrCount = DatabaseQueryBean.getCount("line_number", "PAYMENT_UPLOAD_ERROR_LOG", " p_payment_file_upload_oid = ? and reporting_code_error_ind=?",false, 
								new Object[]{uploadOid, TradePortalConstants.INDICATOR_YES});
int beneErrCount = DatabaseQueryBean.getCount("line_number", "PAYMENT_UPLOAD_ERROR_LOG", " p_payment_file_upload_oid = ? and (reporting_code_error_ind=null or reporting_code_error_ind=?)",false, 
		new Object[]{uploadOid, TradePortalConstants.INDICATOR_NO});

%>
      </span>
      <span class="pageSubHeader-right">

        <%--cquinton 10/31/2012 ir#7015 change return link to close button when no close button present.
            note this does NOT include an icon as in the sidebar--%>
<%
  //cquinton 1/18/2013 replace old close action behavior with page flow logic
  SessionWebBean.PageFlowRef backPage = userSession.peekPageBack();
  String returnAction = backPage.getLinkAction();
  String returnParms = backPage.getLinkParametersString() + "&returning=true";
  String closeLink = formMgr.getLinkAsUrl(returnAction,returnParms,response);
%>
        <button data-dojo-type="dijit.form.Button" name="CloseButton" id="CloseButton" type="button" class="pageSubHeaderButton">
          <%=resMgr.getText("common.CloseText",TradePortalConstants.TEXT_BUNDLE)%>
          <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
            document.location.href = "<%= closeLink %>";
          </script>
        </button> 
        <%=widgetFactory.createHoverHelp("CloseButton", "CloseHoverText") %>
      </span>
      <div style="clear:both;"></div>
    </div>


    <form name="PaymentUploadLogDetailForm" method="POST" action="<%= formMgr.getSubmitAction(response) %>">
    <%if(validationStatus.equals(TradePortalConstants.PYMT_UPLOAD_VALIDATED_AWAITING_REPAIR) && reportingErrCount>0){ %>
		<jsp:include page="/common/ErrorSection.jsp"/>
	<%} %>
      <input type=hidden value="" name=buttonName>
      <div class="formContentNoSidebar">

<%
     // Store this parameter so it can be passed back into the page from somewhere else
     secureParms.put("payment_file_upload_oid", uploadOid);
     secureParms.put("user_oid", thisUser.getAttribute("user_oid"));
	 if (InstrumentServices.isNotBlank(fileUpload.getAttribute("instrument_oid"))) {
		secureParms.put("instrument_type_code", instrument.getAttribute("instrument_type_code"));	 
		secureParms.put("instrument_oid", instrument.getAttribute("instrument_oid"));
	 }
%>
  

        <br>
        <div>
	        <%= widgetFactory.createTextField("User", "PaymentUploadLogDetail.User", thisUser.getAttribute("user_identifier"), "25", true, false, false, "", "", "inline")%>
	        <%= widgetFactory.createTextField("ValidationStatus", "PaymentUploadLogDetail.ValidationStatus", ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.VALIDATION_STATUS,
	          validationStatus,
	          userLocale), "25", true, false, false, "", "", "inline")%>
	        <%= widgetFactory.createTextField("CompletionDate", "PaymentUploadLogDetail.CompletionDate", TPDateTimeUtility.convertGMTDateTimeForTimezoneAndFormat(
								fileUpload.getAttribute("completion_timestamp"),
								TPDateTimeUtility.SHORT,
								resMgr.getResourceLocale(), userTimeZone), "25", true, false, false, "", "", "inline")%>
		</div>
		<div style="clear:both;"></div>
		
		<div>
			<% if (!validationStatus.equals(TradePortalConstants.PYMT_UPLOAD_VALIDATION_SUCCESSFUL)) {
			   	if ( (validationStatus.equals(TradePortalConstants.PYMT_UPLOAD_VALIDATION_FAILED)) &&
			 	     (InstrumentServices.isNotBlank(fileUpload.getAttribute("error_msg")))  && beneErrCount == 0 && reportingErrCount == 0) {
			%>   
			        <%= widgetFactory.createTextField("Errors", "PaymentUploadLogDetail.Errors", fileUpload.getAttribute("error_msg"), "25", true, false, false, "", "", "inline")%>
				    
			<%
				}
				else {
			%>   
			        <%= widgetFactory.createTextField("UploadedBenes", "PaymentUploadLogDetail.UploadedBenes", strUploadNumber, "25", true, false, false, "", "", "inline")%>
			        <%= widgetFactory.createTextField("FailedBenes", "PaymentUploadLogDetail.FailedBenes", fileUpload.getAttribute("number_of_bene_failed"), "25", true, false, false, "", "", "inline")%>
				    
			<% 	}
			   }
			%>   
		</div>
		<div style="clear:both;"></div>
  
<div class="formItem">  
<% if (!validationStatus.equals(TradePortalConstants.PYMT_UPLOAD_VALIDATION_SUCCESSFUL)) {
      if ( (validationStatus.equals(TradePortalConstants.PYMT_UPLOAD_VALIDATION_FAILED)) &&
         (InstrumentServices.isNotBlank(fileUpload.getAttribute("error_msg"))) && beneErrCount == 0 && reportingErrCount == 0 ) {
        //empty statement
      }
      else {
    	  if(beneErrCount>0 ){
      
%>   
	<%=widgetFactory.createSectionHeader("1", "PaymentUploadLogDetail.BeneficiaryErrors") %>
		<span style="font-style:italic;">
        <%=widgetFactory.createSubLabel("PaymentUploadLogDetail.ValidationWithErrorText") %>
	    </span>
<%      
            displayErrorGrid = true;
            if (validationStatus.equals(TradePortalConstants.PYMT_UPLOAD_VALIDATED_WITH_ERRORS)) {
 
               gridHtml = dgridFactory.createDataGrid("CashMgmtPaymentFileUploadErrorsDataGridId","CashMgmtPaymentFileUploadErrorsDataGrid", null);
               gridLayout = dgridFactory.createGridLayout("CashMgmtPaymentFileUploadErrorsDataGrid");
            }
            else {
    	       gridHtml = dgridFactory.createDataGrid("CashMgmtPaymentFileUploadErrorsDataGridId","CashMgmtPaymentFileUploadErrorsNoButtonsDataGrid", null);
               gridLayout = dgridFactory.createGridLayout("CashMgmtPaymentFileUploadErrorsNoButtonsDataGrid");
            }
%>   

        <%=gridHtml%>

<%     
	}
    	  if(reportingErrCount>0 || validationStatus.equals(TradePortalConstants.PYMT_UPLOAD_VALIDATED_AWAITING_REPAIR)){ %>
 </div> <%--Beneficiary Errors section ends --%>
 	<%=widgetFactory.createSectionHeader("2", "PaymentUploadLogDetail.ReportingCodeErrors") %>
		<span style="font-style:italic;">
        <%=widgetFactory.createSubLabel("PaymentUploadLogDetail.ReportingCodeErrText1") %><br>
        <%=widgetFactory.createSubLabel("PaymentUploadLogDetail.ReportingCodeErrText2") %><br>
        <%=widgetFactory.createSubLabel("PaymentUploadLogDetail.ReportingCodeErrText3") %><br>
        <%=widgetFactory.createSubLabel("PaymentUploadLogDetail.ReportingCodeErrText4") %><br>
        <%=widgetFactory.createSubLabel("PaymentUploadLogDetail.ReportingCodeErrText5") %><br><br>
	    </span>
	    
	<div>    
	<%=widgetFactory.createSelectField("ReportingCode1","PaymentFileUploadErrors.ReportingCode1"," ", codeOptions1, false, false, false, "", "", "none")%>
	<span id="applyUpdatesSpan">
		<button data-dojo-type="dijit.form.Button" type="button" name="applyUpdatesButton" id="applyUpdatesButton" class="SearchButton">
		     <%=resMgr.getText("PaymentUploadLogDetail.ApplyUpdates", TradePortalConstants.TEXT_BUNDLE)%>
		     <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
		        	performUpdate();
			</script>
		</button>
	</span>	
	<div style="clear:both;"></div>
	</div>
	<div>
	<%=widgetFactory.createSelectField("ReportingCode2","PaymentFileUploadErrors.ReportingCode2"," ", codeOptions2, false, false, false, "", "", "none")%>
	<span id="confirmRejectSpan" class="pageSubHeader-right">
		<button data-dojo-type="dijit.form.Button" type="button" name="confirmButton" id="confirmButton" class="SearchButton">
		     <%=resMgr.getText("PaymentUploadLogDetail.Confirm", TradePortalConstants.TEXT_BUNDLE)%>
		     <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
		        	setButtonPressed('<%=TradePortalConstants.BUTTON_CONFIRM%>', '0');
					document.forms[0].submit();
			</script>
		</button>
		<button data-dojo-type="dijit.form.Button" type="button" name="rejectButton" id="rejectButton" class="SearchButton">
		     <%=resMgr.getText("PaymentUploadLogDetail.Reject", TradePortalConstants.TEXT_BUNDLE)%>
		     <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
		        	setButtonPressed('<%=TradePortalConstants.BUTTON_REJECT%>', '0');
					document.forms[0].submit();
			</script>
		</button>
		</span>
	<div style="clear:both;"></div>
	</div>
	<br>
	    
<%       //if (request.getParameter("UploadOid") != null) {
            displayErrorGrid = true;
            //if(validationStatus.equals(TradePortalConstants.PYMT_UPLOAD_VALIDATED_AWAITING_REPAIR)){
            	repGridHtml = dgFactory.createDataGrid("CashMgmtPaymentFileRepCodeErrorsDataGridId","CashMgmtPaymentFileRepCodeErrorsDataGrid", null);
                repGridLayout = dgFactory.createGridLayout("CashMgmtPaymentFileRepCodeErrorsDataGrid");
           // }
%>   

        <%=repGridHtml%>

<%      // } %>
 </div> <%--Reporting Code Errors section ends --%>
<%     }
      }
   } //cquinton 10/31/2012 - added closing bracket. missing bracket causes footer not to be included
%>
 </div>	  	
        <%= formMgr.getFormInstanceAsInputField("PaymentUploadLogDetailForm", secureParms) %>

      </div>
    </form>

  </div>
</div>

<%--cquinton Portal Refresh - start--%>
<jsp:include page="/common/Footer.jsp">
  <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
  <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>
<%--cquinton Portal Refresh - end--%>

<% 
  if (displayErrorGrid) {
%>
  <script type="text/javascript">
  require(["t360/OnDemandGrid", "dojo/dom-construct"], function(onDemandGrid, domConstruct){
	<%if(beneErrCount>0){%>		
    var gridLayout = <%=gridLayout%>;
    var initSearchParms="uploadOid=<%=uploadOid%>";
    var viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("CashMgmtPaymentFileUploadErrorsDataView",userSession.getSecretKey())%>";  <%--Rel9.2 IR T36000032596 --%>
    onDemandGrid.createOnDemandGrid("CashMgmtPaymentFileUploadErrorsDataGridId", viewName, gridLayout, initSearchParms,'0'); 
      });
    <%} if(reportingErrCount>0 || validationStatus.equals(TradePortalConstants.PYMT_UPLOAD_VALIDATED_AWAITING_REPAIR)){ %>
    var repGridLayout = <%=repGridLayout%>;
    var initSearchRepParms="uploadOid=<%=uploadOid%>";
    var repViewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("CashMgmtPaymentFileRepCodeErrorsDataView",userSession.getSecretKey())%>";  <%--Rel9.4 CR-1001 --%>
    createDataGrid("CashMgmtPaymentFileRepCodeErrorsDataGridId",repViewName,repGridLayout, initSearchRepParms,'0'); 
    <%}%>
    
    require(["dijit/registry", "dojo/ready", "dojo/aspect", "dojo/domReady!"],
      function(registry,ready,aspect ) {
	    ready(function(){
	    	var section1 = dijit.byId("section1");
	    	var section2 = dijit.byId("section2");
	        var validationStatus = '<%=validationStatus%>';
	        var beneErrCount = <%=beneErrCount%>;
	        var reportingErrCount = <%=reportingErrCount%>;
	        var rgrid=registry.byId("CashMgmtPaymentFileRepCodeErrorsDataGridId");
	        var bgrid=registry.byId("CashMgmtPaymentFileUploadErrorsDataGridId");
	        rgrid.noDataMessage="No Reporting Code Errors";
	        
	        aspect.after(rgrid, "_onFetchComplete", function(){
	        	if(validationStatus == '<%=TradePortalConstants.PYMT_UPLOAD_VALIDATED_WITH_ERRORS%>'){
		    		if(section1 != undefined){ section1.set('open', true);}
		    		if(section2 != undefined){ section2.set('open', false);}
		    		document.getElementById("applyUpdatesSpan").style.visibility='hidden';
			    	document.getElementById("confirmRejectSpan").style.visibility='hidden';
		        }
			});
	        
	        aspect.after(rgrid, "_onFetchComplete", function(){
		        if(validationStatus == '<%=TradePortalConstants.PYMT_UPLOAD_VALIDATED_AWAITING_REPAIR%>'){
					if(section1 != undefined){ section1.set('open', false);}
					if(section2 != undefined){ section2.set('open', true);}
					document.getElementById("applyUpdatesSpan").style.visibility='visible';
			    	document.getElementById("confirmRejectSpan").style.visibility='visible';
			    }else{
			    	document.getElementById("applyUpdatesSpan").style.visibility='hidden';
			    	document.getElementById("confirmRejectSpan").style.visibility='hidden';
			    }
	        });
		  });
    });
    
    function performUpdate(){
    	var formName = 'PaymentUploadLogDetailForm';   
        var buttonName = '<%=TradePortalConstants.BUTTON_APPLY_UPDATES_REPORTING_CODES%>';
        var rowKeys = getSelectedGridRowKeys("CashMgmtPaymentFileRepCodeErrorsDataGridId");
        submitFormWithParms(formName, buttonName, "checkbox", rowKeys);
    }
  </script>
<% 
  } 
%>
	
<jsp:include page="/common/dGridShowCountFooter.jsp">
  <jsp:param name="gridId" value="CashMgmtPaymentFileUploadErrorsDataGridId" />
</jsp:include>

<jsp:include page="/common/gridShowCountFooter.jsp">
  <jsp:param name="gridId" value="CashMgmtPaymentFileRepCodeErrorsDataGridId" />
</jsp:include>

</body>
</html>

<%
   // Finally, reset the cached document to eliminate carryover of
   // information to the next visit of this page.
   xmlDoc = formMgr.getFromDocCache();

   xmlDoc.removeAllChildren("/");

   formMgr.storeInDocCache("default.doc", xmlDoc);
%>
