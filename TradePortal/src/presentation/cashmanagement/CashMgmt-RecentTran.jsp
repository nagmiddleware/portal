<%--
*******************************************************************************
                               Cash Mgmt Authorized List View

  Description:  **ToDo**



*******************************************************************************
--%>
<%--
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,com.ams.tradeportal.agents.*,
                 com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.busobj.*,com.ams.tradeportal.html.*, java.util.*" %>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session">
</jsp:useBean>
<%
   String accountNo = null;
   String currency = null;
   DocumentHandler result = null;
   String userOID = userSession.getUserOid();
   String client_bankOID = userSession.getClientBankOid();
   String corp_oid = userSession.getOwnerOrgOid();
   String acct_oid = null;
   String oid =StringFunction.xssCharsToHtml(request.getParameter("oid"));

   //cquinton 1/18/2013 add page to page flow
   userSession.addPage("goToRecentTransaction", request);

   String serverLocation = formMgr.getServerLocation();
   if (request.getParameter("oid") != null) {
	   acct_oid = EncryptDecrypt.decryptStringUsingTripleDes(request.getParameter("oid"), userSession.getSecretKey());
   }

   boolean errorFound = false;
   MediatorServices medService = new MediatorServices();
   try
      {
        // Call the mediator
    ClientBank clientBank = (ClientBank)EJBObjectFactory.createClientEJB(formMgr.getServerLocation(), "ClientBank");
    clientBank.getDataFromReferenceCache(Long.parseLong(client_bankOID));

  	CorporateOrganization corporateOrganization = (CorporateOrganization)
                     EJBObjectFactory.createClientEJB(formMgr.getServerLocation(), "CorporateOrganization",Long.parseLong(corp_oid));

  	Account account = (Account)
                      EJBObjectFactory.createClientEJB(formMgr.getServerLocation(), "Account",Long.parseLong(acct_oid));


	  accountNo = account.getAttribute("account_number");
      currency = account.getAttribute("currency");

  	  AcctTransactionQuery tran_query = new AcctTransactionQuery();
  	  result = tran_query.queryTransaction(userOID,clientBank,corporateOrganization,account);

	  medService.getErrorManager().setLocaleName(resMgr.getResourceLocale());
      if (result == null || (result !=null && result.getFragment("/Status")!=null)) {
    	  medService.getErrorManager().issueError(
                  TradePortalConstants.ERR_CAT_1,
                  TradePortalConstants.RECENT_TRANS_NOT_AVAILABLE);
    	  errorFound = true;
      }
      else if (result.getFragments("/ResultSetRecord").size() < 1) {
    	  medService.getErrorManager().issueError(
                  TradePortalConstants.ERR_CAT_1,
                  TradePortalConstants.NO_RECENT_TRANS_RETURNED);
    	  errorFound = true;
      }
     }
   catch (RuntimeException ex) {
	   if (ex.getMessage().equals(TradePortalConstants.MISSING_REQUIRED_FIELDS)){
		   medService.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1, TradePortalConstants.ACCT_TRAN_QYERY_MISSING_FIELD);
		   errorFound = true;
	   }
   }
     catch(Exception e)
      {
	   System.out.println("Error calling queryTransaction");
	   e.printStackTrace();
    }
     if (result == null || errorFound) {
   	  DocumentHandler errDoc = formMgr.getFromDocCache();
   	    medService.addErrorInfo();
   	    errDoc.setComponent("/Error", medService.getErrorDoc());
   	    formMgr.storeInDocCache("default.doc", errDoc);
   	    result = new DocumentHandler();
      }
    String xmlStr = result.toString();
    //String xmlStr = "<DocRoot><ResultSetRecord ID=\"0\"><ACCTTRNID>9000007</ACCTTRNID><DATEPOSTED>2007-12-31 00:00:00.0</DATEPOSTED><DESCRIPTION>check 3 deposit</DESCRIPTION><RUNNINGBAL>23123.55</RUNNINGBAL><AMOUNT>903</AMOUNT><CURRENCYCODE>USD</CURRENCYCODE></ResultSetRecord><ResultSetRecord ID=\"1\"><ACCTTRNID>9000008</ACCTTRNID><DATEPOSTED>2007-11-31 00:00:00.0</DATEPOSTED><DESCRIPTION>check 2 deposit</DESCRIPTION><RUNNINGBAL>22</RUNNINGBAL><AMOUNT>802.11</AMOUNT><CURRENCYCODE>USD</CURRENCYCODE></ResultSetRecord><ResultSetRecord ID=\"2\"><ACCTTRNID>9000009</ACCTTRNID><DATEPOSTED>2007-10-31 00:00:00.0</DATEPOSTED><DESCRIPTION>check 1 deposit</DESCRIPTION><RUNNINGBAL>21</RUNNINGBAL><AMOUNT>901</AMOUNT><CURRENCYCODE>USD</CURRENCYCODE></ResultSetRecord></DocRoot>";
    String userSecurityRights = userSession.getSecurityRights();
    String userSecurityType = userSession.getSecurityType();
    String userTimeZone = userSession.getTimeZone();
    String userLocale = userSession.getUserLocale();

    String clientBankOid = userSession.getClientBankOid();
	QueryListView queryListView   = null;

	queryListView = (QueryListView) EJBObjectFactory.createClientEJB(formMgr.getServerLocation(), "QueryListView");
	queryListView.setSQL("select allow_pay_by_another_accnt from client_bank where organization_oid = ? ", clientBankOid);
	queryListView.getRecords();

	DocumentHandler result2 = queryListView.getXmlResultSet();
	String allowPayByAnotherAccnt = result2.getAttribute("/ResultSetRecord(0)/ALLOW_PAY_BY_ANOTHER_ACCNT");


    String pOid = null;
	StringBuilder sqlQueryAcc = new StringBuilder();
    queryListView = null;
    List<Object> sqlParams = new ArrayList<Object>();
    if (userSession.hasSavedUserSession())
    {
		sqlQueryAcc.append("select account_oid, a.account_number as AccountNumber from  ACCOUNT a where  a.deactivate_indicator != ? ");
		sqlQueryAcc.append(" and a.p_owner_oid = ? ");
		sqlParams.add("Y");
		sqlParams.add(userSession.getOwnerOrgOid());
    }
    else
    {
		sqlQueryAcc.append("select account_oid, a.account_number as AccountNumber from  ACCOUNT a, USER_AUTHORIZED_ACCT b where   a.account_oid = b.a_account_oid and a.deactivate_indicator != ? ");
		sqlQueryAcc.append(" and b.p_user_oid = ? ");
		sqlParams.add("Y");
		sqlParams.add(userSession.getUserOid());
    }


	 if (allowPayByAnotherAccnt != null) {
		if (TradePortalConstants.INDICATOR_NO.equals(allowPayByAnotherAccnt)){
			sqlQueryAcc.append(" and a.othercorp_customer_indicator != ?");
			sqlParams.add(TradePortalConstants.INDICATOR_YES);
		}
	 }
	queryListView = (QueryListView) EJBObjectFactory.createClientEJB(formMgr.getServerLocation(), "QueryListView");

	queryListView.setSQL(sqlQueryAcc.toString(), sqlParams);
	queryListView.getRecords();

	DocumentHandler inputDoc = queryListView.getXmlResultSet();

	String options = ListBox.createOptionList(inputDoc,"ACCOUNT_OID", "ACCOUNTNUMBER","", userSession.getSecretKey());
%>
<%-- ********************* HTML for page begins here *********************  --%>

<jsp:include page="/common/Header.jsp">
    <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
    <jsp:param name="includeErrorSectionFlag"  value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>

<div class="pageMain">
<div class="pageContent">
<%
  //cquinton 1/18/2013 replace old close action behavior with page flow logic
  SessionWebBean.PageFlowRef backPage = userSession.peekPageBack();
  String returnAction = backPage.getLinkAction();
  String returnParms = backPage.getLinkParametersString();
  returnParms+="&returning=true";
  String closeLink = formMgr.getLinkAsUrl(returnAction,returnParms,response);
  //KMehta - 16 Sep 2014 - Rel9.1 IR-T36000032300 - Change  - Begin
 // String linkHtm = "<a href='"+closeLink+"'>"+resMgr.getText("SecondaryNavigation.AccountBalances", TradePortalConstants.TEXT_BUNDLE)+"</a>";
  String linkHtm = resMgr.getText("SecondaryNavigation.AccountBalances", TradePortalConstants.TEXT_BUNDLE);
  String title = resMgr.getText("CMRecentTransactions.AccountNumber", TradePortalConstants.TEXT_BUNDLE)+" "+linkHtm; 
%>
<jsp:include page="/common/PageHeader.jsp">
   <jsp:param name="titleKey" value="<%=title%>"/>
   <jsp:param name="linkAction" value="<%=closeLink%>"/>
   <jsp:param name="ignoreValidation" value="Y"/>
   <jsp:param name="helpUrl"  value="customer/recent_transaction.htm"/><%-- .htm help file is missing --%>
</jsp:include>
    <%-- #KMehta - 16 Sep 2014 - Rel9.1 IR-T36000032300 - Change  - End --%>
<form name="RecentTransactionForm" method="POST" action="<%= formMgr.getSubmitAction(response) %>">
<jsp:include page="/common/ErrorSection.jsp" />
  <div  class="formContentNoSidebar">
    <input type=hidden value="" name=buttonName>

<%
  DGridFactory dgFactory = new DGridFactory(resMgr, userSession, formMgr, response);
  String gridHtml = dgFactory. createDataGrid("recentTransactionDataGrid","RecentTransactionDataGrid",null);
  String gridLayout = dgFactory.createGridLayout("recentTransactionDataGrid","RecentTransactionDataGrid");
  WidgetFactory widgetFactory = new WidgetFactory(resMgr);
%>

    <%--cquinton 10/8/2012 include gridShowCounts --%>
    <div class="gridSearch">
      <div class="searchHeader">
        <span class="searchHeaderCriteria">

          <%=widgetFactory.createSearchSelectField("acct_oid", "CMRecentTransactions.account",accountNo, options, "onChange=\"searchRecentTran();\"")%>

        </span>
        <span class="searchHeaderActions">
          <jsp:include page="/common/dGridShowCount.jsp">
            <jsp:param name="gridId" value="recentTransactionDataGrid" />
          </jsp:include>

          <span id="recentTransRefresh" class="searchHeaderRefresh"></span>
	  <%=widgetFactory.createHoverHelp("recentTransRefresh","RefreshImageLinkHoverText") %>
        </span>
        <div style="clear:both;"></div>
      </div>
    </div>

    <%=gridHtml %>
  </div>
</form>

</div>
</div>

<%--cquinton 3/16/2012 Portal Refresh - start--%>
<jsp:include page="/common/Footer.jsp">
   <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_NO%>" />
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>
<script type="text/javascript">
function searchRecentTran() {
    require(["dojo/dom", "t360/OnDemandGrid", "dojo/dom-construct"],
      function(dom, onDemandGrid, domConstruct){
       <%--  var acct_oid=(dom.byId("acct_oid").value).toUpperCase(); --%>
        var acct_oid = dijit.byId("acct_oid").attr('value');
        console.log("acct_oid: " + acct_oid);
        <%-- var filterText=filterTextCS.toUpperCase(); --%>
        var searchParms = "&acct_oid="+acct_oid;
        console.log("SearchParms: " + searchParms);
        onDemandGrid.searchDataGrid("recentTransactionDataGrid", "RecentTransactionDataView",
                       searchParms);
      });
  }
require(["t360/OnDemandGrid", "dojo/dom-construct"],function(onDemandGrid, domConstruct){
  <%--get grid layout from the data grid factory--%>
  var gridLayout = <%= gridLayout %>;

  var initSearchParms = "oid=<%=oid%>&isEJBCall=Y";

  <%--create the grid, attaching it to the div id specified in dataGrid.jsp--%>
  var viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("RecentTransactionDataView",userSession.getSecretKey())%>";  <%--Rel9.2 IR T36000032596 --%>
  var accountDataGrid =  onDemandGrid.createOnDemandGrid("recentTransactionDataGrid", viewName,gridLayout, initSearchParms,'-6');
  });

	  
  function refresh() {
	    require(["dojo/dom", "t360/OnDemandGrid", "dojo/dom-construct"],
	      function(dom, onDemandGrid, domConstruct){
	    	var initSearchParms = "oid=<%=oid%>&isEJBCall=Y";
	    	onDemandGrid.searchDataGrid("recentTransactionDataGrid", "RecentTransactionDataView",initSearchParms);
	      });
	  }

  require(["dojo/query", "dojo/on", "t360/datagrid", "dojo/domReady!"],
      function(query, on, t360grid, t360popup){
    query('#recentTransRefresh').on("click", function() {
      refresh();
    });
  });


  require(["dijit/registry", "t360/datagrid", "dojo/ready"],
		function(registry,datagrid, ready) {
		ready(function(){
		var myGrid = registry.byId("recentTransactionDataGrid");

		myGrid.canSort=function(idx){  return false; };
		});
});

</script>

<%--cquinton 10/8/2012 include gridShowCounts --%>
<jsp:include page="/common/dGridShowCountFooter.jsp">
  <jsp:param name="gridId" value="recentTransactionDataGrid" />
</jsp:include>

<%--cquinton 3/16/2012 Portal Refresh - end--%>

</body>
</html>

<%
   // Finally, reset the cached document to eliminate carryover of
   // information to the next visit of this page.
   DocumentHandler xmlDoc = formMgr.getFromDocCache();

   xmlDoc.removeAllChildren("/");

   formMgr.storeInDocCache("default.doc", xmlDoc);
%>
