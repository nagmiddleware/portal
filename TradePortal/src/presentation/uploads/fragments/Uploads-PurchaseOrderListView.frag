<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
**********************************************************************************
  Uploads Purchase Order List

  Description:
    Contains HTML to create the Purchase Orders tab for the Transactions Home 
  page.  Actions on this page include: linking to the Purchase Order Upload Log
  page, refreshing the page, linking to the Upload Purchase Order Data page,
  deleting selecting purchase order line items, and executing the create lc
  from po line items process.  Additionally, clicking on a po line item displays
  the detail for that line item.

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transactions-PurchaseOrdersListView.jsp" %>
*******************************************************************************
--%> 


	<div class="gridSearch">
	  <div class="searchHeader">
	    <span class="searchHeaderCriteria">
			<%= widgetFactory.createSubLabel("AddStructurePO.ToFilterText") %>
		</span>	
		<span class="searchHeaderActions">
			<span>
			<button data-dojo-type="dijit.form.Button"  name="ViewLog" id="ViewLog" type="button" >
	    		<%=resMgr.getText("common.ViewLogText",TradePortalConstants.TEXT_BUNDLE)%>
	    		<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
					openURL("<%=formMgr.getLinkAsUrl("goToViewPOUploadLog", response)%>");        					
	 			</script>
	  		</button>
	  		<%=widgetFactory.createHoverHelp("ViewLog", "PurchaseOrders.ViewLogText") %>
	  	
	  		<button data-dojo-type="dijit.form.Button"  name="UploadPOData" id="UploadPOData" type="button" >
	    		<%=resMgr.getText("common.UploadPODataText",TradePortalConstants.TEXT_BUNDLE)%>
	    		<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
					openURL("<%=formMgr.getLinkAsUrl("goToUploadPOData", response)%>");        					
	 			</script>
	  		</button>
	  		<%=widgetFactory.createHoverHelp("UploadPOData", "PurchaseOrders.UploadPOData") %>
	  		</span>
	  		<span class="searchHeaderRefresh" name="RefreshThisPageButton" id="RefreshThisPageButton">
      		</span>
    		<%=widgetFactory.createHoverHelp("RefreshThisPageButton", "RefreshImageLinkHoverText") %>
    		</span>
    		
    		<div style="clear:both;"></div>
    		</div>
    		</div>
    		<div class="subHeaderDivider"></div>
    		<br>
    		<div class="gridSearch">
    		<div class="searchHeader">	
        		<span class="searchHeaderCriteria">
    				<i><%=widgetFactory.createSubLabel("AddPOLineItems.NoteFilterText")%></i>
    		</span>
    		<span class="searchHeaderActions">
    				<jsp:include page="/common/gridShowCount.jsp">
    					<jsp:param name="gridId" value="purchaseOrdersDatagridId" />
    				</jsp:include>	
    		</span>	 
        	<div style="clear:both;"></div>
    		</div>
    	</div>
			
			<br>
		<%@ include file="/transactions/fragments/POSearch.frag" %>

  
		<%
			//cquinton 2/21/2013 - pass beanMgr so conditionalDisplay is initialized
			DataGridFactory dgFactory = new DataGridFactory(resMgr, userSession, formMgr, beanMgr, response);
			String gridHtml = dgFactory.createDataGrid("purchaseOrdersDatagridId","PurchaseOrdersDatagrid",null);
		%>
		<%=gridHtml%>
