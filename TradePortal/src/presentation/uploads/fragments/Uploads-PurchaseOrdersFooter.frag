<%--
*******************************************************************************
  Purchase Orders Footer

  Description: Everything that should be loaded at the end of the page
               should be included here.  This includes non-visible html 
               and javascript that should execute on page load.

*******************************************************************************
--%>
<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>


<%
	DataGridFactory dgFactory = new DataGridFactory(resMgr, userSession, formMgr, response);
	String gridLayout = dgFactory.createGridLayout("purchaseOrdersDatagridId", "PurchaseOrdersDatagrid");
%>

<script type="text/javascript">
  	var gridLayout = <%= gridLayout %>,
  		initSearchParms = "userOrgOid=<%=userOrgOid%>";
  		 var savedSort = savedSearchQueryData["sort"];  
  		require(["dojo/dom"],
  			    function(dom){
  			var poNumber = savedSearchQueryData["poNumber"];   
  			lineItemNumber = savedSearchQueryData["lineItemNumber"];  
  			beneName = savedSearchQueryData["beneName"];  
    		 if("" == poNumber || null == poNumber || "undefined" == poNumber)
    			 poNumber=(dom.byId("PONumber").value).toUpperCase();
    		 else
    			dom.byId("PONumber").value = poNumber;
    		 
    	 	if("" == lineItemNumber || null == lineItemNumber || "undefined" == lineItemNumber)
    	 		lineItemNumber=(dom.byId("LineItemNumber").value).toUpperCase();
      		else
      			dom.byId("LineItemNumber").value = lineItemNumber;
    	 	
    	 	 if("" == beneName || null == beneName || "undefined" == beneName)
    	 		beneName=(dom.byId("BeneName").value).toUpperCase();
    		 else
    			dom.byId("BeneName").value = beneName;
    	 	initSearchParms = initSearchParms+"&poNumber="+poNumber+"&lineItemNumber="+lineItemNumber+"&beneName="+beneName;
  		});
  	var viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("PurchaseOrdersDataview",userSession.getSecretKey())%>";  <%--Rel9.2 IR T36000032596 --%>
  	createDataGrid("purchaseOrdersDatagridId", viewName,gridLayout, initSearchParms,savedSort);
	
  	function filterPOs() {
	    require(["dojo/dom"],
	    function(dom){
	        var poNumber = dom.byId("PONumber").value,
	        	lineItemNumber = dom.byId("LineItemNumber").value,
	        	beneName = dom.byId("BeneName").value,
	        	searchParms = '';

	        searchParms = "userOrgOid=<%=userOrgOid%>&poNumber="+poNumber+"&lineItemNumber="+lineItemNumber+"&beneName="+beneName;
			searchDataGrid("purchaseOrdersDatagridId", "PurchaseOrdersDataview", searchParms);
		});
  	}
  	
  	function deleteSelectedPurchaseOrders(){
		var formName = 'PurchaseOrderListForm',
			buttonName = '<%=TradePortalConstants.BUTTON_DELETE_PO%>',
			rowKeys = getSelectedGridRowKeys("purchaseOrdersDatagridId");
		
		if(rowKeys == ""){
			alert("Please pick record(s) and click 'Delete'");
			return;
		}
		
		var confirmDelete = confirm("<%=resMgr.getText("PendingTransactions.PopupMessage",TradePortalConstants.TEXT_BUNDLE)%>");
		
		if(confirmDelete == true){
			submitFormWithParms(formName, buttonName, "checkbox", rowKeys);
		}
  	}
  	
  	function createATPsUsingRulesText(){
  		var formName = 'PurchaseOrderListForm',
		buttonName = '<%=TradePortalConstants.BUTTON_CREATE_ATP_FROM_PO%>',
		rowKeys = getSelectedGridRowKeys("purchaseOrdersDatagridId");
	
		submitFormWithParms(formName, buttonName, "checkbox", rowKeys);
  	}
  	
  	function createLCsUsingRulesText(){
  		var formName = 'PurchaseOrderListForm',
		buttonName = '<%=TradePortalConstants.BUTTON_CREATE_LC_FROM_PO%>',
		rowKeys = getSelectedGridRowKeys("purchaseOrdersDatagridId");
	
		submitFormWithParms(formName, buttonName, "checkbox", rowKeys);
  	}

  	function openURL(URL){
    if (isActive =='Y') {
    	if (URL != '' && URL.indexOf("javascript:") == -1) {
	        var cTime = (new Date()).getTime();
	        URL = URL + "&cTime=" + cTime;
	        URL = URL + "&prevPage=" + context;
	    }
    }
 		document.location.href  = URL;
 		return false;
	}
  	
  	require(["dojo/query", "dojo/on", "dojo/domReady!"],
  	      function(query, on){
  	    query('#RefreshThisPageButton').on("click", function() {
  	    	filterPOs();
  	    });
  	}); 
</script>
<jsp:include page="/common/gridShowCountFooter.jsp">
<jsp:param name="gridId" value="purchaseOrdersDatagridId" />
</jsp:include>