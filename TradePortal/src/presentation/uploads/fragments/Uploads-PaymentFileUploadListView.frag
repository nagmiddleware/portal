<%--
*******************************************************************************
  Payment File Upload

  Description: List uploaded payments and provide ability to upload here.
  
  

*******************************************************************************
--%>
<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>

<%
		   // Get the default Payment upload definition as defined in the org's preference.
                   String sqlQuery1 = "select payment_definition_oid, name, pmt_file_default_ind from user_preferences_pay_def, payment_definitions"
                   +" where a_pay_upload_definition_oid = payment_definition_oid  and user_preferences_pay_def.p_owner_org_oid = ?  and pmt_file_default_ind = ? ";

                   DocumentHandler payUploadDefinitionDoc  = DatabaseQueryBean.getXmlResultSet(sqlQuery1.toString(), false,userSession.getOwnerOrgOid(), "Y");
                   String defaultDefinition = null;
                   if (payUploadDefinitionDoc != null) {
                       defaultDefinition = payUploadDefinitionDoc.getAttribute("/ResultSetRecord(0)/PAYMENT_DEFINITION_OID");
                   }

                   // Retrieves PaymentUpload Definitions used by the corporate org.
                   // Note get the description of the payment method for the user's locale.
                   String loginLocale = userSession.getUserLocale(); // en_US, en, fr_CA, fr, vi etc
                   String loginLang = loginLocale;
                   if (loginLocale.indexOf("_") > 0) loginLang = loginLocale.substring(0, loginLocale.indexOf("_"));  // en, fr, vi etc.
		   sqlQuery1 = "SELECT   payment_DEFINITION_OID,  (CASE WHEN NAME LIKE '%ABA%' THEN TO_NCHAR(NAME)   ELSE (NAME || ' - ' || ("
           +" SELECT descr FROM refdata WHERE (NOT EXISTS(SELECT LOCALE_NAME, descr FROM refdata WHERE code = payment_method "
           +" AND table_type = ? AND locale_name = ? ) AND locale_name IS NULL AND code = payment_method "
           +" AND table_type = ?) OR EXISTS(SELECT LOCALE_NAME, descr FROM refdata WHERE code = payment_method "
           +" AND table_type = ? AND locale_name = ? ) AND locale_name = ? AND code = payment_method "
           +" AND table_type = ?)) END) NAME, upd.pmt_file_default_ind FROM payment_definitions pd, user_preferences_pay_def upd where pd.payment_definition_oid = upd.a_pay_upload_definition_oid"
           +" and upd.p_owner_org_oid = ?  order by "+resMgr.localizeOrderBy("name");
		   List<Object> sqlParams = new ArrayList<Object>();
		   
		   sqlParams.add("PAYMENT_METHOD");
		   sqlParams.add(resMgr.getResourceLocale());
		   sqlParams.add("PAYMENT_METHOD");
		   sqlParams.add("PAYMENT_METHOD");
		   
		   sqlParams.add(resMgr.getResourceLocale());
		   sqlParams.add(resMgr.getResourceLocale());
		   sqlParams.add("PAYMENT_METHOD");
		   sqlParams.add(userSession.getOwnerOrgOid());

		   payUploadDefinitionDoc  = DatabaseQueryBean.getXmlResultSet(sqlQuery1.toString(), false, sqlParams);

		   if(payUploadDefinitionDoc == null)   payUploadDefinitionDoc = new DocumentHandler();
		   StringBuffer payDefOptions = new StringBuffer();
		   String chooseDefinition = resMgr.getText("LocateUploadINV.SelectUploadDefinition", TradePortalConstants.TEXT_BUNDLE);
		   payDefOptions.append("<option value=\"\">"+chooseDefinition+"</option>");
		   payDefOptions.append(Dropdown.createSortedOptions(payUploadDefinitionDoc, "PAYMENT_DEFINITION_OID",
                                                              "NAME", defaultDefinition !=null ? defaultDefinition : "" , userSession.getSecretKey(), resMgr.getResourceLocale()));

%>
<SCRIPT LANGUAGE="JavaScript">



	var blink_speed=200;
	var i=0;
	
function restrictAttach(form,path) {
  var extArr = new Array(".txt");
  var allowUpload = true; <%--  CR 597 --%>
  var file = form.PaymentDataFilePath.value
    if (!file) {
        <%-- BSL IR NRUL080338698 03/28/2012 - Convert numeric character references to Unicode --%>
        alert('<%=StringFunction.asciiToUnicode(resMgr.getTextEscapedJS(
				"PaymentUploadLogDetail.NoPaymentFileError",
				TradePortalConstants.TEXT_BUNDLE))%>');
        return false;
    }

    <%-- NAR IR T36000011413  - Alert Message if definition not selected--%>
    var filedef = form.paymentDefinitionOid;
    if (filedef){
    	filedef = filedef[0].value;
	    var defaultDesc = '<%=resMgr.getText("LocateUploadINV.SelectUploadINVDefinition", TradePortalConstants.TEXT_BUNDLE)%>';
	    if (!filedef || filedef == defaultDesc) {
	        alert('<%=StringFunction.asciiToUnicode(resMgr.getText(
								"StructuredPOUpload.NoPOFileDefError",
								TradePortalConstants.TEXT_BUNDLE))%>');
	        return false;
	    }
	
	    if (path){
	        var delimiter = "\\";
	        delimiter = (file.lastIndexOf("//") != -1)?"//":"\\";
	        delimiter = (file.lastIndexOf("/") != -1)?"/":"\\";
	
	        if (file.lastIndexOf(delimiter) != -1){
	            var slashIndex = file.lastIndexOf(delimiter) ;
	            file = file.substring(0,slashIndex);
	            newPath = file;
	
	            <%-- if path is not same as restricted path the show up the error --%>
	            if(newPath != path){
	                <%-- BSL IR NRUL080338698 03/28/2012 - Convert numeric character references to Unicode --%>
	                alert('<%=StringFunction.asciiToUnicode(resMgr.getTextEscapedJS(
						"PaymentMgmtUploads.RestrictFilePathErrorText",
						TradePortalConstants.TEXT_BUNDLE))%>');
	                return false;
	            }
	        }
	    }
    }

   if (allowUpload) {
	   form.action ="<%= response.encodeURL(request.getContextPath()+ "/transactions/PaymentFileUploadServlet.jsp") %>";
	   form.enctype="multipart/form-data";
	   form.encoding="multipart/form-data";
	   <%-- IR T36000024019 - Rpasupulati --%>
	  <%--  document.getElementById('UploadButtonDiv').style.visibility = 'hidden'; --%>
	  <%-- document.getElementById('UploadButtonDiv').style.display = 'none';	 --%>
	  	
	   return true;
   }
   else {
       alert('<%= resMgr.getTextEscapedJS("PaymentUploadLogDetail.ValidFileExtionsionText", TradePortalConstants.TEXT_BUNDLE) %>');
       return false;
   }
}

function Blink(layerName){

		
			if(i%2==0) {
				eval("document.all"+'["'+layerName+'"]'+
				'.style.visibility="visible"');
			} else {
 				eval("document.all"+'["'+layerName+'"]'+
				'.style.visibility="hidden"');
			}
		 
 		if(i<1) {
			i++;
		} else {
			i--
		}
 		setTimeout("Blink('"+layerName+"')",blink_speed);
	}

</script>

 <%=widgetFactory.createWideSubsectionHeader("LocateUploadPaymentFile.Step1") %>
 <br>

 	
		<div class="formItem"> 
				<%= widgetFactory.createFileField("PaymentDataFilePath", "","class='char40'","", false) %>
 	      	</div>

<% if (TradePortalConstants.PAYMENT_MGMT_FILE_DEF.equals(corpOrg.getAttribute("man_upld_pmt_file_frmt_ind"))) 
{ %>

          <%= widgetFactory.createWideSubsectionHeader("LocateUploadPaymentFile.Step2") %>
          <br>
     <div class="formItem">
 	      <%= widgetFactory.createSelectFieldInline("paymentDefinitionOid", "","", payDefOptions.toString(), false,false,false,"onChange='setManagementCheck();' class='char35em'","","")%>
 	   	  </div>
<% } %>	
     <div style="clear:both;"></div>
                 
       
  <%-- cquinton 10/15/2012 adding Show Counts - use standard search headers as well --%>
  <div class="searchDivider"></div>

  <div class="searchHeader">
    <span class="searchHeaderCriteria">
        
<%
        StringBuffer prependText = new StringBuffer();
		String defaultValue = "";
		String defaultText  = "";
        prependText.append(resMgr.getText("PaymentFileUpload.WorkFor", 
                                           TradePortalConstants.TEXT_BUNDLE));
        prependText.append(" ");

        if(!userSession.hasSavedUserSession())
	     { 
	      defaultValue = EncryptDecrypt.encryptStringUsingTripleDes(MY_WORK, userSession.getSecretKey());
	       defaultText = MY_WORK;
	       
	       dropdownOptions.append("<option value=\"");
	       dropdownOptions.append(defaultValue);
	       dropdownOptions.append("\"");
	       
	       if (selectedWorkflow.equals(defaultText))
	       {
	          dropdownOptions.append(" selected");
	       }
	
	       dropdownOptions.append(">");
	       dropdownOptions.append(resMgr.getText( defaultText, TradePortalConstants.TEXT_BUNDLE));
	       dropdownOptions.append("</option>");
	     }
        // If the user has rights to view child organization work, retrieve the 
        // list of dropdown options from the query listview results doc (containing
        // an option for each child org) otherwise, simply use the user's org 
        // option to the dropdown list (in addition to the default 'My Work' option).

        if (SecurityAccess.hasRights(userSecurityRights, 
                                     SecurityAccess.VIEW_CHILD_ORG_WORK))
        {
           dropdownOptions.append(Dropdown.getUnsortedOptions(hierarchyDoc,"ORGANIZATION_OID", "NAME", prependText.toString(),selectedWorkflow,
        		                 userSession.getSecretKey()));

           // Only display the 'All Work' option if the user's org has child organizations
           if (totalOrganizations > 1)
           {
              dropdownOptions.append("<option value=\"");
              dropdownOptions.append(EncryptDecrypt.encryptStringUsingTripleDes(ALL_WORK, userSession.getSecretKey()));
              dropdownOptions.append("\"");

              if (selectedWorkflow.equals(ALL_WORK))
              {
                 dropdownOptions.append(" selected");
              }

              dropdownOptions.append(">");
              dropdownOptions.append(resMgr.getText(ALL_WORK,TradePortalConstants.TEXT_BUNDLE));
              dropdownOptions.append("</option>");
           }
        }
        else
        {
           dropdownOptions.append("<option value=\"");
           dropdownOptions.append(EncryptDecrypt.encryptStringUsingTripleDes(userOrgOid, userSession.getSecretKey()));
           dropdownOptions.append("\"");

           if (selectedWorkflow.equals(userOrgOid))
           {
              dropdownOptions.append(" selected");
           }

           dropdownOptions.append(">");
           dropdownOptions.append(prependText.toString());
           dropdownOptions.append(userSession.getOrganizationName());
           dropdownOptions.append("</option>");
        }

        extraTags.append("onchange=\"location='");
        extraTags.append(formMgr.getLinkAsUrl("goToUploadsHome", response));
        extraTags.append("&amp;current2ndNav=");
        extraTags.append(TradePortalConstants.NAV__PAYMENT_FILE_UPLOAD);
        extraTags.append("&amp;workflow='+this.options[this.selectedIndex].value\"");

       

        /*if(!userSession.hasSavedUserSession())
         {
           defaultValue = EncryptDecrypt.encryptStringUsingTripleDes(MY_WORK, userSession.getSecretKey());
           defaultText = resMgr.getText( MY_WORK, TradePortalConstants.TEXT_BUNDLE);
         }*/
        

     out.print(widgetFactory.createSearchSelectField("Workflow","PaymentFileUpload.Show","",dropdownOptions.toString(),
    		 "onChange='searchPaymentUploads();'")); 
    
%>
    </span>

<%
         newLink.append(formMgr.getLinkAsUrl("goToUploadsHome", response));
         newLink.append("&current2ndNav=");
         newLink.append(TradePortalConstants.NAV__PAYMENT_FILE_UPLOAD);
         newLink.append("&workflow=");
         newLink.append(EncryptDecrypt.encryptStringUsingTripleDes(selectedWorkflow, userSession.getSecretKey()));
%>
    <span class="searchHeaderActions">
      <jsp:include page="/common/gridShowCount.jsp">
        <jsp:param name="gridId" value="paymentFileUploadGrid" />
      </jsp:include>

      <span id="pmtUploadRefresh" class="searchHeaderRefresh"></span>
	  <%=widgetFactory.createHoverHelp("pmtUploadRefresh","RefreshImageLinkHoverText") %> 
    </span>
    <div style="clear:both;"></div>
  </div>
 
<%
  DataGridFactory dgFactory = new DataGridFactory(resMgr, userSession, formMgr, response);
  String gridHtml = dgFactory.createDataGrid("paymentFileUploadGrid","PaymentFileUploadDataGrid",null);
%>
<%=gridHtml%>
