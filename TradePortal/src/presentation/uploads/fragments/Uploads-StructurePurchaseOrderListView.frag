<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
**********************************************************************************
  Uploads Purchase Order List

  Description:
    Contains HTML to create the Purchase Orders tab for the Transactions Home 
  page.  Actions on this page include: linking to the Purchase Order Upload Log
  page, refreshing the page, linking to the Upload Purchase Order Data page,
  deleting selecting purchase order line items, and executing the create lc
  from po line items process.  Additionally, clicking on a po line item displays
  the detail for that line item.

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transactions-PurchaseOrdersListView.jsp" %>
*******************************************************************************
--%> 


	<div class="gridSearch">
		<div class="searchHeader">	
    		<span class="searchHeaderCriteria">
				<%=widgetFactory.createSubLabel("AddStructurePO.ToFilterText")%>
		</span>
		<span class="searchHeaderActions">
				<jsp:include page="/common/gridShowCount.jsp">
					<jsp:param name="gridId" value="structurePurchaseOrdersDatagridId" />
				</jsp:include>	
			  <span class="searchHeaderRefresh" name="RefreshPOPageButton" id="RefreshPOPageButton">
      			</span>
    		 <%=widgetFactory.createHoverHelp("RefreshPOPageButton", "RefreshImageLinkHoverText") %>
    	</span>	 
    	<div style="clear:both;"></div>
		</div>
	</div>
		<div class="subHeaderDivider"></div>
		<br>
		<%@ include file="/transactions/fragments/POStructuredSearch.frag" %>

  
		<%
			//cquinton 2/21/2013 - pass beanMgr so conditionalDisplay is initialized
			DataGridFactory dgFactory = new DataGridFactory(resMgr, userSession, formMgr, beanMgr, response);
			String gridHtml = dgFactory.createDataGrid("structurePurchaseOrdersDatagridId","StructurePurchaseOrdersDatagrid",null);
		%>
		<%=gridHtml%>
