
<%--
*******************************************************************************
  Uploads Home Page

  Description:
     This page is the uploads main page.

*******************************************************************************
--%>

<%--
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
--%>


<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*, com.ams.tradeportal.html.*,
                  java.util.*"%>

<jsp:useBean id="beanMgr"     class="com.amsinc.ecsg.web.BeanManager"            scope="session"></jsp:useBean>
<jsp:useBean id="formMgr"     class="com.amsinc.ecsg.web.FormManager"            scope="session"></jsp:useBean>
<jsp:useBean id="resMgr"      class="com.amsinc.ecsg.util.ResourceManager"       scope="session"></jsp:useBean>
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>


<%-- ********************* JavaScript for page begins here *********************  --%>


<%-- ************** Data retrieval page setup begins here ****************  --%>

<%
	WidgetFactory widgetFactory = new WidgetFactory(resMgr);
   StringBuffer      onLoad             = new StringBuffer();
   Hashtable         secureParms        = new Hashtable();
   String            helpSensitiveLink  = null;
   String            current2ndNav      = null;
   String            formName           = null;
   String            userOrgOid         = userSession.getOwnerOrgOid();
   String            userOid            = userSession.getUserOid();
   String            userSecurityRights = userSession.getSecurityRights();
   String            userSecurityType   = userSession.getSecurityType();
   StringBuffer      newLink            = new StringBuffer();

   //purchase order vars
   String poNumber = "";
   String lineItemNumber = "";
   String beneName = "";
   boolean canProcessPOForDLC = SecurityAccess.canProcessPurchaseOrder(userSecurityRights, InstrumentType.IMPORT_DLC);
   boolean canProcessPOForATP = SecurityAccess.canProcessPurchaseOrder(userSecurityRights, InstrumentType.APPROVAL_TO_PAY);
   String userOrgAutoLCCreateIndicator = userSession.getOrgAutoLCCreateIndicator();
   String userOrgManualPOIndicator     = userSession.getOrgManualPOIndicator();
   String userOrgAutoATPCreateIndicator = userSession.getOrgAutoATPCreateIndicator();
   String userOrgATPManualPOIndicator   = userSession.getOrgATPManualPOIndicator();

   // DK Rel8.2 IR T36000011671/T36000011677/T36000012151/T36000012149 02/25/2013 Starts
   boolean canProcessInvoiceForATP = SecurityAccess.canProcessATPInvoice(userSecurityRights, InstrumentType.APPROVAL_TO_PAY);
   boolean canProcessLRQInvoice = SecurityAccess.canProcessLRQInvoice(userSecurityRights, InstrumentType.LOAN_RQST);
   // DK Rel8.2 IR T36000011671/T36000011677/T36000012151/T36000012149 02/25/2013 Ends

    final String ALL_WORK = "common.allWork";
   final String MY_WORK = "common.myWork";
   DocumentHandler   hierarchyDoc       = null;
   String            selectedWorkflow   = null;
   int               totalOrganizations = 0;
   StringBuffer      dropdownOptions    = new StringBuffer();
   StringBuffer      extraTags          = new StringBuffer();
   StringBuffer      newUploadLink      = new StringBuffer();
   String            userDefaultWipView = userSession.getDefaultWipView();
   String helpText = "/customer/payment_file_upload.htm";
   String helpSensitiveLink1 =   OnlineHelp.createContextSensitiveLink(helpText,  resMgr, userSession);
	

	String recInvLoanType = "";
	String payInvLoanType = "";
	String invPayUploadDataOption = null;
	String paymentDataFilePath = null;
	String loanType = null;
	String invoiceDefinitionName = null;
	String invItemsGroupingOption = null;
	String invUploadInstrOption = null;
	
	DocumentHandler   doc             = formMgr.getFromDocCache();
	boolean errorFound = String.valueOf(ErrorManager.ERROR_SEVERITY).equals(doc.getAttribute("/In/maxErrorSeverity"));
	if (errorFound){
		invPayUploadDataOption = doc.getAttribute("/In/InvPayUploadDataOption");
		paymentDataFilePath =doc.getAttribute("/In/PaymentDataFilePath");
		loanType =doc.getAttribute("/In/loanType");
		invoiceDefinitionName = doc.getAttribute("/In/invoiceDefinitionName");
		invItemsGroupingOption=doc.getAttribute("/In/InvItemsGroupingOption");
		invUploadInstrOption = doc.getAttribute("/In/InvUploadInstrOption");
	}
	
	//Rel 9.4 CR-1001 START
	if ( (doc != null && doc.getAttribute("/Out/forwardToLogDetail") != null) &&
			 (doc.getAttribute("/Out/forwardToLogDetail").equals(TradePortalConstants.INDICATOR_YES) ) )
	{
		String detailsPage = NavigationManager.getNavMan().getPhysicalPage("PaymentUploadLogDetail", request);
		 %>
		   <jsp:forward page='<%= detailsPage%>' />
		 <%
	}
	//Rel 9.4 CR-1001 END
   // Set the current primary navigation
   userSession.setCurrentPrimaryNavigation("NavigationBar.Transactions");

   //cquinton 1/18/2013 set return page
   if ( "true".equals( request.getParameter("returning") ) ) {
     userSession.pageBack(); //to keep it correct
   }
   else {
     userSession.addPage("goToUploadsHome");
   }

 //IR T36000026794 Rel9.0 07/24/2014 starts
   session.setAttribute("startHomePage", "UploadHome");
   session.removeAttribute("fromTPHomePage");
  //IR T36000026794 Rel9.0 07/24/2014 End
  
   //cquinton 1/18/2013 remove old close action behavior

   CorporateOrganizationWebBean corpOrg = beanMgr.createBean(CorporateOrganizationWebBean.class, "CorporateOrganization");
   corpOrg.getById(userOrgOid);
   //CR-586 Vshah [BEGIN]
   UserWebBean thisUser = beanMgr.createBean(UserWebBean.class, "User");
   thisUser.setAttribute("user_oid", userOid);
   thisUser.getDataFromAppServer();

   userSession.setCurrentPrimaryNavigation("NavigationBar.UploadCenter");

   current2ndNav = request.getParameter("current2ndNav");
   if (current2ndNav == null)
   {
      current2ndNav = (String) session.getAttribute("uploads2ndNav");

      if (current2ndNav == null)
      {
       current2ndNav = TradePortalConstants.NAV__PAYMENT_FILE_UPLOAD;
      }
   }


   // Now perform data setup for whichever is the current tab.  Each block of
   // code sets tab specific parameters (for data retrieval or dropdowns) as
   // well as setting common things like the appropriate help link and formName


   // Set the performance statistic object to have the current tab as its context
    PerformanceStatistic perfStat = (PerformanceStatistic) session.getAttribute("performanceStatistic");

   if((perfStat != null) && (perfStat.isLinkAction()))
           perfStat.setContext(current2ndNav);

   Map searchQueryMap =  userSession.getSavedSearchQueryData();
   Map savedSearchQueryData =null; 
   // ***********************************************************************
   // Data setup for the Purchase Order tab
   // ***********************************************************************
   if (TradePortalConstants.NAV__PURCHASE_ORDERS.equals(current2ndNav))
   {
      formName           = "PurchaseOrderListForm";

     
      helpSensitiveLink = OnlineHelp.createContextSensitiveLink("customer/purchase_orders.htm",  resMgr, userSession);

      String newSearchTrans = request.getParameter("NewSearch");


      if(searchQueryMap!=null && !searchQueryMap.isEmpty()){
			 savedSearchQueryData = (Map)searchQueryMap.get("PurchaseOrdersDataview");
		}
   } //end purchase orders list

   if (TradePortalConstants.NAV__STRUCTURE_PURCHASE_ORDER.equals(current2ndNav))
   {
	   formName           = "PurchaseOrderStructuredListForm";
      helpSensitiveLink = OnlineHelp.createContextSensitiveLink("customer/purchase_orders.htm",  resMgr, userSession);
      if(searchQueryMap!=null && !searchQueryMap.isEmpty()){
    	  savedSearchQueryData = (Map)searchQueryMap.get("StructurePurchaseOrdersDataview");
		}
   }

   // ***********************************************************************
   // Data setup for the payment file upload 2nd nav
   // ***********************************************************************
   if ( TradePortalConstants.NAV__PAYMENT_FILE_UPLOAD.equals(current2ndNav) || TradePortalConstants.NAV__INVOICE_FILE_UPLOAD.equals(current2ndNav))
   {

	  if (TradePortalConstants.NAV__INVOICE_FILE_UPLOAD.equals(current2ndNav)){
	      formName = "InvoiceUploadsForm";
          helpSensitiveLink = OnlineHelp.createContextSensitiveLink("customer/upload_invoice.htm",  resMgr, userSession);
	  }else{
		  formName = "PaymentFileUploadForm";
	      helpSensitiveLink = OnlineHelp.createContextSensitiveLink("customer/payment_file_upload.htm",  resMgr, userSession);
	  }

      // This is the query used for populating the workflow drop down list; it retrieves
      // all active child organizations that belong to the user's current org.
      String sqlQuery = "select organization_oid, name from corporate_org where activation_status = ?  start with organization_oid = ?"
      +" connect by prior organization_oid = p_parent_corp_org_oid order by "+resMgr.localizeOrderBy("name");

      hierarchyDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery, false, TradePortalConstants.ACTIVE, userOrgOid);

      Vector orgListDoc = hierarchyDoc.getFragments("/ResultSetRecord");
      totalOrganizations = orgListDoc.size();


      // Based on the statusType dropdown, build the where clause to include one or more
      // statuses.
      Vector statuses = new Vector();
      int numStatuses = 0;


      // Based on the workflow dropdown, build the where clause to include transactions at
      // various levels of the organization.
      selectedWorkflow = request.getParameter("workflow");

      if (selectedWorkflow == null)
      {
          if (userDefaultWipView.equals(TradePortalConstants.WIP_VIEW_MY_ORG))
          {
             selectedWorkflow = userOrgOid;
          }
          else if (userDefaultWipView.equals(TradePortalConstants.WIP_VIEW_MY_ORG_CHILDREN))
          {
             if(totalOrganizations > 1){
            	 selectedWorkflow = ALL_WORK;
             }else{
            	 selectedWorkflow = userOrgOid;
             }             
          }
          else
          {
             selectedWorkflow = MY_WORK;
          }
          selectedWorkflow = EncryptDecrypt.encryptStringUsingTripleDes(selectedWorkflow, userSession.getSecretKey());
      }      

      session.setAttribute("workflow", selectedWorkflow);

      selectedWorkflow = EncryptDecrypt.decryptStringUsingTripleDes(selectedWorkflow, userSession.getSecretKey());


      onLoad.append("document.TransactionListForm.Workflow.focus();");

   } // End data setup for Payment File Upload Tab

   Debug.debug("form " + formName);
   String searchNav = "uploadHome."+current2ndNav; 
%>

	<%-- ********************* HTML for page begins here *********************  --%>

	<jsp:include page="/common/Header.jsp">
	   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
	   <jsp:param name="includeErrorSectionFlag"  value="<%=TradePortalConstants.INDICATOR_YES%>" />
	   <jsp:param name="additionalOnLoad"         value="<%=onLoad.toString()%>" />
	</jsp:include>

	<%-- Vshah - 08/02/2012 - Rel8.1 - Portal-refresh --%>
	<%--  including TransactionCleanup.jsp to remove lock on instrument which has been placed by this user... --%>
	<%--  this is required because (with Portal-refresh) user can navigate anywhere from MenuBar when on Instrument(detail) page. --%>
	<jsp:include page="/transactions/TransactionCleanup.jsp"></jsp:include>

<jsp:include page="/common/SearchCriteriaCleanup.jsp">
<jsp:param name="searchCriteriaSection" value="<%=searchNav%>"/>
</jsp:include>

	<%--note although uploads are shared, the secondary navigation section
	    are just heading for each of the pages rather than links to other upload pages
	    so we just include on the specific page.
	--%>

	<div class="pageMainNoSidebar">
		<div class="pageContent">

<%
   if ( TradePortalConstants.NAV__PAYMENT_FILE_UPLOAD.equals(current2ndNav) ) {
%>
    <%--W Zhu 9 Jan 2013 Rel 8.4 CR-599 BEGIN --%>
    <%
	   String path = corpOrg.getAttribute("restricted_pay_mgmt_upload_dir"); //todo; replace with payment
	   if(path!=null){
		path =path.replaceAll("\\\\","\\\\\\\\");
	   }else{
		   path ="";
	   }
    %>
	<%
		helpText = "/customer/payment_file_upload.htm";
      String helpSensitiveLink2 =
        OnlineHelp.createContextSensitiveLink(helpText,  resMgr, userSession);
	%>
	<div class="subHeaderDivider"></div>
	
	<div class="secondaryNav">
	
	 <span class="secondaryNavTitle">
		 <%=resMgr.getText("SecondaryNavigation.PaymentFileUpload", TradePortalConstants.TEXT_BUNDLE)%>
	 </span>
	 <span class="secondaryNavHelp">
	  <%-- Srinivasu_D IR#T36000024165 Rel8.4 - Start - alignment issue --%>
	
  <table bgcolor='' width="100%">
	<tr><td style='vertical-align: center'>
       <button data-dojo-type="dijit.form.Button" type="button" class="" id="UploadButtonDiv" name="Upload">
          <%=resMgr.getText("common.UploadFileText", TradePortalConstants.TEXT_BUNDLE)%>
          <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
			
            if ( restrictAttach(document.forms[0],'<%= path %>')) {
					document.forms[0].submit();
            }
          </script>
        </button>
        <%=widgetFactory.createHoverHelp("Upload", "LocateUploadPO.UploadFile") %>
        <button data-dojo-type="dijit.form.Button" name="CloseButton" id="CloseButton" type="button" class="">
          <%=resMgr.getText("common.CloseText",TradePortalConstants.TEXT_BUNDLE)%>
          <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
            document.location.href =
              "<%= formMgr.getLinkAsUrl("goToTradePortalHome", response) %>";
          </script>
        </button>
	
        <%=widgetFactory.createHoverHelp("CloseButton", "CloseHoverText") %>
		</td><td class='secondaryNavHelp'>
		  <%=helpSensitiveLink2%>
		
		<%=widgetFactory.createHoverHelp("secondaryNavHelp","HelpImageLinkHoverText") %> 
		</td>
		</tr>
		</table>
		
  </span>
	 <div style="clear:both;"></div>

</div>

	
	
    <%--W Zhu 9 Jan 2013 Rel 8.4 CR-599 BEGIN --%>
<%
   }
   else if (TradePortalConstants.NAV__INVOICE_FILE_UPLOAD.equals(current2ndNav)){
	   //Added by dillip #IR6000015528
	   String path = corpOrg.getAttribute("restricted_inv_upload_dir");
	   if(path!=null){
		path =path.replaceAll("\\\\","\\\\\\\\");
	   }else{
		   path ="";
	   }
	   //Ended Here
%>
			<jsp:include page="/common/PageHeader.jsp">
    			<jsp:param name="titleKey" value="SecondaryNavigation.InvoiceUploadFile" />
    			<jsp:param name="helpUrl" value="/customer/upload_invoice.htm" />
  			</jsp:include>

  		<div class="subHeaderDivider"></div>
    <%--dillip 10th Apr 2013 ir#T36000015528 --%>
    <div class="pageSubHeader">
      <span class="pageSubHeaderItem">
        <%=resMgr.getText("LocateUploadInvoiceFile.Header", TradePortalConstants.TEXT_BUNDLE)%>
      </span>
      <span class="pageSubHeader-right">
        <button data-dojo-type="dijit.form.Button" type="button" class="pageSubHeaderButton" id="UploadButtonDiv" name="Upload">
          <%=resMgr.getText("common.UploadFileText", TradePortalConstants.TEXT_BUNDLE)%>
          <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
			
            if ( restrictAttach(document.forms[0],'<%= path %>')) {
				

					document.forms[0].submit();

            }
          </script>
        </button>
        <%=widgetFactory.createHoverHelp("Upload", "LocateUploadPO.UploadFile") %>
        <button data-dojo-type="dijit.form.Button" name="CloseButton" id="CloseButton" type="button" class="pageSubHeaderButton">
          <%=resMgr.getText("common.CloseText",TradePortalConstants.TEXT_BUNDLE)%>
          <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
<%--  DK IR T36000016886 Rel8.2 05/12/2013 starts --%>
            document.location.href =
              "<%= formMgr.getLinkAsUrl("goToTradePortalHome", response) %>";
<%--  DK IR T36000016886 Rel8.2 05/12/2013 ends --%>
          </script>
        </button>
        <%=widgetFactory.createHoverHelp("CloseButton", "CloseHoverText") %>
      </span>
      <div style="clear:both;"></div>
    </div>

<%--Ended Here #T36000015528 --%>
<%
   }
   else if (TradePortalConstants.NAV__PURCHASE_ORDERS.equals(current2ndNav) ||
		            TradePortalConstants.NAV__STRUCTURE_PURCHASE_ORDER.equals(current2ndNav)){
%>
			<jsp:include page="/common/PageHeader.jsp">
    			<jsp:param name="titleKey" value="SecondaryNavigation.PurchaseOrders" />
    			<jsp:param name="item1Key" value="SecondaryNavigation.PurchaseOrders.POList" />
    			<jsp:param name="helpUrl" value="/customer/purchase_orders.htm" />
  			</jsp:include>

<%
   }
%>

			<form id="<%=formName%>" name="<%=formName%>" method="POST" data-dojo-type="dijit.form.Form" action="<%= formMgr.getSubmitAction(response) %>">
  				<jsp:include page="/common/ErrorSection.jsp" />
  				<div class="formContentNoSidebar">

  					<input type=hidden value="" name=buttonName>

				<%
					if ( TradePortalConstants.NAV__PAYMENT_FILE_UPLOAD.equals(current2ndNav) )
					{
					   // Store the user's oid and security rights in a secure hashtable for the form
					   secureParms.put("SecurityRights", userSecurityRights);
					   secureParms.put("UserOid",        userOid);
					}
				%>

				<%
					// Based on the current tab, display the appropriate HTML for that tab
					if (TradePortalConstants.NAV__PURCHASE_ORDERS.equals(current2ndNav))
					{
					      secureParms.put("SecurityRights",   userSecurityRights);
					      secureParms.put("UserOid",          userOid);
					      secureParms.put("corporateOrgOid",  userOrgOid);
					      secureParms.put("timeZone",         userSession.getTimeZone());
					      secureParms.put("userLocale",       userSession.getUserLocale());
					      secureParms.put("clientBankOid",    userSession.getClientBankOid());
					      secureParms.put("baseCurrencyCode", userSession.getBaseCurrencyCode());
					      secureParms.put("ownerOrgOid", userSession.getOwnerOrgOid());
				%>
    					<%@ include file="/uploads/fragments/Uploads-PurchaseOrderListView.frag" %>


				<%
					}

				if (TradePortalConstants.NAV__STRUCTURE_PURCHASE_ORDER.equals(current2ndNav))
				{
				      secureParms.put("SecurityRights",   userSecurityRights);
				      secureParms.put("UserOid",          userOid);
				      secureParms.put("corporateOrgOid",  userOrgOid);
				      secureParms.put("timeZone",         userSession.getTimeZone());
				      secureParms.put("userLocale",       userSession.getUserLocale());
				      secureParms.put("clientBankOid",    userSession.getClientBankOid());
				      secureParms.put("baseCurrencyCode", userSession.getBaseCurrencyCode());
				      secureParms.put("ownerOrgOid", userSession.getOwnerOrgOid());
				 %>

				      <%@ include file="/uploads/fragments/Uploads-StructurePurchaseOrderListView.frag" %>
				 <%
				}
					if (TradePortalConstants.NAV__PAYMENT_FILE_UPLOAD.equals(current2ndNav))
					{
				%>
						<%@ include file="/uploads/fragments/Uploads-PaymentFileUploadListView.frag" %>

				<%
					}
					if(TradePortalConstants.NAV__INVOICE_FILE_UPLOAD.equals(current2ndNav))
					{
						  secureParms.put("SecurityRights",   userSecurityRights);
					      secureParms.put("UserOid",          userOid);
					      secureParms.put("corporateOrgOid",  userOrgOid);
					      secureParms.put("timeZone",         userSession.getTimeZone());
					      secureParms.put("userLocale",       userSession.getUserLocale());
					      secureParms.put("clientBankOid",    userSession.getClientBankOid());
					      secureParms.put("baseCurrencyCode", userSession.getBaseCurrencyCode());
				%>
				    <%@ include file="/uploads/fragments/Uploads-InvoiceFileUploadListView.frag" %>
				<%
					}
				      //IAZ CM-451 12/27/08 Begin: add secure params for the CM Tab navigation
				      //secureParms.put("bankBranch",corpOrg.getAttribute("first_op_bank_org"));
				      secureParms.put("userOid",userSession.getUserOid());
				      secureParms.put("securityRights",userSession.getSecurityRights());
				      secureParms.put("clientBankOid",userSession.getClientBankOid());
				      secureParms.put("ownerOrg",userSession.getOwnerOrgOid());
				      //IAZ 12/27/08 End
				      //VS RVU062382912 Adding New parameter value for transactions
				      if (userSession.getSavedUserSession() == null) {
				        secureParms.put("AuthorizeAtParentViewOrSubs", TradePortalConstants.INDICATOR_YES);
				      }
				%>
					<%= formMgr.getFormInstanceAsInputField(formName, secureParms) %>
				</div><%-- end of formContentNoSidebar div --%>
			</form>
		</div><%-- end of pageContent div --%>
	</div><%-- end of pageMainNoSidebar div --%>

	<jsp:include page="/common/Footer.jsp">
	   <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
	   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
	</jsp:include>
<%@ include file="/common/GetSavedSearchQueryData.frag" %>
	<script type="text/javascript" src="/portal/js/datagrid.js"></script>

	<%
	    String msg=null;
		// Javascript for the Purchaseorders dataview is included in this .frag
		if (TradePortalConstants.NAV__PURCHASE_ORDERS.equals(current2ndNav)){
		   msg=resMgr.getText("PurchaseOrders.ConfirmDeleteMessage",
                                                     TradePortalConstants.TEXT_BUNDLE);

	%>
  <%@ include file="/uploads/fragments/Uploads-PurchaseOrdersFooter.frag" %>
  <%}%>

  <%
		if (TradePortalConstants.NAV__STRUCTURE_PURCHASE_ORDER.equals(current2ndNav)){
		   msg=resMgr.getText("PurchaseOrders.ConfirmDeleteMessage",
                                                     TradePortalConstants.TEXT_BUNDLE);

	%>
  <%@ include file="/uploads/fragments/Uploads-StructurePurchaseOrdersFooter.frag" %>
  <%}%>

	<%if(TradePortalConstants.NAV__PAYMENT_FILE_UPLOAD.equals(current2ndNav)){
         msg=resMgr.getText("FutureValueTransactions.PopupMessage",
                                                     TradePortalConstants.TEXT_BUNDLE);
     %>
      <%@ include file="/uploads/fragments/Uploads-PaymentFileUploadFooter.frag" %>

	<%}
	if(TradePortalConstants.NAV__INVOICE_FILE_UPLOAD.equals(current2ndNav)){
		msg=resMgr.getText("FutureValueTransactions.PopupMessage",
                TradePortalConstants.TEXT_BUNDLE);
	%>

	<%@ include file="/uploads/fragments/Uploads-InvoiceFileUploadFooter.frag" %>

	<%} %>

	<%-- **************** JavaScript for Future tab only *********************  --%>

      <script language="JavaScript">

      
         <%--
         // This function is used to display a popup message confirming
         // that the user wishes to delete the selected items in the
         // listview.
		 --%>

         function confirmDelete()
         {
           var   confirmMessage = "<%=msg%>";

            if (!confirm(confirmMessage)) <%--  Modified by LOGANATHAN - 10/10/2005 - IR LNUF092749769 --%>
             {
                formSubmitted = false;
                return false;
             }
           else
           {
              return true;
           }
        }



      </script>
</body>
</html>

<%
   formMgr.storeInDocCache("default.doc", new DocumentHandler());

   // Store the current tab in the session.
   session.setAttribute("uploads2ndNav", current2ndNav);
%>

