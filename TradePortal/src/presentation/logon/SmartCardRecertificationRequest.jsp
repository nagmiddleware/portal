<%--
 *   This page will load the ANZ SmartCard applet. The applet will respond with javascript calls to DoRedirectToSuccess, 
 *   when the SmartCard authentication is successful, or to DoRedirectToFailure, when authentication fails.   These functions
 *   will set a hidden field on the authorize page with Y for success or N for failure, and then will close the popup.
 *
 *     Copyright   2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="java.util.StringTokenizer,com.amsinc.ecsg.web.*, com.amsinc.ecsg.frame.*, com.amsinc.ecsg.util.*,
                 com.ams.tradeportal.common.*, com.ams.tradeportal.mediator.util.*, com.ams.tradeportal.busobj.util.*, com.ams.tradeportal.busobj.webbean.*" %>
                 
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>
<jsp:useBean id="formMgr"     class="com.amsinc.ecsg.web.FormManager"            scope="session"></jsp:useBean>
<jsp:useBean id="resMgr"      class="com.amsinc.ecsg.util.ResourceManager"       scope="session"></jsp:useBean>
<jsp:useBean id="beanMgr"     class="com.amsinc.ecsg.web.BeanManager" scope="session"> </jsp:useBean>

<%

	
  StringBuffer cssFilename = new StringBuffer(TradePortalConstants.IMAGES_PATH);
  cssFilename.append(userSession.getBrandingDirectory());
  cssFilename.append("/");
  cssFilename.append(resMgr.getText("brandedImage.stylesheet", TradePortalConstants.TEXT_BUNDLE));
      
  String orgId               = request.getParameter("organization");
  String brandingDirectory   = StringFunction.xssCharsToHtml(request.getParameter("branding")); //T36000004579 #3;
  String locale              = request.getParameter("locale");
  String bankDate            = request.getParameter("date");
  String certData            = request.getParameter("info");
  String requestedAuthMethod = request.getParameter("auth");
  //cr498 - add submitButtonName for dynamic submission on success
  String submitButtonName    = request.getParameter("submitButtonName");
  //cr498 end
  String physicalPage        = "";
  
//Rel 9.3 XSS CID-11421
 
  // Forward the user to the CompleteRecertification page to close the window, if a reponse is not received.
  // Subtract 15 seconds from the timeout period.
  // This way, the browser will forward to the timeout page before the session
  // is actually killed by the server
  int timeoutInSeconds = SecurityRules.getTimeOutPeriodInSeconds() - 15;
  //cr498 - set submitButtonName in session for CompleteRecertification.jsp
  session.setAttribute("submitButtonName", submitButtonName);
  //cr498 end
      
  //TRUDDEN IR RUJ041658830 Add Begin  
   
  String challengeString;
  String challengeStringKey = ""; 
  if (SecurityRules.useDefaultServerChallengeString()) {
    //get the default challengeString parameter to provide to the LogonApplet
    challengeString = SecurityRules.getSmartCardServerChallengeString();
    if (InstrumentServices.isBlank(challengeString)) {
      challengeString = "$$THEMD5CHALLENGE$$";
    }
  } 
  else {
    //generate a new challengeString
    //CR-473 BEGIN change the challenge string to use transaction data 
    //cr498 - change to use reAuthObjectOid rather than TransactionOid
    // to allow for pay remits and invoices
    //ir cnuk113043991 - just get the first reAuthObjectOid
    String transactionOidBase64 = request.getParameter("reAuthObjectOid0");
    //out.println("reAuthObjectOid parm="+transactionOidBase64);
    String transactionOid = "";
    // If this page is called from the CashMgmt-Transaction-PendingListView.frag, the OID of the first
    // selected transaciton is passed in the request.
    // If this page is called from the Transaction page, the OID of the current transaction is available 
    // in the formMgr as secure parm.
    if (!InstrumentServices.isBlank(transactionOidBase64)) {
      //if found parent is a list
      String checkBoxValue = EncryptDecrypt.encryptStringUsingTripleDes(transactionOidBase64, userSession.getSecretKey());
      StringTokenizer parser = new StringTokenizer(checkBoxValue, "/");
      if (parser.hasMoreTokens()){
        transactionOid = parser.nextToken();
      }           
    }
    else {
      //ir CAUK113043267 start
      //parent is a detail
      //use the parentNi parm to get the oid from secure parms
      String parentNi = request.getParameter("parentNi");
      //out.println("parentNi:"+parentNi);
      if ( parentNi != null ) {
        //try to get from transaction detail page
        transactionOid = formMgr.getSecurePageParm(parentNi,"transaction_oid");
        if (transactionOid==null) {
          //try to get from pay remit detail page
          transactionOid = formMgr.getSecurePageParm(parentNi,"pay_remit_oid");
        }
      }
      //ir CAUK113043267 end
    }
    //out.println("reAuthObjectOid="+transactionOid);
    //ClientServerDataBridge csdb 		= resMgr.getCSDB();
    //csdb.setCSDBValue("SmartCardChallengeStringKey", transactionOid);
       
    if (!InstrumentServices.isBlank(transactionOid)) {
      //out.println("trans oid is " + transactionOid + "<br>");
      TransactionWebBean transaction = beanMgr.createBean(TransactionWebBean.class, "Transaction");
      if ( transaction != null ) {
        //out.println("found a transaction");
        transaction.setAttribute("transaction_oid", transactionOid);
        transaction.getDataFromAppServer();
        String transactionAmount = transaction.getAttribute("copy_of_amount");
        String transactionDate = transaction.getAttribute("date_started");
        //out.println("am and date " + transactionAmount + " " + transactionDate + "<br>");       	   
        challengeStringKey = "transaction_oid="+transactionOid+";copy_of_amount="+transactionAmount+";date_started="+transactionDate;
      } else {
        challengeStringKey = "transaction_oid="+transactionOid+";";
      }
    }
    else { // This should not happen.  Coded to be more solid.
      Integer dateValue = new Integer(GMTUtility.getGMTDateTime().hashCode());
      //cr498 - fix null pointer issue
      if (userSession != null && userSession.getUserOid() != null ) {
        challengeStringKey = userSession.getUserOid().trim();
      }
      if (dateValue != null ) {
        challengeStringKey += dateValue.toString();             
      }
      //out.println("userses " + userSession.getUserOid());
    }
    //out.println("cs key " + challengeStringKey + "<br> chstr md5 " + EncryptDecrypt.createMD5Hash(challengeStringKey.getBytes()));
    challengeString = EncryptDecrypt.bytesToBase64String(EncryptDecrypt.createMD5Hash(challengeStringKey.getBytes()), false);       
    //out.println("<br> strn " + challengeString + "<br>");
    //out.println(com.amsinc.ecsg.util.Base64.encode(EncryptDecrypt.createMD5Hash(challengeStringKey.getBytes())));
  }
  //TRUDDEN IR RUJ041658830 End
  // Store the challenge string for validation when the certificate is read.
  ClientServerDataBridge csdb 		= resMgr.getCSDB();
  csdb.setCSDBValue("SmartCardChallengeStringKey", challengeStringKey);
  csdb.setCSDBValue("SmartCardChallengeString", challengeString);
  // CR-473 END
%>

<%--cquinton 5/16/2013 Rel 8.2 dojoize--%>
<%-- Body tag included as part of common header --%>
<jsp:include page="/common/Header.jsp">
  <jsp:param name="displayHeader" value="<%=TradePortalConstants.INDICATOR_NO%>" />
</jsp:include>

<script LANGUAGE="JavaScript">

    var global = false;	<%-- IAZ 06/04/09 Add use of global throu the JavaCode to prevent ever running Failure() if Success() is called --%>
    setTimeout("document.location.href='<%=formMgr.getLinkAsUrl("goToCompleteRecertification", response)%>'", <%=timeoutInSeconds%>000);

    function DoRedirectToFailure() {
        <%--cr498 change button name to passed in value--%>
        var submitButtonName = "<%=StringFunction.escapeQuotesforJS(submitButtonName)%>";
        
        window.opener.document.forms[0].buttonName.value=submitButtonName;
        window.opener.document.forms[0].reCertOK.value="<%=TradePortalConstants.INDICATOR_NO%>"; 
        if (!global) {
            window.opener.document.forms[0].submit();
        }
        window.close();
    }
       
    function DoRedirectToSuccess() {
        
        <%--cr498 change button name to passed in value--%>
        var submitButtonName = "<%=StringFunction.escapeQuotesforJS(submitButtonName)%>";
        
        logonResponse = document.LogonApplet.logonResponse();
        logonCertificate=document.LogonApplet.logonCertificate();
        
        
        <%-- logonCertificateDN = document.LogonApplet.logonCertificateDN(); --%>
        window.opener.document.forms[0].buttonName.value=submitButtonName;
        <%-- W Zhu 12/16/09 CR-482 Set reCertOK to M in order to differentiate with the other recertification method. --%>
        window.opener.document.forms[0].reCertOK.value="<%=TradePortalConstants.INDICATOR_MULTIPLE%>";   
        window.opener.document.forms[0].logonResponse.value = logonResponse;
        window.opener.document.forms[0].logonCertificate.value = logonCertificate;
        global = true;
        <%-- window.alert("About to submit " + global); --%>
        window.opener.document.forms[0].submit();
        <%-- document.LogonApplet.stop(); IAZ 06/04/09 Delete: we don't need to stop the applet manually --%>
        window.close();
    }
       

</script>

<div class="dialogContent">

  <jsp:include page="/common/PageHeader.jsp">
    <jsp:param name="titleKey" value="Recertification.Title" />
  </jsp:include>

  <div class="formContentNoSidebar">
    <div class="padded">
      <div class="instruction">
        <%= resMgr.getText("SmartCardRecertification.Instructions",TradePortalConstants.TEXT_BUNDLE) %>
      </div>

      <div>
        <applet name=LogonApplet
          code="au.com.securenet.cardserver.SAAMApplet"
          archive="SAAMApplet.jar" width="10px" height="10px" MAYSCRIPT>
          <param name=JSSuccessCall value="DoRedirectToSuccess">
          <param name=JSFailureCall value="DoRedirectToFailure">
          <param name=ServerChallengeString value="<%=challengeString%>">
          <param name=LogonModeFlags
            value="[ReadCertificate]=true;[ExtractDN]=true;[ExtractSerialNumber]=true;">
          <param name=CertSearchOrder value="Signing;Utility">
          <hr>
            Java is not supported by your browser.
          <hr>
        </applet>
      </div>
  
<%		
  Debug.debug("loading SmartCard applet ");   
  //Debug.debug("challengeString = " + challengeString);
%>

    </div>
  </div>
    
<div>

<jsp:include page="/common/Footer.jsp">
  <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_NO%>" />
  <jsp:param name="minimalHeaderFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
  <jsp:param name="includeTimeoutForward" value="false" />
</jsp:include>

<script>
  document.title = '<%=resMgr.getText("Recertification.Title",TradePortalConstants.TEXT_BUNDLE)%>';
</script>

</body>
</html>
