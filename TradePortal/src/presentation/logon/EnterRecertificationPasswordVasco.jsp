<%--
 *     This page will present the user with 2FA VASCO Device Token Code fields to sign the Transaction.
 *	   Rel 8.0 CR-711. Dec 2011
 	   AAlubala
--%>
<%@ page import="com.amsinc.ecsg.web.*, com.amsinc.ecsg.frame.*, com.amsinc.ecsg.util.*,
                 com.ams.tradeportal.common.*, com.ams.tradeportal.mediator.util.*, com.ams.tradeportal.busobj.util.*, com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.Transaction, com.ams.tradeportal.html.*, java.math.BigInteger, java.util.*,java.util.Date,java.text.*" %>
                 
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>
<jsp:useBean id="formMgr"     class="com.amsinc.ecsg.web.FormManager"            scope="session"></jsp:useBean>
<jsp:useBean id="resMgr"      class="com.amsinc.ecsg.util.ResourceManager"       scope="session"></jsp:useBean>
<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session"></jsp:useBean>

<%

	
   WidgetFactory widgetFactory = new WidgetFactory(resMgr);
   boolean instanceHSMEnabled = HSMEncryptDecrypt.isInstanceHSMEnabled();
   StringBuffer cssFilename = new StringBuffer(TradePortalConstants.IMAGES_PATH);
   cssFilename.append(userSession.getBrandingDirectory());
   cssFilename.append("/");
   cssFilename.append(resMgr.getText("brandedImage.stylesheet", TradePortalConstants.TEXT_BUNDLE));   
	boolean isCashInstrument=false;
	boolean isTradeInstrument=false;
	boolean isReceivableInstrument=false;
	boolean isInvoiceMgntInstrument=false;
	boolean isRMInvoiceInstrument=false;
	String VascoKeyInfo="";
   String orgId               = request.getParameter("organization");
   String brandingDirectory   = StringFunction.xssCharsToHtml(request.getParameter("branding")); //T36000006718 check xss
   String locale              = request.getParameter("locale");
   String bankDate            = request.getParameter("date");
   String certData            = request.getParameter("info");
   String submitButtonName    = request.getParameter("submitButtonName");
   String physicalPage        = "";
   String payRemitOid		  = "";
   String invoiceSummaryOid   = "";
   
  TransactionWebBean transaction  = null;
  InstrumentWebBean instrument    = null;
  TemplateWebBean template        = null;
  TermsWebBean terms              = null;   
  TermsPartyWebBean termsPartyPayee = null;
  PayRemitWebBean payRemitWebBean  = null;
  UserWebBean userWebBean		= null;
  InvoiceGroupWebBean invoiceGroupWebBean = null;
  InvoicesSummaryDataWebBean invoicesSummaryWebbean = null;
  Object sqlParams[] = null;
  try{
	   transaction  = (TransactionWebBean)beanMgr.getBean("Transaction");
	   //Commented the below line for PR ER - T36000014931 REL 8.1.0.4. In case of Payments, this object exists, but returns a nullpointer at Line 192. Hence commenting.
           // W Zhu 30 Apr 2013 T36000016258 instantiate instrument bean for the case when this page is called from transaction detail page.
	   instrument    = (InstrumentWebBean) beanMgr.getBean("Instrument");
	   template        = (TemplateWebBean)beanMgr.getBean("Template");
	   terms              = (TermsWebBean) beanMgr.getBean("Terms");  
	   termsPartyPayee = beanMgr.createBean(TermsPartyWebBean.class,"TermsParty");
	   payRemitWebBean = beanMgr.createBean(PayRemitWebBean.class, "PayRemit");
	   userWebBean = beanMgr.createBean(UserWebBean.class, "User");
  }catch(Exception ex){
	  Debug.debug("Exception EnterRecertificationPasswordVasco "+ex);
  }
  
  String userLocale         = userSession.getUserLocale();
  
  //This is the instrument type code
  String instrType    = "";
  //This is the instrument type description for display purposes
  String instrTypeDescForDisplay    = "";
  String transactionType   = "";
  String Ccy="";
  String cnt="1";
  
   String TransactionAmount="0";
   if(!(null == transaction))
   TransactionAmount = transaction.getAttribute("copy_of_amount");
   if(StringFunction.isBlank(TransactionAmount)) TransactionAmount="0";
   String ReferenceID="0";
   if(!(null == instrument))
   ReferenceID = instrument.getAttribute("complete_instrument_id"); 
   if(StringFunction.isBlank(ReferenceID)) ReferenceID="0";
   String BeneAccntNumber="";
   Hashtable addlParms = new Hashtable();
   addlParms.put("brandingDirectory", brandingDirectory);
   addlParms.put("organizationId", orgId);
   addlParms.put("locale", locale);   
   if (!(StringFunction.isBlank(bankDate))) {
       addlParms.put("date", bankDate);
   }   
   if (!(StringFunction.isBlank(certData))) {
       addlParms.put("info", certData);
   } 
   addlParms.put("auth", TradePortalConstants.AUTH_2FA);
   addlParms.put("loginId", userSession.getUserId());
   String recertificationInd = TradePortalConstants.INDICATOR_YES;
   //set submitButtonName in session for CompleteRecertification.jsp
   session.setAttribute("submitButtonName", submitButtonName);
   
   String instrumentTypeCode = "";
   
   boolean isARMInstrument = false;
   boolean isRFInstrument = false;
   
   String allReAuthObjectOids = "";
   int reAuthObjectIdx = 0;
   int invoicesInAGroup=1;
   //IR T36000013570 start
   String mixedPrecision = "";
   String mixCurrencySql1 = "select distinct addl_value,code from refdata where table_type='CURRENCY_CODE' and addl_value is not null and code in (";
   String mixCurrencySql2  = ") order by addl_value desc";
   //IR T36000013570 end
   String rmInvoiceTransaction = "select invoice_oid, invoice_reference_id , currency_code, invoice_total_amount "
  + " from invoice where invoice_oid in (";    
   
   String reAuthObjectOid = request.getParameter("checkbox"+reAuthObjectIdx);
   String reAuthObjectType    = request.getParameter("reAuthObjectType");
   String instrumentSQL = "select complete_instrument_id from instrument where instrument_oid in (select p_instrument_oid from transaction where transaction_oid in ( ";
   String transactionSQL = "select copy_of_currency_code, copy_of_amount, copy_of_instr_type_code, p_instrument_oid from transaction where transaction_oid in ( ";
   String rmTransaction = "select M.MATCHED_AMOUNT, M.INVOICE_MATCHING_STATUS, M.PAY_MATCH_RESULT_OID from PAY_MATCH_RESULT M where M.P_PAY_REMIT_OID in ( ";
   
   rmTransaction  = "select PAYMENT_AMOUNT, TOTAL_INVOICE_AMOUNT, A_INSTRUMENT_OID,CURRENCY_CODE, TOTAL_NUMBER_OF_INVOICES "
        + " from PAY_REMIT "
        + " where PAY_REMIT_OID in ( ";
        
  String invoiceTransaction = "select upload_invoice_oid, invoice_id , currency, amount, linked_to_instrument_type "
  + " from invoices_summary_data where upload_invoice_oid in ()";    

  //invoiceTransaction = "select sum(amount) as AMOUNT, currency, linked_to_instrument_type, upload_invoice_oid, invoice_id from invoices_summary_data where a_invoice_group_oid in ( ";
  invoiceTransaction = "select AMOUNT, currency, linked_to_instrument_type ,NUMBER_OF_INVOICES from invoice_group_view where invoice_group_oid in ( ";
  if("Invoice".equals(reAuthObjectType)){
  
  invoiceTransaction = "select AMOUNT, currency, linked_to_instrument_type, upload_invoice_oid, invoice_id from invoices_summary_data where upload_invoice_oid in ( ";
  }
   String theType = "";
   DocumentHandler listDoc;
   DocumentHandler instrumentDoc;
   DocumentHandler payeeDoc;
   String refIds = "";
   String accntAmnts = "";
   double sumAccntAmounts=0.0;
   String beneAccnts = "";
   String concatenatedAmnts="";
   String concatenatedRefIds="";
   String concatenatedBeneAccnts="";
   String termsPartyOid="";
   String commaSeparatedTransOids="";
   String commaSeparatedInstrOids="";
   int countOfTransactions=0;
   int count=0;
   int instrumentId=0; //instrument_oid to be used in key computation for CASH and TRADE
   DocumentHandler doc1 = null;
   //The current date (numerical equivalent) to be used as part of 
   //vacso key computation
	Date date = new Date();
	long time = date.getTime();
	java.util.Date todaysDate = new java.util.Date();
	//
	//IR#DEUM060139377 - Base the Key information on only day month and year parts of the date
	//and not time component, this is to enable the key to be the same within the same day
	String DATE_FORMAT = "ddMMyyyy";
	SimpleDateFormat formatter = new SimpleDateFormat(DATE_FORMAT);
	String formattedDate = formatter.format(todaysDate);
	if(StringFunction.isNotBlank(formattedDate))
	 time = Long.parseLong(formattedDate);  
	//
	//END IR#DEUM060139377
	//
	double TotalInvoiceAmnt=0;
	int CntOfInvoice=0;
  	String instrumentType="";
   
   if(StringFunction.isNotBlank(reAuthObjectType)){
	   if(reAuthObjectType.equalsIgnoreCase("PayRemit")){
		   isReceivableInstrument = true;
	   }else if(reAuthObjectType.equalsIgnoreCase("Invoice") || reAuthObjectType.equalsIgnoreCase("InvoiceG")){
		   isInvoiceMgntInstrument = true;
		   //IR#DEUM060655441 - Invoices from Invoice Management page need to be differentiated
		   //from older RM invoices since they use different Beans
		   //this will be used later on in mediator bean for error handling
		//   reAuthObjectType="InvoiceMgnt";
		   addlParms.put("reAuthObjectType", "InvoiceMgnt");
	   }
	   else if(reAuthObjectType.equalsIgnoreCase("RMInvoice")){
		   isRMInvoiceInstrument = true;
		   }
   }
  
   if ( reAuthObjectType != null ) {
      addlParms.put("reAuthObjectType", reAuthObjectType);
   }   
   //
   //Set Invoice
   invoiceSummaryOid = (String)session.getAttribute("upload_invoice_oid");   

   // W Zhu 30 Apr 2013 T36000016258 check the validity of instrument and transaction bean.  transaction bean is empty if this is called from pending transaction list page.
   if(instrument != null && transaction != null){
	   instrumentType = instrument.getAttribute("instrument_type_code");
	   if(InstrumentServices.isCashManagementTypeInstrument(instrumentType)) isCashInstrument=true;
	   if(InstrumentServices.isTradeTypeInstrument(instrumentType)) isTradeInstrument=true;
	   ReferenceID = instrument.getAttribute("complete_instrument_id"); //corresponds to the instrument id
	   transactionType   = transaction.getAttribute("transaction_type_code");	  
   } 
   //
   //If it is a receivable instrument, we are coming from Receivables management
   payRemitOid = (String)session.getAttribute("SessionPayRemitOid");
   payRemitWebBean.getById(payRemitOid);
    if(isReceivableInstrument){
    	if(!(null == payRemitWebBean)){
		   String TotalInvoiceAmount = payRemitWebBean.getAttribute("total_invoice_amount");
		   TransactionAmount = TotalInvoiceAmount;
		   String CountOfInvoice = payRemitWebBean.getAttribute("total_number_of_invoices");
		   if(StringFunction.isNotBlank(TotalInvoiceAmount))TotalInvoiceAmnt=Double.parseDouble(TotalInvoiceAmount);
		   if(StringFunction.isNotBlank(CountOfInvoice))CntOfInvoice=Integer.parseInt(CountOfInvoice);
		   //Vasco Key = sum of time, total invoice amount and count of invoice
		     //IR#DEUM060139377
		   VascoKeyInfo = ""+(time+(Math.round(TotalInvoiceAmnt))+CntOfInvoice);		
		   cnt = CountOfInvoice;
		   Ccy = payRemitWebBean.getAttribute("currency_code");
		   ReferenceID = payRemitWebBean.getAttribute("instrument_oid");
     	  	//If Instrument ID is not available, display as NONE
     	  	if(StringFunction.isBlank(ReferenceID)){
     	  		ReferenceID = "NONE";
     	  	}		   
		   
		   instrType = InstrumentType.RECEIVABLES_PAYBLES_MANAGEMENT;
		 //IR#LAUM060134426 - Look up the instrument type desc for the code
	      	instrTypeDescForDisplay = ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.INSTRUMENT_TYPE,instrType, userLocale);
		   
	   }
    }
   if ( reAuthObjectOid != null ) {
      //if not null it means we have a list
      do {
    	 // PR ER-T36000014931 REL 8.1.0.4. Changed the below line form 'encryptStringUsingTripleDes' to 'decryptStringUsingTripleDes', 
    	 // The 'reAuthObjectOid' is encrypted (rowKeys) and hence needs to be decrypted, otherwise throws error
		 String checkBoxValue = EncryptDecrypt.decryptStringUsingTripleDes(reAuthObjectOid, userSession.getSecretKey());
		 StringTokenizer parser = new StringTokenizer(checkBoxValue, "/");
		 int tokenCnt=0;
		 String parserToken;
		 countOfTransactions++;
		 //Only need to get the first token from the parser
		 //Here we are creating a comma separated list of transaction oids (or pay_remit_oids if Receivables Management)
		 //to be used in the SQL statements
		 while (parser.hasMoreTokens() && tokenCnt < 1){
		       //The first item from the parser will be transaction_oid for cash and trade
		       //transactions and for receivables payment matches, it will be pay_remit_oid
		       if(tokenCnt==0){
		    	   if(commaSeparatedTransOids.length()<1){
		    		   parserToken = parser.nextToken();
		    		   commaSeparatedTransOids = commaSeparatedTransOids + parserToken;
		    		   allReAuthObjectOids = commaSeparatedTransOids;
		    	   }else{
		    		   parserToken = parser.nextToken();
		    	   	   commaSeparatedTransOids = commaSeparatedTransOids + "," + parserToken; 
		    	   	   allReAuthObjectOids += "!" + parserToken;
		    	   }
		       }
			   tokenCnt++; 
			   
		 }
		 reAuthObjectIdx++;
      } while ( ( reAuthObjectOid = request.getParameter("checkbox"+reAuthObjectIdx) ) != null );
       //Total selected transactions (when coming from a list page)
      //
      cnt = ""+countOfTransactions;
      //now run the SQL queries with the list of comma separated values from above
      //then loop on the results to create concatenated values
      //
      //If it is an RM instrument, use the RM SQL and obtain the matched amount else use the Trans SQL and get copy of amount
       if(isReceivableInstrument){
		   instrType = InstrumentType.RECEIVABLES_PAYBLES_MANAGEMENT;
		 //jgadela R91IR T36000026319 - SQL INJECTION FIX
	      listDoc = DatabaseQueryBean.getXmlResultSet(rmTransaction+commaSeparatedTransOids+" ) ",false, sqlParams);
	      if(null != listDoc){
		   	  Vector transactionFragments = listDoc.getFragments("/ResultSetRecord");
			  int numItems = transactionFragments.size();
			  for (int i = 0; i < numItems; i++) {
				doc1 = (DocumentHandler) transactionFragments.elementAt(i);
				accntAmnts = doc1.getAttribute("/TOTAL_INVOICE_AMOUNT");
				concatenatedAmnts = concatenatedAmnts + accntAmnts;				
				if(StringFunction.isNotBlank(accntAmnts)){
					//sum the amounts
					sumAccntAmounts = sumAccntAmounts + Double.parseDouble(accntAmnts);
				}
				//IR T36000013570 start
				if(StringFunction.isNotBlank(doc1.getAttribute("/CURRENCY_CODE"))){
					mixCurrencySql1 = mixCurrencySql1 +"'"+ SQLParamFilter.filter(doc1.getAttribute("/CURRENCY_CODE")) +"'";
					if(i+1 !=numItems)
						mixCurrencySql1 = mixCurrencySql1+",";
					else{
						mixCurrencySql1 = mixCurrencySql1+mixCurrencySql2;
					}
				}
				
				//IR T36000013570 end
			  }	
			 	  invoicesInAGroup=numItems;		
			  
	      } 
	      //IR#LAUM060134426 - Look up the instrument type desc for the code
	      instrTypeDescForDisplay = ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.INSTRUMENT_TYPE,instrType, userLocale);
	      
      }else if(isInvoiceMgntInstrument){
		   //The query below with GROUP BY was giving a syntax error in some cases
    	   listDoc = DatabaseQueryBean.getXmlResultSet(invoiceTransaction+commaSeparatedTransOids+" ) ",false, sqlParams);
   
	      if(null != listDoc){
		   	  Vector transactionFragments = listDoc.getFragments("/ResultSetRecord");
			  int numItems = transactionFragments.size();
			  int totCnt=0;
			  for (int i = 0; i < numItems; i++) {
				doc1 = (DocumentHandler) transactionFragments.elementAt(i);
				accntAmnts = doc1.getAttribute("/AMOUNT");
				concatenatedAmnts = concatenatedAmnts + accntAmnts;				
				if(StringFunction.isNotBlank(accntAmnts)){
					//sum the amounts
					sumAccntAmounts = sumAccntAmounts + Double.parseDouble(accntAmnts);
				}
				//IR T36000013570 start
				if(StringFunction.isNotBlank(doc1.getAttribute("/CURRENCY"))){
					mixCurrencySql1 = mixCurrencySql1 +"'"+ SQLParamFilter.filter(doc1.getAttribute("/CURRENCY")) +"'";
					if(i+1 !=numItems)
						mixCurrencySql1 = mixCurrencySql1+",";
					else{
						mixCurrencySql1 = mixCurrencySql1+mixCurrencySql2;
					}
				}
				 if("InvoiceG".equals(reAuthObjectType)){
					 totCnt=totCnt + doc1.getAttributeInt("/NUMBER_OF_INVOICES");	
				  }
				//IR T36000013570 end
			  }	
			  //IR#MHUM061343592 - Count for invoices when a single item is selected should match the invoices in a group.
			  if("Invoice".equals(reAuthObjectType)){
				  invoicesInAGroup=numItems;		
			  }
			  else if("InvoiceG".equals(reAuthObjectType)){
				  invoicesInAGroup=totCnt ;	
				  }	  
			  //if this is an invoice group, then the fragments will correspond to the invoices in the group 	  
	      } 
	      //set the instrument type
	      instrType = doc1.getAttribute("/LINKED_TO_INSTRUMENT_TYPE");;
	    //IR#LAUM060134426 - Look up the instrument type desc for the code
	      instrTypeDescForDisplay = ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.INSTRUMENT_TYPE,instrType, userLocale);

      }
      else if(isRMInvoiceInstrument){
    	  listDoc = DatabaseQueryBean.getXmlResultSet(rmInvoiceTransaction+commaSeparatedTransOids+" ) ",false, sqlParams);
  
	      if(null != listDoc){
		   	  Vector transactionFragments = listDoc.getFragments("/ResultSetRecord");
			  int numItems = transactionFragments.size();
			  for (int i = 0; i < numItems; i++) {
				doc1 = (DocumentHandler) transactionFragments.elementAt(i);
				accntAmnts = doc1.getAttribute("/INVOICE_TOTAL_AMOUNT");
				concatenatedAmnts = concatenatedAmnts + accntAmnts;				
				if(StringFunction.isNotBlank(accntAmnts)){
					//sum the amounts
					sumAccntAmounts = sumAccntAmounts + Double.parseDouble(accntAmnts);
				}
				//IR T36000013570 start
				if(StringFunction.isNotBlank(doc1.getAttribute("/CURRENCY_CODE"))){
					mixCurrencySql1 = mixCurrencySql1 +"'"+ SQLParamFilter.filter(doc1.getAttribute("/CURRENCY_CODE")) +"'";
					if(i+1 !=numItems)
						mixCurrencySql1 = mixCurrencySql1+",";
					else{
						mixCurrencySql1 = mixCurrencySql1+mixCurrencySql2;
					}
				}
				//IR T36000013570 end
			  }	
			  //IR#MHUM061343592 - Count for invoices when a single item is selected should match the invoices in a group.
			  invoicesInAGroup=numItems;		//if this is an invoice group, then the fragments will correspond to the invoices in the group 	  
	      } 
	      //set the instrument type
	      instrType = TradePortalConstants.INV_LINKED_INSTR_REC;
	    //IR#LAUM060134426 - Look up the instrument type desc for the code
	      instrTypeDescForDisplay = ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.INSTRUMENT_TYPE,instrType, userLocale);

     }
      else{
		  //
	      //Transaction SQL to get Transaction Amounts:
	     listDoc = DatabaseQueryBean.getXmlResultSet(transactionSQL+commaSeparatedTransOids+" )", false, sqlParams);
	      if(null != listDoc){
		   	  Vector transactionFragments = listDoc.getFragments("/ResultSetRecord");
			  int numItems = transactionFragments.size();
			  for (int i = 0; i < numItems; i++) {
				doc1 = (DocumentHandler) transactionFragments.elementAt(i);
				accntAmnts = doc1.getAttribute("/COPY_OF_AMOUNT");
				//concatenate the values
				concatenatedAmnts = concatenatedAmnts + accntAmnts;		
				//sum the amounts
				if(StringFunction.isNotBlank(accntAmnts)){
					sumAccntAmounts = sumAccntAmounts + Double.parseDouble(accntAmnts);
				}
				//IR T36000013570 start
				if(StringFunction.isNotBlank(doc1.getAttribute("/COPY_OF_CURRENCY_CODE"))){
					mixCurrencySql1 = mixCurrencySql1 +"'"+ SQLParamFilter.filter(doc1.getAttribute("/COPY_OF_CURRENCY_CODE")) +"'";
					if(i+1 !=numItems)
						mixCurrencySql1 = mixCurrencySql1+",";
					else{
						mixCurrencySql1 = mixCurrencySql1+mixCurrencySql2;
					}
				}
				//IR T36000013570 end
			  }		  
	      }
      }
      
      //The key will be a hash sum of account amounts, the number of transactions
      //and the numeric equivalent of today's date
      //  //IR#DEUM060139377
     VascoKeyInfo=""+(Math.round(sumAccntAmounts)+time+countOfTransactions);
      TransactionAmount = ""+sumAccntAmounts;
      
       if (countOfTransactions > 1) {
          //Instrument type and ID will be 'Multiple Transactions' string
          //MEer IR-32869 Getting "Multiple Transactions" text from resource bundle for localization.
    	  instrTypeDescForDisplay = resMgr.getText("TransactionSigning.MultipleTransactions", TradePortalConstants.TEXT_BUNDLE);
      	  ReferenceID = resMgr.getText("TransactionSigning.MultipleTransactions", TradePortalConstants.TEXT_BUNDLE);
      	  Ccy = "";
      	DocumentHandler mixPrecisionDoc = DatabaseQueryBean.getXmlResultSet(mixCurrencySql1.toString(), false, sqlParams);
        
    	if (mixPrecisionDoc != null) {
                    mixedPrecision = mixPrecisionDoc.getAttribute("ResultSetRecord[0]/CODE");   
    	}
    }else{
    	
    	 if("InvoiceG".equals(reAuthObjectType)){
    			cnt = ""+invoicesInAGroup;
		 }	  
          if(!isInvoiceMgntInstrument && !isReceivableInstrument &&!isRMInvoiceInstrument)
          {
        	  instrType = doc1.getAttribute("/COPY_OF_INSTR_TYPE_CODE");
        	  instrTypeDescForDisplay = ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.INSTRUMENT_TYPE,instrType, userLocale);
   		 Debug.debug("EnterRecertificationPasswordVasco.jsp Debug Statement1 " + doc1.toString() );
   		  DocumentHandler instrResults = DatabaseQueryBean.getXmlResultSet(instrumentSQL+commaSeparatedTransOids+" ))", false, sqlParams);
   			  if(instrResults != null)
			 ReferenceID = instrResults.getAttribute("ResultSetRecord(0)/COMPLETE_INSTRUMENT_ID");
    		
			 Ccy = doc1.getAttribute("/COPY_OF_CURRENCY_CODE");   
			    
			 if(InstrumentType.DOMESTIC_PMT.equals(instrType))
			 {
				//jgadela R90 IR T36000026319 - SQL INJECTION FIX
			 	count = DatabaseQueryBean.getCount("domestic_payment_oid", "domestic_payment", "p_transaction_oid = ?", false , new Object[]{commaSeparatedTransOids});
			 	cnt = Integer.valueOf(count).toString();
			 }
			 
			 TransactionAmount = doc1.getAttribute("/COPY_OF_AMOUNT");
			 Debug.debug("EnterRecertificationPasswordVasco.jsp Debug Statement3 " + "instrTypeDescForDisplay->"+instrTypeDescForDisplay + " ReferenceID->"+ReferenceID + " Ccy->"+Ccy );

			String instrOid  = doc1.getAttribute("/P_INSTRUMENT_OID");
	        if(StringFunction.isNotBlank(instrOid))
	        	instrumentId = Integer.parseInt(instrOid);
	       //Create VascoKey when 1 item is selected from list view for Cash and Trade
      	   if(StringFunction.isNotBlank(TransactionAmount)){
      	   VascoKeyInfo=""+(Math.round(Double.parseDouble(TransactionAmount))+time+instrumentId); 
      	   }
          }	 
    	  if(!(null==instrument)){
    		  instrType = instrument.getAttribute("instrument_type_code");
    		  instrTypeDescForDisplay = ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.INSTRUMENT_TYPE,instrType, userLocale);
    	  }
      	  if(isReceivableInstrument) {
      	  	TransactionAmount = doc1.getAttribute("/TOTAL_INVOICE_AMOUNT");
      	  	Ccy = doc1.getAttribute("/CURRENCY_CODE");
      	  	String cntOfInvoices = doc1.getAttribute("/TOTAL_NUMBER_OF_INVOICES");
      	  	ReferenceID = doc1.getAttribute("/A_INSTRUMENT_OID");
      	  	//
      	  	//If Instrument ID is not available, display as NONE
      	  	if(StringFunction.isBlank(ReferenceID)){
      	  		ReferenceID = "NONE";
      	  	}
      	  	int cntInvoices=0;
      	  	if(StringFunction.isNotBlank(cntOfInvoices)){
      	  		cntInvoices=Integer.parseInt(cntOfInvoices);
      	  	}
      	  	cnt=""+cntOfInvoices; //IR#MHUM061343592 
       	   //IR#DEUM060139377 Create VascoKey when 1 item is selected from list view for RM
       	   if(StringFunction.isNotBlank(TransactionAmount)){
       	   VascoKeyInfo=""+(Math.round(Double.parseDouble(TransactionAmount))+time+cntInvoices);
       	   }
      	  }	
      
      	  if(isInvoiceMgntInstrument) {      		  
      	  	TransactionAmount = doc1.getAttribute("/AMOUNT");
      	  	Ccy = doc1.getAttribute("/CURRENCY");
      	  	//ReferenceID = doc1.getAttribute("/INVOICE_ID"); //Using invoice_id for reference id for invoices.
      	  	ReferenceID = "NONE"; //No Instrument ID for Invoices in the portal. Can't use invoice id because multiple invoices in a group
      	   	cnt = ""+invoicesInAGroup; //IR#MHUM061343592 
       	   //IR#DEUM060139377 Create VascoKey when 1 item is selected from list view for Invoices
       	   if(StringFunction.isNotBlank(TransactionAmount)){
       	   VascoKeyInfo=""+(Math.round(Double.parseDouble(TransactionAmount))+time+invoicesInAGroup);
       	   }
      	  }	
      	  if(isRMInvoiceInstrument){
      		TransactionAmount = doc1.getAttribute("/INVOICE_TOTAL_AMOUNT");
      	  	Ccy = doc1.getAttribute("/CURRENCY_CODE");
      	  	ReferenceID = doc1.getAttribute("/INVOICE_REFERENCE_ID");
      	  	//If Instrument ID is not available, display as NONE
      	  	if(StringFunction.isBlank(ReferenceID)){
      	  		ReferenceID = "NONE";
      	  	}
      	  	int cntInvoices=0;
      	   if(StringFunction.isNotBlank(TransactionAmount)){
       	   VascoKeyInfo=""+(Math.round(Double.parseDouble(TransactionAmount))+time+cntInvoices);
       	   }
      	  }
      	
      }	  
	
   }else {   
	   	  //It is not from a list, but detail page
		   if(!(null==terms))
	         Ccy = terms.getAttribute("amount_currency_code");	
	   	  //only do this for CASH instr, transaction may be null for rm..
	   	  //set cnt for cash
	   	  if(isCashInstrument){
		      if(InstrumentType.DOMESTIC_PMT.equals(transaction.getAttribute("copy_of_instr_type_code"))) {  
			    //Vshah - IR#MAUM060142680 - Rel8.0 - 06/04/2012 - <BEGIN>
			    //jgadela R91IR T36000026319 - SQL INJECTION FIX
	         	count = DatabaseQueryBean.getCount("domestic_payment_oid", "domestic_payment", "p_transaction_oid = ?", false, new Object[]{transaction.getAttribute("transaction_oid")});
	         	cnt = Integer.valueOf(count).toString();
		        //Vshah - IR#MAUM060142680 - Rel8.0 - 06/04/2012 - <END>	         
		      }
	   	  }
	   	  //set cnt for trade
	      if (isTradeInstrument) cnt="1";   
	      //These will be used to determine if a offline user entered on this page can authorize 
	      if(!(null==instrument)){
	        instrType    = instrument.getAttribute("instrument_type_code");
	      	instrTypeDescForDisplay = ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.INSTRUMENT_TYPE,instrType, userLocale);
	      }
	      if(!(null==transaction)){
	        transactionType   = transaction.getAttribute("transaction_type_code");
	        String instrOid  = transaction.getAttribute("instrument_oid");
	        if(StringFunction.isNotBlank(instrOid))
	        	instrumentId = Integer.parseInt(instrOid);
	      }
	      //Now set the Vasco key for cash and trade based on the instrumentId, time and transaction amount
	      //IR#DEUM060139377
	      if(isCashInstrument || isTradeInstrument){
		      if(!StringFunction.isBlank(TransactionAmount)){
		      	  VascoKeyInfo=""+(Math.round(Double.parseDouble(TransactionAmount))+time+instrumentId); //IR#DEUM060139377
		      }
	      }
	      
	       //get reAuthOids
	      //IR_DEUM060655441
	      //start
	      String parentNi = request.getParameter("parentNi");
	      if ( parentNi != null ) {
	         //try to get from transaction detail page
	         reAuthObjectOid = formMgr.getSecurePageParm(parentNi,"transaction_oid");
	         if (reAuthObjectOid==null) {
	            //try to get from pay remit detail page
	            reAuthObjectOid = formMgr.getSecurePageParm(parentNi,"pay_remit_oid");
	         }
	      }
	      allReAuthObjectOids = reAuthObjectOid;
	       
   }
   
  	   if ( allReAuthObjectOids != null && allReAuthObjectOids.length()>0 ) {
	      addlParms.put("reAuthObjectOids", allReAuthObjectOids);
	   }
 
	 //Need to Hash out values greater than 6 characters in length
    //
    //This is just initializing the byte array before using it below
    //
    byte[] hashedValue=EncryptDecrypt.createMD5Hash("000000".getBytes());	
    BigInteger iVal;
    //Process the VascoKeyInfo to remove any non-numeric characters (commas, etc)
    //because the type of VASCO keypad being used will accept only numeric charcaters
    //Note that if it is less than 6 characters and it is all 
    //numeric, the default plain values retrieved above will be used
    //
	VascoKeyInfo=VascoKeyInfo.replaceAll( "[^\\d]", "" );
    if(VascoKeyInfo.length()>6){
		hashedValue = EncryptDecrypt.createMD5Hash(VascoKeyInfo.getBytes());		
		iVal = new BigInteger(1, hashedValue);
		VascoKeyInfo= iVal.toString().substring(0,6);	
    }else if(VascoKeyInfo.length() < 6){
		//Pad the value with 0's if less than 6 in length
		do{
			VascoKeyInfo = "0"+VascoKeyInfo;
		}while(VascoKeyInfo.length()<6);
	}    
    
   addlParms.put("RecertificationIndicator", recertificationInd);
	
	 // Forward the user to the CompleteRecertification page to close the window, if a reponse is not received.
	// Subtract 15 seconds from the timeout period.
	// This way, the browser will forward to the timeout page before the session
	// is actually killed by the server
	 int timeoutInSeconds = SecurityRules.getTimeOutPeriodInSeconds() - 15;   
	 session.setAttribute("recertificationSuccessful", TradePortalConstants.INDICATOR_NO);	
	       
   %>

<%-- Body tag included as part of common header --%>
<jsp:include page="/common/Header.jsp">
  <jsp:param name="displayHeader" value="<%=TradePortalConstants.INDICATOR_NO%>" />
   <jsp:param name="minimalHeaderFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="brandingDirectory" value="<%=brandingDirectory%>" />
</jsp:include>
<%
  if (instanceHSMEnabled)
  {
    //cquinton 5/15/2013 ir#17092 update url so its actually pulled in
%>
<script src="../js/crypto/pidcrypt.js"></script>
<script src="../js/crypto/pidcrypt_util.js"></script>
<script src="../js/crypto/asn1.js"></script><%--needed for parsing decrypted rsa certificate--%>
<script src="../js/crypto/jsbn.js"></script><%--needed for rsa math operations--%>
<script src="../js/crypto/rng.js"></script><%--needed for rsa key generation--%>
<script src="../js/crypto/prng4.js"></script><%--needed for rsa key generation--%>
<script src="../js/crypto/rsa.js"></script><%--needed for rsa en-/decryption--%>
<script src="../js/crypto/md5.js"></script><%--needed for key and iv generation--%>
<script src="../js/crypto/aes_core.js"></script><%--needed block en-/decryption--%>
<script src="../js/crypto/aes_cbc.js"></script><%--needed for cbc mode--%>
<script src="../js/crypto/sha256.js"></script>
<script src="../js/crypto/tpprotect.js"></script><%--needed for cbc mode--%> 
<%
  } // if (instanceHSMEnabled)
%>
<%
  //cquinton 5/1/2013 Rel 8.1.0.6 ir#16256
  //populated userWebBean here. the page heading is also dependent on whether otp or transaction signing	

  //Determine if the user is set to use OTP for Transaction signing, if so, 
  //present a page that looks like the RSA Transaction singing page
  //
  userWebBean.getById(userSession.getUserOid());
  
  String pageHeaderKey = "";
  if (TradePortalConstants.AUTH_2FA_SUBTYPE_OTP.equalsIgnoreCase(userWebBean.getAttribute("auth2fa_subtype"))){
    pageHeaderKey = "Login.2FA";    
  }
  else {
    pageHeaderKey = "SignTransaction.SignTransaction";	
  }

  String focusFieldId = "passwork2FA";

    // W Zhu 12/7/2015 Rel 9.3.5 & 9.4.0.1 T36000045899 & T36000045908 use different phrasing for BTMU 
    String userClientBankOid = userWebBean.getAttribute("client_bank_oid");
    String BTMUeBizClientBankOid = ConfigSettingManager.getConfigSetting("PORTAL_APP", "PORTAL", "VASCO_BTMUeBiz_CB", "");
    String[] BTMUeBizClientBankOids = BTMUeBizClientBankOid.split(";");
    boolean isForBTMU = false;
    for (int i = 0; i < BTMUeBizClientBankOids.length; i++){
        if (userClientBankOid.equals(BTMUeBizClientBankOids[i])) {
            isForBTMU = true;
        }
    }

%>
 
<div class="dialogContent midwidth">
  <jsp:include page="/common/PageHeader.jsp">
    <jsp:param name="titleKey" value="<%= pageHeaderKey %>"/>
  </jsp:include>
  <form id="RecertificationForm" name="RecertificationForm" method="POST" action="<%= formMgr.getSubmitAction(response) %>"
data-dojo-type="dijit.form.Form">
    <div class="formContentNoSidebar">        
    
<%
  if (instanceHSMEnabled) {
%>
      <input type=hidden name="encryptedPassword2FA" value="">
      <input type=hidden name="encryptedAuthorizer" value="">
<%
  } // if (instanceHSMEnabled)
%>
    
      <br/>
	
      <%--this dummy field resolves an issue with internet explorer submitting the form
          on enter.  DO NOT REMOVE.  by including a 2nd text field the submission does not occur
          and the page correctly uses the dojo onkeypress event handler instead!--%>
      <%= widgetFactory.createTextField( "dummy", "", "", "1", false, false, false,
            "style=\"display:none;\"", "", "" ) %>

<%
  if (TradePortalConstants.AUTH_2FA_SUBTYPE_OTP.equalsIgnoreCase(userWebBean.getAttribute("auth2fa_subtype"))){
    //If so display the RSA page format
%>
      <div class="formItem">
        <%= resMgr.getText("Login.2FAInstructions",TradePortalConstants.TEXT_BUNDLE) %>
      </div>
      <br/>

      <%= widgetFactory.createTextField( "password2FA", "", "", "8", false, false, false,
            "type=\"password\" AutoComplete=\"off\" width=\"32\"", "", "" ) %>

      <button id="LoginButton" name="LoginButton" data-dojo-type="dijit.form.Button"
              type="button" class="formItem">
        <%=resMgr.getText("GuaranteeIssue.SubmitText",TradePortalConstants.TEXT_BUNDLE)%>
      </button>

<%
  } 
  else {
    //Otherwise display the VASCO page format

    if(StringFunction.isNotBlank(submitButtonName)){
      if (InstrumentServices.isAnOfflineButton(submitButtonName)) {
%>
        <span class="vascoSectionHeader"> 
          <%= resMgr.getText("SignTransaction.OfflineInstr1",TradePortalConstants.TEXT_BUNDLE) %>
        </span> 
<%
      } 
    }

  String authorizerLabelKey = "SignTransaction.UserID";
  String uid = userSession.getUserId();
  boolean isReadOnly=true;
  if(StringFunction.isNotBlank(submitButtonName)){
    if (InstrumentServices.isAnOfflineButton(submitButtonName)) {	        		  
      authorizerLabelKey = "SignTransaction.OfflineUserID";
      //If it is a proxy authorization, we'll make the field editable
      isReadOnly = false;
      uid="";
      focusFieldId = "authorizer";
    }
  } 
//IR T36000013570 start 
  String tempCurr = Ccy;
  if(StringFunction.isBlank(Ccy))
	  tempCurr = mixedPrecision;
//IR T36000013570 end 
  
%>
      <%= widgetFactory.createTextField( "authorizer", authorizerLabelKey, uid, "32", isReadOnly, false, false,
            "", "", "" ) %>
 
      <div class="indentHalf">
        <span class="vascoSectionHeader" style="width:77%;">
          <%= resMgr.getText("SignTransaction.TransactionDetails",TradePortalConstants.TEXT_BUNDLE) %>
        </span>
      </div>

      <div class="indentHalf">
        <%= widgetFactory.createTextField( "field1", "SignTransaction.InstrumentType", instrTypeDescForDisplay, "30", true, false, false,
              "", "", "inline" ) %>
        <%= widgetFactory.createTextField( "field2", "SignTransaction.InstrumentID", ReferenceID, "30", true, false, false,
              "", "", "inline" ) %>
        <%= widgetFactory.createTextField( "field3", "SignTransaction.CCY", Ccy, "30", true, false, false,
              "", "", "inline" ) %>
        <%= widgetFactory.createTextField( "field4", "SignTransaction.Amount", TPCurrencyUtility.getDisplayAmount(TransactionAmount,tempCurr,userLocale),
             "30", true, false, false, "", "", "inline" ) %>
        <%= widgetFactory.createTextField( "field5", "SignTransaction.Count", cnt, "30", true, false, false,
              "", "", "inline" ) %>
      </div>


  <%--Instructions 1 begin --%>      
           <%--AAlubala IRWZUM060733306 Display different text if it's offline  --%>            
	          <%if(StringFunction.isNotBlank(submitButtonName)){
	      		if (InstrumentServices.isAnOfflineButton(submitButtonName)) {%>
	      		<span class="vascoSectionHeader">
	      			<%= resMgr.getText("SignTransaction.OfflineInstr2",TradePortalConstants.TEXT_BUNDLE) %> 	      		 
	      		</span>
	      		<%}else{%>
	      		<span class="vascoSectionHeader">     
                        <%-- W Zhu 12/7/2015 Rel 9.3.5 & 9.4.0.1 T36000045899 & T36000045908 use different phrasing for BTMU --%>
                        <% if (!isForBTMU) {%>
                            <%= resMgr.getText("SignTransaction.Instractions1",TradePortalConstants.TEXT_BUNDLE) %> 
                        <%} else {%>
                            <%= resMgr.getText("SignTransaction.Instractions1.BTMU",TradePortalConstants.TEXT_BUNDLE) %> 
                        <% } %>
	           	</span>
	         <%} 
	         }%>         

  <%--Instructions 1 end --%>

  <%= widgetFactory.createTextField("keyInfo", "SignTransaction.KeyInfo", VascoKeyInfo, "20", true, false, false, "", "", "") %>

  <%--Instructions 2 begin --%>
           <%--AAlubala IRWZUM060733306 Display different text if it's offline  --%> 
	          <%if(StringFunction.isNotBlank(submitButtonName)){
	      		if (InstrumentServices.isAnOfflineButton(submitButtonName)) {%>	  
	      		<span class="vascoSectionHeader"> 		
	      			<%= resMgr.getText("SignTransaction.OfflineInstr3",TradePortalConstants.TEXT_BUNDLE) %>
	      		</span>	      		
	      		<%}else{%>  
	      		<span class="vascoSectionHeader">                
                        <%-- W Zhu 12/7/2015 Rel 9.3.5 & 9.4.0.1 T36000045899 & T36000045908 use different phrasing for BTMU --%>
                        <% if (!isForBTMU) {%>
                            <%= resMgr.getText("SignTransaction.Instractions2",TradePortalConstants.TEXT_BUNDLE) %> 
                        <%} else {%>
                            <%= resMgr.getText("SignTransaction.Instractions2.BTMU",TradePortalConstants.TEXT_BUNDLE) %> 
                        <% } %>
	           </span>
	         <%} 
	         }%>         
  <%--Instructions 2 end --%>  

           <%--AAlubala IR#LMUM053034272 rel8.0 -05/30/2012 maxlength changed from 7 to 8 for Offline Auth--%>
           <%
	          if(StringFunction.isNotBlank(submitButtonName)){
	      		if (InstrumentServices.isAnOfflineButton(submitButtonName)) {    %>       
      <%= widgetFactory.createTextField( "password2FA", "SignTransaction.SIGCode", "", "8", false, false, false,
            "type=\"password\" AutoComplete=\"off\" width=\"32\"", "", "" ) %>
	      		<%}else{ %>
      <%= widgetFactory.createTextField( "password2FA", "SignTransaction.SIGCode", "", "7", false, false, false,
            "type=\"password\" AutoComplete=\"off\" width=\"32\"", "", "" ) %>
	         <%
	          	}
	          }
	      	%>
	          <%--END IR#LMUM053034272 --%>		

      <button id="SubmitButton" name="Submit" data-dojo-type="dijit.form.Button"
              type="button" class="formItem">
        <%=resMgr.getText("Vasco.Submit",TradePortalConstants.TEXT_BUNDLE)%>
      </button>
      
      <button id="CancelButton" name="Cancel" data-dojo-type="dijit.form.Button"
              type="button" class="formItem">
        <%=resMgr.getText("Vasco.Cancel",TradePortalConstants.TEXT_BUNDLE)%>
      </button>
  
<%
 } //end for checking the user Auth subtype - OTP or SIG
   
 // Store the properties in a hashtable
 //TODO - Remove values that are no longer needed
addlParms.put("VascoKeyInfo", VascoKeyInfo);
addlParms.put("refID", ReferenceID);
addlParms.put("transAmnt", TransactionAmount);
addlParms.put("beneAccntNum", BeneAccntNumber);
addlParms.put("isCMTransaction", String.valueOf(isCashInstrument));
addlParms.put("submitedButtonName", submitButtonName);
addlParms.put("instrumentType",instrType);
addlParms.put("transactionType",transactionType);
  //
%>
<%= formMgr.getFormInstanceAsInputField("RecertificationForm", addlParms) %>


    </div>      
  </form>

<%
   formMgr.storeInDocCache("default.doc",new DocumentHandler());
%>
</div>
<%--cquinton 5/1/2013 Rel 8.1.0.6 ir#16256 start
    remove footer display--%>
<jsp:include page="/common/Footer.jsp">
  <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_NO%>" />
  <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_NO%>" />
</jsp:include>			
<%--cquinton 5/1/2013 Rel 8.1.0.6 ir#16256 end--%>

<script>
  <%--cquinton 5/1/2013 Rel 8.1.0.6 ir#16256 remove unnecessary heading manipulations
      this is now done in Header.jsp by passing displayHeader=N--%>

  document.title = '<%=resMgr.getText(pageHeaderKey,TradePortalConstants.TEXT_BUNDLE)%>';
  
  setTimeout("document.location.href='<%=formMgr.getLinkAsUrl("goToCompleteRecertification", response)%>'", <%=timeoutInSeconds%>000);

  var local = {};

<%
  if (instanceHSMEnabled) {
%>
  function encryptHsmFields() {
    if (typeof(TPProtect) != 'undefined') {
      var public_key = '-----BEGIN PUBLIC KEY-----\n<%= HSMEncryptDecrypt.getPublicKey() %>\n-----END PUBLIC KEY-----';
      var iv = '<%= HSMEncryptDecrypt.getNewIV() %>';
      var protector = new TPProtect();
      RecertificationForm.encryptedPassword2FA.value = protector.Encrypt({publicKey: public_key, usePem: true, iv: iv, data: RecertificationForm.password2FA.value});
      RecertificationForm.password2FA.value = "";
      <%--cquinton 5/15/2013 Rel 8.2 ir#17092 avoid non-existent authorizer field issues--%>
      if ( RecertificationForm.authorizer ) {
        RecertificationForm.encryptedAuthorizer.value = protector.Encrypt({publicKey: public_key, usePem: true, iv: iv, data: RecertificationForm.authorizer.value});
        RecertificationForm.authorizer.value = "";						
      }
    }
  }
<%
  } // if (instanceHSMEnabled)
%>

  require(["dijit/registry", "dojo/on", "t360/common", "dojo/ready"],
      function(registry, on, common, ready) {

    <%--cquinton 2/27/2013 refactor common login logic--%>
    local.authenticate = function() {
      common.setButtonPressed('<%= TradePortalConstants.BUTTON_LOGIN %>','0');
<%
  if (instanceHSMEnabled) {
%>
      encryptHsmFields();
<%
  } // if (instanceHSMEnabled)
%>
      registry.byId("RecertificationForm").submit();
    };

    local.authenticateOnKeyPress = function(event) {
      if (event && event.keyCode == 13) {
        local.authenticate();
      }
    };

    ready(function() {
      <%--set focus--%>
      var focusField = registry.byId("<%=focusFieldId%>");
      if ( focusField ) {
        focusField.focus();
      }

      <%--register event handlers--%>
<%
  if (TradePortalConstants.AUTH_2FA_SUBTYPE_OTP.equalsIgnoreCase(userWebBean.getAttribute("auth2fa_subtype"))){
%>
      var b = registry.byId("LoginButton");
      if (b) {
        b.on("click", function() {
          local.authenticate();
        });
      }
<%
  }
  else {
%>
      var b = registry.byId("SubmitButton");
      if (b) {
        b.on("click", function() {
          local.authenticate();
        });
      }
      var b2 = registry.byId("CancelButton");
      if (b2) {
        b2.on("click", function() {
          window.close();
        });
      }
<%
  }
%>
      <%--for ie, we need an event handler for enter--%>
      <%--cquinton 2/27/2013 move handler to text boxes rather than
          the whole form due to manual button click issues in IE--%>
      registry.byId("password2FA").on("keyPress", function(event) {
        local.authenticateOnKeyPress(event);
      });

    });
  });
</script>
 </body>
</html>
