<%--
 *
 *     Copyright  © 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 *	   CR-804 Rel8.4 01/02/2014
--%>
<%@ page import="com.amsinc.ecsg.web.*,com.amsinc.ecsg.util.*,com.amsinc.ecsg.frame.*,com.ams.tradeportal.mediator.*,com.ams.tradeportal.common.*,com.ams.tradeportal.html.*,java.util.*" %>

<jsp:useBean id="formMgr"     class="com.amsinc.ecsg.web.FormManager"            scope="session" />
<jsp:useBean id="resMgr"      class="com.amsinc.ecsg.util.ResourceManager"       scope="session" />
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session" />


<%

	
	
  WidgetFactory widgetFactory = new WidgetFactory(resMgr);
  String version = resMgr.getText("version", "TradePortalVersion");
  if (version.equals("version")) {
    version = resMgr.getText("Footer.UnknownVersion", 
                             TradePortalConstants.TEXT_BUNDLE);
  }
 	version = EncryptDecrypt.encryptStringUsingTripleDes(version, userSession.getSecretKey());
    version = version.substring(0, 4);
    
	formMgr.setCurrPage("RsaNewPin");

	DocumentHandler doc = formMgr.getFromDocCache();
	Hashtable addlParms = new Hashtable();
	String brandingDirectory;
	String organizationId;
	String loginId = "";
	DocumentHandler brandedButtonDoc;
	String brandedButtonInd = null;
	String brandedButtonDirectory = null;
	boolean isUserPin = false;
	if(doc.getAttribute("/Out/PinState").equals("1") || doc.getAttribute("/Out/PinState").equals("2")){
		isUserPin = true;
	}

	if ((doc != null) && (doc.getAttribute("/In/User/loginId") != null)) {

		// Get page parameters from the XML that was returned from the mediator
		brandingDirectory = doc.getAttribute("/In/brandingDirectory");
		organizationId = doc.getAttribute("/In/organizationId");
		loginId = doc.getAttribute("/In/User/loginId");
		brandedButtonInd = doc.getAttribute("/In/brandbutton");

		// Determine which login attempt this is (the first, the second, the third, etc)
		int loginAttemptNumber;

		try {
			loginAttemptNumber = Integer.parseInt(doc.getAttribute("/In/loginAttemptNumber2FA"));

		} catch (Exception e) {
			loginAttemptNumber = 0;
		}		
		// If the logon failed, and they have already tried to log in the maximum
		// number of times, redirect them to another page
		if (loginAttemptNumber == SecurityRules.getLoginAttemptsBeforeBeingRedirected()) {

			StringBuffer parmsFromLogon = new StringBuffer();
			parmsFromLogon.append("?branding=");
			parmsFromLogon.append(brandingDirectory);
			parmsFromLogon.append("&organization=");
			parmsFromLogon.append(organizationId);
			parmsFromLogon.append("&locale=");
			parmsFromLogon.append(resMgr.getResourceLocale());

			String tooManyLogonsPage = NavigationManager.getNavMan().getPhysicalPage("TooManyInvalidLogonAttempts", request);

%>
		<jsp:forward page='<%=tooManyLogonsPage + parmsFromLogon%>' />
	   <%
	   	}

	   		// Increment the login attempt number
	   		addlParms.put("loginAttemptNumber2FA",
	   				String.valueOf(loginAttemptNumber));
	   	} else {
	   		// Get page parameters from the URL parameters
	   		brandingDirectory = StringFunction.xssCharsToHtml(request.getParameter("branding")); //T36000004579 #3;
	   		organizationId = request.getParameter("organization");
	   		brandedButtonInd = request.getParameter("brandbutton");

	   		// This is the first time in a row that they are logging in, so set count to 1
	   		addlParms.put("loginAttemptNumber2FA", "1");

	   		formMgr.storeInDocCache("default.doc", new DocumentHandler());
	   	}

	   	// Retrieve brand_button_ind from the request parameter.  
	   	// If it is null, get from client bank
	   	if (brandedButtonInd == null) {
	   	 //jgadela R91 IR T36000026319 - SQL INJECTION FIX
	   		String sqlQuery = "select brand_button_ind from client_bank where otl_id = ?";
	   		brandedButtonDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery, false, new Object[]{organizationId});

	   		if (brandedButtonDoc != null) {
	   			brandedButtonDoc = (DocumentHandler) brandedButtonDoc.getFragments("/ResultSetRecord").elementAt(0);
	   			brandedButtonInd = brandedButtonDoc.getAttribute("/BRAND_BUTTON_IND");
	   		}
	   	}
	   	if (brandedButtonInd != null && brandedButtonInd.equals(TradePortalConstants.INDICATOR_YES)) {
	   		brandedButtonDirectory = brandingDirectory;
	   	}

	   	// This flag indicates that the password must be validated.
	   	addlParms.put("auth", TradePortalConstants.AUTH_2FA);
	   	addlParms.put("brandingDirectory", brandingDirectory);
	   	addlParms.put("organizationId", organizationId);
	   	addlParms.put("locale", resMgr.getResourceLocale());
	   	addlParms.put("loginId", loginId);
	   	addlParms.put("brandbutton", brandedButtonInd);
	  	//params to store NewRSAPin and Pinstate as well as pin validation criteria
	   	addlParms.put("LogonStatus", "RSANewPin");
	   	addlParms.put("PinState", doc.getAttribute("/Out/PinState"));
	   	addlParms.put("NewRSAPin", doc.getAttribute("/Out/NewRSAPin"));
	   	addlParms.put("FailedNewPinAttempt", doc.getAttribute("/Out/FailedNewPinAttempt"));	   	
	   	if(isUserPin){
	   		addlParms.put("MaxPinlen", doc.getAttribute("/Out/MaxPinlen"));
		   	addlParms.put("MinPinlen", doc.getAttribute("/Out/MinPinlen"));
		   	addlParms.put("IsPinAlphaNum", doc.getAttribute("/Out/IsPinAlphaNum"));
	   	}
	   %>


<%-- Body tag included as part of common header --%>
<jsp:include page="/common/Header.jsp">
   <jsp:param name="minimalHeaderFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="brandingDirectory" value="<%=brandingDirectory%>" />
</jsp:include>


<div class="pageMainNoSidebar">
  <div id="mainPageContent" class="pageContent">

		<jsp:include page="/common/PageHeader.jsp">
			<jsp:param name="titleKey" value="Login.RsaNewPin" />
		</jsp:include>

		<form id="RsaNewPin" name="RsaNewPin" method="POST" data-dojo-type="dijit.form.Form"
			action="<%=formMgr.getSubmitAction(response)%>">

  <jsp:include page="/common/ErrorSection.jsp" />
			<div class="formContentNoSidebar">
				<div class="padded">
					
					<%=formMgr.getFormInstanceAsInputField("LogonForm",
					addlParms)%>
			<% if (!isUserPin){ %>
					<%=widgetFactory.createTextField("NewRSAPin","Login.NewPin",doc.getAttribute("/Out/NewRSAPin"),"30",true,false,false,"","", "")%>
					<div class="instruction formItem">
       			 		<%= resMgr.getText("Login.NewPinInstruction",TradePortalConstants.TEXT_BUNDLE) %>
     				 </div>
					<%} else {%>
					<div class="instruction formItem">
       			 		<%= resMgr.getText("Login.UserPinInstruction1",TradePortalConstants.TEXT_BUNDLE) %> <%=doc.getAttribute("/Out/MinPinlen")%><%= resMgr.getText("Login.UserPinInstruction2",TradePortalConstants.TEXT_BUNDLE) %> <%=doc.getAttribute("/Out/MaxPinlen")%><%= resMgr.getText("Login.UserPinInstruction3",TradePortalConstants.TEXT_BUNDLE) %> <%=doc.getAttribute("/Out/IsPinAlphaNum").equalsIgnoreCase("true")?"alphanumeric.":"numeric."%>
     				 </div>
     				 <%-- DK IR T36000024175 Rel8.4 01/16/2014 --%>
     				 <%-- DK IR T36000025225 Rel8.4 02/18/2014 --%>
					<%=widgetFactory.createTextField("NewRSAPin","Login.NewRsaPin",doc.getAttribute("/Out/NewRSAPin"),"30",false,false,false,"type=\"password\" AutoComplete=\"off\" width=\"30\"","", "")%>
					<%=widgetFactory.createTextField("ConfirmNewRSAPin","Login.ConfirmNewRsaPin",doc.getAttribute("/Out/ConfirmNewRSAPin"),"30",false,false,false,"type=\"password\" AutoComplete=\"off\" width=\"30\"","", "")%>
					<%} %>					
     				 
					<button id="PinSave" data-dojo-type="dijit.form.Button"
						type="submit" class="formItem" onclick="return clickSubmit()">
						<%=resMgr.getText("common.SaveText",TradePortalConstants.TEXT_BUNDLE)%>
					</button>
				</div>
			</div>
		</form>

		<%
			formMgr.storeInDocCache("default.doc", new DocumentHandler());
		%>
	</div>
</div>
<script src="<%=userSession.getdojoJsPath()%>/dojoconfig.js?<%= version %>"></script>
<script src='<%=userSession.getdojoJsPath()%>/dojo/dojo.js?<%= version %>' data-dojo-config="async: 1" ></script>
<script>
require(["dojo/parser", "dojo/dom-class",
         "dojo/query", "dojo/dom","dojo/NodeList-dom",
         "dijit/form/Form","dijit/form/Button",
         "dijit/form/TextBox","dijit/form/ValidationTextBox",
	     "t360/widget/Tooltip"], function(parser, domClass, query,dom){
	
	parser.parse(dom.byId("mainPageContent"));
       domClass.add(document.body, 'loaded');
});
</script>

<script src="<%=userSession.getdojoJsPath()%>/common.js"></script>
<script>
				function clickSubmit() {	
 					registry.byId("RsaNewPin").submit();
				}
		 
	require(["dijit/registry", "dojo/on", "t360/common", "dojo/ready"],
      function(registry, on, common, ready) {
    ready(function() {

      <%-- var focusField = registry.byId("UserEnterdPin"); --%>
      <%-- focusField.focus(); --%>
	 
  
   <%--for ie, we need an event handler for enter--%>
      registry.byId("LogonForm").on("keyPress", function(event) {
        if (event && event.keyCode == 13) {
          registry.byId("RsaNewPin").submit();
        }
      });
    });
  });
		 
</script>
</body>
</html>