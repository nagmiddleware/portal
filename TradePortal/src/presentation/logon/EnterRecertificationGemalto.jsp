<%--
 *     This page will present the user with Certificate Gemalto Device to sign the Transaction.
 *	   Rel 9.4 CR-923
--%>
<%@ page import="com.amsinc.ecsg.web.*, com.amsinc.ecsg.frame.*, com.amsinc.ecsg.util.*,
                 com.ams.tradeportal.common.*, com.ams.tradeportal.mediator.util.*, com.ams.tradeportal.busobj.util.*, com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.Transaction, com.ams.tradeportal.html.*, java.math.BigInteger, java.util.*,java.util.Date,java.text.*" %>
                 
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>
<jsp:useBean id="formMgr"     class="com.amsinc.ecsg.web.FormManager"            scope="session"></jsp:useBean>
<jsp:useBean id="resMgr"      class="com.amsinc.ecsg.util.ResourceManager"       scope="session"></jsp:useBean>
<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session"></jsp:useBean>

<%

        // Save some simple variables to secure parms
   String orgId               = request.getParameter("organization");
   String brandingDirectory   = StringFunction.xssCharsToHtml(request.getParameter("branding")); //T36000006718 check xss
   String locale              = request.getParameter("locale");
   String bankDate            = request.getParameter("date");
   String certData            = request.getParameter("info");
   String submitButtonName    = request.getParameter("submitButtonName");
   Hashtable addlParms = new Hashtable();
   addlParms.put("brandingDirectory", brandingDirectory);
   addlParms.put("organizationId", orgId);
   addlParms.put("locale", locale);   
   if (!(InstrumentServices.isBlank(bankDate))) {
       addlParms.put("date", bankDate);
   }   
   if (!(InstrumentServices.isBlank(certData))) {
       addlParms.put("info", certData);
   } 
   //addlParms.put("auth", TradePortalConstants.AUTH_2FA);
   addlParms.put("auth", TradePortalConstants.AUTH_CERTIFICATE);
   addlParms.put("CertificateType", TradePortalConstants.GEMALTO);
   addlParms.put("loginId", userSession.getUserId());
   String recertificationInd = TradePortalConstants.INDICATOR_YES;
   //set submitButtonName in session for CompleteRecertification.jsp
   session.setAttribute("submitButtonName", submitButtonName);

    //Date date = new Date();
    java.util.Date todaysDate = new java.util.Date();
    //
    //IR#DEUM060139377 - Base the Key information on only day month and year parts of the date
    //and not time component, this is to enable the key to be the same within the same day
    String DATE_FORMAT = "ddMMyyyy";
    SimpleDateFormat formatter = new SimpleDateFormat(DATE_FORMAT);
    String formattedDate = formatter.format(todaysDate);
    long time = Long.parseLong(formattedDate);  
   
    boolean isCashInstrument=false;
    boolean isTradeInstrument=false;
    boolean isReceivableInstrument=false;
    boolean isInvoiceMgntInstrument=false;
    boolean isRMInvoiceInstrument=false;
    String VascoKeyInfo="";
   String mixedPrecision = "";
   
   
   String physicalPage        = "";
   String payRemitOid		  = "";
   WidgetFactory widgetFactory = new WidgetFactory(resMgr);
   boolean instanceHSMEnabled = HSMEncryptDecrypt.isInstanceHSMEnabled();
   
  String userLocale         = userSession.getUserLocale();
  
  //This is the instrument type code
  String instrType    = "";
  //This is the instrument type description for display purposes
  String instrTypeDescForDisplay    = "";
  String transactionType   = "";
  String Ccy="";
  String cnt="1";
  String referenceNumber = "";
  
   String TransactionAmount="0";
   String instrumentID="0";
   String allReAuthObjectOids = "";
   String reAuthObjectOid = "";   // W Zhu 30 Apr 2013 T36000016258 check the validity of instrument and transaction bean.  transaction bean is empty if this is called from pending transaction list page.
   int instrumentOid=0; //instrument_oid to be used in key computation for CASH and TRADE

   TransactionWebBean transaction  = (TransactionWebBean)beanMgr.getBean("Transaction");
   InstrumentWebBean instrument    = (InstrumentWebBean) beanMgr.getBean("Instrument");
   TermsWebBean terms              = (TermsWebBean) beanMgr.getBean("Terms");  

   // If for Pay Remit
   payRemitOid = (String)session.getAttribute("SessionPayRemitOid");
   String reAuthObjectType    = request.getParameter("reAuthObjectType");
   
   /**************************************************
    * 1. Coming from single transaction details page for
    *     Trade or Cash instrument
    ***************************************************/
   if(instrument != null && transaction != null){
	   
  	String instrumentType="";
       int count=0;
       //
       instrumentType = instrument.getAttribute("instrument_type_code");
       if(InstrumentServices.isCashManagementTypeInstrument(instrumentType)) isCashInstrument=true;
       if(InstrumentServices.isTradeTypeInstrument(instrumentType)) isTradeInstrument=true;
       instrumentID = instrument.getAttribute("complete_instrument_id"); //corresponds to the instrument id
       transactionType   = transaction.getAttribute("transaction_type_code");
       TransactionAmount = transaction.getAttribute("copy_of_amount");
       if(InstrumentServices.isBlank(TransactionAmount)) TransactionAmount="0";
       referenceNumber = instrument.getAttribute("copy_of_ref_num");
	   	  //It is not from a list, but detail page
		  //
          if(!(null==terms))        Ccy = terms.getAttribute("amount_currency_code");	
          //
          //only do this for CASH instr, transaction may be null for rm..
          //set cnt for cash
          if(isCashInstrument){
              if(InstrumentType.DOMESTIC_PMT.equals(transaction.getAttribute("copy_of_instr_type_code"))) {  
                    //Vshah - IR#MAUM060142680 - Rel8.0 - 06/04/2012 - <BEGIN>
                    //jgadela R91IR T36000026319 - SQL INJECTION FIX
                count = DatabaseQueryBean.getCount("domestic_payment_oid", "domestic_payment", "p_transaction_oid = ?", false, new Object[]{transaction.getAttribute("transaction_oid")});
                cnt = Integer.valueOf(count).toString();
                //Vshah - IR#MAUM060142680 - Rel8.0 - 06/04/2012 - <END>	         
              }
          }
          else  if (isTradeInstrument) {
              cnt="1";   
          }
         instrType    = instrument.getAttribute("instrument_type_code");
         //
         instrTypeDescForDisplay = ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.INSTRUMENT_TYPE,instrType, userLocale);
         transactionType   = transaction.getAttribute("transaction_type_code");
         String instrOid  = transaction.getAttribute("instrument_oid");
         if(InstrumentServices.isNotBlank(instrOid))    instrumentOid = Integer.parseInt(instrOid);
          //
          //Now set the Vasco key for cash and trade based on the instrumentOid, time and transaction amount
          //IR#DEUM060139377
          if(!InstrumentServices.isBlank(TransactionAmount)){
              VascoKeyInfo=""+(Math.round(Double.parseDouble(TransactionAmount))+time+instrumentOid); //IR#DEUM060139377
          }

          //
          //get reAuthOids
          //IR_DEUM060655441
          //start
          String parentNi = request.getParameter("parentNi");
          if ( parentNi != null ) {
             //try to get from transaction detail page
             reAuthObjectOid = formMgr.getSecurePageParm(parentNi,"transaction_oid");
             if (reAuthObjectOid==null) {
                //try to get from pay remit detail page
                reAuthObjectOid = formMgr.getSecurePageParm(parentNi,"pay_remit_oid");
             }
          }
          allReAuthObjectOids = reAuthObjectOid;
	      
   } // if(instrument != null && transaction != null){
   /**************************************************
    * 2. Coming from single transaction details page for
    *     Receivables Payables instrument
    ***************************************************/
   else if(!InstrumentServices.isBlank(payRemitOid)){
       	double TotalInvoiceAmnt=0;
	int CntOfInvoice=0;
        PayRemitWebBean payRemitWebBean  = beanMgr.createBean(PayRemitWebBean.class, "PayRemit");
        payRemitWebBean.getById(payRemitOid);

       //
       String TotalInvoiceAmount = payRemitWebBean.getAttribute("total_invoice_amount");
       //
       TransactionAmount = TotalInvoiceAmount;
       String CountOfInvoice = payRemitWebBean.getAttribute("total_number_of_invoices");
       if(InstrumentServices.isNotBlank(TotalInvoiceAmount)) TotalInvoiceAmnt=Double.parseDouble(TotalInvoiceAmount);
       if(InstrumentServices.isNotBlank(CountOfInvoice)) CntOfInvoice=Integer.parseInt(CountOfInvoice);
       //Vasco Key = sum of time, total invoice amount and count of invoice
         //IR#DEUM060139377
       VascoKeyInfo = ""+(time+(Math.round(TotalInvoiceAmnt))+CntOfInvoice);		
       cnt = CountOfInvoice;
       Ccy = payRemitWebBean.getAttribute("currency_code");
       instrumentID = payRemitWebBean.getAttribute("instrument_oid");
        //
        //If Instrument ID is not available, display as NONE
        if(InstrumentServices.isBlankOrWhiteSpace(instrumentID)){
                instrumentID = "NONE";
        }		   

           instrType = InstrumentType.RECEIVABLES_PAYBLES_MANAGEMENT;
         //IR#LAUM060134426 - Look up the instrument type desc for the code
        instrTypeDescForDisplay = ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.INSTRUMENT_TYPE,instrType, userLocale);
		   
    }
   else {

   /**************************************************
    * 3. Coming from transaction list page 
    * Trade or Cash instrument
    * Receivables
    * Invoices
    * RM invoices
    ***************************************************/
   
  //cr710
  InvoiceGroupWebBean invoiceGroupWebBean = null;
  InvoicesSummaryDataWebBean invoicesSummaryWebbean = null;
//jgadela R91IR T36000026319 - SQL INJECTION FIX
  Object sqlParams[] = null;
  //
    TermsPartyWebBean	   termsPartyPayee = beanMgr.createBean(TermsPartyWebBean.class,"TermsParty");
  
   String BeneAccntNumber="";
   String instrumentTypeCode = "";
   
   boolean isARMInstrument = false;
   boolean isRFInstrument = false;
   
   int reAuthObjectIdx = 0;
   //String reAuthObjectOid = request.getParameter("reAuthObjectOid"+reAuthObjectIdx);
   reAuthObjectOid = request.getParameter("checkbox"+reAuthObjectIdx);
   int invoicesInAGroup=1;
   //IR T36000013570 start
   String mixCurrencySql1 = "select distinct addl_value,code from refdata where table_type='CURRENCY_CODE' and addl_value is not null and code in (";
   String mixCurrencySql2  = ") order by addl_value desc";
   //IR T36000013570 end
   String rmInvoiceTransaction = "select invoice_oid, invoice_reference_id , currency_code, invoice_total_amount "
  + " from invoice where invoice_oid in (";    
   
   String instrumentSQL = "select complete_instrument_id from instrument where instrument_oid in (select p_instrument_oid from transaction where transaction_oid in ( ";
   String rmTransaction = "select M.MATCHED_AMOUNT, M.INVOICE_MATCHING_STATUS, M.PAY_MATCH_RESULT_OID from PAY_MATCH_RESULT M where M.P_PAY_REMIT_OID in ( ";
   
   rmTransaction  = "select PAYMENT_AMOUNT, TOTAL_INVOICE_AMOUNT, A_INSTRUMENT_OID,CURRENCY_CODE, TOTAL_NUMBER_OF_INVOICES "
        + " from PAY_REMIT "
        + " where PAY_REMIT_OID in ( ";
        
  String invoiceTransaction = "select upload_invoice_oid, invoice_id , currency, amount, linked_to_instrument_type "
  + " from invoices_summary_data where upload_invoice_oid in ()";    

  //invoiceTransaction = "select sum(amount) as AMOUNT, currency, linked_to_instrument_type, upload_invoice_oid, invoice_id from invoices_summary_data where a_invoice_group_oid in ( ";
  invoiceTransaction = "select AMOUNT, currency, linked_to_instrument_type, upload_invoice_oid, invoice_id from invoices_summary_data where a_invoice_group_oid in ( ";
  //IR T36000019419 start- if grouping is not enabled then where condition should have upload_invoice_oid instead of invoice_group_oid
 // CorporateOrganizationWebBean corpOrg = beanMgr.createBean(CorporateOrganizationWebBean.class, "CorporateOrganization");
 // corpOrg.getById(orgId);
 // boolean groupingEnabled = TradePortalConstants.INVOICES_GROUPED_TPR.equals(corpOrg.getAttribute("allow_grouped_invoice_upload"));
  if("Invoice".equals(reAuthObjectType)){
	  invoiceTransaction = "select AMOUNT, currency, linked_to_instrument_type, upload_invoice_oid, invoice_id from invoices_summary_data where upload_invoice_oid in ( ";
  }
  //IR T36000019419 end
   String theType = "";
   DocumentHandler listDoc;
   DocumentHandler instrumentDoc;
   DocumentHandler payeeDoc;
   String refIds = "";
   String accntAmnts = "";
   double sumAccntAmounts=0.0;
   String beneAccnts = "";
   String concatenatedAmnts="";
   String concatenatedRefIds="";
   String concatenatedBeneAccnts="";
   String termsPartyOid="";
   String commaSeparatedTransOids="";
   String commaSeparatedInstrOids="";
   int countOfTransactions=0;
   DocumentHandler doc1 = null;
   //The current date (numerical equivalent) to be used as part of 
   //vacso key computation
	//
	//END IR#DEUM060139377
	//
   //
   //Determine the Reauth Type
   //
   if(InstrumentServices.isNotBlank(reAuthObjectType)){
	   if(reAuthObjectType.equalsIgnoreCase("PayRemit")){
		   isReceivableInstrument = true;
	   }else if(reAuthObjectType.equalsIgnoreCase("Invoice") || reAuthObjectType.equalsIgnoreCase("InvoiceG")){
		   isInvoiceMgntInstrument = true;
		   //IR#DEUM060655441 - Invoices from Invoice Management page need to be differentiated
		   //from older RM invoices since they use different Beans
		   //this will be used later on in mediator bean for error handling
		   reAuthObjectType="InvoiceMgnt";
	   }
	   else if(reAuthObjectType.equalsIgnoreCase("RMInvoice")){
		   isRMInvoiceInstrument = true;
		   }
   }
   //IR_ DEUM060655441


   // Construct a comma-delimited string of the tranaction OIDs.  Used in SQL.
   if ( reAuthObjectOid != null ) {
      //if not null it means we have a list
      do {
    	 // PR ER-T36000014931 REL 8.1.0.4. Changed the below line form 'encryptStringUsingTripleDes' to 'decryptStringUsingTripleDes', 
    	 // The 'reAuthObjectOid' is encrypted (rowKeys) and hence needs to be decrypted, otherwise throws error
		 String checkBoxValue = EncryptDecrypt.decryptStringUsingTripleDes(reAuthObjectOid, userSession.getSecretKey());
		 StringTokenizer parser = new StringTokenizer(checkBoxValue, "/");
		 int tokenCnt=0;
		 String parserToken;
		 countOfTransactions++;
		 //
		 //Only need to get the first token from the parser
		 //Here we are creating a comma separated list of transaction oids (or pay_remit_oids if Receivables Management)
		 //to be used in the SQL statements
		 while (parser.hasMoreTokens() && tokenCnt < 1){
		       //
		       //The first item from the parser will be transaction_oid for cash and trade
		       //transactions and for receivables payment matches, it will be pay_remit_oid
		       if(tokenCnt==0){
		    	   if(commaSeparatedTransOids.length()<1){
		    		   parserToken = parser.nextToken();
		    		   commaSeparatedTransOids = commaSeparatedTransOids + parserToken;
		    		   allReAuthObjectOids = commaSeparatedTransOids;
		    	   }else{
		    		   parserToken = parser.nextToken();
		    	   	   commaSeparatedTransOids = commaSeparatedTransOids + "," + parserToken; 
		    	   	   allReAuthObjectOids += "!" + parserToken;
		    	   }
		       }
			   tokenCnt++; 
			   
		
		 }
		 //invoicesInAGroup=tokenCnt;		//if this is an invoice group, then the tokens will correspond to the invoices in the group 
         reAuthObjectIdx++;
      } while ( ( reAuthObjectOid = request.getParameter("checkbox"+reAuthObjectIdx) ) != null );
      //
      //
      //Total selected transactions (when coming from a list page)
      //
      cnt = ""+countOfTransactions;
	  //
      //now run the SQL queries with the list of comma separated values from above
      //then loop on the results to create concatenated values
      //
      //If it is an RM instrument, use the RM SQL and obtain the matched amount else use the Trans SQL and get copy of amount
   /**************************************************
    * 3.1. Coming from transaction list page 
    * Receivables
    ***************************************************/
       if(isReceivableInstrument){
		   instrType = InstrumentType.RECEIVABLES_PAYBLES_MANAGEMENT;
		 //jgadela R91IR T36000026319 - SQL INJECTION FIX
	      listDoc = DatabaseQueryBean.getXmlResultSet(rmTransaction+commaSeparatedTransOids+" ) ",false, sqlParams);
	      if(null != listDoc){
		   	  Vector transactionFragments = listDoc.getFragments("/ResultSetRecord");
			  int numItems = transactionFragments.size();
			  for (int i = 0; i < numItems; i++) {
				doc1 = (DocumentHandler) transactionFragments.elementAt(i);
				accntAmnts = doc1.getAttribute("/TOTAL_INVOICE_AMOUNT");
				concatenatedAmnts = concatenatedAmnts + accntAmnts;				
				if(InstrumentServices.isNotBlank(accntAmnts)){
					//sum the amounts
					sumAccntAmounts = sumAccntAmounts + Double.parseDouble(accntAmnts);
				}
				//IR T36000013570 start
				if(StringFunction.isNotBlank(doc1.getAttribute("/CURRENCY_CODE"))){
					mixCurrencySql1 = mixCurrencySql1 +"'"+ SQLParamFilter.filter(doc1.getAttribute("/CURRENCY_CODE")) +"'";
					if(i+1 !=numItems)
						mixCurrencySql1 = mixCurrencySql1+",";
					else{
						mixCurrencySql1 = mixCurrencySql1+mixCurrencySql2;
					}
				}
				//IR T36000013570 end
			  }		  
	      } 
	      //IR#LAUM060134426 - Look up the instrument type desc for the code
	      instrTypeDescForDisplay = ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.INSTRUMENT_TYPE,instrType, userLocale);
	      
      }else if(isInvoiceMgntInstrument){
   /**************************************************
    * 3.2 Coming from transaction list page 
    * Invoices
    ***************************************************/
		   //The query below with GROUP BY was giving a syntax error in some cases
    	//jgadela R91IR T36000026319 - SQL INJECTION FIX
	      listDoc = DatabaseQueryBean.getXmlResultSet(invoiceTransaction+commaSeparatedTransOids+" ) ",false, sqlParams);
   
	      if(null != listDoc){
		   	  Vector transactionFragments = listDoc.getFragments("/ResultSetRecord");
			  int numItems = transactionFragments.size();
			  for (int i = 0; i < numItems; i++) {
				doc1 = (DocumentHandler) transactionFragments.elementAt(i);
				accntAmnts = doc1.getAttribute("/AMOUNT");
				concatenatedAmnts = concatenatedAmnts + accntAmnts;				
				if(InstrumentServices.isNotBlank(accntAmnts)){
					//sum the amounts
					sumAccntAmounts = sumAccntAmounts + Double.parseDouble(accntAmnts);
				}
				//IR T36000013570 start
				if(StringFunction.isNotBlank(doc1.getAttribute("/CURRENCY"))){
					mixCurrencySql1 = mixCurrencySql1 +"'"+ SQLParamFilter.filter(doc1.getAttribute("/CURRENCY")) +"'";
					if(i+1 !=numItems)
						mixCurrencySql1 = mixCurrencySql1+",";
					else{
						mixCurrencySql1 = mixCurrencySql1+mixCurrencySql2;
					}
				}
				//IR T36000013570 end
			  }	
			  //IR#MHUM061343592 - Count for invoices when a single item is selected should match the invoices in a group.
			  invoicesInAGroup=numItems;		//if this is an invoice group, then the fragments will correspond to the invoices in the group 	  
	      } 
	      //set the instrument type
	      instrType = TradePortalConstants.INV_LINKED_INSTR_REC;
	    //IR#LAUM060134426 - Look up the instrument type desc for the code
	      instrTypeDescForDisplay = ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.INSTRUMENT_TYPE,instrType, userLocale);

      }
      else if(isRMInvoiceInstrument){
   /**************************************************
    * 3.3 Coming from transaction list page 
    * RM invoices
    ***************************************************/
    	//jgadela R91IR T36000026319 - SQL INJECTION FIX
	      listDoc = DatabaseQueryBean.getXmlResultSet(rmInvoiceTransaction+commaSeparatedTransOids+" ) ",false, sqlParams);
  
	      if(null != listDoc){
		   	  Vector transactionFragments = listDoc.getFragments("/ResultSetRecord");
			  int numItems = transactionFragments.size();
			  for (int i = 0; i < numItems; i++) {
				doc1 = (DocumentHandler) transactionFragments.elementAt(i);
				accntAmnts = doc1.getAttribute("/INVOICE_TOTAL_AMOUNT");
				concatenatedAmnts = concatenatedAmnts + accntAmnts;				
				if(StringFunction.isNotBlank(accntAmnts)){
					//sum the amounts
					sumAccntAmounts = sumAccntAmounts + Double.parseDouble(accntAmnts);
				}
				//IR T36000013570 start
				if(StringFunction.isNotBlank(doc1.getAttribute("/CURRENCY_CODE"))){
					mixCurrencySql1 = mixCurrencySql1 +"'"+ SQLParamFilter.filter(doc1.getAttribute("/CURRENCY_CODE")) +"'";
					if(i+1 !=numItems)
						mixCurrencySql1 = mixCurrencySql1+",";
					else{
						mixCurrencySql1 = mixCurrencySql1+mixCurrencySql2;
					}
				}
				//IR T36000013570 end
			  }	
			  //IR#MHUM061343592 - Count for invoices when a single item is selected should match the invoices in a group.
			  invoicesInAGroup=numItems;		//if this is an invoice group, then the fragments will correspond to the invoices in the group 	  
	      } 
	      //set the instrument type
	      instrType = TradePortalConstants.INV_LINKED_INSTR_REC;
	    //IR#LAUM060134426 - Look up the instrument type desc for the code
	      instrTypeDescForDisplay = ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.INSTRUMENT_TYPE,instrType, userLocale);

     }
      else{
   /**************************************************
    * 3.4 Coming from transaction list page 
    * Trade or Cash instrument
    ***************************************************/
		  //
	      //Transaction SQL to get Transaction Amounts:
	    	  //jgadela R91IR T36000026319 - SQL INJECTION FIX
              StringBuilder transactionSQL = new StringBuilder()
                    .append("select copy_of_instr_type_code, copy_of_currency_code, max(complete_instrument_id) as complete_instrument_id, max(copy_of_ref_num) as copy_of_ref_num, sum(copy_of_amount) as copy_of_amount, count(*) as the_count")
                    .append(" from transaction, instrument where transaction_oid in (").append(commaSeparatedTransOids).append(")")
                    .append(" and instrument.instrument_oid = transaction.p_instrument_oid")
                    .append(" group by copy_of_instr_type_code, copy_of_currency_code")
                    .append(" order by copy_of_instr_type_code, copy_of_currency_code");
	      listDoc = DatabaseQueryBean.getXmlResultSet(transactionSQL.toString(), false, sqlParams);
	      if(null != listDoc){
		   	  Vector transactionFragments = listDoc.getFragments("/ResultSetRecord");
			  int numItems = transactionFragments.size();
			  for (int i = 0; i < numItems; i++) {
                              if (i == 0) {
                                  instrTypeDescForDisplay = "";
                                  instrumentID = "";
                                  Ccy = "";
                                  cnt = "";
                                  TransactionAmount = "";
                                  referenceNumber = "";
                              }
                              if (i > 0) {
                                  instrTypeDescForDisplay += "<br/>";
                                  instrumentID += "<br/>";
                                  Ccy += "<br/>";
                                  cnt += "<br/>";
                                  TransactionAmount += "<br/>";
                                  referenceNumber += "<br/>";
                              }
				doc1 = (DocumentHandler) transactionFragments.elementAt(i);
				accntAmnts = doc1.getAttribute("/COPY_OF_AMOUNT");
				//concatenate the values
				concatenatedAmnts = concatenatedAmnts + accntAmnts;		
                                
                                instrType = doc1.getAttribute("/COPY_OF_INSTR_TYPE_CODE");
                                instrTypeDescForDisplay += ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.INSTRUMENT_TYPE,instrType, userLocale);
                                int thecount = Integer.parseInt(doc1.getAttribute("/THE_COUNT"));
                                cnt += doc1.getAttribute("/THE_COUNT");
                                
                                if (thecount > 1) {
                                  instrumentID += resMgr.getText("TransactionSigning.MultipleTransactions", TradePortalConstants.TEXT_BUNDLE);
                                  referenceNumber += resMgr.getText("TransactionSigning.MultipleTransactions", TradePortalConstants.TEXT_BUNDLE);
                                }
                                else {
                                  instrumentID += doc1.getAttribute("/COMPLETE_INSTRUMENT_ID");
                                  referenceNumber += doc1.getAttribute("/COPY_OF_REF_NUM");
                                }
                                String currency = doc1.getAttribute("/COPY_OF_CURRENCY_CODE");
                                Ccy += currency;   
			    
                                TransactionAmount += TPCurrencyUtility.getDisplayAmount(accntAmnts,currency,userLocale);
                                
				//sum the amounts
				if(InstrumentServices.isNotBlank(accntAmnts)){
					sumAccntAmounts = sumAccntAmounts + Double.parseDouble(accntAmnts);
				}
			  }		  
	      }
      }
      
      //
      //The key will be a hash sum of account amounts, the number of transactions
      //and the numeric equivalent of today's date
      //  //IR#DEUM060139377
     VascoKeyInfo=""+(Math.round(sumAccntAmounts)+time+countOfTransactions);
     if(isInvoiceMgntInstrument || isReceivableInstrument || isRMInvoiceInstrument) {
      TransactionAmount = ""+sumAccntAmounts;
      }
      
      //Vshah - IR#DAUM060129002 - Rel8.0 - 06/05/2012 - <BEGIN>
      //Commented the below lines and re-written it.
      //Instrument type and ID will be 'Multiple Transactions' string
      //instrTypeDescForDisplay = "Multiple Transactions";
      //instrumentID = "Multiple Transactions";
      if (countOfTransactions > 1) {
          if(isInvoiceMgntInstrument || isReceivableInstrument ||isRMInvoiceInstrument)
          {
              //Instrument type and ID will be 'Multiple Transactions' string
              //MEer IR-32869 Getting "Multiple Transactions" text from resource bundle for localization.
            //  instrTypeDescForDisplay = "Multiple Transactions";
              instrTypeDescForDisplay = resMgr.getText("TransactionSigning.MultipleTransactions", TradePortalConstants.TEXT_BUNDLE);
            //  instrumentID = "Multiple Transactions";
              instrumentID = resMgr.getText("TransactionSigning.MultipleTransactions", TradePortalConstants.TEXT_BUNDLE);
              Ccy = "";
            //IR T36000013570 start 
            //jgadela R91IR T36000026319 - SQL INJECTION FIX
            DocumentHandler mixPrecisionDoc = DatabaseQueryBean.getXmlResultSet(mixCurrencySql1.toString(), false, sqlParams);

            if (mixPrecisionDoc != null) {
                        mixedPrecision = mixPrecisionDoc.getAttribute("ResultSetRecord[0]/CODE");   
            }
            //IR T36000013570 end
          }
      }else{
          if(!isInvoiceMgntInstrument && !isReceivableInstrument &&!isRMInvoiceInstrument)
          {
        	  instrType = doc1.getAttribute("/COPY_OF_INSTR_TYPE_CODE");
        	  instrTypeDescForDisplay = ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.INSTRUMENT_TYPE,instrType, userLocale);
   		 Debug.debug("EnterRecertificationPasswordVasco.jsp Debug Statement1 " + doc1.toString() );
   		  DocumentHandler instrResults = DatabaseQueryBean.getXmlResultSet(instrumentSQL+commaSeparatedTransOids+" ))", false, sqlParams);
   		  //Debug.debug("EnterRecertificationPasswordVasco.jsp Debug Statement2 " + instrumentSQL+commaSeparatedTransOids + " ))");
   	 	  //Debug.debug("EnterRecertificationPasswordVasco.jsp Debug Statement 2a " + instrResults.toString() );
    		  if(instrResults != null) instrumentID = instrResults.getAttribute("ResultSetRecord(0)/COMPLETE_INSTRUMENT_ID");
    		
                 Ccy = doc1.getAttribute("/COPY_OF_CURRENCY_CODE");   

                 if(InstrumentType.DOMESTIC_PMT.equals(instrType))
                 {
                        //jgadela R90 IR T36000026319 - SQL INJECTION FIX
                        int count = DatabaseQueryBean.getCount("domestic_payment_oid", "domestic_payment", "p_transaction_oid = ?", false , new Object[]{commaSeparatedTransOids});
                        cnt = Integer.valueOf(count).toString();
                 }

                 TransactionAmount = doc1.getAttribute("/COPY_OF_AMOUNT");
                 Debug.debug("EnterRecertificationPasswordVasco.jsp Debug Statement3 " + "instrTypeDescForDisplay->"+instrTypeDescForDisplay + " instrumentID->"+instrumentID + " Ccy->"+Ccy );
                             
			 //
	        String instrOid  = doc1.getAttribute("/P_INSTRUMENT_OID");
	        if(InstrumentServices.isNotBlank(instrOid))   	instrumentOid = Integer.parseInt(instrOid);
	        //
               //Create VascoKey when 1 item is selected from list view for Cash and Trade
               if(StringFunction.isNotBlank(TransactionAmount)){
                    VascoKeyInfo=""+(Math.round(Double.parseDouble(TransactionAmount))+time+instrumentOid); 
                }
               TransactionAmount =  TPCurrencyUtility.getDisplayAmount(TransactionAmount,Ccy,userLocale);
               
          }	 
    	  if(!(null==instrument)){
    		  instrType = instrument.getAttribute("instrument_type_code");
    		  instrTypeDescForDisplay = ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.INSTRUMENT_TYPE,instrType, userLocale);
    	  }
      	  //TransactionAmount = TPCurrencyUtility.getDisplayAmountWithoutCommaSeperator(TransactionAmount, doc1.getAttribute("/COPY_OF_CURRENCY_CODE"), resMgr.getResourceLocale());
      	  if(isReceivableInstrument) {
      	  	TransactionAmount = doc1.getAttribute("/TOTAL_INVOICE_AMOUNT");
      	  	Ccy = doc1.getAttribute("/CURRENCY_CODE");
      	  	String cntOfInvoices = doc1.getAttribute("/TOTAL_NUMBER_OF_INVOICES");
      	  	instrumentID = doc1.getAttribute("/A_INSTRUMENT_OID");
      	  	//
      	  	//If Instrument ID is not available, display as NONE
      	  	if(InstrumentServices.isBlankOrWhiteSpace(instrumentID)){
      	  		instrumentID = "NONE";
      	  	}
      	  	int cntInvoices=0;
      	  	if(InstrumentServices.isNotBlank(cntOfInvoices)){
      	  		cntInvoices=Integer.parseInt(cntOfInvoices);
      	  	}
      	  	cnt=""+cntOfInvoices; //IR#MHUM061343592 
       	   //IR#DEUM060139377 Create VascoKey when 1 item is selected from list view for RM
       	   if(StringFunction.isNotBlank(TransactionAmount)){
       	   VascoKeyInfo=""+(Math.round(Double.parseDouble(TransactionAmount))+time+cntInvoices);
       	   }
      	  }	
      
      	  if(isInvoiceMgntInstrument) {      		  
      	  	TransactionAmount = doc1.getAttribute("/AMOUNT");
      	  	Ccy = doc1.getAttribute("/CURRENCY");
      	  	//instrumentID = doc1.getAttribute("/INVOICE_ID"); //Using invoice_id for reference id for invoices.
      	  	instrumentID = "NONE"; //No Instrument ID for Invoices in the portal. Can't use invoice id because multiple invoices in a group
      	  	cnt = ""+invoicesInAGroup; //IR#MHUM061343592 
       	   //IR#DEUM060139377 Create VascoKey when 1 item is selected from list view for Invoices
       	   if(StringFunction.isNotBlank(TransactionAmount)){
       	   VascoKeyInfo=""+(Math.round(Double.parseDouble(TransactionAmount))+time+invoicesInAGroup);
       	   }
      	  }	
      	  if(isRMInvoiceInstrument){
      		TransactionAmount = doc1.getAttribute("/INVOICE_TOTAL_AMOUNT");
      	  	Ccy = doc1.getAttribute("/CURRENCY_CODE");
      	  //	String cntOfInvoices = doc1.getAttribute("/TOTAL_NUMBER_OF_INVOICES");
      	  	instrumentID = doc1.getAttribute("/INVOICE_REFERENCE_ID");
      	  	//
      	  	//If Instrument ID is not available, display as NONE
      	  	if(InstrumentServices.isBlankOrWhiteSpace(instrumentID)){
      	  		instrumentID = "NONE";
      	  	}
      	  	int cntInvoices=0;
      	  /*	if(InstrumentServices.isNotBlank(cntOfInvoices)){
      	  		cntInvoices=Integer.parseInt(cntOfInvoices);
      	  	}
      	  	cnt=""+cntOfInvoices;*/
       	   if(StringFunction.isNotBlank(TransactionAmount)){
       	   VascoKeyInfo=""+(Math.round(Double.parseDouble(TransactionAmount))+time+cntInvoices);
       	   }
      	  }
      	  //

      }	  
	  //Vshah - IR#DAUM060129002 - Rel8.0 - 06/05/2012 - <END> 
	  
   }
   }
   
   //add to secureparms
   //IR_ DEUM060655441
	   if ( allReAuthObjectOids != null && allReAuthObjectOids.length()>0 ) {
	      addlParms.put("reAuthObjectOids", allReAuthObjectOids);
	   }
     	if (reAuthObjectType != null) {
   		addlParms.put("reAuthObjectType", reAuthObjectType);
   	}


	//
    //Need to Hash out values greater than 6 characters in length
    //
    //This is just initializing the byte array before using it below
    //
    byte[] hashedValue=EncryptDecrypt.createMD5Hash("000000".getBytes());	
    BigInteger iVal;
    //
    //Process the VascoKeyInfo to remove any non-numeric characters (commas, etc)
    //because the type of VASCO keypad being used will accept only numeric charcaters
    //Note that if it is less than 6 characters and it is all 
    //numeric, the default plain values retrieved above will be used
    //
	VascoKeyInfo=VascoKeyInfo.replaceAll( "[^\\d]", "" );
    if(VascoKeyInfo.length()>6){
		hashedValue = EncryptDecrypt.createMD5Hash(VascoKeyInfo.getBytes());		
		iVal = new BigInteger(1, hashedValue);
		VascoKeyInfo= iVal.toString().substring(0,6);	
    }else if(VascoKeyInfo.length() < 6){
		//Pad the value with 0's if less than 6 in length
		do{
			VascoKeyInfo = "0"+VascoKeyInfo;
		}while(VascoKeyInfo.length()<6);
	}    
    
   addlParms.put("RecertificationIndicator", recertificationInd);
	
	 // Forward the user to the CompleteRecertification page to close the window, if a reponse is not received.
	// Subtract 15 seconds from the timeout period.
	// This way, the browser will forward to the timeout page before the session
	// is actually killed by the server
	 int timeoutInSeconds = SecurityRules.getTimeOutPeriodInSeconds() - 15;   
	 session.setAttribute("recertificationSuccessful", TradePortalConstants.INDICATOR_NO);	
	
	       
   %>

<%-- Body tag included as part of common header --%>
<jsp:include page="/common/Header.jsp">
  <jsp:param name="displayHeader" value="<%=TradePortalConstants.INDICATOR_NO%>" />
   <jsp:param name="minimalHeaderFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="brandingDirectory" value="<%=brandingDirectory%>" />
</jsp:include>
<%
  if (instanceHSMEnabled)
  {
    //cquinton 5/15/2013 ir#17092 update url so its actually pulled in
%>
<script src="../js/crypto/pidcrypt.js"></script>
<script src="../js/crypto/pidcrypt_util.js"></script>
<script src="../js/crypto/asn1.js"></script><%--needed for parsing decrypted rsa certificate--%>
<script src="../js/crypto/jsbn.js"></script><%--needed for rsa math operations--%>
<script src="../js/crypto/rng.js"></script><%--needed for rsa key generation--%>
<script src="../js/crypto/prng4.js"></script><%--needed for rsa key generation--%>
<script src="../js/crypto/rsa.js"></script><%--needed for rsa en-/decryption--%>
<script src="../js/crypto/md5.js"></script><%--needed for key and iv generation--%>
<script src="../js/crypto/aes_core.js"></script><%--needed block en-/decryption--%>
<script src="../js/crypto/aes_cbc.js"></script><%--needed for cbc mode--%>
<script src="../js/crypto/sha256.js"></script>
<script src="../js/crypto/tpprotect.js"></script><%--needed for cbc mode--%> 
<%
  } // if (instanceHSMEnabled)
%>
<%
  //cquinton 5/1/2013 Rel 8.1.0.6 ir#16256
  //populated userWebBean here. the page heading is also dependent on whether otp or transaction signing	

  //Determine if the user is set to use OTP for Transaction signing, if so, 
  //present a page that looks like the RSA Transaction singing page
  //
  UserWebBean   userWebBean = beanMgr.createBean(UserWebBean.class, "User");
  userWebBean.getById(userSession.getUserOid());
  
  String pageHeaderKey = "SignTransaction.SignTransaction";	

%>
 
<div class="dialogContent midwidth">
  <jsp:include page="/common/PageHeader.jsp">
    <jsp:param name="titleKey" value="<%= pageHeaderKey %>"/>
  </jsp:include>
    <div class="formContentNoSidebar">        
    
<%
  if (instanceHSMEnabled) {
%>
      <input type=hidden name="encryptedPassword2FA" value="">
      <input type=hidden name="encryptedAuthorizer" value="">
<%
  } // if (instanceHSMEnabled)
%>
    
      <br/>
	
      <%--this dummy field resolves an issue with internet explorer submitting the form
          on enter.  DO NOT REMOVE.  by including a 2nd text field the submission does not occur
          and the page correctly uses the dojo onkeypress event handler instead!--%>
      <%= widgetFactory.createTextField( "dummy", "", "", "1", false, false, false,
            "style=\"display:none;\"", "", "" ) %>

<%
    //Otherwise display the VASCO page format


  String authorizerLabelKey = "SignTransaction.UserID";
  String uid = userSession.getUserId();
  boolean isReadOnly=true;
  String tempCurr = Ccy;
  if(StringFunction.isBlank(Ccy))
	  tempCurr = mixedPrecision;
   
     // Store the properties in a hashtable
    addlParms.put("VascoKeyInfo", VascoKeyInfo);
  
%>
      <%= widgetFactory.createTextField( "authorizer", authorizerLabelKey, uid, "32", isReadOnly, false, false,
            "", "", "" ) %>
 
      <div class="indentHalf">
        <span class="vascoSectionHeader" style="width:77%;">
          <%= resMgr.getText("SignTransaction.TransactionDetails",TradePortalConstants.TEXT_BUNDLE) %>
        </span>
      </div>
        <table><tr><td>
      <div class="indentHalf">
          
        <%= widgetFactory.createTextField( "field1", "SignTransaction.InstrumentType", instrTypeDescForDisplay, "30", true, false, false,
              "", "", "inline" ) %>
        <%= widgetFactory.createTextField( "field2", "SignTransaction.InstrumentID", instrumentID, "30", true, false, false,
              "", "", "inline" ) %>
        <%= widgetFactory.createTextField( "field3", "SignTransaction.CCY", Ccy, "30", true, false, false,
              "", "", "inline" ) %>
        <% // For multiple instruments of Cash or Trade, the transaction amount is already formatted.
        if ((instrument == null || transaction == null) && !isInvoiceMgntInstrument && !isReceivableInstrument &&!isRMInvoiceInstrument) {
            %>
        <%= widgetFactory.createTextField( "field4", "SignTransaction.Amount", TransactionAmount,
             "30", true, false, false, "", "", "inline label-right" ) %>
        <%} else { %>
        <%= widgetFactory.createTextField( "field4", "SignTransaction.Amount", TPCurrencyUtility.getDisplayAmount(TransactionAmount,tempCurr,userLocale),
             "30", true, false, false, "", "", "inline" ) %>
        <% } %>
        <% // For Cash or Trade, display customer reference number
        if (!isInvoiceMgntInstrument && !isReceivableInstrument &&!isRMInvoiceInstrument) {
            %>
            <%= widgetFactory.createTextField( "field6", "SignTransaction.CustomerReferenceNumber", referenceNumber,
             "30", true, false, false, "", "", "inline" ) %>
        <% } %>
        <%= widgetFactory.createTextField( "field5", "SignTransaction.Count", cnt, "30", true, false, false,
              "", "", "inline" ) %>
      </div>
        </td><td></table>
      <div class="indentHalf">
          <table ><tr>
                  <td style="vertical-align:top;"">
                      &nbsp;&nbsp;&nbsp;
<EMBED
WIDTH='70'                          
HEIGHT='25'
TYPE='application/x-esigner'
MIME_TYPE='text/plain'
IN_EXPECTEDVERSION=200
CFG_GUI_BUTTON_MODE=1 
CFG_SIGN_UNSEEN=1
CFG_GUI_BUTTON_NAME='<%=resMgr.getText("Vasco.Submit",TradePortalConstants.TEXT_BUNDLE)%>'
CFG_SIGNATURE_FORMAT=PKCS7_detached
IN_DATA='<%=VascoKeyInfo%>'
REPLY_TID='TXN001'
REPLY_URL='<%= formMgr.getLinkAsUrl ("goToTradePortalRecertification", response, addlParms)%>'
>
                  </td>
                  <td style="vertical-align:top;">
<%-- Use non-dojo style button to be more consisten with Gemalto button --%>
      <button id="CancelButton" name="Cancel" OnClick="window.close();"
              type="button" class="formItem" style="height: 25px;background-color: rgb(198, 195, 198)">
          <b><%=resMgr.getText("Vasco.Cancel",TradePortalConstants.TEXT_BUNDLE)%></b>
      </button>
                  </td>
    </tr></table>
              </div>
        <div class="indentHalf">
      <%-- test code --%>
      <%-- = formMgr.getLinkAsHref ("goToTradePortalRecertification", "Test Submit", "&Signature_Data=test", response, addlParms)--%> 

        </div>

    </div>      

<%
   formMgr.storeInDocCache("default.doc",new DocumentHandler());
%>
</div>
<jsp:include page="/common/Footer.jsp">
  <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_NO%>" />
  <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_NO%>" />
</jsp:include>			

<script>
  document.title = '<%=resMgr.getText(pageHeaderKey,TradePortalConstants.TEXT_BUNDLE)%>';
  window.onbeforeunload = function () {
        window.opener.unBusySubmitButton();
    }
  setTimeout("document.location.href='<%=formMgr.getLinkAsUrl("goToCompleteRecertification", response)%>'", <%=timeoutInSeconds%>000);

</script>
 </body>
</html>
