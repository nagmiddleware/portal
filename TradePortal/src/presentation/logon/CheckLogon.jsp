<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.web.*, com.amsinc.ecsg.frame.*, com.amsinc.ecsg.util.*,com.ams.tradeportal.html.*,
                 com.ams.tradeportal.common.*, com.ams.tradeportal.mediator.util.*, com.ams.tradeportal.busobj.webbean.*,
                 java.io.*, java.net.*, javax.crypto.*, javax.crypto.spec.SecretKeySpec, java.util.*, com.amsinc.ecsg.frame.PerfStatConfigManager" %>

<jsp:useBean id="formMgr"     class="com.amsinc.ecsg.web.FormManager"            scope="session"></jsp:useBean>
<jsp:useBean id="resMgr"      class="com.amsinc.ecsg.util.ResourceManager"       scope="session"></jsp:useBean>
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>
<%-- Tang Rel9.3.5 CR#1032 Session Synch -[Begin] --%>
<jsp:useBean id="beanMgr"     class="com.amsinc.ecsg.web.BeanManager"            scope="session"></jsp:useBean>
<%-- Tang Rel9.3.5 CR#1032 Session Synch -[End] --%>
<%



   TradePortalSessionListener   listener                 = null;
   DocumentHandler              logoutDoc                = new DocumentHandler();
   DocumentHandler              xmlDoc                   = formMgr.getFromDocCache();
   String                       orgAutoLCCreateIndicator = null;
   String                       orgManualPOIndicator     = null;
   String                       userSecurityType         = null;
   String                       baseCurrencyCode         = null;
   String                       organizationName         = null;
   String                       bankOrgGroupOid          = null;
   String                       brandingDirectory        = null;
   String                       brandButtonInd           = null;
   String                       clientBankOid            = null;
   String                       globalOrgOid             = null;
   String                       clientBankCode           = null;
   String 				  physicalPage             = null;
   String						timeoutAutoSaveInd		 = null;
   String                       orgAutoATPCreateIndicator = null;
   String                       orgATPManualPOIndicator   = null;
   String orgId               = request.getParameter("organization");
   //CR 821 Rel 8.3 START
   String 						lastLoginTimestamp 		= null;	
   String 						custNotIntgTPS 			= null;
   String 						enableAdminUpdateCentre 			= null;
   // Tang Rel9.3.5 CR#1032 Session Synch -[Begin]
   String                       sessionSynchImg          = null;		   
   // Tang Rel9.3.5 CR#1032 Session Synch -[End]
   
   	if (xmlDoc != null){
   		userSession.setServerName(xmlDoc.getAttribute("/Out/serverName"));
   	}
   //CR 821 Rel 8.3 END
   // Check to see if the user has been locked out
   // If so, forward them to the user locked out page, passing along parameters
   if ( (xmlDoc != null && xmlDoc.getAttribute("/Out/lockedOut") != null) &&
	 (xmlDoc.getAttribute("/Out/lockedOut").equals("true") ) )
	{
       StringBuffer parmsFromLogon = new StringBuffer();
	   parmsFromLogon.append("?branding=");
	   parmsFromLogon.append(xmlDoc.getAttribute("/In/brandingDirectory"));
	   parmsFromLogon.append("&organization=");
	   parmsFromLogon.append(xmlDoc.getAttribute("/In/organizationId"));
	   parmsFromLogon.append("&locale=");
	   parmsFromLogon.append(xmlDoc.getAttribute("/In/locale"));

	   String tooManyLogonsPage = NavigationManager.getNavMan().getPhysicalPage("UserLockedOut", request);
	   
	   //Ravindra - 21/01/2011 - CR-541 - Start
	   //Ravindra - 03/31/2011 - CUUL032965287 - Start
	   String authMethod = xmlDoc.getAttribute("/In/auth");

	   if (TradePortalConstants.AUTH_AES256.equals(authMethod)){
	   		String user=xmlDoc.getAttribute("/In/User/ssoId");
			   // 604 is CMA specific status code to notate "User is locked out"
			   // response.setStatus(HttpResponseUtility.getResponseCode( orgId, user, HttpResponseUtility.LOCKED_USER ) );
	 	   
				// T36000013446- use custom http header to communicate CMA specific status codes - start
			   String customStatus = Integer.toString(HttpResponseUtility.getResponseCode( orgId, user, HttpResponseUtility.LOCKED_USER ));
	 		   response.addHeader(TradePortalConstants.CUSTOM_STATUS_HEADER,customStatus);
	 		   
	 		   
	 		// T36000013446- use custom http header to communicate CMA specific status codes - end
	   }
	   //Ravindra - 03/31/2011 - CUUL032965287 - End
	   //Ravindra - 21/01/2011 - CR-541 - End
	 %>
	   <jsp:forward page='<%= tooManyLogonsPage + parmsFromLogon %>' />
	 <%
	}
   //CR-804 Rel8.4 01/02/2014 starts
   if ( xmlDoc != null && TradePortalConstants.INDICATOR_YES.equals(xmlDoc.getAttribute("/Out/ForwardToEnterPassword2FA")) 
		   || (xmlDoc != null && ("EnterPassword2FA".equals(xmlDoc.getAttribute("/Out/LogonStatus"))
				   || "EnterPassword2FANextCode".equals(xmlDoc.getAttribute("/Out/LogonStatus")))))  {
	   StringBuffer parmsFromLogon = new StringBuffer();
	   parmsFromLogon.append("?branding=");
	   parmsFromLogon.append(xmlDoc.getAttribute("/In/brandingDirectory"));
	   parmsFromLogon.append("&organizationId=");
	   parmsFromLogon.append(xmlDoc.getAttribute("/In/organizationId"));
	   parmsFromLogon.append("&locale=");
	   parmsFromLogon.append(xmlDoc.getAttribute("/In/locale"));

	   String password2FAPage = NavigationManager.getNavMan().getPhysicalPage("EnterPassword2FA", request);
	 %>
	   <jsp:forward page='<%= password2FAPage + parmsFromLogon %>' />
	 <%
      
  }
   else if (xmlDoc != null && "RSANewPin".equals(xmlDoc.getAttribute("/Out/LogonStatus")))  
   {
       StringBuffer parmsFromLogon = new StringBuffer();
	   parmsFromLogon.append("?branding=");
	   parmsFromLogon.append(xmlDoc.getAttribute("/In/brandingDirectory"));
	   parmsFromLogon.append("&organizationId=");
	   parmsFromLogon.append(xmlDoc.getAttribute("/In/organizationId"));
	   parmsFromLogon.append("&locale=");
	   parmsFromLogon.append(xmlDoc.getAttribute("/In/locale"));

	   String password2FAPage = NavigationManager.getNavMan().getPhysicalPage("RsaNewPin", request);
	 %>
	   <jsp:forward page='<%= password2FAPage + parmsFromLogon %>' />
	 <%
     
   }
   //CR-804 Rel8.4 01/02/2014 ends
   else if (xmlDoc != null && "PasswordLogon".equals(xmlDoc.getAttribute("/Out/LogonStatus")))  
   {
     StringBuffer parmsFromLogon = new StringBuffer();
	   parmsFromLogon.append("?branding=");
	   parmsFromLogon.append(xmlDoc.getAttribute("/In/brandingDirectory"));
	   parmsFromLogon.append("&organizationId=");
	   parmsFromLogon.append(xmlDoc.getAttribute("/In/organizationId"));
	   parmsFromLogon.append("&locale=");
	   parmsFromLogon.append(xmlDoc.getAttribute("/In/locale"));

	   String password2FAPage = NavigationManager.getNavMan().getPhysicalPage("EnterPassword", request);
	 %>
	   <jsp:forward page='<%= password2FAPage + parmsFromLogon %>' />
	 <%
     
   }
   // If the user logged in successfully, create and initialize objects in the
   // session for this user
   if ( (xmlDoc != null && xmlDoc.getAttribute("/Out/logonSuccesful") != null) &&
	  (xmlDoc.getAttribute("/Out/logonSuccesful").equals("true")) )
   {
      //cquinton 10/4/2012 Rel portal refresh ir#6032 start
      //moved session reset to first thing so active user session set to new session not old
      //W Zhu 8/20/2012 Rel 8.1 T36000004579 #2 reset the session ID.  This prevents the Session Fixation security vulnerability.
      Debug.debug("CheckLogon: getting old session id");
      HttpSession theOldSession = request.getSession(false);
      Debug.debug("CheckLogon: old session id: " + theOldSession.getId());
      // copy the session values to a temp Map and then to the new session.  
      // Once the session is invalidated its values are no longer accessible.  So need to save them aside temporarily.
      HashMap<String, Object> attributes = new java.util.HashMap<String, Object>();
      for (Enumeration e = theOldSession.getAttributeNames() ; e.hasMoreElements() ;) {
         String attributeName = (String)e.nextElement();
         attributes.put(attributeName, theOldSession.getAttribute(attributeName));
      }
      //IR - T36000013820. save maxInactiveInterval of the old session so that it can be set to newly created session
      int timeoutSecs = theOldSession.getMaxInactiveInterval();
      theOldSession.invalidate();
      HttpSession theNewSession = request.getSession( true );
      for (Map.Entry<String, Object> entry : attributes.entrySet()) {
        theNewSession.setAttribute(entry.getKey(), entry.getValue());
      }
       //IR - T36000013820. set maxinactive interval to new session
      theNewSession.setMaxInactiveInterval(timeoutSecs);
      
      Debug.debug("CheckLogon: new session id: " + theNewSession.getId());
      //cquinton 10/4/2012 Rel portal refresh ir#6032 end

      // Create the listener to check for session timeout.
      listener = new TradePortalSessionListener();
      //cquinton 2/1/2012 Rel 8.0 ppx255 start
      //add the active user session identifier to the session listener
      listener.setActiveUserSessionOid(xmlDoc.getAttribute("/Out/activeUserSessionOid"));
      //cquinton 2/1/2012 Rel 8.0 ppx255 end
      listener.setServerLocation(formMgr.getServerLocation());
      
 		// T36000013446- use custom http header to communicate CMA specific status codes - start
      String user=xmlDoc.getAttribute("/In/User/ssoId");
      String authMethod = xmlDoc.getAttribute("/In/auth");


      if (TradePortalConstants.AUTH_AES256.equals(authMethod)){
          String customStatus = Integer.toString(HttpResponseUtility.getResponseCode( orgId, user, HttpResponseUtility.LOGON_SUCCESS ));
      	  response.addHeader(TradePortalConstants.CUSTOM_STATUS_HEADER,customStatus);
      }
		// T36000013446- use custom http header to communicate CMA specific status codes - end

      // Save the user's userOid to the logout doc so that the user will be logged out
      // once his/her session is over/timed out.
      logoutDoc.setAttribute("/In/User/user_oid",
                             xmlDoc.getAttribute("/Out/User/user_oid"));
      //cquinton 2/1/2012 Rel 8.0 ppx255 start
      logoutDoc.setAttribute("/In/activeUserSessionOid",
                             xmlDoc.getAttribute("/Out/activeUserSessionOid"));
      //cquinton 2/1/2012 Rel 8.0 ppx255 end

      listener.setLogoutDoc(logoutDoc);
      listener.setUserOid(xmlDoc.getAttribute("/Out/User/user_oid"));
      //cquinton 2/1/2012 Rel 8.0 ppx255 start
      //add the active user session identifier to user session
      userSession.setActiveUserSessionOid(xmlDoc.getAttribute("/Out/activeUserSessionOid"));
      //cquinton 2/1/2012 Rel 8.0 ppx255 end
       
      // Putting it in the session prevents it from being garbage collected
      theNewSession.setAttribute("TimeoutListener", listener); //cquinton 10/4/2012 ir#6032 - set to new session

      // Set the current page we're about to be forwarded to in form manager
      formMgr.setCurrPage("TradePortalHome");

      // Set the resource manager's locale based on the user's locale.
      resMgr.setResourceLocale(xmlDoc.getAttribute("/Out/User/locale"));

      // Now save any info that came back from the LogonMediator to the user's session
      organizationName         = xmlDoc.getAttribute("/Out/Session/organizationName");
      brandingDirectory        = xmlDoc.getAttribute("../brandingDirectory");
      brandButtonInd           = xmlDoc.getAttribute("../brandButtonInd");
      clientBankOid            = xmlDoc.getAttribute("../clientBankOid");
      globalOrgOid             = xmlDoc.getAttribute("../globalOrgOid");
      clientBankCode           = xmlDoc.getAttribute("../clientBankCode");
      bankOrgGroupOid          = xmlDoc.getAttribute("../bankOrganizationGroupOid");
      baseCurrencyCode         = xmlDoc.getAttribute("../baseCurrencyCode");
      orgAutoLCCreateIndicator = xmlDoc.getAttribute("../orgAutoLCCreateIndicator");
      orgManualPOIndicator     = xmlDoc.getAttribute("../orgManualPOIndicator");
      custNotIntgTPS 		   = xmlDoc.getAttribute("../cust_is_not_intg_tps");
      enableAdminUpdateCentre  = xmlDoc.getAttribute("../enableAdminUpdateCentre");
      
      //TLE - 08/-0/07 - CR-375 - Begin
      orgAutoATPCreateIndicator = xmlDoc.getAttribute("../orgAutoATPCreateIndicator");
      orgATPManualPOIndicator   = xmlDoc.getAttribute("../orgATPManualPOIndicator");
      //TLE - 08/-0/07 - CR-375 - End
      
      timeoutAutoSaveInd       = xmlDoc.getAttribute("../timeoutAutoSaveInd");
      //IR 23468 - Move this after /out/session/ values are set
      lastLoginTimestamp	   = xmlDoc.getAttribute("/Out/lastLoginTimestamp");//CR 821

      if( (xmlDoc.getAttribute("/Out/passwordValidated") != null) &&
          (xmlDoc.getAttribute("/Out/passwordValidated").equals("true")))
       {
           userSession.setPasswordValidationFlag(true);
       }
      else
       {
           userSession.setPasswordValidationFlag(false);
       }

      if ((organizationName != null) && (brandingDirectory != null))
      {
         userSession.setOrganizationName(organizationName);
         userSession.setBrandingDirectory(brandingDirectory);
         userSession.setBrandButtonInd(TradePortalConstants.INDICATOR_YES.equals(brandButtonInd));      
      }
      if (globalOrgOid != null)
      {
         userSession.setGlobalOrgOid(globalOrgOid);
      }

      if (clientBankCode != null)
      {
         userSession.setClientBankCode(clientBankCode);
      }
     

      if (clientBankOid != null)
      {
         userSession.setClientBankOid(clientBankOid);
      }

      if (bankOrgGroupOid != null)
      {
         userSession.setBogOid(bankOrgGroupOid);
      }

      if (baseCurrencyCode != null)
      {
         userSession.setBaseCurrencyCode(baseCurrencyCode);
      }

      if (orgAutoLCCreateIndicator != null)
      {
         userSession.setOrgAutoLCCreateIndicator(orgAutoLCCreateIndicator);
      }

      if (orgManualPOIndicator != null)
      {
         userSession.setOrgManualPOIndicator(orgManualPOIndicator);
      }

      //TLE - 08/-0/07 - CR-375 - Begin
      if (orgAutoATPCreateIndicator != null)
      {
         userSession.setOrgAutoATPCreateIndicator(orgAutoATPCreateIndicator);
      }

      if (orgATPManualPOIndicator != null)
      {
         userSession.setOrgATPManualPOIndicator(orgATPManualPOIndicator);
      }
      //TLE - 08/-0/07 - CR-375 - End

      if (timeoutAutoSaveInd != null)
      {
         userSession.setTimeoutAutoSaveInd(timeoutAutoSaveInd);
      }
      //CR 821 Rel 8.3 START	
      if(lastLoginTimestamp != null){
    	  userSession.setLastLoginTimestamp(lastLoginTimestamp);
      }
      //CR 821 Rel 8.3 END
		userSession.setAuthMethod(authMethod);
      userSession.setIsSLAMonitoringOn(PerfStatConfigManager.getPerfStatConfigMgr(formMgr.getServerLocation()).getPerfStatConfig("SLAMonitoring", clientBankCode != null ? clientBankCode : "", "N"));
      userSession.setCorpCanAccessDocPrep(xmlDoc.getAttribute("/Out/Session/orgDocPrepInd"));

      userSession.setLoginOrganizationId(xmlDoc.getAttribute("/In/organizationId"));
      userSession.setOwnerOrgOid(xmlDoc.getAttribute("/Out/User/owner_org_oid"));
      userSession.setUserOid(xmlDoc.getAttribute("../user_oid"));
      userSession.setUserFullName(xmlDoc.getAttribute("../user_full_name"));
      userSession.setUserId(xmlDoc.getAttribute("../login_id"));//W Zhu 12/16/09 CR-482 setUserId for 2FA reauthentication.
      //AAlubala
      //IR#MNUM062844286 -SSO user will not have login_id, so setting user_identifier instead
      //start
      userSession.setUserId(xmlDoc.getAttribute("../user_identifier"));//
      //end
      userSession.setDefaultWipView(xmlDoc.getAttribute("../default_wip_view"));
      userSession.setTimeZone(xmlDoc.getAttribute("../timezone"));
      /* pgedupudi - Rel-9.3 CR-976A 03/26/2015 */
      userSession.setBankGrpRestrictRuleOid(xmlDoc.getAttribute("../bank_grp_restrict_rule_oid"));
      userSession.setDatePattern(xmlDoc.getAttribute("../datepattern"));
      userSession.setUserLocale(xmlDoc.getAttribute("../locale"));
   // jgadela Rel 8.4 CR-854 [START]- Adding reporting language option
      userSession.setReportingLanguage(xmlDoc.getAttribute("../reporting_language"));
      userSession.setOwnershipLevel(xmlDoc.getAttribute("../ownership_level"));
      userSession.setUserCanAccessDocPrep(xmlDoc.getAttribute("../doc_prep_ind"));
      userSession.setAuth2FASubTyp(xmlDoc.getAttribute("../auth2fa_subtype"));
      userSession.setShowToolTips(Boolean.valueOf(! "N".equals(xmlDoc.getAttribute("../showtips"))));
      //CR-433 Krishna Begin 05/22/2008
      userSession.setDocPrepURL(xmlDoc.getAttribute("../doc_prep_url"));
      //CR-433 Krishna End 05/22/2008
      userSession.setCanSeeOwnDataUsingSubAccess(xmlDoc.getAttribute("../use_data_while_in_sub_access"));
      
      userSession.setLiveMarketRateInd(xmlDoc.getAttribute("../live_market_rate_ind"));     //-- CR-640/581 Rel 7.1.0 09/22/11  -
      
      //MEerupula Rel 8.4 IR-23915
      userSession.setConfInd(xmlDoc.getAttribute("../confidential_indicator"));
      userSession.setSubsidConfInd(xmlDoc.getAttribute("../subsid_confidential_indicator"));

      String multiPartFlag = xmlDoc.getAttribute("../user_multi_part_trans_view");

      if( (multiPartFlag != null) && (multiPartFlag.equals(TradePortalConstants.INDICATOR_YES)) )
       {
         userSession.setMultiPartMode(true);
       }
      else
        {
         userSession.setMultiPartMode(false);
        }       

      String displayExtendedListview = xmlDoc.getAttribute("../display_extended_listviews");
      if( (displayExtendedListview != null) && (displayExtendedListview.equals(TradePortalConstants.INDICATOR_YES)) )
	  
       {
         userSession.setDisplayExtendedListview(true);
       }
      else
        {
         userSession.setDisplayExtendedListview(false);
        }       

      userSession.setSecurityRights(xmlDoc.getAttribute("/Out/SecurityProfile/security_rights"));

      userSecurityType = xmlDoc.getAttribute("../security_profile_type");
      userSession.setSecurityType(userSecurityType);

      userSession.setCustomerAccessIndicator(xmlDoc.getAttribute("../../User/customer_access_ind"));
      userSession.setCustomerAccessSecurityProfileOid(xmlDoc.getAttribute("../cust_access_sec_profile_oid"));

      //BSL CR 749 05/16/2012 Rel 8.0 BEGIN
      userSession.setUsingMaxSessionTimeout(false);
      userSession.setSessionStartDate(GMTUtility.getGMTDateTime(true, false));
      //BSL CR 749 05/16/2012 Rel 8.0 END
	
       //MEerupula Rel 9.3.5  CR-1029
       if (TradePortalConstants.INDICATOR_YES.equals(custNotIntgTPS)) {
        	 userSession.setCustNotIntgTPS(true);
       }
  

      
      if ( TradePortalConstants.INDICATOR_YES.equals(enableAdminUpdateCentre) ) {
    	  userSession.setEnableAdminUpdateInd(true);
      }
      
      // Tang Rel9.3.5 CR#1032 Session Synch -[Begin]
      String ownershipLevel  = userSession.getOwnershipLevel();
      
      if (ownershipLevel.equals(TradePortalConstants.OWNER_CORPORATE)){
               sessionSynchImg = ConfigSettingManager.getConfigSetting("PORTAL_APP", "PORTAL", "CORPORATE_SESSION_SYNC_IMG", ""); 
      }
      else if (ownershipLevel.equals(TradePortalConstants.OWNER_BOG)){
      		  sessionSynchImg = ConfigSettingManager.getConfigSetting("PORTAL_APP", "PORTAL", "BANK_SESSION_SYNC_IMG", "");
      }
      else
      {
     	 Debug.debug("CheckLogon.jsp : Session synch is not enabled. Session Synch Image is not setup in config table - [" +
      		ownershipLevel + "] - [sessionSynchImg - [" + sessionSynchImg + "]");
      }
      
      if (StringFunction.isNotBlank(sessionSynchImg)) {
      		 
     		Debug.debug("CheckLogon.jsp : Ownership Level - [" + ownershipLevel + "] - sessionSynchImg - [" + 
     		sessionSynchImg + "]");

     		String organizationOid = userSession.getOwnerOrgOid();

 		 	ClientBankWebBean clientBank  = beanMgr.createBean(ClientBankWebBean.class, "ClientBank");
 		 	clientBank.getById(userSession.getClientBankOid());
 	
 		 	String enableAdminUpdate = clientBank.getAttribute("enable_admin_update_centre");
 		 	
 		 	Debug.debug("CheckLogon.jsp : [" + organizationOid + "] Client Bank update center enabled? [" + 
 		 			enableAdminUpdate + "]");
		    
		    if (enableAdminUpdate.equals(TradePortalConstants.INDICATOR_YES)){
		 		if (ownershipLevel.equals(TradePortalConstants.OWNER_CORPORATE)){
 		        	CorporateOrganizationWebBean corporateOrg = beanMgr.createBean(CorporateOrganizationWebBean.class, "CorporateOrganization");
 			       	corporateOrg.getById(organizationOid);
			        organizationOid = corporateOrg.getAttribute("bank_org_group_oid");
		        } 
		 		
		 		BankOrganizationGroupWebBean bankOrgGroup = beanMgr.createBean(BankOrganizationGroupWebBean.class, "BankOrganizationGroup");
		 		bankOrgGroup.getById(organizationOid);
		    
		 		String enableSessionSynchHtml = bankOrgGroup.getAttribute("enable_session_sync_html");
		 		
		 		Debug.debug("CheckLogon.jsp : [" + organizationOid + "] BankOrgGroup session synch enabled? [" + 
		 		enableSessionSynchHtml + "]");
		 	
				if (enableSessionSynchHtml.equals(TradePortalConstants.INDICATOR_YES)){
				 userSession.setSessionSynchImg(sessionSynchImg);
		 		}
		 	} 
      }
      // Tang Rel9.3.5 CR#1032 Session Synch -[End]    		  
    		  
      
      //cquinton 12/13/2012 restore user preferences

      //recent instruments. do it for all, but for admin it will fall out becuase data is not present
      int recentIdx=0;
      String recentTransactionOid = xmlDoc.getAttribute("/Out/UserPrefs/RecentInstrument("+recentIdx+")/transactionOid");
      while ( recentTransactionOid!=null && recentIdx<10 ) { //provide a failsafe 
         String recentInstrumentId = xmlDoc.getAttribute("/Out/UserPrefs/RecentInstrument("+recentIdx+")/instrumentId"); 
         String recentTransactionType = xmlDoc.getAttribute("/Out/UserPrefs/RecentInstrument("+recentIdx+")/transactionType"); 
         String recentTransactionStatus = xmlDoc.getAttribute("/Out/UserPrefs/RecentInstrument("+recentIdx+")/tranactionStatus");

         userSession.addRecentInstrument(
            recentTransactionOid, recentInstrumentId, 
            recentTransactionType, recentTransactionStatus);

         recentIdx++;
         recentTransactionOid = xmlDoc.getAttribute("/Out/UserPrefs/RecentInstrument("+recentIdx+")/transactionOid");
      }
      //set the recent instruments in the session listener so it is persisted at logout
      if ( listener!= null ) {
        listener.setRecentInstrumentsQueue(userSession.getRecentInstrumentsQueue());
      }

      
      // W Zhu 8/17/2012 Rel 8.1 T36000004579 setSecreKey
      try
         {
    	   // Generate a DES3 key for this user session.  This is used to encrypt sensitive information (such as oid) on the web page.
            javax.crypto.KeyGenerator keygen = javax.crypto.KeyGenerator.getInstance("DESede", "BC");
            SecretKey secretKey = keygen.generateKey();
            userSession.setSecreKey(secretKey);
            // Put it on CSDB so that EJBs can use it too.
            String secretKeyString = EncryptDecrypt.bytesToBase64String(secretKey.getEncoded());
            resMgr.getCSDB().setCSDBValue("SecretKey", secretKeyString);
            Debug.debug("CheckLogon: SecretKey=" + secretKeyString);
         }
      catch (Exception e) {
      }

      //cquinton 10/4/2012 Rel portal refresh ir#6032 moved session reset to first thing on success -- see above
      
      //cquinton 2/1/2012 Rel 8.0 ppx255 start
      //check to see if the user is already logged on - if so forward to relogon page
      //in addition, move all forwarding behavior on successful logon
      // to common fragment so it will occur after logon (without already loggedIn forward) or relogon
      if ( TradePortalConstants.INDICATOR_YES.equals(xmlDoc.getAttribute("/Out/forwardToReLogon")) ) {
         physicalPage = NavigationManager.getNavMan().getPhysicalPage("ReLogon", request);
          authMethod = xmlDoc.getAttribute("/In/auth");
%>
         <jsp:forward page='<%= physicalPage %>' >
            <jsp:param name="auth" value="<%= authMethod %>" />
         </jsp:forward>
<%
      } else {
%>
         <%-- refactor forward behavior to fragment --%>
         <%-- refactored - BSL 08/22/11 CR663 Rel 7.1 Begin --%>
         <%-- refactored - BSL 08/22/11 CR663 Rel 7.1 End --%>
         <%@ include file="fragments/forwardOnLogonSuccess.frag" %>


<%
      }
      //cquinton 2/1/2012 Rel 8.0 ppx255 end

   }
   else
   {
      // Logon failed.
      formMgr.setCurrPage("Logon");

      // Forward to the page to display errors.
      // If the user entered a password, send them back to the Enter Password page
      // If the user did not enter a password, send them to the Authentication Errors page
      if(xmlDoc.getAttribute("/In/auth").equals(TradePortalConstants.AUTH_PASSWORD))
         physicalPage = NavigationManager.getNavMan().getPhysicalPage("EnterPassword", request);
      else if (xmlDoc.getAttribute("/In/auth").equals(TradePortalConstants.AUTH_2FA))         
          physicalPage = NavigationManager.getNavMan().getPhysicalPage("EnterPassword2FA", request);
      //BSL 08/22/11 CR663 Rel 7.1 Begin
      else if (TradePortalConstants.AUTH_REGISTERED_SSO.equals(xmlDoc.getAttribute("/In/auth"))) {
          physicalPage = NavigationManager.getNavMan().getPhysicalPage("SSORegistrationPasswordLogon", request);
      }
      else if (TradePortalConstants.AUTH_SSO_REGISTRATION.equals(xmlDoc.getAttribute("/In/auth"))) {
          physicalPage = NavigationManager.getNavMan().getPhysicalPage("EnterSSORegistrationPassword", request);
      }
      //BSL 08/22/11 CR663 Rel 7.1 End
      else {
         physicalPage = NavigationManager.getNavMan().getPhysicalPage("AuthenticationErrors", request);
         
         //Ravindra - 21/01/2011 - CR-541 - Start
         //Ravindra - 03/31/2011 - CUUL032965287 - Start
         String authMethod = xmlDoc.getAttribute("/In/auth");
         if (TradePortalConstants.AUTH_AES256.equals(authMethod)){
            String user=xmlDoc.getAttribute("/In/User/ssoId");
            //Ravindra - 03/31/2011 - CDUL031849759 - Start
            String maxError = xmlDoc.getAttribute("/Error/maxerrorseverity");
            String failureType = HttpResponseUtility.ACCESS_FAILURE; //default
            if (maxError != null && maxError.compareTo(
                String.valueOf(ErrorManager.WARNING_ERROR_SEVERITY)) > 0) {
               //System.out.println("CheckLogon: errors found");
                   
               DocumentHandler errorDoc = xmlDoc.getFragment("/Error");
               String errors = errorDoc.toString();
               if ( errors.indexOf(TradePortalConstants.INCOMPLETE_USER_PROVISIONING) >= 0 ) {
                  failureType = HttpResponseUtility.INCOMPLETE_USER;
                  //System.out.println("CheckLogon: incomplete provisioning found");
               }
            }
            // response.setStatus(HttpResponseUtility.getResponseCode( orgId, user, failureType ));
                
			// T36000013446- use custom http header to communicate CMA specific status codes - start

	  		   String customStatus = Integer.toString(HttpResponseUtility.getResponseCode( orgId, user, failureType ));
	  		   response.addHeader(TradePortalConstants.CUSTOM_STATUS_HEADER,customStatus);
	  		   
  			// T36000013446- use custom http header to communicate CMA specific status codes - end
  		
         }
         //Ravindra - 03/31/2011 - CUUL032965287 - End
         //Ravindra - 21/01/2011 - CR-541 - End
      }
%>
      <jsp:forward page='<%= physicalPage %>' />    
<%
   }
%>
