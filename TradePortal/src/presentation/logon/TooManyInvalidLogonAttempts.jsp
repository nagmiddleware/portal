<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.web.*, com.amsinc.ecsg.frame.*, com.ams.tradeportal.common.*, com.ams.tradeportal.html.*,com.amsinc.ecsg.util.*" %>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager"      scope="session"></jsp:useBean>
<jsp:useBean id="resMgr"  class="com.amsinc.ecsg.util.ResourceManager" scope="session"></jsp:useBean>
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>


<%

	
   String brandingDirectory = StringFunction.xssCharsToHtml(request.getParameter("branding")); //T36000004579 #3;
%>

<jsp:include page="/common/Header.jsp">
   <jsp:param name="minimalHeaderFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="brandingDirectory" value="<%=brandingDirectory%>" />
</jsp:include>

<div class="pageMainNoSidebar">
  <div class="pageContent">

  <%-- include a blank page header just for spacing --%>
  <jsp:include page="/common/PageHeader.jsp">
    <jsp:param name="titleKey" value="LoginFailed.TooManyLoginAttemptsTitle" />
    <jsp:param name="helpUrl" value="customer/login_failed.htm" />
  </jsp:include>

<form>
      
  <div class="formContentNoSidebar padded">

    <div class="instruction">
      <%= resMgr.getText("LoginFailed.TooManyLoginAttemptsTextPart1", TradePortalConstants.TEXT_BUNDLE) %>
      <%= SecurityRules.getLoginAttemptsBeforeBeingRedirected() %>
      <%= resMgr.getText("LoginFailed.TooManyLoginAttemptsTextPart2", TradePortalConstants.TEXT_BUNDLE) %>
    </div>

  </div>

</form>

  </div>
</div>

<jsp:include page="/common/Footer.jsp">
  <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_NO%>" />
  <jsp:param name="minimalHeaderFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
  <jsp:param name="includeTimeoutForward" value="false" />
</jsp:include>

<%
  //Ravindra - 21/01/2011 - CR-541 - Start
  //Ravindra - 03/31/2011 - CUUL032965287 - Start
  DocumentHandler              logoutDoc                = new DocumentHandler();
  DocumentHandler              xmlDoc                   = formMgr.getFromDocCache();
  if (xmlDoc!=null) {
    String authMethod = xmlDoc.getAttribute("/In/auth");
    if (TradePortalConstants.AUTH_AES256.equals(authMethod)){
      String orgId               = request.getParameter("organization");
      String user=xmlDoc.getAttribute("/In/User/ssoId");
      // 604 is CMA specific status code to notate "User is locked out"
      //response.setStatus(HttpResponseUtility.getResponseCode( orgId, user, HttpResponseUtility.LOCKED_USER ));
	  // T36000013446- use custom http header to communicate CMA specific status codes - start
      String customStatus = Integer.toString(HttpResponseUtility.getResponseCode( orgId, user, HttpResponseUtility.LOCKED_USER ));
	  response.addHeader(TradePortalConstants.CUSTOM_STATUS_HEADER,customStatus);
	  // T36000013446- use custom http header to communicate CMA specific status codes - end
 
    }
  }
  //Ravindra - 03/31/2011 - CUUL032965287 - End
  //Ravindra - 21/01/2011 - CR-541 - End
  // Invalidate the user's session to prevent further logon attempts 
  // without creating a new session
  HttpSession userHttpSession = request.getSession(false);

  userHttpSession.invalidate();
%>

</body>
</html>
