<%--
 *   This JSP is used to complete the recertification process.
 
 *   It needs to reload the original window to resume the authorization process that was waiting for 
 *   recertification to complete, and finally, will close the popup window used for the recertification 
 *   process.   
 *
 *     Copyright   2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.util.*,
	  com.ams.tradeportal.common.*" %>
<html>
 <head>
 </head>
           
 <body  onload="completeRecertification();" >

<%

   //cr498 the action to submit on success is gotten from session
   String submitButtonName = (String)session.getAttribute("submitButtonName");
   //Rel 9.3 XSS CID-11407

   String reCertOK = (String)session.getAttribute("recertificationSuccessful");
   session.setAttribute("recertificationSuccessful", "N");
   //CR-711, Rel8.0, 01/20/2012
   String proxyUserOid = "";
   proxyUserOid = (String)session.getAttribute("proxyUserOid");

%>
  
<script language="JavaScript"> 
  function completeRecertification () 
  {    
    <%-- 
    CR-711
    Rel8.0 - 01/20/2012
    Obtain the proxy UserOid and add it on the form
    before submitting it
    --%>

    var proxyOid = "<%=StringFunction.escapeQuotesforJS(proxyUserOid)%>";
    <%--cr498 set button name dynamically. this is now done for more than just authorizations--%>
    var submitButtonName = "<%=StringFunction.escapeQuotesforJS(submitButtonName)%>";
    try{
		window.opener.document.forms[0].buttonName.value=submitButtonName;
		window.opener.document.forms[0].reCertOK.value="<%=reCertOK%>";          
		<%--//set proxy user if we have the value --%>
		window.opener.document.forms[0].proxy.value=proxyOid;
    }catch(e){
    }
    <%--
    CR-711
    Rel8.0
    AAlubala
    01/27/2011
    --%>    
    window.opener.document.forms[0].submit();

    window.close();
  }
</script>
<%
//clean the session
session.removeAttribute("proxyUserOid");
%>
 </body>
</html>