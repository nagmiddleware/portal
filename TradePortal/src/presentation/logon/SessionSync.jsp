<%--
 *  The assumption is CMA portal ensure to call this page in regular intervals to avoid unwanted timeout occurrence from trade portal end.
 *  Call from CMA ensures to extend associated session, here the assumption is pull mechanism  from CMA application to call this jsp page.
 *     
--%>
<%@ page import="com.ams.tradeportal.common.SecurityRules,com.amsinc.ecsg.util.*" %>
<%


String TypeOfOperation= (String) request.getParameter("TypeOfOperation");

if("LOGOUT".equals(TypeOfOperation) || "TIMEOUT".equals(TypeOfOperation)){
	   HttpSession theSession = request.getSession(false);
	   theSession.removeAttribute("TimeoutListener");
	   theSession.invalidate();
 }
//RKAZI CMA SESSION SYNC REL 9.1 10/23/2014 - Start 
else{
	 // Need this code to reset the Session Max Inactive Interval back to its default value as specified in
	 //config xml, especially if the interval was set to a different value when Portal UI check to see if session should be 
	 //timedout or not. As long as CMA portal keeps pinging TP server we keep resetting the Max Inactive Interval.
	 HttpSession theSession = request.getSession(false);
	 System.out.println("Session Sync.jsp called at : ====> " + (new java.util.Date()).toGMTString());
	 System.out.println("Session ID in Session Sync.jsp: ====> " + theSession.getId());
	 System.out.println("Last Accessed Time  in Session Sync.jsp: ====> " + theSession.getLastAccessedTime());
	 System.out.println("Current System Time in Milli seconds  in Session Sync.jsp: ====> " + System.currentTimeMillis());
	 System.out.println("Session Max In Active Interval seconds  in Session Sync.jsp: ====> " + theSession.getMaxInactiveInterval());
	 request.getSession(false).setMaxInactiveInterval(SecurityRules.getTimeOutPeriodInSeconds());
}
//RKAZI CMA SESSION SYNC REL 9.1 10/23/2014 - End
out.print(StringFunction.xssCharsToHtml(TypeOfOperation));
%>
