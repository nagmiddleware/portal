<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
 * This page is a pass-through used to complete the SSO Registration process.
 * It should be configured for the WebLogic Server SAML 2.0 Service Provider
 * as a Redirect URI for the appropriate Identity Provider partner.
 * It should also be a protected page in the web-app deployment descriptor.
 *
 * As a result, WLS will require that the user be authenticated via SAML
 * prior to accessing the page.  The IdP-provided unique identifier for 
 * the authenticated user is retrieved using request.getRemoteUser() so
 * that it can be passed to UserDataMediator to link to the Portal
 * user account.
 *
 * Based on a customization to WLS, passing the parameter AllowCreate=true
 * causes the NameIDPolicy element with attribute AllowCreate="true" to be
 * added to the AuthnRequest.  This page is only intended to be used after
 * successfully authenticating on the EnterSSORegistrationPassword page.
 * This page refreshes itself to cause WLS to initiate SAML authentication. 
 *
 * Note that secureRoot must be specified in TradePortal.properties if the 
 * request is going through an HTTP server because WebLogic will change
 * HTTPS to HTTP when refreshing with a relative URL.
--%>
<%@ page import="com.amsinc.ecsg.web.*, com.amsinc.ecsg.util.*,
                 com.ams.tradeportal.busobj.util.*,
                 com.ams.tradeportal.common.*, java.util.*" %>

<jsp:useBean id="formMgr"     class="com.amsinc.ecsg.web.FormManager"                   scope="session" />
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"/>

<html>
<%

	
	String remoteUser = request.getRemoteUser();
	if (InstrumentServices.isBlank(remoteUser)) {
		// Need to refresh the page to initiate SAML authentication
%>
	<head>
		<%-- BSL CR663 11/11/11 BEGIN - Specify fully qualified URL to resolve issue where https is changed to http on refresh in Apache server. Change number of seconds before page reload from 1 to 0. --%>
		<%-- <meta HTTP-EQUIV="Refresh" content="1;URL=/portal/logon/CompleteSSORegistrationDR.jsp?AllowCreate=true"> --%>
		<%
		String refreshURL = "/portal/logon/CompleteSSORegistrationDR.jsp?AllowCreate=true";
		String secureRoot = null;

		// If secureRoot is specified in TradePortal.properties, prepend it to
		// the refresh URL; otherwise, the refresh URL will be a relative URL.
		// Note that secureRoot must be specified if the request is going 
		// through an HTTP server because WebLogic will change HTTPS to HTTP
		// when refreshing with a relative URL.
		/*
		try {
			ResourceBundle portalProperties = PropertyResourceBundle.getBundle("TradePortal");
			secureRoot = portalProperties.getString("secureRoot");
		}
		catch (Exception e) {
			Debug.debug("Could not retrieve property secureRoot from TradePortal.properties: " + e);
		}
		**/
		
		//Rel9.0 IR#T36000030384 - Start
		//There is more than one possible host where the user is originating from
		//so, the correct host will be determined from the request object
		//and the secureRoot will be derived out of this
		try{
			String url = request.getRequestURL().toString();
			String uri = request.getRequestURI().toString();
			secureRoot = url.substring(0, url.length() - uri.length());
		}catch(Exception e){
			Debug.debug("Exception during the determination of SecureRoot: " + e);
		}
		//End
		
		if (secureRoot != null) {
			refreshURL = secureRoot + refreshURL;
		}

		String refreshContent = "0; url=" + refreshURL;
		Debug.debug("Refreshing with content: " + refreshContent);
		%>
		<meta http-equiv="refresh" content="<%= refreshContent %>">
		<%-- BSL CR663 11/11/11 END --%>
	</head>
	<body></body>
<%
	} else {
		Debug.debug("BSL Completing SSO registration");
%>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"></meta>
		<script language="JavaScript">
		function submitJSP() {
			document.CompleteSSORegistrationDR.submit();
		}
		</script>
	</head>
	<body onLoad="submitJSP()" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" bgcolor="#FFFFFF" link="#0033CC" vlink="#0033CC" alink="#0033CC">
		<form name="CompleteSSORegistrationDR" method="POST" action="<%= formMgr.getSubmitAction(response) %>">
	<%
		String formName = "CompleteSSORegistrationDR";
		Hashtable addlParms = new Hashtable();

		// The remote user will be registered to the userOID
		addlParms.put("newRegisteredSSOId", remoteUser);
		addlParms.put("userOID", userSession.getUserOid());
		addlParms.put("changingRegisteredSSOIdFlag", "true");
	%>
	<%= formMgr.getFormInstanceAsInputField(formName, addlParms) %>
		</form>
	</body>
<%
	}
%>
</html>
