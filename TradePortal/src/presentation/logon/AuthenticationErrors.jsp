<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>


<%@ page import="com.amsinc.ecsg.web.*,com.amsinc.ecsg.util.*,
        com.amsinc.ecsg.frame.*, com.ams.tradeportal.mediator.*,
	  com.ams.tradeportal.common.*, com.ams.tradeportal.html.*" %>
<%--
 // The user is forwarded to this page when the system could not authenticate them
 // based on the certificate they provided.  This page will look for errors in 
 //the default.doc XML document and then display them.   A link is created so that 
 //they can attempt to log on again.
 
--%>

<jsp:useBean id="formMgr"     class="com.amsinc.ecsg.web.FormManager"            scope="session" />
<jsp:useBean id="resMgr"      class="com.amsinc.ecsg.util.ResourceManager"       scope="session" />

<%



   String brandingDirectory = StringFunction.xssCharsToHtml(request.getParameter("branding")); //T36000004579 #3;
%>
<META HTTP-EQUIV= "expires" content = "Saturday, 07-Oct-00 16:00:00 GMT"></meta>

<jsp:include page="/common/Header.jsp">
  <jsp:param name="minimalHeaderFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
  <jsp:param name="brandingDirectory" value="<%=brandingDirectory%>" />
  <jsp:param name="checkChangePassword" value="<%= TradePortalConstants.INDICATOR_NO %>" />
</jsp:include>

<div class="pageMainNoSidebar">
  <div class="pageContent">

  <%-- include a blank page header just for spacing --%>
  <jsp:include page="/common/PageHeader.jsp">
    <jsp:param name="titleKey" value="AuthenticationErrors.LoginFailed" />
  </jsp:include>

  <div class="formContentNoSidebar padded">

    <div class="instruction">
      <%=resMgr.getText("AuthenticationErrors.Message1",TradePortalConstants.TEXT_BUNDLE)%><br>
    </div>
    <div class="instruction">
      <%=resMgr.getText("AuthenticationErrors.Message2",TradePortalConstants.TEXT_BUNDLE)%>
    </div>
 
    <jsp:include page="/common/ErrorSection.jsp" />	

  </div>

  </div>
</div>

<jsp:include page="/common/Footer.jsp">
  <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
  <jsp:param name="minimalHeaderFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
  <jsp:param name="includeTimeoutForward" value="false" />
</jsp:include>

<%
  formMgr.storeInDocCache("default.doc",new DocumentHandler());
  request.getSession().invalidate();
%>

</body>
</html>
