<%--
 *   This fragment determines where to forward the user after after successful
 *   logon.
 *
 *   cquinton 2/16/2012
 *   This was refactored from CheckLogon.jsp to allow for reuse in CheckReLogon.jsp
 *
 *   Inputs:
 *      xmlDoc - the mediator output document
 *         should include /In/auth, /Out/newUser, /Out/passwordExpire

 *   Copyright  � 2002                        
 *   American Management Systems, Incorporated 
 *   All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*, com.ams.tradeportal.common.*" %>

<%
   String forwardPage = "";

   // If this user entered a password (or 2FA), there are certain checks that must happen
   if ( xmlDoc.getAttribute("/In/auth").equals( TradePortalConstants.AUTH_PASSWORD ) ||
        xmlDoc.getAttribute("/In/auth").equals(TradePortalConstants.AUTH_2FA)) {
      // User used a password to authenticate themself
  
      // Check to see if the user is a new user
      // If so, forward them to the change password page
      if( (xmlDoc.getAttribute("/Out/newUser") != null) && 
         (xmlDoc.getAttribute("/Out/newUser").equals("true")) )
      {
         //cquinton 12/20/2012 set forceChangePassword flag in userSession, Header.jsp consults
         userSession.setForceChangePassword(true);
         userSession.setChangePasswordReason("new");

         forwardPage = NavigationManager.getNavMan().getPhysicalPage("ChangePassword", request);
   
         // Add URL parameters to force the user to change their password
         forwardPage = forwardPage + "?force=true&reason=new";
%>
         <jsp:forward page='<%= forwardPage %>' />
<%
      }
      else
      {
         // Check to see if the user's password has expired.
         // If so, forward them to the change password page
         if( (xmlDoc.getAttribute("/Out/passwordExpire") != null) && 
             (xmlDoc.getAttribute("/Out/passwordExpire").equals("true")) )
         {
            //cquinton 12/20/2012 set forceChangePassword flag in userSession, Header.jsp consults
            userSession.setForceChangePassword(true);
            userSession.setChangePasswordReason("expired");

            forwardPage = NavigationManager.getNavMan().getPhysicalPage("ChangePassword", request);

            // Add URL parameters to force the user to change their password
            forwardPage = forwardPage + "?force=true&reason=expired";
   
%>
            <jsp:forward page='<%= forwardPage %>' />
<%
         }
         else
         {
            forwardPage = NavigationManager.getNavMan().getPhysicalPage("TradePortalHome", request);
%>
            <jsp:forward page='<%= forwardPage %>' />
<%
         }
      }
   }
   //BSL 08/22/11 CR663 Rel 7.1 Begin
   else if (TradePortalConstants.AUTH_SSO_REGISTRATION.equals(xmlDoc.getAttribute("/In/auth"))) {
      userSession.setPasswordValidationFlag(false);
      //RKAZI REL 9.1  T36000031975 10/31/2014 - START
      String url = null;
      String ssoRegistrationPageName = "CompleteSSORegistration";
      String secondarySAMLIDP = ConfigSettingManager.getConfigSetting("PORTAL_APP", "PORTAL", "SECONDARY_SAML_IDP", ""); 
      if (secondarySAMLIDP == null){
      	secondarySAMLIDP = "";
      }
      try{
			url = request.getRequestURL().toString().toUpperCase();
			if (url != null && StringFunction.isNotBlank(secondarySAMLIDP)){
				if (url.contains(secondarySAMLIDP.toUpperCase())) {
					ssoRegistrationPageName = "CompleteSSORegistrationDR";
				}
			}
		}catch(Exception e){
			Debug.debug("Exception during the determination of SecureRoot: " + e);
			
		}
		//RKAZI REL 9.1  T36000031975 10/31/2014 - END
      forwardPage = NavigationManager.getNavMan().getPhysicalPage(ssoRegistrationPageName, request);
     
      Debug.debug("forwardOnLogonSuccess -- Forwarding to page  " + forwardPage);
%>
      <jsp:forward page='<%= forwardPage %>' />
<%
   }
   //BSL 08/22/11 CR663 Rel 7.1 End
   else
   {
      // User did not use a password to authenticate themself
      forwardPage = NavigationManager.getNavMan().getPhysicalPage("TradePortalHome", request);

      // Check to see if the user is a new user
      // If so, forward them to the change password page
      if( (xmlDoc.getAttribute("/Out/newUser") != null) && 
          (xmlDoc.getAttribute("/Out/newUser").equals("true")) )
      {
         // Add URL parameters to force the user to change their password
         forwardPage = forwardPage + "?newUser=true";
%>
         <jsp:forward page='<%= forwardPage + "?newUser=true" %>' />
<%
      }
      else
      {
%>
         <jsp:forward page='<%= forwardPage %>' />
<%
      }
   }
%>
