<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
 * This fragment contains the logic to read the ClientBank table to determine the 
 * URL to use in opening a Recertification window.  
 * The certAuthURL java variable (which must be declared externally to allow for
 * conditional inclusion of this fragment) is populated by this fragment with
 * the authentication url. The including jsp should then use
 * the openReauthenticationWindow() function (or other javascript function below)
 * directly.
 *
 * This process is expecting the parameter auth=CERTIFICATE on the URL.
--%>

<%@ page import="com.amsinc.ecsg.web.*,com.amsinc.ecsg.util.*,
        com.amsinc.ecsg.frame.*, com.ams.tradeportal.mediator.*, com.ams.tradeportal.busobj.util.*,
	  com.ams.tradeportal.common.*, com.ams.tradeportal.common.cache.*, com.ams.tradeportal.html.*, java.text.*, java.security.*" %>


<%
     
     QueryListView                  queryListViewCC                   = null;
     StringBuffer                   sqlQueryCC                        = null;
  //////////////////////////////////////////////////////////
  // GET AND CHECK FOR THE PRESENCE OF PAGE PARAMETERS    //
  //////////////////////////////////////////////////////////     

  // Get page parameters 
  String orgId               = userSession.getLoginOrganizationId();
  String brandingDirectory   = userSession.getBrandingDirectory();
  String locale              = userSession.getUserLocale();
  String clientBankOid       = userSession.getClientBankOid();
  String certData            = request.getParameter("info");
  //String certNewLink             = "";
  String EMPTY_URL = "EMPTY_URL";
  //cr498 - this fragment builds certAuthURL
  // certNewLink is no longer used as it does not support multiple buttons
  // each button should explicitly specify the javascript function to call

  // Check for an organization ID and branding directory - these are mandatory
  if(InstrumentServices.isBlank(orgId) || InstrumentServices.isBlank(brandingDirectory))
   {
     Debug.debug("A branding directory and organiztion ID must be specified");
     return;
   }
  else
   {
     // Make it so that the organization ID is not case sensitive
     orgId = orgId.toUpperCase();
   }

  // Check for optional locale
  if(InstrumentServices.isBlank(locale))
   {
     // Default to en_US if no locale is received
     locale = "en_US";
     Debug.debug("No locale specified in TradePortalLogon... defaulting to en_US");
   }
      
   //retrieve ClientBank from cache
   Cache cache = (Cache)TPCacheManager.getInstance().getCache(TradePortalConstants.CLIENT_BANK_CACHE);
   DocumentHandler result = (DocumentHandler)cache.get(clientBankOid);

   //cr498 - remove require_certs_fund_final_auth
   //determination of whether reauthentication is required is done
   // external to this frag if single or via javascript
   // if a listview
   //String requireCert = result.getAttribute("/ResultSetRecord(0)/REQUIRE_CERTS_FUND_FINAL_AUTH");
   //
   //if (InstrumentServices.isBlank(requireCert) || requireCert.equals(TradePortalConstants.INDICATOR_NO)) {
   //    Debug.debug("recertification not required");
   //    return;   
   //}
   EMPTY_URL = "EMPTY_URL";
   // CR-482 Check user's setting how we do recertification
   UserWebBean user = beanMgr.createBean(UserWebBean.class, "User");
   user.getById(userSession.getUserOid());
   String additionalAuthType = user.getAttribute("additional_auth_type");
  
   //cr498 - certAuthURL must be declared external
   //String certAuthURL = null; 
   if (TradePortalConstants.AUTH_2FA.equals(additionalAuthType)) {
	   //AAlubala - CR-711 Rel 8.0 - 11/01/2011
	   //If using VASCO, call the VASCO page, otherwise, use the current page as is
	   if(TradePortalConstants.VASCO_TOKEN.equals(user.getAttribute("token_type"))){
		   certAuthURL = "/portal/logon/EnterRecertificationPasswordVasco.jsp?auth=2FA";
	   }else
       certAuthURL = "/portal/logon/EnterRecertificationPassword2FA.jsp?auth=2FA";
   }
  else if (TradePortalConstants.AUTH_CERTIFICATE.equals(additionalAuthType)){

       if ("G".equals(user.getAttribute("certificate_type"))) { // Gemalto
          certAuthURL = "/portal/logon/EnterRecertificationGemalto.jsp?auth=2FA";
       }
       else {
          certAuthURL = result.getAttribute("/ResultSetRecord(0)/AUTHORIZATION_CERT_AUTH_URL");
       }
   }
    else {
       certAuthURL = EMPTY_URL;
   }

   if (InstrumentServices.isBlank(certAuthURL)) {
      Debug.debug("unable to display reauthentication page due to URL problem");
      //cquinton 1/18/2011 Rel 6.1.0 ir#stul011761874
      //comment out return which stops rendering - handle blank in javascript function below
      //return;
      // W Zhu 11/13/2015 T36000045444 Use EMPTY_URL marker.  Empty certAuthURL allows the user to by-pass user authentication (see sideBar.jsp)
      certAuthURL = EMPTY_URL;
    } else if (!EMPTY_URL.equals(certAuthURL)) {
        // This process is expecting the parameter auth=CERTIFICATE on the URL, and will append the other parameters.
        certAuthURL = certAuthURL + "&organization=" + orgId + "&branding=" + brandingDirectory + "&locale=" + locale;
        if (!(InstrumentServices.isBlank(certData))) {
            System.out.println("Certificate data provided, cert = " + certData);
            certAuthURL = certAuthURL + "&info=" + certData;
        }
        Debug.debug("certAuthURL="+certAuthURL);
        // set the certNewLink java variable - this can be used on
        // any detail transaction pages - it defaults to the Authorize button
        //certNewLink = "javascript:openReauthenticationWindow('"+certAuthURL+"')";
        //Debug.debug("open window " + certNewLink);
   }

   //onLoad+="openReauthenticationWindow('" + certAuthURL + "')";
   
%>
 
<script language="JavaScript">
    function unBusySubmitButton() {
       var submitButton;
       submitButton = dijit.byId("AuthorizeButton");
       if (submitButton) submitButton.cancel();
       submitButton = dijit.byId("PendingTransactions_Authorize");
       if (submitButton) submitButton.cancel();
       submitButton = dijit.byId("PaymentPendingTransactions_Authorize");
       if (submitButton) submitButton.cancel();

    }


    <%--
      // openReauthenticationWindow - open the appropriate reauthentication
      // window.  this function is transaction based and should only be
      // used for detail pages (i.e. non-list) where it is known
      // reauthentication is necessary. Default to Authorize.
      // See complete version of openReauthenticationWindow below for
      // other uses.
      // Arguments:
      //   url - typically use the certAuthURL java variable calculated above
    --%>
    function openReauthenticationWindow (url,buttonName) {
        <%-- //cquinton 1/18/2011 Rel 6.1.0 ir#stul011761874 handle blank url--%>
        <%-- W Zhu 11/13/2015 T36000045444 Use EMPTY_URL marker.  Empty certAuthURL allows the user to by-pass user authentication (see sideBar.jsp) --%>
        if ( url == null || url == '' || url == '<%=EMPTY_URL%>') {
            <%-- BSL IR NRUL080338698 03/28/2012 - Convert numeric character references to Unicode --%>
            alert('<%=StringFunction.asciiToUnicode(resMgr.getTextEscapedJS("E618", "ErrorMessages"))%>');
            return;
        }
        <%--cr498 - rewrite to set parameter defaults for use after authentication is done --%>
        <%-- get the formName from the 1st form --%>
        var formName = document.forms[0].name;

        <%-- default buttonName to Authorize --%>
        if ( buttonName == null ) {
            buttonName = "Authorize";
        }
        url += "&submitButtonName=" + buttonName;

        var reAuthObjectType = "Transaction";
        url += "&reAuthObjectType=" + reAuthObjectType;

        <%-- reAuthObjectOid passed directly to recert pages through secureParms!!! --%>

        if ( document.getElementsByName(formName).length > 0 ) {
            transactionForm = document.getElementsByName(formName)[0];

            <%--ir CAUK113043267 start--%>
            <%-- pass the navigation id reference to the reauth window.
                 This allows the child to get secure parameters --%>
            var parentNi = transactionForm.ni.value;
            url += "&parentNi=" + parentNi;
            <%--ir CAUK113043267 end--%>

            <%-- set the reCertification flag dynamically --%>
            transactionForm.reCertification.value = "<%=TradePortalConstants.INDICATOR_YES%>";
        }

        <%-- CR-473 get transaction OID, which is used to form challenge string for smart card --%>
        <%--cr498 - move all list logic to list version of this function below
        var selectedTransactionOid = "";
        if ( document.getElementsByName("CashMgmt-TransactionForm").length > 0 ) {
            transactionForm = document.getElementsByName("CashMgmt-TransactionForm")[0];
            allInputs = transactionForm.getElementsByTagName("input");
            for (i=0; i<allInputs.length; i++) {
                if (allInputs[i].type == "checkbox" && allInputs[i].checked) {
                    selectedTransactionOid = allInputs[i].value;
                    url += "&TransactionOid=" + selectedTransactionOid;
                    break;
                }
            }
        }
        --%>
        displayReauthenticationWindow(url);
    }

    <%-- cr498 begin add new javascript methods --%>

    <%--
      // openReauthenticationWindowForDetail - open the appropriate reauthentication window.
      // if recertification is successful, the formName/buttonName parameters
      // are used (by CompleteRecertification.jsp) to automatically resubmit 
      // the appropriate form/action.
      // This method should only be used if it is known reauthentication is 
      // necessary (which is determined on the specific page).
      // This should be used only by detail pages where the object oid
      // (transaction oid, pay remit oid, etc) is passed to the recertification
      // window by a secure parm.
      // See additional version below for use by list pages.
      //
      // Arguments:
      //   url - typically use the certAuthURL java variable calculated above
      //   formName - the form to submit if authentication is successful.
      //   buttonName - the action to perform if authentication is successful.
      //   reAuthObjectType - the type of reauthentication object.
      //              this is passed through and used for logging.
      //              Valid values: 'Transaction', 'PayRemit', 'Invoice'
    --%>
    function openReauthenticationWindowForDetail(url,formName,buttonName,reAuthObjectType) {
        <%-- //cquinton 1/18/2011 Rel 6.1.0 ir#stul011761874 handle blank url--%>
        <%-- W Zhu 11/13/2015 T36000045444 Use EMPTY_URL marker.  Empty certAuthURL allows the user to by-pass user authentication (see sideBar.jsp) --%>
        if ( url == null || url == '' || url == '<%=EMPTY_URL%>') {
            <%-- BSL IR NRUL080338698 03/28/2012 - Convert numeric character references to Unicode --%>
            alert('<%=StringFunction.asciiToUnicode(resMgr.getTextEscapedJS("E618", "ErrorMessages"))%>');
            return;
        }
        <%--following verifications ensure args are valid at development time--%>
        if ( formName == null ) {
            alert('Problem opening authentication window: formName required');
        }
        if ( buttonName == null ) {
            alert('Problem opening authentication window: buttonName required');
        }
        if ( reAuthObjectType == null ) {
            alert('Problem opening authentication window: reAuthObjectType required');
        }
        url += "&submitButtonName=" + buttonName;
        url += "&reAuthObjectType=" + reAuthObjectType;
        <%-- reAuthObjectOid passed directly to recert pages through secureParms!!! --%>

        if ( document.getElementsByName(formName).length > 0 ) {
            transactionForm = document.getElementsByName(formName)[0];

            <%--ir CAUK113043267 start--%>
            <%-- pass the navigation id reference to the reauth window.
                 This allows the child to get secure parameters --%>
            var parentNi = transactionForm.ni.value;
            url += "&parentNi=" + parentNi;
            <%--ir CAUK113043267 end--%>

            <%-- set the reCertification flag dynamically --%>
            transactionForm.reCertification.value = "<%=TradePortalConstants.INDICATOR_YES%>";
        }
        displayReauthenticationWindow(url);
    }

    <%--
      // openReauthenticationWindowForList - open the appropriate reauthentication window.
      // This is the list version.  the first checked checkbox value is passed to the
      // reauthentication window for use as the smartcard
      // challenge string and also for logging.  
      //
      // Arguments:
      //   url - typically use the certAuthURL java variable calculated above
      //   formName - the form to submit if authentication is successful.
      //   buttonName - the action to perform if authentication is successful.
      //   reAuthObjectType - the type of reauthentication object.
      //              this is passed through and used for logging.
      //              Valid values: 'Transaction', 'PayRemit', 'Invoice'
    --%>
    function openReauthenticationWindowForList(url,formName,buttonName,reAuthObjectType)
    {
        <%-- //cquinton 1/18/2011 Rel 6.1.0 ir#stul011761874 handle blank url--%>
        <%-- W Zhu 11/13/2015 T36000045444 Use EMPTY_URL marker.  Empty certAuthURL allows the user to by-pass user authentication (see sideBar.jsp) --%>
        if ( url == null || url == '' || url == '<%=EMPTY_URL%>') {
            <%-- BSL IR NRUL080338698 03/28/2012 - Convert numeric character references to Unicode --%>
            alert('<%=StringFunction.asciiToUnicode(resMgr.getTextEscapedJS("E618", "ErrorMessages"))%>');
            setTimeout(function(){ unBusySubmitButton(); }, 50);
            return;
        }
        <%--following verifications ensure args are valid at development time--%>
        if ( formName == null ) {
            alert('Problem opening authentication window: formName required');
        }
        if ( buttonName == null ) {
            alert('Problem opening authentication window: buttonName required');
        }
        if ( reAuthObjectType == null ) {
            alert('Problem opening authentication window: reAuthObjectType required');
        }
        url += "&submitButtonName=" + buttonName;
        url += "&reAuthObjectType=" + reAuthObjectType;

        <%-- pass through the first checkbox value on the form,
             if there is one. The checkbox value is typically a concatenation of
             multiple values.  this is evaluated in the recertification window.
             This is generic for transactions, payment matches, and invoices.
             For smartcard, this value is used for the challenge string.
        --%>
        var form;
        if ( document.getElementsByName(formName).length > 0 ) {
            transactionForm = document.getElementsByName(formName)[0];
            <%--IR T36000019105 start create a form element and set POST method--%>
            form = document.createElement("FORM");
            form.setAttribute("name", "testform");
            form.setAttribute("id", "testform");
            form.setAttribute("method", "POST");
            form.setAttribute("action", url);
	    form.setAttribute("target", "Smartcard");
	    var paramName = "checkbox";
	    <%--IR T36000019105 end--%>
	   		
            allInputs = transactionForm.getElementsByTagName("input");
            <%--ir CNUK113043991 start - pass all selected rows to popup--%>
            var selectedRow = 0;
            for(j=0; j<allInputs.length; j++) {
                for (i=0; i<allInputs.length; i++) {
                    if (allInputs[i].checked && allInputs[i].getAttribute("name") == "checkbox"+j) {
                        var reAuthObjectOid = allInputs[i].value;
                        
                           <%--IR T36000019105 comment this line as we no longer use GET method. Pass oids as part of body (POST method)--%>
                           <%--  url += "&reAuthObjectOid" + selectedRow + "=" + reAuthObjectOid; --%>
                           var hiddenField = document.createElement("input");
                           hiddenField.setAttribute("type", "hidden");
                           hiddenField.setAttribute("name", paramName+selectedRow);
                           hiddenField.setAttribute("value", reAuthObjectOid);
                           console.log(hiddenField);
                           transactionForm.appendChild(hiddenField);
                           
                           var hiddenField1 =document.createElement("input");
                           hiddenField1.setAttribute("type", "hidden");
                           hiddenField1.setAttribute("name", paramName+selectedRow);
                           hiddenField1.setAttribute("value", reAuthObjectOid);
                           form.appendChild(hiddenField1);
                           <%--IR T36000019105 -set the hidden parameters in form element--%>
                           
                        selectedRow++;
                        <%-- break; --%>
                    }
                }
            }
            <%--ir CNUK113043991 end--%>
        }
        <%-- set the reauth flag to multiple --%>
        transactionForm.reCertification.value = "<%=TradePortalConstants.INDICATOR_MULTIPLE%>";

        <%--IR T36000019105 new function passing url and form--%>
        <%-- displayReauthenticationWindow(url); --%>
        displayReauthenticationWindowForList(url,form);
    }
    
    <%--
      // PortalRefresh ER T36000014931 REL 8.1.0.4- This new function has been added for 2FA-Authorize to work from List pages
    
      // openReauthenticationWindowForList - open the appropriate reauthentication window.
      // This is the list version.  the first checked checkbox value is passed to the
      // reauthentication window for use as the smartcard
      // challenge string and also for logging.  
      //
      // Arguments:
      //   url - typically use the certAuthURL java variable calculated above
      //   formName - the form to submit if authentication is successful.
      //   buttonName - the action to perform if authentication is successful.
      //   reAuthObjectType - the type of reauthentication object.
      //              this is passed through and used for logging.
      //              Valid values: 'Transaction', 'PayRemit', 'Invoice'
      //  dataGrid - DataGrid name of the List page
    --%>
    function openReauthenticationWindowForList(url,formName,buttonName,reAuthObjectType,dataGrid)
    {
        <%-- //cquinton 1/18/2011 Rel 6.1.0 ir#stul011761874 handle blank url--%>
        <%-- W Zhu 11/13/2015 T36000045444 Use EMPTY_URL marker.  Empty certAuthURL allows the user to by-pass user authentication (see sideBar.jsp) --%>
        if ( url == null || url == '' || url == '<%=EMPTY_URL%>') {
            <%-- BSL IR NRUL080338698 03/28/2012 - Convert numeric character references to Unicode --%>
            alert('<%=StringFunction.asciiToUnicode(resMgr.getTextEscapedJS("E618", "ErrorMessages"))%>');
            setTimeout(function(){ unBusySubmitButton(); }, 50);
            return;
        }
        <%--following verifications ensure args are valid at development time--%>
        if ( formName == null ) {
            alert('Problem opening authentication window: formName required');
        }
        if ( buttonName == null ) {
            alert('Problem opening authentication window: buttonName required');
        }
        if ( reAuthObjectType == null ) {
            alert('Problem opening authentication window: reAuthObjectType required');
        }
        url += "&submitButtonName=" + buttonName;
        url += "&reAuthObjectType=" + reAuthObjectType;
        
        <%-- Retrieve the selectedRowKeys of the Parent List Page, based on the dataGrid name passed to the function. --%> 
        var rowKeys ;
         require(["dijit/registry", "dojo/query", "dojo/on", "t360/dialog", "dojo/domReady!"],
	   		        function(registry, query, on, dialog ) {
	   		      <%--get array of rowkeys from the grid--%>
	   		      if(registry.byId(dataGrid) != undefined){
	   		    		rowKeys = getSelectedGridRowKeys(dataGrid);
	   		      }
	   	 });
         var form ;
        if ( document.getElementsByName(formName).length > 0 ) {
            transactionForm = document.getElementsByName(formName)[0];
            <%--IR T36000019105 start create a form element and set POST method--%>
            form = document.createElement("FORM");
            form.setAttribute("name", "testform");
            form.setAttribute("id", "testform");
            form.setAttribute("method", "POST");
            form.setAttribute("action", url);
	    form.setAttribute("target", "Smartcard");
	    <%--IR T36000019105 end--%>
			
            <%--ir CAUK113043267 start--%>
            <%-- pass the navigation id reference to the reauth window.
                 This allows the child to get secure parameters --%>
            var parentNi = transactionForm.ni.value;
            url += "&parentNi=" + parentNi;
            
            var selectedRow = 0;
            var paramName = "checkbox";
            
            <%-- Loop through the list of rowKeys and retrieve the primary oid. 
            Create hidden variables of the rowKeys and add to the Form. 
            This is used to submit the selected row information to the mediator at the time of form submit.--%>
            
            if (rowKeys instanceof Array) {
                for (var index in rowKeys) {
                  var reAuthObjectOid = rowKeys[index];
                  <%--IR T36000019105 comment this line as we no longer use GET method. Pass oids as part of body (POST method)--%>
               <%--   url += "&reAuthObjectOid" + selectedRow + "=" + reAuthObjectOid; --%>
                  var hiddenField = document.createElement("input");
                  hiddenField.setAttribute("type", "hidden");
                  hiddenField.setAttribute("name", paramName+selectedRow);
                  hiddenField.setAttribute("value", reAuthObjectOid);
                  console.log(hiddenField);
                  transactionForm.appendChild(hiddenField);
                  <%--IR T36000019105 -set the hidden parameters in form element--%>
                  var hiddenField1 = document.createElement("input");
                  hiddenField1.setAttribute("type", "hidden");
                  hiddenField1.setAttribute("name", paramName+selectedRow);
                  hiddenField1.setAttribute("value", reAuthObjectOid);
                  form.appendChild(hiddenField1);
                  selectedRow++;
                }
              }else{
            	<%--   url += "&reAuthObjectOid" + selectedRow + "=" + rowKeys; --%>
            	  var hiddenField = document.createElement("input");
                  hiddenField.setAttribute("type", "hidden");
                  hiddenField.setAttribute("name", paramName);
                  hiddenField.setAttribute("value", rowKeys);
                  console.log(hiddenField);
                  transactionForm.appendChild(hiddenField);
                  <%--IR T36000019105 -set the hidden parameters in form element--%>
                  var hiddenField1 = document.createElement("input");
                  hiddenField1.setAttribute("type", "hidden");
                  hiddenField1.setAttribute("name", paramName+selectedRow);
                  hiddenField1.setAttribute("value", rowKeys);
                  form.appendChild(hiddenField1);
                  
              }

        }
        <%-- set the reauth flag to multiple --%>
        transactionForm.reCertification.value = "<%=TradePortalConstants.INDICATOR_MULTIPLE%>";
        <%--IR T36000019105 new function passing url and form--%>
        <%-- displayReauthenticationWindow(url); --%>
        displayReauthenticationWindowForList(url,form);
    }

    <%--
      // Conditionally open the reauthentication window.
      // This is meant to be called from a list view where it is dependent on
      // the RequireAuth column to determine if the action requires authentication.
      // if any of the selected rows require authentication, open the reauthentication window
      // otherwise, submit the form/button.
      // See openReauthenticationWindow for parameter descriptions.
    --%>
    function openReauthWindowOnRequireAuth(url,formName,buttonName,reAuthObjectType) {
        <%--following verifications ensure args are valid at development time--%>
        if ( formName == null ) {
            alert('Problem opening authentication window: formName required');
        }
        if ( buttonName == null ) {
            alert('Problem opening authentication window: buttonName required');
        }

        var authRequired = 'N';
        if ( document.getElementsByName(formName).length > 0 ) {
            transactionForm = document.getElementsByName(formName)[0];
            allInputs = transactionForm.getElementsByTagName("input");
            for (i=0; i<allInputs.length; i++) {
                if (allInputs[i].type == "checkbox" && allInputs[i].checked && allInputs[i].getAttribute("name") != "sAll" ) {          
                    var index = allInputs[i].name.split("checkbox")[1];

                    var rowAuthRequired = document.getElementById("RequireAuth"+index).childNodes[0].nodeValue;
                    rowAuthRequired = rowAuthRequired.replace(/^\s+|\s+$/g, '');
                    if ( 'Y' == rowAuthRequired ) {
                        authRequired = 'Y';
                    }
                }
            }
            if ( 'Y' == authRequired ) {
                openReauthenticationWindowForList(url,formName,buttonName,reAuthObjectType);
            } else {
                <%-- get the form number --%>
                var formNumber = 0; <%-- default --%>
                for (var i=0; i<= document.forms.length; i++) {
                    var myForm = document.forms[i];
                    if ( myForm != null && myForm.name == formName ) {
                        formNumber = i;
                    }
                }
                <%-- transactionForm.buttonName.value=buttonName; --%>
                setButtonPressed(buttonName, formNumber); 
                transactionForm.submit();
            }
        } else {
            alert('Problem opening authentication window: form not found');
        }
    }

    <%--cr498 end add new functions--%>
    <%--cr498 remove old functions no longer used

    function openReauthenticationWindowForInstrumentType(url,form,instTypeCode) 
    {
        var reAuth = false;
        var selectedTransactionOid = "";
        if ( document.getElementsByName(form).length > 0 ) {
            transactionForm = document.getElementsByName(form)[0];
            allInputs = transactionForm.getElementsByTagName("input");
            for (i=0; i<allInputs.length; i++) {
                if (allInputs[i].type == "checkbox" && allInputs[i].checked) {
                    var index = allInputs[i].name.split("checkbox")[1];
                    var instType = document.getElementById("InstrumentTypeCode"+index).childNodes[0].nodeValue;
                    instType=instType.replace(/^\s+|\s+$/g, '');
                     
                    if (instType == instTypeCode) {
                        selectedTransactionOid = allInputs[i].value;
                        url += "&TransactionOid=" + selectedTransactionOid;
                        reAuth = true;
                        break;
                    }
                }
            }
        }
        if (reAuth) {
            window.document.forms[0].reCertification.value ="<%=TradePortalConstants.INDICATOR_MULTIPLE%>";
            displayReauthenticationWindow(url);
        }
        else {
            setButtonPressed('Authorize', 0); 
            transactionForm.submit();
        }
    }
    --%>

    function displayReauthenticationWindow (url) 
    {
<%
  //AAlubala - CR-711 Rel 8.0 - 11/01/2011
  //If using VASCO, the popup win will be a little larger - START
  //04/01/2012 - Modify window dimensions

  if (TradePortalConstants.AUTH_2FA.equals(additionalAuthType)) {
    if (TradePortalConstants.VASCO_TOKEN.equalsIgnoreCase(user.getAttribute("token_type")) &&
        TradePortalConstants.AUTH_2FA_SUBTYPE_SIG.equalsIgnoreCase(user.getAttribute("auth2fa_subtype"))) {
    //IR#MNUM062844286 - increase window dimensions
%>            
      winHeight=480;
      winWidth=725;
<%
    }
    else {
%>
      winHeight=250;
      winWidth=725;                 
<%
    }
  }
  else {
%>
      if (url.indexOf("<%=TradePortalConstants.AUTHENT_PAGE_DUMMY%>") != -1)
      {
        winHeight=300;
        winWidth=725;                 
      }
      else if (url.indexOf("Gemalto") != -1) 
      {
        winHeight=400;
        winWidth=725;
      }
      else
      {
        winHeight=200;
        winWidth=525;  
      }
<%
  }
  //END CR-711
%> 		        
        horizLocation = (screen.width-winWidth)/2;
        vertLocation =  (screen.height-winHeight)/2;
        if (url.indexOf("Gemalto") != -1) 
        {
            widgets = 'toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,';
        }
        else
        {
             widgets = 'toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=no,';
        }
        widgets = widgets + 'width='+winWidth+',height='+winHeight+',top='+vertLocation+',left='+horizLocation;
        reauthenticationWin = window.open(url, 'Smartcard', widgets);
        reauthenticationWin.focus();
    }
    
    <%--IR T36000019105 start- added new function passing url and form ....window dimensions are not chnaged--%>
    function displayReauthenticationWindowForList (url,form) 
    {
<%
  if (TradePortalConstants.AUTH_2FA.equals(additionalAuthType)) {
    if (TradePortalConstants.VASCO_TOKEN.equalsIgnoreCase(user.getAttribute("token_type")) &&
        TradePortalConstants.AUTH_2FA_SUBTYPE_SIG.equalsIgnoreCase(user.getAttribute("auth2fa_subtype"))) {

%>            
      winHeight=480;
      winWidth=725;
<%
    }
    else {
%>
      winHeight=250;
      winWidth=725;                 
<%
    }
  }
  else {
%>
      if (url.indexOf("<%=TradePortalConstants.AUTHENT_PAGE_DUMMY%>") != -1)
      {
        winHeight=300;
        winWidth=725;                 
      }
      else if (url.indexOf("Gemalto") != -1) 
      {
        winHeight=400;
        winWidth=725;
      }
      else {
        winHeight=200;
        winWidth=525;  
      }
<%
  }

%> 		        
        horizLocation = (screen.width-winWidth)/2;
        vertLocation =  (screen.height-winHeight)/2;
        if (url.indexOf("Gemalto") != -1) 
        { 
            widgets = 'toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,';
        }
        else
        { 
            widgets = 'toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=no,';
        }
        widgets = widgets + 'width='+winWidth+',height='+winHeight+',top='+vertLocation+',left='+horizLocation;
     
       document.body.appendChild(form);

     <%--  creating the 'Smartcard' window with custom features prior to submitting the form --%>
     window.open("_blank", "Smartcard", widgets);
	 form.submit();
    }
    <%--IR T36000019105 end --%>
</script>
