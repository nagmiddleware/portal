<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.web.*, com.amsinc.ecsg.frame.*, com.ams.tradeportal.common.*, com.ams.tradeportal.html.*, com.amsinc.ecsg.util.*" %>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager"      scope="session"></jsp:useBean>
<jsp:useBean id="resMgr"  class="com.amsinc.ecsg.util.ResourceManager" scope="session"></jsp:useBean>
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>

<%--cquinton 9/19/2012 ir#5507 removed expires meta tag - header.jsp should always be first so doctype is first.
    dispatcher.jsp ensures the expires header is sent regardless--%>

<%

	
   String brandingDirectory = StringFunction.xssCharsToHtml(request.getParameter("branding")); //T36000004579 #3;
%>
<jsp:include page="/common/Header.jsp">
  <jsp:param name="minimalHeaderFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
  <jsp:param name="brandingDirectory" value="<%=brandingDirectory%>" />
  <jsp:param name="checkChangePassword" value="<%= TradePortalConstants.INDICATOR_NO %>" />
</jsp:include>

<div class="pageMainNoSidebar">
  <div class="pageContent">

    <%-- include a blank page header just for spacing --%>
    <jsp:include page="/common/PageHeader.jsp">
      <jsp:param name="titleKey" value="LoginFailed.AccountLockedTitle" />
      <jsp:param name="helpUrl" value="customer/login_failed.htm" />
    </jsp:include>

<form>

  <div class="formContentNoSidebar padded">

    <div class="instruction">
      <%= resMgr.getText("LoginFailed.AccountLockedPart1", TradePortalConstants.TEXT_BUNDLE) %>
      <%= formMgr.getFromDocCache().getAttribute("/Out/lockedOutNumFailedLogons") %>
      <%= resMgr.getText("LoginFailed.AccountLockedPart2", TradePortalConstants.TEXT_BUNDLE) %>
    </div>

  </div>

</form>

  </div>
</div>

<jsp:include page="/common/Footer.jsp">
  <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_NO%>" />
  <jsp:param name="minimalHeaderFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
  <jsp:param name="includeTimeoutForward" value="false" />
</jsp:include>

<%
   // Invalidate the user's session so that they cannot log on under this session again
   HttpSession userHttpSession = request.getSession(false);

   userHttpSession.invalidate();
%>

</body>
</html>
