<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.web.*, com.amsinc.ecsg.frame.*, com.ams.tradeportal.common.*, 
com.ams.tradeportal.html.*,com.crystaldecisions.sdk.framework.IEnterpriseSession,
com.ams.tradeportal.busobj.*, java.util.*, com.amsinc.ecsg.util.*" %>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager"      scope="session"></jsp:useBean>
<jsp:useBean id="resMgr"  class="com.amsinc.ecsg.util.ResourceManager" scope="session"></jsp:useBean>
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>

<%--cquinton 9/10/2012 move header up so doc type is first thing written on page--%>
<jsp:include page="/common/Header.jsp">
  <jsp:param name="minimalHeaderFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>


<%


  boolean isThereAReportingSession = userSession.getReportLoginInd();
  //BSL CR 749 05/15/2012 Rel 8.0 BEGIN
  // Determine if there is a SiteMinder Web Agent SSO session that should be terminated
  boolean isThereASiteMinderWebAgentSession = userSession.isUsingSiteMinderWebAgentSSO();
  //BSL CR 749 05/15/2012 Rel 8.0 END
  
  //cquinton 4/30/2013 Rel 8.1.0.6 ir#16255 start
  PropertyResourceBundle tpProperties = (PropertyResourceBundle) PropertyResourceBundle.getBundle("TradePortal");	
  //read ssowebagent logout url, if it exists, null means use hardcoded value
  String ssoWebAgentLogoutUrl = null;
  if ( isThereASiteMinderWebAgentSession ) {
    try {
      ssoWebAgentLogoutUrl = tpProperties.getString("WebAgent_SSO_Logout_URL");
    } catch ( Exception ex ) {
      // Report there was a problem and and set to null so default is used below
      System.out.println("Logout.jsp: problem reading WebAgent_SSO_Logout_URL. Defaulting");
      ssoWebAgentLogoutUrl = null;
    }
  }
  //cquinton 4/30/2013 Rel 8.1.0.6 ir#16255 end

  // Only go in here if there is a reporting session to clean up

  System.out.println("Logout: isThereAReportingSession = "+ isThereAReportingSession);

  String version = resMgr.getText("version", "TradePortalVersion");
  if (version.equals("version")) {
    version = resMgr.getText("Footer.UnknownVersion", 
                             TradePortalConstants.TEXT_BUNDLE);
  }
//M Eerupula 03/21/2013 Rel 8.2 T3600001495 version is encrypted consistently using Base64 encoding to fix the URL difference for cache busting, means the version is same on all included pages 
	//version = EncryptDecrypt.bytesToBase64String(version.getBytes()); 
	//RPasupulati geting first 4 char's from encripted version String IR no T36000017311 Starts.
    version = EncryptDecrypt.encryptStringUsingTripleDes(version, userSession.getSecretKey());
	version = version.substring(0, 4);
    //RPasupulati geting first 4 char's from encripted version String IR no T36000017311 ENDs.
if(isThereAReportingSession)
{
	
	try
	{
		IEnterpriseSession enterpriseSession;
		
		enterpriseSession =(IEnterpriseSession) session.getAttribute("CE_ENTERPRISESESSION");
		
		    session.removeAttribute("CE_ENTERPRISESESSION");
		    
		    		
		    if(enterpriseSession != null)
		    {
		
			enterpriseSession.logoff();
		        enterpriseSession = null;

			System.out.println("Successfully closed BO XI Session - Logout");
		
		    }
					
		
		
	}//end of try
	catch(Exception e)
	{
		System.out.println("Exception removing BO XI session - logout");
		e.printStackTrace();

		// Remove the webiServer so that portal will attempt to 
		// reconnect to the reports server next time

		//------commented by Sarang Deshpande on 09/11/05 - testing for possible bugs-----
		//application.removeAttribute("webiServer");
		//--------------------------------------------------------------------------------

		userSession.setReportLoginInd(false);
	}
  }//end of if
  //CR 495 Vasavi 06/07/10 Begin
  String         brandingDirectory       = null;
  boolean 	  showLogo		          = true;

  // Retrieve the user's branding directory to obtain the appropriate css stylesheet and logo
  if(request.getParameter("brandingDirectory") != null)
  {
      brandingDirectory = request.getParameter("brandingDirectory");
  }
  else
  {
      brandingDirectory = userSession.getBrandingDirectory();

      if(brandingDirectory == null)
       {
          brandingDirectory = TradePortalConstants.DEFAULT_BRANDING;
          showLogo = false;
       }
    }
  //CR 495 Vasavi 06/07/10 End

  //cquinton 4/30/2013 Rel 8.1.0.6 ir#16255 start
  if (isThereASiteMinderWebAgentSession) {
    if ( ssoWebAgentLogoutUrl!=null && ssoWebAgentLogoutUrl.length()>0 ) {
      response.sendRedirect(ssoWebAgentLogoutUrl);
    }
    else {
      //go to the default web agent logout url
      //note: this really should use sendRedirect as well rather than refresh
      // as that flashes content and is deprecated, but to avoid risk
      // with ER deploy am leaving code as is...
%>
<meta http-equiv="refresh" content="0;url=/saam/SAAMLogin/logout.fcc" />
<%
    }
  }
  //cquinton 4/30/2013 Rel 8.1.0.6 ir#16255 end
%>

<div class="pageMainNoSidebar">
  <div id="pageContent" class="pageContent">

    <jsp:include page="/common/PageHeader.jsp">
      <jsp:param name="titleKey" value="Logout.YouHaveBeenLoggedOut" />
      <jsp:param name="helpUrl" value="customer/logged_out.htm" />
    </jsp:include>

<form>
      
  <div class="formContentNoSidebar padded">


<%
  //CR 495 Vasavi 06/07/10 Begin
  // If a branded header is available, it will exist as a text resource entry
  // with the key Header.PageHeading.xxx, where xxx is the branding directory (case sensitive)

  //todo: not sure if this branded heading stuff is still valid???
  String brandedHeadingTextResourceKey = "Header.PageHeading."+brandingDirectory;
  String headingText;

  try {
    // Attempt to get the branded heading
    headingText = resMgr.getResource(TradePortalConstants.TEXT_BUNDLE).getString(brandedHeadingTextResourceKey);
  }
  catch (MissingResourceException e) {
    // If none exists, use the default
    headingText = resMgr.getText("Header.PageHeading", TradePortalConstants.TEXT_BUNDLE);
  }
  //CR 495 Vasavi 06/07/10 End
%>         
    <div class="instruction">
      <%= resMgr.getText("Logout.LogoutMessage", TradePortalConstants.TEXT_BUNDLE)%>&nbsp;<%= headingText%>.
    </div>

  </div>

</form>

  </div>
</div>


<script src="<%=userSession.getdojoJsPath()%>/dojoconfig.js?<%= version %>"></script>
<script src='<%=userSession.getdojoJsPath()%>/dojo/dojo.js?<%= version %>' data-dojo-config="async: 1" ></script>
<script>
  require(["dojo/dom","dojo/parser", "dojo/dom-class",
           "t360/widget/Tooltip"], 
      function(dom,parser, domClass){
    parser.parse(dom.byId("pageContent"));
    domClass.add(document.body, 'loaded');
});
</script>
<%-- pmitnala - Changes Begin-Help link is not working without include of common.js. Hence this change --%>
<script src="<%=userSession.getdojoJsPath()%>/common.js"></script>
<%-- pmitnala - Changes End --%>

<%
   HttpSession theSession = request.getSession(false);
   theSession.invalidate();
%>
</body>
</html>
