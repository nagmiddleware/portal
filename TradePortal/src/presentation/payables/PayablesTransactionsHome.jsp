<%--
**********************************************************************************
  Receivables Transactions Home

  Description:
     This page is used as the main Receivables Management page. It displays one of
     three tabs: Receivable Match Notices, Invoices, Receivables Instruments.

**********************************************************************************
--%>

<%--
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
--%>
<%@ page import="java.util.*,com.amsinc.ecsg.frame.*, com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*, com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*, com.ams.tradeportal.html.*,
                 com.ams.tradeportal.busobj.webbean.*, com.ams.tradeportal.busobj.util.*" %>

<jsp:useBean id="beanMgr"     class="com.amsinc.ecsg.web.BeanManager"            scope="session"></jsp:useBean>
<jsp:useBean id="formMgr"     class="com.amsinc.ecsg.web.FormManager"            scope="session"></jsp:useBean>
<jsp:useBean id="resMgr"      class="com.amsinc.ecsg.util.ResourceManager"       scope="session"></jsp:useBean>
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>

<% 
  WidgetFactory widgetFactory = new WidgetFactory(resMgr,userSession);
  ReferenceDataManager refData = ReferenceDataManager.getRefDataMgr();

  StringBuffer      onLoad                 = new StringBuffer();
  Hashtable         secureParms            = new Hashtable();
  String            helpSensitiveLink      = null;
  String            userSecurityRights     = null;
  String            userSecurityType       = null;
  String            current2ndNav          = null;
  String            historySearchNav       = null;
  String            userOrgOid             = null;
  String            userOid                = null;
  String            formName               = "";
  StringBuffer      newLink                = new StringBuffer();
  boolean           canViewPayablesMgmt  = false;
  String dataViewName = "";
  String utilisedStatusType = "";
  String appliedStatusType  = "";

  String loginLocale = StringFunction.xssCharsToHtml(userSession.getUserLocale());

  Map searchQueryMap =  userSession.getSavedSearchQueryData();
  Map savedSearchQueryData =null;
  String datePattern = userSession.getDatePattern();
  String datePatternDisplay = refData.getDescr("DATEPATTERN", datePattern, loginLocale);
  String dateWidgetOptions = "placeHolder:'" + datePatternDisplay + "'";
   
   //Added for all the options
   String initSearchParms="";
   DataGridFactory dgFactory = new DataGridFactory(resMgr, userSession, formMgr, beanMgr, response);
   String gridHtml = "";
   String gridLayout = "";
   String encryptedSelectedWorkflow ="";
   
   userSession.setCurrentPrimaryNavigation("NavigationBar.Transactions");
   if ( "true".equals( request.getParameter("returning") ) ) {
     userSession.pageBack(); //to keep it correct
   }
   else {
     userSession.addPage("goToPayableTransactionsHome");
   }
   
 //IR T36000026794 Rel9.0 07/24/2014 starts
   session.setAttribute("startHomePage", "PayableTransactionsHome");
   session.removeAttribute("fromTPHomePage");
   //IR T36000026794 Rel9.0 07/24/2014 End


   StringBuffer newSearchReceivableCriteria = new StringBuffer();

   userSecurityRights           = userSession.getSecurityRights();
   userSecurityType             = userSession.getSecurityType();
   userOrgOid                   = StringFunction.xssCharsToHtml(userSession.getOwnerOrgOid());
   userOid                      = userSession.getUserOid();

   secureParms.put("SecurityRights",        userSecurityRights);
   CorporateOrganizationWebBean org = beanMgr.createBean(CorporateOrganizationWebBean.class, "CorporateOrganization");
   org.getById(userOrgOid);
  
   canViewPayablesMgmt  = SecurityAccess.hasRights(userSecurityRights,  SecurityAccess.ACCESS_RECEIVABLES_AREA);

   current2ndNav = request.getParameter("current2ndNav");
   historySearchNav = request.getParameter("historySearchNav");
   if (current2ndNav == null)
   {
      current2ndNav = (String) session.getAttribute("payTransactions2ndNav");
      historySearchNav = (String) session.getAttribute("historySearchNav");
      if (current2ndNav == null)
      {
         current2ndNav = TradePortalConstants.NAV__PAY_INQUIRIES;
         if(historySearchNav == null){
        	 historySearchNav = TradePortalConstants.NAV__INSTR_INQUIRIES;
         }
      }
   }
   if(historySearchNav == null){
	   historySearchNav = TradePortalConstants.NAV__INSTR_INQUIRIES;
   }
   // Now perform data setup for whichever is the current tab.  Each block of
   // code sets tab specific parameters (for data retrieval or dropdowns) as
   // well as setting common things like the appropriate help link and formName


   // Set the performance statistic object to have the current tab as its context
   PerformanceStatistic perfStat = (PerformanceStatistic) session.getAttribute("performanceStatistic");

   if((perfStat != null) && (perfStat.isLinkAction()))
           perfStat.setContext(current2ndNav);

   // ***********************************************************************
   // Data setup for the Invoices tab
   // ***********************************************************************
   if (TradePortalConstants.NAV__PAY_INVOICES.equals(current2ndNav))
   {
      formName     = "PayMgmtInvForm";

      helpSensitiveLink = OnlineHelp.createContextSensitiveLink("customer/invoice_listview.htm",  resMgr, userSession);

   %>
   <%
   }  // End data setup for Invoice tab.
   else if (TradePortalConstants.NAV__PAY_INQUIRIES.equals(current2ndNav))
   {
	      formName     = "PayMgmtInvForm";

	      helpSensitiveLink = OnlineHelp.createContextSensitiveLink("customer/invoice_listview.htm",  resMgr, userSession);
   }else if (TradePortalConstants.NAV__PAY_CREDIT_NOTES.equals(current2ndNav))
   {
	      formName     = "PayMgmtInvForm";

	      helpSensitiveLink = OnlineHelp.createContextSensitiveLink("customer/invoice_listview.htm",  resMgr, userSession);
}
   String searchNav = "payTranHome."+current2ndNav;
%>

<%-- ********************* HTML for page begins here *********************  --%>

<jsp:include page="/common/Header.jsp">
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="additionalOnLoad"         value="<%=onLoad.toString()%>" />
</jsp:include>

<%--  including TransactionCleanup.jsp to remove lock on instrument which has been placed by this user... --%>
<%--  this is required because (with Portal-refresh) user can navigate anywhere from MenuBar when on Instrument(detail) page. --%>
<jsp:include page="/transactions/TransactionCleanup.jsp"></jsp:include>


<jsp:include page="/common/SearchCriteriaCleanup.jsp">
<jsp:param name="searchCriteriaSection" value="<%=searchNav%>"/>
</jsp:include>

<div class="pageMainNoSidebar">
<%-- IR T36000030360: (gridInvoiceFooterDivHidden) Hide page content, untill widgets get parsed. --%>
<div id="InvoiceMgtDivID" class="pageContent gridInvoiceFooterDivHidden">


<div class="secondaryNav">

  <div class="secondaryNavTitle">
    <%=resMgr.getText("SecondaryNavigation.Payables",
                      TradePortalConstants.TEXT_BUNDLE)%>
  </div>


  <%
    String invoicesSelected  = TradePortalConstants.INDICATOR_NO;
    String inquiriesSelected = TradePortalConstants.INDICATOR_NO;
    String creditNotesSelected = TradePortalConstants.INDICATOR_NO;
      if (TradePortalConstants.NAV__PAY_INVOICES.equals(current2ndNav))
    {
      invoicesSelected = TradePortalConstants.INDICATOR_YES;
    }
    else if(TradePortalConstants.NAV__PAY_INVOICES.equals(current2ndNav))
    {
      inquiriesSelected = TradePortalConstants.INDICATOR_YES;
    }
    else if(TradePortalConstants.NAV__PAY_CREDIT_NOTES.equals(current2ndNav))
    {
      creditNotesSelected = TradePortalConstants.INDICATOR_YES;
    }  

    String linkParms = "";
   
    if(canViewPayablesMgmt == true) {
      linkParms = "current2ndNav=" + TradePortalConstants.NAV__PAY_INVOICES;
  %>
  <jsp:include page="/common/NavigationLink.jsp">
    <jsp:param name="linkTextKey"  value="SecondaryNavigation.Payables.Invoices" />
    <jsp:param name="linkSelected" value="<%= invoicesSelected %>" />
    <jsp:param name="action"       value="goToPayableTransactionsHome" />
    <jsp:param name="linkParms"    value="<%= linkParms %>" />
    <jsp:param name="hoverText"    value="PayableTransaction.invoicesList" />
  </jsp:include>

  <%
  linkParms = "current2ndNav=" + TradePortalConstants.NAV__PAY_INQUIRIES;
  %>
  <jsp:include page="/common/NavigationLink.jsp">
    <jsp:param name="linkTextKey"  value="SecondaryNavigation.Payables.Inquiries" />
    <jsp:param name="linkSelected" value="<%= inquiriesSelected %>" />
    <jsp:param name="action"       value="goToPayableTransactionsHome" />
    <jsp:param name="linkParms"    value="<%= linkParms %>" />
    <jsp:param name="hoverText"    value="PayableTransaction.invoicesList" />
  </jsp:include>
  <%
  linkParms = "current2ndNav=" + TradePortalConstants.NAV__PAY_CREDIT_NOTES;
  %>
  <jsp:include page="/common/NavigationLink.jsp">
    <jsp:param name="linkTextKey"  value="SecondaryNavigation.Payables.CreditNotes" />
    <jsp:param name="linkSelected" value="<%= creditNotesSelected %>" />
    <jsp:param name="action"       value="goToPayableTransactionsHome" />
    <jsp:param name="linkParms"    value="<%= linkParms %>" />
    <jsp:param name="hoverText"    value="PayableTransaction.invoicesList" />
  </jsp:include>
  <%}
  %>

  <div id="secondaryNavHelp" class="secondaryNavHelp">
    <%=helpSensitiveLink%>
  </div>
  <%=widgetFactory.createHoverHelp("secondaryNavHelp","HelpImageLinkHoverText") %>
  <div style="clear:both;"></div>

</div>


<%
    String certAuthURL = ""; //needed for openReauth frag
    Cache reCertCache = (Cache)TPCacheManager.getInstance().getCache(TradePortalConstants.CLIENT_BANK_CACHE);
    DocumentHandler CBCResult = (DocumentHandler)reCertCache.get(userSession.getClientBankOid());
    String requireTranAuth = CBCResult.getAttribute("/ResultSetRecord(0)/REQUIRE_TRAN_AUTH");
    boolean authoriseInvRequireAuth = InstrumentAuthentication.requireTransactionAuthentication(
        requireTranAuth,InstrumentAuthentication.TRAN_AUTH__PAYABLES_MGMT_INV_UPDATES);
    boolean offlineAuthoriseRequireAuth = SecurityAccess.hasRights(userSecurityRights,  SecurityAccess.PAY_MGM_OFFLINE_AUTH);
    String authoriseInvLink = "";
    if (authoriseInvRequireAuth || offlineAuthoriseRequireAuth) {
%>
  <%@ include file="/logon/fragments/openReauthenticationWindow.frag" %>
<%
        authoriseInvLink = "javascript:openReauthenticationWindowForList('" + certAuthURL + "'," +
            "'" + formName + "','" + TradePortalConstants.BUTTON_AUTHORIZE + "'," +
            "'PAYBInvoice','invoicesGrid')";
    }

%>

<form name="<%=formName%>" id="<%=formName%>" method="POST"
      action="<%= formMgr.getSubmitAction(response) %>" style="margin-bottom: 0px;">

 <jsp:include page="/common/ErrorSection.jsp" />

 <div class="formContentNoSidebar">
  <input type=hidden value="" name=buttonName>
 <input type="hidden" name="paymentDate" />
 <input type="hidden" name="paymentAmount" />
 <input type="hidden" name="supplierDate" />
<%   if (authoriseInvRequireAuth || offlineAuthoriseRequireAuth) {
%>

      <input type=hidden name="reCertification" value="<%=TradePortalConstants.INDICATOR_NO%>">
      <input type=hidden name="reCertOK">
      <input type=hidden name="logonResponse">
      <input type=hidden name="logonCertificate">

<%
    }
  // Based on the current link, display the appropriate HTML

   if (TradePortalConstants.NAV__PAY_INVOICES.equals(current2ndNav))
  {
	   if(searchQueryMap!=null && !searchQueryMap.isEmpty()){
		 savedSearchQueryData = (Map)searchQueryMap.get("PayablesMgmtInvoicesDataView");
	}
%>
    <%@ include file="/payables/fragments/PayablesManagement_Invoices.frag" %>

<%
  }else if(TradePortalConstants.NAV__PAY_INQUIRIES.equals(current2ndNav))
  {
      if(searchQueryMap!=null && !searchQueryMap.isEmpty()){
    	  if(TradePortalConstants.NAV__INSTR_INQUIRIES.equals(historySearchNav)){ 
    		    savedSearchQueryData = (Map)searchQueryMap.get("PayablesMgmtInstrumentDataView");
    	  }else if(TradePortalConstants.NAV__PYMTS_INQUIRIES.equals(historySearchNav)){
    		    savedSearchQueryData = (Map)searchQueryMap.get("PayablesMgmtPaymentsDataView");
    	  }
   }
%>
   <%@ include file="/payables/fragments/PayablesManagement_Instruments.frag" %>

<%
 }else if (TradePortalConstants.NAV__PAY_CREDIT_NOTES.equals(current2ndNav))
 {
	 if(searchQueryMap!=null && !searchQueryMap.isEmpty()){
		 savedSearchQueryData = (Map)searchQueryMap.get("PayablesMgmtCreditNoteDataView");
	}
%>
	 <%@ include file="/payables/fragments/PayablesManagement_CreditNotes.frag" %>
<%	 
 }
%>

<%= formMgr.getFormInstanceAsInputField(formName, secureParms) %>
</div>
</form>

</div>
</div>


<form method="post" id="Payables-NotifyPanelUserForm" name="Payables-NotifyPanelUserForm" action="<%=formMgr.getSubmitAction(response)%>">
<input type=hidden name="buttonName" value="">
<input type=hidden name="invoiceSummaryOid" value="">
<%= formMgr.getFormInstanceAsInputField("Payables-NotifyPanelUserForm", secureParms) %>
 </form>
 
<jsp:include page="/common/Footer.jsp">
  <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
  <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>

<%@ include file="/common/GetSavedSearchQueryData.frag" %>
<div id="InvoiceGroupListDialogID" ></div>
<script>

	var initSearchParms="";
	var loginLocale="";
	var instrument="";
	var formName = '<%=formName%>';

	function initializeDataGrid(DataGridId,DataView){

		var gridLayout ;
		<%if(gridLayout!=""){%>
			gridLayout = <%=gridLayout%>;
		<%}%>
			<%--set the initial search parms--%>
		initSearchParms = "<%=StringFunction.xssCharsToHtml(initSearchParms)%>";
		var savedSort = savedSearchQueryData["sort"];  
		 require(["t360/datagrid", "dojo/dom","dijit/registry"],
			        function( t360grid,dom,registry ) {
		if(DataGridId == "invoicesGrid"){
			if("" == savedSort || null == savedSort || "undefined" == savedSort){
		        savedSort = 'InvoiceID';
		     }
			var currency = savedSearchQueryData["currency"]; 
         	var amountFrom = savedSearchQueryData["amountFrom"]; 
         	var amountTo = savedSearchQueryData["amountTo"]; 
         	var fromDate = savedSearchQueryData["fromDate"]; 
         	var toDate = savedSearchQueryData["toDate"]; 
         	var amountType = savedSearchQueryData["amountType"]; 
        	var dateType = savedSearchQueryData["dateType"]; 
        	
        	var invoiceID = savedSearchQueryData["invoiceID"]; 
         	var sellerIdentifier = savedSearchQueryData["sellerIdentifier"]; 
         	
         	if("" == currency || null == currency || "undefined" == currency)
           		currency=dom.byId("Currency").value;
      		 else
      		 {
      			registry.byId("Currency").set('value',currency);
	  		  	}
           	if("" == invoiceID || null == invoiceID || "undefined" == invoiceID)
           		invoiceID=dom.byId("InvoiceID").value.toUpperCase();
      		 else
      			dom.byId("InvoiceID").value = invoiceID;
           	
           	if("" == sellerIdentifier || null == sellerIdentifier || "undefined" == sellerIdentifier)
           		sellerIdentifier=dom.byId("SellerIdentifier").value.toUpperCase();
      		 else
      			dom.byId("SellerIdentifier").value = sellerIdentifier;
           	
           	if("" == amountFrom || null == amountFrom || "undefined" == amountFrom)
           		amountFrom=dom.byId("AmountFrom").value;
      		 else
      			dom.byId("AmountFrom").value = amountFrom;

           	if("" == amountTo || null == amountTo || "undefined" == amountTo)
           		amountTo=dom.byId("AmountTo").value;
      		 else
      			dom.byId("AmountTo").value = amountTo;
        	
        	if("" == fromDate || null == fromDate || "undefined" == fromDate)
        		fromDate=dom.byId("DateFrom").value;
      		 else{
      			registry.byId("DateFrom").set('displayedValue',fromDate);
      		 }
        	
	    	if("" == toDate || null == toDate || "undefined" == toDate)
	    		toDate=dom.byId("DateTo").value;
	  		 else
	  			registry.byId("DateTo").set('displayedValue',toDate);
	    	
	    	if("" == amountType || null == amountType || "undefined" == amountType)
	    		amountType=dom.byId("AmountType").value;
	  		 else
	  		 {
	  			registry.byId("AmountType").set('value',amountType);
	  		  	}
	    	if("" == dateType || null == dateType || "undefined" == dateType)
	    		dateType=dom.byId("DateType").value;
	  		 else
	  		 {
	  			registry.byId("DateType").set('value',dateType);
	  		  	}
	    	
	    	<%-- KMehta - 24 Sep 2014 - Rel9.1 IR-T36000032371 - Change  - Begin --%>
	    	initSearchParms = initSearchParms+"&userOrgOid="+<%=userOrgOid%>+"&invoiceID="+invoiceID
	    	+"&sellerIdentifier="+sellerIdentifier+"&currency="+currency+"&amountType="+amountType+"&amountFrom="+amountFrom+"&amountTo="+amountTo
	    	+"&dateType="+dateType+"&fromDate="+fromDate+"&toDate="+toDate;
	    	   var tempDatePattern = encodeURI('<%=datePattern%>');
		        initSearchParms = initSearchParms+"&dPattern="+tempDatePattern;
		        <%-- KMehta - 24 Sep 2014 - Rel9.1 IR-T36000032371 - Change  - End --%>

		}
	
		loginLocale="<%=loginLocale%>";
		console.log("initSearchParms: " + initSearchParms);
		<%--create the grid, attaching it to the div id specified created in dataGridDataGridFactory.createDataGrid.jsp()--%>
		createDataGrid(DataGridId, DataView,gridLayout, initSearchParms,savedSort); 

		 });
	}

	function booleanFormat(item){
		if(item=='true')
			return true;
		else
			return false;
	}
	<%--provide a search function that collects the necessary search parameters--%>
	  function searchInvoices() {
	    require(["dijit/registry","dojo/dom"],
	      function(registry,dom){

	    	var invoiceID=(dom.byId("InvoiceID").value).toUpperCase();
	    	var sellerIdentifier=(dom.byId("SellerIdentifier").value).toUpperCase();
	    	
	    	var currency=(registry.byId("Currency").value).toUpperCase();
	    	var amountType=(registry.byId("AmountType").value).toUpperCase();
	    	var amountFrom=(dom.byId("AmountFrom").value).toUpperCase();
	    	var amountTo=(dom.byId("AmountTo").value).toUpperCase();

	    	var dateType=(registry.byId("DateType").value).toUpperCase();
	    	var fromDate=(dom.byId("DateFrom").value).toUpperCase();
	    	var toDate=(dom.byId("DateTo").value).toUpperCase();
	    	
	    	var endToEndId=(dom.byId("EndToEndId").value).toUpperCase();

	        var searchParms = "userOrgOid="+<%=userOrgOid%>+"&invoiceID="+invoiceID+"&sellerIdentifier="+
	        sellerIdentifier+"&currency="+currency+"&amountType="+amountType+"&amountFrom="+amountFrom+"&amountTo="+amountTo+"&dateType="+
	        dateType+"&fromDate="+fromDate+"&toDate="+toDate+"&endToEndId="+endToEndId;
			
	        var tempDatePattern = encodeURI('<%=datePattern%>');
	 		searchParms=searchParms+"&dPattern="+tempDatePattern;
	 		
	        console.log("SearchParms: " + searchParms);
	        searchDataGrid("invoicesGrid", "PayablesMgmtInvoicesDataView",
	                       searchParms);
	      });
	  }
	
	 
	  require(["dijit/registry", "dojo/query", "dojo/on", "dojo/dom-class", "t360/dialog", 
	           "dojo/dom", "dijit/layout/ContentPane", "dijit/TooltipDialog", "dijit/popup",
	           "t360/common", "t360/popup", "t360/datagrid",
	           "dojo/ready", "dojo/domReady!"],
	      function(registry, query, on, domClass, dialog,
	               dom, ContentPane, TooltipDialog, popup, 
	               common, t360popup, t360grid,
	               ready ) {
		
	    <%--register widget event handlers--%>
	    ready(function() {
	    	<%-- Based on the conditions Datagrid will be intialize --%>

	    	<% if (TradePortalConstants.NAV__PAY_INVOICES.equals(current2ndNav)){
	    		dataViewName = EncryptDecrypt.encryptStringUsingTripleDes("PayablesMgmtInvoicesDataView",userSession.getSecretKey());  //Rel9.2 IR T36000032596 
	    	%>
	    			<%-- To Intialize Datagrid for INVOICES --%>
	    			initializeDataGrid("invoicesGrid","<%=dataViewName%>");
	    	<%} %>
	    	<% if (TradePortalConstants.NAV__PAY_INQUIRIES.equals(current2ndNav)){
	    	      if(TradePortalConstants.NAV__INSTR_INQUIRIES.equals(historySearchNav)){
	    	      	dataViewName = EncryptDecrypt.encryptStringUsingTripleDes("PayablesMgmtInstrumentDataView",userSession.getSecretKey());  //Rel9.2 IR T36000032596
	    	      %>
	    	       <%-- To Intialize LazyTreeDatagrid for INQUIRIES --%>
	    	       initializeLazyDataGrid("payInstrumentGrid","<%=dataViewName%>");
            <%    }else if(TradePortalConstants.NAV__PYMTS_INQUIRIES.equals(historySearchNav)){
            		dataViewName = EncryptDecrypt.encryptStringUsingTripleDes("PayablesMgmtPaymentsDataView",userSession.getSecretKey());  //Rel9.2 IR T36000032596
            	   %>
            	   initializePymtsDataGrid("payPaymentsGrid","<%=dataViewName%>");
            <%     } %>
           <%  }
	    	
	    	if (TradePortalConstants.NAV__PAY_CREDIT_NOTES.equals(current2ndNav)){
	    		dataViewName = EncryptDecrypt.encryptStringUsingTripleDes("PayablesMgmtCreditNoteDataView",userSession.getSecretKey());   
	    	%>
	    			<%-- To Intialize Datagrid for INVOICES --%>
	    			initializeCreditNotesDataGrid("creditNotesGrid","<%=dataViewName%>");
	    	<%}
            
            if (TradePortalConstants.NAV__PAY_INQUIRIES.equals(current2ndNav)){
            %>
                    var Active = registry.byId("Active");
                    if ( Active ) {
                        Active.on("change", function() {
                        	<% if(TradePortalConstants.NAV__INSTR_INQUIRIES.equals(historySearchNav)){%>
                               searchInstrument();
                            <%}else if(TradePortalConstants.NAV__PYMTS_INQUIRIES.equals(historySearchNav)){%>
                               searchPayments();
                            <%}%>
                      });
                    }
                    
                    var InActive = registry.byId("InActive");
                    if ( InActive ) {
                        InActive.on("change", function() {
                        	<% if(TradePortalConstants.NAV__INSTR_INQUIRIES.equals(historySearchNav)){%>
                            searchInstrument();
                         <%}else if(TradePortalConstants.NAV__PYMTS_INQUIRIES.equals(historySearchNav)){%>
                            searchPayments();
                         <%}%>
                      });
                    }
             <%
            }
          %>
          
        <%-- SSikhakolli - Rel-9.2 CR-914A 02/17/2015 - Addinng below code to protect child rows from mouse selection. --%>
          var myGrid = registry.byId("creditNotesGrid");
          myGrid.onCanSelect=function(idx){
				var selectedItem = this.getItem(idx).i;
				var isChild = (selectedItem.CHILDREN != "true");
			   	return (!isChild);
			};
			
			
			<%-- MEer Rel 9.2 T36000035885 - Change font style to Italic for Child nodes in the CreditNotes Grid --%>
			dojo.connect(myGrid, 'onStyleRow', this, function (row) {
				  var trNode = row.node.childNodes[0].childNodes[0].childNodes[0];
				  if("dojoxGridNoChildren"==trNode.className){
					  dojo.addClass(trNode, "treeGridChildren");
				  }			 
			});
			 
			
	    });
	  });

	  
	  function applyPaymentDate(){
		  var rowKeys = getSelectedGridRowKeys("invoicesGrid");
		  if(rowKeys == ""){
				alert('<%=StringFunction.asciiToUnicode(resMgr.getText(
						"common.selectMoreItems",
						TradePortalConstants.TEXT_BUNDLE))%>');
				return;
		  }
		require(["t360/dialog"], function(dialog) {
 	      dialog.open('InvoiceGroupListDialogID', '<%=resMgr.getText("UploadInvoiceAction.UploadInvoiceApplyPaymentDateText", TradePortalConstants.TEXT_BUNDLE)%>',
	                      'AssignPaymentDate.jsp',
	                      ['selectedCount'], [getSelectedGridRowKeys("invoicesGrid").length], 
	                      'select', this.applyPaymentDateValue);
	  	}); 
	  }
	  
	  function applyPaymentDateValue(payDate) {
			
			 var theForm = document.getElementsByName(formName)[0];
			  theForm.paymentDate.value=payDate;
			  var rowKeys = getSelectedGridRowKeys("invoicesGrid");
			  submitFormWithParms(formName, '<%=TradePortalConstants.BUTTON_INV_APPLY_PAYMENT_DATE%>', "checkbox", rowKeys);
			
	  }
	  
	  function modifySupplierDate(){
		  var rowKeys = getSelectedGridRowKeys("invoicesGrid");
		  if(rowKeys == ""){
				alert('<%=StringFunction.asciiToUnicode(resMgr.getText(
						"common.selectMoreItems",
						TradePortalConstants.TEXT_BUNDLE))%>');
				return;
		  }
			require(["t360/dialog"], function(dialog) {
	 	      dialog.open('InvoiceGroupListDialogID', '<%=resMgr.getText("UploadInvoices.ModifySupplierDate", TradePortalConstants.TEXT_BUNDLE)%>',
		                      'ModifySupplierDate.jsp',
		                      ['selectedCount'], [getSelectedGridRowKeys("invoicesGrid").length], 
		                      'select', this.modifySupplierDateValue);
		  	}); 
		}
		function modifySupplierDateValue(payDate) {
			  	var theForm = document.getElementsByName(formName)[0];
				 theForm.supplierDate.value=payDate;
				  var rowKeys = getSelectedGridRowKeys("invoicesGrid");
				  submitFormWithParms(formName, '<%=TradePortalConstants.PAY_INV_BUTTON_MODIFY_SUPP_DATE%>', "checkbox", rowKeys);
						
		  }
		  
		  function applyEarlyPayAmt(){
			  var rowKeys = getSelectedGridRowKeys("invoicesGrid");
			  if(rowKeys == ""){
					alert('<%=StringFunction.asciiToUnicode(resMgr.getText(
							"common.selectMoreItems",
							TradePortalConstants.TEXT_BUNDLE))%>');
					return;
			  }
				require(["t360/dialog"], function(dialog) {
		 	      dialog.open('InvoiceGroupListDialogID', '<%=resMgr.getText("UploadInvoices.ApplyEarlyPayAmt", TradePortalConstants.TEXT_BUNDLE)%>',
			                      'ApplyEarlyPaymentAmt.jsp',
			                      ['selectedCount'], [getSelectedGridRowKeys("invoicesGrid").length], 
			                      'select', this.applyEarlyPayAmtValue);
			  	}); 
			}
		function applyEarlyPayAmtValue(payAmt) {
				var rowKeys = getSelectedGridRowKeys("invoicesGrid");
					 var theForm = document.getElementsByName(formName)[0];
					 theForm.paymentAmount.value=payAmt;
					 submitFormWithParms(formName,'<%=TradePortalConstants.PAY_INV_BUTTON_APPLY_PAY_AMT%>', "checkbox",rowKeys);
					
		}

	  function submitButtonAction(buttonName){
		  var rowKeys = getSelectedGridRowKeys("invoicesGrid");
		  if(rowKeys == ""){
				alert('<%=StringFunction.asciiToUnicode(resMgr.getText(
						"common.selectMoreItems",
						TradePortalConstants.TEXT_BUNDLE))%>');
				return;
		  }
		  submitFormWithParms(formName, buttonName, "checkbox", rowKeys);
	  }
	  
	 function openURL(URL){
		 if (isActive =='Y') {
			 if (URL != '' && URL.indexOf("javascript:") == -1) {
		        var cTime = (new Date()).getTime();
		        URL = URL + "&cTime=" + cTime;
		        URL = URL + "&prevPage=" + context;
			 }
		 }

		  document.location.href  = URL;
		  return false;
	 }

	  function authoriseInvoices(){
		  var rowKeys = getSelectedGridRowKeys("invoicesGrid");
			 if(rowKeys == ""){
					alert('<%=StringFunction.asciiToUnicode(resMgr.getText(
						"common.selectMoreItems",
						TradePortalConstants.TEXT_BUNDLE))%>');
					return;
			}
		  <%if (authoriseInvRequireAuth && StringFunction.isNotBlank(certAuthURL) &&  StringFunction.isNotBlank(authoriseInvLink) ) {%>
		  		openURL("<%=authoriseInvLink%>");
		  <%} else {%>		
		 	 var buttonName = '<%=TradePortalConstants.BUTTON_AUTHORIZE%>';
			submitFormWithParms(formName, buttonName, "checkbox", rowKeys);
		    <%}%>
	  }
	  
	  function OfflineAuthInvoices(){
		  var rowKeys = getSelectedGridRowKeys("invoicesGrid");
			 if(rowKeys == ""){
					alert('<%=StringFunction.asciiToUnicode(resMgr.getText(
						"common.selectMoreItems",
						TradePortalConstants.TEXT_BUNDLE))%>');
					return;
			}
		  <%if (StringFunction.isNotBlank(certAuthURL) && StringFunction.isNotBlank(authoriseInvLink)) {%>
		  		openURL("<%=authoriseInvLink%>");
		
		    <%}%>
	  }
	  
	  function payablesPanelStatusFormatter(columnValues){
    	  var gridLink = "";
    	 	if(columnValues[2] == '<%=TradePortalConstants.UPLOAD_INV_STATUS_PAR_AUTH%>' ||
    	  		columnValues[2] == '<%=TradePortalConstants.UPLOAD_INV_STATUS_AUTH_FAILED%>'){
    			 var title = "<%=resMgr.getText("Home.PanelAuthTransactions.ViewAuthorisationsDialog.Title", TradePortalConstants.TEXT_BUNDLE)%>"+columnValues[1];
    			 gridLink =  "<a href='javascript:opendialogViewPanelAuths(\""+title+"\",\"invoicesGrid\",\"PAY_INV\");'>"+columnValues[0]+"</a>";
    	  		 return gridLink;
    		}else{
    			return columnValues[0];
    		}
    	}

  require(["dojo/query", "dojo/on", "t360/datagrid", "t360/popup", "dijit/registry", "dojo/domReady!"],
      function(query, on, t360grid, t360popup){

<%
  
  if (TradePortalConstants.NAV__PAY_INVOICES.equals(current2ndNav)){
%>
 <%-- KMehta - 13 Oct 2014 - Rel9.1 IR-T36000033383 - Added domClass  --%>
	require(["dojo/query", "dojo/on", "dojo/dom-class", "t360/datagrid", "t360/popup", "dijit/registry", "dojo/ready", "dojo/domReady!"],
			 function(query, on, domClass, t360grid, t360popup, registry,ready) {
		 ready(function() {
			<%-- IR T36000030360: Hide page content, untill widgets get parsed. --%>
				domClass.add("InvoiceMgtDivID", "gridInvoiceFooterDivVisible");
				domClass.remove("InvoiceMgtDivID", "gridInvoiceFooterDivHidden");

			 if(registry.byId("invoicesGrid")){
				  var invGrid = registry.byId("invoicesGrid");			   
				  on(invGrid,"selectionChanged", function() {
					  if (registry.byId('UploadInvoices_Authorize')){
				          t360grid.enableFooterItemOnSelectionGreaterThan(invGrid,"UploadInvoices_Authorize",0);
					  }
					  if (registry.byId('UploadInvoices_Amounts')){
						  t360grid.enableFooterItemOnSelectionGreaterThan(invGrid,"UploadInvoices_Amounts",0);
					  }
					  if (registry.byId('UploadInvoices_Dates')){
						  t360grid.enableFooterItemOnSelectionGreaterThan(invGrid,"UploadInvoices_Dates",0);
					  }
				  
				 });
		    } 
		 });
	 });

   require(["dojo/query", "dojo/on", "t360/datagrid", "t360/popup", "dojo/domReady!"],
	      function(query, on, t360grid, t360popup){
	    query('#invoicesRefresh').on("click", function() {
	    	searchInvoices();
	    });
    query('#invoicesGridEdit').on("click", function() {
      var columns = t360grid.getColumnsForCustomization('invoicesGrid');
      var parmNames = [ "gridId", "gridName", "columns" ];
      var parmVals = [ "invoicesGrid", "PayablesMgmtInvoicesDataGrid", columns ];
      t360popup.open(
        'invoicesGridEdit', 'gridCustomizationPopup.jsp',
        parmNames, parmVals,
        null, null);  <%-- callbacks --%>
    });
   });
<%
  }
if (TradePortalConstants.NAV__PAY_INQUIRIES.equals(current2ndNav)){
	%>
	<%-- IR T36000030360: Hide page content, untill widgets get parsed. --%>
	require([ 'dojo/dom-class', "dojo/ready" ], function ( domClass, ready) {
		ready(function() {
			domClass.add("InvoiceMgtDivID", "gridInvoiceFooterDivVisible");
			domClass.remove("InvoiceMgtDivID", "gridInvoiceFooterDivHidden");
			});
		});
	<%
	 if(TradePortalConstants.NAV__INSTR_INQUIRIES.equals(historySearchNav)){
	%>
	 query('#inquiresRefresh').on("click", function() {
		    searchInstrument();
		});
	<% }else if(TradePortalConstants.NAV__PYMTS_INQUIRIES.equals(historySearchNav)){%>
	   query('#inquiresRefresh').on("click", function() {
        searchPayments();
    });
	<%}
	 }
if (TradePortalConstants.NAV__PAY_CREDIT_NOTES.equals(current2ndNav)){
	%>
	<%-- IR T36000030360: Hide page content, untill widgets get parsed. --%>
	require([ 'dojo/dom-class', "dojo/ready" ], function ( domClass, ready) {
		ready(function() {
			domClass.add("InvoiceMgtDivID", "gridInvoiceFooterDivVisible");
			domClass.remove("InvoiceMgtDivID", "gridInvoiceFooterDivHidden");
			});
		});
	 query('#creditNotesRefresh').on("click", function() {
		    searchCreditNotes();
		});
	 query('#creditNotesGridEdit').on("click", function() {
	      var columns = t360grid.getColumnsForCustomization('creditNotesGrid');
	      var parmNames = [ "gridId", "gridName", "columns" ];
	      var parmVals = [ "creditNotesGrid", "PayablesMgmtCreditNoteDataGrid", columns ];
	      t360popup.open(
	        'creditNotesGridEdit', 'gridCustomizationPopup.jsp',
	        parmNames, parmVals,
	        null, null);  <%-- callbacks --%>
	    });
	<%
	 }
	%>

  });

 
  function filterPayTranOnEnter(evt, fieldID, buttonId){
	    require(["dojo/on","dijit/registry"],function(on, registry) {
	          on(registry.byId(fieldID), "keypress", function(evt) {
	    if(evt && evt.keyCode==13){
	        if(buttonId == 'Instr'){
	              searchInstrument();
	        }else if(buttonId == 'Pay'){
	              searchPayments();
	        }else if(buttonId == 'Inv'){
	        	  searchInvoices();
            }else if(buttonId == 'Credit'){
	        	  searchCreditNotes();
            }
	}
	          });
	    });
	}

</script>

<%
 
  if (TradePortalConstants.NAV__PAY_INVOICES.equals(current2ndNav)){
%>
<%--cquinton 10/8/2012 include gridShowCounts --%>
<jsp:include page="/common/gridShowCountFooter.jsp">
  <jsp:param name="gridId" value="invoicesGrid" />
</jsp:include>

<%
  } if (TradePortalConstants.NAV__PAY_INQUIRIES.equals(current2ndNav)){
	  %>
	  <%@ include file="/payables/fragments/PayablesManagement_InstrumentsFooter.frag" %>
	  <% if(TradePortalConstants.NAV__INSTR_INQUIRIES.equals(historySearchNav)){%>
		  <jsp:include page="/common/gridShowCountFooter.jsp">
		    <jsp:param name="gridId" value="payInstrumentGrid" />
		  </jsp:include>
	  <% }else if(TradePortalConstants.NAV__PYMTS_INQUIRIES.equals(historySearchNav)){%>
	       <jsp:include page="/common/gridShowCountFooter.jsp">
            <jsp:param name="gridId" value="payPaymentsGrid" />
          </jsp:include>
	  <%
	    }
  }
  if (TradePortalConstants.NAV__PAY_CREDIT_NOTES.equals(current2ndNav)){
	  %>
	  <%@ include file="/payables/fragments/PayablesManagement_CreditNotesFooter.frag" %>
	  <%--cquinton 10/8/2012 include gridShowCounts --%>
	  <jsp:include page="/common/gridShowCountFooter.jsp">
	    <jsp:param name="gridId" value="creditNotesGrid" />
	  </jsp:include>

	  <%
	    }
	  %>


</body>
</html>

<%
   formMgr.storeInDocCache("default.doc", new DocumentHandler());

   session.setAttribute("payTransactions2ndNav", current2ndNav);
   session.setAttribute("historySearchNav", historySearchNav);
   //used in InvoiceDetail.jsp to differentiate rec and pay invoice navigation
   session.setAttribute("fromPayables", "Y");
%>
