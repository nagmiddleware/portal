<%--
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
--%>
<%--
**********************************************************************************
                        Payables Credit Notes Tab

  Description:
    Contains HTML to create the History tab for the Transactions Home page

  This is not a standalone JSP.  It MUST be included using the following tag:

*******************************************************************************
--%>

<%
      secureParms.put("UserOid",        userSession.getUserOid());
      secureParms.put("OrgOid",        userSession.getOwnerOrgOid());
      if (userSession.getSavedUserSession() == null) {
	        secureParms.put("AuthorizeAtParentViewOrSubs", TradePortalConstants.INDICATOR_YES);
	  }else{
		  secureParms.put("AuthorizeAtParentViewOrSubs", TradePortalConstants.INDICATOR_NO);
	  }

      helpSensitiveLink = OnlineHelp.createContextSensitiveLink("customer/invoices.htm",  resMgr, userSession);

      String newSearchTrans = request.getParameter("NewSearch");
      loginLocale = userSession.getUserLocale();

   String optionsAmountType = null;
   String optionsDateType = null;
   String invoiceID = null;
 
%>

<div class="gridSearch">
  <div class="searchHeader">
    <span class="searchHeaderCriteria">
      <%= resMgr.getText("TransactionsMenu.Payables.CreditNotes",TradePortalConstants.TEXT_BUNDLE) %>

    </span>
    <span class="searchHeaderActions">
      <jsp:include page="/common/gridShowCount.jsp">
        <jsp:param name="gridId" value="creditNotesGrid" />
      </jsp:include>
 	  <span id="creditNotesRefresh" class="searchHeaderRefresh"></span>
 	  <%=widgetFactory.createHoverHelp("creditNotesRefresh", "RefreshImageLinkHoverText") %>
      <span id="creditNotesGridEdit" class="searchHeaderEdit"></span>
      <%=widgetFactory.createHoverHelp("creditNotesGridEdit","CustomizeListImageLinkHoverText") %>  
     </span>
    <div style="clear:both;"></div>
  </div>

<%
  gridHtml = dgFactory.createDataGrid("creditNotesGrid","PayablesMgmtCreditNoteDataGrid",null);
  //SSikhakolli - Rel-9.2 - CR-914A - Moving Grid layout to PayablesMgmtCreditNoteDataGrid.xml for Grid Customization.
  gridLayout = dgFactory.createGridLayout("creditNotesGrid","PayablesMgmtCreditNoteDataGrid");
   initSearchParms="userOrgOid="+userOrgOid;
%>

<div class="searchDivider"></div>

<%@ include file="/payables/fragments/PayCreditNotesSearchFilter.frag" %>
</div>
<input type=hidden name=SearchType value="Y">

<%=gridHtml%>

