<script>
var gridLayout;
function dialogQuickView(rowKey){
    var dialogDiv = document.createElement("div");
    dialogDiv.setAttribute('id',"quickviewdialogdivid");
    document.forms[0].appendChild(dialogDiv);
  
    var title="";
  
    require(["t360/dialog"], function(dialog ) {

      dialog.open('quickviewdialogdivid',title ,
                  'TransactionQuickViewDialog.jsp',
                  ['rowKey','DailogId'],[rowKey,'quickviewdialogdivid'], //parameters
                  'select', quickViewCallBack);
    });
  }

  var quickViewFormatter=function(columnValues, idx, level) {
    var gridLink="";
  
    if(level == 0){
  
      if ( columnValues.length >= 2 ) {
        gridLink = "<a href='" + columnValues[1] + "'>" + columnValues[0] + "</a>";
      }
      else if ( columnValues.length == 1 ) {
        //no url parameters, just return the display
        gridLink = columnValues[0];
      }
      return gridLink;
  
    }else if(level ==1){
      if(columnValues[3]=='PROCESSED_BY_BANK'){
        if(columnValues[2]){
          return "<a href='javascript:dialogQuickView("+columnValues[2]+");'>Quick View</a> "+columnValues[0]+ " ";
        }else{
          return columnValues[0];   
        }
  
      }else{
        return columnValues[0];
      }
  
    }else{
    	return "<a href='javascript:dialogQuickView("+columnValues[2]+");'>Quick View</a> "+columnValues[0]+ " ";
    }
  }

  var seqFormatter=function(columnValues,idx,level){
	  var gridlink = "";
	  var gridText = "";
	  console.log(columnValues[2]);
	  if(columnValues[1] != ""){
		  gridText = columnValues[0]+" - "+columnValues[1];
	  }else {
		  gridText = columnValues[0];
	  }
	  gridLink = "<a href='" + columnValues[2] + "'>" + gridText + "</a>";
	  return gridLink;
  }
  var formatGridLinkChild=function(columnValues, idx, level){
    var gridLink="";
  
    if(level == 0){
      return columnValues[0];
  
    }else if(level ==1){
      if ( columnValues.length >= 2 ) {
        gridLink = "<a href='" + columnValues[1] + "'>" + columnValues[0] + "</a>";
      }
      else if ( columnValues.length == 1 ) {
        //no url parameters, just return the display
        gridLink = columnValues[0];
      }
      return gridLink;
    }
  }

  //function to set the title after ajax call of dialog value[2]- dialogId, value[0]-complete instrument id, value[1]-transaction type
  function quickViewCallBack(key,value){
    dijit.byId(value[2]).set('title','Transaction Quick View - '+value[0]+' - '+value[1]);
  }
<%
if (TradePortalConstants.NAV__PAY_INQUIRIES.equals(current2ndNav)) {
	if(TradePortalConstants.NAV__INSTR_INQUIRIES.equals(historySearchNav)){
%>

	
	/*KMehta - 24 Sep 2014 - Rel9.1 IR-T36000032371 - Change  - Begin*/
    var historyGridLayout = [
	  {name:"<%=resMgr.getTextEscapedJS("InstrumentHistory.INSTRUMENT",TradePortalConstants.TEXT_BUNDLE)%>", field:"INSTRUMENT", fields:["INSTRUMENT","INSTRUMENT_linkUrl","transaction_oid","STATUSFORQVIEW","Type"], formatter:quickViewFormatter,width:"180px"} ,
	  {name:"<%=resMgr.getTextEscapedJS("InstrumentHistory.Type",TradePortalConstants.TEXT_BUNDLE)%>", field:"Type", fields:["Type","Type_linkUrl"], formatter:formatGridLinkChild, width:"180px"} ,
	  {name:"<%=resMgr.getTextEscapedJS("InstrumentHistory.CCY",TradePortalConstants.TEXT_BUNDLE)%>", field:"CCY", width:"80px"} ,
	  {name:"<%=resMgr.getTextEscapedJS("InstrumentHistory.AMOUNT",TradePortalConstants.TEXT_BUNDLE)%>", field:"AMOUNT", width:"100px", cellClasses:"gridColumnRight"} ,
	  {name:"<%=resMgr.getTextEscapedJS("InstrumentHistory.STATUS",TradePortalConstants.TEXT_BUNDLE)%>", field:"STATUS", width:"100px"} ,
	  {name:"<%=resMgr.getTextEscapedJS("InstrumentHistory.REFERENCE",TradePortalConstants.TEXT_BUNDLE)%>", field:"REFERENCE", fields:["REFERENCE","REFERENCE_linkUrl"], formatter:formatGridLink, width:"150px"} ];
    /*KMehta - 24 Sep 2014 - Rel9.1 IR-T36000032371 - Change  - End*/
	function initializeLazyDataGrid(DataGridId,DataView){
	  //cquinton 3/11/2013
	  require(["dojo/dom", "dijit/registry", "t360/datagrid", "dojo/domReady!"],
	  function(dom, registry, t360grid ) {
	
		var Active = savedSearchQueryData["Active"]; 
	    var InActive = savedSearchQueryData["InActive"]; 
	    var savedSort = savedSearchQueryData["sort"];  
	    var amountFrom = savedSearchQueryData["amountFrom"];
	    var amountTo = savedSearchQueryData["amountTo"];
	    var currency = savedSearchQueryData["currency"];
	    var instrumentID = savedSearchQueryData["instrumentID"];
	    
		  if("" == savedSort || null == savedSort || "undefined" == savedSort){
		      savedSort = '0';
		   }
		  if("" == Active || null == Active || "undefined" == Active){
		      if(dom.byId("Active").checked)
		          Active="Y";
		  }
		       else{
		  if(Active=="Y")
		      registry.byId("Active").set('checked',true);
		        else
		          registry.byId("Active").set('checked',false);
		   
		   }
		  if("" == InActive || null == InActive || "undefined" == InActive){
		      if(dom.byId("InActive").checked)
		          InActive="Y";
		  }
		   else{
		       if(InActive=="Y")
		           registry.byId("InActive").set('checked',true);
		        else
		          registry.byId("InActive").set('checked',false);
		      
		   }
	  
	      if("" == currency || null == currency || "undefined" == currency)
	          currency=dom.byId("InstrCurrency").value;
	           else
	           {
	               registry.byId("InstrCurrency").set('value',currency);
	           
	          }
	      
	      if("" == instrumentID || null == instrumentID || "undefined" == instrumentID)
	          instrumentID=dom.byId("InstrumentID").value.toUpperCase();
	           else
	              dom.byId("InstrumentID").value = instrumentID;
	    
	    <%--set the initial search parms--%>
	    initSearchParms = '<%=initSearchParms%>'+'&Active='+Active+'&InActive='+InActive;
	    initSearchParms = initSearchParms+"&currency="+currency+"&instrumentID="+instrumentID+"&userOrgOid="+<%=userOrgOid%>;
	    <%--create the grid, attaching it to the div id specified created in dataGridDataGridFactory.createDataGrid.jsp()--%>
	    console.log("initSearchParms-"+initSearchParms);
	    t360grid.createLazyTreeDataGrid(DataGridId,DataView,historyGridLayout,initSearchParms,savedSort);
	  });
	}
<%
	}else if (TradePortalConstants.NAV__PYMTS_INQUIRIES.equals(historySearchNav)){
%>
	function initializePymtsDataGrid(DataGridId,DataView){
	
	    gridLayout = <%=gridLayout%>;
	        <%--set the initial search parms--%>
	    initSearchParms = "<%=initSearchParms%>";
	    var savedSort = savedSearchQueryData["sort"];  
	     require(["t360/datagrid", "dojo/dom","dijit/registry"],
	                function( t360grid,dom,registry ) {
	        if("" == savedSort || null == savedSort || "undefined" == savedSort){
	            savedSort = 'InvoiceID';
	         }
	        
	        var Active = savedSearchQueryData["Active"]; 
	        var InActive = savedSearchQueryData["InActive"]; 
	        var currency = savedSearchQueryData["currency"]; 
	        var amountFrom = savedSearchQueryData["amountFrom"]; 
	        var amountTo = savedSearchQueryData["amountTo"]; 
	        var fromDate = savedSearchQueryData["fromDate"]; 
	        var toDate = savedSearchQueryData["toDate"]; 
	        
	        if("" == Active || null == Active || "undefined" == Active){
	              if(dom.byId("Active").checked)
	                  Active="Y";
	          }
	               else{
	          if(Active=="Y")
	              registry.byId("Active").set('checked',true);
	                else
	                  registry.byId("Active").set('checked',false);
	           
	           }
	          if("" == InActive || null == InActive || "undefined" == InActive){
	              if(dom.byId("InActive").checked)
	                  InActive="Y";
	          }
	           else{
	               if(InActive=="Y")
	                   registry.byId("InActive").set('checked',true);
	                else
	                  registry.byId("InActive").set('checked',false);
	              
	           }
	          if("" == currency || null == currency || "undefined" == currency)
	              currency=dom.byId("PayCurrency").value;
	               else
	               {
	                   registry.byId("PayCurrency").set('value',currency);
	               
	              }
	          if("" == amountFrom || null == amountFrom || "undefined" == amountFrom)
	              amountFrom=dom.byId("PayAmountFrom").value;
	               else
	                  dom.byId("PayAmountFrom").value = amountFrom;
	        
	          if("" == amountTo || null == amountTo || "undefined" == amountTo)
	                  amountTo=dom.byId("PayAmountTo").value;
	               else
	                  dom.byId("PayAmountTo").value = amountTo;
	          
	          if("" == fromDate || null == fromDate || "undefined" == fromDate)
	                fromDate=dom.byId("PaymentDateFrom").value;
	             else{
	                registry.byId("PaymentDateFrom").set('displayedValue',fromDate);
	             }
	            
	            if("" == toDate || null == toDate || "undefined" == toDate)
	                toDate=dom.byId("PaymentDateTo").value;
	             else
	                registry.byId("PaymentDateTo").set('displayedValue',toDate);
	        
	        <%--set the initial search parms--%>
	        var tempDatePattern = encodeURI('<%=datePattern%>');
            initSearchParms = initSearchParms+"&dPattern="+tempDatePattern+"&Active="+Active+"&InActive="+InActive;
	        initSearchParms = initSearchParms+"&amountFrom="+amountFrom+"&amountTo="+amountTo+
	        "&currency="+currency+"&payDateFrom="+fromDate+"&payDateTo="+toDate+"&userOrgOid="+<%=userOrgOid%>;
	        loginLocale="<%=loginLocale%>";
	        console.log("initSearchParms: " + initSearchParms);
	    <%--create the grid, attaching it to the div id specified created in dataGridDataGridFactory.createDataGrid.jsp()--%>
	    
	        createDataGrid(DataGridId, DataView,gridLayout, initSearchParms,savedSort);
	     });
	}
<%	
	}
}
%>


<%
if (TradePortalConstants.NAV__PAY_INQUIRIES.equals(current2ndNav)){
%>
    
function searchInstrument() {
  //cquinton 3/11/2013
  require(["dojo/dom", "dijit/registry", "t360/datagrid", "dojo/domReady!"],
  function(dom, registry, t360grid ) {

    var Active="";
    if(dijit.byId("Active").checked)
      Active="Y";
    else
      Active="N";

    var InActive="";
    if(dijit.byId("InActive").checked)
      InActive="Y";
    else
      InActive="N";

    Currency = dijit.byId("InstrCurrency").attr('value');
              
    var InstrumentID=dom.byId("InstrumentID").value;
    var searchParms = "Active="+Active+"&InActive="+InActive+"&Currency="+Currency+"&InstrumentID="+InstrumentID+"&userOrgOid="+<%=userOrgOid%>;
    var viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("PayablesMgmtInstrumentDataView",userSession.getSecretKey())%>";  <%--Rel9.2 IR T36000032596 --%>
    t360grid.searchLazyTreeDataGrid("payInstrumentGrid", viewName,
      historyGridLayout, searchParms);
  });
}

function searchPayments() {
	  //cquinton 3/11/2013
	  require(["dojo/dom", "dijit/registry", "t360/datagrid", "dojo/domReady!"],
	  function(dom, registry, t360grid ) {

	    var Active="";
	    if(dijit.byId("Active").checked)
	      Active="Y";
	    else
	      Active="N";

	    var InActive="";
	    if(dijit.byId("InActive").checked)
	      InActive="Y";
	    else
	      InActive="N";

	    var PayAmountFrom=dom.byId("PayAmountFrom");
	    var PayAmountTo=dom.byId("PayAmountTo");
	    Currency = dijit.byId("PayCurrency").attr('value');
	              
	    if(PayAmountTo){
	    	PayAmountTo=dom.byId("PayAmountTo").value;
	    }
	    if(PayAmountFrom){
	    	PayAmountFrom=dom.byId("PayAmountFrom").value;
	    }
	    if (isNaN(PayAmountTo))
	    	PayAmountTo='';
	    if (isNaN(PayAmountFrom))
	    	PayAmountFrom='';
	    
	    var PayDateFrom = dom.byId("PaymentDateFrom");
	    var PayDateTo = dom.byId("PaymentDateTo");
	    
	    if(PayDateFrom){
	    	PayDateFrom=dom.byId("PaymentDateFrom").value;
        }
        
        if(PayDateTo){
        	PayDateTo=dom.byId("PaymentDateTo").value;
        }
        
        var tempDatePattern = encodeURI('<%=datePattern%>');
    	    
	    var searchParms = "Active="+Active+"&InActive="+InActive+"&amountFrom="+PayAmountFrom+"&amountTo="+PayAmountTo+"&currency="+Currency+"&payDateFrom="+PayDateFrom+"&payDateTo="+PayDateTo+"&dPattern="+tempDatePattern+"&userOrgOid="+<%=userOrgOid%>;
	    searchDataGrid("payPaymentsGrid", "PayablesMgmtPaymentsDataView",searchParms);
	  });
	}
<%
}
%>
</script>