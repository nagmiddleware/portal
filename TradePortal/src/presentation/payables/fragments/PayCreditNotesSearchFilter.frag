
  <input type="hidden" name="NewSearch" value="Y">
<%
  String options = "";
  
  String currency = request.getParameter("Currency");
  String amountType = request.getParameter("AmountType");	
  
  StringBuffer utilisedStatusTypeBuffer = new StringBuffer(Dropdown.createSortedRefDataOptions("CREDIT_NOTE_UTILISED_STATUS",
		                                             "", loginLocale, null));

  StringBuffer appliedStatusTypeBuffer = new StringBuffer(Dropdown.createSortedRefDataOptions("CREDIT_NOTE_APPLIED_STATUS",
		                                             "", loginLocale, null));

  utilisedStatusType = utilisedStatusTypeBuffer.toString();
  appliedStatusType = appliedStatusTypeBuffer.toString();
  
  StringBuilder acs = new StringBuilder();
  String[] arr = null;

  if (utilisedStatusType.length() > 0) {

    	  int count = 0;
          arr = utilisedStatusType.split("</option>");
          acs = new StringBuilder("data: [{name:\"\",id:\"\"},");
          for (count = 0; count < arr.length ; count++) {
                String str = arr[count];
                int start = str.indexOf("=");
                int end = str.indexOf(">");
                String key = str.substring(start + 2, end - 1);
                String val = str.substring(end + 1, str.length());

                acs.append("{name:\"" + val + "\", id:\"" + key + "\"}");
                acs.append(",");
          }

          acs.deleteCharAt(acs.length() - 1);
          acs.append("]");
          utilisedStatusType = acs.toString();
    }

    if (appliedStatusType.length() > 0) {
          int count = 0;
          arr = appliedStatusType.split("</option>");
          acs = new StringBuilder("data: [{name:\"\",id:\"\"},");
          for (count = 0; count < arr.length; count++) {
                String str = arr[count];
                int start = str.indexOf("=");
                int end = str.indexOf(">");
                String key = str.substring(start + 2, end - 1);
                String val = str.substring(end + 1, str.length());

                acs.append("{name:\"" + val + "\", id:\"" + key + "\"}");
                acs.append(",");
          }

          acs.deleteCharAt(acs.length() - 1);
          acs.append("]");
          appliedStatusType = acs.toString();
    }	
    
      StringBuffer statusTypeOptions = new StringBuffer();
	  statusTypeOptions.append("<option value='").
        append(TradePortalConstants.CREDIT_NOTE_APPLIED_STATUS_TYPE).
        append("'>").
        append(resMgr.getText("CreditNoteDetails.CreditNoteAppliedStatus", TradePortalConstants.TEXT_BUNDLE)).
        append("</option>");
  	statusTypeOptions.append("<option value='").
        append(TradePortalConstants.CREDIT_NOTE_UTILIZED_STATUS_TYPE).
        append("'>").
        append(resMgr.getText("CreditNoteDetails.CreditNoteUtilizedStatus", TradePortalConstants.TEXT_BUNDLE)).
        append("</option>");
%>

<div class="searchDetail">
	<span class="searchCriteria">
		<%=widgetFactory.createSearchTextField("CreditNoteID", "CreditNoteSearch.CreditNoteId", "35","class='char10' onKeydown='filterPayTranOnEnter(window.event, \"CreditNoteID\", \"Credit\");'") %>
		<%=widgetFactory.createSearchTextField("TradingPartner", "CreditNoteSearch.TradingPartner", "20","class='char10' onKeydown='filterPayTranOnEnter(window.event, \"TradingPartner\", \"Credit\");'") %>
		<%options = Dropdown.createSortedRefDataOptions(TradePortalConstants.CURRENCY_CODE, currency, loginLocale);
  			out.println(widgetFactory.createSearchSelectField("CreditStatusType", "CreditNoteSearch.StatusType", " ", statusTypeOptions.toString(),"class='char15' onChange='setStatusOptions();'"));
  			%>
		<%=widgetFactory.createSearchSelectField("CreditStatus", "CreditNoteSearch.Status", " ", "","class='char15' onKeydown='filterPayTranOnEnter(window.event, \"CreditStatus\", \"Credit\");'")%>
	</span>
	
	<span class="searchActions">
		<button data-dojo-type="dijit.form.Button" type="button" id="searchCreditNotes"><%= resMgr.getText("common.searchButton",TradePortalConstants.TEXT_BUNDLE) %>
				<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">searchCreditNotes();</script>
		  	</button>
		  	<%=widgetFactory.createHoverHelp("searchCreditNotes","SearchHoverText") %>
	</span>
	<div style="clear:both"></div>
</div>

<div class="searchDetail">
	<span class="searchCriteria">
		<%options = Dropdown.createSortedRefDataOptions(TradePortalConstants.CURRENCY_CODE, currency, loginLocale);
  			out.println(widgetFactory.createSearchSelectField("CreditCurrency", "CreditNoteSearch.Currency", " ", options,"class='char30' onKeydown='filterPayTranOnEnter(window.event, \"CreditCurrency\", \"Credit\");'"));
  			%>

  		<%
  		Vector  codesToExclude   = new Vector();
  	    codesToExclude.add("INA");
  	    codesToExclude.add("FNA");
  	    codesToExclude.add("PYA");
  	    options = Dropdown.createSortedRefDataOptions(TradePortalConstants.AMOUNT_TYPE, amountType, loginLocale,codesToExclude);
  			out.println(widgetFactory.createSearchSelectField("CreditAmountType", "CreditNoteSearch.AmountType", " ", options,"class='char20' onKeydown='filterPayTranOnEnter(window.event, \"CreditAmountType\", \"Credit\");'"));
  			%>
		<%=widgetFactory.createSearchAmountField("CreditAmountFrom", "CreditNoteSearch.From","","class='char10' onKeydown='filterPayTranOnEnter(window.event, \"CreditAmountFrom\", \"Credit\");'", "") %>
		<%=widgetFactory.createSearchAmountField("CreditAmountTo", "CreditNoteSearch.To","","class='char10' onKeydown='filterPayTranOnEnter(window.event, \"CreditAmountTo\", \"Credit\");'", "") %>
	</span>
	
	
	<div style="clear:both"></div>
</div>

<div class="searchDetail">
	<span class="searchCriteria">
		<%=widgetFactory.createSearchTextField("EndToEndID", "CreditNoteSearch.EndToEndId", "35","class='char15' onKeydown='filterPayTranOnEnter(window.event, \"EndToEndID\", \"Credit\");'") %>
	</span>
	
	
	<div style="clear:both"></div>
</div>

