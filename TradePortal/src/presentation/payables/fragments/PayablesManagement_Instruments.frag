<%--
 *
 *     Copyright  � 2008                         
 *     CGI 
 *     All rights reserved
--%>
<%--
**********************************************************************************
                        Payables Instruments Tab

  Description:
    Contains HTML to create the Receivable Instruments for the Receivables Management Home page

  This is not a standalone JSP.  It MUST be included in a jsp page.
*******************************************************************************
--%> 
 
 <%
    String selectedStatus = "";
    
    String instrumentId =  request.getParameter("InstrumentId");
    String amountTo = request.getParameter("AmountTo");
    String amountFrom= request.getParameter("AmountFrom");
    String currency= request.getParameter("Currency");
    String options="";
   
    String linkHistoryParms = "";
    String linkInstrSelected = "";
    String linkPymtsSelected = "";
    if(TradePortalConstants.NAV__INSTR_INQUIRIES.equals(historySearchNav)){
    	linkInstrSelected = TradePortalConstants.INDICATOR_YES;
    }else if(TradePortalConstants.NAV__PYMTS_INQUIRIES.equals(historySearchNav)){
    	linkPymtsSelected = TradePortalConstants.INDICATOR_YES;
    }
%>	
	
<div class="gridSearch">
  <div class="searchHeader">
    <span class="searchHeaderCriteria">
<%
  StringBuffer instrCriteria = new StringBuffer();
  instrCriteria.append(widgetFactory.createInlineLabel("", "InstrumentHistory.Show"));
  instrCriteria.append(widgetFactory.createCheckboxField("Active", "InstSearch.Active", true, false, false, "", "", "none")); //not a form item
  instrCriteria.append(widgetFactory.createCheckboxField("InActive", "InstSearch.InActive", false, false, false, "", "", "none")); //not a form item
  widgetFactory.wrapSearchItem(instrCriteria, "");//no extra classes
%>
      <%=instrCriteria.toString()%> 
  
     </span>
     
    <%linkHistoryParms = "current2ndNav=" + TradePortalConstants.NAV__PAY_INQUIRIES+"&historySearchNav=" + TradePortalConstants.NAV__INSTR_INQUIRIES;
    %>
      <jsp:include page="/common/NavigationLink.jsp">
        <jsp:param name="linkTextKey"  value="PaySearch.Instrument" />
        <jsp:param name="linkSelected" value="<%= linkInstrSelected %>" />
        <jsp:param name="action"       value="goToPayableTransactionsHome" />
        <jsp:param name="linkParms"    value="<%= linkHistoryParms %>" />
        <jsp:param name="hoverText"    value="PayableTransaction.invoicesList" />
      </jsp:include>

  <%
  linkHistoryParms = "current2ndNav=" + TradePortalConstants.NAV__PAY_INQUIRIES+"&historySearchNav=" + TradePortalConstants.NAV__PYMTS_INQUIRIES;
  %>
  <jsp:include page="/common/NavigationLink.jsp">
    <jsp:param name="linkTextKey"  value="PaySearch.Payments" />
    <jsp:param name="linkSelected" value="<%= linkPymtsSelected %>" />
    <jsp:param name="action"       value="goToPayableTransactionsHome" />
    <jsp:param name="linkParms"    value="<%= linkHistoryParms %>" />
    <jsp:param name="hoverText"    value="PayableTransaction.invoicesList" />
  </jsp:include>    
 
    <span class="searchHeaderActions">
    
      <%if(TradePortalConstants.NAV__INSTR_INQUIRIES.equals(historySearchNav)) {%>
            <jsp:include page="/common/gridShowCount.jsp">
                <jsp:param name="gridId" value="payInstrumentGrid" />
             </jsp:include>
       <%} else if(TradePortalConstants.NAV__PYMTS_INQUIRIES.equals(historySearchNav)){ %>  
            <jsp:include page="/common/gridShowCount.jsp">
                <jsp:param name="gridId" value="payPaymentsGrid" />
             </jsp:include> 
       <%} %>   
		<span id="inquiresRefresh" class="searchHeaderRefresh"></span>
 	  <%=widgetFactory.createHoverHelp("inquiresRefresh", "RefreshImageLinkHoverText") %>
      </span>
    <div style="clear:both;"></div>
  </div>

  <div class="searchDivider"></div>
  <%if(TradePortalConstants.NAV__INSTR_INQUIRIES.equals(historySearchNav)) 
  { %>
    <%@ include file="/payables/fragments/PayInstrumentSearchFilter.frag" %>
  <%} else if(TradePortalConstants.NAV__PYMTS_INQUIRIES.equals(historySearchNav))
  { %>  
    <%@ include file="/payables/fragments/PayMgmtPaymentsSearchFilter.frag" %>
  <%} %>
</div>
  			

 