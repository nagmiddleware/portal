<%--
*******************************************************************************
                  Instrument Search Advanced Filter include file

  Description:
    This is a JSP meant to be included using the <%@ include file="x.jsp" %> 
  tag.  It creates the HTML for displaying the Advanced Filter fields for the 
  Instrument Search.  As it is included with <%@ include %> rather than with
  the <jsp:include> directive, it is not a standalone servlet.  
  
 
  NOTE: Since this JSP is copied into the including page at compile time of
  the including page, changes to this page do not automatically force the
  including pages to be recompiled.  If you make changes to this page, you must
  delete the including page's servlet in order to see the changes.


*******************************************************************************
--%>
<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
  <input type="hidden" name="NewSearch" value="Y">
  <input type="hidden" name="FinanceCheck" value="<%= TradePortalConstants.INDICATOR_NO %>">
<%
  String options = "";
  
  String currency = request.getParameter("Currency");
  String amountType = request.getParameter("AmountType");	
  String dateType = request.getParameter("DateType");	
%>

<div class="searchDetail">
	<span class="searchCriteria">
		<%=widgetFactory.createSearchTextField("InvoiceID", "InvoiceSearch.InvoiceID", "35","class='char15' onKeydown='filterPayTranOnEnter(window.event, \"InvoiceID\", \"Inv\");'") %>
		<%=widgetFactory.createSearchTextField("SellerIdentifier", "InvoiceSearch.SellerID", "20","class='char15' onKeydown='filterPayTranOnEnter(window.event, \"SellerIdentifier\", \"Inv\");'") %>
	</span>
	
	<span class="searchActions">
		<button data-dojo-type="dijit.form.Button" type="button" id="searchParty"><%= resMgr.getText("common.searchButton",TradePortalConstants.TEXT_BUNDLE) %>
				<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">searchInvoices();</script>
		  	</button>
		  	<%=widgetFactory.createHoverHelp("searchParty","SearchHoverText") %>
	</span>
	<div style="clear:both"></div>
</div>

<div class="searchDetail">
	<span class="searchCriteria">
		<%options = Dropdown.createSortedRefDataOptions(TradePortalConstants.CURRENCY_CODE, currency, loginLocale);
  			out.println(widgetFactory.createSearchSelectField("Currency", "InvoiceSearch.Currency", " ", options,"class='char30' onKeydown='filterPayTranOnEnter(window.event, \"Currency\", \"Inv\");'"));
  			%>

  		<%
  		Vector  codesToExclude   = new Vector();
  	    codesToExclude.add("CNA");
  	    codesToExclude.add("FNA");
  	    options = Dropdown.createSortedRefDataOptions(TradePortalConstants.AMOUNT_TYPE, amountType, loginLocale,codesToExclude);
  			out.println(widgetFactory.createSearchSelectField("AmountType", "InvoiceSearch.AmountType", " ", options,"class='char20' onKeydown='filterPayTranOnEnter(window.event, \"AmountType\", \"Inv\");'"));
  			%>
		<%=widgetFactory.createSearchAmountField("AmountFrom", "InvoiceSearch.From","","class='char10' onKeydown='filterPayTranOnEnter(window.event, \"AmountFrom\", \"Inv\");'", "") %>
		<%=widgetFactory.createSearchAmountField("AmountTo", "InvoiceSearch.To","","class='char10' onKeydown='filterPayTranOnEnter(window.event, \"AmountTo\", \"Inv\");'", "") %>
	</span>
	
	
	<div style="clear:both"></div>
</div>

<div class="searchDetail">
	<span class="searchCriteria">
		<%
  	    options = Dropdown.createSortedRefDataOptions(TradePortalConstants.DATE_TYPE, dateType, loginLocale);
  			out.println(widgetFactory.createSearchSelectField("DateType", "InvoiceSearch.DateType", " ", options,"class='char10' onKeydown='filterPayTranOnEnter(window.event, \"DateType\", \"Inv\");'"));
  			%>
		<%=widgetFactory.createSearchDateField("DateFrom","InvoiceSearch.From","class='char15' onKeydown='filterPayTranOnEnter(window.event, \"DateFrom\", \"Inv\");'","constraints:{datePattern:'"+datePattern+"'},placeHolder:'"+datePatternDisplay+"'") %>
		<%=widgetFactory.createSearchDateField("DateTo","InvoiceSearch.To","class='char15' onKeydown='filterPayTranOnEnter(window.event, \"DateTo\", \"Inv\");'","constraints:{datePattern:'"+datePattern+"'},placeHolder:'"+datePatternDisplay+"'") %>
		<%=widgetFactory.createSearchTextField("EndToEndId", "UploadInvoices.EndToEndId", "35","class='char15' onKeydown='filterPayTranOnEnter(window.event, \"EndToEndId\", \"Inv\");'") %>
	</span>
	
	
	<div style="clear:both"></div>
</div>

