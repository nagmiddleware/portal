<%--
*******************************************************************************
                  Instrument Search Advanced Filter include file

  Description:
    This is a JSP meant to be included using the <%@ include file="x.jsp" %> 
  tag.  It creates the HTML for displaying the Advanced Filter fields for the 
  Instrument Search.  As it is included with <%@ include %> rather than with
  the <jsp:include> directive, it is not a standalone servlet.  
  
 
  NOTE: Since this JSP is copied into the including page at compile time of
  the including page, changes to this page do not automatically force the
  including pages to be recompiled.  If you make changes to this page, you must
  delete the including page's servlet in order to see the changes.


*******************************************************************************
--%>
<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>

<div id="InstrSearch">
<div class="searchDetail">
    <span class="searchCriteria">
    <%=widgetFactory.createSearchTextField("InstrumentID", "InstSearch.InstrumentID", "20",  "class='char15'  onKeydown='filterPayTranOnEnter(window.event, \"InstrumentID\", \"Instr\");'") %>
    <% 
    options = Dropdown.createSortedRefDataOptions(TradePortalConstants.CURRENCY_CODE, currency, loginLocale);
    out.println(widgetFactory.createSearchSelectField("InstrCurrency", "InstSearch.Currency", " ", options," class='char22em'  onKeydown='filterPayTranOnEnter(window.event, \"InstrCurrency\", \"Instr\");'"));
    %>
    </span>
    
    <span class="searchActions">
      <button data-dojo-type="dijit.form.Button" type="button" id="searchInstrInquiries">
      <%= resMgr.getText("PartySearch.Search", TradePortalConstants.TEXT_BUNDLE) %>
        <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">searchInstrument();</script>
      </button>
    </span>
    
    <div style="clear:both"></div>
  </div>
</div>
<%
  gridHtml = dgFactory.createDataGrid("payInstrumentGrid","PayablesMgmtInstrumentDataGrid", null);
  gridLayout = dgFactory.createGridLayout("payInstrumentGrid", "PayablesMgmtInstrumentDataGrid");
  initSearchParms="selectedStatus="+selectedStatus+"&userOrgOid="+userOrgOid;
%>
<%=gridHtml%>
 
<%=widgetFactory.createHoverHelp("searchPayInquiries", "SearchHoverText") %>