<%--
*******************************************************************************
                              TradePortal Home Page

  Description:

  The main user landing page.

*******************************************************************************
--%>
<%-- SSikhakolli 1/15/2014
*******************************************************************************
Based on the CR590 requirement "DGrid has been implemented on all Home Page Grids.
A DGridFactory object is created and used in all places where DataGridFactory is used earlier.
All API calls using for DGrid API are in OnDemandGrid.js file under t360 folder.
All the function names are remain same in OnDemandGrid.js file as datagrid.js file in t360 folder.
A new file dGridCustomizationPopup.jsp has been added under dialog folder to handle grid customization popup and it's events.
A new file DGridFactory.java has been added under html directory to create grid layout.
New style classes has been implemented in portalstyle.css file which are overridden from dgrid.css to achieve DataGrid look and feel.
New functions setDynamicGridHeitht(), setGridHeightOnLoad(), removeColumnHider() has been added in TradePortalHome.jsp to manage dynamic grid height na dcustomization.
*******************************************************************************
--%>
<%--
 *
 *     Copyright   2001
 *     American Management Systems, Incorporated
 *     All rights reserved
--%>
<%@ page import="java.text.SimpleDateFormat, com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.html.*,java.util.*" %>

<jsp:useBean id="formMgr"     class="com.amsinc.ecsg.web.FormManager"            scope="session"></jsp:useBean>
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>
<jsp:useBean id="resMgr"      class="com.amsinc.ecsg.util.ResourceManager"       scope="session"></jsp:useBean>
<jsp:useBean id="beanMgr"     class="com.amsinc.ecsg.web.BeanManager"    	 scope="session"></jsp:useBean>
<jsp:useBean id="condDisplay" class="com.ams.tradeportal.html.ConditionalDisplay" scope="request">
  <jsp:setProperty name="condDisplay" property="userSession" value="<%=userSession%>" />
  <jsp:setProperty name="condDisplay" property="beanMgr" value="<%=beanMgr%>" />
</jsp:useBean>

<%
  Debug.debug("***START******** TradePortal Home Page ************START***");

  final String MY_MAIL_AND_UNASSIGNED = "Mail.MeAndUnassigned";
  final String ALL_MAIL = "Mail.AllMail";
  
  final String ALL_MAILMESS                  = "Mail.AllMESS";
  final String READ_MAILMESS                = "Mail.ReadMESS";
  final String UNREAD_MAILMESS              = "Mail.UnreadMESS"; 
		  
  final String ALL_NOTIFS = "Notifications.All";
  final String ALL_NOTIFRUN ="Notifications.AllRUN";
  final String READ_NOTIFS = "Notifications.ReadNotifications";
  final String UNREAD_NOTIFS = "Notifications.UnreadNotifications";
		  
  final String ALL_DEBITNOTIFRUN ="Mail.AllMESS";
  final String READ_DEBITNOTIFS = "Mail.ReadDebitNotif";
  final String UNREAD_DEBITNOTIFS = "Mail.UnreadDebitNotif";
  
  final String ALL = "common.all";
  final String ALL_WORK = "common.allWork";
  final String MY_WORK = "common.myWork";

  String  userOid        = userSession.getUserOid();
  String  userOrgOid	 = userSession.getOwnerOrgOid();
  String  loginLocale    = userSession.getUserLocale();
  String  loginRights    = userSession.getSecurityRights();
  String  defaultWIPView = userSession.getDefaultWipView();
  String  currentSecurityType = userSession.getSecurityType();
  boolean isCustomerSubsidiaryAccess = userSession.hasSavedUserSession();
  DocumentHandler hierarchyDoc = null;
  int totalOrganizations = 0;
  ReferenceDataManager refData = ReferenceDataManager.getRefDataMgr();
  WidgetFactory widgetFactory = new WidgetFactory(resMgr, userSession);// W Zhu Rel 8.2 T36000016357 add userSession
  boolean hasFTBARights = false;
  boolean hasFTDPRights = false;
  String selectedWorkflow = "";
  //DK IR T36000026794 Rel9.0 05/29/2014 starts
  String    fromTPHomePage          = (String) session.getAttribute("fromTPHomePage");
  if(fromTPHomePage == null) {
	  session.setAttribute("fromTPHomePage", TradePortalConstants.TRADE_PORTAL_HOME);
  }
  session.setAttribute("startHomePage", "TradePortalHome");
  //DK IR T36000026794 Rel9.0 05/29/2014 ends

//MEer Rel 8.4 IR-23915  
  
  /*   IR T36000010804 Made changes since the new line character was not getting preserved using the query
       Used AnnouncementWebBean instead to fetch the subject data; and preserved the new line character
	   @ Komal M
  */
  beanMgr.registerBean ("com.ams.tradeportal.busobj.webbean.AnnouncementWebBean",
          "Announcement");
  AnnouncementWebBean announcementBean =
          (AnnouncementWebBean)beanMgr.getBean("Announcement");
  
  String goToMessagesHome = "goToMessagesHome";

  //Variables needed for the sql statements and the returned number of rows.
  CorporateOrganizationWebBean org  = null;
  try {
    org = beanMgr.createBean(CorporateOrganizationWebBean.class, "CorporateOrganization");
    org.getById(userOrgOid);
    
  }
  catch(Exception e){
    System.out.println("************** Unable to build the user object because" +
      " the userOid on the UserSession is null, which caused the" +
      " getDataFromAppserver to fail!  ***************");
  }
  

	// Changes by Pavani - BMO Issue 80 - IR T36000011363 BEGIN. For users having rights to Payment Transactions, certain statuses
	// in All Transactions Status dropdown should be displayed, otherwise not.
  	hasFTBARights = (TradePortalConstants.INDICATOR_YES.equals(org.getAttribute("allow_xfer_btwn_accts")) ||
          TradePortalConstants.INDICATOR_YES.equals(org.getAttribute("allow_xfer_btwn_accts_panel")) ) &&
         SecurityAccess.hasRights(loginRights,SecurityAccess.TRANSFER_CREATE_MODIFY);
	hasFTDPRights = (TradePortalConstants.INDICATOR_YES.equals(org.getAttribute("allow_domestic_payments")) ||
          TradePortalConstants.INDICATOR_YES.equals(org.getAttribute("allow_domestic_payments_panel")) ) &&
         SecurityAccess.hasRights(loginRights,SecurityAccess.DOMESTIC_CREATE_MODIFY);

	// Changes by Pavani - BMO Issue 80 - IR T36000011363 END.
	
  // Determine if the admin or non-admin version of this page should be displayed
  if (TradePortalConstants.ADMIN.equals(currentSecurityType) && TradePortalConstants.INDICATOR_NO.equals(userSession.getCustomerAccessIndicator())){
    userSession.setCurrentPrimaryNavigation("Header.AdminHome");
  }
  else {
    userSession.setCurrentPrimaryNavigation("Header.Home");
  }

  //cquinton 1/17/2013 set return page
  if ( "true".equals( request.getParameter("returning") ) ) {
    userSession.pageBack(); //to keep it correct
  }
  else {
    userSession.addPage("goToTradePortalHome");
  }


  //cquinton 1/18/2013 remove old close action logic

  String refreshLink = formMgr.getLinkAsUrl("goToTradePortalHome", response);
  String editLink = formMgr.getLinkAsUrl("goToDashboardCustomization", response);

  String focusField = ""; //nothing to start
  Map searchQueryMap =  userSession.getSavedSearchQueryData();
  Map savedSearchQueryDataIO =null;
  Map savedSearchQueryDataMM =null;
  Map savedSearchQueryDataN =null;
  Map savedSearchQueryDataAN =null;
  Map savedSearchQueryDataA =null;
  Map savedSearchQueryDataCA =null;
  Map savedSearchQueryDataAT =null;
  Map savedSearchQueryDataRM =null;
  Map savedSearchQueryDataPA =null;
  Map savedSearchQueryData =null;
  Map savedSearchQueryDataFN =null;
  String searchNav = "tradeHome"; 
  String comma ="," ;
  String acctGridDataView = null;
  
  String isDisplayPreDebitNotify = "";
  if(StringFunction.isNotBlank(userSession.getClientBankOid())){
  com.ams.tradeportal.common.cache.Cache reCertCache = (com.ams.tradeportal.common.cache.Cache)com.ams.tradeportal.common.cache.TPCacheManager.getInstance().getCache(TradePortalConstants.CLIENT_BANK_CACHE);
  DocumentHandler CBCResult = (DocumentHandler)reCertCache.get(userSession.getClientBankOid());	
   isDisplayPreDebitNotify = CBCResult.getAttribute("/ResultSetRecord(0)/PRE_DEBIT_FUNDING_OPTION_IND");
  }
  
  String preDebitEnabledAtCorpLevel = TradePortalConstants.INDICATOR_NO;
  if(TradePortalConstants.INDICATOR_YES.equals(isDisplayPreDebitNotify)) {
     preDebitEnabledAtCorpLevel = org.getAttribute("pre_debit_funding_option_ind");
  }
%>
<%-- *****************   HTML starts here   ****************** --%>

<jsp:include page="/common/Header.jsp">
<jsp:param name="fromHomePage" value="Y"/> <%--CR 821 Rel 8.3 --%>
</jsp:include>
<jsp:include page="/common/SearchCriteriaCleanup.jsp">
<jsp:param name="searchCriteriaSection" value="<%=searchNav%>"/>
</jsp:include>
<%-- Vshah - 08/02/2012 - Rel8.1 - Portal-refresh --%>
<%--  including TransactionCleanup.jsp to remove lock on instrument which has been placed by this user... --%>
<%--  this is required because (with Portal-refresh) user can navigate anywhere from MenuBar when on Instrument(detail) page. --%>
<jsp:include page="/transactions/TransactionCleanup.jsp"></jsp:include>

<%--ctq portal refresh begin --%>
<div class="pageMainNoSidebar">
  <div class="pageContent">
<%
  if (TradePortalConstants.ADMIN.equals(currentSecurityType) && TradePortalConstants.INDICATOR_NO.equals(userSession.getCustomerAccessIndicator())) { 
%>
  <jsp:include page="/common/PageHeader.jsp">
    <jsp:param name="titleKeyCamel" value="AdminHome.Title" />
    <jsp:param name="helpUrl" value="/admin/admin_home.htm" />
  </jsp:include>
<% 
  }
  else {
%>
   <jsp:include page="/common/PageHeader.jsp">
    <jsp:param name="titleKey" value="Home.Title" />
    <jsp:param name="refreshUrl" value="<%= refreshLink %>" />
    <jsp:param name="refreshPage" value="true" />
    <jsp:param name="editUrl" value="<%= editLink %>" />
    <jsp:param name="helpUrl" value="/customer/home_page.htm" />
  </jsp:include>
  <%=widgetFactory.createHoverHelp("secondaryNavEdit", "Home.CustomizeDashboard") %>
<% 
  } 
%>



<%
  //build list of sections to display
  List<String> sections = new ArrayList<String>();
	boolean homePanelAuthTranGridFlag 	= false,
			homeRcvTranGridFlag 		= false,
			homeInvoicesOfferedGridFlag = false,
			homeMailGridFlag 			= false,
			homeNotifGridFlag 			= false,
			homeAllTranGridFlag 		= false,
			homeAcctGridFlag 			= false,
			homeDebFundMailGridFlag		= false;
	
	int homePanelAuthTranGridSectionIdx 	= 0,
		homeRcvTranGridSectionIdx 			= 0,
		homeInvoicesOfferedGridSectionIdx 	= 0,
		homeMailGridSectionIdx 				= 0,
		homeNotifGridSectionIdx 			= 0,
		homeAllTranGridSectionIdx 			= 0,
		homeAcctGridSectionIdx 				= 0,
		homeDebFundMailGridSectionIdx		= 0;
			 
	
  DataGridFactory dgFactory = new DataGridFactory(resMgr, userSession, formMgr, beanMgr, response);
  String gridHtml = "";
//CR 590 using dgrid instead of datagrid
  DGridFactory dGridFactory = new DGridFactory(resMgr, userSession, formMgr, beanMgr, response);

  String announcementOid = "";
  String announcementDialogTitle = "";
  boolean isAnnounceCritical = false; //default
  String dialogType = "";//default

  //announcements only display for corporate users
  if (!TradePortalConstants.ADMIN.equals(currentSecurityType)  ) {
    announcementDialogTitle =
      resMgr.getText("announcementDialog.title", TradePortalConstants.TEXT_BUNDLE);

    //get the user's bank org group and client bank
    String clientBankOid = userSession.getClientBankOid();
    String bogOid = userSession.getBogOid();
//out.println("clientbank="+clientBankOid);
//out.println("bogOid="+bogOid);

    //get the user date - which is specific to the user timezone!
    String currentGMTTime = DateTimeUtility.getGMTDateTime(true,false);//do not use override date
    Date myDate = TPDateTimeUtility.getLocalDateTime(
      currentGMTTime, userSession.getTimeZone());
    SimpleDateFormat isoDateFormatter = new SimpleDateFormat("yyyy-MM-dd");
    String myIsoDate = isoDateFormatter.format(myDate);
//out.println("myIsoDate="+myIsoDate);

    //get announcement text and setup the dialog
    String announcementTitle = "";
    String announcementSubject = "";
    StringBuilder aSql = new StringBuilder();
    //first look for criticals - we do this in 2 steps becuase ordering by
    //critical_ind doesn't work with nulls...
    aSql.append("select a.announcement_oid, a.title, a.subject, a.announcement_status ");
    aSql.append("from announcement a, announcement_bank_group b ");
    aSql.append("where a.announcement_oid = b.p_announcement_oid(+) "); //outer join
    //get bank announcements that apply to the bank group
    aSql.append("and ( (a.p_owner_org_oid = ? and a.ownership_level='BANK' and ");
    aSql.append(       "(a.all_bogs_ind='Y' or b.a_bog_oid = ?) ) or ");
    aSql.append(      "(a.p_owner_org_oid = ? and a.ownership_level='BOG') ) ");
    aSql.append("and a.announcement_status = 'ACTIVE' ");
    aSql.append("and a.critical_ind = 'Y' ");
    aSql.append("and a.start_date <= to_date(?, 'YYYY-MM-DD')");
    aSql.append("and a.end_date >= to_date(?, 'YYYY-MM-DD')");
    aSql.append("order by a.start_date desc, a.ownership_level");
    
  //jgadela R91 IR T36000026319 - SQL INJECTION FIX
    Object sqlParams[] = new Object[5];
    sqlParams[0] = clientBankOid;
    sqlParams[1] = bogOid;
    sqlParams[2] = bogOid;
    sqlParams[3] = myIsoDate;
    sqlParams[4] = myIsoDate;
    
    DocumentHandler aListDoc = DatabaseQueryBean.getXmlResultSet(aSql.toString(), false, sqlParams);
    if ( aListDoc == null ) { //no criticals!
      aSql = new StringBuilder();
      aSql.append("select a.announcement_oid, a.title, a.subject, a.announcement_status ");
      aSql.append("from announcement a, announcement_bank_group b ");
      aSql.append("where a.announcement_oid = b.p_announcement_oid(+) "); //outer join
      //get bank announcements that apply to the bank group
      aSql.append("and ( (a.p_owner_org_oid = ? and a.ownership_level='BANK' and ");
      aSql.append(       "(a.all_bogs_ind='Y' or b.a_bog_oid = ?) ) or ");
      aSql.append(      "(a.p_owner_org_oid = ? and a.ownership_level='BOG') ) ");
      aSql.append("and a.announcement_status = 'ACTIVE' ");
      aSql.append("and (critical_ind != 'Y' or critical_ind is null) ");
      aSql.append("and a.start_date <= to_date(?, 'YYYY-MM-DD')");
      aSql.append("and a.end_date >= to_date(?, 'YYYY-MM-DD')");
      aSql.append("order by a.start_date desc, a.ownership_level");
    //jgadela R91 IR T36000026319 - SQL INJECTION FIX
      aListDoc = DatabaseQueryBean.getXmlResultSet(aSql.toString(), false, sqlParams);
    } else {
        isAnnounceCritical = true;
        dialogType = "criticalDialog";
    }
    //only display announcement div if there is one
    if ( aListDoc != null ) {
      // jgadela 09/25/2014 R 9.1 T36000032516 - Critical symbol  for Critical Announcement
      boolean isCritical = false;	
      Vector aList = aListDoc.getFragments("/ResultSetRecord");
      if ( aList.size() > 0 ) {
        DocumentHandler aDoc = (DocumentHandler) aList.get(0);
        announcementOid = aDoc.getAttribute("/ANNOUNCEMENT_OID");
        announcementBean.setAttribute("announcement_oid", aDoc.getAttribute("/ANNOUNCEMENT_OID"));
        announcementBean.getDataFromAppServer();
     
        announcementTitle = aDoc.getAttribute("/TITLE");
        // jgadela 09/25/2014 R 9.1 T36000032516 - Critical symbol  for Critical Announcement
        if(TradePortalConstants.INDICATOR_YES.equalsIgnoreCase(announcementBean.getAttribute("critical_ind"))) {
        	isCritical = true;
        }
        
        if ( announcementTitle == null ) announcementTitle = "";
        announcementSubject =announcementBean.getAttribute("subject"); 
        		//aDoc.getAttribute("/SUBJECT");
        if ( announcementSubject == null ) announcementSubject = "";        
	if ( announcementTitle.length()+announcementSubject.length()> 225) {
          //truncate it. 225 is an attempt to only allow 2 lines
	  announcementSubject = announcementSubject.substring(
	    0, 225-announcementTitle.length()) + " ...";
        }
        String aStatus = aDoc.getAttribute("/ANNOUNCEMENT_STATUS");
        String aStatusDesc = null;
        try {
          aStatusDesc = refData.getDescr("ANNOUNCEMENT_STATUS",
            aStatus, loginLocale);
        } catch (Exception ex ) {} //do nothing
        if ( aStatusDesc != null ) {
          announcementDialogTitle += " - (" + aStatusDesc + ")";
        } else {
          announcementDialogTitle += " - (" + aStatus + ")";
        }
%>
    <div class="homeAnnounce">
<%// jgadela 09/25/2014 R 9.1 T36000032516 - Critical symbol  for Critical Announcement 
if (isCritical) {
%>
  <span> <div class="announceTitle critical" style="margin-bottom:-10px;" ><%=StringFunction.xssCharsToHtml(announcementTitle)%>: </div></span>
<%
    } else {
%> 
      <span style="margin-bottom:5px;" class="homeAnnounceLabel">
        <%=StringFunction.xssCharsToHtml(announcementTitle)%>: 
      </span>
 <%
    } 
%> 
	  		<span style="margin-bottom:5px;" >
	  		<br/><br/><%=announcementSubject %> </span> 
	  <%-- <%= widgetFactory.createTextArea("PreviewDialog", "", announcementSubject, true ,false, false, "style='width:auto; border:0px; overflow:hidden;'  rows='2' cols='80'", "maxLength:'25'","homeAnnounceText" ) %> --%>


      <span  style="margin-bottom:5px;" id="announceMore" class="announceMore">
        <%=resMgr.getText("Home.Announcement.More", TradePortalConstants.TEXT_BUNDLE)%>
      </span>
      <%=widgetFactory.createHoverHelp("announceMore", "Home.AnnounceMoreLink") %>
      <div style="clear:both;"></div>
    </div>
<%
      }
    }
  } //end if !ADMIN
%>


<%// CR ANZ 501 Rel 8.3 05-13-2013 jgadela  [START] - Showing ALERT when there are any pending refdata items to approve%>
<%
StringBuilder dynamicWhere = new StringBuilder("");
dynamicWhere.append(" select count(PENDING_DATA_OID) as COUNT from ref_data_pending a,users u where a.CHANGE_USER_OID = u.user_oid(+)  ");

boolean showNothing = true;
//jgadela R91 IR T36000026319 - SQL INJECTION FIX
List<Object> sqlParamsLst = new ArrayList();

String  userSecurityType  = null;
SessionWebBean userSession1 = userSession.getSavedUserSession();

//jgadela 10/10/2013 - Rel 8.3 IR T36000021772  - Modified code to get userSession.
//Retrieve the user's security type (i.e., ADMIN or NON_ADMIN)
if (userSession1 == null) {
	userSession1 = userSession;
	userSecurityType = userSession.getSecurityType();
}else {
	   	userSecurityType = userSession.getSavedUserSessionSecurityType();
} 
String loginRights1 = userSession1.getSecurityRights();
//jgadela 10/10/2013 - Rel 8.3 IR T36000021772  - Modified the security conditions and sql logic - added condition CHANGED_USER_SECURITY_TYPE = 'ADMIN'
String clientBankID = userSession.getClientBankOid();

if(userSecurityType.equals(TradePortalConstants.ADMIN) &&
		(condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_APPROVE_REF_DATA_ADMIN) || condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_APPROVE_REF_DATA))){
	dynamicWhere.append("and  a.change_user_oid != ? ");
	sqlParamsLst.add(userSession1.getUserOid());
	String currentSessionUserSecType = userSession.getSecurityType();
	if(!TradePortalConstants.OWNER_GLOBAL.equals(userSession.getOwnershipLevel())){
        //	dynamicWhere.append(" and (u.a_client_bank_oid is null or u.a_client_bank_oid = "+ clientBankID+")");// this will be client_bank oid when we login as BANKADMIN
    		if(currentSessionUserSecType.equalsIgnoreCase("ADMIN") ){
    			dynamicWhere.append(" and (a.owner_org_oid = ? ");
    			sqlParamsLst.add(clientBankID);
    		}
    		else{
    		 	dynamicWhere.append(" and (a.owner_org_oid = ? ");
    		 	sqlParamsLst.add(userSession.getOwnerOrgOid());
    		}
        	dynamicWhere.append(" or  a.owner_org_oid in  ("+getBOGUserObjectsSql(userSession1, sqlParamsLst)+"))");
    	}
	// Adding SQL logic to get Admin users  ref data type
	if (SecurityAccess.hasRights(loginRights1, SecurityAccess.MAINTAIN_ADM_USERS)) {
		
		if(currentSessionUserSecType.equalsIgnoreCase("ADMIN")){
        	dynamicWhere.append("and a.ownership_level != 'CORPORATE'");
        	
        }
		sqlParamsLst.add(userSession1.getUserOid());
		dynamicWhere.append("and ((a.CHANGED_OBJECT_OID != ? and " +"(CHANGE_USER_OID in ("+getAdminUserObjectsSql(userSession1,currentSessionUserSecType, sqlParamsLst)+") and CLASS_NAME in ('User')) ");
    	
    	// If the current user security type is NON_ADMIN(In case if the adminuser logged in  accesses the corporate customer page), show only corporate users, which are 
    	// modified by admin users.
    	
    	if (SecurityAccess.hasRights(userSession.getSecurityRights(), SecurityAccess.MAINTAIN_USERS) &&
    			currentSessionUserSecType.equalsIgnoreCase("NON_ADMIN"))  {
    		dynamicWhere.append("and a.ownership_level = 'CORPORATE'");
    		
    	}
    	if(TradePortalConstants.OWNER_TYPE_ADMIN.equalsIgnoreCase(currentSessionUserSecType))
        	dynamicWhere.append(" and CHANGED_USER_SECURITY_TYPE = 'ADMIN')");
        	else
        		dynamicWhere.append(" )");
    	
    	showNothing = false;
    }
	
	// Adding SQL logic to get CorporateOrganization  ref data type
	//If not ASP user then check for CorporateOrganization  ref data type
	if ((!TradePortalConstants.OWNER_GLOBAL.equals(userSession.getOwnershipLevel())) && (SecurityAccess.hasRights(userSession.getSecurityRights(), SecurityAccess.MAINTAIN_CORP_BY_BG) || SecurityAccess.hasRights(loginRights, SecurityAccess.MAINTAIN_CORP_BY_CB))) {
		
    	if(showNothing == true){
    		dynamicWhere.append(" and ( ( (a.CHANGED_OBJECT_OID in  ("+getCorpCustObjectsSql(userSession, sqlParamsLst)+") and CLASS_NAME in ('CorporateOrganization')) " +
    			                         " or (CHANGE_USER_OID in ("+getAdminUserObjectsSql(userSession1,currentSessionUserSecType, sqlParamsLst)+") and CLASS_NAME in ('CorporateOrganization') and CHANGE_TYPE= 'C')     )");
    	}else{
    		dynamicWhere.append(" or ((a.CHANGED_OBJECT_OID in  ("+getCorpCustObjectsSql(userSession, sqlParamsLst)+") and CLASS_NAME in ('CorporateOrganization') ) " +
    				"or (CHANGE_USER_OID in ("+getAdminUserObjectsSql(userSession1,currentSessionUserSecType, sqlParamsLst)+") and CLASS_NAME in ('CorporateOrganization') and CHANGE_TYPE= 'C')        )");
    	}
    	
    	showNothing = false;
    }
	
	// Adding SQL logic to get panel auth groups, which are modified by admin
	if (SecurityAccess.hasRights(userSession.getSecurityRights(), SecurityAccess.MAINTAIN_PANEL_AUTH_GRP)) {
		if(TradePortalConstants.OWNER_TYPE_ADMIN.equalsIgnoreCase(currentSessionUserSecType))
        	dynamicWhere.append(" and CHANGED_USER_SECURITY_TYPE = 'ADMIN')");
    	if(showNothing == true){
    		dynamicWhere.append("and ( (CHANGE_USER_OID in ("+getAdminUserObjectsSql(userSession1,currentSessionUserSecType, sqlParamsLst)+") and CLASS_NAME in ('PanelAuthorizationGroup')  and OWNER_ORG_OID = ? )");
    		sqlParamsLst.add(userOrgOid);
    	}else{
    		dynamicWhere.append("or (CHANGE_USER_OID in ("+getAdminUserObjectsSql(userSession1,currentSessionUserSecType, sqlParamsLst)+") and CLASS_NAME in ('PanelAuthorizationGroup')  and OWNER_ORG_OID = ? )");
    		sqlParamsLst.add(userOrgOid);
    	}
    	
    	showNothing = false;
    }
	
	if(showNothing){
		dynamicWhere.append("and 1=0");
	}else{
		dynamicWhere.append(" )");;
	}
	
  }// This block is used to filter the REF data types for admin user level  
  else {
	  if(condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_APPROVE_REF_DATA)){
	  // dynamicWhere.append(" a.owner_org_oid = "+ userOrgOid);
	   dynamicWhere.append(" and a.owner_org_oid = "+ userSession.getOwnerOrgOid());
	  dynamicWhere.append(" and a.ownership_level = 'CORPORATE'");
	   dynamicWhere.append(" and a.change_user_oid != "+userOid);
	  
	  // Adding SQL logic to get USer   ref data type
	  // T36000018453  - changed the logic for User should not be able to approve changes made to his/her own profile
	  if (SecurityAccess.hasRights(userSession.getSecurityRights(), SecurityAccess.MAINTAIN_USERS)) {
		 // dynamicWhere.append(" and ((CLASS_NAME = 'User')");
		 dynamicWhere.append(" and ((CLASS_NAME = 'User' and a.CHANGED_OBJECT_OID != "+userOid+")");
      	  showNothing = false;
      }
	// Adding SQL logic to get PanelAuthorizationGroup ref data type
  	  if (SecurityAccess.hasRights(userSession.getSecurityRights(), SecurityAccess.MAINTAIN_PANEL_AUTH_GRP)){
      	if(showNothing){
      		dynamicWhere.append(" and (CLASS_NAME = 'PanelAuthorizationGroup'");
		}else{
			dynamicWhere.append(" or CLASS_NAME = 'PanelAuthorizationGroup'");
		}
      	showNothing = false;
      }
	  }
  	  
	if(showNothing){
		dynamicWhere.append("and 1=0");
	}else{
		//jgadela 10/10/2013 - Rel 8.3 IR T36000021772  - Modified the  sql logic - added condition CHANGED_USER_SECURITY_TYPE = 'NON_ADMIN'
		//dynamicWhere.append(" and CHANGED_USER_SECURITY_TYPE = 'NON_ADMIN' )");;
		dynamicWhere.append(" )");
	}
	
  }
//jgadela R91 IR T36000026319 - SQL INJECTION FIX
DocumentHandler aPendCountDoc =  DatabaseQueryBean.getXmlResultSet(dynamicWhere.toString(), false, sqlParamsLst);
//only display announcement div if there is one
int  count = 0;
if ( aPendCountDoc != null ) {
   count = aPendCountDoc.getAttributeInt("/ResultSetRecord/COUNT");
}
//if 1st grid count is 0 then check 2nd grid count
if(count == 0){
	dynamicWhere = new StringBuilder("");
	//jgadela R91 IR T36000026319 - SQL INJECTION FIX
	 sqlParamsLst = new ArrayList();
	if (userSecurityType.equals(TradePortalConstants.ADMIN) &&
			userSession.getSecurityType().equalsIgnoreCase("ADMIN") &&
			(condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_APPROVE_REF_DATA_ADMIN) || condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_APPROVE_REF_DATA))) {
		//logged in user should see own changes
		dynamicWhere.append(" select count(PENDING_DATA_OID) as COUNT from ref_data_pending a,users u where a.CHANGE_USER_OID = u.user_oid(+)  ");

		dynamicWhere.append("and  a.change_user_oid != ? ");
		sqlParamsLst.add(userSession1.getUserOid());
		if(!TradePortalConstants.OWNER_GLOBAL.equals(userSession.getOwnershipLevel())){
	       	dynamicWhere.append(" and (u.a_client_bank_oid is null or u.a_client_bank_oid = ? )");// this will be client_bank oid when we login as BANKADMIN
	       	sqlParamsLst.add(clientBankID);
	    		// dynamicWhere.append(" and (a.owner_org_oid = "+clientBankID);
	    		
	        	//dynamicWhere.append(" or  a.owner_org_oid in  ("+getBOGUserObjectsSql(userSession1)+"))");
	    	}
	//	if (SecurityAccess.hasRights(loginRights1, SecurityAccess.MAINTAIN_ADM_USERS)) {
			
			
			//dynamicWhere.append("and (((a.CHANGED_OBJECT_OID in (" + adminUserIDsSql
			//		+ ") and CLASS_NAME in ('User')) ");
			dynamicWhere.append("and ((a.CHANGED_OBJECT_OID != " + userSession1.getUserOid()
					+ " and " + "(CHANGE_USER_OID in (" + getCorpUserObjectsSql(userSession1, sqlParamsLst)
					+ ") and CLASS_NAME in ('User')) ");

		String currentSessionUserSecType = userSession.getSecurityType();
			if (SecurityAccess.hasRights(userSession.getSecurityRights(), SecurityAccess.MAINTAIN_USERS)
					//even if its admin we need to show corporate user changes
					//&& currentSessionUserSecType.equalsIgnoreCase("NON_ADMIN")
							) {
				dynamicWhere.append("and a.ownership_level = 'CORPORATE'");
			}
			//dynamicWhere
			//		.append(" and CHANGED_USER_SECURITY_TYPE = 'ADMIN')");
			dynamicWhere.append(" )");
			showNothing = false;
	//	}

		String custAccesssecurity_right = "";
		SecurityProfileWebBean custAccessSecurityProfile = beanMgr.createBean(SecurityProfileWebBean.class, "SecurityProfile");
		custAccessSecurityProfile.getById(userSession
				.getCustomerAccessSecurityProfileOid());
		custAccesssecurity_right = custAccessSecurityProfile
				.getAttribute("security_rights");
		
		if (SecurityAccess.hasRights(custAccesssecurity_right,
				SecurityAccess.MAINTAIN_PANEL_AUTH_GRP)) {

			if (showNothing == true) {
				dynamicWhere
						.append("and ( (CHANGE_USER_OID in ("
								+ getCorpUserObjectsSql(userSession1, sqlParamsLst)
								+ ") and CLASS_NAME in ('PanelAuthorizationGroup') " +
								//"and CHANGED_USER_SECURITY_TYPE = 'ADMIN' " +  // not only changes by admin 
								")");
			} else {
				dynamicWhere
						.append("or (CHANGE_USER_OID in ("
								+ getCorpUserObjectsSql(userSession1, sqlParamsLst)
								+ ") and CLASS_NAME in ('PanelAuthorizationGroup') " +
								//"and CHANGED_USER_SECURITY_TYPE = 'ADMIN' " +  // not only changes by admin 
								")");
			}

			showNothing = false;
		}

		if (showNothing) {
			dynamicWhere.append("and 1=0");
		} else {
			dynamicWhere.append(" )");
			
		}
		
		aPendCountDoc =  DatabaseQueryBean.getXmlResultSet(dynamicWhere.toString(), false, sqlParamsLst);
		//only display announcement div if there is one

		if ( aPendCountDoc != null ) {
		 count = aPendCountDoc.getAttributeInt("/ResultSetRecord/COUNT");
		}
	}	
	
	
	
}
%>

<%if(count > 0) {
	String pendingDataGridLink = formMgr.getLinkAsUrl("goToRefDataApproval", "", response);
%>

<div class="homeAnnounce">
      <span style="margin-bottom:7px;" class="homeAnnounceLabel">
        <%=resMgr.getText("PendingRefdata.Home.Alert", TradePortalConstants.TEXT_BUNDLE)%>:
      </span>
      
      <span style="margin-bottom:7px;"  class="homeAnnounceText">
      &nbsp;&nbsp;<%=resMgr.getText("PendingRefdata.Home.Alert.Message", TradePortalConstants.TEXT_BUNDLE)%>
      <%-- <%=announcementSubject %> --%>     
      </span>
     
      <span  style="margin-bottom:7x;" class="announceMore">
      <a href="<%=pendingDataGridLink%>">  <%=resMgr.getText("Home.Announcement.More", TradePortalConstants.TEXT_BUNDLE)%> </a>
      </span>
      <%//=widgetFactory.createHoverHelp("announceMore", "Home.AnnounceMoreLink") %>
      <div style="clear:both;"></div>
    </div>
<%} %>
<%// CR ANZ 501 Rel 8.3 05-13-2013 jgadela  [END] - Showing ALERT when there are any pending refdata items to approve%>


    <jsp:include page="/common/ErrorSection.jsp" />


    <%--main content --%>
<%
  if (!TradePortalConstants.ADMIN.equals(currentSecurityType) ) {

    //get the user's dashboard preferences
    //if user doesn't have any, then go with the default
	//jgadela R91 IR T36000026319 - SQL INJECTION FIX
    String dashSql = "select section_id from dashboard_section where a_user_oid = ? order by display_order";
    DocumentHandler dashListDoc = DatabaseQueryBean.getXmlResultSet(dashSql.toString(), false, new Object[]{userOid});
    if ( dashListDoc != null ) {
      Vector dashList = dashListDoc.getFragments("/ResultSetRecord");
      for( int i=0; i<dashList.size(); i++ ) {
        DocumentHandler dashDoc = (DocumentHandler) dashList.get(i);
        String sectionIdAttr = dashDoc.getAttribute("/SECTION_ID");      
        sections.add(sectionIdAttr);
      }
    }
    
    /* Start Vsarkary Rel 9.2 IR#T36000034829 -- commenting out as the IR says if no Dashboard display preferences section is selected it should not display anything on home page
    As these are loaded as default if no  Dashboard display preferences section is selected. 
    Changes not migrated properly doing it again*/
    /* MEer Rel 9.2 BaseCMActivity#T36000036626- Reverting changes made towards IR#T36000034829 and fix the IR#34829 as per Design appropriately.  */
    
     //default if necessary
    //Rel8.3 IR#T36000023004 - In case of default, check for rights
    if ( sections.size() <= 0 ) {
    	if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_MAIL_MESSAGES ) ) {
   	 		sections.add(TradePortalConstants.DASHBOARD_SECTION__MAIL);
	  }
	   if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_NOTIFICATIONS ) ) {
    		sections.add(TradePortalConstants.DASHBOARD_SECTION__NOTIFICATIONS);
	   }
	   if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_DEBIT_FUNDING_MAIL )
			   && TradePortalConstants.INDICATOR_YES.equals(isDisplayPreDebitNotify)
			   &&  TradePortalConstants.INDICATOR_YES.equals(preDebitEnabledAtCorpLevel) ) {//Rel 9.0 CR 913
	   		sections.add(TradePortalConstants.DASHBOARD_SECTION_DEBIT_FUNDING_MAIL);
		}
	   if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_INVOICE_OFFERED ) ) {
    		sections.add(TradePortalConstants.DASHBOARD_SECTION__INVOICE_OFFERED);
	   }
	   if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_TRANSACTIONS ) ) {
    		sections.add(TradePortalConstants.DASHBOARD_SECTION__ALL_TRANSACTIONS);
       }
	   
	   if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_PANEL_AUTHORISATION_TRANSACTIONS ) ) {
   			sections.add(TradePortalConstants.DASHBOARD_SECTION__PANEL_AUTH_TRANSACTIONS); //CR 821 Rel 8.3
	   }
	   
	   if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_RECEIVABLE_TRANSACTIONS ) ) {
    		sections.add(TradePortalConstants.DASHBOARD_SECTION__RCV_MATCH_NOTICES);
	   }
	   //IR 23015 -same condition as on Accounts menubar
	  /* if ( sections.contains( ConditionalDisplay.DISPLAY_NEW_PAYMENT_INSTRUMENTS ) ) */
	   if ( sections.contains( ConditionalDisplay. DISPLAY_ACCOUNT_BALANCES) ) {
    	  sections.add(TradePortalConstants.DASHBOARD_SECTION__ACCOUNT_BALANCES);
	   } 
    }
 /* MEer Rel 9.2 BaseCMActivity#T36000036626  */
 // End Vsarkary Rel 9.2 IR#T36000034829 
    
    //execute common items if necessary

    //get the org hierarchy - this is used in most view so always do
    	
    // This is the query used for populating the option drop down list;
    // it retrieves all active child organizations that belong to the user's
    // org in addition to the user's org itself.  It is used by both the mail
    // and notification.
    //jgadela R91 IR T36000026319 - SQL INJECTION FIX
    String sqlQuery = "select organization_oid, name from corporate_org where activation_status = ? start with organization_oid = ? "+
    	" connect by prior organization_oid = p_parent_corp_org_oid order by "+resMgr.localizeOrderBy("name");
    hierarchyDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery.toString(), false, TradePortalConstants.ACTIVE, userOrgOid);
    totalOrganizations = hierarchyDoc.getFragments("/ResultSetRecord").size();

  } //end if !ADMIN 
  %>
  <%-- MEer IR-34829 If user customized to display no sections on the portal home page and just display Info msg. --%>
 <%   if ( sections.size() == 1 &&  sections.contains(TradePortalConstants.DASHBOARD_NO_SECTIONS_DISPLAY)){ %>
  <div id="infoSection" class="infos">
    <div id="infoTextMessage"  class="infoText"><%=resMgr.getText("HomePageMsg.Dashboard.No.Sections.Display", TradePortalConstants.TEXT_BUNDLE) %></div>
  </div>
	  
<%  }
 else { //if ADMIN

    sections.add(TradePortalConstants.DASHBOARD_SECTION__ANNOUNCEMENTS);
    sections.add(TradePortalConstants.DASHBOARD_SECTION__LOCKED_OUT_USERS);

  }


  //now spin through the sections list.
  // note that order of the below sections in the code doesn't matter
  // becuase the sections list specifies the order of display

  String sectionId = "";
  int gridShowCount = 5;
  for( int sectionIdx = 0; sectionIdx < sections.size(); sectionIdx++ ) {
    sectionId = sections.get(sectionIdx);
  //Ravindra - CR-708B - Start
    /************************************************************************/
    /*                           INVOICES OFFERED SECTION 					 */
    /************************************************************************/
    if ( TradePortalConstants.DASHBOARD_SECTION__INVOICE_OFFERED.equals(sectionId) &&
         condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_INVOICE_OFFERS ) ) {
    	if(searchQueryMap!=null && !searchQueryMap.isEmpty()){
      		 savedSearchQueryDataIO = (Map)searchQueryMap.get("HomeInvoicesOfferedDataView");
      	}
%>
    <div class="formContentNoSidebar">
      <div class="gridHeader">
        <span class="gridHeaderTitle">
<%
      String invOfferedUrlParm = "&current2ndNav=" + TradePortalConstants.DISPLAY_INVOICE_OFFERED;
%>
	  <a id="invoiceOfferedLink" href="<%= formMgr.getLinkAsUrl("goToInvoiceOffersTransactionsHome", invOfferedUrlParm, response) %>">
            <%=resMgr.getText("Home.InvoicesOffered", TradePortalConstants.TEXT_BUNDLE)%>
          </a>
          <%=widgetFactory.createHoverHelp("invoiceOfferedLink", "Home.InvoicesOfferedLink") %>
        </span>
        <span class="gridHeader-right">
          <span class="gridHeaderShowCounts">
            <span><%=resMgr.getText("Home.ShowCountPreText", TradePortalConstants.TEXT_BUNDLE)%></span>
<%
      String show5Class = "gridHeaderShowCountItem";
      String show10Class = "gridHeaderShowCountItem";
      String show20Class = "gridHeaderShowCountItem";
      if ( gridShowCount == 5 ) {
        show5Class += " selected";
      }
      else if ( gridShowCount == 10 ) {
        show10Class += " selected";
      }
      else if ( gridShowCount == 20 ) {
        show20Class += " selected";
      }
%>
  			<span id="invoicesOfferedShowCount5" class="<%= show5Class %>" >5</span>
            <span id="invoicesOfferedShowCount10" class="<%= show10Class %>" >10</span>
            <span id="invoicesOfferedShowCount20" class="<%= show20Class %>" >20</span>
            <span><%=resMgr.getText("Home.ShowCountBetweenText", TradePortalConstants.TEXT_BUNDLE)%></span>
            <span id="homeInvoicesOfferedGrid_totalCount">0</span>
          </span>
          <span id="homeInvoicesOfferedGridEdit" class="gridHeaderEdit"></span>
          <%=widgetFactory.createHoverHelp("homeInvoicesOfferedGridEdit", "CustomizeListImageLinkHoverText") %>
        </span>
        <%--<div style="clear:both;"></div>--%>
      </div>
      <div>
       <%
       String currency = "";
       String options = Dropdown.createSortedRefDataOptions(TradePortalConstants.CURRENCY_CODE, currency, loginLocale);
       String datePattern = userSession.getDatePattern();
       String dateWidgetOptions = "placeHolder:'" + datePattern + "'"; 

       %>
      	<%=widgetFactory.createSearchSelectField("Currency", "InvoiceSearch.Currency", " ", options,"class='char5'") %>
      	<%=widgetFactory.createSubLabel("InvoicesOffered.Amount")%>
  		<%=widgetFactory.createSearchAmountField("AmountFrom", "InvoiceSearch.From","","style='width:8em' class='char5'", "") %>
		<%=widgetFactory.createSearchAmountField("AmountTo", "InvoiceSearch.To","","style='width:8em' class='char5'", "") %>
		<%=widgetFactory.createSubLabel("InvoicesOffered.DueOrPaymentDateFilter")%>
		<%=widgetFactory.createSearchDateField("DateFrom","InvoiceSearch.From","class='char10'",dateWidgetOptions) %>
		<%=widgetFactory.createSearchDateField("DateTo","InvoiceSearch.To","class='char10'",dateWidgetOptions) %>
      <div style="clear:both"></div>
      </div>
<%
	homeInvoicesOfferedGridFlag = true;
	homeInvoicesOfferedGridSectionIdx = sectionIdx;
	gridHtml = dGridFactory.createDataGrid("homeInvoicesOfferedGrid","HomeInvoicesOfferedDataGrid",null);
%>
      <%= gridHtml %>
    </div>
<%
      if (focusField==null) {
        focusField = "MailOrg";
      }
    } //end if invoices offered section
  //Ravindra - CR-708B - End

    /************************************************************************/
    /*                           MAIL SECTION                               */
    /************************************************************************/

    if ( TradePortalConstants.DASHBOARD_SECTION__MAIL.equals(sectionId) &&
         condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_MAIL_MESSAGES ) ) {
    	if(searchQueryMap!=null && !searchQueryMap.isEmpty()){
   		 savedSearchQueryDataMM = (Map)searchQueryMap.get("HomeMailMessagesDataView");
   	}
%>
    <div class="formContentNoSidebar">
      <div class="gridHeader">
        <span class="gridHeaderTitle">
<%
      String mailUrlParm = "&current2ndNav=" + TradePortalConstants.NAV__MAIL +
        "&currentFolder=" + TradePortalConstants.INBOX_FOLDER +
		    "&currentMailOption" + EncryptDecrypt.encryptStringUsingTripleDes(resMgr.getText("Mail.MeAndUnassigned",
								                                        TradePortalConstants.TEXT_BUNDLE), userSession.getSecretKey());
%>
	  <a id="mailLink" href="<%= formMgr.getLinkAsUrl(goToMessagesHome, mailUrlParm, response) %>">
            <%=resMgr.getText("Home.MailMessages", TradePortalConstants.TEXT_BUNDLE)%>
          </a>
          <%=widgetFactory.createHoverHelp("mailLink", "Home.MailMessagesLink") %>
        </span>
        <span class="gridHeader-right">
          <span class="gridHeaderShowCounts">
            <span><%=resMgr.getText("Home.ShowCountPreText", TradePortalConstants.TEXT_BUNDLE)%></span>
<%
      String show5Class = "gridHeaderShowCountItem";
      String show10Class = "gridHeaderShowCountItem";
      String show20Class = "gridHeaderShowCountItem";
      if ( gridShowCount == 5 ) {
        show5Class += " selected";
      }
      else if ( gridShowCount == 10 ) {
        show10Class += " selected";
      }
      else if ( gridShowCount == 10 ) {
        show20Class += " selected";
      }
%>
            <span id="mailShowCount5" class="<%= show5Class %>" >5</span>
            <span id="mailShowCount10" class="<%= show10Class %>" >10</span>
            <span id="mailShowCount20" class="<%= show20Class %>" >20</span>
            <span><%=resMgr.getText("Home.ShowCountBetweenText", TradePortalConstants.TEXT_BUNDLE)%></span>
            <span id="homeMailGrid_totalCount">0</span>
          </span>
          <span id="homeMailGridEdit" class="gridHeaderEdit"></span>
          <%=widgetFactory.createHoverHelp("homeMailGridEdit", "CustomizeListImageLinkHoverText") %>
        </span>
        <%--<div style="clear:both;"></div>--%>
      </div>
      <div>
<%    //build mail options
      String currentMailOption = EncryptDecrypt.encryptStringUsingTripleDes(MY_MAIL_AND_UNASSIGNED, userSession.getSecretKey());

      StringBuffer mailOrgOptions = new StringBuffer();
      // If the user has rights to view child organization mail, retrieve the list of dropdown
      // options from the DatabaseQueryBean results doc (containing an option for each child org);
      // otherwise, simply use the user's org option to build the dropdown list (in addition
      // to the default 'Me' or 'Me and Unassigned' option).
      mailOrgOptions.append("<option value=\"").
        append(EncryptDecrypt.encryptStringUsingTripleDes(MY_MAIL_AND_UNASSIGNED, userSession.getSecretKey())).
        append("\" selected >");
      mailOrgOptions.append(resMgr.getText( MY_MAIL_AND_UNASSIGNED, TradePortalConstants.TEXT_BUNDLE));
      mailOrgOptions.append("</option>");
      if (SecurityAccess.hasRights(loginRights, SecurityAccess.VIEW_CHILD_ORG_MESSAGES)) {
         mailOrgOptions.append(Dropdown.getUnsortedOptions(hierarchyDoc, "ORGANIZATION_OID", "NAME",
                                                           currentMailOption, userSession.getSecretKey()));
         // Only display the 'All Mail' option if the user's org has child organizations
         if (totalOrganizations > 1) {
            mailOrgOptions.append("<option value=\"").
              append(EncryptDecrypt.encryptStringUsingTripleDes(ALL_MAIL, userSession.getSecretKey())).
              append("\"");
            if (currentMailOption.equals(ALL_MAIL)) {
               mailOrgOptions.append(" selected");
            }
            mailOrgOptions.append(">");
            mailOrgOptions.append(resMgr.getText( ALL_MAIL, TradePortalConstants.TEXT_BUNDLE));
            mailOrgOptions.append("</option>");
         }
      }
      else {
         mailOrgOptions.append("<option value=\"").
           append(EncryptDecrypt.encryptStringUsingTripleDes(userOrgOid, userSession.getSecretKey())).
           append("\"");
         if (currentMailOption.equals(userOrgOid)) {
            mailOrgOptions.append(" selected");
         }
         mailOrgOptions.append(">");
         mailOrgOptions.append(userSession.getOrganizationName());
         mailOrgOptions.append("</option>");
      }
%>
        <%= widgetFactory.createSearchSelectField( "MailOrg", "Home.Mail.Show", "", mailOrgOptions.toString()) %>
        
        <%
          
        StringBuffer dropdownMailOptions = new StringBuffer();
        boolean userCanFilterOrg = true;
      
        dropdownMailOptions.append("<option value='"+ALL_MAILMESS+"' selected='true' >");
        dropdownMailOptions.append(resMgr.getText( ALL_MAILMESS, TradePortalConstants.TEXT_BUNDLE));
        dropdownMailOptions.append("</option>");
     
        dropdownMailOptions.append("<option value='N' selected='true' >");
        dropdownMailOptions.append(resMgr.getText( READ_MAILMESS, TradePortalConstants.TEXT_BUNDLE));
        dropdownMailOptions.append("</option>");
      	
        dropdownMailOptions.append("<option value='Y' selected='true' >");
        dropdownMailOptions.append(resMgr.getText( UNREAD_MAILMESS, TradePortalConstants.TEXT_BUNDLE));
        dropdownMailOptions.append("</option>");
      %>          
        <%=widgetFactory.createSearchSelectField("MailReadUnread", "Mail.ReadUnreadMess", "", 
        		dropdownMailOptions.toString())%>        

               
      </div>
      
      
<%
	homeMailGridFlag = true;
	homeMailGridSectionIdx = sectionIdx;
	gridHtml = dGridFactory.createDataGrid("homeMailGrid","HomeMailMessagesDataGrid",null);
%>
      <%= gridHtml %>
    </div>
<%
      if (focusField==null) {
        focusField = "MailOrg";
      }
    } //end if mail section


    /************************************************************************/
    /*                           NOTIFICATIONS SECTION                      */
    /************************************************************************/

    if ( TradePortalConstants.DASHBOARD_SECTION__NOTIFICATIONS.equals(sectionId) &&
         condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_NOTIFICATIONS ) ) {
    	if(searchQueryMap!=null && !searchQueryMap.isEmpty()){
    		 savedSearchQueryDataN = (Map)searchQueryMap.get("HomeNotificationsDataView");
    	}
%>


    <div class="formContentNoSidebar">
      <div class="gridHeader">
        <span class="gridHeaderTitle">
<%
    String notifUrlParm = "&current2ndNav=" + TradePortalConstants.NAV__NOTIFICATIONS;
%>
	  <a id="notifLink" href="<%= formMgr.getLinkAsUrl(goToMessagesHome, notifUrlParm, response) %>">
            <%=resMgr.getText("Home.Notifications", TradePortalConstants.TEXT_BUNDLE)%>
          </a>
          <%=widgetFactory.createHoverHelp("notifLink", "Home.NotificationsLink") %>
        </span>
        <span class="gridHeader-right">
          <span class="gridHeaderShowCounts">
            <span><%=resMgr.getText("Home.ShowCountPreText", TradePortalConstants.TEXT_BUNDLE)%></span>
<%
      String show5Class = "gridHeaderShowCountItem";
      String show10Class = "gridHeaderShowCountItem";
      String show20Class = "gridHeaderShowCountItem";
      if ( gridShowCount == 5 ) {
        show5Class += " selected";
      }
      else if ( gridShowCount == 10 ) {
        show10Class += " selected";
      }
      else if ( gridShowCount == 10 ) {
        show20Class += " selected";
      }
%>
            <span id="notifShowCount5" class="<%= show5Class %>" >5</span>
            <span id="notifShowCount10" class="<%= show10Class %>" >10</span>
            <span id="notifShowCount20" class="<%= show20Class %>" >20</span>

            <span><%=resMgr.getText("Home.ShowCountBetweenText", TradePortalConstants.TEXT_BUNDLE)%></span>
            <span id="homeNotifGrid_totalCount">0</span>
          </span>
          <span id="homeNotifGridEdit" class="gridHeaderEdit"></span>
          <%=widgetFactory.createHoverHelp("homeNotifGridEdit", "CustomizeListImageLinkHoverText") %>
        </span>
        <%--<div style="clear:both;"></div>--%>
      </div>
      <div>
<%    //build notification options
      String currentNotifOption = EncryptDecrypt.encryptStringUsingTripleDes(userOrgOid, userSession.getSecretKey());

      StringBuffer notifOrgOptions = new StringBuffer();
      // If the user has rights to view child organizations, retrieve the list of
      // dropdown options from the DatabaseQueryBean results doc (containing an
      // option for each child org), including an "All" option.  Otherwise,
      // simply use the user's org as the only option.
      if (SecurityAccess.hasRights(loginRights,
          SecurityAccess.VIEW_CHILD_ORG_MESSAGES)) {
        notifOrgOptions.append(Dropdown.getUnsortedOptions(hierarchyDoc, "ORGANIZATION_OID", "NAME",
                                                           currentNotifOption, userSession.getSecretKey()));

        // Only display the 'All' option if the user's org has child orgs
        if (totalOrganizations > 1) {
          notifOrgOptions.append("<option value=\"");
          notifOrgOptions.append(EncryptDecrypt.encryptStringUsingTripleDes(ALL_NOTIFS, userSession.getSecretKey()));
          notifOrgOptions.append("\"");

        //IR T36000024951, make all selectable based upon condition.
          if (currentNotifOption.equals(ALL_NOTIFS)) {
            notifOrgOptions.append(" selected");
          }

          notifOrgOptions.append(">");
          notifOrgOptions.append(resMgr.getText( ALL_NOTIFS, TradePortalConstants.TEXT_BUNDLE));
          notifOrgOptions.append("</option>");
        }
      }
      else {
        notifOrgOptions.append("<option value=\"");
        notifOrgOptions.append(EncryptDecrypt.encryptStringUsingTripleDes(userOrgOid, userSession.getSecretKey()));
        notifOrgOptions.append("\"");

        if (currentNotifOption.equals(userOrgOid)) {
          notifOrgOptions.append(" selected");
        }

        notifOrgOptions.append(">");
        notifOrgOptions.append(userSession.getOrganizationName());
        notifOrgOptions.append("</option>");
      }
%>
        <%= widgetFactory.createSearchSelectField( "NotifOrg", "Home.Notifications.Show", "", notifOrgOptions.toString()) %>
<%
      StringBuffer notifStatusOptions = new StringBuffer();
      notifStatusOptions.append("<option value='").append(ALL_NOTIFS).append("'").
        append(" selected >").
        append(resMgr.getText( ALL_NOTIFS, TradePortalConstants.TEXT_BUNDLE)).
        append("</option>");
      /* notifStatusOptions.append("<option value='").
        append(TradePortalConstants.TRANS_STATUS_AUTHORIZE_FAILED).
        append("'>").
        append(refData.getDescr("TRANSACTION_STATUS", TradePortalConstants.TRANS_STATUS_AUTHORIZE_FAILED, loginLocale)).
        append("</option>");
      notifStatusOptions.append("<option value='").
        append(TradePortalConstants.TRANS_STATUS_AUTHORIZED).
        append("'>").
        append(refData.getDescr("TRANSACTION_STATUS", TradePortalConstants.TRANS_STATUS_AUTHORIZED, loginLocale)).
        append("</option>"); */
      notifStatusOptions.append("<option value='").
        append(TradePortalConstants.TRANS_STATUS_CANCELLED_BY_BANK).
        append("'>").
        append(refData.getDescr("TRANSACTION_STATUS", TradePortalConstants.TRANS_STATUS_CANCELLED_BY_BANK, loginLocale)).
        append("</option>");
      /* notifStatusOptions.append("<option value='").
        append(TradePortalConstants.TRANS_STATUS_DELETED).
        append("'>").
        append(refData.getDescr("TRANSACTION_STATUS", TradePortalConstants.TRANS_STATUS_DELETED, loginLocale)).
        append("</option>"); */
      notifStatusOptions.append("<option value='").
        append(TradePortalConstants.TRANS_STATUS_PARTIALLY_REJECTED_PAYMENT).
        append("'>").
        append(refData.getDescr("TRANSACTION_STATUS", TradePortalConstants.TRANS_STATUS_PARTIALLY_REJECTED_PAYMENT, loginLocale)).
        append("</option>");
      notifStatusOptions.append("<option value='").
        append(TradePortalConstants.TRANS_STATUS_PROCESSED_BY_BANK).
        append("'>").
        append(refData.getDescr("TRANSACTION_STATUS", TradePortalConstants.TRANS_STATUS_PROCESSED_BY_BANK, loginLocale)).
        append("</option>");
      notifStatusOptions.append("<option value='").
        append(TradePortalConstants.TRANS_STATUS_READY_TO_AUTHORIZE).
        append("'>").
        append(refData.getDescr("TRANSACTION_STATUS", TradePortalConstants.TRANS_STATUS_READY_TO_AUTHORIZE, loginLocale)).
        append("</option>");
      notifStatusOptions.append("<option value='").
        append(TradePortalConstants.TRANS_STATUS_REJECTED_BY_BANK).
        append("'>").
        append(refData.getDescr("TRANSACTION_STATUS", TradePortalConstants.TRANS_STATUS_REJECTED_BY_BANK, loginLocale)).
        append("</option>");
      notifStatusOptions.append("<option value='").
        append(TradePortalConstants.TRANS_STATUS_REJECTED_PAYMENT).
        append("'>").
        append(refData.getDescr("TRANSACTION_STATUS", TradePortalConstants.TRANS_STATUS_REJECTED_PAYMENT, loginLocale)).
        append("</option>");
      /* notifStatusOptions.append("<option value='").
        append(TradePortalConstants.TRANS_STATUS_STARTED).
        append("'>").
        append(refData.getDescr("TRANSACTION_STATUS", TradePortalConstants.TRANS_STATUS_STARTED, loginLocale)).
        append("</option>"); */
%>
        <%=widgetFactory.createSearchSelectField( "NotifStatus", "Home.Notifications.criteria.Status", "", 
        		notifStatusOptions.toString()) %>

<%-- Jaya Mamidala CR49930 Start--%> 

<%  StringBuffer dropdownOptions = new StringBuffer();
    boolean userCanFilterOrg = true;
  
  	dropdownOptions.append("<option value='"+ALL_NOTIFRUN+"' selected='true' >");
  	dropdownOptions.append(resMgr.getText( ALL_NOTIFRUN, TradePortalConstants.TEXT_BUNDLE));
  	dropdownOptions.append("</option>");
 
    dropdownOptions.append("<option value= 'N' selected='true' >");
    dropdownOptions.append(resMgr.getText( READ_NOTIFS, TradePortalConstants.TEXT_BUNDLE));
    dropdownOptions.append("</option>");
  	
    dropdownOptions.append("<option value= 'Y' selected='true' >");
    dropdownOptions.append(resMgr.getText( UNREAD_NOTIFS, TradePortalConstants.TEXT_BUNDLE));
    dropdownOptions.append("</option>");
%>          
    <%=widgetFactory.createSearchSelectField("Read/Unread", "Home.Notifications.criteria.Read/Unread", "", 
        			dropdownOptions.toString())%>
 </div>
 <%-- Jaya Mamidala CR49930 End --%>

<%
	homeNotifGridFlag = true;
	homeNotifGridSectionIdx = sectionIdx;
      gridHtml = dGridFactory.createDataGrid("homeNotifGrid","HomeNotificationsDataGrid",null);
%>
      <%= gridHtml %>

    </div>
<%
      if (focusField==null) {
        focusField = "NotifOrg";
      }
    } //end if notifications section
    
  //CR 913 - Rel 9.0 - vdesingu - Start
    /************************************************************************/
    /*                           PRE-DEBIT FUNDING NOTIFICATIONS            */
    /************************************************************************/
    if ( TradePortalConstants.DASHBOARD_SECTION_DEBIT_FUNDING_MAIL.equals(sectionId) 
    		&& TradePortalConstants.INDICATOR_YES.equals(isDisplayPreDebitNotify)
 		   && TradePortalConstants.INDICATOR_YES.equals(preDebitEnabledAtCorpLevel)
         && condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_DEBIT_FUNDING_MAIL ) ) {
    	if(searchQueryMap!=null && !searchQueryMap.isEmpty()){
     		 savedSearchQueryDataFN = (Map)searchQueryMap.get("HomeDebitFundingMailMsgDataView");
     	}
  %>
      <div class="formContentNoSidebar">
        <div class="gridHeader">
        <span class="gridHeaderTitle">
<%
     String debitFundUrlParm = "&current2ndNav=" + TradePortalConstants.NAV__PRE_DEBIT_FUNDING +
		    "&currentMailOption" + EncryptDecrypt.encryptStringUsingTripleDes(resMgr.getText("Mail.MeAndUnassigned", 
		    		TradePortalConstants.TEXT_BUNDLE), userSession.getSecretKey());
%>
	  <a id="mailLink" href="<%= formMgr.getLinkAsUrl(goToMessagesHome, debitFundUrlParm, response) %>">
           <%=resMgr.getText("Home.DebitFundingNotifications", TradePortalConstants.TEXT_BUNDLE)%>
         </a>
         <%=widgetFactory.createHoverHelp("mailLink", "Home.MailMessagesLink") %>
       </span>

          <span class="gridHeader-right">
            <span class="gridHeaderShowCounts">
              <span><%=resMgr.getText("Home.ShowCountPreText", TradePortalConstants.TEXT_BUNDLE)%></span>
  <%
        String show5Class = "gridHeaderShowCountItem";
        String show10Class = "gridHeaderShowCountItem";
        String show20Class = "gridHeaderShowCountItem";
        if ( gridShowCount == 5 ) {
          show5Class += " selected";
        }
        else if ( gridShowCount == 10 ) {
          show10Class += " selected";
        }
        else if ( gridShowCount == 10 ) {
          show20Class += " selected";
        }
  %>
              <span id="debFundmailShowCount5" class="<%= show5Class %>" >5</span>
              <span id="debFundmailShowCount10" class="<%= show10Class %>" >10</span>
              <span id="debFundmailShowCount20" class="<%= show20Class %>" >20</span>
              <span><%=resMgr.getText("Home.ShowCountBetweenText", TradePortalConstants.TEXT_BUNDLE)%></span>
              <span id="homeDebitFundMailGrid_totalCount">0</span>
            </span>
            <span id="homeDebitFundMailGridEdit" class="gridHeaderEdit"></span>
            <%=widgetFactory.createHoverHelp("homeDebitFundMailGridEdit", "CustomizeListImageLinkHoverText") %>
          </span>
          <%--<div style="clear:both;"></div>--%>
        </div>
        <div>
  <%    //build mail options
        String currentDebFundMailOption = EncryptDecrypt.encryptStringUsingTripleDes(MY_MAIL_AND_UNASSIGNED, userSession.getSecretKey());

        StringBuffer debFundMailOrgOptions = new StringBuffer();
        // If the user has rights to view child organization mail, retrieve the list of dropdown
        // options from the DatabaseQueryBean results doc (containing an option for each child org);
        // otherwise, simply use the user's org option to build the dropdown list (in addition
        // to the default 'Me' or 'Me and Unassigned' option).
        debFundMailOrgOptions.append("<option value=\"").
          append(EncryptDecrypt.encryptStringUsingTripleDes(MY_MAIL_AND_UNASSIGNED, userSession.getSecretKey())).
          append("\" selected >");
        debFundMailOrgOptions.append(resMgr.getText( MY_MAIL_AND_UNASSIGNED, TradePortalConstants.TEXT_BUNDLE));
        debFundMailOrgOptions.append("</option>");
        if (SecurityAccess.hasRights(loginRights, SecurityAccess.VIEW_CHILD_ORG_MESSAGES)) {
           debFundMailOrgOptions.append(Dropdown.getUnsortedOptions(hierarchyDoc, "ORGANIZATION_OID", "NAME",
                                                             currentDebFundMailOption, userSession.getSecretKey()));
           // Only display the 'All Mail' option if the user's org has child organizations
           if (totalOrganizations > 1) {
              debFundMailOrgOptions.append("<option value=\"").
                append(EncryptDecrypt.encryptStringUsingTripleDes(ALL_MAIL, userSession.getSecretKey())).
                append("\"");
              if (currentDebFundMailOption.equals(ALL_MAIL)) {
                 debFundMailOrgOptions.append(" selected");
              }
              debFundMailOrgOptions.append(">");
              debFundMailOrgOptions.append(resMgr.getText( ALL_MAIL, TradePortalConstants.TEXT_BUNDLE));
              debFundMailOrgOptions.append("</option>");
           }
        }
        else {
           debFundMailOrgOptions.append("<option value=\"").
             append(EncryptDecrypt.encryptStringUsingTripleDes(userOrgOid, userSession.getSecretKey())).
             append("\"");
           if (currentDebFundMailOption.equals(userOrgOid)) {
              debFundMailOrgOptions.append(" selected");
           }
           debFundMailOrgOptions.append(">");
           debFundMailOrgOptions.append(userSession.getOrganizationName());
           debFundMailOrgOptions.append("</option>");
        }
        
        String datePattern = userSession.getDatePattern();
        String dateWidgetOptions = "placeHolder:'" + datePattern + "'"; 
        String currency = "";
        String ccyOptions = Dropdown.createSortedRefDataOptions(TradePortalConstants.CURRENCY_CODE, currency, loginLocale);
  %>
		<%= widgetFactory.createSearchSelectField( "DebitFundMailOrg", "Home.Mail.Show", "", debFundMailOrgOptions.toString()) %>

	  <%
  
        StringBuffer dropdownDebitOptions = new StringBuffer();
        boolean userCanFilterOrg = true;
      
        dropdownDebitOptions.append("<option value='"+ALL_DEBITNOTIFRUN+"' selected='true' >");
        dropdownDebitOptions.append(resMgr.getText( ALL_DEBITNOTIFRUN, TradePortalConstants.TEXT_BUNDLE));
        dropdownDebitOptions.append("</option>");
     
        dropdownDebitOptions.append("<option value='N' selected='true' >");
        dropdownDebitOptions.append(resMgr.getText( READ_DEBITNOTIFS, TradePortalConstants.TEXT_BUNDLE));
        dropdownDebitOptions.append("</option>");
      	
        dropdownDebitOptions.append("<option value='Y' selected='true' >");
        dropdownDebitOptions.append(resMgr.getText( UNREAD_DEBITNOTIFS, TradePortalConstants.TEXT_BUNDLE));
        dropdownDebitOptions.append("</option>");
      %>  
		<%= widgetFactory.createSearchSelectField( "DebitReadUnread", "Home.Notifications.debit.Read/Unread", "", dropdownDebitOptions.toString()) %>
		
		<%=widgetFactory.createSubLabel("Home.DebitFundingNotifications.Section.PaymentDate")%>
		<%=widgetFactory.createSearchDateField("PaymentDateFrom","Home.DebitFundingNotifications.PaymentDateFrom","class='char10'",dateWidgetOptions) %>
		<%=widgetFactory.createSearchDateField("PaymentDateTo","Home.DebitFundingNotifications.PaymentDateTo","class='char10'",dateWidgetOptions) %>
		<%=widgetFactory.createSearchSelectField("DebFundCcy", "Home.DebitFundingNotifications.CCY", " ", ccyOptions,"class='char15'") %>
        </div>
  <%
  	homeDebFundMailGridFlag = true;
  	homeDebFundMailGridSectionIdx = sectionIdx;
  	gridHtml = dGridFactory.createDataGrid("homeDebitFundMailGrid","HomeDebitFundingMailMsgDataGrid",null);
  %>
        <%= gridHtml %>
      </div>
  <%
        if (focusField==null) {
          focusField = "DebitFundMailOrg";
        }
      }
  //CR 913 - Rel 9.0 - vdesingu - End


    /************************************************************************/
    /*                           ALL TRANSACTIONS SECTION                   */
    /************************************************************************/

//  //jgadela R92 IR T36000037116 - All transacction is not showing when user is having only access to Payables - So added payable condition
   // if ( TradePortalConstants.DASHBOARD_SECTION__ALL_TRANSACTIONS.equals(sectionId) &&
   //      condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_TRANSACTIONS ) ) {
    	if (	TradePortalConstants.DASHBOARD_SECTION__ALL_TRANSACTIONS.equals(sectionId) &&
        ( (org.allowSomeTradeTransaction() &&
           SecurityAccess.canViewTradeInstrument(loginRights)) ||
          (org.allowSomePaymentTransaction() &&
           SecurityAccess.canViewCashManagement(loginRights))
           ||
          (org.allowPayablesTransaction())
        ) ) {
    	if(searchQueryMap!=null && !searchQueryMap.isEmpty()){
     		 savedSearchQueryDataAT = (Map)searchQueryMap.get("AllTransactionsDataView");
     	}
%>
    <div class="formContentNoSidebar">
      <div class="gridHeader">
        <span class="gridHeaderTitle">
<%    String goToAllTransactionsHome = "goToAllTransactionsHome"; %>
	  <a id="allTranLink" href="<%= formMgr.getLinkAsUrl(goToAllTransactionsHome, response) %>">
            <%=resMgr.getText("Home.AllTransactions", TradePortalConstants.TEXT_BUNDLE)%>
          </a>
          <%=widgetFactory.createHoverHelp("allTranLink", "Home.AllTranLink") %>
        </span>
        <span class="gridHeader-right">
          <span class="gridHeaderShowCounts">
            <span><%=resMgr.getText("Home.ShowCountPreText", TradePortalConstants.TEXT_BUNDLE)%></span>
<%
      String show5Class = "gridHeaderShowCountItem";
      String show10Class = "gridHeaderShowCountItem";
      String show20Class = "gridHeaderShowCountItem";
      if ( gridShowCount == 5 ) {
        show5Class += " selected";
      }
      else if ( gridShowCount == 10 ) {
        show10Class += " selected";
      }
      else if ( gridShowCount == 10 ) {
        show20Class += " selected";
      }
%>
            <span id="allTranShowCount5" class="<%= show5Class %>" >5</span>
            <span id="allTranShowCount10" class="<%= show10Class %>" >10</span>
            <span id="allTranShowCount20" class="<%= show20Class %>" >20</span>
            <span><%=resMgr.getText("Home.ShowCountBetweenText", TradePortalConstants.TEXT_BUNDLE)%></span>
            <span id="homeAllTranGrid_totalCount">0</span>
          </span>
          <span id="homeAllTranGridEdit" class="gridHeaderEdit"></span>
          <%=widgetFactory.createHoverHelp("homeAllTranGridEdit", "CustomizeListImageLinkHoverText") %>
        </span>
        <%--<div style="clear:both;"></div>--%>
      </div>

      <div>
<%    //build alltran options
      String currentAllTranWork = MY_WORK;
      String currentAllTranGroup = TradePortalConstants.INSTR_GROUP__ALL;
      String currentAllTranInstrType = ALL;
      String currentAllTranStatus = TradePortalConstants.STATUS_ALL;

      StringBuffer allTranWork = new StringBuffer();
      // If the user has rights to view child organization work, retrieve the
      // list of dropdown options from the query listview results doc (containing
      // an option for each child org) otherwise, simply use the user's org
      // option to the dropdown list (in addition to the default 'My Work' option).
// IR NIUN012952035
	 StringBuffer dropdownOptions = new StringBuffer();
	 StringBuffer prependText = new StringBuffer();
		prependText.append(resMgr.getText("PendingTransactions.WorkFor", TradePortalConstants.TEXT_BUNDLE));
		prependText.append(" ");
	
	 	String defaultValue = "";
	    String defaultText  = "";
	    
	    selectedWorkflow = request.getParameter("workflow");
	      if (selectedWorkflow == null)
	      {
	         selectedWorkflow = (String) session.getAttribute("workflow");
	         if (selectedWorkflow == null)
	         {
	            if (defaultWIPView.equals(TradePortalConstants.WIP_VIEW_MY_ORG))
	            {
	               selectedWorkflow = userOrgOid;
	            }
	            else if (defaultWIPView.equals(TradePortalConstants.WIP_VIEW_MY_ORG_CHILDREN) && totalOrganizations > 1) //Rel8.3 IR T36000022410 - adding additional condition to check if totalOrganizations >1
	            {
	               selectedWorkflow = ALL_WORK;
	            }
	            else
	            {
	               selectedWorkflow = MY_WORK;
	            }
	            selectedWorkflow = EncryptDecrypt.encryptStringUsingTripleDes(selectedWorkflow, userSession.getSecretKey());
	         }
	      }

	      session.setAttribute("workflow", selectedWorkflow);

	      selectedWorkflow = EncryptDecrypt.decryptStringUsingTripleDes(selectedWorkflow, userSession.getSecretKey());   
	    if(!userSession.hasSavedUserSession())
	     { 
	      defaultValue = EncryptDecrypt.encryptStringUsingTripleDes(MY_WORK, userSession.getSecretKey());
	       defaultText = MY_WORK;
	       
	       dropdownOptions.append("<option value=\"");
	       dropdownOptions.append(defaultValue);
	       dropdownOptions.append("\"");
	       
	       if (selectedWorkflow.equals(defaultText))
	       {
	          dropdownOptions.append(" selected");
	       }
	
	       dropdownOptions.append(">");
	       dropdownOptions.append(resMgr.getText( defaultText, TradePortalConstants.TEXT_BUNDLE));
	       dropdownOptions.append("</option>");
	     }

        // If the user has rights to view child organization work, retrieve the 
        // list of dropdown options from the query listview results doc (containing
        // an option for each child org) otherwise, simply use the user's org 
        // option to the dropdown list (in addition to the default 'My Work' option).

        if (SecurityAccess.hasRights(loginRights, 
                                     SecurityAccess.VIEW_CHILD_ORG_WORK))
        {
           dropdownOptions.append(Dropdown.getUnsortedOptions(hierarchyDoc, 
                                                               "ORGANIZATION_OID", "NAME", 
                                                               prependText.toString(),
                                                               selectedWorkflow, userSession.getSecretKey()));
           // Only display the 'All Work' option if the user's org has child organizations
           if (totalOrganizations > 1)
           {
              dropdownOptions.append("<option value=\"");
              dropdownOptions.append(EncryptDecrypt.encryptStringUsingTripleDes(ALL_WORK, userSession.getSecretKey()));
              dropdownOptions.append("\"");

              if (selectedWorkflow.equals(ALL_WORK))
              {
                 dropdownOptions.append(" selected");
              }

              dropdownOptions.append(">");
              dropdownOptions.append(resMgr.getText( ALL_WORK, TradePortalConstants.TEXT_BUNDLE));
              dropdownOptions.append("</option>");
           }
        }
        else
        {  dropdownOptions.append("<option value=\"");
           dropdownOptions.append(EncryptDecrypt.encryptStringUsingTripleDes(userOrgOid, userSession.getSecretKey()));
           dropdownOptions.append("\"");

           if (selectedWorkflow.equals(userOrgOid))
           {
              dropdownOptions.append(" selected");
           }
           dropdownOptions.append(">");
           dropdownOptions.append(prependText.toString());
           dropdownOptions.append(userSession.getOrganizationName());
           dropdownOptions.append("</option>");
        }
	 //IR NIUN012952035

%>
        <%= widgetFactory.createSearchSelectField("AllTranWork","Home.AllTransactions.Show", "",
        		dropdownOptions.toString(), " class=\"char15\"" )%>

<%
      StringBuffer allTranGroups = new StringBuffer();
        allTranGroups.append("<option value='").
        append(TradePortalConstants.INSTR_GROUP__ALL).
        append("'>").
        append(resMgr.getText( ALL, TradePortalConstants.TEXT_BUNDLE)).
	    append("</option>");
    	//RPasupulati #IR T36000014543 Starts.
      if(condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_NEW_TRADE_INSTRUMENTS)){
         allTranGroups.append("<option value='").
         append(TradePortalConstants.INSTR_GROUP__TRADE).
         append("'>").
         append(refData.getDescr("INSTRUMENT_GROUP", TradePortalConstants.INSTR_GROUP__TRADE, loginLocale)).
         append("</option>");
		}

	   if(condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_NEW_PAYMENT_INSTRUMENTS)){
          allTranGroups.append("<option value='").
          append(TradePortalConstants.INSTR_GROUP__PAYMENT).
          append("'>").
          append(refData.getDescr("INSTRUMENT_GROUP", TradePortalConstants.INSTR_GROUP__PAYMENT, loginLocale)).
          append("</option>");
		}

	   if(condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_NEW_DIRECT_DEBIT_INSTRUMENTS)){
          allTranGroups.append("<option value='").
          append(TradePortalConstants.INSTR_GROUP__DIRECT_DEBIT).
          append("'>").
          append(refData.getDescr("INSTRUMENT_GROUP", TradePortalConstants.INSTR_GROUP__DIRECT_DEBIT, loginLocale)).
          append("</option>");
		}

        if(condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_RECEIVABLE_TRANSACTIONS)){
           allTranGroups.append("<option value='").
           append(TradePortalConstants.INSTR_GROUP__RECEIVABLES).
           append("'>").
           append(refData.getDescr("INSTRUMENT_GROUP", TradePortalConstants.INSTR_GROUP__RECEIVABLES, loginLocale)).
           append("</option>");
          }
      //RPasupulati #IR T36000014543 Ends.

%>
        <%= widgetFactory.createSearchSelectField( "AllTranGroup", "Home.AllTransactions.criteria.InstrumentGroup", "",
              allTranGroups.toString(), " class=\"char10\"" )%>

<%


      StringBuffer allTranInstrTypes = new StringBuffer();
      allTranInstrTypes.append("<option value='").append(ALL).append("'").
        append(" selected >").
        append(resMgr.getText( ALL, TradePortalConstants.TEXT_BUNDLE)).
        append("</option>");
      //popupate the transaction type drop down with all instrument types based on what the user can see
      Vector viewableInstruments = Dropdown.getViewableInstrumentTypes(formMgr,
        new Long(userOrgOid), loginRights, userSession.getOwnershipLevel(), new Long(userOid),"AllTranInstrType" );
      String viewableInstrTypes = Dropdown.getInstrumentList("",  //selected instr type
        loginLocale, viewableInstruments );
      allTranInstrTypes.append(viewableInstrTypes);
      //todo: all add to top
      //this is labelled tran type, but it is the setRwi of instrument types

%>
        <%= widgetFactory.createSearchSelectField( "AllTranInstrType", "Home.AllTransactions.criteria.InstrumentType", "",
              allTranInstrTypes.toString()) %>

<%
      //cquinton 12/7/2012 per bank design call, the default for home all transactions should be 'Started'
      StringBuffer allTranStatus = new StringBuffer();
      allTranStatus.append("<option value='").append(ALL).append("'").
        append(">").
        append(resMgr.getText( ALL, TradePortalConstants.TEXT_BUNDLE)).
        append("</option>");
      allTranStatus.append("<option value='").
        append(TradePortalConstants.TRANS_STATUS_AUTHORIZE_FAILED).
        append("'>").
        append(refData.getDescr("TRANSACTION_STATUS", TradePortalConstants.TRANS_STATUS_AUTHORIZE_FAILED, loginLocale)).
        append("</option>");
      allTranStatus.append("<option value='").
        append(TradePortalConstants.TRANS_STATUS_AUTHORIZED).
        append("'>").
        append(refData.getDescr("TRANSACTION_STATUS", TradePortalConstants.TRANS_STATUS_AUTHORIZED, loginLocale)).
        append("</option>");
      allTranStatus.append("<option value='").
        append(TradePortalConstants.TRANS_STATUS_CANCELLED_BY_BANK).
        append("'>").
        append(refData.getDescr("TRANSACTION_STATUS", TradePortalConstants.TRANS_STATUS_CANCELLED_BY_BANK, loginLocale)).
        append("</option>");
      
      //Sandeep 02/28/2013 Comment -Start
      //Removing Delete option from Status dropdown box. This Code change is Per Kim/Clay as it is a BMO Issue
      //allTranStatus.append("<option value='").
        //append(TradePortalConstants.TRANS_STATUS_DELETED).
        //append("'>").
        //append(refData.getDescr("TRANSACTION_STATUS", TradePortalConstants.TRANS_STATUS_DELETED, loginLocale)).
        //append("</option>");
      //Sandeep 02/28/2013 Comment -End
      
      if(hasFTBARights || hasFTDPRights){
	      allTranStatus.append("<option value='").
	        append(TradePortalConstants.TRANS_STATUS_PARTIALLY_REJECTED_PAYMENT).
	        append("'>").
	        append(refData.getDescr("TRANSACTION_STATUS", TradePortalConstants.TRANS_STATUS_PARTIALLY_REJECTED_PAYMENT, loginLocale)).
	        append("</option>");
      }
      allTranStatus.append("<option value='").
        append(TradePortalConstants.TRANS_STATUS_PROCESSED_BY_BANK).
        append("'>").
        append(refData.getDescr("TRANSACTION_STATUS", TradePortalConstants.TRANS_STATUS_PROCESSED_BY_BANK, loginLocale)).
        append("</option>");
      allTranStatus.append("<option value='").
        append(TradePortalConstants.TRANS_STATUS_PARTIALLY_AUTHORIZED).
        append("'>").
        append(refData.getDescr("TRANSACTION_STATUS", TradePortalConstants.TRANS_STATUS_PARTIALLY_AUTHORIZED, loginLocale)).
        append("</option>");
      allTranStatus.append("<option value='").
        append(TradePortalConstants.TRANS_STATUS_READY_TO_AUTHORIZE).
        append("'>").
        append(refData.getDescr("TRANSACTION_STATUS", TradePortalConstants.TRANS_STATUS_READY_TO_AUTHORIZE, loginLocale)).
        append("</option>");
      allTranStatus.append("<option value='").
        append(TradePortalConstants.TRANS_STATUS_REJECTED_BY_BANK).
        append("'>").
        append(refData.getDescr("TRANSACTION_STATUS", TradePortalConstants.TRANS_STATUS_REJECTED_BY_BANK, loginLocale)).
        append("</option>");
      
    //jgadela Rel-8.3 CR-821 Dev 06/13/2013 - Start
      allTranStatus.append("<option value='").
        append(TradePortalConstants.STATUS_READY_TO_CHECK).
        append("' >").
        append(refData.getDescr("TRANSACTION_STATUS", TradePortalConstants.STATUS_READY_TO_CHECK, loginLocale)).
        append("</option>");
      allTranStatus.append("<option value='").
        append(TradePortalConstants.STATUS_REPAIR).
        append("' >").
        append(refData.getDescr("TRANSACTION_STATUS", TradePortalConstants.STATUS_REPAIR, loginLocale)).
        append("</option>");
     //jgadela Rel-8.3 CR-821 Dev 06/13/2013 - End
      
      if(hasFTBARights || hasFTDPRights){
	      allTranStatus.append("<option value='").
	        append(TradePortalConstants.TRANS_STATUS_REJECTED_PAYMENT).
	        append("'>").
	        append(refData.getDescr("TRANSACTION_STATUS", TradePortalConstants.TRANS_STATUS_REJECTED_PAYMENT, loginLocale)).
	        append("</option>");
	      allTranStatus.append("<option value='").
	      	append(TradePortalConstants.TRANS_STATUS_VERIFIED).
	      	append("'>").
	      	append(refData.getDescr("TRANSACTION_STATUS", TradePortalConstants.TRANS_STATUS_VERIFIED, loginLocale)).
	      	append("</option>");
	      allTranStatus.append("<option value='").
	      	append(TradePortalConstants.TRANS_STATUS_VERIFIED_PENDING_FX).
	      	append("'>").
	      	append(refData.getDescr("TRANSACTION_STATUS", TradePortalConstants.TRANS_STATUS_VERIFIED_PENDING_FX, loginLocale)).
	      	append("</option>");
	      allTranStatus.append("<option value='").
	      	append(TradePortalConstants.TRANS_STATUS_FX_THRESH_EXCEEDED).
	      	append("'>").
	      	append(refData.getDescr("TRANSACTION_STATUS", TradePortalConstants.TRANS_STATUS_FX_THRESH_EXCEEDED, loginLocale)).
	      	append("</option>");
	      allTranStatus.append("<option value='").
	      	append(TradePortalConstants.TRANS_STATUS_AUTH_PEND_MARKET_RATE).
	      	append("'>").
	      	append(refData.getDescr("TRANSACTION_STATUS", TradePortalConstants.TRANS_STATUS_AUTH_PEND_MARKET_RATE, loginLocale)).
	     	append("</option>");
      } 
      allTranStatus.append("<option value='").
        append(TradePortalConstants.TRANS_STATUS_STARTED).
        append("' selected >").
        append(refData.getDescr("TRANSACTION_STATUS", TradePortalConstants.TRANS_STATUS_STARTED, loginLocale)).
        append("</option>");
      
%>
        <%=widgetFactory.createSearchSelectField( "AllTranStatus", "Home.AllTransactions.criteria.Status", "",
              allTranStatus.toString(), " class=\"char15\"" )%>
              <%-- Jaya Mamidala CR Start--%>  

      </div>

<%
	homeAllTranGridFlag = true;
	homeAllTranGridSectionIdx = sectionIdx;
      gridHtml = dGridFactory.createDataGrid("homeAllTranGrid","HomeAllTransactionsDataGrid",null);
%>
      <%= gridHtml %>
    </div>
<%
      if (focusField==null) {
        focusField = "AllTranWork";
      }
    } //end if notifications section

    /************************************************************************/
    /*                           PANEL AUTHORISATION TRANSACTIONS SECTION                   */
    /************************************************************************/
	//jgadela 08/09/2013 REL 8.3 CR-821 - T36000019559, modified the condition check parameter - DISPLAY_PANEL_AUTHORISATION_TRANSACTIONS 
    if ( TradePortalConstants.DASHBOARD_SECTION__PANEL_AUTH_TRANSACTIONS.equals(sectionId) &&
         condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_PANEL_AUTHORISATION_TRANSACTIONS ) ) {
    	if(searchQueryMap!=null && !searchQueryMap.isEmpty()){
      		 savedSearchQueryDataPA = (Map)searchQueryMap.get("PanelAuthTransactionsDataView");
      	}
%>
    <div class="formContentNoSidebar">
      <div class="gridHeader">
        <span class="gridHeaderTitle">
<%    String goToPendingPanelAuthTransactionsHome = "goToPendingPanelAuthTransactionsHome"; %>
	  <a id="panelAuthTranLink" href="<%= formMgr.getLinkAsUrl(goToPendingPanelAuthTransactionsHome, response) %>">
            <%=resMgr.getText("Home.PanelAuthTransactions", TradePortalConstants.TEXT_BUNDLE)%>
          </a>
          <%=widgetFactory.createHoverHelp("panelAuthTranLink", "Home.PendingPanelAuthTranLink") %>
        </span>
        <span class="gridHeader-right">
          <span class="gridHeaderShowCounts">
            <span><%=resMgr.getText("Home.ShowCountPreText", TradePortalConstants.TEXT_BUNDLE)%></span>
<%
      String show5Class = "gridHeaderShowCountItem";
      String show10Class = "gridHeaderShowCountItem";
      String show20Class = "gridHeaderShowCountItem";
      if ( gridShowCount == 5 ) {
        show5Class += " selected";
      }
      else if ( gridShowCount == 10 ) {
        show10Class += " selected";
      }
      else if ( gridShowCount == 10 ) {
        show20Class += " selected";
      }
%>
            <span id="panelAuthTranShowCount5" class="<%= show5Class %>" >5</span>
            <span id="panelAuthTranShowCount10" class="<%= show10Class %>" >10</span>
            <span id="panelAuthTranShowCount20" class="<%= show20Class %>" >20</span>
            <span><%=resMgr.getText("Home.ShowCountBetweenText", TradePortalConstants.TEXT_BUNDLE)%></span>
            <span id="homePanelAuthTranGrid_totalCount">0</span>
          </span>
          <span id="homePanelAuthTranGridEdit" class="gridHeaderEdit"></span>
          <%=widgetFactory.createHoverHelp("homePanelAuthTranGridEdit", "CustomizeListImageLinkHoverText") %>
        </span>
        <%--<div style="clear:both;"></div>--%>
      </div>

      <div>
       <%    //build panelAuthtran options
      String currentpanelAuthTranWork = MY_WORK;
      String currentpanelAuthTranGroup = TradePortalConstants.INSTR_GROUP__ALL;
      String currentpanelAuthTranInstrType = ALL;
      String currentpanelAuthTranStatus = TradePortalConstants.STATUS_ALL;

      StringBuffer panelAuthTranWork = new StringBuffer();
      // If the user has rights to view child organization work, retrieve the
      // list of dropdown options from the query listview results doc (containing
      // an option for each child org) otherwise, simply use the user's org
      // option to the dropdown list (in addition to the default 'My Work' option).
// IR NIUN012952035
	 StringBuffer panelAuthdropdownOptions = new StringBuffer();
	 StringBuffer prependText = new StringBuffer();
		prependText.append(resMgr.getText("PendingTransactions.WorkFor", TradePortalConstants.TEXT_BUNDLE));
		prependText.append(" ");
	
	 	String defaultValue = "";
	    String defaultText  = "";
	    
	    selectedWorkflow = request.getParameter("workflow");
	      if (selectedWorkflow == null)
	      {
	         selectedWorkflow = (String) session.getAttribute("workflow");
	         if (selectedWorkflow == null)
	         {
	            if (defaultWIPView.equals(TradePortalConstants.WIP_VIEW_MY_ORG))
	            {
	               selectedWorkflow = userOrgOid;
	            }
	            else if (defaultWIPView.equals(TradePortalConstants.WIP_VIEW_MY_ORG_CHILDREN))
	            {
	               selectedWorkflow = ALL_WORK;
	            }
	            else
	            {
	               selectedWorkflow = MY_WORK;
	            }
	            selectedWorkflow = EncryptDecrypt.encryptStringUsingTripleDes(selectedWorkflow, userSession.getSecretKey());
	         }
	      }

	      session.setAttribute("workflow", selectedWorkflow);

	      selectedWorkflow = EncryptDecrypt.decryptStringUsingTripleDes(selectedWorkflow, userSession.getSecretKey());   
	    if(!userSession.hasSavedUserSession())
	     { 
	      defaultValue = EncryptDecrypt.encryptStringUsingTripleDes(MY_WORK, userSession.getSecretKey());
	       defaultText = MY_WORK;
	       
	       panelAuthdropdownOptions.append("<option value=\"");
	       panelAuthdropdownOptions.append(defaultValue);
	       panelAuthdropdownOptions.append("\"");
	       
	       if (selectedWorkflow.equals(defaultText))
	       {
	    	   panelAuthdropdownOptions.append(" selected");
	       }
	
	       panelAuthdropdownOptions.append(">");
	       panelAuthdropdownOptions.append(resMgr.getText( defaultText, TradePortalConstants.TEXT_BUNDLE));
	       panelAuthdropdownOptions.append("</option>");
	     }

        // If the user has rights to view child organization work, retrieve the 
        // list of dropdown options from the query listview results doc (containing
        // an option for each child org) otherwise, simply use the user's org 
        // option to the dropdown list (in addition to the default 'My Work' option).

        if (SecurityAccess.hasRights(loginRights, 
                                     SecurityAccess.VIEW_CHILD_ORG_WORK))
        {
        	panelAuthdropdownOptions.append(Dropdown.getUnsortedOptions(hierarchyDoc, 
                                                               "ORGANIZATION_OID", "NAME", 
                                                               prependText.toString(),
                                                               selectedWorkflow, userSession.getSecretKey()));
           // Only display the 'All Work' option if the user's org has child organizations
           if (totalOrganizations > 1)
           {
        	   panelAuthdropdownOptions.append("<option value=\"");
        	   panelAuthdropdownOptions.append(EncryptDecrypt.encryptStringUsingTripleDes(ALL_WORK, userSession.getSecretKey()));
        	   panelAuthdropdownOptions.append("\"");

              if (selectedWorkflow.equals(ALL_WORK))
              {
                 panelAuthdropdownOptions.append(" selected");
              }

              panelAuthdropdownOptions.append(">");
              panelAuthdropdownOptions.append(resMgr.getText( ALL_WORK, TradePortalConstants.TEXT_BUNDLE));
              panelAuthdropdownOptions.append("</option>");
           }
        }
        else
        {   panelAuthdropdownOptions.append("<option value=\"");
        	panelAuthdropdownOptions.append(EncryptDecrypt.encryptStringUsingTripleDes(userOrgOid, userSession.getSecretKey()));
        	panelAuthdropdownOptions.append("\"");

           if (selectedWorkflow.equals(userOrgOid))
           {
        	   panelAuthdropdownOptions.append(" selected");
           }
           panelAuthdropdownOptions.append(">");
           panelAuthdropdownOptions.append(prependText.toString());
           panelAuthdropdownOptions.append(userSession.getOrganizationName());
           panelAuthdropdownOptions.append("</option>");
        }
	 //IR NIUN012952035

%>
        <%= widgetFactory.createSearchSelectField("PanelAuthTranWork","Home.PanelAuthTransactions.Show", "",
        		panelAuthdropdownOptions.toString(), " class=\"char10\"" )%>

<%
	// IR T36000019769 Rel 8.3 START
      String panelAuthTranGroups =null;
	  panelAuthTranGroups = Dropdown.createSortedRefDataOptions("PANEL_INST_GROUP",TradePortalConstants.PANEL_INSTR_GROUP__ALL, loginLocale);
	// IR T36000019769 Rel 8.3 END      

%>
        <%= widgetFactory.createSearchSelectField( "PanelAuthTranGroup", "Home.PanelAuthTransactions.criteria.InstrumentGroup", "",
              panelAuthTranGroups, " class=\"char10\"" )%>

<%


      StringBuffer panelAuthTranInstrTypes = new StringBuffer();
      panelAuthTranInstrTypes.append("<option value='").append(ALL).append("'").
        append(" selected >").
        append(resMgr.getText( ALL, TradePortalConstants.TEXT_BUNDLE)).
        append("</option>");
      //popupate the transaction type drop down with all instrument types based on what the user can see
      Vector viewableInstruments = Dropdown.getViewableInstrumentTypes(formMgr,
        new Long(userOrgOid), loginRights, userSession.getOwnershipLevel(), new Long(userOid));
      String viewableInstrTypes = Dropdown.getInstrumentList("",  //selected instr type
        loginLocale, viewableInstruments );
      panelAuthTranInstrTypes.append(viewableInstrTypes);
      //todo: all add to top
      //this is labelled tran type, but it is the set of instrument types
%>
        <%= widgetFactory.createSearchSelectField( "PanelAuthTranInstrType", "Home.PanelAuthTransactions.criteria.InstrumentType", "",
              panelAuthTranInstrTypes.toString()) %>

<%
      //cquinton 12/7/2012 per bank design call, the default for home all transactions should be 'Started'
      StringBuffer panelAuthTranStatus = new StringBuffer();
      panelAuthTranStatus.append("<option value='").append(ALL).append("'").
        append(">").
        append(resMgr.getText( ALL, TradePortalConstants.TEXT_BUNDLE)).
        append("</option>");
      panelAuthTranStatus.append("<option value='").
        append(TradePortalConstants.TRANS_STATUS_AUTHORIZE_FAILED).
        append("'>").
        append(refData.getDescr("TRANSACTION_STATUS", TradePortalConstants.TRANS_STATUS_AUTHORIZE_FAILED, loginLocale)).
        append("</option>");
      panelAuthTranStatus.append("<option value='").
        append(TradePortalConstants.TRANS_STATUS_PARTIALLY_AUTHORIZED).
        append("'>").
        append(refData.getDescr("TRANSACTION_STATUS", TradePortalConstants.TRANS_STATUS_PARTIALLY_AUTHORIZED, loginLocale)).
        append("</option>");
      panelAuthTranStatus.append("<option value='").
        append(TradePortalConstants.TRANS_STATUS_READY_TO_AUTHORIZE).
        append("'>").
        append(refData.getDescr("TRANSACTION_STATUS", TradePortalConstants.TRANS_STATUS_READY_TO_AUTHORIZE, loginLocale)).
        append("</option>");
      
      StringBuffer panelAuthTransactions = new StringBuffer();
      panelAuthTransactions.append("<option value='").append(ALL_WORK).append("'").
        append(">").
        append(resMgr.getText( ALL_WORK, TradePortalConstants.TEXT_BUNDLE)).
        append("</option>");
      panelAuthTransactions.append("<option value='").append(MY_WORK).
        append("'>").
        append(resMgr.getText(MY_WORK, TradePortalConstants.TEXT_BUNDLE)).
        append("</option>");
      
%>
        <%=widgetFactory.createSearchSelectField( "PanelAuthTransactions", "Home.PanelAuthTransactions.criteria.MyTransactions", "",
              panelAuthTransactions.toString(), " class=\"char10\"" )%>
      </div>

<%
      gridHtml = dGridFactory.createDataGrid("homePanelAuthTranGrid","HomePanelAuthTransactionsDataGrid",null);
%>
      <%= gridHtml %>
    </div>
<%
      if (focusField==null) {
        focusField = "PanelAuthTranWork";
      }
    } //end if PanelAuth section
    
    /************************************************************************/
    /*                  RECEIVABLES MATCH NOTICES SECTION                   */
    /************************************************************************/
    if ( TradePortalConstants.DASHBOARD_SECTION__RCV_MATCH_NOTICES.equals(sectionId) &&
         condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_RECEIVABLE_TRANSACTIONS ) ) {
    	if(searchQueryMap!=null && !searchQueryMap.isEmpty()){
   		 savedSearchQueryDataRM = (Map)searchQueryMap.get("HomeReceivablesMatchingDataView");
   	}
%>
    <div class="formContentNoSidebar">
      <div class="gridHeader">
        <span class="gridHeaderTitle">
<%
      //cquinton 5/22/2013 Rel 8.2 this link supposed to go to match notices page
      String goToReceivableTransactionsHome = "goToReceivableTransactionsHome";
      String receivableMatchUrlParm = "&current2ndNav=" + TradePortalConstants.NAV__RECEIVABLE_MATCH_NOTICES;
%>
	  <a id="rcvMatchLink" href="<%= formMgr.getLinkAsUrl(goToReceivableTransactionsHome, receivableMatchUrlParm, response) %>">
            <%=resMgr.getText("Home.ReceivablesMatching", TradePortalConstants.TEXT_BUNDLE)%>
          </a>
          <%=widgetFactory.createHoverHelp("rcvMatchLink", "Home.ReceivablesMatchingLink") %>
        </span>
        <span class="gridHeader-right">
          <span class="gridHeaderShowCounts">
            <span><%=resMgr.getText("Home.ShowCountPreText", TradePortalConstants.TEXT_BUNDLE)%></span>
<%
      String show5Class = "gridHeaderShowCountItem";
      String show10Class = "gridHeaderShowCountItem";
      String show20Class = "gridHeaderShowCountItem";
      if ( gridShowCount == 5 ) {
        show5Class += " selected";
      }
      else if ( gridShowCount == 10 ) {
        show10Class += " selected";
      }
      else if ( gridShowCount == 10 ) {
        show20Class += " selected";
      }
%>
            <span id="rcvTranShowCount5" class="<%= show5Class %>" >5</span>
            <span id="rcvTranShowCount10" class="<%= show10Class %>" >10</span>
            <span id="rcvTranShowCount20" class="<%= show20Class %>" >20</span>
            <span><%=resMgr.getText("Home.ShowCountBetweenText", TradePortalConstants.TEXT_BUNDLE)%></span>
            <span id="homeRcvTranGrid_totalCount">0</span>
          </span>
          <span id="homeRcvTranGridEdit" class="gridHeaderEdit"></span>
          <%=widgetFactory.createHoverHelp("homeRcvTranGridEdit", "CustomizeListImageLinkHoverText") %>
        </span>
        <%--<div style="clear:both;"></div>--%>
      </div>


      <div>
<%    //build receivables match options
      String currentRcvMatchWork = MY_WORK;
      String currentRcvMatchStatus = TradePortalConstants.STATUS_ALL;
      String currentRcvMatchPaySrc = ALL;

      StringBuffer rcvMatchWork = new StringBuffer();
      // If the user has rights to view child organization work, retrieve the
      // list of dropdown options from the query listview results doc (containing
      // an option for each child org) otherwise, simply use the user's org
      // option to the dropdown list (in addition to the default 'My Work' option).
      if (!userSession.hasSavedUserSession()) {
        rcvMatchWork.append("<option value=\"");
        rcvMatchWork.append(EncryptDecrypt.encryptStringUsingTripleDes(MY_WORK, userSession.getSecretKey()));
        rcvMatchWork.append("\"");
        rcvMatchWork.append(" selected");
        rcvMatchWork.append(">");
        rcvMatchWork.append(resMgr.getText(MY_WORK, TradePortalConstants.TEXT_BUNDLE));
        rcvMatchWork.append("</option>");
      }
      if (SecurityAccess.hasRights(loginRights,
                                   SecurityAccess.VIEW_CHILD_ORG_RECEIVABLES)) {
        rcvMatchWork.append(Dropdown.getUnsortedOptions(hierarchyDoc,
                                                   "ORGANIZATION_OID", "NAME",
                                                   "", "", userSession.getSecretKey()));
        // Only display the 'All Work' option if the user's org has child organizations
        if (totalOrganizations > 1) {
          rcvMatchWork.append("<option value=\"");
          rcvMatchWork.append(EncryptDecrypt.encryptStringUsingTripleDes(ALL_WORK, userSession.getSecretKey()));
          rcvMatchWork.append("\"");
          if (currentRcvMatchWork.equals(ALL_WORK)) {
            rcvMatchWork.append(" selected");
          }
          rcvMatchWork.append(">");
          rcvMatchWork.append(resMgr.getText(ALL_WORK, TradePortalConstants.TEXT_BUNDLE));
          rcvMatchWork.append("</option>");
        }
      }
      else
      {
        rcvMatchWork.append("<option value=\"");
        rcvMatchWork.append(EncryptDecrypt.encryptStringUsingTripleDes(userOrgOid, userSession.getSecretKey()));
        rcvMatchWork.append("\"");
        if (currentRcvMatchWork.equals(userOrgOid)) {
          rcvMatchWork.append(" selected");
        }
        rcvMatchWork.append(">");
        rcvMatchWork.append(userSession.getOrganizationName());
        rcvMatchWork.append("</option>");
      }
%>
        <%=widgetFactory.createSearchSelectField( "RcvMatchWork", "Home.ReceivablesMatching.Show", "",
              rcvMatchWork.toString(), " class=\"char15\"" )%>

<%
      StringBuffer rcvMatchStatus = new StringBuffer();
      // Build the dropdown options (hardcoded in ALL, STARTED, READY, PARTIALLY_AUTH, AUTH_FAILED), re-selecting the
      // most recently selected option.
      rcvMatchStatus.append("<option value='");
      rcvMatchStatus.append(TradePortalConstants.STATUS_ALL);
      rcvMatchStatus.append("' ");
      if (TradePortalConstants.STATUS_ALL.equals(currentRcvMatchStatus)) {
        rcvMatchStatus.append("selected");
      }
      rcvMatchStatus.append(">");
      rcvMatchStatus.append(resMgr.getText("common.StatusAll", TradePortalConstants.TEXT_BUNDLE));
      rcvMatchStatus.append("</option>");

      rcvMatchStatus.append("<option value='");
      rcvMatchStatus.append(TradePortalConstants.STATUS_PAID);
      rcvMatchStatus.append("' ");
      if (TradePortalConstants.STATUS_PAID.equals(currentRcvMatchStatus)) {
        rcvMatchStatus.append("selected");
      }
      rcvMatchStatus.append(">");
      rcvMatchStatus.append(resMgr.getText("common.StatusPaid", TradePortalConstants.TEXT_BUNDLE));
      rcvMatchStatus.append("</option>");

      rcvMatchStatus.append("<option value='");
      rcvMatchStatus.append(TradePortalConstants.STATUS_PENDING);
      rcvMatchStatus.append("' ");
      if (TradePortalConstants.STATUS_PENDING.equals(currentRcvMatchStatus)) {
        rcvMatchStatus.append("selected");
      }
      rcvMatchStatus.append(">");
      rcvMatchStatus.append(resMgr.getText("common.StatusPending", TradePortalConstants.TEXT_BUNDLE));
      rcvMatchStatus.append("</option>");
%>
        <%=widgetFactory.createSearchSelectField( "RcvMatchStatus", "Home.ReceivablesMatching.criteria.Status", "",
              rcvMatchStatus.toString() )%>

<%
      StringBuffer rcvMatchPaySrc = new StringBuffer();
      rcvMatchPaySrc.append("<option value='");
      rcvMatchPaySrc.append(ALL);
      rcvMatchPaySrc.append("' ");
      if (ALL.equals(currentRcvMatchPaySrc)) {
        rcvMatchPaySrc.append("selected");
      }
      rcvMatchPaySrc.append(">");
      rcvMatchPaySrc.append(resMgr.getText(ALL, TradePortalConstants.TEXT_BUNDLE));
      rcvMatchPaySrc.append("</option>");
      String[] paymentSourceValues = {TradePortalConstants.PAYMENT_SOURCE_TYPE_BPY,
                                      TradePortalConstants.PAYMENT_SOURCE_TYPE_CCD,
                                      TradePortalConstants.PAYMENT_SOURCE_TYPE_DIR,
                                      TradePortalConstants.PAYMENT_SOURCE_TYPE_HPA,
                                      TradePortalConstants.PAYMENT_SOURCE_TYPE_LBX,
                                      TradePortalConstants.PAYMENT_SOURCE_TYPE_RTG,
                                      TradePortalConstants.PAYMENT_SOURCE_TYPE_SMT,
                                      TradePortalConstants.PAYMENT_SOURCE_TYPE_SWF };
      for (int i = 0; i < paymentSourceValues.length; i++) {
        rcvMatchPaySrc.append("<option value='");
        rcvMatchPaySrc.append(paymentSourceValues[i]);
        rcvMatchPaySrc.append("' ");
        if (paymentSourceValues[i].equals(currentRcvMatchPaySrc)) {
          rcvMatchPaySrc.append("selected");
        }
        rcvMatchPaySrc.append(">");
        rcvMatchPaySrc.append(ReferenceDataManager.getRefDataMgr().getDescr(
                              TradePortalConstants.PAYMENT_SOURCE_TYPE,
                              paymentSourceValues[i], loginLocale));
        rcvMatchPaySrc.append("</option>");
      }
%>
        <%=widgetFactory.createSearchSelectField( "RcvMatchPaySrc", "Home.ReceivablesMatching.criteria.PaymentSource", "",
              rcvMatchPaySrc.toString() )%>
      </div>
<%
	homeRcvTranGridFlag = true;  
	homeRcvTranGridSectionIdx = sectionIdx;
	gridHtml = dGridFactory.createDataGrid("homeRcvTranGrid","HomeReceivablesMatchingDataGrid",null);
%>
      <%= gridHtml %>
    </div>
<%
      if (focusField==null) {
        focusField = "RcvMatchWork";
      }
    } //end if receivables match notices section


    /************************************************************************/
    /*                           ACCOUNT BALANCE SECTION                    */
    /************************************************************************/

    //cquinton 2/25/2013 - use DISPLAY_ACCOUNT BALANCES instead of NEW PAYMENT, which is incorrect
    if ( TradePortalConstants.DASHBOARD_SECTION__ACCOUNT_BALANCES.equals(sectionId) &&
         condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_ACCOUNT_BALANCES )) {
    	
    	if(searchQueryMap!=null && !searchQueryMap.isEmpty()){
    		 if (userSession.hasSavedUserSession()) {
   		 savedSearchQueryDataCA = (Map)searchQueryMap.get("CustAccountDataView");
    	}
    		 else
    			 savedSearchQueryDataA = (Map)searchQueryMap.get("AccountDataView");
   	}
%>
    <div class="formContentNoSidebar">
      <div class="gridHeader">
        <span class="gridHeaderTitle">
<%    String goToAccountsHome = "goToAccountsHome"; %>
	  <a id="acctLink" href="<%= formMgr.getLinkAsUrl(goToAccountsHome, response) %>">
            <%=resMgr.getText("Home.AccountBalances", TradePortalConstants.TEXT_BUNDLE)%>
          </a>
          <%=widgetFactory.createHoverHelp("acctLink", "Home.AccountBalancesLink") %>
        </span>
        <span class="gridHeader-right">
          <span class="gridHeaderShowCounts">
            <span><%=resMgr.getText("Home.ShowCountPreText", TradePortalConstants.TEXT_BUNDLE)%></span>
<%
      String show5Class = "gridHeaderShowCountItem";
      String show10Class = "gridHeaderShowCountItem";
      String show20Class = "gridHeaderShowCountItem";
      if ( gridShowCount == 5 ) {
        show5Class += " selected";
      }
      else if ( gridShowCount == 10 ) {
        show10Class += " selected";
      }
      else if ( gridShowCount == 10 ) {
        show20Class += " selected";
      }
%>
            <span id="acctShowCount5" class="<%= show5Class %>" >5</span>
            <span id="acctShowCount10" class="<%= show10Class %>" >10</span>
            <span id="acctShowCount20" class="<%= show20Class %>" >20</span>
            <span><%=resMgr.getText("Home.ShowCountBetweenText", TradePortalConstants.TEXT_BUNDLE)%></span>
            <span id="homeAcctGrid_totalCount">0</span>
          </span>
          <%-- vdesingu Rel8.4 CR-590 : added refresh button--%>
           <span id="homeAcctRefresh" class="searchHeaderRefresh"></span>
           <%=widgetFactory.createHoverHelp("homeAcctRefresh","RefreshImageLinkHoverText") %> 
          
          
          <span id="homeAcctGridEdit" class="gridHeaderEdit"></span>
          <%=widgetFactory.createHoverHelp("homeAcctGridEdit", "CustomizeListImageLinkHoverText") %>
        </span>
        <%--<div style="clear:both;"></div>--%>
      </div>

<%
	homeAcctGridFlag = true;
	homeAcctGridSectionIdx = sectionIdx;
      gridHtml = dGridFactory.createDataGrid("homeAcctGrid","HomeAccountDataGrid",null);
%>
      <%= gridHtml %>
    </div>
<%
    } //end if account balances section


    /************************************************************************/
    /*                           ANNOUNCEMENTS SECTION                      */
    /************************************************************************/

    if ( TradePortalConstants.DASHBOARD_SECTION__ANNOUNCEMENTS.equals(sectionId) &&
         ( TradePortalConstants.OWNER_BANK.equals(userSession.getOwnershipLevel()) ||
           TradePortalConstants.OWNER_BOG.equals(userSession.getOwnershipLevel()) ) ) {
    	if(searchQueryMap!=null && !searchQueryMap.isEmpty()){
    		 savedSearchQueryDataAN = (Map)searchQueryMap.get("HomeAnnouncementsDataView");
    	}
    	
    	boolean viewAnnounceAccess = SecurityAccess.hasRights(loginRights,
                SecurityAccess.VIEW_OR_MAINTAIN_ANNOUNCEMENT);
		boolean maintainAnnounceAccess = SecurityAccess.hasRights(loginRights,
                    SecurityAccess.MAINTAIN_ANNOUNCEMENT);
		
		if(viewAnnounceAccess || maintainAnnounceAccess){	
%>

    <div class="formContentNoSidebar">
      <div class="gridHeader">
        <span class="gridHeaderTitle">
<%    String goToAnnouncements = "goToAnnouncementsHome"; %>
	  <a id="announcementsLink" href="<%= formMgr.getLinkAsUrl(goToAnnouncements, response) %>">
            <%=resMgr.getText("Home.Announcements", TradePortalConstants.TEXT_BUNDLE)%>
          </a>
          <%= widgetFactory.createHoverHelp("announcementsLink","AdminHome.AnnouncementsLink") %>
        </span>
        <span class="gridHeader-right">
          <span class="gridHeaderShowCounts">
            <span><%=resMgr.getText("Home.ShowCountPreText", TradePortalConstants.TEXT_BUNDLE)%></span>
<%
      String show5Class = "gridHeaderShowCountItem";
      String show10Class = "gridHeaderShowCountItem";
      String show20Class = "gridHeaderShowCountItem";
      if ( gridShowCount == 5 ) {
        show5Class += " selected";
      }
      else if ( gridShowCount == 10 ) {
        show10Class += " selected";
      }
      else if ( gridShowCount == 10 ) {
        show20Class += " selected";
      }
%>
            <span id="announceShowCount5" class="<%= show5Class %>" >5</span>
            <span id="announceShowCount10" class="<%= show10Class %>" >10</span>
            <span id="announceShowCount20" class="<%= show20Class %>" >20</span>
            <span><%=resMgr.getText("Home.ShowCountBetweenText", TradePortalConstants.TEXT_BUNDLE)%></span>
            <span id="homeAnnounceGrid_totalCount">0</span>
          </span>
          <%--cquinton 12/4/2012 remove grid edits from admin home--%>
        </span>
        <%--<div style="clear:both;"></div>--%>
      </div>

<%
    gridHtml = dGridFactory.createDataGrid("homeAnnounceGrid","HomeAnnouncementsDataGrid",null);
%>
      <%= gridHtml %>
    </div>
<%
		}//end if view/maintain access
    } //end if announcements section


    /************************************************************************/
    /*                          LOCKED OUT USERS SECTION                    */
    /************************************************************************/

    if ( TradePortalConstants.DASHBOARD_SECTION__LOCKED_OUT_USERS.equals(sectionId) ) {
      boolean viewAccess = SecurityAccess.hasRights(loginRights,
                             SecurityAccess.VIEW_OR_MAINTAIN_LOCKED_OUT_USERS);
      boolean maintainAccess = SecurityAccess.hasRights(loginRights,
                                 SecurityAccess.MAINTAIN_LOCKED_OUT_USERS);
      if ( viewAccess || maintainAccess ) {
%>
    <%--include the form for button submission--%>
    <form id="LockedUserChooseForm" name="LockedUserChooseForm" method="post"
      action="<%=formMgr.getSubmitAction(response)%>">

      <%= formMgr.getFormInstanceAsInputField("LockedUserChooseForm", new Hashtable()) %>

      <div class="formContentNoSidebar">
        <div class="gridHeader">
          <%--cquinton 9/10/2012 Rel portal refresh ir#t36000004474
              add missing gridHeaderTitle--%>
          <span class="gridHeaderTitle">
<%
        String goToLockedOutUsers = "goToAdminRefDataHome";
        String lockedUrlParm = "&current2ndNav=lockedUsers";
%>
	    <a id="lockUserLink" href="<%= formMgr.getLinkAsUrl(goToLockedOutUsers, lockedUrlParm, response) %>">
              <%=resMgr.getText("Home.LockedOutUsers", TradePortalConstants.TEXT_BUNDLE)%>
            </a>
            <%= widgetFactory.createHoverHelp("lockUserLink","AdminHome.LockedOutUsersLink") %>
          </span>
          <span class="gridHeader-right">
            <span class="gridHeaderShowCounts">
              <span><%=resMgr.getText("Home.ShowCountPreText", TradePortalConstants.TEXT_BUNDLE)%></span>
<%
        String show5Class = "gridHeaderShowCountItem";
        String show10Class = "gridHeaderShowCountItem";
        String show20Class = "gridHeaderShowCountItem";
        if ( gridShowCount == 5 ) {
          show5Class += " selected";
        }
        else if ( gridShowCount == 10 ) {
          show10Class += " selected";
        }
        else if ( gridShowCount == 10 ) {
          show20Class += " selected";
        }
%>
              <span id="lockedUserShowCount5" class="<%= show5Class %>" >5</span>
              <span id="lockedUserShowCount10" class="<%= show10Class %>" >10</span>
              <span id="lockedUserShowCount20" class="<%= show20Class %>" >20</span>
              <span><%=resMgr.getText("Home.ShowCountBetweenText", TradePortalConstants.TEXT_BUNDLE)%></span>
              <span id="homeLockedUserGrid_totalCount">0</span>
            </span>
            <%--cquinton 12/4/2012 remove grid edits from admin home--%>
          </span>
          <%--<div style="clear:both;"></div>--%>
        </div>

<%
        if ( maintainAccess ) {
          gridHtml = dgFactory.createDataGrid("homeLockedUserGrid","HomeLockedUsersSelectDataGrid",null);
        }
        else {
          gridHtml = dgFactory.createDataGrid("homeLockedUserGrid","HomeLockedUsersDataGrid",null);
        }
%>
      <%= gridHtml %>
      </div>
    </form>
<%
      }
    } //end if locked out users section

  } //end section loop
%>

  </div>
</div>

<%--ctq portal refresh end --%>

<%
   Debug.debug("***END******************** TradePortal Home Page *********************END***");

   formMgr.storeInDocCache("default.doc", new DocumentHandler());

%>

<%
Hashtable secureParms = new Hashtable();
secureParms.put("login_oid", userSession.getUserOid());
secureParms.put("UserOid",userSession.getUserOid());
secureParms.put("login_rights", loginRights);
secureParms.put("SecurityRights", userSession.getSecurityRights());
%>
<%--PMitnala Cr 821 Rel 8.3 START --%>
<form method="post" id="Transaction-NotifyPanelUserForm" name="Transaction-NotifyPanelUserForm" action="<%=formMgr.getSubmitAction(response)%>">
<input type=hidden name="buttonName" value="">
<input type=hidden name="transaction_oid" value=""> 
<input type=hidden name="instrument_oid" value="">  
<%= formMgr.getFormInstanceAsInputField("Transaction-NotifyPanelUserForm", secureParms) %>
 </form>
 <form method="post" id="Receivables-NotifyPanelUserForm" name="Receivables-NotifyPanelUserForm" action="<%=formMgr.getSubmitAction(response)%>">
<input type=hidden name="buttonName" value="">
<input type=hidden name="payRemitOid" value=""> 
<%= formMgr.getFormInstanceAsInputField("Receivables-NotifyPanelUserForm", secureParms) %>
 </form>
 <form method="post" id="InvoiceOffersGroup-NotifyPanelUserForm" name="InvoiceOffersGroup-NotifyPanelUserForm" action="<%=formMgr.getSubmitAction(response)%>">
<input type=hidden name="buttonName" value="">
<input type=hidden name="invoiceOfferGroupOid" value=""> 
<%= formMgr.getFormInstanceAsInputField("InvoiceOffersGroup-NotifyPanelUserForm", secureParms) %>
 </form>
 <%--PMitnala Cr 821 Rel 8.3 END --%>
<jsp:include page="/common/Footer.jsp">
   <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>

<script>

  <%--setup a named variable for placing shared callback functions--%>
  var thisPage = {};

  function myformatter(columnValues) {
<%--
    var gridLink = "";
    if ( columnValues.length >= 2 ) {
      gridLink = "<a href='" + columnValues[1] + "'>" + columnValues[0] + "</a>";
    }
    else if ( columnValues.length == 1 ) {
      //no url parameters, just return the display
      gridLink = columnValues[0];
    }
    return gridLink;
--%>
    return 'myformattervalue';
  }

  function initiateParams(){
  <%--create grids--%>
  require(["dijit/registry", "dojo/dom","t360/datagrid", "dojo/on", "dojo/dom-construct", "dojo/aspect", "dojo/domReady!"],
      function( registry, dom, t360grid, on, domConstruct, aspect ) {
    var gridLayout;
    var initSearchParms;
    var selectedStatus;
    selectedStatus= '<%=TradePortalConstants.STATUS_ALL%>';
    

<%
  //loop through section again, generating necessary javascript
  sectionId = "";
  for( int sectionIdx = 0; sectionIdx < sections.size(); sectionIdx++ ) {
    sectionId = sections.get(sectionIdx);
    //Ravindra - CR-708B - End
    if ( TradePortalConstants.DASHBOARD_SECTION__INVOICE_OFFERED.equals(sectionId) &&
    		condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_INVOICE_OFFERS ) ){
         String homeInvoicesOfferedGridLayout = dGridFactory.createGridLayout("homeInvoicesOfferedGrid","HomeInvoicesOfferedDataGrid");
   %>
   <%--  Moving page layout to XML  --%>
   var invOffGridLayout = <%=homeInvoicesOfferedGridLayout%>;
 <%-- IR 16481 start --%>
   var savedSearchQueryDataIO = {
   				<% 
   				if(savedSearchQueryDataIO!=null && !savedSearchQueryDataIO.isEmpty()) {
   					Iterator<Map.Entry<String, String>> entries = savedSearchQueryDataIO.entrySet().iterator();
   				while (entries.hasNext()) {
   					Map.Entry<String, String> entry = entries.next(); 
   					%>
   				'<%=entry.getKey()%>':'<%=entry.getValue()%>'
   					<%if(entries.hasNext()) {%>
   					<%=comma%>
   					
   				<%
   					}
   				}
   				}%>
   	}  
  
 <%-- IR 16481 end --%>
 var homeInvoicesOfferedGrid = "";
 var homeInvInitSearchParms = "";
 require(["t360/OnDemandGrid"], function(onDemandGrid){
	 
	 var savedSort = savedSearchQueryDataIO["sort"];  
	   if("" == savedSort || null == savedSort || "undefined" == savedSort){
			  savedSort = '1';
		 }
 
	 var viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("HomeInvoicesOfferedDataView",userSession.getSecretKey())%>";  <%--Rel9.2 IR T36000032596 --%>
	 homeInvInitSearchParms="selectedStatus="+selectedStatus;
     homeInvoicesOfferedGrid = onDemandGrid.createOnDemandGrid("homeInvoicesOfferedGrid", viewName,
  		   invOffGridLayout, homeInvInitSearchParms, savedSort);   
 });
 
   <%
       }
//Ravindra - CR-708B - End


    if ( TradePortalConstants.DASHBOARD_SECTION__MAIL.equals(sectionId) &&
         SecurityAccess.hasRights(loginRights,SecurityAccess.ACCESS_MESSAGES_AREA) ){
      String mailGridLayout = dGridFactory.createGridLayout(
        "homeMailGrid","HomeMailMessagesDataGrid");
%>
   var mailGridLayout = <%= mailGridLayout %>;
  <%-- IR 16481 start --%>
    var savedSearchQueryDataMM = {
    				<% 
    				if(savedSearchQueryDataMM!=null && !savedSearchQueryDataMM.isEmpty()) {
    					Iterator<Map.Entry<String, String>> entries = savedSearchQueryDataMM.entrySet().iterator();
    				while (entries.hasNext()) {
    					Map.Entry<String, String> entry = entries.next(); 
    					%>
    				'<%=entry.getKey()%>':'<%=entry.getValue()%>'
    					<%if(entries.hasNext()) {%>
    					<%=comma%>
    					
    				<%
    					}
    				}
    				}%>
    	}
    
 
    var homeMailGrid = "";
    var mailInitSearchParms = "";
    require(["t360/OnDemandGrid"], function(onDemandGrid){
    	
    	var savedSort = savedSearchQueryDataMM["sort"]; 
        if("" == savedSort || null == savedSort || "undefined" == savedSort){
    		  savedSort = '-DateAndTime';
    	 }
        var org = savedSearchQueryDataMM["org"]; 
        if("" == org || null == org || "undefined" == org)
    		org='<%= EncryptDecrypt.encryptStringUsingTripleDes(MY_MAIL_AND_UNASSIGNED, userSession.getSecretKey()) %>';
    	else if(registry.byId("MailOrg")){
    		registry.byId("MailOrg").attr('value',org, false);
    	}
        var readmailmess = savedSearchQueryDataMM["UnreadFlag"]; 
        if("" == readmailmess || null == readmailmess || "undefined" == readmailmess)
        	readmailmess=registry.byId("MailReadUnread").get('value');
    	else if(registry.byId("MailReadUnread"))
    	{
    		registry.byId("MailReadUnread").attr('value',readmailmess, false);
    	}
        mailInitSearchParms = "org="+org+"&UnreadFlag="+readmailmess; 
        
        var viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("HomeMailMessagesDataView",userSession.getSecretKey())%>";<%--Rel9.2 IR T36000032596 --%>
       
    	homeMailGrid = onDemandGrid.createOnDemandGrid("homeMailGrid", viewName,
    			mailGridLayout, mailInitSearchParms, savedSort);
    	 //  - CR49330 -  Start
        var myGrid1 = registry.byId("homeMailGrid");
        
         aspect.after( myGrid1, 'renderRow', function( row, args )
         		{
  	    	  var data = args[0];
  	    	  if("Y" == data.UnreadFlag)
  	    	  {
  	    		  row.style="font-weight: bold;";
  	    	  }
         		  return row;
         		} );
   	  // CR49330 -  End
     	
    });
  
<%
    }

    if ( TradePortalConstants.DASHBOARD_SECTION__NOTIFICATIONS.equals(sectionId) &&
         SecurityAccess.hasRights(loginRights,SecurityAccess.ACCESS_MESSAGES_AREA) ) {
      String notificationsGridLayout = dGridFactory.createGridLayout(
        "homeNotifGrid","HomeNotificationsDataGrid");
      
%>
    var notificationsGridLayout = <%= notificationsGridLayout %>;
  <%-- IR 16481 start --%>
    var savedSearchQueryDataN = {
    				<% 
    				if(savedSearchQueryDataN!=null && !savedSearchQueryDataN.isEmpty()) {
    					Iterator<Map.Entry<String, String>> entries = savedSearchQueryDataN.entrySet().iterator();
    				while (entries.hasNext()) {
    					Map.Entry<String, String> entry = entries.next(); 
    					%>
    				'<%=entry.getKey()%>':'<%=entry.getValue()%>'
    					<%if(entries.hasNext()) {%>
    					<%=comma%>
    					
    				<%
    					}
    				}
    				}%>
    	}
    require(["t360/OnDemandGrid","dojo/query", "dijit/registry", "dojo/dom", "dojo/on", "t360/popup", "t360/datagrid", "dojo/ready", "dojo/aspect", "dojo/domReady!"],
    		function(onDemandGrid,query, registry, dom, on, popup, datagrid, ready,aspect){
    	ready(function() {
    	 var notifInitSearchParms = "";
    	
    	var savedSort = savedSearchQueryDataN["sort"];  
        if("" == savedSort || null == savedSort || "undefined" == savedSort){
    		  savedSort = '-DateAndTime';
    	 }
        var org = savedSearchQueryDataN["org"]; 
        if("" == org || null == org || "undefined" == org){
    		org='<%= EncryptDecrypt.encryptStringUsingTripleDes(userOrgOid, userSession.getSecretKey()) %>';
    		registry.byId("NotifOrg").attr('value',org, false);
        }
    	else if(registry.byId("NotifOrg")){
    		<%-- PMitnala IR#T36000020839 Rel 8.3. Checking if the store has the value that is being set --%>
    	    if(registry.byId("NotifOrg").store.query({value:org}).length>0){
      			registry.byId("NotifOrg").attr('value',org, false);
    	    }
    	}
        var status = savedSearchQueryDataN["status"]; 
        if("" == status || null == status || "undefined" == status)
        	status=registry.byId("NotifStatus").get('value');
    	else if(registry.byId("NotifStatus")){
      			registry.byId("NotifStatus").attr('value',status, false);
    	}
        // CR49930 -  Start
        var readStatus = savedSearchQueryDataN["UnreadFlag"]; 
        if("" == readStatus || null == readStatus || "undefined" == readStatus)
        	readStatus=registry.byId("Read/Unread").get('value');
    	else if(registry.byId("Read/Unread")){
      			registry.byId("Read/Unread").attr('value',readStatus, false);
    	}
        //CR49930 -  Start
       notifInitSearchParms = "org="+org+"&status="+status+"&UnreadFlag="+readStatus; <%-- hardcode it --%>
       var viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("HomeNotificationsDataView",userSession.getSecretKey())%>"; <%--Rel9.2 IR T36000032596 --%>
       onDemandGrid.createOnDemandGrid("homeNotifGrid", viewName,
        		notificationsGridLayout, notifInitSearchParms, savedSort);
     
      //  - CR49330 -  Start
      var myGrid = registry.byId("homeNotifGrid");
      
       aspect.after( myGrid, 'renderRow', function( row, args )
       		{
	    	  var data = args[0];
	    	  if("N" != data.UnreadFlag)
	    	  {
	    		  row.style="font-weight: bold;";
	    	  }
       		  return row;
       		} );
 	  // CR49330 -  End
    });});
<%
    }
  //CR 913 - Rel 9.0 - vdesingu - Start
    if ( TradePortalConstants.DASHBOARD_SECTION_DEBIT_FUNDING_MAIL.equals(sectionId) 
    		&& TradePortalConstants.INDICATOR_YES.equals(isDisplayPreDebitNotify)
 		   &&  TradePortalConstants.INDICATOR_YES.equals(preDebitEnabledAtCorpLevel)
           && SecurityAccess.hasRights(loginRights,SecurityAccess.ACCESS_MESSAGES_AREA) ){
         String debFundMailGridLayout = dGridFactory.createGridLayout(
           "homeDebitFundMailGrid","HomeDebitFundingMailMsgDataGrid");
   %>
      var debFundMailGridLayout = <%= debFundMailGridLayout %>;
     <%-- IR 16481 start --%>
       var savedSearchQueryDataFN = {
       				<% 
       				if(savedSearchQueryDataFN!=null && !savedSearchQueryDataFN.isEmpty()) {
       					Iterator<Map.Entry<String, String>> entries = savedSearchQueryDataFN.entrySet().iterator();
       				while (entries.hasNext()) {
       					Map.Entry<String, String> entry = entries.next(); 
       					%>
       				'<%=entry.getKey()%>':'<%=entry.getValue()%>'
       					<%if(entries.hasNext()) {%>
       					<%=comma%>
       					
       				<%
       					}
       				}
       				}%>
       	}
       
    
       var homeDebFundMailGrid = "";
       var debFundInitSearchParms = "";
       require(["t360/OnDemandGrid"], function(onDemandGrid){
       	
       	var savedSort = savedSearchQueryDataFN["sort"]; 
           if("" == savedSort || null == savedSort || "undefined" == savedSort){
       		  savedSort = '-DateAndTime';
       	 }
           var org = savedSearchQueryDataFN["org"]; 
           if("" == org || null == org || "undefined" == org)
       		org='<%= EncryptDecrypt.encryptStringUsingTripleDes(MY_MAIL_AND_UNASSIGNED, userSession.getSecretKey()) %>';
       	else if(registry.byId("DebitFundMailOrg")){
       		registry.byId("DebitFundMailOrg").attr('value',org, false);
       	}
           var debitStatus = savedSearchQueryDataFN["UnreadFlag"]; 
           if("" == debitStatus || null == debitStatus || "undefined" == debitStatus)
        	   debitStatus=registry.byId("DebitReadUnread").get('value');
       	else if(registry.byId("DebitReadUnread")){
         			registry.byId("DebitReadUnread").attr('value',debitStatus, false);
       	}
      
        debFundInitSearchParms = "org="+org+"&UnreadFlag="+debitStatus; 
        var viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("HomeDebitFundingMailMsgDataView",userSession.getSecretKey())%>"; <%--Rel9.2 IR T36000032596 --%>
        homeDebFundMailGrid = onDemandGrid.createOnDemandGrid("homeDebitFundMailGrid", viewName,
    	debFundMailGridLayout, debFundInitSearchParms, savedSort);
       	
       });
     
   <%
    }
  //CR 913 - Rel 9.0 - vdesingu - End
    if ( TradePortalConstants.DASHBOARD_SECTION__ALL_TRANSACTIONS.equals(sectionId) &&
         ( (org.allowSomeTradeTransaction() &&
            SecurityAccess.canViewTradeInstrument(loginRights)) ||
           (org.allowSomePaymentTransaction() &&
            SecurityAccess.canViewCashManagement(loginRights)) ) ) {
      String allTranGridLayout = dGridFactory.createGridLayout(
        "homeAllTranGrid", "HomeAllTransactionsDataGrid");
      //cquinton 12/7/2012 default all transactions search should be on started status
%>
    var allTranGridLayout = <%= allTranGridLayout %>;
    
  	 <%-- IR 16481 start --%>
    var savedSearchQueryDataAT = {
    				<% 
    				if(savedSearchQueryDataAT!=null && !savedSearchQueryDataAT.isEmpty()) {
    					Iterator<Map.Entry<String, String>> entries = savedSearchQueryDataAT.entrySet().iterator();
    				while (entries.hasNext()) {
    					Map.Entry<String, String> entry = entries.next(); 
    					%>
    				'<%=entry.getKey()%>':'<%=entry.getValue()%>'
    					<%if(entries.hasNext()) {%>
    					<%=comma%>
    					
    				<%
    					}
    				}
    				}%>
    	}
    
   
    require(["t360/OnDemandGrid"], function(onDemandGrid){
    	 var homeAllTranGrid = "";
    	 var allTransInitParms = "";
    	var savedSort = savedSearchQueryDataAT["sort"];  
      	if("" == savedSort || null == savedSort || "undefined" == savedSort){
    		  savedSort = '0';
    	 }
      	var work = savedSearchQueryDataAT["work"]; 
        if("" == work || null == work || "undefined" == work)
        	work='<%=EncryptDecrypt.encryptStringUsingTripleDes(selectedWorkflow, userSession.getSecretKey())%>';
    	else if(registry.byId("AllTranWork")){
    		<%-- PMitnala IR#T36000020839 Rel 8.3. Checking if the store has the value that is being set --%>
    		if(registry.byId("AllTranWork").store.query({value:work}).length>0){
      			registry.byId("AllTranWork").attr('value',work, false);
    	    }
      	}
        var status = savedSearchQueryDataAT["status"]; 
        if("" == status || null == status || "undefined" == status)
        	status='<%=TradePortalConstants.TRANS_STATUS_STARTED%>';
    	else if(registry.byId("AllTranStatus")){
      			registry.byId("AllTranStatus").attr('value',status, false);
    	}
        var grp = savedSearchQueryDataAT["grp"]; 
        if("" == grp || null == grp || "undefined" == grp)
        	grp=registry.byId("AllTranGroup").get('value');
    	else if(registry.byId("AllTranGroup")){
      			registry.byId("AllTranGroup").attr('value',grp, false);
    	}
        var iType = savedSearchQueryDataAT["iType"]; 
        if("" == iType || null == iType || "undefined" == iType)
        	iType=registry.byId("AllTranInstrType").get('value');
    	else if(registry.byId("AllTranInstrType")){
      			registry.byId("AllTranInstrType").attr('value',iType, false);
    	}
    	
    	allTransInitParms = "work="+work+"&status="+status+"&iType="+iType+"&grp="+grp;
    	var viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("AllTransactionsDataView",userSession.getSecretKey())%>";<%--Rel9.2 IR T36000032596 --%>
    	homeAllTranGrid = onDemandGrid.createOnDemandGrid("homeAllTranGrid", viewName,
    			allTranGridLayout, allTransInitParms,savedSort);
    });
   
<%
    }
//CR 821 Rel 8.3 START
    if ( TradePortalConstants.DASHBOARD_SECTION__PANEL_AUTH_TRANSACTIONS.equals(sectionId) &&
            ( (org.allowSomeTradeTransaction() &&
               SecurityAccess.canViewTradeInstrument(loginRights)) ||
              (org.allowSomePaymentTransaction() &&
               SecurityAccess.canViewCashManagement(loginRights)) ) ) {
    	homePanelAuthTranGridFlag = true;
    	homePanelAuthTranGridSectionIdx = sectionIdx;
    	
        String panelAuthTranGridLayout = dGridFactory.createGridLayout(
                "homePanelAuthTranGrid", "HomePanelAuthTransactionsDataGrid");
              //cquinton 12/7/2012 default all transactions search should be on started status
        %>
            
        		<%--  Moving page layout to XML  --%>
          var 	panelAuthGridLayout = <%=panelAuthTranGridLayout%>;  
          	<%-- IR 16481 start --%>
             var savedSearchQueryDataPA = {
             				<% 
             				if(savedSearchQueryDataPA!=null && !savedSearchQueryDataPA.isEmpty()) {
             					Iterator<Map.Entry<String, String>> entries = savedSearchQueryDataPA.entrySet().iterator();
             				while (entries.hasNext()) {
             					Map.Entry<String, String> entry = entries.next(); 
             					%>
             				'<%=entry.getKey()%>':'<%=entry.getValue()%>'
             					<%if(entries.hasNext()) {%>
             					<%=comma%>
             					
             				<%
             					}
             				}
             				}%>
             	}
             var homePanelAuthTranGrid = "";
          	var panelAuthInitSearchParms = "";
          	require(["t360/OnDemandGrid"], function(onDemandGrid){          		
          	
	             var work = savedSearchQueryDataPA["work"]; 
	             if("" == work || null == work || "undefined" == work)
	             	work='<%=EncryptDecrypt.encryptStringUsingTripleDes(selectedWorkflow, userSession.getSecretKey())%>';
	         	else if(registry.byId("PanelAuthTranWork")){
	         		<%-- PMitnala IR#T36000020839 Rel 8.3. Checking if the store has the value that is being set --%>
	         		if(registry.byId("PanelAuthTranWork").store.query({value:work}).length>0){
	           			registry.byId("PanelAuthTranWork").attr('value',work, false);
	         	    }
	           	}
	             var myTransactions = savedSearchQueryDataPA["myTransactions"]; 
	             if("" == myTransactions || null == myTransactions || "undefined" == myTransactions)
	             	myTransactions='<%=ALL_WORK%>';
	         	else if(registry.byId("PanelAuthTransactions")){
	           			registry.byId("PanelAuthTransactions").attr('value',myTransactions, false);
	         	}
	             var grp = savedSearchQueryDataPA["grp"]; 
	            
	             if("" == grp || null == grp || "undefined" == grp){
	             	if(registry.byId("PanelAuthTranGroup"))
	             		grp= registry.byId("PanelAuthTranGroup").get('value');
	             }
	         	else if(registry.byId("PanelAuthTranGroup")){
	         		registry.byId("PanelAuthTranGroup").attr('value', grp, false);
	         	}
	             var iType = savedSearchQueryDataPA["iType"]; 
	             if("" == iType || null == iType || "undefined" == iType){
	             	if(registry.byId("PanelAuthTranInstrType"))
	             	iType=registry.byId("PanelAuthTranInstrType").get('value');
	             }
	         	else if(registry.byId("PanelAuthTranInstrType")){
	           			registry.byId("PanelAuthTranInstrType").attr('value',iType, false);
	         	}
	          	var savedSort = savedSearchQueryDataPA["sort"];  
	          	if("" == savedSort || null == savedSort || "undefined" == savedSort){
	       		  savedSort = '0';
	       	 	}
	          	<%-- IR 16481 end --%>
	          	
	          	<%-- BMO Issue#78 Begins.(Pavani) All_Work was hardcoded previously, which is removed and the workflow from the session is being set --%>
	            panelAuthInitSearchParms = "work="+work;
	          	<%-- BMO Issue#78 Ends --%>
	            <%--  Prateep Gedupudi 24/02/2013 Added confInd param for loading instruments based on confInd for loggedin user  --%>
	          	 <%--MEer Rel 8.4 IR-23915 --%>
	          	panelAuthInitSearchParms += "&iType="+iType+"&grp="+grp+"&myTransactions="+myTransactions+"&status=<%=ALL%>";      
	           
	          	var viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("PanelAuthTransactionsDataView",userSession.getSecretKey())%>";<%--Rel9.2 IR T36000032596 --%>
	            <%-- reuse the alltransactions dataview --%>
	            homePanelAuthTranGrid = onDemandGrid.createOnDemandGrid("homePanelAuthTranGrid", viewName,
	            		panelAuthGridLayout, panelAuthInitSearchParms,savedSort);
	            <%-- Start Vsarkary 9.2 REL IR#T36000023506 Added in order to Hide instrumentID to show Action column first which is depended on instrumentID --%>
	            
	            var instrumentID="HiddenInstrumentID";
   				var gridID = "homePanelAuthTranGrid";
   				onDemandGrid.hideColumn(gridID,instrumentID);
   				
   				<%--  End Vsarkary 9.2 REL IR#T36000023506  --%>
          	});
        <%
            }
  //CR 821 Rel 8.3 END
    if ( TradePortalConstants.DASHBOARD_SECTION__RCV_MATCH_NOTICES.equals(sectionId) &&
         ( org.allowSomeReceivablesTransaction() &&
           SecurityAccess.hasRights(loginRights,SecurityAccess.ACCESS_RECEIVABLES_AREA)) ) {
      String rcvTranGridLayout = dGridFactory.createGridLayout(
        "homeRcvTranGrid", "HomeReceivablesMatchingDataGrid");
      session.setAttribute("PAYMENTREMITDETAIL","goToTradePortalHome");
%>
	<%--  Moving page layout to XML  --%>
	var rcvTranGridLayout = <%=rcvTranGridLayout%>;
  
  <%-- IR 16481 start --%>
    var savedSearchQueryDataRM = {
    				<% 
    				if(savedSearchQueryDataRM!=null && !savedSearchQueryDataRM.isEmpty()) {
    					Iterator<Map.Entry<String, String>> entries = savedSearchQueryDataRM.entrySet().iterator();
    				while (entries.hasNext()) {
    					Map.Entry<String, String> entry = entries.next(); 
    					%>
    				'<%=entry.getKey()%>':'<%=entry.getValue()%>'
    					<%if(entries.hasNext()) {%>
    					<%=comma%>
    					
    				<%
    					}
    				}
    				}%>
    	}
  
  require(["t360/OnDemandGrid"], function(onDemandGrid){
	  var homeRcvTranGrid = "";
	  var homeRcvInitSearchParms = "";
  
	    var savedSort = savedSearchQueryDataRM["sort"]; 
	    if("" == savedSort || null == savedSort || "undefined" == savedSort){
			  savedSort = '0';
		 	}
	    var work = savedSearchQueryDataRM["work"]; 
	    if("" == work || null == work || "undefined" == work)
	    	work='<%= EncryptDecrypt.encryptStringUsingTripleDes(ALL_WORK, userSession.getSecretKey()) %>';
		else if(registry.byId("RcvMatchWork")){
			<%-- SSikhakolli - Rel-8.3 Sys Test IR# T36000020960 09/23/2013 - Begin --%>
	 		if(registry.byId("RcvMatchWork").store.query({value:work}).length>0){
	     		registry.byId("RcvMatchWork").attr('value',work, false);
	   	    }
	 		<%-- SSikhakolli - Rel-8.3 Sys Test IR# T36000020960 09/23/2013 - End --%>
	  	}
	    var status = savedSearchQueryDataRM["status"]; 
	    if("" == status || null == status || "undefined" == status)
	    	status=registry.byId("RcvMatchStatus").get('value');
		else if(registry.byId("RcvMatchStatus")){
	  			registry.byId("RcvMatchStatus").attr('value',status, false);
		}
	    var pSrc = savedSearchQueryDataRM["pSrc"]; 
	    if("" == pSrc || null == pSrc || "undefined" == pSrc)
	    	pSrc=registry.byId("RcvMatchPaySrc").get('value');
		else if(registry.byId("RcvMatchPaySrc")){
	  			registry.byId("RcvMatchPaySrc").attr('value',pSrc, false);
		}
	  <%-- IR 16481 end --%>
	    var viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("HomeReceivablesMatchingDataView",userSession.getSecretKey())%>"; <%--Rel9.2 IR T36000032596 --%>
	    homeRcvInitSearchParms = "work="+work+"&status="+status+"&pSrc="+pSrc; <%-- hardcode it --%>
	    homeRcvTranGrid = onDemandGrid.createOnDemandGrid("homeRcvTranGrid", viewName,
	    		rcvTranGridLayout, homeRcvInitSearchParms,savedSort);
  });
    
<%
    }

    //cquinton 2/25/2013 - use DISPLAY_ACCOUNT BALANCES instead of NEW PAYMENT, which is incorrect
    if ( TradePortalConstants.DASHBOARD_SECTION__ACCOUNT_BALANCES.equals(sectionId) &&
         condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_ACCOUNT_BALANCES )) {
      //cquinton 12/20/2012 fix problem with displaying account data for admin users
      String acctGridLayout = null;
    //String acctGridDataView = null; //Rel 8.4 CR 590 :Declared as global
      if (userSession.hasSavedUserSession()) {
        acctGridLayout = dGridFactory.createGridLayout(
          "homeAcctGrid", "HomeCustAccountDataGrid");
        //reuse the account dataview
        acctGridDataView = "CustAccountDataView";
      }
      else {
        acctGridLayout = dGridFactory.createGridLayout(
          "homeAcctGrid", "HomeAccountDataGrid");
        //reuse the account dataview
        acctGridDataView = "AccountDataView";
      }
      
    //vdesingu Rel8.4 CR-590 : if user login first time then set refresh to true.
      boolean refresh = false;
      if ( !userSession.isAcctBalancesRefreshed() && 
    	      (!userSession.hasSavedUserSession() || userSession.isUsingSubsidiaryAccess())) {
    	    refresh = true;
    	}


%>
   var accGridLayout = <%= acctGridLayout %>;
    var accInitSearchParms = "";
  <%-- IR 16481 start --%>
   var savedSort ="";
    <% if (userSession.hasSavedUserSession()) {%>
    var savedSearchQueryDataCA = {
			<% 
			if(savedSearchQueryDataCA!=null && !savedSearchQueryDataCA.isEmpty()) {
				Iterator<Map.Entry<String, String>> entries = savedSearchQueryDataCA.entrySet().iterator();
			while (entries.hasNext()) {
				Map.Entry<String, String> entry = entries.next(); 
				%>
			'<%=entry.getKey()%>':'<%=entry.getValue()%>'
				<%if(entries.hasNext()) {%>
				<%=comma%>
				
			<%
				}
			}
			}%>
}
     savedSort = savedSearchQueryDataCA["sort"];  
    <%}else{%>
    var savedSearchQueryDataA = {
			<% 
			if(savedSearchQueryDataA!=null && !savedSearchQueryDataA.isEmpty()) {
				Iterator<Map.Entry<String, String>> entries = savedSearchQueryDataA.entrySet().iterator();
			while (entries.hasNext()) {
				Map.Entry<String, String> entry = entries.next(); 
				%>
			'<%=entry.getKey()%>':'<%=entry.getValue()%>'
				<%if(entries.hasNext()) {%>
				<%=comma%>
				
			<%
				}
			}
			}%>
}
     savedSort = savedSearchQueryDataA["sort"]; 
    <%}%>

    <%-- CR 590 --%>
    var homeAcctGrid = "";
    require(["t360/OnDemandGrid"], function(onDemandGrid){
    	
        if("" == savedSort || null == savedSort || "undefined" == savedSort){
  		  savedSort = '0';
  	 	}
        
      <%-- vdesingu Rel8.4 CR-590 start : gridLoadingMessage - if true, Custom grid loadingmessage else, default loading message --%>
        var gridLoadingMessage = '';
        if(<%=refresh%>){
        	gridLoadingMessage = "<%=resMgr.getText("Home.AccountBalanceLoadingMessage", TradePortalConstants.TEXT_BUNDLE) %>";
        }else{
        	gridLoadingMessage = "<%=resMgr.getText("common.Loading", TradePortalConstants.TEXT_BUNDLE) %>";
        }
      <%-- vdesingu Rel8.4 CR-590 End --%>
    
	accInitSearchParms = "refresh=<%=refresh%>"; <%-- vdesingu Rel8.4 CR-590, pass refresh paramenter as initial param --%>

  	  homeAcctGrid = onDemandGrid.createOnDemandGrid("homeAcctGrid", "<%=EncryptDecrypt.encryptStringUsingTripleDes(acctGridDataView,userSession.getSecretKey())%>",
  			accGridLayout, accInitSearchParms, savedSort, null, gridLoadingMessage); <%--Rel9.2 IR T36000032596 --%>
    });

<%
    }

    if ( TradePortalConstants.DASHBOARD_SECTION__ANNOUNCEMENTS.equals(sectionId) ) {
        String announceGridLayout = dGridFactory.createGridLayout(
                "homeAnnounceGrid", "HomeAnnouncementsDataGrid");
        %>
            gridLayout = <%= announceGridLayout %>;
            initSearchParms = "userOrgOid=<%=userOrgOid%>";
          <%-- IR 16481 start --%>
            var savedSearchQueryDataAN = {
        			<% 
        			if(savedSearchQueryDataAN!=null && !savedSearchQueryDataAN.isEmpty()) {
        				Iterator<Map.Entry<String, String>> entries = savedSearchQueryDataAN.entrySet().iterator();
        			while (entries.hasNext()) {
        				Map.Entry<String, String> entry = entries.next(); 
        				%>
        			'<%=entry.getKey()%>':'<%=entry.getValue()%>'
        				<%if(entries.hasNext()) {%>
        				<%=comma%>
        				
        			<%
        				}
        			}
        			}%>
        }
            var savedSort = savedSearchQueryDataAN["sort"]; 
            if("" == savedSort || null == savedSort || "undefined" == savedSort){
        		  savedSort = '-StartDate';
        	 	}
          <%-- IR 16481 end --%>
            <%-- cquinton 9/10/2012 Rel portal refresh - add default sort on start date descending --%>
            var viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("HomeAnnouncementsDataView",userSession.getSecretKey())%>";  <%--Rel9.2 IR T36000032596 --%>
            require(["t360/OnDemandGrid"], function(onDemandGrid){
            var homeAnnounceGrid = onDemandGrid.createOnDemandGrid("homeAnnounceGrid", viewName,
              gridLayout, initSearchParms,savedSort ,
              <%--cquinton 9/20/2012 ir#4158 pass in special not found text--%>
              '<%= resMgr.getText("Home.Announcements.NoActiveAnnouncements", TradePortalConstants.TEXT_BUNDLE) %>');
            homeAnnounceGrid.set('autoHeight',5); <%-- default --%>
            });
            function refreshAnnouncements() {
            	require(["t360/OnDemandGrid"], function(onDemandGridSearch){ 
              var searchParms = "userOrgOid=<%=userOrgOid%>";
              onDemandGridSearch.searchDataGrid("homeAnnounceGrid", "HomeAnnouncementsDataView", searchParms);
            	});
            }
        <%
    }

    if ( TradePortalConstants.DASHBOARD_SECTION__LOCKED_OUT_USERS.equals(sectionId) ) {
      boolean viewAccess = SecurityAccess.hasRights(loginRights,
                             SecurityAccess.VIEW_OR_MAINTAIN_LOCKED_OUT_USERS);
      boolean maintainAccess = SecurityAccess.hasRights(loginRights,
                                 SecurityAccess.MAINTAIN_LOCKED_OUT_USERS);
      if ( viewAccess || maintainAccess ) {
        String lockedUserGridLayout = null;
        if (maintainAccess) {
          lockedUserGridLayout = dgFactory.createGridLayout(
            "homeLockedUserGrid", "HomeLockedUsersSelectDataGrid");
        }
        else {
          lockedUserGridLayout = dgFactory.createGridLayout(
            "homeLockedUserGrid", "HomeLockedUsersDataGrid");
        }
%>
    gridLayout = <%= lockedUserGridLayout %>;
    initSearchParms = "userOrgOid=<%=userOrgOid%>";
    var viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("HomeLockedUsersDataView",userSession.getSecretKey())%>";  <%--Rel9.2 IR T36000032596 --%>
    var lockGrid = t360grid.createDataGrid("homeLockedUserGrid", viewName,
      gridLayout, initSearchParms,'0');
    lockGrid.set('autoHeight',5); <%-- default --%>

<% //enable the reset button on single selection
        if ( maintainAccess ) {
%>
    <%-- only enable grid buttons when specific number of items selected --%>
    thisPage.enableLockGridFooterItems = function() {
      t360grid.enableFooterItemOnSelectionEquals(this,"Home_LockedOutUsers_resetSelectedLockedOutUser",1);
    }
    on(lockGrid,"selectionChanged",thisPage.enableLockGridFooterItems);
<%
        }
%>

    function refreshLockedUsers() {
      var searchParms = "userOrgOid=<%=userOrgOid%>";
      t360grid.searchDataGrid("homeLockedUserGrid", "HomeLockedUsersDataView", searchParms);
    }
<%
      }
    }
  } //end for loop
%>
  });
  
  }

  <%--register event handlers--%>
  require(["dijit/registry", "dojo/query", "dojo/on", "dojo/dom-class", "dojo/dom-style", "t360/dialog",
           "dojo/dom", "dijit/layout/ContentPane", "dijit/TooltipDialog", "dijit/popup",
           "t360/common", "t360/popup", "t360/datagrid", "t360/OnDemandGrid","dgrid/extensions/ColumnResizer", 
           "dojo/aspect", "dojo/ready", "dojo/domReady!"],
      function(registry, query, on, domClass, domStyle, dialog,
               dom, ContentPane, TooltipDialog, popup,
               common, t360popup, t360grid,onDemandGrid,ColumnResizer, aspect,
               ready ) {
	  		var callFunc = true;
<%
  if (!TradePortalConstants.ADMIN.equals(currentSecurityType) || TradePortalConstants.INDICATOR_YES.equals(userSession.getCustomerAccessIndicator())) {
%>
    query('#announceMore').on("click", function() {
      dialog.open('announcementDialog', '<%=announcementDialogTitle%>',
                  'announcementDialog.jsp',
                  <%-- from home page we want every applicable announcement, not just the first identified one --%>
                  
                  null, null, 
                  null, null, '<%=dialogType%>');  <%-- callbacks and critical flag --%>
    });
<%
  }
%>
	<%-- Ravindra - CR-708B - Start  --%>
	query('#homeInvoicesOfferedGridEdit').on("click", function() {
	    var columns = onDemandGrid.getColumnsForCustomization('homeInvoicesOfferedGrid');
	    var parmNames = [ "gridId", "gridName", "columns" ];
	    var parmVals = [ "homeInvoicesOfferedGrid", "HomeInvoicesOfferedDataGrid", columns ];
	    t360popup.open(
	      'homeInvoicesOfferedGridEdit', 'dGridCustomizationPopup.jsp',
	      parmNames, parmVals,
	      null, null);  <%-- callbacks --%>
	  });
	<%-- Ravindra - CR-708B - End --%>

    query('#homeMailGridEdit').on("click", function() {
      var columns = onDemandGrid.getColumnsForCustomization('homeMailGrid');
      var parmNames = [ "gridId", "gridName", "columns" ];
      var parmVals = [ "homeMailGrid", "HomeMailMessagesDataGrid", columns ];
      t360popup.open(
        'homeMailGridEdit', 'dGridCustomizationPopup.jsp',
        parmNames, parmVals,
        null, null);  <%-- callbacks --%>
    });
    query('#homeNotifGridEdit').on("click", function() {
      var columns = onDemandGrid.getColumnsForCustomization('homeNotifGrid');
      var parmNames = [ "gridId", "gridName", "columns" ];
      var parmVals = [ "homeNotifGrid", "HomeNotificationsDataGrid", columns ];
      t360popup.open(
        'homeNotifGridEdit', 'dGridCustomizationPopup.jsp',
        parmNames, parmVals,
        null, null);  <%-- callbacks --%>
    });
    <%-- CR 913 - Rel 9.0 - vdesingu - Start --%>
    query('#homeDebitFundMailGridEdit').on("click", function() {
        var columns = onDemandGrid.getColumnsForCustomization('homeDebitFundMailGrid');
        var parmNames = [ "gridId", "gridName", "columns" ];
        var parmVals = [ "homeDebitFundMailGrid", "HomeDebitFundingMailMsgDataGrid", columns ];
        t360popup.open(
          'homeDebitFundMailGridEdit', 'dGridCustomizationPopup.jsp',
          parmNames, parmVals,
          null, null);  <%-- callbacks --%>
      });
  <%-- CR 913 - Rel 9.0 - vdesingu - End --%>
    query('#homeAllTranGridEdit').on("click", function() {
      var columns = onDemandGrid.getColumnsForCustomization('homeAllTranGrid');
      var parmNames = [ "gridId", "gridName", "columns" ];
      var parmVals = [ "homeAllTranGrid", "HomeAllTransactionsDataGrid", columns ];
      t360popup.open(
        'homeAllTranGridEdit', 'dGridCustomizationPopup.jsp',
        parmNames, parmVals,
        null, null);  <%-- callbacks --%>
    });
    <%-- CR 821 - Rel 8.3 --%>
    query('#homePanelAuthTranGridEdit').on("click", function() {
        var columns = onDemandGrid.getColumnsForCustomization('homePanelAuthTranGrid');
        var parmNames = [ "gridId", "gridName", "columns" ];
        var parmVals = [ "homePanelAuthTranGrid", "HomePanelAuthTransactionsDataGrid", columns ];
        t360popup.open(
          'homePanelAuthTranGridEdit', 'dGridCustomizationPopup.jsp',
          parmNames, parmVals,
          null, null);  <%-- callbacks --%>
      });
    query('#homeRcvTranGridEdit').on("click", function() {
      var columns = onDemandGrid.getColumnsForCustomization('homeRcvTranGrid');
      var parmNames = [ "gridId", "gridName", "columns" ];
      var parmVals = [ "homeRcvTranGrid", "HomeReceivablesMatchingDataGrid", columns ];
      t360popup.open('homeRcvTranGridEdit', 'dGridCustomizationPopup.jsp',
        parmNames, parmVals,
        null, null);  <%-- callbacks --%>
    });
    query('#homeAcctGridEdit').on("click", function() {
      var columns = onDemandGrid.getColumnsForCustomization('homeAcctGrid');
      var parmNames = [ "gridId", "gridName", "columns" ];
      var parmVals = [ "homeAcctGrid", "HomeAccountDataGrid", columns ];
      t360popup.open('homeAcctGridEdit', 'dGridCustomizationPopup.jsp',
        parmNames, parmVals,
        null, null);  <%-- callbacks --%>
    });

    <%--resize event handlers--%>
    <%-- Need to include "dojo/on", "dojo/dom-class", "dojo/dom-style", "dojo/dom" in the require--%>
    <%-- homeInvoicesOfferedGrid --%>
    if(<%=homeInvoicesOfferedGridFlag%>){
	    if (dom.byId('homeInvoicesOfferedGrid')){
		    on(dom.byId('homeInvoicesOfferedGrid'), 'dgrid-refresh-complete', function (event) {
		    	<%-- Rpasupulati IR T36000028477 start --%>
		         var grd = dijit.byId('homeInvoicesOfferedGrid');
		    	 grd.on("dgrid-sort", onSort);
		    	 function onSort(){
		    		 callFunc =false;
		    		 } 
		    	 
		    	 if(callFunc){
		    		 <%-- SSikhakolli - Rel-9.1 Sys Test IR#T36000032844 - 10/16/2014 --%>
		    		 <%-- setGridHeightOnLoad('homeInvoicesOfferedGrid', 'invoicesOfferedShowCount5'); --%>
		    		 setDynamicGridHeitht(null, 'homeInvoicesOfferedGrid', null);
						removeColumnHider('homeInvoicesOfferedGrid');
		    	 }	
		    	<%-- Rpasupulati IR T36000028477 END --%>
		    });
	    }
	    if (dom.byId('invoicesOfferedShowCount5')){
		    on(dom.byId('invoicesOfferedShowCount5'), 'click', function (event) {
		    	setDynamicGridHeitht(event, 'homeInvoicesOfferedGrid', null);
	    	
		    	domClass.remove("invoicesOfferedShowCount10",'selected');
		        domClass.remove("invoicesOfferedShowCount20",'selected');
		        domClass.add("invoicesOfferedShowCount5",'selected');
			});
	    }
	    if (dom.byId('invoicesOfferedShowCount10')){
		    on(dom.byId('invoicesOfferedShowCount10'), 'click', function (event) {
		    	setDynamicGridHeitht(event, 'homeInvoicesOfferedGrid', null);
	    	
		    	domClass.remove("invoicesOfferedShowCount5",'selected');
		        domClass.remove("invoicesOfferedShowCount20",'selected');
		        domClass.add("invoicesOfferedShowCount10",'selected');
			});
	    }
	    if (dom.byId('invoicesOfferedShowCount20')){
		    on(dom.byId('invoicesOfferedShowCount20'), 'click', function (event) {
		    	setDynamicGridHeitht(event, 'homeInvoicesOfferedGrid', null);
	    	
		    	domClass.remove("invoicesOfferedShowCount10",'selected');
		        domClass.remove("invoicesOfferedShowCount5",'selected');
		        domClass.add("invoicesOfferedShowCount20",'selected');
			});
    	    }
    }
	
    <%-- homeMailGrid --%>
    if(<%=homeMailGridFlag%>){
    	
    	 
	    if (dom.byId('homeMailGrid')){
		    on(dom.byId('homeMailGrid'), 'dgrid-refresh-complete', function (event) {
		    	<%-- Rpasupulati IR T36000028477 start --%>
		    	var grd = dijit.byId('homeMailGrid');
		    	 grd.on("dgrid-sort", onSort);
		    	 function onSort(){
		    		 callFunc =false;
		    		 } 
		    	 
		    	 if(callFunc){
		    		<%-- SSikhakolli - Rel-9.1 Sys Test IR#T36000032844 - 10/16/2014 --%>
		  			<%-- setGridHeightOnLoad('homeMailGrid', 'mailShowCount5'); --%>
		  			setDynamicGridHeitht(null, 'homeMailGrid', null);
					removeColumnHider('homeMailGrid');
		    	 }
		    	<%-- Rpasupulati IR T36000028477 END --%>
		    });
	    }
	    if (dom.byId('mailShowCount5')){
		    on(dom.byId('mailShowCount5'), 'click', function (event) {
		    	setDynamicGridHeitht(event, 'homeMailGrid', null);
	    	
		    	domClass.remove("mailShowCount10",'selected');
		        domClass.remove("mailShowCount20",'selected');
		        domClass.add("mailShowCount5",'selected');
			});
	    }
	    if (dom.byId('mailShowCount10')){
		    on(dom.byId('mailShowCount10'), 'click', function (event) {
		    	setDynamicGridHeitht(event, 'homeMailGrid', null);
	    	
		    	domClass.remove("mailShowCount5",'selected');
		        domClass.remove("mailShowCount20",'selected');
		        domClass.add("mailShowCount10",'selected');
			});
	    }
	    if (dom.byId('mailShowCount20')){
		    on(dom.byId('mailShowCount20'), 'click', function (event) {
		    	setDynamicGridHeitht(event, 'homeMailGrid', null);
	    	
		    	domClass.remove("mailShowCount5",'selected');
		        domClass.remove("mailShowCount10",'selected');
		        domClass.add("mailShowCount20",'selected');
			});
	    }
    }
    
    <%-- homeNotifGrid --%>
    if(<%=homeNotifGridFlag%>){
	    if (dom.byId('homeNotifGrid')){
		    on(dom.byId('homeNotifGrid'), 'dgrid-refresh-complete', function (event) {
		    	<%-- Rpasupulati IR T36000028477 start --%>
		         var grd = dijit.byId('homeNotifGrid');
		    	 grd.on("dgrid-sort", onSort);
		    	 function onSort(){
		    		 callFunc =false;
		    		 } 
		    	 
		    	 if(callFunc){
		    		<%-- SSikhakolli - Rel-9.1 Sys Test IR#T36000032844 - 10/16/2014 --%>
		    		<%-- setGridHeightOnLoad('homeNotifGrid', 'notifShowCount5'); --%>
		    		 setDynamicGridHeitht(null, 'homeNotifGrid', null);
						removeColumnHider('homeNotifGrid');
		    	 }	
		    	<%-- Rpasupulati IR T36000028477 END --%>
		    });
	    }
	    if (dom.byId('notifShowCount5')){
		    on(dom.byId('notifShowCount5'), 'click', function (event) {
		    	setDynamicGridHeitht(event, 'homeNotifGrid', null);
	    	
		    	domClass.remove("notifShowCount10",'selected');
		        domClass.remove("notifShowCount20",'selected');
		        domClass.add("notifShowCount5",'selected');
			});
	    }
	    if (dom.byId('notifShowCount10')){
		    on(dom.byId('notifShowCount10'), 'click', function (event) {
		    	setDynamicGridHeitht(event, 'homeNotifGrid', null);
	    	
		    	domClass.remove("notifShowCount5",'selected');
		        domClass.remove("notifShowCount20",'selected');
		        domClass.add("notifShowCount10",'selected');
			});
	    }
	    if (dom.byId('notifShowCount20')){
		    on(dom.byId('notifShowCount20'), 'click', function (event) {
		    	setDynamicGridHeitht(event, 'homeNotifGrid', null);
	    	
		    	domClass.remove("notifShowCount10",'selected');
		        domClass.remove("notifShowCount5",'selected');
		        domClass.add("notifShowCount20",'selected');
			});
	    }
    }
  <%-- CR 913 - Rel 9.0 - vdesingu - Start --%>
    <%-- Debit Funding Mail Grid --%>
    if(<%=homeDebFundMailGridFlag%>){
    	
    	if(dom.byId('homeDebitFundMailGrid')){
		    on(dom.byId('homeDebitFundMailGrid'), 'dgrid-refresh-complete', function (event) {
				
		    	<%-- Rpasupulati IR T36000028477 start --%>
		         var grd = dijit.byId('homeDebitFundMailGrid');
		    	 grd.on("dgrid-sort", onSort);
		    	 function onSort(){
		    		 callFunc =false;
		    		 } 
		    	 
		    	 if(callFunc){
		    		<%-- SSikhakolli - Rel-9.1 Sys Test IR#T36000032844 - 10/16/2014 --%>
		    		<%-- setGridHeightOnLoad('homeDebitFundMailGrid', 'debFundmailShowCount5'); --%>
		    		 setDynamicGridHeitht(null, 'homeDebitFundMailGrid', null);
					removeColumnHider('homeDebitFundMailGrid');
		    		 }	
		    	<%-- Rpasupulati IR T36000028477 END --%>
		    	
		    });
    	}
    	if(dom.byId('debFundmailShowCount5')){
		    on(dom.byId('debFundmailShowCount5'), 'click', function (event) {
		    	setDynamicGridHeitht(event, 'homeDebitFundMailGrid', null);
		    	
		    	domClass.remove("debFundmailShowCount10",'selected');
		        domClass.remove("debFundmailShowCount20",'selected');
		        domClass.add("debFundmailShowCount5",'selected');
			});
    	}
    	if(dom.byId('debFundmailShowCount10')){
		    on(dom.byId('debFundmailShowCount10'), 'click', function (event) {
		    	setDynamicGridHeitht(event, 'homeDebitFundMailGrid', null);
		    	
		    	domClass.remove("debFundmailShowCount5",'selected');
		        domClass.remove("debFundmailShowCount20",'selected');
		        domClass.add("debFundmailShowCount10",'selected');
			});
    	}
    	if(dom.byId('debFundmailShowCount20')){
		    on(dom.byId('debFundmailShowCount20'), 'click', function (event) {
		    	setDynamicGridHeitht(event, 'homeDebitFundMailGrid', null);
		    	
		    	domClass.remove("debFundmailShowCount5",'selected');
		        domClass.remove("debFundmailShowCount10",'selected');
		        domClass.add("debFundmailShowCount20",'selected');
			});
    	}
    }
  <%-- CR 913 - Rel 9.0 - vdesingu - End --%>
    <%-- homeAllTranGrid --%>
    if(<%=homeAllTranGridFlag%>){
	    if (dom.byId('homeAllTranGrid')){
		    on(dom.byId('homeAllTranGrid'), 'dgrid-refresh-complete', function (event) {
		    	<%-- Rpasupulati IR T36000028477 start --%>
		         var grd = dijit.byId('homeAllTranGrid');
		    	 grd.on("dgrid-sort", onSort);
		    	 function onSort(){
		    		 callFunc =false;
		    		 } 
		    	 
		    	 if(callFunc){
		    		<%-- SSikhakolli - Rel-9.1 Sys Test IR#T36000032844 - 10/16/2014 --%>
		    		<%-- setGridHeightOnLoad('homeAllTranGrid', 'allTranShowCount5'); --%>
		    		    setDynamicGridHeitht(null, 'homeAllTranGrid', null);
						removeColumnHider('homeAllTranGrid');
		    		 }	
		    	<%-- Rpasupulati IR T36000028477 END --%>
		     });
	    
	    }
	    if (dom.byId('allTranShowCount5')){
		    on(dom.byId('allTranShowCount5'), 'click', function (event) {
		    	setDynamicGridHeitht(event, 'homeAllTranGrid', null);
	    	
				domClass.remove("allTranShowCount10",'selected');
			    domClass.remove("allTranShowCount20",'selected');
			    domClass.add("allTranShowCount5",'selected');
			});
	    }
	    if (dom.byId('allTranShowCount10')){
		    on(dom.byId('allTranShowCount10'), 'click', function (event) {
		    	setDynamicGridHeitht(event, 'homeAllTranGrid', null);
	    	
				domClass.remove("allTranShowCount5",'selected');
			    domClass.remove("allTranShowCount20",'selected');
			    domClass.add("allTranShowCount10",'selected');
			});
	    }
	    if (dom.byId('allTranShowCount20')){
		    on(dom.byId('allTranShowCount20'), 'click', function (event) {
		    	setDynamicGridHeitht(event, 'homeAllTranGrid', null);
	    	
				domClass.remove("allTranShowCount5",'selected');
			    domClass.remove("allTranShowCount10",'selected');
			    domClass.add("allTranShowCount20",'selected');
			});
	    }
    }
	
    <%-- homePanelAuthTranGrid --%>
    if(<%=homePanelAuthTranGridFlag%>){ 
	    if (dom.byId('homePanelAuthTranGrid')){
	    	on(dom.byId('homePanelAuthTranGrid'), 'dgrid-refresh-complete', function (event) {
	    		<%-- Rpasupulati IR T36000028477 start --%>
		         var grd = dijit.byId('homePanelAuthTranGrid');
		    	 grd.on("dgrid-sort", onSort);
		    	 function onSort(){
		    		 callFunc =false;
		    		 } 
		    	 
		    	 if(callFunc){
		    		<%-- SSikhakolli - Rel-9.1 Sys Test IR#T36000032844 - 10/16/2014 --%>
		    		<%-- setGridHeightOnLoad('homePanelAuthTranGrid', 'panelAuthTranShowCount5'); --%>
		    			setDynamicGridHeitht(null, 'homePanelAuthTranGrid', null);
						removeColumnHider('homePanelAuthTranGrid');
		    		 }	
		    	<%-- Rpasupulati IR T36000028477 END --%>
	    		
		    });
    	}
	    if(dom.byId('panelAuthTranShowCount5')){
		    on(dom.byId('panelAuthTranShowCount5'), 'click', function (event) {
		    	setDynamicGridHeitht(event, 'homePanelAuthTranGrid', null);
		    	
		    	domClass.remove("panelAuthTranShowCount10",'selected');
		        domClass.remove("panelAuthTranShowCount20",'selected');
		        domClass.add("panelAuthTranShowCount5",'selected');
			});
		}
	    if (dom.byId('panelAuthTranShowCount10')){
		    on(dom.byId('panelAuthTranShowCount10'), 'click', function (event) {
		    	setDynamicGridHeitht(event, 'homePanelAuthTranGrid', null);
		    	
		    	domClass.remove("panelAuthTranShowCount5",'selected');
		        domClass.remove("panelAuthTranShowCount20",'selected');
		        domClass.add("panelAuthTranShowCount10",'selected');
			});
	    }
	    if (dom.byId('panelAuthTranShowCount20')){
		    on(dom.byId('panelAuthTranShowCount20'), 'click', function (event) {
		    	setDynamicGridHeitht(event, 'homePanelAuthTranGrid', null);
		    	
		    	domClass.remove("panelAuthTranShowCount10",'selected');
		        domClass.remove("panelAuthTranShowCount5",'selected');
		        domClass.add("panelAuthTranShowCount20",'selected');
			});
	    }
    }
	    
    <%-- homeRcvTranGrid --%>
    if(<%=homeRcvTranGridFlag%>){
    	if (dom.byId('homeRcvTranGrid')){
	    	on(dom.byId('homeRcvTranGrid'), 'dgrid-refresh-complete', function (event) {
	    		<%-- Rpasupulati IR T36000028477 start --%>
		         var grd = dijit.byId('homeRcvTranGrid');
		    	 grd.on("dgrid-sort", onSort);
		    	 function onSort(){
		    		 callFunc =false;
		    		 } 
		    	 
		    	 if(callFunc){
		    		<%-- SSikhakolli - Rel-9.1 Sys Test IR#T36000032844 - 10/16/2014 --%>
		    		<%-- setGridHeightOnLoad('homeRcvTranGrid', 'rcvTranShowCount5'); --%>
		    		 setDynamicGridHeitht(null, 'homeRcvTranGrid', null);
						removeColumnHider('homeRcvTranGrid');
		    		 }	
		    	<%-- Rpasupulati IR T36000028477 END --%>
	    		
		    });
    	}
    	if (dom.byId('rcvTranShowCount5')){
		    on(dom.byId('rcvTranShowCount5'), 'click', function (event) {
		    	setDynamicGridHeitht(event, 'homeRcvTranGrid', null);
		    	
		    	domClass.remove("rcvTranShowCount10",'selected');
		        domClass.remove("rcvTranShowCount20",'selected');
		        domClass.add("rcvTranShowCount5",'selected');
			});
    	}
    	if (dom.byId('rcvTranShowCount10')){
		    on(dom.byId('rcvTranShowCount10'), 'click', function (event) {
		    	setDynamicGridHeitht(event, 'homeRcvTranGrid', null);
		    	
		    	domClass.remove("rcvTranShowCount5",'selected');
		        domClass.remove("rcvTranShowCount20",'selected');
		        domClass.add("rcvTranShowCount10",'selected');
			});
    	}
    	if (dom.byId('rcvTranShowCount20')){
		    on(dom.byId('rcvTranShowCount20'), 'click', function (event) {
		    	setDynamicGridHeitht(event, 'homeRcvTranGrid', null);
		    	
		    	domClass.remove("rcvTranShowCount10",'selected');
		        domClass.remove("rcvTranShowCount5",'selected');
		        domClass.add("rcvTranShowCount20",'selected');
			});
    	}
    }
    
    <%-- homeAcctGrid --%>
    if(<%=homeAcctGridFlag%>){
    	if (dom.byId('homeAcctGrid')){
	    	on(dom.byId('homeAcctGrid'), 'dgrid-refresh-complete', function (event) {
	    		<%-- Rpasupulati IR T36000028477 start --%>
		         var grd = dijit.byId('homeAcctGrid');
		    	 grd.on("dgrid-sort", onSort);
		    	 function onSort(){
		    		 callFunc =false;
		    		 } 
		    	 
		    	 if(callFunc){
		    		<%-- SSikhakolli - Rel-9.1 Sys Test IR#T36000032844 - 10/16/2014 --%>
		    		<%-- setGridHeightOnLoad('homeAcctGrid', 'acctShowCount5'); --%>
		    		 setDynamicGridHeitht(null, 'homeAcctGrid', null);
						removeColumnHider('homeAcctGrid');
		    		 }	
		    	<%-- Rpasupulati IR T36000028477 END --%>
	    		
		    });
    	}
    	if (dom.byId('acctShowCount5')){
		    on(dom.byId('acctShowCount5'), 'click', function (event) {
		    	setDynamicGridHeitht(event, 'homeAcctGrid', null);
		    	
		    	domClass.remove("acctShowCount10",'selected');
		        domClass.remove("acctShowCount20",'selected');
		        domClass.add("acctShowCount5",'selected');
			});
    	}
    	if (dom.byId('acctShowCount10')){
		    on(dom.byId('acctShowCount10'), 'click', function (event) {
		    	setDynamicGridHeitht(event, 'homeAcctGrid', null);
		    	
		    	domClass.remove("acctShowCount5",'selected');
		        domClass.remove("acctShowCount20",'selected');
		        domClass.add("acctShowCount10",'selected');
			});
    	}
    	if (dom.byId('acctShowCount20')){
		    on(dom.byId('acctShowCount20'), 'click', function (event) {
		    	setDynamicGridHeitht(event, 'homeAcctGrid', null);
		    	
		    	domClass.remove("acctShowCount5",'selected');
		        domClass.remove("acctShowCount10",'selected');
		        domClass.add("acctShowCount20",'selected');
			});
    	}
    }
<%
  if ( sections.contains( TradePortalConstants.DASHBOARD_SECTION__ANNOUNCEMENTS ) ) {
	  %>
	    	if (dom.byId('homeAnnounceGrid')){
		    	on(dom.byId('homeAnnounceGrid'), 'dgrid-refresh-complete', function (event) {
			         var grd = dijit.byId('homeAnnounceGrid');
			    	 grd.on("dgrid-sort", onSort);
			    	 function onSort(){
			    		 callFunc =false;
			    		 } 
			    	 
			    	 if(callFunc){
			    		 setDynamicGridHeitht(null, 'homeAnnounceGrid', null);
							removeColumnHider('homeAnnounceGrid');
			    		 }	
			    });
	    	}
	    	if (dom.byId('announceShowCount5')){
			    on(dom.byId('announceShowCount5'), 'click', function (event) {
			    	setDynamicGridHeitht(event, 'homeAnnounceGrid', null);
			    	
			    	domClass.remove("announceShowCount10",'selected');
			        domClass.remove("announceShowCount20",'selected');
			        domClass.add("announceShowCount5",'selected');
				});
	    	}
	    	if (dom.byId('announceShowCount10')){
			    on(dom.byId('announceShowCount10'), 'click', function (event) {
			    	setDynamicGridHeitht(event, 'homeAnnounceGrid', null);
			    	
			    	domClass.remove("announceShowCount5",'selected');
			        domClass.remove("announceShowCount20",'selected');
			        domClass.add("announceShowCount10",'selected');
				});
	    	}
	    	if (dom.byId('announceShowCount20')){
			    on(dom.byId('announceShowCount20'), 'click', function (event) {
			    	setDynamicGridHeitht(event, 'homeAnnounceGrid', null);
			    	
			    	domClass.remove("announceShowCount5",'selected');
			        domClass.remove("announceShowCount10",'selected');
			        domClass.add("announceShowCount20",'selected');
				});
	    	}
	<%
 }
  if ( sections.contains( TradePortalConstants.DASHBOARD_SECTION__LOCKED_OUT_USERS ) ) {
	  %>
	    query('#lockedUserShowCount5').on("click", function() {
	      var myGrid = registry.byId("homeLockedUserGrid");
	      myGrid.set('autoHeight',5);
	      domClass.remove("lockedUserShowCount10",'selected');
	      domClass.remove("lockedUserShowCount20",'selected');
	      domClass.add("lockedUserShowCount5",'selected');
	    });
	    query('#lockedUserShowCount10').on("click", function() {
	      var myGrid = registry.byId("homeLockedUserGrid");
	      myGrid.set('autoHeight',10);
	      domClass.remove("lockedUserShowCount5",'selected');
	      domClass.remove("lockedUserShowCount20",'selected');
	      domClass.add("lockedUserShowCount10",'selected');
	    });
	    query('#lockedUserShowCount20').on("click", function() {
	      var myGrid = registry.byId("homeLockedUserGrid");
	      myGrid.set('autoHeight',20);
	      domClass.remove("lockedUserShowCount5",'selected');
	      domClass.remove("lockedUserShowCount10",'selected');
	      domClass.add("lockedUserShowCount20",'selected');
	    });
	<%
 }
%>
	function resetShowCountLink(gridId){
		if (dom.byId(gridId + 'ShowCount10'))
		    	domClass.remove(gridId + "ShowCount10",'selected');
		if (dom.byId(gridId + 'ShowCount20'))			
	        	domClass.remove(gridId + "ShowCount20",'selected');
	        if (dom.byId(gridId + 'ShowCount5'))	
			domClass.add(gridId + "ShowCount5",'selected');
	}
	<%-- Setting Grid height on click of (5, 10, 20) links --%>
	function setDynamicGridHeitht(event, gridId, sectionIdx){
		<%-- if(gridId == "homeNotifGrid")alert("here"); --%>
		<%-- SSikhakolli - Rel-9.1 Sys Test IR#T36000032844 - 10/16/2014 - Begin --%>
		var myGrid = registry.byId(gridId);
		var rowCount = parseInt("0", 10);
		
		if(event != null && sectionIdx == null){
			rowCount = parseInt(event.target.innerHTML, 10);
		}else if(event == null && sectionIdx != null){
			rowCount = parseInt((dojo.query(".selected"))[sectionIdx].innerHTML,10);
		}else {
			rowCount = parseInt((dojo.query(".selected"))[0].innerHTML,10);
		}
		
		<%--  Calculate the height of the scrollbar --%>
		<%-- var dgridScrollerNode = query('.dgrid-scroller', myGrid.domNode)[0]; --%>
	    var dgridScrollerNode = myGrid.bodyNode;
	    var scrollBarHeight = dgridScrollerNode.offsetHeight - dgridScrollerNode.clientHeight;
	    
		<%-- Include column header height with the grid height --%>
		var maxHeight = (myGrid.rowHeight * (rowCount)) + (myGrid.headerNode.offsetHeight+scrollBarHeight) + 'px';
		var scrollMaxHeight = (myGrid.rowHeight * (rowCount)) + scrollBarHeight + 'px';
		
		domStyle.set(myGrid.domNode, 'maxHeight', maxHeight);
		<%-- domStyle.set(dgridScrollerNode, 'maxHeight', scrollMaxHeight); --%>
		domStyle.set(dgridScrollerNode, 'height', scrollMaxHeight);
		var recCount = myGrid._total;
		
		<%--  below code ensures grid does not show blanks rows when records count is less  --%>
		<%--  then the selected row count. e.g. when row count is 10 and record count is 8 the we --%>
		<%--  show only 8 rows. --%>
		if (recCount < rowCount){
		    var dgridContentNode = myGrid.contentNode;

		    <%-- Adding this code to to prevent IE8 crash --%>
		    if(navigator.appVersion.indexOf("MSIE 8")==-1){
		    	dgridScrollerNode.style.height = '';
		    }
		    <%-- dgridScrollerNode.style.height = ''; commented as it cause horizontal scroll in IE8 to disappear. --%>
		    dgridContentNode.style.height = 'auto';
		    if (dgridContentNode.offsetHeight < dgridScrollerNode.offsetHeight) {
		    	var dgScrlNodeOffsetHght = dgridContentNode.offsetHeight;
				dgridScrollerNode.style.height = dgScrlNodeOffsetHght + 'px';
				if (recCount == 0){
					dgridScrollerNode.style.height = '';
					dgridScrollerNode.style.height = dgScrlNodeOffsetHght + 'px';
				}else if (recCount <= rowCount){
					<%-- jgadela - Rel-9.1 IR#T36000034198 - 11/105/2014 - removed the below condition because it is failing when we remove the columns from the display settings. --%>
					<%--  Aslo the condition is not valid since width is no way related to set the grid display height --%>
					dgridScrollerNode.style.height = (dgridContentNode.offsetHeight + myGrid.rowHeight) + 'px';
					<%-- if (dgridScrollerNode.scrollWidth > dgridScrollerNode.offsetWidth){
		    			dgridScrollerNode.style.height = (dgridContentNode.offsetHeight + myGrid.rowHeight) + 'px';
					} else{
		    			dgridScrollerNode.style.height = (dgridContentNode.offsetHeight) + 'px';
		    		} --%>
		    	}
		    }else{
		    	if (recCount < rowCount){
		    		<%-- jgadela - Rel-9.1 IR#T36000034198 - 11/105/2014 - removed the below condition because it is failing when we remove the columns from  the display settings. --%>
					<%--  Aslo the condition is not valid since width is no way related to set the grid display height --%>
					dgridScrollerNode.style.height = (dgridContentNode.offsetHeight + myGrid.rowHeight) + 'px';
					<%-- if (dgridScrollerNode.scrollWidth > dgridScrollerNode.offsetWidth){
		    			dgridScrollerNode.style.height = (dgridContentNode.offsetHeight + myGrid.rowHeight) + 'px';
					} else{
		    			dgridScrollerNode.style.height = (dgridContentNode.offsetHeight) + 'px';
		    		} --%>
		    		console.log("offsetHeight 2: "+dgridContentNode.offsetHeight);

		    	}
		    }
		    dgridContentNode.style.height = '';
	    }
		<%-- SSikhakolli - Rel-9.1 Sys Test IR#T36000032844 - 10/16/2014 - End --%>
	}
	<%-- 
	
	function setGridHeightOnLoad(gridId, countDiv){
		var myGrid = registry.byId(gridId);
		var rowCount = parseInt(dom.byId(countDiv).innerHTML, 10);
	  	
	    var dgridScrollerNode = myGrid.bodyNode;
	    var scrollBarHeight = dgridScrollerNode.offsetHeight - dgridScrollerNode.clientHeight;
	    var heightClassNamesWithoutHorizScroll = ['rows5-no-y-scroll','rows10-no-y-scroll','rows15-no-y-scroll','rows20-no-y-scroll'];
	    var heightClassNamesWithHorizScroll = ['rows5-with-y-scroll','rows10-with-y-scroll','rows15-with-y-scroll','rows20-with-y-scroll','rows30-with-y-scroll'];
	    var dgridContentNode = myGrid.contentNode;
   		
	    var scrollVisibleHeight = (myGrid.rowHeight * (rowCount+1)) + scrollBarHeight + 15 + 'px';
	    if(scrollBarHeight == 0){

	    	domStyle.set(myGrid.domNode, 'maxHeight', scrollVisibleHeight);
			domStyle.set(dgridScrollerNode, 'maxHeight', '');
			domClass.replace(myGrid.domNode, 'rows' + rowCount + '-no-y-scroll', heightClassNamesWithoutHorizScroll);
			domClass.add(countDiv,'selected');
	    }else {
		    domStyle.set(myGrid.domNode, 'maxHeight', scrollVisibleHeight);
			domStyle.set(dgridScrollerNode, 'maxHeight', '');
			domClass.replace(myGrid.domNode, 'rows' + rowCount + '-with-y-scroll', heightClassNamesWithHorizScroll);
			domClass.add(countDiv,'selected');
	    }

	    if(navigator.appName.indexOf("Internet Explorer")!=-1){    

			if (myGrid.rowHeight > 21){
				myGrid.rowHeight = 21;
			}
	    }		
	    	var maxHeight = (myGrid.rowHeight * (rowCount+1)) + scrollBarHeight + 'px';
			var scrollMaxHeight = (myGrid.rowHeight * (rowCount-1)) + scrollBarHeight + 'px';
			
			domStyle.set(myGrid.domNode, 'maxHeight', scrollVisibleHeight);
			domStyle.set(dgridScrollerNode, 'maxHeight', scrollMaxHeight);
			domStyle.set(dgridScrollerNode, 'height', scrollMaxHeight); 
	       		
	    
	   

	    
	    if(navigator.appName.indexOf("Internet Explorer") ==-1){
	    	dgridScrollerNode.style.height = '';
	    }
	   
	    dgridContentNode.style.height = 'auto';
	    var recCount = myGrid._total;
	    if (dgridContentNode.offsetHeight < dgridScrollerNode.offsetHeight) {
	    	var dgScrlNodeOffsetHght = dgridContentNode.offsetHeight;
		dgridScrollerNode.style.height = dgScrlNodeOffsetHght + 'px';
		
		if (recCount == 0){
			dgridScrollerNode.style.height = '';
			dgridScrollerNode.style.height = dgScrlNodeOffsetHght + 'px';
		}else if (recCount <= rowCount){
	    		dgridScrollerNode.style.height = (dgridContentNode.offsetHeight + myGrid.rowHeight) + 'px';
	    	}
		
	    }else{
	    	if (recCount < rowCount){
	    		dgridScrollerNode.style.height = (dgridContentNode.offsetHeight + myGrid.rowHeight) + 'px';
	    	}
	    }
	    dgridContentNode.style.height = '';

	} --%>
	
	<%-- We have included Dgrid's ColumnHider exteinction to hide/show the grid columns but as we don't require the Dgrids default  --%>
	<%-- customization removing that on grid-refresh-complete event --%>
	function removeColumnHider(grid){
		if(document.getElementById("dgrid-hider-menu-"+grid) != null){
			var columnHider = document.getElementById("dgrid-hider-menu-"+grid);
			columnHider.parentNode.removeChild(columnHider);
		}
	}
	
	<%-- function calculateGridHeight(gridId){
		var myGrid = registry.byId(gridId);
		var dgridScrollerNode = myGrid.bodyNode;
		var dgridContentNode = myGrid.contentNode;
		
		dgridScrollerNode.style.height = '';
	    dgridContentNode.style.height = 'auto';
		if (dgridContentNode.offsetHeight < dgridScrollerNode.offsetHeight) {
			dgridScrollerNode.style.height = dgridContentNode.offsetHeight + 'px';
		}
		dgridContentNode.style.height = '';
	} --%>
    <%--register widget event handlers--%>
    ready(function() {
    	
    	initiateParams();
      var focusFieldId = '<%= focusField %>';
      if ( focusFieldId ) {
        var focusField = registry.byId(focusFieldId);
        focusField.focus();
      }

<%
  if ( sections.contains( TradePortalConstants.DASHBOARD_SECTION__LOCKED_OUT_USERS ) ) {
    String resetUserDialogTitle = resMgr.getTextEscapedJS("UnlockUserPopUp.title", TradePortalConstants.TEXT_BUNDLE);
%>
      var resetLockedUserButton = registry.byId("Home_LockedOutUsers_resetSelectedLockedOutUser");
      if ( resetLockedUserButton ) {
        resetLockedUserButton.on("click", function() {
          <%--get array of rowkeys from the grid--%>
          var myGrid = registry.byId("homeLockedUserGrid");
          var rowItems = myGrid.selection.getSelected();
          var rowKeys = t360grid.getSelectedGridRowKeys("homeLockedUserGrid");
          var userId = rowItems[0].i.UserId;
          dialog.open('resetUserDialog', '<%=resetUserDialogTitle%> - [ ' + userId + ' ]',
                      'UnlockUser.jsp',
                      ['selection','src'],[rowKeys[0],'TradePortalHome'], <%-- selection is UserOId --%>
                      null, null);
        });
      }

<%
  }
%>
<%-- notification start --%>
if(registry.byId('NotifOrg')){
  registry.byId('NotifOrg').on("change", function() {
     searchNotificationGrid();
  });
} 

if(registry.byId('NotifStatus')){
  registry.byId('NotifStatus').on("change", function() {
  	searchNotificationGrid();
  });
}
<%--CR49930 -  Start  --%>
if(registry.byId('Read/Unread')){
	  registry.byId('Read/Unread').on("change", function() {
	  	searchNotificationGrid();
	  });
	}
<%--CR49930 -  Start --%>
 <%-- notification end --%>
 <%-- invoiceoffer start --%>
  if(registry.byId('Currency')){
    registry.byId('Currency').on("change", function() {
  	  searchInvoiceOfferGrid();
    });
  }
  if(registry.byId('AmountFrom')){
    registry.byId('AmountFrom').on("change", function() {
  	  searchInvoiceOfferGrid();
    });
  }
  if(registry.byId('AmountTo')){
    registry.byId('AmountTo').on("change", function() {
  	  searchInvoiceOfferGrid();
    });
  }
  if(registry.byId('DateFrom')){
    registry.byId('DateFrom').on("change", function() {
  	  searchInvoiceOfferGrid();
    });
  }
  if(registry.byId('DateTo')){
    registry.byId('DateTo').on("change", function() {
  	  searchInvoiceOfferGrid();
    });
  }
    <%-- invoiceoffer end --%>
    
    <%-- mail message  --%>
  if(registry.byId('MailOrg')){
    registry.byId('MailOrg').on("change", function() {
  	  searchMailMessageGrid();
    });
  }
  if(registry.byId('MailReadUnread')){
	    registry.byId('MailReadUnread').on("change", function() {
	  	  searchMailMessageGrid();
	    });
	  }
    <%-- All transaction grid --%>
  if(registry.byId('AllTranWork')){  
    registry.byId('AllTranWork').on("change", function() {
  	  searchAllTransactionGrid();
    });
  }
  if(registry.byId('AllTranGroup')){ 
    registry.byId('AllTranGroup').on("change", function() {
  	  searchAllTransactionGrid();
    });
  }
  if(registry.byId('AllTranInstrType')){ 
    registry.byId('AllTranInstrType').on("change", function() {
  	  searchAllTransactionGrid();
    });
  }
  if(registry.byId('AllTranStatus')){ 
    registry.byId('AllTranStatus').on("change", function() {
  	  searchAllTransactionGrid();
    });
  }
    <%-- Panel Auth --%>
     <%-- CR 821 Rel 8.3 START --%>
  if(registry.byId('PanelAuthTranWork')){ 
    registry.byId('PanelAuthTranWork').on("change", function() {
  	  searchPanelAuthGrid();
    });
  }
  if(registry.byId('PanelAuthTranGroup')){ 
    registry.byId('PanelAuthTranGroup').on("change", function() {
  	  searchPanelAuthGrid();
    });
  }
  if(registry.byId('PanelAuthTranInstrType')){ 
    registry.byId('PanelAuthTranInstrType').on("change", function() {
  	  searchPanelAuthGrid();
    });
  }
  if(registry.byId('PanelAuthTranStatus')){
    registry.byId('PanelAuthTranStatus').on("change", function() {
  	  searchPanelAuthGrid();
    });
  }
  if(registry.byId('PanelAuthTransactions')){
    registry.byId('PanelAuthTransactions').on("change", function() {
  	  searchPanelAuthGrid();
    });
  }
    <%-- panel auth --%>
         <%-- CR 821 Rel 8.3 END    --%>
  if(registry.byId('RcvMatchWork')){
    registry.byId('RcvMatchWork').on("change", function() {
  	  searchReceivableMatchGrid();
    });
  }
  if(registry.byId('RcvMatchStatus')){  
    registry.byId('RcvMatchStatus').on("change", function() {
  	  searchReceivableMatchGrid();
    });
  }
  if(registry.byId('RcvMatchPaySrc')){  
    registry.byId('RcvMatchPaySrc').on("change", function() {
  	  searchReceivableMatchGrid();
    });
  }
  
  <%-- SSikhakolli - Rel 9.1 UAT IR#T36000034378 - 11/12/2014 - Begin --%>
  	<%--  Adding code specific to IE8 to fix the Horizantal Scroll Issue for DGrid on Column-Resize.
  	 * following code is used to fix IR# T36000034378 on top of the code added as part of IR# T36000032845.
  	 * TODO: need to remove code encapsulated in if(isIE8){} condition. Only set of statements which are in else case 
  	 *       are enough once we stop supporting IE8
  	  --%>
  	var isIE8 = true;
	var iloop = 1;
	if(navigator.appVersion.indexOf("MSIE 8")==-1){ isIE8 = false};
	
	if(isIE8){
		<%-- IR T36000032845 - Override Column resize listenner using on-dgrid-columnresize. --%>
		if(registry.byId('homeAllTranGrid')){
		  	on(dom.byId('homeAllTranGrid'), 'dgrid-columnresize', function (event) {
		  		setTimeout(function (){
		  			setDynamicGridHeitht(null, 'homeAllTranGrid', <%=homeAllTranGridSectionIdx%>);
		  		},500);
		  	});
	  	}
	  
		if(registry.byId('homePanelAuthTranGrid')){
		  	on(dom.byId('homePanelAuthTranGrid'), 'dgrid-columnresize', function (event) {
		  		setTimeout(function (){
		  			setDynamicGridHeitht(null, 'homePanelAuthTranGrid', <%=homePanelAuthTranGridSectionIdx%>);
		  		},500);
		  	});
	  	}
		
		if(registry.byId('homeRcvTranGrid')){
		  	on(dom.byId('homeRcvTranGrid'), 'dgrid-columnresize', function (event) {
		  		setTimeout(function (){
		  			setDynamicGridHeitht(null, 'homeRcvTranGrid', <%=homeRcvTranGridSectionIdx%>);
		  		},500);
		  	});
	  	}
		
		if(registry.byId('homeAcctGrid')){
		  	on(dom.byId('homeAcctGrid'), 'dgrid-columnresize', function (event) {
		  		setTimeout(function (){
		  			setDynamicGridHeitht(null, 'homeAcctGrid', <%=homeAcctGridSectionIdx%>);
		  		},500);
		  	});
	  	}
		
		if(registry.byId('homeNotifGrid')){
		  	on(dom.byId('homeNotifGrid'), 'dgrid-columnresize', function (event) {
		  		setTimeout(function (){
		  			setDynamicGridHeitht(null, 'homeNotifGrid', <%=homeNotifGridSectionIdx%>);
		  		},500);
		  	});
	  	}
		
		if(registry.byId('homeInvoicesOfferedGrid')){
		  	on(dom.byId('homeInvoicesOfferedGrid'), 'dgrid-columnresize', function (event) {
		  		setTimeout(function (){
		  			setDynamicGridHeitht(null, 'homeInvoicesOfferedGrid', <%=homeInvoicesOfferedGridSectionIdx%>);
		  		},500);
		  	});
	  	}
		
		if(registry.byId('homeMailGrid')){
		  	on(dom.byId('homeMailGrid'), 'dgrid-columnresize', function (event) {
		  		setTimeout(function (){
		  			setDynamicGridHeitht(null, 'homeMailGrid', <%=homeMailGridSectionIdx%>);
		  		},500);
		  	});
	  	}
		
		if(registry.byId('homeDebitFundMailGrid')){
		  	on(dom.byId('homeDebitFundMailGrid'), 'dgrid-columnresize', function (event) {
		  		setTimeout(function (){
		  			setDynamicGridHeitht(null, 'homeDebitFundMailGrid', <%=homeDebFundMailGridSectionIdx%>);
		  		},500);
		  	});
	  	}
	}else{  
		<%-- SSikhakolli - Rel 9.1 UAT IR#T36000034378 - 11/12/2014 - End --%>
		<%-- TODO: need to move these set of statements in else outside, once we stop support IE8..	 --%>
	  	<%-- IR T36000032845 - Override Column resize listenner using aspect. --%>
	  	if(registry.byId('homeAllTranGrid')){
			 aspect.after(registry.byId('homeAllTranGrid'), "resize", function(){
			    var showCount = parseInt((dojo.query(".selected"))[<%=homeAllTranGridSectionIdx%>].innerHTML,10);
				setDynamicGridHeitht(null, 'homeAllTranGrid', <%=homeAllTranGridSectionIdx%>);
				if (showCount == 5){
					domClass.remove("allTranShowCount10",'selected');
				    domClass.remove("allTranShowCount20",'selected');
				    domClass.add("allTranShowCount5",'selected');
				}else if (showCount == 10){
					domClass.add("allTranShowCount10",'selected');
				    domClass.remove("allTranShowCount20",'selected');
				    domClass.remove("allTranShowCount5",'selected');
				}else if (showCount == 20){
					domClass.remove("allTranShowCount10",'selected');
				    domClass.add("allTranShowCount20",'selected');
				    domClass.remove("allTranShowCount5",'selected');
				}
			 });
		}
	
	  	if(registry.byId('homePanelAuthTranGrid')){
			 aspect.after(registry.byId('homePanelAuthTranGrid'), "resize", function(){
			    var showCount = parseInt((dojo.query(".selected"))[<%=homePanelAuthTranGridSectionIdx%>].innerHTML,10);
				setDynamicGridHeitht(null, 'homePanelAuthTranGrid', <%=homePanelAuthTranGridSectionIdx%>);
				if (showCount == 5){
					domClass.remove("panelAuthTranShowCount10",'selected');
				    domClass.remove("panelAuthTranShowCount20",'selected');
				    domClass.add("panelAuthTranShowCount5",'selected');
				}else if (showCount == 10){
					domClass.add("panelAuthTranShowCount10",'selected');
				    domClass.remove("panelAuthTranShowCount20",'selected');
				    domClass.remove("panelAuthTranShowCount5",'selected');
				}else if (showCount == 20){
					domClass.remove("panelAuthTranShowCount10",'selected');
				    domClass.add("panelAuthTranShowCount20",'selected');
				    domClass.remove("panelAuthTranShowCount5",'selected');
				}
			 });
		}
	
	  	if(registry.byId('homeRcvTranGrid')){
			 aspect.after(registry.byId('homeRcvTranGrid'), "resize", function(){
			    var showCount = parseInt((dojo.query(".selected"))[<%=homeRcvTranGridSectionIdx%>].innerHTML,10);
				setDynamicGridHeitht(null, 'homeRcvTranGrid', <%=homeRcvTranGridSectionIdx%>);
				if (showCount == 5){
					domClass.remove("rcvTranShowCount10",'selected');
				    domClass.remove("rcvTranShowCount20",'selected');
				    domClass.add("rcvTranShowCount5",'selected');
				}else if (showCount == 10){
					domClass.add("rcvTranShowCount10",'selected');
				    domClass.remove("rcvTranShowCount20",'selected');
				    domClass.remove("rcvTranShowCount5",'selected');
				}else if (showCount == 20){
					domClass.remove("rcvTranShowCount10",'selected');
				    domClass.add("rcvTranShowCount20",'selected');
				    domClass.remove("rcvTranShowCount5",'selected');
				}
			 });
		}
	
	  	if(registry.byId('homeAcctGrid')){
			 aspect.after(registry.byId('homeAcctGrid'), "resize", function(){
			    var showCount = parseInt((dojo.query(".selected"))[<%=homeAcctGridSectionIdx%>].innerHTML,10);
				setDynamicGridHeitht(null, 'homeAcctGrid', <%=homeAcctGridSectionIdx%>);
				if (showCount == 5){
					domClass.remove("acctShowCount10",'selected');
				    domClass.remove("acctShowCount20",'selected');
				    domClass.add("acctShowCount5",'selected');
				}else if (showCount == 10){
					domClass.add("acctShowCount10",'selected');
				    domClass.remove("acctShowCount20",'selected');
				    domClass.remove("acctShowCount5",'selected');
				}else if (showCount == 20){
					domClass.remove("acctShowCount10",'selected');
				    domClass.add("acctShowCount20",'selected');
				    domClass.remove("acctShowCount5",'selected');
				}
			 });
		}
	
	  	if(registry.byId('homeNotifGrid')){
			 aspect.after(registry.byId('homeNotifGrid'), "resize", function(){
			    var showCount = parseInt((dojo.query(".selected"))[<%=homeNotifGridSectionIdx%>].innerHTML,10);
				setDynamicGridHeitht(null, 'homeNotifGrid', <%=homeNotifGridSectionIdx%>);
				if (showCount == 5){
					domClass.remove("notifShowCount10",'selected');
				    domClass.remove("notifShowCount20",'selected');
				    domClass.add("notifShowCount5",'selected');
				}else if (showCount == 10){
					domClass.add("notifShowCount10",'selected');
				    domClass.remove("notifShowCount20",'selected');
				    domClass.remove("notifShowCount5",'selected');
				}else if (showCount == 20){
					domClass.remove("notifShowCount10",'selected');
				    domClass.add("notifShowCount20",'selected');
				    domClass.remove("notifShowCount5",'selected');
				}
			 });
		}
	
	  	if(registry.byId('homeInvoicesOfferedGrid')){
			 aspect.after(registry.byId('homeInvoicesOfferedGrid'), "resize", function(){
			    var showCount = parseInt((dojo.query(".selected"))[<%=homeInvoicesOfferedGridSectionIdx%>].innerHTML,10);
				setDynamicGridHeitht(null, 'homeInvoicesOfferedGrid', <%=homeInvoicesOfferedGridSectionIdx%>);
				if (showCount == 5){
					domClass.remove("invoicesOfferedShowCount10",'selected');
				    domClass.remove("invoicesOfferedShowCount20",'selected');
				    domClass.add("invoicesOfferedShowCount5",'selected');
				}else if (showCount == 10){
					domClass.add("invoicesOfferedShowCount10",'selected');
				    domClass.remove("invoicesOfferedShowCount20",'selected');
				    domClass.remove("invoicesOfferedShowCount5",'selected');
				}else if (showCount == 20){
					domClass.remove("invoicesOfferedShowCount10",'selected');
				    domClass.add("invoicesOfferedShowCount20",'selected');
				    domClass.remove("invoicesOfferedShowCount5",'selected');
				}
			 });
		}
	
	  	if(registry.byId('homeMailGrid')){
			 aspect.after(registry.byId('homeMailGrid'), "resize", function(){
			    var showCount = parseInt((dojo.query(".selected"))[<%=homeMailGridSectionIdx%>].innerHTML,10);
				setDynamicGridHeitht(null, 'homeMailGrid', <%=homeMailGridSectionIdx%>);
				if (showCount == 5){
					domClass.remove("mailShowCount10",'selected');
				    domClass.remove("mailShowCount20",'selected');
				    domClass.add("mailShowCount5",'selected');
				}else if (showCount == 10){
					domClass.add("mailShowCount10",'selected');
				    domClass.remove("mailShowCount20",'selected');
				    domClass.remove("mailShowCount5",'selected');
				}else if (showCount == 20){
					domClass.remove("mailShowCount10",'selected');
				    domClass.add("mailShowCount20",'selected');
				    domClass.remove("mailShowCount5",'selected');
				}
			 });
		}
	
	  	if(registry.byId('homeDebitFundMailGrid')){
			 aspect.after(registry.byId('homeDebitFundMailGrid'), "resize", function(){
			    var showCount = parseInt((dojo.query(".selected"))[<%=homeDebFundMailGridSectionIdx%>].innerHTML,10);
				setDynamicGridHeitht(null, 'homeDebitFundMailGrid', <%=homeDebFundMailGridSectionIdx%>);
				if (showCount == 5){
					domClass.remove("debFundmailShowCount10",'selected');
				    domClass.remove("debFundmailShowCount20",'selected');
				    domClass.add("debFundmailShowCount5",'selected');
				}else if (showCount == 10){
					domClass.add("debFundmailShowCount10",'selected');
				    domClass.remove("debFundmailShowCount20",'selected');
				    domClass.remove("debFundmailShowCount5",'selected');
				}else if (showCount == 20){
					domClass.remove("debFundmailShowCount10",'selected');
				    domClass.add("debFundmailShowCount20",'selected');
				    domClass.remove("debFundmailShowCount5",'selected');
				}
			 });
		}
	}<%-- end isIE8 else{} --%>
<%-- aspect approach end --%>
  });
  <%-- vdesingu Rel8.4 CR-590 --%>
  
    query('#homeAcctRefresh').on("click", function() {
			refreshAccountBalance();
	});
  
      <%-- CR 913 - Rel 9.0 vdesingu start --%>
    <%-- Pre Debit Funding Notification  --%>
    if(registry.byId('DebitFundMailOrg')){
      registry.byId('DebitFundMailOrg').on("change", function() {
    	  searchDebitFundMailMessageGrid();
      });
    }if(registry.byId('DebitReadUnread')){
        registry.byId('DebitReadUnread').on("change", function() {
        	  searchDebitFundMailMessageGrid();
          });
        }

    if(registry.byId('PaymentDateFrom')){
        registry.byId('PaymentDateFrom').on("change", function() {
      	  searchDebitFundMailMessageGrid();
        });
      }
    if(registry.byId('PaymentDateTo')){
        registry.byId('PaymentDateTo').on("change", function() {
      	  searchDebitFundMailMessageGrid();
        });
      }
    if(registry.byId('DebFundCcy')){
        registry.byId('DebFundCcy').on("change", function() {
      	  searchDebitFundMailMessageGrid();
        });
      }
  <%-- CR 913 - Rel 9.0 - vdesingu End   --%>
      <%-- notification search start --%>
  function searchNotificationGrid(){
	  require(["t360/OnDemandGrid"], function(onDemandGridSearch){   
	      var searchParms = "";
	      var notifOrgSelect = registry.byId("NotifOrg");
	      var notifOrgOid = notifOrgSelect.get('value');
	      if ( notifOrgOid && notifOrgOid.length > 0 ) {
	        searchParms += "org="+notifOrgOid;
	      }
	      var notifStatusSelect = registry.byId("NotifStatus");
	      var notifStatus = notifStatusSelect.get('value');
	      if ( notifStatus && notifStatus.length > 0 ) {
	        if ( searchParms && searchParms.length > 0 ) {
	          searchParms += "&";
	        }
	        searchParms += "status="+notifStatus;
	      } 
	      var notifReadSelect = registry.byId("Read/Unread");
	      var notifreadStatus = notifReadSelect.get('value');
	      if ( notifreadStatus && notifreadStatus.length > 0 ) {
	        if ( searchParms && searchParms.length > 0 ) {
	          searchParms += "&";
	        }
	        searchParms += "UnreadFlag="+notifreadStatus;
	      } 
	   	 onDemandGridSearch.searchDataGrid("homeNotifGrid", "HomeNotificationsDataView", searchParms);
	 resetShowCountLink("notif");
     });
  }    
  <%-- notification search end --%>
  
  <%-- invoice search --%>
  function searchInvoiceOfferGrid(){
	  require(["t360/OnDemandGrid"], function(onDemandGridSearch){   
     		var currency=(dijit.byId("Currency").value).toUpperCase();
	    	var amountFrom=(dom.byId("AmountFrom").value).toUpperCase();
	    	var amountTo=(dom.byId("AmountTo").value).toUpperCase();

	    	var dateFrom=(dom.byId("DateFrom").value).toUpperCase();
	    	var dateTo=(dom.byId("DateTo").value).toUpperCase();
	    	var fvDateFrom ="";
	    	var fvDateTo ="";

	        <%-- var filterText=filterTextCS.toUpperCase(); --%>
	         var searchParms ="currency="+currency+"&amountFrom="+amountFrom+"&amountTo="+amountTo+"&dateFrom="+dateFrom+"&dateTo="+dateTo+"&fvDateFrom="+fvDateFrom+"&fvDateTo="+fvDateTo;
	       

	        onDemandGridSearch.searchDataGrid("homeInvoicesOfferedGrid", "HomeInvoicesOfferedDataView", searchParms);
		resetShowCountLink("invoicesOffered");
	   });
  }
  
  <%-- Mail message search --%>
  function searchMailMessageGrid(){
	  
   	require(["t360/OnDemandGrid"], function(onDemandGridSearch){    
      var searchParms = "";
      var mailOrgSelect = registry.byId("MailOrg");
      var mailOrgOid = mailOrgSelect.get('value');
      if ( mailOrgOid && mailOrgOid.length > 0 ) {
        searchParms += "org="+mailOrgOid;
      }      
      var mailReadSelect = registry.byId("MailReadUnread");
      var mailOrgOidRead = mailReadSelect.get('value');
      if ( mailOrgOidRead && mailOrgOidRead.length > 0 ) 
      {
    	  if ( searchParms && searchParms.length > 0 ) 
    	  {
	          searchParms += "&";
	        
      }
        searchParms += "UnreadFlag="+mailOrgOidRead;
      }
   
   	  onDemandGridSearch.searchDataGrid("homeMailGrid", "HomeMailMessagesDataView", searchParms);
	  resetShowCountLink("mail");
     });
	   
  }
  
  <%-- Rel 9.0 CR 913 - Debit Funding Mail message search --%>
  function searchDebitFundMailMessageGrid(){
	  
  	require(["t360/OnDemandGrid"], function(onDemandGridSearch){    
		
  		var searchParms = "";
		var debFundOrgSelect = registry.byId("DebitFundMailOrg");
		var debitReadSelect = registry.byId("DebitReadUnread");
		var payDateFrom = (dom.byId("PaymentDateFrom").value).toUpperCase();
		var payDateTo = (dom.byId("PaymentDateTo").value).toUpperCase();
		var currency = registry.byId("DebFundCcy");
		var ccySelect = "";
		var dPattern = '<%=userSession.getDatePattern()%>';
		
		if ( debFundOrgSelect && (null != debFundOrgSelect.get('value')) ) {
			debFundOrgOid = debFundOrgSelect.get('value');
		}
		
		if ( debitReadSelect && (null != debitReadSelect.get('value')) ) {
			debFundOrgOid = debitReadSelect.get('value');
		}
		
		<%-- Commenting below lines dues a script error
		if( payDateFrom && (payDateFrom.value != 'Invalid Date') ){	 
			payDateFrom = dojo.date.locale.format(payDateFrom.value, {datePattern: dPattern, selector: "date"});
	     }
		if( payDateTo && (payDateTo.value != 'Invalid Date') ){	   	  
			payDateTo = dojo.date.locale.format(payDateTo.value, {datePattern: dPattern, selector: "date"});
		}
		--%>
		if( currency && (null != currency.get('value')) ){
			ccySelect = currency.get('value');
		}
		
		searchParms ="org="+debFundOrgOid+"&UnreadFlag="+debFundReadOid+"&payDateFrom="+payDateFrom+"&payDateTo="+payDateTo+"&currency="+ccySelect+"&dPattern="+dPattern;
		onDemandGridSearch.searchDataGrid("homeDebitFundMailGrid", "HomeDebitFundingMailMsgDataView", searchParms);
    });
	   
  }
  
  <%-- Search All Transaction Grid --%>
  function searchAllTransactionGrid(){
  
	  require(["t360/OnDemandGrid"], function(onDemandGridSearch){
	      var searchParms = "";
	      var allTranWorkSelect = registry.byId("AllTranWork");
	      var allTranWorkOid = allTranWorkSelect.get('value');
	      if ( allTranWorkOid && allTranWorkOid.length > 0 ) {
	        searchParms += "work="+allTranWorkOid;
	      }
	      var allTranGroupSelect = registry.byId("AllTranGroup");
	      var allTranGroup = allTranGroupSelect.get('value');
	      if ( allTranGroup && allTranGroup.length > 0 ) {
	        if ( searchParms && searchParms.length > 0 ) {
	          searchParms += "&";
	        }
	        searchParms += "grp="+allTranGroup;
	      }
	      var allTranInstrTypeSelect = registry.byId("AllTranInstrType");
	      var allTranInstrType = allTranInstrTypeSelect.get('value');
	      if ( allTranInstrType && allTranInstrType.length > 0 ) {
	        if ( searchParms && searchParms.length > 0 ) {
	          searchParms += "&";
	        }
	        searchParms += "iType="+allTranInstrType;
	      }
	      var allTranStatusSelect = registry.byId("AllTranStatus");
	      var allTranStatus = allTranStatusSelect.get('value');
	      if ( allTranStatus && allTranStatus.length > 0 ) {
	        if ( searchParms && searchParms.length > 0 ) {
	          searchParms += "&";
	        }
	        searchParms += "status="+allTranStatus;
	      }
	      
	      <%--MEer Rel 8.4 IR-23915--%>
	      <%--  Prateep Gedupudi 25/02/2013 Added confInd param for loading instruments based on confInd for loggedin user  --%>
	    <%--  searchParms += "&confInd=<%=confInd%>"; --%>
	      
    	  onDemandGridSearch.searchDataGrid("homeAllTranGrid", "AllTransactionsDataView", searchParms);
	  resetShowCountLink("allTran");
      });
  }
  <%-- Search Panel Auth --%>
  function searchPanelAuthGrid(){
	  require(["t360/OnDemandGrid"], function(onDemandGridSearch){
    	  var searchParms = "";
       	  var panelAuthTranWorkSelect = registry.byId("PanelAuthTranWork");
          var panelAuthTranWorkOid = panelAuthTranWorkSelect.get('value');
          if ( panelAuthTranWorkOid && panelAuthTranWorkOid.length > 0 ) {
            searchParms += "work="+panelAuthTranWorkOid;
          }
          var panelAuthTranGroupSelect = registry.byId("PanelAuthTranGroup");
          var panelAuthTranGroup = panelAuthTranGroupSelect.get('value');
          if ( panelAuthTranGroup && panelAuthTranGroup.length > 0 ) {
            if ( searchParms && searchParms.length > 0 ) {
              searchParms += "&";
            }
            searchParms += "grp="+panelAuthTranGroup;
          }
          var panelAuthTranInstrTypeSelect = registry.byId("PanelAuthTranInstrType");
          var panelAuthTranInstrType = panelAuthTranInstrTypeSelect.get('value');
          if ( panelAuthTranInstrType && panelAuthTranInstrType.length > 0 ) {
            if ( searchParms && searchParms.length > 0 ) {
              searchParms += "&";
            }
            searchParms += "iType="+panelAuthTranInstrType;
          }
          <%-- var panelAuthTranStatusSelect = registry.byId("PanelAuthTranStatus"); --%>
          <%-- var panelAuthTranStatus = panelAuthTranStatusSelect.get('value'); --%>
          <%-- if ( panelAuthTranStatus && panelAuthTranStatus.length > 0 ) { --%>
            <%-- if ( searchParms && searchParms.length > 0 ) { --%>
              <%-- searchParms += "&"; --%>
            <%-- } --%>
            <%-- searchParms += "status="+panelAuthTranStatus; --%>
          <%-- } --%>
          var panelAuthTransactionsSelect = registry.byId("PanelAuthTransactions");
          var panelAuthTransactions = panelAuthTransactionsSelect.get('value');
          if ( panelAuthTransactions && panelAuthTransactions.length > 0 ) {
            if ( searchParms && searchParms.length > 0 ) {
              searchParms += "&";
            }
            searchParms += "myTransactions="+panelAuthTransactions;
          }
          
          <%--  Prateep Gedupudi 25/02/2013 Added confInd param for loading instruments based on confInd for loggedin user  --%>
         <%--    searchParms += "&confInd=<%=confInd%>";--%>
          
          <%-- reuse the alltransactions dataview --%>
          onDemandGridSearch.searchDataGrid("homePanelAuthTranGrid", "PanelAuthTransactionsDataView", searchParms);
	  	  resetShowCountLink("panelAuthTran");
      });
  }
  
  <%-- search Panel Auth --%>
  function searchReceivableMatchGrid(){
	 
      require(["t360/OnDemandGrid"], function(onDemandGridSearch){
    	  var searchParms = "";
      
	      var rcvMatchWorkSelect = registry.byId("RcvMatchWork");
	      var rcvMatchWorkOid = rcvMatchWorkSelect.get('value');
	      if ( rcvMatchWorkOid && rcvMatchWorkOid.length > 0 ) {
	        searchParms += "work="+rcvMatchWorkOid;
	      }
	      var rcvMatchStatusSelect = registry.byId("RcvMatchStatus");
	      var rcvMatchStatus = rcvMatchStatusSelect.get('value');
	      if ( rcvMatchStatus && rcvMatchStatus.length > 0 ) {
	        if ( searchParms && searchParms.length > 0 ) {
	          searchParms += "&";
	        }
	        searchParms += "status="+rcvMatchStatus;
	      }
	      var rcvMatchPaySrcSelect = registry.byId("RcvMatchPaySrc");
	      var rcvMatchPaySrc = rcvMatchPaySrcSelect.get('value');
	      if ( rcvMatchPaySrc && rcvMatchPaySrc.length > 0 ) {
	        if ( searchParms && searchParms.length > 0 ) {
	          searchParms += "&";
	        }
	        searchParms += "pSrc="+rcvMatchPaySrc;
	      }
	      onDemandGridSearch.searchDataGrid("homeRcvTranGrid", "HomeReceivablesMatchingDataView", searchParms);
	      resetShowCountLink("rcvTran");
      });
  }
  
  });

<%
  // The page has been created.  Now determine if this is a new corporate customer user and
  // if so, popup the system overview window.

  if( userSession.getSecurityType().equals(TradePortalConstants.NON_ADMIN) &&
      (request.getParameter("newUser") != null) &&
      (request.getParameter("newUser").equals("true")) &&
      (!"ANZ".equals(userSession.getClientBankCode()))  ) {
%>
  function showSystemOverview() {
    widgets = 'toolbar=0,scrollbars=0,location=0,statusbar=0,menubar=1,resizable=0,width=790,height=525,left = 0,top = 0';

    url = "<%= request.getContextPath() + "/sysoverview/Overview.jsp" %>";
<%--
    // (If the system overview and standard help should be in the same
    // windows rather than different ones, replace the following
    // three lines of code with "openHelp(url);"
--%>
    sysOverviewWin = window.open(url, 'SystemOverview', widgets );
    sysOverviewWin.opener.top.name = "opener";
    sysOverviewWin.focus();
  }

  <%--cquinton 2/14/2013 ir#11715 comment out system overview for now
  showSystemOverview(); --%>
<%
   }
%>
var statusFormatter = function(columnValues){
	  var gridLink = "";
	  
		if(columnValues[2] == 'PARTIALLY_AUTHORIZED' || columnValues[2] == 'AVAILABLE_FOR_AUTHORIZATION' ){
			 var title = "<%=resMgr.getText("Home.PanelAuthTransactions.ViewAuthorisationsDialog.Title", TradePortalConstants.TEXT_BUNDLE)%>"+columnValues[1];
			 gridLink =  "<a href='javascript:opendialogViewPanelAuths(\""+title+"\",\"homeRcvTranGrid\",\"RM\");'>"+columnValues[0]+"</a>";
	  		 return gridLink;
		}else{
			return columnValues[0];
		}
	}
	
var viewAuthorisationsFormatter=function(columnValues) {
	var title = "<%=resMgr.getText("Home.PanelAuthTransactions.ViewAuthorisationsDialog.Title", TradePortalConstants.TEXT_BUNDLE)%>"+columnValues[1];
	var gridLink =  "<a href='javascript:opendialogViewPanelAuths(\""+title+"\",\"homePanelAuthTranGrid\",\"TradeCash\");'>"+"<%=resMgr.getText("Home.PanelAuthTransactions.ViewAuthorisations", TradePortalConstants.TEXT_BUNDLE)%>"+"</a>";
    return gridLink;
}

var viewInvoiceOfferFormatter = function(columnValues){
    var gridLink = "";
  <%-- IR T36000023210 RPasupulati Start --%>
	if(columnValues[1] == 'PARTIALLY_AUTHORIZED' ){
		 var title = "<%=resMgr.getText("Home.PanelAuthTransactions.ViewAuthorisationsDialog.Title", TradePortalConstants.TEXT_BUNDLE)%>"+ "<%=resMgr.getText("TransactionsMenu.InvoiceOffers", TradePortalConstants.TEXT_BUNDLE)%>";
		 var insType = '<%= TradePortalConstants.SUPPLIER_PORTAL %>';
		 gridLink =  "<a href='javascript:opendialogViewPanelAuths(\""+title+"\",\"homeInvoicesOfferedGrid\",\""+insType+"\");'>"+columnValues[0]+"</a>";
    	 return gridLink;
	}else{
		columnValues[0]="";
		<%-- IR T36000023210 RPasupulati Ends --%>
		return columnValues[0];
	}
}
<%-- vdesingu Rel8.4 CR-590 : load custom grid loadingmessage --%>
function refreshAccountBalance(){
	 require(["t360/OnDemandGrid"], function(onDemandGridSearch){ 
		 var searchParms = "refresh=true"; 
		 var gridLoadingMessage = "<%=resMgr.getText("Home.AccountBalanceLoadingMessage", TradePortalConstants.TEXT_BUNDLE) %>";
		 onDemandGridSearch.searchDataGrid("homeAcctGrid", "<%=acctGridDataView%>",
	                   searchParms, gridLoadingMessage);
	 });
}

</script>

<%--dialog divs--%>
<%
  if (!TradePortalConstants.ADMIN.equals(currentSecurityType) || TradePortalConstants.INDICATOR_YES.equals(userSession.getCustomerAccessIndicator())) {
%>
<div id="announcementDialog" ></div>
<%
  }
  //cquinton 9/12/2012 Rel portal refresh ir#t36000004008 add dialog div for reset user
  else {
    if ( sections.contains( TradePortalConstants.DASHBOARD_SECTION__LOCKED_OUT_USERS ) ) {
%>
<div id="resetUserDialog" ></div>
<%
    }
  }
%>


</body>
</html>


<%!
// CR ANZ 501 Rel 8.3 05-13-2013 jgadela  [END] -  Showing ALERT when there are any pending refdata items to approve

private String getBOGUserObjectsSql(SessionWebBean userSession, List<Object> sqlParams) {
    	
    	StringBuffer sql = new StringBuffer("");
        String clientBankID = userSession.getClientBankOid();
    	
    	sql.append("select organization_oid from bank_organization_group  where activation_status = 'ACTIVE' and p_client_bank_oid = ? ");
    	sqlParams.add(clientBankID);
    	
    	return sql.toString();
    }
private String getAdminUserObjectsSql(SessionWebBean userSession,String currentSessionUserSecType, List<Object> sqlParams) {
    	
    	StringBuffer sql = new StringBuffer("");
    	
    	String bogID = userSession.getBogOid();
    	String clientBankID = userSession.getClientBankOid();
    	String ownershipLevel = userSession.getOwnershipLevel();
    	
    	sql.append("select a.user_oid from users a, org_names_view org where activation_status = 'ACTIVE'  and a.p_owner_org_oid=org.organization_oid ");
    	
    	
    	if (ownershipLevel.equals(TradePortalConstants.OWNER_GLOBAL)) {
            sql.append("and (a.ownership_level = '");
            sql.append(TradePortalConstants.OWNER_GLOBAL);
            sql.append("' or a.ownership_level = '");
            sql.append(TradePortalConstants.OWNER_BANK);
            sql.append("')");

         }
         else if (ownershipLevel.equals(TradePortalConstants.OWNER_BANK) && currentSessionUserSecType.equalsIgnoreCase("ADMIN")) {
            sql.append(" and ((a.a_client_bank_oid = ? ");
            sqlParams.add(clientBankID);
            sql.append(" and (a.ownership_level = '");
            sql.append(TradePortalConstants.OWNER_BANK);
            sql.append("' or a.ownership_level = '");
            sql.append(TradePortalConstants.OWNER_BOG);
            sql.append("')) or a.ownership_level = '");
            sql.append(TradePortalConstants.OWNER_GLOBAL);
            sql.append("')");
         }
         else if (ownershipLevel.equals(TradePortalConstants.OWNER_BANK) && currentSessionUserSecType.equalsIgnoreCase("NON_ADMIN")) {
        	 sql.append(" and a.a_client_bank_oid = ? ");
             sqlParams.add(clientBankID);
             sql.append(" and (a.ownership_level = '");
             sql.append(TradePortalConstants.OWNER_BANK);
             sql.append("' or a.ownership_level = '");
             sql.append(TradePortalConstants.OWNER_BOG);
             sql.append("' or a.ownership_level = '");
             sql.append(TradePortalConstants.OWNER_CORPORATE);
			 sql.append("')");
         }
         else {
            sql.append(" and a.p_owner_org_oid = ? ");
            sql.append(" and a.ownership_level = ? ");
            sqlParams.add(bogID);
            sqlParams.add(ownershipLevel);
         }
    	return sql.toString();
    }
    
     private String getCorpCustObjectsSql(SessionWebBean userSession,  List<Object> sqlParams) {
    	
    	StringBuffer sql = new StringBuffer("");
    	
    	String bogID = userSession.getBogOid();
    	String clientBankID = userSession.getClientBankOid();
    	String ownershipLevel = userSession.getOwnershipLevel();
    	
    	sql.append("select   a.organization_oid from corporate_org a, bank_organization_group b where  a.a_bank_org_group_oid = b.organization_oid");
    	
    	if(ownershipLevel.equals(TradePortalConstants.OWNER_BANK)){
        		sql.append(" and b.p_client_bank_oid=?");
        		sqlParams.add(clientBankID);
        }
        else if (ownershipLevel.equals(TradePortalConstants.OWNER_BOG)){
        		sql.append(" and a_bank_org_group_oid=? ");
        		sqlParams.add(bogID);
        }
        else {
            sql.append(" and 1=0");
        }
    	
    	return sql.toString();
    }
     
     private String getCorpUserObjectsSql(SessionWebBean userSession, List<Object> sqlParams) {
     	
     	StringBuffer sql = new StringBuffer("");
     	
     	String bogID = userSession.getBogOid();
     	String clientBankID = userSession.getClientBankOid();
     	String ownershipLevel = userSession.getOwnershipLevel();
     	
     	sql.append("select a.user_oid from users a, org_names_view org where activation_status = ?  and a.p_owner_org_oid=org.organization_oid ");
     	
    	sqlParams.add("ACTIVE");
     	//If ASP login then all the corporate level changes should be visible
     			if (ownershipLevel.equals(TradePortalConstants.OWNER_GLOBAL)) {
     				sql.append("and (a.ownership_level = ? or a.ownership_level = ? or a.ownership_level = ?)");
     				sqlParams.add(TradePortalConstants.OWNER_CORPORATE);
     				sqlParams.add(TradePortalConstants.OWNER_GLOBAL);
     				sqlParams.add(TradePortalConstants.OWNER_BANK);
     			} 
     			//If BANK ADMIN login then the corporate level changes mad by BANK ADMIN/BOG/CORPORATE USER should be visible
     			else if (ownershipLevel.equals(TradePortalConstants.OWNER_BANK)) {
     				sql.append(" and a.a_client_bank_oid = ? and (a.ownership_level = ? or a.ownership_level = ? or a.ownership_level = ?)");
     				sqlParams.add(clientBankID);
     				sqlParams.add(TradePortalConstants.OWNER_CORPORATE);
     				sqlParams.add(TradePortalConstants.OWNER_BANK);
     				sqlParams.add(TradePortalConstants.OWNER_BOG);
     			} 
     			//should not come here as only ASP/BANK Admin can invoke this dataview
     			else {
     				sql.append(" and a.p_owner_org_oid = ? and a.ownership_level = ? ");
     				sqlParams.add(bogID);
     				sqlParams.add(ownershipLevel);
     			}
     	return sql.toString();
     }
    %>
