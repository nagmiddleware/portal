<%--
 *    System Overview Content, page 2
 *
 *     Copyright  � 2002                      
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
            <p class="ControlLabel">
<%= resMgr.getText("SystemOverview.WhatKindOf", TradePortalConstants.TEXT_BUNDLE) %>
</p>
            <p class="BodyText">
<%= resMgr.getText("SystemOverview.YouCanCreate", TradePortalConstants.TEXT_BUNDLE) %>

</p>
            <p class="ControlLabel"><img src="<%= resMgr.getText("SystemOverview.TransOverviewImg", TradePortalConstants.TEXT_BUNDLE) %>" width="565" height="246"></p>
