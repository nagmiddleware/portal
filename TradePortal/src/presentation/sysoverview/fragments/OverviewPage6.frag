<%--
 *    System Overview Content, page 6
 *
 *     Copyright  � 2002                      
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
            <p class="ControlLabel"><%= resMgr.getText("SystemOverview.WhatElseDoINeed", TradePortalConstants.TEXT_BUNDLE) %></p>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td colspan="3">
                  <p class="Subnav"><%= resMgr.getText("SystemOverview.SaveYourWork", TradePortalConstants.TEXT_BUNDLE) %></p>
                </td>
                <td>&nbsp;</td>
              </tr>
              <tr> 
                <td align="center" valign="middle" class="ColorGrey" width="120" nowrap><span class="ControlLabel"> 
                  </span><a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image10','','/portal/images/<%=  resMgr.getText("common.SaveCloseImgRO", TradePortalConstants.TEXT_BUNDLE)%>',1)"><img name="Image10" border="0" src="/portal/images/<%=  resMgr.getText("common.SaveCloseImg", TradePortalConstants.TEXT_BUNDLE)%>" width="96" height="17"></a> 
                </td>
                <td width="15" nowrap>&nbsp;</td>
                <td class="BodyText">
                   <%= resMgr.getText("SystemOverview.YouMustPressSave", TradePortalConstants.TEXT_BUNDLE) %>
                </td>
                <td>&nbsp;</td>
              </tr>
            </table>
            <br>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td colspan="3">
                  <p class="Subnav">
                   <%= resMgr.getText("SystemOverview.AvoidUsingBrowserButtons", TradePortalConstants.TEXT_BUNDLE) %>
                  </p>
                </td>
                <td>&nbsp;</td>
              </tr>
              <tr> 
                <td align="left" valign="top"><span class="ControlLabel"> </span><img src="/portal/images/BackForwardNO.gif" width="199" height="100"> 
                </td>
                <td width="15" nowrap>&nbsp;</td>
                <td class="BodyText" align="left" valign="top">
                   <%= resMgr.getText("SystemOverview.SystemIncludesControls", TradePortalConstants.TEXT_BUNDLE) %>
                </td>
                <td>&nbsp;</td>
              </tr>
            </table>
            <br>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td colspan="3">
                  <p class="Subnav">
                      <%= resMgr.getText("SystemOverview.SessionTimeout", TradePortalConstants.TEXT_BUNDLE) %>
                  </p>
                </td>
                <td>&nbsp;</td>
              </tr>
              <tr> 
                <td align="left" valign="top"> 
                  <p><img src="/portal/images/clock.gif" width="83" height="100"> </p>
                  </td>
                <td width="15" nowrap>&nbsp;</td>
                <td class="BodyText" align="left" valign="top">
                    <%= resMgr.getText("SystemOverview.AfterSessionTimeout", TradePortalConstants.TEXT_BUNDLE) %>
</td>
                <td>&nbsp;</td>
              </tr>
            </table>
            

