<%--
 *    System Overview Content, page 1
 *
 *     Copyright  � 2002
 *     American Management Systems, Incorporated
 *     All rights reserved
--%>
            <p class="ControlLabel"><%= resMgr.getText("SystemOverview.WhatIsTradeServices", TradePortalConstants.TEXT_BUNDLE) %></p>
            <table width="665" border="0" cellpadding="0" cellspacing="0" height="30">
              <tr>
                <td width="122pix" valign="middle" align="center" class="HeaderColor"><a href="#" onMouseOver="MM_showHideLayers('messages','','hide','instruments','','hide','Referencedata','','hide','home','','hide','help','','hide','reports','','hide','logout','','hide')"><img src="<%=logoFilename%>" border="0"></a></td>
                <td width="60%" class="HeaderColor" align="center" valign="middle">
                  <p class="ControlLabelwhite">&nbsp;</p>
                </td>
                <td width="25%" nowrap align="right" valign="middle" class="HeaderColor"><a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image11','','<%= brandedImagePath + logoutImgRO%>',1);MM_showHideLayers('messages','','hide','instruments','','hide','Referencedata','','hide','home','','hide','help','','hide','reports','','hide','logout','','show')"><img name="Image11" border="0" src="<%= brandedImagePath + logoutImg%>" width="96" height="17"></a>
</td>
                <td width="15" nowrap class="HeaderColor">&nbsp;</td>
                <td width="25%" nowrap align="right" valign="middle" class="HeaderColor"><a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image11','','<%= brandedImagePath + homeImgRO%>',1);MM_showHideLayers('messages','','hide','instruments','','hide','Referencedata','','hide','home','','hide','help','','hide','reports','','hide','logout','','show')"><img name="Image11" border="0" src="<%= brandedImagePath + homeImg%>" width="96" height="17"></a>
</td>
                <td width="15" nowrap class="HeaderColor">&nbsp;</td>
                <td width="25%" nowrap align="right" valign="middle" class="HeaderColor"><a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image11','','<%= brandedImagePath + helpImgRO%>',1);MM_showHideLayers('messages','','hide','instruments','','hide','Referencedata','','hide','home','','hide','help','','hide','reports','','hide','logout','','show')"><img name="Image11" border="0" src="<%= brandedImagePath + helpImg%>" width="96" height="17"></a>
</td>
                &nbsp;
                <td width="20pix" nowrap align="right" valign="middle" class="HeaderColor">&nbsp;
                </td>
              </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td align="center" valign="bottom"><img src="<%= brandedImagePath + leftCap%>" width="12" height="34"><a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image101','','<%= brandedImagePath + messagesImgRO%>',1);MM_showHideLayers('messages','','show','instruments','','hide','Referencedata','','hide','home','','hide','help','','hide','reports','','hide','logout','','hide')"><img name="Image101" border="0" src="<%= brandedImagePath + messagesImg%>" class="BodyTextwhite"></a><a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image151','','<%= brandedImagePath + transactionsImgRO%>',1);MM_showHideLayers('messages','','hide','instruments','','show','Referencedata','','hide','home','','hide','help','','hide','reports','','hide','logout','','hide')"><img name="Image151" border="0" src="<%= brandedImagePath + transactionsImg%>" ></a><a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image161','','<%= brandedImagePath + reportsImgRO%>',1);MM_showHideLayers('messages','','hide','instruments','','hide','Referencedata','','hide','home','','hide','help','','hide','reports','','show','logout','','hide')"><img name="Image161" border="0" src="<%= brandedImagePath + reportsImg%>" height="34"></a><a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image171','','<%= brandedImagePath + refdataImgRO%>',1);MM_showHideLayers('messages','','hide','instruments','','hide','Referencedata','','show','home','','hide','help','','hide','reports','','hide','logout','','hide')"><img name="Image171" border="0" src="<%= brandedImagePath + refdataImg%>"></a></td>
                <td width="155" background="<%= brandedImagePath + barFill%>" align="center" valign="bottom" nowrap height="17">&nbsp;</td>

                <td align="center" valign="bottom"><a href="#" onMouseOver="MM_showHideLayers('messages','','hide','instruments','','hide','Referencedata','','hide','home','','hide','help','','hide','reports','','hide','logout','','hide')"><img src="<%= brandedImagePath + rightCap%>" width="18" height="34" border="0"></a></td>
                <td width="100%">&nbsp;</td>
              </tr>
            </table>
            <p class="BodyText">
                <%= resMgr.getText("SystemOverview.SystemAllows", TradePortalConstants.TEXT_BUNDLE) %>
            </p>
            <p class="BodyText">
                <%= resMgr.getText("SystemOverview.SystemIncludes", TradePortalConstants.TEXT_BUNDLE) %>
              <br>
              <br>
            </p>

