<%--
*******************************************************************************
                        View Structured PO Detail

  Description:
     This page is used to display the detail for a purchase order line item.
   It refers common fragment used to display the PO detail

*******************************************************************************
--%>

<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>

<%
  String closeLink = "goToViewStructuredPO";
%>
<%@ include file="/autolc/PurchaseOrderStructuredDetail.frag"%>
