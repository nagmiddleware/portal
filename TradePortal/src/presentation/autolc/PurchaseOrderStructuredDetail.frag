<!DOCTYPE HTML>
<%--
*******************************************************************************
                        Purchase Order Line Item Detail

  Description:
     This page is used to display the detail for a purchase order line item.
  Although it is modeled after the standard ref data pages, this is a readonly
  page.  It is only used to display data and can never update anything.

*******************************************************************************
--%>

<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.html.*,java.text.SimpleDateFormat,java.util.*" %>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" 
   scope="session">
</jsp:useBean>


<%-- ************** Data retrieval page setup begins here ****************  --%>

<%

  WidgetFactory widgetFactory = new WidgetFactory(resMgr, userSession);
  
  boolean isReadOnly = true;
	
  String oid = "";
  String sValue = null;
  boolean showDelete = true;
  boolean showSave   = false;
  String partialShipAllowed= null;
  boolean getDataFromDoc = false;
  boolean retrieveFromDB = false;
  String transactionOid=null;
  Vector vVector;
  int numItems;
  int iLoop;
  String sellerAddress1 = null;
  String sellerAddress2 = null;
  String sellerBank    = null;
  String sellerState = null;
  String sellerCity = null;
  String postCode = null;
  String country = null;
  DocumentHandler doc;
  QueryListView queryListView = null;
  POUtility poUtility = new POUtility();
  Hashtable fields = null;
  StringBuffer sql = new StringBuffer();
  PurchaseOrderLineItemWebBean lineItem = beanMgr.createBean(PurchaseOrderLineItemWebBean.class, "PurchaseOrderLineItem");

  PurchaseOrderWebBean uploadDefn = beanMgr.createBean(PurchaseOrderWebBean.class, "PurchaseOrder");

  String loginLocale;
  String linkArgs[] = {"","","","","",""};
  String userTimeZone = userSession.getTimeZone(); //BSL IR BLUM041758623 04/23/2012 Rel 8.0 ADD
%>

<%
  // Get the webbeans for the login in user and his security profile.  Set the
  // readonly access appropriately.
  loginLocale = userSession.getUserLocale();

  // Get the document from the cache.  We'll use it to repopulate the screen 
  // if the user was entering data with errors.
  doc = formMgr.getFromDocCache();

  Debug.debug("doc from cache is " + doc.toString());

  if (doc.getDocumentNode("/In/POLineItem") == null ) {
     // The document does not exist in the cache.  We are entering the page from
     // the listview page (meaning we're opening an existing item)

     oid = request.getParameter("purchase_order_oid");
     if (oid != null) {
        oid = EncryptDecrypt.decryptStringUsingTripleDes(oid, userSession.getSecretKey());
     }

     if (oid == null) {
       // Should never happen as we can only come to this page for an existing
       // line item.
     } else {
       // We have a good oid so we can retrieve.
       getDataFromDoc = false;
       retrieveFromDB = true;
     }

  } else {
     // The doc does exist so check for errors.  If highest severity < WARNING,
     // its a good update.

     String maxError = doc.getAttribute("/Error/maxerrorseverity");

     if (maxError.compareTo(
                  String.valueOf(ErrorManager.WARNING_ERROR_SEVERITY)) < 0) {
        // We've returned from a delete that was successful
        // We shouldn't be here.  Forward to the TransactionsHome page.
        formMgr.setCurrPage("UploadsHome");
        String physicalPage = NavigationManager.getNavMan().getPhysicalPage("UploadsHome", request);

%>
        <jsp:forward page='<%=physicalPage%>' />
<%
        return;
     } else {
        // We've returned from a save/update/delete but have errors.
        // We will display data from the cache.  Determine if we're
        // in insertMode by looking for a '0' oid in the input section
        // of the doc.

        retrieveFromDB = false;
        getDataFromDoc = true;

        // Use oid from input doc.
        oid = doc.getAttribute("/In/POLineItem/purchase_order_oid");
     }
  }

  if (retrieveFromDB) {
     // Attempt to retrieve the item.  It should always work.  Still, we'll
     // check for a blank oid -- this indicates record not found.  Display error.

     getDataFromDoc = false;
	
     uploadDefn.setAttribute("purchase_order_oid", oid);
     uploadDefn.getDataFromAppServer();
	
     if (uploadDefn.getAttribute("purchase_order_oid").equals("")) {
       oid = "0";
     }
  } 

  if (getDataFromDoc) {
     // Populate the line item bean with the input doc.
     try {
        uploadDefn.populateFromXmlDoc(doc.getComponent("/In"));
     } catch (Exception e) {
        out.println("Contact Administrator: Unable to get PO LineItem attributes "
             + " for oid: " + oid + " " + e.toString());
     }
  }

  // This println is useful for debugging the state of the jsp.
  Debug.debug("retrieveFromDB: " + retrieveFromDB + " getDataFromDoc: "
              + getDataFromDoc + " oid: " + oid);

%> 

<%-- ********************* HTML for page begins here *********************  --%>

<%-- Body tag included as part of common header --%>
<jsp:include page="/common/Header.jsp">
   <jsp:param name="includeNavigationBarFlag" 
              value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeErrorSectionFlag"  
              value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>

<jsp:include page="/common/ButtonPrep.jsp" />

<%
  String instrType = resMgr.getText("POStructureLineItemDetail.PurchaseOrders", TradePortalConstants.TEXT_BUNDLE),
    poItem = resMgr.getText("POLineItemDetail.POItem", TradePortalConstants.TEXT_BUNDLE);
%>
<div class="pageMain">
  <div class="pageContent">
<%
  String pageTitleKey = "SecondaryNavigation.PurchaseOrders",
    helpUrl = "customer/structured_purchase_order_details.htm";
%>
			
    <jsp:include page="/common/PageHeader.jsp">
      <jsp:param name="titleKey" value="<%=pageTitleKey%>" />
      <jsp:param name="item1Key" value="SecondaryNavigation.PurchaseOrders.POItems" />
      <jsp:param name="helpUrl"  value="<%=helpUrl%>" />
    </jsp:include>

    <div class="subHeaderDivider"></div>
			
    <div class="pageSubHeader">
      <span class="pageSubHeaderItem">
        <span><%=instrType%></span>
        <span>&nbsp;-&nbsp;</span>
        <span><%=uploadDefn.getAttribute("purchase_order_num")%></span>
      </span>
      <span class="pageSubHeader-right">
<%
  linkArgs[0] = TradePortalConstants.PDF_PO_DETAILS;
  linkArgs[1] = EncryptDecrypt.encryptStringUsingTripleDes(uploadDefn.getAttribute("purchase_order_oid"));
  linkArgs[2] = "en_US";
 
  linkArgs[3] = userSession.getBrandingDirectory();
  String linkString = formMgr.getDocumentLinkAsURLInFrame(resMgr.getText("UploadInvoiceDetail.PrintInvoiceDetails",
    TradePortalConstants.TEXT_BUNDLE),
    TradePortalConstants.PDF_PO_DETAILS,
    linkArgs,
    response) ;
%>

<button data-dojo-type="dijit.form.Button" name="PrintViewButton" id="PrintViewButton" type="button" class="pageSubHeaderButton">
<%=resMgr.getText("TransactionSubHeader.PrintView",TradePortalConstants.TEXT_BUNDLE)%>
</button>

        
        <button data-dojo-type="dijit.form.Button" name="CloseButton" id="CloseButton" type="button" class="pageSubHeaderButton">
          <%=resMgr.getText("common.CloseText",TradePortalConstants.TEXT_BUNDLE)%>
          <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
            document.location.href =
              '<%= formMgr.getLinkAsUrl("goToUploadsHome", response) %>';
          </script>
        </button> 
        <%=widgetFactory.createHoverHelp("CloseButton", "CloseHoverText") %>
      </span>
      <div style="clear:both;"></div>
    </div>

<form name="POLineItemDetail" method="post" data-dojo-type="dijit.form.Form" action="<%=formMgr.getSubmitAction(response)%>">

<div class="formContentNoSidebar">
		  		
  <input type=hidden value="" name=buttonName>

<%
  // Store values such as the userid, his security rights, and his org in a secure
  // hashtable for the form.  The mediator needs this information.
  Hashtable secureParms = new Hashtable();
  secureParms.put("UserOid", userSession.getUserOid());	
  secureParms.put("purchase_order_oid", oid);
%>

<%= formMgr.getFormInstanceAsInputField("POLineItemDetailForm", secureParms) %>

<%
//This is done to display date based on Locale.
Locale objLocale = new Locale(loginLocale.substring(0, 2), loginLocale.substring(3, 5));
SimpleDateFormat sdf=new SimpleDateFormat("dd MMM yyyy hh:mm:ss",objLocale);
SimpleDateFormat isoDateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S",objLocale);
Date date= isoDateFormatter.parse(uploadDefn.getAttribute("creation_date_time"));
String subHdr = resMgr.getText("PurchaseOrderDetails.PurchaseOrderTimeUploaded", TradePortalConstants.TEXT_BUNDLE)+" "+sdf.format(date);
%>
<%= widgetFactory.createWideSubsectionHeader(subHdr,true,false,false,"") %>

<table class="poContent">
	<tr>
		<td width="25%" > 
		<%= widgetFactory.createTextField("PurchaseOrderNum","PurchaseOrderDetails.PurchaseOrderNum",uploadDefn.getAttribute("purchase_order_num"),"8",true) %>
		</td>
		<td width="25%" >
			<%= widgetFactory.createTextField("PurchaseOrderType","PurchaseOrderDetails.PurchaseOrderType",uploadDefn.getAttribute("purchase_order_type"),"8",true) %>
		</td>
		<td width="25%" >
			<%= widgetFactory.createTextField("Currency","PurchaseOrderDetails.Currency",uploadDefn.getAttribute("currency"),"8",true) %>
		</td>
		<td width="25%" >
		<% String displayAmount = TPCurrencyUtility.getDisplayAmount(uploadDefn.getAttribute("amount").toString(),uploadDefn.getAttribute("currency").toString(), loginLocale);%>
			<%= widgetFactory.createTextField("Amount","PurchaseOrderDetails.Amount",displayAmount,"8",true) %>
		</td>
	</tr>  		
	<tr>
		<td width="25%" > 
		<%= widgetFactory.createTextField("SellerName","PurchaseOrderDetails.SellerName",uploadDefn.getAttribute("seller_name"),"8",true) %>
		</td>
		<td width="25%" >
			<%= widgetFactory.createDateField("IssueDate","PurchaseOrderDetails.IssueDate",uploadDefn.getAttribute("issue_date"), true)%>
		</td>
		<% if(TradePortalConstants.INDICATOR_YES.equals(uploadDefn.getAttribute("partial_shipment_allowed")))
			 partialShipAllowed = "Yes";
		  else
			partialShipAllowed = "No";
		%>
		<td width="25%" >
			<%= widgetFactory.createTextField("PartialShipAllow","PurchaseOrderDetails.PartialShipAllow",partialShipAllowed,"8",true) %>
		</td>
			<%
	    	String sqlQry= "select complete_instrument_id from instrument where instrument_oid = ? ";
		
		String varNameForResult="";
		//Srinivasu_D Rel9.2 12/01/2014 - null check found in 91 UAT logs
		if(StringFunction.isNotBlank(uploadDefn.getAttribute("instrument_oid"))){
		    DocumentHandler results = DatabaseQueryBean.getXmlResultSet(sqlQry, false, new Object[]{uploadDefn.getAttribute("instrument_oid")});
		    if(results != null){
		    varNameForResult= results.getAttribute("/ResultSetRecord(0)/COMPLETE_INSTRUMENT_ID");
		    }
			}
		    if (!InstrumentServices.isBlankOrWhiteSpace(varNameForResult))
		    {
		    %>
				<td width="25%" >
					<%= widgetFactory.createTextField("LinkedToInstrument","PurchaseOrderDetails.LinkedToInstrument",varNameForResult,"8",true) %>
				</td>
		  <%}%>
	</tr> 
	</table>
	  <div class="subHeaderDivider"></div> 
	  <br>
	  
	 <table class="poContent" >
		
	<tr width="100%">
		<td width="25%" nowrap> 
		<%= widgetFactory.createTextField("Incoterm","PurchaseOrderDetails.Incoterm",uploadDefn.getAttribute("incoterm"),"8",true) %>
		</td>
		<td width="25%" nowrap>
			<%= widgetFactory.createTextField("IncotermLoc","PurchaseOrderDetails.IncotermLoc",uploadDefn.getAttribute("incoterm_location"),"8",true) %>
		</td>
		<td width="25%" nowrap>
			<%= widgetFactory.createDateField("LatestShipDate","PurchaseOrderDetails.LatestShipDate",uploadDefn.getAttribute("latest_shipment_date"), true)%>
		</td>
	</tr>
	
  </table>     
  
  <table class="poContent">
  <tr>
  	<td width="50%">
  	<%= widgetFactory.createTextField("GoodsDesc","PurchaseOrderDetails.GoodsDesc",uploadDefn.getAttribute("goods_description"),"8",true) %>
  	</td>
  	<td width="50%">
  	</td>
  </tr>
  </table>
  <%-- Buyer User Defined Details--%>
  
  <table class="poContent">
  	<%
		int colCount = 0;
		for(int x=1; x<=TradePortalConstants.PO_BUYER_OR_SELLER_USER_DEF_FIELD; x++) {
		   if(!InstrumentServices.isBlank(uploadDefn.getAttribute("buyer_user_def"+ x +"_label")) && !InstrumentServices.isBlank(uploadDefn.getAttribute("buyer_user_def"+ x +"_value"))){
			   String attributeName = (String)uploadDefn.getAttribute("buyer_user_def"+ x +"_label");
			   String value = (String)uploadDefn.getAttribute("buyer_user_def"+x+"_value");
			   colCount++;
		    %>
		    <tr>
				<td width="50%" >
				<%= widgetFactory.createTextField(attributeName, attributeName, value, "8", true) %>
				</td>
				<td width="50%">
				</td>
			</tr>	
		<% }
		   
		}%>
	</table>
	
	<%-- Seller User Defined Details--%>
	
	<table class="poContent" >
	  	<%
			int colCount1 = 0;
			for(int x=1; x<=TradePortalConstants.PO_BUYER_OR_SELLER_USER_DEF_FIELD; x++) {
				 if(!InstrumentServices.isBlank(uploadDefn.getAttribute("seller_user_def"+ x +"_label")) && !InstrumentServices.isBlank(uploadDefn.getAttribute("seller_user_def"+ x +"_value"))){
				   String attributeName = (String)uploadDefn.getAttribute("seller_user_def"+ x +"_label");
				   String value = (String)uploadDefn.getAttribute("seller_user_def"+x+"_value");
				   colCount1++;
			    %>
			    <tr>
			    <td width="50%">
					<%= widgetFactory.createTextField(attributeName,attributeName,value,"8",true) %>
				</td>
				<td width="50%">
				</td>
				</tr>	
			<% }
			   
			}%>
		</table>
	


		<div class="subHeaderDivider"></div> 

<%
    DGridFactory dgFactory = new DGridFactory(resMgr, userSession, formMgr, beanMgr, response);
    String gridHtml = "";
	String gridLayout = "";	

	gridHtml = dgFactory.createDataGrid("PurchaseOrderHistoryLogDataGridId","PurchaseOrderHistoryLogDataGrid", null);
	gridLayout = dgFactory.createGridLayout("PurchaseOrderHistoryLogDataGrid");
		  
%><div class="formItem">
	<div class="gridHeader">
	<span class="gridHeaderTitle">
	  <%=resMgr.getText("PurchaseOrderDetails.POStatusLog",TradePortalConstants.TEXT_BUNDLE)%>
	</span>
	</div>
	
		<%=gridHtml %>
		</div>	     
</div>
</form>
</div>
</div>

<jsp:include page="/common/Footer.jsp">
   <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>

<script type="text/javascript">
require(["t360/OnDemandGrid", "dojo/dom-construct"], function(onDemandGrid, domConstruct){
<%--get grid layout from the data grid factory--%>
var logGridLayout = <%= gridLayout %>;
<%--set the initial search parms--%>
<%--Rel 9.2 XSS - CID 11437--%>
var init='purchase_order_oid=<%=StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(uploadDefn.getAttribute("purchase_order_oid")))%>';

<%--create the grid, attaching it to the div id specified in dataGrid.jsp--%>
var viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("PurchaseOrderHistoryLogDataView",userSession.getSecretKey())%>";  <%--Rel9.2 IR T36000032596 --%>
var invoiceHistoryLogGrid =  onDemandGrid.createOnDemandGrid("PurchaseOrderHistoryLogDataGridId", viewName, 
  			logGridLayout, init,'0'); 
});
  

require(["dijit/registry", "dojo/on", "dojo/ready"],
  function(registry, on, ready ) {
	ready(function() {
	var printButton = registry.byId("PrintViewButton");
	if ( printButton ) {
		printButton.on("click", function() {
			window.open("<%= linkString %>","<%=TradePortalConstants.PDF_PO_DETAILS%>");
		});
	}
	});
 });



</script>
<jsp:include page="/common/dGridShowCountFooter.jsp">
  <jsp:param name="gridId" value="PurchaseOrderHistoryLogDataGridId" />
</jsp:include>
</body>
</html>
