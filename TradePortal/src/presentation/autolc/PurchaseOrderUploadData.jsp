<%--
*******************************************************************************
                        Upload Purchase Order Data File Page

  Description:  The Upload Purchase Order Data File page is used for uploading
                a purchase order data file from the user's computer to the
                Trade Portal and starting the process of creating appropriate 
                PO line items and Import LC Issue/Amend transactions (according 
                to options selected by the user in this page). The page will 
                display an informational message indicating whether any current
                PO data files are being processed for the user's organization.
                If the user presses the Upload button while another data file 
                is still being processed, an error will be issued and the user
                will be returned to the Transactions Home page. Otherwise, the
                PO data file and all options made by the user will be saved off
                for a background process to collect, parse, and group as 
                necessary (i.e., using the AutoLCAgent and 
                AutoLCCreateMediator).

*******************************************************************************
--%>

<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.html.*, com.ams.tradeportal.busobj.util.*,java.util.*" %>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager"
    scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" 
   scope="session">
</jsp:useBean>

<%-- ************** Data retrieval page setup begins here ****************  --%>

<%
   WidgetFactory widgetFactory = new WidgetFactory(resMgr);
   MediatorServices   mediatorServices         = null;
   DocumentHandler    poUploadDefinitionDoc    = null;
   DocumentHandler    xmlDoc                   = null;
   StringBuffer       dropdownOptions          = new StringBuffer();
   StringBuffer       sqlQuery                 = new StringBuffer();
   Hashtable          secureParms              = new Hashtable();
   boolean            defaultRadioButtonValue1 = true;
   boolean            defaultRadioButtonValue2 = false;
   boolean            defaultRadioButtonValueA = false;
   boolean            defaultRadioButtonValueB = false;
   boolean            defaultRadioButtonValueC = false;
   boolean            defaultRadioButtonValueD = false;  
   Vector             poUploadDefinitionList   = null;
   String             poItemsGroupingOption    = null;
   String             poUploadDefinitionOid    = null;
   String             poDataOption             = null;
   String             userOrgOid               = null;
   String             hasErrors                = null;
   String             newLink                  = null;
   String             onLoad                   = null;
   String             poUploadInstrType        = null;
   boolean            readOnlyATP              = false;
   boolean            readOnlyDLC              = false;

   String             userSecurityRights        = null;
   userSecurityRights  = userSession.getSecurityRights();
   String             userOrgAutoATPCreateIndicator = null;
   String             userOrgAutoLCCreateIndicator  = null;
   userOrgAutoATPCreateIndicator = userSession.getOrgAutoATPCreateIndicator();
   userOrgAutoLCCreateIndicator = userSession.getOrgAutoLCCreateIndicator();

   boolean canProcessPOForDLC = SecurityAccess.canProcessPurchaseOrder(userSecurityRights, InstrumentType.IMPORT_DLC);
   boolean canProcessPOForATP = SecurityAccess.canProcessPurchaseOrder(userSecurityRights, InstrumentType.APPROVAL_TO_PAY);
   
   // Replaced canProcessPOForATP and canProcessPOForDLC
   if ((userOrgAutoATPCreateIndicator != null && !userOrgAutoATPCreateIndicator.equals(TradePortalConstants.INDICATOR_YES)) || !canProcessPOForATP) {
      readOnlyATP = true;
   }
   
   if ((userOrgAutoLCCreateIndicator != null && !userOrgAutoLCCreateIndicator.equals(TradePortalConstants.INDICATOR_YES)) || !canProcessPOForDLC) {
      readOnlyDLC = true;
   }

   // Retrieve the user's org oid
   userOrgOid = userSession.getOwnerOrgOid();
   List<Object> sqlParams = new ArrayList<Object>();
   // This is the query used for populating the PO Upload Definition dropdown list;
   // it retrieves all PO Upload Definitions that belong to the corporate customer 
   // user's org that are used for uploading or for both uploading and manual entry
   sqlQuery.append("select po_upload_definition_oid, name");
   sqlQuery.append(" from po_upload_definition"); 
   sqlQuery.append(" where a_owner_org_oid in (?");
   sqlParams.add(userSession.getOwnerOrgOid());

   // Also include PO definitions from the user's actual organization if using subsidiary access
   if(userSession.showOrgDataUnderSubAccess())
    {
      sqlQuery.append(",?");
      sqlParams.add(userSession.getSavedUserSession().getOwnerOrgOid());
    }

   sqlQuery.append(") and definition_type in (?,?) ");
   sqlParams.add("UPLOAD");
   sqlParams.add("UPLOAD_AND_MANUAL");
   sqlQuery.append(" order by ");
   sqlQuery.append(resMgr.localizeOrderBy("name"));

   poUploadDefinitionDoc  = DatabaseQueryBean.getXmlResultSet(sqlQuery.toString(), false, sqlParams);

   if(poUploadDefinitionDoc == null)
      poUploadDefinitionDoc = new DocumentHandler();

   // Get the current xml doc from the doc cache and check for errors
   xmlDoc = formMgr.getFromDocCache();

   hasErrors = xmlDoc.getAttribute("/In/HasErrors");

   if ((hasErrors != null) && (hasErrors.equals(TradePortalConstants.INDICATOR_YES)))
   {
      poUploadDefinitionOid = xmlDoc.getAttribute("/In/POUploadDefinition");
      poDataOption          = xmlDoc.getAttribute("/In/PODataOption");

      if (poDataOption.equals(TradePortalConstants.GROUP_PO_LINE_ITEMS))
      {
         poItemsGroupingOption = xmlDoc.getAttribute("/In/POItemsGroupingOption");

         defaultRadioButtonValue1 = false;
         defaultRadioButtonValue2 = true;

         if (poItemsGroupingOption.equals(TradePortalConstants.INCLUDE_ALL_PO_LINE_ITEMS))
         {
            defaultRadioButtonValueA = true;
            defaultRadioButtonValueB = false;
         } else {
            if (poItemsGroupingOption.equals(TradePortalConstants.USE_ONLY_FILE_PO_LINE_ITEMS)) {
                defaultRadioButtonValueA = false;
                defaultRadioButtonValueB = true; 
            } else {
                defaultRadioButtonValueA = false;
                defaultRadioButtonValueB = false;   
            }  
         }
         
         poUploadInstrType = xmlDoc.getAttribute("/In/POUploadInstrOption");
         
         if (poUploadInstrType.equals(InstrumentType.IMPORT_DLC)) {
                defaultRadioButtonValueC = true;
                defaultRadioButtonValueD = false;  
         } else {
             if (poUploadInstrType.equals(InstrumentType.APPROVAL_TO_PAY)) {
                defaultRadioButtonValueC = false;
                defaultRadioButtonValueD = true; 
             } else {
                defaultRadioButtonValueC = false;
                defaultRadioButtonValueD = false;    
             }
         }
         
      }
      else
      {
         defaultRadioButtonValue1 = true;
         defaultRadioButtonValue2 = false;
         defaultRadioButtonValueA = false;
         defaultRadioButtonValueB = false;
         defaultRadioButtonValueC = false;
         defaultRadioButtonValueD = false;
      }
   }
  else
   {
      // Default the PO Upload Definition dropdown to be the default definition for this corporate org
      String defaultDefinitionSql = "select po_upload_definition_oid from po_upload_definition where default_flag = ? and a_owner_org_oid = ?";
      DocumentHandler results = DatabaseQueryBean.getXmlResultSet(defaultDefinitionSql, false, new Object[]{"Y",userSession.getOwnerOrgOid()});
     

      if(results != null)
       {
          poUploadDefinitionOid = results.getAttribute("/ResultSetRecord(0)/PO_UPLOAD_DEFINITION_OID");
       }
   }

   // If the user's org currently has other Purchase Orders being uploaded, issue an 
   // informational message indicating this to the user; do this by checking the active_po_upload 
   // table for active PO's that belong to the user's org.
   String whereClause = "a_corp_org_oid = ?";
   Debug.debug("whereClause=="+whereClause.toString());

   if (DatabaseQueryBean.getCount("active_po_upload_oid", "active_po_upload", whereClause, false, new Object[]{userOrgOid}) != 0)
   {
      mediatorServices = new MediatorServices();
      mediatorServices.getErrorManager().setLocaleName(resMgr.getResourceLocale());
      mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, 
                                                    TradePortalConstants.AUTOLC_PROCESS_UPLOADING);
      mediatorServices.addErrorInfo();

      xmlDoc.setComponent("/Error", mediatorServices.getErrorDoc());

      formMgr.storeInDocCache("default.doc", xmlDoc);
   }

   // Set the focus to the Purchase Order File Location field
   onLoad = "document.forms[0].PurchaseOrderDataFilePath.focus();";

%>

<%-- ********************* JavaScript for page begins here *********************  --%>



<%-- ********************* HTML for page begins here *********************  --%>
<jsp:include page="/common/Header.jsp">
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeErrorSectionFlag"  value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="additionalOnLoad"         value="<%=onLoad%>" />
</jsp:include>

<div class="pageMainNoSidebar">
<div class="pageContent">
    <jsp:include page="/common/PageHeader.jsp">
      <jsp:param name="titleKey" value="SecondaryNavigation.PurchaseOrders" />
      <jsp:param name="item1Key" value="LocateUploadPO.Header" />
      <jsp:param name="helpUrl"  value="customer/upload_unstructured_po_file.htm" />
    </jsp:include>
    <div class="subHeaderDivider"></div>
    <%--cquinton 11/1/2012 ir#7015 change return link for close button--%>
    <div class="pageSubHeader">
      <span class="pageSubHeaderItem">
        <%=resMgr.getText("LocateUploadPO.Header", TradePortalConstants.TEXT_BUNDLE)%>
      </span>
      <span class="pageSubHeader-right">
	<button data-dojo-type="dijit.form.Button" type="button" class="pageSubHeaderButton" id="UploadFileButton">
          <%=resMgr.getText("common.UploadFileText", TradePortalConstants.TEXT_BUNDLE)%>
          <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
            
	   		setButtonPressed('UploadFile', 0);
			document.forms[0].submit();
          </script>
        </button>
        <%=widgetFactory.createHoverHelp("UploadFileButton", "LocateUploadPO.UploadFile") %>
        <button data-dojo-type="dijit.form.Button" name="CloseButton" id="CloseButton" type="button" class="pageSubHeaderButton">
          <%=resMgr.getText("common.CloseText",TradePortalConstants.TEXT_BUNDLE)%>
          <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
            document.location.href =
              "<%= formMgr.getLinkAsUrl("goToUploadsHome", response) %>";
          </script>
        </button> 
        <%=widgetFactory.createHoverHelp("CloseButton", "CloseHoverText") %>
      </span>
      <div style="clear:both;"></div>
    </div>

<%--
   Note: The following form MUST have the 'enctype' attribute specified as "multipart/form-data"
   in order for the contents of the file selected by the user to be uploaded with the other
   form parameter values (i.e., hidden field values, radio button values, etc.). It also must go 
   through the Purchase Order File Upload Servlet, as this servlet handles all file PO upload 
   requests and forwards the user to the appropriate page afterwards.
--%>
<form name="UploadPurchaseOrderDataForm" id="UploadPurchaseOrderDataForm" method="POST" data-dojo-type="dijit.form.Form"  enctype="multipart/form-data"
      action="<%= response.encodeURL(request.getContextPath()+ "/transactions/PurchaseOrderFileUploadServlet.jsp") %>"> 

  <jsp:include page="/common/ErrorSection.jsp" />     
 <div class="formContentNoSidebar">		  
  		  <%= widgetFactory.createWideSubsectionHeader("LocateUploadPO.Step1") %>
  		  <%= widgetFactory.createFileField("PurchaseOrderDataFilePath", "","class='char40'","", false) %>
  		  <%=widgetFactory.createHoverHelp("PurchaseOrderDataFilePath","LocateUploadPO.Step1") %>
          
    	  <%= widgetFactory.createWideSubsectionHeader("LocateUploadPO.Step2") %>
         <div class="formItem">  
         <%=widgetFactory.createRadioButtonField("PODataOption","TradePortalConstants.MANUALLY_ADD_PO_LINE_ITEMS","LocateUploadPO.MakePOLineItemsAvailable",TradePortalConstants.MANUALLY_ADD_PO_LINE_ITEMS,defaultRadioButtonValue1, false," labelClass=\"formWidth\" onClick=\"setPOItemsGroupingOption()\"","") %>                                    
  	     <br> 
         <%=widgetFactory.createRadioButtonField("PODataOption","TradePortalConstants.GROUP_PO_LINE_ITEMS","LocateUploadPO.GroupUploadedPOLineItems",TradePortalConstants.GROUP_PO_LINE_ITEMS,defaultRadioButtonValue2, false," labelClass=\"formWidth\" onClick=\"setPOItemsGroupingOption()\"","") %>                                    
         <br>
  
  
  
    <div class="formItem"> 
    <div class="formItem"> 
    <%  
    //Show only if user has the security right to opt for Import LC for PO file upload.
    if(!readOnlyDLC)
    {
    %>
    
       
        <%=widgetFactory.createRadioButtonField("POUploadInstrOption","TradePortalConstants.AUTO_LC_PO_UPLOAD","LocateUploadPO.ImportLC",TradePortalConstants.AUTO_LC_PO_UPLOAD,defaultRadioButtonValueC, readOnlyDLC,"onClick=\"setPODataOption1()\"","") %>                                     
     
    <%--Krishna IR-AGUI031880087(Visibilty Of LC/ATP option in The PO FileUpload Page)--%>   
    <%  
    //Put empty spaces only if next option ATP security right for PO file upload is there for the User.
    if(!readOnlyATP)
    {
    %>
    &nbsp;
    <%
        }//if(!readOnlyATP)
     } //if(!readOnlyDLC)
    //Show only if user has the security right to opt for Approval to Pay for PO file upload. 
    if(!readOnlyATP)
    {
    %>
    
        <%=widgetFactory.createRadioButtonField("POUploadInstrOption","TradePortalConstants.AUTO_ATP_PO_UPLOAD","LocateUploadPO.ApprovalToPay",TradePortalConstants.AUTO_ATP_PO_UPLOAD,defaultRadioButtonValueD, readOnlyATP,"onClick=\"setPODataOption1()\"","") %>                                    
      
    <%--Krishna IR-AGUI031880087(Visibilty Of LC/ATP option in The PO FileUpload Page) End--%> 
    <%
     } //if(!readOnlyATP)
    %>
   </div>
 
 		<%=widgetFactory.createCheckboxField("POItemsGroupingOption", "LocateUploadPO.IncludePODataInTheFile",TradePortalConstants.USE_ONLY_FILE_PO_LINE_ITEMS, defaultRadioButtonValueA, false, false, " labelClass=\"formWidth\" onClick=\"setPODataOption()\"", "", "" ) %>
 	</div>	
    </div>    
        <%= widgetFactory.createWideSubsectionHeader("LocateUploadPO.Step3") %>
          
          <%
             if (poUploadDefinitionOid == null)
             {
                poUploadDefinitionOid = "";
             }

             dropdownOptions.append(Dropdown.createSortedOptions(poUploadDefinitionDoc, "PO_UPLOAD_DEFINITION_OID", 
                                                                "NAME", poUploadDefinitionOid, userSession.getSecretKey(), resMgr.getResourceLocale()));

             String defaultText = resMgr.getText("LocateUploadPO.SelectUploadDefinition", TradePortalConstants.TEXT_BUNDLE);
             out.println(widgetFactory.createSelectField( "POUploadDefinition", "",defaultText, dropdownOptions.toString(), false));
         %>    
  </div>

  </form>
  </div>
  </div>
</body>
</html>
<jsp:include page="/common/Footer.jsp">
   <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_NO%>" />
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>
<%
   // Finally, reset the cached document to eliminate carryover of
   // information to the next visit of this page.
   formMgr.storeInDocCache("default.doc", new DocumentHandler());
%>
<script language="JavaScript">

<%--
// This function unchecks the PO Items grouping option if the second PO data 
// option is selected and checkes the first PO Items grouping option only if 
// the user is changing to the first PO data option (does nothing if it's 
// already selected.
--%>
function setPOItemsGroupingOption()
{
	require(["dijit/registry"],
	 function(registry) {
	var radio0 = dijit.getEnclosingWidget(document.forms[0].POUploadInstrOption[0]);
    var radio1 = dijit.getEnclosingWidget(document.forms[0].POUploadInstrOption[1]);
   
   if (document.forms[0].PODataOption[0].checked == true)
   {
	   var instrOptionlist = document.getElementsByName("POUploadInstrOption");
		  if(instrOptionlist.length == 1){
			  registry.getEnclosingWidget(document.forms[0].POUploadInstrOption).set('checked',false);
		  }else if(instrOptionlist.length == 2){
			  radio0.set('checked', false);
			  radio1.set('checked', false);
		  }
	  document.forms[0].PODataOption[1].checked = false;
      dijit.byId("POItemsGroupingOption").set('checked',false);
   }
   else
   {
	   if (document.forms[0].PODataOption[1].checked == true) 
      {
    	    document.forms[0].PODataOption[0].checked = false;
       }
   }
	});
}

<%--  This function checks the first PO data option if it hasn't already been  --%>
<%--  selected. This function gets called when the user selects either of the  --%>
<%--  PO Items grouping options. --%>
function setPODataOption()
{
	var ch = dijit.getEnclosingWidget(document.forms[0].PODataOption[1]);
   if (document.forms[0].PODataOption[1].checked == false)
   {
	  document.forms[0].PODataOption[1].checked = true;	  
	  ch.set('checked',true);
   }
   if (document.forms[0].POItemsGroupingOption.checked == true) {
	   dijit.byId("POItemsGroupingOption").set("value","<%= TradePortalConstants.INCLUDE_ALL_PO_LINE_ITEMS %>");
   }else{
	   dijit.byId("POItemsGroupingOption").set("value","<%= TradePortalConstants.USE_ONLY_FILE_PO_LINE_ITEMS%>");
	   dijit.byId("POItemsGroupingOption").set("checked", false);
   }
   
}

function setPODataOption1()
{
	var ch = dijit.getEnclosingWidget(document.forms[0].PODataOption[1]);
	ch.set('checked', true);
      
}



</script>