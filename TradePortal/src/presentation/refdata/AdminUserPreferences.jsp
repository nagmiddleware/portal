<%--
*******************************************************************************
  Admin User Preferences

  Description:
    Displays admin user details directly to user.  Allows user to change their
    password, plus other select user preferences.
      
*******************************************************************************
--%>

<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.ams.tradeportal.busobj.webbean.*,com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.common.*,
                 com.ams.tradeportal.common.cache.*,com.ams.tradeportal.html.*,java.util.*,
                 com.amsinc.ecsg.util.*, com.amsinc.ecsg.frame.*,com.amsinc.ecsg.html.*" %>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session"/>
<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session"/>
<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session"/>
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"/>

<%
  boolean isAdminUser = true;//BSL 09/06/11 CR663 Rel 7.1 Add
  // [START] CR-482  
  // Check if this TradePortal instance is configured for HSM encryption / decryption
  boolean instanceHSMEnabled = HSMEncryptDecrypt.isInstanceHSMEnabled();
	// [END] CR-482
%>


<%-- ************** Data retrieval page setup begins here ****************  --%>

<%
String pageTitle;
String instructions;
String formName;
boolean forcedToChangePassword = false;
boolean isNewUser = false;

String forcedToChangePasswordParm;
String reasonParam;
String helpFileName;
String stricterPasswordSetting = null;
String minimumPasswordLength = null;

Hashtable secureParms = new Hashtable();
DocumentHandler doc = formMgr.getFromDocCache();   
DocumentHandler orgDoc = new DocumentHandler();
DocumentHandler corpSecProfileDoc = new DocumentHandler();
String orgName = "";

  WidgetFactory widgetFactory = new WidgetFactory(resMgr);
  boolean isReadOnly = true;
  boolean isViewInReadOnly = true;
  boolean isWorkGroupReadOnly = false;
  String orgAndLevel = "";
  String options;
  String defaultText;
  String link;
  String links = "";
  String certAuthURL = "";
  String oid = "";
//Suresh CR-603 03/11/2011 Begin
  UserWebBean					  user							  = null;
  StringBuilder                    sqlQuery                        = null; 
  String 						  pSql							  = null;
  String                          dropdownOptions                 = null;
  String 						  allowPayByAnotherAccnt          = "";
  String 						  cbReportingLangOptionInd        = ""; // DK IR T36000025243 Rel8.4 02/18/2014
  //Suresh CR-603 03/11/2011 End
  // CR-451 NShrestha 10/16/2008 Begin --
  int numTotalAccounts = 0;
  int totAccounts = 0;
  int DEFAULT_ACCOUNT_COUNT = 4;
  String buttonPressed = "";
  Vector error = null;
  UserAuthorizedAccountWebBean account[]=null;
  String onLoad = "";
  // CR-451 NShrestha 10/16/2008 End --
  
  //CR-586 Vshah [BEGIN]
    int numTotalTemplateGroups = 0;
    int totTemplateGroups = 0;
    final int DEFAULT_ROWS = 4;
    int DEFAULT_TEMPLATE_COUNT = 4;
    UserAuthorizedTemplateGroupWebBean templateGroup[]=null;
  //CR-586 Vshah [END]
  
  // These booleans help determine the mode of the JSP.  
  boolean getDataFromDoc = false;
  boolean retrieveFromDB = false;
  boolean insertMode = true;

  boolean showSave = true;
  boolean showDelete = true;
  
  boolean showCloseButton=true;
  boolean showDeleteButton=false;
  boolean showSaveButton=false;

  String password1 = "";
  String password2 = "";


  
  // Must initialize these 2 doc, otherwise get a compile error.
  DocumentHandler thresholdGroupDoc = new DocumentHandler();
  DocumentHandler adminSecProfileDoc = new DocumentHandler();
//pgedupudi - Rel-9.3 CR-976A 04/09/2015
  DocumentHandler bnkGrpRestrictRuleDoc = new DocumentHandler();
  DocumentHandler workGroupDoc = new DocumentHandler();
  DocumentHandler accountDoc = new DocumentHandler();
  DocumentHandler templateGroupDoc = new DocumentHandler(); //CR-586


  String multipleTransactionPartsInd;
  String displayExtendedListviewInd;
  
  boolean defaultRadioButtonValue1 = true;
  boolean defaultRadioButtonValue2 = false;

  if(request.getParameter("numberOfMultipleObjects")!=null)
     numTotalAccounts = Integer.parseInt(request.getParameter("numberOfMultipleObjects"));
  else
     numTotalAccounts = DEFAULT_ROWS;
     
  //CR-586 Vshah [BEGIN]   
  if(request.getParameter("numberOfMultipleObjects1")!=null)
     numTotalTemplateGroups = Integer.parseInt(request.getParameter("numberOfMultipleObjects1"));
  else
     numTotalTemplateGroups = DEFAULT_ROWS;   
  //CR-586 Vshah [END]

  
  user = beanMgr.createBean(UserWebBean.class, "User");

  CorporateOrganizationWebBean corporateOrg = null;
  
  //Suresh CR-603 03/14/2011 Begin
  DocumentHandler				  reportCategsDoc				  = null;
  pSql = "select CODE, DESCR from refdata where table_type = 'REPORTING_CATEGORY' and CODE IN ( " + 
		  "select first_rept_categ from CORPORATE_ORG where organization_oid= ?"+
		  " UNION ALL select second_rept_categ from CORPORATE_ORG where organization_oid= ?" +
		  " UNION ALL select third_rept_categ from CORPORATE_ORG where organization_oid=?"+
		  " UNION ALL select fourth_rept_categ from CORPORATE_ORG where organization_oid=?"+
		  " UNION ALL select fifth_rept_categ from CORPORATE_ORG where organization_oid=?"+
		  " UNION ALL select sixth_rept_categ from CORPORATE_ORG where organization_oid=?"+
		  " UNION ALL select seventh_rept_categ from CORPORATE_ORG where organization_oid=?"+
		  " UNION ALL select eighth_rept_categ from CORPORATE_ORG where organization_oid=?"+
		  " UNION ALL select nineth_rept_categ from CORPORATE_ORG where organization_oid=?"+
		  " UNION ALL select tenth_rept_categ from CORPORATE_ORG where organization_oid=?"+
		  " )";

//jgadela  R90 IR T36000026319 - SQL FIX
  Object sqlParams[] = new Object[10];
  sqlParams[0] = userSession.getOwnerOrgOid();
  sqlParams[1] = userSession.getOwnerOrgOid();
  sqlParams[2] = userSession.getOwnerOrgOid();
  sqlParams[3] = userSession.getOwnerOrgOid();
  sqlParams[4] = userSession.getOwnerOrgOid();
  sqlParams[5] = userSession.getOwnerOrgOid();
  sqlParams[6] = userSession.getOwnerOrgOid();
  sqlParams[7] = userSession.getOwnerOrgOid();
  sqlParams[8] = userSession.getOwnerOrgOid();
  sqlParams[9] = userSession.getOwnerOrgOid();
  reportCategsDoc = DatabaseQueryBean.getXmlResultSet(pSql, false, sqlParams);
  //Suresh CR-603 03/14/2011 End

  String loginLocale;
  String loginRights;
  
  String ownershipLevel = "";  
  String userOrgOid = "";
  //Vasavi CR 580 06/01/10 Begin
  String         userSecurityType        = null;
  
  // Retrieve the user's security type (i.e., ADMIN or NON_ADMIN)
  if (userSession.getSavedUserSession() == null) {
  		userSecurityType = userSession.getSecurityType();
   }else {
	   userSecurityType = userSession.getSavedUserSessionSecurityType();
   } 
  Cache CBCache = (Cache)TPCacheManager.getInstance().getCache(TradePortalConstants.CLIENT_BANK_CACHE);
  DocumentHandler CBCResult = (DocumentHandler)CBCache.get(userSession.getClientBankOid());
  String canViewIDFields;
  String canViewPaymentTemplateGroups; //IR-VUK090959969
  
  if (InstrumentServices.isBlank(userSession.getClientBankOid())){ 
  	    canViewIDFields = "";
  	    canViewPaymentTemplateGroups = ""; //IR-VUK090959969
  }else{
  	   canViewIDFields = CBCResult.getAttribute("/ResultSetRecord(0)/CAN_EDIT_ID_IND");
  	   canViewPaymentTemplateGroups = CBCResult.getAttribute("/ResultSetRecord(0)/TEMPLATE_GROUPS_IND"); //IR-VUK090959969
  	   allowPayByAnotherAccnt = CBCResult.getAttribute("/ResultSetRecord(0)/ALLOW_PAY_BY_ANOTHER_ACCNT"); // RKazi IR HRUL062032745 
  	   cbReportingLangOptionInd = CBCResult.getAttribute("/ResultSetRecord(0)/REPORTING_LANG_OPTION_IND"); // DK IR T36000025243 Rel8.4 02/18/2014
  }	
  //Vasavi CR 580 06/01/10 End	
  	// RKazi IR HRUL062032745 Start
  	if (InstrumentServices.isBlank(allowPayByAnotherAccnt)){
  		String clientBankOid = userSession.getClientBankOid();
  		QueryListView queryListView   = null;
  		sqlQuery = new StringBuilder();

  		sqlQuery.append("select allow_pay_by_another_accnt");
  		sqlQuery.append(" from client_bank");
  		sqlQuery.append(" where organization_oid = ?");

  		queryListView = (QueryListView) EJBObjectFactory.createClientEJB(formMgr.getServerLocation(), "QueryListView");
  		//out.println("QUERY: "+sqlQuery.toString());
  		queryListView.setSQL(sqlQuery.toString(),new Object[]{clientBankOid});
  		queryListView.getRecords();

  		DocumentHandler result = queryListView.getXmlResultSet();
  		allowPayByAnotherAccnt = result.getAttribute("/ResultSetRecord(0)/ALLOW_PAY_BY_ANOTHER_ACCNT");
  	}
	// RKazi IR HRUL062032745 Start
// Either get the page parameters from the input cache or from the JSP parameters
if ( (doc.getAttribute("/Error/maxerrorseverity") == null) ||
     (doc.getAttribute("/Error/maxerrorseverity").compareTo(String.valueOf(ErrorManager.WARNING_ERROR_SEVERITY)) < 0) ) {
  Debug.debug("getting values from params...");
  forcedToChangePasswordParm = request.getParameter("force");
  reasonParam = request.getParameter("reason");
}
else {
  Debug.debug("getting values from input doc...");
  forcedToChangePasswordParm = doc.getAttribute("/In/force");
  reasonParam = doc.getAttribute("/In/reason");
}


if ( (forcedToChangePasswordParm != null) &&
    (forcedToChangePasswordParm.equals("true") ) ) {
  forcedToChangePassword = true;
}

pageTitle = "SecondaryNavigation.UserPreferences";
helpFileName = "admin/user_preferences.htm";
formName = "AdminUserPreferences";
/*if( (reasonParam != null) && (reasonParam.equals("new")) ) {
  isNewUser = true;
  pageTitle = "ChangePassword.NewUserTitle";
  instructions = "ChangePassword.NewUserInstructions";
  formName = "ChangePasswordNewUser";
  helpFileName = "customer/change_passwd_first.htm";
}
else if( (reasonParam != null) && (reasonParam.equals("expired")) ) {
  pageTitle = "ChangePassword.Title";
  instructions = "ChangePassword.ExpiredInstructions";
  formName = "ChangePasswordExpire";
  helpFileName = "customer/change_passwd_expired.htm";
}
else {
  pageTitle = "ChangePassword.Title";
  instructions = "ChangePassword.RegularInstructions";
  formName = "ChangePassword";
  helpFileName = "customer/change_passwd_voluntary.htm";
}*/

// Determine text resource key suffix
// Different instructions are displayed if stricter password rules
// are in effect

String suffix = "";

if (userSession.getOwnershipLevel().equals(TradePortalConstants.OWNER_GLOBAL)) {
  stricterPasswordSetting = SecurityRules.getProponixRequireUpperLowerDigit();
  // [START] CR-482
  minimumPasswordLength = String.valueOf(SecurityRules.getProponixPasswordMinimumLengthLimit());
  // [END] CR-482
}
else {
  ClientBankWebBean clientBank = beanMgr.createBean(ClientBankWebBean.class, "ClientBank");
  clientBank.getById(userSession.getClientBankOid());
      
  // [START] CR-482 Updated indenting and added minimumPasswordLength
  if (userSession.getOwnershipLevel().equals(TradePortalConstants.OWNER_CORPORATE)) {
    stricterPasswordSetting = clientBank.getAttribute("corp_password_addl_criteria");
    minimumPasswordLength = clientBank.getAttribute("corp_min_password_length");
  }
  else {
    stricterPasswordSetting = clientBank.getAttribute("bank_password_addl_criteria");
    minimumPasswordLength = clientBank.getAttribute("bank_min_password_len");
  }
  // [END] CR-482
}

if (stricterPasswordSetting.equals(TradePortalConstants.INDICATOR_YES)) {
  suffix = "StricterRules";
}


// CHANGE THIS BASED ON MODE

if(!forcedToChangePassword) {
  onLoad = "document.ChangePassword.currentPassword.focus();";
}
else {
  onLoad = "document.ChangePassword.newPassword.focus();";
}
%>



<%-- Body tag included as part of common header --%>

<%
String minimalHeader = TradePortalConstants.INDICATOR_NO;
if (forcedToChangePassword) {
  minimalHeader = TradePortalConstants.INDICATOR_YES;
}
else {
  minimalHeader = TradePortalConstants.INDICATOR_NO;
}
%>

<%
  // Get the webbeans for the login in user and his security profile.  Set the
  // readonly access appropriately.
  loginLocale = userSession.getUserLocale();
  loginRights = userSession.getSecurityRights();

     corporateOrg = beanMgr.createBean(CorporateOrganizationWebBean.class,"CorporateOrganization");
     corporateOrg.setAttribute("organization_oid", userSession.getOwnerOrgOid());
     corporateOrg.getDataFromAppServer();

  if (SecurityAccess.hasRights(loginRights, SecurityAccess.MAINTAIN_ADM_USERS)) {
     isReadOnly = false;
     
     // If the user's corporate organization requires certificate authentication for users 
     // that can maintain user data and the user's authentication method is password, 
     // display the page in read-only mode and display the information message. 
   
      // NAR-SAUJ060247671 24-SEP-2010
      //if user is admin, force certificate condition should not be check for admin user.if admin has user maintinance access reghts,
      // he will be able to edit customer user data and can create user without checking whether he has 2FA and Certificate rights.
      if(userSecurityType.equals(TradePortalConstants.ADMIN)){
     if (corporateOrg.getAttribute("force_certs_for_user_maint").equals(TradePortalConstants.INDICATOR_YES)) { // Corporate Org requires cert for users that maintain users.
        UserWebBean thisUser = beanMgr.createBean(UserWebBean.class, "User");
        thisUser.setAttribute("user_oid", userSession.getUserOid());
        thisUser.getDataFromAppServer();       
     // T36000013154 Rel 8.2 07/11/2013 - Start
        String authMethod = StringFunction.isNotBlank(thisUser.getAttribute("authentication_method")) ?
       		 thisUser.getAttribute("authentication_method") : corporateOrg.getAttribute("authentication_method");
       		 
        if (!(TradePortalConstants.AUTH_CERTIFICATE.equals(authMethod)
          || TradePortalConstants.AUTH_2FA.equals(authMethod))) { // This user is not using certificate
        	// T36000013154 Rel 8.2 07/11/2013 - End
           // Use a MediatorService to add these errors to the default.doc XML so that 
           // it can be accessed by the error page
           MediatorServices mediatorServices = new MediatorServices();
           mediatorServices.getErrorManager().setLocaleName(resMgr.getResourceLocale());
           mediatorServices.getErrorManager().issueError("AdminUserDetail.jsp",
                                   TradePortalConstants.SECURITY_NEED_CERT_MAINT_USER);
           mediatorServices.addErrorInfo();
           DocumentHandler defaultDoc = new DocumentHandler();
           defaultDoc.setComponent("/Error", mediatorServices.getErrorDoc());
	       defaultDoc.setAttribute("/In/HasErrors",  TradePortalConstants.INDICATOR_YES);
           formMgr.storeInDocCache("default.doc", defaultDoc);

		   isReadOnly = true;
        }
       }
     } 
     
  } else if (SecurityAccess.hasRights(loginRights, SecurityAccess.VIEW_ADM_USERS) || isViewInReadOnly) {
     isReadOnly = true;
  } else {
     // Don't have maintain or view access, send the user back.
        formMgr.setCurrPage("AdminRefDataHome");
        String physicalPage = NavigationManager.getNavMan().getPhysicalPage("AdminRefDataHome", request);
%>
       <jsp:forward page='<%= physicalPage %>' />
<%
        return;
  } 

  // We should only be in this page for corporate users.  If the logged in
  // user's level is not corporate, send back to ref data page.  This
  // condition should never occur but is caught just in case.
  String ownerLevel = userSession.getOwnershipLevel();
    
  /* if (!ownerLevel.equals(TradePortalConstants.OWNER_CORPORATE)) {
        formMgr.setCurrPage("AdminRefDataHome");
        String physicalPage = NavigationManager.getNavMan().getPhysicalPage("AdminRefDataHome", request); */
%>
       <%--  <jsp:forward page='<%= physicalPage %>' /> --%>
<%
       /*  return;
  }
 */
  if( isViewInReadOnly)
  {
 	isReadOnly = true;
  }

  Debug.debug("login_rights is " + loginRights);

  // Get the document from the cache.  We'll use it to repopulate the screen 
  // if the user was entering data with errors.
  doc = formMgr.getFromDocCache();
  Debug.debug("doc from cache is " + doc.toString());
  
//CR-451 NShrestha 10/16/2008 Begin --
  if (doc.getDocumentNode("/In/Update/ButtonPressed") != null) 
  {
      buttonPressed  = doc.getAttribute("/In/Update/ButtonPressed");
      error = doc.getFragments("/Error/errorlist/error");
  }
//CR-451 NShrestha 10/16/2008 End --

  if (doc.getDocumentNode("/In/User") == null ) {
     // The document does not exist in the cache.  We are entering the page from
     // the listview page (meaning we're opening an existing item or creating 
     // a new one.)
     
     oid = EncryptDecrypt.decryptStringUsingTripleDes(EncryptDecrypt.encryptStringUsingTripleDes(userSession.getUserOid(), userSession.getSecretKey()),userSession.getSecretKey());
      

     if (oid == null) {
       getDataFromDoc = false;
       retrieveFromDB = false;
       insertMode = true;
       oid = "0";
     } else if (oid.equals("0")) {
       getDataFromDoc = false;
       retrieveFromDB = false;
       insertMode = true;
     } else {
       // We have a good oid so we can retrieve.
       getDataFromDoc = false;
       retrieveFromDB = true;
       insertMode = false;
     }

  } else {
     // The doc does exist so check for errors.  If highest severity < WARNING,
     // its a good update.

     String maxError = doc.getAttribute("/Error/maxerrorseverity");
     if (maxError.compareTo(String.valueOf(ErrorManager.WARNING_ERROR_SEVERITY))<0) {
        // We've returned from a save/update/delete that was successful
        // We shouldn't be here.  Forward to the RefDataHome page.
        formMgr.setCurrPage("AdminRefDataHome");
        String physicalPage =
                NavigationManager.getNavMan().getPhysicalPage("AdminRefDataHome", request);
%>
       <jsp:forward page='<%= physicalPage %>'/>
<%
        return;
     } else {
        // We've returned from a save/update/delete but have errors.
        // We will display data from the cache.  Determine if we're
        // in insertMode by looking for a '0' oid in the input section
        // of the doc.

        retrieveFromDB = false;
        getDataFromDoc = true;

        String newOid = doc.getAttribute("/In/User/user_oid");

        if (newOid.equals("0")) {
           insertMode = true;
           oid = "0";
        } else {
           // Not in insert mode, use oid from input doc.
           insertMode = false;
           oid = doc.getAttribute("/In/User/user_oid");
        }
     }
  }

  String onlineHelpFileName="";
  onlineHelpFileName = "admin/admin_user_detail.htm";
  // Make sure that users cannot delete themselves
  
  String thisUserOid;
  thisUserOid = userSession.getUserOid();
  //IAZ CR-475 06/21/09 and 06/28/09 Begin
  //if users of this corp (as driven by Client Bank) cannot modify thier own profiles,
  //make sure the mode isReadOnly if user access his/her own profile
  //if ( thisUserOid.equals(oid) ) showDelete = false;
  try {
  if ( thisUserOid.equals(oid) ) 
  {
  	showDelete = false;
  	if (!isReadOnly)
  	{
  	   	if (InstrumentServices.isNotBlank(userSession.getClientBankOid())) 
   		{  	
   			String canModifyProfile = CBCResult.getAttribute("/ResultSetRecord(0)/CAN_EDIT_OWN_PROFILE_IND");
  			if (TradePortalConstants.INDICATOR_NO.equals(canModifyProfile)){
  				isReadOnly = true;
  			}
  
  	 	}
  	}
  }
  }
  catch (Exception any_e)
  {
		System.out.println("General exception " + any_e.toString() + "verifying if user can edit own profile.  Will use default mode.");	
  }   
  //IAZ CR-475 06/21/09 and 06/28/09 End

  if (retrieveFromDB) {
     // Attempt to retrieve the item.  It should always work.  Still, we'll
     // check for a blank oid -- this indicates record not found.  Set to 
     // insert mode and display error.

     getDataFromDoc = false;

     user.setAttribute("user_oid", oid);
     user.getDataFromAppServer();

     if (user.getAttribute("user_oid").equals("")) {
       insertMode = true;
       oid = "0";
       getDataFromDoc = false;
     } else {
       insertMode = false;
     }
  } 
  
  
  if (getDataFromDoc) {
     // Populate the user bean with the input doc.
     try {
        user.populateFromXmlDoc(doc.getComponent("/In"));
     // org and level is an "attribute" we have created for this web page
        // combines the org selected and the level of that org
        orgAndLevel = doc.getAttribute("/In/User/OwnerOrgAndLevel");
     } catch (Exception e) {
        out.println("Contact Administrator: Unable to get User attributes "
             + " for oid: " + oid + " " + e.toString());
     }  
  
  
  }
 /*  else {
	   // Populate account[] bean with account info from document handler
	   QueryListView queryListView=null;
	   try
		{
		   // Removed Code
		}
		catch (Exception e) { e.printStackTrace(); }
		finally {
		try {
		  if (queryListView != null) {
		    queryListView.remove();
		  }
		} catch (Exception e) { System.out.println("Error removing queryListView in UserDetail.jsp"); }
		}
  } */
  
  // Originally we allowed the user to select/set the organization on this page.
  // Now there is a org selection page where the user must select and org before
  // creating a new user.  Because of this, we now know the org (and level of that
  // org) when entering the page for a new user, or from the webbean for an
  // existing user.  The org and level are passed to the mediator as a combination
  // value (i.e., 'org/level')  [could have passed as two parms but kept as one
  // for historical purposes.]

  try {
	  
      // Not in insert mode, get the oid from the webbean.
      // ownershipLevel represents the level for the user record being edited.
      ownershipLevel = user.getAttribute("ownership_level");
      userOrgOid = user.getAttribute("owner_org_oid");
      orgAndLevel = userOrgOid + "/" + ownershipLevel;

  } catch (Exception e) {
    e.printStackTrace();
  }

  // This println is useful for debugging the state of the jsp.
  Debug.debug("retrieveFromDB: " + retrieveFromDB + " getDataFromDoc: "
              + getDataFromDoc + " insertMode: " + insertMode + " oid: "
              + oid);

  if (isReadOnly || insertMode) showDelete = false;
  if (isReadOnly) showSave = false;
  
  isWorkGroupReadOnly = (isReadOnly || userSession.isUsingSubsidiaryAccess()); //WorkGroup not available for subsidiary access
%>

<%
  // Retrieve the data used to populate some of the dropdowns.

  StringBuilder sql = new StringBuilder();

  QueryListView queryListView = null;
  String parentOrgOid = null;
  
  try {
      queryListView = (QueryListView) EJBObjectFactory.createClientEJB(
				formMgr.getServerLocation(), "QueryListView");
     
      // Get all the security profiles that the user may be assigned to 
      sql = new StringBuilder();
      List<Object> sqlParamsSecProf = new ArrayList();
      sql.append("select security_profile_oid, name ");
      sql.append(" from security_profile");
      sql.append(" where security_profile_type = ?");
      sql.append(" and (p_owner_org_oid = ?)" );
      sqlParamsSecProf.add(TradePortalConstants.ADMIN);
      sqlParamsSecProf.add(userSession.getOwnerOrgOid());
    
      
   // Onwership level represents the ownership level of the user being edited.
      // (NOT the level of the logged in user).
      if (ownershipLevel.equals(TradePortalConstants.OWNER_GLOBAL)) {
         // For Global Org users, the list of profiles is those belonging to 
         // the global org and the client banks
         sql.append(" and p_owner_org_oid = ?" );
         sqlParamsSecProf.add(userSession.getGlobalOrgOid());
      } else if (ownershipLevel.equals(TradePortalConstants.OWNER_BANK)) {
         // For Client Bank users, the list of profiles is those belonging to 
         // the global org and the client bank.
         sql.append(" and (p_owner_org_oid = ?");
         sql.append(" or p_owner_org_oid = ?)");
         sqlParamsSecProf.add(userSession.getGlobalOrgOid());
         sqlParamsSecProf.add(userOrgOid);
      } else if (ownershipLevel.equals(TradePortalConstants.OWNER_BOG)) {
         // For BOG users, the list of profiles is those belonging to 
         // the global org, the client bank, or the BOG.  We know that BOG users can
         // only be edited by other BOG users in that BOG or client bank users who
         // own that BOG--therefore, the client bank of the logged in user must be
         // the same as the client bank of the user being edited.
         sql.append(" and (p_owner_org_oid = ?");
         sql.append(" or p_owner_org_oid = ?" );
         sql.append(" or p_owner_org_oid = ?)" );
         sqlParamsSecProf.add(userSession.getGlobalOrgOid());
         sqlParamsSecProf.add(userSession.getClientBankOid());
         sqlParamsSecProf.add(userOrgOid);
       
      }

      sql.append(" order by ");
      sql.append(resMgr.localizeOrderBy("name"));
	  Debug.debug(sql.toString());
      queryListView.setSQL(sql.toString(),sqlParamsSecProf);

      queryListView.getRecords();

      adminSecProfileDoc = queryListView.getXmlResultSet();
      Debug.debug(adminSecProfileDoc.toString());
      
      /* pgedupudi - Rel-9.3 CR-976A 04/09/2015 Start*/
      /* IR#T36000039962 pgedupudi - Rel-9.3 CR-976A 06/04/2015 */
      if(ownershipLevel.equals(TradePortalConstants.OWNER_BANK) ){
	      sql = new StringBuilder();
	      sql.append("select BANK_GRP_RESTRICT_RULES_OID,NAME   ");
	      sql.append(" from BANK_GRP_RESTRICT_RULES");
	      sql.append(" where P_CLIENT_BANK_OID in (?) ");
	      sql.append(" order by ");
	      sql.append(resMgr.localizeOrderBy("name"));
	      Debug.debug(sql.toString());
	      List params=new ArrayList();
	      params.add(userSession.getClientBankOid());
	      bnkGrpRestrictRuleDoc = DatabaseQueryBean.getXmlResultSet(sql.toString(),false,params);
	      Debug.debug(bnkGrpRestrictRuleDoc.toString());
      }
      /* pgedupudi - Rel-9.3 CR-976A 04/09/2015 End*/
 
   // Now get a list of NON_ADMIN security profiles.

      sql = new StringBuilder();
      sql.append("select security_profile_oid, name ");
      sql.append(" from security_profile");
      sql.append(" where security_profile_type = ?");
      List<Object> sqlParamsNonAdmin = new ArrayList();
      sqlParamsNonAdmin.add(TradePortalConstants.NON_ADMIN);
      if (ownershipLevel.equals(TradePortalConstants.OWNER_GLOBAL)) {
         // For global org users, the list of profiles is those belonging to 
         // global org only 
         sql.append(" and p_owner_org_oid = ?");
         sqlParamsNonAdmin.add( userSession.getGlobalOrgOid());
      } else if (ownershipLevel.equals(TradePortalConstants.OWNER_BANK)) {
         // For Client Bank users, the list of profiles is those belonging to 
         // global org or the client bank.
         sql.append(" and (p_owner_org_oid = ?");
         sql.append(" or p_owner_org_oid = ?)");
         sqlParamsNonAdmin.add( userSession.getGlobalOrgOid());
         sqlParamsNonAdmin.add( userOrgOid);
      } else if (ownershipLevel.equals(TradePortalConstants.OWNER_BOG)) {
         // For BOG users, the list of profiles is those belonging to 
         // global org, the client bank, or the BOG.  We know that BOG users can
         // only be edited by other BOG users in that BOG or client bank users who
         // own that BOG--therefore, the client bank of the logged in user must be
         // the same as the client bank of the user being edited.
         sql.append(" and (p_owner_org_oid = ?" );
         sql.append(" or p_owner_org_oid = ?");
         sql.append(" or p_owner_org_oid = ?)" );
         sqlParamsNonAdmin.add(userSession.getGlobalOrgOid());
         sqlParamsNonAdmin.add( userSession.getClientBankOid());
         sqlParamsNonAdmin.add(userOrgOid);
      }


      sql.append(" order by ");
      sql.append(resMgr.localizeOrderBy("name"));
      Debug.debug(sql.toString());
      queryListView.setSQL(sql.toString(),sqlParamsNonAdmin);

      queryListView.getRecords();

      corpSecProfileDoc = queryListView.getXmlResultSet();
      Debug.debug(corpSecProfileDoc.toString());
      
      sql = new StringBuilder();

      if (ownershipLevel.equals(TradePortalConstants.OWNER_GLOBAL)) {
         sql.append("select name from global_organization");
         queryListView.setSQL(sql.toString(),new ArrayList<Object>());
      } else if (ownershipLevel.equals(TradePortalConstants.OWNER_BANK)) {
         sql.append("select name from client_bank");
         sql.append(" where organization_oid = ?");
         queryListView.setSQL(sql.toString(),new Object[]{userOrgOid});
      } else if (ownershipLevel.equals(TradePortalConstants.OWNER_BOG)) {
         sql.append("select name, p_client_bank_oid from bank_organization_group");
         sql.append(" where organization_oid = ?");
         queryListView.setSQL(sql.toString(),new Object[]{userOrgOid});
      }
      Debug.debug(sql.toString());
     

      queryListView.getRecords();
      
      orgDoc = queryListView.getXmlResultSet();
      Debug.debug(orgDoc.toString());
      orgName = orgDoc.getAttribute("/ResultSetRecord(0)/NAME");

      if(ownershipLevel.equals(TradePortalConstants.OWNER_BOG))
       {
         parentOrgOid = orgDoc.getAttribute("/ResultSetRecord(0)/P_CLIENT_BANK_OID");
       }
      
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      try {
          if (queryListView != null) {
              queryListView.remove();
          }
      } catch (Exception e) {
          System.out.println("error removing querylistview in UserDetail.jsp");
      }
  }  // try/catch/finally block


  // Determine if the bank requires passwords for authentication.  This
  // boolean gets passed to a fragment when creating the security section.
  /* String passwordSql = "select authentication_method "
                     + "from corporate_org where organization_oid = '"
                     + userSession.getOwnerOrgOid()+"'"; */

  boolean passwordsUsed = true;
  String passwordSql;
//jgadela R90 IR T36000026319 - SQL INJECTION FIX
  List<Object> sqlParams1 = new ArrayList();


  // Get the authentication method for a global organization user (from global_organization)
  // or a client bank or BOG user (from client_bank)
  if (ownershipLevel.equals(TradePortalConstants.OWNER_GLOBAL)) {
     passwordSql = "select authentication_method from global_organization "
                 + "where organization_oid = ? ";
     sqlParams1.add(userSession.getOwnerOrgOid());

  } else if(ownershipLevel.equals(TradePortalConstants.OWNER_BANK))
      {
        passwordSql = "select authentication_method "
                    + "from client_bank where organization_oid =? ";
        sqlParams1.add(userOrgOid);
      }
     else
       {
        passwordSql = "select authentication_method "
                    + "from client_bank where organization_oid = ? ";
        sqlParams1.add(parentOrgOid);
      }   
  DocumentHandler result = DatabaseQueryBean.getXmlResultSet(passwordSql, false, sqlParams1);

  String corpAuthMethod = result.getAttribute("/ResultSetRecord(0)/AUTHENTICATION_METHOD");  
  String userAuthMethod = user.getAttribute("authentication_method");
  //cquinton 11/15/2012 add fields necessary for common inclusions (though not used)
  String additionalAuthType = user.getAttribute("additional_auth_type");
  String additionalAuth2FAType = user.getAttribute("auth2fa_subtype");

	// [START] CR-482


	if(instanceHSMEnabled)
	{
		if(ownershipLevel.equals(TradePortalConstants.OWNER_GLOBAL))
		{
			Debug.debug("[AdminUserDetail.jsp] User's ownership level is OWNER_GLOBAL");
			stricterPasswordSetting = SecurityRules.getProponixRequireUpperLowerDigit();
			Debug.debug("[AdminUserDetail.jsp] stricterPasswordSetting = " + stricterPasswordSetting);
			minimumPasswordLength = String.valueOf(SecurityRules.getProponixPasswordMinimumLengthLimit());
			Debug.debug("[AdminUserDetail.jsp] minimumPasswordLength = " + minimumPasswordLength);
		} // if(ownershipLevel.equals(TradePortalConstants.OWNER_GLOBAL))
		else 
		{
			ClientBankWebBean clientBank = beanMgr.createBean(ClientBankWebBean.class,"ClientBank");
			
			// [START] PAUK020539189
			String clientBankOid = null;
			if(InstrumentServices.isNotBlank(userSession.getClientBankOid())) 
			{
				clientBankOid = userSession.getClientBankOid();
				Debug.debug("[AdminUserDetail.jsp] Got clientBankOid from userSession.getClientBankOid");
			} // if(InstrumentServices.isNotBlank(userSession.getClientBankOid())) 
			else if(InstrumentServices.isNotBlank(doc.getAttribute("/In/User/user_oid")))
			{
				clientBankOid = doc.getAttribute("/In/User/user_oid");
				Debug.debug("[AdminUserDetail.jsp] Got clientBankOid from doc.getAttribute(\"/In/User/user_oid\")");
			} // else if(InstrumentServices.isNotBlank(doc.getAttribute("/In/User/user_oid")))
			else if(InstrumentServices.isNotBlank(user.getAttribute("client_bank_oid")))
			{
				clientBankOid = user.getAttribute("client_bank_oid");
				Debug.debug("[AdminUserDetail.jsp] Got clientBankOid from user.getAttribute(\"client_bank_oid\")");
			} // else if(InstrumentServices.isNotBlank(user.getAttribute("client_bank_oid")))

			if(clientBankOid != null)
			{
				Debug.debug("[AdminUserDetail.jsp] Getting client bank with oid = "+clientBankOid);
				clientBank.getById(clientBankOid);			

				if(ownershipLevel.equals(TradePortalConstants.OWNER_CORPORATE))
				{
					Debug.debug("[AdminUserDetail.jsp] User's ownership level is OWNER_CORPORATE");
					stricterPasswordSetting = clientBank.getAttribute("corp_password_addl_criteria");
					Debug.debug("[AdminUserDetail.jsp] stricterPasswordSetting = " + stricterPasswordSetting);
					minimumPasswordLength = clientBank.getAttribute("corp_min_password_length");
					Debug.debug("[AdminUserDetail.jsp] minimumPasswordLength = " + minimumPasswordLength);
				} // if(ownershipLevel.equals(TradePortalConstants.OWNER_CORPORATE))
				else
				{
					stricterPasswordSetting = clientBank.getAttribute("bank_password_addl_criteria");
					Debug.debug("[AdminUserDetail.jsp] stricterPasswordSetting = " + stricterPasswordSetting);
					minimumPasswordLength = clientBank.getAttribute("bank_min_password_len");
					Debug.debug("[AdminUserDetail.jsp] minimumPasswordLength = " + minimumPasswordLength);
				} // else
			} // if(clientBankOid != null)
		} // else

		// Default values for stricterPasswordSetting and minimumPasswordLength
		if((stricterPasswordSetting == null) || (stricterPasswordSetting.trim().length() == 0))
		{
			stricterPasswordSetting = TradePortalConstants.INDICATOR_YES;
		}
		if((minimumPasswordLength == null) || (minimumPasswordLength.trim().length() == 0))
		{
			minimumPasswordLength = "8";
		}
		// [END] PAUK020539189
	} // if(instanceHSMEnabled)
	// [END] CR-482
%>

 

<jsp:include page="/common/Header.jsp">
   <jsp:param name="minimalHeaderFlag" value="<%= minimalHeader %>" />
   <jsp:param name="additionalOnLoad" value="<%=onLoad%>" />
</jsp:include>

<div class="pageMainNoSidebar">
  <div class="pageContent">

    <jsp:include page="/common/PageHeader.jsp">
      <jsp:param name="titleKey" value="<%= pageTitle %>" />
      <jsp:param name="helpUrl"  value="<%= helpFileName %>" />
    </jsp:include>

    <%-- cquinton 10/29/2012 ir#7015 remove return link--%>
    <jsp:include page="/common/PageSubHeader.jsp">
      <jsp:param name="title" value="<%= userSession.getOrganizationName() %>" />
    </jsp:include>
        <%
        boolean showtips = userSession.getShowToolTips() == null ? true : userSession.getShowToolTips().booleanValue(); 
        String datepattern = userSession.getDatePattern();
        %>

    <form id="ChangePassword" name="ChangePassword" method="post" data-dojo-type="dijit.form.Form" action="<%=formMgr.getSubmitAction(response)%>">
      <input type=hidden name="buttonName"     value="">
      <input type=hidden value="<%=additionalAuthType%>"
             name="additionalAuthType" id="additionalAuthType">

      <div class="formArea">
        <jsp:include page="/common/ErrorSection.jsp" />		     
        <div class="formContent"> <%-- Form Content Area starts here --%>
<%
  // [START] CR-482
  if (instanceHSMEnabled)
  {
%>
          <input type=hidden name="encryptedCurrentPassword" value="">
          <%-- [START] CR-543 --%>
          <input type=hidden name="protectedCurrentPassword" value="">
          <input type=hidden name="protectedNewPassword" value="">
          <%-- T36000030120-smanohar--	check password complexity at server also for HSMEnabled instances-- START --%>
          <input type=hidden name="encryptedRetypedNewPassword" value="">
          <%-- T36000030120-smanohar--	check password complexity at server also for HSMEnabled instances-- END --%>
          
          <%-- [END] CR-543 --%>
          <input type=hidden name="passwordValidationError" value="">
<%
  } // if (instanceHSMEnabled)
  // [END] CR-482
%>

<%
  Hashtable addlParms = new Hashtable();

  // The system must keep track of how many times the user has entered an invalid current password
  // This is done to prevent someone from attempting to test for different passwords
  // when a user is away from their computer.  

  // If the maximum number has been reached, the user is forwarded
  // to the "Too Many Logons" page, which also logs them out    
  int numberOfWrongPasswords;
  
  try {
    numberOfWrongPasswords = Integer.parseInt(doc.getAttribute("/Out/numberOfWrongPasswords"));
  }
  catch(Exception e) {
    numberOfWrongPasswords = 0;
  }


  if ( numberOfWrongPasswords == SecurityRules.getLoginAttemptsBeforeBeingRedirected() ) {
    String tooManyLogonsPage = NavigationManager.getNavMan().getPhysicalPage("TooManyInvalidLogonAttempts", request);

    StringBuffer parms = new StringBuffer();
    parms.append("?branding=");
    parms.append(userSession.getBrandingDirectory());
    parms.append("&organization=");
    parms.append(userSession.getClientBankCode());
    parms.append("&locale=");
    parms.append(userSession.getUserLocale());

%>
          <jsp:forward page='<%= tooManyLogonsPage + parms %>' />
<%
  }

  // Add the numberOfWrongPasswords to formManager
  addlParms.put("numberOfWrongPasswords", String.valueOf(numberOfWrongPasswords));


  addlParms.put("userOID",userSession.getUserOid());

  // The ChangePasswordMediator (where the data is sent from this page)
  // is also used to unlock users.  These flags tell the mediator that we're not unlocking a user here
  addlParms.put("changingPasswordFlag", "true");
  addlParms.put("resettingUserFlag", "false");

  if(forcedToChangePassword) {
    addlParms.put("force",forcedToChangePasswordParm);
    addlParms.put("checkCurrentPasswordFlag", "false");
  }
  else {
    addlParms.put("checkCurrentPasswordFlag", "true");
  }
      
  if(reasonParam != null) {
    addlParms.put("reason", reasonParam);
  }
  
  addlParms.put("fromUserPref", "true");
%>

          <%= formMgr.getFormInstanceAsInputField(formName, addlParms) %>

          <%= widgetFactory.createSectionHeader("1", "ChangePassword.PasswordSection") %>
            <div class = "columnLeft" style="border-right:0px">
<%
  if (!forcedToChangePassword) {
%>
              <%= widgetFactory.createTextField( "currentPassword", "ChangePassword.CurrentPassword",
                                       "", "32", false, false, false, "type=\"password\" AutoComplete=\"off\"", "", "" ) %>
<%
  }
%>
              <%= widgetFactory.createTextField( "newPassword", "ChangePassword.NewPassword",
                                       "", "32", false, false, false, "type=\"password\" AutoComplete=\"off\"", "", "" ) %>

              <%= widgetFactory.createTextField( "retypePassword", "ChangePassword.RetypeNewPassword",
                                       "", "32", false, false, false, "type=\"password\" AutoComplete=\"off\"", "", "" ) %>

<%
    options = Dropdown.createSortedRefDataOptions("DATEPATTERN",
                datepattern,
                loginLocale);        
%>
        
              <%=widgetFactory.createSelectField("datepattern",
                                        "UserDetail.DatePattern", " ", options, false,
                                        true, false, "", "", "")%>
              <div class = "formItem">        
                        <button id="saveButton" data-dojo-type="dijit.form.Button" type="button" data-dojo-props="iconClass:'save'">
                          <%=resMgr.getText("common.SaveText",TradePortalConstants.TEXT_BUNDLE)%>
                        </button><%-- save formatting button --%>
                        <%=widgetFactory.createHoverHelp("saveButton","SaveHoverText") %>
               </div><%-- end of formItem --%>              
            </div><%-- End of columnLeft --%>
            <div class="columnRight" >
              <%= widgetFactory.createNote("ChangePassword.PasswordRequirements")%>
              <br>
              <br>
              <%= widgetFactory.createNote("ChangePassword.Instructions1") %>
              <br>
              <br>
              <%= widgetFactory.createNote("ChangePassword.Instructions2") %>
              <br>
              <br>
              <%= widgetFactory.createNote("ChangePassword.Instructions3") %>
              <br>
              <br>
              <br>
              <br>
               <%=widgetFactory.createSubLabel("UserDetail.ShowHideTips")%>
              <div class="formItem">
                  <div class="indentHalf">
                  <%=widgetFactory.createRadioButtonField(
                      "showtips",
                      "ShowTips",
                      "UserDetail.ShowTips", "Y", 
                       showtips,
                      false, "", "")%>

                    <br>
                  <%=widgetFactory.createRadioButtonField(
                       "showtips",
                       "HideTips",
                       "UserDetail.HideTips", "N", 
                       ! showtips,
                       false, "", "")%>
                   </div><%-- indentHalf end --%>  
               </div> <%-- formItem end --%> 
            </div><%-- End of columnRight --%>	
          </div><%-- End of createSectionHeader --%>
	
          <%= widgetFactory.createSectionHeader( "2","AdminUserPreferences.General", "noToggle") %> 
            <%@ include file="fragments/AdminUserDetail-General.frag" %> 
          </div>

          <%= widgetFactory.createSectionHeader( "3","AdminUserPreferences.Security", "noToggle") %> 
            <%@ include file="fragments/UserSecuritySection.frag" %>
          </div>
      
          <%= widgetFactory.createSectionHeader( "4","AdminUserPreferences.AssignedTo", "noToggle") %> 
            <%@ include file="fragments/AdminUserDetail-AssignedTo.frag" %> 
          </div> 
 
          <%= widgetFactory.createSectionHeader( "5","AdminUserPreferences.CorporateAccess", "noToggle") %> 
            <%@ include file="fragments/AdminUserDetail-CorporateAccess.frag" %> 	
          </div> 
	
        </div> <%-- Form Content End Here --%>
      </div><%--formArea--%>

      <div class="formSidebar" data-dojo-type="widget.FormSidebar" data-dojo-props="form: 'ChangePassword'">
        <jsp:include page="/common/RefDataSidebar.jsp"> 
          <jsp:param name="showTop" value="true" />
          <jsp:param name="showLinks" value="true" />
          <jsp:param name="cancelAction" value="goToTradePortalHome" />
        </jsp:include>
      </div> <%--closes sidebar area--%>

    <%--cquinton 10/31/2012 move closing form tag so it is before footer, which has other forms--%>
    </form>
  </div>
</div>

<jsp:include page="/common/Footer.jsp">
   <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>
<jsp:include page="/common/RefDataSidebarFooter.jsp" />


<%
  // [START] CR-482
  // Include javascript functions for encryption
  if (instanceHSMEnabled)
  {
%>
<script src="js/crypto/pidcrypt.js"></script>
<script src="js/crypto/pidcrypt_util.js"></script>
<script src="js/crypto/asn1.js"></script><%-- needed for parsing decrypted rsa certificate --%>
<script src="js/crypto/jsbn.js"></script><%-- needed for rsa math operations --%>
<script src="js/crypto/rng.js"></script><%-- needed for rsa key generation --%>
<script src="js/crypto/prng4.js"></script><%-- needed for rsa key generation --%>
<script src="js/crypto/rsa.js"></script><%-- needed for rsa en-/decryption --%>
<script src="js/crypto/md5.js"></script><%-- needed for key and iv generation --%>
<script src="js/crypto/aes_core.js"></script><%-- needed block en-/decryption --%>
<script src="js/crypto/aes_cbc.js"></script><%-- needed for cbc mode --%>
<script src="js/crypto/sha256.js"></script>
<script src="js/crypto/tpprotect.js"></script><%-- needed for cbc mode --%> 
<%
  } // if (instanceHSMEnabled)
%>

<script>

  <%--change password event handler--%>
  require(["dojo/query", "dojo/dom", "dojo/on", "dojo/ready", "dojo/domReady!"],
      function(query, dom, on, ready) {
	  ready(function() {
		    <%-- Save Password button   --%>
		    var saveButton = dom.byId("saveButton");
		    on(saveButton, "click", function(evt){

<%
  if (instanceHSMEnabled) {
    UserWebBean user1 = beanMgr.createBean(UserWebBean.class, "User");
    user1.getById(userSession.getUserOid());    
%>
      var public_key = '-----BEGIN PUBLIC KEY-----\n<%= HSMEncryptDecrypt.getPublicKey() %>\n-----END PUBLIC KEY-----';
      var iv = '<%= HSMEncryptDecrypt.getNewIV() %>';
      var protector = new TPProtect();
          
      <%-- Ensure that the new password and retyped password match --%>
      if (ChangePassword.passwordValidationError.value == "") {
        ChangePassword.passwordValidationError.value = protector.MatchingEntries({errorCode: "<%= TradePortalConstants.ENTRIES_DONT_MATCH %>", newPassword: ChangePassword.newPassword.value, retypePassword: ChangePassword.retypePassword.value});
      }
          
      <%-- Ensure that the new password is not the same as the user_identifier --%>
      if (ChangePassword.passwordValidationError.value == "") {
        ChangePassword.passwordValidationError.value = protector.IsSameAs({errorCode: "<%= TradePortalConstants.PASSWORD_SAME_AS %>", newPassword: ChangePassword.newPassword.value, sameAsString: "<%= user.getAttribute("user_identifier") %>"});
      }

      <%-- Ensure that the new password is not the same as the first_name --%>
      if (ChangePassword.passwordValidationError.value == "") {
        ChangePassword.passwordValidationError.value = protector.IsSameAs({errorCode: "<%= TradePortalConstants.PASSWORD_SAME_AS %>", newPassword: ChangePassword.newPassword.value, sameAsString: "<%= user.getAttribute("first_name") %>"});
      }
          
      <%-- Ensure that the new password is not the same as the last_name --%>
      if (ChangePassword.passwordValidationError.value == "") {
        ChangePassword.passwordValidationError.value = protector.IsSameAs({errorCode: "<%= TradePortalConstants.PASSWORD_SAME_AS %>", newPassword: ChangePassword.newPassword.value, sameAsString: "<%= user.getAttribute("last_name") %>"});
      }

      <%-- Ensure that the new password is not the same as the login_id --%>
      if (ChangePassword.passwordValidationError.value == "") {
        ChangePassword.passwordValidationError.value = protector.IsSameAs({errorCode: "<%= TradePortalConstants.PASSWORD_SAME_AS %>", newPassword: ChangePassword.newPassword.value, sameAsString: "<%= user.getAttribute("login_id") %>"});
      }

      <%-- Ensure that the new password does not start or end with a space --%>
      if (ChangePassword.passwordValidationError.value == "") {
        ChangePassword.passwordValidationError.value = protector.StartEndWithSpace({errorCode: "<%= TradePortalConstants.PASSWORD_SPACE %>", newPassword: ChangePassword.newPassword.value});
      }

      <%-- Ensure that the new password is the required length --%>
      if (ChangePassword.passwordValidationError.value == "") {
        ChangePassword.passwordValidationError.value = protector.IsRequiredLength({errorCode: "<%= TradePortalConstants.PASSWORD_TOO_SHORT %>", newPassword: ChangePassword.newPassword.value, reqLength: <%= StringFunction.xssCharsToHtml(minimumPasswordLength) %>});
      }

      <%-- Ensure that the new password meets the consecutive characters requirements --%>
      if (ChangePassword.passwordValidationError.value == "") {
        ChangePassword.passwordValidationError.value = protector.CheckConsecutiveChars({errorCode: "<%= TradePortalConstants.PASSWORD_CONSECUTIVE %>", newPassword: ChangePassword.newPassword.value});
      }

<%
    if(stricterPasswordSetting.equals(TradePortalConstants.INDICATOR_YES)) { %>

      if (ChangePassword.passwordValidationError.value == "") {
        ChangePassword.passwordValidationError.value = protector.CheckPasswordComplexity({errorCode: "<%= TradePortalConstants.PASSWORD_UPPER_LOWER_DIGIT %>", newPassword: ChangePassword.newPassword.value});
      }
<%
    }
%>

      <%-- Encrypt two versions of the current password, clear-text and SHA-256 hashed --%>
      if (ChangePassword.currentPassword) {
        ChangePassword.encryptedCurrentPassword.value = protector.Encrypt({publicKey: public_key, usePem: true, iv: iv, data: ChangePassword.currentPassword.value});          
        <%-- [START] CR-543 --%>
        var currentPasswordMac = protector.SignMAC({publicKey: public_key, usePem: true, iv: iv, password: ChangePassword.currentPassword.value, challenge: iv});
        ChangePassword.protectedCurrentPassword.value = protector.Encrypt({publicKey: public_key, usePem: true, iv: iv, data: currentPasswordMac});                    
      }
          
      <%-- Encrypt the MAC of the new password --%>
      <%-- T36000030120-smanohar--	check password complexity at server also for HSMEnabled instances-- START --%>
      
      if (ChangePassword.newPassword) {
        var newPasswordMac = protector.SignMAC({publicKey: public_key, usePem: true, iv: iv, password: ChangePassword.newPassword.value, challenge: iv});
        var combinedNewPassword = newPasswordMac + '|' + document.ChangePassword.newPassword.value;
        ChangePassword.protectedNewPassword.value = protector.Encrypt({publicKey: public_key, usePem: true, iv: iv, data: combinedNewPassword});
      }
      
      
      if (document.ChangePassword.retypePassword) {
          document.ChangePassword.encryptedRetypedNewPassword.value = protector.Encrypt({publicKey: public_key, usePem: true, iv: iv, data: document.ChangePassword.retypePassword.value});  
       }    
      <%-- T36000030120-smanohar--	check password complexity at server also for HSMEnabled instances--END --%>
          
      <%-- Blank out the password fields --%>
      if (ChangePassword.currentPassword) {
        ChangePassword.currentPassword.value = "";
      }
      if (ChangePassword.newPassword) {
        ChangePassword.newPassword.value = "";
      }
      if(ChangePassword.retypePassword) {
        ChangePassword.retypePassword.value = "";
      }
    
<%
  
  } // if (instanceHSMEnabled)
%>

      setButtonPressed('<%= TradePortalConstants.BUTTON_SAVE %>','0');
      
      document.forms[0].submit();
    });
  });

    ready(function(){
    	<%-- On Page lode functionality - set divs to Display:none if isAdminUser true --%>
      	var certificateFieldsDiv = '';
      	var twoFAFieldsDiv = '';
      	var ssoFieldsDiv = '';
      	var passwordFieldsDiv = '';
      	var defaultDisplayDiv = '<%=defaultDisplayDiv%>';
      	
      	if(dom.byId('CertificateFieldsDiv')){
      		certificateFieldsDiv = dom.byId('CertificateFieldsDiv');
      		certificateFieldsDiv.style.display = 'none';
      	}
		if(dom.byId('2FAFieldsDiv')){
			twoFAFieldsDiv = dom.byId('2FAFieldsDiv');
			twoFAFieldsDiv.style.display = 'none';
      	}
		if(dom.byId('SSOFieldsDiv')){
			ssoFieldsDiv = dom.byId('SSOFieldsDiv');
			ssoFieldsDiv.style.display = 'none';
      	}
		if(dom.byId('PasswordFieldsDiv')){
			passwordFieldsDiv = dom.byId('PasswordFieldsDiv');
			passwordFieldsDiv.style.display = 'none';
      	}
		
		<%-- based on defaultDisplayDiv set display: block for that particular div --%>
		<%-- defaultDisplayDiv criteria will be decided based on either the  --%>
		<%-- selected auth_method option if it is existing User or --%>
		<%-- selected radio button in ClientBank under Admin User Auth section if it is InsertMode --%>
		if (defaultDisplayDiv.localeCompare('PasswordFields') == 0){
			passwordFieldsDiv.style.display = 'block';
			domClass.remove("PWDcolumnRight", "columnRight");
			domClass.remove("UserLoginPwd", "indentHalf");
		}else if (defaultDisplayDiv.localeCompare('CertificateFields') == 0){
			certificateFieldsDiv.style.display = 'block';
			passwordFieldsDiv.style.display = 'block';
			domClass.remove("UserLoginPwd", "indentHalf");
		}else if (defaultDisplayDiv.localeCompare('SSOFields') == 0){
			ssoFieldsDiv.style.display = 'block';
		}else if (defaultDisplayDiv.localeCompare('2FAFields') == 0){
			twoFAFieldsDiv.style.display = 'block';
			passwordFieldsDiv.style.display = 'block';
			domClass.remove("UserLoginPwd", "indentHalf");
		}
      <%-- buttonpressed to be called for saveFormating button --%>
      var saveFormattingButton = dom.byId("<%=TradePortalConstants.BUTTON_SAVE_FORMATTING%>");
      if ( saveFormattingButton ) {
        on(saveFormattingButton, "click", function(evt){
          setButtonPressed('<%= TradePortalConstants.BUTTON_SAVE_FORMATTING %>','0');
          document.forms[0].submit(); 
        });    
      }
    });
      
  });

</script>

<%
   // Finally, reset the cached document to eliminate carryover of
   // information to the next visit of this page.
   formMgr.storeInDocCache("default.doc", new DocumentHandler());
%>

</body>
</html>
